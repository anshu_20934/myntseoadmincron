<?php
/*****************************************************************************\
+-----------------------------------------------------------------------------+
| X-Cart                                                                      |
| Copyright (c) 2001-2006 Ruslan R. Fazliev <rrf@rrf.ru>                      |
| All rights reserved.                                                        |
+-----------------------------------------------------------------------------+
| PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
| FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
| AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
|                                                                             |
| THIS  AGREEMENT  EXPRESSES  THE  TERMS  AND CONDITIONS ON WHICH YOU MAY USE |
| THIS SOFTWARE   PROGRAM   AND  ASSOCIATED  DOCUMENTATION   THAT  RUSLAN  R. |
| FAZLIEV (hereinafter  referred to as "THE AUTHOR") IS FURNISHING  OR MAKING |
| AVAILABLE TO YOU WITH  THIS  AGREEMENT  (COLLECTIVELY,  THE  "SOFTWARE").   |
| PLEASE   REVIEW   THE  TERMS  AND   CONDITIONS  OF  THIS  LICENSE AGREEMENT |
| CAREFULLY   BEFORE   INSTALLING   OR  USING  THE  SOFTWARE.  BY INSTALLING, |
| COPYING   OR   OTHERWISE   USING   THE   SOFTWARE,  YOU  AND  YOUR  COMPANY |
| (COLLECTIVELY,  "YOU")  ARE  ACCEPTING  AND AGREEING  TO  THE TERMS OF THIS |
| LICENSE   AGREEMENT.   IF  YOU    ARE  NOT  WILLING   TO  BE  BOUND BY THIS |
| AGREEMENT, DO  NOT INSTALL OR USE THE SOFTWARE.  VARIOUS   COPYRIGHTS   AND |
| OTHER   INTELLECTUAL   PROPERTY   RIGHTS    PROTECT   THE   SOFTWARE.  THIS |
| AGREEMENT IS A LICENSE AGREEMENT THAT GIVES  YOU  LIMITED  RIGHTS   TO  USE |
| THE  SOFTWARE   AND  NOT  AN  AGREEMENT  FOR SALE OR FOR  TRANSFER OF TITLE.|
| THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY GRANTED BY THIS AGREEMENT.      |
|                                                                             |
| The Initial Developer of the Original Code is Ruslan R. Fazliev             |
| Portions created by Ruslan R. Fazliev are Copyright (C) 2001-2006           |
| Ruslan R. Fazliev. All Rights Reserved.                                     |
+-----------------------------------------------------------------------------+
\*****************************************************************************/

#
# $Id: func.order.php,v 1.34.2.5 2006/06/26 05:48:52 max Exp $
#
if ( !defined('XCART_START') ) { header("Location: ../"); die("Access denied"); }
include_once($xcart_dir."/include/func/func.core.php");
include_once($xcart_dir."/include/func/func.sms_alerts.php");
include_once($xcart_dir."/include/func/func_sku.php");
include_once($xcart_dir."/include/func/func.mail.php");
include_once($xcart_dir."/include/class/widget/class.widget.keyvaluepair.php");
include_once($xcart_dir."/modules/coupon/database/CouponAdapter.php");
include_once($xcart_dir."/modules/coupon/CouponValidator.php");
include_once($xcart_dir."/include/func/func.mk_shipment_tracking.php");
include_once($xcart_dir."/include/class/class.mail.multiprovidermail.php");
include_once($xcart_dir."/modules/apiclient/CourierApiClient.php");
include_once($xcart_dir. "/modules/apiclient/ShipmentApiClient.php");
include_once($xcart_dir. "/modules/apiclient/ItemApiClient.php");
include_once($xcart_dir."/include/class/oms/ItemManager.php");
include_once($xcart_dir."/env/Host.config.php");
include_once($xcart_dir."/include/class/class.feature_gate.keyvaluepair.php");
include_once($xcart_dir."/include/func/func.mk_orderbook.php");
include_once($xcart_dir."/PHPExcel/PHPExcel.php");
include_once($xcart_dir."/modules/apiclient/ReleaseApiClient.php");
include_once("$xcart_dir/include/class/oms/EventCreationManager.php");
include_once("$xcart_dir/include/class/oms/OrderProcessingMailSender.php");
include_once("$xcart_dir/modules/apiclient/MinacsApiClient.php");
include_once("$xcart_dir/modules/apiclient/TatApiClient.php");
include_once("$xcart_dir/Cache/CacheKeySet.php");

include_once "$xcart_dir/modules/myntCash/MyntCashTransaction.php";
include_once "$xcart_dir/modules/myntCash/MyntCashService.php";
include_once "$xcart_dir/modules/loyaltyPoints/LoyaltyPointsPortalClient";

use enums\revenue\payments\Gateways;
use style\builder\CachedStyleBuilder;

x_load('cart','crypt','mail','user','refer');
$db_query_retries=3;
function create_new_order() {
	global $XCARTSESSID;
	$checksumkey = uniqid();
	$tempOrderQuery = "INSERT INTO mk_temp_order(sessionid, parameter,checksumkey) VALUES('$XCARTSESSID', '','$checksumkey')";
	db_query($tempOrderQuery);
	
	$retrieveTempOrderQuery = "SELECT orderid FROM mk_temp_order WHERE sessionid='$XCARTSESSID' AND checksumkey='$checksumkey'";
	$row = func_query_first($retrieveTempOrderQuery);
	return $row['orderid'];
}

function create_new_invoice_id($orderid) {
	global $sql_tbl, $sqllog, $server_name, $XCARTSESSID;
	$tb_orders = $sql_tbl["orders"];
	$tb_temp_invoice = $sql_tbl["temp_invoice"];
	$session_id = $XCARTSESSID;
	#
	# generate new invoice number
	#
	$sql = "INSERT INTO $tb_temp_invoice(sessionid, orderid) VALUES('".$session_id."', '".$orderid."')";
	$sqllog->debug("File Name::>"."func.mk_orderbook.php"."##Function Name::>func_generate_invoice_id##"."##SQL Info::>".$sql); 
	db_query($sql);

	#
	#Fetch last inserted invoice id of current session id
	#
	$sql = "SELECT invoiceid FROM $tb_temp_invoice WHERE sessionid='".$session_id."' AND orderid='".$orderid."'";
	$sqllog->debug("File Name::>"."func.mk_orderbook.php"."##Function Name::>func_generate_invoice_id##"."##SQL Info::>".$sql); 
	$invoiceid = func_query_first_cell($sql);
	$invoiceid = "VEC_INV_".$invoiceid;
	
	return $invoiceid;
}

function func_get_order_amount_details($orderids) {
	global $sql_tbl;
	if(!is_array($orderids))
		$orderids = array($orderids);
		
	$sql = "SELECT sum(gift_card_amount) as gift_card_amount, sum(subtotal) as subtotal, sum(discount) as discount, sum(cart_discount) as cart_discount, sum(coupon_discount) as coupon_discount, sum(cash_redeemed) as cash_redeemed, sum(pg_discount) as pg_discount, sum(payment_surcharge) as payment_surcharge, sum(total) as total, sum(shipping_cost) as shipping_cost, sum(gift_charges) as gift_charges, sum(cod) as cod, sum(tax) as tax, sum(cashback) as cashback, sum(loyalty_points_used) as loyalty_points_used, loyalty_points_conversion_factor, payment_method, firstname, ordertype, login, shippeddate, issues_contact_number, coupon, cash_coupon_code, date, warehouseid FROM $sql_tbl[orders] WHERE orderid in (".implode(",", $orderids).") group by login";
	$amount_details = func_query_first($sql);
	$order_details = func_query_first_cell("SELECT sum(difference_refund) as difference_refund, sum(taxamount) as taxamount from xcart_order_details where orderid in (".implode(",", $orderids).") and item_status != 'IC'");
	$amount_details['difference_refund'] = $order_details['difference_refund'];
	$amount_details['taxamount'] = $order_details['taxamount'];
	$amount_details['loyalty_credit'] = $amount_details['loyalty_points_used']*$amount_details['loyalty_points_conversion_factor'];
	return $amount_details;
}

function func_get_order_payable_amount($order) {
    if($order['payment_method'] == 'cod'){
        return $order['total'] + $order['cod'] + $order['payment_surcharge'] - $order['gift_card_amount'];
    } else {
        return $order['total'] + $order['cod'] + $order['payment_surcharge'];
    }
}

function func_select_orders($orderids, $exclude_statuses=false) {
	global $sql_tbl, $config;
	
	$sql = "select o.*, cs.courier_service as courier_service_display, cs.website as courier_website from $sql_tbl[orders] o LEFT JOIN $sql_tbl[courier_service] cs ON o.courier_service = cs.code WHERE o.orderid in (".implode(",", $orderids).")";
	if(is_array($exclude_statuses) && !empty($exclude_statuses)) {
		$sql .= " and o.status not in ('".implode("','", $exclude_statuses)."')";
	}
	
	$orders = func_query($sql);

	foreach($orders as $key=>$order) {
		$order["discounted_subtotal"] = $order["subtotal"] - $order["discount"] - $order["coupon_discount"] - $order['cart_discount'];
		$order["original_address"] = $order["s_address"];
		list($order["b_address"], $order["b_address_2"]) = explode("\n", $order["b_address"]);
		$order["b_statename"] = func_get_state($order["b_state"], $order["b_country"]);
		$order["b_countryname"] = func_get_country($order["b_country"]);
		//list($order["s_address"], $order["s_address_2"]) = explode("\n", $order["s_address"]);
		$order["s_statename"] = func_get_state($order["s_state"], $order["s_country"]);
		$order["s_countryname"] = func_get_country($order["s_country"]);
		$orders[$key] = $order;
	}
	return $orders;
}

function func_select_order($orderid) {
	global $sql_tbl, $config, $current_area, $active_modules, $shop_language;

	//$o_date = "date+'".$config["Appearance"]["timezone_offset"]."' as date";
	$order = func_query_first("select o.*, cs.courier_service as courier_service_display, cs.website as courier_website from $sql_tbl[orders] o LEFT JOIN $sql_tbl[courier_service] cs ON o.courier_service = cs.code WHERE o.orderid='$orderid'");
	if(!$order)
		return false;

	$order["discounted_subtotal"] = $order["subtotal"] - $order["discount"] - $order["coupon_discount"];
	$order["original_address"] = $order["s_address"];
	list($order["b_address"], $order["b_address_2"]) = explode("\n", $order["b_address"]);
	$order["b_statename"] = func_get_state($order["b_state"], $order["b_country"]);
	$order["b_countryname"] = func_get_country($order["b_country"]);
	list($order["s_address"], $order["s_address_2"]) = explode("\n", $order["s_address"]);
	$order["s_statename"] = func_get_state($order["s_state"], $order["s_country"]);
	$order["s_countryname"] = func_get_country($order["s_country"]);
    $order['loyalty_credit'] = $order['loyalty_points_used']*$order['loyalty_points_conversion_factor'];

	return $order;
}

#
# This function returns data about specified order ($orderid)
#
function func_order_data($orderid) {

	global $sql_tbl, $config, $smarty, $active_modules, $current_area, $xcart_dir;
	global $xcart_catalogs,$sqllog;

	$products = func_query("SELECT $sql_tbl[order_details].itemid, $sql_tbl[products].*, $sql_tbl[order_details].*, IF($sql_tbl[products].productid IS NOT NULL OR $sql_tbl[order_details].product = '', $sql_tbl[products].product, $sql_tbl[order_details].product) as product, IF($sql_tbl[products].productid IS NULL, 'Y', '') as is_deleted FROM $sql_tbl[order_details] LEFT JOIN $sql_tbl[products] ON $sql_tbl[order_details].productid = $sql_tbl[products].productid WHERE $sql_tbl[order_details].orderid='$orderid'");

	if (!empty($active_modules["Egoods"])) {
		$join .= " LEFT JOIN $sql_tbl[download_keys] ON $sql_tbl[order_details].itemid=$sql_tbl[download_keys].itemid AND $sql_tbl[download_keys].productid=$sql_tbl[order_details].productid";
		$fields .= ", $sql_tbl[download_keys].download_key, $sql_tbl[download_keys].expires";
	}
	$products = func_query("SELECT $sql_tbl[order_details].itemid, $sql_tbl[products].*, $sql_tbl[order_details].*, IF($sql_tbl[products].productid IS NOT NULL OR $sql_tbl[order_details].product = '', $sql_tbl[products].product, $sql_tbl[order_details].product) as product, IF($sql_tbl[products].productid IS NULL, 'Y', '') as is_deleted $fields FROM $sql_tbl[order_details] LEFT JOIN $sql_tbl[products] ON $sql_tbl[order_details].productid = $sql_tbl[products].productid $join WHERE $sql_tbl[order_details].orderid='$orderid'");

	
	if (!is_array($products))
		$products = array();

	#
	# If products are not present in products table, but they are present in
	# order_details, then create fake $products from order_details data
	#
	$is_returns = false;
	if (!empty($products) && !empty($active_modules['RMA'])) {
		foreach ($products as $k => $v) {
			$products[$k]['returns'] = func_query("SELECT * FROM $sql_tbl[returns] WHERE itemid = '$v[itemid]'");
			if (!empty($products[$k]['returns'])) {
				$is_returns = true;
				foreach (array('A','R','C') as $s) {
					$products[$k]['returns_sum_'.$s] = func_query_first_cell("SELECT SUM(amount) FROM $sql_tbl[returns] WHERE itemid = '$v[itemid]' AND status = '$s'");
				}
			}
		}
	}
	$giftcerts = func_query("SELECT * $gc_add_date FROM $sql_tbl[giftcerts] WHERE orderid = '$orderid'");
	if (!empty($giftcerts) && $config["General"]["use_counties"] == "Y") {
		foreach ($giftcerts as $k => $v) {
			if (!empty($v['recipient_county']))
				$giftcerts[$k]['recipient_countyname'] = func_get_county($v['recipient_county']);
		}
	}
	$order = func_select_order($orderid);
	if (!$order)
		return false;

	$order['is_returns'] = $is_returns;
	if ($current_area == "A" || ($current_area == "P" && !empty($active_modules['Simple_Mode']))) {
		if (strpos($order['details'], "{CardNumber}:") !== false && file_exists($xcart_dir."/payment/cmpi.php"))
			$order['is_cc_payment'] = "Y";
	}

	if (func_query_first_cell("SELECT COUNT(*) FROM $sql_tbl[order_details], $sql_tbl[download_keys] WHERE $sql_tbl[order_details].orderid = '$orderid' AND $sql_tbl[order_details].itemid = $sql_tbl[download_keys].itemid")) {
		$order['is_egood'] = 'Y';
	}
	elseif (func_query_first_cell("SELECT COUNT(*) FROM $sql_tbl[order_details], $sql_tbl[products] WHERE $sql_tbl[order_details].orderid = '$orderid' AND $sql_tbl[order_details].productid = $sql_tbl[products].productid AND $sql_tbl[products].distribution != ''")) {
		$order['is_egood'] = 'E';
	}

	$userinfo = func_query_first("SELECT *, date+'".$config["Appearance"]["timezone_offset"]."' as date FROM $sql_tbl[orders] WHERE orderid = '$orderid'");
	if (isset($order["extra"]['additional_fields'])) {
		$userinfo['additional_fields'] = $order["extra"]['additional_fields'];
	}

	$userinfo['titleid'] = func_detect_title($userinfo['title']);
	$userinfo['b_titleid'] = func_detect_title($userinfo['b_title']);
	$userinfo['s_titleid'] = func_detect_title($userinfo['s_title']);
	if ($current_area == 'C') {
		$userinfo['title'] = func_get_title($userinfo['titleid']);
		$userinfo['b_title'] = func_get_title($userinfo['b_titleid']);
		$userinfo['s_title'] = func_get_title($userinfo['s_titleid']);
	}

	$userinfo = func_array_merge(func_userinfo($userinfo["login"], "C", false, false, array("C","H")), $userinfo);

	list($userinfo["b_address"], $userinfo["b_address_2"]) = split("[\n\r]+", $userinfo["b_address"]);
	list($userinfo["s_address"], $userinfo["s_address_2"]) = split("[\n\r]+", $userinfo["s_address"]);

	$userinfo["s_countryname"] = $userinfo["s_country_text"] = func_get_country($userinfo["s_country"]);
	$userinfo["s_statename"] = $userinfo["s_state_text"] = func_get_state($userinfo["s_state"], $userinfo["s_country"]);
	$userinfo["b_statename"] = func_get_state($userinfo["b_state"], $userinfo["b_country"]);
	$userinfo["b_countryname"] = func_get_country($userinfo["b_country"]);
	if ($config["General"]["use_counties"] == "Y") {
		$userinfo["b_countyname"] = func_get_county($userinfo["b_county"]);
		$userinfo["s_countyname"] = func_get_county($userinfo["s_county"]);
	}

	if (!$products)
		$products = array ();

	if (preg_match("/(free_ship|percent|absolute)(?:``)(.+)/S", $order["coupon"], $found)) {
		$order["coupon"] = $found[2];
		$order["coupon_type"] = $found[1];
	}

	//$order["extra"]["tax_info"]["product_tax_name"] = "";
	$_product_taxes = array();

	foreach ($products as $k=>$v) {
		if (!empty($active_modules['Extra_Fields']) && $v['is_deleted'] != 'Y') {
			$products[$k]['extra_fields'] = func_query("SELECT $sql_tbl[extra_fields].*, $sql_tbl[extra_field_values].*, IF($sql_tbl[extra_fields_lng].field != '', $sql_tbl[extra_fields_lng].field, $sql_tbl[extra_fields].field) as field FROM $sql_tbl[extra_field_values], $sql_tbl[extra_fields] LEFT JOIN $sql_tbl[extra_fields_lng] ON $sql_tbl[extra_fields].fieldid = $sql_tbl[extra_fields_lng].fieldid AND $sql_tbl[extra_fields_lng].code = '$shop_language' WHERE $sql_tbl[extra_fields].fieldid = $sql_tbl[extra_field_values].fieldid AND $sql_tbl[extra_field_values].productid = '$v[productid]' AND $sql_tbl[extra_fields].active = 'Y' ORDER BY $sql_tbl[extra_fields].orderby");
		}

		$v['product_options_txt'] = $v['product_options'];
		if ($v["extra_data"]) {
			$v["extra_data"] = unserialize($v["extra_data"]);
			if (is_array(@$v["extra_data"]["display"])) {
				foreach ($v["extra_data"]["display"] as $i=>$j) {
					$v["display_".$i] = $j;
				}
			}
			if (is_array($v["extra_data"]["taxes"])) {
				foreach ($v["extra_data"]["taxes"] as $i=>$j) {
					if ($j["tax_value"] > 0)
						$_product_taxes[$i] = $j["tax_display_name"];
				}
			}
		}

		$v["original_price"] = $v["ordered_price"] = $v["price"];
		$v["price_deducted_tax"] = "Y";

		#
		# Get the original price (current price in the database)
		#
		if ($v['is_deleted'] != 'Y') {
			$v["original_price"] = func_query_first_cell("SELECT MIN($sql_tbl[pricing].price) FROM $sql_tbl[pricing] WHERE $sql_tbl[pricing].productid = '$v[productid]' AND $sql_tbl[pricing].membershipid IN ('$userinfo[membershipid]', 0) AND $sql_tbl[pricing].quantity <= '$v[amount]' AND $sql_tbl[pricing].variantid = 0");
			if (!empty($active_modules['Product_Options']) && $v['extra_data']['product_options']) {
				list($variant, $product_options) = func_get_product_options_data($v['productid'], $v['extra_data']['product_options'],$userinfo['membershipid']);

				if ($product_options === false) {
					unset($product_options);
				}
				else {
					if (empty($variant['price']))
						$variant['price'] = $v["original_price"];

					$v["original_price"] = $variant['price'];
					unset($variant['price']);
					if ($product_options) {
						foreach ($product_options as $o) {
							if ($o['modifier_type'] == '%')
								$v["original_price"] += $v["original_price"]*$o['price_modifier']/100;
							else
								$v["original_price"] += $o['price_modifier'];
						}
					}

					$v['product_options'] = $product_options;

					# Check current and saved product options set
					if (!empty($v['product_options_txt'])) {
						$flag_txt = true;

						# Check saved product options
						$count = 0;
						foreach ($v['product_options'] as $opt) {
							if (preg_match("/".preg_quote($opt['class'],"/").": ".preg_quote($opt['option_name'], "/")."/Sm", $v['product_options_txt']))
								$count++;
						}
						if ($count != count($v['product_options']))
							$flag_txt = false;

						# Check current product options set
						if ($flag_txt) {
							$count = 0;
							$tmp = explode("\n", $v['product_options_txt']);
							foreach ($tmp as $txt_row) {
								if (!preg_match("/^([^:]+): (.*)$/S", trim($txt_row), $match))
									continue;

								foreach ($v['product_options'] as $opt) {
									if ($match[1] == $opt['class'] && $match[2] == trim($opt['option_name'])) {
										$count++;
										break;
									}
								}
							}

							if ($count != count($tmp))
								$flag_txt = false;
						}

						# Force display saved product options set
						# if saved and current product options sets wasn't equal
						if(!$flag_txt)
							$v['force_product_options_txt'] = true;
					}

					if (!empty($variant)) {
						$v = func_array_merge($v, $variant);
					}
				}
			}
		}

		$products[$k] = $v;

	}

	if (count($_product_taxes) == 1) {
		$order["extra"]["tax_info"]["product_tax_name"] = array_pop($_product_taxes);
	}

	if ($order["coupon_type"] == "free_ship") {
		$order["shipping_cost"] = $order["coupon_discount"];
		$order["discounted_subtotal"] += $order["coupon_discount"];
	}


	
	return array(
		"order" => $order,
		"products" => $products,
		"userinfo" => $userinfo,
		"giftcerts" => $giftcerts);
}

#
# This function increments product rating
#
function func_increment_rating($productid) {
	global $sql_tbl;

	db_query("UPDATE $sql_tbl[products] SET rating=rating+1 WHERE productid='$productid'");
}

#
# Decrease number of products in stock and increase product rating
#
function func_decrease_quantity($products) {
	if (!empty($products) && is_array($products)) {
		foreach ($products as $product) {
			func_increment_rating($product["productid"]);
		}
	}

	func_update_quantity($products, false);
}

#
# This function creates order entry in orders table
#
function func_place_order($payment_method, $order_status, $order_details, $customer_notes, $extra = array(), $extras = array()) {
	global $cart, $userinfo, $discount_coupon, $mail_smarty, $config, $active_modules, $single_mode, $partner, $adv_campaignid, $partner_clickid;
	global $sql_tbl, $to_customer;
	global $wlid, $HTTP_COOKIE_VARS;
	global $xcart_dir, $REMOTE_ADDR, $PROXY_IP, $CLIENT_IP, $add_to_cart_time;
	global $arb_account_used, $arb_account;

	$mintime = 10;
	#
	# Lock place order process
	#
	$LOCK = func_lock("place_order");

	$userinfo['title'] = func_get_title($userinfo['titleid'], $config['default_admin_language']);
	$userinfo['b_title'] = func_get_title($userinfo['b_titleid'], $config['default_admin_language']);
	$userinfo['s_title'] = func_get_title($userinfo['s_titleid'], $config['default_admin_language']);

	$check_order = func_query_first("SELECT orderid FROM $sql_tbl[orders] WHERE login='".addslashes($userinfo["login"])."' AND '".time()."'-date<'$mintime'");
	if ($check_order) {
		func_unlock($LOCK);
		return false;
	}

	if (($order_status != "I") && ($order_status != "Q")) {
		func_unlock($LOCK);
		return false;
	}

	$userinfo["email"] = addslashes($userinfo["email"]);

	$orderids = array ();

	#
	# REMOTE_ADDR and PROXY_IP
	#
	$extras['ip'] = $CLIENT_IP;
	$extras['proxy_ip'] = $PROXY_IP;
	if (!empty($cart['shipping_warning'])) {
		$extras['shipping_warning'] = $cart['shipping_warning'];
	}

	if ($add_to_cart_time > 0) {
		$extras['add_to_cart_time'] = time() - $add_to_cart_time;
	}

	if (!empty($HTTP_COOKIE_VARS['personal_client_id'])) {
		$extras['personal_client_id'] = $HTTP_COOKIE_VARS['personal_client_id'];
	}

	#
	# Validate cart contents
	#
	if (!func_cart_is_valid($cart, $userinfo)) {
		# current state of cart is not valid and we cannot
		# re-calculate it now
		func_unlock($LOCK);
		return false;
	}

	$products = $cart['products'];
	func_decrease_quantity($products);

	$giftcert_discount = $cart["giftcert_discount"];
	if ($cart["applied_giftcerts"]) {
		foreach ($cart["applied_giftcerts"] as $k=>$v) {
			$giftcert_str = join("*", array(@$giftcert_str, "$v[giftcert_id]:$v[giftcert_cost]"));
			db_query("UPDATE $sql_tbl[giftcerts] SET status='U' WHERE gcid='$v[giftcert_id]'");
		}
	}

	$giftcert_id = @$cart["giftcert_id"];

	$extra = "";
	if (!empty($active_modules["Anti_Fraud"]) && defined("IS_AF_CHECK") && ($cart['total_cost'] > 0 || $config['Anti_Fraud']['check_zero_order'] == 'Y')) {
		include $xcart_dir."/modules/Anti_Fraud/anti_fraud.php";
	}

	#
	# Store Airborne account information into $order_details
	#
	x_session_register("arb_account_used");
	x_session_register("arb_account");
	if ($arb_account_used) {
		$_code = func_query_first_cell("SELECT code FROM $sql_tbl[shipping] WHERE shippingid='$cart[shippingid]'");
		if ($_code == "ARB")
			$order_details = func_get_langvar_by_name("lbl_arb_account").": ".$arb_account."\n".$order_details;
	}
	$extra['additional_fields'] = $userinfo['additional_fields'];

	foreach ($cart["orders"] as $current_order) {
		$_extra = $extra;
		$_extra["tax_info"] = array (
			"display_taxed_order_totals" => $config["Taxes"]["display_taxed_order_totals"],
			"display_cart_products_tax_rates" => $config["Taxes"]["display_cart_products_tax_rates"] == "Y",
			"taxed_subtotal" => $current_order["display_subtotal"],
			"taxed_discounted_subtotal" => $current_order["display_discounted_subtotal"],
			"taxed_shipping" => $current_order["display_shipping_cost"]);

		if (!empty($active_modules["Special_Offers"]))
			include $xcart_dir."/modules/Special_Offers/place_order_extra.php";

		if (!$single_mode) {
			$giftcert_discount = $current_order["giftcert_discount"];
			$giftcert_str = "";
			if ($current_order["applied_giftcerts"]) {
				foreach($current_order["applied_giftcerts"] as $k=>$v)
					$giftcert_str = join("*", array($giftcert_str, "$v[giftcert_id]:$v[giftcert_cost]"));
			}
		}

		$taxes_applied = addslashes(serialize($current_order["taxes"]));

		$discount_coupon = $current_order["coupon"];
		if (!empty($current_order["coupon"])) {
			$current_order["coupon"] = func_query_first_cell("SELECT coupon_type FROM $sql_tbl[discount_coupons] WHERE coupon='".addslashes($current_order["coupon"])."'")."``".$current_order["coupon"];
		}

		$save_info = $userinfo;
		$userinfo["b_address"] .= "\n".$userinfo["b_address_2"];
		$userinfo["s_address"] .= "\n".$userinfo["s_address_2"];
                if (!empty($cart['shipping_method'])) {
                    $shipping_method = $cart['shipping_method'];
                    if ($shipping_method === "EXPRESS") {
                        $shipping_method = "XPRESS";
                    }
                } else {
                    $shipping_method = "NORMAL";
                }
		#
		# Insert into orders
		#
		$insert_data = array (
			'login' => addslashes($userinfo['login']),
			'membershipid' => $userinfo['membershipid'],
			'membership' => addslashes($userinfo['membership']),
			'total' => $current_order['total_cost'],
			'giftcert_discount' => $giftcert_discount,
			'giftcert_ids' => @$giftcert_str,
			'subtotal' => $current_order['subtotal'],
			'shipping_cost' => $current_order['shipping_cost'],
			'shippingid' => $cart['shippingid'],
			'tax' => $current_order['tax_cost'],
			'taxes_applied' => $taxes_applied,
			'discount' => $current_order['discount'],
			'coupon' => addslashes(@$current_order['coupon']),
			'coupon_discount' => $current_order['coupon_discount'],
			'date' => time(),
			'status' => $order_status,
			'payment_method' => addslashes($payment_method),
			'paymentid' => $cart['paymentid'],
			'flag' => 'N',
			'details' => addslashes(text_crypt($order_details)),
			'customer_notes' => $customer_notes,
			'clickid' => $partner_clickid,
                        'shipping_method' => $shipping_method,
			'extra' => addslashes(serialize($_extra)));

		# copy userinfo
		$_fields = array ('title','firstname','lastname','phone','fax','email','url','company','tax_number','tax_exempt');
		foreach ($_fields as $k) {
			if (!isset($userinfo[$k]))
				continue;

			$insert_data[$k] = addslashes($userinfo[$k]);
		}

		$_fields = array ('title','firstname','lastname','address','city','county','state','country','zipcode');
		foreach (array('b_','s_') as $p) {
			foreach ($_fields as $k) {
				$f = $p.$k;
				if (isset($userinfo[$f])) {
					$insert_data[$f] = addslashes($userinfo[$f]);
				}
			}
		}

		$orderid = func_array2insert('orders', $insert_data);
		unset($insert_data);

		if (!empty($extras) && is_array($extras)) {
			foreach ($extras as $k => $v) {
				if (strlen($v) > 0)
					db_query("INSERT INTO $sql_tbl[order_extras] (orderid, khash, value) VALUES ('$orderid', '".addslashes($k)."', '".addslashes($v)."')");
			}
		}

		$userinfo = $save_info;

		$orderids[] = $orderid;
		$order=func_select_order($orderid);

		#
		# Insert into order details
		#
		if (!empty($products) && is_array($products)) {

		foreach ($products as $pk => $product) {
			if (($single_mode) || ($product["provider"] == $current_order["provider"])) {
				$product["price"] = price_format($product["price"]);
				$product["extra_data"]["product_options"] = $product["options"];
				$product["extra_data"]["taxes"] = $product["taxes"];
				$product["extra_data"]["display"]["price"] = price_format($product["display_price"]);
				$product["extra_data"]["display"]["discounted_price"] = price_format($product["display_discounted_price"]);
				$product["extra_data"]["display"]["subtotal"] = price_format($product["display_subtotal"]);
				if (empty($product["product_orig"]))
					$product["product_orig"] = $product["product"];

				if(!empty($active_modules['Product_Options']))
					$product["product_options"] = func_serialize_options($product["options"]);

				$insert_data = array (
					'orderid' => $orderid,
					'productid' => $product['productid'],
					'product' => addslashes($product['product_orig']),
					'product_options' => addslashes($product['product_options']),
					'amount' => $product['amount'],
					'price' => $product['price'],
					'provider' => addslashes($product["provider"]),
					'extra_data' => addslashes(serialize($product["extra_data"])),
					'productcode' => addslashes($product['productcode']));


				$products[$pk]['itemid'] = func_array2insert('order_details', $insert_data);
				unset($insert_data);

				#
				# Insert into subscription_customers table (for subscription products)
				#
				if (!empty($active_modules["Subscriptions"]))
					include $xcart_dir."/modules/Subscriptions/subscriptions_cust.php";

				#
				# Check if this product is in Wish list
				#
				if (!empty($active_modules["Wishlist"]))
					include $xcart_dir."/modules/Wishlist/place_order.php";

				if (!empty($active_modules["Recommended_Products"])) {
					$rec_counter = func_query_first_cell("SELECT COUNT(*) FROM $sql_tbl[stats_customers_products] WHERE productid='$product[productid]' AND login='".addslashes($userinfo["login"])."'");

					if (!empty($rec_counter)) {
						db_query("UPDATE $sql_tbl[stats_customers_products] SET counter='".($rec_counter+1)."' WHERE productid='$product[productid]' AND login='".addslashes($userinfo["login"])."'");
					}
					else {
						db_query("INSERT INTO $sql_tbl[stats_customers_products] (productid, login, counter) VALUES ('$product[productid]', '".addslashes($userinfo["login"])."', '1')");
					}
				}
			}
		}

		}

		if (!empty($active_modules['XAffiliate'])) {
			#
			# Partner commission
			#
			if (!empty($partner))
				include $xcart_dir."/include/partner_commission.php";

			#
			# Save link order -> advertising campaign
			#
			if ($adv_campaignid)
				include $xcart_dir."/include/adv_campaign_commission.php";
		}

		if ((($single_mode) || (empty($current_order["provider"]))) && (!empty($cart["giftcerts"]))) {
			foreach($cart["giftcerts"] as $gk => $giftcert) {
				$gcid = substr(strtoupper(md5(uniqid(rand()))), 0, 16);

				#
				# status == Pending!
				#
				$insert_data = array (
					'gcid' => $gcid,
					'orderid' => $orderid,
					'purchaser' => addslashes($giftcert['purchaser']),
					'recipient' => addslashes($giftcert['recipient']),
					'send_via' => $giftcert['send_via'],
					'recipient_email' => @$giftcert['recipient_email'],
					'recipient_firstname' => addslashes(@$giftcert['recipient_firstname']),
					'recipient_lastname' => addslashes(@$giftcert['recipient_lastname']),
					'recipient_address' => addslashes(@$giftcert['recipient_address']),
					'recipient_city' => addslashes(@$giftcert['recipient_city']),
					'recipient_county' => @$giftcert['recipient_county'],
					'recipient_state' => @$giftcert['recipient_state'],
					'recipient_country' => @$giftcert['recipient_country'],
					'recipient_zipcode' => @$giftcert['recipient_zipcode'],
					'recipient_phone' => @$giftcert['recipient_phone'],
					'message' => addslashes($giftcert['message']),
					'amount' => $giftcert['amount'],
					'debit' => $giftcert['amount'],
					'status' => 'P',
					'add_date' => time());

				if ($giftcert['send_via'] == 'P') {
					$insert_data['tpl_file'] = $giftcert['tpl_file'];
				}

				func_array2insert('giftcerts', $insert_data);
				unset($insert_data);

				$cart["giftcerts"][$gk]['gcid'] = $gcid;

				#
				# Check if this giftcertificate is in Wish list
				#
				if (!empty($active_modules["Wishlist"])) {
					include $xcart_dir."/modules/Wishlist/place_order.php";
				}
			}
		}

		#
		# Mark discount coupons used
		#
		if ($discount_coupon) {
			$_per_user = func_query_first_cell("SELECT per_user FROM $sql_tbl[discount_coupons] WHERE coupon='$discount_coupon' LIMIT 1");
			if ($_per_user == "Y") {
				$_need_to_update = func_query_first_cell("SELECT times_used FROM $sql_tbl[discount_coupons_login] WHERE coupon='$discount_coupon' AND login='".addslashes($userinfo["login"])."' LIMIT 1");
				if ($_need_to_update)
					db_query("UPDATE $sql_tbl[discount_coupons_login] SET times_used=times_used+1 WHERE coupon='$discount_coupon' AND login='".addslashes($userinfo["login"])."'");
				else
					db_query("INSERT INTO $sql_tbl[discount_coupons_login] (coupon, login, times_used) VALUES ('$discount_coupon', '".addslashes($userinfo["login"])."', '1')");
			}
			else {
				db_query("UPDATE $sql_tbl[discount_coupons] SET times_used=times_used+1 WHERE coupon='$discount_coupon'");
				db_query("UPDATE $sql_tbl[discount_coupons] SET status='U' WHERE coupon='$discount_coupon' AND times_used=times");
			}
			$discount_coupon="";
		}

		#
		# Mail template processing
		#
		$admin_notify = (($order_status == "Q" && $config["Email_Note"]["enable_order_notif"] == "Y") || ($order_status == "I" && $config["Email_Note"]["enable_init_order_notif"] == "Y"));
		$customer_notify = (($order_status == "Q") || ($order_status == "I" && $config["Email_Note"]["enable_init_order_notif_customer"] == "Y"));

		$order_data = func_order_data($orderid);
		$mail_smarty->assign("products",$order_data["products"]);
		$mail_smarty->assign("giftcerts",$order_data["giftcerts"]);
		$mail_smarty->assign("order",$order_data["order"]);
		$mail_smarty->assign("userinfo",$order_data["userinfo"]);

		$prefix = ($order_status=="I"?"init_":"");

		if ($customer_notify) {
			#
			# Notify customer by email
			#
			$to_customer = ($userinfo['language']?$userinfo['language']:$config['default_customer_language']);
			$mail_smarty->assign("products", func_translate_products($order_data["products"], $to_customer));

			//func_send_mail($userinfo["email"], "mail/".$prefix."order_customer_subj.tpl", "mail/".$prefix."order_customer.tpl", $config["Company"]["orders_department"], false);
		}

		if (!empty($order_data["order"]['payment_method_orig'])) {
			$order_data["order"]['payment_method'] = $order_data["order"]['payment_method_orig'];
			$mail_smarty->assign("order",$order_data["order"]);
		}

		$mail_smarty->assign("products",$order_data["products"]);
		if ($admin_notify) {
			#
			# Notify orders department by email
			#
			$mail_smarty->assign("show_order_details", "Y");
			//func_send_mail($config["Company"]["orders_department"], "mail/".$prefix."order_notification_subj.tpl", "mail/order_notification_admin.tpl", $userinfo["email"], true, true);
			$mail_smarty->assign("show_order_details", "");

			#
			# Notify provider (or providers) by email
			#
			if ((!$single_mode) && ($current_order["provider"]) && $config["Email_Note"]["send_notifications_to_provider"] == "Y") {
				$pr_result = func_query_first ("SELECT email, language FROM $sql_tbl[customers] WHERE login='$current_order[provider]'");
				$prov_email = $pr_result ["email"];
				if ($prov_email != $config["Company"]["orders_department"]) {
					$to_customer = $pr_result['language'];
					if (empty($to_customer))
						$to_customer = $config['default_admin_language'];

					//func_send_mail($prov_email, "mail/".$prefix."order_notification_subj.tpl", "mail/order_notification.tpl", $userinfo["email"], false);
				}
			}
			elseif ($config["Email_Note"]["send_notifications_to_provider"] == "Y" && !empty($products) && is_array($products)) {
				$providers = array();
				foreach($products as $product) {
					$pr_result = func_query_first("select email, language from $sql_tbl[customers] where login='$product[provider]'");
					if ($pr_result["email"])
						$providers[$product['provider']] = $pr_result;
				}

				if ($providers) {
					foreach ($providers as $prov_data) {
						if ($prov_data['email'] == $config["Company"]["orders_department"])
							continue;

						$to_customer = $prov_data['language'];
						if (empty($to_customer))
							$to_customer = $config['default_admin_language'];

						//func_send_mail($prov_data['email'], "mail/".$prefix."order_notification_subj.tpl", "mail/order_notification.tpl", $userinfo["email"], false);
					}
				}
			}
		}

		if (!empty($active_modules['Surveys']) && defined("AREA_TYPE") && constant("AREA_TYPE") == 'C') {
			func_check_surveys_events("OPL", $order_data);
		}

	}

	#
	# Send notifications to orders department and providers when product amount in stock is low
	#
	if ($config["General"]["unlimited_products"]!="Y") {
		foreach($order_data["products"] as $product) {
			if ($product['product_type'] == 'C' && !empty($active_modules['Product_Configurator']))
				continue;

			if ($active_modules['Product_Options'] && $product['extra_data']['product_options']) {
				$avail_now = func_get_options_amount($product['extra_data']['product_options'], $product['productid']);
			}
			else {
				$avail_now = func_query_first_cell("SELECT avail FROM $sql_tbl[products] WHERE productid='".$product["productid"]."'");
			}

			if ($product['low_avail_limit'] >= $avail_now && $config['Email_Note']['eml_lowlimit_warning'] == 'Y') {
				#
				# Mail template processing
				#
				$product['avail'] = $avail_now;
				$mail_smarty->assign("product", $product);

				//func_send_mail($config["Company"]["orders_department"], "mail/lowlimit_warning_notification_subj.tpl", "mail/lowlimit_warning_notification_admin.tpl", $config["Company"]["orders_department"], true);

				$pr_result = func_query_first ("SELECT email, language FROM $sql_tbl[customers] WHERE login='".$product["provider"]."'");
				if ((!$single_mode) && ($pr_result["email"]!=$config["Company"]["orders_department"]) && $config['Email_Note']['eml_lowlimit_warning_provider'] == 'Y') {
					$to_customer = $pr_result['language'];
					if (empty($to_customer))
						$to_customer = $config['default_admin_language'];

					//func_send_mail($pr_result["email"], "mail/lowlimit_warning_notification_subj.tpl", "mail/lowlimit_warning_notification_admin.tpl", $config["Company"]["orders_department"], false);
				}
			}
		}
	}

	#
	# Release previously created lock
	#
	func_unlock($LOCK);

	return $orderids;
}
#
#This function returns the status for a particlular order
#
function func_get_order_status($orderid){
	return func_query_first_cell("select status from xcart_orders where orderid='$orderid'");
}

#
#This function returns the status of all orders under it's group order
#
function func_get_group_orders_status($orderid){
    return func_query("select parent.status from xcart_orders as parent join xcart_orders as child on parent.group_id=child.group_id where child.orderid='$orderid'");
}

#
#This function returns the status for a particlular order
#
function func_get_order_affiliate($orderid){
	$changerequest_query="select mka.company_name,xo.orderid from mk_affiliates  mka
	INNER JOIN xcart_orders xo  ON  xo.affiliateid = mka.affiliate_id where xo.orderid='$orderid' ";
	$changerequest_result=db_query($changerequest_query);
	$changerequest_row=db_fetch_row($changerequest_result);
	$changerequest_status=$changerequest_row[0];
	return $changerequest_status;
}

#
#This function returns the status for a particlular order
#
function func_update_dates_orders($orderid, $new_status, $orderdata=false, $statusChangeTime=null){
	$time = time();
	
	if($statusChangeTime != null){
		$time = $statusChangeTime;
	}
	
	global $sqllog;
	
	$update_data = array();
	
	if($new_status=='Q'){
		$update_data['queueddate'] = $time;
	} elseif($new_status=='C' || $new_status == 'PC' ){
		$update_data['completeddate'] = $time;
		if($orderdata['delivereddate'] == '')
			$update_data['delivereddate'] = $time;
		if($orderdata['payment_method'] == 'cod') {
			$update_data['cod_payment_date'] = $time;
		}
	} elseif($new_status =='SH') {  
		$update_data['shippeddate'] = $time;
	} elseif($new_status =='DL') {
		$update_data['delivereddate'] = $time;
	} elseif ($new_status == 'F') {
		$update_data['canceltime'] = $time;
	} else if($new_status == 'PK'){
		$update_data['packeddate'] = $time;
	}
	
	func_array2update('orders', $update_data, "orderid = $orderid");
}
#
#This function returns the changerequeststatus for a particlular order
#
function func_get_order_changereqstatus($orderid){
	$changerequest_query="select distinct changerequest_status from xcart_orders where orderid='$orderid'";
	$changerequest_result=db_query($changerequest_query);
	$changerequest_row=db_fetch_row($changerequest_result);
	$changerequest_status=$changerequest_row[0][0];
	return $changerequest_status;
	
}
#This function returns the chaqueDetails for a particlular order
#
function func_get_order_chequedetails($orderid){
$commentQuery="select cheque_no as chequeno,cheque_date as chequedate,cheque_bounce as bounce from xcart_orders where orderid='$orderid'";
$commentresult=db_query($commentQuery);
$comments=db_fetch_row($commentresult);

return $comments;
	
}
#
#This function returns the changerequeststatus for a particlular order
#
function func_getorder_requests_array($orderid){
	$changerequest_query="select commenttype,commentdate as date,SUBSTRING(description,1,50) as desc_part from `mk_ordercommentslog` where orderid='$orderid' and commenttype='Change Request' order by commentdate desc";
	$commentresult=db_query($changerequest_query);
	$comments=array();
	if ($commentresult)
		{
			$i=0;
	
			while ($row=db_fetch_array($commentresult))
			{
				$comments[$i] = $row;
				$i++;
			}
		}
	return $comments;
}
#
#This function returns the changerequeststatus for a printing orderProcessing sheet
#
function func_getorder_requests_sheet($orderid){
$changerequest_query="select commentaddedby as name,newaddress,giftwrapmessage,DATE_FORMAT(FROM_UNIXTIME(commentdate),'%d-%b-%Y') as date,description as desc_part,attachmentname from `mk_ordercommentslog`
where orderid='$orderid' and commenttype='Order Change' order by commentdate desc";
$commentresult=db_query($changerequest_query);
$comments=array();
if ($commentresult)
	{
		$i=0;

		while ($row=db_fetch_array($commentresult))
		{
			$comments[$i] = $row;
			$i++;
		}
	}
return $comments;
	
}

function func_getorder_QAlog_sheet($orderid){
$changerequest_query="select commentaddedby as name,commenttitle,DATE_FORMAT(FROM_UNIXTIME(commentdate),'%d-%b-%Y') as date,description as desc_part,attachmentname from `mk_ordercommentslog`
where orderid='$orderid' and commenttype='QaFailure' order by commentdate desc";
$commentresult=db_query($changerequest_query);
$comments=array();
if ($commentresult)
	{
		$i=0;

		while ($row=db_fetch_array($commentresult))
		{
			$comments[$i] = $row;
			$i++;
		}
	}
return $comments;

}

function func_getorder_Replacementlog_sheet($orderid){
$changerequest_query="select commentaddedby as name,commenttitle,DATE_FORMAT(FROM_UNIXTIME(commentdate),'%d-%b-%Y') as date,description as desc_part,attachmentname from `mk_ordercommentslog`
where orderid='$orderid' and commenttype='Replacement' order by commentdate desc";
$commentresult=db_query($changerequest_query);
$comments=array();
if ($commentresult)
	{
		$i=0;

		while ($row=db_fetch_array($commentresult))
		{
			$comments[$i] = $row;
			$i++;
		}
	}
return $comments;

}

/************************************************************************/
/*Code by: Lavanya	                                                */
/*Purpose: Get packing information                                 */
/************************************************************************/

function func_get_packing_info($orderid)
{
    global $sql_tbl,$weblog,$sqllog;

	$tb_prod_type  = $sql_tbl["mk_product_type"];
    $order_details  = $sql_tbl["order_details"];
	$shipping_prod_type  = $sql_tbl["shipping_prod_type"];

	$packingInfo=func_query("select name,id,consignment_capacity,sum(od.amount) as quantity,ceil(sum(od.amount)/consignment_capacity) as boxes
                                from $tb_prod_type pt,$order_details od,$shipping_prod_type spt
                                where pt.id=od.product_type and spt.product_type_id=od.product_type and od.orderid=$orderid group by id;");
	 return $packingInfo;
}

function func_get_box_count_for_order($orderid)
{
    
    global $sql_tbl,$weblog,$sqllog;

	$packingInfo=func_get_packing_info($orderid);
    $count=0;
    foreach($packingInfo as $rec){
        $count+=$rec[boxes];
    }
    return $count;
}



function func_get_ready_to_ship_orders($order_by="",$order_dir="",$operations_location ="",$source_type="",$couriers="",$countonly=false,$product_types,$payment_method="",$country_location='')
{
    global $sql_tbl;
    $sql="";


    if(!$countonly) {
         $sql = "SELECT od.itemid as itemid,od.amount as itemqty,pt.name as typename,ps.name as stylename,o.orderid as orderid,o.status as status, o.payment_method as paymentmethod, o.login as login,o.qtyInOrder as qtyInOrder,o.gift_status as gift_status,date_format(FROM_UNIXTIME(date,'%Y-%m-%d'), '%d/%m/%Y') as date,
		 	date_format(FROM_UNIXTIME(queueddate,'%Y-%m-%d'), '%d/%m/%Y') as queueddate, o.courier_service as courier,o.total as total,o.shipping_cost as shipping_cost, o.s_zipcode, o.s_country 
            FROM  $sql_tbl[orders]  o,$sql_tbl[order_details] od, mk_product_type pt,mk_product_style ps,mk_sources srcs
            where o.status='WP' and o.orderid=od.orderid and od.product_type=pt.id and od.product_style=ps.id and srcs.source_id=o.source_id and
            o.orderid not in (select od1.orderid from  $sql_tbl[order_details]  od1 where od1.orderid=o.orderid and od1.item_status!='QD' and od1.item_status!='IC')";
    } else { 
    	$sql="SELECT count(*) FROM  $sql_tbl[orders]  o,$sql_tbl[order_details] od,mk_sources srcs
            where o.status='WP' and o.orderid=od.orderid and srcs.source_id=o.source_id and
            o.orderid not in (select od1.orderid from  $sql_tbl[order_details]  od1 where od1.orderid=o.orderid and od1.item_status!='QD' and od1.item_status!='IC')";
    }
	if($operations_location != ""){
		$sql .= " and o.operations_location = '$operations_location' ";
	}
	if($source_type != ""){
		$sql .= " and srcs.source_type_id = $source_type ";
	}
    /*if($couriers!=""){
        $sql.="and o.courier_service in ('".implode("','",$couriers)."') ";

    }*/
	if($payment_method != "") {
	    $sql .= " and o.payment_method = 'cod' ";
    } else {
    	$sql .= " and o.payment_method != 'cod' " ;
    }
	/*if($product_types!="" && !$countonly){
		$filtered_orders_on_type = func_filter_order_by_type($operations_location,$source_types,$couriers,$product_types);
        $sql.="and o.orderid in ('".implode("','",$filtered_orders_on_type)."') ";
    }*/
    if($country_location!="" && $country_location=='IN'){
        $sql.=" and o.s_country='".$country_location."'";

    }
    else if($country_location!="" && $country_location=='OC'){
        $sql.=" and o.s_country !='IN' and o.s_country !=''";

    }
    if($order_by != "" ){
        $sql=$sql." order by $order_by";
        if($order_dir !="")
          $sql=$sql." $order_dir";
    }	else {
    	$sql=$sql." order by orderid asc";
    }
    
    global $sqllog;
    $sqllog->debug("SQL Query : ".$sql." "." @ line "._LINE_." in file ".__FILE__);
    if($countonly)return func_query_first_cell($sql, true);
    
    else return func_query($sql, true);    
}

function func_get_OpsDone_orders($order_by="",$order_dir="",$operations_location = "",$source_type="",$payment_method,$country_location)
{
    global $sql_tbl;
    $sql = "SELECT od.itemid as itemid,od.amount as itemqty,pt.name as typename,ps.name as stylename,o.orderid as orderid,o.status as status, o.payment_method as paymentmethod, o.login as login,o.qtyInOrder as qtyInOrder,o.gift_status as gift_status,date_format(FROM_UNIXTIME(date,'%Y-%m-%d'), '%d/%m/%Y') as date,
			date_format(FROM_UNIXTIME(queueddate,'%Y-%m-%d'), '%d/%m/%Y') as queueddate,o.courier_service as courier, o.total as total,o.shipping_cost as shipping_cost, o.s_zipcode, o.s_country 
            FROM  $sql_tbl[orders]  o,$sql_tbl[order_details] od, mk_product_type pt,mk_product_style ps,mk_sources srcs
            where o.status='WP' and o.orderid=od.orderid and od.product_type=pt.id and od.product_style=ps.id and o.source_id=srcs.source_id and 
            o.orderid not in (select od1.orderid from  $sql_tbl[order_details]  od1 where od1.orderid=o.orderid and od1.item_status!='OD' and od1.item_status!='QD' and od1.item_status!='IC')";
	
    if($operations_location != ""){
		$sql .= " and o.operations_location = '$operations_location' ";
	}
	if($source_type != ""){
		$sql .= " and srcs.source_type_id = $source_type ";
	}
	
	if($payment_method != "") {
	    $sql .= " and o.payment_method = 'cod' ";
    } else {
    	$sql .= " and o.payment_method != 'cod' " ;
    }
    
	if($country_location!="" && $country_location=='IN'){
        $sql.=" and o.s_country='".$country_location."'";

    }
    else if($country_location!="" && $country_location=='OC'){
        $sql.=" and o.s_country !='IN' and o.s_country !=''";

    }
    /*if($couriers!=""){
        $sql.="and o.courier_service in ('".implode("','",$couriers)."') ";

    }
	
	if($product_types!=""){
		$filtered_orders_on_type = func_filter_OpsDone_order_by_type($operations_location,$source_types,$couriers,$product_types);
        $sql.="and o.orderid in ('".implode("','",$filtered_orders_on_type)."') ";
    }*/
    if($order_by != "" ){
        $sql=$sql." order by $order_by";
        if($order_dir !="")
          $sql=$sql." $order_dir";
    }
    
    global $sqllog;
    $sqllog->debug("SQL Query : ".$sql." "." @ line "._LINE_." in file ".__FILE__);
    
    $orders=func_query($sql);
/*
    //For each other get item details as well.
    for($i=0;$i<count($orders);$i++) {
        $orderid=$orders[$i]['orderid'];
        $item_details_query="select itemid,amount as quantity,pt.name as type,ps.name as name from xcart_order_details od,mk_product_type pt,mk_product_style ps where od.orderid='$orderid' and od.product_type=pt.id and od.product_style=ps.id ";
        $orders[$i]['itemdetails']=func_query($item_details_query);
    }*/

    return $orders;
}

function func_filter_order_by_type($operations_location,$source_types,$couriers,$product_types){
	global $sql_tbl,$sqllog;	
	$sql = "SELECT distinct(o.orderid) as orderid
            FROM  $sql_tbl[orders]  o,$sql_tbl[order_details] od, mk_product_type pt,mk_product_style ps,mk_sources srcs
            where o.status='WP' and o.orderid=od.orderid and od.product_type=pt.id and od.product_style=ps.id and srcs.source_id=o.source_id and
            o.orderid not in (select od1.orderid from  $sql_tbl[order_details]  od1 where od1.orderid=o.orderid and IFNULL(od1.item_status,'N')!='QD')";
	if($operations_location != ""){
		$sql .= " and o.operations_location = '$operations_location' ";
	}
	if($source_types != ""){
		$sql .= " and srcs.source_type_id in ('".implode("','",$source_types)."') ";
	}
    if($couriers!=""){
        $sql.="and o.courier_service in ('".implode("','",$couriers)."') ";

    }
	if($product_types!=""){
		$sql.="and pt.id in ('".implode("','",$product_types)."') ";
	}
	$result = db_query($sql);
	while($row = db_fetch_array($result))
		$filtered_orders_on_type[] = $row['orderid'];

	$sqllog->debug("SQL Query : ".$sql." "." @ line ".__LINE__." in file ".__FILE__);
	return $filtered_orders_on_type; 
}


function func_filter_OpsDone_order_by_type($operations_location,$source_types,$couriers,$product_types){
	global $sql_tbl,$sqllog;	
	 $sql = "SELECT od.itemid as itemid,od.amount as itemqty,pt.name as typename,ps.name as stylename,o.orderid as orderid,o.status as status, o.login as login,o.qtyInOrder as qtyInOrder,o.gift_status as gift_status,date_format(FROM_UNIXTIME(date,'%Y-%m-%d'), '%d/%m/%Y') as date,
			date_format(FROM_UNIXTIME(queueddate,'%Y-%m-%d'), '%d/%m/%Y') as queueddate,o.courier_service as courier, o.total as total,o.shipping_cost as shipping_cost
            FROM  $sql_tbl[orders]  o,$sql_tbl[order_details] od, mk_product_type pt,mk_product_style ps,mk_sources srcs
            where o.status='WP' and o.orderid=od.orderid and od.product_type=pt.id and od.product_style=ps.id and o.source_id=srcs.source_id and 
            o.orderid not in (select od1.orderid from  $sql_tbl[order_details]  od1 where od1.orderid=o.orderid and IFNULL(od1.item_status,'N')!='OD' and IFNULL(od1.item_status,'N')!='QD')";
	if($operations_location != ""){
		$sql .= " and o.operations_location = '$operations_location' ";
	}
	if($source_types != ""){
		$sql .= " and srcs.source_type_id in ('".implode("','",$source_types)."') ";
	}
    if($couriers!=""){
        $sql.="and o.courier_service in ('".implode("','",$couriers)."') ";

    }
	if($product_types!=""){
		$sql.="and pt.id in ('".implode("','",$product_types)."') ";
	}
	$result = db_query($sql);
	while($row = db_fetch_array($result))
		$filtered_orders_on_type[] = $row['orderid'];

	$sqllog->debug("SQL Query : ".$sql." "." @ line ".__LINE__." in file ".__FILE__);
	return $filtered_orders_on_type; 
}

function func_filter_speedpost_serviceable_orders($orders) {
	$filtered_orders = array();
	foreach ($orders as $order) {
		// All zipcodes in India are serviceable by speed post - both general delivery and cod via VPP
		if($order['s_country'] == 'IN') {
			$filtered_orders[] = $order;
		}
	}
	return $filtered_orders;
}

function func_filter_orders_by_serviceability($orders, $courier, $payment_method) {
	$filtered_orders = array();
	
	if($payment_method == 'cod') {
		foreach ($orders as $order) {
			$serviceable = func_check_courier_cod_serviceability($order['s_zipcode'], $courier);
			if($serviceable) {
				$filtered_orders[] = $order;
			}
		}	
	} else {
		foreach ($orders as $order) {
			if($courier == 'DH') { 
				if($order['s_country'] != 'IN' && $order['s_country'] != 'RU') {
					// All countries except India and Russia are serviceable by DHL
					$filtered_orders[] = $order;
				} 
			} else {
				$serviceable = func_check_courier_prepaid_serviceability($order['s_zipcode'], $courier);
				if($serviceable) {
					$filtered_orders[] = $order;
				}
			}
		}
	}
	return $filtered_orders;
}

function func_generate_order_id($session_id,$text)
{
    global $sql_tbl;
    
    $tempOrderQuery = "INSERT INTO mk_temp_order(sessionid, parameter) VALUES('$session_id', '$text')";
    
    $r=db_query($tempOrderQuery);
    $neworderid="";
    if ($r) {
		$neworderid= db_insert_id();
	}
    return $neworderid;
    
}

/**
 * this function updates the shipping time of order
 * 
 * @param int $orderid
 */
function func_update_shipping_time($orderid) {
	$order_time_data = ordertime::get_order_time_data($orderid);
	
	print_r($order_time_data);
	
    $ship_together=0;
    foreach($order_time_data as $key=>$val){
		if($key == "SHIPPED_TOGETHER"){
			$ship_together = $val;//get shipping option(1=together or 0=not together)
		}
	}
	if($ship_together == 1){
		foreach($order_time_data as $key=>$val){
			if($key != "SHIPPED_TOGETHER") {
				/*
				##calculate order processing time
				##based on the day of ship at 7pm
				*/
				$processing_time = ordertime::get_process_time_7pm($val['PROCESSING_DATE']);
				$processing_time_sql = 'UPDATE xcart_order_details SET estimated_processing_date ='.$processing_time.' WHERE orderid =' . $orderid;
				db_query($processing_time_sql);
			}
        }
	} elseif($ship_together == 0){
		foreach($order_time_data as $key=>$val){
			if($key != "SHIPPED_TOGETHER"){
					foreach($val as $sval){
					/*
					##calculate order processing time
					##based on the day of ship at 7pm
					*/
					$processing_time = ordertime::get_process_time_7pm($sval['PRODUCT_PROCESSING_DATE']);//$Sval['processing_date'] = date on which order can be PROCESSED
					$processing_time_sql = 'UPDATE xcart_order_details SET estimated_processing_date ='.$processing_time . 
						' where itemid = '.$sval['PRODUCT_ITEMID'] . ' and orderid =' . $orderid;
					db_query($processing_time_sql);
				}
			}
		}
	}
}


//Get all the items in an order
function func_get_items_in_order($orderid)
{
    global $sql_tbl;

    $sql="select itemid,item_status from {$sql_tbl['order_details']} where orderid='$orderid'";
    $items=func_query($sql);
    return $items;
}

function func_change_item_status($itemid,$status,$old_status="unknown")
{
    global $sql_tbl,$weblog,$sqllog, $db_query_retries;
    $i=$db_query_retries;
    $result = false;
    $sql = "UPDATE $sql_tbl[order_details] SET item_status='$status' ";

    if($status == 'D'){
        $sql .= ", completion_time='$time' ";
    }elseif($status == 'S'){
        $sql .= ", sent_date='$time' ";
    }elseif($status == 'F' || $status == 'H'){
		$sql .= ", assignee='' ";
    }elseif($status == 'I'){
		$sql_orders = "UPDATE {$sql_tbl[orders]} SET status = 'I' where orderid in (select orderid from {$sql_tbl[order_details]} where itemid='{$itemid}')";
		do{
                    $result = db_query($sql_orders);
                }while(--$i>0 && $result===false);
	}
    elseif($status == 'H'){
        $orderid = func_query_first_cell("SELECT orderid FROM $sql_tbl[order_details] WHERE itemid='$itemid'");
        do{
            $result= db_query("UPDATE $sql_tbl[orders] SET status='OH' WHERE orderid='" . $orderid . "'");
        }while(--$i > 0 && $result===false);
    }

    $i=$db_query_retries;
    $result = false;
    
    $sql .= " WHERE itemid='$itemid'";
    do{
        $result = db_query($sql);
    }while(--$i>0 && $result===false);
    $weblog->info("item status change : item $itemid changed from $old_status to $status ");
    $sqllog->debug("sql update : ".$sql);

}

function func_cancel_order($orderids, $new_status, $user, $status_change_comment, $update_inv, $reason_type, $reason, $orderid2fullordercancel=false, $generateGoodWillCoupons=false, $trackprogress=false, $progressKey='', $orderid2couponDetails=FALSE, $isOhCancellation=false) 
{
	global $sql_tbl, $weblog, $xcart_dir;

	$xcache = new XCache();
	if (!is_array($orderids)) $orderids = array($orderids);
	
	$count = 0;
	$finalResponses = array();
	foreach ($orderids as $orderid) {
		$order_data = func_order_data($orderid);
		//update count of processed orders..
		$count++;
		if($trackprogress){
			$progressInfo = $xcache->fetch($progressKey."_progressinfo");
			$progressInfo['completed'] = $count;
            $xcache->store($progressKey."_progressinfo", $progressInfo, 86400);
        }
        
		$order = func_query_first("Select o.*, sum(od.difference_refund) as difference_refund from $sql_tbl[orders] o left outer join
				xcart_order_details od on od.orderid = o.orderid and od.item_status != 'IC' where o.orderid = $orderid  group by o.orderid");
		
		
		if (empty($order)) {
			$finalResponses[$orderid] = array('status'=>"FAILURE", 'reason' => "Order $orderid is Invalid");
			continue; 
		} 
		
		if($order['is_refunded'] == 1){
			$canc_reason_sql = "select * from cancellation_codes where cancel_type = '$reason_type' and cancel_reason = '$reason'";
			$canc_reason = func_query_first($canc_reason_sql);
			
			$sql = "UPDATE $sql_tbl[orders] set status='$new_status', canceltime=".time().", cancellation_code= '".$canc_reason['id'] ."'";
			$sql .= "where orderid = $orderid";
			db_query($sql);
			$finalResponses[$orderid] = array("status" => "SUCCESS", "message" => "status updated to Rejected, refund already issued");
			func_addComment_status($orderid, $new_status, $order['status'], "status updated to Rejected, refund already issued. $status_change_comment", $user);
            EventCreationManager::pushCompleteOrderEvent($orderid);
			continue;
		}
		
		// Order can be rejected or declined, from a states (Q|WP|OH|SH|L);
		if(!in_array($order['status'], array('Q','RFR','WP','OH','PP','F')) && 
			!( ($order['status']=='SH' || $order['status']=='DL' || $order['status']=='C' || $order['status']=='L') && ($reason != 'RTO' || $reason!= 'RTOCR'))
		){
			$finalResponses[$orderid] = array('status'=>"not_in_right_status", 'reason' => "Order $orderid cannot be cancelled from state ".$order['status']." with reason $reason");
			continue; 	
		}
		
		// If the order goes from PP to D/F, the lock on the coupon should be released.
		if ($order['status'] == 'PP' && !empty($order['coupon'])) {
		    // Decrement the coupon lock.
		    require_once $xcart_dir.'/modules/coupon/database/CouponAdapter.php';
		    $couponAdapter = CouponAdapter::getInstance();
		    $couponAdapter->unlockCouponForUser($order['coupon'], $order['login']);
		}
		
		$order_groupid = $order['group_id'];
		
        $cancel_full_order = $orderid2fullordercancel[$orderid] && $orderid2fullordercancel[$orderid] == true?true:false;
        $all_orders_in_group = func_query("Select o.*, sum(od.difference_refund) as difference_refund from $sql_tbl[orders] o, 
        		xcart_order_details od where o.group_id = $order_groupid and od.orderid = o.orderid and od.item_status != 'IC' group by o.orderid");
        if(count($all_orders_in_group) == 1) {
        	if( in_array($order['status'], array('Q','WP','OH','PP')) || 
			( ($order['status']=='SH' || $order['status']=='DL' || $order['status']=='C' || $order['status']=='L') && ($reason == 'RTO' || $reason== 'RTOCR'))
			) {
				$cancel_full_order = true;	
				$to_cancel_orderids = array($orderid);
				$to_cancel_orders = array($order);
				$orderid2Order[$orderid] = $order;
			}
        } else {
			if($cancel_full_order) {
				//add all the active orders to the list of orders to be cancelled
				$orderid2Order = $to_cancel_orderids = $to_cancel_orders = array(); 
				foreach($all_orders_in_group as $order_row) {
					//Full order cancellation can be done from Order details page or bulk cancellation
					//In both these cases, only sub orders in WP state should be marked cancelled.
					if( in_array($order_row['status'], array('Q','WP','OH','PP','RFR')) || 
					  ( ($order_row['status']=='SH' || $order_row['status']=='DL' || $order_row['status']=='C') && ($reason == 'RTO' || $reason== 'RTOCR'))
					){
						$to_cancel_orders[] = $order_row;
						$to_cancel_orderids[] = $order_row['orderid'];  
						$orderid2Order[$order_row['orderid']] = $order_row;
	        		}
	        	}
			} else {
				//add only the current order to list of orders to be cancelled..
				if( in_array($order['status'], array('Q','WP','OH','PP','RFR')) || 
				( ($order['status']=='SH' || $order['status']=='DL' || $order['status']=='C') && ($reason == 'RTO' || $reason== 'RTOCR'))
				) {
					$to_cancel_orderids = array($orderid);
					$to_cancel_orders = array($order);
					$orderid2Order[$orderid] = $order;
				}
			}		
		}
		
		if(!$to_cancel_orderids || count($to_cancel_orderids) == 0) {
			$finalResponses[$orderid] = array('status'=>"not_in_right_status", 'reason' => "Order $orderid cannot be cancelled from state ".$order['status']." with reason $reason");
			continue; 	
		}
		
		$activeOrderidInGroup = false;
		if(!$cancel_full_order) {
			//if we are not cancelling full order find other order in the group which is present
			foreach($all_orders_in_group as $order_row) {
				if($order_row['orderid']!=$orderid && ($order_row['status'] == 'RFR' || $order_row['status'] == 'WP' || $order_row['status'] == 'OH' || $order_row['status'] == 'PK' || $order_row['status'] == 'Q' || $order_row['status'] == 'SH' || $order_row['status'] == 'DL' || $order_row['status'] == 'C')){
					$activeOrderidInGroup = $order_row['orderid']; //we will be transferring our emi charges/anyother charge to this orderid
					break;
				}
			}
		}
		
		if($cancel_full_order) {
			$product_details = func_create_array_products_of_order($to_cancel_orderids);
		} else {
			$product_details = func_create_array_products_of_order($orderid);
		}
		
		$canCancelOrder = true;
		$orderid2WarehouseId = array();
		foreach($product_details as $product) {
			$skuIds[] = $product['sku_id'];
			$orderid2WarehouseId[$product['orderid']] = $product['warehouseid'];
		}
		if($reason=='OOSC') { 
			$response = true;
			if ($order['warehouseid'] > 0) {
				if($update_inv && !empty($skuIds))
					$response = checkSkuOutOfStock($skuIds, $order['warehouseid']);
			
				if($response === false) {
					$finalResponses[$orderid] = array('status'=>'FAILURE', 'reason' => "Not all items for order $orderid are OOS.<br>");
					continue;			
				} else if ($response !== true) {
					$finalResponses[$orderid] = array('status'=>'FAILURE', 'reason' =>"Unable to check OOS status for order - $orderid. Error - ".$response."<br>");
					continue;
				}
			}
		}
		
		$canc_reason_sql = "select * from cancellation_codes where cancel_type = '$reason_type' and cancel_reason = '$reason'";
		
		if($isOhCancellation && $isOhCancellation == true) {
			$canc_reason_sql .= " AND cancellation_mode = 'OH_CANCELLATION'";
		} else {
			if($cancel_full_order)
				$canc_reason_sql .= " AND cancellation_mode = 'FULL_ORDER_CANCELLATION'";
			else
				$canc_reason_sql .= " AND cancellation_mode = 'PART_ORDER_CANCELLATION'";
		}
		$canc_reason = func_query_first($canc_reason_sql);	
		
        $sql = "UPDATE $sql_tbl[orders] set status='$new_status', is_refunded=1, canceltime=".time().", cancellation_code= '".$canc_reason['id'] ."'";
        if($order['payment_method'] == 'cod')
	    	$sql .= ", cod_pay_status='cancelled' ";
		
	    if($activeOrderidInGroup) {
	    	//since there are active orders in group we will shift the emi charge to a active orderid.
			$sql .= ", total= total-$order[gift_charges]-$order[shipping_cost], payment_surcharge = 0.00, shipping_cost=0.00, gift_charges=0.00";
			$chargeSQL = "update $sql_tbl[orders] set payment_surcharge=payment_surcharge+$order[payment_surcharge], shipping_cost=shipping_cost+$order[shipping_cost], gift_charges=gift_charges+$order[gift_charges], total= total + $order[gift_charges] + $order[shipping_cost] where orderid='$activeOrderidInGroup'";
			db_query($chargeSQL);
		}
		
        $sql .= " where orderid in (".implode(",", $to_cancel_orderids).")";

        db_query($sql);

		// In case of PPS enriched order, the refund as well as customer notification will be taken care by OMS (via PPS)
        EventCreationManager::pushCompleteOrderEvent($orderid);
        
		if(FeatureGateKeyValuePairs::getBoolean("myntrashoppingfestleaderboard") && ($reason != 'OOSC')){
             include_once($xcart_dir."/include/class/shopfest/ShopFest.php");
			foreach($to_cancel_orderids as $to_cancel_orderid){
				$o1 = func_query_first("select * from xcart_orders where orderid = $to_cancel_orderid",true);
				$prod_det = func_create_array_products_of_order($to_cancel_orderid); 
		              ShopFest::updateLeaderBoardData(($o1['total']+$o1['cash_redeemed']),$o1['login'],$o1['date'],$prod_det,true);
		 	}
        }
	
	 	//cancel coupons - shopping fest - Raghav
		 if(FeatureGateKeyValuePairs::getBoolean("myntrashoppingfestCoupons"))
		 {
		 	foreach($to_cancel_orderid as $orderid)
		 	{
				 $query = "select coupon_code 
				           from myntra_fest_order_coupon_map as map  , 
				           xcart_orders as orders, cancellation_codes cc,xcart_discount_coupons coup  
				           where coup.coupon = coupon_code and coup.status != 'D' 
				                 and orders.cancellation_code = cc.id and cc.cancel_type = 'CCR' 
				                 and  orders.status = 'F' and map.order_id=orders.group_id and orders.order_id= '".$orderid."'";
							
				$coupons = func_query($query);
				
				foreach($coupons as $id=>$coupon ){
					db_query("UPDATE xcart_discount_coupons set status = 'D' where coupon ='".$coupon['coupon_code']."'");
				}
		 	}
		}


		$wmsOrders[] = array();
		$uniOrders[] = array();
        
		foreach($to_cancel_orderids as $oid) {
			func_addComment_status($oid, $new_status, $orderid2Order[$oid]["status"], $status_change_comment, $user);
			if($update_inv)
				increment_sku($oid, $orderid2Order[$oid]['warehouseid']);
		
			// Assuming that there are only only supply_type kind of items in an order
			$item_details_query = "select od.* from xcart_order_details od where od.orderid = $orderid";
			$item_details = func_query_first($item_details_query, true);
			$rejectReason = false; $rejectReasonDesc = false; $quality = false;
			if ($item_details['supply_type'] === 'JUST_IN_TIME') {
				$rejectReason = 'MARKET_PLACE'; $rejectReasonDesc = 'Market Place'; $quality = 'Q4';
			}

			if ($item_details['supply_type'] === 'JUST_IN_TIME') {
				if($reason != 'RTO' && $reason != 'RTOCR') {
					ItemApiClient::cancelOrderUpdateSkusItems($oid, $orderid2WarehouseId[$oid], $user, 'RETURN_FROM_OPS');
				}//else{
				//	ItemApiClient::cancelOrderUpdateSkusItems($oid, $orderid2WarehouseId[$oid], $user, 'CUSTOMER_RETURNED', $quality, $rejectReason, $rejectReasonDesc);
				//}
				array_push($uniOrders,$oid);
			} else{
				if($reason != 'RTO' && $reason != 'RTOCR') {
					array_push($wmsOrders,$oid);
				} else {
					ItemApiClient::cancelOrderUpdateSkusItems($oid, $orderid2WarehouseId[$oid], $user, 'CUSTOMER_RETURNED', $quality, $rejectReason, $rejectReasonDesc);
				}
			}
		}

		$orderToBePushed = array_merge($wmsOrders,$uniOrders);
		ReleaseApiClient::pushOrderReleaseToWMS($orderToBePushed, $user,'cancelrelease');


/*
			if($reason != 'RTO' && $reason != 'RTOCR') {
				ItemApiClient::cancelOrderUpdateSkusItems($oid, $orderid2WarehouseId[$oid], $user, 'RETURN_FROM_OPS');
			} else {
				ItemApiClient::cancelOrderUpdateSkusItems($orderid, $orderid2WarehouseId[$oid], $user, 'CUSTOMER_RETURNED', $quality, $rejectReason, $rejectReasonDesc);
			}
		}
		ReleaseApiClient::pushOrderReleaseToWMS($to_cancel_orderids, $user,'cancelrelease');
*/


		$issueCoupon = true;
		if($reason == 'INVDCOUP'){
			$issueCoupon = false;	
		}
		
		// MCFAIL: myntcash/cashback usage failed, thus cancelling order without issuing myntcash/cashback back to the customers account
		$refundUsedMyntCash = true;
		if($reason == 'MCFAIL'){
			$refundUsedMyntCash = false;
		}

		$ppsId = func_query_first("select value from order_additional_info where order_id_fk = $orderid and `key` = 'PAYMENT_PPS_ID'");
		if($new_status == 'F' && ($reason == 'RTOCR' || $reason == 'RTO') && ($ppsId['value'] != null && !empty($ppsId['value']))) {
			// As the return_args are never used in case of prequeue_rto (OrderActions), hence simply pass success.
			// Refund will be taken care by PPS, so no need to issue refunds.
			// Notification has already been sent by OMS hence continue.
			$finalResponses[$orderid] = array("status" => 'SUCCESS');
			continue;
		} else {
			$return_args = func_issue_refund($order, $new_status, $user, $to_cancel_orders, $activeOrderidInGroup, $reason, $cancel_full_order, $orderid2couponDetails, $generateGoodWillCoupons, $product_details, $issueCoupon, $refundUsedMyntCash);
		}
		
		//EventCreationManager::pushCompleteOrderEvent($orderid);
		
		$finalResponses[$orderid] = $return_args;
		
		// send cancellation email
		if($cancel_full_order) {
			if($reason==='OOSC' && $new_status == 'F') {
				$template = 'order_cancel_oos';
			} else {
				$template = 'order_cancel';
			}
		} else {
			$template = 'items_cancel';
		}
		$reason_email_contents = $canc_reason['email_content'];
		$reason_email_contents = str_replace("[ORDER_ID]", $order_groupid, $reason_email_contents);
		
		$subjectargs = array("ORDER_ID"	=> $order_groupid);
		$args = array();
		$args = $return_args['mail_args'];
		$args['REASON_TEXT'] = $reason_email_contents;
		
		if($cancel_full_order) {
			$amount_details = func_get_order_amount_details($to_cancel_orderids);
			$orderdetails = create_product_detail_table_new($product_details, $amount_details, 'email', "refund");
			$args['ORDER_DETAILS'] = $orderdetails;
		} else {
			$amount_details = func_get_order_amount_details($orderid);
			$orderdetails = create_product_detail_table_new($product_details, $amount_details, 'email', "refund");
			$args['ITEM_DETAILS'] = $orderdetails;
		}

		$callNumber = \WidgetKeyValuePairs::getWidgetValueForKey('customerSupportCall');
		$args['MYNTRA_CC_PHONE'] = $callNumber;
		 		
		$customKeywords = array_merge($subjectargs, $args);
		$mailDetails = array(
				"template" => $template,
				"to" => $order['login'],
				"bcc" => "myntramailarchive@gmail.com",
				"header" => 'header',
				"footer" => 'footer',
				"mail_type" => \MailType::CRITICAL_TXN
		);
		$multiPartymailer = new \MultiProviderMailer($mailDetails, $customKeywords);
		$flag = $multiPartymailer->sendMail();
		
	}
	return $finalResponses;
}

/*
 * This function is called while marking orders as cancelled or lost
 */
function func_issue_refund($order, $new_status, $user, $to_refund_orders, $activeOrderidInGroup, $reason, $cancel_full_order, $orderid2couponDetails = false, $generateGoodWillCoupons = false, $product_details, $refundCoupon = true, $refundMyntCash = true) {
    global $cashback_gateway_status;
    $http_location = HostConfig::$httpHost;
    global $sql_tbl, $weblog, $xcart_dir;

    $defaultCouponCount = WidgetKeyValuePairs::getWidgetValueForKey('cancelOrderCouponCount');
    $defaultCouponMrp = WidgetKeyValuePairs::getWidgetValueForKey('cancelOrderCouponMrp');
    $defaultCouponMinPurchase = WidgetKeyValuePairs::getWidgetValueForKey('cancelOrderCouponMinPurchase');
    $defaultCouponExpiry = WidgetKeyValuePairs::getWidgetValueForKey('cancelOrderCouponExpiryDays');

    $couponAdapter = CouponAdapter::getInstance();
    $comment = "";
    $refund_msg = "";
    $coupon_usage_text = "";
    $refund_amount = 0;
    $used_loyalty_points_refunded = 0.00;
    $awarded_loyalty_points_reverted = 0.00;
    $online = $order['payment_method'] != 'cod' ? true : false;
    $orderid = $order['orderid'];
    $order_groupid = $order['group_id'];

    $refund_to_cb_acc = $cashback_gateway_status == "on" ? true : false;
    if ($cashback_gateway_status == "internal") {
        $pos = strpos($order['login'], "@myntra.com");
        if ($pos === false)
            $refund_to_cb_acc = false;
        else
            $refund_to_cb_acc = true;
    }



    // dont issue any refund on order cancellation or order-lost if it is an exchange order. 
    // Just issue goodwill coupons when required.
    // TODO: If order type is exchange, refund loyalty points awarded to the previous order, 
    // and used loyalty points to previous order as well.
    $final_refund_amount = 0.00;
    $orderMyntCashDetails = array();
    if ($order['ordertype'] == 'ex'){
    	foreach ($to_refund_orders as $order_row) {
            if ($order_row['is_refunded'] == 0) {

                $cashbackNotUsed = ($order_row['status'] == 'OH') && ($order_row['on_hold_reason_id_fk'] == 6 || $order_row['on_hold_reason_id_fk'] == 7);
                $loyaltyPointsUsageNotDone = ($order_row['status'] == 'OH') && ($order_row['on_hold_reason_id_fk'] == 24 || $order_row['on_hold_reason_id_fk'] == 26);
                $loyaltyPointsAwardNotDone = ($order_row['status'] == 'OH') && ($order_row['on_hold_reason_id_fk'] == 28);

                // The following states are set based on how orchestrator order confirm bean's actions are done
                if ($loyaltyPointsUsageNotDone) {
                    $loyaltyPointsAwardNotDone = true;
                }
                if ($cashbackNotUsed) {
                    $loyaltyPointsUsageNotDone = true;
                    $loyaltyPointsAwardNotDone = true;
                }
                //For exchange, these need to be debited from the current order and credited to parent order.
                //find parent order.
                $originalOrder = func_query_first("SELECT releaseid from exchange_order_mappings where exchange_orderid = $orderid order by exchange_orderid desc", true);
                $originalOrderId = $originalOrder['releaseid'];
                $originalOrderDetails = func_query_first("SELECT orderid, group_id from xcart_orders where orderid = $originalOrderId", true);
                $original_group_id = $originalOrderDetails['group_id'];
                //debit from child order
       			if (!$loyaltyPointsAwardNotDone) {
                    $awarded_loyalty_points_reverted = $order_row['loyalty_points_awarded'];
                }
		       if ($awarded_loyalty_points_reverted > 0.00) {
        		    $ret = LoyaltyPointsPortalClient::creditDebitInactiveForExchangeOrder($order['login'], $orderid,$original_group_id, $awarded_loyalty_points_reverted,"Reversing credit of points for order no ".$orderid,"Awarding points for original order no ".$original_group_id);
                    if(!$ret || $ret['status']['statusType'] == 'ERROR'){
    					$comment .= "Crediting and Debiting $awarded_loyalty_points_reverted failed.\n";
                    }else {
            			$comment .= "Crediting and debiting $awarded_loyalty_points_reverted loyalty points successful.\n";                
            		}
            	}
            }
        }

    }
    if ($order['ordertype'] != 'ex') {
        $mycouponsurl = $http_location . '/mymyntra.php?view=mymyntcredits';

        $debit_from_cb = 0.00;
        $cb_debited_from_refund = 0.00;
        $coupon_amount_to_refund = 0.00;
        $minPurchaseAmount = 0.00;
        foreach ($to_refund_orders as $order_row) {
            if ($order_row['is_refunded'] == 0) {

                $cashbackNotUsed = ($order_row['status'] == 'OH') && ($order_row['on_hold_reason_id_fk'] == 6 || $order_row['on_hold_reason_id_fk'] == 7);
                $giftCardNotUsed = ($order_row['gift_card_amount'] < 0.00);
                $loyaltyPointsUsageNotDone = ($order_row['status'] == 'OH') && ($order_row['on_hold_reason_id_fk'] == 24 || $order_row['on_hold_reason_id_fk'] == 26);
                $loyaltyPointsAwardNotDone = ($order_row['status'] == 'OH') && ($order_row['on_hold_reason_id_fk'] == 25);

                // The following states are set based on how orchestrator order confirm bean's actions are done
                if ($loyaltyPointsUsageNotDone) {
                    $loyaltyPointsAwardNotDone = true;
                }
                if ($cashbackNotUsed) {
                    $loyaltyPointsUsageNotDone = true;
                    $loyaltyPointsAwardNotDone = true;
                }


                $refundMyntCashForOrder = $refundMyntCash;
                if ($cashbackNotUsed && $giftCardNotUsed) {
                    $refundMyntCashForOrder = false;
                    $comment .= ". not issuing cashback, as cashback was originally not debited while order placement.";
                }

                // refund myntcash/cashbcak on if $refundMyntCash is set to true
                if ($refundMyntCashForOrder) {
                    $refund_amount = $order_row['cash_redeemed'];

                    //retrive the orders myntcash/cashback usage details to pass to cashback/myntcash api
                    $orderMyntCashDetails["earned_credit_usage"] += $order_row['earned_credit_usage'];
                    $orderMyntCashDetails["store_credit_usage"] += $order_row['store_credit_usage'];
                } else {
                    $refund_amount = 0.0;
                }

                $minPurchaseAmount += ($order_row['subtotal'] - $order_row['discount'] - $order_row['cart_discount']);

                $refund_amount -= $order_row['difference_refund'];
                if ($online && $reason != 'OPIS') {
                    $refund_amount += $order_row['total'];
                    if (!$activeOrderidInGroup) {
                        $refund_amount += $order_row['payment_surcharge'];
                    }
                    if ($activeOrderidInGroup) {
                        $refund_amount -= $order_row['gift_charges'];
                        $refund_amount -= $order_row['shipping_cost'];
                    }
                }
                if(!$online && $reason != 'OPIS' && $order_row['gift_card_amount']){
                    $refund_amount += $order_row['gift_card_amount'];
                }
                if ($order_row['cashback'] > 0) {
                    $refund_amount -= $order_row['cashback'];
                    if ($refund_amount < 0 && $reason == 'OPIS') {
                        $debit_from_cb += abs($refund_amount);
                        $refund_amount = 0.00;
                    } else {
                        $cb_debited_from_refund += $order_row['cashback'];
                    }
                }
                $final_refund_amount += $refund_amount;

                if (!empty($order_row['coupon'])) {
                    $coupon_amount_to_refund += $order_row['coupon_discount'];
                }

                if (!$loyaltyPointsUsageNotDone) {
                    $used_loyalty_points_refunded += $order_row['loyalty_points_used'];
                }
                if (!$loyaltyPointsAwardNotDone) {
                    $awarded_loyalty_points_reverted += $order_row['loyalty_points_awarded'];
                }
            }
        }

        $comment = "";
        $cb_dedcution_msg = "";
        if ($cb_debited_from_refund > 0) {
            if ($cancel_full_order)
                $cb_dedcution_msg = "Cashback amount of Rs. $cb_debited_from_refund offered on this order has been deducted from your refund amount.";
            else
                $cb_deduction_msg = "Cashback amount of Rs. $cb_debited_from_refund offered on the item(s) has been deducted from your refund amount.";

            $comment .= "Cashback amount of Rs. $cb_debited_from_refund offered on this order has been deducted from refund amount.\n";
        }

        foreach ($to_refund_orders as $order_row) {
            if ($order_row['is_refunded'] == 0) {
                $price_mismatch_refund += $order_row['difference_refund'];
            }
        }

        $orderid = $order['orderid'];
        if ($price_mismatch_refund != 0.00 && $order['payment_method'] == 'cod' && $order_row['cash_redeemed'] == 0.00) {
            $trs = new MyntCashTransaction($order['login'], MyntCashItemTypes::ORDER, $orderid, MyntCashBusinessProcesses::MRP_MISMATCH, 0, 0, abs($price_mismatch_refund), "Cancellation of Order no : $order_groupid. Cashback of Rs. $price_mismatch_refund is debited");
            MyntCashService::debitFromUserMyntCashAccount($trs);
            $cb_dedcution_msg = "Price mismatch credit of Rs. $price_mismatch_refund offered on this order has been reverted.";
            $comment .= "Price mismatch credit of Rs. $price_mismatch_refund offered on this order has been reverted.\n";
        }

        if ($debit_from_cb > 0) {
            $trs = new MyntCashTransaction($order['login'], MyntCashItemTypes::ORDER, $orderid, MyntCashBusinessProcesses::CASHBACK_REVERSAL, 0, 0, abs($price_mismatch_refund), "Online Payment Issue cancellation on Order no : $orderid. Cashback of Rs. $debit_from_cb is debited");
            MyntCashService::debitFromUserMyntCashAccount($trs);
            $cb_dedcution_msg = "Cashback credit of Rs. $debit_from_cb offered on this order has been reverted.";
            $comment .= "Cashback credit of Rs. $debit_from_cb offered on this order has been reverted.\n";
        }

        if ($used_loyalty_points_refunded > 0.00) {
            $ret = LoyaltyPointsPortalClient::creditForItemCancellation($order['login'], $order['group_id'], $used_loyalty_points_refunded, "Refunding  loyalty points used for cancelled item in order $order[group_id]");
            if ($ret['status']['statusType'] == 'SUCCESS') {
            	$comment .= "Refund of " . number_format($used_loyalty_points_refunded, 2) . " loyalty-points-used for cancelled item in order $order[group_id].\n";                
            } else {
            	$comment .= "Refund of " . number_format($used_loyalty_points_refunded, 2) . " loyalty-points-used FAILED for cancelled item in order $order[group_id].\n";             
            }
        }

        if ($awarded_loyalty_points_reverted > 0.00) {
            $ret = LoyaltyPointsPortalClient::debitForItemCancellation($order['login'], $order['group_id'], $awarded_loyalty_points_reverted, "Reversing credit of points for cancellation of order no. $order[group_id]");
            if ($ret['status']['statusType'] == 'SUCCESS') {
                $comment .= "Reverting " . number_format($awarded_loyalty_points_reverted, 2) . " loyalty-points-awarded for cancelled item in order $order[group_id].\n";
            } else {
            	$comment .= "Reverting " . number_format($awarded_loyalty_points_reverted, 2) . " loyalty-points-awarded FAILED for cancelled item in order $order[group_id].\n";                
            }
        }

        $coupon_usage_text = "";
        if ($coupon_amount_to_refund && $refundCoupon) {
            $expirytime = time() + (30 * 24 * 60 * 60);
            $couponType = WidgetKeyValuePairs::getWidgetValueForKey('cancellationCouponType');
            $couponData = $couponAdapter->generateCouponForCancellation($order['coupon'], 'CANC', 6, 'CANCELLATIONS', time(), $expirytime, $order['login'], $couponType, $coupon_amount_to_refund, ($coupon_amount_to_refund/$minPurchaseAmount) * 100, $minPurchaseAmount, '', false);
            $couponCode = $couponData['coupon'];
            $minCartVal = $couponData['minCartVal'];
            $coupon_usage_text = "We are creating a replacement for the coupon " . $order['coupon'] . " used on this transaction. You could use the coupon code: $couponCode on your next transaction on Myntra. The coupon will be valid until " . date('M jS Y', $expirytime) . ".";
            $comment .= "Replacement coupon: ".$couponData['description']." issued\n";
        }

        if ($refund_to_cb_acc) {
            if ($final_refund_amount > 0) {
            	if ($refundMyntCashForOrder) {
                    //retrive the orders myntcash/cashback usage details to pass to cashback/myntcash api
                    $orderMyntCashDetails["store_credit_usage"] += $final_refund_amount;
                }else{
                    $orderMyntCashDetails = array();
                    $orderMyntCashDetails["store_credit_usage"] = $final_refund_amount;
                    $orderMyntCashDetails["earned_credit_usage"]=0;
                }
                if ($cancel_full_order) {
                    //$couponAdapter->creditCashback($order['login'], $final_refund_amount, "Refund on cancellation of order - $order_groupid");
                    $trs = new MyntCashTransaction($order['login'], MyntCashItemTypes::ORDER, $order_groupid, MyntCashBusinessProcesses::FULL_CANCELLATION, 0, 0, 0, "Refund on cancellation of order - $order_groupid");
                    MyntCashService::creditMyntCashForOrderCancellation($trs, $final_refund_amount, $orderMyntCashDetails);
                    $refund_msg = "The refund amount of Rs. " . number_format($final_refund_amount, 2) . " for this order cancellation has been credited to your Mynt Club cashback account. You can view the details of the same by <a href=\"$mycouponsurl\">clicking here</a>.";
                } else {
                    //$couponAdapter->creditCashback($order['login'], $final_refund_amount, "Refund for cancelling item(s) in order - $order_groupid");
                    $trs = new MyntCashTransaction($order['login'], MyntCashItemTypes::ORDER, $order_groupid, MyntCashBusinessProcesses::PARTIAL_CANCELLATION, 0, 0, 0, "Refund for cancelling item(s) in order - $order_groupid");
                    MyntCashService::creditMyntCashForOrderCancellation($trs, $final_refund_amount, $orderMyntCashDetails);
                    $refund_msg = "Your refund amount of Rs " . number_format($final_refund_amount, 2) . " for the cancelled item(s) has been credited to your Mynt Club cashback account. You can view the details of the same by <a href=\"$mycouponsurl\">clicking here</a>.";
                }
            }
            $comment .= "Rs. $final_refund_amount refunded into cashback account for order cancellation.\n";
        } else {
            if ($final_refund_amount > 0) {
                if ($cancel_full_order) {
                    $refund_msg = "Your order amount of Rs. " . number_format($final_refund_amount, 2) . " is being refunded and  should reflect in your account within the next 10 business days.";
                } else {
                    $refund_msg = "Your refund amount of Rs " . number_format($final_refund_amount, 2) . " for the cancelled item(s) is being refunded and should reflect in your account within the next 10 business days.";
                }
                $comment .= "Rs. $final_refund_amount to be refunded to user for order cancellation.\n";
            }
        }
        $weblog->info("final_refund_amount: $final_refund_amount");
    } else {
        $comment .= "No refund issued for the cancellation of exchange order.\n";
    }
    $gw_coupons_msg = "";
    // If it is an order cancellation and good will coupons need to be generated..
    if (($new_status == 'F' && $generateGoodWillCoupons && ($reason == 'OOSC' || $reason == 'VFS' || $reason == 'PNS')) || $new_status == 'L') {
        if ($orderid2couponDetails && count($orderid2couponDetails) > 0) {
            $coupon_details = $orderid2couponDetails[$orderid];
            if ($coupon_details && count($coupon_details) > 0) {
                $couponCount = $coupon_details['count'] != "" ? $coupon_details['count'] : $defaultCouponCount;
                $couponMrp = $coupon_details['amount'] != "" ? $coupon_details['amount'] : $defaultCouponMrp;
                $couponMinPurchase = $coupon_details['min_amount'] != "" ? $coupon_details['min_amount'] : $defaultCouponMinPurchase;
                $couponExpiry = $coupon_details['days'] != "" ? $coupon_details['days'] : $defaultCouponExpiry;
            }
        } else {
            $couponCount = $defaultCouponCount;
            $couponMrp = $defaultCouponMrp;
            $couponMinPurchase = $defaultCouponMinPurchase;
            $couponExpiry = $defaultCouponExpiry;
        }

        $expirytime = time() + (intval($couponExpiry) * 24 * 60 * 60);

        $couponCodes = "";
        for ($i = 0; $i < intval($couponCount); $i++) {
            $coupon = $couponAdapter->generateSingleCouponForUser('GW', 8, 'OOSCANCELLATION', time(), $expirytime, $order['login'], 'absolute', $couponMrp, '', $couponMinPurchase);
            if ($couponCodes != "") {
                $couponCodes .= "/";
            }
            $couponCodes .= $coupon;
        }
        $validity_date = date('M jS Y', $expirytime);

        $gw_coupons_msg_sql = "SELECT * FROM $sql_tbl[mk_email_notification] WHERE name = 'order_cancel_gw_coupon'";
        $gw_coupons_msg_row = func_query_first($gw_coupons_msg_sql);
        $gw_coupons_msg = $gw_coupons_msg_row['body'];
        $gw_coupons_msg = str_replace("[COUPON_CODES]", $couponCodes, $gw_coupons_msg);
        $gw_coupons_msg = str_replace("[COUPON_AMOUNT]", $couponMrp, $gw_coupons_msg);
        $gw_coupons_msg = str_replace("[EXPIRY_DATE]", $validity_date, $gw_coupons_msg);

        $comment .= "Coupon(s) $couponCodes (Rs $couponMrp off $couponMinPurchase) with expiry $validity_date issued to user for order cancellation.\n";
    }

    if ($cancel_full_order) {
        func_addComment_order($order_groupid, $user, 'Note', 'Full Order Cancellation', $comment);
    } else {
        func_addComment_order($order_groupid, $user, 'Note', 'Part Cancellation', "Part of this order with orderid $orderid has been cancelled.");
        func_addComment_order($orderid, $user, 'Note', 'Order Cancellation', $comment);
    }

    //complie mail args
    $args = array("USER" => $order['firstname'],
        "ORDER_ID" => $order_groupid,
        "SHIPMENT_ID" => $orderid,
        "GOODWILL_COUPON_MSG" => $gw_coupons_msg,
        "COUPON_USAGE_TEXT" => $coupon_usage_text,
        "REFUND_MSG" => $refund_msg,
        "CAHBACK_DEDUCT_MSG" => $cb_dedcution_msg
    );

    return array('status' => "SUCCESS", 'refund_amount' => $final_refund_amount, 'mail_args' => $args);
}

#
# This function change order status in orders table
#this method should not be used for order cancellation...
#instead call func_cancel_order()
#
function func_change_order_status($orderids, $statusIn, $login="AUTOSYSTEM", $status_change_comment="", $advinfo="", $update_inventory=true, $oh_reason_id = '', $cancel_args=false, $trackprogress=false, $progressKey='', $notifyCustomer = true, $statusChangeTime=null) {

	global $config, $mail_smarty, $active_modules, $current_area, $cashback_gateway_status, $db_query_retries;
	$http_location = HostConfig::$httpHost;
	global $sql_tbl;
    global $weblog;
	global $session_failed_transaction;
	global $xcart_dir;

	$xcache = new XCache();
	if (!is_array($orderids)) $orderids = array($orderids);
	
	if($statusIn == 'F' || $statusIn == 'D') {
		return func_cancel_order($orderids, $statusIn, $login, $status_change_comment, $update_inventory, $cancel_args['cancellation_type'], $cancel_args['reason'], 
					$cancel_args['orderid2fullcancelordermap'], $cancel_args['generateGoodWillCoupons'], $trackprogress, $progressKey, $cancel_args['orderid2couponDetails'], $cancel_args['is_oh_cancellation']);
	}
	
	$count = 0;
	
	foreach ($orderids as $orderid) {
		$order_data = func_order_data($orderid);
		$status=$statusIn;
        $count++;
        
        if($trackprogress){
	        $progressInfo = $xcache->fetch($progressKey."_progressinfo");
	        $progressInfo['completed'] = $count;
	        $xcache->store($progressKey."_progressinfo", $progressInfo, 86400);
        }
        if(empty($order_data) && $status == 'L'){
        	return array("status" => "failure", "message" => " invalid orderid ");
        }
        
		if (empty($order_data))
			continue;

		$order = $order_data["order"];
		
		if($status == "OH" && !($order["status"] == 'Q' || $order["status"] == 'WP'))
			continue;
/*
		if($status == "Q" && !($order["status"] == 'OH' || $order["status"] == 'PP' || $order["status"] == 'PD' 
		    || $order["status"] == 'D' || ($order["status"] == 'F' && empty($order['cancellation_code'])) ) ) {
			continue;			
		}
*/
		if($status == "WP" || $status == "Q")
			continue;
		
		if($status == "SH" && ( $order["status"] != 'PK' && $order["status"] != 'WP') ) {
			continue;	
		}
		
		if($status == 'PK' && $order["status"] != 'WP'){
			continue;
		}
		
		if($status == 'L' && !($order["status"] == 'WP' || $order["status"] == 'PK' || $order["status"] == 'DL' || $order["status"] == 'SH' 
				|| ($order["status"] == 'C' && $order["payment_method"] != 'cod'))){
			return array("status" => 'failure', "message" => " transition not allowed from its current status");
		}
		
		if ($advinfo)
			$info = addslashes(text_crypt($order["details"]."\n--- Advanced info ---\n".$advinfo));

	// If the order goes from PP to Q, the coupon lock should be released, then send the order for queuing else return
		if ($statusIn == 'Q' && $order['status'] == 'PP') {
			// Decrement the coupon lock.
		    require_once $xcart_dir.'/modules/coupon/database/CouponAdapter.php';
		    $couponAdapter = CouponAdapter::getInstance();
		    
			if(!empty($order['coupon'])) {
				$couponAdapter->updateUsageByUser($order['coupon'], $order_data['userinfo']['login'], $order['subtotal'], $order['coupon_discount'],$orderid);
				$couponAdapter->unlockCouponForUser($order['coupon'], $order_data['userinfo']['login']);	
			}
			if(!empty($order['cash_coupon_code'])) {
				//cashback usage
				//$couponAdapter->updateUsageByUser($order['cash_coupon_code'],$order_data['userinfo']['login'],$order['subtotal'],$order['cash_redeemed'],$orderid);
				//$couponAdapter->unlockCouponForUser($order['cash_coupon_code'], $order_data['userinfo']['login']);
				//$trs = new MyntCashTransaction($order_data['userinfo']['login'], $orderid, MyntCashBusinessProcesses::CASHBACK_USED, 0, 0,$order['cash_redeemed'],  "Usage on order no. $orderid");
				//$myntCashUsageResponse = MyntCashService::debitMyntCashForOrder($trs,$orderid);
				
				
				
			}
			
			if(!empty($order["cash_redeemed"])){
				
				$trs = new MyntCashTransaction($order_data['userinfo']['login'],MyntCashItemTypes::ORDER, $orderid, MyntCashBusinessProcesses::CASHBACK_USED, 0, 0,$order['cash_redeemed'],  "Usage on order no. $orderid");
				$myntCashUsageResponse = MyntCashService::debitMyntCashForOrder($trs,$orderid);
				
				// order status change from PP => Q. If cashback fails, cancell the order with *no cashback return*.
				//myntCashDebitfailureProcedure($orderid,$myntCashUsageResponse,$cancelOrder = true,$continue = false);
				
				if($myntCashUsageResponse === MyntCashService::noEnoughBalanceForDebitRequest
						|| $myntCashUsageResponse === MyntCashService::serviceNotAvailable){
					return false;
				}
				
			}
			/// 
		}
		
		
		
		if ($statusIn == 'Q' && $order['status'] == 'OH') {
			$oh_reason1 = get_oh_reason_for_code("CDE", "CNA");
			$oh_reason2 = get_oh_reason_for_code("CDE", "CSD");
			
			if($order["on_hold_reason_id_fk"] == $oh_reason1['id'] || $order["on_hold_reason_id_fk"] == $oh_reason2['id']){
				if(!empty($order["cash_redeemed"])){
				
					$trs = new MyntCashTransaction($order_data['userinfo']['login'],MyntCashItemTypes::ORDER, $orderid, MyntCashBusinessProcesses::CASHBACK_USED, 0, 0,$order['cash_redeemed'],  "Usage on order no. $orderid");
					$myntCashUsageResponse = MyntCashService::debitMyntCashForOrder($trs,$orderid);
				
					// order status change from PP => Q. If cashback fails, cancell the order with *no cashback return*.
					//myntCashDebitfailureProcedure($orderid,$myntCashUsageResponse,$cancelOrder = true,$continue = false);
				
					if($myntCashUsageResponse === MyntCashService::noEnoughBalanceForDebitRequest
							|| $myntCashUsageResponse === MyntCashService::serviceNotAvailable){
						return false;
					}
				
				}
			}
			
			$oh_reason_cb_failure = get_oh_reason_for_code("CSFE");
			if($order["on_hold_reason_id_fk"] == $oh_reason_cb_failure['id']){
				$original_order = func_query_first("SELECT ex.*, o.group_id from exchange_order_mappings ex, xcart_orders o where ex.exchange_orderid = $orderid and ex.releaseid = o.group_id");
				$creditTransaction = new MyntCashTransaction($order['login'],MyntCashItemTypes::ORDER,  $original_order[group_id],  MyntCashBusinessProcesses::PARTIAL_CANCELLATION ,  0,0, 0,  "Refund on exchange of order - $original_order[group_id]");
				$debitTransaction = new MyntCashTransaction($order['login'], MyntCashItemTypes::ORDER, $orderid, MyntCashBusinessProcesses::CASHBACK_USED, 0, 0, $order['cash_redeemed'], "Debiting amount for the new exchanged order: $orderid");
				
				$old_item = reset(ItemManager::getItemDetailsForItemIds($original_order['itemid'], $original_order['releaseid']));
				$myntCashUsageResponse = MyntCashService::creditAndDebitMyntCashForExchangeOrder($creditTransaction, $debitTransaction, $orderid, $order['cash_redeemed'], array($old_item,));
				if($myntCashUsageResponse === MyntCashService::creditDebitFailure || $myntCashUsageResponse === MyntCashService::serviceNotAvailable){
					return false;
				}
			}
		}
		
		
		
		// If the order goes from D or F to Q, check if coupon is still valid then send the order for queuing else return
		if ($statusIn == 'Q' && ($order['status'] == 'D'||$order['status'] == 'F')) {
			// Decrement the coupon lock.
			require_once $xcart_dir.'/modules/coupon/database/CouponAdapter.php';
			require_once ("$xcart_dir/modules/coupon/CouponValidator.php");
			$couponAdapter = CouponAdapter::getInstance();
			$validator = CouponValidator::getInstance();
			if(!empty($order['coupon'])) {				
				$coupon = $couponAdapter->fetchCoupon($order['coupon']);
				$validator->isCouponValidForUser($coupon,  $order_data['userinfo']['login']); // an exception will be thrown here if coupon is not valid
				$couponAdapter->updateUsageByUser($order['coupon'], $order_data['userinfo']['login'], $order['subtotal'], $order['coupon_discount'],$orderid);	
			}
			if(!empty($order['cash_coupon_code'])) {
				//cashback usage
				$coupon = $couponAdapter->fetchCoupon($order['cash_coupon_code']);
				//$validator->isCouponValidForUser($coupon,  $order_data['userinfo']['login']); // an exception will be thrown here if coupon is not valid
				//$couponAdapter->updateUsageByUser($order['cash_coupon_code'],$order_data['userinfo']['login'],$order['subtotal'],$order['cash_redeemed'],$orderid);
				//$trs = new MyntCashTransaction($order_data['userinfo']['login'], $orderid, MyntCashBusinessProcesses::CASHBACK_USED, 0, 0,$order['cash_redeemed'],  "Usage on order no. $orderid");
				//$myntCashUsageResponse = MyntCashService::debitMyntCashForOrder($trs,$orderid);
			}
			if(!empty($order["cash_redeemed"])){
				
				$trs = new MyntCashTransaction($order_data['userinfo']['login'], MyntCashItemTypes::ORDER,$orderid, MyntCashBusinessProcesses::CASHBACK_USED, 0, 0,$order['cash_redeemed'],  "Usage on order no. $orderid");
				$myntCashUsageResponse = MyntCashService::debitMyntCashForOrder($trs,$orderid);
				
				// order status change from F/D => Q , if cashback fails, do not allow the process
				//myntCashDebitfailureProcedure($orderid,$myntCashDebitResponse,$cancelOrder = true,$continue = true);

				if($myntCashUsageResponse === MyntCashService::noEnoughBalanceForDebitRequest
						|| $myntCashUsageResponse === MyntCashService::serviceNotAvailable){
					return false;
				}
			}
		}
	
                if($statusIn == 'OH'){
			global $weblog;$weblog->debug("oh reason $oh_reason_id");
			//$oh_code = get_oh_reason_for_code($oh_reason);
			$oh_code = get_oh_reason_for_id($oh_reason_id);
			db_query("update $sql_tbl[orders] set status='$status'".(($advinfo)? ", details='".$info."'" : "").",on_hold_reason_id_fk= ".$oh_code['id'] ." where orderid='$orderid'");
			if($order['group_id'] == $order['orderid']) {
				EventCreationManager::pushOrderUpdateEvent($orderid, $status, $status_change_comment, time(), $login, $oh_code['id']);
			} else {
				EventCreationManager::pushReleaseStatusUpdateEvent($orderid, $status, $status_change_comment,time(),$login, array(), $oh_code['id']);
			}
		}else {
                        $result = false;
                        $i = $db_query_retries;
                        do{
                           $result = db_query("update $sql_tbl[orders] set status='$status'".(($advinfo)? ", details='".$info."'" : "")." where orderid='$orderid'");
                        }while(--$i > 0 && $result=== false);
                        if($statusIn == 'SH' || $statusIn == 'C' || $statusIn == 'Q' || $statusIn == 'DL' || $statusIn == 'PK'){
				func_update_dates_orders($orderid, $statusIn, $order, $statusChangeTime);
			}
			if($order['status'] == 'OH') {
				if($orderid == $order['group_id']) {
                                        EventCreationManager::pushOrderUpdateEvent($order['group_id'], $status, $status_change_comment, time(), $login);
				}
                                    EventCreationManager::pushReleaseStatusUpdateEvent($orderid, false, $status_change_comment, time(), $login);
                            } else {
                                if($order['status'] != 'PP' && $order['status'] != 'PV' && $order['status'] != 'D' && $order['status'] != 'F') {
                                    EventCreationManager::pushReleaseStatusUpdateEvent($orderid, $status, $status_change_comment, time(), $login);
                                }
                            }	
		}
		
		if($update_inventory==true&&($statusIn=='PP' ||$statusIn=='PV' || $statusIn=='PD') && ($order['status']=='Q'||$order['status']=='WP'||$order['status']=='OH')){
			require_once $xcart_dir.'/include/func/func_sku.php';
			increment_sku($orderid, $order['warehouseid']);
		}else if($update_inventory==true&&($statusIn=='Q'||$statusIn=='WP'||$statusIn=='SH'||$statusIn=='C'||$statusIn=='OH' ) && ($order['status']=='PP'||$order['status']=='PV'||$order['status']=='PD' ||$order['status']=='F' || $order['status']=='D')){
			require_once $xcart_dir.'/include/func/func_sku.php';
			decrement_sku($orderid, $order['warehouseid']);
		}
		
	    $weblog->info("order status change : $orderid from ".$order["status"]." to $status ");
        //$sqllog->debug("update $sql_tbl[orders] set status='$status' where orderid='$orderid'");

		/*if ($status == "P" && $order["status"] != "P") 
		{
			$flag = true;if($order["status"] == 'OH') {
				ReleaseApiClient::pushOrderReleaseToWMS($orderid, $login,'update');
			}
			if (in_array($order["status"], array('I','Q')) && !empty($active_modules["Anti_Fraud"]) && $config['Anti_Fraud']['anti_fraud_license'] && ($current_area != 'A' && $current_area != 'P') && !empty($order['extra']['Anti_Fraud'])) {
				$total_trust_score = $order['extra']['Anti_Fraud']['total_trust_score'];
				$available_request = $order['extra']['Anti_Fraud']['available_request'];
				$used_request = $order['extra']['Anti_Fraud']['used_request'];

				if ($total_trust_score > $config['Anti_Fraud']['anti_fraud_limit'] || ($available_request <= $used_request && $available_request > 0)) {
					$flag = false;
					db_query("UPDATE $sql_tbl[orders] SET status = 'Q' WHERE orderid = '$orderid'");
                    $weblog->info("order status change : $orderid changed to Q ");
					$to_customer = ($order_if($order["status"] == 'OH') {
				ReleaseApiClient::pushOrderReleaseToWMS($orderid, $login,'update');
			}data['userinfo']['language'] ? $order_data['userinfo']['language'] : $config['default_customer_language']);


					$mail_smarty->assign("products", func_translate_products($order_data["products"], $to_customer));
					$mail_smarty->assign("giftcerts",$order_data["giftcerts"]);
					$mail_smarty->assign("order",$order_data["order"]);
					$mail_smarty->assign("userinfo",$order_data["userinfo"]);
					//func_send_mail($order_data["userinfo"]['email'], "mail/order_customer_subj.tpl", "mail/order_customer.tpl", $config["Company"]["orders_department"], false);
				}
			}

			if ($flag) {
				func_process_order($orderid);
			}
		}else*/
		if ( ($status == "C" && $order["status"] != "C") ||  ($status == "DL" && $order["status"] != "DL")) 
		{
			func_complete_order($orderid, $status);
		}
		elseif ($status == "Q") 
		{
			func_queued_order($orderid, $order["status"]);
		} else if($status == "SH") {
			func_ship_order($order, $order_data['userinfo'], $notifyCustomer);
		} else if($status == 'L' && ($order["status"] == 'WP' || $order["status"] == 'PK' || $order["status"] == 'DL' || $order["status"] == 'SH'
				|| ($order["status"] == 'C' && $order["payment_method"] != 'cod') )) {
			$response = func_lost_order($orderid, $order_data['userinfo']['login'], $cancel_args['cancellation_type'], $cancel_args['reason'], $cancel_args['orderid2fullcancelordermap']);
			return $response;
		}
		
		ReleaseApiClient::pushOrderReleaseToWMS($orderid, $login,'update');
		
		func_addComment_status($orderid, $status, $order["status"], $status_change_comment, $login);
		#
		# Decrease quantity in stock when "declined" or "failed" order is became "completed", "processed" or "queued"
		#
		if ($status != $order["status"] && strpos("DF",$order["status"])!==false && strpos("CPQI",$status)!==false) {
			func_update_quantity($order_data["products"],false);
		}
		
	}   

	$op_message = "Login: $login\nIP: $REMOTE_ADDR\nOperation: change status of orders (".implode(',',$orderids).") to '$statusIn'\n----";
	x_log_flag('log_orders_change_status', 'ORDERS', $op_message, true);
	return true;
}

#
#This function adds a comments log when status is changed for a order
#
function func_addComment_status($orderid, $status,$status_old, $statusMSG = null, $commentBy = null){
	$statusvalue;
	$statusvalue_old;
	switch ($status) {
		
		case 'OH' :
		$statusvalue='On Hold';
		break;
		case "I" :
		$statusvalue='Not Finished';
		break;
		case "Q" :
		$statusvalue='Queued';
		break;
		case "P" :
		$statusvalue='Processed';
		break;
		case "B" :
		$statusvalue='Backordered';
		break;
		case "D" :
		$statusvalue='Declined';
		break;
		case "F" :
		$statusvalue='Rejected';
		break;
		case "C" :
		$statusvalue='Complete';
		break;
		case "PD" :
		$statusvalue='Pending Check Collection';
		break;
		case "PP" :
		$statusvalue='Preprocessed';
		break;
		case "CD" :
		$statusvalue='COD Verification';
		break;
		case "PV" :
		$statusvalue='COD Pending Verification';
		break;
		case "PK" :
		$statusvalue='Packed';
		break;
		case "SH" :
		$statusvalue='Shipped';
		break;
		case "L" :
		$statusvalue='Lost';
		break;
		case "DL" :
		$statusvalue='Delivered';
		break;
		case "WP" :
		$statusvalue='Work in Progress';
		break;
		default:
		$statusvalue='$status';
			
		
	}
	switch ($status_old) {
		
		case 'OH' :
		$statusvalue_old='On Hold';
		break;
		case "I" :
		$statusvalue_old='Not Finished';
		break;
		case "Q" :
		$statusvalue_old='Queued';
		break;
		case "P" :
		$statusvalue_old='Processed';
		break;
		case "B" :
		$statusvalue_old='Backordered';
		break;
		case "D" :
		$statusvalue_old='Declined';
		break;
		case "F" :
		$statusvalue_old='Rejected';
		break;
		case "C" :
		$statusvalue_old='Complete';
		break;
		case "PD" :
		$statusvalue_old='Pending Check Collection';
		break;
		case "PP" :
		$statusvalue_old='Preprocessed';
		break;
		case "CD" :
		$statusvalue_old='COD Verification';
		break;
		case "PV" :
		$statusvalue_old='COD Pending Verification';
		break;
		case "PK" :
		$statusvalue_old='Packed';
		break;
		case "SH" :
		$statusvalue_old='Shipped';
		break;
		case "L" :
		$statusvalue_old='Lost';
		break;
		case "DL" :
		$statusvalue_old='Delivered';
		break;
		case "WP" :
		$statusvalue_old='Work in Progress';
		break;
		default:
		$statusvalue_old='$status_old';
		
	}
	
	$addedby="admin";
	$commenttype="Automated";
	$commenttitle="status change";
	$commentdetails="Changed Order status to ".$statusvalue." From ".$statusvalue_old;
	if ((func_num_args() >= 4) && (!is_null($statusMSG))) {
		$commentdetails=$commentdetails."\n\n".$statusMSG;
	}
	if ((func_num_args() >= 5) && (!is_null($commentBy))) {
		$addedby = $commentBy;
	}
	if($statusvalue=='Rejected' && $statusvalue==$statusvalue_old){
		$commenttitle="Status not changed";
		$commentdetails="Status not changed\n\n$statusMSG";
	}else if($statusvalue==$statusvalue_old){
	
		$commenttitle="Status not changed";
	    $commentdetails="Not changed status but may be made tracking or ccavenue changes";
	}

	func_addComment_order($orderid, $addedby, $commenttype, $commenttitle, $commentdetails);
}

function func_update_order_details($orderid, $firstname,$lastname,$address,$zipcode,$phone,$mobile,$email,$giftmessage){

    
    $sql="update xcart_orders set s_firstname='$firstname',s_lastname='$lastname',s_address='$address', s_zipcode='$zipcode',phone='$phone', mobile='$mobile',email='$email',notes='$giftmessage' where orderid='$orderid'";    
    db_query($sql);
    //automatically add a comment log    

    //func_addComment_order($orderid,"CUSTOMER","Change request","Address change" ,"Address changeed by user");
}

function func_addComment_order($orderid, $addedby,$commenttype,$commenttitle,$commentdetails, $address='', $gift_wrap_message='', $attachmentname=''){
	$time=time();
	$commenttitle = mysql_real_escape_string($commenttitle);
	$commentdetails = mysql_real_escape_string($commentdetails);
	$query = "insert into `mk_ordercommentslog` (orderid,commentaddedby,commenttitle,commenttype,description,commentdate,newaddress,giftwrapmessage,attachmentname) values ($orderid,'$addedby','$commenttitle','$commenttype','$commentdetails','$time','$address','$gift_wrap_message','$attachmentname')";
	db_query($query);
}

#
# This function performs activities nedded when order is processed
#
function func_process_order($orderids) {
	global $config, $mail_smarty, $active_modules;
	global $sql_tbl, $partner, $to_customer;
	global $single_mode;
	global $xcart_dir;
	global $current_area;

	if (empty($orderids))
		return false;

	if (!is_array($orderids))
		$orderids = array($orderids);

	foreach($orderids as $orderid) {

		if (empty($orderid))
			continue;

		$order_data = func_order_data($orderid);
		if (empty($order_data))
			continue;

		$order = $order_data["order"];
		$userinfo = $order_data["userinfo"];
		$products = $order_data["products"];
		$giftcerts = $order_data["giftcerts"];

		$mail_smarty->assign("customer",$userinfo);
		$mail_smarty->assign("products",$products);
		$mail_smarty->assign("giftcerts",$giftcerts);
		$mail_smarty->assign("order",$order);

		#
		# Order processing routine
		# Send gift certificates
		#
		if ($order["applied_giftcerts"]) {
			#
			# Search for enabled to applying GC
			#
			$flag = true;
			foreach ($order["applied_giftcerts"] as $k=>$v) {
				$res = func_query_first("SELECT gcid FROM $sql_tbl[giftcerts] WHERE gcid='$v[giftcert_id]' AND debit>='$v[giftcert_cost]'");
				if (!$res["gcid"]) {
					$flag = false;
					break;
				}
			}

			#
			# Decrease debit for applied GC
			#
			if (!$flag)
				return false;

			foreach ($order["applied_giftcerts"] as $k=>$v) {
				db_query("UPDATE $sql_tbl[giftcerts] SET debit=debit-'$v[giftcert_cost]' WHERE gcid='$v[giftcert_id]'");
				db_query("UPDATE $sql_tbl[giftcerts] SET status='A' WHERE debit>0 AND gcid='$v[giftcert_id]'");
				db_query("UPDATE $sql_tbl[giftcerts] SET status='U' WHERE debit<=0 AND gcid='$v[giftcert_id]'");
			}
		}


		if ($giftcerts) {
			foreach ($giftcerts as $giftcert) {
				db_query("update $sql_tbl[giftcerts] set status='A' where gcid='$giftcert[gcid]'");
				//if ($giftcert["send_via"] == "E")
//					func_send_gc($userinfo["email"], $giftcert, $userinfo['login']);
			}
		}

		#
		# Send mail notifications
		#
		if ($config['Email_Note']['eml_order_p_notif_provider'] == 'Y') {
			$providers= func_query("select provider from $sql_tbl[order_details] where $sql_tbl[order_details].orderid='$orderid' group by provider");

			if (is_array($providers)) {
				foreach($providers as $provider) {
					$email_pro = func_query_first_cell("SELECT email FROM $sql_tbl[customers] WHERE login='$provider[provider]'");
					if (!empty($email_pro) && $email_pro != $config["Company"]["orders_department"]) {
						$to_customer = func_query_first_cell ("SELECT language FROM $sql_tbl[customers] WHERE login='$provider[provider]'");
						if(empty($to_customer))
							$to_customer = $config['default_admin_language'];

						//func_send_mail($email_pro, "mail/order_notification_subj.tpl", "mail/order_notification.tpl", $config["Company"]["orders_department"], false);
					}
				}
			}
		}

		$to_customer = func_query_first_cell ("SELECT language FROM $sql_tbl[customers] WHERE login='$userinfo[login]'");
		if (empty($to_customer))
			$to_customer = $config['default_customer_language'];

		if ($config['Email_Note']['eml_order_p_notif_customer'] == 'Y') {
			$mail_smarty->assign("products", func_translate_products($products, $to_customer));
			$_userinfo = $userinfo;
			$userinfo['title'] = func_get_title($userinfo['titleid'], $to_customer);
			$userinfo['b_title'] = func_get_title($userinfo['b_titleid'], $to_customer);
			$userinfo['s_title'] = func_get_title($userinfo['s_titleid'], $to_customer);
			$mail_smarty->assign("customer",$userinfo);

			if ($current_area == 'C')
				$mail_body_template = "mail/order_customer.tpl";
			else
				$mail_body_template = "mail/order_customer_processed.tpl";
			
			//func_send_mail($userinfo["email"], "mail/order_cust_processed_subj.tpl", $mail_body_template, $config["Company"]["orders_department"], false);

			$userinfo = $_userinfo;
			unset($_userinfo);
		}

		//$mail_smarty->assign("products",$products);
		//$mail_smarty->assign("show_order_details", "Y");
		if ($config['Email_Note']['eml_order_p_notif_admin'] == 'Y') {
			$to_customer = $config['default_admin_language'];
			//func_send_mail($config["Company"]["orders_department"], "mail/order_notification_subj.tpl", "mail/order_notification_admin.tpl", $config["Company"]["orders_department"], true, true);
		}

		$mail_smarty->assign("show_order_details", "");

		#
		# Update statistics for sold products
		#
		if ($active_modules["Advanced_Statistics"]) {
			include $xcart_dir."/modules/Advanced_Statistics/prod_sold.php";
		}

		if (!empty($active_modules["SnS_connector"])) {
			global $HTTP_COOKIE_VARS;

			$_old = $HTTP_COOKIE_VARS;
			$HTTP_COOKIE_VARS['personal_client_id'] = $order['extra']['personal_client_id'];
			func_generate_sns_action("Order", $orderid);
			$HTTP_COOKIE_VARS = $_old;
		}

		if (!empty($active_modules['Surveys']) && !empty($userinfo)) {
			func_check_surveys_events("OPC", $order_data, $userinfo['login']);
		}

	}

}

#
# This function performs activities nedded when order is complete
#
function func_complete_order($orderid, $status) {
	global $weblog, $sql_tbl;
	
	$sql = "UPDATE $sql_tbl[order_details] SET item_status='D' WHERE orderid='".$orderid."' AND item_status != 'IC'";
    db_query($sql);
    $weblog->info("Order Marked $status :: item status change : Changing items of order $orderid to D(delivered)");

    if($status == 'DL') {
    	// Release any item which is on hold due to free item fraud prevention
    	releaseFreeItemShipmentOnHold($orderid);
    }
    
}

function func_ship_order($order, $userinfo, $notifyCustomer = true) {
	global $weblog, $sql_tbl;
	
	$orderid = $order['orderid'];
	
	$sql = "UPDATE $sql_tbl[order_details] SET item_status='S' WHERE orderid='".$orderid."' AND item_status != 'IC'";
	db_query($sql);
	$weblog->info("Order Shipped :: item status change : Changing items of order $orderid to S(shipped)");
	
	if($notifyCustomer == true){
		OrderProcessingMailSender::func_send_order_shipped_email($order, $userinfo);
	}
	
}

function func_lost_order($orderid, $user, $reason_type, $reason, $orderid2fullordercancel) {
	global $weblog, $sql_tbl;
	$weblog->info("orderid: $orderid, user: $user, reason_type: $reason_type, reason: $reason");
	$order = func_query_first("Select o.*, sum(od.difference_refund) as difference_refund from $sql_tbl[orders] o left outer join
			xcart_order_details od on od.orderid = o.orderid and od.item_status != 'IC' where o.orderid = $orderid  group by o.orderid");
	
	$order_groupid = $order['group_id'];
	
	$all_orders_in_group = func_query("Select o.*, sum(od.difference_refund) as difference_refund from $sql_tbl[orders] o,
			xcart_order_details od where o.group_id = $order_groupid and od.orderid = o.orderid and od.item_status != 'IC' group by o.orderid");
	
	$cancel_full_order = false;
	
	if(count($all_orders_in_group) == 1)
	{
		$cancel_full_order = true;
	}

	
	$activeOrderidInGroup = false;
	//if we are not cancelling full order find other order in the group which is present
	foreach($all_orders_in_group as $order_row) {
		if(!$cancel_full_order) {
			if($order_row['orderid']!=$orderid && ($order_row['status'] == 'WP' || $order_row['status'] == 'OH' || $order_row['status'] == 'PK' || $order_row['status'] == 'SH')){
				$activeOrderidInGroup = $order_row['orderid']; //we will be transferring our emi charges/anyother charge to this orderid
				break;
			}
		}
	}
	
	$canc_reason_sql = "select * from cancellation_codes where cancel_type = '$reason_type' and cancel_reason = '$reason'";
	$canc_reason = func_query_first($canc_reason_sql);
	
	$sql = "UPDATE $sql_tbl[orders] set is_refunded=1, cancellation_code= '".$canc_reason['id'] ."'";
	if($order['payment_method'] == 'cod')
		$sql .= ", cod_pay_status='lost' ";
	
	if($activeOrderidInGroup) {
		//since there are active orders in group we will shift the emi charge to a active orderid.
		$sql .= ", total= total-$order[gift_charges]-$order[shipping_cost], payment_surcharge = 0.00, shipping_cost=0.00, gift_charges=0.00 ";
		$chargeSQL = "update $sql_tbl[orders] set payment_surcharge=payment_surcharge+$order[payment_surcharge], shipping_cost=shipping_cost+$order[shipping_cost], gift_charges=gift_charges+$order[gift_charges], total= total + $order[gift_charges] + $order[shipping_cost] where orderid='$activeOrderidInGroup'";
		db_query($chargeSQL);
	}
	
	$sql .= " where orderid = $orderid";
	db_query($sql);
	
	if($order['ordertype'] == 'ex'){
		$returnid = func_query_first_cell("select returnid from exchange_order_mappings where exchange_orderid = $orderid");
		if($returnid){
			db_query("update xcart_returns set status ='RRD' where returnid = $returnid");
		}
	}

	//DON't CALL Cancel Release in WMS!
	//ReleaseApiClient::pushOrderReleaseToWMS($orderid, $user,'cancelrelease');
	ReleaseApiClient::pushOrderReleaseToUnicom($orderid, $user,'cancelrelease');

	$product_details = func_create_array_products_of_order($orderid);
	
	$to_refund_orders = array();
	$to_refund_orders[] = $order;
	if($order['is_refunded'] == 1){
		$response = array("status" => 'failure', "message" => " refund already issued ");
		return $response;
	} else {
		$response = func_issue_refund($order, 'L', $user, $to_refund_orders, $activeOrderidInGroup, $reason, $cancel_full_order, false, true, $product_details);
	}
	if($order['ordertype'] == 'ex'){
		$response['status'] = 'success';
		$response['message'] = " no refund issued for this exchange order ";
		$response['exOrder'] = true;
		$response['refund_amount'] = 0.00;
	}
	// send lost order email
	$template = 'order_lost';
	$reason_email_contents = $canc_reason['email_content'];
	$reason_email_contents = str_replace("[ORDER_ID]", $order_groupid, $reason_email_contents);
	
	$subjectargs = array("ORDER_ID" => $order_groupid);
	$args = array();
	$args = $response['mail_args'];
	$args['REASON_TEXT'] = $reason_email_contents;
	
	$amount_details = func_get_order_amount_details($orderid);
	$orderdetails = create_product_detail_table_new($product_details, $amount_details, 'email', "refund");
	$args['ITEM_DETAILS'] = $orderdetails;
	
	$customKeywords = array_merge($subjectargs, $args);
	$mailDetails = array(
			"template" => $template,
			"to" => $order['login'],
			"bcc" => "myntramailarchive@gmail.com",
			"header" => 'header',
			"footer" => 'footer',
			"mail_type" => \MailType::CRITICAL_TXN
	);
	$multiPartymailer = new \MultiProviderMailer($mailDetails, $customKeywords);
	$flag = $multiPartymailer->sendMail();
	
	EventCreationManager::pushReleaseStatusUpdateEvent($orderid, 'L', "Marking order lost - ReasonType: $reason_type\nReason:$reason", time());
	
	$weblog->info("status: $response[status], refunded amount: $response[refund_amount]");
	return $response;
}

#
# This function performs activities nedded when order is declined
# status may be assign (D)ecline or (F)ail
# (D)ecline order sent mail to customer, (F)ail - not
#
function func_decline_order($orderids, $status = "D") {
	global $config, $mail_smarty;
	global $sql_tbl, $to_customer;

	if (($status != "D") && ($status != "F")) return;

	if (!is_array($orderids))$orderids = array($orderids);

	foreach($orderids as $orderid) {
		#
		# Order decline routine
		#
		$order_data = func_order_data($orderid);
		if (empty($order_data))
			continue;

		$order = $order_data["order"];
		$userinfo = $order_data["userinfo"];
		$products = $order_data["products"];
		$giftcerts = $order_data["giftcerts"];

		# Send mail notifications
		if ($status == "D") {
			$mail_smarty->assign("customer",$userinfo);
			$mail_smarty->assign("products",$products);
			$mail_smarty->assign("giftcerts",$giftcerts);
			$mail_smarty->assign("order",$order);

			if ($config['Email_Note']['eml_order_d_notif_customer'] == 'Y') {
				$to_customer = func_query_first_cell ("SELECT language FROM $sql_tbl[customers] WHERE login='$userinfo[login]'");
				if (empty($to_customer))
					$to_customer = $config['default_customer_language'];

				$mail_smarty->assign("products", func_translate_products($products, $to_customer));
				$userinfo['title'] = func_get_title($userinfo['titleid'], $to_customer);
				$userinfo['b_title'] = func_get_title($userinfo['b_titleid'], $to_customer);
				$userinfo['s_title'] = func_get_title($userinfo['s_titleid'], $to_customer);
				$mail_smarty->assign("customer",$userinfo);
				//func_send_mail($userinfo["email"], "mail/decline_notification_subj.tpl","mail/decline_notification.tpl", $config["Company"]["orders_department"], false);
			}
		}

		#
		# Increase debit for declined GC
		#
		if ($order["applied_giftcerts"]) {
			foreach ($order["applied_giftcerts"] as $k=>$v) {
				if ($order["status"]=="P" || $order["status"]=="C") {
					db_query("UPDATE $sql_tbl[giftcerts] SET debit=debit+'$v[giftcert_cost]' WHERE gcid='$v[giftcert_id]'");
				}

				db_query("UPDATE $sql_tbl[giftcerts] SET status='A' WHERE debit>0 AND gcid='$v[giftcert_id]'");
			}
		}

		# Set GC's status to 'D'
		if ($giftcerts) {
			foreach($giftcerts as $giftcert) {
				db_query("UPDATE $sql_tbl[giftcerts] SET status='D' WHERE gcid='$giftcert[gcid]'");
			}
		}

		if ($config["General"]["unlimited_products"] != "Y") {
			func_update_quantity ($products);
		}
	}

	if (!empty($active_modules["SnS_connector"])) {
		global $HTTP_COOKIE_VARS;

		$_old = $HTTP_COOKIE_VARS;
		$HTTP_COOKIE_VARS['personal_client_id'] = $order['extra']['personal_client_id'];
		func_generate_sns_action("Order", $orderid);
		$HTTP_COOKIE_VARS = $_old;
	}
}

#
# This function performs activities nedded when order status has to be updated 
# from Pre-processed to queued or from pending to queued
#
function func_queued_order($orderid, $status)
{
	global $sql_tbl;
	global $sqllog;
	global $weblog;
	global $XCARTSESSID;
	global $config, $mail_smarty, $active_modules;
	global $xcart_dir, $login;

	$time =time();

	$tb_orders = $sql_tbl["orders"];
	$tb_temp_invoice = $sql_tbl["temp_invoice"];
	$session_id = $XCARTSESSID;

	$amountdetail = func_get_order_amount_details($orderid);
	
	$order_details = func_query_first("select * from xcart_orders where orderid = $orderid",true);
	$amountdetail['zipcode'] = $order_details['s_zipcode'];
	
	$couponCode = $amountdetail['coupon'];
	$coupondiscount = $amountdetail['coupon_discount'];
	$login = $amountdetail['login'];
	$payment_method = $amountdetail['payment_method'];
	########## Total order value ###########33
	$orderTotal = $amountdetail['total'];
	$orgid = $amountdetail['orgid'];
	$cashCouponCode=$amountdetail['cash_coupon_code'];
	$cashdiscount=$amountdetail['cash_redeemed'];
	$isCouponValid = true;
	$isCashValid = true;
	
	if($isCashValid&&$isCouponValid) {
	    // this is for the preprocessed script
		if($status == 'PP' || $status == 'D' || $status == 'PV')
		{
			#Call function to generate the invoive id and update the order status correspondingly
			#
			// determine if cashback should be given on the orderid
			$casback_earnedcredits_enabled = FeatureGateKeyValuePairs::getBoolean('cashback.earnedcredits.enabled',true);
			$process_cashback = true;
			if (!$casback_earnedcredits_enabled) {
				//update cashback_processed flag for the order
				func_array2update('orders',array('cashback_processed' => 1), "orderid=".$orderid);
				$process_cashback = false;
			}
			$invoiceid = func_generate_invoice_id($orderid, "Q", $session_id,$process_cashback);
			#
			#Call function to create the array of all prodcuts of that order
			#
                        
			$productsInCart = func_create_array_products_of_order($orderid);
	    

            $customer = func_customer_order_address_detail($login, $orderid);
            $CustomerFirstName = $customer['firstname'];
            $CustomerLastName = $customer['lastname'];

            //func_order_confirm_mail($payment_method, $productsInCart, $amountdetail, $CustomerFirstName,$login, $orderid, 1, $customer);
         	OrderProcessingMailSender::notifyOrderConfirmation($payment_method,$productsInCart,$amountdetail,$CustomerFirstName,$login,$orderid,$customer);

         	$msg = "Hi ". $CustomerFirstName. ", Your order with order id ".$orderid." has been successfully placed. It will be shipped from our warehouse within the next 24 hours. Thank you for shopping at Myntra";
         	$customerMobileNo = $amountdetail['issues_contact_number'];
            func_send_sms($customerMobileNo,$msg);

		}
		#Send mail to referree about to earned amount
		//func_send_mail_referree_regarding_earned_amount($login, $orderTotal);
		
		// AS part of new wms.. the order should be moved immediately to WP state as there is not going to 
		// be any manual assignment.
		// also move item_status to assign it to OPS Manager which is going to be id 1.
		$weblog->info("Move order to WP state. Current Status - Q.");
		$order_query = "UPDATE $sql_tbl[orders] SET status ='WP' WHERE orderid = $orderid";
		db_query($order_query);
		$order_detail_query = "UPDATE $sql_tbl[order_details] SET item_status = 'A', assignee=1, assignment_time = ".time()." where orderid = $orderid and item_status != 'IC'";
		db_query($order_detail_query);
		func_addComment_status($orderid, "WP", "Q", "Auto moving order to WP state from Q", "AUTOSYSTEM");
		if($status != 'PP' && $status != 'D' && $status != 'PV' && $status != 'F') {
        	EventCreationManager::pushReleaseStatusUpdateEvent($orderid, "WP","Auto moving order to WP state from Q", time(), $login);
        }
		
		return true;
	} else {
		return false;
	}	
}

function func_reject_order($orderid, $status)
{
	global $sql_tbl;
	global $sqllog;
	global $config, $mail_smarty, $active_modules;
	global $xcart_dir;

	$tb_orders = $sql_tbl["orders"];
	
	$sql = "UPDATE $tb_orders SET status='F' WHERE orderid='$orderid'";
	EventCreationManager::pushReleaseStatusUpdateEvent($orderid, "F","Rejecting order", time(), $login);
	$sqllog->debug($sql);
	db_query($sql);
}

//Failed delivery
function func_cod_order_mark_failed_delivery($orderid, $tracking,$courier_code,$reason="Delivery Failed")
{
	global $sql_tbl;
	global $sqllog;	
	
    $time=date("d/m/y : H:i:s");
	$tb_orders = $sql_tbl["orders"];

	$completeddate=time();
    $query_data = array(
                    "tracking"          => $tracking,
                    "status"            => 'FD',
                    "completeddate"     => $completeddate,
                    "courier_service"   => $courier_code
                                );
    func_array2update($sql_tbl['orders'], $query_data, "orderid='$orderid'");

    //Send Reject e-mail.
    $cust_details=func_query_first("select login,firstname,from_unixtime(date) as date from xcart_orders where orderid=$orderid");
    $firstname=$cust_details[firstname];

    $args = array(
					//"ITEM_DETAIL"	=> $item_details,
                    "USER"		=> $firstname,
					"ORDERNO"		=> $orderid,
					"ORDERDATE" 	=> $cust_details['date']
					);
     $subjectarray = array( 	"ORDERNO" 	=> "#".$orderid);
     
     $template="codFailedDelivery";
     
     $emailto=$cust_details[login];
     $from ="MyntraAdmin";
	 sendMessageDynamicSubject($template, $args, $emailto ,$from,$subjectarray);

	$query = "insert into `mk_ordercommentslog` (orderid,commentaddedby,commenttitle,commenttype,description,commentdate) values ($orderid,'Admin','COD_Failure','$tracking','$reason Tracking: $tracking','$time')";
    db_query ($query);

}

function func_cod_cancel_order($orderid)
{
	global $sql_tbl;
	global $sqllog;

    $time=date("d/m/y : H:i:s");

	$tb_orders = $sql_tbl["orders"];

	db_query("UPDATE $tb_orders SET status='F' WHERE orderid='$orderid'");

    //Send Reject e-mail.
    $cust_details=func_query_first("select login,firstname,from_unixtime(date) as date from xcart_orders where orderid=$orderid");
    $firstname=$cust_details[firstname];

    $args = array(
					//"ITEM_DETAIL"	=> $item_details,
                    "USER"		=> $firstname,
					"ORDERNO"		=> $orderid,
					"ORDERDATE" 	=> $cust_details['date']
					);
     $subjectarray = array( 	"ORDERNO" 	=> "#".$orderid);

     $template="codReject";

     $emailto=$cust_details[login];
     $from ="MyntraAdmin";
	 sendMessageDynamicSubject($template, $args, $emailto ,$from,$subjectarray);

	$query = "insert into `mk_ordercommentslog` (orderid,commentaddedby,commenttitle,commenttype,description,commentdate) values ($orderid,'Admin','COD_Failure','$tracking','$reason Tracking: $tracking','$time')";
    db_query ($query);

}

//Get the orderid based on tracking number.
//Note that tracking number could be a part of a comma separated list in the orders table.
function get_orderid_from_tracking($tracking,$courier){
	global $sql_tbl;
	$sql="select orderid from xcart_orders  where courier_service='$courier' and (tracking='$tracking' or tracking like '%,$tracking,%' or tracking like '$tracking,%' or tracking like '%,$tracking')";
	$orderid=func_query_first_cell($sql);
	return $orderid;
}

#
# This function sends GC emails (called from func_place_order
# and provider/order.php"
#
function func_send_gc($from_email, $giftcert, $from_login = '') {
	global $mail_smarty, $config, $to_customer, $sql_tbl;

	$giftcert["purchaser_email"] = $from_email;
	$mail_smarty->assign("giftcert", $giftcert);

	#
	# Send notifs to $orders_department & purchaser
	#
	if (@$config['Gift_Certificates']['eml_giftcert_notif_purchaser'] == 'Y' && (@$config['Gift_Certificates']['eml_giftcert_notif_admin'] != 'Y' || $config["Company"]["orders_department"] != $from_email)) {
		if (!empty($from_login)) {
			$to_customer = func_query_first_cell("SELECT language FROM $sql_tbl[customers] WHERE login = '$from_login'");
			if (empty($to_customer))
				$to_customer = $config['default_customer_language'];
		}

		//func_send_mail($from_email, "mail/giftcert_notification_subj.tpl", "mail/giftcert_notification.tpl", $config["Company"]["orders_department"], false);
	}

	if (@$config['Gift_Certificates']['eml_giftcert_notif_admin'] == 'Y') {
		//func_send_mail($config["Company"]["orders_department"], "mail/giftcert_notification_subj.tpl", "mail/giftcert_notification.tpl", $from_email, true);
	}

	#
	# Send GC to recipient
	#
	$to_customer = '';
	//func_send_mail($giftcert["recipient_email"], "mail/giftcert_subj.tpl", "mail/giftcert.tpl", $from_email, false);
}

#
# Move products back to the inventory
#
function func_update_quantity($products,$increase=true) {
	global $config, $sql_tbl, $active_modules;

	$symbol = ($increase?"+":"-");
	if ($config["General"]["unlimited_products"] != "Y" && is_array($products)) {
		$ids = array();
		foreach ($products as $product) {
			if ($product['product_type'] == 'C' && !empty($active_modules['Product_Configurator']))
				continue;

			$variantid = "";
			if (!empty($active_modules['Product_Options']) && (!empty($product['extra_data']['product_options']) || !empty($product['options']))) {
				$options = (!empty($product['extra_data']['product_options'])?$product['extra_data']['product_options']:$product['options']);
				$variantid = func_get_variantid($options);
			}

			if (!empty($variantid)) {
				db_query("UPDATE $sql_tbl[variants] SET avail=avail$symbol'$product[amount]' WHERE variantid = '$variantid'");
			}
			else {
				db_query("UPDATE $sql_tbl[products] SET avail=avail$symbol'$product[amount]' WHERE productid='$product[productid]'");
			}

			$ids[$product['productid']] = true;
		}

		if (!empty($ids)) {
			func_build_quick_flags(array_keys($ids));
			func_build_quick_prices(array_keys($ids));
		}
	}
}

#
# This function removes orders and related info from the database
# $orders can be: 1) orderid; 2) orders array with orderid keys
function func_delete_order($orders, $update_quantity = true) {
	global $sql_tbl, $xcart_dir;

	$_orders = array();

	if (is_array($orders)) {
		foreach($orders as $order)
			if (!empty($order["orderid"]))
				$_orders[] = $order["orderid"];
	}
	elseif (is_numeric($orders)) {
		$_orders[] = $orders;
	}

	x_log_flag('log_orders_delete', 'ORDERS', "Login: $login\nIP: $REMOTE_ADDR\nOperation: delete orders (".implode(',',$_orders).")", true);
	#
	# Update quantity of products
	#
	if ($update_quantity) {
		foreach($_orders as $orderid) {
			$order_data = func_order_data($orderid);
			if (empty($order_data))
				continue;

			if (strpos("IQ",$order_data["order"]["status"]) !== false) {
				func_update_quantity($order_data["products"]);
			}
		}
	}

	#
	# Delete orders from the database
	#
	$xaff = (func_query_first_cell("SELECT COUNT(*) FROM $sql_tbl[modules] WHERE module_name='XAffiliate'") > 0);
	$xrma = (func_query_first_cell("SELECT COUNT(*) FROM $sql_tbl[modules] WHERE module_name='RMA'") > 0);
	if ($xaff && !isset($sql_tbl['partner_payment'])) {
		@include_once $xcart_dir."/modules/XAffiliate/config.php";
	}

	if ($xrma && !isset($sql_tbl['returns'])) {
		@include_once $xcart_dir."/modules/RMA/config.php";
	}

	db_query("LOCK TABLES $sql_tbl[orders] WRITE, $sql_tbl[order_details] WRITE, $sql_tbl[order_extras] WRITE, $sql_tbl[giftcerts] WRITE, $sql_tbl[subscription_customers] WRITE".(@$xaff?", $sql_tbl[partner_payment] WRITE, $sql_tbl[partner_product_commissions] WRITE, $sql_tbl[partner_adv_orders] WRITE":"").(@$xrma?", $sql_tbl[returns] WRITE":""));

	foreach($_orders as $orderid) {
		$itemids = func_query("SELECT itemid FROM $sql_tbl[order_details] WHERE orderid='$orderid'");
		if(!empty($itemids)) {
			foreach($itemids as $k => $v) {
				$itemids[$k] = $v['itemid'];
			}
		}

		db_query("DELETE FROM $sql_tbl[orders] WHERE orderid='$orderid'");
		db_query("DELETE FROM $sql_tbl[order_details] WHERE orderid='$orderid'");
		db_query("DELETE FROM $sql_tbl[order_extras] WHERE orderid='$orderid'");
		db_query("DELETE FROM $sql_tbl[giftcerts] WHERE orderid='$orderid'");
		if (@$xaff) {
			db_query("DELETE FROM $sql_tbl[partner_payment] WHERE orderid='$orderid'");
			db_query("DELETE FROM $sql_tbl[partner_product_commissions] WHERE orderid='$orderid'");
			db_query("DELETE FROM $sql_tbl[partner_adv_orders] WHERE orderid='$orderid'");
		}

		if (@$xrma && !empty($itemids)) {
			db_query("DELETE FROM $sql_tbl[returns] WHERE itemid IN ('".implode("','", $itemids)."')");
		}

		db_query("DELETE FROM $sql_tbl[subscription_customers] WHERE orderid='$orderid'");
	}

	#
	# Check if no orders in the database
	#
	$total_orders = func_query_first_cell("SELECT COUNT(*) FROM $sql_tbl[orders]");
	if ($total_orders == 0) {
		#
		# Clear Order ID counter (auto increment field in the xcart_orders table)
		#
		db_query("DELETE FROM $sql_tbl[orders]");
		db_query("DELETE FROM $sql_tbl[order_details]");
		if (@$xaff)
			db_query("DELETE FROM $sql_tbl[partner_payment]");

		db_query("DELETE FROM $sql_tbl[subscription_customers]");

	}

	db_query("UNLOCK TABLES");
}

function func_check_merchant_password($config_force = false) {
	global $merchant_password, $current_area, $active_modules, $config;

	return ($merchant_password && ($current_area == 'A' || ($current_area == 'P' && $active_modules["Simple_Mode"])) && ($config['Security']['blowfish_enabled'] == 'Y' || $config_force));
}

#
# This function recrypts data with the Blowfish method.
#
function func_data_recrypt() {
	global $sql_tbl;

	if (!func_check_merchant_password())
		return false;

	$orders = db_query("SELECT orderid, details FROM $sql_tbl[orders] WHERE details NOT LIKE 'C%' AND details != ''");

	if (!$orders)
		return true;

	func_display_service_header("lbl_reencrypting_mkey");
	while ($order = db_fetch_array($orders)) {
		$details = text_decrypt($order['details']);
		$details = (is_string($details)) ? addslashes(func_crypt_order_details($details)) : "";

		func_array2update("orders", array("details" => $details), "orderid = '$order[orderid]'");
		func_flush(". ");
	}

	db_free_result($orders);

	return true;
}

#
# This function decrypts data Blowfish method -> Standart method.
#
function func_data_decrypt() {
	global $sql_tbl;

	if (!func_check_merchant_password(true))
		return false;

	$orders = db_query("SELECT orderid, details FROM $sql_tbl[orders] WHERE details LIKE 'C%'");
	if (!$orders)
		return true;

	func_display_service_header("lbl_reencrypting_skey");
	while ($order = db_fetch_array($orders)) {
		$details = text_decrypt($order['details']);
		$details = is_string($details) ? addslashes(text_crypt($details)) : "";

		func_array2update("orders", array("details" => $details), "orderid = '$order[orderid]'");
		func_flush(". ");
	}

	db_free_result($orders);

	return true;
}

#
# This function recrypts Blowfish-crypted data with new password
# where:
#	old_password - old Merchant password
function func_change_mpassword_recrypt($old_password) {
	global $sql_tbl, $merchant_password;

	if (empty($old_password) || !func_check_merchant_password())
		return false;

	$orders = db_query("SELECT orderid, details FROM $sql_tbl[orders] WHERE details != ''");
	if (!$orders)
		return true;

	$_merchant_password = $merchant_password;
	func_display_service_header("lbl_reencrypting_new_mkey");
	while ($order = db_fetch_array($orders)) {
		$merchant_password = $old_password;
		$details = text_decrypt($order['details']);
		$merchant_password = $_merchant_password;
		$details = is_string($details) ? addslashes(func_crypt_order_details($details)) : "";

		func_array2update("orders", array("details" => $details), "orderid = '$order[orderid]'");

		func_flush(". ");
	}

	db_free_result($orders);

	$merchant_password = $_merchant_password;

	return true;
}

#
# Encryption of the 'details' field of the orders table
#
function func_crypt_order_details($data) {
	if (func_check_merchant_password())
		return text_crypt($data, "C");

	return text_crypt($data);
}

#
# Hash table for func_lock & func_unlock
#
function & func_lock_hash_tbl() {
	static $hash = array();

	return $hash;
}

#
# This function create file lock in temporaly directory
# It will return file descriptor, or false.
#
function func_lock($lockname) {
	global $file_temp_dir;

	if (empty($lockname)) return false;

	$fname = $file_temp_dir.DIRECTORY_SEPARATOR.$lockname;
	$fp = fopen($fname, "w+");
	if (!$fp) return false;

	$attempts = 3;
	while ($attempts > 0) {
		if (flock($fp, LOCK_EX)) {
			$hash =& func_lock_hash_tbl();
			$hash[$fp] = $fname;
			return $fp;
		}
		sleep(5);
		$attempts--;
	}

	return false;
}

#
# This function releases file lock which is previously created by func_lock
#
function func_unlock($fp) {
	global $file_temp_dir;
	$hash =& func_lock_hash_tbl();

	if ($fp === false || empty($hash[$fp])) return false;

	$fname = $hash[$fp];
	unset($hash[$fp]);

	@fclose($fp);
	@unlink($fname);

	return true;
}

#
# Translate products names to local product names
#
function func_translate_products($products, $code) {
	global $sql_tbl;

	if (!is_array($products) || empty($products) || empty($code))
		return $products;

	$hash = array();
	foreach($products as $k => $p) {
		$hash[$p['productid']][] = $k;
	}

	if (empty($hash))
		return $products;

	foreach ($hash as $pid => $keys) {
		$local = func_query_first("SELECT product, descr, fulldescr as fulldescr FROM $sql_tbl[products_lng] WHERE productid = '$pid' AND code = '$code'");
		if (empty($local) || !is_array($local))
			continue;

		foreach($keys as $k) {
			$products[$k] = func_array_merge($products[$k], $local);
		}
	}

	return $products;
}

#
# This function defines internal fields for storing sensitive information in order details
#
function func_order_details_fields($all=false) {
	global $store_cc, $store_ch, $store_cvv2;
	static $all_fields = array (
		"CC" => array (
			"card_name" => "{CardOwner}",
			"card_type" => "{CardType}",
			"card_number" => "{CardNumber}",
			"card_valid_from" => "{ValidFrom}",
			"card_expire" => "{ExpDate}",
			"card_issue_no" => "{IssueNumber}"
		),
		"CC_EXT" => array (
			"card_cvv2" => "CVV2"
		),
		"CH" => array (
			# ACH
			"check_ban" => "{BankAccount}",
			"check_brn" => "{BankNumber}",
			"check_number" => "{FractionNumber}",
			# Direct Debit
			"debit_name" => "{AccountOwner}",
			"debit_bank_account" => "{BankAccount}",
			"debit_bank_number" => "{BankNumber}",
			"debit_bank_name" => "{BankName}"
		)
	);

	$keys = array();
	if ($store_cc || $all) {
		$keys[] = "CC";
		if ($store_cvv2 || $all) $keys[] = "CC_EXT";
	}

	if ($store_ch || $all) $keys[] = "CH";

	$rval = array();
	foreach ($keys as $key) {
		$rval = func_array_merge($rval, $all_fields[$key]);
	}

	return $rval;
}

#
# Convert {CardName} => value of lbl_payment_CardName language variable
#
function func_order_details_fields_as_labels($force=false) {
	$rval = array();
	foreach (func_order_details_fields(true) as $field) {
		if (preg_match('!^\{(.*)\}$!S', $field, $sublabel))
			$rval[$field] = func_get_langvar_by_name('lbl_payment_'.$sublabel[1], NULL, false, $force);
	}

	return $rval;
}

#
# Remove sensitive information from order details
#
function func_order_remove_ccinfo($order_details, $save_4_digits) {
	static $find_re = array (
		1 => array ('/^\{(?:CardOwner|CardType|ExpDate)\}:.*$/mS', '/^CVV2:.*$/mS'),
		0 => array ('/^\{(?:CardOwner|CardType|CardNumber|ExpDate)\}:.*$/mS', '/^CVV2:.*$/mS'),
	);

	$save_4_digits = (int)((bool)$save_4_digits); # can use only 0 & 1

	$order_details = preg_replace($find_re[$save_4_digits], "", $order_details);

	if ($save_4_digits) {
		if (preg_match_all("/^(\{CardNumber\}:)(.*)$/mS", $order_details, $all_matches)) {
			foreach ($all_matches[2] as $matchn => $cardnum) {
				$cardnum = trim($cardnum);
				$order_details = str_replace(
					$all_matches[0][$matchn],
					$all_matches[1][$matchn]." ".str_repeat("*", strlen($cardnum)-4).substr($cardnum, -4),
					$order_details);
			}
		}
	}

	return $order_details;
}

#
# Replace all occurences of {Label} by corresponding language variable
#
function func_order_details_translate($order_details, $force=false) {
	static $labels = array();
	global $shop_language;

	if (empty($labels[$shop_language])) {
		$labels[$shop_language] = func_order_details_fields_as_labels($force);
	}

	$order_details = str_replace(
		array_keys($labels[$shop_language]),
		array_values($labels[$shop_language]),
		$order_details);

	return $order_details;
}

/* get blue dart courier status  and check if a order is delivered or not*/
function func_get_bluedart_paystatus_multiple_awb($awb){
	$bluedartAPI = "http://www.bluedart.com/servlet/RoutingServlet?handler=tnt&action=custawbquery&loginid=BLR00951&awb=awb&format=xml&lickey=319413b2fefe4c53691581e461b04a9e&verno=1.3f*scan=0&numbers=" . $awb;
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $bluedartAPI); 
	curl_setopt($ch, CURLOPT_RETURNTRANSFER,1); 
	$response = curl_exec($ch);
	curl_close($ch); 
	return $response;
	
	/*   Please Note: The blue dart URL gives an XML format output with the following attributes/fields.
	
		[@attributes] => Array
		(
			[WaybillNo] =>
			[RefNo] =>
		)

		[Service] =>
		[PickUpDate] =>
		[Origin] =>
		[Destination] =>
		[ProductType] =>
		[CustomerName] =>
		[SenderName] =>
		[ToAttention] =>
		[Weight] =>
		[Status] =>
		[StatusType] =>
		[ExpectedDeliveryDate] =>
		[expectedDateOfDelivery] =>
	*/
	
}

function get_bluedart_status($awbno) {
	if(!empty($awbno)) {
		$xmlresult = func_get_bluedart_paystatus_multiple_awb($awbno);
	$xmlValidity = simplexml_load_string($xmlresult);
		if(!empty($xmlresult) && ($xmlValidity !== FALSE) ) {
			$bluedartresult = new SimpleXMLElement($xmlresult); 
			
			foreach($bluedartresult as $singleawb) {
				// Note: The Bluedart Status Type is a 2-character field.
				//echo "{$singleawb['WaybillNo']} - {$singleawb->Status} - {$singleawb->StatusType}" . "<br/>";
				return $singleawb->StatusType;
			}
		} 
	}
}

#
#This function returns the payment_method for a particlular order
#
function func_get_order_payment_method($orderid){
	$query="select distinct TRIM(payment_method) from xcart_orders where orderid='$orderid'";
	$query_resultset=db_query($query);
	$query_resultset_row=db_fetch_row($query_resultset);
	$payment_method=$query_resultset_row[0];
	return $payment_method;
}

#
#This function returns the payment_method,status for a particlular order
#
function func_get_order_payment_method_and_status($orderid){
	$query="select distinct TRIM(payment_method) as payment_method,TRIM(status) as status from xcart_orders where orderid=$orderid";
	$query_resultset=func_query($query);
	$info['payment_method']=$query_resultset[0]['payment_method'];
	$info['status']=$query_resultset[0]['status'];
	return $info;
}

function func_get_shipped_orders($params) {
	global $sql_tbl, $sqllog;
	
	$sql = "SELECT SQL_CALC_FOUND_ROWS order_name, payment_method, orderid, login, status, warehouseid, queueddate, shippeddate, delivereddate, completeddate from $sql_tbl[orders] WHERE ";
	
	$order_from_date_break = explode("/", $params['from_date']);
    $from_date = $order_from_date_break[2] . "-" . $order_from_date_break[0] . "-" . $order_from_date_break[1];
    
    $to_date_break = explode("/", $params['to_date']);
    $to_date = $to_date_break[2]."-".$to_date_break[0]."-".$to_date_break[1];
    
    $sql .= " shippeddate >= unix_timestamp('$from_date 00:00:00') AND shippeddate <= unix_timestamp('$to_date 23:59:59')";
    
    if(!empty($params['warehouse_id'])){
    	$sql .= " AND warehouse_id = ".$params['warehouse_id'];
    }
    
    $start = $params['start'] == null ? 0 : $params['start'];
	$limit = $params['limit'] == null ? 30 : $params['limit'];
	$sql .= " LIMIT $start, $limit";
	
	$sqllog->info("Get shipped orders - Query - $sql");
	
    $orders_results = func_query($sql, true);
	$total = func_query("SELECT FOUND_ROWS() total", TRUE);
	
	foreach($orders_results as $index=>$order) {
   		$orders_results[$index]['queueddate'] = $order['queueddate'] != null ? date("d-m-Y H:i:s", $order['queueddate']) : "-";
   		$orders_results[$index]['shippeddate'] = $order['shippeddate'] != null ? date("d-m-Y H:i:s", $order['shippeddate']) : "-";
   		$orders_results[$index]['delivereddate'] = $order['delivereddate'] != null ? date("d-m-Y H:i:s", $order['delivereddate']) : "-";
   		$orders_results[$index]['completeddate'] = $order['completeddate'] != null ? date("d-m-Y H:i:s", $order['completeddate']) : "-";
   	}
   	
   	return array("results" => $orders_results, "count" => $total[0]["total"]);
}

function func_get_cancelled_orders_history($params, $limit_results=TRUE) {
	global $sql_tbl, $sqllog;
	
	$sql = "SELECT SQL_CALC_FOUND_ROWS order_name, payment_method, orderid, subtotal as total, discount, coupon_discount, login, date, canceltime,cancellation_code from $sql_tbl[orders] WHERE (status = 'F' or status = 'D')";
	
	if(!empty($params['orderid'])){
		$sql .= " AND orderid = ".$params['orderid'];
	}
	
	if(!empty($params['payment_method'])){
		if($params['payment_method'] == 'cod') {
			$sql .= " AND payment_method = 'cod'";
		}else{
			$sql .= " AND payment_method != 'cod'";
		}
	}
	
	if(!empty($params['customerlogin'])) {
		$sql .= " AND login = '".$params['customerlogin']."'";
	}
	
	$order_from_date_break = explode("/", $params['from_date']);
    $from_date = $order_from_date_break[2] . "-" . $order_from_date_break[0] . "-" . $order_from_date_break[1];
    
    $to_date_break = explode("/", $params['to_date']);
    $to_date = $to_date_break[2]."-".$to_date_break[0]."-".$to_date_break[1];
    
    $sql .= " AND canceltime >= unix_timestamp('$from_date 00:00:00') AND canceltime  <= unix_timestamp('$to_date 23:59:59')";
    
	if(!empty($params["sort"])) {
		$sql .= " ORDER BY " . $params["sort"];
		$sql .= " " . ($params["dir"] == 'DESC' ? "DESC" : "ASC");
	}

	if($limit_results) {
    	$start = $params['start'] == null ? 0 : $params['start'];
		$limit = $params['limit'] == null ? 30 : $params['limit'];
		$sql .= " LIMIT $start, $limit";
	}
	
	$sqllog->info("Get cancelled orders for - $sql");
	
    $orders_results = func_query($sql, true);
	$total = func_query("SELECT FOUND_ROWS() total", TRUE);

	$cancellation_codes = func_query("select * from cancellation_codes");
	$cc_codes = array();
	
	foreach ($cancellation_codes as $cancellation_code){
		$cc_codes[$cancellation_code['id']] = $cancellation_code['cancel_type']."-".$cancellation_code['cancel_reason'];
	}
	
	foreach ($orders_results as $index=>$order) {
		$orders_results[$index]['final_amount'] = number_format($order['total']-$order['discount'] - $order['coupon_discount'], 2);
		$orders_results[$index]['itemid'] = "-";
		
		$commentsql = "Select description from mk_ordercommentslog WHERE orderid = ".$order['orderid']." AND commenttitle = 'Refund Processed'";
		$refund_comment_result = func_query_first($commentsql);
		if($refund_comment_result) {
			$description = $refund_comment_result['description'];
			$details = explode("\n", $description);
			$refund_details = explode(":", $details[0]);
			$refund_mode_details = explode(":", $details[1]);
			$refund_data_details = explode(":", $details[2]);
			$orders_results[$index]['refund'] = $refund_details[1];
			$orders_results[$index]['refund_mode'] = $refund_mode_details[1];
			if (trim($orders_results[$index]['refund_mode']) == "Coupon"){
				$orders_results[$index]['couponcode'] = $refund_data_details[1];
				$orders_results[$index]['pgname'] = "-";
			} else {
				$orders_results[$index]['pgname'] = $refund_data_details[1];
				$orders_results[$index]['couponcode'] = "-";
			}
		} else {
			$orders_results[$index]['refund'] = "-";
			$orders_results[$index]['refund_mode'] = "-";
		}
		
		$orders_results[$index]['date'] = $order['date'] != null ? date('M jS Y', $order['date']) : "-";
		$orders_results[$index]['canceltime'] = $order['canceltime'] != null ? date('M jS Y', $order['canceltime']) : "-";
		
		//get cancellation reason
		$orders_results[$index]['cancel_reason'] = $cc_codes[$order['cancellation_code']];  
	}
	
	return array("results" => $orders_results, "count" => $total[0]["total"]);
}

function func_get_cancelled_items_history($params, $limit_results=true) {
	global $sql_tbl, $sqllog;
	
	$sql = "SELECT SQL_CALC_FOUND_ROWS o.order_name, o.payment_method, od.itemid, od.orderid, (od.price*od.amount) as total, od.discount, od.coupon_discount_product as coupon_discount, o.login, o.date, od.canceltime,od.cancellation_code from $sql_tbl[orders] o JOIN $sql_tbl[order_details] od ON o.orderid = od.orderid WHERE od.item_status = 'IC'";
	
	if(!empty($params['orderid'])){
		$sql .= " AND od.orderid = ".$params['orderid'];
	}
	
	if(!empty($params['payment_method'])){
		if($params['payment_method'] == 'cod') {
			$sql .= " AND o.payment_method = 'cod'";
		}else{
			$sql .= " AND o.payment_method != 'cod'";
		}
	}
	
	if(!empty($params['customerlogin'])) {
		$sql .= " AND o.login = '".$params['customerlogin']."'";
	}
	
	$order_from_date_break = explode("/", $params['from_date']);
    $from_date = $order_from_date_break[2] . "-" . $order_from_date_break[0] . "-" . $order_from_date_break[1];
    
    $to_date_break = explode("/", $params['to_date']);
    $to_date = $to_date_break[2]."-".$to_date_break[0]."-".$to_date_break[1];
    
    $sql .= " AND od.canceltime >= unix_timestamp('$from_date 00:00:00') AND od.canceltime  <= unix_timestamp('$to_date 23:59:59')";
    
	if(!empty($params["sort"])) {
		$sql .= " ORDER BY " . $params["sort"];
		$sql .= " " . ($params["dir"] == 'DESC' ? "DESC" : "ASC");
	}

	if($limit_results) {
        $start = $params['start'] == null ? 0 : $params['start'];
		$limit = $params['limit'] == null ? 30 : $params['limit'];
		$sql .= " LIMIT $start, $limit";
	}
	
	$sqllog->info("Get cancelled orders for - $sql");
	
    $orders_results = func_query($sql, true);
	$total = func_query("SELECT FOUND_ROWS() total", TRUE);
	
	$cancellation_codes = func_query("select * from cancellation_codes");
	$cc_codes = array();
	
	foreach ($cancellation_codes as $cancellation_code){
		$cc_codes[$cancellation_code['id']] = $cancellation_code['cancel_type']."-".$cancellation_code['cancel_reason'];
	}
	
	foreach ($orders_results as $index=>$order) {
		$orders_results[$index]['final_amount'] = number_format($order['total']-$order['discount'] - $order['coupon_discount'], 2);
		$commentsql = "Select description from mk_ordercommentslog WHERE orderid = ".$order['orderid']." AND commenttitle = 'Refund Processed' and description like '%".$order['itemid']."%'";
		$refund_comment_result = func_query_first($commentsql);
		if($refund_comment_result) {
			$description = $refund_comment_result['description'];
			$details = explode("\n", $description);
			$itemid_details = explode(":", $details[0]);
			$refund_details = explode(":", $details[1]);
			$refund_mode_details = explode(":", $details[2]);
			$refund_data_details = explode(":", $details[3]);
			if(trim($itemid_details[1]) == $order['itemid']) {
				$orders_results[$index]['refund'] = $refund_details[1];
				$orders_results[$index]['refund_mode'] = $refund_mode_details[1];
			} else {
				$orders_results[$index]['refund'] = "-";
				$orders_results[$index]['refund_mode'] = "-";
			}
			if (trim($orders_results[$index]['refund_mode']) == "Coupon"){
				$orders_results[$index]['couponcode'] = $refund_data_details[1];
				$orders_results[$index]['pgname'] = "-";
			} else {
				$orders_results[$index]['pgname'] = $refund_data_details[1];
				$orders_results[$index]['couponcode'] = "-";
			}
		} else {
			$orders_results[$index]['refund'] = "-";
			$orders_results[$index]['refund_mode'] = "-";
		}
		$orders_results[$index]['date'] = $order['date'] != null ? date('M jS Y', $order['date']) : "-";
		$orders_results[$index]['canceltime'] = $order['canceltime'] != null ? date('M jS Y', $order['canceltime']) : "-";
		
		//get cancellation reason
		$orders_results[$index]['cancel_reason'] = $cc_codes[$order['cancellation_code']];;
	}
	
	return array("results" => $orders_results, "count" => $total[0]["total"]);
}

function func_get_cancelled_orders($request, $limit_results=true) {
	if($request['cancellation_type'] == 'F') {
		$return_arr = func_get_cancelled_orders_history($request, $limit_results);
	} else {
		$return_arr = func_get_cancelled_items_history($request, $limit_results);
	}
	
	return $return_arr;
}

function func_create_results_file($data, $sheetName=false) {
	global $xcart_dir, $weblog;
	
	$objPHPExcel = new PHPExcel();
	$objPHPExcel = func_write_data_to_excel($objPHPExcel, $data, 0, $sheetName);
	$weblog->info("File written");
	return 	$objPHPExcel;
}

function func_save_results_file($objPHPExcel, $fileprefix) {
	global $xcart_dir, $weblog;
	
	$filepath = "$xcart_dir/bulkorderupdates/";
	$filename = "$fileprefix-".date("dmy").".xls";
	try {
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->setPreCalculateFormulas(false);
		$objWriter->save($filepath.$filename);
		$weblog->info("File saved");
	}catch(Exception $ex) {
		$weblog->info("Error Occured = ". $ex->getMessage());
	}
	
	return $filename;
}

function func_send_results_file_mail($request) {
	global $xcart_dir;		
	$return_arr = func_get_cancelled_orders($request, false);
	
	if($return_arr['count'] == 0) {
		return array('FAILURE', 'No records found for your search. No mail sent!');
	}
	$objPHPExcel = func_create_results_file($return_arr['results']);
	$filename = func_save_results_file($objPHPExcel, "cancellations");
	$body = "";
	if($request['cancellation_type'] == 'F') {
		$body = "Attached report for full order cancellations";
	} else {
		$body = "Attached report for part cancellations";
	}
	$body .= " performed between ".$request['from_date']." and ".$request['to_date'];
	$status =  func_send_report_mail('Cancelled Orders Search Report', $body, "$xcart_dir/bulkorderupdates/", $filename, 'cancelorders');
	if($status===true) {
		return array("SUCCESS");
	} else {
		return array("FAILURE", "Failed to send mail! Please try again later");			
	}
}

function func_send_bulk_cancellation_report($cancelledOrderids, $invalidOrderIds, $login) {
	global $sql_tbl, $xcart_dir;
	if($cancelledOrderids && count($cancelledOrderids) > 0) {
		$query = "SELECT order_name, payment_method, orderid, subtotal as total, discount, cart_discount, coupon_discount, login, date, canceltime from $sql_tbl[orders] WHERE orderid in (".implode(",", $cancelledOrderids).")";
		$orders_results = func_query($query);
		foreach ($orders_results as $index=>$order) {
			$orders_results[$index]['refund'] = "-";
			$orders_results[$index]['refund_mode'] = "-";
			$orders_results[$index]['date'] = $order['date'] != null ? date('M jS Y', $order['date']) : "-";
			$orders_results[$index]['canceltime'] = $order['canceltime'] != null ? date('M jS Y', $order['canceltime']) : "-";
		}
		
		$objPHPExcel = func_create_results_file($orders_results, 'Cancelled Orders');
		if($invalidOrderIds && count($invalidOrderIds) > 0) {
			$objPHPExcel = func_write_data_to_excel($objPHPExcel, $invalidOrderIds, 1, 'Invalid OrderIds', false, 'Order Id');
		}
	} else {
		if($invalidOrderIds && count($invalidOrderIds) > 0) {
			$objPHPExcel = new PHPExcel();
			$objPHPExcel = func_write_data_to_excel($objPHPExcel, $invalidOrderIds, 0, 'Invalid OrderIds', false, 'Order Id');
		}
	}
	if($objPHPExcel) {
		$filename = func_save_results_file($objPHPExcel, 'cancellations');
		$status =  func_send_report_mail('Bulk Cancelled Orders Report', "Attached is the report of bulk cancellation operation performed by $login", "$xcart_dir/bulkorderupdates/", $filename, 'cancelorders');
		return $status;
	}
	return true;
}

function func_send_report_mail($subject, $body, $reportfilepath, $reportfilename, $reporttype) {
	$report_email_to = WidgetKeyValuePairs::getWidgetValueForKey($reporttype."reportemailto");
	$report_email_cc = WidgetKeyValuePairs::getWidgetValueForKey($reporttype."reportemailcc");
	$ccList = explode(",", $report_email_cc);
	$status = func_send_mail_with_attachment($report_email_to, $subject, $body, $ccList, 'MyntraAdmin', $reportfilepath, $reportfilename); 
	return $status;
}

function func_get_OOS_orders($params, $limit_results = true) {
	global $sql_tbl, $sqllog;
	
	$query = "SELECT SQL_CALC_FOUND_ROWS o.order_name, o.qtyInOrder, o.payment_method,o.payment_surcharge, o.orderid, od.itemid, od.price, od.amount as qty, 
			  od.discount, od.cart_discount_split_on_ratio as cart_discount, od.coupon_discount_product, od.pg_discount, od.cash_redeemed, od.pg_discount, o.status, o.login, o.date, o.queueddate, som.sku_id
              from $sql_tbl[orders] o, $sql_tbl[order_details] od, mk_order_item_option_quantity oq, mk_styles_options_skus_mapping som
              WHERE o.orderid = od.orderid and od.itemid = oq.itemid and oq.optionid = som.option_id AND o.status in ('Q', 'WP', 'OH') and od.item_status != 'IC' ";
	
	$order_from_date_break = explode("/", $params['from_date']);
	$from_date = $order_from_date_break[2] . "-" . $order_from_date_break[0] . "-" . $order_from_date_break[1];
    
	$to_date_break = explode("/", $params['to_date']);
	$to_date = $to_date_break[2]."-".$to_date_break[0]."-".$to_date_break[1];
    
	$query .= " AND o.date >= unix_timestamp('$from_date 00:00:00') AND o.date  <= unix_timestamp('$to_date 23:59:59')";
	$query .= " group by itemid";
	
	$sort_map_arr = array("itemid" => "od.itemid",
		"orderid" => "o.orderid",
		"payment_method" => "o.payment_method",
		"order_name" => "o.order_name",
		"login" => "o.login",
		"date" => "o.date",
		"queueddate" => "o.queueddate",
	);
	
	$query .= " ORDER BY " . ($params["sort"] != "" ? $sort_map_arr[$params["sort"]] : "o.orderid");
	$query .= " " . ($params["dir"] == 'DESC' ? "DESC" : "ASC");
	
	if($limit_results) {
        $start = $params['start'] == null ? 0 : $params['start'];
		$limit = $params['limit'] == null ? 30 : $params['limit'];
		$query .= " LIMIT $start, $limit";
	}
	
	$sqllog->info("OOS Orders Query = ". $query);
	
    $orders_results = func_query($query, true);
    
	$total = func_query("SELECT FOUND_ROWS() total", TRUE);
	
	//get issued items for all orders
	
	// get items for all skus which are in STORED state..
	
		
	foreach($orders_results as $index=>$order) {
		$orders_results[$index]['date'] = $order['date'] != null ? date("d-m-Y H:i:s", $order['date']) : "-";
   		$orders_results[$index]['queueddate'] = $order['queueddate'] != null ? date("d-m-Y H:i:s", $order['queueddate']) : "-";
   		$amount_paid = ($order['price']*$order['qty'] + $order['payment_surcharge'] - $order['discount'] - $order['cart_discount'] - $order['coupon_discount_product'] - $order['cash_redeemed'] - $order['pg_discount']);
   		$orders_results[$index]['amount_paid'] = number_format($amount_paid, 2);
	}
   	
    return array("results" => $orders_results, "count" => $total[0]["total"]);
}

function autoCourierAssignment($whId, $inputOrderids) {
	global $weblog;
	global $sql_tbl;
	
	if(!is_array($inputOrderids)){
		$inputOrderids = array($inputOrderids);
	}
	if(!$inputOrderids || count($inputOrderids)==0){
		$weblog->info("Order id list is empty. Returning without courier assignment.");
		return;			
	}
	$weblog->debug("Starting courier assignment for order ids: " . implode(',', $inputOrderids));

	$orders_sql = "SELECT orderid, ordertype, s_zipcode, s_country, payment_method, total,courier_service from $sql_tbl[orders] where orderid in (".implode(",", $inputOrderids).") and (courier_service is null or courier_service = '') and (tracking is null or tracking = '')";
	$orders = func_query($orders_sql, true);
	if($orders && count($orders)>0) {
		foreach ($orders as $order) {
			$isExchange = $order['ordertype'] == 'ex' ? 'true' : 'false';
			$trackingResponse = CourierApiClient::getTrackingNumberForPincode($whId, trim($order['s_zipcode']), strtolower($order['payment_method'])=='cod'?($order['total'] > 0.00 ? 'true':'false'):'false', $isExchange);
			$weblog->info("Order id: ". $order['orderid']. " Zipcode: " . $order['s_zipcode']. " Tracking Response = ". print_r($trackingResponse, true));
			if($trackingResponse && !empty($trackingResponse['trackingNumber']) && !empty($trackingResponse['trackingNumber'][0])){
				$query_data = array("tracking"=> $trackingResponse['trackingNumber'][0], 'courier_service'=>$trackingResponse['courierOperator'][0]);				
				func_array2update($sql_tbl['orders'], $query_data, "orderid='".$order['orderid']."'");
				func_addComment_order($order['orderid'], "LMS", "Automated", "Courier Assignment", 
					"Order assigned to courier: " .$query_data["courier_service"]. " Trk#: " . $query_data["tracking"]);
				EventCreationManager::pushReleaseUpdateEvent($order['orderid'], "LMS", $trackingResponse['courierOperator'][0],$trackingResponse['trackingNumber'][0]);
			}else{
				$weblog->warn("Order id: ". $order['orderid']. " Zipcode: " . $order['s_zipcode']. " : No tracking number found");
				func_addComment_order($order['orderid'], "LMS", "Automated", "Courier Assignment","No tracking number found for pincode - ".$order['s_zipcode']);									
			}
		}
	}
	$weblog->debug("Ending courier assignment for order ids: " . implode(',', $inputOrderids));
}

function autoTrackingNoAssignment($inputOrderids) {
	global $weblog, $sql_tbl;
    $orderid2TrackingNo = array();
	if(!is_array($inputOrderids)){
		$inputOrderids = array($inputOrderids);
	}
	if(!$inputOrderids || count($inputOrderids)==0){
		$weblog->info("Order id list is empty. Returning without tracking no assignment.");
		return;
	}
	$weblog->debug("Starting tracking no assignment for order ids: " . implode(',', $inputOrderids));

	$orders_sql = "SELECT orderid, s_zipcode, courier_service, warehouseid, payment_method, shipping_method, 
            concat(s_firstname,s_lastname) as receiverName, s_address, s_locality, s_city, s_state, s_country, mobile, s_email, total, subtotal
            from $sql_tbl[orders] where orderid in (".implode(",", $inputOrderids).") and (tracking is null or tracking = '')";
	$orders = func_query($orders_sql);
	if($orders && count($orders)>0) {
		foreach ($orders as $order) {
            if($order['courier_service'] != 'FD') {
                $trackingResponse = CourierApiClient::getTrackingNumberForCourier($order['courier_service'], $order['warehouseid'], $order['payment_method']=='cod'?'true':'false', $order['s_zipcode']);
                if($trackingResponse && !empty($trackingResponse['trackingNumber']) && !empty($trackingResponse['trackingNumber'][0])){
                    $orderid2TrackingNo[$order['orderid']] = $trackingResponse['trackingNumber'][0];
                    $update_data = array("tracking"=> $trackingResponse['trackingNumber'][0]);
                    func_array2update('xcart_orders', $update_data, "orderid=".$order['orderid']);
                    func_addComment_order($order['orderid'], "LMS", "Automated", "Tracking No Assignment", "Order assigned Tracking #: " . $update_data["tracking"]);
                    EventCreationManager::pushReleaseUpdateEvent($order['orderid'], "LMS", $order['courier_service'], $update_data['tracking']);
                }else{
                    $weblog->info("Order id: ".$order['orderid']." No tracking number found");
                    func_addComment_order($order['orderid'], "LMS", "Automated", "Tracking No Assignment", "No tracking number found");
                }
            } else {
                EventCreationManager::pushFedexReleaseEvent($order);
            }
		}
	}
    return $orderid2TrackingNo;
}

function func_update_courier_change($orderId, $courierCode, $trackingNo){
        global $sql_tbl;
        $query_data = array("tracking" => $trackingNo, "courier_service" => $courierCode,);
        return func_array2update($sql_tbl['orders'], $query_data, "orderid='$orderId'");
}
	
function updateOrderTracking($orderId, $adminUser, $adminComment, $courier_code=false, $buffer_tracking=false, $notifyCustomer = true){
	global $weblog;
	global $sql_tbl;
	global $xcart_dir;

	$order = func_select_order($orderId);
	if($order) {
		if($order['status'] != 'PK' && $order['status'] != 'WP'){
			return "Status=" . $order['status'];
		}
		
	    $weblog->debug("Valid order: " . $orderId);
	    $payment_method = $order['payment_method'];
	    $trackingOrderNo = $buffer_tracking;
	        
            if($buffer_tracking) {
			$trackingOrderNo = $buffer_tracking;
	    } else {
	    	$trackingOrderNo = $order['tracking'];
	    }
	    if($courier_code) {
	      	$orderCourier = $courier_code;
	    } else {
	      	$orderCourier = $order['courier_service'];
	    }
	       
	    if(empty($trackingOrderNo) || empty($orderCourier) ){
	    	return "Courier Service or Tracking no is not set";
	    }
				
	    $comment = "Order Shipped through $orderCourier with tracking code $trackingOrderNo. ".$adminComment;
	    if( ($orderCourier != $order['courier_service']) || ($trackingOrderNo != $order['tracking']) )
	    {
	    	$query_data = array("tracking" => $trackingOrderNo , "courier_service" => $orderCourier);
	    }
	    func_array2update($sql_tbl['orders'], $query_data, "orderid='$orderId'");
	    
	    func_change_order_status($orderId, 'SH', $adminUser, $comment, "", false, '',false,false,'',$notifyCustomer);
		
	    $weblog->info("Adding record for shipment tracking orderID: " . $orderId . " Tracking No.: " . $trackingOrderNo . " Courier Operator: " . $orderCourier);
	    trackOrder($orderId, $trackingOrderNo, $orderCourier);
    }else{
    	return "Order not found.";
    }

    return true;
}

function check_order_onhold_rules($orderid, $login, $paymentMethod, $order_channel='web', $myntCashBackUsed=0.00, $gateway=false, $gateway_payment_id=false, $checkIntlCard=false, $gatewayWentTo=false, $binNumber=false, $axisBankDAO=false) {
	global $weblog;
	$orderHasCustomizedProduct = false;
	$codOnHoldFeatureGatewayStatus = false;
	$response = array('sub_reason'=>false);
	if($paymentMethod == "chq") {
		$response['status'] = "PD";
	} else if($paymentMethod == "cod") {
		$codOnHoldFeatureGatewayStatus = FeatureGateKeyValuePairs::getFeatureGateValueForKey('codOnHold');
		$conditionalCodOnHold = FeatureGateKeyValuePairs::getFeatureGateValueForKey('conditionalCodOnHold');
		$weblog->debug("conditionalCodOnHold --- $conditionalCodOnHold");
		if($codOnHoldFeatureGatewayStatus=='true') {
			$response['status'] = "OH";
			$response['reason'] = 'CVP';
			$response['sub_reason'] = 'ALL';
			$response['description'] = "putting the order on hold as it is cod order and for reason : all cod on hold feature";
			$response['verificationRequired'] = true;
		}elseif($conditionalCodOnHold == 'true'){
			if($order_channel==='telesales'){
				// order is placed by admin.. no need to check for on hold rules
				$response['status'] = "Q";
			} else {	
				$put_onhold = check_if_cod_order_be_put_on_hold($login, $orderid);
				if($put_onhold['on_hold']) {
					$sql = "select * from order_oh_reasons_master where reason_code = 'CVP' and subreason_code = '".$put_onhold['reason']."'";
					$reason_text = func_query_first($sql, true);
					$response['status'] = "OH";
					$response['reason'] = 'CVP';
					$response['sub_reason'] = $put_onhold['reason'];
					$response['description'] = "putting the order on hold as it is cod order and for reason : ".$reason_text['subreason_desc'];
					$response['verificationRequired'] = true;
				}
			}
		}
	} else if ($paymentMethod == "NP") {
		$response['status'] = "Q";
	}else{
		if($gateway && $gateway->isPaymentFlagged()){
			//set status on hold because the transaction is marked as flagged by EBS
			$response['status'] = "OH";
			$response['reason'] = 'FRI';
			$response['sub_reason'] = 'PGF';
			$response['description'] = "putting the order on hold as payment is flagged at ebs";
			$response['verificationRequired'] = false;
			func_ebs_flagged_put_on_hold_mail($orderid, $gateway_payment_id);
		} else {
			$response['status'] = "Q";
		}
	}
	
	$fraudMatch = func_query_first("select * from ( select o.orderid,o.total, o.shipping_cost,o.gift_charges, 
            o.payment_surcharge, o.cash_redeemed, o.loyalty_points_used, o.loyalty_points_conversion_factor, 
            sum(od.total_amount + od.taxamount - od.coupon_discount_product - od.pg_discount - o.loyalty_points_conversion_factor * od.item_loyalty_points_used) total_oi 
            from xcart_order_details od,xcart_orders o where o.orderid=od.orderid and  od.item_status!='IC' and od.orderid='$orderid') t where 
            abs( (CASE when (t.total-t.shipping_cost-t.gift_charges+t.cash_redeemed) < 0 then 0 
            else (t.total-t.shipping_cost-t.gift_charges+t.cash_redeemed) end) - abs(t.total_oi))>1");
	
	if(!empty($fraudMatch)){
		global $fraudOrderLog, $XCART_SESSION_VARS;
		$fraudOrderLog->debug("MKORDERBOOK : ORDER: $orderid Detected as fraud ");
		$fraudOrderLog->debug("MKORDERBOOK : ORDER:$orderid SESSIONDUMP{".serialize($XCART_SESSION_VARS)."} ");
				
		$response['status'] = 'OH';
		$response['reason'] = 'FRI';
		$response['sub_reason'] = 'UAPM';
		$response['description'] = 'putting order on hold because of mismatch between price paid by customer and the sum of prices of all items';
		$response['verificationRequired'] = false;
	}
	
	if($checkIntlCard) {
		$intlCardPayment = check_international_payment($orderid, $gatewayWentTo, $binNumber, $axisBankDAO);
		if($intlCardPayment) {
			$response['status'] = 'OH';
			$response['reason'] = 'FRI';
			$response['sub_reason'] = 'INO';
			$response['description'] = "Putting order onhold as it is an international credit card transaction";
			$response['verificationRequired'] = false;
		}
	}

	if ($order_channel==='telesalen' || $order_channel==='telesales') {
		if($myntCashBackUsed>0){
			$response['status'] = 'OH';
			$response['reason'] = 'TEL';
			$response['description'] = "Putting order on hold. Cashback is used on order from telesalen channel.";
			$response['verificationRequired'] = false;
		}
	}
	
	return $response;
	
}

function check_international_payment($orderid, $gatewayWentTo, $binNumber, $axisBankDAO) {
	if($gatewayWentTo==Gateways::AxisBank) {
		$putInternationalTransactionOnHold = FeatureGateKeyValuePairs::getBoolean('payments.intlTransOnHold',false);
		$vpc_verStatus3DS = $axisBankDAO->getVer3DSStatus();
		if($putInternationalTransactionOnHold && !bintest($binNumber)&&$vpc_verStatus3DS!='Y') {
			
			$vpc_receiptNo = $axisBankDAO->getReceiptNumber();
			$vpc_3DSStatusDescription = $axisBankDAO->get3DSStatusDescription();
			$vpc_secureLevel3DS = $axisBankDAO->get3DSSecurityLevel();
			$vpc_enrolled3DS = $axisBankDAO->get3DSEnrolled();
			$vpc_status3DS = $axisBankDAO->get3DSStatus();
			$vpc_AVSResultCode = $axisBankDAO->getAVSResultCode();
			$vpc_AVSResultDescription = $axisBankDAO->getAVSResultDescription();
			$vpc_vCSCResultCode = $axisBankDAO->getCSCResultCode();
			$vpc_CSCResponseDescription = $axisBankDAO->getCSCDescription();
			
			$international_trans_on_hold_mail_body = "This order is put on HOLD as it is a international transaction. Please do a verification and queue the order manually.<br/>
			Futher details are: <br/>
			Order ID: $orderid <br/>
			Customer Name: $customerFName <br/>
			RRN (Reciept Number) : $vpc_receiptNo <br/>
			3DS Verification Status: $vpc_verStatus3DS <br/>
			3DS Verification Description: $vpc_3DSStatusDescription <br/>
			ECI Code: $vpc_secureLevel3DS <br/>
			Card 3DSEnrolled: $vpc_enrolled3DS <br/>
			3DS Status: $vpc_status3DS <br/>
			AddressVerificationCode: $vpc_AVSResultCode <br/>
			AddressVerificationResult: $vpc_AVSResultDescription <br/>
			Card Security Code Match Status: $vpc_vCSCResultCode <br/>
			Card Security Code Match Description: $vpc_CSCResponseDescription <br/>
			For even futher details about the codes refer to Mastercard VPC Integration Reference";
			
			global $configMode;	
			if($configMode=="release"||$configMode=="test" ||$configMode=="local") {
				$sendIntTransMailTo = "engg_qa@myntra.com";
			} else {
				$sendIntTransMailTo = "payments.team@myntra.com";
			}
			
			$mail_details = array( "to" => "$sendIntTransMailTo", "subject" => "International Transaction: $orderid", 
					"content" => $international_trans_on_hold_mail_body, "mail_type" => MailType::CRITICAL_TXN);
			$multiPartymailer = new MultiProviderMailer($mail_details);
			$multiPartymailer->sendMail();
			
			return true;
		}
	}
	
	return false;
}

function check_if_cod_order_be_put_on_hold($login, $orderid) {
	
	$put_onhold = evaluate_cod_oh_conditions($login,$orderid);
	
	if($put_onhold['on_hold']) {
		//{"0-8":"20","8-16":"40","16-24":"40"}
		$totalOrdersPushLimit = FeatureGateKeyValuePairs::getFeatureGateValueForKey('condCODOH.orders.daily.limit');
		$orderOnHoldHourlyLimit = FeatureGateKeyValuePairs::getFeatureGateValueForKey('condCODOH.orders.hourly.percentage.limit');
		
		if($orderOnHoldHourlyLimit && !empty($orderOnHoldHourlyLimit)) {
			$hours2OrdersCountMapping = json_decode($orderOnHoldHourlyLimit, true);
			foreach($hours2OrdersCountMapping as $hourRange=>$percent) {
				$hours = explode("-", $hourRange);
				$currentHour = date('H');
				if($currentHour >= intval($hours[0]) && $currentHour < intval($hours[1]) ) {
					//check the counter...
					$ohOrdersCountRow = func_query_first("Select id, no_of_orders from daily_cod_oh_orders_breakup where order_date = '".date('Y-m-d')."' and start_hour = ".$hours[0]." and end_hour = ".$hours[1]);
					if($ohOrdersCountRow) {
						if($ohOrdersCountRow['no_of_orders'] >= $percent*$totalOrdersPushLimit/100) {
							$return_array['on_hold'] = false;
							return $return_array;
						}
					} else {
						func_array2insert('daily_cod_oh_orders_breakup', array('order_date'=>date('Y-m-d'), 'start_hour'=>$hours[0], 'end_hour'=>$hours[1]));
					}
					db_query("update daily_cod_oh_orders_breakup set no_of_orders = no_of_orders + 1 where order_date = '".date('Y-m-d')."' and start_hour = ".$hours[0]." and end_hour = ".$hours[1]);
					break;
				}
			}
		}
		MinacsApiClient::pushOnHoldOrder($orderid, $put_onhold['reason']);
	}
	
	return $put_onhold;
}


function evaluate_cod_oh_conditions($login,$orderid){
	global $weblog;
	$return_array = array();
	
	$suspiciousAddressLength = WidgetKeyValuePairs::getWidgetValueForKey('condCODOH.suspicious.address.length');
	if($suspiciousAddressLength && !empty($suspiciousAddressLength)){
		$shippingAddress = func_query_first_cell("select s_address from xcart_orders where orderid = ".$orderid);
		if(strlen($shippingAddress) < $suspiciousAddressLength){
			$return_array['on_hold'] = true;
			$return_array['reason'] =  "SAL";
			return $return_array;
		}
	}
	
	$zipcodesPricePointsForOnHold = trim(WidgetKeyValuePairs::getWidgetValueForKey('codOH.zipcodes.price.points'));
	if(!empty($zipcodesPricePointsForOnHold)){
		$zipcodesPricePoints = json_decode($zipcodesPricePointsForOnHold,true);
		$zipcodes = implode(",", $zipcodesPricePoints['zipcodes']);
		$pricerange = implode(" or ", $zipcodesPricePoints['pricerange']);
		$order = func_query_first("select * from xcart_orders where orderid = $orderid and s_zipcode in ($zipcodes) and ($pricerange)");
		if($order)  {
			$return_array['on_hold'] = true;
			$return_array['reason'] =  "ZPPM";
			return $return_array;
		}
	}
	
	$statesForFirstPurchaseOnHold = trim(WidgetKeyValuePairs::getWidgetValueForKey('codOH.firstpurchase.states'));
	if(!empty($statesForFirstPurchaseOnHold)){
		$old_orders = func_query("select orderid from xcart_orders where orderid < $orderid and login = '$login' and queueddate is not null and queueddate !=0 and queueddate != ''");
		// If it is the first cod order of the customer.. check if it is from states
		if(!$old_orders) {
			$order = func_query_first("select * from xcart_orders where orderid =$orderid");
			$states = explode(",", $statesForFirstPurchaseOnHold);
			if(in_array($order['s_state'], $states)) {
				$return_array['on_hold'] = true;
				$return_array['reason'] =  "FCPS"; 
				return $return_array;
			}
		}
	}
	
	if(WidgetKeyValuePairs::getWidgetValueForKey('condCODOH.pincode') == 'true'){
		$order = func_query_first("select * from xcart_orders where orderid =$orderid");
		//pin code
		$first2digits = intval($order["s_zipcode"]/10000);
		if($first2digits >=90 & $first2digits < 100){
			// do nothing
			$weblog->debug("army zip code");
		}else{
			$validationquery = "select count(1) as cnt from valid_pin_combinations where stateCode = '".$order["s_state"]."' and first_2_digits = ".$first2digits;
			$res = func_query($validationquery);
			if($res[0]["cnt"] == 0){
				$return_array['on_hold'] = true;
				$return_array['reason'] =  "SPM"; 
				return $return_array;
			}	
		}	
	}
	
	
	if(WidgetKeyValuePairs::getWidgetValueForKey('condCODOH.nocompleteorders') == 'true'){
		// no completed orders
		$sql = "select count(1) as count from xcart_orders where login = '".$login."' and status in ('DL','C')";
		$result = func_query_first($sql,true);
		if($result['count'] == 0){
			$return_array['on_hold'] = true;
			$return_array['reason'] = 'NC'; 
			return $return_array;
		}
	}
	
	if(WidgetKeyValuePairs::getWidgetValueForKey('condCODOH.anyoflastnordersrto') == 'true'){
		$limit = WidgetKeyValuePairs::getWidgetValueForKey('condCODOH.anyoflastnordersrto.nooforders');
		
		$sql = "select count(1) as count from (select orderid from xcart_orders where login = '$login' and orderid != $orderid and status not in ('PP','PV') order by orderid desc limit 0,$limit) a, mk_old_returns_tracking rto where rto.returntype = 'RTO' and rto.orderid = a.orderid";
		$result = func_query_first($sql);
		if($result['count'] > 0){
			$return_array['on_hold'] = true;
			$return_array['reason'] = "LNRTO"; 
			return $return_array;
		}
	}
	
	
	if(WidgetKeyValuePairs::getWidgetValueForKey('condCODOH.lastnordersincomplete') == 'true'){
		$limit = WidgetKeyValuePairs::getWidgetValueForKey('condCODOH.lastnordersincomp.nooforders');
		
		$sql = "select orderid,status from xcart_orders where login = '$login' and orderid != $orderid and status not in ('PP','PV') order by orderid desc limit 0,$limit";
		$result = func_query($sql);
		global $weblog;
		if($result && count($result) >= $limit){
			$count = 0;
			foreach ($result as $row){
				$weblog->debug($row['status']);
				if($row['status'] != 'PP' && $row['status'] != 'PV'){
					if($row['status'] == 'DL' || $row['status'] == 'SH' || $row['status'] == 'C'){
						$count++;
					}
				}
			}
			$weblog->debug("count $count");
			if($count == 0){
				$return_array['on_hold'] = true;
				$return_array['reason'] = "NINC"; 
				return $return_array;
			}	
		}
	}
	
	if(WidgetKeyValuePairs::getWidgetValueForKey('condCODOH.anyoflastnordersohtrackedrto') == 'true'){
		$limit = WidgetKeyValuePairs::getWidgetValueForKey('condCODOH.trackedandrto.nooforders');
		
		$sql = "select count(1) as count from (select orderid from xcart_orders where login = '$login' and orderid != $orderid and status not in ('PP','PV') order by orderid desc limit 0,$limit) a, cod_oh_orders_dispostions codd, mk_old_returns_tracking rto where rto.returntype = 'RTO' and codd.orderid = a.orderid and rto.orderid = a.orderid";
		$result = func_query_first($sql);
		if($result['count'] > 0){
			$return_array['on_hold'] = true;
			$return_array['reason'] = "OHRTO"; 
			return $return_array;
		}
	}
			
	if(WidgetKeyValuePairs::getWidgetValueForKey('condCODOH.trackedandrejected') == 'true'){
		$limit = WidgetKeyValuePairs::getWidgetValueForKey('condCODOH.trackedandrejected.nooforders');
		//Last order called from this tracker + rejected
		$sql = "select orderid,status from xcart_orders where payment_method = 'cod' and login = '".$login."' and orderid != $orderid and status not in ('PP','PV') order by orderid desc limit 0,".$limit;
		$result = func_query($sql,true);
		if($result && count($result) > 0){
			foreach ($result as $record){
				$sql = "select orderid from cod_oh_orders_dispostions where orderid = ".$record['orderid'] ;
				$result1 = func_query_first($sql,true);
				if($result1['orderid'] != '' && $result1['orderid'] != null && $record['status'] == 'F'){
					$return_array['on_hold'] = true;
					$return_array['reason'] = 'OHF'; 
					return $return_array;
				}	
			}
		}
	}
}

function get_oh_reason_for_code($code, $subcode=false){
	global $xcache;
	if($xcache==null){
		$xcache = new XCache();
	}
	$cachedOHReasons = $xcache->fetch('OH_REASONS');
	if($cachedOHReasons == NULL) {
		$cachedOHReasons = func_query("SELECT * FROM order_oh_reasons_master");
		$xcache->store(OH_REASONS, $cachedOHReasons, 0);
	}
	foreach ($cachedOHReasons as $reason){
		if(!$subcode) {
			if($reason['reason_code'] == $code){
				return $reason;
			}
		} else {
			if($reason['reason_code'] == $code && $reason['subreason_code'] == $subcode){
				return $reason;
			}
		}
	}
	
	return null;
}

function get_oh_reason_for_id($id){
	global $xcache;
	if($xcache==null){
		$xcache = new XCache();
	}
	$cachedOHReasons = $xcache->fetch('OH_REASONS');
	if($cachedOHReasons == NULL) {
		$cachedOHReasons = func_query("SELECT * FROM order_oh_reasons_master");
		$xcache->store(OH_REASONS, $cachedOHReasons, 0);
	}
	foreach ($cachedOHReasons as $reason){
		if($reason['id'] == $id){
			return $reason;
		}
	}
	
	return null;
}

function is_cod_enabled_for_user($login){
	$doRTOCheck = FeatureGateKeyValuePairs::getFeatureGateValueForKey('conditionalCodOnHold.RTOCheckToDisableCOD');
	$limit = WidgetKeyValuePairs::getWidgetValueForKey('conditionalCodOnHold.CustomerRTOLimit');
	$callimgLimit = WidgetKeyValuePairs::getWidgetValueForKey('conditionalCodOnHold.TrackedAndRTOLimit');
	if($doRTOCheck == 'true'){
		$sql = "select rt.orderid,payment_method from mk_old_returns_tracking rt, xcart_orders xo where rt.returntype = 'RTO' and rt.orderid = xo.orderid and xo.login = '".$login."' order by xo.orderid desc";
		$results = func_query($sql,true);
		if(!empty($results)){
			if(count($results) >= $limit){
				return false;
			}
			$orderIdString = "";
			$i=0;
			foreach ($results as $rto){
				if( $i != 0){
					$orderIdString = $orderIdString." , ";
				}
				$orderIdString = $orderIdString. $rto['orderid'];
				$i++;
			}
			$sql = "select count(1) as cnt from cod_oh_orders_dispostions where orderid in (".$orderIdString.")";
			$count = func_query_first($sql,true);
			if($count['cnt'] >= $callimgLimit){
				return false;
			}	
		}	
	}
	return true;
}

/*
function mark_orders_packed($orderId, $adminUser){
	global $weblog;

	$final_response = array();
	$successfulOrderIds = array();
	$weblog->debug("Marking order $orderId packed.");
	$order = func_select_order($orderId);
	if($order) {
		if($order["status"] == 'WP'){
			$response = ItemApiClient::moveOrderItemsToExitBin(array($orderId), $adminUser);
			$unsuccessfulOrderIds = $response[0];
			if(count($unsuccessfulOrderIds) > 0) {
				$final_response['unsuccessful'] = array('orderids'=>$unsuccessfulOrderIds, 'errormsg'=>$response[1]);
			}else{
					
				$response = ShipmentApiClient::pushOrderToLMS($order);				
				if($response === true) {
					$comment = "Order marked Packed(PK) and has been pushed to LMS.";
					func_change_order_status($orderId, 'PK', $adminUser, $comment, "", false);
					$successfulOrderIds[] = $orderId;
				} else {
					if($response !== false) {
						$weblog->info("Failed to push order (id: ". $orderId .") to LMS. " . "FailureMessage: " . $response);
						if(!isset($final_response['failed'])){
							$final_response['failed'] = array();
						}
						$final_response['failed'][] = array('orderid'=>$orderId, 'errormsg'=>$response);
					}
				}
			}
		}else{
			$final_response['failed'][] = array('orderid'=>$orderId, 'errormsg'=>"Order is in " . $order['status'] . " state. Only order in WP state can be marked packed.");
		}
	}else{
		$final_response['failed'][] = array('orderid'=>$orderId, 'errormsg'=>"Order not found in database.");
	}

	if(count($successfulOrderIds) > 0) {
		$final_response['successful'] = array('orderids'=>$successfulOrderIds);
	}
	return $final_response;
} */

function mark_orders_shipped($orderids, $adminUser, $adminComment, $progressKey=false, $orderId2CourierTrackingInfo=false, $notifyCustomer = true) {
	global $weblog;
	
	if(!is_array($orderids))
		$orderids = array($orderids);
	
	$weblog->debug("Marking orders shipped. Order Ids: " .print_r($orderids, true));
	
	$final_response = array();
	
	$invalidOrderIds = array();
	$successfulOrderIds = array();
	if($orderids && !empty($orderids)) {
		$xcache = new XCache();
		if($progressKey) {
    		$progressinfo = array('total'=>count($orderids), 'completed'=>0);
	    	$xcache->store($progressKey."_progressinfo", $progressinfo, 86400);
		}
		$count = 0;
		foreach($orderids as $orderId) {
			$count++;
			if($progressKey) {
		        	$progressInfo = $xcache->fetch($progressKey."_progressinfo");
		        	$progressInfo['completed'] = $count;
		        	$xcache->store($progressKey."_progressinfo", $progressInfo, 86400);
			}
                        $orderDetailsQuery = "select s_zipcode, courier_service, shipping_method, warehouseid from xcart_orders where orderid = " . $orderId;
                        $orderDetails = func_query_first($orderDetailsQuery,true);
                        if (!$orderDetails['shipping_method'] || $orderDetails['shipping_method'] === '') {
                            $orderDetails['shipping_method'] = 'NORMAL';
                        }
                        $actualPromiseTime = TatApiClient::getCachedMinimumTat($orderDetails['s_zipcode'], $orderDetails['warehouseid'], $orderDetails['courier_service'], $orderDetails['shipping_method']);
                        $actualPromiseTimeFormatted = date("Y-m-d H:i:s", time() + $actualPromiseTime * 3600);
                        db_query("insert into order_additional_info(order_id_fk,`key`,`value`,created_on) values ($orderid,'ACTUAL_PROMISE_TIME', $actualPromiseTimeFormatted, now())");
                        
			if($orderId2CourierTrackingInfo && !empty($orderId2CourierTrackingInfo)) {
				$trackingInfo = $orderId2CourierTrackingInfo[$orderId];
				$response = updateOrderTracking($orderId, $adminUser, $adminComment, $trackingInfo['courier'], $trackingInfo['tracking'], $notifyCustomer);
			} else {
				$response = updateOrderTracking($orderId, $adminUser, $adminComment,false, false, $notifyCustomer);
			}
			if($response === true) {
				$successfulOrderIds[] = $orderId;
			} else {
				if($response !== false) {
					if(!isset($final_response['failed']))
						$final_response['failed'] = array();
					$final_response['failed'][] = array('orderid'=>$orderId, 'errormsg'=>$response);
				}else
					$final_response['failed'][] = array('orderid'=>$orderId, 'errormsg'=>'Invalid order.');
			}
			
		}
		if($progressKey)
			$xcache->remove($progressKey."_progressinfo");
	}
	
	if(count($successfulOrderIds) > 0) {
		$final_response['successful'] = array('orderids'=>$successfulOrderIds);
	}
	return $final_response;
}

/* calculates the delivery date based on a base time(ordered date or shipped date)
 * @param: (int)$baseTime - ordered date or shipped date in timestamp
 * @param: (string)$logisticCode - courier service unique code
 * @return: (int)delivery date(in timestamp) based on base time and selected logistic provider
 */
function getOrderDeliveryDateInTimestamp($baseTime, $logisticCode='NoLogistic'){

    $dayInSeconds = 24*60*60;
    
    switch($logisticCode){
        case 'NoLogistic':  # feature gate value for delivery date to show in all communication in confirmation page and mail
                            $orderDeliveryDays = FeatureGateKeyValuePairs::getFeatureGateValueForKey('Order.DeliveryDays.NoLogistic');
                            //by default 7 days from the day of order, if not set in key->value pair
                            $additionalDays = (!empty($orderDeliveryDays))? (int)$orderDeliveryDays * $dayInSeconds : 7 * $dayInSeconds;
                            return ($baseTime + $additionalDays);

                            break;

        case 'ML'    :  # feature gate value for delivery date to show in all communication
                            # once order is shipped from myntra logistic Bangalore
                            $orderDeliveryDays = FeatureGateKeyValuePairs::getFeatureGateValueForKey('Order.DeliveryDays.MyntraLogistics');
                            //by default 3 days from day of ship, if not set in key->value pair
                            $additionalDays = (!empty($orderDeliveryDays))? (int)$orderDeliveryDays * $dayInSeconds : 3 * $dayInSeconds;
                            return ($baseTime + $additionalDays);

                            break;
        default          :  # feature gate value for delivery date to show in all communication
                            # once order is shipped from Third Party Logistic(BD,DTDC etc.)
                            $orderDeliveryDays = FeatureGateKeyValuePairs::getFeatureGateValueForKey('Order.DeliveryDays.ThirdPartyLogistic');
                            //by default 7 days from day of ship, if not set in key->value pair
                            $additionalDays = (!empty($orderDeliveryDays))? (int)$orderDeliveryDays * $dayInSeconds : 7 * $dayInSeconds;
                            return ($baseTime + $additionalDays);

                            break;

    }
}

function addOrderToRtsBatch($orderid,$batchid,$login){
	if($batchid ==null || $batchid == ''){
		$sql = "insert into ready_to_ship_batch (id,orderids,created_by,created_time,last_updated_time) values (null,case when orderids is null then '$orderid' else concat(orderids,',',$orderid) end,'$login',now(),now()) ON DUPLICATE KEY UPDATE orderids = concat(orderids,',',values(orderids)),last_updated_time = now()";
	}else{
		$sql = "update ready_to_ship_batch set orderids = concat(orderids,',',$orderid),last_updated_time = now() where id = $batchid";
	}
	func_query($sql);	
	if($batchid != '' && $batchid != null){
		return $batchid;
	}else{
		return db_insert_id();
	}
}



function func_get_order_cancel_reasons($full_order=true) {
	if($full_order)
		$cancel_codes = func_query("select * from cancellation_codes where cancellation_mode = 'FULL_ORDER_CANCELLATION' and is_active = 1", true);
	else
		$cancel_codes = func_query("select * from cancellation_codes where cancellation_mode = 'PART_ORDER_CANCELLATION' and is_active = 1", true);
	
	$cancellation_types = array();
	$cancellation_reasons = array();
	foreach($cancel_codes as $cancel_code){
		if(!isset($cancellation_types[$cancel_code['cancel_type']])){
			$cancellation_types[$cancel_code['cancel_type']] = $cancel_code['cancel_type_desc']; 
		}
		if(!isset($cancellation_reasons[$cancel_code['cancel_type']])) {
			$cancellation_reasons[$cancel_code['cancel_type']] = array();
		}
		$cancellation_reasons[$cancel_code['cancel_type']][$cancel_code['cancel_reason']] = $cancel_code['cancel_reason_desc']; 
	}
	
	return array('types'=>$cancellation_types, 'type2reasons'=>$cancellation_reasons);
}

function func_get_item_cancel_reasons() {
	$cancel_codes = func_query("select * from cancellation_codes where cancellation_mode = 'ITEM_CANCELLATION'", true);
	
	$cancellation_types = array();
	$cancellation_reasons = array();
	foreach($cancel_codes as $cancel_code){
		if(!isset($cancellation_types[$cancel_code['cancel_type']])){
			$cancellation_types[$cancel_code['cancel_type']] = $cancel_code['cancel_type_desc']; 
		}
		if(!isset($cancellation_reasons[$cancel_code['cancel_type']])) {
			$cancellation_reasons[$cancel_code['cancel_type']] = array();
		}
		$cancellation_reasons[$cancel_code['cancel_type']][$cancel_code['cancel_reason']] = $cancel_code['cancel_reason_desc']; 
	}
	
	return array('types'=>$cancellation_types, 'type2reasons'=>$cancellation_reasons);
}

function isInteger($input){
	if(empty($input) || $input == null){
		return false;
	}
	$input = trim($input);
	$reg_match = preg_match("/\D/",$input); 
	if ( $reg_match>0) {
		return false; 
	} 
	return true;
}

function validateOrdersForShipping($orderids){
	if(!is_array($orderids)){
		$orderids = array($orderids);
	}
	
	$order_details_sql = "select xo.orderid,ordertype,status,courier_service,item_status from xcart_orders xo,xcart_order_details xod where xo.orderid = xod.orderid and xo.orderid in (".implode(",", $orderids).")";
	$order_details = func_query($order_details_sql,true);
	
	$cancelled_orderids = array();
	$not_wp_orderids = array();
	$qa_pending_wp_orderids = array();
	$no_courier_assigned_orderids = array();
	$rtsModule=FeatureGateKeyValuePairs::getFeatureGateValueForKey('rts.orders.scan');

	foreach ($order_details as $row){
		if($row['status'] == 'D' || $row['status'] == 'F'){
			$cancelled_orderids[] = $row['orderid'];
		}
		
		if($row['status'] != 'WP'){
			$not_wp_orderids[] = $row['orderid'];
		}else{
			if($row['item_status'] != 'IC' && $row['item_status'] != 'QD'){
				$qa_pending_wp_orderids[] = $row['orderid'];
			}
			if($row['courier_service'] == '' || $row['courier_service'] == NULL){
				$no_courier_assigned_orderids[] = $row['orderid'];
			}	
		}	
		
		$valid_orders[] = $row['orderid'];
	}
	if(count($valid_orders) > 0){
		$invalid_orders = array_diff($orderids, $valid_orders);	
	}else{
		$invalid_orders = $orderids;
	}
	$retArray = array();
	$remaining_orderids = $orderids;
	if(count($cancelled_orderids) > 0){
		$cancelled_orderids = array_unique($cancelled_orderids);
		$remaining_orderids = array_diff($orderids, $cancelled_orderids);
		$retArray['CANCELLED_ORDERS'] = $cancelled_orderids;
	}
	if(count($not_wp_orderids) > 0){
		$not_wp_orderids = array_unique($not_wp_orderids);
		$remaining_orderids = array_diff($orderids, $not_wp_orderids);
		$retArray['NOT_WIP_ORDERS'] = $not_wp_orderids;
	}
	if(count($qa_pending_wp_orderids) > 0){
		$qa_pending_wp_orderids = array_unique($qa_pending_wp_orderids);
		$remaining_orderids = array_diff($orderids, $qa_pending_wp_orderids);
		$retArray['QA_PENDING_ORDERS'] = $qa_pending_wp_orderids;
	}
	if(count($no_courier_assigned_orderids) > 0){
		$no_courier_assigned_orderids = array_unique($no_courier_assigned_orderids);
		$remaining_orderids = array_diff($orderids, $no_courier_assigned_orderids);
		$retArray['NO_COURIER_ORDERS'] = $no_courier_assigned_orderids;
	}
	if(count($invalid_orders) > 0){
		$invalid_orders = array_unique($invalid_orders);
		$remaining_orderids = array_diff($orderids, $invalid_orders);
		$retArray['INVALID_ORDERS'] = $invalid_orders;
	}
	if(count($remaining_orderids) > 0){
		$retArray['VALID_ORDERS'] = $remaining_orderids;
	}
	return $retArray;
}

function isValidUserOrder($login, $orderid){
	$sql = "select orderid from xcart_orders where orderid = $orderid and login = '$login'";
	$order = func_query_first($sql,true);
	if(!$order)
		return false;
		
	return true;	
}

function findActiveOrderid($orderid,$itemid) {
	if (empty($orderid)) return false;
	$order_group_id = func_query_first_cell("select group_id from xcart_orders where orderid='$orderid'",true); 
	if (empty($order_group_id)) return false;
	$ordersSql = "select orderid from xcart_orders where group_id='$order_group_id' and status!='F'";
	$orderids = func_query($ordersSql,true);
	foreach ($orderids as $orderid) {
		$orderid = $orderid['orderid'];
		$orderDetailsSql = "select itemid from xcart_order_details where orderid='$orderid' and (item_status != 'IC' and item_status != 'RT')";
		$orderDetails = func_query($orderDetailsSql,true);
		foreach ($orderDetails as $orderDetail) {
			if($orderDetail['itemid'] != $itemid) {
				return $orderid;
			}
		}
	}
	return false;
}

/* function checkProductServiceability($styleId, $skuId, $zipcode, $serviceablePaymentModes = false){
	if(!isInteger($zipcode)){
		$returnArray = array(
                	'SERVICEABLE' => '',
        	        'DELIVERY_PROMISE_TIME' => "6-7"
 	       );
	}	
	$time = getTatForSku($skuId, $zipcode,$styleId);
	
	if($time) {
            $days = ceil($time/24);
        } else {
            $days = FeatureGateKeyValuePairs::getInteger('lms.tat.defaultNoOfDays', 5);
        }
	if($days >1){
                $days = $days."-".($days+FeatureGateKeyValuePairs::getInteger('lms.tat.promiseDateRange', 2));
        } else {
		$days = "1-2";
        }
	$cachedStyleBuilderObj=CachedStyleBuilder::getInstance();
	$styleObj=$cachedStyleBuilderObj->getStyleObject($styleId);
	$discountData= $styleObj->getDiscountData();
	$discountAmount = $discountData->discountAmount;
	$productStyleDetails=$styleObj->getProductStyleDetails();
	$originalprice = $productStyleDetails["price"];
	$product = array('style_id'=> $styleId, 'sku_id'=>$skuId, 'totalProductPrice'=>$originalprice, 'discount'=>$discountAmount, 'quantity'=>1);
	
	$returnArray = array(
		'SERVICEABLE' => $serviceablePaymentModes===false?getServiceablePaymentModesForProducts($zipcode, array($product)):$serviceablePaymentModes,
		'DELIVERY_PROMISE_TIME' => $days
	);
		
	return $returnArray;
} */

/* function getTatForSku($skuId,$zipcode,$styleId, $whId = FALSE, $courier=false, $paymentMethod=false){
	$params = array();
	$params[] = $skuId;
	$params[] = $zipcode;
	$params[] = $whId;
	$params[] = $courier;
	$params[] = $paymentMethod;
	
	$cache = new \Cache(TATCacheKeys::$prefix, TATCacheKeys::keySet($whId, $zipcode, $paymentMethod, $courier));
	
	$ret_time = $cache->get(function($skuId,$zipcode,$whId,$courier,$paymentMethod){
	
		$available_wh = array();
			
		if(!$whId || trim($whId) == '' || $whId == null){
			$whId = null;
			include_once "$xcart_dir/modules/apiclient/SkuApiClient.php";
			$inv_counts = SkuApiClient::getAvailableInvCountMap(array($skuId),'warehouseId','PDP');
			$checkInStock=FeatureGateKeyValuePairs::getBoolean('checkInStock', true);
			foreach($inv_counts as $wh_id => $inv_count){
				if($inv_count >0 || !$checkInStock){
					$available_wh[] = $wh_id;
				}
			}
		}else{
			if(!is_array($whId))
				$whId = array($whId);
				
			$available_wh = $whId;
		}
		if(!empty($available_wh)){
			include_once($xcart_dir."/modules/apiclient/TatApiClient.php");
			$time = TatApiClient::getTat($zipcode,$available_wh,$paymentMethod,$courier);
		}
		return $time;
	}, $params, TATCacheKeys::getKey($whId, $zipcode, $paymentMethod, $courier),300);
	
	return $ret_time;
} */

function getDispatchTimeForProduct($baseTime, $skuId){

	$dayInSeconds = 24*60*60;

	$skuId2InvDetailsMap = AtpApiClient::getAvailableInventoryForSkus(array($skuId));
	if($skuId2InvDetailsMap[$skuId]) {
		$leadTime = $skuId2InvDetailsMap[$skuId]['leadTime'];
	}

	return ($baseTime + (1+$leadTime)*$dayInSeconds);
}

function getDeliveryDateForOrder($baseTime,$productDetails,$zipcode,$payment_mode,$courier_service=false, $warehouseId=false, $orderId = false){
	
	if($orderId) {
		$promiseTimeRow = func_query_first("Select value from order_additional_info where order_id_fk = $orderId and `key` = 'CUSTOMER_PROMISE_TIME'", true);
		if($promiseTimeRow)
			return strtotime($promiseTimeRow['value']);
		else
			return false;
	}
	
	$dayInSeconds = 24*60*60;	

	$skuIds = array();
	foreach($productDetails as $product){
		$skuIds[] = $product['sku_id'];
	}
	
	$skuId2InvDetailsMap = AtpApiClient::getAvailableInventoryForSkus($skuIds);
	foreach($productDetails as $index => $product) {
		if($skuId2InvDetailsMap[$product['sku_id']]) {
			if(!$warehouseId) {
				$productDetails[$index]['warehouses'] = explode(",", $skuId2InvDetailsMap[$product['sku_id']]['availableInWarehouses']);
			} else {
				$productDetails[$index]['warehouses'] = array($warehouseId);
			}			
			$productDetails[$index]['leadTime'] = $skuId2InvDetailsMap[$product['sku_id']]['leadTime'];
		}		
	}
	
	$max_days = 0;
	foreach($productDetails as $product){
		if(!empty($product['warehouses']))
			$time = TatApiClient::getCachedMinimumTat($zipcode, $product['warehouses'], $courier_service);
		
		//$time = getTatForSku($product['sku_id'], $zipcode, $product['style_id'], $warehouseId, $courier_service, $payment_mode);
		$days = 0;
		if($time){
			$days = ceil($time/24);
		}
		if($days > $max_days){
			$max_days = $days;
		}
	}	
	
	if($max_days == 0){
		$max_days = FeatureGateKeyValuePairs::getInteger('lms.tat.defaultNoOfDays',5);
	}
    $max_days = $max_days + FeatureGateKeyValuePairs::getInteger('lms.tat.promiseDateRange', 2) + $product['leadTime'];
    $promise_time = $max_days * $dayInSeconds;
	return $baseTime + $promise_time;
}

/**
 * isJewellery
 * isHazmat
 * isFragile
 * isComforter 
 */
function getTagsForStyle($styleIds){

	if(!is_array($styleIds))
		$styleIds = arrya($styleIds);
	
	$styleIds = array_values(array_unique($styleIds));
	
	$style_properties = func_query("select msp.style_id, msp.global_attr_article_type, msp.global_attr_sub_category, at.attribute_type, attribute_value from (((mk_style_properties msp LEFT JOIN mk_style_article_type_attribute_values msav on msp.style_id = msav.product_style_id) LEFT JOIN mk_attribute_type_values atv on msav.article_type_attribute_value_id = atv.id) LEFT JOIN mk_attribute_type at ON at.id = atv.attribute_type_id) where msp.style_id in (".implode(",",$styleIds).")", true);
	$style_tags_map = array();
	$fragileItems = ereg_replace("[^0-9,]", "", FeatureGateKeyValuePairs::getFeatureGateValueForKey('fragile.article.types'));
	$fragileItems = explode(",",$fragileItems);
	
	$hazmatItems = ereg_replace("[^0-9,]", "", FeatureGateKeyValuePairs::getFeatureGateValueForKey('hazmat.article.types'));
	$hazmatItems = explode(",",$hazmatItems);
	
	$styleId2ArtcileTypeIdMap = array();
	$styleId2SubCategoryIdMap = array();
	foreach($style_properties as $style_property){
		if($style_property['attribute_value'] == 'Fine Jewellery'){
			$style_tags_map[$style_property['style_id']][] = array('name'=> 'isJewellery', 'value'=>'true', 'dataType'=>'BOOLEAN');
		}
		$styleId2ArtcileTypeIdMap[$style_property['style_id']] = $style_property['global_attr_article_type'];
		$styleId2SubCategoryIdMap[$style_property['style_id']] = $style_property['global_attr_sub_category'];
	}
	
	foreach($styleIds as $id) {
		if(in_array($styleId2SubCategoryIdMap[$id], $hazmatItems)){
			$style_tags_map[$id][] = array('name'=> 'isHazmat', 'value'=>'true', 'dataType'=>'BOOLEAN');
		}
		if(in_array($styleId2SubCategoryIdMap[$id],$fragileItems) || in_array($styleId2ArtcileTypeIdMap[$id],$fragileItems)){
			$style_tags_map[$id][] = array('name'=> 'isFragile', 'value'=>'true', 'dataType'=>'BOOLEAN');
		}
	}
	
	return $style_tags_map;
}


/*
 * set $continue = true to cancel order incase of failures 
 * 
 * set $continue = true along with $cancelOrder = true. for not cancelling order and do nothing
 */
function myntCashDebitfailureProcedure($orderid,$myntCashDebitResponse,$cancelOrder = false ,$continue = false ){
	
	if($cancelOrder){		
		if($myntCashDebitResponse == MyntCashService::noEnoughBalanceForDebitRequest 
				|| $myntCashDebitResponse == MyntCashService::serviceNotAvailable){
			// cancel the order, if the debit process is a failure
			func_cancel_order($orderid, 'F', "AUTOSYSTEM", "myntcash/cashback debit failure", true, "CCC", "MCFAIL",
					array($orderid=>true), false, false, false, false, false);
			return;
			
		}elseif ($continue){
			// incase of cashback/myntcash failure during F/D => PP/Q migration, do nothing
			return ;
		}
		else{
			
			return ;
		}		
	}
		
	if($myntCashDebitResponse === MyntCashService::noEnoughBalanceForDebitRequest){
		$reasonCode = "CNA";
	}elseif($myntCashDebitResponse === MyntCashService::serviceNotAvailable){
		$reasonCode = "CSD";
	}elseif($myntCashDebitResponse == true){
		return 'Q';
	}else{
		$reasonCode = "CSD";
	}
	
	$updateData = array();
	$updateData['status'] = 'OH';
	$updateData['queueddate'] = null;
	$oh_reason = get_oh_reason_for_code('CDE', $reasonCode);
	$content = "Order $orderid can not be processed, as sufficient cashback balance is not awailable in customers account. Moving order to On Hold state. Please check 	and queue this order manually";
	if($oh_reason != null){
		$updateData['on_hold_reason_id_fk'] = $oh_reason['id'];
		$commentArrayToInsert = Array ("orderid" => $orderid,
				"commenttype" => "Automated",
				"commenttitle" => "status change",
				"commentaddedby" => "admin",
				"description" => "Putting the order on hold as : ".$oh_reason['reason_desc']."-".$oh_reason['subreason_desc'],
				"commentdate" => time());
		
		func_array2insert("mk_ordercommentslog", $commentArrayToInsert);
	}

	func_array2update('orders', $updateData, "orderid = $orderid");

	$mail_details = array(
			"from_email"=>'admin@myntra.com',
			"from_name"=>'OMS Admin',
			"to" => constant("ORDER_OOS_NOTIFICATION"),
			"mail_type" => MailType::CRITICAL_TXN,
			"subject"=> 'Cashback debit transaction error after order payment',
			"content"=> $content
	);
	$multiPartymailer = new MultiProviderMailer($mail_details);
	$multiPartymailer->sendMail();

	return "OH";

}

function putFreeItemShipmentOnHold($group_orderid)  {
	
	$putFreeItemsOnHold = FeatureGateKeyValuePairs::getBoolean("freeitem.onholdcheck.enabled", false);
	if($putFreeItemsOnHold) {
		$orderDetails = func_query("Select od.orderid, itemid, price*amount as totalprice, od.discount, discount_rule_id from xcart_orders o, xcart_order_details od where o.orderid = od.orderid and o.group_id = $group_orderid and o.payment_method = 'cod'");
		if($orderDetails) {
		
			$orderIdsWithFreeItems = array();
			$orderIds = array();
			
			$discountRuleId2OrderIds = array();
			foreach($orderDetails as $itemRow ) {
				if(!isset($discountRuleId2OrderIds[$itemRow['discount_rule_id']])) {
					$discountRuleId2OrderIds[$itemRow['discount_rule_id']] = array();
				}
				
				if(!in_array($itemRow['orderid'], $discountRuleId2OrderIds[$itemRow['discount_rule_id']])) {
					$discountRuleId2OrderIds[$itemRow['discount_rule_id']][] = $itemRow['orderid']; 
				}
			}
			
			foreach($orderDetails as $itemRow ) {
				if($itemRow['totalprice'] == $itemRow['discount'] && 
						count($discountRuleId2OrderIds[$itemRow['discount_rule_id']]) > 1) {
					if(!in_array($itemRow['orderid'], $orderIdsWithFreeItems))
						$orderIdsWithFreeItems[] = $itemRow['orderid'];
				}
				
				if(!in_array($itemRow['orderid'], $orderIds))
					$orderIds[] = $itemRow['orderid'];
			}
	
			$putOrdersOnHold = array();
			if(count($orderIdsWithFreeItems) == count($orderIds)) {
				// if all releases contain free gift items... then place all orderids on hold except for parent orderid 
				foreach($orderIds as $oid) {
					if($oid != $group_orderid)
						$putOrdersOnHold[] = $oid;
				} 
			} else {
				$putOrdersOnHold = $orderIdsWithFreeItems;	
			}
		
			if($putOrdersOnHold && count($putOrdersOnHold) > 0) {
				$oh_reason = get_oh_reason_for_code('FGFP');
				db_query("update xcart_orders set status = 'OH', on_hold_reason_id_fk = ".$oh_reason['id']." where orderid in (".implode(",", $putOrdersOnHold).")");
				
				foreach($putOrdersOnHold as $oid) {
					func_addComment_status($oid, 'OH', 'WP', "Putting on hold as combo items are split. Free item present in this shipment", "SYSTEM");
				}
			}
		}
	}
	
}

function releaseFreeItemShipmentOnHold($shipmentId) {
        $oh_reason = get_oh_reason_for_code('FRI', 'FIDS');
	$onHoldShipments = func_query("Select o.orderid from xcart_orders o, xcart_orders o1 where o.group_id = o1.group_id and o.orderid != o1.orderid and o1.orderid = $shipmentId and o.status = 'OH' and o.on_hold_reason_id_fk = ".$oh_reason['id'], true);
	if($onHoldShipments) {
		$onHoldShipmentIds=array();
		foreach($onHoldShipments as $shipment)
			$onHoldShipmentIds[] = $shipment['orderid'];
		
		func_change_order_status($onHoldShipmentIds, 'Q', 'System' ,"Queing order after $shipmentId is marked Delivered");
	}
}

function updateShippingMethodForOrder($orderids){
	if(!is_array($orderids)){
		$orderids = array($orderids);
	}

	$isXpressShippingEnabled = FeatureGateKeyValuePairs::getBoolean('xpressshipping.enabled',true);
	$isPriorityShippingEnabled = FeatureGateKeyValuePairs::getBoolean('priorityshipping.enabled',true);

	if($isXpressShippingEnabled || $isPriorityShippingEnabled){
		$orders = func_query("select * from xcart_orders where orderid in (".implode(",",$orderids).")");
		foreach($orders as $order){
			$result = getShippingMethod($order,$order['warehouseid'],$order['courier_service'],$order['status']);
			updateOrderShippingMethod($order,$order['warehouseid'],$order['courier_service'],$result);
		}
	}else{
		func_query("update xcart_orders set shipping_method = 'NORMAL' where orderid in (".implode(",",$orderids).")");
	}
}


function getShippingMethod($order,$warehouseid,$courierService,$orderStatus){
	if($orderStatus == 'OH'){
		return 'NORMAL';
	}
	$isXpressShippingEnabled = FeatureGateKeyValuePairs::getBoolean('xpressshipping.enabled',true);
	$isPriorityShippingEnabled = FeatureGateKeyValuePairs::getBoolean('priorityshipping.enabled',true);
	$result = 'NORMAL';
	if($isXpressShippingEnabled){
		$result = checkXpressOrder($order,$warehouseid,$courierService);
	}
	if($result == 'NORMAL' && $isPriorityShippingEnabled){
		$result = checkPriorityOrder($order,$warehouseid);
	}
	
	return $result;
}

function checkXpressOrder($order,$warehouseid,$courierService){
	$tatResponse = TatApiClient::getCourierCutOffs($order['s_zipcode'],$warehouseid,$order['payment_method'], $courierService,'EXPRESS');
	if($tatResponse){
		$capacities = WidgetKeyValuePairs::getWidgetValueForKey('xpressshipping.capacities');
		$hourlyPercentages =  WidgetKeyValuePairs::getWidgetValueForKey('xpressshipping.hourly.percentage');
		$markXpress = false;
		if($hourlyPercentages && !empty($hourlyPercentages) && $capacities && !empty($capacities)) {
			$first2digits = intval($order["s_zipcode"]/10000);
			$capacities = json_decode($capacities,true);
			$dailyLimit = $capacities[$first2digits];
			$hourlyPercentages = json_decode($hourlyPercentages, true);

			foreach($hourlyPercentages as $hourRange=>$percent) {
				$hours = explode("-", $hourRange);
				$currentHour = date('H');
				if($currentHour >= intval($hours[0]) && $currentHour < intval($hours[1]) ) {
					$ohOrdersCountRow = func_query_first("Select id, no_of_orders from xpress_capacity where zipcode = '".$first2digits."' and record_date = '".date('Y-m-d')."' and start_hour = ".$hours[0]." and end_hour = ".$hours[1]);
					if($ohOrdersCountRow) {
						if($ohOrdersCountRow['no_of_orders'] < $percent*$dailyLimit/100) {
							$markXpress = true;
							db_query("update xpress_capacity set no_of_orders = no_of_orders+1 where id = $ohOrdersCountRow[id]"); 
						}
					} else {
						$markXpress = true;
						func_array2insert('xpress_capacity', array('no_of_orders' => 1,'zipcode' => $first2digits,'record_date'=>date('Y-m-d'), 'start_hour'=>$hours[0], 'end_hour'=>$hours[1]));
					}
					break;
				}
			}
		}
	
		return $markXpress?'XPRESS':'NORMAL';
	}else{
		return 'NORMAL';
	}
}

function getCutOffMins($cutoffTime){
	$cutoffTime = explode(":",$cutoffTime);
	return $cutoffTime[0]*60 + ($cutoffTime[1]);
}

function checkPriorityOrder($order,$warehouseid){
	$warehouseids = WidgetKeyValuePairs::getWidgetValueForKey('priorityshipping.warehouseids');
	$warehouseids = explode(",",trim($warehouseids));
	if(in_array($warehouseid,$warehouseids)) {
		$zipcodes = WidgetKeyValuePairs::getWidgetValueForKey('priorityshipping.'.$warehouseid.'.zipcodes');
		$zipcodes = explode(",",trim($zipcodes));
		$timewindow = WidgetKeyValuePairs::getWidgetValueForKey('priorityshipping.'.$warehouseid.'.timewindow');
		$timewindow = explode("-",trim($timewindow));

		$timeDetails = func_query_first("select queueddate, unix_timestamp(concat(from_unixtime(queueddate,'%Y-%m-%d'),' ','".$timewindow[0]."')) as queue_start_time,unix_timestamp(concat(from_unixtime(queueddate,'%Y-%m-%d'),' ','".$timewindow[1]."')) as queue_end_time from xcart_orders where orderid = ".$order['orderid']);
		if(in_array($order['s_zipcode'],$zipcodes) &&
				$timeDetails['queueddate'] > $timeDetails['queue_start_time'] && $timeDetails['queueddate'] < $timeDetails['queue_end_time']){
			return 'PRIORITY';		
		}else{
			return 'NORMAL';
		}
	}else{
		return 'NORMAL';
	}
}

function updateOrderShippingMethod($order,$warehouseid,$courierService,$result){
	$orderid = $order['orderid'];
	if($result == 'XPRESS'){
		$tatResponse = TatApiClient::getCourierCutOffs($order['s_zipcode'],$warehouseid,$order['payment_method'], $courierService,'EXPRESS');
		func_query("update xcart_orders set shipping_method = 'XPRESS' where orderid = ".$orderid);
		
		$pickingTimeInHrs = FeatureGateKeyValuePairs::getFeatureGateValueForKey('xpressshipping.pickingtime');
		$qcTimeInHrs = FeatureGateKeyValuePairs::getFeatureGateValueForKey('xpressshipping.qctime');

		$orderPlacementTime = date("H", $order['queueddate'])*60 + date("i", $order['queueddate']); // 22:55 today 
		$shipByTime = $orderPlacementTime + $pickingTimeInHrs*60 + $qcTimeInHrs*60; 
		
		$cutOff = false;
		$tatInMins = false;
		foreach($tatResponse as $tat){
			if($shipByTime < getCutOffMins($tat['shippingCutoff'])){
				$cutOff = getCutOffMins($tat['shippingCutoff']);
				$tatInMins = $tat['tatTime']*60;
				break;
			}
		}
		if(!$cutOff){
			$cutOff = getCutOffMins($tatResponse[0]['shippingCutoff']) + 24*60;
			$tatInMins = $tatResponse[0]['tatTime']*60;
		}
		
		$timeOfTodaysStart = strtotime(date("Y-m-d"), $order['queueddate']);
		$expectedCutOffTime = date("Y-m-d\\TH:i:s", strtotime("+ ".$cutOff." minutes",$timeOfTodaysStart));
		$expectedPickingTime = date("Y-m-d\\TH:i:s", strtotime("+ ".($cutOff-($qcTimeInHrs*60))." minutes",$timeOfTodaysStart));
		//$expectedSortingTime = date("Y-m-d\\TH:i:s", strtotime("+ ".($orderPlacementTime+($pickingTimeInHrs*60)+($qcTimeInHrs*60))." minutes",$timeOfTodaysStart));
		$customerPromiseTime = date("Y-m-d\\TH:i:s", strtotime("+ ".($cutOff+$tatInMins)." minutes",$timeOfTodaysStart));
		
		func_query("insert into order_additional_info(order_id_fk,`key`,value,created_on) values ($orderid,'EXPECTED_CUTOFF_TIME','". $expectedCutOffTime."',now())");

	}else if($result == 'PRIORITY'){
		func_query("update xcart_orders set shipping_method = 'PRIORITY' where orderid = ".$orderid);
		
		$pickingTimeInHours = FeatureGateKeyValuePairs::getFeatureGateValueForKey('normalshipping.pickingtime');
		$qcTimeInHrs = FeatureGateKeyValuePairs::getFeatureGateValueForKey('normalshipping.qctime');
		
		if($order['queueddate']) {
			$orderPlacementTime = date("H", $order['queueddate'])*60 + date("i", $order['queueddate']);
			$timeOfTodaysStart = strtotime(date("Y-m-d"), $order['queueddate']);
		} else {
			$orderPlacementTime = date("H", $order['date'])*60 + date("i", $order['date']);
			$timeOfTodaysStart = strtotime(date("Y-m-d"), $order['date']);
		}
		
		$shipByTime = $orderPlacementTime+($pickingTimeInHours*60)+($qcTimeInHrs*60);
		$expectedPickingTime = date("Y-m-d\\TH:i:s", strtotime("+ ".($orderPlacementTime+($pickingTimeInHours*60))." minutes",$timeOfTodaysStart));
		//$expectedSortingTime = date("Y-m-d\\TH:i:s", strtotime("+ ".($orderPlacementTime+($pickingTimeInHrs*60)+($qcTimeInHrs*60))." minutes",$timeOfTodaysStart));
		
		$tatHours = TatApiClient::getCachedMinimumTat($order['s_zipcode'],$warehouseid, $courierService, 'NORMAL');
		$customerPromiseTime = date("Y-m-d\\TH:i:s", strtotime("+ ".($shipByTime+($tatHours*60))." minutes",$timeOfTodaysStart));
		
	}else if($result == 'NORMAL'){
		func_query("update xcart_orders set shipping_method = 'NORMAL' where orderid = ".$orderid);
		
		$pickingTimeInHours = FeatureGateKeyValuePairs::getFeatureGateValueForKey('normalshipping.pickingtime');
		$qcTimeInHrs = FeatureGateKeyValuePairs::getFeatureGateValueForKey('normalshipping.qctime');
		
		if($order['queueddate']) {
			$orderPlacementTime = date("H", $order['queueddate'])*60 + date("i", $order['queueddate']);
			$timeOfTodaysStart = strtotime(date("Y-m-d"), $order['queueddate']);
		} else {
			$orderPlacementTime = date("H", $order['date'])*60 + date("i", $order['date']);
			$timeOfTodaysStart = strtotime(date("Y-m-d"), $order['date']);
		}
		
		$shipByTime = $orderPlacementTime+($pickingTimeInHours*60)+($qcTimeInHrs*60);
		$expectedPickingTime = date("Y-m-d\\TH:i:s", strtotime("+ ".($orderPlacementTime+($pickingTimeInHours*60))." minutes",$timeOfTodaysStart));
		//$expectedSortingTime = date("Y-m-d\\TH:i:s", strtotime("+ $shipByTime minutes",$timeOfTodaysStart));
		
		//$tatHours = TatApiClient::getTat($order['s_zipcode'],$warehouseid,$order['payment_method'], $courierService,'NORMAL');
		$tatHours = TatApiClient::getCachedMinimumTat($order['s_zipcode'],$warehouseid, $courierService, 'NORMAL');
		$customerPromiseTime = date("Y-m-d\\TH:i:s", strtotime("+ ".($shipByTime+($tatHours*60))." minutes",$timeOfTodaysStart));
	}
	
	func_query("insert into order_additional_info(order_id_fk,`key`,value,created_on) values ($orderid,'EXPECTED_PICKING_TIME','". $expectedPickingTime."',now())");
	//func_query("insert into order_additional_info(order_id_fk,`key`,value,created_on) values ($orderid,'EXPECTED_SORTING_TIME','". $expectedSortingTime."',now())");
	func_query("insert into order_additional_info(order_id_fk,`key`,value,created_on) values ($orderid,'CUSTOMER_PROMISE_TIME','". $customerPromiseTime."',now())");
}


function fetchProductServiceabilityAndTat($styleId, $skuId, $zipcode, $warehouses, $supplyType, $leadTime, $paymentModes = false){
	if(!isInteger($zipcode)){
		$returnArray = array(
				'SERVICEABLE' => '',
				'DELIVERY_PROMISE_TIME' => "6-7"
		);
	}
	
	if(!empty($warehouses))
		$time = TatApiClient::getCachedMinimumTat($zipcode, $warehouses);

	if($time) {
		$days = ceil($time/24);
	} else {
		$days = FeatureGateKeyValuePairs::getInteger('lms.tat.defaultNoOfDays', 5);
	}
	if(!empty($leadTime)) {
		$days += $leadTime;
	}
	if($days > 1) {
		$days = $days."-".($days+FeatureGateKeyValuePairs::getInteger('lms.tat.promiseDateRange', 2));
	} else {
		$days = "1-2";
	}
	
	$cachedStyleBuilderObj=CachedStyleBuilder::getInstance();
	$styleObj=$cachedStyleBuilderObj->getStyleObject($styleId);
	$discountData= $styleObj->getDiscountData();
	$discountAmount = $discountData->discountAmount;
	$productStyleDetails=$styleObj->getProductStyleDetails();
	$originalprice = $productStyleDetails["price"];
	$product = array('style_id'=> $styleId, 'sku_id'=>$skuId, 'totalProductPrice'=>$originalprice, 
			'discount'=>$discountAmount, 'quantity'=>1);

	$skuDetails[$skuId] = array();
	foreach($warehouses as $whId) {
		$skuDetails[$skuId]['warehouse2AvailableItems'][$whId] = 1;
	}
	
	$returnArray = array(
			'SERVICEABLE' => $paymentModes?$paymentModes:getServiceablePaymentModesForProducts($zipcode, array($product), false , false, $skuDetails),
			'DELIVERY_PROMISE_TIME' => $days
	);

	return $returnArray;
}
?>
