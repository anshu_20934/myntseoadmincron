<?php
/*****************************************************************************\
+-----------------------------------------------------------------------------+
| X-Cart                                                                      |
| Copyright (c) 2001-2006 Ruslan R. Fazliev <rrf@rrf.ru>                      |
| All rights reserved.                                                        |
+-----------------------------------------------------------------------------+
| PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
| FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
| AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
|                                                                             |
| THIS  AGREEMENT  EXPRESSES  THE  TERMS  AND CONDITIONS ON WHICH YOU MAY USE |
| THIS SOFTWARE   PROGRAM   AND  ASSOCIATED  DOCUMENTATION   THAT  RUSLAN  R. |
| FAZLIEV (hereinafter  referred to as "THE AUTHOR") IS FURNISHING  OR MAKING |
| AVAILABLE TO YOU WITH  THIS  AGREEMENT  (COLLECTIVELY,  THE  "SOFTWARE").   |
| PLEASE   REVIEW   THE  TERMS  AND   CONDITIONS  OF  THIS  LICENSE AGREEMENT |
| CAREFULLY   BEFORE   INSTALLING   OR  USING  THE  SOFTWARE.  BY INSTALLING, |
| COPYING   OR   OTHERWISE   USING   THE   SOFTWARE,  YOU  AND  YOUR  COMPANY |
| (COLLECTIVELY,  "YOU")  ARE  ACCEPTING  AND AGREEING  TO  THE TERMS OF THIS |
| LICENSE   AGREEMENT.   IF  YOU    ARE  NOT  WILLING   TO  BE  BOUND BY THIS |
| AGREEMENT, DO  NOT INSTALL OR USE THE SOFTWARE.  VARIOUS   COPYRIGHTS   AND |
| OTHER   INTELLECTUAL   PROPERTY   RIGHTS    PROTECT   THE   SOFTWARE.  THIS |
| AGREEMENT IS A LICENSE AGREEMENT THAT GIVES  YOU  LIMITED  RIGHTS   TO  USE |
| THE  SOFTWARE   AND  NOT  AN  AGREEMENT  FOR SALE OR FOR  TRANSFER OF TITLE.|
| THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY GRANTED BY THIS AGREEMENT.      |
|                                                                             |
| The Initial Developer of the Original Code is Ruslan R. Fazliev             |
| Portions created by Ruslan R. Fazliev are Copyright (C) 2001-2006           |
| Ruslan R. Fazliev. All Rights Reserved.                                     |
+-----------------------------------------------------------------------------+
\*****************************************************************************/

#
# $Id: func.mail.php,v 1.11 2006/04/12 07:36:02 svowl Exp $
#

if ( !defined('XCART_START') ) { header("Location: ../"); die("Access denied"); }

x_load('files');
//putenv("TZ=Asia/Calcutta");
function func_getreportschedules(){
$style_query="select id,name from mk_reports_scheduletypes";
$styleresult=db_query($style_query);
$styles=array();
if ($styleresult)
	{
		$i=0;

		while ($row=db_fetch_array($styleresult))
		{
			$styles[$i] = $row;
			$i++;
		}
	}
	$stylesresults=array();
	foreach($styles as $style){
			$stylesresults[$style['id']]=$style['name'];
	}
	return $stylesresults;
	
	
}
function func_getreportscheduledate(){
$style_query="select id,name from mk_reports_scheduledates";
$styleresult=db_query($style_query);
$styles=array();
if ($styleresult)
	{
		$i=0;

		while ($row=db_fetch_array($styleresult))
		{
			$styles[$i] = $row;
			$i++;
		}
	}
	$stylesresults=array();
	foreach($styles as $style){
			$stylesresults[$style['id']]=$style['name'];
	}
	return $stylesresults;
	
	
}
function func_get_allsku(){
$style_query="select sku_id,sku_name from mk_inventory_sku";
$styleresult=db_query($style_query);
$styles=array();
if ($styleresult)
	{
		$i=0;

		while ($row=db_fetch_array($styleresult))
		{
			$styles[$i] = $row;
			$i++;
		}
	}
	$stylesresults=array();
	foreach($styles as $style){
			$stylesresults[$style['sku_id']]=$style['sku_name'];
	}
	return $stylesresults;
	
	
}
function func_getreportgroups(){
$style_query="select id,name from mk_reports_groups";
$styleresult=db_query($style_query);
$styles=array();
if ($styleresult)
	{
		$i=0;

		while ($row=db_fetch_array($styleresult))
		{
			$styles[$i] = $row;
			$i++;
		}
	}
	$stylesresults=array();
	foreach($styles as $style){
			$stylesresults[$style['id']]=$style['name'];
	}
	return $stylesresults;
	
	
}
function func_getdatatypes(){
$style_query="select id,name from mk_reports_datatypes ";
$styleresult=db_query($style_query);
$styles=array();
if ($styleresult)
	{
		$i=0;

		while ($row=db_fetch_array($styleresult))
		{
			$styles[$i] = $row;
			$i++;
		}
	}
	return $styles;
	
	
}
#
#This function adds a new report group to the database
#
function func_addreportgroup($groupname){
	$query="insert into `mk_reports_groups` (name) values ('$groupname')";
	db_query($query);
}
function func_checkGroup($groupname){
$commentQuery="select id from mk_reports_groups where name='$groupname'";
$commentresult=db_query($commentQuery);
$comments=db_fetch_row($commentresult);
return $comments[0];
}
function func_getallreportslist(){
//$style_query="select groups.name as groupname,report.id as reportid,report.name ". 
//"as reportname from mk_reports_groups groups ,mk_reports report where report.groupid=groups.id ";
$style_query="select groups.name as groupname,report.id as reportid,report.name as reportname ".
 "from mk_reports_groups groups LEFT JOIN mk_reports report ".
 "On report.groupid=groups.id ";

$styleresult=db_query($style_query);
$styles=array();
if ($styleresult)
	{
		$i=0;

		while ($row=db_fetch_array($styleresult))
		{
			$styles[$i] = $row;
			$i++;
		}
	}
	//$stylesresults=array();
	$stylesresultsgrp=array();
	//$oldname="";
	foreach($styles as $style){
		//$newname=$style['name'];
		//if($newname!=$oldname){
			//$oldname=$style['name'];
			$stylesresultsgrp[$style['groupname']][$style['reportid']]=$style['reportname'];
			//if(!empty($styleresults))
			//$stylesresults=array();
		//}
		//$stylesresults[$style['value']]=$style['value'];
			
			
	}
	return $stylesresultsgrp;
}
function str_replace_count($search,$replace,$subject,$times) {
    $subject_original=$subject;
    $len=strlen($search);    
    $pos=0;
    for ($i=1;$i<=$times;$i++) {
        $pos=strpos($subject,$search,$pos);
        if($pos!==false) {                
            $subject=substr($subject_original,0,$pos);
            $subject.=$replace;
            $subject.=substr($subject_original,$pos+$len);
            $subject_original=$subject;
        } else {
            break;
        }
    }
    return($subject);
}
function func_getreportquery($id){
$commentQuery="select mr.name as reportname,mr.query,mr.sumquery,mr.mailto,mrg.id as grp,mr.report_type from mk_reports mr,mk_reports_groups mrg where mr.groupid=mrg.id and mr.id='$id'";
$commentresult=db_query($commentQuery);
$comments=db_fetch_row($commentresult);
return $comments;
	
	
}
function unix_date($input_date,$fieldTypeName)
    { // take date string such as "M/D/year" and return the Unix timestamp
	    if (($fieldTypeName  == "from_date") || ($fieldTypeName == "FRMDAT") || ($fieldTypeName  == "From_Date")){
       			return mktime(0, 0, 0, substr($input_date, 0, 2), substr($input_date, 3, 2), substr($input_date, 6,4)) ; //+ 45000;   
		}else if (($fieldTypeName  == "to_date") || ($fieldTypeName == "TODAT") || ($fieldTypeName  == "To_Date")) {
 		       return mktime(23, 59, 59, substr($input_date, 0, 2), substr($input_date, 3, 2), substr($input_date, 6,4)) ; //  + 45000;   
		}else{
			   return mktime(0, 0, 0, substr($input_date, 0, 2), substr($input_date, 3, 2), substr($input_date, 6,4)); // +45000;
		}
    }

function func_get_scheduled_reports(){
	
	$query= "select ms.id as id,msd.id as dateid,ms.reportid as reportid ,ms.type as type,ms.optionstr as optionstr,ms.nextrun as nextrun,ms.mailto ,msd.month as month,msd.day as day,msd.year as year
from mk_reports_schedule ms LEFT JOIN mk_reports_scheduledates msd ON msd.id=ms.dateid where ms.nextrun<unix_timestamp(now()) ";
   $result=db_query($query);
   $affiliates=array();
  if ($result)
 {
	$i=0;

	while ($row=db_fetch_array($result))
	{
		$affiliates[$i] = $row;
		$i++;
	}
   }
	
	return $affiliates;
}
function func_getallschedules($rid=""){
$commentQuery="select sc.id,rp.name as reportname,stype.name as type ,sdate.name as period,from_unixtime(lastrun+12.5*3600)as lastrun,
from_unixtime(nextrun+12.5*3600)as nextrun,sc.mailto as mailto
from mk_reports rp
INNER JOIN mk_reports_schedule sc ON rp.id=sc.reportid
INNER JOIN mk_reports_scheduletypes stype ON sc.type=stype.id 
LEFT JOIN mk_reports_scheduledates sdate ON sdate.id=sc.dateid";
if($rid != "")
  $commentQuery.=" where rp.id=$rid";
$commentQuery.=" order by rp.id ";
$commentresult=db_query($commentQuery);
$comments=array();
if ($commentresult)
	{
		$i=0;

		while ($row=db_fetch_array($commentresult))
		{
			$comments[$i] = $row;
			$i++;
		}
	}
	return $comments;
	
	
}
function func_get_filters($rid){
	
	$query= "select rf.name as name,rf.datatype_id as type ,rf.default_value,rd.name as dataname,rf.id from mk_reports_filters rf ,mk_reports_datatypes rd
	 where rf.datatype_id=rd.id and report_id='$rid' order by rf.id ";
   $result=db_query($query);
   $affiliates=array();
  if ($result)
 {
	$i=0;

	while ($row=db_fetch_array($result))
	{
		$affiliates[$i] = $row;
		$i++;
	}
   }
	
	return $affiliates;
}


function getorder_feedback_list(){
$query= "select orderid,login,concat(firstname,' ',lastname)as name,DATE_FORMAT(from_unixtime(date),'%d-%M-%Y')as orderdate from xcart_orders where status='C'
and (unix_timestamp(now())-completeddate)>2*604800 ";
   $result=db_query($query);
   $affiliates=array();
  if ($result)
 {
	$i=0;

	while ($row=db_fetch_array($result))
	{
		$affiliates[$i] = $row;
		$i++;
	}
   }
	
	return $affiliates;
}
function selfURL() { $s = empty($_SERVER["HTTPS"]) ? '' : ($_SERVER["HTTPS"] == "on") ? "s" : ""; $protocol = strleft(strtolower($_SERVER["SERVER_PROTOCOL"]), "/").$s; $port = ($_SERVER["SERVER_PORT"] == "80") ? "" : (":".$_SERVER["SERVER_PORT"]); return $protocol."://".$_SERVER['SERVER_NAME']; }
function strleft($s1, $s2) { return substr($s1, 0, strpos($s1, $s2)); }
function func_check_reminder_entry($orderid){
$check=FALSE;
$changerequest_query="select orderid from mk_pending_reminder where orderid='$orderid'";
$changerequest_result=db_query($changerequest_query);
$changerequest_row=db_fetch_row($changerequest_result);
$changerequest_status=$changerequest_row[0];
if(!empty($changerequest_status)){
	$check=TRUE;
}
return $check;
	}
	
?>
