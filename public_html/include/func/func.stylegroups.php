<?php
if (!defined('XCART_START'))
{
	header ("Location: ../");
	die ("Access denied");
}
/**
 * Load all the product type style groups
 *
 */
function load_groups_for_producttype($productTypeID){
	global $weblog,$sqllog,$sql_tbl;
	$groupsids ='';
	$weblog->info("Inside function with parmam ".$productTypeID." : ".__FUNCTION__." @ line ".__LINE__." in file ".__FILE__);
	$sqlQuery  = "SELECT style_group_id FROM mk_product_type_stylegroup_map WHERE product_type ='".intval($productTypeID)."' ";
	$sqllog->debug("SQL Query : ".$sqlQuery." "." @ line ".__LINE__." in file ".__FILE__);
	$resultSet = func_query($sqlQuery);
	if(!empty($resultSet)){
		foreach($resultSet as $_k=>$_groups){
			$groupsids.= $_groups['style_group_id']."_";
		}
		$groupsids = trim($groupsids,"_");
		$weblog->info("Group ids :  ".$groupsids." in  ".__FUNCTION__." @ line ".__LINE__." in file ".__FILE__);
		return $groupsids;
	}else{
		return false;
	}

}
/**
 * Load style customization image
 *
 */
function load_all_style_groupinfo($groupids, $accountType, $shopMasterId){
	global $weblog,$sqllog,$sql_tbl;
	$groupsarray = array();
	if(!empty($groupids)) {
		$groupids = str_replace("_",",",$groupids);
	}
	$weblog->info("Inside function with parmam ".$groupids." : ".__FUNCTION__." @ line ".__LINE__." in file ".__FILE__);
	$sqlQuery  = "SELECT sgm.mapid, sgm.name as style,sgm.style_group_id,sgm.style_id,sgm.is_default,ps.name as stylename,ps.id,ps.price,sg.`group_name`,
	ps.description 
	FROM mk_stylegroup_style_map as sgm 
	INNER JOIN mk_product_style
	as ps ON ps.id =sgm.style_id INNER JOIN `mk_style_group` as sg ON sg.`style_group_id` = sgm.style_group_id 
	WHERE sgm.style_group_id in (".($groupids).") AND ps.is_active =  1 AND ps.styletype = 'P'  ORDER BY sg.display_order ASC ";

	$sqllog->debug("SQL Query : ".$sqlQuery." "." @ line ".__LINE__." in file ".__FILE__);
	$resultSet = func_query($sqlQuery);

	if(!empty($resultSet)){
		foreach($resultSet as $_k => $_groupmapdata){
			$noRows = 0;
			$price = 0;
			if($accountType == 1)
			{
				$checkShopMastersql = "SELECT * FROM mk_shop_masters_catalog where product_style_id=".$_groupmapdata['style_id']." and shop_master_id = $shopMasterId and enable=1" ;
				$queryValid = db_query($checkShopMastersql);
				$noRows = db_num_rows($queryValid);
				$row=db_fetch_row($queryValid);
				$price = $row[3];
			}
			if($accountType == 1 && $noRows == 0)
				continue;
			else
			{
				$sql = "select bkgroundimage from mk_style_customization_area where style_id ='".intval($_groupmapdata['style_id'])."' and isprimary = '1' ";
				$_imagepath = func_query_first_cell($sql);
				$_groupmapdata['image_path'] = $_imagepath;
				if($accountType == 1 && $noRows != 0)
					$_groupmapdata['price'] = $price;
				$groupsarray[$_groupmapdata['style_group_id']][] = $_groupmapdata;
			}
		}
		return $groupsarray;
	}else{
		return false;
	}
}
/**
 * Load default customization image
 *
 */
function load_default_customization(){
	global $weblog,$sqllog,$sql_tbl;
	$weblog->info("Inside function with parmam ".$productTypeID." : ".__FUNCTION__." @ line ".__LINE__." in file ".__FILE__);
	$sqlQuery  = "SELECT style_group_id FROM mk_product_type_stylegroup_map WHERE product_type ='".intval($productTypeID)."'";
	$sqllog->debug("SQL Query : ".$sqlQuery." "." @ line ".__LINE__." in file ".__FILE__);
	$resultSet = func_query($sqlQuery);
	if(!empty($resultSet)){
		foreach($resultSet as $_k=>$_groups){
			$groupsids.= $_groups['style_group_id'].",";
		}
		$groupsids = trim($groupsids,",");
		$weblog->info("Group ids :  ".$groupsids." in  ".__FUNCTION__." @ line ".__LINE__." in file ".__FILE__);
		return $groupsids;
	}else{
		return false;
	}
}

/* loads all type of a group
 * @i/p:groupid
 * @o/p:types[] of the group
 */
function load_product_types_forgroup($group_id){
	global $weblog,$sqllog;
	$weblog->info("Inside function with parmam ".$group_id." : ".__FUNCTION__." @ line ".__LINE__." in file ".__FILE__);
	$query = "select lower(replace(pt.name,' ','-')) as type_name,pt.id as type_id 
				from mk_product_type pt  
				where pt.product_type_groupid = '$group_id' 
				and pt.is_active = '1' 
				and (pt.type = 'P' or pt.type = 'POS')"; //need to remove POS later
	$product_types = func_query($query);
	$sqllog->debug("SQL Query : ".$query." "." @ line ".__LINE__." in file ".__FILE__);
	return $product_types; 
}

/* load styles and style group info based on selected type's groupids
 * @i/p:groupids
 * @o/p:style_group_ino[] of the groups
 */
function load_styles_group_info_forgroups($groupids){
	global $weblog,$sqllog;
	$weblog->info("Inside function with parmam ".$groupids." : ".__FUNCTION__." @ line ".__LINE__." in file ".__FILE__);
	$query  = "SELECT sgm.name as style_map_name,sgm.style_group_id,sgm.is_default,
				ps.name as style_name,ps.id as style_id,ps.price as style_price,ps.description,
				si.image_path as style_image,
				sg.group_name as style_group_name					 
				FROM mk_stylegroup_style_map as sgm 
				INNER JOIN mk_product_style	as ps ON ps.id = sgm.style_id
				INNER JOIN mk_style_images	as si ON si.styleid = ps.id 
				INNER JOIN mk_style_group as sg ON sg.style_group_id = sgm.style_group_id 
				WHERE sgm.style_group_id in ($groupids) 
				AND ps.is_active =  1 
				AND ps.styletype = 'P'  
				AND sg.is_active = 1
				AND si.type='T'
				AND si.isdefault='Y'
				ORDER BY sg.display_order ASC ";
	$style_group_info = func_query($query);
	$sqllog->debug("SQL Query : ".$query." "." @ line ".__LINE__." in file ".__FILE__);
	return $style_group_info; 
}

/* load all customization areas information for the default style id
 * @i/p:styleid
 * @o/p:cust_areas[] of the style
 */
function load_custareas_forstyle($styleid){
	global $weblog,$sqllog;
	$weblog->info("Inside function with parmam ".$styleid." : ".__FUNCTION__." @ line ".__LINE__." in file ".__FILE__);
	$query = "select style_id,name as custarea_name,isprimary,image_t,image_l 
			from mk_style_customization_area where style_id='$styleid'"; 
	$style_cust_info = func_query($query);
	$sqllog->debug("SQL Query : ".$query." "." @ line ".__LINE__." in file ".__FILE__);
	return $style_cust_info; 
}
?>