<?php
include_once "$xcart_dir/include/class/class.ordertime.php";
include_once "$xcart_dir/include/func/func.mkcore.php";
include_once "$xcart_dir/include/func/func.order.php";
include_once "$xcart_dir/modules/EVoucher/EVoucherGenerator.php";
include_once "$xcart_dir/modules/coupon/database/CouponAdapter.php";
require_once "$xcart_dir/include/class/class.mail.multiprovidermail.php";
include_once "$xcart_dir/modules/coupon/SpecialCouponGenerator.php";

if (!defined('XCART_START'))
    {
    header ("Location: ../");
    die ("Access denied");
    }
x_load('mkcore','affiliates');
/**************************************************************************************************************************/
function func_load_payment_options_pos($shopMasterId)
{
	$aclient = SOAP_CLIENT::create_connection('http://localhost:8080/axis2/services/StoreService?wsdl'); // Path to wsdl declaring the service spec.
	$obj->shopMasterId = new SoapVar($shopMasterId, XSD_INT);
	$x = $aclient->getShopMasterdetails($obj);
	if (is_object($x->return))
	{
		$returnarray = array();
		$returnarray[0] = soap_object_to_array($x->return);
		return $returnarray;
	}
	else
	return(convertSoapObjectListToPHPArray($x->return));
}

function func_load_payment_options()                  
    {
		global $sql_tbl;
		$paymentTable        =$sql_tbl["payment_methods"];
		$query = "select $paymentTable.paymentid,$paymentTable.payment_method,$paymentTable.payment_details,
		$paymentTable.payment_template  from $paymentTable where $paymentTable.active='Y' and 
			$paymentTable.payment_method not in ( 'Phone Ordering','Money Order','Fax Ordering','Purchase Order')" ;
		$result     = db_query($query);
	    
	    $paymentMethods=array();

		if ($result)
		{
			$i=0;

			while ($row=db_fetch_row($result))
			{
				$paymentMethods[$i] = $row;
				$i++;
			}
		}

		return $paymentMethods;
    
     }

/**************************************************************************************************************************/



function func_get_customer_detail($login)
{
	global $sql_tbl;
	$tb_customer =$sql_tbl["customers"];
	if(!empty($login)){
		$query = "select 
	                    a.b_title,
	                    a.b_firstname,
	                    a.b_lastname,
	                    a.b_address,
	                    a.b_city,
	                    a.b_country,
	                    a.b_zipcode,
						a.b_state,
		                a.title,
	                    a.firstname,
	                    a.lastname,
	                    a.company,
		                a.s_title,
	                    a.s_firstname,
	                    a.s_lastname,
	                    a.s_address,
	                    a.s_city,
	                    a.s_country,
	                    a.s_state,
	                    a.s_zipcode,
		                a.email, 
	                    a.phone,
						a.mobile,
	                    a.fax,
	                    a.ship2diff ,
	                    a.account_type
	             from $tb_customer a 
	             where 
	                a.login='$login'"; 
		$result = db_query($query);
		$userdetail=array();
		if($result){
			$userdetail = db_fetch_array($result);
		}	
		return $userdetail;
	}else{
		return array();
	}	
	
}


function func_get_customer_detail_from_mobile($mobile)
{
	global $sql_tbl;
	$tb_customer =$sql_tbl["customers"];
	if(!empty($mobile)){
		$query = "select 
	                    a.b_title,
	                    a.b_firstname,
	                    a.b_lastname,
	                    a.b_address,
	                    a.b_city,
	                    a.b_country,
	                    a.b_zipcode,
						a.b_state,
		                a.title,
	                    a.firstname,
	                    a.lastname,
	                    a.company,
		                a.s_title,
	                    a.s_firstname,
	                    a.s_lastname,
	                    a.s_address,
	                    a.s_city,
	                    a.s_country,
	                    a.s_state,
	                    a.s_zipcode,
		                a.email, 
	                    a.phone,
						a.mobile,
	                    a.fax,
	                    a.ship2diff ,
	                    a.account_type
	             from $tb_customer a 
	             where 
	               a.mobile='$mobile'"; 
		$result = func_query($query);
		return $result;
	}else{
		return array();
	}	
	
}

/*********************************************************************/
/***********Address info from order table*****************************/

function func_customer_order_address_detail($login, $orderid)
{
    global $sql_tbl;
    $tb_orders =$sql_tbl["orders"];
    $query = "select a.b_firstname,
                    a.b_lastname,
                    a.b_address,
                    a.b_city,
                    a.b_country,
                    a.b_zipcode,
                    a.b_state,
                    a.b_email,
                    a.firstname,
                    a.lastname,
                    a.company, 
                    a.s_firstname,
                    a.s_lastname,
                    a.s_address,
                    a.s_locality,
                    a.s_city,
                    a.s_country,
                    a.s_state,
                    a.s_zipcode,
                    a.s_email,
                    a.login, 
                    a.phone,
                    a.mobile,
                    a.fax,
                    a.shop_bill_number,
                    c.mobile as user_mobile                    		
             from $tb_orders a , xcart_customers c
             where a.login = c.login and a.login='$login'
             and a.orderid='$orderid'"; 
    $result     = db_query($query);

     $userdetail=array();
    
    if($result)
    {

        $userdetail = db_fetch_array($result);
    }
    

    return $userdetail;
    
}

function func_customer_order_address_detail_by_orderid($orderid)
{
    global $sql_tbl;
    $tb_orders =$sql_tbl["orders"];
    $query = "select a.b_firstname,
                    a.b_lastname,
                    a.b_address,
                    a.b_city,
                    a.b_country,
                    a.b_zipcode,
                    a.b_state,
                    a.b_email,
                    a.firstname,
                    a.lastname,
                    a.company, 
                    a.s_firstname,
                    a.s_lastname,
                    a.s_address,
                    a.s_locality,
                    a.s_city,
                    a.s_country,
                    a.s_state,
                    a.s_zipcode,
                    a.s_email,
                    a.login, 
                    a.phone,
                    a.mobile,
                    a.fax,
                    a.shop_bill_number
             from $tb_orders a 
             where a.orderid='$orderid'";
    $result     = db_query($query);

     $userdetail=array();

    if($result)
    {
	$userdetail = db_fetch_array($result);
    }
    return $userdetail;
}

/******************************************************************/
function func_compute_shipping_cost($shipping_option)
{
	global $sql_tbl;
	$tb_shipping_rates =$sql_tbl["shipping_rates"];
	$query ="select  * from $tb_shipping_rates  where shippingid ='$shipping_option'";
	$result     = db_query($query);
		
    $shippingcost;
	
	if($result)
	{
		$row = db_fetch_array($result);
		$shippingcost =$row['rate'];
	}
	

	return $shippingcost;

}


function func_load_shipping_options()
{
	global $sql_tbl;
	$tb_shipping =$sql_tbl["courier_service"];
	$query="select * from $tb_shipping where enable_status=1 order by orderby";
	$shippingoptions = func_query($query);
	return $shippingoptions;
  
	
}


function func_get_product_customization_detail($productid)
{
	
   global $sql_tbl;
   $tb_product_customized_detail=$sql_tbl["mk_xcart_product_customized_area_rel"];
   $query ="select * from $tb_product_customized_detail where product_id ='$productid' ";
  
   $customizedDetail ;
   $result = db_query($query);
   if ($result)
   {
	    $i =0 ;
		while($row = db_fetch_array($result))
		{
			$customizedDetail[$i++]= $row;
		}
   }
   return $customizedDetail;
}

function func_get_product_customization_image_detail($productid)
{
	global $sql_tbl;
	$tb_product_customized_image_detail=$sql_tbl["mk_xcart_product_customized_area_image_rel"];
	$query="select style_id,area_id,area_image_final,templateid,addonid from $tb_product_customized_image_detail
	where product_id ='$productid'";
	$primages;
    $result = db_query($query);	
	
	if($result)
	{
		$i =0 ;
		while($row = db_fetch_array($result))
		{
			$primages[$i++]= $row;
		}
	
	}

	return $primages ;
}

function func_get_cod_order_mobile_verification_code( $orderid){
    global $sql_tbl;
    $tb_temporder =  $sql_tbl["temp_order"];
    $text = func_query_first_cell("SELECT parameter FROM $tb_temporder WHERE orderid='$orderid'");

    $fieldvalue = explode("||", $text);

	$customerinfo = array();

	foreach($fieldvalue  as $item)
	{
		list($key, $value) = explode('=>', $item, 2);
		$customerinfo[$key] = $value;
	}


    return $customerinfo[codVerificationCode];    
}

function func_cod_queue_order($orderid){    
    global $sql_tbl,$weblog,$sqllog;
    $order_time = ordertime::get_order_time_string($orderid);
	$order_time_data = ordertime::get_order_time_data($orderid);
    $oldstatus=func_query_first_cell("select status from xcart_orders where orderid=$orderid");
    db_query("update xcart_orders set status='Q' where orderid=$orderid");
    db_query("update xcart_orders set queueddate=unix_timestamp(now()) where orderid=$orderid");
    
    foreach($order_time_data as $key=>$val){
				if($key == "SHIPPED_TOGETHER"){
					$ship_together = $val;//get shipping option(1=together or 0=not together)
				}
			}
			if($ship_together == 1){
				foreach($order_time_data as $key=>$val){
					if($key != "SHIPPED_TOGETHER"){
						/*
						##calculate order processing time
						##based on the day of ship at 7pm
						*/
						$processing_time = ordertime::get_process_time_7pm($val['PROCESSING_DATE']);//$val['processing_date'] = date on which order can be PROCESSED
						$processing_time_sql = 'update '.$sql_tbl['order_details'].' set  estimated_processing_date ='.$processing_time.' where orderid ='.$orderid;
						db_query($processing_time_sql);
					}else
						continue;
				}
			}elseif($ship_together == 0){
				foreach($order_time_data as $key=>$val){
					if($key != "SHIPPED_TOGETHER"){
						foreach($val as $sval){
							/*
							##calculate order processing time
							##based on the day of ship at 7pm
							*/
							$processing_time = ordertime::get_process_time_7pm($sval['PRODUCT_PROCESSING_DATE']);//$Sval['processing_date'] = date on which order can be PROCESSED
							$processing_time_sql = 'update '.$sql_tbl['order_details'].' set  estimated_processing_date ='.$processing_time.' where itemid = '.$sval['PRODUCT_ITEMID'].' and orderid ='.$orderid;
							db_query($processing_time_sql);
						}
					}else
						continue;
				}
			}

         $cust_details=func_query_first("select login,firstname,from_unixtime(date) as date from xcart_orders where orderid=$orderid");
         $firstname=$cust_details[firstname];
         $args = array(
					//"ITEM_DETAIL"	=> $item_details,
                    "USER"		=> $firstname,
					"ORDERNO"		=> $orderid,
					"ORDERDATE" 	=> $cust_details['date'],
					"DELIVERY_DATE"	=> $order_time
					);
         $subjectarray = array( 	"ORDERNO" 	=> "#".$orderid);
         $template="codOrderQueued";
        $emailto=$cust_details[login];
        $from ="MyntraAdmin";
		sendMessageDynamicSubject($template, $args, $emailto ,$from,$subjectarray);

}



#
#Function for updating and collecting the shipping and billing address
#This function return array fo shipping and billing address
#
function func_update_billing_shipping_address($login, $orderid, $session_id)
{
	global $sql_tbl;
	global $state_array;
	global $sqllog;
	$tb_temporder =  $sql_tbl["temp_order"];
	$tb_customer =  $sql_tbl["customers"];

	$selecttemp = "SELECT parameter FROM $tb_temporder WHERE orderid='$orderid' AND sessionid='$session_id'";
	$sqllog->debug("File Name::>func.mk_orderbook.php##Function Name::>func_update_billing_shipping_address()####SQL Info::>".$selecttemp); 
	$tempresult = db_query($selecttemp);
	$row = db_fetch_array($tempresult);
	$text = $row['parameter'];
	$fieldvalue = explode("||", $text);

	$customerinfo = array();

	foreach($fieldvalue  as $item)
	{
		list($key, $value) = explode('=>', $item, 2);
		$customerinfo[$key] = $value;
	}

	//$customerinfo['s_country'] = ($customerinfo['s_country'] == 'In')?'India':$customerinfo['s_country'];
	//$customerinfo['b_country'] = ($customerinfo['b_country'] == 'In')?'India':$customerinfo['b_country'];
	//$paybyoption = $customerinfo['payby'];
	//$smarty->assign("paybyoption", $paybyoption);
	
	#	
	#Reterive user information
	#
	$customer = array();
	$customer = $customerinfo;
	$customer['s_state'] =  $customerinfo['s_state'];
	$customer['b_state'] =  $customerinfo['b_state'];
	//$customer['s_abbstate'] =  $customerinfo['s_state']; //for kiosk
	//$customer['b_abbstate'] =  $customerinfo['b_state']; //for kiosk
	#
	#gett the information of logged in user
	#
	$query = "SELECT email,firstname,lastname FROM $tb_customer WHERE login='".$login."' ";
	$sqllog->debug("File Name::>func.mk_orderbook.php##Function Name::>func_update_billing_shipping_address()####SQL Info::>".$query); 
	$rs_customer = db_query($query);
	$row_customer = db_fetch_array($rs_customer);
	$loginEmail = $row_customer['email'];
	$CustomerFirstName = $row_customer['firstname'];
	$CustomerLastName = $row_customer['lastname'];
	#end

	if($customerinfo['updatebill'] == "upbill")
	{    
		$baddress = mysql_escape_string($customerinfo[b_address]);
		$bcountry = mysql_escape_string($customerinfo[b_country]);
		$bcity = mysql_escape_string($customerinfo[b_city]);
		$bstate = mysql_escape_string($customerinfo[b_state]);
		$bfirstname = mysql_escape_string($customerinfo[b_firstname]);
		$blastname = mysql_escape_string($customerinfo[b_lastname]);

		$query = "UPDATE $tb_customer SET b_firstname='$bfirstname', b_lastname='$blastname',
				b_country='$bcountry', b_state='$bstate', b_city='$bcity',
				b_address='".$baddress."', b_zipcode='$customerinfo[b_zipcode]', mobile='$customerinfo[s_mobile]',
				phone='$customerinfo[s_phone]', ship2diff='N' WHERE login='$login'";
		$sqllog->debug("File Name::>func.mk_orderbook.php##Function Name::>func_update_billing_shipping_address()####SQL Info::>".$query);
		db_query($query);
	}

	if($customerinfo['billtoship'] == "btoship" && $customerinfo['updateship'] == "upship")
	{
		$saddress = mysql_escape_string($customerinfo[s_address]);
		$scountry = mysql_escape_string($customerinfo[s_country]);
		$scity = mysql_escape_string($customerinfo[s_city]);
		$sstate = mysql_escape_string($customerinfo[s_state]);
		$sfirstname = mysql_escape_string($customerinfo[s_firstname]);
		$slastname = mysql_escape_string($customerinfo[s_lastname]);
		
		$query = "UPDATE $tb_customer SET b_firstname='$sfirstname',
		b_lastname='$slastname', b_country='$scountry', b_state='$sstate', b_city='$scity',
		b_address='$saddress', b_zipcode='$customerinfo[s_zipcode]', mobile='$customerinfo[s_mobile]',
		phone='$customerinfo[s_phone]', ship2diff='Y' WHERE login='$login'";
		$sqllog->debug("File Name::>func.mk_orderbook.php##Function Name::>func_update_billing_shipping_address()####SQL Info::>".$query);
		db_query($query);

		$customer['b_firstname'] = $customerinfo['s_firstname'];
		$customer['b_lastname'] = $customerinfo['s_lastname'];
		$customer['b_address'] = $saddress;
		$customer['b_country'] = $customerinfo['s_country'];
		$customer['b_city'] = $customerinfo['s_city'];
		$customer['b_state'] = $customer['s_state'];
		$customer['b_phone'] = $customerinfo['s_phone'];
		$customer['b_mobile'] = $customerinfo['s_mobile'];
		$customer['b_email'] = $customerinfo['s_email'];
		$customer['b_zipcode'] = $customerinfo['s_zipcode'];
	}

	if($customerinfo['updateship'] == "upship")
	{
		$saddress = mysql_escape_string($customerinfo[s_address]);
		$scountry = mysql_escape_string($customerinfo[s_country]);
		$scity = mysql_escape_string($customerinfo[s_city]);
		$sfirstname = mysql_escape_string($customerinfo[s_firstname]);
		$slastname = mysql_escape_string($customerinfo[s_lastname]);
		$sstate = mysql_escape_string($customerinfo[s_state]);

		$query = "UPDATE $tb_customer SET s_firstname='$sfirstname', s_lastname='$slastname',
				s_country='$scountry', s_state='$sstate', s_city='$scity',
				s_address='$saddress', s_zipcode='$customerinfo[s_zipcode]', mobile='$customerinfo[s_mobile]',
				phone='$customerinfo[s_phone]' WHERE login='$login'";
		$sqllog->debug("File Name::>func.mk_orderbook.php##Function Name::>func_update_billing_shipping_address()####SQL Info::>".$query);
		db_query($query);
	}

	if($customerinfo['billtoship'] == "btoship" && $customerinfo['updateship'] != "upship")
	{
		$saddress = mysql_escape_string($customerinfo[s_address]);

		$customer['b_firstname'] = $customerinfo['s_firstname'];
		$customer['b_lastname'] = $customerinfo['s_lastname'];
		$customer['b_address'] = $saddress;
		$customer['b_country'] = $customerinfo['s_country'];
		$customer['b_city'] = $customerinfo['s_city'];
		$customer['b_state'] = $customer['s_state'];
		$customer['b_phone'] = $customerinfo['s_phone'];
		$customer['b_mobile'] = $customerinfo['s_mobile'];
		$customer['b_email'] = $customerinfo['s_email'];
		$customer['b_zipcode'] = $customerinfo['s_zipcode'];
	}
	//$customer['s_abbstate'] =  $customer['s_abbstate'] ; // for kiosk
	//$customer['b_abbstate'] =  $customer['b_abbstate'];

	return $customer;
}

#
#Function for generating the invoice id and for updating the order status.
#This function return invoice id
#

function func_generate_invoice_id($orderid, $orderstatus, $session_id, $process_cashback = true, $orderDetails=null)
{
	global $sql_tbl, $sqllog, $server_name; 
	$time = time();
    global $xcart_dir;
	$tb_orders = $sql_tbl["orders"];
	$tb_temp_invoice = $sql_tbl["temp_invoice"];
	
	#
	# generate new invoice number
	#
	$sql = "INSERT INTO $tb_temp_invoice(sessionid, orderid) VALUES('".$session_id."', '".$orderid."')";
	$sqllog->debug("File Name::>"."func.mk_orderbook.php"."##Function Name::>func_generate_invoice_id##"."##SQL Info::>".$sql); 
	db_query($sql);

	#
	#Fetch last inserted invoice id of current session id
	#
	$sql = "SELECT invoiceid FROM $tb_temp_invoice WHERE sessionid='".$session_id."' AND orderid='".$orderid."'";
	$sqllog->debug("File Name::>"."func.mk_orderbook.php"."##Function Name::>func_generate_invoice_id##"."##SQL Info::>".$sql); 
	$invoiceid = func_query_first_cell($sql);
	$invoiceid = "VEC_INV_".$invoiceid;

	$previous_status = array();
	//Getting previous state
	if($orderstatus=='Q' || $orderstatus=='OH') {
		if(empty($orderDetails)) {
			$previous_status=func_get_order_payment_method_and_status($orderid);
		} else {
			$previous_status['status'] = trim($orderDetails['status']);
			$previous_status['payment_method'] = trim($orderDetails['payment_method']);
		}
	}
	
	#
	#Update xcart_orders table with invoice number and order status
	#
	if($orderstatus=='Q'){
		//Earlir order date was also being reset here - removed on Friday aug 14 2009. Add back if needed.
		$invoice_sql = "UPDATE $tb_orders SET invoiceid = '".$invoiceid ."', status='".$orderstatus."',queueddate='".$time."', response_server='$server_name' WHERE orderid = '".$orderid."'";
		/*
			##update estimated processing time 
			##in order details based on shipping together
			##or not for an order
		 */
		
		/*require_once($xcart_dir.'/include/class/class.ordertime.php');
		$order_time_data = ordertime::get_order_time_data($orderid);		
		$ship_together = $order_time_data["SHIPPED_TOGETHER"];
		if($ship_together == 1){
			
			##calculate order processing time 
			##based on the day of ship at 7pm
			
			$processing_time = ordertime::get_process_time_7pm($order_time_data['DATA']['PROCESSING_DATE']);//$val['processing_date'] = date on which order can be PROCESSED
			$processing_time_sql = 'update '.$sql_tbl['order_details'].' set  estimated_processing_date ='.$processing_time.' where orderid ='.$orderid;
			db_query($processing_time_sql);						
		}elseif($ship_together == 0){
			foreach($order_time_data['DATA'] as $sval){
				
				##calculate order processing time 
				##based on the day of ship at 7pm
				
				$processing_time = ordertime::get_process_time_7pm($sval['PRODUCT_PROCESSING_DATE']);//$Sval['processing_date'] = date on which order can be PROCESSED
				$processing_time_sql = 'update '.$sql_tbl['order_details'].' set  estimated_processing_date ='.$processing_time.' where itemid = '.$sval['PRODUCT_ITEMID'].' and orderid ='.$orderid;
				db_query($processing_time_sql);	
			}	
		}*/
		/*======= estimated processing time updation ends here=========================*/
		$result =array();
		if(empty($orderDetails)) {
			$sql = "SELECT payment_method, login,(total + cash_redeemed) as total from xcart_orders where orderid=$orderid";
			$result = func_query_first($sql);
		} else {
			$result['payment_method'] = $orderDetails['payment_method'];
			$result['login'] = $orderDetails['login'];
			$result['total'] = $orderDetails['total'] + $orderDetails['cash_redeemed'];
		}
		$payment_method = $result['payment_method'];
		
		if($process_cashback && ($payment_method != 'cod'))	{
			//update the cashback on the order
			$cashback_gateway_status = FeatureGateKeyValuePairs::getFeatureGateValueForKey('cashbackenabled');
			$useGlobalCashback = false;
    		$enable_cashback_discounted_products = FeatureGateKeyValuePairs::getFeatureGateValueForKey('enableCBOnDiscountedProducts');
			if($enable_cashback_discounted_products == "true") {
    			$useGlobalCashback = true;
    		}
    		$adapter = CouponAdapter::getInstance();
    		$adapter->creditCashBackForOrderid($cashback_gateway_status, $useGlobalCashback, $result['login'], $result['total'], $orderid);
		} 
	}
	else{
		$invoice_sql = "UPDATE $tb_orders SET invoiceid = '".$invoiceid ."', status='".$orderstatus."', date='".$time."', response_server='$server_name' WHERE orderid = '".$orderid."'";
	}
	$sqllog->debug("File Name::>"."func.mk_orderbook.php"."##Function Name::>func_generate_invoice_id##"."##SQL Info::>".$sql); 
	db_query($invoice_sql);

	/*if((($orderstatus=='Q' || $orderstatus=='OH')&&($previous_status['status']=='PP'||$previous_status['status']=='PV'||$previous_status['status']=='PD'||$previous_status['status']=='D')) || ($previous_status['payment_method']=='cod' && $orderstatus=='OH' && $previous_status['status']=='PV')){
		require_once $xcart_dir.'/include/func/func_sku.php';
		decrement_sku($orderid);
	} */ 
    
	return $invoiceid;
}

#
#This function increases coupon locked times
#

function func_reduce_coupon_locked_times($login, $couponCode){
	global $sql_tbl;
	global $sqllog;

	$query = "UPDATE $sql_tbl[discount_coupons] SET times_locked = times_locked - 1 WHERE coupon ='".$couponCode."'";
	$sqllog->debug("File Name::>"."func.mk_orderbook.php"."##Function Name::>func_update_coupon_locked_times##"."##SQL Info::>".$query); 
	db_query($query);
}

#
#This function increases coupon locked times
#

function func_update_coupon_locked_times($login, $couponCode){
	global $sql_tbl;
	global $sqllog;

	$query = "UPDATE $sql_tbl[discount_coupons] SET times_locked = times_locked + 1 WHERE coupon ='".$couponCode."'";
	$sqllog->debug("File Name::>"."func.mk_orderbook.php"."##Function Name::>func_update_coupon_locked_times##"."##SQL Info::>".$query); 
	db_query($query);
}

#
#Function for generating the invoice id and for updating the order status.
#This function return invoice id
#

function func_update_coupon_used_times($login, $couponCode)
{
	global $sql_tbl;
	global $sqllog;
	
	$query = "SELECT * FROM $sql_tbl[discount_coupons_login] WHERE login ='".$login."' AND coupon='".$couponCode."' ";
	$sqllog->debug("File Name::>"."func.mk_orderbook.php"."##Function Name::>func_update_coupon_used_times##"."##SQL Info::>".$query); 
	$rsQuery= db_query($query);

		if(db_num_rows($rsQuery) > 0)
		{
			$query = "UPDATE $sql_tbl[discount_coupons_login] SET times_used = times_used + 1 WHERE coupon ='".$couponCode."' AND login='".$login."'"; 
			$sqllog->debug("File Name::>"."func.mk_orderbook.php"."##Function Name::>func_update_coupon_used_times##"."##SQL Info::>".$query); 
		   db_query($query);
		}
		else
		{
			$query = "INSERT INTO $sql_tbl[discount_coupons_login](coupon, login,times_used)
		   VALUES('".$couponCode."','".$login."',1)";
		   $sqllog->debug("File Name::>"."func.mk_orderbook.php"."##Function Name::>func_update_coupon_used_times##"."##SQL Info::>".$query); 
		   db_query($query);
		}

	/* update the times used */
	$query = "UPDATE $sql_tbl[discount_coupons] SET times_used = times_used + 1 WHERE coupon ='".$couponCode."'";
	$sqllog->debug("File Name::>"."func.mk_orderbook.php"."##Function Name::>func_update_coupon_used_times##"."##SQL Info::>".$query); 
	db_query($query);
}

#
#Function for setting the affiliate status
#
function func_affiliate_commission($affiliateInfo, $orderid, $total)
{
	global $sql_tbl;
	global $sqllog;
	global $weblog;

	###### insert data for affiliates in mk_affiliate_statistics 
	 $affiliateId = $affiliateInfo[0]['affiliate_id'];
	 if($affiliateInfo[0]['contentprovider'] == "Y")
	 {
		  $affiliateType = 'AffiliateContentUser';
	 }
	 else 
	 {
		  $affiliateType = 'AffiliateUser';
	 }
	$weblog->info("Called func_set_affiliate_commission for $affiliateType") ;
	func_set_affiliate_commission($affiliateId,$affiliateType,$total,$orderid); 
	$weblog->info("end");
}


#
#Function for sending the mail to designer regarding the sold design of him/her
#
function func_send_mail_to_designer_regarding_sold_design($productsInCart, $orderid,$amtTotal)
{
	global $sql_tbl;
	global $sqllog;
	global $weblog,$http_location;
	$productid ;
	$designer ='';
	$product ='';
	$productTypeNames ='';

	#
	#Load buyer login id
	#
	$sql = "SELECT login FROM $sql_tbl[orders] WHERE orderid='$orderid'";
	$sqllog->debug("File name::>func.mk_orderbook.php##Function Name::>func_send_mail_to_designer_regarding_sold_design##SQL Info::>$sql");
	$buyer = func_query_first_cell($sql);
    ######### Check if order total is greater than 0 ##############
	$weblog->info("Check the amount total for before sending mail to designer total : $amtTotal  File : ".__FILE__);
	if($amtTotal  > 0)
	{
		 ##### Setting the status , if comm is generated or not ###
		 $SQL = "SELECT comm_generated_status FROM ".$sql_tbl['comm_generated_tracking']." WHERE 
		 orderid = '".intval($orderid)."' AND comm_generated_for = 'DESIGNER' AND comm_generated_status = 'Y' ";
		 if(db_num_rows(db_query($SQL)) == 0){
			//FOR DESIGNER WE HAVE TO RETAIN THIS
			foreach($productsInCart AS $key=>$value)
			{
					$productDetail = $productsInCart[$key];
					$productid =$productDetail['productId'];			
			  /* get all the product type name for affiliates*/
					$productTypeNames .=  $productDetail['productTypeLabel'].",";	 
			// Send mail to those designer those designs has sold

					$userData = db_query("SELECT c.email, c.firstname, c.login,p.is_publish,p.designer
										FROM $sql_tbl[customers] as c INNER JOIN $sql_tbl[products] as p
										WHERE c.login = p.provider
										AND p.productid= $productid");
					$row = db_fetch_array($userData);

					$email = $row['email'];
					$firstName = $row['firstname'];
					$userid = $row['login'];
					$ispublish = $row['is_publish'];
					$designerID = $row['designer'];
					if($buyer !=  $userid  && $ispublish ==1)
					{
						$prdData = func_query_first("SELECT image_portal_t,product FROM $sql_tbl[products] WHERE productid= $productid");
						$image = substr($prdData['image_portal_t'], 2);
						$prdNameToSend = ($prdData['product']);
						$template = "boughtyourdesign";

						/********* setting the  url  **********/
						$ptypeid = $productDetail['producTypeId'];
						$pstyleid =$productDetail['productStyleId'];
						$productTypeLabel =   $productDetail['productTypeLabel'];
						/*$pageURL = 'http';
						 if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
						 $pageURL .= "://";
						 if ($_SERVER["SERVER_PORT"] != "80") {
						  $pageURL .= $_SERVER["SERVER_NAME"].$pageUri;
						 } else {
						  $pageURL .= $_SERVER["SERVER_NAME"].$pageUri;
						 }*/
						
						$pageURL = $http_location."/".str_replace(" ","-",$prdNameToSend)."/".$productTypeLabel."/FC/PD/".$productid;
						/******** end  ******/

						$args = array( "FIRST_NAME" => $firstName,
											"YOUR_DESIGN" => $pageURL,
											"USER_NAME" => $login);
								
						$weblog->info("In mkorderbook.php Send mail to designer if his/her product is bought by anybody with URL ".$pageURL." @line in file ".__FILE__);		
						sendMessage($template, $args, $email);
						$weblog->info("mail sent to designer");
				 }
				 if($ispublish ==1){
				 	$weblog->info("Calling Funtion setCommissionForDesigner($productDetail,$orderid,$amtTotal,$designerID) @line ".__LINE__. " in file ".__FILE__);
					setCommissionForDesigner($productDetail,$orderid,$amtTotal,$designerID);
					$weblog->info("Generated the commission");
					
				 }
			}
			$weblog->info("Insert the data if commission is not generated for affiliate");
			$query = "INSERT INTO $sql_tbl[comm_generated_tracking](orderid,comm_generated_for,comm_generated_status) VALUES('". $orderid."','DESIGNER','Y')";
			db_query($query);
	     } // end of check if comm is paid or not 
	 }
	 
}

#
#Function for sending the mail to affiliate user
#

function func_send_mail_to_affiliate_user($affiliateInfo, $productsInCart, $orderid)
{
	global $sql_tbl;
	global $sqllog;
	global $weblog;
    
	#
	#Load user first name and order amount total
	#
	$sql = "SELECT login, firstname, total FROM $sql_tbl[orders] WHERE orderid = '$orderid'";
	$order_detail = func_query_first($sql);
	$CustomerFirstName = $order_detail['firstname'];
	$total = $order_detail['total'];
	$login = $order_detail['login'];
	$affiliateId = $affiliateInfo[0]['affiliate_id'];

	 $weblog->info("send mail to affiliate on successful order placed in mkorderbook.php  " .$affiliateInfo[0]['contentprovider']);	
	 
	 #
	 # Load all the mail templates of the affiliate 
	 #
	 $emailTemplates = array();
	 $sql_templates = "SELECT * FROM $sql_tbl[affiliate_email_map] 
	                    WHERE affiliate_id = '".$affiliateId."'";
	 $rsTemplates = func_query($sql_templates);
     foreach($rsTemplates as $key => $templates)
     {
         $emailTemplates[$templates[email_event]] =  $templates[email_template] ;  	
     }
	 	 
	// $template = "affiliateorderplaced";
	 $template = $emailTemplates['AFF_ORDER_PLACED'];
	 $affiliateName = $affiliateInfo[0]['company_name'];
	 if($affiliateInfo[0]['contentprovider'] == "Y")
	 {
			  $affiliateType = 'AffiliateContentUser';
	 }
	 else 
	 {
			  $affiliateType = 'AffiliateUser';
	 }
	 $affiliateComm = 0;
				 
	 
	 $from ="MyntraAdmin";
	 $weblog->info("Order id >> $orderid");
	 foreach($productsInCart AS $key=>$value)
	 {
		$productDetail = $productsInCart[$key];
		$productTypeNames =$productDetail['productTypeLabel'];		
		$productTypeNames = trim($productTypeNames,",");
	 }
	
	 $weblog->info("details send to affiliate : aff name:$affiliateName 
	 customer:$CustomerFirstName  user mail : $login  order id : $orderid total amt : $total
	 product name : $productTypeNames comm_amt : $affiliateComm");	
	 
	 $subjectArgs = array("AFFILIATE_NAME" =>  $affiliateName) ;
	 $args = array( "AFFILIATE_NAME" => $affiliateName,
					"CUSTOMER_NAME" => $CustomerFirstName,
					"USER_EMAIL"	=> $login,
					"ORDER_ID" =>      $orderid, 
					"TOTAL_AMT"	 =>    $total ,
					"PRODUCT_NAMES" => $productTypeNames ,
					"COMM_AMT"      => $affiliateComm,
					"FIRST_NAME" => $CustomerFirstName,
				) ;
	 $affiliatemailto = $affiliateInfo[0]['contact_email'];		
	 $from='MyntraAdmin';		
	 $weblog->info("Calling sendMessageDynamicSubject function in mkorderbook.php");
	 sendMessageDynamicSubject($template, $args, $affiliatemailto,$from, $subjectArgs);
	 $weblog->info("End : function call for sendMessageDynamicSubject ended.");
	 
	 ######## Send mail first time to registered affiliate user ##########
	 if($affiliateInfo[0]['isNewUser'] == 'Y') 
	 {
		 $weblog->info("Invoke Query to fetch the password of logged in user ($login)
		 in mkorderbook.php");
		 $rsFetch = db_query("SELECT password FROM $sql_tbl[customers] WHERE 
		 login = '".$login."'");
		 $row = db_fetch_array($rsFetch);
		 $userPass = text_decrypt($row['password']);
		 $weblog->info("User password is $userPass in mkorderbook.php");
		 
		 $weblog->info("Send mail to new registered affiliate user with 
		 customer name :$CustomerFirstName,user name : $login and user pass : $userPass
		 and affiliate name : $affiliateName in mkorderbook.php");
		  //$template = "newaffiliateregistration";
		 $template = $emailTemplates['AFF_NEW_USER'];
		 $mailto = $login;
		 $from = "MyntraAdmin";
		 $args = array("FIRST_NAME" => $CustomerFirstName,
						 "USER_NAME" => $login,
						 "USER_PASSWORD" => $userPass ,
						 "ORDER_ID"  => $orderid,
						 "AFFILIATE_NAME" =>  $affiliateName,
						 "DAYS"           => "5 business days"
					  );
		  sendMessage($template, $args,$mailto,$from);
		  $weblog->info("Mail sent to new registered affiliate user
		  in mkorderbook.php");
	}
	
	######## If already registered ##########
	else 
	{
		//$template =  "affiliateuserorder";
		$template = $emailTemplates['AFF_REG_USER'];
		$weblog->info("Send mail to new registered affiliate user with 
		customer name :$CustomerFirstName,user name : $login 
		and affiliate name : $affiliateName in mkorderbook.php");
		$mailto = $login;
		$args = array("FIRST_NAME" => $CustomerFirstName,
					   "ORDER_ID"  => $orderid,
					   "AFFILIATE_NAME" =>  $affiliateName,
					   "DAYS"           => "5 business days",
					   "USER_NAME"   => $login
					 );
		sendMessage($template, $args,$mailto,$from);
		$weblog->info("Mail sent to new registered affiliate user
		in mkorderbook.php");
	}
		
}


function func_send_order_confirmation_for_reebok_store($affiliateInfo, $productsInCart, $orderid,$shippingdate,$template)
{
		global $sql_tbl;
		global $sqllog;
		global $weblog;
		global $http_location;
		$params = array();
		$params['myntra_banner'] = "<img src='".$http_location."/skin1/affiliatetemplates/images/reebok_ipl/header.jpg' border=0><br/>";
		$sql = "SELECT login, firstname,subtotal,coupon_discount as discount,total,tax,shipping_cost,gift_charges,date_format(from_unixtime(date),'%d-%m-%y') as date,b_address,b_city,b_state,b_zipcode,b_country,s_address,s_city,s_zipcode,s_country,s_zipcode,s_state  FROM $sql_tbl[orders] WHERE orderid = '$orderid'";
		$order_detail = func_query_first($sql);
		$params['customer_name']=$order_detail['firstname'];
		$params['myntra_generated_order_id']=$orderid;
		$params['order_submission_date'] = $order_detail['date'];
		$params['billing_address'] =$order_detail['b_address'].'<br/>'.$order_detail['b_city'].'<br/>'.$order_detail['b_state'].'<br/>'.$order_detail['b_country'].'<br/>'.$order_detail['b_zipcode'];
		$params['shipping_address'] =$order_detail['s_address'].'<br/>'.$order_detail['s_city'].','.$order_detail['s_state'].','.$order_detail['s_country'].'<br/>'.$order_detail['s_zipcode'];
		$params['shipdate']=$shippingdate;
		$toemail = $order_detail['login'];
		
		$params['taxes']=$order_detail['tax'];
		$params['discount']=$order_detail['discount'];
		$params['total_price']=$order_detail['subtotal'];
		$params['shipping']=$order_detail['shipping_cost'];
		$params['gift_charges']=$order_detail['gift_charges'];
		$itemdetails='';
		$count=0;
		foreach($productsInCart AS $key=>$value)
		{
			$productDetail = $productsInCart[$key];
			$productstyle =$productDetail['productStyleName'];	
			$quantity=$productDetail['quantity'];	
			$price=$productDetail['totalPrice'];
			$sql ="select text from mk_xcart_product_customized_area_rel where product_id=".$productDetail['productId'];
			$textdetail = func_query_first($sql);
			$textarray=explode("@",$textdetail['text']);
			$count++;
			$sql ="select quantity_breakup from xcart_order_details where productid=".$productDetail['productId'].
				" and orderid=".$orderid;
			$sizedetail = func_query_first($sql);
			$sizearray =explode(',',$sizedetail['quantity_breakup']);
			$size="";
			for($i=0; $i < count($sizearray); $i++)
			{
				$sizes= explode(':',$sizearray[$i]);
				if($sizes[1] >0)
				{
					$size=$size.$sizes[0]." ";
				}
			}
			$itemdetails=$itemdetails."<br><b>Item".$count."): </b><br>";
			$itemdetails=$itemdetails."<b>&nbsp;&nbsp;Team:</b> ".$productstyle."<br>";
			$itemdetails=$itemdetails."<b>&nbsp;&nbsp;Size:</b> ".$size."<br>";
			$itemdetails=$itemdetails."<b>&nbsp;&nbsp;Quantity:</b> ".$quantity."<br>";
			$itemdetails=$itemdetails."<b>&nbsp;&nbsp;Name to be printed:</b> ".strtoupper($textarray[0])."<br>";
			$itemdetails=$itemdetails."<b>&nbsp;&nbsp;Number to be printed:</b> ".$textarray[1]."<br>";
			$itemdetails=$itemdetails."<b>&nbsp;&nbsp;Price (for Quantity):</b> Rs.".$price."<br><br>";
			
		}

		$params['item_detail']=$itemdetails;
		$params['total_amount']=number_format($order_detail['total']+$order_detail['tax']+$order_detail['shipping_cost'],2);
		$subjectArgs = array("orderid" =>  $orderid) ;

		sendMessageDynamicSubject($template, $params, $toemail,'Reebok Online Store ', $subjectArgs);
 
}


function func_send_shipment_confirmation_for_reebok_store($productsInCart,$orderid,$trackingnumber,$courier_service,$courier_website)
{
		global $sql_tbl;
		global $sqllog;
		global $weblog;
		global $http_location;
		$params = array();
		$params['myntra_banner'] = "<img src='".$http_location."/skin1/affiliatetemplates/images/reebok_ipl/header.jpg' border=0><br/>";
		$sql = "SELECT login, firstname,subtotal,coupon_discount as discount,total,tax,shipping_cost,gift_charges,date_format(from_unixtime(date),'%d-%m-%y') as date,b_address,b_city,b_state,b_zipcode,b_country,s_address,s_city,s_zipcode,s_country,s_zipcode,s_state  FROM $sql_tbl[orders] WHERE orderid = '$orderid'";
		$order_detail = func_query_first($sql);
		$params['customer_name']=$order_detail['firstname'];
		$params['myntra_generated_order_id']=$orderid;
		$params['order_submission_date'] = $order_detail['date'];
		$params['billing_address'] =$order_detail['b_address'].'<br/>'.$order_detail['b_city'].'<br/>'.$order_detail['b_state'].'<br/>'.$order_detail['b_country'].'<br/>'.$order_detail['b_zipcode'];
		$params['shipping_address'] =$order_detail['s_address'].'<br/>'.$order_detail['s_city'].','.$order_detail['s_state'].','.$order_detail['s_country'].'<br/>'.$order_detail['s_zipcode'];
		$params['shipdate']=$shippingdate;
		$toemail = $order_detail['login'];
		
		$params['taxes']=$order_detail['tax'];
		$params['discount']=$order_detail['discount'];
		$params['total_price']=$order_detail['subtotal'];
		$params['shipping']=$order_detail['shipping_cost'];
		$params['gift_charges']=$order_detail['gift_charges'];
		$itemdetails='';
		$count=0;
		foreach($productsInCart AS $key=>$value)
		{
			$productDetail = $productsInCart[$key];
			$productstyle =$productDetail['productStyleName'];	
			$quantity=$productDetail['quantity'];	
			$price=$productDetail['totalPrice'];
			$sql ="select text from mk_xcart_product_customized_area_rel where product_id=".$productDetail['productId'];
			$textdetail = func_query_first($sql);
			$textarray=explode("@",$textdetail['text']);
			$count++;
			$sql ="select quantity_breakup from xcart_order_details where productid=".$productDetail['productId'].
				" and orderid=".$orderid;
			$sizedetail = func_query_first($sql);
			$sizearray =explode(',',$sizedetail['quantity_breakup']);
			$size="";
			for($i=0; $i < count($sizearray); $i++)
			{
				$sizes= explode(':',$sizearray[$i]);
				if($sizes[1] >0)
				{
					$size=$size.$sizes[0]." ";
				}
			}
			$itemdetails=$itemdetails."<br><b>Item".$count."): </b><br>";
			$itemdetails=$itemdetails."<b>&nbsp;&nbsp;Team:</b> ".$productstyle."<br>";
			$itemdetails=$itemdetails."<b>&nbsp;&nbsp;Size:</b> ".$size."<br>";
			$itemdetails=$itemdetails."<b>&nbsp;&nbsp;Quantity:</b> ".$quantity."<br>";
			$itemdetails=$itemdetails."<b>&nbsp;&nbsp;Name to be printed:</b> ".strtoupper($textarray[0])."<br>";
			$itemdetails=$itemdetails."<b>&nbsp;&nbsp;Number to be printed:</b> ".$textarray[1]."<br>";
			$itemdetails=$itemdetails."<b>&nbsp;&nbsp;Price (for Quantity):</b> Rs.".$price."<br><br>";
			
		}

		$params['item_detail']=$itemdetails;
		$params['courier-name']=$courier_service;
		$params['courier-website']=$courier_website;
		$params['tracking-number']=$trackingnumber;
		$params['total_amount']=number_format($order_detail['total']+$order_detail['tax']+$order_detail['shipping_cost'],2);
		$subjectArgs = array("orderid" =>  $orderid) ;

		sendMessageDynamicSubject('reebok_jersey_shipped', $params, $toemail,'Reebok Online Store', $subjectArgs);
 
}
#
#Function for seding the mail to referee regarding earnesd amount
#

function func_send_mail_referree_regarding_earned_amount($login, $amount)
{
	global $sql_tbl;
	global $sqllog;
	global $weblog;

	$sql = "SELECT login,referralid FROM $sql_tbl[referee_map] WHERE email='$login'";
	$sqllog->debug("File Name::>"."func.mk_orderbook.php"."##Function Name::>func_send_mail_referree_regarding_earned_amount##"."##SQL Info::>".$sql); 
	$result = func_query_first($sql);
				
	if($result['login'] != "")
	{
		 $email = $result['login'];
		 $refid = $result['referralid'];
		 
		 #
		 #load login customer first name
		 #
		 $firstname = func_query_first("SELECT firstname FROM $sql_tbl[customers] WHERE login='$login'");
		 $CustomerFirstName = $firstname['firstname'];
			
		 #
		 #load referree customer first name
		 #
		$firstname = func_query_first("SELECT firstname FROM $sql_tbl[customers] WHERE
										login=(SELECT login FROM $sql_tbl[referee_map] WHERE email='$login')");
		 $referree_fname = $firstname['firstname'];

		 $earnedamount = func_insert_referred_amount($email, $refid, $amount);
		 if($earnedamount>0)
		{
			 $template = "referalbuy";
			 $from ="MyntraAdmin";
			 $args = array( "NAME" =>  $referree_fname,
							"XXX"  =>$amount,
							"YYY"  =>$earnedamount,
							"FRIEND_NAME" => $CustomerFirstName
							);
			 sendMessage($template, $args, $email, $from);
		}
	}
}

#
#Function for creating the array of all products of that order
#
function func_create_array_products_of_order($orderids, $productsInCartTemp=array(), $active_items=true, $itemids = false)
{
	global $sql_tbl;
	global $sqllog;
	global $weblog;
 	if(!is_array($orderids))
 		$orderids = array($orderids);
 
    $totalQtyInOffer = 0;
    $productsInCart = array();

    $showGovtTax = \WidgetKeyValuePairs::getWidgetValueForKey('oms.invoiceShowGovtTax');
    if ($showGovtTax == NULL) {
        $showGovtTax = false;
    } else if ($showGovtTax == "true") {
        if ($active_items) {
            $sql .= "SELECT o.gift_card_amount,o.warehouseid,od.product_style as style_id,od.orderid, od.itemid, od.item_status, od.difference_refund, od.tax_rate, od.cart_discount_split_on_ratio as cart_discount_split_on_ratio, actual_product_price, product, productid AS productId, product_style AS productStyleId, product_type AS producTypeId, price AS productPrice, amount AS quantity, (price * amount) AS totalPrice, total, od.discount, o.discount  AS totaldiscount, od.quantity_breakup, promotion_id, addonid, od.pos_unit_mrp, od.pos_total_amount, od.price as unitPrice, od.taxamount as taxamount, od.coupon_discount_product as coupon_discount, od.item_loyalty_points_used, o.loyalty_points_conversion_factor, o.cash_redeemed as cashdiscount, o.cash_coupon_code as cashCouponCode, o.payment_surcharge as payment_surcharge, total_amount, som.sku_id, od.discount_quantity, od.supply_type, od.seller_id, od.govt_tax_rate, od.govt_tax_amount FROM (((xcart_orders o LEFT JOIN xcart_order_details od on o.orderid = od.orderid) LEFT JOIN mk_order_item_option_quantity oq on od.itemid = oq.itemid) LEFT JOIN mk_styles_options_skus_mapping som on oq.optionid = som.option_id) where od.orderid in (" . implode(",", $orderids) . ") AND od.item_status != 'IC'";
        } else {
            $sql .= "SELECT o.gift_card_amount,o.warehouseid,od.product_style as style_id,od.orderid, od.itemid, od.item_status, od.difference_refund, od.tax_rate, od.cart_discount_split_on_ratio as cart_discount_split_on_ratio, actual_product_price, product, productid AS productId, product_style AS productStyleId, product_type AS producTypeId, price AS productPrice, amount AS quantity, (price * amount) AS totalPrice, total, od.discount, o.discount  AS totaldiscount, od.quantity_breakup, promotion_id, addonid, od.pos_unit_mrp, od.pos_total_amount, od.price as unitPrice, od.taxamount as taxamount, od.coupon_discount_product as coupon_discount, od.item_loyalty_points_used, o.loyalty_points_conversion_factor, o.cash_redeemed as cashdiscount, o.cash_coupon_code as cashCouponCode, o.payment_surcharge as payment_surcharge, total_amount, som.sku_id, od.discount_quantity, od.supply_type, od.seller_id, od.govt_tax_rate, od.govt_tax_amount FROM (((xcart_orders o LEFT JOIN xcart_order_details od on o.orderid = od.orderid) LEFT JOIN mk_order_item_option_quantity oq on od.itemid = oq.itemid) LEFT JOIN mk_styles_options_skus_mapping som on oq.optionid = som.option_id) where od.orderid in (" . implode(",", $orderids) . ") AND od.item_status = 'IC'";
        }
    }
    if (!$showGovtTax || $showGovtTax == "false") {
        if ($active_items) {
            $sql .= "SELECT o.warehouseid,od.product_style as style_id,od.orderid, od.itemid, od.item_status, od.difference_refund, od.tax_rate, od.cart_discount_split_on_ratio as cart_discount_split_on_ratio, actual_product_price, product, productid AS productId, product_style AS productStyleId, product_type AS producTypeId, price AS productPrice, amount AS quantity, (price * amount) AS totalPrice, total, od.discount, o.discount  AS totaldiscount, od.quantity_breakup, promotion_id, addonid, od.pos_unit_mrp, od.pos_total_amount, od.price as unitPrice, od.taxamount as taxamount, od.coupon_discount_product as coupon_discount, od.item_loyalty_points_used, o.loyalty_points_conversion_factor, o.cash_redeemed as cashdiscount, o.cash_coupon_code as cashCouponCode, o.payment_surcharge as payment_surcharge, total_amount, som.sku_id, od.discount_quantity, od.supply_type, od.seller_id FROM (((xcart_orders o LEFT JOIN xcart_order_details od on o.orderid = od.orderid) LEFT JOIN mk_order_item_option_quantity oq on od.itemid = oq.itemid) LEFT JOIN mk_styles_options_skus_mapping som on oq.optionid = som.option_id) where od.orderid in (" . implode(",", $orderids) . ") AND od.item_status != 'IC'";
        } else {
            $sql .= "SELECT o.warehouseid,od.product_style as style_id,od.orderid, od.itemid, od.item_status, od.difference_refund, od.tax_rate, od.cart_discount_split_on_ratio as cart_discount_split_on_ratio, actual_product_price, product, productid AS productId, product_style AS productStyleId, product_type AS producTypeId, price AS productPrice, amount AS quantity, (price * amount) AS totalPrice, total, od.discount, o.discount  AS totaldiscount, od.quantity_breakup, promotion_id, addonid, od.pos_unit_mrp, od.pos_total_amount, od.price as unitPrice, od.taxamount as taxamount, od.coupon_discount_product as coupon_discount, od.item_loyalty_points_used, o.loyalty_points_conversion_factor, o.cash_redeemed as cashdiscount, o.cash_coupon_code as cashCouponCode, o.payment_surcharge as payment_surcharge, total_amount, som.sku_id, od.discount_quantity, od.supply_type, od.seller_id FROM (((xcart_orders o LEFT JOIN xcart_order_details od on o.orderid = od.orderid) LEFT JOIN mk_order_item_option_quantity oq on od.itemid = oq.itemid) LEFT JOIN mk_styles_options_skus_mapping som on oq.optionid = som.option_id) where od.orderid in (" . implode(",", $orderids) . ") AND od.item_status = 'IC'";
        }
    }
    if($itemids){
    	$sql .= " AND od.itemid in (".implode(",", $itemids).")";	
    }
    
    $productsInCart = func_query($sql);

	$productids = array();
	foreach($productsInCart as $key => $order)
	{
		$productsInCart[$key]['totalProductPrice'] = $order['totalPrice'];
		$productsInCart[$key]['couponDiscount'] = $order['coupon_disount'];
		$productsInCart[$key]['cartDiscount'] = $order['cart_discount_split_on_ratio'];
		$productsInCart[$key]['totalMyntCashUsage'] = $order['cashdiscount'];
		$productsInCart[$key]['loyalty_credit'] = $order['item_loyalty_points_used']*$order['loyalty_points_conversion_factor'];
		
        $productsInCart[$key]['discountAmount'] = $order['discount'];
		//Query for retriving the style name from mk_product_style
		$stylename = "SELECT name, styletype FROM $sql_tbl[mk_product_style] WHERE id = '".$productsInCart[$key]['productStyleId']."'";
		$styleresult = db_query($stylename);
		$rowstyle = db_fetch_array($styleresult);

		//Query for retriving the style options from mk_product_options
		 $styleoption = "SELECT $sql_tbl[mk_product_options].name AS optionName, $sql_tbl[mk_product_options].value AS optionValue FROM $sql_tbl[mk_product_options], $sql_tbl[mk_product_options_order_details_rel]
		WHERE $sql_tbl[mk_product_options].id = $sql_tbl[mk_product_options_order_details_rel].option_id
		AND $sql_tbl[mk_product_options].style = ".$productsInCart[$key]['productStyleId']."
		AND $sql_tbl[mk_product_options_order_details_rel].orderid = '".$_GET['orderid']."'
		AND $sql_tbl[mk_product_options_order_details_rel].itemid = '".$productsInCart[$key]['productId']."'";

		$optionresult = db_query($styleoption);
		$optionCount = db_num_rows($optionresult);
		$productsInCart[$key]['optionCount'] = $optionCount;
		$i = 0;
		while($optionrow = db_fetch_array($optionresult))
		{
			$productsInCart[$key]['optionNames'][$i] =  $optionrow['optionName'];
			$productsInCart[$key]['optionValues'][$i] =  $optionrow['optionValue'];
			$i++;
		}

		$typename = "SELECT pt.name as productName, pt.label as pLabel, pt.image_t as pImage, sp.global_attr_article_type as article_type, 
					ps.name as sName, substr(sp.article_number,1,10) as article_number, sp.product_display_name, mcc.typename 		
			FROM $sql_tbl[producttypes] AS pt, $sql_tbl[mk_product_style] AS ps, mk_style_properties sp, mk_catalogue_classification mcc
			WHERE pt.id = ps.product_type and sp.style_id = ps.id
			AND sp.global_attr_article_type = mcc.id 
			AND pt.id = ps.product_type AND ps.id=".$productsInCart[$key]['productStyleId']."";

			$typeresult = db_query($typename);
			$row = db_fetch_array($typeresult);
			
			if(isset($tempStr))	{
				$tempStr.=", ";	
			}
			$tempStr .= $productsInCart[$key]['productStyleId'];

			$productsInCart[$key]['productStyleName'] = $row['sName'];
			$productsInCart[$key]['productTypeLabel'] = $row['productName'];
			$productsInCart[$key]['article_number'] = $row['article_number'];
			$productsInCart[$key]['article_type'] = $row['article_type'];
			$productsInCart[$key]['product_display_name'] = $row['product_display_name'];
			$productsInCart[$key]['typename'] = $row['typename'];
			
			if($productsInCart[$key]['addonid'])
			{
				$addonname = get_style_addon_name($productsInCart[$key]['addonid']);
				if($addonname)
				{
					$productsInCart[$key]['productStyleName'] .= " ( $addonname ) ";
				}
			}

			$areaId = func_get_all_orientation_for_default_customization_area($productsInCart[$key]['productStyleId']);

			$productsInCart[$key]['defaultCustomizationId'] = $areaId[0][6];

			$pid = $productsInCart[$key]['productId'];
			$defaultImage = "SELECT image_portal_t  as ProdImage FROM $sql_tbl[products] WHERE productid =$pid";

			$ImageResult = db_query($defaultImage);
			$ImageRow = db_fetch_array($ImageResult);
	
			$productsInCart[$key]['productStyleType'] = $rowstyle['styletype'];
			$productsInCart[$key]['productTypeLabel'] = $row['pLabel'];
			$productsInCart[$key]['designImagePath'] = $ImageRow['ProdImage'];
										
			//$productsInCart[$key]['custAreaCount'] = $productsInSess[$productsInCart[$key]['productId']]['custAreaCount'];	
			
			//size and quantity information to be stored with each product
			if(!empty($productsInCartTemp)){
				$flattenSizeOptions = $productsInCartTemp[$order['productId']]["flattenSizeOptions"];
				$sizenamesunified=$productsInCartTemp[$order['productId']]["sizenamesunified"];
				$sizenames=$productsInCartTemp[$order['productId']]["sizenames"];
				$sizequantities=$productsInCartTemp[$order['productId']]["sizequantities"];
	        	foreach($sizequantities as $i => $j){
	            	if($j){
	    	        	if($flattenSizeOptions){
		            		$productsInCart[$key]["final_size"]= $sizenamesunified[$i];
		            		$productsInCart[$key]["final_size_replaced"]= $sizenames[$i];
	            		}
	            		else{
	            			$productsInCart[$key]["final_size"]= $sizenames[$i];
	            			$productsInCart[$key]["final_size_replaced"]= "";
	            		}
	             	   $productsInCart[$key]["final_quantity"] = $j;
	            	}
	        	}
			}
			else{
				//$all_option_qty_details = explode(",", $productsInCart[$key]['quantity_breakup']);
				$unifiedsizes=Size_unification::getUnifiedSizeByStyleId($productsInCart[$key]['productStyleId'], "array", true);
				$options_qty_mapping = func_query_first("Select po.value as size, oq.quantity from mk_order_item_option_quantity oq, mk_product_options po where oq.optionid = po.id and oq.itemid = ".$productsInCart[$key]['itemid']);
				$productsInCart[$key]['final_size'] = $unifiedsizes[$options_qty_mapping['size']];
				$productsInCart[$key]['final_quantity'] = $options_qty_mapping['quantity'];
				$productsInCart[$key]['final_size_replaced'] = $options_qty_mapping['size'];
			}
	}
	
	if($tempStr){
			$getCatQuery = "SELECT sp.style_id as styleId, sp.global_attr_brand as brandName, cc.typename as articleType FROM mk_style_properties as sp,".
			"mk_catalogue_classification as cc WHERE cc.id=sp.global_attr_article_type AND sp.style_id in (". $tempStr.")";
					
		 	$getCatResult = db_query($getCatQuery);
			$productStyleIds = array();
			while($categoryRow = db_fetch_array($getCatResult))
			{
				$productStyleIds[$categoryRow['styleId']] = $categoryRow['articleType']."|".$categoryRow['brandName'];                
			}
			foreach($productsInCart as $key => $order)
			{
				$styleId = $productsInCart[$key]['productStyleId'];
				$productsInCart[$key]['productCatLabel'] = $productStyleIds[$styleId];            

			}
	}
	return $productsInCart;
}

#
#Function for sending the mail, if user has selected check OR cash payment option
#
function func_send_mail_for_check_and_cash_payment($paymentoption, $orderid, $amount, $login,$ordertime="")
{
	global $sql_tbl;
	global $sqllog;
	global $weblog;
	 #
	 #load login customer first name
	 #
			$sql = "SELECT firstname FROM $sql_tbl[customers] WHERE login='$login'";
			$sqllog->debug("File Name::>"."func.mk_orderbook.php"."##Function Name::>func_send_mail_for_check_and_cash_payment##"."##SQL Info::>".$sql); 
			$result = func_query_first($sql);
			$CustomerFirstName = $result['firstname'];
			$date = $result['date'];
		
			$email = $login;
			$template= ($paymentoption == 'chq')?"checkpayment":"codpayment";            
			$from ="MyntraAdmin";
			$subjectArgs = array("ORDERNO" =>$orderid) ;

            if($paymentoption == 'cod'){

                if(func_query_first_cell("select status from xcart_orders where orderid=$orderid")=="PP")
                    $template='codEmailCCVerify';
                else
                    $template='codpayment';

                $address=func_query_first_cell("select CONCAT(s_address,'\n',s_city,'-',s_zipcode,',',s_country)  from xcart_orders where orderid =$orderid");
                $args = array( "USER" =>$CustomerFirstName,
                            "ORDERNO"  =>$orderid,
                            "ADDRESS"  =>$address,
                            "DELIVERY_DATE"	=> $ordertime
                            );
            }
            else
            $args = array( "USER" =>$CustomerFirstName,
						"ORDERNO"  =>$orderid,
						);

			$weblog->info("Calling sendMessageDynamicSubject function in func.mk_orderbook.php");
			sendMessageDynamicSubject($template, $args, $email,$from, $subjectArgs);
			$weblog->info("End : function call for sendMessageDynamicSubject ended.");

}

#
#Load state label
#
function func_get_state_label($state_code){
        
    $sql = "SELECT state FROM xcart_states WHERE code='".$state_code."'";
    $state_name = func_query_first($sql);
    
    return $state_name['state'];
    }

    #
    #Load country label
    #
    
    
    function func_get_country_label($country_code){

    $code= "country_".$country_code;	   
    $sql = "SELECT value FROM xcart_languages WHERE name='".$code."' AND topic ='Countries'";
    $country_name = func_query_first($sql);
    
    return $country_name['value'];
    }

/* mail with dynamic subject,content and template
 * @i/parameters ::tomail,(subject array or string or null),content[],template
 * @reuturns :: void
 */
function func_send_gift_mail_for_order($login,$subjectargs='',$contentargs,$template,$ordertime="",$from=''){
	global $sql_tbl,$sqllog,$weblog;
	
	/* first name will be set by login
	 * if frist name is not set in $contentargs
	 */
	if(empty($contentargs['USER'])){
		$sql = "SELECT firstname FROM $sql_tbl[customers] WHERE login='$login'";
		$sqllog->debug("File Name::>"."func.mk_orderbook.php"."##Function Name::>function func_send_gift_mail_for_order($login,$subjectargs,$contentargs,$template,$ordertime='')"."##SQL Info::>".$sql); 
		$result = func_query_first($sql);
		$customername = $result['firstname'];
		$contentargs += array( "USER" =>$customername);
	}
	if(empty($subjectargs)){
		$template_query = "SELECT * FROM mk_email_notification WHERE name = '$template'";
		$template_result = db_query($template_query);
		$row = db_fetch_array($template_result);					
		$subjectargs = $row['subject'];
	}
						
	$to = $login;
	$from = (!empty($from))? $from : "MyntraAdmin";
	
	$weblog->info("Calling sendMessageDynamicSubject function in func.mk_orderbook.php");
	sendMessageDynamicSubject($template, $contentargs, $to,$from, $subjectargs);
	$weblog->info("End : function call for sendMessageDynamicSubject ended.");
}

/** mail with dynamic subject,content and template for order confirmation
 * @i/parameters ::payment type(see switch case in function),products_in_order[],order_amount_detail[],to,orderid,ordertime
 * @reuturns :: void=sends mail
 * @deprecated
 */

function func_order_confirm_mail($payment_type,$order_products_detail,$order_amount_detail,$name,$email_to,$order_id,$account_type,$customer,$ivrNumber="") {
	global $weblog, $cashback_gateway_status, $http_location, $enable_cashback_discounted_products;
	
	if($payment_type=="ivr"){
    	$template =  "ivr_order_pending";
    }else {
    	$template =  "order_confirmation";
    }

	$from = '';
	
	//$order_details = create_product_detail_table($order_products_detail, $order_amount_detail, $payment_type);
	$order_details = create_product_detail_table_new($order_products_detail, $order_amount_detail);
	
	$delivery_address = $customer['s_address']."<br>".$customer['s_city']."<br>".$customer['s_state'].", ".$customer['s_country']." - ".$customer['s_zipcode'];
	$casback_earnedcredits_enabled = FeatureGateKeyValuePairs::getBoolean('cashback.earnedcredits.enabled',true);
	$cashbackMessage = "";
	if($cashback_gateway_status == "on" && $casback_earnedcredits_enabled)
	{	
		$url = $http_location."/mymyntra.php?view=mymyntcredits";
		if($enable_cashback_discounted_products == "true")	{
			$cashbackAmount = ($order_amount_detail['total'] +  $order_amount_detail['cash_redeemed']) * 0.1;
		} else {
			$cashbackAmount = 0.0;
			foreach($order_products_detail as $key=>$val){
				if($val['discount'] === 0 && $val['cart_discount_split_on_ratio']===0) {
					$cashbackAmount += ($val['totalPrice'] - $val['coupon_discount']) * 0.1;
				}
			}
		}
		if($cashbackAmount > 0){
			$cashbackAmount = round($cashbackAmount);
			$cashbackMessage ="";
			if($payment_type == "cod") {
				$cashbackMessage = "You will receive a cashback of Rs $cashbackAmount in your MyMyntra account after 30 days.";
			} else { 
				$cashbackMessage = "You will receive a cashback of Rs. $cashbackAmount in your MyMyntra account within the next hour.";				
			}
			$cashbackMessage .= " You can view your cashback account summary <a href=\"$url\">here</a>.<br>";
		}
	}
	$subjectargs = array("ORDER_ID"	=> $order_id);
	$args = array( 	"USER"				=> $name,
					"ORDER_ID"			=> $order_id,
					"ORDER_DETAILS"	    => $order_details,				
					"DELIVERY_DATE"		=> $delivery_date,
					"CASHBACK_SUMMARY"	=> $cashbackMessage,
					"DELIVERY_ADDRESS"	=> $delivery_address,
					"IVR_NUMBER"		=> $ivrNumber,
                    "DECLARATION"       => "This mail is intended only for ".$email_to
			);
	    /*** myntra festival coupon generation :begins***/
		    global $sql_tbl;
		    $args["MYNTRA_FEST_MSG"]=""; // default value for order conformation mailer message
		    $couponCode = $order_amount_detail['coupon'];
		    $couponGroupName = func_query_first("select groupName from ".$sql_tbl['discount_coupons']." where coupon='$couponCode'");
		    if(FeatureGateKeyValuePairs::getBoolean("myntrashoppingfestCoupons")){
		       	if(empty($couponCode) || (!empty($couponGroupName) && $couponGroupName["groupName"]!=WidgetKeyValuePairs::getWidgetValueForKey('myntraFestCouponGroupName'))){
		       		SpecialCouponGenerator::generateSpecialCouponsForOrder($order_id);
				$festCoupons = SpecialCouponGenerator::getAllSpecialCouponForOrder($order_id);
				if(sizeof($festCoupons)>0){
					$args["MYNTRA_FEST_MSG"]=WidgetKeyValuePairs::getWidgetValueForKey('myntraFestOrderConfirmationMailMsg');
				}else{
					$args["MYNTRA_FEST_MSG"]="";
				}
		     }
	   	}
	    /*** myntra festival coupon generation :ends**/

			
    $customKeywords = array_merge($subjectargs, $args);
    
	$mail_details = array( 
						"template" => $template,
						"to" => $email_to,
						"bcc" => "myntramailarchive@gmail.com",
						"header" => 'header',
						"footer" => 'footer',
						"mail_type" => MailType::CRITICAL_TXN
					);
	$multiPartymailer = new MultiProviderMailer($mail_details, $customKeywords);
	return $multiPartymailer->sendMail();
}

function func_gift_card_order_confirm_mail($giftCardOrder,$customer) {
    global $weblog, $cashback_gateway_status, $http_location, $enable_cashback_discounted_products;

    $template =  "gift_card_order_confirmation";
    /* get gift card from the gift card order object */ 
    $giftCard = $giftCardOrder->giftCard;
    $email_to = $giftCard->senderEmail;
	
    /* get the user(sender's) first name and last name */
    $user = $customer['firstname'] . " " . $customer['lastname'];
    /* If the user is a new user then a first name and last name wont be present in which case we can use the sender name provided for the giftcard */
    if(empty($user)){
    	$user=$giftCard->senderName;
    }
	
    /* set template variable for the mail content */
    $subjectargs = array("ORDER_ID"	=> $giftCardOrder->orderId);
    $args = array("USER" => $user,
		"ORDER_ID" => $giftCardOrder->orderId,
		"GIFT_AMOUNT" => $giftCardOrder->subTotal,
                "RECIPIENT_EMAIL" => $giftCard->recipientEmail,
		"SENDER_EMAIL" => $giftCard->senderEmail,
		"RECIPIENT_NAME" => $giftCard->recipientName,
		"SENDER_NAME" => $giftCard->senderName,
		"CASHBACK_USED" => $giftCardOrder->cashRedeemed,
		"EMI_CHARGES" => $giftCardOrder->paymentSurcharge,
		"ORDER_TOTAL" => $giftCardOrder->total,
                "DECLARATION" => "This mail is intended only for ".$email_to
		);
			
    $customKeywords = array_merge($subjectargs, $args);
    
    $mail_details = array("template" => $template,
			"to" => $email_to,
			"bcc" => "myntramailarchive@gmail.com",
			"header" => 'header',
			"footer" => 'footer',
			"mail_type" => MailType::CRITICAL_TXN
			);
				
    /* Trigger mail */
    $multiPartymailer = new MultiProviderMailer($mail_details, $customKeywords);
    return $multiPartymailer->sendMail();
}

/*function create_product_detail_table($order_products_detail, $order_amount_detail, $payment_type="", $cb_deducted=0.00) {
	global $weblog;
	$productdetails = '';
	foreach($order_products_detail as $key=>$val){
		$weblog->info("Options for item - ".$val['final_size']);
		$weblog->info("Quantities for item - ".$val['final_quantity']);
		$productdetails .= '<tr>';
		$productdetails .= '<td style="padding: 10px 5px;">'.$val['productStyleName'].'</td>';
		$productdetails .= '<td style="padding: 10px 5px;">Rs '.$val['unitPrice'].'</td>';
		$per_item_discount = $val['discount']/$val['quantity'];
		$productdetails .= '<td style="padding: 10px 5px;">Rs '.number_format($per_item_discount,2).'</td>';
		if($val['final_size_replaced']){
			$productdetails .= '<td style="padding: 10px 5px;">'.$val['final_size'].'('.$val['final_size_replaced'].')'.'</td>';
		}
		else{
			$productdetails .= '<td style="padding: 10px 5px;">'.$val['final_size'].'</td>';
		}
		$productdetails .= '<td style="padding: 10px 5px;">'.$val['final_quantity'].'</td>';
		$productdetails .= '<td style="padding: 10px 5px;" align=right>Rs '.number_format($val['total_amount'], 2).'</td>';
		$productdetails .= '<td style="padding: 10px 5px;">[Rs '.$val['vatamount'].']</td>';
		$productdetails .= '<td style="padding: 10px 5px;">&nbsp;</td>';
		$productdetails .= '</tr>';
		$productdetails .= '<tr><td colspan=7><hr style="border:1px dotted #AAAAAA;"></td></tr>';
		$productdetails .= "\n";	
	}
	if($productdetails == '') {
		return "<b>No products present in this order</b>";
	}
	if($order_amount_detail) {
		$subTotal = $order_amount_detail['subtotal'];
		$payable_amount = number_format(floor($order_amount_detail['total']+$order_amount_detail['gift_charges']+$order_amount_detail['cod']-$cb_deducted), 2);
		$productdetails .= '<tr><td style="padding: 5px;" colspan=6 align=right>Total Amount (Rs):</td><td style="padding: 5px;" align=right>'.number_format($subTotal, 2).'</td></tr>';
		if ($order_amount_detail['coupon_discount'] != 0.00) {
			$productdetails .= '<tr><td style="padding: 5px;" colspan=6 align=right>Coupon Discounts (Rs):</td><td style="padding: 5px;" align=right>'.number_format($order_amount_detail['coupon_discount'], 2).'</td></tr>';
		}
		if ($order_amount_detail['pg_discount'] != 0.00) {
			$productdetails .= '<tr><td style="padding: 5px;" colspan=6 align=right>Payment Discount (Rs):</td><td style="padding: 5px;" align=right>'.number_format($order_amount_detail['pg_discount'], 2).'</td></tr>';
		}
		if ($order_amount_detail['discount'] != 0.00) {
			$productdetails .= '<tr><td style="padding: 5px;" colspan=6 align=right>Discount (Rs):</td><td style="padding: 5px;" align=right>'.$order_amount_detail['discount'].'</td></tr>';
		}
		if ($order_amount_detail['cash_redeemed'] != 0.00) {
			$productdetails .= '<tr><td style="padding: 5px;" colspan=6 align=right>Mynt Credits used (Rs):</td><td style="padding: 5px;" align=right>'.number_format($order_amount_detail['cash_redeemed'], 2).'</td></tr>';
		}
		$productdetails .= '<tr><td style="padding: 5px;" colspan=6 align=right>Shipping (Rs):</td><td style="padding: 5px;" align=right>'.$order_amount_detail['shipping_cost'].'</td></tr>';
		if ($order_amount_detail['gift_charges'] != 0.00) {
			$productdetails .= '<tr><td style="padding: 5px;" colspan=6 align=right>Gift Wrapping Charges (Rs):</td><td style="padding: 5px;" align=right>'.$order_amount_detail['gift_charges'].'</td></tr>';
		}		
		if ($order_amount_detail['cod'] != 0.00) {
			$productdetails .= '<tr><td style="padding: 5px;" colspan=6 align=right>COD Charges (Rs):</td><td style="padding: 5px;" align=right>'.$order_amount_detail['cod'].'</td></tr>';
		}
		if ($cb_deducted != 0.00) {
			$productdetails .= '<tr><td style="padding: 5px;" colspan=6 align=right>Casback Deducted (Rs):</td><td style="padding: 5px;" align=right>'.$cb_deducted.'</td></tr>';
		}		
		$productdetails .= '<tr><td colspan=7><hr style="border:1px dotted #AAAAAA;"></td></tr>';
		if($payment_type == "cod") {
			$productdetails .= '<tr><td style="padding: 5px;" colspan=6 align=right><b>Final Amount to be paid on delivery (Rs):</b></td><td style="padding: 5px;" align=right><b>'.$payable_amount.'</b></td></tr>';
		} else if($payment_type == "refund"){
			if($order_amount_detail['payment_method'] == 'cod'){
				$refundable_amount = $order_amount_detail['cash_redeemed'];
			} else {
				$refundable_amount = $order_amount_detail['total']+$order_amount_detail['shipping_cost']+$order_amount_detail['gift_charges']+$order_amount_detail['cod'] - $cb_deducted + $order_amount_detail['cash_redeemed'];
			}
			$productdetails .= '<tr><td style="padding: 5px;" colspan=6 align=right><b>Refund Amount (Rs):</b></td><td style="padding: 5px;" align=right><b>'.number_format($refundable_amount,2).'</b></td></tr>';
		} else {
			$productdetails .= '<tr><td style="padding: 5px;" colspan=6 align=right><b>Final Amount (Rs):</b></td><td style="padding: 5px;" align=right><b>'.$payable_amount.'</b></td></tr>';
		}
		$productdetails .= "\n";	
	}
	
	return create_order_details_table('order_details_table', $productdetails);
}


function create_order_details_table($order_details_table_template, $product_details) {
	$row = func_query_first("SELECT * FROM mk_email_notification WHERE name = '$order_details_table_template'");
	$table_contents = $row['body'];
	$table_contents = str_replace("[PRODUCT_DETAILS]", $product_details, $table_contents);
	return $table_contents;
}*/

/**
 * Send out an email containing the e-voucher.
 */
function func_evoucher_mail($name, $email_to, $order_id, $payable_amount)
{
    // Check if the campaign is in effect.
    global $system_online_payment_evoucher;    
    if (!$system_online_payment_evoucher) {
        return;
    }
    
    // Check if the order's payable amount is above the minimum limit.
    global $system_evoucher_minimum_amount;
    if ($payable_amount < $system_evoucher_minimum_amount) {
        return;
    }
    
    $template = 'online_payment_evoucher';
    $from = '';
    
    $now = time();
    $nowDate = date('d F Y', $now);
    $nowDateTime = strtotime($nowDate);
    
    // 3 days expiry time.
    $expire = $nowDateTime + 3 * 24 * 60 * 60;
    $endDate = date('j F Y', $expire);
    
    $voucherGenerator = EVoucherGenerator::getInstance();
    
    // Group- EVouchers, Prefix- EV.
    $couponCode = $voucherGenerator->generateEVoucher($email_to, 10, $nowDateTime, $expire, $order_id);
    if ($couponCode == null) {
        return;
    }
    
    $args = array (
        "USER"         => $name,
        "COUPON"       => $couponCode,
        "EXPIRE"       => $endDate
    );
    
    sendMessageDynamicSubject($template, $args, $email_to ,$from);
}


/* Mail with dynamic subject,content and template for COD above 1000 order, fraud check
 */
function func_cod_order_above_limit($order_products_detail,$order_amount_detail,$name,$loginid,$order_id,$order_time, $limitToCheck, $lastMobileNoUsed){
	global $weblog;
	
	$template = 'cod_above_limit_check';
		
	$from = 'COD Fraud Check';
	$productdetail = '';
	foreach($order_products_detail as $key=>$val){
			
		## add design markup charge to the design
		$query = "select if((markup=0 || isnull(markup)),0,markup) as markup from xcart_products where productid='{$val['productId']}'";
		$markuprate = func_query_first_cell($query);
		$markup = $markuprate/100;
			
		//calculate designer charge from mk_product_style price and product markup
		$untiprice = func_query_first_cell("select price from mk_product_style where id='{$val['productStyleId']}'");
		$designer_charge = $untiprice*$markup;
			
		$untiprice = number_format(($val['productPrice']-$designer_charge),2);//since the product style price may have option price and addon
		$designer_charge = number_format($designer_charge,2);
		$productdetail .= "<tr><td>{$val['productStyleName']} - {$val['quantity_breakup']}</td><td>{$val['quantity']}</td><td align='right'>{$untiprice}</td><td align='right'>{$designer_charge}</td><td align='right'>{$val['totalPrice']}</td></tr>";
	}

	$subjectargs = array("LIMITCHECK"	=> $limitToCheck, "ORDERID"	=> $order_id);
	$args = array( 	"USER"				=> $name,
					"LOGINID"			=> $loginid,
					"ORDERID"			=> $order_id,
					"PRODUCTDETAIL"		=> $productdetail,				
					"SUBTOTAL"			=> number_format($order_amount_detail['subtotal'],2),
					"DISCOUNT"			=> number_format($order_amount_detail['coupon_discount']+$order_amount_detail['ref_discount'],2),
					"TAX"				=> number_format($order_amount_detail['tax'],2),
					"SHIPPINGCHARGE"	=> number_format($order_amount_detail['shipping_cost'],2),
					"EMICHARGE"	=> number_format($order_amount_detail['payment_surcharge'],2),
					"GIFTWRAPPERCHARGE"	=> number_format($order_amount_detail['gift_charges'],2),
					"GRANDTOTAL"		=> number_format($order_amount_detail['total']+$order_amount_detail['tax']+$order_amount_detail['shipping_cost'],2),
					"DELIVERY_DATE"		=> $order_time,
					"LASTMOBILE"		=> $lastMobileNoUsed
			);
	
	$email_to = COD_RELATED_EMAILS;
	$weblog->info("Calling sendMessageDynamicSubject function in file ".__FILE__." in function ".__FUNCTION__);
	sendMessageDynamicSubject($template, $args, $email_to ,$from, $subjectargs);
	$weblog->info("End : function call for sendMessageDynamicSubject ended.");
}


function func_cod_manual_verification_email($order_products_detail,$order_amount_detail,$name,$email_to,$order_id,$account_type,$customer){
	global $weblog;
	
	$template = 'cod_manual_verification';
	$delivery_address = $customer['s_address']."<br>".$customer['s_city']."<br>".$customer['s_state'].", ".$customer['s_country']." - ".$customer['s_zipcode'];
	$order_details = create_product_detail_table_new($order_products_detail, $order_amount_detail);
	
	$subjectargs = array("ORDER_ID"	=> $order_id);
	$args = array( 	"USER"	=> $name,
			"ORDER_ID"		=> $order_id,
			"ORDER_DETAILS"	=> $order_details,
			"DELIVERY_ADDRESS"  => $delivery_address,
			"DECLARATION"  => "This mail is intended only for ".$email_to
	);
	
	$customKeywords = array_merge($subjectargs, $args);
	
	$mail_details = array(
			"template" => $template,
			"to" => $email_to,
			"bcc" => "myntramailarchive@gmail.com",
			"header" => 'header',
			"footer" => 'footer',
			"mail_type" => MailType::CRITICAL_TXN
	);
	$multiPartymailer = new MultiProviderMailer($mail_details, $customKeywords);
	return $multiPartymailer->sendMail();
	
	
}

function func_ebs_flagged_put_on_hold_mail($orderid, $gateway_payment_id)
{
	global $sql_tbl;
	$template = "ebs_flagged_on_hold";
	$alert_to = "alrt_ebs_flagged@myntra.com";
	
	$query = "SELECT login, total, s_firstname, s_lastname, mobile, from_unixtime(date) as order_date FROM $sql_tbl[orders] WHERE orderid='$orderid'";
	$result = func_query_first($query);
	if(!empty($result))
	{
		$args = array(
					"ORDER_ID" => $orderid,
					"GATEWAY_ID" => $gateway_payment_id,
					"ORDER_AMOUNT" => $result['total'],
					"ORDER_DATE" => $result['order_date'],
					"CUSTOMER" => $result['login'],
					"CUSTOMER_NAME" => $result['s_firstname'] . " " . $result['s_lastname'],
					"CUSTOMER_PHONE" => $result['mobile'],
					);
		sendMessageDynamicSubject($template,$args,$alert_to,'',$args,'','',false);
	}
}

function func_ebs_flagged_order_queued_mail($orderid, $gateway_payment_id)
{
	global $sql_tbl;
	$template = "ebs_flagged_queued";
	$alert_to = "alrt_ebs_flagged@myntra.com";
	
	$query = "SELECT login, total, s_firstname, s_lastname, mobile, from_unixtime(date) as order_date, from_unixtime(queueddate) as queueddate FROM $sql_tbl[orders] WHERE orderid='$orderid'";
	$result = func_query_first($query);
	if(!empty($result))
	{
		$args = array(
					"ORDER_ID" => $orderid,
					"GATEWAY_ID" => $gateway_payment_id,
					"ORDER_AMOUNT" => $result['total'],
					"ORDER_DATE" => $result['order_date'],
					"QUEUED_DATE" => $result['queueddate'],
					"CUSTOMER" => $result['login'],
					"CUSTOMER_NAME" => $result['s_firstname'] . " " . $result['s_lastname'],
					"CUSTOMER_PHONE" => $result['mobile'],
					);
		sendMessageDynamicSubject($template,$args,$alert_to,'',$args,'','',false);
	}
}

function func_ebs_flagged_order_rejected_mail($orderid, $gateway_payment_id)
{
	global $sql_tbl;
	$template = "ebs_flagged_rejected";
	$alert_to = "alrt_ebs_flagged@myntra.com";
	
	$query = "SELECT login, total, s_firstname, s_lastname, mobile, from_unixtime(date) as order_date, now() as rejection_date FROM $sql_tbl[orders] WHERE orderid='$orderid'";
	$result = func_query_first($query);
	if(!empty($result))
	{
		$args = array(
					"ORDER_ID" => $orderid,
					"GATEWAY_ID" => $gateway_payment_id,
					"ORDER_AMOUNT" => $result['total'],
					"ORDER_DATE" => $result['order_date'],
					"REJECTION_DATE" => $result['rejection_date'],
					"CUSTOMER" => $result['login'],
					"CUSTOMER_NAME" => $result['s_firstname'] . " " . $result['s_lastname'],
					"CUSTOMER_PHONE" => $result['mobile'],
					);
		sendMessageDynamicSubject($template,$args,$alert_to,'',$args,'','',false);
	}
}

function func_ebs_flagged_cancellation_email($orderid)
{
	global $sql_tbl;
	
	$template = "ebs_flagged_rejection";
	$query = "SELECT login, date, subtotal,discount, cart_discount, firstname,issues_contact_number, coupon_discount, coupon AS couponcode, shipping_cost, tax, ordertype, ref_discount, gift_status,gift_charges, total, cod, qtyInOrder, additional_info FROM $sql_tbl[orders] WHERE orderid = '".$orderid."'";
	$result = func_query_first($query);
	$productsInOrder = func_create_array_products_of_order($orderid);
	$orderdetails = create_product_detail_table_new($productsInOrder, $result);
	if(!empty($result))
	{
		$args = array(
					"ORDER_ID" => $orderid,
					"FIRST_NAME" => $result['firstname'],
					"ORDER_DATE" => date("jS M Y", $result['date']), 
					"ORDER_DETAILS"=>$orderdetails
					);
		$login = $result['login'];
		sendMessageDynamicSubject($template,$args,$login,'',$args);
	}
				
}
/* to get the article type of the product which
 * costs high from $productsInCart structure
 * @param:(array)$productsInCart
 * @return:(string)$articleType
 */
function getHighestPriceArticleType($productsInCart){

    $highestPriceArticleType = $tempStr = '';    
    if(!empty($productsInCart)){
        foreach($productsInCart as $key=>$val){
            if(!empty($tempStr)){
				$tempStr.=", ";
			}
			$tempStr .= $productsInCart[$key]['productStyleId'];
        }
    }
    
    if($tempStr){
        $getCatQuery = "SELECT sp.style_id as styleId, sp.global_attr_brand as brandName, cc.typename as articleType FROM mk_style_properties as sp,".
        "mk_catalogue_classification as cc WHERE cc.id=sp.global_attr_article_type AND sp.style_id in (". $tempStr.")";

        $getCatResult = db_query($getCatQuery);
        $productStyleIds = array();

        while($categoryRow = db_fetch_array($getCatResult))	{
            $productStyleIds[$categoryRow['styleId']]['articleType'] = $categoryRow['articleType'];
        }

        $highestProductPrice = 0;
        foreach($productsInCart as $key => $order)
        {
            $styleId = $productsInCart[$key]['productStyleId'];            

            //to get article type of the highest priced product(for conversion tracking)
            if($productsInCart[$key]['productPrice'] > $highestProductPrice){
                if(empty($productStyleIds[$styleId]['articleType'])){
                    //if highest price article type emty take the next highest priced article type
                    $highestProductPrice = 0;
                } else {
                    $highestPriceArticleType = $productStyleIds[$styleId]['articleType'];
                    $highestProductPrice = $productsInCart[$key]['productPrice'];
                }
            }
        }
	}
    return $highestPriceArticleType;
}
/**
 * Verify order against customer login id and cancels if order is in pre processed state i.e PP PV,
 * @param String $login
 * @param String CASH or DISCOUNT
 * @param String Coupon code
 */
function findAndCancelPPOrderBlockingCoupon($login,$COUPON_TYPE,$couponCode){
	global $weblog;
        $selectquery = ($COUPON_TYPE==='CASH')?"cash_coupon_code":"coupon";
        $last_order_sql="select orderid,coupon,cash_coupon_code from xcart_orders where date > ".(time()-(15*60))." and (status='PP' or status='PV') and login='$login' and ".$selectquery."='$couponCode' limit 1";
     
        $result=func_query_first($last_order_sql,true);
        $orderid_blocking_coupon=$result['orderid'];
		
        if(empty($orderid_blocking_coupon))
                return false;
        $weblog->info("Found:: $COUPON_TYPE coupon($couponCode) locked by $orderid_blocking_coupon");
        $orderid=$orderid_blocking_coupon;
        $otherCouponCode = ($selectquery == "cash_coupon_code")?$result['coupon']:$result['cash_coupon_code'];

        $sql = "update xcart_orders SET status = 'F' where orderid='$orderid'";
        db_query($sql);

        $adapter=CouponAdapter::getInstance();
        $adapter->unlockCouponForUser($couponCode, $login);
		$weblog->info("Coupon restored:: $COUPON_TYPE coupon($couponCode) locked by $orderid_blocking_coupon");

        if(!empty($otherCouponCode)){
                $adapter->unlockCouponForUser($otherCouponCode, $login);
                $weblog->info("Coupon restored:: $COUPON_TYPE coupon($otherCouponCode) locked by $orderid_blocking_coupon");
        }
        return true;

}

/*
 * This method is used to create product details section to be used in order invoice and emails
 * the product geader information comes from DB templates.
 * If order_amount_detail is not passed, then total order calculation is not added to the table.
 */
function create_product_detail_table_new($order_products_detail, $order_amount_detail=false, $display_for="email", $action="", $items_in_different_shipments=false, $invoiceType=false, $margin=false, $showGovtTax=true, $total_gift_card_amount_refund=false) {
	global $smarty;
	$smarty->clear_assign("other_shipment_items");
	$smarty->assign("invoiceType", $invoiceType);
	$smarty->assign("margin", $margin);

	$grand_total = 0.00;
	$skuIds = array();
	foreach($order_products_detail as $index=>$item){
		if($item['cart_discount_split_on_ratio']){
			$order_products_detail[$index]['per_item_discount'] = number_format(($item['discount']+$item['cart_discount_split_on_ratio']), 2);
		} else {	
			$order_products_detail[$index]['per_item_discount'] = number_format(($item['discount']+$item['cart_discount']), 2);
		}
		if(!empty($item['sku_id']) && !in_array($item['sku_id'], $skuIds))
			$skuIds[] = $item['sku_id'];
		if($margin){
			//$total_amount_finance = round($item['total_amount'] - $item['coupon_discount'] - ($margin*($item['total_amount']-$item['coupon_discount']))/100, 2);
			$base_price = round((1 - $margin * 0.01) * ($item['govt_tax_amount'] / ($item['govt_tax_rate'] * 0.01)),2);
			$vatamount_finance = round($item['govt_tax_rate'] * $base_price * 0.01, 2);
                        $total_amount_finance = $base_price + $vatamount_finance;
			$order_products_detail[$index]['total_amount_finance'] = number_format($total_amount_finance, 2);
			$order_products_detail[$index]['base_price'] = number_format($base_price, 2);
			$order_products_detail[$index]['vatamount_finance'] = number_format($vatamount_finance, 2);
			$grand_total += $total_amount_finance;
		}
	}
	
	$skuDetails = SkuApiClient::getSkuDetails($skuIds);
	foreach($skuDetails as $sku) {
		$skuId2SkuCode[$sku['id']] = $sku['code'];	
	}
	
	$smarty->assign("grand_total", number_format($grand_total,2));
	$smarty->assign("order_details", $order_products_detail);
	$smarty->assign("sku_code_mapping", $skuId2SkuCode);
	if($order_amount_detail) {
		if($order_amount_detail['cart_discount_split_on_ratio']){
		//invoice page
			$order_amount_detail['discounted_sub_total'] = number_format($order_amount_detail['subtotal'] - $order_amount_detail['discount'] - $order_amount_detail['cart_discount_split_on_ratio'] - $order_amount_detail['coupon_discount'], 2);
		} else {
			$order_amount_detail['discounted_sub_total'] = number_format($order_amount_detail['subtotal'] - $order_amount_detail['discount'] - $order_amount_detail['cart_discount']- $order_amount_detail['coupon_discount'], 2);
		}
		$order_amount_detail['discount'] = number_format($order_amount_detail['discount']+ $order_amount_detail['cart_discount'], 2);
                $order_amount_detail['discounted_amount'] = number_format($order_amount_detail['subtotal'] - $order_amount_detail['discount'] - $order_amount_detail['cart_discount_split_on_ratio'] - $order_amount_detail['cart_discount'] - $order_amount_detail['coupon_discount'],2);
		$order_amount_detail['difference_refund'] = number_format($order_amount_detail['difference_refund'], 2);
		$order_amount_detail['tax'] = number_format($order_amount_detail['tax'], 2);
		$order_amount_detail['loyalty_credit'] = number_format($order_amount_detail['loyalty_credit'], 2);
		$order_amount_detail['payable_amount'] = number_format(round(func_get_order_payable_amount($order_amount_detail)), 2);
		if($action == "refund"){
			if($order_amount_detail['payment_method'] == 'cod'){
				$refundable_amount = $order_amount_detail['cash_redeemed'] - $order_amount_detail['difference_refund'] + $order_amount_detail['gift_card_amount'];
			} else {
				$refundable_amount = $order_amount_detail['total']+$order_amount_detail['payment_surcharge']+$order_amount_detail['cod'] - $order_amount_detail['cashback'] - $order_amount_detail['difference_refund'] + $order_amount_detail['cash_redeemed'];
			}
			if($order_amount_detail['cashback']) {
				$refundable_amount -= $order_amount_detail['cashback'];
			}
			$order_amount_detail['refundable_amount'] = number_format($refundable_amount, 2);
			if($order_amount_detail['ordertype'] == 'ex'){
				$order_amount_detail['refundable_amount'] = 0.00;
			}
		}

        if($total_gift_card_amount_refund > 0.00 && $order_amount_detail['gift_card_amount'] == 0.00){
            $order_amount_detail['gift_card_amount'] = $total_gift_card_amount_refund;
        }
		
		$smarty->assign("amount_details", $order_amount_detail);
	}
	$smarty->assign("display_for", $display_for);
	
	if($items_in_different_shipments && count($items_in_different_shipments) > 0)
		$smarty->assign("other_shipment_items", $items_in_different_shipments);
        $smarty->assign("show_govt_tax", $showGovtTax);
		
	$productdetails = func_display('invoice_email_item_details.tpl', $smarty, false);
	
	return $productdetails;
}

?>
