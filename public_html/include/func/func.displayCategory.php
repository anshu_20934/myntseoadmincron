<?php

function updateDisplayCategory($id,$style_id=null){
	$display_category_id = $id;
	$sql = "select name,category_rule from mk_display_categories where id='".$display_category_id."'" ;
	$result =func_query_first($sql);
	$display_category_rule = json_decode($result[category_rule]);
	$article_types='';
	foreach ($display_category_rule->article_type_id as $tmp_article_type_id) {
		$article_types.="'".$tmp_article_type_id."',";
	}
	if(!empty($article_types)){
		$article_types=substr($article_types,0,-1);
	}
	if(empty($style_id)){
		// check exact match
		$sql = "update mk_style_properties set display_categories='' where display_categories='".$result['name']."'";
		db_query($sql);
		// check if in between ,category,
		$sql = "update mk_style_properties set display_categories=replace(display_categories,',".$result['name'].",',',') where display_categories like'%,".$result['name'].",%'";
		db_query($sql);
		// check if starting with category,
		$sql = "update mk_style_properties set display_categories=trim(LEADING '".$result['name'].",' FROM display_categories) where display_categories like '".$result['name'].",%'";
		db_query($sql);
		// check if ending with category,
		$sql = "update mk_style_properties set display_categories=trim(TRAILING ',".$result['name']."' FROM display_categories) where display_categories like '%,".$result['name']."'";
		db_query($sql);
	}
	// add new display category
	if(!empty($article_types)){
		$sql = "update mk_style_properties set display_categories= if((display_categories is null) or display_categories='' ,'".$result['name']."',concat(display_categories,',".$result['name']."'))
				 where global_attr_article_type in(".$article_types.")";
	}else{
		$sql = "update mk_style_properties set display_categories= if((display_categories is null) or display_categories='' ,'".$result['name']."',concat(display_categories,',".$result['name']."')) where 1=1 ";
	}

	$attributes_query='';
	foreach ($display_category_rule->row as $tmp_row){
		if(!empty($tmp_row->attribute_type)){
			if(strtolower($tmp_row->attribute_type) != 'colour'){
				$attributes_query.=" ".$tmp_row->attribute_function." global_attr_".str_replace(' ','_',strtolower($tmp_row->attribute_type))." in (";
			}else{
				$attributes_query.=" ".$tmp_row->attribute_function." global_attr_base_".str_replace(' ','_',strtolower($tmp_row->attribute_type))." in (";
			}
			foreach ($tmp_row->attribute_type_value as $tmp_attribute_type_value){
				$attributes_query.="'".$tmp_attribute_type_value."',";
			}
			$attributes_query.="''";
			$attributes_query.=")";
		}
	}
	$sql.=" ".$attributes_query;
	if(!empty($style_id)){
		$sql.=" and style_id='".$style_id."'";
	}
	db_query($sql);
	// update styles
	//	if(empty($style_id)){
	//		$sql = "select style_id from mk_style_properties where global_attr_article_type in(".$article_types.")";
	//		$results =func_query($sql);
	//		$solr_p = new SolrProducts();
	//		foreach ($results as $result) {
	//			$solr_p-> addStyleToIndex($result['style_id'], true, false);
	//		}
	//	}
	//	else{
	//		addStyleToSolrIndex($style_id,false);
	//	}
}
function updateDisplayCategoryForStyle($style_id){
	//reset the display category
	$sql = "update mk_style_properties set display_categories=null where style_id='".$style_id."'";
	db_query($sql);
	//now update it
	$sql = "select id from mk_display_categories where is_active='1'" ;
	$results =func_query($sql);
	foreach ($results as $result) {
		updateDisplayCategory($result[id],$style_id);
	}
}
function update_display_category($display_category_id){
	$sql = "select name from mk_display_categories where id='".$display_category_id."'" ;
	$result =func_query_first($sql);
	$name = $result[name];
	$sql = "select id from mk_style_properties where display_categories like '%".$name."%'";
	$results =func_query($sql);
	$styleids = array();
	foreach ($results as $result_tmp) {
		array_push($styleids,$result_tmp['id']);
	}
	// check exact match
	$sql = "update mk_style_properties set display_categories='' where display_categories='".$result['name']."'";
	db_query($sql);
	// check if in between ,category,
	$sql = "update mk_style_properties set display_categories=replace(display_categories,',".$result['name'].",',',') where display_categories like'%,".$result['name'].",%'";
	db_query($sql);
	// check if starting with category,
	$sql = "update mk_style_properties set display_categories=trim(LEADING '".$result['name'].",' FROM display_categories) where display_categories like '".$result['name'].",%'";
	db_query($sql);
	// check if ending with category,
	$sql = "update mk_style_properties set display_categories=trim(TRAILING ',".$result['name']."' FROM display_categories) where display_categories like '%,".$result['name']."'";
	db_query($sql);



	//	$solr_p = new SolrProducts();
	//	$solr_p-> addStylesToIndex($styleids,true);

}
?>