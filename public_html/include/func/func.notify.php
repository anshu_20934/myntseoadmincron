<?php
include_once "$xcart_dir/include/class/notify/class.notify.php";
include_once("$xcart_dir/include/class/class.feature_gate.keyvaluepair.php");
include_once("$xcart_dir/include/func/func.sms_alerts.php");
include_once("$xcart_dir/include/func/func.mail.php");
include_once("$xcart_dir/include/func/func.notify.php");
include_once("$xcart_dir/size_unification.php");
include_once("$xcart_dir/modules/discount/DiscountEngine.php");
include_once("$xcart_dir/size_unification.php");
include_once("$xcart_dir/include/class/class.mail.template.resolver.php");
/**
 * Return customer info who have subscribed for notifyme
 * @param unknown_type $sku skuid
 * @param unknown_type $limit max customers
 */
function func_getEmails_MobileToNotify_from_notifyme($sku,$limit=false){
	$handlerName=Notifications::$_CUSTOMER_INSTOCK_NOTIFYME;
	global $sql_tbl;
	$tab_mk_notification_subs=$sql_tbl['mk_notification_subs'];
	$tab_mk_notification_subs_instock=$sql_tbl['mk_notification_subs_instock'];
	$tab_customers=$sql_tbl['customers'];
	$tab_mk_notifications=$sql_tbl['mk_notifications'];
	$sql="select ns.id,ns.email,ns.mobile,date_format(ns.lastmodified,'%D %b %Y') as lastmodified,c.firstname from $tab_mk_notification_subs ns inner join $tab_mk_notification_subs_instock nsi on ns.notif_id=(select id from $tab_mk_notifications where name='$handlerName' limit 1)  and ns.notified=0 and nsi.notif_subs_id=ns.id and nsi.sku='$sku' left join $tab_customers c on ns.email=c.login order by ns.lastmodified asc";
	if(!($limit===false)){
		$sql.=" limit 0,$limit";
	}
	$resultSet=func_query($sql);
	return $resultSet;
}

/**
 * Returns notification information
 * @param unknown_type $notificationName
 */
function func_getNotification($notificationName){
	global $sql_tbl;
	$tab_mk_notifications=$sql_tbl['mk_notifications'];
	$sql="select id,name from $tab_mk_notifications  where name='$notificationName'";
	$resultSet=func_query_first($sql);
	return $resultSet;
}

/**
 * 
 * Subscribe for notifyme 
 * @param unknown_type $email
 * @param unknown_type $mobile
 * @param unknown_type $sku skuid
 */
function func_subscribe_notification_instock($email,$mobile,$sku){
	global $sql_tbl;
	$tab_mk_notifications=$sql_tbl['mk_notifications'];
	$tab_mk_notification_subs=$sql_tbl['mk_notification_subs'];
	$tab_mk_notification_subs_instock=$sql_tbl['mk_notification_subs_instock'];
	
	$handlerName=Notifications::$_CUSTOMER_INSTOCK_NOTIFYME;
	$sql="select sn.id,sn.notif_id,sn.email,sn.mobile from $tab_mk_notification_subs sn,$tab_mk_notifications n,$tab_mk_notification_subs_instock sni where sn.notif_id=n.id and sn.notified=0
		and n.name = '$handlerName' and sni.notif_subs_id=sn.id and sni.sku='$sku' and sn.email='$email' order by sn.lastmodified asc";
	$row=func_query_first($sql);
	if(!empty($row)){
		if($row['mobile']!=$mobile){
			$sql="update $tab_mk_notification_subs set mobile='$mobile' where id=$row[id]";
			db_query($sql);
		}
	}else{
		$sql="insert into $tab_mk_notification_subs (notif_id,email,mobile) values((select id from $tab_mk_notifications where name='$handlerName'),'$email','$mobile')";
		
		$r = db_query($sql);
		$notif_subs_i=0;
		if ($r) {
			$notif_subs_i= db_insert_id();
		}
		
		$sql="insert into $tab_mk_notification_subs_instock (notif_subs_id,sku) values($notif_subs_i,'$sku')";
		db_query($sql);
	}
	
	
}

/**
 * Subscrbe for unavailable options which are not available in DB
 * @param unknown_type $email
 * @param unknown_type $mobile
 * @param unknown_type $style
 * @param unknown_type $size
 */
function func_subscribe_notification_instock_undefined_size($email,$mobile,$style,$size){
	global $sql_tbl;
	$tab_mk_notifications=$sql_tbl['mk_notifications'];
	$tab_mk_notification_subs=$sql_tbl['mk_notification_subs'];
	$tab_mk_notification_subs_instock=$sql_tbl['mk_notification_subs_instock_unavailable'];
	
	$handlerName=Notifications::$_CUSTOMER_INSTOCK_NOTIFYME;
	$sql="select sn.id,sn.notif_id,sn.email,sn.mobile from $tab_mk_notification_subs sn,$tab_mk_notifications n,$tab_mk_notification_subs_instock sni where sn.notif_id=n.id and sn.notified=0
		and n.name = '$handlerName' and sni.notif_subs_id=sn.id and sni.style=$style and sni.size='$size' and sn.email='$email' order by sn.lastmodified asc";
	$row=func_query_first($sql);
	if(!empty($row)){
		if($row['mobile']!=$mobile){
			$sql="update $tab_mk_notification_subs set mobile='$mobile' where id=$row[id]";
			db_query($sql);
		}
	}else{
		$sql="insert into $tab_mk_notification_subs (notif_id,email,mobile) values((select id from $tab_mk_notifications where name='$handlerName'),'$email','$mobile')";
		
		$r = db_query($sql);
		$notif_subs_i=0;
		if ($r) {
			$notif_subs_i= db_insert_id();
		}
		
		$sql="insert into $tab_mk_notification_subs_instock (notif_subs_id,style,size) values($notif_subs_i,$style,'$size')";
		db_query($sql);
	}
	
	
}


/**
 * Handler for notifyme
 * @param InstockEvent $event
 */
function func_handle_instock_customer_nofifyme($event){
	$handlerName=Notifications::$_CUSTOMER_INSTOCK_NOTIFYME;
	if(FeatureGateKeyValuePairs::getBoolean($handlerName.'.enable')==false)
	return;
	
	$sql="select po.value,ps.name,ps.id as style,sp.search_image,sp.discount_rule,ps.price,global_attr_brand brand,(select typename from mk_catalogue_classification where id=sp.global_attr_article_type) articletype from mk_product_options po,mk_product_style ps,mk_styles_options_skus_mapping m,mk_style_properties 
sp where po.id=m.option_id and sp.style_id=ps.id and m.style_id = ps.id and m.sku_id=".$event->getSku();
	$resultSet=func_query_first($sql);
	if(empty($resultSet))
		return;

	$notification = func_getNotification($handlerName);
	
    $limit = FeatureGateKeyValuePairs::getFeatureGateValueForKey($handlerName.".emailsPerItem");
	if('ALL'===strtoupper($limit) || !is_numeric($limit)) {
		$limit = false;
	} else {
		$limit = (int)$limit;
		if($event->isJIT()) {
			$limit = $limit*FeatureGateKeyValuePairs::getFeatureGateValueForKey($handlerName.".jitDefaultValue");
		} else {
			$limit = $limit*$event->getQuantity();
		}
	}
	
	$customer_infos = func_getEmails_MobileToNotify_from_notifyme($event->getSku(),$limit);
	if(empty($customer_infos))
		return;
    	
    global $smarty,$http_location,$baseurl;
/*    $discount=0;
    	$discountData = DiscountEngine::executeOnProduct($resultSet['style']);
    	$discountamount = $discountData['amount'];

    	if(!empty($discountamount)) {
    		if($discountamount  > $resultSet['price'] ){
    			$discountamount = $resultSet['price'];
    		}
    		$discount=$discountamount;
    	}
 */   
    $hostUrl = HostConfig::$httpHost;
    $url = "$hostUrl/$resultSet[style]?sku=".$event->getSku()."&trackevent=true&utm_source=notifmlr&utm_medium=mailer&utm_campaign=".date("Y-m-d");
    $size = $resultSet['value'];
    $allSizeOptions = Size_unification::getUnifiedSizeByStyleId($resultSet['style'], "array",true);
    $flattenSizeOptions = Size_unification::ifSizesUnified($allSizeOptions);
    if($flattenSizeOptions && !empty($allSizeOptions[$size])) {
    	$size = $allSizeOptions[$size];
    }
    switch($size){
    	case 'XS':
    		$size='Extra-Small';
    		break;
    	case 'S':
    		$size='Small';
    		break;
    	case 'M':
    		$size='Medium';
    		break;
    	case 'L':
    		$size='Large';
    		break;
    	case 'XL':
    		$size='X-Large';
    		break;
    	case 'XXL':
    		$size='XX-Large';
    		break;
    	case 'XXXL':
    		$size='XXX-Large';
    		break;
    }
	$subject="The ".(empty($resultSet['brand'])?"":$resultSet['brand'])." ".(empty($resultSet['articletype'])?"Item":$resultSet['articletype'])." you requested is now available";
    foreach ($customer_infos as $cust){
    	$user = empty($cust['firstname'])?'User':$cust['firstname'];
    	$smarty->assign('user',$user);
    	$smarty->assign('style',$resultSet['name']);
    	$smarty->assign('size',$size);
    	$smarty->assign('req_date',$cust['lastmodified']);
    	$smarty->assign('url',$url);
    	$smarty->assign('search_image',$resultSet['search_image']);
    	$smarty->assign('price',$resultSet['price']);
    	$smarty->assign('discount',$discount);
    	$smarty->assign('finalprice',$resultSet['price']-$discount);
    	if(!empty($cust['mobile'])){
    		 $message = $smarty->fetch('sms/notifymesms.tpl');
    		 func_send_sms_async($cust['mobile'], $message);
    	}
    	
    	if(!empty($cust['email'])){	
    		$header = EmailTemplateResolver::replaceKeywordsWithValues(EmailTemplateResolver::getTemplate('header'));
    		$body = $smarty->fetch('mail/notifymemail.tpl');
    		$footer = EmailTemplateResolver::replaceKeywordsWithValues(EmailTemplateResolver::getTemplate('footer'));
    		$email = $header['body'].$body.$footer['body']; 
    		func_send_mail_async($cust['email'], "", $subject , $email, "Myntra <no-reply@myntra.com>");
    	}
    	
    	db_query("update mk_notification_subs set notified=1,lastmodified='".date("Y/m/d H:i:s")."' where id=".$cust['id']);
    }
   
}