<?php
if (!defined('XCART_START'))
 {
	 header ("Location: ../");
	die ("Access denied");
 }

// Modified for multiple customization area 
function func_not_published_product_data_insert(&$productsInCart, $login, &$masterCustomizationArrayD,$chosenProductConfiguration,$loc='')
{
	
	global $sql_tbl,$weblog,$sqllog,$affiliateInfo;

	$tb_products       = $sql_tbl["products"];
	$tb_image_T = $sql_tbl["images_T"];
	$tb_product_customized_image_rel = $sql_tbl['mk_xcart_product_customized_area_image_rel'];
	$tb_product_customized_area_rel  = $sql_tbl['mk_xcart_product_customized_area_rel'];
	$provider = $login;
	$productDetail = array();
	global $productids ;
	// Check if product id is available


	foreach($productsInCart AS $key=>$value)
	{
		$productDetail =   $productsInCart[$key];
		$checkProductId = "SELECT productid FROM $tb_products WHERE productid='$productDetail[productId]'";
		$checkResult  = db_query($checkProductId);
		$checkRow = db_num_rows($checkResult);


		if($checkRow == 0)
		{
			$productId = $productDetail['productId'];
			$productCode = "MK".$productDetail['productId'];
			$listPrice = $productDetail['productPrice'];
			$productTypeId = $productDetail['producTypeId'];
			$productStyleId = $productDetail['productStyleId'];
			$adddate= time();
			$defaultCustId = $productDetail['defaultCustomizationId'];
			$defaultOrientationId =  $productDetail['defaultOrientationId'];

			$masterCustomizationArray = $masterCustomizationArrayD[$productId];
			
			$caIdArray = array_keys($masterCustomizationArray);
            $customizationareaId = '';
            $orientationId = '';
           
			for($i=0;$i<count($caIdArray);$i++){

				$caId = $caIdArray[$i];
		        $custarray = $masterCustomizationArray[$caId];
		    
		        // loop orientation  
		        foreach($custarray as $_oid => $orinfo){
		           if(is_array($orinfo)){
		           	          	
				       $oId = $_oid;
				       if($defaultCustId == $caId){
	             		  if($defaultOrientationId == $oId){ 
	    	            	$portalPath =   $masterCustomizationArray[$defaultCustId][$defaultOrientationId]['finalcustomizeThumbimage'];
	    	            	$customizationareaId = $caId; 
	    	            	$orientationId = $oId;
	    	            	
	             		  }else{ 
	    	            	$portalPath =   $masterCustomizationArray[$caId][$oId]['finalcustomizeThumbimage']; 
	    	            	$customizationareaId = $caId; 
	    	            	$orientationId = $oId;
	             		  }
	    	              	          
	    			   }else{
	    			        $portalPath =   $masterCustomizationArray[$caId][$oId]['finalcustomizeThumbimage']; 
	    			        $customizationareaId = $caId; 
	    			        $orientationId = $oId;
	    		       }
				       if(!empty($portalPath)) {
					       $portalimage = new ImageConverter($loc.$portalPath,"jpeg");
						   $portalimagePath = str_ireplace('.png', '.jpg', $portalPath);
						   $masterCustomizationArrayD[$productId][$caId][$oId]['finalcustomizeThumbimage'] = $portalimagePath;
			               x_session_register('masterCustomizationArrayD');
			               addToTempImagesList($portalPath);
			               break 2;
				       }
		          		               		       
		           }
		        	 	
		        }
		        		        	        
			}
			//exit;

    if(!empty($masterCustomizationArrayD[$productId][$customizationareaId][$orientationId]['pasteimagepath']) || !empty($masterCustomizationArrayD[$productId][$customizationareaId][$orientationId]['customizedtext']) )
        $portal_imagePath = $masterCustomizationArrayD[$productId][$customizationareaId][$orientationId]['finalcustomizeThumbimage'];
    //else
      //  $portal_imagePath = $masterCustomizationArrayD[$productId][$curcustomizationarea][$orientation]['finalcustomizeThumbimage'];

		$image_Path = $portal_imagePath ; //$masterCustomizationArrayD[$productId][$defaultCustId][$defaultOrientationId]['finalcustomizeimage'];


			if(preg_match('/http:\/\//',$productsInCart[$productId]['designImagePath']))
			{
					$portal_imagePath = $productsInCart[$productId]['designImagePath'] ;
			}
            $productsInCart[$productId]['designImagePath'] = $portal_imagePath;
			 x_session_register('productsInCart');
			$imageSize = filesize($image_Path);
			$fileName = $image_Path;
			$alt = "";
			$md5 = md5($image_Path);

			$insertProduct = "insert into $tb_products (productid,productcode,
															provider,list_price,
															product_type_id,product_style_id,
															add_date,image_portal_t, is_publish)
												values ($productId,'$productCode',
															'$provider','$listPrice',
															'$productTypeId','$productStyleId',
															$adddate,'$portal_imagePath', 0)";

			$result = db_query($insertProduct);

			db_query("insert into $tb_image_T(id, image, image_path, image_size, filename, alt, md5, date)
			 values ($productId,'$image','$image_Path', '$imageSize','$fileName','$alt','$md5',$adddate)");
		}
	}
	include_once(dirname(__FILE__)."/func.mkmisc.php");
	global $cookieprefix;
    $uuid = stripslashes(getMyntraCookie('MYNTRA_SHOPPING_ID'));
    (!empty($uuid)) ?  store_cart_products($uuid,$login,$productsInCart,$productids,$masterCustomizationArrayD) : '';   ############## Store the updated cutomization detail in cookie############

	foreach($masterCustomizationArrayD AS $key1 => $values1)
	{
		    $caIdArray = array_keys($values1);
			$productId = $key1;
			$productStyleId = $productsInCart[$productId]['productStyleId'];
			$defaultCustId = $productsInCart[$productId]['defaultCustomizationId'];


			for($i=0;$i<count($caIdArray);$i++)
			{

					$caId = $caIdArray[$i];
					$custarray = $masterCustomizationArrayD[$key1][$caId];
					$custAreaForCustId = ''; 
					$templateid = $chosenProductConfiguration["selectedtemplate"];
					$addonid = $chosenProductConfiguration["selectedAddon"];
					foreach($custarray as $_oid => $orinfo){
		               if(is_array($orinfo)){
		               	
		                   $oId = $_oid;
		               	   if(!empty($custarray[$oId]['pasteimagepath']) || !empty($custarray[$oId]['customizedtext']) ){
		               	   
		               	       $custAreaForCustId = $caId; 
							   $image_name = $orinfo['finalcustomizeimage'];//which image should it be ????
						       $thumbimage = $orinfo['finalcustomizeThumbimage'];
						       
						       // text image
						       $textimage = $orinfo['finaltextimage'];
						       //end
	
								$image_x = $orinfo['dxstart'];
								$image_y = $orinfo['dystart'];
								/* get the imagx and image y */
						        $startimage_x = $orinfo['dxstart_image'];
						        $startimage_y = $orinfo['dystart_image'];
						        /*end */
			
						        /*get the  adjusted width and height */
						        $adjimage_x = $orinfo['width_in_final'];
						        $adjimage_y = $orinfo['height_in_final'];
						        /*end
			
						        /*get the orignal image height and width */
						        if(!empty($orinfo['orignal_pastefile'])){
						        	$orignal_image = mysql_escape_string($orinfo['orignal_pastefile']);
						        }else{
						           $orignal_image = mysql_escape_string($orinfo['pasteimagepath']);	
						        }
						        
						        
						        if ( $chosenProductConfiguration[$caId."_".$oId."_".'affiliateoriginalimage'] != '')
								{
									$orignal_image =$chosenProductConfiguration[$caId."_".$oId."_".'affiliateoriginalimage'];
								}
								else if($orinfo['affiliateoriginalimage'] != '') 
								{
									$orignal_image = $orinfo['affiliateoriginalimage'];
								}
								
								
						        /* get the imagx and image y  for text*/
						        $starttext_x = $custarray['dxstart_text'];
				    		    $starttext_y = $custarray['dystart_text'];
				        		/*end */
				        		
				        		$customizedtext = $orinfo['customizedtext'];
	
								if(!empty($affiliateInfo[0]['printedText']))
	        		                $text = mysql_escape_string($affiliateInfo[0]['printedText']);  
								else
									$text = mysql_escape_string($customizedtext['fonttext']);
	
	                            $text_x = $customizedtext['textX'];
								$text_y = $customizedtext['textY'];
								$text_width = ""; //is this really needed??
								$text_height = ""; //is this really needed??
								//TODO need to add text color
								if((($pos=strpos($customizedtext['fontcolor'], "#")) !== false))
					    		{
					       			$txtfontcolor = $customizedtext['fontcolor'];
	                    		}
					    		else
					   			{
					      			$txtfontcolor = "#".dechex($customizedtext['fontcolor']);
					    		}

								/* added to handle limited text
								 * customization for RPOS
								 */
								$isTextLimitation = func_query_first_cell("select textLimitation from mk_customization_orientation where id='{$oId}'");
								if($isTextLimitation){
									//convert pixel to inches
									$limit_text_fontsize = number_format(($customizedtext['fontsize']/40), 2, '.', '');//assumption 1"=40px;
									$text_format = "font:".$customizedtext['fonttypename'].","."size:".$limit_text_fontsize.","."color:".$txtfontcolor;
								} else {					    		
						    		$text_format = "font:".$customizedtext['fonttypename'].","."size:".$customizedtext['fontsize'].","."color:".$txtfontcolor;
								}
								
								$bgcolor = $orinfo['bgcolor'];	
																
								db_query("insert into $tb_product_customized_area_rel (product_id,style_id,area_id,orientation_id,image_name,image_x,image_y,image_width,image_height,text,text_x,text_y,text_width,text_height,text_format,orientation_final_image_display,bgcolor,thumb_image,orientation_final_image_print , templateid,addonid) values ('$productId','$productStyleId','$caId','$oId','$image_name','$startimage_x','$startimage_y','$adjimage_x','$adjimage_y','$text','$starttext_x','$starttext_y','$text_x','$text_y','$text_format','".$orignal_image."','".$bgcolor."','".$thumbimage."','".$textimage."','".$templateid."','".$addonid."')");
		               	   
		               	   } // check for paste image  and filter it	 

               	
		               }
					}
					// put the common generated image in database
					if(!empty($custAreaForCustId)){
	                    $area_image_final = $custarray['common_generated_final_image'] ; 
						db_query ("insert into $tb_product_customized_image_rel (product_id,style_id,area_id,area_image_final,templateid,addonid) values ('$productId','$productStyleId','$caId','$area_image_final','".$templateid."','".$addonid."')");	
					 }
					

			}

	}
}
function loadStyleTemplates($styleid,$customizationid){
  global $sql_tbl,$weblog,$sqllog;
  $weblog->info("Inside Funtion :".__FUNCTION__." @ line ".__LINE__." in file ".__FILE__)  ;
  $sql = "SELECT tm.`template_master_id` as template_id, `template_master_name` as template_name , customization_area_id,template_image, template_thumb_image FROM mk_style_templates inner join `mk_template_master` as tm ON tm.`template_master_id` = mk_style_templates.`template_master_id` WHERE styleid='".$styleid."' 
  AND customization_area_id = '".$customizationid."' AND is_active= 1 "; 
  $records = func_query($sql);
  return (!empty($records)) ? $records : array(); 
}
function loadTemplateOrientation($tplid,$oid){
  global $sql_tbl,$weblog,$sqllog;
  $weblog->info("Inside Funtion :".__FUNCTION__." @ line ".__LINE__." in file ".__FILE__)  ;
  $sql = "SELECT *  FROM mk_template_orientation_map 
  WHERE template_id='".$tplid."' 
  AND customization_orientation_id = '".$oid."' "; 
  $records = func_query_first($sql);
  return (!empty($records)) ? $records : array(); 
}
function getSubTemplateId($tplid,$cid){
  global $sql_tbl,$weblog,$sqllog;
  $weblog->info("Inside Funtion :".__FUNCTION__." @ line ".__LINE__." in file ".__FILE__)  ;
  $sql = "SELECT  template_id  FROM mk_style_templates  
  WHERE template_master_id='".$tplid."'  AND customization_area_id = '".$cid."' "; 
  $records = func_query_first($sql);
  return (!empty($records)) ? $records : array(); 
}

function func_get_calendar_thumb_images(){
          $calendar_images = array();
		 
		  $calendar_images[0] = array("name" => "Jan",
														 	"path" => "skin1/mkimages/calendar_images/cup-of-tea.jpg",
		                                                    "large" => "skin1/mkimages/calendar_images/coffeecup_large.jpg"	  
		  );

         
		  $calendar_images[1] = array("name" => "Feb",
														 	"path" => "skin1/mkimages/calendar_images/car.jpg",
			                                                 "large" => "skin1/mkimages/calendar_images/car_large.jpg"
			  );
		
			$calendar_images[2] = array("name" => "March",
														 	"path" => "skin1/mkimages/calendar_images/cruzship.jpg",
				                                              "large" => "skin1/mkimages/calendar_images/cruzship_large.jpg"	);
			
			$calendar_images[3] =      array("name" => "April",
														 	"path" => "skin1/mkimages/calendar_images/boat.jpg",
															"large" => "skin1/mkimages/calendar_images/boat_large.jpg"	);

		
		$calendar_images[4] = array("name" => "May",
														 	"path" => "skin1/mkimages/calendar_images/equation.jpg",
														 "large" => "skin1/mkimages/calendar_images/genius_large.jpg"	);

		$calendar_images[5]= array("name" => "June",
														 	"path" => "skin1/mkimages/calendar_images/inkpen.jpg",
														"large" => "skin1/mkimages/calendar_images/handwriting_large.jpg"	);

		$calendar_images[6] = array("name" => "July",
														 	"path" => "skin1/mkimages/calendar_images/militarytank.jpg",
														"large" => "skin1/mkimages/calendar_images/military_tank_large.jpg"	);

				$calendar_images[7] = array("name" => "Aug",
														 	"path" => "skin1/mkimages/calendar_images/officeinterior.jpg",
															 "large" => "skin1/mkimages/calendar_images/office_large.jpg"	);
			$calendar_images[8] = array("name" => "Sep",
														 	"path" => "skin1/mkimages/calendar_images/pyramids.jpg",
															 "large" => "skin1/mkimages/calendar_images/pyramid_large.jpg"	);
			$calendar_images[9]= array("name" => "Oct",
														 	"path" => "skin1/mkimages/calendar_images/road.jpg",
																 "large" => "skin1/mkimages/calendar_images/road_large.jpg"	);
			 $calendar_images[10] = array("name" => "Nov",
														 	"path" => "skin1/mkimages/calendar_images/signboard.jpg",
															 	 "large" => "skin1/mkimages/calendar_images/signboard_large.jpg"	);
			$calendar_images[11] = array("name" => "Dec",
														 	"path" => "skin1/mkimages/calendar_images/voting-ad.jpg",
															 "large" => "skin1/mkimages/calendar_images/hoarding_large.jpg"	);
		   return  	$calendar_images;
     
}
// Modified for multiple customization area
function func_not_published_wishlist_data_insert($productDetail, $login, &$masterCustomizationArrayD,$chosenProductConfiguration)
{

	global $sql_tbl,$weblog,$sqllog,$affiliateInfo;

	$tb_products       = $sql_tbl["products"];
	$tb_image_T = $sql_tbl["images_T"];
	$tb_product_customized_image_rel = $sql_tbl['mk_xcart_product_customized_area_image_rel'];
	$tb_product_customized_area_rel  = $sql_tbl['mk_xcart_product_customized_area_rel'];
	$provider = $login;
	//$productDetail = array();
	//global $productids ;
	// Check if product id is available

    $productId = $productDetail['productId'];
    $productCode = "MK".$productDetail['productId'];
    $listPrice = $productDetail['productPrice'];
    $productTypeId = $productDetail['producTypeId'];
    $productStyleId = $productDetail['productStyleId'];
    $adddate= time();
    $defaultCustId = $productDetail['defaultCustomizationId'];
    $defaultOrientationId =  $productDetail['defaultOrientationId'];

    $masterCustomizationArray = $masterCustomizationArrayD;

    $caIdArray = array_keys($masterCustomizationArray);
    $customizationareaId = '';
    $orientationId = '';

    for($i=0;$i<count($caIdArray);$i++){

        $caId = $caIdArray[$i];
        $custarray = $masterCustomizationArray[$caId];

        // loop orientation
        foreach($custarray as $_oid => $orinfo){
           if(is_array($orinfo)){

               $oId = $_oid;
               if($defaultCustId == $caId){
                  if($defaultOrientationId == $oId){
                    $portalPath =   $masterCustomizationArray[$defaultCustId][$defaultOrientationId]['finalcustomizeThumbimage'];
                    $customizationareaId = $caId;
                    $orientationId = $oId;

                  }else{
                    $portalPath =   $masterCustomizationArray[$caId][$oId]['finalcustomizeThumbimage'];
                    $customizationareaId = $caId;
                    $orientationId = $oId;
                  }

               }else{
                    $portalPath =   $masterCustomizationArray[$caId][$oId]['finalcustomizeThumbimage'];
                    $customizationareaId = $caId;
                    $orientationId = $oId;
               }
               if(!empty($portalPath)) {
                   $portalimage = new ImageConverter($portalPath,"jpeg");
                   $portalimagePath = str_ireplace('.png', '.jpg', $portalPath);
                   $masterCustomizationArrayD[$caId][$oId]['finalcustomizeThumbimage'] = $portalimagePath;
                   addToTempImagesList($portalPath);
                   break 2;
               }

           }

        }
    }
    if(!empty($masterCustomizationArrayD[$customizationareaId][$orientationId]['pasteimagepath']) || !empty($masterCustomizationArrayD[$productId][$customizationareaId][$orientationId]['customizedtext']) )
        $portal_imagePath = $masterCustomizationArrayD[$customizationareaId][$orientationId]['finalcustomizeThumbimage'];
    $image_Path = $portal_imagePath ;

    $imageSize = filesize($image_Path);
    $fileName = $image_Path;
    $alt = "";
    $md5 = md5($image_Path);

    $checkProductId = "SELECT productid FROM $tb_products WHERE productid=".$productId;
	$checkResult  = db_query($checkProductId);
	$checkRow = db_num_rows($checkResult);

	if($checkRow == 0)
	{
		$insertProduct = "insert into $tb_products (productid,productcode,
													provider,list_price,
													product_type_id,product_style_id,
													add_date,image_portal_t, is_publish)
										values ($productId,'$productCode',
													'$provider','$listPrice',
													'$productTypeId','$productStyleId',
													$adddate,'$portal_imagePath', 0)";

		$result = db_query($insertProduct);

		db_query("insert into $tb_image_T(id, image, image_path, image_size, filename, alt, md5, date)
				     values ($productId,'$image','$image_Path', '$imageSize','$fileName','$alt','$md5',$adddate)");
	}
	//}
	//include_once(dirname(__FILE__)."/func.mkmisc.php");
	//global $cookieprefix;
    //$uuid = stripslashes(getMyntraCookie('MYNTRA_SHOPPING_ID'));
    //(!empty($uuid)) ?  store_cart_products($uuid,$login,$productsInCart,$productids,$masterCustomizationArrayD) : '';   ############## Store the updated cutomization detail in cookie############

	//foreach($masterCustomizationArray AS $key1 => $values1)
	//{
		    $caIdArray = array_keys($masterCustomizationArrayD);
			//$productId = $key1;
			//$productStyleId = $productsInCart[$productId]['productStyleId'];
			//$defaultCustId = $productsInCart[$productId]['defaultCustomizationId'];


			for($i=0;$i<count($caIdArray);$i++)
			{

					$caId = $caIdArray[$i];
					$custarray = $masterCustomizationArrayD[$caId];
					$custAreaForCustId = '';
					$templateid = $chosenProductConfiguration["selectedtemplate"];
					foreach($custarray as $_oid => $orinfo){
		               if(is_array($orinfo)){

		                   $oId = $_oid;
		               	   if(!empty($custarray[$oId]['pasteimagepath']) || !empty($custarray[$oId]['customizedtext']) ){

		               	       $custAreaForCustId = $caId;
							   $image_name = $orinfo['finalcustomizeimage'];//which image should it be ????
						       $thumbimage = $orinfo['finalcustomizeThumbimage'];

						       // text image
						       $textimage = $orinfo['finaltextimage'];
						       //end

								$image_x = $orinfo['dxstart'];
								$image_y = $orinfo['dystart'];
								/* get the imagx and image y */
						        $startimage_x = $orinfo['dxstart_image'];
						        $startimage_y = $orinfo['dystart_image'];
						        /*end */

						        /*get the  adjusted width and height */
						        $adjimage_x = $orinfo['width_in_final'];
						        $adjimage_y = $orinfo['height_in_final'];
						        /*end

						        /*get the orignal image height and width */
						        if(!empty($orinfo['orignal_pastefile'])){
						        	$orignal_image = mysql_escape_string($orinfo['orignal_pastefile']);
						        }else{
						           $orignal_image = mysql_escape_string($orinfo['pasteimagepath']);
						        }


						        /*if ( $chosenProductConfiguration[$caId."_".$oId."_".'affiliateoriginalimage'] != '')
								{
									$orignal_image =$chosenProductConfiguration[$caId."_".$oId."_".'affiliateoriginalimage'];
								}
								else if($orinfo['affiliateoriginalimage'] != '')
								{
									$orignal_image = $orinfo['affiliateoriginalimage'];
								}*/


						        /* get the imagx and image y  for text*/
						        $starttext_x = $custarray['dxstart_text'];
				    		    $starttext_y = $custarray['dystart_text'];
				        		/*end */

				        		$customizedtext = $orinfo['customizedtext'];

								if(!empty($affiliateInfo[0]['printedText']))
	        		                $text = mysql_escape_string($affiliateInfo[0]['printedText']);
								else
									$text = mysql_escape_string($customizedtext['fonttext']);

	                            $text_x = $customizedtext['textX'];
								$text_y = $customizedtext['textY'];
								$text_width = ""; //is this really needed??
								$text_height = ""; //is this really needed??
								//TODO need to add text color
								if((($pos=strpos($customizedtext['fontcolor'], "#")) !== false))
					    		{
					       			$txtfontcolor = $customizedtext['fontcolor'];
	                    		}
					    		else
					   			{
					      			$txtfontcolor = "#".dechex($customizedtext['fontcolor']);
					    		}

					    		$text_format = "font:".$customizedtext['fonttypename'].","."size:".
								$customizedtext['fontsize'].","."color:".$txtfontcolor;
								$bgcolor = $orinfo['bgcolor'];

								db_query("insert into $tb_product_customized_area_rel (product_id,style_id,area_id,orientation_id,image_name,image_x,image_y,image_width,image_height,text,text_x,text_y,text_width,text_height,text_format,orientation_final_image_display,bgcolor,thumb_image,orientation_final_image_print , templateid) values ('$productId','$productStyleId','$caId','$oId','$image_name','$startimage_x','$startimage_y','$adjimage_x','$adjimage_y','$text','$starttext_x','$starttext_y','$text_x','$text_y','$text_format','".$orignal_image."','".$bgcolor."','".$thumbimage."','".$textimage."','".$templateid."')");

		               	   } // check for paste image  and filter it


		               }
					}
					// put the common generated image in database
					if(!empty($custAreaForCustId)){
	                    $area_image_final = $custarray['common_generated_final_image'] ;
						db_query ("insert into $tb_product_customized_image_rel (product_id,style_id,area_id,area_image_final,templateid) values ('$productId','$productStyleId','$caId','$area_image_final','".$templateid."')");
					 }


			}

	//}
}

function func_get_image_dpi_info($cust_area_id,$cust_orient_id,$selected_template){
	global $sql_tbl,$weblog,$sqllog;
	
	$style_template = $sql_tbl["style_template"];
    $template_orient_map = $sql_tbl["template_orient_map"];
	
	$weblog->info("Inside Funtion :".__FUNCTION__." @ line ".__LINE__." in file ".__FILE__)  ;
	$sql = "select 
				so.width_in,
	    		so.height_in	    		
	    	from 
				{$template_orient_map} as so 
			INNER JOIN 
				{$style_template} as st 
			ON 
				st.template_id = so.template_id 
			where  
				st.customization_area_id = {$cust_area_id} AND so.customization_orientation_id = {$cust_orient_id} AND st.template_master_id = {$selected_template}";
		$orientationDetails = func_query($sql);	
		return $orientationDetails;
}


/* connvertion from rgb to hex and vice-versa
 * @i/p:#FFFFFF
 * @return:#FFFFFF => Array{red=>255,green=>255,blue=>255,r=>255,g=>255,b=>255,0=>255,1=>255,2=>255}
 * @i/p:0 65 255 or 0.65.255 or 0,65,255
 * @return:#FFFFFF => 0 65 255 => #0041FF
 */
function rgb2hex2rgb($c){
   if(!$c) return false;
   $c = trim($c);
   $out = false;
  if(preg_match("/^[0-9ABCDEFabcdef\#]+$/i", $c)){
      $c = str_replace('#','', $c);
      $l = strlen($c) == 3 ? 1 : (strlen($c) == 6 ? 2 : false);

      if($l){
         unset($out);
         $out[0] = $out['r'] = $out['red'] = hexdec(substr($c, 0,1*$l));
         $out[1] = $out['g'] = $out['green'] = hexdec(substr($c, 1*$l,1*$l));
         $out[2] = $out['b'] = $out['blue'] = hexdec(substr($c, 2*$l,1*$l));
      }else $out = false;
             
   }elseif (preg_match("/^[0-9]+(,| |.)+[0-9]+(,| |.)+[0-9]+$/i", $c)){
      $spr = str_replace(array(',',' ','.'), ':', $c);
      $e = explode(":", $spr);
      if(count($e) != 3) return false;
         $out = '#';
         for($i = 0; $i<3; $i++)
            $e[$i] = dechex(($e[$i] <= 0)?0:(($e[$i] >= 255)?255:$e[$i]));
             
         for($i = 0; $i<3; $i++)
            $out .= ((strlen($e[$i]) < 2)?'0':'').$e[$i];
                 
         $out = strtoupper($out);
   }else $out = false;
         
   return $out;
}

?>
