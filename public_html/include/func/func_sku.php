<?php
include_once "$xcart_dir/include/func/func.mkcore.php";
include_once "$xcart_dir/include/class/notify/class.notify.php";
include_once "$xcart_dir/include/func/func.notify.php";
include_once("$xcart_dir/modules/apiclient/SkuApiClient.php");
include_once("$xcart_dir/modules/apiclient/AtpApiClient.php");
include_once("$xcart_dir/include/class/oms/EventCreationManager.php");

/**
 *
 * Decrement the SKU availablity by increasing blocked count for the allocated items
 * @param $orderid
 */
function decrement_sku($orderid, $warehouseId){
	update_sku_from_order($orderid, true, $warehouseId);
}

/**
 *
 * Encrement sku count based on order
 * @param unknown_type $orderid
 */
function increment_sku($orderid, $warehouseId){
	update_sku_from_order($orderid, false, $warehouseId);
}

/**
 *
 * Decrease or increasue sku on basis of order
 * @param unknown_type $orderid
 * @param unknown_type $decrease if true then decrease else increase
 */
function update_sku_from_order($orderid, $increase_block_count, $warehouseId){
	global $sql_tbl, $xcart_dir;
	$tb_order_details = $sql_tbl['order_details'];
	$tb_item_option_quantity = $sql_tbl['mk_order_item_option_quantity'];
	$tb_option_sku_mapping = $sql_tbl['mk_styles_options_skus_mapping'];
	
	$sql = "SELECT od.amount as qty, sku_id from $tb_order_details od, $tb_item_option_quantity oq, $tb_option_sku_mapping som where od.itemid = oq.itemid and oq.optionid = som.option_id and od.orderid=$orderid and od.item_status != 'IC'";
	$orderline_resultset=func_query($sql);
	
	if(empty($orderline_resultset))
		return;
	
	foreach($orderline_resultset as $row){
		$skus[$row['sku_id']] = $row['qty'];	
	}
	
	include_once("$xcart_dir/modules/apiclient/SkuApiClient.php");
	
	// decrease or increase blocked count...
	if($increase_block_count) {
		//new order has been place from here
		SkuApiClient::updateSkuBlockedOrderCount($skus, $increase_block_count, "AUTOSYSTEM", $warehouseId);
	} else {
		// order or item has been cancelled
		EventCreationManager::pushInventoryUpdateToAtpEvent($orderid, null, 'UNBLOCK');
		SkuApiClient::updateSkuBlockedOrderCount($skus, false, "AUTOSYSTEM", $warehouseId);
	}
}

/**
 *Returns sku details for the order
 *[serviceability] - true or false
 *[availability]- IS(In stock),OOS(Full Out of stock),POOS(Partial OOS)
 *[items][$itemid][serviceability] - true or false
 *[items][$itemid][stock] = available inventory
 *[items][$itemid][availability] -  IS(In stock),OOS(Full Out of stock),POOS(Partial OOS)
 * @param $productsInCart
 * @returns $sku_details
 */
function get_sku_details($productsInCart){
	global $weblog;
	$skuIds = array();
	foreach($productsInCart as $product) {
		$skuIds[] = $product['sku_id'];
	}
	$sku_details = OrderWHManager::loadWarehouseInvCounts($skuIds);
	
	return $sku_details;
}

/**
 * This API is used to check which item is unavailable
 * @param type $sku_details
 * @param type $productsInCart
 * @return array of unavailable items along with the unavailable count
 */

function check_item_availability($sku_details, $productsInCart) {
    $sku_availability = array();
        foreach($productsInCart as $product) {
		$qty_ordered = $product['quantity'];
		$sku = $sku_details[$product['sku_id']];
		if($qty_ordered > $sku['availableItems']) {
                    // Associative array of sku vs quanitity not available. Assuming that there is no negative sku count
                    $sku_availability[$product["productStyleName"]] = $qty_ordered - $sku['availableItems'];
                }
	}
        return $sku_availability;
}
/**
 * for pp->q in atp world
 */
function check_order_item_availability_atp($productsInCart){
	global $weblog;
	$itemIds = array();
	foreach($productsInCart as $product) {
		$itemIds[] = $product['itemid'];
	}
	
	$sku_details = AtpApiClient::getAvailableInventoryForItems($itemIds);
	
	foreach($productsInCart as $product) {
		$qty_ordered = $product['quantity'];
		$sku = $sku_details[$product['sku_id']];
		if($qty_ordered > $sku['availableCount'])
			return false;
	}
	return $sku_details;
}


/**
 * 
 *Returns info about order serviceability 
 *[serviceability] - true or false
 *[availability]- IS(In stock),OOS(Full Out of stock),POOS(Partial OOS)
 *[items][$itemid][serviceability] - true or false
 *[items][$itemid][stock] = available inventory
 *[items][$itemid][availability] -  IS(In stock),OOS(Full Out of stock),POOS(Partial OOS)
 * @param $orderid
 */
function check_order_item_availability($productsInCart){
	global $weblog;
	$skuIds = array();
	foreach($productsInCart as $product) {
		$skuIds[] = $product['sku_id'];
	}
	$sku_details = OrderWHManager::loadWarehouseInvCounts($skuIds);
	
	foreach($productsInCart as $product) {
		$qty_ordered = $product['quantity'];
		$sku = $sku_details[$product['sku_id']];
		if($qty_ordered > $sku['availableItems'])
			return false;
	}
	return $sku_details;
}
/**
 * 
 * function to get inv_count and wh_count for a product given itemid
 * @param itemid - id of the item
 */
function getSkuIDForItem($itemid) {
	$skuid_row = func_query_first("SELECT sku_id from mk_order_item_option_quantity oq, mk_styles_options_skus_mapping som where oq.optionid = som.option_id and oq.itemid = $itemid");
	return $skuid_row['sku_id'];
}	

/**
 * function updates SKUs for an item
 * @param itemid - id of the item
 * @param increase - true/false
 * @param quantity - no of units to increase/decrease
 * @param reason_code - code of the reason for any movements in inventory
 * @param orderid - orderid for which the item inv count is changing
 */
/*function update_sku_for_item_quantity($itemid, $increase, $quantity, $reason_code, $orderid, $module='wms') {
	$skuid = getSkuIDForItem($itemid);
	if($increase) {
		$value = $quantity;
	} else {
		$value = -($quantity);
	}
	
	$extra_info['orderid']=$orderid;
	$extra_info['value']=$value;
	$extra_info["reason"]=getReasonByCode($reason_code);
	$extra_info["reasonid"]=$extra_info["reason"]['id'];
	$extra_info['user']="AUTOSYSTEM";
		
	$weblog->info("Extra Info - ". $extra_info);
	updateSKUPropertyWMS($skuid, 'inv_count', $value, "AUTOSYSTEM", false, $extra_info);
	$weblog->info("Done updating inv count");
	
}

function parseBreakupString($breakup){
	if(empty($breakup)===true){
		return null;
	}
	$targets=explode(',',$breakup);
	$result;
	$i=0;
	foreach ($targets as $target){
		$result[$i++]=getOptionAndQuantity($target);
	}
	return $result;
}

function getOptionAndQuantity($option_quantity){
	$targets=explode(':',$option_quantity);
	$result;
	if(count($targets)==3){
		$result['option']=trim($targets[1]);
		$result['quantity']=trim($targets[2]);
	}else{
		$result['option']=trim($targets[0]);
		$result['quantity']=trim($targets[1]);
	}

	return $result;
}
*/

function notifySkuInStock($skus) {
	if(isset($skus['id'])) {
		$instockEvent = new InstockEvent();
		$instockEvent->setSku($skus['id']);
		$instockEvent->setQuantity($skus['count']);
		$instockEvent->setJIT($skus['jit']);
		func_handle_instock_customer_nofifyme($instockEvent);
	} else {
		foreach($skus as $sku) {
			if($sku['count'] > 0){
				$instockEvent = new InstockEvent();
				$instockEvent->setSku($sku['id']);
				$instockEvent->setQuantity($sku['count']);
				$instockEvent->setJIT($sku['jit']);
				func_handle_instock_customer_nofifyme($instockEvent);
			}
		}
	}
	return true;
}

function updateSKUPropertyWMS($skuid, $column, $user, $extra_info, $indexSolr=true){
	if($skuid==null || trim($skuid.'')=='')
		return;
	global $sql_tbl, $xcart_dir, $weblog;
	$sql="SELECT m.sku_id as sku_id, po.is_active as enabled from mk_styles_options_skus_mapping m left join mk_product_options po on m.option_id=po.id where m.sku_id =$skuid";
	$skus_resultset=func_query_first($sql);
	if(!$skus_resultset)
		return;
	
	$previous_sku_state=$skus_resultset;
	$notifyRequired = false;
	//$sku_details = SkuApiClient::getSkuDetailsInvCountMap($skuid);
	$skuId2InvRowMap = AtpApiClient::getAvailableInventoryForSkus($skuid);
	$invRow = $skuId2InvRowMap[$skuid];
	$available_count = $invRow['availableCount'];
	$disabled = true;
	if($available_count <= 0) {
		disableEnableSKU($skuid, '0', $user,$available_count,$indexSolr);
		$disabled = false;
	} else {
		disableEnableSKU($skuid,'1',$user,$available_count,$indexSolr);
		$notifyRequired = true;
		$disabled = true;
	}
	
	/* $extra_info['pre_wh_count']= $available_count;
	$extra_info['pre_inv_count']= $available_count; */
	
	/* $changeRequired = true;
	$notifyRequired = false;
	$jitEnable = false;
	
	$prevEnabledstate = $previous_sku_state['enabled'];
	if($column=='enabled' && $prevEnabledstate == $value) {
		// updating the status of the sku to the same state in which it was.. should never happen.. 
		// but avoiding it..
		$weblog->info("No changes in state.. ");
		$changeRequired = false;
	}
	
	$jit_sourcing_enabled = FeatureGateKeyValuePairs::getFeatureGateValueForKey('jit.sourcing.enabled');
	if($column=='enabled') {
		// Disable Sku is if fired...
		if($value == '0' || $value == 'false') {
			// If sku is getting disabled automatically, fired when change count leads to 
			// available count <= 0, then we should check if we can auto jit the sku..
			/* if($jit_sourcing_enabled == 'true' && $user=="AUTOSYSTEM" && $sku_details[$skuid]['jitSourced'] == 0) {
				// check if the sku can be sourced from market now, if yes then do not disable it
				// update jit_sourced property to true
				$can_source_style = check_if_can_source_style($skuid);
				if($can_source_style) {
					$extra_info_sys['reason']=getReasonByCode("AJE");
					$extra_info_sys["reasonid"]=$extra_info_sys["reason"]['id'];
					$extra_info_sys['pre_wh_count']=$available_count;
					$extra_info_sys['pre_inv_count']=$available_count;
					$extra_info_sys['value']='true';
					$extra_info_sys['user']='AUTOSYSTEM';
					disableEnableSKUJitSource($skuid,'true',"AUTOSYSTEM", $available_count, $indexSolr);
					logchange($skuid, $extra_info_sys);
					// if marked as jit.. then no need to disable
					$changeRequired = false;
				} else {
					if($changeRequired)
						disableEnableSKU($skuid,$value,$user,$available_count,$indexSolr);
				}
			} else {
				if($changeRequired)
					disableEnableSKU($skuid,$value,$user,$available_count,$indexSolr);
			}
			if($changeRequired)
				disableEnableSKU($skuid,$value,$user,$available_count,$indexSolr);
		} else { // enable sku is fired...
			// if it is manual enable.. 
			$canEnableSku = true;
			// If sku is getting enabled automatically, fired when change count leads to 
			// available count > 0, then enable it only if it is not admin disabled ..
			if($user=="AUTOSYSTEM") {
				$canEnableSku = ($sku_details[$skuid]['adminDisabled']==1)?false:true;
			}			
			
			if($canEnableSku) {
				// if there is inventory present.. then disable the jit source flag in case it was enabled.
				if($available_count > 0 && $sku_details[$skuid]['jitSourced'] == 1) {
					$extra_info_sys['reason']=getReasonByCode("AJD");
					$extra_info_sys["reasonid"]=$extra_info_sys["reason"]['id'];
					$extra_info_sys['pre_wh_count']=$available_count;
					$extra_info_sys['pre_inv_count']=$available_count;
					$extra_info_sys['value']='false';
					$extra_info_sys['user']='AUTOSYSTEM';
					disableEnableSKUJitSource($skuid,'false',"AUTOSYSTEM", $available_count, $indexSolr);
					logchange($skuid, $extra_info_sys);
					$notifyRequired = true;
				} else if ($jit_sourcing_enabled == 'true' && $available_count <= 0 && $sku_details[$skuid]['jitSourced'] == 0) {
					// if inv count is < 0.. then try enabling jit source flag in case it was not.
					$can_source_style = check_if_can_source_style($skuid);
					if($can_source_style) {
						$extra_info_sys['reason']=getReasonByCode("AJE");
						$extra_info_sys["reasonid"]=$extra_info_sys["reason"]['id'];
						$extra_info_sys['pre_wh_count']=$available_count;
						$extra_info_sys['pre_inv_count']=$available_count;
						$extra_info_sys['value']='true';
						$extra_info_sys['user']='AUTOSYSTEM';
						disableEnableSKUJitSource($skuid,'true',"AUTOSYSTEM", $available_count, $indexSolr);
						logchange($skuid, $extra_info_sys);
						$notifyRequired = true;
						$jitEnable = true;
					}
				} else if ($available_count > 0) {
					$notifyRequired = true;
				}
				if($changeRequired)
					disableEnableSKU($skuid,$value,$user,$available_count,$indexSolr);
			} else {
				$changeRequired = false;
			}
		}
	}
	
	if($changeRequired) {
		logchange($skuid,$extra_info);
	} */
	
	// if($notifyRequired) {
	// 	$sku = array();
	// 	$sku['id'] = $skuid;
	// 	$sku['count'] = $available_count;
	// 	$status = notifySkuInStock($sku);
	// }
	return $disabled;
}

function disableEnableSkusInBulkAtp($skuids) {
	global $weblog;
	$weblog->debug("disableEnableSKU:begin:".implode(",", $skuids));
	
	$oosSkus = array(); $inStockSkus = array();
	$skuId2InvRowMap = AtpApiClient::getAvailableInventoryForSkus($skuids);
	foreach($skuids as $skuId) {
		if($skuId2InvRowMap[$skuId]['availableCount'] > 0) {
			$weblog->debug("disableEnableSkusInBulkAtp1 $skuId2InvRowMap[$skuId]['availableCount']");
			$inStockSkus[] = $skuId;
		} else {
			$weblog->debug("disableEnableSkusInBulkAtp2 $skuId2InvRowMap[$skuId]['availableCount']");
			$oosSkus[] = $skuId;
		}
	}
	return array($oosSkus, $inStockSkus);
}

function disableEnableSKU($skuid,$enable,$user,$available_inv_count=0,$indexSolr=true){
	global $sql_tbl,$xcart_dir, $weblog;
	$weblog->debug("disableEnableSKU:begin:".$skuid); 
	$tb_mk_product_options = $sql_tbl['mk_product_options'];
	$tb_mk_styles_options_skus_mapping=$sql_tbl['mk_styles_options_skus_mapping'];

	// sometimes there might not be any mapping between an option and sku
	$sku_option_style_sql = "SELECT option_id, style_id from $tb_mk_styles_options_skus_mapping where sku_id =$skuid";
	$sku_option_style_row = func_query_first($sku_option_style_sql);
	if($sku_option_style_row && !empty($sku_option_style_row)) {
		//PORTAL-3396: cmsteam: stop updating product options's active flag based on inventory counts
		//$sql="UPDATE $tb_mk_product_options SET is_active=$enable where id = ".$sku_option_style_row['option_id'];
		//db_query($sql);
		
		if($indexSolr==true) {
			define("FUNC_SKU_CALL",'Y');
			if(!empty($sku_option_style_row['style_id'])){
				include_once($xcart_dir.'/include/solr/solrProducts.php');
				include_once($xcart_dir.'/include/solr/solrUpdate.php');
				$weblog->debug("disableEnableSKU:addStyleToSolrIndex:begin:".$skuid); 
				@addStyleToSolrIndex($sku_option_style_row['style_id'], false);
				$weblog->debug("disableEnableSKU:addStyleToSolrIndex:done:".$skuid); 
			}
			// This code is also present in addStyleToSolrIndex. Need to verify with Sanjay if indexSolr could be false
			// invalidate the pdp cache
			$weblog->debug("disableEnableSKU:CacheInvalidation:begin");
			include_once($xcart_dir."/Cache/Cache.php");
	                include_once("$xcart_dir/Cache/CacheKeySet.php");
			$cache = new Cache(PDPCacheKeys::$prefix.$sku_option_style_row['style_id'], PDPCacheKeys::keySet());
			$cache->invalidate();
			$weblog->debug("disableEnableSKU:CacheInvalidation:done:".$sku_option_style_row['style_id']);
		}
	}
	$weblog->debug("disableEnableSKU:done:".$skuid); 
}

function disableEnableSKUJitSource($skuid,$enable,$user,$available_count, $indexSolr) {
	global $sql_tbl, $xcart_dir;
	if($enable === 'true' || $enable === '1'){
		$enable = 1;
	} else {
		$enable = 0;
	}
	$tb_mk_product_options = $sql_tbl['mk_product_options'];
	$tb_mk_styles_options_skus_mapping=$sql_tbl['mk_styles_options_skus_mapping'];

	$params['jitSourced'] = $enable;
	
	SkuApiClient::updateSku($skuid, $params);
	
	$sku_option_style_sql = "SELECT option_id, style_id from $tb_mk_styles_options_skus_mapping where sku_id =$skuid";
	$sku_option_style_row = func_query_first($sku_option_style_sql);
	
	if( !$enable ) {
		if($available_count <= 0 && $user != "AUTOSYSTEM") {
			if($sku_option_style_row && !empty($sku_option_style_row)) {
				//PORTAL-3396: cmsteam: stop updating product options's active flag based on inventory counts
				//$optionsSql="UPDATE $tb_mk_product_options SET is_active=0 where id = ". $sku_option_style_row['option_id'];
				//db_query($optionsSql);
			}
		}
	}
	
	define("FUNC_SKU_CALL",'Y');
	if(!empty($sku_option_style_row['style_id'])&& $indexSolr==true){
		include_once($xcart_dir.'/include/solr/solrProducts.php');
		include_once($xcart_dir.'/include/solr/solrUpdate.php');
		@addStyleToSolrIndex($sku_option_style_row['style_id'], false, false);
	}
}

/*
function sendThresholdCrossMail($skuid,$user){
	global $smarty;
	$sql="select ps.id as style_id,ps.name as style_name,apf.applied_filters  as brand
	,po.name as opt_name,po.value opt_value	from mk_styles_options_skus_mapping m,mk_product_style ps,mk_applied_filters apf,mk_product_options po 
		Where m.style_id=ps.id and apf.generic_type='style' and apf.generic_id=ps.id and m.option_id=po.id and 
		apf.filter_group_id=(SELECT id FROM mk_filter_group where group_name='Brands')  and m.sku_id=$skuid";
	$resultset = func_query($sql,true);
	if(empty($resultset))
		return;
		
	$skudetails = SkuApiClient::getSkuDetails($skuid);
	$resultset[0]['sku_code'] =$skudetails[0]['code'];		

	$smarty->assign("title",'Threshold Notification');
	$smarty->assign("message",'Following SKU count has crossed threshold limit of '.$resultset[0]['thrs_count']);
	$smarty->assign("product_name",$resultset[0]['style_name']);
	$smarty->assign("sku_code",$resultset[0]['sku_code']);
	$smarty->assign("product_link","http://www.myntra.com/admin/productstyle_action_new.php?mode=modifystyle&style_id=".$resultset[0]['style_id']);
	$smarty->assign("brand",$resultset[0]['brand']);
	$smarty->assign("user",$user);
	//TODO this is not the way to do this
//	$smarty->assign("current_count",$resultset[1]['inv_count']);
//	$smarty->assign("threshold_count",$resultset[1]['thrs_count']);
	$smarty->assign("bottom_message",'');
	$contents=$smarty->fetch("NotificationMail.tpl",$smarty);

	sendNotificationMail($resultset[0]['sku_code'].'#'.$resultset[0]['brand'].'('.$resultset[0]['style_name'].' '.$resultset[0]['opt_name'].':'.$resultset[0]['opt_value'].') count has crossed Threshold ',constant("INVENTORY_STOCK_NOTIFICATIONS"),$contents);
}

function sendSkuEnabledMail($skuid,$user,$available_inv_count=0){
	global $smarty;
	$sql="select ps.id as style_id,ps.name as style_name,apf.applied_filters  as brand
	,po.name as opt_name,po.value opt_value	from mk_styles_options_skus_mapping m,mk_product_style ps,mk_applied_filters apf,mk_product_options po 
		Where m.style_id=ps.id and apf.generic_type='style' and apf.generic_id=ps.id and m.option_id=po.id and 
		apf.filter_group_id=(SELECT id FROM mk_filter_group where group_name='Brands')  and m.sku_id=$skuid";
	$resultset = func_query($sql,true);

	if(empty($resultset))
		return;
		
	$skudetails = SkuApiClient::getSkuDetails($skuid);
	$resultset[0]['sku_code'] =$skudetails[0]['code'];		
	
	$smarty->assign("title",'SKU Enabled Notification');
	$smarty->assign("message","Following SKU has been enabled on the portal. We have a total of $available_inv_count available items today.");
	$smarty->assign("product_name",$resultset[0]['style_name']);
	$smarty->assign("sku_code",$resultset[0]['sku_code']);
	$smarty->assign("product_link","http://www.myntra.com/admin/productstyle_action_new.php?mode=modifystyle&style_id=".$resultset[0]['style_id']);
	$smarty->assign("brand",$resultset[0]['brand']);
	$smarty->assign("user",$user);

	$smarty->assign("bottom_message",'');
	$contents=$smarty->fetch("NotificationMail.tpl",$smarty);
	sendNotificationMail($resultset[0]['sku_code'].'#'.$resultset[0]['brand'].'('.$resultset[0]['style_name'].' '.$resultset[0]['opt_name'].':'.$resultset[0]['opt_value'].') has been enabled ',constant("INVENTORY_STOCK_NOTIFICATIONS"),$contents);
}

function sendSkuDisabledMail($skuid,$user,$available_inv_count=0){
	global $smarty;
		$sql="select ps.id as style_id,ps.name as style_name,apf.applied_filters  as brand
	,po.name as opt_name,po.value opt_value	from mk_styles_options_skus_mapping m,mk_product_style ps,mk_applied_filters apf,mk_product_options po 
		Where m.style_id=ps.id and apf.generic_type='style' and apf.generic_id=ps.id and m.option_id=po.id and 
		apf.filter_group_id=(SELECT id FROM mk_filter_group where group_name='Brands')  and m.sku_id=$skuid";
	$resultset = func_query($sql,true);
	if(empty($resultset))
		return;
		
	$skudetails = SkuApiClient::getSkuDetails($skuid);
	$resultset[0]['sku_code'] =$skudetails[0]['code'];		
	
	$smarty->assign("title",'SKU Disabled Notification');
	$smarty->assign("message",'Following SKU has been disabled on the portal effective immediately. This can no longer be ordered from the website.');
	$smarty->assign("product_name",$resultset[0]['style_name']);
	$smarty->assign("sku_code",$resultset[0]['sku_code']);
	$smarty->assign("product_link","http://www.myntra.com/admin/productstyle_action_new.php?mode=modifystyle&style_id=".$resultset[0]['style_id']);
	$smarty->assign("brand",$resultset[0]['brand']);
	$smarty->assign("user",$user);

	if($user=='AUTOSYSTEM'){
		$smarty->assign("bottom_message",'This will be enabled automatically once the new stock has been inwared in the online inventory system');
	}else{
		$smarty->assign("bottom_message",'This SKU will be available once admin enables it back');
	}

	$contents=$smarty->fetch("NotificationMail.tpl",$smarty);
	sendNotificationMail($resultset[0]['sku_code'].'#'.$resultset[0]['brand'].'('.$resultset[0]['style_name'].' '.$resultset[0]['opt_name'].':'.$resultset[0]['opt_value'].') has been disabled ',constant("INVENTORY_STOCK_NOTIFICATIONS"),$contents);
}


function sendNotificationMail($subject,$toList,$htmlContent){
	global $smarty;
	global $xcart_dir;
	include_once $xcart_dir.'/include/func/func.mkcore.php';
	$subject = "$subject" . date("d/M/Y : H:i:s", time());
	$mail_detail = array(
                        "to"=>$toList,
                        "subject"=>$subject,
                        "content"=>$htmlContent,
                        "from_name"=>'Myntra Inventory System',
                        "from_email"=>'admin@myntra.com',
                        "header"=>"Content-Type: text/html; charset=ISO-8859-1 " . "\n"
                        );
                        $flag = send_mail_on_domain_check($mail_detail);
}
*/

function getUpdateSku($skuid){
	$sql="SELECT ps.name as style_name,po.name as option_name,po.value as option_value,m.sku_id as sku_id,po.is_active as sku_enabled  mk_styles_options_skus_mapping m 
	left join mk_product_options po on m.option_id=po.id 
    left join mk_product_style ps on ps.id=m.style_id 
    where  m.sku_id=$skuid";
	$options_row=func_query_first($sql);
	
	$sku_details = SkuApiClient::getSkuDetails($skuid);
	
	$sku = array();
	$sku['sku_id'] = $sku_details[0]['id'];
	$sku['sku_code'] = $sku_details[0]['code'];
	$sku['sku_enabled'] = $options_row['sku_enabled'];
	$sku['style_name'] = $options_row['style_name'];
	$sku['option_value'] = $options_row['option_value'];
	$sku['jit_sourced'] = $sku_details[0]['jitSourced'];
	
	return $sku;
}

function getUpdateSkuByCode($sku_code){
	$sku_details = SkuApiClient::getSkuDetailsForSkuCode($sku_code);
	
	$sql="SELECT ps.name as style_name,po.name as option_name,po.value as option_value,m.sku_id as sku_id,$sku_code as sku_code,po.is_active as sku_enabled from
	mk_styles_options_skus_mapping m left join mk_product_options po on m.option_id=po.id 
    left join mk_product_style ps on ps.id=m.style_id 
    where m.sku_id =".$sku_details[0]['id'];
    
	$options_row = func_query_first($sql);
	
	$sku = array();
	$sku['sku_id'] = $sku_details[0]['id'];
	$sku['sku_code'] = $sku_details[0]['code'];
	$sku['sku_enabled'] = $options_row['sku_enabled'];
	$sku['style_name'] = $options_row['style_name'];
	$sku['option_value'] = $options_row['option_value'];
	$sku['jit_sourced'] = $sku_details[0]['jitSourced'];
	
	return $sku;
}

function isSkuExistingByCode($sku_code){

	$skuId=getSkuIDByCode($sku_code);
	if(empty($skuId))
	return false;

	return true;
}

function getSkuIDByCode($sku_code){
	global $sql_tbl;
	$resultset = SkuApiClient::getSkuDetailsForSkuCode($sku_code);
	return $resultset[0]['id'];
}

function logchange($skuid,$info){
	global $sql_tbl,$xcart_dir;
	$tb_order_details = $sql_tbl['order_details'];
	$tb_mk_product_options = $sql_tbl['mk_product_options'];
	$tb_mk_styles_options_skus_mapping=$sql_tbl['mk_styles_options_skus_mapping'];
	$tb_mk_skus_logs=$sql_tbl['mk_skus_logs'];
	$tb_mk_skus_inv_order_logs=$sql_tbl['mk_skus_inv_order_logs'];
	$tb_mk_skus_inv_manual_mod_logs=$sql_tbl['mk_skus_inv_manual_mod_logs'];

	$sql="INSERT INTO $tb_mk_skus_logs (skuid,reasonid,change_by,value,extra_info,pre_inv_count,pre_wh_count) VALUES($skuid,$info[reasonid],'$info[user]','$info[value]','$info[extra_info]',$info[pre_inv_count],$info[pre_wh_count])";
	db_query($sql);
	$last_id_rs = func_query("select last_insert_id() as id");
	$log_id=$last_id_rs[0]['id'];

	if($info['reason']['action_type']==="I" && $info['reason']['action_mode_manual']==1){
		if(!empty($info[vendorid])){
			$sql="INSERT into $tb_mk_skus_inv_manual_mod_logs (sku_log_id,lot_id,vendor_id) values($log_id,'$info[lotid]',$info[vendorid])";
			db_query($sql);
		}

	}else if(($info['reason']['action_type']==="I" || $info['reason']['action_type']==="D") && $info['reason']['action_mode_manual']==0){
		$sql="INSERT into $tb_mk_skus_inv_order_logs (sku_log_id,order_id) values($log_id,$info[orderid])";
		db_query($sql);
	}
}

function getVendorIdByCode($code){
	global $sql_tbl;
	$tb_mk_inv_vendor_master=$sql_tbl['mk_inv_vendor_master'];
	$sql="SELECT id from $tb_mk_inv_vendor_master WHERE code='$code'";
	$resultset=func_query($sql);
	return $resultset[0]['id'];
}

function getReasonIdByCode($code){
	global $sql_tbl;
	$tb_mk_inventory_movement_reasons =$sql_tbl['mk_inventory_movement_reasons'];
	$sql="SELECT id FROM $tb_mk_inventory_movement_reasons where code='$code'";
	$resultset=func_query($sql);
	return $resultset[0]['id'];
}
function getReasonById($id){
	global $sql_tbl;
	$tb_mk_inventory_movement_reasons =$sql_tbl['mk_inventory_movement_reasons'];
	$sql="SELECT id,text,action_type,inv_update,wh_update,is_for_system,code,action_mode_manual FROM $tb_mk_inventory_movement_reasons where id=$id";
	$resultset=func_query($sql);
	$row=$resultset[0];
	$ret['id']=$row['id'];
	$ret['text']=$row['text'];
	$ret['action_type']=$row['action_type'];
	$ret['inv_update']=$row['inv_update'];
	$ret['wh_update']=$row['wh_update'];
	$ret['is_for_system']=$row['is_for_system'];
	$ret['code']=$row['code'];
	$ret['action_mode_manual']=$row['action_mode_manual'];
	return $ret;
}

function getReasonByCode($code){
	global $sql_tbl;
	$tb_mk_inventory_movement_reasons =$sql_tbl['mk_inventory_movement_reasons'];
	$sql="SELECT  id,text,action_type,inv_update,wh_update,is_for_system,code,action_mode_manual  FROM $tb_mk_inventory_movement_reasons where code='$code'";
	$resultset=func_query($sql);
	$row=$resultset[0];
	$ret['id']=$row['id'];
	$ret['text']=$row['text'];
	$ret['action_type']=$row['action_type'];
	$ret['inv_update']=$row['inv_update'];
	$ret['wh_update']=$row['wh_update'];
	$ret['is_for_system']=$row['is_for_system'];
	$ret['code']=$row['code'];
	$ret['action_mode_manual']=$row['action_mode_manual'];
	return $ret;
}

function getSkuCounts($skuid){
	global $xcart_dir;
	
	$sku_details = SkuApiClient::getSkuDetailsWithInvCountsForIds($skuid);
	$sku = array();
	$sku['inv_count']= $sku_details[0]['invCount'];
	$sku['wh_count']= $sku_details[0]['invCount'];
	$sku['jit_sourced']=$sku_details[0]['jitSourced'];
	
	return $sku;
}

function canEnableSku($skuid) {
	$sku_details = SkuApiClient::getSkuDetailsWithInvCountsForIds($skuid);
	if($sku_details[0]['jitSourced'] || $sku_details[0]['invCount'] > 0) {
		return true;
	}
	return false;
}

function handle_lot($skuid,$value,$extra_info){
	$reason=$extra_info['reason'];
	if($reason['code']=='NEWP'){
		$sql="select count(*) as numrows from mk_lot_details where lot_id='$extra_info[lotid]' and skuid=$skuid";
		$resultSet=func_query($sql);
		if($resultSet[0]['numrows']==0){
			$jit_inward = $extra_info['inward_type']=='jit'?1:0; 
			$sql="insert into mk_lot_details (lot_id,skuid,quantity,price,jit_inward) values('$extra_info[lotid]',$skuid,$extra_info[value],$extra_info[price],'$jit_inward')";
		}else{
			$sql="update mk_lot_details set quantity=(quantity+$extra_info[value]) where lot_id='$extra_info[lotid]' and skuid=$skuid";
		}
		db_query($sql);
	}else if($reason['action_type']=='I' && $reason['wh_update']==='1'){
		$sql="select count(*) as numrows from mk_lot_details where skuid=$skuid";
		$resultSet=func_query($sql);
		if($resultSet[0]['numrows']!=0){
			$valueToIncrease=($extra_info[value]);
			while(!empty($valueToIncrease)&&$valueToIncrease>0){
				$sql="select lot_id,quantity,quantity_sold from  mk_lot_details where quantity_sold>0 and skuid=$skuid order by lastmodified desc limit 1";
				$resultSet=func_query($sql);
				if(empty($resultSet))
					return;
				$quantityCanBeUpdated=$resultSet[0]['quantity_sold'];
				$lot_id=$resultSet[0]['lot_id'];
				if($quantityCanBeUpdated>=$valueToIncrease){
					$sql="update mk_lot_details set quantity_sold=(quantity_sold-$valueToIncrease) where lot_id='$lot_id' and skuid=$skuid";
				}else{
					$sql="update mk_lot_details set quantity_sold=(quantity_sold-$quantityCanBeUpdated) where lot_id='$lot_id' and skuid=$skuid";
					
				}
				$valueToIncrease-=$quantityCanBeUpdated;
				db_query($sql);
			}
		}
	}else if($reason['action_type']=='D' && $reason['wh_update']==='1'){
		$sql="select count(*) as numrows from mk_lot_details where skuid=$skuid";
		$resultSet=func_query($sql);
		if($resultSet[0]['numrows']!=0){
			$valueToDecrease=-($extra_info[value]);
			while(!empty($valueToDecrease)&&$valueToDecrease>0){
				$sql="select lot_id,quantity,quantity_sold from  mk_lot_details where quantity_sold<quantity  and skuid=$skuid order by lastmodified asc limit 1";
				$resultSet=func_query($sql);
				if(empty($resultSet))
					return;
				$quantityCanBeUpdated=$resultSet[0]['quantity']-$resultSet[0]['quantity_sold'];
				$lot_id=$resultSet[0]['lot_id'];
				if($quantityCanBeUpdated>=$valueToDecrease){
					$sql="update mk_lot_details set quantity_sold=(quantity_sold+$valueToDecrease) where lot_id='$lot_id' and skuid=$skuid";
				}else{
					$sql="update mk_lot_details set quantity_sold=(quantity_sold+$quantityCanBeUpdated) where lot_id='$lot_id' and skuid=$skuid";
					
				}
				$valueToDecrease-=$quantityCanBeUpdated;
				db_query($sql);
			}
		
		}
		
	}
}

function check_if_can_source_style($skuid) {
	global $sql_tbl, $weblog;
	$style_option_sku_tbl = $sql_tbl[mk_styles_options_skus_mapping];
	$style_props_tbl = $sql_tbl[style_properties];
	
	// get style_id for this sku...
	$style_id_sql = "SELECT style_id from $style_option_sku_tbl where sku_id = $skuid";
	$style_id_row = func_query_first($style_id_sql);
	
	$can_be_sourced = false;
	if($style_id_row && count($style_id_row) > 0) {
		$style_id = $style_id_row['style_id'];
		
		$style_fashion_props_query = "SELECT global_attr_brand, global_attr_fashion_type, global_attr_season, global_attr_year from $style_props_tbl WHERE style_id = $style_id";
		$style_fashion_props_row = func_query_first($style_fashion_props_query);
		
		$can_be_sourced = func_check_can_source_style_with_attributes($style_fashion_props_row['global_attr_brand'], 
												$style_fashion_props_row['global_attr_fashion_type'],
												$style_fashion_props_row['global_attr_season'],
												$style_fashion_props_row['global_attr_year']);
												
		$weblog->info("Style id - $style_id - can be jit sourced now - $can_be_sourced");
	}
	return $can_be_sourced;
}

function func_check_can_source_style_with_attributes($brandname, $fashiontype, $season, $year) {
	global $weblog, $sql_tbl;
	$style_props_tbl = $sql_tbl[style_properties];
	$brandIdsForJitSourcing = WidgetKeyValuePairs::getWidgetValueForKey('brandsForJitSourcing');
	$brandNames = array();
	if($brandIdsForJitSourcing && trim($brandIdsForJitSourcing) != "") {
		$brandNamesQuery = "SELECT attribute_value from mk_attribute_type_values where attribute_type_id = 1 and id in ($brandIdsForJitSourcing)";
		$brandNamesResults = func_query($brandNamesQuery);
		foreach ($brandNamesResults as $brand_row) {
			$brandNames[] = $brand_row['attribute_value'];
		}
	}
	
	$weblog->info("Brand Names enabled for jit sourcing = ". implode(", ", $brandNames));
	$can_be_sourced = false;
	if(in_array($brandname, $brandNames)) {
		if( $fashiontype == 'Core') {
			$can_be_sourced = false;
		} else {
			//$current_season_query = "SELECT global_attr_season, global_attr_year from $style_props_tbl where global_attr_season is not NULL order by add_date DESC limit 1";
			//$current_season_row = func_query_first($current_season_query);
			$seasonsForJitSourcing = WidgetKeyValuePairs::getWidgetValueForKey('currentSeasonsForJitSourcing');
			$weblog->info("Seasons for jit sourcing = $seasonsForJitSourcing");
			if($seasonsForJitSourcing && $seasonsForJitSourcing!="") {
				$seasons_array = explode(",", $seasonsForJitSourcing);
				// If it is from fashion type and its season+year is same as current most season+year..
				// then mark it as jit..
				if( in_array($season.":".$year, $seasons_array) ){
					$can_be_sourced = true;
				}
			}
		}
	}
	return $can_be_sourced;
}

function reIndexSKU($skuid) {
	global $sql_tbl,$xcart_dir;
	$tb_mk_styles_options_skus_mapping=$sql_tbl['mk_styles_options_skus_mapping'];

	$sql="SELECT style_id from $tb_mk_styles_options_skus_mapping where sku_id =$skuid";
	$style_resultset=func_query($sql);
	
	define("FUNC_SKU_CALL",'Y');
	if(!empty($style_resultset[0]['style_id'])){
		include_once($xcart_dir.'/include/solr/solrProducts.php');
		include_once($xcart_dir.'/include/solr/solrUpdate.php');
		@addStyleToSolrIndex($style_resultset[0]['style_id'], false, false);
	}
}

function reIndexSKUsInBulk($skuids) {
	global $sql_tbl,$xcart_dir;
	$tb_mk_styles_options_skus_mapping=$sql_tbl['mk_styles_options_skus_mapping'];

	$sql="SELECT distinct style_id from $tb_mk_styles_options_skus_mapping where sku_id in (".implode(',', $skuids).")";
	$style_resultset=func_query($sql);
	
	define("FUNC_SKU_CALL",'Y');
	if($style_resultset && !empty($style_resultset)){
		include_once($xcart_dir.'/include/solr/solrProducts.php');
		include_once($xcart_dir.'/include/solr/solrUpdate.php');
		foreach ($style_resultset as $row) {
			@addStyleToSolrIndex($row['style_id'], false, false, false);
		}
	}
}

function checkSkuOutOfStock($skuIds, $warehouseId) {
	$items = ItemApiClientOld::getStoredItemsForSku($skuIds, $warehouseId);
	if(is_array($items)) {
		if(count($items) == 0)
			return true;
		else
			return false;			
	} else {
		return $items;
	}
}

?>