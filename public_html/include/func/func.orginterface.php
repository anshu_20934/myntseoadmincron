<?php
include_once("func.send_query.php");
if (!defined('XCART_START'))
 {
	 header ("Location: ../");
	die ("Access denied");
 }
 
 
 #
 #Function Name: func_configure_org_account
 #Configure the account for grant access
 #
 function func_configure_org_account($orgId, $posted_config_arr){
 	
 	global $sql_tbl;
 	
 	$tb_org_config_master = $sql_tbl['org_config_master'];
	$tb_org_config_map = $sql_tbl['org_config_map'];
	
	$sql = "SELECT id FROM $tb_org_config_master";
	$confid_id_arr = func_query($sql);
	foreach($confid_id_arr AS $key=>$value){
		$configid = $value['id'];
		if(in_array($configid, $posted_config_arr))
			$configvalue = 1;
		else
			$configvalue = 0;
			
		$sql = "REPLACE INTO $tb_org_config_map(orgid, configid, configvalue) VALUES('".$orgId."','".$configid."','".$configvalue."')";
		db_query($sql);
	}
}

#
#Function Name: func_search_org_account
#Return search query for ORG account
#
function func_search_org_account($accountid=0,$accountname="",$account_status="",$activate_since=""){
	global $sql_tbl;
	$tb_org_accounts = $sql_tbl['org_accounts'];
	$sql = "SELECT id,CONCAT(subdomain,\".myntra.com\") as subdomain,status,FROM_UNIXTIME(activate_date, '%d-%m-%Y') AS active_since FROM $tb_org_accounts WHERE login<>'' "; 
	
	if($accountid!=0){
		$sql .= " AND id='".$accountid."' ";
	}
	if($accountname!=""){
		$sql .= " AND accountname LIKE '%".$accountname."%' ";
	}
	if($account_status!="" AND $account_status!="ss"){
		$sql .= " AND status='".$account_status."' ";
	}
	if($activate_since!=""){
		$sql .= " AND FROM_UNIXTIME(activate_date, '%m/%d/%Y')=FROM_UNIXTIME('".$activate_since."', '%m/%d/%Y')";
	}
	return $sql;
}

#
#Function Name: func_create_organization_customer_account
#Return TRUE OR FALSE
#

function func_create_organization_customer_account($email, $firstname, $lastname, $accounturl, $email, $phone, $address1, $city, $zipcode, $state, $country, $password){
global $sql_tbl;
$tb_customer = $sql_tbl['customers'];
$tb_org_accounts = $sql_tbl['org_accounts'];
$register = "success";
$sql = "SELECT login FROM $tb_customer WHERE login= '".$email."'";
$checkuserid = func_query_first_cell($sql);
if($checkuserid != $email){	
	$profile_values = array();
	$profile_values['login'] = $email;
	$profile_values['usertype'] = "C";
	$profile_values['account_type'] = "OG";
	$password = get_rand_id(8);
	$crypted = addslashes(text_crypt($password));
	$profile_values['password'] = $crypted;
	$profile_values['phone'] = $phone;
	$profile_values['firstname'] = $firstname;
	$profile_values['lastname'] = $lastname;
	$profile_values['b_firstname'] = $firstname;
	$profile_values['b_lastname'] = $lastname;
	$profile_values['s_firstname'] = $firstname;
	$profile_values['s_lastname'] = $lastname;
	$profile_values['b_address'] = $address1;
	$profile_values['b_city'] = $city;
	$profile_values['b_state'] = $state;
	$profile_values['b_zipcode'] = $zipcode;
	$profile_values['b_county'] = $country;
	$profile_values['s_address'] = $address1;
	$profile_values['s_city'] = $city;
	$profile_values['s_state'] = $state;
	$profile_values['s_zipcode'] = $zipcode;
	$profile_values['s_county'] = $country;
	$profile_values['email'] = $email;
	$profile_values['termcondition'] = '1';
	$profile_values['addressoption'] = 'M';
	
	$sql = "INSERT INTO $tb_org_accounts(created_date) VALUES('".$created_date."')";
	db_query($sql);
	$orgId = db_insert_id();
	$profile_values['orgid'] = $orgId;
	func_array2insert($sql_tbl['customers'], $profile_values);
	return $orgId;
}else{
	$register = "failed";
	return $register;
}
}

#
#Function for loading the all selected product type tab for customize section
#
function func_load_product_type_for_customize_tabs($orgId){
global $sql_tbl;
$sql="SELECT c.id AS ptypeid, c.label AS ptypename
	  FROM $sql_tbl[org_style_map] a INNER JOIN  $sql_tbl[mk_product_style] b ON a.styleid=b.id
	  INNER JOIN  $sql_tbl[mk_product_type] c ON b.product_type=c.id  	
	  WHERE a.id='".$orgId."' AND a.status=1 and customize='Y' ";
	 

$types = func_query($sql);

$producttypes = array();
foreach($types AS $key=>$value){
	if(!array_key_exists($value['ptypeid'],$producttypes)){
		$producttypes[$value['ptypeid']]=$value['ptypename'];
	}
}

return $producttypes;
}

#
#Function for loading the all selected product type tab for browse section
#
function func_load_product_type_for_browse_tabs($orgId){
global $sql_tbl;
$sql="SELECT c.id AS ptypeid, c.label AS ptypename
	  FROM $sql_tbl[org_style_map] a INNER JOIN  $sql_tbl[mk_product_style] b ON a.styleid=b.id
	  INNER JOIN  $sql_tbl[mk_product_type] c ON b.product_type=c.id  	
	  WHERE a.id='".$orgId."' AND a.status=1 and browse='Y'";
	 

$types = func_query($sql);

$producttypes = array();
foreach($types AS $key=>$value){
	if(!array_key_exists($value['ptypeid'],$producttypes)){
		$producttypes[$value['ptypeid']]=$value['ptypename'];
	}
}

return $producttypes;
}


#
#Function for loading the all selected styles
#
function func_load_selected_product_styles($orgId,$productTypeId){
global $sql_tbl;
$sql="SELECT b.id AS styleid, b.name AS stylename, b.label AS stylelabel, c.name AS ptypename, c.label AS ptypelabel,d.image_path AS thumbnail, b.description
	  FROM $sql_tbl[org_style_map] a INNER JOIN  $sql_tbl[mk_product_style] b ON a.styleid=b.id
      INNER JOIN  $sql_tbl[mk_product_type] c ON b.product_type=c.id  INNER JOIN $sql_tbl[mk_style_images] d on b.id=d.styleid	 
	  WHERE a.id='".$orgId."' AND c.id='$productTypeId' AND a.status=1 AND a.browse='Y' and d.type='T' and d.isdefault='Y' order by b.label";

$types = func_query($sql);

$selected_styles = array();
$i =0;
foreach($types AS $key=>$value){

        if(stripos($value['stylelabel'],"1/2")===false){
            $style_name = $value['stylelabel'];
        }else{
            $style_name = str_replace("1/2","half",$value['stylelabel']);
        }

        $name = str_replace(" ","-",$style_name);
		$selected_styles[$i]['id']=$value['styleid'];
        $selected_styles[$i]['name']=$style_name;
        $selected_styles[$i]['replaced_name']=$name;
        $selected_styles[$i]['product_type_name']=$value['ptypelabel'];
		$selected_styles[$i]['thumbnail']=$value['thumbnail'];
		$selected_styles[$i]['description']=$value['description'];
	$i++;
}

return $selected_styles;
}


#
#Function for loading the all selected product type with their style id for browse section
#
function func_load_selected_product_type_with_style_id_for_browse($orgId){
global $sql_tbl;
global $weblog;

$sql="SELECT c.id AS ptypeid, c.name AS ptypename, c.image_t as 'thumbnail', c.description as 'descriptions', c.type as 'type',
            c.label AS ptypelabel
	  FROM $sql_tbl[org_style_map] a
      INNER JOIN  $sql_tbl[mk_product_style] b ON a.styleid=b.id
	  INNER JOIN  $sql_tbl[mk_product_type] c ON b.product_type=c.id
	  WHERE a.id='".$orgId."' AND a.status=1 
	  and browse='Y'";
	  //  if(!empty($identifier) && ($orgId ==17 || $orgId ==18) )
		// $sql = $sql. " and identifier='$identifier' ";

$weblog->info(">>>>>>>>>>>>>>>> PRODUCT QUERY >>>>>> ". $sql);
$types = func_query($sql);

$producttypes = array();
foreach($types AS $key=>$value){
	if(!in_array($value,$producttypes))
	$producttypes[]=$value;
}

return $producttypes;
}

#
#Function for loading the all selected product type with their style id for customize section
#
function func_load_selected_product_type_with_style_id_for_customize($orgId){
global $sql_tbl;
global $weblog;

$sql="SELECT c.id AS ptypeid, c.name AS ptypename, c.image_t as 'thumbnail', c.description as 'descriptions', c.type as 'type',
            c.label AS ptypelabel
	  FROM $sql_tbl[org_style_map] a
      INNER JOIN  $sql_tbl[mk_product_style] b ON a.styleid=b.id
	  INNER JOIN  $sql_tbl[mk_product_type] c ON b.product_type=c.id
	  WHERE a.id='".$orgId."' AND a.status=1 
	  and customize='Y'";
	  //  if(!empty($identifier) && ($orgId ==17 || $orgId ==18) )
		// $sql = $sql. " and identifier='$identifier' ";

$weblog->info(">>>>>>>>>>>>>>>> PRODUCT QUERY >>>>>> ". $sql);
$types = func_query($sql);

$producttypes = array();
foreach($types AS $key=>$value){
	if(!in_array($value,$producttypes))
	$producttypes[]=$value;
}

return $producttypes;
}

#
#Check the configuration values
#
function func_load_configuration_values($orgId){
	global $sql_tbl;
	$sql="SELECT configid, configvalue FROM $sql_tbl[org_config_map] WHERE orgid='".$orgId."'";
	$configresult=func_query($sql);
	$configvalue=array();
	foreach($configresult AS $key=>$value){
	 if($value['configid']==1)
	 	$configvalue['usercreate']=$value['configvalue'];
	 if($value['configid']==2)
	 	$configvalue['userpublish']=$value['configvalue'];
	 if($value['configid']==3)
	 	$configvalue['admincreate']=$value['configvalue'];
	 if($value['configid']==4)
	 	$configvalue['adminpublish']=$value['configvalue'];
	}
	return $configvalue;
}

#
#Load all styles added in catalog
#
function func_load_style_catalog($orgId){
	global $sql_tbl;
	$sql = "SELECT b.id as sid, b.name as stylename, c.id as pid, c.name as productname, a.status, a.price 
		FROM $sql_tbl[org_style_map] a INNER JOIN $sql_tbl[mk_product_style] b ON a.styleid = b.id 
		INNER JOIN $sql_tbl[mk_product_type] c ON c.id = b.product_type WHERE a.id='".$orgId."' order by b.name";
	$types = func_query($sql);
	$stylecatalog = array();
	foreach($types AS $key=>$value){
		if(!in_array($value,$stylecatalog))
		$stylecatalog[]=$value;
	}
	return $stylecatalog;
} 

#
#Load all styles added in catalog
#
function func_load_type_style_group($orgId){
	global $sql_tbl;
	$sql = "SELECT b.id as sid, b.name as stylename, b.label as stylelabel, c.id as pid, c.name as productname, c.label as productlabel, a.status, a.price 
					,d.image_path
		FROM $sql_tbl[org_style_map] a 
		INNER JOIN $sql_tbl[mk_product_style] b ON a.styleid = b.id 
		INNER JOIN $sql_tbl[mk_product_type] c ON c.id = b.product_type 
		INNER JOIN $sql_tbl[mk_style_images] d ON b.id=d.styleid
		WHERE a.id='".$orgId."' and a.status=1 AND d.type='T' ";
#
#Check if ORGID=2 then Style Id identifire id B (This is a Temporary Solution)
#
    if($orgId == 2){
        $sql .= " AND a.identifier='B' ";
    }
        $sql .= " ORDER BY c.id";

	$result = db_query($sql);
	$type_style_array=array();
	while($row=db_fetch_array($result)){
        $sql = "SELECT count(*) FROM $sql_tbl[products] WHERE product_style_id='" . $row['sid'] . "'";
        $num_row = func_query_first_cell($sql);
        if($num_row > 0){
            $productlabel = $row['productlabel'];
            $productlabel = str_replace(" ","-",$productlabel);

            if(!array_key_exists($productlabel,$type_style_array)){
                $i=0;
            }
            $type_style_array[$productlabel][$i] = array("style_name"=>$row['stylename'],
                                                                "style_label"=>$row['stylelabel'],
                                                                "style_id"=>$row['sid'],
                                                                "type_id"=>$row['pid'],
                                                                "style_image"=>$row['image_path']);
            $i++;
        }
	}
	
	return $type_style_array;
} 

#
#Check is user administrator
#
function func_is_user_admin_of_organization($orgId){
	global $sql_tbl;
	global $login;
	$sql="SELECT count(*) AS ctr FROM $sql_tbl[org_accounts] WHERE login='".$login."' AND id='".$orgId."'";
	return func_query_first_cell($sql);
}


function func_load_All_design_for_producttype_and_organization($id,$productstyleid,$orgid,$limit, $offset,$sortparam)
{
    global $sql_tbl;
    $productTable = $sql_tbl["products"];
    $producttype = $sql_tbl["mk_product_type"];
    $imageTable = $sql_tbl["images_T"];
    $styleTable = $sql_tbl["mk_product_style"];
    $org_product_map=$sql_tbl["org_products_map"];
    $org_style_map=$sql_tbl["org_style_map"];

	if(!empty($id)){
		$query ="select $productTable.productid, $productTable.product, $org_style_map.price, $imageTable.filename,
				  $producttype.id, $producttype.name, $productTable.product_style_id, $styleTable.name,
				  $productTable.image_portal_t, $productTable.myntra_rating,mk_xcart_product_customized_area_rel.bgimageprw
				  from $productTable, $imageTable, $producttype, $styleTable,$org_product_map ,mk_xcart_product_customized_area_rel,$org_style_map
				  where $productTable.product_type_id='$id'
				  and $productTable.product_style_id='$productstyleid'
				  and $productTable.productid = $imageTable.id
				  and $productTable.product_type_id =  $producttype.id
				  and $styleTable.id =  $productTable.product_style_id 
				  and $org_product_map.productid=$productTable.productid
				  and $org_style_map.styleid=$productTable.product_style_id
				  and $org_product_map.orgid=$org_style_map.id
				  and $org_product_map.orgid='$orgid'
				  and mk_xcart_product_customized_area_rel.product_id=$productTable.productid";
	}
	else{
		$query = "select $productTable.productid, $productTable.product, $org_style_map.price, $imageTable.filename,
				  $producttype.id, $producttype.name, $productTable.product_style_id, $styleTable.name,
				  $productTable.image_portal_t, $productTable.myntra_rating,mk_xcart_product_customized_area_rel.bgimageprw
				  from $productTable,$imageTable ,$producttype ,$styleTable,$org_product_map,mk_xcart_product_customized_area_rel,$org_style_map
				  where $productTable.productid = $imageTable.id
				and $productTable.product_style_id='$productstyleid'
				  and $productTable.product_type_id = $producttype.id
				  and $styleTable.id = $productTable.product_style_id 
				  and $org_product_map.productid=$productTable.productid
				  and $org_style_map.styleid=$productTable.product_style_id
				  and $org_product_map.orgid=$org_style_map.id
				  and $org_product_map.orgid='$orgid'
				  and mk_xcart_product_customized_area_rel.product_id=$productTable.productid";
	}

	if($sortparam != null)
	{
		if($sortparam == "BESTSELLING")
		{
			$query = $query." order by ".$productTable.".SALES_STATS desc ";
		}
		if($sortparam == "USERRATING")
		{
			$query = $query." order by  ".$productTable.".RATING desc ";
		}
		if($sortparam == "MOSTRECENT")
		{
			$query = $query." order by  ".$productTable.".add_date desc ";
		}
		if($sortparam == "EXPERTRATING")
		{
			$query = $query." order by  ".$productTable.".myntra_rating desc ";
		}
	}
	$query=$query."  limit $offset, $limit";

    $result      =db_query($query);

    $products=array();

    if ($result)
        {
        $i=0;

        while ($row=db_fetch_row($result))
            {
            $products[$i] = $row;
            $i++;
            }
        }

   //Access popup image path

   $arrayKey = array_keys($products);

	$arraysize = count($arrayKey);
	for($i=0;$i<count($arrayKey);$i++){
		$prodarray = $arrayKey[$i];
		$productDetail = $products[$prodarray];
		$namelength = strlen($productDetail[1]);
		$productname = ($namelength >= '12')?substr($productDetail[1], 0, 12)." ...":$productDetail[1];
		$arraysize = count($products[$prodarray]);

		$popupQuery = "SELECT popup_image_path
					FROM $sql_tbl[mk_xcart_product_customized_area_rel] a, $sql_tbl[mk_style_customization_area] b, $sql_tbl[products] c
					WHERE a.product_id = '$productDetail[0]'
					AND a.product_id = c.productid
					AND a.style_id = b.style_id
					AND a.area_id = b.id
					AND b.isprimary = 1
					";
		$popupResult = db_query($popupQuery);
		$popupRow = db_fetch_array($popupResult);
		$popupImage =  $popupRow['popup_image_path'];
		$products[$prodarray][$arraysize] = $popupImage;
        $products[$prodarray]['prodname'] = $productname;

	}  //exit;

    return $products;
    }


#
#Laod selected product types
#
function func_get_all_selected_product_types($producttypes_str){
	global $sql_tbl;
    $productTypeTable=$sql_tbl["mk_product_type"];
    $queryString="SELECT a.id,a.label,a.image_t,a.type,a.price_start FROM $productTypeTable a WHERE a.is_active=1 and type='P' AND a.id IN($producttypes_str)";
    $result=db_query($queryString);
	$productTypeDetails=array();
    if($result){
        $i=0;
        while ($row=db_fetch_row($result)){
            $productTypeDetails[$i] = $row;
            $i++;
        }
   }
    return $productTypeDetails;
}


function func_getTotalCountOfProductsForProductType_ForOrganization($orgid,$id = 0,$productstyleid=0){
	     global $sql_tbl;
	    $productTable=$sql_tbl["products"];
	    $producttype =$sql_tbl["mk_product_type"];
	    $styleTable    = $sql_tbl["mk_product_style"];
		$org_product_map=$sql_tbl["org_products_map"];
        $org_style_map=$sql_tbl["org_style_map"];



		$query= "select  count($productTable.productid) as ctr
				 from    $productTable, $producttype ,$styleTable,$org_product_map, $org_style_map
                 where $productTable.product_type_id = $producttype.id
                 and  $styleTable.id =  $productTable.product_style_id
                 and $org_product_map.productid = $productTable.productid
                 and $org_style_map.styleid = $productTable.product_style_id
                 and $org_product_map.orgid = $org_style_map.id
                 and $org_product_map.orgid = '$orgid'";

        if($id != 0)
		{
			$query .= " and $productTable.product_type_id='$id'" ;
		}
		if($productstyleid != 0)
		{
			$query .= " and $productTable.product_style_id='$productstyleid'" ;
		}

	    $result=db_query($query);

	    if ($result)
	        {
	        $row=db_fetch_row($result);
	        return $row[0];
	        }

	    return 0;
}

#
#Function to load the published product for tabs
#    

function func_load_All_published_design_of_organization_for_tabs($id,$orgid,$limit='', $offset='',$sortparam,$productstyleid=0,$balance_condition=''){
global $sql_tbl;
global $login;
global $weblog;
$productTable = $sql_tbl["products"];
$producttype = $sql_tbl["mk_product_type"];
$imageTable = $sql_tbl["images_T"];
$styleTable = $sql_tbl["mk_product_style"];
$org_product_map=$sql_tbl["org_products_map"];
$org_style_map=$sql_tbl["org_style_map"];
$style_images=$sql_tbl["mk_style_images"];

$sql ="select $productTable.productid, $productTable.product, $org_style_map.price, $imageTable.filename,
				  $producttype.id, $producttype.name, $productTable.product_style_id, $styleTable.name, 
				  SUBSTRING($productTable.image_portal_t,2), $productTable.myntra_rating,
                  mk_xcart_product_customized_area_rel.bgimageprw,$style_images.image_path,
                  $styleTable.label,$producttype.label,$styleTable.price as online_price

      from $productTable, $imageTable, $producttype, $styleTable,
           $org_product_map , mk_xcart_product_customized_area_rel, $org_style_map,$style_images, mk_style_customization_area
           
      where $productTable.productid = $imageTable.id
      and $productTable.product_type_id =  $producttype.id
      and $styleTable.id =  $productTable.product_style_id
      and $productTable.product_style_id =  $style_images.styleid
      and $org_product_map.productid=$productTable.productid
      and $org_style_map.styleid=$productTable.product_style_id
      and $org_product_map.orgid=$org_style_map.id
      and mk_xcart_product_customized_area_rel.product_id=$productTable.productid
      and (mk_xcart_product_customized_area_rel.area_id=mk_style_customization_area.id AND mk_style_customization_area.isprimary = 1)
      and $style_images.type='D'
      and $org_product_map.orgid='$orgid'";

        if($productstyleid != 0)
        {
            $sql .= " and $productTable.product_style_id='$productstyleid'" ;
        }
		if(!empty($id))
		{
			$sql .= " and $productTable.product_type_id='$id' ";
		}
		if(!empty($balance_condition)){
			$sql .= " and $org_style_map.price$balance_condition ";
		}
		

    $sql = $sql . "  order by  $styleTable.id asc "; 
		if(!empty($offset) && !empty($limit)){
			$sql .= " limit $offset, $limit";
		}
    $result=db_query($sql);
    $weblog->info("func_load_All_published_design_of_organization_for_tabs ".$sql);
	$products=array();
    if ($result){
            $i=0;
            while ($row=db_fetch_row($result)){
                $products[$i] = $row;
                $i++;
            }
      }

   //Access popup image path

   $arrayKey = array_keys($products);

	$arraysize = count($arrayKey);
	for($i=0;$i<count($arrayKey);$i++){
		$prodarray = $arrayKey[$i];
		$productDetail = $products[$prodarray];
		$namelength = strlen($productDetail[1]);
		$productname = ($namelength >= '12')?substr($productDetail[1], 0, 12)." ...":$productDetail[1];
		$arraysize = count($products[$prodarray]);

		$popupQuery = "SELECT popup_image_path
					FROM $sql_tbl[mk_xcart_product_customized_area_rel] a, $sql_tbl[mk_style_customization_area] b, $sql_tbl[products] c
					WHERE a.product_id = '$productDetail[0]'
					AND a.product_id = c.productid
					AND a.style_id = b.style_id
					AND a.area_id = b.id
					AND b.isprimary = 1";
					
		$popupResult = db_query($popupQuery);
		$popupRow = db_fetch_array($popupResult);
		$popupImage =  $popupRow['popup_image_path'];
		$products[$prodarray][$arraysize] = $popupImage;
        $products[$prodarray]['prodname'] = $productname;

	}  //exit;

    return $products;

}
     
#
#Function to load the published product
#
function func_load_All_published_design_of_organization($orgid){
global $sql_tbl;
global $login;
$sql ="select $sql_tbl[products].productid, $sql_tbl[products].product, FORMAT($sql_tbl[mk_product_style].price+IFNULL($sql_tbl[mk_product_style].price*$sql_tbl[products].markup/100,0),2),$sql_tbl[images_T].filename,
	  $sql_tbl[mk_product_type].id, $sql_tbl[mk_product_type].name, $sql_tbl[products].product_style_id, $sql_tbl[mk_product_style].name,
	  $sql_tbl[products].image_portal_t, $sql_tbl[products].myntra_rating
	  from $sql_tbl[products], 
	  		$sql_tbl[mk_product_type], 
	  		$sql_tbl[mk_product_style], 
	  		$sql_tbl[org_products_map], 
	  		$sql_tbl[images_T]
	  where $sql_tbl[org_products_map].productid=$sql_tbl[products].productid
	  and $sql_tbl[products].productid = $sql_tbl[images_T].id
	  and $sql_tbl[products].product_type_id = $sql_tbl[mk_product_type].id
	  and $sql_tbl[mk_product_style].id = $sql_tbl[products].product_style_id 
	  and $sql_tbl[org_products_map].productid = $sql_tbl[products].productid
	  and $sql_tbl[org_products_map].orgid='$orgid'
	  and $sql_tbl[products].provider='".$login."'";
	  //and $sql_tbl[products].statusid = 1008";

    $result=db_query($sql);
    $products=array();
    if ($result){
        $i=0;
        while ($row=db_fetch_row($result)){
            $products[$i] = $row;
            $i++;
        }
  }

   //Access popup image path

   $arrayKey = array_keys($products);

	$arraysize = count($arrayKey);
	for($i=0;$i<count($arrayKey);$i++){
		$prodarray = $arrayKey[$i];
		$productDetail = $products[$prodarray];
		$namelength = strlen($productDetail[1]);
		$productname = ($namelength >= '12')?substr($productDetail[1], 0, 12)." ...":$productDetail[1];
		$arraysize = count($products[$prodarray]);

		$popupQuery = "SELECT popup_image_path
					FROM $sql_tbl[mk_xcart_product_customized_area_rel] a, $sql_tbl[mk_style_customization_area] b, $sql_tbl[products] c
					WHERE a.product_id = '$productDetail[0]'
					AND a.product_id = c.productid
					AND a.style_id = b.style_id
					AND a.area_id = b.id
					AND b.isprimary = 1";
					//AND c.statusid = 1008";
		$popupResult = db_query($popupQuery);
		$popupRow = db_fetch_array($popupResult);
		$popupImage =  $popupRow['popup_image_path'];
		$products[$prodarray][$arraysize] = $popupImage;
        $products[$prodarray]['prodname'] = $productname;

	}  //exit;

    return $products;

}

#
#Function to check domain name for invitee users
#

function func_check_domain_name($emailarray, $domainname){
#
#Iterate email array and check email ids those are invalid domain name
#
	$wrongemail = "";
	$validemail = array();
	foreach($emailarray AS $key=>$value)
	{
		$email = trim($value);
		$email_break = explode("@", $email);
		$email_domain = explode(".", $email_break[1]);
		if($email_domain[0]!= strtolower($domainname))
		 $wrongemail = $wrongemail.",".$value;
	}
	$wrongemail = trim($wrongemail, ",");

	return $wrongemail;
	
}

#
#Function to generate the discount coupon
#

function func_discount_on_invite($uname,$discount='',$expiryDate='',$couponType='',$usage='',$peruser='',$minAmt='' , $productid='')
{
    global $sql_tbl,$sqllog;
    $couponcode = "MYNPI".get_rand_id(5);
    $expirydate = (!empty($expiryDate)) ? $expiryDate : time() + 14*24*60*60;
    $discount   = (!empty($discount)) ? $discount : 50 ;
    $couponType = (!empty($couponType)) ? $couponType : "absolute" ;
    $usage      = (!empty($usage)) ? $usage : "1" ;
    $peruser    = (!empty($peruser)) ? 'Y' : "N" ;
    $productId  = (empty($productid)) ? '0' :  $productid;

    $minimumAmt = (empty($minAmt)) ? '0' :  $minAmt;
    $query = "INSERT INTO $sql_tbl[discount_coupons] (coupon, discount, groupid, coupon_type, minimum, times, per_user, expire, status, provider, productid, categoryid, recursive, apply_category_once, apply_product_once,freeship,styleid) VALUES ('$couponcode', '".$discount."', NULL, '".$couponType."', '".$minimumAmt."', '".$usage."', '".$peruser."', '$expirydate', 'A', 'myntraProvider', '".$productId."', '0', 'N', 'N', 'N', 'N', '0')";
$sqllog->debug($query);
    mysql_query($query);
    return $couponcode;
}

#
#Function to parse ini files
#

function func_parse_ini($file){
	
	$parsed_array = parse_ini_file($file);
	return $parsed_array;
	
}

/**
 * Load style customization image
 *
 */
function load_all_organization_style_groupinfo($productTypeId, $orgId){
	global $weblog,$sqllog,$sql_tbl;
	$groupsarray = array();
	if(!empty($groupids)) {
		$groupids = str_replace("_",",",$groupids);
	}
	$weblog->info("Inside function with parmam ".$groupids." : ".__FUNCTION__." @ line ".__LINE__." in file ".__FILE__);
	$sqlQuery  = "SELECT ps.id as style_id, ps.name as stylename, ps.label as stylelabel, ps.id, ps.price as online_price, osm.price, ps.description, sca.bkgroundimage
                  FROM mk_product_style as ps
                  INNER JOIN mk_org_style_map as osm ON osm.styleid = ps.id
                  INNER JOIN mk_style_customization_area as sca ON sca.style_id = ps.id
                  WHERE ps.product_type='$productTypeId' AND ps.is_active = '1' AND ps.styletype = 'P' AND osm.id='$orgId' AND osm.status='1' AND osm.customize='Y' AND sca.isprimary='1' 
                  ORDER BY ps.label ASC";
	$sqllog->debug("SQL Query : ".$sqlQuery." "." @ line ".__LINE__." in file ".__FILE__);
	return $resultSet = func_query($sqlQuery);
}


function func_load_organization_product_style_details($productStyleId,$orgid)
{
    global $sql_tbl;
    $productStyleTable=$sql_tbl["mk_product_style"];
    $org_style_map=$sql_tbl["org_style_map"];

    $queryString      ="select      a.id,
                                    a.name,
                                    a.label,
                                    a.description,
                                    a.image_l,
                                    b.price,
                                    a.image_t,
                                    a.image_i,
                                    a.l_width,
                                    a.l_height,
                                    a.description
                                 from
                                    $productStyleTable a INNER JOIN $org_style_map b ON a.id=b.styleid
                                 where
                                    a.id = $productStyleId and
                                    a.is_active=1 and
                                    a.iventory_count > 0
									and b.id='$orgid'";

    $result           =db_query($queryString);

    $productStyleDetails=array();

    if ($result)
        {
        $i=0;

        while ($row=db_fetch_row($result))
            {
            $productStyleDetails[$i] = $row;
            $i++;
            }
        }

    //    print_r($productStyleDetails);
    return $productStyleDetails;
}


function func_load_selected_product_type($orgId,$identifier=''){
global $sql_tbl;

$sql="SELECT c.id AS ptypeid, c.label AS ptypename
	  FROM $sql_tbl[org_style_map] a INNER JOIN  $sql_tbl[mk_product_style] b ON a.styleid=b.id
	  INNER JOIN  $sql_tbl[mk_product_type] c ON b.product_type=c.id  	
	  WHERE a.id='".$orgId."' AND a.status=1 ";

if(!empty($identifier) && ($orgId ==17 || $orgId==18) )
		  $sql = $sql. " and identifier='$identifier' ";
	 

$types = func_query($sql);

$producttypes = array();
foreach($types AS $key=>$value){
	if(!array_key_exists($value['ptypeid'],$producttypes)){
		$producttypes[$value['ptypeid']]=$value['ptypename'];
	}
}

return $producttypes;
}

#
#Send mail to myntra support regarding customer query
#
/*
function send_customer_enquiry($name, $cnumber, $contact_email, $timelines, $enquiry_details){
    
    $headers = "Content-Type: text/html; charset=ISO-8859-1 " . "\n";
	$headers .= "From: ". $name . "<" . $contact_email . ">" . "\n";
    $email = "sales@myntra.com";
    //$email = "nikhil.gupta@myntra.com";
    $subject = "New lead for bulk order";

    $content = "Dear Sales,"."<br/><br/>";
    $content .= "<p>We have received a new lead for bulk order. Please see below for details and contact immediately. </p>"."<br/><br/>";
    $content .= "<b>Customer Name : </b>". $name . "<br/>";
    $content .= "<b>Contact Number : </b>". $cnumber . "<br/>";
    $content .= "<b>Contact E-mail : </b>". $contact_email . "<br/>";
    $content .= "<b>Timeline : </b>". $timelines . "<br/>";
    $content .= "<b>Customer Enquiry : </b>". $enquiry_details . "<br/>";

    $flag = @mail($email, $subject, $content,$headers);

    return $flag;
}
*/
/* to get brandstore styles based on stles csv
 * @parameter::styles csv,orgid
 * @return::multilevel array of style details
 */
function card_group_customization_styles($stylescsv,$orgId){
	global $weblog,$sqllog,$sql_tbl;
	$weblog->info("Inside function with parmam (".$stylescsv.") and ".$orgId." : ".__FUNCTION__." @ line ".__LINE__." in file ".__FILE__);
	$sqlQuery  = "SELECT ps.id as style_id, ps.name as stylename, ps.label as stylelabel, ps.id, osm.price, ps.description, si.image_path as bkgroundimage
                  FROM mk_product_style as ps
                  INNER JOIN mk_org_style_map as osm ON osm.styleid = ps.id
                  INNER JOIN mk_style_images as si ON si.styleid = ps.id
                  WHERE ps.id in ($stylescsv) AND ps.is_active = '1' AND ps.styletype = 'P' AND osm.id='$orgId' AND osm.status='1' AND osm.customize='Y' AND si.type='T' AND si.isdefault='Y' 
                  ORDER BY ps.label ASC";
	
	$sqllog->debug("SQL Query : ".$sqlQuery." "." @ line ".__LINE__." in file ".__FILE__);
	return $resultSet = func_query($sqlQuery);
}

/* returns types based on style price <= balance
 * @parameter::orgid,balance reward points
 * @return::multilevel array of type details
 */
function func_redeemable_types($orgId,$balance){
	global $sql_tbl;
	global $weblog,$sqllog;
	$weblog->info("Inside function func_redeemable_types($orgId,$balance) @ line ".__LINE__." in file ".__FILE__);

	$sql="SELECT 
			distinct(c.id) AS ptypeid,
	 		c.name AS ptypename,
	 		c.image_t as 'thumbnail', 
	 		c.description as 'descriptions', 
	 		c.type as 'type',
	        c.label AS ptypelabel,
	        min(a.price) as startsfrom        
		  FROM $sql_tbl[org_style_map] a
	      INNER JOIN  $sql_tbl[mk_product_style] b ON a.styleid=b.id
		  INNER JOIN  $sql_tbl[mk_product_type] c ON b.product_type=c.id
		  WHERE 
		  	a.id='".$orgId."' 
		  AND
		  	a.status=1
		  AND
		  	a.price <= $balance
		  group by c.id";  	  
	
	$types = func_query($sql);
	$sqllog->debug("SQL Query : ".$sql." "." @ line ".__LINE__." in file ".__FILE__);
	return $types;
}

/* returns types based on style price > balance
 * @parameter::orgid,balance reward points
 * @return::multilevel array of type details
 */
function func_nonredeemable_types($orgId,$balance){
	global $sql_tbl;
	global $weblog,$sqllog;
	$weblog->info("Inside function func_nonredeemable_types($orgId,$balance) @ line ".__LINE__." in file ".__FILE__);
	
	$sql="SELECT 
			distinct(c.id) AS ptypeid,
	 		c.name AS ptypename,
	 		c.image_t as 'thumbnail', 
	 		c.description as 'descriptions', 
	 		c.type as 'type',
	        c.label AS ptypelabel,
	        min(a.price) as startsfrom        
		  FROM $sql_tbl[org_style_map] a
	      INNER JOIN  $sql_tbl[mk_product_style] b ON a.styleid=b.id
		  INNER JOIN  $sql_tbl[mk_product_type] c ON b.product_type=c.id
		  WHERE 
		  	a.id='".$orgId."' 
		  AND
		  	a.status=1
		  AND
		  	a.price > $balance
		  group by c.id";
	
	$types = func_query($sql);
	$sqllog->debug("SQL Query : ".$sql." "." @ line ".__LINE__." in file ".__FILE__);
	return $types;
}


/* returns styles based on style price <= balance
 * @parameter::orgid,typeid,balance reward points
 * @return::multilevel array of style details
 */
function func_redeemable_styles($orgId,$type,$balance){
	global $sql_tbl;
	global $weblog,$sqllog;
	$weblog->info("Inside function func_redeemable_styles($orgId,$type,$balance) @ line ".__LINE__." in file ".__FILE__);
	$sql="SELECT 
			b.id, 
			b.name AS stylename, 
			replace(replace(b.label,'1/2','half'),' ','-') AS name, 
			c.name AS ptypename, 
			c.label AS product_type_name,
			d.image_path AS thumbnail,			 
			b.description,
			a.price,
			a.customize,
			a.browse
	  	FROM $sql_tbl[org_style_map] a 
	  	INNER JOIN  $sql_tbl[mk_product_style] b ON a.styleid=b.id
      	INNER JOIN  $sql_tbl[mk_product_type] c ON b.product_type=c.id  
      	INNER JOIN $sql_tbl[mk_style_images] d on b.id=d.styleid	 
	  	WHERE
	  		a.id='".$orgId."' 
	  	AND c.id='$type' 
	  	AND a.status=1 
	  	AND a.price <= $balance
	  	AND d.type='T' 
	  	AND d.isdefault='Y' 
	  	AND b.is_active='1' 
	  	AND b.styletype='P'
	  	order by b.label";

	$styles = func_query($sql);
	$sqllog->debug("SQL Query : ".$sql." "." @ line ".__LINE__." in file ".__FILE__);
	return $styles;
}

/* returns styles based on style price > balance
 * @parameter::orgid,typeid,balance reward points
 * @return::multilevel array of style details
 */
function func_nonredeemable_styles($orgId,$type,$balance){
	global $sql_tbl;
	global $weblog,$sqllog;
	$weblog->info("Inside function func_nonredeemable_styles($orgId,$type,$balance) @ line ".__LINE__." in file ".__FILE__);
	
	$sql="SELECT 
			b.id, 
			b.name AS stylename, 
			replace(replace(b.label,'1/2','half'),' ','-') AS name, 
			c.name AS ptypename, 
			c.label AS product_type_name,
			d.image_path AS thumbnail, 
			b.description,
			a.price,
			a.customize,
			a.browse
	  	FROM $sql_tbl[org_style_map] a 
	  	INNER JOIN  $sql_tbl[mk_product_style] b ON a.styleid=b.id
      	INNER JOIN  $sql_tbl[mk_product_type] c ON b.product_type=c.id  
      	INNER JOIN $sql_tbl[mk_style_images] d on b.id=d.styleid	 
	  	WHERE 
	  		a.id='".$orgId."' 
	  	AND c.id='$type' 
	  	AND a.status=1 
	  	AND a.price > $balance
	  	AND d.type='T' 
	  	AND d.isdefault='Y'
	  	AND b.is_active='1' 
	  	AND b.styletype='P'
	  	order by b.label";
	$styles = func_query($sql);
	$sqllog->debug("SQL Query : ".$sql." "." @ line ".__LINE__." in file ".__FILE__);
	return $styles;
}

/*
 * checks for promo portal(icici,icici-imint) in the list
 * of promo portals
 * @i/p:int (portal id)
 * @return:bool
 */
function isPromoPortal($orgId){
	/*
	 * append new RR portal to this array 
	 */
	$PromoPortals = array(
				24,
				26,//add here
				);
				
	if(in_array($orgId,$PromoPortals))
		return true;
	else 
		return false;

}


/* checks for brandhsops which
 * shares common css and js in its entire order pipeline
 * @i/p:int (portal id)
 * @return:bool
 */
function isCommonSkin($orgId){
	/*
	 * append new RR portal to this array
	 */
	$commonSkinPortals = array(	37,38,39,40,41	//add here
                        );

	if(in_array($orgId,$commonSkinPortals))
		return true;
	else
		return false;

}

/* to get all issuerscodes(5013,5014 etc..) for a 
 * particular brandstore
 * @i/p:orgid
 * @o/p:array[issuercodes]
 */
function get_issuercodes($orgId){
	global $weblog,$sqllog;
	$weblog->info("Inside function get_isuuercodes($orgId) @ line ".__LINE__." in file ".__FILE__);
		
	$sql = "select issuercode from mk_org_accounts where id = '{$orgId}'";
	$issuercode_string = func_query_first_cell($sql);
	$sqllog->debug("SQL Query : ".$sql." "." @ line ".__LINE__." in file ".__FILE__);
	$issuercodearray = explode(',',trim($issuercode_string,','));
	return $issuercodearray; 
}

/* get all the issuer category styles
 * for customization 
 * @i/p:issuercode,categoryid
 * @o/p:string(styles csv)
 */
function get_issuer_category_styles_csv($issuercode,$categoryid){
	return null;
}

/* get all the issuer category styles
 * for customization 
 * @i/p:issuercode,categoryid
 * @o/p:string(products csv)
 */
function get_issuer_category_products_csv($issuercode,$categoryid){
	return null;
}

/* checks promo code expiry date,
 * no.of times used
 * @i/p:promo code details
 * @o/p:string message and null on valid
 */
function validate_promo_code($couponCodeDetails){
	global $weblog;
	$weblog->info("Inside function validate_promo_code($couponCodeDetails) @ line ".__LINE__." in file ".__FILE__);
	if (empty($couponCodeDetails)){
        $errorMessageCoupon = "No active coupon found matching your coupon code.";
        return $errorMessageCoupon;
    }
    //check for the coupon validity and usage
    $couponExpirationDate = $couponCodeDetails[8];
    if ($couponExpirationDate < time()){
		//check time with Unix Epoch
        $errorMessageCoupon = "You can not avail the gift since is expired";
        return $errorMessageCoupon;
    }

    //check for the coupon startdate
    $couponStartDate = $couponCodeDetails[21];
    if ($couponStartDate > time()){
        $after = date('jS M, Y',$couponStartDate);
        $before = date('jS M, Y',$couponExpirationDate);
        $errorMessageCoupon = "You can not avail the gift now. You can avail it between $after and $before.";
        return $errorMessageCoupon;
    }
    // for peruser check
    if ($couponCodeDetails[7] >= $couponCodeDetails[5]){
	   	$errorMessageCoupon = "You have already availed the gift.";
	    return $errorMessageCoupon;
	}    	
    return "";
}

/*
 * 
 */
function card_group_products($productscsv,$orgId){
	global $sql_tbl,$login,$weblog,$sqllog;
	$weblog->info("Inside function card_group_products($productscsv,$orgId) @ line ".__LINE__." in file ".__FILE__);
	
	$productTable = $sql_tbl["products"];
	$producttype = $sql_tbl["mk_product_type"];
	$imageTable = $sql_tbl["images_T"];
	$styleTable = $sql_tbl["mk_product_style"];
	$org_product_map=$sql_tbl["org_products_map"];
	$org_style_map=$sql_tbl["org_style_map"];
	$style_images=$sql_tbl["mk_style_images"];

	$sql ="select 
			$productTable.productid, 
			$productTable.product, 
			$org_style_map.price, 
			$imageTable.filename,
			$producttype.id, 
			$producttype.name, 
			$productTable.product_style_id, 
			$styleTable.name,
			SUBSTRING($productTable.image_portal_t,2), 
			$productTable.myntra_rating,
	        SUBSTRING(mk_xcart_product_customized_area_rel.bgimageprw,2),
			$style_images.image_path,
	        $styleTable.label,
	        $producttype.label
		from $productTable, $imageTable, $producttype, $styleTable,
	         $org_product_map , mk_xcart_product_customized_area_rel, $org_style_map,$style_images, mk_style_customization_area
	    where 
	        $productTable.productid = $imageTable.id
	    and $productTable.product_type_id =  $producttype.id
	    and $styleTable.id =  $productTable.product_style_id
	    and $productTable.product_style_id =  $style_images.styleid
	    and $org_product_map.productid=$productTable.productid
	    and $org_style_map.styleid=$productTable.product_style_id
	    and $org_product_map.orgid=$org_style_map.id
	    and mk_xcart_product_customized_area_rel.product_id=$productTable.productid
	    and (mk_xcart_product_customized_area_rel.area_id=mk_style_customization_area.id AND mk_style_customization_area.isprimary = 1)
	    and $style_images.type='D'
	    and $productTable.productid in ($productscsv)
	    and $org_product_map.orgid='$orgId'";
		    
		$sql = $sql ."  order by  $styleTable.id desc "; 
		
	    $result=db_query($sql);
	    $sqllog->debug("SQL Query : ".$sql." "." @ line ".__LINE__." in file ".__FILE__);
	
	$products=array();
    if ($result){
    	$i=0;
        while ($row=db_fetch_row($result)){
        	$products[$i] = $row;
            $i++;
        }
    }

   //Access popup image path
	$arrayKey = array_keys($products);

	$arraysize = count($arrayKey);
	for($i=0;$i<count($arrayKey);$i++){
		$prodarray = $arrayKey[$i];
		$productDetail = $products[$prodarray];
		$namelength = strlen($productDetail[1]);
		$productname = ($namelength >= '12')?substr($productDetail[1], 0, 12)." ...":$productDetail[1];
		$arraysize = count($products[$prodarray]);

		$popupQuery = "SELECT 
						popup_image_path
					FROM 
						$sql_tbl[mk_xcart_product_customized_area_rel] a, $sql_tbl[mk_style_customization_area] b, $sql_tbl[products] c
					WHERE 
						a.product_id = '$productDetail[0]'
					AND a.product_id = c.productid
					AND a.style_id = b.style_id
					AND a.area_id = b.id
					AND b.isprimary = 1";
					
		$popupResult = db_query($popupQuery);
		$popupRow = db_fetch_array($popupResult);
		$popupImage =  $popupRow['popup_image_path'];
		$products[$prodarray][$arraysize] = $popupImage;
        $products[$prodarray]['prodname'] = $productname;
	}
    return $products;
}


/* returns types to be shown in brandshop menu
 * @parameter::orgid
 * @return::multilevel array of type details
 */
function orgMenuTypes($orgId){
	global $sql_tbl;
	global $weblog,$sqllog;
	$weblog->info("Inside function orgMenuTypes($orgId) @ line ".__LINE__." in file ".__FILE__);

	$sql="SELECT
			distinct(c.id) AS typeid,
	 		c.name AS typename,
	        c.label AS typelabel	        
		  FROM $sql_tbl[org_style_map] a
	      INNER JOIN  $sql_tbl[mk_product_style] b ON a.styleid=b.id
		  INNER JOIN  $sql_tbl[mk_product_type] c ON b.product_type=c.id
		  WHERE
		  	a.id='".$orgId."'
		  AND
		  	a.status=1		  
		  group by c.id";

	$types = func_query($sql);
	$sqllog->debug("SQL Query : ".$sql." "." @ line ".__LINE__." in file ".__FILE__);

    if(!empty($types)){
        foreach($types as $idx=>$type){
            $sqlQuery  = "SELECT ps.id as style_id, ps.name as stylename, ps.label as stylelabel
                      FROM mk_product_style as ps
                      INNER JOIN mk_org_style_map as osm ON osm.styleid = ps.id
                      WHERE ps.product_type = $type[typeid] AND ps.is_active = '1' AND ps.styletype = 'P' AND osm.id='$orgId' AND osm.status='1' AND osm.browse='Y' 
                      ORDER BY ps.label ASC";
            $stylesResult = db_query($sqlQuery);
            while($style = db_fetch_array($stylesResult)){
                $types[$idx]['styles'][] = array("styleid"=>$style['style_id'],"stylename"=>$style['name'],"stylelabel"=>$style['stylelabel']);
                 
            }
        }


    }
	return $types;
}

/* returns groups,types to be shown in brandshop menu
 * @parameter::orgid
 * @return::multilevel array of group and type details
 */
function orgMenuTags($orgId){
	global $sql_tbl;
	global $weblog,$sqllog;
	$weblog->info("Inside function orgMenuTags($orgId) @ line ".__LINE__." in file ".__FILE__);

    $sql = "SELECT
                distinct(g.id) AS groupid,
                g.name AS groupname,
                g.label AS grouplabel
            FROM $sql_tbl[org_style_map] sm
            INNER JOIN $sql_tbl[mk_product_style] s ON sm.styleid=s.id
            INNER JOIN  $sql_tbl[mk_product_type] t ON s.product_type=t.id
            INNER JOIN  mk_product_group g ON t.product_type_groupid=g.id
            WHERE sm.id='".$orgId."' AND sm.status=1 And t.is_active=1 group by g.id";	

	$groups = func_query($sql);
	$sqllog->debug("SQL Query : ".$sql." "." @ line ".__LINE__." in file ".__FILE__);

    if(!empty($groups)){
        foreach($groups as $idx=>$group){
            $sqlQuery="SELECT
                            distinct(t.id) AS typeid,
                            t.name AS typename,
                            t.label AS typelabel
                          FROM $sql_tbl[org_style_map] sm
                          INNER JOIN  $sql_tbl[mk_product_style] s ON sm.styleid=s.id
                          INNER JOIN  $sql_tbl[mk_product_type] t ON s.product_type=t.id
                          INNER JOIN  mk_product_group g ON t.product_type_groupid=g.id
                          WHERE sm.id='".$orgId."' AND sm.status=1 AND t.is_active=1 And g.id='".$group['groupid']."' group by t.id";

            $typeResult = db_query($sqlQuery);
            while($type = db_fetch_array($typeResult)){
                $groups[$idx]['types'][] = array("typeid"=>$type['typeid'],"typename"=>$type['typename'],"typelabel"=>$type['typelabel']);

            }
        }


    }
	return $groups;
}


/* returns all published products for the brandshop
 * @parameter::orgid
 * @return::multilevel array of type details
 */
function loadAllDesignForOrganization($typeid,$styleid,$productid,$orgid,$limit='', $offset=''){
    global $sql_tbl,$weblog,$sqllog;
    $weblog->info("Inside function loadAllDesignForOrganization($typeid,$styleid,$orgid,$limit,$offset) @ line ".__LINE__." in file ".__FILE__);

    $productTable = $sql_tbl["products"];
    $producttype = $sql_tbl["mk_product_type"];
    $imageTable = $sql_tbl["images_T"];
    $styleTable = $sql_tbl["mk_product_style"];
    $org_product_map=$sql_tbl["org_products_map"];
    $org_style_map=$sql_tbl["org_style_map"];
    $style_images=$sql_tbl["mk_style_images"];
    $productImageDetail = 'mk_xcart_product_customized_area_rel';

    $sql = "select $productTable.productid, $productTable.product,$productTable.product_style_id as styleid,$styleTable.label as stylename,
                    $producttype.id as typeid, $producttype.label as typename,$productTable.myntra_rating as userrating,
                    $productTable.descr,$productImageDetail.bgimageprw as search_image,$productImageDetail.popup_image_path as search_zoom_image,
                    $styleTable.price,$org_style_map.price as discounted_price,
                    $org_product_map.bestseller,$org_product_map.whatsnew,$org_product_map.closeout

            from $productTable, $imageTable, $producttype, $styleTable,
                $org_product_map , mk_xcart_product_customized_area_rel, $org_style_map,$style_images, mk_style_customization_area

            where $productTable.productid = $imageTable.id
            and $productTable.product_type_id =  $producttype.id
            and $styleTable.id =  $productTable.product_style_id
            and $productTable.product_style_id =  $style_images.styleid
            and $org_product_map.productid=$productTable.productid
            and $org_style_map.styleid=$productTable.product_style_id
            and $org_product_map.orgid=$org_style_map.id
            and $styleTable.is_active='1'
            and $styleTable.styletype='P'
            and $org_style_map.status='1'
            and $org_style_map.browse='Y'
            and mk_xcart_product_customized_area_rel.product_id=$productTable.productid
            and (mk_xcart_product_customized_area_rel.area_id=mk_style_customization_area.id AND mk_style_customization_area.isprimary = 1)
            and $style_images.type='D'
            and $org_product_map.orgid='$orgid'";    

    if(!empty($productid)){
        $sql .= " and $productTable.productid='$productid'" ;
    }

    if(!empty($styleid)){
        $sql .= " and $productTable.product_style_id='$styleid'" ;
    }
    
    if(!empty($typeid)){
        $sql .= " and $productTable.product_type_id='$typeid' ";
    }

    $sql = $sql . "  order by  $styleTable.id desc ";
    if(!empty($offset) && !empty($limit)){
        $sql .= " limit $offset, $limit";
    }

    $result = func_query($sql);
    $sqllog->debug("SQL Query : ".$sql." "." @ line ".__LINE__." in file ".__FILE__);
    return $result;    
}

/* returns all published products for the brandshop as required to scrollable widget for home
 * @parameter::array[products],styleid=0
 * @return::multilevel array of product data
 */
function getOrgHomeList($deignData,$styleId=0){
    global $weblog;
    $weblog->info("Inside function getOrgHomeList($deignData,$styleId) @ line ".__LINE__." in file ".__FILE__);
    
    //get sizes for the design from style
    if(!empty($styleId)){        
        $sizesResult = db_query("select value from mk_product_options where is_active=1 and style='".$styleId."'");
        while($row = db_fetch_array($sizesResult)){
            $sizes[] = $row['value'];
        }
        $sizes = implode($sizes,',');
    }

    //$all = array();
    $bestSellers = array();
    $whatsNew = array();
    $closeOut = array();

    
    foreach($deignData as $val){        
        if(!empty($sizes))
            $val['sizes'] = $sizes;
        else {
            $sizesResult = db_query("select value from mk_product_options where is_active=1 and style='".$val['styleid']."'");
            while($row = db_fetch_array($sizesResult)){
                $designSizes[] = $row['value'];
            }
            $val['sizes'] = implode($designSizes,',');
            unset($designSizes);
        }

        //$all[] = $val;

        if($val['bestseller'] == 'Y'){
            $bestSellers[] = $val;
        }

        if($val['whatsnew'] == 'Y'){
            $whatsNew[] = $val;
        }

        if($val['closeout'] == 'Y'){
            $closeOut[] = $val;
        }        
    }

    /*$data[] = array(
                'data' => $all,
                'name' => 'All'
            );*/
    $data[] = array(
                'data' => $bestSellers,
                'name' => 'Best Sellers'
            );
    $data[] = array(
                'data' => $whatsNew,
                'name' => 'Whats New'
            );
    $data[] = array(
                'data' => $closeOut,
                'name' => 'Close Out'                
            );
    return $data;    
}


/* returns all published products for the brandshop as list
 * @parameter::array[products],styleid=0
 * @return::multilevel array of product data
 */
function getOrgList($deignData,$styleId=0){
    global $weblog;
    $weblog->info("Inside function getOrgList($deignData,$styleId) @ line ".__LINE__." in file ".__FILE__);

    //get sizes for the design from style
    if(!empty($styleId)){
        $sizesResult = db_query("select value from mk_product_options where is_active=1 and style='".$styleId."'");
        while($row = db_fetch_array($sizesResult)){
            $sizes[] = $row['value'];
        }
        $sizes = implode($sizes,',');
    }

    $all = array();
    foreach($deignData as $val){
        if(!empty($sizes))
            $val['sizes'] = $sizes;
        else {
            $sizesResult = db_query("select value from mk_product_options where is_active=1 and style='".$val['styleid']."'");
            while($row = db_fetch_array($sizesResult)){
                $designSizes[] = $row['value'];
            }
            $val['sizes'] = implode($designSizes,',');
            unset($designSizes);
        }

        $all[] = $val;
    }
    return $all;
}

/* get all bestsellers style
 * @parameter::orgid
 * @return::multilevel array of style details
 */
function orgPopularList($orgId){
	global $sql_tbl;
	global $weblog,$sqllog;
	$weblog->info("Inside function orgPopularList($orgId) @ line ".__LINE__." in file ".__FILE__);

	$sql= "SELECT ps.id as styleid, ps.name as stylename, ps.label as stylelabel
              FROM mk_product_style as ps
              INNER JOIN mk_org_style_map as osm ON osm.styleid = ps.id
              WHERE ps.is_active = '1' AND ps.styletype = 'P' AND osm.id='$orgId' AND osm.status='1' AND osm.browse='Y' AND osm.bestseller='Y'
              ORDER BY ps.label ASC";

	$styles = func_query($sql);
	$sqllog->debug("SQL Query : ".$sql." "." @ line ".__LINE__." in file ".__FILE__);

	return $styles;
}
?>