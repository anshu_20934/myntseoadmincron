<?php
/*****************************************************************************\
+-----------------------------------------------------------------------------+
| X-Cart                                                                      |
| Copyright (c) 2001-2006 Ruslan R. Fazliev <rrf@rrf.ru>                      |
| All rights reserved.                                                        |
+-----------------------------------------------------------------------------+
| PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
| FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
| AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
|                                                                             |
| THIS  AGREEMENT  EXPRESSES  THE  TERMS  AND CONDITIONS ON WHICH YOU MAY USE |
| THIS SOFTWARE   PROGRAM   AND  ASSOCIATED  DOCUMENTATION   THAT  RUSLAN  R. |
| FAZLIEV (hereinafter  referred to as "THE AUTHOR") IS FURNISHING  OR MAKING |
| AVAILABLE TO YOU WITH  THIS  AGREEMENT  (COLLECTIVELY,  THE  "SOFTWARE").   |
| PLEASE   REVIEW   THE  TERMS  AND   CONDITIONS  OF  THIS  LICENSE AGREEMENT |
| CAREFULLY   BEFORE   INSTALLING   OR  USING  THE  SOFTWARE.  BY INSTALLING, |
| COPYING   OR   OTHERWISE   USING   THE   SOFTWARE,  YOU  AND  YOUR  COMPANY |
| (COLLECTIVELY,  "YOU")  ARE  ACCEPTING  AND AGREEING  TO  THE TERMS OF THIS |
| LICENSE   AGREEMENT.   IF  YOU    ARE  NOT  WILLING   TO  BE  BOUND BY THIS |
| AGREEMENT, DO  NOT INSTALL OR USE THE SOFTWARE.  VARIOUS   COPYRIGHTS   AND |
| OTHER   INTELLECTUAL   PROPERTY   RIGHTS    PROTECT   THE   SOFTWARE.  THIS |
| AGREEMENT IS A LICENSE AGREEMENT THAT GIVES  YOU  LIMITED  RIGHTS   TO  USE |
| THE  SOFTWARE   AND  NOT  AN  AGREEMENT  FOR SALE OR FOR  TRANSFER OF TITLE.|
| THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY GRANTED BY THIS AGREEMENT.      |
|                                                                             |
| The Initial Developer of the Original Code is Ruslan R. Fazliev             |
| Portions created by Ruslan R. Fazliev are Copyright (C) 2001-2006           |
| Ruslan R. Fazliev. All Rights Reserved.                                     |
+-----------------------------------------------------------------------------+
\*****************************************************************************/

#
# $Id: categories.php,v 1.31 2006/02/14 14:45:23 max Exp $
#

require "./auth.php";

function func_getAffiliatesInfo($mode,$affiliateid='',$affiliateSearchText='',$affstatus)
{
	   global  $sql_tbl;
	     #
		 # search the designers
		 #
		 $affiliate  = $sql_tbl['mk_affiliates'];
		 $affiliateStatics =  $sql_tbl['mk_affiliate_statistics'];
		 $orderTbl = $sql_tbl['orders'];
		 $sql_tbl[mk_designer_payment_commission] =  "mk_designer_payment_commission";
       
		 $Sql = "SELECT  $affiliate.*,
		       
		 IF(dtAffStats.commEarned IS NULL ,0, dtAffStats.commEarned) as commEarned ,
		 IF(dtAffPay.paidComm IS NULL ,0 , dtAffPay.paidComm) as paidComm 
		 FROM $affiliate LEFT JOIN ";
		 
 
		  $Sql .= " (
		  SELECT  SUM(IF($affiliateStatics.commission IS NULL,0,$affiliateStatics.commission)) as 
		  commEarned ,
		  $affiliateStatics.affiliate_id as affiliate
		  FROM $affiliateStatics INNER JOIN $orderTbl ON 
		  $orderTbl.orderid = $affiliateStatics.order_id WHERE $orderTbl.status = 'C' 
		  GROUP BY $affiliateStatics.affiliate_id  ) as dtAffStats 
		  ON $affiliate.affiliate_id = dtAffStats.affiliate  ";
		  
		  $Sql .= "  LEFT JOIN (
		  SELECT  SUM(IFNULL(payment_amt,0)) as paidComm,
		  recipient_id as affiliateid
		  FROM $sql_tbl[mk_designer_payment_commission] 
		  WHERE recipient_type = 'Affiliate' 
		  GROUP BY $sql_tbl[mk_designer_payment_commission].recipient_id ) as dtAffPay 
		  ON $affiliate.affiliate_id = dtAffPay.affiliateid  ";
		  
		 if($mode == 'search')
		 {

	      	 if(!empty($affiliateSearchText) ||  !empty($affstatus)){
				 $Sql .= " WHERE ";
	      	 	 if(!empty($affiliateSearchText))
	      	 	   $Sql .= " ($affiliate.company_name LIKE '$affiliateSearchText%'
					  || $affiliate.contact_name LIKE '$affiliateSearchText%' ) AND ";
	      	 	 if(!empty($affstatus))
	      	 	    $Sql .= " $affiliate.active = '$affstatus' ";     

			 }
			 

             $Sql .="  GROUP BY $affiliate.affiliate_id  ORDER BY $affiliate.company_name";
             
          }
	     else if($mode == 'update'||$mode == 'show'||$mode == 'payment'||$mode == 'save')
	     {
		     	 $Sql .=" WHERE $affiliate.affiliate_id = '".$affiliateid."'
				        GROUP BY $affiliate.affiliate_id  ";
		 }
		 else 
		 {
		 	 $Sql .="  GROUP BY $affiliate.affiliate_id  ORDER BY $affiliate.company_name";
		 }
		$affiliates = func_query ($Sql);
		return $affiliates;
}

function getAffiliateCommDetail($status,$affiliateid )
{
	  global  $sql_tbl;
	  $orderTbl = $sql_tbl[orders];
	  $affiliateStatics =  $sql_tbl['mk_affiliate_statistics'];
	   if($status == 'All')
		{
              $SQL = "SELECT  $affiliateStatics.id,$affiliateStatics.commission,
			  $affiliateStatics.affiliate_id,$affiliateStatics.content_from,
			  $affiliateStatics.order_id ,$affiliateStatics.status
			  FROM $affiliateStatics INNER JOIN $orderTbl ON 
			  $orderTbl.orderid = $affiliateStatics.order_id WHERE $orderTbl.status = 'C' 
			  AND affiliate_id = '".$affiliateid."'  ";  
			
			    /*$SQL = "SELECT  * FROM $affiliateStatics
				 WHERE affiliate_id = '".$affiliateid."'  ";*/

		}
		else
		{
			  $SQL = "SELECT   $affiliateStatics.id,$affiliateStatics.commission,
			  $affiliateStatics.affiliate_id,$affiliateStatics.content_from,
			  $affiliateStatics.order_id, $affiliateStatics.status
			  FROM $affiliateStatics INNER JOIN $orderTbl ON 
			  $orderTbl.orderid = $affiliateStatics.order_id WHERE $orderTbl.status = 'C' 
			  AND affiliate_id = '".$affiliateid."'  AND $affiliateStatics.status='".$status."' ";  
						

	    }
		$comm_details = func_query ($SQL);
		return $comm_details;
}

function  getAffiliatePaymentHistory($affiliateid,$recipient_type='')
{
	global  $sql_tbl;
	$sql_tbl[mk_designer_payment_commission] =  "mk_designer_payment_commission";
	
	$SQL= "SELECT  pc.payment_amt,FROM_UNIXTIME(payment_date,'%d/%m/%Y') as payment_date,
	pm.payment_method,s.status , pc.comments FROM
	$sql_tbl[payment_methods] as pm INNER JOIN $sql_tbl[mk_designer_payment_commission] as pc
	ON pm.paymentid =  pc.pay_method
	INNER JOIN $sql_tbl[mk_status] as s ON s.statusid = pc.payment_status
    WHERE pc.recipient_id =  '".$affiliateid."' AND  s.status_type = 'O' AND recipient_type='".$recipient_type."' ";
	$comm_history = func_query ($SQL);
	return $comm_history;
}

function getAffiliateID($affiliateid)
{
	global  $sql_tbl;
	$SQL= "SELECT  affiliate_id FROM $sql_tbl[mk_affiliates] WHERE affiliate_id = '".$affiliateid."'";
	$designer = func_query ($SQL);
	return $designer;

}
function getPendingComm_AffiliatesReport(){
	
	$query= " SELECT mka.userid,mka.affiliate_id as id,mka.company_name as name,mka.contact_email as email,
		       
		 FORMAT(IF(dtAffStats.commEarned IS NULL ,0, dtAffStats.commEarned),2) as total ,
		 FORMAT(IF(dtAffPay.paidComm IS NULL ,0 , dtAffPay.paidComm),2) as paid,
FORMAT((IF(dtAffStats.commEarned IS NULL ,0, dtAffStats.commEarned)-IF(dtAffPay.paidComm IS NULL ,0 , dtAffPay.paidComm)),2) as pending,
CONCAT_WS(',',xc.b_address,xc.b_city,xc.b_state,xc.b_country,xc.b_zipcode) as address
		 FROM mk_affiliates mka
		 INNER JOIN xcart_customers xc  ON  xc.login =mka.userid
LEFT JOIN (
		  SELECT  SUM(IF(mk_affiliate_statistics.commission IS NULL,0,mk_affiliate_statistics.commission)) as 
		  commEarned ,
		  mk_affiliate_statistics.affiliate_id as affiliate
		  FROM mk_affiliate_statistics INNER JOIN xcart_orders ON 
		  xcart_orders.orderid = mk_affiliate_statistics.order_id WHERE xcart_orders.status = 'C' 
		  GROUP BY mk_affiliate_statistics.affiliate_id  ) as dtAffStats 
		  ON mka.affiliate_id = dtAffStats.affiliate
LEFT JOIN (
		  SELECT  SUM(IFNULL(payment_amt,0)) as paidComm,
		  recipient_id as affiliateid
		  FROM mk_designer_payment_commission 
		  WHERE recipient_type = 'Affiliate' 
		  GROUP BY mk_designer_payment_commission.recipient_id ) as dtAffPay 
		  ON mka.affiliate_id = dtAffPay.affiliateid 
where (IF(dtAffStats.commEarned IS NULL ,0, dtAffStats.commEarned)-IF(dtAffPay.paidComm IS NULL ,0 , dtAffPay.paidComm)) >100
GROUP BY mka.affiliate_id  ORDER BY pending desc";
   $result=db_query($query);
   $affiliates=array();
  if ($result)
 {
	$i=0;

	while ($row=db_fetch_array($result))
	{
		$affiliates[$i] = $row;
		$i++;
	}
   }
	
	return $affiliates;
}

?>
