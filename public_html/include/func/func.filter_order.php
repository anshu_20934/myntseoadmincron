<?php
// This method will update the filter order for all the entries depending on the new filter order
// for the row.
require_once "$xcart_dir/include/class/Class.AllSizeArray.php";
$nocache = $_GET['nocache'];
function update_filter_order($table,$row_id,$filter_order, $where_clause){
	if(!empty($where_clause)){
		$where_clause = " and ".$where_clause;
	}else{
		$where_clause="";
	}

	// Get current filter order for the row.
	$res=func_query_first("select filter_order from $table where id='".$row_id."'");
	$filter_order_old = $res[filter_order];
	$res_filter_order=func_query_first("select id from $table where filter_order='".$filter_order."'".$where_clause);
	
	if(empty($row_id)){
		// New record inserted with filter order not 0 otherwise do nothing
		if($filter_order!=0 && !empty($res_filter_order)){
			$update_query="update $table
										set filter_order=filter_order+1 
										where filter_order>='".$filter_order."'  
										and filter_order != 0".$where_clause;
			$result_set=func_query($update_query);
		}
	}else if(empty($filter_order) && !is_numeric($filter_order)){
		// means order is deleted
		//for  zero filter otherwise do nothing
		if($filter_order_old != 0){
			$update_query="update $table
										set filter_order=filter_order-1 
										where filter_order>='".$filter_order_old."'  
										and filter_order != 0".$where_clause;
			$result_set=func_query($update_query);
		}
	}else if($filter_order == $filter_order_old){
		// Same filter order as the previous one so no change
	}else if(empty($res_filter_order) && !is_numeric($res_filter_order)){
		// No filter order exist for this entry so blindly enter it
	}else if($filter_order_old == 0){
		// enabling desabled filter order with non zero value
		$update_query="update $table
									set filter_order=filter_order+1 
									where filter_order>='".$filter_order."'  
									and filter_order != 0".$where_clause;
		$result_set=func_query($update_query);

	}else if($filter_order == 0){
		// disabling the filter
		$update_query="update $table
									set filter_order=filter_order-1 
									where filter_order>='".$filter_order_old."'  
									and filter_order != 0".$where_clause;
		$result_set=func_query($update_query);

	}else if($filter_order > $filter_order_old){
		// entered filter order is greater then the previous one
		$update_query="update $table
									set filter_order=filter_order-1 
									where filter_order<='".$filter_order."'"." 
									and filter_order >='".$filter_order_old."' 
									and id!='".$row_id."' 
									and filter_order != 0".$where_clause;
		$result_set=func_query($update_query);
	}else if($filter_order < $filter_order_old){
		//entered filter order is less then the previous one
		$update_query="update $table
									set filter_order=filter_order+1 
									where filter_order>='".$filter_order."'"." 
									and filter_order <='".$filter_order_old."' 
									and id!='".$row_id."' 
									and filter_order != 0".$where_clause;
		$result_set=func_query($update_query);
	}
}
function sortDisplayCategory($display_category_result_set){
	global $xcache;
	$display_category_ordered_by_filter=$xcache->fetch("display_category_ordered_by_filter");
	if($display_category_ordered_by_filter == NULL || $nocache == 1){
		$display_category_ordered_by_filter=func_query("select lower(name) as name,filter_order 
									from mk_display_categories 
									where is_active=1 and filter_order != 0 order by filter_order asc");
		if($nocache != 1){//Do not store in case nocache param is set
			$xcache->store("display_category_ordered_by_filter", $display_category_ordered_by_filter);
		}
	}
	$display_category_filter_facet_new = new stdClass();
	foreach ($display_category_ordered_by_filter as $tmp_display_category){
		if(!empty($display_category_result_set->$tmp_display_category['name'])){
			$display_category_filter_facet_new->$tmp_display_category['name']=$display_category_result_set->$tmp_display_category['name'];
		}
	}
	return $display_category_filter_facet_new;
}
function sortBrands($brands_result_set){
	global $xcache;

	$brands_ordered_by_filter=$xcache->fetch("brands_ordered_by_filter");
	if($brands_ordered_by_filter == NULL || $nocache == 1){
		$brands_ordered_by_filter=func_query("select attribute_value
									from mk_attribute_type_values 
									where is_active=1 and attribute_type='Brand' and filter_order != 0 order by filter_order asc");
		if($nocache != 1){//Do not store in case nocache param is set
			$xcache->store("brands_ordered_by_filter", $brands_ordered_by_filter);
		}
	}
	$brands_filter_facet_new = new stdClass();
	foreach ($brands_ordered_by_filter as $tmp_brand){
		if(!empty($brands_result_set->$tmp_brand['attribute_value'])){
			$brands_filter_facet_new->$tmp_brand['attribute_value']=$brands_result_set->$tmp_brand['attribute_value'];
		}
	}
	return $brands_filter_facet_new;
}
function sortArticleTypes($article_type_result_set){
	global $xcache;

	$article_types_ordered_by_filter=$xcache->fetch("article_types_ordered_by_filter");
	if($article_types_ordered_by_filter == NULL || $nocache == 1){
		$article_types_ordered_by_filter=func_query("select typename
									from mk_catalogue_classification 
									where is_active=1 and filter_order != 0 order by filter_order asc");
		if($nocache != 1){//Do not store in case nocache param is set
			$xcache->store("article_types_ordered_by_filter", $article_types_ordered_by_filter);
		}
	}
	$article_types_filter_facet_new = new stdClass();
	foreach ($article_types_ordered_by_filter as $tmp_article_type){
		if(!empty($article_type_result_set->$tmp_article_type['typename'])){
			$article_types_filter_facet_new->$tmp_article_type['typename']=$article_type_result_set->$tmp_article_type['typename'];
		}
	}
	return $article_types_filter_facet_new;
}

function sortSizes($sizes_result_set,$article_type){
	$size_result_array = (array)$sizes_result_set;
	$allSizeArray = AllSizeKey::getAllFeatureGateKeyValuePairs();
	$size_factet_array =array();
	foreach ($size_result_array as $key => $row)
	{
		if(!empty($allSizeArray[$key]))
		{
			$size_factet_array[$key] = $allSizeArray[$key];
		}
	}
	// sort it 
	asort($size_factet_array);
	
	
	// fill it value again 
	$size_factet_array_new =array();
	foreach ($size_factet_array as $key => $row)
	{
		$size_factet_array[$key] = $sizes_result_set->$key;
	}
	
	$sizes_filter_facet_new = (OBJECT)$size_factet_array;
	return $sizes_filter_facet_new;
}

function sortArticleSpecificAttributes($article_type_attributes_set,$article_type){
	global $xcache;
        if(empty($article_type))
            return null;

	$res=$xcache->fetchAndStore(function($article_type) {
            return func_query_first("select id from mk_catalogue_classification where typename='".$article_type."' and parent1 !=-1 and parent2 != -1");
        }, array($article_type), "mk_catalogue_classification:".$article_type);
       
	$attributes_new=new stdClass();
        
	foreach ($article_type_attributes_set as $key=>$tmp_attribute){
		$attribute_values_ordered_by_filter= $xcache->fetchAndStore(
                                    function($tmp_attribute, $res){
                                                    return func_query("select attribute_value
									from  mk_attribute_type at
									left join mk_attribute_type_values atv
									on  atv.attribute_type_id = at.id 
									where at.attribute_type='".$tmp_attribute['title']."' 
									and at.catalogue_classification_id='".$res['id']."'  
									and atv.filter_order != 0 order by filter_order asc");
                                    }, 
                                    array($tmp_attribute, $res), 
                                    "attribute_values_ordered_by_filter_".$tmp_attribute['title']."_".$res['id']);
		$attribute_array_new=array();
		foreach ($attribute_values_ordered_by_filter as $attribute_value_tmp){
			if(!empty($tmp_attribute['values'][strtolower($attribute_value_tmp['attribute_value'])])){
				$attribute_array_new[strtolower($attribute_value_tmp['attribute_value'])]=$tmp_attribute['values'][strtolower($attribute_value_tmp['attribute_value'])];
			};
		}
		$attributes_new->$key=array('title'=>$tmp_attribute['title'],'values'=>$attribute_array_new);
	}
	return $attributes_new;
}
?>
