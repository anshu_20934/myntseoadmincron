<?php
if (!defined('XCART_START'))
    {
    header ("Location: ../");
    die ("Access denied");
    }

/**************************************************************************************************************************/
function func_get_special_offer_discount_for_product($productid)
 {
	global $sql_tbl;
	$tb_product_category = $sql_tbl["products_categories"];
	//To get the category of the product
	$query = "select $tb_product_category.categoryid from
	$tb_product_category where $tb_product_category.productid =$productid";
	$result=db_query($query);

	$categoryid ;
	if($result)
	{
		$row=db_fetch_row($result);
		$categoryid = $row[0];
	}


	//Now get the style price for the product
	$tb_style = $sql_tbl["mk_product_style"];
	$tb_product =$sql_tbl["products"];

	$pricequery ="select $tb_style.price  from $tb_style,$tb_product where $tb_product.productid=$productid
	and $tb_product.product_style_id = $tb_style.id";

	$priceresult= db_query($pricequery);
	$productPrice ;
	if($priceresult)
	 {
		$pricerow = db_fetch_row($priceresult);
		$productPrice = $pricerow[0];
	 }
     $offersforproduct = func_get_discount_offer_data($productid,'P');
	 $discountonproduct = func_compute_discount_from_resultset($offersforproduct,$productPrice);
	 $offersforcategory;
	 if($categoryid)
	 $offersforcategory = func_get_discount_offer_data($categoryid,'C');
	 $discountoncategory = func_compute_discount_from_resultset($offersforcategory,$productPrice);

	 return $discountonproduct + $discountoncategory ;
 }
/**************************************************************************************************************************/

function func_get_discount_offer_data($paramid,$paramtype)
{
	global $sql_tbl;
	$tb_offer_condition_params = $sql_tbl["xcart_offer_condition_params"];
	$tb_offer_conditions = $sql_tbl["xcart_offer_conditions"];
	$tb_offers=$sql_tbl["xcart_offers"];
	$tb_offers_bonusses= $sql_tbl["xcart_offer_bonuses"];

	$time =time();
	//To find out if product or the category of that product is having any discount offer directly associated with
	$query = "select $tb_offer_conditions.amount_min,$tb_offer_conditions.amount_max,
	$tb_offers_bonusses.amount_min,$tb_offers_bonusses.amount_max,$tb_offers_bonusses.amount_type
	from $tb_offer_condition_params ,$tb_offer_conditions,$tb_offers,$tb_offers_bonusses
			where $tb_offer_condition_params.param_id = $paramid
			and $tb_offer_condition_params.conditionid = $tb_offer_conditions.conditionid
			and $tb_offer_condition_params.param_type = '$paramtype'
			and $tb_offer_conditions.offerid = $tb_offers.offerid
			and $tb_offers.offerid=$tb_offers_bonusses.offerid
			and $tb_offer_conditions.condition_type='S'
			and $tb_offer_conditions.avail='Y'
			and $tb_offers_bonusses.bonus_type='D'
			and $tb_offers.offer_start <= $time
			and $tb_offers.offer_end >= $time
			and $tb_offers.offer_avail='Y'";
		$offerresult=db_query($query);
    return $offerresult;
}

/****************************************************************************************************************/
function func_compute_discount_from_resultset($offerresult,$price)
{
	 $minAmount ;
	 $maxAmount;
	 $minDiscount;
	 $maxDiscount;
	 $totalDiscount;
	 $discount ;

	 if ($offerresult)
	 {
		 while ($row=db_fetch_row($offerresult))
		 {
			  $minAmount = $row[0];
			  $maxAmount = $row[1];
			  $maxDiscount = $row[3];

			  //if($price >= $minAmount && $price <= $maxAmount)
			  if($price >= $minAmount)
			  {
				  if($row[4] == "%")
				  {
					   //Compute %
					   $discount = $row[2]*($price/100);
					  // if($discount > $maxDiscount)$discount=$maxDiscount;

				  }
				  else if($row[4] == "$")
				  {
					  //compute pure value
					  $discount = $row[2];
					  $percentDsicount =($price/100)*$maxDiscount;
					  //if($discount >$percentDsicount)
					  $discount=$percentDsicount;
				  }
				  $totalDiscount = $totalDiscount + $discount;
			  }

		 }
	 }
	 return $discount;
}
/*******************************************************************************************************/

function func_get_special_offer_discount_for_Total($total)
{
	global $sql_tbl;
	$tb_offer_conditions = $sql_tbl["xcart_offer_conditions"];
	$tb_offers=$sql_tbl["xcart_offers"];
	$tb_offers_bonusses= $sql_tbl["xcart_offer_bonuses"];

	$time =time();
	//To find out if product or the category of that product is having any discount offer directly associated with
	$query = "select $tb_offer_conditions.amount_min,$tb_offer_conditions.amount_max,
	$tb_offers_bonusses.amount_min,$tb_offers_bonusses.amount_max,$tb_offers_bonusses.amount_type
	from $tb_offer_conditions,$tb_offers,$tb_offers_bonusses
			where $tb_offer_conditions.offerid = $tb_offers.offerid
			and $tb_offers.offerid=$tb_offers_bonusses.offerid
			and $tb_offer_conditions.condition_type='T'
			and $tb_offer_conditions.avail='Y'
			and $tb_offers_bonusses.bonus_type='D'
			and $tb_offers.offer_start <= $time
			and $tb_offers.offer_end >= $time
			and $tb_offers.offer_avail='Y'";
	$offerresult=db_query($query);

	return func_compute_discount_from_resultset($offerresult,$total );
}

/****************************************************************************/

function func_get_special_offers_for_Total()
{

	global $sql_tbl;
	$tb_offer_conditions = $sql_tbl["xcart_offer_conditions"];
	$tb_offers=$sql_tbl["xcart_offers"];
	$tb_offers_bonusses= $sql_tbl["xcart_offer_bonuses"];

	$time =time();
	//To find out if product or the category of that product is having any discount offer directly associated with
	$query = "select $tb_offer_conditions.amount_min, $tb_offer_conditions.amount_max,
	$tb_offers_bonusses.amount_min, $tb_offers_bonusses.amount_max, $tb_offers_bonusses.amount_type,
	$tb_offers.offer_name, $tb_offers.offerid
	from $tb_offer_conditions, $tb_offers, $tb_offers_bonusses
			where $tb_offer_conditions.offerid = $tb_offers.offerid
			and $tb_offers.offerid=$tb_offers_bonusses.offerid
			and $tb_offer_conditions.condition_type='T'
			and $tb_offer_conditions.avail='Y'
			and $tb_offers_bonusses.bonus_type='D'
			and $tb_offers.offer_start <= $time
			and $tb_offers.offer_end >= $time
			and $tb_offers.offer_avail='Y'";

	$offerresult=db_query($query);
	$offertotal = array();
	$i = 0;
	while($row = db_fetch_array($offerresult))
	{
		$offertotal[$i] = $row;
		$i++;
	}
	return $offertotal;
}

/**********************************************************************/

function func_get_discount_offers_data($paramidies, $paramtypes)
{
	global $sql_tbl;
	$tb_offer_condition_params = $sql_tbl["xcart_offer_condition_params"];
	$tb_offer_conditions = $sql_tbl["xcart_offer_conditions"];
	$tb_offers= $sql_tbl["xcart_offers"];
	$tb_offers_bonusses= $sql_tbl["xcart_offer_bonuses"];

	$time =time();
	$offers = array();
	foreach($paramidies AS $key=>$value)
	{
	//To find out if product or the category of that product is having any discount offer directly associated with
	$query = "select distinct $tb_offer_conditions.amount_min,$tb_offer_conditions.amount_max,
	$tb_offers_bonusses.amount_min,$tb_offers_bonusses.amount_max,$tb_offers_bonusses.amount_type,
	$tb_offers.offer_name, $tb_offers.offerid AS offerid
	from $tb_offer_condition_params ,$tb_offer_conditions,$tb_offers,$tb_offers_bonusses
			where $tb_offer_condition_params.param_id = $value
			and $tb_offer_condition_params.conditionid = $tb_offer_conditions.conditionid
			and $tb_offer_condition_params.param_type = '$paramtypes'
			and $tb_offer_conditions.offerid = $tb_offers.offerid
			and $tb_offers.offerid=$tb_offers_bonusses.offerid
			and $tb_offer_conditions.condition_type='S'
			and $tb_offer_conditions.avail='Y'
			and $tb_offers_bonusses.bonus_type='D'
			and $tb_offers.offer_start <= $time
			and $tb_offers.offer_end >= $time
			and $tb_offers.offer_avail='Y'";
		$offerresult=db_query($query);
		$i = 0;

		while($row = db_fetch_array($offerresult))
		{

			if(!in_array($row['offerid'], $offers))
			{
				$offers[$i] = $row;
			}
			$i++;
		}

	}
		return $offers;
}
/***********************************************************************/
################ function to check whether the user is buying a promotional offer product ##########
function verify_promotion_tracking_id($promotion_id){
	return false;
}

/* point where in percentage discount
 * to be calculated for group cricket
 * shop for its products in cart
 */
function calcluate_percentage_dicount($productsInCart){
	$group_cricket_products_total = 0;
	$products_count = 0;
	foreach($productsInCart as $p_id=>$prod_detail){
		if($prod_detail['producTypeId'] == 140){
			$group_cricket_products_total += $prod_detail['productPrice'];
			$products_count++; 
		}
	}	
	$percent = ($products_count < 4)? 11 : (($products_count < 7)? 17 : 30);//select percent
	return $group_cricket_products_discount = ($group_cricket_products_total/100) * $percent;//calculate percent discount
} 
##percentage discount for group cricket ends here


?>
