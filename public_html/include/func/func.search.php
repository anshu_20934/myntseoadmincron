<?php
if (!defined('XCART_START'))
{
	header ("Location: ../");
	die ("Access denied");
}
// function to load reccently added  searchs
$business_keywords =  array("","T-Shirt","Mug","Coaster","Mousepad","Greeting Cards","Posters","Keychain","Jigsaws","Notepads","Calendar","Plates","Watch","Pendant",
"Photo Prints",
"Sweatshirt",
"Money Bank",
"Pencil Box","Mirror","Teddy Bear");
//$business_original_keywords = array();
function get_recently_added_search($keywordid,$limit=10)
{
	global $sql_tbl,$weblog,$sqllog;
	$weblog->info("Inside Function :".__FUNCTION__." @ line  ".__LINE__." in file ".__FILE__);
	if($keywordid >2000)
		$range_keyid=$keywordid-1000;
	else
		$range_keyid =1;

	if(!empty($keywordid)){
		$results = func_query("SELECT keyword,orignal_keyword,key_id from mk_stats_search where count_of_products > 3 and key_id < '".$keywordid."' and key_id > '".$range_keyid."' and is_valid='Y' and length(orignal_keyword)>4 and length(orignal_keyword) <20 order by key_id desc limit 10");
		
		return func_filtered_search($results);
	}
	/*
	if(!empty($keywordid))
	{
		$qry = "SELECT prev_keywords FROM mk_stats_search WHERE key_id = $keywordid and is_valid='Y' ";
		$result = db_query($qry);	

		if( $row = db_fetch_row($result) )
		{
			$prev_kw_str = $row[0];
			$flag=0;
			//$flag=1 for if previous keyword is empty or less then 10
			if(empty($prev_kw_str)){
				$flag=1;
			}
			if( !empty($prev_kw_str) )
			{
				$resArray = explode("#",$prev_kw_str);
				$finalResult = array();
				foreach($resArray as $res)
				{
					$rowValues = explode(",",$res);
					$rowArray = array("keyword"=>$rowValues[0],"orignal_keyword"=>$rowValues[1],"key_id"=>$rowValues[2]);
					$finalResult[] = $rowArray;
				}
				//get results from query if results are less then 10 or empty
				
				if(count($finalResult)<10){
					$flag=1;
				}
				if($flag!=1)
				return func_filtered_search($finalResult);
			}
			$results = func_query("SELECT keyword,orignal_keyword,key_id from mk_stats_search where count_of_products > 3 and key_id < '".$keywordid."' and key_id > '".$range_keyid."' and is_valid='Y' and length(orignal_keyword)>4 and length(orignal_keyword) <20 order by key_id desc limit 10");
			return func_filtered_search($results);
		}
	}
	*/
}

//function for related searches
function get_related_searches($keywordid){
	global $sql_tbl,$weblog,$sqllog;

	$weblog->info("Inside Function :".__FUNCTION__." @ line  ".__LINE__." in file ".__FILE__);
	$sql ="select additional_keyword from mk_stats_search where key_id='".$keywordid."' and is_valid='Y' ";
	$sqllog->debug("SQL Query : $sql in File : ".__FILE__);
	$results = db_query($sql);
	$row=db_fetch_array($results);
	$addtional_keywords = $row['additional_keyword'];
	if(!empty($addtional_keywords))
	$related_keywords =func_query("select orignal_keyword,key_id from mk_stats_search where key_id in (".$addtional_keywords.") and is_valid='Y' and count_of_products>3 ");
	
	return $related_keywords;

}
// function to load most popular searches

function get_most_popular_search(){
	global $sql_tbl,$weblog,$sqllog;
	$weblog->info("Inside Function :".__FUNCTION__." @ line  ".__LINE__." in file ".__FILE__);
	$sql = "SELECT keyword,orignal_keyword,key_id from mk_search_popular_kw order by  count_of_searches desc limit 0,10 ";
	//	$sql = "SELECT keyword,orignal_keyword,key_id from mk_search_popular_kw limit 0,5";
	$sqllog->debug("SQL Query : $sql in File : ".__FILE__);
	$result = func_query($sql);
	$result = func_filtered_search($result);
	return $result;
}

function get_all_keywords_from_stats($start,$limit)
{
	$query = "select keyword,orignal_keyword,key_id from mk_search_recent_kw where keyword is not null order by last_search_date desc limit $start,$limit";
		
	$results  = func_filtered_search(func_query($query));

	foreach($results as $key=>$result)
	{
		$keyword = trim($result["filter_keyword"]);
		check_product_type_synonyms($keyword,$filteredKeyword);
		$prodSynonyms = checkForProductSynonyms($keyword);
		$productNameList = array();
		$productTypeList = array();
		if( !empty($prodSynonyms) )
		{
			$productTypeList = explode(",",$prodSynonyms);
		}
			
		$searchdata["keywords"] = $keyword;
		$searchdata["productname"] = $productTypeList[0];
		$designs = load_featured_designs_solr($searchdata,6);
			
		$keyword = str_replace(" ","-",$keyword);
		$result["filter_keyword"] =  $keyword;
		$result["designs"] = $designs;
		$results[$key] = $result;
	}
	//echo "<pre>";print_r( $results ); exit;
	return $results;
}

// function to load recently searches

function get_recently_searched($keywordid,$limit=5){

	global $sql_tbl,$weblog,$sqllog;
	
	// Commented the following log. Got irritated by the sheer volume of the crap it prints..
	//$weblog->info("Inside Function :".__FUNCTION__." @ line  ".__LINE__." in file ".__FILE__);

	if( !empty($keywordid) )
	{

			$sql = "SELECT keyword,orignal_keyword,key_id from mk_search_recent_kw where key_id != $keywordid and length(orignal_keyword)>4 and length(orignal_keyword)<20 order by last_search_date desc limit 0,$limit";
			$result = func_query($sql);
	        	return func_filtered_search($result);
	}
	else
	{
                        $sql = "SELECT keyword,orignal_keyword,key_id from mk_search_recent_kw where length(orignal_keyword)>4 and length(orignal_keyword)<20 order by last_search_date desc limit 0,$limit";
                        $result = func_query($sql);
                        return func_filtered_search($result);

	}
}


function func_filtered_search($tag_array_to_be_filtered){
 global $sql_tbl,$weblog,$sqllog,$filters;
 $curseWordArray =  array();
 //echo  $a= preg_replace("/love/i","","i love");	
 //exit;
 
 // Commenting the following log. Got irritated by the volumes it prints!
 //$weblog->info("Inside Function :".__FUNCTION__." @ line  ".__LINE__." in file ".__FILE__);
 $curseWordArray=file_get_contents(dirname(__FILE__)."/../../skin1/dirtywords.txt");
 $curseWordArray = preg_split("/(\r\n|\r|\n)/",$curseWordArray) ;
	  
				foreach($tag_array_to_be_filtered as $key=>$value){
					$tag_array_to_be_filtered[$key]['filter_keyword'] = $value['orignal_keyword'] ;
			 		foreach($curseWordArray as $curse_word_line){
						$tag_array_to_be_filtered[$key]['filter_keyword']= preg_replace("/$curse_word_line/i","",$tag_array_to_be_filtered[$key]['filter_keyword']);	
						 
			 		}
     			}
		
	return $tag_array_to_be_filtered;
}

function getAddDateForKeyword($keywordid){
	global $sql_tbl,$weblog,$sqllog;
	$weblog->info("Inside Function :".__FUNCTION__." @ line  ".__LINE__." in file ".__FILE__);
	$sql ="SELECT date_added FROM mk_stats_search WHERE key_id ='".trim($keywordid)."' ";
	$sqllog->debug("SQL Query : $sql in File : ".__FILE__);
	$resource = db_query($sql);
	$result = db_fetch_array($resource);
	return $result['date_added'];

}

function get_product_synonyms(){
	$sql = "select product_type_id,synonyms from mk_product_type_synonyms ";
	$synonyms = func_query($sql);
	return $synonyms;
}

function check_product_type_synonyms($keyword,&$filterKeyword){
	
	global $sqllog,$weblog ;
	$synonyms =  get_product_synonyms();
	
	$prduct_type_list = '';
	$filterKeyword = $keyword;
	foreach($synonyms as $_k => $_val){
		$productSyn=  explode(",",$_val['synonyms']);
		foreach($productSyn as $type ){
			$pattern = strtolower($type);
           	
			$keywordArray = explode(" ",$keyword);
			foreach($keywordArray  as $keyVal){
				if(!strcmp(strtolower(trim($keyVal)), strtolower(trim($pattern)))){
					//echo "Match - $keyVal and $pattern";
					$filterKeyword =  str_replace(strtolower(trim($type)),'',strtolower(trim($filterKeyword)));	
				}

			}		
		}
	}
}



function getSearchKeywordById($keyid){
	$sql = "select key_id, keyword,orignal_keyword from `mk_stats_search` where key_id = '".trim($keyid)."' and (is_valid='Y' or is_valid='P') " ;
	$resource = db_query($sql);
	$result = db_fetch_array($resource);
	return $result;
	//return $result['keyword'];
}

function checkForProductSynonyms($keyword){
	global $sqllog,$weblog ;
	$synonyms =  get_product_synonyms();
	$prduct_type_list = '';
	foreach($synonyms as $_k => $_val){
		$productSyn=  explode(",",$_val['synonyms']);
//		foreach($productSyn as $type ){
//			
//			if(!strcmp(strtolower(trim($keyword)),strtolower(trim($type)))){
//				$prduct_type_list .= $_val['product_type_id'].",";
//				$weblog->info("Matched product type list >> ".$prduct_type_list." in serach @ line ".__LINE__."  in file ".__FILE__);
//				
//			}
		foreach($productSyn as $type ){
			$pattern = strtolower($type);
           	$keywordArray = explode(" ",$keyword);
			foreach($keywordArray  as $keyVal){
				if(!strcmp(strtolower(trim($keyVal)), strtolower(trim($pattern)))){
					$prduct_type_list .= $_val['product_type_id'].",";
					$weblog->info("Matched product type list >> ".$prduct_type_list." in serach @ line ".__LINE__."  in file ".__FILE__);
					
				}

			}		
		}
	}

	$weblog->info("Product Type list >> ".$prduct_type_list." used in serach @ line ".__LINE__."  in file ".__FILE__);
	return trim($prduct_type_list,",");
}

function getProductNames($productTypeList)
{
	global $weblog, $xcache;
	$productNameList = array();
	foreach($productTypeList as $prodId)
        {
		$weblog->info($prodId." >>>>>>>>>>>>>> prodId");
                
                $productNameList[] = $xcache->fetchAndStore(function($prodId){
                        $sql = "select name from mk_product_type where id=$prodId";
                        $res= db_query($sql);
                        $row = db_fetch_row($res);
                        return $row[0];
                    }, 
                    array($prodId),
                    "mk_product_type:".$prodId, 
                    1800);
        }	
	return $productNameList;
}

function generate_search_page($keyword,$curseWordArray)
{
	$query = "select key_id from mk_stats_search where orignal_keyword = '$keyword'";
	$res = db_query($query);
	if( db_num_rows($res) !=0 )
	{
		$row = db_fetch_row($res);
		// return (is_new,is_published,new_key_id);
		return array(0,1,$row[0]);
	}
	else
	{
		check_product_type_synonyms($keyword,$filteredKeyword);
		$filthyWordStatus = false;
		$keysArray = explode(" ",trim($filteredKeyword));

		foreach($keysArray as $key=>$value)
		{
			foreach($curseWordArray as $curse_word_line)
			{
				if(strcmp(strtolower(trim($value)),strtolower(trim($curse_word_line))))
				{
					$filterKeywords .= trim($value)." ";
				}
				else
				{
					$filthyWordStatus = true;
				}
			}
		}	
		if(!$filthyWordStatus)
		{
			// Generate a page for the keyword and for (keyword+producttype) combinations if available
			$key_id = track_search_keywords($keyword,$filteredKeyword);	
			$is_published = ($key_id == 0)?0:1;
			return array(1,$is_published,$key_id);
		}
		else
		{
			//consider filthy keyword as published ,to avoid re-run again & again
			// return (is_new,is_published,new_key_id);
			return array(1,1,0);
		}
	}
}

function track_search_keywords($orignal_keyword,$filteredKeyword)
{
	global $business_keywords,$weblog ;
	$business_original_keywords = array() ;
	
	$filteredKeyword = trim($filteredKeyword);
	$weblog->info($orignal_keyword." >>>>>>>>>>>>>> original keyword ");

	$prodSynonyms = checkForProductSynonyms($orignal_keyword);
	$productNameList = array();
	$productTypeList = array();
	if( !empty($prodSynonyms) )
	{
		$productTypeList = explode(",",$prodSynonyms);	
		$productNameList = getProductNames($productTypeList);
	}
	$thisCount = 0;
	$condition = "(shopmasterid:0) AND ";
	// Perform search on all products or on all products with  filteredkeyword	
	if( empty($filteredKeyword) )
        	$condition .= "(*:*)";
       	else
	{
		$filteredKeyword = removeSpecialChars($filteredKeyword);
		$searchKeys = explode(" ",trim($filteredKeyword));
		$condition_tmp = array();
		foreach($searchKeys as $key)
		{
			$key = trim($key);
			
			if( !empty($key) )
				$condition_tmp[] = "(product:$key OR descr:$key OR fulldescr:$key OR keywords:$key OR stylename:$key)";
		}       
		//$condition_tmp[] = "(product:$filteredKeyword OR descr:$filteredKeyword OR fulldescr:$filteredKeyword OR keywords:$filteredKeyword OR stylename:$filteredKeyword)";
		$condition .= "(".implode(" OR ",$condition_tmp).")";
	}
  	try
 	{
		$solrIndex = new solrProducts();
 	}
 	catch(Exception $ex)
  	{
   		print_r($ex);
   	}

   	//	If you want to chage this facetField, check the schema.xml for the fieldname. If it doesnt exist, add the fieldname in the schema,
   	//	rebuild the index. Please note the difference between text(field : name) and string(field : name_exact) types. Also check how 
	//	copyField is used for facet field "name_exact"

	$facetField = "name_exact"; //groupby
	$result = $solrIndex->facetedSearch($condition,$facetField);
	
	//Now update or insert all the generated keywords ( filtered keyword + product name)  with count of products
	$count_for_basekeyword = 0;

	foreach( $result->name_exact as $key=>$value)
	{
		if( in_array($key,$business_keywords) )
		{
        		$product_count_words = $value;
			$product_base_word = trim($filteredKeyword). " " . strtolower(trim($key));
			
                                                
			if($product_count_words > 0)
			{
				$sql = "select key_id from mk_stats_search where orignal_keyword = '".trim($product_base_word)."' and (is_valid='Y' or is_valid='P') ";
				$res = db_query($sql);
				if(db_num_rows($res) == 0)
				{
					
					$sql = "INSERT INTO  mk_stats_search  (keyword,orignal_keyword,date_added,last_search_date,count_of_products,count_of_searches) VALUES('".mysql_escape_string(trim($filteredKeyword))."','".mysql_escape_string(trim($product_base_word))."','".time()."','".time()."',$product_count_words,1) ";
					db_query($sql);
					$key_id = db_insert_id();
					update_prev_keywords($key_id);
				}
				else
				{
					
					$row1 = db_fetch_array($res);
					//$query_data = array();
					$query_data = array(
						"count_of_products" => $product_count_words,
						);
					func_array2update("mk_stats_search",$query_data," key_id = '".$row1['key_id']."'");  
				}
				$count_for_basekeyword = $count_for_basekeyword + $product_count_words ;

			}
			if(!empty($productNameList) && check_product_type_in_basekeyword( $productNameList, $product_base_word) )
			{
				$thisCount = $thisCount + $product_count_words;		
			}
		}
	}
	//Now insert or update filtered keyword with total number of products
	if(!empty($filteredKeyword) && $count_for_basekeyword >0)
	{
		$sql = "select key_id from mk_stats_search where orignal_keyword = '$filteredKeyword' and (is_valid='Y' or is_valid='P') ";
		$res = db_query($sql);
		if( db_num_rows($res) == 0)
		{
			
			$insert_time = time();
			$sql = "INSERT INTO mk_stats_search(keyword,orignal_keyword,date_added,last_search_date,count_of_products,count_of_searches) VALUES('$filteredKeyword','$filteredKeyword',$insert_time,$insert_time,$count_for_basekeyword,1)";
			db_query($sql);
			$key_id = db_insert_id();
			update_prev_keywords($key_id);
		}
		else
		{
			$row1 = db_fetch_array($res);
			$query_data = array("count_of_products"=>$count_for_basekeyword);
			func_array2update("mk_stats_search",$query_data," key_id = '".$row1['key_id']."'");
		}
	} 

	// Now insert or update actual search keyword user is looking for
	$sql = "select key_id, keyword from `mk_stats_search` where orignal_keyword = '".trim($orignal_keyword)."' and (is_valid='Y' or is_valid='P') " ;
	$resource = db_query($sql);

	if(db_num_rows($resource)  > 0 )
	{
		$result = db_fetch_array($resource);
		$sql ="UPDATE  mk_stats_search SET last_search_date ='".time()."' WHERE key_id ='".$result['key_id']."' ";
		db_query($sql);
		return $result['key_id'];

	}else
	{
		if( $thisCount > 0)
		{
			$sql ="INSERT INTO  mk_stats_search  (keyword,orignal_keyword,date_added,last_search_date,count_of_products,count_of_searches) VALUES('".mysql_escape_string(trim($filteredKeyword))."','".mysql_escape_string(trim($orignal_keyword))."','".time()."','".time()."',$thisCount,1) ";
			db_query($sql);
			$key_id = db_insert_id();
			update_prev_keywords($key_id);
			return $key_id;
		}
		return 0;  // 0 products found for this keyword
	}
}

function check_product_type_in_basekeyword($prodNameList,$baseKeyword)
{
	foreach($prodNameList as $prodName)
	{
		if( strpos(strtolower($baseKeyword), strtolower($prodName)) )
		{
			return true;
		}
	}
	return false;
}

function update_prev_keywords($keyid)
{
	
        $query = "call update_previous_keywords($keyid);";
        db_query($query);
	/*$this_date_added = getAddDateForKeyword($keyid);
	$sql = "SELECT keyword,orignal_keyword,key_id from mk_stats_search where count_of_products > 0 and date_added < $this_date_added order by date_added desc limit 0,5";
	$res = db_query($sql);
	$thisRowStr = array();
	while($row=db_fetch_row($res))
	{
		$thisRowStr[] = "$row[0],$row[1],$row[2]";
	}
	$prevKwStr = implode("#",$thisRowStr);
	$sql = "update mk_stats_search set prev_keywords = '$prevKwStr' where key_id=$keyid";
	db_query($sql);*/
}

function update_product_count($total,$keyword,$keyid){
	if($total >0)
	{
		$sql ="UPDATE  mk_stats_search SET count_of_products = $total WHERE key_id ='".$keyid."' ";
		db_query($sql);
	}

	$sql ="UPDATE  mk_stats_search SET count_of_searches = count_of_searches+1,last_search_date ='".time()."' WHERE key_id ='".$keyid."' ";
	db_query($sql);
	
	//$sql ="UPDATE  mk_stats_search SET last_search_date ='".time()."' WHERE key_id ='".$keyid."' ";
	//db_query($sql);
	
//   $sql ="UPDATE  mk_stats_search SET flag_count = 1 WHERE key_id ='".$keyid."' and count_of_products >0 ";
//	db_query($sql);

}

function update_recent_popular_stats($keyword,$org_keyword,$keywordid)
{
	$timestamp = time();
	$query = "call update_recent_popular_search($keywordid,'$keyword','$org_keyword',$timestamp);";
	db_query($query);
}


function get_product_filter_results($keywordid){
	global $weblog,$business_keywords,$sqllog;
	$unique_original_keywords = array() ;	
	$unique_key_ids = array();
	$weblog->info("Inside Function :".__FUNCTION__." @ line  ".__LINE__." in file ".__FILE__);
	$sql = "SELECT keyword  from mk_stats_search  where  key_id = '".$keywordid."' and is_valid='Y' ";
		$sqllog->debug("SQL Query : $sql in File : ".__FILE__);
	$result = db_query($sql);
	while($row = db_fetch_array($result))
  	{
  		$unique_keyword= $row['keyword'];
  		//echo $unique_keyword ;
  	}
	if(!empty($unique_keyword))
	{
  		foreach($business_keywords as $unique)
		{
  		 	$unique_original_keywords[] = trim($unique_keyword)." ".strtolower(trim($unique));
	   	}
	    	foreach ($unique_original_keywords as $v)
		{	
	    		$sql = "select key_id from mk_stats_search where orignal_keyword = '".$v."' and count_of_products >3 and is_valid='Y' limit 1";
	       		$result = db_query($sql);
			while($row = db_fetch_array($result))
  			{
  				$unique_key_ids[]=  $row['key_id'];
  			}
	    }

	    return $unique_key_ids;
	}
	else
	{
		foreach($business_keywords as $unique)
		{
  		 	$unique_original_keywords[] = strtolower(trim($unique));
	    }
	    foreach ($unique_original_keywords as $v)
		{
	    	
	    	$sql = "select key_id from mk_stats_search where orignal_keyword = '".$v."' and count_of_products >3 and is_valid='Y' limit 1";
	       	$result = db_query($sql);
			while($row = db_fetch_array($result))
  			{
  				$unique_key_ids[]=  $row['key_id'];
  			}
	    }
		return $unique_key_ids ;
	}
}



function get_recently_searched_home($keywordid){

	global $sql_tbl,$weblog,$sqllog;
	$weblog->info("Inside Function :".__FUNCTION__." @ line  ".__LINE__." in file ".__FILE__);
	if(!empty($keywordid)){
		$sql = "SELECT keyword ,orignal_keyword,key_id from mk_stats_search  where count_of_products >3 and key_id <> '".$keywordid."' order by  last_search_date desc limit 0,50 ";
		$sqllog->debug("SQL Query : $sql in File : ".__FILE__);
		$result = func_query($sql);
		return $result;
	}else{
		$sql = "SELECT keyword ,orignal_keyword,key_id from mk_stats_search where count_of_products >3 order by  last_search_date desc limit 0,50 ";
		$sqllog->debug("SQL Query : $sql in File : ".__FILE__);
		$result = func_query($sql);
		return $result;

	}

}
function load_featured_designs_solr($searchdata="",$limit=3)
{
	$offset = 0;
	$sortparam = "RANDOM";
	$shopMasterId = 0;
	$numFound = 0;
	$products = func_start_shopping_search($searchdata,$limit,$offset,$sortparam,$shopMasterId,$numFound);
	//adjust the data for current ui display logic in smarty
	$newProducts = array();
	foreach($products as $product)
	{
		$thisproduct = array();
		$thisproduct[0] = $product[0];
		$thisproduct[1] = $product[3];
		$thisproduct[2] = $product[5];
		$thisproduct[3] = $product[1];
		$thisproduct[4] = $product[6];
		$thisproduct[5] = $product[4];
		$thisproduct[6] = "";  // designer name - not present in solrr
		$thisproduct[7] = $product[8];
		$thisproduct[8] = str_replace(array(",",".")," ",$product[12]);	
		$newProducts[] = $thisproduct;	
	}
	return $newProducts;
}

function func_start_shopping_search($searchdata,$limit,$offset,$sortparam,$shopMasterId,&$numFound)
{
        //$catalogid = $searchdata["catalogid"];
        $keywords = $searchdata["keywords"];
        $productname = $searchdata["productname"];
        $pricefrom = $searchdata["pricefrom"];
        $priceto = $searchdata["priceto"];

	// build the query
	$query = "(shopmasterid:$shopMasterId) AND (statusid:1008)";
	
	if( !empty($productname))
	{
		$query .= " AND (typeid:$productname)";	
	}
	if( !empty($keywords) )
	{
		$subQuery = "";
		$keywords = removeSpecialChars($keywords);
		$keys = explode(" ",$keywords);
		$sKeys = array();
		foreach( $keys as $key)
		{	
			if( !empty($key) )
			{
				$sKeys[] = "(keywords:$key OR stylename:$key OR product:$key OR descr:$key OR fulldescr:$key)";
			}
		
		}
		$query .= " AND (".implode("OR",$sKeys).")";
	}
	if( !empty($pricefrom) || !empty($priceto) )
	{
		if( empty($pricefrom) )
			$pricefrom = "*";
		else
			$pricefrom = trim($pricefrom);
		if( empty($priceto) )
			$priceto = "*";
		else
			$priceto = trim($priceto);	
		$query .= " AND (price:[$pricefrom TO $priceto])";	
	}
	//} // end of else
	try
	{
		$solrIndex = new solrProducts();
		switch($sortparam)
                {
                        case "MOSTRECENT" :
                                $sortField = "justarrived desc";
                                //$orderbys[] = "$sql_tbl[products].add_date DESC";
                                break;
                        case "BESTSELLING" :
                                $sortField = "bestselling desc";
                                 //$orderbys[] = "$sql_tbl[products].sales_stats DESC";
                                 break;
                        case "USERRATING" :
                                $sortField = "userrating desc";
                                //$orderbys[] = "$sql_tbl[products].rating DESC";        
                                break;
                        case "EXPERTRATING" :
                                $sortField = "myntrarating desc";
                                //$orderbys[] = "$sql_tbl[products].myntra_rating DESC";
                                break;
			case "RANDOM":
				$uid = time();
				$sortField = "random$uid desc";
               	}
			
		$sResults = $solrIndex->searchAndSort($query,$offset,$limit,$sortField.', justarrived desc');	
		$numFound = $sResults->numFound;
		$products = array();
		$rec  = 0;
		foreach($sResults->docs as $document)
		{
			$productname_orig = $document->__get("product");
			$stopChars = array("#","/","\\","\"","\'","/","?","&");
			$productname_orig = str_replace($stopChars,"",$productname_orig);
			$namelength = strlen($productname_orig);
			$productname = ($namelength >= '22')?substr($productname_orig, 0, 19)."...":$productname_orig;
			$cp_product =  $productname_orig;
			$cp_product = preg_replace("/[^a-zA-Z0-9s-]/", "-", $cp_product);
			$cp_product = preg_replace('#([ ])+#','-',$cp_product);
			$cp_product = preg_replace('#([-])+#','-',$cp_product);

			$products[$rec] = array(
                                      $document->__get("productid"),
                                      $productname_orig,
                                      number_format($document->__get("price")),
                                      $document->__get("imgfilename"),
									  $document->__get("typeid"),
                                      $document->__get("typename"),
                                      $document->__get("styleid"),
                                      $document->__get("stylename"),
                                      $document->__get("image_portal_t"),
                                      $document->__get("myntrarating"),
                                      $document->__get("img_portal_thumb_160"),
                                      $document->__get("popup_image_path"),
				     $document->__get("fulldescr")
				);
            $products[$rec]["prodname"] = $cp_product;
           
			$rec++;
		
		}
		return $products;
		
	}
	catch(Exception $ex)
	{
		$weblog->info(" *************** Solr Exception ****************** ");
		header("location:mksystemerror.php");
	}

}
function func_start_shopping_search_new($searchdata,$limit,$offset,$sortparam,$shopMasterId,&$numFound)
{
        //$catalogid = $searchdata["catalogid"];
        $keywords = $searchdata["keywords"];
        $productname = $searchdata["productname"];
        $pricefrom = $searchdata["pricefrom"];
        $priceto = $searchdata["priceto"];

	// build the query
	$query = "(shopmasterid:$shopMasterId) AND (statusid:1008)";

	if( !empty($productname))
	{
		$query .= " AND (typeid:$productname)";
	}
	if( !empty($keywords) )
	{
		$subQuery = "";
		$keywords = removeSpecialChars($keywords);
		$keys = explode(" ",$keywords);
		$sKeys = array();
		foreach( $keys as $key)
		{
			if( !empty($key) )
			{
				$sKeys[] = "(keywords:$key OR stylename:$key OR product:$key OR descr:$key OR fulldescr:$key)";
			}

		}
		$query .= " AND (".implode("OR",$sKeys).")";
	}
	if( !empty($pricefrom) || !empty($priceto) )
	{
		if( empty($pricefrom) )
			$pricefrom = "*";
		else
			$pricefrom = trim($pricefrom);
		if( empty($priceto) )
			$priceto = "*";
		else
			$priceto = trim($priceto);
		$query .= " AND (price:[$pricefrom TO $priceto])";
	}
	//} // end of else
	try
	{
		$solrIndex = new solrProducts();
		switch($sortparam)
                {
                        case "MOSTRECENT" :
                                $sortField = "justarrived desc";
                                //$orderbys[] = "$sql_tbl[products].add_date DESC";
                                break;
                        case "BESTSELLING" :
                                $sortField = "bestselling desc";
                                 //$orderbys[] = "$sql_tbl[products].sales_stats DESC";
                                 break;
                        case "USERRATING" :
                                $sortField = "userrating desc";
                                //$orderbys[] = "$sql_tbl[products].rating DESC";
                                break;
                        case "EXPERTRATING" :
                                $sortField = "myntrarating desc";
                                //$orderbys[] = "$sql_tbl[products].myntra_rating DESC";
                                break;
			case "RANDOM":
				$uid = time();
				$sortField = "random$uid desc";
               	}

		$sResults = $solrIndex->searchAndSort($query,$offset,$limit,$sortField.', justarrived desc');
		$numFound = $sResults->numFound;
		$products = array();
		$rec  = 0;
		foreach($sResults->docs as $document)
		{
			$productname_orig = $document->__get("product");
			$stopChars = array("#","/","\\","\"","\'","/","?","&");
			$productname_orig = str_replace($stopChars,"",$productname_orig);
			$namelength = strlen($productname_orig);
			$productname = ($namelength >= '22')?substr($productname_orig, 0, 19)."...":$productname_orig;
			$cp_product =  $productname_orig;
			$cp_product = preg_replace("/[^a-zA-Z0-9s-]/", "-", $cp_product);
			$cp_product = preg_replace('#([ ])+#','-',$cp_product);
			$cp_product = preg_replace('#([-])+#','-',$cp_product);

			$products[$rec] = array(
                                      "productid"=>$document->__get("productid"),
                                      "product"=>$productname_orig,
                                      "price"=>number_format($document->__get("price")),
                                      "imgfilename"=>$document->__get("imgfilename"),
									  "typeid"=>$document->__get("typeid"),
                                      "typename"=>$document->__get("typename"),
                                      "styleid"=>$document->__get("styleid"),
                                      "stylename"=>$document->__get("stylename"),
                                      "image_portal_t"=>$document->__get("image_portal_t"),
                                      "myntrarating"=>$document->__get("myntrarating"),
                                      "img_portal_thumb_160"=>$document->__get("img_portal_thumb_160"),
                                      "popup_image_path"=>$document->__get("popup_image_path"),
				                      "fulldescr"=>$document->__get("fulldescr")
				);
            $products[$rec]["prodname"] = $cp_product;
            $products[$rec]["image_men_l"] = $document->__get("image_men_l");
            $products[$rec]["image_men_s"] = $document->__get("image_men_s");
            $products[$rec]["image_women_l"] = $document->__get("image_women_l");
            $products[$rec]["image_women_s"] = $document->__get("image_women_s");
            $products[$rec]["image_kid_l"] = $document->__get("image_kid_l");
            $products[$rec]["image_kid_s"] = $document->__get("image_kid_s");
            $products[$rec]["usergroup"] = $document->__get("usergroup");            
			$rec++;

		}
		return $products;

	}
	catch(Exception $ex)
	{
		$weblog->info(" *************** Solr Exception ****************** ");
		header("location:mksystemerror.php");
	}

}
function func_apparel_start_shopping($searchdata,$limit,$offset,$sortparam,$shopMasterId,&$numFound)
{
        //$catalogid = $searchdata["catalogid"];
		$parentid = $searchdata["parentid"];
		$subparentid = $searchdata["subparentid"];


	// build the query
	$query = "(shopmasterid:$shopMasterId)";

	if(!empty($parentid))
	{
		$query .= " AND (parentid:$parentid)";	
	}
	if(!empty($subparentid))
	{
		$query .= " AND (subparentid:$subparentid)";	
	}


	try
	{
		$solrIndex = new solrProducts();
		switch($sortparam)
                {
                        case "MOSTRECENT" :
                                $sortField = "justarrived desc";
                                //$orderbys[] = "$sql_tbl[products].add_date DESC";
                                break;
                        case "BESTSELLING" :
                                $sortField = "bestselling desc";
                                 //$orderbys[] = "$sql_tbl[products].sales_stats DESC";
                                 break;
                        case "USERRATING" :
                                $sortField = "userrating desc";
                                //$orderbys[] = "$sql_tbl[products].rating DESC";        
                                break;
                        case "EXPERTRATING" :
                                $sortField = "myntrarating desc";
                                //$orderbys[] = "$sql_tbl[products].myntra_rating DESC";
                                break;
						case "RANDOM":
								$uid = time();
								$sortField = "random$uid desc";
               	}
			
		$sResults = $solrIndex->searchAndSort($query,$offset,$limit,$sortField);	
		$numFound = $sResults->numFound;
		$products = array();
		$rec  = 0;
		foreach($sResults->docs as $document)
		{
			$productname_orig = $document->__get("product");
			$stopChars = array(" ","#","/","\\","\"","\'","/","?","&");
			$productname_orig = str_replace($stopChars,"",$productname_orig);
			$namelength = strlen($productname_orig);
			$productname = ($namelength >= '18')?substr($productname_orig, 0, 15)."...":$productname_orig;
			$cp_product =  $productname_orig;
			$cp_product = preg_replace("/[^a-zA-Z0-9s-]/", "-", $cp_product);
			$cp_product = preg_replace('#([ ])+#','-',$cp_product);
			$cp_product = preg_replace('#([-])+#','-',$cp_product);

			$products[$rec] = array(
                                      $document->__get("productid"),
                                      $productname_orig,
                                      number_format($document->__get("price")),
                                      $document->__get("imgfilename"),
				      $document->__get("typeid"),
                                      $document->__get("typename"),
                                      $document->__get("styleid"),
                                      $document->__get("stylename"),
                                      $document->__get("image_portal_t"),
                                      $document->__get("myntrarating"),
                                      $document->__get("img_portal_thumb_160"),
                                      $document->__get("popup_image_path"),
				     $document->__get("fulldescr")
				);
                        $products[$rec]["prodname"] = $cp_product;
			$rec++;
		
		}
		return $products;
		
	}
	catch(Exception $ex)
	{
		$weblog->info(" *************** Solr Exception ****************** ");
		header("location:mksystemerror.php");
	}

}
function removeSpecialChars($keyword)
{
	$keyword = preg_replace("/[^a-zA-Z0-9s-]/", " ", $keyword);
	$keyword = trim($keyword);
	$keyword = preg_replace('#([ ])+#',' ',$keyword);
	//$keyword = preg_replace('#([-])+#',' ',$keyword); 

	return $keyword;
}
function removeCommonWords($input){
      $commonWords = array('a','adj','am','an','and','any','are','aren\'t','as','a\'s','at','b','be','by','c','c\'s','d','e','et','f','h','had','hadn\'t','has','hasn\'t','have','haven\'t','he','he\'d','he\'ll','here\'s','he\'s','i','i\'d','ie','if','i\'ll','i\'m','in','inasmuch','inc','inc.','i\'ve','j','k','l','let\'s','ltd','m','mayn\'t','mightn\'t','mustn\'t','n','nd','needn\'t','o','one\'s','or','oughtn\'t','p','q','qv','r','rd','re','s','shan\'t','she\'d','she\'ll','she\'s','shouldn\'t','sub','sup','t','th','that\'ll','that\'s','that\'ve','there\'d','there\'ll','there\'re','there\'s','there\'ve','they\'d','they\'ll','they\'re','they\'ve','t\'s','u','un','v','viz','vs','w','wasn\'t','we\'d','we\'ll','we\'re','weren\'t','we\'ve','what\'ll','what\'s','what\'ve','where\'s','who\'d','who\'ll','who\'s','won\'t','wouldn\'t','x','y','you\'d','you\'ll','you\'re','you\'ve','z',);
   
      
	 if(stristr($input,"-")==TRUE)
		  return preg_replace('/\b('.implode('|',$commonWords).')\s/','',$input);
	 else
		  return preg_replace('/\b('.implode('|',$commonWords).')\b/','',$input);


   
      }
	  function removeSpecialCharandSpaces($keyword)
	{
        $keyword = preg_replace("/[^a-zA-Z0-9s-]/", " ", $keyword);
        $keyword = trim($keyword);
        $keyword = preg_replace('#([ ])+#',' ',$keyword);
       // $keyword = preg_replace('#([-])+#','-',$keyword);

        return $keyword;
	}
function replace_synonyms($keyword,&$filterkeyword){
//$id =0 for getting keyword and $id=1 for original keyword
$keyword=strtolower($keyword);
$results =db_query("select base_word,synonyms from mk_product_type_synonyms ");
$arr_str=array();
while($row=db_fetch_array($results)){
	$baseword = $row['base_word'];
	$arr_str[strtolower($baseword)]=strtolower($row['synonyms']);

}
foreach($arr_str as $k=>$v){
	foreach(explode(",",$v) as $checksyn){
		foreach(explode(" ",$keyword) as $kword){
				if(!strcmp(trim($checksyn),$kword)){
					$filterkeyword=str_replace(trim($checksyn),"",$keyword);
					return str_replace(trim($checksyn),$k,$keyword);
				}
		}
	}
}
return $keyword;
}
?>
