<?php

include_once("$xcart_dir/include/class/oms/EventCreationManager.php");

if (!defined('XCART_START'))
{
	header ("Location: ../");
	die ("Access denied");
}

/**************************************************************************************************************************/
function func_create_search_query($item_status='', $item_assignee='', $item_location='', $item_id='', $order_id='', $order_status='', $customer_name='', $product_type='',$product_style='', $order_by='', $sort_order='', $from_date='', $to_date='',$operations_location='',$count_only=false, $payment_method,$is_customizable,$sourceid='')
{
	global $sql_tbl;

    if($count_only==false)
	  $sql = "SELECT a.*,i.source_name AS source_name, e.name as product_type_name, a.completion_time AS completion_time, d.name AS stylename, a.location AS order_loc, f.login,f.order_name , f.queueddate AS queueddate, a.quantity_breakup, a.assignment_time AS assignment_time, f.payment_method as paymentmethod ";
    else
      $sql = "SELECT count(*)";
	$sql .= " FROM (((((({$sql_tbl['order_details']} a LEFT JOIN {$sql_tbl['mk_assignee']} b ON a.assignee=b.id) LEFT JOIN {$sql_tbl['operation_location']} g on a.location=g.id) LEFT JOIN $sql_tbl[mk_product_style] d ON a.product_style=d.id) LEFT JOIN $sql_tbl[mk_product_type] e ON a.product_type=e.id) LEFT JOIN $sql_tbl[orders] f ON a.orderid=f.orderid) LEFT JOIN {$sql_tbl['sources']} i ON f.source_id=i.source_id )";
	$sql .= "";
	
	//if(!empty($item_assignee) || $order_by='assignee'){
		//$sql .= " LEFT JOIN $sql_tbl[mk_assignee] b ON ";
	//}

	$sql .= " WHERE f.orgid!='1' ";

	if(!empty($item_status))
	{
		foreach($item_status AS $key=>$value)
		{
			$item_status_str .= ","." '".$value."' ";
		}
		$item_status = trim($item_status_str, ",");
		$sql .=" AND a.item_status IN($item_status)";
	}
	else
	{
        $sql .=" AND (a.item_status = 'UA' OR a.item_status = 'N' OR a.item_status = 'RP') ";
	}
	
	if(!empty($sourceid))
	{
		foreach($sourceid AS $key=>$value)
		{
			$source_id_str .= ","." '".$value."' ";
		}
		$sourceid = trim($source_id_str, ",");
		$sql .=" AND i.source_id IN($sourceid)";
	}


	if(!empty($operations_location)){
		$sql .= " AND f.operations_location = '$operations_location'";
	}

	if(!empty($item_location))
	{
		foreach($item_location AS $key=>$value)
		{
			$item_location_str .= ","." '".$value."' ";
		}
		$item_location = trim($item_location_str, ",");
		$sql .=	" AND a.location IN($item_location)";
	}

	if(!empty($item_assignee))
	{
		foreach($item_assignee AS $key=>$value)
		{
			$item_assignee_str .= ","." '".$value."' ";
		}
		$item_assignee = trim($item_assignee_str, ",");
		$sql .= " AND a.assignee IN($item_assignee) AND a.assignee = b.id";
	}

	if(!empty($item_id))
	{
		$sql .=" AND a.itemid = '$item_id'";
	}
	if(!empty($order_id))
	{ 
		$sql .=" AND a.orderid = '$order_id' ";
	}
    
	if(!empty($order_status))
	{
        foreach($order_status AS $key=>$value)
		{
			$order_status_str .= ","." '".$value."' ";
		}
		$order_status = trim($order_status_str, ",");

		$sql .=" AND f.status IN($order_status)";
	}else{
        $sql .=" AND f.status IN ('Q', 'WP')";
    }


	if(!empty($customer_name))
	{
		//case insensitive pattern match
		//$uc_customer_name = strtoupper($customer_name);
		$sql .=" AND f.b_firstname LIKE '%{$customer_name}%'";
	}
	if(!empty($product_type))
	{
		$sql .=" AND a.product_type = '{$product_type}'";
	}
    if(!empty($product_style))
	{
		$sql .=" AND a.product_style = '{$product_style}'";
	}
    if(!empty($from_date) && !empty($to_date))
	{
        $from_date_break = explode("/",$from_date);
        $from_date = $from_date_break[2]."-".$from_date_break[0]."-".$from_date_break[1];
        //$from_date_timestamp = mktime(0, 0, 0, $from_date_break[0], $from_date_break[1], $from_date_break[2]);

        $to_date_break = explode("/",$to_date);
        $to_date = $to_date_break[2]."-".$to_date_break[0]."-".$to_date_break[1];
        //$to_date_timestamp = mktime(0, 0, 0, $to_date_break[0], $to_date_break[1], $to_date_break[2]);

		//$sql .=" AND (f.queueddate >= '$from_date_timestamp' AND f.queueddate <= '$to_date_timestamp')";
        $sql .= " AND (FROM_UNIXTIME(f.queueddate,'%Y-%m-%d') >= '$from_date' AND FROM_UNIXTIME(f.queueddate,'%Y-%m-%d') <= '$to_date')";
	}
    
	# Section to prevent non online orders from showing here
	//$sql .=" AND f.source_id='1'";
	

	
	// In case payment_method=="", then we need to show both COD and non-COD reports.
    // Note: We remove Junk COD orders in this condition below. i.e those which are not mobile verified as yet. Those junk orders have f.cod_pay_status=''
    $sql = $sql. " and ((f.payment_method='cod' and f.cod_pay_status!='') or f.payment_method!='cod') " ;
	
	if($payment_method == "cod") {
		// this is added by vaibhav to generate a separate cod assignment report
    	$sql = $sql." and f.payment_method= '$payment_method'";
    } else if($payment_method == "noncod") {
    	$sql = $sql. " and f.payment_method != 'cod' " ;
    }
    if($is_customizable == '0') {
		// this is added by vaibhav to generate a separate cod assignment report
    	$sql = $sql." and a.is_customizable= '0'";
    } else if($is_customizable == "1") {
    	$sql = $sql." and a.is_customizable= '1'";
    }

    
	if($order_by == 'product_style')
	{
		$sql .=" ORDER BY d.name";
	}
	elseif($order_by == 'item_status')
	{
		$sql .=" ORDER BY a.item_status";
	}
	elseif($order_by == 'location')
	{
		$sql .=" ORDER BY g.location_name";
	}
	elseif($order_by == 'assignee')
	{
		$sql .=" ORDER BY b.name";
	}
	elseif($order_by == 'product_type')
	{
		$sql .=" ORDER BY product_type_name";
	}
	else
	{
		$sql .=" ORDER BY a.orderid";
	}
	if($sort_order == 'desc')
	{
		$sql .=" DESC";
	}
	else
	{
		$sql .=" ASC";
	}
	
	return $sql;
}


function func_load_assignee_task_list_count($assigneeid)
{
	global $sql_tbl;

	return $sql = "SELECT count(*)
	FROM $sql_tbl[order_details] a
	INNER JOIN $sql_tbl[mk_product_style] d ON a.product_style = d.id
	INNER JOIN $sql_tbl[orders] f ON a.orderid = f.orderid
	LEFT JOIN $sql_tbl[operation_location] g ON a.location = g.id
	WHERE (f.status = 'WP' or f.status = 'OH')
	AND (a.item_status = 'A' OR a.item_status = 'S')
	AND a.assignee = '$assigneeid'";
}


function func_load_assignee_task_list($assigneeid,$sort_by="",$sort_order="")
{    
	global $sql_tbl;

	$sql = "SELECT a.itemid, a.orderid,a.productid, a.quantity_breakup, a.item_status , a.amount, d.name AS stylename, f.order_name,f.login,
	FROM_UNIXTIME(a.assignment_time, '%d-%m-%Y::%H:%i:%s') AS ass_date, g.location_name, a.estimated_processing_date as estimated_date,a.is_customizable,a.product_style
	FROM $sql_tbl[order_details] a
	INNER JOIN $sql_tbl[mk_product_style] d ON a.product_style = d.id
	INNER JOIN $sql_tbl[orders] f ON a.orderid = f.orderid
	LEFT JOIN $sql_tbl[operation_location] g ON a.location = g.id 
	WHERE (f.status = 'WP' or f.status = 'OH')
	AND (a.item_status = 'A' OR a.item_status = 'S')
	AND a.assignee = '$assigneeid'";
    if($sort_by != ""){
       $sql.=" order by $sort_by";
       if($sort_order != "")
          $sql.=" $sort_order";
     }
     else $sql.=" ORDER BY a.orderid ASC";
    return $sql;
}

function func_load_location_task_list($locationid)
{
	global $sql_tbl;
	
	return $sql = "SELECT a.itemid, a.orderid,a.productid, a.item_status , a.amount, d.name AS stylename,f.order_name,
	FROM_UNIXTIME(a.assignment_time, '%d-%m-%Y::%H:%i:%s') AS ass_date,FROM_UNIXTIME(a.sent_date, '%d-%m-%Y::%H:%i:%s') AS sent_date, g.name
	FROM $sql_tbl[order_details] a
	INNER JOIN $sql_tbl[mk_product_style] d ON a.product_style = d.id
	INNER JOIN $sql_tbl[orders] f ON a.orderid=f.orderid 
	LEFT JOIN $sql_tbl[mk_assignee] g ON a.assignee = g.id 
	WHERE (f.status = 'WP')
	AND (a.item_status = 'A' OR a.item_status = 'S') 
	AND a.location = '$locationid'";
}

function func_load_location_assigned_items_details($locationid)
{
	global $sql_tbl;

	$sql = "select sum(a.amount) as total_quantity, a.product_style as style_id, a.product_type as type_id, b.label AS p_type, c.label  AS p_style
           from $sql_tbl[order_details] a
           INNER JOIN $sql_tbl[mk_product_type] b ON a.product_type = b.id
           INNER JOIN $sql_tbl[mk_product_style] c ON a.product_style = c.id
           where a.location='$locationid' and a.item_status='A' group by a.product_style order by a.product_style ";

    //$query_result = func_query($sql);
    return $sql;
}

function func_load_all_items_pending_QA($sort_by,$sort_order,$itemid,$orderid){
    global $sql_tbl;

    $where_clause="";

    if($itemid!="")
       $where_clause=$where_clause." and itemid= $itemid ";

    if($orderid!="")
       $where_clause=$where_clause." and orderid= $orderid ";

    /*$sql = "select itemid,orderid,productid,quantity_breakup,extra_data,ps.name,a.name as assigneeName,amount,pt.group_id as group_id,xod.is_customizable,xod.product_style, sp.article_number as article_number
            from xcart_order_details xod,mk_assignee a,mk_product_type pt,mk_product_style ps LEFT JOIN mk_style_properties sp on (sp.style_id = ps.id)
            where pt.id=xod.product_type and item_status='OD' and a.id=assignee and ps.id=product_style
            {$where_clause}
            order by $sort_by $sort_order";*/
            
    $sql = "select itemid, orderid, productid, quantity_breakup, extra_data, ps.name, a.name as assigneeName, amount, xod.is_customizable, xod.product_style, sp.article_number as article_number 
    		FROM (((( xcart_order_details xod
 				LEFT JOIN mk_assignee a on xod.assignee = a.id)
 				LEFT JOIN mk_product_type pt on pt.id = xod.product_type)
 				LEFT JOIN mk_product_style ps on xod.product_style = ps.id)
 				LEFT JOIN mk_style_properties sp on sp.style_id = xod.product_style)
 				where xod.item_status='OD' $where_clause order by $sort_by $sort_order";
	return $sql;
}

function func_load_all_queued_orders($operations_location='1'){
    global $sql_tbl;

	$sql = "select a.orderid, b.itemid, b.amount as total_quantity, b.product_style as style_id, b.quantity_breakup, b.product_type as type_id,
            c.label AS p_type, d.label AS p_style,FROM_UNIXTIME(a.queueddate, '%d-%m-%Y::%H:%i:%s') AS queueddate
            from $sql_tbl[orders] a
            LEFT JOIN $sql_tbl[order_details] b ON a.orderid = b.orderid
            LEFT JOIN $sql_tbl[mk_product_type] c ON b.product_type = c.id
            LEFT JOIN $sql_tbl[mk_product_style] d ON b.product_style = d.id
           	WHERE a.status='Q' and a.orgid!='1'";

	if(!empty($operations_location)){
		$sql .= " and a.operations_location='$operations_location'";
	}

    $sql .= " order by a.orderid DESC";

    return $sql;
}

function force_download($file, $dir)
{
    if ((isset($file))&&(file_exists($dir.$file))) {
       header("Content-type: application/force-download");
       header('Content-Disposition: inline; filename="' . $dir.$file . '"');
       header("Content-Transfer-Encoding: Binary");
       header("Content-length: ".filesize($dir.$file));
       header('Content-Type: application/octet-stream');
       header('Content-Disposition: attachment; filename="' . $file . '"');
       readfile("$dir$file");
    } else {
       echo "No file selected";
    } //end if

}//end function

function func_get_items_for_order($orderid, $warehouseId) {
	global $sql_tbl, $sqllog, $xcart_dir;
	$query = "select xo.orderid, xo.status, xo.warehouseid, xod.quantity_breakup, xod.itemid, xod.item_status, xod.amount as totalQty,  
			xod.price, xod.total_amount, xod.discount, xod.coupon_discount_product, xod.pg_discount, xod.difference_refund, xod.taxamount, xod.product_style,
			xod.qapass_items,xod.packaging_type,xod.is_packaged, sp.default_image, sp.article_number, sp.product_display_name, som.sku_id, po.value as size 
    		FROM (((( ( $sql_tbl[orders] xo 
    			LEFT JOIN $sql_tbl[order_details] xod on xo.orderid = xod.orderid)
 				LEFT JOIN $sql_tbl[mk_order_item_option_quantity] oq on xod.itemid = oq.itemid)
 				LEFT JOIN $sql_tbl[mk_styles_options_skus_mapping] som on som.option_id = oq.optionid)
 				LEFT JOIN $sql_tbl[style_properties] sp on som.style_id = sp.style_id)
 				LEFT JOIN mk_product_options po on po.id = oq.optionid)		
 				where xod.item_status in ('A', 'RP', 'QD') and xo.orderid = $orderid order by xod.packaging_type desc";
	
	$sqllog->info("SQl to get Order items - $query");
	$orderItemsDetails = func_query($query);
	
	include_once("$xcart_dir/modules/apiclient/ItemApiClient.php");
	$coreItems = ItemApiClient::get_skuitems_for_order($orderid, $warehouseId);
	
	$skuId2CodeMap = array();
	$skuId2ItemBarCodesMap = array();
	foreach($coreItems as $row) {
		if(!isset($skuId2ItemBarCodesMap[$row['sku']['id']]))
			$skuId2ItemBarCodesMap[$row['sku']['id']] = array();
		
		if(!isset($skuId2CodeMap[$row['sku']['id']]))
			$skuId2CodeMap[$row['sku']['id']] = $row['sku']['code'];
			
		$skuId2ItemBarCodesMap[$row['sku']['id']][] = $row['barcode'];
	}
	
	
	$orderItems = array('A'=>array(), 'QD'=>array(),'PREMIUM'=>'N');
	foreach ($orderItemsDetails as $order_item) {
		$skuItemBarCodes = $skuId2ItemBarCodesMap[$order_item['sku_id']];
		$order_item['issued_item_barcodes'] =  implode(",", $skuItemBarCodes);
		$order_item['issuedQty'] = count($skuItemBarCodes);
		$qapass_items = explode(",", $order_item['qapass_items']);
		$noOfQAPassItems = 0;
		foreach ($qapass_items as $itembarcode) {
			if($itembarcode && $itembarcode != "") {
				$noOfQAPassItems++;
			}
		}
		$order_item['sku_code'] = $skuId2CodeMap[$order_item['sku_id']];
		$order_item['qapass_items_count'] = $noOfQAPassItems;
		$order_item['display_image'] = str_replace("_images", "_images_96_128", $order_item['default_image']);
		$order_item['default_image'] = str_replace("_images", "_images_mini", $order_item['default_image']);
		
		$productAndCourierDetails = func_query_first("SELECT xo.courier_service, xod.product_style, 
                xod.is_customizable, xod.extra_data, sp.global_attr_article_type, 
				sp.global_attr_master_category, sp.global_attr_sub_category, sp.global_attr_brand 
				FROM $sql_tbl[orders] xo, $sql_tbl[order_details] xod, mk_style_properties sp 
				where xod.itemid = $order_item[itemid] and xo.orderid = xod.orderid and sp.style_id = xod.product_style", true);
		
		// pick out myntra private brands
		$privateBrands = json_decode(trim(FeatureGateKeyValuePairs::getFeatureGateValueForKey('myntra.private.brands')), true);
		$order_item['special_packaging'] = in_array($productAndCourierDetails['global_attr_brand'], $privateBrands) ? 'Y' : 'N';
		
		// check if Deo Orders are assigned to BD
		$order_item['deos_to_BD'] = 
				($productAndCourierDetails['courier_service'] == 'BD' && $productAndCourierDetails['global_attr_sub_category'] == 35) ? 'Y' : 'N';
        
        // check if item has a customized message
        //$order_item['customized_message'] = $productAndCourierDetails['is_customizable'] == 1 ? $productAndCourierDetails['extra_data'] : '';
        //$order_item['customized_message'] = $productAndCourierDetails['extra_data'] == ''? : '';
        /* NO LONGER USED
        if($order_item['customized_message'] != ''){
            $order_item['customized_message_parts'] = explode(":", $order_item['customized_message']);
        }
        */
                if($productAndCourierDetails['extra_data'] != ''){
                        $order_item['customized_message'] = json_decode(html_entity_decode($productAndCourierDetails['extra_data']),true);
                }

		
		if($order_item['item_status'] == 'A' || $order_item['item_status'] == 'RP') {
			$orderItems['A'][] = $order_item;
		} else {
			$orderItems['QD'][] = $order_item;
		}
                if($order_item['packaging_type'] == 'PREMIUM' && $orderItems['PREMIUM'] == 'N'){
                    $orderItems['PREMIUM'] = 'Y';
                 }
	}
	
	return $orderItems;
}

function func_mark_core_item_qa_pass($request, $qcReason) {
    global $sql_tbl, $sqllog;
    
    $itemid = $request['itemid'];
    $skuItemBarCodes = explode("\n", $request['skuitemcodes']);
    
    $qa_pass_items = $request['processed_items']?$request['processed_items']:"";
    $issued_items = $request['valid_items']?$request['valid_items']:"";
    
    $errorMsg = "";
    $qa_time=time();
    $orderid = $request['orderid'];
    $user = $request['userid'];
	foreach ($skuItemBarCodes as $skuItemCode) {
		$skuItemCode = trim($skuItemCode);
		
		$item_issued = in_array($skuItemCode, explode(",", $issued_items))===false?false:true;
		
		if($skuItemCode != "" && $item_issued) {
			$already_processed = in_array($skuItemCode, explode(",", $qa_pass_items))===false?false:true;
			if(!$already_processed) {			
				$description = "Item Code: $skuItemCode\nAction: QA Pass\n Reason: ".$qcReason['rejectReasonDescription'];
				
		    	func_addComment_order($orderid, $user, 'Note', 'QA Pass', $description);
		    	
			    if($qa_pass_items != "")
			    	$qa_pass_items .= ",";
		   	
		    	$qa_pass_items .= $skuItemCode;
		    	
			} else {
				$errorMsg .= "Item $skuItemCode has already been processed for this order<br>";
			}
		} else {
			if($skuItemCode != "")
				$errorMsg .= "Item $skuItemCode has not been issued for this order<br>";
		}
	}
	
    $sql = "UPDATE $sql_tbl[order_details] SET qapass_items = '$qa_pass_items' WHERE itemid=$itemid";
    $sqllog->info("Execute update query on item - $sql");
    db_query($sql);
    
    return $errorMsg;
}

function func_mark_core_item_qa_fail($request, $qcReason) {
	global $xcart_dir, $sql_tbl, $sqllog;
    include_once("$xcart_dir/modules/apiclient/ItemApiClient.php");
    
    $itemid = $request['itemid'];
    $qa_time=time();
    $warehouseid = $request['warehouseId'];
    $orderid = $request['orderid'];
    $user = $request['userid'];
    $skuItemBarCodes = explode("\n", $request['skuitemcodes']);
    
    $qa_pass_items = $request['processed_items']?$request['processed_items']:"";
    $issued_items = $request['valid_items']?$request['valid_items']:"";
    
    $failureMsg = "";
    foreach ($skuItemBarCodes as $skuItemCode) {
    	$skuItemCode = trim($skuItemCode);
    	
		$already_processed = in_array($skuItemCode, explode(",", $qa_pass_items))===false?false:true;
		$item_issued = in_array($skuItemCode, explode(",", $issued_items))===false?false:true;
    	
    	if($skuItemCode != "") {
	  		if($item_issued && !$already_processed) {
	  			$response = ItemApiClient::remove_item_order_association($orderid, $warehouseid, $skuItemCode, 'RETURN_FROM_OPS', $qcReason['quality'], $user, $qcReason['rejectReason'], $qcReason['rejectReasonDescription']);
    			if($response === true) {
    				func_array2update('order_details', array('item_status'=>'RP'), "itemid = $itemid");
			    	$description = "Item Code - $skuItemCode\nAction: QA Reject\nReason: $qcReason[rejectReason].\nDescription: $qcReason[rejectReasonDescription].\nQuality: $qcReason[quality]";
			    	EventCreationManager::pushItemUpdateEvent($itemid, 'RP', $description, time(), $user);
			    	// Call to decrease release line picked quantity by 1
			    	func_addComment_order($orderid, $user, 'Note', 'QA Fail', $description);
    			} else {
    				$failureMsg .= $response."\n";
    			}
    	  	} else {
    			if(!$item_issued)
    				$failureMsg .= "Item $skuItemCode has not been issued for this order.\n";
    			if($already_processed)
    				$failureMsg .= "Item $skuItemCode has already been processed for this order.\n";
    	  	}
		}
    }
    
    return $failureMsg;
}

function func_mark_item_premium_packaged($orderid, $itemid, $userid){
	global $xcart_dir, $weblog;
        
        $sql = "UPDATE xcart_order_details SET is_packaged = 1 WHERE itemid=$itemid";
        $result = db_query($sql);
   
            
        if($result == 1){
            $commentdetails = "Status for Item Id $itemid changed to Premium Packaged";
            EventCreationManager::pushItemPackagedUpdateEvent($itemid, 'QD','PREMIUM','PREMIUM_PACKAGED', $commentdetails, time(), $userid);
            func_addComment_order($orderid, $userid, 'Note', "Status for Item Id $itemid changed to Premium Packaged", $commentdetails);
            
        }
        return $result;
   
}

function func_mark_item_qadone($orderid, $itemid, $qapass_itembarcodes, $userid, $warehouseid) {
	global $xcart_dir, $weblog;
    include_once("$xcart_dir/modules/apiclient/LocationApiClient.php");
    
	$qabin_barcode = WidgetKeyValuePairs::getWidgetValueForKey("wms.qabin.".$warehouseid.".barcode");
	$qabin_randomcode = WidgetKeyValuePairs::getWidgetValueForKey("wms.qabin.".$warehouseid.".randomcode");
	
	$response = LocationApiClient::moveItemsToBin(explode(",", $qapass_itembarcodes), $warehouseid, $qabin_barcode, $qabin_randomcode, $userid);
	if($response == "") {
		$old_item_status = func_query_column("Select item_status from xcart_order_details where itemid = $itemid");
		func_change_item_status($itemid, 'QD', $old_item_status);
			
		$commentdetails = "Status for item $itemid changed from";
		if($old_item_status == 'RP') {
			$commentdetails .= " RePick ";
		}else {
			$commentdetails .= " Assigned ";
		}
		$commentdetails .= "to QA Done";
		EventCreationManager::pushItemUpdateEvent($itemid, 'QD', $commentdetails, time(), $userid);
		func_addComment_order($orderid, $userid, 'Note', 'Item Status Change', $commentdetails);
	} else {
		return $response;
	}
	return "";
}	

?>
