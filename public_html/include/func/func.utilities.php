<?php
require_once (dirname(__FILE__) . "/../../AMAZONS3/util/S3.php");
require_once (dirname(__FILE__) . "/../../env/System.config.php");
require_once (dirname(__FILE__) . "/../../env/Host.config.php");
include_once (dirname(__FILE__) . "/../../Cache/Redis.php");
/**
 * Created by IntelliJ IDEA.
 * User: Mohammed Asif Hussain
 * Date: Dec 15, 2009
 * Time: 12:47:57 PM
 * To change this template use File | Settings | File Templates.
 */

/**
 * 
 */
function getColorAttributeTypeFamily(){
	$sql = "select family_name, swatch_position from attribute_type_family";
	return func_query($sql);
}

/**
 * @param  $number
 * @param boolean $urldecode
 * @return #Fexplode|#Furldecode|?
 */
function segment($number, $url_decode = FALSE) {
    $url = $_SERVER['REQUEST_URI'];
    $segments = explode("/", $url);
    $segment = isset($segments[$number]) ? $segments[$number] : NULL;
    if ($url_decode) {
        $segment = urldecode($segment);
    }
    return $segment;
}

/**
 * @param  $content
 * @return #Farray_search|?
 */
function segment_contains($content) {
    $url = $_SERVER['REQUEST_URI'];
    $segments = explode("/", $url);
    return array_search($content, $segments);
}

/**
 * @param  $name
 * @param  $default
 * @return #Fstrip_tags|?
 */
function get_var($name, $default = NULL) {
    if (is_post()) {
        if (!isset($_POST[$name])) {
            return $default;
        }
        return filter_var(strip_tags(urldecode($_POST[$name])),FILTER_SANITIZE_STRING);
    } else  if (is_get()) {
        if (!isset($_GET[$name])) {
            return $default;
        }
        return filter_var(strip_tags(urldecode($_GET[$name])),FILTER_SANITIZE_STRING);
    }
    
    return $default;
}

/**
 * @return
 */
function is_post() {
    return isset($_POST) && !empty($_POST);
}

/**
 * @return
 */
function is_get() {
    return isset($_GET) && !empty($_GET);
    ;
}

/**
 * @param  $value
 * @return #Fpreg_replace|?
 */
function space_to_hyphen($value) {
    return preg_replace('#([ ])+#', '-', $value);
}

/**
 * @param  $value
 * @return #Fpreg_replace|?
 */
function hyphen_to_space($value) {
    return preg_replace('#([-])+#', ' ', $value);
}

/**
 * @param  $value
 * @return #Fpreg_replace|?
 */
function space_to_nothing($value) {
    return preg_replace('#([ ])+#', '', $value);
}

/**
 * Checks if a String is strictly ASCII: every character is ASCII
 * @param string $str
 * @return string
 */
function isAscii($str){
	return mb_detect_encoding($str,'ASCII',true);
}

/**
 * @param  $phrase
 * @param int $limit
 * @param int $dots
 * @return #Fsubstr|string|?
 */
function truncate($phrase, $limit = 21, $dots = 3) {
    if (strlen($phrase) >= $limit) {
        $phrase = substr($phrase, 0, $limit - $dots);
        for ($i = 0; $i < $dots; $i++) {
            $phrase .= '.';
        }
    }
    return $phrase;
}

/**
 * @param  $a
 * @param  $b
 * @return int
 */
function _sort_by_length_callback($a, $b) {
    return strlen($b) - strlen($a);
}

/**
 * @param  $array
 * @return
 */
function sort_by_length($array) {
    usort($array, '_sort_by_length_callback');
    return $array;
}

/**
 * Trim all the elements of the array ..
 * @param  $array
 * @return
 */
function trim_all($array) {
    foreach ($array as $key => $value) {
        $array[$key] = trim($value);
    }
    return $array;
}

/**
 * Trim all the elements of the array ..
 * @param  $array
 * @return
 */
function construct_style_url($articletype,$brandname,$stylename,$styleid) {
   
	// The stylename in url should contain only alphanumeric characters and some special characters
	// The stylename in url should have spaces replaced by '-' character
	$stylename = preg_replace("/[^a-zA-Z0-9\s\-]/", "", $stylename);
	$stylename = preg_replace("/[\s-]+/","-",$stylename);
	// We need to limit stylename to 50 characters in URL construction.
	$stylename = substr($stylename, 0, 50);
	// Same properties apply for article type
	$articletype = preg_replace("/[^a-zA-Z0-9\s\-]/", "", $articletype);
	$articletype = preg_replace("/[\s-]+/","-",$articletype);
	// Same properties apply for brand name type
	$brandname = preg_replace("/[^a-zA-Z0-9\s\-]/", "", $brandname);
	$brandname = preg_replace("/[\s-]+/","-",$brandname);
	// The new URL format should be : http://<domainname>/<articleType>/<brandname>/<stylename>/<styleid>/buy
    // The URL should be in lower case.
    $landingpageurl = "$articletype/$brandname/$stylename/$styleid/buy";
    $landingpageurl = strtolower($landingpageurl);
			
    return $landingpageurl;
}

/**
 * @param  $link
 * @param  $save_to
 * @return void
 */
function get_file($link, $save_to) {
    $ch = curl_init($link);
    $fp = fopen($save_to, "w");

    // set URL and other appropriate options
    $options = array(CURLOPT_FILE => $fp,
        CURLOPT_HEADER => 0,
        CURLOPT_FOLLOWLOCATION => 1,
        CURLOPT_TIMEOUT => 60); // 1 minute timeout (should be enough)

    curl_setopt_array($ch, $options);

    $status = curl_exec($ch);
    curl_close($ch);
    fclose($fp);
    return $status;
}

/**
 * Function to check if the resource exists on the passed url. It will retrieve the http headers for the resouse and return
 * @param string $url fully qualified url of the resource 
 * @return boolean true if the url exists else will return false 
 */
function url_exists($url) {
	$hdrs = @get_headers($url);
	return is_array($hdrs) ? preg_match('/^HTTP\\/\\d+\\.\\d+\\s+2\\d\\d\\s+.*$/',$hdrs[0]) : false;
}

/**
 * Function to move a file from current location to S3 bucket
 * Note that file should not contain any space or special chars (hyphen & underscore are safe)
 * @param string  $file the location of file on disk
 * @param string $bucket the S3 bucket where the resource needs to be uploaded in S3
 * @param  string $uri the uri where the resource needs to be placed in S3
 * @param string $contentType the content type of the resource
 * @param int $age the max-age header value that needs to be set
 * @return true in case transfer is successfull else false
 */

function moveToS3($file, $bucket, $uri,$contentType=null, $age=2592000) {
	if(!defined(AMAZON_S3_URL))
    	define("AMAZON_S3_URL", "http://s3.amazonaws.com");
    $url = "http://$bucket.s3.amazonaws.com/$uri";
    $s3 = new S3(SystemConfig::$amazonAccessKey, SystemConfig::$amazonSecretKey);
    $res = $s3->putObjectFile(
        $file,
        $bucket,
        $uri,
        S3::ACL_PUBLIC_READ,
        array(),
        $contentType,
        $age);
    return $res && url_exists($url);
}

function string_begins_with($string, $search) {
	return (strncmp($string, $search, strlen($search)) == 0);
}
/**
 * Function to move the resource to the default bucket
 *  Note that file should not contain any space or special chars (hyphen & underscore are safe)
 * @param String $path the location of the resource on disk.
 * @return boolean In case the transfer is successfull else false
 */
function moveToDefaultS3Bucket($path){
	global $weblog;
	$basePath = HostConfig::$documentRoot;
	$uri = $path;
	if (string_begins_with($uri , '../images'))
		$uri = substr($uri, 3);
	if (string_begins_with($uri , './images'))
		$uri = substr($uri, 2);
	if (string_begins_with($uri , '/images'))
		$uri = substr($uri, 1);
	if (string_begins_with($uri , HostConfig::$cdnBase.'/images'))
		$uri = substr($uri, strlen(HostConfig::$cdnBase));
	if (string_begins_with($uri , $basePath))
		$uri = str_replace($basePath.'/', '', $uri);
	$weblog->debug("Moving file : $basePath/$uri to bucket : ".SystemConfig::$amazonS3Bucket. " at uri : $uri");
	return moveToS3("$basePath/$uri", SystemConfig::$amazonS3Bucket, $uri);
}

/**
 * Function to move image assets to cloudinary
 * @param $filepath string path of the file on local disk to be transfered to cloudinary.
 * @param $uri relative url of the public location of the image on cloudinary.
 * @param uploadOptions array to be provided to cloudinary for upload.
 * @param eagerParams array to the provided to cloudinary for upload.
 */
function moveToCloudinary($filepath, $uri, $uploadOptions=array(), $eagerParams=array()){
    # Eager transformations are applied as soon as the file is uploaded, instead of waiting
    # for a user to request them. 
    if(empty($uploadOptions)){
        $uploadOptions = array();
    }
    if(empty($eagerParams)){
        $eagerParams = array();
    }
    //Cloudinary by default appends .jpg
    $uri = str_replace(".jpg", "", $uri);
    $uri = str_replace(".JPG", "", $uri);
    $result = \Cloudinary\Uploader::upload($filepath,
        array_merge($uploadOptions, array(
            "public_id" => $uri,
            "eager" => $eagerParams,
        )
    ));
    return isset($result)&& !empty($result);

}

function currentPageURL() {
    $pageURL = 'http';
    if ($_SERVER["HTTPS"] == "on") {
        $pageURL .= "s";
    }
    $pageURL .= "://";
    if ($_SERVER["SERVER_PORT"] != "80") {
        $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . filter_var($_SERVER["REQUEST_URI"],FILTER_SANITIZE_STRING);
    } else {
        $pageURL .= $_SERVER["SERVER_NAME"] . filter_var($_SERVER["REQUEST_URI"],FILTER_SANITIZE_STRING);
    }
    return $pageURL;
}

function currentPageURLWithStripedQueryStrings() {
	$pageURL = currentPageURL();
        if (false === ($pos = strpos($pageURL,'?'))) {
                return $pageURL;
        } else {
                return substr($pageURL,0, $pos);
        }
}

function currentPageURLWithStripedQueryStringsAndPort() {
   $pageURL = 'http';
    if ($_SERVER["HTTPS"] == "on") {
        $pageURL .= "s";
    }
    $pageURL .= "://";
    $pageURL .= $_SERVER["SERVER_NAME"] .filter_var($_SERVER["REQUEST_URI"],FILTER_SANITIZE_STRING);
    if (false === ($pos = strpos($pageURL,'?'))) {
            return $pageURL;
    } else {
            return substr($pageURL,0, $pos);
    }
}

function sanitizeDBValues($data){
	if(!is_array($data)){
		return mysql_real_escape_string($data);
	} else {
		foreach ($data as $key=>$value){
			$data[$key] = mysql_real_escape_string($value);
		}
		return $data;
	}
}

/**
* Given an input associative array, it replaces a key with another key
* as per the $replaceArray. Each of the keys in $replaceArray is what is replaced in $origArray
* with the corresponding value
* @param unknown_type $origArray
* @param unknown_type $replaceArray
*/
function replaceKeys($origArray, $replaceArray){
	foreach ($replaceArray as $key=>$newKey) {
		$origValue = $origArray[$key];
		unset($origArray[$key]);
		$origArray[$newKey] = $origValue;
	}
	return $origArray;
}
/**
 * Implode a field of Indexed-cum-associative array
 * @param unknown_type $arrayOfAssocs: this is an indexed array, where each index in itself is an associative array
 * @param unknown_type $key: this is the key in associative array, whose value needs to be inserted in the final string
 * @param unknown_type $glue: this is the delimiter: the glue to be used to separate multiple values
 * @author Kundan
 */
function implodeArrayOfAssocs($arrayOfAssocs,$key,$glue=","){
	$concatStr = "";
	for($i=0;$i<sizeof($arrayOfAssocs); $i++){
		if($i>0){
			$concatStr .= $glue;
		}
		$concatStr.=$arrayOfAssocs[$i][$key];
	}
	return $concatStr;
}

/**
 * 
 *  
 * @param $valueArr: could be an array containing value and type as keys, or just value as scalar
 */
function conditionallyEncloseInQuotes($valueArr) {
	$type = "string";
	$value = null;
	if(is_array($valueArr)) {
		if(isset($valueArr["value"])) {
			$value = $valueArr["value"];
		}
		if(isset($valueArr['type'])) {
			$type = $valueArr['type'];
		} 
	}
	else {
		$value = $valueArr;
	}

	$transformedValue = $value;
	switch ($type) {
		case "string": 
			$transformedValue = "'".mysql_real_escape_string($value)."'"; 
			break;
		case "json":
			$transformedValue = "'".$value."'";
			break;
		default:
			$transformedValue = $value; 
	}
	return $transformedValue; 
}
/**
 * Ceil a negative number to zero
 * @param number $num
 */
function ceilToZero($num) {
	if($num < 0)
		$num = 0;
	return $num;	
}

/**
* Utility function to fire an asynchorous get request to a url
* @url The url to be hit
* @getReq String representation of the get request post the hostname in a url
* e.g. fireAsync("http://www.myntra.com", "/data?test=1")
*/
function fireAsync($url, $getReq){
        $handle = Profiler::startTiming("fire-async");
        $errStr = "";
        $errNo = 0;
        $timeout = 0.050;
        $server = parse_url($url);
        try
        {
            $fp = fsockopen($server['host'], $server['port'] ? $server['port'] : 80, $errNo, $errStr, $timeout);
            if(!$fp){
                    Profiler::increment("fire-async-failure");
            }
            else{
                    $out = "GET /".$getReq." HTTP/1.1\r\n";
                    $out .= "HOST: ".$server['host']."\r\n";
                    $out .= "User-Agent: Mozilla/5.0 Firefox/3.6.12\r\n";
                    $out .= "Connection: Close\r\n\r\n";
                    fwrite($fp, $out);
                    fclose($fp);
            }
        }
        catch(Exception $e)
        {
            Profiler::increment("fire-async-failure");
        }
        Profiler::endTiming($handle);
}

function post_async($url, $params)
{
	foreach ($params as $key => &$val) {
		if (is_array($val)) $val = implode(',', $val);
		$post_params[] = $key.'='.urlencode($val);
	}
	$post_string = implode('&', $post_params);

	$parts=parse_url($url);
	$profilerHandler = Profiler::startTiming("post-async");
	$fp = fsockopen($parts['host'],
	isset($parts['port'])?$parts['port']:80,
	$errno, $errstr, 30);

	$out = "POST ".$parts['path']." HTTP/1.1\r\n";
	$out.= "Host: ".$parts['host']."\r\n";
	$out.= "User-Agent: Mozilla/5.0 Firefox/3.6.12\r\n";
	$out.= "Content-Type: application/x-www-form-urlencoded\r\n";
	$out.= "Cookie: xyz=abc;\r\n";
	$out.= "Content-Length: ".strlen($post_string)."\r\n";
	$out.= "Connection: Close\r\n\r\n";
	
	if (isset($post_string)) $out.= $post_string;

	fwrite($fp, $out);
	fclose($fp);
	Profiler::endTiming($profilerHandler);
}
/**
* Method to convert an object to an array recursively
* @param $array an array or an object which is to be processed recursively
**/
function convertToArrayRecursive($array){
		if (is_object($array)) {
			// Gets the properties of the given object
			// with get_object_vars function
			$array = get_object_vars($array);
		}

		if (is_array($array)) {
			/*
			 * Return array converted to object
			* Using __FUNCTION__ (Magic constant)
			* for recursive call
			*/
			return array_map('convertToArrayRecursive', $array);
		}
		else {
			// Return array
			return $array;
		}
}

function func_copy_tmp_dir_to_images($tmpDir, $baseImagesDir, $s3BaseDir){
    global $weblog;
    global $errorlog;
    $dit = new RecursiveIteratorIterator(
                new RecursiveDirectoryIterator($tmpDir),
                RecursiveIteratorIterator::LEAVES_ONLY);
    while ($dit->valid()) {
        if ($dit->isDot() || is_dir($dit->getPathname()) || (!(strpos ($dit->getPathname(), "svn") === false))) {
            $dit->next();
        } else {
            $path = $dit->getPathName();
            $subPath = $dit->getSubPathName();
            if(!copy($path, "$baseImagesDir/$subPath") || !moveToDefaultS3Bucket("$s3BaseDir/$subPath")){
                $errorlog->error("Error while copying temporary files : $path :  back to images folder at : $baseImagesDir/$subPath and moving to S3");
                $weblog->error("Error while copying temporary files : $path :  back to images folder at : $baseImagesDir/$subPath and moving to S3");
                return false;
            }
            $dit->next();
        }
    }
    return true;
}

/** Function to check if the IP is in valid external IP range.
* I stores the private IP ranges in an array and marks any IP lying in the
* private IP range as invalid
* @param $ip string representation of the the client IP in 4 octets
* @return boolean true in case IP is valid external and false if the IP is in private IP range
*/
function is_ip_valid($ip) {
    $longip = ip2long($ip);
    if (!empty($ip) && ip2long($ip)!=-1 && ip2long($ip)!=false) {
        $private_ips = array (
            array('0','50331647'),           // array('0.0.0.0','2.255.255.255'),
            array('167772160','184549375'),  // array('10.0.0.0','10.255.255.255'),
            array('2130706432','2147483647'),// array('127.0.0.0','127.255.255.255'),
            array('2851995648','2852061183'),// array('169.254.0.0','169.254.255.255'),
            array('2886729728','2887778303'),// array('172.16.0.0','172.31.255.255'),
            array('3221225984','3221226239'),// array('192.0.2.0','192.0.2.255'),
            array('3232235520','3232301055'),// array('192.168.0.0','192.168.255.255'),
            array('4294967040','4294967295') // array('255.255.255.0','255.255.255.255')
           );

       foreach ($private_ips as $r) {
           $min = $r[0];
           $max = $r[1];
           if (($longip >= $min) && ($longip <= $max)){
               return false;
           }
       }
       return true;
   } else {
       return false;
   }
}

/**
* Function to retrieve the users real IP by resolving accross various PHP headers
* and filtering out the proxy IPs if any
*/
function get_user_ip(){
    if (is_ip_valid($_SERVER["HTTP_TRUE_CLIENT_IP"])) {
        return $_SERVER["HTTP_TRUE_CLIENT_IP"];
    }
    if (is_ip_valid($_SERVER["HTTP_CLIENT_IP"])) {
        return $_SERVER["HTTP_CLIENT_IP"];
    }
    if (is_ip_valid($_SERVER["HTTP_X_REAL_IP"])) {
        return $_SERVER["HTTP_X_REAL_IP"];
    }
    // Accessing IPs from left to right in XFF ip range
    foreach (explode(",",$_SERVER["HTTP_X_FORWARDED_FOR"]) as $ip) {
        if (is_ip_valid(trim($ip))) {
            return $ip;
        }
    }
    if (is_ip_valid($_SERVER["HTTP_X_FORWARDED"])) {
        return $_SERVER["HTTP_X_FORWARDED"];
    } elseif (is_ip_valid($_SERVER["HTTP_X_CLUSTER_CLIENT_IP"])) {
        return $_SERVER["HTTP_X_CLUSTER_CLIENT_IP"];
    } elseif (is_ip_valid($_SERVER["HTTP_FORWARDED_FOR"])) {
        return $_SERVER["HTTP_FORWARDED_FOR"];
    } elseif (is_ip_valid($_SERVER["HTTP_FORWARDED"])) {
        return $_SERVER["HTTP_FORWARDED"];
    } else {
        return $_SERVER["REMOTE_ADDR"];
    }
}

/**
* Function to remove empty values from an array. This is a sanity function
* which should be used in case where you want to remove keys associated with empty
* values. Removes null and empty string values only.
* Note: Does not remove 0 or false
**/
function func_remove_empty_values($arr){
    foreach($arr as $key=>$value){
        if(($value === null || trim($value) === '')){
            unset($arr[$key]);
        }
    }
    return $arr;
}
        
/**
 * 
 *  
 * @param $valueArr: could be an array containing value and type as keys, or just value as scalar
 */            
function generate_uuid(){
  /*  
    //return $arr;
    $val = sprintf('%04x%04x%',

      // 32 bits for "time_low"
      mt_rand(0, 0xffff), mt_rand(0, 0xffff)

     // 16 bits for "time_mid"
      mt_rand(0, 0xffff),

      // 16 bits for "time_hi_and_version",
      // four most significant bits holds version number 4
      mt_rand(0, 0x0fff) | 0x4000,

      // 16 bits, 8 bits for "clk_seq_hi_res",
      // 8 bits for "clk_seq_low",
      // two most significant bits holds zero and one for variant DCE1.1
      mt_rand(0, 0x3fff) | 0x8000,

      // 48 bits for "node"
      mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
    ).*/
    return microtime(true) * 10000  + mt_rand(0,99); //+ mt_rand(0, 99);

    //return hexdec($val);
}

function setUUIDAndUserCookie(){
    //Check For UUID And Remeber login here
    //get UUID cookie value. If empty then generate one with 6 months of expiry
    
    global $login,$UUID,$REMEMBER_LOGIN,$UUID_NAME,$REMEMBER_USER,$cookiedomain,$uuidcookiedomain;
    $uuidPrefix = "uuid-";
    
    $curtime = time();
    
    $uuidCookieValue = getMyntraCookie($UUID_NAME, false, '', false);
    if(strpos($uuidCookieValue, $uuidPrefix) !== false){
        $UUID = substr($uuidCookieValue, strlen($uuidPrefix));
    }
    else {
        $UUID = getMyntraCookie($UUID_NAME, false, '', true);
        if(empty($UUID)) $UUID = base64_decode(getMyntraCookie("utrid"));
    }

    $REMEMBER_LOGIN = getMyntraCookie($REMEMBER_USER, false, '', true);
    if(empty($REMEMBER_LOGIN))  $REMEMBER_LOGIN = base64_decode(getMyntraCookie("rem_id"));

    //Check for UUID and extend expiry else create a new cookie Also create a remeber cookie 
    
    $uuidExpiryDays = FeatureGateKeyValuePairs::getInteger("uuidExpiryDays", 180); // default 180days
    $uuid_expirytime = $curtime + ($uuidExpiryDays*24*60*60); 
    
    //deleting the "rem_id" and "xidC_remember" cookie
    setMyntraCookie("rem_id", "", $curtime - 3600, "/", $uuidcookiedomain, false, true); 
    setMyntraCookie("xidC_remember", "", $curtime - 3600, "/", $cookiedomain, false);

    if(empty($UUID)){
      //Set New Cookie
      //create a new uuid
      $UUID = generate_uuid();
    }
    
    //update expiry of UUID and Remeber Cookie
    $uuidNewVer = FeatureGateKeyValuePairs::getBoolean("uuidNewVer", false); // default 180days
    if($uuidNewVer){
        setMyntraCookie($UUID_NAME, "$uuidPrefix$UUID", $uuid_expirytime, "/", $uuidcookiedomain, false, true, true, false);
    }
    else {
        setMyntraCookie($UUID_NAME, $UUID, $uuid_expirytime, "/", $uuidcookiedomain, false, true, true, true);
    }
 
    if(!empty($login)){
      $REMEMBER_LOGIN = $login;
      setMyntraCookie($REMEMBER_USER, $login, $uuid_expirytime, "/", $cookiedomain, false, true, true, true);
    }

}

class XSessionVars {
	public static function get($varName, $defaultValue) {
		global $XCART_SESSION_VARS;
		return isset($XCART_SESSION_VARS[$varName]) ? $XCART_SESSION_VARS[$varName] : $defaultValue;
	}
	
	public static function getNumber($varName, $defaultValue = 0) {
		return self::get($varName, $defaultValue);
	}
	
	public static function getString($varName, $defaultValue = '') {
		return self::get($varName, $defaultValue);
	}
	
	public static function getObject($varName, $defaultValue = null) {
		return self::get($varName, $defaultValue);
	}
}

class RequestVars {
	
	public static function getGetVar($varName) {
		global $_GET;
		return $_GET[$varName];
	}
	
	public static function getPostVar($varName) {
		global $_POST;
		return $_POST[$varName];
	}
	
	public static function getVar($varName, $defaultValue = "", $escapeMode ="none", $filter=false) {
		global $_REQUEST;
		$value = $_REQUEST[$varName];
		if(!isset($value)) {
			$value = $defaultValue;
		}
		switch (strtolower($escapeMode)) {
			case "string": 
				$value = mysql_real_escape_string($value); 
				break;
			case "boolean":
				$value = (strtolower($value) == "true") ? true : false;  	
			default:
				;
			break;
		}
		return $value;
	}
}

class GlobalVars {
	public static function getVar($varName) {
		global $GLOBALS;
		return $GLOBALS[$varName];
	}
	public static function setVar($varName,$value) {
		global $GLOBALS;
		$GLOBALS[$varName] = $value;
	}
}

function genCSRFToken(){

}

function checkCSRF($user_token, $form = "", $action = ""){
  global $XCART_SESSION_VARS;
  if($user_token===$XCART_SESSION_VARS['USER_TOKEN']) return true;
  return false;
}

function showLoginCaptcha($userIP="###", $ip_attempt_limit = 200){
    try {

        Redis::init();
      
        $redisHandle = Redis::getRedisRO();

        $ip_attempts = $redisHandle->get('LOGIN_ATTEMPTS_IP:'.$userIP);

        if(!empty($ip_attempts) and $ip_attempts > $ip_attempt_limit)
            return true;
        
    } catch (EXception $e){

    }
    return false;
}

function setLoginCaptchaInfo($userIP = "##", $login_success = true, $ip_table_ttl = 1800){
    try{
        
        Redis::init(); 
        $redisRWHandle = Redis::getRedisRW();

        if(!$login_success){
          $redisRWHandle->incr('LOGIN_ATTEMPTS_IP:'.$userIP);
          $redisRWHandle->expire('LOGIN_ATTEMPTS_IP:'.$userIP, $ip_table_ttl);
        }

    } catch (EXception $e){

    }
}



/**
 * Replace all characters of string with maskChar except last strlen(string)/3 chars
 * @param string $stringToMast
 * @param char $maskChar
 * @return string maskedSting
 */
function maskString($stringToMask, $defaultMaskChar='*'){
	$length = strlen($stringToMask);
	$stringToMaskArray = str_split($stringToMask,1);	
	$numberOfCharsToShow = $length/3;
	
	for($i=0; $i<$length-$numberOfCharsToShow; $i++){		
		$stringToMaskArray[$i] = $defaultMaskChar;
	}	
	return implode('',$stringToMaskArray);	
}


?>
