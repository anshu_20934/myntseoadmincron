<?php

if (!defined('XCART_START'))
    {
    header ("Location: ../");
    die ("Access denied");
 }

#
#Function Name: func_check_values
#Return : This function return result array of fields
#Parameter: Three parameter 1. Comma seperated attribute, 2. Table names, 3. String after where claus 
#
function func_check_values($fields, $table, $claus)
{
	global $sqllog;
	$sql = "SELECT ".$fields." FROM ". $table ." WHERE ".$claus;
	$sqllog->debug("File Name::>func.refer.php##Function Name::>func_check_values()####SQL Error::>".$sql); 
	$result = func_query_first($sql);
	return $result;
}


#
#Function Name: func_check_valid_email
#Return : This function return an array of invalid email ids
#Parameter: An array of referred email
#
 function func_check_valid_email($emailarray)
 {
#
#Iterate email array and check that email ids are valid or invalid
#
	$wrongemail = "";
	$validemail = array();
	foreach($emailarray AS $key=>$value)
	{
		$email = trim($value);
		if(!eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $email))
		{
		   $wrongemail = $wrongemail.",".$value;
		} 
	}
	$wrongemail = trim($wrongemail, ",");

	return $wrongemail;
 }

#
#Function Name: func_check_refer_same
#Purpose : Function to check is login user trying to refer himself/herself
#Return : Email id
#Parameter: 1. Array of referred email, 2. Login id, 3. Referred id
#
function func_check_refer_same($emailarray,$login,$refid)
{
	global $sql_tbl;
	global $sqllog;
	if(is_array($emailarray))
	{
		$existemail = "";
		foreach($emailarray AS $key=>$value)
		{
			if(!empty($value))
			{
				$email = trim($value);
				if($email == $login)
				{
					$existemail = $existemail.", ".$email;
				}
			}
		}
	}
		return $existemail;
}

#
#Function Name: func_check_registered_email
#Purpose : Function to check is referred email already registered
#Return : array of registered Email ids
#Parameter: 1. Array of referred email, 2. Login id, 3. Referred id
#
function func_check_registered_email($emailarray,$login,$refid)
{
	global $sql_tbl;
	global $sqllog;
	if(is_array($emailarray))
	{
		$existemail = "";
		foreach($emailarray AS $key=>$value)
		{
			if(!empty($value))
			{
				$email = trim($value);
				$sql = "SELECT login FROM $sql_tbl[customers] WHERE login ='$email'";
				$registerd = func_query_first($sql);
				$sqllog->debug("File Name::>"."func.refer.php"."##Function Name::>func_check_duplicate_email()##"."##SQL Error::>".$sql); 
				
				if(!$registerd['login'])
				{
					$existemail = $existemail.", ".$email;
				}
			}
		}
	}
		return $existemail;
}


#
#Function Name: func_check_duplicate_email
#Return : This function return an array of duplicate email ids
#Parameter: An array of referred email
#
function func_check_duplicate_email($emailstring,$login,$refid)
{
	global $sql_tbl;
	global $sqllog;
	$existemail = "";

	$emailarray = explode(",", $emailstring);
#
#Query to check is this email has already been referred by someone or not or referred id already been registered
#
	foreach($emailarray AS $key=>$value)
	{
		if(!empty($value))
		{
			$email = trim($value);
			$sql = "SELECT email FROM $sql_tbl[referee_map] WHERE email = '$email' AND referralid = '$refid'";
			$checkemail = func_query_first($sql);
			$sqllog->debug("File Name::>"."func.refer.php"."##Function Name::>func_check_duplicate_email()##"."##SQL Error::>".$sql); 
			
			if(!$checkemail['email'])
			{
				$existemail = $existemail.", ".$email;
			}
		}
	}
	

	return $existemail; 
}


#
#Function Name: func_get_script_name
#Return : This function return name of current file name
#Parameter: An array of referred email
#
function func_get_script_name()
{
	$file = $_SERVER["SCRIPT_NAME"];
	$break = Explode('/', $file);
	$scriptname = $break[count($break) - 1];
	return $scriptname; 
}

#
#Function Name: func_insert_referred_amount
#Parameter: Four parameter 1. Email id of referred by 2. Referral id 3. Total amount 4. Order id
#
function func_insert_referred_amount($email, $refid, $amount, $orderid ='0')
{
	global $sql_tbl;
	global $sqllog;

#
#Load referral percentage by referred is from mk_referral
#
	$sql = "SELECT refferal_percent, status FROM $sql_tbl[mk_referral] WHERE id='$refid' AND status='1'";
	$sqllog->debug("File Name::>"."func.refer.php"."##Function Name::>func_insert_referred_amount()##"."##SQL Error::>".$sql); 
	$result = func_query_first($sql);
	
	if($result['status'] == '1')
    {
		$refpercent = $result['refferal_percent'];
		$referee_earn_amount = $amount * ($refpercent/100);

			#
			#Insert record in to mk_user_referral_money if earn amount is not zero
			#
			if($referee_earn_amount>0 && $orderid != '0')
			{
			   $sql = "INSERT INTO $sql_tbl[referral_money](login, ref_money, status, orderid) VALUES('$email', '$referee_earn_amount', 'C', $orderid)";
			   $flag = db_query($sql);
			   $sqllog->debug("File Name::>"."func.refer.php"."##Function Name::>func_insert_referred_amount()##"."##SQL Error::>".$sql);
			   return $flag;
			}
	 }
	 if($orderid == 0) { return $referee_earn_amount ; }
}

############# function added to be used in admin section ###########

function func_getReferalInfo($mode,$referid='',$searchText='')
{
 global  $sql_tbl;
	     #
		 # search the designers
		 #
		 $customers  = $sql_tbl['customers'];
		 $referer = 'mk_user_referral_money';
       
		 $Sql = "SELECT  $customers.login,$customers.firstname,$customers.lastname,SUM(IFNULL( $referer.ref_money,0)) as commEarned, $customers.email , $referer.status FROM $customers INNER JOIN $referer ON $customers.login = $referer.login  ";
		 if($mode == 'search')
		 {
			  $andCondition = true;

	      	 if(!empty($searchText)){
				  $Sql .= " WHERE ($customers.firstname LIKE '%$searchText%'
					  || $customers.lastname LIKE '%$searchText%' || $customers.login LIKE '%$searchText%' ) ";
				  $andCondition = false;
				  //$Sql .= " AND $referer.status = 'C'  ";
                  
			 }
             if($andCondition)
                //$Sql .="  WHERE  $referer.status = 'C'  ";

              $Sql .="   GROUP BY $customers.login ORDER BY $customers.firstname";
          }
	     if($mode == 'update'||$mode == 'show'||$mode == 'payment'||$mode == 'save')
	     {
		     	 $Sql .=" WHERE $customers.login = '".$referid."'
				        GROUP BY $customers.login ";
		 }
		   
		$referer = func_query ($Sql);
		return $referer;
}

function getRefererCommDetail($status,$refererid )
{
	  global  $sql_tbl;
	  $referer = 'mk_user_referral_money';
	   if($status == 'All')
		{
				 $SQL = "SELECT  $referer.login , IFNULL($referer.ref_money,0) as commEarned,status,orderid FROM $referer
				 WHERE login = '".$refererid."'   ";

		}
		else
		{
				  $SQL = "SELECT   $referer.login , IFNULL($referer.ref_money,0) as commEarned , status, orderid FROM $referer
				 WHERE login = '".$refererid."' AND status='".$status."'   ";

	    }
		
		$comm_details = func_query ($SQL);
		return $comm_details;
}

function  getRefererPaymentHistory($refererid,$recipient_type='')
{
	global  $sql_tbl;
	$sql_tbl[mk_designer_payment_commission] =  "mk_designer_payment_commission";
	
	/* $SQL= "SELECT  pc.payment_amt,FROM_UNIXTIME(payment_date,'%d/%m/%Y') as payment_date,
	pm.payment_method,pc.payment_status , pc.comments FROM
	$sql_tbl[payment_methods] as pm INNER JOIN $sql_tbl[mk_designer_payment_commission] as pc
	ON pm.paymentid =  pc.pay_method
	 WHERE pc.recipient_id =  '".$refererid."'  AND recipient_type='".$recipient_type."' ";*/
    
	$SQL= "SELECT  pc.payment_amt,FROM_UNIXTIME(payment_date,'%d/%m/%Y') as payment_date,
	pc.payment_status , pc.comments FROM
	$sql_tbl[mk_designer_payment_commission] as pc
	WHERE pc.recipient_id =  '".$refererid."'  AND recipient_type='".$recipient_type."' ";

	$comm_history = func_query ($SQL);
	return $comm_history;
}



 ?>