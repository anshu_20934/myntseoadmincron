<?php
use abtest\MABTest;

function setSessionVarsOnLogin(){
    global $login,$mobile,$login_type,$XCARTSESSID,$cookiedomain,$identifiers,$userfirstname, $userlastname, $gender, $DOB;
    global $account_type,$account_created_date,$xcart_dir;
    include_once("$xcart_dir/include/func/func.core.php");
    include_once \HostConfig::$documentRoot."/include/class/mcart/class.MCartUtils.php";
    x_session_register("userfirstname", $userfirstname);
    x_session_register("userlastname", $userlastname);
    x_session_register("gender", $gender);
    x_session_register("DOB", $DOB);
    x_session_register("account_type");
    x_session_register("login",$login);
    x_session_register("mobile",$mobile);
    x_session_register("login_type",$login_type); 
    $orderCount = func_query_first_cell("select count(1) as to_be_processed from xcart_orders xo 
                                        left join mk_old_returns_tracking rto on xo.orderid = rto.orderid 
                                        where xo.status in ('PK','SH','DL','C','WP','OH','Q','RFR') 
                                        and xo.login = '$login' and rto.orderid is null", true);
    $returningCustomer = $orderCount >= 1 ? 1 : 0;
    x_session_register("returningCustomer", $returningCustomer);

    setCartSessionVarsOnLogin();
    updateSessionUpdatedFlagBySessionId($XCARTSESSID, 'Y');
    setSecureSessionId();

    x_session_register("identifiers",array());

    $identifiers['C'] = array (
        'login' => $login,
        'firstname' => $userfirstname,
        'login_type' => $login_type,
        'account_type' => $account_type,
        'first_login_date' => $account_created_date,
    );
}

function setCartSessionVarsOnLogin() {
        x_session_save();
        MCartUtils::fetchAndUpdateSessionCartVariables();
}


function clearSessionVarsOnLogout($userSuspended = false){
	global $login,$active_modules,$current_type,$identifiers,$old_login_type, $current_type, $cart, $access_status;
    global $merchant_password, $logout_user, $login_redirect, $logout_event, $REMOTE_ADDR,$XCART_SESSION_NAME;
    global $sql_tbl, $weblog, $xcart_dir, $login;
	include_once("$xcart_dir/databaseinit.php");
    include_once("$xcart_dir/include/func/func.core.php");
    x_session_register("identifiers",array());
    x_session_register("payment_cc_fields");
    $payment_cc_fields = array();
    func_unset($identifiers,$current_type);
    if (!empty($active_modules['Simple_Mode'])) {
        if ($current_type == 'A') func_unset($identifiers,'P');
        if ($current_type == 'P') func_unset($identifiers,'A');
    }

    // destroy all the session variable on logout
    if (x_session_is_registered("chosenProductConfiguration"))
        x_session_unregister("chosenProductConfiguration");
    // destroy all the session variable on logout
    if (x_session_is_registered("express_buy_enabled"))
        x_session_unregister("express_buy_enabled");
    //destroy the sxid    
    if(x_session_is_registered("sxid"))
        x_session_unregister("sxid");

    if(x_session_is_registered("selected_address"))
        x_session_unregister("selected_address");

    if (x_session_is_registered("masterCustomizationArray"))
        x_session_unregister("masterCustomizationArray");

    if (x_session_is_registered("masterCustomizationArrayD"))
        x_session_unregister("masterCustomizationArrayD");

    if (x_session_is_registered("productsInCart"))
        x_session_unregister("productsInCart");
    
    if (x_session_is_registered("coupondiscount"))
        x_session_unregister("coupondiscount");

    if (x_session_is_registered("couponCode"))
        x_session_unregister("couponCode");

    if (x_session_is_registered("cashCouponCode"))
        x_session_unregister("cashCouponCode");

    if (x_session_is_registered("cashdiscount"))
        x_session_unregister("cashdiscount");

    if (x_session_is_registered("offers"))
        x_session_unregister("offers");

    if (x_session_is_registered("totaloffers"))
        x_session_unregister("totaloffers");

    if (x_session_is_registered("totaldiscountonoffer"))
        x_session_unregister("totaldiscountonoffer");

    if (x_session_is_registered("productids"))
        x_session_unregister("productids"); 

    if (x_session_is_registered("grandTotal"))
        x_session_unregister("grandTotal"); 

    if (x_session_is_registered("amountAfterDiscount"))
        x_session_unregister("amountAfterDiscount");    

    if (x_session_is_registered("vtr"))
        x_session_unregister("vtr");    

    if (x_session_is_registered("userfirstname"))
        x_session_unregister("userfirstname");    
    if (x_session_is_registered("userlastname"))
        x_session_unregister("userlastname");    
    if (x_session_is_registered("gender"))
        x_session_unregister("gender");    
    if (x_session_is_registered("DOB"))
        x_session_unregister("DOB");    
    if (x_session_is_registered("referer_url"))
        x_session_unregister("referer_url");    

    if (x_session_is_registered("mobile"))
        x_session_unregister("mobile");  
    if (x_session_is_registered("mcartid"))
        x_session_unregister("mcartid");   
    if (x_session_is_registered("mcartidgiftcard"))
        x_session_unregister("mcartidgiftcard");
    if (x_session_is_registered("mcartidsaved"))
         x_session_unregister("mcartidsaved");
	if (x_session_is_registered("loyaltyPointsHeaderDetails"))
         x_session_unregister("loyaltyPointsHeaderDetails");     
	if (x_session_is_registered("telesalesAgent"))
         x_session_unregister("telesalesAgent");
    if (x_session_is_registered("returningCustomer"))
	x_session_unregister("returningCustomer");	    
    if(x_session_is_registered("notificationsData")){
            x_session_unregister("notificationsData");
        }
    if(x_session_is_registered("mrpData")){
            x_session_unregister("mrpData");
        }
	x_session_unregister("returningCustomer");	
    if (x_session_is_registered("loyaltyPointsHeaderDetails"))
         x_session_unregister("loyaltyPointsHeaderDetails");    
    $XCART_SESSION_VARS['firstname'] ="" ;
    include_once(\HostConfig::$documentRoot."/include/class/mcart/class.MCartUtils.php");
    MCartUtils::clearSessionCartVariables();
// x_session_register("masterCustomizationArray");
#
# Insert entry into login_history
#
    db_query("REPLACE INTO $sql_tbl[login_history] (login, date_time, usertype, action, status, ip) VALUES ('$login','".time()."','$login_type','logout','success','$REMOTE_ADDR')");

    $old_login_type = $current_type;
    $cart = "";
    $access_status = "";
    $merchant_password = "";
    $logout_user = true;
    if ($current_type == 'A' || $current_type == 'P')
        func_ge_erase();

    x_session_unregister("hide_security_warning");
    x_session_unregister("login");
    x_session_unregister("login_type");
    x_session_register("login_redirect");
    $login_redirect = 1;

    $logout_event = "success";
    x_session_register("logout_event");
    x_session_unregister("user_account_status");
    x_session_unregister("user_account_membership_details");
    if (x_session_is_registered("fb_uid")) {
        x_session_unregister("fb_uid");
    }

    if(x_session_is_registered("lastAddressUsedID")) {
        x_session_unregister("lastAddressUsedID");
    }
    if($userSuspended){
        setMyntraCookie($XCART_SESSION_NAME, 'ab',time()-3600 , "/", $cookiedomain, false, true);
    }
    
}


function setSecureSessionId(){
    global $cookiedomain,$XCART_SESSION_VARS;
     //***Login Expiry ticket
    $secure_sessid = md5(uniqid(rand()));
    setMyntraCookie('sxid',$secure_sessid, 0 , "/", $cookiedomain, true, true);
    $XCART_SESSION_VARS['sxid']=$secure_sessid;
    $XCART_SESSION_VARS['prompt_login'] = 0;
}

function isGuestCheckout() {
    global $XCART_SESSION_VARS;
    return !empty($XCART_SESSION_VARS['glogin']);
}
