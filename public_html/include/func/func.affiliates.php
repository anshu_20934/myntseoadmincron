<?php
if (!defined('XCART_START'))
 {
	 header ("Location: ../");
	die ("Access denied");
 }
	
include_once("func.mkimage.php");
include_once("func.mail.php");
include_once("func.crypt.php");
include_once("func.randnum.php");
require_once($xcart_dir.'/include/sessionstore/session_store_wrapper.php');

 ############# defined a function to upload the images ########		
function createdir( $path )
{
	$dirpath = $path;
      if(!file_exists($dirpath))
	  {
	       mkdir($dirpath ,  0777);
      }
    
}

function uploadAffiliatesImages($_FILES,$type,$_POST,&$affiliateInfoArray)
{
	 global $xcart_web_dir,$http_location,$affiliatesImagesPath;
	 
	 $errorMsg = '';
	 $step = "step3";
	 unset($affiliateInfoArray['contactinfo']);
	 storeAffiliateInfo($step,$affiliateInfoArray,$_POST,$selectedproducts,$selectedcategories,$productTypes);
	 ##### create affiliates dir if not exist  ##################
	 if(!file_exists($affiliatesImagesPath['logo'].$affiliateInfoArray['contactinfo']['company']))
	 {
		 createdir($affiliatesImagesPath['logo'].$affiliateInfoArray['contactinfo']['company']);
	 }
	 $affiliateImagePath = $affiliatesImagesPath['logo'].$_POST['company'];
	
	 ## end ####

	 switch($type)
     {
			 case "logo" :
	    				if(!empty($affiliateInfoArray['personalize']['logo']))
						{
							addToTempImagesList( $affiliateImagePath."/".$type."/".basename($affiliateInfoArray['personalize']['logo']));
							addToTempImagesList($affiliatesImagesPath."/".$type."/T/".basename($affiliateInfoArray['personalize']['logo']));
						}
						$baseImageName = basename($_FILES['affiliatelogo']['name']);  
						
                         ##### create affiliates  logo dir if not exist  ##################
						 if(!file_exists($affiliateImagePath."/".$type))
								createdir($affiliateImagePath."/".$type);
						
                       	 $afflogopath =  $affiliateImagePath."/".$type;

						$newImageFile =  $afflogopath."/".$_POST['company']."_".$baseImageName;
						$result = @move_uploaded_file($_FILES['affiliatelogo']['tmp_name'], $newImageFile);
						$errorMsg = isValidImage($result,$newImageFile) ;
						
						if(empty($errorMsg))
						{ 
							
							// generating thumbnail image for the uploaded images 
						/*	if(!file_exists($afflogopath."/T"))
								  createdir( $afflogopath."/T");*/
						   $resizeImageName = "r_".basename($newImageFile);
						   //$resizedImagePath = $afflogopath."/T/".basename($newImageFile);
						   $resizedImagePath = $afflogopath."/".$resizeImageName;
						   resizeImageToDimension($newImageFile,$resizedImagePath,LOGO_WID,LOGO_HEG);
						     
						  /* $affiliateInfoArray['personalize']['logo'] =  $http_location."/images/affiliates/".$affiliateInfoArray[contactinfo]['company']."/".$type."/T/".basename($thumnailImagePath);*/
						 	$affiliateInfoArray['personalize']['logo'] =  $http_location."/images/affiliates/".$affiliateInfoArray[contactinfo]['company']."/".$type."/".basename($resizedImagePath);					   
						   $affiliateInfoArray['personalize']['title'] = $_POST['title'];
						   $affiliateInfoArray['personalize']['theme'] = $_POST['theme'];
						   $affiliateInfoArray['personalize']['userAuth'] = $_POST['authtype'];
							  x_session_register("affiliateInfoArray");
						}
	 
				  break;
	  }
	  return $errorMsg;
	
	
}

function isValidImage($uploadStatus,$newImageFile)
{
	global $maxFileSize,$locale;
	$imageAttr = getimagesize($newImageFile);
	$error = false;     
	$errMessage =  '';
	

		if(empty($uploadStatus))
		{
			
			$errMessage = getMessage('FILE_NOT_UPLOADED'); //,$param1,$param2,$param3);
			$error = true;
		}
		if (filesize($newImageFile) > $maxFileSize)
		{
			
			$params = array("1 MB");
			$errMessage = getMessage('MAX_FILE_SIZE',$params);
			$error = true;
		}   
		if ($imageAttr['mime'] != 'image/jpeg' && $imageAttr['mime'] != 'image/png' && $imageAttr['mime'] != 'image/gif')
		{
			 $errMessage = getMessage('NOT_VALID_FILE');
			 $error = true;
		}
		if($error)
		   addToTempImagesList($newImageFile);
	return $errMessage;
}

function func_load_affiliate_details($affiliateid=0)
{
	    global $sql_tbl ;
	    $affiliates = $sql_tbl['mk_affiliates'];
	    $queryString      =
        "select  *   from $affiliates   ";
        if(intval($affiliateid) > 0 )
        {
        	$queryString .= " where  affiliate_id = '".$affiliateid."' and active = 'Y' ";
		}
		else 
		{
			$queryString .= " where active = 'Y' ";
		}
		$affiliatesInfo = func_query($queryString);
		return $affiliatesInfo;
}
function func_load_affiliate_details_bylogin($login)
{
	global $sql_tbl ; 
	$affiliateTable = $sql_tbl['mk_affiliates'];
	$query   =  "SELECT affiliate_id,company_name FROM  $affiliateTable WHERE  userid = '".$login."' AND active = 'Y' ";
	$affiliateInfo = func_query($query);
	return $affiliateInfo;
}


function func_check_user_exist($useremail,&$profilearray=array())
{
	  global $sql_tbl;
	  $rsCustomer = db_query("SELECT login,firstname,usertype FROM $sql_tbl[customers] WHERE login='".$useremail."' ");
	  if(db_num_rows($rsCustomer) == 0)
	  {
		   return false;
	  }
	  else
	 {
		  $rows = db_fetch_array($rsCustomer); 
          $profilearray['login'] = $rows['login'];
		  $profilearray['usertype'] = $rows['usertype'];
		  $profilearray['firstname'] =  $rows['firstname'];
		   return true;
	 }
}

function func_autologin_affiliate_user($email,$profile= array())
{
	    global $XCARTSESSID,$sql_tbl,$identifiers , $userfirstname,$weblog;
        x_session_register("userfirstname");
		$_curtime = time();
       
		#db_query("update mk_session set updated='Y' where sessionid= '$XCARTSESSID' ");
		updateSessionUpdatedFlagBySessionId($XCARTSESSID, 'Y');
	  	db_query("UPDATE $sql_tbl[customers] SET last_login='$_curtime', first_login='$_curtime' WHERE login = '$email' ");
		$auto_login = true;
		$weblog->info("Auto login user >>> ".$profile['login']);
		$login = $profile['login'];
		$login_type = $profile['usertype'];
		$userfirstname = $profile['firstname'];
		$logged = "";
		x_session_register("identifiers",array());
		$identifiers[$login_type] = array (
			'login' => $login,
			'firstname' => $userfirstname,
			'login_type' => $login_type,
		);
		
		
				
}


function func_autoregister_affiliate_user($email,&$profilearray= array(),$firstname='',$lastname='')
{
	    global  $config,$sql_tbl;
		if(!empty($firstname))
		{
			 $firstname =  $firstname;
			
		}
		if(!empty($lastname))
		{
			$lastname = $lastname;
		}
		
		if(empty($firstname) && empty($lastname) ) 
	    {
			   $name = explode("@",$email);
               $firstname = $name[0];
			   $lastname = $name[0];

		}
	   $password  =get_rand_id(8);

		######## Hold the values , which need to be inserted for the users
    	$profile_values = array();
		$profile_values['login'] = $email;
		$profile_values['usertype'] = 'C';
		$crypted = addslashes(text_crypt($password));
		$profile_values['password'] = $crypted;
		$profile_values['firstname'] = $firstname;
		$profile_values['lastname'] = $lastname;
		$profile_values['b_firstname'] = $firstname;
		$profile_values['b_lastname'] = $lastname;
		$profile_values['s_firstname'] = $firstname;
		$profile_values['s_lastname'] = $lastname;
		$profile_values['email'] =$email;
		$profile_values['termcondition'] = '1';
		$profile_values['addressoption'] = 'M';
		$profile_values['account_type'] = 'AF';
	
		
		func_array2insert('customers', $profile_values);
         # array hold user credentials and used auto auto login
     
          $profilearray['login'] = $email;
		  $profilearray['usertype'] = 'C';
		  $profilearray['firstname'] =   $firstname;

 }


function  storeAffiliateInfo($step,&$affiliateInfoArray,$_POST,&$selectedproducts=array(),&$selectedcategories= array(),&$productTypes= array())
{
	         global $weblog,$profile_values,$sql_tbl,$login;
	         global  $affiliatesImagesPath;
	         
             switch($step)
		     {

                   case  "step3":
                             if(empty($_POST['stepBack']))
                              {
						         $affiliateInfoArray['contactinfo'] = array(
								                   "contactname" => $_POST['contactname'],
								                   "company" => $_POST['company'] ,
								                   "email"      =>  $_POST['email'],
								                   "phone1"    => $_POST['phone1'],
								                   "phone2"    =>  $_POST['phone2'],
								                   "website"   =>  $_POST['website'],
								                 
	 						                       );
	 						     $affiliateInfoArray['personalize']['title'] = $_POST['title'];
								 $affiliateInfoArray['personalize']['theme'] = $_POST['theme'];
								 $affiliateInfoArray['personalize']['userAuth'] = $_POST['authtype'];                                      x_session_register("affiliateInfoArray");
					           
                              }
                             if(!empty($affiliateInfoArray['content']['producttype']))
								   $productTypes = explode(",",$affiliateInfoArray['content']['producttype']);

							 if(!empty($affiliateInfoArray['content']['category']))
								   $selectedcategories = explode(",",$affiliateInfoArray['content']['category']);

							 if(!empty($affiliateInfoArray['content']['product']))
								   $selectedproducts = explode(",",$affiliateInfoArray['content']['product']);
                             break; 
				 
					case "confirm" :
						 #
						 # Check if affiliate is content provider or not
						 #
						 if($_POST['personalizegift'] == 'C')
						 {
						 	 # Content and User Provider
						 	 $isContentProvider = 'Y';
						 	 
						 }
						 else if($_POST['personalizegift'] == 'P') 
						 {
						 	 # Only User Provider
						 	 $isContentProvider = 'N';
						 }
						 
						 						 
						 if(!empty($affiliateInfoArray['contactinfo']) && !empty($affiliateInfoArray['personalize']))
						 {
                               $query_data = array(
								"company_name"         => mysql_escape_string($affiliateInfoArray['contactinfo']["company"])  ,
								"contact_name"         => mysql_escape_string($affiliateInfoArray['contactinfo']["contactname"]),
								"contact_email"        => $affiliateInfoArray['contactinfo']["email"],
								"contact_phone1"       => $affiliateInfoArray['contactinfo']["phone1"],
								"contact_phone2"       => $affiliateInfoArray['contactinfo']["phone2"],
								"affiliate_return_url" => $affiliateInfoArray['contactinfo']["website"],
								"title"                => mysql_escape_string($affiliateInfoArray['personalize']['title']),
								"logo"                 =>  mysql_escape_string(basename($affiliateInfoArray['personalize']['logo'])), 
								"image_header"         => mysql_escape_string(basename($affiliateInfoArray['personalize']['header'])),
								"image_footer"         => mysql_escape_string(basename($affiliateInfoArray['personalize']['footer'])),
						        "themeid"              => $affiliateInfoArray['personalize']['theme'],
						        "aff_auth_mechanism"   => $affiliateInfoArray['personalize']['userAuth'],
						        "joined_date"          => time(),
						        "active"               => 'N',
						       	"contentprovider"	   => $isContentProvider,
						       	"userid"			   => $login				
							);
				            $weblog->info("Function name : storeAffiliateInfo()  insert data in mk_affiliates");
				           	$affiliateId = func_array2insert(
								"mk_affiliates",$query_data );
							$imagePath = $affiliatesImagesPath['logo'].$affiliateInfoArray['contactinfo']["company"];
							$weblog->info("Function name : storeAffiliateInfo()  rename affiliate folder with affiliate id  = $affiliateId");
							@rename($imagePath,$affiliatesImagesPath['logo'].$affiliateId);
                            chmod($affiliatesImagesPath['logo'].$affiliateId, 0777);
                            $weblog->info("Function name : storeAffiliateInfo() Login id : $login");
							if(!empty($login))
							{
							  $weblog->info("send mail to registered user>".$affiliateInfoArray['contactinfo']["contactname"]."==".$profile_values['login']);
						   	  $template = "affregisterconfirm";
						      $mailto = $login;
						      $from = "MyntraAdmin";
						      $args = array( "FIRST_NAME" => $affiliateInfoArray['contactinfo']["contactname"]
				                 );
						      $weblog->info("sendMessage function called to send mail");
						   	  sendMessage($template, $args,$mailto,$from);
							}
 	
							######## if affiliate sends only user ######################### 	
	                        if($isContentProvider == 'N')
							{
								 $productids = '';
								 
							     foreach($_POST['product'] as $key => $value)
								 {
									    $productids .= $value.",";
								 }
								 foreach($_POST['category'] as $key => $value)
								 {
										   $categories .=  $value.",";
								 }
								  
								 $product = (!empty($_POST['chkProduct']) ? trim($productids,",") : '');
								 $category = (!empty($_POST['chkCategory']) ? trim($categories,",") : '');
								 $tags = (!empty($_POST['chkTags']) ? $_POST['tags'] : '');
								 (!empty($productids) ? func_array2insert("mk_affiliate_product_map",array("affiliate_id"=>$affiliateId,"product_type_id"=>$product)) : '' );
								   
								 (!empty($category) ? func_array2insert("mk_affiliate_category_map",array("affiliate_id"=>$affiliateId,"category_id "=>$category)) : '' );
								   
								(!empty($tags) ? func_array2insert("mk_affiliate_tags",array("affiliate_id"=>$affiliateId,"tags "=>trim($tags,","))) : '' );		
								    ######### generate code and send generated code by mail ######
								   func_generate_code('N',$affiliateId);	
								    			
							}
							######## if affiliate sends only user and content #########################
							elseif($isContentProvider == 'Y')
							{
								
	  		                     #insert data in  product map
	  		                     $productids = '';
	  		                      		                     
		  		                 foreach($_POST[producttype] as $_key => $_val)
								 {
									 	 $productids .= $_val.",";
								 }
								 $productids = trim($productids,",");
	  		                    
	  		                             
								(!empty($productids)) ?  func_array2insert($sql_tbl['mk_affiliate_product_map'], array("product_type_id" => $productids,"affiliate_id" => $affiliateId )) : '';
								
								######### generate code and send generated code by mail ######
								 if(!empty($productids))
								 {
									   $productTypeIds = explode(",",$productids);
								        func_generate_code('Y',$affiliateId,$productTypeIds);	
								 }
								
							}
							
							x_session_unregister("affiliateInfoArray");
	                        unset($affiliateInfoArray);
	                                       
	                        
						 }
						break;
				       
						   
		   }
}

function func_get_affiliates_types($affiliateId,$explodeTypes = false)
{
	 global $sql_tbl ;
	 $affiliateProductMap = $sql_tbl['mk_affiliate_product_map'];
	 $queryString      =
     "select  *   from $affiliateProductMap  where  affiliate_id = '".$affiliateId."' ";
	 $productMapInfo = func_query($queryString);
	 if($explodeTypes)
	 {
	    $productinfo =  (!empty($productMapInfo[0]['product_type_id'])) ? explode(",",$productMapInfo[0]['product_type_id']) : '';
	    return $productinfo;
	 }
     return  $productMapInfo;
	 
}

function func_get_affiliates_category($affiliateId,$explodeCat = false)
{
	 global $sql_tbl ;
	 $affiliateCategories = $sql_tbl['mk_affiliate_category_map'];
	 $queryString      =
     "select  *   from $affiliateCategories  where  affiliate_id = '".$affiliateId."' ";
	 $categoryInfo = func_query($queryString);
	 if($explodeCat)
	 {
	    $catArray =  (!empty($categoryInfo[0]['category_id'])) ? explode(",",$categoryInfo[0]['category_id']) : '';
	    return $catArray;
	  }
     return  $categoryInfo;
}

function func_get_affiliates_tags($affiliateId,$explodeTags = true)
{
	 global $sql_tbl ;
	 $affiliateTags = $sql_tbl['mk_affiliate_tags'];
	 $queryString      =
     "select  *   from $affiliateTags  where  affiliate_id = '".$affiliateId."' ";
	 $tagsInfo = func_query($queryString);
	 $tagArray = array();
	 
	 if($explodeTags)
	 {
		 $tagArray =  (!empty($tagsInfo[0]['tags'])) ? explode(",",$tagsInfo[0]['tags']) : '' ;
	 }
	 else
	 { 
	    $str = (!empty($tagsInfo[0]['tags'])) ? $tagsInfo[0]['tags'] : '' ;	
	    return $str; 	 
	 }
     return  $tagArray;
	
}
 function func_generate_code($isContent,$affiliateID,$prdtypes = array())
 {
 	
 	 global $sql_tbl;
	 global $http_location;
 	 if($isContent == 'N')
 	 {
 	   $generatedcode = $http_location."/modules/affiliates/affiliatehandler.php?affiliate=$affiliateID&useremail=[USEREMAIL]"	;
 	   db_query("INSERT INTO $sql_tbl[mk_affiliate_generated_code](affiliate_id,generated_code) VALUES ('".$affiliateID."','".mysql_escape_string($generatedcode)."')");
 	 }
 	 else if($isContent == 'Y')
 	 {
 	 	
 	 	 if(!empty($prdtypes))
 	 	 {
 	 	 	foreach($prdtypes as $key => $id)
 	 	 	{
 	 	 		 
 	 	 		 $styleID = func_load_default_style_for_product($id); 
 	 	 		 $codeText .=  $http_location."/modules/affiliates/affiliatehandler.php?affiliate=$affiliateID&image=[IMAGEURL]&imagename=[IMAGENAME]&imageid=[IMAGEID]&product= $id&style=$styleID&useremail=[USEREMAIL]<br>"	;
 	 	 		 $generatedcode = $http_location."/modules/affiliates/affiliatehandler.php?affiliate=$affiliateID&image=[IMAGEURL]&imagename=[IMAGENAME]&imageid=[IMAGEID]&product= $id&style=$styleID&useremail=[USEREMAIL]"	;
 	 	 		 db_query("INSERT INTO $sql_tbl[mk_affiliate_generated_code](affiliate_id,product_type_id,generated_code) VALUES ('".$affiliateID."','".$id."','".mysql_escape_string($generatedcode)."')");
 	 	 		 
 	 	 	}
 	 	 }
 	 	 //sendMessage($template)
 	 	
 	 }
 	
 }
 
 function func_load_default_style_for_product($typeId)
 {
	  global $sql_tbl, $product_style_name;
 	 $defaultStyle = func_query("SELECT id,name FROM $sql_tbl[mk_product_style] WHERE  product_type ='".$typeId."' AND default_style = 'Y' ORDER BY id ASC ");
 	 $product_style_name =  $defaultStyle[0]['name'];
 	 return $defaultStyle[0][id];
 	
 }

########## function to load the transcations of affiliates ##########
function  func_load_affiliate_transactions($affiliateId)
{
     global $sql_tbl;
     $affiliateStatistics = $sql_tbl[mk_affiliate_statistics];
     $paidCommission = $sql_tbl[mk_designer_payment_commission];
	 $orderTbl = $sql_tbl['orders'];
    
     $sql ="SELECT  SUM(IFNULL($affiliateStatistics.commission,0)) as commEarned ,
	  $affiliateStatistics.affiliate_id as affiliate
	  FROM $affiliateStatistics INNER JOIN $orderTbl ON 
	  $orderTbl.orderid = $affiliateStatistics.order_id WHERE $orderTbl.status = 'C' AND affiliate_id = '".$affiliateId."' 
	  GROUP BY $affiliateStatistics.affiliate_id  ";
     $earnedComm  = func_query($sql);
    
	  $sql = "SELECT SUM(IFNULL(commission,0))   as paidcomm FROM $affiliateStatistics WHERE 
     status='1006' AND affiliate_id = '".$affiliateId."' GROUP BY affiliate_id";
     $paidComm  = func_query($sql);
     $dueComm = abs($earnedComm[0][commEarned] - $paidComm[0][paidcomm]);
     $array = array("PAID"=> $paidComm[0][paidcomm] , "EARN"=>$earnedComm[0][commEarned] ,"DUE"=>$dueComm );
     return $array;
     
     

}

######### function to calculate the commission for affiliates ######
function func_set_affiliate_commission($affiliateId,$affiliateType,$total,$orderid)
{
    global $sql_tbl,$weblog;
	$weblog->info(" Called  func_set_affiliate_commission function for ".$affiliateId.",".$affiliateType.",".$orderid);
	$affiliateComm = 0.00;
	return $affiliateComm;	    
}

function func_register_affiliate(&$profile_values,$affiliateInfoArray)
{
	   global $weblog,$config ;
		###### insert data in xcart_customers table and auto login it #####
		$profile_values['login'] = $affiliateInfoArray['contactinfo']["email"];
		$profile_values['usertype'] = 'C';
		$crypted = addslashes(text_crypt( $affiliateInfoArray['contactinfo']['userpass']));
		$profile_values['password'] = $crypted;
		$profile_values['firstname'] = $affiliateInfoArray['contactinfo']["contactname"];
		$profile_values['b_firstname'] = $affiliateInfoArray['contactinfo']["contactname"];					    $profile_values['s_firstname'] = $affiliateInfoArray['contactinfo']["contactname"];
		$profile_values['email'] = $affiliateInfoArray['contactinfo']["email"];;
		$profile_values['termcondition'] = '1';
		$profile_values['addressoption'] = 'M';
		$profile_values['account_type'] = 'AF';
		
		$weblog->info("insert data in xcart customers ");
		func_array2insert('customers', $profile_values);
		if($config['Email_Note']['eml_profile_modified_customer'] = 'Y')
		{
			    //****************************************
			    //Send mail on user registration
			    //****************************************
			   $weblog->info("send mail to registered user>".$profile_values['firstname']."==".$profile_values['login']);
			   $template = "affiliateregistration";
			   $mailto = $profile_values['login'];
			   $from = "MyntraAdmin";
			   $args = array( "FIRST_NAME" => $profile_values['firstname'],
			                  "USER_NAME" => $profile_values['login']);
			   $weblog->info("sendMessage function called to send mail");
			   sendMessage($template, $args,$mailto,$from);
						
		}
	
}

/* get the account type  */
function func_get_loggedin_acc_type($login)
{
	 global $sql_tbl;
	 $rsAccountType = db_query("SELECT affiliate_id FROM $sql_tbl[mk_affiliates] WHERE 
	 userid ='".$login."' ");
	 $result = db_fetch_array($rsAccountType);
	 $isAffiliate  = !empty($result['affiliate_id']) ? 'Y' : 'N';
	 return $isAffiliate;
}


############### function to load all user authentication process ###########
function affiliate_user_authentication()
{
	 global $sql_tbl,$weblog ;
	 $affiliateUserAuth= $sql_tbl['mk_affiliate_auth_mechanism'] = "mk_affiliate_auth_mechanism";
	 $weblog->info("File name : func.affiliates.php >> Query user authentication");
	 $query  = "SELECT * FROM $affiliateUserAuth";
	 $userAuthResult = func_query($query);
	 $weblog->info("File name : func.affiliates.php >> Query executed");
     return $userAuthResult;
	
}

function func_download_affiliate_content(&$affiliatedetails,$otherParam = array()){
	global $weblog,$XCART_SESSION_VARS; 
    $style = $otherParam['STYLE'];
    $imagetype = $otherParam['IMAGE_TYPE'];
    $imageurl = $otherParam['IMAGE_URL'];
  
    //affiliate image path
   	$affiliateimagepath = "../../images/affiliates/".$affiliatedetails[0]['company_name']."/";
	$weblog->info("Download path of affiliate image in func.affiliates.php >> ".$affiliateimagepath);
        $weblog->info("Call to CURL function"); 
	$ch = curl_init($imageurl);
	$filename = $affiliatedetails[0]['company_name']."_".uniqid().".".$imagetype;
	$fp = fopen($affiliateimagepath.$filename, "w");
	$affiliateOriginalImage = $affiliateimagepath.$filename;
	$weblog->info("imagePath : ".$affiliateOriginalImage); 
	curl_setopt($ch, CURLOPT_FILE, $fp);
	curl_setopt($ch, CURLOPT_HEADER, 0);
	$response = curl_exec($ch);
	if(curl_errno($ch)){
		$weblog->info("Error in downloading using curl"); 
	    print curl_error($ch);
		exit();
	}
	
	curl_close($ch);
	fclose($fp);
	$weblog->info("End of downlaod procees using curl"); 
    
	###=====================new cust=====================###
    $weblog->info("Start creating configurationarray"); 
        
    $windowid = md5(rand().time());
	
	if(!empty($affiliateOriginalImage)){
	    $actualThumbPath = "../../images/affiliates/".$affiliatedetails[0]['company_name']."/";
		if(!file_exists($actualThumbPath."T")){
			createdir($actualThumbPath."T");
		}
	    $affiliateThumbImage = $actualThumbPath."T/".$filename;		
	}
	
	// init chosen Product Configuration
	if( isset($XCART_SESSION_VARS["chosenProductConfigurationEx"]) ){		
		$chosenProductConfigurationEx = $XCART_SESSION_VARS["chosenProductConfigurationEx"]; // All Window's
		$thisChosenProductConfiguration = array();
		if( isset($chosenProductConfigurationEx[$windowid]) )  // This Window's
			$thisChosenProductConfiguration = $chosenProductConfigurationEx[$windowid];
	} else {		
		$chosenProductConfigurationEx = array();
		$thisChosenProductConfiguration = array();
	}
	
	// init customization array
	if( isset($XCART_SESSION_VARS["customizationArrayEx"]) ){
		$customizationArrayEx = $XCART_SESSION_VARS["customizationArrayEx"]; // All Window's
		$thisCustomizationArray = array();
		if( isset($customizationArrayEx[$windowid]) ) // This Window's
			$thisCustomizationArray = $customizationArrayEx[$windowid];
	} else {
		$customizationArrayEx = array();
		$thisCustomizationArray = array();
	}
	
	// init master customization array ( please note this is a readonly strcuture and it holds the base informaton of a style, never change this)
	if( isset($XCART_SESSION_VARS["masterCustomizationArrayEx"]) ){
	    $masterCustomizationArrayEx = $XCART_SESSION_VARS["masterCustomizationArrayEx"]; // All Window's
		$thisMasterCustomizationArray = array();
		if( isset($masterCustomizationArrayEx[$windowid]) ) // This Window's
			$thisMasterCustomizationArray = $masterCustomizationArrayEx[$windowid];
	} else {
		$masterCustomizationArrayEx = array();
		$thisMasterCustomizationArray = array();
	}

	//get the styleid
	$styleid = $otherParam['STYLE'];
	$styleid = strip_tags($styleid);
	$styleid = sanitize_int($styleid);
	
	// create customization data builder object	
	$customizationData = new customizationData($styleid);		
	
	// Fill chosen Product Configuration for this product style id
	$thisChosenProductConfiguration = $customizationData->initChosenProductConfigurationData($thisChosenProductConfiguration);
	
	// Fill customization Arrays for this product style id
	$thisCustomizationArray = $customizationData->initCustomizationArrayData($thisCustomizationArray);
		
	// Fill Master Customization Array for this product style id
	$thisMasterCustomizationArray = $customizationData->initMasterCustomizationArrayData($thisMasterCustomizationArray);
			
	//create all required images from original image
	$originalImagePath = $affiliateOriginalImage;//image path downloaded in affiliate folder
	$img_array = explode("/",$originalImagePath);
	$original_image_name = $img_array[count($img_array)-1];
	$img_extn = explode(".",$original_image_name);
	$imageExtension = $img_extn[count($img_extn)-1];
			
	$uniqid = rand().time();
	$final_image_name = $uniqid.".".$imageExtension;
	$newImageFile = "../../images/temp/L_$final_image_name";
	$thumbImageFile = "../../images/temp/T_$final_image_name";
	$iconImageFile = "../../images/temp/I_$final_image_name";
	
	//Generate the original images
	$im = new Imagick($originalImagePath);
	$cloneImage = $im->clone();
	$cloneImage->writeImage($newImageFile);
	$cloneImage->writeImage($affiliateOriginalImage);//image to be stored in affiliate folder

	//create thumbnail images
	$im = new Imagick($newImageFile);
	$im->scaleImage(100,100,true);
	$im->borderImage(new ImagickPixel('GRAY'),1,1);
	$bg = new Imagick(); 
	$bg->newImage(100,100,new ImagickPixel('white'),"jpg");
	$fGeo = $im->getImageGeometry();
	$bGeo = $bg->getImageGeometry();
	$centerx = ($bGeo["width"]/2) - ($fGeo["width"]/2) + $x;
	$centery = ($bGeo["height"]/2) - ($fGeo["height"]/2) + $y;
	$bg->compositeImage($im,Imagick::COMPOSITE_DEFAULT,$centerx,$centery);
	$bg->writeImage($thumbImageFile);
	$bg->writeImage($affiliateThumbImage);//image to be stored in affiliate folder
	$bg->destroy();
	$im->destroy();
	
	//create icon image
	$im = new Imagick($newImageFile);
	$im->scaleImage(40,40,true);
	$im->borderImage(new ImagickPixel('GRAY'),1,1);
	$bg = new Imagick(); 
	$bg->newImage(40,40,new ImagickPixel('white'),"jpg");
	$fGeo = $im->getImageGeometry();
	$bGeo = $bg->getImageGeometry();
	$centerx = ($bGeo["width"]/2) - ($fGeo["width"]/2) + $x;
	$centery = ($bGeo["height"]/2) - ($fGeo["height"]/2) + $y;
	$bg->compositeImage($im,Imagick::COMPOSITE_DEFAULT,$centerx,$centery);
	$bg->writeImage($iconImageFile);
	$bg->destroy();
	$im->destroy();
	
	//Get the iamge attributes
	$imageAttr = getimagesize($newImageFile);
	
	$imageBaseInfo['baseimagename'] = $original_image_name;
	$imageBaseInfo['baseimagewidth'] = $imageAttr[0];
	$imageBaseInfo['baseimageheight'] = $imageAttr[1];
	$imageBaseInfo['baseimagemimetype'] =$imageAttr['mime'];
	$imageBaseInfo['baseimagepath_L'] = substr($newImageFile,4);
	$imageBaseInfo['baseimagepath_T'] = substr($thumbImageFile,4);
	$imageBaseInfo['baseimagepath_I'] = substr($iconImageFile,4);

	//to show warning message on image(downloaded by curl) if image is of lower resolution
	$currentSelectedArea = $thisCustomizationArray['areas'][$thisCustomizationArray['curr_selected_area']];			
	$currentSelectedAreaName = $thisCustomizationArray['curr_selected_area'];			
	$currentSelectOrientation = $currentSelectedArea['curr_selected_orientation'];
	$sizeX = $thisMasterCustomizationArray['areas'][$currentSelectedAreaName]['orientations'][$currentSelectOrientation]['width_in'];
	$sizeY = $thisMasterCustomizationArray['areas'][$currentSelectedAreaName]['orientations'][$currentSelectOrientation]['height_in'];
	$IMAGE_DEFAULT_DPI = 96;
	$warningMessage = '';
	if( ($sizeX*$IMAGE_DEFAULT_DPI*0.66 > $imageAttr[0]) && ($sizeY*$IMAGE_DEFAULT_DPI*0.66 > $imageAttr[1]) )
	{
		$warningMessage = "The picture you have uploaded is too small(".floor($imageAttr[0]/IMAGE_DEFAULT_DPI)."x".floor($imageAttr[0]/IMAGE_DEFAULT_DPI).") to be printed  on the product and may be blurry on the final product. Please see Image Guidelines for details. Thank you.";
	}
	
	$objectid = md5( time().$newImageFile );
	$globalDataArray[$objectid] = $imageBaseInfo;
	
	// initialize all required array for helper class to build cust session data	
	$params = array("custArray"=>$thisCustomizationArray,"masterCustomizationArray"=>$thisMasterCustomizationArray,"chosenProductConfiguration"=>$thisChosenProductConfiguration,"globalDataArray"=>$globalDataArray);
	$customizationHelper = new CustomizationHelper($params );
		
	$areaDetails = $customizationHelper->getCustomizationArrayForCurrentSelectedArea();
	$orientationDetails =  $customizationHelper->getCustomizationArrayForCurrentSelectedOrientation();
	$style_area_icons = $customizationHelper->getAreaIconDetails();
	
	//update image info in cust area		
	$customizationHelper->updateImageInfoInCustArea($objectid);
	$thisCustomizationArray = $customizationHelper->getCustomizationArray();
	
	
	
	//to show affiliate image resolution warning
	if(!empty($warningMessage))
		$thisChosenProductConfiguration += array("affiliatehandler_image_resolution_warning"=>$warningMessage); 	

	//Write back all the data into session vars
	$thisChosenProductConfiguration += array("cust_from_affiliatehandler"=>1); 
	$chosenProductConfigurationEx[$windowid] = $thisChosenProductConfiguration;
	$customizationArrayEx[$windowid] = $thisCustomizationArray;
	$masterCustomizationArrayEx[$windowid] = $thisMasterCustomizationArray;
	
	$XCART_SESSION_VARS["chosenProductConfigurationEx"] = $chosenProductConfigurationEx;
	$XCART_SESSION_VARS["customizationArrayEx"] = $customizationArrayEx;
	$XCART_SESSION_VARS["masterCustomizationArrayEx"] = $masterCustomizationArrayEx;
	$globalDataArray = $XCART_SESSION_VARS["globalDataArray"];
	$globalDataArray[$objectid] = $imageBaseInfo;
	$XCART_SESSION_VARS["globalDataArray"] = $globalDataArray;
	
	$weblog->info("Created configuration array");
	return 	$windowid;
}

function func_create_search_form($affiliateId)
{
	global $weblog;
	 $strHiddenFields = ''; 	
     $affiliatesProductTypes =  func_get_affiliates_types($affiliateId);
	 $affiliatesCategories = func_get_affiliates_category($affiliateId);
	 $affiliatesTags = func_get_affiliates_tags($affiliateId);
	 $typesValues =  $affiliatesProductTypes[0]['product_type_id'];
	 $catValues = $affiliatesCategories[0]['category_id'];
	 #
	 # If product types  selected  at the affiliate registration
	 #
	 if(!empty($typesValues))
	 {
	 	$typeHiddenFields .= "<input type=\"hidden\" name=\"posted_data[by_types]\" value='$typesValues' />";
	    $strHiddenFields .= $typeHiddenFields ;	 
	 }
	 #
	 # If categories selected at the affiliate registration
	 #
	 if(!empty($catValues))
	 {
		 $categoriesHiddenFields .= "<input type=\"hidden\" name=\"posted_data[by_categories]\" value='$catValues' />";
		  $strHiddenFields .= $categoriesHiddenFields ;
	 }
	 
	 $tagHiddenFields = '';
	 #
	 # If tags selected at the affiliate registration
	 #
	 if(!empty($affiliatesTags))
	 {
	   foreach($affiliatesTags as $key=>$value)
	   {
	      $tagHiddenFields .= "<input type=\"hidden\" name=\"posted_data[by_substring][$key]\" value=\"$value\" />";
	   }
	    $strHiddenFields .= $tagHiddenFields ;
	 }
	
	  $str = "<form   name='basicsearchform' action='../../mkSearchResult.php' method =post>
    		 <input type='hidden' name='simple_search' value='Y' />
        	<input type='hidden' name='posted_data[affiliate_search]' value='Y' />
			<input type='hidden' name='mode' value='search' />
			<input type='hidden' name='posted_data[by_title]' value='Y' />
			<input type='hidden' name='posted_data[by_shortdescr]' value='Y' />
			<input type='hidden' name='posted_data[by_fulldescr]' value='Y' />";
      
       
      $str .= $strHiddenFields;
      $str .= "</form>";
    return  $str;
	
}

function func_download_choosen_image($custId, $imgURL, &$chosenProductConfiguration,$affiliateInfo,&$masterCustomizationArray,$switchNewStyle)
{
	    
	    $imgPath = '';
		
		######## delete the existing image to paste when user select another image ########
		$selectedCustID = $custId;
		$uploadedImagePath = $chosenProductConfiguration[$selectedCustID.'uploadedImagePath'];
       // $thumbImagePath = $chosenProductConfiguration[$selectedCustID.'thumbImagePath'];
		$pasteImagePath = $masterCustomizationArray[$selectedCustID]['pasteimagepath'];
        $pastefile = $masterCustomizationArray[$selectedCustID]['pastefile'];
         addToTempImagesList($uploadedImagePath);
	    //@unlink($thumbImagePath);
		addToTempImagesList($pasteImagePath);
	    addToTempImagesList($pastefile);
        unset($chosenProductConfiguration[$selectedCustID.'uploadedImagePath']);
        unset($chosenProductConfiguration[$selectedCustID.'thumbImagePath']);
		unset($masterCustomizationArray[$selectedCustID]['pasteimagepath']);
        unset($masterCustomizationArray[$selectedCustID]['pastefile']);
        unset($masterCustomizationArray[$selectedCustID]['dxstart']);
        unset($masterCustomizationArray[$selectedCustID	]['dystart']);
		####affiliate image path
		$affiliateimagepath = "./images/affiliates/".$affiliateInfo[0]['company_name']."/";
	   
		$imageTypeArray = explode(".",$imgURL);

	    $imagetype = $imageTypeArray[count($imageTypeArray)-1];
	    $ch = curl_init($imgURL);
		$filename = $affiliateInfo[0]['company_name']."_".uniqid().".".$imagetype;
		$fp = fopen($affiliateimagepath.$filename, "w");
			
		$imgPath = $affiliateimagepath.$filename;
		curl_setopt($ch, CURLOPT_FILE, $fp);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		$response = curl_exec($ch);
		if(curl_errno($ch))
		{
		    print curl_error($ch);
			exit();
		}
		
		curl_close($ch);
		fclose($fp);
		$imageAttr = getimagesize($imgPath); 
		if($imageAttr['mime'] != 'image/jpeg' && $imageAttr['mime'] != 'image/png')
		{
			addToTempImagesList($imgPath);
			$imgPath = '';
			return $imgPath;
		}	
		
	    return $imgPath;
}

function editAffiliateLogo($_FILES,$type,$title,&$affiliateInfoArray)
{
	 global $sql_tbl,$xcart_web_dir,$http_location,$affiliatesImagesPath;
	 
	 $errorMsg = '';
	 ##### create affiliates dir if not exist  ##################
	 if(!file_exists($affiliatesImagesPath['logo'].$affiliateInfoArray['contactinfo']['affiliateid']))
	 {
		 createdir($affiliatesImagesPath['logo'].$affiliateInfoArray['contactinfo']['affiliateid']);
	 }
	 $affiliateImagePath = $affiliatesImagesPath['logo'].$affiliateInfoArray['contactinfo']['affiliateid'];
	 ## end ####
	
	switch($type)
     {
			 case "logo" :
	    				if(!empty($affiliateInfoArray['personalize']['logo']))
						{
							addToTempImagesList( $affiliateImagePath."/".$type."/".basename($affiliateInfoArray['personalize']['logo']));
							addToTempImagesList($affiliateImagePath."/".$type."/T/".basename($affiliateInfoArray['personalize']['logo']));
						}
						$baseImageName = basename($_FILES['affiliatelogo']['name']);  
						
                         ##### create affiliates  logo dir if not exist  ##################
						 if(!file_exists($affiliateImagePath."/".$type))
								createdir($affiliateImagePath."/".$type);
						
                       	$afflogopath =  $affiliateImagePath."/".$type;

						$newImageFile =  $afflogopath."/".$affiliateInfoArray['contactinfo']['affiliateid']."_".$baseImageName;
						$result = @move_uploaded_file($_FILES['affiliatelogo']['tmp_name'], $newImageFile);
						$errorMsg = isValidImage($result,$newImageFile) ;
						if(empty($errorMsg))
						{ 
					       $resizeImageName = "r_".basename($newImageFile);
						   $resizeImagePath = $afflogopath."/".$resizeImageName;
						   resizeImageToDimension($newImageFile,$resizeImagePath,LOGO_WID,LOGO_HEG);
						   $affiliateInfoArray['personalize']['logo'] =  $http_location."/images/affiliates/".$affiliateInfoArray['contactinfo']['affiliateid']."/".$type."/".$resizeImageName;
						  #
						  # Update in db also
						  $query = "UPDATE $sql_tbl[mk_affiliates] SET 
						  logo = '".mysql_escape_string($resizeImageName)."' WHERE  
						  affiliate_id = '".$affiliateInfoArray[contactinfo][affiliateid]."' ";
						  db_query($query); 
						   
					  }
				
				  break;
     }
				  
 }
 function  updateAffiliateInfo($step,$_POST,$affiliateid)
{
	        global $weblog,$sql_tbl,$affiliatesImagesPath;
             switch($step)
		     {

                   case  "contact":
                              $query_data = array(
								                   "contact_name" => $_POST['contactname'],
								                   "company_name" => $_POST['company'] ,
								                   "contact_phone1"    => $_POST['phone1'],
								                   "contact_phone2"    =>  $_POST['phone2'],
								                   "affiliate_return_url"   =>  $_POST['website']
							                     );
					          func_array2update($sql_tbl['mk_affiliates'], $query_data, "affiliate_id='$affiliateid'");
							  	 
					               
                              break;
				   case "personal" :
                             
                          $query_data = array(
								                   "title"   => $_POST['title'],
								                   "themeid" => $_POST['theme'],
								                   "aff_auth_mechanism"  => $_POST['authtype'],
								                     
								                );
					      func_array2update($sql_tbl['mk_affiliates'], $query_data, "affiliate_id='$affiliateid'");
					     
                    	break;

					case "content" :
						
						   ######## if affiliate sends only user ######################### 
					   
                            if($_POST['personalizegift'] == 'P')
							{
								  $productids = '';
						          foreach($_POST['product'] as $key => $value)
								  {
									    $productids .= $value.",";
							      }
								  foreach($_POST['category'] as $key => $value)
								  {
									   $categories .=  $value.",";
								  }
							  
								   $product = (!empty($_POST['chkProduct']) ? trim($productids,",") : '');
								   $category = (!empty($_POST['chkCategory']) ? trim($categories,",") : '');
								   $tags = (!empty($_POST['chkTags']) ? $_POST['tags'] : '');
								   
								   $query_data_aff =  array(
										  		"contentprovider" => 'N'
										  		);
								   func_array2update($sql_tbl['mk_affiliates'], $query_data_aff, "affiliate_id='$affiliateid'")	;	  		
								   
								 
								    $chkProductMap  = func_get_affiliates_types($affiliateid);
			                        $chkCategoryMap = func_get_affiliates_category($affiliateid);
			                        $chkTagsMap     = func_get_affiliates_tags($affiliateid,false);
			                        # insert product types if affiliates add product later 
			                        if(empty($chkProductMap))
			                        {
			                        	(!empty($product)) ?  func_array2insert($sql_tbl['mk_affiliate_product_map'], array("product_type_id" => $product, "affiliate_id" => $affiliateid)) : '';
			                        }
			                        else 
			                        {
								   
								   		(!empty($product)) ?  func_array2update($sql_tbl['mk_affiliate_product_map'], array("product_type_id" => $product), "affiliate_id='$affiliateid'") : '';
			                        }
								   
			                        if(empty($chkCategoryMap))
			                        {
			                        	(!empty($category)) ?  func_array2insert($sql_tbl['mk_affiliate_category_map'], array("category_id" => $category,"affiliate_id" => $affiliateid)) : '';
			                        }
			                        else 
			                        {
								   		(!empty($category)) ?  func_array2update($sql_tbl['mk_affiliate_category_map'], array("category_id" => $category), "affiliate_id='$affiliateid'") : '';
			                        }
								   
			                        if(empty( $chkTagsMap))
			                        {
			                        	  (!empty($tags)) ?  func_array2insert($sql_tbl['mk_affiliate_tags'], array("tags" => $tags, "affiliate_id" => $affiliateid)) : '';
			                        }
			                        else 
			                        {
								      (!empty($tags)) ?  func_array2update($sql_tbl['mk_affiliate_tags'], array("tags" => $tags), "affiliate_id='$affiliateid'") : '';
			                        }
								 
								     
							}
							######## if affiliate sends  user and content #########################
							elseif($_POST['personalizegift'] == 'C')
							{
								 
								 $ptype = '';
								
								 foreach($_POST[producttype] as $key => $value)
								 {
								 	 $ptype .= $value.",";
								 }
								 $ptype = trim($ptype,",");
								 $query_data_aff =  array(
										  		"contentprovider" => 'Y'
										  		);
								 func_array2update($sql_tbl['mk_affiliates'], $query_data_aff, "affiliate_id='$affiliateid'")	;	  	
								$chkProductMap  = func_get_affiliates_types($affiliateid);	
								if(empty($chkProductMap))
			                    {
			                        	(!empty($ptype)) ?  func_array2insert($sql_tbl['mk_affiliate_product_map'], array("product_type_id" => $ptype, "affiliate_id" => $affiliateid)) : '';
			                    }
			                    else 
			                    {
								   
								  		(!empty($ptype)) ?  func_array2update($sql_tbl['mk_affiliate_product_map'], array("product_type_id" => $ptype), "affiliate_id='$affiliateid'") : '';  
			                    } 
								
						  }
        
					break;
			
		   }
}


# 
# function load customer informaion
#
function func_load_login_customer_info($login)
{
	   global $weblog,$sql_tbl,$sqllog;
       $customers = $sql_tbl['customers'] ;
       $weblog->info("Function Name : func_load_login_customer_info($login)  File Name : ".__FILE__);
       $query = "SELECT * FROM  $customers WHERE login = '".$login."' ";
       $sqllog->debug("Function Name : func_load_login_customer_info  SQL Query : ".  $query);
       $result = func_query($query);
       
       return  $result;

}
/* Keeping this function in this file util func.mkcore.php does not updated on server */
function func_load_all_public_product_type()
{
    global $sql_tbl;
    $producttypetable=$sql_tbl["mk_product_type"];
    $query           =
        "select $producttypetable.id, $producttypetable.name, $producttypetable.label,$producttypetable.image_t, $producttypetable.type from $producttypetable where  $producttypetable.is_active=1 AND $producttypetable.type='P' order by $producttypetable.id asc";
    $result          =db_query($query);

    $producttypes=array();

    if ($result)
        {
        $i=0;

        while ($row=db_fetch_row($result))
            {
            $producttypes[$i] = $row;
            $i++;
            }
        }

    return $producttypes;
}



function func_get_affiliate_products($affiliateid,$offset)
{
	global $sql_tbl;
	global $sqllog;
	$affiliateproductmap = $sql_tbl['mk_affiliate_generated_code'];
	$product_type = $sql_tbl['mk_product_type'];
	$sql="select a.id,a.name,a.image_t  from $product_type a,$affiliateproductmap b 
	where a.id=b.product_type_id and b.affiliate_id=$affiliateid  order by a.id asc limit 3 offset $offset ";

	

	$result =db_query($sql);
	$productdetail = array();
	
		
	if($result)
	$productdetail = db_fetch_array($result);

	return $productdetail;
	
}

function func_load_product_styles_by_product_id($productTypeId){
    global $sql_tbl;
    $productStyleTable  =   $sql_tbl["mk_product_style"];
    $queryString        =   "SELECT id,product_type,name,image_t,price FROM  $productStyleTable where
                             product_type='$productTypeId'  AND styletype='P'  
                             ORDER BY price ASC";

    $productDetails                 =  func_query($queryString);
   return $productDetails;

}
### Added function for yahoo template store 
function load_affiliate_template($affiliateid){
	return array();
}

#
#Function to  get the height of lines
#
#
#Collecting all header data in variables
#
$fonttype =  "/fonts/arial.ttf";
$fontsize = 7;
$cricinfotext = explode(":",$HTTP_POST_VARS['quote']);
$fonttext = stripslashes($cricinfotext[0]);
$usernametext = "- ".stripslashes($cricinfotext[1]);

function get_expected_hight_of_image_as_per_the_text_string($textSize, $font, $text, $width){
   $dimensions = imagettfbbox($textSize, 0, $font, " "); //use a custom string to get a fixed height.
   $text = str_replace ("\r", '', $text); //Remove windows line-breaks
   $srcLines = split ("\n", $text); //Split text into "lines"
   $dstLines = Array(); // The destination lines array.

   foreach ($srcLines as $currentL) {
       $line = '';
       $words = split (" ", $currentL); //Split line into words.
       foreach ($words as $word) {
           $dimensions = imagettfbbox($textSize, 0, $font, $line.$word);
           $lineWidth = $dimensions[4] - $dimensions[0]; // get the length of this line, if the word is to be included
           if ($lineWidth > $width && !empty($line) ) { // check if it is too big if the word was added, if so, then move on.
               $dstLines[] = ' '.trim($line); //Add the line like it was without spaces.
               $line = '';
           }
           $line .= $word.' ';
       }
       $dstLines[] =  ' '.trim($line); //Add the line when the line ends.
   }
   $lineCount = count($dstLines);
   //Calculate lineheight by common characters.
   $dimensions = imagettfbbox($textSize, 0, $font, "MXQJPmxqjp123"); //use a custom string to get a fixed height.
   $lineHeight = $dimensions[1] - $dimensions[5]; // get the heightof this line
   $textimageheight = $lineHeight *  $lineCount;

   return $textimageheight+($lineHeight * 4);
}

?>
