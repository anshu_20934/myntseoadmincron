<?php

if(!(defined('AREA_TYPE')&&constant('AREA_TYPE')=='A')){
	require_once "$xcart_dir/auth.php";
}
require_once "$xcart_dir/include/class/class.mail.multiprovidermail.php";

function sendResetPasswordMail($mailto,$template=null,$firstname=null){
		global $sql_tbl, $xcart_dir, $http_location;
		if(empty($template)){
			$template = "forgot_password";
		}
        $rand_id= md5($mailto.rand().time());
        while(true){
            $query = "select login from xcart_customers where resetpwd_key = '$rand_id'";
            $res = func_query_first($query);
            if(empty($res)){
                break;
            }else{
                $rand_id = md5($mailto.rand().time());
            }
        }
        $update=array("resetpwd_key"=>$rand_id,"isvalid_resetkey"=>"1");
        $mailto = mysql_real_escape_string($mailto);
        $result=func_array2update('xcart_customers', $update, "login='".$mailto."'");
        $rowsAffected = mysql_affected_rows();
	    
        if(empty($result) || $rowsAffected == 0 ) {return "";}
        
	    $link="<a href='".HostConfig::$httpHost."/reset_password.php?key=".$rand_id."'>".HostConfig::$httpHost."/reset_password.php?key=".$rand_id." </a>";
		$customerSupportCall = WidgetKeyValuePairs::getWidgetValueForKey('customerSupportCall');
	    
        if(empty($firstname)) {
        	if($template=="facebookregistation"){
        		$args = array( "LINK" => $link, "CUST_SUPP" => $customerSupportCall, 'FIRST_NAME'=>"$mailto");
        	} else {	
        		$args = array( "LINK" => $link, "CUST_SUPP" => $customerSupportCall, 'FIRST_NAME'=>'');
        	}
        	
        } else {
        	$args = array( "LINK" => $link, "CUST_SUPP" => $customerSupportCall, 'FIRST_NAME'=>"$firstname");
        }	

        $mail_detail = array(
                        "to"=>trim($mailto),
            			"template"=>$template,	
                        "from_name"=>'Myntra no-reply',
                        "from_email"=>"no-reply@myntra.com",
                       	"mail_type"=> MailType::CRITICAL_TXN
            		  );
        $multiProviderMailer = new MultiProviderMailer($mail_detail, $args);
        $multiProviderMailer->sendMail();
        return "";
} 

?>
