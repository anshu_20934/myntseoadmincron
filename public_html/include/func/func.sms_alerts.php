<?php

function func_send_sms($mobile,$msg){
	func_send_sms_cellnext($mobile,$msg); 
}

/** 
 * Function to send SMS through the Cell Next Api. 
 * The parameters passed to it, are send it an XML message format defined by CellNext
 * @param string $mobile 10-Digit Indian Mobile No. (Country code 91 should not be prefixed to the mobile number)
 * @param string $msg The message to be sent via SMS to the specified mobile number
 */
function func_send_sms_cellnext($mobile,$msg) {
	
	/* With new bulk sms rules from 01-March-2011, there won't be any TD-MYNTRA showing on users mobile.
	 * We append a string 'Myntra: ' before the message content, so that user knows sms has come from myntra.
	 */
	$msg = 'MYNTRA: ' . $msg; 
	
	$url = 'http://203.212.70.100/psms/servlet/psms.Eservice2';
	$data = 'data=<?xml version="1.0" encoding="ISO-8859-1"?>';
	$data = $data . '<!DOCTYPE MESSAGE SYSTEM "http://127.0.0.1/psms/dtd/message.dtd" >';
	$data = $data . '<MESSAGE><USER USERNAME="myntra" PASSWORD="hotmail02"/>';
	$data = $data . '<SMS UDH="0" CODING="1" TEXT="' . $msg . '" PROPERTY="0" ID="1">';
	$data = $data . '<ADDRESS FROM="Myntra" TO="' . $mobile. '" SEQ="1" TAG="Myntra Verification SMS" />';
	$data = $data . '</SMS></MESSAGE>&action=send';

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url); 
	curl_setopt($ch, CURLOPT_RETURNTRANSFER,1); 
	curl_setopt($ch, CURLOPT_POST, 1); 
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data); 
	$response = curl_exec($ch);
	
	curl_close($ch);

	// We can capture the $response, and so sms validation to check if user has received the sms
	// Cell Next APIs provide excellent response codes. We can extend functionality here, and do some string matching on $response
	// to accurately identify, if the SMS has been sent.
	// return $response or true..;
}

function func_send_sms_async($mobile,$msg){
	$array=array("queuedtime"=>date("Y/m/d H:i:s"),'sent'=>1);
	$id=func_array2insert("myntra_notification_message_details", $array);
	$array=array("sms_details_id"=>$id,"mode"=>1,"sendto"=>$mobile,'body'=>mysql_escape_string($msg));
	func_array2insert("myntra_notification_sms_details", $array);
}
?>