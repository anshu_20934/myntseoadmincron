<?php
require_once(dirname(__FILE__)."/../../env/RemoteTimeouts.Config.php");
require_once(dirname(__FILE__)."/../../include/solr/solrProducts.php");
include_once(dirname(__FILE__)."/../../include/dao/class/search/class.MyntraLPAdapter.php");
include_once(dirname(__FILE__)."/../../include/dao/class/search/class.MyntraPageDBAdapter.php");
include_once($xcart_dir."/Profiler/Profiler.php");

class SearchURLGenerator
{
	private $parameters_array = array();
	private $url_desc;
	private $is_valid = true;
	private $gen_link = false;
	private $is_landingpage = false;

    private $isactive = false;
    private $expiryDate;
    private $landingPageType;

	private $parent_landingpage_url;
	public function __construct($parameters, $isactive=false, $expiryDate="", $landingPageType=""){
		if(!empty($parameters['url_desc'])){
			$this->url_desc=$parameters['url_desc'];
		}
		unset($parameters['url_desc']);
		if(!empty($parameters['is_landingpage'])){
			$this->is_landingpage=true;
		}
		unset($parameters['is_landingpage']);
		if(!empty($parameters['parent_landingpage_url'])){
			$this->parent_landingpage_url=$parameters['parent_landingpage_url'];
		}
		unset($parameters['parent_landingpage_url']);

        if(!empty($parameters['generatelink']) && ($parameters['generatelink'] == 'y')){
            $this->gen_link=true;
        }
		unset($parameters['generatelink']);

		$this->parameters_array = $parameters;
		//Sort the parameter array
		$this->sortParameterArray();
		$this->validateParameters();

        $this->isactive = $isactive;
        $this->expiryDate = $expiryDate;
        $this->landingPageType = $landingPageType;
	}

	private function sortParameterArray(){
		ksort($this->parameters_array);
	}

	private function validateParameters(){
		global $xcache;
		$valid_solr_fields=$xcache->fetch("valid_solr_fields");
		$valid_solr_fields_dynamic=$xcache->fetch("valid_solr_fields_dynamic");
		if($valid_solr_fields == NULL || $nocache == 1){
			$valid_solr_fields = array();
			$valid_solr_fields_dynamic = array();
			$solr_product = new SolrProducts();
                        $profilerHandle = Profiler::startTiming("search-url-generator-curl");
			$host = $solr_product->getApacheSolrService()->getHost();
			$port = $solr_product->getApacheSolrService()->getPort();
			$ch = curl_init("http://$host:$port/solr/sprod/admin/file/?file=schema.xml");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HEADER, 0);


			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, \RemoteTimeouts::$localDCConnectTimeout);
			curl_setopt($ch, CURLOPT_TIMEOUT, \RemoteTimeouts::$solrSchemaXmlTimeout);

			$test = curl_exec($ch);
			curl_close($ch);
                        Profiler::endTiming($profilerHandle);
			$doc = new SimpleXmlElement($test, LIBXML_NOCDATA);
			foreach ($doc->fields->field as $field) {
				array_push($valid_solr_fields, $field['name'].'');
			}
			foreach ($doc->fields->dynamicField as $field) {
				array_push($valid_solr_fields_dynamic, $field['name'].'');
			}

			#$xcache->store("valid_solr_fields", $valid_solr_fields);
			#$xcache->store("valid_solr_fields_dynamic", $valid_solr_fields_dynamic);

		}
		foreach ($this->parameters_array as $key=>$parameter){
			if(!in_array($key, $valid_solr_fields) && !$this->isDyncamicField($key, $valid_solr_fields_dynamic) && ($key != "landingPageType")){
				echo 'Not a valid parameter : '.$key.'<br>';
				$this->is_valid=false;
			}
		}
	}

	private function isDyncamicField($key, $valid_solr_fields_dynamic){
		$valid_solr_fields_dynamic=str_replace('*',"", $valid_solr_fields_dynamic);
		foreach ($valid_solr_fields_dynamic as $field) {
			$pos = strpos($key, $field);
			if($pos === false){
				continue;
			}else{
				return true;
			}
		}
		return false;
	}

	public function getParameters(){
		return $this->parameters_array;
	}

	public function getSolrQueryAndDesc($parameter_page_id){
        // Check if generate link is true, if not then don't generate the links
        if(!$this->gen_link){
			return array("page_desc"=>$this->getURLDesc($this->parameters_array));
        }

		$solr_query = '(';
		foreach ($this->parameters_array as $key=>$parameter) {
			if(!empty($parameter)){
				$solr_query .= $key.":";
				$solr_query .= $this->getParameterValues($parameter);
				$solr_query .= ' AND ';
			}
		}
		$solr_query = substr($solr_query, 0, -5);
		$solr_query .= ')';

		$lpAdapter=MyntraLPAdapter::getInstance($this->parameters_array['login'], $this->parameters_array['email']);

		if(!empty($this->parent_landingpage_url)){
			if($lpAdapter->isLandingPage($this->parent_landingpage_url)){
				$parent_solr_query=$lpAdapter->getSolrQueryForLandingPage($this->parent_landingpage_url);
				$solr_query = $this->mergeSOLRQueries($solr_query,$parent_solr_query);
			}else{
				// If parent Landing page url is passed but it is not a landing page, dont generate the parameterized page and landing page just return
				return array("page_desc"=>$this->url_desc);
			}
		}
		// check if query already exist
		$page_key_desc = $lpAdapter->getKeyAndDescForSolrQuery($solr_query);
		$page_key =$page_key_desc['page_key'];
		$page_desc =$page_key_desc['page_desc'];
		$error;
		if(empty($parameter_page_id) && !empty($page_key)){
			$error = "Page Already Exists";
		}else{
			if($this->is_valid){
				$page_desc=$this->getURLDesc($this->parameters_array);

                if(!empty($parameter_page_id)){
				    $page_id_key = $lpAdapter->saveOrUpdateMyntraPage($solr_query,$page_desc,$parameter_page_id);
                    $page_id=$page_id_key['id'];
                    $page_key=$page_id_key['page_key'];
                    $lpAdapter->saveOrUpdateMyntraLandingPage(array("parameterized_page_id"=>$page_id,"page_url"=>$page_desc, "landingPageType" => $this->landingPageType, "isactive" => $this->isactive, "expiryDate" => $this->expiryDate));
			        $error = "Page Edited Successfully";
                }else{
				    $page_id_key = $lpAdapter->saveOrUpdateMyntraPage($solr_query,$page_desc,'');
                    $page_id=$page_id_key['id'];
                    $page_key=$page_id_key['page_key'];
                    if ($this->is_landingpage) {
                        if(! $lpAdapter->isLandingPage(page_url)){
                            $lpAdapter->saveOrUpdateMyntraLandingPage(array("parameterized_page_id"=>$page_id,"page_url"=>$page_desc, "landingPageType" => $this->landingPageType, "isactive" => $this->isactive, "expiryDate" => $this->expiryDate));
                        }
                    }
                }
			}else{
				exit;
			}
		}

		return array("page_key"=>$page_key,"page_desc"=>$page_desc,"error"=>$error);
	}

	private function getURLDesc($parameters_array){
		if(!empty($this->url_desc)){
			return $this->url_desc;
		}

		$values = array_values($parameters_array);
		if(count($parameters_array)==1){
			return strtolower(str_replace(array(","," "), "-", $values[0]));
		}
		else {
			/* URL generation order
			 * brand>gender>team name>sport/usage>article type
			 * */


			$url_desc='';
			// Brand
			$url_desc .= str_replace(array(","," "), "-", $parameters_array['brands_filter_facet']);
			// Gender
			if(!empty($url_desc) && !empty($parameters_array['global_attr_gender'])){
				$url_desc .= "-";
			}
			$url_desc .= str_replace(array(","," "), "-", $parameters_array['global_attr_gender']);

			// Display Category
			if(!empty($url_desc) && !empty($parameters_array['display_category_facet'])){
				$url_desc .= "-";
			}
			$url_desc .= str_replace(array(","," "), "-", $parameters_array['display_category_facet']);

			// Team Name
			if(!empty($url_desc) && !empty($parameters_array['team_filter_facet'])){
				$url_desc .= "-";
			}
			$url_desc .= str_replace(array(","," "), "-", $parameters_array['team_filter_facet']);
			// Usage
			if(!empty($url_desc) && !empty($parameters_array['global_attr_usage'])){
				$url_desc .= "-";
			}
			$url_desc .= str_replace(array(","," "), "-", $parameters_array['global_attr_usage']);

			// Article Type
			if(!empty($url_desc) && !empty($parameters_array['global_attr_article_type_facet'])){
				$url_desc .= "-";
			}
			$url_desc .= str_replace(array(","," "), "-", $parameters_array['global_attr_article_type_facet']);

			if(empty($url_desc)){
				$url_desc = str_replace(array(","," "), "-", implode("-", $parameters_array));
			}

			return strtolower($url_desc);
		}
	}

	private function getParameterValues($parameter){
		$values = explode(',', $parameter);
		sort($values);
		$return_str = '(';
		foreach ($values as $value) {
			// for range query dont put it inside double quotes and the assuming that range query will be of format [0 TO 100]
			if(substr($value,0,1) =='[' && substr($value,-1,1) ==']'){
				$return_str .=$value.' OR ';
			}else{
				$return_str .='"'.$value.'" OR ';
			}
		}
		$return_str = substr($return_str, 0, -4);
		$return_str .= ')';

		return $return_str;
	}

	// This method will merge two queries.

	private function mergeSOLRQueries($solr_query,$parent_solr_query){
		$parameter_array_parent = $this->getArrayFromQuery($parent_solr_query);
		$parameter_array_child = $this->getArrayFromQuery($solr_query);
		foreach ($parameter_array_parent as $key=>$value){
			// check in child if this parameter exist
			$paraent_values = $value;
			$child_values = $parameter_array_child[$key];
			if(!empty($child_values)){
				$merged_values = $this->mergeAttributesValues($paraent_values,$child_values);
				$parameter_array_parent[$key]=$merged_values;
			}
			// After merging remove it from child so that in the last we can merge both arrays
			unset($parameter_array_child[$key]);
		}
		$parameter_array_parent = array_merge($parameter_array_parent,$parameter_array_child);
		ksort($parameter_array_parent);
		$solr_query_final .= '(';
		foreach ($parameter_array_parent as $key=>$value) {
			if(!empty($value) && !empty($key)){
				$solr_query_final .= $key.":";
				$solr_query_final .= "(".$value.")";
				$solr_query_final .= ' AND ';
			}
		}
		$solr_query_final = substr($solr_query_final, 0, -5);
		$solr_query_final .= ')';
		return $solr_query_final;

	}

	private function mergeAttributesValues($paraent_values,$child_values){
		$final_values = $child_values;
		$final_value_array = explode(" OR ", $final_values);
		sort($final_value_array);
		return implode(" OR ",$final_value_array);
	}

	public function getArrayFromQuery($solr_query){
		/* Example of type of URL to parse
		 * (brands_filter_facet:("Anita AND Banita" OR "Nike") AND global_attr_base_colour:("Yellow") AND global_attr_gender:("men"))
		 *
		 * first explode by ") AND" to get all the attributes But you need to remove starting "(" and last 2 "))"
		 *
		 */
		$search_parameters = explode(") AND ", substr($solr_query,1,-2));

		/*
		 * Sample output after this step
		 * Array
		 * (
		 * [0] => brands_filter_facet:("Anita AND Banita" OR "Nike"
		 * [1] => global_attr_base_colour:("Yellow"
		 * [2] => global_attr_gender:("men"
		 * )
		 *
		 */

		// Array to store the parameter values as key=>value
		$search_parameter_value_list = array();
		foreach ($search_parameters as $search_parameter){
			/*
			 * Sample output after this explode where we will parse values with ":("
			 * Array
			 * (
			 * [0] => brands_filter_facet
			 * [1] => "Anita AND Banita" OR "Nike"
			 * )
			 *
			 * Array
			 * (
			 * [0] => global_attr_base_colour
			 * [1] => "Yellow"
			 * )
			 *
			 * Array
			 * (
			 * [0] => global_attr_gender
			 * [1] => "men"
			 * )
			 */
			$search_parameter_values = explode(":(", $search_parameter);
			$search_parameter_value_list[$search_parameter_values[0]] = $search_parameter_values[1];
		}
		/*
		 * Output in the array is in the desired format now
		 * Array
		 * (
		 * [brands_filter_facet] => "Anita AND Banita" OR "Nike"
		 * [global_attr_base_colour] => "Yellow"
		 * [global_attr_gender] => "men"
		 * )
		 *
		 */
		return $search_parameter_value_list;
	}
}
