<?php
/*****************************************************************************\
+-----------------------------------------------------------------------------+
| X-Cart                                                                      |
| Copyright (c) 2001-2006 Ruslan R. Fazliev <rrf@rrf.ru>                      |
| All rights reserved.                                                        |
+-----------------------------------------------------------------------------+
| PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
| FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
| AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
|                                                                             |
| THIS  AGREEMENT  EXPRESSES  THE  TERMS  AND CONDITIONS ON WHICH YOU MAY USE |
| THIS SOFTWARE   PROGRAM   AND  ASSOCIATED  DOCUMENTATION   THAT  RUSLAN  R. |
| FAZLIEV (hereinafter  referred to as "THE AUTHOR") IS FURNISHING  OR MAKING |
| AVAILABLE TO YOU WITH  THIS  AGREEMENT  (COLLECTIVELY,  THE  "SOFTWARE").   |
| PLEASE   REVIEW   THE  TERMS  AND   CONDITIONS  OF  THIS  LICENSE AGREEMENT |
| CAREFULLY   BEFORE   INSTALLING   OR  USING  THE  SOFTWARE.  BY INSTALLING, |
| COPYING   OR   OTHERWISE   USING   THE   SOFTWARE,  YOU  AND  YOUR  COMPANY |
| (COLLECTIVELY,  "YOU")  ARE  ACCEPTING  AND AGREEING  TO  THE TERMS OF THIS |
| LICENSE   AGREEMENT.   IF  YOU    ARE  NOT  WILLING   TO  BE  BOUND BY THIS |
| AGREEMENT, DO  NOT INSTALL OR USE THE SOFTWARE.  VARIOUS   COPYRIGHTS   AND |
| OTHER   INTELLECTUAL   PROPERTY   RIGHTS    PROTECT   THE   SOFTWARE.  THIS |
| AGREEMENT IS A LICENSE AGREEMENT THAT GIVES  YOU  LIMITED  RIGHTS   TO  USE |
| THE  SOFTWARE   AND  NOT  AN  AGREEMENT  FOR SALE OR FOR  TRANSFER OF TITLE.|
| THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY GRANTED BY THIS AGREEMENT.      |
|                                                                             |
| The Initial Developer of the Original Code is Ruslan R. Fazliev             |
| Portions created by Ruslan R. Fazliev are Copyright (C) 2001-2006           |
| Ruslan R. Fazliev. All Rights Reserved.                                     |
+-----------------------------------------------------------------------------+
\*****************************************************************************/

#
# $Id: adv_info.php,v 1.9 2006/01/11 06:55:58 mclap Exp $
#
# Save statistic about Advertising campaigns
#

if ( !defined('XCART_SESSION_START') ) { header("Location: ../"); die("Access denied"); }

x_session_register ("adv_campaignid");
$adv_campaignid_content = getMyntraCookie("adv_campaignid");
$adv_campaignid_time_content = getMyntraCookie("adv_campaignid_time");
if ((empty($adv_campaignid) && !empty($adv_campaignid_content) && !empty($adv_campaignid_time_content))) {
	if ($adv_campaignid_time_content >= time()) {
		$adv_campaignid = 'Y';
	} else {
		setMyntraCookie("adv_campaignid", "", 0, "/", $cookiedomain, 0);
		setMyntraCookie("adv_campaignid_time", "", 0, "/", $cookiedomain, 0);
	}
}

#
# For type 'G' (use GET parameter(s))
#
if ($HTTP_GET_VARS && $REQUEST_METHOD == 'GET' && empty($adv_campaignid)) {
	$gets = func_query("SELECT campaignid, data, type FROM $sql_tbl[partner_adv_campaigns] USE INDEX (type) WHERE type = 'G'");
	$_campaignid = 0;
	if ($gets) {
		foreach ($gets as $v) {	
			$tmp = func_parse_str($v['data']);
			if (!empty($tmp)) {
				$cnt = 0;
				foreach ($tmp as $key => $value) {
					if ($HTTP_GET_VARS[$key] == $value && isset($HTTP_GET_VARS[$key]))
						$cnt++;
				}

				if ($cnt == count($tmp)) {
					$QUERY_STRING = implode("&", array_diff(explode("&", $QUERY_STRING), explode("&", $v['data'])));
					$_campaignid = $v['campaignid'];
					$_type = $v['type'];
					break;
				}
			}
		}
	}
}

#
# For type 'R' (use HTTP referer parameter)
#
if ($HTTP_REFERER && $REQUEST_METHOD == 'GET' && !$_campaignid && empty($adv_campaignid)) {
	$refs = func_query("SELECT campaignid, data FROM $sql_tbl[partner_adv_campaigns] USE INDEX (type) WHERE type IN ('R','L')");
	if ($refs) {
		foreach ($refs as $v) {
			if ($HTTP_REFERER == $v['data']) {
				$_campaignid = $v['campaignid'];
				$_type = 'R';
				break;
			}
		}
	}
}

#
# Save campaignid if not empty
#
if ($_campaignid) {
	if ($_type != 'L')
		db_query("REPLACE INTO $sql_tbl[partner_adv_clicks] VALUES ('$_campaignid', '".time()."')");
	$adv_campaignid = $_campaignid;
	$partner_cookie_length = ($config["XAffiliate"]["partner_cookie_length"] ? $config["XAffiliate"]["partner_cookie_length"]*3600*24 : 0);

	if ($partner_cookie_length) {
		$expiry = mktime(0,0,0,date("m"),date("d"),date("Y")+1);
		setMyntraCookie("adv_campaignid", $adv_campaignid, $expiry, "/", $cookiedomain, 0);
		setMyntraCookie("adv_campaignid_time", time()+$partner_cookie_length, $expiry, "/", $cookiedomain, 0);
	}
	func_header_location(basename($PHP_SELF)."?".$QUERY_STRING);
}

?>
