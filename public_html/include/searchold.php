<?php
/*****************************************************************************\
+-----------------------------------------------------------------------------+
| X-Cart                                                                      |
| Copyright (c) 2001-2006 Ruslan R. Fazliev <rrf@rrf.ru>                      |
| All rights reserved.                                                        |
+-----------------------------------------------------------------------------+
| PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
| FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
| AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
|                                                                             |
| THIS  AGREEMENT  EXPRESSES  THE  TERMS  AND CONDITIONS ON WHICH YOU MAY USE |
| THIS SOFTWARE   PROGRAM   AND  ASSOCIATED  DOCUMENTATION   THAT  RUSLAN  R. |
| FAZLIEV (hereinafter  referred to as "THE AUTHOR") IS FURNISHING  OR MAKING |
| AVAILABLE TO YOU WITH  THIS  AGREEMENT  (COLLECTIVELY,  THE  "SOFTWARE").   |
| PLEASE   REVIEW   THE  TERMS  AND   CONDITIONS  OF  THIS  LICENSE AGREEMENT |
| CAREFULLY   BEFORE   INSTALLING   OR  USING  THE  SOFTWARE.  BY INSTALLING, |
| COPYING   OR   OTHERWISE   USING   THE   SOFTWARE,  YOU  AND  YOUR  COMPANY |
| (COLLECTIVELY,  "YOU")  ARE  ACCEPTING  AND AGREEING  TO  THE TERMS OF THIS |
| LICENSE   AGREEMENT.   IF  YOU    ARE  NOT  WILLING   TO  BE  BOUND BY THIS |
| AGREEMENT, DO  NOT INSTALL OR USE THE SOFTWARE.  VARIOUS   COPYRIGHTS   AND |         	    

| Portions created by Ruslan R. Fazliev are Copyright (C) 2001-2006           |
| Ruslan R. Fazliev. All Rights Reserved.                                     |
+-----------------------------------------------------------------------------+
\*****************************************************************************/

#
# $Id: search.php,v 1.141.2.8 2006/06/06 05:56:52 svowl Exp $
#

if ( !defined('XCART_SESSION_START') ) { header("Location: ../"); die("Access denied"); }

$advanced_options = array("productcode", "productid", "provider", "price_max", "avail_max", "weight_max", "forsale", "flag_free_ship", "flag_ship_freight", "flag_global_disc", "flag_free_tax", "flag_min_amount", "flag_low_avail_limit", "flag_list_price", "flag_vat", "flag_gstpst", "manufacturers");

$sort_fields = array(
	"productcode" 	=> func_get_langvar_by_name("lbl_sku"),
	"title" 		=> func_get_langvar_by_name("lbl_product"),
    "price" 		=> func_get_langvar_by_name("lbl_price"),
    "qtysold"       => func_get_langvar_by_name("lbl_qtysold"),
    "status"        => func_get_langvar_by_name("lbl_status"),
    "style"			=> func_get_langvar_by_name("lbl_productstyle"),
	"orderby"		=> func_get_langvar_by_name("lbl_default"),
	"date"         => "date"

);

if ($config["Appearance"]["display_productcode_in_list"] != "Y")
	unset($sort_fields["productcode"]);

if($current_area == 'A' || $current_area == 'P') {
    $sort_fields["quantity"] = func_get_langvar_by_name("lbl_in_stock");
}

if (empty($search_data)) {
	$search_data = array();
}

if ($REQUEST_METHOD == "POST" && $mode == 'search') {
	#
	# Update the session $search_data variable from $posted_data
	#
	$seachKeyword ='';
	 // check whether start date and end date are entered
     if (!empty($startdate) && !empty($enddate))
      {
    	   $stdate = explode("/",$startdate);
		   $posted_data["start_date"] = mktime(0,0,0,$stdate[0],$stdate[1],$stdate[2]);
		   $enddate = explode("/",$enddate);
		   $posted_data["end_date"] = mktime(23,59,59,$enddate[0],$enddate[1],$enddate[2]);
	 }  
		
		
	if (!empty($posted_data)) {
		$need_advanced_options = false;
		foreach ($posted_data as $k=>$v) {
			if (!is_array($v) && !is_numeric($v))
				$posted_data[$k] = stripslashes($v);

			if (in_array($k, $advanced_options) && $v !== "")
				$need_advanced_options = true;
		}

		# Update the search statistics
		if ($posted_data["substring"]) {
			    
				db_query("INSERT INTO $sql_tbl[stats_search] (search, date) VALUES ('".addslashes($posted_data["substring"])."', '".time()."')");
		}

		if (!$need_advanced_options)
			$need_advanced_options = (doubleval($posted_data["price_min"]) != 0 || intval($posted_data["avail_min"]) != 0 || doubleval($posted_data["weight_min"]) != 0);
		if (!$need_advanced_options && $current_area == "C" && !empty($posted_data["categoryid"]))
			$need_advanced_options = true;
 
		$posted_data["need_advanced_options"] = $need_advanced_options;
 
		#
		# Data convertation for Feature comparison module, Not in use 
		#
		###if(!empty($active_modules['Feature_Comparison'])) {
			##include $xcart_dir."/modules/Feature_Comparison/search_define.php";
		##}

		if (empty($search_data["products"]["sort_field"])) {
			$posted_data["sort_field"] = "title";
			$posted_data["sort_direction"] = 0;
		}
		else {
			$posted_data["sort_field"] = $search_data["products"]["sort_field"];
			$posted_data["sort_direction"] = $search_data["products"]["sort_direction"];
		}

		$posted_data['is_modify'] = $posted_data['is_modify'];
		$posted_data['is_export'] = $posted_data['is_export'];
		func_unset($posted_data, '_');
     	 $search_data["products"] = $posted_data;
     	// print_r($search_data["products"]);
     	//exit;
     	
     	
     	//if(!empty($search_data['products']['by_TypeName'])){
     		 $seachKeyword = $search_data['products']['substring'];	
     		
     	//}else{
     	 //$seachKeyword = $search_data['products']['substring']." ".$search_data['products']['by_TypeName'];	
     		
     	//}
     //added by chander
     	//print_r($search_data["products"]);
     	//exit;
     	
		//$seachKeyword = urlencode($search_data['products']['substring']);
		//$search_data['products']['substring'] =trim($search_data['products']['substring']);
		//$seachKeyword = preg_replace('#([ ])+#','-',$search_data['products']['substring']);
		 
	}

	//If logged in user is mykriti user redirect it to mykriti search result
	if($current_area == 'C')
	{
		    // update the search statistics
			if(!empty($seachKeyword)){
          
				// added by surjan
				// fetch all the product type for synonyms in searched key 
				//checking for nonalphanumeric charcter.
				$seachKeyword = preg_replace("/[^a-zA-Z0-9s-]/", " ", $seachKeyword);
				
				$filteredKeyword = ''; 
				$additional_keyword_field  = '';
				$filteredKeyword = $seachKeyword;
				// filter the keywords
				
				check_product_type_synonyms($seachKeyword,$filteredKeyword);
//				echo $seachKeyword;
//				echo $filteredKeyword ;
//				exit;
//	    		echo "keyword".$seachKeyword ;
//	    		echo  "filteredkeyword".$filteredKeyword ;
//	    		exit;
			 // search for dirty words in the keywords entered without any product type
				$filthyWordStatus = false;
			   //echo "filter ".$filteredKeyword;
				
				$keysArray = explode(" ",$filteredKeyword) ;
				$curseWordArray=file_get_contents(dirname(__FILE__)."/../skin1/dirtywords.txt");
				$curseWordArray = preg_split("/(\r\n|\r|\n)/",$curseWordArray) ;

     			foreach($keysArray as $key=>$value){
			 		foreach($curseWordArray as $curse_word_line){
			 					if(strcmp(strtolower(trim($value)),strtolower(trim($curse_word_line)))){
			 						$filterKeywords .= trim($value)." ";
			 					}else{
			 						$filthyWordStatus = true;
			 					}
			 					 	
			 		}
     			}
					//$filterKeywords = trim($filterKeywords);
					//$filteredKeyword= $filterKeywords ; 
					
				if(!$filthyWordStatus){
					//added by chander
					 $sql = "select name from  mk_product_type where id='".$search_data['products']['by_type']."' ";
					$result = db_query($sql);
					while($row = db_fetch_array($result)){
 					$single_word = $row['name'];
  					}
					
					if((!empty($search_data['products']['by_type']))&&(!strcmp(trim($seachKeyword),trim($filteredKeyword)))){
						
						$seachKeyword = $filteredKeyword." ".strtolower(trim($single_word));
					}
					
					$key_id = track_search_keywords($seachKeyword,$filteredKeyword);
					// newly added 
					$searchKeys = explode(" ",trim($filteredKeyword));
					$whereCondition =  array();
					$condition = " WHERE xcart_products.statusid = 1008  ";
					foreach($searchKeys as $key){
						$whereCondition[] = " ( MATCH(xcart_products.product) AGAINST ('".$key."') OR MATCH(xcart_products.fulldescr) AGAINST ('".$key."') OR MATCH(xcart_products.keywords) AGAINST ('".$key."') ) ";
					}
									
					$sql = "select count(*) as ctr, pt.name from xcart_products INNER JOIN mk_product_type as pt on pt.id=xcart_products.product_type_id ";
					if(!empty($whereCondition)){
						$sql .= $condition . " AND ( ".implode(" AND ",$whereCondition). " ) group by product_type_id ";
					}else{
					  	$sql .= $condition . "  group by product_type_id ";
					}
					
					$result = db_query($sql);
					$count_for_basekeyword = 0;
					while($row = db_fetch_array($result)){
 						 $product_count_words = $row['ctr'];
 						  $product_base_word  = trim($filteredKeyword)." ".strtolower(trim($row['name']));
 						 $sql = "select key_id from mk_stats_search where orignal_keyword  = '".trim($product_base_word)."'" ;
 						 $result1 = db_query($sql);
 						 $row1 = db_fetch_array($result1);
 						
 						if($product_count_words > 0){
 							 $query_data = array();
 							 $query_data = array(
 							               "count_of_products" => $product_count_words,
 							               "flag_count" =>1
 							 );
							 if($product_count_words >0)
							 func_array2update("mk_stats_search",$query_data," key_id = '".$row1['key_id']."'");  
 						}
 						$count_for_basekeyword = $count_for_basekeyword + $product_count_words ;
 					
  					}
					if($count_for_basekeyword >0)
				    db_query("UPDATE mk_stats_search SET count_of_products = '".$count_for_basekeyword."' WHERE orignal_keyword = '".trim($filteredKeyword)."'");
	         		$seachKeyword =  trim($seachKeyword);
	         	    $filter_search_query = preg_replace('#([ ])+#','-',$seachKeyword);
	         	    func_header_location($http_location."/".$filter_search_query."/search/$key_id/");	         	    
				}
				else{
					
					$seachKeyword =  trim($seachKeyword);
	         	    $seachKeyword = preg_replace('#([ ])+#','-',$seachKeyword);
					func_header_location($http_location."/".$seachKeyword."/search/0/");
					
				}
			  }  
			  
			 
	}
	
	
}


if ($mode == "search") {


   	#
	# Perform search and display results
	#
	$data = array();

	$flag_save = false;

	#
	# Initialize service arrays
	#
	$fields = array();
	$fields_count = array();
	$from_tbls = array();
	$inner_joins = array();
	$left_joins = array();
	$where = array();
	$groupbys = array();
	$having = array();
	 $orderbys = array();
	
	$special_chars = false;

	#
	# Prepare the search data
	#
	if (!empty($sort) && isset($sort_fields[$sort])) {
		# Store the sorting type in the session
		$search_data["products"]["sort_field"] = $sort;
		$flag_save = true;
	}

	if (isset($sort_direction)) {
		# Store the sorting direction in the session
		$search_data["products"]["sort_direction"] = $sort_direction;
		$flag_save = true;
	}



	if ($current_area == 'C' && !empty($config['Appearance']['products_order']) && empty($search_data["products"]["sort_field"]))
	{
		$search_data["products"]["sort_field"] = $config['Appearance']['products_order'];
		$search_data["products"]["sort_direction"] = 0;
	}

	if (!empty($page) && $search_data["products"]["page"] != intval($page)) {
		# Store the current page number in the session
		$search_data["products"]["page"] = $page;
		$flag_save = true;
	}

	if (is_array($search_data["products"])) {
		$data = $search_data["products"];
		foreach ($data as $k=>$v)
			if (!is_array($v) && !is_numeric($v))
				$data[$k] = addslashes($v);
	}
	
	
	#
	# Translate service data to inner service arrays
	#
	if (!empty($data['_'])) {
		foreach ($data['_'] as $saname => $sadata) {
			if (isset($$saname) && is_array($$saname) && empty($$saname))
				$$saname = $sadata;
		}
	}

	$sort_string = "";
	$membershipid_string = ($user_account['membershipid'] == 0) ? "= 0" : "IN ('$user_account[membershipid]', 0)";
     
	if($current_area == 'A'){
	  $fields[] = "$sql_tbl[products].*";
	}
	
	$keywordid = 0;
	if(!empty($HTTP_GET_VARS['searchkey'])){
		$searchKeyData = getSearchKeywordById($HTTP_GET_VARS['key_id']);
		$data['substring'] = trim($searchKeyData['keyword']);
		$data['orgsubstring'] = trim($searchKeyData['orignal_keyword']);
//		/if(empty($data['substring']) && $HTTP_GET_VARS['key_id'] != 0)  $data['substring'] =  $data['orgsubstring'];
		$search_item_keyword = preg_replace('#([ ])+#','-',trim($searchKeyData['orignal_keyword'],"-"));
		/*$search_item_keyword = $data['substring'] ;*/
		$keywordid = $searchKeyData['key_id'];
		$smarty->assign("search_key_id",$searchKeyData['key_id']); 
		$smarty->assign("search_key",$searchKeyData['orignal_keyword']); 
		$smarty->assign("search_item_keyword",$search_item_keyword); 
	}
	
	
	
	//$from_tbls[] = "pricing";
	/*$left_joins['quick_flags'] = array(
		"on" => "$sql_tbl[quick_flags].productid = $sql_tbl[products].productid"
	); */
	//$fields[] = "$sql_tbl[quick_flags].*";

	/*$inner_joins['quick_prices'] = array(
		"on" => "$sql_tbl[quick_prices].productid = $sql_tbl[products].productid AND $sql_tbl[quick_prices].membershipid $membershipid_string"
	); */
	//$where[] = "$sql_tbl[quick_prices].priceid = $sql_tbl[pricing].priceid";
	//$fields[] = "$sql_tbl[quick_prices].variantid";
	/*if ($user_account['membershipid'] == 0) {
		$fields[] = "$sql_tbl[pricing].price";
	} else {
		$fields[] = "MIN($sql_tbl[pricing].price) as price";
	}*/

	if ($current_area == 'C' && empty($active_modules['Product_Configurator'])) {
		//$where[] = "$sql_tbl[products].product_type <> 'C'";
		$where[] = "$sql_tbl[products].forsale <> 'B' AND $sql_tbl[products].statusid= 1008";
	}
	if ($current_area == 'C' && defined('SO_CUSTOMER_OFFERS')) {
		# Display all products (including hidden)
		$where[] = "$sql_tbl[products].forsale <> 'N' AND $sql_tbl[products].statusid= 1008";
	}


	if (!$single_mode && AREA_TYPE != 'A' && AREA_TYPE != 'P') {
		/*$inner_joins['ACHECK'] = array(
			"tblname" => 'customers',
			"on" => "$sql_tbl[products].provider = ACHECK.login AND ACHECK.activity='Y'"
		);*/
	}

	$data["substring"] = trim($data["substring"]);


	if (!empty($data["substring"])) {

		$condition = array();
		$search_string_fields = array();
		if (empty($data["by_title"]) && empty($data["by_shortdescr"]) && empty($data["by_fulldescr"]) && empty($data["extra_fields"]) && empty($data["by_sku"])) {
			$search_data["products"]["by_title"] = $data["by_title"] = $data["by_keywords"]="Y";
			$flag_save = true;
		}
        # Search for substring in some fields...

		if (!empty($data["by_title"])) {
			$search_string_fields[] = "product";
		}

		if (!empty($data["by_keywords"])) {
			$search_string_fields[] = "keywords";
		}

		if (!empty($data["by_shortdescr"])) {
			//$search_string_fields[] = "descr";
		}

		if (!empty($data["by_fulldescr"])) {
			$search_string_fields[] = "fulldescr";
		}

		// remove special charater with % if it exist in product name
		
		 // fetch all the product type for synonyms in searched key 
	 	if($current_area == 'C' ){
				$additional_keyword_field  = '';
				$productTypeList = checkForProductSynonyms($data['orgsubstring']);
				if(!empty($productTypeList)){
						$where[] = "$sql_tbl[products].product_type_id  in ( $productTypeList )";
						$smarty->assign("product_type_ids",$productTypeList);
				}
	     }	

		
		if(($pos = strpos($data["substring"], "*")) !== false)
		{
		        $special_chars = true;
		        $data["substring"] = str_replace("*","",$data["substring"]);
		}

		/* set condition  to make seach by product type */
		/*
		if ($current_area == 'C' && !empty($data["by_type"]) && empty($productTypeList)) {
			   	$where[] = "$sql_tbl[products].product_type_id = '".$data[by_type]."'";
		}*/
		/*if ($current_area == 'C' && !empty($data["by_type"])) {
			   	$where[] = "$sql_tbl[products].product_type_id = '".$data[by_type]."'";
		}*/
		/* set condition  to make seach by category type */
		if ($current_area == 'C' && !empty($data["by_category"])) {
			    	$where[] = "$sql_tbl[products_categories].categoryid  = '".$data[by_category]."' ";
		}
		
	    
		
          
       	/************ if user come from search engine like google, track the keyword *****************/
		if(!empty($_SERVER['HTTP_REFERER']) ){
			     $weblog->info("Referer URL : ".$_SERVER['HTTP_REFERER']." @line ".__LINE__." in file ".__FILE__);
	             parse_str($_SERVER['HTTP_REFERER'],$output);
	             $qry_str = $output['q'];		
	             if(!empty($qry_str)){
	                track_search_keywords(urldecode($qry_str));	
	             }
		} 
		
		if ((!empty($data["by_shortdescr"]) || !empty($data["by_fulldescr"])) && $current_area == 'C' && !in_array("keywords", $search_string_fields)) {
			$search_string_fields[] = "keywords";
		}

		$search_words = array();
		if ($config['General']['allow_search_by_words'] == 'Y' && in_array($data['including'], array("all", "any"))) {
			$tmp = trim($data["substring"]);
			if (preg_match_all('/"([^"]+)"/', $tmp, $match)) {
				$search_words = $match[1];
				$tmp = str_replace($match[0], "", $tmp);
			}
			$tmp = explode(" ", $tmp);
			$tmp = func_array_map("trim", $tmp);
			$search_words = array_merge($search_words, $tmp);
			unset($tmp);

			# Check word length limit
			if ($search_word_length_limit > 0) {
				$search_words = preg_grep("/^..+$/", $search_words);
			}

			# Check stop words
			x_load("product");
			$stopwords = func_get_stopwords();
			if (!empty($stopwords) && is_array($stopwords)) {
				$tmp = preg_grep("/^(".implode("|", $stopwords).")$/i", $search_words);
				if (!empty($tmp) && is_array($tmp)) {
					$search_words = array_diff($search_words, $tmp);
					$search_words = array_values($search_words);
				}
				unset($tmp);
			}

			# Check word count limit
			if ($search_word_limit > 0 && count($search_words) > $search_word_limit) {
				$search_words = array_splice($search_words, $search_word_limit-1);
			}
		}
        $count=0;
		foreach ($search_string_fields as $ssf) {
			$count++;
			if ($config['General']['allow_search_by_words'] == 'Y' && !empty($search_words) && in_array($data['including'], array("all", "any"))) {
				
				if ($data['including'] == 'all') {
					$tmp = array();
					foreach ($search_words as $sw) {
						if ($current_area == 'C' || $current_area == 'B')
						{
							if(!empty($sw)){
							    if($special_chars)
							    {
							    	$tmp[] = " $sql_tbl[products].$ssf 
									LIKE '".$sw."%'";
	
							    }
							    else
							    {
								 $tmp[] = "MATCH($sql_tbl[products].$ssf)
								 AGAINST ('".$sw."')";
								}
							}
						} else {
							$tmp[] = "MATCH($sql_tbl[products].$ssf) AGAINST ('".$sw."')";
						}
					}
					if (!empty($tmp))
						$condition[] = "(".implode(" AND ", $tmp).")";
					unset($tmp);

				} else {
					$temp_condition = array();
					foreach ($search_words as $sw) {
						if($count==1){
			     		  //  $customercondition[] = "xcart_customers.firstname LIKE '$sw%' ";
				     	}
				     	$sw = preg_replace("/[^a-z0-9]/", '', $sw);
						if ($current_area == 'C' || $current_area == 'B') {
							$temp_condition[] = " MATCH($sql_tbl[products].$ssf) AGAINST ('".$sw."') ";
						} else {
							$condition[] = "$sql_tbl[products].$ssf REGEXP '".($sw)."'";
						}
				   }
				   ### implode the string append it by AND 
				   
				}

			}
			elseif ($current_area == 'C' || $current_area == 'B')
			{
			     $new_search=explode(" ",$data["substring"]);
			     
			     foreach ($new_search as $word){
			       	if($count==1 && !empty($word)){
				          //$customercondition[] = "xcart_customers.firstname LIKE '$word%' ";
				   	}	
			     
				   if($special_chars)
				   {
				      $condition[] = "$sql_tbl[products].$ssf LIKE '".$data["substring"]."%'";
				   }
				   else
				   {
				   	 if(!empty($word))
				       $condition[] = "MATCH($sql_tbl[products].$ssf) AGAINST ('".$word."')";
				   }

			  }
		    } else {
			   		$condition[] = "MATCH($sql_tbl[products].$ssf) AGAINST ('".$data["substring"]."')";
			}
		   }

		if (!empty($data["by_sku"])) {
			if($special_chars)
			{
				$condition[] = "$sql_tbl[products].productcode LIKE '".$data["substring"]."%'";
			}
			else
			{
			 $condition[] = "$sql_tbl[products].productcode LIKE '%".$data["substring"]."%'";
			}
		}
		 
	    // implemented new search as per the requirement , 
	    if ($current_area == 'C'){
	    	foreach ($search_words as $sw) {
	    		$temp_condition = array();
		    	foreach ($search_string_fields as $ssf) {
		    		$temp_condition[] = " MATCH($sql_tbl[products].$ssf) AGAINST ('".$sw."') ";
		    	}
		    	if(!empty($temp_condition))
		    	  $condition[] =  "(".implode(" OR ", $temp_condition).")";
	    	}
	    }
	    
		### Not in use ###
		/***if (!empty($data["extra_fields"]) && $active_modules['Extra_Fields']) {
						
			foreach ($data["extra_fields"] as $k => $v)
			{
			   if($special_chars)
			   {
			     $condition[] = "($sql_tbl[extra_field_values].value LIKE '".$data["substring"]."%' AND $sql_tbl[extra_fields].fieldid = '$k')";
			   }
			   else
			   {
				$condition[] = "($sql_tbl[extra_field_values].value LIKE '%".$data["substring"]."%' AND $sql_tbl[extra_fields].fieldid = '$k')";
				}
			}

			$left_joins['extra_field_values'] = array(
				"on" => "$sql_tbl[products].productid = $sql_tbl[extra_field_values].productid"
			);
			$left_joins['extra_fields'] = array(
				"on" => "$sql_tbl[extra_field_values].fieldid = $sql_tbl[extra_fields].fieldid AND $sql_tbl[extra_fields].active = 'Y'"
			);
		}***/

		if (!empty($condition))
		{
			###$where[] = "(".implode(" AND ", $condition).")";   // previous code
			if(!empty($customercondition)){
			  $temp[] = "(".implode(" OR ", $customercondition).")";	
			}
			$where[] = (!empty($temp)) ? implode(" OR ", $temp) ." OR " ."(".implode(" AND ", $condition).")" : "(".implode(" AND ", $condition).")";   // temp search
						
		}
		
		unset($condition);

	} else{
		
		if($current_area == 'C' ){
				$additional_keyword_field  = '';
				$productTypeList = checkForProductSynonyms($data['orgsubstring']);
				if(!empty($productTypeList)){
						$where[] = "$sql_tbl[products].product_type_id  in ( $productTypeList )";
						$smarty->assign("product_type_ids",$productTypeList);
				}
	     }	
		
	}# /if (!empty($data["substring"]))
	
	
	#
	# Search for affiliates products 
	#
	
	 $affiliateSearch =   $data["affiliate_search"] ;
  
     $affiliateclause = array();
  	 if ($affiliateSearch == 'Y') {
  	
		$condition = array();
		$search_string_fields = array();
	
        # Search for substring in some fields...

		if (!empty($data["by_title"])) {
			$search_string_fields[] = "product";
		}

		if (!empty($data["by_keywords"])) {
			$search_string_fields[] = "keywords";
		}

		
		/* set condition  to make seach by product type */
		if ($current_area == 'C' && !empty($data["by_types"])) {
			   	$affiliateclause[] = "$sql_tbl[products].product_type_id IN ($data[by_types])";
		}
		/* set condition  to make seach by category type */
		if ($current_area == 'C' && !empty($data["by_categories"])) {
			    	$affiliateclause[] = "$sql_tbl[products_categories].categoryid  IN ($data[by_categories])";
		}



		if ((!empty($data["by_shortdescr"]) || !empty($data["by_fulldescr"])) && $current_area == 'C' && !in_array("keywords", $search_string_fields)) {
			$search_string_fields[] = "keywords";
		}

		$search_words = array();
        if(!empty($data["by_substring"]))
        {  
			foreach ($search_string_fields as $ssf) {
				
				foreach($data["by_substring"] as $key=>$value)
				{
				   
				 	$condition[] = " MATCH($sql_tbl[products].$ssf) AGAINST ('".$value."')";
				}
				
			}
        }
		if (!empty($condition))
		{
			$affiliateclause[] = "(".implode(" OR ", $condition).")";
		}
		unset($condition);
		if(!empty($affiliateclause))
		$where[]  = "(".implode(" OR ", $affiliateclause).")";
	}
	

	
	
	#
	# Search by product features , Not in Use
	#
	##if (!empty($active_modules['Feature_Comparison'])) {
		##include $xcart_dir."/modules/Feature_Comparison/search_define.php";
	##}

	#
	# Internation names & descriptions
	#
	if ($current_area == 'C' || $current_area == 'B') {
		$fields[] = "$sql_tbl[products].productid as productid";
		$fields[] = "$sql_tbl[products].product as product";
		$fields[] = "$sql_tbl[products].descr as descr";
		$fields[] = "$sql_tbl[products].fulldescr as fulldescr";
        $fields[] = "$sql_tbl[products].image_portal_t "; 		
        $fields[] = "$sql_tbl[products].image_portal_thumbnail_160 as img_portal_160 ";
		if (!empty($data["by_title"]) || !empty($data["by_keywords"]) || !empty($data["by_shortdescr"]) || !empty($data["by_fulldescr"])) {
			/*$left_joins['products_lng'] = array(
				"on" => "$sql_tbl[products_lng].productid = $sql_tbl[products].productid AND $sql_tbl[products_lng].code = '$shop_language'"
			);*/
		} else {
			/*$left_joins['products_lng'] = array(
				"on" => "$sql_tbl[products_lng].productid = $sql_tbl[products].productid AND $sql_tbl[products_lng].code = '$shop_language'",
				"only_select" => true
			);*/
		}
	}
    ## Not in Use ############## 
	##if (!empty($data["manufacturers"]) && $active_modules['Manufacturers']) {
		###$where[] = "$sql_tbl[products].manufacturerid IN ('".implode("','", $data["manufacturers"])."')";
	##}

	if ($current_area == 'C') {
		if ($user_account['membershipid'] == 0) {
			//$where[] = "$sql_tbl[category_memberships].membershipid IS NULL AND $sql_tbl[product_memberships].membershipid IS NULL";
		} else {
			$where[] = "($sql_tbl[category_memberships].membershipid IS NULL OR $sql_tbl[category_memberships].membershipid = '$user_account[membershipid]')";
			$where[] = "($sql_tbl[product_memberships].membershipid IS NULL OR $sql_tbl[product_memberships].membershipid = '$user_account[membershipid]')";
		}

		//$where[] = "$sql_tbl[categories].avail = 'Y'";
	}



	$left_joins['products_categories'] = array(
		"on" => "$sql_tbl[products_categories].productid = $sql_tbl[products].productid"
	);
    $inner_joins['mk_product_type'] = array(
		"on" => "$sql_tbl[mk_product_type].id = $sql_tbl[products].product_type_id"
	);
     $inner_joins['mk_product_style'] = array(
		"on" => "$sql_tbl[mk_product_style].id = $sql_tbl[products].product_style_id"
	);
	$inner_joins['customers'] = array(
		"on" => "$sql_tbl[customers].login = $sql_tbl[products].provider"
	);
	$left_joins['categories'] = array(
		"on" => "$sql_tbl[products_categories].categoryid = $sql_tbl[categories].categoryid"
	);
	$left_joins['category_memberships'] = array(
		"on" => "$sql_tbl[category_memberships].categoryid = $sql_tbl[categories].categoryid",
		"parent" => "categories"
	);

	if (!empty($data["categoryid"])) {
		# Search by category...

		$data["categoryid"] = intval($data["categoryid"]);

		$category_sign = "";

		if (empty($data["category_main"]) && empty($data["category_extra"])) {
			$category_sign = "NOT";
		}

		if (!empty($data["search_in_subcategories"])) {
			# Search also in all subcategories
			$categoryid_path = addslashes(func_query_first_cell("SELECT categoryid_path FROM $sql_tbl[categories] WHERE categoryid='".$data["categoryid"]."'"));
			$categoryids = func_query_column("SELECT categoryid FROM $sql_tbl[categories] WHERE categoryid='".$data["categoryid"]."' OR categoryid_path LIKE '$categoryid_path/%'");

			if (is_array($categoryids) && !empty($categoryids)) {
				$where[] = "$sql_tbl[products_categories].categoryid $category_sign IN (".implode(",", $categoryids).")";
			}
		}
		else {
			$where[] = "$category_sign $sql_tbl[products_categories].categoryid='$data[categoryid]'";
		}
		
		
		/*end */
		
		
		$condition = array();

		if (!empty($data["category_main"]))
			//$condition[] = "$sql_tbl[products_categories].main='Y'";

		if (!empty($data["category_extra"]))
			$condition[] = "$sql_tbl[products_categories].main!='Y'";


		if (!empty($condition))
			$where[] = "(".implode(" OR ", $condition).")";
	 }

/******************************************************/
/*Nikhil Gupta (07/04/07)                                                               */
/*Search by product status id														 */
/******************************************************/

		if($current_area == "A" )
		{
		    if ($data["productStatus"] != 0)
			{
			        $where[] = "$sql_tbl[products].statusid = $data[productStatus]";
		    }
		}

		if($current_area == "A" )
		{
		    if ($data["designer"] != 0)
			{
			        $where[] = "$sql_tbl[products].designer = '".$data[designer]."'";
		    }
		    ######################### dont show the 99 Rs tshirt in pending search ####################
		    $where[] = "$sql_tbl[products].product_type_id != 58 ";
		    $rsCheckP = func_query("select productid from mk_org_products_map");
		    if(!empty($rsCheckP)){
		    	$str = '';
		    	foreach($rsCheckP as $_k=>$_v){
		    		$str .= $_v['productid'].",";
		    	}
		    	$str = trim($str,",");
		    	$where[] = "$sql_tbl[products].productid not in ($str) ";
		    }
		   
		}
		
		if($current_area == "A" )
		{
		     if(!empty($posted_data["start_date"]) && !empty($posted_data["end_date"]))
		     {
		     	 $where[] = "$sql_tbl[products].add_date BETWEEN  '".$posted_data["start_date"]."' 
		     	 AND '".$posted_data["end_date"]."'";
		     }
		}
		$where[] = "$sql_tbl[customers].usertype <> 'A' ";
		
		/* changes for search search by product type  and designer */
		if (!empty($data["producttype"]))
		{
			 $where[] = "$sql_tbl[products].product_type_id = '".$data[producttype]."' ";
		}
		if (!empty($data["designer"]))
		{
			   $where[] = "$sql_tbl[products].designer = '".$data[designer]."'";
		}
		if (!empty($data["designertxt"]))
		{
			  $where[] = " ACHECK.firstname LIKE  '".$data[designertxt]."%' || 
			  ACHECK.lastname LIKE  '".$data[designertxt]."%' ";
		}

/******************************************************/


	# /if (!empty($data["categoryid"]))

	if (!empty($data["productcode"])) {
		/*$productcode_cond_string = empty($active_modules['Product_Options']) ? "$sql_tbl[products].productcode" : "IFNULL($sql_tbl[variants].productcode, $sql_tbl[products].productcode)";
		$where[] = "$productcode_cond_string LIKE '%".$data["productcode"]."%'";*/
	}

	if (!empty($data["productid"])) {
		$where[] = "$sql_tbl[products].productid ".(is_array($data["productid"]) ? " IN ('".implode("','", $data["productid"])."')": "= '".$data["productid"]."'");
	}

	if (!empty($data["provider"])) {
		if (is_array($data['provider']))
			$where[] = "$sql_tbl[products].provider IN ('".implode("','", $data['provider'])."')";
		else
			$where[] = "$sql_tbl[products].provider = '".$data["provider"]."'";
	}

	if (!empty($data["price_min"])) {
		$where[] = "$sql_tbl[mk_product_style].price >= '".$data["price_min"]."'";
	}

	if (strlen(@$data["price_max"]) > 0) {
		$where[] = "$sql_tbl[mk_product_style].price <= '".$data["price_max"]."'";
	}

	$avail_cond_string = empty($active_modules['Product_Options']) ? "$sql_tbl[products].avail" : "$sql_tbl[products].avail";
	if (!empty($data["avail_min"])) {
		$where[] = "$avail_cond_string >= '".$data["avail_min"]."'";
	}

	if (strlen(@$data["avail_max"]) > 0) {
		$where[] = "$avail_cond_string <= '".$data["avail_max"]."'";
	}

	$weight_cond_string = empty($active_modules['Product_Options']) ? "$sql_tbl[products].weight" : "$sql_tbl[products].weight";
	if (!empty($data["weight_min"])) {
		$where[] = "$weight_cond_string >= '".$data["weight_min"]."'";
	}

	if (strlen(@$data["weight_max"]) > 0) {
		$where[] = "$weight_cond_string <= '".$data["weight_max"]."'";
	}

	if (!empty($data["forsale"])){
		##$where[] = "$sql_tbl[products].forsale = '".$data["forsale"]."'";
		$where[] = "$sql_tbl[products].forsale = 'Y' ";
	}
		

	if (!empty($data["flag_free_ship"]))
		$where[] = "$sql_tbl[products].free_shipping = '".$data["flag_free_ship"]."'";

	if (!empty($data["flag_ship_freight"]))
		$where[] = "$sql_tbl[products].shipping_freight = '".$data["flag_ship_freight"]."'";

	if (!empty($data["flag_ship_freight"])) {
		if ($data["flag_ship_freight"] == "Y")
			$where[] = "$sql_tbl[products].shipping_freight > 0";
		else
			$where[] = "$sql_tbl[products].shipping_freight = 0";
	}

	if (!empty($data["flag_global_disc"]))
		$where[] = "$sql_tbl[products].discount_avail = '".$data["flag_global_disc"]."'";

	if (!empty($data["flag_free_tax"]))
		$where[] = "$sql_tbl[products].free_tax = '".$data["flag_free_tax"]."'";

	if (!empty($data["flag_min_amount"])) {
		if ($data["flag_min_amount"] == "Y")
			$where[] = "$sql_tbl[products].min_amount != '1'";
		else
			$where[] = "$sql_tbl[products].min_amount = '1'";
	}

	if (!empty($data["flag_low_avail_limit"])) {
		if ($data["flag_low_avail_limit"] == "Y")
			$where[] = "$sql_tbl[products].low_avail_limit != '10'";
		else
			$where[] = "$sql_tbl[products].low_avail_limit = '10'";
	}

	if (!empty($data["flag_list_price"])) {
		if ($data["flag_list_price"] == "Y")
			$where[] = "$sql_tbl[products].list_price != '0'";
		else
			$where[] = "$sql_tbl[products].list_price = '0'";
	}

	if(!empty($active_modules['Product_Options'])) {
		$left_joins["variants"] = array(
			"on" => "$sql_tbl[variants].productid = $sql_tbl[products].productid AND $sql_tbl[quick_prices].variantid = $sql_tbl[variants].variantid",
			"parent" => "quick_prices"
		);
		##foreach ($variant_properties as $property) {
			//$fields[] = "IFNULL($sql_tbl[variants].$property, $sql_tbl[products].$property) as ".$property;
		##}
	}


	if (!empty($data["sort_field"])) {
		# Sort the search results...

		$direction = ($data["sort_direction"] ? "DESC" : "ASC");
		switch ($data["sort_field"]) {
			case "productcode":
				$sort_string = "$sql_tbl[products].productcode $direction";
				break;
			case "title":
				$sort_string = "$sql_tbl[products].product $direction";
				break;
			case "orderby":
				$sort_string = "$sql_tbl[products_categories].orderby $direction";
				break;
			case "quantity":
				$sort_string = "$sql_tbl[products].avail $direction";
				break;
			case "qtysold":
				$sort_string = "$sql_tbl[products].sales_stats $direction";
				break;
			case "status":
				$sort_string = "$sql_tbl[products].statusid $direction";
				break;
			case "style":
				$sort_string = "$sql_tbl[mk_product_style].name $direction";
				break;
			case "price":
				if (!empty($active_modules["Special_Offers"]) && !empty($search_data["products"]["show_special_prices"])) {
					$sort_string = "x_special_price $direction, price $direction";
				}
				else {
					$sort_string = " $sql_tbl[products].list_price $direction";
				}
				break;
			case "date" :
			    $sort_string = " $sql_tbl[products].add_date  $direction";
			    break;
			default:
				$sort_string = "$sql_tbl[products].product";
		}
	}
	else {
		$sort_string = "$sql_tbl[products].product";
	}

	if(!empty($data['sort_condition'])) {
		$sort_string = $data['sort_condition'];
	}
    ########### Not in use ###############
	###if (($current_area == "C" || $current_area == "B") && $config["General"]["disable_outofstock_products"] == "Y") {
		###if (!empty($active_modules['Product_Options'])) {
			//$where[] = "(IFNULL($sql_tbl[variants].avail, $sql_tbl[products].avail) > 0 OR $sql_tbl[products].product_type NOT IN ('','N'))";
		##} else {
			//$where[] = "($sql_tbl[products].avail > 0 OR $sql_tbl[products].product_type NOT IN ('','N')) AND ";
		##}
	##}

	$groupbys[] = "$sql_tbl[products].productid";
	
	
	if($current_area == "A" )
	{
		$orderbys[] = $sort_string;
	    $orderbys[] = "$sql_tbl[products].add_date ASC";
	}
	elseif($current_area == "C") 
	{
		switch($sortparam)
		{	
			case "MOSTRECENT" :	
		  		$orderbys[] = "$sql_tbl[products].add_date DESC";
		  		break;
			case "BESTSELLING" :
				 $orderbys[] = "$sql_tbl[products].sales_stats DESC";
				 break;
			case "USERRATING" :
				$orderbys[] = "$sql_tbl[products].rating DESC";	 
				break;
			case "EXPERTRATING" :
				 $orderbys[] = "$sql_tbl[products].myntra_rating DESC";
				 break;				
		}
	}
	

	#
	# Generate search query
	#
	foreach ($inner_joins as $j) {
		if (!empty($j['fields']) && is_array($j['fields']))
			$fields = func_array_merge($fields, $j['fields']);
	}
	foreach ($left_joins as $j) {
		if (!empty($j['fields']) && is_array($j['fields']))
			$fields = func_array_merge($fields, $j['fields']);
	}

	$fields_count[] = "COUNT($sql_tbl[products].productid)";
	$search_query = "SELECT ".implode(", ", $fields)." ,$sql_tbl[mk_product_type].name,$sql_tbl[mk_product_type].id as typeid,$sql_tbl[products].product_style_id as styleid,$sql_tbl[mk_product_style].name as stylename,FORMAT($sql_tbl[mk_product_style].price+IFNULL($sql_tbl[mk_product_style].price*$sql_tbl[products].markup/100,0),2) as styleprice FROM ";
    
	$search_query_count = "SELECT ".implode(", ", $fields_count)." FROM ";
	if (!empty($from_tbls)) {
		foreach ($from_tbls as $k => $v) {
			$from_tbls[$k] = $sql_tbl[$v];
		}
		$search_query .= implode(", ", $from_tbls).", ";
		$search_query_count .= implode(", ", $from_tbls).", ";
	}
	$search_query .= $sql_tbl['products'];
     $search_query_count .= $sql_tbl['products'];


	foreach ($left_joins as $ljname => $lj) {
		if (!empty($lj['parent']))
			continue;
		$tmp = " LEFT JOIN ";

		if (!empty($lj['tblname'])) {
			$tmp .= $sql_tbl[$lj['tblname']]." as ".$ljname;
		} else {
			$tmp .= $sql_tbl[$ljname];
		}
		$tmp .= " ON ".$lj['on'];
		if (!isset($lj['only_select']))
			$search_query_count .= $tmp;
		$search_query .= $tmp;
	}

	foreach ($inner_joins as $ijname => $ij) {
		$tmp = " INNER JOIN ";
		if (!empty($ij['tblname'])) {
			$tmp .= $sql_tbl[$ij['tblname']]." as ".$ijname;
		} else {
			$tmp .= $sql_tbl[$ijname];
		}
		$tmp .= " ON ".$ij['on'];
		$search_query_count .= $tmp;
		$search_query .= $tmp;
		foreach ($left_joins as $ljname => $lj) {
			if ($lj['parent'] != $ijname)
				continue;
			$tmp = " LEFT JOIN ";
			if (!empty($lj['tblname'])) {
				$tmp .= $sql_tbl[$lj['tblname']]." as ".$ljname;
			} else {
				$tmp .= $sql_tbl[$ljname];
			}
			$tmp .= " ON ".$lj['on'];
			if (!isset($lj['only_select']))
				$search_query_count .= $tmp;
			$search_query .= $tmp;
		}
	}

// loop for outer join


	foreach ($outer_joins as $ljname => $lj) {
		if (!empty($lj['parent']))
			  continue;
		$tmp = " RIGHT OUTER JOIN ";

		if (!empty($lj['tblname'])) {
			$tmp .= $sql_tbl[$lj['tblname']]." as ".$ljname;
		} else {
			$tmp .= $sql_tbl[$ljname];
		}
		$tmp .= " ON ".$lj['on'];
		if (!isset($lj['only_select']))
			$search_query_count .= $tmp;
		$search_query .= $tmp;
	}

	
	
    
	$search_query .= (!empty($where)) ? " WHERE ".implode(" AND ", $where) : '';
	$search_query_count .= (!empty($where)) ? " WHERE ".implode(" AND ", $where) : '';
	
	if(!empty($where))
	{
		if (!empty($groupbys)) {
			$search_query .= " GROUP BY ".implode(", ", $groupbys);
			$search_query_count .= " GROUP BY ".implode(", ", $groupbys);
		}
		if (!empty($having)) {
			$search_query .= " HAVING ".implode(" AND ", $having);
			$search_query_count .= " HAVING ".implode(" AND ", $having);
		}
	}
	if (!empty($orderbys)) {
		$search_query .= " ORDER BY ".implode(", ", $orderbys);
		$search_query_count .= " ORDER BY ".implode(", ", $orderbys);
	}
	//if($search_query_count!= 0)
	 //echo $search_query;
	 //exit;
	 //fundu query
	 $sqllog->debug("Search Query : ".$search_query);
    
	#
	# Calculate the number of rows in the search results
	#
	db_query("SET OPTION SQL_BIG_SELECTS=1");	 
	$_res = db_query($search_query_count);

	$total_items = db_num_rows($_res);
	// add total item to stats table 
	if($current_area == 'C'){
		update_product_count($total_items,$data['substring'],$keywordid);
	}
	
	db_free_result($_res);

    
    if ($total_items > 0) {
		$page = $search_data["products"]["page"];
		
		#
		# Prepare the page navigation
		#
		if (!isset($objects_per_page)) {
			if ($current_area == "C" || $current_area == "B")
				$objects_per_page = $config["Appearance"]["products_per_page"];
			else
				$objects_per_page = $config["Appearance"]["products_per_page_admin"];
		}

		$total_nav_pages = ceil($total_items/$objects_per_page)+1;
        
		if($current_area == 'C'){
			// search for customer section 
			include $xcart_dir."/include/navigation.php";
			$total =$total_items ;
			$limit = $objects_per_page ;			
			$sorturl = $http_location."/".$search_item_keyword."/search/$keywordid/?".(!empty($sortparam)? "sortby=".$sortparam : '') ."&page=" ;
			$sort = '' ;
			
			$paginator = paginate($limit, $page, $total, $sorturl, $sort);
			$smarty->assign("paginator",$paginator);
			$smarty->assign("totalpages",$totalpages);
			$smarty->assign("pg",$page);
			
		}else{
		
			include $xcart_dir."/include/navigation.php";
		}

		#
		# Perform the SQL query and getting the search results
		#
		if (!empty($data["is_modify"])) {
			#
			# Get the products and go to modify them
			#
			$res = db_query($search_query);
			if ($res) {
				$geid = false;
				$productid = false;
				x_load("product");
				while ($pid = db_fetch_row($res)) {
					if (empty($productid))
						$productid = $pid[0];
					$geid = func_ge_add($pid[0], $geid);
				}
				func_header_location("product_modify.php?productid=$productid&geid=".$geid);
			}

		}
		elseif ($data["is_export"] == "Y" || $export == 'export_found') {

			x_load("export");
			# Save the SQL query and go to export them
			func_export_range_save("PRODUCTS", $search_query);
			$top_message['content'] = func_get_langvar_by_name("lbl_export_products_add");
			$top_message['type'] = 'I';
			func_header_location("import.php?mode=export");

		}
		else {
			$search_query .= " LIMIT $first_page, $objects_per_page";
			
			$products = func_query($search_query);
            
         }


		# Clear service arrays
		unset($fields, $fields_count, $from_tbls, $inner_joins, $left_joins, $where, $groupbys, $having, $orderbys);

		if (!empty($products) && $current_area == "C") {
			x_session_register("cart");

			# Get tax rates cache
			/*$ids = array();
			foreach ($products as $v) {
				if ($v['is_taxes'] == 'Y')
					$ids[] = $v;
			}*/

			$_taxes = array();
			/*if (!empty($ids)) {
				x_load("taxes");
				$_taxes = func_get_product_tax_rates($products, $login);
			}*/
			//unset($ids);
            ### Commented not in use   
			/********if (!empty($active_modules['Extra_Fields'])) {

				# Get Extra fields cache
				$ids = array();
				foreach ($products as $k => $v) {
					$ids[] = intval($v['productid']);
				}

				$products_ef = func_query_hash("SELECT $sql_tbl[extra_fields].*, $sql_tbl[extra_field_values].*, IF($sql_tbl[extra_fields_lng].field != '', $sql_tbl[extra_fields_lng].field, $sql_tbl[extra_fields].field) as field FROM $sql_tbl[extra_field_values], $sql_tbl[extra_fields] LEFT JOIN $sql_tbl[extra_fields_lng] ON $sql_tbl[extra_fields].fieldid = $sql_tbl[extra_fields_lng].fieldid AND $sql_tbl[extra_fields_lng].code = '$shop_language' WHERE $sql_tbl[extra_fields].fieldid = $sql_tbl[extra_field_values].fieldid AND $sql_tbl[extra_field_values].productid IN (".implode(",", $ids).") AND $sql_tbl[extra_fields].active = 'Y' ORDER BY $sql_tbl[extra_fields].orderby", "productid");
				unset($ids);
			}***/
			########### Commented not in use ###		
			/****if (!empty($active_modules['Product_Options'])) {

				# Get Product options markups cache
				$ids = array();
				foreach ($products as $v) {
					if (!empty($v['is_product_options']))
						$ids[$v['productid']] = doubleval($v['price']);
				}

				$options_markups = array();
				if (!empty($ids))
					$options_markups = func_get_default_options_markup_list($ids);
				unset($ids);
			}****/

			foreach ($products as $k => $v) {
				//$products[$k]['taxed_price'] = $v['taxed_price'] = $v['price'];
                ## Commented not in use    
				/**if (!empty($active_modules['Product_Options']) && !empty($v['is_product_options']) && !empty($options_markups[$v['productid']])) {

					# Add product options markup
					$products[$k]['price'] += $options_markups[$v['productid']];
					$products[$k]['taxed_price'] = $products[$k]['price'];
					$v = $products[$k];
				}**/

				$in_cart = 0;
				/***if (!empty($cart['products']) && is_array($cart['products'])) {

					# Modify product's quantity based the cart data
					foreach ($cart['products'] as $cv) {
						if ($cv['productid'] == $v['productid'] && $v['variantid'] == $cv['variantid'])
							$in_cart += $cv['amount'];
					}

					$products[$k]['in_cart'] = $in_cart;
					$products[$k]['avail'] -= $in_cart;
					if ($products[$k]['avail'] < 0) {
						$products[$k]['avail'] = 0;
					}
				}***/

				/*if (!empty($active_modules['Extra_Fields']) && isset($products_ef[$v['productid']])) {

					# Get extra fields data
					$products[$k]['extra_fields'] = $products_ef[$v['productid']];
				}*/

				# Get thumbnail URL
				$products[$k]["tmbn_url"] = false;
				if (!is_null($v['image_path_T'])) {
					$products[$k]['is_image_T'] = true;
					if (!empty($v['image_path_T'])) {
						x_load("files");
						$products[$k]["tmbn_url"] = func_get_image_url($v['productid'], "T", $v['image_path_T']);
					}

				} else {
					$products[$k]["tmbn_url"] = func_get_default_image("T");
				}

				unset($products[$k]['image_path_T']);

				# Calculate product taxes 
				/**if (!empty($active_modules["Special_Offers"]) && !empty($search_data["products"]["show_special_prices"])) {
					include $xcart_dir."/modules/Special_Offers/search_results_calculate.php";
				}
				elseif ($v['is_taxes'] == 'Y' && isset($_taxes[$v['productid']])) {
					$products[$k]["taxes"] = func_get_product_taxes($products[$k], $login, false, $_taxes[$v['productid']]);
				}**/

				if ($products[$k]['descr'] == strip_tags($products[$k]['descr']))
					$products[$k]['descr'] = str_replace("\n", "<br />", $products[$k]['descr']);
				if ($products[$k]['fulldescr'] == strip_tags($products[$k]['fulldescr']))
					$products[$k]['fulldescr'] = str_replace("\n", "<br />", $products[$k]['fulldescr']);
			}

			if (!empty($active_modules["Special_Offers"]) && empty($search_data["products"]["show_special_prices"])) {
				func_offers_check_products($login, $current_area, $products);
			}
		}

		if (isset($products_ef))
			unset($products_ef);

		if (isset($options_markups))
			unset($options_markups);

		# Assign the Smarty variables
		if($current_area == 'C')
		{
			if($_GET['advance'] == true)
			{
				$smarty->assign("navigation_script","mkSearchResult.php?mode=search&advance=true");
			}
			else 
			{
				if($data['substring']!="")
					$smarty->assign("navigation_script",$http_location."/".$data['substring']."/search/");
				else
					$smarty->assign("navigation_script","mkSearchResult.php?mode=search");
			}
		}
		else
		{
			$smarty->assign("navigation_script","search.php?mode=search");
		}
     
        //setting mksearch

		//Access popup image path

	   $arrayKey = array_keys($products);

		for($i=0;$i<count($arrayKey);$i++){
			$prodarray = $arrayKey[$i];
			$productDetail = $products[$prodarray];
		    $namelength = strlen($productDetail['product']);
            $productname = ($namelength >= '18')?substr($productDetail['product'], 0, 15)."...":$productDetail['product'];

			$popupQuery = "SELECT popup_image_path
						FROM $sql_tbl[mk_xcart_product_customized_area_rel] a, $sql_tbl[mk_style_customization_area] b, $sql_tbl[products] c
						WHERE a.product_id = '$productDetail[productid]'
						AND a.product_id = c.productid
						AND a.style_id = b.style_id
						AND a.area_id = b.id
						AND b.isprimary = 1
						AND c.statusid = 1008";
			$popupResult = db_query($popupQuery);
			$popupRow = db_fetch_array($popupResult);
			$popupImage =  $popupRow['popup_image_path'];
			$products[$prodarray]['popup_image_path'] = $popupImage;
            $products[$prodarray]['prodname'] = $productname;
            //$products[$prodarray]['product'] = $productname; 
            
            $cp_product = trim($productDetail['product']);
            $cp_product = preg_replace("/[^a-zA-Z0-9s-]/", "-", $cp_product);
            $cp_product = preg_replace('#([ ])+#','-',$cp_product);
            $cp_product = preg_replace('#([-])+#','-',$cp_product); 
            $products[$prodarray]['product'] = $cp_product;         

		}
		
		$arr_rating = array(0,1,2,3,4,5,6,7,8,9,10);
		$smarty->assign("arr_rating", $arr_rating);
		$smarty->assign("products", $products);
		$smarty->assign("first_item", $first_page+1);
		$smarty->assign("last_item", min($first_page+$objects_per_page, $total_items));
	}

	$smarty->assign("total_items",$total_items);
	$smarty->assign("mode", $mode);

	if ($flag_save)
		x_session_save("search_data");
}

if (!isset($search_data["products"]['substring']) && $current_area == 'C') {
	$search_data["products"]['productcode'] = $config['Search_products']['search_products_sku_d'];
	$search_data["products"]['price_min'] = preg_replace("/-.*$/", "", $config['Search_products']['search_products_price_d']);
	$search_data["products"]['price_max'] = preg_replace("/^.*-/", "", $config['Search_products']['search_products_price_d']);
	$search_data["products"]['weight_min'] = preg_replace("/-.*$/", "", $config['Search_products']['search_products_weight_d']);
	$search_data["products"]['weight_max'] = preg_replace("/^.*-/", "", $config['Search_products']['search_products_weight_d']);
	$search_data["products"]['categoryid'] = $config['Search_products']['search_products_category_d'];
}
########### commented not in use ###############
/*********if (!empty($active_modules['Feature_Comparison']) && $current_area != 'C' && $current_area != 'P') {
	$fclasses = func_query("SELECT $sql_tbl[feature_classes].*, IFNULL($sql_tbl[feature_classes_lng].class, $sql_tbl[feature_classes].class) as class FROM $sql_tbl[feature_classes] LEFT JOIN $sql_tbl[feature_classes_lng] ON $sql_tbl[feature_classes].fclassid = $sql_tbl[feature_classes_lng].fclassid AND $sql_tbl[feature_classes_lng].code = '$shop_language' WHERE $sql_tbl[feature_classes].avail = 'Y' ORDER BY $sql_tbl[feature_classes].orderby");
	if(!empty($fclasses)) {
		$smarty->assign("fclasses", $fclasses);
	}
}**********/
$smarty->assign("search_prefilled", $search_data["products"]);
########### commented not in use ###############
/*********if($active_modules['Manufacturers'] && !(!empty($products) && $mode == 'search')) {
	if ($current_area == "C") {
		$manufacturers = func_query("SELECT $sql_tbl[manufacturers].*, IFNULL($sql_tbl[manufacturers_lng].manufacturer, $sql_tbl[manufacturers].manufacturer) as manufacturer, IFNULL($sql_tbl[manufacturers_lng].descr, $sql_tbl[manufacturers].descr) as descr FROM $sql_tbl[manufacturers] USE INDEX (avail) LEFT JOIN $sql_tbl[manufacturers_lng] ON $sql_tbl[manufacturers].manufacturerid = $sql_tbl[manufacturers_lng].manufacturerid AND $sql_tbl[manufacturers_lng].code = '$shop_language' WHERE avail = 'Y' ORDER BY orderby, manufacturer");
	}
	else {
		$manufacturers = func_query("SELECT * FROM $sql_tbl[manufacturers] WHERE avail = 'Y' ORDER BY orderby, manufacturer");
	}

	if ($manufacturers) {
		array_unshift($manufacturers, array("manufacturerid" => '0', "manufacturer" => func_get_langvar_by_name("lbl_no_manufacturer")));
		$tmp = explode("\n", $config['Search_products']['search_products_manufacturers_d']);
		foreach ($manufacturers as $k => $v) {
			if (@in_array($v['manufacturerid'], (array)$search_data["products"]['manufacturers']) || (in_array($v['manufacturerid'], $tmp) && $current_area == 'C'))
				$manufacturers[$k]['selected'] = 'Y';
		}

		if ($manufacturers)
			$smarty->assign("manufacturers", $manufacturers);
	}
}*************/
### Commented not in use ####
/***if ($active_modules['Extra_Fields'] && !(!empty($products) && $mode == 'search')) {
	$extra_fields = func_query("SELECT $sql_tbl[extra_fields].*, IF($sql_tbl[extra_fields_lng].field != '', $sql_tbl[extra_fields_lng].field, $sql_tbl[extra_fields].field) as field FROM $sql_tbl[extra_fields] LEFT JOIN $sql_tbl[extra_fields_lng] ON $sql_tbl[extra_fields].fieldid = $sql_tbl[extra_fields_lng].fieldid AND $sql_tbl[extra_fields_lng].code = '$shop_language' WHERE active = 'Y' ORDER BY field");
	if ($extra_fields) {
		$tmp = explode("\n", $config['Search_products']['search_products_extra_fields']);
		foreach ($extra_fields as $k => $v) {
			if (!in_array($v['fieldid'], $tmp) && $current_area == 'C') {
				unset($extra_fields[$k]);
				continue;
			}

			if ($search_data["products"]['extra_fields'][$v['fieldid']])
				$extra_fields[$k]['selected'] = 'Y';
		}

		if ($extra_fields)
			$smarty->assign("extra_fields", $extra_fields);
	}
}**/
if ($current_area != 'C')
{
	include $xcart_dir."/include/categories.php";
	$designers = func_query("SELECT d.id , c.firstname  as f, c.lastname as l FROM  $sql_tbl[mk_designer] as d INNER JOIN $sql_tbl[customers] as c ON c.login = d.customerid  ORDER BY  c.firstname");
	$smarty->assign("designers", $designers);

}

$search_categories = $smarty->get_template_vars("allcategories");
if ($current_area == "C" && !empty($active_modules["Fancy_Categories"])) {
	if (!function_exists("func_categories_sort_abc")) {
		function func_categories_sort_abc($a, $b) {
			return strcmp($a["category_path"], $b["category_path"]);
		}
	}

	usort($search_categories, "func_categories_sort_abc");
}
#################################################
###############added by swaraj###################
#################################################
//$searched = $HTTP_POST_VARS['posted_data'];
$searchedfor = $data["substring"];
if(isset($searchedfor) && trim($searchedfor) != '')
{
	$pquery = "select tagid from mk_tags where tagname='$searchedfor'";
	$presult = db_query($pquery);
	if(mysql_num_rows($presult)!=0){
		$query  ="update mk_tags a set a.count=a.count+5 where a.tagname='$searchedfor'";
		$result  = db_query($query);
	}
}
#################################################
$smarty->assign("search_categories", $search_categories);
unset($search_categories);

$smarty->assign("sort_fields", $sort_fields);
$smarty->assign("main","search");

?>
