<?php 
//namespace shopfest;

include_once "$xcart_dir/include/func/func.order.php";
include_once("$xcart_dir/include/class/widget/class.widget.keyvaluepair.php");

class ShopFest{
	
//get timestamp from a date.. date is in format YYYY-MM-DD
	public static function getTimeStampFromDate($date){
		list($year, $month, $day) = explode('-',$date);
		$ts =  mktime(0, 0, 0, $month, $day, $year);
		return $ts;
	}
	
	//get date of last saturday of week (week is from sat to friday) given a timestamp
	public static function getWeekStartDate($timeStamp){
		//$startDayOfWeek = 6;
		$startDayOfWeek = WidgetKeyValuePairs::getWidgetValueForKey('fest_week_start_day');
		if(date('w',$timeStamp)!=$startDayOfWeek){
			$weekIndex = $startDayOfWeek==0? 7 : $startDayOfWeek;
			if($weekIndex > date('w',$timeStamp)){
				$lastSat = date('Y-m-d', $timeStamp+(((-7 + $weekIndex) - date('w',$timeStamp))*24*3600));
			}
			else{
				$lastSat = date('Y-m-d', $timeStamp+((($weekIndex) - date('w',$timeStamp))*24*3600));
			}
		}
		else{
			$lastSat = date('Y-m-d', $timeStamp);
		}
		return $lastSat;
	}
		
	//get date of next friday of week (week is from sat to friday) given a timestamp. If given timestamp is of friday, return todays date
	public static function getWeekEndDate($timeStamp){
		//$endDayOfWeek = 5; //get from widget key value
		$endDayOfWeek = WidgetKeyValuePairs::getWidgetValueForKey('fest_week_end_day');
		$weekIndex = ($endDayOfWeek + 8 - date('w',$timeStamp)) % 7;
		$weekIndex = $weekIndex==0 ? 7 : $weekIndex; 
		$lastDayOfWeek = date('Y-m-d', $timeStamp + ($weekIndex * 24*3600));
		return $lastDayOfWeek;
	}
	
		
	public static function getLeaderBoardId(){
		$lb_id = WidgetKeyValuePairs::getWidgetValueForKey('fest_leader_board_id');
		return $lb_id;
	}

	/*
	 * $lb_id -> leader board id
	 * $date -> yyyy-mm-dd
	 * $n -> number of top buyers
	 * $category -> 1:men, 2:women, 3:total
	 * return format -> [login, name, amount]
	 */
	public static function getTopNBuyersOnADate_h($lb_id,$date,$n,$category){
			$leaderboard = func_query_first("select lb.id from leader_board lb where lb.id = $lb_id", true);
			if($leaderboard){ //if leader board exists
				$results = func_query("select login, name, sum(amount) as amount from shopping_fest_leader_board where date='".$date."' and category=$category group by login order by amount desc limit ".$n, true);
			}
		 	return $results;
		}
	
	//Get Top 5 buyer for a particular date..if for today, pass $isToday as true, else as false. For dates other than today, this query is cached for 30 days.. so only once db will be hit
	private function getTopNBuyersOnADate($lb_id,$date,$isToday){
		return $this->getTopCategoryBuyerOnADate($lb_id, $date, $isToday, "Total");
	}
	
	//Get top buyer in Men's / Women's Category on a given date..if for today, pass $isToday as true, else as false. For dates other than today, this query is cached for 30 days.. so only once db will be hit
	// $category will have value either 'Men' or 'Women'
	private function getTopCategoryBuyerOnADate($lb_id,$date,$isToday,$category){

		$n = WidgetKeyValuePairs::getWidgetValueForKey('fest_topNCategoryBuyers');
		$category = ($category == 'Men')? 1 : (($category == 'Women') ? 2 : 3);

		if($category == 3)
			$n = WidgetKeyValuePairs::getWidgetValueForKey('fest_topNbuyers');
		
		if($isToday){
			$date = date('Y-m-d');
		}	
			$trending = ShopFest::getTopNBuyersOnADate_h($lb_id,$date,$n,$category);
		
		
		return $trending;
	}
		
	
	//get users total amount spend in a particular category... $category is either 'Men' or 'Women'
	private function getUsersCategoryBuyOnADate($lb_id,$date,$isToday,$category,$login){
		$category = ($category == 'Men')? 1 : (($category == 'Women') ? 2 : 3);
		
		$query = '';
		
		if($isToday){
			$date = date('Y-m-d');	 	
		}
		
		$query = "select sum(amount) as amount, login, name from shopping_fest_leader_board where login='$login' and date='".$date."' and category=$category group by login order by amount desc";
		return func_query($query, true);
	}

	private function createLBData($dailyWinners, $n){
		$topBuyers = array();
		if($dailyWinners!=false){ //array is not false
				for($i=0;$i<$n;$i++){
					if($dailyWinners[$i]['amount'] != NULL){
						$topBuyers[$i] = $dailyWinners[$i];
						$topBuyers[$i]['pos']=$i+1;
					}
					else{
						$topBuyers[$i]['name'] = null;
						$topBuyers[$i]['login'] = null;
						$topBuyers[$i]['amount']=null;
						$topBuyers[$i]['pos']=$i+1;
					}
				}
			}
		else{//put all positions as null
			for($i=0;$i<$n;$i++){
				$topBuyers[$i]['name'] = null;
				$topBuyers[$i]['amount']=null;
				$topBuyers[$i]['login'] = null;
				$topBuyers[$i]['pos']=$i+1;
			}
		}		
		return $topBuyers;
	}

	private function getPrizes($prizesConfigured, $myAmt, $n, $leaderBoard ){
		$ps=$n;//position
		for($i=$n-1;$i>=0;$i--){
			if(!empty($leaderBoard[$i]) && !empty($leaderBoard[$i]['amount']) && $myAmt<=$leaderBoard[$i]['amount']){
				$ps = $i;
				break;
			}
		}
		$prizesConfigured = explode(";", $prizesConfigured);
		if($ps>=$n-1){//user is not in top $n
			if(empty($leaderBoard[$n-1]['amount'])){
				$bagAllAmt = $top5amount;
			}else{
				$bagAllAmt = $leaderBoard[$n-1]['amount'];
			}
			$prize = $prizesConfigured[$n-1];
		}else{//user in top $n
			if($ps==0){
				$bagAllAmt = $myAmt;
			}else{
				while($prizesConfigured[$ps]==$prizesConfigured[$ps-1]){
					$ps -= 1;
					if($ps==0) {$bagAllAmt = $myAmt; break;}
				}
				if($ps>0){
					$bagAllAmt = $leaderBoard[$ps-1]['amount'];
					$prize = $prizesConfigured[$ps-1];
				}
			}
		}
		$retVal = array();
		$retVal["amt"] = floatval($bagAllAmt);
		$retVal["prize"] = $prize;
		return $retVal;
	}
	
	//This function returns an array containing top 5 buyers,top buyer in men category,top buyer in women category,users total amount, users amount
	//in men's category, users amoung in women's category and the diff amount that user needs to shop for to get into one of these 7 toppers
	//if $full_data is false then it returns an array with 1st 5 values giving top buyers (null if no one on that pos) and rest two giving top buyers in men and women category
	public function getLeaderBoardData($lb_id,$date,$isToday,$login,$user_specific_data){
		if(!FeatureGateKeyValuePairs::getBoolean("myntrashoppingfestleaderboard"))
			return null;
		$top2amount = 0;//10000;
		$top5amount = 0;//5000;
		$topCatAmount = 0;//5000;
		
		$dailyWinners = $this->getTopNBuyersOnADate($lb_id,$date,$isToday);
		$n = WidgetKeyValuePairs::getWidgetValueForKey('fest_topNbuyers');
		
		$leaderBoard = array();
		$leaderBoard['topAll'] = $this->createLBData($dailyWinners, $n);
		
		$nCategory = WidgetKeyValuePairs::getWidgetValueForKey('fest_topNCategoryBuyers');
		$menCategory = $this->getTopCategoryBuyerOnADate($lb_id,$date,$isToday,'Men');
		$leaderBoard['topMen'] = $this->createLBData($menCategory, $nCategory);
		
		$womenCategory = $this->getTopCategoryBuyerOnADate($lb_id,$date,$isToday,'Women');
		$leaderBoard['topWomen'] = $this->createLBData($womenCategory, $nCategory);

		//if we need full data including users data
		if($user_specific_data){
			$user_bag_all = $this->getTotalAmountByUser($lb_id,TRUE,$login);
			$user_women_category = $this->getUsersCategoryBuyOnADate($lb_id,$date,$isToday,'Women',$login);
			$user_men_category = $this->getUsersCategoryBuyOnADate($lb_id,$date,$isToday,'Men',$login);

			$leaderBoard['myBagAll'] = $user_bag_all;
			$leaderBoard['myMen'] = empty($user_men_category)?0:$user_men_category[0]['amount'];
			$leaderBoard['myWomen'] = empty($user_women_category)?0:$user_women_category[0]['amount'];
						
			//for my bag
			$prizesConfigured = WidgetKeyValuePairs::getWidgetValueForKey("fest_prizes_semicolon_separated");
			$retValAll = $this->getPrizes($prizesConfigured, $leaderBoard['myBagAll'], $n, $leaderBoard['topAll']);
		
			//for men category
			$prizesConfigured = WidgetKeyValuePairs::getWidgetValueForKey("fest_prizes_men_semicolon_separated");
			$retValMen = $this->getPrizes($prizesConfigured, $leaderBoard['myMen'], $nCategory, $leaderBoard['topMen']);

			//for men category
			$prizesConfigured = WidgetKeyValuePairs::getWidgetValueForKey("fest_prizes_women_semicolon_separated");
			$retValWomen = $this->getPrizes($prizesConfigured, $leaderBoard['myWomen'], $nCategory, $leaderBoard['topWomen']);
			
			$menCatdiff = $retValMen["amt"] - $leaderBoard['myMen'];
			$womenCatdiff= $retValWomen["amt"] - $leaderBoard['myWomen'];
			$bagAlldiff= $retValAll["amt"] - $leaderBoard['myBagAll'];
		
				
			$mindiff = min(array_filter(array($menCatdiff,$womenCatdiff,$bagAlldiff)));
			if($mindiff == $menCatdiff) {$prize = $retValMen['prize'];}
			elseif($mindiff == $womenCatdiff) {$prize = $retValWomen['prize'];}
			elseif($bagAlldiff == $mindiff) $prize = $retValAll['prize'];
			 
			if(empty($mindiff))
				$msg = WidgetKeyValuePairs::getWidgetValueForKey("fest_top_in_all_cat_msg");
			else { $mindiff++;
				$msg = WidgetKeyValuePairs::getWidgetValueForKey("fest_shop_more_to_win_msg");
				eval("\$msg = \"$msg\";");
			}
			$leaderBoard['msg'] = $msg;
		}
		return $leaderBoard;
		
	}
	

	// get total amount spend by a user in overall category...if $forDay is true then calculate for today or if its false do it for month
	public function getTotalAmountByUser($lb_id,$forDay,$login){
		$leaderboard = func_query_first("select lb.id from leader_board lb where lb.id = $lb_id", true);
		if($forDay){
			$startDate = date("Y-m-d");
			$endDate = date("Y-m-d", strtotime( '+1 day' ) );			
		}
		else{
			$startDate = WidgetKeyValuePairs::getWidgetValueForKey('fest_month_start_date'); 
			$endDate = WidgetKeyValuePairs::getWidgetValueForKey('fest_month_end_date'); 
		}
		
		if($leaderboard){
			$trending = func_query("select login, name, sum(amount) as amount, order_count as orders from shopping_fest_leader_board where login = '$login' and date >= '$startDate' and date <= '$endDate' and category=3 group by login", true);
			if(!empty($trending))
				return $trending[0]["amount"];
			//$results = func_query("select o.login, concat(c.firstname,' ',c.lastname) as name, sum(total+cod+cash_redeemed) as amount, count(distinct orderid) as orders from xcart_orders o use index(order_date), xcart_customers c where o.login = c.login and o. and o.status in ('OH','Q','WP','SH','DL','C') group by o.login", true);
		}
			
		return 0;
	}
	
	//for weekly winners
	/*
	 * return Value 
	 * amount -> the Rs. X more to shop for to get atleast a ticket
	 * tickets -> total number of tickets obtained
	 */
	public function getTotalWeekTickets($lb_id,$login){
		$leaderboard = func_query_first("select lb.id from leader_board lb where lb.id = $lb_id", true);
		if($leaderboard){
			$startPrice = WidgetKeyValuePairs::getFloat("fest_lucky_draw_min_amount", 5000);
			$increment = WidgetKeyValuePairs::getFloat("fest_lucky_draw_increment_amount", 1000);;

			$startDate = ShopFest::getWeekStartDate(time());
			$endDate = ShopFest::getWeekEndDate(time());
			$query = "select sum(amount) as amount, ((sum(amount) - $startPrice) - (sum(amount) -$startPrice)%$increment)/$increment + 1 as tickets, login, name from  shopping_fest_leader_board where ";
			if(!empty($login)){
				$query .= "login='$login' and "; 
			}
			$query .= " category=3 and date>='$startDate' and date<'$endDate' group by login";
			$results = func_query($query, true);
			if(!empty($results))
				if(empty($login))
					return $results;
				if($results[0]["amount"]>=$startPrice)
					$tickets = $results[0]["tickets"];
				else 
					$amount = $startPrice - $results[0]["amount"];
		}
		$retVal = array();
		$retVal["amount"] = empty($amount)?0:$amount;
		$retVal["tickets"] = empty($tickets)?0:$tickets;
		return $retVal;
	}
	
	//get last weeks winners..returns all winners in all weeks passed in this month
	public function getLastWeekWinners($lb_id){
		return func_query("select * from weekly_winners", true);
	}
	
	//get no. of winners in last x days, where x is the number of days passed from dec 1 daily_winners
	public static function getNumberOfWinnersMsg_h($lb_id){
			$todaysTimeStamp = ShopFest::getTimeStampFromDate(date("Y-m-d"));
			$currentWeek = date("w", date("Y-M-D"));	
			$total_daily_winners = func_query("select count(*) as count from daily_winners dw where dw.date <= $todaysTimeStamp ",true);
			$total_winner_count = 0;
			if(!empty($total_daily_winners)){//if its false ie empty query
				$total_winner_count += $total_daily_winners[0]["count"]; 
			}
			/*$total_weekly_winners = func_query("select count(*) as count from weekly_winners ww where ww.week < $currentWeek",true);
			if(!empty($total_weekly_winners)){//if its false ie empty query
				$total_winner_count += $total_weekly_winners[0]["count"]; 
			}*/
			$date1 = WidgetKeyValuePairs::getWidgetValueForKey("fest_month_start_date");
			$diff = abs(strtotime(date('Y-m-d')) - strtotime($date1));
			$no_of_days = floor($diff / (60 * 60 * 24));
			
			if($no_of_days==0){
				$msg = ($total_winner_count)." winners so far"; 
			}
			else{
				$msg = ($total_winner_count)." winners in last ".$no_of_days." days.";
			}
			return $msg;
	}
	
	public function getNumberOfWinnersMsg($lb_id){
		return ShopFest::getNumberOfWinnersMsg_h($lb_id);
		
	}
	
	
	
//get no. of winners in last x days, where x is the number of days passed from dec 1 daily_winners
	public static function getNumberOfWeeklyWinnersMsg_h($lb_id){
			$todaysTimeStamp = ShopFest::getTimeStampFromDate(date("Y-m-d"));
			$currentWeek = date("w", date("Y-M-D"));	
			//$total_daily_winners = func_query("select count(*) as count from daily_winners dw where dw.date <= $todaysTimeStamp ",true);
			$total_winner_count = 0;
			/*if(!empty($total_daily_winners)){//if its false ie empty query
				$total_winner_count += $total_daily_winners[0]["count"]; 
			}*/
			$total_weekly_winners = func_query("select count(*) as count from weekly_winners ww where ww.week < $currentWeek",true);
			if(!empty($total_weekly_winners)){//if its false ie empty query
				$total_winner_count += $total_weekly_winners[0]["count"]; 
			}
			$date1 = WidgetKeyValuePairs::getWidgetValueForKey("fest_month_start_date");
			$diff = abs(strtotime(date('Y-m-d')) - strtotime($date1));
			$no_of_days = floor($diff / (60 * 60 * 24));
			
			if($no_of_days==0){
				$msg = ($total_winner_count)." winners so far"; 
			}
			else{
				$msg = ($total_winner_count)." winners in last ".$no_of_days." days.";
			}
			return $msg;
	}
	
	public function getNumberOfWeeklyWinnersMsg($lb_id){
		return ShopFest::getNumberOfWeeklyWinnersMsg_h($lb_id);
		
	}
	
	
	//for monthly winners $range is true then use min and max, else use only min... ie if no. of users from 50k to 1lk then use $range=true and min = 50k and max = 1lk..
	//if number of users above 1lk then $range =false and min = 1lk and max can be anything as its not used
	public function getAllBuyersInARange($lb_id,$range,$min,$max){
		$leaderboard = func_query_first("select lb.id from leader_board lb where lb.id = $lb_id", true);
			if($leaderboard){
				$startDate = WidgetKeyValuePairs::getWidgetValueForKey('fest_month_start_date'); 
				$endDate = WidgetKeyValuePairs::getWidgetValueForKey('fest_month_end_date'); 
				$query = "select count(*) as count from (select sum(amount) as amount, login from  shopping_fest_leader_board where date>='$startDate' and date<'$endDate' and category=3 group by login) as a"; 
				if($range){
					$query .= " where amount>= $min and amount < $max"; 
				}else{
					$query .= " where amount>= $min";
				}
				$results = func_query($query, true);
				return $results[0]['count'];
			}
		 	return 0;
	}
	
	
	//........functions required to update tables like weekly_winners, daily_winners
	//This function returns for all users who have shopped for more than 5k, no. of tickets earned for given week, starting from $startDate and ending on $endDate
	public static function getAllUsersTicketsForWeek_h($lb_id,$startDate,$endDate){
		$leaderboard = func_query_first("select lb.id from leader_board lb where lb.id = $lb_id", true);
		//remove below two lines
			/*$startTime = 0;
			$endTime = 1356978600;*/
			
		if($leaderboard){
			$startPrice = 5000;
			$increment = 1000;
			//uncomment below two lines
			$startTime = ShopFest::getTimeStampFromDate($startDate);
			$endTime = ShopFest::getTimeStampFromDate($endDate);
			$results = func_query("select *,((amount - $startPrice) - (amount -$startPrice)%$increment)/$increment + 1 as tickets from (select o.login, concat(c.firstname,' ',c.lastname) as name, sum(total+cod+cash_redeemed) as amount from xcart_orders o use index(order_date), xcart_customers c where o.login = c.login and date >= $startTime and date <= $endTime and o.status in ('OH','Q','WP','SH','DL','C','PK') group by o.login ) final where final.amount > $startPrice",true);
		}
		return $results;
	}
	//This function returns for all users who have shopped for more than 5k, no. of tickets earned for given week
	public function getAllUsersTicketsForWeek($lb_id){
		$trending = $this->getTotalWeekTickets($lb_id);
		return $trending;
	}
	
	//Other functions
	//Return number of days remaining for this week to end including today. Assuming week starts from sat and ends on friday
	public function DaysToGoForThisWeek(){
		
		$date1 = ShopFest::getWeekEndDate(time());
		$diff = abs(strtotime(date('Y-m-d')) - strtotime($date1));
		$days = floor($diff /(60*60*24));
		
		//$givenday = date('w',time()); //returns the number for today, with monday being 1 and sat being 6
		$hrs = 24 - date("G")-1;
		$min = 60 - date("i")-1;
		if($days==1){
			$msg = $hrs." HRS ".$min." MIN";
		}
		else{
			$msg =  ($days-1)." DAYS ".$hrs." HRS";
		}
		/*if($givenday ==6){
			$msg = $hrs." HRS ".$min." MIN";
		}
		else{
			$msg =  (6 - $givenday)." DAYS ".$hrs." HRS";
		}	*/	
		return $msg;
	}
	
	//return no. of days remaining for this festival. Assuming it ends on 1 of jan. and dec has 31 days
	public function DaysToGoForMonth(){
		$date1 = WidgetKeyValuePairs::getWidgetValueForKey("fest_month_end_date");
		$diff = abs(strtotime(date('Y-m-d')) - strtotime($date1));
		$days = floor($diff / (60 * 60 * 24));
		//$days = date("t") - date("d");
		$hrs = 24 - date("G") - 1;
		$min = 60 - date("i") - 1;
		if($days==0){
			$msg = $hrs." HRS ".$min." MIN";
		}
		else{
			$msg = $days." DAYS ".$hrs." HRS";
		}
		return $msg;
	}

	public static function insertRUpdateData($login, $category, $amount, $isDebit, $orderDate){
		$date = date("Y-m-d", $orderDate);
		
		//$amount = $isDebit ? (-1 * floatval($amount)) : floatval($amount);
		$amount = $amount<=0 ? 0: $amount;

		//preparing the row for new insert if required
		$row = array(login=>$login, category=>$category, amount=>$amount, order_count=>1, date=>$date);
		
		//same user placing the order multiple times within few milliseconds is very rare
		$query = "select id, amount, order_count from shopping_fest_leader_board where login='$login' and date='$date' and category=$category";
		$results = func_query_first($query, true);
		if(!empty($results)){//update amount and order count
			if(!$isDebit){
 				$amount += floatval($results["amount"]);
				$order_count_increment = 1;
			}else{
 				$amount = floatval($results["amount"]) - floatval($amount);
				$order_count_increment = -1;
			}

			$amount = $amount<=0 ? 0: $amount;
			$updatequery = "update shopping_fest_leader_board set amount=$amount, order_count=order_count+$order_count_increment where login='$login' and date='$date' and category=$category";
			//echo $updatequery; exit;
			db_query($updatequery);
		}else if(!$isDebit){
			$row["name"] = func_query_first_cell("select concat(firstname,' ',lastname) as name from xcart_customers where login='$login'", true);
			func_array2insertWithTypeCheck("shopping_fest_leader_board", $row);
		}
	}
	/*
	 * Call this when an amount needs to added or subtracted from leaderboard
	 * $orderAmount - Total amount paid by/to the user + cashback applied, if zero then the entry for category 3 will not even be considered
	 * $login - email of the user for whom the data in leader board shoul dbe modified
	 * $orderDate - date on which the current operation on the order is placed, cancelled, returned
	 * $isDebit - if true then the amount will be subtracted from the leader board, else will be added to the leader board total
	 * $orderDetails - an array with values for the following keys
	 * 		    key			value
			productStyleId -> style Id of the product
			totalPrice     -> price (MRP) * quantity
			discount       -> discount for all the quantities
			coupon_discount-> the discount applied on the item 
	 */	
	public static function updateLeaderBoardData($orderAmount, $login, $orderDate, $orderDetails, $isDebit = false ){

		$amount = $orderAmount;		
		
		if($amount>0) {
			$category = 3; //first deal with general category
			ShopFest::insertRUpdateData($login, $category, $amount, $isDebit, $orderDate);
		}

		//add data for different categories
	    $styleIdList = array();
	    $orderStyleToAmountMap = array();
        foreach($orderDetails as $orderItem)
        {
                $styleIdList[] = $orderItem['productStyleId'];
                $styleId = $orderItem['productStyleId'];
                $orderStyleToAmountMap["$styleId"] = (floatval($orderItem["totalPrice"]) - floatval($orderItem["discount"])) - floatval($orderItem["coupon_discount"]) ;
        }
		$categories = func_query("select global_attr_gender as category, style_id as style from mk_style_properties where style_id in (".implode(",", $styleIdList).")", true);
		$catArr = array();
		foreach ($categories as $catRow){
			$styleId = $catRow["style"];
			$category = strtolower(trim($catRow["category"]));
			$catArr["$category"] += floatval($orderStyleToAmountMap["$styleId"]);
		}
		foreach ($catArr as $category=>$amount){
			if($category == 'unisex'){
				ShopFest::insertRUpdateData($login, 1, $amount, $isDebit, $orderDate);//Men
				ShopFest::insertRUpdateData($login, 2, $amount, $isDebit, $orderDate);//Women
			}else{
				if($category == 'men') $category = 1;
				else if($category =='women') $category = 2;
				else continue;
				ShopFest::insertRUpdateData($login, $category, $amount, $isDebit, $orderDate);	
			}
		}
		
	} 
}
?>
