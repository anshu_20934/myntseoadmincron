<?php
require_once("$xcart_dir/include/solr/solrProducts.php");
require_once("$xcart_dir/include/solr/solrCategorySearch.php");
require_once("$xcart_dir/include/solr/solrStatsSearch.php");
require_once("$xcart_dir/include/solr/solrQueryBuilder.php");
require_once("$xcart_dir/include/solr/solrUtils.php");
require_once("$xcart_dir/include/func/func.utilities.php");
require_once("$xcart_dir/include/class/search/SearchProducts.php");

require_once("$xcart_dir/include/class/widget/interface.widget.php");
include_once("$xcart_dir/include/class/widget/class.static.functions.widget.php");
include_once("$xcart_dir/include/class/class.feature_gate.keyvaluepair.php");

/**
 * Class for Dynamic content
 * @author yogesh
 *
 */
abstract class DContent{
	/**
	 * Name should be same as feature gate variable to control this content
	 * @var String
	 */
	private $name;
	/**
	 * Placeholder tag
	 * @var unknown_type
	 */
	private $tag;
	function __construct($name,$tag){
		$this->name=$name;
		$this->tag=$tag;
	}
	function getName(){
		return $this->name;
	}
	function getTag(){
		return $this->tag;
	}
	abstract function getContent();
	
	function isEnabled(){
		$enabled=FeatureGateKeyValuePairs::getFeatureGateValueForKey($this->name);
		return !empty($enabled)&&$enabled=='true';
	}
	/**
	 * generates the content
	 */
	function genContent(){
		$this->getContent();
	}
}

/**
 * Class Dynamic content manager
 * This is responsibe for replacing the tags in HTML output with the content of DContent
 * @author yogesh
 *
 */
class DynamicContentManager{
	private $content_array;
	private $capturing=false;
	
	/**
	 * This call makes DynamicContentManager to start capturing buffer. 
	 */
	public function startCapture(){
		ob_start(array($this, 'execute'));
		$this->capturing=true;
	}
	
	function __destruct ()
	{
		if ($this->capturing)
			ob_end_flush();
	}
	
	public function execute ($buffer){
		$this->content = $buffer;
		foreach ($this->content_array as $dContent){
			$div_content=$dContent->getContent();
			if($dContent->isEnabled()==false){
				$div_content="";
			}
			$this->content = str_replace($dContent->getTag(), $div_content, $this->content);
		}
		return $this->content;
	}
	
	/**
	 * resisters a DContent to be replace by by tags
	 * @param DContent $dContent
	 */
	public function addDContent($dContent){
		$this->content_array[$dContent->getName()]=$dContent;
	}
	
}

$__DCONTENT_MANAGER=new DynamicContentManager();

?>