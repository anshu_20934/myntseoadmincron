<?php
/*****************************************************************************\
 +-----------------------------------------------------------------------------+
 | Myntra                                                                      |
 | Copyright (c) 2008 Myntra, Shantanu Bhadoria			                       |
 | All rights reserved.                                                        |
 +-----------------------------------------------------------------------------+
 |                                                                             |
 | The Initial Developer of the Original Code is Shantanu Bhadoria             |
 +-----------------------------------------------------------------------------+
 \*****************************************************************************/

#
# $Id: class.ordertime.php,v 1.0
# Class With functions to calculate shipping time and processing time etc.

require_once 'class.singleton.php';
require_once 'class.myntra_constants.php';

class ordertime{

	// Function to generate a smaller date string for smses
	function get_max_delivery_date($order_id){
	
		$deliverytime= ordertime::get_order_processingtime($order_id) + ordertime::get_order_shippingtime($order_id);
		$delivery_date = ordertime::get_dates_from_time($deliverytime);
		return $delivery_date;
	}

	
	// Function to retireve the estimated processing time for an order given the order id for the order
	function get_order_time_string($order_id,$customer_string="y"){
		$sql_tbl = myntra_constants::$sql_tbl;

		//$db = singleton::getmysqlinstance();

		$query = "select * from {$sql_tbl['orders']} where orderid ='{$order_id}'";
		//$order = $db->get_row($query,ARRAY_A);
		$order = func_query_first($query);

		if($customer_string !="y" && !empty($order[queueddate]))
			$queueddate=$order[queueddate];	
		else $queueddate="";

		//string to return if order is shipped together.
		if( $order['shipment_preferences'] == 'shipped_together'){
			$order_processing_time = ordertime::get_order_processingtime($order_id);
			$shipping_time = ordertime::get_order_shippingtime($order_id);
			$processing_date = ordertime::get_dates_from_time($order_processing_time,$queueddate);
			$delivery_date = ordertime::get_dates_from_time($order_processing_time+$shipping_time,$queueddate);
			$deliverdate_timestamp = strtotime($delivery_date);
			
			//to show buffter of 1 day each earlier or after deliver date
			if($deliverdate_timestamp !== false){
				$previous = date('jS M Y',$deliverdate_timestamp-86400);
				$after = date('jS M Y',$deliverdate_timestamp+(86400*2));
				$delivery_date = "between $previous and $after.";
			}

			if($customer_string == "y")
				$return_string = "Your order will be shipped by {$processing_date} and will be delivered at your doorstep {$delivery_date}.";
			else
				$return_string = "Date of shipping: {$processing_date} , Date of delivery: {$delivery_date}.";
			return $return_string;
		}
		else {
			$shipping_time = ordertime::get_order_shippingtime($order_id);
			$orderitems_processing_times = ordertime::get_orderitems_processingtime($order_id);
			if($customer_string == "y")
				$return_string = 'Order will be delivered to you as per the following schedule : <br><br>';
			$return_string .= '<table width="98%"  border="1" align="center" cellpadding="0" cellspacing="0" style="font-size:12;border-style:solid"><tr style="font-size:14; background-color:#dddddd"><th>Item</th><th>Quantity</th><th>Expected Shipping Date</th><th>Expected Delivery Date</th></tr>';
			foreach($orderitems_processing_times as $row){
				$shipping_date = ordertime::get_dates_from_time($row['PRODUCT_PROCESSING_TIME'],$queueddate);
				$delivery_date = ordertime::get_dates_from_time($row['PRODUCT_PROCESSING_TIME']+$shipping_time,$queueddate);
				$return_string .="<tr align='center'><td>{$row['PRODUCT_NAME']}</td><td>{$row['PRODUCT_QUANTITY']}</td><td>{$shipping_date}</td><td>{$delivery_date}</td></tr>";
			}
			$return_string .='</table>';
			return $return_string;
			
		}
	}
	
		// Function to retireve the estimated processing time for an order given the order id for the order
	function get_order_time_data($order_id){
		$sql_tbl = myntra_constants::$sql_tbl;
		//$db = singleton::getmysqlinstance();
		$query = "select * from {$sql_tbl['orders']} where orderid ='{$order_id}'";
		//$order = $db->get_row($query,ARRAY_A);
		$order = func_query_first($query);
		
		//string to return if order is shipped together.
		if( $order['shipment_preferences'] == 'shipped_together'){
			$order_processing_time = ordertime::get_order_processingtime($order_id);
			$shipping_time = ordertime::get_order_shippingtime($order_id);
			$processing_date = ordertime::get_dates_from_time($order_processing_time);						
			$delivery_date = ordertime::get_dates_from_time($order_processing_time+$shipping_time);
			$return_string = array(
					PROCESSING_DATE => $processing_date,					
					DELIVERY_DATE => $delivery_date
				);
			return array(
				DATA => $return_string,
				SHIPPED_TOGETHER => 1
				);
		}
		else {
			$return_string = array();
			$shipping_time = ordertime::get_order_shippingtime($order_id);
			$orderitems_processing_times = ordertime::get_orderitems_processingtime($order_id);
			foreach($orderitems_processing_times as $row){
				$processing_date = ordertime::get_dates_from_time($row['PRODUCT_PROCESSING_TIME']);
				$delivery_date = ordertime::get_dates_from_time($row['PRODUCT_PROCESSING_TIME']+$shipping_time);
				array_push($return_string,array(
						PRODUCT_ITEMID			=> $row['PRODUCT_ITEMID'],
						PRODUCT_NAME			=> $row['PRODUCT_NAME'],
						PRODUCT_QUANTITY		=> $row['PRODUCT_QUANTITY'],
						PRODUCT_PROCESSING_DATE	=> $processing_date,
						PRODUCT_DELIVERY_DATE	=> $delivery_date
					));
			}
			return array(
				DATA => $return_string,
				SHIPPED_TOGETHER => 0
				);
		}
	}

	//function to get date relative to current date from a given number of days discounting the sundays
	function get_dates_from_time( $time_needed,$start_time="" ){
		$number_of_working_days_needed =floor($time_needed/24)+1;
		
		$working_days_in_current_week = 7-date("N",time());
		$working_days_needed_after_first_sunday = $number_of_working_days_needed-$working_days_in_current_week;
		if($working_days_needed_after_first_sunday<0){
			$working_days_needed_after_first_sunday = 0;
		}
		$days_after_first_sunday = (floor($working_days_needed_after_first_sunday/6)*7)+$working_days_needed_after_first_sunday%6;
		$total_days = $days_after_first_sunday+7-date("N",time());
		
		if($number_of_working_days_needed<=$working_days_in_current_week){
			$total_days = $number_of_working_days_needed;
		}
		
		$total_days++;
		if(empty($start_time))
			$final_time = strtotime("+{$total_days} day",time());
		else 
			$final_time = strtotime("+{$total_days} day",$start_time);
		$final_date = date("jS M Y", $final_time);
		return $final_date;
	}
	
	//function to get time needed for processing an order
	function get_order_processingtime($order_id) {
		$sql_tbl = myntra_constants::$sql_tbl;
		//$db = singleton::getmysqlinstance();

		$order_query = "select DISTINCT a.product_type as product_type, SUM(a.amount) as total_amount, b.processing_time as processing_time, b.max_processing_quantity as max_processing_quantity from {$sql_tbl['order_details']} a INNER JOIN {$sql_tbl['mk_product_type']} b on a.product_type=b.id where a.orderid = '{$order_id}' group by a.product_type";
		$maxtime = 0;
		//$order_result = $db->get_results($order_query,ARRAY_A);
		$order_result = func_query($order_query);
		
		if($order_result){
			foreach($order_result as $row) {
				$maxtime = max($maxtime,($row['processing_time']*(floor($row['total_amount']/$row['max_processing_quantity'])+1)));
			}
		}
		return $maxtime;
	}
	
	
	function get_orderitems_processingtime($order_id) {
		$sql_tbl = myntra_constants::$sql_tbl;
		//$db = singleton::getmysqlinstance();
		$order_item_array = array();
		$order_query = "select DISTINCT a.itemid as itemid, a.product_type as product_type, SUM(a.amount) as total_amount, b.name as product_name, b.processing_time as processing_time, b.max_processing_quantity as max_processing_quantity from {$sql_tbl['order_details']} a INNER JOIN {$sql_tbl['mk_product_type']} b on a.product_type=b.id where a.orderid = '{$order_id}' group by a.product_type";
		$maxtime = 0;
		//$order_result = $db->get_results($order_query,ARRAY_A);
		$order_result = func_query($order_query);
		
		if($order_result){
			foreach($order_result as $row) {
				array_push($order_item_array, array(
													PRODUCT_ITEMID			=> $row['itemid'],
													PRODUCT_NAME			=> $row['product_name'],
													PRODUCT_QUANTITY		=> $row['total_amount'],
													PRODUCT_PROCESSING_TIME	=> ($row['processing_time']*(floor($row['total_amount']/$row['max_processing_quantity'])+1))
												   )
						   );
			}
		}
		return $order_item_array;
	}

	// Function to retrieve the estimated shipping days needed for an order given the order id for the order
	function get_order_shippingtime($order_id) {
		$sql_tbl = myntra_constants::$sql_tbl;
		//$db = singleton::getmysqlinstance();
		$order_query = "select courier_service, s_country, s_state, s_city from {$sql_tbl['orders']} where orderid='{$order_id}'";
		$maxtime = 0;
		//$order_data = $db->get_row($order_query,ARRAY_A);
		$order_data = func_query_first($order_query);
		$order_data['s_country'] = mysql_real_escape_string($order_data['s_country']);
		$order_data['s_state'] = mysql_real_escape_string($order_data['s_state']);
		$order_data['s_city'] = mysql_real_escape_string($order_data['s_city']);
		if($order_data){
			$shipping_time_query1 = "select * from {$sql_tbl['shipping_time']} where LOWER(destination_country)=LOWER('{$order_data['s_country']}') and LOWER(destination_city)=LOWER('{$order_data['s_city']}') and LOWER(destination_state)=LOWER('{$order_data['s_state']}') and courier_code='{$order_data['courier_service']}'";
			//$shipping_time_data1 =$db->get_row($shipping_time_query1,ARRAY_A);
			$shipping_time_data1 =func_query_first($shipping_time_query1);
			if($shipping_time_data1) {
				return ($shipping_time_data1['shipping_time']+$shipping_time_data1['buffer_time']);
			}else {
				$shipping_time_query2 = "select * from {$sql_tbl['shipping_time']} where LOWER(destination_country)=LOWER('{$order_data['s_country']}') and LOWER(destination_state)=LOWER('{$order_data['s_state']}') and LOWER(destination_city)='others' and courier_code='{$order_data['courier_service']}'";
				//$shipping_time_data2=$db->get_row($shipping_time_query2,ARRAY_A);	
				$shipping_time_data2 =func_query_first($shipping_time_query2);
				if ($shipping_time_data2) {
					return ($shipping_time_data2['shipping_time']+$shipping_time_data2['buffer_time']);
				}else{
					$shipping_time_query3 = "select * from {$sql_tbl['shipping_time']} where LOWER(destination_country)=LOWER('{$order_data['s_country']}') and LOWER(destination_state)='others' and LOWER(destination_city)='others' and courier_code='{$order_data['courier_service']}'";
					//$shipping_time_data3 = $db->get_row($shipping_time_query3,ARRAY_A);
					$shipping_time_data3 = func_query_first($shipping_time_query3);
					if($shipping_time_data3) {
						return ($shipping_time_data3['shipping_time']+$shipping_time_data3['buffer_time']);
					}
				}
			}
		}
	}
	
	function get_order_delivery_date($orderid){
		$number_of_working_days_needed = floor((ordertime::get_order_processingtime($orderid)+ordertime::get_order_shippingtime($orderid))/24)+1;
		if(date("N",time())>5) {
			$working_days_in_current_week = 0;
		}
		else {
			$working_days_in_current_week = 6-date("N",time());
		}		
		$working_days_needed_after_first_sunday = $number_of_working_days_needed-$working_days_in_current_week;
		if($working_days_needed_after_first_sunday<0){
			$working_days_needed_after_first_sunday = 0;
		}
		$days_after_first_sunday = (floor($working_days_needed_after_first_sunday/6)*7)+$working_days_needed_after_first_sunday%6;
		$total_days = $days_after_first_sunday+7-date("N",time());
		
		if($number_of_working_days_needed<=$working_days_in_current_week){
			$total_days = $number_of_working_days_needed;
		}
		
		$total_days++;
		
		$deliverytime = strtotime("+{$total_days} day",time());
		$delivery_date = date("jS M Y", $deliverytime);
		return $delivery_date;
	}
	
	/*
	##to get the unix_timestamp at 7pm
	##on the date(order processing date) passed
	*/
	function get_process_time_7pm($processing_date){
		$processing_time = strtotime($processing_date);
		$processing_time = mktime(19,0,0,date('m',$processing_time),date('d',$processing_time),date('Y',$processing_time));
		return 	$processing_time;
	}
}

?>
