
<?php
/**
 * Created by IntelliJ IDEA.
 * User: Mohammed Asif Hussain
 * Date: Dec 22, 2009
 * Time: 10:28:57 AM
 * To change this template use File | Settings | File Templates.
 */

@include_once($xcart_dir."/include/class/search/searchUtil.php");

require_once("$xcart_dir/include/solr/solrProducts.php");
require_once("$xcart_dir/include/solr/solrStatsSearch.php");
require_once("$xcart_dir/include/solr/solrQueryBuilder.php");
require_once("$xcart_dir/include/solr/solrUtils.php");

require_once("$xcart_dir/include/class/widget/class.static.functions.widget.php");

@include ("$xcart_dir/include/func/func.filter_order.php");
include_once($xcart_dir."/include/class/class.feature_gate.keyvaluepair.php");
include_once($xcart_dir."/include/class/class.Search_promotion_key.php");
require_once("$xcart_dir/Cache/CacheQueue.php");
include_once($xcart_dir."/utils/solr/class.SolrQueryUtils.php");
include_once($xcart_dir."/utils/request/class.RequestUtils.php");
include_once($xcart_dir."/Profiler/Profiler.php");

class SearchProducts {

	var $search_phrase;

	var $product_types;

	var $is_design = 0;

	var $synonyms = array();

	var $filtered_phrase;
	
	var $canonical_search_phrase;

	var $sort_param;
	var $sortField;

	var $offset;

	var $limit;

	var $products;

	var $optimProducts;

	var $products_count = 0;

	var $faceted_search_results;
	var $faceted_search_results_new;

	var $highlighted;

	var $recently_added;

	var $most_popular;

	var $related_tags;

	var $related_designs;

	var $meta_keywords;

	var $meta_description;

	var $page_title;

	var $page_h1;

	var $pagination;

	var $stats_search_entry;

	var $description;

	var $top_banner;

	var $key_id;

	var $range_key_id;

	var $solr_product;

	var $solr_stats_search;

	var $abuse_filter;

	var $collected_synonyms;

	var $product_type;

	var $ignore_words;

	var $is_filthy = NULL;

	var $filter = NULL;
	var $filter_url = NULL;

	var $narrow_by = NULL;

	var $fit_filter = NULL;

	var $brand_filter = NULL;

	var $max_price;

	var $price_range = NULL;

	var $original_phrase = NULL;

	var $priority;

	var $priority_booster;

	var $filter_group_map;

	var $final_query;
	var $boosted_query;
	var $curatedSearchQuery;

	var $exclusion_phrase;

	var $phonetic_search_fields;

	var $sort_weightages;

	var $page_promotion;
	var $price_range_details;
	var $discount_percentage_details;
	var $initLatency = 0;
	var $query_level;
	var $is_parameteric_search;
	var $catalogue_typename_map;
	var $offsetCoefficient = 0;

	private $group_boost_map = array ( 'brands' => '3',
										'gender' => '8',
										'catalogue' => '11',
										'categories' => '7',
									);
	private $gender_list = array ('women', 'men', 'kids', 'girls', 'boys', 'girl', 'boy', 'man', 'woman');

	private $thresholdBoostArray = array("lowvisible"=>4,"new"=>1,"replenished"=>1);


	/***
	const QUERY_LEVEL_ARR = array ('NORM' => 'Normal Search',
									'PHONETIC' => 'Phonetic Search',
									'FULL' => 'Full Text Search',
									'NORES' => 'No Result'
									);
	***/

	private static $instance = NULL;

    /**
     * @return SearchProducts The singleton instance.
     */
    public static function getInstance($getStatsInstance=true)
    {
        if (SearchProducts::$instance == NULL) {
            SearchProducts::$instance = new SearchProducts(null,null,$getStatsInstance);
        }

        return SearchProducts::$instance;
    }


	public function __construct($query_phrase = null, $exclusion_phrase = null,$getStatsInstance=true) {
			$startTime = microtime(true);
			$this->initialize($query_phrase, $exclusion_phrase,$getStatsInstance);
			$this->initLatency = microtime(true) - $startTime;
	}

	/**
	 * @return void
	 */
	private function initialize($query_phrase = null, $exclusion_phrase = null,$getStatsInstance=true) {

		if(!empty($query_phrase))
		{
			$this->original_phrase = $query_phrase;
			$this->search_phrase = $query_phrase;
		}
		else
		{
			$this->original_phrase = get_var('query_decoded');
			$this->search_phrase = get_var('query_decoded');
		}

		if(!empty($exclusion_phrase))
		{
			$this->exclusion_phrase = $exclusion_phrase;
		}

		$this->filter = null;

		$this->narrow_by = get_var('narrow_by');

		$this->fit_filter = get_var('fit_filter');

		$this->brand_filter = get_var('brand_filter');

		$this->price_range = get_var('price_range');
		$this->abuse_filter = new AbuseFilter();

		$this->prepare_phrase();

		global $saleClearanceURL, $searchPageSortParam, $saleClearanceSortParam, $searchPageDefaultNoOfItems;

		$this->sort_param = $this->getSortParam($saleClearanceURL, $searchPageSortParam, $saleClearanceSortParam, $this->original_phrase);
		
		$this->limit = ($searchPageDefaultNoOfItems > 0)?$searchPageDefaultNoOfItems:24;
		$this->offset = $this->getPage() - 1;
		global $xcache;
		$cached_array_data = $xcache->fetchAndStore(function(){

			$cached_array_data=array();

			$priority_cache=array();
			$priority_booster_cache=array();
			$phonetic_search_fields_cache=array();

			$priority_res=func_query("select * from mk_solr_sprod_priority where is_active=1");
			if(!empty($priority_res[0]))
			{
				foreach($priority_res as $p)
				{
					$priority_cache[]=$p['name'];
					$priority_booster_cache[$p['name']]=$p['priority'];
					$phonetic_search_fields_cache[$p['name']] = $p['do_phonetic_search'];
					//$this->priority[]=$p['name'];
					//$this->priority_booster[$p['name']]=$p['priority'];
					//$this->phonetic_search_fields[$p['name']] = $p['do_phonetic_search'];
				}
			}

			$cached_array_data['priority_cache'] = $priority_cache;
			$cached_array_data['priority_booster_cache'] = $priority_booster_cache;
			$cached_array_data['phonetic_search_fields_cache'] = $phonetic_search_fields_cache;
			//$this->priority=$priority_cache;
			//$this->priority_booster=$priority_booster_cache;
			//$this->phonetic_search_fields=$phonetic_search_fields_cache;


			$filter_group_result=func_query("select f.filter_name as filter_name, g.group_name as group_name from mk_filters f, mk_filter_group g where f.filter_group_id=g.id and g.is_active='1';");
			$filter_group_map_cache = array();

			if(!empty($filter_group_result))
			{
				foreach($filter_group_result as $p)
				{  //everything as lower case so that it becomes case insenstive
					$filter_group_map_cache[strtolower($p['filter_name'])]=strtolower($p['group_name']);
					//$this->filter_group_map[strtolower($p['filter_name'])]=strtolower($p['group_name']);

				}
			}

			$cached_array_data['filter_group_map_cache'] = $filter_group_map_cache;
			//$this->filter_group_map = $filter_group_map_cache;

			$cat_typename_result=func_query("select id, typename from mk_catalogue_classification where is_active='1' order by parent1 desc;");
			$catalogue_typename_map_cache = array();

			foreach($cat_typename_result as $p)
			{  //everything as lower case so that it becomes case insenstive
				$catalogue_typename_map_cache[$p['id']]=strtolower($p['typename']);
				//$this->catalogue_typename_map[$p['id']]=strtolower($p['typename']);
			}
			$cached_array_data['catalogue_typename_map_cache'] = $catalogue_typename_map_cache;
			//$this->catalogue_typename_map = $catalogue_typename_map_cache;

			$user_interest_result=func_query("select action, weightage from mk_transients_config");
			$sort_weightages_cache = array();

			if(!empty($user_interest_result))
			{
				foreach($user_interest_result as $u)
				{
					$sort_weightages_cache[$u['action']]=$u['weightage'];
					//$this->sort_weightages[$u['action']]=$u['weightage'];
				}
			}
			$cached_array_data['sort_weightages_cache']=$sort_weightages_cache;
			//$this->sort_weightages = $sort_weightages_cache;
			return $cached_array_data;
		}, array(), "searchProductinitialize:cached_array_data",1800);

		$this->priority=$cached_array_data['priority_cache'];
		$this->priority_booster=$cached_array_data['priority_booster_cache'];
		$this->phonetic_search_fields=$cached_array_data['phonetic_search_fields_cache'];
		$this->filter_group_map = $cached_array_data['filter_group_map_cache'];
		$this->catalogue_typename_map = $cached_array_data['catalogue_typename_map_cache'];
		$this->sort_weightages = $cached_array_data['sort_weightages_cache'];
		
		$this->canonical_search_phrase	=	$this->getCanonicalSearchPhrase();
		
		try {
            if($getStatsInstance)
			    $this->solr_stats_search = new solrStatsSearch();
			$this->solr_product = new solrProducts();
		} catch (Exception $ex) {
			global $errorlog;
			$errorlog->error("SearchProducts :: exception while creating solr instance "  . $ex->getMessage());
			//print_r($ex);
		}
		
		
		
	}

	function setOffsetCoefficient($offsetCoefficient){
		$this->offsetCoefficient = $offsetCoefficient;
	}

	function getSortParam($saleClearanceURL, $searchPageSortParam, $saleClearanceSortParam, $query){
		$default_sort_param=get_var('sortby', '');

		if(empty($default_sort_param) && strstr($query, $saleClearanceURL)){
			$default_sort_param=strtoupper($saleClearanceSortParam);
		}
		if(empty($default_sort_param) && !empty($searchPageSortParam)){
			$default_sort_param=strtoupper($searchPageSortParam);
		}
		return $default_sort_param;
	}

	function setSearchPhrase($search_phrase)
	{
		$this->search_phrase = $search_phrase;
		$this->original_phrase = $search_phrase;
	}

	function setExclusionPhrase($exclusion_phrase)
	{
		$this->exclusion_phrase = $exclusion_phrase;
	}

	/**
	 * @return void
	 */
	function prepare_phrase() {
		# Apply all our cleaning parameters to original search term ..
		$this->do_clean_phrase();

		# Find if user has entered any known product types ..
		$this->do_prepare_phrase();
	}

	/**
	 * @return string
	 */
	function get_category_url() {
		$queryBuilder = new solrQueryBuilder();
		$queryBuilder->add_term($this->search_phrase, array('keyword'), NULL, true);
		$query = $queryBuilder->get_query();

		//echo $query;

		if (!empty($query)) {
			$solrIndex = new solrCategorySearch();
			$results = $solrIndex->searchIndex($query, 0, 1);

			$result = documents_to_array($results->docs);

			if (!empty($result)) {
				if (count($result) > 0) {
					return $http_location . "/" . $result[0]['url'];
				}
			}
		}
		return NULL;
	}


	/**
	 * @return #Ffunc_query|?
	 */
	function get_product_synonyms() {
		$sql = "select * from mk_product_type_synonyms ";
		return func_query($sql);
	}

	function get_tshirt_synonyms() {
		$sql = "select * from mk_product_type_synonyms where product_type_id=2";
		return func_query($sql);

	}

	/**
	 * @return void
	 */
	function do_prepare_phrase() {
		$search_phrase = $this->search_phrase;
		$filtered_phrase = $this->search_phrase;
		$this->filtered_phrase = trim(hyphen_to_space(preg_replace('#([ ])+#', ' ', $filtered_phrase)));
		//$this->search_phrase = trim($this->filtered_phrase . ' ' . implode(' ', $this->collected_synonyms));
		$this->search_phrase = trim($this->filtered_phrase);
		$this->filtered_phrase = $this->remove_words($this->filtered_phrase);
	}

	function remove_words($input) {
		$ignoreWords = array('size');
		if (stristr($input, " ") == TRUE) {
			$input = preg_replace('/\b(' . implode('|', $ignoreWords) . ')\b( |^ |)/', '', $input);
		}
		return trim($input);
	}


	/**
	 * We filter all the common words and special characters
	 * @return void
	 */
	function do_clean_phrase() {
		//Removing Unwanted Characters with known values
		/*
		 * strip apostrophe and percentage
		 * Others are replaced with space
		 */


	//	echo" improved<pre>";print_r(	$this->search_phrase);exit;
		$this->search_phrase = preg_replace('/[^a-zA-Z0-9%\#\'\/s-]/', ' ', $this->search_phrase);
		$this->search_phrase = preg_replace('#([ ])+#', ' ', $this->search_phrase);
		$this->search_phrase = $this->abuse_filter->filter($this->search_phrase);

		if($this->search_phrase == 'all'){
			$this->search_phrase = '';
		}
	}

	function execute_new() {
		$this->do_search_new();
		$this->execute_post_process();
	}

	function execute_parametrized() {
		$this->do_search_parametrized();
		$this->execute_post_process();
	}
	
	/**
	 * Post processing after execute
	 */
	function execute_post_process() {
		$this->do_meta_tags();
		$this->do_pagination();
	}


	/** function for getting results and query
	 **/

	function func_get_query($filtered_word, $condition, $sort_field, $priority = ''){
		$queryBuilder = new solrQueryBuilder();
		if (!empty($this->filter)) {
			//this is temp fix if user searches for men then unisex shud also come in search results
			if ($this->filter == 'Men' || $this->filter == 'Women')
			{
				$text=$this->filter." Unisex";
				$queryBuilder->add_filter("filters", $text);
			}
			else
			{
				//			$queryBuildier->add_filter("filters", $this->filter);
				$queryBuilder->add_phrase($this->filter, array("filters"));
			}

			$queryBuilder->add_filter("generic_type", "style");
			if(!empty($this->narrow_by))
			{
				$pos = strpos($this->narrow_by,",");
				if ($pos === false) {
					//this is temp fix if user searches for men then unisex shud also come in search results
					if ($this->narrow_by == 'Men' || $this->narrow_by == 'Women')
					{
						$text=$this->narrow_by." Unisex";
						$queryBuilder->add_filter("filters", $text);
					}
					else
					{
						$queryBuilder->add_filter("filters", $this->narrow_by);
					}
				}
				else
				{
					$narrow_by_arr = explode(",", $this->narrow_by);
					//this is temp fix if user searches for men then unisex shud also come in search results
					if (in_array("Men", $narrow_by_arr) || in_array("Women", $narrow_by_arr))
					{
						foreach ($narrow_by_arr as $narrow)
						{
							if($narrow == 'Men' || $narrow == 'Women' )
							{
								$text=$narrow." Unisex";
								$queryBuilder->add_filter("filters", $text);
							}
							else
							$queryBuilder->add_filter("filters", $narrow);
						}
					}
					else
					{
						foreach ($narrow_by_arr as $narrow)
						{
							$queryBuilder->add_filter("filters", $narrow);
						}
					}
				}

			}


		}
		else {
			$queryBuilder->add_filter('shopmasterid', 0);
			$queryBuilder->add_filter('statusid', 1008);
			$queryBuilder->add_filter('typeid', 2);
		}
		if (!empty($priority) && is_array($priority)) {
			$queryBuilder->add_term($filtered_word, $priority, array('product' => 5), $condition);
		}
		else
		{
			//$queryBuilder->add_term( $this->filtered_phrase , array('keywords', 'stylename', 'product', 'descr', 'fulldescr' ), array('product' => 5),true);
			$queryBuilder->add_term($filtered_word, array('keywords', 'stylename', 'product', 'descr', 'fulldescr'), array('product' => 5), $condition);
		}


		/*foreach ($this->product_types as $product) {
		 $queryBuilder->add_filter('typeid', $product['product_type_id']);
		 }*/

		$this->final_query = $queryBuilder->get_query();
		try {

			$search_results = $this->solr_product->searchAndSort($query, ($this->offset * $this->limit), $this->limit, $sort_field);
			$this->products = documents_to_array($search_results->docs);
			foreach ($this->products as $index => $product) {
				$truncated_name = truncate($product['product']);
				$product['product_name_truncated'] = $truncated_name;
				$this->products[$index] = $product;

			}
			$this->products_count = $search_results->numFound;
		} catch (Exception $ex) {
			global $errorlog;
			$errorlog->error("SearchProducts :: "  . $ex->getMessage());
			//print_r($ex);
		}

	}
	/**
	 function for
	 ignore keywords

	 **/
	function func_ignore_keywords($keywords, $ignore_keywords) {
		//remove s1 and s1 from filtered_phrase
		//and copy into ignore keywords
		//$this->ignore_words

		$arr_ignore_words = array("india", "bangalore", "bengaluru", "new delhi", "delhi", "mumbai", "bombay", "hyderabad", "pune", "chennai", "kolkata", "calcutta",
            "buy", "online", "gifts", "print", "printing", "photo", "design", "designs", "personalized", "customized", "custom made", "make", "your", "own", "personalize", "personalise", "customise", "customize", "personalised", "customised", "myntra", "merchandise", "merchandize");
		$arr_words = explode(" ", strtolower($keywords));
		$list_keywords = strtolower($keywords);
		foreach ($arr_ignore_words as $v) {
			foreach ($arr_words as $k) {
				if (strcmp($k, $v) == 0) {
					$list_ignorewords = $list_ignorewords . " " . $k;
					$list_keywords = str_replace($k, "", $list_keywords);
				}
			}
		}
		$this->ignore_words = trim($list_ignorewords);
		return trim($list_keywords);
	}

	/**
	 * Faceting for the search phrase ..
	 * @param  $search_phrase
	 * @return array
	 */
	function do_faceted_search($priority = '', $condition = false) {
		// No need to search for filthy stuff ..
		if ($this->is_filthy()) {
			return;
		}
		$queryBuilder = new solrQueryBuilder();

		$queryBuilder->create_query();
		if (!empty($this->filtered_phrase))
		{
			if ($this->filtered_phrase == 'Men' || $this->filtered_phrase == 'Women')
			{
				$text=$this->filtered_phrase." Unisex";
				$queryBuilder->add_filter("filters", $text);
			}
			else
			{
				$queryBuilder->add_filter("filters", $this->filtered_phrase);
			}
		}

		if (!empty($this->narrow_by)) {
			$queryBuilder->add_filter("filters", $this->narrow_by);
		}
		if (!empty($this->fit_filter) && $this->fit_filter != 'ALL')
		{
			if ($this->fit_filter == 'Men' || $this->fit_filter == 'Women')
			{
				$text=$this->fit_filter." Unisex";
				$queryBuilder->add_filter("filters", $text);
			}
			else
			{
				$queryBuilder->add_filter("filters", $this->fit_filter);
			}
		}
		if (!empty($this->brand_filter) && $this->brand_filter != 'ALL') {
			$queryBuilder->add_filter("filters", $this->brand_filter);
		}
		if (!empty($this->price_range) && $this->price_range != 'ALL') {
			$range = str_replace("TO", " TO ", $this->price_range);
			$queryBuilder->add_filter("price", "[" . $range . "]");
		}
		if (!empty($this->product_types)) {
			foreach ($this->product_types as $product) {
				$queryBuilder->add_filter('typeid', $product['product_type_id']);
			}
		}
		$queryBuilder->add_filter("search_type", "noncustomizable");
		$queryBuilder->end_query();


		if (!((!empty($this->fit_filter) && $this->fit_filter != 'ALL') || (!empty($this->brand_filter) && $this->brand_filter != 'ALL'))) {
			$queryBuilder->set_clause("OR");
			$queryBuilder->create_query();
			$queryBuilder->add_filter('shopmasterid', 0);
			$queryBuilder->add_filter('statusid', 1008);
			$queryBuilder->add_filter('typeid', 2);
			if (!empty($this->price_range) && $this->price_range != 'ALL') {
				$range = str_replace("TO", " TO ", $this->price_range);
				$queryBuilder->add_filter("price", "[" . $range . "]");
			}
			if (!empty($priority) && is_array($priority)) {
				$queryBuilder->add_term($this->filtered_phrase, $priority, array('filters' => 10, 'stylename' => 9, 'product' => 5), $condition);
			}
			else
			{
				$queryBuilder->add_term($this->filtered_phrase, array('keywords', 'stylename', 'product', 'descr', 'fulldescr'), array('product' => 5), $condition);
			}
			$queryBuilder->end_query();
		}
		else if (!empty($this->brand_filter) && $this->brand_filter == 'myntra') {
			$queryBuilder->set_clause("OR");
			$queryBuilder->create_query();
			$queryBuilder->add_filter('shopmasterid', 0);
			$queryBuilder->add_filter('statusid', 1008);
			$queryBuilder->add_filter('typeid', 2);
			if (!empty($this->price_range) && $this->price_range != 'ALL') {
				$range = str_replace("TO", " TO ", $this->price_range);
				$queryBuilder->add_filter("price", "[" . $range . "]");
			}
			if (!empty($priority) && is_array($priority)) {
				$queryBuilder->add_term($this->filtered_phrase, $priority, array('filters' => 10, 'stylename' => 9, 'product' => 5), $condition);
			}
			else
			{
				$queryBuilder->add_term($this->filtered_phrase, array('keywords', 'stylename', 'product', 'descr', 'fulldescr'), array('product' => 5), $condition);
			}
			$queryBuilder->add_filter('filters', 'myntra');
			$queryBuilder->end_query();

		}
		//      echo "facet".$queryBuilder->query; echo "<br><br>";
		$query = $queryBuilder->query;
		try {

			$result = $this->solr_product->facetedSearch($query, "filters_facet");
			$term_freq = array();

			foreach ($result->filters_facet as $key => $value) {
				$term_freq[$key] = $value;
			}
			//print_r($term_freq);
			$this->faceted_search_results = $term_freq;
		} catch (Exception $ex) {
			global $errorlog;
			$errorlog->error("SearchProducts :: exception while doing faceted search "  . $ex->getMessage());
			//print_r($ex);
		}
	}

	/**
	 * @return void
	 */
	function get_related_designs() {
		$queryBuilder = new solrQueryBuilder();
		$queryBuilder->add_term($this->filtered_phrase, array('orignal_keyword'), NULL, true);
		$queryBuilder->add_filter('is_valid', '1');
		$query = $queryBuilder->get_query();

		//echo $query;

		try {

			$search_results = $this->solr_stats_search->searchAndSort($query, 0, 10, 'count_of_products desc');
			$related_designs = documents_to_array($search_results->docs);

			$signatures = array();
			foreach ($related_designs as $key => $rd) {
				if (!isset($signatures[$rd['signature']])) {
					$signatures[$rd['signature']] = TRUE;
				} else {
					unset($related_designs[$key]);
				}
			}

			$this->related_designs = $related_designs;

		} catch (Exception $ex) {
			global $errorlog;
			$errorlog->error("SearchProducts :: exception while getting related designs "  . $ex->getMessage());
			//print_r($ex);
		}
	}

	function getRelatedDesignsByTags($keyword, $start = 0, $limit = 10) {
		return Array();
		$queryBuilder = new solrQueryBuilder();
		//$queryBuilder->add_term($keyword, array('keywords'), NULL, true);
		$queryBuilder->add_term($keyword, array('keywords', 'stylename', 'product', 'descr', 'fulldescr'), array('product' => 5), "myntrarating desc");
		$queryBuilder->add_filter('statusid', '1008');
		$queryBuilder->add_filter('shopmasterid', 0);
		$queryBuilder->add_filter('typeid', 2);


		$query = $queryBuilder->get_query();
		$related_designs = array();
		//        echo $query;

		try {

			$search_results = $this->solr_product->searchAndSort($query, $start, $limit, "myntrarating desc");
			$results = documents_to_array($search_results->docs);
			foreach ($results as $index => $product) {
				$truncated_name = truncate($product['product']);
				$product['product_name_truncated'] = $truncated_name;
				$related_designs[$index] = $product;

			}

			return $related_designs;

		} catch (Exception $ex) {
			global $errorlog;
			$errorlog->error("SearchProducts :: exception while getting related designs "  . $ex->getMessage());
			//print_r($ex);
		}
	}

	function getRelatedProductsByTags($keyword, $priority = '', $start = 0, $limit = 10, $search_type='noncustomizable', $shopmasterid = 0) {
		$queryBuilder = new solrQueryBuilder();
		//$queryBuilder->add_term($keyword, array('keywords'), NULL, true);

		if(empty($priority)) {
			// If no priorities are defined, then we use the global prioirty setting defined in mk_solr_sprod_priority table.
			$priority = $this->priority;
		}

		$queryBuilder->add_term($keyword, $priority, $this->priority_booster, "myntrarating desc");
		if(!empty($search_type)) {
			$queryBuilder->add_filter('search_type', $search_type);
		}

		if(empty($shopmasterid)) {
			$queryBuilder->add_filter('shopmasterid', 0);
		} else if($shopmasterid != 'empty') {
			$queryBuilder->add_filter('shopmasterid', $shopmasterid);
		}

		$query = $queryBuilder->get_query();
		$related_designs = array();

		try {

			$search_results = $this->solr_product->searchAndSort($query, $start, $limit, "myntrarating desc");
			$results = documents_to_array($search_results->docs);
			foreach ($results as $index => $product) {
				$truncated_name = truncate($product['product']);
				$product['product_name_truncated'] = $truncated_name;
				$related_products[$index] = $product;

			}
			return $related_products;

		} catch (Exception $ex) {
			global $errorlog;
			$errorlog->error("SearchProducts :: exception while getting related product by tags "  . $ex->getMessage());
			//print_r($ex);
		}
	}
	/**
	 * @return void
	 */
	function do_highlight() {
		$highlighted = array();
		foreach ($this->faceted_search_results as $type => $count) {
			$search_phrase = trim(strtolower($type));
			foreach ($this->product_types as $key => $value) {
				$types = split(',', strtolower($value['synonyms']));
				$types = trim_all($types);
				if (in_array($search_phrase, $types)) {
					$highlighted[$type] = TRUE;
				}
			}
		}
		$this->highlighted = $highlighted;
	}

	/**
	 * @return void
	 */
	function get_recently_added_search() {
		if ($this->key_id > 2000)
		$this->range_key_id = $this->key_id - 1000;
		else
		$this->range_key_id = 1;

		$results = func_query("SELECT keyword,orignal_keyword,key_id from mk_stats_search where count_of_products > 3 and  key_id < '" . $this->key_id . "' and key_id > '" . $this->range_key_id . "' and is_valid='Y' and length(orignal_keyword)>4 and length(orignal_keyword) <20 order by key_id desc limit 10");

		$this->recently_added = $this->abuse_filter->filter_array($results, 'orignal_keyword');
	}

	/**
	 * @return void
	 */
	function get_most_popular_search() {
		$sql = "SELECT keyword, orignal_keyword, key_id from mk_search_popular_kw order by  count_of_searches desc limit 0,10 ";
		$result = func_query($sql);
		foreach ($result as $key => $value) {
			$result[$key]['filter_keyword'] = $this->abuse_filter->filter($value['orignal_keyword']);
		}
		$this->most_popular = $result;
	}

	/**
	 * @return void
	 */
	function get_related_tags() {
		$url = "http://www.myntra.com/" . str_replace(" ", "-", $this->filtered_phrase) . "/search/";
		$this->related_tags = memcache_get_object($url);
		if ($this->related_tags == null) {
			$this->related_tags = func_relatedtags($url, "Buy " . $this->filtered_phrase . " online in India");
			memcache_set_object($url, $this->related_tags, 7 * 24 * 3600);
		}
	}

	/**
	 * @return void
	 */
	function do_meta_tags() {
		$meta_keyword = $this->search_phrase;
		$meta_keyword_case = ucfirst($meta_keyword);
		$meta_original_keyword = $this->filtered_phrase;

		if ($meta_keyword != $meta_original_keyword) {
			if(empty($this->meta_keywords) || trim($this->meta_keywords) =='')
			$this->meta_keywords = "$meta_keyword_case,$meta_keyword_case for men, $meta_keyword_case for women, $meta_keyword_case for girls, $meta_keyword_case for boys, cool $meta_keyword_case,buy $meta_keyword_case,buy $meta_keyword_case india";
			if(empty($this->meta_description) || trim($this->meta_description) =='')
			$this->meta_description = $meta_keyword_case . ": Choose from " . $this->products_count . " designs for " . $meta_keyword_case . ".Starting Rs " . $this->get_price() . ". All India FREE Shipping. Ships internationally within 3 days.";
			if(empty($this->page_title) || trim($this->page_title) =='')
			$this->page_title = $meta_keyword_case . " | Buy " . $meta_keyword_case . " Online in India";
		} else {
			if(empty($this->meta_keywords) || trim($this->meta_keywords) =='')
			$this->meta_keywords = "$meta_keyword_case,$meta_original_keyword in India, buy $meta_original_keyword online in india, online store for $meta_original_keyword, Shop online for $meta_original_keyword, online shopping for $meta_original_keyword, Online shopping for $meta_original_keyword in India, $meta_original_keyword reviews and prices in India";
			if(empty($this->meta_description) || trim($this->meta_description) =='')
			$this->meta_description = "Choose from wide range of " . $meta_keyword_case . " to buy online. All India FREE Shipping. Cash on Delivery available.";
			if(empty($this->page_title) || trim($this->page_title) =='')
			$this->page_title = $meta_keyword_case . " | Buy " . $meta_keyword_case . " Online in India";
		}
	}

	/**
	 * @return void
	 */

	function get_price() {
		$arr_productids = '';
		foreach ($this->product_types as $product) {
			$arr_productids = $arr_productids . "," . $product['product_type_id'];
		}
		$arr_productids = trim($arr_productids, ",");
		if (!empty($arr_productids)) {
			$results = "select min(price) as product_price from mk_product_style where product_type in (" . $arr_productids . ") and is_active=1 and styletype='P' ";
			$row = db_fetch_array(db_query($results));
			return $row['product_price'];
		} else {
			return '';
		}

	}

	function do_pagination() {
		global $http_location;
		$base_url = $http_location . "/" . str_replace(" ","-",$this->search_phrase) .(!empty($this->sort_param) ? "/" . $this->sort_param : '') . "/";
		$this->pagination = paginate_search($this->limit, $this->getPage(), $this->products_count, $base_url, '');
	}

	function do_pagination_sportswear() {
		global $http_location;
		$base_url = $http_location . "/sportswear/" . space_to_hyphen($this->filter_url) . "?" . (!empty($this->sort_param) ? "sortby=" . $this->sort_param : '') . (!empty($this->price_range) ? "&price_range=" . $this->price_range : '') . (!empty($this->narrow_by) ? "&narrow_by=" . $this->narrow_by : '') . (!empty($this->brand_filter) ? "&brand_filter=" . $this->brand_filter : '') . (!empty($this->fit_filter) ? "&fit_filter=" . $this->fit_filter : '') . "&amp;page=";
		$this->pagination = paginate_search($this->limit, $this->getPage(), $this->products_count, $base_url, '');
	}


	/**
	 * @param  $filtered_search_phrase
	 * @param  $original_search_phrase
	 * @param  $total_count
	 * @return void
	 */
	function track_search_term() {
			global $xcache;
            if(!$this->is_filthy()) {
                try {
			$cache = new CacheQueue('queue:mk_stats_search');
			$data = array(
                            'keyword' => $this->filtered_phrase,
                            'orignal_keyword' => $this->search_phrase,
                            'date_added' => time(),
                            'last_search_date' => time(),
                            'count_of_products' => ($this->products_count == NULL ? 0 : $this->products_count),
                            'count_of_searches' => 1,
                            'is_valid' => 'Y'
                            );
            	    $cache_key_id = 'search_tracking';
                    $cache->rpush($cache_key_id, $data);

            $this->stats_search_entry = $xcache->fetchAndStore(function($filtered_phrase){
            	return func_select_first('mk_stats_search',
				array(
		        		'keyword' => $filtered_phrase
				));

            }, array($this->filtered_phrase), "filteredPhrase-".$this->filtered_phrase, 3600);

			if (!empty($this->stats_search_entry['description']))
				$this->description = $this->stats_search_entry['description'];
			if (!empty($this->stats_search_entry['top_banner']))
				$this->top_banner = $this->stats_search_entry['top_banner'];
			if(!empty($this->stats_search_entry['meta_keyword']))
				$this->meta_keywords = $this->stats_search_entry['meta_keyword'];
			if(!empty($this->stats_search_entry['meta_description']))
				$this->meta_description = $this->stats_search_entry['meta_description'];
			if(!empty($this->stats_search_entry['page_title']))
				$this->page_title = $this->stats_search_entry['page_title'];
			if(!empty($this->stats_search_entry['page_h1']))
				$this->page_h1 = $this->stats_search_entry['page_h1'];
			if(!empty($this->stats_search_entry['page_promotion']))
				$this->page_promotion = $this->stats_search_entry['page_promotion'];

                }
                catch (Exception $ex)
                {
                        global $errorlog;
                        $errorlog->error("SearchProducts :: exception while tracking search query to redis ".$ex->getMessage());
                }
            }
	}

	/**
	 * Kunal : Not used any more. To be deleted
	 * @return void
	 */
	/*function update_recent() {
		if (!$this->is_filthy()) {
			$query = 'call update_recent_popular_search("' . $this->key_id . '","' . $this->filtered_phrase . '","' . $this->search_phrase . '", unix_timestamp());';
			db_query($query);
		}
	}*/

	/**
	 * @return int
	 */
	function is_no_index() {
		return ($this->products_count == 0 || $this->stats_search_entry['is_valid'] == 'N' || $this->is_filthy()) ? 2 : 0;
	}

	/**
	 * @return
	 */
	function is_filthy() {
		if ($this->is_filthy == NULL) {
			$this->is_filthy = $this->abuse_filter->is_filthy($this->original_phrase);
		}
		return $this->is_filthy;
	}

	/**
	 * @return int
	 */
	function is_empty() {
		return $this->products_count == 0 ? 1 : 0;
	}

	/**
	 * @return void
	 */
	function get_suggestions() {
		if (!empty($this->product_types)) {
			$this->product_type = array_shift($this->product_types);
		}
	}

	/**
	 * @param  $style_ids
	 */
	function get_top_selling($style_ids) {

		$queryBuilder = new solrQueryBuilder();
		$queryBuilder->add_filter("generic_type", "style");
		$limit = 15;
		if(!empty($style_ids))
		$limit = sizeof($style_ids);
		foreach ($style_ids as $style_id) {
			$queryBuilder->add_or_filter("styleid", $style_id["product_style_id"]);
		}
		$query =  $queryBuilder->get_query();

		$response = $this->solr_product->searchAndSort($query, 0 , $limit);

		return documents_to_array($response->docs);
	}


	/*
	 * NEW search
	 */
	function do_search_new() {
		$this->do_search_parametrized();
	}

    /*
     * Initialize the search query
     */
    function initSearchQuery() {
        //check if the keyword is parametrized or landing page
        if (RequestUtils::getRequestType() == RequestType::$PARAMETRIZED || RequestUtils::getRequestType() == RequestType::$LANDING_PAGE) {
            $this->final_query = SolrQueryUtils::getSolrQueryForActiveStyle();
            $this->is_parameteric_search = 'y';
        } else {
            //this checks if there is any filthy text
            if ($this->is_filthy()) {
                return;
            }
            //this uses the full_text_myntra field of solr (check schema) to do the full text search
            $this->final_query = $this->fullTextSearch();
            
            //this is for adding the exclusion phrase in the final query
            if(!empty($this->exclusion_phrase)) {
                $this->final_query .= " - " . $this->func_get_query_new($this->exclusion_phrase, "true");
            }
        }
    }


	/*
	 * Parametrized search
	 */
	function do_search_parametrized() {
        //initialize the final_query
        $this->initSearchQuery();
        if ($this->is_filthy()) {
                return;
        }
		/*
		 * First Perform facet query
		 * Decide the sorting field
		 * Get results
		 * */
		$this->faceted_search_results_new = $this->performUnifiedFacetedSolrQuery();
		$sort_field = $this->GetGenderModifiedSolrField();
		$this->query_level = 'NORM';
		
		//perform the solr query: boosted search and normal part both and now the curated one
		$this->performSolrQuery($sort_field);
		$this->DoAlternateSearch();
	}

    /*
     * get the facet fields and ranges for facet query
     */
    function getFacetQueryParams() {
        //facet fields for filter
        $facetFields = array(
                        "global_attr_master_category_facet",
                        "global_attr_sub_category_facet",
                        "brands_filter_facet",
                        "global_attr_gender",
                        "global_attr_article_type_facet",
                        "sizeGroup",
                        "typename",
                        "colour_family_list",
                        "sizes_facet",
                        "discounted_price",
                        "categories");

        //facet ranges for filter
        $facetRanges = array();
        $facetRange["range_name"] = "discount_percentage";
        $facetRange["range_start"] = "10";
        $facetRange["range_end"] = "100";
        $facetRange["range_gap"] = "10";
        $facetRanges[] = $facetRange;

        //complete facet params
        $facetParams = array();
        $facetParams["field"] = $facetFields;
        $facetParams["range"] = $facetRanges;

        return $facetParams;
    }

    /*
     * get the curated search query
     */
    function initCuratedSearchQuery() {
        $this->curatedSearchQuery = array();
        $this->curatedSearchQuery['enable'] = false;
        //curated search is done in default search and can be done in parameteric search only
        if (($this->sort_param == "") && ($this->is_parameteric_search == 'y')) {
            //read this variable using feature gate
            $curatedSearchEnabled = FeatureGateKeyValuePairs::getBoolean("curatedSearch.enabled",false);

            //do the AB testing for curated search
            if ($curatedSearchEnabled) {
                global $_MABTestObject;
                $showCuratedSearch = $_MABTestObject->getUserSegment("CuratedSearch");
                if($showCuratedSearch == "control") {
                    $curatedSearchEnabled = false;
                }
            }
            
            $doCuratedSearch = false; //this if curated search is defined for current search
            //check if its a parameterized search or landing page url then go ahead with curated search
            if ($curatedSearchEnabled) {
                // this assumes that any call to this function is parameterised and its landing page is original search phrase
                // as parameterized key is not in use for search from UI
                $curatedData = SolrQueryUtils::getCuratedSearchData($this->original_phrase);
                if(!(is_null($curatedData) || empty($curatedData))) {
                    //curated search related initialization data
                    // no. of curated slots >= no. of styles in the curated query
                    $this->curatedSearchQuery['style'] = $this->getSolrQueryOfStyleCSV($curatedData['style_id_csv']);
                    $this->curatedSearchQuery['no_slots'] = $curatedData['no_slots'];
                    $this->curatedSearchQuery['sortfield'] = $this->getSortFieldBySortParam($curatedData['remaining_slots_rule']);
                    $this->curatedSearchQuery['sort'] = $this->getSortFieldAsSolrSortField($curatedData['remaining_slots_rule']);
                    $this->curatedSearchQuery['enable'] = true;
                }
            }
        }
    }
	
	function GetGenderModifiedSolrField() {
		$sort_field = $this->getSortField();
		try {
			$genderArray = array();
			$genderArray = $this->faceted_search_results_new->global_attr_gender;
			$genderArray = (array) $genderArray;
			if(empty($genderArray[men]))
			{
				$genderArray[men]=0;
			}
			if(empty($genderArray[boy]))
			{
				$genderArray[boy]=0;
			}
			if(empty($genderArray[women]))
			{
				$genderArray[women]=0;
			}
			if(empty($genderArray[girl]))
			{
				$genderArray[girl]=0;
			}
			$brandArray = $this->faceted_search_results_new->brands_filter_facet;
			$brandArray = (array) $brandArray;
			$styleCount= array_sum($brandArray);
			$numOfUnisexProduct = $genderArray[men]+$genderArray[boy] + $genderArray[women]+$genderArray[girl] - $styleCount;
			$searchComparator = FeatureGateKeyValuePairs::getFeatureGateValueForKey("RatioForGenderSpecificRevenue");
			if(empty($searchComparator))
			{
				$searchComparator =3;
			}
			$searchComparator = (int)$searchComparator;
			if(($genderArray[men]+$genderArray[boy] - $numOfUnisexProduct)>$searchComparator*($genderArray[women]+$genderArray[girl]-$numOfUnisexProduct))
			{
				$sort_field = $this->getSortField(revenue_male_sort_field,potential_revenue_male_sort_field);
			}
			if($searchComparator*($genderArray[men]+$genderArray[boy]-$numOfUnisexProduct)<($genderArray[women]+$genderArray[girl]-$numOfUnisexProduct))
			{
				$sort_field = $this->getSortField(revenue_female_sort_field,potential_revenue_female_sort_field);
			}
		}
		catch(Exception $e)
		{}
		return $sort_field;
	
	}

	/** function for getting results and query
	 **/

	function func_get_query_new($filtered_word, $condition, $is_phonetic_search_on = false) {
		$queryBuilder = new solrQueryBuilder();
		$queryBuilder->add_filter("statusid",'1008');

        $includeOOS = get_var('include_oos', 0);
        if (empty($includeOOS)) {
			$dontShowOOSProducts = FeatureGateKeyValuePairs::getFeatureGateValueForKey("dontShowOOSProducts");
			if((!empty($dontShowOOSProducts) && $dontShowOOSProducts == 'true'))
				$queryBuilder->add_filter("count_options_availbale","[1 TO *]");
        }

		$sizeArray = Array();

		if (!empty($this->priority) && is_array($this->priority))
		{
			$search_query=explode(" ",$filtered_word);
			$search_query = array_unique($search_query);
			$queryword_map = Array();
			$generalWords = Array(); //these will be words which don't belong to any filter
			foreach($search_query as $query_word){

				//special treatement to size
				if(strpos($this->filter_group_map[strtolower($query_word)] , "size")  === false)
				{
					//find out which of these are filters in our application
					//add_fitler()
					//also find out which belogn to same filter_group
					//sort queryies based on filter_group if last filter_group === this filter_group OR otherwise AND
					//storing everything as lower case
					if(array_key_exists(strtolower($query_word), $this->filter_group_map))
					{
						$queryword_map[strtolower($query_word)] = $this->filter_group_map[strtolower($query_word)];
					}
					else
					{
						array_push($generalWords, strtolower($query_word));
					}
				}
				else
				{
					array_push($sizeArray, $query_word);
				}
			}

			arsort($queryword_map);

			$last_filter_group = '';
			$opened_braces = false;
			foreach($queryword_map as $queryword=>$querygroup)
			{
				if($queryword =='')continue;
				if(!empty($last_filter_group) && $last_filter_group === $querygroup)
				{
					//need to do OR query
					$queryBuilder->append_to_query('OR');
					$queryBuilder->add_term($queryword, $this->priority, $this->priority_booster, $condition, 'OR', false);
					$unclosed_brace = true;
				}
				else //need to do AND query
				{
					if($unclosed_brace)
					$queryBuilder->append_to_query(') AND (');
					else
					$queryBuilder->append_to_query('(');

					$queryBuilder->add_term($queryword, $this->priority, $this->priority_booster, $condition, 'OR', false);
					$unclosed_brace = true;
				}
				$last_filter_group = trim($querygroup);
			}
			if($unclosed_brace)
			$queryBuilder->append_to_query(')');

			if(true && !empty($generalWords))
			{
				$generalWordsExplodedSet = groupify($generalWords);
				//print_r($generalWordsExplodedSet);
				//if(!empty($queryBuilder->current_query))
				//$queryBuilder->append_to_query(' AND ');
				$queryBuilder->append_to_query('+(');
				$addOR = false;
				foreach ($generalWordsExplodedSet as $generalWordsSet)
				{
					if($addOR)
					$queryBuilder->append_to_query('OR');
					$queryBuilder->append_to_query(' ( ');
					foreach($generalWordsSet as $generalWordsGrouped)
					{
						$queryBuilder->add_term($generalWordsGrouped, $this->priority, $this->priority_booster, false, 'OR', true, true,$is_phonetic_search_on,$this->phonetic_search_fields);
					}
					$addOR = true;
					$queryBuilder->append_to_query(' ) ');
				}
				$queryBuilder->append_to_query(' ) ');
			}

			if(!empty($sizeArray))
			$queryBuilder->add_specific_terms($sizeArray, "sizes", "9"); //shipping priority has been hard coded

			$queryBuilder->end_query();
		}
		else
		{
			$queryBuilder->add_term($filtered_word, array('keywords', 'stylename', 'product', 'descr', 'fulldescr'), array('product' => 5), true);
		}

		return $queryBuilder->get_query();


	}

	public function performSolrQuery($sort_field, $boostedQuery = "", $filteredQuery = "", $preformBoostQuery = true) {
		global $weblog;
		try {
			$normalQuery = $this->final_query;
			/**
			 * Curated Search:
			 * 1. Do only for POPULARITY sort
			 * 2. Get style solr query and no. of blocks and sorting rule from DB
			 * 3. Remove the curated styles from search
			 * 4. Sort the remaining blocks using curated sort rule
			 */
			//initialise no. of curated items in search results
			$curatedItems = array();
			if ($this->sort_param == "") {
				//initialize curated search
				$this->initCuratedSearchQuery();
				$doCuratedSearch = $this->curatedSearchQuery['enable'];
				if($doCuratedSearch) {
					$curatedQuery = $this->curatedSearchQuery['style'];
					$curatedSlotCount = $this->curatedSearchQuery['no_slots'];
					$curatedSortParamKey = $this->curatedSearchQuery['sortfield'];
					// no. of curated slots shown
					$noCuratedItemShown = min($this->offset * $this->limit, $curatedSlotCount);
					// no. of curated slots to be shown for current page
					$noCuratedItemReq = min($curatedSlotCount - $noCuratedItemShown, $this->limit);
					//get total no. of curated items which can be shown
					$noCuratedItemRemaining = max(0, $curatedSlotCount - ($pageNo * $pageLimit));
					
					//get curated items for current page if it can be shown
					if ($noCuratedItemReq > 0) {
						//get curated items
						$curatedItems = $this->performCuratedSearchOperation ($normalQuery, $curatedQuery, $curatedSlotCount, $this->offset, $this->limit, $curatedSortParamKey);
					}
					
					//need to generate the curated items query to remove from the normal query
					$curatedItemsQuery = $this->getAllCuratedItemQuery($curatedQuery, $curatedSlotCount, $curatedSortParamKey);
					$totalCuratedItem = $this->getStyleCountFromStyleSolrQuery($curatedItemsQuery);
					if ($curatedItemsQuery != "") {
						$normalQuery = "(($normalQuery)  - ($curatedItemsQuery))";
					}
				}
			}
			
			//AB Test for Search Optimization
			global $_MABTestObject;
			$optimVariant= $_MABTestObject->getUserSegment("SearchOptim");
			if ($optimVariant == 'test') {
				$normalOffset = $this->offset * $this->limit;
				$search_results = $this->solr_product->searchAndSort($this->final_query, $normalOffset, $this->limit, $sort_field, array('fl'=>'*,score'));
				$this->products_count = $search_results->numFound;
				$this->products = documents_to_array($search_results->docs);
				if (!empty($curatedItems)) {
					$this->optimProducts = array_merge($curatedItems, $this->products);
					$this->products = $this->removeDuplicateStyles($this->optimProducts);
					$this->products = array_splice($this->products, 0, $this->limit);
				}
			} else {
				// if the count of curated items is equal to page limit then do not do the normal search
				$noNormalItemRequired = $this->limit - count($curatedItems);
				//do the normal search for remaining slots - style ids of curated search
				//when it reaches to normal search, it means that all the curated items have been shown
				if ($noNormalItemRequired > 0) {
					$normalOffset = $this->offset * $this->limit - $totalCuratedItem + count($curatedItems);
					$search_results = $this->solr_product->searchAndSort($normalQuery, $normalOffset, $this->limit, $sort_field, array('fl'=>'*,score'));
					$this->products_count = $search_results->numFound + $totalCuratedItem;
					$this->products = documents_to_array($search_results->docs);
					//merging the curated search items to the search result list
					if (!empty($curatedItems)) {
						$this->products = array_slice(array_merge($curatedItems, $this->products), 0, $this->limit);
					}
					
				} else {
						$search_results = $this->solr_product->searchAndSort($normalQuery, 0, 0, $sort_field, array('fl'=>'*,score'));
						$this->products_count = $search_results->numFound + $totalCuratedItem;
						$this->products = $curatedItems;
				}
			}
			
			$styleids = "  ";
			foreach($this->products as $product){
				$styleids .= $product['styleid'] . "  ";
			}
			global $weblog;
			$weblog->info("styleids ::: $styleids");

			$this->products = WidgetUtility::getArraySolrFormattedData($this->products);
			foreach ($this->products as $index => $product){
				$truncated_name = truncate($product['product']);
				$product['product_name_truncated'] = $truncated_name;
				$this->products[$index] = $product;
			}
		}
		catch (Exception $ex){
			global $errorlog;
			$errorlog->error("SearchProducts :: exception while executing query " . $normalQuery . " " . $ex->getMessage());
		}
	}
	
	/**
	 * 
	 * @param string $normalQuery : main search solr query
	 * @param string $curatedQuery : curated solr query
	 * @param int $curatedSlotCount : no of slots fixed for this curated search
	 * @param int $pageNo : current page no
	 * @param int $pageLimit :  max no. of search items per page 
	 * @param string $curatedSortParamKey : default sorting order for the remaining slots
	 */
 	public function performCuratedSearchOperation ($normalQuery, $curatedQuery, $curatedSlotCount, $pageNo, $pageLimit, $curatedSortParamKey) {
			
		// no. of curated slots shown
		$noCuratedItemShown = min($this->offset * $this->limit, $curatedSlotCount);
		// no. of curated slots to be shown for current page
		$noCuratedItemReq = min($curatedSlotCount - $noCuratedItemShown, $this->limit);
		//get total no. of curated items which can be shown
		$noCuratedItemRemaining = max(0, $curatedSlotCount - ($pageNo * $pageLimit));
		
		//get curated items for current page if it can be shown
		if ($noCuratedItemReq > 0) {
			//initialise array
			$curatedQueryItems = array();
			$curatedSortItems = array();
			
			/**
			 *  1st get curated items from curated solr style query
			 */
			$curatedQueryItems = $this->getCuratedQueryItem($normalQuery, $curatedQuery, $curatedSlotCount, $pageNo, $pageLimit, $noCuratedItemReq);
			$curatedQueryItemsCount = count($curatedQueryItems);
				
			/**
			 *  2nd if above count of items is less than required no. of items
			 *  then get remaining curated items using the curated sort rule
			 */
			$noCuratedItemReq = $noCuratedItemReq - $curatedQueryItemsCount;
			if ($noCuratedItemReq > 0) {
				$curatedSortItems = $this->getCuratedSortRuleItem($normalQuery, $curatedQuery, $curatedSlotCount, $pageNo, $pageLimit, $curatedSortParamKey, $noCuratedItemReq);
			}
			
			//merge all the curated items
			$curatedItems = array_merge($curatedQueryItems, $curatedSortItems);
		
		} else {
			$curatedItems = array();
		}
		
		return $curatedItems;
	}
	
	/*
	 * in curated search 1st the search by style id is done using the style id based solr query
	 * then for remaining slots given sort method is used in the remaining style ids
	 */
	//get all curated items for a page in its limit from curated style solr query
	public function getCuratedQueryItem($normalQuery, $curatedQuery, $curatedSlotCount, $pageNo, $pageLimit, $noCuratedItemReq) {
		if($curatedQuery == '') {
			return array();
		}
		//no. of curated items shown till now
		$noCuratedItemShown = $pageNo * $pageLimit;
		
		//get the curated items based on curated solr style query
		$curatedResults = $this->solr_product->searchAndSort("($curatedQuery) AND ($normalQuery)", $noCuratedItemShown, $noCuratedItemReq);
		$curatedItems = documents_to_array($curatedResults->docs);
		
		$curatedItems = $this->sortCuratedStyleItems($curatedItems, $curatedQuery);
		
		return $curatedItems;
	}
	
	//get curated slot items for a page in its limit using its curated sort rule exclude the curated style solr query items
	//it assumes that we have shown all the items of curated style solr query results
	public function getCuratedSortRuleItem($normalQuery, $curatedQuery, $curatedSlotCount, $pageNo, $pageLimit, $curatedSortParamKey, $noCuratedItemReq) {
		//total curated items shown
		$noCuratedItemShown = $pageNo * $pageLimit;

		//total no. of curated items from curated solr style query shown
		if($curatedQuery == '') {
			$curatedQueryItems =  array();
		} else {
			$curatedResults = $this->solr_product->searchAndSort("($curatedQuery) AND ($normalQuery)", 0, $curatedSlotCount);
			$curatedQueryItems = documents_to_array($curatedResults->docs);
		}
		$noCuratedQueryItemShown = count($curatedQueryItems);
		
		//no. of curated items from sort rule shown
		$noCuratedSortItemShown = max(0, $noCuratedItemShown - $noCuratedQueryItemShown);
		
		//get the curated items based on curated sort rule for this page
		if($curatedQuery == '') {
			$curatedResults = $this->solr_product->searchAndSort($normalQuery, $noCuratedSortItemShown, $noCuratedItemReq, $curatedSortParamKey);
		} else {
			$curatedResults = $this->solr_product->searchAndSort("($normalQuery) - ($curatedQuery)", $noCuratedSortItemShown, $noCuratedItemReq, $curatedSortParamKey);
		}
		$curatedItems = documents_to_array($curatedResults->docs);
		
		return $curatedItems;
	}
	
	/**
	 * sort curated items fethced using style solr query
	 * @param array $searchItems : list of search items
	 * @param string $styleSolrQuery  : solr query of the form styleid:( 0 OR STYLEID1 OR ... )
	 * @return array sorted search items
	 */
	public function sortCuratedStyleItems($searchItems, $styleSolrQuery) {
		if(!empty($searchItems)) {
			$styleIds = $this->getStyleIdFromStyleSolrQuery($styleSolrQuery);
			$styleIds = array_flip($styleIds);
			$sortedSearchItems = array();
			
			foreach ($searchItems as $item) {
				$sortedSearchItems[$styleIds[$item['styleid']]] = $item;
			}
			
			ksort($sortedSearchItems);
			$searchItems = $sortedSearchItems;
		}
		return $searchItems;
	}
	
	//get single query for list of all curated items
	public function getAllCuratedItemQuery($curatedQuery, $curatedSlotCount, $curatedSortParamKey) {
		
		//get all curated query items
		if($curatedQuery == '') {
			$curatedItems =  array();
		} else {
			$curatedResults = $this->solr_product->searchAndSort("($curatedQuery) AND ($this->final_query)", 0, $curatedSlotCount);
			$curatedItems = documents_to_array($curatedResults->docs);
		}
		$noCuratedQueryItemShown = count($curatedItems);
		
		//get all curated sort rull items
		if($curatedQuery == '') {
			$curatedResults = $this->solr_product->searchAndSort($this->final_query, 0, $curatedSlotCount - $noCuratedQueryItemShown, $curatedSortParamKey);
		} else {
			$curatedResults = $this->solr_product->searchAndSort("($this->final_query) - ($curatedQuery)", 0, $curatedSlotCount - $noCuratedQueryItemShown, $curatedSortParamKey);
		}
		$curatedItems = array_merge($curatedItems, documents_to_array($curatedResults->docs));
		
		$allCuratedItemQuery = $this->getStyleIdsQueryFromItemsArray($curatedItems);
		
		return $allCuratedItemQuery;
	}
	
	/**
	 * function to generate a solr query for style id using the input an array of styles
	 * @param an array of solr document converted to array $itemsArray
	 * @return solr query using the style id
	 */
	public function getStyleIdsQueryFromItemsArray($itemsArray) {
		$styleIdsQuery = "";
		if(!empty($itemsArray)) {
			$key = 'styleid';
			$styleIdArray = array_map(function($item) use ($key) {
				return $item[$key];
			}, $itemsArray);
			$styleIdList = implode(" OR ", $styleIdArray);
			$styleIdsQuery = "styleid:( 0 OR $styleIdList )";
		}
	
		return $styleIdsQuery;
	}
	
	/**
	 * function to get count of styles from a style id based solr query
	 * @param strin $styleSolrQuery : solr query of the form styleid:( 0 OR STYLEID1 OR ... )
	 */
	public function getStyleCountFromStyleSolrQuery($styleSolrQuery) {
		$styleId = explode(' OR ', $styleSolrQuery);
		return count($styleId) - 1;
	}
	
	/**
	 * function to get all styles from a style id based solr query
	 * @param strin $styleSolrQuery : solr query of the form styleid:( 0 OR STYLEID1 OR ... )
	 */
	public function getStyleIdFromStyleSolrQuery($styleSolrQuery) {
		$rawStyleIds = explode(' OR ', $styleSolrQuery);
		$styleIds = array();
		foreach ($rawStyleIds as $key=>$styleId) {
			$styleIds[$key] = preg_replace("/[^0-9]/", "", $styleId);
		}
		return $styleIds;
	}

	/**
	 * function to get solr query from csv style ids
	 * @param string $styleIdCSV : comma separated styles
	 * @return string
	 */
	public function getSolrQueryOfStyleCSV($styleIdCSV) {
		$rawStyleIds = explode(",", $styleIdCSV);
		$styleIds = array();
		foreach ($rawStyleIds as $key=>$styleId) {
			$styleIds[$key] = preg_replace("/[^0-9]/", "", $styleId);
		}
		$styleIdsQuery = "";
		if(!empty($styleIds)) {
			$styleIdList = implode(" OR ", $styleIds);
			$styleIdsQuery = "styleid:( 0 OR $styleIdList )";
		}
		
		return $styleIdsQuery;
	}

	public function performFacetedSolrQuery()
	{
		//get faceted results
		//get all the results those which are present should be true rest should be false $this->filter_group_map
		try
		{
			$term_freq = array();
			$b_res = $this->solr_product->facetedSearch($this->final_query, "brands_filter_facet");
			foreach ($b_res->brands_filter_facet as $key => $value)
			{
				$term_freq[strtolower($key)] = $value;
			}
			$b_res = $this->solr_product->facetedSearch($this->final_query, "teamsleagues_filter_facet");
			foreach ($b_res->teamsleagues_filter_facet as $key => $value)
			{
				$term_freq[$key] = $value;
			}
			$b_res = $this->solr_product->facetedSearch($this->final_query, "sports_filter_facet");
			foreach ($b_res->sports_filter_facet as $key => $value)
			{
				$term_freq[$key] = $value;
			}
			$b_res = $this->solr_product->facetedSearch($this->final_query, "fit_filter_facet");
			foreach ($b_res->fit_filter_facet as $key => $value)
			{
				$term_freq[$key] = $value;
			}
			$b_res = $this->solr_product->facetedSearch($this->final_query, "team_filter_facet");
			foreach ($b_res->team_filter_facet as $key => $value)
			{
				$term_freq[$key] = $value;
			}
			$b_res = $this->solr_product->facetedSearch($this->final_query, "themes_filter_facet");
			foreach ($b_res->themes_filter_facet as $key => $value)
			{
				$term_freq[$key] = $value;
			}
			$b_res = $this->solr_product->facetedSearch($this->final_query, "categories_filter_facet");
			foreach ($b_res->categories_filter_facet as $key => $value)
			{
				$term_freq[$key] = $value;
			}
			$b_res = $this->solr_product->facetedSearch($this->final_query, "sizes_facet");
			foreach ($b_res->sizes_facet as $key => $value)
			{
				$term_freq[$key] = $value;
			}
			$oldfacet=array_keys($term_freq);
			$facet = Array();
			foreach ($this->filter_group_map as $key=>$value)
			{
				$facet[$key]['group_name'] = $value;
				if(key_exists($key, $term_freq))
				$facet[$key]['is_present'] = true;
				else
				$facet[$key]['is_present'] = false;
				/*if(key_exists($key, $term_freq))
				 $facet[$key] = true;
				 else
				 $facet[$key] = false;*/
			}
			$this->faceted_search_results = $facet;
		} catch (Exception $ex) {
			global $errorlog;
			$errorlog->error("SearchProducts :: exception while doing faceted search "  . $ex->getMessage());
		}

	}

	public function getSingleStyle($styleId) {
		$queryBuilder = new solrQueryBuilder();
		$queryBuilder->add_filter("styleid", $styleId);
		$query =  $queryBuilder->get_query();
		$response = $this->solr_product->searchAndSort($query, 0 , 1);
		$arrayDocument = documents_to_array($response->docs);
		$singleData = $arrayDocument[0];
		return $singleData;
	}

	public function getStyleByID($styleId) {
		$queryBuilder = new solrQueryBuilder();
		$queryBuilder->add_filter("styleid", $styleId);
		$query =  $queryBuilder->get_query();
		$response = $this->solr_product->searchAndSort($query, 0 , 1);
		$this->products = documents_to_array($response->docs);
		$this->products = WidgetUtility::getArraySolrFormattedData($this->products);
		$singleData = $this->products[0];
		return $singleData;
	}

	public function getStyleByName($stylename) {
		$queryBuilder = new solrQueryBuilder();


		$queryBuilder = new solrQueryBuilder();
		$tags= explode(' ',$stylename);
		foreach($tags as $tag)
		{
			if(!empty($tag))
			{
				$queryBuilder->add_filter("stylename", '"'.$tag.'"');
			}
		}
		$query =  $queryBuilder->get_query();
		$response = $this->solr_product->searchAndSort($query, 0 , 1);
		$this->products = documents_to_array($response->docs);
		$this->products = WidgetUtility::getArraySolrFormattedData($this->products);
		$singleData = $this->products[0];
		return $singleData;
	}

	public function getSingleProductOrDesign($id) {
		$queryBuilder = new solrQueryBuilder();
		$queryBuilder->add_filter("id", $id);
		$query =  $queryBuilder->get_query();
		$response = $this->solr_product->searchAndSort($query, 0 , 1);
		$arrayDocument = documents_to_array($response->docs);
		$singleData = $arrayDocument[0];
		$singleData = WidgetUtility::getWidgetFormattedData($singleData);
		return $singleData;
	}

	public function getMultipleProductOrDesign($ids){
		if(!is_array($ids) || empty($ids)) return null;
		$queryBuilder = new solrQueryBuilder();
		$limit=0;
		$weight = count($ids)+1;
		foreach($ids as $id){
			$queryBuilder->add_or_filter("id", $id."^".$weight);
			$limit++;
			$weight--;
		}

	    $includeOOS = get_var('include_oos', 0);
		$query =  $queryBuilder->get_query();
        if(empty($includeOOS)){
			$query .= " AND count_options_availbale:[1 TO *]";
        }
		$response = $this->solr_product->searchAndSort($query, 0 , $limit);
		$arrayDocument = documents_to_array($response->docs);
		$arrayDocument = WidgetUtility::getArraySolrFormattedData($arrayDocument);
		return $arrayDocument;
	}

	public function fixImagePaths($obj, $query='') {
		if(empty($this->products) || empty($query)) {
			return;
		}

		$query = strtolower($query);

		foreach ($this->products as $key => $singleproduct) {
			if($singleproduct['generic_type'] == 'design') {
				if(preg_match("/women/i", $query) || preg_match("/womens/i",$query)) {
					$singleproduct['searchimagepath'] = $singleproduct['image_women_s'];
					$singleproduct['zoomedimagepath'] = $singleproduct['image_women_l'];
				} else if(preg_match("/kid/i",$query) || preg_match("/kids/i",$query)) {
					$singleproduct['searchimagepath'] = $singleproduct['image_kid_s'];
					$singleproduct['zoomedimagepath'] = $singleproduct['image_kid_l'];
				} else if(preg_match("/men/i",$query) || preg_match("/mens/i",$query)) {
					$singleproduct['searchimagepath'] = $singleproduct['image_men_s'];
					$singleproduct['zoomedimagepath'] = $singleproduct['image_men_l'];
				}else {
					$usergroup=strtolower($singleproduct['usergroup']);
					$search_image="image_".$usergroup."_s";
					$zoom_image="image_".$usergroup."_l";
					$singleproduct['searchimagepath'] = $singleproduct[$search_image];
					$singleproduct['zoomedimagepath'] = $singleproduct[$zoom_image];
				}

			} else {
				$singleproduct['searchimagepath'] = $singleproduct['search_image'];
				$singleproduct['zoomedimagepath'] = $singleproduct['search_zoom_image'];
			}

			if(!isset($singleproduct['searchimagepath']) || empty($singleproduct['searchimagepath'])
			|| strlen($singleproduct['searchimagepath'])<5) {
				$singleproduct['searchimagepath'] = '';
				$singleproduct['zoomedimagepath'] = '';
			}

			if(!empty($singleproduct['searchimagepath'])) {
				$lowerCasePath = strtolower($singleproduct['searchimagepath']);
				$ext = substr($lowerCasePath, strrpos($lowerCasePath, '.') + 1);

				if($ext == 'jpg' || $ext == 'png') {
					// This is fine.. no issues.
				} else {
					$singleproduct['searchimagepath'] = '';
					$singleproduct['zoomedimagepath'] = '';
				}
			}

			$this->products[$key] = $singleproduct;
		}
	}
	function getRelatedProductsPDP($tags,$exclude_id='',$offset=0,$limit=10)
	{
		$queryBuilder = new solrQueryBuilder();
		$queryBuilder->add_filter("*:* -id",$exclude_id);
		foreach($tags as $tag)
		{
			if(!empty($tag))
			{
				$queryBuilder->add_or_filter("keywords", '"'.$tag.'"');
				//				$queryBuilder->add_phrase($tag,array("keywords","product"),true);
			}
		}
		$dontShowOOSProducts = FeatureGateKeyValuePairs::getFeatureGateValueForKey("dontShowOOSProducts");
		if((!empty($dontShowOOSProducts) && $dontShowOOSProducts == 'true'))
			$queryBuilder->add_filter("count_options_availbale","[1 TO *]");
		$query =  $queryBuilder->get_query();
		$response = $this->solr_product->searchAndSort($query, $offset , $limit);
		$arrayDocument = documents_to_array($response->docs);
		$arrayDocument = WidgetUtility::getArraySolrFormattedData($arrayDocument);
		return $arrayDocument;
	}
	public function getDiscountPercentageRange($b_res = null){
        $count = array();
        foreach($b_res->discount_percentage_details->counts as $tmp_key=>$tmp_value){
            $range_key=intVal($tmp_key);
            $count[$range_key]=$tmp_value;
        }
        // order descending to count aggregate numbers
        krsort($count);
        $total_count=0;
        foreach($count as $key=>$tmp_count){
        // aggregated values for count
            $total_count += $tmp_count;
            if($key <=50){
                $count[$key]=$total_count;
            }else{
                unset($count[$key]);
            }
        }   
        $b_res->discount_percentage_details->counts=(object)$count;
        return $b_res->discount_percentage_details;
    }
	public function getPriceRange($cntRangeBreakup = 4,$b_res = null) {
		global $_MABTestObject;
		$shownewui= $_MABTestObject->getUserSegment("PriceRange");
		$shownewui = "v1";
		if($shownewui == "v1") {
			$prices = array();
			$i=0;
			foreach ($b_res->discounted_price as $key => $value) {
				for($j=0;$j<$value;$j++) {
					$prices[($i)] = $key;
					$i++;
				}
			}
			$lowerBound = floor($prices[0]);
			$upperBound = ceil($prices[sizeof($prices) -1]);
			$range = Array();
			$range['rangeMin'] = doRoundDown($lowerBound, 0);
			$range['rangeMax'] = doRoundUp($upperBound);
			$range['breakUps'] = Array();
			// Breaking Such That all the range have almost equal number of elements
			// 1st verify the maximum number of ranges possible
			$maxBreakup = getMaxBreakupRange($range['rangeMin'], $range['rangeMax']);
			if($cntRangeBreakup>$maxBreakup) {
				$cntRangeBreakup = $maxBreakup;
			}
			$startPoint = array();
			$endPoint = array();
			$elementCount = array();
			$numOfElements = sizeof($prices);
			$begPos=0;
			for($i=0;$i<$cntRangeBreakup;$i++) {
				if($begPos>=$numOfElements)
				break;
				$pos = $begPos+ ceil($numOfElements/$cntRangeBreakup);
				if($pos>=$numOfElements) {
					$pos = $numOfElements-1;
				}
				if($i!=0) {
					$temp = doRoundDown($endPoint[$i-1]+1);
					if($temp<=$range['rangeMax']) {
						$startPoint[$i] = $temp;
					} else {
						break;
					}
				} else {
					$startPoint[$i] = $range['rangeMin'];
				}
				$endPoint[$i] = doRoundUp($prices[$pos]);
				$left =($pos+1>=$numOfElements-1?$numOfElements-1:$pos+1);
				$right = $numOfElements-1;
				$flag=true;
				do {
					$flag = !$flag;
					$mid = ($flag)?ceil(($left + $right)/2):floor(($left + $right)/2);
					if($prices[$mid-1]<=$endPoint[$i]&& $prices[$mid]>$endPoint[$i]) {
						$pos = $mid;
						break;
					} else if ($prices[$mid]>$endPoint[$i]) {
						$right = $mid;
					} else {
						$left = $mid;
					}
				} while($left<$right);
				$elementCount[$i]= $pos-$begPos;
				//as pos can't be more than last elements index if there is only one element it gives wrong count as 0
				if($pos==$begPos && $pos == $numOfElements -1) {
					$elementCount[$i]= 1;
					break;
				}
				$begPos=$pos;
			}
			for($i=0;$i<sizeof($startPoint);$i++) {
				$range['breakUps'][$i]['range'] = "$startPoint[$i] TO $endPoint[$i]";
				$range['breakUps'][$i]['rangeMin'] = $startPoint[$i];
				$range['breakUps'][$i]['rangeMax'] = $endPoint[$i];
				$range['breakUps'][$i]['occurace'] = $elementCount[$i];
			}
			return $range;
		} else {
			$term_freq = array();
			$b_res = $this->solr_product->facetedSearch($this->final_query, "discounted_price");
			foreach ($b_res->discounted_price as $key => $value) {
				$term_freq[($key)] = $value;
			}
			$prices = array_keys($term_freq);
			$lowerBound = floor($prices[0]);
			$upperBound = ceil($prices[sizeof($prices) -1]);
			$range = Array();
			$range['rangeMin'] = doRoundDown($lowerBound, 0);
			$range['rangeMax'] = doRoundUp($upperBound);
			$range['breakUps'] = Array();
			for($i = 0; $i < $cntRangeBreakup; ) {//intentionally not putting autoincrement in loop as there can be cases where we might have to collapse the ranges
				$range['breakUps'][$i] = Array();
				$startPoint = round($lowerBound + $i * ($upperBound - $lowerBound)/$cntRangeBreakup);
				$startPoint = doRoundDown($startPoint, $i);
				$endPoint = round($upperBound - ($cntRangeBreakup - $i - 1) * ($upperBound - $lowerBound)/$cntRangeBreakup);
				$endPoint = doRoundUp($endPoint);
				$range['breakUps'][$i]['range'] = "$startPoint TO $endPoint";
				$range['breakUps'][$i]['rangeMin'] = $startPoint;
				$range['breakUps'][$i]['rangeMax'] = $endPoint;
				$range['breakUps'][$i]['occurace'] = 0;
				foreach ($prices as $price) {
					if($price >= $startPoint && $price <= $endPoint) {
						$range['breakUps'][$i]['occurace'] += $term_freq[$price];
					}
				}
				if($range['breakUps'][$i]['occurace'] == 0) { //this means that there is no occurance in this range so lets short cirtuit the range
					$cntRangeBreakup = $cntRangeBreakup - 1;//decreasing the number of sub ranges
					$i = 0;//reseting loop couter
					$range['breakUps'] = Array();
				} else {
					$i++;//incrementing loop counter
				}
			}
			return $range;
		}
	}


	public function getAutoSuggest($phrase)
	{
		try
		{
			$queryWords = explode(" ", $phrase);
			$query = "";
			foreach ($queryWords as $word) {
				if(!empty($word))
				$query .= "keyword:$word";
			}
			$query .= "*";
			$response = $this->solr_stats_search->searchAndSort($queries, 0, 5, "count_of_products desc, count_of_searches desc");
			print_r($response);
		}
		catch (Exception $ex)
		{
			print_r($ex);
		}
	}
	
    public function getSortField($revenueSortField  = "revenue_sort_field", $potentialRevenueSortField  = "potential_revenue_sort_field") {
        $sortField = $this->getSortFieldBySortParam($this->sort_param, $revenueSortField, $potentialRevenueSortField);
        if("$this->query_level" == "FULL") {
            if(!empty($sortField)) {
                $sortField = "score desc , ".$sortField;
            } else {
                $sortField = "score desc";
            }
        }
        $this->sortField = $sortField;
        return $this->sortField;
    }

    /*
     * general sort field based on sort param input
     */
    public function getSortFieldBySortParam($sortParam, $revenueSortField  = "revenue_sort_field", $potentialRevenueSortField  = "potential_revenue_sort_field") {
        global $_MABTestObject;
        $styleRATest = FeatureGateKeyValuePairs::getFeatureGateValueForKey("styleRATest");
        $showAdjustedRevenueSearch = $_MABTestObject->getUserSegment($styleRATest);
        switch ($sortParam) {
            case "PRICEA" :
                $sortField = "discounted_price asc, price asc, score desc, styleid desc";
                break;
            case "PRICED" :
                $sortField = "discounted_price desc, price desc, score desc, styleid desc ";
                break;
            case "DISCOUNT" :
                $sortField = "discount_percentage desc,$revenueSortField desc, $potentialRevenueSortField desc, global_attr_catalog_add_date desc";
                break;
            case "RECENCY" :
                $sortField = "global_attr_catalog_add_date desc, count_options_availbale desc, score desc, styleid desc";
                break;
            case "POPULAR" :
            	if ($styleRATest == "styleRevenueAdjusted4") {
            		if($showAdjustedRevenueSearch == "test") {
            			if(preg_match('/^revenue_(.*)sort_field$/', $revenueSortField)) {
            				$revenueSortField  = str_replace("revenue_", "revenue_adjusted_", $revenueSortField);
            			}
            		}
            	} else {
            		if (empty($revenueSortField)) {
            			$revenueSortField  = "real_revenue_sort_field";
            		} else {
            			if(preg_match('/^revenue_(.*)sort_field$/', $revenueSortField)) {
            				$revenueSortField  = str_replace("revenue_", "real_revenue_", $revenueSortField);
            			}
            		}
            	}

                $sortField = "count_options_availbale desc, $revenueSortField desc, $potentialRevenueSortField desc, global_attr_catalog_add_date desc";
                break;

            default :
            	if ($styleRATest == "styleRevenueAdjusted4") {
            		if($showAdjustedRevenueSearch == "test") {
            			if(preg_match('/^revenue_(.*)sort_field$/', $revenueSortField)) {
            				$revenueSortField  = str_replace("revenue_", "revenue_adjusted_", $revenueSortField);
            			}
            		}
            	} else {
            		if (empty($revenueSortField)) {
            			$revenueSortField  = "real_revenue_sort_field";
            		} else {
            			if(preg_match('/^revenue_(.*)sort_field$/', $revenueSortField)) {
            				$revenueSortField  = str_replace("revenue_", "real_revenue_", $revenueSortField);
            			}
            		}
            		$matches = array();
                    if (preg_match('/test([0-9]+)/', $showAdjustedRevenueSearch, $matches) == 1 ) {
                        $storeName = "style_store".$matches[1]."_";
                        $revenueSortField  = str_replace("real_revenue_", $storeName, $revenueSortField);
                    }
            	}
            	$sortField = "count_options_availbale desc, $revenueSortField desc, $potentialRevenueSortField desc, global_attr_catalog_add_date desc";
        }
        return $sortField;
    }

	public function getSortFieldForSolr() {
		return $this->getSortFieldAsSolrSortField($this->sort_param);
	}

/*
* get mapping of sort param with solr sort field
*/
	public function getSolrSortField() {
		$solrSort = array();
		$solrSort["PRICEA"] = $this->getSortFieldAsSolrSortField("PRICEA");
		$solrSort["PRICED"] = $this->getSortFieldAsSolrSortField("PRICED");
		$solrSort["DISCOUNT"] = $this->getSortFieldAsSolrSortField("DISCOUNT");
		$solrSort["RECENCY"] = $this->getSortFieldAsSolrSortField("RECENCY");
		$solrSort["POPULAR"] = $this->getSortFieldAsSolrSortField("POPULAR");
		return $solrSort;
	}
/*
* sort field in associative form for solr service
*/
    public function getSortFieldAsSolrSortField($sortParam = "POPULAR", $revenueSortField  = "revenue_sort_field", $potentialRevenueSortField  = "potential_revenue_sort_field") {
        $sortField = array();
        $styleRATest = FeatureGateKeyValuePairs::getFeatureGateValueForKey("styleRATest");
        global $_MABTestObject;
        $showAdjustedRevenueSearch = $_MABTestObject->getUserSegment($styleRATest);
        //switch the sort param to get the sort fields
        switch ($sortParam) {
            case "PRICEA" :
                $sort["sort_field"] = "discounted_price";
                $sort["order_by"] = "asc";
                $sortField[] = $sort;
                $sort["sort_field"] = "price";
                $sort["order_by"] = "asc";
                $sortField[] = $sort;
                $sort["sort_field"] = "score";
                $sort["order_by"] = "desc";
                $sortField[] = $sort;
                $sort["sort_field"] = "styleid";
                $sort["order_by"] = "desc";
                $sortField[] = $sort;
                break;
            case "PRICED" :
                $sort["sort_field"] = "discounted_price";
                $sort["order_by"] = "desc";
                $sortField[] = $sort;
                $sort["sort_field"] = "price";
                $sort["order_by"] = "desc";
                $sortField[] = $sort;
                $sort["sort_field"] = "score";
                $sort["order_by"] = "desc";
                $sortField[] = $sort;
                $sort["sort_field"] = "styleid";
                $sort["order_by"] = "desc";
                $sortField[] = $sort;
                break;
            case "DISCOUNT" :
                $sort["sort_field"] = "discount_percentage";
                $sort["order_by"] = "desc";
                $sortField[] = $sort;
                $sort["sort_field"] = "$revenueSortField";
                $sort["order_by"] = "desc";
                $sortField[] = $sort;
                $sort["sort_field"] = "$potentialRevenueSortField";
                $sort["order_by"] = "desc";
                $sortField[] = $sort;
                $sort["sort_field"] = "global_attr_catalog_add_date";
                $sort["order_by"] = "desc";
                $sortField[] = $sort;
                break;
            case "RECENCY" :
                $sort["sort_field"] = "global_attr_catalog_add_date";
                $sort["order_by"] = "desc";
                $sortField[] = $sort;
                $sort["sort_field"] = "count_options_availbale";
                $sort["order_by"] = "desc";
                $sortField[] = $sort;
                $sort["sort_field"] = "score";
                $sort["order_by"] = "desc";
                $sortField[] = $sort;
                $sort["sort_field"] = "styleid";
                $sort["order_by"] = "desc";
                $sortField[] = $sort;
                break;
            case "POPULAR" :
            	if ($styleRATest == "styleRevenueAdjusted4") {
            		if($showAdjustedRevenueSearch == "test") {
            			if(preg_match('/^revenue_(.*)sort_field$/', $revenueSortField)) {
            				$revenueSortField  = str_replace("revenue_", "revenue_adjusted_", $revenueSortField);
            			}
            		}
            	} else {
            		if (empty($revenueSortField)) {
            			$revenueSortField  = "real_revenue_sort_field";
            		} else {
            			if(preg_match('/^revenue_(.*)sort_field$/', $revenueSortField)) {
            				$revenueSortField  = str_replace("revenue_", "real_revenue_", $revenueSortField);
            			}
            		}
            	}

                $sort["sort_field"] = "count_options_availbale";
                $sort["order_by"] = "desc";
                $sortField[] = $sort;
                $sort["sort_field"] = "$revenueSortField";
                $sort["order_by"] = "desc";
                $sortField[] = $sort;
                $sort["sort_field"] = "$potentialRevenueSortField";
                $sort["order_by"] = "desc";
                $sortField[] = $sort;
                $sort["sort_field"] = "global_attr_catalog_add_date";
                $sort["order_by"] = "desc";
                $sortField[] = $sort;
                break;

            default :
            	if ($styleRATest == "styleRevenueAdjusted4") {
            		if($showAdjustedRevenueSearch == "test") {
            			if(preg_match('/^revenue_(.*)sort_field$/', $revenueSortField)) {
            				$revenueSortField  = str_replace("revenue_", "revenue_adjusted_", $revenueSortField);
            			}
            		}
            	} else {
            		if (empty($revenueSortField)) {
            			$revenueSortField  = "real_revenue_sort_field";
            		} else {
            			if(preg_match('/^revenue_(.*)sort_field$/', $revenueSortField)) {
            				$revenueSortField  = str_replace("revenue_", "real_revenue_", $revenueSortField);
            			}
            		}
            		$matches = array();
            		if (preg_match('/test([0-9]+)/', $showAdjustedRevenueSearch, $matches) == 1 ) {
            			$storeName = "style_store".$matches[1]."_";
            			$revenueSortField  = str_replace("real_revenue_", $storeName, $revenueSortField);
            		}
            	}
            	 
                $sort["sort_field"] = "count_options_availbale";
                $sort["order_by"] = "desc";
                $sortField[] = $sort;
                $sort["sort_field"] = "$revenueSortField";
                $sort["order_by"] = "desc";
                $sortField[] = $sort;
                $sort["sort_field"] = "$potentialRevenueSortField";
                $sort["order_by"] = "desc";
                $sortField[] = $sort;
                $sort["sort_field"] = "global_attr_catalog_add_date";
                $sort["order_by"] = "desc";
                $sortField[] = $sort;
                break;
        }
        return $sortField;
    }

	public function getSingleStyleFormattedSolrData($styleid) {

		$solrSearchProducts = SearchProducts::getInstance();
		$singleStyleSolrData = $solrSearchProducts->getSingleProductOrDesign("0_style_$styleid");

		if(!empty($singleStyleSolrData)) {
			$singleStyleSolrData = WidgetUtility::getWidgetFormattedData($singleStyleSolrData);
		}

		return $singleStyleSolrData;
	}

	public function getMultipleStyleFormattedSolrData($styleids) {
		$ids = array();
		foreach ($styleids as $styleid) {
			$ids[] = "0_style_$styleid";
		}
		$solrSearchProducts = SearchProducts::getInstance();
		$multiStyleSolrData = $solrSearchProducts->getMultipleProductOrDesign($ids);
		return $multiStyleSolrData;
	}

	public function getAjaxQueryResult($final_query=null,$filteredQuery, $sort_param = null, $page_num = 0, $noofitems=24,$boostedQuery="",$queryType = "")
	{
            global $errorlog;
		//echo " boostedQuery ". $boostedQuery;exit;
		if(!empty($final_query)){
			$this->final_query = $final_query . $filteredQuery;
		}else{
            $searchType = FeatureGateKeyValuePairs::getFeatureGateValueForKey("search.type");
            $fullTextSearchRes = $this->fullTextSearch();
            if(!empty($searchType) && $searchType == "free" && !empty($fullTextSearchRes)){
                $original_query = $this->fullTextSearch();
            }else{
			    $original_query = $this->func_get_query_new($this->filtered_phrase, "true");
            }
			$filteredQueryNew = $original_query . $filteredQuery;
			$this->final_query = $filteredQueryNew;
		}
		$this->sort_param = $sort_param;
		$this->faceted_search_results_new = $this->performUnifiedFacetedSolrQuery();
		$sort_field = $this->GetGenderModifiedSolrField();
		$this->offset = $page_num;
		$this->limit = $noofitems;
		$this->query_level = 'NORM';
		
		//set if the ajax call is parameteriezed or not
		if (!is_null($final_query) && !empty($final_query)) {
			$this->is_parameteric_search = 'y';
		}

		try {
		$this->performSolrQuery($sort_field,$boostedQuery,$filteredQuery);//,$boostedQuery);
		}
		catch(exception $e) {
			global $solrlog;
			$errorlog->error("Error in Solr search in line: ".$e->getLine()." in file: ".$e->getFile().": ".$e->getMessage()." ; \n Stack Trace:".$e->getTraceAsString());
		}
		//in old UI we need to perform full text search as in old ui page is rendered through search.php and ajaxSearch.php
		if(strtolower($queryType) != "norm")
		{
			$this->DoAlternateSearch($filteredQuery,$sort_field);
		}
	}
	public function DoAlternateSearch($filteredQuery = "",$sort_field = "")
	{
		if(empty($this->products_count) || $this->products_count == 0)
		{
	        $this->is_parameteric_search = 'n';
			$this->final_query = $this->get_query_full_text($this->filtered_phrase);
			$this->final_query  = $this->final_query.$filteredQuery;
			$this->faceted_search_results_new = $this->performUnifiedFacetedSolrQuery();
			if(empty($sort_field)){
				$sort_field = $this->getSortField().', score desc ';
			}
			$this->performSolrQuery($sort_field);
		}
	}
	public function fullTextSearch()
	{
			return $this->get_query_full_text($this->filtered_phrase," ");
	}

	public function performUnifiedFacetedSolrQuery() {
        global $xcache;
        $offerFilterEnabled = FeatureGateKeyValuePairs::getFeatureGateValueForKey("offerFilterEnabled");
        if($offerFilterEnabled){
        	$facetFieldArray = Array("global_attr_master_category_facet","global_attr_sub_category_facet","brands_filter_facet","global_attr_gender","global_attr_article_type_facet","sizeGroup","typename","colour_family_list","sizes_facet","discounted_price", "categories","dre_offerType");
        }else{
        	$facetFieldArray = Array("global_attr_master_category_facet","global_attr_sub_category_facet","brands_filter_facet","global_attr_gender","global_attr_article_type_facet","sizeGroup","typename","colour_family_list","sizes_facet","discounted_price", "categories");
        }
		
		try {
		    $b_res_facet_fields = $this->solr_product->multipleFacetedSearch($this->final_query, $facetFieldArray);
		    $b_res = $b_res_facet_fields->facet_fields;
		    $b_res->price_range_details = $this->getPriceRange(4,$b_res);
		    $b_res->discount_percentage_details = $b_res_facet_fields->facet_ranges->discount_percentage;
		    $this->price_range_details = $b_res->price_range_details;
            $this->discount_percentage_details = $this->getDiscountPercentageRange($b_res);
            /**
             * @var array $sizeGroup contains count of search result for each size group
             * key = size group name
             * value = count of styles in that size group in search
             */
			$sizeGroup = ((array)($b_res->sizeGroup));
			
			$sizeGroupFlag = false;
			//if there is only 1 category size group and it is not the _empty_ size group
			if (empty($sizeGroup["_empty_"]) && count($sizeGroup) == 1) {
				$sizeGroupFlag =true;
			}
			
			global $_MABTestObject;
			$showStickySize = $_MABTestObject->getUserSegment("stickySize");
			//set smarty variable for size group
			global $smarty;
			if(isset($smarty)) {
				$smarty->assign('showStickySize', $showStickySize);
			}
			/* Add sizes to the list only if there is only one article type items are listed
			   show specific attributes type only when single article type is present
			*/
			if (count((array)($b_res->global_attr_article_type_facet)) == 1 || $sizeGroupFlag) {
				if(count((array)($b_res->global_attr_article_type_facet)) == 1) {
					foreach($b_res->global_attr_article_type_facet as $key=>$article_type){
						$article_type = $key;
						break;
					}
					$results = $xcache->fetchAndStore(function($article_type) {
										$sql = "select at.attribute_type as attribute_type from mk_catalogue_classification cc
												left join mk_attribute_type at on at.catalogue_classification_id=cc.id
												where is_filter=1 and cc.typename='".$article_type."'";
										return func_query($sql);
								}, array($article_type), "article_attribute:".$article_type);
					$facetFieldArray=Array();
					foreach ($results as $result) {
						array_push($facetFieldArray,str_replace(" ","_",$result['attribute_type']).'_article_attr');
					}
					$b_res2 = $this->solr_product->multipleFacetedSearch($this->final_query, $facetFieldArray)->facet_fields;
					unset($b_res2->numFound);
					$b_res_modified=new stdClass();
					foreach ($b_res2 as $key=>$tmp_attr_values) {
						if (count((array)$tmp_attr_values) != 0) {
							$values_array=array();
							foreach ($tmp_attr_values as $key2=>$tmp_attr_value) {
								$values_array[$key2]=$tmp_attr_value;
							}
							$b_res_modified->$key=array('title'=>str_replace('_',' ',str_replace('_article_attr','',$key)),'values'=>$values_array);
						}
					}
					$b_res->article_type_attributes=$b_res_modified;
				}
				
				//sticky size read the earlier selected size for this size group and saved it in session
				if (($showStickySize == "test") && $sizeGroupFlag) {
					$sizeGroupKey = implode(",", array_keys($sizeGroup));
					
					//set smarty variable for size group
					global $smarty;
					if(isset($smarty)) {
						$smarty->assign("searchSizeGroup", $sizeGroupKey);
					}
					
					global $XCART_SESSION_VARS;
					
					$userEmailId = $XCART_SESSION_VARS['login'];
					$savedSizes = array();
					if($userEmailId != '') {
						$savedSizesVal = func_query_first_cell("SELECT sizes_csv FROM sticky_size_search WHERE login = '$userEmailId' AND  size_group = '$sizeGroupKey'", TRUE);
						if($savedSizesVal != '') {
							$savedSizes["$sizeGroupKey"] = $savedSizesVal;
						}
					}
					$fpcSizeGroup = $XCART_SESSION_VARS['fpcSizeGroup'];
					$fpcSizeGroup = $this->updateFpcCookieVal($fpcSizeGroup, $savedSizes);
					x_session_unregister('fpcSizeGroup',true);
					x_session_register('fpcSizeGroup', "$fpcSizeGroup");
					if(isset($smarty)) {
						$smarty->assign("fpcSizeGroup", $fpcSizeGroup);
					}
				}
			} else {
				//sticky sizes: set the saved sizes of size group in session
				if ($showStickySize == "test") {
					global $XCART_SESSION_VARS;
					$userEmailId = $XCART_SESSION_VARS['login'];
					
					$savedSizes =  array();
					if($userEmailId != '') {
						foreach ($sizeGroup as $sizeGroupKey=>$value) {
							$savedSizesVal = func_query_first_cell("SELECT sizes_csv FROM sticky_size_search WHERE login = '$userEmailId' AND  size_group = '$sizeGroupKey'", TRUE);
							//if there is any saved sizes updae the cookie
							if($savedSizesVal != '') {
								$savedSizes["$sizeGroupKey"] = $savedSizesVal;
							}
						}
					}
					$fpcSizeGroup = $XCART_SESSION_VARS['fpcSizeGroup'];
					$fpcSizeGroup = $this->updateFpcCookieVal($fpcSizeGroup, $savedSizes);
					x_session_unregister('fpcSizeGroup',true);
					x_session_register('fpcSizeGroup', "$fpcSizeGroup");
					global $smarty;
					if(isset($smarty)) {
						$smarty->assign("fpcSizeGroup", $fpcSizeGroup);
					}
				}
				
				unset($b_res->sizes_facet);
			}
            $this->genderFix($b_res);
			$this->sortByFilterOrder($b_res,$article_type);
            unset($b_res->numFound);
			return $b_res;
		} catch (Exception $ex) {
			global $errorlog;
			$errorlog->error("SearchProducts :: exception while doing faceted search "  . $ex->getMessage());
		}
	}
	
	/**
	 * function to update the fpc cookie for a given size group and selected sizes
	 * @param array $sizes : selected/saved sizes of the size group colon separated e.g. array("shoe_size" => "1:2:3")
	 */
	public function updateFpcCookieVal($fpcSizeGroup, $sizesSaved) {
		//handle the boudary cases
		if(empty($sizesSaved)) {
			if($fpcSizeGroup == '') {
				$fpcSizeGroup = "$fpcSizeGroup|";
				$fpcSizeGroup = preg_replace('/(\|){2,}/', '|', $fpcSizeGroup);
			}
			return $fpcSizeGroup;
		}
		
		foreach ($sizesSaved as $sizeGroup => $sizes) {
			$pattern = '/'.$sizeGroup.'[^\|]*(\|){0,1}/';
			$replacement = $sizeGroup.'='.$sizes;
			
			//we remove the old value and add update one at the end
			$fpcSizeGroup = preg_replace($pattern, '', $fpcSizeGroup);
			if($fpcSizeGroup != "") {
				$fpcSizeGroup = $fpcSizeGroup."|".$replacement;
				$fpcSizeGroup = preg_replace('/(\|){2,}/', '|', $fpcSizeGroup);
			} else {
				$fpcSizeGroup = $replacement;
			}
		}	
			
		return $fpcSizeGroup;
	}
	
    public function genderFix($b_res){
        // Check if number of products is same as number of products in one gender
        // if yes then we need to reset other gender counts.
        foreach($b_res->global_attr_gender as $gender=>$count){
            if($count == $b_res->numFound){
                unset($b_res->global_attr_gender);
                $b_res->global_attr_gender->$gender = $count;
                break;
            }
        }
    }
	public function getTotalPages()
	{
		$pages = ($this->products_count <= $this->limit) ? 1 : ceil($this->products_count / $this->limit);
		return $pages;
	}

	private function sortByFilterOrder($result_set, $article_type){
		//Sort Display categories
		//only size to be sorted, rest all should be alphabetic
		//$result_set->display_category_facet=sortDisplayCategory($result_set->display_category_facet);

		//Sort Brand
		//only size to be sorted, rest all should be alphabetic
		//$result_set->brands_filter_facet=sortBrands($result_set->brands_filter_facet);

		// filter on article types
		//only size to be sorted, rest all should be alphabetic
		//$result_set->global_attr_article_type_facet=sortArticleTypes($result_set->global_attr_article_type_facet);

		// order aticle specific attributes
		// first get the article_type_id
		//only size to be sorted, rest all should be alphabetic
		//$result_set->article_type_attributes=sortArticleSpecificAttributes($result_set->article_type_attributes,$article_type);

		// sort sizes
		$result_set->sizes_facet=sortSizes($result_set->sizes_facet,$article_type);
	}

	private function getPage(){
		$page = get_var('page', 1);
		global $pageLimit;
		if(!isset($pageLimit)) {
			$pageLimit = FeatureGateKeyValuePairs::getFeatureGateValueForKey("searchPaginationLimit");
		}
		if(!is_numeric($pageLimit)) {
			$pageLimit = 100;
		}
		if(is_numeric($page) && $page <= $pageLimit){
			return $page;
		}else{
			return 1;
		}
	}

	public function getSolrCallLatencies() {
		return $this->solr_product->getSolrCallLatencies();
	}

	public function getInitLatency() {
		return $this->initLatency;
	}

	public function getFinalQuery() {
		return $this->final_query;
	}

	public function getBoostedQuery() {
		return $this->boosted_query;
	}

	public function getCuratedQuery() {
		$curatedQuery = $this->curatedSearchQuery;
		$curatedQuery['start'] = 0;
		$curatedQuery['rows'] = $curatedQuery['no_slots'];
		unset($curatedQuery['sortfield']);
		return $curatedQuery;
	}

	public function removeDuplicateStyles($styleArray) {
		$styleIdArray = array();
		foreach ($styleArray as $index => $style) {
			$styleId = $style['styleid'];
			if(in_array($styleId, $styleIdArray)) {
				unset($styleArray[$index]);
			} else {
				$styleIdArray[] = $styleId;
			}
		}
		return $styleArray;
	}

	public function getSeachOptimProducts() {
		if(empty($this->optimProducts)) {
			$this->optimProducts = $this->products;
		}
		return $this->getSearchProducts($this->optimProducts);
	}

	public function getSearchProducts($products) {
		$retProducts = array();
		foreach ($products as $product) {
			$retProducts[] = $this->minifySearchProduct($product);
		}
		return $retProducts;
	}

	public function minifySearchProduct($product) {
		$docFields = array(
			"brands_filter_facet",
			"colourVariantsData", //-> this stores the colour grouping data
			//"style_group", //this stores the list of style id grouped together
			//"global_attr_base_colour", // required for colour grouping
			//"global_attr_colour1", // required for colour grouping
			"dre_landing_page_url", // required for colour grouping and pdp page link - this is same as landing_page_url
			"search_image", // required for colour grouping and search page image
			"discount",
			"discount_label",
			"dre_discount_label",
			"discounted_price",
			"id",
			"price",
			"product",
			"sizes",
			"styleid",
			"stylename",
			"visual_tag");
		foreach ($product as $key => $value) {
			if(!in_array($key, $docFields)) {
				unset($product[$key]);
			}
		}
		return $product;
	}

	public function getSortFieldVal() {
		return $this->sortField;
	}

	/** Returns query for full text search **/
	function get_query_full_text($phrase,$operator="OR") {
		$query = "";
		$queryBuilder = new solrQueryBuilder();
		$queryBuilder->add_filter("statusid",'1008');

		$dontShowOOSProducts = FeatureGateKeyValuePairs::getFeatureGateValueForKey("dontShowOOSProducts");
		if((!empty($dontShowOOSProducts) && $dontShowOOSProducts == 'true'))
			$queryBuilder->add_filter("count_options_availbale","[1 TO *]");
		if (!empty($this->price_range) && $this->price_range != 'ALL') {
			$range = str_replace("TO", " TO ", $this->price_range);
			$queryBuilder->add_filter("price", "[" . $range . "]");
		}
		$phrase_array = $this->full_text_keywords_booster(explode(' ',$phrase));
		$boosted_phrase= implode(" $operator ",$phrase_array);
		if(!empty($boosted_phrase)){
			$query = '('.$queryBuilder->get_current_query()."AND ( full_text_myntra:($boosted_phrase)))";
		}
        if($operator=='OR'){
		    $this->query_level = 'FULL';
		    Profiler::increment("Search_OR_Query");
        }
        else {
		    Profiler::increment("Search_AND_Query");	
        }
		return $query;
		//$this->dumper_solr_query($this->final_query);
	}

	function full_text_keywords_booster($phrase_array = array()) {
		if(!is_array($phrase_array) || empty($phrase_array)) return ;
		$phrase_array = array_filter($phrase_array);
		$catatalogue_map = array_flip($this->catalogue_typename_map);
		foreach( $phrase_array as $index=>$keyword) {
			if( isset($this->filter_group_map[$keyword]) && isset($this->group_boost_map[$this->filter_group_map[$keyword]])) {
				$phrase_array[$index] = $keyword.'^'.$this->group_boost_map[$this->filter_group_map[$keyword]];
			}
			else if(isset($catatalogue_map[$keyword]) && isset($this->group_boost_map['catalogue'])) {
				$phrase_array[$index] = $keyword.'^'.$this->group_boost_map['catalogue'];
			}
			else if(in_array($keyword,$this->gender_list) && isset($this->group_boost_map['gender'])) {
				$phrase_array[$index] = $keyword.'^'.$this->group_boost_map['gender'];
			}
			else {
				$phrase_array[$index] = $keyword.'^1';
			}
		}
		return $phrase_array;
	}

	/* Dumps the Solr Query in readable format */
	function dumper_solr_query($query) {
		$len = strlen($query);
		$j=1;
		for($i=0;$i<$len;$i++) {
			$char = substr($query, $i,1);
			if($char == '(') {
				if(substr($query, $i-1,1) != ':') {
					$new_q .= "</br><b>$j</b>";
					for($k=1;$k<=$j;$k++) {
						$new_q .="--";
					}
					$j++;
				}
				else {
					$started = true;
				}
			} else if($char =='O') {
				if(substr($query, $i+1,1) == 'R') {
					$new_q .= "</br>";
					for($k=1;$k<=$j;$k++) {
						$new_q .="--";
					}
				}
			} else if($char == ')')  {
				if( $started != true) {
					$j--;
					$new_q .= "</br><b>$j</b>";
					for($k=1;$k<=$j;$k++) {
						$new_q .="--";
					}
				} else {
					$started = false;
				}
			}
			$new_q .= $char;
		}
		echo "<pre>";
		var_dump($new_q);
		echo "</pre>";
		die();
	}

	/**
	 * this function should be called to get the boosed items (for e.g. lowvisible)
	 * offset has to be maintained in user cookie.
	 * if the offset is not present then it has to be randomly generated
	 * limit of the product picked should be derived from $thresholdBoostArray
	 */
	function getBoostedItems($query, $boostFiledArray = array(), $offsetCoefficient=0, $boostAvailable=array()){
		global $weblog;
		$retVal = array();
		if(empty($boostAvailable)){
			//this means that the faceted search hasn't on boost fields
			$facetedResult = $this->faceted_search_results_new->categories;
			foreach($facetedResult as $key=>$val){
				foreach($boostFiledArray as $boostFieldKey=>$count)
					if(substr($boostFieldKey,0,strlen($key)) == $key)
						$boostAvailable[$boostFieldKey] = $val;
			}
		}
		if($offsetCoefficient == 0){
			$offsetCoefficient = rand(0,100);
		}

		$weblog->info("boosted items ::: offset $offsetCoefficient query :: $query");

		foreach($boostFiledArray as $boostField=>$numOfItemsRequired){
			if($boostAvailable[$boostField] > 0 && $numOfItemsRequired>0)
				$retVal = array_merge($retVal,$this->getBoostedItemsForField($query, $boostField, $boostAvailable[$boostField], $numOfItemsRequired, $offsetCoefficient));
		}
		$this->boosted_query = array();
		$this->boosted_query['query'] = $this->boostedItmeSolrQuery($retVal);
		return $retVal;
	}

	/**
	 * append boosted field check and querry
	 */
	function getBoostedItemsForField($query, $boostField, $numberOfBoostItemsAvaialble, $numOfItemsRequired=1, $offsetCoefficient){
		if(($numberOfBoostItemsAvaialble - $numOfItemsRequired) > 0)
			$boostOffset = intval(($numberOfBoostItemsAvaialble - $numOfItemsRequired) * $offsetCoefficient /100);
		else
			$boostOffset = 0;
		$query .= " AND categories:" . $boostField;
		$results = $this->solr_product->searchAndSort($query, $boostOffset, $numOfItemsRequired);
		$boostedResults = documents_to_array($results->docs);
		//echo "<br>$boostField $query";
		//foreach($boostedResults as $result)
			//echo " ". $result['styleid'];
		return $boostedResults;
	}


	function boostedItmeSolrQuery($boostedItems){
		$boostedItemsQuery = "styleid:(";
		$boostedItemsQuery .= " 0 ";
		foreach($boostedItems as $item){
			$boostedItemsQuery .= " OR ";
			$boostedItemsQuery .=  $item['styleid'];
		}
		$boostedItemsQuery .= ")";
		return $boostedItemsQuery;
	}
	/**
	 * the boosted items should appear for ajax as well
	 * this func should return query from boosted items.
	 */
	function addBoostedItemsInQuery($query, $boostedItems=array()){
		if(empty($boostedItems))
			return $query;

		$query = $this->boostedItmeSolrQuery($boostedItems) . " OR " . $query;
		//echo "<br>new query :: " .$query;
		return $query;
 	}

	/**
	 * the items which have appread on the first page due to boosting should be removed from subsequent pages
	 * this func should return query from boosted items.
	 */
	function removeBoostedItemsFromQuery($query, $boostedItems=array()){
		if(empty($boostedItems))
			return $query;

		$query .=  " - " .$this->boostedItmeSolrQuery($boostedItems);
			return $query;
 	}

	/**
	 * boosted items need to be merged with normal results. the postition of merged items should be preserved in cookie
	 */
	function mergeItems($normalItems, $boostedItems, $mergePositions=array(4,9,14,19,22,23)) {
		global $weblog;
		$weblog->info("mergeItems :: number of normal items " . count($normalItems) .  "number of boosted items ". count($boostedItems));
		if ((count($normalItems) + count($boostedItems)) < $this->limit) {
			return array_merge($normalItems, $boostedItems);
		} else {
			$mergedItems = $normalItems;
			$i=0;
			foreach($mergePositions as $mergePosition) {
				if($i < count($boostedItems)) {
					array_splice($mergedItems,$mergePosition,0,array($boostedItems[$i]));
					$i++;
				}
			}
			array_splice($mergedItems, count($normalItems));
		}
		return $mergedItems;
	}

	/*
	 * The following function returns a search querywords by removing the redundant words for catagory in the search query 
     * Function depreciated
	*/
	function getCanonicalSearchPhrase()
	{
        return false;
		$searchKeywords			= 	$this->original_phrase;
		$flag					=	false;
		$list					=	array();
		$cached_Catalogy_Map	=	$this->getCategoryMap();
		$queryWords				=	explode("-",$searchKeywords);
		if(is_array($queryWords))
		{
			foreach($queryWords as $key=>$value)
			{
				if($cached_Catalogy_Map[$value]==1)
				{
					// setting value to 2 from 1 implies that we have encountered this particular catagory once in our search key words.
					$cached_Catalogy_Map[$value]	=	2;
					$list[]							=	$value;
				}
				else if($cached_Catalogy_Map[$value]==2)
				{
					$flag	=	true;
				}
				else
				{
					$list[]	=	$value;
				}
			}
			$filteredQueryWords		=	implode("-", $list);
		} else {
			$filteredQueryWords		=	$this->original_phrase;
		}
		
		return $filteredQueryWords;
	}
	
	/*
	 * Following function will hit the xcache and returns a map containing the names of the catagory names cached from mk_catalogue_classification table
     * Function depreciated
	*/
	function getCategoryMap()
	{
		global $xcache;
		$cached_catagoryNames_Map	=	$xcache->fetchAndStore(function(){
	
			$cached_catagoryNames_Map	=	array();
			$cat_typename_result		=	func_query("select typename from mk_catalogue_classification where is_active='1' order by parent1 desc;");
	
			foreach($cat_typename_result as $p)
			{
				$cached_catagoryNames_Map[strtolower($p['typename'])]	=	1;
			}
			return $cached_catagoryNames_Map;
		}, array(), "searchProductsCatagoryMap:cached_category_data",1800);
			
			return $cached_catagoryNames_Map;
	}
}
