<?php
chdir(dirname(__FILE__));
require_once "../../../auth.php";
require_once "../../../include/func/func.db.php";
define(IS_TRACKING_ON, true);

class UserInterestCalculator
{
	
	const BASE_ACTION = "DefaultAction";
	const ADD_TO_CART = "AddToCart";
	const ADD_TO_SAVED = "AddToSaved";
	const PRODUCT_VIEW = "ProductView";
	const PURCHASE = "Purchase";
	
	const TRANSIENT_CONFIG_TABLE = 'mk_transients_config';
	const TODAYS_TRANSIENT_TABLE = 'mk_style_todays_transients';
	const COMPLETE_TRANSIENT_TABLE = 'mk_style_transient_data';
	const AGGREGATED_TRANSIENT_TABLE = 'mk_style_aggregated_transients';
	//var $actionName = BASE_ACTION;

	static function trackAction($styleId, $actionName = self::BASE_ACTION)
	{
		if(!IS_TRACKING_ON)
			return;
		
		
		global $XCART_SESSION_VARS;
		if($XCART_SESSION_VARS['is_robot'] === 'Y')
			return;//we should not track bot's behaviour
			
		$upsertQuery = "insert into " . self::TODAYS_TRANSIENT_TABLE ." (style_id, action, todays_count) values($styleId, '$actionName', 1) on duplicate key update todays_count = (todays_count + 1)";
		db_exec($upsertQuery);
		return;
	}
	
	static function aggregateData()
	{
		$now = time();
		$insertTodaysDataQuery = "insert into ". self::COMPLETE_TRANSIENT_TABLE. " (select style_id, $now as date, action, todays_count as count from " . self::TODAYS_TRANSIENT_TABLE . " where todays_count != 0)"; 
		db_exec($insertTodaysDataQuery);
		
		$resetTodaysDataQuery = "update " . self::TODAYS_TRANSIENT_TABLE . " set todays_count = 0";
		//$resetTodaysDataQuery = "truncate table $this->todaysTransientTable";
		db_exec($resetTodaysDataQuery);
		
		$cutoffDaysQuery = "select action, cutoff_days from " . self::TRANSIENT_CONFIG_TABLE;

		$cutOffDaysRes = func_query($cutoffDaysQuery);
		
		if(!empty($cutOffDaysRes))
		{
			$cleanAggregatesQuery = "truncate table " . self::AGGREGATED_TRANSIENT_TABLE;
			db_exec($cleanAggregatesQuery);
			
			foreach ($cutOffDaysRes as $row)
			{
				$actionName = $row['action'];
				$cutoffDays = $row['cutoff_days'];
				$aggregaterQuery = "insert into " . self::AGGREGATED_TRANSIENT_TABLE . " 
 						(select style_id, action, sum(count) as aggregated_count from " . self::COMPLETE_TRANSIENT_TABLE. " 
						where action = '$actionName' and date > (unix_timestamp() - $cutoffDays *24*60*60) 
						group by style_id)" ;
				db_exec($aggregaterQuery);
			}
		}
	}
}
