<?php
//require_once '../../../auth.php';
//$in = Array();
//$maxGroupLength = 3;
//$in[0] = "myntra1";
//$in[1] = "myntra2";
//$in[2] = "myntra3";
//$in[3] = "myntra4";
//$in[4] = "myntra5";
//$in[5] = "myntra6";
//$in[6] = "myntra7";
//$in[7] = "myntra8";
//$output = groupify($in);
//echo "\noutput = "; print_r($output);

function groupify($inputVar, $maxGroupLength = NULL, $pivotSize = 0, $pivotPosition = 0, $iteration = 0, $retval = NULL)
{
	if($pivotSize + $pivotPosition < sizeof($inputVar))
	{
		if(empty($maxGroupLength))
		{
			if(sizeof($inputVar) > 6)
			{
				$retval = Array();
				array_push($retval, $inputVar);
				return $retval;
			}
			else if(sizeof($inputVar) > 4)
				$maxGroupLength  = 2;
			else 
				$maxGroupLength = 3;
		}
		//echo "\n--------------------------------------------------------------------";
		$pivot = "";
		for($i=0; $i < $pivotSize && sizeof($inputVar) > $pivotPosition + $i; $i++)	
			$pivot .= " " . $inputVar[$pivotPosition + $i];
		//echo "\npivotPosition = $pivotPosition pivotSize = $pivotSize pivot = $pivot interation = $iteration"; 
		if(empty ($retval))
			$retval = Array();
		//$retval[$iteration] = Array();
	
		if(sizeof($inputVar) > $pivotPosition + $pivotSize)
		{
			$pivot .= " " . $inputVar[$pivotPosition + $pivotSize];
		}
		$prePivotGroups = Array();
		if($pivotPosition > 0)
		{
			$prePivotIn = Array();
			for($i = 0; $i < $pivotPosition; $i++)
				array_push($prePivotIn,$inputVar[$i]);
			$prePivotGroups = groupify($prePivotIn, $maxGroupLength);
		}
		
		if(empty($prePivotGroups))
		{
			$retval[$iteration] = Array();
			for($i = 0; $i < sizeof($inputVar); $i++)
			{
				if($i < $pivotPosition)
					array_push($retval[$iteration],$inputVar[$i]);
				else if ($i == $pivotPosition)
					array_push($retval[$iteration],trim($pivot));
				else if($i > $pivotPosition + $pivotSize)
					array_push($retval[$iteration],$inputVar[$i]);
			}
			$iteration++;
		}
		foreach ($prePivotGroups as $prePivotGroupsElements)
		{
			$retval[$iteration] = Array();
			foreach($prePivotGroupsElements as $elements)
				array_push($retval[$iteration],$elements);
				
			array_push($retval[$iteration],trim($pivot));
		
			for($i = $pivotPosition + $pivotSize + 1; $i < sizeof($inputVar); $i++)
				array_push($retval[$iteration],$inputVar[$i]);
			
			$iteration++;
		}
		
		$pivotSize++;
		//echo "\nretval = "; print_r($retval);
		if($pivotSize < $maxGroupLength && $pivotSize <= sizeof($inputVar))
		{
			groupify($inputVar, $maxGroupLength, $pivotSize, $pivotPosition, $iteration, $retval);
		}
		else if($pivotPosition < sizeof($inputVar))
		{
			groupify($inputVar, $maxGroupLength, 1, ++$pivotPosition, $iteration, $retval);
		}
	}
	return $retval;
}

function doRoundUp($entry, $target = 100)
{     
	$entry = (int)(($entry  + $target) / $target) * $target - 1;
	return $entry;
}                               

function doRoundDown($entry, $step, $target = 100)
{
	$entry = ((int)(($entry + $target) / $target) - 1) * $target;
	if($step > 0)
	$entry += $target;
	return $entry;
}

function getMaxBreakupRange($min,$max,$target =100)
{
	return ceil((($max-$min)/$target));
}
