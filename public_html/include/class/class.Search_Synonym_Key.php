<?php
use cache\InvalidateXCache;
	class SearchSynonymKey{
		
		const SEARCH_SYNONYM_KEY_CACHE = 'search_synonym_key';
		private static function GetSearchSynonym()
		{
			$SynonymQuery = "select * from search_synonym_table";
			$result = func_query($SynonymQuery);
			$synonymArray = array();
			$synonymFlag = false;
			foreach ($result as $key => $row)
			{
				$row[original] = strtolower($row[original]);
				$synonymArray[$row[original]] = $row[corrected];
			}
			return $synonymArray;
		}
		public static function refreshKeyValuePairsInCache() {
			$xCacheInvalidator =  InvalidateXCache::getInstance();
			$xCacheInvalidator->invalidateCacheForKey(self::SEARCH_SYNONYM_KEY_CACHE);
			global $xcache ;
			if($xcache==null){
				$xcache = new XCache();
			}
			$cachedSearchSynonymParameters = SearchSynonymKey::GetSearchSynonym();
    		$xcache->storeobject(self::SEARCH_SYNONYM_KEY_CACHE, $cachedSearchSynonymParameters, 0);
		}
		
    	public static function getAllSearchSynonymValuePairs() {
    		global $xcache;
    		if($xcache==null){
				$xcache = new XCache();
			}
			$cachedSearchSynonymParameters = $xcache->fetchobject (self::SEARCH_SYNONYM_KEY_CACHE);
  			if($cachedSearchSynonymParameters == NULL) {
    			 $cachedSearchSynonymParameters = SearchSynonymKey::GetSearchSynonym();
    			 $xcache->storeobject(self::SEARCH_SYNONYM_KEY_CACHE, $cachedSearchSynonymParameters, 0);
    		}
    		return $cachedSearchSynonymParameters;
    	}
	}

?>