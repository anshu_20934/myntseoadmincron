<?php
global $xcart_dir;
require_once "$xcart_dir/include/class/class.feature_gate.keyvaluepair.php";
require_once "$xcart_dir/include/class/class.mail.mailproviderfactory.php";
/**
 * Mails are classified into various types (3 currently)
 * This is set in mail_detail. 
 * Depending upon mail_type, appropriate provider is used to send the mail 
 * @author kundan
 */
class MailType {
	
	const CRITICAL_TXN = "CriticalTxn";
	const NON_CRITICAL_TXN = "NonCriticalTxn";
	const PROMOTIONAL = "Promotional";
	
	/**
	 * Mailers (or Mail service providers) for sending critical Transactional Mails: comma separated string
	 */
	const FG_CRITICAL_TXN_MAILERS_KEY = "Mailers.CriticalTxn";
	
	/**
	 * Mailers (or Mail service providers) for sending non-critical transactional Mails: comma separated string
	 * If no Mail channel or type is specified, this is assumed to be the default type
	 */
	const FG_NON_CRITICAL_TXN_MAILERS_KEY = "Mailers.NonCriticalTxn";
	
	/**
	 * Mailers (or Mail service providers) for sending promotional Mails: comma separated string
	 */
	const FG_PROMOTIONAL_MAILERS_KEY = "Mailers.Promotional";
	
	public static function init() {
		MailProviderFactory::init();
	}
	
	public static function getProvidersForMailType($mailType) {
		self::init();
		if($mailType === self::CRITICAL_TXN) {
			return self::getProvidersForFeatureGateKey(self::FG_CRITICAL_TXN_MAILERS_KEY);
		}
		else if($mailType === self::NON_CRITICAL_TXN) {
			return self::getProvidersForFeatureGateKey(self::FG_NON_CRITICAL_TXN_MAILERS_KEY);
		}
		else if($mailType === self::PROMOTIONAL) {
			return self::getProvidersForFeatureGateKey(self::FG_PROMOTIONAL_MAILERS_KEY);
		}
		if(empty($mailType)) {
			return self::getProvidersForEmptyMailType();
		}
	}
	
	private static function getProvidersForEmptyMailType() {
		return Array(MailProviderFactory::getDefaultProvider(), MailProviderFactory::getBackupProvider());
	}
	
	private static function getProvidersForFeatureGateKey($featureGateKey) {
		$retMailProviders = Array();
		$mailProviderNameArr = FeatureGateKeyValuePairs::getStrArray($featureGateKey);
		if(!empty($mailProviderNameArr)) {
			foreach ($mailProviderNameArr as $eachMailProviderName) {
				$retMailProviders[] = MailProviderFactory::getMailProvider($eachMailProviderName);
			}
			return $retMailProviders;
		}
		else {
			return self::getProvidersForEmptyMailType();
		}
		
	}
}
?>