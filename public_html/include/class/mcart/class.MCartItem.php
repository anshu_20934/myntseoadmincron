<?php

use enums\cart\CartContext;

include_once("$xcart_dir/modules/apiclient/SkuApiClient.php");
include_once("$xcart_dir/modules/apiclient/AtpApiClient.php");
include_once("$xcart_dir/modules/apiclient/ServiceabilityApiClient.php");
include_once("$xcart_dir/include/func/func.courier.php");
include_once("$xcart_dir/include/func/func.mkcore.php");
include_once "$xcart_dir/include/class/mcart/class.MCartUtils.php";

abstract class MCartItem extends Product{
	public static $__ITEM_STATE_OUT_OF_STOCK=0;
	public static $__ITEM_STATE_PARTIAL_OUT_OF_STOCK=1;
	public static $__ITEM_STATE_IN_STOCK=2;
	public static $__MAX_AVAILABILITY=99;
	/**
	 * style array has all the required information about style
	 *id: style id
	 *name: style name
	 * @var array
	 */
	private $style;

	private $styleid;
	/**
	 *
	 *id: SKU id
	 *code: SKU code
	 * @var array
	 */
	protected $sku;
	/**
	 * Non-unified size name
	 * @var String
	 */
	private $size;
	
	/**
	 * Last Api call for wms
	 * @var long
	 */
	private $lastApiCall=0;

	private $instock=0;
        
    public $itemAddedByDiscountEngine = false;
    
    public $isCouponApplicable = false;

    public $itemAddDate ;
    
    public $groupId;
    public $discountOnSet;
    public $couponApplicable;
    
    public $isSizeAutoSelected = false;
    
    /*
     * loyalty Points
     */
    public $loyaltyPointsAwarded = 0;
    public $loyaltyPointsAwardFactor = 1;
    public $loyaltyPointsConversionFactor = 0;
    public $loyaltyPointsUsed = 0 ;
    
	
    public $isCustomizable = false;
    
    public $isCustomized = false;
    
    public $customName;
    
    public $customNumber;

    public $customizedMessage;
    
    private $sellerId;
    
	public function __construct($productId,$skuID,$quantity) {
		$this->itemAddDate = time();
		$this->fetchItemInfo($skuID);
		$styleid = $this->getStyleId();
		$size = $this->getSizeName();
		$product['productId']=$productId;
        
        //XXX: ... And, the typos transcend!
        $product['producTypeId']=$this->style['productTypeId'];
        
        $product['productStyleId']=$styleid;
        $product['productStyleType']=$this->style['styleType'];
        $product['vatRate']=$this->style['vatRate'];;
        $product['quantity']=$quantity;
        $product['productPrice']=$this->style['price'];
        $net_of_tax= $this->style['price'] / (1 + ($this->style['vatRate'] / 100)) ;
		$vat_on_product = $net_of_tax * ($this->style['vatRate'] / 100);
		        
        $product['unitPrice']=$this->style['price']-$vat_on_product;

        $this->isCustomizable = (bool)$this->style['is_customizable'] &&
                                FeatureGateKeyValuePairs::getBoolean('personalization.enabled');
      
        parent::__construct($product);
        //$this->refreshItemAvailability();
	}

	
	public function getSkuId(){
		return $this->sku['id'];
	}
	/**
	 * Type of object Non Personalized or Personalized
	 */
	public function getType(){
		return MCart::$__ITEM_NON_PERS;
	}

	
	public function getStyleId(){
		return $this->styleid;
	}
	
	public function getSellerId(){
		return $this->sellerId;
	}
	
	/**
	 * style array has all the required information about style
	 * style['id']
	   style['name']
	   style['brand']
       style['productTypeId']
       style['productTypeLabel']
       style['styleType']
       style['price']
       style['gender']
       style['vatRate']
       style['is_customizable']
       style['cartImagePath']
       style['landingpageurl']

	 * @var array
	 */
	public function getStyleArray(){
		return $this->style;
	}
	
	public function setStyleArray($array){
		$this->style = $array;
	}
	
	/**
	 *
	 *id: SKU id
	 *code: SKU code
	 *jit_sourced:jit sourced
	 *isflatten: is Flattened Size
	 *unifiedsize: unified size
	 * @var array
	 */
	public function getSKUArray(){
		return $this->sku;
	}
	
	/**
	 * Non Unified Size of item
	 */
	public function getSizeName(){
		return $this->size;
	}
	
	public function getLoyaltyPointsAwarded(){
		return $this->loyaltyPointsAwarded;
	}
	
	public function getLoyaltyPointsUsed(){
		return $this->loyaltyPointsUsed;
	}
	
	public function setLoyaltyPointsUsed($loyaltyPoints,$conversionFactor){
		$this->loyaltyPointsUsed = $loyaltyPoints;
		$this->loyaltyPointsConversionFactor = $conversionFactor;
	}
	
	/*
	 * refresh and set loyalty points that are awarded for this product
	 * computes the to be paid subtotal of the product after item discount, 
	 * cart discount and coupon discount;
	 */
	public function refreshAwardedLoyaltyPoints($cartNuserLevelAwardFactor=0){
		$productSubTotal = (($this->getProductPrice()
				+ $this->getUnitAdditionalCharges())
				* $this->getQuantity() - ($this->getDiscountAmount()*$this->discountQuantity )/* item discount*/
				- $this->getCartDiscount() - $this->getDiscount()/* coupon discount on each product */ 
				- ($this->loyaltyPointsUsed*$this->loyaltyPointsConversionFactor) /* loyalty points used on each product*/);
		/*
		 * if product level award factor is lessthan or equal to zero assign it to 1
		 */
		if($this->loyaltyPointsAwardFactor <= 0 || $this->loyaltyPointsAwardFactor  == null){
			$this->loyaltyPointsAwardFactor = 1;
		}
		$this->loyaltyPointsAwarded = ($productSubTotal*$this->loyaltyPointsAwardFactor*$cartNuserLevelAwardFactor)/100;
	}

	protected function fetchItemInfo($skuID){
		global $sql_tbl,$weblog;
		$tab_mk_product_style  = $sql_tbl["mk_product_style"];
		$tab_mk_product_options  = $sql_tbl["mk_product_options"];
		$tab_mk_style_properties  = $sql_tbl["style_properties"];
		$skuID = mysql_real_escape_string($skuID);
		$sql="SELECT ps.product_type producttypeid,ps.name stylename,ps.is_customizable,ps.styleType,
			ps.label,ps.price, sp.global_attr_gender gender,sp.global_attr_brand brand, sp.global_attr_article_type article_type, pt.product_type_groupid producttypegroupid,pt.name producttypename, pt.vat,sp.default_image,
			sosm.sku_id skuid, po.id as optionid, ps.id as styleid, po.value as sizevalue      
			FROM mk_product_style ps, mk_style_properties sp, mk_product_type pt,mk_product_options po,mk_styles_options_skus_mapping sosm 
			WHERE ps.product_type=pt.id AND sp.style_id=ps.id and sosm.sku_id='".$skuID."' and po.style=ps.id and sosm.style_id=ps.id and sosm.option_id =po.id";// and po.value='".($this->size)."'
		
		$resultSet=func_query_first($sql);
		$this->updateItemInfo($resultSet);
	
	}

	protected function updateItemInfo($resultSet){
		global $sql_tbl,$weblog;
                global $errorlog;
		if(empty($resultSet)){
			$weblog->error("MCartItem:refreshItem:$this->styleid:Empty style info");
			$errorlog->error("MCartItem:refreshItem:$this->styleid:Empty style info");
			return;
		}
		
		$this->styleid=$resultSet['styleid'];
		$this->size=$resultSet['sizevalue'];
		
		$this->style['id']=$this->styleid;
		$this->style['name']=$resultSet['stylename'];
		$this->style['brand']=$resultSet['brand'];
		$this->style['article_type']=$resultSet['article_type'];
		$this->style['productTypeId']=$resultSet['producttypeid'];
		$this->style['productTypeLabel']=$resultSet['producttypename'];
		$this->style['styleType']=$resultSet['styleType'];
		$this->style['price']=$resultSet['price'];
		$this->style['gender']=$resultSet['gender'];
		$this->style['vatRate']=$resultSet['vat'];
		$this->style['is_customizable']=$resultSet['is_customizable'];


		
		
		
		$def_image=$resultSet['default_image'];
		$img_thumb = str_replace("_images", "_images_96_128",$def_image );
		$this->style['cartImagePath']=$img_thumb;

		$solrSearchProducts = SearchProducts::getInstance();
		$solrWidgetData = $solrSearchProducts->getSingleStyleFormattedSolrData($this->styleid);
		$this->style['landingpageurl']=$solrWidgetData['landingpageurl'];

		if(!empty($solrWidgetData["loyaltyPointsAwardFactor"])){
			$this->loyaltyPointsAwardFactor = $solrWidgetData["loyaltyPointsAwardFactor"];
		}
		
		$this->sku['id']=$resultSet['skuid'];
		
		$sizeOptions=Size_unification::getUnifiedSizeByStyleId($this->styleid, "array");
		$flattenSizeOptions = Size_unification::ifSizesUnified($sizeOptions);

		$this->sku["isflatten"]=$flattenSizeOptions;
		if($flattenSizeOptions){
			$this->sku["unifiedsize"]=$sizeOptions[$this->size];
		}
	}
	
	/**
	 * refreshes cart item ,
	 * Note: Quantity is saved
	 * @var $refreshFromDb if true refreshes all cached style sku data from db
	 */
	public function refreshItem($refreshFromDb,$skuResultSet){
		if(!empty($skuResultSet)){
			$this->updateItemInfo($skuResultSet);
		}else if($refreshFromDb) {
			$skuID = $this->sku['id'];
			$this->fetchItemInfo($skuID);
		}
		// Feature gate condition to enable Non-discount-products coupon
		// Use these feature in class.MCart.php
		$condNoDiscountCoupon = FeatureGateKeyValuePairs::getBoolean('condNoDiscountCoupon');
		$styleExclusionFG = FeatureGateKeyValuePairs::getBoolean('styleExclusion');
		
		if($styleExclusionFG && (!$this->couponApplicable)){
			$this->isCouponApplicable = false;
		}else if ($condNoDiscountCoupon && ($this->isDiscountedProduct)){
			$this->isCouponApplicable = false;
		}else{
			$this->isCouponApplicable = true;
		}
               
        $this->productTypeId = $this->style['productTypeId'];
        $this->productStyleType = $this->style['styleType'];
        $this->vatRate = $this->style['vatRate'];
        $net_of_tax= $this->style['price'] / (1 + ($this->style['vatRate'] / 100)) ;
		$vat_on_product = $net_of_tax * ($this->style['vatRate'] / 100);
		        
        $this->unitPrice=$this->style['price']-$vat_on_product;
                
        $this->totalPrice = $this->quantity * $this->unitPrice;
        
        // $unitPrice * (1 + $vatRate / 100)
        $this->productPrice = $this->style['price'];
        
        $this->totalProductPrice = $this->productPrice * $this->quantity;

	}
	
	/**
	 * 
	 *Refreshes Item's availability with as latest 
	 */
	public function refreshItemAvailability(){

		$skudetails = AtpApiClient::getAvailableInventoryForSkus($this->sku['id']);
		
		//$skudetails = SkuApiClient::getSkuDetailsWithInvCountsForIds($this->sku['id']);
		$skuDetail = array();
		$skuDetail['id'] = $skudetails[$this->sku['id']]['skuId'];
		//$skuDetail['code'] = $skudetails[$this->sku['id']]['code'];
		//$skuDetail['jitSourced'] = $skudetails[$this->sku['id']]['jitSourced'];
		$skuDetail['availableItems'] = $skudetails[$this->sku['id']]['availableCount'];
		$skuDetail['availableInWarehouses'] = $skudetails[$this->sku['id']]['availableInWarehouses'];
		$skuDetail['supplyType'] = $skudetails[$this->sku['id']]['supplyType'];
		$skuDetail['leadTime'] = $skudetails[$this->sku['id']]['leadTime'];
		
		/* foreach($skudetails as $inv_row) {
			$inv_row['invCount'] = $inv_row['invCount']>0?$inv_row['invCount']:0;
			$skuDetail['availableItems'] += $inv_row['invCount'];
			if(!isset($skuDetail['warehouse2AvailableItems'][$inv_row['warehouseId']])) 
				$skuDetail['warehouse2AvailableItems'][$inv_row['warehouseId']] = 0;
			
			if($inv_row['invCount'] > 0)
				$skuDetail['warehouse2AvailableItems'][$inv_row['warehouseId']] += $inv_row['invCount'];
		} */
		
		$this->updateItemAvailabilityFromArray($skuDetail);
	}
	
	
	/**
	*
	*Update Item's availability with
	*/
	public function updateItemAvailabilityFromArray($skudetail){
		/* $jitSourcingEnabled = FeatureGateKeyValuePairs::getBoolean("jit.sourcing.enabled", false);	
		$checkInStock=FeatureGateKeyValuePairs::getBoolean('checkInStock', true);
		if(!$checkInStock){
			$this->instock=self::$__ITEM_STATE_IN_STOCK;
			$this->sku['availableItems']=self::$__MAX_AVAILABILITY;
			$this->sku['warehouse2AvailableItems']=array('1'=>self::$__MAX_AVAILABILITY, '2'=>self::$__MAX_AVAILABILITY);
			return;
		} */
		
		if($this->style['styleType']==='A'){
			$this->instock=self::$__ITEM_STATE_OUT_OF_STOCK;
			$this->sku['availableItems']=0;
			return;
		}
		
		/* $this->sku['code'] = $skudetail['code'];
		$this->sku['jit_sourced'] = $skudetail['jitSourced'];
		if(!$jitSourcingEnabled)
			$this->sku['jit_sourced'] = 0; 
			
		if($this->sku['jit_sourced'] == 1){
			$this->instock=self::$__ITEM_STATE_IN_STOCK;
			$this->sku['availableItems']=self::$__MAX_AVAILABILITY;
			$this->sku['warehouse2AvailableItems']=array('1'=>self::$__MAX_AVAILABILITY, '2'=>self::$__MAX_AVAILABILITY);
		}else{
			$this->sku['availableItems']= $skudetail['availableItems'];
			$this->sku['warehouse2AvailableItems']= $skudetail['warehouse2AvailableItems'];
		}
		*/
		$this->sku['availableItems']= $skudetail['availableItems'];
		$this->sku['availableInWarehouses']= $skudetail['availableInWarehouses'];
		$this->sku['supplyType']= $skudetail['supplyType'];
		$this->sku['leadTime']= $skudetail['leadTime'];
		
		if($this->sku['availableItems']<=0){
			$this->instock=self::$__ITEM_STATE_OUT_OF_STOCK;
		}else if($this->sku['availableItems']<= $this->getQuantity()){
			$this->instock=self::$__ITEM_STATE_PARTIAL_OUT_OF_STOCK;
		}else{
			$this->instock=self::$__ITEM_STATE_IN_STOCK;
		}
	}

	public function getItemAvailability(){
		return $this->instock;
	}
	
	public function getItemAvailableQuantity(){
		return $this->sku['availableItems'];
	}

	public function equals($mcartItem){
		if($mcartItem==null)
			return false;
		if(empty($mcartItem->style)||empty($mcartItem->sku))
			return true;
			
		if($this->style['id']==$mcartItem->style['id'] && $this->sku['id']==$mcartItem->sku['id']){
			return true;
		}
		return false;
	}

	public function validate(){
		//LIVE-16:Kundan/Yogesh: We no longer validate by size. It is done by SKU-ID alone
		if(empty($this->style['id'])||empty($this->sku['id']))	
			return false;
		return true;
	}
	
	/**
	 * 
	 * Getter for unit vat of item
	 */
 	public function getUnitVAT()
    {
        return $this->productPrice * $this->vatRate / 100;
    }
    
    /**
     * Getter for unit price of item
     */
    public function getUnitPrice(){
    	return $this->unitPrice;
    }
    
    public function setQuantity($quantity){
    	$this->quantity=abs($quantity);
    }
    
    /**
     * update time of last WMS API Call 
     */
	protected function updateApiCallTime(){
		$this->lastApiCall=time();
	}
	
	public function getItemForDiscount()
	{
		if($this->itemAddedByDiscountEngine)
			return null;
                
		$cartItem = array();
		$cartItem['styleId'] = $this->styleid;
		$cartItem['skuId'] = $this->sku['id'];
		$cartItem['styleType'] = $this->styleType;
		$cartItem['quantity'] = $this->quantity;
		$cartItem['productPrice'] = ceil($this->productPrice);
		$cartItem['totalProductPrice'] = $this->totalProductPrice;
        $cartItem['comboId'] = $this->comboId;
                
		return $cartItem;
	}
	
	/*
	 * export object to compute cashback usage at product level 
	 */
	public function getItemForCashbackUsage()
	{
		return $this->getItemForDiscount();
	}
	
	public function getFreeItemForDiscount()
	{
		if($this->itemAddedByDiscountEngine){
	
			$cartItem = array();
			$cartItem['styleId'] = $this->styleid;
			$cartItem['skuId'] = $this->sku['id'];
			$cartItem['styleType'] = $this->styleType;
			$cartItem['quantity'] = $this->quantity;
			$cartItem['productPrice'] = ceil($this->productPrice);
			$cartItem['totalProductPrice'] = $this->totalProductPrice;
			$cartItem['comboId'] = $this->comboId;
			$cartItem['discountRuleId'] = $this->discountRuleId;
			
			return $cartItem;
			}
		else {
			return null;
		}
	}
	
	public function setDiscountFields($discountedCartItem)
	{
		//$this->styleid = $discountedCartItem->styleId;
		$this->styleType = $discountedCartItem->styleType;
		//$this->quantity = $discountedCartItem->quantity;
		//$this->productPrice = $discountedCartItem->productPrice;
		//$this->totalProductPrice = $discountedCartItem->totalProductPrice;
		$this->discountRuleId = $discountedCartItem->discountRuleId;
		$this->discountRuleRevId = $discountedCartItem->discountRuleHistoryId;
		$this->discountQuantity = $discountedCartItem->discountQuantity;
		$this->associatedStyleIds = $discountedCartItem->associatedStyleIDs;
		$this->discountAmount = $discountedCartItem->discountAmount;
		$this->isDiscountRuleApplied = $discountedCartItem->discountRuleApplied;
		$this->isDiscountedProduct = $discountedCartItem->discountedProduct;
		$this->couponApplicable = $discountedCartItem->couponApplicable;
		$this->isReturnable = $discountedCartItem->returnable;
		$this->discountDisplayText = $discountedCartItem->displayText;
		$this->totalDiscountAmount = $discountedCartItem->totalDiscount;
		$this->groupId = $discountedCartItem->groupId;
		$this->discountOnSet = $discountedCartItem->discountOnSet;
		$this->cartDiscount = $discountedCartItem->cartDiscountSplitOnRatio;
        $this->comboId = $discountedCartItem->comboId;
        $this->itemAddedByDiscountEngine = $discountedCartItem->newItemToBeAdded==1?true:false;

        //TODO::Move this piece to service
		if($this->discountDisplayText && $this->discountDisplayText->params && $this->discountDisplayText->params->discountAmount){
        	$this->discountDisplayText->params->discountAmount = round((float)$this->discountDisplayText->params->discountAmount);
        	if($this->discountDisplayText->id == 'FC_Cart_CM_Amount')
        		$this->discountDisplayText->params->discountAmount = $this->quantity * $this->discountDisplayText->params->discountAmount;
        }

	}
	
	/**
	 * returns true is item is added by discount engine
	 * as part of discount policy
	 */
	public function isItemAddedByDiscountEngine(){
		return $this->itemAddedByDiscountEngine;
	}
	
	public function setItemAddedByDiscountEngine($boolean){
		$this->itemAddedByDiscountEngine = ($boolean==1);
	}
	
	/**
	 * Clear product discount
	 */
	public function clearProductDiscount(){
		
		// pass this id to discount engine, to identify the user selected free item in cart
		if( !$this->itemAddedByDiscountEngine){
			$this->discountRuleId = 0;
		}
		
		$this->discountRuleRevId = 0;
		$this->discountQuantity = 0;
		$this->associatedStyleIds = null;
		$this->discountAmount = 0;
		$this->isDiscountRuleApplied = false;
		$this->isDiscountedProduct = false;
		$this->isReturnable = true;
		$this->discountDisplayText = "";
		$this->totalDiscountAmount = 0;
		$this->groupId = 0;
		$this->discountOnSet = false;
		$this->cartDiscount = 0;
		$this->comboId = 0;
	}
	
	public function getCartDiscount()
	{
		return $this->cartDiscount;
	}
	/**
	 * Clears coupon discount from the product
	 */
	public function clearCouponDiscount()
	{
		$this->totalDiscount = 0.0;
	}
	
	public function beforeSerialize(){
	}
	
	public function afterDeSerialize(){
		
	}
	
	public function getItemAddDate(){
		return $this->itemAddDate;
	}
	
	public function toArray(){
		$reflect = new ReflectionClass($this);
		$props   = $reflect->getProperties(ReflectionProperty::IS_PUBLIC | ReflectionProperty::IS_PROTECTED | ReflectionProperty::IS_PRIVATE);
		$arr = array();
		foreach ($props as $prop) {
			if(!$prop->isStatic() && $prop->class == "MCartItem"){
				$p = $prop->getName();
				$arr[$prop->getName()]=$this->$p;
			}
	
		}
		$arr[style]=$this->style;
		
		$arr[styleid]=$this->styleid;
		$arr[sku]=$this->sku;
		$arr[size]=$this->size;
		$arr[lastApiCall]=$this->lastApiCall;
		$arr[instock]=$this->instock;
		
		$arr2= parent::toArray();
		$arr=array_merge($arr,$arr2);
		return $arr;
	}

    public function createArrayFromObj() {
    	$arr = MCartUtils::createArrayFromObj(get_object_vars($this));
    	return $arr;
    }
    
    public function createObjFromArray($productsArray){
    
    	$this->productId = $productsArray["productId"];
    	$this->productTypeId = $productsArray["productTypeId"];
    	$this->productStyleId = $productsArray["productStyleId"];
    	$this->productStyleType = $productsArray["productStyleType"];
    	$this->vatRate = $productsArray["vatRate"];
    	$this->quantity = $productsArray["quantity"];
    	$this->unitPrice = $productsArray["unitPrice"];
    	$this->totalPrice = $productsArray["totalPrice"];
    	$this->productPrice = $productsArray["productPrice"];
    	$this->unitAdditionalCharges = $productsArray["unitAdditionalCharges"];
    	$this->totalProductPrice = $productsArray["totalProductPrice"];
    	$this->totalAdditionalCharges = $productsArray["totalAdditionalCharges"];
    	$this->totalDiscount = $productsArray["totalDiscount"];
    	$this->couponCode = $productsArray["couponCode"];
    	$this->totalCashDiscount = $productsArray["totalCashDiscount"];
    	$this->storeCreditMyntCashUsage = $productsArray["storeCreditMyntCashUsage"];
    	$this->earnedCreditMyntCashUsage = $productsArray["earnedCreditMyntCashUsage"];
    	$this->totalMyntCashUsage = $productsArray["totalMyntCashUsage"];
	$this->discountRuleId = $productsArray["discountRuleId"];
    	$this->comboId = $productsArray["comboId"];
    	$this->cartDiscount = $productsArray["cartDiscount"];
    	$this->styleType = $productsArray["styleType"];
    	$this->discountRuleRevId = $productsArray["discountRuleRevId"];
    	$this->discountQuantity = $productsArray["discountQuantity"];
    	$this->isDiscountRuleApplied = $productsArray["isDiscountRuleApplied"];
    	$this->isDiscountedProduct = $productsArray["isDiscountedProduct"];
    	$this->isReturnable = $productsArray["isReturnable"];
    	$this->totalDiscountAmount = $productsArray["totalDiscountAmount"];
    	$this->discountAmount = $productsArray["discountAmount"];
    	$this->groupId = $productsArray["groupId"];
    	$this->discountOnSet = $productsArray["discountOnSet"];
    	$discountDisplayTextArr = $productsArray["discountDisplayText"];
    	if(!empty($discountDisplayTextArr)){
    		$this->discountDisplayText = MCartUtils::createStdClass($discountDisplayTextArr);
    	}
    	$this->discountLabelText = $productsArray["discountLabelText"];
    	//mcartitem
    	$this->style = $productsArray["style"];
    	$this->styleid = $productsArray["styleid"];
    	$this->sku = $productsArray["sku"];
    	$this->size = $productsArray["size"];
    	$this->lastApiCall = $productsArray["lastApiCall"];
    	$this->instock = $productsArray["instock"];
    	$this->itemAddedByDiscountEngine = $productsArray["itemAddedByDiscountEngine"];
    	$this->isCouponApplicable = $productsArray["isCouponApplicable"];
    	$this->couponApplicable = $productsArray["couponApplicable"];
    	$this->itemAddDate = $productsArray["itemAddDate"];
    	$this->isCustomizable = $productsArray['isCustomizable'] &&
                                FeatureGateKeyValuePairs::getBoolean('personalization.enabled');
     	$this->isCustomized = $productsArray['isCustomized'];
     	$this->customName = $productsArray['customName'];
     	$this->customNumber = $productsArray['customNumber'];
     	$this->customizedMessage = $productsArray['customizedMessage'];


    	$this->loyaltyPointsAwarded = $productsArray["loyaltyPointsAwarded"];
    	$this->loyaltyPointsAwardFactor = $productsArray["loyaltyPointsAwardFactor"];
    	$this->loyaltyPointsConversionFactor = $productsArray["loyaltyPointsConversionFactor"];
    	$this->loyaltyPointsUsed = $productsArray["loyaltyPointsUsed"];
    	$this->packagingType = $productsArray["packagingType"];
    	return $this;
    }

    public function isCustomizable() {
        return $this->isCustomizable;
    }

    public function setCustomizable($isCustomizable) {
        $this->isCustomizable = $isCustomizable;
    }

    public function isCustomized() {
        return $this->isCustomized;
    }

    public function setCustomized($isCustomized) {
        $this->isCustomized = $isCustomized;
    }

    public function getCustomName() {
        return $this->customName;
    }

    public function setCustomName($customName) {
        $this->customName = $customName;
    }

    public function getCustomNumber() {
        return $this->customNumber;
    }

    public function setCustomNumber($customNumber) {
        $this->customNumber = $customNumber;
    }

    public function getCustomizedMessage() {
        return $this->customizedMessage;
    }

    public function setCustomizedMessage($customizedMessage) {
        $this->customizedMessage = $customizedMessage;
    }
    
    public function getLoyaltyPointsConversionFactor(){
    	return $this->loyaltyPointsConversionFactor;
    }
    
    public function isCouponApplicable(){
    	return $this->couponApplicable;
    }
    public function getTotalDiscountAmount(){
    	return $this->totalDiscountAmount;
    }
    public function getItemsAvailabilityArray(){
		$availability = array ();
		if ($this->getItemAvailability () == MCartItem::$__ITEM_STATE_OUT_OF_STOCK) {
			$availability [$this->getProductId ()] ['status'] = MCartItem::$__ITEM_STATE_OUT_OF_STOCK;
			$availability [$this->getProductId ()] ['stock'] = 0;
		}
		$skuArray = $this->getSKUArray ();
		
		if (($skuArray ['availableItems']) <= 0) {
			$availability [$this->getProductId ()] ['status'] = MCartItem::$__ITEM_STATE_OUT_OF_STOCK;
			$availability [$this->getProductId ()] ['stock'] = 0;
			if ($this->getItemAvailability () == MCartItem::$__ITEM_STATE_IN_STOCK || $this->getItemAvailability () == MCartItem::$__ITEM_STATE_PARTIAL_OUT_OF_STOCK) {
				$availability [$this->getProductId ()] ['intotaloos'] = true;
			}
		} else if (($skuArray ['availableItems']) < $this->getQuantity ()) {
			$availability [$this->getProductId ()] ['status'] = MCartItem::$__ITEM_STATE_PARTIAL_OUT_OF_STOCK;
			$availability [$this->getProductId ()] ['stock'] = $skuArray ['availableItems'];
		} else {
			$availability [$this->getProductId ()] ['status'] = MCartItem::$__ITEM_STATE_IN_STOCK;
			$availability [$this->getProductId ()] ['stock'] = $this->getQuantity ();
		}
		return $availability;
	}
        
        public function getPackagingType(){
            return 'NORMAL';
        }
}

class MCartStyleItem extends MCartItem{

	public function __construct($productId,$styleid,$quantity=1){
		$skuid = $this->getSkuID_tmp($styleid);
		parent::__construct($productId,$skuid,$quantity);
	}
	public function getSkuID_tmp($styleid){
        $styleOptions = func_load_all_product_options($styleid);

        $styleAvailableSKUs = array();
        foreach ($styleOptions as $key=>$value) {
            if($value['sku_count'] > 0)
                $styleAvailableSKUs[$value['sku_id']] = $value;
        }
        if(empty($styleAvailableSKUs)) {
            return 0;
        }

        ksort($styleAvailableSKUs);
        return key($styleAvailableSKUs);
	}
}

class MCartNPItem extends MCartItem{


	public function __construct($productId,$skuid,$quantity=1){
		parent::__construct($productId,$skuid,$quantity);
	}
	/**
	 * Tests the equality of type of two cart items,
	 * In case of non personalized items if styleid and skuid are same items are same
	 * @param MCartItem $mcartItem
	 */
	public function equals($mcartItem){
		return parent::equals($mcartItem);
	}
	
	public static function getDefaultSkuID($styleid){
		global $sql_tbl,$weblog;
                global $errorlog;
		$tab_mk_product_style  = $sql_tbl["mk_product_style"];
		$tab_mk_product_options  = $sql_tbl["mk_product_options"];
		$tab_mk_style_properties  = $sql_tbl["style_properties"];
		$styleid = mysql_real_escape_string($styleid);
		$sql = "SELECT sosm.sku_id FROM mk_product_style ps, mk_style_properties sp, mk_styles_options_skus_mapping sosm  WHERE sp.style_id=ps.id and sosm.style_id=ps.id and sp.style_id='$styleid' order by sosm.sku_id limit 1";
		$resultSet=func_query_first($sql);
		if(empty($resultSet)){
			$weblog->error("MCartStyleItem:getSjuID:$styleid:Empty style info");
			$errorlog->error("MCartStyleItem:getSjuID:$styleid:Empty style info");
			return;
		}
		return $resultSet['sku_id'];
	}
}


class MCartPItem extends MCartItem{

	private $custInfo;
	private $custArea;
	public function __construct($productId,$skuid,$quantity=1){
		parent::__construct($productId,$skuid,$quantity);
	}

	public function setCustInfoArray($custInfo){
		$this->custInfo=$custInfo;
	}

	public function getCustInfoArray(){
		return $this->custInfo;

	}
	public function setTotalCustomizationArea($totalCustomizedAreas){
		$this->custArea=$totalCustomizedAreas;
	}
	
	public function getTotalCustomizationArea(){
		return $this->custArea;
	}
	/**
	 * Tests the equality of type of two cart items,
	 * In case of non personalized items if styleid and skuid are same items are same
	 * @param MCartItem $mcartItem
	 */
	public function equals($mcartItem){
		if(!parent::equals($mcartItem))
			return false;
		$diff=self::arrayCompare($mcartItem->custInfo,$this->custInfo,array('area_image_final','image_name','original_image','original_text'));
		
		return $diff;
	}
	
	private static function arrayCompare($aArray1, $aArray2,$ignored_keys=array()) {
		$aReturn = array();

		foreach ($aArray1 as $mKey => $mValue) {
			if(in_array($mKey,$ignored_keys))
			continue;
			if (array_key_exists($mKey, $aArray2)) {
				if (is_array($mValue)) {
						$aRecursiveDiff = self::arrayCompare($mValue, $aArray2[$mKey],$ignored_keys);
					if ($aRecursiveDiff==false) { return false; }
				} else {
					if ($mValue != $aArray2[$mKey]) {
						return false;
					}
				}
			} else {
				return false;
			}
		}

		return true;
	}
}

?>
