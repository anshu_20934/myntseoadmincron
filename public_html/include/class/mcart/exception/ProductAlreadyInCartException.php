<?php
namespace mcart\exception;
use base\exception\BaseRedirectableException;
require_once \HostConfig::$documentRoot."/include/class/mcart/class.MCartItem.php";

class ProductAlreadyInCartException extends BaseRedirectableException {
	
	private $product;
	
	public function __construct(\MCartItem $product, $message = "", $redirectUrl = "") {
		if($product != null) {
			$skuId = $product->getSKUArray();
			$skuId = $skuId['id'];
			$qty = $product->getQuantity();
			$message = "ProductAlreadyInCartException: Product exists in cart with SKU-ID: $skuId with quantity: $qty: ".$message;
			$this->product = $product;
		}
		else {
			$message = "ProductAlreadyInCartException: ".$message;
		}
		parent::__construct($message, $redirectUrl);
	}	

	public function getProduct() {
	    return $this->product;
	}
}