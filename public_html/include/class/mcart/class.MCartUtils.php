<?php
/**
 * MCartUtils
 * @author yogesh
 *
 */
use mcart\ServiceCart;
include_once "$xcart_dir/include/func/func.loginhelper.php";
use enums\cart\CartContext;
require_once \HostConfig::$documentRoot.'/components/curl.php';
class MCartUtils{
	/**
	 * Generates UUID
	 */
	public static function generateUUID(){
		$uuid = mysql_result(mysql_query('Select UUID()'),0); 
		return $uuid;
	}
	
	/**
	 * Handle cart conflict,
	 * By replacing with latest cart
	 * @param MCart $cartLogin
	 * @param MCart $cartTemp
	 */
	public static function handleCartConflictReplaceWithLatestCart($cartLogin,$cartTemp){
		if($cartTemp==null )
			return $cartLogin;
		if($cartLogin==null)
			return $cartTemp;
		if($cartLogin->lastmodified > $cartTemp->lastmodified){
			self::deleteAllCarts($cartLogin->getLogin(), $cartLogin->getContext(),"cookie != '".mysql_real_escape_string($cartLogin->getCartID())."'");
			return $cartLogin;
		}else{
			self::deleteAllCarts($cartLogin->getLogin(), $cartLogin->getContext());
			return $cartTemp;
		}		
	}
	
	public static function deleteCart($mcart, $archiveBeforeDeletion = false){
		if($mcart==null)
			return false;
		$dbAdapter=MCartDBAdapter::getInstance();
		$context = $mcart->getContext();
		$cartidKey = ($context === CartContext::DefaultContext) ? "": $context;
		if($XCART_SESSION_VARS[MCartFactory::$_MCARTID.$cartidKey]==$mcart->getCartID()){
			x_session_unregister(MCartFactory::$_MCARTID.$cartidKey);
		}
        if (isGuestCheckout()) {
            x_session_unregister($mcart->getCartID());
        }
		self::clearSessionCartVariables($mcart->getContext());
		if($archiveBeforeDeletion == true) {
			$dbAdapter->archiveCartInOrder($mcart);
		}
		if(isGuestCheckout() || ($mcart!=null && $mcart instanceof ServiceCart)){
			self::doCartDeleteByContext($mcart->getContext());
		}
		$dbAdapter->deleteCart($mcart);
		
	}
	
	public static function deleteAllCarts($login,$context,$condition=false){
		if(empty($login)||empty($context))
			return false;
		$dbAdapter=MCartDBAdapter::getInstance();
		$dbAdapter->deleteAllCarts($login,$context,$condition);
	
	}
	
	public static function persistCart($mcart){
		if($mcart!=null && $mcart instanceof ServiceCart){
			$mcart->commitOperations();
			global $XCART_SESSION_VARS;
			$cartidKey = $mcart->getContext () === CartContext::DefaultContext ? "" : $mcart->getContext ();
			$XCART_SESSION_VARS ["mcartid" . $cartidKey] = $mcart->getCartID ();
			self::updateSessionCartVariables($mcart,$mcart->getContext());
			return ; 
		}
		if($mcart!=null && $mcart->isChanged()){
			if(count($mcart->getProducts())==0){
				MCartUtils::deleteCart($mcart);
				return;
			}
				
			$dbAdapter=MCartDBAdapter::getInstance();
			switch ($mcart->getContext()){
				case CartContext::DefaultContext:case 'telesales':
					$mcart->expiryDate=time()+MCart::$__DEFAULT_EXPIRY_TIME;
					break;
				case CartContext::SavedContext: 
						$mcart->expiryDate=time()+MCart::$__SAVED_EXPIRY_TIME;
						break;
				default:
					$mcart->expiryDate=time()+MCart::$__DEFAULT_EXPIRY_TIME;
			}
			$dbAdapter->saveOrUpdateMCart($mcart);
		}
	}
	
        public static function getComboDisplayData($mcart, $needStyles = false, $comboId = null)
        {
		      global $sortField,$columns;
                      $returnObj = new stdClass();
                        if($needStyles) {
				
				$queryDone = false;
                                foreach($mcart->getSetDisplayData() as $cid=>$data){
                                        if($cid==$comboId){
                                                //$data->styleIds = DiscountEngine::getComboAssociatedStyleids($comboId);
                                                $solrProducts = new \SolrProducts();
                                                $data->styleIds = $solrProducts->getComboRelatedStyleId($comboId, $columns,$sortField);
                                                $data->allStyleDetails = $data->styleIds;
						$styleIdsArray = array();
                                                for ($i = 0; $i < sizeof($data->styleIds); $i++) {
                                                    $styleIdsArray[$i] = $data->styleIds[$i]["styleid"];
                                                }
                                                
                                                $data->styleIds = $styleIdsArray;
						$queryDone = true;
                                                $returnObj = $data;
                                                break;
                                        }else{
						
                                                $returnObj = null;
                                        }
                                }
                        }else{
                                $returnObj = $mcart->getSetDisplayData();
                        }
			if ($needStyles && !$queryDone){
                           $solrProducts2 = new \SolrProducts();
	  
		           global $styleIdsArray,$styleIdsDetail;            
		           $styleIdsDetail = $solrProducts2->getComboRelatedStyleId($comboId, $columns,$sortField);
		           $styleIdsArray = array();
		           for ($i = 0; $i < sizeof($styleIdsDetail); $i++) {
		                 $styleIdsArray[$i] = $styleIdsDetail[$i]["styleid"];
		           }
                        };

                        return $returnObj;
        }

		public static function getCartDiscount($mcart)
		{
			$mcart->getCartLevelDiscount();
		}

        public static function getCartDisplayData($mcart)
        {
            return $mcart->getDisplayData();
        }
        
        public static function getExcludedArticleTypes($mcart) {
        	return $mcart->excludedArticleTypes;
        }

        public static function getCartSlabDisplayData($mcart)
        {
        	return $mcart->getSlabDisplayData();
        }
        
        public static function convertCartToArray($mcart){
		$productsArray;
		foreach ($mcart->getProducts() as $cartProduct) {
			//var_dump($mcart->getProducts());exit;
			$productsArray[$cartProduct->getProductID()] = self::convertCartItemToArray($cartProduct, $mcart->getContext());
        }
        return $productsArray;
	}
	
	public static function convertCartProductsToArray($products, $context){
		$productsArray;
		foreach ($products as $cartProduct) {
			//var_dump($mcart->getProducts());exit;
			$productsArray[$cartProduct->getProductID()] = self::convertCartItemToArray($cartProduct, $context);
		} 
		return $productsArray;
	}
	
	public static function convertCartItemToArray($cartProduct, $context=CartContext::DefaultContext){
		$styleArray = $cartProduct->getStyleArray();
			$skuArray = $cartProduct->getSKUArray();
			$vatRate = 0.0;
			if($cartProduct->getUnitAdditionalCharges() > 0) {
				$vatCommonRate = \FeatureGateKeyValuePairs::getFloat("vat.commonrate", 0.0);
				$vatRate = min($vatCommonRate, $cartProduct->getVatRate());
			}

			// personalization
			$personalizationEnabled = \FeatureGateKeyValuePairs::getBoolean('personalization.enabled');
			$product =   array(
							'productId'=>$cartProduct->getProductID(),
							'producTypeId' => $styleArray['productTypeId'],
							'productStyleId' => $styleArray['id'],
							'productStyleName' => $styleArray['name'],
							'quantity'=> $cartProduct->getQuantity() ,
							'gender'=>$styleArray['gender'],
							'brand'=>$styleArray['brand'],
							'article_type'=>$styleArray['article_type'],
							'productPrice'=>$cartProduct->getProductPrice(),
                            'unitPrice'=>$cartProduct->getUnitPrice(),
							'totalPrice'=>$cartProduct->getTotalProductPrice(),
							'systemDiscount'=>0,
							'productStyleType'=>$styleArray['styleType'],
							'sizenames'=>array($cartProduct->getSizeName()),
							'sizequantities'=>array($cartProduct->getQuantity()),
							'sizename'=>$cartProduct->getSizeName(),
							'sizequantity'=>$cartProduct->getQuantity(),
							'actualPriceOfProduct' =>$cartProduct->getProductPrice(),
							'operation_location' => 0, //We are not having operation location now
							'vatRate'=>$vatRate,
							'totalAdditionalCharges'=>$cartProduct->getTotalAdditionalCharges(),
                            'unitVat'=>$cartProduct->getUnitVAT(),
                            'is_published'=>0,
                            'is_customizable'=>$cartProduct->isCustomizable(),
                            'isCustomized'=> intval($cartProduct->isCustomized() && $personalizationEnabled),
                            'customizedMessage'=> $personalizationEnabled ? $cartProduct->getCustomizedMessage() : '',
                            'landingpageurl'=>$styleArray['landingpageurl'],
							'cartImagePath'=>$styleArray['cartImagePath'],
							'flattenSizeOptions'=>$skuArray['isflatten'],
							'sizenamesunified'=>array($skuArray['unifiedsize']),
							'sizenameunified'=>$skuArray['unifiedsize'],
							'skuid' => $skuArray['id']
			);
			
			$itemsAvailabilityArray = $cartProduct->getItemsAvailabilityArray();
			$itemsAvailabilityArray = $itemsAvailabilityArray['itemAvailabilityDetailMap'];
			if($itemsAvailabilityArray) {
				$product['availableItems'] = $itemsAvailabilityArray[$cartProduct->getSellerID()]['availableCount'];
				$product['availableInWarehouses'] = $itemsAvailabilityArray[$cartProduct->getSellerID()]['availableInWarehouses'];
				$product['sellerId'] = 	$cartProduct->getSellerID();
				$product['supplyType'] = $itemsAvailabilityArray[$cartProduct->getSellerID()]['supplyType'];
			} else {
				$product['availableItems'] = $skuArray['availableItems'];
				$availableWhs = array_keys($skuArray['warehouse2AvailableItems']);
				if(!empty($availableWhs)) {
					$product['availableInWarehouses'] = $availableWhs;
				}
				$product['sellerId'] = 	1;
				$product['supplyType'] = 'ON_HAND';
			}
			
			if($context == CartContext::GiftCardContext){
				$product['giftCardDetails']=$cartProduct->getGiftCardDetails();
			}
			
            $product['totalProductPrice']=$cartProduct->getTotalProductPrice();
            
            $product['discountAmount'] = $cartProduct->getTotalDiscountAmount();
            $product['discountQuantity'] = $cartProduct->getDiscountQuantity();
            $product['discountRuleRevId'] = $cartProduct->getDiscountRuleRevId();
            $product['packagingType'] = $cartProduct->getPackagingType();
            $product['discountRuleId'] = $cartProduct->getDiscountRuleId();
            $product['isDiscounted'] = $cartProduct->isDiscountedProduct();
            $product['couponApplicable'] = $cartProduct->isCouponApplicable();
            $product['isReturnable'] = $cartProduct->isReturnable();
            $product['cartDiscount'] = $cartProduct->getCartDiscount();
            $product['discountComboId'] = $cartProduct->getComboId();
            $product['isDiscountRuleApplied'] = $cartProduct->isDiscountRuleApplied();
            $product['freeItem'] = $cartProduct->isItemAddedByDiscountEngine();
            
            $couponDiscount = $cartProduct->getDiscount();
	        if ($product['totalProductPrice'] < $couponDiscount) {
                $couponDiscount = $product['totalProductPrice'];
            }
            $product['couponDiscount'] = $couponDiscount;
            $cartProduct->setCouponDiscount($couponDiscount);
            $totalDiscount = $cartProduct->getTotalDiscountAmount() + $cartProduct->getDiscount();
	        if ($product['totalProductPrice'] < $totalDiscount) {
                $totalDiscount = $product['totalProductPrice'];
            }
            $product['discount'] = $totalDiscount;
            $priceafterdiscount=$product['totalProductPrice'] - $totalDiscount;
            $cashdiscount = $cartProduct->getCashDiscount();
            if ($priceafterdiscount < $cashdiscount) {
                $cashdiscount = $priceafterdiscount;
            }
            $cartProduct->setCashCouponDiscount($cashdiscount);
            $product['cashdiscount'] = $cashdiscount;
            $product['discountDisplayText'] = $cartProduct->getDiscountDisplayText();
            
            //MyntCash usage fields   
          
            $product['totalMyntCashUsage'] = $cartProduct->getTotalMyntCashUsage();
            
            $product['loyaltyPointsConversionFactor'] = $cartProduct->getLoyaltyPointsConversionFactor();
            $product['loyaltyPointsUsed'] = $cartProduct->getLoyaltyPointsUsed(); 
            $product['loyaltyPointsAwarded'] = $cartProduct->getLoyaltyPointsAwarded();
            
			return $product;
	}

	public static function genProductId(){
		$len = 4;
		$time = time();
		$timefirstdigit = substr($time, 0, 3);
		$timelastdigit = substr($time, -3);
		// check if generated product id is not unique, then regenerate it
		$res = 1;
		do{
			$productId = $timefirstdigit.get_rand_number($len).$timelastdigit;
			$res = func_query_first_cell("select count(*) from xcart_products where productid=$productId");
		}while(!empty($res));
		return $productId;
	}
	
	
	public static function updateSessionCartVariables($mcart,$context=enums\cart\CartContext::DefaultContext){
		global $XCART_SESSION_VARS;
		self::clearSessionCartVariables($context);
		if($mcart == null){
			self::updateSmartyVariables();
			return;
		}
		
		$cartAmount=$mcart->getMrp()+$mcart->getAdditionalCharges()-$mcart->getDiscount()-$mcart->getDiscountAmount()+$mcart->getShippingCharge()-$mcart->getCartLevelDiscount()+$mcart->getGiftCharge();
		
		if($mcart->getContext()==enums\cart\CartContext::DefaultContext){
			$XCART_SESSION_VARS['CART:totalAmountCart']=$cartAmount;
			$XCART_SESSION_VARS['CART:cartTotalQuantity']=$mcart->getItemQuantity();
			$XCART_SESSION_VARS['CART:totalQuantity']=$mcart->getItemQuantity();
		}else if($mcart->getContext()==enums\cart\CartContext::SavedContext){
			$XCART_SESSION_VARS['CART:savedItemCount']=$mcart->getItemQuantity();
		}
		self::updateSmartyVariables();
	}
	
	public static function fetchAndUpdateSessionCartVariables(){
		$savedItemAction = new saveditems\action\SavedItemAction();
		self::updateSessionCartVariables($savedItemAction->getCart(),enums\cart\CartContext::DefaultContext);
		self::updateSessionCartVariables($savedItemAction->getSaved(),enums\cart\CartContext::SavedContext);
	}
	
	public static function clearSessionCartVariables($context){
		
		global $XCART_SESSION_VARS;
		if(empty($context) || $context==enums\cart\CartContext::DefaultContext){
			unset($XCART_SESSION_VARS['CART:totalAmountCart']);
			unset($XCART_SESSION_VARS['CART:cartTotalQuantity']);
			unset($XCART_SESSION_VARS['CART:totalQuantity']);
		}
		if(empty($context) || $context==enums\cart\CartContext::SavedContext){
			unset($XCART_SESSION_VARS['CART:savedItemCount']);
		}
	}

	public static function clearCartVariables($context, $session_data){
		
		if(empty($context) || $context==enums\cart\CartContext::DefaultContext){
			unset($session_data["CART:totalAmountCart"]);
			unset($session_data["CART:cartTotalQuantity"]);
			unset($session_data["CART:totalQuantity"]);
		}
		return $session_data;
	}
	
	public static function updateSmartyVariables(){
		global $smarty;
	
		$smarty->assign('totalAmountCart',XSessionVars::get("CART:totalAmountCart", 0));
		$smarty->assign('cartTotalQuantity', XSessionVars::get("CART:cartTotalQuantity", 0));
		$smarty->assign('totalQuantity', XSessionVars::get("CART:totalQuantity", 0));
		$smarty->assign('savedItemCount', XSessionVars::get("CART:savedItemCount", 0));
	
	}

	public static function createArrayFromObj($array) {
        foreach($array as $key => $value) {
            if (is_object($value)) {
            	if($value instanceof stdClass){
            		$array[$key] =  MCartUtils::createArrayFromObj(get_object_vars($array[$key]));
            	}
            	else{
            		$array[$key] = $value->createArrayFromObj();
            	}
            }
            if (is_array($value)) {
                $array[$key] = MCartUtils::createArrayFromObj($value);
            }
        }
        return $array;
    } 
    
    
    public function createStdClass($arr){
    	$obj = new stdClass();
    	foreach($arr as $key => $value){
    		if( $key == "params"){
    			$obj->params = (object)$value;
    		}else{
    			$obj->$key  = $value;
    		}
    	}
    	return $obj;
    }
    
    
    public static function doCartGet($context="default",$realtime=false){
    	global $weblog;
    	$weblog->debug("Firing cart get request");
    	$url = HostConfig::$portalAbsolutServiceInternalHost . "/cart/$context";
    	if($realtime===true)
    		$url .= '?rt=true';
    	$ch = curlInit($url, self::getHeadersToSend(), self::getCookiesToSend());
    	list($result, $error, $info) = curlExec($ch);
    	curl_close($ch);
    	if (!empty($error) || empty($result) || $info['http_code'] != 200) {
    		global $errorlog;
    		$errorlog->error("Cart Service Error! $error url: $url status: " . $info['http_code'] . " response: " . strip_tags($result));
    		return null;
    	}
    	$result = json_decode($result,true);
    	return $result[data][0];
    }
    
    public static function doCartDeleteByContext($context="default"){
    	global $weblog;
    	$weblog->debug("Firing cart delete request");
    	$url = HostConfig::$portalAbsolutServiceInternalHost . "/cart/$context";
    	$ch = curlInit($url, self::getHeadersToSend(), self::getCookiesToSend());
    	curlSetVerbDelete($ch);
    	list($result, $error, $info) = curlExec($ch);
    	curl_close($ch);
    	if (!empty($error) || empty($result) || $info['http_code'] != 200) {
    		global $errorlog;
    		$errorlog->error("Cart Service Error! $error url: $url status: " . $info['http_code'] . " response: " . strip_tags($result));
    		return null;
    	}
    	$result = json_decode($result,true);
    	return $result[data][0];
    }
    
    public static function doCartGetById($id){
    	global $weblog;
    	$weblog->debug("Firing cart get request");
    	$url = HostConfig::$portalAbsolutSecuredServiceInternalHost . "/cart/id/$id";
    	$ch = curlInit($url, self::getHeadersToSend(), self::getCookiesToSend());
    	list($result, $error, $info) = curlExec($ch);
    	curl_close($ch);
    	if (!empty($error) || empty($result) || $info['http_code'] != 200) {
    		global $errorlog;
    		$errorlog->error("Cart Service Error! $error url: $url status: " . $info['http_code'] . " response: " . strip_tags($result));
    		return null;
    	}
    	$result = json_decode($result,true);
    	return $result[data][0];
    }
    
    public static function doCartGetByLogin($login,$context = "default"){
    	global $weblog;
    	$weblog->debug("Firing cart get request");
    	$url = HostConfig::$portalAbsolutSecuredServiceInternalHost . "/cart/$context/user/$login";
    	$ch = curlInit($url, self::getHeadersToSend(), self::getCookiesToSend());
    	list($result, $error, $info) = curlExec($ch);
    	curl_close($ch);
    	if (!empty($error) || empty($result) || $info['http_code'] != 200) {
    		global $errorlog;
    		$errorlog->error("Cart Service Error! $error url: $url status: " . $info['http_code'] . " response: " . strip_tags($result));
    		return null;
    	}
    	$result = json_decode($result,true);
    	return $result[data][0];
    }
    
    public static function doCartApplyCoupon($coupon,$context="default"){
    	global $weblog;
    	$weblog->debug("Firing cart coupon apply request");
    	$url = HostConfig::$portalAbsolutServiceInternalHost . "/cart/$context/applymyntcoupon";
    	$ch = curlInit($url, self::getHeadersToSend(), self::getCookiesToSend());
    	curlSetVerbPut($ch, json_encode($coupon));
    	list($result, $error, $info) = curlExec($ch);
    	curl_close($ch);
    	if (!empty($error) || empty($result) || $info['http_code'] != 200) {
    		global $errorlog;
    		$errorlog->error("Cart Service Error! $error url: $url status: " . $info['http_code'] . " response: " . strip_tags($result));
    		return null;
    	}
    	$result = json_decode($result,true);
    	return $result[data][0];
    }
    
    public static function doCartRemoveCoupon($coupon,$context="default"){
    	global $weblog;
    	$weblog->debug("Firing cart coupon remove request");
    	$url = HostConfig::$portalAbsolutServiceInternalHost . "/cart/$context/applymyntcoupon";
    	$ch = curlInit($url, self::getHeadersToSend(), self::getCookiesToSend());
    	curlSetVerbDelete($ch,json_encode($coupon));
    	list($result, $error, $info) = curlExec($ch);
    	curl_close($ch);
    	if (!empty($error) || empty($result) || $info['http_code'] != 200) {
    		global $errorlog;
    		$errorlog->error("Cart Service Error! $error url: $url status: " . $info['http_code'] . " response: " . strip_tags($result));
    		return null;
    	}
    	$result = json_decode($result,true);
    	return $result[data][0];
    }
    
    
    public static function doCartApplyLoyaltyPoints($entry,$context="default"){
    	global $weblog;
    	$weblog->debug("Firing cart loyalty apply request");
    	$url = HostConfig::$portalAbsolutServiceInternalHost . "/cart/$context/applyloyaltypoints";
    	$ch = curlInit($url, self::getHeadersToSend(), self::getCookiesToSend());
    	curlSetVerbPut($ch, json_encode($entry));
    	list($result, $error, $info) = curlExec($ch);
    	curl_close($ch);
    	if (!empty($error) || empty($result) || $info['http_code'] != 200) {
    		global $errorlog;
    		$errorlog->error("Cart Service Error! $error url: $url status: " . $info['http_code'] . " response: " . strip_tags($result));
    		return null;
    	}
    	$result = json_decode($result,true);
    	return $result[data][0];
    }
    
    public static function doCartRemoveLoyaltyPoints($context="default"){
    	global $weblog;
    	$weblog->debug("Firing cart loyalty remove request");
    	$url = HostConfig::$portalAbsolutServiceInternalHost . "/cart/$context/applyloyaltypoints";
    	$ch = curlInit($url, self::getHeadersToSend(), self::getCookiesToSend());
    	curlSetVerbDelete($ch);
    	list($result, $error, $info) = curlExec($ch);
    	curl_close($ch);
    	if (!empty($error) || empty($result) || $info['http_code'] != 200) {
    		global $errorlog;
    		$errorlog->error("Cart Service Error! $error url: $url status: " . $info['http_code'] . " response: " . strip_tags($result));
    		return null;
    	}
    	$result = json_decode($result,true);
    	return $result[data][0];
    }
    
    
    public static function doCartApplyGiftMessage($entry,$context="default"){
    	global $weblog;
    	$weblog->debug("Firing Gift message apply request");
    	$url = HostConfig::$portalAbsolutServiceInternalHost . "/cart/$context/giftwrap";
    	$ch = curlInit($url, self::getHeadersToSend(), self::getCookiesToSend());
    	curlSetVerbPut($ch, json_encode($entry));
    	list($result, $error, $info) = curlExec($ch);
    	curl_close($ch);
    	if (!empty($error) || empty($result) || $info['http_code'] != 200) {
    		global $errorlog;
    		$errorlog->error("Cart Service Error! $error url: $url status: " . $info['http_code'] . " response: " . strip_tags($result));
    		return null;
    	}
    	$result = json_decode($result,true);
    	return $result[data][0];
    }
    
    public static function doCartRemoveGiftMessage($context="default"){
    	global $weblog;
    	$weblog->debug("Firing gift message remove request");
    	$url = HostConfig::$portalAbsolutServiceInternalHost . "/cart/$context/giftwrap";
    	$ch = curlInit($url, self::getHeadersToSend(), self::getCookiesToSend());
    	curlSetVerbDelete($ch);
    	list($result, $error, $info) = curlExec($ch);
    	curl_close($ch);
    	if (!empty($error) || empty($result) || $info['http_code'] != 200) {
    		global $errorlog;
    		$errorlog->error("Cart Service Error! $error url: $url status: " . $info['http_code'] . " response: " . strip_tags($result));
    		return null;
    	}
    	$result = json_decode($result,true);
    	return $result[data][0];
    }
    
    public static function doCartApplyMyntCredits($entry,$context="default"){
    	global $weblog;
    	$weblog->debug("Firing cart loyalty apply request");
    	$url = HostConfig::$portalAbsolutServiceInternalHost . "/cart/$context/applymyntcredit";
    	$ch = curlInit($url, self::getHeadersToSend(), self::getCookiesToSend());
    	curlSetVerbPut($ch, json_encode($entry));
    	list($result, $error, $info) = curlExec($ch);
    	curl_close($ch);
    	if (!empty($error) || empty($result) || $info['http_code'] != 200) {
    		global $errorlog;
    		$errorlog->error("Cart Service Error! $error url: $url status: " . $info['http_code'] . " response: " . strip_tags($result));
    		return null;
    	}
    	$result = json_decode($result,true);
    	return $result[data][0];
    }
    
    public static function doCartRemoveMyntCredits($context="default"){
    	global $weblog;
    	$weblog->debug("Firing cart loyalty remove request");
    	$url = HostConfig::$portalAbsolutServiceInternalHost . "/cart/$context/applymyntcredit";
    	$ch = curlInit($url, self::getHeadersToSend(), self::getCookiesToSend());
    	curlSetVerbDelete($ch);
    	list($result, $error, $info) = curlExec($ch);
    	curl_close($ch);
    	if (!empty($error) || empty($result) || $info['http_code'] != 200) {
    		global $errorlog;
    		$errorlog->error("Cart Service Error! $error url: $url status: " . $info['http_code'] . " response: " . strip_tags($result));
    		return null;
    	}
    	$result = json_decode($result,true);
    	return $result[data][0];
    }
    public static function doCartUpdate($requestData,$context="default"){
    	global $weblog;
    	$weblog->debug("Firing cart get request");
    	$url = HostConfig::$portalAbsolutServiceInternalHost . "/cart/$context/multi";
    	if($realtime===true)
    		$url .= '?rt=true';
    	$ch = curlInit($url, self::getHeadersToSend(), self::getCookiesToSend());
    	curlSetVerbPut($ch, json_encode($requestData));
    	list($result, $error, $info) = curlExec($ch);
    	curl_close($ch);
    	if (!empty($error) || empty($result) || $info['http_code'] != 200) {
    		global $errorlog;
    		$errorlog->error("Cart Service Error! $error url: $url status: " . $info['http_code'] . " response: " . strip_tags($result));
    		return null;
    	}
    	$result = json_decode($result,true);
    	return $result[data][0];
    }
  
    
    public static function doWishListGet(){
    	global $weblog;
    	$weblog->debug("Firing wishlist get request");
    	$url = HostConfig::$portalAbsolutServiceInternalHost . "/wishlist";
    	if($realtime===true)
    		$url .= '?rt=true';
    	$ch = curlInit($url, self::getHeadersToSend(), self::getCookiesToSend());
    	list($result, $error, $info) = curlExec($ch);
    	curl_close($ch);
    	if (!empty($error) || empty($result) || $info['http_code'] != 200) {
    		global $errorlog;
    		$errorlog->error("Cart Service Error! $error url: $url status: " . $info['http_code'] . " response: " . strip_tags($result));
    		return null;
    	}
    	$result = json_decode($result,true);
    	return $result[data][0];
    }
    
    public static function doWishListUpdate($requestData){
    	global $weblog;
    	$weblog->debug("Firing wishlist update request");
    	$url = HostConfig::$portalAbsolutServiceInternalHost . "/wishlist/multi";
    	if($realtime===true)
    		$url .= '?rt=true';
    	$ch = curlInit($url, self::getHeadersToSend(), self::getCookiesToSend());
    	curlSetVerbPut($ch, json_encode($requestData));
    	list($result, $error, $info) = curlExec($ch);
    	curl_close($ch);
    	if (!empty($error) || empty($result) || $info['http_code'] != 200) {
    		global $errorlog;
    		$errorlog->error("Cart Service Error! $error url: $url status: " . $info['http_code'] . " response: " . strip_tags($result));
    		return null;
    	}
    	$result = json_decode($result,true);
    	return $result[data][0];
    }
    
    public static function getCookiesToSend(){
    	$cookieString = '';
    	$cookieNames = array('xid', 'sxid','mab');
    	foreach ($cookieNames as $key) {
    		$name = HostConfig::$cookieprefix . $key;
    		$value = $_COOKIE[$name];
    		if ($cookieString) {
    			$cookieString .= ';';
    		}
    		$cookieString .= $name . '=' . addslashes(rawurlencode($value));
    	}
    	return $cookieString;
    }
    
    public static function getHeadersToSend(){
    	global $XCART_SESSION_VARS;
        $headers = array();
    	$headers[] = 'Content-Type:application/json';
    	$headers[] = 'Accept:application/json';
    	$headers[] = 'X-CSRF-TOKEN:' . $XCART_SESSION_VARS['USER_TOKEN'];
    	return $headers;
    }
    
    
    public static function updateSessionCartVariablesServices($cart,$context=enums\cart\CartContext::DefaultContext){
    	if(empty($mcart)){
    		self::updateSmartyVariables();
    		return;
    	}
    
    	$cartAmount=$cart[totalPriceWithoutMyntCash];
    	$cartQuantity = 0;
    	foreach($cart[cartItemEntries] as $ci){
    		$cartQuantity+=$ci[quantity];
    	}
    
    	if(strtolower($mcart["context"])==enums\cart\CartContext::DefaultContext){
    		$XCART_SESSION_VARS['CART:totalAmountCart']=$cartAmount;
    		$XCART_SESSION_VARS['CART:cartTotalQuantity']=$cartQuantity;
    		$XCART_SESSION_VARS['CART:totalQuantity']=$cartQuantity;
    	}
    	self::updateSmartyVariables();
    }
    
    public static function updateSessionWishlistVariablesServices($cart){
    	
    	if(empty($mcart)){
    		self::updateSmartyVariables();
    		return;
    	}
    
    	$cartAmount=$cart[totalPriceWithoutMyntCash];
    	$cartQuantity = 0;
    	foreach($cart[cartItemEntries] as $ci){
    		$cartQuantity+=$ci[quantity];
    	}
    
    	if(strtolower($mcart["context"])==enums\cart\CartContext::SavedContext){
    		$XCART_SESSION_VARS['CART:savedItemCount']=$cartQuantity;
    	}
    	self::updateSmartyVariables();
    }
    
    public static function fetchAndUpdateSessionCartVariablesServices(){
    	self::clearSessionCartVariables($context);
    	$cart = self::doCartGet();
    	$wishlist = self::doWishListGet();
    	self::updateSessionCartVariablesServices($cart,enums\cart\CartContext::DefaultContext);
    	self::updateSessionWishlistVariablesServices($wishlist);
    }
}

?>
