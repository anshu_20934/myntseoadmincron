<?php
use mcart\ServiceCart;

use giftcard\manager\MCartGiftCard;
use enums\cart\CartContext;
include_once "$xcart_dir/include/dao/class/mcart/class.MCartDBAdapter.php";
/**
 * Factory class to get cart objects
 * @author yogesh
 *
 */
class MCartFactory {
	
	public static $_MCARTID='mcartid';
	private static $CART_CACHE_KEY='cartCacheKey';
	/**
	 * Returns a cart for a login
	 * @param String $login
	 * @param boolean $createnew
	 * @param String $context
	 */
	public function getCartForLogin($login, $createnew = false, $context = CartContext::DefaultContext){
		if(empty($login)){
			return null;
		}
		if($context !== CartContext::GiftCardContext){
			$mcart= new ServiceCart($context,$login);
			return $mcart;
		}
		$dbAdapter = MCartDBAdapter::getInstance();
		$mcart=$dbAdapter->fetchMCart("login='$login' and context='$context' and  expirydate > ".time()." order by last_modified desc");
		if($mcart==null && $createnew){	
			//if no cart exists and $createnew=true create a new cart
			$mcart= $this->createTemporaryCart($context);
			$mcart->setContext($context);
			$mcart->setLogin($login);
			MCartUtils::deleteAllCarts($login, $context);
		}
		return $mcart;		
	}
	
	/**
	 * 
	 * returns cart for user who is owning current Session
	 * @param boolean $createnew
	 * @param String $context
	 */
	public function getCurrentUserCart($createnew = false, $context = CartContext::DefaultContext){
		
	
		if(!empty($GLOBALS[self::$CART_CACHE_KEY][$context])){
			return $GLOBALS[self::$CART_CACHE_KEY][$context];
		}
		
		global $XCART_SESSION_VARS;
		$login = $XCART_SESSION_VARS['login'];
		$mcart;
		$cartidKey = ($context === CartContext::DefaultContext) ? "": $context;
		if($context !== CartContext::GiftCardContext){
			$mcart= new ServiceCart($context);
			$GLOBALS[self::$CART_CACHE_KEY][$context]=$mcart;
			return $mcart;
		}
		if(empty($XCART_SESSION_VARS[self::$_MCARTID.$cartidKey])){
			if(!empty($login)){
				$mcart=$this->getCartForLogin($login,$createnew,$context);
			}
			x_session_unregister(self::$_MCARTID.$cartidKey);
		}else{
			$dbAdapter=MCartDBAdapter::getInstance();
			$tempCart=$dbAdapter->fetchMCart("cookie='".$XCART_SESSION_VARS[self::$_MCARTID.$cartidKey]."' and context='$context' and  expirydate > ".time());
			$mcart=$tempCart;
			if(!empty($login) && ($tempCart == null || $tempCart->getLogin()=="")){
				$perCart=$this->getCartForLogin($login,false,$context);
				if($perCart!=null){
					if($perCart->getCartID()!=$XCART_SESSION_VARS[self::$_MCARTID.$cartidKey]){//conflict
						$mcart=MCartUtils::handleCartConflictReplaceWithLatestCart($perCart, $tempCart);
					}else{
						$mcart=$perCart;
					}
				}else{
					$mcart=$tempCart;
				}
				if($mcart!==null){
					$mcart->setLogin($login);
					MCartUtils::persistCart($mcart);
				}
				
			}
		}
		if($mcart==null && $createnew){
			$mcart= $this->createTemporaryCart($context);
			$mcart->setContext($context);
			$mcart->setLogin($login);
		}
		if($mcart!=null){
			$XCART_SESSION_VARS[self::$_MCARTID.$cartidKey]=$mcart->getCartID();
			$GLOBALS[self::$_MCARTID.$cartidKey]=$mcart->getCartID();
			x_session_register(self::$_MCARTID.$cartidKey,$mcart->getCartID());
			$mcart->setLogin($login);
		}
		$GLOBALS[self::$CART_CACHE_KEY][$context]=$mcart;
		if($mcart==null){
			MCartUtils::updateSessionCartVariables($mcart,$context);
		}
		return $mcart;
	}
	
	private function createTemporaryCart($context){
		if($context === CartContext::GiftCardContext){
			$mcart= new MCartGiftCard();
		}
		else{
			$mcart= new MCart();	
		}
		$mcart->setCartCreateTimestamp(time());
		$mcart->setCartID(MCartUtils::generateUUID());
		$mcart->setContext($context);
		$mcart->setAsSaved();
		return $mcart;
	}
	
	
	
}
?>
