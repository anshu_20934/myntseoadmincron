<?php
namespace mcart;

use enums\cart\CartContext;

class ServiceCartItem extends \MCartItem{

	private $cartItemResponse;
	private $serviceCart;

	public function __construct(ServiceCart $serviceCart,$cartItemResponse){
		$this->serviceCart = $serviceCart;
		$this->cartItemResponse = $cartItemResponse;
	}

	public function getSkuId(){
		return $this->cartItemResponse[skuId];
	}
	public function getType(){
		return \MCart::$__ITEM_NON_PERS;
	}

	public function getStyleId(){
		return $this->cartItemResponse[styleId];
	}

	public function getSellerId(){
		return $this->cartItemResponse[sellerId];
	}
	public function getStyleArray(){
		$style = array();
		$style['id'] = $this->cartItemResponse[styleId];
		$style['name'] = $this->cartItemResponse[title];
		$style['brand'] = $this->cartItemResponse[brand];
		$style['productTypeId'] = $this->cartItemResponse[productTypeId];
		$style['article_type'] = $this->cartItemResponse[articleTyleId];
		$style['styleType'] = $this->cartItemResponse[productStyleType];
		$style['price'] =  $this->cartItemResponse[unitMrp];
		$style['gender']=  $this->cartItemResponse[gender];
		$style['vatRate'] =  $this->cartItemResponse[vatRate];
		$style['is_customizable'] = $this->cartItemResponse[isCustomizable];
		$style['cartImagePath'] =  str_ireplace("_images", "_images_96_128", $this->cartItemResponse[itemImage]);
		$style['landingpageurl'] = $this->cartItemResponse[landingPageUrl];
		return $style;
	}

	public function setStyleArray($array){
	}

	public function getSKUArray(){
		$this->sku= $this->getItemsAvailabilityArray();
		$this->sku[id] = $this->cartItemResponse[skuId];
		$this->sku[unifiedsize] = $this->cartItemResponse[selectedSize][unifiedSize];
		if($this->cartItemResponse[selectedSize][available]!=true){
			$this->sku[availableItems]=0;
		}
		return $this->sku;
	}

	public function getItemAvailability(){
		if($this->cartItemResponse[selectedSize][available]==true){
			return self::$__ITEM_STATE_IN_STOCK;
		}else if($this->getQuantity()>$this->cartItemResponse[selectedSize][availableQuantity]){
			return self::$__ITEM_STATE_PARTIAL_OUT_OF_STOCK;
		}
		return self::$__ITEM_STATE_OUT_OF_STOCK;
	}
	
	public function getItemAvailableQuantity(){
		if($this->cartItemResponse[selectedSize][available]!=true){
			return 0;
		}
		return $this->cartItemResponse[selectedSize][availableQuantity];
	}
	
	public function getSizeName(){
		return $this->cartItemResponse[selectedSize][unifiedSize];
	}

	public function getLoyaltyPointsAwarded(){
		return $this->cartItemResponse[loyaltyPointsAwarded];
	}

	public function getLoyaltyPointsUsed(){
		return $this->cartItemResponse[loyaltyPointsUsed];
	}

	public function setLoyaltyPointsUsed($loyaltyPoints,$conversionFactor){
	}

	public function refreshAwardedLoyaltyPoints($cartNuserLevelAwardFactor=0){
	}

	protected function fetchItemInfo($skuID){
	}

	protected function updateItemInfo($resultSet){
	}

	public function refreshItem($refreshFromDb,$skuResultSet){
	}



	public function equals($mcartItem){
	}

	public function validate(){
		return true;
	}
	public function getUnitVAT()
	{
		return $this->cartItemResponse[vatRate] * $this->cartItemResponse[unitMrp] / 100;
	}

	public function getUnitPrice(){
		return $this->cartItemResponse[unitMrp];
	}

	public function setQuantity($quantity){
	}
	protected function updateApiCallTime(){
	}

	public function getItemForDiscount()
	{
		if($this->cartItemResponse[freeItem])
			return null;

		$cartItem = array();
		$cartItem['styleId'] = $this->getStyleId();
		$cartItem['skuId'] = $this->getSkuId();
		$cartItem['styleType'] = $this->cartItemResponse[styleType];
		$cartItem['quantity'] = $this->getQuantity();
		$cartItem['productPrice'] = ceil($this->getProductPrice());
		$cartItem['totalProductPrice'] = $this->getTotalProductPrice();
		$cartItem['comboId'] = $this->cartItemResponse[comboId];

		return $cartItem;
	}

	public function getFreeItemForDiscount()
	{
		if($this->cartItemResponse[freeItem]){

			$cartItem = array();
			$cartItem['styleId'] = $this->getStyleId();
			$cartItem['skuId'] = $this->getSkuId();
			$cartItem['styleType'] = $this->cartItemResponse[styleType];
			$cartItem['quantity'] = $this->getQuantity();
			$cartItem['productPrice'] = ceil($this->getProductPrice());
			$cartItem['totalProductPrice'] = $this->getTotalProductPrice();
			$cartItem['comboId'] = $this->cartItemResponse[comboId];
			$cartItem['discountRuleId'] = $this->cartItemResponse[discountRuleId];

			return $cartItem;
		}
		else {
			return null;
		}
	}

	public function setDiscountFields($discountedCartItem){
	}

	public function isItemAddedByDiscountEngine(){
		return $this->cartItemResponse[freeItem];
	}

	public function setItemAddedByDiscountEngine($boolean){

	}

	public function clearProductDiscount(){

	}

	public function getCartDiscount()
	{
		return $this->cartItemResponse[totalBagDiscountDecimal];
	}

	public function clearCouponDiscount(){
	}

	public function beforeSerialize(){
	}

	public function afterDeSerialize(){

	}

	public function getItemAddDate(){
	}

	public function toArray(){
	}

	public function createArrayFromObj() {
	}

	public function createObjFromArray($productsArray){
	}

	public function isCustomizable() {
		return $this->cartItemResponse[isCustomizable];
	}

	public function setCustomizable($isCustomizable) {
	}

	public function isCustomized() {
		return $this->isCustomized;
	}

	public function setCustomized($isCustomized) {
		$this->serviceCart->setItemCustomizationEnabled($this->getProductId(), $isCustomized);
	}

	public function getCustomName() {
		return $this->cartItemResponse[customName];
	}

	public function setCustomName($customName) {
		$this->serviceCart->setItemCustomizationName($this->getProductId(), $customName);
	}

	public function getCustomNumber() {
		return $this->cartItemResponse[customNumber];
	}

	public function setCustomNumber($customNumber) {
		$this->serviceCart->setItemCustomizationNumber($this->getProductId(), $customNumber);
	}

	public function getCustomizedMessage() {
		return $this->cartItemResponse[customizedMessage];
	}

	public function setCustomizedMessage($customizedMessage) {
		$this->serviceCart->setItemCustomizationMessage($this->getProductId(), $customizedMessage);
	}
	
	public function getProductId()
	{
		return $this->cartItemResponse[itemId];
	}

	public function getProductTypeId()
	{
		return $this->cartItemResponse[productTypeId];
	}
	
	public function getProductStyleId()
	{
		return  $this->cartItemResponse[styleId];
	}
	
	public function getQuantity()
	{
		return $this->cartItemResponse[quantity];
	}
	
	public function getTotalPrice()
	{
		return  $this->cartItemResponse[totalPriceDecimal];
	}
	
	/**
	 * Getter for the unit price, inclusive of VAT.
	 */
	public function getProductPrice()
	{
		return  $this->cartItemResponse[unitMrp];
	}
	
	public function getUnitAdditionalCharges()
	{
		return  $this->cartItemResponse[unitAdditionalCharge];
	}

	public function setUnitAdditionalCharges($charges) {}
	
	public function getTotalAdditionalCharges()
	{
		return  $this->getUnitAdditionalCharges()* $this->getQuantity();
	}
	
	public function setTotalAdditionalCharges($charges) {}
	

	public function getTotalProductPrice()
	{
		return  $this->cartItemResponse[totalMrp];
	}
	
	public function getVAT()
	{
		return $this->getTotalProductPrice()*$this->getVatRate();
	}
	
	public function getVatRate()
	{
		return $this->cartItemResponse[vatRate];
	}
	
	public function getDiscountAmount(){
		return $this->cartItemResponse[unitDiscount];
	}
	

	public function getTotalDiscountAmount(){
		return $this->cartItemResponse[totalDiscountDecimal];
	}
	
	public function setDiscountAmount($amount)
	{}
	
	public function getDiscount()
	{
		return $this->cartItemResponse[totalCouponDiscountDecimal];
	}
	
	public function getCouponCode()
	{}
	/**
	 * Getter for cash discount.
	 * @deprecated use getTotalMyntCashUsage
	 */
	public function getCashDiscount()
	{
		return $this->cartItemResponse[totalMyntCashUsageDecimal];
	}
	
	public function getTotalMyntCashUsage()
	{
		return $this->cartItemResponse[totalMyntCashUsageDecimal];
	}

	public function setMyntCashUsage($totalMyntCashUsage)
	{}
	
	public function getCouponDiscount()
	{
		return $this->cartItemResponse[totalCouponDiscountDecimal];
	}
	public function getCashCouponCode()
	{}
	public function setCouponDiscount($discount)
	{}
	public function setCouponCode($couponCode)
	{}
	public function setCashCouponDiscount($discount)
	{}

	public function setCashCouponCode($couponCode)
	{}

	public function clearDiscount()
	{}
	
	public function clearCashDiscount()
	{}
	
	
	public function mergeProduct($product)
	{}
	
	public function getDiscountQuantity(){
		return $this->cartItemResponse[discountQuantity];
	}
	
	public function getDiscountRuleRevId(){
		return $this->cartItemResponse[discountRuleHistoryId];
	}
        
        public function getPackagingType(){
            return $this->cartItemResponse[packagingType];
        }
	
	public function isDiscountedProduct(){
		return $this->cartItemResponse[isReturnable];
	}
	
	public function getDiscountRuleId(){
		return $this->cartItemResponse[discountRuleId];
	}

	
	public function isDiscountRuleApplied(){
		return $this->cartItemResponse[isDiscountRuleApplied];
	}
	public function isReturnable(){
		return $this->cartItemResponse[isReturnable];
	}
	
	public function getComboId(){
		return $this->cartItemResponse[comboId];	
	}
	
	public function isCouponApplicable(){
		return $this->cartItemResponse[couponApplicable];
	}
	
	public function getDiscountDisplayText(){
		return ServiceCart::convertToDisplayData($this->cartItemResponse[discountDisplayText][rawDiscountDataEntry]);
	}
	
	public function getDiscountData(){
		return ServiceCart::convertToDisplayData($this->cartItemResponse[displayData][rawDiscountDataEntry]);
	}
	
	public function getItemsAvailabilityArray(){
		$availability = array ();
		
		if ($this->cartItemResponse [selectedSize] [available] == true) {
			$availability ['status'] == \MCartItem::$__ITEM_STATE_IN_STOCK;
			$availability ['stock'] = $this->cartItemResponse  [selectedSize] [availableQuantity];
		} elseif ($this->cartItemResponse [selectedSize] [available] == false && $this->cartItemResponse [selectedSize] [availableQuantity] < $this->cartItemResponse [selectedSize] [quantity]) {
			$availability ['status'] == \MCartItem::$__ITEM_STATE_PARTIAL_OUT_OF_STOCK;
			$availability ['stock'] = $this->cartItemResponse  [selectedSize] [availableQuantity];
		} else {
			$availability ['status'] = \MCartItem::$__ITEM_STATE_OUT_OF_STOCK;
			$availability ['stock'] = 0;
		}
		$availability ['itemAvailabilityDetailMap'] = $this->cartItemResponse [selectedSize] [itemAvailabilityDetailMap];
		$availability[availableItems]=$availability ['stock'];
		return $availability;
	}
}