<?php
use abtest\MABTest;

use enums\cart\CartContext;

global $xcart_dir;
include_once ("$xcart_dir/modules/cart/Cart.php");
require_once ($xcart_dir."/include/class/search/SearchProducts.php");
include_once "$xcart_dir/include/class/mcart/class.MCartFactory.php";
include_once "$xcart_dir/include/class/mcart/class.MCartItem.php";
include_once "$xcart_dir/include/class/mcart/class.MCartUtils.php";
include_once "$xcart_dir/include/func/func.mkcore.php";
include_once ($xcart_dir."/include/class/class.feature_gate.keyvaluepair.php");
include_once "$xcart_dir/modules/coupon/database/CouponAdapter.php";
include_once "$xcart_dir/modules/coupon/CouponValidator.php";
include_once "$xcart_dir/modules/discount/DiscountEngine.php";
include_once "$xcart_dir/modules/coupon/CouponDiscountCalculator.php";
include_once "$xcart_dir/modules/coupon/CashCouponCalculator.php";
include_once "$xcart_dir/modules/myntCash/MyntCashCalculator.php";
include_once "$xcart_dir/modules/myntCash/MyntCashService.php";
include_once "$xcart_dir/modules/RestAPI/CouponActions.php";

use enums\base\ActionType;
use mcart\exception\ProductAlreadyInCartException;
use saveditems\dataobject\SavedItem;
use giftcard\manager\MCartGiftCardItem;

include_once("$xcart_dir/modules/apiclient/ServiceabilityApiClient.php");
include_once("$xcart_dir/include/func/func.courier.php");
include_once "$xcart_dir/modules/loyaltyPoints/LoyaltyPointsPortalClient.php";
include_once "$xcart_dir/modules/loyaltyPoints/LoyaltyPointsHelper.php";
include_once "$xcart_dir/modules/vat/VatCalculator.php";

/**
 * MCart
 * @author yogesh
 *
 */
class MCart extends Cart{
	
	//Type Non personalized
	public static $__ITEM_NON_PERS=0;
	//Type Personalized
	public static $__ITEM_PERS=1;
	//Type Gift Card
	public static $__ITEM_GC=2;
	
	public static $__DEFAULT_EXPIRY_TIME=18144000;//30 days
	public static $__ONECLICKBUY_EXPIRY_TIME=600;//10 mins
	public static $__SAVED_EXPIRY_TIME=31536000;//365 days
	public static $__COUPON_TYPE_DEFAULT=0;
	
	public static $__COUPON_TYPE_CASHBACK=1;
	
	public static $__COUPON_APP_CODE_SUCCESS=1;
	
	public static $__COUPON_APP_CODE_FAILED=2;
	
	public static $__DISCOUNT_ENGINE_FEATUREGATE_KEY="discountEngine.change.revisionNumber";
	
	private $cartCreateTimestamp;
	
	private $cartIsNew = false;
	
	private $changed=false;
	private $cartID="";
	private $orderID="";
	private $login="";
	private $context;
	private $coupons;
	private $lastContentUpdated;
	public  $lastmodified;
	public  $expiryDate; 
		
	//Last availibility api call time
	private $availApiCall=0;
	
	protected  $nonDiscountedAmount = 0;
	protected $nonDiscountedItemCount = 0;
	
	protected $couponAppliedItemCount=0;
	protected $couponAppliedSubTotal=0;
	
	//Adding two more variables for coupon applicability
	protected $couponApplicableItemCount=0;
	protected $couponApplicableAmount=0;
	
        protected $couponApplicableCartItems;
        protected $couponNotApplicableCartItems;
        protected $couponApplicabilityMessage;
	
	protected $shippingCharge = 0;
	protected $shippingMethod = "NORMAL";

	protected $shippingDiscountText;
	
	protected $giftCharge = 0;
	protected $giftMessage;
	
	protected $isGiftOrder = false;
	
	
	protected $bagDiscount = 0;
	protected $dreRev = 0;

	//slab discount display data
	public $slabDisplayData=null;
	
	
	/* TotalMyntCashUsage */
	
	protected $useMyntCash = true;
	
	protected $autoApplyCoupon = true;
	
	protected $userEnteredMyntCashAmount = 0.0;
	
	/*
	 * MyntCash usage
	*/
	protected $giftOrShippingMyntCashUsage = 0.0;
	protected $totalMyntCashUsage = 0.0;
	
	/*
	 * Loyalty Points
	 */
	protected $useLoyaltyPoints = false;
	protected $loyaltyPointsAwarded = 0;
	protected $loyaltyPointsUsed = 0;
	protected $userEnteredLoyaltyPointsToUse = 0;
	protected $loyaltyPointsConversionFactor = 0;
	protected $giftOrShippingLoyaltyPointsUsage = 0;
	protected $loyatltyPointsUserSpecificAwardFactor = 0;
	protected $loyaltyPointsCartSpecificAwardFactor = 1;
	protected $loyaltyPointsUsedCashEqualent = 0.0;
	
		
	
	public function __construct(){
	 	parent::__construct(array());
	}
	public function isChanged(){
		return $this->changed;
	}
	public function setCartID($id){
		$this->cartID=$id;
	}
	public function getCartID(){
		return $this->cartID;
	}
	public function setOrderID($id){
		$this->orderID=$id;
	}
	public function getOrderID(){
		return $this->orderID;
	} 
	public function setAsSaved(){
		$this->changed=false;
	}
	public function setAsChanged(){
		$this->changed=true;
	}
	public function setLogin($login){
		if(!empty($login) && $this->login!=$login){
			$this->login=$login;
			$this->changed=true;
		}
	}
	public function getLogin(){
		return $this->login;
	}
	public function setContext($context){
		if($this->context!=$context){
			$this->context=$context;
			$this->changed=true;
		}
	}
	public function getContext(){
		return $this->context;
	}
	
	public function getLastContentUpdatedTime(){
		return $this->lastContentUpdated;
	}
		
	public function setLastContentUpdatedTime($time){
		$this->lastContentUpdated = $time;
		$this->changed=true;
	}
	
	public function getNonDiscountedAmount(){
		return $this->nonDiscountedAmount;
	}
	
	public function getNonDiscountedItemCount(){
		return $this->nonDiscountedItemCount;
	}

	public function getCouponApplicableItemCount(){
		return $this->couponApplicableItemCount;
	}
	
	public function getCouponAppliedItemCount(){
		return $this->couponAppliedItemCount;
	}
	
	public function setCouponAppliedItemCount($itemCount){
		 $this->couponAppliedItemCount=$itemCount;
	}
	
	public function getCouponAppliedSubtotal(){
		return $this->couponAppliedSubTotal;
	}
	
	public function setCouponAppliedSubtotal($appliedSubtotal){
		 $this->couponAppliedSubTotal = $appliedSubtotal;
	}
	
        public function getCouponApplicableCartItems() {
                return $this->couponApplicableCartItems;
        }
        
        public function setCouponApplicableCartItems($applicableProducts) {
                $this->couponApplicableCartItems = $applicableProducts;
        }
	
        public function getCouponNotApplicableCartItems() {
                return $this->couponNotApplicableCartItems;
        }
        
        public function setCouponApplicabilityMessage($couponApplicabilityMessage) {
                $this->couponApplicabilityMessage = $couponApplicabilityMessage;
        }
        
        public function getCouponApplicabilityMessage() {
                return $this->couponApplicabilityMessage;
        }
        
        public function setCouponNotApplicableCartItems($notApplicableProducts) {
                $this->couponNotApplicableCartItems = $notApplicableProducts;
        }
        
        
	public function refresh($refreshFromDb=false,$forcePriceRecalculation=false,$forceProductDiscountRecalculation=false,$forceItemAvailabilityRefresh=false){
		
		if((time()-$this->dreRev) > (WidgetKeyValuePairs::getWidgetValueForKey(self::$__DISCOUNT_ENGINE_FEATUREGATE_KEY)*60) ){
			//TODO change this logic to revision number after discount engine integrations
			$forceProductDiscountRecalculation=true;
		}
		
		if($refreshFromDb || $forcePriceRecalculation || $forceProductDiscountRecalculation){
			$this->noOfItems=0;
			$this->price=0.0;
			$this->mrp=0.0;
			$this->additionalCharges=0.0;
			$this->vat=0.0;
		
			// Get information from db in bulk if $referesh from DB is true
			if($this->getProducts()){
				$resultSet = $this->getItemsInfo();
			}
			
			foreach ($this->getProducts() as $product) {
				$product->refreshItem("false",$resultSet[$product->getSkuId()]);
				$productId = $product->getProductId();
				$quantity = $product->getQuantity();
	    
				$this->noOfItems += $quantity;
				$this->price += $product->getTotalPrice();
				$this->mrp += $product->getTotalProductPrice();
				$this->additionalCharges += $product->getTotalAdditionalCharges();
				$this->vat += $product->getVAT();
				$this->productsArray[$productId] = $product;
				$this->productQtyArray[$productId] = $quantity;
				$styleArray = $product->getStyleArray();
				$product->setCustomizable((bool)$styleArray['is_customizable'] &&
				                            FeatureGateKeyValuePairs::getBoolean('personalization.enabled'));
			}
			
			//$this->setShippingCharge(); // unecessary call for setShippingCharge
			
			$this->reCalculatePricesAndDiscounts($forceProductDiscountRecalculation);
			$this->changed=true;
		}
		
		//to refresh the nondiscounted item count and ampount fields
		$this->getNonDiscountedItemsSubtotal();
		
		$this->refreshItemsAvailability($forceItemAvailabilityRefresh);
		MCartUtils::updateSessionCartVariables($this,$this->getContext());
	}
	
	protected function getItemsInfo(){
			// Get all the skuids associated with this cart
			$skuIdList = array();
			foreach ($this->getProducts() as $product) {
				array_push($skuIdList, $product->getSkuId()) ;
			}

			$commaSeparatedSkuIds = implode(',', $skuIdList);

			$sql="SELECT ps.product_type producttypeid,ps.name stylename,ps.is_customizable,ps.styleType,
			ps.label,ps.price, sp.global_attr_gender gender,sp.global_attr_brand brand, sp.global_attr_article_type article_type, pt.product_type_groupid producttypegroupid,pt.name producttypename, pt.vat,sp.default_image,
			sosm.sku_id skuid, po.id as optionid, ps.id as styleid, po.value as sizevalue      
			FROM mk_product_style ps, mk_style_properties sp, mk_product_type pt,mk_product_options po,mk_styles_options_skus_mapping sosm 
			WHERE ps.product_type=pt.id AND sp.style_id=ps.id and sosm.sku_id in (".$commaSeparatedSkuIds.") and po.style=ps.id and sosm.style_id=ps.id and sosm.option_id =po.id";// and po.value='".($this->size)."'

			$resultSet=func_query($sql);
			
			$outResult = array();
			foreach ($resultSet as  $tempResult) {
				$outResult[$tempResult['skuid']] = $tempResult;
			}
		
			return $outResult;
	}

	/**
	 * Refresh the availability of items
	 * @param boolean $forced if true will not use cache availibility data
	 */
	public function refreshItemsAvailability($forced = false){	
		if(!$forced){
			$theshold=FeatureGateKeyValuePairs::getInteger('cart.apiCalls.Threshold');
			if((time()-$this->availApiCall) < ($theshold*60)){
				return;
			}
		}
		$skuIds;
		foreach ($this->getProducts() as $product) {
			$skuIds[]=$product->getSkuId();
		}
		
		//$sku_details_with_availablecounts = SkuApiClient::getSkuDetailsWithInvCountsForIds($skuIds);
		$sku_details_with_availablecounts = AtpApiClient::getAvailableInventoryForSkus($skuIds);
		
		$skuDetails = array();
		/* foreach($sku_details_with_availablecounts as $inv_row) {
			//$sku2details_mapping[$row['id']][] = $row;
			if(!isset($skuDetails[$inv_row['id']])) {
				$skuDetails[$inv_row['id']] = array();
				$skuDetails[$inv_row['id']]['code'] = $inv_row['code'];
				$skuDetails[$inv_row['id']]['jitSourced'] = $inv_row['jitSourced'];
				$skuDetails[$inv_row['id']]['warehouse2AvailableItems'] = array();
				$skuDetails[$inv_row['id']]['availableItems'] = 0;
			}
			
			$inv_row['invCount'] = $inv_row['invCount']>0?$inv_row['invCount']:0;
			$skuDetails[$inv_row['id']]['availableItems'] += $inv_row['invCount'];
			
			if(!isset($skuDetails[$inv_row['id']]['warehouse2AvailableItems'][$inv_row['warehouseId']]))
				$skuDetails[$inv_row['id']]['warehouse2AvailableItems'][$inv_row['warehouseId']] = 0;
			
			if($inv_row['invCount'] > 0)
				$skuDetails[$inv_row['id']]['warehouse2AvailableItems'][$inv_row['warehouseId']] += $inv_row['invCount'];
			
		} */
		foreach($sku_details_with_availablecounts as $skuId => $sku) {
			$skuDetail = array();
			$skuDetail['id'] = $skuId;
			//$skuDetail['code'] = $skudetails[$this->sku['id']]['code'];
			//$skuDetail['jitSourced'] = $skudetails[$this->sku['id']]['jitSourced'];
			$skuDetail['availableItems'] = $sku['availableCount'];
			$skuDetail['availableInWarehouses'] = $sku['availableInWarehouses'];
			$skuDetail['supplyType'] = $sku['supplyType'];
			$skuDetail['leadTime'] = $sku['leadTime'];
			
			$skuDetails[$skuId] = $skuDetail; 
		}
		
		foreach ($this->getProducts() as $product) {
			$product->updateItemAvailabilityFromArray($skuDetails[$product->getSkuId()]);			 
		}
		
		$this->availApiCall=time();
		$this->changed=true;
	}
	
	public function removeItem($product,$callRefresh=true){
		if($product!=null){
			$productId = $product->getProductId();
			if (!empty($this->productsArray[$productId])){
					
				$quantity = $product->getQuantity();

				$this->noOfItems -= $quantity;
				$this->price -= $product->getTotalPrice();
				$this->mrp -= $product->getTotalProductPrice();
				$this->additionalCharges -= $product->getTotalAdditionalCharges();
				$this->vat -= $product->getVAT();
				$this->discount -= $product->getDiscount();
				$this->cashdiscount -= $product->getCashDiscount();

				$this->productsArray[$productId] = $product;
				$this->productQtyArray[$productId] = $quantity;
				unset($this->productsArray[$productId]);
				unset($this->productQtyArray[$productId]);
				$this->changed=true;
				$this->setLastContentUpdatedTime(time());
				if($callRefresh==true){
					$this->refresh(false,true,true);
				}
			}
		}

	}
	
	public function updateItem($cartitem,$skuId,$qty){
		$product = $myCart->getProduct($cartitem);
		if($product!=null){
			$myCart->removeItem($product);
		}
		
		// add as new item
		$mcartItem = new MCartNPItem($cartitem, $skuId, $qty);
		if($product!=null){
			$mcartItem->itemAddDate = $product->itemAddDate;
		}
		$msg = '';
		try {
			$myCart->addProduct($mcartItem, true);
		} catch (ProductAlreadyInCartException $e) {
			$msg = "Item is already added to the bag";
		}
	}
	
	public function addProduct($newProduct, $toMerge = false , $callRefresh=true){
		if(!$newProduct->validate()){
			return false;
		}
		$add = true;
		foreach ($this->getProducts() as $product) {
			if($product->equals($newProduct)) {
				if(!$toMerge) {
					throw new ProductAlreadyInCartException($product);
				}
				else {
					$product->mergeProduct($newProduct);
					$add = false;
				}
				$add = false;
			}
		}
		if($add) {
			parent::addProduct($newProduct);
		}	
		$this->setLastContentUpdatedTime(time());
		if($callRefresh==true){
			$this->refresh(false,true,true);
		}
		$this->changed=true;
	}
	public function addProductToShortList($styleId){
		parent::addProductToShortList($styleId);
		$this->changed=true;
	}
	public function getShippingCharge() {
    	return $this->shippingCharge;
    }
	
	public function setShippingCharge($loyaltyAcct=null) {
        global $XCART_SESSION_VARS;
		$subTotal = $this->mrp+$this->additionalCharges-$this->getTotalProductItemDiscount();
    	$this->shippingCharge = func_calculate_shipping_charges($subTotal);
    	
    	$loyaltyFreeShippingFG = FeatureGateKeyValuePairs::getBoolean('freeShippingForLoyalty',false);
    	$doFreeShippingForFirstOrder = FeatureGateKeyValuePairs::getBoolean('shipping.charges.free.firstorder',false);
        $isFirstOrder = !$XCART_SESSION_VARS['returningCustomer'];
    	
    	if($this->shippingCharge > 0){

            if ($doFreeShippingForFirstOrder && $isFirstOrder) {
                $this->shippingCharge = 0;
    			$this->shippingDiscountText["id"]="Free_Shipping_FirstOrder_Message";
    			$this->shippingDiscountText["params"]["message"]="firstOrderFreeShipping";
            } else if($loyaltyFreeShippingFG && $loyaltyAcct != null && isset($loyaltyAcct["tier"]) && $loyaltyAcct["tier"] == 3){	
    			$this->shippingCharge = 0;	
    			$this->shippingDiscountText["id"]="Free_Shipping_Message";
    			$this->shippingDiscountText["params"]["message"]="loyaltyFreeShipping";
    		}else{
    			$system_free_shipping_amount = WidgetKeyValuePairs::getWidgetValueForKey('shipping.charges.cartlimit');
    			$this->shippingDiscountText["id"]="Free_Shipping_Condition";
    			$this->shippingDiscountText["params"]["minMore"]=$system_free_shipping_amount-$subTotal;
    		}
    	}else{
    		$this->shippingDiscountText = null;
    	}
    }
    public function getShippingMethod() {
    	return $this->shippingMethod;
    }

    /** Discount amount in cart.php refers to the 
    * total of system discount on all products.
    */
    
    public function getTotalProductItemDiscount(){
    	$discountAmount = 0.0;
    	foreach ($this->getProducts() as $product){
    		$discountAmount += $product->totalDiscountAmount;
    	}
    	return $discountAmount;
    }
	
	public function updateItemQuantity($product,$quantity){
		if (!empty($this->productsArray[$product->getProductId()])){
			if($quantity<=0){
				$this->removeItem($product);
			}else{
				if($quantity==$product->getQuantity())
					return;
				$product->setQuantity($quantity);
				$this->productQtyArray[$product->getProductId()]=$quantity;
			}
			$this->changed=true;
			$this->setLastContentUpdatedTime(time());
			$this->refresh(false,true,true,true);
		}
	}
	
	/**
	 * Given a SKU Id, add/change quantity
	 * add: the quantity is added to existing quantity
	 * update: the quantity is repaced with existing quantity
	 * @param int skuId
	 * @param int $quantityToAddOrUpdate
	 * @param string $mode : add or update
	 */
	public function addOrUpdateItemQuantityBasedOnSkuId($skuId, $quantityToAddOrUpdate = 1, $mode = ActionType::Update) {
		
		foreach ($this->productsArray as $eachProduct) {
			$currSkuId = $eachProduct->getSKUArray();
			$currSkuId = $skuId['id'];
			if($skuId === $currSkuId) {
				$qtySelected = $eachProduct->getQuantity();
				
				if($mode === ActionType::Add) {
					if($qtySelected < 0)
						$qtySelected = 0;
					$qtySelected += $quantityToAdd;
				} elseif ($mode === ActionType::Update) {
					if($quantityToAddOrUpdate <= 0) {
						$this->removeItem($eachProduct);
					} else {
						$qtySelected = $quantityToAddOrUpdate;
					}
				} else {
					global $errlog;
					$errlog->error(__FILE__.":".__FUNCTION__." was called with mode other than add or update.. this is not allowed");
					throw new MyntraException(__FILE__.":".__FUNCTION__." was called with mode other than add or update.. this is not allowed");
				}
				
				$eachProduct->setQuantity = $qtySelected;
				$this->productQtyArray[$eachProduct->getProductId()] = $qtySelected;
				$this->changed = true;
				$this->setLastContentUpdatedTime(time());
				break;
			}
		}
		
		if (!empty($this->productsArray[$product->getProductId()])){
			if($quantity<=0){
				$this->removeItem($product);
			}else{
				$product->setQuantity($quantity+$product->getQuantity());
				$this->productQtyArray[$product->getProductId()] = $quantity+$product->getQuantity();
			}
			$this->changed=true;
			$this->setLastContentUpdatedTime(time());
		}
	}
	
	public function isReadyForCheckout(){
		if(count($this->getProducts())==0)
			return false;
		$carryOverQty;
		foreach($this->getProducts() as $product){
			if($product->getItemAvailability()==MCartItem::$__ITEM_STATE_OUT_OF_STOCK){
				return false;
			}
			$skuArray=$product->getSKUArray();
			$carryOverQty[$skuArray['id']]+=$product->getQuantity();
			if($carryOverQty[$skuArray['id']]>$skuArray['availableItems'])
				return false;			
		}
		return true;
	}
	
	public function getStyleIds() {
		$styleIds = array();
		foreach ($this->productsArray as $eachProduct) {
			$styleIds[] = $eachProduct->getStyleId();
		}
		return $styleIds;
	}
	
	public function getItemsAvailabilityArray(){
		$availability=array();
		$carryOverQty;
		foreach($this->getProducts() as $product){
			if($product->getItemAvailability()==MCartItem::$__ITEM_STATE_OUT_OF_STOCK){
				$availability[$product->getProductId()]['status']=MCartItem::$__ITEM_STATE_OUT_OF_STOCK;
				$availability[$product->getProductId()]['stock']=0;
			}
			$skuArray=$product->getSKUArray();
				
			if(($skuArray['availableItems']-$carryOverQty[$skuArray[id]])<=0){
				$availability[$product->getProductId()]['status']=MCartItem::$__ITEM_STATE_OUT_OF_STOCK;
				$availability[$product->getProductId()]['stock']=0;
				if($product->getItemAvailability()==MCartItem::$__ITEM_STATE_IN_STOCK ||$product->getItemAvailability()==MCartItem::$__ITEM_STATE_PARTIAL_OUT_OF_STOCK ){
					$availability[$product->getProductId()]['intotaloos']=true;
				}
			}else if(($skuArray['availableItems']-$carryOverQty[$skuArray[id]])<$product->getQuantity()){
				$availability[$product->getProductId()]['status']=MCartItem::$__ITEM_STATE_PARTIAL_OUT_OF_STOCK;
				$availability[$product->getProductId()]['stock']=$skuArray['availableItems']-$carryOverQty[$skuArray[id]];
			}else{
				$availability[$product->getProductId()]['status']=MCartItem::$__ITEM_STATE_IN_STOCK;
				$availability[$product->getProductId()]['stock']=$product->getQuantity()+$carryOverQty[$skuArray[id]];
			}		
			$carryOverQty[$skuArray[id]]+=$product->getQuantity();	
		}
		return $availability;
	}
	
	public function removeInvalidItems(){
		foreach($this->getProducts() as $product){
			if(!$product->validate()){
				$this->removeItem($product);
			}
		}
	}
	
	public function getProductIdsInCart(){
		$productids=array();
		foreach($this->getProducts() as $product){
			$productids[]=$product->getProductId();
		}
		return $productids;
	}
	
	/**
	 * Adds a coupon code.
	 * Calling this function would not apply the coupon to cart.
	 * It just stores coupon with cart  ,
	 * NOTE: THis call does not invoke recalculation on cart
	 * @param String $couponcode
	 */
	public function addCouponCode($couponcode,$type,$paramsArray=false){
		if(empty($couponcode))
			return;
		unset($this->coupons[$type]);
		$this->coupons[$type][$couponcode]['code']=$couponcode;
		$this->coupons[$type][$couponcode]['params']=$paramsArray;
	}
	
	
	/**
	 * Adds a coupon property.
	 * @param String $couponcode
	 */
	public function addCouponProperty($couponcode,$type,$property,$value){
		if(empty($couponcode))
			return;
		$this->coupons[$type][$couponcode][$property]=$value;
	}
	
	/**
	 * 
	 * Retrive all coupon code
	 * 
	 */
	public function getAllCouponCodes($type){
		return $this->coupons[$type];
	}
	/**
	 *NOTE: THis call does not invoke recalculation on cart
	 * @param unknown_type $couponCode
	 * @param unknown_type $type
	 */
	public function removeCoupon($couponCode,$type){
		$this->autoApplyCoupon = false;
		unset($this->coupons[$type][$couponCode]);
	}
	
	protected function setCouponApplicationCode($couponCode,$type,$code,$errorMsg=""){
		$this->coupons[$type][$couponCode]['acode']=$code;
		$this->coupons[$type][$couponCode]['emsg']=$errorMsg;
	}
	
	public function getCouponApplicationCode($couponCode,$type){
		return $this->coupons[$type][$couponCode]['acode'];
	}
	
	public function getCouponApplicationErrorMessage($couponCode,$type){
		return $this->coupons[$type][$couponCode]['emsg'];
	}
	
	
	public function getCouponParams($couponCode,$type){
		return $this->coupons[$type][$couponCode]['params'];
	}
	
	/**
	 * 
	 * @param String $type
	 */
	public function clearAllCoupons($type){
		unset($this->coupons[$type]);
	}
	
	public function getAllSkus() {
		$skus = array();
		foreach($this->getProducts() as $product){
			$sku = $product->getSKUArray();
			$skus[$sku['id']] = $sku;
		}
		return $skus;
	}

	public function getProductsForCombo($comboid){
		$productids=array();
		foreach($this->getProducts() as $product){
			if(isset($product->comboId) && $product->comboId>0)
			$productids[]=$product->getProductId();
		}
		return $productids;
	}
        
	public function getJsonForDiscount()
	{
		$cartForDiscount = array();
		$cartForDiscount['discountAmount'] = $this->getDiscountAmount();
		$cartForDiscount['totalItems'] = $this->noOfItems;
		$cartForDiscount['aditionalCharges'] = $this->additionalCharges;
		$cartForDiscount['nonDiscountedAmount'] = $this->nonDiscountedAmount;
		$cartForDiscount['nonDiscountedItemCount'] = $this->nonDiscountedItemCount;
		$cartForDiscount['context'] = $this->context;
		$this->discountOnCart = 0;		
		$this->bagDiscount = 0;
		$items = array();
		
		$freeItems=array();
		
		foreach ($this->productsArray as $product) {
                    $cartItem = $product->getItemForDiscount();
                    if($cartItem!=null)
                        array_push($items, $cartItem);
                    else{
                    	$freeItem = $product->getFreeItemForDiscount();
                    	if($freeItem!=null){
                    		array_push($freeItems,$freeItem);
                    	} 
                        $this->removeItem($product, false);
                        //MCartUtils::persistCart($this)
                    } 
		}
        $this->setDisplayData = null;
		$this->displayData = null;
        $cartForDiscount['items'] = $items;
        $cartForDiscount['freeItems']=$freeItems;
		return json_encode($cartForDiscount);
		
	}

	public function getCartLevelDiscount()
	{
		return $this->bagDiscount;
	}
	
	public function setDiscountFields($discountedCart)
	{
		$this->noOfItems = $discountedCart->totalItems;
		$this->setDisplayData = $discountedCart->comboDisplayData;
		$this->bagDiscount = $discountedCart->discountOnCart;
		$this->displayData = $discountedCart->displayMessage;
		$this->slabDisplayData = $discountedCart->slabDisplayMessage;
		$this->excludedArticleTypes = $discountedCart->excludedArticleTypes;
		
		foreach ($discountedCart->styleIdItemMap as $styleId => $skuIdArray) {
			foreach($skuIdArray as $skuId => $productDetails){
				if($productDetails->newItemToBeAdded==1){
					if($skuId == 0){
						//Add
						$mcartItem= new MCartStyleItem(MCartUtils::genProductId(),$styleId,$productDetails->discountQuantity);
					} else {
						//create a new product
						$mcartItem= new MCartNPItem($skuId,$skuId,$productDetails->discountQuantity);
					}
					$mcartItem->setDiscountFields($productDetails);
					$this->addProduct($mcartItem,false,false);
					unset($discountedCart->styleIdItemMap->{$styleId}->{$skuId});
				}
			}
		}

		$map = array();
		foreach ($this->productsArray as $product) {
			$sku = $product->getSKUArray();
			$styleId = $product->getStyleId();
			if($discountedCart->styleIdItemMap->{$styleId}->{$sku['id']})
				$product->setDiscountFields($discountedCart->styleIdItemMap->{$styleId}->{$sku['id']});
			unset($discountedCart->styleIdItemMap->{$styleId}->{$sku['id']});
			if(!isset($map[$product->discountRuleId])) $map[$product->discountRuleId] = 0;
			$map[$product->discountRuleId] = $product->discountOnSet;
		}

		foreach ($map as $ruleId => $discountOnSet) {
			$this->discountOnCart += $discountOnSet;
		}
		
	}
	
	public function getCartSubtotal(){
    	return $this->getMrp() + $this->getAdditionalCharges() - $this->getDiscountAmount() -$this->getCartLevelDiscount();
    }
    
    
    public function getDiscountedItemsSubtotal()
    {
    	$discountItemsSubtotal = 0.0;
    	// Iterate over the products in the cart to find Amount of prior discounted products.
    	foreach( $this->getProducts() as $id=>$product) {
    		// *check whether the product is a discounted product to skip coupon application
    		if (( $product->isDiscountedProduct )){
    			$discountItemsSubtotal += $product->getTotalProductPrice() + $product->getTotalAdditionalCharges();
    		}
    	}

    	return $discountItemsSubtotal;
    }
    /**
     * returns Cart subtotal excluding the sum of discounted products
     */
    public function getNonDiscountedItemsSubtotal()
    {
    	$nonDiscountedItemsSubtotal = $this->getMrp() + $this->getAdditionalCharges() - $this->getDiscountedItemsSubtotal();
    	$countNonDiscounted=0;
    	foreach( $this->getProducts() as $id=>$product) {
    		if($product->isDiscountedProduct) continue;
    		$nonDiscountedItemsSubtotal -= $product->cartDiscount;
    		$countNonDiscounted=$countNonDiscounted+$product->getQuantity();
    	}
    	$this->nonDiscountedAmount=$nonDiscountedItemsSubtotal;
    	$this->nonDiscountedItemCount=$countNonDiscounted;
    	return $nonDiscountedItemsSubtotal;
    }

    public function getCouponNotApplicableItemsSubtotal()
    {
    	$ct = func_num_args();

    	$couponNotApplicableItemsSubtotal = 0.0;
    	
    	$condNoDiscountCoupon = FeatureGateKeyValuePairs::getBoolean('condNoDiscountCoupon');
    	
       	if($ct > 0) {
    		$condNoDiscountCoupon = func_get_arg(0);
    	}
    	
    	$styleExclusionFG = FeatureGateKeyValuePairs::getBoolean('styleExclusion');
    	
    	// Iterate over the products in the cart to find Amount of prior discounted products.
    	foreach( $this->getProducts() as $id=>$product) {
    		// *check whether coupon can be applicable on the product
    		if (($styleExclusionFG == 1 && !$product->couponApplicable) 
    				|| ($condNoDiscountCoupon == 1 && $product->isDiscountedProduct)){
    			$couponNotApplicableItemsSubtotal += $product->getTotalProductPrice() + $product->getTotalAdditionalCharges();
    			}              
    		else if(!$condNoDiscountCoupon) {
    			$couponNotApplicableItemsSubtotal += $product->getDiscountAmount();
    	}
    	}
    	
    	return $couponNotApplicableItemsSubtotal;
    }
    
    public function getCouponApplicableItemsSubtotal()
    {
    	$ct = func_num_args();
    	
    	$condNoDiscountCoupon = FeatureGateKeyValuePairs::getBoolean('condNoDiscountCoupon');
  
    	if($ct > 0) {
    		$condNoDiscountCoupon = func_get_arg(0);
    	}
    	
    	$styleExclusionFG = FeatureGateKeyValuePairs::getBoolean('styleExclusion');
    	
    	$couponApplicableItemsSubtotal = $this->getMrp() + $this->getAdditionalCharges() - $this->getCouponNotApplicableItemsSubtotal($condNoDiscountCoupon);
    	
    	$countCouponApplicable = 0;
    	foreach( $this->getProducts() as $id=>$product) {
    		if(($styleExclusionFG == 1 && !$product->couponApplicable) 
    				|| ($condNoDiscountCoupon == 1 && $product->isDiscountedProduct)) 
    			continue;
    		$couponApplicableItemsSubtotal -= $product->cartDiscount;
    		$countCouponApplicable = $countCouponApplicable + $product->getQuantity();
    	}
    	$this->couponApplicableAmount=$couponApplicableItemsSubtotal;
    	$this->couponApplicableItemCount=$countCouponApplicable;
    	
    	return $couponApplicableItemsSubtotal;
    }
    
    
    public function getCouponApplicableSubtotal($applicableItemPrices){
        if ("cart" == MABTest::getInstance()->getUserSegment("coupon_applicability")) {
            return $this->getCartSubtotal();
        }
    	if(sizeof($this->couponApplicableCartItems)==0) return 0.0;
    	
    	
    	
    	$applicableItemsTotal = 0;
    	foreach($applicableItemPrices as $id=>$price){
    		$applicableItemsTotal += $price;
    	}
    	
    	return $applicableItemsTotal +  $this->getAdditionalCharges();
    	
    }
    
	/**
	 * 
     * Recalculate prices in cart 
     * applies product discount/coupon discount/mynt credits etc etc
     *
	 * @param boolean $forceDiscountApiCall  
	 * @throws CouponLockedException
	 */
    protected function reCalculatePricesAndDiscounts($forceDiscountApiCall){
    	
    	//Removing old cashback coupons is exists
    	unset($this->coupons[self::$__COUPON_TYPE_CASHBACK]);
    	
    	if($this->coupons != null && !is_array($this->coupons)){ // HACK to remove old cashback coupons
    		$this->coupons= array();
    	}

    	//clear vat fields before recalculation
    	$this->clearVat();
    	
    	if($forceDiscountApiCall){
    		$this->clearProductsDiscount();
    	}
    	$this->clearCouponDiscount();
    	$this->clearCashDiscount(); 	
    	//Appply Discount
    	if($forceDiscountApiCall){
		$this->dreRev= time();//WidgetKeyValuePairs::getWidgetValueForKey(self::$__DISCOUNT_ENGINE_FEATUREGATE_KEY); //TODO DISCOUNT ENGINE INTEGRATION
    		DiscountEngine::executeOnCart($this);
    		
		}
    	
    	$this->setShippingCharge();
    	
    	//Prepare for coupon validation
    	
    	// Instance of the coupon database liaison.
    	$adapter = CouponAdapter::getInstance();
    	$validator = CouponValidator::getInstance();
    	$calculator = CouponDiscountCalculator::getInstance();
    	$cashcalculator = CashCouponCalculator::getInstance();
    	
    	//Default coupon calculations
    	$discountCouponArray=array_keys($this->getAllCouponCodes(self::$__COUPON_TYPE_DEFAULT));
    	
    	//Currently We have just one coupon for each type
    	$discountCouponCode=$discountCouponArray[0];
    	if(MABTest::getInstance()->getUserSegment("checkout") ==="test"){
    		$autoApplyCoupon = MABTest::getInstance()->getUserSegment("couponAutoApplyTest");
    	}
    	
    	if($this->autoApplyCoupon == true && $autoApplyCoupon == "apply" && 
    			($this->context == CartContext::DefaultContext || 
    			$this->context == CartContext::OneClickContext )&& empty($discountCouponCode)){
    		$couponAction = new CouponActions();
    		$couponsApplicable = $couponAction->sortCoupons($this);
    		foreach($couponsApplicable as $coupon){
    			if($coupon["isApplicable"] == true){
    				$this->addCouponCode($coupon["coupon"], self::$__COUPON_TYPE_DEFAULT);
    				//Default coupon calculations
    				$discountCouponArray=array_keys($this->getAllCouponCodes(self::$__COUPON_TYPE_DEFAULT));
    					
    				//Currently We have just one coupon for each type
    				$discountCouponCode=$discountCouponArray[0];
    				break;
    			}
    		}
    	}
    	
    	if(!empty($discountCouponCode)){
    		try {
    			//fetch Coupon
    			$discountCouponObject = $adapter->fetchCoupon($discountCouponCode,true);// get coupon from read database
    		 	try{
    		 		if($discountCouponObject!=null){
    		 			$this->addCouponProperty($discountCouponCode, self::$__COUPON_TYPE_DEFAULT, "expiry", $discountCouponObject->getEndDate());
    		 		}
    		 		//Check Validity
    		 		$validator->isCouponValidForUser($discountCouponObject, $this->login, true);
    			}catch (CouponLockedException $ex){
    				//Cancel last PP order if coupon was already blocked by that order
    		 		$unblocked=findAndCancelPPOrderBlockingCoupon( $this->login,"DISCOUNT",$discountCouponCode);
    		 		//If Coupon unlocked and order cancelled revalidate coupon
    		 		if($unblocked){
    		 			$discountCouponObject = $adapter->fetchCoupon($discountCouponCode,true); // get coupon from read database
    		 			$isValid = $validator->isCouponValidForUser($discountCouponObject, $this->login, true);
    		 		}else{
    		 			throw $ex;
    		 		}
    			}
    			//Apply coupon on cart
    			$calculator->applyCouponOnCart($discountCouponObject, $this);
    			
    			//Mark coupon success
    			$this->setCouponApplicationCode($discountCouponCode, self::$__COUPON_TYPE_DEFAULT, self::$__COUPON_APP_CODE_SUCCESS);
    			
            } catch (CouponAmountException $cae) {
                $this->setCouponApplicabilityMessage(array($cae->getmessage()));
                $this->setCouponApplicationCode($discountCouponCode, self::$__COUPON_TYPE_DEFAULT, self::$__COUPON_APP_CODE_FAILED,$cae->getMessage());
            } catch (CouponCountException $cce) {
                $this->setCouponApplicabilityMessage(array($cce->getmessage()));
                $this->setCouponApplicationCode($discountCouponCode, self::$__COUPON_TYPE_DEFAULT, self::$__COUPON_APP_CODE_FAILED,$cce->getMessage());
            } catch (CouponException $cnfe) {
                $this->setCouponApplicationCode($discountCouponCode, self::$__COUPON_TYPE_DEFAULT, self::$__COUPON_APP_CODE_FAILED,$cnfe->getMessage());
            }
    	}

    	$vatEnabled = FeatureGateKeyValuePairs::getBoolean("vat.enabled", false);
    	if($vatEnabled) {
    		$this->calculateVat();
    	}


    	//Cash coupon calculations
    	$cashCouponCodeArray=array_keys($this->getAllCouponCodes(self::$__COUPON_TYPE_CASHBACK));
    	//Currently We have just one coupon for each type
    	$cashCouponCode=$cashCouponCodeArray[0];
   		
    	//------------------------------------------------------------------------------------loyalty points application--------------------------
    	  	
    	$lpEnabled = FeatureGateKeyValuePairs::getBoolean('myntra.loyalty.enabled', false);
    	 
    	
    	if($lpEnabled){
    		$loyaltyUsers = WidgetKeyValuePairs::getWidgetValueForKey("loyalty.users");
    		if(!empty($loyaltyUsers) && strtolower($loyaltyUsers) == 'all'){
    			$lpEnabled = true;
    		}elseif(!empty($loyaltyUsers)){
    			$lpEnabled = false;
    			$enabledUsers = explode(",", $loyaltyUsers);
    			$lpEnabled = in_array($this->login, $enabledUsers) || in_array(substr($this->login, strpos($this->login,"@")), $enabledUsers);
    		}else{
    			$lpEnabled = false;
    		}
    	}
    	
    	$cartLogin = $this->login;
    	if($lpEnabled && !empty($cartLogin)){
    		
    		$lpD = LoyaltyPointsPortalClient::getAccountInfo($this->login);
    		
    		$this->setShippingCharge($lpD); // recalculate shipping charges based on loyalty info
    		
	    	//loyalty points usage
	    	if($this->useLoyaltyPoints && $this->login != null){
	    		 
	    		
	    	
	    		if($lpD["activePointsBalance"] >= $lpD["thresholdForUsage"] && ($lpD["tNcAccepted"] == "true" || $lpD["tNcAccepted"]) ){
	    		
	    			$userEnteredLoyaltyPoints = $lpD["activePointsBalance"]; // to be replaced with userEnteredLoyaltyPoint
	    			LoyaltyPointsHelper::applyLoyaltyPointsUsageOnCart($this, $userEnteredLoyaltyPoints, $lpD["activePointsBalance"],$lpD["conversionFactor"]);
	    		}else{
	    			LoyaltyPointsHelper::removeLoyaltyPointsUsage($this);
	    		}
	    		 
	    	}else{
	    		LoyaltyPointsHelper::removeLoyaltyPointsUsage($this);
	    	}
	    	
	    	$this->setLoyaltyPointsAwarded($lpD["awardFactor"]); // refresh the loyalty points awarded
    	}else{
    		LoyaltyPointsHelper::removeLoyaltyPointsUsage($this);
    		$this->setLoyaltyPointsAwarded(0);
    	}
    	
    	//------------------------------------------------------------------------------------loyalty points application-ends----------------------
    	   
    	
    	//------------------------------------------------------------------------------------cashback application--------------------------
    	
    	//MyntCash usage 
    	if($this->useMyntCash && $this->login != null){
	    	
	    	$mcD = MyntCashService::getUserMyntCashDetails($this->login);
	    	MyntCashCalculator::applyMyntCashUsageOnCart($this, /*$this->userEnteredMyntCashAmount*/ $mcD['balance'], $mcD);
	    	
    	}else{
    		MyntCashCalculator::removeMyntCashUsage($this);
    	}
    	//------------------------------------------------------------------------------------cashback application-ends----------------------
    	
    	
    	
    	
    	
    	
    	$this->changed=true;
    }

    //vat calculation method
    public function calculateVat() {
    	
    	// computes and sets vat 
    	VatCalculator::refreshVatOnCart($this);
    	       
    }

    public function clearVat() {
#       $this->additionalCharges = 0;
#       $vatEnabled = FeatureGateKeyValuePairs::getBoolean("vat.enabled", false);
#        if($vatEnabled) {
         if(true) { 
            $this->additionalCharges = 0;
            foreach ($this->getProducts() as $product) {
                $product->setTotalAdditionalCharges(0);
                $product->setUnitAdditionalCharges(0);
            }
        }
    }

    /**
    * Clears product discount on cart and cart items
    */
    public function clearProductsDiscount(){
    	
    	//remove items which were added by discount engine
     	/*foreach ($this->productsArray as $product) {
     		if($product->isItemAddedByDiscountEngine())
     			$this->removeItem($product,false);
    	}
    	*/
    	$this->noOfItems = 0;
    	$this->setDisplayData = "";
    	$this->displayData = "";
    	foreach ($this->productsArray as $product) {
    		$product->clearProductDiscount();
    	}
    	$this->discountOnCart = 0;
		$this->bagDiscount = 0;
    	
    }
    
    /**
     * Clears coupon discount on cart and cart items
     */
    public function clearCouponDiscount(){
    	 
   		 $this->discount = 0.0;
        // Iterate over products to remove discounts.
        foreach ($this->productsArray as $id=>$product) {
            $product->clearCouponDiscount();
        }
    	 
    }
    
    /**
     * returns default coupon discount
     * @return number
     */
    public function getTotalCouponDiscount(){
    	return $this->discount;
    }
    
    /*
     * use available MyntCash on this cart
     * 
     * set user entered amount for myntcash usage
     */
    public function enableMyntCashUsage($amount){
    	$this->useMyntCash = true;
    	$this->userEnteredMyntCashAmount = $amount;
    }
    
    public function disableAutoApplyCoupon(){
    	$this->autoApplyCoupon = false;
    }
    
    public function removeMyntCashUsage(){
    	$this->useMyntCash = false;
    	$this->userEnteredMyntCashAmount = 0.0;
    }
    
    public function isMyntCashUsed(){
    	return $this->useMyntCash;
    }
      
    
    /*
     * Getter for totalMyntCashUsage
    */
    public function getTotalMyntCashUsage()
    {
    	return $this->totalMyntCashUsage;
    }
    
    /*
     * overriding $Cart->getCashDiscount()
     */
    public function getCashDiscount(){
    	return $this->getTotalMyntCashUsage();
    }
    
    
    /*
     * Set or update MyntCash usage
    */
    public function setMyntCashUsage( )
    {
    	$this->totalMyntCashUsage = 0.0;
    	
    	foreach($this->getProducts() as $product){
    		$this->totalMyntCashUsage += $product->getTotalMyntCashUsage();    		
    	}

    	$this->totalMyntCashUsage += $this->giftOrShippingMyntCashUsage;
    	//$this->cashdiscount = $this->totalMyntCashUsage;
    }

    /*
     * Set giftOrShippingMyntCashUsage
     */
    public function setGiftOrShippingMyntCashUsage($amount){
    	$this->giftOrShippingMyntCashUsage = $amount;
    }

    
    public function getLoyaltyPointsAwarded(){
    	return $this->loyaltyPointsAwarded;	
    }
    
    public function getLoyaltyPointsCashUsed(){
    	return $this->loyaltyPointsUsed*$this->loyaltyPointsConversionFactor;
    }
    
    public function getLoyaltyPointsUsed(){
    	return $this->loyaltyPointsUsed;
    }
    
    public function getLoyaltyPointsConversionFactor(){
    	return $this->loyaltyPointsConversionFactor;
    }
    
    public function getLoyaltyPointsCartSpecificAwardFactor(){
    	return $this->loyaltyPointsCartSpecificAwardFactor;
    }
    
    public function getLoyatltyPointsUserSpecificAwardFactor(){
    	return $this->loyatltyPointsUserSpecificAwardFactor;
    }
    
    public function getUseLoyaltyPoints(){
    	return $this->useLoyaltyPoints;
    }
    
    /*
     * compute and set loyalty points awarded
     */
    public function setLoyaltyPointsAwarded($awardFactor = 0){
    	$this->loyaltyPointsAwarded = 0;
    	$this->loyatltyPointsUserSpecificAwardFactor = $awardFactor;
    	foreach ($this->getProducts() as $product){
    		$product->refreshAwardedLoyaltyPoints($awardFactor*$this->loyaltyPointsCartSpecificAwardFactor);
    		$this->loyaltyPointsAwarded += $product->getLoyaltyPointsAwarded();
    	}
    	$this->loyaltyPointsAwarded = $this->loyaltyPointsAwarded;
    }
    
    /*
     * set and compute loyalty points computed
     */
    public function setLoyaltyPointsUsage($conversionFactor){
    	$this->loyaltyPointsUsed = 0;
    	foreach ($this->getProducts() as $product){
    		$this->loyaltyPointsUsed += $product->getLoyaltyPointsUsed();
    	}
    	
    	$this->loyaltyPointsUsed += $this->giftOrShippingLoyaltyPointsUsage;
    	$this->loyaltyPointsConversionFactor = $conversionFactor;
    	$this->loyaltyPointsUsedCashEqualent = $this->loyaltyPointsConversionFactor * $this->loyaltyPointsUsed;
    }
    
    /*
     * Set giftOrShipping loyalty points usage
    */
    public function setGiftOrShippingLoyaltyPointsUsage($points){
    	$this->giftOrShippingLoyaltyPointsUsage = $points;
    }
    
    /*
     * Get giftOrShipping loyalty points usage
    */
    public function getGiftOrShippingLoyaltyPointsUsageCashEqualent(){
    	return $this->giftOrShippingLoyaltyPointsUsage*$this->loyaltyPointsConversionFactor;
    }
    
    /*
     * remove loyalty points usage on cart
     */    
    public function disableLoyaltyPointsUsage(){
    	$this->loyaltPointsUsed = 0;
    	$this->loyaltyPointsConversionFactor = 0;
    	$this->userEnteredLoyaltyPointsToUse = 0;
    	$this->useLoyaltyPoints = false;
    }
    
    /*
     * enable loyalty points usage
     */
    public function enableLoyaltyPointsUsage($userEnteredPoints = 0){
    	$this->userEnteredLoyaltyPointsToUse = $userEnteredPoints;
    	$this->useLoyaltyPoints = true;
    }
    
    
    public function getCartCreateTimestamp(){
    	return $this->cartCreateTimestamp;	
    }
    
    public function setCartCreateTimestamp($cartCreateTimestamp){
    	$this->cartIsNew = true;
    	$this->cartCreateTimestamp=$cartCreateTimestamp;
    }
	public function getIsGiftOrder(){
    	return $this->isGiftOrder;	
    }
    public function setIsGiftOrder($isGiftOrder){
    	$this->isGiftOrder = $isGiftOrder;
    }
    
	public function getGiftCharge(){
    	return $this->giftCharge;	
    }
    public function setGiftCharge($giftCharge){
    	$this->giftCharge = $giftCharge;
    }
    
    public function getGiftMessage(){
    	return $this->giftMessage;	
    }
    public function setGiftMessage($giftMessage){
    	$this->giftMessage = $giftMessage;
    }
    public function isCartNew(){
    	return $this->cartIsNew;
    }
    public function setCartNew($bool){
    	$this->cartIsNew=$bool;
    }
    public function beforeSerialize(){
    }
    
    public function afterDeSerialize(){
    	
    }
    
    public function clear(){
    	parent::clear();
    	$this->clearProductsDiscount();
    	$this->clearDiscount();
    	$this->clearCashDiscount();
    	$this->clearCouponDiscount();
}
    public function toArray(){
    	$coupons = $this->coupons;
    	if(!empty($coupons)){
    		foreach($coupons as $key=>$val){
    			if(empty($val))
    				unset($this->coupons[$key]);
    		}
    	}
    	$reflect = new ReflectionClass($this);
    	$props   = $reflect->getProperties(ReflectionProperty::IS_PUBLIC | ReflectionProperty::IS_PROTECTED | ReflectionProperty::IS_PRIVATE);
    	$arr = array();
    	foreach ($props as $prop) {
    		if(!$prop->isStatic() && $prop->class == "MCart"){
    			$p = $prop->getName();
    			$arr[$prop->getName()]=$this->$p;
    		}
    		
    	}
    	$arr2= parent::toArray();
    	$arr=array_merge($arr,$arr2);
    	$arr[cartCreateTimestamp]= (int)cartCreateTimestamp;
    	return $arr;
    }
   
    public function toJson() {
        $arr = $this->createArrayFromObj();
        $res =  json_encode($arr);
        return $res;
    }

    public function createArrayFromObj() {
    	$arr = MCartUtils::createArrayFromObj(get_object_vars($this));
    	return $arr;
    }
    
    public function fromJson($jsonArray,$context){
    	$cartArray = json_decode($jsonArray,true);

    	$this->cartCreateTimestamp = $cartArray["cartCreateTimestamp"];
    	$this->cartIsNew = $cartArray["cartIsNew"];
    	$this->changed = $cartArray["changed"];
    	$this->cartID = $cartArray["cartID"];
    	$this->orderID = $cartArray["orderID"];
    	$this->login = $cartArray["login"];
    	$this->context = $cartArray["context"];
    	$this->coupons = $cartArray["coupons"];
    	$this->lastContentUpdated = $cartArray["lastContentUpdated"];
    	$this->lastmodified = $cartArray["lastmodified"];
    	$this->expiryDate = $cartArray["expiryDate"];
    	$this->availApiCall = $cartArray["availApiCall"];
    	$this->nonDiscountedAmount = $cartArray["nonDiscountedAmount"];
    	$this->nonDiscountedItemCount = $cartArray["nonDiscountedItemCount"];
    	$this->couponAppliedItemCount = $cartArray["couponAppliedItemCount"];
    	$this->couponAppliedSubTotal = $cartArray["couponAppliedSubTotal"];
    	$this->couponApplicableItemCount = $cartArray["couponApplicableItemCount"];
    	$this->couponApplicableAmount = $cartArray["couponApplicableAmount"];
    	$this->couponApplicableCartItems = $cartArray["couponApplicableCartItems"];
    	$this->shippingCharge = $cartArray["shippingCharge"];
    	$this->shippingDiscountText = $cartArray["shippingDiscountText"];
    	$this->giftCharge = $cartArray["giftCharge"];
    	$this->giftMessage = $cartArray["giftMessage"];
    	$this->isGiftOrder = $cartArray["isGiftOrder"];
    	$this->bagDiscount = $cartArray["bagDiscount"];
    	$this->dreRev = $cartArray["dreRev"];
    	$this->useMyntCash = $cartArray["useMyntCash"];
    	$this->autoApplyCoupon = $cartArray["autoApplyCoupon"];
    	$this->userEnteredMyntCashAmount = $cartArray["userEnteredMyntCashAmount"];
    	$this->giftOrShippingMyntCashUsage = $cartArray["giftOrShippingMyntCashUsage"];
    	$this->totalMyntCashUsage = $cartArray["totalMyntCashUsage"];
    	
    	$this->useLoyaltyPoints = $cartArray["useLoyaltyPoints"] == null? false:$cartArray["useLoyaltyPoints"];
    	$this->loyaltyPointsAwarded = $cartArray["loyaltyPointsAwarded"];
    	$this->loyaltyPointsUsed = $cartArray["loyaltyPointsUsed"];
    	$this->userEnteredLoyaltyPointsToUse = $cartArray["userEnteredLoyaltyPointsToUse"];
    	$this->loyaltyPointsConversionFactor = $cartArray["loyaltyPointsConversionFactor"];
    	$this->giftOrShippingLoyaltyPointsUsage = $cartArray["giftOrShippingLoyaltyPointsUsage"];
    	$this->loyatltyPointsUserSpecificAwardFactor = $cartArray["loyatltyPointsUserSpecificAwardFactor"] == null? 0:$cartArray["loyatltyPointsUserSpecificAwardFactor"];
    	$this->loyaltyPointsCartSpecificAwardFactor = $cartArray["loyaltyPointsCartSpecificAwardFactor"] == null? 1:$cartArray["loyaltyPointsCartSpecificAwardFactor"];
    	$this->loyaltyPointsUsedCashEqualent = $cartArray["loyaltyPointsUsedCashEqualent"];
    	
    	//cart
    	$this->productsArray = $this->getCartItemsFromArray($cartArray["productsArray"],$context);  
    	$this->productQtyArray = $cartArray["productQtyArray"];
    	$this->noOfItems = $cartArray["noOfItems"];
    	$this->price = $cartArray["price"];
    	$this->mrp = $cartArray["mrp"];
    	$this->vat = $cartArray["vat"];
    	$this->additionalCharges = $cartArray["additionalCharges"];
    	$this->discount = $cartArray["discount"];
    	$this->cashdiscount = $cartArray["cashdiscount"];
    	$this->discountOnCart = $cartArray["discountOnCart"];

    	$setDisplayDataArr = $cartArray["setDisplayData"];
    	if(!empty($setDisplayDataArr)){
    		$setDisplayDataObj = new stdClass();
    		foreach($setDisplayDataArr as $key => $value){
    			if( is_array($setDisplayDataArr[$key])){
    				$setDisplayDataObj->$key  = MCartUtils::createStdClass($value);//$id;
    			}
    		}
    		$this->setDisplayData = $setDisplayDataObj;
    	}
    	
    	$displayDataArr = $cartArray["displayData"];
    	if(!empty($displayDataArr)){
    		$this->displayData = MCartUtils::createStdClass($displayDataArr);
    	}
    	
    	$slabDisplayDataArr = $cartArray["slabDisplayData"];
    	if(!empty($slabDisplayDataArr)){
    		$this->slabDisplayData = MCartUtils::createStdClass($slabDisplayDataArr);
    	}    	
		$this->excludedArticleTypes = $cartArray["excludedArticleTypes"];

    	return $this;
    }
    
    private function getCartItemsFromArray($productsArray,$cartContext){

    	$products = array();
    
    	foreach ($productsArray as $key=>$product) {
    		
    		if($cartContext === CartContext::SavedContext){
    			$cartItem = new SavedItem();
    		}elseif ($cartContext === CartContext::GiftCardContext) {
    			$cartItem = new MCartGiftCardItem();
    		}else{
    			$cartItem = new MCartNPItem();
    		}
    		$products[$key] = $cartItem->createObjFromArray($product);
    	}    	
    	return $products;
    }
    
    public function getSlabDisplayData(){
    	return $this->slabDisplayData;
    }
}
?>


