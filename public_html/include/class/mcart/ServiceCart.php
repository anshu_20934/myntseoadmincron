<?php
namespace mcart;

use enums\cart\CartContext;

class ServiceCart extends \MCart{

	private $serviceCalled = false;

	private $cartResponse;

	private $cartContext;

	private $operations;
	
	private $realtime = false;
	
	private $login ;

	public function __construct($cartContext,$login=false){
		if($login!==false)
			$this->login = $login;
		$this->cartContext = $cartContext;
	}
	public function isChanged(){
	}
	public function setCartID($id){
	}
	public function getCartID(){
		if($this->serviceCalled== false){
			$this->doServiceCall();
		}
		return $this->cartResponse[cartID];
	}
	/* public function setOrderID($id){ TODO
		$this->orderID=$id;
	}
	public function getOrderID(){}  */
	public function setAsSaved(){
	}
	public function setAsChanged(){
	}
	public function setLogin($login){
	}
	public function getLogin(){
		if($this->serviceCalled== false){
			$this->doServiceCall();
		}
		return $this->cartResponse[login];
	}
	public function setContext($context){
	}
	public function getContext(){
		return strtolower($this->cartContext);
	}
	public function getLastContentUpdatedTime(){
	}

	public function setLastContentUpdatedTime($time){
	}

	public function getNonDiscountedAmount(){
	}

	public function getNonDiscountedItemCount(){
		if($this->serviceCalled== false){
			$this->doServiceCall();
		}
		$count = 0.0;
		foreach($this->cartResponse[cartItemEntries] as $item){
			if($item[totalDiscount] <= 0)
				$count+=$item[quantity];
		}
		return $count;
	}

	public function getCouponApplicableItemCount(){
		return $this->getCouponAppliedItemCount();
	}
	public function getCouponAppliedItemCount(){
		if($this->serviceCalled== false){
			$this->doServiceCall();
		}
		$count = 0.0;
		foreach($this->cartResponse[cartItemEntries] as $item){
			if($item[totalCouponDiscount] > 0)
				$count+=$item[quantity];
		}
		return $count;
	}
	public function setCouponAppliedItemCount($itemCount){
	}
	public function getCouponAppliedSubtotal(){
		if($this->serviceCalled== false){
			$this->doServiceCall();
		}
		$amount = 0.0;
		foreach($this->cartResponse[cartItemEntries] as $item){
			if($item[totalCouponDiscount] > 0)
				$amount+=$item[totalDiscountDecimal];
		}
		return $amount;
	}
	public function setCouponAppliedSubtotal($appliedSubtotal){
	}
	public function getCouponApplicableCartItems() {
	}
	public function setCouponApplicableCartItems($applicableProducts) {
	}
	public function getCouponNotApplicableCartItems() {
	}
	public function setCouponApplicabilityMessage($couponApplicabilityMessage) {
	}
	public function getCouponApplicabilityMessage() {
	}
	public function setCouponNotApplicableCartItems($notApplicableProducts) {
	}
	public function refresh($refreshFromDb=false,$forcePriceRecalculation=false,$forceProductDiscountRecalculation=false,$forceItemAvailabilityRefresh=false){
		$this->realtime = $refreshFromDb;
	}
	protected function getItemsInfo(){
	}
	public function refreshItemsAvailability($forced = false){
		//parent::refreshItemsAvailability(true);
	}
	public function removeItem($product,$callRefresh=true){
		$this->serviceCalled = false;
		if($product!=null){
			$this->operations[item]["remove"][]=$product->getProductId();
		}
	}
	public function addProduct($product, $toMerge = false , $callRefresh=true){
		$this->serviceCalled = false;
		$array = array();
		$array[skuId] = $product->getSkuId();
		$array[quantity] = $product->getQuantity();
		$array[styleId] = $product->getStyleId();
		$this->operations[item]["add"][]= $array;
	}
	public function addProductToShortList($styleId){
	}
	public function getShippingCharge() {
		return $this->cartResponse[shippingCharge];
	}
	public function getShippingMethod() {
            if($this->serviceCalled== false){
		$this->doServiceCall();
            }
            return $this->cartResponse[shippingMethod];
	}
	public function setShippingCharge() {
	}
	public function getTotalProductItemDiscount(){
		if($this->serviceCalled== false){
			$this->doServiceCall();
		}
		$discountAmount = 0.0;
		foreach($this->cartResponse[cartItemEntries] as $item){
			$discountAmount+=$item[totalDiscountDecimal];
		}
		return $discountAmount;
	}
	public function updateItemQuantity($product,$quantity){
		$this->serviceCalled = false;
		$array = array();
		$array["itemId"]=$product->getProductId();
		$array[skuId] = $product->getSkuId();
		$array[quantity] = $quantity;
		$array[styleId] = $product->getStyleId();
		$this->operations[item]["update"][]= $array;
	}
	
	public function updateItem($cartitem,$skuId,$qty){
		$this->serviceCalled = false;
		$array = array();
		$array["itemId"]=$cartitem;
		$array[skuId] = $skuId;
		$array[quantity] = $qty;
		$this->operations[item]["update"][]= $array;
	}
	public function addOrUpdateItemQuantityBasedOnSkuId($skuId, $quantityToAddOrUpdate = 1, $mode = ActionType::Update) {
	}
	public function isReadyForCheckout(){
		if($this->serviceCalled== false){
			$this->doServiceCall();
		}
		return $this->cartResponse[readyForCheckout];
	}
	public function getStyleIds() {
	}
	public function getItemsAvailabilityArray(){
		if($this->serviceCalled== false){
			$this->doServiceCall();
		}
		$availability=array();
		$carryOverQty;
		foreach($this->cartResponse[cartItemEntries] as $product){
			if($product[selectedSize][available]==true){
				$availability[$product["itemId"]]['status']==\MCartItem::$__ITEM_STATE_IN_STOCK;
				$availability[$product["itemId"]]['stock']=$product[selectedSize][availableQuantity];
			}elseif($product[selectedSize][available]==false && $product[selectedSize][availableQuantity] < $product[selectedSize][quantity]){
				$availability[$product["itemId"]]['status']==\MCartItem::$__ITEM_STATE_PARTIAL_OUT_OF_STOCK;
				$availability[$product["itemId"]]['stock']=$product[selectedSize][availableQuantity];
			}else{
				$availability[$product["itemId"]]['status']=\MCartItem::$__ITEM_STATE_OUT_OF_STOCK;
				$availability[$product["itemId"]]['stock']=0;
			}
			$availability[$product["itemId"]]['itemAvailabilityDetailMap']= $product[selectedSize][itemAvailabilityDetailMap];
		}
		return $availability;
	}
	public function removeInvalidItems(){
	}
	public function getProductIdsInCart(){
		if($this->serviceCalled== false){
			$this->doServiceCall();
		}
		$productIds = array();
		foreach($this->cartResponse[cartItemEntries] as $item){
			$productIds[]=$item[itemId];
		}
		return $productIds;
	}
	public function addCouponCode($couponcode,$type,$paramsArray=false){
		$this->serviceCalled = false;
		$this->operations["coupon"]["action"]="add";
		$this->operations["coupon"][code]=$couponcode;
	}
	public function addCouponProperty($couponcode,$type,$property,$value){
	}
	public function getAllCouponCodes($type){
		if ($this->serviceCalled == false) {
			$this->doServiceCall ();
		}
		$couponArray = array ();
		if (! empty ( $this->cartResponse [appliedCoupons] [0] )) {
			$coupon = $this->cartResponse [appliedCoupons] [0][coupon];
			$couponArray [$coupon] [code] = $coupon;
			$couponArray [$coupon] [expiry] = $this->cartResponse [appliedCoupons] [0] [expiryDate] / 1000;
			$couponArray [$coupon] [acode] = $this->cartResponse [appliedCoupons] [0][applicationState] [state] === "SUCCESS" ? \MCart::$__COUPON_APP_CODE_SUCCESS : \MCart::$__COUPON_APP_CODE_FAILED;
			$couponArray [$coupon] [emsg] = $this->cartResponse [appliedCoupons] [0] [applicationState] [message];
            $couponArray [$coupon] [description] = $this->cartResponse [appliedCoupons] [0] [description];
            $couponArray [$coupon] [groupType] = $this->cartResponse [appliedCoupons] [0] [groupType];

        }
		return $couponArray;
	}
	public function removeCoupon($couponCode,$type){
		$this->serviceCalled = false;
		$this->operations["coupon"]["action"]="remove";
		$this->operations["coupon"][code]=$couponCode;
	}
	protected function setCouponApplicationCode($couponCode,$type,$code,$errorMsg=""){
	}
	public function getCouponApplicationCode($couponCode,$type){
		if($this->serviceCalled== false){
			$this->doServiceCall();
		}
		if($this->cartResponse[appliedCoupons][0][applicationState][state]==="SUCCESS"){
			return self::$__COUPON_APP_CODE_SUCCESS;
		}else{
			return self::$__COUPON_APP_CODE_FAILED;
		}
	}
	public function getCouponApplicationErrorMessage($couponCode,$type){
		if($this->serviceCalled== false){
			$this->doServiceCall();
		}
		return $this->cartResponse[appliedCoupons][0][applicationState][message];
	}
	public function getCouponParams($couponCode,$type){
	}
	public function clearAllCoupons($type){
	}
	public function getAllSkus() { //TODO to verify working
		if($this->serviceCalled== false){
			$this->doServiceCall();
		}
		$skus = array();
		foreach($this->cartResponse[cartItemEntries] as $item){
			$skus[$item[selectedSize][skuId]] = $item[selectedSize];
			$skus[$item[selectedSize][skuId]][id] = $item[selectedSize][skuId];
		}
		return $skus;
	}
	public function getProductsForCombo($comboid){
	}
	public function getJsonForDiscount(){
	}
	public function getCartLevelDiscount()
	{
		if($this->serviceCalled== false){
			$this->doServiceCall();
		}
		return $this->cartResponse[bagDiscountDecimal];
	}
	public function setDiscountFields($discountedCart){
	}
	public function getCartSubtotal(){
		if($this->serviceCalled== false){
			$this->doServiceCall();
		}
		return $this->cartResponse[subTotalPriceDecimal];
	}
	public function getDiscountedItemsSubtotal(){
	}
	public function getNonDiscountedItemsSubtotal(){
		if($this->serviceCalled== false){
			$this->doServiceCall();
		}
		$subtotal = 0;
		foreach($this->cartResponse[cartItemEntries] as $item){
			if($item[totalDiscount]<=0){
				$subtotal+=$item[subTotalDecimal];
			}
		}
		return $subtotal;
	}
	public function getCouponNotApplicableItemsSubtotal(){
	}
	public function getCouponApplicableItemsSubtotal(){
		if($this->serviceCalled== false){
			$this->doServiceCall();
		}
		$subtotal = 0;
		foreach($this->cartResponse[cartItemEntries] as $item){
			if($item[couponApplicable]===true){
				$subtotal+=$item[subTotalDecimal];
			}
		}
		return $subtotal;
	}
	public function getCouponApplicableSubtotal($applicableItemPrices){
	}
	protected function reCalculatePricesAndDiscounts($forceDiscountApiCall){
	}
	public function calculateVat() {
	}
	public function clearVat() {
	}
	public function clearProductsDiscount(){
	}
	public function clearCouponDiscount(){
	}
	public function getTotalCouponDiscount(){
		if($this->serviceCalled== false){
			$this->doServiceCall();
		}
		return $this->cartResponse[totalCouponDiscountDecimal];
	}
	public function enableMyntCashUsage($amount){
		$this->serviceCalled = false;
		$this->operations["myntcash"][enabled]=true;
	}
	public function disableAutoApplyCoupon(){
	}
	public function removeMyntCashUsage(){
		$this->serviceCalled = false;
		$this->operations["myntcash"][enabled]=false;
	}
	public function isMyntCashUsed(){
		if($this->serviceCalled== false){
			$this->doServiceCall();
		}
		return $this->cartResponse[useMyntCash];
	}
	public function getTotalMyntCashUsage()
	{
		if($this->serviceCalled== false){
			$this->doServiceCall();
		}
		return $this->cartResponse[totalMyntCashUsageDecimal];
	}
	public function getCashDiscount(){
		if($this->serviceCalled== false){
			$this->doServiceCall();
		}
		return $this->cartResponse[totalMyntCashUsageDecimal];
	}
	public function setMyntCashUsage(){
	}
	public function setGiftOrShippingMyntCashUsage($amount){
	}
	public function getLoyaltyPointsAwarded(){
		if($this->serviceCalled== false){
			$this->doServiceCall();
		}
		return $this->cartResponse[loyaltyPointsAwarded];
	}

	public function getLoyaltyPointsCashUsed(){
		if($this->serviceCalled== false){
			$this->doServiceCall();
		}
		return $this->cartResponse[loyaltyPointsUsedCashEqualent];
	}

	public function getLoyaltyPointsUsed(){
		if($this->serviceCalled== false){
			$this->doServiceCall();
		}
		return $this->cartResponse[loyaltyPointsUsed];
	}

	public function getLoyaltyPointsConversionFactor(){
		if($this->serviceCalled== false){
			$this->doServiceCall();
		}
		return $this->cartResponse[loyaltyPointsConversionFactor];
	}
	public function getLoyaltyPointsCartSpecificAwardFactor(){
	}
	public function getLoyatltyPointsUserSpecificAwardFactor(){
	}
	public function getUseLoyaltyPoints(){
		if($this->serviceCalled== false){
			$this->doServiceCall();
		}
		return $this->cartResponse[useLoyaltyPoints];
	}
	public function setLoyaltyPointsAwarded($awardFactor = 0){
	}
	public function setLoyaltyPointsUsage($conversionFactor){
	}
	public function setGiftOrShippingLoyaltyPointsUsage($points){
	}
	public function getGiftOrShippingLoyaltyPointsUsageCashEqualent(){
	}
	public function disableLoyaltyPointsUsage(){
		$this->serviceCalled = false;
		$this->operations["loyaltyPointsUsage"][enabled]=false;
	}
	public function enableLoyaltyPointsUsage($userEnteredPoints = 0){
		$this->serviceCalled = false;
		$this->operations["loyaltyPointsUsage"][enabled]=true;
	}
	public function getCartCreateTimestamp(){
	}
	public function setCartCreateTimestamp($cartCreateTimestamp){
	}
	public function getIsGiftOrder(){
		if($this->serviceCalled== false){
			$this->doServiceCall();
		}
		return $this->cartResponse[isGiftOrder];
	}
	public function setIsGiftOrder($isGiftOrder){
		$this->serviceCalled = false;
		$this->operations["giftmessage"][enabled] = $isGiftOrder;
	}
	public function getGiftCharge(){
		if($this->serviceCalled== false){
			$this->doServiceCall();
		}
		return $this->cartResponse[giftCharge];
	}
	public function setGiftCharge($giftCharge){
	}

	public function getGiftMessage(){
		if($this->serviceCalled== false){
			$this->doServiceCall();
		}
		return $this->cartResponse[giftMessage][recipient]."___".$this->cartResponse[giftMessage][sender]."___".$this->cartResponse[giftMessage][message];
	}
	public function setGiftMessage($giftMessage){
		$this->serviceCalled = false;
		$this->operations["giftmessage"][enabled]=true;
		$this->operations["giftmessage"]["message"]=$giftMessage;
	}
	public function isCartNew(){
		return false;
	}
	public function setCartNew($bool){
	}
	public function beforeSerialize(){
	}
	public function afterDeSerialize(){
	}
	public function toJson() {
	}
	public function createArrayFromObj() {
	}
	public function fromJson($jsonArray,$context){
	}
	private function getCartItemsFromArray($productsArray,$cartContext){
	}
	public function getMrp(){
		if($this->serviceCalled== false){
			$this->doServiceCall();
		}
		return $this->cartResponse[totalMrpDecimal];
	}
	public function setMrp($mrp){
	}
	public function getVat()
	{
		return 0;
		/* if($this->serviceCalled== false){
			$this->doServiceCall();
		}
		return $this->cartResponse[vatCharge]; */
	}
	public function getAdditionalCharges()
	{
		if($this->serviceCalled== false){
			$this->doServiceCall();
		}
		return $this->cartResponse[vatCharge];
	}
	
	public function setAdditionalCharges($charges){
	}
	public function getDiscount(){
		if($this->serviceCalled== false){
			$this->doServiceCall();
		}
		return $this->cartResponse[totalCouponDiscountDecimal];
	}

	public function getDiscountAmount(){
		if($this->serviceCalled== false){
			$this->doServiceCall();
		}
		return $this->cartResponse[totalDiscountDecimal];
	}
	public function getProducts()
	{
		if($this->serviceCalled== false){
			$this->doServiceCall();
		}
		if(empty($this->productsArray)){
			foreach ( $this->cartResponse [cartItemEntries] as $item ) {
				$this->productsArray [$item [itemId]] = new ServiceCartItem ($this, $item );
			}
		}
		return $this->productsArray;
	}
	
	public function getProduct($id)
	{
		$products = $this->getProducts();
		return $products[$id];
	}
	
	public function getProductQtyArray()
	{
		if($this->serviceCalled== false){
			$this->doServiceCall();
		}
		$productArray = array();
		foreach($this->cartResponse[cartItemEntries] as $item){
			$productArray[$item[itemId]]= $item[quantity];
		}
		return $productArray;
	}
	
	public function getItemQuantity()
	{
		$count = 0;
		foreach ($this->getProductQtyArray() as $id=>$qty) {
			$count += $qty;
		}
	
		return $count;
	}
	
	public function setCouponDiscount($discount)
	{}

	public function setCashCouponDiscount($discount)
	{}

	public function clear(){
	}
	public function clearDiscount(){
	}
	public function clearCashDiscount(){
	}
	public function getCashBackAmountForDisplay(){
		return number_format($this->getCashDiscount(), 2, ".", '');	 
	}
	public function getTotalCashBackAmount(){
		return number_format($this->getCashDiscount(), 2, ".", '');
	}
	public function getCashbackToGivenOnCart(){
		return 0;
	}
	
	public function deconstructCart(&$productsInCart){}
	
	public function toArray(){}
	
	public function getSetDisplayData(){
		if($this->serviceCalled== false){
			$this->doServiceCall();
		}
		$array = array();
		foreach($this->cartResponse[setDisplayData] as $comboId=>$displayData){
			$array[$comboId] = $this->convertToDisplayData($this->cartResponse[$displayData][rawDiscountDataEntry]);
		}
		return $array;
	}
	
	public function getDisplayData(){
		if($this->serviceCalled== false){
			$this->doServiceCall();
		}
		return $this->convertToDisplayData($this->cartResponse[displayData][rawDiscountDataEntry]);
	}
	
	public function getSlabDisplayData(){
		if($this->serviceCalled== false){
			$this->doServiceCall();
		}
		return $this->convertToDisplayData($this->cartResponse[slabBasedDisplayData][rawDiscountDataEntry]);
	}
	public static function convertToDisplayData($rawDisplayData){
		$array = new \stdClass();
		$array->discountId=$rawDisplayData[discountId];
		$array->id=$rawDisplayData[id];
		$array->tooltip_id=$rawDisplayData[toolTipId];
		$array->tooltip_exclusion_id=$rawDisplayData[toolTipExclusionId];
		$array->icon=$rawDisplayData[iconId];
		$array->params= new \stdClass();
		foreach($rawDisplayData[params] as $key=>$value){
			$array->params->$key=$value;
		}
		return $array;
	}
	
	public function setItemCustomizationEnabled($item,$enabled){
		$this->serviceCalled = false;
		$this->operations[item]["customize"][$item][enabled]= $enabled;
	}
	
	public function setItemCustomizationMessage($item,$msg){
		$this->serviceCalled = false;
		$this->operations[item]["customize"][$item][message]= $msg;
	}
	
	public function setItemCustomizationNumber($item,$num){
		$this->serviceCalled = false;
		$this->operations[item]["customize"][$item][number]= $num;
	}
	
	public function setItemCustomizationName($item,$name){
		$this->serviceCalled = false;
		$this->operations[item]["customize"][$item][name]= $name;
	}
	private function doServiceCall(){
		if(!empty($this->operations)){
			$this->commitOperations();
		}else{
			if($this->cartContext=="saved"){
				$this->cartResponse = \MCartUtils::doWishListGet();
			}else{
				if(empty($this->login))
					$this->cartResponse = \MCartUtils::doCartGet($this->cartContext,$this->realtime);
				else
					$this->cartResponse = \MCartUtils::doCartGetByLogin($this->login,$this->cartContext);
			}
		}
		if($this->cartContext=="saved"){
			$this->cartResponse[cartID]=$this->cartResponse[wishListID];
			$this->cartResponse[cartItemEntries]=$this->cartResponse[wishListItemEntries];
		}
		$this->productsArray = null;
		$this->serviceCalled = true;
		//$this->refreshItemsAvailability(true);
	}
	
	public function commitOperations(){
		foreach ($this->operations as $key =>$operation){
			$call = array();
			if($key == "item"){
				foreach($operation["remove"] as $value){
					$a=array();
					$a[itemId]= $value;
					$a[operation]="DELETE";
					$call[] = $a;
				}
				foreach($operation["add"] as $value){
					$a=array();
					$a[skuId]= $value[skuId];
					$a[operation]="ADD";
					$a[quantity]=$value[quantity];
					$a[styleId]=$value[styleId];
					$call[] = $a;
				}
				foreach($operation["update"] as $value){
					$a=array();
					$a[skuId]= $value[skuId];
					$a[itemId]= $value[itemId];
					$a[operation]="UPDATE";
					$a[quantity]=$value[quantity];
					$a[styleId]=$value[styleId];
					$call[] = $a;
				}
				foreach($operation["customize"] as $itemId => $value){
					$a=array();				
					$a[itemId]= $itemId;
					$a[customName]=$value[name];
					$a[doCustomize]=$value[enabled];
					$a[customNumber]=$value[number];
					$a[customizedMessage]=$value[message];
					$call[] = $a;
				}
				if($this->cartContext==="saved"){
					$result = \MCartUtils::doWishListUpdate($call);
					if($result!=null){
						$this->cartResponse = $result;
					}
				}
				else{
					$result = \MCartUtils::doCartUpdate($call);
					if($result!=null){
						$this->cartResponse = $result;
					}
				}
			}elseif($this->cartContext==="saved"){
				continue;	
			}elseif($key=="coupon"){
				if($operation[action]=="remove"){
					$result = \MCartUtils::doCartRemoveCoupon(array(coupon=>$operation[code]),$this->cartContext);
					if($result!=null){
						$this->cartResponse = $result;
					}
				}elseif($operation[action]=="add"){
					$result = \MCartUtils::doCartApplyCoupon(array(coupon=>$operation[code]),$this->cartContext);
					if($result!=null){
						$this->cartResponse = $result;
					}
				}
			}elseif($key=="loyaltyPointsUsage"){
				if($operation[enabled]===true){
					$result = \MCartUtils::doCartApplyLoyaltyPoints(array(usePoints=>0),$this->cartContext);
					if($result!=null){
						$this->cartResponse = $result;
					}
				}else{
					$result = \MCartUtils::doCartRemoveLoyaltyPoints($this->cartContext);
					if($result!=null){
						$this->cartResponse = $result;
					}
				}
			}elseif($key=="myntcash"){
				if($operation[enabled]===true){
					$result = \MCartUtils::doCartApplyMyntCredits(array(useAmount=>100),$this->cartContext);
					if($result!=null){
						$this->cartResponse = $result;
					}
				}else{
					$result = \MCartUtils::doCartRemoveMyntCredits($this->cartContext);
					if($result!=null){
						$this->cartResponse = $result;
					}
				}
			}elseif($key=="giftmessage"){
				if($operation[enabled]===true){
					$split = split("___", $operation[message]);
					$result = \MCartUtils::doCartApplyGiftMessage(array(recipient=>$split[0],sender=>$split[1],message=>$split[2]),$this->cartContext);
					if($result!=null){
						$this->cartResponse = $result;
					}
				}else{
					$result = \MCartUtils::doCartRemoveGiftMessage($this->cartContext);
					if($result!=null){
						$this->cartResponse = $result;
					}
				}
			}
		}
		$this->operations = null;
		$this->serviceCalled = true;
	}
}