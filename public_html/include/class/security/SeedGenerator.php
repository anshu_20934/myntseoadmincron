<?php

namespace security;

use security\Random;

class SeedGenerator {

	function __construct(){
	}

	public function generateRandomSeed($seed, $len, $version) {
		$randSeed = "";
		if($version == 1){
			$randSeed = $this->getRandFromSeedUsingRNG($seed, $len);
		}else if($version == 2){
			$randSeed = $this->generateSeedRandomly($seed, $len);
		}
		return $randSeed;
	}

	/*
	  Generates a seeded random string of length >= $len
	  Generated random string is completely determined by the value of /4seed and $len
	*/

	//using Random Number Generator
	private function getRandFromSeedUsingRNG($seed, $len){
		srand($seed);
		$gen_str_seed = "abcdefghijklmnopqustuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ987654321!@#$^%&*(()+-={}<>;:?";
		$gen_str = str_repeat( $gen_str_seed, floor($len/strlen($gen_str_seed))+1 );
	  
		$ret = substr(str_shuffle($gen_str), 0, $len); 

	  	srand(time());

	  	return $ret;
	}

	private function generateSeedRandomly($seed, $len) {
		$rand = new Random($seed);

		$gen_str_seed = "abcdefghijklmnopqustuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ987654321!@#$^%&*(()+-={}<>;:?";
		$gen_str = str_repeat( $gen_str_seed, floor($len/strlen($gen_str_seed))+1 );
	  
		$randomStr = $rand->str_shuffle($gen_str);
		
		/* Now lets start from the modulo of the random str length  and then have a circular substr*/
		$start = $seed % strlen($randomStr); 
		$ret = $this->substr_circular($randomStr, $start, $len);

		$rand->seed(time());		

		return $ret;
	}	

	private function substr_circular($str, $start, $len){
		if($start + $len > strlen($str)){
			$substr1 = substr($str, $start);
			$substr2 = substr($str, 0, ($len - strlen($substr1)));
			return $substr1 . $substr2;
		}else{
			return substr($str, $start, $len);
		}
	}

}; 



?>
