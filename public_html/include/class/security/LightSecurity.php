<?php

namespace security;

use security\SeedGenerator;
use security\Random;

/* -----------------------------------Light Weight Encryption and Decryption--------------------------------------------------------*/

/*  
  Secret key for this technique is a prime number 
  we can find the list of prime numbers at http://primes.utm.edu/lists/small/millions/
  Secret key of len no more than 9 digits should suffice
  keys are retrieved through config
  format of storing keys is : version_0:key0|version_1:key1|.....|version_n:keyn
  eg : 0:43|1:17
*/

class LightSecurity {
    const salt = 'ACQ9y^cU!X';
    const lightEncryptionKeys = "0:694847999";

	//for given (a,b) it returns (d, x, y) s.t (ax+by-1)%d = 0, where d = gcd(a,b)
	private static function extended_euclid($a, $b){
		if($b == 0) return array($a, 1, 0);
		$rem = $a%$b;
		if($rem < 0) $rem += $b; 
		$tmp = self::extended_euclid($b, $rem);
		return array($tmp[0], $tmp[2], $tmp[1]-floor($a/$b)*$tmp[2]);
	}


	// given k and p, it returns ki s.t (k*ki-1)%p = 0
	private static function modinv($k, $p){
		$res = self::extended_euclid($k, $p);
		$inv = $res[1] % $p;
		if($inv < 0) $inv += $p;
		return $inv;
	}

	/*
	  take a xor of two strings, $msg and $key,
	  this xor is character by character
	  if strlen($msg) > strlen($key), we keep appending initial value of $key to the end  
	*/
    private static function stringXOR($s1, $s2) {
        $m = strlen($s1) >= strlen($s2) ? $s2 : $s1;
        $fxr = "";
        for($i = 0, $n = strlen($m); $i < $n; $i++) {
            $fxr += chr(ord($s1[$i]) ^ ord($s2[$i]));
        }
        return $fxr;
    }

	private static function xorMessage($msg, $key){
		$encodedMsg = "";
	  
		$l1 = strlen($msg) ; $l2 = strlen($key);
		$t1 = floor($l1/$l2); $t2 = $l1%$l2;
	  
		for($i=0;$i<$t1;$i++){
			$encodedMsg .= $key ^ substr($msg, $i*$l2, $l2);
		}
		if($t2) $encodedMsg .= substr($msg, $l2*$t1, $t2) ^ substr($key, 0, $t2);
		return $encodedMsg;
	}

	//this function fetch the secret key for a given version, if version is not given it picks the latest one.
	//return type is (version, key)
	private static function getLESecretKey($k_v = 0){
		$keyString = self::lightEncryptionKeys;
		$keys = explode("|", $keyString);

	  	$keyMap = array(); $currMap = array();
	 
		foreach($keys as $k){
			$currMap = explode(":", $k);
			$keyMap[$currMap[0]] = $currMap[1];
		}
	 
		if(empty($k_v)) return $currMap;
		return array($k_v, $keyMap[$k_v]);
	}

	/*
	  encrypts a string $msg 
	  k_v(optional) is version of secret key to be used, if not given latest version is used
	*/
	public static function encrypt($msg, $version = 1) {
	  	if (empty($msg)) return "";
        if (empty($version)) $version = 1;

		$LESecretKey = self::getLESecretKey();
	  	$p = $LESecretKey[1];

        if ($version == 1) {
            $k = rand(2, $p-2);
        }
        else if ($version == 2) {
            $rand = new Random(time());
            $k = abs($rand->rand());
        }
        $ki = self::modinv($k, $p);
	  
	  	$seedGenerator = new SeedGenerator();
		$padding = $seedGenerator->generateRandomSeed($ki, strlen($msg), $version);
		
        $xor = self::xorMessage($msg, $padding);
        $finalStr = $xor."@".$LESecretKey[0]."#".$k."$".$version;
        $encoded = base64_encode($finalStr);

        if ($version > 1) {
            $sign = md5($encoded . self::salt);
            $retVal = $encoded . '.' . $sign;
        }
        else {
            $retVal = $encoded;
        }
        // error_log(sprintf("encrypt msg = %s p = %d k = %d ki = %d ver = %d pad = %s ret = %s\n", 
        //         $msg, $p, $k, $ki, $version, bin2hex($padding), $retVal));

        // if (php_sapi_name() == 'cli') {
        //    file_put_contents('php://stderr', sprintf("php encrypt version: %d output: %s\n", $version, $retVal));
        // }
        return $retVal;
	}

	/*
	  decrypts the given message
	  versioninng of key is auto handled
	  returns empty is the strings given does now follow light encryption format
	*/
	public static function decrypt($msg){
		
	  	if(empty($msg)) return "";
        if (strpos($msg, '@') !== false) return "";

        $dotPos = strrpos($msg, '.');
        if ($dotPos > 0) {
            $signActual = substr($msg, $dotPos + 1);
            $msg = substr($msg, 0, $dotPos);
            $signExpected = md5($msg . self::salt);
        }

	  	$msg = base64_decode($msg);
	  	$key_pos = strrpos($msg, '#');
		$key = substr($msg, $key_pos+1);

		// See if there is the version string in key, if so split it
		$pos = strrpos($key, '$');
	    if($pos){
	    	$key_orig = $key;
	    	$key = substr($key_orig, 0, $pos + 1);
	    	$key = intval($key);
	    	$version = substr($key_orig, $pos + 1, 1);
	    }else{
	    	$version = 1;
	    }

        // check the signature, for the version 2 encryptions
        if ($version > 1) {
            if ($dotPos === false) return "";
            if ($signActual !== $signExpected) return '';
        }

		$stripped_msg = substr($msg, 0, $key_pos); 
		$key_v_pos = strrpos($stripped_msg, '@');
		$key_v = substr($stripped_msg, $key_v_pos+1);
	  
	  	if(!(ctype_digit($key_v) and ctype_digit($key))) // format of $msg is incorrect
	    	return "";

		$stripped_msg = substr($stripped_msg, 0, $key_v_pos);
	  
        $k_v = (int)$key_v;
        $k = (int)$key;

		$LESecretKey = self::getLESecretKey($k_v);
	  	$p = $LESecretKey[1];
		$ki = self::modinv($k, $p);
		
	  	$seedGenerator = new SeedGenerator();
	  	
	  	/** Decryption calls will depend upon the version on which the whole string was encrypted **/
		$padding = $seedGenerator->generateRandomSeed($ki, strlen($stripped_msg), $version);
	  
        $finalStr = self::xorMessage($stripped_msg, $padding);
        // error_log(sprintf("decrypt msg = %s p = %d k = %d ki = %d ver = %d pad = %s ret = %s\n", 
        //         bin2hex($stripped_msg), $p, $k, $ki, $version, bin2hex($padding), bin2hex($finalStr)));

        // if (php_sapi_name() == 'cli') {
        //    file_put_contents('php://stderr', sprintf("php decrypt version: %d output: %s\n", $version, $finalStr));
        // }
        return $finalStr;
	}

	/*--------------------------------------------------------------------------------------------------------------------*/    


}

?>
