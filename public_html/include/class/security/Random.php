<?php

/*
   A C-program for MT19937, with initialization improved 2002/1/26.
   Coded by Takuji Nishimura and Makoto Matsumoto.
 
   Before using, initialize the state by using init_genrand(seed)  
   or init_by_array(init_key, key_length).
 
   Copyright (C) 1997 - 2002, Makoto Matsumoto and Takuji Nishimura,
   All rights reserved.                          
 
   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions
   are met:
 
     1. Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer.
 
     2. Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.
 
     3. The names of its contributors may not be used to endorse or promote 
        products derived from this software without specific prior written 
        permission.
 
   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
   CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 
 
   Any feedback is very welcome.
   http://www.math.sci.hiroshima-u.ac.jp/~m-mat/MT/emt.html
   email: m-mat @ math.sci.hiroshima-u.ac.jp (remove space)
*/

namespace security;

/**
The algorithm implemented here is Mersenne Twister Pseudo Random Number Generator 
For more details and Pseudo code please refer http://en.wikipedia.org/wiki/Mersenne_twister

Mersenne Twister Home Page - http://www.math.sci.hiroshima-u.ac.jp/~m-mat/MT/emt.html

Author of this file: Narendra Mudigal
**/

const N = 624;
const M = 397;
const MATRIX_A = 0x9908b0df;
const UPPER_MASK = 0x80000000;
const LOWER_MASK = 0x7fffffff;
const INT_32_MAX = 0x7fffffff;

function define_constants() {
  $code = "";
  $namespace = __NAMESPACE__;

  foreach (array(10, 11, 12, 14, 20, 21, 22, 26, 27, 31) as $n) {
    $val = ~((~0) << $n);
    $code .= "namespace $namespace { const MASK$n = $val; }\n";
  }

  foreach (array(16, 31, 32) as $n) {
    $val = pow(2, $n);
    $code .= "namespace $namespace { const TWO_TO_THE_$n = $val; }\n";
  }

  eval($code);

  $val = MASK31 | (MASK31 << 1);
  eval("namespace $namespace { const MASK32 = $val; }");
}

define_constants();

class Random {
  
	# the class constant is not used anywhere in this namespace,
	# but it makes the API cleaner.

	function __construct() {
		//$this->bits32 = PHP_INT_MAX == 2147483647;

		if(func_num_args() == 1) {
			$this->seed(func_get_arg(0));
        }
        else {
            $arg0 = time();
            //echo(sprintf("mynt:security Random() seeded with %s\n", $arg0));
            $this->seed($arg0);
        }
	}

	public function seed($o_integer_seed) {
		$integer_seed = $this->force_32_bit_int($o_integer_seed);
        //echo(sprintf("mynt:security Random() o_integer_seed = %s integer_seed = %s\n", $o_integer_seed, $integer_seed));

		$mt = &$this->mt;
		$mti = &$this->mti;

		$mt = array_fill(0, N, 0);

		$mt[0] = $integer_seed;

		for($mti = 1; $mti < N; $mti++) {
			$mt[$mti] = $this->add_2($this->mul(1812433253,
				($mt[$mti - 1] ^ (($mt[$mti - 1] >> 30) & 3))), $mti);
			/*
			mt[mti] =
			(1812433253UL * (mt[mti-1] ^ (mt[mti-1] >> 30)) + mti);
			*/
		}
	}

	/* for int32 */
	public function rand() {
		static $mag01 = array(0, MATRIX_A);

		$mt = &$this->mt;
		$mti = &$this->mti;

		if ($mti >= N) { /* generate N words all at once */
			for ($kk=0;$kk<N-M;$kk++) {
				$y = ($mt[$kk]&UPPER_MASK)|($mt[$kk+1]&LOWER_MASK);
				$mt[$kk] = $mt[$kk+M] ^ (($y >> 1) & MASK31) ^ $mag01[$y & 1];
			}
			for (;$kk<N-1;$kk++) {
				$y = ($mt[$kk]&UPPER_MASK)|($mt[$kk+1]&LOWER_MASK);
				$mt[$kk] =
					$mt[$kk+(M-N)] ^ (($y >> 1) & MASK31) ^ $mag01[$y & 1];
			}
		
			$y = ($mt[N-1]&UPPER_MASK)|($mt[0]&LOWER_MASK);
			$mt[N-1] = $mt[M-1] ^ (($y >> 1) & MASK31) ^ $mag01[$y & 1];

			$mti = 0;
		}

		$y = $mt[$mti++];

		/* Tempering */
		$y ^= ($y >> 11) & MASK21;
		$y ^= ($y << 7) & ((0x9d2c << 16) | 0x5680);
		$y ^= ($y << 15) & (0xefc6 << 16);
		$y ^= ($y >> 18) & MASK14;

		return $y;
	}

	private function rand_range($n, $min, $max, $tmax) {

		$range = $max - $min;
		$reminder = $n % $range;
		return $min + $reminder;
	}

	public function str_shuffle($str) {

		/* The implementation is stolen from array_data_shuffle       */
		/* Thus the characteristics of the randomization are the same */
		$n_elems = strlen($str);

		if ($n_elems <= 1) {
			return null;
		}

		$n_left = $n_elems;

		while (--$n_left) {
			$rnd_idx = $this->rand();
			$rnd_idx = $this->rand_range($rnd_idx, 0, $n_left, INT_32_MAX);
			if ($rnd_idx != $n_left) {
				$temp = $str[$n_left];
				$str[$n_left] = $str[$rnd_idx];
				$str[$rnd_idx] = $temp;
			}
		}

		return $str;

	}
	/*
	takes 2 integers, treats them as unsigned 32-bit integers,
	and adds them.

	it works by splitting each integer into
	2 "half-integers", then adding the high and low half-integers
	separately.

	a slight complication is that the sum of the low half-integers
	may not fit into 16 bits; any "overspill" is added to the sum
	of the high half-integers.
	*/ 
	private function add_2($n1, $n2) {
		$x = ($n1 & 0xffff) + ($n2 & 0xffff);

		return 
			(((($n1 >> 16) & 0xffff) + 
			(($n2 >> 16) & 0xffff) + 
			($x >> 16)) << 16) | ($x & 0xffff);
	}

	private function force_32_bit_int($x) {
		/*
		it would be un-PHP-like to require is_integer($x),
		so we have to handle cases like this:

		$x === pow(2, 31)
		$x === strval(pow(2, 31))

		we are also opting to do something sensible (rather than dying)
		if the seed is outside the range of a 32-bit unsigned integer.
		*/

		if(is_integer($x)) {
			/* 
			we mask in case we are on a 64-bit machine and at least one
			bit is set between position 32 and position 63.
			*/
			return $x & MASK32;
		} else {
			$x = floatval($x);

			$x = $x < 0? ceil($x): floor($x);

			$x = fmod($x, TWO_TO_THE_32);

			if($x < 0)
			$x += TWO_TO_THE_32;

			return $this->unsigned2signed($x);
		}
	}

	private function unsigned2signed($unsigned_integer) {
		## assert($unsigned_integer >= 0);
		## assert($unsigned_integer < pow(2, 32));
		## assert(floor($unsigned_integer) === floatval($unsigned_integer));

		return intval($unsigned_integer < TWO_TO_THE_31? $unsigned_integer:
		$unsigned_integer - TWO_TO_THE_32);
	}

	private function mul($a, $b) {
		/*
		a and b, considered as unsigned integers, can be expressed as follows:

		a = 2**16 * a1 + a2,

		b = 2**16 * b1 + b2,

		where

		0 <= a2 < 2**16,
		0 <= b2 < 2**16.

		given those 2 equations, what this function essentially does is to
		use the following identity:

		a * b = 2**32 * a1 * b1 + 2**16 * a1 * b2 + 2**16 * b1 * a2 + a2 * b2

		note that the first term, i.e. 2**32 * a1 * b1, is unnecessary here,
		so we don't compute it.

		we could make the following code clearer by using intermediate
		variables, but that would probably hurt performance.
		*/

		return
			$this->unsigned2signed(fmod(TWO_TO_THE_16 *
											/*
											the next line of code calculates a1 * b2,
											the line after that calculates b1 * a2, 
											and the line after that calculates a2 * b2.
											*/
											((($a >> 16) & 0xffff) * ($b & 0xffff) +
											(($b >> 16) & 0xffff) * ($a & 0xffff)) +
											($a & 0xffff) * ($b & 0xffff),

									TWO_TO_THE_32));
	}	


}



?>
