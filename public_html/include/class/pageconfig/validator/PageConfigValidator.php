<?php
namespace pageconfig\validator;
use enums\pageconfig\PageConfigLocationConstants;

use base\dataobject\validator\Validator;
use pageconfig\exception\ImageValidationException;
use enums\base\ActionType;

class PageConfigValidator implements Validator { 

	private  $images, $imageRule, $currentImageCountInDB, $mode, $variantFlag;
	const delta = 0;//difference in pixels in image height and width for tolerance
	public function __construct(array &$images, $imageRule, $currentImageCountInDB, $mode = ActionType::Add, $variantFlag = false) {
		$this->images = $images;
		$this->imageRule = $imageRule;
		$this->currentImageCountInDB = $currentImageCountInDB;
		$this->mode = $mode;
		$this->variantFlag = $variantFlag;
	}
	
	public function validate($locationType="") {
		/*
			Modified in order to support storeinstore feature and varying height of landing page sub-banners
			If image_rule['height'] is -1 , validateBatchImageSize method checks if the batch of 3 images have the same non-zero height and width = image_rule['width']
		*/
		if($this->imageRule["height"] == -1){
			error_log("Batch Image height check");
			if($this->mode === ActionType::Add || $this->mode === ActionType::Update) {
				foreach ($this->images as &$eachImage) {
					if($eachImage['image_is_uploaded'] || !($locationType===PageConfigLocationConstants::SearchPage)){
						$this->validateImageFile($eachImage);
					}
				}
				$this->validateBatchImageSize($this->images);
			}
		}else{
			if($this->mode === ActionType::Add || $this->mode === ActionType::Update) {
				foreach ($this->images as &$eachImage) {
					if($eachImage['image_is_uploaded'] || !($locationType===PageConfigLocationConstants::SearchPage)){						
						$this->validateImageFile($eachImage);
						$this->validateImageSize($eachImage);
						$eachImage["target_url"] = $this->transformTargetURL($eachImage);				
					}
				}
			}
		}	
		if(!$this->variantFlag){
			if($this->mode === ActionType::Add || $this->mode === ActionType::Delete) {
				$this->validateImageCounts();
			}
		}
	
	}
	
	public function validateImageFile(array $image) {
		$isFile = $image["source"] === "file";
		
		if($isFile && $image["image_file_error"] != 0) {
			throw new ImageValidationException("Image file has errors: ","file-error", $image);
		}
		
/* 		$fileType = explode("/", $image["image_file_type"]);
		if($fileType[0] != "image") {
			throw new ImageValidationException("Uploaded file is probably not an image: ","file-not-image", $image);
		}
 */		
		if($image["image_file_size"] > $this->imageRule["size"]) {
			throw new ImageValidationException("Image file size limit exceeded: ","file-size-limit", $image, $this->imageRule);
		}
	}
	
	public function validateImageSize(array $image) {
		$reqdHeight = $this->imageRule["height"];
		$reqdWidth = $this->imageRule["width"];
		if(abs($reqdHeight - $image["height"]) > self::delta || abs($reqdWidth - $image["width"]) > self::delta) {
			throw new ImageValidationException("Image Size Validation failed: ","size",$image, $this->imageRule);
		}
	}

	public function transformTargetURL(array $image) {
		return strtolower($image["target_url"]);
	}
	
	public function validateBatchImageSize(array $images) {
		$reqdWidth = $this->imageRule["width"];
		$imgCount = 0 ;
		foreach ($images as &$eachImage) {
			if($eachImage['image_is_uploaded'] || !($locationType===PageConfigLocationConstants::SearchPage)){
				if(abs($reqdWidth - $eachImage["width"]) > self::delta || $eachImage["height"] == self::delta) {
					throw new ImageValidationException("Image Size Validation failed: ","size",$eachImage, $this->imageRule);
				}
				$height[$imgCount] = $eachImage["height"];
				$imgCount++;
			}
		}
		if(count(array_count_values($height)) != 1){
				throw new ImageValidationException("Image Size Validation failed: ","size",$images, $this->imageRule);
		}
	}
	
	public function validateImageCounts() {
		$numNewImages = count($this->images);
		$step = $this->imageRule["step"];
		if($numNewImages != $step) {
			throw new ImageValidationException("Image Count Validation failed:", "count-step", $this->images, $this->imageRule);
		}
		$maxReq = $this->imageRule["max"];
		$minReq = $this->imageRule["min"];
		$newNumber = ($this->mode == ActionType::Add) ? $this->currentImageCountInDB + $numNewImages : $this->currentImageCountInDB - $numNewImages;
		if(($this->mode == ActionType::Delete) && ($newNumber < $minReq)) {
			throw new ImageValidationException("Image Count Validation failed:", "count-min", $this->images, $this->imageRule);
		}
		if(($this->mode == ActionType::Add) && ($newNumber > $maxReq)) {
			throw new ImageValidationException("Image Count Validation failed:", "count-max", $this->images, $this->imageRule);
		}
	}
}
