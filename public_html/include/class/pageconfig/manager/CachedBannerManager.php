<?php
namespace pageconfig\manager;
use abtest\MABTest;
use pageconfig\dao\ImageReferenceDAO;
use base\manager\BaseManager;

require_once \HostConfig::$documentRoot.'/modules/RestAPI/RestRequest.php';


class CachedBannerManager extends BaseManager {
	/**
	 * 15 mins: banners will be queried from DB every these many seconds
	 */
	//const cacheTTL = 1500;
	public static function getCachedBanners($locationType, $section, $pageLocation) {
		global $xcache;
		global $banner_service_down;
		$personalizedBannersEnabled=\FeatureGateKeyValuePairs::getBoolean('PersonalizedBannersEnabled');
		$userGroup = CachedBannerManager::getUserGroup();
		$locationGroup = CachedBannerManager::getLocationGroup();
		$cacheKey = "banners_array_values".strtolower("$locationType-$section-$pageLocation");

		if ($personalizedBannersEnabled) {
			$cacheKey = "banners_array_values".strtolower("$locationType-$section-$pageLocation-$userGroup");
		}

		if (!empty($locationGroup)){
			$cacheKey .= strtolower("-$locationGroup");
		}
		
		$cachedValueArrayNew = $xcache->fetchObject($cacheKey);

		if(!$cachedValueArrayNew) {
				
			if ($personalizedBannersEnabled && empty($banner_service_down)) {
				$cachedValueArrayNew = CachedBannerManager::getBannersFromService($locationType,$section,urlencode($pageLocation),$userGroup,$locationGroup);
					if(empty($cachedValueArrayNew)){
						$imageReferenceDAO = new ImageReferenceDAO();
						$cachedValueArrayNew = $imageReferenceDAO->findByLocationAndSection($locationType,$section,$pageLocation);	
					}
			}else{
				$imageReferenceDAO = new ImageReferenceDAO();
				$cachedValueArrayNew = $imageReferenceDAO->findByLocationAndSection($locationType,$section,$pageLocation);	
			}

			if(empty($cachedValueArrayNew)){//in case of the entry wasnt there - eg login banner not present
				$cachedValueArrayNew = '$';
			}

			$xcache->storeObject($cacheKey, $cachedValueArrayNew);//, self::cacheTTL);
			
		}
		return $cachedValueArrayNew=='$'?null:$cachedValueArrayNew;
	}

	public static function getMultiBannerData($slideShowBanner,$locationType, $section, $pageLocation){		
		global $xcache;
		$userGroup = CachedBannerManager::getUserGroup();
		$cacheKey = "multiclick_banner_data".strtolower("$locationType-$section-$pageLocation-$userGroup");		
		$cachedValueArray = $xcache->fetchObject($cacheKey);
		if(!$cachedValueArray){
			$imageIds = array();
			$imageIdsVariant = array();
			foreach ($slideShowBanner as $banner) {
				if($banner["multiclick"]==1){
					if (empty($banner["group_id"])) {
						array_push($imageIds, $banner["image_id"]);	
					}else{
						array_push($imageIdsVariant, $banner["image_id"]);	
					}
					
				}
			}
			$imageReferenceDAO = new ImageReferenceDAO();
			$multiBannerArray = $imageReferenceDAO->multiBannerData(implode(",", $imageIds));
			$multiBannerArrayVariant = $imageReferenceDAO->multiBannerData(implode(",", $imageIdsVariant),$userGroup);
			$cachedValueArray = array();
			//First Add data for normal banner
			foreach ($imageIds as $imageId) {
				$cachedValueArray[$imageId] = array();
				foreach ($multiBannerArray as $banner) {
					if($imageId==$banner['image_id']){
						$banner['coordinates'] = explode(',', $banner['coordinates']);
						$banner['width'] = abs($banner['coordinates'][2]-$banner['coordinates'][0]);
						$banner['height'] = abs($banner['coordinates'][3]-$banner['coordinates'][1]);
						array_push($cachedValueArray[$imageId], $banner);
					}
				}
			}
			//Now add for variant
			foreach ($imageIdsVariant as $imageId) {
				$cachedValueArray[$imageId."-".$userGroup] = array();
				foreach ($multiBannerArrayVariant as $banner) {
					if($imageId==$banner['image_id']){
						$banner['coordinates'] = explode(',', $banner['coordinates']);
						$banner['width'] = abs($banner['coordinates'][2]-$banner['coordinates'][0]);
						$banner['height'] = abs($banner['coordinates'][3]-$banner['coordinates'][1]);
						array_push($cachedValueArray[$imageId."-".$userGroup], $banner);
					}
				}
			}
			$xcache->storeObject($cacheKey, $cachedValueArray);
		}
		return $cachedValueArray;
	}

	public static function getBannersFromService($locationType,$section,$pageLocation,$userGroup,$locationGroups=""){
		$url = \HostConfig::$bannerServiceUrl.'/getBanners?location_type='.$locationType.'&section='.$section.'&page_location='.$pageLocation.'&user_group='.$userGroup.'&location_groups='.$locationGroups;
		$method = 'GET';
		$data = array();
		$request = new \RestRequest($url, $method);
		$bannerServiceTimeout = \FeatureGateKeyValuePairs::getFeatureGateValueForKey('bannerServiceTimeout');
		$request->setTimeOut(empty($bannerServiceTimeout)?$bannerServiceTimeout : 5);
		$headers = array();
		$headers[] = 'Accept: application/json';
		$headers[] = 'Content-type: application/json';
		$headers[] = 'Accept-Language: en-US';
		$request->setHttpHeaders($headers);
		$request->execute();
		$info = $request->getResponseInfo();
		$body = $request->getResponseBody();
		$body = json_decode($body,true);
		
		if ($info['http_code'] == 0) {
			global $banner_service_down;
			$banner_service_down = true;
		}

		if($info['http_code'] == 200){
			if($body['personalizedBannerResponse']['status']['statusType'] == 'SUCCESS'){
				if(($body['personalizedBannerResponse']['status']['totalCount']) == 1){
					$tempArr = array();
					$tempArr[] = $body['personalizedBannerResponse']['data']['banner'];
					return $tempArr;
				}else{
					return $body['personalizedBannerResponse']['data']['banner'];		
				}
				
			}				
		}
	}

	public static function getUserGroup(){
		global $userGroupUps;
		global $login;
		if(empty($login)){
			return "";
		}
		$personalizedBannersEnabled=\FeatureGateKeyValuePairs::getBoolean('PersonalizedBannersEnabled');
		if(!$personalizedBannersEnabled){
			return "";
		}		
		if(isset($userGroupUps)){
			return $userGroupUps;
		}		
		$url = \HostConfig::$notificationsUpsUrl.'/getAttr?userid=' . $login . '&attribute=usergroupid&value=';
		$method = 'GET';
		$data = array();
		$request = new \RestRequest($url, $method);
		$headers = array();
		$headers[] = 'Accept: application/json';
		$headers[] = 'Content-type: application/json';
		$headers[] = 'Accept-Language: en-US';
		$request->setHttpHeaders($headers);
		$request->execute();
		$info = $request->getResponseInfo();
		$body = $request->getResponseBody();
		if($info['http_code'] == 200){
			$body = json_decode($body,true);
			if($body['errorCode'] == 0){
				$userGroupUps = $body['body']['usergroupid'];
				return $userGroupUps;
			}	
		}
		return "";
	}

    public static function getLocationGroup(){
        // If location exist in session just return
        global $XCART_SESSION_VARS;
        if(isset($XCART_SESSION_VARS['ipLocationGroups'])){
            $ipLocationGroupsStr =  $XCART_SESSION_VARS['ipLocationGroups'];
            $ipLocationGroupsStrArr = explode("___", $ipLocationGroupsStr);
            
            $createTime = $ipLocationGroupsStrArr[1];
            if(!empty($createTime) && (time() - $createTime)/60 < 30){
            	return $ipLocationGroupsStrArr[0];
            }
        }
        // otherwise get it from api and store it in session, 
        // first check for ab testing
        $locationBasedBanners = MABTest::getInstance()->getUserSegment("locationBasedBanner");
        if($locationBasedBanners=="test"){
        	$server_ip = get_user_ip();
        	if(!empty($server_ip)){

		        $url = \HostConfig::$locationGroupServiceUrl.'/ip/'. $server_ip;
		        $method = 'GET';
		        $data = array();
		        $request = new \RestRequest($url, $method);
		        $headers = array();
		        $headers[] = 'Accept: application/json';
		        $headers[] = 'Content-type: application/json';
		        $headers[] = 'Accept-Language: en-US';
		        $request->setHttpHeaders($headers);
		        $request->execute();
		        $info = $request->getResponseInfo();
		        $body = $request->getResponseBody();
		        if($info['http_code'] == 200){
		            $body = json_decode($body,true);
		            if(!empty($body['locationGroupResponse']['data']['LocationGroup'])){
		            	if(isset($body['locationGroupResponse']['data']['LocationGroup'][0])){
		                    $location_groups=array();
		                    foreach($body['locationGroupResponse']['data']['LocationGroup'] as $lgroup){
		                        array_push($location_groups,$lgroup['id']);
		                    }
		                    $locationGroup = implode(',',$location_groups);
		            	}else{
		            		$locationGroup = $body['locationGroupResponse']['data']['LocationGroup']['id'];
		            	}
		                if (x_session_is_registered("ipLocationGroups"))
							x_session_unregister("ipLocationGroups");
		                x_session_register("ipLocationGroups", $locationGroup . "___" . time());
		                return $locationGroup;
		            }
		        }
		    }
        }
        if (x_session_is_registered("ipLocationGroups"))
			x_session_unregister("ipLocationGroups");
        x_session_register("ipLocationGroups", "___" . time());
        return "";
    }


}
