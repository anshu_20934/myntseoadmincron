<?php
namespace pageconfig\manager;
use pageconfig\exception\ImageUploadException;

use imageUtils\Jpegmini;
use pageconfig\dao\ImageReferenceDAO;

use pageconfig\exception\ImageValidationException;
use pageconfig\dao\ImageRulesDAO;
use pageconfig\manager\CachedBannerManager;
use enums\pageconfig\PageConfigLocationConstants;
use enums\pageconfig\PageConfigSectionConstants;
use base\manager\BaseManager;

require_once \HostConfig::$documentRoot . "/include/class/cache/XCache.php";
include_once \HostConfig::$documentRoot ."/utils/imagetransferS3.php";
include_once \HostConfig::$documentRoot ."/Profiler/Profiler.php";
class PageConfigManager extends BaseManager {
	private $imageRule, $pageLocation;
	private $imageRulesDAO, $imageReferenceDAO;
	private $xcache;
	private $variantFlag, $imageRuleId;
	public function __construct($locationType, $section, $pageLocation = "", $variantFlag = false, $parentImageId = "") {
		$this->variantFlag = $variantFlag;
		$this->parentImageId = $parentImageId;
		$this->imageRulesDAO = new ImageRulesDAO();
		$this->imageRule = $this->imageRulesDAO->findByTypes($locationType, $section);
		$this->pageLocation = empty($pageLocation) ? "" : $pageLocation;		
		$this->imageReferenceDAO = new ImageReferenceDAO($this->variantFlag,$this->parentImageId);
		
		global $xcache ;
		if($xcache==null){
			$xcache = new XCache();
		}
		$this->xcache = $xcache;
	}
	
	public function getRulesForLocationType($locationType) {
		$images = $this->imageRulesDAO->getRules($locationType);
		$imageMap = array();
        foreach($images as $image){
            $imageMap[$image['image_rule_id']]=$image;
        }
        return $imageMap;
	}
	
	public function getRulesByImageRuleId($imageRuleId){
		$rules = $this->imageRulesDAO->getRulesByRuleId($imageRuleId);
		return $rules;
	}

	/**
	 * Fetch images for this image manager
	 */
	public function getImages() {
		return  $this->imageReferenceDAO->findByRuleAndLocation($this->imageRule["image_rule_id"], $this->pageLocation);
	}
	
	/**
	 * Fetch images for this image manager and cache it in xcache
	 */
	public function getCachedImages() {
		return CachedBannerManager::getCachedBanners($this->imageRule["location_type"], $this->imageRule["section"], $this->pageLocation);
	}

	public function getCachedMultiClickData($slideShowBanners) {
		return $multiBannerData = CachedBannerManager::getMultiBannerData($slideShowBanners,$this->imageRule["location_type"], $this->imageRule["section"], $this->pageLocation);
	}
	/**
	 * View images with rule and pagelocation override
	 * @param unknown_type $imageRule
	 * @param unknown_type $pageLocation
	 */
	public function viewImages($imageRule, $pageLocation) {
		return $this->imageReferenceDAO->findByRuleAndLocation($imageRule["image_rule_id"], $pageLocation);
	}

	public function viewImagesForVariant(){
		return $this->imageReferenceDAO->getVariantImages();
	}
	
	public function getMultiBannerData($image_id){
		return $this->imageReferenceDAO->multiBannerData($image_id);
	}

	public function addImages(array &$images) {
		$this->uploadImages($images);
		foreach ($images as &$eachImage) {
			$this->imageReferenceDAO->addImage($eachImage);
		}
	}
	
	private function uploadImages(array &$images){
		foreach ($images as &$eachImage) {
			if($eachImage["source"]==="file" && $eachImage["image_is_uploaded"]) {
				if(!($this->uploadImageToCDN($eachImage)&&
				Jpegmini::copyCompressedPlaceholderFile($eachImage["image_url"])&&
				\myntra\utils\cdn\imagetransferS3::compress($eachImage["image_url"]))){
					throw new ImageUploadException($eachImage["image_url"]);
				}else{
					if(strpos($eachImage["image_url"], \HostConfig::$cdnBase) !== false){
						$image = substr($eachImage["image_url"], strlen(\HostConfig::$cdnBase));
					}else{
						$image = $eachImage["image_url"];
					}
					$image = \HostConfig::$documentRoot.$image;
					\Profiler::increment("imagesize-banner-pageconfig", filesize($image));
				}
			}
		}
	}
	
	public function updateImages(array $images) {
		$this->uploadImages($images);
		foreach($images as $img) {
			$this->imageReferenceDAO->updateImage($img);
		}
	}

	public function updateMultiBannerData(array $images){
		foreach ($images as $img) {
			$this->imageReferenceDAO->updateMultiClickData($img);
		}
	}
		
	public function deleteImages($images, $imageIds) {
		$this->imageReferenceDAO->deleteImages($imageIds, 1); 
	}

	public function getImagesByIds($imageIds) {
		return $this->imageReferenceDAO->findByImageIds($imageIds);
	}
	
	public function moveUp($image) {
		$retval = $this->imageReferenceDAO->getMaxSortOrderLessThanGiven($image["image_rule_id"], $image["page_location"], $image["sort_order"]);
		if(!empty($retval) && !empty($retval["sort_order"]) && !empty($retval["image_id"])) {
			//swap sort-orders of given image and that of the image just fetched
			return $this->imageReferenceDAO->swapSortOrders($retval["image_id"], $retval["sort_order"], $image["image_id"], $image["sort_order"]);
		} else {
			return false;
		}
	}

	public function moveDown($image) {
		$retval = $this->imageReferenceDAO->getMinSortOrderGreaterThanGiven($image["image_rule_id"], $image["page_location"], $image["sort_order"]);
		if(!empty($retval) && !empty($retval["sort_order"]) && !empty($retval["image_id"])) {
			//swap sort-orders of given image and that of the image just fetched
			return $this->imageReferenceDAO->swapSortOrders($retval["image_id"], $retval["sort_order"], $image["image_id"], $image["sort_order"]);
		} else {
			return false;
		}
	}
	
	public function getRule() {
		return $this->imageRule;
	}
	
	public function uploadImageToCDN(&$image) {
		$fileUploadedToCDN = moveToDefaultS3Bucket($image["image_url"]);
		if($fileUploadedToCDN){
			$image["image_url"] = \HostConfig::$cdnBase ."/{$image['image_url']}";
		} 
		return $fileUploadedToCDN;//else
		//TODO: send a mail to noc@myntra.com with the image URL which needs to be uploaded to CDN and the image path needs to be updated in DB
		//TODO: improve error handling if the cdn upload fails
	}
	
	public function populateImageParams(array &$images) {
		foreach ($images as &$img) {
			$isFile = $img["source"] === "file";
			if($img['image_is_uploaded'] !== false){

				$file = $isFile ? $img["image_file_tmp"] : $img["image_url"];
				try {
					$im = new \Imagick($file);
					$img["width"] = $im->getImageWidth();
					$img["height"] = $im->getImageHeight();
					$img["image_file_size"] = $im->getImageSize();
					unset($im);
				} catch (\ImagickException $e) {
					//Now check if the URL/File exists
					$handle = @fopen($file, 'r');
					if($handle === false) {
						throw new ImageValidationException("File/URL: $file: does not exist or not readable: ", "i/o", $img);
					} else {
						throw new ImageValidationException("Unable to load the File/URL as image: ", "file-not-image", $img);
					}
					@fclose($handle);
				}
				if($isFile) {
					$relativeFileLocation = 'images/banners/' . time() . '-' . $img["image_file_name"];
					$absoluteFileLocation = \HostConfig::$documentRoot .'/'.$relativeFileLocation;
					if(!copy($img["image_file_tmp"], $absoluteFileLocation)) {
						throw new ImageValidationException("Failed to copy to images directory", "i/o", $img);
					}
					$img["image_url"] = $relativeFileLocation;
				}
			}
			$img["image_rule_id"] = $this->imageRule["image_rule_id"];
			$img["page_location"] = $this->pageLocation;
		}
	}
	
	public function getCurrentImageCount() {
		return $this->imageReferenceDAO->getCurrentImageCount($this->imageRule['image_rule_id'], $this->pageLocation);
	}
	
	public function getSortOrderForNewImage() {
		$maxSortOrder = $this->imageReferenceDAO->getMaxSortOrder($this->imageRule['image_rule_id'], $this->pageLocation);
		return $maxSortOrder + 1;
	}
	
	/**
	 * Fetch all the page-locations for a given location type
	 * e.g. get all the search page locations, if locationType = SearchPage.
	 * or get all landing page locations, if locationType = LandingPage
	 */
	public function getPageLocations($includeDeleted = 0) {
		return $this->imageReferenceDAO->fetchPageLocations($this->imageRule["location_type"], $includeDeleted);
	}

	public function getVariantImages($images){		
		$imageIds = array();
		foreach ($images as $parentItem) {
			foreach ($parentItem as $imageItem) {
				array_push($imageIds, $imageItem[image_id]);
			}
		}
		$variantData = $this->imageReferenceDAO->getVariantImagesByParentIds($imageIds);
		return $variantData;
	}
}
