<?php
namespace pageconfig\action;
use pageconfig\exception\ImageValidationException;

use enums\base\ActionType;
use pageconfig\validator\PageConfigValidator;
use pageconfig\manager\PageConfigManager;

class PageConfigAddAction extends BasePageConfigAction {
	
	protected $images, $variantFlag;
	
	public function __construct($locationType, $section, $pageLocation) {
		$this->images = $this->getRequestVar("image");
		$this->variantFlag = false;
		if($this->images[0]['variant_flag'] == 'true'){
			$this->variantFlag = true;
			parent::__construct($locationType, $section, $pageLocation, $this->variantFlag);
		}
		else{
			parent::__construct($locationType, $section, $pageLocation);
		}
		$this->init();
		$this->setImages();
	}
	
	public function getImages() {
		return $this->images;
	}
	
	private function setImages() {
		$imageFiles = $this->getUploadedFileInRequest("image");
		$fileIndex = 0;
		for($i = 0; $i < count($this->images); $i++) {
			if(empty($this->images[$i]["image_url"])) {
				$this->images[$i]["source"] = "file";
				$this->images[$i]["image_file_name"] = space_to_hyphen($imageFiles["name"][$fileIndex]["image_file"]);
				$this->images[$i]["image_file_type"] = $imageFiles["type"][$fileIndex]["image_file"];
				$this->images[$i]["image_file_tmp"] = $imageFiles["tmp_name"][$fileIndex]["image_file"];
				$this->images[$i]["image_file_error"] = $imageFiles["error"][$fileIndex]["image_file"];
				$this->images[$i]["image_file_size"] = $imageFiles["size"][$fileIndex]["image_file"];
				if($this->images[$i]["image_file_error"] === UPLOAD_ERR_NO_FILE ){
					$this->images[$i]["image_is_uploaded"] = false;
				}else{
					$this->images[$i]["image_is_uploaded"] = true;
				}
				$fileIndex++;
			} else {
				$this->images[$i]["source"] = "url";
				$this->images[$i]["image_is_uploaded"] = true;
			}
		}
		$currentImageCountInDB = $this->manager->getCurrentImageCount();
		try {
			$this->manager->populateImageParams($this->images);
		} catch (\Exception $e) {
			throw new ImageValidationException($e->getMessage(), "i/o");
		}		
		$this->validator = new PageConfigValidator($this->images, $this->manager->getRule(), $currentImageCountInDB, ActionType::Add, $this->variantFlag);
		$this->validator->validate($this->locationType);
	}
	
	public function run() {
		$this->manager->addImages($this->images);
	}
}
