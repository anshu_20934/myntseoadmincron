<?php
namespace pageconfig\action;
use pageconfig\exception\ImageValidationException;

use enums\base\ActionType;
use pageconfig\manager\PageConfigManager;
use pageconfig\validator\PageConfigValidator;
/**
 * Action to be invoked when uploading an image
 * //TODO: this should ideally extend: BaseAuthenticatedAuthorizedAction: need to write such a class 
 * @author kundan
 */
class PageConfigMoveAction extends BasePageConfigAction {
	
	protected $imageId, $image, $direction;
	
	public function __construct($locationType, $section, $pageLocation, $mode = ActionType::MoveUp) {
		 if($this->getRequestVar("variantFlag")){
                $this->variantFlag = true;
                parent::__construct($locationType, $section, $pageLocation, $this->variantFlag);
        }
        else{
                parent::__construct($locationType, $section, $pageLocation);
        }

		$this->init();
		$this->direction = $mode;
		
		$this->setImageId();
		$this->setImage();
	}
	
	public function setImageId() {
		$this->imageId = trim($this->getRequestVar("imageId"));
	}
	
	public function setImage() {
		$img = $this->manager->getImagesByIds($this->imageId);
		if(!empty($img) && is_array($img)) {
			$this->image = $img[0];
		} else {
			throw new ImageValidationException("Could not fetch image corresponding to the selected image", "fetch-failed");
		}
	}
	
	public function run() {
		if($this->direction === ActionType::MoveUp) {
			$this->manager->moveUp($this->image);
		} elseif ($this->direction === ActionType::MoveDown) {
			$this->manager->moveDown($this->image);
		}		
	}
}