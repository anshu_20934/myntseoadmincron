<?php
namespace pageconfig\action;
use base\action\BaseAuthenticatedAuthorizedAction;
use pageconfig\manager\PageConfigManager;
class BasePageConfigAction extends BaseAuthenticatedAuthorizedAction {

	protected $locationType, $section, $pageLocation, $validator, $manager, $variantFlag, $parentImageId, $imageRuleId;
	
	public function __construct($locationType, $section, $pageLocation = "", $variantFlag = false, $parentImageId= "", $imageRuleId= "") {
		parent::__construct();
		$this->locationType = $locationType;
		$this->section = $section;
		$this->pageLocation = $pageLocation;
		$this->variantFlag = $variantFlag;
		$this->parentImageId = $parentImageId;
		$this->imageRuleId = $imageRuleId;		
	}
	
	protected function init() {
		$this->manager = new PageConfigManager($this->locationType, $this->section, $this->pageLocation, $this->variantFlag, $this->parentImageId);		
	}
	
	public function getLocationType() {
	    return $this->locationType;
	}
	
	public function getSection() {
	    return $this->section;
	}
	
	public function getPageLocation() {
	    return $this->pageLocation;
	}

	public function getVariantFlag() {
	    return $this->variantFlag;
	}

	public function parentImageId() {
	    return $this->parentImageId;
	}

	public function imageRuleId() {
	    return $this->imageRuleId;
	}	
}