<?php
namespace pageconfig\action;
use enums\base\ActionType;
use pageconfig\manager\PageConfigManager;
use pageconfig\validator\PageConfigValidator;
use pageconfig\exception\ImageValidationException;
/**
 * Action to be invoked when uploading an image
 * //TODO: this should ideally extend: BaseAuthenticatedAuthorizedAction: need to write such a class 
 * @author kundan
 */
class PageConfigUpdateAction extends BasePageConfigAction {
	
	protected $images;
	
	public function __construct($locationType, $section, $pageLocation, $parentImageId) {
		parent::__construct($locationType, $section, $pageLocation, $parentImageId);
		$this->init();
		$this->setImages();
	}
	
	public function getImages() {
		return $this->images;
	}
	
	public function setImages() {
		$this->images = $this->getRequestVar("image");
		$imageFiles = $this->getUploadedFileInRequest("image");
		foreach ($this->images as $imageId => &$eachImage) {
			if(!empty($eachImage["image_url"]) && empty($imageFiles["name"][$imageId]["image_file"])) {
				$eachImage["source"] = "url";
				$this->images[$imageId]["image_is_uploaded"] = true;
			}
			else {
				$eachImage["source"] = "file";
				$eachImage["image_file_name"] = space_to_hyphen($imageFiles["name"][$imageId]["image_file"]);
				$eachImage["image_file_type"] = $imageFiles["type"][$imageId]["image_file"];
				$eachImage["image_file_tmp"] = $imageFiles["tmp_name"][$imageId]["image_file"];
				$eachImage["image_file_error"] = $imageFiles["error"][$imageId]["image_file"];
				$eachImage["image_file_size"] = $imageFiles["size"][$imageId]["image_file"];
				if($eachImage["image_file_error"] === UPLOAD_ERR_NO_FILE || $eachImage["image_file_error"] ===null){
					$this->images[$imageId]["image_is_uploaded"] = false;
				}else{
					$this->images[$imageId]["image_is_uploaded"] = true;
				}
			}	
		}
		$currentImageCountInDB = $this->manager->getCurrentImageCount();
		try {
			$this->manager->populateImageParams($this->images);
		} catch (\Exception $e) {
			throw new ImageValidationException("It is from here".$e->getMessage(), "i/o");
		}
		$this->validator = new PageConfigValidator($this->images, $this->manager->getRule(), $currentImageCountInDB, ActionType::Update);
		$this->validator->validate($this->locationType);
	}
	
	public function run() {
		$this->manager->updateImages($this->images);
		$this->manager->updateMultiBannerData($this->images);
	}
	
}
