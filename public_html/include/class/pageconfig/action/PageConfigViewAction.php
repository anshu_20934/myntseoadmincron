<?php
namespace pageconfig\action;
use enums\pageconfig\PageConfigLocationConstants;
use enums\base\ActionType;
use pageconfig\action\BasePageConfigAction;
use pageconfig\manager\PageConfigManager;

class PageConfigViewAction extends BasePageConfigAction {
	//constants related to page: storyboard
	public function __construct($locationType, $section, $pageLocation, $variantFlag, $parentImageId, $imageRuleId) {
		parent::__construct($locationType, $section, $pageLocation, $variantFlag, $parentImageId, $imageRuleId);
		$this->init();
	}
	
	public function run() {
		global $smarty;
		$this->assignSmartyConsts();
		$imagesSets;
		if($this->variantFlag){
			$rules = $this->manager->getRulesByImageRuleId($this->imageRuleId);			
		}
		else{			
			$rules = $this->manager->getRulesForLocationType($this->locationType);
		}
		$multiBannerData = array();
		if(!$rules[0]["require_page_location"] || ($rules[0]["require_page_location"] && !empty($this->pageLocation))) {
			$imagesSets = array();
			foreach ($rules as $k=>$rule) {
				if($this->variantFlag){
					$imagesRef = $this->manager->viewImagesForVariant();
				}
				else{
					$imagesRef = $this->manager->viewImages($rule, $this->pageLocation);
				}
				foreach($imagesRef as $unit) {
					$multiBannerData["$unit[image_id]"] = $this->manager->getMultiBannerData($unit[image_id]);
					for($i=0; $i<count($multiBannerData["$unit[image_id]"]);$i++){							
						$multiBannerData["$unit[image_id]"][$i]['coordinates'] = explode(',', $multiBannerData["$unit[image_id]"][$i]['coordinates']);							
					}					
				}				
				unset($unit);
				$parentImagesSets = array();
				if($rule['parent_rule_id'] > 0){
					$parentImagesSets[] = $this->manager->viewImages($rules[$rule['parent_rule_id']], $this->pageLocation);
					if(!empty($parentImagesSets)){
                    	$pImagesSet = array();
                    	foreach($parentImagesSets[0] as $parentImage){
                        	$pImagesSet[$parentImage['image_id']]=$parentImage['image_url'];
                    	}
                    	foreach($imagesRef as &$imageRef){                    		
							$imageRef['parent_image_url'] = $pImagesSet[$imageRef['parent_image_id']];
							unset($pImagesSet[$imageRef['parent_image_id']]);
						}
						$rule['parent_images'] = $pImagesSet;
                	}
				}
				$rules[$k] = $rule;
				$imagesSets[] = $imagesRef;
			}			
		} else {
			//if page location is required but not set, then don't get any sections or banners to display
			unset($rules);
		}
		$variantImages = $this->manager->getVariantImages($imagesSets);
 		$smarty->assign("imagesSets",$imagesSets);
		$smarty->assign("rules",$rules);
		$smarty->assign("main","pageconfig");
		$smarty->assign("multiBannerData",$multiBannerData);
		$smarty->assign("variantImages",$variantImages);
		func_display("admin/home.tpl",$smarty);
	}
	
	private function assignSmartyConsts() {
		global $smarty;
		global $pageLocationId;

		$smarty->assign("addAction", ActionType::Add);
		$smarty->assign("updateAction", ActionType::Update);
		$smarty->assign("deleteAction", ActionType::Delete);
		$smarty->assign("moveUpAction", ActionType::MoveUp);
		$smarty->assign("moveDownAction", ActionType::MoveDown);
		$smarty->assign("pageLocationId", $pageLocationId);
		$smarty->assign("pageLocation", $this->pageLocation);
		$smarty->assign("locationType", $this->locationType);
		$smarty->assign("postBackURL", $this->getRequestedURL());

		if($this->variantFlag){
			$smarty->assign("sVariantFlag", $this->variantFlag);
			$smarty->assign("sParentImageId", $this->parentImageId);
			$smarty->assign("sImageRuleId", $this->imageRuleId);
		}
		
		$showLocation = false;
		$rule = $this->manager->getRule();
	
		if($rule["require_page_location"]) {
			$locations = $this->manager->getPageLocations();
			$smarty->assign("locations",$locations);
			$locationsJson = json_encode($locations);
			$smarty->assign("locationsJson",$locationsJson);
			$showLocation = true;
		}
		$smarty->assign("showLocation", $showLocation);
	}
}
