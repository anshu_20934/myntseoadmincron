<?php
namespace pageconfig\action;
use enums\base\ActionType;
use pageconfig\manager\PageConfigManager;
use pageconfig\validator\PageConfigValidator;
/**
 * Action to be invoked when uploading an image
 * //TODO: this should ideally extend: BaseAuthenticatedAuthorizedAction: need to write such a class 
 * @author kundan
 */
class PageConfigDeleteAction extends BasePageConfigAction {
	
	protected $imageIds, $images;
	
	public function __construct($locationType, $section, $pageLocation, $variantFlag, $parentImageId, $imageRuleId) {
		parent::__construct($locationType, $section, $pageLocation, $variantFlag, $parentImageId, $imageRuleId);
		$this->init();
		$imgIdStr = trim($this->getRequestVar("imageId"));
		$this->setImageIds();
		$this->setImages();
	}
	
	public function setImageIds() {
		$imgIdStr = trim($this->getRequestVar("imageId"));
		$this->imageIds = explode(",", $imgIdStr);
	}
	
	public function setImages() {
		$this->images = $this->manager->getImagesByIds($this->imageIds);
		$currentImageCountInDB = $this->manager->getCurrentImageCount();
		if(!$this->variantFlag){
			$this->validator = new PageConfigValidator($this->images, $this->manager->getRule(), $currentImageCountInDB, ActionType::Delete);
			$this->validator->validate();
		}		
	}
	
	public function run() {
		$this->manager->deleteImages($this->images, $this->imageIds);
	}
}