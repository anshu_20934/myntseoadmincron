<?php
namespace pageconfig\exception;

use base\exception\RedirectableValidationException;
class ImageUploadException extends \MyntraException {
	private $image;
	public function getDisplayMessage() {
		return $this->displayMessage;
	}
	public function __construct($image) {
		$this->displayMessage = "Image : $image cannot be uploaded to S3";
		parent::__construct($this->displayMessage);
	}
}
