<?php
namespace pageconfig\exception;
use base\exception\RedirectableValidationException;
class ImageValidationException extends RedirectableValidationException {
	private $image, $type, $rule, $displayMessage;
	public function getDisplayMessage() {
		return $this->displayMessage;
	}
	public function __construct($message, $type, $image, $rule) {
		$this->image = $image;
		$this->type = $type;
		$this->rule = $rule;
		
		if($image && $image["source"] === "file") {
			$file = $image["image_file_name"];
		} else if($image && $image["source"] === "url") {
			$file = $image["image_url"];
		}
		
		switch ($type) {
			case "size":
				$this->displayMessage = "Rule for {$rule["location_type"]} - {$rule["section"]} requires image: $file to be of 
				{$rule['width']}x{$rule['height']} px while image is of {$image['width']}x{$image['height']} px";
				break;
			case "count-step":
				$this->displayMessage = "Rule for {$rule["location_type"]} - {$rule["section"]} requires exactly {$rule['step']} images to be uploaded or deleted at a time";
				break;
			case "count-min":
				$this->displayMessage = "Rule for {$rule["location_type"]} - {$rule["section"]} requires minimum {$rule['min']} images to be left after deletion, which is violated";
				break;
			case "count-max":
				$this->displayMessage = "Rule for {$rule["location_type"]} - {$rule["section"]} requires at maximum {$rule['max']} images to be left after addition, which is violated";
				break;
			case "file-error":
				$this->displayMessage = "Uploaded file or URL: $file has errors. {$image['image_file_error']}";
				break;
			case "file-not-image":
				$this->displayMessage = "Uploaded file or URL: $file is probably not an image";
				break;
			case "file-size-limit":
				$ruleSize = number_format($rule["size"]/1024, 0);
				$imageSize = number_format($image['image_file_size']/1024, 0);
				$this->displayMessage = "Uploaded file or URL: for {$rule["location_type"]} - {$rule["section"]} : $file size limit is {$ruleSize} KB while file-size is {$imageSize} KB";
				break;
			case "i/o": 
				$this->displayMessage = "I/O exception while loading image from uploaded file/url: $file: $message";
			case "duplicate":
				$this->displayMessage = "Duplicate entry: $message";
			default:
				;
			break;
		}
		
		
		$message .= $this->displayMessage;
		parent::__construct($message, "image", $type);
	}
}
