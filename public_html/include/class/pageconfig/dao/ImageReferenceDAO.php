<?php
namespace pageconfig\dao;
use pageconfig\dataobject\ImageReferenceDO;
use base\dao\BaseDAO;
use enums\base\ActionType;
use pageconfig\exception\ImageValidationException;

require_once \HostConfig::$documentRoot . "/include/func/func.db.php";
class ImageReferenceDAO extends BaseDAO {
	
	private $tbl, $ruleTbl, $variantFlag, $parentImageId, $multiclickTable;
	
	private $reqdDBCols = array("image_rule_id", "page_location", 
							"image_url", "alt_text", "target_url", "title", "description", "fashion_banners",
							"width", "height", "sort_order","parent_image_id"); 
	private $primaryKey = "image_id";
	
	public function __construct($variantFlag = false, $parentImageId = "") {
		global $sql_tbl;
		$this->variantFlag = $variantFlag;
		$this->parentImageId = $parentImageId;
		if($this->variantFlag){
			$this->tbl = $sql_tbl["image_ref_variant"];
			$this->multiclickTable = $sql_tbl["multi_click_table_variant"];
			array_push($this->reqdDBCols, "group_id");
			array_push($this->reqdDBCols, "location_group_id");
			array_push($this->reqdDBCols, "variant_location");
		}
		else{
			$this->tbl = $sql_tbl["image_ref"];
			$this->multiclickTable = $sql_tbl["multi_click_table"];			
		}
		$this->ruleTbl = $sql_tbl["image_rule"];
	}
	
	public function updateImage(array &$image) {
		$img2update = $this->stripUnrequiredKeys($image, ActionType::Update);
		return func_array2updateWithTypeCheck($this->tbl, $img2update, array($this->primaryKey => $image[$this->primaryKey]));
	}

	public function updateMultiClickData(array &$image){
		$sql = "delete from $this->multiclickTable where image_id in($image[image_id])";
		db_query($sql);
		$dbQuery = array();		
		if($image['multiFlag']==1){
			$sql = "update $this->tbl set multiclick = 1 where image_id = $image[image_id]";
		}
		else{
			$sql = "update $this->tbl set multiclick = 0 where image_id = $image[image_id]";	
		}
		db_query($sql);
		foreach ($image['multi'] as $imgKey => $img) {
			$id = $image['image_id'];
			$url = $img['target']; //echo $url;
			$title = $img['title'];
			$coordinates = $img[0].','.$img[1].','.$img[2].','.$img[3];
			$tempQuery = "($id,'$url','$coordinates','$title')";
			array_push($dbQuery, $tempQuery);
		}
		$dbQuery = implode(',', $dbQuery);
		$sql = "insert into $this->multiclickTable values $dbQuery";
		if($dbQuery){
			return db_query($sql);
		}
	}
	
	public function addImage(array &$image) {
		$img2ins = $this->stripUnrequiredKeys($image);
		if(!$this->variantFlag){			
			$this->validateDuplicateEntry($img2ins['parent_image_id'],$img2ins['image_rule_id']);
		}
		$img2ins["sort_order"] = $this->getMaxSortOrder($img2ins["image_rule_id"]) + 1;
		$imageId = func_array2insertWithTypeCheck($this->tbl, $img2ins);
		if(!$imageId === false) {
			$image["sort_order"] = $img2ins["sort_order"];
			$image["image_id"] = $imageId;
		}
		return true;		
	}
	/**
	 * Deletes an array of images at a time
	 * @param array $images
	 */
	public function deleteImages(array $imageIds, $softDelete = false) {
		$imageIdsCSV = implode(",", $imageIds);
		if(!$softDelete) {
			$sql = "DELETE FROM {$this->tbl} WHERE {$this->primaryKey} IN ({$imageIdsCSV})";
		} else {
			$sql = "UPDATE {$this->tbl} SET deleted=true WHERE {$this->primaryKey} IN ({$imageIdsCSV})";
		}
		return db_query($sql);
	}	
	
	/**
	 * Given an assoc array to insert,remove the keys which are not required: which are not columns in the table 
	 * @param array $image
	 * @return array
	 */
	private function stripUnrequiredKeys($image, $mode = ActionType::Add) {
		$image2ins = $image;
		foreach ($image2ins as $key => $value) {
			if(array_search($key, $this->reqdDBCols, true) === false) {
				unset($image2ins[$key]);
			}
		}
		if($mode === ActionType::Update) {
			unset($image2ins[$this->primaryKey]);
		}
		return $image2ins;
	}
	
	public function updateImageURL(array $image) {
		$toUpdate = array("image_url"=>$image["image_url"]);
		$whereClause = array("image_id"=>$image["image_id"]);
		return func_array2updateWithTypeCheck($this->tbl, $toUpdate, $whereClause);
	}
	
	public function updateSortOrder($imageId, $sortOrder) {
		$toUpdate = array("sort_order"=>$sortOrder);
		$whereClause = array("image_id"=>$imageId);
		return func_array2updateWithTypeCheck($this->tbl, $toUpdate, $whereClause);
	}
	
	/**
	 * Get the number of existing images of given rule-id (implying given locationType and section) and pageLocation 
	 * @param unknown_type $imageRuleId
	 * @param unknown_type $pageLocation
	 * @return number
	 */
	
	public function getCurrentImageCount($imageRuleId, $pageLocation = "", $deleted = 0) {
		
		$filters = array("image_rule_id" => $imageRuleId, "page_location" => $pageLocation, "deleted" => array("value"=>$deleted, "type"=>"integer"));
		$res = func_select_columns_query($this->tbl, "count(*) as count_images",$filters);
		if(!empty($res[0]) && !empty($res[0]["count_images"])) {
			$count = $res[0]["count_images"];
		}
		return (empty($count) || ($count < 0)) ? 0 : $count;
	}
	
	/**
	 * Get the highest sort-order available for a given image type (rule) and page-location
	 * @param unknown_type $imageRuleId
	 * @param unknown_type $pageLocation
	 */
	public function getMaxSortOrder($imageRuleId, $pageLocation = "") {
		$filters = array("image_rule_id" => $imageRuleId);
		if(!empty($pageLocation)) {
			$filters["page_location"] = $pageLocation;
		}
		$res = func_select_columns_query($this->tbl, "max(sort_order) as max_sort_order",$filters);
		if(!empty($res[0]) && !empty($res[0]["max_sort_order"])) {
			$maxSortOrder = $res[0]["max_sort_order"];
		}
		return (empty($maxSortOrder) || ($maxSortOrder < 0)) ? 0 : $maxSortOrder;
	}
	
	/**
	 * Finds the highest sort order less than a given sort order for a particular image class (identified by image-rule-id and page-location)
	 * If any sort order less than givenSortOrder is not found, then it returns the given sort-order only 
	 * @param unknown_type $imageRuleId
	 * @param unknown_type $pageLocation
	 * @param unknown_type $givenSortOrder
	 */
	public function getMaxSortOrderLessThanGiven($imageRuleId, $pageLocation = "", $givenSortOrder, $deleted = 0) {
		$projection = "sort_order, image_id";
		$whereClause = "image_rule_id = $imageRuleId and deleted = $deleted and page_location = '{$pageLocation}' and sort_order < $givenSortOrder";
		$sql = "SELECT $projection FROM {$this->tbl} WHERE $whereClause ORDER BY sort_order DESC LIMIT 1";
		$val = func_query_first($sql);
		return $val;
	}
	
	/**
	* Finds the highest sort order less than a given sort order for a particular image class (identified by image-rule-id and page-location)
	* If any sort order less than givenSortOrder is not found, then it returns the given sort-order only
	* @param unknown_type $imageRuleId
	* @param unknown_type $pageLocation
	* @param unknown_type $givenSortOrder
	*/
	public function getMinSortOrderGreaterThanGiven($imageRuleId, $pageLocation = "", $givenSortOrder, $deleted = 0) {
		$projection = "sort_order, image_id";
		$whereClause = "image_rule_id = $imageRuleId and deleted = $deleted and page_location = '{$pageLocation}' and sort_order > $givenSortOrder";
		$sql = "SELECT $projection FROM {$this->tbl} WHERE $whereClause ORDER BY sort_order ASC LIMIT 1";
		$val = func_query_first($sql);
		return $val;
	}
	
	/**
	 * Given two images and their sort-orders, interchange their sort-orders
	 * This is typically required in move-up or move-down operation
	 * @param unknown_type $imageId1
	 * @param unknown_type $sortOrder1
	 * @param unknown_type $imageId2
	 * @param unknown_type $sortOrder2
	 */
	public function swapSortOrders($imageId1,$sortOrder1,$imageId2,$sortOrder2) {
		$tempSortOrder = 0;
		db_query("SET AUTOCOMMIT=0");
		db_query("START TRANSACTION");
		$wasSuccessful1 = $this->updateSortOrder($imageId1, $tempSortOrder);
		$wasSuccessful2 = $this->updateSortOrder($imageId2, $sortOrder1);
		$wasSuccessful3 = $this->updateSortOrder($imageId1, $sortOrder2);
		$wasSuccessful = $wasSuccessful1 && $wasSuccessful2 && $wasSuccessful3;
		if($wasSuccessful) {
			db_query("COMMIT");
			db_query("SET AUTOCOMMIT=1");
			return true;
		} else {
			db_query("ROLLBACK");
			db_query("SET AUTOCOMMIT=1");
			return false;
		}
	}
	/**
	 * Fetch Images based on their imageIds
	 * @param unknown_type $imageIds
	 */
	public function findByImageIds($imageIds) {
		if(empty($imageIds)) {
			return;
		}
		if(is_array($imageIds)) {
			$imageIds = implode(",", $imageIds);
		}
		$sql = "select * from {$this->tbl} where {$this->primaryKey} in ({$imageIds})";
		return execute_sql($sql, "assoc");
	} 
	
	/**
	 * Fetch an image based on its id
	 * @param unknown_type $imageId
	 */
	public function findByImageId($imageId) {
		return func_select_first($this->tbl, array("image_id"=>$imageId));
	}
	

	public function validateDuplicateEntry($parentImageId, $ruleId) {
		/* for a particular parent there can be only one child as of now*/
		if($parentImageId){
			$sql = "select * from {$this->tbl} where image_rule_id = $ruleId and parent_image_id = $parentImageId";
			$resultSet = func_query($sql);
			if(!empty($resultSet))
				throw new ImageValidationException("Child image exists for parent: $parentImageId and rule: $ruleId","duplicate");
		}
	}

	/**
	 * List Images based on Rule and Location
	 * @param unknown_type $imageRuleId
	 * @param unknown_type $pageLocation
	 * @param unknown_type $deleted
	 */
	public function findByRuleAndLocation($imageRuleId, $pageLocation, $deleted = 0) {
		$sql = "select * from {$this->tbl} where image_rule_id = $imageRuleId and page_location = '{$pageLocation}' and deleted = {$deleted} order by sort_order asc";
		return func_query($sql);
	}

	public function getVariantImages($deleted = 0){
		$sql = "select * from {$this->tbl} where parent_image_id = {$this->parentImageId} and deleted = {$deleted} order by sort_order asc";
		return func_query($sql);
	}
	
	/**
	 * Used to fetch multiclick coordinates and urls based on image id
	 */	
	public function multiBannerData($image_id,$groupId=''){
		if(strlen($image_id) > 0){
			if($groupId == ''){
				if(!$this->variantFlag){
					$sql = "select * from mk_multibanner_data where image_id in ($image_id)";
				}
				else{
					$sql = "select * from mk_multibanner_data_variant where image_id in ($image_id)";
				}	
			}
			else{
				$sql = "select * from mk_multibanner_data_variant where image_id in ($image_id)";
			}				
			
			return func_query($sql);
		}		
	}

	/**
	 * Used to fetch Images by LocationType, Section and PageLocation
	 */
	public function findByLocationAndSection($locationType, $section, $pageLocation = "", $deleted = 0) {
		$sql = "select ref.*
				from page_config_image_reference ref
				inner join page_config_image_rule rule
				on ref.image_rule_id = rule.image_rule_id
				and rule.location_type = '{$locationType}' and rule.section = '{$section}' and ref.page_location = '$pageLocation' and ref.deleted=$deleted";
		return func_query($sql);		
	}
	
	/**
	* Used to fetch Images 
	*/
	public function getAllImageReferences($deleted = 0) {
		$sql = "select ref.*,rule.location_type,rule.section  
					from page_config_image_reference ref
					inner join page_config_image_rule rule
					on ref.image_rule_id = rule.image_rule_id and ref.deleted=$deleted order by ref.sort_order";
		return func_query($sql);
	}
	
	/**
	 * returns image refrence map with location_type-section-page_location as key
	 * @param unknown_type $deleted
	 */
	public function getAllImageReferencesMap($deleted = 0){
		$resultSet=$this->getAllImageReferences($deleted);
		$result=array();
		foreach($resultSet as $row){
			$result[strtolower($row['location_type']."-".$row['section']."-".$row['page_location'])][]=$row;
		}
		return $result;
	}
	
	/**
	 * Fetch all the page-locations for a given location type
	 * e.g. get all the search page locations, if locationType = SearchPage.
	 * or get all landing page locations, if locationType = LandingPage
	 * @param unknown_type $locationType
	 * @param unknown_type $deleted
	 */
	public function fetchPageLocations($locationType, $deleted = 0) {
		$sql = "SELECT distinct(page_location) as page_location FROM {$this->tbl} ref 
				INNER JOIN {$this->ruleTbl} rule
				ON ref.image_rule_id = rule.image_rule_id
				WHERE  rule.location_type = '{$locationType}' and deleted=$deleted order by page_location";
		return func_query_column($sql, "page_location");
	}

	public function getVariantImagesByParentIds($imageIds){
		$sql = 'select * from page_config_image_reference_variant where parent_image_id in('.implode($imageIds,',').') and deleted = 0';
		return func_query($sql);
	}
}
