<?php
namespace pageconfig\dao;
use base\dao\BaseDAO;
use base\exception\BaseRedirectableException;

class ImageRulesDAO extends BaseDAO {

	private $tblName;
	public function __construct() {
		global $sql_tbl;
		$this->tblName = $sql_tbl["image_rule"];;
	}
	
	public function findByRuleId($imageRuleId) {
		return func_select_first($this->tblName, array("image_rule_id" => $imageRuleId));
	}
	
	public function findByTypes($locationType, $section) {
		if(empty($locationType) || empty($section)) {
			throw new BaseRedirectableException(__FILE__.":".__FUNCTION__.":".__LINE__.": locationType or section is missing in call to function");
		}
		return func_select_first($this->tblName, array("location_type" => $locationType, "section" => $section));
	}

	public function getRules($locationType) {
		$criteria = array("location_type"=>$locationType);
		return func_select_query($this->tblName, $criteria);
	
	}

	public function getRulesByRuleId($imageRuleId){
		$sql = 'select * from '. $this->tblName. ' where image_rule_id='.$imageRuleId;
		return func_query($sql);
	}
}
