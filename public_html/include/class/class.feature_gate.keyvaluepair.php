<?php
	class FeatureGateKeyValuePairs {
		
		const FEATURE_GATE_KVPAIR_CACHE = 'feature_gate_key_value_pairs';
                private static $kvpairs = array();
		
		public static function refreshKeyValuePairsInCache() {
                        global $xcache;
                        if ($xcache == null) {
                            $xcache = new XCache();
                        }
                        $KVPairs = self::getAllFeatureGateKeyValuePairs();                        
                        $arr = array();
                        foreach($KVPairs as $kv) {
                            // dont put the desc etc in xcache
                            $arr[trim($kv['key'])] = trim($kv['value']);
                        }
                        self::$kvpairs = $arr;
                        
                        $xcache->storeObject(self::FEATURE_GATE_KVPAIR_CACHE, $arr, 0);
		}

                public static function getAllFeatureGateKeyValuePairs() {
                        $KVPairs = func_query("SELECT * FROM mk_feature_gate_key_value_pairs");
                        return $KVPairs;
                }
    	
		public static function getFeatureGateValueForKey($key, $defaulVal = NULL) {
                        $key = trim($key);
                        self::ensureInMem();
                        $val = self::$kvpairs[$key];
                        if($val == NULL) {
                            return $defaulVal;
                        }
                        return trim($val);
        	}
                
                
                public static function ensureInMem() {
                        if (empty(self::$kvpairs)) {
                            global $xcache;
                            if($xcache==null){
                                    $xcache = new XCache();
                            }
                            $arr = $xcache->fetchObject(self::FEATURE_GATE_KVPAIR_CACHE);
                            if (empty($arr)) {
                                self::refreshKeyValuePairsInCache();
                            } else {
                                self::$kvpairs = $arr;
                            }
                        }
                }


                /**
    	 * Get boolean value for a Feature-Gate key.
    	 * Parses the value for the key,converts them to boolean and returns
    	 * if the value is 'true' or '1' returns true. if value is 'false' or '0', returns false, otherwise returns $defaultValue 
    	 */
    	public static function getBoolean($key,$defaultValue=false) {
			if($defaultValue != true && $defaultValue != false) {
				throw new Exception("Exception: ".__FILE__.":".__LINE__.":defaultValue must be either true or false");
			}
			$value = strtolower(trim(self::getFeatureGateValueForKey($key)));
			if($value == "true" || $value == "1" || $value == 1){
				return true;
			}
			else if($value === "false" || $value === "0" || $value === 0){
				return false;
			}
			else {
				return $defaultValue;
			}
    	}
    	
    	/**
    	 * Get integer value for a Feature-Gate key.
    	 * Parses the value for the key,converts them to integer and returns
    	 */
    	public static function getInteger($key,$defaultValue=-1) {
			if(!is_numeric($defaultValue)) {
				throw new Exception("Exception: ".__FILE__.":".__LINE__.":defaultValue must be a number");
			}
			$value = trim(self::getFeatureGateValueForKey($key));
			if(is_numeric($value)){
				return (int)$value;
			}
			else {
				return $defaultValue;
			}
    	}
    	
    	/**
    	 * Get float value for a Feature-Gate key.
    	 * Parses the value for the key,converts them to float and returns
    	 */
    	public static function getFloat($key,$defaultValue=-1) {
			if(!is_numeric($defaultValue)) {
				throw new Exception("Exception: ".__FILE__.":".__LINE__.":defaultValue must be a number");
			}
			$value = strtolower(trim(self::getFeatureGateValueForKey($key)));
			if(is_numeric($value)){
				return (float)$value;
			}
			else {
				return $defaultValue;
			}
    	}
    	
    	/**
    	 * Get String array for comma-separated values
    	 * Parses the value for the key, delimits values based on comma and converts to array after trimming
    	 */
    	public static function getStrArray($key,$default=array()) {
			$value = strtolower(trim(self::getFeatureGateValueForKey($key)));
			if(!empty($value)) {
				return array_map("trim", explode(",",$value));	
			}
			else {
				return $default;
			}
    	}
    	
    	/**
    	 *  Example of a value validator function. This function checks if our values are numberic in nature.
    	 *  @param $key
    	 *  @param $value
    	 *  @param $errMsg - updated in case of failure with appropriate message.
    	 *  
    	 *  Returns 1 if validation is successfull. For failure cases, return 0
    	 *  and populate $errMsg with debugging information.
    	 *  
    	 *  Please use this example while generating more value validators in future.
    	 * 
    	 */
    	private function numbericValidator ($key, $value, &$errMsg){
    		$errMsg = "";  // clear errMsg
    		try{
    			$codeString = "\$retval=is_numeric(".$value.");";
    			eval($codeString);	
       		} catch (Exception $e) {    			
    			throw new Exception("Exception: ".__FILE__.":".__LINE__.":CodeString to cause exception is: ".$codeString);
    		}
    		
    		if(!$retval) { 
    			// update appropraite errMsg.
    			$errMsg = "value for the key '".$key."' => ".$value." must be a number";
    		}
    		return $retval;
    	}
    	
    	/** 
    	 * Reads associative k.v pairs from feature gate. 
    	 * Restriction on keys - only the keys passed in default array would be considered valid.
    	 * Returns an array containing all the k/v pairs specified in default array. 
    	 * 
    	 * Value for a key is determined as follows:
    	 *  if key is present in xcache, value = value specified in the cache
    	 *  if key is not present in xcache, value = default value for key (as per $default array)
    	 *  
    	 * Validation of all the values take place as per the $defaultValueValidator function. 
    	 * if validation fails, value for a key  = default value for the key (as per $default array)
    	 * throw an error if validation of default value fails as well.
    	 * 
    	 * All the values are transformed using $transformFunc and returned
    	 *  
    	 * @param $key 
    	 * @param $default
    	 * @param $transformFunc - Transformation to be applied on the value 'v'
    	 * @param $defaultValueValidator - Validator function for values (from admin interface and defaults)
    	 * @param $listDelim
    	 * @param $keyValDelim
    	 * 
    	 *  Ex: $default = { a => 2, b=>4 } , value specified on admin = a=>3,b=>4,c=>5
    	 *  This function would return 
    	 *  Array(
    	 *  	[a] => 3
    	 *  	[b] => 4
    	 *  );
    	 *  
    	 *  $default = { a => 2, b=>4 } , value specified on admin = a=>3, b=>sd,c=>5
    	 *  This function would return 
    	 *  Array(
    	 *  	[a] => 3
    	 *  	[b] => 4
    	 *  );
    	 * 
    	 */
    	public static function getAssociativeArray($key, $default, $transformFunc = "intval", 
    				$defaultValueValidator = "numbericValidator", $listDelim = ",", $keyValDelim = "="){
    		$value = trim(self::getFeatureGateValueForKey($key));
    		if(!empty($value)) {
				$arr = array_map("trim", explode($listDelim,$value));
				foreach ($arr as $key=>$value )	{
					$a = explode($keyValDelim,$value);
					//check the keys are present in $default array
					if(key_exists($a[0], $default)){
						$defaultValue = $default[$a[0]];
						// call validation on default value
						$codeValidationString = "\$retval=self::".$defaultValueValidator.
										 "(\"". $a[0]. "\",\"". $defaultValue. "\",\$errorMsg);";
						eval($codeValidationString);
						if(!$retval) {
							if(empty($errorMsg)) {
								$errorMsg = "CodeString to cause exception is: ".$codeValidationString;
							}
							throw new Exception("Exception: ".__FILE__.":".__LINE__.": default ".$errorMsg);
						}
								
						// call validation on value read from cache
						$codeValidationString = "\$retval=self::".$defaultValueValidator. 
									"(\"". $a[0]. "\",\"". $a[1]. "\",\$errorMsg);";
						eval($codeValidationString);
						if(!$retval){
							// validation fails, use default value
							$a[1]=$defaultValue;
						}
									
						// assign the transformed value to the key
						$codeString = "\$arr2[".$a[0]."]=".$transformFunc."(".$a[1].");";
						try{
							eval($codeString);
						} catch (Exception $e){
							throw new Exception("Exception: ".__FILE__.":".__LINE__.
									": CodeString to cause exception is: ".$codeString);
						}
					}
				}
				// For unspecified keys, take the default values from default array
				foreach ($default as $key=>$value){
					if(!key_exists($key, $arr2)){
						// call validation on default value
						$codeValidationString = "\$retval=self::".$defaultValueValidator. 
										"(\"". $key. "\",\"". $value. "\",\$errorMsg);";
						eval($codeValidationString);
						if(!$retval) {
							if(empty($errorMsg)) {
								$errorMsg = "CodeString to cause exception is: ".$codeValidationString;
							}
							throw new Exception("Exception: ".__FILE__.":".__LINE__.": default ".$errorMsg);
						}
						
						// assign the transformed value to the key
						$codeString = "\$arr2[".$key."]=".$transformFunc."(".$value.");";
						try{
							eval($codeString);
						} catch (Exception $e){
							throw new Exception("Exception: ".__FILE__.":".__LINE__.
										": CodeString to cause exception is: ".$codeString);
						}
					}
				}
				return $arr2;
    		}
    		else {
    			return $default;
    		}
    	}
	}
	
	/*
	class FeatureGateConstants {
		
		public static $freeIntlShippingEnabled = false;
		public static $minOrderForFreeIntlShipping = -1;
		public static $paymentOptionsOrder = Array("credit_card", "debit_card", "net_banking", "cod");
		
		public static function isFreeIntlShippingEnabled() {
			$freeIntlShippingEnabled = FeatureGateKeyValuePairs::getBoolean('FreeIntlShipping.Enabled');
			return $freeInternationalShippingEnabled;			
		}
		
		public static function getMinOrderForFreeIntlShipping() {
			$minOrderForFreeIntlShipping = $freeIntlShippingEnabled ? FeatureGateKeyValuePairs::getInteger('FreeIntlShipping.MinOrder', -1) : -1;
			return $minOrderForFreeIntlShipping;
		}
		
		public static function getPaymentOptionsOrder() {
			$paymentOptionsOrder = FeatureGateKeyValuePairs::getStrArray("PaymentOptionsOrder", $paymentOptionsOrder);
			return $paymentOptionsOrder;
		}
		
	}*/
?>
