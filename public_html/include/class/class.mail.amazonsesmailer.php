<?php
// Include the SDK
require_once "$xcart_dir/include/AmazonSDK/sdk.class.php";
require_once "$xcart_dir/include/AmazonSDK/services/ses.class.php";
require_once "$xcart_dir/include/class/class.mail.abstractmailserviceprovider.php";
require_once "$xcart_dir/Profiler/Profiler.php";

/**
 * Use Amazon Simple Email Service to Send Emails
 * @author kundan
 */
class AmazonSESMailer extends AbstractMailServiceProvider {

	private $amazonSES;
	
	public function __construct() {
		global $system_amazon_access_key, $system_amazon_secret_key;
		// Instantiate the Amazon class
		$this->amazonSES = new AmazonSES ( $system_amazon_access_key, $system_amazon_secret_key );
	}
	/**
	 * In order to send email from Amazon SES, the sender needs to be verified. This is true for both demo(trial) & production mode
	 * However, for trial mode, each recipient email also needs to be verified.
	 * Use this utility method to verify an email address 
	 * @param unknown_type $email
	 */
	public function verifyEmail($email) {
		try {
			return $this->amazonSES->verify_email_address($email);	
		} catch (Exception $e) {
			global $maillog;
			$maillog->error($e->getMessage().":".$e->getFile().":".$e->getLine()."\n".$e->getTraceAsString());
		}
		return false;
	}
	
	/**
	 * Get an associative array containing no. of mails allowed to be sent in a 24 hour moving window,
	 * no. of mails actually sent in 24 hour window, whether sending 1 more mail now is permitted or not  
	 */
	public function getDailySendQuota() {
		try {
			$rQuota = $this->amazonSES->get_send_quota ();
			$ret['quota'] = (int) $rQuota->body->GetSendQuotaResult->Max24HourSend;
			$ret['sent'] = (int) $rQuota->body->GetSendQuotaResult->SentLast24Hours;
			$ret['limitreached'] = ($ret['sent'] >= $ret['quota']) ? 1 : 0;
			return $ret;
		} catch (Exception $e) {
			global $maillog;
			$maillog->error($e->getMessage().":".$e->getFile().":".$e->getLine()."\n".$e->getTraceAsString());
		}
		return null;
	}
	
	/**
	 * Get statistics of mails sent, bounced, rejected etc
	 */
	public function getSentStatistics() {
		try {
			return $this->amazonSES->get_send_statistics();
		} catch (Exception $e) {
			global $maillog;
			$maillog->error($e->getMessage().":".$e->getFile().":".$e->getLine()."\n".$e->getTraceAsString());
		}
		return null;
	}
	/**
	 * Send mail
	 * @param mail details array with keys 'to', 'cc', 'bcc', 'subject', 'content', from_email, from_name
	 * </p>
	 * @return bool true if the mail was successfully accepted for delivery by ses, false otherwise.
	 * </p>
	 */
	public function sendMail($mail_detail) {
                $profiler_handle = Profiler::startTiming("ses-mailer");

		$sesMailDetails = new AmazonSESMailAdapter($mail_detail);
		
		//disabling the quota check for improving performance
		/*//what's our quota?
		$quotaVsSent = $this->getDailySendQuota();
		if(!$quotaVsSent['sending']) {
			//quota is reached, so return false, so that it would use fallback: myntra mail server
			return false;
		}*/
		
		//construct mail. send email
		global $maillog;
		try {
			$rSendEmail = $this->amazonSES->send_email ($sesMailDetails->source, $sesMailDetails->destination, $sesMailDetails->message );
			if ($rSendEmail->status == 200) {
				//capture the message id of the sent message
				$id = $rSendEmail->body->SendEmailResult->MessageId;
				$maillog->info(__FILE__.__FUNCTION__.":Mail sent via SES:".$id);
                                Profiler::endTiming($profiler_handle);
				return true;
			}
			else {
				$maillog->error(__FILE__.__FUNCTION__.":Mail could not be sent via SES:".$rSendEmail->body['error']);
			}
		}
		 catch (Exception $e) {
			global $maillog;
			$maillog->error($e->getMessage().":".$e->getFile().":".$e->getLine()."\n".$e->getTraceAsString());
		}
                Profiler::increment("ses-mailer-failure");
		return false; 
	}
}


/**
 * Adapter to convert between our regular mail_detail array and the one Amazon SES Expects
 * @author kundan
 */
class AmazonSESMailAdapter {
	
	public $source;
	public $destination;
	public $message;
	
	public function __construct($mail_detail, $html_mode = true) {
		$this->source = trim ( $mail_detail ['from_email'] );
		if (! empty ( $mail_detail ['from_name'] )) {
			$this->source = trim ( $mail_detail ['from_name'] ) . " <" . trim ( $mail_detail ['from_email'] ) . ">";
		}
		
		$addresses = array ();
		
		if (! empty ( $mail_detail ['to'] )) {
			$addresses['ToAddresses'] = explode(",",$mail_detail ['to']);
		}
		if (! empty ( $mail_detail ['cc'] )) {
			$addresses ['CcAddresses'] = explode(",",$mail_detail ['cc']);
		}
		/* if (! empty ( $mail_detail ['bcc'] )) {
			$addresses ['BccAddresses'] = explode(",",$mail_detail ['bcc']);
		} */
		$this->destination = $addresses;
		
		if($html_mode) {
			$this->message = CFComplexType::map ( array ('Subject.Data' => $mail_detail['subject'], 'Body.Html.Data' => $mail_detail['content'] ) );
		}
		else {
			$this->message = CFComplexType::map ( array ('Subject.Data' => $mail_detail['subject'], 'Body.Text.Data' => $mail_detail['content'] ) );
		}
	}
}
 
?>
