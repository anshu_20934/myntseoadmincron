<?php

namespace imageUtils;

class Jpegmini
{
    const BANNER_IMAGE = "hp_banner";
    const FILTER_LOGO_IMAGE = "filter_logo";
    const FILTER_BANNER_IMAGE = "filter_banner";
    const STYLE_IMAGE_ORIG = "style_orig";
    const STYLE_IMAGE_48_96 = "style_48_64";
    const STYLE_IMAGE_96_128 = "style_96_128";
    const STYLE_IMAGE_180_240 = "style_180_240";
    const STYLE_IMAGE_240_320 = "style_240_320";
    const STYLE_IMAGE_360_480 = "style_360_480";
    const STYLE_IMAGE_540_720 = "style_540_720";
    const STYLE_IMAGE_1080_1440 = "style_1080_1440";
    const STYLE_SIZECHART_IMAGE = "style_sizechart";

    private static $log;

    private static function getLog(){
        if(self::$log == null){
            global $weblog;
            self::$log = $weblog;
        }
        return self::$log;
    }

    public static function getCompressedImagePathFromOriginalPath($filepath){
        $position = strrpos($filepath , ".");
        if($position !== false){
            $compressedPath = substr($filepath, 0, $position)."_mini".substr($filepath, $position);
        }
        return $compressedPath;
    }
            
    public static function compress($path, $options=array()){
        self::getLog()->debug(__class__.":".__line__.": Compressing '$path' with options :".print_r($options, true));
        if(empty($path)){
            self::getLog()->debug(__class__.":".__line__.": Returning as the path is empty");
            return false;
        }
        //Release the jpegmini locks if existing
        exec("sudo rm -rf /dev/shm/sem.SFNT*");
        $licenceFile = \JpegminiConfig::$licenceFile;
        $command = "jpegmini -f=\"$path\" -lc=$licenceFile";
        // If image does not exist on local directory download it from S3
        if(!file_exists($path)){
            $s3ImagePath = "http://".\SystemConfig::$amazonS3Bucket.".s3.amazonaws.com".substr($path, strlen(\HostConfig::$documentRoot));
            if(copy($s3ImagePath, $path) == false){
                self::getLog()->debug(__class__.":".__line__.":Not able to download original image from : '$s3ImagePath' : to :  '$path'");
                return false;
            }
        }
        self::getLog()->debug(__class__.":".__line__.": Running system command '$command'");
        exec($command, $output, $ret);
        if($ret != 0){
            self::getLog()->debug(__class__.":".__line__.": Returning false due to error :$ret encountered while executing command  , output = ".print_r($output, true));
            return false;
        }
        self::getLog()->debug(__class__.":".__line__.": Returning true as command ran successfully with return as $ret and output = ".print_r($output, true));
        return true;
    }
    /*
    * Function to copy compressed placeholder file.
    * @param string file path of the file to compress
    * @return true in case of success and false in case of failure
    */
    function copyCompressedPlaceholderFile($filesCsv){
        $files = explode(',',$filesCsv);
        foreach ($files as $file){
            if(empty($file))
                continue;
            if(strpos($file, \HostConfig::$cdnBase) === 0){
                $file = substr($file, strlen(\HostConfig::$cdnBase));
            }
            if(strpos($file, \HostConfig::$documentRoot) === 0){
                $file = substr($file, strlen(\HostConfig::$documentRoot));
            }
            if(strpos($file, "./images/") === 0){
                $file = substr($file, 1);
            }
            if(strpos($file, "../images/") ===0){
                $file = substr($file, 2);
            }if(strpos($file, "images/") ===0){
                $file = "/".$file;
            }
            $fileOrg= $file;
            $file = dirname(__FILE__)."/../../..$file";
            if(empty($file) || !is_file($file)){
                self::getLog()->debug(__class__.":".__line__.": Returning false as file is empty or does not exist: '$file'");
                return false;
            }
            if(is_file($file)){
                if(copy($file, self::getCompressedImagePathFromOriginalPath($file)) == false||moveToDefaultS3Bucket(self::getCompressedImagePathFromOriginalPath($fileOrg))==false){
                    self::getLog()->debug(__class__.":".__line__.": Failed to copy compressed placeholder file for : '$file'");
                    return false;
                }
            }
            self::getLog()->debug(__class__.":".__line__.": Successfully copied compressed placeholder file for :'$file'");
        }
        return true;
    }
}
?>
