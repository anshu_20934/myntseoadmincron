<?php

namespace imageUtils\dao;
use cache\keyvaluepairs\JpegminiKeyValuePairs;

use base\dao\BaseDAO;
include_once("$xcart_dir/include/class/widget/class.widget.keyvaluepair.php");
include_once("$xcart_dir/include/func/func.utilities.php");

/*
* Dao for Jpegmini configuration
*/
class JpegminiDBAdapter extends BaseDAO {

    private static $configKey = "jpegminiConfig";

    public function getConfig($key){
        global $weblog;
		$result = JpegminiKeyValuePairs::getKeyValuePairInstance()->getJSONDecodedArray(self::$configKey);
		$return = empty($key) ? $result : $result[$key];
		return $return;
    }

    public function updateConfig($data){
        if(empty($data) || !is_array($data)){
            return;
        }
		$config = self::getConfig();
        foreach($data as $key=>$value){
            $update_key = $value["image_key"];
			if(is_array($value)){
            	$enabled = $value["enabled"];
            	$enabled = ($enabled == 1) ? "1" : "0";
			}else{
				$update_key = $key;
				$enabled = $value;
			}
			if($key != $update_key){
				unset($config[$key]);
			}
			$config[$update_key] = $enabled;
        }
		$config = json_encode($config);
		\WidgetKeyValuePairs::setWidgetValueForKey(null, self::$configKey, $config);
		\WidgetKeyValuePairs::refreshKeyValuePairsInCache();
		JpegminiKeyValuePairs::getKeyValuePairInstance()->refresh();
    }

    public function deleteConfig($key){
        if(empty($key) || !is_string($key)){
            return;
        }

        $key = mysql_real_escape_string($key);
		$config = self::getConfig();
		unset($config[$key]);
		$config = json_encode($config);
        \WidgetKeyValuePairs::setWidgetValueForKey(null, self::$configKey, $config);
        \WidgetKeyValuePairs::refreshKeyValuePairsInCache();
        JpegminiKeyValuePairs::getKeyValuePairInstance()->refresh();
    }
}
