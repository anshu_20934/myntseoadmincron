<?php
include_once $xcart_dir."/include/func/func.utilities.php";
include_once $xcart_dir."/include/func/func.user.php";
require_once $xcart_dir."/include/func/func.core.php";
include_once($xcart_dir."/include/class/class.ordertime.php");
include_once($xcart_dir."/include/func/func.mk_old_returns_tracking.php");
include_once "$xcart_dir/modules/loyaltyPoints/LoyaltyPointsPortalClient.php";
include_once "$xcart_dir/modules/loyaltyPoints/LoyaltyPointsPortalHelper.php";
include_once("$xcart_dir/include/class/oms/EventCreationManager.php");

use user\UserAuthenticationManager;
use revenue\payments\service\PaymentServiceInterface;

Class MyMyntra {

    public static $ERROR   = "error";
    public static $MESSAGE = "message";
    public static $COMPLETED_ORDER   = "completed";
    public static $OUTSTANDING_ORDER = "outstanding";
    public static $OTHER_ORDER       = "other";

    private $_login;

    function __construct($login){
        $this->login = $login;
    }

    function addAddress($data){
        if(!empty($this->login) && !empty($data)){
            $data["login"] = $this->login;
            $result = func_select_first('mk_customer_address', array('login'=>$this->login));
            if($result == NULL){//If this is the first address added by the user then mark it as default
                $data["default_address"] = 1;
            }
            $result2 = func_array2insertWithTypeCheck('mk_customer_address', $data);
            if($result2){
                return array(self::$MESSAGE=>"Address has been added successfully", "id" => $result2);
            } else {
                return array(self::$ERROR=>"Address could not be added due to unknown error");
            }
        }
    }

    function updateAddress($data){
        $id = $data["id"];
        unset($data["id"]);
        if ( !empty($this->login) && !empty($data) && !empty($id) ){
            if(!empty($this->login) && !empty($data)){
                $result = func_select_first('mk_customer_address', array('login'=>$this->login, 'id'=>$id));
                if($result != NULL){

                	$whereClause = array(
                		"id" => array("value"=>$id, "type"=>"number"),
                		"login" => $this->login
                	);

                    $result2 = func_array2updateWithTypeCheck('mk_customer_address', $data, $whereClause);
                    if($result2){
                        return array(self::$MESSAGE=>"Address has been updated successfully");
                    } else {
                        return array(self::$ERROR=>"Address could not be updated due to unknown error");
                    }
                }
            }
        }
    }

    function deleteAddress($id){
        if(!empty($this->login) && !empty($id)){
            $id = mysql_real_escape_string($id);
            $result = func_select_first('mk_customer_address', array('login'=>$this->login, 'id'=>$id), NULL, NULL, array("default_address"=>"desc"));
            if($result != NULL){
                $sql = "DELETE from mk_customer_address where id = $id";
                $delete_result = db_query($sql);
                if( $result['default_address'] == 1 ){// If we have deleted a default address we need to mark the first available address as default
                    $result2 = func_select_first('mk_customer_address', array('login'=>$this->login));
                    if( $result2 != NULL ){
                        func_array2update('mk_customer_address', array('default_address'=>1), 'id = ' . $result2['id']);
                    }
                }
                if($delete_result){
                    return array(self::$MESSAGE=>"Address has been deleted successfully");
                } else {
                    return array(self::$ERROR=>"Address could not be deleted due to unknown error");
                }
            }
        }
    }

    function getUserAddresses(){
        $countries = func_get_countries();
        $addresses = func_select_query('mk_customer_address', array('login'=>$this->login), NULL, NULL, array("default_address"=>"desc"));
        foreach ($addresses as $key=>$address){
            $country_code = $address['country'];
            $state_code = $address['state'];
            $addresses[$key]['country_display'] = func_get_country($country_code);
            $addresses[$key]['state_display']  = func_get_state($state_code, $country_code);
        }
        return $addresses;
    }

    function setDefaultAddress($id){
        if( !empty($this->login) && !empty($id) ){
            $result = func_select_first('mk_customer_address', array('login'=>$this->login, 'id'=>$id));
            if($result != NULL){// Check to see if the selected address is for the user
                $default_res = func_select_first('mk_customer_address', "login='$this->login' AND default_address=1");
                if($default_res != NULL) {
                    $default_id = $default_res["id"];
                } else {
                    $default_id = NULL;
                }

                $data = array("default_address"=>0);
                $ret = func_array2update('mk_customer_address', $data, "login='$this->login' ");
                if($ret){
                    $data = array("default_address"=>1);
                    $ret2 = func_array2update('mk_customer_address', $data, "login='$this->login' AND id=$id");
                    if($ret){
                        return array(self::$MESSAGE=> "Address set as default successfully");
                    } elseif($default_id != NULL){
                        // Setting the previous set default address as default
                        func_array2update('mk_customer_address', array('default_address'=>1), "login='$this->login' AND id=$default_id");
                        return array(self::$ERROR=> "Address could not be set as default due to unknown error");
                    } else {
                        return array(self::$ERROR=> "Address could not be set as default due to unknown error");
                    }
                } else {
                    return array(self::$ERROR=> "Address could not be set as default due to unknown error");
                }

            }
        }
    }

    function updateProfile($data){
        $data = sanitizeDBValues($data);
        if( $this->login && $data ){
            $ret = func_array2update('xcart_customers', $data, "login='$this->login'");
            if($ret){
                return array(self::$MESSAGE=>"Your profile has been successfully updated");
            } else {
                return array(self::$ERROR=>"Profile could not be updated due to unknown error");
            }
        }
    }
    
    function insertPersonna($data){
        if( $this->login && $data ){
            $ret = func_array2insertWithTypeCheck('customer_personna', $data);
            return $ret;
            
        }
    }
    
    function updatePersonna($data){
        $data = sanitizeDBValues($data);
        if( $this->login && $data ){
        	$ret = func_array2updateWithTypeCheck('customer_personna', $data, "login='$this->login'");
          	return $ret;
        }
    }

    function changePassword($old_password, $new_password){
    	$userAuthMgr = new UserAuthenticationManager();
    	
    	try{
    		$userAuthMgr->changeUserPassword($this->login, user\User::USER_TYPE_CUSTOMER, $old_password, $new_password);
    		return array(self::$MESSAGE=>"Password updated successfully");
    	}catch(user\exception\UserDoesNotExistsException $ex){
    		return array(self::$ERROR=>"Userid supplied is not valid");
    	}catch(user\exception\PasswordDoesNotMatchException $ex){
    		return array(self::$ERROR=>"Old Password supplied is not valid");
    	}catch(MyntraException $ex){
    		return array(self::$ERROR=>"Password could not be changed due to unknown error");
    	}
    }
    function getUserProfileData(){
        if(!$this->login){
            return NULL;
        }
        $userProfileData = func_select_first('xcart_customers', array('login'=>$this->login));
        return $userProfileData;
    }
    
     function getUserPersonnaData(){
        if(!$this->login){
            return NULL;
        }
        
        $userPersonnaData = func_select_first('customer_personna', array('login'=>$this->login));
        return $userPersonnaData;
    }
    

    function getBasicUserProfileData($login){
        if(!$this->login){
            return NULL;
        }
        
        global $sql_tbl;
        $query = "SELECT login, firstname, lastname, mobile from " . $sql_tbl['customers'] . " WHERE login = '$login';";
        $userProfileData = func_query_first($query);
        return $userProfileData;
    }

    function getBreakUpQuantityArray($quantityBreakup) {
    	$optionQuantity = array();

    	if(!empty($quantityBreakup)) {

    		$quantityArray = explode(",", $quantityBreakup);

    		foreach ($quantityArray as $singleOptionQuantity) {
    			$singleOptionQuantityValue = explode(":", $singleOptionQuantity);
    			$size = $singleOptionQuantityValue[0];
    			$quantity = $singleOptionQuantityValue[1];

    			switch (strtolower($size)) {
    				case 'xxs' 	:  $singleOptionQuantityValue[0] = 'Extra Extra Small';
    								break;
					case 'xs'	:  	$singleOptionQuantityValue[0] = 'Extra Small';
    								break;
    				case 's'	:	$singleOptionQuantityValue[0] = 'Small';
    								break;
    				case 'm'	:	$singleOptionQuantityValue[0] = 'Medium';
    								break;
    				case 'l'	:	$singleOptionQuantityValue[0] = 'Large';
    								break;
    				case 'xl'	:	$singleOptionQuantityValue[0] = 'Extra Large';
    								break;
    				case 'xxl'	:	$singleOptionQuantityValue[0] = 'Extra Extra Large';
    								break;
    			}

    			if(is_numeric($size)) {
    				$singleOptionQuantityValue[0] = 'Size ' . $size;
    			}

    			if(isset($quantity) && !empty($quantity)) {
    				$optionQuantity[] = array("option"=>$size, "size"=>$singleOptionQuantityValue[0], "quantity"=>$singleOptionQuantityValue[1]);
    			}
    		}
    	}
    	return $optionQuantity;
    }

    /*
     * updates orders table item status to non-returnable 
     * if succeeded in update, call loyalty points service for points activation
     * 
     */
    function confirmGetLoyaltyPoints($login,$itemId,$orderId){ 

    	
    	
    	$row = func_query_first("select item_loyalty_points_awarded,o.group_id from xcart_order_details as od, xcart_orders as o where o.orderid=od.orderid and o.orderid = $orderId and o.status in ('DL','C') and  od.itemid = ".mysql_escape_string($itemId),true);
    	
    	if(empty($row)){
    		return array();
    	}
    	
    	$query = "update xcart_order_details set is_returnable = 0 where itemid = ".mysql_escape_string($itemId)." and is_returnable != 0";
        
        db_query($query);
        
        $rowsAffected = db_affected_rows();
        
        $retArr = array();
        if($rowsAffected > 0 && isset($row["item_loyalty_points_awarded"])){
        	$retArr = LoyaltyPointsPortalClient::activatePointsForOrder($login, $row["group_id"], floatval($row["item_loyalty_points_awarded"]), "Activating Points for order no. ".$row["group_id"]);
        	LoyaltyPointsPortalHelper::resetLoyaltyPointsHeaderDetails($login);
            if(empty($retArr['status']) || ($retArr['status']['statusType'] != 'SUCCESS')){
                $query = "update xcart_order_details set is_returnable = 1 where itemid = ".mysql_escape_string($itemId)." and is_returnable != 1";
                db_query($query);
            }
        }
        EventCreationManager::pushCompleteOrderEvent($orderId);
        return $retArr;
    }
    
    
    function getUserOrderHistory($getItemDetails = false, $limit = 20){
    	global $sql_tbl, $weblog;
    	
    	$paymentServiceEnabled = PaymentServiceInterface::getPaymentServiceEnabledForUser($this->login);
    	if(!$paymentServiceEnabled){
        $query = "SELECT a.warehouseid,a.orderid, invoiceid, a.login, firstname, lastname, status, date, queueddate, shippeddate, delivereddate, a.ordertype, 
	                  total, subtotal,gift_charges, shipping_cost, tax, coupon_discount, cash_redeemed, pg_discount,payment_surcharge as payment_surcharge, discount, gift_charges, cod,
	                  a.courier_service as courier_code, cashback, tracking, gift_status,notes, d.payment_option as payment_method, qtyInOrder,
	                  s_firstname, s_lastname, s_address, s_city, s_locality, s_state, c.state as s_state_name, s_zipcode, s_country, mobile,
	                  b.code, b.courier_service, b.website, a.ccavenue, a.loyalty_points_awarded, a.channel, a.group_id, a.cart_discount,a.payment_method as pm
	                  FROM xcart_orders a LEFT JOIN mk_courier_service b ON a.courier_service=b.code
	                  LEFT JOIN xcart_states c ON a.s_state = c.code and a.s_country = c.country_code
					  LEFT JOIN mk_payments_log d ON a.group_id=d.orderid
	                  WHERE a.login='$this->login' AND status != 'PP' AND status != 'F' ";
	
	
	        $query .= "ORDER BY group_id DESC,shippeddate desc ";
	        if(!empty($limit) && $limit>0) {
	        	$query .= "limit $limit ";
	        }
	
	        $result = func_query($query);
    	}
    	else {
        $query = "SELECT a.warehouseid,a.orderid, invoiceid, a.login, firstname, lastname, status, date, queueddate, shippeddate, delivereddate, a.ordertype, 
    		total, subtotal,gift_charges, shipping_cost, tax, coupon_discount, cash_redeemed, pg_discount,payment_surcharge as payment_surcharge, discount, gift_charges, cod,
    		a.courier_service as courier_code, cashback, tracking, gift_status,notes, qtyInOrder,
    		s_firstname, s_lastname, s_address, s_city, s_locality, s_state, c.state as s_state_name, s_zipcode, s_country, mobile,
    		b.code, b.courier_service, b.website, a.ccavenue, a.loyalty_points_awarded, a.channel, a.group_id, a.cart_discount,a.payment_method as pm
    		FROM xcart_orders a LEFT JOIN mk_courier_service b ON a.courier_service=b.code
    		LEFT JOIN xcart_states c ON a.s_state = c.code and a.s_country = c.country_code
    		WHERE a.login='$this->login' AND status != 'PP' AND status != 'F' ";
    		
    		$query .= "ORDER BY group_id DESC,shippeddate desc ";
    		if(!empty($limit) && $limit>0) {
    			$query .= "limit $limit ";
    		}
    		
    		$result = func_query($query);
        	unset($orderElement);
    	}

    	$completedOrders = array();
        $outstandingOrders = array();
        $inCompleteOrders = array();
        $otherOrders = array();
        $orderMap = array();

        if($result && count($result) > 0){
        	$orderIds = array();
        	$orderId2ItemsMapping = array();
        	if($getItemDetails) {
	        	foreach($result as $order) {
                    $orderIds[] = $order['orderid'];
                }

	        	$items_sql = "SELECT a.*, xo.warehouseid, a.completion_time AS completion_time, a.product_style, d.name AS stylename,
	                          a.quantity_breakup, a.assignment_time AS assignment_time, sp.default_image,
	                          sp.global_attr_article_type,sp.global_attr_master_category, sp.global_attr_sub_category,xa.returnid, xa.is_refunded,xa.status as return_status,
	                          a.product_style as style_id, a.is_returnable, a.item_loyalty_points_awarded, a.item_loyalty_points_used, xo.loyalty_points_conversion_factor, sosm.sku_id 
	                          FROM ((((((xcart_order_details a JOIN mk_product_style d ON a.product_style=d.id)
	                          	JOIN xcart_orders xo ON xo.orderid = a.orderid)
	                          LEFT JOIN mk_style_properties sp on d.id=sp.style_id)
	                          left join mk_order_item_option_quantity oq on a.itemid = oq.itemid)
	                          left join mk_styles_options_skus_mapping sosm on oq.optionid = sosm.option_id)
	                          left outer join xcart_returns xa on  xa.status != 'RRD' and xa.itemid = a.itemid)
	                          where a.item_status != 'IC' AND a.orderid in (" . implode(",", $orderIds) . ")";

               	$items = func_query($items_sql);
               	
               	if($items){
               		foreach ($items as $key=>$item) {
		               	$exchangeOrderExists = func_query_first("SELECT ex.* from exchange_order_mappings ex, xcart_orders o where "
		               			."ex.itemid = $item[itemid] and o.orderid = ex.exchange_orderid and o.status not in ('F', 'D', 'L')
		               			order by exchange_orderid DESC", true);
		               	if($exchangeOrderExists != ''){
		               		$exchangeRTOd = isRTO($exchangeOrderExists['exchange_orderid']);
		               		if($exchangeRTOd){
		               			$items[$key]['exchange_orderid'] = '';
		               		} else {
		               			$items[$key]['exchange_orderid'] = $exchangeOrderExists['exchange_orderid'];
		               		}
		               	}
		               	else {
		               		$items[$key]['exchange_orderid'] = '';
		               	}
               		}
               	}

               	if($items) {
               		foreach($items as $key=>$item) {
               			if(!isset($orderId2ItemsMapping[$item['orderid']]))
               				$orderId2ItemsMapping[$item['orderid']] = array();

               			$orderId2ItemsMapping[$item['orderid']][] = $item;
               		}
               	}
        	}
        	$return_disabled_styles = \FeatureGateKeyValuePairs::getFeatureGateValueForKey('return.disable.category');
			$return_disabled_styles = explode(",", $return_disabled_styles);
			//$jewelleryItems = explode(',', trim(FeatureGateKeyValuePairs::getFeatureGateValueForKey('jewellery.article.types')));

            foreach($result as $order){
            	$order_items = $orderId2ItemsMapping[$order['orderid']];
                if($order_items && count($order_items) > 0) {
                	$final_items = array();
                    foreach($order_items as $key=>$singleitem) {
                    	if(!empty($singleitem['default_image'])) {
                    		// Rectangular thumbnails
                    		$singleitem['default_image'] = str_replace("_images", "_images_96_128", $singleitem['default_image']);
                    	}
                    	$styleid=$singleitem['product_style'];
                    	$unifiedsize=Size_unification::getUnifiedSizeByStyleId($styleid,"array",true);

                    	$singleitem['subtotal_price'] =number_format($singleitem['price']*$singleitem['amount'], 2, '.', '');
                    	if($singleitem['discount'] != 0.00) {
							$singleitem['discount_percent'] = round(($singleitem['discount']/($singleitem['subtotal_price']))*100);
							$singleitem['discounted_price'] = number_format(($singleitem['price']*$singleitem['amount'] - $singleitem['discount']), 2, '.','');
						}
                        $singleitem['loyalty_points_used_rupees'] = $singleitem['item_loyalty_points_used'] * $singleitem['loyalty_points_conversion_factor'] ;
                    	$singleitem['final_price_paid'] = number_format(($singleitem['subtotal_price'] - $singleitem['discount'] - $singleitem['cart_discount_split_on_ratio'] - $singleitem['coupon_discount_product'] - $singleitem['cash_redeemed'] - $singleitem['pg_discount'] - ($singleitem['item_loyalty_points_used'] * $singleitem['loyalty_points_conversion_factor'] )), 2, '.', '');

						if(!empty($singleitem['global_attr_article_type']) && !empty($singleitem['product_style'])){
                    		$fineJewelleryItem = func_query("select * from mk_attribute_type at, mk_attribute_type_values av, mk_style_article_type_attribute_values sav where at.id = av.attribute_type_id and av.id = sav.article_type_attribute_value_id and at.catalogue_classification_id = ".$singleitem['global_attr_article_type']." and sav.product_style_id = ".$singleitem['product_style'] ." and av.attribute_value = 'Fine Jewellery'", true);
						}
						
						if($singleitem['is_returnable']==0 && $singleitem['is_discounted']==1){//in case of discounted free items do not support returnability
							$return_allowed = false;
                            $singleitem['return_criteria'] = 'not_returnable';
						}elseif($order['status'] == 'DL' || $order['status'] == 'C'){
							if($order['ordertype'] == 'ex'){
								$parentid = func_query_first_cell("select releaseid from exchange_order_mappings where exchange_orderid = $order[orderid]", true);
								$curr_ordertype = $order['ordertype'];
							}
							$exorderid = $order['orderid'];
							$original_delivereddate = $order['delivereddate'];
							while($curr_ordertype == 'ex'){
								$parent_orderid = func_query_first_cell("select releaseid from exchange_order_mappings where exchange_orderid = $exorderid", true);
								$parent_order = func_query_first("select ordertype, delivereddate from xcart_orders where orderid = $parent_orderid", true);
								if($parent_order['ordertype'] == 'ex'){
									$exorderid = $parent_orderid;
								} else {
									$original_delivereddate = $parent_order['delivereddate'];
									$curr_ordertype = '';
									break;
								}
							}
							if($order['delivereddate'] && $order['delivereddate'] >0 ){
								if($fineJewelleryItem && count($fineJewelleryItem) > 0){
									$returnPeriod = 15*24*60*60;
								}
								else {
									$returnPeriod = 30*24*60*60;
								}
								$return_allowed = ($original_delivereddate + $returnPeriod > time())?true:false;
							}else{
								$return_allowed = false;
							}
                            $singleitem['return_criteria'] = $return_allowed ? 'allowed' : 'time_exceeded';
                    	}else{
                    		$return_allowed = false;
                    	}

                    	$itemid = $singleitem['itemid'];

                    	$sql = "select * from mk_order_item_option_quantity moioq, mk_product_options po where moioq.optionid = po.id and moioq.itemid = $itemid";
						$order_option_qty = func_query_first($sql);
						$optionsQty = $this->getBreakUpQuantityArray($order_option_qty['value'].":".$order_option_qty['quantity']);
						foreach ($optionsQty as $option) {
							if($option['quantity']) {
								$singleitem['option'] = $option['option'];
								$singleitem['size'] = $option['size'];
								$singleitem['quantity'] = $option['quantity'];
								break;
							}
						}
						// If order was complete and item status is not QD or D, it means item was returned.
						// Get the return request id for this item.
						$singleitem['unifiedsize']=$unifiedsize[$order_option_qty['value']];
						$singleitem['quantity_breakup_message'][0]["unifiedsize"]=$unifiedsize[$order_option_qty['value']];
	                    if($return_allowed){
					        if(in_array($singleitem['global_attr_article_type'], $return_disabled_styles) || in_array($singleitem['global_attr_master_category'], $return_disabled_styles) || in_array($singleitem['global_attr_sub_category'], $return_disabled_styles)){
                                //$singleitem['return_allowed'] = false;
                                $singleitem['return_criteria'] = 'not_returnable';
					        }else if($fineJewelleryItem && count($fineJewelleryItem) > 0){
					    		$singleitem['return_criteria'] = 'jewellery';
					    	} 
					    }

					    $final_items[] = $singleitem;

                    }
                   	$order['items'] = $final_items;
                }

                $status = $order['status'];
                $supported_cour_op = FeatureGateKeyValuePairs::getFeatureGateValueForKey('shipmentTracking.supportedCourierOperator');
                $supported_order_status = FeatureGateKeyValuePairs::getFeatureGateValueForKey('shipmentTracking.supportedOrderStatus');
                $supported_cour_op_array = explode(",", $supported_cour_op);
                $supported_order_status_Array = explode(",", $supported_order_status);
                $order['showTrackingStatus'] = "false";
                /*Check if this order satisfies both the condition like
                 * courier operator and order status for which shipment tracking should be shown
                 */
                
                foreach ($supported_cour_op_array as $cour_op_key => $cour_op_value) {
                    if (strcmp($cour_op_value, $order['courier_code']) == 0) {
                        if ($order['delivereddate'] != null && $order['delivereddate'] != "" && $order['delivereddate'] != 0 &&
                                $order['delivereddate'] < (time() - 30 * 24 * 60 * 60)) {
                            $order['showTrackingStatus'] = "purged";
                        } else {
                            foreach ($supported_order_status_Array as $order_status_key => $order_status_value) {
                                if (strcmp($order_status_value, $status) == 0) {
                                	$order['showTrackingStatus'] = "true";
                                    /* $count_sql = "select count(*) AS cnt FROM mk_shipment_tracking_detail mstd,mk_shipment_tracking mst
                                    WHERE mst.id = mstd.id AND mst.ref_id = " . $order['orderid'];
                                    $shipmentTrkCount = func_query($count_sql);
                                    foreach ($shipmentTrkCount as $key => $value) {
                                        if ($value['cnt'] > 0) {
                                            $order['showTrackingStatus'] = "true";
                                        } else {
                                            $order['showTrackingStatus'] = "empty";
                                        }
                                    } */
                                }
                            }
                        }
                    } else {
                    	$order['showTrackingStatus'] = "unsupported";
                    }
                }

                $incOrder = false;
                switch($status) {

                		/** --- (1) Cases for Completed Orders --- **/

                        case 'C' : // 'C' implies order is Complete
                        {
                                $order['customerstatusmessage'] = 'DELIVERED';
                                $completedOrders[] = $order;
                                $incOrder = true;
                                break;
                        }

                        case 'DL' : // 'DL' implies order is Delivered
                        {
                                $order['customerstatusmessage'] = 'DELIVERED';
                                $completedOrders[] = $order;
                                $incOrder = true;
                                break;
                        }

                        /** --- End Completed Orders --- **/




                        /** ### (2) Cases for Outstanding Orders ### **/

                        case 'OH' : // On Hold
                        {
                                $order['customerstatusmessage'] = "BEING PROCESSED";
                                $outstandingOrders[] = $order;
                                $incOrder = true;
                                break;
                        }

                        case 'PD' : // Pending Check Collection
                        {
                                $order['customerstatusmessage'] = "Pending Check Collection";
                                $outstandingOrders[] = $order;
                                $incOrder = true;
                                break;
                        }

                        case 'Q' : // Order is Queued for ops guys to start work on.
                        {
                                $order['customerstatusmessage'] = "BEING PROCESSED";
                                $outstandingOrders[] = $order;
                                $incOrder = true;
                                break;
                        }

                        case 'PK' : // Order is packed and handed over to logistics section for shipping.
                        {
                        		$order['customerstatusmessage'] = "PROCESSED";
                        		$outstandingOrders[] = $order;
                        		$incOrder = true;
                        		break;
                        }
                        	
                        case 'SH' : // 'COD' order has been Shipped
                        {

                            $order['customerstatusmessage'] = "SHIPPED";
                            if($order['queueddate'] && !empty($order['queueddate'])){
                                //get delivery date based on shiptime and logistic selected
                                $order['receiptdate'] = getDeliveryDateForOrder($order['shippeddate'], $orderId2ItemsMapping[$order['orderid']], $order['s_zipcode'], $order['pm'],$order['courier_code'],false, $order['orderid']);
                            }

                            $outstandingOrders[] = $order;
                            $incOrder = true;

                            break;
                        }

                        case 'WP' : // Work in Progress  (Shown WIP in admin)
                        {
                                $order['customerstatusmessage'] = "BEING PROCESSED";
                                $outstandingOrders[] = $order;
                                $incOrder = true;
                                break;
                        }

                        case 'RFR' : // Ready For Release
                        {
                                $order['customerstatusmessage'] = "READY FOR RELEASE";
                                $outstandingOrders[] = $order;
                                $incOrder = true;
                                break;
                        }
                        /** ### End Outstanding Orders ### **/



                        /** ### (3) Handle other Status Codes ### **/

                        case 'D' : // Declined Order
                        {
                                $order['customerstatusmessage'] = "DECLINED";
                                $otherOrders[] = $order;
                                break;
                        }

                        case 'F' : // Failed
                        {
                                $order['customerstatusmessage'] = "FAILED";
                                $otherOrders[] = $order;
                                break;
                        }

                        case 'PP' : // Order in Pre-Processed state
                        {
                                $order['customerstatusmessage'] = "PRE PROCESSED";
                                $otherOrders[] = $order;
                                break;
                        }


                        /** ### End other Status Codes ### **/


                        /** --- Obsolete Status Codes. Not doing anything with them **/

                        case 'PV': break; // COD order - Pending Verification
                        case 'B' : break; // Backordered
                        case 'FD': break; // Failed to Deliver
                        case 'I' : break; // Implies Not Finished
                        case 'PC': break; // Pending Delivery

                        /** --- End Obsolete Status Code --- **/
                }
                // process only pending and completed orders
                if (!$incOrder) continue;

                // orderMap - the first item is the base order and the rest are splitted orders
                $groupId = $order['group_id'];
                if(!isset($orderMap[$groupId]))
                	$orderMap[$groupId] = array('orders' => array(), 'total' => 0, 'count' => 0);

                $orderMap[$groupId]['orders'][] = $order;
            }
        }
		$current_time = time();
        foreach ($orderMap as $id => $val) {
            $orderMap[$id]['delivered'] = 0;
            $orderMap[$id]['titles'] = array();
            $cancellable = 'true';
            $shipping_dates = array();
            foreach ($orderMap[$id]['orders'] as $order) {
            	if($order['shippeddate'] > 0){
            		$shipping_dates[] = $order['shippeddate']; 
            	}else{
            		$shipping_dates[] = $current_time;
            	}
          		if($order['status'] != 'WP' && $order['status'] != 'OH'){
          			$cancellable = 'false';
          		}
          		$orderMap[$id]['count'] += $order['qtyInOrder'];
                $orderMap[$id]['total'] += $order['total']+$order['payment_surcharge'];
                $orderMap[$id]['payment_surcharge'] += $order['payment_surcharge'];
                if ($order['customerstatusmessage'] == 'DELIVERED') {
                    $orderMap[$id]['delivered'] += $order['qtyInOrder'];
                }
                foreach ($order['items'] as $orderitem) {
                    $orderMap[$id]['titles'][] = $orderitem['stylename'];
                    $orderMap[$id]['itemsCount'] += $orderitem['amount'];
                }
				$orderMap[$id]['cancellable'] = $cancellable;
            }
			if(count($shipping_dates) > 1){
				array_multisort($shipping_dates,$orderMap[$id]['orders']);
			}
            // set the status to completed, if all the splitted orders are completed

            $orderMap[$id]['status'] = $orderMap[$id]['delivered'] == $orderMap[$id]['count'] ? 'COMPLETED' : 'PENDING';
        }

        $weblog->info("All Orders - ". print_r($orderMap, true));

        return $orderMap;
    }

    static function splitFirstLastName($name){
        $name = trim($name);
        if(empty($name)){
            return null;
        }
        if(strrpos($name, " ") == false){
            return array($name);
        }
        $lastpos = strrpos($name, " ");
        $firstname = substr($name, 0 , $lastpos);
        $lastname = substr($name, $lastpos + 1);
        return array($firstname, $lastname);
    }

    function getUserReturns($login,$completed) {
    	global $sql_tbl, $weblog;
    	$weblog->debug("completed flag - $completed");
    	if($completed == '' || $completed == null){
    		$returnsSql = "SELECT sd.cust_end_state, r.*,xo.group_id as group_id,xo.payment_surcharge as payment_surcharge, xo.payment_method as payment_method, xo.status as order_status from xcart_returns r, mk_return_status_details sd,xcart_orders xo where r.status = sd.code and r.orderid = xo.orderid and r.status != 'RRD' and r.login = '$login' order by r.createddate desc";	
    	}else{
    		if($completed == 'true'){
    			$returnsSql = "SELECT r.*,xo.status as order_status,xo.payment_surcharge as payment_surcharge from xcart_returns r, mk_return_status_details sd,xcart_orders xo where r.status = sd.code and sd.cust_end_state = 1 and r.orderid = xo.orderid and r.status != 'RRD' and r.login = '$login'";
    		}else{
    			$returnsSql = "SELECT r.*,xo.status as order_status,xo.payment_surcharge as payment_surcharge from xcart_returns r, mk_return_status_details sd,xcart_orders xo where r.status = sd.code and sd.cust_end_state = 0 and r.orderid = xo.orderid and r.status != 'RRD' and r.login = '$login'";
    		}
    	}

    	$weblog->debug("Execute Query - $returnsSql");

    	$results = func_query($returnsSql);
    	if($results) {
		
		$formattedReturnsData=format_return_requests_fields($results);
		$pendingReturnsData=array();
		$completedReturnsData=array();

		foreach($formattedReturnsData as $key => $value){

			$eom = func_query_first("select returnid,exchange_orderid from exchange_order_mappings where returnid = ".$value['returnid'], true);
			
			if(!$eom || ($eom && isRTO($eom['exchange_orderid']))){
				$status=$value["cust_end_state"];
				if($status || $value['is_refunded']){
					array_push($completedReturnsData, $formattedReturnsData[$key]);
				}
				else{
					array_push($pendingReturnsData, $formattedReturnsData[$key]);
				}
			}
		}
		$finalReturnsData["pending"]=$pendingReturnsData;
		$finalReturnsData["completed"]=$completedReturnsData;
    		return $finalReturnsData;
    	}
    	return array();
    }

    function getOrderAndReturnsCounts($login){
    	$counts = array();
    	$counts["completed_orders"] = 0;
    	$counts["pending_orders"] = 0;
    	$counts["completed_returns"] = 0;
 		$counts["pending_returns"] = 0;
    	$sql = "select sum(case when status in ('DL','C') then 1 else 0 end) as completed_orders, sum(case when status in ('OH','PD','Q','WP','SH','RFR') then 1 else 0 end) as pending_orders from xcart_orders WHERE login='$this->login' AND status != 'PP' AND status != 'F'";
    	$orders = func_query_first($sql);
    	$counts["completed_orders"] = $orders['completed_orders']!=''?$orders['completed_orders']:0;
    	$counts["pending_orders"] = $orders['pending_orders']!=''?$orders['pending_orders']:0;
    	$sql = "SELECT count(1) as cnt,sd.cust_end_state from xcart_returns r, mk_return_status_details sd where r.status = sd.code and r.status != 'RRD' and login = '$login' group by sd.cust_end_state";
    	$returns = func_query($sql);
    	foreach ($returns as $return){
    		if($return['cust_end_state'] == 1){
    			$counts["completed_returns"] = $return['cnt'];
    		}else{
    			$counts["pending_returns"] = $return['cnt'];
    		}
    	}
    	return $counts;
    }

    function getOrderAndReturnsCountsForDashboard($login){
    	$counts = array();
    	$counts["completed_orders"] = 0;
    	$counts["pending_orders"] = 0;
    	$counts["completed_returns"] = 0;
 		$counts["pending_returns"] = 0;
    	$sql = "SELECT group_id, status from xcart_orders WHERE login='$this->login' AND status != 'PP' AND status != 'F'";
    	$orders = func_query($sql);
    	$baseOrders = array();
    	foreach ($orders as $order) {
    		if ($order['status'] == 'OH' || $order['status'] == 'PD' || $order['status'] == 'Q' || $order['status'] == 'WP' || $order['status'] == 'SH'|| $order['status'] == 'RFR') {
    			$baseOrders[$order['group_id']] = 'P';
    		} else if (($order['status'] == 'C' || $order['status'] == 'DL') && $baseOrders[$order['group_id']] != 'P') {
    			$baseOrders[$order['group_id']] = 'C';
    		}
    	}
    	$orderCounts = array_count_values($baseOrders);
    	$counts["completed_orders"] = $orderCounts['C']?$orderCounts['C']:0;
    	$counts["pending_orders"] = $orderCounts['P']?$orderCounts['P']:0;

	$sql = "SELECT count(1) as cnt,sd.cust_end_state, r.returnid from xcart_returns r, mk_return_status_details sd where r.status = sd.code and r.status != 'RRD' and login = '$login' group by sd.cust_end_state";
        $returns = func_query($sql);
           
        foreach ($returns as $return){
	   $eom = func_query_first("select returnid,exchange_orderid from exchange_order_mappings where returnid = ".$return['returnid'], true);
		   
	   if(!$eom || ($eom && isRTO($eom['exchange_orderid']))){
		   if($return['cust_end_state'] == 1){
			   $counts["completed_returns"] = $return['cnt'];
		   }else{
			   $counts["pending_returns"] = $return['cnt'];
		   }
	   }
        }

    	return $counts;
    }
   
    //Brand Preferences Section
    
    
    
    function getRevisionNumber(){
    	$values=array(
			'revtstmp'=>time()
    	);
    	$insert_result = func_array2insertWithTypeCheck('REVINFO', $values);
    	return $insert_result;
    }
    
    function insertBrandPreferences($brands,$revno){
    	$c_time=time();
    	foreach($brands as $insertBrand){
			$values=array(
				'login'=>$this->login,
				'brand_id'=>$insertBrand,
				'created_on'=>$c_time
				);
			$insert_result = func_array2insertWithTypeCheck('user_brand_preferences', $values);
			//insert into histroy table - 0(insert)
			$values['id']=$insert_result;
			$values['revision_number']=$revno;
			$values['revision_type']=1;
			$insertHistory=$this->insertBrandPreferenceHistory($values);
		}
    }
    
	function deleteBrandPreferences($brands,$revno){
  		$c_time=time();
		foreach($brands as $delBrand){
			//insert into history table - 1(delete)
			$sql="SELECT id from user_brand_preferences WHERE login='$this->login' and brand_id=$delBrand" ;
			$delId=func_query_first_cell($sql);
			if(!empty($delId)) {
				$values=array(
					'id'=>$delId,
					'login'=>$this->login,
					'brand_id'=>$delBrand,
					'created_on'=>$c_time,
					'revision_number'=>$revno,
					'revision_type'=> 0//delete
				);
				$insertHistory=$this->insertBrandPreferenceHistory($values);
				//delete from preference table
				$delsql = "DELETE from user_brand_preferences where login = '$this->login' and brand_id = $delBrand";
           		$delete_result = db_query($delsql);
			}
        }
  
    }
    
    function insertBrandPreferenceHistory($values){
     	$insert_result = func_array2insertWithTypeCheck('user_brand_preferences_history', $values);
     	return $insert_result;
    
    }
    
    
    
  function insertInterests($interests,$revno){
  	   	$c_time=time();
		foreach($interests as $insertInterest){
			$values=array(
				'login'=>$this->login,
				'interest_id'=>$insertInterest,
				'created_on'=>$c_time
				);
			$insert_result = func_array2insertWithTypeCheck('user_interests', $values);
		//insert into histroy table - 0(insert)
			$values['id']=$insert_result;
			$values['revision_number']=$revno;
			$values['revision_type']=1;
			$insertHistory=$this->insertInterestsHistory($values);
		}
    }
    
 	function deleteInterests($interest,$revno){
  		$c_time=time();
		foreach($interest as $delInterest){
			//insert into history table - 1(delete)
			$sql="SELECT id from user_interests WHERE login='$this->login' and interest_id=$delInterest" ;
			$delId=func_query_first_cell($sql);
			if(!empty($delId)) {
				$values=array(
					'id'=>$delId,
					'login'=>$this->login,
					'interest_id'=>$delInterest,
					'created_on'=>$c_time,
					'revision_number'=>$revno,
					'revision_type'=> 0//delete
				);
				$insertHistory=$this->insertInterestsHistory($values);
				//delete from preference table
				$delsql = "DELETE from user_interests where login = '$this->login' and interest_id = $delInterest";
           		$delete_result = db_query($delsql);
			}
        }
  
    }
    
    function insertInterestsHistory($values){
     	$insert_result = func_array2insertWithTypeCheck('user_interests_history', $values);
     	return $insert_result;
    
    }
    
    function getUserBrandPreferences(){
		$query="select brand_id from user_brand_preferences where login='$this->login'";
		$result=func_query_column($query,'brand_id');
 	 	return $result;
 	}
 	
 	function getUserInterests(){
		$query="select interest_id from user_interests where login='$this->login'";
		$result=func_query_column($query,'interest_id');
 	 	return $result;
 	}
 	
 	function addBrandsToCache(){
 		global $xcache ;
 		$brandsResults = $xcache->fetchAndStore(function(){
		$resultArray=db_query("select id, attribute_value from mk_attribute_type_values where attribute_type='Brand' order by attribute_value",true);
		$finalResult=array();
		while($result = db_fetch_array($resultArray)){
			$finalResult[$result['id']] = array($result['attribute_value']);
			
		}
		return $finalResult;},
		array(),
		'brands_list_array');
		return $brandsResults;
 	}
 	
 	function addInterestsToCache(){
 		global $xcache ;
 		$interestsResults = $xcache->fetchAndStore(function(){
			$resultArray=db_query("select id, interest_name from interests_list order by interest_name",true);
			$finalResult=array();
			while($result = db_fetch_array($resultArray)){
				$finalResult[$result['id']] = array($result['interest_name']);
			}
			return $finalResult;},
			array(),
			'interest_list_array');
		return 	$interestsResults;
 	}
 	
}
