<?php
namespace saveditems\action;
use base\dataobject\validator\ITokenValidator;
use base\dataobject\validator\DefaultTokenValidator;
use base\action\BaseTokenValidatedAction;
use enums\cart\CartContext;
use saveditems\action\SavedItemAction;

require_once \HostConfig::$documentRoot . "/include/class/mcart/class.MCartUtils.php";

/**
 * This action will be used as a base of all kinds of Modifications related to Saved Items: 
 * including delete, moving from cart to saved item, moving from saved item to cart.
 *  
 * @author kundan
 *
 */
class SavedItemBaseModificationAction extends SavedItemAction implements ITokenValidator {

	/**
	 * This is the item-id that will be set fr	
	 */
	protected $itemId;

	public function validate() {
		$validator = new DefaultTokenValidator();
		$validator->validate();
	}
	
	public function __construct() {
		parent::__construct();
		$this->validate();
		$this->initForModification();
	}
	
	protected function initForModification() {
		$this->setItemId($this->getRequestVar("itemId"));
	}
	
	public function save() {
		\MCartUtils::persistCart($this->savedItems);
	}
	
	public function saveCart() {
		\MCartUtils::persistCart($this->cartItems);
	}
	/**
	 * This is the item id (cart-specific) which will be passed from (usually) Form Post Parameters
	 * Its meaning will be dependent on the specific action to be performed
	 */
	public function setItemId($itemId) {
		$this->itemId = $itemId;
	}
	
	public function getItemId() {
		return $this->getItemId();
	}
	
	public function getSavedItem() {
		return $this->savedItems->getProduct($this->itemId);
	}
	
	public function getCartItem() {
		return $this->cartItems->getProduct($this->itemId);	
	}
}