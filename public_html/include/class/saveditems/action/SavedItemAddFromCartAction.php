<?php
namespace saveditems\action;
use mcart\exception\ProductAlreadyInCartException;
use saveditems\action\SavedItemBaseModificationAction;

class SavedItemAddFromCartAction extends SavedItemBaseModificationAction {

	private $removeFromCart;
	public function __construct() {
		parent::__construct();
		$this->initForModification();
		$this->removeFromCart = true;		
	}
	/**
	 * Set whether we need to remove from cart when we add to saved from cart
	 * @param unknown_type $removeFromCart
	 */
	public function setRemoveFromCartFlag($removeFromCart) {
		$this->removeFromCart = $removeFromCart;
	}
	
	public function run() {
		$itemToRemove = $this->getCartItem();
		if(!isset($itemToRemove)) {
			return "item no longer in cart";
		}
		//Add this item to saved
		$sku = $itemToRemove->getSKUArray();
		$skuIdOfItemToAdd = $sku['id'];
		$itemToAdd = $this->getItemBySkuId($skuIdOfItemToAdd, $itemToRemove->getQuantity());
		try {
			$this->savedItems->addProduct($itemToAdd, false);
			$this->save();
			if($this->removeFromCart) {
				//Remove this item from cart
				$this->cartItems->removeItem($itemToRemove);
				$this->saveCart();
			}
			return "saved";
		} catch (ProductAlreadyInCartException $prodAlreadyInCartEx) {
			$product = $prodAlreadyInCartEx->getProduct();
			$skuId = $product->getSKUArray();
			$skuId = $skuId['id'];
			$qty = $product->getQuantity();
			if ($this->removeFromCart) {
				//Remove this item from cart
				$this->cartItems->removeItem($itemToRemove);
				$this->saveCart();
			}
			return "already-in-saved";
		}
	}	
}