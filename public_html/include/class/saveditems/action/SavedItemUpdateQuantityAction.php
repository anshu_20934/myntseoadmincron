<?php
namespace saveditems\action;

use saveditems\action\SavedItemBaseModificationAction;

/**
 * Update quantity of saved item
 * @author kundan
 */
class SavedItemUpdateQuantityAction extends SavedItemBaseModificationAction {

	public function __construct() {
		parent::__construct();
	}
	
	public function initForModification() {
		parent::initForModification();
		$this->setQuantity($this->getRequestVar($this->itemId."qty"));
	}
	public function run() {
		$savedItem = $this->savedItems->getProduct($this->itemId);
		$this->savedItems->updateItemQuantity($savedItem, $this->quantity);
		$this->save();
	}
}