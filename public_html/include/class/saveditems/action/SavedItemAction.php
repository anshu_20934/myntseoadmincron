<?php
namespace saveditems\action;
use base\action\BaseAction;
use enums\cart\CartContext;
use saveditems\dataobject\SavedItem;

require_once(\HostConfig::$documentRoot."/include/class/mcart/class.MCart.php");
require_once(\HostConfig::$documentRoot."/include/class/mcart/class.MCartFactory.php");
require_once(\HostConfig::$documentRoot."/modules/discount/DiscountEngine.php");

class SavedItemAction extends BaseAction {

	protected $savedItems, $cartItems;
	protected $quantity;
	
	public function __construct() {
		parent::__construct();
		$mcartFactory= new \MCartFactory();
		$this->savedItems = $mcartFactory->getCurrentUserCart(true, CartContext::SavedContext);
		$this->cartItems = $mcartFactory->getCurrentUserCart(true, CartContext::DefaultContext);
	}

	
	public function getQuantity() {
		return $this->quantity;
	}
	
	public function setQuantity($quantity) {
		$this->quantity = $quantity;
	}
	
	protected function getItemBySkuId($productSkuId, $quantity = 1) {
		return new SavedItem(\MCartUtils::genProductId(), $productSkuId, $quantity);
	}
	
	/**
	 * Get the total number of items in Saved
	 */
	public function getSavedItemCount() {
		return $this->savedItems->getItemQuantity();
	}
	
	/**
	 *  Get the total number of items in Cart
	 */
	public function getCartItemCount() {
		return $this->cartItems->getItemQuantity();
	}
	
	/**
	* Get array of styles in Saved
	*/
	public function getStyleIdsInSaved() {
		return $this->savedItems->getStyleIds();
	}
	
	/**
	 * Get array of styles in cart
	 */
	public function getStyleIdsInCart() {
		return $this->cartItems->getStyleIds();
	}
	
	/**
	 * Get products in Cart
	 */
	public function getProductsInCart() {
		return $this->cartItems->getProducts();
	}
	
	/**
	* Get product ids in Cart
	*/
	public function getProductsIdsInCart() {
		return $this->cartItems->getProductIdsInCart();
	}
	
	/**
	* Get product ids in Cart
	*/
	public function getProductsIdsInSaved() {
		return $this->savedItems->getProductIdsInCart();
	}
	
	/**
	 * Get cart items availability array 
	 */
	public function getCartItemsAvailabilityArray() {
		return $this->cartItems->getItemsAvailabilityArray();
	}
	
	/**
	* Get saved items availability array
	*/
	public function getSavedItemsAvailabilityArray() {
		return $this->savedItems->getItemsAvailabilityArray();
	}
	/**
	 * Get products in Saved
	 */
	public function getProductsInSaved() {
		return $this->savedItems->getProducts();
	}
	
	/**
	 * @return \MCart 
	 */
	public function getSaved() {
		return $this->savedItems;
	}
	
	public function getCart() {
		return $this->cartItems;
	}
	
	public function getCartAmountToDisplay() {
		$mcart = $this->cartItems;
		if($mcart == null) 
			return 0;
		return $mcart->getMrp()+$mcart->getAdditionalCharges()-$mcart->getDiscount()-$mcart->getCashDiscount()-$mcart->getDiscountAmount()+$mcart->getShippingCharge();
	}
	
	public function isCartNull() {
		return $this->cartItems == null;
	}
	
	public function isSavedNull() {
		return $this->savedItems == null;
	}
}
