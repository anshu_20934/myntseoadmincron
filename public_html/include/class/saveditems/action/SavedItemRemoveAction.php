<?php
namespace saveditems\action;
use saveditems\action\SavedItemBaseModificationAction;

class SavedItemRemoveAction extends SavedItemBaseModificationAction {

	public function __construct() {
		parent::__construct();
	}
	
	public function run() {
		$itemToRemove = $this->getSavedItem();
		$this->savedItems->removeItem($itemToRemove);
		$this->save();
	}	
}