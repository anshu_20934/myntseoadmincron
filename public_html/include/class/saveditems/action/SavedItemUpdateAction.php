<?php
namespace saveditems\action;
use enums\base\ActionType;

use saveditems\action\SavedItemAddAction;
use saveditems\action\SavedItemBaseModificationAction;
use saveditems\dataobject\SavedItem;
/**
 * Update quantity of saved item
 * @author kundan
 */
class SavedItemUpdateAction extends SavedItemBaseModificationAction {

	protected $skuId;
	public function __construct() {
		parent::__construct();
	}
	
	public function initForModification() {
		parent::initForModification();
	}
	
	public function run() {
		//remove the item id from cart..
		$savedItem = $this->savedItems->getProduct($this->itemId);
		$this->savedItems->removeItem($savedItem);
		//add product in cart
		$savedItemForAddition = $this->getItemBySkuId($this->skuId, $this->quantity);
		$this->savedItems->addProduct($savedItemForAddition,true);
		//save the saved-items
		$this->save();
	}

	protected function getItemBySkuId($productSkuId, $quantity = 1) {
		return new SavedItem($this->itemId ==null ?\MCartUtils::genProductId():$this->itemId, $productSkuId, $quantity);
	}
	
	public function getSkuId() {
	    return $this->skuId;
	}

	public function setSkuId($skuId) {
	    $this->skuId = $skuId;
	}
}