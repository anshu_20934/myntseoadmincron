<?php
namespace saveditems\action;
use mcart\exception\ProductAlreadyInCartException;

use saveditems\action\SavedItemBaseModificationAction;

class SavedItemMoveToCartAction extends SavedItemBaseModificationAction {

	public function __construct() {
		parent::__construct();
	}
	
	public function run() {
		$itemToRemove = $this->getSavedItem();
		if(!isset($itemToRemove)) {
			return "item no longer in cart";
		}
		//Add this item to cart
		$sku = $itemToRemove->getSKUArray();
		$skuIdOfItemToAdd = $sku['id'];
		$itemToAdd = $this->getItemBySkuId($skuIdOfItemToAdd, $itemToRemove->getQuantity());
		try {
			$this->cartItems->addProduct($itemToAdd, false);
			$this->saveCart();
			//Remove this item from saved
			$this->savedItems->removeItem($itemToRemove);
			$this->save();
			return "moved-from-saved-to-cart";
		} catch (ProductAlreadyInCartException $prodAlreadyInCartEx) {
			$product = $prodAlreadyInCartEx->getProduct();
			$skuId = $product->getSKUArray();
			$skuId = $skuId['id'];
			$qty = $product->getQuantity();
			//Remove this item from saved
			$this->savedItems->removeItem($itemToRemove);
			$this->save();
			return "already-in-cart";
		}
	}	
}