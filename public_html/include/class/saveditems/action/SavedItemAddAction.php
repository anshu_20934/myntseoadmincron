<?php
namespace saveditems\action;
use mcart\exception\ProductAlreadyInCartException;
include_once \HostConfig::$documentRoot . "/include/class/instrumentation/BaseTracker.php";
include_once \HostConfig::$documentRoot . "/include/class/search/UserInterestCalculator.php";

/**
 * Adding an item to Saved or Cart uses Product's SKU ID and Quantity array
 * @author kundan
 */
class SavedItemAddAction extends SavedItemBaseModificationAction {

	protected $productSkuId, $selectedQuantity, $savedItemForAddition , $productStyleId;
	
	public function __construct() {
		parent::__construct();
		$this->initForAddition();		
	}
	
	protected function initForAddition() {
		$this->setProductSkuId();
		$this->setSelectedQuantity();
		$this->setItemForAddition();
	}
	
	protected function setProductSkuId() {
		
		$this->productSkuId =(INT) $_POST["productSKUID"];
		$this->productStyleId =(INT) $_POST["productStyleId"];
	}
	
	protected function setSelectedQuantity() {
		$sizequantityArray = $_POST['sizequantity'];
		for($i=0; $i<count($sizequantityArray); $i++){
			if(!empty($sizequantityArray[$i])){
				$this->selectedQuantity = $sizequantityArray[$i];
				break;
			}
		}
		$this->selectedQuantity = mysql_real_escape_string($this->selectedQuantity);
		$this->selectedQuantity = empty($this->selectedQuantity) ? 1 : $this->selectedQuantity;
	}
	
	protected function setItemForAddition() {

		$skuid = $this->productSkuId;
		if(empty($skuid)){
			$this->productSkuId = \MCartNPItem::getDefaultSkuID($this->productStyleId);
		}
		$this->savedItemForAddition = $this->getItemBySkuId($this->productSkuId, $this->selectedQuantity);
		if(empty($skuid)){
			$this->savedItemForAddition->isSizeAutoSelected = true;
		}
	}
	
	public function run() {
		try {
			$this->savedItems->addProduct($this->savedItemForAddition, false);
			$this->doTrack();
			$this->save();
			return "saved";
		} catch (ProductAlreadyInCartException $prodAlreadyInCartEx) {
			$product = $prodAlreadyInCartEx->getProduct();
			$sku = $product->getSKUArray();
			$skuId = $sku["id"];
			$qty = $product->getQuantity();
			return "notsaved:already exists as SKU ID $skuId with qty: $qty";
			//TODO: redirect with error/warning: that the item already exists
			//TODO: set $skuId and $qty in Smarty so as to show it in UI
		}
	}

	protected function doTrack() {
		$tracker = new \BaseTracker(\BaseTracker::ADD_TO_SAVED);
		$productStyleId = $this->savedItemForAddition->getStyleId();
		
		if(!empty($productStyleId)) {
			\UserInterestCalculator::trackAction($productStyleId, \UserInterestCalculator::ADD_TO_SAVED);
		}
		if(!is_null($tracker->addToSavedData)) {
			$tracker->addToSavedData->setSavedItem(\MCartUtils::convertCartItemToArray($this->savedItemForAddition));
		}
		$tracker->fireRequest();
	}
}