<?php
namespace saveditems\action;
use enums\base\ActionType;

use saveditems\action\SavedItemAddAction;
use saveditems\action\SavedItemBaseModificationAction;
/**
 * Update quantity of saved item
 * @author kundan
 */
class SavedItemUpdateSkuAction extends SavedItemBaseModificationAction {

	protected $skuId;
	public function __construct() {
		parent::__construct();
	}
	
	public function initForModification() {
		parent::initForModification();
		$this->setQuantity($this->getRequestVar($this->itemId."qty"));
		$this->setSkuId($this->getRequestVar($this->itemId."size"));
		
	}
	
	public function run() {
		//add product in cart
		$savedItemForAddition = $this->getItemBySkuId($this->skuId, $this->quantity);
		$this->savedItems->addProduct($savedItemForAddition,true);
		
		//remove the item id from cart..
		$savedItem = $this->savedItems->getProduct($this->itemId);
		$this->savedItems->removeItem($savedItem);
		
		//save the saved-items
		$this->save();
	}

	public function getSkuId() {
	    return $this->skuId;
	}

	public function setSkuId($skuId) {
	    $this->skuId = $skuId;
	}
}