<?php

class readini {
	Function ReadIniValue ($IniFile, $IniKey, $IniVar) {
		$Ini_Key = "[".strtolower($IniKey)."]";
		$Ini_Variable = strtolower($IniVar);
	
		$Ini_File = file($IniFile);
	
		unset($Ini_Value);
		
		for($Ini_Rec=0; $Ini_Rec<sizeof($Ini_File); $Ini_Rec++) {
			$Ini_Temp = trim($Ini_File[$Ini_Rec]); // Save the original var value to return it as result
			$Ini_Tmp = strtolower($Ini_Temp);
		
			if ( substr_count($Ini_Tmp, "[") > 0 ) $Ini_Ready = 0;	
	
			if ( $Ini_Tmp == $Ini_Key ) $Ini_Ready = 1;
			
	
	        If ( (substr_count($Ini_Tmp, "[") == 0) && ($Ini_Ready == 1) ) {
				if (substr_count($Ini_Tmp, $Ini_Variable . "=") > 0) {
			  	     $Ini_Value = substr($Ini_Temp, strlen($Ini_Variable . "="));
				     return $Ini_Value;
				}
			}
		} // end for
	
		if ( !$Ini_Value ) {
			return "ERROR .: Key: [<b>".strtoupper($IniKey)."</b>] or Variable: <b>".strtoupper($IniVar)."</b>, does not exist in <b>".strtoupper($IniFile)."</b> file !"; // Key or Variable NOT FOUND in INI file
		}
	
	} // end function

} // end class

?>
