<?php
namespace base\builder;

interface BaseBuilder {
	public function build();
}