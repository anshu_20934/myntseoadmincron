<?php
namespace base\action;
require_once \HostConfig::$documentRoot . "/include/func/func.utilities.php";

class BaseAuthenticatedAuthorizedAction extends BaseAction {
	public function __construct() {
		parent::__construct();
		$requestedURL =  $this->getRequestedURL();
		$segments = explode("/", $requestedURL);
		if($segments[0] === "admin") {
			require_once \HostConfig::$documentRoot . "/include/security.php";
		}
	}
}