<?php
namespace base\action;
require_once \HostConfig::$documentRoot."/include/func/func.utilities.php";

class BaseAction {
	
	public function __construct() {
		
	}
	
	public function getLogin() {
		global $XCART_SESSION_VARS;
		return $XCART_SESSION_VARS["login"];
	}
	
	/**
	 * For each of the POST & GET Parameters: 
	 * for them if there is a setter method, call them
	 * if the parameter name is myVar, then the method-name searched for is setMyVar
	 */
	protected function callSetters() {
		
	}
	
	public function getSessionId() {
		global $XCARTSESSID;
		return $XCARTSESSID;
	}
	
	public function getRequestVar($varName, $defaultValue = "", $escapeMode ="none") {
		return \RequestVars::getVar($varName, $defaultValue, $escapeMode);
	}
	
	public function getSessionVar($varName, $defaultValue = null) {
		return \XSessionVars::get($varName, $defaultValue);
	}
	
	public function getUploadedFileInRequest($fileName) {
		global $HTTP_POST_FILES;
		return $HTTP_POST_FILES[$fileName];
	}
	/**
	 * This returns the requested URL without query parameters.
	 * if the URL is http://a.b.com/context/1.php?a=b&c=d
	 * then this would return: /context/a.php
	 */
	public function getRequestedURL() {
		global $_SERVER;
		return $_SERVER["PHP_SELF"];
	}
}