<?php
namespace base\action;
use base\exception\TokenInvalidException;

class BaseTokenValidatedAction extends BaseAuthenticatedAuthorizedAction {
	
	private $tokenInRequest, $tokenInSession;
	public function init() {
		$this->tokenInRequest = $this->getRequestVar("_token");
		$this->tokenInSession = $this->getSessionVar("USER_TOKEN");
	}
	
	public function __construct() {
		$this->init();
		if($this->tokenInRequest!== $this->tokenInSession) {
			throw new TokenInvalidException("", $this->tokenInRequest, $this->tokenInSession);
		}
	}
}