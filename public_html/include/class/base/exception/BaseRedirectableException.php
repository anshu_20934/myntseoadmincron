<?php
namespace base\exception;
require_once \HostConfig::$documentRoot . "/exception/MyntraException.php";
class BaseRedirectableException extends \MyntraException {
	/**
	 * This variable stores the URL to redirect to in case of exception 
	 * @var unknown_type
	 */
	private $redirectUrl;
	
	function __construct($message, $redirectUrl) {
		parent::__construct($message);
		$this->redirectUrl = $redirectUrl;
	}
	public function getRedirectUrl() {
		return $this->redirectUrl;
	}
}