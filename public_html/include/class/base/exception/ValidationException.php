<?php
/**
 * Exception to be thrown when any vallidation exception is encountered 
 * @author kundan
 *
 */
namespace base\exception;

require_once \HostConfig::$documentRoot."/exception/MyntraException.php";

class ValidationException extends \MyntraException {
	
	private $dataObjectName, $erroneousField;
	/**
	 * @param unknown_type $message
	 * @param unknown_type $dataObjectName: the data-object whose validation failed
	 * @param unknown_type $erroneousField: Erroneous field could be a field-name as string 
	 * or an array of fields where each field is of type string
	 */
	public function __construct($message, $dataObjectName="", $erroneousField="") {

		$this->dataObjectName = $dataObjectName;
		$this->erroneousField = $erroneousField;
		
		$excMsg = "ValidationException: ";
		if(!empty($dataObjectName)) {
			$excMsg .= "in data-object-name: ".$dataObjectName." ";
		}
		if(!empty($erroneousField)) {
			if(is_array($erroneousField)) {
				$excMsg .= "in Fields: ".implode(", ", $erroneousField). " ";
			} else {
				$excMsg .= "in Field: ".$erroneousField. " ";				
			}
		}
		$excMsg .= " : ".$message;
		parent::__construct($excMsg);	
	}
	
	/**
	 * The DataObject name whose validation failed  	 
	 */
	public function getDataObjectName() {
		return $this->dataObjectName;
	}
	
	/**
	 * Erroneous field could be a field-name as string 
	 * or an array of fields where each field is of type string
	 */
	public function getErroneousField() {
		return $this->erroneousField;
	}
}