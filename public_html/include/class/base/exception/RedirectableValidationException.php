<?php
namespace base\exception;

class RedirectableValidationException extends ValidationException {
private $redirectUrl;
	
	function __construct($message, $dataObjectName="", $erroneousField="", $redirectUrl="") {
		parent::__construct($message, $dataObjectName, $erroneousField);
		$this->redirectUrl = $redirectUrl;
	}
	public function getRedirectUrl() {
		return $this->redirectUrl;
	}
}