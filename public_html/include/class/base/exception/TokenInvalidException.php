<?php
namespace base\exception;

class TokenInvalidException extends BaseRedirectableException {
	public function __construct($msg, $tokenInSession, $tokenInRequest, $redirectUrl) {
		$msg = "TokenInvalidException: token-in-session: $tokenInSession while token-in-request: $tokenInRequest ; ". $msg;
		parent::__construct($msg, $redirectUrl);
	}	
}