<?php
namespace base\dataobject;

use base\dataobject\IBaseOrderDO;

class BaseDO implements IBaseOrderDO {
	public function __construct() {
		
	}

	public function toString() {
		ob_start();
		var_dump($this);
		$doDump = ob_get_contents();
		ob_end_clean();
		$className = get_class();
		return $className.": ".$doDump;
	} 
}