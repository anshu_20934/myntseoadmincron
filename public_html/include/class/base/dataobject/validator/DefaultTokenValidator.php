<?php
namespace base\dataobject\validator;
use base\exception\TokenInvalidException;
use base\dataobject\validator\ITokenValidator;

class DefaultTokenValidator implements ITokenValidator {
	
	public function validate() {
		$tokenInSession = \XSessionVars::getString("USER_TOKEN", null);
		$tokenToValidate = \RequestVars::getVar("_token", null);
		if(empty($tokenInSession) ||empty($tokenToValidate) || $tokenToValidate!== $tokenInSession) {
			throw new TokenInvalidException("", $tokenInSession, $tokenToValidate);
		}
	}
}