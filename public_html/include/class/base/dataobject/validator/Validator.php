<?php

namespace base\dataobject\validator;
/**
 * Validators which want to validate DataObjects should implement this interface
 * and implement this single method: validate
 * $dataObject to be validated must be passed in constructor to class implementing Validator
 * @author kundan
 */
interface Validator {
	/**
	 * This single method should validate the $dataObject
	 * This should not return anything; instead throw ValidationException or RedirectableValidationException
	 */
	public function validate();
}