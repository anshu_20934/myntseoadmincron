<?php

namespace base\dao;

class BaseDAO {
	
	public function getRAWData($sql,$singleRow=false,$slaveDB=false){
		if($singleRow==true){
			$result=func_query_first($sql,$slaveDB);
		}else{
			$result=func_query($sql,$slaveDB);
		}
		return $result;
	}
	
	/**
	 * Function generates select query and run
	 * @param unknown_type $tbl
	 * @param unknown_type $columns
	 * @param unknown_type $filters
	 * @param unknown_type $start
	 * @param unknown_type $count
	 * @param unknown_type $orders
	 * @param unknown_type $slaveDB
	 */
	public function selectColumnsQuery($tbl, $columns = "*", $filters = NULL, $start = NULL, $count = 10, $orders = NULL, $slaveDB=false) {
		if (is_array($columns)) {
			$columns=array_map("mysql_real_escape_string", $columns);
			$columns = implode("`, `", $columns);
			$columns = "`$columns`";
		}
		$sql = "SELECT $columns FROM ".mysql_real_escape_string($tbl);
		if(is_array($filters)){
			$sql .=' WHERE ';
			$index = 0 ;
			foreach($filters as $key => $value){
				if($index > 0 ){
					$sql.= ' AND ';
				}
				$sql .= "`".mysql_real_escape_string($key)."`".'="'. mysql_real_escape_string($value).'"';
				$index++;
			}
		}
	
	
		if(is_array($orders)){
			$sql .=' ORDER BY ';
			$index = 0 ;
			foreach($orders as $key => $value){
				if($index > 0 ){
					$sql.= ' , ';
				}
				$sql .=  "`".mysql_real_escape_string($key)."`".' '. mysql_real_escape_string($value);
				$index++;
			}
		}
	
		if(isset($start) && $start >= 0) {
			$start =(int)$start;
			$count = (int) $count;
			$sql.=' LIMIT '.$start.','.$count;
		}
		return $this->getRAWData($sql,false,$slaveDB);
	}
}

?>