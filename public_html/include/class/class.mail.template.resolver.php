<?php

/**
 * Helper class to build mail-details from template
 * @author kundan
 *
 */
class EmailTemplateResolver {
	
	private $templateName;
	private $subject;
	private $rawSubject;
	private $body;
	private $rawBody;
	private $fromName;
	private $fromDesignation;
	
	private $mailDetails;
	
	public function __construct($templateName, $customKeywordValues = Array(), $bodyHeader='', $bodyFooter='') {
		$this->templateName = $templateName;
		$row = self::getTemplate($this->templateName);
		if(empty($row)) {
			global $errorlog;
			$errorlog->error(__FILE__.__FUNCTION__.__LINE__.":No template with name: ".$templateName." found..");
			throw new MyntraException("Failed to populate Mail Details from Template.");
		}

		$this->rawSubject = $row['subject'];
		$this->subject = self::replaceKeywordsWithValues($this->rawSubject, $customKeywordValues);
		$this->rawBody = $row['body'];
		$this->body = self::replaceKeywordsWithValues($this->rawBody, $customKeywordValues);
		$this->from_name = $row['from_name'];
		$this->from_designation = $row['from_designation'];
		
		$this->mailDetails = Array('subject' => $this->subject, 'from_name'=>$this->fromName, 'from_designation'=>$this->fromDesignation);
		
		if(!empty($bodyHeader)) {
			$hdrTemplate = self::getTemplate($bodyHeader);
			$header = self::replaceKeywordsWithValues($hdrTemplate['body']);
		}
		if(!empty($bodyFooter)) {
			$ftrTemplate = self::getTemplate($bodyFooter);
			$footer = self::replaceKeywordsWithValues($ftrTemplate['body']);
		}
		$this->mailDetails['content'] = $header.$this->body.$footer;
	}
	
	public function getRawSubject() {
		return $this->rawSubject;
	}
	
	public function getSubject() {
		return $this->subject;
	}
	
	public function getRawBody() {
		return $this->rawBody;
	}
	
	public function getBody() {
		return $this->body;
	}
	
	public function getFromName() {
		return $this->fromName;
	}
	
	public function getFromDesignation() {
		return $this->fromDesignation;
	}
	
	public function getMailDetails() {
		return $this->mailDetails;
	}
	
	/**
	 * 
	 * Replace keywords with values: both common keywords and custom keywords are supported
	 * @param $input
	 * @param $customKeywordValues this should be an associative array with keyword as key: should be without []. These will be replaced inside the input along with reqular definded keywords
	 * e.g. $customKeywordValues = Array("CustomerName"=>"Kundan Burnwal","Designation"=>"Senior Software Engineer");
	 * Note that it doesn't do in-place replacement. Instead the replaced string is returned
	 */
	public static function replaceKeywordsWithValues($input,$customKeywordValues) {
		global $cashback_gateway_status;
		$callNumber = WidgetKeyValuePairs::getWidgetValueForKey('customerSupportCall');
		$callPeriod = WidgetKeyValuePairs::getWidgetValueForKey('customerSupportTime');
		$shippingPeriod = WidgetKeyValuePairs::getWidgetValueForKey('shippingperiod');
		$cashbackMessage = ($cashback_gateway_status == "on")? "10% CASHBACK": "";
		
		$keywordVsValues = Array();
		$keywordVsValues['[MYNTRA_PHONE]'] = $callNumber;
		$keywordVsValues['[CASHBACK_MESSAGE]'] = $cashbackMessage;
		$keywordVsValues['[MYNTRA_CC_PHONE]'] = $callNumber.' ('.$callPeriod.')';
		$keywordVsValues['[SHIPPING_PERIOD]'] = $shippingPeriod;
		
		if(!empty($customKeywordValues)){
			foreach ($customKeywordValues as $keyword => $value) {
				$keywordVsValues["[$keyword]"] = $value;
			}
		}
		$output = $input;
		foreach ($keywordVsValues as $keyword=>$value){
			$output = str_replace($keyword, $value, $output);	
		}
		return $output;
	}
	
	/**
	 * Get an associative array containing subject, body, from_name, from_designation, given name of an email template
	 */
	public static function getTemplate($template){
		global $sql_tbl;
		$tb_email_notification  = $sql_tbl["mk_email_notification"];
	    $template_query = "SELECT * FROM $tb_email_notification WHERE name = '$template'";
		$template_result = db_query($template_query,true);
		$row = db_fetch_array($template_result);
		return $row;
	}	
}

?>