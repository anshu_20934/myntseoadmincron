<?php
/**
 * These are set of Utility classes for doing various things with Elabs: these are functions which would be used in ALL iCubes/ELabs APIs
 * e.g. they're used in Mailing List Creation, CSV upload, Attribute creation etc
 * @author kundan
 *
 */
class ElabsUtils {
	//these are default values
	/* const SITE_ID = '2010001582';
	const PASSWORD = 'm1ntraFtp';
	const SERVER_FARM = 'https://www.elabs11.com/API/mailing_list.html';
	 */
	private $siteId;
	private $apiPassword;
	private $serverFarm;
	
	private static $initialized = false;
	
	public function __construct() {
		global $xcart_dir;
		$configuration = parse_ini_file("$xcart_dir/env/serverinfo.ini", TRUE);
		$elabsConfig = $configuration["ELABS"];
		$this->siteId = $elabsConfig["site_id"]; 
		$this->apiPassword = $elabsConfig["api_password"];
		$this->serverFarm = $elabsConfig["server_farm"];
		//echo "siteid: ".$this->siteId. " api-passwd:".$this->apiPassword." serverFarm:".$this->serverFarm;
	}
	
	public function getResponse($type, $activity, $input) {
		$url = $this->getUrl($type, $activity, $input);
		//echo "\nfinal url:".$url;
		return file_get_contents($url);
	}
	
	public function getUrl($type, $activity, $input) {
		return $this->serverFarm.'?type='.$type.'&activity='.$activity.'&input='.urlencode($input);
	}
	
	public function surroundInputWithGlobalParams($input) {
		return 
		 '<DATASET>'.
		 self::getInputXmlLine("SITE_ID", $this->siteId).
		 self::getInputXmlDataLine("extra", $this->apiPassword, "password").
		 $input.
		 '</DATASET>';
	}
	
	public static function getInputXmlLine($tag, $value) {
		return "<$tag>$value</$tag>";
	}
	
	public static function getInputXmlDataLine($type,$value,$id) {
		//if type = extra, then id is required. otherwise, its not
		if($type === 'extra' && empty($id)) {
			throw new Exception("Exception: getInputXmlDataLine():for type = extra, id must be provided", 0);
		}
		elseif ($type === 'extra' && !empty($id)) {
			return "<DATA type=\"$type\" id=\"$id\">".$value."</DATA>";
		}
		return "<DATA type=\"$type\">".$value."</DATA>";		
	} 

	/**
	 * @param $xmlResponse The Xml response received from API
	 * @param $fields The field(attributes) values (in DATA) which we want to retrieve from the response Xml
	 */
	public static function parseResponseXml($xmlResponse, $attributes) {
		$xml = simplexml_load_string($xmlResponse);
		$returnValue = Array();
				
		if(!empty($xmlResponse) && ($xml !== FALSE) ) {
			$returnValueArr = get_object_vars($xml);
			
			if($returnValueArr['TYPE'] === 'success') {
				if(empty($attributes)) {
					$returnValue = $returnValueArr;
				}
				else {
					$returnValue = self::getAttributes($xml, $attributes);
				}
			}
			elseif($returnValueArr['TYPE'] === 'error') {
				$returnValue['errmsg'] = $returnValueArr['DATA'];
			}
			$returnValue['result'] = $returnValueArr['TYPE'];
		}
		return $returnValue;			
	} 
	
	public static function getAttributes($simpleXmlElement, $attributes) {
		$returnVal = Array();
		foreach($simpleXmlElement->DATA as $eachXmlData) {		
			foreach($eachXmlData->attributes() as $attr => $attrval) {
				$attr = (string)$attr;
				$attrval = (string)$attrval;
				$value = (string)$eachXmlData;
				
				if(is_array($attributes)) {
					foreach ($attributes as $eachAttribute) {
						if($eachAttribute === $attrval) {
							$returnVal[$attrval] = $value;
						}	
					}
				}
				else {
					if($attributes === $attrval) {
						$returnVal[$attrval] = $value;
					}
				}
			}
		}
		return $returnVal;
	}
}
?>
