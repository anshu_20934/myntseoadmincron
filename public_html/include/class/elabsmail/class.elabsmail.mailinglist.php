<?php 

require_once dirname(__FILE__)."/class.elabsmail.utils.php";

class MailingList {
	
	public $mailingListId;
	
	private static $elabsUtilsHandle = null;
	
	private static function getElabsUtilshandle() {
		if(self::$elabsUtilsHandle == null) {
			self::$elabsUtilsHandle = new ElabsUtils();
		}
		return self::$elabsUtilsHandle;
	}
	
	private static function buildOptionalParams($input, $clickThruUrl, $replyFwdEmail, $replyFromEmail, $replyFromName, $replyFwdSubject, $footerText, $footerHtml) {
		if(!empty($clickThruUrl)) { 
			$input .= ElabsUtils::getInputXmlDataLine("extra", $clickThruUrl, "CLICKTHRU_URL"); 
		}
		if(!empty($replyFwdEmail)) {
			$input .= ElabsUtils::getInputXmlDataLine("extra", $replyFwdEmail, "REPLY_FORWARD_EMAIL");
		}
		if(!empty($replyFromEmail)) {
			$input .= ElabsUtils::getInputXmlDataLine("extra", $replyFromEmail, "REPLY_FROM_EMAIL");
		}
		if(!empty($replyFromName)) {
			$input .= ElabsUtils::getInputXmlDataLine("extra", $replyFromName, "REPLY_FROM_NAME");
		}
		if(!empty($replyFwdSubject)) {
			$input .= ElabsUtils::getInputXmlDataLine("extra", $replyFwdSubject, "REPLY_FORWARD_SUBJECT");
		}
		if(!empty($footerText)) {
			$input .= ElabsUtils::getInputXmlDataLine("extra", $footerText, "FOOTER_TEXT");
		}
		if(isset($footerHtml)) {		
			$input .= ElabsUtils::getInputXmlDataLine("extra", $footerHtml, "FOOTER_HTML");
		}
		return $input;
	}
	
	public function __construct($name, $clickThruUrl, $replyFwdEmail, $replyFromEmail, $replyFromName, $replyFwdSubject, $footerText, $footerHtml) {
		
		$elabsUtils = self::getElabsUtilshandle();
		
		$type = "list";
		$activity = "add";
		
		$input = ElabsUtils::getInputXmlDataLine("name", $name);
		$input = self::buildOptionalParams($input, $clickThruUrl, $replyFwdEmail, $replyFromEmail, 
					$replyFromName, $replyFwdSubject, $footerText, $footerHtml);
							
		$input = $elabsUtils->surroundInputWithGlobalParams($input);
		$response = $elabsUtils->getResponse($type, $activity, $input);
		$retval = $elabsUtils->parseResponseXml($response, 'mlid');
		
		if($retval['result'] === 'success') {
			$this->mailingListId = $retval['mlid'];
		}
		elseif($retval['result'] === 'error') {
			echo "Error Creating Mailing List: ".$retval['errmsg']."\n";
			throw new Exception("Failed to create mailing list");
		}
	}
	
	public function uploadContacts($pathToCsvFile, $emailToSendUploadStatus, $delimiter = ',', $update = 'on', 
													$active = 'active', $validateEmail = true, $untrash = "on") {

			self::uploadContactsToMailingList($this->mailingListId, $pathToCsvFile, $emailToSendUploadStatus, 
					$delimiter, $update, $active, $validateEmail, $untrash); 
	}
	
	public static function uploadContactsToMailingList($mailingListId, $pathToCsvFile, $emailToSendUploadStatus, $delimiter = ',', $applyTrigger='yes',
										$update = 'on', $active = 'active', $validateEmail = true, $untrash = "on", $deleteBlank='on') {
			
			$elabsUtils = self::getElabsUtilshandle();								
			$type = "record";  
			$activity = "upload";
			$input = ElabsUtils::getInputXmlLine("MLID", $mailingListId).
					 ElabsUtils::getInputXmlDataLine("email", $emailToSendUploadStatus).
					 ElabsUtils::getInputXmlDataLine("extra", $pathToCsvFile, "file").
					 ElabsUtils::getInputXmlDataLine("extra", $applyTrigger, "trigger").
					 ElabsUtils::getInputXmlDataLine("extra", $update, "update").
					 ElabsUtils::getInputXmlDataLine("extra", $deleteBlank, "delete_blank").
					 ElabsUtils::getInputXmlDataLine("extra", $active, "type").
					 /*ElabsUtils::getInputXmlDataLine("extra", $untrash, "untrash").*/
					 ElabsUtils::getInputXmlDataLine("extra", $delimiter, "delimiter");
					 
			$input = $elabsUtils->surroundInputWithGlobalParams($input);		 
			$response = $elabsUtils->getResponse($type, $activity, $input);
			$retVal = ElabsUtils::parseResponseXml($response);
			return $retVal;		  
	}
	
	public function getUploadStatus($filePath) {
		return self::getUploadStatusForMailingList($this->mailingListId, $filePath);
	}
	
	public static function getUploadStatusForMailingList($mailingListId, $filePath) {
		
		$elabsUtils = self::getElabsUtilshandle();
		
		$type = "record";  
		$activity = "upload-status";
		
		$input = ElabsUtils::getInputXmlLine("MLID", $mailingListId).
				 ElabsUtils::getInputXmlDataLine("extra", $filePath, "file");
				 
		$input = $elabsUtils->surroundInputWithGlobalParams($input);		 
		$response = $elabsUtils->getResponse($type, $activity, $input);
		$respObj = ElabsUtils::parseResponseXml($response);
		
		if(!empty($respObj['DATASET_1'])) {
			$retVal['status'] = (string)$respObj['DATASET_1']->STATUS;			
			$retVal['size'] = (double)$respObj['DATASET_1']->SIZE;
			$retVal['processed'] = (double)$respObj['DATASET_1']->PROCESSED;
			$retVal['file'] = (string)$respObj['DATASET_1']->FILE;
			$retVal['timeleft'] = (string)$respObj['DATASET_1']->TIMELEFT;
			$retVal['etc'] = (string)$respObj['DATASET_1']->ETC;
			return $retVal;	
		}
		return $respObj;
	}

	public function displayUploadProgress($filePath, $timeBetweenChecks = 1) {
		return self::displayUploadProgressForMailingList($this->mailingListId, $filePath, $timeBetweenChecks);
	}
	
	public static function displayUploadProgressForMailingList($mailingListId, $filePath, $timeBetweenChecks = 1) {
		sleep(1);
		$uploadStatus = self::getUploadStatusForMailingList($mailingListId, $filePath);
		echo "Displaying Upload/Import progress for: ".$uploadStatus['file']."\n";
		echo $uploadStatus['status']." ";
		ob_flush();
		$lastStatus = $uploadStatus['status'];
		while ($uploadStatus['status']!=='done' && $uploadStatus['status']!=='retry' && $uploadStatus['status']!=='fatal') {
			$uploadStatus = self::getUploadStatusForMailingList($mailingListId, $filePath);
			
			if($uploadStatus['status']!==$lastStatus) {
				echo $uploadStatus['status']." ";
				ob_flush();
			}
			$lastStatus = $uploadStatus['status'];
			sleep($timeBetweenChecks);	
		}
	}
	/*
	public function delete() {
		$this->deleteMailingList($this->mailingListId);
	}
	
	public static function deleteMailingList($mailingListId) {
		$elabsUtils = self::getElabsUtilshandle();
		$type = "list";
		$activity = "delete";
		$input = ElabsUtils::getInputXmlLine("MLID", $mailingListId);
		$input = $elabsUtils->surroundInputWithGlobalParams($input);
		$response = $elabsUtils->:getResponse($type, $activity, $input);
		$retval = ElabsUtils::parseResponseXml($response);
		
		if($retval['result'] === 'success') {
			$this = null;
			//$this->mailingListId = $retval['mlid'];
		}
		elseif($retval['result'] === 'error') {
			echo "Error Deleting Mailing List: ".$retval['errmsg']."\n";
			throw new Exception("Failed to Delete mailing list");
		}
	}
	*/
}

?>