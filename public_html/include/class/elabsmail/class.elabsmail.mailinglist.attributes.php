<?php

require_once dirname(__FILE__)."/class.elabsmail.utils.php";
/**
 * Every Mailing List can have Attributes.
 * As such, each Attribute is globally(across all mailing lists) unique. 
 * The same attribute can be applied to multiple mailing lists. 
 * In that case, the reference 
 * @author kundan
 *
 */
class MailingListAttribute {
	
	public $attributeId;
	public $enabled;
	public $type;
	public $name;
	public $mailingListId;

	private static $elabsUtils;
	public static function getElabsUtilsHandle() {
		if(empty(self::$elabsUtils))
		self::$elabsUtils = new ElabsUtils();
		return self::$elabsUtils;
	}
	
	public function __construct($name, $mailingListId, $attrType="text", $enabled="enabled") {
		$type = "demographic";
		$activity = "add";
		
		$elabsUtils = self::getElabsUtilsHandle();
		
		$input = ElabsUtils::getInputXmlLine("MLID", $mailingListId).
				 ElabsUtils::getInputXmlDataLine($attrType, $name).
				 ElabsUtils::getInputXmlDataLine("state", $enabled);
		$input = $elabsUtils->surroundInputWithGlobalParams($input);

		$response = $elabsUtils->getResponse($type, $activity, $input);
		$retval = ElabsUtils::parseResponseXml($response);
		if($retval['result'] === 'success') {
			$this->attributeId = $retval['DATA'];
			$this->name = $name;
			$this->enabled = $enabled;
			$this->type = $attrType;
			$this->mailingListId = $mailingListId;
		}
		elseif($retval['result'] === 'error') {
			echo "Error Creating Mailing List Attribute: ".$retval['errmsg']."\n";
			throw new Exception("Failed to create mailing list");
		}
	}
	
	public function enable() {
		if($this->enableAttributeForMailingList($this->attributeId, $this->mailingListId)) {
			$this->enabled = "enabled";;
			return true;
		}
		return false;
	}
	
	public static function enableAttributeForMailingList($attributeId, $mailingListId) {
		$type = "demographic";
		$activity = "enable";
		
		$elabsUtils = self::getElabsUtilsHandle();
		
		$input = ElabsUtils::getInputXmlLine("MLID", $mailingListId);
		if(is_array($attributeId)) {
			foreach ($attributeId as $eachAttrId) {
				$input .= ElabsUtils::getInputXmlDataLine("id", $eachAttrId);
			}
		} else {
			$input .= ElabsUtils::getInputXmlDataLine("id", $attributeId);
		}
		$input = $elabsUtils->surroundInputWithGlobalParams($input);

		$response = $elabsUtils->getResponse($type, $activity, $input);
		$retval = ElabsUtils::parseResponseXml($response);
		if($retval['result'] === 'success') {
			return true;	
		}
		elseif($retval['result'] === 'error') {
			echo "Error Enabling Mailing List Attribute: ".$retval['errmsg']."\n";
			throw new Exception("Failed to Enable Attribute");
		}
		
	}
	
	public function disable() {
		if($this->disableAttributeForMailingList($this->attributeId, $this->mailingListId)) {
			$this->enabled = "disabled";
			return true;
		}
		return false;
	}
	
	public static function disableAttributeForMailingList($attributeId, $mailingListId) {
		$type = "demographic";
		$activity = "disable";
		$elabsUtils = self::getElabsUtilsHandle();
		$input = ElabsUtils::getInputXmlLine("MLID", $mailingListId).
				 ElabsUtils::getInputXmlDataLine("id", $attributeId);
		$input = $elabsUtils->surroundInputWithGlobalParams($input);

		$response = $elabsUtils->getResponse($type, $activity, $input);
		$retval = ElabsUtils::parseResponseXml($response);
		if($retval['result'] === 'success') {
			return true;
		}
		elseif($retval['result'] === 'error') {
			echo "Error disabling Mailing List Attribute: ".$retval['errmsg']."\n";
			throw new Exception("Failed to disable attribute");
		}
	}
}
?>