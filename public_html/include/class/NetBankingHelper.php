<?php

use cache\InvalidateXCache;

class NetBankingHelper {
	
	const NET_BANKING_BANKS_CACHE = 'net_banking_banks';
	const NET_BANKING_BANKS_TEXT_CACHE = 'net_banking_banks_text';
	const NET_BANKING_GATEWAY_CACHE ='net_banking_gateways';
	
	public static function getNetBankingSelectBoxText() {
		global $xcache;
		if($xcache==null) {
			$xcache = new XCache();
		}
		$text = $xcache->fetch(self::NET_BANKING_BANKS_TEXT_CACHE);
		if($text == NULL) {
			$bank_list = self::getActiveBanksList();
			$isPopularAdded = false;
			$isnonPopularAdded =false;
			$text = "<option value=''>Select your bank</option>";
			foreach($bank_list as $key=>$banks) {
				$bank_name = $banks['name'];
				$bank_id = $key;
				$is_popular = $banks['is_popular'];
				if($is_popular) {
					if(!$isPopularAdded){
						$text .= "<optgroup label='Popular banks'>";
						$isPopularAdded = true; 
					}
					$text .= "<option value='$bank_id'>$bank_name</option>";				
				} else {
					if($isPopularAdded) {
						$text .= "</optgroup>";
						$isPopularAdded = false;
					}
					if(!$isnonPopularAdded){
						$text .= "<optgroup label='All banks'>";
						$isnonPopularAdded = true; 
					}
					$text .= "<option value='$bank_id'>$bank_name</option>";
				}
			}
			if($isnonPopularAdded) {
				$text .= "</optgroup>";
				$isnonPopularAdded = false;
			}
    		$xcache->store(self::NET_BANKING_BANKS_TEXT_CACHE, $text, 0);
    	}
    	
    	return $text;
	}
	
	public static function getActiveBanksList() {
		global $xcache;
		if($xcache==null) {
			$xcache = new XCache();
		}
		$cachedList = $xcache->fetch(self::NET_BANKING_BANKS_CACHE);
		if($cachedList == NULL) {
    		$result = func_query("select id,name,is_popular from mk_netbanking_banks where id in (select distinct(bank_id) from mk_netbanking_mapping where is_active=1) order by is_popular desc,name");
    		$cachedList = array();
    		foreach ($result as $banks) {
    			$bank_name = $banks['name'];
				$bank_id = $banks['id'];
				$is_popular = $banks['is_popular'];				
    			$cachedList[$bank_id] = array("name" =>$bank_name,"is_popular" => $is_popular);
    		}
    		$xcache->store(self::NET_BANKING_BANKS_CACHE, $cachedList, 0);
    	}
    	return $cachedList;
	}
	
	public static function getActiveGatewaysList() {
		global $xcache;
		if($xcache==null) {
			$xcache = new XCache();
		}
		$cachedList = $xcache->fetch(self::NET_BANKING_GATEWAY_CACHE);
		if($cachedList == NULL) {
    		 $cachedList = func_query("select id,name,weight from mk_netbanking_gateways where id in (select distinct(gateway_id) from mk_netbanking_mapping where is_active=1)");
    		 $xcache->store(self::NET_BANKING_GATEWAY_CACHE, $cachedList, 0);
    	}
    	return $cachedList;
	}
	
	public static function getActiveGatewaysListForBank($bankid) {
		global $xcache;
		if($xcache==null) {
			$xcache = new XCache();
		}
		$cachedList = $xcache->fetch(self::NET_BANKING_GATEWAY_CACHE.'-'.$bankid);
		if($cachedList == NULL) {
    		 $cachedList = func_query("select name,weight,gateway_code from mk_netbanking_mapping m,mk_netbanking_gateways g where bank_id=$bankid and is_active=1 and m.gateway_id=g.id");
    		 $xcache->store(self::NET_BANKING_GATEWAY_CACHE.'-'.$bankid, $cachedList, 0);
    	}
    	return $cachedList;		
	}
	
	public static function logNetBankingChangeActivity($login,$action,$bank_modified='',$gateway_modified='',$extra_info='') {
		$_time = time();
		func_query("insert into `mk_netbanking_logs` (`login`,`time_modify`,`action`,`bank_modified`,`gateway_modified`,`extra_info`) values ('$login',$_time,'$action','$bank_modified','$gateway_modified','$extra_info')");	
	}

	public static function getNetBankingGateway($gateways) {
		//expecting a array having name,weight and bankcode
		if(!is_array($gateways)) return null;
		$arrayElement = array();
		$weightarray = array();
		foreach($gateways as $gateway){
			$weightarray[] = $gateway['weight'];
			$arrayElement[] = array('name' =>$gateway['name'],'gateway_code' => $gateway['gateway_code'] );
		}
		return common\Util::selectRandomArrayElement($arrayElement,$weightarray);
	}
	
	public static function clearCachedValues() {
		$toInvalidateKey = array(self::NET_BANKING_BANKS_CACHE,self::NET_BANKING_BANKS_TEXT_CACHE,self::NET_BANKING_GATEWAY_CACHE);
		$xCacheInvalidator =  InvalidateXCache::getInstance();
		$xCacheInvalidator->invalidateCacheForKey($toInvalidateKey);
	}
}
?>