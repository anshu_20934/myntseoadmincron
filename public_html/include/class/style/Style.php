<?php

namespace style;

class Style
{
	private $version = 0;
	
	private $specificAttributeFitInfo =NULL;

	private $styleid = NULL;

	private $brandName = NULL;

	private $styleProperties = NULL;

	private $styleIdDetails = NULL;

	private $productStyleDetails = NULL;

	private $productStyleCount = NULL;

	private $brandData = NULL;

	private $discountData = NULL;

	private $catalogueClassificationData = NULL;

	private $productOptionNameValues = NULL;
	

	private $availableOptionsSizeUnificationData = NULL;

	private $allOptionsSizeUnificationData = NULL;

	private $sizeOptionsTooltipData = NULL;

	private $productTypeId = NULL;

	private $productTypeLabel = NULL;

	private $isCustomizable = false;

	private $pageUrl = "";

	private $sizeChartImagePath = "";

	private $productStyleLabel = "";

	private $styleColorGroupId = NULL;

	private $styleColorGroupRelatedStyleIds = NULL;

	private $styleDetailsForColorGroupId = NULL;

	private $crossSellProductInfo = "";		//this holds the cross selling category for style

	private $productOptions = array();

	private $loyaltyFactor = 0;
	
	/**
	 * Constructor.
	 */
	public function __construct($styleid)
	{
		$this->styleid = $styleid;
		$this->version = 1;
	}

	/**
	 *
	 *
	 */
	public function getStyleId(){
		return $this->styleid;
	}

	/**
	 *
	 *
	 */
	public function getStyleProperties(){
		return $this->styleProperties;
	}

	/**
	 *
	 *
	 */
	public function setStyleProperties($styleProperties){
		$this->styleProperties=$styleProperties;
	}

	/**
	 *
	 *
	 */
	public function getStyleIdDetails(){
		return $this->styleIdDetails;
	}

	/**
	 *
	 *
	 */
	//paroksh
	public function setSpecificAttributeFitInfo($fitInfo){
		$this->specificAttributeFitInfo = $fitInfo;
	}
	public function getSpecificAttributeFitInfo(){
		return $this->specificAttributeFitInfo;
	}
	
	//paroksh ends;
	public function setStyleIdDetails($styleIdDetails){
		$this->styleIdDetails=$styleIdDetails;
	}

	/**
	 *
	 *
	 */
	public function getProductStyleDetails(){
		return $this->productStyleDetails;
	}

	/**
	 *
	 *
	 */
	public function setProductStyleDetails($productStyleDetails){
		$this->productStyleDetails=$productStyleDetails;
	}

	/**
	 *
	 *
	 */
	public function getProductStyleCount(){
		return $this->productStyleCount;
	}

	/**
	 *
	 *
	 */
	public function setProductStyleCount($productStyleCount){
		$this->productStyleCount=$productStyleCount;
	}

	/**
	 *
	 *
	 */
	public function getBrandData(){
		return $this->brandData;
	}

	/**
	 *
	 *
	 */
	public function setBrandData($brandData){
		$this->brandData=$brandData;
	}

	/**
	 *
	 *
	 */
	public function getBrandName(){
		return $this->brandName;
	}

	/**
	 *
	 *
	 */
	public function setBrandName($brandName){
		$this->brandName=$brandName;
	}

	/**
	 *
	 *
	 */
	public function getCatalogueClassificationData(){
		return $this->catalogueClassificationData;
	}

	/**
	 *
	 *
	 */
	public function setCatalogueClassificationData($catalogueClassificationData){
		$this->catalogueClassificationData=$catalogueClassificationData;
	}

	/**
	 *
	 *
	 */
	public function getAllProductOptions(){
		return $this->getProductOptions();
	}


	/**
	 *
	 *
	 */
	public function getDiscountData(){
		return $this->discountData;
	}

	/**
	 *
	 *
	 */
	public function setDiscountData($discountData){
		$this->discountData=$discountData;
	}

	/**
	 *
	 *
	 */
	public function getSizeUnificationDataForAvailableOptions(){
		return $this->availableOptionsSizeUnificationData;
	}

	/**
	 *
	 *
	 */
	public function setSizeUnificationDataForAvailableOptions($availableOptionsSizeUnificationData){
		$this->availableOptionsSizeUnificationData=$availableOptionsSizeUnificationData;
	}

	/**
	 *
	 *
	 */
	public function getSizeUnificationDataForAllOptions(){
		return $this->allOptionsSizeUnificationData;
	}

	/**
	 *
	 *
	 */
	public function setSizeUnificationDataForAllOptions($allOptionsSizeUnificationData){
		$this->allOptionsSizeUnificationData=$allOptionsSizeUnificationData;
	}

	/**
	 *
	 *
	 */
	public function getSizeOptionsTooltipData(){
		return $this->sizeOptionsTooltipData;
	}

	/**
	 *
	 *
	 */
	public function setSizeOptionsTooltipData($sizeOptionsTooltipData){
		$this->sizeOptionsTooltipData=$sizeOptionsTooltipData;
	}

	/**
	 *
	 *
	 */
	public function getActiveProductOptionsData(){
		$ret = array();
		$productOptions = $this->productOptions;
		foreach ($productOptions as $option){
			if($option["is_active"]==1 && $option["sku_count"]>0){
				$ret[]= $option;
			}
		}
		return $ret;
	}


	/**
	 *
	 *
	 */
	public function getInactiveProductOptionsData(){
		$ret = array();
		$productOptions = $this->productOptions;
		foreach ($productOptions as $option){
			if(empty($option["is_active"]) || $option["sku_count"]<=0){
				$ret[]= $option;
			}
		}
		return $ret;
	}



	/**
	 *
	 *
	 */
	public function getColorGroupId(){
		return $this->styleColorGroupId;
	}

	/**
	 *
	 *
	 */
	public function setColorGroupId($styleColorGroupId){
		$this->styleColorGroupId=$styleColorGroupId;
	}

	/**
	 *
	 *
	 */
	public function getStyleColorGroupRelatedStyleIds(){
		return $this->styleColorGroupRelatedStyleIds;
	}

	/**
	 *
	 *
	 */
	public function setStyleColorGroupRelatedStyleIds($styleColorGroupRelatedStyleIds){
		$this->styleColorGroupRelatedStyleIds=$styleColorGroupRelatedStyleIds;
	}
	/**
	 *
	 *
	 */
	public function getStyleIdDetailsForColorGroup(){
		return $this->styleDetailsForColorGroupId;
	}

	/**
	 *
	 *
	 */
	public function setStyleIdDetailsForColorGroup($styleDetailsForColorGroupId){
		$this->styleDetailsForColorGroupId=$styleDetailsForColorGroupId;
	}

	public function getCrossSellProductInfo()
	{
		return $this->crossSellProductInfo;
	}

	public function setCrossSellProductInfo($crossSellProductInfo)
	{
		$this->crossSellProductInfo = $crossSellProductInfo;
	}
	
	public function getProductOptions(){
		return $this->productOptions;
	}
	
	public function setProductOptions($productOptions){
		$this->productOptions=$productOptions;
	}
	
	public function setProductOptionNameValues($productOptionNameValues){
		$this->productOptionNameValues=$productOptionNameValues;
	}
	
	/**
	 *
	 *
	 */
	public function getProductOptionNameValues(){
		return $this->productOptionNameValues;
	}

	public function getLoyaltyFactor(){
		return $this->loyaltyFactor;
	}

	public function setLoyaltyFactor($loyaltyFactor){
		$this->loyaltyFactor = $loyaltyFactor;
	}
	

}