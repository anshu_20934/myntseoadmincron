<?php

namespace style\builder;
global $xcart_dir;
require_once "$xcart_dir/modules/loyaltyPoints/LoyaltyPointsStyleObject.php";
use style\Style;
use style\dao\StyleDBAdapter;

class StyleBuilder
{	
	
	private static $instance = NULL;

	/**
	 * Constructor.
	 */
	public function __construct()
	{
	}
	
    /**
     * @return StyleBuilder The singleton instance.
     */
    public static function getInstance(){
        if (StyleBuilder::$instance == NULL) {
            StyleBuilder::$instance = new StyleBuilder();
        }
		
        return StyleBuilder::$instance;
    }

    /**
     * 
     * @param unknown_type $productStyleId
     * @return \style\Style
     */
	public function getStyleObject($productStyleId){
		$map = $this->getStyleObjectMap(array($productStyleId));
		if(empty($map[$productStyleId]))
			return null;
        return $$map[$productStyleId];
	}
	

	/*Color Group related*
	 * 
	 */
	 private function getStyleIdDetailsForColorGroupIds($styleInGroupArray){
		$searchProducts = \SearchProducts::getInstance();
		$solrResult = $searchProducts->getMultipleStyleFormattedSolrData($styleInGroupArray);
		return $solrResult;
	}
	


	
	private static function getSizeUnificationDataForAllOptions($allProductOptionDetails, $productStyleId){
		return \Size_unification::getUnifiedSizeForOptions($allProductOptionDetails, $productStyleId, "array");
	}
	
	private static function getSizeOptionsTooltipData($allProductOptionDetails, $productStyleId){
		return \Size_unification::getUnifiedSizeForOptions($allProductOptionDetails, $productStyleId, "array", "tooltip");
	}
	
	private static function getRelatedProductsFromSolr($productTags, $productStyleId){
		$searchProducts = SearchProducts::getInstance();	
		$relatedProducts=$searchProducts->getRelatedProductsPDP($productTags,"0_style_$productStyleId",0,5);
		return $relatedProducts;
	}
	
	
	public function getStyleObjectMap($productStyleIdsArray){
		$styleDBAdapterObj = StyleDBAdapter::getInstance();
		$styleObjMap=$styleDBAdapterObj->getStyleObjectsMap($productStyleIdsArray);
		
		$styleObj=$this->populateStyleDataOptimal($styleObjMap);
		
		
		return $styleObjMap;
	}
	
	private function populateStyleDataOptimal(array $styleObjMap){
		
		$stylesInGroup = array();
		$styleIds = array();
		foreach($styleObjMap as $styleId=>$styleObj){
			/* Call all other function here */
			$productStyleId=$styleObj->getStyleId();
			//$styleObj->setAllProductOptions($this->getAllProductOptions($productStyleId));
			//$styleObj=$this->setActiveAndInactiveProductOptionsData($styleObj);
			$productOptionDetails=$styleObj->getActiveProductOptionsData();
			$allProductOptionDetails=$styleObj->getProductOptions();
			$availableOptions = $styleObj->getActiveProductOptionsData();
			//$styleObj=$this->setProductOptionNameValues($styleObj);
			//TODO need to optimize this also
			$styleObj->setSizeUnificationDataForAvailableOptions($this->getSizeUnificationDataForAllOptions($availableOptions, $productStyleId));
			$styleObj->setSizeUnificationDataForAllOptions($this->getSizeUnificationDataForAllOptions($allProductOptionDetails, $productStyleId));
			$styleObj->setSizeOptionsTooltipData($this->getSizeOptionsTooltipData($allProductOptionDetails, $productStyleId));
			$stylesInGroup = array_merge($stylesInGroup,$styleObj->getStyleColorGroupRelatedStyleIds());
			$styleIds[] = $styleObj->getStyleId();
		}
		
		
		$solrResult = $this->getStyleIdDetailsForColorGroupIds($stylesInGroup);
		foreach($solrResult as $solrStyle){
			$solrStyleResult[$solrStyle["styleid"]]=$solrStyle;
		}
		$discountData = \DiscountEngine::getDataForMultipleStyles('pdp', $styleIds);
		self::loadStyleAvailabilityData($styleObjMap);
        self::getLoyaltyConversionFactor($styleObj);
		foreach($styleObjMap as $styleId=>$styleObj){
			$styleObj->setDiscountData($discountData->$styleId);
			$styleGroupDetail=array();
			$styleColourGroups = $styleObj->getStyleColorGroupRelatedStyleIds();
			foreach($styleColourGroups as $styleId){
				if(!empty( $solrStyleResult[$styleId]))
					$styleGroupDetail[] = $solrStyleResult[$styleId];
			}
			$styleObj->setStyleIdDetailsForColorGroup($styleGroupDetail); 
			$availableOptions = $styleObj->getActiveProductOptionsData();
			$styleObj->setSizeUnificationDataForAvailableOptions($this->getSizeUnificationDataForAllOptions($availableOptions, $productStyleId));
			$this->setProductOptionNameValues($styleObj);
		}
	}
	
	private function getIndexedArray($array){
		$ret;
		foreach($array as $val)
			$ret[] = $val;
		
		return $ret;
	}
	
	private function setProductOptionNameValues(Style $styleObj){
		$optionDetailsArray=$styleObj->getAllProductOptions();
		if (empty($optionDetailsArray)){
			return $styleObj;
		}
		else{
			$optionNameValueArray = array();
			$prevName = "";
			$values = array();
			for($i=0;$i<=count($optionDetailsArray);$i++){
				$name = $optionDetailsArray[$i][1];
				if ($name != $prevName){
					if ($i != 0){
						$optionNameValueArray[] = array($prevName,$values);
					}
					$prevName = $name;
					$values = array();
				}
				$values[] = array($optionDetailsArray[$i][2],$optionDetailsArray[$i][0],$optionDetailsArray[$i][3],$optionDetailsArray[$i][4]);
			}
			$productOptionNameValues=$optionNameValueArray;
		}
	
		$styleObj->setProductOptionNameValues($productOptionNameValues);
		return $styleObj;
	}
	
	
 	public static function loadStyleAvailabilityData(array $styleObjMap){
        $skuIds = array();
        foreach($styleObjMap as $styleId=>$styleObj){
            $allProductOptionDetails=$styleObj->getProductOptions();
            foreach($allProductOptionDetails as $option){
                $skuIds[]=$option["sku_id"];
            }
        }
        $skuId2AvailableInventoryMap = \AtpApiClient::getAvailableInventoryForSkus($skuIds);
        global $weblog;
        $weblog->info("sku available count for skulist");
        foreach ($styleObjMap as $styleId=>$styleObj) {
            $allProductOptionDetails=$styleObj->getProductOptions();
            foreach($allProductOptionDetails as &$option){
                $available_count = $skuId2AvailableInventoryMap[$option['sku_id']]['availableCount'];
                $option['availableInWarehouses'] = $skuId2AvailableInventoryMap[$option['sku_id']]['availableInWarehouses'];
                $option['leadTime'] = $skuId2AvailableInventoryMap[$option['sku_id']]['leadTime'];
                $option['supplyType'] = $skuId2AvailableInventoryMap[$option['sku_id']]['supplyType'];
                $option['is_active'] = $option['is_active'] == 1 && ($available_count > 0);
                $option['sku_count'] = $available_count;
            }

            $styleObj->setProductOptions($allProductOptionDetails);
        }

    }

    public static function getLoyaltyConversionFactor(Style $styleObj){
        $style_properties = $styleObj->getStyleProperties();
        $loyaltyStyleObject = new \LoyaltyPointsStyleObject($styleObj->getStyleId(),$styleObj->getBrandName(),$style_properties["global_attr_gender"],$style_properties["global_attr_article_type"]);
        $loyaltyFactor = \LoyaltyPointsPortalClient::getStyleLoyaltyFactor($loyaltyStyleObject);
        //Set LoyaltyPoints; 
        $styleObj->setLoyaltyFactor($loyaltyFactor);       
    }
	
}
