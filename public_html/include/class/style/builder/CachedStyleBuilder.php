<?php

namespace style\builder;
use style\builder\StyleBuilder;

class CachedStyleBuilder
{
	
	private static $instance = NULL;
	
	private $cache = NULL;

	/**
	 * Constructor.
	 */
	public function __construct()
	{
	}
	
    /**
     * @return StyleBuilder The singleton instance.
     */
    public static function getInstance(){
        if (CachedStyleBuilder::$instance == NULL) {
            CachedStyleBuilder::$instance = new CachedStyleBuilder();
        }
		
        return CachedStyleBuilder::$instance;
    }
	
	/**
	 * 
	 * 
	 */
	public function getStyleObject($productStyleId){
		$map = $this->getStyleObjects(array($productStyleId));
		if(!empty($map[$productStyleId])){
			$discountData = \DiscountEngine::getDataForAStyle('pdp',$productStyleId);
			$styleObj = $map[$productStyleId];
            if($discountData) {
            	$styleObj->setDiscountData($discountData);
            }
            return $styleObj;
        }
		return null;
	}	
	
	public function getStyleObjects($productStyleIdsArray){
		$styleIdToObjectMap;
		$uncachedStyles;
		foreach ($productStyleIdsArray as $id){
			$this->cache = new \Cache(\PDPCacheKeys::$prefix.$id, \PDPCacheKeys::keySet());
			$styleObject = $this->cache->getValue(\PDPCacheKeys::$styleObject);
			if(!empty($styleObject)){
				$styleIdToObjectMap[$id] = $styleObject;
			}else{
				$uncachedStyles[]=$id;
			}
		}
	
		if(!empty($uncachedStyles)){
			$styleBuilderObject=StyleBuilder::getInstance();
			$styleObjectMap  = $styleBuilderObject->getStyleObjectMap($uncachedStyles);
		}
		
		foreach ($uncachedStyles as $id){
			$this->cache = new \Cache(\PDPCacheKeys::$prefix.$id, \PDPCacheKeys::keySet());
			$styleObject = $this->cache->setVal(\PDPCacheKeys::$styleObject,$styleObjectMap[$id]);
			$styleIdToObjectMap[$id]=$styleObjectMap[$id];
		}
		return $styleIdToObjectMap;
	}
}
