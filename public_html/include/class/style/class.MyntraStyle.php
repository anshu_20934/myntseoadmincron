<?php
include_once ($xcart_dir."/include/class/class.feature_gate.keyvaluepair.php");
include_once($xcart_dir."/include/func/func_sku.php");
include_once("$xcart_dir/modules/apiclient/SkuApiClient.php");

class MyntraStyle
{
	/**
	 * Constructor. Style creating related code
	 */
	public function __construct()
	{
	}

	public static function getSkuId($skuCode){
		global $weblog;
		$weblog->info("Checking sku id for skuCode: $skuCode");
		$skudetails = SkuApiClient::getSkuDetailsWithInvCountsForCodes($skuCode);
		return $skudetails[0]['id'];
	}
	
	/**
	 * This is unused - very old legacy code - should no longer be used
	 * this method will check if the option can be enabled or not
	 * @param $sku_code
	 * @param $brand
	 * @param $fashion_type
	 * @param $season
	 * @param $year
	 *
	public static function check_can_enable_option($sku_code, $brand, $fashion_type, $season, $year) {
		global $xcart_dir, $weblog, $login;
		$weblog->info("Checking sku:sku_code brand fashion_type season year:$sku_code, $brand, $fashion_type, $season, $year");

		$skudetails = SkuApiClient::getSkuDetailsWithInvCountsForCodes($sku_code);
		$sku_id = $skudetails[0]['id'];
		$jit_sourced = $skudetails[0]['jitSourced'];
		
		$inv_count = 0;
		foreach($skudetails as $sku_row) {
			$inv_count += $sku_row['invCount']>0?$sku_row['invCount']:0;
		}
		
		//if not inv count present.. then check if style can be jit sourced..
		$jit_sourcing_enabled = FeatureGateKeyValuePairs::getFeatureGateValueForKey('jit.sourcing.enabled');
		if($jit_sourcing_enabled == 'true' && $inv_count <= 0 && $jit_sourced==0) {
			$weblog->info("Style id - $styleid - checking jit_source");
			$can_jit_source = func_check_can_source_style_with_attributes($brand, $fashion_type, $season, $year);
			$weblog->info("Style id - $styleid - can be jit sourced now - $can_jit_source");
			if($can_jit_source) {
				$extra_info = array();
				$extra_info['reason']=getReasonByCode("AJE");
				$extra_info["reasonid"]=$extra_info["reason"]['id'];
				$extra_info['value']='true';
				$extra_info['user']="AUTOSYSTEM";
				updateSKUPropertyWMS($sku_id, 'jit_sourced', 'true', $login, $extra_info, $inv_count);

				$jit_sourced = true;
			}
		}
		//if inventory count is greater than 0 or is jit, then enable it otherwise disable it
		$option_enabled = 0;
		if($inv_count > 0 || $jit_sourced){
			$option_enabled = 1;
		}
		return array($sku_id, $option_enabled);
	}*/
}