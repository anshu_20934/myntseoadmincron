<?php
namespace style;

class StyleGroup{
	
	private $patternName;
	
	private $groupType;
	
	private $styleIdsArray;
	
	private $groupId;
	
	public function __construct($patternName,$groupType,$styleIdsArray=array(),$groupId=0){
		$this->patternName=$patternName;
		$this->groupType=$groupType;
		$this->groupId=$groupId;
		$this->styleIdsArray=$styleIdsArray;
	}
	
	public function addToGroup($styleId){
		if(!in_array($styleId,$this->styleIdsArray)){
			$this->styleIdsArray[]=$styleId;
		}
	}
	
	public function getPatternName(){
		return $this->patternName;
	}
	
	public function getGroupType(){
		return $this->groupType;
	}
	
	public function getStyleIdsArray(){
		return $this->styleIdsArray;
	}
	
	public function getGroupId(){
		return $this->groupId;
	}
	
	public function setGroupId($groupId){
		$this->groupId=$groupId;
	}
}