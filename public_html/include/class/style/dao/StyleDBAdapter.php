<?php
namespace style\dao;
use style\Style;
use style\dao\StyleGroupDBAdapter;
/**
 * DB Adapter class for Style
 * @author raghu
 *
 */
class StyleDBAdapter {

	private static $table_mk_product_style = "mk_product_style";

	private static $table_mk_style_properties = "mk_style_properties";

	private static $table_mk_catalog_classification = "mk_catalogue_classification";

	private static $table_mk_filters = "mk_filters";

	private static $table_mk_product_type = "mk_product_type";

	private static $table_mk_style_groups = "style_groups";

	private static $table_mk_attribute_type_values = "mk_attribute_type_values";

	private static $table_recommendation_rules_input = "recommendation_rules_input";

	private static $table_recommendation_rules_output = "recommendation_rules_output";

	private static $table_recommendation_rules_mapping = "recommendation_rules_mapping";

	private static $table_mk_product_options= "mk_product_options";

	private static $table_mk_styles_options_skus_mapping="mk_styles_options_skus_mapping";

	private static $instance = NULL;

	private $json_brand_article_id = NULL;

	/**
	 * Constructor.
	 */
	public function __construct()
	{
		$this->json_brand_article_id = \WidgetKeyValuePairs::getWidgetValueForKey('brand-article-id');
	}

	/**
	 * @return StyleDBAdapter The singleton instance.
	 */
	public static function getInstance(){
		if (StyleDBAdapter::$instance == NULL) {
			StyleDBAdapter::$instance = new StyleDBAdapter();

		}

		return StyleDBAdapter::$instance;
	}

	public function getStyleObject($productStyleId){
		$styleMap = $this->getStyleObjectsMap(array($productStyleId));
		return $styleMap[$productStyleId];
	}



	//@author: Paroksh Saxena
	public function updateProductStyleTable($query_data,$styleId){
		return func_array2updateWithTypeCheck(self::$table_mk_product_style, $query_data, "id='$styleId' ");
	}

	//@author: Paroksh Saxena
	public function getStylePropertiesForStyleIDList($styleIdsCSV){
		$sql = "SELECT * FROM ".self::$table_mk_style_properties." WHERE style_id in ($styleIdsCSV)";
		return func_query($sql, TRUE);
	}


	public function getSpecificAttributeFitInfo(Style $styleObj){
		$result = array(false,false);

		$catalogData = $styleObj->getCatalogueClassificationData();
		$article_id = $catalogData['article_id'];
		$brandInfo = $styleObj->getBrandData();
		$brand_id = $brandInfo['brand_id'];


		$brand_article_id = json_decode($this->json_brand_article_id,true);

		 
		foreach($brand_article_id as $key => $value){
			if($value['brand_id'] == $brand_id && $value['article_id'] == $article_id){
				$result[1] = $value['info'];
				$result[0] = true;
				break;
			}
		}
		return $result;
	}


	/**
	 * @author yogesh
	 */

	public function getStyleObjectsMap($productStyleIdsArray){
		 
		$stylePropertiesArray=$this->getMultiStyleProperties($productStyleIdsArray);
		$styleMap =array();
		 
		$styleGroupAdapter = new StyleGroupDBAdapter();
		$styleGroupsArray = $styleGroupAdapter->getStyleGroupsMapForMultipleStyles($productStyleIdsArray,"taba.group_type='color'");

		foreach ($stylePropertiesArray as $styleDetailsRow){
			$styleObj = $styleMap[$styleDetailsRow["psid"]];
			if($styleObj == null){
				$styleObj = new Style($styleDetailsRow["psid"]);
				$this->populateStyleDataWithRow($styleObj,$styleDetailsRow);
			}
			$this->populateStyleOptionDataWithRow($styleObj,$styleDetailsRow);
			$styleMap[$styleDetailsRow["psid"]] = $styleObj;
			$groupArray = $styleGroupsArray[$styleObj->getStyleId()];

			$styleIds=array();
			if(!empty($groupArray) && $groupArray[0]!= null){
				$styleIds=$groupArray[0]->getStyleIdsArray();
			}
			$styleObj->setStyleColorGroupRelatedStyleIds($styleIds);

		}
		 
		foreach($styleMap as $styleId=>$styleObj){
			$arr = $styleObj->getProductOptions();
			foreach($arr as $id=> $option){
				$option = array_merge($option,$this->getIndexedArray($option));
				$arr[$id] = $option;
			}
			$styleObj->setProductOptions($arr);
		}
		 
		$this->populateRecommendationDataForStyleMap($styleMap);
		 
		return $styleMap;
	}

	private function getMultiStyleProperties($productStyleIdsArray){

		$style_properties_sql="select ps.image_l psimage_l,ps.image_t psimage_t, ps.image_i psimage_i,
		ps.l_width psl_width, ps.l_height psl_height, ps.id psid, ps.product_type psproduct_type,
		ps.name psname,ps.label pslabel,ps.description psdescription,ps.is_active psis_active,
		ps.price psprice,ps.styletype psstyletype,ps.style_code psstyle_code,ps.parent_style psparent_style,
		ps.material psmaterial,ps.size_chart_image pssize_chart_image,ps.cod_enabled pscod_enabled, ps.iventory_count psiventory_count,
		ps.is_customizable psis_customizable, sp.* , pt.id ptproduct_type, pt.product_type_groupid ptproduct_type_groupid,
		pt.name ptname,br.filter_name brfilter_name,br.logo brlogo,cc1.typename as ccarticletype,cc1.id as ccarticle_id,
		cc1.disclaimer_title as ccdisclaimer_title,cc1.disclaimer_pdp_display_text as ccdisclaimer_pdp_display_text,
		cc2.typename as ccsub_category, cc2.id as ccsub_category_id , cc3.typename as ccmaster_category,
		cc3.id as ccmaster_category_id , atv.id brbrand_id, po.id poid, po.name poname, po.value povalue, po.price poprice,
		po.default_option podefault_option, som.sku_id as posku_id, po.is_active pois_active from  "
		.self::$table_mk_product_style." ps join "
		.self::$table_mk_style_properties." sp on ps.id=sp.style_id and ps.id in (".implode(",", $productStyleIdsArray).") left join "
		.self::$table_mk_filters." br on br.filter_name=sp.global_attr_brand left join "
		.self::$table_mk_product_type." pt on pt.id =ps.product_type left join "
		.self::$table_mk_catalog_classification ." cc1 on sp.global_attr_article_type=cc1.id left join "
		.self::$table_mk_catalog_classification ." cc2 on cc1.parent1=cc2.id left join "
		.self::$table_mk_catalog_classification ." cc3 on cc1.parent2=cc3.id left join "
		.self::$table_mk_attribute_type_values  ." atv on atv.attribute_value = global_attr_brand left join "
		.self::$table_mk_product_options  ." po on po.style = ps.id inner join "
		.self::$table_mk_styles_options_skus_mapping  ." som on  po.id = som.option_id "

		."order by ps.id, po.id";
		 
		return func_query($style_properties_sql,true);
	}

	private function populateStyleDataWithRow(Style $styleObj, $row){
		/* Call all other function here */
		$productStyleId=$styleObj->getStyleId();
		$styleDetailsArray = array();
		$stylePropertyArray = array();
		$styleProductTypeArray = array();
		$brandDetailsArray = array();
		$catalogCatalogueClassificationDataArray = array();
		foreach($row as $key => $value){
			$prefix = substr($key, 0,2);
			$newKey = substr($key, 2);
			if($prefix==="ps"){
				$styleDetailsArray[$newKey] = $row[$key];
			}else if($prefix==="pt"){
				$styleProductTypeArray[$newKey] = $row[$key];
			}else if($prefix==="br"){
				$brandDetailsArray[$newKey] =  $row[$key];
			}else if($prefix==="cc"){
				$catalogCatalogueClassificationDataArray[$newKey] =  $row[$key];
			}else if($prefix==="po"){
				 
			}else{
				$stylePropertyArray[$key] =  $row[$key];
			}
		}

		$stylePropertyArray['pdpUrl'] = construct_style_url($catalogCatalogueClassificationDataArray['articletype'], $stylePropertyArray['global_attr_brand'], $stylePropertyArray['product_display_name'], $stylePropertyArray['style_id']);

		$styleObj->setProductStyleDetails($styleDetailsArray);
		$styleObj->setStyleProperties($stylePropertyArray);
		$styleObj->setStyleIdDetails($styleProductTypeArray);
		 
		if($styleDetailsArray["iventory_count"]>0 && $styleDetailsArray["styletype"]=="P"){
			$styleObj->setProductStyleCount(1); // why do we need this
		}else{
			$styleObj->setProductStyleCount(0);
		}
		$styleObj->setBrandName($stylePropertyArray["global_attr_brand"]);
		$styleObj->setBrandData($brandDetailsArray);
		 
		$styleObj->setCatalogueClassificationData($catalogCatalogueClassificationDataArray);
		$styleObj->setSpecificAttributeFitInfo($this->getSpecificAttributeFitInfo($styleObj));
		 
		return $styleObj;
	}

	private function populateStyleOptionDataWithRow(Style $styleObj, $row){
		foreach($row as $key => $value){
			$prefix = substr($key, 0,2);
			$newKey = substr($key, 2);
			if($prefix==="po"){
				$styleOption[$newKey] = $row[$key];
			}
		}
		 
		$arr = $styleObj->getProductOptions();
		$arr[] =$styleOption;
		$styleObj->setProductOptions($arr);
		return $styleObj;
	}

	private function populateRecommendationDataForStyleMap(array $styleMap){
		$input_ruleArray;
		foreach($styleMap as $styleId=>$styleObj){
			$styleProperties = $styleObj->getStyleProperties();
			$catalogData = $styleObj->getCatalogueClassificationData();

			$article_id = (int)$catalogData['article_id'];
			$gender = $styleProperties["global_attr_gender"];
			if($gender == "Unisex")
				$gender = "Men";
			$input_ruleArray[]="(rri.global_attr_article_type='{$article_id}' and rri.global_attr_gender='".mysql_real_escape_string($gender)."')";
		}
		 
		 
		 
		$recommendation_input_rule_sql = "select  rri.global_attr_article_type,rri.global_attr_gender,rro.output_rule from "
		.self::$table_recommendation_rules_input." rri ,"
		.self::$table_recommendation_rules_mapping." rrm ,"
		.self::$table_recommendation_rules_output." rro
		where rri.id=rrm.input_rule_id  and rrm.output_rule_id = rro.id and (".implode(" or ",$input_ruleArray).")";
		$out = func_query($recommendation_input_rule_sql,true);
		 
		$tempArray;
		foreach($out as $row){
			$temp = json_decode($row["output_rule"], true);
			$tempArray[$row["global_attr_article_type"]][$row["global_attr_gender"]]["article_type"][]=$temp[0]["article_type"];
		}
		 
		 
		foreach($styleMap as $styleId=>$styleObj){
			$styleProperties = $styleObj->getStyleProperties();
			$catalogData = $styleObj->getCatalogueClassificationData();
			$article_id = (int)$catalogData['article_id'];
			$gender = $styleProperties["global_attr_gender"];
			if($gender == "Unisex")
				$gender = "Men";

			$styleObj->setCrossSellProductInfo(implode(",",$tempArray[$article_id][$gender]["article_type"]));
		}
	}
	
	private function getIndexedArray($array){
		$ret;
		foreach($array as $val)
			$ret[] = $val;
	
		return $ret;
	}
}
