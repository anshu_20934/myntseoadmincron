<?php
namespace style\dao;

use style\StyleGroup;
/**
 * Adapter class for Style Groups
 * @author yogesh
 *
 */
class StyleGroupDBAdapter {

	private $tab_style_groups="style_groups";


	/**
	 * Function to save a new Style Group
	 * @param StyleGroup $styleGroup
	 */
	public function saveNewGroup(StyleGroup $styleGroup){
		$styleArray= $styleGroup->getStyleIdsArray();
		$arrayToInsert= array('pattern_name'=>$styleGroup->getPatternName(),'group_type'=>$styleGroup->getGroupType());
		$first=true;
		foreach ($styleArray as $style_id){
			$arrayToInsert['style_id']=$style_id;
			$groupId=func_array2insertWithTypeCheck($this->tab_style_groups, $arrayToInsert);
			if($first===true){
				$arrayToInsert['group_id']=$groupId;
				func_array2updateWithTypeCheck($this->tab_style_groups, $arrayToInsert, " id=$groupId");
				$first=false;
			}
		}

		$styleGroup->setGroupId($groupId);

		return true;
	}

	/**
	 * remove/delete groups
	 * @param array $groupsArray
	 */
	public function dissolveGroups(array $groupsArray){
		$group_ids=array();
		foreach ($groupsArray as $group){
			$group_ids[]=$group->getGroupId();
		}
		return db_query("delete from $this->tab_style_groups where group_id in (".mysql_real_escape_string(implode(',',$group_ids)).")");
	}
	
	/**
	 * 
	 * returns Style Group array for passes styleIds
	 * @param array $styleIds
	 * @param string $extra_conditions
	 * @param string $orderBy
	 */
	public function getStyleGroupsForMultipleStyles(array $styleIds,$extra_conditions=false,$orderBy=false){
		$linkStyleIds=mysql_real_escape_string(implode(',',$styleIds));
		$sql="select * from $this->tab_style_groups taba, 
			(select distinct group_id from $this->tab_style_groups where style_id in( $linkStyleIds )) tabb 
			 where taba.group_id =tabb.group_id  ";
		if($extra_conditions!=false){
			$sql.=" and $extra_conditions";
		}
		if($orderBy!=false){	
			$sql.=" $orderBy ";
		}
		$result_set=func_query($sql,true);

		return $this->getStyleGroupsFromRawData($result_set);
	}
	
	/**
	 *
	 * returns Style Group array for passed styleIds
	 * @param array $styleIds
	 * @param string $extra_conditions
	 * @param string $orderBy
	 */
	public function getStyleGroupsMapForMultipleStyles(array $styleIds,$extra_conditions=false,$orderBy=false){
		$groups = $this->getStyleGroupsForMultipleStyles($styleIds,$extra_conditions,$orderBy);
		$styleGroupMap= array();
		foreach($styleIds as $styleId){
			foreach($groups as $group){
				$arr = $group->getStyleIdsArray();
				if(in_array($styleId, $arr)){
					$styleGroupMap[$styleId][]=$group;
				}
			}
		}
		return $styleGroupMap;
	}
	
	
	/**
	 * returns style groups for single styleid
	 * @param integer $styleId
	 * @param string $extra_conditions
	 * @param string $orderBy
	 */
	public function getStyleGroupsForSingleStyle($styleId,$extra_conditions=false,$orderBy=false){
		return $this->getStyleGroupsForMultipleStyles(array($styleId),$extra_conditions,$orderBy);
	}

	/**
	 * unlink styles from a group
	 * @param integer $groupId
	 * @param array $styleIds
	 */
	public function unlinkStylesGroups($groupId,array $styleIds){

		return db_query("delete from $this->tab_style_groups where group_id='$groupId' and style_id in (".mysql_real_escape_string(implode(',',$styleIds)).")");
	}

	/**
	 * return Groups array for passed groupIds
	 * @param array $groupIds
	 * @param string $extra_conditions
	 */
	public function getStyleGroups(array $groupIds,$extra_conditions=false){
		$groupIdscsv=mysql_real_escape_string(implode(',',$groupIds));
		if(count($groupIds)==1){
			$sql="select * from $this->tab_style_groups where
		group_id = $groupIdscsv ";
		}else{
			$sql="select * from $this->tab_style_groups where
				group_id in ( $groupIdscsv )  ";
		}
		if($extra_conditions!=false){
			$sql.=" and $extra_conditions";
		}

		$result_set=func_query($sql,true);
		return $this->getStyleGroupsFromRawData($result_set);
	}

	/**
	 * return Group for passed groupId
	 * @param integer $groupId
	 * @param string $extra_conditions
	 */
	public function getStyleGroup($groupId,$extra_conditions=false){
		$result=$this->getStyleGroups(array($groupId),$extra_conditions);
		return $result[0];
	}
	

	private function getStyleGroupsFromRawData($result_set){
		$groupArray;
		foreach ($result_set as $row){
			$groupArray[$row['group_id']]['group_id']=$row['group_id'];
			$groupArray[$row['group_id']]['pattern_name']=$row['pattern_name'];
			$groupArray[$row['group_id']]['style_ids'][]=$row['style_id'];
			$groupArray[$row['group_id']]['group_type']=$row['group_type'];
		}

		foreach ($groupArray as $group){
			$styleGroup=new StyleGroup($group['pattern_name'], $group['group_type'],$group['style_ids'],$group['group_id']);
			$result[]=$styleGroup;
		}
		return $result;
	}

	/**
	 * get Style Groups
	 * @param string $extra_conditions
	 * @param string $orderBy
	 * @param integer $start
	 * @param integer $limit
	 */
	public function getAllStyleGroups($extra_conditions=false,$orderBy=false,$start=0,$limit=30){
		$start=mysql_real_escape_string($start);
		$limit=mysql_real_escape_string($limit);
		$sql="select distinct group_id from $this->tab_style_groups ";
		if($extra_conditions!=false){
			$sql.=" where $extra_conditions";
		}
		if($orderBy!=false){
			$sql.=" $orderBy ";
		}
		$sql.=" limit $start,$limit";

		$result_set=func_query($sql,true);
		$groupId;
		foreach ($result_set as $row){
			$groupId[]=$row['group_id'];
		}

		return $this->getStyleGroups($groupId);
	}

	/**
	 * Get number of existing groups
	 * @param string $extra_conditions
	 */
	public function getAllStyleGroupsCount($extra_conditions=false){
		$sql="select count(distinct group_id) totalCount from $this->tab_style_groups ";
		if($extra_conditions!=false){
			$sql.=" where $extra_conditions";
		}

		$result_set=func_query_first_cell($sql,true);

		return $result_set;
	}
}