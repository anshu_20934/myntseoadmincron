<?php
namespace style\dao;
use base\dao\BaseDAO;

class ProductOptionsDAO extends BaseDAO{
	
	/**
	 * Get an associative array of options for a style
	 * key: option value (e.g. S, M, L, XL) and value: option-id  
	 * @param unknown_type $styleId
	 */
	public function getProductOptions($styleId) {
		$tbl = "mk_product_options";
		$cols = array("id", "trim(value) as value");
		$filters = array("style" => $styleId);
		$result = func_select_columns_query($tbl, $cols, $filters, NULL, 100);
		
		$productOptions;
		foreach($result as $row){
			$productOptions[$row['value']]=$row['id'];
		}
		return $productOptions;
	}
	
	public function getOptionIdForSKUId($skuId) {
		$tbl = "mk_styles_options_skus_mapping";
		return func_query_first_cell("SELECT option_id from $tbl where sku_id=".$skuId);
	}
}