<?php

namespace style;
use style\exception\StyleGroupLinkingConflictException;
use style\dao\StyleGroupDBAdapter;
use style\exception\StyleGroupDoesNotExistsException;

/**
 * Style Group Manger.
 * This guy know how to handle groups
 * @author yogesh
 *
 */
class StyleGroupManager{
	
	/**
	 * Call to update or save a new group,
	 * Internal complexities for linking unlinking merging will be taken care
	 * @param array $linkStyleIdsArray
	 * @param string $groupType
	 * @param string $patternName
	 * @param integer $groupId
	 * @throws StyleGroupDoesNotExistsException
	 * @throws StyleGroupLinkingConflictException
	 */
	public function linkStyles(array $linkStyleIdsArray,$groupType,$patternName,$groupId=0){
		$adapter= new StyleGroupDBAdapter();
		//get groups for all styles to link to detect any conflict
		$existingGroups=$adapter->getStyleGroupsForMultipleStyles($linkStyleIdsArray," taba.group_type='$groupType'");
		$styleGroup=null;
		foreach ($existingGroups as $group){
			if($groupId == $group->getGroupId()){
				$styleGroup = $group;
				break;
			}
			
		}
		
		foreach ($existingGroups as $group){
			if($groupId == $group->getGroupId())
				continue;
			//Detect conflicts
			if($group->getPatternName()!=$patternName && ($styleGroup==null || ($styleGroup->getPatternName() != $group->getPatternName()))){
				if($styleGroup == null){
					throw new StyleGroupLinkingConflictException($linkStyleIdsArray,$group);
				}else{
					throw new StyleGroupLinkingConflictException($linkStyleIdsArray,$styleGroup);
				}
			}
			
			//Keep on merging small small groups to form a big one
			$linkStyleIdsArray= array_merge($linkStyleIdsArray,$group->getStyleIdsArray());
		}
		
		if($styleGroup!=null){
			$existingGroups[]=$styleGroup;
		}
		
		$linkStyleIdsArray=array_unique($linkStyleIdsArray);
		
		$styleGroup = new StyleGroup($patternName, $groupType,$linkStyleIdsArray);
		
		//Dissolve all old small poor groups
		if(!empty($existingGroups)){
			$adapter->dissolveGroups($existingGroups);
		}
		
		//Save Mr new Born as a new Group
		$adapter->saveNewGroup($styleGroup);
		return true;
	}
	
	/**
	 * 
	 * Unlinks styles from a group
	 * @param integer $groupId
	 * @param array $linkStyleIdsArray
	 */
	public function unlinkStyles($groupId,array $linkStyleIdsArray){
		$adapter= new StyleGroupDBAdapter();
		$adapter->unlinkStylesGroups($groupId, $linkStyleIdsArray);
		return true;
	}
	
}