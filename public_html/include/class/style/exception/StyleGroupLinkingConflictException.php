<?php
namespace style\exception;

use style\StyleGroup;

global $xcart_dir;
include_once "$xcart_dir/exception/MyntraException.php";
class StyleGroupLinkingConflictException extends \MyntraException{
	
	public function __construct($linkStyleIdsArray,StyleGroup $conflictingGroup){
		parent::__construct("Cannot Link Styles[".implode(',',$linkStyleIdsArray)."] as few Styles exists in different
		 Pattern [{$conflictingGroup->getPatternName()}] with same group type [".implode(',',$conflictingGroup->getStyleIdsArray())."]");
	}
}