<?php
namespace style\exception;

global $xcart_dir;
include_once "$xcart_dir/exception/MyntraException.php";
class StyleGroupDoesNotExistsException extends \MyntraException{
	
	public function __construct($group_id){
		parent::__construct("Group with id=$group_id does not exists ");
	}
}