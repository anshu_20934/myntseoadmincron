<?php
/*****************************************************************************\
+-----------------------------------------------------------------------------+
| Myntra                                                                      |
| Copyright (c) 2008 Myntra, Shantanu Bhadoria			                       |
| All rights reserved.                                                        |
+-----------------------------------------------------------------------------+
|                                                                             |
| The Initial Developer of the Original Code is Shantanu Bhadoria             |
+-----------------------------------------------------------------------------+
\*****************************************************************************/

#
# $Id: class.ordersearch.php,v 1.0
# Class With functions to retrieve search results on orders
class ordersearch {

    function func_create_search_query($request) {
    	global $current_area, $config, $sql_tbl;
    	$sql = "SELECT orderid, order_name, status, customer, issues_contact_number, date, queueddate, tax, ordertype, ((total+cod+payment_surcharge)- ifnull(ref_discount,0.00)) as order_total_amount,
				payment_method FROM $sql_tbl[orders] WHERE source_id = 1";

		$filter_query = array();
				
		if(!empty($request['order_status']))
			$filter_query[] .= "status = '".$request['order_status']."'";
			
		if(!empty($request['orderid']))
			$filter_query[] .= "orderid = ".$request['orderid'];
			
		if(!empty($request['customer_login']))
			$filter_query[] .= "login = '".$request['customer_login']."'";
			
		if(!empty($request['customer_name']))
			$filter_query[] .= "s_firstname = '".$request['customer_name']."'";
		
		if(!empty($request['customer_phonenum']))
			$filter_query[] .= "issues_contact_number = '".$request['customer_phonenum']."'";
			
		if(!empty($request['payment_method'])) {
			if($request['payment_method'] == 'cod')
				$filter_query[] .= "payment_method = 'cod'";
			else
				$filter_query[] .= "payment_method != 'cod'";	
		}
		
		if($request['ordertype'] != -1)
			$filter_query[] .= "ordertype = '".$request['ordertype']."'";
			
		if(!empty($request['warehouse_id']))
			$filter_query[] .= "warehouseid = ".$request['warehouse_id'];
			
		if(!empty($request['from_order_date']))
			$filter_query[] .= "date >= unix_timestamp('".$request['from_order_date']."')";
			
		if(!empty($request['to_order_date']))
			$filter_query[] .= "date <= unix_timestamp('".$request['to_order_date']." 23:59:59')";	
			
		
		if(!isset($objects_per_page)){
			if ($current_area == 'C')
				$objects_per_page = $config[Appearance][products_per_page];
			else
				$objects_per_page = $config[Appearance][products_per_page_admin];
		}
		if(!$objects_per_page)
			$objects_per_page = 30;
		
		$current_page = $_POST['current_page'];
			
		if(!empty($filter_query)) {
			$sql .= " AND ".implode(" AND ", $filter_query);
		}
		$sql .= " order by group_id desc";
		$offset = ($current_page-1)*$objects_per_page;
		$sql .= " LIMIT $offset, $objects_per_page";
		return $sql;	
    }

    function func_create_giftsearch_query($request) {
    	global $current_area, $config, $sql_tbl;
    	$sql = "SELECT orderid, status, login, created_on, updated_on, total as order_total_amount, payment_option FROM gift_cards_orders WHERE payment_option = 'on'";
    
    	$filter_query = array();
    
    	if(!empty($request['order_status']))
    		$filter_query[] .= "status = '".$request['order_status']."'";
    		
    	if(!empty($request['orderid']))
    		$filter_query[] .= "orderid = ".$request['orderid'];
    		
    	if(!empty($request['customer_login']))
    		$filter_query[] .= "login = '".$request['customer_login']."'";
    		
    	if(!empty($request['from_order_date']))
    		$filter_query[] .= "created_on >= unix_timestamp('".$request['from_order_date']."')";
    		
    	if(!empty($request['to_order_date']))
    		$filter_query[] .= "created_on <= unix_timestamp('".$request['to_order_date']." 23:59:59')";
    		
    
    	if(!isset($objects_per_page)){
    		if ($current_area == 'C')
    			$objects_per_page = $config[Appearance][products_per_page];
    		else
    			$objects_per_page = $config[Appearance][products_per_page_admin];
    	}
    	if(!$objects_per_page)
    		$objects_per_page = 30;
    
    	$current_page = $_POST['current_page'];
    		
    	if(!empty($filter_query)) {
    		$sql .= " AND ".implode(" AND ", $filter_query);
    		$sql .= " order by orderid desc";
    		$offset = ($current_page-1)*$objects_per_page;
    		$sql .= " LIMIT $offset, $objects_per_page";
    	}	else {
    		$sql .= " order by orderid desc";
    		$offset = ($current_page-1)*$objects_per_page;
    		$sql .= " LIMIT $offset, $objects_per_page";
    	}
    	return $sql;
    }
}
