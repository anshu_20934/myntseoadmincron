<?php
include_once ("$xcart_dir/include/class/class.feature_gate.keyvaluepair.php");

class PaymentGatewayDiscount {
	
	static $cardsBin = array("CITI" => array('532662','532663','549777','549778','549779','508159','508125','508126','508192','508227','553682','553683','553684','515566','549177'),
						"ICICI" => array('402635','402657','418123','418124','418157','418158','418228','421323','421395','421627','421628','421630','428382','428383','430366','430367','430370','430371','436543','455446','455451','462952','466704','466706','466730','466731','473196','531804','532666','532718','534044','535960','539998',
										'402368','402853','405533','407651','407659','410059','410202','413289','420580','444341','445084','447746','447747','447758','455452','462986','462999','470573','473115','473195','486409','486410','486630','490247','517637','517653','517719','523951','524376','525996','540282','540529','545207','547467','553414')
						);
	
	/*
	 * $checkAmount -- Amount above which discount needs to be applied
	 * $amount -- Amount on which discount is to be applied
	 */
	public static function getDiscount($amount,$checkAmount,$bin) {
		$totalDiscount = 0;
		$citiDiscount = self::calculateCitiDiscount($amount,$checkAmount,$bin);
		$iciciDiscount = self::calculateICICIDiscount($amount,$checkAmount,$bin);
		$totalDiscount = $totalDiscount + $citiDiscount + $iciciDiscount;
		return $totalDiscount;
	}
	
	private static function calculateCitiDiscount($amount,$checkAmount,$bin) {
		$citi_discount = FeatureGateKeyValuePairs::getInteger('payments.citiBankCardDiscount');
		if($citi_discount>0&&$checkAmount>1000) {
			if(in_array($bin, self::$cardsBin['CITI'])) {
				$citiDiscount = ($amount)*$citi_discount/100;
				$citiDiscount = round($citiDiscount, 2);
				return $citiDiscount;
			} else {
				return 0;
			}
		} else {
			return 0;
		}
	}
	
	private static function calculateICICIDiscount($amount,$checkAmount,$bin) {
		$icici_discount = FeatureGateKeyValuePairs::getInteger('payments.iciciBankCardDiscount');
		if($icici_discount>0) {
			if(in_array($bin, self::$cardsBin['ICICI'])) {
				$iciciDiscount = ($amount)*$icici_discount/100;
				$iciciDiscount = round($iciciDiscount, 2);
				return $iciciDiscount;
			} else {
				return 0;
			}
		} else {
			return 0;
		}
	}
	
}
?>