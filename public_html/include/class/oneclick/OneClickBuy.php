<?php
namespace oneclick;

use enums\cart\CartContext;
require_once (\HostConfig::$documentRoot."/include/class/mcart/class.MCart.php");

class OneClickBuy{
	
	private $skuId;
	
	public function getSkuId(){
		return $this->skuId;
	}
	
	public function setSkuId($skuid){
		$this->skuId = $skuid;
	}
	
	public function processOneClickBuy(){
		global $XCARTSESSID, $login;
		
		$mCartFactory = new \MCartFactory();
		
		$oneClickCart = $mCartFactory->getCurrentUserCart(true, CartContext::OneClickContext);
		if ($oneClickCart->isCartNew()){
			$mcartItem= new \MCartNPItem(\MCartUtils::genProductId(),$this->getSkuId(),1);
			try {
				$oneClickCart->addProduct($mcartItem);
			} catch (ProductAlreadyInCartException $e) {
				
			}
		}else{
			$oneClickCart->clear(); 
			$mcartItem= new \MCartNPItem(\MCartUtils::genProductId(),$this->getSkuId(),1);
			try {
				$oneClickCart->addProduct($mcartItem);
			} catch (ProductAlreadyInCartException $e) {
			
			}
		}
		
		\MCartUtils::persistCart($oneClickCart);
		
		func_header_location("$https_location/expressbuy.php");
		
		exit;
		
		
	}
	
	
	
}


?>