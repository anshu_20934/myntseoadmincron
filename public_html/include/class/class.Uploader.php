<?php
/**
 * Uploader
 * @author yogesh
 *
 */
abstract class Uploader{
	protected $host;
	private $autoDisconnectHost;
	private $connOpened=false;
	protected $fileUploaded=false;
	protected function __construct($host,$autoDisconnectHost=true){
		$this->host=$host;
		$this->autoDisconnectHost=$autoDisconnectHost;
	}
	
	public function __destruct(){
		if($this->connOpened == true){
			$this->close();
		}
	}
	
	protected abstract function connectHost();
	
	protected abstract function disconnectHost();
	
	public function connect(){
		if($this->connOpened == false && $this->connectHost()){
			$this->connOpened = true;
		}
		return $this->connOpened;
	}
	
	public function close(){
		if($this->connOpened == true && $this->disconnectHost()){
			$this->connOpened = false;
		}
		return !$this->connOpened;
	}
	
	public function upload($srcfile,$destfile,$callBackFunction=null,$startpos){
		$this->fileUploaded = $this->uploadFile($srcfile, $destfile);
		if($this->autoDisconnectHost){
			$this->close();
		}
		if(function_exists($callBackFunction)) {
			$callBackFunction($this->fileUploaded);
		}
		return $this->fileUploaded;
	}
	public abstract function login($user='',$password='');
	protected abstract function uploadFile($srcfile,$destfile);
}

?>