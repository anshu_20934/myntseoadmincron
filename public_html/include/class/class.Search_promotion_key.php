<?php
use cache\InvalidateXCache;
	class SearchPromotionKey{
		
		const SEARCH_PROMOTION_KEY_CACHE = 'search_promotion_key';
		public static function GetSearchPromotion()
		{			
			$cachedSearchPromotion = func_query("SELECT * FROM search_feature_gate  where has_position =1");
			$cachedValue = array();
			$cachedPosition = array();
			$allPosition = array();
			foreach ($cachedSearchPromotion as $key => $row)
			{
				$cachedValue[$row[field_name]] = $row[field_value];
				$localArray = array();
				if(!empty($row[position]))
				{
					$localArray = explode(',', $row[position]);
				}
				sort($localArray);
				$cachedPosition[$row[field_name]] = $localArray;
				for($i=0;!empty($localArray[$i]);$i++)
				{
					$allPosition[] = $localArray[$i];
				}
			}
			sort($allPosition);
			$cachedSearchParameters = array();
			$cachedSearchParameters['value'] = $cachedValue;
			$cachedSearchParameters['position'] = $cachedPosition;			
			$cachedSearchParameters['AllPosition'] = $allPosition;
			$cachedSearchPromotion = func_query("SELECT * FROM search_feature_gate  where has_position =0");
			foreach ($cachedSearchPromotion as $key => $row)
			{
				$cachedSearchParameters[$row[field_name]] = $row[field_value];
			}
			return $cachedSearchParameters;
		}
		
		public static function refreshKeyValuePairsInCache() {
			$xCacheInvalidator =  InvalidateXCache::getInstance();
			$xCacheInvalidator->invalidateCacheForKey(self::SEARCH_PROMOTION_KEY_CACHE);
			global $xcache ;
			if($xcache==null){
				$xcache = new XCache();
			}
			$cachedSearchParameters = SearchPromotionKey::GetSearchPromotion();
    		$xcache->storeobject(self::SEARCH_PROMOTION_KEY_CACHE, $cachedSearchParameters, 0);
		}
		
    	public static function getAllFeatureGateKeyValuePairs() {
    		global $xcache;
    		if($xcache==null){
				$xcache = new XCache();
			}
			$cachedSearchParameters = $xcache->fetchobject (self::SEARCH_PROMOTION_KEY_CACHE);
  			if($cachedSearchParameters == NULL) {
    			 $cachedSearchParameters = SearchPromotionKey::GetSearchPromotion();
    			 $xcache->storeobject(self::SEARCH_PROMOTION_KEY_CACHE, $cachedSearchParameters, 0);
    		}
    		return $cachedSearchParameters;
    	}
		public static function getFeatureGateKeyValuePairs($keyParam) {
    		global $xcache;
    		if($xcache==null){
				$xcache = new XCache();
			}
			$cachedSearchParameters = $xcache->fetchobject (self::SEARCH_PROMOTION_KEY_CACHE);
  			if($cachedSearchParameters == NULL) {
    			 $cachedSearchParameters = SearchPromotionKey::GetSearchPromotion();
    			 $xcache->storeobject(self::SEARCH_PROMOTION_KEY_CACHE, $cachedSearchParameters, 0);
    		}
    		return $cachedSearchParameters[$keyParam];
    	}
	}

?>