<?php
namespace vtr\client;
require_once \HostConfig::$documentRoot."/exception/MyntraException.php";
/**
 * VTR Service Client
 * @author kundan
 *
 */
abstract class StyleState {
	const ENABLED = "enabled";
	const DISABLED = "disabled";
	const ALL = "all";
}
class VirtualTrialRoomClient {
	const BlockSize = 400;
	
	/**
	 * 
	 * Disable a comma separated list of style-ids from VTR
	 * Note: this only does it from VTR-Service. We still need to do changes at Solr Document level for these style-ids
	 * @param unknown_type $styleIds
	 */
	public static function disableStyles($styleIds, $enable = false){
		global $portalapilog;
		if(empty($styleIds)){
			throw new \MyntraException("Enable/Disable Styles should have style-ids passed");
		}
		if(is_array($styleIds)){
			$styleIds = implode(",", $styleIds);
		}
		$headers = array();
		array_push($headers, 'Accept: application/json');
		array_push($headers, 'Content-Type: application/json');
		$url = \HostConfig::$vtrServiceHost;
		$serviceUrl = ($enable)? "/vtr/enable-indexing" : "/vtr/disable-indexing"; 
		$url .= $serviceUrl;
		
		$portalapilog->debug("Enabling/Disabling Styles on VTR");
		$restRequest = new \RestRequest($url, 'POST',$styleIds);
		$restRequest ->setHttpHeaders($headers);
		$restRequest ->execute();
		$responseInfo = $restRequest ->getResponseInfo();
		$responseBody = $restRequest ->getResponseBody();
		if(!$responseBody || isset($responseBody->status) || isset($responseBody->message) || $responseInfo['http_code'] == 500) {
			//error logging here
			$portalapilog->error('Enable/Disable Style on VTR failed:--'.print_r($restRequest,true));
			return "ERROR";
		}
		return $responseBody;
	}
	
	public static function getStyles($type="all"){
		global $portalapilog;
		if($type) {
			$type = trim(strtolower($type));
		}
		$headers = array();
		array_push($headers, 'Accept:  application/json');
		array_push($headers,'Content-Type: application/json');
		if($type === "enabled"){
			$url = \HostConfig::$vtrServiceHost."/vtr/styles";
			array_push($headers,'enabledOnly: true');
		}
		elseif ($type === "disabled"){
			$url = \HostConfig::$vtrServiceHost."/vtr/disabled-styles";
		}else {//if type is anything else, assume the type is: all
			$type = "all";
			$url = \HostConfig::$vtrServiceHost."/vtr/styles";
			array_push($headers,'enabledOnly: false');
		}
		
		$portalapilog->debug("Getting ".$type." Styles on VTR");
		$restRequest = new \RestRequest($url, 'GET');
		$restRequest ->setHttpHeaders($headers);
		$restRequest ->execute();
		$responseInfo = $restRequest ->getResponseInfo();
		$responseBody = $restRequest ->getResponseBody();
			
		if(!$responseBody || isset($responseBody->status) || isset($responseBody->message) || $responseInfo['http_code'] == 500) {
			//error logging here
			$portalapilog->error('Get Styles '.$type.' on VTR:--'.print_r($restRequest,true));
		}
		$currObject = json_decode($responseBody);
		$currArr = convertToArrayRecursive($currObject);
		return $currArr;
	}
	
	/**
	 * @param unknown_type $styleIds
	 */
	public static function getStyleDataForIndexing($styleIds){
		global $portalapilog;
		$url = \HostConfig::$vtrServiceHost."/vtr/styledata";
		$headers = array();
		array_push($headers, 'Accept:  application/json');
		array_push($headers,'Content-Type: application/json');
		$returnArr = array();
		$blockSize = self::BlockSize;
		
		if(empty($styleIds)) { 
			$styleIds = self::getStyles(StyleState::ENABLED);
		}
		
		$styleArr = is_array($styleIds) ? $styleIds : explode(",",$styleIds); 
		
		for($i=0;$i<sizeof($styleArr);$i+=$blockSize){
			$currArr = null;
			$currStyleSet = implode(",", array_slice($styleArr, $i, $blockSize));
			$portalapilog->debug("Getting VTR Data for Solr for styles:$currStyleSet");
			$restRequest = new \RestRequest($url, 'POST', $currStyleSet);
			$restRequest ->setHttpHeaders($headers);
			$restRequest ->execute();
			$responseInfo = $restRequest ->getResponseInfo();
			$responseBody = $restRequest ->getResponseBody();
			
			if(!$responseBody || isset($responseBody->status) || isset($responseBody->message) || $responseInfo['http_code'] == 500) {
				//error logging here
				$portalapilog->error('Get VTR Data for Solr:--'.print_r($restRequest,true));
			}
			$currObject = json_decode($responseBody);
			$currArr = convertToArrayRecursive($currObject);
			$returnArr = array_combine(array_merge(array_keys($returnArr),array_keys($currArr)), array_merge(array_values($returnArr),array_values($currArr)));
			unset($currArr);
		}
		
		return $returnArr;
	}
}
