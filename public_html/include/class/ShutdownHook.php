<?php

class ShutdownHook{
	/**
	 * Precedence 1 should be given to critical function like session save etc
	 * Precedence 2 yet to be allocated
	 * Precedence 3 should be given to cleanup functions
	 * Precedence 4 should be given to profilers
	 * Precedence 5 should be given to error handlers 
	 */
	const PRECEDENCE_1=1;
	const PRECEDENCE_2=2;
	const PRECEDENCE_3=3;
	const PRECEDENCE_4=4;
	const PRECEDENCE_5=5;
	
	
	private static $hooks = array(1=>null,2=>null,3=>null,4=>null,5=>null);
	/**
	 * 
	 * Add to shutdown hook
	 * @param string or array $hook
	 * @param integer 1-5 $precedence
	 */
	public static function addHook($hook,$precedence = 1){
		self::$hooks[$precedence][] = $hook;
	}
	
	public static function runShutdownHooks(){
		//print_r(error_get_last());
		if($_GET["showerror"]=="true" && \HostConfig::$configMode!="prod"){
			print_r(error_get_last());
		}
		foreach (self::$hooks as $hookArray){
            if (empty($hookArray)) continue;
			foreach ($hookArray as $hook){
				if(is_array($hook)){
					call_user_func($hook);
				}else{
					$hook();
				}
			}
		}
	}
	
}

register_shutdown_function(array(ShutdownHook,'runShutdownHooks'));
