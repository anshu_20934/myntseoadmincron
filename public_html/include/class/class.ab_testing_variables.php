<?php

	class ABTestingVariables {
		
		private static $map = null;
		
		const ABTESTING_VARIABLES_CACHE = 'abtesting_variables_cache';
		
		public static function refreshAllInCache() {
			global $xcache ;
			if($xcache == null){
				$xcache=new XCache();
			}
			$resultSet = func_query("SELECT t.id tid,t.name tname,t.enabled tenabled,t.completed tcompleted,t.api_version tapi_version,v.id vid,t.source_type tsource_type,
			t.seg_algo tseg_algo,t.ga_slot tga_slot,t.omni_slot as tomni_slot, t.filters tfilters,v.name vname,v.percent_probability vp_prob,
			v.inline_html vinline_html,v.final_variant vfinal_variant,v.jsFile,v.cssFile,v.config_json,v.algo_config_json
			 FROM mk_abtesting_tests t,mk_abtesting_variations v where v.ab_test_id=t.id and t.completed=0");
			$cachedVars=ABTestingVariables::buildArray($resultSet);
			$xcache->store(ABTestingVariables::ABTESTING_VARIABLES_CACHE, $cachedVars, 0);
			return $cachedVars;
		}
		
    	public static function getAllTestVariables() {
    		global $xcache;
		 if(self::$map== null){

	    		if($xcache == null){
				$xcache=new XCache();
			}
			$cachedVars = $xcache->fetch(ABTestingVariables::ABTESTING_VARIABLES_CACHE);
  			if($cachedVars == NULL) {
    			 $cachedVars = ABTestingVariables::refreshAllInCache();
    			}
			self::$map = $cachedVars;
		}
    		 return self::$map;
    	}
    	
		public static function getTestVariables($test) {
			
			if(self::$map== null){
				global $xcache ;
				if($xcache == null){
					$xcache=new XCache();
				}
				$cachedVars = $xcache->fetch(ABTestingVariables::ABTESTING_VARIABLES_CACHE);
				if($cachedVars == NULL) {
					$cachedVars = ABTestingVariables::refreshAllInCache();
				}
				self::$map = $cachedVars;
			}
    		return self::$map[$test];
      	}
    	
    	
    	public static function buildArray($resultSet){
    		$array=array();
    		foreach ($resultSet as $row){
    			$array[$row['tname']]['name']=$row['tname'];
    			$array[$row['tname']]['enabled']=$row['tenabled'];
    			$array[$row['tname']]['id']=$row['tid'];
    			$array[$row['tname']]['filters']=$row['tfilters'];
    			$array[$row['tname']]['ga_slot']=$row['tga_slot'];
    			$array[$row['tname']]['omni_slot']=$row['tomni_slot'];
    			$array[$row['tname']]['seg_algo']=$row['tseg_algo'];
    			$array[$row['tname']]['completed']=$row['tcompleted'];
    			$array[$row['tname']]['apiVersion']=$row['tapi_version'];
    			$array[$row['tname']]['source_type']=$row['tsource_type'];
    			$array[$row['tname']]['variations'][$row['vname']]['name']=$row['vname'];
    			$array[$row['tname']]['variations'][$row['vname']]['id']=$row['vid'];
    			$array[$row['tname']]['variations'][$row['vname']]['p_prob']=$row['vp_prob'];
    			$array[$row['tname']]['variations'][$row['vname']]['inline_html']=$row['vinline_html'];
    			$array[$row['tname']]['variations'][$row['vname']]['final_variant']=$row['vfinal_variant'];
    			$array[$row['tname']]['variations'][$row['vname']]['jsFile']=$row['jsFile'];
    			$array[$row['tname']]['variations'][$row['vname']]['cssFile']=$row['cssFile'];
    			if(!empty($row['config_json'])){
    				$array[$row['tname']]['variations'][$row['vname']]['config']=self::convertToArrayRecursive(json_decode($row['config_json']));
    			}
    			if(!empty($row['algo_config_json'])){
    				$array[$row['tname']]['variations'][$row['vname']]['algo_config']=self::convertToArrayRecursive(json_decode($row['algo_config_json']));
    			}
    		}
    		return $array;
    	}
    	
    	public static function convertToArrayRecursive($array){
    		if (is_object($array)) {
    			// Gets the properties of the given object
    			// with get_object_vars function
    			$array = get_object_vars($array);
    		}

    		if (is_array($array)) {
    			/*
    			 * Return array converted to object
    			* Using __FUNCTION__ (Magic constant)
    			* for recursive call
    			*/
    			return array_map(array(self, 'convertToArrayRecursive'), $array);
    		}
    		else {
    			// Return array
    			return $array;
    		}
    	}
	}
	
?>
