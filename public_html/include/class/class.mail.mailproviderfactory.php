<?php

require_once "$xcart_dir/include/class/class.mail.abstractmailserviceprovider.php";
require_once "$xcart_dir/include/class/class.mail.amazonsesmailer.php";
require_once "$xcart_dir/include/class/class.mail.myntramailer.php";
require_once "$xcart_dir/include/class/class.feature_gate.keyvaluepair.php";

/**
 * This class gives an instance of the Mail Service Provider class, given the provider
 * @author kundan
 */
class MailProviderFactory {
	
	const FG_DEFAULT_EMAIL_PROVIDER_KEY = "DefaultEmailProvider";
	const FG_BACKUP_EMAIL_PROVIDER_KEY = "BackupEmailProvider";
	
	static $initialized = false;
	
	static $defaultProvider = "myntra";
	
	static $backupProvider = "ses";
	
	static $amazonSESMailer, $myntraMailer, $MailProviderFactories;
	/**
	 * Which email provider to use to send email: e.g. ses, icubes, myntra
	 * @param unknown_type $provider
	 */
	public static function setDefaultProvider($provider) {
		self::$defaultProvider = strtolower($provider);
	}
	
	/**
	 * In case default email provider fails to send email, which backup provider to use? 
	 * @param unknown_type $provider
	 */
	public function setBackupProvider($provider) {
		self::$backupProvider = strtolower($provider);
	}
	
	public static function getDefaultProvider() {
		self::init();
		$defaultProviderFG = strtolower(FeatureGateKeyValuePairs::getFeatureGateValueForKey(self::FG_DEFAULT_EMAIL_PROVIDER_KEY));
		if(!empty($defaultProviderFG) && array_key_exists($defaultProviderFG, self::$MailProviderFactories)) {
			self::setDefaultProvider($defaultProviderFG);
		}
		return self::$MailProviderFactories[self::$defaultProvider];
	}
	
	public static function getBackupProvider() {
		self::init();
		$backupProviderFG = strtolower(FeatureGateKeyValuePairs::getFeatureGateValueForKey(self::FG_BACKUP_EMAIL_PROVIDER_KEY));
		if(!empty($backupProviderFG) && array_key_exists($backupProviderFG, self::$MailProviderFactories)) {
			self::setBackupProvider($backupProviderFG);
		}
		return self::$MailProviderFactories[self::$backupProvider];
	}
	/**
	 * this should be called before any other method
	 */
	public static function init() {
		if(self::$initialized) {
			return;
		}
		self::$amazonSESMailer = new AmazonSESMailer();
		self::$myntraMailer = new MyntraMailer();
		self::$MailProviderFactories = array("ses"=> self::$amazonSESMailer, "myntra"=>self::$myntraMailer);			
		//TODO: add other provider classes in future
		self::$initialized = true;
	}
	
	/**
	 * Get mail provider based on provider-name.can be blank, in which case it would use default
	 * @param unknown_type $providerName
	 */
	public static function getMailProvider($providerName) {
		self::init();
		//first get the provider as per the given provider name.  
		$provider = self::$MailProviderFactories[$providerName];
		if(empty($providerName) || empty($provider)) {
			//given provider name is empty or no implementation exists for given provider name, so get default provider
			$provider = self::getDefaultProvider();
			if(empty($provider)) {
				//default provider is also non-existent, so 
				$provider = self::getBackupProvider();
			}
		}	
		return $provider;
	}
}
?>