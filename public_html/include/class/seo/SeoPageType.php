<?php
namespace seo;
/**
  * the page families supported
  */
abstract class SeoPageType {
    const STATIC_PAGE = 1;
    const SEARCH_PAGE = 2;
    const PDP_PAGE    = 3;
}

?>
