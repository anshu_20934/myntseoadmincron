<?php 
namespace seo;

class SeoPagedataBodyWidget extends SeoPagedataWidget {
    
    const VIEW = "view/body_text.tpl";

    public function __construct($dao, $map) {
        parent::__construct($dao, $map);
    }

    public function render() {
        $tags = $this->_data->getBodyText();
       // echo "<PRE>";print_r($tags);exit;
        foreach ($tags as $tag => $template) {
            $this->_renderer->assign($tag, $this->replacePropertyTokens($template));
        }

        return $this->_renderer->fetch('file:'.__DIR__.'/'.self::VIEW);
    }

}

?>
