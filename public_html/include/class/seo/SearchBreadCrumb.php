<?php

namespace seo;

include_once("$xcart_dir/include/class/widget/class.widget.keyvaluepair.php");

class SearchBreadCrumb extends SeoBreadCrumb {

    public function getData($productData) {
        $rule = \WidgetKeyValuePairs::getWidgetValueForKey('searchBreadCrumbRuleInJson');
        return $this->generateBreadCrumb($productData, $rule);
    }

}

?>