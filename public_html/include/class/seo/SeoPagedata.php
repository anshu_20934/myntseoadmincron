<?php
namespace seo;

class SeoPagedata {
    private $_dao;
    private $_pagedata;
    private $_defaultPagedata;
    private $_logger;

    private function init() {
        $this->_dao = new SeoDAO();
        $this->_logger = $GLOBALS['seolog']; 
    }

	public function __construct ($macros) {
        if (empty($macros)) {
            throw new Exception("Empty macros");
        }

        //Feature Gate for reading SEO data from DB
        global $xcart_dir;
        include_once($xcart_dir."/include/class/class.feature_gate.keyvaluepair.php");
        $seoAdminDBenabled=\FeatureGateKeyValuePairs::getBoolean('seoAdminDBenabled');
        //TODO: Not a good idea to fallback on DB.
        if ($seoAdminDBenabled == 'true'){
            $this->init();
            $this->_pagedata = $this->_dao->findOnePagedata($macros);
        }
        else {            
            include_once($xcart_dir."/include/class/seo/SeoAdminCache.php");
            $hash1 = SeoHashUtil::toMacroHash($macros);
            $hash2 = SeoHashUtil::toHash($macros->url, $macros->page_type);
            $hash3 = SeoHashUtil::toHashGroup($macros->page_type);
            //Read from Redis
            \SeoAdminCache::init();
            $this->_pagedata = \SeoAdminCache::getDataFromCache($hash1, $hash2, $hash3);
        }
	}

	public function getBaseTags() {
        if (empty($this->_pagedata)) {
            return null;
        }
        return array(   'title' => $this->_pagedata['head_title'],
                        'keywords' => $this->_pagedata['head_meta_keywords'],
                        'description' => $this->_pagedata['head_meta_description'],
                );
	}
    public function getBaseTitle(){
        if (empty($this->_pagedata)) {
            return null;
        }
        return $this->_pagedata['head_title'];
    }

	public function getOGTags() {
        if (empty($this->_pagedata)) {
            return null;
        }
        return array(   "og_image" => $this->_pagedata['head_meta_og_image'],
                        "og_title" => $this->_pagedata['head_meta_og_title'],
                        "og_url" => $this->_pagedata['head_meta_og_url'],
                        "og_description" => $this->_pagedata['head_meta_og_description'],
                );
	}

    public function getTwitterTags(){
        return array(
                        "twitter_card" => $this->_pagedata['head_meta_twitter_card'],
                        "twitter_url" => $this->_pagedata['head_meta_twitter_url'],
                        "twitter_title" => $this->_pagedata['head_meta_twitter_title'],
                        "twitter_description" => $this->_pagedata['head_meta_twitter_description'],
                        "twitter_image" => $this->_pagedata['head_meta_twitter_image'],
            );
    }
    public function getBodyText(){
        return array(
                        "page_title" => $this->_pagedata['page_title'],
                        "page_description" => $this->_pagedata['page_description'],
            );
    }
    public function getpageTitle(){
        return $this->_pagedata['page_title'];               
	}
    public function getRuleUrl() {
         return $this->_pagedata['url'];
    }
}
?>
