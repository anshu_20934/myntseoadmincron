<?php

namespace seo;

abstract class SeoBreadCrumbKeyEnum {

	//const __default = self::Home;

    const Home = "h";
    const MasterCategory = "mc";
    const SubCategory = "sc";
    const ArticleType = "at";
    const ProductName = "pn";
    const Brand = "b";
    const Gender = "g";

}

?>