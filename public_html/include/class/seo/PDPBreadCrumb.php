<?php

namespace seo;

include_once("$xcart_dir/include/class/widget/class.widget.keyvaluepair.php");

class PDPBreadCrumb extends SeoBreadCrumb {

    public function getData($productData) {
    	$rule = \WidgetKeyValuePairs::getWidgetValueForKey('pdpBreadCrumbRuleInJson');
        return $this->generateBreadCrumb($productData, $rule);
    }
}

?>