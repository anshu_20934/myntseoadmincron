<?php 
namespace seo;
class SeoPagedataOGWidget extends SeoPagedataWidget {
    
    const VIEW = "view/og.tpl";

    public function __construct($dao, $map) {
        parent::__construct($dao, $map);
    }

    public function render() {
        $tags = $this->_data->getOGTags();
        foreach ($tags as $tag => $template) {
            $this->_renderer->assign($tag, $this->replacePropertyTokens($template));
        }

        return $this->_renderer->fetch('file:'.__DIR__.'/'.self::VIEW);
    }

}

?>
