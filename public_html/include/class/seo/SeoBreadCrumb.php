<?php

namespace seo;

include_once("$xcart_dir/include/class/widget/class.widget.keyvaluepair.php");

class SeoBreadCrumb {
	
	private $synonyms = null;
	
	protected function generateBreadCrumb($productData, $rule){

		/*
			$productData is of the structure key => value where key is the BreadCrumbKeyEnum and value the current value for the key
			$rule is an array where the position refers to the breadCrumbKey, which needs to be replaced by the value from productData
		*/

		$synonymInJson = \WidgetKeyValuePairs::getWidgetValueForKey('breadCrumbSynonymList');
		$this->synonyms = json_decode($synonymInJson, true);

        $breadCrumbArr = array();
        $i=0;
        $rule=json_decode($rule);
        
        if($rule == null){ // fall back on the default one
        	$rule = $rule = '["'.SeoBreadCrumbKeyEnum::Home.'","'.SeoBreadCrumbKeyEnum::MasterCategory.'","'.SeoBreadCrumbKeyEnum::Gender.'-'.SeoBreadCrumbKeyEnum::MasterCategory.'","'.SeoBreadCrumbKeyEnum::ArticleType.'","'.SeoBreadCrumbKeyEnum::Brand.'-'.SeoBreadCrumbKeyEnum::ArticleType.'","'.SeoBreadCrumbKeyEnum::ProductName.'"]';
        	$rule = json_decode($rule);
        }

        $productTitle = trim($productData[SeoBreadCrumbKeyEnum::ProductName]);
        $showProductTitle = true;
        foreach ($rule as $breadCrumbEnums) {
            $bcs = explode("-", $breadCrumbEnums);
            $dn = array(); $url = array(); $dnWithoutSynonym = array();
            $continue = false;
            foreach ($bcs as $breadCrumbEnum) {
                if($breadCrumbEnum == SeoBreadCrumbKeyEnum::Home){
                    array_push($dn, 'Home');
                    array_push($url, '');
                } else {
                    $data = $productData[$breadCrumbEnum];
                    if(empty($data)) { $continue=true; break;}
                    if(!empty($this->synonyms[strtolower($data)]))
                        $data = $this->synonyms[strtolower($data)];
                    array_push($dn, $data);
                    array_push($dnWithoutSynonym, $productData[$breadCrumbEnum]);
                    if($breadCrumbEnum!=SeoBreadCrumbKeyEnum::ProductName)
                        array_push($url, implode("-",explode(" ",str_replace("'","",$data))));
                }
            }

            if($continue) continue;
            $finalDisplayName = implode(" ", $dn);
            $finalOriginalDisplayName = implode(" ", $dnWithoutSynonym);
            if(!empty($finalDisplayName) && SeoBreadCrumbKeyEnum::ProductName!=$breadCrumbEnums && strcasecmp($finalOriginalDisplayName, $productTitle)==0)   
                    $showProductTitle=false;

            if(SeoBreadCrumbKeyEnum::ProductName==$breadCrumbEnums && !$showProductTitle) break;

            $className = get_class($this);
            $label = null;
            if(stripos($className,"Search") === false)
                    $label = "PDP_breadcrumb_click";
            else
                    $label = "List_breadcrumb_click"; 

            if(!empty($url)) {
                $burl = strtolower(implode("-", $url)) . '?s=bc"';
                $bcrumb = <<<MARKUP
                <div class="bread-crumb" itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                    <a itemprop="url" href="/{$burl}" onclick="_gaq.push(['_trackEvent', '{$label}', '{$finalDisplayName}'])">
                        <span itemprop="title">{$finalDisplayName}</span>
                    </a>
                </div>
MARKUP;
                $breadcrumbArr[$i++] = $bcrumb;
            }
            else if(!empty($dn)) {
                $breadcrumbArr[$i++] = $finalDisplayName;
            }
        }
        $breadCrumb = implode(" <span class='sep'> / </span> ", $breadcrumbArr);

        return $breadCrumb;        
	}

}
	
?>
