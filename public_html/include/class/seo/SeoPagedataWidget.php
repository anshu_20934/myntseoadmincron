<?php 
namespace seo;

abstract class SeoPagedataWidget {
    
    /**
     * SeoPagedata access common to all
     * widgets of this type
     */
    protected $_data;

    /**
     * property-map from the request page
     */
    private $_map;


    /**
     * logger common to all widgets
     */
    protected $_logger;

    /**
     * renderer common to all widgets
     */
    protected $_renderer;

    private function init() {
        $this->_logger = $GLOBALS['seolog'];
        $this->_renderer = $GLOBALS['smarty']; 
    }


    public function __construct($dao, $map) {
        $this->init();
        $this->_data = $dao;
        $this->_map = $map->toArray();
    }

    /*
     * @template : rule set for tag
     *
     * replace all the tokens set in @template to matching
     * values of the property map
     */
    protected function replacePropertyTokens($template) {
        //check if this template contains tokens at all
        if (false === strpos($template, "{#")) {
            return $template;
        }

        $tokens = preg_split('/({#[a-zA-Z0-9_]+})/i', $template, -1, PREG_SPLIT_DELIM_CAPTURE);

        foreach ($tokens as $key => $value) {
           if(strpos($value,'{#') === 0 ){
                $value = substr($value,2,-1);
                if (isset($this->_map[$value]))
                    $tokens[$key] = $this->formatPropertyToken($value,$this->_map[$value]);
                else unset($tokens[$key]);
           }
        }

        $new_template = implode('', $tokens);

        return $new_template;
    }

    private function formatPropertyToken($key,$value){
        switch($key){
            case 'search_string' :   $fvalue =  ucwords(str_replace('-', ' ', $value));
                break;
            case 'category' :        $fvalue =  ucwords(str_replace('-', ' ', $value));
                break;
            case 'sub_category' :    $fvalue =  ucwords(str_replace('-', ' ', $value));
                break;
            case 'article_type' :    $fvalue =  ucwords(str_replace('-', ' ', $value));
                break;
            case 'brand' :           $fvalue =  ucwords(str_replace('-', ' ', $value));
                break;
            case 'gender' :          $fvalue =  ucwords($value);
                break;
            case 'colour' :          $fvalue =  ucwords($value);
                break;
            case 'size' :            $fvalue =  ucwords($value);
                break;
            case 'name' :            $fvalue =  ucwords($value);
                break;
            case 'page_name' :       $fvalue =  ucwords($value);
                break;
            case 'price' :           $fvalue =  number_format(round($value),0,'.',',');
                break;
            case 'discount' :        $fvalue =  number_format(round($value),0,'.',',');
                break;
            case 'discounted_price' : $fvalue =  number_format(round($value),0,'.',',');
                                        if(!$fvalue) $fvalue = '';
                break;
            case 'discount_string' : $fvalue =  ucwords(strip_tags($value));
                break;
            default:            $fvalue = $value;
                break;
        }
            if($fvalue ===  null) $fvalue = '';

            return $fvalue;
    }

    abstract function render();
}


?>
