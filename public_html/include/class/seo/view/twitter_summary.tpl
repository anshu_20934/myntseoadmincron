        <meta name="twitter:card"        content="summary">
        <meta name="twitter:url"         content="{include file='string:{$twitter_url}'}">
        <meta name="twitter:title"       content="{include file='string:{$twitter_title}'}">
        <meta name="twitter:description" content="{include file='string:{$twitter_description}'}">
        <meta name="twitter:image"       content="{include file='string:{$twitter_image}'}">
