 {if $page_description}
 	<div id="mk-search-description">
		<h2>About {include file='string:{$page_title}'}</h2>
		<p>{$page_description|unescape}</p>
	</div>
{/if}
