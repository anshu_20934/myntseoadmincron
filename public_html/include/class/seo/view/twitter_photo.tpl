<meta name="twitter:card"            content="photo">
<meta name="twitter:site"            content="{include file='string:{$twitter_site}'}">
<meta name="twitter:creator"         content="{include file='string:{$twitter_creator}'}">
<meta name="twitter:url"             content="{include file='string:{$twitter_url}'}">
<meta name="twitter:title"           content="{include file='string:{$twitter_title}'}">
<meta name="twitter:description"     content="{include file='string:{$twitter_description}'}">
<meta name="twitter:image"           content="{include file='string:{$twitter_image}'}">
<meta name="twitter:image:width"     content="{include file='string:{$twitter_image_width}'}">
<meta name="twitter:image:height"    content="{include file='string:{$twitter_image_height}'}">
