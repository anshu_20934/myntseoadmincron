        <meta property="og:image"        content="{include file='string:{$og_image}'}">
        <meta property="og:title"        content="{include file='string:{$og_title}'}">
        <meta property="og:url"          content="{include file='string:{$og_url}'}">
        <meta property="og:description"  content="{include file='string:{$og_description}'}">
