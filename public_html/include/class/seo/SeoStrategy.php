<?php
namespace seo;
/**
 * Blueprint to control every aspect of SEO-enhancing publication
 * on specific pages or a pattern-matching family. Under this model,
 * pages are distinguished based on the special {hash} string 
 * generated based on {url} and other page specifics.
 *
 * @category seo
 * @package  seo
 * @author   Rahul Jaimini<rahul.jaimini@myntra.com>
 */
class SeoStrategy { 
    /**
     *@type: SeoPageType
     */
    private $_type;

    /**
     * widgets set on this url
     */
    private $_widgets;


    /**
     * the one point access to data configured in admin
     */
    private $_data;

    /**
     * reference to the view controller
     */
    private $_render;

    /**
     * reference to the error logger
     */
    private $_logger;

    /**
     * to dynamically resolve params set by admin
     * for a url or group
     * NOTE: no bad entries expected
     */
    private $_macros;


    private function init() {
        $this->_widgets = new \stdClass;
        $this->_render = $GLOBALS['smarty'];
        $this->_logger = &$GLOBALS['seolog']; 
    }


    public function __construct ($macros) {
        if (empty($macros)) {
            throw new Exception("macros missing");
        }
        $this->_macros = $macros;
        $this->init();
        try {
           $this->_data = new SeoPagedata($this->_macros);
        } catch (Exception $e) {
            $this->_logger->error("SeoStrategy :: ".$e->getMessage());
            throw new Exception($e->getMessage());
        }

        self::populateWidgets();
    }

    private function populateWidgets() {
        $widget = new SeoPagedataBaseWidget($this->_data, $this->_macros);
        $this->_widgets->baseWidget = $widget->render();
        if($this->getBaseTitle){
            $widget = new SeoPagedataOGWidget($this->_data, $this->_macros);
            $this->_widgets->ogWidget = $widget->render();
            $widget = new SeoPagedataTwitterWidget($this->_data, $this->_macros);
            $this->_widgets->twitterWidget = $widget->render();
        }
        $widget = new SeoPagedataBodyWidget($this->_data, $this->_macros);
        $this->_widgets->bodyWidget = $widget->render();
    }

    public function getBaseWidget() {
        return $this->_widgets->baseWidget;
    }

    public function getOGWidget() {
        return $this->_widgets->ogWidget;
    }

    public function getTwitterWidget() {
        return $this->_widgets->twitterWidget;
    }

    public function getBodyWidget() {
        return $this->_widgets->bodyWidget;
    }
    public function getH1Title() {
        return $this->_data->getpageTitle();
    }
     
    public function getRule() {
	return $this->_data->getRuleUrl();
    }	

    public function getBaseTitle(){
        return $this->_data->getBaseTitle();
    }
}
?>

