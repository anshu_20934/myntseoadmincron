<?php
global $xcart_dir;
include_once($xcart_dir."/Cache/ManagedTableCache.php");
include_once($xcart_dir."/Cache/CacheKeySet.php");

class SeoAdminCache {
    private static $seoAdminCache;
	private static $seoBlackListWordCache;
    
    public static function init($expiry) {
        self::$seoAdminCache = new ManagedTableCache(function()  {
            return SeoAdminCache::getSeoAdminData(null);
        }, \SEOAdminCacheKeys::$prefix , $expiry);
		
        self::$seoBlackListWordCache = new ManagedTableCache(function()  {
            return SeoAdminCache::getSeoBlackListedWords(null);
        }, \SEOAdminCacheKeys::$prefix , $expiry);
    }

    public static function getSeoAdminData($ignore) {    
       $query = "select pr.pagehash,pbw.page_title,pbw.page_description,pw.*, pr.score, pr.url,pr.canonical,ic.title ic_title,ic.sub_title ic_sub_title,ic.content ic_content from adseo_pagerule pr inner join adseo_pagewidget pw on pw.pagerule_id = pr.id left join adseo_pagebodywidget pbw on pbw.pagerule_id = pr.id left join adseo_inlinecontent ic on ic.pagerule_id = pr.id where pr.is_active = 1"; 
        $data = func_query($query);
        $table = array();
        foreach ($data as $row) {
            $table[$row['pagehash']] = $row;
        }
        return $table;
    }
	
    public static function getSeoBlackListedWords($ignore) {    
        $query = "select blackListedWord from adseo_blacklistedword where is_active = 1";        
        $data = func_query_column($query);
        $table = array();
        $table['blackListedWord'] = $data;
        return $table;
    }

    public static function setCacheForAdminData(){
        self::$seoAdminCache->populateCache();
		self::$seoBlackListWordCache->populateCache();     
    }

    public static function getDataFromCache($key1, $key2, $key3){
        $data = self::$seoAdminCache->getMultiple($key1, $key2, $key3);
        $pagedata = array();
        $maxscore = 0;
        foreach ($data as $row){
            if(!empty($row)){                
                if($row['score'] >= $maxscore){
                    $pagedata = $row;
                    $maxscore = $row['score'];
                }
            }
        } 
        return $pagedata;
    }
    
    public static function getDataForHash($urlHash) {
        $data = self::$seoAdminCache->get($urlHash);
        if(!empty($data)) {
            return $data;
        }
        return false;
    }
}

?>
