<?php
namespace seo;

class SeoPropertyMapKeys {
    public static $keys = array (
        SeoPageType::STATIC_PAGE    => 'url',
        SeoPageType::SEARCH_PAGE    => 'count,url',
        SeoPageType::PDP_PAGE       => 'id, name, image, price, discount',
    );
}
?>
