<?php
namespace seo;

class SeoDAO implements ISeoDAO {

    public function findOnePagedata($macros) {
        $query = "select pbw.page_title,pbw.page_description,pw.*, pr.score from adseo_pagerule pr inner join adseo_pagewidget pw ".
                    "on pw.pagerule_id = pr.id left join adseo_pagebodywidget pbw on pbw.pagerule_id = pr.id where ".
                    "pr.is_active = 1 and ".
                    "pr.pagehash in ('" .SeoHashUtil::toMacroHash($macros)."', '".
                                        SeoHashUtil::toHash($macros->url, $macros->page_type)."', '".
                                        SeoHashUtil::toHashGroup($macros->page_type)."') ".
                    "order by pr.score desc";
        
        return (func_query_first($query));
    }
}

?>
