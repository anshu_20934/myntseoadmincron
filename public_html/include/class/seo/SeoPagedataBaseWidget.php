<?php 
namespace seo;

class SeoPagedataBaseWidget extends SeoPagedataWidget {
    
    const VIEW = "view/base.tpl";

    public function __construct($dao, $map) {
        parent::__construct($dao, $map);
    }

    public function render() {
        $tags = $this->_data->getBaseTags();
        foreach ($tags as $tag => $template) {
            $this->_renderer->assign($tag, $this->replacePropertyTokens($template));
        }

        return $this->_renderer->fetch('file:'.__DIR__.'/'.self::VIEW);
    }

}

?>
