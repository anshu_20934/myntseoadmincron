<?php

class InstockEvent {
	public static $_EVENT_ID='INSTOCK_EVENT';
	private $sku;
	private $quantity;
	private $jit=false;
	
	public function setJIT($jit){
		$this->jit=$jit;
	}
	public function isJIT(){
		return $this->jit;
	}
	public function setSku($sku){
		$this->sku=$sku;
	}
	public function getSku(){
		return $this->sku;
	}
	public function setQuantity($quantity){
		$this->quantity=$quantity;
	}
	public function getQuantity(){
		return $this->quantity;
	}
	public function getType(){
		NotificationHandlerFactory::$_NOTIFY_TYPE_SKU_INSTOCK;
	}
}

class Notifications{
	public static $_CUSTOMER_INSTOCK_NOTIFYME="customernotifyme.instock";
}

?>
