<?php

require_once "$xcart_dir/include/class/class.mail.abstractmailserviceprovider.php";

class MyntraMailer extends AbstractMailServiceProvider {
	
	/**
	 * @param unknown_type $mail_detail: this should contain as key=>value:
	 * to, cc, bcc, subject, content, from_name, from_email, from_designation (optional),
	 * template = name of template: if this key is used, then subject, content, from_name, from_designation
	 * are overriden from the template which is fetched from Database
	 *
	 * @see include/class/AbstractMailServiceProvider::sendMail()
	 */
	public function sendMail($mail_detail) {
		global $maillog;
		$ret = send_mail_on_domain_check($mail_detail);
		if($ret) {
			$maillog->info(__FILE__.__FUNCTION__.":Mail sent to queue via Myntra");
		}
		else {
			$maillog->error(__FILE__.__FUNCTION__.":Sending mail to the queue failed via Myntra");
		}
		return $ret;
	}
}