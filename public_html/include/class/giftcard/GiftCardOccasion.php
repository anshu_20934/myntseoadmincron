<?php
namespace giftcard;
include_once "$xcart_dir/include/class/mcart/class.MCartUtils.php";

class GiftCardOccasion{
	
	public $id;
	
	public function getId(){
		return $this->id;
	}
	
	public function setId($id){
		$this->id = $id;
	}
	
    public function createArrayFromObj() {
        $arr = \MCartUtils::createArrayFromObj(get_object_vars($this));
        return $arr;
    }
}
?>