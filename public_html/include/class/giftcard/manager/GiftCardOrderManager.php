<?php
namespace giftcard\manager;

use base\manager\BaseManager;

use order\dataobject\builder\AmountDOBuilder;
use order\dataobject\builder\OrderItemsDOBuilder;
use order\dataobject\builder\PaymentGatewayFormBuilder;
use order\dataobject\builder\TempOrderDOBuilder;

use order\dataobject\AmountDO;
use order\dataobject\DiscountDO;
use order\dataobject\GiftDO;
use order\dataobject\OrderDO;
use order\dataobject\PaymentDO;
use order\dataobject\ShippingDO;
use order\dataobject\TempOrderDO;

use order\dataobject\validator\CashOnDeliveryValidator;
use order\dataobject\validator\DiscountDOValidator;
use order\dataobject\validator\ShippingDOValidator;

use order\dao\OrderDAO;
use order\dao\mapper\OrderDOMapper;

use order\exception\CartTamperedException;
use order\exception\EmptyCartException;
use order\exception\NoSuitableGatewayException;
use order\exception\UserNotLoggedInException;

use enums\revenue\payments\PaymentType;
use revenue\payments\PaymentGatewayFactory;
use style\dao\ProductOptionsDAO;

use base\dataobject\IBaseOrderDO;

require_once(\HostConfig::$documentRoot."/modules/giftcards/GiftCardsHelper.php");
require_once \HostConfig::$documentRoot."/include/class/mcart/class.MCartUtils.php";

class GiftCardOrderManager extends BaseManager {

	public $orderDO;
	private $orderDAO, $productOptionsDAO;
	
	public function __construct(IBaseOrderDO $orderDO) {
		$this->orderDO = $orderDO;
		$this->orderDAO = new OrderDAO();
		$this->productOptionsDAO = new ProductOptionsDAO();
	}
        
        
	public function checkoutOrderActions($pgDiscount, $emiCharge, $lastUpdatedTime) {
		$this->checkEmptyLogin();
		$this->checkForEmptyCart();
                // TODO -- TS need to look at
		//$this->checkForCartTamperingOrderActions($lastUpdatedTime);
		$this->validateDOs();
		$paymentDO = $this->orderDO->getPaymentDO();
		$cardDO = $paymentDO->getCardDO();
		$amountDO = $this->orderDO->getAmountDO();
		$paymentDO->setPaymentGatewayDiscount($pgDiscount);
		$amountDO->setPaymentGatewayDiscount($pgDiscount);
		$paymentDO->setEMICharge($emiCharge);
		$amountDO->setEMICharge($emiCharge);
		$this->initAmountDOFields();
		$this->createOrder();
		$this->lockCouponsAfterUsage();
		//fraud logging
		//$this->checkAndLogForFraud();
	}
	
	/**
	 * This will work on OrderDO
	 */
	public function checkout() {
		
		$this->checkPrerequisites();
		$this->validateDOs();
		$this->initDerivedParams();
		$this->createOrder();
		$this->lockCouponsAfterUsage();
		
		//For Gift Card order we need not provide the payment gateway discount
		$this->initPaymentGatewayDiscount();
		//fraud logging
		//$this->checkAndLogForFraud();
	}

	public function lockCouponsAfterUsage() {
		$couponCode = $this->orderDO->getDiscountDO()->getCouponCode();
		$cashCouponCode = $this->orderDO->getDiscountDO()->getCashCouponCode();
		
		if(!empty($couponCode) || !empty($cashCouponCode)) {
			$adapter = \CouponAdapter::getInstance();
			$validator = \CouponValidator::getInstance();

			if(!empty($couponCode)) {
				# Update the number of times the coupon has been used once the order is placed
				$adapter->lockCouponForUser($couponCode, $this->orderDO->getLogin());
			}	
			if(!empty($cashCouponCode)) {
	            # Update the number of times the coupon has been used once the order is placed
	            $adapter->lockCouponForUser($cashCouponCode, $this->orderDO->getLogin());
	        }	
		}
	}
	/**
	 * 
	 * Enter description here ...
	 */
	
	public function getPaymentGatewayForm() {
		$pgFormBuilder = new PaymentGatewayFormBuilder($this->orderDO);
		$form = $pgFormBuilder->build();		
		return $form;
	}
	
	protected function createOrder() {
		//OrderDAO methods to be called on OrderDO:
		//(1) createAndSaveTempOrder (2) create order	
		
		$paymentDO = $this->orderDO->getPaymentDO();
		$discountDO = $this->orderDO->getDiscountDO();
		$cartDO = $this->orderDO->getCartDO();
		$amountDO = $this->orderDO->getAmountDO();
		$login = $cartDO->getCart()->getLogin();
		//Create temp order to get orderid to be used
		$this->createAndSaveTempOrder();
		$orderId = $this->orderDO->getOrderId();
		$giftCard=$this->getGiftCardDetails($cartDO);
		
		$giftCardOrderDetails=$this->getGiftCardOrderDetails($login, $orderId, $paymentDO, $discountDO, $amountDO, $giftCard);
		
		//(1) Create gift card and the corresponding order
		$returnObj = \GiftCardsHelper::buyGiftCard($giftCardOrderDetails);
		
		if($returnObj != NULL){
			$orderId = $returnObj->orderid;
			$this->orderDO->setOrderId($orderId);
			//Order-name is a placeholder for future: now it is set to order-id only
			$this->orderDO->setOrderName($orderId);//$user_source_code.'-'.$orderid.
			$this->orderDO->setGroupId($orderId);
			if(x_session_is_registered("gcorderID")) {
				x_session_unregister("gcorderID");
			}
			x_session_register("gcorderID", $orderId);
		}
		else{
			//There seems to be problem creating the giftcard order. Redirect the user to payment page asking him to try again.
		}
	}

	protected function createAndSaveTempOrder() {
		$tempOrderDOBuilder = new TempOrderDOBuilder($this->orderDO);
		$tempOrderDO = $tempOrderDOBuilder->build();
		$tempOrderDO = $this->orderDAO->saveTempOrder($tempOrderDO);
		$this->orderDO->setOrderId($tempOrderDO->getOrderId());
		
		//Order-name is a placeholder for future: now it is set to order-id only
		$this->orderDO->setOrderName($tempOrderDO->getOrderId());//$user_source_code.'-'.$orderid.
		$this->orderDO->setGroupId($tempOrderDO->getOrderId());
		$this->orderDO->setTempOrderDO($tempOrderDO);
	}
	
	private function getGiftCardOrderDetails($login, $orderId, $paymentDO, $discountDO, $amountDO, $giftCard){
		//$giftObject=new stdClass();
		$giftCardOrderDetails=array();
		$cashRedeemed=$discountDO->getCashRedeemed();
		$cashCouponCode=$discountDO->getCashCouponCode();
		$productBagDiscount=$discountDO->getProductBagDiscount();
		$productItemDiscount=$discountDO->getProductItemDiscount();
		$paymentGatewayDiscount=$paymentDO->getPaymentGatewayDiscount();
		$paymentMethod=$paymentDO->getPaymentOption();
		$emiCharge=$amountDO->getEMICharge();
		$codCharge=$paymentDO->getCODCharge();
		$giftCardOrderDetails["discount"]=(empty($productItemDiscount))?0.00:$productItemDiscount;
		$giftCardOrderDetails["cashRedeemed"]=(empty($cashRedeemed))?0.00:$cashRedeemed;
		$giftCardOrderDetails["cashCouponCode"]=(empty($cashCouponCode))?"":$cashCouponCode;
		$giftCardOrderDetails["paymentGatewayDiscount"]=(empty($productBagDiscount))?0.00:$productBagDiscount;
		$giftCardOrderDetails["cartDiscount"]=(empty($paymentGatewayDiscount))?0.00:$paymentGatewayDiscount;
		$giftCardOrderDetails["paymentOption"]=(empty($paymentMethod))?"":$paymentMethod;
		$giftCardOrderDetails["codCharge"]=(empty($codCharge))?0.00:$codCharge;
		$giftCardOrderDetails["paymentSurcharge"]=(empty($emiCharge))?0.00:$emiCharge;
		$giftCardOrderDetails["orderId"]=$orderId;
		$giftCardOrderDetails["login"]=$login;
		$giftCardOrderDetails["giftCard"]=$giftCard;
		return $giftCardOrderDetails;
	}
	
	private function getGiftCardDetails($cartDO){
		$productsInCart = $cartDO->getProductsInCart();
		$giftCard=$productsInCart["giftcard"]["giftCardDetails"];
		return $giftCard;	
	}
	
	/**
	 * Persists OrderItems
	 * @param array<OrderItemDO> $orderItems
	 */
	public function createOrderItems() {
		$orderItemsDOBuilder = new OrderItemsDOBuilder($this->orderDO);
		$orderItems = $orderItemsDOBuilder->build();
		
		foreach ($orderItems as $eachOrderItem) {
			$itemId = $this->orderDAO->saveOrderItem($eachOrderItem);
			//set this item id on the item
			$eachOrderItem->setItemId($itemId);
		}
		$this->orderDO->setOrderItems($orderItems);
	}
	
	public function createProducts() {
		$orderItems = $this->orderDO->getOrderItems();
		
		foreach ($orderItems as $eachOrderItem) {
			$this->orderDAO->saveProduct($eachOrderItem);
		}		
	}
	
	private function updateOrderItemQuantities() {
		
		$this->orderDAO->updateOrderQuantity($this->orderDO->getOrderId(), $this->orderDO->getTotalQuantity());
		
		$orderItems = $this->orderDO->getOrderItems();
		foreach ($orderItems as $eachOrderItem) {
			
			$skuId = $eachOrderItem->get("skuid");
			//for kundan for this query to a better place.
			$itemOptionId = $this->productOptionsDAO->getOptionIdForSKUId($skuId);
			$itemId = $eachOrderItem->getItemId(); 
			$itemSizeQuanity = $eachOrderItem->get("quantity");			
			
			$this->orderDAO->saveOptionOrderQuantity($itemId, $itemOptionId, $itemSizeQuanity);
		}
	}
	
	/**
	 * Log if the order is detected as fraud. This uses a global $fraudOrderLog to log
	 */
	
	protected function checkAndLogForFraud() {
		$orderId = $this->orderDO->getOrderId();
		
		$fraudMatch = $this->orderDAO->checkForFraud($orderId);
		if(!empty($fraudMatch)) {
			
			global $fraudOrderLog, $XCART_SESSION_VARS;
			$fraudOrderLog->debug("MKORDERINSERT : ORDER:$orderId Detected as fraud ");

			$myCart = $this->orderDO->getCartDO()->getCart();
			if($myCart!==null){
				$fraudOrderLog->debug("MKORDERINSERT : ORDER:$orderId CARTDUMP{".serialize($myCart)."}");
			}
			else{
				$fraudOrderLog->debug("MKORDERINSERT : ORDER:$orderid CARTDUMP{EMPTY}");
			}
			$fraudOrderLog->debug("MKORDERINSERT : ORDER:$orderId SESSIONDUMP{".serialize($XCART_SESSION_VARS)."} ");
		}
	}
	
	protected function initDerivedParams() {
		$this->initPaymentGatewayDiscount();
		$this->initEMICharges();
		$this->initAmountDOFields();
	}
	
	protected function initMoreDerivedParams() {
		global $order_source_code, $order_source_id;//defined in config.ini
		$this->orderDO->setOrderName($order_source_code ."-". $this->orderDO->getOrderId());
		$this->orderDO->setSourceId($order_source_id);
	}
	
	//TODO: populate PG discount from OrderDO
	private function initPaymentGatewayDiscount() {
		global $https_location;
		
		$paymentDO = $this->orderDO->getPaymentDO();
		$cardDO = $paymentDO->getCardDO();
		$amountDO = $this->orderDO->getAmountDO();
		
		$gateway = PaymentGatewayFactory::getInstance()->getGateway($paymentDO);
		if($gateway == null) {
			throw new NoSuitableGatewayException("Could not fetch gateway to calculate Payment Gateway Discount", $paymentDO);
		}
		$gateway->setHttpsLocation($https_location);

		//Set the gateway on PaymentDO
		$paymentDO->setGateway($gateway);
		
		$pgDiscount = 0;
		if($gateway->getPaymentType() == PaymentType::Credit || $gateway->getPaymentType() == PaymentType::Debit || $gateway->getPaymentType() == PaymentType::CreditDebit) {
			$pgDiscount = $gateway->getDiscountByCardNumber($amountDO->getTotalBeforePGDiscount(), $amountDO->getAmountWithProductDiscount());
		} elseif($gateway->getPaymentType() == PaymentType::NetBanking) {
			$pgDiscount = $gateway->getDiscountOnBank("icici", $amountDO->getTotalBeforePGDiscount());
		}
		$paymentDO->setPaymentGatewayDiscount($pgDiscount);
		$amountDO->setPaymentGatewayDiscount($pgDiscount);
	} 
	
	//init EMI Charges to be called after initPaymentGatewayDiscount
	private function initEMICharges() {
		
		$paymentDO = $this->orderDO->getPaymentDO();
		$cardDO = $paymentDO->getCardDO();
		$amountDO = $this->orderDO->getAmountDO();
		
		$gateway = $paymentDO->getGateway();
		if($gateway == null) {
			throw new NoSuitableGatewayException("Could not fetch gateway for EMI Option", $paymentDO);
		}
		
		$emiCharge = 0;
		$emiBank = $paymentDO->getEMIBank();
		
		$amountToApplyEMIChargeOn = $amountDO->getTotalBeforePGDiscount() - $amountDO->getPaymentGatewayDiscount();
		
		if(!empty($emiBank)) {
			$emiCharge = $gateway->getEMICharges($emiBank,$amountToApplyEMIChargeOn);
		}
		
		$paymentDO->setEMICharge($emiCharge);
		$amountDO->setEMICharge($emiCharge);
	}
	
	private function initAmountDOFields() {
		$amountDO = $this->orderDO->getAmountDO();
		AmountDOBuilder::buildTotal($amountDO);
		AmountDOBuilder::buildFinalAmountToCharge($amountDO);
	}
	
	protected function validateDOs() {
		/*$validator = new ShippingDOValidator($this->orderDO->getShippingDO());
		$validator->validate();
		unset($validator);*/
		
		$validator = new DiscountDOValidator($this->orderDO);
		$validator->validate();
		unset($validator);

		/*$validator = new CashOnDeliveryValidator($this->orderDO);
		$validator->validate();
		unset($validator);*/
		
	}
	
	protected function checkPrerequisites() {
		$this->checkEmptyLogin();
		$this->checkForEmptyCart();
		$this->checkForCartTampering();
	}
	
	private function checkEmptyLogin() {
		$login = $this->orderDO->getLogin();
		if(empty($login)) {
			throw new UserNotLoggedInException("User not logged in or session timeout");
		}
	}
	
	private function checkForEmptyCart() {
		$cartDO = $this->orderDO->getCartDO();
		$productsInCart = $cartDO->getProductsInCart();
		if($cartDO == null || $productsInCart == null) {
			throw new EmptyCartException("Cart is Empty during checkout: nothing to buy");
		}
	}
	
	private function checkForCartTampering() {
		global $XCART_SESSION_VARS;
		$myCart = $this->orderDO->getCartDO()->getCart();
		if($XCART_SESSION_VARS['LAST_CART_UPDATE_TIME'] != $myCart->getLastContentUpdatedTime()){
			$myCart->setLastContentUpdatedTime(time());
			\MCartUtils::persistCart($myCart);
			throw new CartTamperedException("Last modified time of cart: {$myCart->getLastContentUpdatedTime()} is not equal to last modified time of cart available in session: {$XCART_SESSION_VARS["LAST_CART_UPDATE_TIME"]}"); 
		}
	}
}
?>