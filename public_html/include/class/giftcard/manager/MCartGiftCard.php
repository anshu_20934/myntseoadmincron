<?php
namespace giftcard\manager;

use giftcard\GiftCard;
use giftcard\GiftCardOccasion;
use giftcard\GiftCardMessage;
include_once "$xcart_dir/include/class/mcart/class.MCart.php";


/* Raghu - Cart for GiftCard */
class MCartGiftCard extends \MCart{
	
	public function __construct(){
	 	parent::__construct(array());
	}
	
	public function isReadyForCheckout(){
		return true;
	}

	public function refresh($refreshFromDb=false,$forcePriceRecalculation=false,$forceProductDiscountRecalculation=false,$forceItemAvailabilityRefresh=false){
		if($forcePriceRecalculation || $forceProductDiscountRecalculation){
			$this->noOfItems=0;
			$this->price=0.0;
			$this->mrp=0.0;
			$this->additionalCharges=0.0;
			$this->vat=0.0;
			foreach ($this->getProducts() as $product) {
				$product->refreshItem($refreshFromDb);
				$productId = $product->getProductId();
				$quantity = $product->getQuantity();
	    
				$this->noOfItems += $quantity;
				$this->price += $product->getTotalPrice();
				$this->mrp += $product->getTotalProductPrice();
				$this->additionalCharges += $product->getTotalAdditionalCharges();
				$this->vat += $product->getVAT();
				$this->productsArray[$productId] = $product;
				$this->productQtyArray[$productId] = $quantity;

			}
			$this->setShippingCharge();
			$this->reCalculatePricesAndDiscounts($forceProductDiscountRecalculation);
			$this->changed=true;
		}
		
		//to refresh the nondiscounted item count and ampount fields
		$this->getNonDiscountedItemsSubtotal();
		$this->refreshItemsAvailability($forceItemAvailabilityRefresh);
		\MCartUtils::updateSessionCartVariables($this,$this->getContext());		
	}
	
	public function removeInvalidItems(){
		
	}
	
	public function setDiscountFields($discountedCart){

	}
	
	public function getShippingCharge() {
    	return 0;
    }
	
    //For gift cards shipping charge is not applicable
	public function setShippingCharge() {
    	$this->shippingCharge = 0;
    }
    
    //For gift cards cart level discount is not applicable
	public function getCartLevelDiscount()
	{
		return 0;
	}
	
}?>
