<?php
namespace giftcard\manager;
use giftcard\GiftCard;
use giftcard\GiftCardOccasion;
use giftcard\GiftCardMessage;
include_once "$xcart_dir/include/class/mcart/class.MCart.php";
include_once "$xcart_dir/include/class/mcart/class.MCartItem.php";
include_once "$xcart_dir/include/class/mcart/class.MCartUtils.php";

/* Raghu - Mgiftcard */
class MCartGiftCardItem extends \MCartItem{
		
	public $giftCardDetails;
    	
    public function getGiftCardDetails(){
    	return $this->giftCardDetails;
    }
    
    public function setGiftCardDetails($giftCardDetails){
    	$this->giftCardDetails = $giftCardDetails;
    }	
	
	public function __construct($value,$quantity){
		
        $product['quantity']=$quantity;
        $product['productPrice']=$value;
        $product['unitPrice']=$value;

        $this->quantity = $product['quantity'];
        $this->unitPrice = $product['unitPrice'];
        $this->totalPrice = $this->quantity * $this->unitPrice;
        // $unitPrice * (1 + $vatRate / 100)
        $this->productPrice = $product['productPrice'];
        
        $this->totalProductPrice = $this->productPrice * $this->quantity;
        $this->totalDiscount = $product['discount'];
        $this->totalCashDiscount = $product['cashdiscount'];
        $this->vatRate = 0;
        
        //placeholder id
        $this->styleid = "giftcard";
        $this->style['id']=$this->styleid;
		$this->style['name']=$this->styleid;
		$this->style['brand']=$this->styleid;
		$this->style['productTypeId']=$this->styleid;;
		$this->style['productTypeLabel']=$this->styleid;
		$this->style['styleType']=$this->styleid;
		$this->productId = $this->styleid;
        
        $this->refreshItemAvailability();
	}
	
	/**
	 * Type of object Non Personalized or Personalized
	 */
	public function getType(){
		return MCart::$__ITEM_GC;
	}
	
	/**
	 * refreshes cart item ,
	 * Note: Quantity is saved
	 * @var $refreshFromDb if true refreshes all cached style sku data from db
	 */
	public function refreshItem($refreshFromDb){

	}
	
	/**
	 * 
	 *Refreshes Item's availability with as latest 
	 */
	public function refreshItemAvailability(){
		$this->updateItemAvailabilityFromArray();
	}
	
	/**
	*
	*Update Item's availability with
	*/
	public function updateItemAvailabilityFromArray(){
		$this->instock=self::$__ITEM_STATE_IN_STOCK;
	}

	public function getItemAvailability(){
		return $this->instock;
	}

	public function equals($mcartItem){
		return false;
	}

	public function validate(){
		return true;
	}
	
	/**
	 * 
	 * Getter for unit vat of item
	 */
 	public function getUnitVAT()
    {
        return $this->productPrice * $this->vatRate / 100;
    }
    
    /**
     * Getter for unit price of item
     */
    public function getUnitPrice(){
    	return $this->unitPrice;
    }
    
    public function setQuantity($quantity){
    	$this->quantity=$quantity;
    }
	
	public function beforeSerialize(){
	}
	
	public function afterDeSerialize(){
		
	}
	
	public function createObjFromArray($product){
		$giftCard = new GiftCard();
        $giftCardMessage = new GiftCardMessage();
        $giftCardOccasion = new GiftCardOccasion();

        $giftCardOccasion->setId($product["giftCardDetails"]["giftCardMessage"]["giftCardOccasion"]["id"]);
        $giftCardMessage->setGiftCardOccasion($giftCardOccasion);
        $giftCardMessage->setMessage($product["giftCardDetails"]["giftCardMessage"]["message"]);
        $giftCard->setGiftCardMessage($giftCardMessage);
        $giftCard->setType($product["giftCardDetails"]["type"]);
        $giftCard->setSenderEmail($product["giftCardDetails"]["senderEmail"]);
        $giftCard->setRecipientEmail($product["giftCardDetails"]["recipientEmail"]);
        $giftCard->setAmount($product["giftCardDetails"]["amount"]);
        $giftCard->setQuantity($product["giftCardDetails"]["quantity"]);
        $giftCard->setRecipientName($product["giftCardDetails"]["recipientName"]);
        $giftCard->setSenderName($product["giftCardDetails"]["senderName"]);
        $this->setGiftCardDetails($giftCard);
        unset($product["giftCardDetails"]);
        parent::createObjFromArray($product);
        return $this;
    }

    public function createArrayFromObj() {
    	$arr = \MCartUtils::createArrayFromObj(get_object_vars($this));
    	return $arr;
    }
}
?>
