<?php
namespace giftcard;
include_once "$xcart_dir/include/class/mcart/class.MCartUtils.php";

class GiftCardMessage{
	
	public $message;
	
	public $giftCardOccasion;

	public function getGiftCardOccasion(){
		return $this->giftCardOccasion;
	}
	
	public function setGiftCardOccasion($giftCardOccasion){
		$this->giftCardOccasion = $giftCardOccasion;
	}
	
	public function getMessage() {
		return $this->message;
	}

	public function setMessage($message) {
		$this->message = $message;
	}
	
    public function createArrayFromObj() {
        $arr = \MCartUtils::createArrayFromObj(get_object_vars($this));
        return $arr;
    }
}
?>