<?php

namespace giftcard\dataobject\builder;

use order\dataobject\builder\OrderDOBuilder;
use order\dataobject\builder\AmountDOBuilder;
use order\dataobject\builder\BillingAddressDOBuilder;
use order\dataobject\builder\DiscountDOBuilder;
use order\dataobject\builder\PaymentDOBuilder;
use order\dataobject\builder\ShippingDOBuilder;
use order\dataobject\builder\TempOrderDOBuilder;
use order\dataobject\builder\OrderInputParamDOBuilder;

use order\dataobject\AddressDO;
use order\dataobject\BillingAddressDO;
use order\dataobject\CartDO;
use order\dataobject\DiscountDO;
use order\dataobject\GiftDO;
use order\dataobject\OrderDO;
use order\dataobject\PaymentDO;
use order\dataobject\ShippingDO;
use order\dataobject\InputParamDO;

use giftcard\dataobject\GiftCardOrderDO;

use base\exception\BaseRedirectableException;

require_once \HostConfig::$documentRoot."/include/class/mcart/class.MCart.php";

class GiftCardOrderDOBuilder extends OrderDOBuilder{

	/**
	 * This creates an instance of OrderDO
	 * which in turn is composed of multiple other DOs 
	 * @return: an Instance of OrderDO
	 */
	public function build(\MCart $cart) {
		try {
			$orderDO = new GiftCardOrderDO($cart, $XCARTSESSID);
			$this->createAndAssignDOs($orderDO, $cart);
			return $orderDO;	
		} catch (BaseRedirectableException $ex) {
			throw $ex;
		} catch (RedirectableValidationException $rex) {
			throw $rex;
		}
	}
	
}