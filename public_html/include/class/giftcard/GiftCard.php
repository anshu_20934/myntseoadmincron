<?php
namespace giftcard;
include_once "$xcart_dir/include/class/mcart/class.MCartUtils.php";

class GiftCard{
	
	public $giftCardMessage;

	public $type;
		
	public $senderEmail;
	
	public $recipientEmail;
	
	public $amount;

	public $quantity;
	
	public $recipientName;
	
	public $senderName;
	
	public function getGiftCardMessage() {
		return $this->giftCardMessage;
	}

	public function setGiftCardMessage($giftCardMessage) {
		$this->giftCardMessage = $giftCardMessage;
	}

	public function getType() {
		return $this->type;
	}

	public function setType($type) {
		$this->type = $type;
	}

	public function getSenderEmail() {
		return $this->senderEmail;
	}

	public function setSenderEmail($senderEmail) {
		$this->senderEmail = $senderEmail;
	}

	public function getRecipientEmail() {
		return $this->recipientEmail;
	}

	public function setRecipientEmail($recipientEmail) {
		$this->recipientEmail = $recipientEmail;
	}

	public function getAmount() {
		return $this->amount;
	}

	public function setAmount($amount) {
		$this->amount = $amount;
	}

	public function getQuantity() {
		return $this->quantity;
	}

	public function setQuantity($quantity) {
		$this->quantity = $quantity;
	}
	
	public function getSenderName() {
		return $this->senderName;
	}

	public function setSenderName($senderName) {
		$this->senderName = $senderName;
	}
	
	public function getRecipientName() {
		return $this->recipientName;
	}

	public function setRecipientName($recipientName) {
		$this->recipientName = $recipientName;
	}

	public function createArrayFromObj() {
    	$arr = \MCartUtils::createArrayFromObj(get_object_vars($this));
    	return $arr;
    }
}
?>