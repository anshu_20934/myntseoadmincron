<?php
namespace utils;

class RandomStringGenerator{
	private static $CHARS_ALLOWED = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWZYZ";
	
	public static function generateRandomString($size){
		$random="";
		for ($p = 0; $p < $size; $p++) {
			$random .= self::$CHARS_ALLOWED[mt_rand(0, strlen(self::$CHARS_ALLOWED))];
		}
		return $random;
	}
}