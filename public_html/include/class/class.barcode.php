<?php
// Define variable to prevent hacking
define('IN_CB',true);

// Including all required classes
require('barcodegenerator/class.barcode.Constants.php');
require('barcodegenerator/class.barcode.FColor.php');
require('barcodegenerator/class.barcode.base.php');
require('barcodegenerator/class.barcode.FDrawing.php');

// including the barcode technology
include('barcodegenerator/class.barcode.code39.php');

// Creating some Color (arguments are R, G, B)
$color_black = new FColor(0,0,0);
$color_white = new FColor(255,255,255);

/* Here is the list of the arguments:
1 - Thickness
2 - Color of bars
3 - Color of spaces
4 - Resolution
5 - Text
6 - Text Font (0-5) */
$barcode_height = 10;

$drawtext=$_GET['text'];
if($drawtext=="")
   $drawtext=true;
else if ($drawtext=="0")
   $drawtext=false;

if(intval($_GET['height'])>10) {
	$barcode_height=intval($_GET['height']);
}

$barcode_input = "NOINPUT";
if(isset($_GET['code'])){
	//Sanitize barcode input
	$barcode_input = preg_replace("/[^A-Za-z0-9\-]/","",$_GET['code']);
}
$code_generated = new code39($barcode_height,$color_black,$color_white,1,$barcode_input,2,$drawtext);

/* Here is the list of the arguments
1 - Width
2 - Height
3 - Filename (empty : display on screen)
4 - Background color */
$drawing = new FDrawing(500,500,'',$color_white);
$drawing->init(); // You must call this method to initialize the image
$drawing->add_barcode($code_generated);
$drawing->draw_all();
$im = $drawing->get_im();

// Next line create the little picture, the barcode is being copied inside
$im2 = imagecreate($code_generated->lastX,$code_generated->lastY);
imagecopyresized($im2, $im, 0, 0, 0, 0, $code_generated->lastX, $code_generated->lastY, $code_generated->lastX, $code_generated->lastY);
$drawing->set_im($im2);

// Header that says it is an image (remove it if you save the barcode to a file)
header('Content-Type: image/png');

// Draw (or save) the image into PNG format.
$drawing->finish(IMG_FORMAT_PNG);
?>

