<?php 
class CustomizationHelper 
{

	// Array to hold customization data
	private $custArray = array();
	
	// Array to hold Master Customization Data
	private $masterCustomizationArray = array();

	// Array to hold chosen product configuration
	private $chosenProductConfiguration = array();

	// Array to hold global data array
	private $globalDataArray = array();

	// Images base dir
	private $baseDir;

	private $IMAGE_DPI = 96;

	private $resizeBy = 2;
	private $moveBy = 4;
	private $rotateBy = 90;
	private $inchesToPixel = 76;//assumption for conversion from inches to pixels, 1" = 30px

	// Construct helper class - init customization array, master Customization Array and chosen product configuration of this object
	public function CustomizationHelper($params,$document_root='')
	{
		$this->custArray = $params["custArray"];
		$this->chosenProductConfiguration = $params["chosenProductConfiguration"];
		$this->masterCustomizationArray = $params["masterCustomizationArray"];
		$this->globalDataArray = $params["globalDataArray"];
        if(empty($document_root))
		    $this->baseDir = $_SERVER["DOCUMENT_ROOT"]."/";
        else
            $this->baseDir = $document_root;
	}
	public function updateGlobalDataArray($p_globalDataArray)
	{
		$this->globalDataArray = $p_globalDataArray;
	}
	public function getMasterCustomizationArray()
	{
		return $this->masterCustomizationArray;
	}

	public function getChosenProductConfiguration()
	{
		return $this->chosenProductConfiguration;
	}
	
	public function getCustomizationArray()
	{
		return $this->custArray;
	}
	
	
	private function getOrientationNameFromObjectID($objectid) {
		$thisCustArray = $this->getCustomizationArrayForCurrentSelectedArea();
		$thisCustArrayOrientations = $thisCustArray["orientations"];
		foreach($thisCustArrayOrientations as $name=>$customizationobject) {
			if($customizationobject["imageobjectid"] == $objectid || $customizationobject["textobjectid"]==$objectid) {
				return $name;
			}
		}
		return "";
	}

	//generate current selected object's image	
	public function generateObjectImage($objectid,$object,$writeToFile=false,$filePath="",$guiding_image = false, $orientationinfo=null)
	{
		if( $object["type"] == "image")
		{
			$im = $this->generateObjectImage_Image($objectid,$object,$guiding_image);
			if( $writeToFile )
			{
				$im->writeImage($this->baseDir.$filePath);
				$im->destroy();
				return $filePath;		
			}
			else
				return $im;
		}
		else
		if( $object["type"] == "text")
		{
			$im = $this->generateObjectImage_Text($objectid,$object,$guiding_image, $orientationinfo);
			if( $writeToFile )
			{
				//get orientation width & height from master array
				/*$currSelectedArea = $this->getSelectedAreaName();
				$masterArray = $this->masterCustomizationArray["areas"][$currSelectedArea];
				$orientationname = $this->getSelectedOrientationName();
				$ortwidth = $masterArray["orientations"][$orientationname]["width_in"] * $this->IMAGE_DPI;
				$ortheight = $masterArray["orientations"][$orientationname]["height_in"] * $this->IMAGE_DPI;
				$im = $this->createImage($ortwidth,$ortheight);
				$im->drawImage($draw);*/
				$im->writeImage($this->baseDir.$filePath);
				$im->destroy();
				return $filePath;
			}
			else {
				return $im;
			}
		}

	}
	public function getUsetInputTextDetails_scaled($objectid,$objectinfo)
	{
		$text  = $objectinfo["text"];
		$customizationInfo = $objectinfo["customizationinfo"];
		$dx = $customizationInfo["dx"];
		$dy = $customizationInfo["dy"];
		$font = $customizationInfo["style"];
		$size = $customizationInfo["size"];
		$size_in_inches = ($customizationInfo["size"]/$this->inchesToPixel);//converting from pixels to inches for operational purpose,assumption: 1"=30px;
		$color = $customizationInfo["color"];

		$size = $size * $customizationInfo["scaleup_font_factor"];	
		$dx = $dx * $customizationInfo["scaleup_font_factor"];
		$dy = $dy * $customizationInfo["scaleup_font_factor"];

		$textDetails["text"] = $text;
		$textDetails["start_x"] = $dx;   //TODO: make them wrt to orientation start
		$textDetails["start_y"] = $dy;
		$textDetails["text_format"] = "font:$font,size:$size,size(in inches):$size_in_inches,color:$color";

		return $textDetails;
		
	}
	
	// Generate final image for textobject in current selected orientation
	private function generateObjectImage_Text($objectid,$object,$guiding_image = false, $orientationinfo=null)
	{
		$text  = $object["text"];
		$customizationInfo = $object["customizationinfo"];
		$dx = $customizationInfo["dx"];
		$dy = $customizationInfo["dy"];
		$font = $customizationInfo["style"];
		$size = $customizationInfo["size"];
		$color = $customizationInfo["color"];

		$currSelectedArea = $this->getSelectedAreaName();
		$masterArray = $this->masterCustomizationArray["areas"][$currSelectedArea];
		
		if($orientationinfo) {
			$orientationname = $this->getOrientationNameFromObjectID($objectid);
		} else { 
			$orientationname = $this->getSelectedOrientationName(); // Pravin - This causes issues for jerseys migration.
		}

		$ortwidth = $masterArray["orientations"][$orientationname]["size_x"]; 
		$ortheight = $masterArray["orientations"][$orientationname]["size_y"];
		
		if( $guiding_image ) {
			$size = $size * $customizationInfo["scaleup_font_factor"] * (200/$this->IMAGE_DPI);	
			$ortwidth = $ortwidth * $customizationInfo["scaleup_font_factor"] * (200/$this->IMAGE_DPI);
			$ortheight = $ortheight * $customizationInfo["scaleup_font_factor"] * (200/$this->IMAGE_DPI);
		}
		
		if($orientationinfo) {
			$pathToReplaceImagesAsText = $orientationinfo["pathForImagesToReplaceText"];
		}
		
		if(!empty($pathToReplaceImagesAsText)) { 
			$isTextStoredAsImages = true;
		}

		if($isTextStoredAsImages == true) {
			$im2 = new Imagick();
			$text = strtolower($text);
			
			$len = strlen($text);
			for($i=0; $i<$len; $i++) {
				$pathImage = $pathToReplaceImagesAsText . $text[$i] . ".png";
				if(file_exists($pathImage)) {
					$im2->readImage("$pathImage");
				}
			}
			
			// Append the images into one
			$im2->resetIterator();
			
			$combined = $im2->appendImages(false);
			
			$scalefactor = 1;
			$checkscale1 = ($ortheight)/($combined->getimageheight());
			$checkscale2 = ($ortwidth)/($combined->getimagewidth());
			
			if($checkscale1<$scalefactor) {
				$scalefactor = $checkscale1;
			}
			if($checkscale2<$scalefactor) {
				$scalefactor = $checkscale2;
			}
			
			if($scalefactor<1) {
				$combined = $this->resizeImage($combined,$ortwidth*$scalefactor,$ortheight*$scalefactor);
			}
			
			$im2->destroy();
			$combined->setImageFormat("png");
			return $combined;
			
		} else {
	  		/* Create new imagick object */
			$im_draw = new ImagickDraw();
			
			$im_draw->setFont($this->baseDir.$font);
			$im_draw->setFontSize($size);
			$im_draw->setFillColor($color); // Some error here ?
			$im_draw->setStrokeAntialias(true);
			$im_draw->setTextAntialias(true);
			$im_draw->setGravity(imagick::GRAVITY_CENTER);
			$im_draw->annotation(0,0,"$text");
			
			$im = $this->createImage($ortwidth,$ortheight);
			$im->drawImage($im_draw);

			/*$text_image = $this->createImage($ortwidth,$ortHeight);
			$text_image = mergeImages($text_image,$im,Imagick::COMPOSITE_MULTIPLY,$dx,$dy,Imagick::GRAVITY_CENTER); */
	  		$im_draw->destroy();
			return $im;
		}

	}
	
	// generate final image for imageobject in current selected orientation
	private function generateObjectImage_Image($objectid,$object,$guiding_image = false)
	{
		// get object base info from global data
		$baseinfo = $this->globalDataArray[$objectid];

		// get object customization info from customization array
		$custinfo = $object["customizationinfo"];
				
		$baseimage = $this->createImageFromFile($this->baseDir.$baseinfo["baseimagepath_L"]);
	
		// get width,height,rotation angle and apply the parameters on baseimage
		$owidth = $custinfo["width"];
		$oheight = $custinfo["height"];

		if( $guiding_image ) // generated scaled up image
		{
			$curr_selected_area = $this->getSelectedAreaName();
			$curr_selected_orientation = $this->getSelectedOrientationName();

			$owidth = $owidth * $custinfo["scaleup_width_factor"] * (200/$this->IMAGE_DPI);
			$oheight = $oheight * $custinfo["scaleup_height_factor"] * (200/$this->IMAGE_DPI);
		}
	
	
		$oimage = $this->cloneImage($baseimage);
		$oimage = $this->resizeImage($oimage,$owidth,$oheight);

		//if rotation angle defined
		if( $custinfo["rotationAngle"] != 0 )
		{
			$oimage = $this->rotateImage($oimage,$custinfo["rotationAngle"]);
		}
		$baseimage->destroy();
		return $oimage;
	}

	// generate orientation image of curent selected area
	public function generateOrientationImage($orientationname,$orientationinfo,$writeToFile=false,$filePath="",$guiding_image=false)
	{

		//get orientation width & height from master array
		$currSelectedArea = $this->getSelectedAreaName();
		$masterArray = $this->masterCustomizationArray["areas"][$currSelectedArea];
		if( $guiding_image )
		{
			$ortwidth = $masterArray["orientations"][$orientationname]["width_in"] * $this->IMAGE_DPI * (200/$this->IMAGE_DPI) ;
			$ortheight = $masterArray["orientations"][$orientationname]["height_in"] * $this->IMAGE_DPI * (200/$this->IMAGE_DPI);
		}
		else
		{
			$ortwidth = $masterArray["orientations"][$orientationname]["size_x"];
			$ortheight = $masterArray["orientations"][$orientationname]["size_y"];
		}

		// create transparent image for this orientation
		$orientation_trans_image = $this->createImage($ortwidth,$ortheight,false,$orientationinfo["bgcolor"]);

		// arrange objects of orientation ( first image and then text )
		$imageObject = ""; $textObject  = "";
		$imageObjectZIndex = -1; $textObjectZIndex = -1;
		$imgobjectid = $orientationinfo["imageobjectid"];
		if( !empty($imgobjectid) )
		{
			$imageObject = $orientationinfo[$imgobjectid];
			$imageObjectZIndex = $imageObject["customizationinfo"]["zindex"];
		}
			
		$textobjectid = $orientationinfo["textobjectid"];
		if( !empty( $textobjectid ) )
		{
			$textObject = $orientationinfo[$textobjectid];
			$textObjectZIndex = $textObject["customizationinfo"]["zindex"];
		}
		$objectsArray = array();
		//if( $imageObjectZIndex < $textObjectZIndex )
			$objectsArray = array( $imgobjectid=>$imageObject, $textobjectid=>$textObject );
		//else
			//$objectsArray = array( $textobjectid=>$textObject, $imgobjectid=>$imageObject );*/

		// place objects in their respective coords on the orientation transparent image
		foreach($objectsArray as $id=>$object)
		{
			if( !empty($object) )
			{
				// generate image for object
				$oimage = $this->generateObjectImage($id,$object,false,"",$guiding_image, $orientationinfo);
				
				// merge object onto orientation
				$odx = $object["customizationinfo"]["dx"];
				$ody = $object["customizationinfo"]["dy"];
				if( $object["type"] == "image")
				{
					if( $guiding_image )
					{
						$odx = $odx * $object["customizationinfo"]["scaleup_width_factor"] * (200/$this->IMAGE_DPI);
						$ody = $ody * $object["customizationinfo"]["scaleup_height_factor"] * (200/$this->IMAGE_DPI) ;
					}
					$orientation_trans_image = $this->mergeImages($orientation_trans_image,$oimage,Imagick::COMPOSITE_DEFAULT,$odx,$ody,Imagick::GRAVITY_CENTER);
				}
				else
				{
					if($guiding_image)
					{
						$odx = $odx * $object["customizationinfo"]["scaleup_font_factor"] * (200/$this->IMAGE_DPI);
						$ody = $ody * $object["customizationinfo"]["scaleup_font_factor"] * (200/$this->IMAGE_DPI) ;
					}
					$orientation_trans_image = $this->mergeImages($orientation_trans_image,$oimage,Imagick::COMPOSITE_DEFAULT
,$odx,$ody,Imagick::GRAVITY_CENTER);
				}
				// free object image
				$oimage->destroy();
			} 
		}	
		
		if( $writeToFile )
		{
			
			$orientation_trans_image->writeImage($this->baseDir.$filePath);
			$orientation_trans_image->destroy();
			return $filePath;
		}
		else
			return  $orientation_trans_image;

	}

	//generate current selected area's image
	public function generateAreaImage()
	{
		// customization array for current selected area
		$thisCustArray = $this->getCustomizationArrayForCurrentSelectedArea();

		// current selected area name
		$currSelectedArea = $this->getSelectedAreaName();

		// master customization array for current selected area
		$masterArray = $this->masterCustomizationArray["areas"][$currSelectedArea];

		// get the area background image
		$areabgimage = $this->createImageFromFile($this->baseDir.$masterArray["bgimage"]);
		
		if( $thisCustArray["is_customized"] == 1 )
		{
			// get area co-ordinates from master customization array
			$mdx = $masterArray["start_x"];
			$mdy = $masterArray["start_y"];
			$mwidth = $masterArray["size_x"];
			$mheight = $masterArray["size_y"];

			// create a transparent image for the area width & height
			$area_trans_image  = $this->createImage($mwidth,$mheight);
			
			//store  current selected orientation name to reset it back after the for loop
			$curr_selected_orientation  = $this->getSelectedOrientationName();

			// loop through all orientations - please note the orientations are always sorted in ascending order of their zindices
			foreach($thisCustArray["orientations"] as $orientationname=>$orientationinfo)
			{
				if( $orientationinfo["is_customized"] == 0 )
					continue;
				
				// create orientation image
				$orientation_trans_image = $this->generateOrientationImage($orientationname,$orientationinfo,false,"");

				// merge orientation image onto the area trans image
				$ortdx = $masterArray["orientations"][$orientationname]["start_x"];
				$ortdy = $masterArray["orientations"][$orientationname]["start_y"];
				$area_trans_image = $this->mergeImages($area_trans_image,$orientation_trans_image,Imagick::COMPOSITE_DEFAULT,$ortdx,$ortdy,Imagick::GRAVITY_NORTHWEST);
				//free orientation_trans_image
				$orientation_trans_image->destroy();
			}
			//reset current selected orientation
			$this->setSelectedOrientationName($curr_selected_orientation);

			// merge area trans image onto the area image
			$area_trans_image->flattenImages();
			$finalImage = $this->mergeImages($areabgimage,$area_trans_image,Imagick::COMPOSITE_DEFAULT,$mdx,$mdy,Imagick::GRAVITY_NORTHWEST);	
			$area_trans_image->destroy();
		}			
		else  // no customization, show background image
		{
			$finalImage = $areabgimage;
		}
		$finalImage->setImageFormat("jpg");
		//header("Content-type: image/jpg");
	    //header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
	    //header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
		//echo  $finalImage;
		$newfile = $this->baseDir.$this->generateImageName();
		$finalImage->writeImage($newfile);
		
		$finalImage->destroy();
		return $newfile;
	}
	//function to check customization status
	public function getCustomizationStatus()
	{
		$thisCustArray = $this->getCustomizationArray();
		foreach($thisCustArray["areas"] as $area)
		{
			if( $area["is_customized"] == 1 )
				return 1;
		}
		return 0;
	}
	public function getImageBlurStatus()
	{
		$thisCustArray = $this->getCustomizationArrayForCurrentSelectedObject();
		if( empty($thisCustArray) )
			return 0;
		$thisCustArray = $thisCustArray["customizationinfo"];
		if( empty($thisCustArray) )
			return 0;
		if( ($thisCustArray["width"] > $thisCustArray["max_width_allowed"]) ||  ($thisCustArray["height"] > $thisCustArray["max_height_allowed"]) )
			return 1;
		return 0;
	}

	// function to show current selected area's customized image
	public function showFinalCustomization()
	{
		$thisCustArray = $this->getCustomizationArrayForCurrentSelectedArea();	
		return $thisCustArray["final_image_name"];
	}

	/*function to get generated image filename*/
	public function generateImageName()
	{
		//delete previously generated image for this area if exists
		$thisCustArray = $this->getCustomizationArrayForCurrentSelectedArea();
		if( isset($thisCustArray["final_image_name"]) )
		{
			if( file_exists($this->baseDir.$thisCustArray["final_image_name"]) )
			{
				@unlink($this->baseDir.$thisCustArray["final_image_name"]);
			}
		}
		//generate a new name for the new image
		$thisCustArray["final_image_name"] = "images/temp/image_".rand().time().".jpg";
		$this->setCustomizationArrayForCurrentSelectedArea($thisCustArray);
		return $thisCustArray["final_image_name"];
	}
	public function getDefaultAreaId()
	{
		foreach($this->masterCustomizationArray["areas"] as $key=>$area)
		{
			if( $area["is_primary"] == 1)
				return $area["areaid"];
		}
	}
	public function getDefaultAreaName()
	{
		foreach($this->masterCustomizationArray["areas"] as $key=>$area)
		{
			if( $area["is_primary"] == 1)
				return $key;
		}
	}
	public function getDefaultOrientationId()
	{
		foreach($this->masterCustomizationArray["areas"][$this->getDefaultAreaName()]["orientations"] as $key=>$orientation)
		{
			if( $orientation["is_primary"] == 'Y' || $orientation["is_primary"] == 'y')
				return $orientation["orientationid"];
		}
	}
	// get all the area icon details ( to display area icons in customization page )
	public function getAreaIconDetails()
	{
		$areaicons = array();
		foreach($this->masterCustomizationArray["areas"] as $key=>$area)
		{
			$areaicon = array();
			$areaicon["area_icon"] = $area["area_icon"];
			$areaicon["name"] = $key;

			$areaicons[] =$areaicon;
		}
		return $areaicons;
	}
	// get all area names
	public function getAreaNames()
	{
		$thisCustArray = $this->getCustomizationArray();
		$areanames = array();
		foreach($thisCustArray["areas"] as $key=>$area)
		{
			$areanames[] = $key;
		}
		return $areanames;
	}
	//get all orientation names of current selected areaname
	public function getOrientationNames()
	{
		$thisCustArray = $this->getCustomizationArrayForCurrentSelectedArea();
		$orientationNames = array();
		foreach($thisCustArray["orientations"] as $key=>$area)
		{
			$orientationNames[] = $key;
		}
		return $orientationNames;
	}
	// get all objects of current selected orientation
	public function getObjectNames()
	{
		$thisCustArray = $this->getCustomizationArrayForCurrentSelectedOrientation();
		$objectNames["image"] = $thisCustArray["imageobjectid"];
		$objectNames["text"] = $thisCustArray["textobjectid"];

		return $objectNames;
	}
	
	public function isAllOrientationSingleTextLimited() {
		$thisCustArray = $this->getCustomizationArrayForCurrentSelectedArea();
		$orientationNames = array();
		$check = true;
		
		foreach($thisCustArray["orientations"] as $key=>$orientationinfo)
		{
			if(!empty($orientationinfo["pathForImagesToReplaceText"])) {
				continue;
			} else if($orientationinfo["imageAllowed"]==1 || $orientationinfo["imageCustAllowed"]==1) {
				return false;
			} else if(empty($orientationinfo["textAllowed"])) {
				return false;
			} else if(!empty($orientationinfo["textCustAllowed"])) {
				return false;
			} else if(!empty($orientationinfo["textLimitation"])) {
				$query = "select * from mk_text_font_detail where orientation_id=" . $orientationinfo["orientationid"]; 
				$results = db_query($query);
				$count = db_num_rows($results);
				if($count<=1) {
					continue;
				} else {
					return false;
				}
			}
		}
		
		return $check;
	}

	/*************************************************************************/
	/* GETTER & SETTER FOR CUSTOMIZATION (AREA,ORIENTATION,OBJECT level APIs)
	/*************************************************************************/

	// get current selected area name 
        public function getSelectedAreaName()
        {
                return $this->custArray["curr_selected_area"];
        }
        // set current selected area name
        public function setSelectedAreaName($areaname)
        {
                $this->custArray["curr_selected_area"] = $areaname;
        }
	

	// get customization array for the given area
	public function getCustomizationArrayForArea($areaname)
	{
		return $this->custArray["areas"][$areaname];
	}
	// set customization array for the given area
	public function setCustomizationArrayForArea($areaname,$thisCustArray)
	{
		$this->custArray["areas"][$areaname] = $thisCustArray;
	}

	// get  customization array for current selected area
	public function getCustomizationArrayForCurrentSelectedArea()
	{
		// get the current selected area name
		$currSelectedArea = $this->getSelectedAreaName();
		// get the current selected area array
		return $this->getCustomizationArrayForArea($currSelectedArea);

	}
	// set customization array for current selected area
	public function setCustomizationArrayForCurrentSelectedArea($thisCustArray)
	{
		// get the current selected area name
		$currSelectedArea = $this->getSelectedAreaName();
		// set the current selected area with updated cust Array
		$this->setCustomizationArrayForArea($currSelectedArea,$thisCustArray);
	}

	//get current selected orientation name
	public function getSelectedOrientationName()
	{
		$thisCustArray = $this->getCustomizationArrayForCurrentSelectedArea();
		return $thisCustArray["curr_selected_orientation"];
	}
	//set current selected orientation name
	public function setSelectedOrientationName($orientationname)
	{
		$thisCustArray = $this->getCustomizationArrayForCurrentSelectedArea();
		$thisCustArray["curr_selected_orientation"] = $orientationname;
		$this->setCustomizationArrayForCurrentSelectedArea($thisCustArray);
	}

	// get customization array for orientation
	public function getCustomizationArrayForOrientation($orientationname)
	{
		$thisCustArray = $this->getCustomizationArrayForCurrentSelectedArea();	
		return $thisCustArray["orientations"][$orientationname];
	}
	// set customization array for orientation
	public function setCustomizationArrayForOrientation($orientationname,$thisOrientCustArray)
	{
		$thisCustArray = $this->getCustomizationArrayForCurrentSelectedArea();
		$thisCustArray["orientations"][$orientationname] = $thisOrientCustArray;
		$this->setCustomizationArrayForCurrentSelectedArea($thisCustArray);
	}

	// set customization array for current selected orientation
	public function setCustomizationArrayForCurrentSelectedOrientation($thisOrientCustArray)
	{
		$orientationname = $this->getSelectedOrientationName();
		$this->setCustomizationArrayForOrientation($orientationname,$thisOrientCustArray);
	}
	public function getCustomizationArrayForCurrentSelectedOrientation()
	{
		$orientationname = $this->getSelectedOrientationName();
		return $this->getCustomizationArrayForOrientation($orientationname);
	}
	public function setSelectedObjectName($objectname)
	{
		$thisCustArray = $this->getCustomizationArrayForCurrentSelectedOrientation();
		$thisCustArray["curr_selected_object"] = $objectname;
		$this->setCustomizationArrayForCurrentSelectedOrientation($thisCustArray);
	}
	public function getSelectedObjectName()
	{
		$thisCustArray = $this->getCustomizationArrayForCurrentSelectedOrientation();
		return $thisCustArray["curr_selected_object"];
	}
	public function setCustomizationArrayForObject($objectname,$objectinfo)
	{
		$thisCustArray = $this->getCustomizationArrayForCurrentSelectedOrientation();
		$thisCustArray[$objectname] = $objectinfo;
		$this->setCustomizationArrayForCurrentSelectedOrientation($thisCustArray);
	}
	public function getCustomizationArrayForObject($objectname)
	{
		$thisCustArray = $this->getCustomizationArrayForCurrentSelectedOrientation();
		return $thisCustArray[$objectname];
	}
	public function setCustomizationArrayForCurrentSelectedObject($objectinfo)
	{
		$objectname = $this->getSelectedObjectName();
		$this->setCustomizationArrayForObject($objectname,$objectinfo);
	}
	public function getCustomizationArrayForCurrentSelectedObject()
	{
		$objectname = $this->getSelectedObjectName();
		return $this->getCustomizationArrayForObject($objectname);
	}
	public function getImageObjectIDForCurrentOrientation()
	{
		$thisCustArray = $this->getCustomizationArrayForCurrentSelectedOrientation();
		return $thisCustArray["imageobjectid"];
	}
	public function getTextObjectIDForCurrentOrientation()
	{
		$thisCustArray = $this->getCustomizationArrayForCurrentSelectedOrientation();
		return $thisCustArray["textobjectid"];
	}
	public function getTextObjectDataForCurrentOrientation()
	{
		$textObjectID = $this->getTextObjectIDForCurrentOrientation();
		$thisCustArray = $this->getCustomizationArrayForCurrentSelectedOrientation();
		return $thisCustArray[$textObjectID]["text"];
	}
	//to get limitation tpl for text customization 
	public function getTextLimitationTplForCurrentOrientation()
	{
		$thisCustArray = $this->getCustomizationArrayForCurrentSelectedOrientation();
		return $thisCustArray["textLimitationTpl"];
	}
	//to get text font colors for limited customization for orientation 
	public function getTextFontColorsForCurrentOrientation()
	{
		$thisCustArray = $this->getCustomizationArrayForCurrentSelectedOrientation();
		return $thisCustArray["textFontColors"];
	}
	//to get text font default color for limited customization for orientation 
	public function getTextColorDefaultForCurrentOrientation()
	{
		$thisCustArray = $this->getCustomizationArrayForCurrentSelectedOrientation();
		return $thisCustArray["textColorDefault"];
	}

	// To get text default font of limited customization for orientation 
	public function getTextDefaultFontForCurrentOrientation()
	{
		$thisCustArray = $this->getCustomizationArrayForCurrentSelectedOrientation();
		$textFontLimitations = $this->getTextFontLimitationsForCurrentOrientation();
		return $textFontLimitations["default"];		
		
		/*if($thisCustArray['textLimitation']){
			$defaultFontTypeQuery = db_query("select f.name as font_name from mk_text_font_detail d,mk_fonts f where f.id=d.font_id and d.orientation_id='{$thisCustArray['orientationid']}' and d.font_default='Y'");	
			$defaultFontTypeInfo = db_fetch_array($defaultFontTypeQuery);
			return $thisCustArray[$defaultFontTypeInfo['font_name']];
		}else
			return array();*/
		// correct below
	}
	
	// To get text default font size of limited customization for orientation 
	public function getTextDefaultFontSizeMaxcharsForCurrentOrientation()
	{
		$defaultFontDetailInfo = $this->getTextDefaultFontForCurrentOrientation();
		$defaultFontSizeAndMaxchars = array();
		
		if($defaultFontDetailInfo) {
			$defaultFontSizeAndMaxchars["font_size"] = $defaultFontDetailInfo["font_size"];
			$defaultFontSizeAndMaxchars["maxchars"] = $defaultFontDetailInfo["maxchars"];
		}
		
		return $defaultFontSizeAndMaxchars;
		
		/*if($thisCustArray['textLimitation']){
			$defaultFontSizeQuery = db_query("select font_size,maxchars from mk_text_font_detail where orientation_id='{$thisCustArray['orientationid']}' and font_default='Y' and size_default='Y'");	
			$defaultFontSizeAndMaxchars = db_fetch_array($defaultFontSizeQuery);
			return $defaultFontSizeAndMaxchars;
		}else
			return array();*/
	}
	
	
	//to get text default font sizes for orientation 
	public function getTextDefaultFontSizesForCurrentOrientation()
	{
		/*$defaultFontTypeInfo = $this->getTextDefaultFontForCurrentOrientation();
		if(!empty($defaultFontTypeInfo))
			return $defaultFontTypeInfo['textFontSizes'];
		else
			return array();*/
		
		$textDefaultFontSizes = array();
		$defaultFontDetailInfo = $this->getTextDefaultFontForCurrentOrientation();
		$textDefaultFontSizes[] = $defaultFontDetailInfo;
		return $textDefaultFontSizes;
	}
	
	public function getTextCharSizeTypeOrientationLimitations() {
		
		$thisCustArray = $this->getCustomizationArrayForCurrentSelectedArea();
		$textCharSizeType = array();
		 
		foreach ($thisCustArray["orientations"] as $key=>$orientationInfo) {
			if($thisCustArray["orientations"][$key]["textFontLimitations"]["default"]) {
				$defaultOrientationDetails = $thisCustArray["orientations"][$key]["textFontLimitations"]["default"];
				$textCharSizeType[$key] =  array("maxchars"=>$defaultOrientationDetails["maxchars"], "chartype"=>$defaultOrientationDetails["chartype"],
													"charcase"=>$defaultOrientationDetails["charcase"]);
			}
		}
		
		return $textCharSizeType;
		//echo "<PRE>"; print_r($textCharSizeType); exit;
		
	}
	
	public function getTextFontLimitationsForCurrentOrientation()
	{
		$thisCustArray = $this->getCustomizationArrayForCurrentSelectedOrientation();
		return $thisCustArray["textFontLimitations"];
	}
		
	public function getFewSpecificTextFontLimitationsForCurrentOrientation()
	{
		$thisCustArray = $this->getCustomizationArrayForCurrentSelectedOrientation();
		
		$textFontLimitations = $thisCustArray["textFontLimitations"];
		
		$limitedFontDetails = array();
		foreach ($textFontLimitations as $key=>$textFontDetailInfo) {
			$fontid = $textFontDetailInfo["font_id"];
			if($textFontDetailInfo["font_default"]=="Y") {
				unset($limitedFontDetails[$fontid]);
			}

			if(!isset($limitedFontDetails[$fontid])) {
				$limitedFontDetails[$fontid] = $textFontDetailInfo;
			}
		}
		
		return $limitedFontDetails;
	}
	
	
	/* not used 
	public function clearImageInCustomizationArrayForOrientation($thisCustArray,$basedir)
	{
		//echo "<pre>"; print_r( $thisCustArray ); exit;
		$imageId = $thisCustArray['imageobjectid'];
		$imagebaseInfo = $thisCustArray[$imageId]["baseinfo"];
		$imgPrefix = array("L","T","I");
		
		foreach($imgPrefix as $pre)
		{
			$imagePath = $basedir.$imagebaseInfo["baseimagepath_".$pre];
			@unlink($imagePath);
		}
		unset($thisCustArray[$imageId]);
		$thisCustArray['imageobjectid'] = "";

		$this->setCustomizationArrayForCurrentSelectedOrientation($thisCustArray);
		
	}*/
	public function hasImageInCurrentSelectedOrientation()
	{
		$thisCustArray = $this->getCustomizationArrayForCurrentSelectedOrientation();
		if (empty( $thisCustArray["imageobjectid"] ))
			return 0;
		else
			return 1;
	}
	public function isTextInputAllowed()
	{
		$thisCustArray = $this->getCustomizationArrayForCurrentSelectedOrientation();
		return $thisCustArray["textAllowed"];
	}
	public function isImageInputAllowed()
	{
		$thisCustArray = $this->getCustomizationArrayForCurrentSelectedOrientation();
		return $thisCustArray["imageAllowed"];
	}
	public function isImageCustomizationAllowedForOrientation()
	{
		$thisCustArray = $this->getCustomizationArrayForCurrentSelectedOrientation();
		return $thisCustArray["imageCustAllowed"];
	}
	public function isTextCustomizationAllowedForOrientation()
	{
		$thisCustArray = $this->getCustomizationArrayForCurrentSelectedOrientation();
		return $thisCustArray["textCustAllowed"];
	}	
	public function isTextLimitationForOrientation()
	{
		$thisCustArray = $this->getCustomizationArrayForCurrentSelectedOrientation();
		return $thisCustArray["textLimitation"];
	}
	
	public function hasPathForImagesToReplaceTextInCurrentSelectedOrientation()
	{
		$thisCustArray = $this->getCustomizationArrayForCurrentSelectedOrientation();
		return $thisCustArray["pathForImagesToReplaceText"];
	}
	
	
	public function hasTextInCurrentSelectedOrientation()
	{
		$thisCustArray = $this->getCustomizationArrayForCurrentSelectedOrientation();
		if (empty($thisCustArray["textobjectid"] ))
			return 0;
		else
			return 1;
	}

	/*************************************************************/
	/* Initialize customization info for uploaded image 		  /
	/*************************************************************/
	public function updateImageInfoInCustArea($objectid,$zIndex="", $customized = true)
	{
		$image_data = $this->globalDataArray[$objectid];
		$thisCustArray = $this->getCustomizationArrayForCurrentSelectedOrientation();

		$thisCustArray["imageobjectid"] = $objectid;	
		$thisCustArray["curr_selected_object"] = $objectid;
        if($customized){
            $thisCustArray["is_customized"] = 1;
        }

		$thisCustArray["bgcolor"] = "";

		$basewidth = $image_data["baseimagewidth"];
		$baseheight = $image_data["baseimageheight"];
		
		$imageArray["customizationinfo"] = $this->createCustomizationInfoForObjects($objectid,$basewidth,$baseheight,$zIndex);
		$imageArray["type"] = "image";
	
		$thisCustArray[$objectid] = $imageArray;	
		$this->setCustomizationArrayForCurrentSelectedOrientation($thisCustArray);
	
		// mark the area also as customized area
		$areaCustArray = $this->getCustomizationArrayForCurrentSelectedArea();
        if($customized){
            $areaCustArray["is_customized"] = 1;
        }
		$this->setCustomizationArrayForCurrentSelectedArea($areaCustArray);
	}
	
	public function updateTextInfoInCustArea($text,$zIndex="")
	{
		if( !empty($text) )
		{
			$thisCustArray = $this->getCustomizationArrayForCurrentSelectedOrientation();

			$newTextObj = false;
			$objectid = $thisCustArray["textobjectid"];
			if( empty($objectid) )
			{
				$objectid = md5(rand().time());
				$newTextObj = true;
			}
	
			$thisCustArray["textobjectid"] = $objectid;
			$thisCustArray["curr_selected_object"] = $objectid;
			$thisCustArray["is_customized"] = 1;
			
			//on text limitation takes default font color,size and type from DB(orientation) according to style, added by arun
			$isTextLimitation = $this->isTextLimitationForOrientation();
			
			if($newTextObj || $isTextLimitation) {

				$textArray["customizationinfo"] = $this->createCustomizationInfoForText($text,$zIndex);
				$textArray["type"] = "text";
				$textArray["text"] = $text;
				$thisCustArray[$objectid] = $textArray;
				
			} else {
				$thisCustArray[$objectid]["text"] = $text;
			}
			
			$this->setCustomizationArrayForCurrentSelectedOrientation($thisCustArray);
			// mark the area also as customized area
			$areaCustArray = $this->getCustomizationArrayForCurrentSelectedArea();
			$areaCustArray["is_customized"] = 1;
			$this->setCustomizationArrayForCurrentSelectedArea($areaCustArray);
		}
		else
		{
			$thisCustArray = $this->getCustomizationArrayForCurrentSelectedOrientation();
			$objectid = $thisCustArray["textobjectid"];
			$thisCustArray["textobjectid"] = "";
			if( $this->hasImageInCurrentSelectedOrientation() )
			{
				$thisCustArray["is_customized"] = 1;
				$thisCustArray["curr_selected_object"] = $this->getImageObjectIDForCurrentOrientation();
			}
			else
			{
				$thisCustArray["is_customized"] = 0;
				$thisCustArray["curr_selected_object"] = "";
			}
			unset($thisCustArray[$objectid]);
			$this->setCustomizationArrayForCurrentSelectedOrientation($thisCustArray);

			// mark the area also as customized area
			$areaCustArray = $this->getCustomizationArrayForCurrentSelectedArea();
			if( $thisCustArray["is_customized"] )
				$areaCustArray["is_customized"] = 1;
			else
				$areaCustArray["is_customized"] =  0;
			$this->setCustomizationArrayForCurrentSelectedArea($areaCustArray);	

		}
	}

	private function createCustomizationInfoForText($text,$zIndex)
	{
		$curr_selected_area = $this->getSelectedAreaName();
		$curr_selected_orientation = $this->getSelectedOrientationName();
		
		$hasPathForImagesToReplaceText = $this->hasImageInCurrentSelectedOrientation();
		
		//on text limitation takes default font color,size and type from DB(orientation), added by arun
		$isTextLimitation = $this->isTextLimitationForOrientation();
		
		if(!empty($hasPathForImagesToReplaceText)) {
			// We want to ignore setting default color or font, as text-as-images will be used in customization
		} else if($isTextLimitation){
			$TextDefaultColor = $this->getTextColorDefaultForCurrentOrientation();
			$TextDefaultFont = $this->getTextDefaultFontForCurrentOrientation();//an associattive array of fontid,name,image,ttf
			$defaultFontSizeAndMaxchars = $this->getTextDefaultFontSizeMaxcharsForCurrentOrientation();//default font size in inches and maxchars for the size
		}
		
		//get orientation width & height from master
		$owidth = $this->masterCustomizationArray["areas"][$curr_selected_area]["orientations"][$curr_selected_orientation]["size_x"];
		$oheight = $this->masterCustomizationArray["areas"][$curr_selected_area]["orientations"][$curr_selected_orientation]["size_y"];

		$owidth_in = $this->masterCustomizationArray["areas"][$curr_selected_area]["orientations"][$curr_selected_orientation]["width_in"];
		$oheight_in = $this->masterCustomizationArray["areas"][$curr_selected_area]["orientations"][$curr_selected_orientation]["height_in"];

		$customizationInfo = array();
		$customizationInfo["rotationAngle"] = 0;
		if( empty($zIndex) )
			$customizationInfo["zindex"] = $this->maxZIndexOfAllObjectsInThisOrientation();
		else
			$customizationInfo["zindex"] = $zIndex;
		$customizationInfo["dx"] = 0;
		$customizationInfo["dy"] = 0;
		$customizationInfo["color"] = (!empty($TextDefaultColor))?$TextDefaultColor:"BLACK";
		$customizationInfo["style"] = (!empty($TextDefaultFont))?"./fonts/{$TextDefaultFont['font_ttf_name']}":"./fonts/ARIALNB.TTF";
		
		//not accurate  conversion from inches to pixels, assumption:1"=30px
		$customizationInfo["size"] = (!empty($defaultFontSizeAndMaxchars))?(number_format($this->inchesToPixel * $defaultFontSizeAndMaxchars['font_size'],'0','','')):20;
		
		$scaleup_font_factor = ($this->IMAGE_DPI * $owidth_in) / $owidth;  // here scale_with should be owidth , if ratio is same
		$customizationInfo["scaleup_font_factor"] = $scaleup_font_factor;

		return $customizationInfo;
	}

	
	// create customization info
	private function createCustomizationInfoForObjects($objectid,$basewidth,$baseheight,$zIndex)
	{
		$curr_selected_area = $this->getSelectedAreaName();
		$curr_selected_orientation = $this->getSelectedOrientationName();
		//get orientation width & height from master
		$owidth = $this->masterCustomizationArray["areas"][$curr_selected_area]["orientations"][$curr_selected_orientation]["size_x"];
		$oheight = $this->masterCustomizationArray["areas"][$curr_selected_area]["orientations"][$curr_selected_orientation]["size_y"];

		$owidth_in = $this->masterCustomizationArray["areas"][$curr_selected_area]["orientations"][$curr_selected_orientation]["width_in"];
		$oheight_in = $this->masterCustomizationArray["areas"][$curr_selected_area]["orientations"][$curr_selected_orientation]["height_in"];
			
		$x_scale_factor = $owidth / ($owidth_in * $this->IMAGE_DPI ); 
		$y_scale_factor = $oheight / ( $oheight_in * $this->IMAGE_DPI );
	
		$max_width_allowed = $basewidth * $x_scale_factor;
		$max_height_allowed = $baseheight * $y_scale_factor;

		if( $max_width_allowed < $owidth && $max_height_allowed < $oheight )
		{
			if( $max_width_allowed < $max_height_allowed )
				$resizeratio = $max_width_allowed/$basewidth;
			else
				$resizeratio = $max_height_allowed/$baseheight;
		}
		else
		{
			if( ($max_width_allowed/$owidth) < ($max_height_allowed/$oheight) )
				$resizeratio = ($oheight/$baseheight);
			else
				$resizeratio = ($owidth/$basewidth);
		}
		$resizeW = $basewidth * $resizeratio;
		$resizeH = $baseheight * $resizeratio;

		$customizationinfo = array();
		$customizationinfo["rotationAngle"] = 0;
		if( empty($zIndex) )
			$customizationinfo["zindex"] = $this->maxZIndexOfAllObjectsInThisOrientation();
		else	
			$customizationinfo["zindex"] = $zIndex;
		//dx,dy wrt orientations centre(x,y)
		$customizationinfo["dx"] = 0;
		$customizationinfo["dy"] = 0;
		$customizationinfo["width"] = $resizeW;
		$customizationinfo["height"] = $resizeH;
		$customizationinfo["max_width_allowed"] = $max_width_allowed;
		$customizationinfo["max_height_allowed"] = $max_height_allowed;

                //ideally the ratio of ortwidth/ortheight shud be same as width_in/height_in, but they are not same, so considering the largest
                //ignore above comment - venu ( added for mug guiding image generation )
                $scale_with = ($owidth > $oheight)?$owidth:$oheight;

                $scaleup_width_factor = ($this->IMAGE_DPI * $owidth_in) / $owidth;  // here scale_with should be owidth , if ratio is same
                $scaleup_height_factor = ($this->IMAGE_DPI * $oheight_in) / $oheight; // here scale_with should be oheight , if ratio is same
                $customizationinfo["scaleup_width_factor"] = $scaleup_width_factor;
                $customizationinfo["scaleup_height_factor"] = $scaleup_height_factor;


		return $customizationinfo;
	}

	// as of now assumption is to have only two objects - one image & one text
	private function maxZIndexOfAllObjectsInThisOrientation()
	{
		$thisCustArray = $this->getCustomizationArrayForCurrentSelectedOrientation();	
		$imageObjectid = $thisCustArray["imageobjectid"];
		$textObjectid = $thisCustArray["textobjectid"];

		$imgZIndex = 0;
		$textZIndex = 0;

		if( isset($thisCustArray[$imageObjectid]["customizationinfo"]) )
			$imgZIndex = $thisCustArray[$imageObjectid]["customizationinfo"]["zindex"];
		if( isset($thisCustArray[$textObjectid]["customizationinfo"]) )
			$textZIndex = $thisCustArray[$textObjectid]["customizationinfo"]["zindex"];

		if( $imgZIndex > $textZIndex )
			return $imgZIndex+1;
		else
			return $textZIndex+1;		
	}

	/*********************************************************************************************/
	/* Action Handler :: Change object properties in customization array as per the action taken  /
	/*********************************************************************************************/
	public function applyCustomizationParams($params)
	{
		$ortCustArray = $this->getCustomizationArrayForCurrentSelectedOrientation();
		$action = $params["action"];
		$objectid = $ortCustArray["curr_selected_object"];
		
		switch($action)
		{
			case 'none' :	// No action specified , just load the customization final image
						break;
						
			case 'move':  	// move the object wrt current x,y
					$unit = $params["unit"];
					if(empty($unit)) {
						$unit=0;
					}
					$direction = $params["direction"];
					$this->moveObject($direction, $unit);
					break;
							
			case 'resize':	// resize the object wrt to (current width and height )or (current radius ) or (current xradius and yradius )
					$resize = $params["resizeby"];
					$unit = $params["unit"];
					if(empty($unit)) {
						$unit=0;
					}
					$this->resizeObject($resize, $unit);
					break;		
						
			case 'rotate':	// update rotation angle of the object
					$direction = $params["rotateby"];
					$this->rotateObject($direction);
					break; 
							
			case 'flip':		
							
			case 'flop':	// Flip or Flop the object vertically or horizontally
					$flipDirection = $_GET["direction"];
					$this->flipFlopObject($flipDirection);
					break;
				
			case 'fith':	// Adjust x,y coordinates of the object to fit horizontally to the customization area
					// Note this function may use move after updating x,y
					$this->centerAlignObjectHorizontally();
					break;
							
			case 'fitv':	// Adjust x,y coordinates of the object to fit vertically to the customization area
					// Note this function may use move after updating x,y
					$this->centerAlignObjectVertically();
					break;
						
			case 'specialeffect':
					// Special effects like Emboss, Charcoal , Glassy , Wet Floor .... 
					// This can be used for special products like Glass, Tile, Canvas, Wood, Iron, Gold etc
					$effect = $_GET["effect"];
					$this->applyEffectToObject($effect);
					break;
			case 'changearea':
					// Change the customization area
					$areaname = $params["areaname"];
					$this->changeCurrSelectedArea($areaname);
					break;
							
			case 'changeobject':
					// change the curr selected object 
					$oid = $_GET["objectid"];
					$this->changeCurrSelectedObjectId($oid);
					break;
			case 'delobj':
					$this->deleteCurrentSelectedObject();
					break;
			case 'changebgcolor':
					$color = $_GET["color"];
					$this->changeBgColor($color);
					break;
			case 'changefontcolor':
					$color = $_GET["color"];
					$this->changeFontColor($color);
					break;
			case 'changefont':
					$font = $_GET["font"];
					$this->changeFont($font);
					break;
		}
		return true;
	}
	
	//change current text's font color
	function changeFontColor($color)
	{
		$thisCustArray = $this->getCustomizationArrayForCurrentSelectedObject();
		$custinfo = $thisCustArray["customizationinfo"];
		$custinfo["color"] = "#$color";
		$thisCustArray["customizationinfo"] = $custinfo;
		$this->setCustomizationArrayForCurrentSelectedObject($thisCustArray);
	}
	
	//change current text's font
	function changeFont($font)
	{
		$thisCustArray = $this->getCustomizationArrayForCurrentSelectedObject();
		$custinfo = $thisCustArray["customizationinfo"];
		$custinfo["style"] = $font;
		$thisCustArray["customizationinfo"] = $custinfo;
		$this->setCustomizationArrayForCurrentSelectedObject($thisCustArray);
	}

	//change background color of current selected orientation
	function changeBgColor($color)
	{
		$thisCustArray = $this->getCustomizationArrayForCurrentSelectedOrientation();
		if( !empty($color) )
			$thisCustArray["bgcolor"] = "#$color";
		else
			$thisCustArray["bgcolor"] = "";

		$this->setCustomizationArrayForCurrentSelectedOrientation($thisCustArray);
	}

	//delete current selected object info in current selected orientation ( note : this is only to delete image )
	function deleteCurrentSelectedObject()
	{
		$thisCustArray = $this->getCustomizationArrayForCurrentSelectedOrientation();
		$objectid = $thisCustArray["imageobjectid"];
		$thisCustArray["imageobjectid"] = "";
		if($this->hasTextInCurrentSelectedOrientation()) {
			$thisCustArray["is_customized"] = 1;
			$thisCustArray["curr_selected_object"] = $this->getTextObjectIDForCurrentOrientation();
			 
		} else {
			$thisCustArray["is_customized"] = 0;
			$thisCustArray["curr_selected_object"] = "";
		}
		
		unset($thisCustArray[$objectid]);

		// When removing an image, we also erase existing background associated with it.
		// Note: Background colors are applied only on image customizations.
		// So we can ignore the case, if text customization has background color. 
		// Text customization foreground color is stored at [objectid]["customizationinfo"]["color"].
		$thisCustArray["bgcolor"]="";
		
		$this->setCustomizationArrayForCurrentSelectedOrientation($thisCustArray);

		// mark the area also as customized area or not
		$areaCustArray = $this->getCustomizationArrayForCurrentSelectedArea();
		if( $thisCustArray["is_customized"] ) {
			$areaCustArray["is_customized"] = 1;
		} else {
			$areaCustArray["is_customized"] =  0;
		}

		
		$this->setCustomizationArrayForCurrentSelectedArea($areaCustArray);	
	}

	// flip the object along the flip direction and add action to LOG
	private function flipFlopObject($flipdirection)
	{
	}
		
	// rotate object by rotateAngel and add action to LOG
	private function rotateObject($direction)
	{
		$thisCustArray = $this->getCustomizationArrayForCurrentSelectedObject();
		$custinfo = $thisCustArray["customizationinfo"];
		$type = $thisCustArray["type"];
		if( $direction == "cw" )
			$sign = -1;
		else
		if( $direction == "acw")
			$sign = 1;
		$custinfo["rotationAngle"] = $custinfo["rotationAngle"] + ($sign * $this->rotateBy);
		$thisCustArray["customizationinfo"] = $custinfo;
		$this->setCustomizationArrayForCurrentSelectedObject($thisCustArray);
	}
		
	// apply effect to the object and add the action to LOG
	private function applyEffectToObject($effect)
	{
	}
		
	// resize the object by resize percentage
	private function resizeObject($resize, $unit=0)
	{
		$thisCustArray = $this->getCustomizationArrayForCurrentSelectedObject();
		$custinfo = $thisCustArray["customizationinfo"];
		$isSizeDynamic = 0;
		$type = $thisCustArray["type"];
		if(empty($unit)) {
			$unit=1;
		}
			
		if( $resize == "out")
			$sign = 1 * $unit;
		else if( $resize == "in")
			$sign = -1 * $unit;
		else{
			//in case resize on dynamic value(in inches)
			$sign = 1 * $unit;
			$isSizeDynamic = 1;
		}
			
			
		if( $type == "image" )
		{
			$width_rvalue = $custinfo["width"] * ($sign*$this->resizeBy)/100;
			$height_rvalue = $custinfo["height"] * ($sign*$this->resizeBy)/100;
			$custinfo["width"] = $custinfo["width"] + $width_rvalue;
			$custinfo["height"] = $custinfo["height"] + $height_rvalue;
		}
		else
		{
			if($isSizeDynamic) {
				//not accurate of conversion from inches to pixels, assumption:1"=30px
				$custinfo["size"] = number_format(($this->inchesToPixel * $resize),'0','','');
				
			} else {
				$custinfo["size"] = $custinfo["size"]+($sign * $this->resizeBy);
			}
			
		}
		$thisCustArray["customizationinfo"] = $custinfo;
		$this->setCustomizationArrayForCurrentSelectedObject($thisCustArray);
	}

		
	// update (centery) to customization area center vertically
	private function centerAlignObjectVertically()
	{
		$thisCustArray = $this->getCustomizationArrayForCurrentSelectedObject();
		$custinfo = $thisCustArray["customizationinfo"];
		$custinfo["dy"] = 0;
		$thisCustArray["customizationinfo"] = $custinfo;
		$this->setCustomizationArrayForCurrentSelectedObject($thisCustArray);
	}
		
	// update (centerx) to customization area center horizontally
	private function centerAlignObjectHorizontally()
	{
		$thisCustArray = $this->getCustomizationArrayForCurrentSelectedObject();
        $custinfo = $thisCustArray["customizationinfo"];
	    $custinfo["dx"] = 0;
		$thisCustArray["customizationinfo"] = $custinfo;
		$this->setCustomizationArrayForCurrentSelectedObject($thisCustArray);
	}
		
	// move (centerx,centery) of the object by dx,dy 
	private function moveObject($direction, $unit=0)
	{
		
		$thisCustArray = $this->getCustomizationArrayForCurrentSelectedObject();
		$type = $thisCustArray["type"];
		$custinfo = $thisCustArray["customizationinfo"];
		
		if(empty($unit))
			$unit=1;

		switch($direction) {

			case "tl":
					$custinfo["dx"] = $custinfo["dx"]-($this->moveBy * $unit);
					$custinfo["dy"] = $custinfo["dy"]-($this->moveBy * $unit);
					break;
					
			case "t":
					$custinfo["dy"] = $custinfo["dy"]-($this->moveBy * $unit);
					break;
			case "tr":
					$custinfo["dx"] = $custinfo["dx"]+($this->moveBy * $unit);
					$custinfo["dy"] = $custinfo["dy"]-($this->moveBy * $unit);
					break;
			case "r":	
					$custinfo["dx"] = $custinfo["dx"]+($this->moveBy * $unit);
					break;
			case "m":	
					$custinfo["dx"] = $custinfo["dy"] = 0;
					break;
			case "l":	
					$custinfo["dx"] = $custinfo["dx"]-($this->moveBy * $unit);
					break;
			case "bl":
					$custinfo["dx"] = $custinfo["dx"]-($this->moveBy * $unit);
					$custinfo["dy"] = $custinfo["dy"]+($this->moveBy * $unit);
					break;
			case "b":
					$custinfo["dy"] = $custinfo["dy"]+($this->moveBy * $unit);
					break;
			case "br":
					$custinfo["dx"] = $custinfo["dx"]+($this->moveBy * $unit);
					$custinfo["dy"] = $custinfo["dy"]+($this->moveBy * $unit);
					break;
		}		
		$thisCustArray["customizationinfo"] = $custinfo;
		$this->setCustomizationArrayForCurrentSelectedObject($thisCustArray);
	}
	// change current selected area id
	private function changeCurrSelectedArea($areaname)
	{
		$this->custArray["curr_selected_area"] = $areaname;
	}
		
	// change current selected object id
	private function changeCurrSelectedObjectId($oid)
	{
		$thisCustArray = $this->getCustomizationArrayForCurrentSelectedOrientation();
		$thisCustArray["curr_selected_object"] = $oid;
		$this->setCustomizationArrayForCurrentSelectedOrientation($thisCustArray);
	}



	// *************************
	// Imagick helper functions
	// *************************
	private function createImageFromFile( $imagepath )
	{
		try {
			return new Imagick($imagepath);
		}
		catch(Exception $e)
		{
			echo $e->getMessage();
		}
	}
	private function cloneImage($im)
	{
		return $im->clone();
	}
	private function createImage($width,$height,$createJPG=false,$bgcolor='')
	{
		$im =  new Imagick();
		if( $createJPG )
		{
			$im->newImage($width, $height,new ImagickPixel(),"jpg");
		}
		else
		{
			if( empty($bgcolor) )
			{
				$color = 'white'; $trans_color = 'white';
			}
			else
			{
				$color = $bgcolor; $trans_color = "";
			}
			$im->newImage($width, $height,new ImagickPixel($color));
$im->setImageFormat('png');
			if( !empty($trans_color) )
				$im->transparentPaintImage(new ImagickPixel($trans_color),0,0,false);
		}
		return $im;

	}
	private function mergeImages($background,$foreground,$const_composite,$x,$y,$const_gravity)
	{
		$fGeo = $foreground->getImageGeometry();	
		$bGeo = $background->getImageGeometry();
		switch($const_gravity)  // imagickmagick is not compiled against the latst vesio, setImageGravity function is not supported -so writing our own
		{
			case Imagick::GRAVITY_CENTER:
					$centerx = ($bGeo["width"]/2) - ($fGeo["width"]/2) + $x;
					$centery = ($bGeo["height"]/2) - ($fGeo["height"]/2) + $y;
					$background->compositeImage($foreground,$const_composite,$centerx,$centery);
					break;
			case Imagick::GRAVITY_NORTHWEST:
					$background->compositeImage($foreground,$const_composite,$x,$y);
					break;
			case Imagick::GRAVITY_NORTH:
					$centerx = ($bGeo["width"]/2) - ($fGeo["width"]/2) + $x;
					$background->compositeImage($foreground,$const_composite,$centerx,$y);
					break;
		}
					
		return $background;
	}
	private function resizeImage($im,$width,$height)
	{
		$im->resizeImage($width,$height,Imagick::FILTER_LANCZOS,1);
		return $im;
	}
	private function rotateImage($im,$rotateAngle)
	{
		$im->setImageFormat("png");
		$im->rotateImage(new ImagickPixel('white'), $rotateAngle);
		//$im->transparentPaintImage(new ImagickPixel('white'),0,0,false);
		return $im;
	}
	public function scaleImagesEx($srcFile,$allTargetFiles)
	{
		$orig_src = $this->createImageFromFile($srcFile);	
		foreach($allTargetFiles as $thisTgtFile)
		{
			$width = $thisTgtFile["width"];
			$height = $thisTgtFile["height"];
			$tgtFile = $thisTgtFile["tgtFile"];
			$trans = $this->createImage($width,$height);
			$clone_src = $this->cloneImage($orig_src);
			$clone_src->scaleImage($width,0);
			$tgt = $this->mergeImages($trans,$clone_src,Imagick::COMPOSITE_DEFAULT,0,0,Imagick::GRAVITY_CENTER);
			$tgt->writeImage($tgtFile);

			$trans->destroy();
			$clone_src->destroy();
			$tgt->destroy();
		}
		$orig_src->destroy();
		return $allTargetFiles;
	}
}

// End Class CustomizationHelper
