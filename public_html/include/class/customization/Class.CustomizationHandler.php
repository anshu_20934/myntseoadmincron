<?php
include_once("../../func/func.mkcore.php");
include_once("../../../mkImageTransform.php");
include_once("../customizationtoolsapis.php");
include_once("../../func/func.randnum.php");
include_once("../../TempImages.php");
class CustomizationHandler
{
	public function __construct()
	{
	}

	public function changeSelectedTemplate()
	{
	}
	public function changeSelectedAddon($selectedCustomizationAreaId,$addonId,$selectedOrientation,&$custarray,$chosenProductConfiguration,$smarty,$customizationAreaImage,$action)
	{
		// step 1: Load large image and new coordinates from orientation add on map

		$sql = "select 	c.area_id as area_id , 
				po.orientation_id as id ,
				po.start_x as start_x,
				po.start_y as start_y,
            			po.size_x as size_x,
				po.size_y as size_y, 
				po.width_in,
				po.height_in,
				po.image_path as template_image 
				from mk_productoption_orientation_map po join mk_customization_orientation c on po.orientation_id = c.id
				where po.orientation_id=$selectedOrientation and po.product_option_id=$addonId";
		$orientationDetails = func_query($sql);
		$image = imagecreatefrompng($orientationDetails[0]["template_image"]);

		// Step 2: image create from png
		 $bg = imagecolorallocate($image, 0, 0, 0);
		 $w = imagecolorallocate($image, 255, 255, 255);
		 $b = imagecolorallocate($image, 0, 0, 0);
		 $style = array($b,$b,$b,$b,$b,$b,$b,$b,$b,$b,$b,$b,$b,$b,$b,
					$w,$w,$w,$w,$w,$w,$w,$w,$w,$w,$w,$w,$w,$w,$w);
		imagesetstyle($image,$style);
		imagesetthickness($image,1);
		##generate the dotted boundary on customization area
		$this->generate_customization_layout($custarray,$chosenProductConfiguration,$orientationDetails,$image,$selectedCustomizationAreaId);
	
	}
	private function generate_customization_layout(&$custarray,$chosenProductConfiguration,$orientationDetails,$image,$customizationAreaId)
        {
		global $bgcolor,$isJPG;
        global $weblog;
		$imgToDisplay = '';
		foreach($orientationDetails as $_k => $_value)
		{

			$custarray[$_value['id']]['x'] =  $_value['start_x'];
			$custarray[$_value['id']]['y'] =  $_value['start_y'];
			$custarray[$_value['id']]['w'] =  $_value['size_x'];
			$custarray[$_value['id']]['h'] =  $_value['size_y'];
			$custarray[$_value['id']]['i'] =  $_value['template_image'];


			if (!empty($custarray[$_value['id']]['bgcolor'])) 
				$bgcolor = $custarray[$_value['id']]['bgcolor'];
			else $bgcolor = '';
			
			if (empty($bgcolor))
			{
				imagepolygon($image,array (
									$_value['start_x'], $_value['start_y'],
									$_value['start_x'], $_value['start_y']+$_value['size_y'],
									$_value['start_x']+$_value['size_x'], $_value['start_y']+$_value['size_y'],
									$_value['start_x']+$_value['size_x'], $_value['start_y']
								),
								4,
								IMG_COLOR_STYLED);

			}
			else
			{
				$bgcolorRGB = html2rgb($bgcolor);
				list($rgb, $r, $g, $b) = split('[(,)]', $bgcolorRGB);
				$backgroundColor = imagecolorallocate($image,$r,$g,$b);
				imagefilledpolygon($image,array (
									$_value['start_x'], $_value['start_y'],
									$_value['start_x'], $_value['start_y']+$_value['size_y'],
									$_value['start_x']+$_value['size_x'], $_value['start_y']+$_value['size_y'],
									$_value['start_x']+$_value['size_x'], $_value['start_y']
								),
								4,
								$backgroundColor);

			}
			//check the previous color stored for bgcolor in the session

			$len = 4;
			if ($isJPG)
			{
				$tempBaseFile = './images/customized/cust1'.time().get_rand_id($len).'.jpg';
				if(!imagejpeg($image,$tempBaseFile))
				    $weblog->info("unable to create file $tempBaseFile in Class.CustomizationHandler.php");
			}else
			{
				$tempBaseFile = './images/customized/cust1'.time().get_rand_id($len).'.png';
				if(!imagepng($image,$tempBaseFile))
					$weblog->info("unable to create file $tempBaseFile in Class.CustomizationHandler.php");
			}

			addToTempImagesList($tempBaseFile);
			if(!empty($custarray[$_value['id']]['imageAfterResizing']))
				$uploaded_image  = $custarray[$_value['id']]['imageAfterResizing']; // resized image
			else
				$uploaded_image  = $chosenProductConfiguration[$_value['area_id']."_".$_value['id']."_uploadedImagePath"];

			$customizationAreaWidth = $_value['size_x'];
			$customizationAreaHeight = $_value['size_y'];
			if ( $uploaded_image != "")
			{
				if ($isJPG)
				{
					$pasteImage=imagecreatefromjpeg($uploaded_image);
				}
				else
				{
					$pasteImage=imagecreatefrompng($uploaded_image);
				}
				
				$imageproperties =getimagesize($uploaded_image);
				
				$width_orig=$imageproperties[0];
				$height_orig=$imageproperties[1];
				$ratio_orig = $width_orig/$height_orig;
				$widthMax = max($customizationAreaWidth,$width_orig);
				$heightMax = max($customizationAreaHeight,$height_orig);
				$custarray[$_value['id']]['pasteimagepath'] = $uploaded_image;

				$customizatio_aspect_ratio =$customizationAreaWidth/$customizationAreaHeight;
				$srcx=0;
				$srcy=0;
				$height_new=0;
				$width_new=0;
				if($customizatio_aspect_ratio >= 1)
				{
					$height_new = $width_orig/$customizatio_aspect_ratio;
					$width_new=$width_orig;
					$srcy=($height_orig-$height_new)/2;
				}
				else
				{
					$width_new = $height_orig * $customizatio_aspect_ratio;
					$height_new=$height_orig;
					$srcx=($width_orig-$width_new)/2;
				}

				$tempimg ='';
				$path='';
				$imageCropper = new mkImageTransform();
				$imageCropper->sourceFile = $uploaded_image;
				$imageCropper->maintainAspectRatio = 0;
				$imageCropper->resizeToWidth = $width_new;

				$imageCropper->resizeToHeight = $height_new;	
 
				if ($imageproperties['mime'] =='image/jpeg'){
					$imageCropper->jpegOutputQuality =100;
					$imageCropper->targetFile = './images/customized/cust_crop.'.time().get_rand_id($len).'.jpg';
				}else{
					$imageCropper->targetFile = './images/customized/cust_crop.'.time().get_rand_id($len).'.png';
				}	 
				$imageCropper->crop($srcx,$srcy);

				if ($imageproperties['mime'] =='image/jpeg')
				{
					imagecreatefromjpeg($imageCropper->targetFile);
				}
				else
				{
					imagecreatefrompng($imageCropper->targetFile);
				}
				$path=$imageCropper->targetFile;
	
				$widthinCA =$customizationAreaWidth;//width of the image pasted in customization area
				$heightinCA=$customizationAreaHeight;//height of the image pasted in customization area
				
				
//				if (($width_orig > $customizationAreaWidth) && ($width_orig >= $height_orig))
//				{
//					$widthinCA = $customizationAreaWidth;
//					$heightinCA = $widthinCA/$ratio_orig;
//					if ($heightinCA > $customizationAreaHeight)
//					{
//						$heightinCA = $customizationAreaHeight;
//						$widthinCA = $heightinCA * $ratio_orig;
//					}
//				}
//				elseif (($height_orig > $customizationAreaHeight) && ($height_orig >= $width_orig))
//				{
//					$heightinCA = $customizationAreaHeight;
//					$widthinCA = $ratio_orig * $heightinCA;
//					if ($widthinCA > $customizationAreaWidth)
//					{
//						$widthinCA = $customizationAreaWidth;
//						$heightinCA = $widthinCA/$ratio_orig;
//					}
//				}
//				elseif (($width_orig <= $customizationAreaWidth) && ($width_orig >= $height_orig))
//				{
//					$widthinCA = $width_orig;
//					$heightinCA = $widthinCA/$ratio_orig;
//					if ($heightinCA > $customizationAreaHeight)
//					{
//						$heightinCA = $customizationAreaHeight;
//						$widthinCA = $heightinCA * $ratio_orig;
//					}
//				}
//				elseif (($height_orig <= $customizationAreaHeight) && ($height_orig >= $width_orig))
//				{
//					$heightinCA = $height_orig;
//					$widthinCA = $ratio_orig * $heightinCA;
//					if ($widthinCA > $customizationAreaWidth)
//					{
//						$widthinCA = $customizationAreaWidth;
//						$heightinCA = $widthinCA/$ratio_orig;
//					}
//				}
//				
				//TODO carry out resizing only for those images which are larger in size than cust area
				$imageManipulator = new mkImageTransform();
				$imageManipulator->sourceFile = $path;
				if ($isJPG)
				{
					$imageManipulator->jpegOutputQuality = 100;
					$imageManipulator->targetFile = './images/customized/cust.'.time().get_rand_id($len).'.jpg';
				}
				else
				{
					$imageManipulator->targetFile = './images/customized/cust.'.time().get_rand_id($len).'.png';
				}
				addToTempImagesList($imageManipulator->targetFile);
				$len =4;
				$imageManipulator->maintainAspectRatio = 0;
				//list($widthinCA,$heightinCA) = _rethinkImageDimensions($customizationAreaId, $customizationAreaHeight, $customizationAreaWidth, $width_orig, $height_orig);
				$imageManipulator->resizeToWidth = $widthinCA;
				$imageManipulator->resizeToHeight = $heightinCA;
				$imageManipulator->resize();
				if ($isJPG)
				{
					$pasteImage = imagecreatefromjpeg($imageManipulator->targetFile);
				}
				else
				{
					$pasteImage = imagecreatefrompng($imageManipulator->targetFile);
				}
				//store the resized file path in the session array
				$custarray[$_value['id']]['pastefile'] =  $imageManipulator->targetFile;
				//by default dx,dy are 0
				$dx = ($customizationAreaWidth - $widthinCA)/2;
				$dy = ($customizationAreaHeight - $heightinCA)/2;
				$len = 4;
				imagecopyresampled($image, $pasteImage, $_value['start_x']+$dx,$_value['start_y']+$dy, 0, 0, $widthinCA, $heightinCA, $widthinCA, $heightinCA);
				if ($isJPG)
				{
					$tempCustImageFile = "./images/customized/custtemp".time().get_rand_id($len).".jpg";
					if(!imagejpeg($image,$tempCustImageFile))
						$weblog->info("unable to create file $tempCustImageFile in Class.CustomizationHandler.php");

				}
				else
				{
					$tempCustImageFile = "./images/customized/custtemp".time().get_rand_id($len).".png";
					if(!imagepng($image,$tempCustImageFile))
						$weblog->info("unable to create file $tempCustImageFile in Class.CustomizationHandler.php");
				}
				
				addToTempImagesList($tempCustImageFile);
				###$imageCleanUpList[] = $tempCustBgImageFile;

				$custarray[$_value['id']]['dxstart'] = $dx;
				$custarray[$_value['id']]['dystart'] = $dy;
				$custarray[$_value['id']]['generatedimage'] = $tempCustImageFile;
				$custarray[$_value['id']]['generatedimagenotext'] = $tempCustImageFile;
				$custarray[$_value['id']]['orientationid'] = $_value['id'];
				$custarray['common_generated_img'] = $tempCustImageFile;
				$custarray['common_generated_final_image'] = $tempCustImageFile;
				$imgToDisplay = $tempCustImageFile;
				//this code is for handling the situation wherein somebody uploads an image after putting some text in the customization
				//area
			}
			else
			{
				if ($isJPG)
				{
					$tempCustImageFile = "./images/customized/custtemp".time().get_rand_id($len).".jpg";
					if(!imagejpeg($image,$tempCustImageFile))
						$weblog->info("unable to create file $tempCustImageFile in Class.CustomizationHandler.php");
				}
				else
				{
					$tempCustImageFile = "./images/customized/custtemp".time().get_rand_id($len).".png";
					if(!imagepng($image,$tempCustImageFile))		
						$weblog->info("unable to create file $tempCustImageFile in Class.CustomizationHandler.php");
				}
				addToTempImagesList($tempCustImageFile);
				$custarray[$_value['id']]['generatedimage'] = $tempCustImageFile;
				$custarray[$_value['id']]['generatedimagenotext'] = $tempCustImageFile;
				$custarray['common_generated_img'] = $tempCustImageFile;
				$custarray['common_generated_final_image'] = $tempCustImageFile;
			}
		}
	}
	public function createDefaultCustomization()
	{
	}
	private function placeImageOnCustomization()
	{
	}
	private function placeTextOnCustomization()
	{
	}
	private function moveOverlayObject()
	{
	}
	private function rotateOverlayObject()
	{
	}
	private function fitOverlayOjectToVertical()
	{
	}
	private function fitOVerlayObjectToHorizontal()
	{
	}
	private function zoomOverlayObject()
	{
	}
	private function fillBgColor()
	{
	}
	private function setFontColor()
	{
	}

	// returns an object which holds all data required to do customization
	private function parseRequestParams()
	{
		//setter and getter menthods for every attribute like dx,dy,width...
	}

}

?>

