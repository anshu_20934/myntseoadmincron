<?php

class CustomizationData
{
	private $productStyleId;

	public function __construct($productStyleId)
	{
		$this->productStyleId = $productStyleId;
	}

	private function loadCustomizationData_preLoaded()
	{
			$thisCustomizationArray = $this->loadCustomizationData();
			return $thisCustomizationArray;
	}

	private function loadMasterCustomizationData_preLoaded()
	{
			$thisCustomizationArray = $this->loadMasterCustomizationData();
			return $thisCustomizationArray;
	}

	private function loadMasterCustomizationData()
	{
		$thissCustArray = array();	
		$productStyleId = $this->productStyleId;
	
		// load all areas for this style
		$areaQuery = db_query("select id,name,image_l,image_t,isprimary from mk_style_customization_area where style_id = $productStyleId");
		$allAreas = array();
		while($area = db_fetch_array($areaQuery) )
		{	
			$areaname = $area["name"];
			$areaid  = $area["id"];
			$thisArea = array(
								"areaid"=>$area["id"],
								"price"=> "notused",
								"bgimage"=>$area["image_l"],
								"area_icon"=>$area["image_t"],
								"is_primary"=>$area["isprimary"],
								"start_x"=>0,
								"start_y"=>0,
								"size_x"=>0,
								"size_y"=>0
							);
			// load all orientations for this style & area      
			$orientationQuery = db_query("select id,location_name,start_x,start_y,size_x,size_y,width_in,height_in,orientation_order,ort_default from  mk_customization_orientation where area_id = $areaid");
			$allOrientations = array();
			while($orientation = db_fetch_array($orientationQuery))
			{
				$orientationname = $orientation["location_name"];
				$thisOrientation = array(
											"orientationid" => $orientation["id"],
											"is_primary" => $orientation["ort_default"],
											"start_x" => $orientation["start_x"],
											"start_y" => $orientation["start_y"],
											"size_x" => $orientation["size_x"],
											"size_y" => $orientation["size_y"],
											"width_in" => $orientation["width_in"],
											"height_in" => $orientation["height_in"],
											"orientation_order" => $orientation["orientation_order"]
										);
				$allOrientations[$orientationname] = $thisOrientation;
			}
			// A tweak to define customization co-ordinates at the area level 
			// Remove this logic when we have customization co-ordinates defined for areas also
			// From all orientations in this area , calculate area co-ordinates
			// area["start_x"] = min(start_x) of all orientations
			// area["start_y"] = min(start_y) of all orientations
			// area["end_x"] =  max(end_x) of all orientations - end_x of orientation is it's (start_x + size_x)
			// area["end_y"] = max(end_y) of all orientations - end_y is orientation is it's (start_y + size_y)
			$start_x = 1000000; // some large number
			$start_y = 1000000; // some large number
			$end_x = -100000; // some small number
			$end_y = -100000; // some small number
			foreach($allOrientations as $orientation)
			{
				if( $start_x > $orientation["start_x"] )
					$start_x = $orientation["start_x"];
				if( $start_y > $orientation["start_y"] )
					$start_y = $orientation["start_y"];
				if( $end_x < $orientation["start_x"]+$orientation["size_x"])
					$end_x = $orientation["start_x"]+$orientation["size_x"];
				if( $end_y < $orientation["start_y"]+$orientation["size_y"])
					$end_y = $orientation["start_y"]+$orientation["size_y"];
			}
			$thisArea["start_x"] = $start_x;
			$thisArea["start_y"] = $start_y;
			$thisArea["size_x"] = $end_x-$start_x;
			$thisArea["size_y"] = $end_y-$start_y;
			// Now move all orientations start_x start_y wrt to the area's start_x,start_y ( not the bgimage's start_x,start_y )
			foreach($allOrientations as $key=>$orientation)
			{
				//adjust start_x,start_y wrt to area start_x,start_y
				$orientation["start_x"] = $orientation["start_x"] - $thisArea["start_x"];
				$orientation["start_y"] = $orientation["start_y"] - $thisArea["start_y"];
				//write back
				$allOrientations[$key] = $orientation;
			}
			$thisArea["orientations"] = $allOrientations;
			$allAreas[$areaname] = $thisArea;

		}
		$thisCustArray["areas"] = $allAreas;

		return $thisCustArray; 

	}
    private function loadMasterCustomizationDataForOldSmallDesign_preLoaded()
        {
		$thissCustArray = array();
		$productStyleId = $this->productStyleId;

		// load all areas for this style
		$areaQuery = db_query("select id,name,image_l,image_t,isprimary from mk_style_customization_area where style_id = $productStyleId");
		$allAreas = array();
		while($area = db_fetch_array($areaQuery) )
		{
			$areaname = $area["name"];
			$areaid  = $area["id"];
			$thisArea = array(
								"areaid"=>$area["id"],
								"price"=> "notused",
								"bgimage"=>$area["image_l"],
								"area_icon"=>$area["image_t"],
								"is_primary"=>$area["isprimary"],
								"start_x"=>0,
								"start_y"=>0,
								"size_x"=>0,
								"size_y"=>0
							);
			// load all orientations for this style & area
			$orientationQuery = db_query("select id,location_name,start_x,start_y,size_x,size_y,width_in,height_in,orientation_order,ort_default from  mk_customization_orientation where area_id = $areaid");
			$allOrientations = array();
			while($orientation = db_fetch_array($orientationQuery))
			{
				$orientationname = $orientation["location_name"];
				$thisOrientation = array(
											"orientationid" => $orientation["id"],
											"is_primary" => $orientation["ort_default"],
											"start_x" => $orientation["start_x"],
											"start_y" => 80,
											"size_x" => $orientation["size_x"],
											"size_y" => 156,
											"width_in" => 8,
											"height_in" => 8,
											"orientation_order" => $orientation["orientation_order"]
										);
				$allOrientations[$orientationname] = $thisOrientation;
			}
			// A tweak to define customization co-ordinates at the area level
			// Remove this logic when we have customization co-ordinates defined for areas also
			// From all orientations in this area , calculate area co-ordinates
			// area["start_x"] = min(start_x) of all orientations
			// area["start_y"] = min(start_y) of all orientations
			// area["end_x"] =  max(end_x) of all orientations - end_x of orientation is it's (start_x + size_x)
			// area["end_y"] = max(end_y) of all orientations - end_y is orientation is it's (start_y + size_y)
			$start_x = 1000000; // some large number
			$start_y = 1000000; // some large number
			$end_x = -100000; // some small number
			$end_y = -100000; // some small number
			foreach($allOrientations as $orientation)
			{
				if( $start_x > $orientation["start_x"] )
					$start_x = $orientation["start_x"];
				if( $start_y > $orientation["start_y"] )
					$start_y = $orientation["start_y"];
				if( $end_x < $orientation["start_x"]+$orientation["size_x"])
					$end_x = $orientation["start_x"]+$orientation["size_x"];
				if( $end_y < $orientation["start_y"]+$orientation["size_y"])
					$end_y = $orientation["start_y"]+$orientation["size_y"];
			}
			$thisArea["start_x"] = $start_x;
			$thisArea["start_y"] = $start_y;
			$thisArea["size_x"] = $end_x-$start_x;
			$thisArea["size_y"] = $end_y-$start_y;
			// Now move all orientations start_x start_y wrt to the area's start_x,start_y ( not the bgimage's start_x,start_y )
			foreach($allOrientations as $key=>$orientation)
			{
				//adjust start_x,start_y wrt to area start_x,start_y
				$orientation["start_x"] = $orientation["start_x"] - $thisArea["start_x"];
				$orientation["start_y"] = $orientation["start_y"] - $thisArea["start_y"];
				//write back
				$allOrientations[$key] = $orientation;
			}
			$thisArea["orientations"] = $allOrientations;
			$allAreas[$areaname] = $thisArea;

		}
		$thisCustArray["areas"] = $allAreas;

		return $thisCustArray;

	}
    private function loadMasterCustomizationDataPublishFlow($imageGenarationFromScript=false)
	{
		$thissCustArray = array();
		$productStyleId = $this->productStyleId;

		// load all areas for this style
		$areaQuery = db_query("select id,name,image_l,image_t,isprimary,image_z from mk_style_customization_area where style_id = $productStyleId");
		$allAreas = array();
		while($area = db_fetch_array($areaQuery) )
		{
			$areaname = $area["name"];
			$areaid  = $area["id"];
			$thisArea = array(
								"areaid"=>$area["id"],
								"price"=> "notused",
								"bgimage"=>empty($area["image_z"])?$area["image_l"]:$area["image_z"],
								"area_icon"=>$area["image_t"],
								"is_primary"=>$area["isprimary"],
								"start_x"=>0,
								"start_y"=>0,
								"size_x"=>0,
								"size_y"=>0
							);
			// load all orientations for this style & area
		if($imageGenarationFromScript){
			 $z_image_details=getimagesize("./".$area["image_z"]);
		         $l_image_details=getimagesize("./".$area["image_l"]);
		}
		else{
		            $z_image_details=getimagesize("../".$area["image_z"]);
		            $l_image_details=getimagesize("../".$area["image_l"]);
		}
            if(!empty($z_image_details))
            {
                    $z_width=$z_image_details[0];
                    $z_height=$z_image_details[1];
                    $l_width=$l_image_details[0];
                    $l_height=$l_image_details[1];
                    $z_w_scale_factor=$z_width/$l_width;
                    $z_h_scale_factor=$z_height/$l_height;


            }

			$orientationQuery = db_query("select id,location_name,start_x,start_y,size_x,size_y,width_in,height_in,orientation_order,ort_default from  mk_customization_orientation where area_id = $areaid");
			$allOrientations = array();
			while($orientation = db_fetch_array($orientationQuery))
			{
				$orientationname = $orientation["location_name"];
				$thisOrientation = array(
											"orientationid" => $orientation["id"],
											"is_primary" => $orientation["ort_default"],
											"start_x" => empty($z_w_scale_factor)?$orientation["start_x"]:($orientation["start_x"]*$z_w_scale_factor),
											"start_y" =>  empty($z_h_scale_factor)?$orientation["start_y"]:($orientation["start_y"]*$z_h_scale_factor),
											"size_x" => empty($z_w_scale_factor)?$orientation["size_x"]:($orientation["size_x"]*$z_w_scale_factor),
											"size_y" => empty($z_h_scale_factor)?$orientation["size_y"]:($orientation["size_y"]*$z_h_scale_factor),
											"width_in" => $orientation["width_in"],
											"height_in" => $orientation["height_in"],
											"orientation_order" => $orientation["orientation_order"]
										);
				$allOrientations[$orientationname] = $thisOrientation;
			}
			// A tweak to define customization co-ordinates at the area level
			// Remove this logic when we have customization co-ordinates defined for areas also
			// From all orientations in this area , calculate area co-ordinates
			// area["start_x"] = min(start_x) of all orientations
			// area["start_y"] = min(start_y) of all orientations
			// area["end_x"] =  max(end_x) of all orientations - end_x of orientation is it's (start_x + size_x)
			// area["end_y"] = max(end_y) of all orientations - end_y is orientation is it's (start_y + size_y)
			$start_x = 1000000; // some large number
			$start_y = 1000000; // some large number
			$end_x = -100000; // some small number
			$end_y = -100000; // some small number
			foreach($allOrientations as $orientation)
			{
				if( $start_x > $orientation["start_x"] )
					$start_x = $orientation["start_x"];
				if( $start_y > $orientation["start_y"] )
					$start_y = $orientation["start_y"];
				if( $end_x < $orientation["start_x"]+$orientation["size_x"])
					$end_x = $orientation["start_x"]+$orientation["size_x"];
				if( $end_y < $orientation["start_y"]+$orientation["size_y"])
					$end_y = $orientation["start_y"]+$orientation["size_y"];
			}
			$thisArea["start_x"] = $start_x;
			$thisArea["start_y"] = $start_y;
			$thisArea["size_x"] = $end_x-$start_x;
			$thisArea["size_y"] = $end_y-$start_y;
			// Now move all orientations start_x start_y wrt to the area's start_x,start_y ( not the bgimage's start_x,start_y )
			foreach($allOrientations as $key=>$orientation)
			{
				//adjust start_x,start_y wrt to area start_x,start_y
				$orientation["start_x"] = $orientation["start_x"] - $thisArea["start_x"];
				$orientation["start_y"] = $orientation["start_y"] - $thisArea["start_y"];
				//write back
				$allOrientations[$key] = $orientation;
			}
			$thisArea["orientations"] = $allOrientations;
			$allAreas[$areaname] = $thisArea;

		}
		$thisCustArray["areas"] = $allAreas;

		return $thisCustArray;

	}
    //this function can be removed later after running the imageGeneration script
    private function loadMasterCustomizationDataForImageGenerationScriptOldDesigns()
	{
		$thissCustArray = array();
		$productStyleId = $this->productStyleId;

		// load all areas for this style
		$areaQuery = db_query("select id,name,image_l,image_t,isprimary,image_z from mk_style_customization_area where style_id = $productStyleId");
		$allAreas = array();
		while($area = db_fetch_array($areaQuery) )
		{
			$areaname = $area["name"];
			$areaid  = $area["id"];
			$thisArea = array(
								"areaid"=>$area["id"],
								"price"=> "notused",
								"bgimage"=>empty($area["image_z"])?$area["image_l"]:$area["image_z"],
								"area_icon"=>$area["image_t"],
								"is_primary"=>$area["isprimary"],
								"start_x"=>0,
								"start_y"=>0,
								"size_x"=>0,
								"size_y"=>0
							);
			// load all orientations for this style & area
            $z_image_details=getimagesize("./".$area["image_z"]);
            $l_image_details=getimagesize("./".$area["image_l"]);
            if(!empty($z_image_details))
            {
                    $z_width=$z_image_details[0];
                    $z_height=$z_image_details[1];
                    $l_width=$l_image_details[0];
                    $l_height=$l_image_details[1];
                    $z_w_scale_factor=$z_width/$l_width;
                    $z_h_scale_factor=$z_height/$l_height;


            }

			$orientationQuery = db_query("select id,location_name,start_x,start_y,size_x,size_y,width_in,height_in,orientation_order,ort_default from  mk_customization_orientation where area_id = $areaid");
			$allOrientations = array();
			while($orientation = db_fetch_array($orientationQuery))
			{
				$orientationname = $orientation["location_name"];
				$thisOrientation = array(
											"orientationid" => $orientation["id"],
											"is_primary" => $orientation["ort_default"],
											"start_x" => empty($z_w_scale_factor)?$orientation["start_x"]:($orientation["start_x"]*$z_w_scale_factor),
											"start_y" =>  empty($z_h_scale_factor)? $orientation["start_y"] :($orientation["start_y"] * $z_h_scale_factor),
											"size_x" => empty($z_w_scale_factor)?$orientation["size_x"]:($orientation["size_x"]*$z_w_scale_factor),
											"size_y" => empty($z_h_scale_factor)?156:(156 * $z_h_scale_factor),
											"width_in" => 8,
											"height_in" => 8,
											"orientation_order" => $orientation["orientation_order"]

                    					);
				$allOrientations[$orientationname] = $thisOrientation;
			}
			// A tweak to define customization co-ordinates at the area level
			// Remove this logic when we have customization co-ordinates defined for areas also
			// From all orientations in this area , calculate area co-ordinates
			// area["start_x"] = min(start_x) of all orientations
			// area["start_y"] = min(start_y) of all orientations
			// area["end_x"] =  max(end_x) of all orientations - end_x of orientation is it's (start_x + size_x)
			// area["end_y"] = max(end_y) of all orientations - end_y is orientation is it's (start_y + size_y)
			$start_x = 1000000; // some large number
			$start_y = 1000000; // some large number
			$end_x = -100000; // some small number
			$end_y = -100000; // some small number
			foreach($allOrientations as $orientation)
			{
				if( $start_x > $orientation["start_x"] )
					$start_x = $orientation["start_x"];
				if( $start_y > $orientation["start_y"] )
					$start_y = $orientation["start_y"];
				if( $end_x < $orientation["start_x"]+$orientation["size_x"])
					$end_x = $orientation["start_x"]+$orientation["size_x"];
				if( $end_y < $orientation["start_y"]+$orientation["size_y"])
					$end_y = $orientation["start_y"]+$orientation["size_y"];
			}
			$thisArea["start_x"] = $start_x;
			$thisArea["start_y"] = $start_y;
			$thisArea["size_x"] = $end_x-$start_x;
			$thisArea["size_y"] = $end_y-$start_y;
			// Now move all orientations start_x start_y wrt to the area's start_x,start_y ( not the bgimage's start_x,start_y )
			foreach($allOrientations as $key=>$orientation)
			{
				//adjust start_x,start_y wrt to area start_x,start_y
				$orientation["start_x"] = $orientation["start_x"] - $thisArea["start_x"];
				$orientation["start_y"] = $orientation["start_y"] - $thisArea["start_y"];
				//write back
				$allOrientations[$key] = $orientation;
			}
			$thisArea["orientations"] = $allOrientations;
			$allAreas[$areaname] = $thisArea;

		}
		$thisCustArray["areas"] = $allAreas;

		return $thisCustArray;

	}

	private function loadCustomizationData()
	{
		$thisCustArray = array();
		$productStyleId = $this->productStyleId;
		$areaQuery = db_query("select id,name,isprimary,customizationcharge from mk_style_customization_area where style_id = $productStyleId");
		$allAreas = array();
		$curr_selected_area = "";
		while( $area = db_fetch_array($areaQuery) )
		{
			$areaname = $area["name"];
			$areaid = $area["id"];
			$is_default = $area["isprimary"];
			$customizationcharge = $area["customizationcharge"];
			if( $is_default == 1)
				$curr_selected_area = $areaname;

			$thisArea = array(
								"areaid" => $areaid,
								"is_customized" =>0,
								"curr_selected_orientation" => '',
								"customizationcharge" => $customizationcharge
							);

			$allOrientations = array();
			$curr_selected_orientation = "";
			$orientationQuery = db_query("select id,location_name,orientation_order,ort_default from mk_customization_orientation where area_id = $areaid order by orientation_order desc");
			while( $orientation = db_fetch_array($orientationQuery) )
			{
				$orientationId = $orientation["id"];
				$orientationName = $orientation["location_name"];
				$is_default = $orientation["ort_default"];
				if( $is_default == 'Y')
					$curr_selected_orientation = $orientationName;
				$orientation_order = $orientation["orientation_order"];
				
				$orientationConfigurationQuery = db_query("select imageAllowed, imageCustAllowed, textAllowed, textCustAllowed, textLimitation, textLimitationTpl, textFontColors, textColorDefault, pathForImagesToReplaceText from mk_customization_orientation where id =$orientationId");
				$orientationConfiguration = db_fetch_array($orientationConfigurationQuery);
				$thisOrientation = array(
										"orientationid" => $orientationId,
										"is_customized" => 0,
										"zindex" => $orientation_order,
										"imageobjectid" => "",
										"textobjectid" => "",
										"imageAllowed" => $orientationConfiguration["imageAllowed"],
										"imageCustAllowed" => $orientationConfiguration["imageCustAllowed"],
										"textAllowed" => $orientationConfiguration["textAllowed"],
										"textCustAllowed" => $orientationConfiguration["textCustAllowed"],
										"textLimitation" => $orientationConfiguration["textLimitation"],										
										"pathForImagesToReplaceText" => $orientationConfiguration["pathForImagesToReplaceText"],
										"actionlog" => array( "image"=>array() , "text"=>array() )
									);
									
					
				/* add text limitation info in orientation array such as font color, type and size.
				 * Added by arun  */
				if($thisOrientation['textLimitation']) {
					
					$thisOrientation += array(
											"textLimitationTpl" => $orientationConfiguration["textLimitationTpl"],
											"textColorDefault" => $orientationConfiguration["textColorDefault"],//color id(from DB)
											"textFontColors" => trim($orientationConfiguration["textFontColors"],','),//comma separated											
										);
					//get font colors code from table
					if(!empty($thisOrientation['textFontColors'])) {
						$colorArray = $thisOrientation['textFontColors'];
					}
					
					if(!empty($thisOrientation['textColorDefault'])) {
						if(!empty($colorArray)) {
							$colorArray.=",";
						}
						$colorArray.= $thisOrientation['textColorDefault'];
					}
					
					$textFontColorsQuery = db_query("select id,color from mk_text_font_color where id in ($colorArray)");
					$textFontColors = array();
					while($row = db_fetch_array($textFontColorsQuery)){
						if($thisOrientation['textColorDefault'] == $row['id'])
							$thisOrientation['textColorDefault'] = $row['color'];//color code for eg:#000000
						$textFontColors[] = $row['color'];					
					}
					$thisOrientation['textFontColors'] = $textFontColors;//assigning colors array instead of csv.

					
					//get font type info		
					/*$defaultFontTypeQuery = db_query("select d.font_id,f.name as font_name,f.image_name as image,f.ttf_name as ttf from mk_text_font_detail d,mk_fonts f where f.id=d.font_id and d.orientation_id='{$orientationId}' and d.font_default='Y'");
					$defaultFontTypeInfo = db_fetch_array($defaultFontTypeQuery);
					if(!empty($defaultFontTypeInfo)){
						$defaultFontTypeInfo = array(
													"textFontId" => $defaultFontTypeInfo["font_id"],
													"textFontName" => $defaultFontTypeInfo["font_name"],
													"textFontImage" => $defaultFontTypeInfo["image"],
													"textFontTTF" => $defaultFontTypeInfo["ttf"],
													"textFontSizes" => array()
												);
						//get font type's size info											
						$allSizesOfFontTypeQuery = db_query("select orientation_id,font_id,font_size,maxchars,font_default,size_default from mk_text_font_detail where orientation_id='{$orientationId}' and font_id='{$defaultFontTypeInfo['textFontId']}'");
						while($row = db_fetch_array($allSizesOfFontTypeQuery)){
							$allSizesOfFontType['orientation_id'] = $row['orientation_id'];
							$allSizesOfFontType['font_id'] = $row['font_id'];
							$allSizesOfFontType['font_size'] = $row['font_size'];
							$allSizesOfFontType['maxchars'] = $row['maxchars'];
							$allSizesOfFontType['font_default'] = $row['font_default'];					
							$allSizesOfFontType['size_default'] = $row['size_default'];
							$allSizesOfFontTypeIndexed[] = $allSizesOfFontType; 
						}
						$defaultFontTypeInfo['textFontSizes'] = $allSizesOfFontTypeIndexed;//assign all font sizes of particular font type
						$thisOrientation[$defaultFontTypeInfo['textFontName']] = $defaultFontTypeInfo;//assign font type info into orientation
					}*/
					

					$thisOrientation['textFontLimitations'] = array();
					$limitedFontDetailsQuery = db_query("select fd.orientation_id, fd.id, fd.font_id, fon.name, " .
								 	"fon.image_name, fon.ttf_name, fd.font_size, fd.maxchars, fd.font_default, fd.size_default, fd.chartype, fd.charcase " .
									"from mk_text_font_detail fd, mk_fonts fon where orientation_id='{$orientationId}' " .
									"and fon.id=fd.font_id order by fd.font_id");
					
					while ($row = db_fetch_array($limitedFontDetailsQuery)) {
						$mkfontdetailid = $row['id'];
						$singleSizeOfFontType = array();
						$singleSizeOfFontType['orientation_id']	= $row['orientation_id'];
						$singleSizeOfFontType['font_detail_id']	= $row['id'];
						$singleSizeOfFontType['font_id'] 		= $row['font_id'];
						$singleSizeOfFontType['font_name'] 		= $row['name'];
						$singleSizeOfFontType['font_image_name']= $row['image_name'];
						$singleSizeOfFontType['font_ttf_name'] 	= $row['ttf_name'];
						$singleSizeOfFontType['font_size'] 		= $row['font_size'];
						$singleSizeOfFontType['maxchars'] 		= $row['maxchars'];
						$singleSizeOfFontType['font_default'] 	= $row['font_default'];					
						$singleSizeOfFontType['size_default'] 	= $row['size_default'];
						$singleSizeOfFontType['chartype'] 		= $row['chartype'];
						$singleSizeOfFontType['charcase'] 		= $row['charcase'];
						
						if($row['font_default']=="Y") {
							$thisOrientation['textFontLimitations']['default'] = $singleSizeOfFontType;
						} else {						
							$thisOrientation['textFontLimitations'][] = $singleSizeOfFontType;
						}
					}
					
				}
				
				/*## till here ##*/
				$allOrientations[$orientationName] = $thisOrientation;
			}
			$thisArea["orientations"] = $allOrientations;
			$thisArea["curr_selected_orientation"] = $curr_selected_orientation;
			$allAreas[$areaname] = $thisArea;
		}
		$thisCustArray["areas"] = $allAreas;
		$thisCustArray["curr_selected_area"] = $curr_selected_area;
		return $thisCustArray;
	}
	
	public function compareCustArraysForSimilarity($oldCustArray,$newCustArray)
	{
		$oldArray = array();
		foreach($oldCustArray["areas"] as $akey=>$area)
		{
			foreach($area["orientations"] as $okey=>$orientation)
			{
				$oldArray["areas"][$akey]["orientations"][$okey] = $orientation;
			}
		}
		$newArray = array();
		foreach($newCustArray["areas"] as $akey=>$area)
		{
			foreach($area["orientations"] as $okey=>$orientation)
			{
				$newArray["areas"][$akey]["orientations"][$okey] = $orientation;
			}
		}
		foreach($oldArray["areas"] as $oakey=>$oarea)
		{
			$narea = $newArray["areas"][$oakey];
			if( empty($narea) ) // no such area in newArray with $oakey
				return 0;
			foreach($oarea["orientations"] as $ookey=>$oort)
			{
				$nort = $narea["orientations"][$ookey];
				if( empty($nort) ) // no such orientation in newArray with $ookey
					return 0;
			}
		}
		return 1;
	}
	public function smartMergeOldCustWithNewCust($oldCustArray,$newCustArray,$masterCustomizationArray,$globalDataArray)
	{
		$params = 	array(	"custArray"=>$newCustArray,
							"globalDataArray"=>$globalDataArray,
							"chosenProductConfiguration"=>null,
							"masterCustomizationArray"=>$masterCustomizationArray
					);
		
		$customizationHelper = new CustomizationHelper($params);
		
		/* on text limitation do not merge orientation detail directly
		 * instead build default text limitation detail according to new orientation of the style() 
		 */		
		//$isTextLimitation = $customizationHelper->isTextLimitationForOrientation();
		
		foreach($oldCustArray["areas"] as $akey=>$area)
		{
			if( $area["is_customized"] == 1)
			{
				$customizationHelper->setSelectedAreaName($akey);
				foreach($area["orientations"] as $okey=>$orientation)
				{
					if( $orientation["is_customized"] == 1)
					{
						$customizationHelper->setSelectedOrientationName($okey);
						if( !empty($newCustArray["areas"][$akey]) && !empty($newCustArray["areas"][$akey]["orientations"][$okey]) )
						{						
							$textobjectid = $orientation["textobjectid"];
							if( !empty($textobjectid) )
							{
								$text = $orientation[$textobjectid]["text"];
								$zIndex = $orientation[$textobjectid]["customizationinfo"]["zindex"];
								$customizationHelper->updateTextInfoInCustArea($text,$zIndex);
							}

							$imageobjectid = $orientation["imageobjectid"];
							if( !empty($imageobjectid) )
							{
								$zIndex = $orientation[$imageobjectid]["customizationinfo"]["zindex"];
								$customizationHelper->updateImageInfoInCustArea($imageobjectid,$zIndex);
							}
							//do not merge orientation directly from old to new in case of limited text customization
							//if(!$isTextLimitation){
								$newCustOrtArray = $newCustArray["areas"][$akey]["orientations"][$okey];
								$new_ort_id = $newCustOrtArray["orientationid"];
								$oldCustOrtArray = $orientation;
								$oldCustOrtArray["orientationid"] = $new_ort_id;
								$customizationHelper->setCustomizationArrayForCurrentSelectedOrientation($oldCustOrtArray);
							//}
						}
					}
				}
				$newCustAreaArray = $customizationHelper->getCustomizationArrayForCurrentSelectedArea();
				$newCustAreaArray["is_customized"] = $area["is_customized"];
				$customizationHelper->setCustomizationArrayForCurrentSelectedArea($newCustAreaArray);
			}
		}
		$curr_selected_area = $oldCustArray["curr_selected_area"];
		$curr_selected_orientation = $oldCustArray["areas"][$curr_selected_area]["curr_selected_orientation"];
		$customizationHelper->setSelectedAreaName($curr_selected_area);
		$customizationHelper->setSelectedOrientationName($curr_selected_orientation);
		$newCustArray = $customizationHelper->getCustomizationArray();
		return $newCustArray;
	}

	public function initChosenProductConfigurationData($thisChosenProductConfiguration)
	{
		$productStyleId = $this->productStyleId;
	        //for start shopping flow get product styledetails with markup price
	        if(!empty($thisChosenProductConfiguration['productId'])){
		      	$productId = $thisChosenProductConfiguration['productId'];
		        $productStyleDetails = func_load_product_style_details_markup($productStyleId,$productId,$priceForPOS,$shopMasterId);
			}
	        else {
				$productStyleDetails = func_load_product_style_details($productStyleId,$priceForPOS,$shopMasterId);
				$thisChosenProductConfiguration["is_customizable"]=$productStyleDetails[0][12];
	        }

		$productStyleLabel = $productStyleDetails[0][1];
		//$unitPrice = $productStyleDetails[0][5];
		$typedata = func_style_id_detail($productStyleId);
		$productTypeId=strip_tags($typedata["product_type"]);
		$productTypeLabel=strip_tags($typedata["name"]);
		$productTypeGroupId=strip_tags($typedata["product_type_groupid"]);

		$thisChosenProductConfiguration["productStyleId"] = $productStyleId;
		$thisChosenProductConfiguration["productTypeGroupId"] = $productTypeGroupId;
		$thisChosenProductConfiguration["productTypeId"] = $productTypeId;
		$thisChosenProductConfiguration["productTypeLabel"] = $productTypeLabel;
		$thisChosenProductConfiguration["productStyleLabel"] = $productStyleDetails[0][1];
		//$thisChosenProductConfiguration["unitPrice"] = $productStyleDetails[0][5];
		
		//added by vaibhav to support different options for a any product style 
                $styleProductOptions = func_load_product_options($productStyleId);
                $thisChosenProductConfiguration["style_product_options"] = $styleProductOptions;
                $optionCnt = count($styleProductOptions);
                if(!empty($styleProductOptions)) {
                        for ( $i=0; $i < $optionCnt; $i++ ) {
                                if( ($styleProductOptions[$i][3] > 0) && ($styleProductOptions[$i][4] == 'Y') ) {
                                        $thisChosenProductConfiguration["unitPrice"]= $styleProductOptions[$i][3];
                                        break;
                                } else {
                                        $thisChosenProductConfiguration["unitPrice"]= $productStyleDetails[0][5];
                                }
                        }
                } else {
                        $thisChosenProductConfiguration["unitPrice"]= $productStyleDetails[0][5];
                }

                $productOptionDetails=func_load_product_options_new_cust($productStyleId);
                $thisChosenProductConfiguration["sizeChartInfo"]=$productOptionDetails;

		return $thisChosenProductConfiguration;
	}
	public function initCustomizationArrayData($thisCustomizationArray)
	{
		//$productStyleId = $this->productStyleId;
	    //$smartMerge = false;

		// try merging old style's customization info with new style's customization ONLY IF PRODUCT TYPES OF OLD AND NEW STYLES ARE SAME.
		//if(!empty($thisCustomizationArray) )
		//{
		//	$oldCustomizationArray = $thisCustomizationArray;
		//	$smartMerge = true;
		//}
		$thisCustomizationArray = $this->loadCustomizationData_preLoaded();
		// smart merge old style's customization array and new style customization array ( this is to retain the imageobject and textobjects of old style)
		//if($smartMerge)
		//	$thisCustomizationArray = $this->smartMergeOldCustWithNewCust($oldCustomizationArray,$thisCustomizationArray);

		return $thisCustomizationArray;
	}

	public function initMasterCustomizationArrayData($thisMasterCustomizationArrayEx)
	{
		$thisMasterCustomizationArrayEx = $this->loadMasterCustomizationData_preLoaded();
		return $thisMasterCustomizationArrayEx;
	}
    public function loadMasterCustomizationDataForOldSmallDesign($thisMasterCustomizationArrayEx)
	{
		$thisMasterCustomizationArrayEx = $this->loadMasterCustomizationDataForOldSmallDesign_preLoaded();
		return $thisMasterCustomizationArrayEx;
	}
    public function loadMasterCustomizationDataForPublishFlow($thisMasterCustomizationArrayEx,$imageGenarationFromScript=false)
	{
		$thisMasterCustomizationArrayEx = $this->loadMasterCustomizationDataPublishFlow($imageGenarationFromScript);
		return $thisMasterCustomizationArrayEx;
	}
    public function loadMasterCustomizationDataForImageGenerationScript($thisMasterCustomizationArrayEx)
	{
		$thisMasterCustomizationArrayEx = $this->loadMasterCustomizationDataForImageGenerationScriptOldDesigns();
		return $thisMasterCustomizationArrayEx;
	}


	public function deleteOldImages($oldCustArray)
	{
		foreach($oldCustArray["areas"] as $area)
		{
			$imageFile = $area["final_image_name"];
			if( file_exists($imageFile) )
				@unlink($imageFile);
		}
	}
}

?>
