<?php
namespace order\manager;

class OrderLogger {
	
	private $ORDERLOG;
	private $login;
	public function __construct($logFile, $login) {
		global $var_dirs;
		$this->login = $login;
		$this->ORDERLOG = fopen($logFile,"a+");
		if(empty($this->ORDERLOG)) {
			echo "failed to open log file: $logFile for writing: ";
		}
	}
	
	public function getLogin() {
		return $this->login;
	}
	
	public function logSession($sessionId, $sessionVars) {
		$this->log("++++++++++++++++++++++++STARTING ORDER PROCESSING FOR".$this->login." at ".date("F j, Y, g:i a")."++++++++++++++++\n");
		$sessionDumpStr = "";
		ob_start();
		var_dump($sessionVars);
		$sessionDumpStr = ob_get_contents();
		ob_end_clean();
		$this->log("\tSession ID - ".$sessionId."\n");
		$this->log("\tSession Dump - ".$sessionDumpStr."\n");
	}
	
	public function log($str) {
		if($this->ORDERLOG)
			fputs($this->ORDERLOG, $str, strlen($str));
	}
	
	public function close() {
		if($this->ORDERLOG)
			fclose($this->ORDERLOG);
	}
	
	public function __destruct() {
		$this->close();
	}
}
?>