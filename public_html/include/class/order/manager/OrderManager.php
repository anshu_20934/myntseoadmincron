<?php
namespace order\manager;

use base\manager\BaseManager;

use order\dataobject\builder\AmountDOBuilder;
use order\dataobject\builder\OrderItemsDOBuilder;
use order\dataobject\builder\PaymentGatewayFormBuilder;
use order\dataobject\builder\TempOrderDOBuilder;

use order\dataobject\AmountDO;
use order\dataobject\DiscountDO;
use order\dataobject\GiftDO;
use order\dataobject\OrderDO;
use order\dataobject\PaymentDO;
use order\dataobject\ShippingDO;
use order\dataobject\TempOrderDO;

use order\dataobject\validator\CashOnDeliveryValidator;
use order\dataobject\validator\DiscountDOValidator;
use order\dataobject\validator\ShippingDOValidator;

use order\dao\OrderDAO;
use order\dao\mapper\OrderDOMapper;

use order\exception\CartTamperedException;
use order\exception\EmptyCartException;
use order\exception\NoSuitableGatewayException;
use order\exception\UserNotLoggedInException;
use order\exception\CartOOSException;

use enums\revenue\payments\PaymentType;
use revenue\payments\PaymentGatewayFactory;
use style\dao\ProductOptionsDAO;
include_once "$xcart_dir/include/func/func.loginhelper.php";
require_once \HostConfig::$documentRoot."/include/class/mcart/class.MCartUtils.php";
require_once \HostConfig::$documentRoot."/modules/apiclient/AtpApiClient.php";
require_once \HostConfig::$documentRoot."/include/class/oms/EventCreationManager.php";

class OrderManager extends BaseManager {

	public $orderDO;
	private $orderDAO, $productOptionsDAO;
	
	public function __construct(OrderDO $orderDO) {
		$this->orderDO = $orderDO;
		$this->orderDAO = new OrderDAO();
		$this->productOptionsDAO = new ProductOptionsDAO();
	}
	
	/**
	 * This will work on OrderDO
	 */
	public function checkout() {
		$this->checkPrerequisites();
		
		$this->validateDOs();
		$this->initDerivedParams();
		$this->createOrder();
		$this->lockCouponsAfterUsage();
		//fraud logging
		$this->checkAndLogForFraud();
	}
	
	public function checkoutOrderActions($pgDiscount, $emiCharge, $lastUpdatedTime) {
		$this->checkEmptyLogin();
		$this->checkForEmptyCart();
        // TODO -- TS need to look at
		$this->checkForCartTamperingOrderActions($lastUpdatedTime);
		$this->checkForItemOOS();
		$this->validateDOs();
		$paymentDO = $this->orderDO->getPaymentDO();
		$cardDO = $paymentDO->getCardDO();
		$amountDO = $this->orderDO->getAmountDO();
		$paymentDO->setPaymentGatewayDiscount($pgDiscount);
		$amountDO->setPaymentGatewayDiscount($pgDiscount);
		$paymentDO->setEMICharge($emiCharge);
		$amountDO->setEMICharge($emiCharge);
		$this->initAmountDOFields();
		$this->createOrder();
		$this->lockCouponsAfterUsage();
		//fraud logging
		$this->checkAndLogForFraud();
	}

	public function lockCouponsAfterUsage() {
		$couponCode = $this->orderDO->getDiscountDO()->getCouponCode();
		$cashCouponCode = $this->orderDO->getDiscountDO()->getCashCouponCode();
		
		if(!empty($couponCode) || !empty($cashCouponCode)) {
			$adapter = \CouponAdapter::getInstance();
			$validator = \CouponValidator::getInstance();

			if(!empty($couponCode)) {
				# Update the number of times the coupon has been used once the order is placed
				$adapter->lockCouponForUser($couponCode, $this->orderDO->getLogin());
			}	
			if(!empty($cashCouponCode)) {
	            # Update the number of times the coupon has been used once the order is placed
	            $adapter->lockCouponForUser($cashCouponCode, $this->orderDO->getLogin());
	        }	
		}
	}
	/**
	 * 
	 * Enter description here ...
	 */
	
	public function getPaymentGatewayForm() {
		$pgFormBuilder = new PaymentGatewayFormBuilder($this->orderDO);
		$form = $pgFormBuilder->build();
		return $form;
	}
	
	protected function createOrder() {
		//OrderDAO methods to be called on OrderDO:
		//(1) createAndSaveTempOrder (2) create order (3) create order items (4) update order item quantities	
		
		$this->createAndSaveTempOrder();
		$this->initMoreDerivedParams();
		$result = $this->orderDAO->saveOrder($this->orderDO);
		
		$this->createOrderItems();
		
		$this->createProducts();
		
		$this->updateOrderItemQuantities();
		
		if(x_session_is_registered("orderID")) {
			x_session_unregister("orderID");
		}
		x_session_register("orderID", $this->orderDO->getOrderId());
	}

	protected function createAndSaveTempOrder() {
		$tempOrderDOBuilder = new TempOrderDOBuilder($this->orderDO);
		$tempOrderDO = $tempOrderDOBuilder->build();
		$tempOrderDO = $this->orderDAO->saveTempOrder($tempOrderDO);
		$this->orderDO->setOrderId($tempOrderDO->getOrderId());
		
		//Order-name is a placeholder for future: now it is set to order-id only
		$this->orderDO->setOrderName($tempOrderDO->getOrderId());//$user_source_code.'-'.$orderid.
		$this->orderDO->setGroupId($tempOrderDO->getOrderId());
		$this->orderDO->setTempOrderDO($tempOrderDO);
	}
	
	/**
	 * Persists OrderItems
	 * @param array<OrderItemDO> $orderItems
	 */
	public function createOrderItems() {
		$orderItemsDOBuilder = new OrderItemsDOBuilder($this->orderDO);
		$orderItems = $orderItemsDOBuilder->build();
		
		foreach ($orderItems as $eachOrderItem) {
			$itemId = $this->orderDAO->saveOrderItem($eachOrderItem);
			//set this item id on the item
			$eachOrderItem->setItemId($itemId);
		}
		$this->orderDO->setOrderItems($orderItems);
	}
	
	public function createProducts() {
		$orderItems = $this->orderDO->getOrderItems();
		
		foreach ($orderItems as $eachOrderItem) {
			$this->orderDAO->saveProduct($eachOrderItem);
		}		
	}
	
	private function updateOrderItemQuantities() {
		
		$this->orderDAO->updateOrderQuantity($this->orderDO->getOrderId(), $this->orderDO->getTotalQuantity());
		
		$orderItems = $this->orderDO->getOrderItems();
		foreach ($orderItems as $eachOrderItem) {
			
			$skuId = $eachOrderItem->get("skuid");
			//for kundan for this query to a better place.
			$itemOptionId = $this->productOptionsDAO->getOptionIdForSKUId($skuId);
			$itemId = $eachOrderItem->getItemId(); 
			$itemSizeQuanity = $eachOrderItem->get("quantity");			
			
			$this->orderDAO->saveOptionOrderQuantity($itemId, $itemOptionId, $itemSizeQuanity);
		}
	}
	
	/**
	 * Log if the order is detected as fraud. This uses a global $fraudOrderLog to log
	 */
	
	protected function checkAndLogForFraud() {
		$orderId = $this->orderDO->getOrderId();
		
		$fraudMatch = $this->orderDAO->checkForFraud($orderId);
		if(!empty($fraudMatch)) {
			
			global $fraudOrderLog, $XCART_SESSION_VARS;
			$fraudOrderLog->debug("MKORDERINSERT : ORDER:$orderId Detected as fraud ");

			$myCart = $this->orderDO->getCartDO()->getCart();
			if($myCart!==null){
				$fraudOrderLog->debug("MKORDERINSERT : ORDER:$orderId CARTDUMP{".serialize($myCart)."}");
			}
			else{
				$fraudOrderLog->debug("MKORDERINSERT : ORDER:$orderid CARTDUMP{EMPTY}");
			}
			$fraudOrderLog->debug("MKORDERINSERT : ORDER:$orderId SESSIONDUMP{".serialize($XCART_SESSION_VARS)."} ");
		}
	}
	
	protected function initDerivedParams() {
		$this->initPaymentGatewayDiscount();
		$this->initEMICharges();
		$this->initAmountDOFields();
	}
	
	protected function initMoreDerivedParams() {
		global $order_source_code, $order_source_id;//defined in config.ini
		$this->orderDO->setOrderName($order_source_code ."-". $this->orderDO->getOrderId());
		// har coding for myntra.com to 1
		$order_source_id = 1;
		$this->orderDO->setSourceId($order_source_id);
	}
	
	//TODO: populate PG discount from OrderDO
	private function initPaymentGatewayDiscount() {
		global $https_location;
		
		$paymentDO = $this->orderDO->getPaymentDO();
		$cardDO = $paymentDO->getCardDO();
		$amountDO = $this->orderDO->getAmountDO();
		
		$gateway = PaymentGatewayFactory::getInstance()->getGateway($paymentDO);
		if($gateway == null) {
			throw new NoSuitableGatewayException("Could not fetch gateway to calculate Payment Gateway Discount", $paymentDO);
		}
		$gateway->setHttpsLocation($https_location);

		//Set the gateway on PaymentDO
		$paymentDO->setGateway($gateway);
		
		$pgDiscount = 0;
		if($gateway->getPaymentType() == PaymentType::Credit || $gateway->getPaymentType() == PaymentType::Debit || $gateway->getPaymentType() == PaymentType::CreditDebit) {
			$pgDiscount = $gateway->getDiscountByCardNumber($amountDO->getTotalBeforePGDiscount(), $amountDO->getAmountWithProductDiscount());
		} elseif($gateway->getPaymentType() == PaymentType::NetBanking) {
			$pgDiscount = $gateway->getDiscountOnBank("icici", $amountDO->getTotalBeforePGDiscount());
		}
		$paymentDO->setPaymentGatewayDiscount($pgDiscount);
		$amountDO->setPaymentGatewayDiscount($pgDiscount);
	} 
	
	//init EMI Charges to be called after initPaymentGatewayDiscount
	private function initEMICharges() {
		
		$paymentDO = $this->orderDO->getPaymentDO();
		$cardDO = $paymentDO->getCardDO();
		$amountDO = $this->orderDO->getAmountDO();

		$gateway = $paymentDO->getGateway();
		if($gateway == null) {
			throw new NoSuitableGatewayException("Could not fetch gateway for EMI Option", $paymentDO);
		}
		
		$emiCharge = 0;
		$emiBank = $paymentDO->getEMIBank();
		
		$amountToApplyEMIChargeOn = $amountDO->getTotalBeforePGDiscount() - $amountDO->getPaymentGatewayDiscount();
		
		if(!empty($emiBank)) {
			$emiCharge = $gateway->getEMICharges($emiBank,$amountToApplyEMIChargeOn);
		}
		
		$paymentDO->setEMICharge($emiCharge);
		$amountDO->setEMICharge($emiCharge);
	}
	
	private function initAmountDOFields() {
		$amountDO = $this->orderDO->getAmountDO();
		AmountDOBuilder::buildTotal($amountDO);
		AmountDOBuilder::buildFinalAmountToCharge($amountDO);
	}
	
	protected function validateDOs() {
		$validator = new ShippingDOValidator($this->orderDO->getShippingDO());
		$validator->validate();
		unset($validator);
		
		$validator = new DiscountDOValidator($this->orderDO);
		$validator->validate();
		unset($validator);

		$validator = new CashOnDeliveryValidator($this->orderDO);
		$validator->validate();
		unset($validator);
		
	}
	
	protected function checkPrerequisites() {
		$this->checkEmptyLogin();
		$this->checkForEmptyCart();
        // TODO -- TS need to look at
		$this->checkForCartTampering();
	}
	
	private function checkEmptyLogin() {
		$login = $this->orderDO->getLogin();
		if(!isGuestCheckout() && empty($login)) {
			throw new UserNotLoggedInException("User not logged in or session timeout");
		}
	}
	
	private function checkForEmptyCart() {
		$cartDO = $this->orderDO->getCartDO();
		$productsInCart = $cartDO->getProductsInCart();
		if($cartDO == null || $productsInCart == null) {
			throw new EmptyCartException("Cart is Empty during checkout: nothing to buy");
		}
	}
	
	private function checkForItemOOS() {
		$cartDO = $this->orderDO->getCartDO();
		$productsInCart = $cartDO->getProductsInCart();
		$skuIds = array();
                $skuId2QtyMap = array();
		foreach($productsInCart as $product) {
			$skuIds[] = $product['skuid'];
                        $skuId2QtyMap[$product['skuid']] = $product['quantity'];
		}
		
		$skuId2InvCountMap = \AtpApiClient::getAvailableInventoryForSkus($skuIds);
		$oosSkuIds = array();
		foreach ($skuIds as $skuId) {
			if(isset($skuId2InvCountMap[$skuId]) && $skuId2InvCountMap[$skuId]['availableCount'] < $skuId2QtyMap[$skuId] ) {
				$oosSkuIds[] = $skuId;
			}
		}
		//If there are oos items, then throw exception and push an event to queue for refreshing caches
		if(count($oosSkuIds) > 0) {
			\EventCreationManager::pushStyleRefreshEvent($oosSkuIds);
			throw new CartOOSException("Some of the items in cart are out of stock");
		}
	}
	
	private function checkForCartTampering() {
		global $XCART_SESSION_VARS;
		//$myCart = $this->orderDO->getCartDO()->getCart();
		$cartDO = $this->orderDO->getCartDO();
		$productsInCart = $cartDO->getProductsInCart();
		foreach($productsInCart as $product) {
			if($product['quantity'] <= 0)
				throw new CartTamperedException("Cart tampered. Quantity for some of the items is negative.");
		}
		/*if($XCART_SESSION_VARS['LAST_CART_UPDATE_TIME'] != $myCart->getLastContentUpdatedTime()){
			$myCart->setLastContentUpdatedTime(time());
			\MCartUtils::persistCart($myCart);
			throw new CartTamperedException("Last modified time of cart: {$myCart->getLastContentUpdatedTime()} is not equal to last modified time of cart available in session: {$XCART_SESSION_VARS["LAST_CART_UPDATE_TIME"]}");
		}*/
	}
	
	private function checkForCartTamperingOrderActions($lastUpdatedTime) {
		//$myCart = $this->orderDO->getCartDO()->getCart();
		$cartDO = $this->orderDO->getCartDO();
		$productsInCart = $cartDO->getProductsInCart();
		foreach($productsInCart as $product) {
			if($product['quantity'] <= 0)
				throw new CartTamperedException("Cart tampered. Quantity for some of the items is negative.");
		}
		/*if($lastUpdatedTime != $myCart->getLastContentUpdatedTime()){
			$myCart->setLastContentUpdatedTime(time());
			\MCartUtils::persistCart($myCart);
			throw new CartTamperedException("Last modified time of cart: {$myCart->getLastContentUpdatedTime()} is not equal to last modified time of cart available in session: {$lastUpdatedTime}");
		}*/
	}
}
?>
