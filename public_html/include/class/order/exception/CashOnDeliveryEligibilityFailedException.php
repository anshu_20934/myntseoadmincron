<?php
namespace order\exception;
use base\exception\RedirectableValidationException;

class CashOnDeliveryEligibilityFailedException extends RedirectableValidationException {

	private $codEligibilityArr;
	
	public function __construct($codEligibilityArr, $message, $redirectUrl) {

		$this->codEligibilityArr = $codEligibilityArr;
		$message = "CashOnDeliveryInEligibility: ErrorCode: " .$codEligibilityArr['errorCode']." : ". $message;
		parent::__construct($message, "PaymentDO", "cashOnDeliveryCharge", $redirectUrl);
		
	}	
}