<?php
namespace order\exception;

use order\dataobject\OrderDO;
use base\dataobject\BaseDO;
use base\exception\RedirectableValidationException;

class OrderCreationFailedException extends RedirectableValidationException {
	
	private $baseDO;

	/**
	 * baseDO can be any child of BaseDO class: e.g. OrderDO, TempOrderDO 
	 */
	public function __construct($message, BaseDO $baseDO = null, $errFields) {
		if($orderDO !=null) {
			$message .= "\n".$baseDO->toString();
			$this->baseDO = $baseDO;
			$dataObjName = get_class($baseDO);
		} else {
			$dataObjName = "";
		}
		parent::__construct($message, $dataObjName, $errFields);
	}
	public function getDO() {
		return $this->baseDO;
	}
}