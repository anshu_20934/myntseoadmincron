<?php
namespace order\exception;

use base\exception\RedirectableValidationException;

class NoShippingAddressException extends RedirectableValidationException {
	
	public function __construct($message, $dataObjName="", $errFields="", $redirectUrl="") {
		$message = "NoShippingAddressException: ".$message;
		parent::__construct($message, $dataObjName, $errFields, $redirectUrl);
	}
}
?>