<?php
namespace order\exception;

use order\dataobject\PaymentDO;
use base\exception\RedirectableValidationException;

class NoSuitableGatewayException extends RedirectableValidationException {
	
	private $paymentDO;
	
	public function __construct($message, PaymentDO $paymentDO, $redirectUrl="") {
		$this->paymentDO = $paymentDO;
		$dataObjName="PaymentDO";
		$message = "NoSuitableGatewayException: ".$message;
		parent::__construct($message, $dataObjName, $errFields, $redirectUrl);
	}
	
	public function toString() {
		return $this->paymentDO->toString();
	}
}
?>