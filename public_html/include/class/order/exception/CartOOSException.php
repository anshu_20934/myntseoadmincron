<?php
namespace order\exception;

use base\exception\BaseRedirectableException;

class CartOOSException extends BaseRedirectableException {
	
	public function __construct($message, $redirectUrl="") {
		$message = "CartOOSException: ".$message;
		parent::__construct($message, $redirectUrl);
	}
}
?>