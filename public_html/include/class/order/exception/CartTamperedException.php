<?php
namespace order\exception;

use base\exception\BaseRedirectableException;
require_once \HostConfig::$documentRoot."/include/class/mcart/class.MCart.php";

class CartTamperedException extends BaseRedirectableException {
	
	private $mcart;
	
	public function __construct($message, \MCart $mcart = null, $redirectUrl="") {
		$this->mcart = $mcart;
		$message = "CartTamperedException: Cart was modified (possibly tampered) during checkout: ".$message;
		parent::__construct($message, $redirectUrl);
	}
}
?>