<?php
namespace order\exception;

use base\exception\BaseRedirectableException;

class EmptyCartException extends BaseRedirectableException {
	
	public function __construct($message, $redirectUrl="") {
		$message = "EmptyCartException: ".$message;
		parent::__construct($message, $redirectUrl);
	}
}
?>