<?php
namespace order\exception;

use base\exception\BaseRedirectableException;

class UserNotLoggedInException extends BaseRedirectableException {
	
	public function __construct($message, $redirectUrl) {
		$message = "UserNotLoggedInException: ".$message;
		parent::__construct($message, $redirectUrl);
	}
}
?>