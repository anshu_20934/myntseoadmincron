<?php
namespace order\dao;
use order\dataobject\AddressDO;

use base\dao\BaseDAO;

class AddressDAO extends BaseDAO {
	/**
	 * All these methods return AddressDO instances
	 * @param unknown_type $login
	 */
	public function getDefaultAddress($login) {
		$address = func_select_first('mk_customer_address', array('login' => $login,'default_address'=>1));		
		return (!empty($address)) ? new AddressDO($address) : null;
	}
	
	public function getLatestAddress($login) {
		$address = func_select_first('mk_customer_address', array('login' => $login),0,1,array('datecreated' => 'desc'));
		return (!empty($address)) ? new AddressDO($address) : null;
	}
	
	public function getAddress($addressId) {
		$address = func_select_first('mk_customer_address', array('id' => $addressId));
		return (!empty($address)) ? new AddressDO($address) : null;
	}
}