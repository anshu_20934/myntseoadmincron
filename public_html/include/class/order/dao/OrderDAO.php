<?php
namespace order\dao;

use order\dataobject\OrderItemDO;

use order\dao\mapper\OrderItemDOMapper;

use order\dao\mapper\XCartProductDOMapper;

use base\dao\BaseDAO;

use order\dataobject\OrderDO;
use order\dataobject\TempOrderDO;
use order\dao\mapper\OrderDOMapper;
use order\exception\OrderCreationFailedException;

class OrderDAO extends BaseDAO {
	
	/**
	 * Creates a Temp Order Entry in the table and returns the temp order just generated:
	 * This order id is used as Primary Key in xcart_orders for insertion
	 */
	public function saveTempOrder(TempOrderDO $tempOrderDO) {
        global $weblog;
		$orderCRC=$tempOrderDO->getOrderCRC();
                $sessionID=$tempOrderDO->getSessionId();
		$cols = array(
			"sessionid" => (empty($sessionID))?"":$sessionID,
			"checksumkey" => $tempOrderDO->getChecksumKey(),
			"order_crc" => (empty($orderCRC))?'0':"$orderCRC" 
		);

        $weblog->error("Test1: ".$cols);
		func_array2insertWithTypeCheck("mk_temp_order", $cols);
		//TODO: add logging support
		$orderIdCreated = db_insert_id();
		
		$cols["orderid"] = $orderIdCreated;
        $weblog->error("Test2: ".$cols);
		//Don't compare parameter exactly: escaping, unescaping while inserting to DB may create a difference.
		unset($cols["parameter"]);
		//Do a double check: whether last id corresponds to a an order id for this Temp Order
		$tempOrderArr = func_select_first("mk_temp_order", $cols);
        $weblog->error("Test3: ".$tempOrderArr);
		if(!empty($tempOrderArr)) {
			$tempOrderDO->setOrderId($orderIdCreated);
			return $tempOrderDO;
		}
		else {
			throw new OrderCreationFailedException("Failed to create order because temp order creation failed with the right parameters: ",$cols, "orderId");
		}
	}
	
	public function saveOrder(OrderDO $orderDO) {
		//Get the Associative array having DB Mapping between table fields (xcart_orders) and OrderDO
		$orderDOMapper = new OrderDOMapper($orderDO);
		$orderTblAssocArr = $orderDOMapper->getDBArray();
                global $telesalesUser;
                global $telesales;
                if ($telesales){
                    $add_fields['user_name']= $telesalesUser;
                    $result = func_array2insertWithTypeCheck_log("xcart_orders", $orderTblAssocArr,true,$add_fields);
                }
                else {
                    
                    $result = func_array2insertWithTypeCheck("xcart_orders", $orderTblAssocArr);
                }
		return $result;
	}
	
	/**
	 * Inserts an Order Detail row corresponding to item purchased 
	 * in Order Details Table 
	 * and returns an itemId for the row just insert
	 * @param $orderItem The OrderItemDO object to be inserted into the table
	 * @return itemId
	 */
	public function saveOrderItem(OrderItemDO $orderItem) {
		
		$orderItemDOMapper = new OrderItemDOMapper($orderItem);
		$orderDetailsTblAssocArr = $orderItemDOMapper->getDBArray();
		$itemId = func_array2insertWithTypeCheck("xcart_order_details", $orderDetailsTblAssocArr);
		return $itemId;
	}
	
	public function saveProduct(OrderItemDO $orderItem) {
		$xCartProductDOMapper = new XCartProductDOMapper($orderItem);
		$xCartProductTblAssocArr = $xCartProductDOMapper->getDBArray();
		if(!empty($xCartProductTblAssocArr)) {
			func_array2insertWithTypeCheck("xcart_products", $xCartProductTblAssocArr);
		}
	}
	
	/**
	 * Once all order items are inserted in order_details, update the quantity in order in orders table 
	 * @param integer $orderId
	 * @param integer $quantity
	 */
	public function updateOrderQuantity($orderId, $quantity) {
		func_array2updateWithTypeCheck("xcart_orders", 
			array("qtyInOrder" => array("type" =>"integer", "value"=> $quantity)), array("orderid" => $orderId)
		);
	}
	
	/**
	 * Save the quantity of each option of each item: 
	 * 
	 * @param integer $itemId The primary key (auto-generated and auto-incremented) field in xcart_order_details
	 * @param integer $optionId The option id corresponding to the size (e.g. S may correspond to option id 9, M to 10, etc) 
	 * @param integer $quantity The quantity of the above optionId of the above item in the current order
	 * 							ItemId is order-specific. If the same Item is purchased in different orders, itemId will
	 * 							be different for each order
	 */
	public function saveOptionOrderQuantity($itemId, $optionId, $quantity) {
		$tbl = "mk_order_item_option_quantity";
		
		$arr2ins = array("itemid" => $itemId, "optionid" => $optionId, "quantity" => $quantity);
		$res = func_array2insertWithTypeCheck($tbl, $arr2ins);
	}
	
	/**
	 * Check if this order was created by fraudulent means
	 * @param integer $orderId
	 */
	public function checkForFraud($orderId) {
		
	    $fraudMatch = func_query_first("select * from (
                            select o.orderid,o.total,o.shipping_cost,o.gift_charges,o.payment_surcharge, sum((od.actual_product_price* od.amount) + od.discount - od.coupon_discount_product - od.cash_redeemed - od.pg_discount) total_oi
                            from xcart_order_details od,xcart_orders o 
                            where o.orderid=od.orderid and  od.item_status!='IC' and od.orderid='$orderId') t
                            where abs( (CASE when (t.total-t.shipping_cost-t.gift_charges) < 0 then 0 else (t.total-t.shipping_cost) end) - abs(t.total_oi))>1"
	    			  );
		return $fraudMatch;
	}
	
	public function findExistingOrder($login, $orderCRC, $timeGap = 1800) {
		$findExistingOrderSql = "select max(ord.orderid) ".
								"from mk_temp_order tmpord ".
								"inner join xcart_orders ord on ord.orderid = tmpord.orderid ".
								"inner join mk_payments_log pmt on pmt.orderid = tmpord.orderid ".
								"where ".
								"unix_timestamp(now()) - pmt.time_insert < ($timeGap) and ".
								"tmpord.order_crc = $orderCRC ".
								"and ord.status = 'PP' ".
								"and ord.login = '".$login."'";
		
		$existingOrderid = func_query_first_cell($findExistingOrderSql);
		return $existingOrderid;
	}
	
	public function getCustomerInfo($login) {
		$customerInfo = func_select_first('xcart_customers', array('login' => $login));
		return (!empty($customerInfo)) ? $customerInfo : null;
	} 
}

?>