<?php
namespace order\dao\mapper;
use order\dataobject\OrderItemDO;

use base\dao\mapper\BaseDOMapper;

class OrderItemDOMapper extends BaseDOMapper {
	
	private $orderItem;
	public function __construct(OrderItemDO $orderItem) {
		$this->orderItem = $orderItem;
	}
	
	public function getDBArray() {
		$i = $this->orderItem;
		$templateId = $i->get("template_id");
		$discountRuleId = $i->get("discountRuleId");
		$discountRuleRevId = $i->get("discountRuleRevId");
                $packaging_type = $i->get("packagingType");
		
		if(!isset($discountRuleId)) {
			$discountRuleId = NULL;
		}
		if(!isset($discountRuleRevId)) {
			$discountRuleRevId = NULL;
		}
		
		$discountOnProduct  = ($i->get("discountAmount") != 0) ? $i->get("discountAmount") : $i->get("systemDiscount");
	
		$totalAmount = $i->get("totalAmount");
		$cartDiscount = $i->get("cartDiscount");

		$sellerId = $i->get("sellerId") ? $i->get("sellerId"): 1;
		$supply_type = $i->get("supplyType") ? $i->get("supplyType") : "ON_HAND";
		
		return array(
			"orderid" => $i->getOrderId(),//get from OrderDO
			
			"productid" =>  $i->get("productId"),
			"product_style" => $i->get("productStyleId"),
			"product_type" => $i->get("producTypeId"),
			"item_status" => 'UA',
			
			"is_customizable" => $i->get("isCustomized"),
			"jit_purchase" => $i->get("isJITSourced"),
			
			"style_template" => empty($templateId) ? 0 : $templateId,
	
			"price" => $i->get("productPrice"),
			"amount" => $i->get("quantity"),
			"total_amount" => $totalAmount, 
			"actual_product_price" => $i->get("actualPriceOfProduct"),
			"extra_data" => $i->get("customizedMessage"),
			"tax_rate" => $i->get("vatRate"),
			"taxamount" => $i->get("vatAmount"), 
			"shipping_amount" => $i->get("shippingAmount"), 
			"shipping_tax" => $i->get("shippingTaxAmount"), 
			
			"discount" => $discountOnProduct,
			"pg_discount" => $i->get("pgDiscount"),
			
			"coupon_code" => $i->get("couponCode"),
			"coupon_discount_product" => $i->get("couponDiscount"),
			/*"cash_redeemed" => $i->get("cashdiscount"),*/
			
			// MyntCash usage fields
			"cash_redeemed" => $i->get("totalMyntCashUsage"),
						
			"cash_coupon_code" => $i->get("cashCouponCode"),
			
			"quantity_breakup" => $i->get("quantityBreakup"), 
			"location" => $i->get("operation_location"),
				
			// loyalty points fields			
			"item_loyalty_points_used" => $i->get("loyaltyPointsUsed"),
			"item_loyalty_points_awarded" => $i->get("loyaltyPointsAwarded"),
			
			//EOSS-changes from Narendra	
			"is_discounted"  =>  $i->get("isDiscounted"),
			"discount_quantity"  =>  $i->get("discountQuantity"),
			"cart_discount_split_on_ratio"  =>  $cartDiscount,
			"discount_rule_id"  =>  $discountRuleId,
			"discount_rule_rev_id"  =>  $discountRuleRevId,
			"is_returnable"  =>  $i->get("isReturnable"),

			//seller and supply type info
			"seller_id" => $sellerId,
			"supply_type" => $supply_type,
			
                        //premium packaging
                        "packaging_type" => $packaging_type,
                    
			//unused parameters
			"product_options" => '',
			"provider" => "",
			"product" => "",
			"promotion_id" => 0,
		
		);
	}
}
