<?php
namespace order\dao\mapper;
use order\dataobject\ShippingDO;
use base\dao\mapper\BaseDOMapper;
class ShippingDOMapper extends BaseDOMapper {
	
	private $shippingDO;
	
	public function __construct(ShippingDO $shippingDO) {
		$this->shippingDO = $shippingDO;
	}
	
	public function getDBArray() {
		$s = $this->shippingDO;
		$a = $this->shippingDO->getAddressDO();
		return array (
			"warehouseid" => $s->getWarehouseId(),
			"shipment_preferences" => $s->getShippingPreferences(),	
		
			"s_title" => $a->getTitle(),
			"s_firstname" => $a->getName(),
			"s_address" => $a->getAddress(),
			"s_locality" => $a->getLocality(),
			"s_city" => $a->getCity(),
			"s_state" => $a->getState(),
			"s_country" => $a->getCountry(),
			"s_zipcode" => $a->getZipcode(),
			"s_email" => $a->getEmail(),
		
			"title" => $a->getTitle(),
			//should be customer's name.. set in orderdomapper
			//"firstname" => $a->getName(),
			"phone" => $a->getPhone(),
			"email" => $a->getEmail(),
			"mobile" => $a->getMobile(),
			"shipping_method" => $s->getShippingMethod()
			//should be customer's mobile number..set in orderdomapper 
			//"issues_contact_number" => $a->getMobile()
		);
	}
	
}
