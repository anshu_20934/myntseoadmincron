<?php
namespace order\dao\mapper;
use order\dataobject\OrderItemDO;

use base\dao\mapper\BaseDOMapper;

class XCartProductDOMapper extends BaseDOMapper {
	private $orderItem;
	
	public function __construct(OrderItemDO $orderItem) {
		$this->orderItem = $orderItem;
	}
	
	public function getDBArray() {
		//global $login; //fixme: for kundan it should not be global, but i am not able to find $login may not be needing it commented out 
		$i = $this->orderItem;
		$xcart_ipt_160 ='';
		$xcart_ti = '';
		$is_published = $i->get("is_published");
		$productId = $i->get("productId");
		$styleId = $i->get("productStyleId");
		$typeId = $i->get("producTypeId");
		$listPrice = $i->get("productPrice");
		$templateid = $i->get("template_id");
		$templateid = empty($templateid)?0:$templateid;
		$addonid = $i->get("addon_id");
		$addonid = empty($addonid)?0:$addonid;
		$time = time();

		//on refresh - shouldnot enter again - so check against the DB also
		$query = "select count(*) from xcart_products where productid=$productId";
		$res = db_query($query);
		$row = db_fetch_row($res);

		if( $row[0] ==  1) $is_published = 1;
		
		if( !$is_published )
		{
			$isCustomizable = $i->get("is_customizable");
            //if non customizable product
			if(isset($isCustomizable) &&  $isCustomizable == '0')
			{
				$res=func_query_first("select * from mk_style_properties where style_id=$styleId");
				$def_image=$res['default_image'];
				$thumb_image=str_replace("_images","_images_96_128",$def_image);
				$image_portal_thumbnail_160=str_replace("_images","_images_96_128",$def_image);
				$xcart_ipt_160 = $image_portal_thumbnail_160;
				$xcart_ti = $thumb_image;
			}
			
			return array(
				"productid" => $productId,
				"productcode" =>"MK$productId",
				"provider" => "", //was $login not needed may be.
				"list_price" => $listPrice,
				"add_date" => $time,
				"forsale" => 'Y',
				"designer" => 0,
				"statusid" => 1009,
				"product_type_id" => $typeId,
				"product_style_id" => $styleId,
				"descr" => '',
				"fulldescr" => '',
				"image_portal_t" => $xcart_ti,
				"is_publish" => 0,
				"image_portal_thumbnail_160" => $xcart_ipt_160,
			);
		} else {
			return null;
		}
		
	} 
	
}