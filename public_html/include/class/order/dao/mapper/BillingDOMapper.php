<?php
namespace order\dao\mapper;

use base\dao\mapper\BaseDOMapper;
use order\dataobject\BillingAddressDO;

class BillingDOMapper extends BaseDOMapper {
	
	private $billingAddressDO;
	
	public function __construct(BillingAddressDO $billingAddressDO) {
		$this->billingAddressDO  = $billingAddressDO;
	}
	
	public function getDBArray() {
		$b = $this->billingAddressDO;
		
		return array (
			"b_firstname" => $b->getName(),
			"b_address" => $b->getAddress(),
			"b_city" => $b->getCity(),
			"b_state" => $b->getState(),
			"b_country" => $b->getCountry(),
			"b_zipcode" => $b->getZipcode(),
			"b_mobile" => $b->getMobile(),
			"b_email" => $b->getEmail()		
		);
	}
	
}
