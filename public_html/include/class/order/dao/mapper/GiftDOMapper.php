<?php
namespace order\dao\mapper;
use order\dataobject\GiftDO;
use base\dao\mapper\BaseDOMapper;
class GiftDOMapper extends BaseDOMapper {
	
	private $giftDO;
	
	public function __construct(GiftDO $giftDO) {
		$this->giftDO = $giftDO;
	}
	
	function getDBArray() {
		$g = $this->giftDO;
		return array(
			"gift_status" => $g->getStatus(),
			"gift_charges" => $g->getCharge(),
			"notes" => $g->getMessage()
		);
	}
}