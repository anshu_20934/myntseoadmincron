<?php
namespace order\dao\mapper;
use order\dataobject\DiscountDO;

use base\dao\mapper\BaseDOMapper;

class DiscountDOMapper extends BaseDOMapper {
	
	private $discountDO;
	
	public function __construct(DiscountDO $discountDO) {
		$this->discountDO = $discountDO;
	}
	
	public function getDBArray() {
		$d = $this->discountDO;
		return array(
			
			/*"coupon_discount" => $d->getCouponDiscount(),
			"discount" => $d->getProductDiscount(),*/
			"cart_discount" => $d->getProductBagDiscount(),		
			"coupon" => $d->getCouponCode(),
			
			"cash_coupon_code" => $d->getCashCouponCode(),
			//MyntCash usage fields
			"cash_redeemed" => $d->getTotalMyntCashUsage(),
				
			//loyalty points usage and award fields			
			"loyalty_points_used" => $d->getLoyaltyPointsUsed(),
			"loyalty_points_awarded" => $d->getLoyaltyPointsAwarded(),
			"loyalty_points_conversion_factor"=>$d->getLoyaltyPointsConversionFactor()
		);
	}
}
