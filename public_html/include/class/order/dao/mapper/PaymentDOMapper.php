<?php
namespace order\dao\mapper;
use order\dataobject\PaymentDO;
use base\dao\mapper\BaseDOMapper;

class PaymentDOMapper extends BaseDOMapper {
	
	private $paymentDO;
	
	public function __construct(PaymentDO $paymentDO) {
		$this->paymentDO = $paymentDO;
	}
	
	public function getDBArray() {
		$p = $this->paymentDO;
		return array(
			"payment_method" => $p->getPaymentOption(),
			"cod_payment_date" => $p->getCODPaymentDate(), 
			"cod_pay_status" => $p->getCODPayStatus()			
		);
	}
}