<?php
namespace order\dao\mapper;
use order\dataobject\AmountDO;
use base\dao\mapper\BaseDOMapper;
class AmountDOMapper extends BaseDOMapper {
	
	private $amountDO;
	
	public function __construct(AmountDO $amountDO) {
		$this->amountDO = $amountDO;
	}
	
	public function getDBArray() {
		$a = $this->amountDO;
		
		return array (
			
			"total" => $a->getTotal(),
			"subtotal" => $a->getSubTotal(),
			"tax" => $a->getVat(),
			//emi charge
			"payment_surcharge" => $a->getEMICharge(),
			//Discounts
			"discount" => $a->getProductItemDiscount(),
			"coupon_discount" => $a->getCouponDiscount(),
			"cash_redeemed" => $a->getCashRedeemed(),
			"pg_discount" => $a->getPaymentGatewayDiscount(),
			
			//Shipping Charges
			// Shipping charges are saved in this column but are already part of total field
			"shipping_cost" => $a->getShippingCharge(),
			
			// Other Charges
			"cod" => $a->getCashOnDeliveryCharge(),
			"gift_charges" => $a->getGiftCharge(), 
		);
	}
	
}
