<?php
namespace order\dao\mapper;
use base\dao\mapper\BaseDOMapper;
class UnusedFieldsMapper extends BaseDOMapper{
	
	public function getDBArray() {
		return array (
			"membership" => "",
			"shopid" => 0,
			"guid" => 0,
			"affiliateid" => 0,
			"aff_discount" => 0,
			"aff_orderid" => 0,
			"orgid" => 0,
			"ref_discount" => 0,
		
			"shippingid" => 0,
			"courier_service" => "",
			"tracking" => "",
			
			"giftcert_discount" => 0.0,
			"giftcert_ids" => "",
		
			"taxes_applied" => 0,
		
			"details" =>  "",
			"customer_notes" => "",
			"extra" => "",
			"flag" => "N",
			"additional_info" => "",
		
			"paymentid" => "",
			
			"s_lastname" => "",
			"b_title" => "",
			"b_lastname" => "",
			"lastname" => "",
			"company" => "",
			"fax" => "",
			"url" => "",
			
		);
	}
}