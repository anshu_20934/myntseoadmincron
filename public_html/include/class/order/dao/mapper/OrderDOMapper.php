<?php
namespace order\dao\mapper;
use order\dataobject\OrderDO;
use base\dao\mapper\BaseDOMapper;

use order\dao\mapper\AmountDOMapper;
use order\dao\mapper\BillingDOMapper;
use order\dao\mapper\DiscountDOMapper;
use order\dao\mapper\GiftDOMapper;
use order\dao\mapper\PaymentDOMapper;
use order\dao\mapper\ShippingDOMapper;
use order\dao\mapper\UnusedFieldsMapper;

class OrderDOMapper extends BaseDOMapper {
	
	private $orderDO;
	
	public function __construct(OrderDO $orderDO) {
		$this->orderDO = $orderDO;
	}
	
	public function getDBArray() {
		$o = $this->orderDO;
		$orderFields = array(
			"orderid" => $o->getOrderId(),
			"order_name" => $o->getOrderName(),
			"source_id" => $o->getSourceId(),
			"login" => $o->getLogin(),
			"customer" => $o->getLogin(),
			"date" => $o->getDate(),
			"status" => $o->getStatus(),
			"channel" => $o->getChannel(),
			"request_server" => $o->getRequestServer(),
			"group_id" => $o->getGroupId(),
			"firstname" => $o->getCustomerName(),
			"issues_contact_number" => $o->getCustomerMobile(),
             "ordertype" => $o->getOrdertype()
		);
		
		//Get the DBArray from other Mappers composed in OrderDO
		$amtDOM = new AmountDOMapper($o->getAmountDO());
		$shipDOM = new ShippingDOMapper($o->getShippingDO());
		$billDOM = new BillingDOMapper($o->getBillingAddressDO());
		$discDOM = new DiscountDOMapper($o->getDiscountDO());
		$giftDOM = new GiftDOMapper($o->getGiftDO());
		$pmtDOM = new PaymentDOMapper($o->getPaymentDO());
		$unusedFieldsDOM = new UnusedFieldsMapper();
		
		return array_merge($orderFields, $amtDOM->getDBArray(), $shipDOM->getDBArray(), 
			$billDOM->getDBArray(), $discDOM->getDBArray(), $giftDOM->getDBArray(),
			$pmtDOM->getDBArray(), $unusedFieldsDOM->getDBArray());
	}
	
}
