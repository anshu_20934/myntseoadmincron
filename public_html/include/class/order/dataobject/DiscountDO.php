<?php
namespace order\dataobject;
use base\dataobject\BaseDO;

/*
 * contains all fields related to promotions in a cart/order
 */
class DiscountDO extends BaseDO {
	
	private $couponCode, $cashCouponCode;
	private $couponDiscount, $couponPercent; 
	private $cashRedeemed;
	private $productItemDiscount;
	private $productBagDiscount;
	
	private $totalMyntCashUsage;
	
	/*
	 * loyalty points related fields
	 */
	private $loyaltyPointsAwarded = 0;
	private $loyaltPointsUsed = 0;
	private $loyaltyPointsConversionFactor = 0;
	
	public function __construct() {
		;
	}
	//--Coupon Code
	public function getCouponCode() {
		return $this->couponCode;
	}
	public function setCouponCode($couponCode) {
		$this->couponCode = $couponCode;
	}
	//--Cash Coupon Code
	public function getCashCouponCode() {
		return $this->cashCouponCode; 
	}
	public function setCashCouponCode($cashCouponCode) {
		$this->cashCouponCode = $cashCouponCode; 
	}
	//--Coupon Discount	
	public function getCouponDiscount() {
		return $this->couponDiscount;
	}
	public function setCouponDiscount($couponDiscount) {
		$this->couponDiscount = $couponDiscount;
	}
	//--Coupon Percent	
	public function getCouponPercent() {
		return $this->couponPercent;
	}
	public function setCouponPercent($couponPercent) {
		$this->couponPercent = $couponPercent;
	}
	//--Cash Redeemed	
	public function getCashRedeemed() {
		return $this->cashRedeemed;
	}
	public function setCashRedeemed($cashRedeemed) {
		$this->cashRedeemed = $cashRedeemed;
	}
	
	//-- MyntCash usage
		
	public function getTotalMyntCashUsage() {
		return $this->totalMyntCashUsage;
	}
	public function setTotalMyntCashUsage($totalMyntCashUsage) {
		$this->totalMyntCashUsage = $totalMyntCashUsage;
	}
	
	//--Product Item Discount	
	public function getProductItemDiscount() {
		return $this->productItemDiscount;
	}
	public function setProductItemDiscount($productItemDiscount) {
		$this->productItemDiscount = $productItemDiscount;
	}
	
	//--Product Bag Discount	
	public function getProductBagDiscount() {
		return $this->productBagDiscount;
	}
	public function setProductBagDiscount($productBagDiscount) {
		$this->productBagDiscount = $productBagDiscount;
	}
	
	// loyalty points related fields
	public function setLoyaltyPointsUsed($points){
		$this->loyaltPointsUsed = $points;
	}
	
	public function getLoyaltyPointsUsed(){
		return $this->loyaltPointsUsed;
	}
	
	public function setLoyaltyPointsAwarded($points){
		$this->loyaltyPointsAwarded= $points;
	}
	
	public function getLoyaltyPointsAwarded(){
		return $this->loyaltyPointsAwarded;
	}
	
	public function setLoyaltyPointsConversionFactor($factor){
		$this->loyaltyPointsConversionFactor = $factor;
	}
	
	public function getLoyaltyPointsConversionFactor(){
		return $this->loyaltyPointsConversionFactor;
	}
}