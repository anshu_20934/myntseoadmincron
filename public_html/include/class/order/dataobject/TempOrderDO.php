<?php
namespace order\dataobject;
use base\dataobject\BaseDO;

class TempOrderDO extends BaseDO {
	
	private $orderId, $sessionId, $params, $checksumKey, $orderCRC;
	
	public function __construct() {
		
	}
	
	public function getOrderId() {
		return $this->orderId;
	}
	public function setOrderId($orderId) {
		$this->orderId = $orderId;
	}
		
	public function getSessionId() {
		return $this->sessionId;
	}
	public function setSessionId($sessionId) {
		$this->sessionId = $sessionId;
	}
	
	public function getParams() {
		return $this->params;
	}
	public function setParams($params) {
		$this->params = $params;
	}
	
	public function getChecksumKey() {
		return $this->checksumKey;
	}
	public function setChecksumKey($checksumKey) {
		$this->checksumKey = $checksumKey;
	}
	
	public function getOrderCRC() {
		return $this->orderCRC;
	}
	public function setOrderCRC($orderCRC) {
		$this->orderCRC = $orderCRC;
	}	
}