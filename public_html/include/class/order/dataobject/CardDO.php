<?php
namespace order\dataobject;
use base\dataobject\BaseDO;

class CardDO extends BaseDO {
	//Fields for credit-card-payment
	private $number, $holderName;
	private $expiryMonth, $expiryYear, $cvvNumber;
	
	public function __construct($number, $holderName, $expiryMonth, $expiryYear, $cvvNumber) {
		$number = str_replace('-', '', $number);    // baseline single quote
		$number = str_replace(' ', '', $number);    // baseline single quote
		$this->number = $number;
		$this->holderName = $holderName;
		$this->expiryMonth = $expiryMonth;
		$this->expiryYear = $expiryYear;
		$this->cvvNumber = $cvvNumber;
	}
	public function getNumber() {
		return $this->number;
	}
	public function getHolderName() {
		return $this->holderName;
	}
	public function getExpiryMonth() {
		return $this->expiryMonth;
	}
	public function getExpiryYear() {
		return $this->expiryYear;
	}
	public function getCvvNumber() {
		return $this->cvvNumber;
	}
}
?>