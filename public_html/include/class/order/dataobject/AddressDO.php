<?php
namespace order\dataobject;
use base\dataobject\BaseDO;

class AddressDO extends BaseDO {
	
	private $title, $name,$country, $state, $zipcode, $city, $locality, $address, 
		$addressId, $email, $mobile, $phone, $addressArr;
	
	public function __construct($addressArr) {
		//if nothing is provided as argument, the caller 
		//can still set all the fields through setters
		if(empty($addressArr)) {
			return;
		}
		$this->title = $addressArr['title'];
		$this->addressId = $addressArr['id'];
		$this->name = $addressArr['name']; 
		$this->address = str_ireplace("\r\n"," ", $addressArr['address']);
		$this->locality = $addressArr['locality'];
		$this->city = $addressArr['city'];
		$this->state = $addressArr['state'];
		$this->country = $addressArr['country'];
		$this->zipcode = strtolower($addressArr['pincode']);
		$this->mobile = html_entity_decode( $addressArr['mobile'] );
		$this->phone = html_entity_decode( $addressArr['phone'] );
		$this->email = html_entity_decode( $addressArr['email'] );
		//don't use pincode as it comes from db. use zipcode instead
		$addressArr['zipcode'] = $addressArr['pincode'];
		unset($addressArr['pincode']);
		
		$this->addressArr = $addressArr;
	}
	public function getAddressArr() {
		if(empty($this->addressArr)) {
			$this->addressArr['title'] = $this->title;
			$this->addressArr['id'] = $this->addressId;
			$this->addressArr['name'] = $this->name; 
			$this->addressArr['address'] = $this->address;
			$this->addressArr['locality'] = $this->locality;
			$this->addressArr['city'] = $this->city;
			$this->addressArr['state'] = $this->state;
			$this->addressArr['country'] = $this->country;
			$this->addressArr['pincode'] = $this->zipcode;
			$this->addressArr['mobile'] = $this->mobile;
			$this->addressArr['phone'] = $this->phone;
			$this->addressArr['email'] = $this->email;
		}
		return $this->addressArr;
	}
	public function getZipcode() {
		return $this->zipcode;
	}
	public function setZipcode($zipcode) {
		$this->zipcode = $zipcode;
	}
	public function getCountry() {
		return $this->country;
	}
	public function setCountry($country) {
		$this->country = $country;
	}
	public function getState() {
		return $this->state;
	}
	public function setState($state) {
		$this->state = $state;
	}
	public function getCity() {
		return $this->city;
	}
	public function setCity($city) {
		$this->city = $city;
	}
	public function getLocality() {
		return $this->locality;
	}
	public function setLocality($locality) {
		$this->locality = $locality;
	}
	public function getAddress() {
		return $this->address;
	}
	public function setAddress($address) {
		$this->address = str_ireplace("\r\n", " ", $address);
	}
	public function getAddressId() {
		return $this->addressId;
	}
	public function setAddressId($addressId) {
		$this->addressId = $addressId;
	}
	public function getMobile() {
		return $this->mobile;
	}
	public function setMobile($mobile) {
		$this->mobile = html_entity_decode( $mobile );
	}
	public function getPhone() {
		return $this->phone;
	}
	public function setPhone($phone) {
		$this->phone = html_entity_decode($phone);
	}
	public function getEmail() {
		return $this->email;
	}
	public function setEmail($email) {
		$this->email = html_entity_decode( $email );
	}
	public function getName() {
		return $this->name;
	}
	public function setName($name) {
		$this->name = $name;
	}
	public function getTitle() {
		return $this->title;
	}
	public function setTitle($title) {
		$this->title = $title;
	}
}