<?php
namespace order\dataobject;
use base\dataobject\BaseDO;

class GiftDO extends BaseDO {
	private $status, $message, $charge;
	
	public function __construct($status, $message, $charge) {
		$this->status = $status;
		$this->message = $message;
		$this->charge = $charge;
	}
	
	public function getCharge() {
		return $this->charge;
	}
	public function getStatus() {
		return $this->status;
	}
	public function getMessage() {
		return $this->message;
	}
}