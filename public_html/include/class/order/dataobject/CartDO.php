<?php
namespace order\dataobject;
use order\exception\EmptyCartException;
use base\dataobject\BaseDO;
require_once \HostConfig::$documentRoot."/include/class/mcart/class.MCart.php";


class CartDO extends BaseDO {
	
	private $cart, $productsInCart, $productIds, $cartAmount;
	private $subTotal, $totalQuantity, $discountRationSum, $productTypeQuantityArray;

	public function __construct(\MCart $cart) {
		if($cart == null) {
			throw new EmptyCartException("Cart Empty: cannot create cart object without any products in it");
		}
		$this->cart = $cart;
		$this->cartAmount =  number_format($cart->getMrp()+$cart->getAdditionalCharges(), 2, ".", '');
		$this->productsInCart = \MCartUtils::convertCartToArray($this->cart);
		if(empty($this->productsInCart)) {
			throw new EmptyCartException("Cart Empty: cannot create cart object without any products in it");
		}
		$this->productIds = $this->cart->getProductIdsInCart();
		$this->initParams();
	}
	
	public function getCart() {
		return $this->cart;
	}
	
	public function getProductsInCart() {
		return $this->productsInCart;
	}
	
	public function getProductIds() {
		return $this->productIds;
	}
	
	public function getCartAmount() {
		return $this->cartAmount;
	}
	
	public function getProductTypeQuantityArray() {
		return $this->productTypeQuantityArray;
	}
	
	protected function initParams() {
		
		$this->discountRatioSum = 0;
		$this->productTypeQuantityArray = array();
		foreach($this->productsInCart AS $key => $productDetail) {
			//Find the sum of discount ratio
			if($productDetail['discount'] == 0)
				$this->discountRatioSum += $productDetail['totalPrice'];
			$this->subtotal += $productDetail['totalPrice'];
			$this->totalQuantity +=  $productDetail['quantity'];
		
			//Create an array for quantities of product type 
			if(array_key_exists($productDetail['producTypeId'], $this->productTypeQuantityArray)) {
				$this->productTypeQuantityArray[$productDetail[producTypeId]]['qty'] += $productDetail['quantity'];
				$this->productTypeQuantityArray[$productDetail[producTypeId]]['stylecount']++;   
			}
			else {
				$this->productTypeQuantityArray[$productDetail[producTypeId]]['qty'] = $productDetail['quantity'];
				$this->productTypeQuantityArray[$productDetail[producTypeId]]['stylecount'] = 1;
			}
		}
	}
	
}
