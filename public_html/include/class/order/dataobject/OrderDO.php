<?php
namespace order\dataobject;
use base\dataobject\BaseDO;
require_once \HostConfig::$documentRoot."/include/class/mcart/class.MCart.php";
class OrderDO extends BaseDO {
	
	private $orderId, $orderName, $sourceId, $groupId;	
	//-- All Order related stuff directly
	private $login, $userType, $sessionId;
	
	private $customerName; 
	
	private $customerMobile;
	
	private $channel, $status;
	
	private $ordertype;

	//Order Date (Timestamp)
	private $date;
	
	private $requestServer, $responseServer;
	
	//-- All DOs
    private $inputParamDO;
	private $ShippingDO, $billingAddressDO;
	private $paymentDO, $shippingDO, $cartDO;
	private $discountDO, $giftDO;
	
	private $amountDO;
	
	private $tempOrderDO;
	
	private $orderItems;
	
	private $totalQuantity;
    
    // context of the order (eg: telesales etc..)
    private $context='default';
	
	public function __construct(\MCart $cart) {				
		$this->login = $cart->getLogin();
        // set the context from cart(eg: telesales etc)
        $this->context = $cart->getContext();
	}
	//Order ID
	public function getOrderId() {
		return $this->orderId;
	}
	public function setOrderId($orderId) {
		$this->orderId = $orderId;
	}

    // context
    public function setContext($context){
        $this->context=$context;		
	}
	public function getContext(){
		return $this->context;
	}
	
	//Order name
	public function getOrderName() {
		return $this->orderName;
	}
	public function setOrderName($orderName) {
		$this->orderName = $orderName;
	}
	//Login
	public function getLogin() {
		return $this->login;
	}
    public function setLogin($login) {
        $this->login = $login;
	}
	//CustomerName
	public function getCustomerName() {
		return $this->customerName;
	}
	public function setCustomerName($customerName) {
		$this->customerName = $customerName;
	}
	//CustomerName
	public function getCustomerMobile() {
		return $this->customerMobile;
	}
	public function setCustomerMobile($mobile) {
		$this->customerMobile = $mobile;
	}
	//User Type
	public function getUserType() {
		return $this->userType;
	}
	public function setUserType($userType) {
		$this->userType = $userType;
	}
	
	public function getSessionId() {
		return $this->sessionId;
	}

    public function setSessionId($sessionId) {
		return $this->sessionId = $sessionId;
	}

	//Request & Response Servers
	public function getRequestServer() {
		return $this->requestServer;
	}
	public function setRequestServer($requestServer) {
		$this->requestServer = $requestServer;
	}
	public function getResponseServer() {
		return $this->responseServer;
	}
	public function setResponseServer($responseServer) {
		$this->responseServer = $responseServer;
	}
    
    //--input parameters
	public function getInputParamDO() {
		return $this->inputParamDO;
	}
	public function setInputParamDO($inputParamDO) {
		$this->inputParamDO = $inputParamDO;
	}

	//--Cart
	public function getCartDO() {
		return $this->cartDO;
	}
	public function setCartDO($cartDO) {
		$this->cartDO = $cartDO;
	}
	
	//--Shipping Address
	public function getShippingDO() {
		return $this->ShippingDO;
	}
	public function setShippingDO(ShippingDO $ShippingDO) {
		$this->ShippingDO = $ShippingDO;
	}
	
	//--Billing Address
	public function getBillingAddressDO() {
		return $this->billingAddressDO;
	}
	public function setBillingAddressDO(BillingAddressDO $billingAddressDO) {
		$this->billingAddressDO = $billingAddressDO;
	}
	
	//--Payment
	public function getPaymentDO() {
		return $this->paymentDO;
	}
	public function setPaymentDO(PaymentDO $paymentDO) {
		$this->paymentDO = $paymentDO;
	}
	
	//--Discount
	public function getDiscountDO() {
		return $this->discountDO;
	}
	public function setDiscountDO(DiscountDO $discountDO) {
		$this->discountDO = $discountDO;
	}
	
	//--Gift
	public function getGiftDO() {
		return $this->giftDO;
	}
	public function setGiftDO(GiftDO $giftDO) {
		$this->giftDO = $giftDO;
	}
	
	//--Temp Order DO
	public function getTempOrderDO() {
		return $this->tempOrderDO;
	}
	public function setTempOrderDO($tempOrderDO) {
		$this->tempOrderDO = $tempOrderDO;
	}

	//--Amount DO
	public function getAmountDO() {
		return $this->amountDO;
	}
	public function setAmountDO(AmountDO $amountDO) {
		$this->amountDO = $amountDO;
	}
	//Order Item DO array
	public function getOrderItems() {
		return $this->orderItems;
	}
	public function setOrderItems(array $orderItems) {
		$this->orderItems = $orderItems;
	}
	
	//--Total Quantity in Order
	public function getTotalQuantity() {
		return $this->totalQuantity;
	}
	public function setTotalQuantity($totalQuantity) {
		$this->totalQuantity = $totalQuantity;
	}
	//--Channel
	public function getChannel() {
		return $this->channel;
	}
	public function setChannel($channel) {
		$this->channel = $channel;
	}
	
	//--Order Status
	public function getStatus() {
		return $this->status;
	}
	public function setStatus($status) {
		$this->status = $status;
	}
	
	//--Order Date
	public function getDate() {
		return $this->date;
	}
	public function setDate($date) {
		$this->date = $date;
	}
	
	//--Source Id
	public function getSourceId() {
		return $this->sourceId;
	}
	public function setSourceId($sourceId) {
		$this->sourceId = $sourceId;
	}
	
	public function getGroupId() {
		return $this->groupId; 
	}
	
	public function setGroupId($groupId) {
		 $this->groupId = $groupId;
	}

    public function getOrdertype() {
        return $this->ordertype;
    }

    public function setOrdertype($ordertype) {
    	$this->ordertype = $ordertype;
    }
}