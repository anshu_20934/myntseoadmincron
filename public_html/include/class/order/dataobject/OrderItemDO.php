<?php
namespace order\dataobject;

use base\dataobject\BaseDO;

class OrderItemDO extends BaseDO {
	
	private $orderId ;
	private $itemId;
	
	/**
	 * Conscious decision to keep it as associative array only: there are too many fields and
	 * all get set from $productDetail Arraya anyways. Only few fields are derived 
	 * and they'll be maintained here separately from the productDetail array
	 * @var array
	 */
	private $productDetail;

	/*
	, $productId, $styleId, $productTypeId, $styletemplate, $operationLocation;
	private $itemStatus, $isCustomizable, $isReturnable, $isJITPurchase, $isDiscounted;

	private $productPrice, $quantity, $quantityBreakup, $totalAmount, $actualProductPrice;
	private $vatRate, $vatAmount, $shippingAmount, $shippingTaxAmount;
	
	private $discount, $discountRuleId, $discountRuleRevId, $paymentGatewayDiscount;
	
	private $discountQuantity, $cartDiscount;

	private $couponCode, $cashCouponCode, $couponDiscount, $cashRedeemed; 
	*/
	
	/* 
	Computed fields:
	private $totalAmount, $vatRate, $vatAmount, $shippingAmount, $shippingTaxAmount;
	private $discount, $paymentGatewayDiscount, $couponCode, $cashCouponCode, $quantityBreakup;
	*/
	public function __construct($orderId, $productDetail) {
		$this->orderId = $orderId;
		$this->productDetail = $productDetail;	
	}
	
	public function getOrderId() {
		return $this->orderId;
	}
	
	public function getItemId() {
		return $this->itemId;
	}
	
	public function setItemId($itemId) {
		$this->itemId = $itemId;
	}
	
	public function get($key) {
		return $this->productDetail[$key];
	}

	public function set($key, $value) {
		$this->productDetail[$key] = $value;
	}
	
	public function getAllDetails() {
		return $this->productDetail;
	}
}