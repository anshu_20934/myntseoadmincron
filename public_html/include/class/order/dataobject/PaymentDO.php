<?php
namespace order\dataobject;
use base\dataobject\BaseDO;

class PaymentDO extends BaseDO {
	private $paymentMethod, $paymentOption, $paymentGatewayDiscount;

	//fields for credit/debit card are encapsulated in CardDO
	private $cardDO;
	
	//Fields for netbanking
	private $bankId;
	
	//Fields for COD
	private $codCharge, $codPaymentDate, $codPayStatus;
	
	//The field used for storing the payment gateway
	private $gateway;
	
	private $emiBank;
	
	private $emiCharge;
	
	public function __construct($paymentMethod, $cardDO, $bankId = null) {
		$this->paymentMethod = $paymentMethod;
		$this->cardDO = $cardDO; 
		$this->bankId = $bankId;
	}

	public function isCODOrder() {
		return ($this->paymentOption === 'cod');
	}
	
	public function getPaymentOption() {
		return $this->paymentOption;
	}
	public function setPaymentOption($paymentOption) {
		$this->paymentOption = $paymentOption;
	}
	
	public function getPaymentMethod() {
		return $this->paymentMethod;
	}

	public function getCardDO() {
		return $this->cardDO;
	}
	public function setCardDO($cardDO) {
		$this->cardDO = $cardDO;
	}
	
	public function setEMIBank($emiBank) {
		$this->emiBank = $emiBank;
	}
	
	public function getPaymentGatewayDiscount() {
		return $this->paymentGatewayDiscount;
	}
	public function setPaymentGatewayDiscount($paymentGatewayDiscount) {
		$this->paymentGatewayDiscount = $paymentGatewayDiscount;
	}
	
	public function getEMICharge() {
		return $this->emiCharge;
	}
	
	public function setEMICharge($emiCharge) {
		return $this->emiCharge;
	}
	
	public function getBankId() {
		return $this->bankId;
	}
	
	public function getEMIBank() {
		return $this->emiBank;
	}
	
	public function getGateway() {
		return $this->gateway;
	}
	public function setGateway($gateway) {
		$this->gateway = $gateway;
	}
	
	public function getCODCharge() {
		return $this->codCharge;
	}
	public function setCODCharge($codCharge) {
		return $this->codCharge = $codCharge;
	}
	
	public function getCODPayStatus() {
		return $this->codPayStatus;
	}
	public function setCODPayStatus($codPayStatus) {
		return $this->codPayStatus = $codPayStatus;
	}
	
	public function getCODPaymentDate() {
		return $this->codPaymentDate;
	}
	public function setCODPaymentDate($codPaymentDate) {
		return $this->codPaymentDate = $codPaymentDate;
	}
	
}