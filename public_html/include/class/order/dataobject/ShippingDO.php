<?php
namespace order\dataobject;
use base\dataobject\BaseDO;

/**
 * ShippingDO is basically AddressDO plus some extra fields 
 * as well as logistic id
 * @author kundan
 */
class ShippingDO extends BaseDO {
	
	private $logisticId, $addressDO, $shippingRate, $shippingRateArray, 
			$warehouseId, $shippingPreferences;
	
	private $shippingMethod = 'NORMAL';		
	
	public function __construct(AddressDO $addressDO, $logisticId, $shipmentPreferences, $shippingMethod) {
		$this->logisticId = $logisticId;
		$this->addressDO = $addressDO;
		$this->shippingPreferences = $shipmentPreferences;
                $this->shippingMethod = $shippingMethod;
	}
	public function getLogisticId() {
		return $this->logisticId;
	}
	public function getAddressDO() {
		return $this->addressDO;
	}
	public function getAddressArr() {
		return $this->addressDO->getAddressArr();
	}
	public function getShippingMethod() {
		return $this->shippingMethod;
	}
	public function setShippingMethod($shippingMethod) {
		$this->shippingMethod = $shippingMethod;
	}
	public function getShippingRate() {
		return $this->shippingRate;
	}
	public function setShippingRate($shippingRate) {
		$this->shippingRate = $shippingRate;
	}
	public function getShippingRateArray() {
		return $this->shippingRateArray;
	}
	public function setShippingRateArray(array $shippingRateArray) {
		$this->shippingRateArray = $shippingRateArray;
	}
	public function getWarehouseId() {
		return $this->warehouseId;
	}
	public function setWarehouseId($warehouseId) {
		$this->warehouseId = $warehouseId;
	}
	public function getShippingPreferences() {
		return $this->shippingPreferences;
	}
	public function setShippingPreferences($shippingPreferences) {
		$this->shippingPreferences = $shippingPreferences;
	}
}