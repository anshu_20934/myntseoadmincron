<?php
namespace order\dataobject;
use base\dataobject\BaseDO;
class AmountDO extends BaseDO {
	
	//input fields: from session as of today
	private $mrp, $vat, $amount, $totalAmount;
	
	//TODO: check from Mitesh what's the use of TotalAmount: it is used only in CODEligibility and then just inserted in xcart_orders table
	
	//discount fields
	private $productItemDiscount, $productBagDiscount, $couponDiscount, $cashRedeemed, $paymentGatewayDiscount;
	
	//charges
	private $shippingCharge, $cashOnDeliveryCharge, $giftCharge, $emiCharge;
	
	//derived fields
	private $total, $subTotal, $totalBeforePGDiscount, $amountWithProductDiscount, $finalAmountToCharge;
	
	public function __construct($mrp, $vat, $amount, $totalAmount) {
		$this->mrp = $mrp; 
		$this->vat = $vat; 
		$this->amount = $amount;
		$this->totalAmount = $totalAmount;
	}
	
	//All initial non-discount Session-Set fields here
	/**
	 * Maximum Retail Price ??
	 */
	public function getMRP() {
		return $this->mrp;
	}
	public function getVat() {
		return $this->vat;
	}
	/**
	 * Amount: The Cart Amount : excludes any discounts (Coupon Discount, Cash Redeemed, even Product Discount: EOSS?) and any charges
	 */
	public function getAmount() {
		return $this->amount;
	}
	public function getTotalAmount() {
		return $this->totalAmount;
	}
	public function setTotalAmount($totalAmount) {
		$this->totalAmount = $totalAmount;
	}
	
	//All Discounts here
	public function getProductItemDiscount() {
		return $this->productItemDiscount;
	}
	public function setProductItemDiscount($productDiscount) {
		$this->productItemDiscount = $productDiscount;
	}
	public function getProductBagDiscount() {
                return $this->productBagDiscount;
        }
        public function setProductBagDiscount($productDiscount) {
                $this->productBagDiscount = $productDiscount;
        }
	public function getCouponDiscount() {
		return $this->couponDiscount;
	}
	public function setCouponDiscount($couponDiscount) {
		$this->couponDiscount = $couponDiscount;
	}
	public function getCashRedeemed() {
		return $this->cashRedeemed;
	}
	public function setCashRedeemed($cashRedeemed) {
		$this->cashRedeemed = $cashRedeemed;
	}
	public function getPaymentGatewayDiscount() {
		return $this->paymentGatewayDiscount;
	}
	public function setPaymentGatewayDiscount($paymentGatewayDiscount) {
		$this->paymentGatewayDiscount = $paymentGatewayDiscount;
	}
	
	//All Charges here
	public function getShippingCharge() {
		return $this->shippingCharge;
	}
	public function setShippingCharge($shippingCharge) {
		$this->shippingCharge = $shippingCharge;
	}
	
	public function getEMICharge() {
		return $this->emiCharge;
	}
	public function setEMICharge($emiCharge) {
		$this->emiCharge = $emiCharge;
	}
	
	public function getCashOnDeliveryCharge() {
		return $this->cashOnDeliveryCharge;
	}
	public function setCashOnDeliveryCharge($cashOnDeliveryCharge) {
		$this->cashOnDeliveryCharge = $cashOnDeliveryCharge;
	}
	public function getGiftCharge() {
		return $this->giftCharge;
	}
	public function setGiftCharge($giftCharge) {
		$this->giftCharge = $giftCharge;
	}
	
	//All derived, computed fields here
	public function getTotal() {
		return $this->total;
	}
	public function setTotal($total) {
		$this->total = $total;
	}
	public function getSubTotal() {
		return $this->subTotal;
	}
	public function setSubTotal($subTotal) {
		$this->subTotal = $subTotal;
	}
	
	/**
	 * Payment Gateway discount is applied on this Total Amount
	 */
	public function getTotalBeforePGDiscount() {
		return $this->totalBeforePGDiscount;
	}
	public function setTotalBeforePGDiscount($totalBeforePGDiscount) {
		$this->totalBeforePGDiscount = $totalBeforePGDiscount;
	}
	
	/**
	 * This field is used to divide payment gateway discount (pg_discount) propotionally in for xcart_order_details table. 
	 * Also used in calculation of Payment Gateway Discount
	 */
	public function getAmountWithProductDiscount() {
		return $this->amountWithProductDiscount;
	}
	public function setAmountWithProductDiscount($amountWithProductDiscount) {
		$this->amountWithProductDiscount = $amountWithProductDiscount;
	}
	
	/**
	 * This is the amount that is passed to payment gateway/finally charged to the customer
	 */
	public function getFinalAmountToCharge() {
		return $this->finalAmountToCharge;
	}
	public function setFinalAmountToCharge($finalAmountToCharge) {
		$this->finalAmountToCharge = $finalAmountToCharge;
	}
}
