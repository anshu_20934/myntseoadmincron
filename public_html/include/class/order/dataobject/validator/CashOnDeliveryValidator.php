<?php
namespace order\dataobject\validator;

use order\dataobject\OrderDO;
use base\dataobject\validator\Validator;
use order\exception\CashOnDeliveryEligibilityFailedException;

class CashOnDeliveryValidator implements Validator {
	
	private $orderDO;
	public function __construct(OrderDO $orderDO) {
		$this->orderDO = $orderDO;
	}
	
	public function validate() {
		$paymentDO = $this->orderDO->getPaymentDO();
		$amountDO = $this->orderDO->getAmountDO();
		
		$codCharge = 0;
		if($paymentDO->isCODOrder()) {
			
			$totalAmount = round($amountDO->getTotalAmount());
			$cashRedeemed = round($amountDO->getCashRedeemed());
			$shippingDO = $this->orderDO->getShippingDO();
			$address = $shippingDO->getAddressArr();
			$cartDO = $this->orderDO->getCartDO();
			$productsInCart = $cartDO->getProductsInCart();
			
			$jewelleryItemsInOrder = 'none'; // other values can be 'some', 'all'
			$jewelleryItemsCount = 0;
			
			$skuDetails = array();
			foreach($productsInCart as $index=>$product){
				$fineJewelleryItem = func_query("select * from mk_attribute_type at, mk_attribute_type_values av, mk_style_article_type_attribute_values sav where at.id = av.attribute_type_id and av.id = sav.article_type_attribute_value_id and at.catalogue_classification_id = ".$product['article_type']." and sav.product_style_id = ".$product['productStyleId'] ." and av.attribute_value = 'Fine Jewellery'", true);
				if($fineJewelleryItem && count($fineJewelleryItem)>0){
					$jewelleryItemsCount++;
					$jewelleryItemsInOrder = 'some';
				}
				$productsInCart[$index]['style_id'] = $product['productStyleId'];
				$productsInCart[$index]['sku_id'] = $product['skuid'];
				$skuDetails[$product['skuid']] = array('id'=>$product['skuid'], 'availableItems'=> $product['availableItems'], 'warehouse2AvailableItems'=> $product['warehouse2AvailableItems']);
			}
			
			if($jewelleryItemsCount == count($productsInCart)){
				$jewelleryItemsInOrder = 'all';
			}
			
			$courierServiceabilityVersion = \FeatureGateKeyValuePairs::getFeatureGateValueForKey('lms.courierserviceability.version', 'old');
			if($courierServiceabilityVersion == 'new') {
				$paymentModes = getServiceablePaymentModesForProducts($address['zipcode'], $productsInCart, false, false, $skuDetails);
				$codElegiblity = verifyCODElegiblityForUser($this->orderDO->getLogin(), $address['mobile'],$address['country'],$totalAmount, strtolower($address['zipcode']),
						$productsInCart, $cartDO->getProductIds(),$cashRedeemed, $jewelleryItemsInOrder, false, $paymentModes);
			} else {
				$codElegiblity = verifyCODElegiblityForUser($this->orderDO->getLogin(),$address['mobile'], $address['country'],$totalAmount,strtolower($address['zipcode']),
						$productsInCart, $cartDO->getProductIds(),$cashRedeemed, $jewelleryItemsInOrder);
			}
			
			$codErrorCode = $codEligiblity['errorCode'];
			
			if($codErrorCode!=0) {
				throw new CashOnDeliveryEligibilityFailedException($codEligiblity, "User not eligible for Cash On Delivery for this order");
			}
		}
	}
}
