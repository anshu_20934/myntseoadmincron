<?php
namespace order\dataobject\validator;

use order\dataobject\ShippingDO;
use base\dataobject\validator\Validator;
use order\exception\NoShippingAddressException;

class ShippingDOValidator implements Validator {
	
	private $shippingDO;
	
	public function __construct(ShippingDO $shippingDO) {
		$this->shippingDO = $shippingDO;
	}
	
	public function validate() {
		$addressDO = $this->shippingDO->getAddressDO();
		$dataObjName = "ShippingDO->AddressDO";
		$errFields;
		$isValid = true;
		if($addressDO == null) {
			$errFields = array("country", "state", "city", "zipcode");
			$isValid = false;
		} else {
			$errFields = array();
			
			$country = $addressDO->getCountry();
			if(empty($country)) {
				$errFields[] = "country";
				$isValid = false;
			}
			
			$state = $addressDO->getState();
			if(empty($state)) {
				$errFields[] = "state";
				$isValid = false;
			}
			
			$city = $addressDO->getCity();
			if(empty($city)) {
				$errFields[] = "city";
				$isValid = false;
			}
			
			$zipcode = $addressDO->getZipcode();
			if(empty($zipcode)) {
				$errFields[] = "zipcode";
				$isValid = false;
			}
		}
		if(!$isValid) {
			throw new NoShippingAddressException("Abort: Shipping address or part of it is blank.", $dataObjName, $errFields);
		}
	}
}