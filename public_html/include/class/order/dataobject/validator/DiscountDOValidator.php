<?php
namespace order\dataobject\validator;

use order\dataobject\DiscountDO;
use order\dataobject\OrderDO;
use base\dataobject\validator\Validator;
global $xcart_dir;
require_once \HostConfig::$documentRoot."/modules/coupon/database/CouponAdapter.php";
require_once \HostConfig::$documentRoot."/modules/coupon/CouponValidator.php";
require_once \HostConfig::$documentRoot."/include/func/func.mk_orderbook.php";
/**
 * DiscountValidation is always in context of an order
 * @author kundan
 *
 */
class DiscountDOValidator implements Validator {
	
	private $orderDO;
	private $adapter, $validator;
	/**
	 * Discount may need to be validated not just for the coupon-codes and cash-coupon-codes
	 * but also for the user. in future, discount may need validation against products in cart or some more 
	 * attributes encapsulated in Order. So passing OrderDO itself 
	 * @param OrderDO $orderDO
	 */
	public function __construct(OrderDO $orderDO) {
		$this->orderDO = $orderDO;
		$discountDO = $orderDO->getDiscountDO();
		$couponCode = $discountDO->getCouponCode();
		$cashCouponCode = $discountDO->getCashCouponCode();
		if(!empty($couponCode) || !empty($cashCouponCode)) {
			$this->adapter = \CouponAdapter::getInstance();
			$this->validator = \CouponValidator::getInstance();
		}
	}
	
	public function validate() {
		$this->checkCashbackAndCouponValidity();
	}
	
	protected function checkCashbackAndCouponValidity() {
		$discountDO = $this->orderDO->getDiscountDO();
        // TODO -- TS to check is needed
		$this->validateCoupon($discountDO->getCouponCode());
        // TODO -- TS to check is needed
		$this->validateCoupon($discountDO->getCashCouponCode(),"CASH");
	}
	
	//TODO: this method should not return anything. Not whether it is valid or not.
	// it should just throw an exception, otherwise return nothing.
	//Check with Mitesh on this.
	protected function validateCoupon($couponCode,$type="DISCOUNT") {
		if(empty($couponCode))
			return;
	    $couponMessage = "";
	
	    // Find whether the coupon is still valid for the user.
	    $isValid = false;
		try {
	            $coupon = $this->adapter->fetchCoupon($couponCode);
	            try{
	            	$isValid = $this->validator->isCouponValidForUser($coupon, $this->orderDO->getLogin());
	            }
	            catch (\CouponLockedException $ex){
	            	//if coupon is blocked, it might have got blocked in the previous payment attempt, applying the same coupon
	            	//so try and unblock. Such coupons are identified by the order status: PP 
		            $unblocked = findAndCancelPPOrderBlockingCoupon($this->orderDO->getLogin(),$type,$couponCode);
		            
		            if($unblocked){
		            	$coupon = $this->adapter->fetchCoupon($couponCode);
		            	$isValid = $this->validator->isCouponValidForUser($coupon, $this->orderDO->getLogin());
		            }
		            else {
		            	error_log("Inside CouponLockedgeneral  448 ".$this->orderDO->getLogin()." coupon code is ".$coupon->getCouponCode);
		            	throw $ex;
		            }
	            }
	            catch (\CouponLockedForUserException $ex){
		            $unblocked = findAndCancelPPOrderBlockingCoupon($this->orderDO->getLogin(),$type,$couponCode);
		            if($unblocked){
		            	$coupon = $this->adapter->fetchCoupon($couponCode);
		            	$isValid = $this->validator->isCouponValidForUser($coupon, $this->orderDO->getLogin());
		            }
		            else {
		            	error_log("Inside CouponLockedException  448 ".$this->orderDO->getLogin()." coupon code is ".$couponCode);
		            	throw $ex;
		            }
	            }
	            if(!$isValid) {
	            	error_log("Inside coupon validation  448 ".$this->orderDO->getLogin()." coupon code is ".$couponCode);
	            	throw new \CouponCodeInvalidException("Coupon Code is invalid", 0, $couponCode);
	            }
		}
		catch (\CouponException $ex) {
			error_log("Inside couponexception in DiscountDOValidator  448 ".$login." coupon code is ".$couponCode);
		    $couponMessage = $ex->getMessage();
		    $couponMessage = "<font color=\"red\">$couponMessage</font>";
		    throw $ex;
		}
	}
}
