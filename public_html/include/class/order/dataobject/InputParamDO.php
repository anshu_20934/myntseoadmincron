<?php
namespace order\dataobject;
use base\dataobject\BaseDO;

/**
 * 
 * @author Arun Kumar K
 */
class InputParamDO extends BaseDO {

    private $param = array();

    public function __set($name, $value){
        $this->param[$name] = $value;
    }

    public function __get($name){
        if (array_key_exists($name, $this->param)) {
            return $this->param[$name];
        } else {
            return null;
        }
    }

}