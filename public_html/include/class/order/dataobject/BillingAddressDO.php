<?php
namespace order\dataobject;
use order\dataobject\AddressDO;
/**
 * BillingAddressDO is basically an AddressDO 
 * @author kundan
 */
class BillingAddressDO extends AddressDO {
	
	public function __construct($addressArr) {
		parent::__construct($addressArr);
	}
}