<?php
namespace order\dataobject\builder;

use order\dataobject\OrderDO;
use order\dataobject\AmountDO;
use base\builder\BaseBuilder;

class AmountDOBuilder implements BaseBuilder {
	
	private $orderDO;
	private $cart;
	public function __construct(OrderDO $orderDO,\MCart $cart) {
		$this->orderDO = $orderDO;
		$this->cart = $cart;
	}
	
	public function build() {
		$discountDO = $this->orderDO->getDiscountDO();
		$paymentDO = $this->orderDO->getPaymentDO();

		$mrp =  $this->cart->getMrp();
		$vat =  0.0;
		$vatEnabled = \FeatureGateKeyValuePairs::getBoolean("vat.enabled", false);
		if($vatEnabled) {
			$vat =  $this->cart->getAdditionalCharges();
		}

		$amount = $this->cart->getMrp();
		$totalAmount = $this->cart->getMrp()+$vat-$this->cart->getTotalProductItemDiscount()-$this->cart->getCartLevelDiscount()
		- $this->cart->getTotalCouponDiscount()-$this->cart->getCashDiscount() + $this->cart->getShippingCharge() + $this->cart->getGiftCharge() - $this->cart->getLoyaltyPointsCashUsed();
		$shippingCharge = $this->cart->getShippingCharge();
		
		
		
		$couponDiscount = $discountDO->getCouponDiscount();
		$cashRedeemed = $discountDO->getCashRedeemed();
		$productItemDiscount = $discountDO->getProductItemDiscount();
		$productBagDiscount = $discountDO->getProductBagDiscount();
		
		$lotaltyPointsUsedInCash = $this->cart->getLoyaltyPointsCashUsed();
		
		$subTotal = $amount;
		$amountWithProductDiscount = $amount - $productItemDiscount - $productBagDiscount;

		$totalBeforePGDisc = $subTotal - $couponDiscount - $cashRedeemed - $lotaltyPointsUsedInCash 
							- $productItemDiscount - $productBagDiscount + $shippingCharge + $this->cart->getGiftCharge()+$vat;
		$totalBeforePGDisc = ceilToZero($totalBeforePGDisc);
		
		$amountDO = new AmountDO($mrp, $vat, $amount, $totalAmount);
		
		$amountDO->setCouponDiscount($couponDiscount);
		$amountDO->setCashRedeemed($cashRedeemed);
		$amountDO->setProductItemDiscount($productItemDiscount);		
		$amountDO->setProductBagDiscount($productBagDiscount);
		$amountDO->setSubTotal($subTotal);
		
		$amountDO->setShippingCharge($shippingCharge);
		$amountDO->setCashOnDeliveryCharge($paymentDO->getCODCharge());
		$amountDO->setGiftCharge($this->orderDO->getGiftDO()->getCharge());
		$amountDO->setAmountWithProductDiscount($amountWithProductDiscount);
		$amountDO->setTotalBeforePGDiscount($totalBeforePGDisc);
		
		return $amountDO;
	}
	
	/**
	 * To Set the Total Amount: this includes Payment Gateway Discount, but excludes GiftCharge, ShippingCharge and CODCharge
	 * caution: this should be called only after Payment Gateway Discount has been set
	 * @param AmountDO $amountDO
	 */
	public static function buildTotal(AmountDO $amountDO) {
		$amountDO->setTotal($amountDO->getTotalBeforePGDiscount() - $amountDO->getPaymentGatewayDiscount());
	}
	
	/**
	 * To Set Final Amount to Charge from the Customer: this is what is sent to Payment Gateway
	 * Caution: this should be called only after Total is set on the AmountDO (e.g. by calling buildTotal on the AmountDO) 
	 * @param AmountDO $amountDO
	 */
	public static function buildFinalAmountToCharge(AmountDO $amountDO) {
		// used to pass on to Payment Gateway
		$finalAmountToCharge = $amountDO->getTotal() + $amountDO->getCashOnDeliveryCharge() + $amountDO->getEMICharge();
		$finalAmountToCharge = number_format($finalAmountToCharge, 2, ".", '');		
		$finalAmountToCharge = ceilToZero($finalAmountToCharge);		
		if($finalAmountToCharge<1) { //less than 1re payment is not accepted by banks to making the amount to zero in case final amount falls below 1re.
			$finalAmountToCharge=0;
		}				  
		$amountDO->setFinalAmountToCharge($finalAmountToCharge);
	}
}
