<?php
namespace order\dataobject\builder;
use order\dataobject\ShippingDO;
use order\dataobject\InputParamDO;
use order\dao\AddressDAO;
use order\dataobject\OrderDO;
use base\builder\BaseBuilder;

class ShippingDOBuilder implements BaseBuilder {
	
	private $orderDO, $addressDAO;
	public function __construct(OrderDO $orderDO) {
		$this->orderDO = $orderDO;
		$this->addressDAO = new AddressDAO();
	}
	
	public function build() {
        $inputParamDO = $this->orderDO->getInputParamDO();
		$addressId = $inputParamDO->addressId;
		$login = $this->orderDO->getLogin();
		$addressObj;
		if(!empty($addressId)) { //address was explicitly passed: usually from request parameter
			$addressObj = $this->addressDAO->getAddress($addressId);
		}
		else {
			//fetch default address for this login
			$addressObj = $this->addressDAO->getDefaultAddress($login);
			if($addressObj == null) {
				//if there's no default address for this login, fetch the latest address
				$addressObj = $this->addressDAO->getLatestAddress($login);
		    }     
		}
        
		$logisticId = $inputParamDO->logisticId;
		$shippingPreferences = $inputParamDO->shippingPreference;
                $shippingMethod = $inputParamDO->shippingMethod;
		return ($addressObj == null) ? null : (new ShippingDO($addressObj, $logisticId, $shippingPreferences, $shippingMethod));
	}
}
