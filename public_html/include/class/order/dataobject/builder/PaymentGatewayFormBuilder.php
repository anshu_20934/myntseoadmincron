<?php
namespace order\dataobject\builder;

use enums\cart\CartContext;

use base\builder\BaseBuilder;
use order\dataobject\AddressDO;
use order\dataobject\BillingAddressDO;
use order\dataobject\OrderDO;
use order\exception\NoSuitableGatewayException;
use revenue\payments\PaymentGatewayFactory;

use base\dataobject\IBaseOrderDO;

class PaymentGatewayFormBuilder implements BaseBuilder {
	
	private $orderDO;
	
	public function __construct(IBaseOrderDO $orderDO) {
		$this->orderDO = $orderDO;
	}
	
	public function build() {

		global $_SERVER, $XCART_SESSION_NAME, $XCARTSESSID, $configMode,$https_location,$login;
		
		$ord = $this->orderDO;
		
		$cartDO = $ord->getCartDO();
		$cart = $cartDO->getCart();
		$cartContext = $cart->getContext(); 
		
		$orderid = $ord->getOrderId();
		
		$shipAddress = $this->buildShippingAddressForPayment();
		$billAddress = $this->buildBillingAddressForPayment(); 
		
	    $Amount = $ord->getAmountDO()->getFinalAmountToCharge();// $final_total_amount;//$_POST['Amount'];//your script should substitute the amount in the quotes provided here
	    $userType = $ord->getUserType();        
	    $cookieContent = getUnserializedCookieContent('utm_track_1');
	    
	    $extraParams['cardDetails'] = $ord->getPaymentDO()->getCardDO();
		$extraParams['accept_http'] = $_SERVER["HTTP_ACCEPT"];
		$extraParams['http_user_agent'] = $_SERVER["HTTP_USER_AGENT"];
		$extraParams['billAddress'] = $billAddress;
	    $extraParams['shipAddress'] = $shipAddress;
		$extraParams['session_name'] = $XCART_SESSION_NAME;
	    $extraParams['session_id'] = $XCARTSESSID;    
	    $extraParams['utm_source'] = $cookieContent['utm_source'];
	    $extraParams['utm_medium'] = $cookieContent['utm_medium'];
	    $extraParams['saleDetail'] = 'Myntra Designs';
	    $extraParams['configMode'] = $configMode;
	    if($cartContext == CartContext::GiftCardContext){
	    	$orderid="G_".$orderid;
	    }
		
	    $notWorkingGateways = array();

	    $gateway = $this->orderDO->getPaymentDO()->getGateway();
	    $form = $gateway->getFormToSubmit($orderid, $Amount, $extraParams);
	    
	    while(empty($form)) {
			
	    	$notWorkingGateways[] = $gateway->getName();
	    	
			$gateway = PaymentGatewayFactory::getInstance()->getGateway($ord->getPaymentDO(), $notWorkingGateways);
			
			if($gateway == null) {
				throw new NoSuitableGatewayException("No suitable gateway found", $ord->getPaymentDO());
			}	    
			$gateway->setHttpsLocation($https_location);
			$form = $gateway->getFormToSubmit($orderid, $Amount, $extraParams);
	    }
	    
	    $gateway->insertLogData($orderid,$Amount,$login);
	    
	    return $form;
	}
	
	private function buildShippingAddressForPayment() {
		if($this->orderDO->getShippingDO() != NULL){
			$shipAdrs = $this->orderDO->getShippingDO()->getAddressDO();
			return $this->sanitizeAddressForPayment($shipAdrs);
		}
		else{
			return NULL;
		}
	}
	
	/**
	 * 
	 * If the fields are missing in billing address, copy them from Shipping Address
	 * And then return an AddressDO object corresponding to billing address
	 */
	private function buildBillingAddressForPayment() {
		if($this->orderDO->getShippingDO() != NULL){
			$shipAdrs = $this->orderDO->getShippingDO()->getAddressDO();
			$billAdrs = $this->orderDO->getBillingAddressDO();
			if($billAdrs == null) {
				$billAdrs = new BillingAddressDO();
			}
			
			$name = $billAdrs->getName();
			if(empty($name) || $name == "Name") {
				$billAdrs->setName($shipAdrs->getName());
			}
			$bAdrs = $billAdrs->getAddress();
			if(empty($bAdrs) || $bAdrs == "Enter your billing address here") {
				$billAdrs->setAddress($shipAdrs->getAddress());
			}
			$bCity = $billAdrs->getCity();
			if(empty($bCity) || $bCity == "City") {
				$billAdrs->setCity($shipAdrs->getCity());
			}
			$bState = $billAdrs->getState();
			if(empty($bState) || $bState == "State") {
				$billAdrs->setState($shipAdrs->getState());
			}
			$bPinCode = $billAdrs->getZipcode();
			if(empty($bPinCode) || $bPinCode == "PinCode") {
				$billAdrs->setZipcode($shipAdrs->getZipcode());
			}
			$bCountry = $billAdrs->getCountry();
			if(empty($bCountry)) {
				$billAdrs->setCountry($shipAdrs->getCountry());
			}
			$billAdrs->setEmail(fsanitize_email($billAdrs->getEmail()));
			
			return $this->sanitizeAddressForPayment($billAdrs);
		}
		else{
			return NULL;
		}
		/*
		//CCAvenue identifies full country name, we should send country name, not country code. Eg. India for IN
		//$returnCountry = func_get_countries();
		$billing_cust_country_code = $billing_cust_country;
		//$billing_cust_country = $returnCountry[$billing_cust_country];
		//$shipping_cust_country =$returnCountry[$delivery_cust_country];
		*/
		
	}

	/**
	 * Sanitize each address field for passing to payment gateway
	 * @param AddressDO $adrs
	 */
	public static function sanitizeAddressForPayment(AddressDO $adrs) {
		$pmtAdrs = new AddressDO();
		$pmtAdrs->setName(self::sanitizeEachField($adrs->getName(), false));
		$address = self::sanitizeEachField($adrs->getAddress(), false);
		$pmtAdrs->setAddress(htmlspecialchars($address,ENT_QUOTES));
		$pmtAdrs->setCountry(self::sanitizeEachField($adrs->getCountry()));
		$pmtAdrs->setCity(self::sanitizeEachField($adrs->getCity()));
		$pmtAdrs->setState(self::sanitizeEachField($adrs->getState()));
		$pmtAdrs->setMobile(self::sanitizeEachField($adrs->getMobile()));
                $pmtAdrs->setPhone(self::sanitizeEachField($adrs->getPhone()));
		$pmtAdrs->setZipcode(self::sanitizeEachField($adrs->getZipcode()));
		$pmtAdrs->setEmail(self::sanitizeEachField($adrs->getEmail(),true, false));
		return $pmtAdrs;
	}
	
	public static function sanitizeEachField($str, $stripTags = true, $allowAlphaNumericOnly = true) {
		if($stripTags) {
			$str = strip_tags($str);
		}
		if($allowAlphaNumericOnly) {
			$str = sanitize_paranoid_string($str);
		}
		return $str;
	}
	
}