<?php
namespace order\dataobject\builder;

use base\builder\BaseBuilder;
use order\dataobject\AddressDO;
use order\dataobject\CartDO;
use order\dataobject\DiscountDO;
use order\dataobject\OrderItemDO;
use order\dataobject\OrderDO;
use order\dataobject\ShippingDO;

require_once \HostConfig::$documentRoot . "/modules/apiclient/SkuApiClient.php";

class OrderItemsDOBuilder implements BaseBuilder {
	
	private $orderDO;
	private $orderItems;
	
	public function __construct(OrderDO $orderDO) {
		$this->orderDO = $orderDO;
		$this->orderItems = array();
	}

	/**
	 * Returns an array of OrderItemDO
	 * @see include/class/base/builder/base\builder.BaseBuilder::build()
	 */
	public function build() {
		$orderId = $this->orderDO->getOrderId();
		$productsInCart = $this->orderDO->getcartDO()->getProductsInCart();
		$qtyInOrder = 0;
		
		foreach($productsInCart as $key => $productDetail) {
			$orderItem = $this->buildEachOrderItem($orderId, $productDetail);
			$qtyInOrder += $orderItem->get("quantity");
			$this->orderItems[] = $orderItem;
		}
		$this->orderDO->setTotalQuantity($qtyInOrder);
		return $this->orderItems;
	}
	
	/**
	 * 
	 * @param $productDetail
	 */
	public function buildEachOrderItem($orderId, array $productDetail) {
		$item = new OrderItemDO($orderId, $productDetail);
		//call other build methods on eachItem
		$this->buildQuantityBreakup($item);
		$this->buildShippingInfo($item);
		$this->buildAmounts($item);
		$this->buildJustInTimeSourcing($item);
		return $item;
	}
	
	private function buildFromOrder(OrderItemDO $item) {
		$discDO = $this->orderDO->getDiscountDO();
		$item->set("couponCode", $discDO->getCouponCode());
		$item->set("cashCouponCode", $discDO->getCashCouponCode());
	}
	
	/**
	 * This will set a property: quantityBreakup in productDetail in the item
	 * @param $item
	 */
	private function buildQuantityBreakup(OrderItemDO $item) {
		$sizeNames = $item->get("sizenames");
		$sizeQuantities = $item->get("sizequantities");
		
		$qcount = count($sizeNames);
		$quantityBreakup = "";
		for($i=0;$i<$qcount;$i++) {
			$sizeStr = $sizeNames[$i]. ":" . $sizeQuantities[$i];
			if($i!=$qcount-1)
				$quantityBreakup .= $sizeStr . ",";
			else
				$quantityBreakup .= $sizeStr;
		}
		$item->set("quantityBreakup", $quantityBreakup);
	}
	
	/**
	 * This will set 2 fields: shippingTaxAmount, shippingAmount
	 * @param OrderItemDO $item
	 */
	private function buildShippingInfo(OrderItemDO $item) {
		
		$s = $this->orderDO->getShippingDO();
		$totalShippingCharge = $s->getShippingRate();
		//Currently, all orders have free shipping, so all shipping charges become 0
		if(empty($totalShippingCharge)) {
			$shippingTaxAmount = 0;
	        $shippingAmount = 0;
		}
		else {
			$a = $s->getAddressDO();
			$c = $this->orderDO->getCartDO();
			
			$shippingRateArray = func_shipping_rate_re(
									$a->getCountry(),$a->getState(), $a->getCity(), 
									array($item->getAllDetails()), null, null, 
									$s->getLogisticId(), $a->getZipcode(), $c->getCartAmount()
								 );
			$shippingRate = $shippingRateArray['shiprate'];
	
			global $sql_tbl;
			$productTypeId = $item->get("producTypeId");
			$vatRate = $item->get("vatRate");
			
			$consignmentCapacity = func_query_first_cell("SELECT consignment_capacity FROM $sql_tbl[shipping_prod_type] WHERE product_type_id='".$productTypeId."'");
			$productTypeQuantityArray = $c->getProductTypeQuantityArray();
			
			$totalunits = ceil($productTypeQuantityArray[$productTypeId]['qty']/$consignmentCapacity);
			$shippingTotalCost = ($totalunits *  $shippingRate)/$productTypeQuantityArray[$productTypeId]['stylecount'];
		    
			$shippingTaxAmount = ($shippingTotalCost * ($vatRate/100));
		    $shippingAmount = $shippingTotalCost - $shippingTaxAmount;
		}
		$item->set("shippingAmount", $shippingAmount);					 
		$item->set("shippingTaxAmount", $shippingTaxAmount);
	}
	
	private function buildAmounts(OrderItemDO $item) {
		$a = $this->orderDO->getAmountDO();
		$pgDiscount = $a->getPaymentGatewayDiscount();
		$amountWithProductDiscount = $a->getAmountWithProductDiscount();
		
		$discount = 0;
		
		$discountAmount = $item->get("discountAmount");
		if(empty($discountAmount)) {
			$systemDiscount = $item->get("systemDiscount");
			if(!empty($systemDiscount)) {
				$discount = $systemDiscount;
			}
		} else {
			$discount = $discountAmount;
		}
		
		$vatAmount = $item->get("unitVat") * $item->get("quantity");
		$vatEnabled = \FeatureGateKeyValuePairs::getBoolean("vat.enabled", false);
		if($vatEnabled) {
			$vatAmount = $item->get("totalAdditionalCharges");
		} else {
			$vatAmount = 0.0;
		}
		$vatAmount = number_format($vatAmount, 2, ".", '');
	
		//discountAmount => ProductItemDiscount
		//cartDiscount => ProductBagDiscount	
		$totalAmount = ($item->get("productPrice") * $item->get("quantity")) - $item->get("discountAmount") - $item->get("cartDiscount");
		$totalAmount = number_format($totalAmount, 2, ".", '');
		
		$pgDiscountProduct = ($totalAmount*$pgDiscount)/$amountWithProductDiscount;
		$pgDiscountProduct = number_format($pgDiscountProduct, 2, ".", ''); 
		
		$item->set("discount", $discount);
		$item->set("vatAmount", $vatAmount);
		$item->set("totalAmount", $totalAmount);
		$item->set("pgDiscount", $pgDiscountProduct);
				
	}
	
	private function buildJustInTimeSourcing(OrderItemDO $item) {
		$item->set("isJITSourced", 0);
		/* $jitSourcingEnabled = \FeatureGateKeyValuePairs::getBoolean("jit.sourcing.enabled", false);
		$isJITPurchase = false;
		if($jitSourcingEnabled) {
			$skuid = $item->get("skuid");
			$sku_details = \SkuApiClient::getSkuDetailsInvCountMap($skuid);
			if($sku_details[$skuid]['availableItems'] <= 0 && $sku_details[$skuid]['jitSourced'] == true){
				$isJITPurchase = true;
			}
		} 
		$item->set("isJITSourced", $isJITPurchase);		*/
	}
}
