<?php
namespace order\dataobject\builder;

use enums\cart\CartContext;
use order\dataobject\OrderDO;
use order\dataobject\TempOrderDO;
use order\dataobject\InputParamDO;
use base\builder\BaseBuilder;

class TempOrderDOBuilder implements BaseBuilder {
	
	private $orderDO;
	
	public function __construct($orderDO) {
		$this->orderDO = $orderDO;
	}
	
	public function build() {		
		$tempOrderDO = new TempOrderDO(); 
		$tempOrderDO->setOrderCRC($this->getOrderCRC());        
		$tempOrderDO->setSessionId($this->orderDO->getInputParamDO()->sessionId);
		$tempOrderDO->setChecksumKey(uniqid());
		return $tempOrderDO;
	}
	
	public function getParamsStr($params) {
		$text = "";
		foreach($params AS $key=>$value) {
			switch ($key) {
				case 'totalquantity': $fieldvalue = sanitize_int(strip_tags($value)); break;
				//Kundan: Warning: thisTotalAmount, thisAmount are never set in code, so they're actually useless
				case 'shippingrate': $fieldvalue = sanitize_float(strip_tags($thisShippingRate)); break;
				case 'totalamount': $fieldvalue = sanitize_float(strip_tags($thisTotalAmount)); break;
				
				case 'Amount': 
				case 'hideref':
				case 'hidevat':
				case 'hidetotamount': $fieldvalue = sanitize_float(strip_tags($value)); break;
				
				case 's_address': 
				case 'b_address': $fieldvalue = mysql_real_escape_string(str_ireplace("\r\n"," ",$value)); break;
				
				case 's_option': 
				case 's_email':
				case 'b_email':	$fieldvalue = $value; break;
				default: $fieldvalue = sanitize_paranoid_string(strip_tags($value));
				break;
			}
			if($text == "") {
				$text .= "$key=>".$fieldvalue;
			} else {
				$text .= "||$key=>".$fieldvalue;
			}
		}		
		$ship2diff = $params['billtoship'] ? 'Y' : 'N';
		$text = $text."||ship2diff=>".$ship2diff;
		return $text;
	}
	
	/**
	 * Kundan: create a hash out of these things:
	 * Cart, Total, CashCouponCode, CouponCode, Shipping Address 
	 * $address, $cashCouponCode, $couponCode, $productsInCart, $myCart, $login
	 */		
	public function getOrderCRC() {
		$cartDO = $this->orderDO->getCartDO();
		$cart = $cartDO->getCart();
		if($cart->getContext() == CartContext::GiftCardContext){
			$discountDO = $this->orderDO->getDiscountDO();
			$shippingDO = $this->orderDO->getShippingDO();
			if($shippingDO != NULL){
				$addressDO = $shippingDO->getAddressDO();
			}
			else{
				$addressDO = NULL;
			}
		
			if($addressDO != NULL){
				$arrForCRC = array($cartDO->getProductsInCart(), $this->orderDO->getLogin(), 
						   $discountDO->getCouponCode(), $discountDO->getCashCouponCode(), 
						   $addressDO->getAddressArr()
					 );
			}
			else{
				$arrForCRC = array($cartDO->getProductsInCart(), $this->orderDO->getLogin(), 
						   $discountDO->getCouponCode(), $discountDO->getCashCouponCode(), 
						   null
					 );
			}
			
			return crc32(var_export($arrForCRC, true));
		}
		else{
			$discountDO = $this->orderDO->getDiscountDO();
			$shippingDO = $this->orderDO->getShippingDO();
			$addressDO = $shippingDO->getAddressDO();
		
			$arrForCRC = array($cartDO->getProductsInCart(), $this->orderDO->getLogin(), 
						   $discountDO->getCouponCode(), $discountDO->getCashCouponCode(), 
						   $addressDO->getAddressArr()
					 );
			return crc32(var_export($arrForCRC, true));
		}
	}
}