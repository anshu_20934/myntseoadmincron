<?php
namespace order\dataobject\builder;
use order\dataobject\BillingAddressDO;

use order\dataobject\AddressDO;
use order\dataobject\InputParamDO;
use order\dataobject\ShippingDO;
use order\dao\AddressDAO;
use order\dataobject\OrderDO;
use base\builder\BaseBuilder;

class BillingAddressDOBuilder implements BaseBuilder {
	
	private $orderDO;
	
	public function __construct(OrderDO $orderDO) {
		$this->orderDO = $orderDO;
	}
	
	public function build() {
        $inputParamDO = $this->orderDO->getInputParamDO();
		$badrs = new BillingAddressDO();

		$badrs->setName($inputParamDO->billingFirstName);

		$origAdrs = $inputParamDO->billingAddress;
		$address = mysql_real_escape_string(str_replace("\r\n", " ", $origAdrs));
		$badrs->setAddress($address);

		$badrs->setCity($inputParamDO->billingCity);

		$badrs->setState($inputParamDO->billingState);

		$badrs->setCountry($inputParamDO->billingCountry);

		$badrs->setZipcode($inputParamDO->billingZipcode);
		
		if($this->orderDO->getShippingDO() != NULL){
			//Billing mobile is set to shipping mobile as we don't ask this in the form
                        $mobile = $this->orderDO->getShippingDO()->getAddressDO()->getMobile();
                        $phone  = $this->orderDO->getShippingDO()->getAddressDO()->getPhone();
                        if($mobile != null && $mobile != ''){
                            $badrs->setMobile($mobile);
                        }else {
                            $badrs->setMobile($phone);
                        }
		}
		
		$badrs->setEmail($this->orderDO->getLogin());
		return $badrs;
	}
}