<?php
namespace order\dataobject\builder;
use order\dataobject\InputParamDO;
use base\builder\BaseBuilder;
use enums\cart\CartContext;

class OrderInputParamDOBuilder implements BaseBuilder {

    private $context;

    public function __construct($context) {        
		$this->context = $context;
	}

    public function build(){
        
        global $XCARTSESSID;

        $inputParamDO = new InputParamDO();

        if($this->context == 'default' || $this->context == CartContext::OneClickContext){
            
            // these call the magic function(__set()) in InputParamDO to set corresponding values
            $inputParamDO->addressId = \RequestVars::getVar("address");
            $inputParamDO->logisticId= \RequestVars::getVar("s_option","","string");
            $inputParamDO->shippingPreference = \RequestVars::getVar("shipment_preferences","","string");
            $inputParamDO->userType = \XSessionVars::get('login_type','');
            $inputParamDO->billingFirstName = \RequestVars::getVar("b_firstname","","string");
            $inputParamDO->billingAddress = \RequestVars::getVar("b_address");
            $inputParamDO->billingCity = \RequestVars::getVar("b_city","","string");
            $inputParamDO->billingState = \RequestVars::getVar("b_state","","string") ;
            $inputParamDO->billingCountry = \RequestVars::getVar("b_country","","string");
            $inputParamDO->billingZipcode = \RequestVars::getVar("b_zipcode","","string");
            $inputParamDO->sessionId = $XCARTSESSID;
            $inputParamDO->couponPercent = \XSessionVars::getNumber('couponpercent'); //YOGESH is it being used?? ;
            $inputParamDO->paymentMethod = \RequestVars::getVar("pm");
            $inputParamDO->cardNumber = \RequestVars::getVar("card_number");
            $inputParamDO->cardBillName = \RequestVars::getVar("bill_name");
            $inputParamDO->cardMonth = \RequestVars::getVar("card_month");
            $inputParamDO->cardYear = \RequestVars::getVar("card_year");
            $inputParamDO->cvvCode = \RequestVars::getVar("cvv_code");
            $inputParamDO->bankId = \RequestVars::getVar("netBankingCards");
            $inputParamDO->EMIBank = \RequestVars::getVar("emi_bank");

        } else if($this->context == 'telesales'){

            // these call the magic function(__set()) in InputParamDO to set corresponding values
            $inputParamDO->addressId = \RequestVars::getVar("addressid");;
            $inputParamDO->logisticId = \RequestVars::getVar("s_option","","string");
            $inputParamDO->shippingPreference = "shipped_together";
            $inputParamDO->paymentMethod = \RequestVars::getVar("pm");
            $inputParamDO->userType = "C";

            $inputParamDO->sessionId = $XCARTSESSID;
            $inputParamDO->couponPercent = \XSessionVars::getNumber('couponpercent'); //YOGESH is it being used?? ;
        } else if($this->context == 'giftcard'){
            
            // these call the magic function(__set()) in InputParamDO to set corresponding values
            $inputParamDO->addressId = \RequestVars::getVar("address");
            $inputParamDO->logisticId= \RequestVars::getVar("s_option","","string");
            $inputParamDO->shippingPreference = \RequestVars::getVar("shipment_preferences","","string");
            $inputParamDO->userType = \XSessionVars::get('login_type','');
            $inputParamDO->billingFirstName = \RequestVars::getVar("b_firstname","","string");
            $inputParamDO->billingAddress = \RequestVars::getVar("b_address");
            $inputParamDO->billingCity = \RequestVars::getVar("b_city","","string");
            $inputParamDO->billingState = \RequestVars::getVar("b_state","","string") ;
            $inputParamDO->billingCountry = \RequestVars::getVar("b_country","","string");
            $inputParamDO->billingZipcode = \RequestVars::getVar("b_zipcode","","string");
            $inputParamDO->sessionId = $XCARTSESSID;
            $inputParamDO->couponPercent = \XSessionVars::getNumber('couponpercent'); //YOGESH is it being used?? ;
            $inputParamDO->paymentMethod = \RequestVars::getVar("pm");
            $inputParamDO->cardNumber = \RequestVars::getVar("card_number");
            $inputParamDO->cardBillName = \RequestVars::getVar("bill_name");
            $inputParamDO->cardMonth = \RequestVars::getVar("card_month");
            $inputParamDO->cardYear = \RequestVars::getVar("card_year");
            $inputParamDO->cvvCode = \RequestVars::getVar("cvv_code");
            $inputParamDO->bankId = \RequestVars::getVar("netBankingCards");
            $inputParamDO->EMIBank = \RequestVars::getVar("emi_bank");

        }

        return $inputParamDO;
    }
}