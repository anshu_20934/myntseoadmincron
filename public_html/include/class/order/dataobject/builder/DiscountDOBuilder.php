<?php
namespace order\dataobject\builder;
use order\dataobject\DiscountDO;
use order\dataobject\InputParamDO;
use order\dataobject\OrderDO;

use base\builder\BaseBuilder;

class DiscountDOBuilder implements BaseBuilder {
	
	private $cart, $orderDO;
	
	public function __construct(\MCart $cart, $orderDO){
		$this->cart=$cart;
        $this->orderDO=$orderDO;
	}
	
	public function build() {

		$discountDO = new DiscountDO();
		
		//Set Coupon Discount and related fields
		$couponDiscount =  $this->cart->getDiscount();
		//--If no coupon discount is set, then set coupon code and percent to blank
		if(isset($couponDiscount) && $couponDiscount>0) {
			$couponCodeArray=array_keys($this->cart->getAllCouponCodes(\MCart::$__COUPON_TYPE_DEFAULT));
			$couponCode = $couponCodeArray[0];            
			$couponPercent = $this->orderDO->getInputParamDO()->couponPercent;
		} else {
			$couponDiscount  = 0;
			$couponCode = "";
			$couponPercent = 0;
		}
		//Set these in the DO
		$discountDO->setCouponCode($couponCode);
		$discountDO->setCouponPercent($couponPercent);
		$discountDO->setCouponDiscount($couponDiscount);
		
		//Set Cash Coupon Code and related fields
		$cashRedeemed =  $this->cart->getCashDiscount();
		$cashCouponCodeArray=array_keys($this->cart->getAllCouponCodes(\MCart::$__COUPON_TYPE_CASHBACK));
		$cashCouponCode = ($cashRedeemed > 0) ?$cashCouponCodeArray[0] : "";
		
		//MyntCash (new version of cashback) fields		
		$totalMyntCashUsage = $this->cart->getTotalMyntCashUsage();
				
		$discountDO->setTotalMyntCashUsage($totalMyntCashUsage);
		
		$cashRedeemed = $totalMyntCashUsage;
		
		//Set these in the DO
		$discountDO->setCashCouponCode($cashCouponCode);
		$discountDO->setCashRedeemed($cashRedeemed);
		
		//set loyalty points fields in DO
		$discountDO->setLoyaltyPointsUsed($this->cart->getLoyaltyPointsUsed());
		$discountDO->setLoyaltyPointsAwarded($this->cart->getLoyaltyPointsAwarded());
		$discountDO->setLoyaltyPointsConversionFactor($this->cart->getLoyaltyPointsConversionFactor());
		
		//Set Product Discount
		$discountDO->setProductItemDiscount($this->cart->getTotalProductItemDiscount());
		$discountDO->setProductBagDiscount($this->cart->getCartLevelDiscount());
		return $discountDO;
	}
	
}