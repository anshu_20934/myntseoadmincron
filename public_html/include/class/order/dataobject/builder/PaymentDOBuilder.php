<?php
namespace order\dataobject\builder;

use enums\revenue\payments\PaymentType;

use order\dataobject\OrderDO;
use order\dataobject\CardDO;
use order\dataobject\PaymentDO;
use order\dataobject\InputParamDO;
use base\builder\BaseBuilder;

class  PaymentDOBuilder implements BaseBuilder {

    private $orderDO;

	public function __construct($orderDO) {
		$this->orderDO = $orderDO;
	}
	
	public function build() {
        $inputParamDO = $this->orderDO->getInputParamDO();
		$paymentMethod =  $inputParamDO->paymentMethod;
		$cardDO = null; $bankId = null;
		
		if($paymentMethod === PaymentType::Credit || $paymentMethod === PaymentType::Debit || $paymentMethod === PaymentType::EMI) {
			$cardDO = new CardDO(
                            $inputParamDO->cardNumber,$inputParamDO->cardBillName,
                            $inputParamDO->cardMonth,$inputParamDO->cardYear,
                            $inputParamDO->cvvCode
						);	
		}
		elseif ($paymentMethod === PaymentType::NetBanking) {
			$bankId = $inputParamDO->bankId;
		}
		$paymentDO = new PaymentDO($paymentMethod, $cardDO, $bankId);
		
		if($paymentMethod === PaymentType::EMI) {
			$paymentDO->setEMIBank($inputParamDO->EMIBank);
		}
		
		
		$this->buildPaymentOption($paymentDO);
		$this->buildCashOnDeliveryInfo($paymentDO);
		return $paymentDO;
	}
	
	
	private static function buildPaymentOption(PaymentDO $paymentDO) {
		
		switch ($paymentDO->getPaymentMethod()) {
			case 'cod':
			    // Pravin - COD Feature. Setting payment option to 'cod'. In xcart_orders, such orders will store payment_method as 'cod'
			    $paymentOption = 'cod';
				break;
			case 'NP': 
				$paymentOption = 'NP';
				break;
			case 'ivr':
				$paymentOption = 'ivr';
				break;
			default:
				$paymentOption = 'on';				
		}
		$paymentDO->setPaymentOption($paymentOption);
	}
	
	private static function buildCashOnDeliveryInfo(PaymentDO $paymentDO) {
		$codCharge = 0;
		if($paymentDO->isCODOrder()) {
			$codChargeFG = \FeatureGateKeyValuePairs::getInteger('codCharge');
			$codCharge = ($codChargeFG > 0) ? $codChargeFG : 0;
			$paymentDO->setCODPayStatus("pending");
		}
		$paymentDO->setCODCharge($codCharge);
	}

}