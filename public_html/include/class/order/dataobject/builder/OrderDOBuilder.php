<?php

namespace order\dataobject\builder;

use order\dataobject\builder\AmountDOBuilder;
use order\dataobject\builder\BillingAddressDOBuilder;
use order\dataobject\builder\DiscountDOBuilder;
use order\dataobject\builder\PaymentDOBuilder;
use order\dataobject\builder\ShippingDOBuilder;
use order\dataobject\builder\TempOrderDOBuilder;
use order\dao\OrderDAO;

use order\dataobject\AddressDO;
use order\dataobject\BillingAddressDO;
use order\dataobject\CartDO;
use order\dataobject\DiscountDO;
use order\dataobject\GiftDO;
use order\dataobject\OrderDO;
use order\dataobject\PaymentDO;
use order\dataobject\ShippingDO;
use order\dataobject\InputParamDO;

use base\exception\BaseRedirectableException;

require_once \HostConfig::$documentRoot."/include/class/mcart/class.MCart.php";
include_once "$xcart_dir/include/func/func.loginhelper.php";
class OrderDOBuilder {

	private $orderDAO;
	
	public function __construct() {
		$this->orderDAO = new OrderDAO();
	}
	
	public function getOrderDAO() {
		return $this->orderDAO;
	}
	
	/**
	 * This creates an instance of OrderDO
	 * which in turn is composed of multiple other DOs 
	 * @return: an Instance of OrderDO
	 */
	public function build(\MCart $cart) {
		try {
			$orderDO = new OrderDO($cart);
			$this->createAndAssignDOs($orderDO, $cart);
			return $orderDO;	
		} catch (BaseRedirectableException $ex) {
			throw $ex;
		} catch (RedirectableValidationException $rex) {
			throw $rex;
		}
	}
	
	public function buildOrderActions(\MCart $cart, $addressId, $clientCode, $paymentMethod, $b_firstname, $b_address, $b_city, $b_state, $b_country, $b_zipcode, $sessionId) {
		try {
			$orderDO = new OrderDO($cart);
	
	        // This should be the first DO to be created
	        // input parameters DO(all order related session and request params DO)
	        $inputParamDO = $this->getInputParamDO($orderDO->getContext());
	        // Build inputParamDO with extra parameters
	        // these call the magic function(__set()) in InputParamDO to set corresponding values
	        $inputParamDO->addressId = $addressId;
	        $inputParamDO->userType = 'C';
	        $inputParamDO->billingFirstName = $b_firstname;
	        $inputParamDO->billingAddress = $b_address;
	        $inputParamDO->billingCity = $b_city;
	        $inputParamDO->billingState = $b_state;
	        $inputParamDO->billingCountry = $b_country;
	        $inputParamDO->billingZipcode = $b_zipcode;
	        $inputParamDO->sessionId = $sessionId;
	        $inputParamDO->paymentMethod = $paymentMethod;
	        $shippingMethodCheck = $cart->getShippingMethod();
	        if (!empty($shippingMethodCheck)) {
	        	$shipping_method = $cart->getShippingMethod();
                    if ($shipping_method === "EXPRESS") {
                        $shipping_method = "XPRESS";
                    }
                } else {
                    $shipping_method = "NORMAL";
                }
            $inputParamDO->shippingMethod = $shipping_method;

            if (isGuestCheckout()) {
            	$orderDO->setOrdertype('gc');
            }else if( $cart->getContext() == 'oneclick'){
            	$orderDO->setOrdertype('ec');
        	}
            
	        $orderDO->setInputParamDO($inputParamDO);        
	
			$orderDO->setSessionId($inputParamDO->sessionId);
	
			//Cart DO
			$orderDO->setCartDO($this->getCartDO($cart));
			
			//Shipping Address DO
			$shippingDO = $this->getShippingDO($orderDO);
                        if($shippingDO != NULL){
                            $orderDO->setShippingDO($shippingDO);
                        }
			
			//Discount
			$orderDO->setDiscountDO($this->getDiscountDO($cart, $orderDO));
	
			//Temp Order
			$orderDO->setTempOrderDO($this->getTempOrderDO($orderDO));
			
			//Payment
			$paymentDO = $this->getPaymentDO($orderDO);
			$orderDO->setPaymentDO($paymentDO);
			// Default Channel is Web. In case of IVR payment, the channel is IVR
		 	// In case of Cash on Delivery option, the initial order status should be set to PV which implies "Pending Verification"
		 	//otherwise, default order status is PP: Pre-processed        
			$channel = ($paymentDO->getPaymentMethod() === 'ivr') ? 'ivr' : 'web';
                        if ($clientCode === 'mobile'||$clientCode === 'moneclick')
                            $channel = 'mobile';
                        if ($clientCode === 'telesalen')
                            $channel = 'telesalen';
			$orderDO->setChannel($channel);
			
			$status = ($paymentDO->getPaymentMethod() === 'cod') ? 'PV' : 'PP';
			$orderDO->setStatus($status);
			
			//Date/Timestamp
			$orderDO->setDate(time());
			$orderDO->setRequestServer(\HostConfig::$serverName);
	
			//Gift
			$orderDO->setGiftDO($this->getGiftDO($cart));
			
			//AmountDO
			$orderDO->setAmountDO($this->getAmountDO($orderDO,$cart));
			
			//Billing Address
			$orderDO->setBillingAddressDO($this->getBillingAddressDO($orderDO));
			
			//Set User-Type
			$orderDO->setUserType($inputParamDO->userType);
                        
                        $customerInfo = $this->orderDAO->getCustomerInfo($orderDO->getLogin());
                        if($customerInfo['mobile'] === '' || $customerInfo['mobile'] === null ){
                            $mobile = $orderDO->getBillingAddressDO()->getMobile();
                            $phone = $orderDO->getBillingAddressDO()->getPhone();
                            if($mobile != '' && $mobile != null){
                                $orderDO->setCustomerMobile($mobile);
                            }
                            else if ($phone != '' && $phone != null){
                                $orderDO->setCustomerMobile($phone);
                            }
                            else{
                                $shMobile = $orderDO->getShippingDO()->getAddressDO()->getMobile();
                                $shPhone  = $orderDO->getShippingDO()->getAddressDO()->getPhone();
                                if($shMobile != '' && $shMobile != null){
                                    $orderDO->setCustomerMobile($shMobile);
                                }
                                else if ($shPhone != '' && $shPhone != null){
                                    $orderDO->setCustomerMobile($shPhone);
                                }
                            }

                        }
                        else{
                            $orderDO->setCustomerMobile( html_entity_decode($customerInfo['mobile']));
                        }
                        $orderDO->setCustomerName($customerInfo['firstname']." ".$customerInfo['lastname']);
	
			return $orderDO;
		} catch (BaseRedirectableException $ex) {
			throw $ex;
		} catch (RedirectableValidationException $rex) {
			throw $rex;
		}
	}
	
	/**
	 * All composed DOs for an OrderDO are created in this method 
	 * @param OrderDO $orderDO
	 * @param MCart $cart
	 */
	
	protected function createAndAssignDOs(OrderDO $orderDO, \MCart $cart) {        

        // This should be the first DO to be created
        // input parameters DO(all order related session and request params DO)
        $inputParamDO = $this->getInputParamDO($orderDO->getContext());
        $orderDO->setInputParamDO($inputParamDO);        

		$orderDO->setSessionId($inputParamDO->sessionId);

		//Cart DO
		$orderDO->setCartDO($this->getCartDO($cart));
		
		//Shipping Address DO
		$shippingDO = $this->getShippingDO($orderDO);
		//var_dump($shippingDO);exit;
		if($shippingDO != NULL){
			$orderDO->setShippingDO($shippingDO);
		}
		
		//Discount
		$orderDO->setDiscountDO($this->getDiscountDO($cart, $orderDO));

		//Temp Order
		$orderDO->setTempOrderDO($this->getTempOrderDO($orderDO));
		
		//Payment
		$paymentDO = $this->getPaymentDO($orderDO);
		$orderDO->setPaymentDO($paymentDO);
		// Default Channel is Web. In case of IVR payment, the channel is IVR
	 	// In case of Cash on Delivery option, the initial order status should be set to PV which implies "Pending Verification"
	 	//otherwise, default order status is PP: Pre-processed        
		$channel = ($paymentDO->getPaymentMethod() === 'ivr') ? 'ivr' : 'web';
                global $skin;
                if ($skin === "skinmobile")
                    $channel = "mobile";
                global $telesales;
                if ($telesales)
                    $channel = "telesalen";
		$orderDO->setChannel($channel);
		
                
		$status = ($paymentDO->getPaymentMethod() === 'cod') ? 'PV' : 'PP';
		$orderDO->setStatus($status);
		
		//Date/Timestamp
		$orderDO->setDate(time());
		$orderDO->setRequestServer(\HostConfig::$serverName);

		//Gift
		$orderDO->setGiftDO($this->getGiftDO($cart));
		
		//AmountDO
		$orderDO->setAmountDO($this->getAmountDO($orderDO,$cart));
		
		//Billing Address
		$orderDO->setBillingAddressDO($this->getBillingAddressDO($orderDO));
		
		//Set User-Type
		$orderDO->setUserType($inputParamDO->userType);
		
		//Set customer information
		$customerInfo = $this->orderDAO->getCustomerInfo($orderDO->getLogin());
                if($customerInfo['mobile'] === '' || $customerInfo['mobile'] === null ){
                             $mobile = $orderDO->getBillingAddressDO()->getMobile();
                            $phone = $orderDO->getBillingAddressDO()->getPhone();
                            if($mobile != '' && $mobile != null){
                                $orderDO->setCustomerMobile($mobile);
                            }
                            else if ($phone != '' && $phone != null){
                                $orderDO->setCustomerMobile($phone);
                            }
                            else{
                                $shMobile = $orderDO->getShippingDO()->getAddressDO()->getMobile();
                                $shPhone  = $orderDO->getShippingDO()->getAddressDO()->getPhone();
                                if($shMobile != '' && $shMobile != null){
                                    $orderDO->setCustomerMobile($shMobile);
                                }
                                else if ($shPhone != '' && $shPhone != null){
                                    $orderDO->setCustomerMobile($shPhone);
                                }
                            }
                }
                else{
                    $orderDO->setCustomerMobile( html_entity_decode($customerInfo['mobile']));
                }

		$orderDO->setCustomerName($customerInfo['firstname']." ".$customerInfo['lastname']);
	}

    // return input params DO
    public function getInputParamDO($context){
        $orderInputParamDOBuilder = new OrderInputParamDOBuilder($context);
        return $orderInputParamDOBuilder->build();                   
    }
	
	public function getCartDO(\MCart $cart) {
		return new CartDO($cart);
	}
	
	public function getShippingDO(OrderDO $orderDO) {
		$shippingDOBuilder = new ShippingDOBuilder($orderDO);
		return $shippingDOBuilder->build();
	}
	
	public function getDiscountDO(\MCart $cart, OrderDO $orderDO) {
		$discountDOBuilder = new DiscountDOBuilder($cart, $orderDO);
		return $discountDOBuilder->build();
	}
	
	public function getTempOrderDO(OrderDO $orderDO) {
		$tempOrderDOBuilder = new TempOrderDOBuilder($orderDO);
		return $tempOrderDOBuilder->build();
	}
	
	public function getPaymentDO(OrderDO $orderDO) {
		$paymentDOBuilder = new PaymentDOBuilder($orderDO);		
		return $paymentDOBuilder->build();
	}
	
	public function getGiftDO($cart) {
		//load gift message information        
		if($cart->getIsGiftOrder()) {
			$giftStatus = 'Y';
			$giftMessage = addslashes($cart->getGiftMessage());
			$giftCharge = $cart->getGiftCharge();
		}
		else {
			$giftStatus = "N";
			$giftCharge = 0;
			$giftMessage = "";
		}
		return new GiftDO($giftStatus, $giftMessage,$giftCharge);
	}
	
	/**
	 * Some fields in Billing Address get populated from Shipping Address, e.g. mobile no.
	 * Also, email gets populated from login
	 * @param ShippingDO $shippingDO
	 */
	public function getBillingAddressDO(OrderDO $orderDO) {
		//Populate Billing Address DO: only for Credit Cards and Debit Cards Payment Mode
		$badrsDOBuilder = new BillingAddressDOBuilder($orderDO);
		return $badrsDOBuilder->build();	
	}
	
	public function getAmountDO(OrderDO $orderDO,\MCart $cart) {
		$amountDOBuilder = new AmountDOBuilder($orderDO,$cart);
		return $amountDOBuilder->build();
	}
	
}
