<?php
namespace cache\keyvaluepairs;

use cache\dao\MCacheDBAdapter;

use cache\keyvaluepairs\SimpleDBFullTableCachedKeyValuePairs;

use cache\keyvaluepairs\SimpleDBKeyValuePairs;

use cache\MXCache;

class MobileUAWhitelistKeyValuePairs{
    
    
    const MOBILE_UA_WHITELIST_CACHE_KEY="MOBILE_UA_WHITELIST_CACHE";
    
    private static $keyValuePairInstance = null;
    
    public static function getKeyValuePairInstance(){
        if(self::$keyValuePairInstance == null){
            self::$keyValuePairInstance = new SimpleDBFullTableCachedKeyValuePairs(self::MOBILE_UA_WHITELIST_CACHE_KEY, 
                    MCacheDBAdapter::TABLE_WIDGET_KEY_VALUE_PAIR, new MXCache(),"key","value" , array("key"=>"mobileUAWhitelist"));
        }
        
        return self::$keyValuePairInstance;
    }
    
}
