<?php
namespace cache\keyvaluepairs;

use cache\dao\MCacheDBAdapter;

use cache\MCache;

use cache\keyvaluepairs\CachedKeyValuePairs;

class SimpleDBFullTableCachedKeyValuePairs extends CachedKeyValuePairs{
	
	protected $map;
	
	protected $tableName;
	protected $keyColumn;
	protected $valueColumn;
	protected $queryFilterArray;
	protected $cacheKey;
	
	public function __construct($cacheKey, $tableName , MCache $cacheObj, $keyColumn="key", $valueColumn='value', $queryFilterArray=null){
		$this->cacheKey = $cacheKey;
		$this->tableName = $tableName;
		$this->keyColumn = $keyColumn;
		$this->valueColumn = $valueColumn;
		$this->queryFilterArray = $queryFilterArray;
		
		parent::__construct($cacheObj);
	}
	
	/**
	 * Init or reinit cache
	 * @see cache\keyvaluepairs.CachedKeyValuePairs::init()
	 */
	protected function init($forced = false){
		if($forced === true){
			$this->cacheObj->remove($this->cacheKey);
		}
		$this->map = $this->cacheObj->fetchAndStore(function($tableName,$keyColumn,$valueColumn, $queryFilterArray){
			
			$mCacheDBAdapter = new MCacheDBAdapter();
			$map = $mCacheDBAdapter->fetchTableKeyValueMap($tableName,$keyColumn,$valueColumn, $queryFilterArray);
			if(empty($map)){
				$map = \cache\keyvaluepairs\SimpleDBFullTableCachedKeyValuePairs::CACHE_NULL_VALUE;
			}
			return $map;
			
		}, array($this->tableName,$this->keyColumn,$this->valueColumn, $this->queryFilterArray), $this->cacheKey);
		
		if($this->map === self::CACHE_NULL_VALUE){
			$this->map = array();
		}
	}
	
	protected function getValueFromCache($key){
		return $this->map[$key];
	}
}