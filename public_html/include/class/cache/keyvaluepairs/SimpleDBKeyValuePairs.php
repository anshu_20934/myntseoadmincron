<?php
namespace cache\keyvaluepairs;

use cache\dao\MCacheDBAdapter;

use cache\MCache;

use cache\keyvaluepairs\CachedKeyValuePairs;

class SimpleDBKeyValuePairs extends CachedKeyValuePairs{
	
	protected $tableName;
	protected $keyColumn;
	protected $valueColumn;
	protected $queryFilterArray;
	protected $cacheKey;
	
	public function __construct($cacheKey, $tableName , MCache $cacheObj, $keyColumn="key", $valueColumn='value', $queryFilterArray=null){
		$this->tableName = $tableName;
		$this->keyColumn = $keyColumn;
		$this->valueColumn = $valueColumn;
		$this->queryFilterArray = $queryFilterArray;
		$this->cacheKey = $cacheKey;
		parent::__construct($cacheObj);
	}
	
	/**
	 * Init or reinit cache
	 * @see cache\keyvaluepairs.CachedKeyValuePairs::init()
	 */
	public function init($forced = false){
		
	}
	
	protected function getValueFromCache($key){
		$value = $this->cacheObj->fetch($this->translateKey($key));
		if($value === self::CACHE_NULL_VALUE)
			return null;
		
		if($value == null){
			$mCacheDBAdapter = new MCacheDBAdapter();
			$value = $mCacheDBAdapter->fetchTableKeyValue($this->tableName, $this->keyColumn , $this->valueColumn , $key);
			$valueToPutInCache = $value;
			if($valueToPutInCache == null){
				$valueToPutInCache = self::CACHE_NULL_VALUE;
			}
			$this->cacheObj->store($this->translateKey($key), $valueToPutInCache, 0);
		}
		
		return $value;
		
	}
	
	protected function translateKey($key){
		return get_class($this)."$".$this->cacheKey."$".$this->tableName."$".$key;
	}
}