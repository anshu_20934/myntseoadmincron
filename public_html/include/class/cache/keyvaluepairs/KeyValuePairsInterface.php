<?php
namespace cache\keyvaluepairs;

use cache\MCache;

interface KeyValuePairsInterface{
	
	
	/**
	 * Get boolean value for a Feature-Gate key.
	 * Parses the value for the key,converts them to boolean and returns
	 * if the value is 'true' or '1' returns true. if value is 'false' or '0', returns false, otherwise returns $defaultValue
	 */
	public function getBoolean($key,$defaultValue=false);
	 
	/**
	 * Get integer value for a Feature-Gate key.
	 * Parses the value for the key,converts them to integer and returns
	 */
	public function getInteger($key,$defaultValue=-1);
	
	/**
	 * Get float value for a Feature-Gate key.
	 * Parses the value for the key,converts them to float and returns
	 */
	public function getFloat($key,$defaultValue=-1) ;
	 
	/**
	 * Get String array for comma-separated values
	 * Parses the value for the key, delimits values based on comma and converts to array after trimming
	 */
	public function getStrArray($key,$default=array());
	
	public function getString($key);
	
	/**
	 * Return json decode value for a key
	 * @param String $key
	 */
	public function getJSONDecodedArray($key);
	
	
}