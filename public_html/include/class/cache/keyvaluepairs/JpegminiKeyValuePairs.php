<?php
namespace cache\keyvaluepairs;

use cache\dao\MCacheDBAdapter;

use cache\keyvaluepairs\SimpleDBFullTableCachedKeyValuePairs;

use cache\keyvaluepairs\SimpleDBKeyValuePairs;

use cache\MXCache;

class JpegminiKeyValuePairs{
	
	
	const JPEG_MINI_CACHE_KEY="JPEG_MINI_CACHE";
	
	private static $keyValuePairInstance = null;
	
	public static function getKeyValuePairInstance(){
		if(self::$keyValuePairInstance == null){
			self::$keyValuePairInstance = new SimpleDBFullTableCachedKeyValuePairs(self::JPEG_MINI_CACHE_KEY, 
					MCacheDBAdapter::TABLE_WIDGET_KEY_VALUE_PAIR, new MXCache(),"key","value" , array("key"=>"jpegminiConfig"));
			
			/* self::$keyValuePairInstance = new SimpleDBKeyValuePairs(self::JPEG_MINI_CACHE_KEY, 
				MCacheDBAdapter::TABLE_WIDGET_KEY_VALUE_PAIR, new MXCache()); */
		}
		
		return self::$keyValuePairInstance;
	}
	
}