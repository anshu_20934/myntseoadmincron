<?php
namespace cache\keyvaluepairs;

use cache\MCache;

abstract class CachedKeyValuePairs implements KeyValuePairsInterface{
	
	const CACHE_NULL_VALUE = "$$$$";
	
	protected $cacheObj;
	
	public function __construct(MCache $cacheObj){
		$this->cacheObj = $cacheObj;
		$this->init();
	}
	
	abstract protected function init($forced = false);
	
	public function refresh($key){
		$this->init(true);
	}
	
	/**
	 * Get boolean value for a Feature-Gate key.
	 * Parses the value for the key,converts them to boolean and returns
	 * if the value is 'true' or '1' returns true. if value is 'false' or '0', returns false, otherwise returns $defaultValue
	 */
	public function getBoolean($key,$defaultValue=false) {
		$value = $this->getValueFromCache($key);
		if($value === "true" || $value === "1" || $value === 1 || $value === true){
			return true;
		}
		else if($value === "false" || $value === "0" || $value === 0 || $value === false){
			return false;
		}
		else {
			return $defaultValue;
		}
	}
	 
	/**
	 * Get integer value for a Feature-Gate key.
	 * Parses the value for the key,converts them to integer and returns
	 */
	public function getInteger($key,$defaultValue=-1) {
		$value = $this->getValueFromCache($key);
		if(is_numeric($value)){
			return (int)$value;
		}
		else {
			return $defaultValue;
		}
	}
	
	/**
	 * Get float value for a Feature-Gate key.
	 * Parses the value for the key,converts them to float and returns
	 */
	public function getFloat($key,$defaultValue=-1) {
		$value = $this->getValueFromCache($key);
		if(is_numeric($value)){
			return (float)$value;
		}
		else {
			return $defaultValue;
		}
	}
	 
	/**
	 * Get String array for comma-separated values
	 * Parses the value for the key, delimits values based on comma and converts to array after trimming
	 */
	public function getStrArray($key,$default=array()) {
		$value = $this->getValueFromCache($key);
		if(!empty($value)) {
			return array_map("trim", explode(",",$value));
		}
		else {
			return $default;
		}
	}
	
	public function getString($key){
		$value = $this->getValueFromCache($key);
		return $value;
	}
	
	/**
	 * Return json decode value for a key
	 * @param String $key
	 */
	public function getJSONDecodedArray($key){
		$value = $this->getValueFromCache($key);
		$value = json_decode($value);
		$value = convertToArrayRecursive($value);
		return $value;
	}
	
	
	abstract protected function getValueFromCache($key);
	
}