<?php
namespace cache\keyvaluepairs;

use cache\dao\MCacheDBAdapter;

use cache\keyvaluepairs\SimpleDBFullTableCachedKeyValuePairs;

use cache\keyvaluepairs\SimpleDBKeyValuePairs;

use cache\MXCache;

class CMSTimerMetaDataKeyValuePairs{
	
	
	const CMS_TIMER_META_CACHE_KEY="CMS_TIMER_META";
	
	private static $keyValuePairInstance = null;
	
	public static function getKeyValuePairInstance(){
		if(self::$keyValuePairInstance == null){
			self::$keyValuePairInstance = new SimpleDBFullTableCachedKeyValuePairs(self::CMS_TIMER_META_CACHE_KEY, 
					MCacheDBAdapter::TABLE_WIDGET_KEY_VALUE_PAIR, new MXCache(),"key","value" , array("key"=>"cms.timer.metadata"));
		}
		return self::$keyValuePairInstance;
	}
	
}
