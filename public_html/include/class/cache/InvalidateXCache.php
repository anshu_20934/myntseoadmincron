<?php

namespace cache;

require_once (dirname(__FILE__)."/../../../env/XCache.config.php");

class InvalidateXCache {
	
	private $instanse=null;
	
	private $servers;
	private $path;
	private $userName;
	private $password;
	
	public static function getInstance() {		
		if($instanse==null) {
			$instance = new InvalidateXCache();
		}		
		return $instance;		
	}
	
	private function __construct() {
		$this->userName = \XCacheConfig::$username;
		$this->password = \XCacheConfig::$password;
		$this->servers = \XCacheConfig::$servers;
		$this->path = \XCacheConfig::$path;
	}
	
	
	private function postAsync($url,$post_string,$userName,$password) {
		
		$parts=parse_url($url);
		$userPassword = $userName . ":" . $password;
	    $fp = fsockopen($parts['host'],
	        isset($parts['port']) ? $parts['port'] : 80,
	        $errno, $errstr, 30);
	
	    $out = "POST ".$parts['path']." HTTP/1.1\r\n";
	    $out.= "Host: ".$parts['host']."\r\n";
	    $out.= "Authorization: Basic ".base64_encode("$userPassword")."\r\n";
	    $out.= "User-Agent: Curl XCache Clear\r\n";
	    $out.= "Content-Type: application/x-www-form-urlencoded\r\n";
	    $out.= "Content-Length: ".strlen($post_string)."\r\n";
	    $out.= "Connection: Close\r\n\r\n";
	    if (isset($post_string)) $out.= $post_string;
	
	    fwrite($fp, $out);
	    fclose($fp);
	}
	
	private function post_async_all($url,$type,$userName,$password) {
		if(!isset($type)) {
			$type = 0;
		}
		$post_string = "type=$type&cacheid=4&clearcache=Clear";
		
		$this->postAsync($url, $post_string, $userName, $password);
		
	}
	
	
	private function post_async_key($url, $params,$userName, $password)
	{
	    foreach ($params as $key => &$val) {
	      if (is_array($val)){
	      	$val = implode(',', $val);
	      }
	      $post_params[] = 'remove[]='.urlencode($val);
	    }
	    $post_string = implode('&', $post_params);
	    
	    $this->postAsync($url, $post_string, $userName, $password);
	}
	
	
	/*
	 * Function to invalidate Xcache across all the servers
	 * @params $key can be an array containing list of keys or a single key to invalidate
	 */
	
	public function invalidateCacheForKey($key) {
		
		if(empty($key)) {
			return false;
		}
		
		$xcache_server = $this->servers;
		foreach ($xcache_server as $server) {
			$url = "http://". $server . $this->path;
			if(is_array($key)) {
				$this->post_async_key($url, $key,$this->userName,$this->password);
			} else {
				$this->post_async_key($url, array($key),$this->userName,$this->password);
			}
		}
		
		return true;		
	}
	
	public function invalidateAllPHPCache() {
		$xcache_server = $this->servers;
		foreach ($xcache_server as $server) {
			$url = "http://". $server . $this->path;
			$this->post_async_all($url, 0, $this->userName, $this->password);
		}
		
		return true;
	}
	
	public function invalidateAllVarCache() {
		$xcache_server = $this->servers;
		foreach ($xcache_server as $server) {
			$url = "http://". $server . $this->path;
			$this->post_async_all($url, 1, $this->userName, $this->password);
		}
		
		return true;
		
	}
	
	public function invalidateCompleteXCache() {
		$this->invalidateAllPHPCache();
		$this->invalidateAllVarCache();
		return true;
	}
}
?>
