<?php
namespace cache\dao;

use base\dao\BaseDAO;

class MCacheDBAdapter extends BaseDAO{
	
	const TABLE_WIDGET_KEY_VALUE_PAIR = "mk_widget_key_value_pairs";
	
	public function fetchTableKeyValueMap($tableName,$keyColumn='key',$valueColumn='value',$filters=null){
		
		$resultSet = $this->selectColumnsQuery($tableName,array($keyColumn, $valueColumn), $filters, null, null, null, true );
		
		return $this->convertResultSetToMap($resultSet,$keyColumn,$valueColumn);
	}
	
	public function fetchTableKeyValue($tableName,$keyColumn='key',$valueColumn='value', $key){
		$resultSet = $this->selectColumnsQuery($tableName,array($keyColumn, $valueColumn), array($keyColumn=>$key), null, null, null, true );
		return $resultSet[0][$valueColumn];
	}
	
	public function convertResultSetToMap($resultSet,$keyColumn, $valueColumn){
		$map = false;
		foreach ($resultSet as $row){
			$map[$row[$keyColumn]] = trim($row[$valueColumn]);
		}
		return $map;
	}
}