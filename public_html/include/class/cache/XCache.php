<?php
include_once($xcart_dir."/Profiler/Profiler.php");
/**
 * Created by IntelliJ IDEA.
 * User: Mohammed Asif Hussain
 * Date: Dec 24, 2009
 * Time: 9:50:32 AM
 * To change this template use File | Settings | File Templates.
 */

class XCache {
    /**
     * On start up check if the extension is installed ..
     */
    var $installed = FALSE;
    
    var $key_prefix = "";

    /**
     * @return void
     */

    public function __construct()
    {
    	$this->key_prefix= HostConfig::$xcachePrefix;
        if(extension_loaded('XCache'))
        {
            $this->installed = TRUE;
        }
    }

    /**
     * Write data to cache.
     *
     * @param string $id cache id
     *
     * @param string $data
     *
     * @param int $expire in seconds
     *
     * @return bool success
     */
    public function store($id, $data, $expire = 0)
    {
    	$profileHandler = Profiler::startTiming("cache-set");
       	$ret = $this->installed && xcache_set($this->key_prefix.$id, $data, $expire);
        Profiler::endTiming($profileHandler, Profiler::getSamplingRate());
        return $ret;
    }


    /**
    * Write data to cache.
    *
    * @param string $id cache id
    *
    * @param Object $data
    *
    * @param int $expire in seconds
    *
    * @return bool success
    */
    public function storeObject($id, $dataObject, $expire = 0)
    {
    	$profileHandler = Profiler::startTiming("cache-set");
    	$ret = $this->installed && xcache_set($this->key_prefix.$id, serialize($dataObject), $expire);
    	Profiler::endTiming($profileHandler, Profiler::getSamplingRate());
    	return $ret;
    }
    
    /**
     * Fetch the cached content
     *
     * @param string $id cache id
     *
     * @return object ..
     */
    public function fetch($id)
    {
        if($this->installed == FALSE)
        {
            return false;
        }

        $profileHandler = Profiler::startTiming("cache-get");
        $ret =  xcache_get($this->key_prefix.$id);
        Profiler::endTiming($profileHandler, Profiler::getSamplingRate());
        return $ret;
    }
    
    /**
    * Fetch the cached content
    *
    * @param string $id cache id
    *
    * @return object ..
    */
    public function fetchObject($id)
    {
    	if($this->installed == FALSE)
    	{
    		return false;
    	}
    	$profileHandler = Profiler::startTiming("cache-get");
    	$ret =  unserialize(xcache_get($this->key_prefix.$id));
    	Profiler::endTiming($profileHandler, Profiler::getSamplingRate());
    	return $ret;
    }
    
    /**
     * Remove the cached content on demand
     *
     * @param string $id cache id
     *
     * @return status
     */
    public function remove($id)
    {
        if($this->installed == FALSE)
        {
            return false;
        }

        $profileHandler = Profiler::startTiming("cache-set");
        $ret =   xcache_unset($this->key_prefix.$id);
        Profiler::endTiming($profileHandler, Profiler::getSamplingRate());
        return $ret;
    }
    
    /**
     * 
     * This is a composite function which both fetches from xcache (if found) as well as adds to xcache if missed
     * @param unknown_type $function : the function handlre to be called in case of cache miss and the return value should be the value to be put in cache
     * @param unknown_type $params : the additional args to be passed to the function
     * @param unknown_type $id : the key in xcache in which the value is to be added
     * @param unknown_type $expiry : the expiry time (in seconds?)
     * 
     * @author Kundan Burnwal
     */
	public function fetchAndStore($function, $params, $id, $expiry=0) {
		global $weblog;
		global $errorlog;
		// check if key is in cache
		$cachedVal = $this->fetch($id);
		if(!($cachedVal===null)){
			//$weblog->debug("xcache:".$id.":cache-hit");
			return $cachedVal;
		}
		//$weblog->debug($id.":cache-miss");
		// Compute value from user defined function
		$val = call_user_func_array($function, $params);
		try {
			$this->store($id, $val, $expiry);
		} catch (Exception $e) {
            $weblog->error($e);
            $errorlog->error($e);
		}
		return $val;
	}
    
}
