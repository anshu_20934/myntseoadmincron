<?php
namespace cache;
include_once(\HostConfig::$documentRoot."/Profiler/Profiler.php");

abstract class MCache{
	
	private $key_prefix = "";
	
	
	public function __construct(){
		$this->key_prefix= \HostConfig::$xcachePrefix;
	}
	
	abstract protected function fetchValue($key);
	
	abstract protected function storeValue($key, $value, $expire);
	
	abstract protected function removeValue($key);
	
	
	/**
	 * Cache fetch
	 * @param String $key
	 */
	public function fetch($key){
		$profileHandler = \Profiler::startTiming("cache-get");
		$ret = $this->fetchValue($this->key_prefix.$key);
		\Profiler::endTiming($profileHandler, \Profiler::getSamplingRate());
		return $ret;
	}
	
	/**
	 * Cache store
	 * @param String $key
	 * @param array|String|Number $value
	 * @param int $expire
	 */
	public function store($key, $value, $expire){
		$profileHandler = \Profiler::startTiming("cache-set");
		$ret = $this->storeValue($this->key_prefix.$key, $value, $expire);
		\Profiler::endTiming($profileHandler, \Profiler::getSamplingRate());
		return $ret;
	}
	
	/**
	 * Cache fetch
	 * Internally deserializes, should only be used only if the object was stored using storeObject method
	 * @param String $key
	 */
	public function fetchObject($key){
		$profileHandler = \Profiler::startTiming("cache-get");
		$ret = unserialize($this->fetchValue($this->key_prefix.$key));
		\Profiler::endTiming($profileHandler, \Profiler::getSamplingRate());
		return $ret;
	}
	
	/**
	 * Cache store objects. Serializes object internally and then stores it
	 * @param String $key
	 * @param array|String|Number $value
	 * @param int $expire
	 */
	public function storeObject($key, $value, $expire = 0 ){
		$profileHandler = \Profiler::startTiming("cache-set");
		$ret = $this->storeValue($this->key_prefix.$key, serialize($value), $expire);
		\Profiler::endTiming($profileHandler, \Profiler::getSamplingRate());
		return $ret;
	}
	
	/**
	 * removes a key from cache
	 * @param String $key
	 */
	public function remove($key){
		$profileHandler = \Profiler::startTiming("cache-set");
		$ret = $this->removeValue($this->key_prefix.$key);
		\Profiler::endTiming($profileHandler, \Profiler::getSamplingRate());
		return $ret;
	}
	
	
	/**
	 *
	 * This is a composite function which both fetches from xcache (if found) as well as adds to xcache if missed
	 * @param function $function : the function handlre to be called in case of cache miss and the return value should be the value to be put in cache
	 * @param array $params : the additional args to be passed to the function
	 * @param String $key : the key in xcache in which the value is to be added
	 * @param int $expiry : the expiry time (in seconds?)
	 *
	 */
	public function fetchAndStore($function, $params, $key, $expiry=0){
	
		$cachedVal = $this->fetch($key);
		
		if(!($cachedVal===null)){
			return $cachedVal;
		}	
		$val = call_user_func_array($function, $params);
		$this->store($key, $val, $expiry);

		return $val;
		
	}
	
	/**
	 * For Objects
	 * This is a composite function which both fetches from xcache (if found) as well as adds to xcache if missed
	 * @param function $function : the function handlre to be called in case of cache miss and the return value should be the value to be put in cache
	 * @param array $params : the additional args to be passed to the function
	 * @param String $key : the key in xcache in which the value is to be added
	 * @param int $expiry : the expiry time (in seconds?)
	 *
	 */
	public function fetchAndStoreObject($function, $params, $key, $expiry=0){
	
		$cachedVal = $this->fetchObject($key);
	
		if(!($cachedVal===null)){
			return $cachedVal;
		}
		$val = call_user_func_array($function, $params);
		$this->storeObject($key, $val, $expiry);
		
		return $val;
	
	}
	
}
