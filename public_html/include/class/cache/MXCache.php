<?php
namespace cache;

class MXCache extends MCache{

	private $installed = FALSE;

	public function __construct(){
		if(extension_loaded('XCache'))
		{
			$this->installed = TRUE;
		}
	}

	protected function fetchValue($key){
		if($this->installed == FALSE)
		{
			return false;
		}

		return  xcache_get($key);
	}

	protected function storeValue($key, $value, $expire){
		return $this->installed && xcache_set($key, $value, $expire);
	}

	protected function removeValue($key){
		if($this->installed == FALSE)
		{
			return false;
		}

		return  xcache_unset($key);
	}
}