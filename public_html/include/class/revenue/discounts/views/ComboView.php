<?php
namespace revenue\discounts\views;

use revenue\discounts\builder\ComboPageBuilder;


class ComboView {
	
	private $type;
	private $params; 
	
	public function __construct($params){
		global $smarty;
		$smarty->debugging = false; // the default
		$smarty->debugging_ctrl = ($_SERVER['SERVER_NAME'] == 'localhost') ? 'URL' : 'NONE';
		
		//$this->type = $type;
		$this->params = $params;
	}	

	public function display() {
	
		$builder = new ComboPageBuilder($this->params['cid']);

		$builder->build((int)$this->params['page']);
		$uiParams = $builder->getUiParams();
		//echo "<PRE>";print_r($uiParams);exit;
		global $smarty;
		foreach ($uiParams as $k => $v) {
			$smarty->assign($k, $v);
		}
                $smarty->assign("uiParamsJSON",  json_encode($uiParams));
                $smarty->assign("cid",$this->params['cid']);
		//let the UI remain the same
		if(empty($this->params['mini'])){
			func_display("combo/combo_view.tpl", $smarty);	
		}
		else {
                        if(!$this->params['all']){
                             unset($uiParams["data"]);
                             unset($uiParams["comboSet"]);
                        }
			echo json_encode(array('status'=>'success','data'=> $uiParams));
			exit;
		}
	}

}

?>
