<?php
namespace revenue\discounts\views;

use revenue\discounts\builder\ComboPageBuilder;

class AjaxComboView {
	
	private $type;
	private $params; 
	
	public function __construct($type, $params){
		global $smarty;
		$smarty->debugging = false; // the default
		$smarty->debugging_ctrl = ($_SERVER['SERVER_NAME'] == 'localhost') ? 'URL' : 'NONE';
		
		$this->type = $type;
		$this->params = $params;
	}	

	public function display() {
		$builder = new ComboPageBuilder($this->params['cid'],$this->params['sortby']);

		$builder->build((int)$this->params['page']);
		$uiParams = $builder->getUiParams();
		// /echo "<PRE>";print_r($uiParams);exit;
		global $smarty;
		foreach ($uiParams as $k => $v) {
			$smarty->assign($k, $v);
		}
		//echo this json encode
		if($this->params['all']){
                    echo json_encode(array('status'=>'success','data'=> $uiParams));
                    exit;
                }
		$sortedData =$smarty->fetch("combo/combo_items.tpl", $smarty);
                 
		echo json_encode(array('status'=>'success','data'=>$sortedData));
		exit;
	}

}

?>
