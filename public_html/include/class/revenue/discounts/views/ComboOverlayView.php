<?php
namespace revenue\discounts\views;

use revenue\discounts\builder\CartComboBuilder;
use revenue\discounts\builder\PdpComboBuilder;
use style\builder\CachedStyleBuilder;
use style\builder\StyleBuilder;


class ComboOverlayView {
	
	private $type;
	private $params; 
	
	public function __construct($type, $params){
		global $smarty;
		$smarty->debugging = false; // the default
		$smarty->debugging_ctrl = ($_SERVER['SERVER_NAME'] == 'localhost') ? 'URL' : 'NONE';
		
		$this->type = $type;
		$this->params = $params;
	}	

	public function getJsonData() {
        $uiParams = $this->getData();
        $json = array();
        foreach($uiParams["comboSet"] as $freeItem){
            if(!empty($uiParams["freeproducts"][$freeItem["styleid"]])){
                $freeItem["isSelected"] =  true;
                foreach($uiParams["freeproducts"][$freeItem["styleid"]] as $skuid=>$details)
                {
                	$freeItem["skuId"] = $skuid;
                	$freeItem["itemid"] = $details['productid'];
                }    
            } else {
                $freeItem["isSelected"] = false;
            }
            
            if(isset($_SERVER["HTTPS"]))//then it is https
            {
            	$imageURL = $freeItem["search_image"];
            	if(!strncmp($imageURL, \HostConfig::$cdnBase, strlen(\HostConfig::$cdnBase))){
            		$imageURL = substr_replace($imageURL, \HostConfig::$secureCdnBase, 0, strlen(\HostConfig::$cdnBase));
            	}
            	$freeItem["search_image"] = $imageURL;
            }

            
            /* get the sku and size details */
			$cachedStyleBuilderObj=CachedStyleBuilder::getInstance();
			$styleObj=$cachedStyleBuilderObj->getStyleObject($freeItem["styleid"]);
			$freeItem['productOptions'] = $styleObj->getAllProductOptions();


            $json[$freeItem["styleid"]]=$freeItem;
        }
        $json = json_encode($json);
        return $json;
    }	
	
	private function getData() {
		if($this->type=='cart'){
			$builder = new CartComboBuilder($this->params['id'], $this->params['icm'], $this->params['m'], $this->params['ct'], $this->params['freecartc']);	
		}else{
			$builder = new PdpComboBuilder($this->params['sid']);
		}
		
		$builder->build();
		$uiParams = $builder->getUiParams();
		return $uiParams;
	}
	
	public function display() {
		$uiParams = $this->getData();

		global $smarty;
		foreach ($uiParams as $k => $v) {
			$smarty->assign($k, $v);
		}
		if($this->params['freecartc'])
			func_display("cart/combo_overlay_cl.tpl", $smarty); 
		else
			func_display("cart/combo_overlay.tpl", $smarty);
	}

}

?>
