<?php
namespace revenue\discounts\builder;

use style\builder\CachedStyleBuilder;
use style\builder\StyleBuilder;

class PdpComboBuilder extends ComboBuilder{
	
	private $styleId;
	
	public function __construct($styleId){
		$this->styleId=$styleId;
	}	
	
	public function build() {

		//Get the discountdata from the styleObj
		$cachedStyleBuilderObj= CachedStyleBuilder::getInstance();
		$styleObj=$cachedStyleBuilderObj->getStyleObject($this->styleId);
		if($styleObj == NULL){
			if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
				$content = json_encode(array('status'=>'error','message'=>'style not found'));
				die($content);
			}else{
				$XCART_SESSION_VARS['errormessage']="Requested page was not found on this server";
		   		func_header_location("mksystemerror.php");
		   		exit;	
			}
			
		}
		$discountData= $styleObj->getDiscountData();
		$this->discountId = $discountData->discountId;
		$this->relatedStyleIds = $discountData->discountToolTipText->relatedProducts;	
		parent::build();		
		
		global $_rst;
	    	$displayText = $_rst[$discountData->discountToolTipText->id];
		$minMore = 0;
		$cType = '';

		if(!empty($discountData->discountToolTipText->params)){
	            	foreach($discountData->discountToolTipText->params as $k=>$v) {
        	        	$this->uiParams['dre_'.$k] = $v;
            		}
        	}

		if(isset($this->uiParams['dre_buyAmount'])){
		    $minMore = $this->uiParams['dre_buyAmount'];
		    $this->uiParams['productsSet']['dre_buyAmount'] = $minMore;
		    $cType = 'a';
		}else{
		    $minMore = $this->uiParams['dre_buyCount'];
		    $this->uiParams['productsSet']['dre_buyCount'] = $minMore;
		    $cType = 'c';
		}
		$this->uiParams['productsSet']['dre_count'] = $this->uiParams['dre_count'];		
		
		$this->uiParams["min"] = $minMore;
		$this->uiParams["ctype"] = $cType;
		$this->uiParams["discountIcon"] = $_rst[$discountData->icon."_L"];
		$this->uiParams["displayText"] = $displayText;
	
	}  
	
}

?>
