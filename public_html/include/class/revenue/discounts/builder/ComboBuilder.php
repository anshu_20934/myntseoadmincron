<?php
namespace revenue\discounts\builder;

use style\builder\CachedStyleBuilder;
use style\builder\StyleBuilder;
//var_dump(require_once("include/class/style/dao/StyleDBAdapter.php"));

class ComboBuilder {
	
	protected $discountId;
	protected $relatedStyleIds;
	protected $isConditionMet = false;
	protected $sortField = '';
			
	protected $uiParams = array();
	
	public function __construct(){
	
	}

	public function build($pageNum = -1) {
		$pageSize = 1000;	
		$mcartFactory= new \MCartFactory();
		$myCart= $mcartFactory->getCurrentUserCart(true);
                global $sortField,$columns;
                $columns = array("styleid","product","stylename","price","global_attr_brand","search_image","dre_percent","dre_amount","sizes","discount","discount_label","dre_buyCount","dre_count","dre_buyAmount");
                 $solrProducts = new \SolrProducts();
                $searchProducts = new \SearchProducts();
                $searchProducts->sort_param = $this->sortField;
                $sortField = $searchProducts->getSortField();
		$comboData =  \MCartUtils::getComboDisplayData($myCart, true, $this->discountId);
		$productArray = (array) \MCartUtils::convertCartToArray($myCart);
		$cartComboData = $myCart->getDisplayData();
		$itemsAvailabilityArray = $myCart->getItemsAvailabilityArray();
		$cachedStyleBuilderObj= CachedStyleBuilder::getInstance();
		//else relatedproducts will be updated by PdpRedisData
		global $styleIdsArray,$styleIdsDetail;
		if(!empty($comboData) && isset($comboData->styleIds))
			$this->relatedStyleIds = $comboData->styleIds;
		else
                        $this->relatedStyleIds = $styleIdsArray;

		//get the products from the cart belonging to this combo if any 
		$comboSavings = 0;
		$comboTotal = 0;
		$productMiniArray = array();
		$selectedStyles = array();
		$selectedStyleProductMap = array();
		$styleIds = array();
		// /echo "<PRE>";print_r($productArray);exit;
		foreach ($productArray as $idx => $product) {
			if($product['discountComboId']==$this->discountId && !$product['freeItem']){
				array_push($styleIds, $product['productStyleId']);
				array_push($selectedStyles, $product['productStyleId']);
				//$selectedStyleProductMap[$product['skuid']] = array( 'itemid' => $idx);
				$comboSavings = $comboSavings + $product['discountAmount'];
				$comboTotal = $comboTotal + ($product['productPrice'] * $product['quantity']);
			
				$productMini = array();
				$productMini['productid'] = $idx;
				$productMini['productPrice'] = $product['productPrice'];
				$productMini['quantity'] = $product['quantity'];
				$productMini['discountAmount'] = $product['discountAmount'];
				$productMini['comboitemtype'] = 'default';
				if(!empty($product['sizenameunified'])){
					$productMini['sizenameunified'] = $product['sizenameunified'];
				}
				else {
					$productMini['sizenameunified'] = $product['sizename'];	
				}
				
	    
				$productMiniArray[$product['productStyleId']][$product['skuid']] = $productMini;
				if(isset($selectedSizes[$product['productStyleId']])){
					array_push($selectedSizes[$product['productStyleId']],$product['skuid']);
				}else {
					$selectedSizes[$product['productStyleId']] = array();
					array_push($selectedSizes[$product['productStyleId']],$product['skuid']);
				}
			} else if($product['freeItem']){
				$productMini = array();
				$productMini['productid'] = $idx;
				$productMini['productPrice'] = $product['productPrice'];
				$productMini['quantity'] = $product['quantity'];
				$productMini['discountAmount'] = $product['discountAmount'];
				$productMini['comboitemtype'] = 'freeitem';
				$freeproductMiniArray[$product['productStyleId']][$product['skuid']] = $productMini;
			}
			$selectedStyleProductMap[$product['skuid']] = array( 'itemid' => $idx);
		}
			
		//get the display data for the related products from solr
		
		if (!empty($comboData) && isset($comboData->allStyleDetails))
                        $solrResult = $comboData->allStyleDetails;
                else
                        $solrResult = $styleIdsDetail ;

                
                $relatedProdNum = sizeof($solrResult);
                if ($pageNum >= 0){
                    $solrResult=$this->splitBetweenIndexes($pageNum*$pageSize, $pageSize*($pageNum+1), $solrResult);
                }
		$allSolrResult = $solrResult;
		$productsSet = array();

		if(!empty($comboData->params)){
		    foreach($comboData->params as $k=>$v) {
		        $productsSet['dre_'.$k] = $v;
		    }
			$freeItemsIds = $comboData->params->freeItems;
		} else if(!empty($cartComboData->params)){
			$freeItemsIds = $cartComboData->params->freeItems;
		}

		if(!empty($freeItemsIds)){
			$freeItemDetails =array();
			foreach ($freeItemsIds as $p) {
				$freeItemStyleObj=$cachedStyleBuilderObj->getStyleObject($p);
				$freeItemStyleProperties = $freeItemStyleObj->getStyleProperties();
	    		$freeItemDetails[$p]['search_image'] = $freeItemStyleProperties[0]['search_image'];
			}		
		
		}


		$productStyleIdStr = "'".implode($freeItemsIds,"','")."'";
 		$sql = "SELECT  * FROM mk_style_properties s, mk_product_style a WHERE s.style_id = a.id AND a.id IN ($productStyleIdStr)";
		$styleIdDetails = func_query($sql,true);
		foreach($styleIdDetails as $id=>$arr){
			$a = array();
			$a['styleid'] = $arr['style_id'];
			$a['stylename'] = $arr['product_display_name'];
			$a['product'] = $arr['product_display_name'];
			$a['price'] = $arr['price'];
			$a['global_attr_brand'] = $arr['global_attr_brand'];
			$a['search_image'] = $arr['search_image']; 
			$freeItemsSolrResult['fi'.$id] = $a;
			$allSolrResult['fi'.$id]=$a;
		}
		$dataMap = array();
		foreach ($allSolrResult as $p) {
		    $dataMap[$p['styleid']] = $p;
		}		
		
		global $_rst;

		$this->uiParams = array(
				"comboId"=> $this->discountId,
				"selectedSKUs"=> $selectedSizes,
				"data"=> $solrResult,
				"comboSet"=>$allSolrResult,
				"dataMap"=> $dataMap,
				"freeItemsSolrResult" => $freeItemsSolrResult,
				"isConditionMet"=> $this->isConditionMet,
				"selectedStyles"=> $selectedStyles,
				"selectedStyleProductMap" => $selectedStyleProductMap,
				"comboSavings"=> $comboSavings,
				"comboTotal"=> $comboTotal,
				"products"=> $productMiniArray,
				"freeproducts"=>$freeproductMiniArray,
				"discountIcon"=> $_rst[$comboData->icon."_L"],
				"displayText"=> $_rst[$comboData->id],
				"productsSet"=>$productsSet,
				"freeItemDetails"=>	$freeItemDetails,
				"dre_freeItems"=>json_decode($productsSet['dre_freeItems']),
				"dre_itemName"=>$productsSet['dre_itemName'],
				"dre_itemPrice"=>$productsSet['dre_itemPrice'],
                                "relatedProdNum"=>$relatedProdNum,
				);
		//echo "<PRE>";print_r($this->uiParams);exit;
	}  
	private function  splitBetweenIndexes($i,$j,$completeArray){
            
            
            for ($x = $i;$x<$j;$x++){
                if ($x < sizeof($completeArray))
                 $retArray[$x-$i]=$completeArray[$x];
                  
                else
                  return $retArray;
            }
            return $retArray;
        }
	public function getUiParams() {
		return $this->uiParams;
	}
}

?>
