<?php
namespace revenue\discounts\builder;

class CartComboBuilder extends ComboBuilder{
	
	private $minMore;
	private $cType;
	public $freeCartC;
	
	public function __construct($discountId, $isConditionMet, $minMore, $cType,$freeCartC){
		$this->discountId = $discountId;
		$this->minMore = $minMore;
		$this->isConditionMet = $isConditionMet;
		$this->cType = $cType;
		$this->freeCartC= $freeCartC;
	}	

	public function build() {
		parent::build();
		
		$this->uiParams["min"] = $this->minMore;
		$this->uiParams["ctype"] = $this->cType;
	}
}

?>
