<?php
namespace revenue\discounts\builder;

use style\builder\CachedStyleBuilder;
use style\builder\StyleBuilder;

class AjaxComboBuilder extends ComboBuilder{
	
	public function __construct($discountId,$sortField){
		$this->discountId=$discountId;
		$this->sortField=$sortField;
	}	
	
	public function build($pageNum = -1) {
		parent::build($pageNum);		
	
	}  
	
}

?>
