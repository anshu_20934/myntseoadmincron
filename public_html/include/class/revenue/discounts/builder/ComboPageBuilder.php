<?php
namespace revenue\discounts\builder;

use style\builder\CachedStyleBuilder;
use style\builder\StyleBuilder;

class ComboPageBuilder extends ComboBuilder{
	
	/*Usefull only when cartitem related to this discount is empty*/
	private $styleId;
			
	public function __construct($discountId,$sortField = ''){
		$this->discountId=$discountId;
		$this->sortField=$sortField;
	}	
	public function build($pageNum = -1) {
		
		$mcartFactory= new \MCartFactory();
		$myCart= $mcartFactory->getCurrentUserCart(true);
		$productArray =  convertToArrayRecursive($myCart->getSetDisplayData());	
		
		parent::build($pageNum);

                /*$data = \DiscountEngine::getComboAssociatedStyleids($this->discountId);
                $this->styleId = $data['0'];*/
                $cachedStyleBuilderObj= CachedStyleBuilder::getInstance();
//      var_dump($this->relatedStyleIds);exit;
		if (empty($this->relatedStyleIds)){
			global $styleIdsArray;
			$this->relatedStyleIds = $styleIdsArray;
		}
			
                $styleObj=$cachedStyleBuilderObj->getStyleObject($this->relatedStyleIds[0]);
                if($styleObj == NULL){
                        if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
                                $content = json_encode(array('status'=>'error','message'=>'style not found'));
                                die($content);
                        }else{
                                $XCART_SESSION_VARS['errormessage']="Requested page was not found on this server";
                                func_header_location("mksystemerror.php");
                                exit;
                        }

                }
                $discountData= $styleObj->getDiscountData();
                $this->discountId = $discountData->discountId;
                /*$this->relatedStyleIds = $discountData->discountToolTipText->relatedProducts;
                */
        //if cartitem is empty fetch one styleid from AssociatedStyleIds and build using the styleId

                global $_rst;
            $displayText = $_rst[$discountData->discountToolTipText->id];
                $minMore = 0;
                $cType = '';


		if(!empty($discountData->discountToolTipText->params)){
        	foreach($discountData->discountToolTipText->params as $k=>$v) {
	        	$this->uiParams['dre_'.$k] = $v;
    		}
    	}

		if(isset($this->uiParams['dre_buyAmount'])){
		    $minMore = $this->uiParams['dre_buyAmount'];
		    $this->uiParams['productsSet']['dre_buyAmount'] = $minMore;
		    $cType = 'a';
		}else{
		    $minMore = $this->uiParams['dre_buyCount'];
		    $this->uiParams['productsSet']['dre_buyCount'] = $minMore;
		    $cType = 'c';
		}

		if(!empty($discountData->discountToolTipText->params->freeItems)){
			$this->uiParams['productsSet']['dre_freeItems'] = $discountData->discountToolTipText->params->freeItems;
			$freeItemDetails =array();
			foreach ($discountData->discountToolTipText->params->freeItems as $p) {
				$freeItemStyleObj=$cachedStyleBuilderObj->getStyleObject($p);
				$freeItemStyleProperties = $freeItemStyleObj->getStyleProperties();
	    		$freeItemDetails[$p]['search_image'] = $freeItemStyleProperties['search_image'];
			}		
			$this->uiParams['freeItemDetails']= $freeItemDetails;	

		}

		$this->uiParams['productsSet']['dre_count'] = $this->uiParams['dre_count'];		
		$this->uiParams['productsSet']['dre_count'] = $this->uiParams['dre_count'];		
		$this->uiParams["discountIcon"] = $_rst[$discountData->icon."_L"];
		$this->uiParams["displayText"] = $displayText;	
				
		if(empty($productArray[$this->discountId])){	
			$this->uiParams["min"] = $minMore;
			$this->uiParams["ctype"] = $cType;
		}
		else{
			//echo "<PRE>";print_r($productArray[$this->discountId]);exit;
			$this->uiParams["min"] =empty($productArray[$this->discountId]['params']['buyAmount'])?$productArray[$this->discountId]['params']['buyCount']:$productArray[$this->discountId]['params']['buyAmount'];;
			$this->uiParams["ctype"] = empty($productArray[$this->discountId]['params']['buyAmount'])?'c':'a';	
		}
		
		

	}
	
}

?>
