<?php
namespace revenue\payments;

use order\dataobject\PaymentDO;

use revenue\payments\gateway\AtomGateway;
use revenue\payments\gateway\CODGateway;
use revenue\payments\gateway\AxisBankGateway;
use revenue\payments\gateway\CCAvenueGateway;
use revenue\payments\gateway\EBSGateway;
use revenue\payments\gateway\HDFCBankGateway;
use revenue\payments\gateway\ICICIBankGateway;
use revenue\payments\gateway\CITIBankGateway;
use revenue\payments\gateway\TekProcessGateway;


use enums\revenue\payments\CardType;
use enums\revenue\payments\PaymentType;
use enums\revenue\payments\Gateways;

use order\exception\NoSuitableGatewayException;

use common\Util;

require_once \HostConfig::$documentRoot."/icici/ajax_bintest.php";

class PaymentGatewayFactory {
	

	public static function getInstance() {
		static $pgf = null;
		if($pgf==null) {
			$pgf = new PaymentGatewayFactory();
		}
		return $pgf;
	}
	
	
	private function __construct() {
			
	}
	
	private function getNetBankingGateway($bankid, $notWorkingGateways) {
		global $weblog;
		$netbanking_gateways = \NetBankingHelper::getActiveGatewaysListForBank($bankid);
        $gatewaySelected = \NetBankingHelper::getNetBankingGateway($netbanking_gateways);             
        if(empty($gatewaySelected)) {
        	//no supporting gateway for the bank found redirecting him to payment page if the bank is show in the payment page bank list its time to clear xcache.
            if(!empty($weblog)) $weblog->info("Redirecting back to the payment page. Reason: No suitable gateway found for $bankName, id= $bank_id");
            func_header_location($https_location . "/mkpaymentoptions.php?transaction_status=N",false);
        }
        
        $gateway = strtolower($gatewaySelected['name']);
        $gateway_code = strtoupper($gatewaySelected['gateway_code']);    
        
        switch ($gateway) {
        	case Gateways::tekProcess:
        		if(!in_array($gateway, $notWorkingGateways)) {
        			return new TekProcessGateway($bankid, $gateway_code);
        		}
        		break;
        	case Gateways::EBS:
        		if(!in_array($gateway, $notWorkingGateways)) {
        			return new EBSGateway($bankid, $gateway_code);
        		}
        		break;
        	case Gateways::CCAvenue:
        		if(!in_array($gateway, $notWorkingGateways)) {
        			return new CCAvenueGateway($bankid, $gateway_code, null, PaymentType::NetBanking);
        		}	
        		break;
        	default:
        		if(!empty($weblog)) $weblog->info("Redirecting back to the payment page. Reason: No suitable gateway found for $bankName, id= $bank_id");
            	func_header_location($https_location . "/mkpaymentoptions.php?transaction_status=N",false);
            	break;
        }
	}
	
	private function getEMIGateway($ccNumber,$paymentType,$emiBank) {

		$bin = substr($ccNumber, 0,6);
		$bankName = getIssuingBank($bin);
		$cardIssuer = $this->determineCardType($ccNumber);
		if($emiBank==Gateways::ICICIBank3EMI || $emiBank==Gateways::ICICIBank6EMI) {
			$iciciEMIBINS = \FeatureGateKeyValuePairs::getStrArray('Payments.EMIBanks.ICICI.Bins');
			//check if the bin is in $iciciEMIBIN
			if(in_array($bin, $iciciEMIBINS)) {				
				$gateway = new ICICIBankGateway($ccNumber, $cardIssuer,$bankName,$paymentType,$emiBank);
				return $gateway; 
			} else {
				//cant process the card number provided
				return false;
			}
		}
		else if($emiBank==Gateways::CITIBank3EMI || $emiBank==Gateways::CITIBank6EMI){
			$citiEMIBINS = \FeatureGateKeyValuePairs::getStrArray('Payments.EMIBanks.CITI.Bins');
			//check if the bin is in $citiEMIBIN
			if(in_array($bin, $citiEMIBINS)) {
				$gateway = new CITIBankGateway($ccNumber, $cardIssuer,$bankName,$paymentType,$emiBank);
				return $gateway;
			} else {
				//cant process the card number provided
				return false;
			}			
		}
		
	}
	
	private function getCardGateway($ccNumber,$paymentType) {
		//assign right banks based on weights
	    $cardIssuer = $this->determineCardType($ccNumber);
	    $bin = substr($ccNumber, 0,6);
        $bankName = getIssuingBank($bin);
        $icici_gateway_up = \FeatureGateKeyValuePairs::getBoolean('payments.iciciPG',false);
        $axis_gateway_up =  \FeatureGateKeyValuePairs::getBoolean('payments.axisPG',false);
        $icici_gateway_available = \FeatureGateKeyValuePairs::getBoolean('payments.isICICIPGAvailable',false);
        $axis_gateway_available = \FeatureGateKeyValuePairs::getBoolean('payments.isAxisPGAvailable',false);
        
        
        
        $gatewayList = array();
        $weights = array();
		
		$cardIssuer = $this->determineCardType($ccNumber);
		$bin = substr($ccNumber, 0,6);
		$bankName = getIssuingBank($bin);
		$icici_gateway_up = \FeatureGateKeyValuePairs::getBoolean('payments.iciciPG',false);
		$axis_gateway_up =  \FeatureGateKeyValuePairs::getBoolean('payments.axisPG',false);
		$icici_gateway_available = \FeatureGateKeyValuePairs::getBoolean('payments.isICICIPGAvailable',false);
		$axis_gateway_available = \FeatureGateKeyValuePairs::getBoolean('payments.isAxisPGAvailable',false);
		
		$hdfc_gateway_up = \FeatureGateKeyValuePairs::getBoolean('payments.hdfcPG',false);
		$hdfc_gateway_available = \FeatureGateKeyValuePairs::getBoolean('payments.isHDFCPGAvailable',false);
		
		//$citi_gateway_up = \FeatureGateKeyValuePairs::getBoolean('payments.citiPG',false);
		//$citi_gateway_available = \FeatureGateKeyValuePairs::getBoolean('payments.iscitiPGAvailable',false);
	
		//return new CITIBankGateway($ccNumber, $cardIssuer,$bankName,$paymentType);
		
		foreach ($notWorkingGateways as $gatewayName) {
			if($gatewayName==Gateways::HDFCBank) {
				$hdfc_gateway_available = false;
			} elseif($gatewayName==Gateways::ICICIBank) {
				$icici_gateway_available = false;
			} elseif($gatewayName==Gateways::AxisBank) {
				$axis_gateway_available = false;
			} elseif($gatewayName==Gateways::CITIBank) {
				$citi_gateway_available = false;
			}
		}
		
		$gatewayList = array();
		$weights = array();
		
		$gateway = null;
		$lastHour = time() - 3600;
		
//		if($configMode=="local"||$configMode=="test"||$configMode=="release") {
//			if($ccNumber=="4012888888881881"||$ccNumber=="4123450131001381") {
//					$bankName = "ICICI TEST CARDS";
//			}
//		}
        if($cardIssuer==CardType::VISA || $cardIssuer==CardType::MasterCard) {
			$lastHour = time() - 3600;
			$processInternationalTransactionInline = \FeatureGateKeyValuePairs::getBoolean('payments.inlineInternationalTransaction',false);
			$gateway_cutoff_transaction = \FeatureGateKeyValuePairs::getInteger('payments.inlinePGCutOffTrans',50);
        	if($bankName && $icici_gateway_up && $icici_gateway_available) {
        		//find the number of PP transaction for ICICI Gateway in last one hour        			
        		$iciciTimeOutCount = func_query_first_cell("select count(*) from mk_payments_log where payment_gateway_name='icici' and is_complete is null and time_insert>$lastHour");
        		if($gateway_cutoff_transaction > $iciciTimeOutCount) {        		
        			$weights[]= \FeatureGateKeyValuePairs::getInteger('payments.iciciPGWeight', 0);
        			$gatewayList[] = new ICICIBankGateway($ccNumber, $cardIssuer,$bankName,$paymentType);
        		}
        	}	       		
        	
        	if(($bankName||$processInternationalTransactionInline) && $axis_gateway_up && $axis_gateway_available) {
        		$axisTimeOutCount = func_query_first_cell("select count(*) from mk_payments_log where payment_gateway_name='axis' and is_complete is null and time_insert>$lastHour");
        		if($gateway_cutoff_transaction > $axisTimeOutCount) {        		
        			$weights[]= \FeatureGateKeyValuePairs::getInteger('payments.axisPGWeight', 0);        			
        			$gatewayList[] = new AxisBankGateway($ccNumber, $cardIssuer,$bankName,$paymentType);        		
        		}
        	}
        	
        }
        
        if($cardIssuer==CardType::Maestro) { 
        	//It can be maestro card see if icici is able to process it. 
			if($bankName && $icici_gateway_up &&$icici_gateway_available) {
				$weights[]= \FeatureGateKeyValuePairs::getInteger('payments.icicPGWeight', 0);
				$gatewayList[] = new ICICIBankGateway($ccNumber, $cardIssuer,$bankName,$paymentType);
			}
        	        	
        }
        $gateway = Util::selectRandomArrayElement($gatewayList, $weights);
        if(empty($gateway)) {
        	if($cardIssuer==CardType::VISA || $cardIssuer==CardType::MasterCard) {
        		if(($bankName||$processInternationalTransactionInline)&&$axis_gateway_up && $axis_gateway_available) {
        			$gateway = new AxisBankGateway($ccNumber, $cardIssuer,$bankName,$paymentType); //fall back to axis in case no other gateway is available
        		} elseif($bankName && $icici_gateway_up&&$icici_gateway_available) {
        			$gateway = new ICICIBankGateway($ccNumber, $cardIssuer,$bankName,$paymentType); //fall back to icici in case no other gateway is available
        		} else {
        			$gateway = new CCAvenueGateway(null, null, $ccNumber, $paymentType, $cardIssuer,$bankName);//fall back to ccavenue in case no other gateway is available
        		}
        	} else {
        		$gateway = new CCAvenueGateway(null, null, $ccNumber, $paymentType, $cardIssuer,$bankName);//fall back to ccavenue in case no other gateway is available
        	}
        }        
        return $gateway;        
	}
	
	public function getGateway(PaymentDO $paymentDO, $notWorkingGateways) {
		$paymentType = $paymentDO->getPaymentMethod();
		
		$gateway = null;
		switch($paymentType) {
			case PaymentType::Credit:
			case PaymentType::Debit:
				/* Flow for both Debit and Credit are same as of now */
				//ICICI, Axis, HDFC, CITI
				$gateway = $this->getCardGateway($paymentDO->getCardDO()->getNumber(), $paymentType);
				break;
					
			case PaymentType::NetBanking:
				
				$gateway = $this->getNetBankingGateway($paymentDO->getBankId(), $notWorkingGateways);
				break;
				
			case PaymentType::EMI:
				$emiBank = $paymentDO->getEMIBank();
				$gateway = $this->getEMIGateway($paymentDO->getCardDO()->getNumber(), $paymentType,$emiBank);
				break;
				
			case PaymentType::COD:
				$gateway = new CODGateway();
				break;
				
			case PaymentType::IVR:
				$gateway = new AtomGateway();
				break;
				
			case PaymentType::CashCard:
			default:
				//TODO: check with Mitesh: what is gatewayCode ??? // this condition should not occur probable exception is better.
				throw new NoSuitableGatewayException("Could not fetch gateway default condition reached in payment gateway factory", $paymentDO);
				/*$bankid = $paymentDO->getBankId();
				if(!empty($bankid)) {
					$gateway = $this->getNetBankingGateway($paymentDO->getBankId(), $notWorkingGateways);
				} else {
					$gateway =  new CCAvenueGateway(null, null, null, PaymentType::Credit,null,null);	
				}*/				
				break;	
		}
		
		return $gateway;
	}
	
	
	/*public function getGateway($paymentType, $ccNumber, $bankid, $notWorkingGateways) {
		$gateway = null;
		switch($paymentType) {
			case PaymentType::Credit:
			case PaymentType::Debit:
				// Flow for both Debit and Credit are same as of now
				//ICICI, Axis, HDFC
				$gateway = $this->getCardGateway($ccNumber);
				break;
					
			case PaymentType::NetBanking:
				$gateway = $this->getNetBankingGateway($bankid, $notWorkingGateways);
				break;
				
			case PaymentType::COD:
				$gateway = new CODGateway();
				break;
				
			case PaymentType::IVR:
				$gateway = new AtomGateway();
				break;
				
			case PaymentType::CashCard:
			default:
				return new CCAvenueGateway($bankid, $gatewayCode, $ccNumber, $paymentType);
				break;	
		}
		
		return $gateway;
	}*/
	
	
	private function determineCardType($number) {
		// Based on code from here 
		// http://mel-green.com/2008/11/determine-credit-card-type-with-javascript/		
		if(preg_match('/^4/', $number)) {
			return CardType::VISA;
		} elseif(preg_match('/^5[1-5]/', $number)) {
			return CardType::MasterCard;
		} elseif(preg_match('/^3(?:0[0-5]|[68])/', $number)) {
			return CardType::Diners;
		} elseif(preg_match('/^(34|37)/', $number)) {
			return CardType::AmEx;
		} else {
			return CardType::Maestro; //default should be maestro otherwise icici maestro cards will fail
		}
	}
	
}
?>
