<?php

namespace revenue\payments\service;
use abtest\MABTest;
include_once "$xcart_dir/modules/coupon/database/CouponAdapter.php";
include_once "$xcart_dir/modules/myntCash/MyntCashService.php";
include_once "$xcart_dir/modules/myntCash/MyntCashRawService.php";


class PaymentServiceInterface {
	
	public static function addBank($name, $isPopular) {
		$baseURL= \HostConfig::$paymentServiceInternalHost."/admin";
		$postdata = array();
		$postdata["name"] = $name;
		$postdata["is_popular"] = $isPopular;
		$postdata = json_encode($postdata);
		$url = $baseURL."/netbanking/banks/add";
		PaymentServiceInterface::performPostCurlCall($url,$postdata);

	}

	public static function addGateway($name, $weight) {
		$baseURL= \HostConfig::$paymentServiceInternalHost."/admin";
		$postdata = array();
		$postdata["name"] = $name;
		$postdata["weight"] = intval($weight);
		$postdata = json_encode($postdata);
		$url = $baseURL."/netbanking/gateways/add";
		PaymentServiceInterface::performPostCurlCall($url,$postdata);

	}

	public static function updateBanks($id, $name, $isPopular) {
		$baseURL= \HostConfig::$paymentServiceInternalHost."/admin";
		$postdata = array();
		$postdata["name"] = $name;
		$postdata["is_popular"] = $isPopular;
		$postdata = json_encode($postdata);
		$url = $baseURL."/netbanking/banks/update/".$id;
		PaymentServiceInterface::performPostCurlCall($url, $postdata);
	}

	public static function updateGateways($id, $name, $weight){
		$baseURL= \HostConfig::$paymentServiceInternalHost."/admin";
		$postdata = array();
		$postdata["name"] = $name;
		$postdata["weight"] = intval($weight);
		$postdata = json_encode($postdata);
		$url = $baseURL."/netbanking/gateways/update/".$id;
		PaymentServiceInterface::performPostCurlCall($url, $postdata);
	}

	public static function deleteBank($bankId){
		$baseURL= \HostConfig::$paymentServiceInternalHost."/admin";
		$url = $baseURL."/netbanking/banks/delete/".$bankId;
		PaymentServiceInterface::performDeleteCurlCall($url);
	}

	public static function deleteGateway($gatewayId){
		$baseURL= \HostConfig::$paymentServiceInternalHost."/admin";
		$url = $baseURL."/netbanking/gateways/delete/".$gatewayId;
		PaymentServiceInterface::performDeleteCurlCall($url);
	}

	public static function listBanks(){
		$baseURL= \HostConfig::$paymentServiceInternalHost."/admin";
		$url = $baseURL."/netbanking/banks/list/";
		$banksList = PaymentServiceInterface::performGetCurlCall($url);
		return $banksList;
	}

	public static function listGateways(){
		$baseURL= \HostConfig::$paymentServiceInternalHost."/admin";
		$url = $baseURL."/netbanking/gateways/list/";
		$banksList = PaymentServiceInterface::performGetCurlCall($url);
		return $banksList;
	}

	public static function activateMapping($mappingId){
		$baseURL= \HostConfig::$paymentServiceInternalHost."/admin";
		$url = $baseURL."/netbanking/gateway-mappings/activate/".$mappingId;
		PaymentServiceInterface::performPostCurlCall($url);
	}

	public static function deactivateMapping($mappingId){
		$baseURL= \HostConfig::$paymentServiceInternalHost."/admin";
		$url = $baseURL."/netbanking/gateway-mappings/deactivate/".$mappingId;
		PaymentServiceInterface::performPostCurlCall($url);
	}

	public static function getAllMapping(){
		$baseURL= \HostConfig::$paymentServiceInternalHost."/admin";
		$url = $baseURL."/netbanking/gateway-mappings/list/";
		return PaymentServiceInterface::performGetCurlCall($url);
	}

	public static function activateAllForBank($bank_id){
		$baseURL= \HostConfig::$paymentServiceInternalHost."/admin";
		$url = $baseURL."/netbanking/gateway-mappings/activateAllGateways/".$bank_id;
		return PaymentServiceInterface::performPostCurlCall($url);
	}

	public static function activateAll(){
		$baseURL= \HostConfig::$paymentServiceInternalHost."/admin";
		$url = $baseURL."/netbanking/gateway-mappings/activateAllMappings";
		return PaymentServiceInterface::performPostCurlCall($url);
	}

	public static function getUsedGatewaysForBank($bank_id){
		$baseURL= \HostConfig::$paymentServiceInternalHost."/admin";
		$url = $baseURL."/netbanking/gateway-mappings/bank/list/".$bank_id;
		return PaymentServiceInterface::performGetCurlCall($url);
	}

	public static function getUsedBanksForGateway($gateway_id){
		$baseURL= \HostConfig::$paymentServiceInternalHost."/admin";
		$url = $baseURL."/netbanking/gateway-mappings/gateway/list/".$gateway_id;
		return PaymentServiceInterface::performGetCurlCall($url);
	}

	public static function addMapping($bank_id, $gateway_id, $gateway_bank_code_new){
		$baseURL= \HostConfig::$paymentServiceInternalHost."/admin";
		$postdata = array();
		$postdata["bank_id"] = intval($bank_id);
		$postdata["gateway_id"] = intval($gateway_id);
		$postdata["gateway_code"] = $gateway_bank_code_new;
		$postdata = json_encode($postdata);
		//print_r($postdata); exit;
		$url = $baseURL."/netbanking/gateway-mappings/add";
		PaymentServiceInterface::performPostCurlCall($url,$postdata);
	}

	public static function deleteMapping($mapping_id){
		$baseURL= \HostConfig::$paymentServiceInternalHost."/admin";
		$url = $baseURL."/netbanking/gateway-mappings/delete/".$mapping_id;
		PaymentServiceInterface::performDeleteCurlCall($url);
	}

	public static function updateMappingGatewayCode($mapping_id, $new_gateway_code){
		$baseURL= \HostConfig::$paymentServiceInternalHost."/admin";
		$postdata = array();
		$postdata["id"] = intval($mapping_id);
		$postdata["gateway_code"] = $new_gateway_code;
		$postdata = json_encode($postdata);
		$url = $baseURL."/netbanking/gateway-mappings/update/";
		PaymentServiceInterface::performPostCurlCall($url, $postdata);
	}
	
	public static function getPaymentLog($orderId){
		$baseURL= \HostConfig::$paymentServiceInternalHost."/admin/paymentlog/";
		$url = $baseURL.$orderId;
		return PaymentServiceInterface::performGetCurlCall($url);
	}
	
	public static function updatePaymentLog($orderId, $postdata){
		$baseURL= \HostConfig::$paymentServiceInternalHost."/admin/paymentlog";
		$url = $baseURL."/update/".$orderId;
		$postdata = json_encode($postdata);
		PaymentServiceInterface::performPostCurlCall($url, $postdata);
	}

	public static function performPostCurlCall($url, $postdata){
		global $login;
		$ch = curl_init($url);
		curl_setopt($ch,CURLOPT_HTTPHEADER,array('user:'.$login,'Content-Type: application/json'));
		curl_setopt($ch, CURLOPT_POST      ,1);
		curl_setopt($ch, CURLOPT_POSTFIELDS    ,$postdata);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION  ,1);
		curl_setopt($ch, CURLOPT_HEADER      ,0);  // DO NOT RETURN HTTP HEADERS
		curl_setopt($ch, CURLOPT_RETURNTRANSFER  ,1); // RETURN THE CONTENTS OF THE CALL
		curl_setopt($ch,CURLOPT_TIMEOUT,30); //30 sec time out
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,0);

		$response = curl_exec($ch);
		curl_close($ch);
		return $response;
	}

	public static function performDeleteCurlCall($url){
		global $login;
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
		curl_setopt($ch,CURLOPT_HTTPHEADER,array('user:'.$login));
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch,CURLOPT_TIMEOUT,30); //30 sec time out
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,0);

		$response = curl_exec($ch);
		curl_close($ch);
		return $response;
	}

	public static function performGetCurlCall($url){
		global $login;
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
		curl_setopt($ch,CURLOPT_HTTPHEADER,array('user:'.$login));
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch,CURLOPT_TIMEOUT,30); //30 sec time out
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,0);
		$response = curl_exec($ch);
		curl_close($ch);
		return $response;
	}

	public static function getPaymentServiceEnabledForUser($login){
		$_MABTestObject = MABTest::getInstance();
		
		$paymentServiceEnabled = \FeatureGateKeyValuePairs::getBoolean('payments.serviceEnabled');
		$paymentServiceEnabledForAll = \FeatureGateKeyValuePairs::getBoolean('payments.serviceEnabledForAll');

		if($paymentServiceEnabled) {
			if(!$paymentServiceEnabledForAll){
				$paymentServiceEnabledForUsers = \FeatureGateKeyValuePairs::getStrArray('payments.serviceEnabledForUsers');
				$paymentServiceEnabled = in_array($login, $paymentServiceEnabledForUsers);
				if(!$paymentServiceEnabled){
					$paymentServiceABTest=$_MABTestObject->getUserSegment("paymentServiceEnabledAB");
					if($paymentServiceABTest == 'test'){
						$paymentServiceEnabled = true;
					}
				}	
			}
		}
		return $paymentServiceEnabled;
	}

	public static function getExpressBuyEnabledForUser($login){
		$cookieSet=getMyntraCookie('__expressCookie__');
		global $XCART_SESSION_VARS;

		if(!empty($cookieSet) || empty($XCART_SESSION_VARS['express_buy_enabled'])){
			//call service to get the value
			$url = \HostConfig::$paymentServiceInternalHost.'/admin/userpreference/list/'.$login.'/oneclick_enable';
 			
 			$response =PaymentServiceInterface::performGetCurlCall($url);
			$responseBody = json_decode($response);
			if($responseBody->preferenceValue == 'true'){
				if(x_session_is_registered("express_buy_enabled")){
					 x_session_unregister("express_buy_enabled");
				}
				x_session_register('express_buy_enabled','true');
				deleteMyntraCookie('__expressCookie__');
				return true;
			}
			else if ($responseBody->preferenceValue == 'false') {
				deleteMyntraCookie('__expressCookie__');
				if(x_session_is_registered("express_buy_enabled")){
					 x_session_unregister("express_buy_enabled");
				}
				x_session_register('express_buy_enabled','false');
			}

		}
		else {
			if($XCART_SESSION_VARS['express_buy_enabled']=='true'){
				return true;
			}
		}

		return false;
	}
        
    public static function showExpressBuyForUser($login){
    	global $skin;
       	$PCIDSSCompliant = $skin == 'skinmobile' ? \FeatureGateKeyValuePairs::getBoolean('payments.service.mobile-PCI-DSS-Compliant')
       						: (\FeatureGateKeyValuePairs::getBoolean('payments.service.PCI-DSS-Compliant') || in_array($login, \FeatureGateKeyValuePairs::getStrArray('payments.savedcards.override')));
       	if($PCIDSSCompliant) {
			$paymentServiceEnabledForUsers = PaymentServiceInterface::getPaymentServiceEnabledForUser($login);
			if($paymentServiceEnabledForUsers) {
				//Check AB  Test/Feature Gate/Session Value
				//get Feature Gate here
				$expressFeatureOn=\FeatureGateKeyValuePairs::getBoolean('payments.expressbuy.enabled');
				if($expressFeatureOn){
					//get AB test here
					$_MABTestObject = MABTest::getInstance();
					$expressABTest=$_MABTestObject->getUserSegment("ExpressBuyABTest");
					if($expressABTest == 'test'){
						$enabled=PaymentServiceInterface::getExpressBuyEnabledForUser($login);
						if($enabled){
							return true;
						}
					}
				}
			}
		}
		return false;
    }

    public static function isPaymentPage(){
    	$curPage=strtok($_SERVER['REQUEST_URI'],'?');
     	if($curPage=='/mkpaymentoptions.php' || $curPage=='/mkpaymentoptions_re.php' || $curPage == '/expressbuy.php' || $curPage=='/mkgiftcardpaymentoptions.php'){
    		return true;
    	}
    	return false;
    }
    
    public static function showLoginPromptForUser($login) {
//     	
    	//$secureLoginEnabled=\FeatureGateKeyValuePairs::getBoolean('secure.login');
    	$secureLoginFeatureGate=\FeatureGateKeyValuePairs::getBoolean('secure.login');
		$_MABTestObject = MABTest::getInstance();
		$secureLoginABTest=$_MABTestObject->getUserSegment("secureSignin");
		if($secureLoginABTest == 'secure' && $secureLoginFeatureGate){
			$secureLoginEnabled = true; 
		}
		else{
			$secureLoginEnabled = false; 
		}

    	$paymentServiceEnabled=\FeatureGateKeyValuePairs::getBoolean('payments.serviceEnabled');
    	$paymentServiceEnabledForUser = PaymentServiceInterface::getPaymentServiceEnabledForUser($login);
    	if($secureLoginEnabled && $paymentServiceEnabled && $paymentServiceEnabledForUser){
    		
    		$loginPromptAlways=\FeatureGateKeyValuePairs::getBoolean('payments.login.always-prompt');
    		if($loginPromptAlways){
    			return true;
    		}
    		$showLoginPromptIfUserHasSavedCard=\FeatureGateKeyValuePairs::getBoolean('payments.login.savedcard-prompt');
    	
    		if($showLoginPromptIfUserHasSavedCard && PaymentServiceInterface::userHasSavedCard($login)){
    			return true;
    		}
    		$showLoginPromptIfUserHasCashBack=\FeatureGateKeyValuePairs::getBoolean('payments.login.cashback-prompt');
    		if($showLoginPromptIfUserHasCashBack && PaymentServiceInterface::userHasCashBack($login)){
    			return true;
    		}
    	}
    	return false;
    }
    
    public static function userHasSavedCard($login){
    	$baseURL= \HostConfig::$paymentServiceInternalHost;
    	$url = $baseURL."/admin/paymentInstruments/hasSavedCards/".$login;
    	$result = json_decode(PaymentServiceInterface::performGetCurlCall($url));
    	if($result->status == 'success'){
    		return true;
    	}
    	return false;
    }
    
    public static function userHasCashBack($login){
    	/*$couponAdapter = \CouponAdapter::getInstance();
    	$cashbackCoupon = $couponAdapter->getCashbackCoupon($login);
    	$cashback_balance = (empty($cashbackCoupon)) ? '' : $cashbackCoupon['MRPAmount'];
    	if(!empty($cashback_balance)){
    		return true;
    	}*///OLD CODE

    	$myntCashDetails = \MyntCashRawService::getBalance($login);
    	if(!empty($myntCashDetails["balance"]) && ($myntCashDetails["balance"] > 0)){
    		return true;
    	}
		return false;

    }
}

?>
