<?php

namespace revenue\payments\gateway;

use enums\HDFCResponseStatus;
use enums\HDFCResult;
use enums\Gateways;

class HDFCBankGateway extends CreditDebitCardGateway {
	
	private static $id = 90002250;
	private static $pwd = "password1";
	
	
	public function getName() {
		return Gateways::HDFCBank;
	}
	
	public function isPaymentInline() {
		return false;
	}
	
	public static function ismyPG()
	{
		$rAddr = getenv('REMOTE_ADDR');
		if($rAddr == "221.134.101.174" || $rAddr == "221.134.101.169") return true;
		
		return false;
	}
	
	public function getFormToSubmit($orderId, $amount, $extra) {
        $url ="https://securepgtest.fssnet.co.in/pgway/servlet/PaymentInitHTTPServlet";
        
        $toLog = array();
        $toLog['orderid'] = $orderId;
        if($extra['configMode']=="local"||$extra['configMode']=="test"||$extra['configMode']=="release") {
			if($extra['configMode']=="release"){
				$toLog['orderid'] = substr($orderId, 5); //"RELE_".$orderid;
			} else {
				$toLog['orderid'] = substr($orderId, 5); //"ALPHA".$orderid
			}		
		}
		
        $hdfcBankParameter = array();
        $hdfcBankParameter['id'] = HDFCBankGateway::$id;
        $hdfcBankParameter['password'] = HDFCBankGateway::$pwd;
        $hdfcBankParameter['action'] = 1;
        $hdfcBankParameter['langid'] = "USA";
        $hdfcBankParameter['currencycode'] = 356;
        $hdfcBankParameter['amt'] = $amount;
        //$hdfcBankParameter['responseURL'] = $this->getHttpsLocation()."/mkHDFCPGSuccess.php"; //in case success
        $hdfcBankParameter['responseURL'] = $this->http_loc."/mkPGSuccess.php"; //in case success
        //$hdfcBankParameter['responseURL'] = 'http://122.166.122.123/sgms/hdfc_success_test.html';
        $hdfcBankParameter['errorURL'] = $this->http_loc."/mkorderBook.php"; //in case of failure
        $hdfcBankParameter['trackid'] = $orderId;
        
        /* 	User Defined Fileds as per Merchant or bank requirment. Merchant MUST ensure merchant 
			merchant is not passing junk values OR CRLF in any of the UDF. In below sample UDF values 
			are not utilized */
        $card = $extra['cardDetails'];
        $bAddress = $card->billingAddress;
        //$bAddress = new BillToAddress(); 
        $hdfcBankParameter['udf1'] = $extra['saleDetail'];
        $hdfcBankParameter['udf2'] = $bAddress->mstrEmail;
        $hdfcBankParameter['udf3'] = $bAddress->mstrPhone;
        $hdfcBankParameter['udf4'] = $extra['session_name'];
        $hdfcBankParameter['udf5'] = $extra['session_id']; //session_id - extra parameter
        
        
		$param = "";
		$isFirst = true;
		foreach ($hdfcBankParameter as $key=>$value) {
			if($isFirst) $isFirst = false;
			else $param = $param . "&"; 
				
			$param = $param . $key . "=" . $value;
		} 
		
		echo "<BR> All Params <BR> ". $param . "<BR>";
		
        $ch = curl_init() or die(curl_error()); 
		curl_setopt($ch, CURLOPT_POST,1); 
		curl_setopt($ch, CURLOPT_POSTFIELDS,$param); 
		curl_setopt($ch, CURLOPT_PORT, 443); // port 443
		curl_setopt($ch, CURLOPT_URL,$url);// here the request is sent to payment gateway 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,0); //create a SSL connection object server-to-server
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,0); 
		$data1=curl_exec($ch) or die(curl_error());
		
		global $weblog;
		global $errorlog;
		curl_close($ch); 
		$response = $data1;
        try
		{
			$index=strpos($response,"!-");
			$ErrorCheck=substr($response, 1, $index-1);//This line will find Error Keyword in response
			if($ErrorCheck == 'ERROR')//This block will check for Error in response
			{
				$failedurl = $this->http_loc.'/mkorderBook.php?';
				//header("location:". $failedurl );
				$this->formAction = $failedurl;
				$this->postParams = array();
				$this->postParams['message'] = "PAYMENT FAILED ('.$response.' )";
				$this->postParams['orderid'] = $orderId; 

				$toLog['action'] = $hdfcBankParameter['action'];
				$toLog['err_message'] = $this->postParams['message'];
				$toLog['status'] = HDFCResponseStatus::Forwarded;
				func_array2insert('mk_hdfc_payments_log', $toLog);
				
				//echo "<BR> the generated HTML <BR> ".parent::generateHTML();
				return parent::generateHTML();
			}
			else
			{
				// If Payment Gateway response has Payment ID & Pay page URL		
				$i =  strpos($response,":");
				// Merchant MUST map (update) the Payment ID received with the merchant Track Id in his database at this place.
				$paymentId = substr($response, 0, $i);
				$paymentPage = substr( $response, $i + 1);
				// here redirecting the customer browser from ME site to Payment Gateway Page with the Payment ID
				$this->formAction = $paymentPage;
				$this->redirectingMessage = "Please wait... while redirecting to HDFC payment gateway. It may take some time";
				$this->postParams = array();
				$this->postParams['PaymentID'] = $paymentId;
				
				//store this payment id along with other statuses
				$toLog['status'] = HDFCResponseStatus::Forwarded;
				$toLog['paymentid'] = $paymentId;
				$toLog['paymentURL'] = $paymentPage;
				$toLog['action'] = $hdfcBankParameter['action'];
				func_array2insert('mk_hdfc_payments_log', $toLog);

				return parent::generateHTML();
			}
		}
		catch(Exception $e)
		{
			$weblog->error($e->getMessage());
			$errorlog->error($e->getMessage());
		}
        
		return null;
	}
	
	private static function validateDualResponse($p, $x) {
		foreach ($p as $k => $v) {
			eval('if($v!=$x->'.$k.') return false;');
		}
		return true;
	}
	
	public static function handleResponse($orderid, $params, $ipAddress, $amountToBePaid, $https_location) {
		
		global $weblog;
                global $errorlog;
		
		$getParams = $params['udf4'] . '=' . $params['udf5'];
		
		try{
			
		$weblog->debug("IN hendleResponse 1");
		$weblog->debug('AMountToBePaid 6 '. $amountToBePaid);
			$toLog = array();
			$toLog['responseText'] = mysql_escape_string(print_r($params, true));
			$toLog['fromIP'] = mysql_escape_string($ipAddress);
			$toLog['errorCode'] = $params['Error'];
			$toLog['errorText'] = $params['ErrorText'];
			$toLog['orderAmount'] = $amountToBePaid;
			$toLog['status'] = HDFCResponseStatus::Error;//Payment failed
		$weblog->debug("IN hendleResponse 2");	
			$whereClause = '';
			
			if(HDFCBankGateway::ismyPG()) {
				$paymentId = $params['paymentid'];
				$orderId = $orderid;
				$amount = $params['amt'];
		$weblog->debug("IN hendleResponse 3");		
				$whereClause = " orderid=" .$orderId. " and paymentid=".$paymentId;
				
				if($toLog['errorCode']=='') {
					//store the response into the db	
					$toLog['result'] = $params['result'];
					$toLog['auth'] = $params['auth'];
					$toLog['avr'] = $params['avr'];
					$toLog['ref'] = $params['ref'];
					$toLog['tranid'] = $params['tranid'];
					$toLog['postdate'] = $params['postdate'];
					$toLog['approvedAmount'] = $amount;
					
					if($amount > $amountToBePaid - 1) {
						$retValue['isPaymentDataCorrect'] = true;
					}
					if($params['result']==HDFCResult::Captured || $params['result']==HDFCResult::Approved) {
				   		$ReqString = "<id>".HDFCBankGateway::$id."</id><password>".HDFCBankGateway::$pwd."</password><action>8</action><transid>".$params['tranid']."</transid>";
				   
					    $DualReqURL = "https://securepgtest.fssnet.co.in/pgway/servlet/TranPortalXMLServlet";
				   $weblog->debug("IN hendleResponse 4");
						$dvreq = curl_init() or die(curl_error()); 
					    curl_setopt($dvreq, CURLOPT_POST,1); 
					    curl_setopt($dvreq, CURLOPT_POSTFIELDS,$ReqString); 
					    curl_setopt($dvreq, CURLOPT_URL,$DualReqURL); 
					    curl_setopt($dvreq, CURLOPT_PORT, 443);
					    curl_setopt($dvreq, CURLOPT_RETURNTRANSFER, 1); 
					    curl_setopt($dvreq, CURLOPT_SSL_VERIFYHOST,0); 
					    curl_setopt($dvreq, CURLOPT_SSL_VERIFYPEER,0); 
					    $dataret=curl_exec($dvreq) or die(curl_error());
					    curl_close($dvreq); 
$weblog->debug("IN hendleResponse 5");
					    $DVresponse = $dataret;
					    $GEnXMLForm="<xmltg>".$DVresponse."</xmltg>";
					    $xmlSTR = simplexml_load_string( $GEnXMLForm,null,true);

					    if(!HDFCBankGateway::validateDualResponse($params,$xmlSTR)) {
					    	$weblog->debug("IN hendleResponse 6");
					    	$toLog['errorCode'] = isset($params['Error'])?$params['Error']:'';
					    	$toLog['errorText'] = 'Payment failed in Dual Verification - Dual verification response : ' . mysql_escape_string($xmlSTR);
					    	$toLog['status'] = HDFCResponseStatus::DualVerificationFailed; //Dual Verification Failed
					    	$getParams = '&orderid=' . $orderid .'&paymentid=' . $hdfcPaymentData['paymentid'] . '&status=' . HDFCResponseStatus::DualVerificationFailed;
					    } else {
					    	$weblog->debug("IN hendleResponse 7");
					    	//$retValue['isPaymentCompete'] = true;
					    	$toLog['action'] = 8; //Dual Verification Succeded
							$toLog['status'] = HDFCResponseStatus::DualVerificationSucceeded;
					    	$getParams = '&orderid=' . $orderid .'&paymentid=' . $hdfcPaymentData['paymentid'] . '&status=' . HDFCResponseStatus::DualVerificationSucceeded;		    	
					    }
					} else { //result is neither captured nor approved
						$weblog->debug("IN hendleResponse 8");
						$toLog['errorText'] = 'Payment is neither Captured nor Approved';
						$toLog['status'] = HDFCResponseStatus::PaymentDeclined;
						$getParams = '&orderid=' . $orderid .'&paymentid=' . $hdfcPaymentData['paymentid'] . '&status=' . HDFCResponseStatus::PaymentDeclined;
					}
				} else { //error in the result
					$weblog->debug("IN hendleResponse 9");
					$toLog['errorCode'] = $params['Error'];
					$toLog['errorText'] = $params['ErrorText'];
					$getParams = '&status=' . HDFCResponseStatus::Error;
				}
				$weblog->debug("IN hendleResponse 10");
				func_array2update('mk_hdfc_payments_log', $toLog, $whereClause);
			} else { //ipaddress verification failed
				$weblog->debug("IN hendleResponse 11");
				$toLog['errorText'] = "IP Address mismatch";
				$toLog['status'] = HDFCResponseStatus::IPMismatch;
				func_array2insert('mk_hdfc_payments_log', $toLog);
				$getParams = '&orderid=' . $orderid .'&status=' . HDFCResponseStatus::IPMismatch;
			}
		} catch (Exception $e) {
			$weblog->debug("IN hendleResponse 12");
			$weblog->error($e->getMessage());
			$errorlog->error($e->getMessage());
			$getParams = '&orderid=' . $orderid .'&status=' . HDFCResponseStatus::Exception;
		}
		$weblog->debug("IN hendleResponse 13");
		echo 'REDIRECT=' . $https_location . "/mkorderBook.php?" . $getParams;
	}
}

?>