<?php

namespace revenue\payments\gateway;

use enums\revenue\payments\PaymentType;
use enums\revenue\payments\Gateways;

class CODGateway extends PaymentGateway {

	public function getFormToSubmit($orderId, $amount, $extra) {
		$this->formAction = $this->getHttpsLocation()."/mkorderBook.php?type=cod&orderid=".$orderId ."&".$extra['session_name']."=".$extra['session_id'];
        $this->redirectingMessage ="Please wait... while we process your order";
        $query = "update xcart_orders set cod_pay_status='pending' where orderid = '$orderId'";
        db_query($query);
        return parent::generateHTML();
	}
	
	public function verifyPaymentData($returnedData, $amountToBepaid, $prevStatus) {
		$this->setAmountPaid($amountToBepaid);
		$this->setAmountToBePaid($amountToBepaid);
		if($prevStatus == 'PV'||$prevStatus == 'D'||$prevStatus == 'PP'){
			$this->setPaymentDataCorrect(true);
			$this->setPaymentCompleted(true);
			return $this->validatePayment();
		} else {
			throw new \Exception("Invalid Order State", 500);
		}
	}
	
	public function isPaymentInline() {
		return true;
	}
	
	public function getName() {
		return Gateways::COD;
	}
	
	public function getPaymentType() {
		return PaymentType::COD;
	}
	
	public function getBankName() {
		return null;
	}
	
	public function getCardBin() {
		return null;
	}
	
	public function getPaymentIssuer() {
		return null;
	}
}
?>