<?php
namespace revenue\payments\gateway;

use enums\revenue\payments\CardType;
use enums\revenue\payments\PaymentType;
use enums\revenue\payments\Gateways;

class AxisBankGateway extends CreditDebitCardGateway {
	
	const SECURE_SECRET = "5A0FEEDF45C6B00190DF81EB6E8A149E";
	
	public function getName() {
		return Gateways::AxisBank;
	}
	
	public function getFormToSubmit($orderId, $amount, $extra) {
		$Redirect_Url = $this->getHttpsLocation()."/mkorderBook.php?orderid=".$orderId."&".$extra['session_name']."=".$extra['session_id']."&utm_source_confirm=".$extra['utm_source']."&utm_medium_confirm=".$extra['utm_medium']."";
		$this->formAction ="https://migs.mastercard.com.au/vpcpay";
		$this->redirectingMessage = "Redirecting to Verify by Visa / Master Card Secure";
		$cardDetails = $extra['cardDetails'];
		$billingAddress = $extra['billAddress'];
		$configMode = $extra['configMode'];
		
        $this->postParams = array();
		$this->postParams['vpc_Amount'] = $amount * 100;
		
		$expiryDate = $cardDetails->getExpiryYear().$cardDetails->getExpiryMonth(); 
		if(empty($expiryDate)) {
			$expiryDate = "4912";
		}
		$this->postParams['vpc_CardExp'] = $expiryDate;
		$this->postParams['vpc_CardNum'] = $cardDetails->getNumber();//5123456789012346/4557012345678902

		$cvvNumber = $cardDetails->getCvvNumber();
		if(!empty($cvvNumber)) {
			$this->postParams['vpc_CardSecurityCode'] = $cvvNumber; //optional
		}
		
		$this->postParams['vpc_Command'] = "pay";
		$this->postParams['vpc_Locale'] = "en";
		/*if($configMode=="local"||$configMode=="test"||$configMode=="release"){
			$this->postParams['vpc_Merchant'] = "TESTMYNTRA1";
			$this->postParams['vpc_AccessCode'] = "8A240123";
		} else {*/
			$this->postParams['vpc_Merchant'] = "MYNTRA1";
			$this->postParams['vpc_AccessCode'] = "6D6F7CD9";
		//}
		if($configMode=="local"||$configMode=="test"||$configMode=="release") {
			if($configMode=="release") {
				$this->postParams['vpc_MerchTxnRef'] = "RELE_".$orderId; //orderid
			} else if($configMode=="local") {
				$this->postParams['vpc_MerchTxnRef'] = "LOCAL".$orderId; //orderid
			} else {
				$this->postParams['vpc_MerchTxnRef'] = "ALPHA".$orderId; //orderid
			}
		} else {
			$this->postParams['vpc_MerchTxnRef'] = $orderId; //orderid
		}
		$this->postParams['vpc_OrderInfo'] = $orderId;
		$this->postParams['vpc_ReturnURL'] = $Redirect_Url;
		$this->postParams['vpc_Version'] = "1";
		if($this->getPaymentIssuer()==CardType::VISA) {
			$this->postParams['vpc_card'] = "Visa";
		} else {
			$this->postParams['vpc_card'] = "Mastercard"; //Visa/Mastercard/Amex
		}
		
		$this->postParams['vpc_gateway'] = "ssl"; //ssl/threeDSecure
		if($billingAddress != NULL){
		$this->postParams['vpc_AVS_Street01'] = substr($billingAddress->getAddress(),0,128);
		$this->postParams['vpc_AVS_City']=substr($billingAddress->getCity(),0,128);
		$this->postParams['vpc_AVS_StateProv']=substr($billingAddress->getState(),0,128);
		$this->postParams['vpc_AVS_PostCode'] = substr($billingAddress->getZipcode(),0,9);
		
		$billing_ISO3country = getISO3CountryCode($billingAddress->getCountry());
		if(empty($billing_ISO3country)) {
			$billing_ISO3country = "IND";
		}
		}
		$secureHash = $this->hashAllFields($this->postParams);
		$this->postParams['vpc_SecureHash'] = $secureHash;
		return parent::generateHTML();
	}
	
	public function verifyPaymentData($gatewayResponse, $amountToBepaid, $prevStatus) {
		
		if($prevStatus!='PP') {
			throw new \Exception("Invalid Order State", 500);
		}
		
		if(empty($gatewayResponse)) {
			$this->setPaymentDataCorrect(true);
			$this->setPaymentCompleted(false);
			return $this->validatePayment();
		}
		
		$vpc_Txn_Secure_Hash = $this->null2unknown($gatewayResponse['vpc_SecureHash']);
		unset($gatewayResponse['vpc_SecureHash']);
		unset($gatewayResponse['orderid']);
		unset($gatewayResponse['xid']);
		unset($gatewayResponse['utm_source_confirm']);
		unset($gatewayResponse['utm_medium_confirm']);
		
		$vpc_txnResponseCode = $this->null2unknown($gatewayResponse['vpc_TxnResponseCode']);		
		$vpc_amount = $this->null2unknown($gatewayResponse['vpc_Amount']);	    	        
		
		$axisAmountPaid = $vpc_amount/100;
		
		$this->setAmountPaid($axisAmountPaid);
		$this->setAmountToBePaid($amountToBepaid);
		$this->setResponseCode($vpc_txnResponseCode);
		$this->setCompleteResponse($gatewayResponse);
		$this->setResponseMessage($gatewayResponse['vpc_Message']);
		$this->setBankTransactionID($gatewayResponse['vpc_AuthorizeId']);
		$this->setGatewayTransactionID($gatewayResponse['vpc_ReceiptNo']);
		//NOTE: If the vpc_TxnResponseCode in not a single character then
		//there was a Virtual Payment Client error and we cannot accurately validate
		//the incoming data from the secure hash.
		if(!empty($vpc_Txn_Secure_Hash) && strcasecmp($vpc_Txn_Secure_Hash, "No Value Returned") != 0 && strlen($vpc_txnResponseCode)==1) {
			//validate hash code now
			$secureHash = $this->hashAllFields($gatewayResponse);
			// Validate the Secure Hash (remember MD5 hashes are not case sensitive)
			if(strcasecmp($secureHash, $vpc_Txn_Secure_Hash)==0) {
				// Show this page as an error page if error condition
				if ($vpc_txnResponseCode === 0||$vpc_txnResponseCode === '0') {
					if($this->verifyPaidAmount($axisAmountPaid, $amountToBepaid)) {
						$this->setPaymentCompleted(true);
						$this->setPaymentDataCorrect(true);
					} else {
						$this->setPaymentCompleted(true);
						$this->setPaymentDataCorrect(false);
					}
				} else {
					$this->setPaymentCompleted(false);
					$this->setPaymentDataCorrect(true);
				}
			} else {
				//hash not matched DATA Tampered
				$this->setPaymentCompleted(false);
				$this->setPaymentDataCorrect(false);
			}
		} else {
			$this->setPaymentCompleted(false);
			$this->setPaymentDataCorrect(true);
		}		
		
		return $this->validatePayment();
	}
	
	
	/* This method is used in case of validate the transaction only when data is coming through server to server api calls
	 * Check sum match is ignored here.
	 * */
	public function verifyReconciledData($gatewayResponse, $amountToBepaid) {
		
		if(empty($gatewayResponse)) {
			$this->setPaymentDataCorrect(true);
			$this->setPaymentCompleted(false);
			return $this->validatePayment();
		}
		
		
		$vpc_txnResponseCode = $this->null2unknown($gatewayResponse['vpc_TxnResponseCode']);		
		$vpc_amount = $this->null2unknown($gatewayResponse['vpc_Amount']);

		$axisAmountPaid = $vpc_amount/100;
		
		$this->setAmountPaid($axisAmountPaid);
		$this->setAmountToBePaid($amountToBepaid);
		$this->setResponseCode($vpc_txnResponseCode);
		$this->setCompleteResponse($gatewayResponse);
		$this->setResponseMessage($gatewayResponse['vpc_Message']);
		$this->setBankTransactionID($gatewayResponse['vpc_AuthorizeId']);
		$this->setGatewayTransactionID($gatewayResponse['vpc_ReceiptNo']);
		
		if ($vpc_txnResponseCode === 0||$vpc_txnResponseCode === '0') {
			if($this->verifyPaidAmount($axisAmountPaid, $amountToBepaid)) {
				$this->setPaymentCompleted(true);
				$this->setPaymentDataCorrect(true);
			} else {
				$this->setPaymentCompleted(true);
				$this->setPaymentDataCorrect(false);
			}
		} else {
			$this->setPaymentCompleted(false);
			$this->setPaymentDataCorrect(true);
		}
		
		return $this->validatePayment();
		
	}
	
	/**
	* This method is for sorting the fields and creating an MD5 secure hash.
	*
	* @param fields is a map of all the incoming hey-value pairs from the VPC
	* @return string md5String is the hash being returned for comparison to the incoming hash
	*/
	private function hashAllFields($fields = array()) {
		// create a list and sort it
		ksort($fields);
		//print_r($fields);exit;
		$buf=self::SECURE_SECRET;
		// iterate through the list and add the remaining field values	
		foreach ($fields as $key=>$value){
			if(isset($value)) $buf = $buf.$value;
		}	
		
		$md5String = md5($buf);
		
		return $md5String;
	    
	} // end hashAllFields()

	/*
	 * This method takes a data String and returns a predefined value if empty
	 * If data Sting is null, returns string "No Value Returned", else returns input
	 *
	 * @param in String containing the data String
	 * @return String containing the output String
	 */
	private function null2unknown($in) {
		if (isset($in)) {
			return $in;
		} else {
			return "No Value Returned";
		}
	} // null2unknown()
	
}
?>