<?php

namespace revenue\payments\gateway;

use enums\revenue\payments\Gateways;

class TekProcessGateway extends NetBankingGateway {
	
	private function getPropertyData() {
		$https_location = $this->getHttpsLocation();
		if(empty($https_location)){
			global $https_location;
		}
		
	    $property = array("BillerId"=>"L1664",
	    					"ResponseUrl"=>"$https_location/mkorderBook.php",
	    					"CRN"=>"INR",
	    					"CheckSumKey"=>"5835571401KHXYYB",
	    					"CheckSumGenUrl"=>"https://www.tpsl-india.in/PaymentGateway/CheckSumRequest",
	    					"TPSLUrl"=>"https://www.tpsl-india.in/PaymentGateway/TransactionRequest.jsp");
		
		return $property;
	                
	}

	public function getFormToSubmit($orderID,$txnAmount,$extraParams) {
		$configMode = $extraParams['configMode'];
		$https_location = $this->getHttpsLocation();
		if(empty($https_location)){
			global $https_location;
		}
		$bankCode = $this->gatewayCode;
		$property_data_array=$this->getPropertyData();
		if($configMode=="release"){
				$orderID = "R".trim($orderID);
		} elseif($configMode=="local") {
			$orderID = "L".trim($orderID);
		} else if($configMode=="test") {
			$orderID = "T".trim($orderID);
		}
		
		$txtBillerId=$property_data_array["BillerId"];
		$txtResponseUrl=$property_data_array["ResponseUrl"];
		$txtCRN=$property_data_array["CRN"];
		$txtCheckSumKey=$property_data_array["CheckSumKey"];
		$CheckSumGenUrl=trim($property_data_array["CheckSumGenUrl"]);
		$TPSLUrl=$property_data_array["TPSLUrl"];
		
		//---Create String using property data 
		$txtBillerIdStr="txtBillerid=".trim($txtBillerId)."&";
		$txtResponseUrl="txtResponseUrl=".trim($txtResponseUrl)."&";
		$txtCRN="txtCRN=".trim($txtCRN)."&";
		$txtCheckSumKey="txtCheckSumKey=".trim($txtCheckSumKey);
						
		$Proper_string=$txtBillerIdStr.$txtResponseUrl.$txtCRN.$txtCheckSumKey;
		
		//Encrypting values
		$transaction = trim($orderID);
		$market = trim($orderID);
		$account = "1";
		$transaction_amount = trim($txnAmount);
		$bankCode = trim($bankCode);
		
		$txtVals = $transaction . $market . $account . $transaction_amount . $bankCode;
		$txt_encrypt = md5(base64_encode($txtVals));
		
		
		$txtForEncode = trim($txt_encrypt) . trim($property_data_array["CheckSumKey"]);
		$txtPostid = md5($txtForEncode);
		
		// Creating string for $Postid.
		$txtPostid="txtPostid=".$txtPostid; 
		
		$UserDataString="txtTranID=$transaction&txtMarketCode=$market&txtAcctNo=$account&txtTxnAmount=$transaction_amount&txtBankCode=$bankCode";
		$PostData=trim($UserDataString)."&".trim($Proper_string)."&".trim($txtPostid);

		$certFileLocation =  dirname(__FILE__) . '/keystoretechp.pem' ;
		//-----Curl main Function Start
 		//$PostData =array("txtTranID"=>$transaction,"txtMarketCode"=>$market,"txtAcctNo"=>$account,
 		//					"txtTxnAmount"=>$transaction_amount,"txtBankCode"=>$bankCode,"txtBillerid"=>$txtBillerId,
 		//					"txtResponseUrl"=>$txtResponseUrl,"txtCRN"=>$txtCRN,"txtCheckSumKey"=>$txtCheckSumKey,"txtPostid"=>$txtPostid);
		// INITIALIZE ALL VARS
		$ch = curl_init($CheckSumGenUrl);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, False);
		curl_setopt($ch, CURLOPT_CAINFO, $certFileLocation); //Setting certificate path
		curl_setopt ($ch, CURLOPT_SSLCERTPASSWD, 'changeit');
		curl_setopt($ch, CURLOPT_POST      ,1);
		curl_setopt($ch, CURLOPT_TIMEOUT  ,10); // Not required. Don't use this.Mitesh -- using this for safety
		curl_setopt($ch, CURLOPT_REFERER  ,"$https_location/mkorderinsert.php"); //Setting header URL: 
		curl_setopt($ch, CURLOPT_POSTFIELDS    ,$PostData);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION  ,1); 
		curl_setopt($ch, CURLOPT_HEADER      ,0);  // DO NOT RETURN HTTP HEADERS 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER  ,1); // RETURN THE CONTENTS OF THE CALL
		 
		$Received_CheckSum_Data = curl_exec($ch);
		curl_close($ch);
		
		$Received_CheckSum_Data = trim($Received_CheckSum_Data);
		if(empty($Received_CheckSum_Data)) return false;
		
		if(!is_numeric($Received_CheckSum_Data)){ // Validating whether the Receieved value is numeric.
			return false;		
		}
		
		//re-defining the variables without & and variables.
		
		$txtBillerIdStr=$txtBillerId;
		$txtResponseUrl=$property_data_array["ResponseUrl"];
		$txtCRN=$property_data_array["CRN"];
		$txtCheckSumKey=$property_data_array["CheckSumKey"];
		
		//################# read post data to variable ###############

		$txtTranID=$transaction;
		$txtMarketCode=$market;
		$txtAcctNo=$account;
		$txtTxnAmount=$transaction_amount;
		$txtBankCode=$bankCode;

		//################# read post data end ###############

		//###########  Create msg String ###########################
 		$ParamString=trim($txtBillerId)."|".trim($txtTranID)."|NA|NA|".trim($txtTxnAmount)."|".trim($txtBankCode)."|NA|NA|".trim($txtCRN)."|NA|NA|NA|NA|NA|NA|NA|".trim($txtMarketCode)."|".trim($txtAcctNo)."|NA|NA|NA|NA|NA|".trim($txtResponseUrl);

 		$finalUrlParam=$ParamString."|".$Received_CheckSum_Data; 

 		$this->formAction = $TPSLUrl;
 		$this->redirectingMessage = "Redirecting to Bank Site for Payment Processing";
	    $this->postParams = array();
		$this->postParams['msg'] = $finalUrlParam;

		return parent::generateHTML();		
	}
	
	public function verifyPaymentData($returnedData, $amountToBepaid, $prevStatus) {
		
		if($prevStatus != "PP") {
			throw new \Exception("Invalid Order State", 500);
		}
		$https_location = $this->getHttpsLocation();
		if(empty($https_location)){
			global $https_location;
		}
		
		if(!is_array($returnedData)) {
			$this->setPaymentCompleted(false);
			$this->setPaymentDataCorrect(true);
			return $this->validatePayment();
		}
		
		
		$billerID = trim($returnedData[0]);
		$orderID = trim($returnedData[1]);
		$tpslTransactionID = trim($returnedData[2]);
		$bankTransactionID = trim($returnedData[3]);
		$amountPaid = trim($returnedData[4]);
		$bankID = trim($returnedData[5]);
		$txnDate = trim($returnedData[13]);
		$authStatus = trim($returnedData[14]);
		$marketCode = trim($returnedData[16]);
		$account = trim($returnedData[17]);
		$errorDescription = trim($returnedData[24]);
		$checkSum = trim($returnedData[25]);
		
		if(!empty($txnDate)) {
			$this->setTransactionTime(strtotime($txnDate));
		}
		
		$this->setAmountPaid($amountPaid);
		$this->setAmountToBePaid($amountToBepaid);
		$this->setResponseCode($authStatus);
		$this->setCompleteResponse($returnedData);
		$this->setResponseMessage($errorDescription);
		$this->setBankTransactionID($bankTransactionID);
		$this->setGatewayTransactionID($tpslTransactionID);
		
		$property_data_array=$this->getPropertyData();
		$txtBillerId=$property_data_array["BillerId"];
		$txtResponseUrl=$property_data_array["ResponseUrl"];
		$txtCRN=$property_data_array["CRN"];
		$txtCheckSumKey=$property_data_array["CheckSumKey"];
		$CheckSumGenUrl=trim($property_data_array["CheckSumGenUrl"]);
		$TPSLUrl=$property_data_array["TPSLUrl"];
		$txtResponseKey = $returnedData[0] ."|".$returnedData[1] ."|".$returnedData[2] ."|".$returnedData[3] ."|".$returnedData[4] ."|".$returnedData[5] ."|".$returnedData[6] ."|".$returnedData[7] ."|".$returnedData[8] ."|".$returnedData[9] ."|".$returnedData[10] ."|".$returnedData[11] ."|".$returnedData[12] ."|".$returnedData[13] ."|".$returnedData[14] ."|".$returnedData[15] ."|".$returnedData[16] ."|".$returnedData[17] ."|".$returnedData[18] ."|".$returnedData[19] ."|".$returnedData[20] ."|".$returnedData[21] ."|".$returnedData[22] ."|".$returnedData[23] ."|".$returnedData[24] ."|".$txtCheckSumKey;	
		$txtResponseKey = "txtResponseKey=" . $txtResponseKey;
		
		$certFileLocation =  dirname(__FILE__) . '/keystoretechp.pem' ;
		
		$ch = curl_init($CheckSumGenUrl);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 1);
 		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, False);
 		curl_setopt($ch, CURLOPT_CAINFO, $certFileLocation); //Setting certificate path
 		curl_setopt ($ch, CURLOPT_SSLCERTPASSWD, 'changeit');
 		curl_setopt($ch, CURLOPT_POST      ,1);
		curl_setopt($ch, CURLOPT_TIMEOUT  ,20); // Not required. Don't use this. Mitesh -- using this for safety
 		curl_setopt($ch, CURLOPT_REFERER  ,"$https_location/mkorderBook.php"); //Setting header URL
 		curl_setopt($ch, CURLOPT_POSTFIELDS    ,$txtResponseKey);
 		curl_setopt($ch, CURLOPT_FOLLOWLOCATION  ,1); 
 		curl_setopt($ch, CURLOPT_HEADER      ,0);  // DO NOT RETURN HTTP HEADERS 
 		curl_setopt($ch, CURLOPT_RETURNTRANSFER  ,1); // RETURN THE CONTENTS OF THE CALL
 
		$Received_CheckSum_Data = curl_exec($ch);
		curl_close($ch);

		$Received_CheckSum_Data = trim($Received_CheckSum_Data);
		
		
		if(empty($Received_CheckSum_Data)) {
			//cant verify the validity of data so will fail the payment for now.
			$this->setPaymentCompleted(false);
			$this->setPaymentDataCorrect(true);
			return $this->validatePayment();
		}
		//echo "<br>CheckSum=".$Received_CheckSum_Data;
		if(!is_numeric($Received_CheckSum_Data)){ // Validating whether the Receieved value is numeric.
			//some error occured with checksum veryfication server.
			$this->setPaymentCompleted(false);
			$this->setPaymentDataCorrect(true);
			return $this->validatePayment();		
		}
		
		
		if($Received_CheckSum_Data == $checkSum) {
			if($authStatus=='0300' && $this->verifyPaidAmount($amountPaid, $amountToBepaid)) {
				//pass
				$this->setPaymentCompleted(true);
				$this->setPaymentDataCorrect(true);
			} else {
				//fail
				$this->setPaymentCompleted(false);
				$this->setPaymentDataCorrect(true);				
			}
		} else {
			//data tampered fraud :D
			$this->setPaymentCompleted(false);
			$this->setPaymentDataCorrect(false);
		}
		
		return $this->validatePayment();
		
	}
	
	public function getName() {
		return Gateways::tekProcess;
	}
	

}
?>