<?php
namespace revenue\payments\gateway;

use enums\revenue\payments\PaymentType;
use enums\revenue\payments\Gateways;

class AtomGateway extends PaymentGateway {
	
	public function getPaymentType() {
		return PaymentType::IVR;
	}
	
	public function getName() {
		return Gateways::Atom;
	}
	
	public function getFormToSubmit($orderId, $amount, $extra) {
		$this->formAction = $this->getHttpsLocation()."/mkphoneOrder.php?orderid=".$orderId;
    	$this->redirectmessage = "Please wait... while we process your order";
		return parent::generateHTML();
	}
	
	public function verifyPaymentData($returnedData, $amountToBepaid, $prevStatus) {		
		if($prevStatus == 'PV'||$prevStatus == 'D'||$prevStatus == 'PP'){
			$this->setPaymentDataCorrect(true);
			$this->setPaymentCompleted(false);
			return $this->validatePayment();
		} else {
			throw new \Exception("Invalid Order State", 500);
		}
	}
	
	public function isPaymentInline() {
		return false;
	}
	
	
	public function getBankName() {
		return null;
	}
	
	public function getCardBin() {
		return null;
	}
	
	public function getPaymentIssuer() {
		return null;
	}
}
?>