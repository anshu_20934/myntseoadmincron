<?php

namespace revenue\payments\gateway;

use order\dataobject\BillingAddressDO;

use enums\revenue\payments\PaymentType;
use enums\revenue\payments\Gateways;
use PaymentGatewayDiscount;

class CCAvenueGateway extends NetBankingGateway {
	
	private $paymentType;
	protected $cardNumber;
	protected $cardIssuer;
	private $Merchant_Id = "M_xmukesh_5506";
	private $WorkingKey = "bj7xvl403fy6qlfth4n1m7sjdgyzwy28";
	public function __construct($bankid, $gatewayCode, $ccNumber, $paymentType, $cardIssuer,$bankName) {
		$this->paymentType = $paymentType;
		if($paymentType==PaymentType::NetBanking)
			parent::__construct($bankid, $gatewayCode);
		else {
			$this->cardNumber = $ccNumber;
			$this->cardIssuer = $cardIssuer;
			$this->bankName = $bankName;
		}	
	}
		
	public function getName() {
		return Gateways::CCAvenue;
	}
	
	public function getPaymentType() {
		return $this->paymentType;	
	}
	
	public function getCardBin() {
		if(!empty($this->cardNumber)) return substr($this->cardNumber, 0,6);
		else return null;
	}
	
	public function getPaymentIssuer() {
		if(empty($this->cardIssuer)) {
			return $this->bankName;
		} else {
			return $this->cardIssuer;
		}
	} 
	
	public function getDiscountByCardNumber($total, $amountWithProductDiscount) {
		$bin = substr($this->cardNumber, 0,6);
		return PaymentGatewayDiscount::getDiscount($total, $amountWithProductDiscount,$bin);		
	}
	
	public function getFormToSubmit($orderId, $amount, $extra) {
		global $xcart_dir;
		$this->redirectingMessage = "Please wait... while redirecting to ccavenue. It may take some time";
		$Redirect_Url = $this->getHttpsLocation()."/mkorderBook.php?orderid=".$orderId."&".$extra['session_name']."=".$extra['session_id']."&utm_source_confirm=".$extra['utm_source']."&utm_medium_confirm=".$extra['utm_medium']."";
		
		/*echo "Extra : <BR>";
		foreach ($extra as $key => $value) {
			echo "Key: " . $key ;
			if(is_a($value, 'CardInfo')) $value = $value->toString();
			if(is_a($value, 'ShipToAddress')) $value = $value->toString();
			echo "  Value: " . $value . "<BR>";
		}*/
		$configMode = $extra['configMode'];
		if($configMode=="local"||$configMode=="test"||$configMode=="release"){
	        if($configMode=="release"){
	        	$orderId = "RELE_".$orderId;
	        } elseif($configMode=="local"){
	        	$orderId = "LOCAL".$orderId;
	        } else {
	        	$orderId = "ALPHA".$orderId;	            
	        }
	    }
	    
	    $Checksum = $this->getCheckSum($this->Merchant_Id,$amount,$orderId ,$Redirect_Url,$this->WorkingKey);
		$this->postParams = array();
		$this->postParams['Order_Id'] = $orderId;
		$this->postParams['Amount'] = $amount;
		$this->postParams['Merchant_Id'] = $this->Merchant_Id;
		$this->postParams['Redirect_Url'] = $Redirect_Url;
		$this->postParams['Checksum'] = $Checksum;
		
		$returnCountry = func_get_countries();
		$billingAddress = $extra['billAddress'];
		if($billingAddress != NULL){
		 	$billing_cust_country = $billingAddress->getCountry();
			$billing_cust_country = $returnCountry[$billing_cust_country];
			
			
			$this->postParams['billing_cust_name'] = $billingAddress->getName();
			$this->postParams['billing_cust_address'] = trim($billingAddress->getAddress());
			$this->postParams['billing_cust_state'] = $billingAddress->getState();
			$this->postParams['billing_cust_city'] = $billingAddress->getCity();
			$this->postParams['billing_cust_country'] = $billing_cust_country;
			$this->postParams['billing_zip_code'] = $billingAddress->getZipcode();
			$this->postParams['billing_cust_email'] = $billingAddress->getEmail();
			
			$billingPhone = $billingAddress->getMobile();
	
			if(empty($billingPhone)) $billingPhone = $billingAddress->getPhone();
			
			if(!empty($billingPhone)) $this->postParams['billing_cust_tel'] = $billingPhone;
		}
		$shipAddress = $extra['shipAddress'];
			if($shipAddress != NULL){$shipping_cust_country = $shipAddress->getCountry();
			$shipping_cust_country = $returnCountry[$shipping_cust_country];
			
			$this->postParams['delivery_cust_name'] = $shipAddress->getName(); 
			$this->postParams['delivery_cust_address'] = trim($shipAddress->getAddress());
			$this->postParams['delivery_cust_state'] = $shipAddress->getState();
			$this->postParams['delivery_cust_city'] = $shipAddress->getCity();
			$this->postParams['delivery_cust_country'] = $shipping_cust_country;
			$this->postParams['delivery_zip_code'] = $shipAddress->getZipcode();
			$this->postParams['delivery_cust_email'] = $shipAddress->getEmail();
			
			$shipPhone = $shipAddress->getMobile();
			if(empty($shipPhone)) $shipPhone = $shipAddress->getPhone();		
			if(!empty($shipPhone)) $this->postParams['delivery_cust_tel'] = $shipPhone;
		}
		
		
		if(isset($this->gatewayCode)) {
			$this->postParams['cardOption'] = "netBanking";
			$this->postParams['netBankingCards'] = $this->gatewayCode;
			$this->formAction = "https://www.ccavenue.com/servlet/new_txn.PaymentIntegration";
		} else {
			$this->formAction = "https://www.ccavenue.com/shopzone/cc_details.jsp";
		}
		$amount =(int)$amount;
		if($amount==0) {
			$this->formAction = $Redirect_Url;
			$this->redirectingMessage = "Please wait... while we process your order";
		}
		
		$this->postParams[$extra['session_name']] = $extra['session_id'];
		return parent::generateHTML(); 
	}
	
	public function verifyPaymentData($returnedData, $amountToBepaid, $prevStatus) {
		
		if($prevStatus!='PP') {
			throw new \Exception("Invalid Order State", 500);
		}
		
		$this->setAmountToBePaid($amountToBepaid);
		$this->setAmountPaid(0);
		if($amountToBepaid == 0) {
			$this->setPaymentCompleted(true);
			$this->setPaymentDataCorrect(true);
			return $this->validatePayment();
		}		
		
		if(empty($returnedData['AuthDesc'])) {
			$this->setPaymentDataCorrect(true);
			$this->setPaymentCompleted(false);
			return $this->validatePayment();
		}
		
		
		$this->setAmountPaid($returnedData['Amount']);
		$this->setResponseCode($returnedData['AuthDesc']);
		$this->setCompleteResponse($returnedData);		
		$this->setBankTransactionID($returnedData['nb_bid']);
		$this->setGatewayTransactionID($returnedData['nb_order_no']);
		
		$ccAvenueCheckSum = $this->verifychecksum($this->Merchant_Id, $returnedData['Order_Id'], $returnedData['Amount'], $returnedData['AuthDesc'], $returnedData['Checksum'], $this->WorkingKey);
		if($ccAvenueCheckSum && $this->verifyPaidAmount($returnedData['Amount'], $amountToBepaid)) {
			$this->setPaymentDataCorrect(true);
			if($returnedData['AuthDesc'] == 'Y') {
				$this->setPaymentCompleted(true);
			} else {
				$this->setPaymentCompleted(false);
			}
		} else {
			$this->setPaymentDataCorrect(false);
			$this->setPaymentCompleted(false);
		}
		
		return $this->validatePayment();
				
	}
	
	
	/* This method is used in case of validate the transaction only when data is coming through server to server api calls
	 * Check sum match is ignored here.
	 * */
	public function verifyReconciledData($returnedData, $amountToBepaid) {
		
		if(empty($returnedData['AuthDesc'])) {
			$this->setPaymentDataCorrect(true);
			$this->setPaymentCompleted(false);
			return $this->validatePayment();
		}
		
		
		$this->setAmountToBePaid($amountToBepaid);		
		$this->setAmountPaid($returnedData['Amount']);
		$this->setResponseCode($returnedData['AuthDesc']);
		$this->setCompleteResponse($returnedData);		
		$this->setBankTransactionID($returnedData['nb_bid']);
		$this->setGatewayTransactionID($returnedData['nb_order_no']);
		
		if($this->verifyPaidAmount($returnedData['Amount'], $amountToBepaid)) {
			$this->setPaymentDataCorrect(true);
			if($returnedData['AuthDesc'] == 'Y') {
				$this->setPaymentCompleted(true);
			} else {
				$this->setPaymentCompleted(false);
			}
		} else {
			$this->setPaymentDataCorrect(false);
			$this->setPaymentCompleted(false);
		}
		
		return $this->validatePayment();		
	} 
	
	private function getchecksum($MerchantId,$Amount,$OrderId ,$URL,$WorkingKey) {
		$str ="$MerchantId|$OrderId|$Amount|$URL|$WorkingKey";
		$adler = 1;
		$adler = $this->adler32($adler,$str);
		return $adler;
	}
	
	private function verifychecksum($MerchantId,$OrderId,$Amount,$AuthDesc,$CheckSum,$WorkingKey) {
		$str = "$MerchantId|$OrderId|$Amount|$AuthDesc|$WorkingKey";
		$adler = 1;
		$adler = $this->adler32($adler,$str);
		
		if($adler == $CheckSum)
			return true;
		else
			return false;
	}
	
	private function adler32($adler , $str)	{
		$BASE =  65521 ;
	
		$s1 = $adler & 0xffff ;
		$s2 = ($adler >> 16) & 0xffff;
		for($i = 0 ; $i < strlen($str) ; $i++)
		{
			$s1 = ($s1 + Ord($str[$i])) % $BASE ;
			$s2 = ($s2 + $s1) % $BASE ;
				//echo "s1 : $s1 <BR> s2 : $s2 <BR>";
	
		}
		return $this->leftshift($s2 , 16) + $s1;
	}
	
	private function leftshift($str , $num)	{
	
		$str = DecBin($str);
	
		for( $i = 0 ; $i < (64 - strlen($str)) ; $i++)
			$str = "0".$str ;
	
		for($i = 0 ; $i < $num ; $i++) 
		{
			$str = $str."0";
			$str = substr($str , 1 ) ;
			//echo "str : $str <BR>";
		}
		return $this->cdec($str) ;
	}
	
	private function cdec($num)	{
	
		for ($n = 0 ; $n < strlen($num) ; $n++)
		{
		   $temp = $num[$n] ;
		   $dec =  $dec + $temp*pow(2 , strlen($num) - $n - 1);
		}
	
		return $dec;
	}
	
	
	
}
