<?php

namespace revenue\payments\gateway;

use enums\revenue\payments\Gateways;
use common\Crypt_RC4;

class EBSGateway extends NetBankingGateway {
	
	private $paymentFlagged=false;
	
	private $ebs_secret_key = "b9e5685ab6439dcaa55013a27f44decf";
	
	public function getFormToSubmit($orderId, $amount, $extra) {
	    
		$Redirect_Url = $this->getHttpsLocation()."/mkorderBook.php?orderid=".$orderId."&".$extra['session_name']."=".$extra['session_id']."&utm_source_confirm=".$extra['utm_source']."&utm_medium_confirm=".$extra['utm_medium'];
		
	    $ebs_secret_key = $this->ebs_secret_key;	 // Your Secret Key
	    $ebs_account_id = 6047;
	    $ebs_mode = "LIVE";
		$configMode = $extra['configMode'];
	    if($configMode=="local"||$configMode=="test"||$configMode=="release"){
	        if($configMode=="release"){
	        	$orderId = "RELE_".$orderId;	         
	        } else if($configMode=="local"){
	        	$orderId = "LOCAL".$orderId;
	        }else {
	        	$orderId = "ALPHA".$orderId;	           
	        }
	    }	
	    $ebs_string = "$ebs_secret_key|$ebs_account_id|$amount|$orderId|$Redirect_Url|$ebs_mode";
	    $ebs_secure_hash = md5($ebs_string);
	    
	    $this->formAction = "https://secure.ebs.in/pg/ma/sale/pay/";
	    $this->redirectingMessage = "Redirecting to EBS";
	    $this->postParams = array();
		$this->postParams['account_id'] = $ebs_account_id;
		$this->postParams['return_url'] = $Redirect_Url . '&DR={DR}';
		$this->postParams['mode'] = $ebs_mode;
		$this->postParams['reference_no'] = $orderId;
		$this->postParams['amount'] = $amount;
		$this->postParams['secure_hash'] = $ebs_secure_hash;
		$gateway_code = $this->getGatewayCode();
		if(isset($gateway_code)) $this->postParams['payment_option'] = $gateway_code;
		$this->postParams['description'] = $extra['saleDetail'];
		
		$bAddress = $extra['billAddress'];
		$shipAddress = $extra['shipAddress'];
		
		$this->postParams['name'] = $bAddress->getName();
		$this->postParams['address'] = trim($bAddress->getAddress());
		$this->postParams['city'] = $bAddress->getCity();
		$this->postParams['state'] = $bAddress->getState();
		$this->postParams['postal_code'] = $bAddress->getZipcode();
		$this->postParams['country'] = $bAddress->getCountry();
		$this->postParams['email'] = $bAddress->getEmail();
		
		$billingPhone = $bAddress->getMobile();
		if(empty($billingPhone)) $billingPhone = $bAddress->getPhone();		
		if(!empty($billingPhone)) $this->postParams['phone'] = $billingPhone;
		
		$this->postParams['ship_name'] = $shipAddress->getName();
		$this->postParams['ship_address'] = trim($shipAddress->getAddress());
		$this->postParams['ship_city'] = $shipAddress->getCity();
		$this->postParams['ship_state'] = $shipAddress->getState();
		$this->postParams['ship_postal_code'] = $shipAddress->getZipcode();
		$this->postParams['ship_country'] = $shipAddress->getCountry();
		
		$shipPhone = $shipAddress->getMobile();
		if(empty($shipPhone)) $shipPhone = $shipAddress->getPhone();		
		if(!empty($shipPhone)) $this->postParams['ship_phone'] = $shipPhone;
		
		
		$this->postParams[$extra['session_name']] = $extra['session_id'];

		return parent::generateHTML();		
	}

	public function getName() {
		return Gateways::EBS;
	}
	
	public function isPaymentFlagged() {
		return $this->paymentFlagged;	
	}
	
	public function verifyPaymentData($returnedData, $amountToBepaid, $prevStatus) {
		
		if($prevStatus!='PP') {
			throw new \Exception("Invalid Order State", 500);
		}
		
		if(empty($returnedData['DR'])) {
			$this->setPaymentDataCorrect(true);
			$this->setPaymentCompleted(false);
			return $this->validatePayment();
		}
		
		$secret_key = $this->ebs_secret_key;
		
		$DR = preg_replace("/\s/","+",$returnedData['DR']);
		$rc4 = new Crypt_RC4($secret_key);
		$QueryString = base64_decode($DR);
		$rc4->decrypt($QueryString);
		$QueryString = split('&',$QueryString);

		$response = array();
		foreach($QueryString as $param){
			$param = split('=',$param);
			$response[$param[0]] = urldecode($param[1]);
		}
		//for EBS:- ther is no cheksum whil sending data to EBS so validating back whether amount is tampered or not
        
		$amountPaid= $response['Amount'];
		
		if(isset($response['IsFlagged']) && $response['IsFlagged'] == 'NO' ) {
			$this->paymentFlagged = false; 
		} else if(isset($response['IsFlagged']) && $response['IsFlagged'] == 'YES' ) {
			$this->paymentFlagged = true;
		}
		
		if(!empty($response['DateCreated'])) {
			$this->setTransactionTime(strtotime($response['DateCreated']));
		}		
		
		$this->setAmountPaid($amountPaid);
		$this->setAmountToBePaid($amountToBepaid);
		$this->setResponseCode($response['ResponseCode']);
		$this->setCompleteResponse($response);
		$this->setResponseMessage($response['ResponseMessage']);
		$this->setBankTransactionID($response['TransactionID']);
		$this->setGatewayTransactionID($response['PaymentID']);
				
		if(!$this->verifyPaidAmount($amountPaid, $amountToBepaid) || $response['Mode'] == 'TEST') {
			$response['ResponseCode']=1;
			$response['AmountTampered']=1;
			$this->setPaymentCompleted(false);
			$this->setPaymentDataCorrect(false);        
		} else if($response['ResponseCode']==0){
			$this->setPaymentDataCorrect(true);
			$this->setPaymentDataCorrect(true);
		} else {
			$this->setPaymentDataCorrect(false);
			$this->setPaymentDataCorrect(true);
		}
		
		return $this->validatePayment();
	}
	
}