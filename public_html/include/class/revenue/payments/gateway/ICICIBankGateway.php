<?php

namespace revenue\payments\gateway;

use enums\revenue\payments\CardType;
use enums\revenue\payments\PaymentType;
use enums\revenue\payments\Gateways;

//TODO::CardISSUER AND CARDTYPE
class ICICIBankGateway extends CreditDebitCardGateway {
	
	public function getName() {
		if($this->emiType!=null) {
			return $this->emiType;
		} else {
			return Gateways::ICICIBank;
		}
	}
	
	public static function getEMICharges($emiOption,$amount) {
		$emiCharge = 0;
		if($emiOption === Gateways::ICICIBank3EMI) {
			$ICICIdurationData = \FeatureGateKeyValuePairs::getFeatureGateValueForKey('Payments.EMIBanks.ICICI.3MonthEMI');
			$ICICIdurationData = json_decode($ICICIdurationData,true);
			$emiCharge = $ICICIdurationData['Charge'];
				
		} elseif ($emiOption === Gateways::ICICIBank6EMI) {
			$ICICIdurationData = \FeatureGateKeyValuePairs::getFeatureGateValueForKey('Payments.EMIBanks.ICICI.6MonthEMI'); 
			$ICICIdurationData = json_decode($ICICIdurationData,true);
			$emiCharge = $ICICIdurationData['Charge'];
		}
		
		if(empty($emiCharge)) return 0;
		return $emiCharge;
		
	}
	
	public function getFormToSubmit($orderId, $amount, $extra) {
		global $cookieprefix,$cookiedomain;
		$cardDetails = $extra['cardDetails'];
		$configMode = $extra['configMode'];
        $expiryMonth = $cardDetails->getExpiryMonth();
		$expiryYear = $cardDetails->getExpiryYear();
		$expiryDate = $expiryMonth.$expiryYear; 
		if(empty($expiryDate)) {
			$expiryDate = "1249";
		}
        $holderName = $cardDetails->getHolderName();
		$cardType = "CREDI";
		$cardCVV = $cardDetails->getCvvNumber();
		if(empty($cardCVV)) {
			$cardCVV = "";
		}
		$cardNum = $cardDetails->getNumber();
		$toEncrypt = $cardNum . ";" . $expiryDate . ";". $cardCVV . ";" . $holderName .";" . $this->cardIssuer . ";" . $cardType. ";";
		//echo "$toEncrypt";exit;
		$encryptedtext = text_crypt($toEncrypt);

		if($configMode=="local"||$configMode=="test"||$configMode=="release") {
			if($configMode=="release"){
				$iciciMerchantID= "M00000000002585";
				$orderId = "RELE_".$orderId;
			} else {
				$iciciMerchantID= "M00000000003433";
				$orderId = "ALPHA".$orderId;
			}
			
		} else {
			$iciciMerchantID= "M00000000005357";
		}
		
		if($this->emiType!=null) {
			if($this->emiType==Gateways::ICICIBank3EMI) {
				$iciciMerchantID= "M00000000006566";
			} else if($this->emiType==Gateways::ICICIBank6EMI){
				$iciciMerchantID= "M00000000006567";
			}
		}
		
		if($extra['order_type'] == 'gifting'){
			$orderId = "G_".$orderId;
		}
		
		//delete the cookie
		setMyntraCookie("myntra_secure", "", time()-3600,'/',$cookiedomain, true, true);
		//set the cookie
		$time = 0; //If set to 0, or omitted, the cookie will expire at the end of the session (when the browser closes)
		setMyntraCookie('myntra_secure', $encryptedtext,$time,'/',$cookiedomain, true, true);
		
		
		$purchaseAmount = $amount * 100;
		$displayAmount = "INR".$amount;
		$accept_hdr = $extra['accept_http'];
		$user_agent = $extra['http_user_agent'];
		
	    $this->formAction = "https://3dsecure.payseal.com/MultiMPI/from_icici_merchant.jsp";
	    $this->redirectingMessage = "Redirecting to Verify by Visa / Master Card Secure";
	    $this->postParams = array();
		$this->postParams['pan'] = $cardNum;
		$this->postParams['expirydate'] = $expiryDate;
		$this->postParams['purchaseAmount'] = $purchaseAmount;
		$this->postParams['displayAmount'] = $displayAmount;
		$this->postParams['shoppingContext'] = $orderId;
		$this->postParams['merchantID'] = $iciciMerchantID;
		$this->postParams['currencyVal'] = 356;
		$this->postParams['exponent'] = 2;
		$this->postParams['deviceCategory'] = 0;
		$this->postParams['acceptHdr'] = $accept_hdr;
		$this->postParams['agentHdr'] = $user_agent;
		return parent::generateHTML();
	}
	
	public function verifyPaymentData($iciciPaymentData, $amountToBepaid, $prevStatus) {
		if($prevStatus != "PP") {
			throw new \Exception("Invalid Order State", 500);
		}
		
		if(empty($iciciPaymentData)) {
			$this->setPaymentDataCorrect(true);
			$this->setPaymentCompleted(false);
			return $this->validatePayment();
		}
		
		$iciciGatewayResponseCode = $iciciPaymentData['gateway_response_code'];
		$iciciAmountPaid = $iciciPaymentData['purchaseamount']/100;
		
		$this->setAmountPaid($iciciAmountPaid);
		$this->setAmountToBePaid($amountToBepaid);
		$this->setResponseCode($iciciGatewayResponseCode);
		$this->setCompleteResponse($iciciPaymentData['response']);
		$this->setResponseMessage($iciciPaymentData['gateway_response_message']);
		$this->setBankTransactionID($iciciPaymentData['epg_trasaction_id']);
		$this->setGatewayTransactionID($iciciPaymentData['gateway_transaction_id']);
		
		if(isset($iciciGatewayResponseCode) && ($iciciGatewayResponseCode===0||$iciciGatewayResponseCode==='0')) {
			if($this->verifyPaidAmount($iciciAmountPaid, $amountToBepaid)) {
				$this->setPaymentCompleted(true);
				$this->setPaymentDataCorrect(true);
				return true;
			} else {
				$this->setPaymentCompleted(false);
				$this->setPaymentDataCorrect(false);				
			}
		} else {
			$this->setPaymentCompleted(false);
			$this->setPaymentDataCorrect(true);
		}
		return false;
	}
	
}
?>