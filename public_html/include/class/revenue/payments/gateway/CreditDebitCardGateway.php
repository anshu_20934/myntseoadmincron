<?php

namespace revenue\payments\gateway;

use enums\revenue\payments\PaymentType;
use PaymentGatewayDiscount;

abstract class CreditDebitCardGateway extends PaymentGateway {
	protected $cardNumber;
	protected $cardIssuer;
	protected $bankName;
	protected $paymentType;
	protected $emiType;
	
	public function __construct($ccNumber, $cardIssuer, $bankName, $paymentType,$emiType=null) {
		$this->cardNumber = $ccNumber;
		$this->cardIssuer = $cardIssuer;
		$this->bankName = $bankName;
		$this->paymentType = $paymentType;
		$this->emiType = $emiType;
		if(!empty($emiType)) $this->isEMI = true;
	}
	
	public static function endsWith($haystack, $needle) {
	    $length = strlen($needle);
	    if ($length == 0) {
	        return true;
	    }
	
	    $start  = $length * -1; //negative
	    return (substr($haystack, $start) === $needle);
	}
	
	public function setCardNumber($cardNumber) {
		$this->cardNumber = $cardNumber;	
	}
	
	public function setCardIssuer($cardIssuer) {
		$this->cardIssuer = $cardIssuer;
	}
	
	public function setBankName($bankName) {
		$this->bankName = $bankName;
	}
	
	public function isPaymentInline() {
		return true;
	}
	
	public function getCardNumber() {
		return $this->cardNumber;	
	}	
	
	public function getCardBin() {
		return substr($this->cardNumber, 0,6);	
	}
	
	public function getPaymentIssuer() {
		return $this->cardIssuer;	
	}
	
	public function getBankName() {
		return $this->bankName;
	}
	
	public function getDiscountByCardNumber($total, $amountWithProductDiscount) {
		$bin = $this->getCardBin();
		return PaymentGatewayDiscount::getDiscount($total, $amountWithProductDiscount,$bin);		
	}

	public function getPaymentType() {
		if(empty($this->paymentType)) {
			return PaymentType::CreditDebit;	
		} else {
			return $this->paymentType;
		}
	}

}
?>