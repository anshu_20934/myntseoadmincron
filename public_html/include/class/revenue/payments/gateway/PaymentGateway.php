<?php
namespace revenue\payments\gateway;

abstract class PaymentGateway {
	
	protected $formAction;
	protected $postParams;
	protected $redirectingMessage;
	private $https_loc;
	private $isPaymentDataCorrect=false;
	private $isPaymentComplete=false;
	protected $isEMI=false;
	private $transactionTime,$amountToBePaid,$amountPaid,$responseCode,$completeResponse,$responseMessage,$bankTransactionID,$gatewayTransactionID, $orderId;
	
	abstract public function getPaymentType();
	abstract public function getFormToSubmit($orderId, $amount, $extra);
	abstract public function getName();
	abstract public function getBankName();
	abstract public function getCardBin();
	abstract public function getPaymentIssuer();
	abstract public function isPaymentInline();
	abstract function verifyPaymentData($returnedData,$amountToBepaid,$prevStatus);
	
	public function isEMI(){
		return $this->isEMI;
	}
	
	public function getTransactionTime() {
		return $this->transactionTime;
	}
	
	public function setTransactionTime($transactionTime) {
		$this->transactionTime = $transactionTime;
	}
	
	public function setAmountToBePaid($amountToBePaid){
		$this->amountToBePaid = $amountToBePaid;
	}
	
	public function getAmountToBePaid(){
		return $this->amountToBePaid;
	}
	
	public function setAmountPaid($amountPaid){
		$this->amountPaid = $amountPaid;
	}
	
	public function getAmountPaid() {
		return $this->amountPaid;
	}
	
	
	public function setResponseCode($code) {
		$this->responseCode = $code;
	}
		
	public function getResponseCode() {
		return $this->responseCode;
	}
	
	public function setCompleteResponse($gatewayResponse) {
		$this->completeResponse = $gatewayResponse;
	}
	
	public function getCompleteResponse() {
		return $this->completeResponse;
	}
	
	public function setResponseMessage($responseMessage) {
		$this->responseMessage = $responseMessage;
	}
	
	public function getResponseMessage() {
		return $this->responseMessage;
	}
	
	public function setBankTransactionID($id) {
		$this->bankTransactionID = $id;
	}
	
	public function getBankTransactionID() {
		return $this->bankTransactionID;
	}
	
	public function setGatewayTransactionID($id) {
		$this->gatewayTransactionID = $id;
	}
	
	public function getGatewayTransactionID() {
		return $this->gatewayTransactionID;
	}
		
	public function getOrderId() {
	    return $this->orderId;
	}

	public function setOrderId($orderId) {
	    $this->orderId = $orderId;
	}
	
	public function verifyPaidAmount($amountPaid,$amountToBePaid) {
		if($amountPaid>$amountToBePaid-1) {
			return true;
		} else {
			return false;
		}
	}
	
	public static function getEMICharges($emiOption,$amount) {
		return 0;
	}
	
	public function insertLogData($orderID,$amount,$login) {
		global $_SERVER, $CLIENT_IP;
		$toLog = array();
		$orderID=str_replace("G_", "", $orderID);
		$toLog['orderid'] = $orderID;
		$toLog['login'] = $login;
		$toLog['amount'] = $amount;
		if(isset($_SERVER['HTTP_USER_AGENT'])) $toLog['user_agent'] = mysql_real_escape_string($_SERVER['HTTP_USER_AGENT']);
		$gatewayName = $this->getName();
		$toLog['payment_gateway_name'] = $gatewayName;
	
		$payment_method = $this->getPaymentType();
		
		if(!empty($payment_method)) {
			$toLog['payment_option'] = $payment_method;
		}	
		
		$paymentIssuer = $this->getPaymentIssuer();
		
		if(!empty($paymentIssuer)) $toLog['payment_issuer'] = $paymentIssuer;	
	
		if($this->isPaymentInline()) {
			$toLog['is_inline'] = 1;
		} else {
			$toLog['is_inline'] = 0;
		}
	
		$bankName = $this->getBankName();
		
		if(!empty($bankName)) {
			$toLog['card_bank_name'] = $bankName;
		}
		
		$bin = $this->getCardBin();
	
		if(!empty($bin)) {
			$toLog['bin_number'] = $bin;
		}
	
		if(isset($CLIENT_IP)) {
			$toLog['ip_address'] = mysql_escape_string($CLIENT_IP);
		} elseif(isset($_SERVER["REMOTE_ADDR"])) {
			$toLog['ip_address'] = mysql_escape_string($_SERVER["REMOTE_ADDR"]);
		}
	
		$toLog['time_insert'] = time();

		func_array2insertWithTypeCheck('mk_payments_log', $toLog);
		
	}	
	
	public function updateLogData($orderid,$completeVia="Auto") {
		$toLog = array();
		$toLog['time_return'] = time();
		$transactionTime = $this->getTransactionTime();
		if(!empty($transactionTime)) $toLog['time_transaction'] = $transactionTime;
		$toLog['is_tampered'] = $this->getPaymentDataCorrect()?0:1;
		$toLog['is_complete'] = $this->validatePayment()?1:0;
		$toLog['is_flagged'] = $this->isPaymentFlagged()?1:0;
		$toLog['amountToBePaid'] = $this->getAmountToBePaid();
		$gatewayResponseCode = $this->getResponseCode();
		if(isset($gatewayResponseCode)) $toLog['response_code'] = $gatewayResponseCode;
		$toLog['amountPaid'] = $this->getAmountPaid();
		$gatewayResponse = $this->getCompleteResponse();
		if(!empty($gatewayResponse)) $toLog['gatewayResponse'] = print_r($gatewayResponse,true);
		$gatewayMessage = $this->getResponseMessage();
		if(!empty($gatewayMessage)) $toLog['response_message'] = $gatewayMessage;
		$bankTransID = $this->getBankTransactionID();
		if(!empty($bankTransID)) $toLog['bank_transaction_id'] = $bankTransID;
		$gatewayTransID = $this->getGatewayTransactionID();
		if(!empty($gatewayTransID)) $toLog['gateway_payment_id'] = $gatewayTransID;
		$toLog['completed_via'] = $completeVia;
		func_array2updateWithTypeCheck('mk_payments_log', $toLog,"orderid=$orderid");
		return $toLog;
	}
	
	public function setHttpsLocation($loc) {
		$this->https_loc = $loc;	
	}
	
	protected function getHttpsLocation() {
		$https_location = $this->https_loc;
		if(empty($https_location)){
			global $https_location;
			$this->https_loc = $https_location;
		}
		
		return $this->https_loc;	
	}
	
	protected function generateHTML() {
		global $secure_cdn_base;
		$html = "
	    <form name='subFrm' action='". $this->formAction."' method='POST'>
			<center><img src='".$secure_cdn_base."/images/image_loading.gif'></center>
			<br>
			<center><b>". $this->redirectingMessage ."</b></center>
		";
	    
	    
	    if(!empty($this->postParams)) {
		    foreach($this->postParams as $key=>$value) {
		    	if(isset($value)) {
		    		$html = $html . "<input type='hidden' name='". $key ."' value='".$value."' />";
		    	} else {
		    		$html = $html . "<input type='hidden' name='". $key ."' value='' />";
		    	}
		    }
	    }
	    
	    $html = $html . "</form>";
	    
	    return $html;
	}
	
	protected function setPaymentDataCorrect($value) {
		$this->isPaymentDataCorrect = $value;
	}
	
	protected function setPaymentCompleted($value) {
		$this->isPaymentComplete = $value;
	}
	
	public function getPaymentDataCorrect() {
		return $this->isPaymentDataCorrect;
	}
	
	public function getPaymentCompleted() {
		return $this->isPaymentComplete;
	}
	
	public function validatePayment() {
		return ($this->getPaymentCompleted()&&$this->getPaymentDataCorrect());
	}
	
	public function isPaymentFlagged(){
		return false;
	}

                
        /*
         * First arg : success/failure
         * Subsequent args: event names (atleast two)
         * 
         */
        private function pushEvent(/*$success, $name1, $name2 [,name3...]*/) {
            global $xcart_dir;
            require_once($xcart_dir."/Profiler/Profiler.php");

            $argList = func_get_args();
            $numArgs = func_num_args();
            $success = $argList[0];
            $metricName = "payment";

            if($success) {
                $metricName .= ":success";
            } else {
                $metricName .= ":failure";
            }

            $metricName .= ":".$argList[1];
            
            $fire = false;
            for ($i=2; $i < $numArgs; $i++) {
                $metricName .= ":";
                if(!empty($argList[$i])) {
                    $metricName .= $argList[$i];
                    $fire = true;
                }
            }            
            if($fire) {
                \Profiler::increment($metricName);
            }
        }

        /*
         * Push payement related metrics
         * 
         * Metrics:
         * --------
         * G:Gateway:(4 CCA/ICIC/AXIS/TechProc etc) 
         * PM:Payment Method: (CC/DC/Net) 
         * Bank
         * PIssuer (Visa/MasterCard/BankName)
         * 
         * DB Keys and Cardinality:
         * --------------------- 
         * G : payment_gateway_name: ccavenue 12 (4/5)
         * PM : payment_option: netbanking 12 (5 cod/netbanking/cc/dc/phone banking)
         * Bank: card_bank_name: IDEB_N 147
         * PIssuer: payment_issuer: IDEB_N 94 
         * 
        */
        public function pushMetrics($logDetails){
            $success = $this->getPaymentCompleted();
            
            $this->pushEvent($success, "gw_option", $logDetails["payment_gateway_name"], $logDetails["payment_option"]);
            $this->pushEvent($success, "bank", $logDetails["card_bank_name"]);
            $this->pushEvent($success, "issuer", $logDetails["payment_issuer"]);                
        }	
        
        public function performCurlCall($url, $postData) {
        	//echo "$url\n";
        	//echo "$postData\n";
        	$ch = curl_init($url);
        	curl_setopt($ch, CURLOPT_POST      ,1);
        	curl_setopt($ch, CURLOPT_POSTFIELDS    ,$postData);
        	curl_setopt($ch, CURLOPT_FOLLOWLOCATION  ,1);
        	curl_setopt($ch, CURLOPT_HEADER      ,0);  // DO NOT RETURN HTTP HEADERS
        	curl_setopt($ch, CURLOPT_RETURNTRANSFER  ,1); // RETURN THE CONTENTS OF THE CALL
        	curl_setopt($ch,CURLOPT_TIMEOUT,30); //30 sec time out
        	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,0);
        	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,0);
        
        	$response = curl_exec($ch);
        	curl_close($ch);
        	 
        	 
        	$response = trim($response);
        	 
        	if(empty($response)) {
        		return null;
}
        	return $response;
       }
}
?>