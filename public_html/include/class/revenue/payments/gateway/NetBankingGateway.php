<?php

namespace revenue\payments\gateway;

use enums\revenue\payments\PaymentType;
//use class.feature_gate.keyvaluepair;
//use netbankingHelper;

abstract class NetBankingGateway extends PaymentGateway {
	protected $gatewayCode;	
	protected $bankid;
	protected $bankName;
	
	public function __construct($bankid, $gatewayCode) {
		$this->gatewayCode = $gatewayCode;
		$this->bankid = $bankid;
		$activeBanks = \NetBankingHelper::getActiveBanksList();
		
		$this->bankName = $activeBanks[$bankid]['name'];
	}
		
	public function getGatewayCode() {
		return $this->gatewayCode;
	}
	
	public function getPaymentType() {
		return PaymentType::NetBanking;	
	}
	
	public function getBankName() {
		return $this->bankName;
	}
	
	public function getPaymentIssuer(){
		return $this->bankName;
	}
	
	public function getCardBin(){
		return null;
	}
	
	public function isPaymentInline(){
		return false;
	}
	
	public function getDiscountOnBank($bankName, $totalAmount) {
		$discountAmount = 0;
		if(stripos($this->bankName, $bankName) !== false) { //icici bank			
			$icici_discount = \FeatureGateKeyValuePairs::getInteger('payments.iciciBankCardDiscount');
			if($icici_discount>0) {				
				$discountAmount = $totalAmount*$icici_discount/100;
				$discountAmount = round($discountAmount, 2);			
			}
		}
		return $discountAmount;
	}
	
	
}
?>