<?php
namespace revenue\payments\gateway;

use enums\revenue\payments\CardType;
use enums\revenue\payments\PaymentType;
use enums\revenue\payments\Gateways;


class CITIBankGateway extends CreditDebitCardGateway {
	
	public function getName() {
		if($this->emiType!=null) {
			return $this->emiType;
		} else {
		return Gateways::CITIBank;
		}
	}
	
	public static function getEMICharges($emiOption,$amount) {
		$emiCharge = 0;
		if($emiOption === Gateways::CITIBank3EMI) {
			$CITIdurationData = \FeatureGateKeyValuePairs::getFeatureGateValueForKey('Payments.EMIBanks.CITI.3MonthEMI');
			$CITIdurationData = json_decode($CITIdurationData,true);
			$emiCharge = $CITIdurationData['Charge'];
			
		} elseif ($emiOption === Gateways::CITIBank6EMI) {
			$CITIdurationData = \FeatureGateKeyValuePairs::getFeatureGateValueForKey('Payments.EMIBanks.CITI.6MonthEMI');
			$CITIdurationData = json_decode($CITIdurationData,true);
			$emiCharge = $CITIdurationData['Charge'];
		}
	
		if(empty($emiCharge)) return 0;
	
		return $emiCharge;
	
	}
	
	public function getFormToSubmit($orderId, $amount, $extra) {
		
		$https_location = $this->getHttpsLocation();

		if(empty($https_location)){
			global $https_location;
		}
		$CITIGatewayURL = "https://www.citibank.co.in/servlets/TransReq"; 
		$CITIMerchantIDCredit="44433067";
		$CITIMerchantIDDebit="44433081";
		if($this->paymentType === PaymentType::Credit ){
			$CITIMerchantID = $CITIMerchantIDCredit; 
		} else if ($this->paymentType === PaymentType::Debit){
			$CITIMerchantID = $CITIMerchantIDDebit;
		}
		
		
		if($this->emiType!=null) {
			if($this->emiType==Gateways::CITIBank3EMI) {
			//	echo "inside citi 3 emi </br>";exit;
				$CITIMerchantID= "44433079";
			} else if($this->emiType==Gateways::CITIBank6EMI){
			//	echo "inside citi 6emi </br>";exit;
				$CITIMerchantID= "44433093";
			}
		}
		$this->formAction = $CITIGatewayURL;
		$this->redirectingMessage = "Redirecting to Bank Site for Payment Processing";

		//sample authorization request
		/* https://www.citibank.co.in/servlets/TransReq?
		MalltoCiti=0100|https://www.test.citibank.co.in/PgJsps/Merchant1.jsp|1|214140383054|06042009092239|
		44223949|ABC123456XYZ|002546|2.00|CN=5401759002376007;ED=0510;CV=000| */
		
		$Msg_code = "0100";
		$_records = 1;
					
		$year = gmdate("Y"); $month = gmdate("m"); $day = gmdate("d"); 
		$hour = gmdate("H"); $minute = gmdate("i"); $second = gmdate("s");
		$date_param = $day.$month.$year.$hour.$minute.$second;
		
		$cols = array(
				"transactionid" => $orderId
		);
		
		func_array2insertWithTypeCheck("citi_trace_transactionid", $cols);
		$traceId = db_insert_id();
		$traceId = str_pad($traceId, 6, '0', STR_PAD_LEFT);
		
	
		$formatted_amount = number_format($amount,2,".","");
		
		$cardDetails = $extra['cardDetails'];
		$expiryDate = $cardDetails->getExpiryMonth().$cardDetails->getExpiryYear();
		$cardNo = $cardDetails->getNumber();
		$cvvNumber = $cardDetails->getCvvNumber();
		$cardParam = "CN=".$cardNo.";ED=".$expiryDate.";CV=".$cvvNumber;

		//$Redirect_Url = $this->getHttpsLocation()."/mkorderBook.php";
		$Redirect_Url = "https://secure.myntra.com/payments/response/citibank";
		
		$chkSumMsg = $Msg_code."|".$Redirect_Url."|".$_records."|".$date_param."|".$CITIMerchantID."|".$orderId."|".$traceId."|".$formatted_amount."|";
		$chkSumKey = "12ab12ef1db41ca4";
		require_once(dirname(__FILE__)."/../../../../../java/Java.inc");
		$chk = new \Java("com.citibank.pg.CheckSumCalculator");
		$checksum = $chk->calculateCheckSum($chkSumMsg,$chkSumKey);
		$finalUrlParam = $Msg_code."|".$Redirect_Url."|".$_records."|".$checksum."|".$date_param."|".$CITIMerchantID."|".$orderId."|".$traceId."|".$formatted_amount."|".$cardParam."|";		
		$this->postParams = array();
		$this->postParams['MalltoCiti'] = $finalUrlParam;
		
		return parent::generateHTML();
	}
	
	public function verifyPaymentData($returnedData, $amountToBepaid, $prevStatus) {
		if($prevStatus != "PP") {
			throw new \Exception("Invalid Order State", 500);
		}
		if(empty($returnedData)) {
			$this->setPaymentDataCorrect(true);
			$this->setPaymentCompleted(false);
			return $this->validatePayment();
		}

		$ReceivedParams = explode("|",$returnedData['CititoMall']);
		
		$this->setCompleteResponse($returnedData);
		$this->setResponseMessage($returnedData['CititoMall']);
		
		$Msg_code = $ReceivedParams[0];
		$checksum_recd = $ReceivedParams[1];
		$records = $ReceivedParams[2];
		$merchantcode = $ReceivedParams[3];
		$order_no = $ReceivedParams[4];
		$amountPaid = $ReceivedParams[5];
		$auth_response = $ReceivedParams[6];
		$auth_code = $ReceivedParams[7];
		$trace_no = $ReceivedParams[8];
		
		$this->setAmountPaid($amountPaid);
		$this->setAmountToBePaid($amountToBepaid);
		$this->setCompleteResponse($returnedData['CititoMall']);
		$this->setResponseCode($auth_response);
		$this->setBankTransactionID($auth_code);
		$this->setGatewayTransactionID($trace_no);
		
		$chkSumMsg = $ReceivedParams[0]."|".$ReceivedParams[2]."|".$ReceivedParams[3]."|".$ReceivedParams[4]."|".$ReceivedParams[5]."|".$ReceivedParams[6]."|".$ReceivedParams[7]."|".$ReceivedParams[8]."|";
		require_once(dirname(__FILE__)."/../../../../../java/Java.inc");
		$chk = new \Java("com.citibank.pg.CheckSumCalculator");
	
		$chkSumKey = "12ab12ef1db41ca4";
		$checksum_calc = $chk->calculateCheckSum($chkSumMsg,$chkSumKey);
		
		if (strcmp($checksum_recd,$checksum_calc) == 0) {
			$checksum_match = true;
		}
		else{
			$checksum_match = false;		
			}
		
		if (strcmp($auth_response,"Y:") == 0) {
			$auth_decision = true;
		}
		else{
			$auth_decision = false;			
		}
		if($checksum_match) {
			if($auth_decision && $this->verifyPaidAmount($amountPaid, $amountToBepaid)) {
				//pass
				$this->setPaymentCompleted(true);
				$this->setPaymentDataCorrect(true);
			} else {
				//fail
				$this->setPaymentCompleted(false);
				$this->setPaymentDataCorrect(true);
			}
		} else {
			//data tampered fraud
			$this->setPaymentCompleted(false);
			$this->setPaymentDataCorrect(false);
		}

		return $this->validatePayment();
		
		
		 
	
	}
	
	public function serverToServerCall($merchantID, $callType, $amount, $orderId, $traceId) {
		
		if (strcmp($callType,"Status") == 0){
			
			//$Redirect_Url = $this->getHttpsLocation()."/mkorderBook.php";
			$Redirect_Url = "https://secure.myntra.com/payments/response/citibank";
			$_records = 1;
			
			$year = gmdate("Y"); $month = gmdate("m"); $day = gmdate("d");
			$hour = gmdate("H"); $minute = gmdate("i"); $second = gmdate("s");
			$date_param = $day.$month.$year.$hour.$minute.$second;
			
			$CITIGatewayURL = "https://www.citibank.co.in/servlets/TransReq";
			$CITIMerchantID= $merchantID;
			
			$formatted_amount = number_format($amount,2,".","");
			
			$chkSumMsg = "0120"."|".$Redirect_Url."|".$_records."|".$date_param."|".$CITIMerchantID."|".$orderId."|".$traceId."|".$formatted_amount."|";
			
			$chkSumKey = "12ab12ef1db41ca4";
			require_once(dirname(__FILE__)."/../../../../../java/Java.inc");
			$chk = new \Java("com.citibank.pg.CheckSumCalculator");
			$checksum = $chk->calculateCheckSum($chkSumMsg,$chkSumKey);
			$postData = "MalltoCiti=0120"."|".$Redirect_Url."|".$_records."|".$checksum."|".$date_param."|".$CITIMerchantID."|".$orderId."|".$traceId."|".$formatted_amount."|";
			$res=$this->performCurlCall($CITIGatewayURL, $postData);
			
					
		}	
		if (strcmp($callType,"Cancel") == 0){
				
			$Redirect_Url = "https://secure.myntra.com/payments/response/citibank";
			$_records = 1;
				
			$year = gmdate("Y"); $month = gmdate("m"); $day = gmdate("d");
			$hour = gmdate("H"); $minute = gmdate("i"); $second = gmdate("s");
			$date_param = $day.$month.$year.$hour.$minute.$second;
				
			$CITIGatewayURL = "https://www.citibank.co.in/servlets/TransReq";
			$CITIMerchantID= $merchantID;
				
			$formatted_amount = number_format($amount,2,".","");
				
			$chkSumMsg = "0400"."|".$Redirect_Url."|".$_records."|".$date_param."|".$CITIMerchantID."|".$orderId."|".$traceId."|".$formatted_amount."|";
			$chkSumKey = "12ab12ef1db41ca4";
			require_once(dirname(__FILE__)."/../../../../../java/Java.inc");
			$chk = new \Java("com.citibank.pg.CheckSumCalculator");
			$checksum = $chk->calculateCheckSum($chkSumMsg,$chkSumKey);
			$postData = "MalltoCiti=0400"."|".$Redirect_Url."|".$_records."|".$checksum."|".$date_param."|".$CITIMerchantID."|".$orderId."|".$traceId."|".$formatted_amount."|";
			$res=$this->performCurlCall($CITIGatewayURL, $postData);
				
				
		}			
	}
	
	
}

?>
