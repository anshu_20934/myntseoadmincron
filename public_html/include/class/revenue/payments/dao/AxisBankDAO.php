<?php
namespace revenue\payments\dao;

class AxisBankDAO {
	private $orderid;
	private $login;
	private $responsecode;
	private $amount;
	private $locale;
	private $batch_no;
	private $command;
	private $message;
	private $version;
	private $cardtype;
	private $orderinfo;
	private $receipt_no;
	private $merchant_id;
	private $merchant_txn_ref;
	private $authorize_id;
	private $transaction_no;
	private $acq_response_code;
	private $csc_result_code;
	private $csc_request_code;
	private $trans_type_3ds;
	private $ver_status_3ds;
	private $ver_token_3ds;
	private $ver_security_level_3ds;
	private $enrolled_3ds;
	private $xid;
	private $eci;
	private $status_3ds;
	private $avs_request_code;
	private $avs_result_code;
	private $avs_acq_response_code;
	private $response_code_description;
	private $csc_response_description;
	private $status_3ds_description;
	private $avs_result_code_description;
	private $response;

	public function setOrderID($orderid){
		$this->orderid = $orderid;
	}
	
	public function getOrderID(){
		return $this->orderid;
	}
	
	public function setLogin($login) {
		$this->login = $login;
	}
	
	public function getLogin() {
		return $this->login;
	}
	
	public function setResponseCode($response) {
		$this->responsecode = $response;
		$this->setResponseCodeDescription($this->getResponseDescription($response));
	}
	
	public function getResponseCode() {
		return $this->responsecode;
	}
	
	public function setResponseCodeDescription($desctiption) {
		$this->response_code_description = $desctiption;		
	}
	
	public function getResponseCodeDescription() {
		return $this->response_code_description;
	}
	
	public function setAmount($amount) {
		$this->amount = $amount;
	}
	
	public function getAmount(){
		return $this->amount;
	}
	
	public function setAVSResponseCode($code) {
		$this->avs_acq_response_code = $code;
	}
	
	public function getAVSResponseCode() {
		return $this->avs_acq_response_code;
	}
	
	public function setAVSRequestCode($code) {
		$this->avs_request_code = $code;
	}
	
	public function getAVSRequestCode() {
		return $this->avs_request_code;
	}
	
	public function setAVSResultCode($code) {
		$this->avs_result_code = $code;
		$this->setAVSResultDescription($this->displayAVSResponse($code));
	}
	
	public function getAVSResultCode() {
		return $this->avs_result_code;
	}
	
	public function setAVSResultDescription($description) {
		$this->avs_result_code_description = $description;
	}
	
	public function getAVSResultDescription() {
		return $this->avs_result_code_description;
	}
	
	public function set3DSECI($eci) {
		$this->eci = $eci;
	}
	
	public function get3DSECI() {
		return $this->eci;
	}
	
	public function setMessage($message) {
		$this->message = $message;
	}
	
	public function getMessage() {
		return $this->message;
	}
	
	public function setReceiptNumber($rrn) {
		$this->receipt_no = $rrn;
	}
	
	public function getReceiptNumber() {
		return $this->receipt_no;
	}
	
	public function setVer3DSStatus($status) {
		$this->ver_status_3ds = $status;
		$this->set3DSStatusDescription($this->getStatusDescription($status));
	}
	
	public function getVer3DSStatus() {
		return $this->ver_status_3ds;
	}
	
	public function set3DSStatus($status) {
		$this->status_3ds = $status;
	}
	
	public function get3DSStatus() {
		return $this->status_3ds;
	}
	
	public function set3DSStatusDescription($description) {
		$this->status_3ds_description = $description;
	}
	
	public function get3DSStatusDescription() {
		return $this->status_3ds_description;
	}
	
	public function setCSCResultCode($code) {
		$this->csc_result_code = $code;
		$this->setCSCDescription($this->displayCSCResponse($code));
	}
	
	public function getCSCResultCode() {
		return $this->csc_result_code;
	}
	
	public function setCSCDescription($description) {
		$this->csc_response_description = $description;
	}
	
	public function getCSCDescription() {
		return $this->csc_response_description;
	}
	
	public function set3DSSecurityLevel($code) {
		$this->ver_security_level_3ds = $code;
	}
	
	public function get3DSSecurityLevel() {
		return $this->ver_security_level_3ds;
	}
	
	public function set3DSEnrolled($status) {
		$this->enrolled_3ds = $status;
	}
	
	public function get3DSEnrolled(){
		return $this->enrolled_3ds;
	}
	
	
	//TO DO: Add more getter and setter and expand this method
	public function intializeWithOrderID($orderid) {
		if(empty($orderid)) return false;
		
		$data = func_query_first("select * from mk_axis_payments_log where orderid=" . mysql_escape_string($orderid), true); //query to slave
		if(empty($data)) {
			$data = func_query_first("select * from mk_axis_payments_log where orderid=" . mysql_escape_string($orderid)); //query to master
		}
		
		if(empty($data)) return false;
		
		$this->setOrderID($orderid);
		$this->setLogin($data['login']);
		$this->setResponseCode($data['response_code']);
		$this->setAmount($data['amount']);
		$this->setMessage($data['message']);
		
		$this->set3DSECI($data['eci']);
		$this->set3DSEnrolled($data['enrolled_3ds']);
		$this->set3DSSecurityLevel($data['ver_security_level_3ds']);
		$this->set3DSStatus($data['status_3ds']);
		$this->set3DSStatusDescription($data['status_3ds_description']);
		
		$this->setCSCResultCode($data['csc_result_code']);
		$this->setCSCDescription($data['csc_response_description']);
		
		
		$this->setAVSRequestCode($data['avs_request_code']);
		$this->setAVSResponseCode($data['avs_acq_response_code']);
		$this->setAVSResultCode($data['avs_result_code']);
		$this->setAVSResultDescription($data['avs_result_code_description']);
	}
	
	public function insertIntoDB($orderid, $login, $gatewayResponse) {
		$toAxisLog = array();
		
		$this->setOrderID($orderid);
		$this->setLogin($login);
		$this->setResponseCode($gatewayResponse['vpc_TxnResponseCode']);
		$this->setAmount($gatewayResponse['vpc_Amount']);
		$this->setMessage($gatewayResponse['vpc_Message']);
		
		$this->set3DSECI($gatewayResponse['vpc_3DSECI']);
		$this->set3DSEnrolled($gatewayResponse['vpc_3DSenrolled']);
		$this->set3DSSecurityLevel($gatewayResponse['vpc_VerSecurityLevel']);
		$this->set3DSStatus($gatewayResponse['vpc_3DSstatus']);
		
		
		$this->setCSCResultCode($gatewayResponse['vpc_CSCResultCode']);
		
		
		
		$this->setAVSRequestCode($gatewayResponse['vpc_AVSRequestCode']);
		$this->setAVSResponseCode($gatewayResponse['vpc_AcqAVSRespCode']);
		$this->setAVSResultCode($gatewayResponse['vpc_AVSResultCode']);
		
		
		$toAxisLog['orderid'] = $orderid;
		$toAxisLog['login'] = $login;
		$toAxisLog['response_code'] = $gatewayResponse['vpc_TxnResponseCode'];
		$toAxisLog['amount'] = $gatewayResponse['vpc_Amount'];
		$toAxisLog['locale'] = $gatewayResponse['vpc_Locale'];
		$toAxisLog['batch_no'] = $gatewayResponse['vpc_BatchNo'];
		$toAxisLog['command'] = $gatewayResponse['vpc_Command'];
		$toAxisLog['message'] = $gatewayResponse['vpc_Message'];
		$toAxisLog['version'] = $gatewayResponse['vpc_Version'];
		$toAxisLog['cardtype'] = $gatewayResponse['vpc_Card'];
		$toAxisLog['orderinfo'] = $gatewayResponse['vpc_OrderInfo'];
		$toAxisLog['receipt_no'] = $gatewayResponse['vpc_ReceiptNo'];
		$toAxisLog['merchant_id'] = $gatewayResponse['vpc_Merchant'];
		$toAxisLog['merchant_txn_ref'] = $gatewayResponse['vpc_MerchTxnRef'];
		$toAxisLog['authorize_id'] = $gatewayResponse['vpc_AuthorizeId'];
		$toAxisLog['transaction_no'] = $gatewayResponse['vpc_TransactionNo'];
		$toAxisLog['acq_response_code'] = $gatewayResponse['vpc_AcqResponseCode'];
		
			
		// CSC Receipt Data
		$toAxisLog['csc_result_code'] = $gatewayResponse['vpc_CSCResultCode'];
		$toAxisLog['csc_request_code'] = $gatewayResponse['vpc_CSCRequestCode'];
		$toAxisLog['acq_csc_resp_code'] = $gatewayResponse['vpc_AcqCSCRespCode'];
	
		// 3-D Secure Data
		$toAxisLog['trans_type_3ds'] = $gatewayResponse['vpc_VerType'];
		$toAxisLog['ver_status_3ds'] = $gatewayResponse['vpc_VerStatus'];
		$toAxisLog['ver_token_3ds'] = $gatewayResponse['vpc_VerToken'];
		$toAxisLog['ver_security_level_3ds'] = $gatewayResponse['vpc_VerSecurityLevel'];
		$toAxisLog['enrolled_3ds'] = $gatewayResponse['vpc_3DSenrolled'];
		$toAxisLog['xid'] = $gatewayResponse['vpc_3DSXID'];
		$toAxisLog['eci'] = $gatewayResponse['vpc_3DSECI'];
		$toAxisLog['status_3ds'] = $gatewayResponse['vpc_3DSstatus'];
		
		
		//Address Verification Service (AVS) DATA
		$toAxisLog['avs_request_code'] = $gatewayResponse['vpc_AVSRequestCode'];		
		$toAxisLog['avs_result_code'] = $gatewayResponse['vpc_AVSResultCode'];		
		$toAxisLog['avs_acq_response_code'] = $gatewayResponse['vpc_AcqAVSRespCode'];

		$vpc_responseDescription = $this->getResponseDescription($gatewayResponse['vpc_TxnResponseCode']);
		$vpc_CSCResponseDescription = $this->displayCSCResponse($gatewayResponse['vpc_CSCResultCode']);
		$vpc_3DSStatusDescription = $this->getStatusDescription($gatewayResponse['vpc_VerStatus']);
		$vpc_AVSResultDescription = $this->displayAVSResponse($gatewayResponse['vpc_AVSResultCode']);
		$toAxisLog['response_code_description'] = $vpc_responseDescription;
		$toAxisLog['csc_response_description'] = $vpc_CSCResponseDescription;
		$toAxisLog['status_3ds_description'] = $vpc_3DSStatusDescription;
		$toAxisLog['avs_result_code_description'] = $vpc_AVSResultDescription;
		$toAxisLog['response'] = (print_r($gatewayResponse,true));
		func_array2insertWithTypeCheck('mk_axis_payments_log', $toAxisLog);		
	}
	
/*
	 * This function uses the returned status code retrieved from the Digital
	 * Response and returns an appropriate description for the code
	 *
	 * @param vResponseCode String containing the vpc_TxnResponseCode
	 * @return description String containing the appropriate description
	 */
	public function getResponseDescription($vResponseCode) {
		$result = "";
		// check if a single digit response code
		if (strlen($vResponseCode) == 1) {
			switch ($vResponseCode) {
				case '0' : $result = "Transaction Successful"; break;
				case '1' : $result = "Unknown Error"; break;
				case '2' : $result = "Bank Declined Transaction"; break;
				case '3' : $result = "No Reply from Bank"; break;
				case '4' : $result = "Expired Card"; break;
				case '5' : $result = "Insufficient Funds"; break;
				case '6' : $result = "Error Communicating with Bank"; break;
				case '7' : $result = "Payment Server System Error"; break;
				case '8' : $result = "Transaction Type Not Supported"; break;
				case '9' : $result = "Bank declined transaction (Do not contact Bank)"; break;
				case 'A' : $result = "Transaction Aborted"; break;
				case 'B' : $result = "Transaction Blocked"; break;
				case 'C' : $result = "Transaction Cancelled"; break;
				case 'D' : $result = "Deferred transaction has been received and is awaiting processing"; break;
				case 'E' : $result = "Transaction Declined - Refer to card issuer";break;
	 			case 'F' : $result = "3D Secure Authentication failed"; break;
				case 'I' : $result = "Card Security Code verification failed"; break;
				case 'L' : $result = "Shopping Transaction Locked (Please try the transaction again later)"; break;
				case 'N' : $result = "Cardholder is not enrolled in Authentication Scheme"; break;
				case 'P' : $result = "Transaction has been received by the Payment Adaptor and is being processed"; break;
				case 'R' : $result = "Transaction was not processed - Reached limit of retry attempts allowed"; break;
				case 'S' : $result = "Duplicate SessionID (OrderInfo)"; break;
				case 'T' : $result = "Address Verification Failed"; break;
				case 'U' : $result = "Card Security Code Failed"; break;
				case 'V' : $result = "Address Verification and Card Security Code Failed"; break;
				case '?' : $result = "Transaction status is unknown"; break;
				default  : $result = "Unable to be determined";
			}
			return $result;
		} else {
			return "No Value Returned";
		}
	} // getResponseDescription()


	/**
	 * This function uses the QSI AVS Result Code retrieved from the Digital
	 * Receipt and returns an appropriate description for this code.
	 *
	 * @param vAVSResultCode String containing the vpc_AVSResultCode
	 * @return description String containing the appropriate description
	 */
	public function displayAVSResponse($vAVSResultCode) {
		$result = "";
		if (isset($vAVSResultCode)) {
			if (strcasecmp($vAVSResultCode,"Unsupported")==0 || strcasecmp($vAVSResultCode,"No Value Returned")==0) {
				$result = "AVS not supported or there was no AVS data provided";
			} else {                
				switch ($vAVSResultCode){
					case 'A' : $result = "Address match only"; break;
					case 'B' : $result = "Street address matches, but postal code could not be verified because of incompatible formats."; break;
					case 'C' : $result = "Neither the street address nor the postal code could be verified because of incompatible formats."; break;
					case 'D' : $result = "Visa Only. Street addresses and postal code match.";break;
					case 'E' : $result = "Address and ZIP/postal code not provided"; break;
					case 'F' : $result = "Visa Only, UK-issued cards. Acquirer sent both street address and postal code; both match.";break;
					case 'G' : $result = "Issuer does not participate in AVS (international transaction)"; break;
					case 'I' : $result = "Visa Only. Address information not verified for international transaction.";break;
					case 'M' : $result = "Visa Only. Street addresses and postal codes match."; break;
					case 'N' : $result = "Address and ZIP/postal code not matched"; break;
					case '0' : $result = "AVS not requested"; break;
					case 'P' : $result = "Visa Only. Postal code matches, but street address not verified because of incompatible formats."; break;
					case 'R' : $result = "Issuer system is unavailable"; break;
					case 'S' : $result = "Service not supported or address not verified (international transaction)"; break;
					case 'U' : $result = "Address unavailable or not verified"; break;
					case 'W' : $result = "9 digit ZIP/postal code matched, Address not Matched"; break;
					case 'X' : $result = "Exact match - address and 9 digit ZIP/postal code"; break;
					case 'Y' : $result = "Exact match - address and 5 digit ZIP/postal code"; break;
					case 'Z' : $result = "5 digit ZIP/postal code matched, Address not Matched"; break;
					default  : $result = "Unable to be determined";
				}
			}
		} else {
			$result = "null response";
		}
		return $result;
	}

	/**
	 * This function uses the QSI CSC Result Code retrieved from the Digital
	 * Receipt and returns an appropriate description for this code.
	 *
	 * @param vCSCResultCode String containing the vpc_CSCResultCode
	 * @return description String containing the appropriate description
	 */
	public function displayCSCResponse($vCSCResultCode) {
		$result = "";
		if (isset($vCSCResultCode)) {
	
			if ((strcasecmp($vCSCResultCode, "Unsupported")==0)  || (strcasecmp($vCSCResultCode,"No Value Returned")==0)) {
				$result = "CSC not supported or there was no CSC data provided";
			} else {
				switch ($vCSCResultCode){
					case 'M' : $result = "Exact code match"; break;
					case 'S' : $result = "Merchant has indicated that CSC is not present on the card (MOTO situation)"; break;
					case 'P' : $result = "Code not processed"; break;
					case 'U' : $result = "Card issuer is not registered and/or certified"; break;
					case 'N' : $result = "Code invalid or not matched"; break;
					default  : $result = "Unable to be determined";
				}
			}
	
		} else {
			$result = "null response";
		}
		return $result;
	}

	/**
	 * This method uses the 3DS verStatus retrieved from the
	 * Response and returns an appropriate description for this code.
	 *
	 * @param vpc_VerStatus String containing the status code
	 * @return description String containing the appropriate description
	 */
	public function getStatusDescription($vStatus) {
		$result = "";
		if (isset($vStatus)) {
	
			if ( (strcasecmp($vStatus, "Unsupported")==0)  || (strcasecmp($vStatus,"No Value Returned")==0)) {
				$result = "3DS not supported or there was no 3DS data provided";
			} else {
				switch ($vStatus){
					case 'Y'  : $result = "The cardholder was successfully authenticated."; break;
					case 'E'  : $result = "The cardholder is not enrolled."; break;
					case 'N'  : $result = "The cardholder was not verified."; break;
					case 'U'  : $result = "The cardholder's Issuer was unable to authenticate due to some system error at the Issuer."; break;
					case 'F'  : $result = "There was an error in the format of the request from the merchant."; break;
					case 'A'  : $result = "Authentication of your Merchant ID and Password to the ACS Directory Failed."; break;
					case 'D'  : $result = "Error communicating with the Directory Server."; break;
					case 'C'  : $result = "The card type is not supported for authentication."; break;
					case 'M'  : $result = "Attempts processing was used. Payment is performed with authentication";break;
					case 'S'  : $result = "The signature on the response received from the Issuer could not be validated."; break;
					case 'T'  : $result = "ACS timed out.";break;
					case 'P'  : $result = "Error parsing input from Issuer."; break;
					case 'I'  : $result = "Internal Payment Server system error."; break;								
					default   : $result = "Unable to be determined"; break;					
				}
			}
		} else {
			$result = "null response";
		}
		return $result;
	}
}

?>