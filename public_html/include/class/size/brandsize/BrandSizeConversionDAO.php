<?php
namespace size\brandsize;
use base\dao\BaseDAO;

class BrandSizeConversionDAO extends BaseDAO {
	

	public function getAgeGroups(){
		return getSingleColumnAsArray("select attribute_value as age_group from mk_attribute_type_values where attribute_type = 'Age Group'", true);
	}
	
	public function getUnifiedArticleTypes($ageGroup){
		$sql = "select distinct(sp.global_attr_article_type) as articletytpeid, cc.typename as articletype
				from mk_style_properties sp 
				inner join mk_catalogue_classification cc on (cc.parent2 != -1 and cc.id = sp.global_attr_article_type)
				inner join mk_catalogue_classification ccmc on cc.parent2 = ccmc.id
				where 
				sp.global_attr_age_group = '{$ageGroup}' and 
				sp.size_representation_image_url is not null
				order by ccmc.id, cc.typename";
		return getResultsetAsPrimaryKeyValuePair($sql, "articletytpeid", "articletype",true);
	}
	
	public function getUnifiedBrands($articleTypeId,$ageGroup){
		$sql =	"select distinct(global_attr_brand) as brand , av.id as brandid
				from mk_style_properties sp
				inner join mk_attribute_type_values av on (av.attribute_type = 'Brand' and av.attribute_value = sp.global_attr_brand and sp.size_representation_image_url is not null)
				where sp.size_representation_image_url is not null and global_attr_article_type = $articleTypeId and global_attr_age_group = '{$ageGroup}'
				order by brand";
		return getResultsetAsPrimaryKeyValuePair($sql, "brandid", "brand",true);
	}
	
	public function getSizes($articleTypeId,$brandId,$ageGroup){
		$sql = "select sizevalueid, size, jsonOutput 
				from mk_size_representation_rule 
				where article_type_id = $articleTypeId and brand_Id = $brandId and age_group = '{$ageGroup}' and style_id is null";
		return getResultsetAsPrimaryKeyValuePair($sql, "sizevalueid",null,true);
	}
	
	public function getMeasurementsForSkus($skuids){
		$sql = "select som.sku_id as skuid,po.size_representation as measurement
				from mk_product_options po
				inner join mk_styles_options_skus_mapping som on (po.id = som.option_id)
				where som.sku_id in ($skuids)";	
		return getResultsetAsPrimaryKeyValuePair($sql, "skuid", "measurement",true);
	}
	
}