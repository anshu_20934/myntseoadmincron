<?php
namespace size;
/**
 * This class has methods which fetch various display texts for size measurements based on Master/Sub category,Article-Type, Age-Group etc
 * @author kundan
 *
 */
class SizeMeasurementDisplay {
	
	private $articleTypeId, $articleTypeName, $ageGroup, $masterCategoryId, $subCategoryId;
	private $guideType;
	public function __construct($articleTypeId, $articleTypeName, $ageGroup, $masterCategoryId, $subCategoryId) {
		$this->articleTypeId = $articleTypeId;
		$this->articleTypeName = $articleTypeName;
		$this->ageGroup = $ageGroup;
		$this->masterCategoryId = $masterCategoryId;
		$this->subCategoryId = $subCategoryId;
	}
	
	/**
	 * Get Display text Corresponding to master Category for measurement
	 */
	public function getMasterCategoryText() {
		$masterCategoryToDisplay = "";
		switch ($this->masterCategoryId) {
			case 9: $masterCategoryToDisplay = "garment"; break;
			case 10: $masterCategoryToDisplay = "footwear"; break;
		}
		return $masterCategoryToDisplay;
	}
	
	/**
	 * 
	 */
	public function getNoteForMasterCategory() {
		return ($this->masterCategoryId == 9 && $this->subCategoryId != 136) ? "The diagram provides garment measurements which will be different from your body measurement depending on whether the garment is supposed to be loose or tight fitting." : "";
	}
	/**
	 * Get Measurement Tolerance Text
	 * @return string
	 */
	public function getMeasurementToleranceText() {
		return ($this->masterCategoryId == 9 && $this->subCategoryId != 136) ? $this->articleTypeName." measurements shown will have a tolerance of 0.5 inches/1.25 cm." : "";
	}
	
	/**
	 * Get Measurement Guide Text (HTML)
	 */
	public function getMeasurementGuideText() {
		$this->guideType = $this->getGuideTypeBasedOnArticleTypeAndAgeGroup();
		if (!empty($this->guideType)) {
			//locate a file with this name inside the directory:
			$measureGuideFile = \HostConfig::$documentRoot . "/config/size_measurement_guides/{$this->guideType}.html";
			if(file_exists($measureGuideFile)) {
				return file_get_contents($measureGuideFile);
			}
		}
		return "";
	}
	
	private function getGuideTypeBasedOnArticleTypeAndAgeGroup() {
		$measurementGuideType = "";	
		//Measurement guides currently apply only for Apparel & Footwear
		if($this->masterCategoryId != 9 && $this->masterCategoryId != 10) {
			$measurementGuideType = "";
		}
		//Footwear Master category
		elseif($this->masterCategoryId == 10) {
			$measurementGuideType = "Footwear";
		}
		//Apparel Master-category
		elseif ($this->masterCategoryId == 9) {
			//No measurement text for subcategory: Socks, Salwar suit
			if(in_array($this->subCategoryId, array(22,157))){
				$measurementGuideType = "";
			}
			//No measurement text for swimwear, Shirt & Accessory Set,Suspenders,Rompers, Shirt & Accessory Set,Suspenders,Rompers
			elseif(in_array($this->articleTypeId,array(75, 261,176,204, 257,179,142))) {
				$measurementGuideType = "";
			}
			//Bottomwear
			elseif($this->subCategoryId == 29) {
				$measurementGuideType = "Bottomwear";
			}
			//Topwear
			elseif ($this->subCategoryId == 31) {
				//All Women Topwear should use Women Topwear Type,
				//while all others (including all kids) should use Men-Topwear Type
				if ($this->ageGroup === "Adults-Women") {
					$measurementGuideType = "Women-Topwear";
				}
				else {
					$measurementGuideType = "Men-Topwear";
				}
			}
			//Innerwear Sub-category
			elseif ($this->subCategoryId == 136) {
				//Period Panties,Perfume Panties,Disposible Panties,Hygiene Panties,Boxers,Briefs, Pyjama-Tops, sari
				if (in_array($this->articleTypeId,array(275,276,277,274,139,137, 194, 188))) {
					$measurementGuideType = "Bottomwear";
				}
				//camisoles, Sports Bra, Bra 
				elseif(in_array($this->articleTypeId, array(141,151,153))){
					$measurementGuideType = "Women-Topwear";
				}
				//innerwear vests
				elseif($this->articleTypeId == 138){
					$measurementGuideType = "Men-Topwear";
				}
				//innerwear t-shirts
				elseif($this->articleTypeId == 193){
					if($this->ageGroup == "Adults-Women"){
						$measurementGuideType = "Women-Topwear";
					}
					else {
						$measurementGuideType = "Men-Topwear";
					}
				}
			}
			//Loungewear and Nightwear
			elseif($this->subCategoryId == 200) {
				//Lounge Shorts,Lounge Pants
				if(in_array($this->articleTypeId, array(197,175))){
					$measurementGuideType = "Bottomwear";
				}
				//Night Dress,Pyjama Set
				elseif(in_array($this->articleTypeId, array(201,196))){
					if($this->ageGroup == "Adults-Women"){
						$measurementGuideType = "Women-Set";
					}
					else {
						$measurementGuideType = "Men-Set";
					}
				}
				//Robe
				elseif(in_array($this->articleTypeId, array(258))){
					if($this->ageGroup == "Adults-Women"){
						$measurementGuideType = "Women-Topwear";
					}
					else {
						$measurementGuideType = "Men-Topwear";
					}
				}
			}
			//Dress
			elseif($this->subCategoryId == 30){
				//Jumpsuit,Dresses
				if(in_array($this->articleTypeId, array(207,79))){
					if($this->ageGroup == "Adults-Women"){
						$measurementGuideType = "Women-Topwear";
					}
					else {
						$measurementGuideType = "Men-Topwear";
					}
				}
			}
		}
		return $measurementGuideType;
	}
}