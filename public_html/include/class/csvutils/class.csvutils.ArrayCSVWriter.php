<?php
require_once dirname(__FILE__).'/class.csvutils.AbstractCSVWriter.php';

/**
 * 
 * Use this class to Write a PHP Associative Array to CSV File
 * @author kundan
 *
 */
class ArrayCSVWriter extends AbstractCSVWriter {
	
	public function __construct($fileName,$delimiter=",",$endOfLine="\n") {
		$this->fileName = $fileName;
		$this->delimiter = $delimiter;
		$this->endOfLine = $endOfLine;
		$this->openFile();		
	}
	
	public function __destruct() {
		$this->closeFile();
	}
	
	/**
	 * Writes 1st line CSV Header
	 * This method should be called only after calling setHeaderColumns()
	 * @see include/class/csvutils/AbstractCSVWriter::writeHeader()
	 */
	public function writeHeader() {
		fwrite($this->fileHandle, implode($this->delimiter, $this->headerColumns).$this->endOfLine);
	}
	
	/**
	 * Writes PHP Associative array to CSV File
	 * The dataArray should be an array of associative arrays
	 * such that each row of the associative array has keys corresponding to the header rows 
	 * @see include/class/csvutils/AbstractCSVWriter::writeData()
	 */
	public function writeRows($dataArray) {
		foreach ($dataArray as $eachDataRow) {
			$this->writeRow($eachDataRow, $this->fileHandle);
		}
	}
	
	/**
	 * Appends a row of data to CSV file 
	 * Each row of the associative array has keys corresponding to the header rows
	 * @see include/class/csvutils/AbstractCSVWriter::writeRow()
	 */
	public function writeRow($eachDataRow) {
		$isFirst = true;
		foreach($this->headerColumns as $eachColumnHeader) {
			if(!$isFirst) {
				fwrite($this->fileHandle, $this->delimiter.$eachDataRow[$eachColumnHeader]);
			}
			else {
				fwrite($this->fileHandle, $eachDataRow[$eachColumnHeader]);
			}
			$isFirst = false;
		}
		fwrite($this->fileHandle, $this->endOfLine);
	}
}