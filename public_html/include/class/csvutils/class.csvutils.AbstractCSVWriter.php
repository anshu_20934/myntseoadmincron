<?php

abstract class AbstractCSVWriter {
	/**
	 * The FileName to save CSV File To 
	 */
	protected $fileName;
	/**
	 * The delimiter between columns
	 */
	protected $delimiter = ",";
	/**
	 * End of line character
	 */
	protected $endOfLine = "\n";
	/**
	 * Array of column names: header as it should appear in the header row of CSV
	 */
	protected $headerColumns;
	/**
	 * Whether to append to file or overwrite? 
	 */
	protected $writeMode = "w+";

	/**
	 * The handle to CSV file being written 
	 */
	protected $fileHandle;
	
	public function getFileName() {
		return $this->$fileName;
	}
	
	public function setFileName($fileName) {
		$this->$fileName = $fileName;
	}
	
	public function getDelimiter() {
		return $this->delimiter;
	}
	
	public function setDelimiter($delimiter) {
		$this->delimiter = $delimiter;
	}
	
	public function getEndOfLine() {
		return $this->endOfLine;
	}
	
	public function setEndOfLine($endOfLine) {
		$this->endOfLine = $endOfLine;
	}
	
	public function getHeaderColumns() {
		return $this->headerColumns;
	}
	/**
	 * Must be an array of strings
	 */
	public function setHeaderColumns($headerColumns) {
		$this->headerColumns = $headerColumns;
	}
	
	/**
	 * Should be be a+ for append, or w+ for overwrite 
	 */
	public function getWriteMode() {
		return $this->writeMode;
	}
	
	public function setWriteMode($writeMode) {
		$this->writeMode = $writeMode;
	}
	
	public function openFile() {
		$this->fileHandle = @fopen($this->fileName, $this->writeMode);
	}
	
	public function closeFile() {
		fclose($this->fileHandle);
	}
	/**
	 * This method should be called only after calling setHeaderColumns()
	 */
	public abstract function writeHeader();
	public abstract function writeRows($data);
	public abstract function writeRow($dataRow);
}

?>