<?php

/*
 * Decorators for adding url data  as  array list- used in SitemapGenerator Classes
 * Following are supported :-
 * Root 'loc' key - via SitemapUrl
 * Url, Lastmod and Image Elements
 */

interface SitemapUrlInterface {

	public function getUrl();
	
	public function getOptions();
	
}

class SitemapUrl implements SitemapUrlInterface {
	private $url = array();
	private $options = array();
	
	public function __construct($urlLocation, $options=array()) {
		$this->url['loc'] = $urlLocation;
		$this->options = $options;
	}
	
	public function getUrl() {
		return $this->url;
	}
	public function getOptions() {
		return $this->options;
	}
}

abstract class SitemapUrlDecorator implements SitemapUrlInterface {
	/*
	 * Following functions are declared in interface. 
	 * Can not redeclare due to bug in PHP
	 * https://bugs.php.net/bug.php?id=43200
	 * 
	abstract public function getUrl();
	
	abstract public function getOptions();
	*/
}

class SitemapUrlLastmod extends SitemapUrlDecorator {
	
	public $urlObj; // SitemapUrl object - public / protected ?
	
	public function __construct($urlObj) {
		$this->urlObj = $urlObj;
	}
	
	public function getUrl() {
		$options = $this->urlObj->getOptions();
		$url['lastmod'] = $options['lastmod'];
		$url = array_merge($this->urlObj->getUrl(),$url);
		return $url;
	}
	
	public function getOptions() {
		return $this->urlObj->getOptions();
	}
}

class SitemapUrlPriority extends SitemapUrlDecorator {
	
	public $urlObj; // SitemapUrl object - public / protected ?
	
	public function __construct($urlObj) {
		$this->urlObj = $urlObj;
	}
	
	public function getUrl() {
		$options = $this->urlObj->getOptions();
		$url['priority'] = $options['priority'];
		$url = array_merge($this->urlObj->getUrl(),$url);
		return $url;
	}
	
	public function getOptions() {
		return $this->urlObj->getOptions();
	}
}

class SitemapUrlImage extends SitemapUrlDecorator {
	
	public $urlObj; 
	
	public function __construct($urlObj) {
		$this->urlObj = $urlObj;
	}
	
	public function getUrl() {
		$options = $this->urlObj->getOptions();
		if(!isset($options['image']) || !is_array($options['image']))  $ops['image'] = array();
		else $ops['image'] = $options['image'];
		$url = array_merge($this->urlObj->getUrl(),$ops);
		return $url;
	}
	
	public function getOptions() {
		return $this->urlObj->getOptions();
	}
}
