<?php
include_once(dirname(__FILE__).'/../../../env/Host.config.php');

// Class to hold constants for classes and scripts related to sitemaps
class SitemapMyntra {
	// URLs per sitemap maximum
	const MAX_URLS_PER_SITEMAP = 40000;

	const MAX_URLS_PER_INDEX_SITEMAP = 399;
	
	const BASE_URL = "http://myntra.com/sitemaps/";
	
	const SITE_URL = "http://www.myntra.com/";
	
	//Access class constants statically
	private function __construct() {}
}

/*
 * Abstract class that hold basic functionality for creating gzipped xml sitemaps files
 * Extend this class for your custom sitemap file formats (eg, index, image etc )
 * Use SitemapXMLDecorators and SitemapUrlDecorators for creating DOM elements and adding data in child classes
 * When limit of MAX_URLS_PER_SITEMAP is crossed, the class automatically creates multiple files - but avoid crossing this limit to ensure low memory usage
 */

abstract class SitemapGenerator extends SitemapMyntra {

    protected $xmlUrl = '';    
    protected $xmlXsi = "http://www.w3.org/2001/XMLSchema-instance";
    protected $xmlVer = '1.0';
    protected $xmlEncoding = 'UTF-8';
    protected $xmlSchemaLoc = 'http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd';
    protected $xmlNs = 'http://www.sitemaps.org/schemas/sitemap/0.9';
    protected $xmlNs_xhtml="http://www.w3.org/1999/xhtml";
    protected $xmlPath = '';
    protected $urls = array();
    protected $urlCount = 0;
    protected $finished = false;
    protected $defaultlastMod;
    public 	  $fileCount = 0;
	public    $xmlIdentifier;
	
	public function __construct($filename,$target) {
		if(empty($filename)) die("Please provide filename");
		global $xcart_dir;
		//$this->xmlUrl = $hostConfig[$configMode]['baseurl'].'sitemaps/';
		$this->xmlUrl = HostConfig::$sitemapURL;
		$this->xmlIdentifier = strtolower($filename);
		$this->xmlPath	=	$target.'/';
		$this->setDefaultLastmod();
	}

	// Populate XML data from class variables and create XML file
	abstract protected function populateXmlData();

	// add URL and its properties (eg. priority, lastmod ) with $options array
	abstract public function addUrl($urlLocation ='' ,$options = array());
	
	// When all urls have been added, finally call this function to create xml file. It handles creating multiple xml files if needed
    public function write() {
    	for($i = 0; $i <= $this->fileCount; $i++) {
    		$this->populateXmlData($i);
    	}
    	$this->finished();
    }
    
    // Get urls of XML files created , to be used in sitemap-index file
	public function getFileUrls() {
		$files = array();
    	for($i = 0; $i <= $this->fileCount; $i++) {
			$files[] = $this->fileUrlFormat($i);
    	}
    	return $files;
    }
    
    //XML file url format
    public function fileUrlFormat($fileNum=0) {
    	if($this->fileCount > 0 && $fileNum > 0) {
    		$fileurl = $this->xmlUrl.$this->xmlIdentifier.$fileNum.".xml.gz";
    	}
    	else {
    		$fileurl = $this->xmlUrl.$this->xmlIdentifier.".xml.gz";
    	}
    	return $fileurl;
    }
    
    // Get paths of XML files created
    public function getFileNames() {
    	$files = array();
    	for($i = 0; $i <= $this->fileCount; $i++) {
			$files[] = $this->fileNameFormat($i);
    	}
    	return $files;
    }
    
    protected function fileNameFormat($fileNum=0) {
    	if($this->fileCount > 0 && $fileNum > 0) {
    		$filename = $this->xmlPath.$this->xmlIdentifier.$fileNum.".xml";
    	}
    	else {
    		$filename = $this->xmlPath.$this->xmlIdentifier.".xml";
    	}
    	return $filename;
    }
    
    protected function getDefaultlastmod() {
    	return $this->defaultLastMod;
    }
    
    //Set default lastmod (when not provided) to be 'current time'
    protected function setDefaultlastmod() {
    	$this->defaultLastMod = date(DATE_ATOM); 
    }
    
    // If true, dont add any more urls - via addUrl() function
    protected function isFinished() {
    	return $this->finished;
    }
    
    protected function finished() {
    	$this->finished = true;
    }
    
    // Gzip and create the xml from the xml string
    /*protected function gzipFile($filename, $xml) {
    	$gz = gzopen($filename,'w9');
		$a = gzwrite($gz, $xml);
		gzclose($gz);
    }*/
    
    public function getMaxUrlsPerSitemap() {
    	return self::MAX_URLS_PER_SITEMAP;
    }
    
    //Various counters used for creating multiple sitemaps if needed
	protected function setCounters() {
    	if($this->urlCount >= $this->getMaxUrlsPerSitemap()) {
    		$this->fileCount++;
    		$this->urlCount = 0;
    	}
    	$this->urlCount++;
    }
}



/* 
 * Class to create sitemap with 'index' format
 * XMl file is not zipped in this class
 */

class SitemapIndexGenerator extends SitemapGenerator {

	public function addUrl($urlLocation ='' ,$options = array()) {
		if($this->isFinished() === true ) die("File already written");
		if(!isset($options['lastmod'])) $options['lastmod'] = $this->getDefaultlastmod();
		
		$urlObj = new SitemapUrl($urlLocation, $options);
		$urlObj = new SitemapUrlLastmod($urlObj); //Decorate with urlLastmod Decorator
		$url = $urlObj->getUrl();	
    	
    	$this->urls[$this->fileCount][] = $url;
    	$this->setCounters();
		
    }

	protected  function populateXmlData($currentFileNum =0) {
		$doc = new DomDocument('1.0','utf-8');
		
		$elemset = $doc->createElement('sitemapindex');
		$elemset->setAttribute("xmlns", $this->xmlNs);
		$elemset->setAttribute("xmlns:xsi", $this->xmlXsi);
		$elemset->setAttribute("xsi:schemaLocation", $this->xmlSchemaLoc);		
		$elemset = $doc->appendChild($elemset);
    	foreach($this->urls[$currentFileNum] as $url) {
    		$elemObj = new SitemapDom($doc, $elemset, 'sitemap', $url);
			$elemObj = new SitemapDomLocation($elemObj); //Decorate with location Decorator
            $elemObj = new SitemapDomLastmod($elemObj);
			$doc = $elemObj->getDoc();
			
    	}
		$doc->save($this->fileNameFormat($currentFileNum)); // No gzipping for index file
    }
    
    protected function fileNameFormat($fileNum=0) {
    	if($this->fileCount > 0 && $fileNum > 0) {
    		$filename = $this->xmlPath.$this->xmlIdentifier.$fileNum.".xml";
    	}
    	else {
    		$filename = $this->xmlPath.$this->xmlIdentifier.".xml";
    	}
    	return $filename;
    }

    public function getMaxUrlsPerSitemap() {
        return self::MAX_URLS_PER_INDEX_SITEMAP;
    }
}


/* 
 * Class to create sitemap with 'file list' format
 */

class SitemapFileGenerator extends SitemapGenerator {	
    
	protected	$lastmods = array();
	
	public function addUrl($urlLocation ='' ,$options = array()) {
		if($this->isFinished() === true ) die("File already written");
		if(!isset($options['lastmod'])) $options['lastmod'] = $this->getDefaultlastmod();
		
		$urlObj = new SitemapUrl($urlLocation, $options);
        $urlObj = new SitemapUrlLastmod($urlObj); //Decorate with urlLastmod Decorator
        $urlObj = new SitemapUrlPriority($urlObj);
		$url = $urlObj->getUrl();	
    	
    	$this->urls[$this->fileCount][] = $url;
    	$this->setCounters();
    }
   
   //tested and seems fine on fox7 
    protected  function populateXmlData($currentFileNum = 0) {
    	$fileurl = $this->fileUrlFormat($currentFileNum);
    	
    	$doc = new DomDocument('1.0', 'utf-8');
		$elemset = $doc->createElement('urlset');
		$elemset->setAttribute("xmlns", $this->xmlNs);
		$elemset->setAttribute("xmlns:xsi", $this->xmlXsi);
		$elemset->setAttribute("xsi:schemaLocation", $this->xmlSchemaLoc);
        $elemset->setAttribute("xmlns:xhtml", $this->xmlNs_xhtml);
		$elemset = $doc->appendChild($elemset);
    	foreach($this->urls[$currentFileNum] as $url) {
			$elemObj = new SitemapDom($doc, $elemset, 'url', $url);
			$elemObj = new SitemapDomLocation($elemObj); // Decorate with Location decorator
            $elemObj = new SitemapDomxhtml($elemObj);  //adds android deep links for seo purpose to the sitemap
            $elemObj = new SitemapDomLastmod($elemObj); // Decorate with Lastmod decorator
            $elemObj = new SitemapDomPriority($elemObj); // Decorate with Priority decorator
			$doc = $elemObj->getDoc();
			if(!isset($this->lastmods[$this->fileUrlFormat($currentFileNum)])) $this->lastmods[$this->fileUrlFormat($currentFileNum)] = $url['lastmod'];
            
			//Maintain the latest "getLastmod" date of that xml file - to be used in Index Sitemap file
			if(strtotime($this->lastmods[$this->fileUrlFormat($currentFileNum)]) < strtotime($url['lastmod']) ) {
				$this->lastmods[$this->fileUrlFormat($currentFileNum)] = $url['lastmod'];
			} 
    	}
    	$doc->save($this->fileNameFormat($currentFileNum));
		//$xml = $doc->saveXml();
        //$this->gzipFile($this->fileNameFormat($currentFileNum),$doc->saveXML());
    }    
    
    /* Get latest last modification date for each file 
     * Returns an array
     * */
	public function getLastmods() {
		return $this->lastmods;
    }
}


class SitemapImageGenerator extends SitemapGenerator {	
    
  	protected $xmlImage ="http://www.google.com/schemas/sitemap-image/1.1";
  	protected $xmlVer = '1.0';
  	protected $xmlEncoding = 'UTF-8';
	protected $xmlNs = 'http://www.sitemaps.org/schemas/sitemap/0.9';
    
	/*
	 * Method overridden to maintain xml file size
	 */
	public function getMaxUrlsPerSitemap() {
		// decreasing the MaxUrlPerSitemap as image sitemap file is expected to be 10 times size of its corresponding product sitemap file
    	return self::MAX_URLS_PER_SITEMAP/10;
    }
    
	public function addUrl($urlLocation ='' ,$options = array()) {
		if($this->isFinished() === true ) die("File already written");
		
		$urlObj = new SitemapUrl($urlLocation, $options);
		$urlObj = new SitemapUrlImage($urlObj); // Decorate with Image Url decorator
		$url = $urlObj->getUrl();
    	
    	$this->urls[$this->fileCount][] = $url;
		$this->setCounters();
    }
        
    protected  function populateXmlData($currentFileNum = 0) {
    	$fileurl = $this->fileUrlFormat($currentFileNum);
    	$doc = new DomDocument('1.0', 'utf-8');
		$elemset = $doc->createElement('urlset');
		$elemset->setAttribute("xmlns", $this->xmlNs);
		$elemset->setAttribute("xmlns:image", $this->xmlImage);
        

		$elemset = $doc->appendChild($elemset);
    	foreach($this->urls[$currentFileNum] as $url) {
    		
    		$elemObj = new SitemapDom($doc, $elemset, 'url', $url);
			$elemObj = new SitemapDomLocation($elemObj);

            
            


			$elemObj = new SitemapDomImageLoc($elemObj);
			$doc = $elemObj->getDoc();

    	}
    	$doc->save($this->fileNameFormat($currentFileNum));
    	//$this->gzipFile($this->fileNameFormat($currentFileNum),$doc->saveXML());
    }    
}
