<?php

require_once __DIR__.'/../../../env.php';
require_once __DIR__."/../../func/func.utilities.php";
require_once __DIR__."/../../../Cache/Redis.php";
/*
 * classes for fetching Data , required for XML population, from Solr/Mysql
 * Fetches data with max limit of SitemapMyntra::MAX_URLS_PER_SITEMAP, in one cycle
 * call function getData() multiple times to get all results
 * call function hasMore() function to find out whether more results can be fetched
 */
abstract class SitemapDataFetcher extends  SitemapMyntra {
	private 	$hasMore = false;
	protected 	$numTotalFound = 0;
	protected 	$numRemaining = 0;
	protected 	$dataObject;
	protected 	$fileNum = -1;
	protected 	$myn;
	protected 	$sort;
	protected 	$query;
	public 		$updateOnlyLast = false;
	public 		$weblog;

	abstract protected function executeQuery($offset,$limit, $params=array());

	abstract protected function setDataObject();

	public function __construct() {
		global $weblog;
		$this->weblog = $weblog;
		$this->myn = self::SITE_URL;
		$this->setDataObject();
		$this->initializeVars();
	}

	protected function initializeVars() {
		$this->numTotalFound = $this->executeQuery(0, 1)->numFound;
		$this->numRemaining = $this->numTotalFound;
	}

	public function hasMore() {
		if($this->numRemaining <=0 ) return false;
		else return true;
	}

	public function getFileNum() {
		return $this->fileNum;
	}

	public function getData() {
		$this->fileNum++;
		$offset = $this->numTotalFound - $this->numRemaining;
		$limit = self::MAX_URLS_PER_SITEMAP;
			if($this->updateOnlyLast == true ) {
			$url_per_site = self::MAX_URLS_PER_SITEMAP;
			$limit = $this->numTotalFound%$url_per_site;
			if($limit == 0 ) {
				$limit = $url_per_site;
				$offset = (((int)($this->numTotalFound/$url_per_site))-1)*$url_per_site;
			}
			else {
				$offset = ((int)($this->numTotalFound/$url_per_site))*$url_per_site;
			}
			$this->numRemaining = 0;
			$this->fileNum = (int)($this->numTotalFound/$url_per_site)?(int)($this->numTotalFound/$url_per_site):'';
		}
		$rows = $this->executeQuery( $offset, $limit);
		$this->numRemaining = $this->numRemaining - self::MAX_URLS_PER_SITEMAP;
		if($this->numRemaining <=0 ) $this->numRemaining;
		return $rows;
	}
}

/*
 * class to fetch stats_search results
 */

class SearchSitemapData extends SitemapDataFetcher{

	protected $query= array(
						'count_of_products' => array('$gt' => 0)
						);
	protected $sort = array('keyword'=>1);

	protected function setDataObject() {
		$this->dataObject = new MongoDBSearch(HostConfig::$mongodbHost, HostConfig::$mongodbPort,
                                                HostConfig::$mongodb, HostConfig::$configMode);
	}

	protected function executeQuery($offset,$limit, $params = array()) {
		return $this->dataObject->executeQuery($this->query,  $offset, $limit,$this->sort, $params);
	}

	public function getUrlFormatted($row = array()) {
		if(empty($row)) return '';
		if(substr_count($row['keyword'],"-") > HostConfig::$searchKeywordTokenLimit) return '';
		$r = $row;
		$orignal_keyword = trim(preg_replace("/[^a-zA-Z0-9s-]/", "-", trim($r['keyword'])));
		$orignal_keyword = preg_replace('#([ ])+#','-',$orignal_keyword);
		$orignal_keyword = preg_replace('#([-])+#','-',$orignal_keyword);
		$url = $this->myn.strtolower(str_replace(" ","-",$orignal_keyword))."/search/";
		return $url;
	}
	
	protected function initializeVars() {
		$this->numTotalFound = $this->dataObject->getNumEntries($this->query);
		$this->numRemaining = $this->numTotalFound;
	}

}


/*
 * class to fetch product results
 */

class ProductSitemapData extends SitemapMyntra{
	private 	$hasMore = false;
	public 		$updateOnlyLast = false;
	public 		$weblog;
	protected 	$dataObject;
	protected	$myn;
	protected 	$fileType;
	protected 	$fileName;
	private     $obj; 
	private		$firstLevelIndex = -1;
	protected 	$query="global_attr_article_type_facet: ";
	protected 	$sort = 'styleid asc';
	protected 	$fields='id,global_attr_brand,display_category,product,global_attr_catalog_add_date,global_attr_article_type';
	private     $filenameTaken;
	
	
	public function __construct() {
		global $weblog;
		$this->weblog = $weblog;
		$this->myn = self::SITE_URL;
	        $this->filenameTaken = array();
		$this->setDataObject();
		$this->initializeVars();
		
	}
	
	protected function setDataObject() {
		$this->dataObject = new SolrCommon(SOLRConfig::$searchhost,'sprod');
	}
	
	protected function initializeVars() {
        $this->obj = array();
	$this->splicedUrls = array();
	$this->attributeUrlMap =  array();
	$this->attrPriority = '0.9';
	$this->normalPriority = '1.0';
        $this->seoRedisConnection = Redis::getSeoRedisRW();
        /* The 2 types are :
         * brands : just one file containing all the brands
         * category : masterCategory, subCategory, articleType
         */
        //brands
		$this->obj[] = $this->getURLKeywords4Category('brands','brands_filter_facet');
        //category
		$facetFields = array (  'global_attr_master_category_facet',
							    'global_attr_sub_category_facet',
							    'global_attr_article_type_facet',);
	    $query = "*:* count_options_availbale:[1 TO *] ";
	    //$query = "global_attr_article_type_facet:\"Tshirts\" count_options_availbale:[1 TO *] ";
        $facetResults = $this->dataObject->multipleFacetedSearch($query, $facetFields)->facet_fields;
        $facetValues = array();
		foreach ($facetFields as $key) {
			 $vals = get_object_vars($facetResults->$key);
			 foreach($vals as $k=>$v){
                 if ($k == "_empty_") continue;
                 $facetValues[$k] = $key;
			 }
        }

        foreach($facetValues as $key=>$val) {
		    $this->obj[] = $this->getURLKeywords4Category($key,$val);
        }
        if (count($this->obj) > 0) {
		    $this->hasMore = true;
        }
		foreach($this->obj as $k=>$v) {	
		    foreach($v["keywords"] as $key=>$value){
			if(in_array($value['keyword'],$this->splicedUrls)) {
				array_splice($this->obj[$k]['keywords'], $key,1);
			}
		    }
		}
	}
	
	private function getURLKeywords4Category($category, $categoryType) {
	    $informationArray = array();
	    $uniquePathList = array();
	    $keywordList = array();
	    $attributeFacetsJson =  FeatureGateKeyValuePairs::getFeatureGateValueForKey('sitemap.facetsList');
	    $attributeFacetsFilter = FeatureGateKeyValuePairs::getFeatureGateValueForKey('sitemap.facetsList.filter');
	    $attributeProductCountMin = FeatureGateKeyValuePairs::getFeatureGateValueForKey('sitemap.attrProdcutCount.min');

            $attributeFacetedList = json_decode($attributeFacetsJson,true);
	    $attributeFacetedFilters = json_decode($attributeFacetsFilter,true);
	
        if ($category == 'brands') {
            $informationArray['is_article_type'] = false;
            $informationArray['file_name'] = 'brands';
            $informationArray['category'] = 'brands';
            $brands = $this->getFacetedList('*:*', $categoryType);
            foreach ($brands as $brand) {
                if (!empty($brand)) {
                    $genders = $this->getFacetedList("brands_filter_facet:\"$brand\"","global_attr_gender");
                    $uniquePathList[$this->joinWords($brand)] = array("priority" => $this->normalPriority,"pattern"=>'b');
		    if(count($genders) > 1) {
		    	foreach($genders as $gender) {
			    $uniquePathList[$this->joinWords($brand,$gender)] = array("priority" => $this->normalPriority, "pattern"=>'b,g');
		        }
		    }
                }
            }
        } else {
            $informationArray['is_article_type'] = 
                ($categoryType == "global_attr_article_type_facet")?true:false;
            if ($this->filenameTaken[$category] > 0) {
                $informationArray['file_name'] = $this->cleanse($category.$this->filenameTaken[$category]);
            } else {
                $informationArray['file_name'] = $this->cleanse($category);
            }
            $this->filenameTaken[$category]++;
            $informationArray['category'] = $category;
	    switch($categoryType) {
	        case 'global_attr_article_type_facet':
                    $cPattern = 'at';
	            break;
               case 'global_attr_master_category_facet':
		    $cPattern = 'c';
		    break;
               case 'global_attr_sub_category_facet':
                    $cPattern = 'sc';
		    break;
	    }
            //*category*
            $uniquePathList[$this->joinWords($category)] = array("priority" => $this->normalPriority,"pattern" => $cPattern);

            $brands = $this->getFacetedList("$categoryType:\"$category\"", "brands_filter_facet");
	    $articleAttributes = $this->getAttributeFacetedList("$categoryType:\"$category\"", 
					$attributeFacetedList,
					$attributeFacetedFilters,
					$attributeProductCountMin);
	    $uniquePathList = $this->getAttrUniquePathList($articleAttributes,$uniquePathList,array('category'=>$category,'categoryType'=>$categoryType,'type'=>'C','cPattern'=>$cPattern));

            foreach ($brands as $brand) {
                //*the brands under this category*
                $uniquePathList[$this->joinWords($brand, $category)] = array("priority"=>$this->normalPriority,"pattern"=>'b,'.$cPattern);

		$articleAttributes = $this->getAttributeFacetedList("$categoryType:\"$category\" AND 
                                                brands_filter_facet:\"$brand\"", $attributeFacetedList,
						$attributeFacetedFilters,
						$attributeProductCountMin);
	    	$uniquePathList = $this->getAttrUniquePathList($articleAttributes,$uniquePathList,array('brand'=>$brand,'category'=>$category,'categoryType'=>$categoryType,'type'=>'B','cPattern'=>$cPattern));

                $genders = $this->getFacetedList("$categoryType:\"$category\" AND 
                                                brands_filter_facet:\"$brand\"",
                                                "global_attr_gender");
                if (count($genders) > 1) {
                    foreach ($genders as $gender) {
                        //*the genders on this brand*
                        $uniquePathList[$this->joinWords($gender, $brand, $category)] = array("priority" => $this->normalPriority, "pattern" => 'g,b,'.$cPattern);
                        $uniquePathList[$this->joinWords($gender,$category)] = array("priority" => $this->normalPriority, "pattern" => 'g,'.$cPattern);
				
                        /* PORTAL-3191 - size urls are no more needed in the sitemap */
                        $articleAttributes = $this->getAttributeFacetedList("$categoryType:\"$category\" AND 
                                                        brands_filter_facet:\"$brand\" AND 
                                                        global_attr_gender:\"$gender\"",
                                                        $attributeFacetedList,$attributeFacetedFilters,$attributeProductCountMin);
	    		$uniquePathList = $this->getAttrUniquePathList($articleAttributes,$uniquePathList,array('brand'=>$brand,'gender'=>$gender,'categoryType'=>$categoryType,'category'=>$category,'type'=>'G','cPattern'=>$cPattern));
	           }
                }             
            }

            $numPDPUrls = $this->calculateNumPDPUrls($category, $categoryType); 
            $informationArray['num_pdp_urls'] = ($numPDPUrls == NULL)?0:$numPDPUrls;
        }
        $configured_synonyms = $this->getSynonymsFromDB();
        foreach ($uniquePathList as $path => $data) {
            $path = $this->replaceSynonymsInPath($path, $configured_synonyms);
            $tempArr = array();
            $tempArr['keyword'] = $path;
            $tempArr['priority'] = $data["priority"];
            $key = $this->getRedisCanonicalKey($tempArr['keyword']);
            try {
                print $this->seoRedisConnection->set($key,serialize(array("pattern"=>$data["pattern"],"canonical"=>$tempArr['keyword'])));
            } catch(Exception $e) {
                $this->seoRedisConnection = Redis::getSeoRedisRW();
                print $this->seoRedisConnection->set($key,serialize(array("pattern"=>$data["pattern"],"canonical"=>$tempArr['keyword'])));
	        } 
            $keywordsList[] = $tempArr;
        }
        $informationArray['keywords'] = $keywordsList;
        $informationArray['num_list_urls'] = count($keywordsList);
        $informationArray['pdp_file_count'] = -1;
        $informationArray['list_file_count'] = -1;
		return $informationArray;
	}

    private function getRedisCanonicalKey($path) {
        $words = explode('-',$path);
        sort($words);
        $newPath = implode('-',$words);
        return 'seocanonical:'.md5($newPath);
    }
    private function isCatInAttr($attr,$cat) {
        $attr = strtolower($attr);
        $cat = strtolower($cat);
        $attribute = explode('-',$attr);

        
        if(strtolower($cat) == strtolower($attr)) {
            return true;
        }
        
        if(in_array($cat,$attribute)) {
            return true;
        }

        return false;

    }
	
	private function replaceSynonymsInPath($path, $configured_synonyms){
        $pathParts = explode("-", $path);
        $newPathParts = array();
        foreach($pathParts as $index=>$part) { 
	        if(array_key_exists($part,$configured_synonyms)){
			    $synParts = explode("-",$configured_synonyms[$part]);
			    foreach($synParts as $pos=>$replace){
				    $newPathParts[] = $replace;
				}
            }else{
                $newPathParts[] = $part;
            }
        }
        return implode('-',array_unique($newPathParts));
	}
	
    private function getSynonymsFromDB(){
        $sql="SELECT original,corrected FROM search_synonym_table";
        $synonymsFromDB = func_query($sql, true);
        $attributeSynonyms = array();
        if($synonymsFromDB){
            foreach($synonymsFromDB as $index=>$val){
                if($val['original'] and $val['corrected']){
                    $attributeSynonyms[$this->cleanse($val['original'])]=$this->cleanse($val['corrected']);
                }
            }
        }
        return $attributeSynonyms;
    }
    public function getAttrUniquePathList($articleAttributes, $uniquePathList,$attrData) {
        $type =  $attrData['type'];
        $category =  $attrData['category'];
        $brand  = $attrData['brand'];
        $gender = $attrData['gender'];
	$cPattern = $attrData['cPattern'];
        $categoryType  = $attrData['categoryType'];
        $keyword = false;
        
        foreach ($articleAttributes as $attribute) {
            $attribute = $this->cleanse($attribute);
            switch($type) {
            case 'C':
                if(!$this->isCatInAttr($attribute,$category)) {
                    $keyword = $this->joinWords($attribute, $category);
                    $uniquePathList[$keyword] = array("priority" => $this->attrPriority, "pattern"=> 'att,'.$cPattern); 
                } else {
                    $keyword = $attribute;
                    $uniquePathList[$attribute] = array("priority" => $this->attrPriority, "pattern"=> 'att'); 
                }
                break;
            case 'B':
                if(!$this->isCatInAttr($attribute,$category)) {
                    $keyword = $this->joinWords($brand, $attribute, $category);
                    $uniquePathList[$keyword] = array("priority"=>$this->attrPriority, "pattern"=>'b,att,'.$cPattern);
                } else {
                    $keyword = $this->joinWords($brand, $attribute);
                    $uniquePathList[$keyword] = array("priority"=>$this->attrPriority, "pattern"=>'b,att');
                }
                break;
            case 'G':
                if(!$this->isCatInAttr($attribute,$category)) {
                    $uniquePathList[$this->joinWords($gender, $attribute, $category)] = array("priority"=>$this->attrPriority,"pattern"=>'g,att,'.$cPattern);
                } else {
                    $uniquePathList[$this->joinWords($gender, $attribute)] = array("priority"=>$this->attrPriority,"pattern"=>'g,att');
                }
                break;
            }

            if($categoryType == "global_attr_master_category_facet" || $categoryType  == "global_attr_sub_category_facet") {
                if($brand) {
                    if(is_array($this->attributeUrlMap[$brand])) {
                        if(is_array($this->attributeUrlMap[$brand][$attribute])) {
                            if($this->attributeUrlMap[$brand][$attribute]['val'] == $this->getNumberOfProducts($keyword)) {
                                $this->splicedUrls[] = $this->attributeUrlMap[$brand][$attribute]['key'];
                            }
                        } else {
                            $this->attributeUrlMap[$brand][$attribute] = array();
                            $this->attributeUrlMap[$brand][$attribute]['key'] = $keyword;
                            $this->attributeUrlMap[$brand][$attribute]['val'] = $this->getNumberOfProducts($keyword);
                        }
                    } else {
                        $this->attributeUrlMap[$brand] =  array();
                        $this->attributeUrlMap[$brand][$attribute] = array();
                        $this->attributeUrlMap[$brand][$attribute]['key'] = $keyword;
                        $this->attributeUrlMap[$brand][$attribute]['val'] = $this->getNumberOfProducts($keyword);
                    }
                } else {
                    if(is_array($this->attributeUrlMap[$attribute])) {
                        if($this->attributeUrlMap[$attribute]['val'] == $this->getNumberOfProducts($keyword)) {
                            $this->splicedUrls[] = $this->attributeUrlMap[$attribute]['key'];
                        }		
                    } else {
                        $this->attributeUrlMap[$attribute] = array();
                        $this->attributeUrlMap[$attribute]['val'] = $this->getNumberOfProducts($keyword);
                        $this->attributeUrlMap[$attribute]['key'] = $keyword;
                    }
                }
            }
        }
        return $uniquePathList; 
    }
    
    public function getNumberOfProducts($keyword) {
        $keyword = implode(" ",explode('-',$keyword));
        $res = $this->dataObject->searchIndex("(( full_text_myntra:($keyword)) AND (count_options_availbale:[1 TO *]))");
        return $res->numFound;
    }		
    public function getLandingPageUrls() {
        // urls generated based on category/sub-category/article-type/brand/gender combinations
        $keywordUrls = array();
        foreach ($this->obj as $categories) {
            foreach ($categories['keywords'] as $keywords) {
                $keywordUrls[] = $keywords['keyword'];
            }
        }

        // landing pages are configured in the SEOAdmin tool
        $landingPageUrls = array();
        $langingPageData = array();
        $rows = func_query('select url, updated_on from adseo_pagerule', true);
        foreach ($rows as $row) {
            $url = str_replace('http://www.myntra.com/', '', $row['url']);
            $landingPageUrls[] = $url;
            $landingPageData[$url] = array(
                'lastmod' => $row['updated_on']
            );
        }

        // exclude the landing pages that are already covered in the keyword based urls
        $diffUrls = array_diff($landingPageUrls, $keywordUrls);

        $data = array();
        $configured_synonyms = $this->getSynonymsFromDB();
        foreach ($diffUrls as $url) {
			$lastMod = str_replace(' ', 'T', $landingPageData[$url]['lastmod']) . '+05:30';            
            $url = $this->replaceSynonymsInPath($url, $configured_synonyms);
            $data[] = array(
                'url' => $this->myn.$url,
                // assume the datetime stored in the DB is IST and use ISO date format
                'lastmod' => $lastMod
            );
        }

        return $data;
    }

	private function calculateNumPDPUrls($category, $categoryType){
		if ($categoryType != "global_attr_article_type_facet")	return 0;
		//aditional solr query
		return $this->executeQuery($category, 0, 1)->numFound;
	}
	
	
	private function getFacetedList($query, $facetField){
		$returnList = array();
		$query .= " count_options_availbale:[1 TO *] ";
		$results = $this->dataObject->facetedSearch($query,$facetField);
		foreach (get_object_vars($results->$facetField) as $key=>$val) {
			$key = trim($key);
			if(($facetField == "sizes_facet") && 
                (strstr($key,"freesize") || strstr($key,"-") || strstr($key,"/") )) {
			    continue;
			}
			$returnList[] = str_ireplace("apparel","clothing",$key);
		}
		return $returnList;
	}

    private function getAttributeFacetedList($query,$facetFields,$filters,$minResultCount = 30) {
        $returnList = array();
	    $facetResults = array();
    	$query .= " count_options_availbale:[1 TO *] ";
    	$facetList = array_chunk($facetFields,10);
	    foreach($facetList as $val) {
    	   $temp = $this->dataObject->multipleFacetedSearch($query, $val,0,0,$minResultCount)->facet_fields;
	       $facetResults = array_merge((array)$facetResults,(array)$temp);
    	}
        foreach ($facetFields as $key) {
	    	$vals = get_object_vars($facetResults[$key]);
		    foreach($vals as $k=>$v){
		        $k = strtolower($k); 
    		    if($filters[$key]) {
	        	    if($filters[$key]['disallow']) {
        			    if(in_array($k,strtolower($filters[$key]['disallow']))) continue;
			        }
        			if($filters[$key]['allow']) {
		        	    if(!in_array($k,strtolower($filters[$key]['allow']))) continue;
			        }
                }
    		    if ($k == "_empty_" || $k == 'na' || $k == 'other' || $k == 'others') continue;
    	        $returnList[]= str_ireplace("apparel","clothing",$k);
	        }
        }
        return $returnList;
    }

	private function cleanse($word){
		$word = preg_replace("/[^a-zA-Z0-9\s\-]/", "", trim($word));
		$word = preg_replace("/[\s-]+/","-",$word);
		return $word;
	}

    private function joinWords() {
        $words = func_get_args();
        foreach ($words as &$word) {
            $word = strtolower($this->cleanse($word));
        }

        return implode('-', $words);
    }
	

	public function hasMore() {
		return $this->hasMore;
	}
	
	public function getFileType() {
		return $this->fileType;
	}

	public function getFileName() {
		return $this->fileName;
	}
	public function getData() {
		$readListData = $readPDPData = false;
		$startIndex = 0;
		$endIndex = 0;
		if($this->firstLevelIndex == -1) $this->firstLevelIndex = 0;
	
		$numListRead = ( $this->obj[$this->firstLevelIndex]['list_file_count'] + 1)
						* self::MAX_URLS_PER_SITEMAP;
		$numPDPRead =  ( $this->obj[$this->firstLevelIndex]['pdp_file_count'] + 1) 
						* self::MAX_URLS_PER_SITEMAP;
		if(($numListRead >= $this->obj[$this->firstLevelIndex]['num_list_urls']) && 
			( $numPDPRead >= $this->obj[$this->firstLevelIndex]['num_pdp_urls'])) {
		//move to next category
			if($this->firstLevelIndex ==  (count($this->obj) - 1 )) {
				$this->hasMore= false;
				$this->fileName = '';
				return null; 
			}
			$this->firstLevelIndex++;
			$numListRead = 0;
			$numPDPRead = 0; 
		}	
		
		if($this->obj[$this->firstLevelIndex]['num_list_urls'] <= $numListRead) {
			if($this->obj[$this->firstLevelIndex]['num_pdp_urls'] > $numPDPRead)	{
				$readPDPData = true;
				$numRead = $numPDPRead;
			}
		} else {
			$readListData = true;
			$numRead = $numListRead;
		} 

		$startIndex = $numRead;
		$endIndex = $startIndex + self::MAX_URLS_PER_SITEMAP;

		if($readListData) {
			$this->fileName = "sitemap-list-".$this->obj[$this->firstLevelIndex]['file_name']; 
			$this->fileType = "list"; 
			$this->obj[$this->firstLevelIndex]['list_file_count']++;
		
			if($this->obj[$this->firstLevelIndex]['list_file_count'] != 0) {
				$this->fileName .= $this->obj[$this->firstLevelIndex]['list_file_count'];
			}
			return array_slice($this->obj[$this->firstLevelIndex]['keywords'],$startIndex, self::MAX_URLS_PER_SITEMAP);
		}
	
		if($readPDPData)	{
			$this->fileType = "pdp"; 
			$this->fileName = "sitemap-pdp-".$this->obj[$this->firstLevelIndex]['file_name'];  
			$this->obj[$this->firstLevelIndex]['pdp_file_count']++;
			if($this->obj[$this->firstLevelIndex]['pdp_file_count'] != 0) {
				$this->fileName .= $this->obj[$this->firstLevelIndex]['pdp_file_count'];
			}
	
			return $this->executeQuery($this->obj[$this->firstLevelIndex]['category'], $startIndex, self::MAX_URLS_PER_SITEMAP)->docs;
		}
	}

    // this function is used for generating the pdp urls.
    // here we don't need to check the availability in stock (ie., appending the 'count_options_availbale:[1 TO *]' in the solr query)
	protected function executeQuery($articleType, $offset,$limit, $params=array()) {
		if(!isset($params['fl'])) $params['fl'] = $this->fields;
		if(isset($params['sort'])) $this->sort = $params['sort'];
		$query = $this->query;
		$query .= "\"".$articleType."\"";
		// $query .= " count_options_availbale:[1 TO *] ";
		return $this->dataObject->searchAndSort($query, $offset, $limit, $this->sort,array('fl'=>$this->fields));
	}
		

	public function getUrlFormatted($row = array(), $urlType) {

		if(empty($row)) return '';
		$r = $row;
		
		if ($urlType == "list") {
		    return $this->myn.$r['keyword'];
		}
		
		$styleid = str_replace('0_style_','',$r->id);
		if(empty($r->global_attr_brand)) {
			$brandname = 'branded';
		} else {
			$brandname = $r->global_attr_brand;
		}
		if(empty($r->global_attr_article_type)) {
			$articletype = 'lifestyle';
		} else {
			$articletype = $r->global_attr_article_type;
		}
		$stylename = $r->product;

		$url = construct_style_url($articletype,$brandname,$stylename,$styleid);
		$url = $this->myn.$url;
		return $url;
	}
}

/*
 * Class for fetch images for each product
 * Create SitemapDataFetcherMysql class for Mysql functions when needed , and inherit that class instead of SitemapMyntra
 */
 
class ImageSitemapData extends SitemapMyntra{

	protected $query= "SELECT style_id, default_image, front_image, left_image, right_image, back_image, top_image, bottom_image, search_image, search_zoom_image from mk_style_properties where style_id IN ('";
	protected $sort = 'styleid asc';
	protected $styleIdArr = array();
	protected $urlIdArr = array();
	
	public function __construct() {
	}

	//Pass product style id and Url associated with that ID
	public function addProductDetails($data=array()) {
		if(isset($data['styleid']) && !empty($data['url'])) {
			$this->styleIdArr[] = $data['styleid'];
			$this->urlIdArr[$data['styleid']] = $data['url'];
			return true;
		}
		else return false;
	}
	
	// Final function to get data for all the product style_ids passed to the class
	public function getData() {
		return $this->getImagesFromDB();	
	}
	
	protected function getImagesFromDB() {
		$sql = $this->query.implode("','",$this->styleIdArr)."')";
		$properties=func_query($sql);
		
	 	if(!empty($properties)) {
        	foreach ($properties as $p) {
        		$image_locs = array();
        		$style_id = $p['style_id'];
        		if(!empty($p['default_image'])) $image_locs[] = array('loc'=>$p['default_image']);
        		if(!empty($p['front_image'])) $image_locs[] = array('loc'=>$p['front_image']);
        		if(!empty($p['left_image'])) $image_locs[] = array('loc'=>$p['left_image']); 
        		if(!empty($p['right_image'])) $image_locs[] = array('loc'=>$p['right_image']);
        		if(!empty($p['back_image'])) $image_locs[] = array('loc'=>$p['back_image']);
        		if(!empty($p['top_image'])) $image_locs[] = array('loc'=>$p['top_image']);
        		if(!empty($p['bottom_image'])) $image_locs[] = array('loc'=>$p['bottom_image']);
        		if(!empty($p['search_image'])) $image_locs[] = array('loc'=>$p['search_image']);
        		if(!empty($p['search_zoom_image'])) $image_locs[] = array('loc'=>$p['search_zoom_image']);
	        	if(!empty($image_locs)) {
	        		$result['url'] = $this->urlIdArr[$style_id];
	        		$result['image'] = $image_locs;
	        		$this->result[] = $result;
		        }
        	}
		}
		unset($properties);
		return $this->result;
	}
}
