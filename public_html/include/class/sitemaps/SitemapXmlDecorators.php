<?php
/*
 * Decorators for adding XML DOM elements - used in SitemapGenerator Classes
 * Following are supported :-
 * Root element - via SitemapDom
 * Location, Lastmdo adn Image Elements
 */
interface SitemapDomInterface {
	
	public function getDomElement();
	
	public function getDomData();
	
	public  function getDoc();
}


class SitemapDom implements SitemapDomInterface {
	private $domData = array();
	private $root; //Dom root element
	private $doc; //DOM
	private $elem;
	private $urlset;
	
	public function __construct($doc,$urlset, $root,$domData=array()) {
		$this->root = $root;
		$this->doc = $doc;
		$this->domData = $domData;
		$this->urlset = $urlset;
		$this->elem = $this->urlset->appendChild($this->doc->createElement($this->root));
	}
	
	public function getDomElement() {
		return $this->elem;
	}
	public function getDomData() {
		return $this->domData;
	}
	public  function getDoc() {
		return $this->doc;
	}
}

abstract class SitemapDomDecorator implements SitemapDomInterface {
	
	/*
	 * Following functions are declared in interface. 
	 * Can not redeclare due to bug in PHP
	 * https://bugs.php.net/bug.php?id=43200
	 * 
	abstract public function getDomElement();
	
	abstract public  function getDoc();
		
	abstract public function getDomData();
	*/
	
}

class SitemapDomLocation extends SitemapDomDecorator{
	
	private $domElemObj;
	
	public function __construct($domElemObj) {
		$this->domElemObj = $domElemObj;
	}
	
	public function getDomElement() {
		return $this->domElemObj->getDomElement();
	}
	
	public function getDoc() {
		$d = $this->domElemObj->getDoc();
		$data = $this->domElemObj->getDomData();
		$elem = $this->domElemObj->getDomElement();
		
		$elem = $elem->appendChild($d->createElement("loc"));
		$elem->appendChild($d->createTextNode($data['loc']));


		
		return $d;
	}
	
	public function getDomData() {
		return $this->domElemObj->getDomData();
	}
}


class SitemapDomxhtml extends SitemapDomDecorator{
	
	private $domElemObj;
	
	public function __construct($domElemObj) {
		$this->domElemObj = $domElemObj;
	}
	
	public function getDomElement() {
		return $this->domElemObj->getDomElement();
	}
	
	public function getDoc() {
		$d = $this->domElemObj->getDoc();
		$data = $this->domElemObj->getDomData();
		$elem = $this->domElemObj->getDomElement();
		$theurl = $data['loc'];
		$theurl_pos = strpos($theurl, 'myntra.com');
		if(!!$theurl_pos && $theurl_pos>0 ){ 
			$theandroidurl = "android-app://com.myntra.android/myntra/".strstr($theurl, 'myntra.com');
			$elem = $elem->appendChild($d->createElement("xhtml:link"));
			$elem->setAttribute("rel","alternate");
			$elem->setAttribute("href",$theandroidurl);
		} 
		return $d;
	}
	
	public function getDomData() {
		return $this->domElemObj->getDomData();
	}
}


class SitemapDomLastmod extends SitemapDomDecorator{
	
	private $domElemObj;
	
	public function __construct($domElemObj) {
		$this->domElemObj = $domElemObj;
	}
	
	public function getDomElement() {
		return $this->domElemObj->getDomElement();
	}
	
	public function getDoc() {
		$d = $this->domElemObj->getDoc();
		$data = $this->domElemObj->getDomData();
		$elem = $this->domElemObj->getDomElement();
		$elem = $elem->appendChild($d->createElement("lastmod"));
		$elem->appendChild($d->createTextNode($data['lastmod']));
		return $d;
	}
	
	public function getDomData() {
		return $this->domElemObj->getDomData();
	}
}

class SitemapDomPriority extends SitemapDomDecorator{
	
	private $domElemObj;
	
	public function __construct($domElemObj) {
		$this->domElemObj = $domElemObj;
	}
	
	public function getDomElement() {
		return $this->domElemObj->getDomElement();
	}
	
	public function getDoc() {
		$d = $this->domElemObj->getDoc();
		$data = $this->domElemObj->getDomData();
		$elem = $this->domElemObj->getDomElement();
		$elem = $elem->appendChild($d->createElement("priority"));
		$elem->appendChild($d->createTextNode($data['priority']));
		return $d;
	}
	
	public function getDomData() {
		return $this->domElemObj->getDomData();
	}
}
class SitemapDomImageLoc extends SitemapDomDecorator{
	
	private $domElemObj;
	
	public function __construct($domElemObj) {
		$this->domElemObj = $domElemObj;
	}
	
	public function getDomElement() {
		return $this->domElemObj->getDomElement();
	}
	
	public function getDoc() {	
		$d = $this->domElemObj->getDoc();
		$data = $this->domElemObj->getDomData();
		$elem = $this->domElemObj->getDomElement();
		
		foreach($data['image'] as $image) {
			$img= $elem->appendChild($d->createElement("image:image"));
			$img_loc = $img->appendChild($d->createElement("image:loc"));
			$img_loc = $img_loc->appendChild($d->createTextNode($image['loc']));
		}
		return $d;
	}
	
	public function getDomData() {
		return $this->domElemObj->getDomData();
	}
}