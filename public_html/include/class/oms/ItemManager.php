<?php
/*
 * Created on Feb 3, 2012
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
 
include_once("$xcart_dir/modules/apiclient/SkuApiClient.php");
include_once("$xcart_dir/modules/apiclient/ItemApiClient.php"); 
include_once("$xcart_dir/include/func/func.mkcore.php");
include_once("$xcart_dir/include/func/func_sku.php");
include_once("$xcart_dir/include/func/func.mk_orderbook.php");
include_once("$xcart_dir/include/func/func.order.php");
include_once("$xcart_dir/include/func/func.user.php");
include_once("$xcart_dir/modules/coupon/database/CouponAdapter.php");
include_once("$xcart_dir/include/class/class.mail.multiprovidermail.php");
include_once("$xcart_dir/env/Host.config.php");
include_once("$xcart_dir/modules/apiclient/ReleaseApiClient.php");
include_once("$xcart_dir/include/class/oms/EventCreationManager.php");
include_once($xcart_dir."/include/class/class.feature_gate.keyvaluepair.php");
include_once "$xcart_dir/modules/myntCash/MyntCashTransaction.php";
include_once "$xcart_dir/modules/myntCash/MyntCashService.php";
include_once "$xcart_dir/modules/loyaltyPoints/LoyaltyPointsPortalClient.php";
include_once("$xcart_dir/include/class/widget/class.widget.keyvaluepair.php");


 
class ItemManager {
 	
 	public static function assignItems($order, $itemid2QtyMap, $itemid2ItemMap, $itemid2OptionQtyMap) {

		$order['total'] = 0.00;
		$order['subtotal'] = 0.00;
		$order['coupon_discount'] = 0.00;
		$order['discount'] = 0.00;
		$order['pg_discount'] = 0.00;
		$order['cash_redeemed'] = 0.00;
		$order['loyalty_points_used'] = 0.00;
		$order['loyalty_points_awarded'] = 0.00;
		$order['earned_credit_usage'] = 0.00;
		$order['store_credit_usage'] = 0.00;
		$order['cashback'] = 0.00;
		$order['qtyInOrder'] = 0;
		$order['tax'] = 0.00;
		$order['cart_discount'] = 0.00;
        $order['gift_card_amount'] = 0.00;
		
		$order['items'] = array();
		
		$order['firstname'] = mysql_real_escape_string($order['firstname']);
		$order['lastname'] = mysql_real_escape_string($order['lastname']);
		$order['s_firstname'] = mysql_real_escape_string($order['s_firstname']);
		$order['s_lastname'] = mysql_real_escape_string($order['s_lastname']);
		$order['s_address'] = mysql_real_escape_string($order['s_address']);
		$order['s_city'] = mysql_real_escape_string($order['s_city']);
		$order['b_address'] = mysql_real_escape_string($order['b_address']);
		$order['b_city'] = mysql_real_escape_string($order['b_city']);
		
 		foreach($itemid2QtyMap as $itemid=>$qty) {
 			$item = $itemid2ItemMap[$itemid];
 			
 			$per_item_vat = $item['taxamount']/$item['amount'];
			$per_item_coupon_discount = $item['coupon_discount_product']/$item['amount'];
			$per_item_cash_redeemed = $item['cash_redeemed']/$item['amount'];
			$per_item_earned_credit_usage= $item['earned_credit_usage']/$item['amount'];
			$per_item_store_credit_usage = $item['store_credit_usage']/$item['amount'];
			$per_item_discount = $item['discount']/$item['amount'];
			$per_item_pg_discount = $item['pg_discount']/$item['amount'];
			$per_item_cb = $item['cashback']/$item['amount'];
			$per_item_cart_discount = $item['cart_discount_split_on_ratio']/$item['amount'];
			$per_item_loyalty_points_used = $item['item_loyalty_points_used']/$item['amount'];
			$per_item_loyalty_points_awarded = $item['item_loyalty_points_awarded']/$item['amount'];
            $per_item_gift_card_amount = $item['gift_card_amount']/$item['amount'];
			
			$item['amount'] = $qty;
			$item['taxamount'] = $per_item_vat*$qty;
			$item['discount'] = $per_item_discount*$qty;
			$item['cart_discount_split_on_ratio'] = $per_item_cart_discount * $qty;
			$item['total_amount'] = $item['price']*$qty - $item['discount'] - $item['cart_discount_split_on_ratio'];
			$item['coupon_discount_product'] = $per_item_coupon_discount*$qty;
			$item['cash_redeemed'] = $per_item_cash_redeemed*$qty;
			$item['earned_credit_usage'] = $per_item_earned_credit_usage*$qty;
			$item['store_credit_usage'] = $per_item_store_credit_usage*$qty;
			$item['pg_discount'] = $per_item_pg_discount*$qty;
			$item['cashback'] = $per_item_cb*$qty;
            $item['item_loyalty_points_used'] = $per_item_loyalty_points_used*$qty;
            $item['item_loyalty_points_awarded'] = $per_item_loyalty_points_awarded*$qty;
            $item['gift_card_amount'] = $per_item_gift_card_amount*$qty;

			$order['subtotal'] += $item['price']*$qty;
			$order['coupon_discount'] += $item['coupon_discount_product'];
			$order['discount'] += $item['discount'];
			$order['pg_discount'] += $item['pg_discount'];
			$order['cash_redeemed'] += $item['cash_redeemed'];
			$order['earned_credit_usage'] += $item['earned_credit_usage'];
			$order['store_credit_usage'] += $item['store_credit_usage'];
			$order['loyalty_points_used'] += $item['item_loyalty_points_used'];
			$order['loyalty_points_awarded'] += $item['item_loyalty_points_awarded'];
            $order['cashback'] += $item['cashback'];
			$order['tax'] += $item['taxamount'];
			$order['qtyInOrder'] += $qty;
			$order['cart_discount'] += $item['cart_discount_split_on_ratio'];
            $order['gift_card_amount'] += $item['gift_card_amount'];
			$order['total'] += ($item['total_amount'] + $item['taxamount'] - $item['coupon_discount_product'] 
                    - $item['cash_redeemed'] - $item['pg_discount'] - $item['item_loyalty_points_used']*$order['loyalty_points_conversion_factor']);
			
			$optionQtyMapping = $itemid2OptionQtyMap[$itemid];
			$optionQtyMapping['quantity'] = $qty;
			$item['option_qty_mapping'] = $optionQtyMapping;
			
			$order['items'][] = $item;
 		}
 		
 		return $order;
 	}
 	
 	public static function updateOrderItemMovement(&$from_order, &$to_order, &$item, $qty_to_move) {
 		global $weblog;
 		if($item['amount'] == $qty_to_move) {
 			$new_item = false;
 			$weblog->info("Full item needs to be moved.. Moving to new order id");
 			if(isset($to_order['orderid']))
 				$item['orderid'] = $to_order['orderid'];
 			else
 				unset($item['orderid']);
 				
 			ItemManager::updateOrderAmountDetailsForItemMovement($from_order, $item, $to_order);	
 		} else {
 			$new_item = ItemManager::splitItem($item, $qty_to_move);
 			
			$new_item['item_status'] =  'A';
			$new_item['assignee'] =  1;
			$new_item['assignment_time'] = time();
			if(isset($to_order['orderid']))
 				$new_item['orderid'] = $to_order['orderid'];
 			else
 				unset($new_item['orderid']);
			ItemManager::updateOrderAmountDetailsForItemMovement($from_order, $new_item, $to_order);		
 		}
 		
 		return $new_item;
 	} 
 	
 	public static function cancelItemFromOrder(&$order, &$item, $qty_to_cancel, $update_inv, $user, &$new_item) {
		global $weblog;
		$errorMsg = ""; 		
 		$status = true;
 		
 		if($update_inv && $item['sku_id']) {
 			if($myntraItem)
				$status = SkuApiClient::updateSkuBlockedOrderCount(array($item['sku_id']=>$qty_to_cancel), false, $user, $order['warehouseid']);
 		}
 		
		if($status === false) {
			$errorMsg = "We were not able to decrement the blocked count for the deleted item skus for itemid - ".$item['itemid'];
		} else {
			$removedItemBarcodes = true;
			if($item['sku_id'])
				$removedItemBarcodes = ItemApiClient::cancelItemsFromOrder($order['orderid'], $order['warehouseid'], $item['sku_id'], $item['amount'], $qty_to_cancel, $user);
			
			if ($removedItemBarcodes === false) {
				// revert the update blocked order count...
				if($update_inv) {
					if($myntraItem)
						SkuApiClient::updateSkuBlockedOrderCount(array($item['sku_id']=>$qty_to_cancel), true, $user, $order['warehouseid']);
				}
				$errorMsg = "We are not able to delete skuitems association with order for itemid - ".$item['itemid'];
			} else {
				$qapass_items = $item['qapass_items'];
				$qapass_items = explode(",", $qapass_items);
				$weblog->info("QA Pass Items - ". print_r($qapass_items, true));
				
				if(count($qapass_items) > 0) {
					$item_removed_from_qapass = false;
					$new_qapass_items = ""; 
					// check if the cancelled item was marked qa pass
					$weblog->info("Removed Items - ". print_r($removedItemBarcodes, true));
					foreach ($qapass_items as $itembarcode) {
						if(!in_array($itembarcode, $removedItemBarcodes)) {
							$weblog->info("Item $itembarcode is not deleted keep in pass list");
							$new_qapass_items[] = $itembarcode;
						} else {
							$weblog->info("Item $itembarcode is deleted removing from qapass");
							$item_removed_from_qapass = true;
						}
					}
					
					if($item_removed_from_qapass) {
						if(count($new_qapass_items) > 0)
							$new_qapass_items = implode(",", $new_qapass_items);
						else
							$new_qapass_items = "";
						
						func_array2update("order_details", array('item_status'=>'A', 'qapass_items'=>$new_qapass_items), "itemid = ". $item['itemid']);
					}
				}
			}
		}
		
		if($errorMsg == "") {
			//cancel complete item...
			if($qty_to_cancel == $item['amount']) {
				$item['item_status'] = 'IC';
				$item['canceltime'] = time();
				ItemManager::updateOrderAmountDetailsForItemMovement($order, $item);
				$new_item = false;
			} else { // cancel part of the item
				$new_item = ItemManager::splitItem($item, $qty_to_cancel);
				
				$new_item['item_status'] = 'IC';
				$new_item['canceltime'] = time();
				ItemManager::updateOrderAmountDetailsForItemMovement($order, $new_item);
			}
		}
		
		return $errorMsg;
 	}
 	
 	private static function updateOrderAmountDetailsForItemMovement(&$from_order, $item_to_move, &$to_order=false) {
        $from_order['total'] -=  ($item_to_move['price']*$item_to_move['amount'] + $item_to_move['taxamount']
                - $item_to_move['coupon_discount_product'] - $item_to_move['discount'] - $item_to_move['cart_discount_split_on_ratio'] 
                - $item_to_move['cash_redeemed'] - $item_to_move['pg_discount'] 
                - $item_to_move['item_loyalty_points_used'] * $from_order['loyalty_points_conversion_factor']);
		$from_order['subtotal'] -= ($item_to_move['price']*$item_to_move['amount']);
		$from_order['coupon_discount'] -= $item_to_move['coupon_discount_product'];
		$from_order['discount'] -= $item_to_move['discount'];
		$from_order['pg_discount'] -= $item_to_move['pg_discount'];
		$from_order['cash_redeemed'] -= $item_to_move['cash_redeemed'];
		$from_order['cashback'] -= $item_to_move['cashback'];
		$from_order['earned_credit_usage'] -= $item_to_move['earned_credit_usage'];
		$from_order['store_credit_usage'] -= $item_to_move['store_credit_usage'];
		$from_order['loyalty_points_used'] -= $item_to_move['item_loyalty_points_used'];
		$from_order['loyalty_points_awarded'] -= $item_to_move['item_loyalty_points_awarded'];
		$from_order['tax'] -= $item_to_move['taxamount'];
		$from_order['qtyInOrder'] -= $item_to_move['amount'];
		$from_order['cart_discount'] -= $item_to_move['cart_discount_split_on_ratio'];
		$from_order['gift_card_amount'] -= $item_to_move['gift_card_amount'];

		if($to_order) {
			$to_order['total'] += ($item_to_move['price']*$item_to_move['amount'] + $item_to_move['taxamount'] 
                    - $item_to_move['coupon_discount_product'] - $item_to_move['discount'] - $item_to_move['cart_discount_split_on_ratio'] 
                    - $item_to_move['cash_redeemed'] - $item_to_move['pg_discount']
                    - $item_to_move['item_loyalty_points_used'] * $from_order['loyalty_points_conversion_factor']);
			$to_order['subtotal'] += ($item_to_move['price']*$item_to_move['amount']);
			$to_order['coupon_discount'] += $item_to_move['coupon_discount_product'];
			$to_order['discount'] += $item_to_move['discount'];
			$to_order['pg_discount'] += $item_to_move['pg_discount'];
			$to_order['cash_redeemed'] += $item_to_move['cash_redeemed'];
			$to_order['earned_credit_usage'] += $item_to_move['earned_credit_usage'];
			$to_order['store_credit_usage'] += $item_to_move['store_credit_usage'];
            $to_order['loyalty_points_used'] += $item_to_move['item_loyalty_points_used'];
            $to_order['loyalty_points_awarded'] += $item_to_move['item_loyalty_points_awarded'];
			$to_order['cashback'] += $item_to_move['cashback'];
			$to_order['tax'] += $item_to_move['taxamount'];
			$to_order['qtyInOrder'] += $item_to_move['amount'];
			$to_order['cart_discount'] += $item_to_move['cart_discount_split_on_ratio'];
            $to_order['gift_card_amount'] += $item_to_move['gift_card_amount'];
		}
 	}
 	
 	public static function splitItem(&$item, $qty_to_split) {
 		global $weblog;
 		$per_item_vat = $item['taxamount']/$item['amount'];
		$per_item_coupon_discount = $item['coupon_discount_product']/$item['amount'];
		$per_item_cash_redeemed = $item['cash_redeemed']/$item['amount'];
		$per_item_earned_credit_usage = $item['earned_credit_usage']/$item['amount'];
		$per_item_store_credit_usage = $item['store_credit_usage']/$item['amount'];
		$per_item_loyalty_points_used = $item['item_loyalty_points_used']/$item['amount'];
		$per_item_loyalty_points_awarded = $item['item_loyalty_points_awarded']/$item['amount'];
		
		$per_item_discount = $item['discount']/$item['amount'];
		$per_item_pg_discount = $item['pg_discount']/$item['amount'];
		$per_item_cb = $item['cashback']/$item['amount'];
		$per_item_cart_discount = $item['cart_discount_split_on_ratio']/$item['amount'];
		$per_item_difference_refund = $item['difference_refund']/$item['amount'];
        $per_item_gift_card_amount = $item['gift_card_amount']/$item['amount'];

		$new_item = $item;
		unset($new_item['itemid']);
		unset($new_item['qapass_items']);
                $new_item['qapass_items'] = '';
		//$new_product_details = ItemManager::createNewProductid();
		//$new_item['productid'] = $new_product_details['productid'];
		//$weblog->info("Part of item needs to be moved.. Creating new itemid");
		
        $new_item['is_packaged'] = 0;
		$new_item['amount'] = $qty_to_split;
		$new_item['discount_quantity'] = $new_item['amount'];
		$new_item['taxamount'] =  $per_item_vat * $new_item['amount'];
		$new_item['discount'] = $per_item_discount * $new_item['amount'];
		$new_item['pg_discount'] = $per_item_pg_discount * $new_item['amount'];
		$new_item['cash_redeemed'] = $per_item_cash_redeemed * $new_item['amount'];
		$new_item['store_credit_usage'] = $per_item_store_credit_usage * $new_item['amount'];
		$new_item['earned_credit_usage'] = $per_item_earned_credit_usage * $new_item['amount'];
		$new_item['item_loyalty_points_used'] = $per_item_loyalty_points_used * $new_item['amount'];
		$new_item['item_loyalty_points_awarded'] = $per_item_loyalty_points_awarded * $new_item['amount'];
		
		$new_item['cashback'] = $per_item_cb*$new_item['amount'];
		$new_item['coupon_discount_product'] = $per_item_coupon_discount * $new_item['amount'];
		$new_item['cart_discount_split_on_ratio'] = $per_item_cart_discount * $new_item['amount'];
		$new_item['total_amount'] = $new_item['price']*$new_item['amount'] - $new_item['discount'] - $new_item['cart_discount_split_on_ratio'];
		$new_item['difference_refund'] = $per_item_difference_refund * $new_item['amount'];
        $new_item['gift_card_amount'] = $per_item_gift_card_amount * $new_item['amount'];
		
		$item['amount'] -=  $qty_to_split; 
		$item['discount_quantity'] = $item['amount'];
		$item['taxamount'] =  $per_item_vat * $item['amount'];
		$item['discount'] = $per_item_discount * $item['amount'];
		$item['pg_discount'] = $per_item_pg_discount * $item['amount'];
		$item['cash_redeemed'] = $per_item_cash_redeemed * $item['amount'];
		$item['store_credit_usage'] = $per_item_store_credit_usage * $item['amount'];
		$item['earned_credit_usage'] = $per_item_earned_credit_usage * $item['amount'];
        $item['item_loyalty_points_used'] = $per_item_loyalty_points_used * $item['amount'];
		$item['item_loyalty_points_awarded'] = $per_item_loyalty_points_awarded * $item['amount'];
		$item['cashback'] = $per_item_cb * $item['amount'];
		$item['coupon_discount_product'] = $per_item_coupon_discount * $item['amount'];
		$item['cart_discount_split_on_ratio'] = $per_item_cart_discount * $item['amount'];
		$item['difference_refund'] = $per_item_difference_refund * $item['amount'];
		$item['total_amount'] = $item['price']*$item['amount'] - $item['discount'] - $item['cart_discount_split_on_ratio'];
        $item['gift_card_amount'] = $per_item_gift_card_amount * $item['amount'];
		
		$old_option_qty_row = func_query_first("SELECT * from mk_order_item_option_quantity where itemid = ".$item['itemid']);
		$old_option_qty_row['quantity'] = $item['amount'];
		$item['item_option_qty'] = $old_option_qty_row;
			
		$new_option_qty_row = array('optionid'=>$old_option_qty_row['optionid'],
									 'quantity'=>$qty_to_split);
		$new_item['item_option_qty'] = $new_option_qty_row;

		return $new_item;
 	}
 	
 	private static function createNewProductid() {
 		$len = 4;
		$time = time();
		$timefirstdigit = substr($time, 0, 3);
		$timelastdigit = substr($time, -3);
	    $productId = $timefirstdigit.get_rand_number($len).$timelastdigit;
	    
	    $productsql = "SELECT *	FROM xcart_products WHERE productid = $productId";
		$new_product_details = func_query_first($productsql);
		$new_product_details['productid'] = $productId;
		$new_product_details['productcode'] = "MK".$productId;
		return $new_product_details;
 	}
 	
 	public static function getProductDetailsForItemIds($itemIds, $orderid=false) {
 		global $weblog, $sql_tbl;
 		
 		if(!is_array($itemIds))
 			$itemIds = array($itemIds);
 		
 		$products_sql = "SELECT xod.*, xod.amount as quantity, sp.article_number AS article_number, po.value as size, som.sku_id, sp.default_image FROM ((((xcart_order_details xod LEFT JOIN mk_order_item_option_quantity oq on xod.itemid = oq.itemid) LEFT JOIN mk_style_properties sp on xod.product_style = sp.style_id) LEFT JOIN mk_product_options po on oq.optionid = po.id) LEFT JOIN mk_styles_options_skus_mapping som on oq.optionid = som.option_id) where xod.item_status <> 'IC' AND xod.itemid in (".implode(",", $itemIds).")";
		if($orderid)
			$products_sql .= " AND orderid = $orderid";
							
		$productsInCart = func_query($products_sql, true);
		
		if($productsInCart) {
			foreach($productsInCart as $key => $product){
				$productsInCart[$key]['style_id'] = $productsInCart[$key]['product_style'];
				$stylenamesql = "SELECT name, label FROM $sql_tbl[mk_product_style] WHERE id = ".$productsInCart[$key]['product_style']."";
				$stylerow = func_query_first($stylenamesql);
				$productsInCart[$key]['productStyleName'] = $stylerow['name'];
			    $productsInCart[$key]['productStyleLabel'] = $stylerow['label'];
			
			    /*$productname_sql = "SELECT product, image_portal_T AS ProdImage FROM xcart_products WHERE productid = ".$productsInCart[$key]['productid']."";
				$productrow = func_query_first($productname_sql, true);
			    if(!empty($productrow)) {
					$productsInCart[$key]['productName'] = $productrow['product'];
					if(preg_match('/http:\/\//',$productrow['ProdImage'])) {
			        	$productsInCart[$key]['designImagePath'] = $productrow['ProdImage'];
				    }else{
						$productsInCart[$key]['designImagePath'] = ".".$productrow['ProdImage'];
					}
			    }*/
			    if(!empty($product['default_image'])) {
					$productsInCart[$key]['designImagePath'] = str_replace("_images", "_images_96_128", $product['default_image']);
				}
			
				$totalPrice = $productsInCart[$key]['price']*$productsInCart[$key]['amount'];
				$productsInCart[$key]['totalPrice'] = number_format($totalPrice, 2);
			}
		}
		
		return $productsInCart;
 	}
 	
 	public static function getItemDetailsForItemIds($itemIds, $orderid=false) {
 		if(!is_array($itemIds)) 
 			$itemIds = array($itemIds);
 		
 		$item_details_sql = "Select od.*, od.amount as quantity, sku_id from ((xcart_order_details od LEFT JOIN mk_order_item_option_quantity oq on od.itemid = oq.itemid) LEFT JOIN mk_styles_options_skus_mapping som on oq.optionid = som.option_id) where od.itemid in (".implode(",", $itemIds).")";
 		
		if($orderid){
			$item_details_sql .= " AND od.orderid = $orderid";
		}
		$item_details = func_query($item_details_sql);
		
 		return $item_details;
 	}
 	
 	public static function calculateItemCancellationRefunds($item, $payment_method, $customer_login, &$total_cash_refund, 
            &$total_cb_deducted, &$total_coupon_discount_refund, &$total_used_loyalty_points_refunded, &$total_awarded_loyalty_points_reverted, $loyalty_points_conversion_factor, &$total_gift_card_amount_refund) {
 		
		$item_total = $item['price']*$item['amount'];
		$refund_coupon_discount = $item['coupon_discount_product'];
        $item_loyalty_points_used = $item['item_loyalty_points_used'];
        $item_loyalty_points_awarded = $item['item_loyalty_points_awarded'];
        $item_gift_card_amount = $item['gift_card_amount'];
        
		if($payment_method == 'cod') {
			$refund_amount = $item['cash_redeemed'] - $item['difference_refund'];
		} else {
			$refund_amount = $item['total_amount'] + $item['taxamount'] - $item['coupon_discount_product'] 
            - $item['difference_refund'] - $item_loyalty_points_used*$loyalty_points_conversion_factor;
		}
		$cb_to_deduct = $item['cashback'];
        		
		if($item['coupon_code']) {
			$couponAdapter = CouponAdapter::getInstance();
    		$couponAdapter->cancelCouponUsageForItem($item['coupon_code'], $customer_login, $item_total, $refund_coupon_discount);
    	}
			
		if($cb_to_deduct > 0) {
			$refund_amount -= $cb_to_deduct;
		}

        $total_gift_card_amount_refund += $item_gift_card_amount;
		$total_cash_refund += $refund_amount;
		$total_cb_deducted += $cb_to_deduct;
		$total_coupon_discount_refund += $refund_coupon_discount;
        $total_used_loyalty_points_refunded += $item_loyalty_points_used;
        $total_awarded_loyalty_points_reverted += $item_loyalty_points_awarded;
 	}
 	
 	private static function calculateAndIssueCouponRefund($item_details, $login, $subtotal, $coupon, $request=''){
		global $weblog,$_POST;	
		$couponValue = 0;
		$minPurchase = 0;
		foreach ($item_details as $item) {
			$qty_to_cancel = intval($_POST['item_quantity_'.$item['itemid']]);
			$couponValue += ($item['coupon_discount_product']*$qty_to_cancel/$item['amount']);
			$minPurchase += ($item['total_amount']*$qty_to_cancel/$item['amount']);
		}		
		$expirytime = time()+(30*24*60*60);
		$weblog->debug("couponValue ".$couponValue);
		$weblog->debug("minPurchase ".$minPurchase);
		
		$couponAdapter = CouponAdapter::getInstance();
		$couponType = WidgetKeyValuePairs::getWidgetValueForKey('cancellationCouponType');
		$couponData = $couponAdapter->generateCouponForCancellation($item_details[0]['coupon_code'],'CANC', 6, 'CANCELLATIONS', time(), 
						$expirytime, $login,$couponType, $couponValue, ($couponValue/$minPurchase) * 100, $minPurchase,'',true);
		return $couponData;	
	}
 	
 	public static function processItemCancelRefundAndNotifyCustomer($order, $item_details, $cancelled_itemids, $cancel_reason, $refund_to_cb, 
            $total_cash_refund, $total_coupon_discount_refund, $total_cb_deducted, $total_used_loyalty_points_refunded, 
            $total_awarded_loyalty_points_reverted, $cancel_reason_email_contents, $notify_customer,$original_subtotal,$cancelled_items_details, $total_gift_card_amount_refund) {
 		global $sql_tbl;
 		$refund_comment = "";
 		$couponAdapter = CouponAdapter::getInstance();
 		
 		if($order['ordertype'] == 'ex'){
            if($total_awarded_loyalty_points_reverted > 0.00 && !($order['status'] == 'OH' && $order['on_hold_reason_id_fk'] == 25)){
            	//For exchange, these need to be debited from the current order and credited to parent order.
                //find parent order.
                $originalOrder = func_query_first("SELECT releaseid from exchange_order_mappings where exchange_orderid = $orderid order by exchange_orderid desc", true);
                $originalOrderId = $originalOrder['releaseid'];
                $originalOrderDetails = func_query_first("SELECT orderid, group_id from xcart_orders where orderid = $originalOrderId", true);
                $original_group_id = $originalOrderDetails['group_id'];
                $ret = LoyaltyPointsPortalClient::creditDebitInactiveForExchangeOrder($order['login'], $orderid,$original_group_id, $total_awarded_loyalty_points_reverted,"Reversing credit of points for order no ".$order[group_id],"Awarding points for original order no ".$original_group_id);
                if(!$ret || $ret['status']['statusType'] == 'ERROR'){
                    $refund_comment .= "Reverting ".number_format($total_awarded_loyalty_points_reverted, 2)." loyalty-points-awarded FAILED for cancelled item in order $order[group_id].\n";
                } else {
                    $refund_comment .= "Reverting ".number_format($total_awarded_loyalty_points_reverted, 2)." loyalty-points-awarded for cancelled item in order $order[group_id].\n";
                }
            }
        }

 		if($order['ordertype'] != 'ex'){
            if($total_used_loyalty_points_refunded > 0.00 && !($order['status'] == 'OH' && ($order['on_hold_reason_id_fk'] == 24 || $order['on_hold_reason_id_fk'] == 26))){
                $ret = LoyaltyPointsPortalClient::creditForItemCancellation($order['login'], $order['group_id'], $total_used_loyalty_points_refunded, 
                        "Refunding loyalty points used for cancelled item in order $order[group_id]");
                if($ret['status']['statusType'] == 'ERROR'){
                    $refund_comment .= "Refund of ".number_format($total_used_loyalty_points_refunded, 2)." loyalty-points-used FAILED for cancelled item in order $order[group_id].\n";
                } else {
                    $refund_comment .= "Refund of ".number_format($total_used_loyalty_points_refunded, 2)." loyalty-points-used for cancelled item in order $order[group_id].\n";
                } 
            }
            
            if($total_awarded_loyalty_points_reverted > 0.00 && !($order['status'] == 'OH' && $order['on_hold_reason_id_fk'] == 25)){
                $ret = LoyaltyPointsPortalClient::debitForItemCancellation($order['login'], $order['group_id'], $total_awarded_loyalty_points_reverted, 
                        "Reversing credit of points for part cancellation of order no. $order[group_id]");
                if($ret['status']['statusType'] == 'ERROR'){
                    $refund_comment .= "Reverting ".number_format($total_awarded_loyalty_points_reverted, 2)." loyalty-points-awarded FAILED for cancelled item in order $order[group_id].\n";
                } else {
                    $refund_comment .= "Reverting ".number_format($total_awarded_loyalty_points_reverted, 2)." loyalty-points-awarded for cancelled item in order $order[group_id].\n";
                }
            }
	 		if($refund_to_cb) {
	            if($total_cash_refund != 0.00) {
	            	//$couponAdapter->creditCashback($order['login'], $total_cash_refund, "Refund for cancelling item(s) in order - ".$order['group_id']);
            	            	
                	$trs = new MyntCashTransaction($order['login'],MyntCashItemTypes::ORDER, $order['group_id'],  MyntCashBusinessProcesses::PARTIAL_CANCELLATION ,  0,0, 0,  "Refund for cancelling item(s) in order - ".$order['group_id']);

                	$ret = MyntCashService::creditMyntCashForItemCancellation($trs, $total_cash_refund, $cancelled_items_details);
	       		}
	            $refund_comment .= "Refund of Rs $total_cash_refund credited to CB Account.\n";
	        } else {
	        	$refund_comment .= "Refund to be issued for Rs $total_cash_refund by CC team.\n";
	        }
	        
	        $coupon_msg = "";
			if($total_coupon_discount_refund != 0.00 && !empty($order['coupon'])) {
				$expirytime = time()+(30*24*60*60);
				$couponData = ItemManager::calculateAndIssueCouponRefund($item_details, $order['login'], $original_subtotal, $order['coupon']);
				$coupon = $couponData['coupon'];
				$minCartVal = $couponData['minCartVal']; 
				$coupon_msg = "<p>We are creating a replacement for the coupon ".$order['coupon']." used on this transaction. You could use the coupon code: $coupon on your next transaction on Myntra. The coupon would be valid for the next 30 days, so do make use of it within ".date('M jS Y', $expirytime).".</p>";
				$refund_comment .= "Replacement coupon: ".$couponData['description']." issued\n";

			}
			
			$gw_coupons_msg = "";
			// If the cancellation was by CC team for OOS issue, then create coupons for user
			if($cancel_reason=='OOSC' || $cancel_reason=='PMM') {
				$couponCount = WidgetKeyValuePairs::getWidgetValueForKey('cancelOrderCouponCount');
				$couponMrp = WidgetKeyValuePairs::getWidgetValueForKey('cancelOrderCouponMrp');	
				$couponMinPurchase = WidgetKeyValuePairs::getWidgetValueForKey('cancelOrderCouponMinPurchase');	
				$couponExpiry = WidgetKeyValuePairs::getWidgetValueForKey('cancelOrderCouponExpiryDays');
				$expirytime = time()+(intval($couponExpiry)*24*60*60);
				$couponCodes = "";
				for($i=0; $i<intval($couponCount); $i++) {
					$coupon = $couponAdapter->generateSingleCouponForUser('GW', 8, 'OOSCANCELLATION', time(), $expirytime, 
						$order['login'], 'absolute', $couponMrp, '', $couponMinPurchase);
						if($couponCodes != "") {
							$couponCodes .= "/";
						}
						$couponCodes .= $coupon;
				}
				$validity_date = date('M jS Y', $expirytime);
				
				$gw_coupons_msg_sql = "SELECT * FROM $sql_tbl[mk_email_notification] WHERE name = 'order_cancel_gw_coupon'";
				$gw_coupons_msg_row = func_query_first($gw_coupons_msg_sql);
				$gw_coupons_msg = $gw_coupons_msg_row['body'];
				$gw_coupons_msg = str_replace("[COUPON_CODES]", $couponCodes, $gw_coupons_msg);
				$gw_coupons_msg = str_replace("[COUPON_AMOUNT]", $couponMrp, $gw_coupons_msg);
				$gw_coupons_msg = str_replace("[EXPIRY_DATE]", $validity_date, $gw_coupons_msg);
				
				$refund_comment .= "Coupon(s) $couponCodes (Rs $couponMrp off $couponMinPurchase) with expiry $validity_date issued to user for item cancellation\n";
			}
 		} else {
 			$refund_comment .= "Items cancelled from the exchange-order. No refund issued.";
 		}
		
		//if(isset($_POST['notify_customer_email'])) {
		if($notify_customer) {
			$cancel_email_contents = str_replace("[ORDER_ID]", $order['group_id'], $cancel_reason_email_contents);
			
			$cancelled_items = func_create_array_products_of_order($order['orderid'], array(), false, $cancelled_itemids);
			$sql = "SELECT gift_card_amount, sum(price*amount) as subtotal, sum(discount) as discount, sum(cart_discount_split_on_ratio) as cart_discount, sum(coupon_discount_product) as coupon_discount, sum(cash_redeemed) as cash_redeemed, sum(item_loyalty_points_used) as item_loyalty_points_used, sum(pg_discount) as pg_discount, sum(total_amount+taxamount-coupon_discount_product-cash_redeemed-pg_discount) as total,sum(cashback) as cashback, sum(difference_refund) as difference_refund, sum(taxamount) as tax FROM xcart_order_details WHERE itemid in (".implode(",", $cancelled_itemids).")";
			$order_amount_details = func_query_first($sql);
			$order_amount_details['payment_method'] = $order['payment_method'];
            $order_amount_details['loyalty_credit'] = $order_amount_details['item_loyalty_points_used']*$order['loyalty_points_conversion_factor'];	
			$order_amount_details['total'] -= $order_amount_details['loyalty_credit'];
            
			if($order['status']=='F') {
				if($order['payment_surcharge'] > 0) {
					$order_amount_details['payment_surcharge'] = $order['payment_surcharge'];
				}
				if($order['shipping_cost'] > 0){
					$order_amount_details['shipping_cost'] = $order['shipping_cost'];
					$order_amount_details['total']+=$order_amount_details['shipping_cost'];
				}
				if($order['gift_charges'] > 0){
					$order_amount_details['gift_charges'] = $order['gift_charges'];
					$order_amount_details['total']+=$order_amount_details['gift_charges'];
				}
			}
			$order_amount_details['ordertype'] = $order['ordertype'];
			$cancelled_item_details = create_product_detail_table_new($cancelled_items, $order_amount_details,'email','refund',false,false,false,false,$total_gift_card_amount_refund);
			
			$refund_msg = "";
			if($total_cash_refund != 0.00) {
				if($refund_to_cb) {
					$http_location = HostConfig::$httpHost;
					$mycouponsurl = $http_location.'/mymyntra.php?view=mymyntcredits';
					$refund_msg = "Your refund amount of Rs ".number_format($total_cash_refund, 2)." for the cancelled item(s) has been credited to your Mynt Club cashback account. You can view the details of the same by <a href=\"$mycouponsurl\">clicking here</a>.";
				} else {
					$refund_msg = "Your refund amount of Rs. ".number_format($total_cash_refund, 2)." for the cancelled item(s) is being refunded and should reflect in your account within the next 10 business days.";
				}
			}
			
			$cb_deduction_msg = "";
			if($total_cb_deducted > 0) {
				$cb_deduction_msg = "Cashback amount of Rs $total_cb_deducted offered on the item(s) has been deducted from your refund amount.";
				$refund_comment .= "Cashback amount of Rs $total_cb_deducted offered on the item(s) has been deducted from refund amount\n";
			}
			if($order['ordertype'] == 'ex'){
				$cb_deduction_msg = "";
				$refund_msg = "";
			}
            //email template
            $template = "items_cancel";
            
            $userinfo = func_userinfo($order['login'], 'C');

            //build content and subject of the mail
            $subjectArgs = array("ORDER_ID"	=> $order['group_id']);
            $bodyArgs = array(
                            "USER"	=> $userinfo['firstname'],
							"ORDER_ID"	=> $order['group_id'],
							"REASON_TEXT" => $cancel_email_contents,
							"ITEM_DETAILS" => $cancelled_item_details,
							"REFUND_MSG"=>$refund_msg,
							"COUPON_USAGE_TEXT"=>$coupon_msg,
							"GOODWILL_COUPON_MSG"=>$gw_coupons_msg,
							"CAHBACK_DEDUCT_MSG"=>$cb_deduction_msg
                        );
            $customKeywords = array_merge($subjectArgs, $bodyArgs);
            $mailDetails = array(
                    "template" => $template,
                    "to" => $order['login'],
                    "bcc" => "myntramailarchive@gmail.com",
                    "header" => 'header',
                    "footer" => 'footer',
                    "mail_type" => \MailType::CRITICAL_TXN
                );
            $multiPartymailer = new \MultiProviderMailer($mailDetails, $customKeywords);
            //$flag = $multiPartymailer->sendMail();  
            
		}
		
		return $refund_comment;
 	}
	
 	
 	public static function cancelItems($request) {
 		global $cashback_gateway_status, $sql_tbl;
 		$orderid = $request['orderid'];
		$order = func_query_first("SELECT * FROM $sql_tbl[orders] WHERE orderid = '".$orderid."'");

        $total_gift_card_amount_refund = 0.00;
		$original_subtotal = $order['subtotal'];
		$total_cash_refund = 0.00;
		$total_coupon_discount_refund = 0.00;
		$total_cb_deducted = 0.00;
        $total_used_loyalty_points_refunded = 0.00;
		$total_awarded_loyalty_points_reverted = 0.00;
        $loyalty_points_conversion_factor = $order['loyalty_points_conversion_factor'];
		$cancel_comment = '';
		$couponAdapter = CouponAdapter::getInstance();
		$itemscancelled = false;
		$finalErrorMsg = "";
		$successMsg = "";
		
		$cancel_type = $request['cancellation_type'];
		$cancel_reason = $request['cancellation_reason'];
		
		// get all details for current items to be deleted...
		$item_details = ItemManager::getItemDetailsForItemIds(explode(",", $_GET['itemids']));
		
		$cancel_code_row = func_query_first("select id, email_content from cancellation_codes where cancel_type = '$cancel_type' and cancel_reason = '$cancel_reason' AND cancellation_mode = 'ITEM_CANCELLATION'");
		
		$new_items = array();
		$update_items = array();
		$cancelled_items_original = array();
		$cancelled_itemids = array();
		$cancelled_items_details = array(); // details of items that are to be cancelled. to be used by myntcash/cashback api
		
		foreach ($item_details as $item) {
			if($item['item_status'] == 'IC'){
				$finalErrorMsg .= "Item has already been cancelled <br>";
			} else {
				$item_total = 0.00;
				$refund_amount = 0.00;
				$cb_to_deduct = 0.00;
				$refund_discount = 0.00;
                $used_loyalty_points_refunded = 0.00;
                $awarded_loyalty_points_reverted = 0.00;
				
				$canCancelItem = true;
				/*if($cancel_reason=='OOSC' && $item['sku_id'] && $order['warehouseid'] > 0) { 
					$response = checkSkuOutOfStock($item['sku_id'], $order['warehouseid']);
					if($response === false) {
						$canCancelItem = false;
					} else if ($response === true) {
						$canCancelItem = true;
					} else {
						$canCancelItem = false;
						$finalErrorMsg .= "Unable to check OOS for item. Error - ".$response."<br>";
					}
				}*/
				
				if($canCancelItem) {
					$qty_to_cancel = intval($request['item_quantity_'.$item['itemid']]);
					$new_item = false;
                    $upd_inventory = true;
                    if ($order['warehouseid'] == 0)
                        $upd_inventory = false;
                    
					$response = ItemManager::cancelItemFromOrder($order, $item, $qty_to_cancel, $upd_inventory, $request['login'], $new_item);
					if($response != "") {
						$finalErrorMsg .= $response."<br>";
					} else {
						$itemscancelled = true;
						$cancelled_items_original[] = $item;	
						if($new_item){
							$new_item['cancellation_code'] = $cancel_code_row['id'];
							$new_items[] = $new_item;
							$cancelled_items_details[] = $new_item;
							ItemManager::calculateItemCancellationRefunds($new_item, $order['payment_method'], $order['login'],
                                    $total_cash_refund, $total_cb_deducted, $total_coupon_discount_refund,
                                    $total_used_loyalty_points_refunded, $total_awarded_loyalty_points_reverted, $loyalty_points_conversion_factor, $total_gift_card_amount_refund);
						} else{
							$item['cancellation_code'] = $cancel_code_row['id'];
                            
							$cancelled_itemids[] = $item['itemid'];
							$cancelled_items_details[] = $item;

							ItemManager::calculateItemCancellationRefunds($item, $order['payment_method'], $order['login'], 
                                    $total_cash_refund, $total_cb_deducted, $total_coupon_discount_refund,
                                    $total_used_loyalty_points_refunded, $total_awarded_loyalty_points_reverted, $loyalty_points_conversion_factor, $total_gift_card_amount_refund);
						}
						$update_items[] = $item;
						$cancel_comment .= "Cancelled $qty_to_cancel pieces from item ".$item['itemid'].".\n";
						$successMsg .= "Cancelled $qty_to_cancel pieces from item ".$item['itemid'].".<br>";
					}
				} else {
					$finalErrorMsg .= "Not all of the selected items are OOS. Some items are in STORED state.<br>";
				}
			}
		}
			
		if($itemscancelled){
			$userlogin = $order['login'];
	        $refund_to_cb_acc = $cashback_gateway_status=="on"?true:false;
	        if($cashback_gateway_status == "internal") {
	            $pos = strpos($userlogin, "@myntra.com");
	            if($pos === false)
	                $refund_to_cb_acc = false;
	            else
	                $refund_to_cb_acc = true;
	        }
			
			foreach($new_items as $item_row) {
				$item_opt_qty = $item_row['item_option_qty'];
				unset($item_row['itemid']);
				unset($item_row['sku_id']);
				unset($item_row['item_option_qty']);
				unset($item_row['quantity']);
				
				$new_itemid = func_array2insert('order_details', $item_row);
				$item_opt_qty['itemid'] = $new_itemid;
				func_array2insert('mk_order_item_option_quantity', $item_opt_qty);
				$cancelled_itemids[] = $new_itemid;
			}
			
			foreach($update_items as $item_row) {
				$item_opt_qty = $item_row['item_option_qty'];
				unset($item_row['item_option_qty']);
				unset($item_row['sku_id']);
				unset($item_row['quantity']);
				
				func_array2update('order_details', $item_row, "itemid = ".$item_row['itemid']);
				if($item_opt_qty)
					func_array2update('mk_order_item_option_quantity', $item_opt_qty, "itemid = ".$item_row['itemid']);
			}
			
			EventCreationManager::pushInventoryUpdateToAtpEvent($order['orderid'], $cancelled_itemids,'UNBLOCK');
			
			$order['s_firstname'] = mysql_real_escape_string($order['s_firstname']);
			$order['s_lastname'] = mysql_real_escape_string($order['s_lastname']);
			$order['s_address'] = mysql_real_escape_string($order['s_address']);
			$order['s_city'] = mysql_real_escape_string($order['s_city']);
			$order['b_address'] = mysql_real_escape_string($order['b_address']);
			$order['b_city'] = mysql_real_escape_string($order['b_city']);
			$order['s_locality'] = mysql_real_escape_string($order['s_locality']);
			
			$active_items = func_query("SELECT * from $sql_tbl[order_details] WHERE item_status != 'IC' and orderid = $orderid");
			if(!$active_items){
				$order['status'] = 'F';
				$order['canceltime'] = time();
				$order['cancellation_code'] = $cancel_code_row['id'];
				
				if($order['payment_surcharge'] > 0 || $order['gift_charges'] > 0 || $order['shipping_cost'] > 0 ) {
					//find any other active order in group we will refund emi charge only when complete order is cancelled
					$order_groupid = $order['group_id'];
					$active_order_in_group = func_query_first("select orderid, total from $sql_tbl[orders] where group_id = $order_groupid and status in ('Q','WP','OH','SH','DL','C') and orderid!='$orderid'");
					if($active_order_in_group && !empty($active_order_in_group)) {
						$active_order = array();
						$active_order['payment_surcharge'] = $order['payment_surcharge'];
						$active_order['gift_charges'] = $order['gift_charges'];
						$active_order['shipping_cost'] = $order['shipping_cost'];
						$active_order['total'] = $active_order_in_group['total'] + $order['gift_charges'] + $order['shipping_cost'];  

						$order['total'] -= ($order['gift_charges'] + $order['shipping_cost']);
						$order['payment_surcharge'] = 0.00;
						$order['gift_charges'] = 0.00;
						$order['shipping_cost'] = 0.00;
						func_array2updateWithTypeCheck("orders", $active_order, "orderid = $active_order_in_group[orderid]");
					} else {
						if($order['shipping_cost'] > 0 && $order['payment_method'] != 'cod') {
							$total_cash_refund += $order['shipping_cost'];
						}
						if($order['gift_charges'] > 0 && $order['payment_method'] != 'cod') {
							$total_cash_refund += $order['gift_charges'];
						}
						$total_cash_refund += $order['payment_surcharge'];
					}
				} 
				
				if($order['payment_method'] == 'cod')
					$order['cod_pay_status'] = 'cancelled';
			}

            if($order['payment_method'] == 'cod') {
                $total_cash_refund += $total_gift_card_amount_refund;
            }

			func_array2update("orders", $order, "orderid = $orderid");
			
			$cancel_comment = $request['cancel_comment'];
			$commenttime = time();
			$cancelledby = $request['login'];
			$cancel_comment .= "\nType: $cancel_type.\nReason : $cancel_reason. \nRemarks: $cancel_comment\n";
			
			ReleaseApiClient::pushOrderReleaseToWMS($orderid, $cancelledby,'update');
			func_addComment_order($orderid, $cancelledby, 'Note', 'Item Cancelled', $cancel_comment);
			
			$notify_customer = isset($request['notify_customer_email'])?true:false;
			$refund_comment = ItemManager::processItemCancelRefundAndNotifyCustomer($order, $cancelled_items_original, $cancelled_itemids, $cancel_reason, $refund_to_cb_acc, 
																					$total_cash_refund, $total_coupon_discount_refund, $total_cb_deducted,
                                                                                    $total_used_loyalty_points_refunded, $total_awarded_loyalty_points_reverted,
																					$cancel_code_row['email_content'], $notify_customer,$original_subtotal,$cancelled_items_details, $total_gift_card_amount_refund);
			
			func_addComment_order($orderid, $cancelledby, 'Note', 'Item Cancellation Refund', $refund_comment);  
			
			if(FeatureGateKeyValuePairs::getBoolean("myntrashoppingfestleaderboard") && ($reason != 'OOSC')){
                                global $xcart_dir;
                                include_once($xcart_dir."/include/class/shopfest/ShopFest.php");
                                foreach($cancelled_itemids as $cancelled_itemid){
                                        $productArr = func_create_array_products_of_order($order['orderid'], array(), false, array($cancelled_itemid));
                                        ShopFest::updateLeaderBoardData($productArr[0]['total_amount']+$productArr[0]['cashback'],$order['login'],$order['date'],$productArr,true);
                                }
                        }

		}
		return array('failure'=>$finalErrorMsg, 'success'=>$successMsg, 'is_exchange_order'=>($order['ordertype']=='ex'), 'order_groupid'=>$order['group_id']);
 	}
 	
}
?>
