<?php
 include_once($xcart_dir."/include/func/func.mk_orderbook.php");
 include_once($xcart_dir."/include/func/func.order.php");
 include_once($xcart_dir."/include/func/func.returns.php");
 include_once($xcart_dir."/modules/apiclient/SkuApiClient.php");
 include_once($xcart_dir."/include/class/oms/ItemManager.php");
 include_once("$xcart_dir/include/class/class.mail.multiprovidermail.php");
 include_once($xcart_dir."/modules/apiclient/ReleaseApiClient.php");
 include_once("$xcart_dir/include/class/oms/EventCreationManager.php");
 include_once($xcart_dir."/modules/coupon/database/CouponAdapter.php");
 include_once("$xcart_dir/include/class/oms/OrderWHManager.php");
 include_once($xcart_dir."/modules/apiclient/OrderApiClient.php");
 include_once($xcart_dir. "/modules/apiclient/ShipmentApiClient.php");
 include_once($xcart_dir. "/modules/apiclient/AtpApiClient.php");
 include_once($xcart_dir. "/modules/apiclient/AtpApiClientV2.php");
 include_once("$xcart_dir/include/class/oms/ReturnsStatusProcessor.php");
 include_once("$xcart_dir/include/class/oms/OrderProcessingMailSender.php");
 include_once($xcart_dir."/modules/apiclient/PpsApiClient.php");
 
 include_once "$xcart_dir/modules/myntCash/MyntCashTransaction.php";
 include_once "$xcart_dir/modules/myntCash/MyntCashService.php";
 include_once "$xcart_dir/modules/loyaltyPoints/LoyaltyPointsPortalClient";
 
 class OrderExchangeManager {
 	
 	public static function createReturnidForExchanges($exchangeid, $login, $courierCode, $trackingNumber) {
 		global $sql_tbl, $weblog;
 		
 		$exchangeOrderMapping = func_query_first("SELECT ex.* from exchange_order_mappings ex, xcart_orders o where " 
 				."ex.exchange_orderid = $exchangeid and o.orderid = ex.exchange_orderid and o.status not in ('F', 'D', 'L') order by exchange_orderid DESC");
 		if(!$exchangeOrderMapping){
 			return array("status" => false, "message" => "exchangeid does not exist", "returnid" => '');
 		}
 		$orderid = $exchangeOrderMapping['releaseid'];
 		$itemid = $exchangeOrderMapping['itemid'];

 		$item_return_sql = "Select returnid from xcart_returns where status != 'RRD' and itemid = $itemid";
 		$return_req = func_query_first($item_return_sql);

 		$return_details = array();
 		if(!$return_req) {
 			$orderdetails = func_query_first("SELECT * from xcart_orders where orderid = $exchangeid", true);
 			$itemdetails = func_query_first("select * from ((mk_order_item_option_quantity moioq LEFT JOIN mk_product_options po on moioq.optionid = po.id) LEFT JOIN mk_styles_options_skus_mapping mosm ON moioq.optionid = mosm.option_id) where moioq.itemid = $itemid", true);
 			
 			$option = $itemdetails['value'];
 			$quantity = $itemdetails['quantity'];
 			$customer_login = $orderdetails['login'];
 			$userid = $login;
 			$reasoncode = $orderdetails['notes'];
 			$details = 'Creating a return for exchange order '. $exchangeid;
 			$return_mode = 'pickup';
 			$selected_address_id = false;
 			$pickup_courier = $courierCode;
 			
 			$exchange_address = array();
 			$exchange_address['name'] = ($orderdetails['s_firstname'].' '.$orderdetails['s_lastname']);
 			$exchange_address['address'] = ($orderdetails['s_address']);
 			$exchange_address['city'] = ($orderdetails['s_city']);
 			$exchange_address['state'] = ($orderdetails['s_state']);
 			$exchange_address['country'] = ($orderdetails['s_country']);
 			$exchange_address['pincode'] = ($orderdetails['s_zipcode']);
 			$exchange_address['mobile'] = ($orderdetails['mobile']);
 			$exchange_address['email'] = ($orderdetails['s_email']);
 			$exchange_address['phone'] = ($orderdetails['phone']);
 			
 			$return_details = func_add_new_return_request($orderid, $itemid, $option, $quantity, $customer_login, $userid, $reasoncode, 
 					$details, $return_mode, $selected_address_id, true, $pickup_courier, $exchange_address, $trackingNumber);
 			if($return_details["status"]){
 				db_query("update exchange_order_mappings set returnid = $return_details[returnid] where exchange_orderid = $exchangeid");
 				func_addComment_order($orderid, 'LMS ADMIN', 'Automated', 'Return for Exchange ', "Created Returnid $return_details[returnid] for this order.");
 			}
 			return array("status" => true, "message" => "new return created", "returnid" => $return_details["returnid"], "return_row" => $return_details["return_req"]);
 		}
 		return array("status" => false, "message" => "returnid already exists", "returnid" => $return_req["returnid"]);
 	}
 	
 	public static function createExchangeforOrder($orderid, $itemid, $optionid, $qty_to_exchange, $sku_id, $userid, $reasoncode, $comment, $selected_address_id, $isAdmin = true) {
 		global $xcart_dir, $sql_tbl, $weblog;
		define("RETRY", 3);

		$original_order = func_query_first("SELECT * from $sql_tbl[orders] WHERE orderid = $orderid", true);
		$old_item = reset(ItemManager::getItemDetailsForItemIds($itemid, $orderid));
		$oldOptionId = func_query_first("select optionid from mk_order_item_option_quantity where itemid = $itemid");
		$ppsId = func_query_first("select value from order_additional_info where order_id_fk = $orderid and `key` = 'PAYMENT_PPS_ID'");

		if($ppsId['value'] != null && !empty($ppsId['value'])){
			$ppsResponse = PpsApiClient::ppsEnabledCreateExchange($original_order, $old_item, $oldOptionId['optionid'], $ppsId['value'], $qty_to_exchange, $sku_id, $optionid, $userid, $reasoncode, $comment, $selected_address_id);
			if ($ppsResponse['status'] == "success" && $ppsResponse['exchangeOrderId'] != null) {

                $exchangeOrderId = $ppsResponse['exchangeOrderId'];
                $exchangeItemId = $ppsResponse['$exchangeItemId']; // PPS is not passing this yet.

                func_array2insert("exchange_order_mappings", array("exchange_orderid" => $exchangeOrderId, "releaseid" => $original_order['orderid'], "itemid" => $old_item['itemid']));
				return array("status" => true, "message" => "new exchange_orderid created", "exchange_orderid" => $ppsResponse['exchangeOrderId']);
			} else {
				return array("status" => false, "not_allowed" => true,
					"message" => "Failed to create exchange", "warning_message" => $ppsResponse['warning_message']);
			}
		} else {
			for($i = 1; $i <= RETRY; $i++) {

				$omsExchangeCreationResponse = OrderApiClient::createExchangeOrder($original_order, $old_item, $qty_to_exchange, $sku_id, $optionid, $userid, $reasoncode, $comment, $selected_address_id);
				if ($omsExchangeCreationResponse['status'] == 'success'
					&& $omsExchangeCreationResponse['exchangeOrder'] != null && $omsExchangeCreationResponse['exchangeOrder'][0]['id'] != null) {

					$exchangeOrderId = $omsExchangeCreationResponse['exchangeOrder'][0]['id'];
                    $exchangeItemId = $omsExchangeCreationResponse['exchangeOrder'][0]['orderReleases'][0]['orderLines'][0]['id'];

                    func_array2insert("exchange_order_mappings", array("exchange_orderid" => $exchangeOrderId, "releaseid" => $original_order['orderid'], "itemid" => $old_item['itemid']));
					for($i = 1; $i <= RETRY; $i++){
						$omsQueueExchangeReponse = OrderApiClient::queueExchangeOrder($exchangeOrderId);
						if ($omsQueueExchangeReponse['status'] == 'success' && $omsExchangeCreationResponse['exchangeOrder'] != null) {
							return array("status" => true, "message" => "new exchange_orderid created", "exchange_orderid" => $exchangeOrderId);
						}
					}
				}
			}

			return array("status" => false, "not_allowed" => true, "message" => "Failed to create exchange", "warning_message" => $omsExchangeCreationResponse['warning_message']);
		}

 		$return_exists = func_query_first_cell("select returnid from xcart_returns where itemid = $itemid and status != 'RRD'");
 		if($return_exists){
 			return array("status" => false, "warning_message" => "returnid already exists", "not_allowed" => true, "warn_code"=>"RETURN_EXIST");
 		}

 		$exchangeid_exists = func_query_first_cell("select ex.exchange_orderid from exchange_order_mappings ex, xcart_orders o where ex.itemid = $itemid
 				and o.orderid=ex.exchange_orderid and o.status not in ('F', 'D', 'L') order by exchange_orderid DESC");
 		if($exchangeid_exists){
 			$exchangeRTOd = func_query_first_cell("select id from mk_old_returns_tracking where orderid = $exchangeid_exists");
 			if (!$exchangeRTOd){
 				return array("status" => false, "message" => "exchange_orderid already exists", "exchange_orderid" => $exchangeid_exists, "warn_code"=>"EXCHANGE_EXIST");
 			}
 		}
 		if(!$exchangeid_exists || ($exchangeid_exists && $exchangeRTOd)) {
 			$original_order = func_query_first("SELECT * from $sql_tbl[orders] WHERE orderid = $orderid", true);
			$old_item = reset(ItemManager::getItemDetailsForItemIds($itemid, $orderid));

 			if($old_item) {

				$enabledNewInventory = FeatureGateKeyValuePairs::getFeatureGateValueForKey('exchange.newinventory', 'true');
				if ($enabledNewInventory == 'true') {
					$response = AtpApiClientV2::blockATPInventoryForItem($optionid, $itemid, $qty_to_exchange);
					if (!$response) {
						return array("status" => false, "error_message" => "Item is out of stock", "not_allowed" => true, "error_code" => "ITEM_OOS");
					}
				}

 				$couponAdapter = CouponAdapter::getInstance();
 				$new_order = $original_order;
 				unset($new_order['orderid']);
 				$new_order['total'] = 0.00;
 				$new_order['payment_surcharge'] = 0.00;
 				$new_order['cod'] = 0.00;
 				$new_order['cashback'] = 0.00;
                $new_order['loyalty_points_used'] = 0.00;
                $new_order['loyalty_points_awarded'] = 0.00;
 				$new_order['qtyInOrder'] = $qty_to_exchange;
 				$new_order['courier_service'] = '';
 				$new_order['tracking'] = '';
 				$new_order['shipping_cost'] = 0.00;
 				$new_order['gift_charges'] = 0.00;
 				$new_order['notes'] = $reasoncode;
 				$new_order['payment_method'] = 'on';
 				$new_order['ordertype'] = 'ex';
 				$new_order['gift_status'] = 'N';
                $new_order['shipping_method'] = 'NORMAL';
                $new_order['gift_card_amount'] = 0.00;
 			
 				if(!is_array($selected_address_id)){
 					$address_sql = "Select * from mk_customer_address where login='$original_order[login]' and id = $selected_address_id";
 					$new_address = func_query_first($address_sql);
 					$new_order['s_firstname'] = ereg_replace("[^A-Za-z0-9 _]", "", $new_address['name']);
 					$new_order['firstname'] = ereg_replace("[^A-Za-z0-9 _]", "", $new_address['name']);
 					$new_order['s_address'] = mysql_real_escape_string($new_address['address']);
 					$new_order['s_city'] = mysql_real_escape_string($new_address['city']);
 					$new_order['s_state'] = mysql_real_escape_string($new_address['state']);
 					$new_order['s_country'] = mysql_real_escape_string($new_address['country']);
 					$new_order['s_zipcode'] = mysql_real_escape_string($new_address['pincode']);
 					$new_order['s_email'] = mysql_real_escape_string($new_address['email']);
 					$new_order['mobile'] = mysql_real_escape_string($new_address['mobile']);
					$new_order['s_locality'] = mysql_real_escape_string($new_address['locality']);
 				}			
 				else{
					$new_order['s_firstname'] = ereg_replace("[^A-Za-z0-9 _]", "", $selected_address_id['name']);
					$new_order['firstname'] = ereg_replace("[^A-Za-z0-9 _]", "", $selected_address_id['name']);
					$new_order['s_address'] = mysql_real_escape_string($selected_address_id['address']);
 					$new_order['s_city'] = mysql_real_escape_string($selected_address_id['city']);
 					$new_order['s_state'] = mysql_real_escape_string($selected_address_id['state']);
 					$new_order['s_country'] = mysql_real_escape_string($selected_address_id['country']);
 					$new_order['s_zipcode'] = mysql_real_escape_string($selected_address_id['zipcode']);
 					$new_order['s_email'] = mysql_real_escape_string($selected_address_id['email']);
 					$new_order['mobile'] = mysql_real_escape_string($selected_address_id['mobile']);
					$new_order['s_locality'] = mysql_real_escape_string($selected_address_id['locality']);
 				}
 			
 				/* $original_order['s_firstname'] = mysql_real_escape_string($original_order['s_firstname']);
 				$original_order['s_lastname'] = mysql_real_escape_string($original_order['s_lastname']);
 				$original_order['s_address'] = mysql_real_escape_string($original_order['s_address']);
 				$original_order['s_locality'] = mysql_real_escape_string($original_order['s_locality']);
 				$original_order['s_city'] = mysql_real_escape_string($original_order['s_city']);
 				$original_order['b_address'] = mysql_real_escape_string($original_order['b_address']);
 				$original_order['b_city'] = mysql_real_escape_string($original_order['b_city']); */
 				
 				$new_orderid = create_new_order();
 				$new_order['orderid'] = $new_orderid;
 				$new_order['group_id'] = $new_orderid;
 				$new_order['invoiceid'] = create_new_invoice_id($new_orderid);
 				$new_order['date'] = time();
 				$new_order['queueddate'] = time();
 				$new_order['shippeddate'] = NULL;
 				$new_order['delivereddate'] = NULL;
 				$new_order['completeddate'] = NULL;
 				$new_order['packeddate'] = NULL;
 				$new_order['splitdate'] = NULL;
 				$new_order['warehouseid'] = NULL;
 				$new_order['status'] = 'PP';
 				$new_order['payment_method'] = 'on';
 				
 				// split items to create a row with equal quantity to exchange
 				if($old_item['amount'] != $qty_to_exchange) {
 					$new_item_row = ItemManager::splitItem($old_item, $qty_to_exchange);
 					
 					// created a new item-row. This one will be used for creating a return
 					$item_opt_qty = $new_item_row['item_option_qty'];
 					unset($new_item_row['itemid']);
 					unset($new_item_row['sku_id']);
 					unset($new_item_row['item_option_qty']);
 				    unset($new_item_row['quantity']);
 						
 					$new_itemid = func_array2insert('order_details', $new_item_row);
 					$item_opt_qty['itemid'] = $new_itemid;
 					func_array2insert('mk_order_item_option_quantity', $item_opt_qty);
 						
 					$old_item_opt_qty = $old_item['item_option_qty'];
 					unset($old_item['item_option_qty']);
 					unset($old_item['sku_id']);
 					unset($old_item['quantity']);
 						
 					func_array2update('order_details', $old_item, "itemid = ".$old_item['itemid']);
 					if($old_item_opt_qty)
 						func_array2update('mk_order_item_option_quantity', $old_item_opt_qty, "itemid = ".$old_item['itemid']);
 				} else {
 					$new_item_row = $old_item;
 					$old_option_qty_row = func_query_first("SELECT * from mk_order_item_option_quantity where itemid = ".$old_item['itemid'], true);
 					$new_item_row['item_option_qty'] = $old_option_qty_row;
 					$new_itemid = $old_item['itemid'];
 				}

                $new_item_row['gift_card_amount'] = 0.00;
 				
 				/**********************************************************************/
 				func_array2insert("exchange_order_mappings", array("exchange_orderid" => $new_orderid, "releaseid" => $original_order['orderid'], "itemid" => $new_itemid));
 				/**********************************************************************/
 				// created a new item-row. This one will be used for creating an exchange
 				$exchange_item_row = $new_item_row;
 				$exchange_item_row['orderid'] = $new_orderid;
                $exchange_item_row['item_status'] = 'A';
                $exchange_item_row['qapass_items'] = NULL;
                $loyalty_credit = $exchange_item_row['item_loyalty_points_used']*$new_order['loyalty_points_conversion_factor'];
                $exchange_item_row['cash_redeemed'] = $new_item_row['total_amount'] + $new_item_row['taxamount'] - $new_item_row['coupon_discount_product'] - $new_item_row['pg_discount'] - $loyalty_credit;
                $exchange_item_row['total_amount'] = $new_item_row['total_amount'];
                $exchange_item_row['is_returnable'] = 1;
                $exchange_item_row['is_packaged'] = 0;
 				
 				$new_order['cash_redeemed'] = $exchange_item_row['cash_redeemed'];
 				$new_order['subtotal'] = $exchange_item_row['price']*$exchange_item_row['amount'];
 				$new_order['coupon_discount'] = $exchange_item_row['coupon_discount_product'];
 				$new_order['discount'] = $exchange_item_row['discount'];
 				$new_order['cart_discount'] = $exchange_item_row['cart_discount_split_on_ratio'];
 				$new_order['pg_discount'] = $exchange_item_row['pg_discount'];
 				$new_order['tax'] = $exchange_item_row['taxamount'];
 				$new_order['loyalty_points_used'] = $exchange_item_row['item_loyalty_points_used'];
                $new_order['loyalty_points_awarded'] = $exchange_item_row['item_loyalty_points_awarded'];

 				$size = func_query_first_cell("select value from mk_product_options where id = $optionid", true);
 				$exchange_item_row['quantity_breakup'] = "$size:$qty_to_exchange";
 				//fetch corresponding items option quantity
 				$exchange_item_opt_qty = func_query_first("SELECT * from mk_order_item_option_quantity where itemid = ".$new_itemid, true);
 				
 				unset($exchange_item_row['itemid']);
 				unset($exchange_item_row['sku_id']);
 				unset($exchange_item_row['quantity']);
 				unset($exchange_item_row['item_option_qty']);

 				$exchange_itemid = func_array2insert('order_details', $exchange_item_row);
                $exchange_item_row['itemid'] = $exchange_itemid;
                $exchange_item_row['quantity'] = $exchange_item_row['amount'];
                $weblog->info("Exchange Item - ". print_r($exchange_item_row, true));				
 				$exchange_item_opt_qty['itemid'] = $exchange_itemid;
 				$exchange_item_opt_qty['optionid'] = $optionid;
 				func_array2insert('mk_order_item_option_quantity', $exchange_item_opt_qty);
 					
 				func_array2insert("orders", $new_order);

 				/*
 				$courierServiceabilityVersion = FeatureGateKeyValuePairs::getFeatureGateValueForKey('lms.courierserviceability.version', 'old');
 				if($courierServiceabilityVersion == 'new') {
 					$sku_details = AtpApiClient::getAvailableInventoryForSkus(array($sku_id));
	 				//$sku_details = OrderWHManager::loadWarehouseInvCounts(array($sku_id));
	 				$exchange_item_row['style_id'] = $exchange_item_row['product_style'];
	 				$exchange_item_row['sku_id'] = $sku_id;
	 				$whIdToOrderIds = OrderWHManager::assignWarehouseForOrder($new_orderid, $new_order, array($exchange_item_row), $sku_details, false, false, 'EXCHANGE');
	 				EventCreationManager::pushInventoryUpdateToAtpEvent($new_orderid);
	 				if($whIdToOrderIds) {
	 					OrderWHManager::updateInventoryAfterSplit($new_orderid, $whIdToOrderIds, "Order created from $new_orderid and moved to WP state", "Exchage Flow");
	 				}
	 				$weblog->info(print_r($whIdToOrderIds,true));
	 				
	 				$exchangeOrderIds = array();
	 				foreach($whIdToOrderIds as $whId => $oids){
	 					$exchangeOrderIds = array_merge($exchangeOrderIds, $oids);
	 				}
 				} else {
 					$whId2AvailableCountMap = SkuApiClient::getAvailableInvCountMap($sku_id, 'warehouseId');
 					if ($whId2AvailableCountMap[$original_order['warehouseid']] < $qty_to_exchange){
 						$whIdToOrderId = OrderWHManagerOld::assignWarehouseForOrder($new_orderid, $new_order['s_address']);
 					} else {
 						$whIdToOrderId[$original_order['warehouseid']] = $new_orderid;
 					}
 					if($whIdToOrderId) {
 						OrderWHManagerOld::updateInventoryAfterSplit($new_orderid, $whIdToOrderId, "Order created from $new_orderid and moved to WP state", "Exchage Flow");
 					}
 					$weblog->info(print_r($whIdToOrderId,true));
 					$exchangeOrderIds = array_values($whIdToOrderId);
 				}
 				
 				if(count($exchangeOrderIds) > 1){
 					//split the original item-qty to create equal number of returns
 					foreach($exchangeOrderIds as $oid){
 						if($oid != $new_orderid){
 							$qty_in_current_wh = func_query_first_cell("select amount from xcart_order_details where orderid = $oid");
	 						$split_item_row = ItemManager::splitItem($new_item_row, $qty_in_current_wh);
	 						// created a new item-row. This one will be used for creating a return
	 						$item_opt_qty = $split_item_row['item_option_qty'];
	 						unset($split_item_row['itemid']);
	 						unset($split_item_row['sku_id']);
	 						unset($split_item_row['item_option_qty']);
	 						unset($split_item_row['quantity']);
	 							
	 						$split_itemid = func_array2insert('order_details', $split_item_row);
	 						$item_opt_qty['itemid'] = $split_itemid;
	 						func_array2insert('mk_order_item_option_quantity', $item_opt_qty);
	 							
	 						$new_item_opt_qty = $new_item_row['item_option_qty'];
	 						unset($new_item_row['item_option_qty']);
	 						unset($new_item_row['sku_id']);
	 						unset($new_item_row['quantity']);
	 							
	 						func_array2update('order_details', $new_item_row, "itemid = ".$new_item_row['itemid']);
	 						if($new_item_opt_qty){
	 							func_array2update('mk_order_item_option_quantity', $new_item_opt_qty, "itemid = ".$new_item_row['itemid']);
	 						}
	 						func_array2insert("exchange_order_mappings", array("exchange_orderid" => $oid, "releaseid" => $original_order['orderid'], "itemid" => $split_itemid));
 						}
 					}
 				}
 				*/

				$enabledNewInventory = FeatureGateKeyValuePairs::getFeatureGateValueForKey('exchange.newinventory', 'true');
				if ($enabledNewInventory == 'false') {
					EventCreationManager::pushInventoryUpdateToAtpEvent($new_orderid);
				}

				$status = 'Q';
 				$onHoldReasonCode = false;
 				$ex_reason = get_reason_displayname($reasoncode);
 				
 				$creditTransaction = new MyntCashTransaction($original_order['login'],MyntCashItemTypes::ORDER,  $original_order[group_id],  MyntCashBusinessProcesses::PARTIAL_CANCELLATION ,  0,0, 0,  "Refund on exchange of order - $original_order[group_id]");
 				$debitTransaction = new MyntCashTransaction($original_order['login'], MyntCashItemTypes::ORDER, $new_orderid, MyntCashBusinessProcesses::CASHBACK_USED, 0, 0, $exchange_item_row['cash_redeemed'], "Debiting amount for the new exchanged order: $new_orderid");
 				$myntCashUsageResponse = MyntCashService::creditAndDebitMyntCashForExchangeOrder($creditTransaction, $debitTransaction, $new_orderid, $exchange_item_row['cash_redeemed'], array($old_item,));
 				// if failure response from Cashback service, put exchange order on hold.
 				if($myntCashUsageResponse === MyntCashService::creditDebitFailure || $myntCashUsageResponse === MyntCashService::serviceNotAvailable){
 					$status = 'OH';
 					$onHoldReasonCode = 'CSFE';
 					$comment .= "Creding and debiting cashback for exchange order $new_orderid FAILED. Putting Order On hold.";
 				}
                
 				if($status == 'Q') {	                
                    $awarded_loyalty_points = $new_order['loyalty_points_awarded'];
                    if($awarded_loyalty_points > 0) {
                        $ret = LoyaltyPointsPortalClient::creditDebitInactiveForExchangeOrder($new_order['login'], $original_order['group_id'],$new_order['orderid'], $awarded_loyalty_points,"Reversing credit of points for order no ".$original_order['group_id'],"Awarding points for exchange order no ".$new_order['orderid']);
                        if(!$ret || $ret['status']['statusType'] == 'ERROR'){
                            $status = 'OH';
                            $onHoldReasonCode = 'LDNA';
                            $comment .= "Crediting and Debiting $awarded_loyalty_points loyalty-points-issued FAILED. Putting Order On hold";
                        }
                        /*else{
                            $used_loyalty_points_refunded = $new_order['loyalty_points_used'];
                            if($used_loyalty_points_refunded > 0) {
                                $ret = LoyaltyPointsPortalClient::creditDebitActiveForExchangeOrder($new_order['login'], $original_order['group_id'],$new_order['orderid'], $used_loyalty_points_refunded);
                                if(!$ret || $ret['status']['statusType'] == 'ERROR'){
                                    $status = 'OH';
                                    $onHoldReasonCode = 'LDNA';
                                    $comment .= "Crediting and Debiting $used_loyalty_points_refunded loyalty-points-used FAILED. Putting Order On hold";
                                }
                            }
                        }*/
                    }                   

 				}  
 				
 				if($status == 'OH' && $onHoldReasonCode) {
 					$oh_reason = get_oh_reason_for_code($onHoldReasonCode);
 					db_query("update xcart_orders set status = 'OH', on_hold_reason_id_fk = '".$oh_reason['id']."' where orderid = $new_orderid");
 					$new_order_comment = "Order $new_orderid is created as an exchange order for $original_order[orderid]. \nRemarks: $ex_reason.\n$comment";
 					func_addComment_order($new_orderid, $userid, 'Info', 'Exchange Order Created', $new_order_comment);
 					EventCreationManager::pushCompleteOrderEvent($new_orderid);
 				}

 				if($status == 'Q') {
 					EventCreationManager::pushCompleteOrderEvent($new_orderid);
	 				/**********************************************************************/
					$updateSql = "UPDATE xcart_orders SET status='Q',queueddate='".time()."', response_server='$server_name', cashback_processed = 1 WHERE orderid = $new_orderid";
					db_query($updateSql);
					
					func_addComment_status($new_orderid, "Q", "PP", "Exchange created successfuly, Queuing Order.", "AUTOSYSTEM");
					
					EventCreationManager::pushOrderConfirmationEvent($new_orderid);
					
					$old_order_comment = "Order $new_orderid is created as an exchange order for $original_order[orderid]. \nRemarks: $ex_reason: $comment";
					func_addComment_order($orderid, $userid, 'Info', 'Exchange Order Created', $old_order_comment);
					
					$new_order_comment = "Order $new_orderid is created as an exchange order for $original_order[orderid]. \nRemarks: $ex_reason: $comment";
	 				func_addComment_order($new_orderid, $userid, 'Info', 'Exchange Order Created', $new_order_comment);
 				}
 				
 				//Notify the customer about new exchange created.
 				$order_data = func_order_data($new_orderid);
				OrderProcessingMailSender::send_exchange_order_mail($order_data["order"], "exchange_confirmation", '' , $isAdmin);
				return array("status" => true, "message" => "new exchange_orderid created", "exchange_orderid" => $new_orderid, "newitemid" => $exchange_itemid);
 			}
 		}
 	}
 	 	
 	public static function getItemSkuDetails($itemid){
 		global $portalapilog;
 		
 		$sizesql = "select po.id, po.value, po.is_active, som.sku_id from xcart_order_details xod, mk_product_options po, mk_styles_options_skus_mapping som
 		where po.id = som.option_id and xod.product_style = po.style and xod.itemid = $itemid";
 		$sizes_available = func_query($sizesql);
 		$itemSkus = array();
 		
 		if($sizes_available){
	 		foreach ($sizes_available as $row) {
	 			$skuIds[] = $row['sku_id'];
	 		}
	 		$portalapilog->info("skuids available ".print_r($skuIds, true));
	 		//$skuId2AvailableCountMap = SkuApiClient::getAvailableInvCountMap($skuIds);
	 		$skuId2AvailableCountMap = AtpApiClient::getAvailableInventoryForSkus($skuIds);
	 		$portalapilog->info("sku available count for style - $itemid = ". print_r($skuId2AvailableCountMap, true));
	 		
	 		foreach ($sizes_available as $availablesize) {
	 			$available_count = $skuId2AvailableCountMap[$availablesize['sku_id']]['availableCount'];
	 			$itemSkus[] = array("size" => $availablesize['value'], "optionid" => $availablesize['id'], "sku_id" => $availablesize['sku_id'],
	 					"is_available" => ($availablesize['is_active'] == 1 && $available_count > 0), 
	 					"sku_count" => $available_count, "availableInWarehouses" => $skuId2AvailableCountMap[$availablesize['sku_id']]['availableInWarehouses']);
	 		}
 		}
 		return $itemSkus;
 	}

	 public static function getItemSkuDetailsFromATP($itemid){
		 global $portalapilog;

		 $sizesql = "select po.id, po.value, po.is_active, som.sku_id,xod.supply_type,xod.seller_id from xcart_order_details xod,
					mk_product_options po, mk_styles_options_skus_mapping som where po.id = som.option_id and xod.product_style = po.style
					and xod.itemid = $itemid";
		 $sizes_available = func_query($sizesql);
		 $itemSkus = array();
		 $skuIds = array();
		 $supply_type = '';
		 $seller_id = '';
		 if($sizes_available){
			 foreach ($sizes_available as $row) {
				 $skuIds[] = $row['sku_id'];
				 $supply_type = $row['supply_type'];
				 $seller_id = $row['seller_id'];
			 }

			 $skuId2AvailableCountMap = AtpApiClientV2::getInventoryFromAtp($skuIds, $supply_type, $seller_id);

			 foreach ($sizes_available as $availablesize) {
				 $available_count = $skuId2AvailableCountMap[$availablesize['sku_id']]['availableCount'];
				 $itemSkus[] = array("size" => $availablesize['value'], "optionid" => $availablesize['id'], "sku_id" => $availablesize['sku_id'],
					 "is_available" => ($availablesize['is_active'] == 1 && $available_count > 0),
					 "sku_count" => $available_count, "availableInWarehouses" => $skuId2AvailableCountMap[$availablesize['sku_id']]['availableInWarehouses']);
			 }
		 }
		 return $itemSkus;
	 }

 	public static function handleExchangeOrderFailedDelivery($exchangeid, $returnStatus){
 		$returnid = func_query_first_cell("select returnid from exchange_order_mappings where exchange_orderid = $exchangeid");
 		if($returnid){
 			$processor = new ReturnsStatusProcessor();
 			if($returnStatus == 'RPU'){
 				// Failed Exchange delivery but successful return pickup: issue refund in this case.
 				db_query("update xcart_returns set is_refunded = 0 where returnid = $returnid");
 				$comment = 'Marking return as picked-up upon LMS successful pickup';
 			}
 			else {
 				$comment = 'Marking return as declined after failed exchange delivery';
 			}
 			$response = $processor->changeReturnsStatus(trim($returnid), $returnStatus, '', '', '', '', 'LMS_API',$comment);
 			
 			if($response['status']){
 				return array("status" => true, "message" => "exchange order marked as RTO, return updated");
 			} else {
 				return array("status" => false, "message" => "exchange order marked as RTO, return not updated");
 			}
 		} else{
 			return array("status" => false, "message" => "exchange order marked as RTO, but no returnid found associated with this exchange");
 		}
 	}
 }
 
 ?>
