<?php

/*
 * Created on 27-Nov-2012
 * Author: Raghu Kishore
 * 
 */


include_once "$xcart_dir/include/func/func.order.php";
include_once "$xcart_dir/include/func/func.mk_orderbook.php";
include_once($xcart_dir."/include/class/class.mail.multiprovidermail.php");
include_once($xcart_dir."/include/class/class.ab_testing_variables.php");
include_once($xcart_dir."/include/func/func.sms_alert.php");
include_once ($xcart_dir."/include/class/class.feature_gate.keyvaluepair.php");
include_once("$xcart_dir/modules/apiclient/TatApiClient.php");
include_once("$xcart_dir/env/Host.config.php");

use feedback\instance\MFBInstance;
use feedback\manage\MFB;
use abtest\resolver\ResolverFactory;
use abtest\algos\MABTestSegmentationAlgoFactory;


class OrderProcessingMailSender {
	
	const ORDER_SHIPPED = 'ORDER_SHIPPED';
	
	private static function getMailType(){
		$useSmtp =  FeatureGateKeyValuePairs::getBoolean("UseLocalSmtpForOrderProcessingMails",false);
		if($useSmtp){
			return MailType::NON_CRITICAL_TXN;
		}
		else {
			return MailType::CRITICAL_TXN;
		}
	}
	
	private static function getEmailTemplate($orderid, $event) {
		func_query("select xo.status,count(1) from xcart_orders xo, xcart_orders xo1 where xo.group_id = xo1.group_id and xo1.orderid = 4547070 group by xo.status");
	}

	public static function send_shipment_status_update_mail($orderid, $template, $deliveredDate, $deliveryStaffName = null, $deliveryStaffMobileNumber = null) {
		global $weblog;
		if (!isInteger($orderid)) {
			return array (
				'orderid' => $orderid,
				'result' => false,
				'reason' => "$orderid is invalid"
			);
		}

		$sql = "SELECT * FROM xcart_orders WHERE orderid = '" . $orderid . "'";
		$order = func_query_first($sql);
		if (!$order) {
			return array (
				'orderid' => $orderid,
				'result' => false,
				'reason' => "$orderid does not exist"
			);
		}
		$parentOrderId = $order['group_id'];
		$sqlForFetchingParent = "SELECT * FROM xcart_orders WHERE orderid = '" . $parentOrderId . "'";
		$parentOrder = func_query_first($sqlForFetchingParent);
		
		$order['loyalty_credit'] = $order['loyalty_points_used']*$order['loyalty_points_conversion_factor'];
		if (strtolower($template) == 'out_for_delivery') {
			include_once ($xcart_dir . "/include/func/func.sms_alerts.php");
			if ($order['payment_method'] == 'on') {
				$msg = "Hi $order[firstname], your shipment with shipment id $order[orderid] and order id $order[group_id] is out for delivery. It will be delivered by our delivery staff $deliveryStaffName (Phone: $deliveryStaffMobileNumber) by the end of today. Thank you for shopping at Myntra";
			}
			elseif ($order['payment_method'] == 'cod') {
				$ezetapCartPaymentEnabledZipcodes = FeatureGateKeyValuePairs::getFeatureGateValueForKey('ezetap.cardpayment.enabled.zipcodes', 'none');
				$ezetapCartPaymentEnabledZipcodes = explode(",", $ezetapCartPaymentEnabledZipcodes);
				if($ezetapCartPaymentEnabledZipcodes[0] == 'all' || ($ezetapCartPaymentEnabledZipcodes[0] !== 'none' && in_array($order['s_zipcode'], $ezetapCartPaymentEnabledZipcodes))) {
					$msg = "Hi $order[firstname] your shipment with shipment id $order[orderid] and order id $order[group_id] is out for delivery and will be delivered by end of today. You can pay Rs.".number_format(round($order[total]), 2)." for this order by cash or by using your Visa/Mastercard credit or debit card. Please ask our delivery staff $deliveryStaffName (Phone: $deliveryStaffMobileNumber) for the option to pay by card. Thank you for shopping at Myntra";
                    $appendGoonjMessage = FeatureGateKeyValuePairs::getFeatureGateValueForKey('append.goonj.message', 'false');
                    if($appendGoonjMessage) {
                        $msg.= " Spread happiness this Diwali with Myntra & Goonj by donating your used clothes in good condition. Please hand over your items to our delivery staff. Thank you for delivering more smiles with Myntra";
                    }
					$template = 'out_for_delivery_cod_ezetap';
				} else {
					$msg = "Hi $order[firstname], your shipment with shipment id $order[orderid] and order id $order[group_id] is out for delivery. It will be delivered by our delivery staff $deliveryStaffName (Phone: $deliveryStaffMobileNumber) by the end of today. Please have Rs. ".number_format(round($order[total]), 2)." ready to pay for the order. Thank you for shopping at Myntra";
					$template = 'out_for_delivery_cod';
				}
			}
			if ($msg) {
				func_send_sms($order['mobile'], $msg);
			}
		}
		$delivery_address = $order['s_address'] . "<br>" . $order['s_city'] . "<br>" . $order['s_state'] . ", " . $order['s_country'] . " - " . $order['s_zipcode'];
		
        $productsIncart = func_create_array_products_of_order($orderid);
		$orderdetails = create_product_detail_table_new($productsIncart, $order);

		$courier_details = func_query_first("select courier_service,website from mk_courier_service where code = '" . $order['courier_service'] . "'");
		$subjectargs = array (
			'ORDER_ID' => $orderid
		);
		$args = array (
			"USER" => $order['firstname'],
			"ORDER_ID" => $order['group_id'],
			"SHIPMENT_ID" => $orderid,
			"TRACKING_NUMBER" => $order['tracking'],
			"COURIER_SERVICE" => $courier_details['courier_service'],
			"COURIER_WEBSITE" => $courier_details['website'],
			"ORDER_DETAILS" => $orderdetails,
			"DELIVERY_DATE" => $deliveredDate,
			"ORDER_AMOUNT" => number_format(round(func_get_order_payable_amount($order)), 2),
			"DECLARATION" => "This mail is intended only for " . $order['login'],
			"CUSTOMER_PHONE_NUMBER" => $order['mobile'],
			"DELIVERY_ADDRESS" => $delivery_address,
			"DELIVERY_STAFF_NAME" => $deliveryStaffName,
			"DELIVERY_STAFF_MOBILE_NUMBER" => $deliveryStaffMobileNumber,
			"SHIPPING_CHARGE" =>$parentOrder['shipping_cost']
		);
		
		if(strtolower($template == 'successful_delivery') || strtolower($template == 'delayed_delivery')) {
			$MFBConfigArray = array(
					"CUSTOMER_EMAIL"=> $order['login'],
					"REFERENCE_ID"=>$orderid,
					"REFERENCE_TYPE"=>"SHIPMENT",// SHIPMENT,ORDER,RETURN,SR
					"FEEDBACK_NAME"=>"shipment"
			);
			
			$MFBInstanceObj = new MFBInstance();
			$MFBContent = $MFBInstanceObj->getMFBMailContent($MFBConfigArray);
			
			if($MFBContent !== false){
				//send mail with MFB content appended as (type-critical)
				$args['MFB_FORM'] = $MFBContent['MFBMailHTML'];
			} else {
				$args['MFB_FORM'] = "";
			}
		}
		if(strtolower($template == 'delayed_delivery')){
		 $msg = "Dear $order[firstname], Your shipment $orderid for order $order[group_id] has been delivered. We apologise for not delivering the shipment by the promised date. We have refunded the delivery charges of Rs. $parentOrder[shipping_cost] for order. $order[group_id] to your Myntra cashback account.";
		  global $configMode;
	          $to = $order['mobile'];
       		  if($configMode != "prod"){
               	      $to = '9945668118';
       		  }
		  func_send_sms($to, $msg);
		}
		

		$customKeywords = array_merge($subjectargs, $args);
		$mailDetails = array (
			"template" => $template,
			"to" => $order['login'],
			"bcc" => "myntramailarchive@gmail.com",
			"header" => 'header',
			"footer" => 'footer',
			"mail_type" => self::getMailType()
		);
		$multiPartymailer = new \ MultiProviderMailer($mailDetails, $customKeywords);
		$flag = $multiPartymailer->sendMail();
		if (!$flag) {
			
			if($MFBContent !== false){
				$MFBInstanceObj->deleteMFBInstance($MFBContent['MFBInstanceId']);
			}
			
			return array (
				'orderid' => $orderid,
				'result' => false,
				'reason' => "could not send mail"
			);
		}
	}

	private static function get_template_for_event($event, $order) {
		if($event == self::ORDER_SHIPPED){
			$sql = "select xo.group_id,count(1) as actual,sum(case when xo.status in ('OH','Q','WP','PK') then 1 else 0 end) as to_be_processed,sum(case when xo.status in ('SH','DL','C') then 1 else 0 end) as processed from xcart_orders xo, xcart_orders xo1 where xo.group_id = xo1.group_id and xo.status in ('OH','Q','WP','PK','SH','DL','C') and xo1.orderid = ".$order['orderid']." group by xo.group_id";
			$order_stats = func_query_first($sql,true);
			if($order['courier_service'] == 'ML'){
				if($order_stats['actual'] > 1){
					if($order_stats['to_be_processed'] == 0){
						return array("template"=>"orderprocessedandhanddelivered_part","showOtherParts"=>false);				
					}else{
						return array("template"=>"orderprocessedandhanddelivered_part_first","showOtherParts"=>true);
					}	
				}else{
					return array("template"=>"orderprocessedandhanddelivered","showOtherParts"=>false);
				}
			}else{
				if($order_stats['actual'] > 1){
					if($order_stats['to_be_processed'] == 0){
						return array("template"=>"order_shipped_confirmation_part","showOtherParts"=>false);				
					}else{
						return array("template"=>"order_shipped_confirmation_part_first","showOtherParts"=>true);
					}	
				}else{
					return array("template"=>"order_shipped_confirmation","showOtherParts"=>false);
				}
			}
			
		}
	}

	public static function func_send_order_shipped_email($order, $userinfo) {
		global $sql_tbl, $weblog;
		$errorMsg = "";
		$orderid = $order['orderid'];

		$mobile = $order['issues_contact_number'];
		$customer_fname = $userinfo['firstname'];
		$courier_service = $order['courier_service'];
		$tracking = $order['tracking'];
                $warehouseId = $order['warehouseid'];
                $zipcode = $order['s_zipcode'];

		$courier = get_courier_by_code($courier_service);
		$template_response = self::get_template_for_event(self::ORDER_SHIPPED,$order);
		$template = $template_response['template'];

		$orders_in_group = func_query("Select orderid from $sql_tbl[orders] where group_id = " . $order['group_id'] . " and orderid != $orderid and status not in ('F', 'D')");
		//More orders present in this group.. then it is part shipment
		$items_in_different_shipments = array ();
		if ($orders_in_group && $template_response['showOtherParts']) {
			foreach ($orders_in_group as $order_row) {
				$other_order_items = func_create_array_products_of_order($order_row['orderid']);
				if ($other_order_items && !empty ($other_order_items)) {
					foreach ($other_order_items as $item)
						$items_in_different_shipments[] = $item;
				}
			}
		}

		$order_amount_detail = func_get_order_amount_details($orderid);
		$productsIncart = func_create_array_products_of_order($orderid);
		$order_details = create_product_detail_table_new($productsIncart, $order_amount_detail, "email", "", $items_in_different_shipments);
		//get delivery date based on shiptime and logistic selected
                $promiseDateRange = \FeatureGateKeyValuePairs::getInteger('lms.tat.promiseDateRange', 2);
                $shippedDate = time();
                if (!empty($order['shippeddate'])) {
                        if ($order['shippeddate'] > 0) {
                            $shippedDate = $order['shippeddate'];
                        }
                    }
                if ($order['shipping_method'] === 'XPRESS') {
                    $actualPromiseTime = \TatApiClient::getCachedMinimumTat($zipcode, array($warehouseId), $courier_service, 'EXPRESS');
                } 
                else if($order['shipping_method'] === 'SDD'){
                    $actualPromiseTime = \TatApiClient::getCachedMinimumTat($zipcode, array($warehouseId), $courier_service, 'SDD');
                }
                 else{
                     $actualPromiseTime = \TatApiClient::getCachedMinimumTat($zipcode, array($warehouseId), $courier_service, 'NORMAL') + $promiseDateRange * 24;
                }
                // Calculation of promise date. This has to be moved to LMS getPromiseDate API during migration
                $delivery_date = date("jS M Y", $shippedDate + $actualPromiseTime * 3600);
		// Send a mesage only when order is shipped.
		//Hi Raghu, your shipment with shipment id 332343 and order id 332111 has been shipped through Myntra Logistics and will be delivered to you by Jan 4, 2013. Thank you for shopping at Myntra
		$msg = "Hi " . $customer_fname . ", your shipment with shipment id $orderid and order id ". $order['group_id']." has been shipped through ";
		if ($courier_service == 'ML') {
			$msg .= "Myntra Logistics and will be delivered to you by ".$delivery_date.".";
		} else {
			$msg .= $courier['courier_service']." and will be delivered to you by ".$delivery_date.".";
			$msg .= " The $courier[courier_service] tracking code is $tracking.";
		}
		
		$msg .= " Thank you for shopping at Myntra";
		func_send_sms($mobile, $msg);
		
		$subjectargs = array (
			'ORDER_ID' => $order['group_id']
		);
		
		$args = array (
			"USER" => $userinfo['firstname'],
			"ORDER_ID" => $order['group_id'],
			"SHIPMENT_ID" => $orderid,
			"TRACKING_NUMBER" => $tracking,
			"COURIER_SERVICE" => $courier['courier_service'],
			"COURIER_WEBSITE" => $courier['website'],
			"SHIPMENT_DETAILS" => $order_details,
			"DELIVERY_DATE" => $delivery_date,
			"DECLARATION" => "This mail is intended only for " . $order['customer']
		);

		$customKeywords = array_merge($subjectargs, $args);
		$mailDetails = array (
			"template" => $template,
			"to" => $order['customer'],
			"bcc" => "myntramailarchive@gmail.com",
			"header" => 'header',
			"footer" => 'footer',
			"mail_type" => self::getMailType()
		);
		$multiPartymailer = new \ MultiProviderMailer($mailDetails, $customKeywords);
		$flag = $multiPartymailer->sendMail();

	}

	public static function send_oh_call_fail_mail($trial_number, $orderid) {
		$template = "CODOH_CALLING_MAIL_" . trim($trial_number);
		if (FeatureGateKeyValuePairs :: getFeatureGateValueForKey($template) == 'true') {
			$order = func_query_first("select login,firstname,from_unixtime(date,'%D %M %Y') as orderdate from xcart_orders where orderid = $orderid");
			$subjectargs = array (
				"ORDER_ID" => $orderid
			);

			$args = array (
				"USER" => $order['firstname'],
				"ORDER_ID" => $orderid,
				"ORDER_DATE" => $order['orderdate']
			);

			$mailDetails = array (
				"from_email" => "support@myntra.com",
				"from_name" => "Myntra Support",
				"template" => $template,
				"to" => $order['login'],
				"bcc" => "myntramailarchive@gmail.com",
				"header" => 'header',
				"footer" => 'footer',
				"mail_type" => self::getMailType()
			);
			$multiPartymailer = new \ MultiProviderMailer($mailDetails, $args);
			$flag = $multiPartymailer->sendMail();
		}
	}
	
	public static function send_exchange_order_mail($new_order, $template, $mailArgs,$isAdmin = true){
		$http_location = HostConfig::$httpHost;
		global $smarty;
		
		$myReturnsUrl = $http_location."/mymyntra.php?view=myreturns#";
		$myOrdersUrl = $http_location."/mailers/my/orders";
		$address.= $new_order['s_firstname']. $new_order['s_lastname']."<br>";
		$address.= $new_order['s_address']."<br>";
		$address.= $new_order['s_city']." - " . $new_order['s_zipcode']."<br>";
		$address.= $new_order['s_state'] . ", " . $new_order['s_country'];
		
		$products_sql = "SELECT xod.orderid, xod.itemid, product, sp.article_number AS article_number, sp.default_image, productid, product_style, xod.product_type, xod.price, amount, discount AS productDiscount, cart_discount_split_on_ratio As cartDiscount, coupon_discount_product as item_coupon_discount, item_status,
		quantity_breakup, xod.is_customizable, xod.is_returnable, xod.difference_refund, cash_redeemed AS item_cashdiscount, pg_discount AS item_pg_discount, cashback as item_cashback, ps.name as productStyleName, ps.label as productStyleLabel,
		som.sku_id, po.value as size, sp.global_attr_article_type,sp.global_attr_master_category, sp.global_attr_sub_category FROM
		xcart_order_details xod, mk_order_item_option_quantity oq, mk_styles_options_skus_mapping som, mk_style_properties sp, mk_product_options po, mk_product_style ps
		where xod.itemid = oq.itemid and oq.optionid = som.option_id and xod.product_style = sp.style_id and sp.style_id = ps.id and som.option_id = po.id and xod.item_status != 'IC' AND orderid = $new_order[orderid] ";
			
		$all_productsInCart = func_query($products_sql, true);
		$delivery_date = date("jS M Y", getDeliveryDateForOrder($new_order['queueddate'], $all_productsInCart, $new_order['s_zipcode'], $new_order['payment_mode'],$new_order['courier_service'], $new_order['warehouseid']));
		if($template == "exchange_failed_delivery"){
			$delivery_date = date("jS M Y", time());
		}

		$originalSize = func_query_first_cell("select quantity_breakup from xcart_order_details od, exchange_order_mappings eom 
				where od.itemid = eom.itemid and eom.exchange_orderid = $new_order[orderid]", true);
		$originalSize = explode(":", $originalSize);
		$originalSize = $originalSize[0];
		
		$newSize = $all_productsInCart[0]['quantity_breakup'];
		$newSize = explode(":", $newSize);
		$newSize = $newSize[0];
		
		$smarty->assign("address", $address);
		$smarty->assign("delivery_date", $delivery_date);
		$smarty->assign("originalSize", $originalSize);
		$smarty->assign("newSize", $newSize);
		$smarty->assign("exchangeid", $new_order['group_id']);
		$smarty->assign("productDesc", $all_productsInCart[0]['productStyleName']);
		$smarty->assign("quantity", $all_productsInCart[0]['amount']);
		if($mailArgs){
			$smarty->assign("courier", $mailArgs['courier_service_display']);
			$smarty->assign("tracking", $mailArgs['tracking']);
			$smarty->assign("deliveryStaff_name", $mailArgs['deliveryStaff_name']);
			$smarty->assign("deliveryStaff_mobile", $mailArgs['deliveryStaff_mobile']);
		}
		if($template == "exchange_confirmation" && $isAdmin){
			$shipmentdetails = func_display('admin/exchange_mail_details.tpl', $smarty, false);
		} else {
			$shipmentdetails = func_display('exchange_mail_details.tpl', $smarty, false);
		}
		
		// send sms to customer for the following cases only
		switch($template){
			case "exchange_confirmation":
				$msg = "Hi $new_order[firstname], Your exchange order with order id $new_order[group_id] has been successfully placed. It will be delivered to you by $delivery_date. 
				Please hand over the original item ( (".$all_productsInCart[0]['productStyleName'].") - Size ($originalSize)) to our delivery staff at the time of delivery. Thank you for shopping at Myntra";
				func_send_sms($new_order['mobile'], $msg);
				break;
			
			case "exchange_shipped":
				$msg = "Hi $new_order[firstname], Your shipment: $new_order[orderid] for exchange order: $new_order[group_id] has been shipped by Myntra Logistics and will be delivered to you by $delivery_date. Please hand over the original item ( ".$all_productsInCart[0]['productStyleName']." - Size $originalSize) to our delivery staff at the time of delivery.Thank you for shopping at Myntra";
				func_send_sms($new_order['mobile'], $msg);
				break;
			
			case "exchange_out_for_delivery":
				$msg = "Hi $new_order[firstname], Your shipment: $new_order[orderid] for exchange order: $new_order[group_id] is out for delivery. It will be delivered by our delivery staff $mailArgs[deliveryStaff_name] (Phone: $mailArgs[deliveryStaff_mobile]) by the end of today. 
				Please hand over the original item (".$all_productsInCart[0]['productStyleName']." - Size $originalSize) to our delivery staff at the time of delivery. Thank you for shopping at Myntra";
				func_send_sms($new_order['mobile'], $msg);
				break;
		}
		
		//build content and subject of the mail
		$subjectArgs = array();
		$bodyArgs = array(
				"USER"=>$new_order['firstname'],
				"SHIPMENT_DETAILS"=>$shipmentdetails,
				"DELIVERY_DATE"=>$delivery_date,
				"MY_ORDERS_URL"=>$myOrdersUrl,
				"MY_RETURNS_URL"=>$myReturnsUrl
		);
		
		if(strtolower($template) == 'exchange_successful_delivery') {
			$MFBConfigArray = array(
					"CUSTOMER_EMAIL"=> $new_order['email'],
					"REFERENCE_ID"=> $new_order['orderid'],
					"REFERENCE_TYPE"=>"EXCHANGE",// SHIPMENT,ORDER,RETURN,SR
					"FEEDBACK_NAME"=>"exchange"
			);
				
			$MFBInstanceObj = new MFBInstance();
			$MFBContent = $MFBInstanceObj->getMFBMailContent($MFBConfigArray);
				
			if($MFBContent !== false){
				//send mail with MFB content appended as (type-critical)
				$bodyArgs['MFB_FORM'] = $MFBContent['MFBMailHTML'];
			} else {
				$bodyArgs['MFB_FORM'] = "";
			}
		}
		

		$customKeywords = array_merge($subjectArgs, $bodyArgs);
		$mailDetails = array(
				"template" => $template,
				"to" => $new_order['login'],
				"bcc" => "myntramailarchive@gmail.com",
				"header" => 'header',
				"footer" => 'footer',
				"mail_type" => self::getMailType()
		);
		$multiPartymailer = new \MultiProviderMailer($mailDetails, $customKeywords);
		$flag = $multiPartymailer->sendMail();
			
		if(!$flag) {
			if($MFBContent !== false){
				$MFBInstanceObj->deleteMFBInstance($MFBContent['MFBInstanceId']);
			}
		}
		
		return $flag;
	}
	
	public static function sendOrderConfirmationNotification($orderid) {
		$order = func_query_first("SELECT gift_card_amount, group_id, group_concat(orderid) as releaseids, date, queueddate, firstname, s_firstname, s_lastname, s_address, s_locality, s_city, s_country, s_zipcode, sum(subtotal) as subtotal, sum(discount) as discount, sum(cart_discount) as cart_discount, sum(coupon_discount) as coupon_discount, sum(cash_redeemed) as cash_redeemed, sum(pg_discount) as pg_discount, sum(payment_surcharge) as payment_surcharge, sum(total) as total, sum(shipping_cost) as shipping_cost, sum(gift_charges) as gift_charges, sum(cod) as cod, sum(tax) as tax, sum(cashback) as cashback, sum(loyalty_points_used) as loyalty_points_used, sum(loyalty_points_awarded) as loyalty_points_awarded, loyalty_points_conversion_factor, payment_method, login, issues_contact_number FROM xcart_orders WHERE group_id = $orderid group by group_id");
		if($order) {
			$productsInCart = func_create_array_products_of_order(explode(",", $order['releaseids']));
            $order['loyalty_credit'] = $order['loyalty_points_used']*$order['loyalty_points_conversion_factor'];
			//$msg = "Hi $order[firstname], Your order with order id $orderid has been successfully placed. It will be shipped from our warehouse within the next 24 hours. Thank you for shopping at Myntra";
            $msg = "Hi $order[firstname], Your order with order id $orderid has been successfully placed. Once the items are shipped from our warehouse, we will send you an email with the expected delivery date. Thank you for shopping at Myntra";
			if($order['payment_method'] == "chq"){
				OrderProcessingMailSender::notifyOrderConfirmation('check',$productsInCart, $order, $order['firstname'], $order['login'], $orderid, $order);
			} else if($order['payment_method'] == "NP" || $order['payment_method'] == "on"){
				OrderProcessingMailSender::notifyOrderConfirmation('on',$productsInCart, $order, $order['firstname'], $order['login'], $orderid, $order);
			} else if($order['payment_method'] == "cod"){
				OrderProcessingMailSender::notifyOrderConfirmation('cod',$productsInCart, $order, $order['firstname'], $order['login'], $orderid, $order);
			}
			global $weblog;
			$weblog->info("Send sms ----  $msg ---- for order $orderid");
			func_send_sms_cellnext($order['issues_contact_number'], $msg);
			return true;
		}
		return false;
	}
	
	public static function notifyOrderConfirmation($payment_type,$order_products_detail, $order,$name,$email_to,$order_id,$customer,$ivrNumber="") {
		global $weblog, $cashback_gateway_status, $http_location, $enable_cashback_discounted_products;
		
		if($payment_type=="ivr"){
			$template =  "ivr_order_pending";
		}else {
			$template =  "order_confirmation";
		}
		
		//get delivery date based on ordertime and logistic feature gate value
		$delivery_date = "";//date("jS M Y", getDeliveryDateForOrder($order_amount_detail['date'], $order_products_detail, $order_amount_detail['zipcode'], $payment_type, $order_amount_detail['warehouseid']));
		
		$from = '';
		
		$onHandItems = array(); $jitSourcesItems = array(); $marketPlaceItems = array();
		foreach($order_products_detail as $product) {
			if($product['supply_type'] == 'ON_HAND' && $product['seller_id'] == 1) {
				$onHandItems[] = $product;
			} else if ($product['supply_type'] == 'JUST_IN_TIME' && $product['seller_id'] == 1) {
				$jitSourcesItems[] = $product;
			} else if ($product['seller_id'] > 1) {
				$jitSourcesItems[] = $product;
			}
		}
		
		if(!empty($onHandItems) && count($onHandItems) > 0) {
			foreach($onHandItems as $index=>$item) {
				$onHandItems[$index]['dispatch_date'] = date('d-m-Y', getDispatchTimeForProduct($order['queueddate'], $item['sku_id'])); 
			}
			$onhand_order_details = "The following items in your order will be shipped from our warehouse within 24 hours. Once these items have been shipped, we will send you an email with the expected delivery date.<br><br>";
			if(!empty($jitSourcesItems) && count($jitSourcesItems) > 0) {
                $onhand_order_details .= create_product_detail_table_new($onHandItems, false, "email", "", false, false, false, false);
            } else {
                $onhand_order_details .= create_product_detail_table_new($onHandItems, $order, "email", "", false, false, false, false);
            }
		} else {
			$onhand_order_details = "";
		}
		if(!empty($jitSourcesItems) && count($jitSourcesItems) > 0) {
			foreach($jitSourcesItems as $index=>$item) {
				$jitSourcesItems[$index]['dispatch_date'] = date('d-m-Y', getDispatchTimeForProduct($order['queueddate'], $item['sku_id']));
			}
			$jit_order_details = "The following items in your order are stocked at our partner locations and these will reach you as separate packages and at different times. As soon as these items are dispatched we will send you an email with the expected delivery date.<br><br>";
			$jit_order_details .= create_product_detail_table_new($jitSourcesItems, $order, "email", "", false, false, false, false);
		} else {
			$jit_order_details = "";
		}
		
		if($order['loyalty_points_awarded'] > 0){
                $loyaltyPointsAwarded = "<b>My Privilege</b><br/><br/> You have been awarded ".ceil($order['loyalty_points_awarded'])." points <a href=\"http://www.myntra.com/mymyntra.php?view=myprivilege#myprivilege\"> Click here to view</a><br/><br/>";
        }
        $delivery_address = $customer['s_firstname']."<br>".$customer['s_address']."<br>".$customer['s_city']."<br>".$customer['s_state'].", ".$customer['s_country']." - ".$customer['s_zipcode'];
		$casback_earnedcredits_enabled = FeatureGateKeyValuePairs::getBoolean('cashback.earnedcredits.enabled',true);
		$cashbackMessage = "";
		if($cashback_gateway_status == "on" && $casback_earnedcredits_enabled)
		{
			$url = $http_location."/mymyntra.php?view=mymyntcredits";
			if($enable_cashback_discounted_products == "true")	{
				$cashbackAmount = ($order['total'] +  $order['cash_redeemed']) * 0.1;
			} else {
				$cashbackAmount = 0.0;
				foreach($order_products_detail as $key=>$val){
					if($val['discount'] === 0 && $val['cart_discount_split_on_ratio']===0) {
						$cashbackAmount += ($val['totalPrice'] - $val['coupon_discount']) * 0.1;
					}
				}
			}
			if($cashbackAmount > 0){
				$cashbackAmount = round($cashbackAmount);
				$cashbackMessage ="";
				if($payment_type == "cod") {
					$cashbackMessage = "You will receive a cashback of Rs $cashbackAmount in your MyMyntra account after 30 days.";
				} else {
					$cashbackMessage = "You will receive a cashback of Rs. $cashbackAmount in your MyMyntra account within the next hour.";
				}
				$cashbackMessage .= " You can view your cashback account summary <a href=\"$url\">here</a>.<br>";
			}
		}
		$subjectargs = array("ORDER_ID"	=> $order_id);
		$args = array( 	"USER"	=> $name,
				"ORDER_ID"			=> $order_id,
				"ONHAND_ORDER_DETAILS"	    => $onhand_order_details,
				"JIT_ORDER_DETAILS"	    => $jit_order_details,
				"ORDER_DETAILS"	    => $order_details,
				"DELIVERY_DATE"		=> $delivery_date,
				"CASHBACK_SUMMARY"	=> $cashbackMessage,
                "LOYALTY_AWARDED"   => $loyaltyPointsAwarded,
				"DELIVERY_ADDRESS"	=> $delivery_address,
				"IVR_NUMBER"		=> $ivrNumber,
				"DECLARATION"       => "This mail is intended only for ".$email_to
		);
		
		$MFBConfigArray = array();
		
		// A-B test default variant 
		$MFBConfigArrayControl = array(
				"CUSTOMER_EMAIL"=> $email_to,
				"REFERENCE_ID"=>$order_id,
				"REFERENCE_TYPE"=>"ORDER",// SHIPMENT,ORDER,RETURN,SR
				"FEEDBACK_NAME"=>"order"
		);
		// A-B test variant		
		$MFBConfigArrayTest = array(
				"CUSTOMER_EMAIL"=> $email_to,
				"REFERENCE_ID"=>$order_id,
				"REFERENCE_TYPE"=>"SELECTION",// SHIPMENT,ORDER,RETURN,SR,SELECTION
				"FEEDBACK_NAME"=>"selection"
		);		
		
		// get the A-B test veraint based on NPSratio algo
		$NPStestResult = "control";// set the default A-B test result
		if (MABTestSegmentationAlgoFactory::getSegmentationAlgo("NPSRatio") != null){
			$NPSSegment = MABTestSegmentationAlgoFactory::getSegmentationAlgo("NPSRatio")->generateSegment("NPSRatio");
			$NPStestResult = ResolverFactory::getFastResolver("NPSRatio")->resolve($NPSSegment);
		}		
		
		// choose the MFB template based on A-B test
		if(strtolower($NPStestResult) == strtolower("control")){
			$MFBConfigArray = $MFBConfigArrayControl; 
		} else {
			$MFBConfigArray = $MFBConfigArrayTest;
		}
		
		$MFBInstanceObj = new MFBInstance();
		$MFBContent = $MFBInstanceObj->getMFBMailContent($MFBConfigArray);
			
		if($MFBContent !== false){
			//send mail with MFB content appended as (type-critical)
			$args['MFB_FORM'] = $MFBContent['MFBMailHTML'];
		} else {
			$args['MFB_FORM'] = "";
		}
		
				/*** myntra festival coupon generation :begins***/
		global $sql_tbl;
		$args["MYNTRA_FEST_MSG"]=""; // default value for order conformation mailer message
		$couponCode = $order['coupon'];
		$couponGroupName = func_query_first("select groupName from ".$sql_tbl['discount_coupons']." where coupon='$couponCode'");
		if(FeatureGateKeyValuePairs::getBoolean("myntrashoppingfestCoupons")){
			if(empty($couponCode) || (!empty($couponGroupName) && $couponGroupName["groupName"]!=WidgetKeyValuePairs::getWidgetValueForKey('myntraFestCouponGroupName'))){
				SpecialCouponGenerator::generateSpecialCouponsForOrder($order_id);
				$festCoupons = SpecialCouponGenerator::getAllSpecialCouponForOrder($order_id);
				if(sizeof($festCoupons)>0){
					$args["MYNTRA_FEST_MSG"]=WidgetKeyValuePairs::getWidgetValueForKey('myntraFestOrderConfirmationMailMsg');
				}else{
					$args["MYNTRA_FEST_MSG"]="";
				}
			}
		}
		/*** myntra festival coupon generation :ends**/
		$customKeywords = array_merge($subjectargs, $args);
	
		$mail_details = array(
			"template" => $template,
			"to" => $email_to,
			"bcc" => "myntramailarchive@gmail.com",
			"header" => 'header',
			"footer" => 'footer',
			"mail_type" => self::getMailType()
		);
		$multiPartymailer = new MultiProviderMailer($mail_details, $customKeywords);
		$mailstatus = $multiPartymailer->sendMail();
		
		if(!$mailstatus) {
			if($MFBContent !== false){
				$MFBInstanceObj->deleteMFBInstance($MFBContent['MFBInstanceId']);
			}
		}
		 
		return $mailstatus;
	}
	
	
	public static function notifyCodOrderOnHold($orderid) {
		$callNumber = WidgetKeyValuePairs::getWidgetValueForKey('customerSupportCall');
		$order = func_query_first("SELECT group_id, group_concat(orderid) as releaseids, date, firstname, s_firstname, s_lastname, s_address, s_locality, s_city, s_country, s_zipcode, sum(subtotal) as subtotal, sum(discount) as discount, sum(cart_discount) as cart_discount, sum(coupon_discount) as coupon_discount, sum(cash_redeemed) as cash_redeemed, sum(loyalty_points_used) as loyalty_points_used, loyalty_points_conversion_factor, sum(pg_discount) as pg_discount, sum(payment_surcharge) as payment_surcharge, sum(total) as total, sum(shipping_cost) as shipping_cost, sum(gift_charges) as gift_charges, sum(cod) as cod, sum(tax) as tax, sum(cashback) as cashback, payment_method, login, issues_contact_number FROM xcart_orders WHERE group_id = $orderid group by group_id");
		if($order) {
            $order['loyalty_credit'] = $order['loyalty_points_used']*$order['loyalty_points_conversion_factor'];
			$productsInCart = func_create_array_products_of_order(explode(",", $order['releaseids']));
			$msg = "Dear $order[firstname], We need to verify some details to confirm your order with order id $orderid. We will contact you at your phone number $customerMobileNo by the end of day today or tomorrow morning. If you prefer, you can call our customer support at $callNumber at your convenience to confirm this order";
			OrderProcessingMailSender::notifyCodOnHoldOrderManualVerification($productsInCart, $order, $order['firstname'], $order['login'], $orderid, 0, $order);
			func_send_sms_cellnext($order['issues_contact_number'], $msg);
			return true;
		}
		return false;
	}
	
	public static function notifyCodOnHoldOrderManualVerification($order_products_detail,$order_amount_detail,$name,$email_to,$order_id,$account_type,$customer) {
		global $weblog;
		
		$template = 'cod_manual_verification';
		$delivery_address = $customer['s_address']."<br>".$customer['s_city']."<br>".$customer['s_state'].", ".$customer['s_country']." - ".$customer['s_zipcode'];
		$order_details = create_product_detail_table_new($order_products_detail, $order_amount_detail);
		
		$subjectargs = array("ORDER_ID"	=> $order_id);
		$args = array( 	"USER"	=> $name,
				"ORDER_ID"		=> $order_id,
				"ORDER_DETAILS"	=> $order_details,
				"USER_PHONE_NO" => $order_amount_detail['issues_contact_number'],
				"DELIVERY_ADDRESS"  => $delivery_address,
				"DECLARATION"  => "This mail is intended only for ".$email_to
		);
		
		$customKeywords = array_merge($subjectargs, $args);
        global $telesales;
                
        if (!$telesales){
            $mail_details = array(
			"template" => $template,
			"to" => $email_to,
			"bcc" => "myntramailarchive@gmail.com",
			"header" => 'header',
			"footer" => 'footer',
			"mail_type" => self::getMailType()
			);
        } else {
           $mail_details = array(
				"template" => $template,
				"to" => $email_to,
                "cc" => "",
				"bcc" => "myntramailarchive@gmail.com",
				"header" => 'header',
				"footer" => 'footer',
				"mail_type" => self::getMailType()
           );
        }
		$multiPartymailer = new MultiProviderMailer($mail_details, $customKeywords);
		return $multiPartymailer->sendMail();
	}

	public static function notifyCodOnHoldOrderVerified($orderId) {
		global $http_location;
		
		$order = func_query_first("SELECT group_id, group_concat(orderid) as releaseids, date, queueddate, firstname, s_firstname, s_lastname, s_address, s_locality, s_city, s_country, s_zipcode, sum(subtotal) as subtotal, sum(discount) as discount, sum(cart_discount) as cart_discount, sum(coupon_discount) as coupon_discount, sum(cash_redeemed) as cash_redeemed, sum(loyalty_points_used) as loyalty_points_used, loyalty_points_conversion_factor, sum(pg_discount) as pg_discount, sum(payment_surcharge) as payment_surcharge, sum(total) as total, sum(shipping_cost) as shipping_cost, sum(gift_charges) as gift_charges, sum(cod) as cod, sum(tax) as tax, sum(cashback) as cashback, payment_method, login, issues_contact_number FROM xcart_orders WHERE group_id = $orderId group by group_id");
		$items = func_create_array_products_of_order(explode(",", $order['releaseids']));
		$order['loyalty_credit'] = $order['loyalty_points_used']*$order['loyalty_points_conversion_factor'];
		$delivery_date = date("jS M Y", getDeliveryDateForOrder($order['queueddate'], $items, $order['s_zipcode'], $order['payment_method'], $order['courier_service'], $order['warehouseid'], $order['orderid']));
		$order_details = create_product_detail_table_new($items, $order);
		$delivery_address = $order['s_address']."<br>".$order['s_city']."<br>".$order['s_state'].", ".$order['s_country']." - ".$order['s_zipcode'];
		
                $myOrdersUrl = $http_location."/mailers/my/orders"; 		
		//send sms
		$customerMobileNo = $order['issues_contact_number'];
		$msg = "Hi $order[firstname], Your order with order id $orderId has been successfully placed. It will be delivered to you by $delivery_date. Thank you for shopping at Myntra";
		func_send_sms_cellnext($customerMobileNo,$msg);
		
		$subjectargs = array("ORDER_ID"	=> $orderId);
		$args = array("USER"	=> $order['firstname'],
				"ORDER_ID"		=> $orderId,
				"ORDER_DETAILS"	=> $order_details,
				"USER_PHONE_NO" => $customerMobileNo,
				"DELIVERY_DATE"		=> $delivery_date,
				"MYORDERS_URL" => $myordersUrl,
				"CONTACTUS_URL" => $http_location."/contactus",
				"DELIVERY_ADDRESS"  => $delivery_address,
				"DECLARATION"  => "This mail is intended only for ".$order['login']
		);
		
		$customKeywords = array_merge($subjectargs, $args);
		
		$mail_details = array(
				"template" => 'cod_manual_verified',
				"to" => $order['login'],
				"bcc" => "myntramailarchive@gmail.com",
				"header" => 'header',
				"footer" => 'footer',
				"mail_type" => self::getMailType()
		);
		$multiPartymailer = new MultiProviderMailer($mail_details, $customKeywords);
		return $multiPartymailer->sendMail();
		
	}
	
	public static function notifyCodOnHoldOrderRettempt($orderId) {
		global $http_location;
		
		$order = func_query_first("Select * from xcart_orders where orderid = $orderId", true);
		$items = func_create_array_products_of_order($orderId);
		$order['loyalty_credit'] = $order['loyalty_points_used']*$order['loyalty_points_conversion_factor'];
		$order_details = create_product_detail_table_new($items, $order);
		$delivery_address = $order['s_address']."<br>".$order['s_city']."<br>".$order['s_state'].", ".$order['s_country']." - ".$order['s_zipcode'];
		
		$myOrdersUrl = $http_location."/mailers/my/orders"; 
		
		//send sms
		$customerMobileNo = $order['issues_contact_number'];
		$callNumber = WidgetKeyValuePairs::getWidgetValueForKey('customerSupportCall');
		$msg = "Dear $order[firstname], We need to verify some details to confirm your order with order id $orderId. We attempted to contact you at your phone number +91-$customerMobileNo today. We will call you again by end of day today or tomorrow morning. If you prefer, you can call our customer support at $callNumber at your convenience to confirm this order";
		func_send_sms_cellnext($customerMobileNo,$msg);
		
		//send email
		$subjectargs = array("ORDER_ID"	=> $orderId);
		$args = array("USER"	=> $order['firstname'],
				"ORDER_ID"		=> $orderId,
				"ORDER_DETAILS"	=> $order_details,
				"USER_PHONE_NO" => $customerMobileNo,
				"DELIVERY_ADDRESS"  => $delivery_address,
				"DECLARATION"  => "This mail is intended only for ".$order['login']
		);
		
		$customKeywords = array_merge($subjectargs, $args);
		
		$mail_details = array(
				"template" => 'cod_attempt_failure',
				"to" => $order['login'],
				"bcc" => "myntramailarchive@gmail.com",
				"header" => 'header',
				"footer" => 'footer',
				"mail_type" => self::getMailType()
		);
		$multiPartymailer = new MultiProviderMailer($mail_details, $customKeywords);
		return $multiPartymailer->sendMail();
	}
}

?>
