<?php

/*
 * Created on Feb 2, 2012
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 * 
 */

include_once($xcart_dir . "/include/func/func.mk_orderbook.php");
include_once($xcart_dir . "/include/func/func.order.php");
include_once($xcart_dir . "/modules/apiclient/SkuApiClient.php");
include_once($xcart_dir . "/include/class/oms/ItemManager.php");
include_once("$xcart_dir/include/class/class.mail.multiprovidermail.php");
include_once($xcart_dir . "/modules/apiclient/ReleaseApiClient.php");
include_once("$xcart_dir/include/class/oms/EventCreationManager.php");
include_once("$xcart_dir/modules/apiclient/ServiceabilityApiClient.php");
include_once("$xcart_dir/include/class/oms/OrderWHManagerOld.php");
include_once "$xcart_dir/modules/apiclient/TaxEngineClient.php";
include_once "$xcart_dir/modules/apiclient/TatApiClient.php";

class OrderWHManager {

    public static function loadWarehouseInvCounts($skuIds) {
        $skuDetails = SkuApiClient::getSkuDetailsInvCountMap($skuIds);
        $checkInStock = FeatureGateKeyValuePairs::getBoolean('checkInStock', true);
        if (!$checkInStock) {
            foreach ($skuDetails as $skuId => $sku) {
                $skuDetails[$skuId]['warehouse2AvailableItems'][1] = 99;
                $skuDetails[$skuId]['availableItems'] = 99;
            }
        }
        return $skuDetails;
    }

    public static function loadServiceabilityDetails($productsInCart, &$skuDetails, $zipcode, $payment_method, $serviceMode = 'DELIVERY', $checkPrice = false) {

        $requestData = constructRequestObjectForServiceabilityCheck($productsInCart, $zipcode, $serviceMode, $payment_method, false, $skuDetails, $checkPrice);

        /*
          $items = array();
          $products = func_query("select style_id, sku_id from mk_styles_options_skus_mapping where sku_id in (".implode(",",array_keys($skuDetails)).")",true);
          $sku_style_map = array();
          foreach($sku_styles as $sku_style){
          $sku_style_map[$sku_style['sku_id']] = $sku_style['style_id'];
          }
          $style_tags_map = getTagsForStyle(array_values($sku_style_map));

          foreach($skuDetails as $skuId => $skuDetail){
          $item = array('sku' => $skuId);
          $warehouses = array();
          foreach($skuDetail['availableItems'] as $whId => $invCount){
          if($invCount > 0){
          $item['warehouses'][] = array('warehouse' => $whId);
          }
          }
          $style_tags = $style_tags_map[$sku_style_map[$skuId]];
          foreach($style_tags as $style_tag){
          $item['tags'][] = array('tag' => $style_tag);
          }
          $items[] = array('item' => $item);
          } */

        $paymentMode = ($payment_method == 'cod') ? "cod" : "on";
        $serviceability_details = ServiceabilityApiClient::getServiceabilityDetails($requestData, $paymentMode);

        if (!$serviceability_details || empty($serviceability_details)) {
            return array('status' => false, 'reason' => 'lms is not available right now!');
        }

        foreach ($serviceability_details as $skuId => $whId2CourierLimitRuleMap) {
            if (!empty($whId2CourierLimitRuleMap)) {
                foreach ($whId2CourierLimitRuleMap as $whId => $courier2LimitRuleMap) {
                    $invCount = $skuDetails[$skuId]['warehouse2AvailableItems'][$whId];
                    unset($skuDetails[$skuId]['warehouse2AvailableItems'][$whId]);

                    if (!empty($courier2LimitRuleMap)) {
                        foreach ($courier2LimitRuleMap as $courier => $limitRule) {
                            $availabilityData = array('invCount' => $invCount);
                            foreach ($limitRule as $key => $value)
                                $availabilityData[$key] = $value;

                            $skuDetails[$skuId]['warehouse2AvailableItems'][$whId . "__" . $courier] = $availabilityData;
                        }
                    } else {
                        return array('status' => false, 'reason' => 'No courier to service this order fully');
                    }
                }
            } else {
                return array('status' => false, 'reason' => 'No courier to service this order fully');
            }
        }

        foreach ($skuDetails as $skuId => $skuDetail) {
            if (empty($skuDetail['warehouse2AvailableItems']))
                return array('status' => false, 'reason' => 'Some of the items are not serviceable from any warehouse');
        }

        return array('status' => true);
    }

    public static function updateInventoryAfterSplit($group_orderid, $whIdToOrderIdMap, $comment, $user) {
        $releaseIds = array();
        foreach ($whIdToOrderIdMap as $whId => $oids) {
            foreach ($oids as $oid) {
                decrement_sku($oid, $whId);
                $releaseIds[] = $oid;
                if ($group_orderid != $oid) {
                    func_addComment_order($oid, $user, 'INFO', 'Order Split', "Order created from $group_orderid. $comment");
                }
            }
        }
        if ($releaseIds > 1)
            putFreeItemShipmentOnHold($group_orderid);
    }

    public static function assignWarehouseForOrder($orderid, $shipping_address, $productsInCart = false, $sku_details = false, $new_order_comment = false, $courier_info_loaded = false, $serviceMode = 'DELIVERY') {
        global $weblog;
        if (!$productsInCart) {
            $productsInCart = func_create_array_products_of_order($orderid);
        }

        if (!$sku_details) {
            $skuIds = array();
            foreach ($productsInCart as $product) {
                $skuIds[] = $product['sku_id'];
            }
            $sku_details = OrderWHManager::loadWarehouseInvCounts($skuIds);
        }
        $order_details = func_query_first("select * from xcart_orders where orderid = $orderid");
        $canProcessOrder = true;
        $processingErrorMsg = "";
        $subject = "";
        if (!$sku_details) {
            $canProcessOrder = false;
            $processingErrorMsg = "Some items are not available in any warehouse";
            $subject = "Order OOS after payment";
        } else {
            foreach ($sku_details as $sku) {
                if (!isset($sku['warehouse2AvailableItems']) || empty($sku['warehouse2AvailableItems'])) {
                    $canProcessOrder = false;
                    $processingErrorMsg = "Some items are not available in any warehouse";
                    $subject = "Order OOS after payment";
                    break;
                }
            }
        }
        if ($order_details['ordertype'] == 'ex') {
            $serviceMode = 'EXCHANGE';
        }
        if ($canProcessOrder) {
            if (!$courier_info_loaded) {
                $serviceabilityResponse = OrderWHManager::loadServiceabilityDetails($productsInCart, $sku_details, $shipping_address['s_zipcode'], $order_details['payment_method'], $serviceMode);
                if ($serviceabilityResponse['status'] == false) {
                    $canProcessOrder = false;
                    $processingErrorMsg = "Some items are not serviceable from any warehouse";
                    $subject = "Order not serviceable after payment";
                }
            }
        }
        $weblog->info("Sku Details - " . print_r($sku_details, true));
        if (!$canProcessOrder) {
            $updateData['status'] = 'OH';
            $updateData['queueddate'] = null;
            $oh_reason = get_oh_reason_for_code('RPE');
            $content = "Order $orderid can not be processed as - $processingErrorMsg. Moving order to On Hold state. Please check the stock and queue this order manually";
            if ($oh_reason != null) {
                $updateData['on_hold_reason_id_fk'] = $oh_reason['id'];
                $sql = "select * from mk_order_action_reasons where action = 'oh_disposition' and reason = 'SCU'";
                $reason_text = func_query_first($sql, true);
                $commentArrayToInsert = Array("orderid" => $orderid,
                    "commenttype" => "Automated",
                    "commenttitle" => "status change",
                    "commentaddedby" => "admin",
                    "description" => "Putting the order on hold as : " . $reason_text['reason_display_name'] . " - $processingError",
                    "commentdate" => time());
                func_array2insert("mk_ordercommentslog", $commentArrayToInsert);
            }

            func_array2update('orders', $updateData, "orderid = $orderid");

            $mail_details = array(
                "from_email" => 'admin@myntra.com',
                "from_name" => 'OMS Admin',
                "to" => constant("ORDER_OOS_NOTIFICATION"),
                "mail_type" => MailType::NON_CRITICAL_TXN,
                "subject" => $subject,
                "content" => $content
            );
            $multiPartymailer = new MultiProviderMailer($mail_details);
            $multiPartymailer->sendMail();

            return false;
        }


        //If multiple items are there.. check if they can be processed from one single warehouse..
        if (count($productsInCart) > 1) {
            $whIdCourier2Itemid2QtyMap = OrderWHManager::checkForCommonWarehouse($productsInCart, $sku_details, $shipping_address, $order_details['payment_method']);
        }

        if (!$whIdCourier2Itemid2QtyMap)
            $whIdCourier2Itemid2QtyMap = OrderWHManager::getWarehouse2ItemQuantityInfo($productsInCart, $sku_details, $shipping_address, $order_details['payment_method']);

        $weblog->info("OrderWHManager: WarehouseId->ItemId->qty split - " . print_r($whIdCourier2Itemid2QtyMap, true));

        if (count($whIdCourier2Itemid2QtyMap) > 1) {
            // split is required..
            $whCourier2OrderId = OrderWHManager::splitOrder($orderid, $whIdCourier2Itemid2QtyMap, $new_order_comment);
        } else {
            // there is only 1 warehouse for all items. So no need to split anything..
            $whIdCourierKeys = array_keys($whIdCourier2Itemid2QtyMap);

            $whIdCourier = explode("__", $whIdCourierKeys[0]);
            $sql = "update xcart_orders set warehouseid = " . $whIdCourier[0] . ", courier_service = '" . $whIdCourier[1] . "' where orderid = $orderid";
            db_query($sql);
            $whCourier2OrderId[$whIdCourierKeys[0]] = $orderid;
            //updateShippingMethodForOrder($orderid);
        }

        OrderWHManager::assignTrackingNumberForOrders($whCourier2OrderId, $order_details['payment_method'], $order_details['s_zipcode']);

        return OrderWHManager::splitOrderForCourierAmountLimits(array_values($whCourier2OrderId));
    }

    public static function checkForCommonWarehouse($productsInCart, $sku_details, $shipping_address, $payment_method) {
        global $weblog;
        $commonWarehouseCourier = false;
        foreach ($productsInCart as $product) {
            $quantity_ordered = $product['quantity'];
            $price = ($product['total_amount'] - $product['coupon_discount'] - $product['cash_redeemed'] - $product['pg_discount']) / $product['quantity'];
            $warehouse2AvailableItems = $sku_details[$product['sku_id']]['warehouse2AvailableItems'];
            $availableWarehouseCouriers = array();
            foreach ($warehouse2AvailableItems as $whIdCourier => $availabilityData) {
                if ($availabilityData['invCount'] >= $quantity_ordered && ( $payment_method != 'cod' || ($payment_method == 'cod' && $availabilityData['CODLimit'] >= $price) )) {
                    $availableWarehouseCouriers[] = $whIdCourier;
                }
            }
            $weblog->info("checkForCommonWarehouse:: Item " . $product['itemid'] . " is available in Warehouses " . implode(",", $availableWarehouseCouriers));
            if ($commonWarehouseCourier === false) {
                $commonWarehouseCourier = $availableWarehouseCouriers;
            } else {
                $commonWarehouseCourier = array_intersect($commonWarehouseCourier, $availableWarehouseCouriers);
            }
        }

        $commonWarehouseCourier = array_values($commonWarehouseCourier);
        if (count($commonWarehouseCourier) == 0) {
            return false;
        } else if (count($commonWarehouseCourier) == 1) {
            $selectedWhCourier = $commonWarehouseCourier[0];
        } else {
            $selectedWhCourier = OrderWHManager::getPreferedWarehouseForAddress($shipping_address, $commonWarehouseCourier);
        }

        $whIdCourier2Itemid2QtyMap = array();
        foreach ($productsInCart as $product) {
            $whIdCourier2Itemid2QtyMap[$selectedWhCourier][$product['itemid']] = $product['quantity'];
        }

        return $whIdCourier2Itemid2QtyMap;
    }

    public static function getWarehouse2ItemQuantityInfo($productsInCart, $sku_details, $shipping_address, $payment_method) {
        global $weblog;

        // for each product, check available count and assign item and qty to different warehouses.
        $whIdCourier2ItemId2QuantityMap = array();
        foreach ($productsInCart as $product) {
            $quantity_ordered = $product['quantity'];
            $price = ($product['total_amount'] - $product['coupon_discount_product'] - $product['cash_redeemed'] - $product['pg_discount']) / $product['quantity'];
            $warehouseCourier2AvailablityData = $sku_details[$product['sku_id']]['warehouse2AvailableItems'];
            $weblog->info("OrderWHManager: warehouse->counts for item -  " . $product['itemid'] . " (" . $product['sku_id'] . ") = " . print_r($warehouseCourier2AvailablityData, true));

            $availableWarehouseCouriers = array();
            foreach ($warehouseCourier2AvailablityData as $whCourier => $availablityData) {
                if ($availablityData['invCount'] >= $quantity_ordered && ( $payment_method != 'cod' || ($payment_method == 'cod' && $availablityData['CODLimit'] >= $price) )) {
                    $availableWarehouseCouriers[] = $whCourier;
                }
            }

            $weblog->info("OrderWHManager: Item - " . $product['itemid'] . " with qty - $quantity_ordered is present in " . count($availableWarehouseCouriers) . " warehouse-courier combinations");

            if (count($availableWarehouseCouriers) == 0) {
                // need a split.. cannot be processed from single wh
                OrderWHManager::getItemCountPerWarehouse($product['itemid'], $quantity_ordered, $warehouseCourier2AvailablityData, $shipping_address, $whIdCourier2ItemId2QuantityMap, $price, $payment_method);
            } else if (count($availableWarehouseCouriers) == 1) {
                // full available in only one WH.. need to be processed from there
                $whIdCourier2ItemId2QuantityMap[$availableWarehouseCouriers[0]][$product['itemid']] = $quantity_ordered;
            } else {
                // available in multiple wh.. process from nearest... based on metrics
                $selectedWhIdCourier = OrderWHManager::getPreferedWarehouseForAddress($shipping_address, $availableWarehouseCouriers);

                $whIdCourier2ItemId2QuantityMap[$selectedWhIdCourier][$product['itemid']] = $quantity_ordered;
            }
        }
        return $whIdCourier2ItemId2QuantityMap;
    }

    private static function getPreferedWarehouseForAddress($shipping_address, $availableInWhCouriers) {
        global $weblog;

        $whid2CouriersMap = array();
        foreach ($availableInWhCouriers as $whCourier) {
            $warehouseCourier = explode("__", $whCourier);
            $whid2CouriersMap[$warehouseCourier[0]][] = $warehouseCourier[1];
        }

        $whIds = array_values(array_unique(array_keys($whid2CouriersMap)));
        $selectedWhId = $whIds[0];
        if (count($whIds) > 1) {
            $zipcode = $shipping_address['s_zipcode'];
            $first2digits = intval($zipcode / 10000);
            $weblog->info("Shipping Address zipcode prefix = $first2digits");
            $sql = "Select * from mk_shipping_cost_metrics where state_zipcode_prefix = '$first2digits' and warehouse_id in (" . implode(",", $whIds) . ")";
            $results = func_query($sql);
            if ($results) {
                $minimum_cost = $results[0]['shipping_cost'];
                $selectedWhId = $results[0]['warehouse_id'];
                foreach ($results as $row) {
                    $weblog->info("Wh Id scanned - " . $row['warehouse_id']);
                    if ($row['shipping_cost'] < $minimum_cost) {
                        $minimum_cost = $row['shipping_cost'];
                        $selectedWhId = $row['warehouse_id'];
                    }
                    $weblog->info("Min cost now - $minimum_cost and wh id - $selectedWhId");
                }
            }
        }

        return $selectedWhId . '__' . $whid2CouriersMap[$selectedWhId][0];
    }

    private static function getItemCountPerWarehouse($itemid, $req_quantity, $warehouseCourier2AvailablityData, $shipping_address, &$whIdCourier2ItemId2QuantityMap, $price, $payment_method) {
        // considering there are n warehouses...
        // check if it can be fulfilled from multiple warehouses
        //take only those warehouses..

        $whId2AvailableCountMap = array();
        foreach ($warehouseCourier2AvailablityData as $whIdCourier => $availablityData) {
            $whIdCourierArray = explode("__", $whIdCourier);
            if (!isset($whId2AvailableCountMap[$whIdCourierArray[0]]))
                $whId2AvailableCountMap[$whIdCourierArray[0]] = $availablityData['invCount'];
        }

        foreach ($warehouseCourier2AvailablityData as $whIdCourier => $availablityData) {
            $whIdCourierArray = explode("__", $whIdCourier);
            $availableInventoryCount = $whId2AvailableCountMap[$whIdCourierArray[0]];

            if ($availableInventoryCount == 0) {
                continue;
            }

            if (( $payment_method != 'cod' || ($payment_method == 'cod' && $availablityData['CODLimit'] >= $price))) {

                if ($req_quantity > $whId2AvailableCountMap[$whIdCourierArray[0]]) {
                    $whIdCourier2ItemId2QuantityMap[$whIdCourier][$itemid] = $whId2AvailableCountMap[$whIdCourierArray[0]];
                    $req_quantity -= $whId2AvailableCountMap[$whIdCourierArray[0]];
                    $whId2AvailableCountMap[$whIdCourierArray[0]] = 0;
                } else {
                    $whIdCourier2ItemId2QuantityMap[$whIdCourier][$itemid] = $req_quantity;
                    $req_quantity = 0;
                    break;
                }
            }
        }

        if ($req_quantity > 0) {
            /**
             * if there are lesser items in all warehouses together.. 
             * then we add difference in ordered quantity and available quantity to first wh in the loop and break;
             */
            foreach ($whIdCourier2ItemId2QuantityMap as $whIdCourier => $item2count) {
                $whIdCourier2ItemId2QuantityMap[$whIdCourier][$itemid] += $req_quantity;
                break;
            }
        }
    }

    public static function splitOrder($orderid, $whIdCourier2Itemid2QtyMap, $new_order_comment) {
        global $XCARTSESSID, $sql_tbl;
        $original_order = func_query_first("Select * from xcart_orders where orderid = $orderid");

        $all_orders = array();
        foreach ($whIdCourier2Itemid2QtyMap as $whIdCourier => $itemid2QtyMap) {
            $all_orders[$whIdCourier] = $original_order;
        }

        $items = func_query("Select * from xcart_order_details where orderid = $orderid");
        $itemid2ItemMap = array();
        foreach ($items as $item) {
            $itemid2ItemMap[$item['itemid']] = $item;
        }

        $itemids = array_keys($itemid2ItemMap);
        $item_options_qty = func_query("SELECT * from mk_order_item_option_quantity where itemid in (" . implode(",", $itemids) . ")");
        $itemid2OptionQtyMap = array();
        foreach ($item_options_qty as $row) {
            $itemid2OptionQtyMap[$row['itemid']] = $row;
        }

        foreach ($whIdCourier2Itemid2QtyMap as $whIdCourier => $item2QtyMap) {
            $all_orders[$whIdCourier] = ItemManager::assignItems($all_orders[$whIdCourier], $item2QtyMap, $itemid2ItemMap, $itemid2OptionQtyMap);
        }

        $whCourier2OrderId = array();
        $initialWHCourier = min(array_keys($all_orders));
        foreach ($all_orders as $whIdCourier => $order) {
            if ($whIdCourier == $initialWHCourier) {
                // original order
                $whcouriers = explode("__", $whIdCourier);
                $items = $order['items'];
                unset($order['items']);
                $order['warehouseid'] = $whcouriers[0];
                $order['courier_service'] = $whcouriers[1];
                $order['total'] += $order['shipping_cost'];
                $order['total'] += $order['gift_charges'];

                func_array2update('orders', $order, "orderid = $orderid");

                $itemIdsInOrder = array();
                foreach ($items as $item) {
                    $option_qty_mapping = $item['option_qty_mapping'];
                    unset($item['option_qty_mapping']);
                    unset($item['quantity']);
                    func_array2update('order_details', $item, "itemid = " . $item['itemid']);
                    func_array2update('mk_order_item_option_quantity', $option_qty_mapping, "itemid = " . $item['itemid']);
                    $itemIdsInOrder[] = $item['itemid'];
                }
                $whCourier2OrderId[$whIdCourier] = $orderid;

                $delete_moveditems_sql = "DELETE from $sql_tbl[order_details] where orderid = $orderid and itemid not in (" . implode(",", $itemIdsInOrder) . ")";
                db_query($delete_moveditems_sql);
            } else {
                // new orders
                $items = $order['items'];
                unset($order['items']);
                unset($order['orderid']);

                $whcouriers = explode("__", $whIdCourier);
                $new_orderid = create_new_order();
                $order['invoiceid'] = create_new_invoice_id($new_orderid);
                $order['orderid'] = $new_orderid;
                $order['warehouseid'] = $whcouriers[0];
                $order['courier_service'] = $whcouriers[1];

                $order['date'] = time();
                if ($order['status'] == 'WP') {
                    $order['queueddate'] = time();
                }
                $order['shipping_cost'] = 0.00;
                $order['gift_charges'] = 0.00;
                $order['payment_surcharge'] = 0.00;
                func_array2insert('orders', $order);

                foreach ($items as $item) {
                    $option_qty_mapping = $item['option_qty_mapping'];
                    unset($item['itemid']);
                    unset($item['option_qty_mapping']);
                    unset($item['quantity']);
                    $item['orderid'] = $new_orderid;
                    $new_itemid = func_array2insert('order_details', $item);
                    $option_qty_mapping['itemid'] = $new_itemid;
                    func_array2insert('mk_order_item_option_quantity', $option_qty_mapping);
                }
                $whCourier2OrderId[$whIdCourier] = $new_orderid;
            }
        }
        //updateShippingMethodForOrder(array_values($whCourier2OrderId));
        return $whCourier2OrderId;
    }

    // ---------------------------------------------------------------------------------------------------------------------------------------------------	
    // Reassign warehouse
    public static function assignOrderItemsToNewWarehouse($orderid, $itemid, $qty, $warehouseId, $user) {
        global $weblog;
        $order = func_query_first("Select * from xcart_orders where orderid = $orderid");

        $order['firstname'] = mysql_real_escape_string($order['firstname']);
        $order['lastname'] = mysql_real_escape_string($order['lastname']);
        $order['s_firstname'] = mysql_real_escape_string($order['s_firstname']);
        $order['s_lastname'] = mysql_real_escape_string($order['s_lastname']);
        $order['s_address'] = mysql_real_escape_string($order['s_address']);
        $order['s_locality'] = mysql_real_escape_string($order['s_locality']);
        $order['s_city'] = mysql_real_escape_string($order['s_city']);
        $order['b_address'] = mysql_real_escape_string($order['b_address']);
        $order['b_city'] = mysql_real_escape_string($order['b_city']);

        $item_results = ItemManager::getItemDetailsForItemIds($itemid);
        $item = $item_results[0];
        $skuId = $item['sku_id'];
        $whId2NameMap = WarehouseApiClient::getWarehouseId2NameMap();
        // whole order needs to be moved to new warehouse
        $orderComment = "";
        $original_wh_id = $order['warehouseid'];
        $courier = '';
        $item_split_govt_tax_restamp_required = false;
        //deciding courier
        //$skuDetails = OrderWHManager::loadWarehouseInvCounts($skuId);
        $skuDetails[$skuId] = array('availableItems' => $item['quantity'],
            'warehouse2AvailableItems' => array($warehouseId => $item['quantity']));
        $orderType = 'DELIVERY';
        if ($order['ordertype'] == 'ex') {
            $orderType = 'EXCHANGE';
        }
        $serviceabilityResponse = OrderWHManager::loadServiceabilityDetails($item_results, $skuDetails, $order['s_zipcode'], $order['payment_method'], $orderType);
        $price = ($item['total_amount'] - $item['coupon_discount_product'] - $item['cash_redeemed'] - $item['pg_discount']) / $item['quantity'];
        $warehouseCourier2AvailablityData = $skuDetails[$skuId]['warehouse2AvailableItems'];
        foreach ($warehouseCourier2AvailablityData as $whCourier => $availablityData) {
            $whIdCourierArr = explode("__", $whCourier);
            if ($whIdCourierArr[0] == $warehouseId && ( $order_details['payment_method'] != 'cod' || ($order_details['payment_method'] == 'cod' && $availablityData['CODLimit'] >= $price) )) {
                $courier = $whIdCourierArr[1];
                break;
            }
        }

        if ($order['qtyInOrder'] == $qty) {
            $order['courier_service'] = '';
            $order['tracking'] = '';
            $weblog->info("Moving all items in the order. Moving order to warehouse - $warehouseId");
            $order['warehouseid'] = $warehouseId;
            $order['courier_service'] = $courier;
            $orderComment = "$qty pieces of Item - $itemid moved from " . $whId2NameMap[$original_wh_id] . " to " . $whId2NameMap[$warehouseId] . ". Moving full order to target warehouse.";
        } else {
            $weblog->info("Part of order needs to be moved.. Creating new order.");
            $new_orderid = create_new_order();
            $order['qtyInOrder'] -= $qty;
            $new_order = $order;
            $new_order['orderid'] = $new_orderid;
            $new_order['invoiceid'] = create_new_invoice_id($new_orderid);
            $new_order['splitdate'] = time();
            $new_order['queueddate'] = time();
            $new_order['total'] = 0.00;
            $new_order['subtotal'] = 0.00;
            $new_order['coupon_discount'] = 0.00;
            $new_order['discount'] = 0.00;
            $new_order['pg_discount'] = 0.00;
            $new_order['payment_surcharge'] = 0.00;
            $new_order['cash_redeemed'] = 0.00;
            $new_order["store_credit_usage"] = 0.00;
            $new_order["earned_credit_usage"] = 0.00;
            $new_order["loyalty_points_used"] = 0.00;
            $new_order["loyalty_points_awarded"] = 0.00;
            $new_order['cashback'] = 0.00;
            $new_order['qtyInOrder'] = $qty;
            $new_order['tax'] = 0.00;
            $new_order['shipping_cost'] = 0.00;
            $new_order['gift_charges'] = 0.00;
            $new_order['courier_service'] = $courier;
            $new_order['tracking'] = '';
            $new_order['warehouseid'] = $warehouseId;
            $new_order['gift_card_amount'] = 0.00;

            $orderComment = "$qty pieces of Item - $itemid moved from " . $whId2NameMap[$original_wh_id] . " to " . $whId2NameMap[$warehouseId] . ". Created New Order - $new_orderid for warehouse - " . $whId2NameMap[$warehouseId] . ". ";

            $new_item = ItemManager::updateOrderItemMovement($order, $new_order, $item, $qty);

            if ($new_item) {
                $item_option_qty = $new_item['item_option_qty'];
                unset($new_item['item_option_qty']);
                unset($new_item['sku_id']);
                unset($new_item['quantity']);

                $itemid = func_array2insert('order_details', $new_item);
                $item_option_qty['itemid'] = $itemid;
                func_array2insert('mk_order_item_option_quantity', $item_option_qty);
                $orderComment .= " Created new item - " . $new_item['itemid'] . " with required no of pieces.";
                // Re-stamp tax for the original order because the item quantity has been changed
                $item_split_govt_tax_restamp_required = true;
            } else {
                $orderComment .= " Complete Item is moved to new order.";
            }

            $item_option_qty = $item['item_option_qty'];
            unset($item['item_option_qty']);
            unset($item['sku_id']);
            unset($item['quantity']);
            func_array2update('order_details', $item, "itemid = " . $item['itemid']);
            if ($item_option_qty)
                func_array2update('mk_order_item_option_quantity', $item_option_qty, 'itemid = ' . $item['itemid']);
        }

        func_array2update('orders', $order, 'orderid = ' . $order['orderid']);
        if ($new_order) {
            if ($new_order['total'] + $new_order['gift_charges'] <= 0.00)
                $new_order['payment_method'] = 'on';

            func_array2insert('orders', $new_order);
            ReleaseApiClient::pushOrderReleaseToWMS($new_orderid, $user, '');
            //updateShippingMethodForOrder(array($orderid,$new_orderid));
            $whCourier2OrderId[$warehouseId . "__" . $courier] = $new_orderid;
            OrderWHManager::assignTrackingNumberForOrders($whCourier2OrderId, $new_order['payment_method'], $new_order['s_zipcode']);
        }else {
            //updateShippingMethodForOrder($orderid);
            $whCourier2OrderId[$warehouseId . "__" . $courier] = $orderid;
            OrderWHManager::assignTrackingNumberForOrders($whCourier2OrderId, $order['payment_method'], $order['s_zipcode']);
        }
        
        if ($item_split_govt_tax_restamp_required) {
            self::stampTax($orderid);
        }
        
        // decrease blocked count from original wh and move it to new warehouse..
        SkuApiClient::moveSkuBlockedOrderCount($skuId, $qty, $original_wh_id, $warehouseId, $user);
        
        ReleaseApiClient::pushOrderReleaseToWMS($orderid, $user, 'update');
        
        EventCreationManager::pushCompleteOrderEvent($order['group_id']);

        func_addComment_order($orderid, $user, 'Note', 'Warehouse Reassignment', $orderComment);

        return array('SUCCESS', "Warehouse reassignment successful for $qty pieces of Item");
    }

    public static function stampPromiseDates($orderId) {
        // The order id here is the release id
        // Assuming that we have to stamp considering the current time as the queued date
        // Get all the order details for the particular release
        $order_details = func_query_first("select * from xcart_orders where orderid = $orderId");
        $promiseDateEntries = \TatApiClient::getPromiseDate($order_details['s_zipcode'], $order_details['warehouseid'], $order_details['courier_service'], $order_details['shipping_method']);
        // Stamp the promise date entries into order_additional_info
        //
        // TODO: Combine all the queries into single bulk insert query
        db_query("insert into order_additional_info(order_id_fk,`key`,value,created_on) values"
            . " ($orderId,'EXPECTED_PICKING_TIME','" . $promiseDateEntries['pickByCutOff'] . "',now()),"
            . " ($orderId,'EXPECTED_QC_TIME','" . $promiseDateEntries['qcByCutOff'] . "',now()),"
            . " ($orderId,'EXPECTED_PACKING_TIME','" . $promiseDateEntries['packByCutOff'] . "',now()),"
            . " ($orderId,'EXPECTED_CUTOFF_TIME','" . $promiseDateEntries['shippingCutOff'] . "',now()),"
            . " ($orderId,'CUSTOMER_PROMISE_TIME','" . $promiseDateEntries['promiseDate'] . "',now())");
    }
    
    public static function stampTax($releaseId) {
        $showGovtTax = \WidgetKeyValuePairs::getWidgetValueForKey('oms.stampGovtTaxEnabled');
        if ($showGovtTax == NULL) {
            $showGovtTax = false;
        } else if ($showGovtTax == "true") {
            self::fetchAndStampGovtTax($releaseId);
        }
    }
    
    public static function fetchAndStampGovtTax($releaseId) {
        $fetchReleaseInfoQuery = "select * from xcart_orders where orderid = ".$releaseId;
        $releaseInfo = func_query_first($fetchReleaseInfoQuery);
        $sourceWarehouseId = $releaseInfo['warehouseid'];
        $destinationPincode = $releaseInfo['s_zipcode'];
        $fetchLineInfoQuery = "select * from xcart_order_details where orderid = $releaseId";
        $lineInfo = func_query($fetchLineInfoQuery);
        $lines = array();
        if (!is_array($lineInfo)) {
            $lines = array($lineInfo);
        } else {
            $lines = $lineInfo;
        }
        // Total money paid
        $releaseValue = $releaseInfo['total'] + $releaseInfo['cash_redeemed'] + $releaseInfo['loyalty_points_used'] * $releaseInfo['loyalty_points_conversion_factor'] - $releaseInfo['shipping_cost'] - $releaseInfo['gift_charges'];
        foreach ($lineInfo as $singleLineInfo) {
            if ($singleLineInfo['item_status'] != 'IC') {
                $itemid = $singleLineInfo['itemid'];
                $styleId = $singleLineInfo['product_style'];
                $itemValue = $singleLineInfo['price'] * $singleLineInfo['amount'] - $singleLineInfo['discount'] - $singleLineInfo['coupon_discount_product'] - $singleLineInfo['cart_discount_split_on_ratio'] + $singleLineInfo['taxamount'];
                if ($releaseValue > 0) {
                    $itemLevelCost = ($itemValue / $releaseValue) * ($releaseInfo['shipping_cost'] + $releaseInfo['gift_charges']);
                }
                $itemCost = $singleLineInfo['price'] * $singleLineInfo['amount'] - $singleLineInfo['discount'] - $singleLineInfo['coupon_discount_product'] - $singleLineInfo['cart_discount_split_on_ratio'] + $singleLineInfo['taxamount'] + $itemLevelCost;
                $taxInfo = self::fetchGovtTax($styleId, $itemCost, $sourceWarehouseId, $destinationPincode);
                $itemUpdateQuery = "update xcart_order_details set govt_tax_rate = " . $taxInfo['govtTaxRate'] . ", govt_tax_amount = " . $taxInfo['govtTaxAmount'] . "where itemid = $itemid";
                db_query($itemUpdateQuery);
            }
        }
    }

    public static function fetchGovtTax($styleId, $itemCost, $sourceWarehouseId, $destinationPincode) {
        global $login;
        $taxResponse = \TaxEngineClient::fetchGovtTax($sourceWarehouseId, $destinationPincode, $styleId, $itemCost, $login);
        if ($taxResponse['status'] == 'success') {
            return $taxResponse['data'];
        }
    }
    
    public static function assignTrackingNumberForOrders($whCourier2OrderId, $paymentMethod, $zipcode) {
        global $weblog;
        $orderid2TrackingNo = array();
        foreach ($whCourier2OrderId as $whCourierKey => $orderid) {
            self::stampTax($orderid);
            self::stampPromiseDates($orderid);
            $response = autoTrackingNoAssignment($orderid);
            $orderid2TrackingNo[$orderid] = $response[$orderid];
            /* $whIdCourierArray = explode("__", $whCourierKey);
              $weblog->info("Fetch tracking no for courier - ". $whIdCourierArray[1]." and warehouseid - ".$whIdCourierArray[0]);
              $trackingResponse = CourierApiClient::getTrackingNumberForCourier($whIdCourierArray[1], $whIdCourierArray[0], $paymentMethod=='cod'?'true':'false', $zipcode);
              if($trackingResponse && !empty($trackingResponse['trackingNumber']) && !empty($trackingResponse['trackingNumber'][0])){
              $orderid2TrackingNo[$orderid] = $trackingResponse['trackingNumber'][0];
              $update_data = array("tracking"=> $trackingResponse['trackingNumber'][0]);
              func_array2update('xcart_orders', $update_data, "orderid=$orderid");
              func_addComment_order($orderid, "LMS", "Automated", "Tracking No Assignment", "Order assigned Tracking #: " . $update_data["tracking"]);
              }else{
              $weblog->info("Order id: $orderid  No tracking number found");
              func_addComment_order($orderid, "LMS", "Automated", "Tracking No Assignment", "No tracking number found");
              } */
        }
        return $orderid2TrackingNo;
    }

    public static function splitOrderForCourierAmountLimits($orderids) {
        global $weblog;
        if (!is_array($orderids)) {
            $orderids = array($orderids);
        }
        $orders = func_query("select * from xcart_orders where orderid in (" . implode(",", $orderids) . ")");
        $whId2OrderidsMap = array();

        foreach ($orders as $order) {
            $courierLimits = CourierApiClient::getCapacityForCourier($order['courier_service']);
            $capacity = $courierLimits['CODLimit'];
            $weblog->info("Capacity for courier = " . $capacity);
            if ($order['payment_method'] == 'cod' && $order['total'] > $capacity) {
                $order_details = func_query("select * from xcart_order_details where orderid = $order[orderid] order by price");
                $itemid2ItemMap = array();
                $shipments = array();
                $allItemsAdded = false;
                foreach ($order_details as $index => $item) {
                    $itemid2ItemMap[$item['itemid']] = $item;
                    $order_details[$index]['unitprice'] = ($item['total_amount'] - $item['coupon_discount_product'] - $item['cash_redeemed'] - $item['pg_discount']) / $item['amount'];
                }

                while (!$allItemsAdded) {
                    $allItemsAdded = true;
                    $currentShipment = array('price' => 0);
                    foreach ($order_details as $index => $item) {
                        if ($item['amount'] > 0) {
                            $price = $item['unitprice'];
                            $qty = $item['amount'];

                            $remainingCapacity = $capacity - $currentShipment['price'];
                            if ($price * $qty >= $remainingCapacity) {
                                $qtyToAdd = intval($remainingCapacity / $price);
                            } else {
                                $qtyToAdd = $qty;
                            }
                            if ($qtyToAdd > 0) {
                                $currentShipment['price'] += $price * $qtyToAdd;
                                $currentShipment['items'][$item['itemid']] = $qtyToAdd;
                                $order_details[$index]['amount'] -= $qtyToAdd;
                            }

                            if ($order_details[$index]['amount'] > 0)
                                $allItemsAdded = false;
                        }
                    }

                    $shipments[] = $currentShipment;
                }

                $weblog->info("Shipments to be Created  - " . $shipments);

                $itemids = array_keys($itemid2ItemMap);
                $item_options_qty = func_query("SELECT * from mk_order_item_option_quantity where itemid in (" . implode(",", $itemids) . ")");
                $itemid2OptionQtyMap = array();
                foreach ($item_options_qty as $row) {
                    $itemid2OptionQtyMap[$row['itemid']] = $row;
                }
                $orderid = $order['orderid'];

                foreach ($shipments as $idx => $shipment) {
                    $all_orders[] = ItemManager::assignItems($order, $shipment['items'], $itemid2ItemMap, $itemid2OptionQtyMap);
                }

                foreach ($all_orders as $key => $orderAfterSplit) {
                    if ($key == 0) {
                        // original order
                        $items = $orderAfterSplit['items'];
                        unset($orderAfterSplit['items']);
                        $orderAfterSplit['total'] += $orderAfterSplit['shipping_cost'];
                        $orderAfterSplit['total'] += $orderAfterSplit['gift_charges'];

                        func_array2update('orders', $orderAfterSplit, "orderid = $orderid");

                        $itemIdsInOrder = array();
                        foreach ($items as $item) {
                            $option_qty_mapping = $item['option_qty_mapping'];
                            unset($item['option_qty_mapping']);
                            func_array2update('order_details', $item, "itemid = " . $item['itemid']);
                            func_array2update('mk_order_item_option_quantity', $option_qty_mapping, "itemid = " . $item['itemid']);
                            $itemIdsInOrder[] = $item['itemid'];
                        }

                        $delete_moveditems_sql = "DELETE from xcart_order_details where orderid = $orderid and itemid not in (" . implode(",", $itemIdsInOrder) . ") and item_status != 'IC'";
                        db_query($delete_moveditems_sql);
                        $whId2OrderidsMap[$order['warehouseid']][] = $orderid;
                    } else {
                        // new orders
                        $items = $orderAfterSplit['items'];
                        unset($orderAfterSplit['items']);
                        unset($orderAfterSplit['orderid']);

                        $new_orderid = create_new_order();
                        $orderAfterSplit['invoiceid'] = create_new_invoice_id($new_orderid);
                        $orderAfterSplit['orderid'] = $new_orderid;
                        unset($orderAfterSplit['tracking']);
                        $orderAfterSplit['date'] = time();
                        if ($orderAfterSplit['status'] == 'WP') {
                            $orderAfterSplit['queueddate'] = time();
                        }
                        $orderAfterSplit['shipping_cost'] = 0.00;
                        $orderAfterSplit['gift_charges'] = 0.00;
                        $orderAfterSplit['payment_surcharge'] = 0.00;
                        func_array2insert('orders', $orderAfterSplit);

                        foreach ($items as $item) {
                            $option_qty_mapping = $item['option_qty_mapping'];
                            unset($item['itemid']);
                            unset($item['option_qty_mapping']);

                            $item['orderid'] = $new_orderid;
                            $new_itemid = func_array2insert('order_details', $item);
                            $option_qty_mapping['itemid'] = $new_itemid;
                            func_array2insert('mk_order_item_option_quantity', $option_qty_mapping);
                        }

                        $whCourier2OrderId[$order['warehouseid'] . "__" . $order['courier_service']] = $new_orderid;
                        OrderWHManager::assignTrackingNumberForOrders($whCourier2OrderId, $order['payment_method'], $order['s_zipcode']);
                        $whId2OrderidsMap[$order['warehouseid']][] = $new_orderid;
                    }
                }
            } else {
                $whId2OrderidsMap[$order['warehouseid']][] = $order['orderid'];
            }
        }
        return $whId2OrderidsMap;
    }

}

?>
