<?php
global $xcart_dir;
include_once($xcart_dir."/include/func/func.returns.php");
include_once($xcart_dir."/modules/apiclient/LocationApiClient.php");
include_once("$xcart_dir/include/class/class.feature_gate.keyvaluepair.php");
include_once("$xcart_dir/include/class/widget/class.widget.keyvaluepair.php");
include_once("$xcart_dir/modules/apiclient/ShipmentApiClient.php");
include_once("$xcart_dir/modules/apiclient/ItemApiClient.php");
include_once("$xcart_dir/modules/apiclient/DeliveryCenterApiClient.php");
include_once("$xcart_dir/include/class/oms/EventCreationManager.php");
include_once($xcart_dir."/include/func/func.sms_alert.php");

class ReturnsStatusProcessor{
	
	public function getReturnDetails($returnid){
		if(!isInteger($returnid)){
			return array('status'=>false,'reason'=>"$returnid is invalid");
		}		
		$return = format_return_request_fields($returnid);
		if(!$return){
			return array('status'=>false,'reason'=>"$returnid does not exist");
		}
		return array('status'=>true,'return_details' => $return);
	}
		
	public function isValidTransition($fromStatus,$toStatus){
		$query = "SELECT to_state from mk_return_state_transitions where from_state = '$fromStatus' and to_state = '$toStatus'";
		$transition = func_query_first($query,true);
		if(!$transition){
			return array('status'=>false,'reason'=>"Transition from ".getReturnStatusByCode($fromStatus)." to ".getReturnStatusByCode($toStatus)." is not allowed");			
		}else{
			return array('status'=>true);
		}		
	}
	
	public function changeReturnsStatus($returnid, $status, $courier_service, $tracking_no, $itemBarCodes, $qcReason, $login,$user_comment,$shipped_items = '', $warehouseid=NULL,$deliveryCenterCode = '',$validated_reasoncode='', $tokenNo = ''){
		// is valid return
		global $weblog;
        global $errorlog;
		$return_details_result = $this->getReturnDetails($returnid);
		if(!$return_details_result['status']){
			return $return_details_result;
		}
		$return_details = $return_details_result['return_details'];
		// is status change allwowed
		$from_status = $return_details['status'];
		$allowed_transition = $this->isValidTransition($return_details['status'], $status);
		if(!$allowed_transition['status']){
			return $allowed_transition;
		}
		$title = "Remarks (".getReturnStatusByCode($status).")";
		$error = false;
		$errorMsg = "Unexpected Error occured.";
		$orderid = $return_details['orderid'];
		$itemid = $return_details['itemid'];
		$return_data = array('status'=>$status);
		
		// basic input checks
		if(($status == 'RQF' || $status == 'RQP') && $itemBarCodes == ''){
			$error = true;
			$errorMsg = 'Item barcode(s) is mandatory for this transition';
		}
		
		if(($status == 'RADC' || $status == 'RJDC') && $deliveryCenterCode == ''){
			$error = true;
			$errorMsg = 'DC-Code is mandatory for this transition';
		}
		
		if($status == 'RRC' && $warehouseid == NULL){
			$error = true;
			$errorMsg = 'Warehouse location not present for accpeting items';
		}
		
		if(($status == 'RIS' || $status == 'RJS') && $qcReason == false) {
			$error = true;
			$errorMsg = 'Please select quality check update reason'; 
		}
		
		// material movement
		if(!$error){
			if($status == 'RRQP' || $status == 'RPUQ2' || $status == 'RPUQ3'){
				$pickup_supp_couriers = func_get_pickup_couriers($return_details['zipcode'], $return_details['style_id'], $return_details['sku_id']);
				if(is_return_eligible_for_pushing_to_ld($pickup_supp_couriers[0])){
					$lms_pickup_status = ShipmentApiClient::createReturnPickUpRequest($returnid,$pickup_supp_couriers[0]);
					if($lms_pickup_status['status'] == 'success'){
						if($status == 'RRQP'){
							add_returns_logs_usercomment($returnid, $user_comment, $login, $title,$from_status,$status);
							$from_status = $status;
							$user_comment = "auto advancing to RPI";
							$status = 'RPI';
							$return_data['status'] = 'RPI';
                                                        $return_data['courier_service'] = $pickup_supp_couriers[0];
                                                        $return_data['tracking_no'] = $lms_pickup_status['tracking_number'];
						}
						if($status == 'RPUQ2'){
							add_returns_logs_usercomment($returnid, $user_comment, $login, $title,$from_status,$status);
							$from_status = $status;
							$user_comment = "auto advancing to RPI2";
							$status = 'RPI2';
							$return_data['status'] = 'RPI2';
						}
                                                if($status == 'RPUQ3'){
							add_returns_logs_usercomment($returnid, $user_comment, $login, $title,$from_status,$status);
							$from_status = $status;
							$user_comment = "auto advancing to RPI3";
							$status = 'RPI3';
							$return_data['status'] = 'RPI3';
						}
						$return_data['dc_code'] = $lms_pickup_status['deliveryCenterCode'];
					}else{
						$errorMsg = '';
					}	
				}else{
					$return_data['courier_service'] = $pickup_supp_couriers[0];
				}	
			}
			
			if($itemBarCodes && $itemBarCodes != "" && ($status == 'RQF' || $status == 'RQP')) {
				$itemBarCodes = explode(",", $itemBarCodes);
				$scannedItemBarCodes = array_unique($itemBarCodes);
				if(count($scannedItemBarCodes) != $return_details['quantity']){
					$error = true;
					$errorMsg = "Number of items scanned donot match the number of items to be received in the return";
				}
				if(!$error){
					if(!empty($shipped_items)){
						$to_be_returned = explode(",", $shipped_items);
						$returnAtWhId = $return_details['warehouseid'];
						$sourceWhId = $return_details['source_warehouseid'];
						$myntraOwnedWarehouses = FeatureGateKeyValuePairs::getFeatureGateValueForKey('wms.myntra.warehouses');
						$myntraOwnedWarehouses = explode(",", $myntraOwnedWarehouses);
						if(!in_array($sourceWhId, $myntraOwnedWarehouses))
							$returnAtWhId = $sourceWhId;
						//$response = LocationApiClient::acceptItemsAtWarehouse($to_be_returned, $returnAtWhId, $login);
						$response = ItemApiClient::moveItemsToAcceptedReturns($to_be_returned,$returnAtWhId,$login);
						if(!empty($response)){
							$error = true;
							$errorMsg = "Unable to accept items. Error - ".$response;
						}
					}
				}
			}
			if($status == 'RADC' || $status == 'CPDC'){
				$dcid = DeliveryCenterApiClient::getDeliveryCenterID($deliveryCenterCode);
				if($dcid['status']=='success'){
					$deliveryCenterId = $dcid['data']['id'];
					$weblog->info("returnid - $returnid, DC-CODE - $deliveryCenterCode, DC_id - $deliveryCenterId");
					
					$pushToLMS = ShipmentApiClient::createReturnPickUpRequest($returnid,'ML','RT',$deliveryCenterId);
					if($pushToLMS['status'] == "success") {
						$msg = "Return pushed successfully for local delivery.";
						$weblog->info($msg);
					}else {
						$error = true;
						$errorMsg = "Error occurred while pushing return for local delivery.";
						$weblog->info($errorMsg);
					}
				}
				else {
					$weblog->error($dcid['msg']);
					$errorlog->error($dcid['msg']);
					$error = true;
					$errorMsg = $dcid['msg'];
				}
			}
			
			/* if(($status == 'RPI' || $status == 'RPI2') && empty($tokenNo) && $courier_service != 'ML') {
				$error = true;
				$errorMsg = "Token Number is needed for marking pickup initiated for 3PL couriers.";
			} */
			
			if($status == 'RIS' || $status == 'RJS'){
				$respstatus = true;
				
				$returnAtWhId = $return_details['warehouseid'];
				$sourceWhId = $return_details['source_warehouseid'];
				$myntraOwnedWarehouses = FeatureGateKeyValuePairs::getFeatureGateValueForKey('wms.myntra.warehouses');
				$myntraOwnedWarehouses = explode(",", $myntraOwnedWarehouses);
				if(!in_array($sourceWhId, $myntraOwnedWarehouses))
					$returnAtWhId = $sourceWhId;
				
				$respstatus = move_returned_items_to_CR($return_details['orderid'], $returnAtWhId, $return_details['itembarcode'], $qcReason['quality'], $login, $qcReason['rejectReason'], $qcReason['rejectReasonDescription'], $return_details['warehouseid']);
				/*if($respstatus === true) {
					$weblog->info("return_request.php: Item marked for restocking");
				} else {
					$error = true;
					$errorMsg = $respstatus;
				}*/
			}
			if($status == 'RRD' && $return_details['return_mode'] == 'pickup' && $return_details['dc_code'] != null){
				$respstatus = true;
				$respstatus = ShipmentApiClient::rejectReturn($return_details['returnid']);
				if($respstatus['status'] == "success") {
					$weblog->info("return_request.php: return rejected in lms");
				} else {
					$error = true;
					$errorMsg = "Return rejection failed in lms with message ::::: ".$respstatus['msg'];
				}
			}
		}
		
		
		if(!$error) {
			$order_details = array();
			if($courier_service != ''){
				$return_data['courier_service'] = mysql_real_escape_string($courier_service);
			}
			if($tracking_no != ''){
				$return_data['tracking_no'] = $tracking_no;
			}
			if($deliveryCenterCode != ''){
				$return_data['dc_code'] = $deliveryCenterCode;
			}
			
			$args = array();
			
			if(($status == 'RPI' || $status == 'RPI2' || $status == 'RPI3') && !empty($tokenNo) && $courier_service != 'ML') {
				$return_data['token_no'] = $tokenNo;
			}
			
			if(($status == 'RQF' || $status == 'RQP')) {
				$return_data['itembarcode'] = implode(",", $scannedItemBarCodes);
				/* if($validated_reasoncode != ""){
					$validated_reason = get_reason_displayname($validated_reasoncode);
					$return_data['validated_reason'] = $validated_reason;
					$user_comment = "Return reason validated to $validated_reason ".$user_comment;
					if($validated_reasoncode == 'WIR'){
						$status = ItemApiClient::update_not_found_items($scannedItemBarCodes, $login);
						$weblog->info($status['message']);
						if($status['success']){
							$return_data['status'] = 'RRJ';
						}
					}
				} */
			}
			
			if($status == 'RQF') {
				$minReturnAmount = FeatureGateKeyValuePairs::getFeatureGateValueForKey('returnMinDeliveryAmount');
				if($return_details['item_total_amount'] < $minReturnAmount){
					add_returns_logs_usercomment($returnid, "moving to RQSP automatically as refund amount is less than $minReturnAmount", $login, $title,$from_status,$status);
					$from_status = $status;
					$status = 'RQSP';
				}else{
					$return_data['qafail_reason'] = $user_comment;
				}
			}
			if($status == 'RQP' || $status == 'RQSP' || $status == 'RQCP' || ($status == 'RPU' && $courier_service == 'ML') || $status == 'RADC' || $status == 'CPDC') {
				db_query("update xcart_returns set is_refunded = 1 where returnid = $returnid and is_refunded = 0", false);
				$noOfUpdatedRows = db_affected_rows();
				if($noOfUpdatedRows > 0){
					$refund_params = issue_refund_for_return($return_details, $_POST['login']);
					$weblog->info("return_request.php: Refund Issued Successfully");
					$return_data['is_refunded'] = true;
					$return_data['refundeddate'] = time();
					$return_data['couponcode'] = $refund_params['coupon'];
					$return_data['refundamount'] = $refund_params['refund'];
					$args['AMOUNT'] = $return_data['refundamount'];
					$args['COUPON'] = $return_data['couponcode'];
				}
			}
			
			if($status == 'RQSF' || $status == 'CFDC'){
				$return_data['qafail_reason'] = $user_comment;
			}
			if($status == 'RRD') {
				//send_return_cancellation_mail($return_details, $user_comment);
				$return_data['cancel_reason'] = $user_comment;
			}
			if($status == 'RRC') {
				$return_data['warehouseid'] = $warehouseid;
				$order_details['item_status'] = 'RT';
				$args['RECEIVED_DATE'] = date('d-m-Y H:i:s', $return_data['receiveddate']);
			}
			if($status == 'RRS') {
				$order = func_query_first("select status from xcart_orders where orderid = $orderid");
				if($order['status'] == 'C' || $order['status'] == 'DL'){
					$order_details['item_status'] = 'D';
				}else{
					$order_details['item_status'] = 'S';
				}
			}
			if($status == 'RSD') {
				$args['REDELIVER_DATE'] = date('d-m-Y H:i:s', $return_data['redelivereddate']);
				$args['RESHIP_COURIER'] = $return_details['reship_courier'];
				$args['RESHIP_TRACKING]'] = $return_details['reship_tracking_no'];
			}
			if($status == 'RRQS') {
				$return_data['return_mode'] = 'self';
				$return_data['courier_service'] = '';
				$return_data['tracking_no'] = '';
				$selfDeliveryCredit = WidgetKeyValuePairs::getWidgetValueForKey('returnSelfDeliveryCredit');
				$return_data['delivery_credit'] = $selfDeliveryCredit;
			}
			if($status == 'RRQP') {
				$return_data['return_mode'] = 'pickup';
				$return_data['courier_service'] = '';
				$return_data['tracking_no'] = '';
				$return_data['delivery_credit'] = 0;
			}
			if(!empty($order_details)){
				func_array2update("order_details", $order_details, "itemid=".$itemid);
				EventCreationManager::pushItemUpdateEvent($itemid, $order_details['item_status'], "Changing item_status to ".$order_details['item_status']." from returns page".$user_comment, time(), $login);
			}
			func_array2update("returns", $return_data, "returnid=".$returnid);

			\EventCreationManager::pushRMSSyncEvent($returnid);

			$weblog->debug(" from status $from_status  to status $status");
			if($qcReason !== false && !empty($qcReason)) {
				$user_comment .= "<br>Reason: $qcReason[rejectReason]<br>Description: $qcReason[rejectReasonDescription]<br>Quality: $qcReason[quality]";
			}
			add_returns_logs_usercomment($returnid, $user_comment, $login, $title,$from_status,$status);

			return array('status' => true, 'source_warehouseid'=>$return_details['source_warehouseid'], 'source_wh_name'=>$return_details['source_wh_name']);
		}
		
		return array('status' => false,'reason'=>$errorMsg);
	}
	
	public function processReturnPickupFailure($returnid,$comment, $permanentReject = false){
		$return_details_result = $this->getReturnDetails($returnid);
		if(!$return_details_result['status']){
			return $return_details_result;
		}
		$return_details = $return_details_result['return_details'];
		if($return_details['status'] == 'RRD'){
			add_returns_logs_usercomment($returnid, "Psuedo success response sent to lms", 'LMS_API', "request for marking pickup failed has been responded as success as status is already in RRD");
			return array('status'=>true,'reason'=>"Return is in ".getReturnStatusByCode('RRD')." status",'code'=>'ALREADY_IN_RRD');
		}
		if($permanentReject){
			$this->changeReturnsStatus(trim($returnid), 'RPF', '', '', '', '', 'LMS_API','Marking return as failed from LMS'.$comment);
			return $this->changeReturnsStatus(trim($returnid), 'RJUP', '', '', '', '', 'LMS_API','Marking return as failed from LMS permanently'.$comment);
		}
		if($return_details['status'] == 'RPI'){
			return $this->changeReturnsStatus(trim($returnid), 'RPF', '', '', '', '', 'LMS_API','Marking return as failed from LMS'.$comment);
		}
		if($return_details['status'] == 'RPI2'){
			return $this->changeReturnsStatus(trim($returnid), 'RPF2', '', '', '', '', 'LMS_API','Marking return as failed from LMS'.$comment);
		}
		if($return_details['status'] == 'RPI3'){
			return $this->changeReturnsStatus(trim($returnid), 'RPF3', '', '', '', '', 'LMS_API','Marking return as failed from LMS'.$comment);
		}
        	return array('status'=>false,'reason'=>'Return is not in a valid state for pick up failure');      
	}
	
	public function validateItemBarCodes($barcodes, $orderid, $itemid){
		if($barcodes == ''){
			return array('status'=>false,'reason'=>'No barcodes entered','code'=>'no_bar_code');
		}
		$itembarcodes = explode(",", $barcodes);
		return validate_return_itembarcode($itembarcodes, $orderid, $itemid);
		
	}
	
	public function sendPickupStatusUpdateMail($returnId,$pickupStatus,$pickupTime,$reason = '', $deliveryStaff=false){
		if($pickupStatus == 'REJECT'){
			return array('returnid'=>$returnId,'result' => true); 
		}
		global $weblog;
		$weblog->debug("returnid $returnId status $pickupStatus");
		if(!isInteger($returnId)){
			return array('returnid'=>$returnId,'result' => false,'reason'=>"$returnId is invalid");
		}
		
		$return_details_result = $this->getReturnDetails($returnId);
		if(!$return_details_result['status']){
			return $return_details_result;
		}
		$return_details = $return_details_result['return_details'];
		
		if($pickupStatus == 'SUCCESS'){
			return;
		}else if($pickupStatus == 'FAILURE'){
			if( strpos($reason,'NOT_AVAILABLE')  !== false ){
				$template = 'failed_pickup';
				return array('returnid'=>$returnId,'result' => true);	
			}else{
				return array('returnid'=>$returnId,'result' => true);
			}
		}else if ($pickupStatus == 'OUT_FOR_PICKUP'){
			if(strtoupper($return_details['refund_mode']) == "OR" 
					|| strtoupper($return_details['refund_mode']) == "NEFT"){
				$template = 'out_for_pickup_with_NEFT_OR';
			} else {
				$template = 'out_for_pickup';
			}			
			
			$msg = "Hi $return_details[name], We have scheduled a pickup for your Myntra return with Return ID: $returnId. Please keep the packet ready, but do not seal it. Our agent $deliveryStaff[firstName](Ph. $deliveryStaff[mobile]) will do the pickup by the end of the day.";
			func_send_sms_cellnext($return_details['mobile'], $msg);
			
		}
		$delivery_address = $return_details['address']."<br>".$return_details['city']."<br>".$return_details['state'].", ".$return_details['country']." - ".$return_details['zipcode'];
		$return_item_info = create_return_details_section($return_details, 0, 0, 0, false);
		
		$subjectargs = array('RETURN_ID' => $returnId);
		$args = array(  
			"USER"        		=> $return_details['name'],
			"ORDER_ID"          => $returnId,
			"RETURN_DETAILS"		=> $return_item_info,
			"PICKUP_DATE"		=> $pickupTime,
			"DECLARATION"       => "This mail is intended only for ".$return_details['email'],
			"PICKUP_ADDRESS" => $delivery_address,
			"PHONE_NUMBER" => $return_details['mobile'] 
		);
	
		$customKeywords = array_merge($subjectargs, $args);
		$mailDetails = array(
			"template" => $template,
			"to" => $return_details['email'],
			"bcc" => "myntramailarchive@gmail.com",
			"header" => 'header',
			"footer" => 'footer',
			"mail_type" => \MailType::CRITICAL_TXN
		);
		$multiPartymailer = new \MultiProviderMailer($mailDetails, $customKeywords);
		$flag = $multiPartymailer->sendMail();
		if(!$flag){
			return array('returnid'=>$returnId,'result' => false,'reason'=>"could not send mail");
		}		
	}
}

?>

