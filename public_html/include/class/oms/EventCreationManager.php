<?php

include_once("$xcart_dir/include/class/oms/OrderModelTransformer.php");
include_once("$xcart_dir/modules/amqpclients/producer/AMQPMessageProducer.php");
include_once("$xcart_dir/include/class/class.feature_gate.keyvaluepair.php");


class EventCreationManager{
	
	public static function pushOrderConfirmationEvent($orderid, $gift_card_amount=false) {
		global $server_name, $login;
		$pushEventsEnabled = FeatureGateKeyValuePairs::getBoolean('oms.orderevents.push.enabled', false);
		if($pushEventsEnabled) {
            if($gift_card_amount) {
                $confirmMessage = array('orderId' => $orderid, 'responseServer' => $server_name, 'status' => 'Q', 'comment' => 'Payment Successful. Queue Order.', 'giftCardAmount' => $gift_card_amount, 'userId' => $login);
            } else {
                $confirmMessage = array('orderId' => $orderid, 'responseServer' => $server_name, 'status' => 'Q', 'comment' => 'Payment Successful. Queue Order.', 'userId' => $login);
            }
            $client = AMQPMessageProducer::getInstance();
			return $client->sendMessage('preOrderConfirmedEvents', $confirmMessage, 'orderUpdateEntry','xml',true);
		}
	}

    public static function pushOrderProcessEvent($orderid, $order_data=array()) {
		global $login, $server_name;
		$pushEventsEnabled = FeatureGateKeyValuePairs::getBoolean('oms.orderevents.push.enabled', false);
		if($pushEventsEnabled) {
			$confirmMessage = array('orderId'=> $orderid, 'responseServer' => $server_name, 'status' => 'Q', 'comment'=>'Payment Successful. Queue Order.', 'userId' => $login);
            $confirmMessage = array_merge($confirmMessage, $order_data);
			$client = AMQPMessageProducer::getInstance();
            return $client->sendMessage('orderConfirmedEvents', $confirmMessage, 'orderUpdateEntry','xml',true);
		}
	}
    
    public static function pushReadyForReleaseEvent($orderid, $userId) {
		global $login, $server_name;
		$pushEventsEnabled = FeatureGateKeyValuePairs::getBoolean('oms.orderevents.push.enabled', false);
		if($pushEventsEnabled) {
			$confirmMessage = array('releaseId'=> $orderid, 'responseServer' => $server_name, 'status' => 'RFR', 'comment'=>'marking release as RFR from portal', 'userId' => $userId);
			$client = AMQPMessageProducer::getInstance();
            return $client->sendMessage('orderEvents', $confirmMessage, 'releaseUpdateEntry','xml',true);
		}
	}

    public static function pushReadyForReleaseEventWithQueuedDate($orderid, $queuedDate, $userId) {
        global $login, $server_name;
        $pushEventsEnabled = FeatureGateKeyValuePairs::getBoolean('oms.orderevents.push.enabled', false);
        if($pushEventsEnabled) {
            $confirmMessage = array('releaseId'=> $orderid, 'responseServer' => $server_name, 'status' => 'RFR', 'comment'=>'marking release as RFR', 'updatedOn' => date('Y-m-d\TH:i:s', $queuedDate), 'userId' => $userId);
            $client = AMQPMessageProducer::getInstance();
            return $client->sendMessage('orderEvents', $confirmMessage, 'releaseUpdateEntry','xml',true);
        }
    }
	
	public static function pushCompleteOrderEvent($orderid){
		$pushEventsEnabled = FeatureGateKeyValuePairs::getBoolean('oms.orderevents.push.enabled', false);
		if($pushEventsEnabled) {
			$newOrderModel = OrderModelTransformer::transformToNewModel($orderid);
			if($newOrderModel && !empty($newOrderModel)) {
				$client = AMQPMessageProducer::getInstance();
				return $client->sendMessage('orderCreationEvents', array('orderData' => $newOrderModel), 'orderCreationMessage','xml',true);
			} else {
				global $portalapilog;
				$portalapilog->info("OrderModelTransformer error - No details found for order - $orderid");
			}
		}
	}	
	
	public static function pushReleaseStatusUpdateEvent($orderid,$status,$comment,$update_time,$updated_by='SYSTEM', $extraData=array(), $onhold_reason_id = false){
		$pushEventsEnabled = FeatureGateKeyValuePairs::getBoolean('oms.orderevents.push.enabled', false);
		if($pushEventsEnabled) {
			$extraData['releaseId'] = $orderid;
			$extraData['comment'] = $comment;
			$extraData['updatedOn'] = date('Y-m-d\TH:i:s', $update_time);
			$extraData['userId'] = $updated_by;
			if($status == 'OH') {
				$extraData['onHold'] = 1;
				if($onhold_reason_id) {
					$extraData['onHoldReasonId'] = $onhold_reason_id;
				}
			} else {
                if($status){
                    $extraData['status'] = $status;
                }
				$extraData['onHold'] = 0;
			}
			
			$client = AMQPMessageProducer::getInstance();
			
			if($status == 'SH' || $status == 'DL') {
				$client->sendMessage('upsnotification', $extraData, 'releaseUpdateEntry', 'xml', true);
			}
			
			return $client->sendMessage('orderEvents', $extraData, 'releaseUpdateEntry','xml',true);
		}
	}
	
	public static function pushReleaseUpdateEvent($orderid, $updated_by='SYSTEM', $courierService=false, $trackingNo=false, 
					$receiverName=false, $address=false, $city=false, $locality=false, $state=false, $zipcode=false, 
							$country=false, $mobile=false, $email=false){
		$pushEventsEnabled = FeatureGateKeyValuePairs::getBoolean('oms.orderevents.push.enabled', false);
		if($pushEventsEnabled) {
			$data = array();
			$data['releaseId'] = $orderid;
			$data['userId'] = $updated_by;
			if($receiverName !== false)
				$data['receiverName'] = $receiverName;
			if($address !== false)
				$data['address'] = $address;
			if($city !== false)
				$data['city'] = $city;
			if($locality !== false)
				$data['locality'] = $locality;
			if($state !== false)
				$data['state'] = $state;
			if($zipcode !== false)
				$data['zipcode']=$zipcode;
			if($country !== false)
				$data['country'] = $country;
			if($mobile !== false)
				$data['mobile'] = $mobile;
			if($email !== false)
				$data['email'] = $email;
			if($courierService !== false) { 
				if(empty($courierService) || $courierService === '0') 
					$data['courierService'] = '';
				else
					$data['courierService'] = $courierService;
			}
			if($trackingNo !== false)
				$data['trackingNo'] = $trackingNo;
			
			$client = AMQPMessageProducer::getInstance();
			return $client->sendMessage('orderEvents', $data, 'releaseUpdateEntry','xml',true);
		}
	}
    
	public static function pushFedexReleaseEvent($order_data, $updated_by='SYSTEM'){
		$pushEventsEnabled = FeatureGateKeyValuePairs::getBoolean('oms.orderevents.push.enabled', false);
		if($pushEventsEnabled) {
			$data = array();
			$data['releaseId'] = $order_data['orderid'];
			$data['userId'] = $updated_by;
            
            $data['zipcode']=$order_data['s_zipcode'];
            $data['cod']=$order_data['payment_method'] == 'cod' ? 1 : 0;
            $data['warehouseId'] = $order_data['warehouseid'];
            $data['courierService'] = $order_data['courier_service'];
            $data['receiverName'] = $order_data['receiverName'];
            $data['address'] = $order_data['s_address'];
            $data['locality'] = $order_data['s_locality'];
            $data['city'] = $order_data['s_city'];
            $data['state'] = $order_data['s_state'];
            $data['country'] = $order_data['s_country'];
            $data['mobile'] = $order_data['mobile'];
            $data['email'] = $order_data['s_email'];
            $data['codAmount'] = $order_data['total'];
            $data['mrpTotal'] = $order_data['subtotal'];

            $item_details_query = "SELECT typename FROM mk_catalogue_classification mcc JOIN mk_style_properties msp ON mcc.id=msp.global_attr_article_type JOIN xcart_order_details xod ON msp.style_id=xod.productid where xod.orderid=".$order_data['orderid'];
            $item_details = func_query($item_details_query);
            $item_description = "";
            if($item_details && count($item_details)>0) {
                foreach ($item_details as $item_detail) {
                    $item_description .= $item_detail['typename'] . ", ";
                }
            }

            $data['itemDescription'] = $item_description;
            if (!$order_data['shipping_method'] || $order_data['shipping_method'] === '') {
                $data['shippingMethod'] = 'NORMAL';
            } else {
                $data['shippingMethod'] = $order_data['shipping_method'];
            }
			$client = AMQPMessageProducer::getInstance();
			return $client->sendMessage('assignTrackingNoEvents', $data, 'releaseUpdateEntry','xml',true);
		}
	}
	
	public static function pushOrderUpdateEvent($orderid, $status, $comment, $update_time, $updated_by='SYSTEM', $onhold_reason_id=false){
		$pushEventsEnabled = FeatureGateKeyValuePairs::getBoolean('oms.orderevents.push.enabled', false);
		if($pushEventsEnabled) {
			$data = array();
			$data['orderId'] = $orderid;
			if($cancellation_reason_id) {
				$data['cancellationReasonId'] = $cancellation_reason_id;
			}
			$data['status'] = $status;
			if($status == 'OH') {
				if($onhold_reason_id) {
					$data['onHoldReasonId'] = $onhold_reason_id;
				}
			}
			$data['comment'] = $comment;
			$data['updatedOn'] = date('Y-m-d\TH:i:s', $update_time);
			$data['userId'] = $updated_by;
			
			$client = AMQPMessageProducer::getInstance();
			return $client->sendMessage('orderEvents', $data, 'orderUpdateEntry','xml',true);
		}
	}
	
        public static function pushItemPackagedUpdateEvent($itemId, $status, $packagingType,$packagingStatus,$comment, $update_time,$updated_by='SYSTEM'){
                $pushEventsEnabled = FeatureGateKeyValuePairs::getBoolean('oms.orderevents.push.enabled', false);
		if($pushEventsEnabled) {
			$data = array();
			$data['orderLineId'] = $itemId;
			$data['status'] = $status;
                        $data['packagingType'] = $packagingType;
                        $data['packagingStatus'] = $packagingStatus;
			$data['comment'] = $comment;
			$data['updatedOn'] = date('Y-m-d\TH:i:s', $update_time);
			$data['userId'] = $updated_by;
			
			$client = AMQPMessageProducer::getInstance();
			return $client->sendMessage('orderEvents', $data, 'orderLineUpdateEntry','xml',true);
		}

            
        }
        
	public static function pushItemUpdateEvent($itemId, $status, $comment, $update_time,$updated_by='SYSTEM'){
		$pushEventsEnabled = FeatureGateKeyValuePairs::getBoolean('oms.orderevents.push.enabled', false);
		if($pushEventsEnabled) {
			$data = array();
			$data['orderLineId'] = $itemId;
			$data['status'] = $status;
			$data['comment'] = $comment;
			$data['updatedOn'] = date('Y-m-d\TH:i:s', $update_time);
			$data['userId'] = $updated_by;
			
			$client = AMQPMessageProducer::getInstance();
			return $client->sendMessage('orderEvents', $data, 'orderLineUpdateEntry','xml',true);
		}
	}
	
	
	public static function pushInventoryUpdateToAtpEvent($orderId,$itemIds = null,$operation = 'BLOCK'){
		
		$myntraOwnedWarehouses = FeatureGateKeyValuePairs::getFeatureGateValueForKey('wms.myntra.warehouses');
		$myntraOwnedWarehouses = explode(",", $myntraOwnedWarehouses);
		
		$data = array();
		if($itemIds !=null){
			if(!is_array($itemIds)){
				$itemIds = array($itemIds);
			}
			$items = func_query("select od.seller_id,od.supply_type,sosm.sku_id,od.amount,o.warehouseid from xcart_orders o, xcart_order_details od, mk_order_item_option_quantity oq, mk_styles_options_skus_mapping sosm where o.orderid = od.orderid and od.itemid = oq.itemid and oq.optionid = sosm.option_id and od.product_style = sosm.style_id and od.itemid in (".implode(",",$itemIds).") group by od.seller_id,od.supply_type,sosm.sku_id");
		}else{
			$items = func_query("select od.seller_id,od.supply_type,sosm.sku_id,od.amount,o.warehouseid from xcart_orders o, xcart_order_details od, mk_order_item_option_quantity oq, mk_styles_options_skus_mapping sosm where o.orderid = od.orderid and od.itemid = oq.itemid and oq.optionid = sosm.option_id and od.product_style = sosm.style_id and od.orderid = $orderId group by od.seller_id,od.supply_type,sosm.sku_id");
		}
		if($items) {
			$inventoryResponse = array();
			$data = array();
			foreach($items as $key => $item){
				if($operation == 'BLOCK' || ($operation == 'UNBLOCK' && in_array($item['warehouseid'], $myntraOwnedWarehouses))) {
					$inventory['storeId'] = 1;
					$inventory['sellerId'] = $item['seller_id'];
					$inventory['skuId'] = $item['sku_id'];
					$inventory['blockedOrderCount'] = $item['amount'];
					$inventory['supplyType'] = $item['supply_type'];
					$data[] = array('inventory' => $inventory);
				}
			}	
		
			if(!empty($data)) {	
			    $inventoryResponse['data'] = $data;
			    $inventoryResponse['storeId'] = 1;
			    $inventoryResponse['operation'] = $operation;
							
			    $client = AMQPMessageProducer::getInstance();
			    return $client->sendMessage('reserveInventoryQueue', $inventoryResponse, 'inventoryAllocationRequest','xml',true,'oms',array('seller_id' => 1));
                        }
		}
		return true;
	}
	
	public static function pushOrderAssignWarehouseEvent($orderid, $user) {
		global $server_name;
		$pushEventsEnabled = FeatureGateKeyValuePairs::getBoolean('oms.orderevents.push.enabled', false);
		if($pushEventsEnabled) {
			$confirmMessage = array('orderId'=> $orderid, 'responseServer' => $server_name, 'status' => 'Q', 'comment'=>'Queue Order from OH', 'userId' => $login);
			$client = AMQPMessageProducer::getInstance();
			return $client->sendMessage('postPaymentProcessingQueue', $confirmMessage, 'orderUpdateEntry','xml',true);
             //           return $client->sendMessage('randomTestQueue', $confirmMessage, 'orderUpdateEntry','xml',true);
		}
	}
	
	public static function pushStyleRefreshEvent($skuIds) {
		if(empty($skuIds))
			return;
		if(!is_array($skuIds))
			$skuIds = array($skuIds);
		
		$request = array();
		$request['handler'] = 'Skus';
		$request['data'] = array('action' => 'reindex', 'userId' => 'System');
		foreach($skuIds as $skuId) 
			$request['data']['skus'][] = array('sku' => array('id' => $skuId));
		
		$client = AMQPMessageProducer::getInstance();
		return $client->sendMessage('skuChangeQueue', $request, 'PortalApiRequest','xml',true);
		
	}
	
	public static function pushWMSReleaseEvent($wmsReleasesRequest, $action) {
		global $server_name, $login;
		$client = AMQPMessageProducer::getInstance();
		if($action == '')
			return $client->sendMessage('createReleaseWMS', $wmsReleasesRequest, 'orderReleaseResponse', 'xml', true);
		else
			return $client->sendMessage('updateReleaseWMS', $wmsReleasesRequest, 'orderReleaseResponse', 'xml', true);
	}

	public static function pushRMSUpdateEvent($returnId, $status)
	{
		global $login, $server_name;
		$pushEventsEnabled = FeatureGateKeyValuePairs::getBoolean('rms.updateevents.push.enabled', false);
		if ($pushEventsEnabled) {
			$data = array('returnId' => $returnId, 'toState' => $status, 'userComment' => 'pushing update from portal', 'createdBy' => $login);
			$client = AMQPMessageProducer::getInstance();
			$client->sendMessage('portalReturnUpdateEvents', $data, '', 'json', true, 'rms');
		}
	}

	public static function pushRMSSyncEvent($returnId)
	{
		global $login, $server_name;
		$pushEventsEnabled = FeatureGateKeyValuePairs::getBoolean('rms.syncevents.push.enabled', false);
		if ($pushEventsEnabled) {
			$data = array('returnid' => $returnId);
			$client = AMQPMessageProducer::getInstance();
			$client->sendMessage('portalReturnsSync', $data, '', 'json', true, 'rms');
		}
	}

	public static function pushRMSOldDataSyncEvent($returnId)
	{
		global $login, $server_name;
		$pushEventsEnabled = FeatureGateKeyValuePairs::getBoolean('rms.syncevents.push.enabled', false);
		if ($pushEventsEnabled) {
			$data = array('returnid' => $returnId);
			$client = AMQPMessageProducer::getInstance();
			$client->sendMessage('portalReturnsOldSync', $data, '', 'json', true, 'rms');
		}
	}

	public static function pushRMSDeclineEvent($returnId)
	{
		global $login, $server_name;
		$pushEventsEnabled = FeatureGateKeyValuePairs::getBoolean('rms.declineevents.push.enabled', false);
		if ($pushEventsEnabled) {
			$data = array('returnId' => $returnId, 'toState' => 'RRD', 'userComment' => 'declining returns from portal', 'createdBy' => $login);
			$client = AMQPMessageProducer::getInstance();
			$client->sendMessage('portalReturnUpdateEvents', $data, '', 'json', true, 'rms');
		}
	}
	
}
?>
