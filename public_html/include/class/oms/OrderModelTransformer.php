<?php

class OrderModelTransformer{
	
	public static function transformToNewModel($orderid){
	    	$releaseSql = "select xo.* from xcart_orders xo, xcart_orders xo1 where xo.group_id = xo1.group_id and xo1.orderid = ".$orderid;
            $releases = func_query($releaseSql);
            if($releases) {
            	$order = array();
	            $order['id'] = $releases[0]['group_id'];
	            $order['login'] = $releases[0]['login'];
	            //$order['storeId'] = $releases[0]['source_id'];
	            $order['userContactNo'] = $releases[0]['issues_contact_number'];
	            $order['customerName'] = $releases[0]['firstname'];
	            $order['paymentMethod'] = $releases[0]['payment_method'];
	            $order['channel'] = $releases[0]['channel'];
	            $order['giftOrder'] = $releases[0]['gift_status']=='Y'?1:0;
	            $order['notes'] = $releases[0]['notes'];
	            $order['couponCode'] = $releases[0]['coupon'];
	            $order['cashCouponCode'] = $releases[0]['cash_coupon_code'];
	            $order['requestServer'] = $releases[0]['request_server'];
	            $order['responseServer'] = $releases[0]['response_server'];
                     if($releases[0]['on_hold_reason_id_fk'] != 10 && $releases[0]['on_hold_reason_id_fk'] != 27){
                        $order['onHoldReasonId'] = $releases[0]['on_hold_reason_id_fk'];
                     }
	            $order['orderProcessingFlowMode'] = 'OMS';
	            $order['queuedOn'] = date('Y-m-d',$releases[0]['queueddate'])."T".date('H:i:s',$releases[0]['queueddate'])."+05:30";
				if(!empty($releases[0]['ordertype']))
					$order['orderType'] = $releases[0]['ordertype'];
		    	else
					$order['orderType'] = 'on';
	            
	            $order['mrpTotal'] = 0.00;
	            $order['discount'] = 0.00;
	            $order['cartDiscount'] = 0.00;
	            $order['couponDiscount'] = 0.00;
	            $order['cashRedeemed'] = 0.00;
	            $order['pgDiscount'] = 0.00;
	            $order['finalAmount'] = 0.00;
	            $order['shippingCharge'] = 0.00;
	            $order['giftCharge'] = 0.00;
	            $order['codCharge'] = 0.00;
	            $order['emiCharge'] = 0.00;
	            $order['taxAmount'] = 0.00;
	            $order['cashbackOffered'] = 0.00;
                $order['loyaltyPointsUsed'] = 0.00;
                $order['loyaltyPointsAwarded'] = 0.00;
                //$order['giftCardAmount'] = 0.00;
	            
	            $billing_address = array();
	            $billing_address['billingFirstName'] = $releases[0]['b_firstname'];
	            $billing_address['billingLastName'] = $releases[0]['b_lastname'];
	            $billing_address['billingAddress'] = $releases[0]['b_address'];
	            $billing_address['billingCity'] = $releases[0]['b_city'];
	            $billing_address['billingCounty'] = $releases[0]['b_county'];
	            $billing_address['billingState'] = $releases[0]['b_state'];
	            $billing_address['billingZipCode'] = $releases[0]['b_zipcode'];
	            $billing_address['billingMobile'] = $releases[0]['b_mobile'];
	            $billing_address['billingEmail'] = $releases[0]['b_email'];
	            $order['billingAddress'] = $billing_address;
	            
	            $order['createdOn'] = date('Y-m-d',$releases[0]['date'])."T".date('H:i:s',$releases[0]['date'])."+05:30";

	            if($releases[0]['channel']=='telesales'){
	            	$cclogin=func_query_first_cell("select cclogin from telesales_logs where orderid=$orderid");
	            	$order['createdBy'] = $cclogin;
	            } else 
	                $order['createdBy'] = $order['login'];
	            
	            $inprogressReleases = 0;
	            $processedReleases = 0;
	            $completedReleases = 0;
	            $cancelledReleases = 0;
	            
	            $order['onHold'] = 0;
	            $orderOnHold = false;
	            if(count($releases) == 1) {
	            	if($releases[0]['status'] == 'OH') {
	            		if($releases[0]['warehouseid'] > 0)
	            			$order['onHold'] = 0;
	            		else {
	            			$order['onHold'] = 1;
	            			$orderOnHold = true;
	            		}
                                    if($releases[0]['on_hold_reason_id_fk'] != 10 && $releases[0]['on_hold_reason_id_fk'] != 27){
                                        $order['onHoldReasonId'] = $releases[0]['on_hold_reason_id_fk'];
                                    }
	            	}
	            }
	            
	            $orderReleases = array();
	            foreach($releases as $release) {
	            	$orderRelease = array();
	            	
	                $orderRelease['userContactNo'] = $release['issues_contact_number'];
	                $releaseonHold = 0;
	            	if($release['status'] == 'OH') {
	            		if(!$orderOnHold) {
	            			$releaseonHold = 1;
		            		if($release['warehouseid'] > 0)
		            			$orderRelease['status'] = 'WP';
		            		else
		            			$orderRelease['status'] = 'Q';
	            		} else {
	            			$orderRelease['status'] = 'Q';
	            			$releaseonHold = 0;
	            		}
	            		$orderRelease['onHoldReasonId'] = $release['on_hold_reason_id_fk'];
	            	} else
	            		$orderRelease['status'] = $release['status'];
	            	
	            	$orderRelease['onHold'] = $releaseonHold;
	            	/*
	            	if($orderRelease['status'] == 'Q' || $orderRelease['status'] == 'WP') {
	            		$inprogressReleases += 1;
	            	}
	            	if($orderRelease['status'] == 'PK' || $orderRelease['status'] == 'SH' || $orderRelease['status'] == 'L') {
	            		$processedReleases += 1;
	            	}
	            	if($orderRelease['status'] == 'DL' || $orderRelease['status'] == 'C') {
	            		$completedReleases += 1;
	            	} */
	            	if($orderRelease['status'] == 'F') {
	            		$cancelledReleases += 1;
	            	}
	            	
	            	$orderRelease['id'] = $release['orderid'];
	            	$orderRelease['orderId'] = $release['group_id'];
	            	//$orderRelease['storeId'] = $release['source_id'];
	            	$orderRelease['login'] = $release['login'];
	            	$orderRelease['invoiceId'] = $release['invoiceid'];
            		
            		$orderRelease['refunded'] = $release['is_refunded'];
            		// If in portal release is cancelled or lost then force set is_refunded to true
            		if($release['is_refunded'] == 0 && ($release['status'] == 'F' || $release['status'] == 'L'))
            			$orderRelease['refunded'] = 1;
            			
            		$orderRelease['shippingMethod'] = $release['shipping_method'];
	            	$orderRelease['paymentMethod'] = $release['payment_method'];
	            	$orderRelease['receiverName'] = trim($release['s_firstname']." ".$release['s_lastname']);
	            	$orderRelease['address'] = $release['s_address'];
	            	$orderRelease['locality'] = $release['s_locality'];
	            	$orderRelease['city'] = $release['s_city'];
	            	$orderRelease['state'] = $release['s_state'];
	            	$orderRelease['country'] = $release['s_country'];
	            	$orderRelease['zipcode'] = trim($release['s_zipcode']);
	            	$orderRelease['mobile'] = $release['mobile'];
                        
                        if(!empty($release['s_email'])){
                            $orderRelease['email'] = $release['s_email'];
                        }else if(!empty($release['customer'])){
                            $orderRelease['email'] = $release['customer'];
                        }else if (!empty($release['email'])){
                            $orderRelease['email'] = $release['email'];
                        }else if (!empty($release['b_email'])){
                            $orderRelease['email'] = $release['b_email'];                            
                        }else if (!empty($release['login'])){
                            $orderRelease['email'] = $release['login'];                            
                        }
	            	if(!empty($release['courier_service']) && $release['courier_service'] !== '0')
	            		$orderRelease['courierCode'] = $release['courier_service'];
	            	else
	            		$orderRelease['courierCode'] = '';
	            	
	            	$orderRelease['trackingNo'] = $release['tracking'];
                    if($release['warehouseid']!=0)
					{
    	                $orderRelease['warehouseId'] = $release['warehouseid'];
                   	}
	            	$orderRelease['codPaymentStatus'] = $release['cod_pay_status'];
	            	$orderRelease['chequeNo'] = $release['cheque_no'];
	            	$orderRelease['cancellationReasonId'] = $release['cancellation_code'];
	            	$orderRelease['loyaltyPointsAwarded'] = $release['loyalty_points_awarded'];
                    $orderRelease['loyaltyPointsUsed'] = $release['loyalty_points_used'];
                $orderRelease['loyaltyConversionFactor'] = $release['loyalty_points_conversion_factor'];
                        
	            	if(!empty($orderRelease['splitdate']))
	            		$orderRelease['createdOn'] = date('Y-m-d',$release['splitdate'])."T".date('H:i:s',$release['splitdate'])."+05:30";
	            	else
	            		$orderRelease['createdOn'] = date('Y-m-d',$release['date'])."T".date('H:i:s',$release['date'])."+05:30";

	            	$orderRelease['queuedOn'] =  date('Y-m-d',$release['queueddate'])."T".date('H:i:s',$release['queueddate'])."+05:30";
	            	
	            	if(!empty($release['packeddate']))	
	            		$orderRelease['packedOn'] = date('Y-m-d',$release['packeddate'])."T".date('H:i:s',$release['packeddate'])."+05:30";
	            	if(!empty($release['shippeddate']))
	            		$orderRelease['shippedOn'] = date('Y-m-d',$release['shippeddate'])."T".date('H:i:s',$release['shippeddate'])."+05:30";
	            	if(!empty($release['delivereddate']))
	            		$orderRelease['deliveredOn'] = date('Y-m-d',$release['delivereddate'])."T".date('H:i:s',$release['delivereddate'])."+05:30";
	            	if(!empty($release['completeddate']))	
	            		$orderRelease['completedOn'] = date('Y-m-d',$release['completeddate'])."T".date('H:i:s',$release['completeddate'])."+05:30";
	            	if(!empty($release['canceltime']))	
	            		$orderRelease['cancelledOn'] = date('Y-m-d',$release['canceltime'])."T".date('H:i:s',$release['canceltime'])."+05:30";
	
	            	$orderRelease['mrpTotal'] = $release['subtotal'];
					$orderRelease['discount'] = $release['discount'];
		            $orderRelease['cartDiscount'] = $release['cart_discount'];
		            $orderRelease['couponDiscount'] = $release['coupon_discount'];
		            $orderRelease['cashRedeemed'] = $release['cash_redeemed'];
		            $orderRelease['pgDiscount'] = $release['pg_discount'];
		            $orderRelease['finalAmount'] = $release['total'] + $release['cod'];
		            $orderRelease['shippingCharge'] = $release['shipping_cost'];
		            $orderRelease['giftCharge'] = $release['gift_charges'];
		            $orderRelease['codCharge'] = $release['cod'];
		            $orderRelease['emiCharge'] = $release['payment_surcharge'];
		            $orderRelease['taxAmount'] = $release['tax'];
		            $orderRelease['cashbackOffered'] = $release['cashback'];
                    $orderRelease['giftCardAmount'] = $release['gift_card_amount'];
		            
		            $salesorderid = func_query_first("select * from exchange_order_mappings where exchange_orderid = $release[orderid]");
					if($salesorderid){
						$orderRelease['exchangeReleaseId'] = $salesorderid['releaseid'];
					}
	            	
					if($release['channel']=='telesales'){
						$cclogin=func_query_first_cell("select cclogin from telesales_logs where orderid=$orderid");
						$orderRelease['createdBy'] = $cclogin;
					} else
						$orderRelease['createdBy'] = $release['login'];
					
		            if($release['status'] == 'F' && ($release['cancellation_code'] == 5 || $release['cancellation_code'] == 6 || $release['cancellation_code'] == 37 || $release['cancellation_code'] == 38)) {
		            	$orderRelease['status'] = 'RTO';
		            	unset($orderRelease['cancellationReasonId']);
		            }
		            
		            //additional info
		            $additionalInfoRows = func_query("select * from order_additional_info where order_id_fk = $release[orderid]");
		            if($additionalInfoRows) {
			            foreach ($additionalInfoRows as $row) {
			         		switch($row['key']) {
			         			case 'EXPECTED_PICKING_TIME':
                                                                $expectedPickingTime = new DateTime($row['value']);
                                                                $orderRelease['expectedPickingTime'] = $expectedPickingTime->format('Y-m-d') . "T" . $expectedPickingTime->format('H:i:s') . "+05:30";
                                                                break;
			         			case 'CUSTOMER_PROMISE_TIME':
                                                                $customerPromiseTime = new DateTime($row['value']);
                                                                $orderRelease['customerPromiseTime'] = $customerPromiseTime->format('Y-m-d') . "T" . $customerPromiseTime->format('H:i:s') . "+05:30";
                                                                break;
			         			case 'EXPECTED_CUTOFF_TIME':
			         				$expectedCutoffTime = new DateTime($row['value']);
                                                                $orderRelease['expectedCutoffTime'] = $expectedCutoffTime->format('Y-m-d') . "T" . $expectedCutoffTime->format('H:i:s') . "+05:30";
                                                                break;
                                case 'EXPECTED_PACKING_TIME':
                                        $expectedPackingTime = new DateTime($row['value']);
                                        $orderRelease['expectedPackingTime'] = $expectedPackingTime->format('Y-m-d') . "T" . $expectedPackingTime->format('H:i:s') . "+05:30";
                                        break;
                                case 'EXPECTED_QC_TIME':
                                        $expectedQCTime = new DateTime($row['value']);
                                        $orderRelease['expectedQCTime'] = $expectedQCTime->format('Y-m-d') . "T" . $expectedQCTime->format('H:i:s') . "+05:30";
                                        break;
                            }
			            }
		            }
		            
		            // order lines
		            $items = func_query("select xod.*,oq.optionid as option_id,oq.quantity,sosm.sku_id from ( ( xcart_order_details xod left outer join mk_order_item_option_quantity oq on xod.itemid = oq.itemid) left outer join mk_styles_options_skus_mapping sosm on oq.optionid = sosm.option_id and sosm.sku_id > 0) where orderid = $release[orderid] group by xod.itemid");
		            
		            $styleIds = array();
		            foreach($items as $item) {
		            	$styleIds[] = $item['product_style'];
		            }
		            
		            $styleId2TagsMap = getTagsForStyle($styleIds);
		            
		            foreach($items as $item){						
		            	$line = array();
						$line['orderId'] = $release['group_id'];
						$line['orderReleaseId'] = $release['orderid'];
						$line['id'] = $item['itemid'];
						$line['styleId'] = $item['product_style'];
						$line['optionId'] = $item['option_id'];
						if(!empty($item['sku_id']))
							$line['skuId'] = $item['sku_id'];
						
						if(empty($item['item_status']) || $item['item_status'] == 'UA')
							$line['status'] = 'A';
						else
							$line['status'] = $item['item_status'];
						
						$line['sellerId'] = $item['seller_id'];
						if(!empty($item['supply_type']))
							$line['supplyType'] = $item['supply_type'];
						else
							$line['supplyType'] = 'ON_HAND';
							
						$line['quantity'] = $item['amount'];
						$line['discountedQuantity'] = $item['discount_quantity'];
						$line['unitPrice'] = $item['price'];
						$line['discount'] = $item['discount'];
						$line['cartDiscount'] = $item['cart_discount_split_on_ratio'];
						$line['pgDiscount'] = $item['pg_discount'];
						$line['couponDiscount'] = $item['coupon_discount_product'];
						$line['cashRedeemed'] = $item['cash_redeemed'];
						$line['finalAmount'] = ($item['price']*$item['amount'])+$item['taxamount']-$item['discount']-$item['cart_discount_split_on_ratio']-$item['pg_discount']-$item['coupon_discount_product']-$item['cash_redeemed']-($item['item_loyalty_points_used']*$releases[0]['loyalty_points_conversion_factor']);
						$line['taxAmount'] = $item['taxamount'];
						$line['cashbackOffered'] = $item['cashback'];
						$line['taxRate'] = $item['tax_rate'];
						$line['discountRuleId'] = $item['discount_rule_id'];
						$line['discountRuleRevId'] = $item['discount_rule_rev_id'];
						$line['isDiscountedProduct'] = $item['is_discounted'];
						$line['isReturnableProduct'] = $item['is_returnable'];
                        if($item['is_packaged']) {
                            $line['packagingStatus'] = 'PREMIUM_PACKAGED';
                        } else {
                            $line['packagingStatus'] = 'NOT_PACKAGED';
                        }
                        $line['packagingType'] = $item['packaging_type'];
                        $govtTaxSync = \WidgetKeyValuePairs::getWidgetValueForKey('oms.taxInformationSyncEnabled');
                        if ($govtTaxSync == NULL) {
                            $govtTaxSync = false;
                        } else if ($govtTaxSync == "true") {
                            $line['govtTaxRate'] = $item['govt_tax_rate'];
                            $line['govtTaxAmount'] = $item['govt_tax_amount'];
                        }
                                                
                        $line['giftCardAmount'] = $item['gift_card_amount'];
                        $line['loyaltyPointsUsed'] = $item['item_loyalty_points_used'];
                        $line['loyaltyPointsAwarded'] = $item['item_loyalty_points_awarded'];
                        $line['loyaltyPointsCredit'] = $item['item_loyalty_points_used'] * $release['loyalty_points_conversion_factor'];
                        $line['loyaltyConversionFactor'] = $release['loyalty_points_conversion_factor'];
                                                
						$line['isCustomizable'] = $item['is_customizable'];
                        $line['customizedMessage'] = $item['extra_data'];
                        
						if($salesorderid){
							$line['exchangeLineId'] = $salesorderid['itemid'];
						}
						if(!empty($item['canceltime']))
							$line['cancelledOn'] = date('Y-m-d',$item['canceltime'])."T".date('H:i:s',$item['canceltime'])."+05:30";
						
						if(!empty($item['cancellation_code']))		
							$line['cancellationReasonId'] = $item['cancellation_code'];
		            	
		            	$line['createdBy'] = $order['login'];
		            	
		            	// product tags
		            	$line['isJewellery'] = 0;
		            	$line['isHazMat'] = 0;
		            	$line['isFragile'] = 0;
		            	$productTags = $styleId2TagsMap[$item['product_style']];
		            	foreach($productTags as $tag) {
		            		switch($tag['name']) {
		            			case 'isJewellery':
		            				$line['isJewellery'] = 1;
		            				break;
	            				case 'isHazmat':
	            					$line['isHazMat'] = 1;
	            					break;
            					case 'isFragile':
            						$line['isFragile'] = 1;
            						break;
		            		}
		            	}
		            	
		            	$orderRelease['orderLines'][] = array('orderLine' => $line);
		            }
				            
		            $order['orderReleases'][] = array('orderRelease' => $orderRelease);
	            	
                    $order['mrpTotal'] += $release['subtotal'];
                    $order['discount'] += $release['discount'];
		            $order['cartDiscount'] += $release['cart_discount'];
		            $order['couponDiscount'] += $release['coupon_discount'];
		            $order['cashRedeemed'] += $release['cash_redeemed'];
		            $order['pgDiscount'] += $release['pg_discount'];
		            $order['finalAmount'] += $release['total'] + $release['cod'];
		            $order['shippingCharge'] += $release['shipping_cost'];
		            $order['giftCharge'] += $release['gift_charges']; 
		            $order['codCharge'] += $release['cod'];
		            $order['emiCharge'] += $release['payment_surcharge'];
		            $order['taxAmount'] += $release['tax'];
		            $order['cashbackOffered'] += $release['cashback'];
                    $order['loyaltyPointsUsed'] += $release['loyalty_points_used'];
                    $order['loyaltyPointsAwarded'] += $release['loyalty_points_awarded'];
		            $order['loyaltyConversionFactor'] = $release['loyalty_points_conversion_factor'];
                    $order['loyaltyPointsCredit'] += ($release['loyalty_points_used'] * $release['loyalty_points_conversion_factor']);
                    //$order['giftCardAmount'] += $release['gift_card_amount'];
	            }
	            
	            // this is for PP & D orders.. also initial F orders
	            /* $order['orderStatus'] = $releases[0]['status'];
	            
	            if($inprogressReleases > 0) {
	            	$order['orderStatus'] = 'WP';	
	            } else if ($processedReleases > 0 ) {
		            	$order['orderStatus'] = 'P';
		        } else {
		        	if ( $completedReleases+$cancelledReleases  == count($releases)) {
		        		$order['orderStatus'] = 'C';
		        	}
		        	if($cancelledReleases == count($releases)) {
		        		$order['orderStatus'] = 'F';
		        		$full_order_cancellation = func_query_first_cell("select id from cancellation_codes where id = ".$orderReleases[0][cancellation_code] ." and cancellation_mode = 'FULL_ORDER_CANCELLATION'",true);
		        		$order['cancellationReasonId'] =  $orderReleases[0][cancellation_code];
		        		$order['cancelledOn'] = date('Y-m-d',$releases[0]['canceltime'])."T".date('H:i:s',$releases[0]['canceltime'])."+05:30";
		        	}
            	} */
            	//check for full order cancellation... first check if release no 1 is cancelled.. was it a full oder cancellation
	            if(!empty($releases[0]['cancellation_code']) && $cancelledReleases == count($releases)) {
	            	//$order['orderStatus'] = 'F';
	            	$full_order_cancellation = func_query_first_cell("select id from cancellation_codes where id = ".$releases[0]['cancellation_code'] ." and cancellation_mode = 'FULL_ORDER_CANCELLATION'",true);
	            	if($full_order_cancellation) {
	            		$order['cancellationReasonId'] =  $releases[0]['cancellation_code'];
	            		$order['cancelledOn'] = date('Y-m-d',$releases[0]['canceltime'])."T".date('H:i:s',$releases[0]['canceltime'])."+05:30";
	            	}
	            }
	            
            	$order['onHold'] = 0;
            	if($releases[0]['status'] == 'OH')
            		$order['onHold'] = 1;
	            
	            return $order;
            }      
            return false;
	}
	
	
	public static function createNewOrderReleaseModel($releaseIds){
		$releaseSql = "select * from xcart_orders where orderid in (".implode(",", $releaseIds).")";
		$releases = func_query($releaseSql);
		if($releases) {
			$orderReleases = array();
			foreach($releases as $release) {
				$orderRelease = array();
				$orderRelease['userContactNo'] = $release['issues_contact_number'];
				$releaseonHold = 0;
				if($release['status'] == 'OH') {
					$releaseonHold = 1;
					if($release['warehouseid'] > 0)
						$orderRelease['status'] = 'WP';
					else
						$orderRelease['status'] = 'Q';
					
				} else
					$orderRelease['status'] = $release['status'];
	
				$orderRelease['onHold'] = $releaseonHold;
	
				$orderRelease['id'] = $release['orderid'];
				$orderRelease['orderId'] = $release['group_id'];
				$orderRelease['storeId'] = $release['source_id'];
				$orderRelease['login'] = $release['login'];
	
				$orderRelease['refunded'] = $release['is_refunded'];
				// If in portal release is cancelled or lost then force set is_refunded to true
				if($release['is_refunded'] == 0 && ($release['status'] == 'F' || $release['status'] == 'L'))
					$orderRelease['refunded'] = 1;
				 
				$orderRelease['shippingMethod'] = $release['shipping_method'];
				$orderRelease['paymentMethod'] = $release['payment_method'];
				$orderRelease['receiverName'] = trim($release['s_firstname']." ".$release['s_lastname']);
				$orderRelease['address'] = $release['s_address'];
				$orderRelease['locality'] = $release['s_locality'];
				$orderRelease['city'] = $release['s_city'];
				$orderRelease['state'] = $release['s_state'];
				$orderRelease['country'] = $release['s_country'];
				$orderRelease['zipcode'] = trim($release['s_zipcode']);
				$orderRelease['mobile'] = $release['mobile'];
                                if (!empty($release['s_email'])) {
                                    $orderRelease['email'] = $release['s_email'];
                                } else if (!empty($release['customer'])) {
                                    $orderRelease['email'] = $release['customer'];
                                } else if (!empty($release['email'])) {
                                    $orderRelease['email'] = $release['email'];
                                } else if (!empty($release['b_email'])) {
                                    $orderRelease['email'] = $release['b_email'];
                                } else if (!empty($release['login'])) {
                                    $orderRelease['email'] = $release['login'];
                                }
                                if(!empty($release['courier_service']) && $release['courier_service'] !== '0')
					$orderRelease['courierCode'] = $release['courier_service'];
				else
					$orderRelease['courierCode'] = '';
	
				$orderRelease['trackingNo'] = $release['tracking'];
                if($release['warehouseid']!=0)
				{
                	$orderRelease['warehouseId'] = $release['warehouseid'];
                }
	
				if(!empty($orderRelease['splitdate']))
					$orderRelease['createdOn'] = date('Y-m-d',$release['splitdate'])."T".date('H:i:s',$release['splitdate'])."+05:30";
				else
					$orderRelease['createdOn'] = date('Y-m-d',$release['date'])."T".date('H:i:s',$release['date'])."+05:30";
	
				$orderRelease['queuedOn'] =  date('Y-m-d',$release['queueddate'])."T".date('H:i:s',$release['queueddate'])."+05:30";
	
				$orderRelease['mrpTotal'] = $release['subtotal'];
				$orderRelease['discount'] = $release['discount'];
				$orderRelease['cartDiscount'] = $release['cart_discount'];
				$orderRelease['couponDiscount'] = $release['coupon_discount'];
				$orderRelease['cashRedeemed'] = $release['cash_redeemed'];
				$orderRelease['pgDiscount'] = $release['pg_discount'];
				$orderRelease['finalAmount'] = $release['total'] + $release['cod'];
				$orderRelease['shippingCharge'] = $release['shipping_cost'];
				$orderRelease['giftCharge'] = $release['gift_charges'];
				$orderRelease['codCharge'] = $release['cod'];
				$orderRelease['emiCharge'] = $release['payment_surcharge'];
				$orderRelease['taxAmount'] = $release['tax'];
                $orderRelease['giftCardAmount'] = $release['gift_card_amount'];
	
				if($release['status'] == 'F' && ($release['cancellation_code'] == 5 || $release['cancellation_code'] == 6 || $release['cancellation_code'] == 37 || $release['cancellation_code'] == 38)) {
					$orderRelease['status'] = 'RTO';
					unset($orderRelease['cancellationReasonId']);
				}
	
				//additional info
				$additionalInfoRows = func_query("select * from order_additional_info where order_id_fk = " . $release['orderid']);
				if($additionalInfoRows) {
					foreach ($additionalInfoRows as $row) {
						switch($row['key']) {
							case 'EXPECTED_PICKING_TIME':
								$orderRelease['expectedPickingTime'] = $row['value']; break;
							case 'CUSTOMER_PROMISE_TIME':
								$orderRelease['customerPromiseTime'] = $row['value']; break;
							case 'EXPECTED_CUTOFF_TIME':
								$orderRelease['expectedCutoffTime'] = $row['value']; break;
						}
					}
				}
	
				// order lines
				$items = func_query("select xod.*,oq.optionid as option_id,oq.quantity,sosm.sku_id from ( ( xcart_order_details xod left outer join mk_order_item_option_quantity oq on xod.itemid = oq.itemid) left outer join mk_styles_options_skus_mapping sosm on oq.optionid = sosm.option_id and sosm.sku_id > 0) where orderid = $release[orderid] group by xod.itemid");
	
				foreach($items as $item){
					$line = array();
					$line['orderId'] = $release['group_id'];
					$line['orderReleaseId'] = $release['orderid'];
					$line['id'] = $item['itemid'];
					$line['styleId'] = $item['product_style'];
					$line['optionId'] = $item['option_id'];
					if(!empty($item['sku_id']))
						$line['skuId'] = $item['sku_id'];
	
					if(empty($item['item_status']) || $item['item_status'] == 'UA')
						$line['status'] = 'A';
					else
						$line['status'] = $item['item_status'];
	
					$line['sellerId'] = $item['seller_id'];
					if(!empty($item['supply_type']))
						$line['supplyType'] = $item['supply_type'];
					else
						$line['supplyType'] = 'ON_HAND';
						
					if($release['status'] == 'F' || $item['item_status'] == 'IC')
						$line['quantity'] = 0;
					else
						$line['quantity'] = $item['amount'];
					
					$line['discountedQuantity'] = $item['discount_quantity'];
					$line['unitPrice'] = $item['price'];
					$line['discount'] = $item['discount'];
					$line['cartDiscount'] = $item['cart_discount_split_on_ratio'];
					$line['pgDiscount'] = $item['pg_discount'];
					$line['couponDiscount'] = $item['coupon_discount_product'];
					$line['cashRedeemed'] = $item['cash_redeemed'];
					$line['finalAmount'] = ($item['price']*$item['amount'])+$item['taxamount']-$item['discount']-$item['cart_discount_split_on_ratio']-$item['pg_discount']-$item['coupon_discount_product']-$item['cash_redeemed']-($item['item_loyalty_points_used']*$releases[0]['loyalty_points_conversion_factor']);
					$line['taxAmount'] = $item['taxamount'];
					$line['taxRate'] = $item['tax_rate'];
					$line['isDiscountedProduct'] = $item['is_discounted'];
					$line['isReturnableProduct'] = $item['is_returnable'];
                    $line['giftCardAmount'] = $item['gift_card_amount'];
					$line['isCustomizable'] = $item['is_customizable'];
					$line['customizedMessage'] = $item['extra_data'];
                                        $line['packagingType'] = $item['packaging_type'];
                                        if ($item['is_packaged']) {
                                            $line['packagingStatus'] = 'PREMIUM_PACKAGED';
                                        }
                                        $line['createdBy'] = $order['login'];
                                        
					// product tags
					$line['isJewellery'] = 0;
					$line['isHazMat'] = 0;
					$line['isFragile'] = 0;
					$productTags = $styleId2TagsMap[$item['product_style']];
					foreach($productTags as $tag) {
						switch($tag['name']) {
							case 'isJewellery':
								$line['isJewellery'] = 1;
								break;
							case 'isHazmat':
								$line['isHazMat'] = 1;
								break;
							case 'isFragile':
								$line['isFragile'] = 1;
								break;
						}
					}
					 
					$orderRelease['orderLines'][] = array('orderLine' => $line);
				}
	
				$orderReleases[] = $orderRelease;
			} 
		}
		
		return $orderReleases;
	}
			 
}

?>
