<?php

class OrderDataCorrectionManager{
	
	public static function markOrderShipped($orderid,$from_status,$login = 'SYSTEM'){
		$order_details = func_query_first("select * from xcart_orders where orderid =".$orderid,true);
		if($order_details['status'] == 'L' || $order_details['status'] == 'DL' || $order_details['status'] == 'F' || ($order_details['status'] == 'C' 
			&& (($order_details['payment_method'] == 'cod' && $order_details['cod_pay_status'] != 'paid') || $order_details['payment_method'] == 'on'))){
				
			if($from_status == 'RTO'){
				$rto_id = func_query_first_cell("select id from mk_old_returns_tracking where orderid = ".$orderid,true);
				if($rto_id){
					func_query("delete from mk_old_returns_tracking_activity where returnid = $rto_id");
					func_query("delete from mk_old_returns_tracking where id = $rto_id");
				}	
			}
			$tracking = func_query_first("select * from mk_shipment_tracking where ref_type='o' and ref_id = ".$orderid,true);
			
			$tracking_detail = array();
			$tracking_detail['id'] = $tracking['id'];
			$tracking_detail['location'] = '';
			$tracking_detail['action_date'] = date('Y-m-d H:i:s');
			$tracking_detail['activity_type'] = 'IT';
			$tracking_detail['external_tracking_code'] = $order_details['tracking'];
			$tracking_detail['remark'] = 'Correcting a manual error';
			
			func_array2insert('mk_shipment_tracking_detail', $tracking_detail);
			func_query("update mk_shipment_tracking set delivery_status='FIT',delivery_date=null,last_updated_time=now() where ref_type='o' and ref_id = ".$orderid);
			func_query("update xcart_order_details set item_status = 'S' where orderid = $orderid and item_status = 'D'");
			if($order_details['payment_method'] == 'cod'){
				func_query("update xcart_orders set cod_pay_status = 'pending',status='SH',delivereddate = null where orderid = $orderid");	
			}else{
				func_query("update xcart_orders set status='SH',delivereddate = null,completeddate=null where orderid = $orderid");
			}
			func_addComment_order($orderid, $login, 'Automated', 'status change', 'Force updating to shipped');
                        EventCreationManager::pushReleaseStatusUpdateEvent($orderid, 'SH', 'Force updated to shipped', $order_details['shippeddate'], $login);
			return array('status'=>true);
		}else{
			return array('status'=>false,'reason'=>"Order $orderid is not in DL/C status. this operation cannot be done");
		}	
	}
	
	public static function markOrderDelivered($orderid,$from_status,$login = 'SYSTEM'){
		$order_details = func_query_first("select * from xcart_orders where orderid =".$orderid,true);
		
		if($order_details['status'] == 'SH' || $order_details['status'] == 'L' || $order_details['status'] == 'F') {
			if($from_status == 'RTO'){
				$rto_id = func_query_first_cell("select id from mk_old_returns_tracking where orderid = ".$orderid,true);
				if($rto_id){
					func_query("delete from mk_old_returns_tracking_activity where returnid = $rto_id");
					func_query("delete from mk_old_returns_tracking where id = $rto_id");
				}	
			}
			
			$tracking = func_query_first("select * from mk_shipment_tracking where ref_type='o' and ref_id = ".$orderid,true);
			
			$tracking_detail = array();
			$tracking_detail['id'] = $tracking['id'];
			$tracking_detail['location'] = '';
			$tracking_detail['action_date'] = date('Y-m-d H:i:s');
			$tracking_detail['activity_type'] = 'DL';
			$tracking_detail['external_tracking_code'] = $order_details['tracking'];
			$tracking_detail['remark'] = 'order delivered';
			
			func_array2insert('mk_shipment_tracking_detail', $tracking_detail);
			
			func_query("update mk_shipment_tracking set delivery_status='DL',delivery_date=now(),last_updated_time=now() where ref_type='o' and ref_id = ".$orderid);
			func_query("update xcart_order_details set item_status = 'D' where orderid = $orderid and item_status = 'S'");
			$currtime = time();
			if($order_details['payment_method'] == 'cod'){
				func_query("update xcart_orders set status='DL',delivereddate = $currtime where orderid = $orderid");	
			}else{
				func_query("update xcart_orders set status='C',delivereddate = $currtime,completeddate = $currtime where orderid = $orderid");
			}
			func_addComment_order($orderid, $login, 'Automated', 'status change', 'Force updating to delivered');
                        EventCreationManager::pushReleaseStatusUpdateEvent($orderid, 'DL', 'Force updated to delivered', time(), $login);
			return array('status'=>true);
		} else {
			return array('status'=>false,'reason'=>"Order $orderid is not in shipped status. this operation cannot be done");
		}	
	}
	
}

?>