<?php
/*
 * Created on Feb 2, 2012
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 * 
 */
 
 include_once($xcart_dir."/include/func/func.mk_orderbook.php");
 include_once($xcart_dir."/include/func/func.order.php");
 include_once($xcart_dir."/modules/apiclient/SkuApiClient.php");
 include_once($xcart_dir."/include/class/oms/ItemManager.php");
 include_once("$xcart_dir/include/class/class.mail.multiprovidermail.php");
 include_once($xcart_dir."/modules/apiclient/ReleaseApiClient.php");
 include_once("$xcart_dir/include/class/oms/EventCreationManager.php");
 
 class OrderWHManagerOld {
 	
 	public static function loadWarehouseInvCounts($skuIds) {
 		$skuDetails = SkuApiClient::getSkuDetailsInvCountMap($skuIds);
 		$checkInStock=FeatureGateKeyValuePairs::getBoolean('checkInStock', true);
 		if(!$checkInStock) {
	 		foreach($skuDetails as $skuId=>$sku) {
	 			$skuDetails[$skuId]['availableItems'] = 99;
	 		}
 		}
		return $skuDetails;
 	}
 	
 	public static function updateInventoryAfterSplit($group_orderid, $whIdToOrderIdMap, $comment, $user) {
 		$releaseIds = array();
 		foreach($whIdToOrderIdMap as $whId=>$oid) {
			decrement_sku($oid, $whId);
			$releaseIds[] = $oid;
			if($group_orderid != $oid) {
				func_addComment_order($oid, $user, 'INFO', 'Order Split', "Order created from $group_orderid. $comment");
			}
		}
		if($releaseIds > 1)
			putFreeItemShipmentOnHold($group_orderid);
 	}
 	
 	public static function assignWarehouseForOrder($orderid, $shipping_address, $productsInCart=false, $sku_details=false, $new_order_comment=false) {
 		global $weblog;
 		if(!$productsInCart) {
 			$productsInCart = func_create_array_products_of_order($orderid);
 		}
 		
 		if(!$sku_details) {
 			$skuIds = array();
	 		foreach($productsInCart as $product) {
	 			$skuIds[] = $product['sku_id'];
	 		}
 			$sku_details = OrderWHManagerOld::loadWarehouseInvCounts($skuIds);
 		}
 		
 		$weblog->info("Sku Details - ". print_r($sku_details, true));
 		
 		$canProcessOrder = true;
 		if(!$sku_details)
 			$canProcessOrder = false;
 		else {
 			foreach($sku_details as $sku) {
 				if(!isset($sku['warehouse2AvailableItems']) || empty($sku['warehouse2AvailableItems'])) {
 					$canProcessOrder = false;
 					break;
 				}
 			}
 		} 
 		
 		if(!$canProcessOrder) {
 			$updateData['status'] = 'OH';
 			$updateData['queueddate'] = null;
 			$oh_reason = get_oh_reason_for_code('OPE');
 			$content = "Order $orderid can not be processed as all the items stock information was not available while assigning warehouse. Moving order to On Hold state. Please check the stock and queue this order manually";
			if($oh_reason != null){
				$updateData['on_hold_reason_id_fk'] = $oh_reason['id'];
				$sql = "select * from mk_order_action_reasons where action = 'oh_disposition' and reason = 'SCU'";
				$reason_text = func_query_first($sql, true);
				$commentArrayToInsert = Array ("orderid" => $orderid,
	      								"commenttype" => "Automated", 
	      								"commenttitle" => "status change", 
	      								"commentaddedby" => "admin", 
	      								"description" => "Putting the order on hold as : ".$reason_text['reason_display_name'], 
	      								"commentdate" => time());
	    		func_array2insert("mk_ordercommentslog", $commentArrayToInsert);
			}
			
			func_array2update('orders', $updateData, "orderid = $orderid");
			
			$mail_details = array( 
								"from_email"=>'admin@myntra.com',
								"from_name"=>'OMS Admin',
								"to" => constant("ORDER_OOS_NOTIFICATION"),
								"mail_type" => MailType::NON_CRITICAL_TXN,
								"subject"=> 'Order OOS After Payment',
								"content"=> $content
							);
			$multiPartymailer = new MultiProviderMailer($mail_details);
			$multiPartymailer->sendMail();
			
			return false;
 		}
 		
 		//If multiple items are there.. check if they can be processed from one single warehouse..
 		if(count($productsInCart) > 1){
 			$whId2ItemId2QuantityMap = OrderWHManagerOld::checkForCommonWarehouse($productsInCart, $sku_details, $shipping_address);
 		}

		if(!$whId2ItemId2QuantityMap)
			$whId2ItemId2QuantityMap = OrderWHManagerOld::getWarehouse2ItemQuantityInfo($productsInCart, $sku_details, $shipping_address);
			
 		$weblog->info("OrderWHManagerOld: WarehouseId->ItemId->qty split - ". print_r($whId2ItemId2QuantityMap, true));
		
		if(count($whId2ItemId2QuantityMap) > 1) {
			// split is required..
			$whIdToOrderId = OrderWHManagerOld::splitOrder($orderid, $whId2ItemId2QuantityMap, $new_order_comment);
		} else {
			// there is only 1 warehouse for all items. So no need to split anything..
			$whIds = array_keys($whId2ItemId2QuantityMap);
			$sql = "update xcart_orders set warehouseid = $whIds[0] where orderid = $orderid";
			db_query($sql);
			$whIdToOrderId[$whIds[0]] = $orderid;
			//updateShippingMethodForOrder($orderid);
		}
		
		return $whIdToOrderId;
 	} 
 	
 	public static function checkForCommonWarehouse($productsInCart, $sku_details, $shipping_address) {
 		global $weblog;
 		$commonWarehouses = false;
 		foreach($productsInCart as $product) {
 			$quantity_ordered = $product['quantity'];
 			$warehouse2AvailableItems = $sku_details[$product['sku_id']]['warehouse2AvailableItems'];
 			$availableWarehouses = array();
 			foreach($warehouse2AvailableItems as $whId=>$availableCount) {
 				if($availableCount >= $quantity_ordered) {
 					$availableWarehouses[] = $whId;
 				}
 			}
 			$weblog->info("checkForCommonWarehouse:: Item ".$product['itemid']." is available in Warehouses ".implode(",", $availableWarehouses));
 			if($commonWarehouses===false) {
 				$commonWarehouses = $availableWarehouses;
 			} else {
 				$commonWarehouses = array_intersect($commonWarehouses, $availableWarehouses);
 			}
 		}
 		
 		$commonWarehouses = array_values($commonWarehouses);
 		if(count($commonWarehouses) == 0) {
 			return false;
 		} else if(count($commonWarehouses) == 1) {
 			$selectedWhId = $commonWarehouses[0];
 		} else {
 			$selectedWhId = OrderWHManagerOld::getPreferedWarehouseForAddress($shipping_address);
 		}
 		
 		$whId2Itemid2QtyMap = array();
 		foreach($productsInCart as $product) {
 			$whId2Itemid2QtyMap[$selectedWhId][$product['itemid']] = $product['quantity'];
 		}
 		
 		return $whId2Itemid2QtyMap;
 	}
 	
 	public static function getWarehouse2ItemQuantityInfo($productsInCart, $sku_details, $shipping_address) {
 		global $weblog;
 		// for each product, check available count and assign item and qty to different warehouses.
		$whId2ItemId2QuantityMap = array();
 		foreach($productsInCart as $product) {
 			$quantity_ordered = $product['quantity'];
 			$warehouse2AvailableItems = $sku_details[$product['sku_id']]['warehouse2AvailableItems'];
 			$weblog->info("OrderWHManagerOld: warehouse->counts for item -  ".$product['itemid']." (".$product['sku_id'].") = ". print_r($warehouse2AvailableItems, true));
 			$availableInWh = 0;
 			$selectedWhId = false;
 			foreach($warehouse2AvailableItems as $whId=>$availableCount) {
 				if($availableCount >= $quantity_ordered) {
 					$availableInWh++;
 					$selectedWhId = $whId;
 				}
 			}
 			
 			$weblog->info("OrderWHManagerOld: Item - ".$product['itemid']." with qty - $quantity_ordered is present in $availableInWh warehouses");
 			
 			if($availableInWh == 0) {
 				// need a split.. cannot be processed from single wh
 				OrderWHManagerOld::getItemCountPerWarehouse($product['itemid'], $quantity_ordered, $warehouse2AvailableItems, $shipping_address, $whId2ItemId2QuantityMap);
 			} else if($availableInWh == 1) {
 				// full available in only one WH.. need to be processed from there
 				$whId2ItemId2QuantityMap[$selectedWhId][$product['itemid']] = $quantity_ordered;
 			} else {
 				// available in multiple wh.. process from nearest... based on metrics
 				$selectedWhId = OrderWHManagerOld::getPreferedWarehouseForAddress($shipping_address);
 				$whId2ItemId2QuantityMap[$selectedWhId][$product['itemid']] = $quantity_ordered;
 			}
 		}
 		return $whId2ItemId2QuantityMap;
 	}
 	
 	private static function getPreferedWarehouseForAddress($shipping_address) {
 		global $weblog;
 		$zipcode = $shipping_address['s_zipcode'];
 		$first2digits = intval($zipcode/10000);
 		$weblog->info("Shipping Address zipcode prefix = $first2digits");
 		$sql = "Select * from mk_shipping_cost_metrics where state_zipcode_prefix = '$first2digits'";
 		$results = func_query($sql);
 		$selectedWhId = 1;
 		if($results){
 			$minimum_cost = $results[0]['shipping_cost'];
 			$selectedWhId = $results[0]['warehouse_id'];
 			foreach($results as $row) {
 				$weblog->info("Wh Id scanned - ".$row['warehouse_id']);
 				if($row['shipping_cost'] < $minimum_cost) {
 					$minimum_cost = $row['shipping_cost'];
 					$selectedWhId = $row['warehouse_id'];
 				}
 				$weblog->info("Min cost now - $minimum_cost and wh id - $selectedWhId");
 			}
 		}
 		return $selectedWhId;
	}
	
	private static function getItemCountPerWarehouse($itemid, $req_quantity, $warehouse2AvailableItems, $shipping_address, &$whId2ItemId2QuantityMap) {
 			// considering there are n warehouses
 			foreach($warehouse2AvailableItems as $whId=>$availableCount) {
 				if($availableCount == 0) {
 					continue;
 				}
 				if($req_quantity > $availableCount) {
 					$whId2ItemId2QuantityMap[$whId][$itemid] = $availableCount;
 					$req_quantity -= $availableCount;
 				} else {
 					$whId2ItemId2QuantityMap[$whId][$itemid] = $req_quantity;
 					$req_quantity = 0;
 					break;
 				}
 			}
	 		if($req_quantity > 0) {
	 			foreach($whId2ItemId2QuantityMap as $whId=>$item2count) {
	 				$whId2ItemId2QuantityMap[$whId][$itemid] +=  $req_quantity;
	 				break;
	 			}
	 		}
	}
	
	
	public static function splitOrder($orderid, $whId2ItemId2QuantityMap, $new_order_comment) {
		global $XCARTSESSID, $sql_tbl;
		$original_order = func_query_first("Select * from xcart_orders where orderid = $orderid");
		
		$all_orders = array();
		foreach($whId2ItemId2QuantityMap as $whId=>$itemid2QtyMap) {
			$all_orders[$whId] = $original_order;
		}
		
		$items = func_query("Select * from xcart_order_details where orderid = $orderid");
		$itemid2ItemMap = array();
		foreach($items as $item) {
			$itemid2ItemMap[$item['itemid']] = $item;
		}		
		
		$itemids = array_keys($itemid2ItemMap);
		$item_options_qty = func_query("SELECT * from mk_order_item_option_quantity where itemid in (".implode(",", $itemids).")");
		$itemid2OptionQtyMap = array();
		foreach($item_options_qty as $row) {
			$itemid2OptionQtyMap[$row['itemid']] = $row;
		}
		
		foreach($whId2ItemId2QuantityMap as $whId => $item2QtyMap) {
			$all_orders[$whId] = ItemManager::assignItems($all_orders[$whId], $item2QtyMap, $itemid2ItemMap, $itemid2OptionQtyMap);
		}
		
		$whId2OrderId = array();
		$initial_wh_id = min(array_keys($all_orders));
		foreach($all_orders as $whId => $order) {
			if($whId == $initial_wh_id) {
				// original order
				$items = $order['items'];
				unset($order['items']);
				$order['warehouseid'] = $whId;
				$order['total'] += $order['shipping_cost'];
				$order['total'] += $order['gift_charges'];
				
				func_array2update('orders', $order, "orderid = $orderid");
				
				$itemIdsInOrder = array();
				foreach($items as $item) {
					$option_qty_mapping = $item['option_qty_mapping'];
					unset($item['option_qty_mapping']);
					unset($item['quantity']);
					func_array2update('order_details', $item, "itemid = ".$item['itemid']);
					func_array2update('mk_order_item_option_quantity', $option_qty_mapping, "itemid = ".$item['itemid']);
					$itemIdsInOrder[] = $item['itemid'];
				}
				$whId2OrderId[$whId] = $orderid;		
				
				$delete_moveditems_sql = "DELETE from $sql_tbl[order_details] where orderid = $orderid and itemid not in (".implode(",", $itemIdsInOrder).")"; 			
				db_query($delete_moveditems_sql);

			} else {
				// new orders
				$items = $order['items'];
				unset($order['items']);
				unset($order['orderid']);
				
				$new_orderid = create_new_order();
				$order['invoiceid'] = create_new_invoice_id($new_orderid);
				$order['orderid'] = $new_orderid;
				$order['warehouseid'] = $whId;
				$order['date'] = time();
				if($order['status'] == 'WP') {
					$order['queueddate'] = time();
				}
				$order['shipping_cost'] = 0.00;
				$order['gift_charges'] = 0.00;
				$order['payment_surcharge'] = 0.00;
				func_array2insert('orders', $order);
				
				foreach($items as $item) {
					$option_qty_mapping = $item['option_qty_mapping'];
					unset($item['itemid']);
					unset($item['option_qty_mapping']);
					unset($item['quantity']);
					
					$item['orderid'] = $new_orderid;
					$new_itemid = func_array2insert('order_details', $item);
					$option_qty_mapping['itemid'] = $new_itemid;
					func_array2insert('mk_order_item_option_quantity', $option_qty_mapping);
				}
				$whId2OrderId[$whId] = $new_orderid;
			}
		}
		//updateShippingMethodForOrder(array_values($whId2OrderId));
		return $whId2OrderId;
	} 
	
// ---------------------------------------------------------------------------------------------------------------------------------------------------	
	// Reassign warehouse
	public static function assignOrderItemsToNewWarehouse($orderid, $itemid, $qty, $warehouseId, $user) {
 		global $weblog;
 		$order = func_query_first("Select * from xcart_orders where orderid = $orderid");
 		
 		$order['firstname'] = mysql_real_escape_string($order['firstname']);
 		$order['lastname'] = mysql_real_escape_string($order['lastname']);
 		$order['s_firstname'] = mysql_real_escape_string($order['s_firstname']);
		$order['s_lastname'] = mysql_real_escape_string($order['s_lastname']);
		$order['s_address'] = mysql_real_escape_string($order['s_address']);
		$order['s_city'] = mysql_real_escape_string($order['s_city']);
		$order['b_address'] = mysql_real_escape_string($order['b_address']);
		$order['b_city'] = mysql_real_escape_string($order['b_city']);
			
		$item_results = ItemManager::getItemDetailsForItemIds($itemid);
		$item = $item_results[0];
 		$skuId = $item['sku_id'];
 		$whId2NameMap = WarehouseApiClient::getWarehouseId2NameMap();
 		// whole order needs to be moved to new warehouse
 		$orderComment = "";
 		$original_wh_id = $order['warehouseid'];
 		if($order['qtyInOrder'] == $qty) {
 			$order['courier_service'] = '';
			$order['tracking'] = '';
 			$weblog->info("Moving all items in the order. Moving order to warehouse - $warehouseId");
 			$order['warehouseid'] = $warehouseId; 
 			$orderComment = "$qty pieces of Item - $itemid moved from ".$whId2NameMap[$original_wh_id]." to ".$whId2NameMap[$warehouseId].". Moving full order to target warehouse.";	
 		} else {
 			$weblog->info("Part of order needs to be moved.. Creating new order.");
			$new_orderid = create_new_order();
			$new_order = $order;
			$new_order['orderid'] = $new_orderid;
			$new_order['invoiceid'] = create_new_invoice_id($new_orderid);
			$new_order['splitdate'] = time();
			$new_order['queueddate'] = time();
			$new_order['total'] = 0.00;
			$new_order['subtotal'] = 0.00;
			$new_order['coupon_discount'] = 0.00;
			$new_order['discount'] = 0.00;
			$new_order['pg_discount'] = 0.00;
			$new_order['payment_surcharge'] = 0.00;
			$new_order['cash_redeemed'] = 0.00;
			$new_order["store_credit_usage"] = 0.00;
			$new_order["earned_credit_usage"] = 0.00;
			$new_order['cashback'] = 0.00;
			$new_order['qtyInOrder'] = 0;
			$new_order['tax'] = 0.00;
			$new_order['shipping_cost'] = 0.00;
			$new_order['gift_charges'] = 0.00;
			$new_order['courier_service'] = '';
			$new_order['tracking'] = '';
			$new_order['warehouseid'] = $warehouseId;
            $new_order['gift_card_amount'] = 0.00;
			
			$orderComment = "$qty pieces of Item - $itemid moved from ".$whId2NameMap[$original_wh_id]." to ".$whId2NameMap[$warehouseId].". Created New Order - $new_orderid for warehouse - ".$whId2NameMap[$warehouseId].". ";
			
			$new_item = ItemManager::updateOrderItemMovement($order, $new_order, $item, $qty);
 			
 			if($new_item) {
	 			$item_option_qty = $new_item['item_option_qty'];
	 			unset($new_item['item_option_qty']);
	 			unset($new_item['sku_id']);
	 			unset($new_item['quantity']);
				
				$itemid = func_array2insert('order_details', $new_item);
				$item_option_qty['itemid'] = $itemid;
				func_array2insert('mk_order_item_option_quantity', $item_option_qty);
				$orderComment .= " Created new item - ".$new_item['itemid']." with required no of pieces.";
	 		} else {
	 			$orderComment .= " Complete Item is moved to new order.";
	 		}
	 		
	 		$item_option_qty = $item['item_option_qty'];
	 		unset($item['item_option_qty']);
	 		unset($item['sku_id']);
	 		unset($item['quantity']);
			func_array2update('order_details', $item, "itemid = ".$item['itemid']);
			if($item_option_qty)
				func_array2update('mk_order_item_option_quantity', $item_option_qty, 'itemid = '.$item['itemid']);
 		}	
 		
 		func_array2update('orders', $order, 'orderid = '.$order['orderid']);
		if($new_order){
			func_array2insert('orders', $new_order);
			ReleaseApiClient::pushOrderReleaseToWMS($new_orderid, $user,'');
			//updateShippingMethodForOrder(array($orderid,$new_orderid));
		}else{
			//updateShippingMethodForOrder($orderid);
		}
 		
		// decrease blocked count from original wh and move it to new warehouse..
		SkuApiClient::moveSkuBlockedOrderCount($skuId, $qty, $original_wh_id, $warehouseId, $user);
		
		ReleaseApiClient::pushOrderReleaseToWMS($orderid, $user,'update');
		
		EventCreationManager::pushCompleteOrderEvent($order['group_id']);
		
		func_addComment_order($orderid, $user, 'Note', 'Warehouse Reassignment', $orderComment);
		
		return array('SUCCESS', "Warehouse reassignment successful for $qty pieces of Item");
 	}	
 	
 }
 
?>
