<?php

namespace oms;

global $xcart_dir;
include_once $xcart_dir . "/include/func/func.mk_old_returns_tracking.php";
include_once("$xcart_dir/include/func/func.mkcore.php");
include_once("$xcart_dir/include/func/func.mk_orderbook.php");
include_once("$xcart_dir/include/func/func.mkspecialoffer.php");
include_once("$xcart_dir/include/func/func.inventory.php");
include_once("$xcart_dir/include/func/func.mail.php");
include_once("$xcart_dir/include/func/func_sku.php");
include_once("$xcart_dir/include/class/class.orders.php");
include_once("$xcart_dir/include/func/func.courier.php");
include_once("$xcart_dir/include/func/func.returns.php");
include_once("$xcart_dir/include/func/func.mk_shipment_tracking.php");
include_once("$xcart_dir/include/class/class.feature_gate.keyvaluepair.php");
include_once("$xcart_dir/modules/apiclient/ItemApiClient.php");
include_once("$xcart_dir/modules/apiclient/LocationApiClient.php");
include_once("$xcart_dir/modules/apiclient/SkuApiClient.php");
include_once("$xcart_dir/modules/apiclient/WarehouseApiClient.php");
include_once("$xcart_dir/include/class/oms/OrderWHManager.php");
include_once("$xcart_dir/modules/apiclient/TripApiClient.php");
include_once("$xcart_dir/modules/apiclient/ReleaseApiClient.php");
include_once("$xcart_dir/include/class/oms/EventCreationManager.php");
include_once($xcart_dir . "/include/class/shopfest/ShopFest.php");
include_once("$xcart_dir/include/class/oms/OrderProcessingMailSender.php");
include_once("$xcart_dir/modules/apiclient/MinacsApiClient.php");
include_once "$xcart_dir/modules/myntCash/MyntCashTransaction.php";
include_once "$xcart_dir/modules/myntCash/MyntCashService.php";
include_once "$xcart_dir/modules/loyaltyPoints/LoyaltyPointsPortalClient";
include_once "$xcart_dir/modules/apiclient/OrderServiceApiClient.php";
include_once "$xcart_dir/modules/apiclient/TatApiClient.php";
x_load('mail', 'order');

class OrderDetailRequestProcessor {

    public function processRequestToResetCourierTracking($orderid) {
        global $login;
        $update_data = array('tracking' => '', 'courier_service' => '');
        func_array2update('orders', $update_data, "orderid = $orderid");
        \EventCreationManager::pushReleaseUpdateEvent($orderid, $login, '', '');
        func_addComment_order($orderid, $login, 'Note', 'Courier Tracking Reset', "Reset Courier Tracking info for order");
        func_header_location("/admin/order.php?orderid=" . $orderid . "&statussuccessmsg=Successfully reset courier tracking");
    }

    public function processRequestToRemoveItem($userid, $orderid, $skuItemCodeToRemove, $skuId) {
        global $weblog, $sql_tbl;

        $order = func_query_first("Select warehouseid from xcart_orders where orderid = $orderid", true);

        $response = \ItemApiClient::remove_item_order_association($orderid, $order['warehouseid'], $skuItemCodeToRemove, 'RETURN_FROM_OPS', false, $userid);
        //if($response === true) {
        $items_query = "Select od.itemid, qapass_items, amount, item_status, sku_id from xcart_order_details od, mk_order_item_option_quantity oq, mk_styles_options_skus_mapping som where od.itemid = oq.itemid and oq.optionid = som.option_id and od.orderid = $orderid";
        $order_items = func_query($items_query, true);

        foreach ($order_items as $row) {
            if ($row['sku_id'] == $skuId) {
                $qapass_items_string = $row['qapass_items'];
                $qapass_items = explode(",", $qapass_items_string);
                $newqapass_items = array();
                $item_removed = false;
                foreach ($qapass_items as $itembarcode) {
                    if (trim($itembarcode) != trim($skuItemCodeToRemove)) {
                        $newqapass_items[] = $itembarcode;
                    } else {
                        $item_removed = true;
                    }
                }

                $update_sql = "update xcart_order_details set item_status = 'RP' ";
                if ($item_removed) {
                    $update_sql .= ",qapass_items = '" . implode(",", $newqapass_items) . "'";
                }
                $update_sql .= " where item_status != 'IC' and itemid = " . $row['itemid'];
                db_query($update_sql);
                \EventCreationManager::pushItemUpdateEvent($row['itemid'], 'RP', "Moving to Repick after removing $skuItemCodeToRemove from item", time(), $userid);
                $commentdetails = "Removed Issued item $skuItemCodeToRemove from order";
                func_addComment_order($orderid, $_POST['userid'], 'Note', 'Sku Item Removed', $commentdetails);
                $responsemsg = "Successfully removed item $skuCodeToRemove from order $orderid";
                break;
            }
        }
        //} else {
        //	$weblog->info("Failed to remove item $skuCodeToRemove from order $orderid");
        //	$responsemsg = "Failed to remove item $skuCodeToRemove from order $orderid";
        //}

        func_header_location("/admin/order.php?orderid=" . $orderid . "&statussuccessmsg=$responsemsg");
    }

    public function processRequestForOHResolutionUpdate($login, $orderid, $oh_res, $oh_sub_res, $oh_res_comment) {
        global $sql_tbl, $weblog;
        $curr_time = date('Y-m-d H:i:s');
        $oh_res_comment = mysql_real_escape_string($oh_res_comment);
        $sql = "insert into cod_oh_orders_dispostions_log (refid,orderid,disposition,reason,trial_number,created_by,comment,created_time,logged_time) select id,orderid,disposition,reason,trial_number,created_by,comment,created_time,'$curr_time' from cod_oh_orders_dispostions where orderid = $orderid";
        func_query($sql);
        $sql = "update cod_oh_orders_dispostions set created_time = '$curr_time',created_by = '" . $login . "',comment = '" . $oh_res_comment . "',disposition = '" . $oh_res . "' , reason = '" . $oh_sub_res . "' , trial_number = trial_number + 1 where orderid = $orderid";
        func_query($sql);

        $sql = "select * from cod_oh_orders_dispostions where orderid = $orderid";
        $curr_dispositon = func_query_first($sql);
        $maxNoOfTrials = \WidgetKeyValuePairs::getWidgetValueForKey('conditionalCodOnHold.maxNoOfTrials');
        if ($curr_dispositon['trial_number'] <= $maxNoOfTrials && $oh_res == 'NR') {
            \OrderProcessingMailSender::notifyCodOnHoldOrderRettempt($orderid);
        }
        if (($curr_dispositon['trial_number'] >= $maxNoOfTrials && $oh_res == 'NR') || $oh_res == 'CR') {
            if ($oh_res == 'NR') {
                $oh_sub_res = 'NR';
            }
            $cancel_args = array(
                'cancellation_type' => $oh_res,
                'reason' => $oh_sub_res,
                'orderid2fullcancelordermap' => array($orderid => true),
                'generateGoodWillCoupons' => false,
                'is_oh_cancellation' => true
            );
            func_change_order_status($orderid, 'F', $login, "Cancelling order from oh tracker, for more details refer oh calling log section", '', true, '', $cancel_args);
            \MinacsApiClient::removeOnHoldOrder($orderid, 'Order Cancelled');
        } else if ($oh_res == 'CQ') {
            $all_orderids = func_query("Select orderid, group_id from $sql_tbl[orders] where group_id=$orderid");

            $orderids = array();
            foreach ($all_orderids as $row) {
                $orderids[] = $row['orderid'];
                //func_change_order_status($row['orderid'], 'Q', $login ,"Queing order from oh tracker. for more details refer oh calling log section");
                db_query("update xcart_orders set status = 'Q', queueddate = " . time() . " where orderid = $orderid");
                func_addComment_status($row['orderid'], "Q", "OH", $oh_res_comment, $login);
                if ($row['orderid'] == $row['group_id']) {
                    \EventCreationManager::pushOrderUpdateEvent($row['group_id'], 'Q', 'Moving from OH to Q', time(), $login);
                }
                \EventCreationManager::pushReadyForReleaseEvent($row['orderid'], $login);
            }
            //updateShippingMethodForOrder($orderids);
            //when queued from cod on hold page..
            //\OrderProcessingMailSender::notifyCodOnHoldOrderVerified($orderid);

            \MinacsApiClient::removeOnHoldOrder($orderid, 'Order Queued');
        }

        func_header_location("order.php?orderid=" . $orderid);
    }

    public function processRequestForOrderStatusChange($orderid, $login, $status, $tracking, $courier_service, $comment_msg_on, $reason_type, $oh_reason, $details, $sale_type, $sales_agent, $status_change_comment_msg, $ccavenue, $force_inv_update) {
        global $sql_tbl, $weblog;
        $order = func_select_order($orderid);
        $order_status_inDB = $order['status'];

        $errorMsg = $this->validateOrderStatusTransition($status, $order_status_inDB, $ccavenue);
        if ($errorMsg != "") {
            func_header_location("order.php?orderid=$orderid&statuserrormsg=$errorMsg");
        }

        if ($ccavenue != 'N') {
            db_query("UPDATE $sql_tbl[orders] SET ccavenue='Y' WHERE orderid='$orderid'");
        }

        if ($courier_service !== "0" && !empty($courier_service)) {
            $query_data = array(
                "tracking" => $tracking,
                "courier_service" => $courier_service,
            );
            \EventCreationManager::pushReleaseUpdateEvent($orderid, $login, $courier_service, $tracking);
        }

        if ($sale_type != "0") {
            $count = func_query_first_cell("select count(*) from mk_tele_sales where orderid=$orderid");

            if ($_POST['sale_type'] == "TS") {
                $agent = ($sales_agent != null) ? $sales_agent : $login;
                if (!$count)
                    db_query("insert into mk_tele_sales(orderid,sales_agent) values ('$orderid','$agent')");
                else
                    db_query("update mk_tele_sales set sales_agent='$agent' where orderid='$orderid'");
            } else
                db_query("delete from mk_tele_sales where orderid='$orderid'");
        }

        if ($order['status'] != 'PP' || $order['status'] != 'PD' || $order['status'] != 'CD') {
            if (isset($details)) {
                $query_data['details'] = func_crypt_order_details($details);
            }
        }
        func_array2update("orders", $query_data, "orderid = '$orderid'");

        if ($status == 'SH') {
            $final_response = mark_orders_shipped(array($orderid), $login, 'Marked from Order Details Page. ' . $status_change_comment_msg);

            $errorMsg = "";
            if (isset($final_response['unsuccessful'])) {
                $errorMsg .= "Failed to ship order. Error - " . $final_response['unsuccessful']['errormsg'] . "<br>";
            }
            if (isset($final_response['failure']) && count($final_response['failure']) > 0) {
                $record = $final_response['failure'][0];
                $errorMsg .= "Failed to ship order. Error - " . $record['errormsg'] . "<br>";
            }

            if (isset($final_response['successful'])) {
                $successMsg = "Shipped Order successfully";
            }
            if ($errorMsg != "") {
                func_header_location("order.php?orderid=$orderid&statuserrormsg=$errorMsg");
            }
        } else if ($status === 'F' || $status === 'D') {
            $cancel_fullorder = $_POST['cancel_complete_order'] != "" && $_POST['cancel_complete_order'] == 'true' ? true : false;
            $update_inv = true; //!($comment_msg_on == 'OOSC');
            if ($status === 'F') {
                func_header_location("order.php?orderid=$orderid&statuserrormsg=Please use prism for cancellation. Say no to x5 admin");
            } else {
                $cancel_args = array(
                    'cancellation_type' => $reason_type,
                    'reason' => $comment_msg_on,
                    'orderid2fullcancelordermap' => array($orderid => $cancel_fullorder),
                    'generateGoodWillCoupons' => true
                );

                $response = func_change_order_status($orderid, $status, $login, $status_change_comment_msg, "", $update_inv, '', $cancel_args);
                $response = $response[$orderid];
                if ($response && $response['status'] == "FAILURE")
                    func_header_location("order.php?orderid=$orderid&statuserrormsg=" . $response['reason']);

                $successMsg = "Cancelled Order successfully";
            }
        } else if ($status === 'L') {
            $cancel_args = array(
                'cancellation_type' => 'LOC',
                'reason' => "LIT",
                'generateGoodWillCoupons' => false,
                'is_oh_cancellation' => false,
                'orderid2fullcancelordermap' => array($orderid => false)
            );
            $response = func_change_order_status($orderid, 'L', 'System Script', "Marking the order as Lost", '', false, '', $cancel_args);

            if ($response['status'] == 'failure') {
                func_header_location("order.php?orderid=$orderid&statuserrormsg=" . $response['message']);
            } elseif ($response['exOrder'] === true) {
                $successMsg.= "Order marked as lost, no refund issued for this exchange order ";
            } else {
                $successMsg = "Order marked as lost and refund issued successfully";
            }
        } else if ($status === 'Q' && $order_status_inDB == 'OH') {
            $on_hold_govt_tax_failure = false;
            $releaseIds = array();
            if ($orderid == $order['group_id']) {
                \EventCreationManager::pushOrderUpdateEvent($order['group_id'], $status, 'Moving from OH to Q', time(), $login);
                if (in_array($order['on_hold_reason_id_fk'], array(1, 2, 5, 8, 21, 22, 23, 29))) {
                    if ($order['on_hold_reason_id_fk'] == 29) {
                        $on_hold_govt_tax_failure = true;
                    }
                    $orders = func_query("select orderid from xcart_orders where group_id = $orderid and status = 'OH'");
                    foreach ($orders as $row)
                        $releaseIds[] = $row['orderid'];
                } else {
                    $releaseIds[] = $orderid;
                }
            } else {
                $releaseIds[] = $orderid;
            }
            foreach ($releaseIds as $releaseId) {
                $queuedDate = time();
                if ($on_hold_govt_tax_failure) {
                    \OrderWHManager::stampTax($releaseId);
                }
                db_query("update xcart_orders set status = 'Q', queueddate = " . $queuedDate . " where orderid = $releaseId");
                func_addComment_status($releaseId, "Q", "OH", $status_change_comment_msg, $login);
                \EventCreationManager::pushReadyForReleaseEventWithQueuedDate($releaseId, $queuedDate, $login);
            }
        } else {
            func_change_order_status($orderid, $status, $login, $status_change_comment_msg, "", false, $oh_reason);
            $successMsg = "Updated Order status to $status Successfully";
        }

        //if order released or put on hold then send mail
        if ($status != $order_status_inDB) {
            $from = "MyntraOperation";
            $args = array("ORDER_ID" => $orderid);
            if ($status == 'OH') {
                $loginEmail = "alrt_orderonhold@myntra.com";
                $template = "orderOnHold";
                sendMessage($template, $args, $loginEmail, $from, $bcc);
            }
            if (($status == 'P' || $status == 'Q' || $status == 'C') && ($order_status_inDB == 'OH')) {
                $loginEmail = "alrt_orderreleased@myntra.com";
                $template = "releaseOrder";
                sendMessage($template, $args, $loginEmail, $from, $bcc);
            }
        }

        $top_message = array(
            "content" => func_get_langvar_by_name("txt_order_has_been_changed")
        );

        func_header_location("order.php?orderid=$orderid&statussuccessmsg=$successMsg");
    }

    public function processRequestToShowOrderDetails($orderid) {
        global $xcart_dir, $smarty, $sql_tbl, $weblog, $login, $XCART_SESSION_VARS, $mk_style_properties;

        $order_data = func_order_data($orderid);
        $order = $order_data["order"];
        $userinfo = $order_data["userinfo"];

        $smarty->assign('order', $order);
        $smarty->assign("customer", $userinfo);

        $warehouse = \WarehouseApiClient::getWarehouseDetails($order['warehouseid']);
        $smarty->assign('warehouse', $warehouse['name']);

        $curr_order = func_query_first("select * from $sql_tbl[orders] where orderid = $orderid");
        $curr_ordertype = $curr_order['ordertype'];
        if ($curr_ordertype == 'ex') {
            $parentid = func_query_first_cell("select releaseid from exchange_order_mappings where exchange_orderid = $curr_order[orderid]", true);
        }
        $smarty->assign('curr_ordertype', $curr_ordertype);
        $smarty->assign("parent_orderid", $parentid);

        $exorderid = $orderid;
        $original_delivereddate = $curr_order['delivereddate'];
        $original_shippeddate = $curr_order['shippeddate'];
        while ($curr_ordertype == 'ex') {
            $parent_orderid = func_query_first_cell("select releaseid from exchange_order_mappings where exchange_orderid = $exorderid", true);
            $parent_order = func_query_first("select ordertype, delivereddate, shippeddate from xcart_orders where orderid = $parent_orderid", true);
            if ($parent_order['ordertype'] == 'ex') {
                $exorderid = $parent_orderid;
            } else {
                $original_delivereddate = $parent_order['delivereddate'];
                $original_shippeddate = $parent_order['shippeddate'];
                $curr_ordertype = '';
                break;
            }
        }

        //pickup_changes

        $returnPickupExcludedlist = \WidgetKeyValuePairs::getWidgetValueForKey('pickup.disabled.articletypes');
        $returnPickupExcludedArray = explode(",", $returnPickupExcludedlist);

        if ($original_delivereddate && $original_delivereddate > 0) {
            $returnable = ($original_delivereddate + 30 * 24 * 60 * 60 > time()) ? 'true' : 'false';
        } else {
            $returnable = 'false';
        }

        if ($returnable != 'true') {
            $roles = $XCART_SESSION_VARS['user_roles'];
            $userRoles = array();
            foreach ($roles as $role) {
                $userRoles[] = $role['role'];
            }
            if (in_array('CST', $userRoles)) {
                $returnable = 'true';
            }
        }

        $exchangesEnabled = \FeatureGateKeyValuePairs::getBoolean('exchanges.enabled', false);

        $smarty->assign("returnable", $returnable);
        $smarty->assign("exchangesEnabled", $exchangesEnabled);
        $smarty->assign("orderid", $orderid);

        $shipmentids = array($orderid);
        $shipmentId2OrdersMap = array($orderid => $order);
        if ($orderid == $order['group_id']) {
            // getting child orders.
            $sql = "select * from xcart_orders where group_id=$orderid and orderid != $orderid";
            $group_sub_orders = func_query($sql);
            if ($group_sub_orders) {
                foreach ($group_sub_orders as $row) {
                    $group_sub_orderids[] = $row['orderid'];
                    $shipmentids[] = $row['orderid'];
                    $shipmentId2OrdersMap[$row['orderid']] = $row;
                }

                $smarty->assign("group_sub_orderids", $group_sub_orderids);
            }
        } else {
            // this might be a child order itself..in that case set parent id
            $smarty->assign("group_main_orderid", $order['group_id']);
        }


        $return_disabled_styles = \FeatureGateKeyValuePairs::getFeatureGateValueForKey('return.disable.category');
        $return_disabled_styles = explode(",", $return_disabled_styles);
        $weblog->debug(print_r($return_disabled_styles, true));

        $products_sql = "SELECT xod.orderid, xod.gift_card_amount, xod.itemid, product, sp.article_number AS article_number, sp.default_image, productid, product_style, xod.product_type, xod.price, amount, discount AS productDiscount, cart_discount_split_on_ratio As cartDiscount, coupon_discount_product as item_coupon_discount, item_status, quantity_breakup, xod.is_customizable, xod.is_returnable, xod.difference_refund, xod.taxamount, xod.item_loyalty_points_used, cash_redeemed AS item_cashdiscount, pg_discount AS item_pg_discount, cashback as item_cashback, ps.name as productStyleName, ps.label as productStyleLabel, som.sku_id, po.value as size, sp.global_attr_article_type ,sp.global_attr_master_category, sp.global_attr_sub_category, xod.extra_data FROM (((((xcart_order_details xod LEFT JOIN mk_order_item_option_quantity oq on xod.itemid = oq.itemid) LEFT JOIN mk_styles_options_skus_mapping som on oq.optionid = som.option_id) LEFT JOIN mk_style_properties sp on xod.product_style = sp.style_id) LEFT JOIN mk_product_options po on oq.optionid = po.id) LEFT JOIN mk_product_style ps on xod.product_style = ps.id)  where xod.item_status <> 'IC' AND orderid in (" . implode(",", $shipmentids) . ")";
        $all_productsInCart = func_query($products_sql, true);

        $all_skuIds = array();
        $styleIds = array();
        foreach ($all_productsInCart as $key => $product) {
            if ($product['sku_id'])
                $all_skuIds[] = $product['sku_id'];
            $styleIds[] = $product['product_style'];
        }

        $all_sku_details = \SkuApiClient::getSkuDetails($all_skuIds);
        $skuId_to_skudetails_mapping = array();
        foreach ($all_sku_details as $sku_row) {
            $skuId_to_skudetails_mapping[$sku_row['id']] = $sku_row;
        }

        $styleId2TagsMap = getTagsForStyle($styleIds);
        $customize_order = false;

        foreach ($all_productsInCart as $key => $product) {
            $all_productsInCart[$key]['quantity'] = $product['amount'];
            $all_productsInCart[$key]['style_id'] = $product['product_style'];
            $sku_details = $skuId_to_skudetails_mapping[$product['sku_id']];
            $all_productsInCart[$key]['sku_code'] = $sku_details['code'];

            $productTags = $styleId2TagsMap[$product['product_style']];
            $productTagsArr = array();
            foreach ($productTags as $tag) {
                $productTagsArr[] = $tag['name'];
            }
            $all_productsInCart[$key]['tags'] = implode(", ", $productTagsArr);

            if (!empty($product['default_image'])) {
                $all_productsInCart[$key]['designImagePath'] = str_replace("_images", "_images_96_128", $product['default_image']);
                $all_productsInCart[$key]['default_image'] = str_replace("_images", "_images_mini", $product['default_image']);
            }

            if ($product['extra_data'] != '') {
                $customize_order = true;
            }

            $totalPrice = $product['price'] * $product['amount'];
            $productsInCart[$key]['quantity'] = $product['amount'];
            $productsInCart[$key]['totalProductPrice'] = $totalPrice;
            $productsInCart[$key]['discount'] = $product['productDiscount'];
            $productsInCart[$key]['couponDiscount'] = $product['item_coupon_discount'];
            $productsInCart[$key]['totalMyntCashUsage'] = $product['item_cashdiscount'];
            $productsInCart[$key]['taxamount'] = $product['taxamount'];
            $productsInCart[$key]['item_loyalty_points_used'] = $product['item_loyalty_points_used'];
            $productsInCart[$key]['gift_card_amount'] = $product['gift_card_amount'];
            $all_productsInCart[$key]['loyalty_points_conversion_factor'] = $order['loyalty_points_conversion_factor'];
            $product['loyalty_credits'] = $productsInCart[$key]['item_loyalty_points_used'] * $all_productsInCart[$key]['loyalty_points_conversion_factor'];
            $all_productsInCart[$key]['loyalty_credits'] = number_format($product['loyalty_credits'], 2);
            $all_productsInCart[$key]['is_returnable'] = $product['is_returnable'];
            $all_productsInCart[$key]['totalPrice'] = number_format($totalPrice, 2);

            $return_query = "select returnid from xcart_returns where status != 'RRD' and itemid = " . $product['itemid'];
            $return_info = func_query_first($return_query, true);
            $all_productsInCart[$key]['returnid'] = $return_info['returnid'];
            if($order['payment_method'] == 'cod') {
                $all_productsInCart[$key]['amount_paid'] = number_format($totalPrice - $product['gift_card_amount'] - $product['productDiscount'] - $product['cartDiscount'] - $product['item_coupon_discount'] - $product['item_pg_discount'] - $product['item_cashdiscount'] + $product['taxamount'] - $product['loyalty_credits'], 2);
            } else {
                $all_productsInCart[$key]['amount_paid'] = number_format($totalPrice - $product['productDiscount'] - $product['cartDiscount'] - $product['item_coupon_discount'] - $product['item_pg_discount'] - $product['item_cashdiscount'] + $product['taxamount'] - $product['loyalty_credits'], 2);
            }
            $exchange_query = "select ex.exchange_orderid from exchange_order_mappings ex, xcart_orders o where ex.itemid = $product[itemid]
 				and o.orderid=ex.exchange_orderid and o.status not in ('F', 'D', 'L') order by exchange_orderid DESC";
            $exchange_info = func_query_first($exchange_query,true);
            if($exchange_info['exchange_orderid']){
                $exchangeRTOd = func_query_first_cell("select id from mk_old_returns_tracking where orderid = $exchange_info[exchange_orderid]");
                if ($exchangeRTOd){
                    $all_productsInCart[$key]['exchange_orderid'] = '';
                } else {
                    $all_productsInCart[$key]['exchange_orderid'] = $exchange_info['exchange_orderid'];
                }
            }

            //pickup changes
            $all_productsInCart[$key]['pickup_excluded'] = false;

            if($returnable == true){
                //pickup changes
                if(in_array($all_productsInCart[$key]['global_attr_article_type'], $returnPickupExcludedArray)){
                    $all_productsInCart[$key]['pickup_excluded'] = true;
                }
                $all_productsInCart[$key]['returnable'] = 'true';
                if(in_array($product['global_attr_article_type'], $return_disabled_styles) || in_array($product['global_attr_master_category'], $return_disabled_styles) || in_array($product['global_attr_sub_category'], $return_disabled_styles)){
                    $all_productsInCart[$key]['returnable'] = 'false';
                }
                if($all_productsInCart[$key]['is_returnable'] == 0){
                    $all_productsInCart[$key]['returnable'] = 'false';
                }
                if($all_productsInCart[$key]['returnable'] != 'true'){
                    $roles=$XCART_SESSION_VARS['user_roles'];
                    $userRoles = array();
                    foreach($roles as $role){
                        $userRoles[] = $role['role'];
                    }

                    if($all_productsInCart[$key]['is_returnable'] == 0) {
                        $all_productsInCart[$key]['returnable'] = 'false';
                    }
                    if(in_array('CST', $userRoles)){
                        $all_productsInCart[$key]['returnable'] = 'true';
                    }
                }
            }
        }

        foreach($all_productsInCart as $product) {
            $temporder = $shipmentId2OrdersMap[$product['orderid']];
            if(!isset($temporder['items']))
                $temporder['items'] = array();

            $temporder['items'][] = $product;
            $shipmentId2OrdersMap[$product['orderid']] = $temporder;
        }

        if(!empty($shipmentId2OrdersMap))
            $smarty->assign("shipments", $shipmentId2OrdersMap);

        // Get special offer for discount on total
        $grandTotal = $order['subtotal'];
        $coupondiscount = $order['coupon_discount'];
        $cashdiscount = $order['cash_redeemed'];
        $couponCode = $order['coupon'];
        $total = $order['total'];
        $shipcost = $order['shipping_cost'];
        $vatCst = $order['tax'];
        $loyalty_credits = $order['loyalty_points_used']*$order['loyalty_points_conversion_factor'];
        $gift_status = $order['gift_status'];
        $ref_discount = $order['ref_discount'];
        $gift_charges = $order['gift_charges'];
        $payment_method = $order['payment_method'];
        $cod = $order['cod'];
        $pg_discount = $order['pg_discount'];
        $emi_charge = $order['payment_surcharge'];
        $order_cashback = $order['cashback'];
        $gift_card_amount = $order['gift_card_amount'];
        $cartDiscount = $order['cart_discount'];
        $specialofferdiscount = $order['discount'];
        if($payment_method == 'cod')
            $amounttobepaid = $total + $emi_charge - $gift_card_amount;
        else
            $amounttobepaid = $total + $emi_charge;
        $channel=$order_data['order']['channel'];
        if($channel=='telesales'){
            $cclogin=func_query_first_cell("select cclogin from telesales_logs where orderid=$orderid");
            $smarty->assign("channel", $channel);
            $smarty->assign("cclogin", $cclogin);
        }

        if($payment_method == 'cod')
            $amounttobepaid = $amounttobepaid + $cod;

        $amtAfterRefDeducton = $amounttobepaid - $ref_discount;
        $amounttobepaid = number_format($amounttobepaid,2,".",'');

        $smarty->assign("customized_order",$customize_order);
        $smarty->assign("ref_discount", $ref_discount);
        $smarty->assign("gift_status", $gift_status);
        $smarty->assign("gift_charges", $gift_charges);
        $smarty->assign("grandTotal", number_format($grandTotal,2));
        $smarty->assign("specialofferdiscount", number_format($specialofferdiscount,2));
        $smarty->assign("cartDiscount", number_format($cartDiscount,2));
        $smarty->assign("coupondiscount", $coupondiscount);
        $smarty->assign("cashdiscount", $cashdiscount);
        $smarty->assign("couponCode", $couponCode);
        $smarty->assign("amounttobepaid", number_format($amounttobepaid,2));
        $smarty->assign("amtAfterRefDeducton", number_format($amtAfterRefDeducton,2));
        $smarty->assign("shippingcost", number_format($shipcost,2));
        $smarty->assign("vatCst", number_format($vatCst,2));
        $smarty->assign("loyalty_credits", number_format($loyalty_credits,2));
        $smarty->assign("payment_method",$payment_method);
        $smarty->assign("cod", number_format($cod,2));
        $smarty->assign("productsInCart", $all_productsInCart);
        $smarty->assign("orderid", $_GET['orderid']);
        $smarty->assign("trackevent", $_GET['trackevent']);
        $smarty->assign("pg_discount", number_format($pg_discount,2));
        $smarty->assign("emi_charge", number_format($emi_charge,2));
        $smarty->assign("order_cashback", $order_cashback);
        $smarty->assign("gift_card_amount", $gift_card_amount);
        $smarty->assign("show_order_details", "Y");

        //Load delivery estimate customer promise.
        //$delivery_date = date("jS M Y", getOrderDeliveryDateInTimestamp($order['queueddate'], $order['courier_service']));
        /* if($order['queueddate'] && $order['queueddate'] > 0) {
            $fromTime = !empty($order['shippeddate'])?$order['shippeddate']:$order['queueddate'];
            if($order['shipping_method'] == 'XPRESS') {
                $expressDeliveryTime = getDeliveryDateForOrder($fromTime, $all_productsInCart, $order['s_zipcode'], $order['payment_method'],$order['courier_service'], $order['warehouseid'], $orderid);
                if($expressDeliveryTime) {
                    $smarty->assign("express_delivery_date", date("d-m-Y H:i:s", $expressDeliveryTime));
                }
            }

            $delivery_date = date("d-m-Y H:i:s", getDeliveryDateForOrder($fromTime, $all_productsInCart, $order['s_zipcode'], $order['payment_method'],$order['courier_service'], $order['warehouseid']));
            $smarty->assign("expected_delivery_date", $delivery_date);
        } */

        if($order['queueddate'] && $order['queueddate'] > 0) {
            //then show stamped customer promise time
            $isPromiseDateExist = getDeliveryDateForOrder($order['queueddate'], $all_productsInCart, $order['s_zipcode'], $order['payment_method'],$order['courier_service'], $order['warehouseid'],$order['orderid']);
            if($isPromiseDateExist){
                $expectedDeliveryTime = date("d-m-Y H:i:s", $isPromiseDateExist);
            }else{
                $expectedDeliveryTime = date("d-m-Y H:i:s", getDeliveryDateForOrder($order['queueddate'], $all_productsInCart, $order['s_zipcode'], $order['payment_method'],$order['courier_service'], $order['warehouseid']));
            }

            $smarty->assign("expected_delivery_date", $expectedDeliveryTime);
            if(!empty($order['shippeddate'])) {
                // then show shipped date + delivery tat + offset
                $shippedDate = time();
                if ($order['shippeddate'] > 0) {
                    $shippedDate = $order['shippeddate'];
                }
                $shipping_method = $order['shipping_method'];
                $promiseDateRange = \FeatureGateKeyValuePairs::getInteger('lms.tat.promiseDateRange', 2);
                if ($shipping_method === 'XPRESS' || $shipping_method === 'SDD') {
                    $promiseDateRange = 0;
                    if($shipping_method === 'XPRESS'){

                        $shipping_method = 'EXPRESS';
                    }
                }
                $actualPromiseDateQuery = "select `value` from order_additional_info where order_id_fk = ".$orderid." and `key` = 'ACTUAL_PROMISE_TIME'";
                $actualPromiseTime = func_query_first_cell($actualPromiseDateQuery);
                if (!$actualPromiseTime || $actualPromiseTime === '') {
                    $actualPromiseTimeDuration = \TatApiClient::getCachedMinimumTat($order['s_zipcode'], array($order['warehouseid']), $order['courier_service'], $shipping_method) + $promiseDateRange * 24;
                    $actualDeliveryTime = date("d-m-Y H:i:s", $shippedDate + $actualPromiseTimeDuration * 3600);
                } else {
                    $actualDeliveryTime = date('d-m-Y H:i:s', strtotime($actualPromiseTime));
                }
                $smarty->assign("actual_delivery_date", $actualDeliveryTime);
            }
        }

        #
        #Load courier service provider
        #
        $courier_services = get_supported_courier_partners();
        $smarty->assign("courier_services", $courier_services);

        $smarty->assign("admin_login", $login);
        $smarty->assign("amountafterdiscount", $total);
        $smarty->assign("vat", $vatCst);
        $smarty->assign("loyalty_points_used", $loyalty_points_used);
        $smarty->assign("shippingRate", $shipcost);
        $smarty->assign("userinfo", $userinfo);
        //$smarty->assign("cheque_recieved",$_GET['cheque_recieved']);
        # Assign the current location line
        //$smarty->assign("location", $location);
        $order_status_display = func_get_order_status($orderid);

        if ($order_status_display == 'PP' || $order_status_display == 'PV' || $order_status_display == 'D') {
            $codErrorMessage = $this->checkPP2CODAvailibility($orderid, $order_status_display, $orderloginid, $maxcodorderid, $zipcode, $all_productsInCart);
            $smarty->assign("codErrorMessage", $codErrorMessage);
        }

        //Load RTO status
        if (isRTO($orderid)) {
            $smarty->assign("rto_queued", "true");
            $orderData = getOrderReturnData($orderid, 'RTO');
            $all_states = _get_old_rto_states();
            $smarty->assign("rto_state", $all_states[$orderData['currstate']]);
        }

        $sql = "select id,sales_agent from mk_tele_sales where orderid='$orderid'";
        $telesale_data = func_query_first($sql);
        if (!count($telesale_data))
            $smarty->assign("saletype", "DS");
        else {
            $smarty->assign("saletype", "TS");
            $smarty->assign("saleagent", "$telesale_data[sales_agent]");
        }

        //Cancellation section start
        if ($order['orderid'] == $order['group_id']) {
            $cancel_reason_info = func_get_order_cancel_reasons(true);
        } else {
            $cancel_reason_info = func_get_order_cancel_reasons(false);
        }

        $smarty->assign("cancellation_types", $cancel_reason_info['types']);
        $smarty->assign("cancellation_reasons", $cancel_reason_info['type2reasons']);
        // cancellation section end
        // OH Reason
        $oh_reasons = func_query("select * from order_oh_reasons_master where is_manual_change = 1");
        $oh_reason_codes = array();
        foreach ($oh_reasons as $oh_reason) {
            $oh_reason_codes[$oh_reason['id']] = $oh_reason['reason_desc'];
        }
        $smarty->assign("oh_reasons", $oh_reason_codes);

        // oh section start
        if ($order['status'] == 'OH') {
            $smarty->assign("oh_reason", get_oh_reason_for_id($order['on_hold_reason_id_fk']));
            $sql = "select reason,reason_display_name,parent_reason from mk_order_action_reasons where action ='oh_disposition' and is_internal_reason = 0";
            $results = func_query($sql, true);
            $oh_master_disp = array();
            $oh_child_disp = array();
            foreach ($results as $oh_disp) {
                if ($oh_disp['parent_reason'] == null) {
                    $oh_master_disp[$oh_disp['reason']] = $oh_disp['reason_display_name'];
                } else {
                    if ($oh_child_disp[$oh_disp['parent_reason']] == null) {
                        $oh_child_disp[$oh_disp['parent_reason']] = array();
                    }
                    $oh_child_disp[$oh_disp['parent_reason']][$oh_disp['reason']] = $oh_disp['reason_display_name'];
                }
            }
            $sql = "select a.*,moar.reason_display_name as primary_reason from mk_order_action_reasons moar,(select cdd.*,mr.reason_display_name from cod_oh_orders_dispostions cdd,mk_order_action_reasons mr where cdd.reason = mr.reason and  cdd.orderid = $orderid) a where a.disposition = moar.reason";
            $smarty->assign("oh_curr_disp", func_query_first($sql));
            $smarty->assign("oh_master_disp", $oh_master_disp);
            $smarty->assign("oh_child_disp", $oh_child_disp);
        }

        if (isset($_GET['statuserrormsg'])) {
            $smarty->assign('statusErrorMsg', $_GET['statuserrormsg']);
        }

        if (isset($_GET['statussuccessmsg'])) {
            $smarty->assign('statusSuccessMsg', $_GET['statussuccessmsg']);
        }

        $smarty->assign("order_status", "$order_status_display");
    }

    public function checkAvailabilityAndQueueOrder($orderid) {
        global $login;
        $productDetails = func_create_array_products_of_order($orderid);
        $checkInventoryForProducts = array();
        foreach ($productDetails as $row) {
            if ($row['supply_type'] == 'ON_HAND' && $row['seller_id'] == 1) {
                $checkInventoryForProducts[] = $row;
            }
        }

        $itemsAvailable = true;
        if (!empty($checkInventoryForProducts) && count($checkInventoryForProducts) > 0) {
            $itemsAvailable = check_order_item_availability($checkInventoryForProducts);
        }

        if ($itemsAvailable) {
            $updateSql = "UPDATE xcart_orders SET status='Q',queueddate='" . time() . "' WHERE orderid = $orderid";
            db_query($updateSql);
            func_addComment_status($orderid, "Q", 'OH', "Queuing order from OH.", "AUTOSYSTEM");
            EventCreationManager::pushOrderAssignWarehouseEvent($orderid, $login);
            $response = array("Success", "Order Queued Successfully");
        }

        $response = array("Failure", "Order is not serviceable as items are out of Stock");
        return $response;
    }

    public static function stampGovtTax($orderid) {
        global $login;
        $orderResponse = \OrderServiceApiClient::stampGovtTax($orderid, $login);
        if ($orderResponse && $orderResponse['status'] == 'SUCCESS') {
            $response = array("Success", "Successfully stamped govt tax. Please wait for sometime to reflect the status");
        } else {
            $response = array("Failure", "Unable to stamp govt tax. Please contact engineering team");
        }
        return $response;
    }    
    
    public function checkAvailabilityAndQueueRelease($orderid) {
        global $login;
        
        $order_details = func_query_first("select * from xcart_orders where orderid = $orderid", true);
        if( $order_details['shipping_method'] === 'SDD' || $order_details['shipping_method'] === 'XPRESS'){
        	return array("Failure", "This Express Delivery Order cannot be re-queued. It needs to be cancelled");
        }
        
        $products = func_create_array_products_of_order($orderid);
        $checkInventory = true;
        foreach ($products as $row) {
            if ($row['supply_type'] == 'JUST_IN_TIME') {
                $checkInventory = false;
            }
        }
        
        if($checkInventory) {
	        $skuDetails = get_sku_details($products);
	        if (!$skuDetails) {
	            return array("Failure", "Order is not serviceable as items are out of Stock");
	        } else {
	            $skuNotAvailable = check_item_availability($skuDetails, $products);
	            if (sizeof($skuNotAvailable) > 0) {
	                // There are some items that are not available in the warehouse
	                // Construct the mapping for item name to quantity
	                $respMsg = "";
	                foreach ($skuNotAvailable as $itemName => $quantity) {
	                    $respMsg .= "Quantity not available for item: " . $itemName . " is " . $quantity . "\n";
	                }
	                return array("Failure", $respMsg);
	            }
	        }
        }
            
            
		$apiClient = new \ApiClient('omsOrderReleaseServiceUrl', 'assignWarehouse/'.$orderid, false, 'sanjay.yadav');
		$apiClient->getDataForAction();
		if($apiClient->isSuccess()) {
			db_query("update xcart_orders set status = 'Q', queueddate = unix_timestamp(now()) where orderid = ".$orderid);
			self::stampGovtTax($orderid);
                        \EventCreationManager::pushReadyForReleaseEvent($orderid, 'sanjay.yadav');
			func_addComment_status($orderid, "Q", "OH", "Assigned Warehouse successfully. Moving order to Queued from OH", $login);
			return array("Success", "Order Queued Successfully");
		} else {
			return array("Failure", "Unable to Assign warehouse for this order - ". $apiClient->getErrorMsg());
		}
            
            /*
            $order_details = func_query_first("select * from xcart_orders where orderid = $orderid", true);
            if( $order_details['shipping_method'] === 'SDD' || $order_details['shipping_method'] === 'XPRESS'){
                $response = array("Failure", "This Express Delivery Order cannot be re-queued. It needs to be cancelled");
                return $response;
            }
            if ($order_details['ordertype'] == 'ex') {
                $serviceType = "EXCHANGE";
            } else {
                $serviceType = "DELIVERY";
            }
            $serviceabilityResponse = \OrderWHManager::loadServiceabilityDetails($products, $skuDetails, $order_details['s_zipcode'], $order_details['payment_method'], $serviceType);
            if ($serviceabilityResponse['status'] != true) {
                $response = array("Failure", $serviceabilityResponse['reason']);
            } else {
                $whIdToOrderIds = \OrderWHManager::assignWarehouseForOrder($orderid, $order_details, $products, $skuDetails, "Order created from $orderid and moved to WP state", true);
                if ($whIdToOrderIds) {
                    \OrderWHManager::updateInventoryAfterSplit($orderid, $whIdToOrderIds, "Order created from $orderid and moved to WP state", $login);
                    func_addComment_status($orderid, "WP", "OH", "Moving order to WP state from OH", $login);
                    $releaseIdsToPush = array();
                    foreach ($whIdToOrderIds as $whId => $oids) {
                        $releaseIdsToPush = array_merge($releaseIdsToPush, $oids);
                        foreach ($oids as $oid) {
                            self::stampPromiseDate($oid);
                            func_array2update('orders', array('status' => 'WP', 'queueddate' => time()), "orderid = $oid");
                        }
                    }
                    \ReleaseApiClient::pushOrderReleaseToWMS($releaseIdsToPush);
                    \EventCreationManager::pushCompleteOrderEvent($orderid);
                    $response = array("Success", "Order Queued Successfully");
                }
                if (\FeatureGateKeyValuePairs::getBoolean("myntrashoppingfestleaderboard")) {
                    ShopFest::updateLeaderBoardData($order_details['total'] + $order_details['cash_redeemed'], $login, time(), $products, false);
                }
            }
            
            */
    }

    public static function stampPromiseDate($orderId) {
        // Assuming that we have to stamp considering the current time
        // Get all the order details for the particular release
        $order_details = func_query_first("select * from xcart_orders where orderid = $orderId");
        $promiseDateEntries = \TatApiClient::getPromiseDate($order_details['s_zipcode'], $order_details['warehouseid'], $order_details['courier_service'], $order_details['shipping_method']);
        // Stamp the promise date entries into order_additional_info
        //
        // TODO: Combine all the queries into single bulk insert query
        db_query("insert into order_additional_info(order_id_fk,`key`,value,created_on) values"
            . " ($orderId,'EXPECTED_PICKING_TIME','" . $promiseDateEntries['pickByCutOff'] . "',now()),"
            . " ($orderId,'EXPECTED_QC_TIME','" . $promiseDateEntries['qcByCutOff'] . "',now()),"
            . " ($orderId,'EXPECTED_PACKING_TIME','" . $promiseDateEntries['packByCutOff'] . "',now()),"
            . " ($orderId,'EXPECTED_CUTOFF_TIME','" . $promiseDateEntries['shippingCutOff'] . "',now()),"
            . " ($orderId,'CUSTOMER_PROMISE_TIME','" . $promiseDateEntries['promiseDate'] . "',now())");
    }

    public function getItemDetailsForOrder($orderid) {
        $order = func_query_first("select warehouseid from xcart_orders where orderid = $orderid", true);
        $warehouseId = $order['warehouseid'];
        $coreItemDetails = \ItemApiClient::get_skuitems_for_order(trim($orderid), $warehouseId);
        $skuItems = array();
        $skuCodeToItemsCount = array();
        foreach ($coreItemDetails as $item) {
            $skuItem = array();
            $skuItem['id'] = $item['barcode'];
            $skuItem['itemBarCode'] = $item['barcode'];
            $skuItem['skuId'] = $item['sku']['id'];
            $skuItem['skuCode'] = $item['sku']['code'];
            $skuItem['vendorArticleNo'] = $item['sku']['vendorArticleNo'];
            $skuItem['vendorArticleName'] = $item['sku']['vendorArticleName'];
            $skuItem['size'] = $item['sku']['size'];
            $skuItem['itemStatus'] = $item['itemStatus'];
            $skuItems[] = $skuItem;
            if (isset($skuCodeToItemsCount[$item['sku']['code']]))
                $skuCodeToItemsCount[$item['sku']['code']] += 1;
            else
                $skuCodeToItemsCount[$item['sku']['code']] = 1;
        }

        if (count($skuItems) > 0) {
            $skuItems = \LocationApiClient::addLocationInfoForItems($skuItems, $warehouseId);
        }
        return $skuItems;
    }

    private function checkPP2CODAvailibility($orderid, $order_status_display, $orderloginid, $maxcodorderid, $zipcode, $productsInCart) {

        $orderresults = func_query_first("select warehouseid,login, s_country, total as amountpayable, s_zipcode, mobile from xcart_orders where orderid='$orderid'");

        $country = $orderresults['s_country'];
        $amountpayable = $orderresults['amountpayable'];
        $zipcode = $orderresults['s_zipcode'];
        $orderloginid = $orderresults['login'];
        $ordermobile = $orderresults['mobile'];

        $lastpv = "true";

        if ($order_status_display == 'PV') {
            $maxcodorderid = func_query_first_cell("select max(orderid) as maxid from xcart_orders where login='" . $orderloginid . "' and payment_method='cod'");
            if ($maxcodorderid != $orderid) {
                $lastpv = "false";
            }
        }

        $codAllZipCodeEnable = trim(strtolower(\FeatureGateKeyValuePairs::getFeatureGateValueForKey('codEnableAllZipCode')));
        $codAllZipCodeEnable = $codAllZipCodeEnable == "true" ? true : false;
        $codAmountRange = trim(\FeatureGateKeyValuePairs::getFeatureGateValueForKey('cod.limit.range'));
        $codAmountRangeArray = explode("-", $codAmountRange);
        $codMinVal = $codAmountRangeArray[0];
        $codMaxVal = $codAmountRangeArray[1];

        $skuDetails = check_order_item_availability_atp($productsInCart);

        if ($lastpv == "false") {
            $codErrorMessage = "This COD order cannot be confirmed. User has placed another COD order. Order #$maxcodorderid";
        } else if ($country != 'IN') {
            $codErrorMessage = "COD is applicable only for India";
        } else if ($amountpayable < $codMinVal || $amountpayable > $codMaxVal) {
            $codErrorMessage = "COD applicable only for order valued between Rs. $codMinVal to Rs. $codMaxVal";
        } else if (func_check_cod_updaid_order($orderloginid)) {
            // For loginid fraud check, we see if the customer has any cod order, which was sent back.
            // Basically, customer refused to accept the order which they placed online using COD, and then did not pay bluedart for it.
            $codErrorMessage = "COD is disabled for login: $orderloginid";
        } else if (func_cod_check_mobile_number_fraud($ordermobile)) {
            // For mobile fraud check, we see if user has use a mobile no., which was earlier used for any unpaid order (by any loginid)
            $codErrorMessage = "COD is disabled for mobile no. $ordermobile";
        } else {
            $queryOrderAlreadyConfirmed = "select * from cash_on_delivery_info where order_id='" . $orderid . "' and login_id='" . $orderloginid . "' and order_confirmed='Y'";
            $resulCheckOrderConfirmed = func_query_first($queryOrderAlreadyConfirmed);

            if ($resulCheckOrderConfirmed) {
                $codErrorMessage = "COD order was already confirmed but declined later";
            } else {

                $codpendingamount = func_cod_pending_amount($orderloginid);

                $codCreditBalance = $codMaxVal - $codpendingamount;
                if ($codCreditBalance < 0) {
                    $codCreditBalance = 0;
                }

                if (($amountpayable + $codpendingamount) > $codMaxVal) {
                    // User has placed some order currently, and that amount summed with previous COD pending payments, has exceeded codMaxVal.
                    $codErrorMessage = "User has Rs. $codpendingamount pending for COD orders. Allowable COD limit for this user is Rs. $codCreditBalance";
                } else {
                    if (!$skuDetails) {
                        $codErrorMessage = "Order is not serviceable as items are out of Stock";
                    } else if (!func_check_cod_serviceability_products($zipcode, $productsInCart, 'none', false, $skuDetails)) {
                        $codErrorMessage = "Pincode $zipcode for this order, is out of service area for COD option.";
                    }
                }
            }
        }
        return $codErrorMessage;
    }

    private function validateOrderStatusTransition($status, $oldstatus, $ccavenue) {

        $errorMsg = "";

        if($status == 'RTO' || $oldstatus == 'RTO'){
            $errorMsg = "Can't move an order to/from RTO state";
        }
        if ($oldstatus == 'C' && $status != 'L') {
            $errorMsg = "Can't move an order from Completed state to any other state";
        }

        if ($status == 'C' && $oldstatus != 'DL') {
            $errorMsg = "Can't move an order to Complete state without it being Delivered";
        } else if ($status == 'C' && $ccavenue == 'N') {
            $errorMsg = "Please change the status of Cash payment status to Paid";
        }

        if ($status == 'DL') {
            $errorMsg = "Marking Delivered from here is not allowed";
        }

        if ($status == 'SH') {
            $errorMsg = "Marking Shipped from here is not allowed";
        }

        if ($status == 'D') {
            $errorMsg = "Can't move an order to Declined manually";
        }

        if ($oldstatus == 'PV') {
            $errorMsg = "You cannot change order status from COD Pending Verification to any other status";
        }

        if ($oldstatus == 'PP') {
            $errorMsg = "You cannot change order status from Pre-Processed to any other status";
        }

        if ($status == 'Q' && $oldstatus != 'OH') {
            $errorMsg = "You cannot change the status of an order to Queued";
        }

        if ($oldstatus == 'L' && !($status == 'DL' || $status == 'F')) {
            $errorMsg = "You cannot change the status of a Lost order to this status";
        }

        if ($oldstatus != 'WP' && $status == 'WP') {
            $errorMsg = "You cannot change the status of an order to Work in Progress here.\n Please use Item Assignment form to assign items";
        }

        if ($oldstatus == 'OH' && !($status == 'Q' || $status == 'F')) {
            $errorMsg = "You can change from \"On Hold\" status only to Queued/Rejected status";
        }

        if ($status == 'F' && ($oldstatus == 'SH' || $oldstatus == 'DL' || $oldstatus == 'C' || $oldstatus == 'PK' || $oldstatus == 'WP')) {
            $errorMsg = "Orders cannot be cancelled from here. Please use prism";
        }

        if ($status == 'L' && !($oldstatus == 'SH' || $oldstatus == 'DL' || $oldstatus == 'WP' || $oldstatus == 'PK' || $oldstatus == 'C')) {
            $errorMsg = "Orders cannot be marked as Lost from this status";
        }
        return $errorMsg;
    }

    public function updateOHResponse($orderid, $resolution, $disposition, $attemptNo, $attemptTime, $userId, $remarks, $action, $address = false, $phoneNo = false) {

        $remarks = mysql_real_escape_string($remarks);
        $sql = "insert into cod_oh_orders_dispostions_log (refid, orderid, disposition, reason, trial_number, created_by, comment, created_time, logged_time) select id,orderid,disposition,reason,trial_number,created_by,comment,created_time, '$attemptTime' from cod_oh_orders_dispostions where orderid = $orderid";
        func_query($sql);
        $sql = "update cod_oh_orders_dispostions set created_time = '$attemptTime', created_by = '" . $userId . "',comment = '" . $remarks . "',disposition = '" . $resolution . "' , reason = '" . $disposition . "' , trial_number = $attemptNo where orderid = $orderid";
        func_query($sql);

        if (strtolower($action) == 'cancel') {
            if ($resolution == 'NR') {
                $disposition = 'NR';
            }
            $cancel_args = array(
                'cancellation_type' => $resolution,
                'reason' => $disposition,
                'orderid2fullcancelordermap' => array($orderid => true),
                'generateGoodWillCoupons' => false,
                'is_oh_cancellation' => true
            );
            func_change_order_status($orderid, 'F', $userId, "Cancelling order from On Hold Integration Tracker.", '', true, '', $cancel_args);
        }

        if (strtolower($action) == 'modify') {

            $old_address_query = "SELECT CONCAT(s_firstname,' ',s_lastname) customer_name, login, s_address, s_country, s_state, s_city, s_zipcode, mobile FROM xcart_orders WHERE orderid=" . $orderid;
            $old_address = func_query_first($old_address_query, true);

            $updateParams = array();
            $newAddress = '';
            if ($disposition == 'NAV' || $disposition == 'NAPV') {
                //$updateParams['s_firstname'] = mysql_real_escape_string($address['name']);
                $newAddress .= mysql_real_escape_string($address['customer_name']);
                $updateParams['s_address'] = mysql_real_escape_string($address['street']);
                $newAddress .= ", " . mysql_real_escape_string($address['street']);
                $updateParams['s_locality'] = mysql_real_escape_string($address['locality']);
                $newAddress .= ", " . mysql_real_escape_string($address['locality']);
                $updateParams['s_city'] = mysql_real_escape_string($address['city']);
                $newAddress .= ", " . mysql_real_escape_string($address['city']);
                $updateParams['s_state'] = mysql_real_escape_string($address['state']);
                $newAddress .= ", " . mysql_real_escape_string($address['state']);
                /* $updateParams['s_country'] = mysql_real_escape_string($address['country']);
                  $newAddress .= ", ".mysql_real_escape_string($address['country']); */
                $updateParams['s_zipcode'] = mysql_real_escape_string($address['zipcode']);
                $newAddress .= ", " . mysql_real_escape_string($address['zipcode']);

                $updateParams['courier_service'] = '';
                $updateParams['tracking'] = '';
            }

            if ($disposition == 'NAPV' || $disposition == 'NPV') {
                $updateParams['mobile'] = mysql_real_escape_string($phoneNo);
                $newAddress .= ", " . mysql_real_escape_string($phoneNo);
            }
            if (!empty($updateParams)) {
                func_array2update('xcart_orders', $updateParams, "group_id=$orderid");

                \EventCreationManager::pushReleaseUpdateEvent($orderid, $userId, '', '', $address['name'], $address['street'], $address['city'], $address['locality'], $address['state'], $address['zipcode'], $address['country'], $address['phone']);

                $commentdetails = $remarks . "::Old Address->" . mysql_real_escape_string($old_address["customer_name"]) . "," . mysql_real_escape_string($old_address["s_address"]) . "," . mysql_real_escape_string($old_address["s_country"]) . "," . mysql_real_escape_string($old_address["s_state"]) . "," . mysql_real_escape_string($old_address["s_city"]) . "," . mysql_real_escape_string($old_address["s_zipcode"]) . "," . mysql_real_escape_string($old_address["mobile"]) . "," . mysql_real_escape_string($old_address["s_email"]);
                func_addComment_order($orderid, $userId, 'ORDER_UPDATE', 'ADDRESS_CHANGE', 'Shipping address modified' . $commentdetails, $newAddress);
            }
        }

        if (strtolower($action) == 'queue' || strtolower($action) == 'modify') {
            $all_orderids = func_query("Select orderid from xcart_orders where group_id=" . $orderid);
            foreach ($all_orderids as $row) {
                func_change_order_status($row['orderid'], 'Q', $userId, "Queing order from oh integration tracker. for more details refer oh calling log section");
            }
            \OrderProcessingMailSender::notifyCodOnHoldOrderVerified($orderid);
        }

        if ((strtolower($action) == 'reattempt' || strtolower($action) == 'transferred to myntra') && $resolution == 'NR') {
            \OrderProcessingMailSender::notifyCodOnHoldOrderRettempt($orderid);
        }
    }

    function checkCashBackAndQueueOrder($orderid) {
        global $login;
        //$change_status = func_change_order_status($orderid, 'Q', $login ,"Queuing order from OH through admin", "", false);
        $order_data = func_order_data($orderid);
        $order = $order_data["order"];

        $oh_reason1 = get_oh_reason_for_code("CDE", "CNA");
        $oh_reason2 = get_oh_reason_for_code("CDE", "CSD");

        if ($order["on_hold_reason_id_fk"] == $oh_reason1['id'] || $order["on_hold_reason_id_fk"] == $oh_reason2['id']) {
            if (!empty($order["cash_redeemed"])) {
                $trs = new \MyntCashTransaction($order['login'], \MyntCashItemTypes::ORDER, $orderid, \MyntCashBusinessProcesses::CASHBACK_USED, 0, 0, $order['cash_redeemed'], "Usage on order no. $orderid");
                $myntCashUsageResponse = \MyntCashService::debitMyntCashForOrder($trs, $orderid);
                // order status change from PP => Q. If cashback fails, cancell the order with *no cashback return*.
                //myntCashDebitfailureProcedure($orderid,$myntCashUsageResponse,$cancelOrder = true,$continue = false);
                if ($myntCashUsageResponse === \MyntCashService::noEnoughBalanceForDebitRequest || $myntCashUsageResponse === \MyntCashService::serviceNotAvailable) {
                    return array("Failure", "Cashback debit unsuccesful");
                }
            }
        }

        $oh_reason_cb_failure = get_oh_reason_for_code("CSFE");
        if ($order["on_hold_reason_id_fk"] == $oh_reason_cb_failure['id']) {
            $original_order = func_query_first("SELECT ex.*, o.group_id from exchange_order_mappings ex, xcart_orders o where ex.exchange_orderid = $orderid and ex.releaseid = o.group_id");
            $creditTransaction = new \MyntCashTransaction($order['login'], \MyntCashItemTypes::ORDER, $original_order[group_id], \MyntCashBusinessProcesses::PARTIAL_CANCELLATION, 0, 0, 0, "Refund on exchange of order - $original_order[group_id]");
            $debitTransaction = new \MyntCashTransaction($order['login'], \MyntCashItemTypes::ORDER, $orderid, \MyntCashBusinessProcesses::CASHBACK_USED, 0, 0, $order['cash_redeemed'], "Debiting amount for the new exchanged order: $orderid");

            $old_item = reset(\ItemManager::getItemDetailsForItemIds($original_order['itemid'], $original_order['releaseid']));
            $myntCashUsageResponse = \MyntCashService::creditAndDebitMyntCashForExchangeOrder($creditTransaction, $debitTransaction, $orderid, $order['cash_redeemed'], array($old_item,));
            if ($myntCashUsageResponse === \MyntCashService::creditDebitFailure || $myntCashUsageResponse === \MyntCashService::serviceNotAvailable) {
                return array("Failure", "Cashback debit unsuccesful");
            }
        }

        $msg = "Cashback debited Succesfully.";

        $products = func_create_array_products_of_order($orderid);
        $checkInventoryForProducts = array();
        foreach ($products as $row) {
            if ($row['supply_type'] == 'ON_HAND' && $row['seller_id'] == 1) {
                $checkInventoryForProducts[] = $row;
            }
        }

        $itemsAvailable = true;
        if (!empty($checkInventoryForProducts) && count($checkInventoryForProducts) > 0) {
            $itemsAvailable = check_order_item_availability($checkInventoryForProducts);
        }

        if (!$itemsAvailable) {
            $msg.= " Order is not serviceable as items are out of Stock";
            db_query("Update xcart_orders set status = 'OH', on_hold_reason_id_fk = 4 where orderid = $orderid");
            \EventCreationManager::pushOrderUpdateEvent($orderid, "OH", "No inventory for order", time(), 'System', 4);
        } else {
            //$order = func_query_first("select * from xcart_orders where orderid = $orderid", true);
            $updateSql = "UPDATE xcart_orders SET status='Q',queueddate='" . time() . "' WHERE orderid = $orderid";
            db_query($updateSql);
            func_addComment_status($orderid, 'Q', 'OH', 'Cashback Debit Successful. Queuing order.', $login);
            \EventCreationManager::pushOrderUpdateEvent($orderid, 'Q', 'Cashback Debit Successful. Queuing order.', time());
            \EventCreationManager::pushReleaseStatusUpdateEvent($orderid, 'Q', "Cashback Debit Successful. Queuing order.", time(), $login);
            sleep(5);
            \EventCreationManager::pushOrderAssignWarehouseEvent($orderid, $login);
            /* $serviceabilityResponse = \OrderWHManager::loadServiceabilityDetails($products, $skuDetails, $order['s_zipcode'],$order['payment_method']);
              if($serviceabilityResponse['status'] != true) {
              $msg.= " ".$serviceabilityResponse['reason'];
              } else {
              $whIdToOrderIds = \OrderWHManager::assignWarehouseForOrder($orderid, $order, $products, $skuDetails,"Order created from $orderid and moved to WP state", true);
              if($whIdToOrderIds) {
              \OrderWHManager::updateInventoryAfterSplit($orderid, $whIdToOrderIds, "Order created from $orderid and moved to WP state", $login);
              $releaseIdsToPush = array();
              foreach($whIdToOrderIds as $whId=>$oids) {
              $releaseIdsToPush = array_merge($releaseIdsToPush, $oids);
              }
              \ReleaseApiClient::pushOrderReleaseToWMS($releaseIdsToPush);
              \EventCreationManager::pushCompleteOrderEvent($orderid);
              $msg.= " Order Queued Successfully";

              } else {
              $msg .= " Order is not serviceable as items are out of Stock";
              db_query("Update xcart_orders set status = 'OH', on_hold_reason_id_fk = 4 where orderid = $orderid");
              }
              } */
        }
        return array("Success", $msg);
    }

}

?>
