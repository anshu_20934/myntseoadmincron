<?php
/**
 * Abstract Mailer: This should be used in future for writing all Provider mailers
 * Mail Service can be provided by Multiple Providers
 * All Providers (e.g. Myntra Email Provider, Amazon SES Provider, iCubes Mail Provider)
 * should all derive from this class and just implement this method: sendMail
  * @author kundan
 *
 */
abstract class AbstractMailServiceProvider {

	/**
	 * All Provider Email Service Classes should just implement this one method
	 * 
	 * @param unknown_type $mail_detail: this should contain as key=>value:
	 * to, cc, bcc, subject, content, from_name, from_email, from_designation (optional),
	 * template = name of template: if this key is used, then subject, content, from_name, from_designation
	 * header = header template name,footer = footer template name
	 * are overriden from the template which is fetched from Database
	 */
	public abstract function sendMail($mail_detail);
	
}
?>