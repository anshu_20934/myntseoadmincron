<?php
namespace enums\pageconfig;

abstract class PageConfigSectionConstants {
	const StaticImage = "static";
	const SlideshowImage = "slideshow";
	const TextImage = "text";
}
