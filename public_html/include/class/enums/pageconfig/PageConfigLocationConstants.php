<?php
namespace enums\pageconfig;

abstract class PageConfigLocationConstants {
	const HomePage = "home";
	const NewHomePage = "new-home-page";
	const NewHomeFeatures = "new-home-features";
	const NewHomeSubBanners = "new-home-subbanners";
	const MobileHomePage = "mobilehome";
	const LandingPage = "landing";
	const SearchPage = "search";
	const StoryBoard = "storyboard";
	const LoginPage = "login";
	const FashionStory = "fashion-story";
	const TopNavDefault = "topnav-default";
	const FashionTrends = "fashion-trends";
	const TopNavImages = "topnav-images";
	const NewLoginPage = "new-login";
	const NewLoginPageBig = "new-login-big";
}