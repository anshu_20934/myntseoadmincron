<?php
namespace enums\base;
abstract class ActionType {
	const Add = "add";
	const View = "view";
	const Update = "update";
	const Create = "create";
	const Read = "read";
	const Delete = "delete";
	const MoveUp = "moveUp";
	const MoveDown = "moveDown";
}