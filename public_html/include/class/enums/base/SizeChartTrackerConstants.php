<?php
namespace enums\base;
abstract class SizeChartTrackerConstants {
	const OldSizechartCookie = "oldsizechart-styles";
	const NewSizechartCookie = "newsizechart-styles";
	const Delimiter = "|";
	const Expiry = 1;
	const MaxStylesPerCookie = 8;
}
