<?php

namespace enums\cart;

abstract class CartContext {
	const DefaultContext = 'default';
	const TeleSalesContext = 'telesales';
	const SavedContext = 'saved';
	const GiftCardContext = 'giftcard';
	const OneClickContext = 'oneclick';
}
?>