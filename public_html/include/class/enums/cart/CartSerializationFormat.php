<?php

namespace enums\cart;

abstract class CartSerializationFormat {
	const PhpFormat = 'php';
	const JsonFormat = 'json';
	const MJsonFormat = 'mjson';
}
?>