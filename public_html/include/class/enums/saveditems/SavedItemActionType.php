<?php
namespace enums\saveditems;
use enums\base\ActionType;

abstract class SavedItemActionType extends ActionType {
	const MoveItemToCart = "moveToCart";
	const AddItemFromCart = "addFromCart";
	const AddQuantity = "addQuantity";
	const UpdateQuantity = "updateQuantity";
	const GetCartItemCount = "getCartItemCount";
	const GetSavedItemCount = "getSavedItemCount";
	const GetStyleIdsInCart = "getStyleIdsInCart";
	const GetStyleIdsInSaved = "getStyleIdsInSaved";
	const UpdateSkuInSaved = "updateSku";
}