<?php 
namespace enums\revenue\payments;

class PaymentType {
	const Credit = 'creditcards';
	const Debit = 'debitcard';
	const NetBanking = 'netbanking';
	const COD = 'cod';
	const IVR = 'ivr';
	const CashCard = 'cashcard';
	const CreditDebit = 'creditdebitcards';
	const EMI = 'emi';
}
?>