<?php

namespace revenue\payments\enums;

abstract class HDFCResult {
	const Captured = 'CAPTURED';
	const Approved = 'APPROVED'; 
};
?>