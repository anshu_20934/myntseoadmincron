<?php

namespace revenue\payments\enums;

abstract class HDFCResponseStatus {
	const Exception = 6;
	const DualVerificationSucceeded = 5;
	const DualVerificationFailed = 4;
	const PaymentDeclined = 3;
	const Error = 2;
	const IPMismatch = 1;
	const Forwarded = 0;
};
?>