<?php

namespace enums\revenue\payments;

abstract class Gateways {
	const tekProcess = 'tekprocess';
	const AxisBank = 'axis';
	const HDFCBank = 'hdfc';
	const ICICIBank = 'icici';
	const CCAvenue = 'ccavenue';
	const Atom = 'atom';
	const COD = 'cod';
	const EBS = 'ebs';
	const ICICIBank3EMI = 'icici3emi';
	const ICICIBank6EMI = 'icici6emi';
	const CITIBank3EMI = 'citi3emi';
	const CITIBank6EMI = 'citi6emi';
	const CITIBank = 'citi';
}
?>