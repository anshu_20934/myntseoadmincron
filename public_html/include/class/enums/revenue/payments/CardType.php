<?php

namespace enums\revenue\payments;

abstract class CardType {
	const VISA = 'VISA';
	const MasterCard = 'MC';
	const AmEx = 'AMEX';
	const Maestro = 'MAEST';
	const Diners = 'DINERS';
	const None = '';
}
?>
