<?php
##########################################################################
## Author	: Shantanu Bhadoria											##
## Date		: 14 Oct'2008												##
## Copyright Myntra Designs												##
## 																		##
## Myntra mailer class													##
## This class contains all the functions associated with creation of an ##
## mail. 																##
##########################################################################
require_once 'class.myntra_constants.php';
require_once 'class.singleton.php';
class mail{
	function send_mail($to, $subject, $body, $from, $extra_headers=array()) {
		global $config;
		global $current_language;
		global $sql_tbl;

		if (empty($to)) return;

		$from = preg_replace('![\x00-\x1f].*$!sm', '', $from);

		if (X_DEF_OS_WINDOWS) {
			$body = preg_replace("/(?<!\r)\n/S", "\r\n", $body);
			$lend = "\r\n";
		}
		else {
			$lend = "\n";
		}

		$m_from = $from;
		$m_subject = $subject;

		if ($config["Email"]["use_base64_headers"] == "Y") {
			$m_subject = func_mail_quote($m_subject,$charset);
		}

		$headers = array (
			"X-Mailer" => "PHP/".phpversion(),
			"MIME-Version" => "1.0",
			"Content-Type" => "text/plain"
		);

		if (trim($m_from) != "") {
			$headers["From"] = $m_from;
			$headers["Reply-to"] = $m_from;
		}

		$headers_str = "";
		foreach ($headers as $hfield=>$hval)
			$headers_str .= $hfield.": ".$hval.$lend;

		if (preg_match('/([^ @,;<>]+@[^ @,;<>]+)/S', $from, $m))
			@mail($to,$m_subject,$body,$headers_str, "-f".$m[1]);
		else
			@mail($to,$m_subject,$body,$headers_str);
	}
	
	function sendMessage($template, $args, $mailto,$from='',$bcc=''){
		$db = singleton::getmysqlinstance();
		$sql_tbl = myntra_constants::$sql_tbl;
		
		$tb_email_notification  = $sql_tbl["mk_email_notification"];
        $sendMail = true;   
		$template_query = "SELECT * FROM $tb_email_notification WHERE name = '$template'";
		$row = $db->get_row($template_query, 'ARRAY_A');

		$subject = $row['subject'];
		$content = $row['body'];
		$email = trim($mailto);
        
		if(empty($content)){
        	$sendMail = false;
        	########### Mail empty mail template to Ashutosh ######
        	$flag = @mail("alrt_templatenotavailable@myntra.com", "Empty Mail Content template","The template is not available in our DB : ".$template,"From: Myntra Customer Service <support@myntra.com>" . "\n");
        }else{
			$keys = array_keys($args);
			//$key ;
			for($i = 0; $i < count($keys); $i++){
				$key = $keys[$i];
				$content = str_replace("[".$key."]",$args[$key],$content);
			}
			
			if($row['from_name'] != "" & $row['from_designation'] != ""){
				$content = str_replace("[FROM_NAME]", $row['from_name'],$content);
				$content = str_replace("[FROM_DESIGNATION]", $row['from_designation'],$content);
			}
			$content .= "<br/><br/>";
					 
			$headers = "Content-Type: text/html; charset=ISO-8859-1 " . "\n";
			$headers .= "From: Myntra Customer Service <support@myntra.com>" . "\n";
			$headers .= "Bcc: myntramailarchive@gmail.com" . "\n";
			
			if(!empty($bcc))
				 $headers .= "Bcc: $bcc" . "\n"; 
        }
        if($sendMail){
		 	//$flag = @mail($email, $subject, $content,$headers);
			mail::send_mail($email, $subject, $content,'myntra_admin@myntra.com',$headers);
			//mail::send_mail('tulasi.bodasingu@myntra.com', $subject, $content,'myntra_admin@myntra.com',$headers);
        }
		//return $flag;
	}
}

/**
 * added by arun to send 
 * mail with multiple attachments, 
 * multiple (to,cc,bcc) and mail
 * content can be plain text or html
 * document(see commented code below)=> tells how to make
 * use of classs AbstratMail,AttachmentMail(to send attachment mail) and 
 * Multipart(object representation of the file attached) * 
 */
//### example to send an attachment mail ###//
	/*
	require("class.mail.php");
		
	$to = "you@company.com";
	$addTo = "other@company.com";
	$msgOK = "Send OK";
	$msgFAILED = "Send FAILED";
	$subject = $message = "Nothing";	
	
	// Send mails with  attachments in several formats
	// Send 1: High priority and content in HTML (already have <html> tags)
	// Send 2: Normal priority and content in complete HTML (without have <html> tags)
	// Send 3: Low Priority and content in text format
	
	//$mail2 = new AttachmentMail(receiver, subject, from name, from email);
	$mail2 = new AttachmentMail($to, $subject, "", "aaa@aaa.com.br");
	
	$mp1 = new Multipart("attach/phpconference.jpg");
	$mail2->addAttachment($mp1);
	$mail2->addAttachment(new Multipart("attach/php.gif"));
	
	$mail2->addTo($addTo);//multiple receivers 
	$mail2->setBodyHtml("<h1>".$message."</h1>");
	$mail2->setPriority(AbstractMail::HIGH_PRIORITY);
	if ($mail2->send())
		echo $msgOK;
	else
		echo $msgFAILED;
	
	$mail2->resetHeaders();
	$mail2->setHtml("<h1>".$message."</h1>");
	$mail2->setPriority(AbstractMail::NORMAL_PRIORITY);
	if ($mail2->send())
		echo $msgOK;
	else
		echo $msgFAILED;
	
	$mail2->resetHeaders();
	$mail2->setBodyText($message);
	$mail2->setPriority(AbstractMail::LOW_PRIORITY);
	if ($mail2->send())
		echo $msgOK;
	else
		echo $msgFAILED;*/
//### example to send an attachment mail ends ###//

/**
 * Abstract class used for email sender implementation classes
 *
 * @package			mail
 * @author			gustavo.gomes
 * @copyright		2006
 */
abstract class AbstractMail {
	
	const CRLF = "\n";
	
	const HIGH_PRIORITY = 2;
	const NORMAL_PRIORITY = 3;
	const LOW_PRIORITY = 4;
	
	protected $to = array();
	protected $cc = array();
	protected $bcc = array();
	protected $fromName;
	protected $fromMail;
	protected $subject;
	
	protected $contentType;
	protected $charset;
	
	protected $priority = 2;
	
	protected $headers = array();
	protected $body;

	public function __construct($to, $subject, $fromName="", $fromMail="") {
		$this->to[] = $to;
		$this->subject = $subject;
		$this->fromName = $fromName;
		$this->fromMail = $fromMail;
		$this->init();
	}
	
	private function init() {
		if ($this->fromName != "" && $this->fromMail != "") {
			$this->addHeader("From: ".$this->fromName." <".$this->fromMail.">");
			$this->addHeader("Reply-To: ".$this->fromName." <".$this->fromMail.">");
		} else if ($this->fromName == "" && $this->fromMail != "") {
			$this->addHeader("From: ".$this->fromMail);
			$this->addHeader("Reply-To: ".$this->fromMail);
		}
	}
	
	public function getPriority() {
		return $this->priority;
	}
	
	public function setPriority($priority) {
		$this->priority = $priority;
	}
	
	public function getContentType() {
		return $this->contentType;
	}
	
	public function getCharset() {
		return $this->charset;
	}
	
	public function addTo($mail) {
		if(self::validateAddress($mail))//added by arun
			$this->to[] = $mail;
	}

	public function addCC($mail) {
		//added by arun for multiple cc
		if(self::validateAddress($mail))
			$this->cc[] = $mail;
		
		//$this->addHeader("CC:".$mail);
	}

	public function addBCC($mail) {
		//added by arun for multiple bcc
		if(self::validateAddress($mail))
			$this->bcc[] = $mail;
		
		//$this->addHeader("BCC:".$mail);
	}

	public function addHeader($header) {
		$this->headers[] = $header;
	}
	
	protected function buildTo() {
		return implode(", ",$this->to);
	}
	
	protected function buildHeaders() {
		$headers = "";
		
		//added by arun to build cc and bcc
		$this->addHeader("CC:".implode(",",$this->cc));
		$this->addHeader("BCC:".implode(",",$this->bcc));
		
		if (count($this->headers) > 0) {
			for ($i = 0;$i < count($this->headers)-1;$i++)
				$headers .= $this->headers[$i].self::CRLF;
			$headers .= $this->headers[$i];
		}
		return $headers;
	}
	
	protected function createMessageHeaders($contentType, $encode="") {
		$out = "";
		if ($encode != "")
			$out .= "Content-type:".$contentType."; charset=".$encode;
		else
			$out .= "Content-type:".$contentType.";";
		return $out;
	}

	public static function validateAddress($mailadresse) {
		if (!preg_match("/[a-z0-9_-]+(\.[a-z0-9_-]+)*@([0-9a-z][0-9a-z-]*[0-9a-z]\.)+([a-z]{2,4})/i",$mailadresse))
			return false;
		return true;
	}
	
	public function resetHeaders() {
		$this->headers = array();
		$this->init();
	}
	
	public abstract function setBodyHtml($html, $charset="iso-8859-1");

	public abstract function setHtml($html, $charset="iso-8859-1");

	public abstract function setBodyText($text);
	
	public abstract function send();
	
}


/**
 * Class to send emails with attachments in Text and HTML formats
 *
 * @package			mail
 * @author			gustavo.gomes
 * @copyright		2006
 */
class AttachmentMail extends AbstractMail {

	private $uidBoundary;
	
	private $delimiter;
	
	private $contentTransferEncode = "7bit";

	private $attachment = array();

	public function __construct($to, $subject, $fromName="", $fromMail="") {
		// Create a unique id boundary
		$this->uidBoundary = "_".md5(uniqid(time()));
		$this->delimiterBoundary = "--".$this->uidBoundary.self::CRLF;

		parent::__construct($to, $subject, $fromName, $fromMail);
	}

	public function setBodyHtml($html, $charset="iso-8859-1") {
		$this->contentType = "text/html";
		$this->charset = $charset;
		$this->body = $this->createMessageHeaders("text/html",$charset);
		$this->body .= "<html><head>";
		$this->body .= "<meta http-equiv=Content-Type content=\"text/html; charset=".$charset."\">";
		$this->body .= "</head><body>";
		$this->body .= nl2br($html)."";
		$this->body .= "</body></html>";
		$this->body .= self::CRLF.self::CRLF;
	}
	
	public function setHtml($html, $charset="iso-8859-1") {
		$this->contentType = "text/html";
		$this->charset = $charset;
		$this->body = $this->createMessageHeaders("text/html",$charset);
		$this->body .= nl2br($html)."".self::CRLF.self::CRLF;
	}

	public function setBodyText($text) {
		$this->contentType = "text/plain";
		$this->charset = "";
		$this->body = $this->createMessageHeaders("text/plain");
		$this->body .= $text.self::CRLF.self::CRLF;
	}
	
	protected function createMessageHeaders($contentType, $encode="") {
		$out = $this->delimiterBoundary;
		$out .= parent::createMessageHeaders($contentType, $encode);
		$out .= "Content-Transfer-Encoding: ".$this->contentTransferEncode.self::CRLF.self::CRLF;
		return $out;
	}
	
	public function addAttachment($part) {
		$this->attachment[] = $part;
	}

	public function send() {
		$this->addHeader("MIME-Version: 1.0");
		$this->addHeader("X-Mailer: Attachment Mailer ver. 1.0");
		$this->addHeader("X-Priority: ".$this->priority);
		$this->addHeader("Content-type: multipart/mixed;".self::CRLF.chr(9)." boundary=\"".$this->uidBoundary."\"".self::CRLF);
		$this->addHeader("This is a multi-part message in MIME format.");
		$headers = $this->buildHeaders();
		return mail($this->buildTo(),
					$this->subject,
					$this->body.$this->createAttachmentBlock(),
					$headers);
	}
	
	private function createAttachmentBlock() {
		$block = "";
		
		if (count($this->attachment) > 0) {
			$block .= $this->delimiterBoundary;
			for ($i = 0;$i < count($this->attachment);$i++) {
				$block .= $this->attachment[$i]->getContent();
				$block .= $this->delimiterBoundary;
			}
		}
		return $block;
	}
}

/**
 * Representation for a attachment
 *
 * @package			mail
 * @author			gustavo.gomes
 * @copyright		2006
 */
class Multipart {
	
	const DISPOSITION_ATTACHMENT = "attachment";
	const DISPOSITION_INLINE = "inline";
	
	private $content;
	
	private $disposition;
	
	public function __construct($file="",$disposition="attachment") {
		if ($file != "")
			$this->setContent($file, $disposition);
	}
	
	public function getContent() {
		return $this->content;
	}
	
	public function getDisposition() {
		return $this->disposition;
	}
	
	/**
	 * Use for $dispoition "attachment" or "inline"
	 * (f.e. example images inside a html mail
	 * 
	 * @param	file	string - nome do arquivo
	 * @param	disposition	string
	 * @return	boolean
	 */
	public function setContent($file, $disposition = "attachment") {
		$this->disposition = $disposition;
		$fileContent = $this->getFileData($file);
		if ($fileContent != "") {
			$filename = basename($file);
			//$fileType = mime_content_type($file);
			$fileType = self::getMimeType($file);
			$chunks = chunk_split(base64_encode($fileContent));
			
			$mailPart = "";
			if ($fileType)
				$mailPart .= "Content-type:".$fileType.";".AbstractMail::CRLF.chr(9)." name=\"".$filename."\"".AbstractMail::CRLF;
			$mailPart .= "Content-length:".filesize($file).AbstractMail::CRLF;
			$mailPart .= "Content-Transfer-Encoding: base64".AbstractMail::CRLF;
			$mailPart .= "Content-Disposition: ".$disposition.";".chr(9)."filename=\"".$filename."\"".AbstractMail::CRLF.AbstractMail::CRLF;
			$mailPart .= $chunks;
			$mailPart .= AbstractMail::CRLF.AbstractMail::CRLF;
			$this->content = $mailPart;
			return true;
		}			
		return false;
	}
	
	private function getFileData($filename) {
		if (file_exists($filename))
			return file_get_contents($filename);
		return "";
	}
	
	public static function getMimeType(&$file) {
		$type = false;
		// Fileinfo documentation says fileinfo_open() will use the
		// MAGIC env var for the magic file
		if (extension_loaded('fileinfo') && isset($_ENV['MAGIC']) &&
		($finfo = finfo_open(FILEINFO_MIME, $_ENV['MAGIC'])) !== false) {
			if (($type = finfo_file($finfo, $file)) !== false) {
				// Remove the charset and grab the last content-type
				$type = explode(' ', str_replace('; charset=', ';charset=', $type));
				$type = array_pop($type);
				$type = explode(';', $type);
				$type = array_shift($type);
			}
			finfo_close($finfo);

		// If anyone is still using mime_content_type()
		} elseif (function_exists('mime_content_type'))
			$type = mime_content_type($file);

		if ($type !== false && strlen($type) > 0) return $type;

		// Otherwise do it the old fashioned way
		static $exts = array(
			'jpg' => 'image/jpeg', 'gif' => 'image/gif', 'png' => 'image/png',
			'tif' => 'image/tiff', 'tiff' => 'image/tiff', 'ico' => 'image/x-icon',
			'swf' => 'application/x-shockwave-flash', 'pdf' => 'application/pdf',
			'zip' => 'application/zip', 'gz' => 'application/x-gzip',
			'tar' => 'application/x-tar', 'bz' => 'application/x-bzip',
			'bz2' => 'application/x-bzip2', 'txt' => 'text/plain',
			'asc' => 'text/plain', 'htm' => 'text/html', 'html' => 'text/html',
			'xml' => 'text/xml', 'xsl' => 'application/xsl+xml',
			'ogg' => 'application/ogg', 'mp3' => 'audio/mpeg', 'wav' => 'audio/x-wav',
			'avi' => 'video/x-msvideo', 'mpg' => 'video/mpeg', 'mpeg' => 'video/mpeg',
			'mov' => 'video/quicktime', 'flv' => 'video/x-flv', 'php' => 'text/x-php'
		);
		$ext = strToLower(pathInfo($file, PATHINFO_EXTENSION));
		return isset($exts[$ext]) ? $exts[$ext] : 'application/octet-stream';
	}
}
?>
