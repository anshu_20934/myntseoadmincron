<?php
namespace macros;

class PageMacros {
    
    protected $pageMacros;
    
    public function __construct($pagetype=''){
        switch($pagetype){
            case 'PDP': $this->pageMacros = Macros::getFor('PDP');
                break;
            case 'SEARCH': $this->pageMacros = Macros::getFor('SEARCH');
                break;
            default: $this->pageMacros = Macros::getFor('DEFAULT');
                break;
        }
    }

    public function __get($name)
    {
        if (array_key_exists($name, $this->pageMacros)) {
            return $this->pageMacros[$name];
        } else {

        }
    }
    
    public function __set($key,$value){
        if(array_key_exists($key, $this->pageMacros)){
            $this->pageMacros[$key] = $value;
        } else {

        }
    }

    public function toArray(){
        return $this->pageMacros;
    }
}
