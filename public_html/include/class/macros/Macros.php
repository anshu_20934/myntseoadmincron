<?php
namespace macros;

class Macros {
    private static $baseMacros = array(
            'url'=>null,
            'page_type'=>null,
            'page_name'=>null,
        );

    private static $pdpMacros = array(
            'style_id'=>null,
            'vendor_id'=>null,
            'name'=>null,
            'brand'=>null,
            'article_type'=>null,
            'sub_category'=>null,
            'category'=>null,
            'gender'=>null,
            'image'=>null,
            'search_image'=>null,
            'price'=>null,
            'discount'=>null,
            'discounted_price'=>null,
            'discount_string'=>null,
            'in_stock' => null,
			'colour' => null,
        );
    private static $searchMacros = array(
			'search_string' => null,
			'category' => null,
			'sub_category' => null,
			'article_type' => null,
			'brand' =>  null,
			'gender' => null,
			'colour' => null,
			'size' => null,
        );

    public static function getFor($pageKey) {
        $macros = self::$baseMacros;

        switch($pageKey){
            case 'PDP': $macros = array_merge($macros, self::$pdpMacros);
                break;
            case 'SEARCH': $macros = array_merge($macros, self::$searchMacros);
                break;
            default:
                break;
        }
        return $macros;
    }
}

?>
