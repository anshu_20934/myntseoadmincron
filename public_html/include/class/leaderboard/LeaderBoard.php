<?php

namespace leaderboard;

include_once($xcart_dir."/Cache/Cache.php");
include_once("$xcart_dir/Cache/CacheKeySet.php");
use leaderboard\LeaderBoardCacheSetter;

class LeaderBoard {
	
	public function getApplicableLeaderBoardToUser($login){
		if($login !=null && $login != ""){
			$leaderboards = func_query("select lb.*,lbc.rule,lbc.rule_string from leader_board lb, leader_board_criteria lbc where lb.id = lbc.lb_fk_id  and lb.is_enabled = 1 and lb.display_start_time < unix_timestamp(now()) and lb.display_end_time > unix_timestamp(now())",true);
			foreach($leaderboards as $leaderboard){
				if($leaderboard['rule'] == 'list'){
					$exists = func_query_first("select * from leader_board_users where lb_fk_id = $leaderboard[id] and login = '$login'");
					if($exists){
						return $leaderboard['id'];
					}
				}else{
					/*$sub_queries = explode("___",$leaderboard['rule_string']);
					foreach($sub_queries as $sub_query){
						$part1 = explode(".",$sub_query);
						$attr = $part1[0];
						$part2 = explode(":",$part1[1]);
						$comparator = $part2[0];
						$value = $part2[1];
						switch ($attr) {
							case 'LOGIN':
								
								break;
								
							default:
								break;
						}
					}*/
					$exists = func_query_first("select * from xcart_customers where $leaderboard[rule_string]",true);
					if(!$exists){
						return $leaderboard['id'];
					}
				}				   
			}
		}	   
		return null;
	}
	
	public function getLeaderBoardInterval( $startTime, $endTime, $refreshDuration = 0 ) {
		$interval = array( 'startTime' => $startTime, 'endTime' => $endTime );
		if (!empty($refreshDuration)) {
			$currentTime    = time();
			$currentInteval = floor(($currentTime - $startTime) / $refreshDuration);
			$newStartTime   = $startTime + $currentInteval * $refreshDuration;
			$newEndTime     = $startTime + ($currentInteval + 1) * $refreshDuration;
			
			$interval[ 'startTime' ] = $newStartTime;
			$interval[ 'endTime' ]   = $newEndTime;
 		}
		return $interval;
	}

	public function getUserPurchasesForLeaderBoard($login,$lb_id){
		$leaderboard = func_query_first("select lb.*,lbc.rule, lbc.rule_string from leader_board lb, leader_board_criteria lbc where lb.id = lbc.lb_fk_id  and lb.id = $lb_id", true);
		$leaderboardInterval = self::getLeaderBoardInterval($leaderboard['start_time'], $leaderboard['end_time'], $leaderboard['refresh_duration']);
		if($leaderboard){
			$purchaseSummary = func_query_first("SELECT concat(c.firstname, ' ', c.lastname) AS username, xo.login, sum(total+cod+gift_charges+cash_redeemed) AS total, count(DISTINCT group_id) AS noOfOrders FROM xcart_orders xo, xcart_customers c WHERE c.login = xo.login AND xo.login = '$login' AND date >= $leaderboardInterval[startTime] and date <= $leaderboardInterval[endTime] AND xo.status IN ('OH','Q','WP','SH','DL','C','PK') GROUP BY xo.login", true);
			return $purchaseSummary;
		}   
		return false;
	}
	
	public function getTrendingStylesForLeaderBoard($lb_id){
		$cache = self::getLeaderBoardCache($lb_id);
		$trending = $cache->getValue(\LeaderBoardCacheKeys::$trending_styles);
		return $trending;
	}
	
	public function getTickerMessageForLeaderBoard($lb_id){
		$msg = func_query_first_cell("select message from leader_board_ticker_messages where lb_fk_id = $lb_id order by id desc limit 1", true);
		if(!$msg){
			return "Welcome !";
		}
		return $msg;
	}

	public function getAllConfigurableFields( $lb_id ) {
		return func_query_first("select * from leader_board_ticker_messages where lb_fk_id = $lb_id order by id desc limit 1", true);
	}

	protected function getLeaderBoardCache($lb_id) {
		$cache = new \Cache(\LeaderBoardCacheKeys::$prefix.$lb_id, \LeaderBoardCacheKeys::keySet());
		return $cache;
	}
	
	public function getTopNBuyersForLeaderBoard($lb_id){
		$cache = self::getLeaderBoardCache($lb_id);
		$top10Buyers = $cache->getValue(\LeaderBoardCacheKeys::$top10Buyers);
		return $top10Buyers;
	}

	public function getTopNMenBuyersForLeaderBoard($lb_id){
		$cache = self::getLeaderBoardCache($lb_id);
		$top10MenBuyers = $cache->getValue(\LeaderBoardCacheKeys::$top10MenBuyers);
		return $top10MenBuyers;
	}
	
	public function getTopNWomenBuyersForLeaderBoard($lb_id){
		$cache = self::getLeaderBoardCache($lb_id);
		$top10WomenBuyers = $cache->getValue(\LeaderBoardCacheKeys::$top10WomenBuyers);
		return $top10WomenBuyers;
	}

	public function getUserRank($login,$lb_id){
		return 42;
	}
	
	public static function createNewLeaderBoard($leaderboard) {
		
	}
	
	public static function updateLeaderBoardTickerMessage($board_id, $message) {
	
	}
	
	public static function getAllLeaderBoards() {
		$leaderboards = func_query("Select * from leader_board as lb inner join leader_board_criteria as lbc on lb.id = lbc.lb_fk_id where is_enabled = 1", true);
		return $leaderboards;
	}

	public static function getLeaderBoardMetaData($lb_id) {
		$cache = self::getLeaderBoardCache($lb_id);
		$metaData = $cache->get(function($lb_id){
				$metaData = func_query_first("Select * from leader_board as lb inner join leader_board_criteria as lbc on lb.id = lbc.lb_fk_id where lb.id = $lb_id and is_enabled = 1", true);
				return $metaData;
		}, array($lb_id), \LeaderBoardCacheKeys::$meta,1800);
		return $metaData;
	}
	
	public static function getAllLeaderBoardFields() {
		$leaderboardmessages = func_query("Select * from leader_board_ticker_messages", true);
		$leaderboard2Messages = array();
		foreach($leaderboardmessages as $message) {
			$leaderboard2Messages[$message['lb_fk_id']] = $message;
		}
		return $leaderboard2Messages;
	}
	
	public static function updateLeaderBoardFields($lbId, $tickerMessage, $pageTitle, $bannerUrl, $bannerHref) {
		$updateArray = array('message'=>$tickerMessage, 'title'=>$pageTitle, 'banner_url'=>$bannerUrl, 'banner_href'=>$bannerHref);
		func_array2update('leader_board_ticker_messages', $updateArray, "lb_fk_id = $lbId");
		
		//$cache = self::getLeaderBoardCache($lbId);
		//$cache->invalidate();
	}
	
}

?>

