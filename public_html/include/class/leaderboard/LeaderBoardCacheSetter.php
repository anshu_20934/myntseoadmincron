<?php

namespace leaderboard;

include_once($xcart_dir."/Cache/Cache.php");
include_once("$xcart_dir/Cache/CacheKeySet.php");

class LeaderBoardCacheSetter extends LeaderBoard{

    /** This needs to be called from the cron **/
    public function setCacheForTrendingStyles($lb_id, $expiry){
        $cache = self::getLeaderBoardCache($lb_id);
        $value = self::getTrendingStylesForLeaderBoardFromDB($lb_id);
        $cache->setVal(\LeaderBoardCacheKeys::$trending_styles, $value, $expiry);
    }
    
    /** This needs to be called from the cron **/ 
    public function setCacheForTopNBuyers($lb_id, $expiry){
        $cache = self::getLeaderBoardCache($lb_id);

        $overallLeaders = self::getTopNBuyersForLeaderBoardFromDB($lb_id);
        $cache->setVal(\LeaderBoardCacheKeys::$top10Buyers, $overallLeaders, $expiry);

        $menLeaders = self::getTopNMenBuyersForLeaderBoardFromDB($lb_id);
        $cache->setVal(\LeaderBoardCacheKeys::$top10MenBuyers, $menLeaders, $expiry);

        $womenLeaders = self::getTopNWomenBuyersForLeaderBoardFromDB($lb_id);
        $cache->setVal(\LeaderBoardCacheKeys::$top10WomenBuyers, $womenLeaders, $expiry);

    }
        
	private function getTrendingStylesForLeaderBoardFromDB($lb_id){
		$leaderboard = func_query_first("select lb.*,lbc.rule,lbc.rule_string from leader_board lb, leader_board_criteria lbc where lb.id = lbc.lb_fk_id  and lb.id = $lb_id",true);
		$startTime = $leaderboard['start_time'];
		$endTime = $leaderboard['end_time'];
		if($leaderboard){
			if($leaderboard['rule'] == 'list'){
				$products = func_query("select sp.style_id as styleId,sp.product_display_name as style_name, sp.default_image from mk_style_properties sp, (Select sku_id, sum(od.amount) as totalItems from xcart_orders o, xcart_order_details od, mk_order_item_option_quantity oq, mk_styles_options_skus_mapping som,leader_board_users lb where o.login = lb.login and lb.lb_fk_id = $lb_id and od.orderid = o.orderid and od.itemid = oq.itemid and oq.optionid = som.option_id and o.date >= $startTime and o.date <= $endTime and o.status in ('F','OH','Q','WP','SH','DL','C') group by sku_id order by totalItems desc limit 5) b, mk_styles_options_skus_mapping som where som.sku_id = b.sku_id and som.style_id = sp.style_id", true);
			}else{
				$products = func_query("select sp.style_id as styleId,sp.product_display_name as style_name, sp.default_image from mk_style_properties sp, (Select sku_id, sum(od.amount) as totalItems from xcart_orders o, xcart_order_details od, mk_order_item_option_quantity oq, mk_styles_options_skus_mapping som where od.orderid = o.orderid and od.itemid = oq.itemid and oq.optionid = som.option_id and o.date >= $startTime and o.date <= $endTime and o.status in ('F','OH','Q','WP','SH','DL','C') group by sku_id order by totalItems desc limit 5) b, mk_styles_options_skus_mapping som where som.sku_id = b.sku_id and som.style_id = sp.style_id", true);
			}	
		}
		foreach($products as $index=>$product) {
			$products[$index]['imagepath'] = str_replace("and _images", "_images_48_64_mini", $product['default_image']);
		}
		return $products;
	}

	private function getTopNBuyersForLeaderBoardFromDB($lb_id) {
		$leaderboard = func_query_first("select lb.*,lbc.rule,lbc.rule_string from leader_board lb, leader_board_criteria lbc where lb.id = lbc.lb_fk_id  and lb.id = $lb_id",true);
		$leaderboardInterval = self::getLeaderBoardInterval($leaderboard['start_time'], $leaderboard['end_time'], $leaderboard['refresh_duration']);		
		$startTime = $leaderboardInterval['startTime'];
		$endTime = $leaderboardInterval['endTime'];
		if($leaderboard){
			if($leaderboard['rule'] == 'list'){
				$results = func_query("select o.login, concat(c.firstname,' ',c.lastname) as username, sum(total+cod+gift_charges+cash_redeemed) as totalAmount, count(distinct orderid) as orders from xcart_orders o use index(order_date), xcart_customers c, leader_board_users lb where o.login = lb.login and lb.lb_fk_id = $lb_id and o.login = c.login and date >= $startTime and date <= $endTime and o.status in ('OH','Q','WP','SH','DL','C','PK') group by o.login order by totalAmount desc limit 10", true);
			}else{
				$results = func_query("select o.login, concat(c.firstname,' ',c.lastname) as username, sum(total+cod+gift_charges+cash_redeemed) as totalAmount, count(distinct orderid) as orders from xcart_orders o use index(order_date), xcart_customers c where o.login = c.login and date >= $startTime and date <= $endTime and o.status in ('OH','Q','WP','SH','DL','C') group by o.login order by totalAmount desc limit 10", true);
			}	
		}
	 	return $results;
	}	

	private function getTopNMenBuyersForLeaderBoardFromDB($lb_id) {
		$leaderboard = func_query_first("select lb.*,lbc.rule,lbc.rule_string from leader_board lb, leader_board_criteria lbc where lb.id = lbc.lb_fk_id  and lb.id = $lb_id",true);
		$leaderboardInterval = self::getLeaderBoardInterval($leaderboard['start_time'], $leaderboard['end_time'], $leaderboard['refresh_duration']);		
		$startTime = $leaderboardInterval['startTime'];
		$endTime = $leaderboardInterval['endTime'];
		if($leaderboard){
			if($leaderboard['rule'] == 'list'){
				$results = func_query("select o.login, concat(c.firstname,' ',c.lastname) as username, sum(total+cod+gift_charges+cash_redeemed) as totalAmount, count(distinct orderid) as orders from xcart_orders o use index(order_date), xcart_customers c, leader_board_users lb where o.login = lb.login and lb.lb_fk_id = $lb_id and o.login = c.login and date >= $startTime and date <= $endTime and o.status in ('OH','Q','WP','SH','DL','C','PK') and (c.gender = 'M' or c.gender is NULL) group by o.login order by totalAmount desc limit 10", true);
			}else{
				$results = func_query("select o.login, concat(c.firstname,' ',c.lastname) as username, sum(total+cod+gift_charges+cash_redeemed) as totalAmount, count(distinct orderid) as orders from xcart_orders o use index(order_date), xcart_customers c where o.login = c.login and date >= $startTime and date <= $endTime and o.status in ('OH','Q','WP','SH','DL','C') and (c.gender = 'M' or c.gender is NULL) group by o.login order by totalAmount desc limit 10", true);
			}	
		}
	 	return $results;
	}	

	private function getTopNWomenBuyersForLeaderBoardFromDB($lb_id) {
		$leaderboard = func_query_first("select lb.*,lbc.rule,lbc.rule_string from leader_board lb, leader_board_criteria lbc where lb.id = lbc.lb_fk_id  and lb.id = $lb_id",true);
		$leaderboardInterval = self::getLeaderBoardInterval($leaderboard['start_time'], $leaderboard['end_time'], $leaderboard['refresh_duration']);		
		$startTime = $leaderboardInterval['startTime'];
		$endTime = $leaderboardInterval['endTime'];
		if($leaderboard){
			if($leaderboard['rule'] == 'list'){
				$results = func_query("select o.login, concat(c.firstname,' ',c.lastname) as username, sum(total+cod+gift_charges+cash_redeemed) as totalAmount, count(distinct orderid) as orders from xcart_orders o use index(order_date), xcart_customers c, leader_board_users lb where o.login = lb.login and lb.lb_fk_id = $lb_id and o.login = c.login and date >= $startTime and date <= $endTime and o.status in ('OH','Q','WP','SH','DL','C','PK') and c.gender = 'F' group by o.login order by totalAmount desc limit 10", true);
			}else{
				$results = func_query("select o.login, concat(c.firstname,' ',c.lastname) as username, sum(total+cod+gift_charges+cash_redeemed) as totalAmount, count(distinct orderid) as orders from xcart_orders o use index(order_date), xcart_customers c where o.login = c.login and date >= $startTime and date <= $endTime and o.status in ('OH','Q','WP','SH','DL','C') and c.gender = 'F' group by o.login order by totalAmount desc limit 10", true);
			}	
		}
	 	return $results;
	}		

}

?>