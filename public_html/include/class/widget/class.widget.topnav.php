<?php
require_once("$xcart_dir/include/class/widget/interface.widget.php");

class WidgetTopNavigation implements WidgetInterface {
	
	private $tableName = 'mk_widget_top_nav';
	public static $WIDGETNAME = 'TopNavigation';
	public static $XCACHEID   = 'TopNavigationWidgetData';

	public function setWidgetName($widgetname) {
		$this->_widgetname = self::$WIDGETNAME;
	}
	
	public function getWidgetID() {
		return null;
	}
	
	public function setWidgetID($widgetid) {
		// Does nothing here.
	}
	
	public function getData($nocache_param=false) {
		global $xcache, $nocache;
                if(!$nocache_param && $nocache != 1){
		    // Fetch top navigation data from xcache
		    $xcachedTopNavigationData = $xcache->fetch(self::$XCACHEID);
	
		    if($xcachedTopNavigationData!=null) {
			    // If data exists in xcache and nocache param is not set in URL, we just return that.
			    return $xcachedTopNavigationData;
		    }
                }
		
		
		$sql = "select * from $this->tableName where parent_id=-1 order by display_order";
		
		$topNavResultSet = db_query($sql);
		
		$treeNavs = array();
		
		$level1ids = array();
		while($eachNavResult = db_fetch_array($topNavResultSet)) {
			$id = $eachNavResult['id'];
			$level1ids[] = $id;
			$treeNavs[$id] = $eachNavResult;
		}
		
		if(!empty($level1ids)) {
			$level1idCommaList = implode(',', $level1ids);
			$sql2 = "select * from $this->tableName where parent_id in ($level1idCommaList) order by display_order";
			$topNavResultSet = db_query($sql2);
			
			$level2ids = array(); 
			$level3parentparentids = array();
			while($eachNavResult = db_fetch_array($topNavResultSet)) {
				$id = $eachNavResult['id'];
				$parentid = $eachNavResult['parent_id'];
				$level2ids[] = $id;
				$treeNavs[$parentid]['child'][$id] = $eachNavResult;
				$level3parentparentids[$id] = $parentid;  
			}
				
			if(!empty($level2ids)) {
				$level2idCommaList = implode(',', $level2ids);
				$sql3 = "select * from $this->tableName where parent_id in ($level2idCommaList) order by display_order";
				$topNavResultSet = db_query($sql3);
				
				while($eachNavResult = db_fetch_array($topNavResultSet)) {
					$id = $eachNavResult['id'];
					$parentid = $eachNavResult['parent_id'];
					$parentparentid = $level3parentparentids[$parentid];
					$treeNavs[$parentparentid]['child'][$parentid]['child'][$id] = $eachNavResult;
				}
			}
		}

		// If we reach here, top navigation data was not stored in cache. We cache now in case nocache param is not set.
		if($nocache != 1){
		    $xcache->store(self::$XCACHEID, $treeNavs, 2592000);
		}
		
		return $treeNavs;
	}

	public function getDataForAdmin() {
		return $this->getData(true);
	}
	
}
?>
