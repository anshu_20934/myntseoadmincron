<?php
interface WidgetInterface {
	public function setWidgetName($widgetname);
	public function setWidgetID($widgetid);
	public function getData();
	public function getDataForAdmin();
	public function getWidgetID();
}
?>