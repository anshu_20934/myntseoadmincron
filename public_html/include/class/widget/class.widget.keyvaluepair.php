<?php
    class WidgetKeyValuePairs {
        
        const WIDGET_KVPAIR_CACHE = 'widget_key_value_pairs';
        const TABLE_NAME = 'mk_widget_key_value_pairs';

        private static $kvpairs = array();

        public static function refreshKeyValuePairsInCache() {
                global $xcache;
                if ($xcache == null) {
                    $xcache = new XCache();
                }
                $KVPairs = self::getAllWidgetKeyValuePairs();                        
                $arr = array();
                foreach($KVPairs as $kv) {
                    // dont put the desc etc in xcache
                    $arr[trim($kv['key'])] = trim($kv['value']);
                }
                self::$kvpairs = $arr;

                $xcache->storeObject(self::WIDGET_KVPAIR_CACHE, $arr, 0);
        }

        public static function getAllWidgetKeyValuePairs() {
                $KVPairs = func_query("SELECT * FROM ".self::TABLE_NAME);
                return $KVPairs;
        }

        public static function getWidgetValueForKey($key) {
                $key = trim($key);
                self::ensureInMem();
                $val = self::$kvpairs[$key];
                if($val == NULL) {
                    return NULL;
                }
                return trim($val);
        }                
                
        public static function ensureInMem() {
                if (empty(self::$kvpairs)) {
                    global $xcache;
                    if($xcache==null){
                            $xcache = new XCache();
                    }
                    $arr = $xcache->fetchObject(self::WIDGET_KVPAIR_CACHE);
                    if (empty($arr)) {
                        self::refreshKeyValuePairsInCache();
                    } else {
                        self::$kvpairs = $arr;
                    }
                }
        }


        /**
        * Method to save the widget key value
        * @param $id int value of the id field for the table. If this is not passed then 'key' parameter is compulsary.
        * @param $key string value of the key for the widget. If this is not passed then 'id' parameter is compulsary.
        * @param $value string value of the widget.
        * @param $description string description of the widget.
        **/
        public static function setWidgetValueForKey($id, $key, $value, $description){
            if(empty($key) && empty($value) && empty($description)){
                return;
            }
            if(empty($id) && empty($key)){
                return;
            }
            $updateArr = array();
            if(!empty($key)){
                $updateArr["`key`"] = $key;
            }
            
            $updateArr["`value`"] = $value;

            if(!empty($description)){
                $updateArr["`description`"] = $description;
            }

            if(!empty($id)){
                $where = " id=$id ";
            }else{
                $where = " `key`='$key' ";
            }
            func_array2updateWithTypeCheck(self::TABLE_NAME, $updateArr, $where);
            self::refreshKeyValuePairsInCache();
        }
        
        
        /**
        * Get boolean value for a Feature-Gate key.
         * Parses the value for the key,converts them to boolean and returns
        * if the value is 'true' or '1' returns true. if value is 'false' or '0', returns false, otherwise returns $defaultValue
        */
        public static function getBoolean($key,$defaultValue=false) {
        	if($defaultValue != true && $defaultValue != false) {
        		throw new Exception("Exception: ".__FILE__.":".__LINE__.":defaultValue must be either true or false");
        	}
        	$value = strtolower(trim(self::getWidgetValueForKey($key)));
        	if($value == "true" || $value == "1" || $value == 1){
        		return true;
        	}
        	else if($value == "false" || $value == "0" || $value == 0){
        		return false;
        	}
        	else {
        		return $defaultValue;
        	}
        }
         
        /**
        * Get integer value for a Feature-Gate key.
        * Parses the value for the key,converts them to integer and returns
        */
        public static function getInteger($key,$defaultValue=-1) {
          	if(!is_numeric($defaultValue)) {
        		throw new Exception("Exception: ".__FILE__.":".__LINE__.":defaultValue must be a number");
        	}
        	$value = trim(self::getWidgetValueForKey($key));
        	if(is_numeric($value)){
        		return (int)$value;
        	}
        	else {
        		return $defaultValue;
        	}
        }
        	 
        /**
        * Get float value for a Feature-Gate key.
        * Parses the value for the key,converts them to float and returns
        */
        public static function getFloat($key,$defaultValue=-1) {
        	if(!is_numeric($defaultValue)) {
        		throw new Exception("Exception: ".__FILE__.":".__LINE__.":defaultValue must be a number");
        	}
        	$value = strtolower(trim(self::getWidgetValueForKey($key)));
        	if(is_numeric($value)){
        		return (float)$value;
        	}
        	else {
        		return $defaultValue;
        	}
        }
         
        /**
        * Get String array for comma-separated values
        * Parses the value for the key, delimits values based on comma and converts to array after trimming
        */
        public static function getStrArray($key,$default=array()) {
        	$value = strtolower(trim(self::getWidgetValueForKey($key)));
        	if(!empty($value)) {
        		return array_map("trim", explode(",",$value));
        	}
        	else {
        		return $default;
        	}
        }
    }
?>
