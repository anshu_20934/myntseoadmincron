<?php
require_once("$xcart_dir/include/class/widget/class.widget.php");

class WidgetMapping {
	
	private $_tablename = 'mk_widget_page_mapping';
	public static $HOMEPAGE = 'home';
	
	public function getWidgets($page) {
		if(!isset($page) || empty($page)) {
			return null;
		}
		
		$sql = "select widget_name from mk_widget_page_mapping wmap, mk_widget wd where wd.id = wmap.widget_id "
		     . "and page_name='$page' order by wmap.display_order";
		$result = db_query($sql);

		$scrollableWidget = array();
		
		while ($row = db_fetch_array($result)) {
			$widgetname = $row['widget_name'];
			$widget = new Widget($widgetname);
			
			$widgetdata = $widget->getData();
			if(!empty($widgetdata)) {
				$scrollableWidget[] = $widgetdata;
			}
		}

		if(empty($scrollableWidget))
			return null;

		return $scrollableWidget;
	}
	
}
?>