<?php
require_once("$xcart_dir/include/solr/solrProducts.php");
require_once("$xcart_dir/include/solr/solrCategorySearch.php");
require_once("$xcart_dir/include/solr/solrStatsSearch.php");
require_once("$xcart_dir/include/solr/solrQueryBuilder.php");
require_once("$xcart_dir/include/solr/solrUtils.php");
require_once("$xcart_dir/include/func/func.utilities.php");
require_once("$xcart_dir/include/class/search/SearchProducts.php");
require_once("$xcart_dir/include/class/widget/interface.widget.php");
include_once("$xcart_dir/include/class/widget/class.static.functions.widget.php");

class WidgetQuery implements WidgetInterface {
	
	private $_tableName = 'mk_widget_query';
	private $_widgetname;
	private $_widgetID;
	private $_query;
	private $_ids;
	
	function __construct($widgetname) {
		$this->setWidgetName($widgetname);
	}
	
	public function setWidgetName($widgetname) {
		$this->_widgetname = $widgetname;
	}
	
	public function setWidgetID($widgetid) {
		$this->_widgetID = $widgetid;
	}

	public function getWidgetID() {
		return $this->_widgetID;
	}
	
	public function getData() {
		// We fetch the tags to pass to Solr by database query.
		$ids = $this->getIDsFromQuery();
		
		// The priority is only based on Stylename
		$search_priority = array("stylename");
		
		$search_type = null;		// We want to avoid $search_type = 'noncustomizable', so we set this to null.
		$shopmasterid = 'empty';	// We want to avoid $shopmasterid being set to 0, in solrQuery.
		$start = 0;
		
		$solrSearchProducts = SearchProducts::getInstance();
		
		$widgetArray = array();
		foreach ($ids as $singleid) {
			$productid = '0_style_' . $singleid;
			$singleSolrWidget = $solrSearchProducts->getSingleProductOrDesign($productid);
			$singleWidget = WidgetUtility::getWidgetFormattedData($singleSolrWidget);
			$widgetArray[] = $singleWidget;
		}
		
		$widgetQueryData = array();
		$widgetQueryData['data'] = $widgetArray;
		$widgetQueryData['name'] = $this->_widgetname;
		$widgetQueryData['nameclean'] = preg_replace("/[^a-zA-Z0-9\i]/", "_", $this->_widgetname);
		return $widgetQueryData;
	}
	
	public function getDataForAdmin() {
		$sql = $this->_query;
		$result = db_query($sql);
		$tags = array();
		while($row = db_fetch_array($result)) {
			$tags[] = $row;
		}
		
		return $tags;
	}
	
	private function getIDsFromQuery() {
		$sqlGetQuery = "select * from " . $this->_tableName . " where widget_id = " . $this->getWidgetID();
		$resultGetQuery = db_query($sqlGetQuery);
		$rowQuery = db_fetch_array($resultGetQuery);
		
		$actualSQLQuery = $rowQuery['querytext'];
		
		$result = db_query($actualSQLQuery);
		$productIDArray = array();
		while($row = db_fetch_array($result)) {
			$productIDArray[] = $row['id'];
		}
		
		$this->_ids = $productIDArray;
		
		return $this->_ids;
	}
}
?>