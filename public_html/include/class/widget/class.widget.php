<?php
// Any new widget that is defined, will be added below in require sections.
require_once("$xcart_dir/include/class/widget/class.widget.topnav.php");
require_once("$xcart_dir/include/class/widget/class.widget.topnavv2.php");
require_once("$xcart_dir/include/class/widget/class.widget.topnavv3.php");
require_once("$xcart_dir/include/class/widget/class.widget.mostpopular.php");
require_once("$xcart_dir/include/class/widget/class.widget.tag.php");
require_once("$xcart_dir/include/class/widget/class.widget.skulist.php");
require_once("$xcart_dir/include/class/widget/class.widget.query.php");

class Widget {
	private  $_widgetname;
	private  $_widgettype;
	private  $_widgetid;
	
	// $_mode is set to 'admin' so that admin specific data can be retrieved.
	private $_mode = 'portal';
	
	private $_tablename = 'mk_widget';
	
	function __construct($widgetname) {
		$this->_widgettype = null;
		$this->setWidgetName($widgetname);
	}
	
	private function setWidgetName($widgetname) {
		$this->_widgetname = $widgetname;
	}

	public function getWidgetName() {
		return $this->_widgetname;
	}
	
	public function getWidgetType() {
		return $this->_widgettype;
	}
	
	public function getWidgetID () {
		return $this->_widgetid;
	}
	
	public function getData() {
		$data = $this->checkAndGetPredefinedWidgetData();
		
		if(empty($data)) {
			// This implies that widget is not predefined. 
			// We need to do a database lookup for finding type of widget and data associated with it. 
			$data = $this->getWidgetDataFromDatabase();
		}

		return $data;
	}
	
	public function getDataForAdmin() {
		$this->_mode = 'admin';
		return $this->getData();
	}

	private function checkAndGetPredefinedWidgetData() {
		$widget = null;
		if($this->_widgetname == WidgetTopNavigation::$WIDGETNAME) {
			$widget = new WidgetTopNavigation();
		} else if ($this->_widgetname == WidgetMostPopular::$WIDGETNAME) {
			$widget = new WidgetMostPopular();
		} else if ($this->_widgetname == WidgetTopNavigationV2::$WIDGETNAME) {
			$widget = new WidgetTopNavigationV2();
		} else if ($this->_widgetname == WidgetTopNavigationV3::$WIDGETNAME) {
			$widget = new WidgetTopNavigationV3();
		}
		
		return $this->getWidgetData($widget);
	}
	
	private function getWidgetDataFromDatabase() {
		$widget = null;
				
		if(!empty($this->_widgetname)) {
			$sqlSafeWidgetName = mysql_real_escape_string($this->getWidgetName());
			$sql = "select * from " . $this->_tablename . " where widget_name='$sqlSafeWidgetName'";
			
			$result = db_query($sql);
			$row = db_fetch_array($result);
			
			$this->_widgetid 	= $row['id'];
			$this->_widgetname 	= $row['widget_name'];
			$this->_widgettype	= $row['widget_type'];

			if($this->_widgettype == 'tag') {
				$widget = new WidgetTag($this->getWidgetName());
			} else if($this->_widgettype == 'sku') {
				$widget = new WidgetSKUList($this->getWidgetName());
			} else if($this->_widgettype == 'query') {
				$widget = new WidgetQuery($this->getWidgetName());
			}
			
			if(!empty($widget)) {
				$widget->setWidgetID($this->_widgetid);
			}
		}
		
		return $this->getWidgetData($widget);
	}

	private function getWidgetData($widget) {
		if(!empty($widget)) {
			if($this->_mode=='portal') {
				return $widget->getData();
			} else if($this->_mode=='admin') {
				return $widget->getDataForAdmin();
			}
		}
		return null;
	}
	
	public function setDataToSmarty($smartyObject, $smartVariableName = null, $widgetdata = null) {
		if($widgetdata==null) {
			// Pravin . We could xcache the data here. 
			$widgetdata = $this->getData();
		}
        if($this->_widgetname == WidgetTopNavigation::$WIDGETNAME) {
            //torestructure the topnav menu data as needed
            $featuredProducts=$this->getFeaturedProducts($widgetdata);
            $widgetdata=$this->restructureTopNavData($widgetdata);
            $smartyObject->assign("topNavigationWidget",$widgetdata);
            $smartyObject->assign("featuredProducts",$featuredProducts);
        } else if ($this->_widgetname == WidgetMostPopular::$WIDGETNAME) {
            $smartyObject->assign("mostPopularWidget",$widgetdata);
        } else if(!empty($smartVariableName)) {
            $smartyObject->assign($smartVariableName,$widgetdata);
        } else if($this->_widgetname == WidgetTopNavigationV2::$WIDGETNAME) {
            $smartyObject->assign("topNavigationWidgetV2",$widgetdata);
        } else if($this->_widgetname == WidgetTopNavigationV3::$WIDGETNAME) {
            $smartyObject->assign("topNavigationWidget",$widgetdata);
        }
	}
	
	public function getFeaturedProducts($widgetdata){
		$featuredProducts=array();
		foreach($widgetdata as $key => $value){
			foreach($value["child"] as $_key => $_value){
				if(empty($featuredProducts[$value["link_name"]][$_value["display_category"]]) && !empty($_value["styleid"])){
					$featuredProducts[$value["link_name"]][$_value["display_category"]]=$_value["styleid"];
				}	
			}
		}

		return $featuredProducts;
	}
	
	public function restructureTopNavData($widgetdata){
		$topNavData=array();
		
		foreach($widgetdata as $key => $value){
			$_topNavData=array();
			foreach($value["child"] as $_key => $_value){
				$__topNavData=array();
				if(empty($_topNavData[$_value["display_category"]])){
					$_topNavData[$_value["display_category"]] = array();
				}
				foreach($_value["child"] as $__key => $__value){
					$__topNavData[$__value["link_name"]]=$__value["link_url"];
				}
				$_topNavData[$_value["display_category"]][$_value["link_name"]]=$__topNavData;
			}
			$topNavData[$value["link_name"]]=$_topNavData;
		}
		
		return (!empty($topNavData))?$topNavData:$widgetdata;
	}
	
	/** This is used only for debugging purposes, and any reference to widget->viewHelpData on production should be removed. <br/>
	 * This function prints the data fetched in a neater format, and very helpful for debugging purposes.
	 */
	public function viewHelpData($widgetdata = null) {
		$data = $widgetdata;
		if(empty($data)) {
			if($this->_mode=='portal') {
				$data = $this->getData();
			} else if($this->_mode=='admin') {
				$data = $this->getDataForAdmin();
			} else {
				$data = null;
			}
		}
		echo "<PRE>";
		print_r($data);
		exit;
	}
}
?>
