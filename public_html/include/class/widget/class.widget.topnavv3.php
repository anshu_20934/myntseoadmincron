<?php
require_once("$xcart_dir/include/class/widget/interface.widget.php");
define('FUNC_SKU_CALL', 'Y');
include_once "$xcart_dir/include/solr/solrProducts.php";

class WidgetTopNavigationV3 implements WidgetInterface {
	
	private $tableName = 'mk_widget_top_nav_v3';
	public static $WIDGETNAME = 'TopNavigationV3';
	public static $XCACHEID   = 'TopNavigationWidgetDataV3';

	public function setWidgetName($widgetname) {
		$this->_widgetname = self::$WIDGETNAME;
	}
	
	public function getWidgetID() {
		return null;
	}
	
	public function setWidgetID($widgetid) {
		// Does nothing here.
	}
	
	public function getData($nocache_param=false) {
		global $xcache, $nocache;
        if(!$nocache_param && $nocache != 1){
		    // Fetch top navigation data from xcache
		    $xcachedTopNavigationData = $xcache->fetch(self::$XCACHEID);
	
		    if($xcachedTopNavigationData!=null) {
			    // If data exists in xcache and nocache param is not set in URL, we just return that.
			    return $xcachedTopNavigationData;
		    }
        }
		
		
		$sql = "select * from $this->tableName where parent_id=-1 order by display_order";
		
		$topNavResultSet = db_query($sql);
		
		$treeNavs = array();
		
		$level1ids = array();
		while($eachNavResult = db_fetch_array($topNavResultSet)) {
			$id = $eachNavResult['id'];
			$level1ids[] = $id;
			$treeNavs[$id] = $eachNavResult;
		}
		
		if(!empty($level1ids)) {
			$level1idCommaList = implode(',', $level1ids);
			$sql2 = "select * from $this->tableName where parent_id in ($level1idCommaList) order by display_order";
			$topNavResultSet = db_query($sql2);
			
			$level2ids = array(); 
			$level3parentparentids = array();
			while($eachNavResult = db_fetch_array($topNavResultSet)) {
				$id = $eachNavResult['id'];
				$parentid = $eachNavResult['parent_id'];
				$level2ids[] = $id;
				$treeNavs[$parentid]['child'][$id] = $eachNavResult;
				$level3parentparentids[$id] = $parentid;  
			}
				
			if(!empty($level2ids)) {
				$level2idCommaList = implode(',', $level2ids);
				$sql3 = "select * from $this->tableName where parent_id in ($level2idCommaList) order by display_order";
				$topNavResultSet = db_query($sql3);
				
				while($eachNavResult = db_fetch_array($topNavResultSet)) {
					$id = $eachNavResult['id'];
					$parentid = $eachNavResult['parent_id'];
					$parentparentid = $level3parentparentids[$parentid];
					$treeNavs[$parentparentid]['child'][$parentid]['child'][$id] = $eachNavResult;
				}
			}
		}

		// If we reach here, top navigation data was not stored in cache. We cache now in case nocache param is not set.
		if($nocache != 1){
		    $xcache->store($this::$XCACHEID, $treeNavs, 2592000);
		}
		
		return $treeNavs;
	}

	public function getDataForAdmin() {
		return $this->getData(true);
	}
	
	public function setNavId($navId,$landingPage) {
        try{
            $db_adapter = MyntraPageDBAdapter::getInstance();
            if($db_adapter->isLandingPage($landingPage)){
                $solrQuery = $db_adapter->getSolrQueryForLandingPage($landingPage);
                $solrProducts = new solrProducts();
                $out = $solrProducts->searchAndSort(
                            $solrQuery . " AND count_options_availbale:[0 TO *]", 
                            $start=0, 
                            $limit=100000, 
                            $sortField='',
                            $params=array("fl"=>"styleid")
                        );
                $style_ids=array();
                foreach($out->docs as $doc){
                    $style_ids[]=$doc->styleid;
                }
                $id_list = implode(",",$style_ids);
                if(!empty($id_list)){
                    $nav_id_update  = "update mk_style_properties set ";
                    $nav_id_update .= "nav_id='".$navId."' ";
                    $nav_id_update .= "where style_id in (".$id_list.")";

                    $res=func_query($nav_id_update);
                }
            }
        }catch(Exception $e){
            echo "error";
        }
		// Does nothing here.
	}
}
?>
