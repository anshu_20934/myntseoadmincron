<?php
namespace widget;

include_once (\HostConfig::$documentRoot .  "/include/class/widget/class.widget.skulist.php");
require_once(\HostConfig::$documentRoot . "/include/class/search/SearchProducts.php");
include_once \HostConfig::$documentRoot . "/include/dao/class/search/class.MyntraPageDBAdapter.php";
require_once \HostConfig::$documentRoot . "/Cache/Cache.php";
require_once \HostConfig::$documentRoot . "/include/class/cache/XCache.php";
use landingpage\manager\SectionManager;
use landingpage\manager\LPManager;
use abtest\MABTest;

class ProductSectionsWidget implements IWidget{
    function render(){
        $lpname = $_GET['lpname'];
        global $smarty;        
        $cache = new \XCache();
        $lpmanager = new LPManager();
        $lpdetails = $lpmanager->getDetailsByLPNameCached($lpname);
        $sectionmanager =  new SectionManager(); 
        $allSections = $sectionmanager->getAllSectionsCached($lpdetails["id"]);
        $tmp_list=array();
        global $colourOptionsVariant;
        $colourOptionsVariant = MABTest::getInstance()->getUserSegment('ListPageColourOptions');
        $smarty->assign("colourOptionsVariant", $colourOptionsVariant);
  
        foreach($allSections as $sectionData){
           $cacheProductSections = new \XCache();
           $out_content = $cacheProductSections->fetchAndStore(function($sectionData){
                
                $link_url_pieces = explode("?",$sectionData['url']);
                $link_url = $link_url_pieces[0]; // url entered -> solr query from admin
                $link_url_full = $sectionData['url']; // hyperlink on section title from admin
                $sort_by_str = explode("=" , $link_url_pieces[1]);
                $sort_by = $sort_by_str[1];
                $cat_content['id'] = $sectionData['id'];
                $cat_content['url'] = $sectionData['target_url'];
                $cat_content['title'] = $sectionData['title'];
                $cat_content['description'] = $sectionData['description'];
                
                $dbAdapter=\MyntraPageDBAdapter::getInstance();                                                     
                $qu =  $dbAdapter->getSolrQueryForLandingPage($link_url);  
                $searchProducts = new \SearchProducts($link_url,null,$getStatsInstance);
                $searchProducts->getAjaxQueryResult($qu,null,$sort_by, 0,$sectionData["no_of_products"],"",""); //number from admin

                $style_array = array();
                foreach($searchProducts->products as $tmpProduct){
                    array_push($style_array,$tmpProduct['styleid']);
                }

                $styleWidget=new \WidgetSKUList('lpProductSection',$style_array);
                $widgetData=$styleWidget->getData();

                /**
                 * Start of style grouping currently data is based on colour group
                 */
                $styleIdGroup = array(); //style ids to be searched for style groupping
                $styleIdGroupKey = array(); //style ids already present in $products

                global $colourOptionsVariant ;
                foreach($widgetData["data"] as $key => &$val) {
                    $pos = stripos($val["product"], $val["brands_filter_facet"]);
                    $val["product"] = trim(substr($val["product"], $pos+strlen($val["brands_filter_facet"]), strlen($val["product"])));
                    if ($colourOptionsVariant == 'test') {
                        $styleIdsArray = explode(",", $val['style_group']);
                        $styleIdGroup = array_merge($styleIdGroup, $styleIdsArray);
                        $styleIdGroupKey[] = $val['styleid'];
                    }
                }


                unset($val);
                if ($colourOptionsVariant == 'test') {

                    //remove duplicates and already solr searched styles from $styleIdGroup
                    $styleIdGroup = array_filter(array_unique($styleIdGroup));//list of style ids which belong to a group
                    $styleIdGroupDiff = array_diff($styleIdGroup, $styleIdGroupKey);//list of style ids for which solr data need to be fetched

                    $solrSearchProducts = \SearchProducts::getInstance();

                    $stylesArray = @$solrSearchProducts->getMultipleStyleFormattedSolrData($styleIdGroupDiff);

                    //creat a separate array for group styles
                    $solrStyles = array();
                    foreach ($stylesArray as $style) {
                        $solrStyles[$style['styleid']] = $style;
                    }

                    $styleIdGroupDiff = array_diff($styleIdGroup, $styleIdGroupDiff);
                    foreach($widgetData["data"] as $style) {
                        if(in_array($style['styleid'], $styleIdGroupDiff)) {
                            $solrStyles[$style['styleid']] = $style;
                        }
                    }

                    foreach($widgetData["data"] as $key => &$val) {
                        if ($optimVariant == 'test') {
                            $val['search_image'] = $val['searchimagepath'];
                        }
                    $styleIdsArray = explode(",", $val['style_group']);
                    if(count($styleIdsArray ) > 1){
                        $val['colourVariantsData'] = array();
                        foreach ($styleIdsArray as $styleId) {
                            if ($styleId != $val['styleid']) {
                                $style = $solrStyles[$styleId];
                                if(!empty($style)) {
                                    $articletype = $style['global_attr_article_type'];
                                    $brandname = $style['global_attr_brand'];
                                    $stylename = $style['stylename'];
                                    $styleid = $style['styleid'];
                                    $landingpageurl = construct_style_url($articletype,$brandname,$stylename,$styleid);
                                    $val['colourVariantsData'][$styleid] = array(
                                        "global_attr_base_colour" => $style['global_attr_base_colour'],
                                        "global_attr_colour1" => $style['global_attr_colour1'],
                                        "landing_page_url" => $landingpageurl,
                                        "dre_landing_page_url" => $landingpageurl,
                                        "search_image" => $style['search_image']
                                    );
                                }
                            }
                        }
                       }
                    }
                }
                unset($val);

                /**
                 * End of style grouping
                 */

                global $smarty;
                $smarty->assign('title',$sectionData['title']);
                $smarty->assign('url',$sectionData['target_url']);
                $smarty->assign('sectiondescription',$sectionData['description']);
                $smarty->assign('no_of_products',$sectionData['no_of_products']);
                $smarty->assign('is_carousel',$sectionData['is_carousel']);        
                $smarty->assign('widget',$widgetData);
                $smarty->assign('quicklookenabled','true');
           
                $div_content=$smarty->fetch("inc/lp-product-section.tpl", $smarty); // tpl to be modified from tabview to stack view 
                $cat_content['is_carousel'] = $sectionData['is_carousel'] && ($sectionData['no_of_products'] > 5);
                $cat_content['div_content'] = $div_content;

                return $cat_content;                    

            }, array($sectionData), "section-data-lpid-".$lpdetails["id"]."-".$sectionData['id'],300);
            
            $tmp_list[]=$out_content;

        }

        
        if (!empty($tmp_list)){ 
            $resp = array( 
                'lpdetails' => $lpdetails,
                'status'  => 'SUCCESS', 
                'sectiondata' => $tmp_list
            ); 
        } 
        else { 
            $resp = array( 
                'status'  => 'ERROR', 
                'sectiondata' => '',
                'lpdetails' => $lpdetails, 
            ); 
        }

        global $_MABTestObject;
        //$resp["popular_ab_value"] = $_MABTestObject->getUserSegment("newPopularWidget");
        //$resp["popular_ab_value"] = 'test';
    
        echo json_encode($resp);
    }
  
}
?>
