<?php
namespace widget;


include_once (\HostConfig::$documentRoot .  "/include/class/widget/class.widget.skulist.php");
require_once(\HostConfig::$documentRoot . "/include/class/search/SearchProducts.php");
include_once \HostConfig::$documentRoot . "/include/dao/class/search/class.MyntraPageDBAdapter.php";
require_once \HostConfig::$documentRoot . "/Cache/Cache.php";
require_once \HostConfig::$documentRoot . "/include/class/cache/XCache.php";

class MostPopularWidget implements IWidget{
    function render(){

        global $smarty;        

        $cache = new \XCache();
        $tab_list = $cache->fetchAndStore(function(){
            $sql = "select * from mk_widget_mostpopular where parent_id=-1 order by display_order";
            return func_query($sql,true);  
        }, array(), "most-popular");


        $tab_info=array();

        foreach($tab_list as $tab){
            $cacheTabCategories = new \xCache();
            $link_list = $cacheTabCategories->fetchAndStore(function($tabid){
                $sql = "select * from mk_widget_mostpopular where parent_id=".$tabid ." order by display_order";
                return func_query($sql,true);
            }, array($tab['id']), "tab-category-list".$tab['id']);
            
            $tmp_list=array();
            foreach($link_list as $tmpLink){

                $cacheCategorydata = new \XCache();
                $out_content = $cacheCategorydata->fetchAndStore(function($tmp_link){
             
                    $link_url_pieces = explode("?",$tmp_link['link_url']);
                    $link_url = $link_url_pieces[0];
                    $link_url_full = $tmp_link['link_url'];
                    $sort_by_str = explode("=" , $link_url_pieces[1]);
                    $sort_by = $sort_by_str[1];
                    $cat_content = array('link_url'=>$link_url_full);

                    $dbAdapter=\MyntraPageDBAdapter::getInstance();                                                     
                    $qu =  $dbAdapter->getSolrQueryForLandingPage($link_url);  
                    $searchProducts = new \SearchProducts($link_url,null,$getStatsInstance);
                    $searchProducts->getAjaxQueryResult($qu,null,$sort_by, 0,10,"","");

                    $style_array = array();
                    foreach($searchProducts->products as $tmpProduct){
                        array_push($style_array,$tmpProduct['styleid']);
                    }

                    $styleWidget=new \WidgetSKUList('headerCarouselItems',$style_array);
                    $widgetData=$styleWidget->getData();

                    global $smarty;        
                    $smarty->assign('widget',$widgetData);
                    $smarty->assign('quicklookenabled','true');
                    $div_content=$smarty->fetch("inc/most-popular.tpl", $smarty);
                    $cat_content['div_content'] = $div_content;
                    
                    return $cat_content;                    

                }, array($tmpLink), "tab-category-data-".$tmpLink['id'],300);

                $tmp_list[$tmpLink['link_name']]=$out_content;

            }
            if(!empty($tab['link_url']) && !empty($tmp_list)){
                $tab_info[$tab['link_name']] = $tmp_list;
            }
        }

        if (!empty($tab_info)){ 
            $resp = array( 
                'status'  => 'SUCCESS', 
                'tabinfo' => $tab_info
            ); 
        } 
        else { 
            $resp = array( 
                'status'  => 'ERROR', 
                'content' => '' 
            ); 
        }

        global $_MABTestObject;
        $resp["popular_ab_value"] = $_MABTestObject->getUserSegment("newPopularWidget");

        echo json_encode($resp);
    }
    function getWidget($styleids){
                $styleWidget=new \WidgetSKUList('headerCarouselItems',$styleids);
                        $widgetFormattedData = $styleWidget->getData();
                        return $widgetFormattedData;
    }
}
?>
