<?php

class WidgetUtility {
	
	public static function getWidgetFormattedData($solrWidgetData) {
		$widgetData = $solrWidgetData;
		
		// Most jerseys have a target url defined for them.
		$landingpageurl = $widgetData['target_url'];
		
		if(empty($landingpageurl)) {
			
			// If target_url is not set, we create the new landing page URL format.
			
			$articletype = $widgetData['global_attr_article_type'];
			$brandname = $widgetData['global_attr_brand'];
			$stylename = $widgetData['stylename'];
			$styleid = $widgetData['styleid'];
			
			if(empty($brandname)) {
				$brandname = 'branded';
			}
			
			if(empty($articletype)) {
				$articletype = 'lifestyle';
			}
			
			// The stylename in url should contain only alphanumeric characters and some special characters
			// The stylename in url should have spaces replaced by '-' character
			$stylename = preg_replace("/[^a-zA-Z0-9\s\-]/", "", $stylename);
			$stylename = preg_replace("/[\s-]+/","-",$stylename);
			
			// We need to limit stylename to 50 characters in URL construction.
			$stylename = substr($stylename, 0, 50);
			
			// Same properties apply for article type
			$articletype = preg_replace("/[^a-zA-Z0-9\s\-]/", "", $articletype);
			$articletype = preg_replace("/[\s-]+/","-",$articletype);

			// Same properties apply for brand name type
			$brandname = preg_replace("/[^a-zA-Z0-9\s\-]/", "", $brandname);
			$brandname = preg_replace("/[\s-]+/","-",$brandname);
			
			// The new URL format should be : http://<domainname>/<articleType>/<brandname>/<stylename>/<styleid>/buy
			// The URL should be in lower case.
			$landingpageurl = "$articletype/$brandname/$stylename/$styleid/buy";
			$landingpageurl = strtolower($landingpageurl);
		}

		$widgetData['landingpageurl'] = $landingpageurl;
		$widgetData['descr'] = strip_tags($singleWidget['descr']);
	
		include_once "$xcart_dir/include/class/class.feature_gate.keyvaluepair.php";
		
		$applyCBOnDiscountedItems = FeatureGateKeyValuePairs::getFeatureGateValueForKey('enableCBOnDiscountedProducts');
		if(empty($applyCBOnDiscountedItems)){
			$applyCBOnDiscountedItems = false;	
		} else if($applyCBOnDiscountedItems == 'false'){
			$applyCBOnDiscountedItems = false;
		} else {
			$applyCBOnDiscountedItems = true;
		}
		
		$widgetData['cashback'] = 0;
		if($applyCBOnDiscountedItems)	{
			$widgetData['showcashback'] = "true";
			$widgetData['cashback'] = $widgetData['discounted_price'] * 0.1;
		} else {
			if(isset($widgetData['discount']) && ($widgetData['discount'] > 0))	{
				$widgetData['showcashback'] = "false";
			} else {
				$widgetData['showcashback'] = "true";
				$widgetData['cashback'] = $widgetData['discounted_price'] * 0.1;
			}
		}

		//Looking up into the resource strings to get the resource for teh resource id
		global $_rst;
		if(!empty($_rst[$widgetData['discount_label']]))
			$widgetData['discount_label'] = $_rst[$widgetData['discount_label']];
		
		return $widgetData;
	}
	
	public static function getArraySolrFormattedData($solrArrayWidgetData) {
		foreach($solrArrayWidgetData as $key=>$solrWidgetData) {
			$solrWidgetData = WidgetUtility::getWidgetFormattedData($solrWidgetData);
			$solrArrayWidgetData[$key] = $solrWidgetData;
		}
		return $solrArrayWidgetData;
	}

	public static function generateNavId($styleId) {
        if(!empty($styleId)){
            $topNavQuery = "select * from ". mk_widget_top_nav_v3." order by id desc";            
            $nav_list = func_query($topNavQuery,true);
            define('FUNC_SKU_CALL', 'Y');
            include_once "$xcart_dir/include/solr/solrProducts.php";
            foreach($nav_list as $nav_link){
                try{
                    $db_adapter = MyntraPageDBAdapter::getInstance();
                    $landingPage=substr($nav_link['link_url'],1);
                    if($db_adapter->isLandingPage($landingPage)){
                        $solrQuery = $db_adapter->getSolrQueryForLandingPage($landingPage);
                        $solrProducts = new solrProducts();
                        $out = $solrProducts->searchAndSort(
                                    $solrQuery . " AND styleid:$styleId", 
                                    $start=0, 
                                    $limit=1, 
                                    $sortField='',
                                    $params=array("fl"=>"styleid")
                                );
                        if($out->numFound ==1){
                            $nav_id_update  = "update mk_style_properties set ";
                            $nav_id_update .= "nav_id='" . $nav_link['id'] . "' ";
                            $nav_id_update .= "where style_id='".$styleId."'";
                            $res=func_query($nav_id_update);
                            return $nav_link['id'];
                        }
                    }
                }catch(Exception $e){
                    echo "Exception";
                }
            }
        }
    }
}
?>
