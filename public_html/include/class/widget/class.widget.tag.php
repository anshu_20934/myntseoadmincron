<?php
require_once("$xcart_dir/include/solr/solrProducts.php");
require_once("$xcart_dir/include/solr/solrCategorySearch.php");
require_once("$xcart_dir/include/solr/solrStatsSearch.php");
require_once("$xcart_dir/include/solr/solrQueryBuilder.php");
require_once("$xcart_dir/include/solr/solrUtils.php");
require_once("$xcart_dir/include/func/func.utilities.php");
require_once("$xcart_dir/include/class/search/SearchProducts.php");

require_once("$xcart_dir/include/class/widget/interface.widget.php");
include_once("$xcart_dir/include/class/widget/class.static.functions.widget.php");

class WidgetTag implements WidgetInterface {
	
	private $_tableName = 'mk_widget_tag';
	private $_widgetname;
	private $_widgetID;
	private $_tags;
	private $_searchLimit = 5;	// Default value is 5.
	
	function __construct($widgetname) {
		$this->setWidgetName($widgetname);
	}
	
	public function setWidgetName($widgetname) {
		$this->_widgetname = $widgetname;
	}
	
	public function setWidgetID($widgetid) {
		$this->_widgetID = $widgetid;
	}

	public function getWidgetID() {
		return $this->_widgetID;
	}
	
	public function getData() {
		// We fetch the tags to pass to Solr by database query.
		$tags = $this->getAndSetTagData();		
		
		//$search_priority = array("stylename");
		$search_priority = null;
		
		$search_type = null;		// We want to avoid $search_type = 'noncustomizable', so we set this to null.
		$shopmasterid = 'empty';		// We want to avoid $shopmasterid being set to 0, in solrQuery.
		$start = 0;
		
		$solrSearchProducts = SearchProducts::getInstance();
		$solrData = $solrSearchProducts->getRelatedProductsByTags($tags, $search_priority, $start, $this->_searchLimit, $search_type, $shopmasterid);
		
		$widgetArray = array();

		foreach ($solrData as $singleSolrWidget) {
			$singleWidget = WidgetUtility::getWidgetFormattedData($singleSolrWidget);
			$widgetArray[] = $singleWidget;
		}
				
		$widgetTagData = array();
		$widgetTagData['data'] = $widgetArray;
		$widgetTagData['name'] = $this->_widgetname;
		$widgetTagData['nameclean'] = preg_replace("/[^a-zA-Z0-9\i]/", "_", $this->_widgetname);
		
		return $widgetTagData;
	}
	
	public function getDataForAdmin() {
		$sql = "select * from " . $this->_tableName;
		$result = db_query($sql);
		$tags = array();
		while($row = db_fetch_array($result)) {
			$tags[] = $row;
		}
		
		return $tags;
	}
	
	private function getAndSetTagData() {
		$sql = "select * from " . $this->_tableName . " where widget_id = " . $this->getWidgetID();
		$result = db_query($sql);
		$row = db_fetch_array($result);
		
		$tagCommaSeparated = $row['tag'];
		$this->_tags = $tagCommaSeparated;
		$this->_searchLimit = $row['limit'];
		
		return $this->_tags;
	}
}
?>