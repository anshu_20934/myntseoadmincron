<?php
require_once("$xcart_dir/include/class/widget/interface.widget.php");

class WidgetMostPopular implements WidgetInterface {
	
	private $tableName = 'mk_widget_most_popular';
	private $dynamicLimit = 30;
	public static $WIDGETNAME = 'MostPopular';

	public function setWidgetName($widgetname) {
		$this->_widgetname = self::$WIDGETNAME;
	}
	
	public function setWidgetID($widgetid) {
		// Does nothing here.
		// This is a fixed widget, so need widget ID is associated with it.
	}
	
	public function getData() {
		return ($this->getMostPopularData($this->dynamicLimit));
	}

	public function getDataForAdmin() {
		return ($this->getMostPopularData('all'));
	}
	
	public function getWidgetID() {
		return null;
	}
	
	public function getMostPopularData($limit) {
                global $xcache;                
                $mostPopularArray = $xcache->fetchAndStore(function($limit, $tableName){
                        if($limit == 'all') {
                                // For Admin operations, we need to show all data.
                                $sql = "select * from " . $tableName;
                        } else {
                                $sql = "(select * from " . $tableName . " where is_permanent=1) union "
                                     . "(select * from " . $tableName . " where is_permanent=0 and 1 order by rand() limit $limit )";
                        }
                        $result = db_query($sql);
                        		$mostPopularArray = array();
                        while($row = db_fetch_array($result)) {
                                // We separate the permanent / dynamic Tags so that its easy to differentiate between them.
                                if($row['is_permanent'] == 1) {
                                        $mostPopularArray['permanent'][] = $row;
                                } else {
                                        $mostPopularArray['dynamic'][] = $row;
                                }
                        }
                        return $mostPopularArray;
                }, array($limit, $this->tableName), $this->tableName.":".$limit);
		return $mostPopularArray;
	}
}
?>