<?php
include_once($xcart_dir."/include/class/widget/class.widget.php");
if($smarty) {
	// Get the widget for TopNavigation.
	// This data is used in /skin1/site/menu.tpl
	if($menu_version === "v2"){
		$widget = new Widget(WidgetTopNavigationV2::$WIDGETNAME);
		$widget->setDataToSmarty($smarty);	
	}
	else{
		$widget = new Widget(WidgetTopNavigation::$WIDGETNAME);
		$widget->setDataToSmarty($smarty);
	}
	
}
?>
