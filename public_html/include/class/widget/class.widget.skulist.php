<?php
require_once("$xcart_dir/include/solr/solrProducts.php");
require_once("$xcart_dir/include/solr/solrCategorySearch.php");
require_once("$xcart_dir/include/solr/solrStatsSearch.php");
require_once("$xcart_dir/include/solr/solrQueryBuilder.php");
require_once("$xcart_dir/include/solr/solrUtils.php");
require_once("$xcart_dir/include/func/func.utilities.php");
require_once("$xcart_dir/include/class/search/SearchProducts.php");

require_once("$xcart_dir/include/class/widget/interface.widget.php");
include_once("$xcart_dir/include/class/widget/class.static.functions.widget.php");

class WidgetSKUList implements WidgetInterface {
	
	private $_tableName = 'mk_widget_sku_list';
	private $_widgetname;
	private $_widgetID;
	private $_skus;
	private $_styleids;
	
	function __construct($widgetname, $styleids = null) {
		$this->setWidgetName($widgetname);
		$this->_styleids = $styleids; // Array of styleids.
	}
	
	public function setWidgetName($widgetname) {
		$this->_widgetname = $widgetname;
	}
	
	public function setWidgetID($widgetid) {
		$this->_widgetID = $widgetid;
	}

	public function getWidgetID() {
		return $this->_widgetID;
	}
	
	public function getData() {
		// We fetch the tags to pass to Solr by database query.
		$skus = $this->_styleids;
		if(empty($skus)) {
			$skus = $this->getSKUs();
		}
		
		// The priority is only based on Stylename
		$search_priority = array("stylename");
		
		$search_type = null;		// We want to avoid $search_type = 'noncustomizable', so we set this to null.
		$shopmasterid = 'empty';	// We want to avoid $shopmasterid being set to 0, in solrQuery.
		$start = 0;
		
		$solrSearchProducts = SearchProducts::getInstance();
		
		$widgetArray = array();
		$skuids = array();
		foreach ($skus as $singlesku) {
			$skuids[] = '0_style_' . $singlesku;
			/*$singleSolrWidget = $solrSearchProducts->getSingleProductOrDesign($skuid);
			$singleWidget = WidgetUtility::getWidgetFormattedData($singleSolrWidget);
			$widgetArray[] = $singleWidget;*/
		}
		$widgetData = $solrSearchProducts->getMultipleProductOrDesign($skuids);
		foreach($widgetData as $data){
			$widgetArray[] = WidgetUtility::getWidgetFormattedData($data);
		}
		$widgetSKUData = array();
		$widgetSKUData['data'] = $widgetArray;
		$widgetSKUData['name'] = $this->_widgetname;
		$widgetSKUData['nameclean'] = preg_replace("/[^a-zA-Z0-9\i]/", "_", $this->_widgetname);
		return $widgetSKUData;
	}
	
	public function getDataForAdmin() {
		$sql = "select * from " . $this->_tableName;
		$result = db_query($sql);
		$tags = array();
		while($row = db_fetch_array($result)) {
			$tags[] = $row;
		}
		
		return $tags;
	}
	
	private function getSKUs() {
		$widgetID = $this->getWidgetID();
		
		if(empty($widgetID)) return array();
		
		$sql = "select sku from " . $this->_tableName . " where widget_id = '" . $widgetID ."'";
		$sql.= " and sku not in (select id from mk_product_style where styletype='A')"; 
		$sql.= " order by display_order";
		$result = db_query($sql);
		$skuArray = array();
		while($row = db_fetch_array($result)) {
			$skuArray[] = $row['sku'];
		}
		
		$this->_skus = $skuArray;
		
		return $this->_skus;
	}
}
?>