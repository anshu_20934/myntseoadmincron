<?php

namespace notification;

class Notification
{
        const PAYMENT_PAGE_TIME_LIMIT_CONSTANT='timeLimit';
        private $user =NULL;
        private $mobile =NULL;
        private $page =NULL;
        private $type =NULL;
        private $message =NULL;
	/**
	 * Constructor.
	 */
	public function __construct($user,$mobile,$page,$type,$message)
	{
		$this->user=$user;
		$this->mobile=$mobile;
		$this->page=$page;
		$this->type=$type;
		$this->message=$message;
	}

        public function getDBArray(){
          $outArray = array(
            'user'=>$this->user,
            'page'=>$this->page,
            'type'=>$this->type,
            'message'=>$this->message
          );
          return $outArray;
        }

        public function getMessage(){
          return $this->message;
        }

        public function getUser(){
          return $this->user;
        }
        
        public function getMobile(){
          return $this->mobile;
        }
}
