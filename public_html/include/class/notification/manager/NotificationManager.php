<?php
namespace notification\manager;
use notification\Notification;
use notification\dao\NotificationDAO;
include_once(\HostConfig::$documentRoot . "/include/func/func.mail.php");
include_once(\HostConfig::$documentRoot . "/include/class/class.feature_gate.keyvaluepair.php");
require_once \HostConfig::$documentRoot . "/include/class/mcart/class.MCart.php";

class NotificationManager
{	
	
	/**
	 * Constructor.
	 */
	public function __construct()
	{
	}
	

    public function saveNotification(Notification $notification){
      if(!$this->isValidNotification($notification)){
        echo "not valid request";
        return false;
      }
      $notificationDao = new NotificationDAO();
      $notification_id = $notificationDao->createNotification($notification);
      $this->sendMail($notification,$notification_id); 
      return true;
    }

    private function sendMail(Notification $notification,$notification_id){
      $mcartDBAdapter = new \MCartFactory();
      $defaultCart = $mcartDBAdapter->getCartForLogin($notification->getUser());
      $cartData = $this->getCartData($defaultCart);
      $cartHtml = $cartData['cartHtml'];
      $totalAmount = $cartData['totalAmount'];
      if(!empty($notification_id)){
          $cc_notification_users = \FeatureGateKeyValuePairs::getFeatureGateValueForKey("ccNotificationUsers");
          if(!empty($cc_notification_users)){
            $mail_details = array(
                                           "to" => $cc_notification_users,
                                           "subject" => "Payment Page Alert [Cart Total Amount::". $totalAmount ." for user " . $notification->getUser() . " with mobile# " . $notification->getMobile() . "]",
                                           "content" => "User Name :: " .$notification->getUser() . "<br>" 
                                                        . "Phone No :: " . $notification->getMobile() . "<br>"
                                                        . "Notification Message :: " . $notification->getMessage() . "<br>"
                                                        . "notification id is :: " . $notification_id . "<br>"
                                                        . "cartDetails <br>"
                                                        . $cartHtml,
                                           "mail_type" => \MailType::CRITICAL_TXN
            );
            $multiPartymailer = new \MultiProviderMailer($mail_details);
            $result=$multiPartymailer->sendMail();
          }
      }
    }

    private function getCartData($defaultCart){
    
		$customerName = $defaultCart->getLogin();
		
		$login = $defaultCart->getLogin();
		//$cartId = $defaultCart["cart"]->getCartID();//we might need this in future
		
		//======================
		$myCart =  $defaultCart;
		$additionalCharges = number_format($myCart->getAdditionalCharges(), 2, ".", '');
		$totalMRP = number_format($myCart->getMrp(), 2, ".", '');
		$vat = number_format($myCart->getVat(), 2, ".", '');
		$totalQuantity = number_format($myCart->getItemQuantity(), 0);
		$couponDiscount = number_format($myCart->getDiscount(), 2, ".", '');
		$cashDiscount = number_format($myCart->getCashDiscount(), 2, ".", '');
		$productDiscount = number_format($myCart->getDiscountAmount(), 2, ".", '');
		$shippingCharge = number_format($myCart->getShippingCharge(), 2, ".",'');
		$cartLevelDiscount = round(number_format($myCart->getCartLevelDiscount(), 2, ".", ''));
		$totalAmount = $totalMRP + $additionalCharges - $couponDiscount - $cashDiscount - $productDiscount - $cartLevelDiscount + $shippingCharge;
		$cashBackDisplayAmount= $totalMRP + $additionalCharges - $productDiscount - $couponDiscount;
		$coupondiscount = $couponDiscount;
		$cashdiscount = $cashDiscount;
		$mrp = $totalMRP;
		$amount = $totalMRP + $additionalCharges + $shippingCharge;
		$savings = $productDiscount + $couponDiscount + $cashDiscount + $cartLevelDiscount;
	        
                global $smarty;       
		$smarty->assign("totalAmount",$totalAmount);
		$smarty->assign("amount",$amount);
		//======================
		$productsInCart = \MCartUtils::convertCartToArray($defaultCart);
		$smarty->assign("productsInCart",$productsInCart);
		
		$smarty->assign("login",$login);
		$smarty->assign("customerName",$customerName);
		$cartHtml = $smarty->fetch("inc/cart-mailer.tpl", $smarty);
                return array('cartHtml'=>$cartHtml,'totalAmount'=>$totalAmount);
		
    }

    private function isValidNotification(Notification $notification){
	global $XCART_SESSION_VARS;
        if($XCART_SESSION_VARS['notification_tokens'] && in_array('timeLimit',$XCART_SESSION_VARS['notification_tokens'])){
          $XCART_SESSION_VARS['notification_tokens']=array_diff($XCART_SESSION_VARS['notification_tokens'],array(Notification::PAYMENT_PAGE_TIME_LIMIT_CONSTANT));
          return true;
        }
        return false;
    }

}
