<?php
namespace notification\dao;
use notification\Notification;
/**
 * DB Adapter class for notification 
 * @author lohiya
 *
 */
class NotificationDAO{

    private $table = "notification";
    

    /**
     * Constructor.
     */
    public function __construct()
    {
    }
    /*
     * create Notification in DB
     */
    public function createNotification(Notification $notification){
          $cols = $notification->getDBArray(); 
          $insert_id = func_array2insertWithTypeCheck($this->table, $cols);
          return $insert_id;
    }
	
}
