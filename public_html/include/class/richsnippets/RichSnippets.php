<?php

namespace richsnippets;

include_once($xcart_dir."/include/class/class.feature_gate.keyvaluepair.php");

class RichSnippets {
    private $isEnabled;

    public function getIsEnabled(){
        if(!isset($this->isEnabled))
            $this->isEnabled=\FeatureGateKeyValuePairs::getBoolean('RichSnippetsEnabled');
        return $this->isEnabled;
    }
}
