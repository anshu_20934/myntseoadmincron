<?php

namespace richsnippets;
use macros\PageMacros;

class PDPRichSnippets extends RichSnippets{

    private  $macrosObj;
    
    public function __construct($macrosObj){
        if(get_class($macrosObj) != 'macros\PageMacros')
            $this->macrosObj = new PageMacros();
        else
            $this->macrosObj = $macrosObj;
    }

    public function getDisplayFinalPrice(){
        if($this->getIsEnabled()){
            $displayFinalPrice = number_format(round($this->macrosObj->discounted_price),0,'.',',');
            /*if($this->macrosObj->discount > 0 ){
                $displayFinalPrice = number_format(round($this->macrosObj->price - $this->macrosObj->discount),0,'.',',');
            } else {
                $displayFinalPrice = number_format(round($this->macrosObj->price),0,'.',',');
            }
            $displayFinalPrice = $this->macrosObj->price;*/
        } else $displayFinalPrice = '';
        return $displayFinalPrice;
    }
}
