<?php

namespace mobile\dao;
use cache\keyvaluepairs\MobileUAWhitelistKeyValuePairs;

use base\dao\BaseDAO;
include_once("$xcart_dir/include/class/widget/class.widget.keyvaluepair.php");
include_once("$xcart_dir/include/func/func.utilities.php");

/*
* Dao for Mobile User Agent Whitelist 
*/
class MobileUAWhitelistDBAdapter extends BaseDAO {

    private static $key = "mobileUAWhitelist";

    public function getWhitelist(){
        $result = MobileUAWhitelistKeyValuePairs::getKeyValuePairInstance()->getJSONDecodedArray(self::$key);
        return $result;
    }

    public function updateWhitelist($data){
        if(empty($data) || !is_array($data)){
            $data = array();
        }
        $data = json_encode($data);
        \WidgetKeyValuePairs::setWidgetValueForKey(null, self::$key, $data);
        \WidgetKeyValuePairs::refreshKeyValuePairsInCache();
        MobileUAWhitelistKeyValuePairs::getKeyValuePairInstance()->refresh();
    }
}
