<?php
include_once "$xcart_dir/include/class/class.dynamic_content.php";
include_once "$xcart_dir/include/class/widget/class.widget.skulist.php";

/**
 * DContent for recently visited PDP
 * @author yogesh
 *
 */
class RecentViewContent extends DContent{
	private static  $RECENT_VIEWED_COOKIE="RVC001";
	private static  $RECENT_HISTORY_LIMIT=15;
	private static  $RECENT_VIEW_SCROLLABLE_PAGE_SIZE=4;
	private $recent_styles;
	private $styles_to_show;
	private $content;
	private $smarty;
	private $recent_styles_products;
	private $recent_div_content=null;
	private $no_contents=false;
	private $ga_action;
	private $ga_category;
	function __construct ($smarty,$name,$tag,$ga_category,$ga_action)
	{
		parent::__construct($name, $tag);
		$this->smarty=$smarty;
		$this->ga_action=$ga_action;
		$this->ga_category=$ga_category;
		
		$recent_views=array();
		$recent_views_cookie_content = getMyntraCookie(RecentViewContent::$RECENT_VIEWED_COOKIE);
		if(!empty($recent_views_cookie_content))
			$recent_views=explode(',',$recent_views_cookie_content);
		$recent_views=array_unique($recent_views);
		$solrSearchProducts = SearchProducts::getInstance();		
		$multiStyleSolrData=@$solrSearchProducts->getMultipleStyleFormattedSolrData($recent_views);
		$products = array();
		
		$recent_views_aux = array();

		foreach ($multiStyleSolrData as $singleStyleSolrData){
			if(!empty($singleStyleSolrData)) {
				$singleStyleSolrData = WidgetUtility::getWidgetFormattedData($singleStyleSolrData);
				if(!empty($singleStyleSolrData['count_options_availbale'])&&!$singleStyleSolrData['count_options_availbale']<=0){
					$recent_views_aux[]=$singleStyleSolrData['styleid'];
					$products[] = $singleStyleSolrData;
				}
			}			
		}
		
		
		$recent_views=array_slice($recent_views_aux,0,RecentViewContent::$RECENT_HISTORY_LIMIT);
		$recent_views_products = array_slice($products,0,RecentViewContent::$RECENT_HISTORY_LIMIT);
		$this->recent_styles_products = $recent_views_products;
		$recent_views_str=implode(',',$recent_views);
		$this->recent_styles=$recent_views;
		$this->styles_to_show=$recent_views;
		global $xcart_http_host,$xcart_https_host;
		if(!empty($recent_views_cookie_content)){
			setMyntraCookie(RecentViewContent::$RECENT_VIEWED_COOKIE,$recent_views_str,time()+3600*24*15,'/',$xcart_http_host);
		}
		
		if(empty($this->recent_styles)||count($this->recent_styles)==0)
			$no_contents=true;
	}
	
	/**
	 * Any call to this function should be made before anything is written to output
	 * @param unknown_type $style_id
	 */
	function addStyleToRecentViews($style_id){
		if(empty($style_id))
			return;
		
		$recent_views=array();
		$recent_views_cookie_content = getMyntraCookie(RecentViewContent::$RECENT_VIEWED_COOKIE);
		if(!empty($recent_views_cookie_content))
			$recent_views=explode(',',$recent_views_cookie_content);
		$solrSearchProducts = SearchProducts::getInstance();
		$product=@$solrSearchProducts->getSingleStyleFormattedSolrData($style_id);
		if(!empty($product)&&!empty($product['count_options_availbale'])&&!$product['count_options_availbale']<=0){
			array_unshift($recent_views,$style_id);
		}
		$recent_views=array_unique($recent_views);
		$recent_views=array_slice($recent_views,0,RecentViewContent::$RECENT_HISTORY_LIMIT);
		$recent_views_str=implode(',',$recent_views);
		global $xcart_http_host,$xcart_https_host;
		setMyntraCookie(RecentViewContent::$RECENT_VIEWED_COOKIE,$recent_views_str,time()+3600*24*15,'/',$xcart_http_host);
		return $recent_views;
	}
	
	private function getRecentViewedWidgetStyle(){
		global $shownewui;
		if(empty($this->styles_to_show))
			return null;
		$recent_viewed_count=count($this->recent_styles);
		$this->smarty->assign("recent_viewed_count",$recent_viewed_count);
		$r_count_limit=5;

		/****If recently viewed product count less than 5 fill the space with recommended products****/
			if($recent_viewed_count < $r_count_limit){
				$solrSearchProducts = SearchProducts::getInstance();
				$product=$this->recent_styles_products[0];//@$solrSearchProducts->getSingleStyleFormattedSolrData($this->recent_styles[0]);
				$similarProducts=$solrSearchProducts->getRelatedProductsPDP(explode(',',$product['keywords']),'0_style_'.$this->recent_styles[0],0,8);
				$j=0;
				foreach ($similarProducts as $product){
					if(!empty($product['styleid'])&&!in_array($product['styleid'],$this->styles_to_show)){
						$this->styles_to_show[]=$product['styleid'];
						$this->recent_styles_products[] = WidgetUtility::getWidgetFormattedData($product);
						$j++;
						if($j==($r_count_limit)){
							break;
						}
					}
				}
				
			}	
		
		$widgetData = array();
		$widgetData['data'] = $this->recent_styles_products;
		$widgetData['name'] = "recent views";
		$widgetData['nameclean'] = preg_replace("/[^a-zA-Z0-9\i]/", "_", "recent views");
		return $widgetData;
	}
	
	public function getContent(){
		global $shownewui;
		if($this->recent_div_content!=null){
			return  $this->recent_div_content;
		}
		if(!$this->no_contents ){
			$recent_widget=$this->getRecentViewedWidgetStyle();
			if($recent_widget==null)
				return "";
			if(count($this->recent_styles)<RecentViewContent::$RECENT_VIEW_SCROLLABLE_PAGE_SIZE){
				$this->smarty->assign("pagename","Recommended for you");
			}else{
				$this->smarty->assign("pagename","Recently viewed by you");
			}
			$this->smarty->assign("ga_category",$this->ga_category);
			$this->smarty->assign("ga_action",$this->ga_action);
			$this->smarty->assign("suggested_start",count($this->recent_styles));
			$this->smarty->assign("widget",$recent_widget);
			$this->recent_div_content=$this->smarty->fetch("inc/recent-widget.tpl", $this->smarty);
		}else{
			$this->recent_div_content="";
		}
		
		return $this->recent_div_content;
	}
	
	function setGACategory($category){
		$this->ga_category=$category;
	}
	function setGAAction($action){
		$this->ga_action=$action;
	}
}


$__RECENT_HISTORY_DCONTENT= new RecentViewContent($smarty,"recent_viewed.enable", "{RECENT_WIDGET_PLACEHOLDER}");
$__RECENT_HISTORY_DCONTENT->setGAAction("recently_viewed");
$__DCONTENT_MANAGER->addDContent($__RECENT_HISTORY_DCONTENT);

?>
