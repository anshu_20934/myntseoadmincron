<?php
require_once "$xcart_dir/include/class/class.mail.template.resolver.php";
require_once "$xcart_dir/include/class/class.mail.mailproviderfactory.php";
require_once "$xcart_dir/include/class/class.mail.mailtype.php";
require_once "$xcart_dir/include/func/func.core.php";
require_once "$xcart_dir/Profiler/Profiler.php";

class MultiProviderMailer {
	
	private $mailDetails;
	private $mailDefaults = Array("from_name"=>"Myntra Customer Service", "from_email"=>"support@myntra.com", "headers"=>"Content-Type: text/html");
	private $customKeywords;
	
	public function __construct($mailDetails, $customKeywords) {
		$this->customKeywords = $customKeywords;
		if(!empty($mailDetails['template'])) {
			$templateResolver = new EmailTemplateResolver(
															$mailDetails['template'], $this->customKeywords, 
															$mailDetails['header'], $mailDetails['footer']
														 );
			unset($mailDetails['template']);											 
			$this->mailDetails = $templateResolver->getMailDetails();			
		}
		$this->substituteWithDefaults();
		$this->substituteWithAnother($mailDetails);
	}
	
	/**
	 * Set default parameters of mail_details: if those fields are blank, corresponding defaults would be used
	 * @param unknown_type $defaults
	 */
	public function setMailDefaults($defaults) {
		$this->mailDefaults = $defaults;
		$this->substituteWithDefaults();				
	}
	
	/**
	 * Substitute Mail details values with default if empty
	 * @param unknown_type $mail_details
	 * @param unknown_type $defaults
	 */
	protected function substituteWithDefaults() {
		$this->substituteWithAnother($this->mailDefaults);	
	}
	
	/**
	 * Substitute Mail details values with default if empty
	 * @param unknown_type $mail_details
	 * @param unknown_type $defaults
	 */
	protected function substituteWithAnother($otherMailDetails) {
		foreach ($otherMailDetails as $key=>$value) {
			if(	!empty($otherMailDetails[$key])) {
				$this->mailDetails[$key] = $otherMailDetails[$key];
			}
		}		
	}
	
	/**
	 * This is the top-level method to be invoked for sending mail. this uses the appropriate mail provider based on mail_type
	 */
	public function sendMail() {
        $profiler_handle = Profiler::startTiming("sendMail");
		global $maillog;
		$providers = MailType::getProvidersForMailType($this->mailDetails['mail_type']);
		foreach ($providers as $provider) {
			try {
				$maillog->info("using provider to send mail: ".get_class($provider));
				$retval = $provider->sendMail($this->mailDetails);
				if($retval) {
					$maillog->info("mail sending was successful using provider: ".get_class($provider));
                    Profiler::endTiming($profiler_handle);
					return true;
				}
			}
			catch (Exception $e) {
				$maillog->error("failed sending mail using provider:".get_class($provider));
				$maillog->error($e->getMessage().":".$e->getFile().":".$e->getLine()."\n".$e->getTraceAsString());
			}
		}
		$maillog->error("Not able to send mail through any provider");
                Profiler::endTiming($profiler_handle);
		return false; 
	}
}

?>