<?php
/******************************************************************************#
 #=============================================================================#
 | Myntra                                                                      |
 | Copyright (c) 2008 Myntra, Arun Kumar		                       		   |
 | All rights reserved.                                                        |
 |-----------------------------------------------------------------------------|
 |                                                                             |
 | The Initial Developer of the Original Code is Arun Kumar             	   |
 #=============================================================================#
 #*****************************************************************************/

/*
## Class With functions to generate report
*/
class report{
	
	
	/*
	##function to gives the time stamp of the following one at a time
	##based on the parameter daily,weekly,monthly,quarterly and yearly
	##all time stamp has been taken according to 12:00AM in the morning
	##1)start of the day 12:00AM
	##2)an array week start date and end date
	##3)start of the month
	##4)an array of start of the quarter and end of the quarter
	##5)start of the year
	*/
	function _get_timestamp($frequency=''){
		$year = date('Y');
		$month = date('m');
		$day = date('d',time());
		
		if(empty($frequency))
			$frequency = 'daily';
		
		switch ($frequency){
			case 'daily':
							return $beginning_of_the_day = mktime(0,0,0,$month,$day,$year);				
							break;
							
			case 'weekly':
							$isoday = date('N',mktime(0,0,0,$month,$day,$year));//day of the week
							$monday = strtotime('-'.($isoday-1).'day',mktime(0,0,0,$month,$day,$year));//beginning of the week(monday)
							$next_monday = $monday + (86400*7);//till 12:00AM i.e till complete sunday
							return $week = array('START'=>$monday,'END'=>$next_monday);
							break;
							
			case 'monthly':
							return $beginning_of_the_month = mktime(0,0,0,$month,1,$year);
							break;
			case 'quarterly':
							if($month  >= 1  && $month <= 3 ) {
								$from = mktime(0,0,0,1,1,$year);
								$to =    mktime(23,59,59,3,31,$year);
											
							}else if($month  >= 4  && $month <= 6){			
							    $from = mktime(0,0,0,4,1,$year);
							    $to =    mktime(23,59,59,6,30,$year);			
							    
							}else if($month  >= 7  && $month <= 9){			
							    $from = mktime(0,0,0,7,1,$year);
							    $to =    mktime(23,59,59,9,30,$year);			
							    
							}else if($month  >= 10  && $month <= 12){			
							    $from = mktime(0,0,0,10,1,$year);
							    $to =    mktime(23,59,59,12,31,$year);			
							}
							return $quarter = array('START'=>$from,'END'=>$to);
							break;
			case 'yearly':
							return $beginning_of_the_year = mktime(0,0,0,1,1,$year);
							break;
		}		
	}

	/*
	##Function to generate revenue report of all sources
	*/
	function func_create_revenue_query($params){
		global $sql_tbl;
		/*
		##calculate the period for which revenue report to
		##be generated
		*/
		$term = report::_get_timestamp($params['TERM']);
		
		$sql = "SELECT 
					sum(o.total) as revenue,
					s.source_name,
					st.type_name,
					loc.location_name					
				FROM
					{$sql_tbl['orders']} o 
					
				LEFT JOIN
					{$sql_tbl['sources']} s
				ON
					s.source_id = o.source_id
												
				LEFT JOIN
					{$sql_tbl['source_types']} st
				ON
					st.type_id = s.source_type_id
					
				LEFT JOIN
					{$sql_tbl['source_locations']} loc
				ON
					loc.location_id = s.location_id";
		
		
		$sql .= " WHERE
					o.orgid = '0'
				 AND
				 	o.queueddate IS NOT NULL
				 AND
				 	o.source_id IS NOT NULL
				 AND
				 	o.source_id != '0'
				 AND
				 	st.active = '1'";
		
		
		if(is_array($term)){
			$sql .= " AND o.queueddate BETWEEN  '{$term['START']}' AND '{$term['END']}'";
		}else {
			$sql .= " AND o.queueddate > '{$term}'";
		}
		
		if($params['GROUP_BY'] == 'source_type') {
			$sql .= " GROUP BY st.type_name";
		}
		elseif($params['GROUP_BY'] == 'source_name') {
			$sql .= " GROUP BY s.source_name";
		}
		elseif($params['GROUP_BY'] == 'source_location') {
			$sql .= " GROUP BY loc.location_name";
		}			
		else{
			$sql .= " GROUP BY s.source_name";
		}	
		

		if($params['ORDER_BY'] == 'source_type') {
			$sql .= " ORDER BY st.type_name";
		}
		elseif($params['ORDER_BY'] == 'source_name') {
			$sql .= " ORDER BY s.source_name";
		}
		elseif($params['ORDER_BY'] == 'source_location') {
			$sql .= " ORDER BY loc.location_name";
		}
		elseif($params['ORDER_BY'] == 'revenue') {
			$sql .= " ORDER BY revenue";
		}			
		else{
			$sql .= " ORDER BY s.source_name";
		}

		if($params['SORT_ORDER'] == 'desc') {
			$sql .= " DESC";
		}
		else {
			$sql .= " ASC";
		}

		//echo "<br/>sql=>{$sql}<br/>";
		return $sql;
	}
}
