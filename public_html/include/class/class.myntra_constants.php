<?php
##########################################################################
## Author	: Shantanu Bhadoria											##
## Date		: 14 Oct'2008												##
## Copyright Myntra Designs												##
## 																		##
## Myntra createorder class												##
## This class contains all the constants associated with myntra system	##
##########################################################################

require_once 'class.ez_sql.mysql.php';

class myntra_constants {
	public static $sql_tbl = array (
			'comm_track'									=> 'mk_comm_generated_tracking',
			'customers'										=> 'xcart_customers',
			'designer_commission'							=> 'mk_designer_commission',
			'images_T'										=> 'xcart_images_T',
			'order_details'									=> 'xcart_order_details',
			'orders'										=> 'xcart_orders',
			'mk_customization_orientation'					=> 'mk_customization_orientation',
			'mk_email_notification'							=> 'mk_email_notification',
			'mk_product_options'							=> 'mk_product_options',
			'mk_product_options_order_details_rel'			=> 'mk_product_options_order_details_rel',
			'mk_product_style'								=> 'mk_product_style',
			'mk_product_type'								=> 'mk_product_type',
			'mk_product_type_stylegroup_map'				=> 'mk_product_type_stylegroup_map',
			'mk_product_type_synonyms'						=> 'mk_product_type_synonyms',
			'mk_style_category_map'							=> 'mk_style_category_map',
			'mk_style_customization_area'					=> 'mk_style_customization_area',
			'mk_style_customization_techniques_map'			=> 'mk_style_customization_techniques_map',
			'mk_style_images'								=> 'mk_style_images',
			'mk_template_master'							=> 'mk_template_master',
			'template_orient_map'							=> 'mk_template_orientation_map',
			'mk_xcart_order_customized_area_image_rel'		=> 'mk_xcart_order_customized_area_image_rel',
			'mk_xcart_order_details_customization_rel'		=> 'mk_xcart_order_details_customization_rel',
			'mk_xcart_product_customized_area_image_rel'	=> 'mk_xcart_product_customized_area_image_rel',
			'mk_xcart_product_customized_area_rel'			=> 'mk_xcart_product_customized_area_rel',
			'products'										=> 'xcart_products',
			'shipping_time'									=> 'mk_shipping_time',
			'style_group' 									=> 'mk_style_group',
			'style_review_comments'							=> 'mk_style_review_comments',
			'style_template'								=> 'mk_style_templates',	
			'stylegroup_style_map'							=> 'mk_stylegroup_style_map',
			'template_orient_map'							=> 'mk_template_orientation_map',
			'types'											=> 'mk_product_type'
			);
}
?>
