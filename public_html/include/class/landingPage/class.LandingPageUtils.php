<?php
include_once "$xcart_dir/include/dao/class/search/class.MyntraPageDBAdapter.php";
class LandingPageAdminActionType{
	static $ADD = 'add';
	static $DELETE = 'delete';
	static $UPDATE = 'update';
    static $SEARCH = 'search';
}

class LandingPageAdmin{

	private $actionType;
    private $search_keyword;
    private $page_size = 50;
    private $page = 0;

	public function __construct(){
		if(!empty($_POST['mode'])){
			$this->actionType=$_POST['mode'];
		}
        if(!empty($_POST['search_keyword'])){
            $this->search_keyword = $_POST['search_keyword']; 
        }
        if(!empty($_POST['page'])){
            $this->page = $_POST['page']; 
        }
        if(!empty($_POST['page_size'])){
            $this->page_size = $_POST['page_size']; 
        }
	}

	public function getAction(){
		return $this->actionType;
	}

	public function executeAction(){
		$db_adapter = MyntraPageDBAdapter::getInstance();
		if($this->actionType == LandingPageAdminActionType::$ADD){
			$parameterized_page_id = $db_adapter->getParametrizedIdForKey($_POST['page_key']);
			if($db_adapter->isLandingPage($_POST['page_url'])){
				$top_message["content"] = "Landing page:: ".$_POST['page_url']." Already exist !!";
			}else if(!empty($parameterized_page_id)){
				$query_data = array(
	                            "page_url"  =>$_POST['page_url'],
	                            "parameterized_page_id"  =>$parameterized_page_id,
	                            "is_autosuggest"  =>$_POST['is_autsuggest'],
	                            "alt_text"  =>$_POST['alt_text']
				) ;
				$db_adapter->saveOrUpdateMyntraLandingPage($query_data);
				$top_message["content"] = "Landing page Added successfully for url ".$_POST['page_url']."!!";
			}else{
				$top_message["content"] = "Unable to add Landing page for key:: ".$_POST['page_key']."!!";
			}

		}elseif ($this->actionType == LandingPageAdminActionType::$UPDATE){
			$posted_data = $_POST['posted_data'];
			if (is_array($posted_data))
			{
				foreach ($posted_data as $id=>$v)
				{
					if (empty($v["to_delete"]))
					continue;

					// Get parametrized page id
					$parameterized_page_id = $db_adapter->getParametrizedIdForKey(trim($v['page_key']));
					if(!empty($parameterized_page_id) && !empty($v["page_url"])){
						$query_data = array(
								"id" => trim($v["gid"]),
	                            "page_url"  =>(trim($v["page_url"])),
	                            "parameterized_page_id"  =>($parameterized_page_id),
	                            "is_autosuggest"  =>((!empty($v['is_autosuggest']))?1:0),
	                            "alt_text"  =>$v['alt_text']
						) ;
						$db_adapter->saveOrUpdateMyntraLandingPage($query_data);
					}
				}
				$top_message["content"] = "Landing page edited successfully!!";

			}

		}elseif ($this->actionType == LandingPageAdminActionType::$DELETE){
			$posted_data = $_POST['posted_data'];
			if (is_array($posted_data))
			{
				foreach ($posted_data as $id=>$v)
				{
					if (empty($v["to_delete"]))
					continue;

					// Get parametrized page id
					$landing_page_id = trim($v["gid"]);
					$db_adapter->deleteMyntraLandingPage($landing_page_id);
				}
			}
			$top_message["content"] = "Deleted Landing page !!";

		}else if($this->actionType == LandingPageAdminActionType::$SEARCH){
			$top_message["content"] = "searching for :: ". $this->getSearchKeyword() ." !!";
        }
		return $top_message;
	}

    public function getSearchKeyword(){
        return $this->search_keyword;
    }
    
    public function getPageSize(){
        return $this->page_size;
    }

    public function getPage(){
        return $this->page;
    }

}
