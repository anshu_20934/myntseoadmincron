<?php

/**
 * FTPUploader
 * @author yogesh
 *
 */
require_once dirname(__FILE__).'/class.Uploader.php';

class FTPActions extends Uploader {
	private $connectionHandle;
	private $userLoggedin = false;
	private $mode = FTP_BINARY;
	private $startpos = null;
	private $usePasvMode = true;
	
	public function __construct($host,$autoDisconnectHost=true,$usePasvMode=true){
		parent::__construct($host,$autoDisconnectHost);
	}
	
	public function __destruct(){
		parent::__destruct();
	}
	
	protected function connectHost(){
		$connectionHandle=ftp_connect($this->host);
		$this->connectionHandle=$connectionHandle;
		if(empty($connectionHandle)){
			return false;
		}
		return true;
	}
	
	protected function disconnectHost(){
		return ftp_close($this->connectionHandle);
	}
	
	public function login($user='',$password=''){
		$result=ftp_login($this->connectionHandle, $user, $password);
		//Turn on FTP Passive Mode: required if the user is behind firewall
		ftp_pasv($this->connectionHandle, $this->usePasvMode);
		if(empty($result)){
			$this->userLoggedin=false;
			return false;
		}
		$this->userLoggedin = true;
		return true;
	}
	
	protected function uploadFile($srcfile,$destfile){
		if(!$this->userLoggedin){
			return false;
		}
		$upload = ftp_put($this->connectionHandle, $destfile, $srcfile, $this->mode,$this->startpos);
		
		return $upload;
	}
	
	public function setMode($mode){
		$this->mode=$mode;
	}
	
	public function setStartPostion($startpos){
		$this->startpos=$startpos;
	}
	
	public function mkdir($directory){
		return ftp_mkdir($this->connectionHandle, $directory);
	}
	
	public function chdir($directory) {
		return ftp_chdir($this->connectionHandle, $directory);
	}
	
	public function rmdir($directory) {
		return ftp_rmdir($this->connectionHandle, $directory);
	}
	
	public function delete($pathToFile) {
		return ftp_delete($this->connectionHandle, $pathToFile);
	}
	
	public function listFiles($directory) {
		return ftp_nlist($this->connectionHandle, $directory);
	}
	
	public function chdirup() {
		return ftp_cdup($this->connectionHandle);
	}
	
	public function isdir($file) {
		if($this->chdir($file)) { 
        	$this->chdirup(); 
        	return true; 
    	} 
	    else { 
	        return false; 
	    } 
	}
	
	public function deleteDirectoryRecursively($directory) {
		$files = $this->listFiles($directory);
		$this->chdir($directory);
		foreach ($files as $file) {
			if($this->isdir($file)) {
				$this->deleteDirectoryRecursively($file);
			}
			else {
				$this->delete($file);	
			}
		}
		$this->chdirup();
		$this->rmdir($directory);
	}
}