<?php

class MongoDBSearch {
	protected $_collection;
	protected $_logger;

    protected function init() {
	    $this->_logger=$GLOBALS['errorlog'];
    }
	
	public function __construct($host, $port, $db,$collection, $options = array())
	{
        $this->init();
		$connection_string = "mongodb://".$host.":".$port;
        $options = empty($options) ? array() : $options;

        try {
            $client = new MongoClient($connection_string, $options);
    		$this->_collection = $client->selectCollection($db,$collection);
        } catch (MongoConnectionException $e) {
            $this->_logger->error("mongo: MongoConnectionException ".$e->getMessage()); 
            throw new Exception("mongo: MongoConnectionException");
        } catch (InvalidArgumentException $e) {
            $this->_logger->error("mongo: InvalidArgumentException ".$e->getMessage()); 
            throw new Exception("mongo: InvalidArgumentException");
        }
	}
	
	/*
	 * @param string $queries The raw query string
	 * @param int $offset The starting offset for result documents
	 * @param int $limit The maximum number of result documents to return
	 * @param string $sortField Field to sort with 
	 * @return mongocursor object
	 */
	public function executeQuery($queries, $start=0, $limit=20, $sortField='',$params=array())
	{

		$searchDocuments = $this->_collection->find($queries)->sort($sortField)->skip($start)->limit($limit)->timeout(120000);
		/*
		foreach ($searchDocuments as $doc) {
			echo "[".$doc['keyword']."]=> [".$doc['count_of_products']."]";
		} */ 
		return $searchDocuments;
		
	}
	
	/*
	 * Returns the number of entries for the search query
	 */
	public function getNumEntries($query)
    {
         $numEntries = $this->_collection->find($query)->count();
         return $numEntries;
    }
	
}
?>
