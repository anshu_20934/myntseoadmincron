<?php
/**
 * Created by IntelliJ IDEA.
 * User: Mohammed Asif Hussain
 * Date: Jan 19, 2010
 * Time: 10:18:55 AM
 * To change this template use File | Settings | File Templates.
 */

class HomePage {

    var $HOME_PAGE = 'home_page';

    var $HOME_PAGE_CACHE_TIMEOUT = 900;

    var $FEATURED_CACHE = 'home_page_featured_designs';

    var $FEATURED_CACHE_TIMEOUT = 604800;

    var $PRODUCT_TYPES_CACHE = 'home_page_product_types';

    var $PRODUCT_TYPES_CACHE_TIMEOUT = 604800;

    var $featured_designs;

    var $product_types;

    var $testimonials;
    var $TESTIMONIALS_CACHE = 'home_testimonials_cache';

    var $images;
    var $IMAGES_CACHE = 'home_images_cache';

    var $tabs;
    var $TABS_CACHE = 'home_tabs_cache';

    var $tabs_content;
    var $TABS_CONTENT_CACHE = 'home_tabs_content_cache';

    var $properties;
    var $PROPERTIES_CACHE = 'home_properties_cache';

    var $cache;

    var $cacheable = NULL;

    var $home_page_cache = NULL;

    public function __construct() {
        $this->cache = new XCache();
    }

    /**
     * @return #P#CHomePage.product_types|?
     */
    function get_product_types() {
        if ($this->product_types == NULL) {
            $this->product_types = $this->cache->fetch($this->PRODUCT_TYPES_CACHE);
            if ($this->product_types == NULL) {
                $this->product_types = execute_sql(
                    "SELECT p.*  FROM mk_product_style s, mk_product_type p  WHERE s.is_customizable = 1 and p.id = s.product_type and p.is_active = 1 and p.type = 'p' group by p.id order by display_order asc"
                );
                $this->cache->store($this->PRODUCT_TYPES_CACHE, $this->product_types, $this->PRODUCT_TYPES_CACHE_TIMEOUT);
            }
        }
        return $this->product_types;
    }

    /**
     * @return #P#CHomePage.product_types|?
     */
    function get_images($type) {
        global $nocache;
        if ($this->images == NULL) {
            $this->images = $this->cache->fetch($this->IMAGES_CACHE);
            if ($this->images == NULL || $nocache == 1) {
                $images = func_select_query(
                    'mk_widget_banners',
                    array('banner_type' => $type),
                    NULL,
                    NULL,
                    array('display_order' => 'asc')
                );
                $this->images  = $images;
                if($nocache != 1){//Do not store if no cache is set
                    $this->cache->store($this->IMAGES_CACHE, $this->images);
                }
            }
        }
        return $this->images;
    }


    /**
     * @return #P#CHomePage.product_types|?
     */
    function get_tabs() {
        if ($this->tabs == NULL) {
            $this->tabs = $this->cache->fetch($this->TABS_CACHE);
            if ($this->tabs == NULL) {
                $tabs = func_select_query(
                    'mk_tabs',
                    array(
                        'enabled' => 1
                    ),
                    NULL,
                    NULL,
                    array('display_order' => 'asc')
                );
                foreach ($tabs as $tab) {
                    $this->tabs [$tab['id']] = $tab;
                }

                $this->cache->store($this->TABS_CACHE, $this->tabs);
            }
        }
        return $this->tabs;
    }

    /**
     * @return #P#CHomePage.product_types|?
     */
    function get_tabs_content() {
        if ($this->tabs_content == NULL) {
            $this->tabs_content = $this->cache->fetch($this->TABS_CONTENT_CACHE);
            if ($this->tabs_content == NULL) {
                $this->tabs_content = array();
                foreach ($this->tabs as $tab) {
                    $products = func_select_query(
                        'mk_tab_content',
                        array(
                            'tab_id' => $tab['id'],
                            'enabled' => 1
                        ),
                        NULL,
                        NULL,
                        array('display_order' => 'asc')
                    );

                    foreach ($products as $prod) {
                        $this->tabs_content[$tab['id']] [] = func_select_first('xcart_products', array('productid' => $prod['product_id']));
                    }
                }

                $this->cache->store($this->TABS_CONTENT_CACHE, $this->tabs_content);
            }
        }

        return $this->tabs_content;
    }

    function get_properties() {
        if ($this->properties == NULL) {
            $this->properties = $this->cache->fetch($this->PROPERTIES_CACHE);
            if ($this->properties == NULL) {
                $properties = func_select_query(
                    'mk_config',
                    array(
                        'enabled' => 1,
                        'page' => 'home'
                    )
                );

                foreach ($properties as $property) {
                    $list = $this->properties[$property['type']];
                    $list[$property['property']] = $property;
                    $this->properties[$property['type']]  = $list;
                } 

              
                $this->cache->store($this->PROPERTIES_CACHE, $this->properties);

            }
        }
        return $this->properties;
    }

    /**
     * @return #M#P#CHomePage.cache.fetch|?
     */
    function get_home_page($productuiABparam) {
        global $xcart_http_host, $smarty, $menu, $nocache, $shownewui;
        $this->home_page_cache = $this->cache->fetch($xcart_http_host . '-' . $this->HOME_PAGE . '-' . $productuiABparam);
        if($this->home_page_cache == NULL || $nocache == 1){
            return null;
        }
        
        $smarty->assign("home_page_content", $this->home_page_cache);
        $smarty->assign("shownewui", $shownewui);
        global $skin;
        if($skin === "skin2") {
        	$home_page_content = $smarty->fetch("index.tpl");
        }
        else {
        	$home_page_content = func_display("myntra/myntra_home.tpl", $smarty, false);
        }
        return $home_page_content;
    }

    /**
     * @param  $cache
     * @return #M#P#CHomePage.cache.store|?
     */
    function set_home_page($cache, $productuiABparam) {
        global $xcart_http_host, $nocache;
        if($nocache == 1){// If no cache is set then do not store/update
            return;
        }
        
        $this->home_page_cache = $cache;
        return $this->cache->store($xcart_http_host . '-' . $this->HOME_PAGE . '-' . $productuiABparam, $cache, $this->HOME_PAGE_CACHE_TIMEOUT);
    }

}
