<?php
use cache\InvalidateXCache;
	class AllSizeKey{
		
		const ALL_SIZE_ARRAY = 'all_size_array';
		public static function GetAllSize()
		{
			$SynonymQuery = "select * from mk_all_size_order order by intorder";
			$result = func_query($SynonymQuery);
			$allSizeArray = array();
			$synonymFlag = false;
			foreach ($result as $key => $row)
			{
				$row[StringSize] = strtolower($row[StringSize]);
				$size = (string) $row[StringSize];
				$size = "$size";
				$allSizeArray["$size"] = $row[intorder];
			}
			return $allSizeArray;
		}
		public static function refreshKeyValuePairsInCache() {
			$xCacheInvalidator =  InvalidateXCache::getInstance();
			$xCacheInvalidator->invalidateCacheForKey(self::ALL_SIZE_ARRAY);
			global $xcache ;
			if($xcache==null){
				$xcache = new XCache();
			}
			$cachedAllSizeParameters = AllSizeKey::GetAllSize();
    		$xcache->store(self::ALL_SIZE_ARRAY, $cachedAllSizeParameters, 0);
		}
		
    	public static function getAllFeatureGateKeyValuePairs() {
    		global $xcache;
    		if($xcache==null){
				$xcache = new XCache();
			}
			$cachedAllSizeParameters = $xcache->fetch(self::ALL_SIZE_ARRAY);
  			if($cachedAllSizeParameters == NULL) {
    			 $cachedAllSizeParameters = AllSizeKey::GetAllSize();
    			 $xcache->store(self::ALL_SIZE_ARRAY, $cachedAllSizeParameters, 0);
    		}
    		return $cachedAllSizeParameters;
    	}
	}

?>