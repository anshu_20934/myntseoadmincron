<?php
namespace landingpage\action;
use enums\base\ActionType;
use landingpage\manager\SectionManager;

/**
 * Action to be invoked when uploading an image
 * //TODO: this should ideally extend: BaseAuthenticatedAuthorizedAction: need to write such a class 
 * @author kundan
 */
class SectionMoveAction extends BaseSectionAction {
	
	protected $sectionid,$newsortorder,$sectionidtoswap,$oldsortorder;
	
	public function __construct($sectionid,$newsortorder,$sectionidtoswap,$oldsortorder,$lpid){
		parent::__construct($lpid);
		$this->sectionid = $sectionid;
		$this->newsortorder = $newsortorder;
		$this->sectionidtoswap = $sectionidtoswap;
		$this->oldsortorder = $oldsortorder;
		$this->init();
		
	}
	
	
	public function run() {
		$this->manager->updateSortOrder($this->sectionid,$this->newsortorder,$this->sectionidtoswap,$this->oldsortorder);
	}
}