<?php
namespace landingpage\action;
use enums\pageconfig\PageConfigLocationConstants;
use enums\base\ActionType;
use landingpage\action\BaseSectionAction;
use landingpage\manager\SectionManager;
use landingpage\manager\LPManager;

class SectionViewAction extends BaseSectionAction {
	//constants related to page: storyboard
	protected $lpid;
	public function __construct($lpid='') {
		$this->lpid = $lpid;
		parent::__construct($lpid);
		$this->init();
	}
	
	public function run() {
		global $smarty;
		$this->assignSmartyConsts();
		if(!empty($this->lpid)){

			$sectionDetails=$this->manager->getAllSections($this->lpid);
			$sortOrder = $this->manager->getHighestSortOrderVal($this->lpid);
			$sortOrder =$sortOrder[0]['max_sort_order']+1;	
			
		}
		$lpmanager = new LPManager();
		$lpDetails=$lpmanager->getLPDetailsByLPId($this->lpid);	
		$smarty->assign('lpDetails',$lpDetails[0]);
		$smarty->assign("lpDetailsJSON",json_encode($lpDetails[0]));
		$smarty->assign('sectionDetails',$sectionDetails);
		$smarty->assign('sortOrderForNextSection',$sortOrder);
		$smarty->assign('lpid',$this->lpid);
		$smarty->assign("main","sectioncontroller");
		func_display("admin/home.tpl",$smarty);
	}
	
	private function assignSmartyConsts() {
		global $smarty;
		
		$smarty->assign("postBackURL", $this->getRequestedURL());

	}
}
