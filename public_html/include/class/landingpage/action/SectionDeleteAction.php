<?php
namespace landingpage\action;
use enums\base\ActionType;
use landingpage\manager\SectionManager;

/**
 * Action to be invoked when uploading an image
 * //TODO: this should ideally extend: BaseAuthenticatedAuthorizedAction: need to write such a class 
 * @author kundan
 */
class SectionDeleteAction extends BaseSectionAction {
		
	protected $lpid,$sectionid;
	
	public function __construct($sectionid,$lpid) {
		parent::__construct($lpid);
		$this->sectionid = $sectionid;
		$this->lpid = $lpid;
		$this->init();
		
	}
	
	public function run() {
		$return = $this->manager->deleteSectionData($this->sectionid);
		global $smarty;
		$smarty->assign('status','section deleted');
		$smarty->assign('statuscode',1);
	}
}