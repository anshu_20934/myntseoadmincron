<?php
namespace landingpage\action;
use enums\base\ActionType;
use landingpage\manager\LPManager;

class LPAddAction extends BaseLPAction {
	
	protected $lpdata;
	
	public function __construct($title = '',$stitle='',$name,$description = '',$image = '') {
		parent::__construct();
		$this->lpdata = array(
				"title"=> $title,
				"lp_name"=> strtolower($name),
				"description" => $description,
				"image" => $image,
				"subtitle"=>$stitle 
			);
		$this->init();
		
	}
	
	public function run() {
		$return = $this->manager->addLP($this->lpdata);
		global $smarty;
		$smarty->assign('status','landing page details added');
		return $return;
		
	}
}
