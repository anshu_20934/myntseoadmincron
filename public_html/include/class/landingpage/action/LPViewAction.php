<?php
namespace landingpage\action;
use enums\pageconfig\PageConfigLocationConstants;
use enums\base\ActionType;
use landingpage\action\BaseLPAction;
use landingpage\manager\LPManager;

class LPViewAction extends BaseLPAction {
	//constants related to page: storyboard
	protected $lpid;
	public function __construct($lpid='') {
		$this->lpid = $lpid;
		parent::__construct($lpid);
		$this->init();
	}
	
	public function run() {
		global $smarty;
		$this->assignSmartyConsts();
		$locations=$this->manager->getAllPageLocations();//Use new table to fetch the data\
		$locationsKV=$this->manager->getAllPageLocationsKeyValue();
		if(!empty($this->lpid)){
			$lpDetails=$this->manager->getLPDetailsByLPId($this->lpid);	
		}
	
		$showLocation = true;
		$smarty->assign("locations",$locations);
		$smarty->assign("locationsKV",$locationsKV);
		$smarty->assign("locationsJson",json_encode($locations))	;
		$smarty->assign("locationsKVJson",json_encode($locationsKV));
		$smarty->assign("showLocation", $showLocation);
		$smarty->assign("pageLocation", $locationsKV[$this->lpid]);
		$smarty->assign("pageLocationID", $this->lpid);
		$smarty->assign("main","lpcontroller");
		$smarty->assign("lpDetails",json_encode($lpDetails[0]));
		func_display("admin/home.tpl",$smarty);
	}
	
	private function assignSmartyConsts() {
		global $smarty;
		
		$smarty->assign("postBackURL", $this->getRequestedURL());

	}
}
