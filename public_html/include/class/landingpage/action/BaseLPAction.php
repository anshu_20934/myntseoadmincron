<?php
namespace landingpage\action;
use base\action\BaseAuthenticatedAuthorizedAction;
use landingpage\manager\LPManager;
class BaseLPAction extends BaseAuthenticatedAuthorizedAction {
	protected $manager;
	public function __construct($lpid='') {
		$this->lpid = $lpid;
		parent::__construct();

	}

	protected function init() {
		$this->manager = new LPManager($this->lpid);		
	}
	

}