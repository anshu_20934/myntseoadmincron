<?php
namespace landingpage\action;
use enums\base\ActionType;
use landingpage\manager\SectionManager;

/**
 * Action to be invoked when uploading an image
 * //TODO: this should ideally extend: BaseAuthenticatedAuthorizedAction: need to write such a class 
 * @author kundan
 */
class SectionUpdateAction extends BaseSectionAction {
	
	protected $sectiondata,$lpid,$sectionid;
	
	public function __construct($sectionid,$title,$description='',$link,$solrurl,$noofproducts,$iscarousel=0,$sortorder,$lpid) {
		parent::__construct($lpid);
		$this->sectionid = $sectionid;
		$this->lpid = $lpid;
		$this->sectiondata = array(
				"lp_id"=> $lpid,
				"title"=> $title,
				"target_url" => $link,
				"url" => $solrurl,
				"description" => $description,
				"no_of_products" =>  $noofproducts,
				"is_carousel" => $iscarousel,
				"sort_order" =>$sortorder
				);
		$this->init();
		
	}
	
	public function run() {
		$return = $this->manager->updateSectionData($this->sectiondata,$this->sectionid);
		global $smarty;
		$smarty->assign('status','section data updated');
		$smarty->assign('statuscode',1);
	}
	
}
