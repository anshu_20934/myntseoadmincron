<?php
namespace landingpage\action;
use enums\base\ActionType;
use landingpage\manager\SectionManager;

class SectionAddAction extends BaseSectionAction {
	
	protected $sectiondata,$lpid;
	
	public function __construct($title,$description='',$link,$solrurl,$noofproducts,$iscarousel=0,$sortorder,$lpid) {
		parent::__construct($lpid);
		$this->lpid = $lpid;
		global $login;
		$this->sectiondata = array(
				"lp_id"=> $lpid,
				"title"=> $title,
				"target_url" => $link,
				"url" => $solrurl,
				"description" => $description,
				"no_of_products" =>  $noofproducts,
				"is_carousel" => $iscarousel,
				"sort_order"   => $sortorder,
				"created_by" => $login,
				"created_on" => time()
			);
		$this->init();
		
	}
	
	public function run() {
		$return = $this->manager->insertSectionData($this->sectiondata);
		global $smarty;
		$smarty->assign('status','section added');
		return $return;
		
	}
}
