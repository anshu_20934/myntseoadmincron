<?php
namespace landingpage\action;
use base\action\BaseAuthenticatedAuthorizedAction;
use landingpage\manager\SectionManager;
class BaseSectionAction extends BaseAuthenticatedAuthorizedAction {
	protected $manager;
	public function __construct($lpid='') {
		//$this->lpid = $lpid;
		parent::__construct();

	}

	protected function init() {
		$this->manager = new SectionManager($this->lpid);		
	}

	public function getLPid() {
	    return $this->lpid;
	}
	

}