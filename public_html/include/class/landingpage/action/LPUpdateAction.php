<?php
namespace landingpage\action;
use enums\base\ActionType;
use landingpage\manager\LPManager;

/**
 * Action to be invoked when uploading an image
 * //TODO: this should ideally extend: BaseAuthenticatedAuthorizedAction: need to write such a class 
 * @author kundan
 */
class LPUpdateAction extends BaseLPAction {
	
	protected $lpdata,$lpid;
	
	public function __construct($lpid,$title = '',$stitle='',$name,$description = '',$image = '') {
		parent::__construct($lpid);
		$this->lpdata = array(
				"title"=> $title,
				"lp_name"=> strtolower($name),
				"description" => $description,
				"image" => $image,
				"subtitle" => $stitle
			);
		$this->init();
		
	}
	
	public function run() {
		$return = $this->manager->updateLP($this->lpdata,$this->lpid);
		global $smarty;
		$smarty->assign('status','landing page details updated');
		$smarty->assign('statuscode',1);
	}
	
}
