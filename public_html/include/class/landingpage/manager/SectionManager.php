<?php
namespace landingpage\manager;
use base\manager\BaseManager;
use landingpage\dao\SectionDAO;

class SectionManager extends BaseManager {
	private $lpid,$sectionDAO;
	public function __construct($lpid='') {
		$this->lpid = $lpid;
		$this->sectionDAO = new SectionDAO();
	}
	
	public function getAllSections($lpid){
		//modify the result set and retur
		$result = $this->sectionDAO->getSectionsByLPId($lpid);
		return $result;
	}

	public function getAllSectionsCached($lpid){
		global $xcache;
		$cachedValue = $xcache->fetchObject('lp-sections-sectionid-'.$lpid);
		if(!$cachedValue){
			$cachedValue =  $this->sectionDAO->getSectionsByLPId($lpid);
			$xcache->storeObject('lp-sections-sectionid-'.$lpid,$cachedValue);
		}
		return $cachedValue;
	}

	public function getHighestSortOrderVal($lpid){
		//modify the result set and retur
		$result = $this->sectionDAO->getHighestSortOrder($lpid);
		return $result;
	}


	public function insertSectionData($sectiondata){
		return $this->sectionDAO->insertSection($sectiondata);
	}


	public function updateSectionData($sectiondata,$sectionid){
		
		return $this->sectionDAO->updateSection($sectiondata,$sectionid);
	}


	public function deleteSectionData($sectionid){
		return $this->sectionDAO->deleteSection($sectionid);
	}

	public function updateSortOrder($sectionid,$newsortorder,$sectionidtoswap,$oldsortorder){
		//update both rows

		$newSort = array(
					"sort_order"=>$newsortorder
		);

		$oldSort= array(
					"sort_order"=>$oldsortorder
		);

		$this->sectionDAO->updateSection($newSort,$sectionid);
		$this->sectionDAO->updateSection($oldSort,$sectionidtoswap);
	}
}
