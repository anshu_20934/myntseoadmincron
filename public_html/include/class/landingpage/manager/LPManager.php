<?php
namespace landingpage\manager;
use base\manager\BaseManager;
use landingpage\dao\LPDAO;


class LPManager extends BaseManager {
	private $lpid,$lpDAO;
	public function __construct($lpid='') {
		$this->lpid = $lpid;
		$this->lpDAO = new LPDAO();
	}
	
	public function getLPDetailsByLPId($lpid){
		//modify the result set and retur
		$result = $this->lpDAO->getDetailsByLPId($lpid);
		return $result;
	}

	public function getDetailsByLPNameCached($lpname){
		//modify the result set and retur
		global $xcache;
		//if xcache key exists then rertun with else fetch from DB
		$cachedValue = $xcache->fetchObject('lp-name-keyname-'.strtolower($lpname));
		
		if(!$cachedValue){
			$result = $this->lpDAO->getLPDetailsByLPName($lpname);
			$cachedValue = $result[0];
			$xcache->storeObject('lp-name-keyname-'.$lpname,$cachedValue);
		}
		return $cachedValue;
	}

	public function getAllLPs(){
		//modify the result set and retur
		$result = $this->lpDAO->getAllLPs($lpid);
		return $result;
	}

	public function getAllPageLocations(){
		//modify the result set and retur
		$lpnames = array();
		$resultSet = $this->lpDAO->getAllLPs($lpid);
		foreach($resultSet as $result){
			$lpnames[] = $result['lp_name'];
		}
		return $lpnames;
	}

	public function getAllPageLocationsKeyValue(){
		//modify the result set and retur
		$lpnames = array();
		$resultSet = $this->lpDAO->getAllLPs($lpid);
		foreach($resultSet as $result){
			$lpnames[$result['id']] = $result['lp_name'];
		}
		return $lpnames;
	}

	public function updateLP($lpdata,$lpid){

		return $this->lpDAO->updateLPById($lpdata,$lpid);
	}

	public function addLP($lpdata){
		return $this->lpDAO->insertLP($lpdata);
	}

	

}
