<?php
namespace landingpage\dao;
use base\dao\BaseDAO;
use base\exception\BaseRedirectableException;

class SectionDAO extends BaseDAO {

	private $tblName;
	public function __construct() {
		global $sql_tbl;
		$this->tblName = $sql_tbl["landingpage_products"];;
	}

	public function getSectionsByLPId($lpid){
		$sql = 'select * from '. $this->tblName. ' where lp_id='.$lpid .' order by sort_order desc';
		return func_query($sql);
	}
	public function getHighestSortOrder($lpid){
		$sql = 'select max(sort_order) as max_sort_order from '. $this->tblName. ' where lp_id='.$lpid;
		return func_query($sql);
	}

	
	public function updateSection($sectiondata,$sectionid){
		//echo 1;exit;
		$resultID = func_array2updateWithTypeCheck($this->tblName,$sectiondata,"id='$sectionid'");
		return $resultID;
	}

	public function insertSection($sectiondata){
		$resultID = func_array2insertWithTypeCheck($this->tblName,$sectiondata);
		return $resultID;
	}

	public function deleteSection($sectionid){
		$resultID = func_array2deleteWithTypeCheck($this->tblName,"id='$sectionid'");
		return $resultID;
	}


}
