<?php
namespace landingpage\dao;
use base\dao\BaseDAO;
use base\exception\BaseRedirectableException;

class LPDAO extends BaseDAO {

	private $tblName;
	public function __construct() {
		global $sql_tbl;
		$this->tblName = $sql_tbl["landingpage"];;
	}

	public function getDetailsByLPId($LPId){
		$sql = 'select * from '. $this->tblName. ' where id='.$LPId;
		return func_query($sql);
	}

	public function getLPDetailsByLPName($lpname){
		$sql = "select * from ". $this->tblName. " where lp_name='$lpname'";
		return func_query($sql);
	}


	public function getAllLPs(){
		$sql = 'select * from '. $this->tblName;
		return func_query($sql);
	}

	public function updateLPById($lpdata,$lpid){
		$resultID = func_array2updateWithTypeCheck($this->tblName,$lpdata,"id='$lpid'");
		return $resultID;
	}

	public function insertLP($lpdata){
		$resultID = func_array2insertWithTypeCheck($this->tblName,$lpdata);
		return $resultID;
	}

}
