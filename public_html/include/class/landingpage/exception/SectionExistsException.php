<?php
namespace landingpage\exception;

use base\exception\RedirectableValidationException;
class LPExistsException extends \MyntraException {
	
	public function getDisplayMessage() {
		return $this->displayMessage;
	}
	public function __construct($lpname) {
		$this->displayMessage = "landing page already exists";
		parent::__construct($this->displayMessage);
	}
}
