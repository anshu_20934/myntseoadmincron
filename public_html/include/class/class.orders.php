<?php
##########################################################################
## Author	: Shantanu Bhadoria											##
## Date		: 14 Oct'2008												##
## Copyright Myntra Designs												##
## 																		##
## Myntra createorder class												##
## This class contains all the functions associated with creation of an ##
## order. Scroll Down for errorcodes list.								##
##########################################################################

require_once 'class.singleton.php';
require_once 'class.myntra_constants.php';

class orders {
	function get_order_status_by_source($source_id,$db) {
		if (isset($source_id)){
			$sql_tbl = myntra_constants::$sql_tbl;
			#Getting a singleton instance of the db object
			//$db = singleton::getmysqlinstance();
			$query = "select order_name,queueddate,completeddate,status from {$sql_tbl[orders]} where source_id = '".$db->escape($source_id)."'";
			//$result = $db->get_results($query,ARRAY_A);
			$result = func_query($query);
			return $result;
		}
	}
	function create_connection($WSDL){
		ini_set('soap.wsdl_cache_enabled', '0');
		
		try{
			$client = new SoapClient($WSDL,array(
				'trace' 		=> 1,
				'exceptions'	=> 0)
			);
		}catch (SoapFault $exception){
			echo $exception;
		}
		return $client;
	}
	function getStore($email)
	{
		$aclient = orders::create_connection('http://localhost:8080/axis2/services/StoreService?wsdl'); // Path to wsdl declaring the service spec.
		$obj->email = new SoapVar($email, XSD_STRING);
		$x = $aclient->getStore($obj);
		return($x->return);
	}
	function recalculate_productsInCart($productsInCart,$orgid="", $posInsertOrderFlag) {
		global $XCART_SESSION_VARS;
		global $login;
		global $relianceIP;
		$userType = $XCART_SESSION_VARS['login_type'];
		$shopMasterId = 0;
		$accountType = 0;
		
		$sql_tbl = myntra_constants::$sql_tbl;
		#Getting a singleton instance of the db object
		//$db = singleton::getmysqlinstance();

		$_productsInCart = $productsInCart;
		foreach ( $_productsInCart as $productid=>$productdetails){
			if(is_numeric($productdetails[productStyleId])){
				$styledetails=array();
				if($accountType == 1)
				{
					if($posInsertOrderFlag == 1)
					{
						$query = "select a.product_type,mk_shop_masters_catalog.store_rate - (mk_shop_masters_catalog.store_rate * mk_shop_masters_catalog.markdown_percentage/100) as unitprice, mk_shop_masters_catalog.store_rate as mrp,b.vat,mk_shop_masters_catalog.markdown_percentage as markdown from mk_shop_masters_catalog, $sql_tbl[mk_product_style] a LEFT JOIN $sql_tbl[mk_product_type] b on a. product_type = b.id where a.id='$productdetails[productStyleId]' and mk_shop_masters_catalog.product_style_id=a.id and mk_shop_masters_catalog.shop_master_id = $shopMasterId";
					}
					else
						$query = "select a.product_type,mk_shop_masters_catalog.store_rate as unitprice,b.vat from mk_shop_masters_catalog, $sql_tbl[mk_product_style] a LEFT JOIN $sql_tbl[mk_product_type] b on a. product_type = b.id where a.id='$productdetails[productStyleId]' and mk_shop_masters_catalog.product_style_id=a.id and mk_shop_masters_catalog.shop_master_id = $shopMasterId";
					//$styledetails = $db->get_row($query,ARRAY_A);
					$styledetails = func_query_first($query);
					$queryForMarkup="select a.product_type,a.price as unitprice,b.vat from $sql_tbl[mk_product_style] a LEFT JOIN $sql_tbl[mk_product_type] b on a. product_type = b.id where a.id='$productdetails[productStyleId]'";
					//$styledetailsformarkup = $db->get_row($queryForMarkup,ARRAY_A);
					$styledetailsformarkup = func_query_first($queryForMarkup);
				}
				else
				{
					if(is_numeric($orgid) && $orgid!=0){
						$query="select a.product_type,c.price as unitprice,b.vat from $sql_tbl[mk_product_style] a LEFT JOIN $sql_tbl[mk_product_type] b on a. product_type = b.id LEFT JOIN mk_org_style_map c on c.styleid=a.id where c.id=$orgid and a.id='$productdetails[productStyleId]'";
						//$styledetails = $db->get_row($query,ARRAY_A);
						$styledetails = func_query_first($query);
					}
					if(empty($styledetails)) {
						$query="select a.product_type,a.price as unitprice,b.vat from $sql_tbl[mk_product_style] a LEFT JOIN $sql_tbl[mk_product_type] b on a. product_type = b.id where a.id='$productdetails[productStyleId]'";
						//$styledetails = $db->get_row($query,ARRAY_A);
						$styledetails = func_query_first($query);
					}
				}


				//TODO : Define unitPrice array for each size
				//	 Define AddonPrice array for each size (One may chose addon for one size and not for another)
				//	 If unitprice for size is NA , then override with style price
				//	 Add markup on unit price for each size
				//	 And finally calculae total price iterating through all size

				//	for now a dirty hack as Canvass Prints has only one size selected for every order item, remove this asap
				//	Requires UI change also, we need to display unitprice and add on price for every size
				
				//override option price by style price for only online and POS(not for brandstores-since no addon concept)
				if(empty($orgid)){
					$sizenameArray = $_productsInCart[$productid][sizenames];
					foreach($sizenameArray as $sizename)
					{
						$qry = "select id from mk_product_options where style=$productdetails[productStyleId] and value='$sizename'";
											$res = db_query($qry); $row = db_fetch_row($res);
											$optionid = $row[0];
						$qry = "select price from mk_product_options where id=$optionid";
						
						$res = db_query($qry);
						if( $row = db_fetch_row($res) )
						{
							if( !empty($row[0]) ){
								$styledetails[unitprice] = $row[0];
								if($posInsertOrderFlag == 1){
									$styledetails[unitprice] = $row[0] - ($row[0] * $styledetails[markdown] / 100 );
									$styledetails[mrp] = $row[0];
								}
							}
						}
					}
				}
				
				$query="select markup from $sql_tbl[products] where productid=$productdetails[productId]";
				//$designdetails = $db->get_row($query,ARRAY_A);
				$designdetails = func_query_first($query);
				
				#Rewriting recalculated variables
				$_productsInCart[$productid][producTypeId]=$styledetails[product_type];
				if(is_array($_productsInCart[$productid][sizenames])
				&& is_array($_productsInCart[$productid][sizequantities])
				&& count($_productsInCart[$productid][sizenames])
				&& count($_productsInCart[$productid][sizequantities])
				){
					$_productsInCart[$productid][quantity] = array_sum($_productsInCart[$productid][sizequantities]);
				}
				
				if($accountType == 1)
				{				
					//For adding markup to POS
					$stylePrice = $styledetailsformarkup[unitprice];
					$_productsInCart[$productid][actualPriceOfProduct]=$styledetails[unitprice] + ($stylePrice*$designdetails[markup]/100);
					if($posInsertOrderFlag == 1)
						$posTotalPriceWithoutMarkdown = $styledetails[mrp] + ($stylePrice*$designdetails[markup]/100);

				}
				else
				{
					$_productsInCart[$productid][actualPriceOfProduct]=$styledetails[unitprice] + ($styledetails[unitprice]*$designdetails[markup]/100);
				}
				$_productsInCart[$productid][productPrice]=$styledetails[unitprice];
				$_productsInCart[$productid][totalPrice]=$_productsInCart[$productid][actualPriceOfProduct]*$_productsInCart[$productid][quantity];
				$_productsInCart[$productid][vatRate]=$styledetails[vat];
				if($posInsertOrderFlag == 1)
				{
					$_productsInCart[$productid][posUnitPriceWithoutMarkdown] = $styledetails[mrp];
					$_productsInCart[$productid][posTotalPriceWithoutMarkdown] = $posTotalPriceWithoutMarkdown * $_productsInCart[$productid][quantity];
				}

			}
		}
		$returnvalue = $_productsInCart;
		return $returnvalue;
	}

	function _recalculate_productsInCart($productsInCart,$orgid="", $posInsertOrderFlag) {
		global $XCART_SESSION_VARS;
		global $login;
		$userType = $XCART_SESSION_VARS['login_type'];
		$shopMasterId = 0;
		$accountType = 0;
		$sql_tbl = myntra_constants::$sql_tbl;
		#Getting a singleton instance of the db object
		//$db = singleton::getmysqlinstance();

		$_productsInCart = $productsInCart;
		foreach ( $_productsInCart as $productid=>$productdetails){
			if(is_numeric($productdetails[productStyleId])){
				$styledetails=array();
				if($accountType == 1)
				{
					if($posInsertOrderFlag == 1)
					{
						$query = "select a.product_type,mk_shop_masters_catalog.store_rate - (mk_shop_masters_catalog.store_rate * mk_shop_masters_catalog.markdown_percentage/100) as unitprice, mk_shop_masters_catalog.store_rate as mrp,b.vat from mk_shop_masters_catalog, $sql_tbl[mk_product_style] a LEFT JOIN $sql_tbl[mk_product_type] b on a. product_type = b.id where a.id='$productdetails[productStyleId]' and mk_shop_masters_catalog.product_style_id=a.id and mk_shop_masters_catalog.shop_master_id = $shopMasterId";
					}
					else
						$query = "select a.product_type,mk_shop_masters_catalog.store_rate as unitprice,b.vat from mk_shop_masters_catalog, $sql_tbl[mk_product_style] a LEFT JOIN $sql_tbl[mk_product_type] b on a. product_type = b.id where a.id='$productdetails[productStyleId]' and mk_shop_masters_catalog.product_style_id=a.id and mk_shop_masters_catalog.shop_master_id = $shopMasterId";
					//$styledetails = $db->get_row($query,ARRAY_A);
					$styledetails = func_query_first($query);
					$queryForMarkup="select a.product_type,a.price as unitprice,b.vat from $sql_tbl[mk_product_style] a LEFT JOIN $sql_tbl[mk_product_type] b on a. product_type = b.id where a.id='$productdetails[productStyleId]'";
					//$styledetailsformarkup = $db->get_row($queryForMarkup,ARRAY_A);
					$styledetailsformarkup = func_query_first($queryForMarkup);
				}
				else
				{
					if(is_numeric($orgid) && $orgid!=0){
						$query="select a.product_type,c.price as unitprice,b.vat from $sql_tbl[mk_product_style] a LEFT JOIN $sql_tbl[mk_product_type] b on a. product_type = b.id LEFT JOIN mk_org_style_map c on c.styleid=a.id where c.id=$orgid and a.id='$productdetails[productStyleId]'";
						//$styledetails = $db->get_row($query,ARRAY_A);
						$styledetails = func_query_first($query);
					}
					if(empty($styledetails)) {
						$query="select a.product_type,a.price as unitprice,b.vat from $sql_tbl[mk_product_style] a LEFT JOIN $sql_tbl[mk_product_type] b on a. product_type = b.id where a.id='$productdetails[productStyleId]'";
						//$styledetails = $db->get_row($query,ARRAY_A);
						$styledetails = func_query_first($query);
					}
				}


				//TODO : Define unitPrice array for each size
				//	 Define AddonPrice array for each size (One may chose addon for one size and not for another)
				//	 If unitprice for size is NA , then override with style price
				//	 Add markup on unit price for each size
				//	 And finally calculae total price iterating through all size

				//	for now a dirty hack as Canvass Prints has only one size selected for every order item, remove this asap
				//	Requires UI change also, we need to display unitprice and add on price for every size
				$sizenameArray = $_productsInCart[$productid][sizenames];
				foreach($sizenameArray as $sizename)
				{
					$qry = "select id from mk_product_options where style=$productdetails[productStyleId] and value='$sizename'";
                                        $res = db_query($qry); $row = db_fetch_row($res);
                                        $optionid = $row[0];

					$qry = "select price from mk_product_options where id=$optionid";
					$res = db_query($qry);
					if( $row = db_fetch_row($res) )
					{
						if( !empty($row[0]) )
						$styledetails[unitprice] = $row[0];
					}
				}
				$query="select markup from $sql_tbl[products] where productid=$productdetails[productId]";
				//$designdetails = $db->get_row($query,ARRAY_A);
				$designdetails = func_query_first($query);
				#Rewriting recalculated variables
				$_productsInCart[$productid][producTypeId]=$styledetails[product_type];
				if(is_array($_productsInCart[$productid][sizenames])
				&& is_array($_productsInCart[$productid][sizequantities])
				&& count($_productsInCart[$productid][sizenames])
				&& count($_productsInCart[$productid][sizequantities])
				){
					$_productsInCart[$productid][quantity] = array_sum($_productsInCart[$productid][sizequantities]);
				}
				
				if($accountType == 1)
				{				
					//For adding markup to POS
					$stylePrice = $styledetailsformarkup[unitprice];
					$_productsInCart[$productid][actualPriceOfProduct]=$styledetails[unitprice] + ($stylePrice*$designdetails[markup]/100);
					if($posInsertOrderFlag == 1)
						$posTotalPriceWithoutMarkdown = $styledetails[mrp] + ($stylePrice*$designdetails[markup]/100);

				}
				else
				{
					$_productsInCart[$productid][actualPriceOfProduct]=$styledetails[unitprice] + ($styledetails[unitprice]*$designdetails[markup]/100);
				}
				$_productsInCart[$productid][productPrice]=$styledetails[unitprice];
				$_productsInCart[$productid][totalPrice]=$_productsInCart[$productid][actualPriceOfProduct]*$_productsInCart[$productid][quantity];
				$_productsInCart[$productid][vatRate]=$styledetails[vat];
				if($posInsertOrderFlag == 1)
				{
					$_productsInCart[$productid][posUnitPriceWithoutMarkdown] = $styledetails[mrp];
					$_productsInCart[$productid][posTotalPriceWithoutMarkdown] = $posTotalPriceWithoutMarkdown * $_productsInCart[$productid][quantity];
				}

			}
		}
		$returnvalue = $_productsInCart;
		return $returnvalue;
	}
}

# Error Codes :
# 0 : No Error, perfect, All went well (Atleast that's what we think!!)
# 1 : Order source information in sync credentials and actual order data does not match. Someone is trying to screw with us.
# 2 : Failed to insert in mk_temp_order
# 3 : Failed to insert in orders
# 4 : Failed to insert in order_details
# 5 : Failed to insert in mk_product_options_order_details_rel
# 6 : Failed to insert in mk_xcart_order_details_customization_rel
# 7 : Failed to insert in mk_xcart_order_customized_area_image_rel
# 8 : Failed to create customer
# 9 : Failed to create product
?>
