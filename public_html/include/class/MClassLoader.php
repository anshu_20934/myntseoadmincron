<?php

if (defined('MCLASS_LOADER')) {
	return;
	include_once ("$xcart_dir/include/class/MClassLoader.php");
}

define('MCLASS_LOADER', true);
MClassLoader::register();


class MClassLoader{
	private static $classpath=array();

	public static function register() {
		return spl_autoload_register(array('MClassLoader', 'load'));
	}

	public static function load($classNameWithNameSpace){
		if (class_exists($classNameWithNameSpace)) {
			return false;
		}

		foreach (self::$classpath as $path){
			
			$classNameWithNameSpaceRep=str_replace('\\', '/', $classNameWithNameSpace);	
			$fileName=$path.'/'.$classNameWithNameSpaceRep.".php";
			if (file_exists($fileName)===true || (is_readable($fileName) === true)) {
				include_once $fileName;
				return true;
			}
		}

		return false;
	}
	
	/**
	 * Adds a path into class path
	 * @param unknown_type $path
	 */
	public static function addToClassPath($path){
		array_unshift(self::$classpath,$path);
	}
}

?>