<?php

namespace user\dao;

use user\exception\UserDoesNotExistsException;

use user\exception\UserAlreadyExistsException;

use user\User;

use base\dao\BaseDAO;

class UserDAO extends BaseDAO{
	
	private $userTable = "xcart_customers";
	private $userTelesalesTable = "telesales_password";
         public function getTeleSalesPwds(User $user){
                $curTime = time();
                $sql = "select tmp_password_md5,created_by from {$this->userTelesalesTable}
                    where user_id='".mysql_real_escape_string($user->getLogin())."'
                         and start_time < $curTime and end_time > $curTime ";
                $resultSet=$this->getRAWData($sql,false,false);
		if(empty($resultSet)){
			return null;
		}
                
                return $resultSet;
        }
	public function getUserBasicDetails($login,$extraConditions=false){
		if(empty($login))
			return null;
		$sql="select firstname,lastname,DOB,gender,login,email,mobile,usertype,password,password_md5,first_login,last_login,status
		 from {$this->userTable} where login= '".mysql_real_escape_string($login)."'";
		if(!empty($extraConditions)){
			$sql.=" and $extraConditions";
		}
		$resultSet=$this->getRAWData($sql,true,true);
		if(empty($resultSet)){
			return null;
		}
		if($resultSet['status'] == 'N'){
			return 'N';
		}
		
		$user= new User($resultSet['login'],$resultSet['usertype'],$resultSet['password'],$resultSet['password_md5'],$resultSet['first_login']);
		$user->setEmail($resultSet['email']);
		$user->setLastLoginTime($resultSet['last_login']);
		$user->setFirstName($resultSet['firstname']);
		$user->setLastName($resultSet['lastname']);
		$user->setGender($resultSet['gender']);
		$user->setDOB($resultSet['DOB']);
		$user->setMobile($resultSet['mobile']);
		return $user;
		
	}
	
	public function updateLastLoginTime(User $user){
		db_query("UPDATE {$this->userTable} SET last_login='".time()."',password_md5='{$user->getPasswordMD5()}' 
			WHERE login = '".mysql_real_escape_string($user->getLogin())."'");
		return true;
                 
	}
	public function setNewTeleSalesPwd(User $user,$teleSalesPwd, $exp){
                $telesales_profile= array();
                $telesales_profile["tmp_password_md5"] = $teleSalesPwd;
                $curTime = time();
                $telesales_profile["start_time"] = $curTime;
                $expTime = $curTime + $exp* 60;
                $telesales_profile["end_time"] = $expTime;
                $telesales_profile["user_id"]= $user->getLogin();
                global $telesalesUser;
                $telesales_profile["created_by"]= $telesalesUser;
                $add_fields = array();
                
                $add_fields["user_name"]= $telesalesUser;
                return func_array2insertWithTypeCheck_log($this->userTelesalesTable , $telesales_profile,true,$add_fields);
        }
       

        public function saveNewUser(User $user){
		$existingUser = $this->getUserBasicDetails($user->getLogin());
		if($existingUser != null){
			throw new UserAlreadyExistsException($user->getLogin());
		}
		
		$profile_values = array();
		$profile_values['email'] = $user->getEmail();
		$profile_values['login'] = $user->getLogin();
		$profile_values['mobile'] = $user->getMobile();
		$profile_values['usertype'] = $user->getUserType();
		$profile_values['password'] = $user->getPassword();
		$profile_values['password_md5']= $user->getPasswordMD5();
		$profile_values['cart'] = "";
		$profile_values['termcondition'] = '1';
		$profile_values['addressoption'] = 'M';
		$profile_values['first_login'] = $user->getFirstLoginTime();
		$profile_values['last_login'] = $user->getFirstLoginTime();
		$profile_values['referer'] = $user->getMRPRefrer();
		$gender=$user->getGender();
		
		$firstName = $user->getFirstName();
		$lastName = $user->getLastName();
		$DOB = $user->getDOB();

		if(!empty($gender)){
			$profile_values['gender']=$gender;
		}

		if(!empty($firstName)){
			$profile_values['firstname'] = $firstName;
		}
		if(!empty($lastName)){
			$profile_values['lastname'] = $lastName;
		}
		if(!empty($DOB)){
			$profile_values['DOB'] = $DOB;
		}
		


		func_array2insertWithTypeCheck($this->userTable , $profile_values);
	}
	
	public function updateUser(User $user,$profile_values=array()){
	
		$profile_values['email'] = $user->getEmail();
		$profile_values['login'] = $user->getLogin();
		$profile_values['mobile'] = $user->getMobile();
		$profile_values['usertype'] = $user->getUserType();
		$profile_values['password'] = $user->getPassword();
		$profile_values['password_md5']= $user->getPasswordMD5();
		$profile_values['cart'] = "";
		$profile_values['termcondition'] = '1';
		$profile_values['addressoption'] = 'M';
		$profile_values['first_login'] = $user->getFirstLoginTime();
		$profile_values['last_login'] = $user->getFirstLoginTime();
		$profile_values['referer'] = $user->getMRPRefrer();
		func_array2updateWithTypeCheck($this->userTable , $profile_values , array('login'=>"{$user->getLogin()}"));
	}
}