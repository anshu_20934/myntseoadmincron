<?php

namespace user;

use user\exception\PasswordDoesNotMatchException;

use utils\RandomStringGenerator;

use user\exception\UserDoesNotExistsException;

use user\exception\AccountSuspendedException;

use user\dao\UserDAO;

global $xcart_dir;
require_once "$xcart_dir/modules/coupon/database/CouponAdapter.php";
include_once "$xcart_dir/modules/coupon/mrp/SmsVerifier.php";
require_once "$xcart_dir/modules/coupon/mrp/RuleExecutor.php";

class UserAuthenticationManager{
		
	
	/**
	 * 
	 * authenticae user
	 * @param User $user
	 * @param unknown_type $password
	 * @param unknown_type $userType
	 * @throws PasswordDoesNotMatchException
	 */
	public function authenticateUser(User $user,$password,$userType = User::USER_TYPE_CUSTOMER,$with_exception = true){
//                if (x_session_is_registered("telesalesAgent"))
//                    x_session_unregister("telesalesAgent");
                if (x_session_is_registered("telesalesAdminName"))
                    x_session_unregister("telesalesAdminName");
                if ($userType === User::USER_TYPE_CUSTOMER && $this->authenticateTelesalesUser($user, $password))
                    return true;
		
                    
                    return $this->authenticateUserWithMD5($user, $password, $with_exception);
	
	}
        public function authenticateTelesalesUser(User $user, $password){
            $dbapdapter = new UserDAO();
            $pwdArray = $dbapdapter->getTeleSalesPwds($user);
            
            
                foreach ($pwdArray as $pwd){
                   // echo $pwd["tmp_password_md5"]."<br>";
                   // echo $password."<br>";
                    
                        if ($pwd["tmp_password_md5"] == $password){
                            x_session_register("telesalesAgent",time());
                            global $telesalesUser;
                            if (!x_session_is_registered("telesalesAdminName")){
                                $telesalesUser = $pwd["created_by"];
                                x_session_register("telesalesAdminName", $telesalesUser);
                               
                            }
                            if (!x_session_is_registered("oldCheckout")){
                                 x_session_register("oldCheckout");
                            }
                            return true;
                        }
                }
            
            return false;
        }
        
        public function generateNewTeleSalesPwd($username,$expiry){
            
            $randomPwd = RandomStringGenerator::generateRandomString(20);
            $dbapdapter = new UserDAO();
            $userdetails = $dbapdapter->getUserBasicDetails($username);
            if ($userdetails == "N")
                return "N";
            $dbapdapter->setNewTeleSalesPwd($userdetails, $randomPwd, $expiry);
            return $randomPwd;
        }

    
	
	private function authenticateUserWithMD5(User $user,$password,$with_exception){
		$password = trim($password);
		$md5String=$user->getPasswordMD5 ();
		$split = explode ( "|", $md5String );
		
		$md5Value = $split[0];
		$random = $split[1];
		
		if(md5($password.$random) !== $md5Value){
			
				throw new PasswordDoesNotMatchException($password);
		}
		if(empty($random)){
			$user->setPasswordMD5(self::encryptToMD5($password));
		}
		return true;
	}
	
	/*
		checks if given user credentials(userid and password) is a valid record
		this can be used as a check at palaces like signing a user if already existing one enters details in signup page
	*/
	public function loginCredentialCheck($userId,$password,$userType = User::USER_TYPE_CUSTOMER){
		$dbadapter = new UserDAO();
		$user = $dbadapter->getUserBasicDetails($userId,"usertype='$userType'");
		
		if($user ==  null) return false; // user does not exist
	
		if($user == 'N') return false; // user account suspended

		$authenticate = $this->authenticateUser($user, $password ,$userType, false);
		
		if($authenticate !=true) return false; // password mismatch

		return true;
	}

	/**
	 * 
	 * perform sign in
	 * @param unknown_type $userId
	 * @param unknown_type $password
	 * @param unknown_type $userType
	 * @throws UserDoesNotExistsException
	 * @throws PasswordDoesNotMatchException
	 * @throws AccountSuspendedException
	 * @return \user\User
	 */
	public function performUserSignIn($userId,$password,$userType = User::USER_TYPE_CUSTOMER){
		$dbadapter = new UserDAO();
		$user = $dbadapter->getUserBasicDetails($userId,"usertype='$userType'");
		
		if($user ==  null){
			throw new UserDoesNotExistsException($userId);
		}
		if($user == 'N'){
			throw new AccountSuspendedException($userId);	
		}
		$authenticate = $this->authenticateUser($user, $password ,$userType);
		
		if($authenticate !=true){
			throw new PasswordDoesNotMatchException($password);
		}
		
		$this->doPostLoginSequences($user);
		return $user;
	}
	
	private function doPostLoginSequences(User $user){
		$dbadapter = new UserDAO();
		$dbadapter->updateLastLoginTime($user);
	}
	
	public function performUserRegistration($profileData){
		$blowfishPass = "";
		$md5Value = self::encryptToMD5($profileData['password']);
		$user = new User($profileData['login'], $profileData['usertype'], $blowfishPass, $md5Value);
		$user->setEmail($profileData['login']);
		$user->setMobile($profileData['mobile']);
		$user->setMRPRefrer($profileData['referer']);
		$user->setFirstLoginTime(time());
		if(!empty($profileData['gender'])){
			$user->setGender($profileData['gender']);	
		}

		//NEW FIELDS FOR SIGNUP
		if(!empty($profileData['firstName'])){
			$user->setFirstName($profileData['firstName']);
		}
		if(!empty($profileData['lastName'])){
			$user->setLastName($profileData['lastName']);
		}
		if(!empty($profileData['DOB'])){
			$user->setDOB($profileData['DOB']);
		}
		
		$dbadapter = new UserDAO();
		$dbadapter->saveNewUser($user);
		$this->doPostRegisterSequences($user);
		return $user;
	}
	
	private function doPostRegisterSequences(User $user){
		$dbadapter = new UserDAO();
	
		if($user->getUserType() === User::USER_TYPE_CUSTOMER){
			$smsVerifier = \SmsVerifier::getInstance();
			$smsVerifier->addNewMapping($user->getMobile(), $user->getLogin());
			$couponAdapter = \CouponAdapter::getInstance();
			$couponAdapter->customerJoined($user->getLogin(), $user->getMRPRefrer());
			
			// Synchronous call to the MRP New Login rule.

			$ruleExecutor = \RuleExecutor::getInstance();
			$ruleExecutor->runSynchronous("customer", $user->getLogin(), "1");    // "1" is the ruleId for "NewLogin" rule.
		}
	}
	
	public static function encryptToMD5($string){
		$random = RandomStringGenerator::generateRandomString(30);
		$md5Value = md5($string.$random);
		return "$md5Value|$random";
	}
	
	public static function encryptToBlowfish($string){
		return text_crypt($string);
	}
	
	/**
	 * 
	 * change user password
	 * @param unknown_type $userId
	 * @param unknown_type $userType
	 * @param unknown_type $oldPassword
	 * @param unknown_type $newPassword
	 * @throws UserDoesNotExistsException
	 * @throws PasswordDoesNotMatchException
	 */
	public function changeUserPassword($userId,$userType,$oldPassword,$newPassword){
		$dbadapter = new UserDAO();
		$user = $dbadapter->getUserBasicDetails($userId,"usertype='$userType'");
		
		if($user ==  null){
			throw new UserDoesNotExistsException($userId);
		}
		$authenticate = $this->authenticateUser($user, $oldPassword ,$userType);
		
		if($authenticate !=true){
			throw new PasswordDoesNotMatchException($oldPassword);
		}
		
		$user->setPasswordMD5(self::encryptToMD5($newPassword));
		$user->setPassword("");
		
		$dbadapter->updateUser($user,array("resetpwd_key"=>""));
		return $user;
	}
}
