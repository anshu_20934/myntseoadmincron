<?php

namespace user;

use base\dataobject\BaseDO;

class User extends BaseDO{
	
	const USER_TYPE_ADMIN = 'A';
	const USER_TYPE_CUSTOMER = 'C';
	const USER_TYPE_PROVIDER = 'P';
	
	private $login;
	private $email;
	private $mobile;
	private $userType;
	private $password;
	private $passwordMD5;
	private $firstLogin;
	private $lastLogin;
	private $referer;

	private $firstName;
	private $lastName;
	private $DOB;
    
	private $mrpRefrer;
	private $gender;
	
	public function __construct($login,$userType,$password,$passwordMD5 , $firstLogin){
		$this->login = $login;
		$this->password = $password;
		$this->passwordMD5 = $passwordMD5;
		$this->userType = $userType;
		$this->firstLogin = $firstLogin;
	}
	
	public function getMobile(){
		return $this->mobile;
	}
	
	public function setMobile($mobile){
		$this->mobile=html_entity_decode( $mobile );
	}
	
	public function getLogin(){
		return $this->login;
	}
	
	public function getEmail(){
		return $this->email;
	}
	
	public function setEmail($email){
		$this->email = html_entity_decode( $email );
	}
	
	public function getPassword(){
		return $this->password;
	}
	
	public function getPasswordMD5(){
		return $this->passwordMD5;
	}
	
	public function setPasswordMD5($md5Value){
		$this->passwordMD5 = $md5Value;
	}
	
	public function getFirstLoginTime(){
		return $this->firstLogin;
	}
	
	public function setFirstLoginTime($firstLogin){
		$this->firstLogin = $firstLogin;
	}
	
	public function setLastLoginTime($lastLogin){
		$this->lastLogin=$lastLogin;
	}
	
	public function getLastLoginTime(){
		return $this->lastLogin;
	}
	
	public function getUserType(){
		return $this->userType;
	}
	
	public function setFirstName($firstName){
		$this->firstName=$firstName;
	}
	
	public function getFirstName(){
		return $this->firstName;
	}
	
	public function setLastName($lastName){
		$this->lastName=$lastName;
	}
	
	public function getLastName(){
		return $this->lastName;
	}

	public function setMRPRefrer($mrpRefrer){
		$this->mrpRefrer = $mrpRefrer;
	}
	
	public function getMRPRefrer(){
		return $this->mrpRefrer;
	}
	
	public function setPassword($password){
		$this->password = $password;
	}
	
	public function setGender($gender){
		$this->gender = $gender;
	}
	
	public function getGender(){
		return $this->gender;
	}

	public function getDOB(){
		return $this->DOB;
	}

	public function setDOB($DOB){
		$this->DOB = $DOB;
	}

}