<?php

namespace user\exception;

class UserDoesNotExistsException extends AuthenticationException{
	public function __construct($login){
		parent::__construct("Login '$login' does not exists");
	}
}