<?php

namespace user\exception;

class UserAlreadyExistsException extends UserCreateException{
	public function __construct($login){
		parent::__construct("User with login '$login' already exists");
	}
}