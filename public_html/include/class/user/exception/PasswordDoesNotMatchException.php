<?php

namespace user\exception;

class PasswordDoesNotMatchException extends AuthenticationException{
	public function __construct($password){
		parent::__construct("Password is wrong");
	}
}