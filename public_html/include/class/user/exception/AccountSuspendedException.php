<?php

namespace user\exception;

class AccountSuspendedException extends AuthenticationException{
	public function __construct($login){
		parent::__construct("Account '$login' is suspended");
	}
}