<?php
namespace user\exception;

class UserCreateException extends \MyntraException{
	public function __construct($message){
		parent::__construct("New User cannot be created because $message");
	}
}