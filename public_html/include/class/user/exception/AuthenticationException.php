<?php
namespace user\exception;

class AuthenticationException extends \MyntraException{
	public function __construct($message){
		parent::__construct("Authentication Failed because $message");
	}
}