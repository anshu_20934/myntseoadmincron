<?php

/**
 * Created by IntelliJ IDEA.
 * User: Mohammed Asif Hussain
 * Date: Dec 22, 2009
 * Time: 1:25:50 PM
 * To change this template use File | Settings | File Templates.
 */

include_once(\HostConfig::$documentRoot."/../widget/class.widget.keyvaluepair.php");
class AbuseFilter {

    var $words = NULL;
    
    var $commonWords = NULL;

    var $ABUSE_WORD_CACHE = 'abuse_word_cache';

    var $ABUSE_WORD_CACHE_TIMEOUT = 3600;
    
    var $COMMON_WORD_CACHE = 'common_word_cache';

    var $COMMON_WORD_CACHE_TIMEOUT = 3600;

    var $cache;

    public function __construct() {
        $this->cache = new XCache();
    }

    /**
     * @return #P#CAbuseFilter.words|?
     */
    function _get_words() {
        if ($this->words == null) {
            $this->words = $this->cache->fetchObject($this->ABUSE_WORD_CACHE);
            if ($this->words == NULL) {
                $curse_word = file_get_contents(dirname(__FILE__) . "/../../../skin1/dirtywords.txt");
                $this->words = preg_split("/(\r\n|\r|\n)/", strtolower($curse_word)) ;
                //read and merge the keywords set in widget k/v pairs
                $handPickedAbuseKeywords =  WidgetKeyValuePairs::getStrArray("search.handPickedAbuseKeywords");
                $handPickedAbuseKeywords = array_map("strtolower", $handPickedAbuseKeywords);
                $this->words = array_merge($this->words, $handPickedAbuseKeywords);
                $this->cache->storeObject($this->ABUSE_WORD_CACHE, $this->words, $this->ABUSE_WORD_CACHE_TIMEOUT);
            }
        }
        return $this->words;
    }

    /**
     * @param  $phrase
     * @return #Fimplode|?
     */
    function filter($phrase) {
        $search_word = explode(" ", trim(strtolower(preg_replace("/[^a-zA-Z0-9s]/", " ", $phrase))));
        $words = $this->_get_words();
        foreach ($search_word as $key => $value) {
            if (in_array($value, $words)) {
                $pattern = '/\b' . trim($value) . '\b/';
                $phrase = preg_replace($pattern, '', $phrase);
            }
        }
        return $phrase;
    }

    /**
     * Filters a array of words ..
     * Also can pass a db result and column name which we want to filter ..
     * @param  $array
     * @return
     */
    function filter_array($array, $field = NULL) {
        foreach ($array as $key => $value) {
            if (empty($field)) {
                $array[$key] = $this->filter($value) ;
            } else {
                $value[$field] = $this->filter($value[$field]);
                $array[$key] = $value;
            }
        }
        return $array;
    }

    /**
     * inefficient ..
     * @param  $phrase
     * @return boolean
     */
    function is_filthy($phrase) {
    	$phrase = trim(strtolower(preg_replace("/[^a-zA-Z0-9s]/", " ", $phrase)));
    	$phrase = str_replace("\'", "", $phrase);
        $original_words = explode(" ", $phrase);
        $words = $this->_get_words();
        foreach ($original_words as $key => $value) {
            if (in_array($value, $words)) {
            	return true;
            }
        }
        return false;
    }
    
        /**
     * @return #P#CAbuseFilter.words|?
     */
    function _get_common_words() {
        if ($this->commonWords == null) {
            $this->commonWords = $this->cache->fetchObject($this->COMMON_WORD_CACHE);
            if ($this->commonWords == NULL) {
                $common_word = file_get_contents(dirname(__FILE__) . "/../../../skin1/commonWords.txt");
                $this->commonWords = preg_split("/(\r\n|\r|\n)/", strtolower($common_word)) ;
                $this->cache->storeObject($this->COMMON_WORD_CACHE, $this->commonWords, $this->COMMON_WORD_CACHE_TIMEOUT	);
            }
        }
        return $this->commonWords;
    }

    /**
     * @param  $phrase
     * @return #Fimplode|?
     */
    function filter_common_words($phrase, $delimiter = " ") {
    	$phrase = trim(strtolower(preg_replace("/[^a-zA-Z0-9s]/", $delimiter, $phrase)));
    	$phrase = str_replace("\'", "", $phrase);
        $original_words = explode($delimiter, $phrase);
        $replaced_words = array();
        $words = $this->_get_common_words();
        foreach ($original_words as $key => $value) {
            if (!in_array($value, $words)) {
            	array_push($replaced_words, $value);
            }
        }
        $retval = "";
        if(!empty($replaced_words))
        	$retval = implode($delimiter, $replaced_words);
        return $retval;
    }
    
}
