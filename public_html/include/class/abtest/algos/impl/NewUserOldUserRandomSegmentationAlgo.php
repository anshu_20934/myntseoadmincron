<?php
namespace abtest\algos\impl;
use abtest\algos\MABTestAlgo;

class NewUserOldUserRandomSegmentationAlgo implements MABTestAlgo{
	
	public function generateSegment($mabTestName){
		if(!empty($_COOKIE['xid'])){
			return "old:".rand(1, 100);
		}else{
			return "new:".rand(1, 100);
		}
	}
}
?>