<?php
namespace abtest\algos\impl;
use abtest\algos\MABTestAlgo;

class RandomSegmentationAlgo implements MABTestAlgo{
	
	public function generateSegment($mabTestName){
		return rand(1, 100);
	}
}
?>