<?php
namespace abtest\algos;
/**
 * Interface for Segmentation Algo implementations,
 * @author yogesh
 *
 */
interface MABTestAlgo{
	/**
	 * Generates a segment.
	 * Output of this function cannot be directly mapped to test variant.
	 * this output has to go through resolver to extract test variant of this segmentation
	 * @param String $mabTestName
	 * @return String or int
	 */
	public function generateSegment($mabTestName);
}

?>