<?php
namespace abtest\algos;
/**
 * Factory to get Segmentation Algorithm Objects
 * This factory implictly caches the objects and 
 * return the cached object if it cache is hit otherwise 
 * create a new instance and return
 * @author yogesh
 *
 */
class MABTestSegmentationAlgoFactory{

	private static $__ALGO_TAG="MAB-SEG_ALGO";
	/**
	 * Returns Segmentation Algo object for a test
	 * @param String $mabTestName
	 * @return MABTestAlgo
	 */
	public static function getSegmentationAlgo($mabTestName){
		$abtest=\ABTestingVariables::getTestVariables($mabTestName);
		$algoClassName=$abtest['seg_algo'];
		$cachedAlgo=new $algoClassName();
		return $cachedAlgo;
	}

}

?>