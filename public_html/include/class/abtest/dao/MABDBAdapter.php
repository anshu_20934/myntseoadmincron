<?php
namespace abtest\dao;
use abtest\exception\MABDuplicateTestException;

use abtest\resolver\ResolverFactory;

/**
 * DAO for MABTest
 * @author yogesh
 *
 */
class MABDBAdapter {

	private $tab_mk_abtesting_tests;
	private $tab_mk_abtesting_variations;

	private static $instance = null;
	private static $COMMENT_COMPLETE="<font color=\"green\" style=\"font: bold;\">COMPLETED:</font>";
	private static $COMMENT_FIXME="<font color=\"red\" style=\"font: bolder;\">FIXME:</font>";
	private static $COMMENT_FILE="<font color=\"\" style=\"font: bold;\">File:</font>";

	/**
	 * Constructor.
	 */
	protected function __construct()
	{
		global $sql_tbl;
		$this->tab_mk_abtesting_tests = $sql_tbl["mk_abtesting_tests"];
		$this->tab_mk_abtesting_variations = $sql_tbl['mk_abtesting_variations'];
	}


	/**
	 * Returns an instance
	 */
	public static function getInstance(){
		if(self::$instance==null){
			self::$instance= new MABDBAdapter();
		}
		return self::$instance;
	}

	/**
	 * 
	 * Save or update a test from a test array having changes
	 * @param Array $abtest
	 */
	public function addOrUpdateFromArray($abtest,$comments){
		if($abtest['id']<0){
			return $this->createTestFromArray($abtest, $comments);
		}
		$storedTestArray=$this->fetchTestArrayById($abtest['id']);
		$diffOfNewArray=$this->arrayRecursiveDiff($abtest,$storedTestArray);
		$diffOfOldArray=$this->arrayRecursiveDiff($storedTestArray,$abtest);
		if(empty($diffOfNewArray)&&empty($diffOfOldArray)){
			return false;
		}
		$changelog=$this->generateChangeLog($diffOfNewArray,$diffOfOldArray,$abtest,$storedTestArray);
		
		$arrayToInsertTest = array(
						'name'=>$abtest[name],
						'enabled'=>$abtest[enabled],
						'ga_slot'=>empty($abtest[ga_slot])?0:$abtest[ga_slot],
						'omni_slot'=>empty($abtest[omni_slot])?0:$abtest[omni_slot],
						'seg_algo'=>$abtest[seg_algo],
						'filters'=>$abtest[filters],
						'completed'=>$abtest[completed],
						'source_type'=>$abtest[source_type]
					);
		func_array2updateWithTypeCheck($this->tab_mk_abtesting_tests, $arrayToInsertTest,"id='".mysql_real_escape_string($abtest[id])."'") ;
		
		$arrayToInsertVariant = array(
						'ab_test_id'=>$abtest['id'],
					);
		foreach ($abtest['variations'] as $var){
			$arrayToInsertVariant['name']=$var[name];
			$arrayToInsertVariant['percent_probability']=$var[p_prob];
			$arrayToInsertVariant['inline_html']=$var[inline_html];
			$arrayToInsertVariant['final_variant']=0;
			$arrayToInsertVariant['jsFile']=$var[jsFile];
			$arrayToInsertVariant['cssFile']=$var[cssFile];
			$arrayToInsertVariant['algo_config_json']=json_encode($var[algo_config]);
			$arrayToInsertVariant['config_json']=json_encode($var[config]);
			if($var[id]>0){
				func_array2updateWithTypeCheck($this->tab_mk_abtesting_variations, $arrayToInsertVariant,"id='".mysql_real_escape_string($var[id])."'");
			}else{
				func_array2insertWithTypeCheck($this->tab_mk_abtesting_variations, $arrayToInsertVariant);
			}
		}
		
		global $login;
		$this->addAuditLog($abtest['id'], $login, $changelog, $comments,$storedTestArray);
		\ABTestingVariables::refreshAllInCache();
		ResolverFactory::clearResolverCache($abtest['name']);
		return true;
	}
	
	
	public function createTestFromArray($abtest,$comments){
		
		if($this->isTestExisting($abtest[name])){
			throw new MABDuplicateTestException($abtest);
		}
		$changelog="<font color=\"green\" style=\"font: bold;\">New Test Created:</font>";
		$arrayToInsertTest = array(
						'name'=>$abtest[name],
						'enabled'=>$abtest[enabled],
						'ga_slot'=>empty($abtest[ga_slot])?0:$abtest[ga_slot],
						'omni_slot'=>empty($abtest[omni_slot])?0:$abtest[omni_slot],
						'seg_algo'=>$abtest[seg_algo],
						'filters'=>$abtest[filters],
						'completed'=>0,
						'source_type'=>$abtest[source_type]
					);
		$newTestId = func_array2insertWithTypeCheck($this->tab_mk_abtesting_tests, $arrayToInsertTest);
				
		$arrayToInsertVariant = array(
						'ab_test_id'=>$newTestId,
					);
		foreach ($abtest['variations'] as $var){
			$arrayToInsertVariant['name']=$var[name];
			$arrayToInsertVariant['percent_probability']=$var[p_prob];
			$arrayToInsertVariant['inline_html']=$var[inline_html];
			$arrayToInsertVariant['final_variant']=0;
			$arrayToInsertVariant['jsFile']=$var[jsFile];
			$arrayToInsertVariant['cssFile']=$var[cssFile];
			$arrayToInsertVariant['algo_config_json']=json_encode($var[algo_config]);
			$arrayToInsertVariant['config_json']=json_encode($var[config]);
			func_array2insertWithTypeCheck($this->tab_mk_abtesting_variations, $arrayToInsertVariant);
		}
		
		global $login;
		$this->addAuditLog($newTestId, $login, $changelog, $comments);

		return true;
	}
	
	/**
	 * Completes a test with given name,
	 * @param String $abtestname test to mark completed
	 * @param String $variationName final variant selected
	 * @param String $comments 
	 */
	public function completeTest($abtestname,$variationName,$comments){
		$storedTestArray=$this->fetchTestArrayByName($abtestname);
		$abtestid=$storedTestArray['id'];
		$variationid=$storedTestArray['variations'][$variationName]['id'];
		if(empty($abtestid)|| empty($variationid))
		return false;
		
		$sql="update {$this->tab_mk_abtesting_tests} set completed='1' where id=$abtestid";
		db_query($sql);
	
		$sql="update {$this->tab_mk_abtesting_variations} set percent_probability='100',final_variant=1 where id=$variationid";
		db_query($sql);
		$sql="update {$this->tab_mk_abtesting_variations} set percent_probability='0' where id!=$variationid and ab_test_id=$abtestid";
		db_query($sql);
		$changelog=self::$COMMENT_COMPLETE."$variationName rolled out to everyone.";
		global $login;
		$this->addAuditLog($abtestid, $login, $changelog, "REASON:".$comments,$storedTestArray);
		\ABTestingVariables::refreshAllInCache();
		return true;
	}
	
	
	/**
	 * Adds audit logs for a test
	 * @param int $abtestid
	 * @param String $user
	 * @param String $changelog
	 * @param String $comments
	 * @param array $testArray array to dump as json 
	 */
	public function addAuditLog($abtestid,$user,$changelog,$comments,$testArray=array()){
		if(empty($abtestid))
		return false;
		$sql="insert into mk_abtest_audit_logs (abtestid,login,details,comment,jsondump) values('$abtestid','$user','".mysql_real_escape_string($changelog)."','".mysql_real_escape_string($comments)."','".addslashes(json_encode($testArray))."')";
		db_query($sql);
	}
	
	/**
	 * 
	 * Returns audit logs for a test with pagination
	 * @param int $abtestid
	 * @param int $start
	 * @param int $limit
	 * @param String $order order by clause
	 */
	public function getAuditLogs($abtestid,$start=0,$limit=30,$order="order by lastmodified desc"){
		$sql="select * from mk_abtest_audit_logs where abtestid='$abtestid' $order";
		if(!empty($limit)){
			$sql.=" limit $start,$limit";
		}
		$result=func_query($sql);
		return $result;
	}
	
	/**
	 * Recursively finds and return difference between two arrays
	 * @param array $aArray1
	 * @param array $aArray2
	 */
	public function arrayRecursiveDiff($aArray1, $aArray2) {
		$aReturn = array();
	
		foreach ($aArray1 as $mKey => $mValue) {
			if (array_key_exists($mKey, $aArray2)) {
				if (is_array($mValue)) {
					$aRecursiveDiff = $this->arrayRecursiveDiff($mValue, $aArray2[$mKey]);
					if (count($aRecursiveDiff)) {
						$aReturn[$mKey] = $aRecursiveDiff;
					}
				} else {
					if ($mValue != $aArray2[$mKey]) {
						$aReturn[$mKey] = $mValue;
					}
				}
			} else {
				$aReturn[$mKey] = $mValue;
			}
		}
	
		return $aReturn;
	}
	
	private function generateChangeLog($diffNewArray,$diffOldArray,$abtestNew,$abtestOld){

		$keysNew = array_keys($diffNewArray);
		$keysOld = array_keys($diffOldArray);
		$keysUnion = array_unique(array_merge($keysNew,$keysOld));
		$logs=array();
		foreach($keysUnion as $key){
			if($key=='enabled'){
				if(!empty($diffNewArray[$key])){
					$logs[]="Test Enabled";
				}else{
					$logs[]="Test Disabled";
				}
			}elseif($key=='ga_slot'){
				$logs[]="GA Slot Changed From '$diffOldArray[$key]' to '$diffNewArray[$key]' ";
			}elseif($key=='omni_slot'){
				$logs[]="Omniture Slot Changed From '$diffOldArray[$key]' to '$diffNewArray[$key]' ";
			}elseif($key=='seg_algo'){
				$logs[]="Segmentation Algo Changed From '$diffOldArray[$key]' to '$diffNewArray[$key]' ";
			}elseif($key=='filters'){
				$logs[]="Filters Changed From '$diffOldArray[$key]' to '$diffNewArray[$key]'  ";
			}elseif($key=='completed'){
				$logs[]="Test Completed ";
			}elseif($key=='source_type'){
				$logs[]="Source Type Changed From '$diffOldArray[$key]' to '$diffNewArray[$key]' ";
			}
		}
		
		$keysNew = array_keys($diffNewArray['variations']);
		$keysOld = array_keys($diffOldArray['variations']);
		if(!is_array($keysNew)){
			$keysNew=array();
		}
		if(!is_array($keysOld)){
			$keysOld=array();
		}
		$keysUnion = array_unique(array_merge($keysNew,$keysOld));
		
		foreach($keysUnion as $key){
			if(!empty($diffNewArray['variations'][$key]) && empty($abtestOld['variations'][$key])){
				$logs[]="Variant {$diffNewArray['variations'][$key]['name']} Added";
				continue;
			}else if(empty($abtestNew['variations'][$key]) && !empty($diffOldArray['variations'][$key])){
				$logs[]="Variant {$diffOldArray['variations'][$key]['name']} Deleted";
				continue;
			}
			
			foreach ($diffNewArray['variations'][$key] as $prop=>$value){
				if($prop=='p_prob'){
					$logs[]="Variant '$key' Probability Changed From '{$diffOldArray['variations'][$key][$prop]}' to '{$value}' ";
				}elseif($prop=='inline_html'){
					$logs[]="Variant '$key' Inline Html Changed ";
				}elseif($prop=='jsFile'){
					$logs[]="Variant '$key' JS File Changed From '{$diffOldArray['variations'][$key][$prop]}' to '{$value}' ";
				}elseif($prop=='cssFile'){
					$logs[]="Variant '$key' CSS File Changed From '{$diffOldArray['variations'][$key][$prop]}' to '{$value}' ";
				}elseif($prop=='algo_config'){
					$logs[]="Variant '$key' Algo Config Changed";
					$logs[]="<div style='color:orange'>".implode('<br>',array_map(array(self, 'preAppend'),$this->generateArrayChangeLog($diffNewArray['variations'][$key][$prop],
					$diffOldArray['variations'][$key][$prop],$abtestNew['variations'][$key][$prop],$abtestOld['variations'][$key][$prop])))."</div>";
				}elseif($prop=='config'){
					$logs[]="Variant '$key' Configuration Changed";
					$logs[]="<div style='color:orange'>".implode('<br>',array_map(array(self, 'preAppend'),$this->generateArrayChangeLog($diffNewArray['variations'][$key][$prop],
					$diffOldArray['variations'][$key][$prop],$abtestNew['variations'][$key][$prop],$abtestOld['variations'][$key][$prop])))."</div>";
				}
				
			}
		}
	
		return implode('<br>',$logs);
	}
	
	private function generateArrayChangeLog($diffNewArray=array(),$diffOldArray=array(),$origNew,$origOld){
		$keysNew = array_keys($diffNewArray);
		$keysOld = array_keys($diffOldArray);
		if(!is_array($keysNew)){
			$keysNew=array();
		}
		if(!is_array($keysOld)){
			$keysOld=array();
		}
		$keysUnion = array_unique(array_merge($keysNew,$keysOld));
		$logs=array();
		foreach($keysUnion as $key){
			if(empty($diffNewArray[$key])&&!empty($origOld[$key])){
				$logs[]="Deleted: '$key'";
			}else if(!empty($origNew[$key])&&empty($diffOldArray[$key])){
				$logs[]="Added  : '$key'";
			}else{
				if(is_array($diffNewArray[$key])){
					$logs[]="Updated:  '$key' Array Changed";
					$logs=array_merge($logs,array_map(array(self, 'preAppend'),$this->generateArrayChangeLog($diffNewArray[$key],$diffOldArray[$key],
					$origNew[$key],$origOld[$key])));
				}else{
					$logs[]="Updated: '$key' from '{$diffOldArray[$key]}' to '{$diffNewArray[$key]}'";
				}
			}
		}
		return $logs;
	}
	
	private static function preAppend($row){
		return "&nbsp;&nbsp;".$row;
	}
	
	/**
	 * Fetches AB Tests form DB
	 * @param String $condition 
	 * @param unknown_type $orderby
	 */
	public function fetchALLABTestArray($condition=false,$orderby=" order by t.id desc") {
		$query="SELECT t.id tid,t.name tname,t.enabled tenabled,t.completed tcompleted,t.api_version tapi_version,v.id vid,t.source_type tsource_type,
		t.seg_algo tseg_algo,t.ga_slot tga_slot,t.omni_slot tomni_slot,t.filters tfilters,v.name vname,v.percent_probability vp_prob,v.inline_html vinline_html,
		v.final_variant vfinal_variant,v.jsFile,v.cssFile,v.config_json,v.algo_config_json FROM mk_abtesting_tests t,mk_abtesting_variations v where v.ab_test_id=t.id";
		
		
		if(!empty($condition)){
			$query.=" and $condition";
		}
		$query.="$orderby";
		$resultSet = func_query($query,true);
		
		$array=self::buildArray($resultSet);
		return $array;
	}
	
	
	/**
	 * Logs an access to a completed test
	 * @param int $testId
	 * @param String $file file from the test is accessed from
	 * @param unknown_type $line line number in file
	 */
	public function logCompletedTestAccess($testId,$file,$line) {
		$logs=$this->getAuditLogs($testId,0,20);
		foreach ($logs as $log){
			if($log['comment']===self::$COMMENT_FILE."$file ($line)"){
				return;
			}
			if(substr($log['details'], 0, strlen(self::$COMMENT_COMPLETE)) === self::$COMMENT_COMPLETE){
				break;
			}
		}
		$this->addAuditLog($testId, "ABTest Framework", self::$COMMENT_FIXME." Test has
		been marked completed but code has not been removed yet", self::$COMMENT_FILE."$file ($line)");
	}
	
	
	public function logUndefinedTestAccess($testName,$file,$line) {
		global $weblog, $errorlog;
		$weblog->error("Undefined MAB test Access: {$testName}  File: {$file} Line:{$line}");
		$errorlog->error("Undefined MAB test Access: {$testName}  File: {$file} Line:{$line}");
	}
	
	private function buildArray($resultSet){
		$array=array();
		foreach ($resultSet as $row){
			$array[$row['tname']]['name']=$row['tname'];
			$array[$row['tname']]['enabled']=$row['tenabled'];
			$array[$row['tname']]['id']=$row['tid'];
			$array[$row['tname']]['filters']=$row['tfilters'];
			$array[$row['tname']]['ga_slot']=$row['tga_slot'];
			$array[$row['tname']]['omni_slot']=$row['tomni_slot'];
			$array[$row['tname']]['seg_algo']=$row['tseg_algo'];
			$array[$row['tname']]['completed']=$row['tcompleted'];
			$array[$row['tname']]['apiVersion']=$row['tapi_version'];
			$array[$row['tname']]['source_type']=$row['tsource_type'];
			$array[$row['tname']]['variations'][$row['vname']]['name']=$row['vname'];
			$array[$row['tname']]['variations'][$row['vname']]['id']=$row['vid'];
			$array[$row['tname']]['variations'][$row['vname']]['p_prob']=$row['vp_prob'];
			$array[$row['tname']]['variations'][$row['vname']]['inline_html']=$row['vinline_html'];
			$array[$row['tname']]['variations'][$row['vname']]['final_variant']=$row['vfinal_variant'];
			$array[$row['tname']]['variations'][$row['vname']]['jsFile']=$row['jsFile'];
    		$array[$row['tname']]['variations'][$row['vname']]['cssFile']=$row['cssFile'];
    		if(!empty($row['config_json'])){
    			$array[$row['tname']]['variations'][$row['vname']]['config']=self::convertToArrayRecursive(json_decode($row['config_json']));
    		}
    		if(!empty($row['algo_config_json'])){
    			$array[$row['tname']]['variations'][$row['vname']]['algo_config']=self::convertToArrayRecursive(json_decode($row['algo_config_json']));
    		}
		}
		return $array;
	}

	public static function convertToArrayRecursive($array){
		if (is_object($array)) {
			// Gets the properties of the given object
			// with get_object_vars function
			$array = get_object_vars($array);
		}

		if (is_array($array)) {
			/*
			 * Return array converted to object
			* Using __FUNCTION__ (Magic constant)
			* for recursive call
			*/
			return array_map(array(self, 'convertToArrayRecursive'), $array);//array_map(array(self, self::convertToArrayRecursive), $array);
		}
		else {
			// Return array
			return $array;
		}
	}
	
	/**
	 * Return true if a test with given name exists
	 * @param unknown_type $name
	 */
	public function isTestExisting($name){
		$query="SELECT t.id tid FROM mk_abtesting_tests t where t.name='$name'";
		$resultSet = func_query($query,true);
		return !empty($resultSet);
	}
	
	/**
	 * Fetch a test by name
	 * @param unknown_type $name
	 */
	public function fetchTestArrayByName($name){
		$result = $this->fetchALLABTestArray("t.name='".mysql_real_escape_string($name)."'");
		return $result[$name];
	}
	
	/**
	 * Fetch Test by id
	 * @param unknown_type $id
	 */
	public function fetchTestArrayById($id){
		$result = $this->fetchALLABTestArray("t.id='".mysql_real_escape_string($id)."'");
		foreach ($result as $row){
			return $row;
		}
		return false;
	}
}

?>