<?php
namespace abtest\exception;

global $xcart_dir;
include_once "$xcart_dir/exception/MyntraException.php";
class MABDuplicateTestException extends \MyntraException{
	
	public function __construct($abtest){
		parent::__construct("MAB Test with name {$abtest[name]} already exists!!");
	}
}