<?php
namespace abtest\resolver;
/**
 * Interface for fast resolver, 
 * Output of Segmentation Algo MABTestAlgo c
 * For Resolving the cookie values to variant 
 * @author yogesh
 *
 */
interface Resolver{
	/**
	 * Resolves a segmentation value to test variant
	 * @param String or int $segmentValue
	 * @return String varient to which user is assigned to 
	 */
	public function resolve($segmentValue);
	
	/**
	 * Resolve a segmentValue from variant name
	 * @param unknown_type $variantName
	 */
	public function reverseResolve($variantName);
}
?>