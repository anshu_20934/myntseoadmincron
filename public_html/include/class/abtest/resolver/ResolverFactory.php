<?php
namespace abtest\resolver;

use abtest\resolver\impl\DefaultResolver;


/**
 * Resolver factory to get resolver object for a test.
 * Resolver object are cached by the factory
 * @author yogesh
 *
 */
class ResolverFactory{
	
	/**
	 * All Algo to Resolver class mapping
	 * @var array
	 */
	private static $_ALGO_RESOLVER_MAPPING=	array('abtest\\algos\\impl\\RandomSegmentationAlgo'=>'abtest\\resolver\\impl\\RandomAlgoResolver',
												  'abtest\\algos\\impl\\NewUserOldUserRandomSegmentationAlgo'=>'abtest\\resolver\\impl\\NewUserOldUserRandomAlgoResolver',
												);	
	
	private static $__RESOLVER_TAG="MAB-RES";
	
	
	/**
	 * Returns resolver object for a test.
	 * @param String $mabTestName
	 * @return Resolver
	 */
	public static function getFastResolver($mabTestName){
		return self::createResolverObject($mabTestName);
	}


	private  static function createResolverObject($mabTestName){
		$abtest=\ABTestingVariables::getTestVariables($mabTestName);
		$algoClass=$abtest['seg_algo'];
		if(!empty(self::$_ALGO_RESOLVER_MAPPING[$algoClass])){
			$resolverClass=self::$_ALGO_RESOLVER_MAPPING[$algoClass];
			$instance = new $resolverClass($mabTestName);
		}else{
			$instance = new DefaultResolver();
		}
		
		return $instance;
	}
	
	/**
	 * Clears resolver object for a test from cache
	 * @param String $mabTestName
	 */
	public static function clearResolverCache($mabTestName){
		global $xcache ;
		
		if($xcache == null){
			$xcache=new \XCache();
		}
		$key=self::$__RESOLVER_TAG."-".$mabTestName;
		$xcache->remove($key);
	}
}
?>