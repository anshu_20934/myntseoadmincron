<?php
namespace abtest\resolver\impl;
use abtest\resolver\Resolver;

class RandomAlgoResolver implements Resolver{
	private $mabTestName;
	private $variations;
	public function __construct($mabTestName){
		$this->mabTestName=$mabTestName;
		$abtest=\ABTestingVariables::getTestVariables($mabTestName);
		$min=1;
		foreach ($abtest['variations'] as $variation){
			$this->variations[$variation['name']]['name']=$variation['name'];
			$this->variations[$variation['name']]['min']=$min;
			$this->variations[$variation['name']]['max']=$min+(int)$variation['p_prob']-1;
			$min+=(int)$variation['p_prob'];
		}
	}
	public function resolve($segmentValue){
		$segmentValue=(int)$segmentValue;
		foreach ($this->variations as $variation){
			if($segmentValue>=$variation['min']&& $segmentValue<=$variation['max']){
				return $variation['name'];
			}
		}
		return 'control';
	}
	
	public function reverseResolve($variantName){
		foreach ($this->variations as $variation){
			if($variantName===$variation['name']){
				return $variation['min'];
			}
		}
	}
}
?>