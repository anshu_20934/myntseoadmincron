<?php
namespace abtest\resolver\impl;
use abtest\MABTest;

use abtest\resolver\Resolver;

class NewUserOldUserRandomAlgoResolver implements Resolver{
	private $mabTestName;
	private $varOldUsers;
	private $varNewUsers;
	public function __construct($mabTestName){
		$this->mabTestName=$mabTestName;
		$abtest=\ABTestingVariables::getTestVariables($mabTestName);
		$min=1;
		foreach ($abtest['variations'] as $variation){
			$this->varOldUsers[$variation['name']]['name']=$variation['name'];
			$this->varOldUsers[$variation['name']]['min']=$min;
			$this->varOldUsers[$variation['name']]['max']=$min+(int)$variation['algo_config']['old']-1;
			$min+=(int)$variation['algo_config']['old'];
		}
		$min=1;
		foreach ($abtest['variations'] as $variation){
			$this->varNewUsers[$variation['name']]['name']=$variation['name'];
			$this->varNewUsers[$variation['name']]['min']=$min;
			$this->varNewUsers[$variation['name']]['max']=$min+(int)$variation['algo_config']['new']-1;
			$min+=(int)$variation['algo_config']['new'];
		}
	}
	public function resolve($segmentValue){
		
		$split= explode(":",$segmentValue);
		$segmentValue=(int)$split[1];
		if($split[0]=='old'){
			$variations = $this->varOldUsers;
			MABTest::getInstance()->addTrackingTags($this->mabTestName,"userType","old");
		}else{
			$variations = $this->varNewUsers;
			MABTest::getInstance()->addTrackingTags($this->mabTestName,"userType","new");
		}
		foreach ($variations as $variation){
			if($segmentValue>=$variation['min']&& $segmentValue<=$variation['max']){
				return $variation['name'];
			}
		}
		return 'control';
	}
	
	public function reverseResolve($variantName){
		foreach ($this->varOldUsers as $variation){
			if($variantName===$variation['name']  && $variation['max']>=$variation['min']){
				return "old:".$variation['min'];
			}
		}
	}
}
?>