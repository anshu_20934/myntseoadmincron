<?php 
namespace abtest;
use abtest\filters\MABFilterFactory;
use abtest\dao\MABDBAdapter;
use abtest\filters\impl\MABFilterChainImpl;

use abtest\algos\MABTestSegmentationAlgoFactory;
use abtest\resolver\ResolverFactory;
use instrumentation\MABTestTracker;

include_once \HostConfig::$documentRoot."/include/class/class.feature_gate.keyvaluepair.php";
include_once(\HostConfig::$documentRoot."/include/class/class.ab_testing_variables.php");
require_once (\HostConfig::$documentRoot."/databaseinit.php");
include_once (\HostConfig::$documentRoot."/AnalyticsBase.php");
/**
 * 
 * MABTest class,
 * This class manages all the abtests defined 
 * @author yogesh
 *
 */
class MABTest{
	
	//Source type tpl
	const _SOURCE_TYPE_TPL="tpl";
	//Source type inline html
	const _SOURCE_TYPE_HTML_INLINE="inline_html";
	//Source type Jss Css Html
	const _SOURCE_TYPE_JS_CSS_HTML_INLINE="inline_js_css_html";
	//ABtest completed test cache key
	const _COMPLETED_TEST_CACHE="mab_completed_test_cache";
	//Abtest undefined test cache key
	const _UNDEFINED_TEST_CACHE="mab_undefined_test_cache";
	
	private $apiVersion = 0;
	
	private $testsMap=array();
	
	private $completeTestsMap=array();
	
	private $completedTestsCache = null;
	
	private $jsArray=array();

	private $cssArray=array();

	private $ga_calls="/*MAB GA CALLS*/";

	private $css="";

	private static $instance = null;

	private function __construct(){
	}

	private function init(){

		$this->apiVersion = \HostConfig::$currentMABTestAPIVersion;
		$abtests=\ABTestingVariables::getAllTestVariables();
		//check if dev mode is enabled or not
		$testModeEnabled=\FeatureGateKeyValuePairs::getBoolean("mabtest.testmode.enable");
		global $cookieprefix,$cookiedomain;
		
		$cookieName=$cookieprefix.'mab';
		if(!empty($_COOKIE[$cookieName]))
			$cookieCombinedTestValue=explode('||',$_COOKIE[$cookieName]);
		foreach ($cookieCombinedTestValue as $value){
			$split=explode('|',$value);
			$cookieCombinedValue[$split[0]]=$split[1];
		}
		
		foreach ($abtests as $abtest){
			$filteredOut=false;
			$userSegment = 'control';
			$cookieValue="";
			if( !empty( $_GET[$abtest['name']]) && in_array($_GET[$abtest['name']],array_keys($abtest['variations']))){
				$userSegment=$_GET[$abtest['name']];
			}else if($abtest['enabled']==='1' && $abtest['apiVersion'] <= $this->apiVersion){
				//If test is enabled then only we need to try to segment user
				
				// we need to apply filters even the user is already segmented
				if(!empty($abtest['filters'])){
					// If filters are associated with test
					//create filterchain and add all filters
					$filterChain=new MABFilterChainImpl($abtest['name']);
					$filters=explode(",",$abtest['filters']);
					foreach ($filters as $filterName){
						$filterObject=MABFilterFactory::getFilter($filterName);
						$filterChain->addFilter($filterObject);
					}
					//execute filterchain
					$filteredOut=$filterChain->doFilter();
				}
				//if dev mode is enabled do not use cookie
				if(!empty($cookieCombinedValue[$abtest['name']]) && $testModeEnabled!=true){
					if($filteredOut===false){
						$cookieValue=$cookieCombinedValue[$abtest['name']];
					}
				}else{
					if($filteredOut===false){
						//No need to execute segmentation if user is filtered out
						$segmentAlgo=MABTestSegmentationAlgoFactory::getSegmentationAlgo($abtest['name']);
						$cookieValue=$segmentAlgo->generateSegment($abtest['name']);
					}
				}
				if($filteredOut===false){
					$resolver=ResolverFactory::getFastResolver($abtest['name']);
					$userSegment=$resolver->resolve($cookieValue);
				}
				$cookieCombinedValue[$abtest['name']]=$cookieValue;
			}
			
			$this->testsMap[$abtest['name']]['user-segment']=$userSegment;
			$this->testsMap[$abtest['name']]['filtered-out']=(string)$filteredOut;
			$this->testsMap[$abtest['name']]['cookie-value']=$cookieValue;
			$this->testsMap[$abtest['name']]['seg-algo']=$abtest['seg-algo'];
			$this->testsMap[$abtest['name']]['filters']=$abtest['filters'];
			$this->testsMap[$abtest['name']]['ga_slot']=$abtest['ga_slot'];
			$this->testsMap[$abtest['name']]['omni_slot']=$abtest['omni_slot'];
			$this->testsMap[$abtest['name']]['enabled']=$abtest['enabled'];
			$this->testsMap[$abtest['name']]['ga_set']=false;
			$this->testsMap[$abtest['name']]['should-track']=false;
			$this->css.=" mab-{$abtest['name']}-$userSegment ";
		}
		
		$cookieValueArray=array();
		foreach ($cookieCombinedValue as $key=>$value){
			if(empty($value)){
				continue;
			}
			$cookieValueArray[]="$key|$value";
		}
                
		setcookie($cookieName, implode('||',$cookieValueArray), time() + (60 * 60 * 24 * 365), '/', $cookiedomain,false,true);
			
	}
	
	/**
	 * Returns Instance for MABTest 
	 * @return \abtest\MABTest
	 */
	public static function getInstance(){
		if(self::$instance == null){
			self::$instance = new MABTest(); 
			self::$instance->init();
		}
		return self::$instance;
	}
	
	/**
	 * returns usersegment for a particular abtest
	 * @param String $mabTestName
	 */
	public function getUserSegment($mabTestName){
		global $smarty;
		$test=$this->testsMap[$mabTestName];
		if(!empty($test)){
			if($test['ga_set']===false && $test['enabled']==='1'){
				// ga_set is not specific to Google Analytics. It should have been renamed to tracking_set
				$segment=$test['user-segment'];
				foreach ($test["tracking-data"] as $key=>$value){
					$segment.="-$key:$value";
				}
				if(!empty($test['ga_slot'])) {
					// GA tracking only when ga slot is defined. ga_slot values will vary from 1-5
					$this->ga_calls="_gaq.push([\"_setCustomVar\", {$test['ga_slot']}, \"$mabTestName\", \"{$segment}\", 1]);".$this->ga_calls;
					if($smarty!=null){
						$smarty->assign("_MAB_GA_calls",$this->getGACallsString());
					}
				}
				
				if(!empty($test['omni_slot'])){
					// Set ABtest segment data into omniture slots.
					$analyticsObj = \AnalyticsBase::getInstance();
					
					// In Omniture admin, $mabTestName would have been set to $test['omni_slot']
					// Example: newVsOld ABTest in omniture is defined for eVar25, basically omni_slot=25
					$analyticsObj->setABTestData($test['omni_slot'], $segment);
				}
				
				$this->testsMap[$mabTestName]['ga_set']=true;
			}
			$this->testsMap[$mabTestName]['should-track']=true;
			return $test['user-segment'];
		}
		$test=$this->getCompletedTest($mabTestName);
		if(!empty($test) && $this->completeTestsMap[$mabTestName]['css-set']!=true){
			$this->css.=" mab-$mabTestName-".$test['user-segment']." ";
			if($smarty!=null){
				$smarty->assign("_MAB_css_classes",$this->getCSSclasses());
			}
			$this->completeTestsMap[$mabTestName]['css-set']=true;
		}
		
		if(!empty($test))
			return $test['user-segment'];
		return 'control';
	}
	
	/**
	 * returns source value i.e inline html value for a test
	 * @param String $mabTestName
	 */
	public function getSourceValue($mabTestName){
		$userSegment=$this->getUserSegment($mabTestName);
		$abtest=\ABTestingVariables::getTestVariables($mabTestName);
		if(empty($abtest)){
			$abtest = $this->getCompletedTest($mabTestName);
		}		
		if($abtest['source_type']===self::_SOURCE_TYPE_JS_CSS_HTML_INLINE){
			$this->addExtraJsCssForTest($abtest,$userSegment);
		}
		return $abtest['variations'][$userSegment]['inline_html'];
	}
	
	/**
	 * Returns config value for config based abtests
	 * @param String $mabTestName
	 */
	public function getConfigValue($mabTestName){
		$userSegment=$this->getUserSegment($mabTestName);
		$abtest=\ABTestingVariables::getTestVariables($mabTestName);
		if(empty($abtest)){
			$abtest = $this->getCompletedTest($mabTestName);
		}
		return $abtest['variations'][$userSegment]['config'];
	}
	
	/**
	 * Adds extra js and css required for specific abtest variation
	 * @param unknown_type $abtest
	 * @param unknown_type $userSegment
	 */
	private function addExtraJsCssForTest($abtest,$userSegment){
		global $smarty;
		if(!empty($abtest['variations'][$userSegment]['jsFile']) && !in_array($abtest['variations'][$userSegment]['jsFile'],$this->jsArray)){
			$this->jsArray[]=$abtest['variations'][$userSegment]['jsFile'];
			if($smarty!=null){
				$smarty->assign('_MAB_Js_Array',$this->jsArray);
			}
		}
		if(!empty($abtest['variations'][$userSegment]['cssFile']) && !in_array($abtest['variations'][$userSegment]['cssFile'],$this->cssArray)){
			$this->cssArray[]=$abtest['variations'][$userSegment]['cssFile'];
			if($smarty!=null){
				$smarty->assign('_MAB_Css_Array',$this->cssArray);
			}
		}
	}
	
	/**
	 * Compose and returns GA call set for all tests
	 */
	public function getGACallsString(){
		return $this->ga_calls;
	}
	
	/**
	 * Logs completed test access
	 * @param unknown_type $testId
	 */
	private function logCompletedTestAccess($testId){
		$trace=debug_backtrace(false);
		$adapter=MABDBAdapter::getInstance();
		$adapter->logCompletedTestAccess($testId,$trace[2]['file'],$trace[2]['line']);
	}
	
	/**
	 * log access for undefined test
	 * @param String $testName
	 */
	private function logUndefinedTestAccess($testName){
		$trace=debug_backtrace(false);
		$adapter=MABDBAdapter::getInstance();
		$adapter->logUndefinedTestAccess($testName,$trace[2]['file'],$trace[2]['line']);
	}
	
	/**
	 * Test which are active are stored in cache, but if code is trying to access a test which is completed,
	 * This function calls would return detail array for that test.
	 * and would log the access in db and log file
	 * @param unknown_type $mabTestName
	 * @return Ambigous <>|boolean|unknown
	 */
	private function getCompletedTest($mabTestName){
		
		global $xcache ;
		if($xcache == null){
			$xcache=new \XCache();
		}
		if($this->completedTestsCache==null){
			$this->completedTestsCache = $xcache->fetch(self::_COMPLETED_TEST_CACHE);
		}
		$cachedVars = $this->completedTestsCache;
		
		if(!empty($cachedVars[$mabTestName])){
			return $cachedVars[$mabTestName];
		}
		$undefinedTestCachedVars = $xcache->fetch(self::_UNDEFINED_TEST_CACHE);
		if(!empty($undefinedTestCachedVars[$mabTestName])){
			return array("user-segment"=>"control");
		}
		$adapter=MABDBAdapter::getInstance();
		$testArray=$adapter->fetchALLABTestArray("t.name='".mysql_real_escape_string($mabTestName)."' and t.completed=1 ");
		
		if(empty($testArray)){
			$undefinedTestCachedVars[$mabTestName]=true;
			$this->logUndefinedTestAccess($mabTestName);
			$xcache->store(self::_UNDEFINED_TEST_CACHE,$undefinedTestCachedVars);
			return array("user-segment"=>"control");
		}
		foreach ($testArray[$mabTestName]['variations'] as $key=>$variation){
			if($variation['final_variant']==='1'){
				$testArray[$mabTestName]['user-segment']=$key;
				break;
			}
		}
		$this->completedTestsCache[$mabTestName]=$testArray[$mabTestName];
		$xcache->store(self::_COMPLETED_TEST_CACHE,$this->completedTestsCache);
		$this->logCompletedTestAccess($testArray[$mabTestName]['id']);
		return $this->completedTestsCache[$mabTestName];
	}
	
	/**
	 * Returns combined css classess for all test seperated by (Space)
	 */
	public function getCSSclasses(){
		return $this->css;
	}
	
	/**
	 * Returns JS files used in abtest segments
	 */
	public function getExtraJsArray(){
		return $this->jsArray;
	}
	
	/**
	 * Returns CsS files used in abtest segments
	 */
	public function getExtraCssArray(){
		return $this->cssArray;
	}
	
	/**
	 * returns tracking data for ab tests
	 * @return MABTestTracker 
	 */
	public function getTrackingData(){
		$tracker= new MABTestTracker();
		foreach ($this->testsMap as $testName=>$test) {
			if($test['should-track']===true){
				$tracker->addTest($testName, $test['user-segment'], $test['filtered-out'],$test["tracking-data"]);
			}
		}
		return $tracker;
	}
	
	public function addTrackingTags($testName,$key,$value){
		$this->testsMap[$testName]["tracking-data"][$key]=$value;
	}
	
	public function updateTemplateData(){
		global $smarty;
		if($smarty!=null){
			$smarty->assign("_MAB_css_classes",$this->getCSSclasses());
			$smarty->assign("_MAB_GA_calls",$this->getGACallsString());
			$smarty->assign('_MAB_Js_Array',$this->jsArray);
			$smarty->assign('_MAB_Css_Array',$this->cssArray);
		}
	}
}
global $_MABTestObject;
$_MABTestObject= MABTest::getInstance();

?>
