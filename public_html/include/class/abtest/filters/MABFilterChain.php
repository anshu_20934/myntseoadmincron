<?php
namespace abtest\filters;
/**
 * Interface for MABFilterChain
 * @author yogesh
 *
 */
interface MABFilterChain{
	/**
	 * Call to this causes execution of next filter in the chain
	 * returns false in case where all the filters are executed successfully 
	 *  and user is viable to apply for segmentation
	 * @return boolean true if user is filtered out otherwise false
	 * 
	 */
	public function doFilter();
}
?>