<?php
namespace abtest\filters;

/**
 * Interface for MABTestFilter
 * @author yogesh
 *
 */
interface MABTestFilter{
	/**
	 * returns true
	 * @param MABFilterChain $filterChain
	 */
	public function doFilter($filterChain);
}
?>