<?php
namespace abtest\filters\impl;
use abtest\filters\MABTestFilter;

class MABAdminUserFilter implements MABTestFilter{
	public function doFilter($filterChain){
		$areatype=constant('AREA_TYPE');
		if($areatype==='A'||$areatype==='P')
			return true;
		
		
		return $filterChain->doFilter();
	}
}
?>