<?php
namespace abtest\filters\impl;
include_once \HostConfig::$documentRoot."/include/sessions.php";
use abtest\filters\MABTestFilter;

class MABNonMyntraUserFilter implements MABTestFilter{
        public function doFilter($filterChain){
                global $XCART_SESSION_VARS;
                $login = $XCART_SESSION_VARS['login'];
                $length = strlen("@myntra.com");
        
                if(substr($login, -$length) != "@myntra.com")
                return true;
                
                return $filterChain->doFilter();
        }
}

?>