<?php
namespace abtest\filters\impl;
use abtest\filters\MABTestFilter;

class MABMyntraUserFilter implements MABTestFilter{
	public function doFilter($filterChain){
		global $login;
		if(substr( $login, strlen( $login ) - strlen( "@myntra.com" ) ) === "@myntra.com")
		return true;
		
		
		return $filterChain->doFilter();
	}
}
?>