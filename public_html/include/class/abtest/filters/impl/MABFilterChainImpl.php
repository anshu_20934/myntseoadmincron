<?php
namespace abtest\filters\impl;
use abtest\filters\MABFilterChain;

/**
 * MABFilterChain implementaion
 * @author yogesh
 *
 */
class MABFilterChainImpl implements  MABFilterChain{
	private $filters;
	private $segmentAlgo;
	private $testName;
	private $filterCounter=0;
	
	public function __construct($testName){
		$this->testName=$testName;
	}
	
	public function doFilter(){
		if($this->filterCounter>=count($this->filters))
			return false;
		$filter=$this->filters[$this->filterCounter];
		$this->filterCounter++;
		return $filter->doFilter($this);
	
	}
	
	/**
	 * Adds a filter in chain.
	 * Filters will be executed in order of insertion
	 * @param unknown_type $filter
	 */
	public function addFilter($filter){
		$this->filters[]=$filter;
	}
	
}

?>