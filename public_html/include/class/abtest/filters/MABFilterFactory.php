<?php
namespace abtest\filters;

/**
* Factory Class For Filter Objects
* It caches filter objects 
* @author yogesh
*
*/
class MABFilterFactory{
	
	private static $__FILTER_TAG="MAB-FIL";
	
	/**
	 * Returns a filter object
	 * @param String $filterClassName
	 * @return MABTestFilter
	 */
	public static function getFilter($filterClassName){
		global $xcache ;

		if($xcache == null){
			$xcache=new \XCache();
		}
		$key=self::$__FILTER_TAG."-".$filterClassName;
		$cachedFilter = $xcache->fetchObject($key);
		if($cachedFilter== null){
			$cachedFilter= new $filterClassName();
			$xcache->storeObject($key,$cachedFilter);
		}
		return $cachedFilter;
	}
}
?>