<?php
/*****************************************************************************\
 +-----------------------------------------------------------------------------+
 | Myntra                                                                      |
 | Copyright (c) 2008 Myntra, Shantanu Bhadoria			                       |
 | All rights reserved.                                                        |
 +-----------------------------------------------------------------------------+
 |                                                                             |
 | The Initial Developer of the Original Code is Shantanu Bhadoria             |
 +-----------------------------------------------------------------------------+
 \*****************************************************************************/

#
# $Id: class.singleton.php,v 1.0
# Class With functions to retrieve a signleton object of other classes
# Singleton Class is a class for which only one instance exists. 
# This means all objects of the class refer to the same instance of the class.
# Useful for performance. e.g avoiding creation of multiple database handle objects 
# in every module included by a script.

class singleton
{
    // implements the 'singleton' design pattern.
    function getinstance ($class)
    {

        static $instances = array();  // array of instance names

        if (!array_key_exists($class, $instances)) {
            // instance does not exist, so create it
            $instances[$class] = new $class;
        } // if

        $instance =& $instances[$class];

        return $instance;

	} // getInstance

	//implements singleton pattern for a mysql object.Note that here we return a precreated object of the class.
	function getmysqlinstance(){
        global $xcart_dir;
		static $instances = array();  // array of instance names
		
        if (!array_key_exists('ezSQL_mysql', $instances)) {
			require_once 'class.readini.php';
			require_once 'class.ez_sql.mysql.php';
			$sql_host = readini::ReadIniValue($xcart_dir.'/env/serverinfo.ini','soapserver','sql_host');
			$sql_user = readini::ReadIniValue($xcart_dir.'/env/serverinfo.ini','soapserver','sql_user');
			$sql_db = readini::ReadIniValue($xcart_dir.'/env/serverinfo.ini','soapserver','sql_db');
			$sql_password = readini::ReadIniValue($xcart_dir.'/env/serverinfo.ini','soapserver','sql_password');
			$db_handle = singleton::getinstance('ezSQL_mysql');
			$db_handle->ezSQL_mysql($sql_user,$sql_password,$sql_db,$sql_host);
			$instances['ezSQL_mysql'] = $db_handle;
		}
		
        $instance =& $instances['ezSQL_mysql'];
		return $instance;
	}

} // singleton
?>
