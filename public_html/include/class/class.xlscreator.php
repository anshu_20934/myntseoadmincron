<?php
##########################################################################
## Author	: Lavanya Tekumalla											##
## Date		: 25 Mar'2009												##
## Copyright Myntra Designs												##
## 																		##
## XLS Converter class													##
## This class takes in a query result/ array of information and writes  ##
## out a string in excel format.																##
##########################################################################

class xlsCreator{

    public $buffer="";

    function xlsBOF() {       
       $this->buffer=$this->buffer.pack("ssssss", 0x809, 0x8, 0x0, 0x10, 0x0, 0x0);
       return;
   }

    function xlsEOF() {        
        $this->buffer=$this->buffer.pack("ss", 0x0A, 0x00);
        return;
    }

    function xlsWriteNumber($Row, $Col, $Value) {        
        $this->buffer=$this->buffer.pack("sssss", 0x203, 14, $Row, $Col, 0x0);
        $this->buffer=$this->buffer.pack("d", $Value);
        return;
    }

    function xlsWriteLabel($Row, $Col, $Value ) {
        $L = strlen($Value);        
        $this->buffer=$this->buffer.pack("ssssss", 0x204, 8 + $L, $Row, $Col, 0x0, $L);
        $this->buffer=$this->buffer.$Value;
    return;
    }

    
    function xlsCreator(){}

    
    //Could be used to directly work on an MYSQL query array result.
	function xlsConvertQueryResult($table,$filename="xlsfile"){
         
         /*header("Pragma: public");
         header("Expires: 0");
         header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
         header("Content-Type: application/force-download");
         header("Content-Type: application/octet-stream");
         header("Content-Type: application/download");;
         header("Content-Type: application/vnd.ms-excel");
         header("Content-Disposition: attachment;filename=$filename.xls"); //
         header("Content-Transfer-Encoding: binary");*/

         xlscreator::xlsBOF();
         if($table) {
             
             $count=0;
             foreach($table[0] as $key=>$value){
                 xlscreator::xlsWriteLabel(0,$count++,$key);
             }

             $xlsRow = 1;
             foreach($table as $tablerow){
                $xlscolumn=0;
                foreach($tablerow as $key=>$value)
                      xlscreator::xlsWriteLabel($xlsRow,$xlscolumn++,$value);
                $xlsRow++;
                }

         }
         xlscreator::xlsEOF();

         return $this->buffer;
    }
}
?>
