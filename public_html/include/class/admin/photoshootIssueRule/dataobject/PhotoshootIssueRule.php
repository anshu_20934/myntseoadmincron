<?php
/** 
  * @author: Ravi Gupta
  */

namespace admin\photoshootIssueRule\dataobject;

/**
 * This a Model Object for DB Table photoshoot_style_size_issue_rule
 * @author Ravi
 *
 */
Class PhotoshootIssueRule
{
	
	private $login; //login of user who creates the DO
	
	// define all the table col as private variable
	private $id;
	private $idBrand;
	private $idArticleType;
	private $idGender;
	private $idAgeGroup;
	private $idUsage;
	private $sizeOption1;
	private $sizeOption2;
	private $sizeOption3;
	private $createdOn;
	private $createdBy;
	private $updatedOn;
	private $updatedBy;
	
	/**
	 * Default constructor
	 * @param String $login user login id
	 */
	function __construct($login)
	{
		$this->login = $login;
	}
		
	/**
	 * Using an array to initialize an object of PhotoshootIssueRule class
	 * @param Array $rawPhotoshootIssueRule with <b>key:column_name</b> and <b>value:column_value</b>
	 */
	public function initPhotoshootIssueRuleObject($rawPhotoshootIssueRule)
	{
		if(empty($rawPhotoshootIssueRule))
			return false;
			
		$this->setId($rawPhotoshootIssueRule['id']);
		$this->setIdBrand($rawPhotoshootIssueRule['id_attribute_type_values_fk_brand']);
		$this->setIdArticleType($rawPhotoshootIssueRule['id_catalogue_classification_fk']);
		$this->setIdGender($rawPhotoshootIssueRule['id_attribute_type_values_fk_gender']);
		$this->setIdAgeGroup($rawPhotoshootIssueRule['id_attribute_type_values_fk_age_group']);
		$this->setIdUsage($rawPhotoshootIssueRule['id_attribute_type_values_fk_usage']);
		$this->setSizeOption1($rawPhotoshootIssueRule['size_option_1']);
		$this->setSizeOption2($rawPhotoshootIssueRule['size_option_2']);
		$this->setSizeOption3($rawPhotoshootIssueRule['size_option_3']);
		$this->setCreatedOn($rawPhotoshootIssueRule['created_on']);
		$this->setCreatedBy($rawPhotoshootIssueRule['created_by']);
		$this->setUpdatedOn($rawPhotoshootIssueRule['updated_on']);
		$this->setUpdatedBy($rawPhotoshootIssueRule['updated_by']);
		return true;
	}
	
	/**
	 * Method to set the PhotoshootIssueRule object id
	 * @param int $id
	 */
	public function setId($id)
	{
		if(!empty($id))
		{
			$this->id = mysql_real_escape_string($id);
		}
	}
	
	/**
	 * Method to return the PhotoshootIssueRule object id and if its value is empty returns NULL
	 * @return int object id and if empty returns NULL
	 */
	public function getId()
	{
		if(!empty($this->id))
		{
			return $this->id;
		}
		return null;
	}
	
	/**
	 * Method to set the PhotoshootIssueRule object's brand id
	 * @param int $idBrand
	 */
	public function setIdBrand($idBrand)
	{
		if(!empty($idBrand))
		{
			$this->idBrand = mysql_real_escape_string($idBrand);
		}
	}
	
	/**
	 * Method to return the PhotoshootIssueRule object's brand id and if its value is empty returns NULL
	 * @return int brand id and if empty returns NULL
	 */
	public function getIdBrand()
	{
		if(!empty($this->idBrand))
		{
			return $this->idBrand;
		}
		return null;
	}
	
	/**
	 * Method to set the PhotoshootIssueRule object's article type id
	 * @param int $idArticleType
	 */
	public function setIdArticleType($idArticleType)
	{
		if(!empty($idArticleType))
		{
			$this->idArticleType = mysql_real_escape_string($idArticleType);
		}
	}
	
	/**
	 * Method to return the PhotoshootIssueRule object's article type id and if its value is empty returns NULL
	 * @return int article type id and if empty returns NULL
	 */
	public function getIdArticleType()
	{
		if(!empty($this->idArticleType))
		{
			return $this->idArticleType;
		}
		return null;
	}
	
	/**
	 * Method to set the PhotoshootIssueRule object's gender id
	 * @param int $idGender
	 */
	public function setIdGender($idGender)
	{
		if(!empty($idGender))
		{
			$this->idGender = mysql_real_escape_string($idGender);
		}
	}
	
	/**
	 * Method to return the PhotoshootIssueRule object's gender id and if its value is empty returns NULL
	 * @return int gender id and if empty returns NULL
	 */
	public function getIdGender()
	{
		if(!empty($this->idGender))
		{
			return $this->idGender;
		}
		return null;
	}
	
	/**
// 	 * Method to set the PhotoshootIssueRule object's age group id
	 * @param int $idAgeGroup
	 */
	public function setIdAgeGroup($idAgeGroup)
	{
		if(!empty($idAgeGroup))
		{
			$this->idAgeGroup = mysql_real_escape_string($idAgeGroup);
		}
	}
	
	/**
	 * Method to return the PhotoshootIssueRule object's age group id and if its value is empty returns NULL
	 * @return int age group id and if empty returns NULL
	 */
	public function getIdAgeGroup()
	{
		if(!empty($this->idAgeGroup))
		{
			return $this->idAgeGroup;
		}
		return null;
	}
	
	/**
	 * Method to set the PhotoshootIssueRule object's usage id
	 * @param int $idUsage
	 */
	public function setIdUsage($idUsage)
	{
		if(!empty($idUsage))
		{
			$this->idUsage = mysql_real_escape_string($idUsage);
		}
	}
	
	/**
	 * Method to return the PhotoshootIssueRule object's usage id and if its value is empty returns NULL
	 * @return int usage id and if empty returns NULL
	 */
	public function getIdUsage()
	{
		if(!empty($this->idUsage))
		{
			return $this->idUsage;
		}
		return null;
	}
	
	/**
	 * Method to set the PhotoshootIssueRule object's 1st size id
	 * @param String $sizeOption1 e.g. US 10
	 */
	public function setSizeOption1($sizeOption1)
	{
		if(!empty($sizeOption1))
		{
			$this->sizeOption1 = mysql_real_escape_string($sizeOption1);
		}
	}
	
	/**
	 * Method to return the PhotoshootIssueRule object' 1st siez id and if its value is empty returns NULL
	 * @return String 1st size option e.g. US 10 and if empty returns NULL
	 */
	public function getSizeOption1()
	{
		if(!empty($this->sizeOption1))
		{
			return $this->sizeOption1;
		}
		return null;
	}
	
	/**
	 * Method to set the PhotoshootIssueRule object's 2nd size id
	 * @param String $sizeOption2 e.g. US 10
	 */
	public function setSizeOption2($sizeOption2)
	{
		if(!empty($sizeOption2))
		{
			$this->sizeOption2 = mysql_real_escape_string($sizeOption2);
		}
	}
	
	/**
	 * Method to return the PhotoshootIssueRule object's 2nd size id and if its value is empty returns NULL
	 * @return String 2nd size option e.g. US 10 and if empty returns NULL
	 */
	public function getSizeOption2()
	{
		if(!empty($this->sizeOption2))
		{
			return $this->sizeOption2;
		}
		return null;
	}
	
	/**
	 * Method to set the PhotoshootIssueRule object's 3rd size id
	 * @param String $sizeOption3 e.g. US 10
	 */
	public function setSizeOption3($sizeOption3)
	{
		if(!empty($sizeOption3))
		{
			$this->sizeOption3 = mysql_real_escape_string($sizeOption3);
		}
	}
	
	/**
	 * Method to return the PhotoshootIssueRule object's 3rd size id and if its value is empty returns NULL
	 * @return String 3rd size option e.g. US 10 and if empty returns NULL
	 */
	public function getSizeOption3()
	{
		if(!empty($this->sizeOption3))
		{
			return $this->sizeOption3;
		}
		return null;
	}
	
	/**
	 * Method to set the PhotoshootIssueRule object's created date
	 * @param int $createdOn
	 */
	public function setCreatedOn($createdOn)
	{
		if(!empty($createdOn))
		{
			$this->createdOn = mysql_real_escape_string($createdOn);
		}
	}
	
	/**
	 * Method to return the PhotoshootIssueRule object's created date and if its value is empty returns NULL
	 * @return int created on and if empty returns NULL
	 */
	public function getCreatedOn()
	{
		if(!empty($this->createdOn))
		{
			return $this->createdOn;
		}
		return null;
	}
	
	/**
	 * Method to set the PhotoshootIssueRule object's created by value
	 * @param string $createdBy
	 */
	public function setCreatedBy($createdBy)
	{
		if(!empty($createdBy))
		{
			$this->createdBy = mysql_real_escape_string($createdBy);
		}
	}
	
	/**
	 * Method to return the PhotoshootIssueRule object's created by value and if its value is empty returns NULL
	 * @return string created by value and if empty returns NULL
	 */
	public function getCreatedBy()
	{
		if(!empty($this->createdBy))
		{
			return $this->createdBy;
		}
		return null;
	}
	
	/**
	 * Method to set the PhotoshootIssueRule object's updated on value
	 * @param int $updatedOn
	 */
	public function setUpdatedOn($updatedOn)
	{
		if(!empty($updatedOn))
		{
			$this->updatedOn = mysql_real_escape_string($updatedOn);
		}
	}
	
	/**
	 * Method to return the PhotoshootIssueRule object's updated on value and if its value is empty returns NULL
	 * @return int updated on value and if empty returns NULL
	 */
	public function getUpdatedOn()
	{
		if(!empty($this->updatedOn))
		{
			return $this->updatedOn;
		}
		return null;
	}
	
	/**
	 * Method to set the PhotoshootIssueRule object's updated by value
	 * @param string $updatedBy
	 */
	public function setUpdatedBy($updatedBy)
	{
		if(!empty($updatedBy))
		{
			$this->updatedBy = mysql_real_escape_string($updatedBy);
		}
	}
	
	/**
	 * Method to return the PhotoshootIssueRule object's updated by value and if its value is empty returns NULL
	 * @return int  and if empty returns NULL
	 */
	public function getUpdatedBy()
	{
		if(!empty($this->updatedBy))
		{
			return $this->updatedBy;
		}
		return null;
	}
	
}