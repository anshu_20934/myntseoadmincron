<?php

namespace admin\photoshootIssueRule\dao;
include_once \HostConfig::$documentRoot."/include/dao/class/class.GenericDAO.php";
use admin\photoshootIssueRule\dataobject\PhotoshootIssueRule;

/**
 * This is DAO class for PhotoshootIssueRule class
 * This handles all the DB related stuff like insert, update delete using PhotoshootIssueRule object
 * @author Ravi
 *
 */
class PhotoshootIssueRuleDAO extends \GenericDAO
{
	const SUCCESS_ADD = 1;
	const ERROR_ADD = -1;
	const ERROR_ADD_SAME_SIZE = -3;
	const ERROR_ADD_ISSUE_ALREADY_EXIST = -2;
	const SUCCESS_DELETE = 1;
	const ERROR_DELETE = -1;
	const SUCCESS_UPDATE = 1;
	const ERROR_UPDATE = -1;
	const SUCCESS_ERROR_UPDATE = -2;
	const ERROR_UNKNOWN = 0;
	
	private $tablePhotoshootIssueRule;
	/**
	 * Private member to hold the singeton instance reference
	 * @var PhotoshootIssueRuleDAO
	 */
	private static $instance = null;
	
	/**
     * Protected Constructor for singleton usability
     */
    protected function __construct()
    {
        $this->tablePhotoshootIssueRule = "photoshoot_style_size_issue_rule";
    }
	
    /**
     * Method to return single instance of DAO for PhotoshootIssueRuleDAO
     * @return Singleton reference of PhotoshootIssueRuleDAO class
     */
	public static function getInstance()
	{
		if(self::$instance==null)
		{
			self::$instance= new PhotoshootIssueRuleDAO();
		}
		return self::$instance;
	}
	
	/**
	 *  Method to get PhotoshotIssueRule from DB as object array
	 *  @param Array $condition in which key:value is column_name:value
	 *  @return PhotoshootIssueRule $photoshootIssueRule objects array
	 */
	public function getPhotoshootIssueRule($condition)
	{
		$rawPhotoshootIssueRule = array();
		$rawPhotoshootIssueRule = $this->getRawDataFromTable($this->tablePhotoshootIssueRule, $condition);
		if(empty($rawPhotoshootIssueRule))
		{
			return null;
		}
		$photoshootIssueRule=array();
		foreach ($rawPhotoshootIssueRule as $rawRule)
		{
			$rule = new PhotoshootIssueRule($rawRule['created_by']);
			$initObj = $rule->initPhotoshootIssueRuleObject($rawRule);
			$photoshootIssueRule[]=$rule;
		}
		return $photoshootIssueRule;
	}
	
	/**
	 * Method to add a PhotoshotIssueRule object in DB
	 * @param PhotoshootIssueRule $photoshootIssueRule
	 * @return boolean about success/failure
	 */
	public function addPhotoshootIssueRule($photoshootIssueRule)
	{
		//check if the rule is already in the db
		$exists = $this->existsPhotoshootIssueRule($photoshootIssueRule, 'add');
		if($exists)
		{
			return $this::ERROR_ADD_ISSUE_ALREADY_EXIST;
		}
		
		$dataAdd = $this->dataArrayPhotoshootIssueRule($photoshootIssueRule, 'add', false);
		if(($dataAdd['size_option_2'] != '') || ($dataAdd['size_option_3'] != ''))
		{
			if(($dataAdd['size_option_1'] == $dataAdd['size_option_2']) 
			|| ($dataAdd['size_option_2'] == $dataAdd['size_option_3']) 
			|| ($dataAdd['size_option_3'] == $dataAdd['size_option_1']))
			{
				return $this::ERROR_ADD_SAME_SIZE;
			}
		}
		$resultAdd = func_array2insertWithTypeCheck($this->tablePhotoshootIssueRule, $dataAdd);
		if($resultAdd)
		{
			return $this::SUCCESS_ADD;
		}
		else
		{
			return $this::ERROR_ADD;
		}
		return $this::ERROR_UNKNOWN; 
	}
	
	/**
	 * Method to deletes the given PhotoshotIssueRule object
	 * @param PhotoshootIssueRule $photoshootIssueRule
	 * @return boolean about success/failure
	 */
	public function deletePhotoshootIssueRule($photoshootIssueRule)
	{
		$id = $photoshootIssueRule->getId();
		$delResult = $this->deleteDataFromTableUsingId($this->tablePhotoshootIssueRule, $id);
		if($delResult == 0)
		{
			return $this::ERROR_DELETE;
		}
		else
		{
			return $this::SUCCESS_DELETE;
		}
	}
		
	/**
	 * Method to update the given PhotoshotIssueRule object or object array
	 * @param PhotoshootIssueRule or Array of PhotoshootIssueRule $photoshootIssueRuleArray
	 * @return array $updateFailedId
	 */
	public function updatePhotoshootIssueRule($photoshootIssueRuleArray)
	{
		$updateSucess = false;
		$updateError = false;
		$updateCommit = false;
		$updateFailedId = array();
		if(is_array($photoshootIssueRuleArray))
		{
			foreach ($photoshootIssueRuleArray AS $num=>$photoshootIssueRule)
			{
		        $resultUpdate = $this->updateSinglePhotoshootIssueRule($photoshootIssueRule);
		        if(!$resultUpdate)
		        {
		        	$updateFailedId[] = $photoshootIssueRule->getId();
		        }
			}
		}
		else
		{
			$photoshootIssueRule = $photoshootIssueRuleArray;
			if(!$resultUpdate)
		    {
		       	$updateFailedId[] = $photoshootIssueRule->getId();
		    }
		}
		return $updateFailedId;		  
	}

	/**
	 * Private method to update the given photoshoot issue rule object
	 * @param PhotoshootIssueRule $photoshootIssueRule
	 * @return boolean about success/failure
	 * 
	 */
	private function updateSinglePhotoshootIssueRule($photoshootIssueRule)
	{
		//check if there is no changes
		$exists = $this->existsPhotoshootIssueRule($photoshootIssueRule, 'update');
		if($exists)
		{
			return false;	
		}
		else
		{
			$dataUpdate = $this->dataArrayPhotoshootIssueRule($photoshootIssueRule, 'update', false);
			if(($dataUpdate['size_option_2'] != '') || ($dataUpdate['size_option_3'] != ''))
			{
				if(($dataUpdate['size_option_1'] == $dataUpdate['size_option_2'])
				|| ($dataUpdate['size_option_2'] == $dataUpdate['size_option_3'])
				|| ($dataUpdate['size_option_3'] == $dataUpdate['size_option_1']))
				{
					return false;
				}
			}
			$resultUpdate = func_array2updateWithTypeCheck($this->tablePhotoshootIssueRule, $dataUpdate, "id=".$photoshootIssueRule->getId()."");
			return $resultUpdate;
		}
	}
	
	/**
	 * Method to check if the given Photoshoot Issue Rule object exists in DB
	 * @param Photoshoot $photoshootIssueRule
	 * @param String $mode takes value add/update
	 * @return boolean true/false
	 */
	private function existsPhotoshootIssueRule($photoshootIssueRule, $mode)
	{
		if($mode == 'add')
		{
			$dataAddExists = $this->dataArrayPhotoshootIssueRule($photoshootIssueRule, 'add', true);
			$exists = func_select_first($this->tablePhotoshootIssueRule, $dataAddExists);
			if(empty($exists))
			{
				return false;
			}
			else
			{
				return true;
			}
		}
		elseif($mode == 'update')
		{
			$dataUpdateExists = $this->dataArrayPhotoshootIssueRule($photoshootIssueRule, 'update', true);
			$exists = func_select_first($this->tablePhotoshootIssueRule, $dataUpdateExists);
			if(empty($exists))
			{
				//now check if similar rule except size value exist at other id, this is for restricting making duplicate rules while updating
				$dataAddExists = $this->dataArrayPhotoshootIssueRule($photoshootIssueRule, 'add', true);
				$exists = func_select_first($this->tablePhotoshootIssueRule, $dataAddExists);
				if(empty($exists))
				{
					return false;
				}
				else
				{
					if($exists['id'] == $photoshootIssueRule->getId())
					{
						return false;
					}
					else
					{
						return true;
					}
				}
			}
			else
			{
				return true;
			}			
		}
	}
	
	/**
	 * Method to create data array from PhotoshootIssueRule object
	 * @param PhotoshootIssueRule $photoshootIssueRule
	 * @param String $mode takes value add/update
	 * @param boolean $exists takes value true/false
	 * @return array $data
	 */
	private function dataArrayPhotoshootIssueRule($photoshootIssueRule, $mode, $exists = false)
	{
		$data = array(
				"id_attribute_type_values_fk_brand"     => $photoshootIssueRule->getIdBrand(),
				"id_catalogue_classification_fk"        => $photoshootIssueRule->getIdArticleType(),
				"id_attribute_type_values_fk_gender"    => $photoshootIssueRule->getIdGender(),
				"id_attribute_type_values_fk_age_group" => $photoshootIssueRule->getIdAgeGroup(),
				"id_attribute_type_values_fk_usage"     => $photoshootIssueRule->getIdUsage()
				);
		if($exists)
		{
			if($mode == 'add')
			{
				return $data;
			}
			elseif($mode == 'update')
			{
				$data["size_option_1"] = $photoshootIssueRule->getSizeOption1();
				$data["size_option_2"] = $photoshootIssueRule->getSizeOption2();
				$data["size_option_3"] = $photoshootIssueRule->getSizeOption3();
				return $data;
			}
		}
		else
		{
			$data["size_option_1"] = $photoshootIssueRule->getSizeOption1();
			$data["size_option_2"] = $photoshootIssueRule->getSizeOption2();
			$data["size_option_3"] = $photoshootIssueRule->getSizeOption3();
			$data["updated_on"] = $photoshootIssueRule->getUpdatedOn();
			$data["updated_by"] = $photoshootIssueRule->getUpdatedBy();
			
			if($mode == 'add')
			{
				$data["created_on"] = $photoshootIssueRule->getCreatedOn();
				$data["created_by"] = $photoshootIssueRule->getCreatedBy();
			}
			
			return $data;
		}
	}
}
?>