<?php

namespace admin\catalog\dao;
use cache\keyvaluepairs\CMSTimerMetaDataKeyValuePairs;

use base\dao\BaseDAO;
include_once("$xcart_dir/include/class/widget/class.widget.keyvaluepair.php");
include_once("$xcart_dir/include/func/func.utilities.php");

/*
* Dao for Catalog Timer
*/
class TimerDBAdapter extends BaseDAO {

    private static $metaDataConfigKey = "cms.timer.metadata";
    const CMS_CATALOG_TIMER_TABLE = 'cms_catalog_timer';

    public function getRoles(){
        $roleData = self::getMetaData();
        return array_keys($roleData);
    }

    public function getMetaData($role){
        $result = CMSTimerMetaDataKeyValuePairs::getKeyValuePairInstance()->getJSONDecodedArray(self::$metaDataConfigKey);
        $result = empty($role) ? $result : $result[$role];
        return $result;
    }

    public function updateMetaData($data){
        if(empty($data) || !is_array($data)){
            $data = array();
        }
        $data = json_encode($data);
        \WidgetKeyValuePairs::setWidgetValueForKey(null, self::$metaDataConfigKey, $data);
        \WidgetKeyValuePairs::refreshKeyValuePairsInCache();
        CMSTimerMetaDataKeyValuePairs::getKeyValuePairInstance()->refresh();
    }

    public function startTimer($styleId, $login, $catalogRole, $articleTypeId, $articleType){
        if(empty($styleId) || empty($login) || empty($catalogRole)){
            return false;
        }
        $now = time();
        $sql = "insert into ". self::CMS_CATALOG_TIMER_TABLE ." (style_id, login, role, start, article_type_id, article_type) 
                VALUES('$styleId', '$login', '$catalogRole', $now, '$articleTypeId', '$articleType')";
                //echo $sql;exit;
        return db_query($sql);
    }

    public function stopTimer($styleId, $login, $catalogRole){
        if(empty($styleId) || empty($login) || empty($catalogRole)){
            return false;
        }
        $now = time();
        $filters = array('login'=>$login, 'style_id'=>$styleId, 'role'=>$catalogRole);
        $data = func_select_first(self::CMS_CATALOG_TIMER_TABLE, $filters, 0, 1, array('start'=>'desc'));
        if($data == null || empty($data['start'])){
            return false;
        }
        $timeTaken = $now - $data['start'];
        $sql = "update ".self::CMS_CATALOG_TIMER_TABLE." set end = $now , time_taken = $timeTaken where style_id = '$styleId' and login = '$login' and role='$catalogRole' and start='".$data['start']."'";
        if(db_query($sql)){
            return $timeTaken;
        } else {
            return false;
        }
    }

}
