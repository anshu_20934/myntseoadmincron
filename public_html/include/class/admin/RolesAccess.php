<?php
/** 
  * @author: Ravi Gupta
  * This class contains all methods to add/update/delete roles access for admin pages
  */

include_once $xcart_dir."/include/dao/class/admin/RolesAccessDBAdapter.php";


Class RolesAccess  {
	
	// define all the table col as private variable
	private $login;
	private $pageid;
	private $pagename;
	private $roles;
	private $extraLoginsAllowed;
	private $created_on;
	private $created_by;
	private $updated_on;
	private $update_by;
	
	function __construct($login){
		$this->login = $login;
	}
		
	/**
	 * using raw data to initialize roles access object
	 */
	public function initRolesAccessObject($rawRolesAccess){
		if(empty($rawRolesAccess))
			return false;
		$this->setPageid($rawRolesAccess['id']);
		$this->setPagename($rawRolesAccess['pagename']);
		$this->setRoles($rawRolesAccess['roles']);
		$this->setExtraLoginsAllowed($rawRolesAccess['extra_logins_allowed']);
		$this->setCreatedOn($rawRolesAccess['created_on']);
		$this->setCreatedBy($rawRolesAccess['created_by']);
		$this->setUpdatedOn($rawRolesAccess['updated_on']);
		$this->setUpdatedBy($rawRolesAccess['updated_by']);
		return true;
	}
	
	public function setPageid($id){
		if(!empty($id)){
			$this->pageid = mysql_real_escape_string($id);
		}
	}
	
	public function getPageid(){
	if(!empty($this->pageid)){
			return $this->pageid;
		}
		return null;
	}
	
	public function setPagename($pagename){
		if(!empty($pagename)){
			$this->pagename = mysql_real_escape_string($pagename);
		}
	}
	
	public function getPagename(){
		if(!empty($this->pagename)){
			return $this->pagename;
		}
		return null;
	}
	
	public function setRoles($roles){
		if(!empty($roles)){
			$this->roles = mysql_real_escape_string($roles);
		}
	}
	
	public function getRoles(){
		if(!empty($this->roles)){
			return $this->roles;
		}
		return null;
	}
	
	public function setExtraLoginsAllowed($extraLoginsAllowed){
		if(!empty($extraLoginsAllowed)){
			$this->extraLoginsAllowed = mysql_real_escape_string($extraLoginsAllowed);
		}
	}
	
	public function getExtraLoginsAllowed(){
		if(!empty($this->extraLoginsAllowed)){
			return $this->extraLoginsAllowed;
		}
		return null;
	}
	
	public function setCreatedOn($created_on){
		if(!empty($created_on)){
			$this->created_on = mysql_real_escape_string($created_on);
		}
	}
	
	public function getCreatedOn(){
		if(!empty($this->created_on)){
			return $this->created_on;
		}
		return null;
	}
	
	public function setCreatedBy($created_by){
		if(!empty($created_by)){
			$this->created_by = mysql_real_escape_string($created_by);
		}
	}
	
	public function getCreatedBy(){
		if(!empty($this->created_by)){
			return $this->created_by;
		}
		return null;
	}
	
	public function setUpdatedOn($updated_on){
		if(!empty($updated_on)){
			$this->updated_on = mysql_real_escape_string($updated_on);
		}
	}
	
	public function getUpdatedOn(){
		if(!empty($this->updated_on)){
			return $this->updated_on;
		}
		return null;
	}
	
	public function setUpdatedBy($updated_by){
		if(!empty($updated_by)){
			$this->updated_by = mysql_real_escape_string($updated_by);
		}
	}
	
	public function getUpdatedBy(){
		if(!empty($this->updated_by)){
			return $this->updated_by;
		}
		return null;
	}
	
}