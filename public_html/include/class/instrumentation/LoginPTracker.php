<?php

namespace instrumentation;

class LoginPTracker
{
    
	const SUCCESS = "success";
	const PASSWORD_FAILURE = "passdoesnotmach";
	const USER_DOESNT_EXISTS_FAILURE = "userdoesntexists";
	const USER_ALREADY_EXISTS_FAILURE = "useralreadyexists";
	const USER_SUSPENDED_FAILURE = "usersuspended";
	var $action; 
	var $finalResult;
	var $userEmail;
	var $userGender;
	
	public function __construct($paramsArray)
	{
	}
	
	public function setAction($action)
	{
		$this->action = $action;
	}
	
	public function setFinalResult($finalResult)
	{
		$this->finalResult = $finalResult;
	}

	public function setUserEmail($userEmail)
	{
		$this->userEmail = $userEmail;
	}
	
	public function setUserGender($userGender)
	{
		$this->userGender = $userGender;
	}
	
}
