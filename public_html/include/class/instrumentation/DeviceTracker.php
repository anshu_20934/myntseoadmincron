<?php

use web\utils\DeviceDetector;

class DeviceTracker{

	var $isMobile = false;
	
	public function __construct()
	{
		$this->isMobile = DeviceDetector::getInstance()->isMobile();
	}
}
