<?php

class SearchPageTracker
{
	var $searchQuery = "";
	var $queriedByUser = false;
	var $numOfResults = 0;
	var $timeTakenToSearch = 0;
	var $filters = null;
	var $resultSet = null;
	var $solrCallLatency = array();
	var $initLatency = 0;
	
	/*
	public function __construct($query, $numOfRes = 0,  $timeTaken = 0, $queriedByUser = false)
	{		
		$this->searchQuery = $query;
		$this->numOfResults = $numOfRes;
		$this->timeTakenToSearch = $timeTaken;
		$this->queriedByUser = $queriedByUser;
	}
	*/
	/*
	 * this function has been written to loosely bound the page sepecific info
	 */
	public function __construct($paramsArray)
	{
		global $_SERVER, $_GET;
		$requestUri = $_SERVER['REQUEST_URI'];
        $requestUri = urldecode($requestUri);
        $qpos = strpos($requestUri,"?");
        $request = $requestUri;
        if ($qpos !== false) {
           $request = substr($requestUri, 0, $qpos);
        }
        $this->searchQuery = strtolower(str_replace("-"," ",str_replace("/","",$request)));
		$this->numOfResults = $paramsArray['numOfSearchResutls'];
		$this->timeTakenToSearch = $paramsArray['timeTakenToSearch'];
		$this->queriedByUser = filter_input(INPUT_GET, 'userQuery', FILTER_SANITIZE_STRING) ? true : false;
	}
	
	public function setNumOfResults($numOfResults)
	{
		$this->numOfResults = $numOfResults;
	}
	
	public function setTimeTakenToSearch($timeTakenToSearch)
	{
		$this->timeTakenToSearch = $timeTakenToSearch;
	}
	
	public function setQueriedByUser($queriedByUser)
	{
		$this->queriedByUser = $queriedByUser;
	}
	
	public function setFilters($json)
	{
		$this->filters = $json;
	}
	
	public function setResultSet($resultSet)
	{
		$this->resultSet = $resultSet;
	}
	
	public function setSolrCallLatency(array $latency)
	{
		$this->solrCallLatency = $latency;
	}
	
	public function setSearchInitLatency($latency)
	{
		$this->initLatency = $latency;
	}
}
