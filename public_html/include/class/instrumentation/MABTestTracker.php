<?php

namespace  instrumentation;

class MABTestTracker{
	var $mabtests=array();
	
	/**
	 * Adds a test in tracking record,
	 * $tracking data is a
	 * @param String $testName
	 * @param String $variantName
	 * @param String $filteredOut
	 * @param String $tracking_data map to tag records with extra details
	 */
	public function addTest($testName,$variantName,$filteredOut,$tracking_data){
		if(empty($filteredOut))
			$filteredOut='false';
		else 
			$filteredOut='true';
		$array=array('abtestName'=>$testName,'variantName'=>$variantName,'filteredOut'=>$filteredOut);
		
		foreach ($tracking_data as $key=>$value){
			$array[$key]=$value;
		}
		
		$this->mabtests[]=$array;
	}
}
?>