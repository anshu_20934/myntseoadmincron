<?php
class PDPTracker
{
	//"styleId" => $pdpStyleId, "pageRank" => $pdpSerpRank, "referrer" => $pdpRefPage
	
	var $styleId;
	var $listPage; //list-widgets-others
	var $serp;
	var $brand;
	var $articleType;
	var $recommendationType;
	var $recommendationStyleIds;
	var $previousPDPStyleId;
	var $previousPDPRecommendationType;
	var $previousPDPRecommendationStyleIds;
	var $availableSizes;
	
	public function __construct($paramsArray)
	{
		global $_GET;
		$this->serp = intval(filter_input(INPUT_GET, 'serp', FILTER_SANITIZE_STRING));
		$this->listPage = strtolower(str_replace("-"," ",filter_input(INPUT_GET, 'searchQuery', FILTER_SANITIZE_STRING)));
	}
	
	public function setStyleId($styleId)
	{
		$this->styleId = $styleId;
	}
	
	public function setPdpReferer($pdpReferer)
	{
		$this->pdpReferer = $pdpReferer;
	}
	
	public function setSerp($serp)
	{
		$this->serp = $serp;
	}

	public function setBrand($brand)
	{
		$this->brand = $brand;
	}

	public function setArticleType($articleType)
	{
		$this->articleType = $articleType;
	}

	public function setPreviousPDPStyleId($previousPDPStyleId)
	{
		$this->previousPDPStyleId=$previousPDPStyleId;
	}

	public function setRecommendationType($recommendationType)
	{
		$this->recommendationType=$recommendationType;
	}

	public function setRecommendationStyleIds($recommendationStyleIds)
	{
		$this->recommendationStyleIds=$recommendationStyleIds;
	}

	public function setPreviousPDPRecommendationType($previousPDPRecommendationType)
	{
		$this->previousPDPRecommendationType=$previousPDPRecommendationType;
	}

	public function setPreviousPDPRecommendationStyleIds($previousPDPRecommendationStyleIds)
	{
		$this->previousPDPRecommendationStyleIds=$previousPDPRecommendationStyleIds;
	}

	public function setAvailableSizes($availableSizes)
	{
		$this->availableSizes = $availableSizes;
	}
}

