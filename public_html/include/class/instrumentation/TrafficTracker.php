<?php
@include_once ($xcart_dir."/include/func/func.seo.php");
@include_once "$xcart_dir/include/class/class.feature_gate.keyvaluepair.php";

class TrafficTracker
{
	const UTM_COOKIE_NAME = 'utm_track_1';
	var $source;
	var $medium;
	var $campaign;
	var $channel;
	
	public function __construct()
	{
        global $_GET;
        global $isCPC;
        global $isOrganicReferer;
        global $seoHost;

        $cookieContent = getUnserializedCookieContent(self::UTM_COOKIE_NAME);
		if(isset($_GET['utm_source']) || isset($_GET['utm_medium']) || isset($_GET['utm_campaign'])){
			$this->source = $_GET['utm_source'];
			$this->medium = $_GET['utm_medium'];
			$this->campaign = $_GET['utm_campaign'];
        }
        elseif($isCPC){
            $this->source = $seoHost; 
            $this->medium = "cpc";
        }
        elseif($isOrganicReferer){
            $this->source = "referral";
            $this->medium = $seoHost;
        }
		elseif(!empty($cookieContent)){
			$this->source = $cookieContent['utm_source'];
			$this->medium = $cookieContent['utm_medium'];
			$this->campaign = $cookieContent['utm_campaign'];
		}
		else{
			$this->source = 'direct';
			$this->medium = 'direct';
			$this->campaign = 'direct';
		}
		$this->channel = self::getChannelFromRules($this->source, $this->medium);
	}

	static function getChannelFromRules($source, $medium)
	{
		$source = strtolower($source);
		$medium = strtolower($medium);

		$updateChannelAttributionLogic = FeatureGateKeyValuePairs::getBoolean('updateChannelAttributionLogic');

		if($updateChannelAttributionLogic)
		{
			if(substr($source, 0, strlen('remarketing')) === 'remarketing')
				return 'Remarketing';
			if(substr($medium, 0, strlen('branding')) === 'branding')
				return 'Branding';
			if(substr($source, 0, strlen('fb')) === 'fb')
				return 'Facebook';
			if($medium == 'cpc')
				return 'CPC';
			if(substr($medium, 0, strlen('mailer')) === 'mailer' && substr($source, 0, 10) !== 'mrp_mailer')
				return 'Mailer';
			if(substr($source, 0, strlen('mrp_mailer')) === 'mrp_mailer')
				return 'MRP';
			if(
				(
					stripos($medium, 'google') !== false  ||
					stripos($medium, 'yahoo') !== false  ||
					stripos($medium, 'bing') !== false  ||
					stripos($medium, 'babylon') !== false  ||
					stripos($medium, 'conduit') !== false  ||
					stripos($medium, 'ask') !== false  ||
					stripos($medium, 'yandex') !== false  ||
					stripos($medium, 'avg') !== false  ||
					stripos($medium, 'aol') !== false  ||
					stripos($medium, 'comcast') !== false  ||
					stripos($medium, 'incredimail') !== false  ||
					stripos($medium, 'baidu') !== false  ||
					stripos($medium, 'search') !== false  
				)
				&& $source == 'referral')
				return 'Organic';
			if(substr($source, 0, strlen('aff')) === 'aff')
				return 'Affiliates';
			if($source == 'track.in.omgpm.com' && $medium == 'referral')
				return 'Affiliates';
			if($source == 'shoogloo' && $medium == 'affiliate')
				return 'Affiliates';
			if(substr($medium, 0, strlen('partnership')) === 'partnership')
				return 'Partnerships';
			if($medium == 'direct' && $source == 'direct')
				return 'Direct';
			return 'Other';
		}else{
			if($medium == 'cpc')
                        return 'CPC';
                	if(
			    (
				stripos($medium, 'google') !== false  ||
                                stripos($medium, 'yahoo') !== false  ||
                                stripos($medium, 'bing') !== false  ||
                                stripos($medium, 'babylon') !== false  ||
                                stripos($medium, 'conduit') !== false  ||
                                stripos($medium, 'ask') !== false  ||
                                stripos($medium, 'yandex') !== false  ||
                                stripos($medium, 'avg') !== false  ||
                                stripos($medium, 'aol') !== false  ||
                                stripos($medium, 'comcast') !== false  ||
                                stripos($medium, 'incredimail') !== false  ||
                                stripos($medium, 'baidu') !== false  ||
                                stripos($medium, 'search') !== false
                            )
                            && $source == 'referral')
                            return 'Organic';
           		if(stripos($medium, 'mailer') !== false && (substr($source, 0, strlen('marketing')) === 'marketing') )
            		    return 'Mailer';
                	if(substr($source, 0, strlen('fb')) === 'fb')
                            return 'Facebook';
                	if(substr($source, 0, strlen('mlr')) === 'mlr')
                            return 'Mailer';
                	if(substr($source, 0, strlen('mrp_mailer')) === 'mrp_mailer')
                            return 'MRP';
                	if(substr($source, 0, strlen('aff')) === 'aff')
                            return 'Affiliates';
                	if(substr($source, 0, strlen('mything')) === 'mything')
                            return 'Affiliates';
                	if(substr($source, 0, strlen('tyroo')) === 'tyroo')
                            return 'Affiliates';
                	if($medium == 'direct' && $source == 'direct')
                            return 'Direct';
                	return 'Other';
        	}
	}
}
