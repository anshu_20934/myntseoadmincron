<?php
class RecommendationTracker
{
	//"styleId" => $pdpStyleId, "pageRank" => $pdpSerpRank, "referrer" => $pdpRefPage
	
	var $styleId;
	var $page;
	var $recommendationType;
	var $recommendationStyleIds;
	
	public function __construct($paramsArray)
	{
		global $_GET;
	}
	
	public function setStyleId($styleId)
	{
		$this->styleId = $styleId;
	}
	
	public function setPage($page)
	{
		$this->page = $page;
	}

	public function setRecommendationType($recommendationType)
	{
		$this->recommendationType=$recommendationType;
	}

	public function setRecommendationStyleIds($recommendationStyleIds)
	{
		$this->recommendationStyleIds=$recommendationStyleIds;
	}
}
