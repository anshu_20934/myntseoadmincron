<?php

class ConfirmationPageTracker
{
	var $cartData;
	var $paymentData;
	var $orderId;
	var $styleIdsWithOldSizeChartClicked;
	var $styleIdsWithNewSizeChartClicked;
	
	public function setCartData($cart) {
		$this->cartData = $cart;
	}
	
	public function setPaymentData($paymentData) {
		$this->paymentData = $paymentData;
	}
	
	public function setOrderId($orderId) {
		$this->orderId = $orderId;
	}
	
   /**
	* Kundan: now we track: out of the order items, which were the styles for which user clicked on their size old charts
	* We send this array of styles to Mongo DB
	*/
	public function setStyleIdsWithOldSizeChartClicked($styleArr){
		$this->styleIdsWithOldSizeChartClicked = $styleArr;
	}
  
   /**
	* Kundan: now we track: out of the order items, which were the styles for which user clicked on their new size charts
	* We send this array of styles to Mongo DB
	*/
	public function setStyleIdsWithNewSizeChartClicked($styleArr){
		$this->styleIdsWithNewSizeChartClicked = $styleArr;
	}
}
