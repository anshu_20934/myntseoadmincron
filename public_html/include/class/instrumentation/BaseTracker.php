<?php
//chdir(dirname(__FILE__));
#require_once "../../../auth.php";
global $xcart_dir;
include_once "$xcart_dir/include/class/instrumentation/SearchPageTracker.php";
include_once "$xcart_dir/include/class/instrumentation/PDPTracker.php";
include_once "$xcart_dir/include/class/instrumentation/RecommendationTracker.php";
include_once "$xcart_dir/include/class/instrumentation/TrafficTracker.php";
include_once "$xcart_dir/include/class/instrumentation/DeviceTracker.php";
include_once "$xcart_dir/include/class/instrumentation/ConfirmationPageTracker.php";
include_once "$xcart_dir/include/class/instrumentation/AddToCartTracker.php";
include_once "$xcart_dir/include/class/instrumentation/AddToSavedTracker.php";
include_once "$xcart_dir/include/func/func.utilities.php";
include_once "$xcart_dir/modules/RestAPI/RestRequest.php";
include_once "$xcart_dir/Cache/Cache.php";

class BaseTracker
{
	
	const HOME_PAGE = "home";
	const SEARCH_PAGE = "search";
	const AJAX_SEARCH = "ajaxSearch";
	const PDP = "pdp";
	const RECO_WIDGET = "recoWidget";
	const MINI_PIP = "miniPip";
	const CART_PAGE = "cart";
	const ADD_TO_CART = "addToCart";
	const ADD_TO_SAVED = "addToSaved";
	const PAYMENT_PAGE = "payment";
	const ORDER_INSERT = "orderInsert";
	const CONFIRMATION = "confirmation";
	const MYMYNTRA = "mymyntra";
	const MICRO_SESSION_COOKIE_NAME = "microsessid";
	const MICRO_SESSION_TIMEOUT = 1800;
	const LANDING_PAGE = "landingPage";
	const REGISTRATION = "register";
	const SIGNIN = "signin";
	const BENCHMARK_DATA = "bench";
    const WMS_REST_CALL_LATENCIES = "wms";
    const CACHE_CALL_LATENCIES = "redis";
	const CHECKOUT_PAGE = "checkout";
    const CHECKOUT_PAGE_CART_VIEW = "checkout_cart";
    const CHECKOUT_PAGE_ADDRESS_VIEW = "checkout_address";
	const CHECKOUT_PAGE_PAYMENT_VIEW = "checkout_payment";

	var $objData;
	
	var $startTime;
	var $endTime;
	var $startCompleteTime;
	var $endCompleteTime;
	
	var $page;
	
	var $searchData;
	var $recommendationData;
	var $pdpData;
	var $addToCartData;
	var $addToSavedData;
	var $confirmationData;
	var $loginData;

	
	public function __construct($page, $paramsArray = null)
	{
		$this->objData['page'] = $page;
		$this->startCompleteTimer();
		$this->startTimer();
		$this->setMicroSession();
		$this->setGenericCaptures();
		$this->populatePageSpecificData($paramsArray);
	}

	private function setGenericCaptures()
	{
		global $_SERVER, $XCARTSESSID, $XCART_SESSION_VARS, $_GET, $server_name,$CLIENT_IP,$UUID,$REMEMBER_USER,$REMEMBER_LOGIN;
		$this->objData['timestamp'] = $_SERVER['REQUEST_TIME'];
		$this->objData['cookieId'] = $XCARTSESSID; 
		$this->objData['clientIP'] = $CLIENT_IP;
		$this->objData['serverName'] = $server_name;//using this instead of $_SERVER['SERVER_NAME'] as the later will always be myntra.com
		$this->objData['login'] = $XCART_SESSION_VARS['login'];
		$this->objData['userAgent'] = $_SERVER['HTTP_USER_AGENT'];
		$this->objData['isRobot'] = ($XCART_SESSION_VARS['is_robot'] == 'Y') ? true : false;
		if(empty($UUID)){
			setUUIDAndUserCookie();
		}
		$this->objData['uuid'] = $UUID;
		$this->objData['rememberLogin'] = $REMEMBER_LOGIN;
		$this->objData['referer'] = parse_url($_SERVER['HTTP_REFERER']);
		//this is required as parseUrl host contains www.
		$this->objData['referer']['domain']  = str_ireplace('www.', '', $this->objData['referer']['host']);
		//this is truncated as this can be very very long
		$this->objData['referer']['query'] = substr($this->objData['referer']['query'], 0, 500);
		//of no importance
		unset($this->objData['referer']['scheme']);
		
		$this->objData['hostURL'] = $_SERVER['SERVER_NAME'] . substr($_SERVER['REQUEST_URI'], 0, 300);
		
		$trafficData = new TrafficTracker();
		if(!is_null($trafficData))
			$this->objData['trafficData'] = $trafficData;
		
		$deviceData = new DeviceTracker();
		if(!is_null($deviceData))
			$this->objData['deviceData'] = $deviceData;
		
	}
	
	private function setMicroSession(){
        global $BTHTTPCookie;
		$value = getMyntraCookie(self::MICRO_SESSION_COOKIE_NAME);		
		/**
		 * Previous logic that used to clear micro session cookie has been removed. This cookie will be refreshed every 30 mins now.
		 */
		if(!isset($value)){
			$value = rand(0,1000);
            if ($BTHTTPCookie)
                setMyntraCookie(self::MICRO_SESSION_COOKIE_NAME, $value, time() + self::MICRO_SESSION_TIMEOUT, "/",false,true);
            else
    			setMyntraCookie(self::MICRO_SESSION_COOKIE_NAME, $value, time() + self::MICRO_SESSION_TIMEOUT, "/"); // Add timout cookie
			$this->objData['microSessionID'] = $value; 
		}else{ // Refresh the timout cookie's expires time
            if ($BTHTTPCookie)
                  setMyntraCookie(self::MICRO_SESSION_COOKIE_NAME, $value, time() + self::MICRO_SESSION_TIMEOUT, "/",false,true);
            else
			      setMyntraCookie(self::MICRO_SESSION_COOKIE_NAME, $value, time() + self::MICRO_SESSION_TIMEOUT, "/");
			$this->objData['microSessionID'] = $value; 
		}
	}
	
	public function clearMicroSession(){
        global $BTHTTPCookie;
        if ($BTHTTPCookie)
            setMyntraCookie(self::MICRO_SESSION_COOKIE_NAME, 0, time() - 3600 , "/",false,true);
        else
      		setMyntraCookie(self::MICRO_SESSION_COOKIE_NAME, 0, time() - 3600 , "/");
	}
	
	private function startTimer()
	{
		$this->startTime = microtime(true);
	}
	
	private function endTimer()
	{
		$this->endTime = microtime(true);
		$this->objData['pageLoadTime'] = $this->endTime - $this->startTime; 
	}

	private function startCompleteTimer($startCompleteTime = null)
	{
		if(!is_null($startCompleteTime))
			$this->startCompleteTime = $startCompleteTime;
		else{
			global $authStartTime;
			if(!is_null($authStartTime))
				$this->startCompleteTime = $authStartTime;
			else
				$this->startCompleteTime = microtime(true);
		}
	}
	
	private function endCompleteTimer($endCompleteTime = null)
	{
		if(!is_null($endCompleteTime))
			$this->endCompleteTime = $endCompleteTime;
		else
			$this->endCompleteTime = microtime(true);

		$this->objData['completeLoadTime'] = $this->endCompleteTime - $this->startCompleteTime; 
	}
	
	public function populatePageSpecificData($paramsArray)
	{
		switch($this->objData['page'])
		{
			case self::SEARCH_PAGE :
				$this->searchData = new SearchPageTracker(null);
				$this->objData['searchData'] = $this->searchData;
                $this->seoData = new StdClass;
				$this->objData['seoData'] = $this->seoData;
				break;
			case self::AJAX_SEARCH :
				$this->searchData = new SearchPageTracker(null);
				$this->objData['searchData'] = $this->searchData;
				break;	
			case self::PDP :
				$this->pdpData = new PDPTracker(null);
				$this->objData['pdpData'] = $this->pdpData;
                $this->seoData = new StdClass;
				$this->objData['seoData'] = $this->seoData;
				break;
			case self::RECO_WIDGET :
				$this->recommendationData = new RecommendationTracker(null);
				$this->objData['recommendationData'] = $this->recommendationData;
				break;
			case self::MINI_PIP :
				$this->pdpData = new PDPTracker(null);
				$this->objData['pdpData'] = $this->pdpData;
				break;
			case self::CONFIRMATION :
				$this->confirmationData = new ConfirmationPageTracker();
				$this->objData['orderData'] = $this->confirmationData;
				break;
			case self::ADD_TO_CART :
				$this->addToCartData = new AddToCartTracker($paramsArray);
				$this->objData['addToCartData'] = $this->addToCartData;
				break;
			case self::ADD_TO_SAVED :
				$this->addToSavedData = new AddToSavedTracker($paramsArray);
				$this->objData['addToSavedData'] = $this->addToSavedData;
				break;
			case self::SIGNIN:
				$this->loginData = new instrumentation\LoginPTracker($paramsArray);
				$this->objData['signin'] = $this->loginData;
			default :
				//$this->objData['page'] = self::HOME_PAGE;
				break;
			
		}
	}
	
	public function fireRequest()
	{	
		$this->endTimer();
		$toFire = FeatureGateKeyValuePairs::getBoolean("fireTrackingRequest");
		if($toFire !== true)
			return;
		ShutdownHook::addHook(array(&$this,"fireRequestOnShutdown"),ShutdownHook::PRECEDENCE_4);
	}

	public function fireRequestOnShutdown()
	{
		global $tracking_system_url;
		global $weblog, $configMode;
		global $_MABTestObject;
		global $bench_counts;
		//Adding AB Test tracking data
                if(!empty($_MABTestObject)) {
                    $this->objData['mabTestData']= $_MABTestObject->getTrackingData();
                }
		$this->objData[self::BENCHMARK_DATA] = $bench_counts;
                $this->objData[self::WMS_REST_CALL_LATENCIES] = RestRequest::getLatencies();
                $this->objData[self::CACHE_CALL_LATENCIES] = Cache::getLatencies();
		$this->endCompleteTimer();
		$json = json_encode($this->objData);
        $weblog->info('BaseTrackerData: ' . $json);

		$base64Str = base64_encode($json);
		//Making it url safe
		$base64Str = strtr($base64Str, "+/", "-_");
		fireAsync($tracking_system_url,"data?"  . $base64Str . "#" .$configMode);
	}
}
