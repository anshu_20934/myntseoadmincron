<?php
##########################################################################
## Author	: Shantanu Bhadoria											##
## Date		: 14 Oct'2008												##
## Copyright Myntra Designs												##
## 																		##
## Myntra createorder class												##
## This class contains all the functions associated with creation of an ##
## order. Scroll Down for errorcodes list.								##
##########################################################################

require_once 'class.singleton.php';
require_once 'class.ordertime.php';
require_once 'class.myntra_constants.php';
require_once 'class.mail.php';

class createorder {
	function func_createorder($input_ds){
		$sql_tbl = myntra_constants::$sql_tbl;
		$db = singleton::getmysqlinstance();

		createorder::_func_lock('createorder');
		$return_value[errorcode] = 0;
		$return_value[logs] = '';
					
		
		# verify ordername against source etc.	
		if(
				(
					($input_ds[sync_credentials][source_id] != $input_ds[tables][orders][source_id]) // not a valid remote order
					|| (!(preg_match('/^'.$input_ds[sync_credentials][source_code].'\-[0-9]*/',$input_ds[tables][orders][order_name])))
				)
				&&
				(
					($input_ds[tables][orders][order_name] != '') //not a local order either
					|| ($input_ds[tables][orders][source_id] != '')
					|| (isset($input_ds[sync_credentials][login])) # there has to be a login if it was authenticated remotely :)
				)
			){
			$return_value[logs] .= "\nCredentials authenticated but they dont match order information provided";
			$return_value[errorcode] = 1;
			return $return_value;
		}

		

	//Creating customer
		$cust_output = createorder::_create_customer( $input_ds[tables][customers]);	
		if($cust_output[errorcode] == 0){
			$return_value[logs] .= "\n".$cust_output[logs];
			$return_value[errorcode] = 8;
			return $return_value;
		}
		else {
			$return_value[logs] .= "\n".$cust_output[logs];
		}

	//Creating product
		$prod_output = createorder::_create_product( $input_ds[tables]);
		if($prod_output[errorcode] == 0){
			$return_value[logs] .= "\n".$prod_output[logs];
			$return_value[errorcode] = 9;
			return $return_value;
		}
		else {
			$return_value[logs] .= "\n".$prod_output[logs];
		} 
	$input_ds[tables][orders][queueddate] = time();
	//Creating order
		$order_output = createorder::_create_order( $input_ds);
		if($order_output[errorcode]){
			$return_value[logs] .= "\n".$order_output[logs];
			$return_value[logs] .= "\n An error was encountered while creating the order";
			$return_value[errorcode] = $order_output[errorcode];
			$return_value[recovery_mode] = $order_output[recovery_mode];
			return $return_value;
		}
		else {
			$return_value[logs] .= "\n".$order_output[logs];
			$input_ds[tables][orders][orderid] = $order_output[orderid];
			$return_value[logs] .= "\norder id assignment into inpurt_ds[] array :: input_ds[tables][orders][orderid]=>{$input_ds[tables][orders][orderid]} :: @line ".__LINE__. " :: in file ".__FILE__;
		}
		
		//calculating designer commission for queued order
		$commision_output = createorder::_get_commission($input_ds['tables']);
		if($commision_output['errorcode']){
			$return_value['logs'] .= "\n".$commision_output['logs'];
			$return_value['logs'] .= "\n An error was encountered while calculating the designer commission";
			$return_value['errorcode'] = $commision_output['errorcode'];
			$return_value['recovery_mode'] = $commision_output['recovery_mode'];
			return $return_value;
		}
		else {
			$return_value['logs'] .= "\n".$commision_output['logs'];
		}

		$return_value['errorcode'] = 0;
		return $return_value;
	}

	# This function creates the customer for the order if he doesn't exist. params ($sql_tbl sql tables definition array. 
	# return errorcode values
	# 0 : error executing sql
	# 1 : successfully created customer
	# 2 : customer exists
	function _create_customer( $customers){	//Customer Creation Stage
		$sql_tbl = myntra_constants::$sql_tbl;
		$db = singleton::getmysqlinstance();
		$query = "SELECT * FROM {$sql_tbl[customers]} WHERE login = '".$db->escape($customers[login])."'";
		//if(!$db->query($query)){
		if(!func_query($query)) {
			$tb_columns = array('login','usertype','password','password_hint','password_hint_answer','about_me','interests','mylinks',
				'b_title','b_firstname','b_lastname','b_address','b_city','b_county','b_state','b_country','b_zipcode',
				's_title','s_firstname','s_lastname','s_address','s_city','s_county','s_state','s_country','s_zipcode',
				'title','firstname','lastname','company','email','phone','mobile','fax','url','card_name','card_type','card_number','card_expire',
				'card_cvv2','last_login','first_login','status','referer','ssn','language','cart','change_password','parent','pending_plan_id',
				'activity','membershipid','pending_membershipid','tax_number','tax_exempt','myimage','termcondition','ship2diff','addressoption',
				'offline','dailydesign');
			$q_strings = createorder::_create_key_value_strings($tb_columns,$customers);
			$query = "INSERT INTO {$sql_tbl[customers]}({$q_strings[keys]}) VALUES({$q_strings[values]})";
			//if(!$db->query($query)){
			if(!func_query($query)) {
				$return_value[logs] = "Query Failed to excute $query";
				$return_value[errorcode] = 0;
				return $return_value;
			}
			$return_value[logs] = "Query executed to create customer : $query";
			$return_value[errorcode] = 1;
			return $return_value;
			}
		$return_value[logs] = "Customer exists so we are good";
		$return_value[errorcode] = 2;
		return $return_value;
	}

	# This function creates a product detail in all the product tables
	# return errorcode values
	# 0 : failure
	# 1 : success
	function _create_product( $prod_tables){	//Product Creation Stage
		$sql_tbl = myntra_constants::$sql_tbl;
		$db = singleton::getmysqlinstance();
		
		//products table
		$tb_columns = array('productid','productcode','product','provider','distribution','weight','list_price','descr','fulldescr','reject_reason','avail','rating','forsale','add_date','views_stats',
			'sales_stats','del_stats','shipping_freight','free_shipping','discount_avail','min_amount','dim_x','dim_y','dim_z','low_avail_limit','free_tax','product_type','manufacturerid','return_time',
			'keywords','designer','is_customizable','expiration_date','statusid','modified_date','product_type_id','product_style_id','contestid','image_portal_t','is_publish','myntra_rating',
			'image_portal_thumbnail_160');
		foreach($prod_tables[products] as $temp_table_array){
			$q_strings = createorder::_create_key_value_strings($tb_columns,$temp_table_array);
			$query = "INSERT INTO {$sql_tbl[products]}({$q_strings[keys]}) SELECT {$q_strings[values]} FROM DUAL WHERE NOT EXISTS(SELECT * FROM {$sql_tbl[products]} WHERE productid='{$temp_table_array[productid]}')";
			$db->query($query);
			if($db->last_error){
				$return_value[logs] = "\nQuery failed to execute $query";
				$return_value[logs] .= "\n Query Failed with error : ".$db->last_error;
				$return_value[errorcode] = 0;
				return $return_value;
			}
			$return_value[logs] = "\nQuery executed $query";
		}

		//mk_xcart_product_customized_area_image_rel table
		$tb_columns = array('product_id','style_id','area_id','area_image_final','templateid');
		foreach($prod_tables[mk_xcart_product_customized_area_image_rel] as $temp_table_array){
			$q_strings = createorder::_create_key_value_strings($tb_columns,$temp_table_array);
			$query = "INSERT INTO {$sql_tbl[mk_xcart_product_customized_area_image_rel]}({$q_strings[keys]}) SELECT {$q_strings[values]} FROM DUAL WHERE NOT EXISTS(SELECT * FROM {$sql_tbl[mk_xcart_product_customized_area_image_rel]} WHERE product_id='{$temp_table_array[product_id]}' AND style_id='{$temp_table_array[style_id]}' AND area_id={$temp_table_array[area_id]})";
			$db->query($query);
			if($db->last_error){
				$return_value[logs] .= "\nQuery failed to execute $query";
				$return_value[logs] .= "\n Query Failed with error : ".$db->last_error;
				$return_value[errorcode] = 0;
				return $return_value;
			}
			$return_value[logs] .= "\nQuery executed $query";
		}

		//mk_xcart_product_customized_area_rel table
		$tb_columns = array('product_id','style_id','area_id','orientation_id','image_name','image_x','image_y','image_width','image_height','text','text_x','text_y','text_width','text_height',
			'text_format','orientation_final_image_display','popup_image_path','bgcolor','thumb_image','product_cust_th_img','templateid');
		foreach($prod_tables[mk_xcart_product_customized_area_rel] as $temp_table_array){
			$q_strings = createorder::_create_key_value_strings($tb_columns,$temp_table_array);
			$query = "INSERT INTO {$sql_tbl[mk_xcart_product_customized_area_rel]}({$q_strings[keys]}) SELECT {$q_strings[values]} FROM DUAL WHERE NOT EXISTS(SELECT * FROM {$sql_tbl[mk_xcart_product_customized_area_rel]} WHERE product_id='{$temp_table_array[product_id]}' AND style_id='{$temp_table_array[style_id]}' AND area_id='{$temp_table_array[area_id]}' AND orientation_id='{$temp_table_array[orientation_id]}')";
			$db->query($query);
			if($db->last_error){
				$return_value[logs] = "\nQuery failed to execute $query";
				$return_value[logs] .= "\n Query Failed with error : ".$db->last_error;
				$return_value[errorcode] = 0;
				return $return_value;
			}
			$return_value[logs] .= "\nQuery executed $query";
		}

		$return_value[logs] .= "\nProduct successfully created";
		$return_value[errorcode] = 1;
		return $return_value;

	}

	# Internal function to create an order
	function _create_order($input_ds){		//Order creation Stage
		$sql_tbl = myntra_constants::$sql_tbl;
		$db = singleton::getmysqlinstance();
		if(!isset($input_ds[tables][orders][order_name])){
			$input_ds[tables][orders][order_name] = 'undef';
			$input_ds[tables][orders][source_id] = 'undef';
		}
		$query = "SELECT orderid FROM {$sql_tbl[orders]} WHERE order_name = '".$db->escape($input_ds[tables][orders][order_name])."'";
		$query_result = $db->get_var($query);
		if ($query_result){
			$orderid = $query_result;
			$return_value[orderid] = $orderid;
			$return_value[logs] = "\norder id assignment :: return_value[orderid]=>{$return_value[orderid]} :: @line ".__LINE__. " :: in file ".__FILE__;
			$recovery_mode = 1;
			$return_value[recovery_mode] = 1;
			$return_value[logs] .= "\nOrder exists retrieved orderid $orderid . . . entering recovery mode";
		}
		else {
			$recovery_mode = 0;
			$return_value[recovery_mode] = 0;

			$log_values = array('b_firstname','b_lastname','b_address','b_country','b_city','b_state','b_zipcode',
								's_firstname','s_lastname','s_address','s_country','s_city','s_state','s_zipcode',
								'mobile','updatebill','updateship','billtoship','deliverymethod','totalamount','shippingrate');
			$text = '';
			foreach($log_values as $key=>$value){
				$text .= "{$value}=>".$db->escape($input_ds[tables][orders][$value])."&";
			}			
			$query = "INSERT INTO mk_temp_order(sessionid, parameter) values('".uniqid($input_ds[sync_credentials][source_code])."','$text')";
				$return_value[logs] .= "\ntemp orders table query : $query";
			if(!$db->query($query)){
				$return_value[logs] .= "\nfailed to insert stuff into db temp tables, quitting now";
				$return_value[errorcode] = 2;
				return $return_value;
			}
			$orderid = $db->insert_id;	
			$return_value[orderid] = $orderid;
			$return_value[logs] .= "\norder id assignment :: return_value[orderid]=>{$return_value[orderid]} :: @line ".__LINE__. " :: in file ".__FILE__;
			// Inserting Ordername and source_id for locally created orders
			if($input_ds[tables][orders][order_name] == 'undef'){
				$input_ds[tables][orders][order_name] = $input_ds[sync_credentials][source_code].'-'.$orderid;
				$input_ds[tables][orders][source_id] = $input_ds[sync_credentials][source_id];
			}

			// orders table
			$temp_table_array = $input_ds[tables][orders];
			$temp_table_array[orderid] = $orderid;		# Replacing the original orderid with local auto incremented orderid from temp table
			$tb_columns = array( 'orderid','invoiceid','login','membership','total','giftcert_discount','giftcert_ids','subtotal', 'discount','coupon',
							'coupon_discount','shippingid','tracking','shipping_cost','tax','taxes_applied','date','status','payment_method',
							'flag','details','customer_notes','notes','extra','customer','title','firstname','lastname','company',
							'b_title','b_firstname','b_lastname','b_address','b_city','b_state','b_country','b_zipcode',
							's_title','s_firstname','s_lastname','s_address','s_city','s_state','s_country','s_zipcode',
							'phone','fax','url','email','paymentid', 'shipping_method','qtyInOrder','mobile','gift_status','gift_pack','ref_discount', 
							'gift_charges','affiliateid','cod','queueddate','shopid','guid','aff_discount','aff_orderid','orgid','issues_contact_number',
							'shipment_preferences','additional_info','order_name','source_id' );
			$keys = '';
			$values = '';
			foreach($tb_columns as $value){
				$keys .= "{$value}, ";
				$values .= "'".$db->escape($temp_table_array[$value])."', ";
			}
			$keys = trim($keys,', ');
			$values = trim($values,', ');
			$query = "INSERT INTO {$sql_tbl[orders]}({$keys}) VALUES({$values})";
			if(!$db->query($query)){
				$return_value[logs] .= "\nFailed to insert into orders, quitting now";
				$return_value[errorcode] = 3;
				return $return_value;
			}
			$return_value[logs] .= "\norders table query : $query";
		}
			
			
		// order_details table
		$tb_columns = array('orderid','productid','price','amount','provider','product','product_style','product_type','discount',
							'quantity_breakup','location','item_status','tax_rate','taxamount','shipping_amount','shipping_tax',
							'total_amount','tpl_used_id','promotion_id','style_template'
						);
		foreach($input_ds[tables][order_details] as $temp_table_array){		// Prepare queries for each row of order_details table
			$temp_table_array[orderid]=$orderid;
			$temp_table_array[item_status]='UA';   // added to force unassigned item status on any new orders being created. 
			$keys = '';
			$values = '';
			foreach($tb_columns as $value){
				$keys .= "{$value}, ";
				$values .= "'".$db->escape($temp_table_array[$value])."', ";
			}
			$keys = trim($keys,', ');
			$values = trim($values,', ');

			$query = "SELECT itemid FROM {$sql_tbl[order_details]} WHERE orderid='{$orderid}' and productid='{$temp_table_array[productid]}'"; 
			if($recovery_mode && $db->get_var($query)){
				$item_id_mapping[$temp_table_array[itemid]] = $db->get_var();		//when query is not provided get_var gets cached result 
				$return_value[item_id_mapping][$temp_table_array[itemid]] = $db->get_var();
				$return_value[logs] .= "\nOrder details recover query : $query";

			}
			else {
				$query = "INSERT INTO {$sql_tbl[order_details]}({$keys}) VALUES({$values})";
				$db->query($query );
				if($db->last_error){
					$return_value[logs] .= "\nFailed to insert into order details,$query\nquitting now ";
					$return_value[errorcode] = 4;
					return $return_value;
				}
				$item_id_mapping[$temp_table_array[itemid]] = $db->insert_id;
				$return_value[item_id_mapping][$temp_table_array[itemid]] = $db->insert_id;
				$return_value[logs] .= "\nOrder details table query : $query";
			}
		}
		
					
		// mk_product_options_order_details_rel table
		$tb_columns = array('orderid','option_id','itemid');
		foreach($input_ds[tables][mk_product_options_order_details_rel] as $temp_table_array){
			$temp_table_array[orderid] = $orderid;
			$keys = '';
			$values = '';
			foreach($tb_columns as $value){
				$keys .= "{$value}, ";
				$values .= "'".$db->escape($temp_table_array[$value])."', ";
			}
			$keys = trim($keys,', ');
			$values = trim($values,', ');
			$query = "INSERT INTO {$sql_tbl[mk_product_options_order_details_rel]}({$keys}) SELECT {$values} FROM DUAL WHERE NOT EXISTS(SELECT * FROM {$sql_tbl[mk_product_options_order_details_rel]} WHERE orderid='{$temp_table_array[orderid]}' AND option_id='{$temp_table_array[option_id]}' AND itemid='{$temp_table_array[itemid]}')";
			$db->query($query );
			if($db->last_error){
				$return_value[logs] .= "\nFailed to insert into mk_product_options_order_details_rel : $query \nquitting now";
				$return_value[errorcode] = 5;
				return $return_value;
			}
			$return_value[logs] .= "\nmk_product_options_order_details_rel table query : $query";
		}
			
		// mk_xcart_order_details_customization_rel table
		$tb_columns = array('itemid','orientation_id','image','orderid','style_id','area_id','image_x','image_y','image_width',
							'image_height','text','text_x','text_y','text_width','text_height','text_format',
							'orientation_final_image_display','orientation_final_image_print','bgcolor','templateid');
		foreach($input_ds[tables][mk_xcart_order_details_customization_rel] as $temp_table_array){
			$temp_table_array[orderid] = $orderid;
			$keys = '';
			$values = '';
			foreach($tb_columns as $value){
				$keys .= "{$value}, ";
				$values .= "'".$db->escape($temp_table_array[$value])."', ";
			}
			$keys = trim($keys,', ');
			$values = trim($values,', ');
			$query = "INSERT INTO {$sql_tbl[mk_xcart_order_details_customization_rel]}({$keys}) SELECT {$values} FROM DUAL WHERE NOT EXISTS(SELECT * FROM {$sql_tbl[mk_xcart_order_details_customization_rel]} WHERE itemid='{$temp_table_array[itemid]}' AND orientation_id='{$temp_table_array[orientation_id]}' AND orderid='{$temp_table_array[orderid]}')";
			$db->query($query );
			if($db->last_error){
				$return_value[logs] .= "\nFailed to insert into mk_xcart_order_details_customization_rel : $query \nquitting now";
				$return_value[errorcode] = 6;
				return $return_value;
			}
			$order_details_customization_id_mapping[$temp_table_array[id]] = $db->insert_id;
			$return_value[order_details_customization_id_mapping][$temp_table_array[id]] = $db->insert_id;
			$return_value[logs] .= "\nmk_xcart_order_details_customization_rel table query : $query";
		}

		// mk_xcart_order_customized_area_image_rel table
		$tb_columns = array('orderid','item_id','style_id','area_id','area_image_final','templateid');
		foreach($input_ds[tables][mk_xcart_order_customized_area_image_rel] as $temp_table_array){
			$temp_table_array[orderid] = $orderid;
			$temp_table_array[id] = $order_details_customization_id_mapping[$temp_table_array[id]];
			$keys = '';
			$values = '';
			foreach($tb_columns as $value){
				$keys .= "{$value}, ";
				$values .= "'".$db->escape($temp_table_array[$value])."', ";
			}
			$keys = trim($keys,', ');
			$values = trim($values,', ');
			$query = "INSERT INTO {$sql_tbl[mk_xcart_order_customized_area_image_rel]}({$keys}) SELECT {$values} FROM DUAL WHERE NOT EXISTS(SELECT * FROM {$sql_tbl[mk_xcart_order_customized_area_image_rel]} WHERE orderid='{$temp_table_array[orderid]}' AND item_id='{$temp_table_array[item_id]}' AND style_id='{$temp_table_array[style_id]}' AND area_id='{$temp_table_array[area_id]}')";
			$db->query($query );
			if($db->last_error){
				$return_value[logs] .= "\nFailed to insert into mk_xcart_order_customized_area_image_rel,quitting now";
				$return_value[errorcode] = 7;
				return $return_value;
			}
			$return_value[logs] .= "\nmk_xcart_order_customized_area_image_rel table query : $query";
		}
		$return_value[orderid] = $orderid;
		$return_value[logs] .= "\norder id assignment :: return_value[orderid]=>{$return_value[orderid]} :: @line ".__LINE__. " :: in file ".__FILE__;

		# Setting Estimated Processing times for the order items
		$order_time_data = ordertime::get_order_time_data($orderid);
		if($order_time_data['SHIPPED_TOGETHER'] == 1){
			/*
			##calculate order processing time 
			##based on the day of ship at 7pm
			*/
			$processing_time = ordertime::get_process_time_7pm($order_time_data['DATA']['PROCESSING_DATE']);//$val['processing_date'] = date on which order can be PROCESSED
			$query = 'update '.$sql_tbl['order_details'].' set  estimated_processing_date ='.$processing_time.' where orderid ='.$orderid;
			$db->query($query );
			if($db->last_error){
				$return_value[logs] .= "\nFailed to update processing time into xcart_order_details,quitting now";
				$return_value[errorcode] = 7;
				return $return_value;
			}
		}
		else{
			foreach($order_time_data['DATA'] as $key=>$val){
				/*
				##calculate order processing time 
				##based on the day of ship at 7pm
				*/
				$processing_time = ordertime::get_process_time_7pm($val['PRODUCT_PROCESSING_DATE']);//$val['processing_date'] = date on which order can be PROCESSED
				$processing_time_sql = 'update '.$sql_tbl['order_details'].' set  estimated_processing_date ='.$processing_time.' where itemid = '.$val['PRODUCT_ITEMID'].' and orderid ='.$orderid;
				$db->query($query );
				if($db->last_error){
					$return_value[logs] .= "\nFailed to update processing time into xcart_order_details,quitting now";
					$return_value[errorcode] = 7;
					return $return_value;
				}						
			}
		}

	
		return $return_value;
	}

	/*
	## This function calculates the designer commission for
	## each published product in queued order
	## return errorcode values
	## 0 : failure
	## 1 : success
	*/
	function _update_item_processing_time($tables){
		$sql_tbl = myntra_constants::$sql_tbl;
		$db = singleton::getmysqlinstance();

	}

	/*
	## This function calculates the designer commission for
	## each published product in queued order
	## return errorcode values
	## 0 : failure
	## 1 : success
	*/
	function _get_commission($tables){
		$sql_tbl = myntra_constants::$sql_tbl;
		$db = singleton::getmysqlinstance();
		##order and items detail
		$orderid = $tables['orders']['orderid'];
		$order_status = $tables['orders']['status'];
		$order_amount = $tables['orders']['total'];
		$order_details = $tables['order_details'];
		
		$return_value[logs] = "\nbefor calculating commission checking for :: orderid=>{$orderid},status=>{$order_status},amount=>{$order_amount} :: @line ".__LINE__. " :: in file ".__FILE__;
		
		##if order queued and order_total_amt >0 caculate commission
		if($order_amount <= 0 && $order_status != 'Q'){
			$return_value[logs] .= "\ncondition for designer commission is failed for one among the following :: amount=>{$order_amount},status=>{$order_status} :: @line ".__LINE__. " :: in file ".__FILE__;

		}else{
			/*
			##check for designer commission 
			##already given for this order
			*/
			$query = "select 
							comm_generated_status 
					from
							{$sql_tbl['comm_track']}
					where
							orderid = {$orderid}
					AND
							comm_generated_for = 'DESIGNER' 
					AND
							comm_generated_status = 'Y'";
			$db->query($query);
			if($db->num_rows > 0){
				//terminate since for the order enclosed products, commission is given to relevant designer
				$return_value[logs] .= "\ncondition for designer commission is failed for commission tracking :: for orderid=>{$orderid}, there is an entry in table mk_comm_generated_tracking as 'DESIGNER' and 'Y' :: @line ".__LINE__. " :: in file ".__FILE__;
			}else{
				/*
				##for each item in order calculate 
				##designer commission according to the product
				##and send mail to the dsigner
				*/
				foreach($order_details as $val){
					$item_product_id = $val['productid'];
					$item_product_style_id = $val['product_style'];
					$item_product_price = $val['price'];	
					$item_product_qty = $val['amount'];
					
					$query = "SELECT
									c.email,
									c.firstname, 
									p.is_publish,
									p.product,
									p.designer,
									t.name as product_type_name									
							FROM 
									{$sql_tbl['customers']} as c
									
							INNER JOIN
									{$sql_tbl['products']} as p
							on
									c.login = p.provider
									
							INNER JOIN
									{$sql_tbl['types']} as t
							on
									p.product_type_id = t.id									
							WHERE
									p.productid = {$item_product_id}";
					
					$product_designer_info = $db->get_row($query);
					
					if($db->last_error){
						$return_value[logs] .= "\nQuery failed to execute :: {$query}";
						$return_value[logs] .= "\n Query Failed with error :: {$db->last_error}";
						$return_value[errorcode] = 12;					
					}
					
					##calculate commission only if the product is published
					if($product_designer_info->is_publish == 1){//if buyer is designer condition is not required
						##details to calculate commission
						$product_detail = array(
												'PRODUCTID' => $item_product_id,
												'PRICE' => $item_product_price,
												'QTY'	=> $item_product_qty,
												'STYLEID'=> $item_product_style_id
											);
						$designer_id = $product_designer_info->designer;
						
						$return_value[logs] .= "\nall condition are met for commission and following function is called :: createorder::_calculate_commission(\$orderid,\$designer_id,\$product_detail) :: @line ".__LINE__. " :: in file ".__FILE__;
						
						$calculate_commission_output = createorder::_calculate_commission($orderid,$designer_id,$product_detail);//calculate commission
						
						if($calculate_commission_output['errorcode']){
							$return_value['logs'] .= "\n".$calculate_commission_output['logs'];
							$return_value['logs'] .= "\nall condition are met for commission and following function encountered a problem :: createorder::_calculate_commission(,\$orderid,\$designer_id,\$product_detail) :: @line ".__LINE__. " :: in file ".__FILE__;
							$return_value['errorcode'] = $calculate_commission_output['errorcode'];
							return $return_value;
							
						}
						$return_value['logs'] .= "\n".$calculate_commission_output['logs'];
					
						##dsigner and product detail to send mail
						$designer_first_name = $product_designer_info->firstname;
						$designer_email = $product_designer_info->email;
						$product_name = $product_designer_info->product;
						$product_type = $product_designer_info->product_type_name;
						$mail_template = "boughtyourdesign";												
						$product_page_URL = "http://www.myntra.com/".str_replace(" ","-",$product_name)."/".$product_type."/FC/PD/".$item_product_id;
						
						$args = array(
									"FIRST_NAME" => $designer_first_name,
									"YOUR_DESIGN" => $product_page_URL
								);
						
						$return_value[logs] .= "\ncommission is calculated and mail function is called :: mail::sendMessage($mail_template,$args,$designer_email) :: @line ".__LINE__. " :: in file ".__FILE__;
														
						mail::sendMessage($mail_template,$args,$designer_email);
						
						$return_value[logs] .= "\ncommission is calculated and mail sent successfully to :: designer=>{$designer_email} :: @line ".__LINE__. " :: in file ".__FILE__;
						
					}else{
						$return_value[logs] .= "\ncommission is not calculated for :: product_id=>{$item_product_id} in orderid=>{$orderid} :: following condition is not satisfied :: is_publish=>{$product_designer_info->is_publish} :: @line ".__LINE__. " :: in file ".__FILE__;
						continue;
					}
				}//foreach
				
				##update commission given to the designer for the order
				$query = "INSERT INTO {$sql_tbl['comm_track']}(orderid,comm_generated_for,comm_generated_status) VALUES('{$orderid}','DESIGNER','Y')";
				$db->query($query);
				
				if($db->last_error){
					$return_value[logs] .= "\nQuery failed to execute :: {$query}";
					$return_value[logs] .= "\n Query Failed with error :: {$db->last_error}";
					$return_value[errorcode] = 10;
					return $return_value;
				}else{
					$return_value[logs] .= "\nQuery executed $query";
				}
			}//num_rows=0
		}//order==Q and amt>0		
		return $return_value;
	}
	
	/*
	## This function calculates commission
	## for the designer for his product
	## and commission payment date
	*/
	function _calculate_commission($orderid,$designerid,$product_detail){
		$sql_tbl = myntra_constants::$sql_tbl;
		$db = singleton::getmysqlinstance();
		
		$return_value['logs'] = 'No Commission Applied';
		return $return_value;
	}
	

	# This function constructs a string of keys and values from the input
	function _create_key_value_strings($tb_columns, $table_array){
		$db = singleton::getmysqlinstance();
		$return_value[keys] = '';
		$return_value[values] = '';
		foreach($tb_columns as $value){
			$return_value[keys] .= "{$value}, ";
			$return_value[values] .= "'".$db->escape($table_array[$value])."', ";
		}
		$return_value[keys] = trim($return_value[keys],', ');
		$return_value[values] = trim($return_value[values],', ');
		return $return_value;		
	}

	# this function checks the input for validity of all field tables e.g. enumerations integers etc.
	function _check_input_fields($input_ds){
	}
	
	# This Function(incomplete) acquires a write ex lock on a file and if lock is not immidiately acquired waits until the lock is available
	# This allows batch processing of all functions that use this lock.
	function _func_lock($function_name){
		return 1;
	}
}
# Error Codes :
# 0 : No Error, perfect, All went well (Atleast that's what we think!!)
# 1 : Order source information in sync credentials and actual order data does not match. Someone is trying to screw with us.
# 2 : Failed to insert in mk_temp_order
# 3 : Failed to insert in orders
# 4 : Failed to insert in order_details
# 5 : Failed to insert in mk_product_options_order_details_rel
# 6 : Failed to insert in mk_xcart_order_details_customization_rel
# 7 : Failed to insert in mk_xcart_order_customized_area_image_rel
# 8 : Failed to create customer
# 9 : Failed to create product
# 10: Failed to insert in mk_comm_generating_track for particular order, after paying commission.
# 11: Failed to insert in mk_designer_commission after calculating commission for a designer.
# 12: Failed to select designer and product detail
# 13: failed to select commission rate
# 14: Failed to select discount on product
?>
