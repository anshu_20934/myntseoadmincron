<?php
$match_fail_message = array(
	"AN"=>"Please enter only alphanumeric! eg:'test2','123','abc' etc..",
	"AN_"=>"Please enter only alphanumeric (underscore '_' is allowed)! eg:'test2_','12_3','abc' etc..",
	"AN_S"=>"Please enter only alphanumeric, starting letter should not be a space! eg:'0test2_','12_3','_ab c' etc..",
	"AN_S(25)"=>"Please enter only alphanumeric, starting letter should not be a space! eg:'0test2_','12_3','_ab c' etc..",
	"AN-_S(30)"=>"Please enter only alphanumeric, starting letter should not be a space! eg:'name', 'fname-lname', '0test2_','12_3','_ab c' etc..",
	"EMAIL"=>"Please enter valid email ID"	
	);

$regex_matches = array(
	"AN"=>"/^[A-Z0-9]+$/i",//only alphanumerics without spaces and leading 0 is accepted
	"AN_"=>"/^[\w]+$/i",//only alphanumerics without spaces and leading 0,_ is accepted
	"AN_S"=>"/^[\w]+[\w\s]*$/i",//only alphanumerics with spaces inbetween and leading 0,_ is accepted
	"AN_S(25)"=>"/^[\w]{1}[\w\s]{0,24}$/i",//AN_S characteristics for 25 letters, min=1 letter,max=25
	"AN-_S(30)"=>"/^[\w]{1}[\w\s-]{0,29}$/i",//AN-_S characteristics for 30 letters,hyphen (-) is allowed min=1 letter,max=30
	"EMAIL"=>"/^[_a-zA-Z0-9-]+(\.[_%a-zA-Z0-9-]+)*@[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*(\.[A-Za-z]{2,3})$/"
	);

function regex_validate($test_string,$validation_type){
	global $regex_matches;
	if(preg_match($regex_matches[$validation_type],$test_string)){
		return true;
	}else{
		return false;
	}
}
?>