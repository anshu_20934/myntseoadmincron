<?php
/*****************************************************************************\
+-----------------------------------------------------------------------------+
| X-Cart                                                                      |
| Copyright (c) 2001-2006 Ruslan R. Fazliev <rrf@rrf.ru>                      |
| All rights reserved.                                                        |
+-----------------------------------------------------------------------------+
| PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
| FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
| AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
|                                                                             |
| THIS  AGREEMENT  EXPRESSES  THE  TERMS  AND CONDITIONS ON WHICH YOU MAY USE |
| THIS SOFTWARE   PROGRAM   AND  ASSOCIATED  DOCUMENTATION   THAT  RUSLAN  R. |
| FAZLIEV (hereinafter  referred to as "THE AUTHOR") IS FURNISHING  OR MAKING |
| AVAILABLE TO YOU WITH  THIS  AGREEMENT  (COLLECTIVELY,  THE  "SOFTWARE").   |
| PLEASE   REVIEW   THE  TERMS  AND   CONDITIONS  OF  THIS  LICENSE AGREEMENT |
| CAREFULLY   BEFORE   INSTALLING   OR  USING  THE  SOFTWARE.  BY INSTALLING, |
| COPYING   OR   OTHERWISE   USING   THE   SOFTWARE,  YOU  AND  YOUR  COMPANY |
| (COLLECTIVELY,  "YOU")  ARE  ACCEPTING	AND AGREEING  TO  THE TERMS OF THIS |
| LICENSE   AGREEMENT.   IF  YOU    ARE  NOT  WILLING   TO  BE  BOUND BY THIS |
| AGREEMENT, DO  NOT INSTALL OR USE THE SOFTWARE.  VARIOUS   COPYRIGHTS   AND |
| OTHER   INTELLECTUAL   PROPERTY   RIGHTS    PROTECT   THE   SOFTWARE.  THIS |
| AGREEMENT IS A LICENSE AGREEMENT THAT GIVES  YOU  LIMITED  RIGHTS   TO  USE |
| THE  SOFTWARE   AND  NOT  AN  AGREEMENT  FOR SALE OR FOR  TRANSFER OF TITLE.|
| THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY GRANTED BY THIS AGREEMENT.      |
|                                                                             |
| The Initial Developer of the Original Code is Ruslan R. Fazliev             |
| Portions created by Ruslan R. Fazliev are Copyright (C) 2001-2006           |
| Ruslan R. Fazliev. All Rights Reserved.                                     |
+-----------------------------------------------------------------------------+
\*****************************************************************************/

#
# $Id: login.php,v 1.120.2.4 2006/06/16 13:00:50 max Exp $
#

include "../top.inc.php";
include("./func/func.mkcore.php");


if (!defined('XCART_START')) die("ERROR: Can not initiate application! Please check configuration.");

require_once $xcart_dir."/init.php";
#require_once $xcart_dir."/include/nocookie_warning.php";
require_once($xcart_dir.'/include/sessionstore/session_store_wrapper.php');

x_load('crypt','mail');

x_session_register("logout_user");
x_session_register("login");
x_session_register("userfirstname");

x_session_register("login_type");
x_session_register("logged");
x_session_register("previous_login_date");

x_session_register("login_attempt");
x_session_register("cart");
x_session_register("intershipper_recalc");

x_session_register("merchant_password");
x_session_register("account_type"); 
x_session_register("master_name"); 
 
$merchant_password = "";

$login_error = false;

switch ($redirect) {
	case "admin":
		$redirect_to = DIR_ADMIN;
		$current_type = 'A';
		break;
	case "provider":
		$redirect_to = DIR_PROVIDER;
		$current_type = 'P';
		break;
	case "partner":
		$redirect_to = DIR_PARTNER;
		$current_type = 'B';
		break;
	case "customer":
	default:
		$redirect_to = DIR_CUSTOMER;
		$current_type = 'C';
}
$redirect_to = $current_location.$redirect_to;
if ($REQUEST_METHOD == "POST") {
	$intershipper_recalc = "Y";

	x_session_register("identifiers",array());
	if ($mode == "login") {

		$username = $HTTP_POST_VARS["username"];
		$password = addslashes(text_crypt($HTTP_POST_VARS["password"]));
		$user_data = func_query_first("SELECT * FROM $sql_tbl[customers] WHERE login = '$username' AND usertype='$usertype' AND status='Y'");
		$allow_login = true;
		//added to enable role based access
		$roleQuery="select mr.type as role,mr.allowed_actions as actions from mk_roles mr,mk_user_roles mur where mur.roleid=mr.id and login = '$username'";
		$roleresult=db_query($roleQuery);
		$roles=array();
		if ($roleresult)
		{
			$i=0;

			while ($row=db_fetch_array($roleresult))
			{
				$roles[$i] = $row;
				$i++;
			}
		}
		$XCART_SESSION_VARS['user_roles']=$roles;

		if ($usertype == 'A' || ($usertype == "P" && $active_modules["Simple_Mode"])) {
			$iplist = array_unique(split('[ ,]+', $admin_allowed_ip));
			$iplist = array_flip($iplist);
			unset($iplist[""]);
			$iplist = array_flip($iplist);
			if (count($iplist) > 0)
				$allow_login = in_array($REMOTE_ADDR, $iplist);
		}
        
		$right_password = $user_data["password"];
		if (is_null($right_password)) {
			x_log_flag("log_decrypt_errors", "DECRYPT", "Could not decrypt password for the user ".$username, true);
		}
      
		
		if (!empty($user_data) && $password == $right_password && !empty($password) && $allow_login) {
			 $firstname =  $user_data['firstname'];
			 $lastname =  $user_data['lastname'];
			 $account_type = $user_data['account_type'];
			unset($right_password);
			$identifiers[$usertype] = array (
				'login' => $username,
                'firstname' => $firstname,
                'lastname' => $lastname,
				'login_type' => $usertype,
				'account_type' => $account_type
			);	
	
		$shopMasterId = 0;
		$accountType=0;
		if($account_type == "P2" || $account_type == "SM"){
			$accountType=1;
			$shop = getStoreForEmail($username);
				if($account_type == "SM")
					$shopMasterId = $shop['shop_master_id'];
				else
					$shopMasterId = $shop->shop_master_id;
		$aclient = SOAP_CLIENT::create_connection('http://localhost:8080/axis2/services/StoreService?wsdl'); // Path to wsdl declaring the service spec.
		$obj->shopMasterId = new Soapvar($shopMasterId, XSD_INT);
		$x = $aclient->getShopMasterdetails($obj);
		$mastername = $x->return->name;
		$identifiers[$usertype]['master_name'] = $mastername;
	    x_session_register('identifiers');

		}
		        
		if (!empty($active_modules['Simple_Mode'])) {
				if ($usertype == 'A') $identifiers['P'] = $identifiers[$usertype];
				if ($usertype == 'P') $identifiers['A'] = $identifiers[$usertype];
			}
#
# Success login
#
			#db_query("update mk_session set updated='Y' where sessionid= '$XCARTSESSID'");
			updateSessionUpdatedFlagBySessionId($XCARTSESSID, 'Y');
			x_session_register("login_change");
			if ($user_data["change_password"] == "Y") {
				$login_change["login"] = $user_data["login"];
				$login_change["login_type"] = $usertype;
				func_header_location($redirect_to."/change_password.php");
			}
			x_session_unregister("login_change");
          
			$login = $user_data["login"];  //$username; 
			$login_type = $usertype;
            $userfirstname = $user_data["firstname"];  //user first name
            
          //  $XCART_SESSION_VARS['firstname'] = $firstname;
          
        
			
			$logged = "";
			if ($usertype == "C") {
				if(!empty($active_modules['SnS_connector']))
					func_generate_sns_action("Login");
				x_session_register("login_redirect");
				$login_redirect = 1;
			}
			

          
#
# 1) generate $last_login by current timestamp and update database
# 2) insert entry into login history
#
			$tm = time();

			$previous_login_date = func_query_first_cell("SELECT last_login FROM $sql_tbl[customers] WHERE login='$login'");
			if ($previous_login_date == 0)
				$previous_login_date = $tm;

			db_query("UPDATE $sql_tbl[customers] SET last_login='$tm' WHERE login='$login'");
			db_query("REPLACE INTO $sql_tbl[login_history] (login, date_time, usertype, action, status, ip) VALUES ('$username','$tm','$usertype','login','success','$REMOTE_ADDR')");


#
# Set cookie with username if Greet visitor module enabled
#

			if (!empty($active_modules["Greet_Visitor"]) && $login_type == "C")
				include $xcart_dir."/modules/Greet_Visitor/set_cookie.php";

			$logout_user = false;
			
#
# If shopping cart is not empty then user is redirected to cart.php
# Default password alert
#
			if ($login_type == "A" || $login_type == "P") {
				$to_url = (!empty($active_modules["Simple_Mode"]) || $login_type == "A" ? $xcart_catalogs["admin"] : $xcart_catalogs["provider"])."/home.php";
				$current_area = $login_type;
				include $xcart_dir."/include/get_language.php";
			}
			

			$default_accounts = func_check_default_passwords($login);

            
			if (!empty($default_accounts)) {
				$current_area = $login_type;
				$txt_message = strip_tags(func_get_langvar_by_name("txt_your_password_warning_js",false,false,true));
				$txt_continue = strip_tags(func_get_langvar_by_name("lbl_continue",false,false,true));
				$txt_message_js = func_js_escape($txt_message);
				$javascript_message = <<<JS
<script language='JavaScript'>
	alert('$txt_message_js');
	self.location='$to_url';
</script>
$txt_message
<br /><br />
<a href="$to_url">$txt_continue</a>
JS;
			}
			elseif ($usertype == "A" || !empty($active_modules["Simple_Mode"])) {
		
				$default_accounts = func_check_default_passwords();
				if (!empty($default_accounts)) {
					$txt_message = strip_tags(func_get_langvar_by_name("txt_default_passwords_warning_js", array("accounts"=>implode(", ", $default_accounts)),false,true));
					$txt_continue = strip_tags(func_get_langvar_by_name("lbl_continue",false,false,true));
					$javascript_message = <<<JS
<script language='JavaScript'>
	alert('$txt_message');
	self.location='$to_url';
</script>
$txt_message
<br /><br />
<a href="$to_url">$txt_continue</a>
JS;
				}
			}
			if ($login_type == "C" && $user_data["cart"] && func_is_cart_empty($cart))
				$cart = unserialize($user_data["cart"]);
					

			if ($login_type == "C" || $login_type == "B") {

				#For POS login
				if($account_type == "P2" || $account_type == "SM")
                			func_header_location($xcart_web_dir."/mkstartshopping.php",false);
				#
				# Redirect to saved URL
				x_session_register("remember_data");
				if ($is_remember == 'Y' && !empty($remember_data)) {
					
					func_header_location($remember_data['URL'], false);
				}
				
						
					$filetype = base64_encode($_POST['filetype']);
				   if($appreferer)
					{
						func_header_location($appreferer, false);
					}

				    if(strstr($HTTP_REFERER, 'community') === FALSE)
					{
						if(strstr($HTTP_REFERER, '?') === FALSE)
							$xcart_dir = $HTTP_REFERER . "?filetype=".$filetype;
						else
							$xcart_dir = $HTTP_REFERER . "&filetype=".$filetype;
					}
                    else
	          		{
						if(strstr($HTTP_REFERER, '?') === FALSE)
							$xcart_dir = $HTTP_REFERER . "?filetype=".$filetype;
						else
							$xcart_dir = $HTTP_REFERER . "&filetype=".$filetype;
					}
					
			        //func_header_location($xcart_web_dir.strrchr($HTTP_REFERER, "/"), false);
//					func_header_location($xcart_dir, false);
				//load the change password page 
		 func_header_location($xcart_web_dir."/reset_password.php?msg=password_changed");	
			}
			
			

			if (($config["General"]["default_pwd"] == "Y") && !empty($javascript_message) && $admin_safe_mode == false) {
				x_session_save();
				echo $javascript_message;
				exit;
			}
		
			if($login_type == "C")
			{
					func_header_location($xcart_web_dir."/home.php");
			}
			else if($login_type == "A")
			{
				func_header_location($xcart_web_dir."/admin/home.php");
			}
			else if($login_type == "P")
			{
				func_header_location($xcart_web_dir."/provider/home.php");
			}
		
		} else {
#
# Login incorrect
#
			$login_status = "failure";

			

			unset($right_password);

			if (!$allow_login)
				$login_status = "restricted";

			$disabled = func_query_first_cell("SELECT COUNT(*) FROM $sql_tbl[customers] WHERE login='$username' AND usertype='$usertype' AND status<>'Y' AND status<>'A'");
			if ($disabled) {
				x_session_register('top_message');
				$top_message['type'] = 'I';
				$top_message['content'] = func_get_langvar_by_name('err_account_temporary_disabled');
				$login_status = 'disabled';
			}

			if (!func_query_first("SELECT login FROM $sql_tbl[login_history] WHERE login='$username' AND date_time='".time()."'"))
				db_query("REPLACE INTO $sql_tbl[login_history] (login, date_time, usertype, action, status, ip) VALUES ('$username','".time()."','$usertype','login','$login_status', '$REMOTE_ADDR')");

			if (($redirect == "admin" || (@$active_modules["Simple_Mode"] == "Y" && $redirect == "provider")) && $config['Email_Note']['eml_login_error'] == 'Y') {
#
# Send security alert to website admin
#
				@func_send_mail($config["Company"]["site_administrator"], "mail/login_error_subj.tpl", "mail/login_error.tpl", $config["Company"]["site_administrator"], true);

			}

#
# After 3 failures redirects to Recover password page
#
	/*I  am commeting passwrod recovery*/
			
			$login_attempt++;
			//if ($login_attempt >= 3) {
			//	$login_attempt = "";
			//	func_header_location($redirect_to."/help.php?section=Password_Recovery");
			//}
			//else
			//{
				if($current_type == "C")
				{
				if($HTTP_POST_VARS['posshop'] == "true"){
					func_header_location($xcart_web_dir."/".strtolower($XCART_SESSION_VARS['identifiers'][$current_type]['master_name'])."?incorrect=true",false);
					}
					if(isset($appreferer))
					{
		                            $redirector = base64_encode(urlencode($appreferer));
					}
//					func_header_location($xcart_web_dir."/mkloginerror_message.php?message=login_incorrect&redirecturl=$redirector",false);
					func_header_location($xcart_web_dir."/register.php?message=login_incorrect&redirecturl=$redirector",false);
				}
				else if($current_type == "A")
				{

					func_header_location($xcart_web_dir."/admin/error_message.php?login_incorrect");
				}
				else if($current_type == "P")
				{
					func_header_location($xcart_web_dir."/provider/error_message.php?login_incorrect",false);
				}

				
			//}
		}
	}
}

##################### START BLOCK LOGOUT CODE ##################################
if ($mode == "logout") 
{
	
	x_session_register("identifiers",array());
	x_session_register("payment_cc_fields");
	$payment_cc_fields = array();

	func_unset($identifiers,$current_type);

	if (!empty($active_modules['Simple_Mode'])) {
		if ($current_type == 'A') func_unset($identifiers,'P');
		if ($current_type == 'P') func_unset($identifiers,'A');
	}

	// destroy all the session variable on logout
	if (x_session_is_registered("chosenProductConfiguration"))
            x_session_unregister("chosenProductConfiguration");
      //x_session_register("chosenProductConfiguration");


	if (x_session_is_registered("masterCustomizationArray"))
		x_session_unregister("masterCustomizationArray");

	if (x_session_is_registered("masterCustomizationArrayD"))
		x_session_unregister("masterCustomizationArrayD");

    if (x_session_is_registered("productsInCart"))
		x_session_unregister("productsInCart");

	if (x_session_is_registered("coupondiscount"))
		x_session_unregister("coupondiscount");

	if (x_session_is_registered("couponCode"))
		x_session_unregister("couponCode");

	if (x_session_is_registered("offers"))
		x_session_unregister("offers");

	if (x_session_is_registered("totaloffers"))
		x_session_unregister("totaloffers");

	if (x_session_is_registered("totaldiscountonoffer"))
		x_session_unregister("totaldiscountonoffer");

	if (x_session_is_registered("productids"))
		x_session_unregister("productids");	
		
	if (x_session_is_registered("grandTotal"))
		x_session_unregister("grandTotal");	
		
	if (x_session_is_registered("amountAfterDiscount"))
		x_session_unregister("amountAfterDiscount");	
     
     if (x_session_is_registered("userfirstname"))
        x_session_unregister("userfirstname");    

       $XCART_SESSION_VARS['firstname'] ="" ;
	
// x_session_register("masterCustomizationArray");
#
# Insert entry into login_history
#
	db_query("REPLACE INTO $sql_tbl[login_history] (login, date_time, usertype, action, status, ip) VALUES ('$login','".time()."','$login_type','logout','success','$REMOTE_ADDR')");
	
	$old_login_type = $current_type;
	$login = "";
	$login_type = "";
	$cart = "";
	$access_status = "";
	$merchant_password = "";
	$logout_user = true;
	if ($current_type == 'A' || $current_type == 'P')
		func_ge_erase();

	x_session_unregister("hide_security_warning");
	x_session_register("login_redirect");
	$login_redirect = 1;
	
	$logout_event = "success";
	x_session_register("logout_event");
	
#
#If user logged out from private interface  
#	
	if($HTTP_POST_VARS['redirectto']=="private"){
	 	if (x_session_is_registered("orgId"))
        	x_session_unregister("orgId");
        if (x_session_is_registered("headerpath"))
        	x_session_unregister("headerpath");    
        if (x_session_is_registered("orgname"))
        	x_session_unregister("orgname"); 
        
        $accounturl = $HTTP_POST_VARS['accountname'];
        $orgId = $HTTP_POST_VARS['orgId'];
        func_header_location($http_location,false); 
	}

	if($current_type == "C")
	{
		if($account_type == "P2" || $account_type == "SM"){
			func_header_location($xcart_web_dir."/".strtolower($XCART_SESSION_VARS['identifiers'][$current_type]['master_name']),false);
		}else
			func_header_location($xcart_web_dir."/home.php",false);
	}
	else if($current_type == "A")
	{
		func_header_location($xcart_web_dir."/admin/home.php",false);
	}
	else if($current_type == "P")
	{
		func_header_location($xcart_web_dir."/provider/home.php",false);
	}
	
}
##################### END BLOCK FOR LOGOUT  ##################################

if ($old_login_type == 'C') {
	if (!empty($HTTP_REFERER) && (strncasecmp($HTTP_REFERER, $http_location, strlen($http_location)) == 0 || strncasecmp($HTTP_REFERER, $https_location, strlen($https_location)) == 0)) {
		if (strpos($HTTP_REFERER, "mode=order_message") === false &&
			strpos($HTTP_REFERER, "mode=wishlist") === false &&
			strpos($HTTP_REFERER, "bonuses.php") === false &&
			strpos($HTTP_REFERER, "returns.php") === false &&
			strpos($HTTP_REFERER, "orders.php") === false &&
			strpos($HTTP_REFERER, "giftreg_manage.php") === false &&
            strpos($HTTP_REFERER, "order.php") === false &&
			strpos($HTTP_REFERER, "register.php?mode=delete") === false &&
			strpos($HTTP_REFERER, "register.php?mode=update") === false) {
			func_header_location($redirect_to.strrchr($HTTP_REFERER, "/"), false);
		}
	}
}

//func_header_location($xcart_web_dir."/home.php", false);
if($current_type == "C")
{
	func_header_location($xcart_web_dir."/home.php",false);
}
else if($current_type == "A")
{
	func_header_location($xcart_web_dir."/admin/home.php",false);
}
else if($current_type == "P")
{
	func_header_location($xcart_web_dir."/provider/home.php",false);
}
?>
