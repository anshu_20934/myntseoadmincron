<?php
/*****************************************************************************\
+-----------------------------------------------------------------------------+
| X-Cart                                                                      |
| Copyright (c) 2001-2006 Ruslan R. Fazliev <rrf@rrf.ru>                      |
| All rights reserved.                                                        |
+-----------------------------------------------------------------------------+
| PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
| FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
| AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
|                                                                             |
| THIS  AGREEMENT  EXPRESSES  THE  TERMS  AND CONDITIONS ON WHICH YOU MAY USE |
| THIS SOFTWARE   PROGRAM   AND  ASSOCIATED  DOCUMENTATION   THAT  RUSLAN  R. |
| FAZLIEV (hereinafter  referred to as "THE AUTHOR") IS FURNISHING  OR MAKING |
| AVAILABLE TO YOU WITH  THIS  AGREEMENT  (COLLECTIVELY,  THE  "SOFTWARE").   |
| PLEASE   REVIEW   THE  TERMS  AND   CONDITIONS  OF  THIS  LICENSE AGREEMENT |
| CAREFULLY   BEFORE   INSTALLING   OR  USING  THE  SOFTWARE.  BY INSTALLING, |
| COPYING   OR   OTHERWISE   USING   THE   SOFTWARE,  YOU  AND  YOUR  COMPANY |
| (COLLECTIVELY,  "YOU")  ARE  ACCEPTING  AND AGREEING  TO  THE TERMS OF THIS |
| LICENSE   AGREEMENT.   IF  YOU    ARE  NOT  WILLING   TO  BE  BOUND BY THIS |
| AGREEMENT, DO  NOT INSTALL OR USE THE SOFTWARE.  VARIOUS   COPYRIGHTS   AND |
| OTHER   INTELLECTUAL   PROPERTY   RIGHTS    PROTECT   THE   SOFTWARE.  THIS |
| AGREEMENT IS A LICENSE AGREEMENT THAT GIVES  YOU  LIMITED  RIGHTS   TO  USE |
| THE  SOFTWARE   AND  NOT  AN  AGREEMENT  FOR SALE OR FOR  TRANSFER OF TITLE.|
| THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY GRANTED BY THIS AGREEMENT.      |
|                                                                             |
| The Initial Developer of the Original Code is Ruslan R. Fazliev             |
| Portions created by Ruslan R. Fazliev are Copyright (C) 2001-2006           |
| Ruslan R. Fazliev. All Rights Reserved.                                     |
+-----------------------------------------------------------------------------+
\*****************************************************************************/

#
# $Id: bots.php,v 1.8 2006/01/11 06:55:58 mclap Exp $
#
# Bot identificator module
#
if ( !defined('XCART_START') ) { header("Location: ../"); die("Access denied"); }

/* Pravin. We avoid setting session variables here. In init.php, bots.php is included before including sessions.php
 * This is done, so that in sessions.php, we have prior knowledge, if a robot/spider is accessing the page, in which case we don't save session in DB 
 */ 
// x_session_register("is_robot");
// x_session_register("robot");

/** The function returns an array of Bots fetched from database 
 */
function getUserAgentArray()
{
	global $xcache;
	
	$id = "bots";
	
	// Fetch user agents from xcache
	$ua = $xcache->fetch($id);
	
	if($ua==null) {
		// The useragents are not yet stored in xcache. We fetch them from DB
		
		$query = "select * from mk_bots";
		$ua = array();
		$res = db_query($query);
		if ($res) {
			while ($row = db_fetch_row($res)) {
				$bot_type = $row[1];
				$bot_name = $row[2];
				
				if(array_key_exists($bot_type, $ua)) {
					// In case an array exists, we append new bot to existing array list of values.
					$ua[$bot_type][] = $bot_name;
				} else {
					$ua[$bot_type] = array($bot_name);
				}				
			}
		}

		db_free_result($res);
		
		// We store the newly fetched bots into xcache
		$xcache->store($id, $ua, 2592000);
	} 
	
	return $ua;
} // End of function


if(!empty($HTTP_USER_AGENT) && !defined("IS_ROBOT") && empty($is_robot)) {

	$ua = getUserAgentArray();
	
	$hosts = array(
		"Infoseek" => array('198.5.210.','204.162.96.','204.162.97.','204.162.98.','205.226.201.','205.226.203.','205.226.204.'),
		"Lycos" => array('206.79.171.','207.77.90.','208.146.26.','209.67.228.','209.67.229.')
	);
	
	if (!empty($ua[$HTTP_USER_AGENT])) {
		define("IS_ROBOT", 1);
		define("ROBOT", $HTTP_USER_AGENT);
	}
	else {
		foreach ($ua as $k => $v) {
			foreach ($v as $u) {
				if (stristr($HTTP_USER_AGENT, $u) !== false) {
					define("IS_ROBOT", 1);
					define("ROBOT", $k);
					break;
				}
			}

			if (defined("IS_ROBOT")) break;
		}
	}

	if (!defined("IS_ROBOT") && !empty($REMOTE_ADDR)) {
		foreach ($hosts as $k => $v) {
			foreach ($v as $u) {
				if (strncmp($REMOTE_ADDR, $u, strlen($u)) === 0) {
					define("IS_ROBOT", 1);
					define("ROBOT", $k);
					break;
				}
			}

			if (defined("IS_ROBOT")) { 
				break;
			}
		}
	}
	
	unset($ua, $hosts);

	if (defined("IS_ROBOT")) {
		$is_robot = 'Y';
		$robot = ROBOT;
	} else {
		$is_robot = 'N';
		$robot='';
	}
} elseif (defined("IS_ROBOT")) {
	$is_robot = 'Y';
} elseif (!empty($is_robot)) {
	if ($is_robot == 'Y') {
		define("IS_ROBOT", 1);
		define("ROBOT", $robot);
	} 
}

$smarty->assign("is_robot", $is_robot);
?>
