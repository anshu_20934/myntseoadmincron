<?php
require "./auth.php";
if(!empty($mode)) $mode ="";
ini_set("display_errors",1);
if ($REQUEST_METHOD == "POST") 
{
			if(!$_POST[sameAsShip]) $sameAsShip = 'N';
			else $sameAsShip = $_POST[sameAsShip];

			$date = explode("/",$_POST['orderdate']);
			$date = mktime(0,0,0, $date[0],$date[1],$date[2]);
			
            $profile_values = array();
            $profile_values['login'] = $_POST[email];
            $profile_values['email'] = $_POST[email];
            $profile_values['usertype'] = 'C';
            $profile_values['firstname'] = $_POST[firstname];
            $profile_values['lastname'] = $_POST[lastname];
            $profile_values['phone'] = $_POST[s_phone];
            $profile_values['mobile'] = $_POST[s_mobile];
            $profile_values['b_firstname'] = $_POST[firstname];
            $profile_values['b_lastname'] = $_POST[lastname];
            $profile_values['b_address'] = $_POST[b_address];
            $profile_values['b_city'] = $_POST[b_city];
            $profile_values['b_state'] = $_POST[b_state];
            $profile_values['b_country'] = $_POST[b_country];
            $profile_values['b_zipcode'] = $_POST[b_zip];
            $profile_values['s_firstname'] = $_POST[firstname];
            $profile_values['s_lastname'] = $_POST[lastname];
            $profile_values['s_address'] = $_POST[s_address];
            $profile_values['s_city'] = $_POST[s_city];
            $profile_values['s_state'] = $_POST[s_state];
            $profile_values['s_country'] = $_POST[s_country];
            $profile_values['s_zipcode'] = $_POST[s_zip];
            $profile_values['ship2diff'] = $sameAsShip;
            $profile_values['offline'] = 'Y';
			$profile_values['company'] = $_POST[company];
			$profile_values['first_login'] = $date;

    if ($_POST['mode'] == "savecustomer") 
    {
        $result = db_query("SELECT login FROM $sql_tbl[customers] WHERE login='$_POST[email]'");
		$numrow = db_num_rows($result);
	
        if($numrow == 0)
        {
            func_array2insert('customers', $profile_values);
	    
			header("location:offline_orders.php?email=$_POST[email]");
        }
        else
        {
            $top_message["content"] = "$_POST[email] already exist. Please try again";
        }
    }  
	if ($_POST['mode'] == "updatecustomer") 
    {
       
            func_array2update('customers', $profile_values, "login='$_POST[email]'");
			header("location:offline_orders.php?email=$_POST[email]&action=update");
    }  
}

if($_GET[email])
{
	$offlinecustomer = func_query_first("SELECT 	login,firstname,lastname,phone,mobile,b_firstname,b_lastname,b_address,b_city,b_state,b_country,b_zipcode,s_firstname,s_lastname,s_address,s_city,s_state,s_country,s_zipcode,ship2diff as sameAsShip, company, FROM_UNIXTIME(first_login, '%m/%d/%Y') as first_login FROM $sql_tbl[customers] WHERE login='$_GET[email]'");
	

	$smarty->assign ("offlinecustomer", $offlinecustomer);
	if($_GET[order]=='success')
		$top_message["content"] = "Congratulation!! $offlinecustomer[firstname]'s order has been placed successfully. To see the details of his order go to order detail page by clicking of Orders link in  left menu. ";
	elseif($_GET[action]=='exist')
		$top_message["content"] = "$_GET[email] customer selected";
	else
		$top_message["content"] = "$_GET[email] customer created successfully";
}

//message update
if($_GET['action'] == 'update')
{
	$top_message["content"] = "$_GET[email] customer updated successfully";
}

//Load existing offline customer
$offlinecust = func_query("SELECT firstname, lastname, login FROM xcart_customers WHERE offline='Y'", true);
if(!empty($offlinecust))
{
	$custarr = array();
	foreach($offlinecust AS $key=>$value)
	{
		$custdetail = $value;
		$custarr[$custdetail['login']] =  $custdetail['firstname']." ".$custdetail['lastname'];
	}
}


$smarty->assign ("custarr", $custarr);
$smarty->assign ("pb_state", $_POST['b_state']);
$smarty->assign ("ps_state", $_POST['s_state']);
$smarty->assign ("s_state", $offlinecustomer['s_state']);
$smarty->assign ("b_state", $offlinecustomer['b_state']);
$smarty->assign ("custemail", $_GET[email]);
$smarty->assign ("top_message", $top_message);
?>