<?php
/*****************************************************************************\
+-----------------------------------------------------------------------------+
| X-Cart                                                                      |
| Copyright (c) 2001-2006 Ruslan R. Fazliev <rrf@rrf.ru>                      |
| All rights reserved.                                                        |
+-----------------------------------------------------------------------------+
| PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
| FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
| AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
|                                                                             |
| THIS  AGREEMENT  EXPRESSES  THE  TERMS  AND CONDITIONS ON WHICH YOU MAY USE |
| THIS SOFTWARE   PROGRAM   AND  ASSOCIATED  DOCUMENTATION   THAT  RUSLAN  R. |
| FAZLIEV (hereinafter  referred to as "THE AUTHOR") IS FURNISHING  OR MAKING |
| AVAILABLE TO YOU WITH  THIS  AGREEMENT  (COLLECTIVELY,  THE  "SOFTWARE").   |
| PLEASE   REVIEW   THE  TERMS  AND   CONDITIONS  OF  THIS  LICENSE AGREEMENT |
| CAREFULLY   BEFORE   INSTALLING   OR  USING  THE  SOFTWARE.  BY INSTALLING, |
| COPYING   OR   OTHERWISE   USING   THE   SOFTWARE,  YOU  AND  YOUR  COMPANY |
| (COLLECTIVELY,  "YOU")  ARE  ACCEPTING  AND AGREEING  TO  THE TERMS OF THIS |
| LICENSE   AGREEMENT.   IF  YOU    ARE  NOT  WILLING   TO  BE  BOUND BY THIS |
| AGREEMENT, DO  NOT INSTALL OR USE THE SOFTWARE.  VARIOUS   COPYRIGHTS   AND |
| OTHER   INTELLECTUAL   PROPERTY   RIGHTS    PROTECT   THE   SOFTWARE.  THIS |
| AGREEMENT IS A LICENSE AGREEMENT THAT GIVES  YOU  LIMITED  RIGHTS   TO  USE |
| THE  SOFTWARE   AND  NOT  AN  AGREEMENT  FOR SALE OR FOR  TRANSFER OF TITLE.|
| THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY GRANTED BY THIS AGREEMENT.      |
|                                                                             |
| The Initial Developer of the Original Code is Ruslan R. Fazliev             |
| Portions created by Ruslan R. Fazliev are Copyright (C) 2001-2006           |
| Ruslan R. Fazliev. All Rights Reserved.                                     |
+-----------------------------------------------------------------------------+
\*****************************************************************************/

#
# $Id: sessions.php,v 1.62.2.2 2006/06/08 10:43:27 svowl Exp $
#

use abtest\MABTest;

if ( !defined('XCART_START') ) { header("Location: ../"); die("Access denied"); }

#
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# DO NOT CHANGE ANYTHING BELOW THIS LINE UNLESS
# YOU REALLY KNOW WHAT ARE YOU DOING
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#

if (defined('XCART_SESSION_START'))
	return;

define("XCART_SESSION_START", 1);

if ($use_sessions_type == 2)
	include $xcart_dir."/include/mysql_sessions.php";
require_once($xcart_dir.'/include/sessionstore/session_store_wrapper.php');
require_once($xcart_dir.'/include/sessionstore/serialization_format_wrapper.php');
require_once($xcart_dir.'/include/idp/SessionHandler.php');

#
# PHP build-in sessions tuning (for type "1" & "2")
#

# PHP 4.3.0 and higher allow to turn off trans-sid using this command:
ini_set("url_rewriter.tags","");
# Let's garbage collection will occurs more frequently
ini_set("session.gc_probability",90);
ini_set("session.gc_divisor",100); # for PHP >= 4.3.0
ini_set("session.use_cookies", false);

#
# Anti cache block
#

if (defined("SET_EXPIRE")) {
	header("Expires: ".gmdate("D, d M Y H:i:s", SET_EXPIRE)." GMT");
} else {
	header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
}
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");

if (defined("SET_EXPIRE")) {
	header("Cache-Control: public");
}
elseif ($HTTPS) {
	header("Cache-Control: private, must-revalidate");
}
else {
	header("Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
	header("Pragma: no-cache");
}

$XCARTSESSID = htmlspecialchars(getMyntraCookie($XCART_SESSION_NAME));

if (isset($XCARTSESSID))
	$XCARTSESSID = $XCARTSESSID;
else {
	$XCARTSESSID = false;
}


if($HTTPS && empty($XCARTSESSID) && in_array($_SERVER['SCRIPT_NAME'], $secure_pages_list)) { //https set in https_detect.php included in prepare.php
	//no session found for the https page, we will redirect him to a http page where a cookie for https will be set
	func_header_location($http_location. "/mkSetSession.php?ref=".$_SERVER['SCRIPT_NAME']);
}

if($_SERVER['SCRIPT_NAME'] === "/mkSetSession.php" && isset($_GET['ref']) && !empty($XCARTSESSID)) {
	func_header_location($https_location. $_GET['ref'] . "?" . $XCART_SESSION_NAME . "=" . $XCARTSESSID);
}

x_session_start($XCARTSESSID);
/***INVOKE SESSION TIMEOUT CALL IF YOU ARE in SECURE PAGE***/
x_session_timeout($XCARTSESSID);

ShutdownHook::addHook("x_session_save");




####################################################################
#   FUNCTIONS
####################################################################

#
# Start session
#
function x_session_start($sessid) {

	global $XCART_SESSION_VARS, $XCART_SESSION_NAME, $XCARTSESSID;
        global $sql_tbl, $use_sessions_type;
	global $HTTP_SERVER_VARS;
	global $cookiedomain;

	global $is_robot; // This was set in bots.php ( bots.php is included before sessions.php in init.php)

	if($is_robot=="Y") {
		// If its a robot, we skip further operations of creating and saving a session in DB.
		return;
	}

	# $sessid should contain only '0'..'9' or 'a'..'z' or 'A'..'Z'
	if (strlen($sessid) > 50 || !empty($sessid) && !preg_match('!^[0-9a-zA-Z\-]+$!S', $sessid)) {
		$sessid = "";
	}

	$XCART_SESSION_VARS = array();
	#
	# For new sessions always generate unique id
	#

	$l = 0;
	if (isset($HTTP_SERVER_VARS["REMOTE_PORT"]))
		$l = $HTTP_SERVER_VARS["REMOTE_PORT"];

	list($usec, $sec) = explode(' ', microtime());
	// Pravin . We can get rid of srand function as myntra.com has php version 5.3.3
	// and http://php.net/manual/en/function.srand.php mentions the following note:
	// "As of PHP 4.2.0, there is no need to seed the random number generator with srand() or mt_srand() as this is now done automatically"
	//srand((float) $sec + ((float) $usec * 1000000) + (float)$l);

	$sessid_is_empty = empty($sessid);
	$session_data_type_json = x_session_is_JSON($sessid);
	$sessReset = false;
	$sess_data = array();
	if (!$sessid_is_empty && ($use_sessions_type == 2 || $use_sessions_type == 3)) {
	    $sess_data = getSessionBySessionId($sessid);
	    if (empty($sess_data))
	       $sessid = "";
		//TO convert old session id to new JJ format
	    if(!x_session_is_JSON($sessid)){
	    	$sessid = "";
	    	$sessReset = true;
	    }
	}

	if (empty($sessid)) {
           $sessid = SessionHandler::createSession();
	}

	if ($use_sessions_type < 3) {
		if ($use_sessions_type == 2) {
			# restore handler for mysql sessions
			session_set_save_handler (
				'db_session_open',
				'db_session_close',
				'db_session_read',
				'db_session_write',
				'db_session_destroy',
				'db_session_gc'
				);
		}

		#
		# Using standard PHP sessions
		#
		session_cache_limiter('none');

		session_name($XCART_SESSION_NAME);
		if ($sessid)
			session_id($sessid);

		session_start();

		if (strlen(session_encode()) == 0) {
			define("NEW_SESSION", true);
			if (!$sessid_is_empty && $use_sessions_type == 1) {
				$sessid = md5(uniqid(rand()));
				session_id($sessid);
			}
		}

		$XCARTSESSID = session_id();

		return;
	}


	// 20-Dec-2010. Pravin.  The sessions length was 10800 seconds earlier, which is 180 mins = 3 hrs only.
	// This is being changed to .. 30 days   (30 days =  30 x 24 x 60 x 60 seconds = 2592000 seconds)
	#keeping it 1 week default : 1 week = 604 800 second
	$sessionExpirySecs = FeatureGateKeyValuePairs::getInteger("sessionExpirySecs", 604800);
	$curtime = time();
	// Expiry_time set for 30 days.
	$expiry_time = $curtime + $sessionExpirySecs;

	if ($sess_data) {
		$XCART_SESSION_VARS = x_session_unserialize_by_type($session_data_type_json, $sess_data["data"]);
		/* set globals */
		if (isset($GLOBALS['sessionExpiry']))
		   unset($GLOBALS['sessionExpiry']);
		$GLOBALS['sessionExpiry'] = $expiry_time;

		if (isset($GLOBALS['sessionLastAccess']))
		    unset($GLOBALS['sessionLastAccess']);
		$GLOBALS['sessionLastAccess'] = $curtime;
	}
	if($sessReset || empty($sess_data)) {
		define("NEW_SESSION", true);
		$expiry_time = time()+FeatureGateKeyValuePairs::getInteger("session.expiry.InitialSessionExpirySecs", 600); // 10 minutes expiry on first request
	//	replaceSessionDataBySessionId($sessid, $curtime, $expiry_time, 'N');
	}

	$XCARTSESSID = $sessid;
	// Pravin - Earlier expire value was set to 0. This caused cookie to expire at the end of the session (when the browser closes)
	// We change this now, and we use above set $expiry_time, which will expire cookie after $expiry_time. (and not when browser is closed).
	// Setting the cookie HTTP only and passing false for secure flag as we want to use it for secure and non secure pages.
	setMyntraCookie($XCART_SESSION_NAME, $XCARTSESSID, $expiry_time, "/", $cookiedomain, false, true);
	session_hijacking_log();
}

#
# Change current session to session with specified ID
#
function x_session_id($sessid="") {
	global $sql_tbl, $use_sessions_type, $XCART_SESSION_VARS, $XCARTSESSID, $XCART_SESSION_UNPACKED_VARS;

	$XCART_SESSION_VARS = array();
	if ($use_sessions_type < 3) {
		#
		# Using standard PHP sessions
		#
		if ($sessid) {
			session_write_close();
			x_session_start($sessid);
			return;
		}

		$XCARTSESSID = session_id();

		return $XCARTSESSID;
	}

	if ($sessid) {
	    $sess_data = getSessionBySessionId($sessid);
		$XCARTSESSID = $sessid;
		if ($sess_data) {
			$XCART_SESSION_VARS = x_session_unserialize($sessid, $sess_data["data"]);
			if (!empty($XCART_SESSION_UNPACKED_VARS)) {
				foreach ($XCART_SESSION_UNPACKED_VARS as $var => $v) {
					if (isset($GLOBALS[$var]))
						unset($GLOBALS[$var]);

					unset($XCART_SESSION_UNPACKED_VARS[$var]);
				}
			}
		}
		else {
			x_session_start($sessid);
		}
	}
	else {
		$sessid = $XCARTSESSID;
	}

	return $sessid;
}

#
# Cut off variable if it is come from _GET, _POST or _COOKIES
#
function check_session_var($varname) {
	global $_GET, $_POST, $_COOKIE;

	if (isset($_GET[$varname]) || isset($_POST[$varname]) || isset($_COOKIE[$varname]))
		return false;

	return true;
}

#
# Register variable XCART_SESSION_VARS array from the database
#
function x_session_register($varname, $default="") {
	global $XCART_SESSION_VARS, $XCART_SESSION_UNPACKED_VARS;
	global $use_sessions_type;
	global $_SESSION;

	if (empty($varname))
		return false;

	if ($use_sessions_type < 3) {
		#
		# Using standard PHP sessions
		#
		if (!session_is_registered($varname) && check_session_var($varname)) {
			$_SESSION[$varname] = $default;
		}

		session_register($varname);

		#
		# Register global variable
		#
		$GLOBALS[$varname] =& $_SESSION[$varname];
		return;
	}

	#
	# Register variable $varname in $XCART_SESSION_VARS array
	#
	if (!isset($XCART_SESSION_VARS[$varname])) {
		if (isset($GLOBALS[$varname]) && check_session_var($varname)) {
			$XCART_SESSION_VARS[$varname] = $GLOBALS[$varname];
		}
		else {
			$XCART_SESSION_VARS[$varname] = $default;
		}
	}
	else {
		if (isset($GLOBALS[$varname]) && check_session_var($varname)) {
			$XCART_SESSION_VARS[$varname] = $GLOBALS[$varname];
		}
	}

	#
	# Unpack variable $varname from $XCART_SESSION_VARS array
	#
	$XCART_SESSION_UNPACKED_VARS[$varname] = $XCART_SESSION_VARS[$varname];
	$GLOBALS[$varname] = $XCART_SESSION_VARS[$varname];
}

#
# Save the XCART_SESSION_VARS array in the database
#
function x_session_save() {
	global $XCARTSESSID;
	global $XCART_SESSION_VARS, $XCART_SESSION_UNPACKED_VARS;
	global $sql_tbl, $use_sessions_type, $bench_max_session;

    global $is_robot;
	if ($is_robot=="Y") {
           // This is a robot, so avoid unnecessary saves to sessions service and database.
          return;
    }

	if ($use_sessions_type < 3) {
		#
		# Using standard PHP sessions
		#
		return;
	}

	$varnames = func_get_args();
	if (!empty($varnames)) {
		foreach ($varnames as $varname) {
			if (isset($GLOBALS[$varname]))
				$XCART_SESSION_VARS[$varname] = $GLOBALS[$varname];
		}
	}
	elseif (is_array($XCART_SESSION_UNPACKED_VARS)) {
		foreach ($XCART_SESSION_UNPACKED_VARS as $varname=>$value) {
			if (isset($GLOBALS[$varname]))
				$XCART_SESSION_VARS[$varname] = $GLOBALS[$varname];
		}
	}

	#
	# Save session variables in the database
	#
	if (defined("BENCH") && constant("BENCH")) {
		$len = strlen(serialize($XCART_SESSION_VARS));
		if ($bench_max_session < $len)
			$bench_max_session = $len;
	}

	$expiryTime = null;
	$lastAccessedTime = null;

	if(defined("NEW_SESSION") && constant("NEW_SESSION")==true){
		$expiryTime = time()+FeatureGateKeyValuePairs::getInteger("session.expiry.InitialSessionExpirySecs", 600); // 10 minutes expiry on first request
	}else if (isset($GLOBALS['sessionExpiry'])){
	    $expiryTime = $GLOBALS['sessionExpiry'];
	}

	if (isset($GLOBALS['sessionLastAccess']))
	    $lastAccessedTime = $GLOBALS['sessionLastAccess'];

	updateSessionBySessionId($XCARTSESSID, $expiryTime, $lastAccessedTime, x_session_serialize($XCARTSESSID, $XCART_SESSION_VARS));
}

#
# Unregister variable $varname from $XCART_SESSION_VARS array
#
function x_session_unregister($varname, $unset_global=false) {
	global $XCART_SESSION_VARS, $XCART_SESSION_UNPACKED_VARS;
	global $use_sessions_type;

	if (empty($varname))
		return false;

	if ($use_sessions_type < 3) {
		#
		# Using standard PHP sessions
		#
		session_unregister($varname);
		return;
	}

	func_unset($XCART_SESSION_VARS, $varname);
	func_unset($XCART_SESSION_UNPACKED_VARS, $varname);

	if ($unset_global) {
		func_unset($GLOBALS, $varname);
	}
}

#
# Find out whether a global variable $varname is registered in
# $XCART_SESSION_VARS array
#
function x_session_is_registered($varname) {
	global $XCART_SESSION_VARS;
	global $use_sessions_type;

	if (empty($varname))
		return false;

	if ($use_sessions_type < 3) {
		#
		# Using standard PHP sessions
		#
		return session_is_registered($varname);
	}

	return isset($XCART_SESSION_VARS[$varname]);
}

#*****************SESSION TIMEOUT************************#
#
#********  This sblock takes care of the user session. *******
#******* If the user is inactive for X minutes on the payment page then we ask him to login again***#
#
#

function x_session_timeout($sessid){
	global $XCART_SESSION_VARS, $XCART_SESSION_NAME, $XCARTSESSID , $XCART_SECURE_SESSID;
	global $HTTP_SERVER_VARS,$HTTPS;
	global $cookiedomain;
	$sessionInactiveThreshold = FeatureGateKeyValuePairs::getInteger("sessionInactiveThreshold", 15*60);
	$curtime = time();
	if(!isset($XCART_SESSION_VARS['lastSessionTime'])) {
		$lastSessionTime = $curtime;
	} else {
		$lastSessionTime = $XCART_SESSION_VARS['lastSessionTime'];
	}

	$XCART_SESSION_VARS['lastSessionTime'] = $curtime;

	if(empty($XCART_SESSION_VARS['prompt_login'])) {
		if(($curtime - $lastSessionTime) > $sessionInactiveThreshold){
			$XCART_SESSION_VARS['prompt_login']=1;
		}
		if($HTTPS){
			//check for sxid cookie and if its  valid
			if(!getMyntraCookie('sxid') || getMyntraCookie('sxid')!=$XCART_SESSION_VARS['sxid']){
				$XCART_SESSION_VARS['prompt_login']=1;
			}
		}
	}
}

function change_sessionid(){
	if(!FeatureGateKeyValuePairs::getBoolean("session.changeid.enabled", false)){
		return;
	}
	$sessid = SessionHandler::createSession();
	global $XCARTSESSID ;
	updateSessionBySessionId($XCARTSESSID, time(), time(), x_session_serialize($XCARTSESSID, array()));
	$XCARTSESSID = $sessid;
	global $XCART_SESSION_NAME;
	global $cookiedomain;
	$expiry_time = time()+FeatureGateKeyValuePairs::getInteger("session.expiry.InitialSessionExpirySecs", 600); // 10 minutes expiry on first request
	setMyntraCookie($XCART_SESSION_NAME, $XCARTSESSID, $expiry_time, "/", $cookiedomain, false, true);
}

function session_hijacking_log(){
	if(!FeatureGateKeyValuePairs::getBoolean("session.hijacking.log.enabled", false)){
		return;
	}
	global $XCART_SESSION_VARS,$XCARTSESSID;
	global $authLogger;
	if(!empty($_SERVER['HTTP_USER_AGENT'])){
		if(!empty($XCART_SESSION_VARS["uaString"])&& $_SERVER['HTTP_USER_AGENT'] !=$XCART_SESSION_VARS["uaString"]){
			Profiler::increment("user-agent-mismatch");
			if($authLogger!=null){
				$authLogger->debug("$XCARTSESSID UA mismatch expected {$XCART_SESSION_VARS["uaString"]} actual {$_SERVER['HTTP_USER_AGENT']}");
			}
		}
		$XCART_SESSION_VARS["uaString"]=$_SERVER['HTTP_USER_AGENT'];
	}

	$ip = get_user_ip();
	if(is_ip_valid($ip)){
		if(!empty($XCART_SESSION_VARS["uip"])&& $ip!=$XCART_SESSION_VARS["uip"])
		{
			Profiler::increment("user-ip-mismatch");
			if($authLogger!=null){
				$authLogger->debug("$XCARTSESSID IP mismatch expected {$XCART_SESSION_VARS["uip"]} actual $ip");
			}
		}
		$XCART_SESSION_VARS ["uip"] = $ip;
	}

}
?>
