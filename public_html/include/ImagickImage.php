<?php
/*
* Author: Nishith Shrivastav
* This file is to resize the images to given size.
*/
 
class ImagickImage {
   
   var $image;
 
   function load($filename) {
      $this->image = new Imagick();
      $this->image->readImage($filename);
   }
   function save($filename, $compression=85) {
      if($compression != null){
          $this->image->setImageCompressionQuality($compression);
      }else{
          $this->image->setImageCompressionQuality(100);
      }
      $this->image->stripImage();
      $this->image->writeImage($filename);
   }
   function getWidth() {
      return $this->image->getImageWidth();
   }
   function getHeight() {
      return $this->image->getImageHeight();
   }
   function resizeToHeight($height) {
      $ratio = $height / $this->getHeight();
      $width = $this->getWidth() * $ratio;
      $this->resize($width,$height);
   }
   function resizeToWidth($width) {
      $ratio = $width / $this->getWidth();
      $height = $this->getheight() * $ratio;
      $this->resize($width,$height);
   }
   function scale($scale) {
      $width = $this->getWidth() * $scale/100;
      $height = $this->getheight() * $scale/100; 
      $this->resize($width,$height);
   }
   function resize($width,$height) {
      $this->image->scaleImage($width, $height);
   }      
   function isJpeg() {
      return ( $this->image->getCompression == Imagick::COMPRESSION_JPEG );
   }
}
?>

