<?php
/*****************************************************************************\
+-----------------------------------------------------------------------------+
| X-Cart                                                                      |
| Copyright (c) 2001-2006 Ruslan R. Fazliev <rrf@rrf.ru>                      |
| All rights reserved.                                                        |
+-----------------------------------------------------------------------------+
| PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
| FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
| AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
|                                                                             |
| THIS  AGREEMENT  EXPRESSES  THE  TERMS  AND CONDITIONS ON WHICH YOU MAY USE |
| THIS SOFTWARE   PROGRAM   AND  ASSOCIATED  DOCUMENTATION   THAT  RUSLAN  R. |
| FAZLIEV (hereinafter  referred to as "THE AUTHOR") IS FURNISHING  OR MAKING |
| AVAILABLE TO YOU WITH  THIS  AGREEMENT  (COLLECTIVELY,  THE  "SOFTWARE").   |
| PLEASE   REVIEW   THE  TERMS  AND   CONDITIONS  OF  THIS  LICENSE AGREEMENT |
| CAREFULLY   BEFORE   INSTALLING   OR  USING  THE  SOFTWARE.  BY INSTALLING, |
| COPYING   OR   OTHERWISE   USING   THE   SOFTWARE,  YOU  AND  YOUR  COMPANY |
| (COLLECTIVELY,  "YOU")  ARE  ACCEPTING  AND AGREEING  TO  THE TERMS OF THIS |
| LICENSE   AGREEMENT.   IF  YOU    ARE  NOT  WILLING   TO  BE  BOUND BY THIS |
| AGREEMENT, DO  NOT INSTALL OR USE THE SOFTWARE.  VARIOUS   COPYRIGHTS   AND |
| OTHER   INTELLECTUAL   PROPERTY   RIGHTS    PROTECT   THE   SOFTWARE.  THIS |
| AGREEMENT IS A LICENSE AGREEMENT THAT GIVES  YOU  LIMITED  RIGHTS   TO  USE |
| THE  SOFTWARE   AND  NOT  AN  AGREEMENT  FOR SALE OR FOR  TRANSFER OF TITLE.|
| THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY GRANTED BY THIS AGREEMENT.      |
|                                                                             |
| The Initial Developer of the Original Code is Ruslan R. Fazliev             |
| Portions created by Ruslan R. Fazliev are Copyright (C) 2001-2006           |
| Ruslan R. Fazliev. All Rights Reserved.                                     |
+-----------------------------------------------------------------------------+
\*****************************************************************************/

#
# $Id: history_order.php,v 1.32 2006/01/16 06:47:33 mclap Exp $
#
# Collect infos about ordered products
#

if ( !defined('XCART_SESSION_START') ) { header("Location: ../"); die("Access denied"); }

x_load('order');

if (empty($mode)) $mode = "";

if(!empty($orderid))
{

	$order_data = func_order_data($orderid);

	if (empty($order_data))
	    	return false;

	#
	# Security check if order owned by another customer
	#
	if ($current_area == 'C' && $order_data["userinfo"]["login"] != $login) {
		func_header_location("error_message.php?access_denied&id=35");
	}

	$smarty->assign("order_details_fields_labels", func_order_details_fields_as_labels());
	$smarty->assign("order", $order_data["order"]);
	$smarty->assign("customer", $order_data["userinfo"]);
	$order_data["products"] = func_translate_products($order_data["products"], ($order_data["userinfo"]['language']?$order_data["userinfo"]['language']:$config['default_customer_language']));
	$smarty->assign("products", $order_data["products"]);
	$smarty->assign("giftcerts", $order_data["giftcerts"]);
	if ($order_data)
	{
		$owner_condition = "";
		if ($current_area == "C")
			$owner_condition = " AND $sql_tbl[orders].login='".$login."'";
		elseif ($current_area == "P" && !$single_mode ) {
			$owner_condition = " AND $sql_tbl[order_details].provider='".$login."'";
		}
		# find next
		$tmp = func_query_first("SELECT $sql_tbl[orders].orderid FROM $sql_tbl[orders], $sql_tbl[order_details] WHERE $sql_tbl[orders].orderid>'".$orderid."' AND $sql_tbl[order_details].orderid=$sql_tbl[orders].orderid $owner_condition ORDER BY $sql_tbl[orders].orderid ASC LIMIT 1");
		if (!empty($tmp["orderid"]))
			$smarty->assign("orderid_next", $tmp["orderid"]);
		# find prev
		$tmp = func_query_first("SELECT $sql_tbl[orders].orderid FROM $sql_tbl[orders], $sql_tbl[order_details] WHERE $sql_tbl[orders].orderid<'".$orderid."' AND $sql_tbl[order_details].orderid=$sql_tbl[orders].orderid $owner_condition ORDER BY $sql_tbl[orders].orderid DESC LIMIT 1");

	   # get the details of products

	   $orderDetail = "SELECT FROM_UNIXTIME($sql_tbl[orders].date, '%d-%m-%Y') as date, $sql_tbl[orders].payment_method as payment_method,
	   $sql_tbl[orders].status as payment_status, $sql_tbl[order_details].amount AS items ,
	   $sql_tbl[orders].shipping_method as shipping_method
	   FROM $sql_tbl[orders]
	   INNER  JOIN $sql_tbl[order_details] ON $sql_tbl[orders].orderid = $sql_tbl[order_details].orderid
	   WHERE  $sql_tbl[orders].orderid = '".$orderid."'";
	   $orderresult = db_query($orderDetail);
	   $orderRow = db_fetch_array($orderresult);
	  	$smarty->assign("date", $orderRow['date']);
		$smarty->assign("payment_method", $orderRow['payment_method']);
		$smarty->assign("shipping_method", $orderRow['shipping_method']);
		$smarty->assign("payment_status", $orderRow['payment_status']);
		$smarty->assign("items", $orderRow['items']);

		$smarty->assign("orderid", $_GET['orderid']);

		$productsInCart = func_query("SELECT product, productid, product_style, product_type, price, amount, subtotal, total, $sql_tbl[order_details].discount AS productDiscount, $sql_tbl[orders].discount AS totaldiscountonProducts, coupon_discount FROM $sql_tbl[order_details], $sql_tbl[orders] WHERE $sql_tbl[order_details].orderid = $sql_tbl[orders].orderid AND $sql_tbl[order_details].orderid = '".$_GET['orderid']."'");


foreach($productsInCart as $key => $order) {


//Query for retriving the style name from mk_product_style
$stylename = "SELECT name FROM $sql_tbl[mk_product_style] WHERE id = ".$productsInCart[$key]['product_style']."";
$styleresult = db_query($stylename);
$stylerow = db_fetch_array($styleresult);
$productsInCart[$key]['productStyleName'] = $stylerow['name'];

//Query for retriving the style options from mk_product_options
$styleoption = "SELECT $sql_tbl[mk_product_options].name AS optionName, $sql_tbl[mk_product_options].value AS optionValue FROM $sql_tbl[mk_product_options], $sql_tbl[mk_product_options_order_details_rel]
WHERE $sql_tbl[mk_product_options].id = $sql_tbl[mk_product_options_order_details_rel].option_id
AND $sql_tbl[mk_product_options].style = ".$productsInCart[$key]['product_style']."
AND $sql_tbl[mk_product_options_order_details_rel].orderid = '".$_GET['orderid']."'
AND $sql_tbl[mk_product_options_order_details_rel].itemid = ".$productsInCart[$key]['productid']."";

$optionresult = db_query($styleoption);
$i = 0;
while($optionrow = db_fetch_array($optionresult))
{
	$productsInCart[$key]['OptionNames'][$i] =  $optionrow['optionName'];
    $productsInCart[$key]['OptionValues'][$i] =  $optionrow['optionValue'];
	$i++;
}

$custArea = "SELECT ca.name AS DesignName, ca.label AS Designlabel, cia.area_image_final AS DesignImage,
					co.location_name AS Ori_Name
					FROM $sql_tbl[mk_style_customization_area] ca,
					$sql_tbl[mk_xcart_order_customized_area_image_rel] cia,
					$sql_tbl[mk_customization_orientation] co,
					$sql_tbl[mk_xcart_order_details_customization_rel] odc
					WHERE  ca.id = cia.area_id
					AND ca.style_id = cia.style_id
					AND ca.id = co.area_id
					AND odc.orientation_id = co.id
					AND odc.orderid = 'cia.orderid'
					AND odc.itemid = cia.item_id
					AND cia.style_id= ".$productsInCart[$key]['product_style']."
					AND cia.item_id = ".$productsInCart[$key]['productid']."
					AND cia.orderid = '".$_GET['orderid']."'";

$custresult = db_query($custArea);
$j = 0;
while($custrow = db_fetch_array($custresult))
{
	$productsInCart[$key]['Designlabel'][$j] =  $custrow['Designlabel'];
	$productsInCart[$key]['DesignImage'][$j] =  $custrow['DesignImage'];
	$productsInCart[$key]['OriName'][$j] =  $custrow['Ori_Name'];
	$j++;
}



$typename = "SELECT name, label, image_t FROM $sql_tbl[producttypes] WHERE id = ".$productsInCart[$key]['product_type']."";

$typeresult = db_query($typename);
$row = db_fetch_array($typeresult);

$productsInCart[$key]['product_type'] = $row['name'];
$productsInCart[$key]['productTypeLabel'] = $row['label'];
$productsInCart[$key]['designImagePath'] = $row['image_t'];
$productsInCart[$key]['totalPrice'] = $productsInCart[$key]['price'] * $productsInCart[$key]['amount'];

}

$smarty->assign("productsInCart", $productsInCart);

		if (!empty($tmp["orderid"]))
			$smarty->assign("orderid_prev", $tmp["orderid"]);
	}
}

$location[] = array(func_get_langvar_by_name("lbl_orders_management"), "orders.php");
$location[] = array(func_get_langvar_by_name("lbl_order_details_label"), "");



?>
