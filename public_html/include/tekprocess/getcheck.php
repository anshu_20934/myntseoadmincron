<?php
chdir(dirname(__FILE__));
class TekProcessPaymentProcess {
	private static $isPaymentDataCorrect=false;
	private static $isPaymentCompete=false;
	
	private static function getPropertyData() {
		global $https_location;
	    $property = array("BillerId"=>"L1664",
	    					"ResponseUrl"=>"$https_location/mkorderBook.php",
	    					"CRN"=>"INR",
	    					"CheckSumKey"=>"5835571401KHXYYB",
	    					"CheckSumGenUrl"=>"https://www.tpsl-india.in/PaymentGateway/CheckSumRequest",
	    					"TPSLUrl"=>"https://www.tpsl-india.in/PaymentGateway/TransactionRequest.jsp");
		
		return $property;
	                
	}

	public static function getFormToSubmit($orderID,$txnAmount,$bankCode) {
		global $https_location,$configMode;
		$property_data_array=self::getPropertyData();
		if($configMode=="release"){
				$orderID = "R".trim($orderID);
		} elseif($configMode=="local") {
			$orderID = "L".trim($orderID);
		} else if($configMode=="test") {
			$orderID = "T".trim($orderID);
		}
		
		$txtBillerId=$property_data_array["BillerId"];
		$txtResponseUrl=$property_data_array["ResponseUrl"];
		$txtCRN=$property_data_array["CRN"];
		$txtCheckSumKey=$property_data_array["CheckSumKey"];
		$CheckSumGenUrl=trim($property_data_array["CheckSumGenUrl"]);
		$TPSLUrl=$property_data_array["TPSLUrl"];
		
		//---Create String using property data 
		$txtBillerIdStr="txtBillerid=".trim($txtBillerId)."&";
		$txtResponseUrl="txtResponseUrl=".trim($txtResponseUrl)."&";
		$txtCRN="txtCRN=".trim($txtCRN)."&";
		$txtCheckSumKey="txtCheckSumKey=".trim($txtCheckSumKey);
						
		$Proper_string=$txtBillerIdStr.$txtResponseUrl.$txtCRN.$txtCheckSumKey;
		
		//Encrypting values
		$transaction = trim($orderID);
		$market = trim($orderID);
		$account = "1";
		$transaction_amount = trim($txnAmount);
		$bankCode = trim($bankCode);
		
		$txtVals = $transaction . $market . $account . $transaction_amount . $bankCode;
		$txt_encrypt = md5(base64_encode($txtVals));
		
		
		$txtForEncode = trim($txt_encrypt) . trim($property_data_array["CheckSumKey"]);
		$txtPostid = md5($txtForEncode);
		
		// Creating string for $Postid.
		$txtPostid="txtPostid=".$txtPostid; 
		
		$UserDataString="txtTranID=$transaction&txtMarketCode=$market&txtAcctNo=$account&txtTxnAmount=$transaction_amount&txtBankCode=$bankCode";
		$PostData=trim($UserDataString)."&".trim($Proper_string)."&".trim($txtPostid);

		//echo "$PostData<br/>";
		//-----Curl main Function Start
 		//$PostData =array("txtTranID"=>$transaction,"txtMarketCode"=>$market,"txtAcctNo"=>$account,
 		//					"txtTxnAmount"=>$transaction_amount,"txtBankCode"=>$bankCode,"txtBillerid"=>$txtBillerId,
 		//					"txtResponseUrl"=>$txtResponseUrl,"txtCRN"=>$txtCRN,"txtCheckSumKey"=>$txtCheckSumKey,"txtPostid"=>$txtPostid);
		// INITIALIZE ALL VARS
		$ch = curl_init($CheckSumGenUrl);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, False);
		curl_setopt($ch, CURLOPT_CAINFO, getcwd() . '/keystoretechp.pem'); //Setting certificate path
		curl_setopt ($ch, CURLOPT_SSLCERTPASSWD, 'changeit');
		curl_setopt($ch, CURLOPT_POST      ,1);
		curl_setopt($ch, CURLOPT_TIMEOUT  ,10); // Not required. Don't use this.Mitesh -- using this for safety
		curl_setopt($ch, CURLOPT_REFERER  ,"$https_location/mkorderinsert.php"); //Setting header URL: 
		curl_setopt($ch, CURLOPT_POSTFIELDS    ,$PostData);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION  ,1); 
		curl_setopt($ch, CURLOPT_HEADER      ,0);  // DO NOT RETURN HTTP HEADERS 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER  ,1); // RETURN THE CONTENTS OF THE CALL
		 
		$Received_CheckSum_Data = curl_exec($ch);
		curl_close($ch);
		
		$Received_CheckSum_Data = trim($Received_CheckSum_Data);
		if(empty($Received_CheckSum_Data)) return false;
		//echo "<br>CheckSum=".$Received_CheckSum_Data;
		if(!is_numeric($Received_CheckSum_Data)){ // Validating whether the Receieved value is numeric.
			return false;		
		}
		
		//re-defining the variables without & and variables.
		
		$txtBillerIdStr=$txtBillerId;
		$txtResponseUrl=$property_data_array["ResponseUrl"];
		$txtCRN=$property_data_array["CRN"];
		$txtCheckSumKey=$property_data_array["CheckSumKey"];
		
		//################# read post data to variable ###############

		$txtTranID=$transaction;
		$txtMarketCode=$market;
		$txtAcctNo=$account;
		$txtTxnAmount=$transaction_amount;
		$txtBankCode=$bankCode;

		//################# read post data end ###############

		//###########  Create msg String ###########################
 		$ParamString=trim($txtBillerId)."|".trim($txtTranID)."|NA|NA|".trim($txtTxnAmount)."|".trim($txtBankCode)."|NA|NA|".trim($txtCRN)."|NA|NA|NA|NA|NA|NA|NA|".trim($txtMarketCode)."|".trim($txtAcctNo)."|NA|NA|NA|NA|NA|".trim($txtResponseUrl);

 		$finalUrlParam=$ParamString."|".$Received_CheckSum_Data; 

		 //----Create dyanamic form and submit to payment gatway
		//echo "here";
		return "
			 	<form id='subFrm' name='subFrm' method='post' action='".$TPSLUrl."'>
					<input type='hidden' name='msg' value='".$finalUrlParam."'>
				</form>
				<script>
					document.subFrm.submit();
				</script>
			  ";		
	}
	
	public static function isPaymentDataCorrect(){
		return self::$isPaymentDataCorrect;
	}
	
	public static function isPaymentCompleted(){
		return self::$isPaymentCompete;
	}
	
	public static function verifyPaymentData($returnedData,$amountToBepaid) {
		global $https_location;
		if(!is_array($returnedData)){
			return false;
		}
		
		$property_data_array=self::getPropertyData();
		$txtBillerId=$property_data_array["BillerId"];
		$txtResponseUrl=$property_data_array["ResponseUrl"];
		$txtCRN=$property_data_array["CRN"];
		$txtCheckSumKey=$property_data_array["CheckSumKey"];
		$CheckSumGenUrl=trim($property_data_array["CheckSumGenUrl"]);
		$TPSLUrl=$property_data_array["TPSLUrl"];
		$txtResponseKey = $returnedData[0] ."|".$returnedData[1] ."|".$returnedData[2] ."|".$returnedData[3] ."|".$returnedData[4] ."|".$returnedData[5] ."|".$returnedData[6] ."|".$returnedData[7] ."|".$returnedData[8] ."|".$returnedData[9] ."|".$returnedData[10] ."|".$returnedData[11] ."|".$returnedData[12] ."|".$returnedData[13] ."|".$returnedData[14] ."|".$returnedData[15] ."|".$returnedData[16] ."|".$returnedData[17] ."|".$returnedData[18] ."|".$returnedData[19] ."|".$returnedData[20] ."|".$returnedData[21] ."|".$returnedData[22] ."|".$returnedData[23] ."|".$returnedData[24] ."|".$txtCheckSumKey;	
		$txtResponseKey = "txtResponseKey=" . $txtResponseKey;
		
		$ch = curl_init($CheckSumGenUrl);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 1);
 		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, False);
 		curl_setopt($ch, CURLOPT_CAINFO, getcwd() . '/keystoretechp.pem'); //Setting certificate path
 		curl_setopt ($ch, CURLOPT_SSLCERTPASSWD, 'changeit');
 		curl_setopt($ch, CURLOPT_POST      ,1);
		curl_setopt($ch, CURLOPT_TIMEOUT  ,20); // Not required. Don't use this. Mitesh -- using this for safety
 		curl_setopt($ch, CURLOPT_REFERER  ,"$https_location/mkorderBook.php"); //Setting header URL
 		curl_setopt($ch, CURLOPT_POSTFIELDS    ,$txtResponseKey);
 		curl_setopt($ch, CURLOPT_FOLLOWLOCATION  ,1); 
 		curl_setopt($ch, CURLOPT_HEADER      ,0);  // DO NOT RETURN HTTP HEADERS 
 		curl_setopt($ch, CURLOPT_RETURNTRANSFER  ,1); // RETURN THE CONTENTS OF THE CALL
 
		$Received_CheckSum_Data = curl_exec($ch);
		curl_close($ch);

		$Received_CheckSum_Data = trim($Received_CheckSum_Data);
		
		
		if(empty($Received_CheckSum_Data)) {
			//cant verify the validity of data so will fail the payment for now.
			self::$isPaymentCompete=false;
			self::$isPaymentDataCorrect=true;
			return false;
		}
		//echo "<br>CheckSum=".$Received_CheckSum_Data;
		if(!is_numeric($Received_CheckSum_Data)){ // Validating whether the Receieved value is numeric.
			//some error occured with checksum veryfication server.
			self::$isPaymentCompete=false;
			self::$isPaymentDataCorrect=true;
			return false;		
		}
		
		$billerID = trim($returnedData[0]);
		$orderID = trim($returnedData[1]);
		$tpslTransactionID = trim($returnedData[2]);
		$bankTransactionID = trim($returnedData[3]);
		$amountPaid = trim($returnedData[4]);
		$bankID = trim($returnedData[5]);
		$txnDate = trim($returnedData[13]);
		$authStatus = trim($returnedData[14]);
		$marketCode = trim($returnedData[16]);
		$account = trim($returnedData[17]);
		$errorDescription = trim($returnedData[24]);
		$checkSum = trim($returnedData[25]);
		if($Received_CheckSum_Data == $checkSum) {
			if($authStatus=='0300' && $amountPaid > $amountToBepaid -1 ) {
				//pass
				self::$isPaymentCompete=true;
				self::$isPaymentDataCorrect=true;
				return true;
			} else {
				//fail
				self::$isPaymentCompete=false;
				self::$isPaymentDataCorrect=true;
				return false;
			}
		} else {
			//data tampered fraud :D
			self::$isPaymentCompete=false;
			self::$isPaymentDataCorrect=false;
			return false;
		}
		
	}
}

?>