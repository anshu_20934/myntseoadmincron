<?php
/*****************************************************************************\
+-----------------------------------------------------------------------------+
| X-Cart                                                                      |
| Copyright (c) 2001-2006 Ruslan R. Fazliev <rrf@rrf.ru>                      |
| All rights reserved.                                                        |
+-----------------------------------------------------------------------------+
| PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
| FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
| AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
|                                                                             |
| THIS  AGREEMENT  EXPRESSES  THE  TERMS  AND CONDITIONS ON WHICH YOU MAY USE |
| THIS SOFTWARE   PROGRAM   AND  ASSOCIATED  DOCUMENTATION   THAT  RUSLAN  R. |
| FAZLIEV (hereinafter  referred to as "THE AUTHOR") IS FURNISHING  OR MAKING |
| AVAILABLE TO YOU WITH  THIS  AGREEMENT  (COLLECTIVELY,  THE  "SOFTWARE").   |
| PLEASE   REVIEW   THE  TERMS  AND   CONDITIONS  OF  THIS  LICENSE AGREEMENT |
| CAREFULLY   BEFORE   INSTALLING   OR  USING  THE  SOFTWARE.  BY INSTALLING, |
| COPYING   OR   OTHERWISE   USING   THE   SOFTWARE,  YOU  AND  YOUR  COMPANY |
| (COLLECTIVELY,  "YOU")  ARE  ACCEPTING  AND AGREEING  TO  THE TERMS OF THIS |
| LICENSE   AGREEMENT.   IF  YOU    ARE  NOT  WILLING   TO  BE  BOUND BY THIS |
| AGREEMENT, DO  NOT INSTALL OR USE THE SOFTWARE.  VARIOUS   COPYRIGHTS   AND |
| OTHER   INTELLECTUAL   PROPERTY   RIGHTS    PROTECT   THE   SOFTWARE.  THIS |
| AGREEMENT IS A LICENSE AGREEMENT THAT GIVES  YOU  LIMITED  RIGHTS   TO  USE |
| THE  SOFTWARE   AND  NOT  AN  AGREEMENT  FOR SALE OR FOR  TRANSFER OF TITLE.|
| THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY GRANTED BY THIS AGREEMENT.      |
|                                                                             |
| The Initial Developer of the Original Code is Ruslan R. Fazliev             |
| Portions created by Ruslan R. Fazliev are Copyright (C) 2001-2006           |
| Ruslan R. Fazliev. All Rights Reserved.                                     |
+-----------------------------------------------------------------------------+
\*****************************************************************************/

#
# $Id: partner_info.php,v 1.22 2006/01/11 06:55:59 mclap Exp $
#

if ( !defined('XCART_SESSION_START') ) { header("Location: ../"); die("Access denied"); }

x_session_register ("partner");
x_session_register ("partner_clickid");

$_tmp_current_host = $xcart_http_host;
$_tmp = parse_url($current_location);
if (!empty($_tmp['host']))
	$_tmp_current_host = $_tmp['host'];

$partner_cookie_content = getMyntraCookie("partner");
$partner_time_cookie_content =  getMyntraCookie("partner_time");

	
if (empty($partner) && !empty($HTTP_GET_VARS["partner"])) {
	#
	# Assign current partner value
	#
	$partner = $HTTP_GET_VARS ["partner"];
	#
	# Users has clicked onto banner
	#
	db_query ("INSERT INTO $sql_tbl[partner_clicks] (login, add_date, class, productid, bannerid, referer) VALUES ('$partner', '".time()."', '$cl', '$productid', '$bid', '$HTTP_REFERER')");
	$partner_clickid = db_insert_id();
	#
	# Set cookies
	#
	$partner_cookie_length = ($config["XAffiliate"]["partner_cookie_length"] ? $config["XAffiliate"]["partner_cookie_length"]*3600*24 : 0);

	if ($partner_cookie_length) {
		$expiry = mktime(0,0,0,date("m"),date("d"),date("Y")+1);
		setMyntraCookie("partner_clickid", $partner_clickid, $expiry, "/", $cookiedomain, 0);
		setMyntraCookie("partner", $partner, $expiry, "/", $cookiedomain, 0);
		setMyntraCookie("partner_time", time()+$partner_cookie_length, $expiry, "/", $cookiedomain, 0);
	}
}
elseif (empty($partner) && !empty($partner_cookie_content) && !empty($partner_time_cookie_content)) {
	if ($partner_time_cookie_content >= time()) {
		#
		# Assign current partner value
		#
		$partner = $partner_cookie_content;
		$partner_clickid = getMyntraCookie("partner_clickid");
	} else {
		#
		# Remove cookies if $partner_cookie_length is expired
		#
		setMyntraCookie("partner", "", 0, "/", $cookiedomain, 0);
		setMyntraCookie("partner_clickid", "", 0, "/", $cookiedomain, 0);
		setMyntraCookie("partner_time", "", 0, "/", $cookiedomain, 0);
	}
}
?>
