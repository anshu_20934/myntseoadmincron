<?php


class PortalNodeInterface {

	public static function performGetCurlCall($url){
		global $login;
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch,CURLOPT_TIMEOUT,30); //30 sec time out
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,0);
		$response = curl_exec($ch);
        $info =  curl_getinfo($ch,CURLINFO_HTTP_CODE);
        curl_close($ch);
        if($info !== 200 || empty($response)){
            return null;
        }
		return $response;
	}

	public static function  getCachedNodeHeaderFooter($login){
		global $xcache ;
		$state = empty($login)?'loggedout':'loggedin';
		$url = \HostConfig::$baseUrl.'api/headerfooter?loggedin='.(empty($login)?'false':'true');
	    $nodeHeaderFooterHTML = $xcache->fetchAndStore(
	   		function($url){
	        	return PortalNodeInterface::performGetCurlCall($url);
			},
			array($url),
			"nodeapi:".$state."headerfooter",
			1800
		);
	    return $nodeHeaderFooterHTML;
	}
}
