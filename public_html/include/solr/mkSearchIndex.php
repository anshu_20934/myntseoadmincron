<?php	
	try
	{		
		$index = $_POST["comboSearch"];
		$queries = array();
		if($_POST['txtSearch'])
		{
			$sterms = explode(" ",preg_replace("/[^a-zA-Z0-9s-]/", " ", $_POST['txtSearch']));
			$count = count($sterms);		
			$temp = "";			
			for($i=0;$i<$count;$i++)
			{	
				$s = $sterms[$i];
				if($s != "" )
				{
					$queries[0] .= "(keywords:$s OR stylename:$s OR product:$s OR descr:$s OR fulldescr:$s) ";
					$temp = $temp . " $s";
				}
				if($i != $count-1)
					$queries[0] .= " AND ";
			}
			if($count != 1)
			{
				$s = $temp;
				$queries[0] .= "AND (keywords:$s OR stylename:$s OR product:$s OR descr:$s OR fulldescr:$s)";
			}
		}
		else
			Header('Location:mynIndex.php?ec=opf'); 
		//print_r($queries);	
		//exit;
		switch($index)
		{
			case 'p' :
							include_once('solrProducts.php'); 
							$solrIndex = new solrProducts();
							break;
			case 'ps':
							include_once('solrProductStyles.php');
							$solrIndex = new solrProductStyles();
							break;
			case 's'	:
							include_once('solrShops.php');
							$solrIndex = new solrShops();
							break;
			case 'd'	:
							include_once('solrDesigners.php');
							$solrIndex = new solrDesigners();
							break;
			default :
							Header('Location:mynIndex.php?ec=opf');
		}
		$solrIndex->searchAndSort($queries,0,50,"styleprice asc");				
	}
	catch(Exception $ex)
	{
		Header('Location:mynIndex.php?ec=ex');
	}
?>
