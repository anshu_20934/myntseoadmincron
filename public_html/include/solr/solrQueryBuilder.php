<?php
/**
 * Created by IntelliJ IDEA.
 * User: Mohammed Asif Hussain
 * Date: Dec 21, 2009
 * Time: 10:49:13 AM
 * To change this template use File | Settings | File Templates.
 *
 * TODO - class is not been fully tested yet .. If u find bugs .. Please report ..
 */

class solrQueryBuilder {

    var $query = '';

    var $current_query = '';

    var $filters  = array();

    var $or_filters  = array();

    var $range_filters  = array();

    public function __construct(){

    }

    public function create_query() {
        $this->current_query = '';     
    }

    public function end_query() {
        $this->query .= '('.$this->get_current_query().')';
        $this->clear();
    }
    
    public function append_to_query($appender) {
        $this->current_query .= " " . $appender. " ";
    }

    public function set_clause($clause) {
        $this->query .=' '.$clause;  
    }

    public function add_filter($field, $value, $required = false){
        $this->filters[] = array($field => ($required ? '+' : '').$value);
    }

    public function add_or_filter($field, $value, $required = false){
        $this->or_filters[] = array($field => ($required ? '+' : '').$value);
    }

    public function add_range_filter($field, $start, $end){
        $this->range_filters[] =  $field .':['.$start.' TO '. $end.']';
    }

    public function add_term($phrase, $fields, $field_boasts = NULL, $required_all = false, $operator = 'OR', $enclosing_brackets = true, $isCompletePhrase = false, $isPhoneticSearchOn = false, $phoneticSearchFields = NULL){
        $phrase = trim($phrase);
        if (!empty($phrase)) {
        	if(!$isCompletePhrase)
            	$terms = split(' ',$phrase );
            else 
            {
            	$terms = Array();
            	if(strpos($phrase, " ") !== false)
            		$phrase = "\"" .$phrase . "\"";
            	array_push($terms, $phrase);
            }

            $field_index  = 0;

            if($enclosing_brackets === true)
            	$this->current_query.='+(';
            foreach( $fields as $field){
                if($field_index > 0 ){
                    $this->current_query .= $operator.' ';
                }
                $this->current_query .= $field.' :( ';
                foreach($terms as $term){
                    $term = trim($term);
                    if(!empty($term )){
                        $boast ='';
                        if(!empty($field_boasts)){
                            $boast = $field_boasts[$field];
                            if(!empty($boast)){
                                $boast = '^'.$boast;
                            }
                        }
                        $phonetic = false;
                        if($isPhoneticSearchOn && !empty($phoneticSearchFields))
                        {
                        	if($phoneticSearchFields[$field] == 1)
                        		$phonetic = true;		
                        	
                        }
                        if($phonetic && strpos($term, " ") === false)
                        	$this->current_query .=  ($required_all ? '+' : '').$term.'~'.$boast.' ';
                        else
                        	$this->current_query .=  ($required_all ? '+' : '').$term.$boast.' ';
                    }
                }
                $this->current_query .= ' ) ';
                $field_index++;
            }
            if($enclosing_brackets === true)
            	$this->current_query.=')';
        }
    }
    
    public function add_specific_terms($array_phrase, $field, $field_boast){
    	$specific_subquery = '';
    	
    	
		foreach ($array_phrase as $phrase){
    		if (!empty($phrase)) {
                if(!empty($field_boast)){
			if(empty($specific_subquery))
				$specific_subquery .= $field.' :( ';
			else
				$specific_subquery .= ' OR ';
                	$boast = '^'.$field_boast;
                	$specific_subquery .=  $phrase.$boast;
                }
    		}
		}
	if(!empty($specific_subquery))
    		$specific_subquery .= ")";
    	if(!empty($this->current_query))
    		$this->current_query .= ' AND ' . $specific_subquery;
    	else
    		$this->current_query = $specific_subquery;
    }

    public function add_phrase($phrase, $fields, $required_all = false){
        foreach( $fields as $field){
            $this->current_query .= $field.' :( ';
            $this->current_query .=  ($required_all ? '+' : '').' "'.$phrase.'" ';
            $this->current_query .= ' ) ';
        }
    }

    public function query_added(){
        return !empty($this->current_query) || !empty($this->filters) || !empty($this->or_filters) || !empty($this->range_filters) ;
    }

    function get_current_query() {
        if(!empty($this->filters)){
            if(!empty($this->current_query)){
                $this->current_query.= ' AND ';
            }
            $filter_index = 0 ;
            foreach($this->filters as $filter){
                if($filter_index > 0){
                    $this->current_query.= ' AND ';
                }
                foreach($filter as $field => $value){
                    $this->current_query.= '('. $field.':'.$value.')';
                    $filter_index++;
                }
            }
        }

        if(!empty($this->or_filters)){
            if(!empty($this->current_query)){
                $this->current_query .= ' AND';
            }            
            $this->current_query .= ' (';
            $filter_index = 0 ;
            foreach($this->or_filters as $filter){
                if($filter_index > 0){
                    $this->current_query.= ' OR ';
                }
                foreach($filter as $field => $value){
                    $this->current_query.= '('. $field.':'.$value.')';
                    $filter_index++;
                }
            }
            $this->current_query .= ')';
        }
        
        if(!empty($this->range_filters)){
            if(!empty($this->current_query)){
                $this->current_query.= ' AND ';
            }
            $filter_index = 0 ;
            foreach($this->range_filters as $filter){
                if($filter_index > 0){
                    $this->current_query.= ' AND ';
                }
                $this->current_query.= $filter;
                $filter_index++;
            }
        }

        return $this->current_query;
    }

    function clear(){
        $this->filters = array();
        $this->or_filters = array();
        $this->range_filters = array();
    }

    function get_query() {
        if(empty($this->query)) {
            return $this->get_current_query();
        } else {
            return $this->query;
        }
    }
}
