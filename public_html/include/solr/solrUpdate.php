<?php

function deleteShopMaster($shopMasterId) {
    try
    {
        $solrProducts = new solrProducts();
        $solrProducts->emptyIndex($shopMasterId);

    } catch (Exception $ex) {

    }
}


function addShopMaster($shopMasterId) {
    try
    {
        $solrProducts = new solrProducts();
        $solrProducts->indexProducts($shopMasterId);

    }
    catch (Exception $ex)
    {
        print_r($ex);
    }
}


function addProductToSolrIndex($productid) {
    try
    {
        $solrProducts = new solrProducts();
        $solrProducts->addProductToIndex($productid);

    }
    catch (Exception $ex)
    {
    }
}

function addStyleToSolrIndex($styleid,$commitToSolr=true, $flush=true, $callApi=true) {
    try
    {
        $solrProducts = new solrProducts();
        $solrProducts->addStyleToIndex($styleid, $flush, $callApi);
    } catch (Exception $ex) {

    }
}

function deleteProductFromSolrIndex($productid) {
    try
    {
        $solrProducts = new solrProducts();
        $solrProducts->deleteProductFromIndex($productid);
    } catch (Exception $ex) {

    }
}

function deleteStyleFromSolrIndex($styleid,$commitToSolr=true) {
    $doc_id = "0" . "_style_" . $styleid;
    try
    {
        $solrProducts = new solrProducts();
        $solrProducts->deleteDocumentFromIndex($doc_id,$commitToSolr);
    } catch (Exception $ex) {

    }
}

?>
