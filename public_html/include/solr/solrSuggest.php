<?php
include_once('../../auth.php');
require_once( 'Apache/Solr/Service.php' );
require_once($xcart_dir."/include/class/class.feature_gate.keyvaluepair.php");
include_once('solrCommon.php');
include_once('solrUtils.php');

/**
 * solrSuggest 
 * 
 * @uses solrCommon
 * @author Ashish Lohiya <ashish.lohiya@myntra.com> 
 */
class solrSuggest extends solrCommon {

    var $table = 'landing_page';
    var $core  = 'suggest';

	public function __construct($hostname=null)
	{
		if(empty($hostname) && !empty(SOLRConfig::$searchhost)){
			$hostname=SOLRConfig::$searchhost;	
		}else if(empty($hostname)){
			$hostname="localhost";	
		}
		parent::__construct($hostname,$this->core);
	}

    function indexProducts(){
    	$orderMap = (OBJECT)json_decode(FeatureGateKeyValuePairs::getFeatureGateValueForKey("categoryLandingOrder"));
        $offset = 0 ;
        $limit = 2000;
        $documents = $this->get_suggest_documents($offset, $limit,$orderMap);
        while(!empty($documents)) {
             $this->update($documents);
             $offset += $limit;
             $documents = $this->get_suggest_documents($offset, $limit,$orderMap); 
        }
        //$this->optimizeIndex();
        $this->commitToServers();
    }


	public function commitToServers(){
		global $solr_servers;
		foreach ($solr_servers as $server) {
			$solr = new solrSuggest($server);
			$solr->commitIndex();
		}
	}

    function get_suggest_documents($offset  = NULL, $limit = NULL,$orderMap){
        $query = "select * from ".$this->table." limit $limit offset $offset";
        $results = func_query($query, true);
        if(!empty($results)){
            $documents = array();
            foreach($results as $result){
                $doc = $this->get_document($result,$orderMap);
                if(!empty($doc)){
                    $documents[] = $doc; 
                }
            }
        }
        return $documents;
    }

    /**
     * @param  $row_data
     * @return void
     */
     function update($documents){
         global $solr_servers;
         foreach ($solr_servers as $server) {
             $solr = new solrSuggest($server);
             $solr->loadDocs($documents);
         }
    }

    /**
     *
     * @param  $id
     * @return void
     */
    function remove_document($id){
        global $solr_servers;
        foreach ($solr_servers as $server) {
            echo "{removing for id ::".$id."},";
            $solr = new solrSuggest($server);
            $this->removeById($id);
        }
    }

    /**
     * @param  $result
     * @return Apache_Solr_Document
     */
    function get_document($result,$orderMap){
        if($result['is_autosuggest'] == 0){
            $this->remove_document($result['id']);
            return null;
        }
        echo $result['id'].",";
        $document = new Apache_Solr_Document();
        addValue($document,'id',$result['id']);
        if(!empty($result['alt_text'])){
            addValue($document,'keyword', str_replace("-"," ",$result['alt_text']));
            addValue($document,'keyword_edgy', str_replace("-"," ",$result['alt_text']));
        }else{
            addValue($document,'keyword', str_replace("-"," ",$result['page_url']));
            addValue($document,'keyword_edgy', str_replace("-"," ",$result['page_url']));
        }
        addValue($document,'search_keyword', str_replace("-"," ",$result['page_url']));
        addValue($document,'count_of_products',$result['number_of_results']);
        addValue($document,'count_of_searches',1);
        $category = $result['category'];
        if(!empty($category))
        {
        	addValue($document,'category',$category);
        }
        else
        {
        	addValue($document,'category',"All Others");
        }
        if(!empty($orderMap->$category))
        {
        	addValue($document,'relevance_order',(int)$orderMap->$category);
        }
        else
        {
        	addValue($document,'relevance_order',0);
        }
        return  $document;
    }
}
