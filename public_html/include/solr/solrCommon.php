<?php
require_once('Apache/Solr/Service.php' );
require_once('Apache/Solr/Response.php' );
require_once('Apache/Solr/Document.php' );
class SolrCommon
{
	protected $solr;
	private $core;
	
	public function __construct($hostname,$core)
	{
		global $solr_port;
		$this->solr = new Apache_Solr_Service($hostname, $solr_port,"/solr/$core");
	}
	
	public function getApacheSolrService(){
		return $this->solr;
	}
	public function optimizeIndex()
	{
		$this->solr->optimize();
	}
	public function emptyIndex($shopMasterId="")
	{
		if( empty($shopMasterId) )
		$query = "*:*";
		else
		$query = "shopmasterid:$shopMasterId";
		$solr = &$this->solr;
		$solr->deleteByQuery($query); //deletes ALL documents - be careful :)
		$this->commitIndex();
	}
	public function searchIndex($queries,$start = 0 ,$limit = 0)
	{
		$solr = &$this->solr;
		$response = $solr->search( $queries, $start, $limit );
		if ( $response->getHttpStatus() == 200 )
		{
			if ( $response->response->numFound > 0 )
			{
				return $response->response;
			}
			else
			{
				return $response->getHttpStatusMessage();
			}
		}
	}
	public function facetedSearch($queries,$facetField,$start=0,$limit=0,$minCount=1,$facetSortField="index",$facetLimit=-1)
	{
		$solr = &$this->solr;
		$aParams = array("facet"=>"true","facet.field"=>$facetField,"facet.mincount"=>$minCount,"facet.sort"=>$facetSortField,"facet.limit"=>$facetLimit);
		$response = $solr->search($queries,$start,$limit,$aParams);
		if( $response->getHttpStatus() == 200)
		{
			if( $response->response->numFound > 0)
			{
				return ($response->facet_counts->facet_fields);
			}
		}
	}

	public function fieldListSearch($queries,$fieldList,$start=0,$limit=0,$minCount=1)
	{
		$solr = &$this->solr;
		//$aParams = array("facet"=>"true","facet.field"=>$facetField,"facet.mincount"=>$minCount,"facet.sort"=>$facetSortField,"facet.limit"=>$facetLimit);
		$aParams= array("fl"=>$fieldList);
		$response = $solr->search($queries,$start,$limit,$aParams);
		if( $response->getHttpStatus() == 200)
		{
			if( $response->response->numFound > 0)
			{
				return $response->response->docs;
			}
		}
	}

	public function multipleFacetedSearch($queries,$facetFieldArray = Array(),$start=0,$limit=0,$minCount=1,$facetSortField="index",$facetLimit=-1)
	{
		//$facetFieldArray should be like Array("brands_filter_facet", "price")
		$solr = &$this->solr;
		$aParams = array("facet"=>"true","facet.field"=>$facetFieldArray,"facet.mincount"=>$minCount,"facet.limit"=>$facetLimit);
        // Add fields for discount_percentage range facet
        $aParams["facet.range"] = "discount_percentage";
        $aParams["f.discount_percentage.facet.range.start"] = "10";
        $aParams["f.discount_percentage.facet.range.end"] = "100";
        $aParams["f.discount_percentage.facet.range.gap"] = "10";
		if($facetSortField == "index")
			$aParams["facet.sort"] = "index";
		$response = $solr->search($queries,$start,$limit,$aParams);
		if( $response->getHttpStatus() == 200)
		{
			if( $response->response->numFound > 0)
			{
                // setting additional property for count results to figure out multiple things
                $response->facet_counts->facet_fields->numFound = $response->response->numFound;
				return ($response->facet_counts);
			}
		}
	}
	
	/*
	 * @param string $queries The raw query string
	 * @param int $offset The starting offset for result documents
	 * @param int $limit The maximum number of result documents to return
	 * @param string $sortField Field to sort with 
	 * @param array $params key / value pairs for other query parameters (see Solr documentation) 
	 * @return Apache_Solr_Response
	 */
	public function searchAndSort($queries, $start=0, $limit=20, $sortField='',$params=array())
	{
		//If both $sortField and $params['sort'] are passed to  this function, $sortField will get higher priority
		$solr = &$this->solr;
		$querySolr = $queries;
		$s = $sortField;
		if (!is_array($params)) {
			$params = array();
		}
		
		$aParams = $params;
		if($s !='')
		{
			$aParams['sort'] = $s;  //always consider just arrived
		}
		$response = $solr->search( $querySolr,$start, $limit, $aParams);
		if ( $response->getHttpStatus() == 200 )
		{
			if ( $response->response->numFound > 0 )
			{
				return $response->response;
			}
			else
			{
				return  $response->getHttpStatusMessage();
			}
		}
	}
	
    public function searchHighlighted($queries, $start=0, $limit=20, $sortField='',$params=array())
	{
		//If both $sortField and $params['sort'] are passed to  this function, $sortField will get higher priority
		$solr = &$this->solr;
		$querySolr = $queries;
		$s = $sortField;
		if (!is_array($params)) {
			$params = array();
		}
		
		$aParams = $params;
		if($s !='')
		{
			$aParams['sort'] = $s;  //always consider just arrived
		}
		$response = $solr->search( $querySolr,$start, $limit, $aParams);
		if ( $response->getHttpStatus() == 200 )
		{
			if ( $response->response->numFound > 0 )
			{
				return $response;
			}
			else
			{
				return  $response->getHttpStatusMessage();
			}
		}
	}
	public function commitIndex()
	{
		$this->solr->commit();
	}
	
	public function commitToAllServers(){
		global $solr_servers;
		foreach ($solr_servers as $server) {
			$solr = new solrProducts($server);
			$solr->commitIndex();
		}
	}
	
	
	protected function loadDocs($documents)
	{
		global $errorlog;
		try
		{
			$solr = &$this->solr;
			return $solr->addDocuments( $documents );
		}
		catch ( Exception $e ) {
			$errorlog->error($e->getMessage());
		}
	}

	protected function removeById($id)
	{
		global $errorlog;
		try
		{
			$solr = &$this->solr;
			return $solr->deleteById($id);
		}
		catch ( Exception $e ) {
			$errorlog->error($e->getMessage());
		}
	}
	
	public function getSolrCallLatencies()
	{
		return $this->solr->getSolrCallLatencies();
	}
}
?>
