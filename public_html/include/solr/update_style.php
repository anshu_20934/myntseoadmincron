<?php
include_once('../../auth.php');
include_once('solrProducts.php');
if (isset($_GET['styleid'])) {
    $styleid = $_GET['styleid'];
    $solrProducts = new solrProducts();
    $solrProducts->addStyleToIndex($styleid);
}