<?php	
	try
	{
		$index = $_GET["index"];
		switch($index)
		{
			case 'p' :
							include_once('solrProducts.php'); 
							$solrIndex = new solrProducts();
							break;
			case 'ps':
							include_once('solrProductStyles.php');
							$solrIndex = new solrProductStyles();
							break;
			case 's'	:
							include_once('solrShops.php');
							$solrIndex = new solrShops();
							break;
			case 'd'	:
							include_once('solrDesigners.php');
							$solrIndex = new solrDesigners();
							break;
			default :
							Header('Location:mynIndex.php?ec=opf');
		}
		$solrIndex->optimizeIndex();
		Header('Location:mynIndex.php?ec=ops');
							
	}
	catch(Exception $ex)
	{
		Header('Location:mynIndex.php?ec=ex');
	}
?>