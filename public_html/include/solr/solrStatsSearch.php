<?php
include_once(HostConfig::$documentRoot.'/auth.php');
require_once( 'Apache/Solr/Service.php' );
include_once('solrCommon.php');
include_once('solrUtils.php');
/**
 * Created by IntelliJ IDEA.
 * User: Mohammed Asif Hussain
 * Date: Dec 21, 2009
 * Time: 9:31:15 AM
 * To change this template use File | Settings | File Templates.
 */

class solrStatsSearch extends solrCommon {

    var $table = 'mk_stats_search' ;

    var $core  = 'stats_search';

    public function __construct($hostname="")
    {
        global $solr_servers;
        if(empty($hostname)){
            $hostname = $solr_servers[0];
        }
        parent::__construct($hostname, $this->core);
    }

    function indexProducts(){
        $offset = 0 ;
        $limit = 2000;
        $documents = $this->get_stats_search_documents($offset, $limit);
        while(!empty($documents)) {
             $this->update($documents);
             $offset += $limit;
             $documents = $this->get_stats_search_documents($offset, $limit); 
        }
        //$this->optimizeIndex();
        $this->commitToAllServers();
    }

    function get_stats_search_documents($offset  = NULL, $limit = NULL){
        $query = "select * from ".$this->table." limit $limit offset $offset";
        $results = func_query($query, true);
        if(!empty($results)){
            $documents = array();
            foreach($results as $result){
                $documents[] =  $this->get_document($result);
            }
        }
        return $documents;
    }

    /**
     * Call this method when you do not have the full db row ..
     * @param  $id
     * @return void
     */
    function update_document($id){
        $result = func_select_first($this->table, array('key_id' => $id));
        if ($result) {
            $document = $this->get_document($result);
            $this->update(array($document));
        }
    }


    /**
     *  Call this method when you have the full db row ..
     * @param  $row_data
     * @return void
     */
    function update_result($row_data){
        $document = $this->get_document($row_data);
        $this->update(array($document));
    }
    /**
     * @param  $row_data
     * @return void
     */
     function update($documents){
         global $solr_servers;
         foreach ($solr_servers as $server) {
             $solr = new solrStatsSearch($server);
             $solr->loadDocs($documents);
             $solr->commitIndex();
         }
    }

    /**
     *
     * @param  $id
     * @return void
     */
    function remove_document($id){
        global $solr_server;
        foreach ($solr_server as $server) {
            $solr = new solrStatsSearch($server);
            $this->removeById($id);
            $solr->commitIndex();
        }
    }

    /**
     * @param  $result
     * @return Apache_Solr_Document
     */
    function get_document($result){
        $document = new Apache_Solr_Document();
        addValue($document,'key_id',$result['key_id']);
        addValue($document,'keyword',$result['keyword']);
        addValue($document,'orignal_keyword',$result['orignal_keyword']);
        addValue($document,'last_search_date',$result['last_search_date']);
        addValue($document,'count_of_products',$result['count_of_products']);
        addValue($document,'count_of_searches',$result['count_of_searches']);
        addValue($document,'is_valid', $result['is_valid'] == 'Y' ? true : false);
        addValue($document,'meta_description', $result['meta_description'] );
        addValue($document,'page_title', $result['page_title'] );
        addValue($document,'meta_keyword', $result['meta_keyword'] );
        addValue($document,'description', $result['description'] );
        addValue($document,'top_banner', $result['top_banner'] );
        addValue($document,'serp', $result['serp'] );
        addValue($document,'url', $result['url'] );
        addValue($document,'page_h1', $result['page_h1'] );
        return  $document;
    }
}
