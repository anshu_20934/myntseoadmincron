<?php
require_once( 'Apache/Solr/Service.php' );
include_once('solrCommon.php');
include_once('solrUtils.php');
class SolrProductStyles extends SolrCommon
{
	public function __construct()
  	{
		parent::__construct("sprodstyle");
	}
	public function indexProductStyles()
	{
		$documents = $this->getProductStyleDocuments();
		$this->loadDocs($documents,$this->solr);
        $this->commitIndex();
	}
	public function reIndexProductStyles()
	{
		//needs optimization after reindexing
		// difference between index & reindex ???
	}
	public function deleteProductStyleDocFromIndex($documentId)
	{
		$solr = &$this->solr;
    	$solr->deleteById($documentId);
		commitIndex();	
	}
	public function addProductStyleDocToIndex($document)
	{
		$solr = &$this->solr;
		$solr->addDocument($document);
		commitIndex();
	}
	private function getProductStyleDocuments()
	{
		//load data from DB to create index table
 		include_once('dbConnection.php');
 
		// load all product styles
		$qry = "select id,product_type,name,label,description,style_code,price from mk_product_style";
		$res = mysql_query($qry);
		$productStyle = array();
		while( $row = mysql_fetch_assoc($res) )
		{
			$id = $row['id'];  
			$productStyle[$id] = $row;
		}
	
		// load all product types
		$qry = "select id,name,label,description,image_l,image_i,group_id from mk_product_type";	
		$res = mysql_query($qry);
		$productType = array();
		while( $row = mysql_fetch_assoc($res) )
		{
		 	$id = $row['id'];
		 	$productType[$id] = $row;  
		}
		 
		// load all product groups
		$qry = "select id,name,label,description from mk_product_group";
		$res = mysql_query($qry);
		$productGroup = array();
		while( $row = mysql_fetch_assoc($res) )
		{
			$id = $row['id'];
			$productGroup[$id] = $row;
 		}

		$documents = array();
		foreach( $productStyle as $key=>$value )
		{
			$psDoc = new Apache_Solr_Document();
			$styleid = $key;
			$thisStyle = $productStyle[$styleid];
	
			$typeid  = $thisStyle['product_type'];
			$thisType = $productType[$typeid];
	
			$groupid = $thisType['group_id'];
			$thisGroup = $productGroup[$groupid];
	
			addValue($psDoc,'id',$styleid);
			addValue($psDoc,'psname',$thisStyle['name']);
			addValue($psDoc,'pslabel',$thisStyle['label']);
			addValue($psDoc,'psdesc',$thisStyle['description']);
			addValue($psDoc,'pscode',$thisStyle['style_code']);
			addValue($psDoc,'psprice',floatval($thisStyle['price']));
	
			addValue($psDoc,'ptname',$thisType['name']);
			addValue($psDoc,'ptlabel',$thisType['label']);
			addValue($psDoc,'ptdesc',$thisType['description']);
			addValue($psDoc,'ptimage_l',$thisType['image_l']);
			addValue($psDoc,'ptimage_i',$thisType['image_i']);
	
			addValue($psDoc,'pgname',$thisGroup['name']);
			addValue($psDoc,'pglabel',$thisGroup['label']);
			addValue($psDoc,'pgdesc',$thisGroup['description']);

			$documents[] = $psDoc;
 		}
		return $documents;
	}
}
?>
