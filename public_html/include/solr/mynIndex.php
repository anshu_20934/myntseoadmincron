<html>
	<head>
		<title> 
			Myntra Search Engine Demo
		</title> 
		<script>
			function clearText()
			{
				var searchBox = document.getElementById("txtSearch");
				if( searchBox.value == "enter search string here")
				{
					searchBox.value = "";
				}
			}
			function addText()
			{
				var searchBox = document.getElementById("txtSearch");
				if( searchBox.value == "")
					searchBox.value = "enter search string here"	
			}
			function validate()
			{
				var searchBox = document.getElementById("txtSearch");
				var msg="";
				if( searchBox.value == "enter search string here")
				{
					msg = "Please enter your search keywords";
					document.getElementById("searchError").innerHTML = msg;
					return;
				}
				var optSearch = document.getElementById("comboSearch");
				if( optSearch.value == "0")
				{
					msg = "Please select a category";
					document.getElementById("searchError").innerHTML = msg;
					return;
				}
				document.forms["searchForm"].submit();				
			}
		</script>
	</head>
	<body>
		<h1> Search Demo </h1>
		<form id="searchForm" method="post" action="mkSearchIndex.php">
			<table id="searchTable">
				<tr> 
					<td>
						<input type="text" value="enter search string here" id="txtSearch" name="txtSearch" size="30" onfocus="clearText()" onblur="addText()"/>
					</td>
					<td> 
						<select id="comboSearch" name="comboSearch">
							<option value="0"> Select Category </option>
							<option value="p"> products </option>
							<option value="d"> designers </option>
							<option value="s"> shops </option>
							<option value="ps"> product styles </option>
						</select>
					</td>
					<td>
						<input type="button" value="search" onclick="validate()"/>
					</td>
				</tr>
				<tr>
					<td colspan="3"><font color="RED"><span id="searchError"></span></font></td>
				</tr>
			</table>
		</form>
		
		<h1>Index Information - Admin Panel</h1>
		<table width="70%" border="1">
			<tr>
				<th> Category </th>
				<th> Index Built on</th>
				<th> Actions </th>
			</tr>
			<tr>
				<td align="center"> Products </td>
				<td align="center" id="pidxDate">Not Available</td>
				<td align="center">
					<table>
						<tr>
							<td>
								<form action="mkBuildIndex.php?index=p" method="post">
									<input type="submit" value="build index"/>
								</form>
							</td>
							<td>
								<form action="rebuildIndex.php?index=p" method="post">
									<input type="submit" value="rebuild index"/>
								</form>
							</td>
							<td>
								<form action="mkOptimizeIndex.php?index=p" method="post">
									<input type="submit" value="optimize Index"/>
								</form>
							</td>
							<td>
								<form action="mkDeleteIndex.php?index=p" method="post">
									<input type="submit" value="delete Index"/>
								</form>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td align="center"> Shops </td>
				<td align="center" id="pidxDate">Not Available</td>
				<td align="center">
					<table>
						<tr>
							<td>
								<form action="mkBuildIndex.php?index=s" method="post">
									<input type="submit" value="build Index"/>
								</form>
							</td>
							<td>
								<form action="rebuildIndex.php?index=p" method="post">
									<input type="submit" value="rebuild Index"/>
								</form>
							</td>
							<td>
								<form action="mkOptimizeIndex.php?index=p" method="post">
									<input type="submit" value="optimize Index"/>
								</form>
							</td>
							<td>
								<form action="mkDeleteIndex.php?index=p" method="post">
									<input type="submit" value="delete Index"/>
								</form>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td align="center"> Designers </td>
				<td align="center" id="didxDate">Not Available</td>
				<td align="center">
					<table>
						<tr>
							<td>
								<form action="mkBuildIndex.php?index=d" method="post">
									<input type="submit" value="build index"/>
								</form>
							</td>
							<td>
								<form action="rebuildIndex.php?index=d" method="post">
									<input type="submit" value="rebuild index"/>
								</form>
							</td>
							<td>
								<form action="mkOptimizeIndex.php?index=d" method="post">
									<input type="submit" value="optimize Index"/>
								</form>
							</td>
							<td>
								<form action="mkDeleteIndex.php?index=d" method="post">
									<input type="submit" value="delete Index"/>
								</form>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td align="center"> Product Styles </td>
				<td align="center" id="psidxDate">Not Available</td>
				<td align="center">
					<table>
						<tr>
							<td>
								<form action="mkBuildIndex.php?index=ps" method="post">
									<input type="submit" value="build index"/>
								</form>
							</td>
							<td>
								<form action="rebuildIndex.php?index=ps" method="post">
									<input type="submit" value="rebuild index"/>
								</form>
							</td>
							<td>
								<form action="mkOptimizeIndex.php?index=ps" method="post">
									<input type="submit" value="optimize Index"/>
								</form>
							</td>
							<td>
								<form action="mkDeleteIndex.php?index=ps" method="post">
									<input type="submit" value="delete Index"/>
								</form>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td align="center"> Category Search </td>
				<td align="center" id="psidxDate">Not Available</td>
				<td align="center">
					<table>
						<tr>
							<td>
								<form action="mkBuildIndex.php?index=cs" method="post">
									<input type="submit" value="build index"/>
								</form>
							</td>
							<td>
								<form action="rebuildIndex.php?index=cs" method="post">
									<input type="submit" value="rebuild index"/>
								</form>
							</td>
							<td>
								<form action="mkOptimizeIndex.php?index=cs" method="post">
									<input type="submit" value="optimize Index"/>
								</form>
							</td>
							<td>
								<form action="mkDeleteIndex.php?index=cs" method="post">
									<input type="submit" value="delete Index"/>
								</form>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td align="center"> Stats Search </td>
				<td align="center" id="psidxDate">Not Available</td>
				<td align="center">
					<table>
						<tr>
							<td>
								<form action="mkBuildIndex.php?index=ss" method="post">
									<input type="submit" value="build index"/>
								</form>
							</td>
							<td>
								<form action="rebuildIndex.php?index=ss" method="post">
									<input type="submit" value="rebuild index"/>
								</form>
							</td>
							<td>
								<form action="mkOptimizeIndex.php?index=ss" method="post">
									<input type="submit" value="optimize Index"/>
								</form>
							</td>
							<td>
								<form action="mkDeleteIndex.php?index=ss" method="post">
									<input type="submit" value="delete Index"/>
								</form>
							</td>

						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td align="center">Name Search </td>
				<td align="center" id="psidxDate">Not Available</td>
				<td align="center">
					<table>
						<tr>
							<td>
								<form action="mkBuildIndex.php?index=ns" method="post">
									<input type="submit" value="build index"/>
								</form>
							</td>
							<td>
								<form action="rebuildIndex.php?index=ns" method="post">
									<input type="submit" value="rebuild index"/>
								</form>
							</td>
							<td>
								<form action="mkOptimizeIndex.php?index=ns" method="post">
									<input type="submit" value="optimize Index"/>
								</form>
							</td>
							<td>
								<form action="mkDeleteIndex.php?index=ns" method="post">
									<input type="submit" value="delete Index"/>
								</form>
							</td>

						</tr>
					</table>
				</td>
			</tr>
		</table>
		<div id="statusMsg">
			<?php
				$code = $_GET["ec"];
				switch($code)
				{
					case 'ops'; 	
									echo "operation Success";
									break;
					case 'opf';	
									echo "operation failure";
									break;
				}
			?>
		</div>
	</body>
</html>