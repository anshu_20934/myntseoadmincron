<?php
include_once(HostConfig::$documentRoot.'/auth.php');
require_once( 'Apache/Solr/Service.php' );
include_once('solrCommon.php');
include_once('solrUtils.php');
/**
 * Created by IntelliJ IDEA.
 * User: Mohammed Asif Hussain
 * Date: Dec 21, 2009
 * Time: 9:31:15 AM
 * To change this template use File | Settings | File Templates.
 */

class solrCategorySearch extends solrCommon {

    var $table = 'mk_category_search' ;

    var $core  = 'category_search';


    public function __construct($hostname="")
    {
        global $solr_servers;
        if(empty($hostname)){
            $hostname = $solr_servers[0];
        }
        parent::__construct($hostname, $this->core);
    }

    function indexProducts(){
        $offset = 0 ;
        $limit = 1000;
        $documents = $this->get_category_search_documents($offset, $limit);
        while(!empty($documents)) {
            $this->update($documents);
            $offset += $limit;
            $documents = $this->get_category_search_documents($offset, $limit);
        }
        $this->optimizeIndex();
    }

    function get_category_search_documents($offset  = NULL, $limit = NULL){
        $results = func_select_query($this->table, NULL, $offset, $limit );
        if(!empty($results)){
            $documents = array();
            foreach($results as $result){
                $documents [] = $this->get_document($result);
            }
        }
        return $documents;
    }

    /**
     * Call this method when you do not have the full db row ..
     * @param  $id
     * @return void
     */
    function update_document($id){
        $result = func_select_first($this->table, array('key_id' => $id));
        if ($result) {
            $document = $this->get_document($result); 
            $this->update(array($document));
        }
    }


    /**
     *  Call this method when you have the full db row ..
     * @param  $row_data
     * @return void
     */
    function update_result($row_data){
        $document = $this->get_document($row_data);
        $this->update(array($document));
    }
    
    /**
     * @param  $documents
     * @return void
     */
    function update($documents){
        global $solr_servers;
        foreach($solr_servers as $server){
            $solr = new solrCategorySearch($server);
            $solr->loadDocs($documents);
            $solr->commitIndex();
        }
    }
    /**
     *
     * @param  $id
     * @return void
     */
    function remove_document($id){
         global $solr_servers;
        foreach($solr_servers as $server){
            $solr = new solrCategorySearch($server);
            $this->removeById($id);
            $solr->commitIndex();
        }
    }

    function get_document($result){
        $document = new Apache_Solr_Document();
        addValue($document,'id',$result['key_id']);
        addValue($document,'url',$result['url']);
        addValue($document,'keyword',$result['keyword']);
        return  $document;
    }
}
