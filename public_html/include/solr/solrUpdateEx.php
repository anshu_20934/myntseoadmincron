<?php
include_once("solrProducts.php");
include_once("solrUpdate.php");
/* This is a HTTP interface over solrUpdate.php to enable others making HTTP calls to update solr. */
/* Extend this file as & when required. */

$isValidRequest = true;

if( isset($_GET["index"])  && isset($_GET["action"])  && isset($_GET["smid"])  )
{
	$index = $_GET["index"];
	$action = $_GET["action"];
	$smId = $_GET["smid"];

	if( empty($index) || empty($action) || empty($smId) )
		$isValidRequest = false;
	if( $smId == "zero")
		$smId = 0;
}
else
{
	$isValidRequest = false;
}

if( !$isValidRequest )
{
	echo "Invalid params";
	exit;
}

if( $index == "p" )
{
	if( $action == "asm")
	{
        if( $smId == "all") {
            $shop_masters = func_select_query('mk_shop_masters', array('status' => '1'));
            foreach($shop_masters as $shop_master){
                addShopMaster($shop_master['shop_master_id']);
            }
        } else {
            addShopMaster($smId);
        }
	}
	else
	if( $action == "dsm")
	{
		deleteShopMaster($smId);
	}
}

?>
