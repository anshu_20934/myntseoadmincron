<?php
require_once( 'Apache/Solr/Service.php' );
include_once('solrCommon.php');
include_once('solrUtils.php');
class SolrDesigners extends SolrCommon
{
	public function __construct()
  	{
		parent::__construct("sdesigner");
	}
	public function indexDesigners()
	{
		$documents = $this->getDesignerDocuments();
		$this->loadDocs($documents,$this->solr);
        $this->commitIndex();
	}
	public function reIndexDesigners()
	{
		//needs optimization after reindexing
		// difference between index & reindex ???
	}
	public function deleteDesignerDocFromIndex($documentId)
	{
		$solr = &$this->solr;
    	$solr->deleteById($documentId);
		commitIndex();	
	}
	public function addDesignerDocToIndex($document)
	{
		$solr = &$this->solr;
		$solr->addDocument($document);
		commitIndex();
	}
	private function getDesignerDocuments()
	{
		$documents = array();
		//load from DB
		return $documents;
	}
}
?>
