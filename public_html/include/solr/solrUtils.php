<?php
function addValue($doc,$key,$value)
{
    if ( is_array( $value ) )
    {
        foreach ( $value as $datum )
        {
            $doc->setMultiValue( $key, $datum );
        }
    }
    else
    {
        $doc->$key = $value;
    }
}
/**
 * @param  $documents - solr document array
 * @return array - array of documents, where each document contains a field value mapping .. 
 */
function documents_to_array($documents){
    $documents_array = array();
    foreach($documents as $document)
    {
        $document_array = array();
        $fields = $document-> getFieldNames();
        foreach($fields as $field ){
            $document_array[$field] =  $document->__get($field);
        }
        $documents_array [] = $document_array;
    }
    return $documents_array;
}
/**
 * @param  $documents
 * @return array
 */
function array_to_documents($documents){
    $documents_array = array();
    foreach($documents as $document)
    {
        $document_array = array();
        $fields = $document-> getFieldNames();
        foreach($fields as $field ){
            $document_array[$field] =  $document->__get($field);
        }
        $documents_array [] = $document_array;
    }
    return $documents_array;
}

?>
