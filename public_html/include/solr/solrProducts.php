<?php
if((!defined('FUNC_SKU_CALL'))||!(constant('FUNC_SKU_CALL')==='Y'))
include_once(dirname(__FILE__).'/../../auth.php');
require_once('Apache/Solr/Service.php' );
include_once('solrCommon.php');
include_once('solrUtils.php');
include_once($xcart_dir."/include/class/class.feature_gate.keyvaluepair.php");
include_once($xcart_dir."/include/class/class.Search_promotion_key.php");
include_once($xcart_dir."/modules/discount/DiscountEngine.php");
include_once($xcart_dir."/include/class/abuse/AbuseFilter.php");
include_once($xcart_dir."/include/class/style/dao/StyleGroupDBAdapter.php");
include_once ("$xcart_dir/modules/styleService/styleServiceClient.php");
require_once($xcart_dir."/env/SOLR.config.php");
require_once("$xcart_dir/include/func/func.utilities.php");
use vtr\client\VirtualTrialRoomClient;
use style\dao\StyleGroupDBAdapter;

set_time_limit(0);
class SolrProducts extends SolrCommon
{
	public function __construct($hostname=null)
	{
		if(empty($hostname) && !empty(SOLRConfig::$searchhost)){
			$hostname=SOLRConfig::$searchhost;	
		}else if(empty($hostname)){
			$hostname="localhost";	
		}
		parent::__construct($hostname,"sprod");
	}
	public function indexProducts($shopMasterId="")
	{
		global $sql_tbl;
		$productTable = $sql_tbl["products"];
		$producttype = $sql_tbl["mk_product_type"];
		$imageTable = $sql_tbl["images_T"];
		$styleTable = $sql_tbl["mk_product_style"];

		$query = "select count(*) from $productTable,$imageTable ,$producttype ,$styleTable";
		$query.= " where $productTable.productid = $imageTable.id
                   and $productTable.product_type_id = $producttype.id
                   and $styleTable.id = $productTable.product_style_id and statusid=1008";

		$res = db_query($query,true);
		$row = db_fetch_row($res);
		$maxDocs = $row[0];

		$block = 1000;

		for($i=0;$i<$maxDocs;$i=$i+$block)
		{
			/*echo $i.' <br />';
			 ob_flush();
			 flush();*/
			$documents = $this->getProductDocuments($shopMasterId,$i,$block);
			$this->loadDocs($documents,$this->solr);
			$this->commitIndex();
		}
	}
	
	public function indexNonCustomizedStyles($page_number=-1,$page_size=5000,$only_local=false)
	{
		global $sql_tbl;
		$styleTable = $sql_tbl["mk_product_style"];
		$enableAllStyles=FeatureGateKeyValuePairs::getBoolean('enableAllStyles');
			//index only non customized styles
		$query = "select $styleTable.id from $styleTable 
				  	join mk_product_type t on $styleTable.product_type=t.id 
				  where $styleTable.styletype='P' 
				  	and $styleTable.is_customizable='0'
				   	and t.type ='P'";
			if(!$enableAllStyles){
				$query.=" and $styleTable.is_active='1'";
			}
			$query.=" order by 1 desc";
		
		// Set the page number
		if($page_number != -1){
			$page_number=$page_number-1;
			$offset_number=$page_number*$page_size;
			$query.=" limit $offset_number,$page_size";
		}
		$styleIdArrayToIndex = getSingleColumnAsArray($query,true);
		//Kundan: performance optimization and possibly bug fix for vtr indexing of solr enabled styles
		//don't pass any argument.. it will fetch for all styles; 
		$vtrStyleMap = VirtualTrialRoomClient::getStyleDataForIndexing();
		
		$discountDetails = DiscountEngine::getDataForMultipleStyles("search", $styleIdArrayToIndex);
        
        // get all the colour schems before hand
        $colour_family_query = 
            "select ".
                    "atv.attribute_value as attribute_value, ".
                    "group_concat(atf.family_name separator ',') as family_name, ".
                    "group_concat(atf.swatch_position separator ',') as family_swatch_position ".
                "from ".
                    "mk_attribute_type_values atv ".
                        "left join attribute_type_family_member atfm ".
                            "on ".
                            "atv.id=atfm.id_attribute_type_value ".
                        "left join attribute_type_family atf ".
                            "on ".
                            "atfm.id_attribute_type_family=atf.id ".
                "where atv.attribute_type_id=4 group by atv.attribute_value";
    	$colourFamilyList_out = func_query($colour_family_query,true);
    	$colourFamilyList = array();
        foreach ($colourFamilyList_out as $item_colour) {
            $colourFamilyList[$item_colour['attribute_value']] = $item_colour['family_name'];
        }
    	$colourFamilySwatchList = array();
        foreach ($colourFamilyList_out as $item_colour) {
            $colourFamilySwatchList[$item_colour['attribute_value']] = $item_colour['family_swatch_position'];
        }
		$style_counter=20;
		foreach ($styleIdArrayToIndex as $styleID)
		{
			//Kundan: if a style is not VTR enabled, send the paramater as true so that no fresh service call is made for that style
			$vtrStyleDetails = empty($vtrStyleMap[$styleID]) ? true : $vtrStyleMap[$styleID];
			$this->addStyleToIndex($styleID, true, false,true,$discountDetails->$styleID,$vtrStyleDetails,$only_local,$colourFamilyList,$colourFamilySwatchList);
			if($page_number == -1){
				echo $styleID.",";
				if($style_counter == 0){
					echo "\n";
					$style_counter=20;
				}else{
					$style_counter--;
				}
			}
		}
	}
	
	public function deleteNonCustomizedDisabledStyles($param='')
	{
		global $sql_tbl;
		$styleTable = $sql_tbl["mk_product_style"];
		$query = "select $styleTable.id from $styleTable join mk_product_type t on $styleTable.product_type=t.id  where ($styleTable.styletype='A' or $styleTable.is_active='0') and t.type ='P' and $styleTable.is_customizable='0' order by 1";
				
		$res = func_query($query,true);
		$commitCounter = 1;
		foreach ($res as $id)
		{
			$this->deleteStyleFromIndex($id['id'],false,false);
			$commitCounter++;
			echo $id['id']."??";
		}
	}
	
	public function addProductToIndex($productid)
	{
		global $sql_tbl;
		$productTable = $sql_tbl["products"];
		$producttype = $sql_tbl["mk_product_type"];
		$imageTable = $sql_tbl["images_T"];
		$styleTable = $sql_tbl["mk_product_style"];

		$query = "select
		$productTable.productid as productid,
		$productTable.product product,
                                        FORMAT($styleTable.price+IFNULL($styleTable.price*$productTable.markup/100,0),2) as price, 
                                        $imageTable.filename as imgfilename,
                                        $producttype.id as typeid,
                                        $producttype.name as typename,
                                        $productTable.product_style_id as styleid,
                                        $styleTable.name as stylename,
                                        $productTable.image_portal_t,
                                        $productTable.myntra_rating,
                                        $productTable.image_portal_thumbnail_160 as img_portal_160,
                                        $productTable.keywords as keywords,
                                        $productTable.add_date as justarrived,
                                        $productTable.rating as userrating,
                                        $productTable.myntra_rating as mynrating,
                                        $productTable.sales_stats as bestselling,
                                        $productTable.descr as descr,
                                        $productTable.fulldescr as fulldescr,

                                        $producttype.type as product_type,
                                        $styleTable.cod_enabled as style_cod,
                                        $producttype.cod_enabled as type_cod
                        				from $productTable,$imageTable,$producttype,$styleTable 
				                        where $productTable.productid = $imageTable.id
                                        and $productTable.product_type_id = $producttype.id
                                        and $styleTable.id = $productTable.product_style_id 
                                        and $productTable.productid = $productid";

                                        $documents = $this->getProductDocumentsEx("",$query);
	       								$this->publishToAllServers($documents);
	}

	public function addStylesToIndex($styleids,$flush=true,$echoTobeDone=false){
		$count=count($styleids);
		if($echoTobeDone) echo "Adding styles to index:\n";
		foreach($styleids as $key => $value){
			$styleid = is_array($value) ? $value["style_id"] : $value;
			if($echoTobeDone) echo $styleid." ";
			$this->addStyleToIndex($styleid,$flush);
		}
	}

	public function addStyleToIndex(
		$styleid,$flush=true, $callApi=true,
		$useDiscountData=false,$discountData=null,
		$vtrStyleData=null,
		$only_local=false,$colourFamilyList=null,$colourFamilySwatchList=null)
{
		global $solrlog, $xcart_dir;
		// invalidate the pdp cache 
		include_once($xcart_dir."/Cache/Cache.php");
        include_once("$xcart_dir/Cache/CacheKeySet.php");
		$cache = new Cache(PDPCacheKeys::$prefix.$styleid, PDPCacheKeys::keySet());
		$cache->invalidate();		
		$reIndexJavaServiceCall=FeatureGateKeyValuePairs::getBoolean('reIndexJavaServiceCall',true);
		if ($reIndexJavaServiceCall) {
			styleServiceClient::reindex_single_service_call($styleid);
		}
		else {
		$enableAllStyles=FeatureGateKeyValuePairs::getBoolean('enableAllStyles',false);
		$maxSizeOption = FeatureGateKeyValuePairs::getInteger('Number-Of-Size-Options',4);

		$mandatoryFields = array();
		$smid=0;
		$query="select s.id,s.product_type,s.name,s.label,s.price,s.size_chart_image,s.is_customizable,s.description,s.items_sold,s.cod_enabled as style_cod,
	        p.article_number,p.product_tags,p.product_display_name,p.variant_name,p.discount_rule,p.product_information,p.default_image,p.front_image,p.left_image,p.right_image,p.back_image,p.top_image,p.search_image,p.search_zoom_image,p.filters,p.user_rating,p.user_rate,p.myntra_rating,p.target_url,p.add_date,
	        t.name as product_type_name,t.cod_enabled as type_cod from mk_product_style s left join mk_style_properties p on s.id=p.style_id left join mk_product_type t on s.product_type=t.id where s.id=".$styleid ;
		if(!$enableAllStyles){
			$query=$query ." and s.styletype = 'P' and s.is_active = 1";
		}
		$result=func_query($query,true);
		if(!empty($result)){
			// Adding Colour Group Style Ids
			$styleGroupAdapter = new StyleGroupDBAdapter();
			$styleGroupArray = $styleGroupAdapter->getStyleGroupsForSingleStyle($styleid,"taba.group_type='color'");
			$styleIdsArray = array();
			if (!empty($styleGroupArray) && $styleGroupArray[0]!= null) {
				$styleIdsArray = $styleGroupArray[0]->getStyleIdsArray();
			}
			$styleGroupStyleIds = implode("," , array_unique($styleIdsArray));
			
			//TODO: Kundan: Can we merge these 2 queries with the main query above?
		$product_type_group=func_query("select ptg.name from mk_product_type pt ,mk_product_group ptg where pt.product_type_groupid=ptg.id and pt.id=".$result[0]['product_type'],true);
		$solrlog->info("Adding style - $styleid to solr index");
		$sizes=Size_unification::getUnifiedSizeByStyleId($styleid, "string", false, $callApi);
		$sizeGroup = Size_unification::getSizeGroup($styleid);
		$solrlog->info("Found active Sizes - $sizes");
		if(!empty($sizes))
		{
			$size_options_array=explode(",",$sizes);
			$count_options_available = sizeof($size_options_array);
			//echo"blah $maxSizeOption";//exit;
			if($count_options_available  > $maxSizeOption)
			{
				//echo"\n reached normal $maxSizeOption  left\n";
				$count_options_available = $maxSizeOption;
			}
		}
		//PORTAL-309: Now we want another variable: is_item_in_stock, which is basically a thresholding of size_options_array: 0 is the size is 0, 1 if the size is greater than 0
		//We want to do this, because we want to sort based on Price (ASC) only for items available in stock (is_item_in_stock=1). If item_in_stock is 0, then those showed be shown at the end
		$is_item_in_stock = $count_options_available >= 1 ? 1 : 0;
		//catalog classification fields
		$catQuery = "select c.typename as global_attr_article_type, p1.typename as global_attr_sub_category, p2.typename as global_attr_master_category, ";
		$catQuery .= "s.global_attr_brand, s.global_attr_age_group, s.global_attr_gender, s.global_attr_base_colour, s.global_attr_colour1, s.global_attr_colour2, ";
		$catQuery .= "s.global_attr_fashion_type, s.global_attr_season, s.global_attr_year, s.global_attr_usage, s.catalog_date, s.display_categories as display_categories ";
		$catQuery .= "from mk_style_properties s left join mk_catalogue_classification c on s.global_attr_article_type=c.id left join mk_catalogue_classification p1 on c.parent1 = p1.id left join mk_catalogue_classification p2 on c.parent2 = p2.id ";
		$catQuery .= "where s.style_id = ".$styleid;
		$catResult = func_query_first($catQuery,true);

		$averageRating = 0;
		$applied_filters=func_query("select f.applied_filters,g.group_name from mk_applied_filters f,mk_filter_group g where f.filter_group_id=g.id and f.generic_id='".$styleid."'",true);
		foreach($applied_filters as $filters)
		{
			if(!empty($filters['applied_filters'])){
				$app_filters[$filters['group_name']]=$filters['applied_filters'].",".$filters['group_name'];
				$app_filters_facet[$filters['group_name']]=$filters['applied_filters'];
			}
			else{
				$app_filters[$filters['group_name']]='';
			}
		}
        $discount=0;
        $priceAfterDiscount=$result[0]['price'];
        $discount_type = '';
        $discount_value = 0;
		$discountLabel = '';
		$discountPercentage = 0;
				
		if(!$useDiscountData){
			$discountData = DiscountEngine::getDataForAStyle("search", $result[0]['id']);
		}
	
                
		if(!empty($discountData)) {
            $discountLabelParams = $discountData->displayText->params;
			$discountLabel = $discountData->displayText->id;

			if(empty($discountLabel))$discountLabel='';
			
			$discount = $discountData->discountAmount;
			if(empty($discount))$discount = 0;
			
			$discount_type = $discountData->discountType;
			if(empty($discount_type)) $discount_type ='';
			
			if($discount>0) {
				$discount_value = $discount;
				$priceAfterDiscount=$result[0]['price'] - $discount;
				$discountPercentage = ($discount*100)/$result[0]['price'];
			}
		}

		$priceAfterDiscount=round($priceAfterDiscount);// round the DISCOUNTED PRICE
		$discount =round($discount);
			//If vtr info is not already passed, then this method has most likely been invoked for a very limited set of styles
			//in which case, get the vtr data from service for the style
			if(empty($vtrStyleData)){
				$vtrStyleData = VirtualTrialRoomClient::getStyleDataForIndexing($styleid);
				if(!empty($vtrStyleData) && !empty($vtrStyleData[$styleid])){
					$vtrStyleData = $vtrStyleData[$styleid];
				}
			}
       	
		/*sort fields based on revenue and potential revenue*/
		$styleVisibilityQuery = "SELECT * FROM mk_style_visibility_info WHERE style_id = '$styleid'";
		$styleVisibilityResult = func_query_first($styleVisibilityQuery, TRUE);
		$action_count_array = Array();
        $testVariants = array();
		if(!empty($styleVisibilityResult))
		{
			$action_count_array['Purchase'] = $styleVisibilityResult['style_sold_count'];
			$action_count_array['AddToCart'] = $styleVisibilityResult['style_added_to_cart_count'];
			$action_count_array['ProductView'] = $styleVisibilityResult['style_pdp_view_count'];
			$testVariants['revenue'] = $styleVisibilityResult['style_revenue'];
			$testVariants['revenue_adjusted'] = $styleVisibilityResult['style_revenue_adjusted'];
			$testVariants['real_revenue'] = $styleVisibilityResult['style_real_revenue'];
			$testVariants['style_store1'] = $styleVisibilityResult['style_store1'];
			$testVariants['style_store2'] = $styleVisibilityResult['style_store2'];
			$testVariants['style_store3'] = $styleVisibilityResult['style_store3'];
			$testVariants['style_store4'] = $styleVisibilityResult['style_store4'];
			$testVariants['style_store5'] = $styleVisibilityResult['style_store5'];
		}
		else 
		{
			$action_count_array['Purchase'] = 0;
			$action_count_array['AddToCart'] = 0;
			$action_count_array['ProductView'] = 0;
			$testVariants['revenue'] = 0;
			$testVariants['revenue_adjusted'] = 0;
			$testVariants['real_revenue'] = 0;
			$testVariants['style_store1'] = 0;
			$testVariants['style_store2'] = 0;
			$testVariants['style_store3'] = 0;
			$testVariants['style_store4'] = 0;
			$testVariants['style_store5'] = 0;
		}
    
		$realRevenue = $testVariants['real_revenue'];
			
		if(!isset($action_count_array['AddToCart']) || empty($action_count_array['AddToCart']) || $action_count_array['AddToCart'] == 0)
			$potentialRevenue = 0;
		else
			$potentialRevenue = ceil($priceAfterDiscount * $action_count_array['AddToCart']);
		
		$pvv1 = $styleVisibilityResult['pvv1'];
		$pvv2 = $styleVisibilityResult['pvv2'];
		$pvv3 = $styleVisibilityResult['pvv3'];
		$lowVisibilityThreshold = SearchPromotionKey::getFeatureGateKeyValuePairs("LowVisibilityThreshold");
		$lowVisibilityThreshold = (int)$lowVisibilityThreshold;
		if(strtolower($styleVisibilityResult['new']) == 'true'){
			$visibilityCategory = 'new';
		}else if(strtolower($styleVisibilityResult['replenished']) == 'true'){
			$visibilityCategory = 'replenished';
		}else if($styleVisibilityResult['sum_list_count'] < $lowVisibilityThreshold && (!empty($styleVisibilityResult))){
			$visibilityCategory = 'lowvisible';
		}else{
			$visibilityCategory = 'normal';
		} 
		
		$abuseFilter = new AbuseFilter();
		$psDoc = new Apache_Solr_Document();
		$uniqueid = $smid."_style_".trim($styleid);
		addValue($psDoc,'id',$uniqueid);
		addValue($psDoc,'productid','');
		addValue($psDoc,'product',!empty($result[0]['product_display_name'])?$result[0]['product_display_name']:$result[0]['name']);
		addValue($psDoc,'price',$result[0]['price']);
		addValue($psDoc,'imgfilename','');
		addValue($psDoc,'typeid',$result[0]['product_type']);
		addValue($psDoc,'typename',$result[0]['product_type_name']);
		$mandatoryFields['typename'] = $result[0]['product_type_name'];
		addValue($psDoc,'styleid',$result[0]['id']);
		addValue($psDoc,'stylename',$result[0]['name']);
		addValue($psDoc,'filtered_stylename', $abuseFilter->filter_common_words($result[0]['name']));
		addValue($psDoc,'image_portal_t','');
		//		addValue($psDoc,'myntrarating',10);
		addValue($psDoc,'myntrarating',!empty($result[0]['myntra_rating'])?$result[0]['myntra_rating']:10);
		addValue($psDoc,'img_portal_thumb_160','');
		addValue($psDoc,'keywords',!empty($result[0]['product_tags'])?$result[0]['product_tags']:'');
		addValue($psDoc,'filtered_keywords', $abuseFilter->filter_common_words($result[0]['product_tags'], ","));
		addValue($psDoc,'justarrived',0);
		addValue($psDoc,'userrating',round($averageRating));
		addValue($psDoc,'bestselling',!empty($result[0]['items_sold'])?$result[0]['items_sold']:0);
		addValue($psDoc,'descr',$result[0]['description']);
		addValue($psDoc,'fulldescr','');
		addValue($psDoc,'product_type','');
		addValue($psDoc,'shopmasterid',0);
		addValue($psDoc,'popup_image_path','');
        addValue($psDoc,'image_men_l','');
        addValue($psDoc,'image_men_s','');
        addValue($psDoc,'image_women_l','');
        addValue($psDoc,'image_women_s','');
        addValue($psDoc,'image_kid_l','');
        addValue($psDoc,'image_kid_s','');
        addValue($psDoc,'image_men_l','');
        addValue($psDoc, 'men_styleid' ,'');
        addValue($psDoc, 'women_styleid' ,'');
        addValue($psDoc, 'kid_styleid' ,'');
        if($result[0]['is_customizable'] == 1)
            $temp="customizable";
        else
            $temp="noncustomizable";
        addValue($psDoc,'search_type',$temp);
        addValue($psDoc,'generic_type',"style");
        addValue($psDoc,'discount_type',$discount_type);
		if(isset($discount_value) && !empty($discount_value) && is_numeric($discount_value)){
	        addValue($psDoc,'discount_value',$discount_value);
		}else{
			addValue($psDoc,'discount_value',0);
		}
        addValue($psDoc,'discount',$discount);
        addValue($psDoc,'discount_percentage',$discountPercentage);
        addValue($psDoc,"discounted_price",$priceAfterDiscount);
        addValue($psDoc,'discount_label',$discountLabel);
        
        if(isset($discountLabelParams)) {
            foreach($discountLabelParams as $key=>$value) {
			if(is_array($value)) {
				$v = json_encode($value);
			} else $v = $value;
			addValue($psDoc,'dre_'.$key, $v);
            }
            $dreDiscountLabel = $this->getDiscountLabelValue($discountLabel, $discountLabelParams);
        	addValue($psDoc,'dre_discount_label',$dreDiscountLabel);
        }

		if(!empty($sizes))
		{
			$size_options_array=explode(",",$sizes);
			$sizesString = implode("','", $size_options_array);
			$skuQuery = "select scsv.size_value, msosm.sku_id from mk_product_style mps, 
														 mk_styles_options_skus_mapping msosm, 
														 mk_product_options mpo,
														 size_chart_scale_value scsv
														 where mps.id = msosm.style_id and 
														 msosm.option_id = mpo.id and 
														 scsv.id = mpo.unified_size_value_id and 
														 mps.id = " . $styleid . " 
														 and scsv.size_value in ('" . $sizesString . "')";
			$skuResult = func_query($skuQuery,true);
			for($i = 0; $i < count($skuResult); $i++) {
				addValue($psDoc, 'skuForSize_'.$skuResult[$i]['size_value'], $skuResult[$i]['size_value'] . ":" . $skuResult[$i]['sku_id']);			
			}
		}

        addValue($psDoc,'product_sku_id',0);
        addValue($psDoc,'variant_name',!empty($result[0]['variant_name'])?$result[0]['variant_name']:'');
        addValue($psDoc,'categories',$visibilityCategory);
        addValue($psDoc,'search_zoom_image',!empty($result[0]['search_zoom_image'])?$result[0]['search_zoom_image']:'');
        addValue($psDoc,'search_image',!empty($result[0]['search_image'])?$result[0]['search_image']:'');
	if(!empty($result[0]['filters']))
	{
	        addValue($psDoc,'filters',str_replace(","," ",$result[0]['filters']));
	        addValue($psDoc,'filters_facet',str_replace(","," ",$result[0]['filters']));
	}
	else
	{
                addValue($psDoc,'filters',''); 
                addValue($psDoc,'filters_facet',''); 
        }
	//addValue($psDoc,'categories_filter',!empty($app_filters['Categories'])?str_replace(","," ",$app_filters['Categories']):'');
	addValue($psDoc,'brands_filter',!empty($app_filters['Brands'])?str_replace(","," ",$app_filters['Brands']):'');
	addValue($psDoc,'teamsleagues_filter',!empty($app_filters['TeamsLeagues'])?str_replace(","," ",$app_filters['TeamsLeagues']):'');
	addValue($psDoc,'sports_filter',!empty($app_filters['Sports'])?str_replace(","," ",$app_filters['Sports']):'');
	addValue($psDoc,'fit_filter',!empty($app_filters['Fit'])?str_replace(","," ",$app_filters['Fit']):'');
	addValue($psDoc,'team_filter',!empty($app_filters['Team'])?str_replace(","," ",$app_filters['Team']):'');
	addValue($psDoc,'themes_filter',!empty($app_filters['Themes'])?str_replace(","," ",$app_filters['Themes']):'');

		//addValue($psDoc,'categories_filter_facet',!empty($app_filters['Categories'])?str_replace(","," ",$app_filters['Categories']):'');
		//addValue($psDoc,'brands_filter_facet',!empty($app_filters_facet['Brands'])?str_replace(","," ",$app_filters_facet['Brands']):'');
		addValue($psDoc,'brands_filter_facet',$catResult['global_attr_brand']);
		$mandatoryFields['brands_filter_facet'] = $catResult['global_attr_brand'];
		addValue($psDoc,'teamsleagues_filter_facet',!empty($app_filters['TeamsLeagues'])?str_replace(","," ",$app_filters['TeamsLeagues']):'');
		addValue($psDoc,'sports_filter_facet',!empty($app_filters['Sports'])?str_replace(","," ",$app_filters['Sports']):'');
		addValue($psDoc,'fit_filter_facet',!empty($app_filters['Fit'])?str_replace(","," ",$app_filters['Fit']):'');
		addValue($psDoc,'team_filter_facet',!empty($app_filters['Team'])?str_replace(","," ",$app_filters['Team']):'');
		addValue($psDoc,'themes_filter_facet',!empty($app_filters['Themes'])?str_replace(","," ",$app_filters['Themes']):'');

		addValue($psDoc,'product_type_group',!empty($product_type_group[0]['name'])?$product_type_group[0]['name']:'');
			addValue($psDoc,'style_group',!empty($styleGroupStyleIds)?$styleGroupStyleIds:'');
		addValue($psDoc,'cod',($result[0]['style_cod']=='Y' && $result[0]['type_cod']=='Y')?'Y':'N');
		addValue($psDoc,'sizes',$sizes);
		addValue($psDoc,'sizes_facet',$sizes);
		addValue($psDoc,'sizeGroup',$sizeGroup);
		if (isset($count_options_available) && !empty($count_options_available) && is_numeric($count_options_available)
		 && $catResult['global_attr_master_category'] == "Accessories"){
		 	if(isset($realRevenue) && !empty($realRevenue) && is_numeric($realRevenue) && $realRevenue>0)
		 	{
		 		//echo"\n reached acc $maxSizeOption  left\n";
				addValue($psDoc, 'count_options_availbale', $maxSizeOption);
		 	}
		 	else
		 	{
		 		addValue($psDoc, 'count_options_availbale', $count_options_available);
		 	}
		}
		else if(isset($count_options_available) && !empty($count_options_available) && is_numeric($count_options_available)){
			addValue($psDoc, 'count_options_availbale', $count_options_available);
		}else{
			addValue($psDoc, 'count_options_availbale', 0);
		}
	/*
	addValue($psDoc, 'count_options_available', $count_options_available);
	//PORTAL-309
	addValue($psDoc, 'is_item_in_stock', $is_item_in_stock); */
		addValue($psDoc, 'count_product_viewed', !empty($action_count_array['ProductView']) ? $action_count_array['ProductView'] : '0');
		addValue($psDoc, 'count_added_to_cart', !empty($action_count_array['AddToCart']) ? $action_count_array['AddToCart'] : '0');
		addValue($psDoc, 'count_purchased', !empty($action_count_array['Purchase']) ? $action_count_array['Purchase'] : '0');

		//catalog classification fields
		addValue($psDoc, 'global_attr_article_type', $catResult['global_attr_article_type']);
		//for facet search
		addValue($psDoc, 'global_attr_article_type_facet', $catResult['global_attr_article_type']);
		$mandatoryFields['global_attr_article_type_facet'] = $catResult['global_attr_article_type'];
		addValue($psDoc, 'global_attr_sub_category', $catResult['global_attr_sub_category']);
		addValue($psDoc, 'global_attr_sub_category_facet', $catResult['global_attr_sub_category']);
		$mandatoryFields['global_attr_sub_category_facet'] = $catResult['global_attr_sub_category'];
		addValue($psDoc, 'global_attr_master_category', $catResult['global_attr_master_category']);
		addValue($psDoc, 'global_attr_master_category_facet', $catResult['global_attr_master_category']);
		$mandatoryFields['global_attr_master_category_facet'] = $catResult['global_attr_master_category'];
		addValue($psDoc, 'global_attr_brand', $catResult['global_attr_brand']);
		addValue($psDoc, 'global_attr_age_group', $catResult['global_attr_age_group']);

		//landing page url
		$landing_page_url = construct_style_url($catResult['global_attr_article_type'], $catResult['global_attr_brand'], $result[0]['name'], $result[0]['id']);
		addValue($psDoc, "dre_landing_page_url", $landing_page_url);
			
			
			if(!empty($vtrStyleData)){
				if(!empty($vtrStyleData["torsos"])){
					addValue($psDoc, 'torsos', $vtrStyleData['torsos']);
				}
				if(!empty($vtrStyleData["torso_types"])){
					addValue($psDoc, 'torso_types', $vtrStyleData['torso_types']);
				}
				if(!empty($vtrStyleData["client_types"])){
					addValue($psDoc, 'client_types', $vtrStyleData['client_types']);
				}
			}
			
			
		$gender = $catResult['global_attr_gender'];
		if(isset($catResult['global_attr_age_group']) && !empty($catResult['global_attr_age_group'])){
			$tokens = explode('-', $catResult['global_attr_age_group']);
			if(!empty($tokens[1])){
				if($tokens[1] == 'Unisex' && $tokens[0] == 'Adults'){
					$gender	="men women";
				}
				else if($tokens[1] == 'Unisex' && $tokens[0] == 'Kids'){
					$gender	="boys girls";
				}
				else{
					$gender = $tokens[1];
				}
			}else{
				if($catResult['global_attr_gender'] == 'Unisex'){
					$gender	="men women";
				}else{
					$gender = $catResult['global_attr_gender'];
				}
			}
		}
		addValue($psDoc, 'global_attr_gender', strtolower($gender));
		$mandatoryFields['global_attr_gender'] = strtolower($gender);
		addValue($psDoc, 'global_attr_base_colour', $catResult['global_attr_base_colour']);
		addValue($psDoc, 'global_attr_colour1', $catResult['global_attr_colour1']);
		addValue($psDoc, 'global_attr_colour2', $catResult['global_attr_colour2']);
        
        // Update color groups in solr
        if(empty($colourFamilyList)){
            $colour_family_query = 
                "select ".
                        "atv.attribute_value as attribute_value, ".
                        "group_concat(atf.family_name separator ',') as family_name, ".
                        "group_concat(atf.swatch_position separator ',') as family_swatch_position ".
                    "from ".
                        "mk_attribute_type_values atv ".
                            "left join attribute_type_family_member atfm ".
                                "on ".
                                "atv.id=atfm.id_attribute_type_value ".
                            "left join attribute_type_family atf ".
                                "on ".
                                "atfm.id_attribute_type_family=atf.id ".
                    "where atv.attribute_type_id=4 and atv.attribute_value in ('". 
                        $catResult['global_attr_base_colour']."','".
                        $catResult['global_attr_colour1']."','".
                        $catResult['global_attr_colour2']."'".
                        ") group by atv.attribute_value";
            $colourFamilyList_out = func_query($colour_family_query,true);
            $colourFamilyList = array();
            foreach ($colourFamilyList_out as $item_colour) {
                $colourFamilyList[$item_colour['attribute_value']] = $item_colour['family_name'];
            }
            $colourFamilySwatchList = array();
            foreach ($colourFamilyList_out as $item_colour) {
                $colourFamilySwatchList[$item_colour['attribute_value']] = $item_colour['family_swatch_position'];
            }
        }

        $colours_str="";
        if(!empty($catResult['global_attr_base_colour'])){
            $colours_str .=$colourFamilyList[$catResult['global_attr_base_colour']];
        }
        
        if(!empty($catResult['global_attr_colour1'])){
            $colours_str .= "," . $colourFamilyList[$catResult['global_attr_colour1']];
        }

        if(!empty($catResult['global_attr_colour2'])){
            $colours_str .= "," . $colourFamilyList[$catResult['global_attr_colour2']];
        }

        $colour_family_list = implode("," , array_unique(explode(",",$colours_str)));

        $colours_swatch_str="";
        if(!empty($catResult['global_attr_base_colour'])){
            $colours_swatch_str .=$colourFamilySwatchList[$catResult['global_attr_base_colour']];
        }
        
        if(!empty($catResult['global_attr_colour1'])){
            $colours_swatch_str .= "," . $colourFamilySwatchList[$catResult['global_attr_colour1']];
        }

        if(!empty($catResult['global_attr_colour2'])){
            $colours_swatch_str .= "," . $colourFamilySwatchList[$catResult['global_attr_colour2']];
        }

        $colour_family_swatch_position_list = implode("," , array_unique(explode(",",$colours_swatch_str)));
        
		addValue($psDoc, 'colour_family_list', $colour_family_list);
		addValue($psDoc, 'colour_family_swatch_position_list', $colour_family_swatch_position_list);
    
		addValue($psDoc, 'global_attr_usage', $catResult['global_attr_usage']);
		addValue($psDoc, 'global_attr_season', $catResult['global_attr_season']);
		
		if(!empty($catResult['global_attr_year']))
			addValue($psDoc, 'global_attr_year', $catResult['global_attr_year']);
		else
			addValue($psDoc, 'global_attr_year', '2006');
		if(!empty($catResult['global_attr_fashion_type'])){
			addValue($psDoc, 'global_attr_fashion_type', $catResult['global_attr_fashion_type']);
		}else{
			addValue($psDoc, 'global_attr_fashion_type', "Core");
		}
		if(!empty($catResult['catalog_date']))
		{
			$catalog_date = date('Y-m-d\TH:i:s\Z',$catResult['catalog_date']);
			addValue($psDoc, 'global_attr_catalog_add_date', $catalog_date);
		}
		else
		{
			$cur_date=date('Y-m-d\TH:i:s\Z',time());
			addValue($psDoc, 'global_attr_catalog_add_date', $cur_date);
		}

		/*
		 * add article specific attributes
		 * */

		$asaQuery = "select at.attribute_type as attribute_type,atv.attribute_value as attribute_value
					from mk_style_article_type_attribute_values satv
						left join mk_attribute_type_values atv on satv.article_type_attribute_value_id=atv.id 
						left join mk_attribute_type at on atv.attribute_type_id=at.id
					where satv.product_style_id='".$styleid."'";
		$asaResults = func_query($asaQuery,true);
		foreach($asaResults as $asaResult){
			$attribute_type=str_replace(" ","_",$asaResult[attribute_type]);
			addValue($psDoc, $attribute_type.'_article_attr', strtolower($asaResult['attribute_value']));
			addValue($psDoc, $attribute_type.'_article_attr_text', strtolower($asaResult['attribute_value']));
		}


        /**
        * introducing 5 more test variants for default search
        */
		$genderInfo = explode('-', $catResult['global_attr_age_group']);
		$unisexToFemale = (FeatureGateKeyValuePairs::getInteger('UnisexRevenueAsFemale'))/100;
		$maleToFemale = (FeatureGateKeyValuePairs::getInteger('MaleRevenueAsFemale'))/100;
        foreach ($testVariants as $testVariant => $value) {
            if(isset($value) && !empty($value) && is_numeric($value)) {
                addValue($psDoc, $testVariant.'_sort_field', $value);

                $maleRev = 0;
                $femaleRev =0;
                if($genderInfo[1] ==  "Men" || $genderInfo[1] == "Boys" )
                {
                    $maleRev = $value;
                    $femaleRev = (int) ($maleToFemale*$value);
                }
                else if($genderInfo[1] ==  "Women" || $genderInfo[1] ==  "Girls")
                {
                    $femaleRev = $value;
                    $maleRev = $value;
                }
                else
                {
                    $maleRev = (int) $value*(1-$unisexToFemale);
                    $femaleRev = (int)$value*$unisexToFemale;
                }
                $maleRev = ceil($maleRev);
                $femaleRev = ceil($femaleRev);
                addValue($psDoc, $testVariant.'_male_sort_field', $maleRev);
                addValue($psDoc, $testVariant.'_female_sort_field', $femaleRev);
            } else {
                addValue($psDoc, $testVariant.'_sort_field', 0);
                addValue($psDoc, $testVariant.'_male_sort_field', 0);
                addValue($psDoc, $testVariant.'_female_sort_field', 0);
            }
        }

		addValue($psDoc, 'pvv1_sort_field', $pvv1);
		addValue($psDoc, 'pvv2_sort_field', $pvv2);
		addValue($psDoc, 'pvv3_sort_field', $pvv3);
		
		if(isset($potentialRevenue) && !empty($potentialRevenue) && is_numeric($potentialRevenue)){
			$malePotRev = 0;
			$femalePotRev =0;
			if($genderInfo[1] ==  "Men" || $genderInfo[1] == "Boys" )
			{
				$malePotRev = $potentialRevenue;
				$femalePotRev = (int) ($maleToFemale*$potentialRevenue);
			}
			else if($genderInfo[1] ==  "Women" || $genderInfo[1] ==  "Girls")
			{
				$femalePotRev = $potentialRevenue;
				$malePotRev = $potentialRevenue;
			}
			else
			{
				$malePotRev = (int)$potentialRevenue*(1-$unisexToFemale);
				$femalePotRev = (int) $potentialRevenue*$unisexToFemale;
			}
			//echo"\n male $maleRev female $femaleRev  rev $realRevenue";
			$malePotRev = ceil($malePotRev);
			$femalePotRev = ceil ($femalePotRev);
			addValue($psDoc, 'potential_revenue_sort_field', $potentialRevenue);
			addValue($psDoc, 'potential_revenue_male_sort_field', $malePotRev);
			addValue($psDoc, 'potential_revenue_female_sort_field', $femalePotRev);
		}else{
			addValue($psDoc, 'potential_revenue_sort_field', 0);
			addValue($psDoc, 'potential_revenue_male_sort_field', 0);
			addValue($psDoc, 'potential_revenue_female_sort_field', 0);
		}

		/*---------------------------------------------------------------------------------------------------------*/


		addValue($psDoc, 'display_category', $catResult['display_categories']);
		addValue($psDoc, 'display_category_facet', (isset($catResult['display_categories']) && !empty($catResult['display_categories']))? $catResult['display_categories'] : "");
		
		addValue($psDoc, 'main_category', '');
		//addValue($psDoc, 'categories', '');
		addValue($psDoc, 'statusid', '1008');
		addValue($psDoc, 'designer', '');
		addValue($psDoc, 'product_price', '');

		if(!empty($result[0]['add_date']))
		{
			$add_date=date('Y-m-d\TH:i:s\Z',$result[0]['add_date']);
			addValue($psDoc, 'add_date', $add_date);
		}
		else
		{
			$cur_date=date('Y-m-d\TH:i:s\Z',time());
			addValue($psDoc, 'add_date', $cur_date);
		}
 
		addValue($psDoc, 'productcode', 0);
		addValue($psDoc, 'quantity_sold', 0);
		addValue($psDoc, 'target_url', $result[0]['target_url']);
		
		$allMandatoryFieldsPresent = true;
		foreach ($mandatoryFields as $key => $value) {
			$value = trim($value);
			if($value == '') {
				$allMandatoryFieldsPresent = false;
			}
		}

		$documents[] = $psDoc;
		if($flush){
			ob_flush();
			flush();
		}
		if($allMandatoryFieldsPresent){
			$this->publishToAllServers($documents,$only_local);
		}
	}
	}
	}

	public function publishToAllServers($documents,$only_local=false) {
		global $solr_servers;
		global $solrlog;
		$style_id_list='';
		foreach($documents as $document_tmp){
			$style_id_list.='['.$document_tmp->styleid.']';	
		}
		if($only_local){
			$solr = new solrProducts();
			$out_res = $solr->loadDocs($documents);
		}else{
			foreach ($solr_servers as $server) {
				$solr = new solrProducts($server);
				$out_res = $solr->loadDocs($documents);
				$solrlog->info("Server : ". $server);
				$solrlog->info("Style ids :". $style_id_list);
				$solrlog->info("SolrResponse Start: -----------------------------------");
				$solrlog->info(print_r($out_res,true));
				$solrlog->info("SolrResponse End: -----------------------------------");
			}
		}
	}

	public function getDiscountLabelValue($discountLabel, $discountLabelParams) {
		$dreDiscountLabel = '';
		$discountLabelParams = (array) $discountLabelParams;
		switch($discountLabel) {
			case 'BuyX_GetY_Search':
				$buyCount = $discountLabelParams['buyCount'] - $discountLabelParams['count'];
				$count = $discountLabelParams['count'];
				$dreDiscountLabel = "(Buy $buyCount Get $count Free)";
				break;
			case 'Flat_Search_Percent':
				$dreDiscountLabel = "(".$discountLabelParams['percent']."% OFF)";
				break;
			case 'Flat_Search_Amount':
				$dreDiscountLabel = "(".$discountLabelParams['amount']." OFF)";
				break;
			case 'FC_Search_Percent':
				$dreDiscountLabel = "<em>".$discountLabelParams['percent']."% OFF *</em>";
				break;
			case 'FC_Search_Amount':
				$dreDiscountLabel = "<em>".$discountLabelParams['amount']." OFF *</em>";
				break;
			case 'BuyCount_GetCount_Search':
				$dreDiscountLabel = "(Add ".$discountLabelParams['buyCount']." Get ".$discountLabelParams['count']." Free)";
				break;
			case 'NoCondition_GetPercent_Search':
				$dreDiscountLabel = "(".$discountLabelParams['percent']."% OFF)";
				break;
			case 'NoCondition_GetAmount_Search':
				$dreDiscountLabel = "(".$discountLabelParams['amount']." OFF)";
				break;
			case 'BuyCount_GetPercent_Search':
				$dreDiscountLabel = "<em>".$discountLabelParams['percent']."% OFF *</em>";
				break;
			case 'BuyCount_GetAmount_Search':
				$dreDiscountLabel = "<em>".$discountLabelParams['amount']." OFF *</em>";
				break;
			case 'BuyAmount_GetPercent_Search':
				$dreDiscountLabel = "<em>".$discountLabelParams['percent']."% OFF *</em>";
				break;
			case 'BuyAmount_GetAmount_Search':
				$dreDiscountLabel = "<em>".$discountLabelParams['amount']." OFF *</em>";
				break;
			case 'Free_Item_Search':
				$dreDiscountLabel = '<div class=\'gift red-text right\'>Get <b>Free Gift</b><i>&nbsp;</i></div>';
				break;
			case 'Free_Item_Search_C':
				$dreDiscountLabel = '<div class=\'gift red-text right\'>Get <b>Free Gift</b>*<i>&nbsp;</i></div>';
				break;
			case 'BuyCount_GetFreeunlistedItem_Search':
				$dreDiscountLabel = '<div class=\'gift red-text right\'>Get <b>Free Gift</b>*<i>&nbsp;</i></div>';
				break;
			case 'BuyAmount_GetFreeunlistedItem_Search':
				$dreDiscountLabel = '<div class=\'gift red-text right\'>Get <b>Free Gift</b>*<i>&nbsp;</i></div>';
				break;
			case 'NoCondition_GetFreeunlistedItem_Search':
				$dreDiscountLabel = '<div class=\'gift red-text right\'>Get <b>Free Gift</b><i>&nbsp;</i></div>';
				break;
		}
		return $dreDiscountLabel;
	}

	public function deleteProductFromIndex($productid)
	{
		$query = "select product_style_id from xcart_products where productid=$productid";
		$res = db_query($query,true);
		$row = db_fetch_row($res);
		$productStyleId = $row[0];

		//delete from online
		$documentId = "0"."_design_".$productid;
		$this->deleteDocumentFromIndex($documentId);
	}
	
	public function deleteStyleFromIndex($styleid='',$all=false,$commitToSolr=false)
	{
		$doc_id="0_style_".$styleid;
		$this->deleteDocumentFromIndex($doc_id,$commitToSolr);
	}
	
	public function deleteDocumentFromIndex($documentId,$commitToSolr=false)
	{
		global $solr_servers;
		global $solrlog;
		global $xcart_dir;
		// invalidate the pdp cache
		$styleId = substr($documentId, 8);
		include_once("$xcart_dir/Cache/Cache.php");
                include_once("$xcart_dir/Cache/CacheKeySet.php");
		$cache = new Cache(PDPCacheKeys::$prefix.$styleId, PDPCacheKeys::keySet());
		$cache->invalidate();
		
		foreach($solr_servers as $server){
			$solr = new solrProducts($server);
			$out_res=$solr->removeById($documentId);
			$solrlog->info("Server : ". $server);
			$solrlog->info("Deleting Style ids :". $documentId);
			$solrlog->info("SolrResponse Start: -----------------------------------");
			$solrlog->info(print_r($out_res,true));
			$solrlog->info("SolrResponse End: -----------------------------------");
			if($commitToSolr==true)
				$solr->commitIndex();
		}
	}
	private function getProductDocuments($shopMasterId="",$lb,$size)
	{
		global $sql_tbl;
		$productTable = $sql_tbl["products"];
		$producttype = $sql_tbl["mk_product_type"];
		$imageTable = $sql_tbl["images_T"];
		$styleTable = $sql_tbl["mk_product_style"];


		$query = "select
		$productTable.productid as productid,
		$productTable.product product,
                                        FORMAT($styleTable.price+IFNULL($styleTable.price*$productTable.markup/100,0),2) as price, 
                                        $imageTable.filename as imgfilename,
                                        $producttype.id as typeid,
                                        $producttype.name as typename,
                                        $productTable.product_style_id as styleid,
                                        $styleTable.name as stylename,
                                        $productTable.image_portal_t,
                                        $productTable.myntra_rating,
                                        $productTable.image_portal_thumbnail_160 as img_portal_160,
                                        $productTable.keywords as keywords,
                                        $productTable.add_date as justarrived,
                                        $productTable.rating as userrating,
                                        $productTable.sales_stats as bestselling,
                                        $productTable.descr as descr,
                                        $productTable.fulldescr as fulldescr,

                                        $producttype.type as product_type
					from $productTable,$imageTable ,$producttype ,$styleTable";
                                        
                                        $query .= " where $productTable.productid = $imageTable.id
                                          and $productTable.product_type_id = $producttype.id
                                          and $styleTable.id = $productTable.product_style_id and statusid=1008 
                                           ";

                                        $query .= " limit $lb,$size";
                                        $documents = $this->getProductDocumentsEx($shopMasterId,$query);
                                        echo $lb.' '.count($documents).'??';
                                        ob_flush();
                                        flush();
                                        return $documents;
	}

	private function getProductDocumentsEx($shopMasterId="",$query)
	{

		$res = db_query($query,true);

		//$sdata = mysql_fetch_array($res);

		$documents = array();
		while( $sdata = db_fetch_row($res)  )
		{

			// Fetch popup image path  for this product id
			$popupQuery = "SELECT popup_image_path,image_men_l,image_men_s,image_women_l,image_women_s,image_kid_l,image_kid_s
                                FROM mk_xcart_product_customized_area_rel a, mk_style_customization_area b, xcart_products c 
                                WHERE a.product_id = '$sdata[0]'
                                AND a.product_id = c.productid
                                AND a.style_id = b.style_id
                                AND a.area_id = b.id
				AND b.isprimary = 1
                                ";

			$pResult = db_query($popupQuery,true);
			$prow = db_fetch_row($pResult);
				
			$productStyleId = $sdata[6];
			$smdata = array();
			$psDoc = $this->createProductDocument($sdata,$smdata,$prow);
			$documents[] = $psDoc;
		}
		return $documents;
	}
	
	private function createProductDocument($sdata,$smdata,$prow)
	{
		$smid = 0;
		$price  = $sdata[2];
		if( !empty( $smdata ) )  // overwrite with store price and set proper shop master id
		{
			$smid = $smdata[0];
			$price = $smdata[1];
		}
		$psDoc = new Apache_Solr_Document();
		$uniqueid = $smid."_design_".$sdata[0];
		addValue($psDoc,'id',$uniqueid);
		addValue($psDoc,'productid',$sdata[0]);
		addValue($psDoc,'product',$sdata[1]);

		$price = str_replace(",","",$price);
		addValue($psDoc,'price',$price);
		addValue($psDoc,'imgfilename',$sdata[3]);
		addValue($psDoc,'typeid',$sdata[4]);
		addValue($psDoc,'typename',$sdata[5]);
		addValue($psDoc,'styleid',$sdata[6]);
		addValue($psDoc,'stylename',$sdata[7]);
		addValue($psDoc,'image_portal_t',$sdata[8]);
		if( empty($sdata[9]) )
		$temp = 0;
		else
		$temp = $sdata[9];
		addValue($psDoc,'myntrarating',$temp);
		addValue($psDoc,'img_portal_thumb_160',$sdata[10]);
		addValue($psDoc,'keywords',$sdata[11]);
		if(empty($sdata[12]))
		$temp = 0;
		else
		$temp = $sdata[12];
		addValue($psDoc,'justarrived',$temp);
		if( empty($sdata[13]) )
		$temp = 0;
		else
		$temp = $sdata[13];
		addValue($psDoc,'userrating',$temp);

		if( empty($sdata[14]))
		$temp = 0;
		else
		$temp = $sdata[14];
		addValue($psDoc,'bestselling',$temp);
		addValue($psDoc,'descr',$sdata[15]);
		addValue($psDoc,'fulldescr',$sdata[16]);
		addValue($psDoc,'product_type',$sdata[17]);
		addValue($psDoc,'shopmasterid',$smid);
		addValue($psDoc,'popup_image_path',$prow[0]);

		addValue($psDoc,'image_men_l',$prow[1]);
		addValue($psDoc,'image_men_s',$prow[2]);
		addValue($psDoc,'image_women_l',$prow[3]);
		addValue($psDoc,'image_women_s',$prow[4]);
		addValue($psDoc,'image_kid_l',$prow[5]);
		addValue($psDoc,'image_kid_s',$prow[6]);
		addValue($psDoc,'brands_filter',"myntra Brands");
		addValue($psDoc,'teamsleagues_filter',"");
		addValue($psDoc,'brands_filter_facet',"myntra Brands");
		addValue($psDoc,'filters_facet',"myntra");
		if(isset($price) && !empty($price) && is_numeric($price)){
			addValue($psDoc, 'discounted_price', $price);
		}else{
			addValue($psDoc, 'discounted_price', 0);
		}
		addValue($psDoc,'cod',($sdata[20] == 'Y' && $sdata[21]=='Y')?'Y':'N');

		// For style ids ..
		$groupid = array();
		$groupid["mens"] = array("55", "58", "59", "61", "62");
		$groupid["womens"] = array("56", "60");
		$groupid["kids"] = array("57");

		$sql = "select style_group_id from mk_stylegroup_style_map where style_id=" . $sdata[6];
		$result = mysql_query($sql);
		$ids_for_currentstyle = array();
		while ($row = mysql_fetch_array($result))
		{
			$ids_for_currentstyle[] = $row['style_group_id'];
		}
		foreach ($ids_for_currentstyle as $id)
		{
			if (in_array($id, $groupid["mens"])) {
				addValue($psDoc, 'men_styleid' ,$sdata[6]);
				addValue($psDoc, 'women_styleid' ,7);
				addValue($psDoc, 'kid_styleid' ,67);
				addValue($psDoc, 'usergroup', 'Men');
				break;
			}
			else if (in_array($id, $groupid["womens"])) {
				addValue($psDoc, 'men_styleid' ,3);
				addValue($psDoc, 'women_styleid' ,$sdata[6]);
				addValue($psDoc, 'kid_styleid' ,67);
				addValue($psDoc, 'usergroup', 'Women');
				break;
			}
			else if (in_array($id, $groupid["kids"])) {
				addValue($psDoc, 'men_styleid' ,3);
				addValue($psDoc, 'women_styleid' ,7);
				addValue($psDoc, 'kid_styleid' ,$sdata[6]);
				addValue($psDoc, 'usergroup', 'Kid');
				break;
			}
		}

		addValue($psDoc,'search_type','design');
		addValue($psDoc,'generic_type',"design");

		// For admin
		global $sql_tbl;
		$product = func_select_first($sql_tbl["products"], array('productid' => $sdata[0]));
		$main_category = '';
		$categories = '';
		$product_categories = func_select_query($sql_tbl["products_categories"], array('productid' => $sdata[0]));
		if ($product_categories) {
			foreach ($product_categories as $c) {
				$category = func_select_first($sql_tbl["categories"], array('categoryid' => $c['categoryid']));
				$categories .= $category['category'] . ' ';
				if($c['main'] == 'Y'){
					$main_category = $c['categoryid'];
				}
			}
		}
		addValue($psDoc, 'main_category', $main_category);
		//addValue($psDoc, 'categories', $categories);
		addValue($psDoc, 'statusid', $product['statusid']);
		addValue($psDoc, 'designer', $product['designer']);
		addValue($psDoc, 'product_price', $price);
		addValue($psDoc, 'add_date', date('Y-m-d\TH:i:s\Z', (int)$product['add_date']));
		addValue($psDoc, 'productcode', $product['productcode']);
		addValue($psDoc, 'quantity_sold', empty($product['quantity_sold']) ? 0 : $product['quantity_sold']);
		// For admin sorting ..
		addValue($psDoc, 'sku', $product['productcode']);
		addValue($psDoc, 'title', $sdata[1]);
		addValue($psDoc, 'style', $sdata[7]);
		addValue($psDoc, 'qtysold', empty($product['quantity_sold']) ? 0 : $product['quantity_sold']);
		addValue($psDoc, 'status', empty($product['statusid']) ? 0 : $product['statusid']);


		return $psDoc;
	}

	public function reIndexProducts()
	{
		$this->emptyIndex();
		$this->indexProducts();
	}

	/*
	 * $columns - array of columns required as output, if null or empty then all columns will be returned
	 * $styleIds - array of styleIds
	 * $sortField - solr field on which we would want to sort the data on
	 */
	public function getCustomizedDataForStyles($styleIds, $columns = null, $sortField = '') {

		if($styleIds==null || !is_array($styleIds) || empty($styleIds))
			return null;
			
		$query = '(styleid:(';
		$orStatement = '';
		foreach ($styleIds as $sId) {
			$query = $query . $orStatement . '"'.$sId.'"';
			$orStatement = ' OR ';
		}
		$query = $query . ') AND count_options_availbale:[1 TO *])';
		
		$params = array();
		if(is_array($columns) && !empty($columns)){
				$params["fl"] = implode(",", $columns); 
		}
		
		$sResults = $this->searchAndSort($query, 0, 1000, $sortField, $params);
		
		if(empty($sResults))
			return array();

		$products = documents_to_array($sResults->docs);	
		$products = WidgetUtility::getArraySolrFormattedData($products);
	
		return $products;	
	}
        public function getComboRelatedStyleId($combo_id, $columns = null, $sortField = '') {

	
		$query = '(dre_comboId:(';
		$query = $query . '"'.$combo_id.'"';
			
		
		$query = $query . ') AND count_options_availbale:[1 TO *])';
		
		$params = array();
		if(is_array($columns) && !empty($columns)){
				$params["fl"] = implode(",", $columns); 
		}
		
		$sResults = $this->searchAndSort($query, 0, 10000000, $sortField, $params);
		
		if(empty($sResults))
			return array();

		$products = documents_to_array($sResults->docs);	
		$products = WidgetUtility::getArraySolrFormattedData($products);
	
		return $products;	
	}
}


