<?php
include_once('../../auth.php');
include_once('solrProducts.php');
if (isset($_GET['id'])) {
    $id = $_GET['id'];
    $solrProducts = new solrProducts();
    $solrProducts->addProductToIndex($id);
}