<?php
/*****************************************************************************\
+-----------------------------------------------------------------------------+
| X-Cart                                                                      |
| Copyright (c) 2001-2006 Ruslan R. Fazliev <rrf@rrf.ru>                      |
| All rights reserved.                                                        |
+-----------------------------------------------------------------------------+
| PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
| FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
| AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
|                                                                             |
| THIS  AGREEMENT  EXPRESSES  THE  TERMS  AND CONDITIONS ON WHICH YOU MAY USE |
| THIS SOFTWARE   PROGRAM   AND  ASSOCIATED  DOCUMENTATION   THAT  RUSLAN  R. |
| FAZLIEV (hereinafter  referred to as "THE AUTHOR") IS FURNISHING  OR MAKING |
| AVAILABLE TO YOU WITH  THIS  AGREEMENT  (COLLECTIVELY,  THE  "SOFTWARE").   |
| PLEASE   REVIEW   THE  TERMS  AND   CONDITIONS  OF  THIS  LICENSE AGREEMENT |
| CAREFULLY   BEFORE   INSTALLING   OR  USING  THE  SOFTWARE.  BY INSTALLING, |
| COPYING   OR   OTHERWISE   USING   THE   SOFTWARE,  YOU  AND  YOUR  COMPANY |
| (COLLECTIVELY,  "YOU")  ARE  ACCEPTING	AND AGREEING  TO  THE TERMS OF THIS |
| LICENSE   AGREEMENT.   IF  YOU    ARE  NOT  WILLING   TO  BE  BOUND BY THIS |
| AGREEMENT, DO  NOT INSTALL OR USE THE SOFTWARE.  VARIOUS   COPYRIGHTS   AND |
| OTHER   INTELLECTUAL   PROPERTY   RIGHTS    PROTECT   THE   SOFTWARE.  THIS |
| AGREEMENT IS A LICENSE AGREEMENT THAT GIVES  YOU  LIMITED  RIGHTS   TO  USE |
| THE  SOFTWARE   AND  NOT  AN  AGREEMENT  FOR SALE OR FOR  TRANSFER OF TITLE.|
| THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY GRANTED BY THIS AGREEMENT.      |
|                                                                             |
| The Initial Developer of the Original Code is Ruslan R. Fazliev             |
| Portions created by Ruslan R. Fazliev are Copyright (C) 2001-2006           |
| Ruslan R. Fazliev. All Rights Reserved.                                     |
+-----------------------------------------------------------------------------+
\*****************************************************************************/

#
# $Id: login.php,v 1.120.2.4 2006/06/16 13:00:50 max Exp $
#

include "../top.inc.php";
include("./func/func.mkcore.php");
include_once "$xcart_dir/include/func/func.loginhelper.php";

if (!defined('XCART_START')) die("ERROR: Can not initiate application! Please check configuration.");

require_once $xcart_dir."/init.php";
#require_once $xcart_dir."/include/nocookie_warning.php";
require_once($xcart_dir.'/include/sessionstore/session_store_wrapper.php');

x_load('crypt','mail');

x_session_register("logout_user");
x_session_register("login");
x_session_register("userfirstname");

x_session_register("login_type");
x_session_register("logged");
x_session_register("previous_login_date");

x_session_register("login_attempt");
x_session_register("cart");
x_session_register("intershipper_recalc");

x_session_register("merchant_password");
x_session_register("account_type"); 
x_session_register("master_name"); 
 
$merchant_password = "";

$login_error = false;

switch ($redirect) {
	case "admin":
		$redirect_to = DIR_ADMIN;
		$current_type = 'A';
		break;
	case "provider":
		$redirect_to = DIR_PROVIDER;
		$current_type = 'P';
		break;
	case "partner":
		$redirect_to = DIR_PARTNER;
		$current_type = 'B';
		break;
	case "customer":
	default:
		$redirect_to = DIR_CUSTOMER;
		$current_type = 'C';
}
$redirect_to = $current_location.$redirect_to;
if ($REQUEST_METHOD == "POST") {
	$intershipper_recalc = "Y";

	x_session_register("identifiers",array());
	if ($mode == "login") {

		$username = $HTTP_POST_VARS["username"];
		
		$userAuthManager = new user\UserAuthenticationManager();
		$authenticationSuccess = false;
		$password = $HTTP_POST_VARS["password"];
		try{
			if($usertype==='A'){
				$user = $userAuthManager->performUserSignIn($username, $password ,user\User::USER_TYPE_ADMIN);
				$authenticationSuccess = true;
			}else if($usertype==='P'){
				$user = $userAuthManager->performUserSignIn($username, $password ,user\User::USER_TYPE_PROVIDER);
				$authenticationSuccess = true;
			}
			
		}catch(user\exception\UserDoesNotExistsException $ex){
			$authenticationSuccess = false;
			$userExists = false;
		}catch(user\exception\PasswordDoesNotMatchException $ex){
			$authenticationSuccess = false;
			$userExists = true;
		}
		
		if($authenticationSuccess === true){
			$user_data = func_query_first("SELECT * FROM $sql_tbl[customers] WHERE login='".mysql_real_escape_string($username)."' AND usertype='".mysql_real_escape_string($usertype)."' AND status='Y'");
			$allow_login = true;
			//added to enable role based access
			$roleQuery="select mr.type as role,mr.allowed_actions as actions from mk_roles mr,mk_user_roles mur where mur.roleid=mr.id and login='".mysql_real_escape_string($username)."'";
			$roleresult=db_query($roleQuery);
			$roles=array();
			if ($roleresult)
			{
				$i=0;

				while ($row=db_fetch_array($roleresult))
				{
					$roles[$i] = $row;
					$i++;
				}
			}
			$XCART_SESSION_VARS['user_roles']=$roles;

			if ($usertype == 'A' || ($usertype == "P" && $active_modules["Simple_Mode"])) {
				$iplist = array_unique(split('[ ,]+', $admin_allowed_ip));
				$iplist = array_flip($iplist);
				unset($iplist[""]);
				$iplist = array_flip($iplist);
				if (count($iplist) > 0)
				$allow_login = in_array($REMOTE_ADDR, $iplist);
			}

			$right_password = $user_data["password"];
		}
		if (!empty($user_data) && $authenticationSuccess === true && !empty($password) && $allow_login) {

			 $firstname =  $user_data['firstname'];
			 $lastname =  $user_data['lastname'];
			 $account_type = $user_data['account_type'];
			 unset($right_password);
			 $identifiers[$usertype] = array (
				'login' => $username,
                'firstname' => $firstname,
                'lastname' => $lastname,
				'login_type' => $usertype,
				'account_type' => $account_type
			 );
			 $accountType=0;

			 if (!empty($active_modules['Simple_Mode'])) {
			 	if ($usertype == 'A') $identifiers['P'] = $identifiers[$usertype];
			 	if ($usertype == 'P') $identifiers['A'] = $identifiers[$usertype];
			 }
#
# Success login
#
			#db_query("update mk_session set updated='Y' where sessionid= '$XCARTSESSID'");
			updateSessionUpdatedFlagBySessionId($XCARTSESSID, 'Y');
			x_session_register("login_change");
			if ($user_data["change_password"] == "Y") {
				$login_change["login"] = $user_data["login"];
				$login_change["login_type"] = $usertype;
				func_header_location($redirect_to."/change_password.php");
			}
			x_session_unregister("login_change");
          
			$login = $user_data["login"];  //$username; 
			$login_type = $usertype;
            $userfirstname = $user_data["firstname"];  //user first name
            
			$logged = "";
			

			          
			#
			# 1) generate $last_login by current timestamp and update database
			# 2) insert entry into login history
			#
			$tm = time();
			db_query("REPLACE INTO $sql_tbl[login_history] (login, date_time, usertype, action, status, ip) VALUES ('$username','$tm','$usertype','login','success','$REMOTE_ADDR')");


			$logout_user = false;
			
			if ($login_type == "A" || $login_type == "P") {
				$to_url = (!empty($active_modules["Simple_Mode"]) || $login_type == "A" ? $xcart_catalogs["admin"] : $xcart_catalogs["provider"])."/home.php";
				$current_area = $login_type;
			}
			
		
			if($login_type == "A")
			{
				func_header_location($xcart_web_dir."/admin/home.php");
			}
			else if($login_type == "P")
			{
				func_header_location($xcart_web_dir."/provider/home.php");
			}
		
		} else {
#
# Login incorrect
#	
			$incorrect_password = false;
			$login_status = "failure";
					
			if(!empty($user_data) && $password != $right_password){
				$incorrect_password = true;	
			}

			unset($right_password);

			if (!$allow_login)
				$login_status = "restricted";

			$disabled = func_query_first_cell("SELECT COUNT(*) FROM $sql_tbl[customers] WHERE login='$username' AND usertype='$usertype' AND status<>'Y' AND status<>'A'");
			if ($disabled) {
				x_session_register('top_message');
				$top_message['type'] = 'I';
				$top_message['content'] = "Account is Disabled!!";
				$login_status = 'disabled';
			}

			if ($userExists === true)
				db_query("REPLACE INTO $sql_tbl[login_history] (login, date_time, usertype, action, status, ip) VALUES ('$username','".time()."','$usertype','login','$login_status', '$REMOTE_ADDR')");

			if (($redirect == "admin" || (@$active_modules["Simple_Mode"] == "Y" && $redirect == "provider")) && $config['Email_Note']['eml_login_error'] == 'Y') {
				#
				# Send security alert to website admin
				#
				@func_send_mail($config["Company"]["site_administrator"], "mail/login_error_subj.tpl", "mail/login_error.tpl", $config["Company"]["site_administrator"], true);

			}

			if($current_type == "A"){
				func_header_location($xcart_web_dir."/admin/error_message.php?login_incorrect");
			}else if($current_type == "P"){
				func_header_location($xcart_web_dir."/provider/error_message.php?login_incorrect",false);
			}

				
			//}
		}
	}
}

##################### START BLOCK LOGOUT CODE ##################################
if ($mode == "logout") 
{
	
	clearSessionVarsOnLogout();
	x_session_register("showNudge","false");
#
#If user logged out from private interface  
#	
	if($HTTP_POST_VARS['redirectto']=="private"){
	 	if (x_session_is_registered("orgId"))
        	x_session_unregister("orgId");
        if (x_session_is_registered("headerpath"))
        	x_session_unregister("headerpath");    
        if (x_session_is_registered("orgname"))
        	x_session_unregister("orgname"); 
        
        //$accounturl = $HTTP_POST_VARS['accountname'];
        //$orgId = $HTTP_POST_VARS['orgId'];
        func_header_location($http_location,false); 
	}

	if($current_type == "A")
	{
            if (x_session_is_registered("telesalesAgent"))
               x_session_unregister('telesalesAgent');
            if (x_session_is_registered("telesalesAdminName"))
                x_session_unregister("telesalesAdminName");
            x_session_register("forcelogout");
		func_header_location($xcart_web_dir."/admin/home.php",false);
	}
	else if($current_type == "P")
	{
		func_header_location($xcart_web_dir."/provider/home.php",false);
	}
	
}
##################### END BLOCK FOR LOGOUT  ##################################

if ($old_login_type == 'C') {
	if (!empty($HTTP_REFERER) && (strncasecmp($HTTP_REFERER, $http_location, strlen($http_location)) == 0 || strncasecmp($HTTP_REFERER, $https_location, strlen($https_location)) == 0)) {
		if (strpos($HTTP_REFERER, "mode=order_message") === false &&
			strpos($HTTP_REFERER, "mode=wishlist") === false &&
			strpos($HTTP_REFERER, "bonuses.php") === false &&
			strpos($HTTP_REFERER, "returns.php") === false &&
			strpos($HTTP_REFERER, "orders.php") === false &&
			strpos($HTTP_REFERER, "giftreg_manage.php") === false &&
            strpos($HTTP_REFERER, "order.php") === false &&
			strpos($HTTP_REFERER, "register.php?mode=delete") === false &&
			strpos($HTTP_REFERER, "register.php?mode=update") === false) {
			func_header_location($xcart_web_dir."/",false);
		}
	}
}

//func_header_location($xcart_web_dir."/home.php", false);
if($current_type == "C")
{
	func_header_location($xcart_web_dir."/",false);
}
else if($current_type == "A")
{
	func_header_location($xcart_web_dir."/admin/home.php",false);
}
else if($current_type == "P")
{
	func_header_location($xcart_web_dir."/provider/home.php",false);
}
?>
