<?php
/*****************************************************************************\
+-----------------------------------------------------------------------------+
| X-Cart                                                                      |
| Copyright (c) 2001-2006 Ruslan R. Fazliev <rrf@rrf.ru>                      |
| All rights reserved.                                                        |
+-----------------------------------------------------------------------------+
| PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
| FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
| AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
|                                                                             |
| THIS  AGREEMENT  EXPRESSES  THE  TERMS  AND CONDITIONS ON WHICH YOU MAY USE |
| THIS SOFTWARE   PROGRAM   AND  ASSOCIATED  DOCUMENTATION   THAT  RUSLAN  R. |
| FAZLIEV (hereinafter  referred to as "THE AUTHOR") IS FURNISHING  OR MAKING |
| AVAILABLE TO YOU WITH  THIS  AGREEMENT  (COLLECTIVELY,  THE  "SOFTWARE").   |
| PLEASE   REVIEW   THE  TERMS  AND   CONDITIONS  OF  THIS  LICENSE AGREEMENT |
| CAREFULLY   BEFORE   INSTALLING   OR  USING  THE  SOFTWARE.  BY INSTALLING, |
| COPYING   OR   OTHERWISE   USING   THE   SOFTWARE,  YOU  AND  YOUR  COMPANY |
| (COLLECTIVELY,  "YOU")  ARE  ACCEPTING  AND AGREEING  TO  THE TERMS OF THIS |
| LICENSE   AGREEMENT.   IF  YOU    ARE  NOT  WILLING   TO  BE  BOUND BY THIS |
| AGREEMENT, DO  NOT INSTALL OR USE THE SOFTWARE.  VARIOUS   COPYRIGHTS   AND |
| OTHER   INTELLECTUAL   PROPERTY   RIGHTS    PROTECT   THE   SOFTWARE.  THIS |
| AGREEMENT IS A LICENSE AGREEMENT THAT GIVES  YOU  LIMITED  RIGHTS   TO  USE |
| THE  SOFTWARE   AND  NOT  AN  AGREEMENT  FOR SALE OR FOR  TRANSFER OF TITLE.|
| THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY GRANTED BY THIS AGREEMENT.      |
|                                                                             |
| The Initial Developer of the Original Code is Ruslan R. Fazliev             |
| Portions created by Ruslan R. Fazliev are Copyright (C) 2001-2006           |
| Ruslan R. Fazliev. All Rights Reserved.                                     |
+-----------------------------------------------------------------------------+
\*****************************************************************************/

#
# $Id: history_order.php,v 1.32 2006/01/16 06:47:33 mclap Exp $
#
# Collect infos about ordered products
#

if ( !defined('XCART_SESSION_START') ) { header("Location: ../"); die("Access denied"); }
x_load('order', 'mk_orderbook');
include_once($xcart_dir."/include/class/class.mail.multiprovidermail.php");

global $smarty,$orderid;
$order_data = func_order_data($orderid);

if (empty($order_data))
	return false;

if($order_data['order']['status'] == 'P'){
	$template = "orderprocessed";
	$args = array( "FIRST_NAME" =>$order_data['userinfo']['firstname']);
	//sendMessage($template, $args, $order_data['userinfo']['email']);
}

#
# Security check if order owned by another customer
#
if ($current_area == 'C' && $order_data["userinfo"]["login"] != $login) {
	func_header_location("error_message.php?access_denied&id=35");
}
$smarty->assign("order_details_fields_labels", func_order_details_fields_as_labels());
$smarty->assign("order", $order_data["order"]);
$smarty->assign('order_time', $order_time);
$smarty->assign("customer", $order_data["userinfo"]);
$order_data["products"] = func_translate_products($order_data["products"], ($order_data["userinfo"]['language']?$order_data["userinfo"]['language']:$config['default_customer_language']));
$smarty->assign("products", $order_data["products"]);

if($order_data["giftcerts"]) {
	$smarty->assign("giftcerts", $order_data["giftcerts"]);
}
if ($order_data) {
	$owner_condition = "";
	if ($current_area == "C")
		$owner_condition = " AND $sql_tbl[orders].login='".$login."'";
	elseif ($current_area == "P" && !$single_mode ) {
		$owner_condition = " AND $sql_tbl[order_details].provider='".$login."'";
	}
}
$location[] = array(func_get_langvar_by_name("lbl_orders_management"), "orders.php");
$location[] = array(func_get_langvar_by_name("lbl_order_details_label"), "");

if(!empty($active_modules['Anti_Fraud'])) {
    include $xcart_dir."/modules/Anti_Fraud/order.php";
}

function _calculate_from_notification_matrix($notification_matrix){
	$returnval = array();
	list($returnval['customer']['confemail'],$returnval['customer']['confsms'],$returnval['customer']['shipemail'],$returnval['customer']['shipsms'],$returnval['store']['confemail'],$returnval['store']['confsms'],$returnval['store']['shipemail'],$returnval['store']['shipsms'],$returnval['shopmaster']['confemail'],$returnval['shopmaster']['confsms'],$returnval['shopmaster']['shipemail'],$returnval['shopmaster']['shipsms'],$returnval['myntra']['confemail'],$returnval['myntra']['confsms'],$returnval['myntra']['shipemail'],$returnval['myntra']['shipsms']) = split('[;,]',$notification_matrix);
	return $returnval;
}
?>
