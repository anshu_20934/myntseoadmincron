<?php
use base\dao\BaseDAO;
include_once (dirname(__FILE__)."/../../../../modules/RestAPI/RestRequest.php");

class MyntraPageDBAdapter extends BaseDAO{
	private static $tab_myntra_page;
	private static $tab_myntra_landin_page;
	private static $tab_myntra_curated_search_list;
	private static $instance = null;
    private static $landing_page_cache;
    private static $parametric_page_cache;
    private static $parametric_page_id_cache;
    private static $curated_search_list_cache;
        
    public static function init() {
    	global $sql_tbl, $xcart_dir;
        self::$tab_myntra_page = $sql_tbl["myntra_page"];
        self::$tab_myntra_landin_page = $sql_tbl["myntra_landing_page"];
        self::$tab_myntra_curated_search_list = $sql_tbl["myntra_curated_search_list"];
        include_once($xcart_dir."/Cache/TableCache.php");
        global $weblog;
        $weblog->info(self::$tab_myntra_landin_page);
        $weblog->info(self::$tab_myntra_page);
        self::$landing_page_cache = TableCache::getTableCache(self::$tab_myntra_landin_page, 'page_url');
        self::$parametric_page_cache = TableCache::getTableCache(self::$tab_myntra_page, 'page_key');
        self::$parametric_page_id_cache = TableCache::getTableCache(self::$tab_myntra_page, 'id');
        self::$curated_search_list_cache = TableCache::getTableCache(self::$tab_myntra_curated_search_list, 'fk_parameterized_page_id');
	}

	/**
	 * Constructor.
	 */
	protected function __construct()
	{
        self::init();
	}


	/**
	 * Generates UUID
	 */
	public static function generateUUID(){
		$uuid = md5(mysql_result(mysql_query('Select UUID()'),0));
		return $uuid;
	}

	public static function getInstance(){
		if(self::$instance==null){
			self::$instance= new MyntraPageDBAdapter();
		}
		return self::$instance;
	}

	public function getLandingPageDetails($page_url){
	    $page_url = mysql_escape_string($page_url);
	    return self::$landing_page_cache->get($page_url);
	}

	public function getPageKeyDetails($page_key) {
	    $page_key = mysql_escape_string($page_key);
	    return self::$parametric_page_cache->get($page_key);
	}

	public function getPageIdDetails($id) {
	    $id = mysql_escape_string($id);
	    return self::$parametric_page_id_cache->get($id);
	}
	
	public function getCuratedSearchDetails($parameterized_page_id) {
		$id = (int)$parameterized_page_id;
	    if($id != 0) {
	    		    return self::$curated_search_list_cache->get($id);
		}
	    return NULL;
	}
        
	public function isLandingPage($page_url){
                $val = $this->getLandingPageDetails($page_url);
		if(!empty($val)){
			return true;
		}else{
			return false;
		}
	}

	public function getSolrQueryForKey($page_key){
		$res = $this->getPageKeyDetails($page_key);
        return $res['parameter_string'];
	}

	public function getParametrizedIdForPageUrl($page_url){
	    $page_url = mysql_escape_string($page_url);
	    $landingPage = self::$landing_page_cache->get($page_url);
	    return $landingPage['parameterized_page_id'];
	}


	public function getParametrizedIdForKey($page_key){
		$res = $this->getPageKeyDetails($page_key);
		return $res['id'];
	}

	public function getCuratedSearchDataForKey($page_key) {
		$parameterized_page_id = $this->getParametrizedIdForKey($page_key);
		$res = $this->getCuratedSearchDetails($parameterized_page_id);
		return $res;
	}
	
	public function getCuratedSearchDataForLandingPage($page_url) {
		$landingPage = $this->getLandingPageDetails($page_url);
		$parameterized_page_id = $landingPage['parameterized_page_id'];
		$res = $this->getCuratedSearchDetails($parameterized_page_id);
		return $res;
	}
	
        // infrequently used: before inserting the new query
	public function getKeyAndDescForSolrQuery($parameter_string){
		$parameter_string_key=md5($parameter_string);
		$parameter_string=mysql_real_escape_string($parameter_string);
		$sql="select page_key,page_desc from ".self::$tab_myntra_page." where parameter_hash_key = '$parameter_string_key' and parameter_string = '$parameter_string'";
		//                $sql="select page_key,page_desc from ".self::$tab_myntra_page." where  parameter_string = '$parameter_string'";
		$result=$this->getRAWData($sql,true);
		return $result;
	}

    public function getSolrQueryForLandingPage($page_url){
		$res = $this->getLandingPageDetails($page_url);
	    $id = $res['parameterized_page_id'];
	    $res = $this->getPageIdDetails($id);
	    return $res["parameter_string"];
	}

	public function saveOrUpdateMyntraPage($parameter_string,$page_desc='',$parameter_page_id) {
		$page_desc = strtolower($page_desc);
		$sql = "SELECT id,page_key FROM ".self::$tab_myntra_page." WHERE parameter_string ='".mysql_real_escape_string($parameter_string)."'";
		$rsCheck = db_query($sql);

		if(!empty($parameter_page_id))
		{
		    $sql = "SELECT id,page_key FROM ".self::$tab_myntra_page." WHERE id=$parameter_page_id";
		    $rsCheck = func_query($sql);
			$uuid=$rsCheck[0]['page_key'];
			$parameter_string_key=md5($parameter_string);
			$parameter_string=mysql_real_escape_string($parameter_string);
			$page_desc = mysql_real_escape_string($page_desc);
			$page_id=$parameter_page_id;
			$sql = "UPDATE ".self::$tab_myntra_page." SET page_desc='$page_desc' , parameter_hash_key='$parameter_string_key' , parameter_string='$parameter_string' where id='$parameter_page_id'";
			db_query($sql);
		}
		else if(db_num_rows($rsCheck) > 0)
		{
			$uuid=$rsCheck[0]['page_key'];
			$page_id=$rsCheck[0]['id'];
			$parameter_string=mysql_real_escape_string($parameter_string);
			$page_desc = mysql_real_escape_string($page_desc);
			$sql = "UPDATE ".self::$tab_myntra_page." SET page_desc='$page_desc' where parameter_string='$parameter_string'";
			db_query($sql);
		}
		else
		{
			$uuid=$this->generateUUID();
			$parameter_string_key=md5($parameter_string);
			$parameter_string=mysql_real_escape_string($parameter_string);
			$page_desc = mysql_real_escape_string($page_desc);
			$sql = "INSERT INTO ".self::$tab_myntra_page." (page_key,parameter_string,page_desc,parameter_hash_key) VALUES('".$uuid."','".$parameter_string."','".$page_desc."','".$parameter_string_key."')";
			db_query($sql);
			$page_id=db_insert_id();
		}

        self::$parametric_page_cache->refresh($uuid);
        self::$parametric_page_id_cache->refresh($page_id);

        $this->recacheServiceCache($page_id, "parameterizedPage");

		return array("id"=>$page_id,"page_key"=>$uuid);
	}

	public function saveOrUpdateMyntraLandingPage($query_data) {
		if(!empty($query_data['id']))
		{
            $sql = "select page_url from ".self::$tab_myntra_landin_page." where id = '".mysql_real_escape_string($query_data['id'])."'";
            $old_page_url = func_query_first_cell($sql, true);
                        
			$query_data['page_url'] = mysql_real_escape_string($query_data['page_url']);
			$sql = "UPDATE ".self::$tab_myntra_landin_page.
                    " SET parameterized_page_id='".mysql_real_escape_string($query_data['parameterized_page_id']).
                    "' , page_url='".$query_data['page_url'].
                    "' , is_autosuggest='".mysql_real_escape_string($query_data['is_autosuggest']).
                    "' , alt_text='".mysql_real_escape_string($query_data['alt_text']).
                    "' where id='".mysql_real_escape_string($query_data['id'])."'";
			db_query($sql);
                        
            self::$landing_page_cache->refresh($old_page_url);
		}
		else
		{
			$query_data['page_url'] = mysql_real_escape_string($query_data['page_url']);
			$sql_check = "select id from ".self::$tab_myntra_landin_page." where page_url='".$query_data['page_url']."'";
			$rsCheck = db_query($sql_check);
			if(db_num_rows($rsCheck) == 0){
				$sql = "INSERT INTO ".self::$tab_myntra_landin_page.
                        " (page_url,parameterized_page_id,alt_text) VALUES('".
                        $query_data['page_url']."','".$query_data['parameterized_page_id']."','".$query_data['page_url']."')";
				db_query($sql);
			}
		}
        self::$landing_page_cache->refresh($query_data['page_url']);
        $landing_page_id = db_insert_id();
        $this->recacheServiceCache($landing_page_id, "landingPage");        
	}

	public function deleteMyntraLandingPage($landing_page_id){
		if(!empty($landing_page_id))
		{
			$landing_page_id=mysql_real_escape_string($landing_page_id);
            $sql = "SELECT page_url from ".self::$tab_myntra_landin_page." where id='$landing_page_id'";
            $page_url = func_query_first_cell($sql, true);

            $this->removeServiceCacheEntry($landing_page_id, "landingPage");

            $sql = "DELETE from ".self::$tab_myntra_landin_page." where id='$landing_page_id'";
			db_query($sql);
            self::$landing_page_cache->refresh($page_url);
 		}
	}

    public function deleteMyntraPage($pageKey) {
        // ideally this method should be on page_key
        // that would require changes in admin/solr_search.php forms
        if(!empty($pageKey))
        {
            $sql = "select id from ".self::$tab_myntra_page." where page_key = '$pageKey'";
            $page_id = func_query_first_cell($sql, true);
            $page_details = self::$parametric_page_id_cache->get($page_id);
            $page_key = $page_details['page_key'];

            $sql = "select id, page_url from ".self::$tab_myntra_landin_page." where parameterized_page_id = '$page_id'";
            $result = func_query($sql, true);
            $landing_page_id = $result[0]['id'];
            $page_urls = $result[0]['page_url'];

            $this->removeServiceCacheEntry($page_id, "parameterizedPage");

            $deleteQuery = "delete  from ".self::$tab_myntra_landin_page." where parameterized_page_id='$page_id'";
            func_query($deleteQuery);

            $deleteQuery = "delete  from ".self::$tab_myntra_page." where page_key = '$pageKey'";
            func_query($deleteQuery);

            foreach($page_urls as $tmppageurl){
                self::$landing_page_cache->refresh($tmppageurl['page_url']);
            }
            self::$parametric_page_cache->refresh($page_key);
            self::$parametric_page_id_cache->refresh($page_id);
        }
    }

    public function removeServiceCacheEntry($landing_page_id, $cacheName) {
    	global $weblog;
        $url = HostConfig::$searchServiceUrl.'/removeCacheEntry/' . $landing_page_id;
        $returnObject=null;
        $headers = array();
        array_push($headers, 'Accept:  application/json');
        array_push($headers,'Content-Type: application/json');
        array_push($headers,'cacheName: ' . $cacheName);
        array_push($headers,'accept-language: en-US');
        $request = new RestRequest($url, 'GET');
        $request->setHttpHeaders($headers);
        $request->execute();
        $responseInfo = $request->getResponseInfo();
        $responseBody = $request->getResponseBody();
        $partialReturnObject = json_decode($responseBody);
        if(!$responseBody || isset($responseBody->status) || isset($responseBody->message) || $responseInfo['http_code'] == 500) {
            $weblog->debug(' Service side recaching of Landing page :--'.print_r($request,true));
        }
        if(empty($returnObject)) {
            $returnObject = $partialReturnObject;   
        } else {
            foreach ($partialReturnObject as $key=>$value) {
                $returnObject->$key = $value;
            }
        }
        return $returnObject; 
    }

    public function recacheServiceCache($landing_page_id, $cacheName) {
    	global $weblog;
        $url = HostConfig::$searchServiceUrl.'/reCacheEntry/' . $landing_page_id;
        $returnObject=null;
        $headers = array();
        array_push($headers, 'Accept:  application/json');
        array_push($headers,'Content-Type: application/json');
        array_push($headers,'cacheName: ' . $cacheName);
        array_push($headers,'accept-language: en-US');
        $request = new RestRequest($url, 'GET');
        $request->setHttpHeaders($headers);
        $request->execute();
        $responseInfo = $request->getResponseInfo();
        $responseBody = $request->getResponseBody();
        $partialReturnObject = json_decode($responseBody);
        if(!$responseBody || isset($responseBody->status) || isset($responseBody->message) || $responseInfo['http_code'] == 500) {
            $weblog->debug(' Service side cache removal of Landing page :--'.print_r($request,true));
        }
        if(empty($returnObject)) {
            $returnObject = $partialReturnObject;   
        } else {
            foreach ($partialReturnObject as $key=>$value) {
                $returnObject->$key = $value;
            }
        }
        return $returnObject; 
    }
}
