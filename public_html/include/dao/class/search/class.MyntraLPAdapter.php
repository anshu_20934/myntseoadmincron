<?php
use base\dao\BaseDAO;
include_once (dirname(__FILE__)."/../../../../modules/RestAPI/RestRequest.php");

class MyntraLPAdapter extends BaseDAO{

    private static $instance = null;

    private static $headers = array();

    private function getRestrequestObj($url, $request_type, $params = array(), $ignoreactive = false){
        switch($request_type){
            case 'GET':
                $request = new RestRequest($url, 'GET');
                break;
            case 'POST':
                $request = new RestRequest($url, 'POST', json_encode($params));
                break;
            case 'DELETE':
                $request = new RestRequest($url, 'DELETE');
                break;
        }

        if($ignoreactive){
            $temp_headers = array();

            array_push($temp_headers, 'Accept:  application/json');
            array_push($temp_headers,'Content-Type: application/json');
            array_push($temp_headers,'accept-language: en-US');
            array_push($temp_headers,'ignoreactive: true');

            $request->setHttpHeaders($temp_headers);
        } else{
            $request->setHttpHeaders(self::$headers);
        }

        return $request;
    }

    protected function __construct($login, $email){
        self::$headers = array();

        array_push(self::$headers, 'Accept:  application/json');
        array_push(self::$headers,'Content-Type: application/json');
        array_push(self::$headers,'accept-language: en-US');

        if(!empty($login)){
            array_push(self::$headers, 'login: '. $login);
        }

        if(!empty($email)){
            array_push(self::$headers, 'email: '. $email);
        }
    }

    public static function getInstance($login, $email){
        if(self::$instance == null){
            self::$instance = new MyntraLPAdapter($login, $email);
        }

        return self::$instance;
    }

	public function isLandingPage($page_url){
        $url = HostConfig::$searchServiceUrl.'/parameterizedpage/pageurl/' . $page_url;

        $request = $this->getRestrequestObj($url, 'GET');
        $request->execute();

        $responseInfo = $request->getResponseInfo();
        $responseBody = $request->getResponseBody();
        $jsonResponseBody = json_decode($responseBody);

        if( !empty($jsonResponseBody) && isSet($jsonResponseBody->data) &&
            isSet($jsonResponseBody->data->id) && !empty($jsonResponseBody->data->id)) return true;

        return false;
	}

    public function getSolrQueryForLandingPage($page_url){
        $url = HostConfig::$searchServiceUrl.'/landingpage/' . $page_url;

        $request = $this->getRestrequestObj($url, 'GET');
        $request->execute();

        $responseInfo = $request->getResponseInfo();
        $responseBody = $request->getResponseBody();
        $jsonResponseBody = json_decode($responseBody);

        if( !empty($jsonResponseBody) && isSet($jsonResponseBody->data) &&
            !empty($jsonResponseBody->data) && isSet($jsonResponseBody->data->parameterizedPageId) ){

            $url = HostConfig::$searchServiceUrl.'/parameterizedpage/id/' . $jsonResponseBody->data->parameterizedPageId;

            $request = $this->getRestrequestObj($url, 'GET');
            $request->execute();

            $responseInfo = $request->getResponseInfo();
            $responseBody = $request->getResponseBody();
            $jsonResponseBody = json_decode($responseBody);

            if( !empty($jsonResponseBody) && isSet($jsonResponseBody->data) &&
                isSet($jsonResponseBody->data->parameterString) && !empty($jsonResponseBody->data->parameterString) ){
                return $jsonResponseBody->data->parameterString;
            }
        }

        return "";
    }

    public function getKeyAndDescForSolrQuery($parameter_string){
        $parameter_string_key=md5($parameter_string);

        $url = HostConfig::$searchServiceUrl.'/parameterizedpage/paramhashkey/'. $parameter_string_key;

        $request = $this->getRestrequestObj($url, 'GET');
        $request->execute();

        $responseInfo = $request->getResponseInfo();
        $responseBody = $request->getResponseBody();
        $jsonResponseBody = json_decode($responseBody);

        $result = array('page_key' => '', 'page_desc' => '');

        if(!empty($jsonResponseBody) && isSet($jsonResponseBody->data)){
            $result['page_key'] = $jsonResponseBody->data->pageKey;
            $result['page_desc'] = $jsonResponseBody->data->pageDesc;
        }

        return $result;
    }

    public function saveOrUpdateMyntraPage($parameter_string, $page_desc, $parameter_page_id) {
        $url = HostConfig::$searchServiceUrl.'/parameterizedpage/saveorupdate';

        $requestBody = array();

        if(!empty($parameter_page_id)){
            $requestBody['id'] = $parameter_page_id;
        }

        $requestBody['parameterString'] =  $parameter_string;

        if(!empty($page_desc)){
            $requestBody['pageDesc'] = $page_desc;
        }

        $request = $this->getRestrequestObj($url, 'POST', $requestBody);
        $request->execute();

        $responseInfo = $request->getResponseInfo();
        $responseBody = $request->getResponseBody();
        $jsonResponseBody = json_decode($responseBody);

        //Check on the successfull execution of curl request
        if( !empty($jsonResponseBody) && isSet($jsonResponseBody->status) &&
            isSet($jsonResponseBody->status->statusType) && !empty($jsonResponseBody->status->statusType) &&
            $jsonResponseBody->status->statusType == "SUCCESS"){

            if(!empty($parameter_page_id)){
                $url = HostConfig::$searchServiceUrl.'/parameterizedpage/id/'. $parameter_page_id;

            } else if(!empty($page_desc)){
                $url = HostConfig::$searchServiceUrl.'/parameterizedpage/pageurl/'. $page_desc;
            }

            $request = $this->getRestrequestObj($url, 'GET');
            $request->execute();

            $responseInfo = $request->getResponseInfo();
            $responseBody = $request->getResponseBody();
            $jsonResponseBody = json_decode($responseBody);

            if( !empty($jsonResponseBody) && isSet($jsonResponseBody->data) &&
                isSet($jsonResponseBody->data->id) && !empty($jsonResponseBody->data->id) &&
                isSet($jsonResponseBody->data->pageKey) && !empty($jsonResponseBody->data->pageKey) ){

                return array("id"=>$jsonResponseBody->data->id,"page_key"=>$jsonResponseBody->data->pageKey);
            }
        }

        return array();
    }

	public function saveOrUpdateMyntraLandingPage($query_data) {
        $url = HostConfig::$searchServiceUrl.'/landingpage/'. $query_data['page_url'];

        $request = $this->getRestrequestObj($url, 'GET', array(), true);
        $request->execute();

        $responseInfo = $request->getResponseInfo();
        $responseBody = $request->getResponseBody();
        $jsonResponseBody = json_decode($responseBody);

        $requestBody = array();

        if( !empty($jsonResponseBody) && isSet($jsonResponseBody->status)
            && isSet($jsonResponseBody->status->statusType) && $jsonResponseBody->status->statusType == 'ERROR'){

            $url = HostConfig::$searchServiceUrl.'/landingpage/add';

         } else{
             $url = HostConfig::$searchServiceUrl.'/landingpage/update';
             $requestBody['id'] = $jsonResponseBody->data->id;
         }

        $requestBody['pageUrl'] =  $query_data['page_url'];
        $requestBody['parameterizedPageId'] = $query_data['parameterized_page_id'];
        $requestBody['isAutoSuggest'] = 'false';
        $requestBody['isAutoCreated'] = 'false';
        $requestBody['isActive'] =  isSet($query_data['isactive']) && $query_data['isactive'] ? 'true' : 'false';

        if(isSet($query_data['landingPageType']) && !empty($query_data['landingPageType'])){
            $requestBody['landingPageType'] = $query_data['landingPageType'];
        }

        if(isSet($query_data['expiryDate']) && !empty($query_data['expiryDate'])){
            $requestBody['expiryDate'] = $query_data['expiryDate'];
        }

        $request = $this->getRestrequestObj($url, 'POST', $requestBody);
        $request->execute();

        $responseInfo = $request->getResponseInfo();
        $responseBody = $request->getResponseBody();
        $jsonResponseBody = json_decode($responseBody);
	}

    public function recacheServiceCache($landing_page_id, $cacheName) {
    	global $weblog;
        $url = HostConfig::$searchServiceUrl.'/reCacheEntry/' . $landing_page_id;
        $returnObject=null;
        $headers = array();
        array_push($headers, 'Accept:  application/json');
        array_push($headers,'Content-Type: application/json');
        array_push($headers,'cacheName: ' . $cacheName);
        array_push($headers,'accept-language: en-US');
        $request = new RestRequest($url, 'GET');
        $request->setHttpHeaders($headers);
        $request->execute();
        $responseInfo = $request->getResponseInfo();
        $responseBody = $request->getResponseBody();
        $partialReturnObject = json_decode($responseBody);
        if(!$responseBody || isset($responseBody->status) || isset($responseBody->message) || $responseInfo['http_code'] == 500) {
            $weblog->debug(' Service side cache removal of Landing page :--'.print_r($request,true));
        }
        if(empty($returnObject)) {
            $returnObject = $partialReturnObject;
        } else {
            foreach ($partialReturnObject as $key=>$value) {
                $returnObject->$key = $value;
            }
        }
        return $returnObject;
    }

    public function deleteMyntraPage($pageKey) {
        // ideally this method should be on page_key
        // that would require changes in admin/solr_search.php forms
        if(!empty($pageKey))
        {
            $url = HostConfig::$searchServiceUrl.'/parameterizedpage/key/'. $pageKey;

            $request = $this->getRestrequestObj($url, 'GET');
            $request->execute();
            $responseInfo = $request->getResponseInfo();
            $responseBody = $request->getResponseBody();
            $jsonResponseBody = json_decode($responseBody);

            // get page_url from parameterizedpage
            if( !empty($jsonResponseBody) && isSet($jsonResponseBody->data) &&
                isSet($jsonResponseBody->data->pageDesc) && !empty($jsonResponseBody->data->pageDesc) ){
                $url = HostConfig::$searchServiceUrl.'/landingpage/'. $jsonResponseBody->data->pageDesc;

                $request = $this->getRestrequestObj($url, 'GET');
                $request->execute();
                $responseInfo = $request->getResponseInfo();
                $responseBody = $request->getResponseBody();
                $jsonResponseBody = json_decode($responseBody);

                // get landing_page_id that needs to be deleted
                if( !empty($jsonResponseBody) && isSet($jsonResponseBody->data) &&
                    isSet($jsonResponseBody->data->id) && !empty($jsonResponseBody->data->id)){

                    $url = HostConfig::$searchServiceUrl.'/landingpage/delete/'. $jsonResponseBody->data->id;
                    $request = $this->getRestrequestObj($url, 'DELETE');
                    $request->execute();
                    $responseInfo = $request->getResponseInfo();
                    $responseBody = $request->getResponseBody();
                    $jsonResponseBody = json_decode($responseBody);
                }
            }
        }
    }
}

?>
