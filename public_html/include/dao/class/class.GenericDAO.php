<?php
/**
 * This is generic class for DAO based coding which has DB basic functionalities
 * @author Yogesh, Ravi
 *
 */
class GenericDAO{
	
	public function getRAWData($sql,$singleRow=false){
		if($singleRow==true){
			$result=func_query_first($sql);
		}else{
			$result=func_query($sql);
		}
		return $result;
	}
	
	/**
	 * Method to retrieve data from a table. e.g. getRawDataFromTable('xcart_orders', array('login'=>'example@myntra.com'), 0, 10, array('orderid'=>'desc'));
	 * @param  string $tbl Table name
	 * @param  array $filters Array containing key->value format of filters 
	 * @param  int $start the offset where the result set returned should start from. Default to null
	 * @param int $count The number of records to be retuned.
	 * @param  array $orders Array of key->value formatted orders that needs to be performed. The <b>key</b> is the column name on 
	 * 			which the ordering is to be performed and value contains the type of order i.e. asc or desc
	 * @return array
	 */
	public function getRawDataFromTable($tbl, $filters = NULL, $start = NULL, $count = 10, $orders = NULL) {
		$sql = 'SELECT * FROM '.$tbl;
		if(is_array($filters)){
			$sql .=' WHERE ';
			$index = 0 ;
			foreach($filters as $key => $value){
				if($index > 0 ){
					$sql.= ' AND ';
				}
				$sql .= $key.'="'. $value.'"';
				$index++;
			}
		}
	
		if(is_array($orders)){
			$sql .=' ORDER BY ';
			$index = 0 ;
			foreach($orders as $key => $value){
				if($index > 0 ){
					$sql.= ' , ';
				}
				$sql .= $key.' '. $value;
				$index++;
			}
		}
	
		if(isset($start) && $start >= 0) {
			$sql.=' LIMIT '.$start.','.$count;
		}
		//echo  $sql;  die();
		return execute_sql($sql) ;
	}
	
	/**
	 * Method to delete from a given table using id given in an array 
	 * @param String $tbl table name
	 * @param array $ids an array of ids which has to be deleted
	 * @return int $rowsDeleted returns no. of rows deleted
	 */
	public function deleteDataFromTableUsingId($tbl, $ids)
	{
		$delQuery = "DELETE FROM $tbl WHERE id ";	
		if(is_array($ids))
		{
			$id = implode(",", $ids);
			$delQuery.=" in (".$ids.")";
		}
		else
		{
			$delQuery.=" = '".$ids."'";
		}
		db_query($delQuery);
		$rowsDeleted = db_affected_rows();
		return $rowsDeleted;
	}
	
	
}

?>