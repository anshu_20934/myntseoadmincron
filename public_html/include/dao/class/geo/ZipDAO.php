<?php
namespace geo;

class ZipDAO extends GeoDAO {

	public function __construct() {
		parent::__construct();
	}
	
	public function getAllStateLevelZips() {
		$sql =  "select s.iso_code as code, group_concat(z.zip_code SEPARATOR '|') as zips ".
				"from zips z ".
				"inner join states s on z.state_woeid = s.woeid ".
				"where z.zip_type = ". PlaceType::STATE ." ".
				"group by s.iso_code ";
		$result = mysql_query($sql, $this->link);
		$stateZips = $this->transformResultsetToHashmap($result, "code");
		foreach ($stateZips as $stateCode => $zipsCSV) {
			$stateZips[$stateCode] = explode("|", $zipsCSV['zips']);
		}
		return $stateZips;
	}
}
?>