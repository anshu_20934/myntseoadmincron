<?php

namespace geo;

class PlaceType {
	const TOWN = 7, STATE = 8, COUNTY = 9, LOCAL_ADMIN_AREA = 10, 
	POSTAL_CODE = 11, COUNTRY = 12, ISLAND = 13, AIRPORT = 14, DRAINAGE = 15, SUBURB = 22;
}

class GeoDAO {
	
	protected $link;
	
	protected function __construct($use_ro = true) {
		if($use_ro) {
			$this->link = mysql_connect(\GeoDBConfig::$ro['host'], \GeoDBConfig::$ro['user'], \GeoDBConfig::$ro['password'], true);
			mysql_select_db(\GeoDBConfig::$ro['db'], $this->link);
		}
		else {
			$this->link = mysql_connect(\GeoDBConfig::$rw['host'], \GeoDBConfig::$rw['user'], \GeoDBConfig::$rw['password'], true);
			mysql_select_db(\GeoDBConfig::$rw['db'], $this->link);
		}
		mysql_query("SET SESSION group_concat_max_len = 65536", $this->link);
	}

	protected function transformResultsetToHashmap($qryResult, $keyField) {
		$retArr = array();
		while ($row = mysql_fetch_assoc($qryResult)) {
			$retArr[$row[$keyField]] = $row;
		}
		return $retArr;
	}
	
	private function buildSqlFragment($assocArr, $delimiter) {
		$sqlFragment = "";
		$numSetFields = count($assocArr);
		$index = 0;
		foreach ($assocArr as $key=>$value) {
			$sqlFragment .= $key."=".encloseInQuotes($value);
			$index++;
			if($index < numSetFields) {
				$sqlFragment .= "$delimiter ";
			}
		}
		return $sqlFragment;
	}
	
	public function updateAssocArray($assocArr2update, $whereCondAssocArr, $tbl) {
		$setFragment = $this->buildSqlFragment($assocArr2update, ",");
		$whereFragment = $this->buildSqlFragment($whereCondAssocArr, " and ");
		$updateSql = "UPDATE $tbl SET ".$setFragment." WHERE ".$whereFragment;
		echo "update sql:". $updateSql;
		return $this->executeInsertOrUpdate($updateSql);
	}
	
	public function insertAssocArray($assoc_arr, $tbl) {
		$keys = implode(",",array_keys($assoc_arr));
		$values = array_values($assoc_arr);
		$values = array_map("encloseInQuotes", $values);
		$values = implode(",",$values);
		
		$sql = "INSERT INTO $tbl (".$keys.") VALUES (".$values.")";
		return $this->executeInsertOrUpdate($sql);
	}
	
	function executeInsertOrUpdate($sql) {
		if(mysql_query($sql, $this->link)) {
			return mysql_affected_rows($this->link);
		}
		return false;
	}

	public static function getWoeid($place) {
		if($place instanceof stdClass) {
			return $place->woeid;
		}
		else {
			return $place['woeid'];			
		}
	}
	
	public static function getName($place) {
		if($place instanceof stdClass) {
			return $place->name;
		}
		else {
			return $place['name'];			
		}
	}
	
	public static function insertLog($placeType, $place, $parentPlace, $numRows) {
		if($numRows > 0)
			echo "Inserting: $placeType: ".self::getName($place)." of parent:".self::getName($parentPlace)." ; $numRows inserted\n";
	}
	
	public function queryFirstCell($sql) {
		$res = mysql_query($sql, $this->link);
		$row = mysql_fetch_row($res);
		return $row[0];	
	}
	
	public function queryFirstRow($sql) {
		$res = mysql_query($sql, $this->link);
		$row = mysql_fetch_assoc($res);
		return $row;	
	}

	public function getRAWData($sql,$singleRow=false){
		if($singleRow==true){
			return $this->queryFirstRow($sql,$this->link);
		}else{
			$result=mysql_query($sql, $this->link);
			$rows=Array();
			while($row = mysql_fetch_assoc($result)) {
				$rows[] = $row;
			}
			return $rows;
		}
	}
	
}

?>