<?php
require_once "$xcart_dir/include/class/admin/RolesAccess.php";

use base\dao\BaseDAO;

class RolesAccessDBAdapter extends BaseDAO{
	
	private $tab_mk_roles_access;
	
	const SUCCESS_ADD = 1;
	const ERROR_ADD = -1;
	const ERROR_ADD_FILE_ALREADY_EXIST = -2;
	const SUCCESS_DELETE = 1;
	const ERROR_DELETE = -1;
	const SUCCESS_UPDATE = 1;
	const ERROR_UPDATE = -1;
	
	private static $instance = null;
	
	/**
     * Constructor.
     */
    protected function __construct()
    {
        global $sql_tbl;
        $this->tab_mk_roles_access = "mk_roles_access";
    }
	
    
	public static function getInstance(){
		if(self::$instance==null){
			self::$instance= new RolesAccessDBAdapter();
		}
		return self::$instance;
	}
	
	/**
	 *  Get Roles access for single row
	 *  @param String $condition
	 */
	
	public function fetchRolesAccess($condition){
		$rawRolesAccess = $this->getRawRolesAccessFromDB($condition);
		if(empty($rawRolesAccess))
			return null;
		$rolesAccess = new RolesAccess($rawRolesAccess['created_by']);
		$init_obj = $rolesAccess->initRolesAccessObject($rawRolesAccess);
		//$rolesaccess->setAsSaved();
		return $rolesAccess;
	}
	
	//for accessing 1 roles access
	private function getRawRolesAccessFromDB($condition){
		$sql="select * from $this->tab_mk_roles_access";
		if(!empty($condition)){
			$sql.=" where ".$condition;
		}
		$result=$this->getRAWData($sql,true);
		return $result;
	}

	/** 
	 * fetch array of roles access
	 */
	
	public function fetchRolesAccessArray($condition){
		$rawRolesAccessesArray=array();
		$rawRolesAccessArray=$this->getRawRolesAccessArrayFromDB($condition);
		if(empty($rawRolesAccessArray))
			return null;
		$rolesAccessArray=array();
		foreach ($rawRolesAccessArray as $rawRolesAccess){
			$rolesAccess = new RolesAccess($rawRolesAccess['created_by']);
			$init_obj = $rolesAccess->initRolesAccessObject($rawRolesAccess);
			$rolesAccessArray[]=$rolesAccess;
		}
		
		return $rolesAccessArray;
	}

	//for accessing multipel rows
	private function getRawRolesAccessArrayFromDB($condition){
		$sql="select * from $this->tab_mk_roles_access";
		if(!empty($condition)){
			$sql.=" where ".$condition;
		}
		$result=$this->getRAWData($sql,false);
		return $result;
	}
	
	//add the roles access object
	public function addRolesAccess($rolesAccess){
		$pagename = $rolesAccess->getPagename();
		$exists = func_query("select * from $this->tab_mk_roles_access where pagename='".$pagename."'");
		if(empty($exists)){
			$data_add = array(
				"pagename"   => $pagename,
				"roles"      => $rolesAccess->getRoles(),
				"extra_logins_allowed" => $rolesAccess->getExtraLoginsAllowed(),
				"created_on" => $rolesAccess->getCreatedOn(),
				"created_by" => $rolesAccess->getCreatedBy(),
				"updated_on" => $rolesAccess->getUpdatedOn(),
				"updated_by" => $rolesAccess->getUpdatedBy()
			);
			$result_add = func_array2insert($this->tab_mk_roles_access, $data_add);
			if($result_add){
				return $this::SUCCESS_ADD;
			}
			else {
				return $this::ERROR_ADD;
			}
		}
		return $this::ERROR_ADD_FILE_ALREADY_EXIST;
	}
	
	//deletes the given roles access object
	public function deleteRolesAccess($rolesAccess){
		$id = $rolesAccess->getPageid();
		$del_query = "delete FROM $this->tab_mk_roles_access WHERE id ='".$id."'";
		db_query($del_query);
		$del_result = db_affected_rows();
		if($del_result == 0){
			return $this::ERROR_DELETE;
		}
		else {
			return $this::SUCCESS_DELETE;
		}
	}
	
	
	//checks before updating if there are changes
	public function filterUpdateRolesAccess($rolesAccessArray){
		$update_commit = false;
		if(is_array($rolesAccessArray)){
			foreach ($rolesAccessArray AS $num=>$rolesAccess){
				$data_update = array(
	        	'id'=>$rolesAccess->getPageid(),
	        	'pagename'=>$rolesAccess->getPagename(),
	        	'roles'=>$rolesAccess->getRoles(),
	        	'extra_logins_allowed'=>$rolesAccess->getExtraLoginsAllowed()
		        );
		        $exists = func_select_first($this->tab_mk_roles_access, $data_update);
		        if(empty($exists)){
		        	$result_update = $this->updateSingleRolesAccess($rolesAccess);
		        	if($result_update)
		        		$update_commit = true;
		        }
			}
		}
		else {
			$rolesAccess = $rolesAccessArray;
			$data_update = array(
        	'id'=>$rolesAccess->getPageid(),
        	'pagename'=>$rolesAccess->getPagename(),
        	'roles'=>$rolesAccess->getRoles(),
        	'extra_logins_allowed'=>$rolesAccess->getExtraLoginsAllowed()
	        );
	        $exists = func_select_first($this->tab_mk_roles_access, $data_update);
	        if(empty($exists)){
	        	$result_update = $this->updateSingleRolesAccess($rolesAccess);
	        	if($result_update)
	        		$update_commit = true;
	        }
		}
		if($update_commit)
			return $this::SUCCESS_UPDATE;
		else
			return $this::ERROR_UPDATE;		  
	}
	
	//updates the given roels access object
	public function updateRolesAccess($rolesAccessArray){
		$update_commit = false;
		if(is_array($rolesAccessArray)){
			foreach ($rolesAccessArray AS $num=>$rolesAccess){
				$result_update = $this->updateSingleRolesAccess($rolesAccess);
		        if($result_update)
		        	$update_commit = true;
			}
		}
		else {
			$rolesAccess = $rolesAccessArray;
			$result_update = $this->updateSingleRolesAccess($rolesAccess);
	        if($result_update)
	        	$update_commit = true;
		}
		if($update_commit)
			return $this::SUCCESS_UPDATE;
		else
			return $this::ERROR_UPDATE;	
	}
	
	private function updateSingleRolesAccess($rolesAccess){
		$data_update = array(
        	'pagename'=>$rolesAccess->getPagename(),
        	'roles'=>$rolesAccess->getRoles(),
        	'extra_logins_allowed'=>$rolesAccess->getExtraLoginsAllowed(),
        	'updated_by'=>$rolesAccess->getUpdatedBy(),
        	'updated_on'=>$rolesAccess->getUpdatedOn()
        );
        $result_update = func_array2update($this->tab_mk_roles_access, $data_update, "id=".$rolesAccess->getPageid()."");
        return $result_update;
	}
	
}
?>