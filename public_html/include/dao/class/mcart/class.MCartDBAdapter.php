<?php

use base\dao\BaseDAO;
use mcart\exception\ProductAlreadyInCartException;
use giftcard\manager\MCartGiftCard;
use enums\cart\CartContext;
use enums\cart\CartSerializationFormat;
use abtest\MABTest;


class MCartDBAdapter extends BaseDAO{
	
	private $tab_mk_shopping_cart;
	private $tab_xcart_customers;
	private $tab_mk_cart_order_map;
	
	private static $instance = null;
	
	/**
     * Constructor.
     */
    protected function __construct()
    {
        global $sql_tbl;
        $this->tab_mk_shopping_cart = $sql_tbl["mk_cart"];
        $this->tab_xcart_customers = $sql_tbl['customers'];
        $this->tab_mk_cart_order_map = 'cart_order_mapping';
    }
	
	
	
	public static function getInstance(){
		if(self::$instance==null){
			self::$instance= new MCartDBAdapter();
		}
		return self::$instance;
	}
	
	/**
	 * Given a cart-id, check if such a cart exists in DB
	 * @param unknown_type $cartId
	 */
	public function cartExists($cartId) {
		$sql="select count(*) as cartidexists from $this->tab_mk_shopping_cart where cookie = '".mysql_real_escape_string($cartId)."'";
		$result=$this->getRAWData($sql,true);
		return !empty($result["cartidexists"]);
	}
	
	/**
	 * Returns MCart object
	 * @param String $condition
	 */
	public function fetchMCart($condition){
		$rawCart=$this->getRawCartFromDB($condition);
		if(empty($rawCart))
			return null;
		$mcart=$this->genMCartObject($rawCart);
		$mcart->setAsSaved();
		return $mcart;
	}
	
	/**
	 * Return array of MCarts;
	 * @param String $sql: abandoned cart sql
	 */
	public function fetchAbandonedCarts($condition){
		$cartsArray=array();
		$rawCarts=$this->getRawCartWithCustNameFromDB($condition);
		
		if(empty($rawCarts))
			return $cartsArray;
			
		foreach ($rawCarts as $rawCart){
			$mcart=$this->genMCartObject($rawCart);
			if($mcart->getItemQuantity() < 1) {
				continue;
			}
			$mcart->setAsSaved();
			$cartsArray[]= Array("cart"=>$mcart, "firstname"=>$rawCart['firstname'], "lastname"=>$rawCart['lastname']);
		}		
		return $cartsArray;
	}
	
	/**
	 * Return array of MCarts;
	 * @param String $condition
	 */
	public function fetchMCarts($condition){
		$cartsArray=array();
		$rawCarts=$this->getRawCartsFromDB($condition);
		if(empty($rawCarts))
			return $cartsArray;
		
		foreach ($rawCarts as $rawCart){
			$mcart=$this->genMCartObject($rawCart);
			$mcart->setAsSaved();
			$cartsArray[]=$mcart;
		}
		
		return $cartsArray;
	}
	
	private function getRawCartFromDB($condition){
		$sql="select * from $this->tab_mk_shopping_cart";
		if(!empty($condition)){
			$sql.=" where $condition";
		}
		$result=$this->getRAWData($sql,true);
		return $result;
	}
	
	private function getRawCartWithCustNameFromDB($condition){
		$sql="select cart.*, cust.firstname, cust.lastname from $this->tab_mk_shopping_cart as cart ".
		     "inner join $this->tab_xcart_customers as cust on cart.login = cust.login ";
		if(!empty($condition)){
			$sql.=" where $condition";
		}
		$result=$this->getRAWData($sql,false,true);
		return $result;
	}
	
	private function getRawCartsFromDB($condition){
		$sql="select * from $this->tab_mk_shopping_cart";
		if(!empty($condition)){
			$sql.=" where $condition";
		}
		$result=$this->getRAWData($sql,false);
		return $result;
	}
	
	/**
	 * 
	 * Enter description here ...
	 * @param MCart $rawCart
	 */
	private function genMCartObject($rawCart){
		if(empty($rawCart))
			return null;
                
		$serialization_format = $rawCart["serialization_format"];
		$cartContext = $rawCart['context'];

		if($serialization_format === CartSerializationFormat::PhpFormat){
			$mcartDump=unserialize($rawCart['cart_data']);
		}
		if($serialization_format === CartSerializationFormat::JsonFormat){
			if($cartContext == CartContext::GiftCardContext){
				$userCart = new MCartGiftCard();
			}else{
				$userCart = new MCart();	
			}
			$mcartDump = $userCart->fromJson($rawCart['cart_data'],$cartContext);
		}	
		if($serialization_format === CartSerializationFormat::MJsonFormat){
			$mcartDump = $this->deserializeMJson($rawCart['cart_data']);
			
		}
		
		if($mcartDump instanceof MCart ){
			$mcart=$mcartDump;
			$mcart->lastmodified=$rawCart['last_modified'];
			$mcart->expiryDate=$rawCart['expirydate'];
		}else{
			$mcart = null;
		}
		if($mcart!=null){
			$mcart->removeInvalidItems();
		}
		$mcart->setCartNew(false);
		return $mcart;
	}
	
	public function saveOrUpdateMCart($mcart){
		$sql = "SELECT * FROM $this->tab_mk_shopping_cart WHERE cookie ='".mysql_real_escape_string($mcart->getCartID())."'";
		$rsCheckCartUser = db_query($sql);
		
		$_MABTestObject = MABTest::getInstance();
		$serializationFormatABTest = $_MABTestObject->getUserSegment("cart_serialization_format");

		if($serializationFormatABTest === 'test'){
			$serialization_format = CartSerializationFormat::JsonFormat;
			$cart_data = $mcart->toJson();
		}else{
			$serialization_format = CartSerializationFormat::PhpFormat;
			$cart_data = serialize($mcart);
		}

		if(db_num_rows($rsCheckCartUser) > 0)
		{
			$sql = "UPDATE $this->tab_mk_shopping_cart SET cart_data = '".addslashes($cart_data)."', login = '".mysql_real_escape_string($mcart->getLogin())."', last_modified = ".time().",expiryDate='".$mcart->expiryDate."',context='".mysql_real_escape_string($mcart->getContext())."',serialization_format='".$serialization_format."' WHERE cookie='".mysql_real_escape_string($mcart->getCartID())."'";
			db_query($sql);
		}
		else
		{
			$sql = "INSERT INTO $this->tab_mk_shopping_cart (cookie,login,cart_data,expiryDate,last_modified,context,serialization_format) VALUES('".$mcart->getCartID()."','".mysql_real_escape_string($mcart->getLogin())."','".addslashes($cart_data)."','".$mcart->expiryDate."',".time().",'".mysql_real_escape_string($mcart->getContext())."','".$serialization_format."')";
			db_query($sql);
		}
	}
	
	public function deleteCartByID($mCartID){
		$sql = "delete FROM $this->tab_mk_shopping_cart WHERE cookie ='".mysql_real_escape_string($mCartID)."'";
		return db_query($sql);
	}
	
	public function archiveCartInOrder($mcart){
		$orderId = $mcart->getOrderId();
		if(empty($orderId)){
			$orderId = 0;
		}
		
		$sql = "SELECT count(*) FROM $this->tab_mk_cart_order_map WHERE cartId ='".mysql_real_escape_string($mcart->getCartID())."'";
		$numRows = func_query_first_cell($sql, true);
			
		if(empty($numRows)) {
			$insertData = array("cartId"=>$mcart->getCartID(), "orderId"=> $orderId, "cart_data"=>addslashes(serialize($mcart)));
			return func_array2insertWithTypeCheck($this->tab_mk_cart_order_map, $insertData);
		}  
	}
	
	public function deleteCart($mcart){
		
		if($mcart->getLogin()!=null && trim($mcart->getLogin())!="" ){
			$sql = "delete FROM $this->tab_mk_shopping_cart WHERE login ='".mysql_real_escape_string($mcart->getLogin())."' and context='".mysql_real_escape_string($mcart->getContext())."'";
		}else{
			$sql = "delete FROM $this->tab_mk_shopping_cart WHERE cookie ='".mysql_real_escape_string($mcart->getCartID())."'";
		}
		return db_query($sql);
	}
	
	public function deleteAllCarts($login,$context,$condition=false){
		if(empty($login)||empty($context))
		return false;
		$sql = "delete FROM $this->tab_mk_shopping_cart WHERE login ='".mysql_real_escape_string($login)."' and context='".mysql_real_escape_string($context)."'";
		if(!empty($condition)){
			$sql.=" and $condition ";
		}
		return db_query($sql);
	}
	
	/*
	 * "giftMessage":null,"useMyntCash":true,"giftOrder":false
	 */
	
	private function deserializeMJson($json){
		$cart_data = json_decode($json,true);
		$cart = new MCart();
		$cart->setCartID($cart_data[cartID]);
		$cart->setCartCreateTimestamp($cart_data[cartCreateDate]/1000);
		$cart->setOrderID($cart_data[orderID]);
		$cart->setLogin($cart_data[login]);
		$cart->setContext($cart_data[context]);
		$cart->setLastContentUpdatedTime($cart_data[lastmodified]/1000);
		
		foreach($cart_data[appliedCoupons] as $coupon){
			$cart->addCouponCode($coupon, MCart::$__COUPON_TYPE_DEFAULT);
		}
		if($cart_data[giftOrder]=="true"){
			$giftingCharges = WidgetKeyValuePairs::getInteger('giftwrap.charges');
			$cart->setIsGiftOrder(true);
			$cart->setGiftCharge($giftingCharges);
		}
		if($cart_data[useMyntCash] == "true"){
			$cart->enableMyntCashUsage($cart_data[userEnteredMyntCashAmount]);
		}else{
			$cart->removeMyntCashUsage();
		}
		if($cart_data[useLoyaltyPoints] == "true"){
			$cart->enableLoyaltyPointsUsage();
		}
		
		if($cart_data[giftMessage]!=null){
			$cart->setGiftMessage($cart_data[giftMessage][recipient]."___".
					$cart_data[giftMessage][sender]."___".$cart_data[giftMessage][message]);
		}
		
		foreach($cart_data[cartItems] as $cartitem){
			$ci = new MCartNPItem($cartitem[itemId], $cartitem[skuId]);
			$ci->setQuantity($cartitem[quantity]);
			$ci->setItemAddedByDiscountEngine($cartitem[freeItem]=="true");
			$ci->itemAddDate = $cartitem[itemAddDate]/1000;
			$ci->discountRuleId = $cartitem[discountRuleId];
			$ci->styleType = $cartitem[styleType];
			try{
				$cart->addProduct($ci);
			}catch (Exception $ex){
				
			}
		}
		return $cart;
	}
}

?>
