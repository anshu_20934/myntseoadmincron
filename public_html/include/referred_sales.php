<?php
/*****************************************************************************\
+-----------------------------------------------------------------------------+
| X-Cart                                                                      |
| Copyright (c) 2001-2006 Ruslan R. Fazliev <rrf@rrf.ru>                      |
| All rights reserved.                                                        |
+-----------------------------------------------------------------------------+
| PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
| FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
| AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
|                                                                             |
| THIS  AGREEMENT  EXPRESSES  THE  TERMS  AND CONDITIONS ON WHICH YOU MAY USE |
| THIS SOFTWARE   PROGRAM   AND  ASSOCIATED  DOCUMENTATION   THAT  RUSLAN  R. |
| FAZLIEV (hereinafter  referred to as "THE AUTHOR") IS FURNISHING  OR MAKING |
| AVAILABLE TO YOU WITH  THIS  AGREEMENT  (COLLECTIVELY,  THE  "SOFTWARE").   |
| PLEASE   REVIEW   THE  TERMS  AND   CONDITIONS  OF  THIS  LICENSE AGREEMENT |
| CAREFULLY   BEFORE   INSTALLING   OR  USING  THE  SOFTWARE.  BY INSTALLING, |
| COPYING   OR   OTHERWISE   USING   THE   SOFTWARE,  YOU  AND  YOUR  COMPANY |
| (COLLECTIVELY,  "YOU")  ARE  ACCEPTING  AND AGREEING  TO  THE TERMS OF THIS |
| LICENSE   AGREEMENT.   IF  YOU    ARE  NOT  WILLING   TO  BE  BOUND BY THIS |
| AGREEMENT, DO  NOT INSTALL OR USE THE SOFTWARE.  VARIOUS   COPYRIGHTS   AND |
| OTHER   INTELLECTUAL   PROPERTY   RIGHTS    PROTECT   THE   SOFTWARE.  THIS |
| AGREEMENT IS A LICENSE AGREEMENT THAT GIVES  YOU  LIMITED  RIGHTS   TO  USE |
| THE  SOFTWARE   AND  NOT  AN  AGREEMENT  FOR SALE OR FOR  TRANSFER OF TITLE.|
| THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY GRANTED BY THIS AGREEMENT.      |
|                                                                             |
| The Initial Developer of the Original Code is Ruslan R. Fazliev             |
| Portions created by Ruslan R. Fazliev are Copyright (C) 2001-2006           |
| Ruslan R. Fazliev. All Rights Reserved.                                     |
+-----------------------------------------------------------------------------+
\*****************************************************************************/

#
# $Id: referred_sales.php,v 1.9.2.1 2006/08/01 05:57:37 max Exp $
#
# Display advertising statistics
#

if ( !defined('XCART_START') ) { header("Location: ../"); die("Access denied"); }

if ($StartDay)
	$search['start_date'] = mktime(0, 0, 0, $StartMonth, $StartDay, $StartYear);

if ($EndDay)
	$search['end_date'] = mktime(23, 59, 59, $EndMonth, $EndDay, $EndYear);

if ($search) {
	$where = array();

	if ($search['partner'])
		$where[] = "$sql_tbl[customers].login = '$search[partner]'";

	if ($search['productcode'])
		$where[] = "$sql_tbl[products].productcode = '$search[productcode]'";

	if ($search['status'])
		$where[] = "$sql_tbl[partner_payment].paid = '$search[status]'";

	if ($search['start_date'] && $search['end_date'])
		$where[] = $search['end_date']." > $sql_tbl[partner_payment].add_date AND $sql_tbl[partner_payment].add_date > ".$search['start_date'];

	if ($current_area == 'B')
		$where[] = "$sql_tbl[partner_payment].login = '$login'";
	else
		$where[] = "$sql_tbl[partner_payment].affiliate = ''";

	if (!$search['sort_by'])
		$search['sort_by'] = 'total';

	if ($where)
		$where_condition = " AND ".implode(" AND ", $where);

	if ($search['top'])
		$sales = func_query("SELECT $sql_tbl[products].product, $sql_tbl[products].productid, SUM($sql_tbl[partner_payment].commissions) as commissions,  SUM($sql_tbl[order_details].amount) as amount, SUM($sql_tbl[order_details].amount*$sql_tbl[order_details].price) as total, COUNT($sql_tbl[partner_payment].orderid) as sales, SUM($sql_tbl[partner_product_commissions].product_commission) as product_commission FROM $sql_tbl[customers], $sql_tbl[partner_payment], $sql_tbl[order_details], $sql_tbl[products], $sql_tbl[orders], $sql_tbl[partner_product_commissions] WHERE $sql_tbl[partner_product_commissions].itemid = $sql_tbl[order_details].itemid AND $sql_tbl[partner_product_commissions].orderid = $sql_tbl[order_details].orderid AND $sql_tbl[partner_product_commissions].login = $sql_tbl[customers].login AND $sql_tbl[customers].usertype = 'B' AND $sql_tbl[customers].status = 'Y' AND $sql_tbl[partner_payment].login = $sql_tbl[customers].login AND $sql_tbl[partner_payment].orderid = $sql_tbl[order_details].orderid AND $sql_tbl[orders].orderid = $sql_tbl[order_details].orderid AND $sql_tbl[order_details].productid = $sql_tbl[products].productid ".$where_condition." GROUP BY $sql_tbl[order_details].productid ORDER BY $search[sort_by] DESC");
	else
		$sales = func_query("SELECT $sql_tbl[partner_product_commissions].product_commission, $sql_tbl[partner_payment].*, $sql_tbl[customers].*, $sql_tbl[order_details].*, $sql_tbl[products].product, $sql_tbl[products].productid, $sql_tbl[order_details].amount*$sql_tbl[order_details].price as total FROM $sql_tbl[customers], $sql_tbl[partner_payment], $sql_tbl[order_details], $sql_tbl[products], $sql_tbl[orders], $sql_tbl[partner_product_commissions] WHERE $sql_tbl[partner_product_commissions].itemid = $sql_tbl[order_details].itemid AND $sql_tbl[partner_product_commissions].orderid = $sql_tbl[order_details].orderid AND $sql_tbl[partner_product_commissions].login = $sql_tbl[customers].login AND $sql_tbl[customers].usertype = 'B' AND $sql_tbl[customers].status = 'Y' AND $sql_tbl[partner_payment].login = $sql_tbl[customers].login AND $sql_tbl[partner_payment].orderid = $sql_tbl[order_details].orderid AND $sql_tbl[orders].orderid = $sql_tbl[order_details].orderid AND $sql_tbl[order_details].productid = $sql_tbl[products].productid ".$where_condition." GROUP BY $sql_tbl[partner_payment].payment_id, $sql_tbl[order_details].itemid");

	if ($current_area == 'B' && $sales && $search['top'] != 'Y') {
		$parent_pending = 0;
		$parent_paid = 0;
		foreach ($sales as $k => $v) {
			if (!empty($v['affiliate'])) {
				if ($v['paid'] == 'Y')
					$parent_paid += $v['product_commission'];
				else
					$parent_pending += $v['product_commission'];
				unset($sales[$k]);

			}
		}
		$smarty->assign("parent_pending", $parent_pending);
		$smarty->assign("parent_paid", $parent_paid);
	}
	$smarty->assign("sales", $sales);
}

$smarty->assign("partners", func_query("SELECT * FROM $sql_tbl[customers] WHERE usertype = 'B' AND status = 'Y'"));

$smarty->assign("search", $search);

$smarty->assign("month_begin", mktime(0,0,0,date('m'),1,date('Y')));
?>
