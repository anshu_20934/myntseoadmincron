<?php
/*****************************************************************************\
+-----------------------------------------------------------------------------+
| X-Cart                                                                      |
| Copyright (c) 2001-2006 Ruslan R. Fazliev <rrf@rrf.ru>                      |
| All rights reserved.                                                        |
+-----------------------------------------------------------------------------+
| PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
| FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
| AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
|                                                                             |
| THIS  AGREEMENT  EXPRESSES  THE  TERMS  AND CONDITIONS ON WHICH YOU MAY USE |
| THIS SOFTWARE   PROGRAM   AND  ASSOCIATED  DOCUMENTATION   THAT  RUSLAN  R. |
| FAZLIEV (hereinafter  referred to as "THE AUTHOR") IS FURNISHING  OR MAKING |
| AVAILABLE TO YOU WITH  THIS  AGREEMENT  (COLLECTIVELY,  THE  "SOFTWARE").   |
| PLEASE   REVIEW   THE  TERMS  AND   CONDITIONS  OF  THIS  LICENSE AGREEMENT |
| CAREFULLY   BEFORE   INSTALLING   OR  USING  THE  SOFTWARE.  BY INSTALLING, |
| COPYING   OR   OTHERWISE   USING   THE   SOFTWARE,  YOU  AND  YOUR  COMPANY |
| (COLLECTIVELY,  "YOU")  ARE  ACCEPTING  AND AGREEING  TO  THE TERMS OF THIS |
| LICENSE   AGREEMENT.   IF  YOU    ARE  NOT  WILLING   TO  BE  BOUND BY THIS |
| AGREEMENT, DO  NOT INSTALL OR USE THE SOFTWARE.  VARIOUS   COPYRIGHTS   AND |
| OTHER   INTELLECTUAL   PROPERTY   RIGHTS    PROTECT   THE   SOFTWARE.  THIS |
| AGREEMENT IS A LICENSE AGREEMENT THAT GIVES  YOU  LIMITED  RIGHTS   TO  USE |
| THE  SOFTWARE   AND  NOT  AN  AGREEMENT  FOR SALE OR FOR  TRANSFER OF TITLE.|
| THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY GRANTED BY THIS AGREEMENT.      |
|                                                                             |
| The Initial Developer of the Original Code is Ruslan R. Fazliev             |
| Portions created by Ruslan R. Fazliev are Copyright (C) 2001-2006           |
| Ruslan R. Fazliev. All Rights Reserved.                                     |
+-----------------------------------------------------------------------------+
\*****************************************************************************/

#
# $Id: categories.php,v 1.93.2.1 2006/04/13 14:58:05 svowl Exp $
#

if ( !defined('XCART_SESSION_START') ) { header("Location: ../"); die("Access denied"); }

x_load('files');

#
# Functions definition
#

#
# This function builds the categories list within specified category ($cat)
#
function func_get_categories_list($cat=0, $short_list=true, $flag=NULL) {

    global $current_area, $sql_tbl, $shop_language, $active_modules, $config, $xcart_dir;

	    $categories = func_query ("SELECT $sql_tbl[mk_featured_categories].category_id, $sql_tbl[categories].category, $sql_tbl[mk_featured_categories].display_order , $sql_tbl[mk_featured_categories].is_active from $sql_tbl[mk_featured_categories], $sql_tbl[categories] where $sql_tbl[mk_featured_categories].category_id=$sql_tbl[categories].categoryid   order by $sql_tbl[mk_featured_categories].display_order");
	
	return $categories;
	
	//return array("all_categories" => $all_categories, "categories" => $categories, "subcategories" => $subcategories);
}


#
# Main code
#
if (empty($mode)) $mode = "";


$cat = (empty ($cat) ? "0" : $cat);

/* posting the category data */



if ($REQUEST_METHOD == "POST") {

	
  
  
	if ($mode == "applycat") {

		#
		# Update featured category list
		#
		if (is_array($posted_data)) {
			foreach ($posted_data as $categoryid=>$v) {
				if(!empty($v["delete"]))
				{
					  
					 $query_data = array(
						"is_active" => (!empty($v["is_active"]) ? "1" : "0"),
						"display_order" => intval($v["order_by"])
					);
					func_array2update("mk_featured_categories", $query_data, "category_id='$categoryid'");
					$top_message["content"] = func_get_langvar_by_name("msg_adm_featcategory_upd");
					$top_message["anchor"] = "featured";
				}
			}
			
		}
	
	} elseif ($mode == "deletecat") {

		#
		# Delete selected featured categories from the list
		#
		if (is_array($posted_data)) {
			
			foreach ($posted_data as $categoryid=>$v) {
				
               	if (empty($v["delete"]))
					continue;
				db_query ("DELETE FROM $sql_tbl[mk_featured_categories] WHERE category_id='$categoryid'");
			}
			$top_message["content"] = func_get_langvar_by_name("msg_adm_featcategory_del");
		}

	} elseif ($mode == "addcat" && intval($newproductid) > 0) {

		#
		# Add new featured product
		#
		$newavail = (!empty($newavail) ? "1" : "0");
		if ($neworder == "") {
			$maxorder = func_query_first_cell("SELECT MAX(display_order) FROM $sql_tbl[mk_featured_categories] WHERE category_id='$f_cat'");
			$neworder = $maxorder + 10;
		}

		if (!func_query_first("SELECT category_id FROM $sql_tbl[mk_featured_categories] WHERE category_id='$newproductid'")) {
            
			db_query("REPLACE INTO $sql_tbl[mk_featured_categories] (category_id, display_order, is_active) VALUES ('$newproductid','$neworder','$newavail')");
			$top_message["content"] = func_get_langvar_by_name("msg_adm_featcategory_upd");
		}else{
           $top_message["content"] = func_get_langvar_by_name("lbl_already_exist_record");
		}
	}
	
	$top_message["anchor"] = "featured";
	
	//func_header_location("admin_homepage.php?cat=$cat");

}


#
# Gather the array of categories and extract into separated arrays:
# $all_categories, $categories and $subcategories
#
if (defined('FEATURED_CATEGORY')) {
	$_categories = func_get_categories_list($cat);
}

$smarty->assign("categories", $_categories);


$smarty->assign("cat", $cat);

?>