<?php

include_once "../../top.inc.php";
include_once ($xcart_dir. "/authAPI.php");
@include_once ($xcart_dir. "/Profiler/Profiler.php");
include_once($xcart_dir.'/logger.php');
include_once $xcart_dir."/init.php";
require_once($xcart_dir."/include/fb/facebook_login.php");
include_once "$xcart_dir/include/class/class.feature_gate.keyvaluepair.php";

	$fbUpdateEnabled=\FeatureGateKeyValuePairs::getBoolean('FBDatabase.update.enabled');
	if(empty($fbUpdateEnabled)){
		return;
	}
	
	$facebook_login = new Facebook_Login();
	$facebook = $facebook_login->facebook;
	$myntra_login = $_POST['myntra_login'];
	unset($_POST['myntra_login']);
	$facebook  = $facebook->setAccessToken($_POST['fb_access_token']);
	$weblog->debug("File fb_database_update.php : Myntra Login: $myntra_login : FB_APP_ID: $facebook_app_id");
	$weblog->debug("Access Token: " . print_r($_POST['fb_access_token'],TRUE));
	$fb_uid = $facebook->getUser();
	if ($fb_uid) {
		$weblog->debug("File fb_database_update.php : FB UID: $fb_uid");		
		try {
			$multiQuery = "{ 
		  		'query1':'SELECT uid,first_name,last_name,name,birthday_date,sex,hometown_location,relationship_status,current_location,activities,interests,music,tv,movies,books,quotes,about_me,email,family FROM user WHERE uid =$fb_uid',
	     		'query2':'SELECT uid,first_name,last_name,name,birthday_date,sex,hometown_location,current_location,family,relationship_status,activities,interests,music,tv,movies,books,quotes,about_me from user WHERE uid in (SELECT uid2 FROM friend WHERE uid1 = $fb_uid)'
	     	}";
			  
			$param = array(       
				'method' => 'fql.multiquery',       
				'queries' => $multiQuery,       
				'callback' => '');
			$weblog->debug("File fb_database_update.php : Firing FQL multiQuery Request");
			$queryresults = $facebook->api($param);
			$weblog->debug("File fb_database_update.php : Got FQL multiQuery result");
		    $fb_me = $queryresults[0]['fql_result_set'][0];
		    $fb_friends = $queryresults[1]['fql_result_set'];
		    $fb_likes=null;
		    //$weblog->debug("File fb_database_update.php : FB_ME : " . print_r($fb_me,TRUE));
		    //$weblog->debug("File fb_database_update.php : FB_friends : " . print_r($fb_friends,TRUE));
		    $facebook_login->insertOrUpdateFaceBookDB($fb_me,$fb_friends,$fb_likes,$myntra_login);
		    $facebook_login->updateFaceBookCustomerToDB($fb_me,$fb_uid);
		    $weblog->debug("File fb_database_update.php : Completed");
			  //  $fb_likes = $queryresults[2]['fql_result_set'];
			//insertOrUpdateFaceBookDB($fb_me,$fb_friends,$fb_likes,$myntra_login);
		    
		}catch (FacebookApiException $e) {
			$weblog->debug("File fb_database_update.php : FACEBOOK API EXCEPTION");
		}  
	} else {
		$weblog->debug("File fb_database_update.php : NO FB Session");
	}
?>
