<?php
require_once("$xcart_dir/include/fb/facebook.php");

define('FACEBOOK_APP_ID', $facebook_app_id);
define('FACEBOOK_SECRET', $facebook_app_secret);

/**
 *
 * @author Mitesh Gupta <mitesh.gupta@myntra.com>
 */
class Facebook_Login
{
	public $facebook;

	public function getFacebookCookie($app_id=FACEBOOK_APP_ID, $application_secret=FACEBOOK_SECRET) {
		$args = array();
		parse_str(trim($_COOKIE['fbs_' . $app_id], '\\"'), $args);
		ksort($args);
		$payload = '';
		foreach ($args as $key => $value) {
			if ($key != 'sig') {
				$payload .= $key . '=' . $value;
			}
		}
		if (md5($payload . $application_secret) != $args['sig']) {
			return null;
		}
		return $args;
	}
	
	public function __construct() {
		$this->facebook = new Facebook(array(
			'appId'  => FACEBOOK_APP_ID,
			'secret' => FACEBOOK_SECRET,
			'fileUpload' => true,  
			'cookie' => true,
		));	
		$this->facebook->setFileUploadSupport(true);
	}
	
	public function insertFamiliyInformation($profile_id,$data){
		
		$del="DELETE from mk_facebook_family where profile_id='$profile_id'";
		$status = db_query($del);
		if(status&&!empty($data)) {
			$sql = array(); 
			foreach( $data as $row ) {
	    		$sql[] = '("'.mysql_real_escape_string($profile_id).'", "'.mysql_real_escape_string($row['uid']).'","'.mysql_real_escape_string($row['name']).'","'.mysql_real_escape_string($row['birthday ']).'","'.mysql_real_escape_string($row['relationship']).'")';
			}
			db_query('INSERT INTO mk_facebook_family (profile_id, fb_uid, fb_name, fb_birthday, fb_relationship) VALUES '.implode(',', $sql));
		}
		//mysql_real_query('INSERT INTO mk_facebook_family (profile_id, fb_uid, fb_name, fb_birthday, fb_relationship) VALUES '.implode(',', $sql));
	}
	
	public function insertOrUpdateFaceBookDB($fb_me,$fb_friends,$fb_likes,$myntra_login=null){
		$facebook_values = array();
		$fb_uid = $this->facebook->getUser();
		$facebook_values['fb_name'] = mysql_real_escape_string($fb_me["name"]);
		$facebook_values['fb_first_name'] = mysql_real_escape_string($fb_me["first_name"]);
		$facebook_values['fb_last_name'] = mysql_real_escape_string($fb_me["last_name"]);
		$birth_date = date_create_from_format('m/d/Y',$fb_me["birthday_date"]);
		$facebook_values['fb_birthday'] =date_format($birth_date,'Y-m-d');
		$facebook_values['fb_about_me'] = mysql_real_escape_string($fb_me["about_me"]);
		$facebook_values['fb_email'] = $fb_me["email"];
		$facebook_values['fb_mobile_phone'] = mysql_real_escape_string($fb_me["mobile_phone"]);
		$facebook_values['fb_address_street'] = mysql_real_escape_string($fb_me["address"]["street"]);
		$facebook_values['fb_address_city'] = mysql_real_escape_string($fb_me["current_location"]["city"]);
		$facebook_values['fb_address_state'] = mysql_real_escape_string($fb_me["current_location"]["state"]);
		$facebook_values['fb_address_country'] = mysql_real_escape_string($fb_me["current_location"]["country"]);
		$facebook_values['fb_address_zip'] = mysql_real_escape_string($fb_me["current_location"]["zip"]);
		$facebook_values['fb_gender'] = mysql_real_escape_string($fb_me["sex"]);
		$facebook_values['fb_hometown'] = mysql_real_escape_string($fb_me["hometown_location"]["city"]);
		$facebook_values['fb_location'] = mysql_real_escape_string($fb_me["current_location"]["city"]);
		$facebook_values['fb_relationship_status'] = mysql_real_escape_string($fb_me["relationship_status"]);
		$facebook_values['fb_activities'] = mysql_real_escape_string($fb_me["activities"]);
		$facebook_values['fb_interests'] = mysql_real_escape_string($fb_me["interests"]);
		$facebook_values['fb_music'] = mysql_real_escape_string($fb_me["music"]);
		$facebook_values['fb_tv'] = mysql_real_escape_string($fb_me["tv"]);
		$facebook_values['fb_movies'] = mysql_real_escape_string($fb_me["movies"]);
		$facebook_values['fb_books'] = mysql_real_escape_string($fb_me["books"]);
		$facebook_values['fb_quotes'] = mysql_real_escape_string($fb_me["quotes"]);
		$facebook_values['myntra_login'] = mysql_real_escape_string($fb_me["email"]);
		if(!empty($myntra_login)){
			$facebook_values['myntra_login'] = mysql_real_escape_string($myntra_login);
		}
		//$this->insertFamiliyInformation($fb_uid,$fb_me["family"]);
		if(!empty($fb_likes)) $facebook_values['fb_likes'] = $this->custom_mysql_escape_string($fb_likes);
		$facebook_values['fb_family'] = $this->custom_mysql_escape_string($fb_me["family"]);
		if(!empty($fb_friends)) $facebook_values['fb_friends'] = $this->custom_mysql_escape_string($fb_friends);
		/*$facebook_values['fb_movies'] = $this->getFacebookMovies();
		$facebook_values['fb_music'] = $this->getFacebookMusic();
		$facebook_values['fb_books'] = $this->getFacebookBooks();
		$facebook_values['fb_interests'] = $this->getFacebookInterests();
		$facebook_values['fb_quotes'] = $this->getFacebookQuotes();*/
		$_curtime = time();
		$facebook_values['fb_last_login'] = $_curtime;
		$facebook_values['fb_session_key'] = mysql_real_escape_string(json_encode($this->facebook->getAccessToken()));
		$myntraEmail = func_query_first_cell("SELECT myntra_login FROM mk_facebook_user WHERE fb_uid='$fb_uid'");
		if(!empty($myntraEmail)) {
			func_array2update('mk_facebook_user', $facebook_values, "fb_uid='$fb_uid'");
		} else {			
			$facebook_values['fb_uid'] = $fb_uid;
			$facebook_values['myntra_login'] = $fb_me["email"];
			if(!empty($myntra_login)){
				$facebook_values['myntra_login'] = $myntra_login;
			}
			$facebook_values['fb_first_login'] = $_curtime;
			func_array2update('mk_facebook_user', $facebook_values, "fb_uid='$fb_uid'");
		}		
	}
	
	public function updateFaceBookCustomerToDB($fb_me,$fb_uid){
		$profile_values = array();
		$personna_values=array();
		$myntraLogin = func_query_first_cell("SELECT myntra_login FROM mk_facebook_user WHERE fb_uid='$fb_uid'");
		$profile_values['firstname'] = mysql_real_escape_string($fb_me["first_name"]);
		$profile_values['lastname'] = mysql_real_escape_string($fb_me["last_name"]);
		$profile_values['b_firstname'] = mysql_real_escape_string($fb_me["first_name"]);
		$profile_values['b_lastname'] = mysql_real_escape_string($fb_me["last_name"]);
		$profile_values['s_firstname'] = mysql_real_escape_string($fb_me["first_name"]);
		$profile_values['s_lastname'] =mysql_real_escape_string($fb_me["last_name"]);
		############### fields added later ######################### 
		$profile_values['gender'] = mysql_real_escape_string($fb_me["sex"]);
		$profile_values['about_me'] = mysql_real_escape_string($fb_me["about_me"]);    
		$birth_date = date_create_from_format('m/d/Y',$fb_me["birthday_date"]);
		$profile_values['DOB'] = date_format($birth_date,'Y-m-d')?date_format($birth_date,'Y-m-d'):$birth_date;		
		$_curtime = time();
		$profile_values['last_login']=$_curtime;
		func_array2update('customers', $profile_values,"login='$myntraLogin'");
		
		$personna_values['relationship_status'] = mysql_real_escape_string($fb_me["relationship_status"]);
		if(func_query_first_cell("SELECT COUNT(*) FROM customer_personna WHERE login='$myntraLogin'")> 0){
			func_array2updateWithTypeCheck('customer_personna', $personna_values,"login='$myntraLogin'");
		}
		else {
			$personna_values['login']=mysql_real_escape_string($myntraLogin); 
			func_array2insertWithTypeCheck('customer_personna', $personna_values);
		}
		
		
	}
	
	public function getFacebookLikes(){
		try{
			$fb_likes = $this->facebook->api('/me/likes');
			return $this->custom_mysql_escape_string($fb_likes);
		} catch (FacebookApiException $e) {
	    	error_log($e);
	  	}
	}
	
	public function getFacebookMovies(){
		try{
			$fb_movies = $this->facebook->api('/me/movies');
			return $this->custom_mysql_escape_string($fb_movies);
		} catch (FacebookApiException $e) {
	    	error_log($e);
	  	}
	}
	
	public function getFacebookMusic(){
		try{
			$fb_music = $this->facebook->api('/me/music');
			return $this->custom_mysql_escape_string($fb_music);
		} catch (FacebookApiException $e) {
	    	error_log($e);
	  	}
	}
	
	public function getFacebookBooks(){
		try{
			$fb_books = $this->facebook->api('/me/books');
			return $this->custom_mysql_escape_string($fb_books);
		} catch (FacebookApiException $e) {
	    	error_log($e);
	  	}
		
	}
	
	public function getFacebookInterests(){
		try{
			$fb_interests = $this->facebook->api('/me/interests');
			return $this->custom_mysql_escape_string($fb_interests);
		} catch (FacebookApiException $e) {
	    	error_log($e);
	  	}
	}
	
	public function getFacebookQuotes(){
		try{
			$fb_quotes = $this->facebook->api('/me/quotes');
			return $this->custom_mysql_escape_string($fb_quotes);
		} catch (FacebookApiException $e) {
	    	error_log($e);
	  	}
	}
	
	public function custom_mysql_escape_string($obj){
		$quote = "\"";
		$json_obj = json_encode($obj);
		$length = strlen($json_obj); 
		
		$maxLength = 65400;//a little less than mysql text column.
		if($length>$maxLength) {
				Profiler::increment("facebook-maxlength-exceeded");
		}

		if(is_array($obj)) {
			$arrayLen = count($obj);			
			while($length>$maxLength) {
				$arrayLen = floor($arrayLen*$maxLength/$length);
				$obj = array_slice($obj, 0,$arrayLen);
				$json_obj = json_encode($obj);
				$length = strlen($json_obj);
				Profiler::increment("facebook-maxlength-truncate-attempts");
			}
		}		
		$retval = mysql_real_escape_string($json_obj);
		$retval = $quote . $retval . $quote;
		return $retval;
	}
}
