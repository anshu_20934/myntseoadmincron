<?php 

function getGraphObject($query) {

    $object_types = func_select_query("mk_opengraph_objects");
    foreach ($object_types as $object_type) {
        $synonyms = explode(",", $object_type['synonyms']);
        foreach ($synonyms as $synonym) {
            $pattern = '/\b' . $synonym . '\b/';
            if (preg_match($pattern, $query) > 0) {
                return $object_type['type'];
            }
        }
    }

    return "product";
}

