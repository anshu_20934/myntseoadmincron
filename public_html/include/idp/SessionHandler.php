<?php
include_once $xcart_dir.'/modules/RestAPI/RestRequest.php';
    class SessionHandler {

        public function createSession() {
            global $errorlog;
            global $weblog;
            $profileHandler = Profiler::startTiming("idp-create-session");

            $url = HostConfig::$portalIDPSessionServiceHost.'/create';
            $restRequest = new RestRequest($url, 'GET');

            $headers = array('Accept:  application/json');
            $restRequest->setHttpHeaders($headers);

            $restRequest->execute();
            $responseInfo = $restRequest->getResponseInfo();
            $responseBody = $restRequest->getResponseBody();
            if(!$responseBody || $responseBody->status == 'ERROR') {
                $weblog->error('IDP Session :: '.print_r($restRequest,true));
                $errorlog->error('IDP Session :: '.print_r($restRequest,true));
                return null;
            }

            $session = json_decode($responseBody,true);
            Profiler::endTiming($profileHandler);
            return $session['id']; 
        }
    }
?>
