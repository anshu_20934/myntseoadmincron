<?php
#
# $Id: search.php,v 1.141.2.8 2006/06/06 05:56:52 svowl Exp $
#
include_once("../auth.php");
//require_once('solr/Apache/Solr/Service.php');
//include_once('solr/solrCommon.php');
//include_once('solr/solrUtils.php');
require_once('solr/solrProducts.php');
include_once ("func/func.search.php");
require_once ("func/func.core.php");
include("func/func_seo.php");
// empty unneccessary session variables
if (x_session_is_registered("chosenProductConfiguration"))
x_session_unregister("chosenProductConfiguration");

if (x_session_is_registered("masterCustomizationArray"))
x_session_unregister("masterCustomizationArray");
## Commented not in use ###########
###if($active_modules["Manufacturers"])
###include $xcart_dir."/modules/Manufacturers/customer_manufacturers.php";
$tmp=strstr($QUERY_STRING, "$XCART_SESSION_NAME=");
if (!empty($tmp))
$QUERY_STRING=ereg_replace("$XCART_SESSION_NAME=([0-9a-zA-Z]*)", "", $QUERY_STRING);


//copied from func.search.php and added teedy and mirror to the list - by venu
$business_keywords =  array("","T-Shirt","Mug","Coaster","Mousepad","Greeting Card","Poster","Keychain","Jigsaw","Notepad","Calendar","Plate","Watch","Pendant","Photo Prints","Sweatshirt","Money Bank","Pencil Box","Teddy Bear","Mirror");


########### added for break URL rewriting#############
$searchPageURL = $_SERVER['REQUEST_URI'];
$_element = explode("/",$searchPageURL);
//echo "<pre>";
//print_r($_element);
$scindex = array_search("search",$_element);
if($scindex !== false){
    $keyid =    $scindex+1;
    $mode =  $_element[$scindex];
    $HTTP_GET_VARS['searchkey'] = urldecode($_element[--$scindex]);

    $HTTP_GET_VARS['key_id'] =  $_element[$keyid];

}
x_session_register("search_data");
$sortparam = $sortparam = strip_tags($HTTP_GET_VARS['sortby']);
if($sortparam  == "USERRATING")
{
    $displayHeading = "top rated by users";
}
elseif($sortparam  == "BESTSELLING")
{
    $displayHeading = "best selling products";
}
elseif($sortparam  == "MOSTRECENT")
{
    $displayHeading = "most recent products";
}
elseif($sortparam  == "EXPERTRATING")
{
    $displayHeading = "top rated by experts";
}
else
{
    $displayHeading = "top rated by experts";
    $sortparam = "EXPERTRATING";
}
$smarty->assign("sortMethod",$sortparam);


if (empty($search_data)) {
	$search_data = array();
}
if ($REQUEST_METHOD == "POST" && $mode == 'search') {
	#
	# Update the session $search_data variable from $posted_data
	#
	$seachKeyword ='';
	 // check whether start date and end date are entered
     if (!empty($startdate) && !empty($enddate))
      {
    	   $stdate = explode("/",$startdate);
		   $posted_data["start_date"] = mktime(0,0,0,$stdate[0],$stdate[1],$stdate[2]);
		   $enddate = explode("/",$enddate);
		   $posted_data["end_date"] = mktime(23,59,59,$enddate[0],$enddate[1],$enddate[2]);
	 }  
		$posted_data["substring"]= removeCommonWords(strtolower($posted_data["substring"]));
		//$filterkeyword =$posted_data["substring"];
		//$posted_data["substring"]=replace_synonyms($posted_data["substring"],&$filterkeyword);
		$posted_data["substring"] =trim(removeSpecialCharandSpaces($posted_data["substring"]));

		# Update the search statistics*/
		if ($posted_data["substring"]) {
			    
				db_query("INSERT INTO $sql_tbl[stats_search] (search, date) VALUES ('".addslashes($posted_data["substring"])."', '".time()."')");
		}

     	
		$seachKeyword = urlencode(trim($posted_data['substring']));
		//$search_data['products']['substring'] =trim($search_data['products']['substring']);
		$seachKeyword = preg_replace('#([ ])+#',' ',$seachKeyword);
		 

	//If logged in user is mykriti user redirect it to mykriti search result
	if($current_area == 'C')
	{
		    // update the search statistics
			if(!empty($seachKeyword)){
          
				$seachKeyword = preg_replace("/[^a-zA-Z0-9s-]/", " ", $seachKeyword);
				
				$filteredKeyword = ''; 
				$additional_keyword_field  = '';
				$filteredKeyword = $seachKeyword;
				// filter the keywords
				
				check_product_type_synonyms($seachKeyword,$filteredKeyword);
				//echo "search word".$seachKeyword." filteredkeyword".$filteredKeyword;exit;
				//$filteredKeyword = checkForProductSynonyms($seachKeyword);
				$filthyWordStatus = false;
				$keysArray = explode(" ",trim($filteredKeyword));
				$curseWordArray=file_get_contents(dirname(__FILE__)."/../skin1/dirtywords.txt");
				$curseWordArray = preg_split("/(\r\n|\r|\n)/",$curseWordArray) ;

     			foreach($keysArray as $key=>$value){
			 		foreach($curseWordArray as $curse_word_line){
			 					if(strcmp(strtolower(trim($value)),strtolower(trim($curse_word_line)))){
			 						$filterKeywords .= trim($value)." ";
			 					}else{
			 						$filthyWordStatus = true;
			 					}
			 					 	
			 		}
     			}
					
				if(!$filthyWordStatus){
					 $sql = "select name from  mk_product_type where id='".$search_data['products']['by_type']."' ";
					$result = db_query($sql);
					while($row = db_fetch_array($result)){
 					$single_word = $row['name'];
  					}
					
					if((!empty($search_data['products']['by_type']))&&(!strcmp(trim($seachKeyword),trim($filteredKeyword)))){
						
						$seachKeyword = $filteredKeyword." ".strtolower(trim($single_word));
					}
					//echo $seachKeyword."    ".$filteredKeyword;
						$results=db_query("select url from mk_category_search where keyword='".trim($seachKeyword)."' ");
						$row=db_fetch_array($results);
							if(db_num_rows($results)!=0){
								func_header_location($http_location."/".$row['url']);
							}
							$key_id = track_search_keywords($seachKeyword,$filteredKeyword);
							$seachKeyword =  trim($seachKeyword);
							$filter_search_query = preg_replace('#([ ])+#','-',$seachKeyword);
							$HTTP_GET_VARS['searchkey'] =  $filter_search_query;
							$HTTP_GET_VARS['key_id'] = $key_id;
				}
				else
				{
	         	    		$seachKeyword = preg_replace('#([ ])+#','-',$seachKeyword);
							$HTTP_GET_VARS['searchkey'] =  $searchKeyword;
                    		$HTTP_GET_VARS['key_id'] = 0;
				}
			  }  
			  
			 
	}
	
	
}
// GET request directly comes here - POST passes through the above IF block
if ($mode == "search") {

//code for checking category,it will redirect to category page
$newresult=db_query("select orignal_keyword from mk_stats_search where key_id='".$HTTP_GET_VARS['key_id']."' " );
$newrow=db_fetch_array($newresult);
$results=db_query("select url from mk_category_search where key_id='".$HTTP_GET_VARS['key_id']."' or keyword='".$newrow['orignal_keyword']."' ");
$row=db_fetch_array($results);
	if(db_num_rows($results)!=0){
		func_header_location($http_location."/".$row['url']);
	}
//end of the category code
   	#
	# Perform search and display results
	#
	$data = array();
	$flag_save = false;

	if (!empty($page) && $search_data["products"]["page"] != intval($page))
	{
		# Store the current page number in the session
		$search_data["products"]["page"] = $page;
		$flag_save = true;
	}

	$keywordid = 0;
	$validSearch=false;
	//print_r($HTTP_GET_VARS);exit;
	if(!empty($HTTP_GET_VARS['searchkey']))
	{
		$searchKeyData = getSearchKeywordById($HTTP_GET_VARS['key_id']);
		//important code dont modify it plssssssssss,for deleting duplicate content from google's index.
		//$noindex=2 means it will add  noindex in the metatags is results are empty,in case of is_valid=N
		if(empty($searchKeyData)){
			$noindex=2;
			$smarty->assign("noindex",$noindex); 
		}
		if(!empty($searchKeyData) )
		{
			$data['substring'] = trim($searchKeyData['keyword']);
			$data['orgsubstring'] = trim($searchKeyData['orignal_keyword']);
			if( empty($data['substring']) )
			{
				// cleaning old data , for some reason keyword is empty for some search terms in DB
				// we can update the DB with correct information untill all the data is cleared.
				check_product_type_synonyms($data['orgsubstring'],$filteredKeyword);
				$data['substring'] = $filteredKeyword;
				$query = "update mk_stats_search set keyword='$filteredKeyword' where key_id=$HTTP_GET_VARS[key_id]";
				db_query($query);
			}
			$search_item_keyword = preg_replace('#([ ])+#','-',trim($searchKeyData['orignal_keyword'],"-"));
			
			$keywordid = $searchKeyData['key_id'];
			$smarty->assign("search_key_id",$searchKeyData['key_id']); 
			$smarty->assign("search_key",$searchKeyData['orignal_keyword']); 
			$smarty->assign("search_item_keyword",$search_item_keyword); 
			$validSearch = true;
		}
	}
	if( $validSearch )
	{
		$data["substring"] = trim($data["substring"]);
		if (!empty($data["substring"])) 
		{      
	       	/************ if user come from search engine like google, track the keyword *****************/
			if(!empty($_SERVER['HTTP_REFERER']) )
			{
				     $weblog->info("Referer URL : ".$_SERVER['HTTP_REFERER']." @line ".__LINE__." in file ".__FILE__);
		             parse_str($_SERVER['HTTP_REFERER'],$output);
		             $qry_str = $output['q'];		
		             if(!empty($qry_str)){
		                track_search_keywords(urldecode($qry_str));	
		             }
			} 
		}
	

		switch($sortparam)
		{	
			case "MOSTRECENT" :
				$sortField = "justarrived desc";	
		  		//$orderbys[] = "$sql_tbl[products].add_date DESC";
		  		break;
			case "BESTSELLING" :
				$sortField = "bestselling desc";
				 //$orderbys[] = "$sql_tbl[products].sales_stats DESC";
				 break;
			case "USERRATING" :
				$sortField = "userrating desc";
				//$orderbys[] = "$sql_tbl[products].rating DESC";	 
				break;
			case "EXPERTRATING" :
				$sortField = "myntrarating desc";
				 //$orderbys[] = "$sql_tbl[products].myntra_rating DESC";
				 break;				
		}
		$query = "(shopmasterid:0) AND ";
		$hasKeyword = false;
		//$data['substring'] = removeSpecialChars($data['substring']);
		if($data['substring'])
	    	{
			$query .= "(";
	        	$sterms = explode(" ",preg_replace("/[^a-zA-Z0-9s-]/", " ", $data['substring']));
	        	$count = count($sterms);
	        	$temp = "";
	     		for($i=0;$i<$count;$i++)
	     		{
	   			$s = trim($sterms[$i]);
				$stopChars = array(" ","#","/","\\","\"","'","/","?","&","(",")","`",",","^","*","<",">",".","+",":",";","@","!","%","_","-");
				$s = str_replace($stopChars," ",$s);
				
	         		if(!empty($s) )
	   			{
					$query .= "(keywords:$s OR stylename:$s OR product:$s OR descr:$s OR fulldescr:$s) ";
	       				$temp = $temp . " $s";
					if($i != $count-1)
	        				$query .= " OR ";
	        		}	
	        	}
	 		if($count != 1)
	      		{
	    			$s = $temp;
	  			$query .= "OR (keywords:$s OR stylename:$s OR product:$s OR descr:$s OR fulldescr:$s)";
	  		}   
			$query .= ")";
			$hasKeyword = true;                                      
	   	}     
		//echo $data['orgsubstring'];
	 	$productTypeList = checkForProductSynonyms($data['orgsubstring']);
		//print_r($productTypeList);
		if(!empty($productTypeList))
	    	{
			//$smarty->assign("product_type_ids",$productTypeList);
			$productTypeList = explode(",",$productTypeList);
			if($hasKeyword) 	
		   		$query .= " AND (";
			else
				$query .= " (";
			$smarty->assign("product_type_ids",$productTypeList);
	    		$count = count($productTypeList);
			for($i=0;$i<$count;$i++)
	  			{
				$query .= "(typeid:$productTypeList[$i])";
				if($i != $count-1)
	         		$query .= " OR ";
    			}
  			$query .= ")";
    		}
		try
    		{
    		 	$solrIndex = new solrProducts();
    		}
   		catch(Exception $ex)
		{
 			print_r( $ex);
   		}
		/*if (!isset($objects_per_page)) 
			$objects_per_page = $config["Appearance"]["products_per_page"];*/
		$sResults = $solrIndex->searchIndex($query);
		//$sResults = $solrIndex->searchAndSort($query,$first_page,$objects_per_page,$sortField);
		$total_items = $sResults->numFound;
		
		update_product_count($total_items,$data['substring'],$keywordid);
	    	if ($total_items > 0) 
		{
			update_recent_popular_stats($data['substring'],$data['orgsubstring'],$keywordid);
			$page = $search_data["products"]["page"];
		
			if (!isset($objects_per_page)) 
			{
					//changed to display 16 results in only search page
					$objects_per_page = $config["Appearance"]["products_per_page"]+4;
			}
	
			$total_nav_pages = ceil($total_items/$objects_per_page)+1;
	        
			// search for customer section 
			include $xcart_dir."/include/navigation.php";
			$total =$total_items ;
			$limit = $objects_per_page ;			
			$sorturl = $http_location."/".$search_item_keyword."/search/$keywordid/?".(!empty($sortparam)? "sortby=".$sortparam : '') ."&page=" ;
			$sort = '' ;
			
			$paginator = paginate_search($limit, $page, $total, $sorturl, $sort);
			$smarty->assign("paginator",$paginator);
			$smarty->assign("totalpages",$totalpages);
			$smarty->assign("pg",$page);
			try
                        {
                            $solrIndex = new solrProducts();
                        }
                        catch(Exception $ex)
                        {
                            print_r( $ex);
                        }

                        $sResults = $solrIndex->searchAndSort($query,$first_page,$objects_per_page,$sortField);

			foreach($sResults->docs as $document)
   	    		{
				$productname = $document->__get("product");
				$namelength = strlen($productname);
        	    		$productname = ($namelength >= '21')?substr($productname, 0, 18)."...":$productname;
				$cp_product =  $document->__get("product");
        	    		$cp_product = preg_replace("/[^a-zA-Z0-9s-]/", "-", $cp_product);
        	    		$cp_product = preg_replace('#([ ])+#','-',$cp_product);
        	    		$cp_product = preg_replace('#([-])+#','-',$cp_product);


        	   		$products[] = array(
                        	      "productid"=> $document->__get("productid"),
                        	       "product" => $cp_product,
                        	       "descr" => $document->__get("descr"),
                        	       "fulldescr" => $document->__get("fulldescr"),
                        	       "image_portal_t" => $document->__get("image_portal_t"),
                        	       "img_portal_160" => $document->__get("img_portal_thumb_160"),
                        	       "name" => $document->__get("typename"),
                        	       "typeid" => $document->__get("typeid"),
                        	       "styleid" => $document->__get("styleid"),
                        	       "stylename" => $document->__get("stylename"),
                        	       "styleprice" => $document->__get("price"),
					"prodname" => $productname,
					"popup_image_path"=> $document->__get("popup_image_path")
                           	);
       		 	}

			//Dont remove this - this is to "Sort By : Expert Rating | User Rating | Best Selling | Just Arrived" link in the page
			if($data['substring']!="")
	        	$smarty->assign("navigation_script",$http_location."/".$data['substring']."/search/");
			 else
				$smarty->assign("navigation_script","mkSearchResult.php?mode=search");	

			$smarty->assign("products", $products);
			$smarty->assign("first_item", $first_page+1);
			$smarty->assign("last_item", min($first_page+$objects_per_page, $total_items));
		}
	} // end if valid search
	$smarty->assign("total_items",$total_items);
	$smarty->assign("mode", $mode);
	if ($flag_save)
		x_session_save("search_data");
}


// Venugopal - This is to update MK_TAGS table - not sure why this is used
$searchedfor = $data["substring"];
if(isset($searchedfor) && trim($searchedfor) != '')
{
    $pquery = "select tagid from mk_tags where tagname='$searchedfor'";
    $presult = db_query($pquery);
    if(mysql_num_rows($presult)!=0)
    {
        $query  ="update mk_tags a set a.count=a.count+5 where a.tagname='$searchedfor'";
        $result  = db_query($query);
    }
}



$smarty->assign("search_prefilled", $search_data["products"]);
$smarty->assign("sort_fields", $sort_fields);
$smarty->assign("main","search");

//load product types
$productTypes =func_load_product_type();
$smarty->assign("producttypes", $productTypes);
$captions = array();
$smarty->assign("thickbox","<link rel=\"stylesheet\" href=\"$xcart_web_dir/skin1/css/thickbox.css\" type=\"text/css\" media=\"screen\" />");
$captions[0] =  "Search Results";
##$smarty->assign("captions", $captions);
$smarty->assign("advance", $advance);

########## Load the search types ##########
$prdStr = '';
foreach($productTypes as $_k=>$val){
    $prdStr .= $val[1].',';
}
$meta_keyword = $data["substring"];
$meta_orignal_keyword = $data['orgsubstring'];

$uniq_keyword = str_replace($meta_keyword ,"",  $meta_orignal_keyword);

if(!empty($uniq_keyword)){
$metaKeywords = $meta_orignal_keyword.",".$prdStr;
$smarty->assign("metaDescription","Buy ".$meta_orignal_keyword." in India. The best ".$meta_orignal_keyword." designs available online. Create your own personalized ".$meta_orignal_keyword );
$smarty->assign("pageTitle","Buy ".$meta_orignal_keyword." online in India" );
}else{
$metaKeywords = $meta_keyword.",".$prdStr;
$smarty->assign("metaDescription","Buy ".$meta_keyword.",gifts,t-shirts,mugs,posters online in India.The best ".$meta_keyword." gifts and t-shirts available online. Create your own personalized ".$meta_keyword." gifts" );
$smarty->assign("pageTitle","Buy ".$meta_keyword." gifts and merchandize in India" );
}

//code for getting description
$results=db_query("select description from mk_stats_search where key_id='".$keywordid."' ");
$row_description=db_fetch_array($results);
$smarty->assign ("description",$row_description['description']);

$smarty->assign ("metaKeywords",$metaKeywords);
$recently_added = get_recently_added_search($keywordid);
$popular_search= get_most_popular_search();
$product_filter_results = get_product_filter_results($keywordid);
$smarty->assign("keywordid",$keywordid);
$smarty->assign("product_filter_results",$product_filter_results);

$related_searches = get_related_searches($keywordid);
$smarty->assign("related_searches", $related_searches );
$smarty->assign("recently_added", $recently_added );
$smarty->assign("popular_search", $popular_search);

//code for getting related searches from google code
$related_designs_by_myntra= func_relatedtags("http://www.myntra.com/".str_replace(" ","-",$meta_orignal_keyword)."/search/".$keywordid."/","Buy ".$meta_orignal_keyword." online in India");
$smarty->assign("related_designs_by_myntra", $related_designs_by_myntra);


func_display("customer/mkSearchResult.tpl",$smarty);


?>
