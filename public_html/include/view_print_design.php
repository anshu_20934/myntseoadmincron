<?php
/*****************************************************************************\
+-----------------------------------------------------------------------------+
| X-Cart                                                                      |
| Copyright (c) 2001-2006 Ruslan R. Fazliev <rrf@rrf.ru>                      |
| All rights reserved.                                                        |
+-----------------------------------------------------------------------------+
| PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
| FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
| AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
|                                                                             |
| THIS  AGREEMENT  EXPRESSES  THE  TERMS  AND CONDITIONS ON WHICH YOU MAY USE |
| THIS SOFTWARE   PROGRAM   AND  ASSOCIATED  DOCUMENTATION   THAT  RUSLAN  R. |
| FAZLIEV (hereinafter  referred to as "THE AUTHOR") IS FURNISHING  OR MAKING |
| AVAILABLE TO YOU WITH  THIS  AGREEMENT  (COLLECTIVELY,  THE  "SOFTWARE").   |
| PLEASE   REVIEW   THE  TERMS  AND   CONDITIONS  OF  THIS  LICENSE AGREEMENT |
| CAREFULLY   BEFORE   INSTALLING   OR  USING  THE  SOFTWARE.  BY INSTALLING, |
| COPYING   OR   OTHERWISE   USING   THE   SOFTWARE,  YOU  AND  YOUR  COMPANY |
| (COLLECTIVELY,  "YOU")  ARE  ACCEPTING  AND AGREEING  TO  THE TERMS OF THIS |
| LICENSE   AGREEMENT.   IF  YOU    ARE  NOT  WILLING   TO  BE  BOUND BY THIS |
| AGREEMENT, DO  NOT INSTALL OR USE THE SOFTWARE.  VARIOUS   COPYRIGHTS   AND |
| OTHER   INTELLECTUAL   PROPERTY   RIGHTS    PROTECT   THE   SOFTWARE.  THIS |
| AGREEMENT IS A LICENSE AGREEMENT THAT GIVES  YOU  LIMITED  RIGHTS   TO  USE |
| THE  SOFTWARE   AND  NOT  AN  AGREEMENT  FOR SALE OR FOR  TRANSFER OF TITLE.|
| THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY GRANTED BY THIS AGREEMENT.      |
|                                                                             |
| The Initial Developer of the Original Code is Ruslan R. Fazliev             |
| Portions created by Ruslan R. Fazliev are Copyright (C) 2001-2006           |
| Ruslan R. Fazliev. All Rights Reserved.                                     |
+-----------------------------------------------------------------------------+
\*****************************************************************************/

#
# $Id: history_order.php,v 1.32 2006/01/16 06:47:33 mclap Exp $
#
# Collect infos about ordered products
#

if ( !defined('XCART_SESSION_START') ) { header("Location: ../"); die("Access denied"); }

x_load('order');
include_once("../mkImageTransform.php");
include_once("../include/func/func.mkcore.php");
include_once("../include/func/func.randnum.php");
include_once("func.stylepreview.php");

if (empty($mode)) $mode = "";

if(!empty($orderid))
{

	$order_data = func_order_data($orderid);

	if (empty($order_data))
	    	return false;

	#
	# Security check if order owned by another customer
	#
	if ($current_area == 'C' && $order_data["userinfo"]["login"] != $login) {
		func_header_location("error_message.php?access_denied&id=35");
	}
	$smarty->assign("order_details_fields_labels", func_order_details_fields_as_labels());
	$smarty->assign("order", $order_data["order"]);
	$smarty->assign("customer", $order_data["userinfo"]);
	$order_data["products"] = func_translate_products($order_data["products"], ($order_data["userinfo"]['language']?$order_data["userinfo"]['language']:$config['default_customer_language']));
	$smarty->assign("products", $order_data["products"]);
	$smarty->assign("giftcerts", $order_data["giftcerts"]);
	if ($order_data)
	{
		$owner_condition = "";
		if ($current_area == "C")
			$owner_condition = " AND $sql_tbl[orders].login='".$login."'";
		elseif ($current_area == "P" && !$single_mode ) {
			$owner_condition = " AND $sql_tbl[order_details].provider='".$login."'";
		}
		# find next
		$tmp = func_query_first("SELECT $sql_tbl[orders].orderid FROM $sql_tbl[orders], $sql_tbl[order_details] WHERE $sql_tbl[orders].orderid>'".$orderid."' AND $sql_tbl[order_details].orderid=$sql_tbl[orders].orderid $owner_condition ORDER BY $sql_tbl[orders].orderid ASC LIMIT 1");
		if (!empty($tmp["orderid"]))
			$smarty->assign("orderid_next", $tmp["orderid"]);
		# find prev
		$tmp = func_query_first("SELECT $sql_tbl[orders].orderid FROM $sql_tbl[orders], $sql_tbl[order_details] WHERE $sql_tbl[orders].orderid<'".$orderid."' AND $sql_tbl[order_details].orderid=$sql_tbl[orders].orderid $owner_condition ORDER BY $sql_tbl[orders].orderid DESC LIMIT 1");

	   # get the details of products
	   $previewQuery = "SELECT pt.name as type_name,ps.name as style_name
	   FROM $sql_tbl[orders]
	   INNER  JOIN $sql_tbl[order_details] ON $sql_tbl[orders].orderid = $sql_tbl[order_details].orderid
	   INNER JOIN  $sql_tbl[mk_product_type] pt ON  pt.id = $sql_tbl[order_details].product_type
	   INNER JOIN  $sql_tbl[mk_product_style] ps ON  ps.id = $sql_tbl[order_details].product_style
	   WHERE  $sql_tbl[orders].orderid = '".$orderid."' AND  $sql_tbl[order_details].productid='".$_GET[productId]."' "; 
	   $sqllog->debug("Query  : ". $previewQuery." File Name : ".__FILE__);

	   $products_design_preview = func_query($previewQuery);
       $smarty->assign("products_design_preview", $products_design_preview);
       $productTypeName =  $products_design_preview[0]['name'];
       
	  

	   $styleoption = "SELECT $sql_tbl[mk_product_options].name AS optionName, $sql_tbl[mk_product_options].value AS optionValue FROM $sql_tbl[mk_product_options]
	   INNER JOIN $sql_tbl[mk_product_options_order_details_rel]
	   ON $sql_tbl[mk_product_options].id = $sql_tbl[mk_product_options_order_details_rel].option_id
	   WHERE $sql_tbl[mk_product_options_order_details_rel].orderid = '".$orderid."'
	   AND $sql_tbl[mk_product_options_order_details_rel].itemid = ".$_GET['productId']."";
	    $sqllog->debug("Query  : ". $styleoption." File Name : ".__FILE__."  Line No ".__LINE__);

		$optionresult = db_query($styleoption);
		while($optionrow = db_fetch_array($optionresult))
		{
			$optionName[] =  $optionrow['optionName'];
			$optionValue[] =  $optionrow['optionValue'];
		}

		$custArea ="SELECT ca.name AS CustName, ca.label AS CustLabel, ca.id AS AreaId, odc.area_image_final AS CustImage,odc.style_id,
		templateid,addonid
		FROM $sql_tbl[mk_style_customization_area] ca,
		$sql_tbl[mk_xcart_order_customized_area_image_rel] odc
		WHERE ca.id = odc.area_id
		AND odc.orderid='".$orderid."'
		AND odc.item_id= ".$_GET['productId']." 
        group by odc.orderid,odc.item_id,odc.area_id,odc.style_id,odc.templateid";
		
		$sqllog->debug("Query  : ". $custArea." File Name : ".__FILE__."  Line No ".__LINE__);   
		
        $custResult = db_query($custArea);
		$i=0;
		$productID = $_GET['productId'];
		$templates = array();		
		while($custrow = db_fetch_array($custResult))
		{
			$actualCustomizeAreaId =  $custrow['AreaId'];
			$custAreaId[] =  $custrow['AreaId'];
			$custName[] =  $custrow['CustName'];
			$custLabel[] =  $custrow['CustLabel'];
			############ added for album book ##################
			$custPageNo[] =  $custrow['page_no'];
			
			/* get the actual style id and the style id for which the order has been placed */
			$sql = "SELECT image,product_style_id,style_id,p.product,area_id FROM $sql_tbl[mk_xcart_order_details_customization_rel] 
			as orderrel  INNER JOIN 
			$sql_tbl[products] as p ON 
			p.productid= orderrel.itemid WHERE 
			itemid='".$productID."' AND orderid = '".$orderid."' AND area_id='".$custrow['AreaId']."' " ;
			$sqllog->debug("Query  : ". $sql." File Name : ".__FILE__."  Line No ".__LINE__);    
			$rsRow1 = db_query($sql);
			$result1 = db_fetch_array($rsRow1);	
			# compare the orignal and styleid for which the order has been placed.If style ids are same generate 
			# the image  
			if($result1[product_style_id] != $result1[style_id] ) 
			{
				
				$sql = "SELECT image_l,id FROM mk_style_customization_area WHERE 
				style_id= '".$custrow[style_id]."' AND  name = '$custrow[CustName]' ";
				$sqllog->debug("Query  : ". $sql." File Name : ".__FILE__."  Line No ".__LINE__);   
				$rsRow2 = db_query($sql);
				$result2 = db_fetch_array($rsRow2);	
				$tempImage = changeCustImage($result2 ['id'],$result2['image_l'],$result1['image'],$actualCustomizeAreaId);
				$custImage[] = $tempImage;
			}
			else 
			{
				if(!file_exists(".".$custrow['CustImage']))
				{
					$sql = "SELECT image_l,id FROM mk_style_customization_area WHERE 
					style_id= '".$custrow[style_id]."' AND id ='".$custrow[AreaId]."' ";
					$sqllog->debug("Query  : ". $sql." File Name : ".__FILE__."  Line No ".__LINE__);   
					$rsRow2 = db_query($sql);
					$result2 = db_fetch_array($rsRow2);	
					if(preg_match('/http:\/\//',$result1['image']))
				    {
						  $custImage[] = $result1['image'];
					}
					else
                    { 
					    $tempImage = changeCustImage($custrow['AreaId'],$result2['image_l'],$result1['image'],$actualCustomizeAreaId);
					    $custImage[] = $tempImage;
					}
					
				}
				else 
				{
				  	   $custImage[] = ".".$custrow['CustImage'];
				}
			}
			
			
			// load template if exist 
			
			if(!empty($custrow['templateid'])){
					$tpls = db_query("SELECT template_master_name as template_name  FROM mk_template_master WHERE template_master_id ='".$custrow['templateid']."' ");
					$result = db_fetch_array($tpls);
					$template = $result['template_name'];
					$templates[] = $template;
					
		 	}
			// load addonid if exist
			$addons = array();
			if( !empty($custrow['addonid']))
			{
				$selectedAddonName = get_style_addon_name($custrow['addonid']);
			        if( $selectedAddonName)
			        {
        			        $addon = $selectedAddonName;
					$addons[] = $addon;
        			}
			}		
			/* added later to show the preview details */

			$previewDetails = "SELECT ca.id,ca.image_x,ca.image_y,ca.image_width,ca.image_height,ca.text,
			 				   ca.text_x,ca.text_y,
			                   ca.text_width ,ca.text_height,ca.text_format,ca.orientation_final_image_display,
			                   ca.bgcolor,orientation_id
                               FROM $sql_tbl[mk_style_customization_area] AS sa
							   INNER JOIN  $sql_tbl[mk_xcart_order_details_customization_rel] AS ca
							   ON sa.id = ca.area_id WHERE
							   ca.orderid='".$orderid."'
							   AND ca.itemid= '".$_GET['productId']."' and ca.area_id = '".$custrow['AreaId']."'
                               group by ca.orderid,ca.itemid,ca.orientation_id,ca.area_id,ca.templateid,ca.style_id ";
			 ############## added this code for photo album ##################### 
			 //$previewDetails = (!empty($custrow['page_no'])) ?  $previewDetails." and page_no ='$custrow[page_no]'" : $previewDetails;
			 //$previewDetails = (!empty($custrow['templateid'])) ?  $previewDetails." and templateid ='$custrow[templateid]'" : $previewDetails;
			
			 $sqllog->debug("Query  : ". $previewDetails." File Name : ".__FILE__."  Line No ".__LINE__);   
             $previewResult= db_query($previewDetails);
			   if(!empty($custrow['page_no'])){
				$smarty->assign("moreOrientation", "Y");
				$custTempArray = array();
				while($previewRow    = db_fetch_array($previewResult)){
					
			       	$custTempArray[] =  $previewRow;
			       	###$custAreaDetails[] =  $previewRow;
			       	list($width,$height)=getimagesize("../".$previewRow['orientation_final_image_display']);
           			$previewstRow['image_width']."*".$previewRow['image_height'];
					$zoom[] = number_format(($previewRow['image_width']*$previewRow['image_height'])/($width*$height)*100);
			    }
			    $custAreaDetails[] = $custTempArray;
			
			}else{
				// changed the code for multiple orientation
				$custTempArray = array();
				while($previewRow    = db_fetch_array($previewResult)){
					$ortRow = db_query("SELECT location_name FROM mk_customization_orientation WHERE id ='".$previewRow['orientation_id']."' ");
					$result = db_fetch_array($ortRow);
					$previewRow['orientation_name'] = $result['location_name'];
					
					
			       	$custTempArray[] =  $previewRow;
			       	list($width,$height)=getimagesize("../".$previewRow['orientation_final_image_display']);
           			$zoom[] = number_format(($previewRow['image_width']*$previewRow['image_height'])/($width*$height)*100);
					
					
			    }
			    $custAreaDetails[$custrow['AreaId']] = $custTempArray;
				
			    // previous code 
			    /*$previewRow    = db_fetch_array($previewResult);	
			    $custAreaDetails[] =  $previewRow;
				list($width,$height)=getimagesize("../".$previewRow['orientation_final_image_display']);
           		$previewRow['image_width']."*".$previewRow['image_height'];
				$zoom[] = number_format(($previewRow['image_width']*$previewRow['image_height'])/($width*$height)*100);*/
			}
			/*  end       */
		}
//	
		$smarty->assign("custAreaDetails", $custAreaDetails);
		$smarty->assign("zoom", $zoom);
		for($i=0; $i<count($custImage); $i++){
			$imagePath = explode("/", $custImage[$i]);
			$imageName[] = $imagePath[3];
		}
//         echo "<pre>";
// 	     print_r($custAreaDetails);
//		 exit;

		$smarty->assign("custAreaId", $custAreaId);
        $smarty->assign("custNames", $custName);
		$smarty->assign("custLabel", $custLabel);
		$smarty->assign("custImage", $custImage);
		$smarty->assign("imageName", $imageName);
		$smarty->assign("custPageNum", $custPageNo);
		
		$smarty->assign("templates", $templates); // templates if used 
		$smarty->assign("addons",$addons);
 
		if($optionName && $optionValue) {
		$smarty->assign("optionNames", $optionName);
		$smarty->assign("optionValues", $optionValue);
		} else {
			$notavailable = "No option available";
			$smarty->assign("noOption", $notavailable);
		}

		$smarty->assign("orderId", $orderid);
		$smarty->assign("productId", $_GET['productId']);






		if (!empty($tmp["orderid"]))
			$smarty->assign("orderid_prev", $tmp["orderid"]);
	}
}

$location[] = array(func_get_langvar_by_name("lbl_orders_management"), "orders.php");
$location[] = array(func_get_langvar_by_name("lbl_order_details_label"), "");


?>
