<?php
/*****************************************************************************\
+-----------------------------------------------------------------------------+
| X-Cart                                                                      |
| Copyright (c) 2001-2006 Ruslan R. Fazliev <rrf@rrf.ru>                      |
| All rights reserved.                                                        |
+-----------------------------------------------------------------------------+
| PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
| FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
| AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
|                                                                             |
| THIS  AGREEMENT  EXPRESSES  THE  TERMS  AND CONDITIONS ON WHICH YOU MAY USE |
| THIS SOFTWARE   PROGRAM   AND  ASSOCIATED  DOCUMENTATION   THAT  RUSLAN  R. |
| FAZLIEV (hereinafter  referred to as "THE AUTHOR") IS FURNISHING  OR MAKING |
| AVAILABLE TO YOU WITH  THIS  AGREEMENT  (COLLECTIVELY,  THE  "SOFTWARE").   |
| PLEASE   REVIEW   THE  TERMS  AND   CONDITIONS  OF  THIS  LICENSE AGREEMENT |
| CAREFULLY   BEFORE   INSTALLING   OR  USING  THE  SOFTWARE.  BY INSTALLING, |
| COPYING   OR   OTHERWISE   USING   THE   SOFTWARE,  YOU  AND  YOUR  COMPANY |
| (COLLECTIVELY,  "YOU")  ARE  ACCEPTING  AND AGREEING  TO  THE TERMS OF THIS |
| LICENSE   AGREEMENT.   IF  YOU    ARE  NOT  WILLING   TO  BE  BOUND BY THIS |
| AGREEMENT, DO  NOT INSTALL OR USE THE SOFTWARE.  VARIOUS   COPYRIGHTS   AND |
| OTHER   INTELLECTUAL   PROPERTY   RIGHTS    PROTECT   THE   SOFTWARE.  THIS |
| AGREEMENT IS A LICENSE AGREEMENT THAT GIVES  YOU  LIMITED  RIGHTS   TO  USE |
| THE  SOFTWARE   AND  NOT  AN  AGREEMENT  FOR SALE OR FOR  TRANSFER OF TITLE.|
| THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY GRANTED BY THIS AGREEMENT.      |
|                                                                             |
| The Initial Developer of the Original Code is Ruslan R. Fazliev             |
| Portions created by Ruslan R. Fazliev are Copyright (C) 2001-2006           |
| Ruslan R. Fazliev. All Rights Reserved.                                     |
+-----------------------------------------------------------------------------+
\*****************************************************************************/

#
# $Id: search.php,v 1.141.2.8 2006/06/06 05:56:52 svowl Exp $
#

if ( !defined('XCART_SESSION_START') ) { header("Location: ../"); die("Access denied"); }
$advanced_options = array("productcode", "productid", "provider", "price_max", "avail_max", "weight_max", "forsale", "flag_free_ship", "flag_ship_freight", "flag_global_disc", "flag_free_tax", "flag_min_amount", "flag_low_avail_limit", "flag_list_price", "flag_vat", "flag_gstpst", "manufacturers");

$sort_fields = array(
	"productcode" 	=> func_get_langvar_by_name("lbl_sku"),
	"title" 		=> func_get_langvar_by_name("lbl_product"),
    "price" 		=> func_get_langvar_by_name("lbl_price"),
    "qtysold"       => func_get_langvar_by_name("lbl_qtysold"),
    "status"        => func_get_langvar_by_name("lbl_status"),
    "style"			=> func_get_langvar_by_name("lbl_productstyle"),
	"orderby"		=> func_get_langvar_by_name("lbl_default"),
	"stats"         => "stats"

);

if ($config["Appearance"]["display_productcode_in_list"] != "Y")
	unset($sort_fields["productcode"]);

/*if($current_area == 'A' || $current_area == 'P') {
    $sort_fields["quantity"] = func_get_langvar_by_name("lbl_in_stock");
}*/

if (empty($search_data)) {
	$search_data = array();
}

if ($mode == "search") {

    if(isset($_GET['did']))
    {
    	 $designerid = $_GET['did'];
    }
	#
	# Perform search and display results
	#
	$data = array();

	$flag_save = false;

	#
	# Initialize service arrays
	#
	$fields = array();
	$fields_count = array();
	$from_tbls = array();
	$inner_joins = array();
	$left_joins = array();
	$where = array();
	$groupbys = array();
	$having = array();
	$orderbys = array();
	$special_chars = false;

	#
	# Prepare the search data
	#
	if (!empty($sort) && isset($sort_fields[$sort])) {
		# Store the sorting type in the session
		$search_data["products"]["sort_field"] = $sort;
		$flag_save = true;
	}

	if (isset($sort_direction)) {
		# Store the sorting direction in the session
		$search_data["products"]["sort_direction"] = $sort_direction;
		$flag_save = true;
	}





	if (!empty($page) && $search_data["products"]["page"] != intval($page)) {
		# Store the current page number in the session
		$search_data["products"]["page"] = $page;
		$flag_save = true;
	}

	if (is_array($search_data["products"])) {
		$data = $search_data["products"];
		foreach ($data as $k=>$v)
			if (!is_array($v) && !is_numeric($v))
				$data[$k] = addslashes($v);
	}
	#
	# Translate service data to inner service arrays
	#
	if (!empty($data['_'])) {
		foreach ($data['_'] as $saname => $sadata) {
			if (isset($$saname) && is_array($$saname) && empty($$saname))
				$$saname = $sadata;
		}
	}

	$sort_string = "";
	$membershipid_string = ($user_account['membershipid'] == 0) ? "= 0" : "IN ('$user_account[membershipid]', 0)";

	$fields[] = "$sql_tbl[products].*";
	$from_tbls[] = "pricing";
	$left_joins['quick_flags'] = array(
		"on" => "$sql_tbl[quick_flags].productid = $sql_tbl[products].productid"
	);
	$fields[] = "$sql_tbl[quick_flags].*";

	$inner_joins['quick_prices'] = array(
		"on" => "$sql_tbl[quick_prices].productid = $sql_tbl[products].productid AND $sql_tbl[quick_prices].membershipid $membershipid_string"
	);
	$where[] = "$sql_tbl[quick_prices].priceid = $sql_tbl[pricing].priceid";
	$fields[] = "$sql_tbl[quick_prices].variantid";
	if ($user_account['membershipid'] == 0) {
		$fields[] = "$sql_tbl[pricing].price";
	} else {
		$fields[] = "MIN($sql_tbl[pricing].price) as price";
	}
	

	$data["substring"] = trim($data["substring"]);


	$left_joins['products_categories'] = array(
		"on" => "$sql_tbl[products_categories].productid = $sql_tbl[products].productid"
	);
    $inner_joins['mk_product_type'] = array(
		"on" => "$sql_tbl[mk_product_type].id = $sql_tbl[products].product_type_id"
	);
     $inner_joins['mk_product_style'] = array(
		"on" => "$sql_tbl[mk_product_style].id = $sql_tbl[products].product_style_id"
	);
	$left_joins['categories'] = array(
		"on" => "$sql_tbl[products_categories].categoryid = $sql_tbl[categories].categoryid"
	);
	$left_joins['category_memberships'] = array(
		"on" => "$sql_tbl[category_memberships].categoryid = $sql_tbl[categories].categoryid",
		"parent" => "categories"
	);
	$left_joins['product_memberships'] = array(
		"on" => "$sql_tbl[product_memberships].productid = $sql_tbl[products].productid"
	);

	

/******************************************************/
/*Nikhil Gupta (07/04/07)                                                               */
/*Search by product status id														 */
/******************************************************/

		

		if($current_area == "A" )
		{
		    if ($data["designer"] != 0)
			{
			        $where[] = "$sql_tbl[products].designer = '".$data[designer]."'";
		    }
		    elseif(!empty($_GET['did']))
		    {
		     	$where[] = "$sql_tbl[products].designer = '".$_GET[did]."'";
		     	$smarty->assign("did", $_GET[did]);

		    }
		}

/******************************************************/


	# /if (!empty($data["categoryid"]))

	if (!empty($data["productcode"])) {
		$productcode_cond_string = empty($active_modules['Product_Options']) ? "$sql_tbl[products].productcode" : "IFNULL($sql_tbl[variants].productcode, $sql_tbl[products].productcode)";
		$where[] = "$productcode_cond_string LIKE '%".$data["productcode"]."%'";
	}

	if (!empty($data["productid"])) {
		$where[] = "$sql_tbl[products].productid ".(is_array($data["productid"]) ? " IN ('".implode("','", $data["productid"])."')": "= '".$data["productid"]."'");
	}

	if (!empty($data["provider"])) {
		if (is_array($data['provider']))
			$where[] = "$sql_tbl[products].provider IN ('".implode("','", $data['provider'])."')";
		else
			$where[] = "$sql_tbl[products].provider = '".$data["provider"]."'";
	}

	if (!empty($data["price_min"])) {
		$where[] = "$sql_tbl[mk_product_style].price >= '".$data["price_min"]."'";
	}

	if (strlen(@$data["price_max"]) > 0) {
		$where[] = "$sql_tbl[mk_product_style].price <= '".$data["price_max"]."'";
	}

	$avail_cond_string = empty($active_modules['Product_Options']) ? "$sql_tbl[products].avail" : "IFNULL($sql_tbl[variants].avail, $sql_tbl[products].avail)";
	if (!empty($data["avail_min"])) {
		$where[] = "$avail_cond_string >= '".$data["avail_min"]."'";
	}

	if (strlen(@$data["avail_max"]) > 0) {
		$where[] = "$avail_cond_string <= '".$data["avail_max"]."'";
	}

	$weight_cond_string = empty($active_modules['Product_Options']) ? "$sql_tbl[products].weight" : "IFNULL($sql_tbl[variants].weight, $sql_tbl[products].weight)";
	if (!empty($data["weight_min"])) {
		$where[] = "$weight_cond_string >= '".$data["weight_min"]."'";
	}

	if (strlen(@$data["weight_max"]) > 0) {
		$where[] = "$weight_cond_string <= '".$data["weight_max"]."'";
	}

	if (!empty($data["forsale"]))
		$where[] = "$sql_tbl[products].forsale = '".$data["forsale"]."'";

	if (!empty($data["flag_free_ship"]))
		$where[] = "$sql_tbl[products].free_shipping = '".$data["flag_free_ship"]."'";

	if (!empty($data["flag_ship_freight"]))
		$where[] = "$sql_tbl[products].shipping_freight = '".$data["flag_ship_freight"]."'";

	if (!empty($data["flag_ship_freight"])) {
		if ($data["flag_ship_freight"] == "Y")
			$where[] = "$sql_tbl[products].shipping_freight > 0";
		else
			$where[] = "$sql_tbl[products].shipping_freight = 0";
	}

	if (!empty($data["flag_global_disc"]))
		$where[] = "$sql_tbl[products].discount_avail = '".$data["flag_global_disc"]."'";

	if (!empty($data["flag_free_tax"]))
		$where[] = "$sql_tbl[products].free_tax = '".$data["flag_free_tax"]."'";

	if (!empty($data["flag_min_amount"])) {
		if ($data["flag_min_amount"] == "Y")
			$where[] = "$sql_tbl[products].min_amount != '1'";
		else
			$where[] = "$sql_tbl[products].min_amount = '1'";
	}

	if (!empty($data["flag_low_avail_limit"])) {
		if ($data["flag_low_avail_limit"] == "Y")
			$where[] = "$sql_tbl[products].low_avail_limit != '10'";
		else
			$where[] = "$sql_tbl[products].low_avail_limit = '10'";
	}

	if (!empty($data["flag_list_price"])) {
		if ($data["flag_list_price"] == "Y")
			$where[] = "$sql_tbl[products].list_price != '0'";
		else
			$where[] = "$sql_tbl[products].list_price = '0'";
	}

	if(!empty($active_modules['Product_Options'])) {
		$left_joins["variants"] = array(
			"on" => "$sql_tbl[variants].productid = $sql_tbl[products].productid AND $sql_tbl[quick_prices].variantid = $sql_tbl[variants].variantid",
			"parent" => "quick_prices"
		);
		foreach ($variant_properties as $property) {
			$fields[] = "IFNULL($sql_tbl[variants].$property, $sql_tbl[products].$property) as ".$property;
		}
	}


	if (!empty($data["sort_field"])) {
		# Sort the search results...

		$direction = ($data["sort_direction"] ? "DESC" : "ASC");
		switch ($data["sort_field"]) {
			case "productcode":
				$sort_string = "$sql_tbl[products].productcode $direction";
				break;
			case "title":
				$sort_string = "$sql_tbl[products].product $direction";
				break;
			case "orderby":
				$sort_string = "$sql_tbl[products_categories].orderby $direction";
				break;
			case "quantity":
				$sort_string = "$sql_tbl[products].avail $direction";
				break;
			case "qtysold":
				$sort_string = "$sql_tbl[products].sales_stats $direction";
				break;
			case "status":
				$sort_string = "$sql_tbl[products].statusid $direction";
				break;
			case "style":
				$sort_string = "$sql_tbl[mk_product_style].name $direction";
				break;
			case "price":
				if (!empty($active_modules["Special_Offers"]) && !empty($search_data["products"]["show_special_prices"])) {
					$sort_string = "x_special_price $direction, price $direction";
				}
				else {
					$sort_string = " $sql_tbl[products].list_price $direction";
				}
				break;
			case "stats" :
			    $sort_string = " $sql_tbl[products].sales_stats $direction";
			    break;
			default:
				$sort_string = "$sql_tbl[products].product";
		}
	}
	else {
		$sort_string = "$sql_tbl[products].product";
	}

	if(!empty($data['sort_condition'])) {
		$sort_string = $data['sort_condition'];
	}

	if (($current_area == "C" || $current_area == "B") && $config["General"]["disable_outofstock_products"] == "Y") {
		if (!empty($active_modules['Product_Options'])) {
			$where[] = "(IFNULL($sql_tbl[variants].avail, $sql_tbl[products].avail) > 0 OR $sql_tbl[products].product_type NOT IN ('','N'))";
		} else {
			$where[] = "($sql_tbl[products].avail > 0 OR $sql_tbl[products].product_type NOT IN ('','N')) AND ";
		}
	}

	$groupbys[] = "$sql_tbl[products].productid";
	$orderbys[] = $sort_string;
	$orderbys[] = "$sql_tbl[products].product ASC";

	#
	# Generate search query
	#
	foreach ($inner_joins as $j) {
		if (!empty($j['fields']) && is_array($j['fields']))
			$fields = func_array_merge($fields, $j['fields']);
	}
	foreach ($left_joins as $j) {
		if (!empty($j['fields']) && is_array($j['fields']))
			$fields = func_array_merge($fields, $j['fields']);
	}

	$fields_count[] = "COUNT($sql_tbl[products].productid)";
	$search_query = "SELECT ".implode(", ", $fields)." ,$sql_tbl[mk_product_type].name,$sql_tbl[mk_product_type].id as typeid,$sql_tbl[products].product_style_id as styleid,$sql_tbl[mk_product_style].name as stylename,$sql_tbl[mk_product_style].price as styleprice FROM ";

	$search_query_count = "SELECT ".implode(", ", $fields_count)." FROM ";
	if (!empty($from_tbls)) {
		foreach ($from_tbls as $k => $v) {
			$from_tbls[$k] = $sql_tbl[$v];
		}
		$search_query .= implode(", ", $from_tbls).", ";
		$search_query_count .= implode(", ", $from_tbls).", ";
	}
	$search_query .= $sql_tbl['products'];
     $search_query_count .= $sql_tbl['products'];


	foreach ($left_joins as $ljname => $lj) {
		if (!empty($lj['parent']))
			continue;
		$tmp = " LEFT JOIN ";

		if (!empty($lj['tblname'])) {
			$tmp .= $sql_tbl[$lj['tblname']]." as ".$ljname;
		} else {
			$tmp .= $sql_tbl[$ljname];
		}
		$tmp .= " ON ".$lj['on'];
		if (!isset($lj['only_select']))
			$search_query_count .= $tmp;
		$search_query .= $tmp;
	}

	foreach ($inner_joins as $ijname => $ij) {
		$tmp = " INNER JOIN ";
		if (!empty($ij['tblname'])) {
			$tmp .= $sql_tbl[$ij['tblname']]." as ".$ijname;
		} else {
			$tmp .= $sql_tbl[$ijname];
		}
		$tmp .= " ON ".$ij['on'];
		$search_query_count .= $tmp;
		$search_query .= $tmp;
		foreach ($left_joins as $ljname => $lj) {
			if ($lj['parent'] != $ijname)
				continue;
			$tmp = " LEFT JOIN ";
			if (!empty($lj['tblname'])) {
				$tmp .= $sql_tbl[$lj['tblname']]." as ".$ljname;
			} else {
				$tmp .= $sql_tbl[$ljname];
			}
			$tmp .= " ON ".$lj['on'];
			if (!isset($lj['only_select']))
				$search_query_count .= $tmp;
			$search_query .= $tmp;
		}
	}

// loop for outer join


	/*foreach ($outer_joins as $ljname => $lj) {
		if (!empty($lj['parent']))
			  continue;
		$tmp = " RIGHT OUTER JOIN ";

		if (!empty($lj['tblname'])) {
			$tmp .= $sql_tbl[$lj['tblname']]." as ".$ljname;
		} else {
			$tmp .= $sql_tbl[$ljname];
		}
		$tmp .= " ON ".$lj['on'];
		if (!isset($lj['only_select']))
			$search_query_count .= $tmp;
		$search_query .= $tmp;
	}*/


	$search_query .= " WHERE ".implode(" AND ", $where);


	$search_query_count .= " WHERE ".implode(" AND ", $where);
	if (!empty($groupbys)) {
		$search_query .= " GROUP BY ".implode(", ", $groupbys);
		$search_query_count .= " GROUP BY ".implode(", ", $groupbys);
	}
	if (!empty($having)) {
		$search_query .= " HAVING ".implode(" AND ", $having);
		$search_query_count .= " HAVING ".implode(" AND ", $having);
	}
	if (!empty($orderbys)) {
		$search_query .= " ORDER BY ".implode(", ", $orderbys);
		$search_query_count .= " ORDER BY ".implode(", ", $orderbys);
	}
 
      
	#
	# Calculate the number of rows in the search results
	#
	db_query("SET OPTION SQL_BIG_SELECTS=1");
	$_res = db_query($search_query_count);

	$total_items = db_num_rows($_res);
	db_free_result($_res);

   //echo($search_query);
	if ($total_items > 0) {
		$page = $search_data["products"]["page"];
		//print_r($search_data["products"]);
		#
		# Prepare the page navigation
		#
		if (!isset($objects_per_page)) {
			if ($current_area == "C" || $current_area == "B")
				$objects_per_page = $config["Appearance"]["products_per_page"];
			else
				$objects_per_page = $config["Appearance"]["products_per_page_admin"];
		}

		$total_nav_pages = ceil($total_items/$objects_per_page)+1;

		include $xcart_dir."/include/navigation.php";

		#
		# Perform the SQL query and getting the search results
		#
		if (!empty($data["is_modify"])) {
			#
			# Get the products and go to modify them
			#
			$res = db_query($search_query);
			if ($res) {
				$geid = false;
				$productid = false;
				x_load("product");
				while ($pid = db_fetch_row($res)) {
					if (empty($productid))
						$productid = $pid[0];
					$geid = func_ge_add($pid[0], $geid);
				}
				func_header_location("product_modify.php?productid=$productid&geid=".$geid);
			}

		}
		elseif ($data["is_export"] == "Y" || $export == 'export_found') {

			x_load("export");
			# Save the SQL query and go to export them
			func_export_range_save("PRODUCTS", $search_query);
			$top_message['content'] = func_get_langvar_by_name("lbl_export_products_add");
			$top_message['type'] = 'I';
			func_header_location("import.php?mode=export");

		}
		else {
			$search_query .= " LIMIT $first_page, $objects_per_page";
			//echo($search_query);
			//exit;

			$products = func_query($search_query);

            


		}


		# Clear service arrays
		unset($fields, $fields_count, $from_tbls, $inner_joins, $left_joins, $where, $groupbys, $having, $orderbys);


		if (isset($products_ef))
			unset($products_ef);

		if (isset($options_markups))
			unset($options_markups);

		
		# Assign the Smarty variables
		if($current_area == 'A')
		{
			$smarty->assign("navigation_script","search_designer.php?mode=search&did=$_GET[did]");
		}


        //setting mksearch

		//Access popup image path

	   
		$smarty->assign("products", $products);
		$smarty->assign("first_item", $first_page+1);
		$smarty->assign("last_item", min($first_page+$objects_per_page, $total_items));
	}

	$smarty->assign("total_items",$total_items);
	$smarty->assign("mode", $mode);

	if ($flag_save)
		x_session_save("search_data");
}




//print_r($search_data["products"]);
$smarty->assign("search_prefilled", $search_data["products"]);

$designers = db_query("SELECT  c.firstname  as f, c.lastname as l FROM  $sql_tbl[mk_designer] 
as d INNER JOIN $sql_tbl[customers] as c ON c.login = d.customerid  WHERE d.id='".$designerid."'");
$rowDesigner = db_fetch_array($designers);
$smarty->assign("designerName", 'All designs by '.$rowDesigner['f']);




$smarty->assign("search_categories", $search_categories);
unset($search_categories);

$smarty->assign("sort_fields", $sort_fields);
$smarty->assign("main","search_designer");

?>