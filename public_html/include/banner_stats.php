<?php
/*****************************************************************************\
+-----------------------------------------------------------------------------+
| X-Cart                                                                      |
| Copyright (c) 2001-2006 Ruslan R. Fazliev <rrf@rrf.ru>                      |
| All rights reserved.                                                        |
+-----------------------------------------------------------------------------+
| PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
| FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
| AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
|                                                                             |
| THIS  AGREEMENT  EXPRESSES  THE  TERMS  AND CONDITIONS ON WHICH YOU MAY USE |
| THIS SOFTWARE   PROGRAM   AND  ASSOCIATED  DOCUMENTATION   THAT  RUSLAN  R. |
| FAZLIEV (hereinafter  referred to as "THE AUTHOR") IS FURNISHING  OR MAKING |
| AVAILABLE TO YOU WITH  THIS  AGREEMENT  (COLLECTIVELY,  THE  "SOFTWARE").   |
| PLEASE   REVIEW   THE  TERMS  AND   CONDITIONS  OF  THIS  LICENSE AGREEMENT |
| CAREFULLY   BEFORE   INSTALLING   OR  USING  THE  SOFTWARE.  BY INSTALLING, |
| COPYING   OR   OTHERWISE   USING   THE   SOFTWARE,  YOU  AND  YOUR  COMPANY |
| (COLLECTIVELY,  "YOU")  ARE  ACCEPTING  AND AGREEING  TO  THE TERMS OF THIS |
| LICENSE   AGREEMENT.   IF  YOU    ARE  NOT  WILLING   TO  BE  BOUND BY THIS |
| AGREEMENT, DO  NOT INSTALL OR USE THE SOFTWARE.  VARIOUS   COPYRIGHTS   AND |
| OTHER   INTELLECTUAL   PROPERTY   RIGHTS    PROTECT   THE   SOFTWARE.  THIS |
| AGREEMENT IS A LICENSE AGREEMENT THAT GIVES  YOU  LIMITED  RIGHTS   TO  USE |
| THE  SOFTWARE   AND  NOT  AN  AGREEMENT  FOR SALE OR FOR  TRANSFER OF TITLE.|
| THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY GRANTED BY THIS AGREEMENT.      |
|                                                                             |
| The Initial Developer of the Original Code is Ruslan R. Fazliev             |
| Portions created by Ruslan R. Fazliev are Copyright (C) 2001-2006           |
| Ruslan R. Fazliev. All Rights Reserved.                                     |
+-----------------------------------------------------------------------------+
\*****************************************************************************/

#
# $Id: banner_stats.php,v 1.17 2006/02/16 08:34:40 max Exp $
# 
# Display banners statistics
#

if ( !defined('XCART_SESSION_START') ) { header("Location: ../"); die("Access denied"); }

if ($StartDay)
	$search['start_date'] = mktime(0, 0, 0, $StartMonth, $StartDay, $StartYear);

if ($EndDay)
	$search['end_date'] = mktime(23, 59, 59, $EndMonth, $EndDay, $EndYear);

if ($search) {
	if($current_area == "B")
		$search['partner'] = $login;
	$where = array();

	if ($search['start_date'] && $search['end_date'])
		$where[] = "$search[end_date] > add_date AND add_date > $search[start_date]";

	if ($search['partner'])
		$where[] = "login = '$search[partner]'";

	if ($where)
		$where_condition = " AND ".implode(" AND ", $where);

	$views = func_query("SELECT bannerid, productid, class, COUNT(*) as views FROM $sql_tbl[partner_views] WHERE 1 $where_condition GROUP BY bannerid, productid, class");

	if ($views) {
		$banners = array();
		$total = array();
		foreach ($views as $k => $v) {
			$banners[$k]['bannerid'] = $v['bannerid'];
			if ($v['bannerid'])
				$banners[$k]['banner'] = func_query_first_cell("SELECT banner FROM $sql_tbl[partner_banners] WHERE bannerid = '$v[bannerid]'");

			$banners[$k]['views'] = $v['views'];
			$banners[$k]['class'] = $v['class'];
			$banners[$k]['productid'] = $v['productid'];
			$banners[$k]['clicks'] = func_query_first_cell("SELECT COUNT(*) FROM $sql_tbl[partner_clicks] WHERE bannerid = '$v[bannerid]' AND class = '$v[class]' AND productid = '$v[productid]' $where_condition");

			if ($v['productid'])
				$banners[$k]['product'] = func_query_first_cell("SELECT product FROM $sql_tbl[products] WHERE productid = '$v[productid]'");

			$banners[$k]['click_rate'] = round($banners[$k]['clicks']/$banners[$k]['views'], 2);
			$total['clicks'] += $banners[$k]['clicks'];
			$total['views'] += $banners[$k]['views'];
		}

		$total['click_rate'] = round($total['clicks']/$total['views'], 2);
		$smarty->assign ("banners", $banners);
		$smarty->assign ("total", $total);
	}
}

$smarty->assign ("partners", func_query("SELECT * FROM $sql_tbl[customers] WHERE usertype = 'B' AND status = 'Y'"));

$smarty->assign ("search", $search);
$smarty->assign ("month_begin", mktime(0,0,0,date('m'),1,date('Y')));

?>
