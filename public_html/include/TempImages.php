<?php

/* Code for dealing with Temp Images*/

if (!defined('XCART_START'))
{

   header ("Location: ../");
   die ("Access denied");
}

/*Implementation of the method to add temporary images to a DB based clean up list 
 * This cleanup list is cleared up by a backend cron.
 */
function addToTempImagesList($image){
	  global $sql_tbl, $XCARTSESSID;

	  $temp_images_table=$sql_tbl[temp_images];

	  db_query("insert into $temp_images_table(imagepath,sessid) values ('$image','$XCARTSESSID')");
}


//
// This class can be used as a singleton to keep adding to the temp_images_list
// in memory and persist the list to the database in the destructor of the class
// once per php instead of once per image creation.
//
/*
class TempImages{

	private $_cleanupList;

	private function addToListHelper($image){
		$this->_cleanupList[]=$image;
	}

	private function __construct(){
		$this->_cleanupList=array();
	}

    // Persist the list onto the database when the destructor is called 
	//
	public function __destruct(){
		
		global $sql_tbl, $XCARTSESSID;
	    $temp_images_table=$sql_tbl[temp_images];

		foreach($this->_cleanupList as $image){
        	  db_query("insert into $temp_images_table(imagepath,sessid) values ('$image','$XCARTSESSID')");
		}
		$this->cleanupList=array();
	}

	// This is a singleton method that keeps a single instance.
	// Once the php program that is called ends, the destructor
	// of this instance is called and the internal list is persisted in the database.
	//
	// The advantage of using a singleton is that the list is written to the database once.
	// At the end of every program instead of every temporary image being written
	// to the database.
	//
	public static function addToList($image){

	  static $singleton_instance=null;
	  if($singleton_instance==null){
		  $singleton_instance=new TempImages();
	  }

	  $singleton_instance->addToListHelper($image);

	}
}
 */

?>
