<?php
require_once("func/func.itemsearch.php");

if (!defined('XCART_SESSION_START')) {
    header("Location: ../");
    die("Access denied");
}


$search = new solrProducts();
$queryBuilder = new solrQueryBuilder();

$search_prefilled = null;
$session_search_data = null;

if ($REQUEST_METHOD == "POST") {

    // Fix up the date
    $stdate = explode("/", $startdate);
    $posted_data["start_date"] = mktime(0, 0, 0, $stdate[0], $stdate[1], $stdate[2]);
    $enddate = explode("/", $enddate);
    $posted_data["end_date"] = mktime(23, 59, 59, $enddate[0], $enddate[1], $enddate[2]);

    // Put it all in session, for successive gets ..
    $search_data = $posted_data;
    $session_search_data = $search_data;
} else {
    $mode = get_var('mode');
    if ($mode != null) {
        $session_search_data = $search_data;
    }
}


// Check if category was searched ..
$category = $session_search_data["categoryid"];
if (!empty($category)) {
    $queryBuilder->add_filter("main_category", $category);
    // Check if we need to look in any sub categories ..
    $sub_categories = $session_search_data['search_in_subcategories'];
    if ($sub_categories) {
        $category = func_select_first($sql_tbl["categories"], array('categoryid' => $category));
        $categories = func_query_column("SELECT category FROM $sql_tbl[categories] WHERE categoryid='" . $data["categoryid"] . "' OR categoryid_path LIKE '{$category['categoryid_path']}/%'");
        foreach ($categories as $c) {
            $queryBuilder->add_phrase($c, array('categories'));
        }
    }
}

// What was search ?
$search_phrase = $session_search_data['substring'];

if (!empty($search_phrase)) {
    $fields = array();
    // What fields the user wants us to search on ..
    if ($session_search_data['by_title'] == 'on') {
        $fields [] = 'product';
    }

    if ($session_search_data['by_shortdescr'] == 'on') {
        $fields [] = 'descr';
    }

    if ($session_search_data['by_fulldescr'] == 'on') {
        $fields [] = 'fulldescr';
    }

    if ($session_search_data['by_keywords'] == 'on') {
        $fields [] = 'keywords';
    }

    $queryBuilder->add_term($search_phrase, $fields);
}

// What kind of product status is user looking for ?
$product_status = $session_search_data['productStatus'];

if (!empty($product_status)) {
    $queryBuilder->add_filter("statusid", $product_status);
}

// Filter based on product rating
$product_rating = $session_search_data['productRating'];
if (isset($product_rating) && strlen(trim($product_rating)) > 0) {
    $queryBuilder->add_filter("myntrarating", "($product_rating)");
}

// Is the search on a designer??
$designer = $session_search_data['designer'];

if (!empty($designer)) {
    $queryBuilder->add_filter("designer", $designer);
}

// Is the search on a designer??
$designer = $session_search_data['productid'];

if (!empty($designer)) {
    $queryBuilder->add_filter("productid", $designer);
}

// Was it a date search ? 
$start = $session_search_data['start_date'];
$end = $session_search_data['end_date'];
if (!empty($start) && !empty($end)) {
    $queryBuilder->add_range_filter("add_date", date('Y-m-d\TH:i:s\Z', $start), date('Y-m-d\TH:i:s\Z', $end));
} else if (!empty($start)) {
    $queryBuilder->add_range_filter("add_date", date('Y-m-d\TH:i:s\Z', $start), "*");
}

// If any search criteria was added
// the we put a default shop master filter ..
// We do this because all the products are available in shop master 0 (myntra)
if ($queryBuilder->query_added()) {
    $queryBuilder->add_filter('shopmasterid', 0);
    $queryBuilder->add_filter("generic_type", "design"); 
    $query = $queryBuilder->get_query();
}


if (!empty($query)) {

    //echo $query;

    $page = get_var('page', 1);
    $search_data['sort_field'] = get_var('sort_field', 'add_date');
    $search_data['sort_direction'] = get_var('sort_direction', 1);
    $limit = 30;
    $offset = ($page - 1) * $limit;
     
	if (empty($search_data['sort_field'])) {
        	$result = $search->searchIndex($query, $offset, $limit);
	} else {
        	$result = $search->searchAndSort($query, $offset, $limit, $search_data['sort_field'] . ' ' . ($search_data['sort_direction']  == 0 ? 'asc' : 'desc'));         }
    
	if (get_var('export', false) == 'export_found') {
       		$result = $search->searchIndex($query, 0, $result->numFound);
	}

    $products = documents_to_array($result->docs);
    $smarty->assign("arr_rating", array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11));
    if ($products) {
        $changed = array();
        foreach ($products as $p) {
            $p['image_portal_t'] = str_replace('./', '/', $p['image_portal_t']);
            $p['myntra_rating'] = $p['myntrarating'];
            $changed [] = $p;
        }
        $products = $changed;
    }

    if (get_var('export', false) == 'export_found') {
    	$file_name_suffix = time();
	$product_report = fopen($xcart_dir."/admin/product_search_report/product_search_report_$file_name_suffix.csv","a");
	$summary_table = "SKU, Product, Product Style, QTY SOLD, Status, Myntra Rating, ADD Date";
    	fwrite($product_report, $summary_table);
    	fwrite($product_report, "\r\n");
	
    	$string = "";
	if ($products) {
		foreach ($products as $p) {
			$string .= $p['sku'] . "" . $p['product'] . "," . $p['style'] . "," . $p['quantity_sold'] . "," . $p['statusid'] . "," . $p['myntra_rating'] . "," . $p['add_date'];
			$string .= "\r\n";
		}
	}

	fwrite($product_report, $string);
        fwrite($product_report, "\r\n");

	$filename = "product_search_report_$file_name_suffix.csv";
        $dir = "../admin/product_search_report/";
        force_download($filename, $dir);
	exit;

    }

    $total_nav_pages = ceil($result->numFound/ $limit) +1; 
    $smarty->assign("products", $products);
    $smarty->assign("total_items", $result->numFound);
    $smarty->assign("first_item", $offset + 1);
    $smarty->assign("last_item", $offset + $limit);

    // this builds the navigation based on $total_nav_pages ..
    include $xcart_dir."/include/navigation.php";
        
    $smarty->assign("navigation_script", "search.php?mode=search" . (!empty($search_data['sort_field']) ? '&sort=' . $search_data['sort_field'] . '&sort_direction=' . $search_data['sort_direction'] : ''));
    $smarty->assign("mode", "search");
}

$smarty->assign("search_prefilled", $search_data);  


x_session_save("search_data");


if ($current_area != 'C') {
    include $xcart_dir . "/include/categories.php";
    $designers = func_query("SELECT d.id , c.firstname  as f, c.lastname as l FROM  $sql_tbl[mk_designer] as d INNER JOIN $sql_tbl[customers] as c ON c.login = d.customerid  ORDER BY  c.firstname");
    $smarty->assign("designers", $designers);
	$ratings = func_query_column("SELECT DISTINCT myntra_rating from $sql_tbl[products] where myntra_rating >= 0 ORDER BY myntra_rating");
    $smarty->assign("avail_arr_rating", $ratings);
}

$search_categories = $smarty->get_template_vars("allcategories");
if ($current_area == "C" && !empty($active_modules["Fancy_Categories"])) {
    if (!function_exists("func_categories_sort_abc")) {
        function func_categories_sort_abc($a, $b) {
            return strcmp($a["category_path"], $b["category_path"]);
        }
    }

    usort($search_categories, "func_categories_sort_abc");
}

$smarty->assign("search_categories", $search_categories);
$smarty->assign("main", "search");

?>
