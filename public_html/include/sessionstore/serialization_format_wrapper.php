<?php
/**
  The following three methods take care of the 
  new encoding format (JSON) for session data 
  as a wrapper
*/
function x_session_is_JSON($sessid) {
    return strlen($sessid)>32 && !strncmp($sessid, 'JJ', 2);
}


function x_session_serialize($sessid, $sess_data) {
    if (x_session_is_JSON($sessid)) {
       return json_encode($sess_data);
    }
    return serialize($sess_data);
}

function x_session_unserialize($sessid, $sess_data) {
    if (x_session_is_JSON($sessid)) {
       return json_decode($sess_data, true);
    }
    return unserialize($sess_data);
}

function x_session_unserialize_by_type($isJsonSession, $sess_data) {
	if ($isJsonSession == true) {
		return json_decode($sess_data, true);
	}
	return unserialize($sess_data);
}
?>
