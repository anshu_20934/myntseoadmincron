<?php
require_once ($xcart_dir.'/logger.php');
require_once($xcart_dir.'/include/class/widget/class.widget.keyvaluepair.php');
require_once($xcart_dir.'/include/class/class.feature_gate.keyvaluepair.php');
require_once($xcart_dir.'/include/sessionstore/rest_request_client.php');
require_once($xcart_dir.'/include/sessionstore/session_store_client_exception.php');

include_once($xcart_dir."/Profiler/Profiler.php");


/**
 * Session Store Client to perform REST calls to Session Store Service
 *
 */
class SessionStoreClient{

    /* Session Store Service host ip is obtained through FeatureGate */
    //var $SERVICE_HOST_IP = "SessionServiceLB-559108960.ap-southeast-1.elb.amazonaws.com";
    //var $SERVICE_HOST_IP = "127.0.0.1";
    var $SERVICE_URL = "http://localhost:8080/SessionStoreService";
    var $SERVICE_PATH = "rest";
    var $RESOURCE_PATH = "sessions";
    var $SERVICE_HOST_KEY = "sessionService.Host.V2";
    
    
    /**
     * Helper function to prepare service url
     * @return string
     */
    function prepareServiceUrl() {
    	$serviceHostIps = FeatureGateKeyValuePairs::getStrArray($this->SERVICE_HOST_KEY, array("127.0.0.1:8080"));
    	$start_index = strpos($this->SERVICE_URL, "localhost");
    	$str_length = strlen("localhost:8080");
    	$rest_uri = substr_replace($this->SERVICE_URL, $serviceHostIps[0], $start_index, $str_length);
    	$rest_uri_path = "$rest_uri/$this->SERVICE_PATH/$this->RESOURCE_PATH";
    	return $rest_uri_path;
    }
    
    /**
     * GET function
     * @param unknown_type $resource_id
     */
    function performGetCall($resource_id){
    	$handle = Profiler::startTiming("session_store_get_call");
        global $sessionlog;
        $rest_uri = $this->prepareServiceUrl();
        $rest_uri_complete = "$rest_uri/$resource_id";
        $rest = new SessionRestRequest($rest_uri_complete, 'GET');
        $starttime = microtime(true);
        try {
            $rest->execute();
            $sessionlog->debug("Session Service GET call for id : $resource_id : time msec: "
                                .(round((microtime(true) - $starttime)*1000)));
            $response = $rest->getResponseBody();
            $responseInfo = $rest->getResponseInfo();
            $responseCode = $responseInfo['http_code'];
            $curlErrNo = $rest->getCurlErrNo();
            if (!$this->exceptionOccured($response, $responseCode, $curlErrNo)) {
            	Profiler::endTiming($handle);
                return $this->resurrectSession($response);
            } else {
                throw new SessionStoreClientException("Exception during GET : ".$response);
            }
        } catch (InvalidArgumentException $e) {
            $errMsg = "Exception during GET : invalid call arguments error : ".($e->getMessage());
            Profiler::endTiming($handle);
            throw new SessionStoreClientException($errMsg);
        } catch (SessionStoreClientException $e) {
        	Profiler::endTiming($handle);
            throw $e;
        } catch (Exception $e) {
            $errMsg = "Exception during GET : error : ".($e->getMessage());
            Profiler::endTiming($handle);
            throw new SessionStoreClientException($errMsg);
        }
    }

    /**
     * Helper function to resurrect session from xml
     * @param associative array
     */
    function resurrectSession($xmlRepresentation){
        $xmlparser = xml_parser_create();
        if (!xml_parse_into_struct($xmlparser, $xmlRepresentation, $values)) {
           $errMsg = sprintf("\nSession XML Parsing Error: %s at line %d",
                                     xml_error_string(xml_get_error_code($xmlparser)),
                                     xml_get_current_line_number($xmlparser));
           throw new SessionStoreClientException($errMsg);
        }
        xml_parser_free($xmlparser);
        return $values;
    }

    /**
     * PUT function
     * @param associative array $session
     */
    function performPutCall($session){
    	$handle = Profiler::startTiming("session_store_set_call");
        global $sessionlog;
        $sessionXml = $this->prepareXMLRepresentation($session);
        $session_id = $session["id"];
        $rest_uri = $this->prepareServiceUrl();
        $rest_uri_complete = "$rest_uri/$session_id";
        $rest = new SessionRestRequest($rest_uri_complete, 'PUT', $sessionXml);
        $starttime = microtime(true);
        try {
            $rest->execute();
            $sessionlog->debug("Session Service PUT call for id : $session_id : time msec: "
                                    .(round((microtime(true) - $starttime)*1000)));
            $response = $rest->getResponseBody();
            $responseInfo = $rest->getResponseInfo();
            $responseCode = $responseInfo['http_code'];
            $curlErrNo = $rest->getCurlErrNo();
            if (!$this->exceptionOccured($response, $responseCode, $curlErrNo)) {
            	Profiler::endTiming($handle);
                return $response;
            } else {
                throw new SessionStoreClientException("Exception during PUT : ".$response);
            }
        } catch (InvalidArgumentException $e) {
            $errMsg = "Exception during PUT : invalid call arguments error : ".($e->getMessage());
            Profiler::endTiming($handle);
            throw new SessionStoreClientException($errMsg);
        } catch (SessionStoreClientException $e) {
        	Profiler::endTiming($handle);
            throw $e;
        } catch (Exception $e) {
            $errMsg = "Exception during PUT : error : ".($e->getMessage());
            Profiler::endTiming($handle);
            throw new SessionStoreClientException($errMsg);
        }
    }

    /**
     * POST function
     * @param associative array $session$responseInfo
     */
    function performPostCall($session){
    	$handle = Profiler::startTiming("session_store_set_call");
        global $sessionlog;
        $sessionXml = $this->prepareXMLRepresentation($session);
        $session_id = $session["id"];
        $rest_uri = $this->prepareServiceUrl();
        $rest_uri_complete = "$rest_uri/$session_id";
        $rest = new SessionRestRequest($rest_uri_complete, 'POST', $sessionXml);
        $starttime = microtime(true);
        try {
            $rest->execute();
            $sessionlog->debug("Session Service POST call for id : $session_id : time msec: "
                                    .(round((microtime(true) - $starttime)*1000)));
            $response = $rest->getResponseBody();
            $responseInfo = $rest->getResponseInfo();
            $responseCode = $responseInfo['http_code'];
            $curlErrNo = $rest->getCurlErrNo();
            if (!$this->exceptionOccured($response, $responseCode, $curlErrNo)) {
            	Profiler::endTiming($handle);
                return $response;
            } else {
                throw new SessionStoreClientException("Exception during POST: ".$response);
            }
        } catch (InvalidArgumentException $e) {
            $errMsg = "Exception during POST : invalid call arguments error : ".($e->getMessage());
            Profiler::endTiming($handle);
            throw new SessionStoreClientException($errMsg);
        } catch (SessionStoreClientException $e) {
        	Profiler::endTiming($handle);
            throw $e;
        } catch (Exception $e) {
            $errMsg = "Exception during POST : error : ".($e->getMessage());
            Profiler::endTiming($handle);
            throw new SessionStoreClientException($errMsg);
        }
    }

    /*
     * Valid xml representation
     * <session>
     *  <id>05864142cb8b3bd72ad6e6c3c304c06e</id>
     *  <creation_date>1319093358</creation_date>
     *  <last_accessed_date>1319093358</last_accessed_date>
     *  <last_modified_date>1319093358</last_modified_date>
     *  <expiry_date>1319103358</expiry_date>
     *  <updated_flag>Y</updated_flag>
     *  <data><![CDATA[s:8:"is_robot";s:1:"N";]]></data>
     * </session>
     */
    /**
     * Helper function to prepare xml representation
     * @param associative array $session
     * @throws InvalidSessionException
     * @return string
     */
    function prepareXMLRepresentation($session){
        global $sessionlog;
        $xmlRepresentation = "<session>";
        if (array_key_exists("id", $session) && ($val = $session["id"]) != null) {
            $xmlRepresentation = "$xmlRepresentation<id>$val</id>";
        } else {
            throw new SessionStoreClientException("Session does not contain primary key 'id'");
        }

        if (array_key_exists("creationDate", $session) && ($val = $session["creationDate"]) != null) {
            $xmlRepresentation = "$xmlRepresentation<creation_date>$val</creation_date>";
        }
        if (array_key_exists("lastAccessedDate", $session) && ($val = $session["lastAccessedDate"]) != null) {
            $xmlRepresentation = "$xmlRepresentation<last_accessed_date>$val</last_accessed_date>";
        }
        if (array_key_exists("lastModifiedDate", $session) && ($val = $session["lastModifiedDate"]) != null) {
            $xmlRepresentation = "$xmlRepresentation<last_modified_date>$val</last_modified_date>";
        }
        if (array_key_exists("expiryDate", $session) && ($val = $session["expiryDate"]) != null) {
            $xmlRepresentation = "$xmlRepresentation<expiry_date>$val</expiry_date>";
        }
        if (array_key_exists("data", $session) && ($val = $session["data"]) != null) {
            $xmlRepresentation = "$xmlRepresentation<data><![CDATA[$val]]></data>";
        }
        if (array_key_exists("updatedFlag", $session) && ($val = $session["updatedFlag"]) != null) {
            $xmlRepresentation = "$xmlRepresentation<updated_flag>$val</updated_flag>";
        }
        $xmlRepresentation = "$xmlRepresentation</session>";
        return $xmlRepresentation;
    }

    /**
     * Helper function for checking if exception has occured
     * @param $response, $responseInfo, $curlErrNo
     * @return boolean
     */
    function exceptionOccured($response, $responseCode, $curlErrNo){
        if  ($response === false || $curlErrNo != 0) {
            /* some special cases of exceptions when service is not running or curl fails */
            $errMsg = "Exception due to misconfiguration of service response : $response : curl error : $curlErrNo ";
            throw new SessionStoreClientException($errMsg);
        } else {
            $exceptionOccurred = false;
            switch ($responseCode) {
                case '200':
                case '201':
                case '202':
                    $exceptionOccurred = false;
                    break;
                default: /* if any other http response code */
                    $exceptionOccurred = true;
            }
            return $exceptionOccurred;
        }
    }
}
?>
