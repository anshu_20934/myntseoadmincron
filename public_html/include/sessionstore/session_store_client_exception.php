<?php
/**
 * Generic exception for php REST client
 *
 */
class SessionStoreClientException extends Exception {
    public function __construct($message = null, $code = 0)
    {
        if (!$message) {
            throw new $this('Unknown '. get_class($this));
        }
        parent::__construct($message, $code);
    }

    public function __toString()
    {
        return get_class($this) . " '{$this->message}'\n"
        . "{$this->getTraceAsString()}";
    }
}
?>