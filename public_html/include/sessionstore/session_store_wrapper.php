<?php
require_once($xcart_dir.'/logger.php');
require_once($xcart_dir.'/include/sessionstore/session_store_client.php');
require_once($xcart_dir.'/include/class/widget/class.widget.keyvaluepair.php');
require_once($xcart_dir.'/include/class/class.feature_gate.keyvaluepair.php');

/**
 * Helper function
 * @param associative array $input
 * @return associative array
 */
function prepareArray($input) {
    $output = array();
    foreach($input as $key => $value) {
        switch ($key) {
            case "id":
                $output["sessid"] = $value;
                break;
            case "creationDate":
                $output["start"] = $value;
                break;
            case "expiryDate":
                $output["expiry"] = $value;
                break;
            case "lastAccessedDate":
                $output["lastaccesstime"] = $value;
                break;
            case "updatedFlag":
                $output["updated"] = $value;
                break;
            case "data":
                $output["data"] = $value;
                break;
        }
    }
    return $output;
}

/**
 * Function maps xml tags from Session Store with legacy db column names
 * @param associative array $input
 * @return associative array
 */
function prepareArrayFromXML($input) {
    $output = array();
    foreach($input as $key => $value) {
        $tag = $value['tag'];
        $tagval = null;
        /* some node might not have value */
        if (array_key_exists("value", $value)){
            $tagval = $value['value'];
        }

        switch (strtolower($tag)) {
            case "id":
                $output["sessid"] = $tagval;
                break;
            case "creation_date":
                $output["start"] = $tagval;
                break;
            case "expiry_date":
                $output["expiry"] = $tagval;
                break;
            case "last_accessed_date":
                $output["lastaccesstime"] = $tagval;
                break;
            case "updated_flag":
                $output["updated"] = $tagval;
                break;
            case "data":
                $output["data"] = $tagval;
                break;
        }
    }
    return $output;
}

/**
 * Get session by session id
 * @param string $sessionId
 * @return associative array of session data
 */
function getSessionBySessionId($sessionId){
    global $sessionlog, $errorlog;
    $enableSessionStore = FeatureGateKeyValuePairs::getBoolean('enableSessionStoreService', false);
    $fallback = false;
    if ($enableSessionStore) {
        $client = new SessionStoreClient();
        $response = null;
        try {
            $response = $client->performGetCall($sessionId);
            $sessionlog->debug("Session Service successful GET (session data) for id : ".$sessionId); 
            return prepareArrayFromXML($response);
        } catch (SessionStoreClientException $e) {
            $errMsg = $e->getMessage();
            if (stripos($errMsg, "does not exist", 0) !== false) {
               $sessionlog->error("Session Service trying to GET non-existing for id: $sessionId error: ".$errMsg);
               // GT : Commenting out errorlog as it is creating unnecessary noise in prod error log increasing its size
               // $errorlog->error("Session Service trying to GET non-existing for id: $sessionId error: ".$errMsg);
                return null;
            } else {
                /* fall back to database incase of internal server error */
                $fallback = true;
                $sessionlog->error("Session Service trying to GET fallback to database for id: $sessionId : ".$errMsg);
                // GT : Commenting out errorlog as it is creating unnecessary noise in prod error log increasing its size
                // $errorlog->error("Session Service trying to GET fallback to database for id: $sessionId : ".$errMsg);
            }
        }
    }

    if (!$enableSessionStore || $fallback) {
       $sessionlog->debug("Selecting session from database : ".$sessionId);
       $data = func_query_first("SELECT * FROM xcart_sessions_data WHERE sessid='$sessionId'");
       if (empty($data)) {
           $sessionlog->debug("Couldn't get session from database for id: ".$sessionId);
       }
       return $data;
    }
}

/**
 * Function to check if session exists
 * @param string $sessionId
 * @return true if exists, false otherwise
 */
function doesSessionExist($sessionId){
    global $sessionlog, $errorlog;
    $enableSessionStore = FeatureGateKeyValuePairs::getBoolean('enableSessionStoreService', false);
    $fallback = false;
    if ($enableSessionStore) {
        $client = new SessionStoreClient();
        try {
            $sessionlog->debug("Session Service trying GET (session exists) for id : ".$sessionId);
            $response = $client->performGetCall($sessionId);
            $sessionlog->debug("Session Service successful GET (session exists) for id : ".$sessionId);
            return true;
        } catch (SessionStoreClientException $e) {
            $errMsg = $e->getMessage();
            if (stripos($errMsg, "does not exist", 0) !== false) {
                $sessionlog->warn("Session Service doesn't have id: $sessionId : error : ".$errMsg);
                return false;
            } else {
                /* fall back to database incase of internal server error */
                $fallback = true;
                $sessionlog->error("Session Service trying GET for id: $sessionId falling back to database : ".$errMsg);
                //GT : Commenting out errorlog as it is creating unnecessary noise in prod error log increasing its size
                //$errorlog->error("Session Service trying GET for id: $sessionId falling back to database : ".$errMsg);
            }
        }
    }

    if (!$enableSessionStore || $fallback) {
       $sessionlog->debug("Selecting session from database : ".$sessionId);
       $result = (func_query_first_cell("SELECT COUNT(*) FROM xcart_sessions_data WHERE sessid='$sessionId'") != 0);
       if (!$result) {
           $sessionlog->debug("Couldn't get session from database for id: ".$sessionId);
       }
       return $result;
    }
}

/**
 * Function to update expiry date for session
 * @param string $sessionId
 * @param string representation of long $expiryDate
 * @param string representation of long $currTime
 * @return true if successful, false otherwise
 */
function updateSessionExpiryBySessionId($sessionId, $expiryDate, $currTime){
    global $sessionlog, $errorlog;
    $enableSessionStore = FeatureGateKeyValuePairs::getBoolean('enableSessionStoreService', false);
    $fallback = false;
    if ($enableSessionStore) {
        $client = new SessionStoreClient();
        try {
            $input = array("id"=>$sessionId,
                           "expiryDate"=>$expiryDate,
                           "lastModifiedDate"=>$currTime,
                           "lastAccessedDate"=>$currTime);
            $response = $client->performPostCall($input);
            $sessionlog->debug("Session Service successful POST (session expiry) for id : ".$sessionId);
            return true;
        } catch (SessionStoreClientException $e) {
            $errMsg = $e->getMessage();
            if (stripos($errMsg, "does not exist", 0) !== false) {
                $sessionlog->error("Session Service trying to POST (update expiry) non-existing for id: $sessionId error : ".$errMsg);
                //GT : Commenting out errorlog as it is creating unnecessary noise in prod error log increasing its size
                // $errorlog->error("Session Service trying to POST (update expiry) non-existing for id: $sessionId error : ".$errMsg);
                return false;
            } else {
                /* fall back to database incase of internal server error */
                $fallback = true;
                $sessionlog->error("Session Service exception while POST (update expiry) for id: $sessionId falling back to database : ".$errMsg);
                //GT : Commenting out errorlog as it is creating unnecessary noise in prod error log increasing its size
                // $errorlog->error("Session Service exception while POST (update expiry) for id: $sessionId falling back to database : ".$errMsg);
            }
        }
    }

    if (!$enableSessionStore || $fallback) {
        $sessionlog->debug("Updating sessions data in database for session id : ".$sessionId);
        db_query("UPDATE xcart_sessions_data SET expiry='$expiryDate' WHERE sessid='$sessionId'");
        db_query("update mk_session set lastaccesstime='$currTime' where sessionid= '$sessionId'");
        return true;
    }
}

/**
 * Function to get last accessed time as difference and updated flag
 * @param string $sessionId
 * @param long $currTime
 * @return last accessed time as diff and updated flag
 */
function getLastAccessedTimeAsDiffAndUpdatedFlagBySessionId($sessionId, $currTime){
    global $sessionlog, $errorlog;
    $enableSessionStore = FeatureGateKeyValuePairs::getBoolean('enableSessionStoreService', false);
    $fallback = false;
    $output = array();
    if ($enableSessionStore) {
        $client = new SessionStoreClient();
        $response = null;
        try {
            $response = $client->performGetCall($sessionId);
            $resultMap = prepareArrayFromXML($response);
            $output["diff"] = $currTime - intval($resultMap["lastaccesstime"]);
            $output["updated"] = $resultMap["updated"];
            $sessionlog->debug("Session Service successful GET (session updated flag) for id : ".$sessionId);
            return $output;
        } catch (SessionStoreClientException $e) {
            $errMsg = $e->getMessage();
            if (stripos($errMsg, "does not exist", 0) !== false) {
                $sessionlog->error("Session Service trying to GET non-existing for id: $sessionId error : ".$errMsg);
                //GT : Commenting out errorlog as it is creating unnecessary noise in prod error log increasing its size
                // $errorlog->error("Session Service trying to GET non-existing for id: $sessionId error : ".$errMsg);
                return $output;
            } else {
                /* fall back to database incase of internal server error */
                $fallback = true;
                $sessionlog->error("Session Service trying to GET for id: $sessionId falling back to database : ".$errMsg);
                //GT : Commenting out errorlog as it is creating unnecessary noise in prod error log increasing its size
                // $errorlog->error("Session Service trying to GET for id: $sessionId falling back to database : ".$errMsg);
            }
        }
    }

    if (!$enableSessionStore || $fallback) {
        $sessionlog->debug("Selecting last access time from database for session id : ".$sessionId);
        $data = func_query_first("select $currTime-lastaccesstime as 'diff',updated from mk_session where sessionid='$sessionId'");
        if (empty($data)) {
            $sessionlog->debug("Couldn't get last access time from database for id: ".$sessionId);
        }
        return $data;
    }
}

/**
 * Function to replace session data
 * @param string $sessionId
 * @param long $currTime
 * @param long $expiry
 * @param string flag $updated
 */
function replaceSessionDataBySessionId($sessionId, $currTime, $expiry, $updated) {
    global $sessionlog, $errorlog;
    $enableSessionStore = FeatureGateKeyValuePairs::getBoolean('enableSessionStoreService', false);
    $fallback = false;
    if ($enableSessionStore) {
        $client = new SessionStoreClient();
        try {
            $input = array("id"=>$sessionId,
                           "lastAccessedDate"=>$currTime,
                           "lastModifiedDate"=>$currTime,
                           "updatedFlag"=>$updated,
                           "expiryDate"=>$expiry,
                           "creationDate"=>$currTime,
                           "data"=>" ");
            $start_time = round(microtime(true) * 1000);
            $response = $client->performPutCall($input);
            $sessionlog->debug("Session Service successful PUT (session replace) for id : ".$sessionId);
            return true;
        } catch (SessionStoreClientException $e) {
            $errMsg = $e->getMessage();
            if (stripos($errMsg, "does not exist", 0) !== false) {
                $sessionlog->error("Session Service trying to PUT (replace) non-existing for id: $sessionId error : ".$errMsg);
                // $errorlog->error("Session Service trying to PUT (replace) non-existing for id: $sessionId error : ".$errMsg);
                return false;
            } else {
                /* fall back to database incase of internal server error */
                $fallback = true;
                $sessionlog->error("Session Service trying to PUT (replace) for id: $sessionId falling back to database : ".$errMsg);
                //GT : Commenting out errorlog as it is creating unnecessary noise in prod error log increasing its size
                // $errorlog->error("Session Service trying to PUT (replace) for id: $sessionId falling back to database : ".$errMsg);
            }
        }
    }

    if (!$enableSessionStore || $fallback) {
        $sessionlog->debug("Replacing session in database for session id : ".$sessionId);
        db_query("REPLACE INTO mk_session(sessionid,lastaccesstime,updated) values('$sessionId', $currTime, '$updated')");
        db_query("REPLACE INTO xcart_sessions_data (sessid, start, expiry, data) VALUES('$sessionId', '$currTime', '$expiry', '')");
        return true;
    }
}


/**
 * Function to update session data
 * @param string $sessionId
 * @param string representation of data $updatedDataSerialized
 */
function updateSessionDataBySessionId($sessionId, $updatedDataSerialized) {
    global $sessionlog, $errorlog;
    $enableSessionStore = FeatureGateKeyValuePairs::getBoolean('enableSessionStoreService', false);
    $fallback = false;
    if ($enableSessionStore) {
        $client = new SessionStoreClient();
        try {
            $input = array("id"=>$sessionId,
                           "data"=>$updatedDataSerialized);
            $response = $client->performPostCall($input);
            $sessionlog->debug("Session Service successful POST (session data) for id : ".$sessionId);
            return true;
        } catch (SessionStoreClientException $e) {
            $errMsg = $e->getMessage();
            if (stripos($errMsg, "does not exist", 0) !== false) {
                $sessionlog->error("Session Service trying to POST (data) non-existing for id: $sessionId error : ".$errMsg);
                //GT : Commenting out errorlog as it is creating unnecessary noise in prod error log increasing its size
                // $errorlog->error("Session Service trying to POST (data) non-existing for id: $sessionId error : ".$errMsg);
                return false;
            } else {
                /* fall back to database incase of internal server error */
                $fallback = true;
                $sessionlog->error("Session Service trying to POST (data) for id: $sessionId falling back to database : ".$errMsg);
                //GT : Commenting out errorlog as it is creating unnecessary noise in prod error log increasing its size
                // $errorlog->error("Session Service trying to POST (data) for id: $sessionId falling back to database : ".$errMsg);
            }
        }
    }

    if (!$enableSessionStore || $fallback) {
        $sessionlog->debug("Updating session data in database for session id : ".$sessionId);
        db_query("UPDATE xcart_sessions_data SET data='".addslashes($updatedDataSerialized)."' WHERE sessid='$sessionId'");
        return true;
    }
}

/**
 * Combo call to update session related data, expiryDate, and lastAccessedDate
 * Enter description here ...
 * @param unknown_type $sessionId
 * @param unknown_type $updatedDataSerialized
 */
function updateSessionBySessionId($sessionId, $expiryTime, $lastAccessedTime, $updatedDataSerialized) {
    global $sessionlog, $errorlog;
    $enableSessionStore = FeatureGateKeyValuePairs::getBoolean('enableSessionStoreService', false);
    $fallback = false;
    if ($enableSessionStore) {
        $client = new SessionStoreClient();
        try {
            $input = array("id"=>$sessionId,
                           "lastAccessedDate"=>$lastAccessedTime,
                           "lastModifiedDate"=>$lastAccessedTime,
                           "expiryDate"=>$expiryTime,
                           "data"=>$updatedDataSerialized);
            $response = $client->performPostCall($input);
            $sessionlog->debug("Session Service successful POST (session data) for id : ".$sessionId);
            return true;
        } catch (SessionStoreClientException $e) {
            $errMsg = $e->getMessage();
            if (stripos($errMsg, "does not exist", 0) !== false) {
                $sessionlog->error("Session Service trying to POST (data) non-existing for id: $sessionId error : ".$errMsg);
                // $errorlog->error("Session Service trying to POST (data) non-existing for id: $sessionId error : ".$errMsg);
                return false;
            } else {
                /* fall back to database incase of internal server error */
                $fallback = true;
                $sessionlog->error("Session Service trying to POST (data) for id: $sessionId falling back to database : ".$errMsg);
                // $errorlog->error("Session Service trying to POST (data) for id: $sessionId falling back to database : ".$errMsg);
            }
        }
    }

    if (!$enableSessionStore || $fallback) {
        $sessionlog->debug("Updating session data in database for session id : ".$sessionId);
        if (!is_null($expiryTime)) {
            db_query("UPDATE xcart_sessions_data SET expiry='$expiryTime', data='".addslashes($updatedDataSerialized)."' WHERE sessid='$sessionId'");
        } else {
            db_query("UPDATE xcart_sessions_data SET data='".addslashes($updatedDataSerialized)."' WHERE sessid='$sessionId'");
        }
        if (!is_null($lastAccessedTime)) {
            db_query("update mk_session set lastaccesstime='$lastAccessedTime' where sessionid= '$sessionId'");
        }
        return true;
    }
}

/**
 * Function to update 'updated' flag of session
 * @param string $sessionId
 * @param string $updatedFlag
 * @return true if successful, otherwise false
 */
function updateSessionUpdatedFlagBySessionId($sessionId, $updatedFlag){
    global $sessionlog, $errorlog;
    $enableSessionStore = FeatureGateKeyValuePairs::getBoolean('enableSessionStoreService', false);
    $fallback = false;
    if ($enableSessionStore) {
        $client = new SessionStoreClient();
        try {
            $input = array("id"=>$sessionId,
                           "updatedFlag"=>$updatedFlag);
            $response = $client->performPostCall($input);
            $sessionlog->debug("Session Service successful POST (session updated flag) for id : ".$sessionId);
            return true;
        } catch (SessionStoreClientException $e) {
            $errMsg = $e->getMessage();
            if (stripos($errMsg, "does not exist", 0) !== false) {
                $sessionlog->error("Session Service trying to POST (updated) non-existing for id: $sessionId error : ".$errMsg);
                // $errorlog->error("Session Service trying to POST (updated) non-existing for id: $sessionId error : ".$errMsg);
                return false;
            } else {
                /* fall back to database incase of internal server error */
                $fallback = true;
                $sessionlog->error("Session Service trying to POST (updated) for id: $sessionId falling back to database : ".$errMsg);
                // $errorlog->error("Session Service trying to POST (updated) for id: $sessionId falling back to database : ".$errMsg);
            }
        }
    }

    if (!$enableSessionStore || $fallback) {
        $sessionlog->debug("Update last updated flag in database for session id : ".$sessionId);
        db_query("update mk_session set updated='$updatedFlag' where sessionid= '$sessionId'");
        return true;
    }
}
?>
