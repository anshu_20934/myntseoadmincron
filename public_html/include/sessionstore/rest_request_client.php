<?php
require_once ($xcart_dir.'/logger.php');

/**
 * Generic REST request utility for php from Ian
 * http://www.gen-x-design.com/archives/making-restful-requests-in-php/
 * Modified to suite Session Store Client purpose
 */

/**
 * php REST client using curl to make GET,PUT, and POST calls
 */
class SessionRestRequest
{
    protected $url;
    protected $verb;
    protected $requestBody;
    protected $requestLength;
    protected $username;
    protected $password;
    protected $acceptType;
    protected $responseBody;
    protected $responseInfo;
    protected $curlErrorNo;
    protected $logger;

    /**
     * Constructor
     * @param string $url
     * @param string $verb
     * @param string $requestBody
     * @throws InvalidArgumentException
     */
    public function __construct($url = null, $verb = 'GET', $requestBody = null) {
        $this->url                = $url;
        $this->verb               = $verb;
        $this->requestBody        = $requestBody;
        $this->username           = null;
        $this->password           = null;
        $this->responseBody       = null;
        $this->responseInfo       = null;

        if ($this->requestBody != null && !is_string($this->requestBody)) {
            throw new InvalidArgumentException('Invalid data input for requestBody.  String expected');
        }
    }

    /**
     * Function to reset
     */
    public function flush() {
        $this->requestBody        = null;
        $this->verb               = 'GET';
        $this->responseBody       = null;
        $this->responseInfo       = null;
    }

    /**
     * Generic execute function
     * @throws InvalidArgumentException
     * @throws Exception
     */
    public function execute() {
        global $sessionlog;
        global $errorlog;
        $ch = curl_init();
        $this->setAuth($ch);

        try {
            switch (strtoupper($this->verb)) {
                case 'GET':
                    $this->executeGet($ch);
                    break;
                case 'POST':
                    $this->executePost($ch);
                    break;
                case 'PUT':
                    $this->executePut($ch);
                    break;
                case 'DELETE':
                    $this->executeDelete($ch);
                    break;
                default:
                    throw new InvalidArgumentException('Current verb (' . $this->verb . ') is an invalid REST verb.');
            }
        }
        catch (InvalidArgumentException $e) {
            curl_close($ch);
            $sessionlog->error("Illegal request verb for REST client: ".$e->getMessage());
            $errorlog->error("Illegal request verb for REST client: ".$e->getMessage());
            throw $e;
        }
        catch (Exception $e) {
            curl_close($ch);
            $sessionlog->error("REST client exception: ". $e->getMessage());
            $errorlog->error("REST client exception: ". $e->getMessage());
            throw $e;
        }

    }

    /**
     * Function for GET execution
     * @param curl handle $ch
     */
    protected function executeGet($ch) {
        $headers=array("Accept: application/xml");
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $this->doExecute($ch);
    }

    /**
     * Function for POST execution
     * @param curl handle $ch
     * @throws InvalidArgumentException
     */
    protected function executePost($ch) {
        if ($this->requestBody == null || !is_string($this->requestBody)) {
            throw new InvalidArgumentException('Invalid data input for requestBody.  String expected');
        }

        //TODO: url encoding support
        //$urlEncodedRequestBody = urlencode($this->requestBody)

        $headers=array("Accept: application/xml", "Content-Type: application/xml");
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $this->requestBody);
        curl_setopt($ch, CURLOPT_POST, true);

        $this->doExecute($ch);
    }

    /**
     * Function for PUT execution
     * @param curl handle $ch
     * @throws InvalidArgumentException
     */
    protected function executePut($ch) {
        if ($this->requestBody == null || !is_string($this->requestBody)) {
            throw new InvalidArgumentException('Invalid data input for requestBody.  String expected');
        }

        //TODO: urlencoding support on server if needed
        //$urlEncodedRequestBody = urlencode($this->requestBody);

        $requestLength = strlen($this->requestBody);

        $fh = fopen('php://memory', 'rw');
        fwrite($fh, $this->requestBody);
        rewind($fh);

        curl_setopt($ch, CURLOPT_INFILE, $fh);
        curl_setopt($ch, CURLOPT_INFILESIZE, $requestLength);

        $headers=array("Accept: application/xml", "Content-Type: application/xml");
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_PUT, true);

        $this->doExecute($ch);

        fclose($fh);
    }

    /**
     * Function for DELETE execution
     * @param curl handle $ch
     */
    protected function executeDelete($ch) {
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');

        $this->doExecute($ch);
    }

    /**
     * Generic function to excute
     * @param curl handle $curlHandle
     */
    protected function doExecute(&$curlHandle) {
        $this->setCurlOpts($curlHandle);
        $this->responseBody = curl_exec($curlHandle);
        $this->responseInfo	= curl_getinfo($curlHandle);
        $this->curlErrorNo = curl_errno($curlHandle);
        curl_close($curlHandle);
    }

    /**
     * Generic function to set curl options
     * @param curl handle $curlHandle
     */
    protected function setCurlOpts(&$curlHandle) {
        curl_setopt($curlHandle, CURLOPT_TIMEOUT, 5);
        curl_setopt($curlHandle, CURLOPT_CONNECTTIMEOUT, 1);
        curl_setopt($curlHandle, CURLOPT_URL, $this->url);
        curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, true);
    }

    /**
     * Function to perform authentication
     * @param curl handle $curlHandle
     */
    protected function setAuth(&$curlHandle) {
        if ($this->username !== null && $this->password !== null) {
            curl_setopt($curlHandle, CURLOPT_HTTPAUTH, CURLAUTH_DIGEST);
            curl_setopt($curlHandle, CURLOPT_USERPWD, $this->username . ':' . $this->password);
        }
    }

    /**
     * Getteres and Setters
     */
    public function getAcceptType() {
        return $this->acceptType;
    }

    public function setAcceptType($acceptType) {
        $this->acceptType = $acceptType;
    }

    public function getPassword() {
        return $this->password;
    }

    public function setPassword($password) {
        $this->password = $password;
    }

    public function getResponseBody() {
        return $this->responseBody;
    }

    public function getResponseInfo() {
        return $this->responseInfo;
    }
    
    public function getCurlErrNo() {
        return $this->curlErrorNo;
    }

    public function getUrl() {
        return $this->url;
    }

    public function setUrl($url) {
        $this->url = $url;
    }

    public function getUsername() {
        return $this->username;
    }

    public function setUsername($username) {
        $this->username = $username;
    }

    public function getVerb() {
        return $this->verb;
    }

    public function setVerb($verb) {
        $this->verb = $verb;
    }
}
