<?php
use order\dataobject\builder\OrderDOBuilder;

use order\exception\CartTamperedException;
use order\exception\EmptyCartException;
use order\exception\NoSuitableGatewayException;
use order\exception\OrderCreationFailedException;
use order\exception\UserNotLoggedInException;

use order\dataobject\builder\OrderInputParamDOBuilder;
use order\manager\OrderManager;

use abtest\MABTest;

//Kundan: We no longer use mkorderinsert_re.php: all of that goes inside here, and nested classes
//include_once("$xcart_dir/mkorderinsert_re.php");
define('_CONTROLLER_','orderinsert');
if(file_exists("./auth.php")) {
	include_once("./auth.php");
} else if (file_exists("../auth.php")) {
	include_once("../auth.php"); // happens for secure pages
} 
include_once "$xcart_dir/include/class/mcart/class.MCart.php";
include_once("$xcart_dir/include/class/oms/EventCreationManager.php");
//Redirect to gift card order insert
//This is needed here because tekprocess gateway has this php name fixed for it to work with
$order_type=filter_input(INPUT_POST, 'order_type', FILTER_SANITIZE_STRING);
if(isset($order_type) && !empty($order_type) && $order_type == 'gifting'){
	include_once "$xcart_dir/mkgiftcardorderinsert.php";
	exit;
}

//Get the cart for which Order needs to be created
$mcartFactory = new MCartFactory();
$cart = $mcartFactory->getCurrentUserCart();
//NOTE: Removed Logging to order.log

//Create Order Data Object Builder from which Order DO and all child DOs get created 
$orderDOBuilder = new OrderDOBuilder();

global $http_location, $https_location, $weblog, $errlog, $XCART_SESSION_VARS;

//Build OrderDO from the cart
try {
	if($cart == null) {
		throw new EmptyCartException("Cart Empty while checkout: may be due to concurrent activity");
	}
	$orderDO = $orderDOBuilder->build($cart);	
} catch (UserNotLoggedInException $exc) {
	redirectAndCloseOverlayPaymentWindow($http_location);
} catch (EmptyCartException $exc) {
	redirectAndCloseOverlayPaymentWindow("$http_location/mkmycart.php?at=c&pagetype=productdetail&src=po");
} catch (Exception $e) {
	$weblog->info("Redirecting back to the payment page. Reason: Exception Occured:".$e);
	redirectAndCloseOverlayPaymentWindow($https_location . "/mkpaymentoptions.php?transaction_status=N");
}

//Get an instance of OrderManager, which will do the top level business transaction
$orderManager = new OrderManager($orderDO);

//Stage-1: Create order by doing checkout
try {
	$orderManager->checkout();
	EventCreationManager::pushCompleteOrderEvent($orderDO->getOrderId());
} catch (UserNotLoggedInException $unli) {
	//User's session somehow lost
	$weblog->error("Redirecting back to home page.Reason: ".$unli);
	$errorlog->error("Redirecting back to home page.Reason: ".$unli);
	redirectAndCloseOverlayPaymentWindow($http_location);
} catch (EmptyCartException $ece) {
	//stale cart , cart is made empty from another tab/window - cannot proceed. Rebuild the cart again
	$weblog->info("Redirecting back to home page.Reason: ".$unli);
	redirectAndCloseOverlayPaymentWindow("$http_location/mkmycart.php?at=c&pagetype=productdetail&src=po");
} catch (CartTamperedException $cte) {
	//Cart was possibly tampered. Rebuild the cart again
	$weblog->error("Redirecting to cart page: Reason: cart was possibly tampered: ".$cte);
	$errorlog->error("Redirecting to cart page: Reason: cart was possibly tampered: ".$cte);
	redirectAndCloseOverlayPaymentWindow("$http_location/mkmycart.php?at=c&pagetype=productdetail&src=mi");
} catch (NoShippingAddressException $exc) {
	redirectAndCloseOverlayPaymentWindow($https_location . "/mkcustomeraddress.php?change=true");
} catch(CouponException $cpexc) {
	//NOTE: Removed x_session_register: redirectedToCart:  
	$weblog->info("Redirecting back to the payment page. Reason: ".$cpexc->getMessage());
	redirectAndCloseOverlayPaymentWindow($https_location . "/mkpaymentoptions.php");
} catch(OrderCreationFailedException $ordex) {
	$weblog->error("Order creation failed: ".$ordex->getDO());
	$errorlog->error("Order creation failed: ".$ordex->getDO());
	$XCART_SESSION_VARS['errormessage'] = "Your Transaction failed. We request you to kindly re-try the transaction.";
	redirectAndCloseOverlayPaymentWindow($https_location . "/mkpaymentoptions.php?transaction_status=N");
} catch (NoSuitableGatewayException $e) {
	global $weblog;
	$weblog->info("Redirecting back to the payment page. Reason: No suitable gateway found for calculating Payment gateway Discount. payment dataobject:".$e->toString());
	$paymentDO = $orderManager->orderDO->getPaymentDO();
	$emi_bank = $paymentDO->getEMIBank();
	if(!empty($emi_bank)) {
		redirectAndCloseOverlayPaymentWindow($https_location . "/mkpaymentoptions.php?transaction_status=EMI");
	} else {
		redirectAndCloseOverlayPaymentWindow($https_location . "/mkpaymentoptions.php?transaction_status=N");
	}
} catch (Exception $e) {
	$weblog->info("Redirecting back to the payment page. Reason: Exception Occured:" . $e);
	redirectAndCloseOverlayPaymentWindow($https_location . "/mkpaymentoptions.php?transaction_status=N");
}

//Stage-2: Attempt to pay for this order
try {
	$form = $orderManager->getPaymentGatewayForm();
} catch (NoSuitableGatewayException $e) {
	global $weblog;
	$weblog->info("Redirecting back to the payment page. Reason: No suitable gateway found for payment dataobject:".$e->toString());
	redirectAndCloseOverlayPaymentWindow($https_location . "/mkpaymentoptions.php?transaction_status=N");
	//func_header_location($https_location . "/mkpaymentoptions.php?transaction_status=N",false);
} catch (Exception $e) {
	$weblog->info("Redirecting back to the payment page. Reason: Exception Occured:".$e);
	redirectAndCloseOverlayPaymentWindow($https_location . "/mkpaymentoptions.php?transaction_status=N");
}

?>

<body onload="javascript: submitForm()">

<?php echo $form; 
?>

	<script type="text/javascript">
	 function submitForm(){
	     document.subFrm.submit(); 
	 }
	 </script>
</body>
