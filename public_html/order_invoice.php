<?php
require "./auth.php";
include_once("./include/func/func.mk_orderbook.php");
include_once("./include/func/func.order.php");
include_once("./include/func/func.mkcore.php");
include_once("./include/func/func.mkspecialoffer.php");
include_once("$xcart_dir/modules/apiclient/WarehouseApiClient.php");

$smarty->assign("location", $location);
$orderids = $HTTP_GET_VARS['orderids'];

$orderids = explode(',',$orderids);
$buffer = array();

$warehouses = WarehouseApiClient::getAllWarehouses();
$whId2warehouse = array();
foreach($warehouses as $warehouse) {
	$whId2warehouse[$warehouse['id']] = $warehouse;
}

foreach($orderids as $xyz=>$orderid){

  $order_data = func_order_data($orderid);
  $order = $order_data["order"];
  $userinfo = $order_data["userinfo"];
  $giftcerts = $order_data["giftcerts"];

  $warehouse = $whId2warehouse[$order['warehouseid']];

  $invoicetocustomer=0;
  $userType = $XCART_SESSION_VARS['login_type'];
  if(($XCART_SESSION_VARS['identifiers'][$userType]['account_type'] == "SM")||($XCART_SESSION_VARS['identifiers'][$userType]['account_type'] == "P2")){
  	$smarty->assign('usertype',$XCART_SESSION_VARS['identifiers'][$userType]['account_type']);
  	$query = "select mk_sources.*,mk_shop_masters.options,mk_shop_masters.email as shop_master_email from mk_shop_masters,mk_sources,xcart_orders where mk_sources.shop_master_id=mk_shop_masters.shop_master_id and mk_sources.source_id=xcart_orders.source_id and xcart_orders.orderid='$orderid'";
  	$result = func_query_first($query);
  	$invoicetocustomer = getParamFromOptionsList($result[options],'invoicetocustomer');
  	if(strtolower($result[source_manager_email])!=strtolower($login)&&strtolower($result[shop_master_email])!=strtolower($login)){
  		$errormessage = "We are sorry, you are not authorized to access this page.";
  		$XCART_SESSION_VARS['errormessage'] = $errormessage;
  	    header("Location:mksystemerror.php");
  		exit;
  	}
  	 
  	$smarty->assign('store',$result);
  }
  if((($XCART_SESSION_VARS['identifiers']['A']['login_type'] == 'A') && ($HTTP_GET_VARS['type'] == 'admin'))||($XCART_SESSION_VARS['identifiers'][$userType]['account_type'] == "SM")||($XCART_SESSION_VARS['identifiers'][$userType]['account_type'] == "P2")){
      $query = "select source_type_id from xcart_orders o LEFT JOIN mk_sources s ON s.source_id=o.source_id where o.orderid = '{$orderid}'" ;
      $source_type_id = func_query_first_cell($query);
      $smarty->assign('source_type_id', $source_type_id);
      $tb_product_category = $sql_tbl["products_categories"];

      if(empty($giftcerts)){
          $giftcerts = 0;
      }

      
      $smarty->assign("order",$order);
      //$smarty->assign("userinfo",$userinfo);
      $smarty->assign("giftcerts",$giftcerts);

      $today = date("d/m/Y",$order[date]);
      $smarty->assign("today",$today);
      switch($order['status']){
          case 'P': $status = 'Processed'; break;
          case 'C': $status = 'Complete'; break;
          case 'Q': $status = 'Queued'; break;
          case 'D': $status = 'Declined'; break;
          case 'F': $status = 'Failed'; break;
          case 'PD': $status = 'Pending'; break;
          //case 'PP': $status = 'PP'; break;
      }
      $smarty->assign("status", $status );
      $smarty->assign("invoiceid", $order['invoiceid'] );
      $smarty->assign("orderid", $orderid );
      $smarty->assign("deliverymethod", $order['shipping_method'] );

      #
      #Call function to load the customer details
      #
      $customerdetail = func_customer_order_address_detail($order['login'], $orderid);

      #
      #Load country label
      #
      $sql = "SELECT name, value FROM xcart_languages WHERE topic='Countries'";
      $country_list = func_query($sql);
      foreach($country_list AS $key => $array){
          $name = explode("_", $array['name']);
          if(strstr(trim($customerdetail['s_country']), $name[1])){
              $customerdetail['s_country'] = $array['value'];
          }
          if(strstr(trim($customerdetail['b_country']), $name[1])){
              $customerdetail['b_country'] = $array['value'];
          }
      }
      $customerdetail['s_state'] = func_get_state_label($customerdetail['s_state']);
      $customerdetail['b_state'] = func_get_state_label($customerdetail['b_state']);



      $smarty->assign("userinfo", $customerdetail);
      $smarty->assign("mobile", $customerdetail['mobile']);

      $smarty->assign("address", stripslashes($customerdetail['s_address']));
      $smarty->assign("pincode", $customerdetail['s_zipcode']);
      $smarty->assign("phone", $customerdetail['phone']);
      $smarty->assign("mobile", $customerdetail['mobile']);
      $smarty->assign("fax", $customerdetail['fax']);
      $smarty->assign("email", $customerdetail['s_email']);
      $smarty->assign("statename", $customerdetail['s_state']);
      $smarty->assign("countryname", $customerdetail['s_country']);
      $smarty->assign("cityname", $customerdetail['s_city']);
      $smarty->assign("locality", $customerdetail['s_locality']);
      $optionCounter = array();
      if( $source_type_id == 11 && $view != 'OD')
          $productsInCart = func_query("SELECT product, productid, product_style, product_type, pos_unit_mrp as price, price as store_price, amount, subtotal, total, cod ,$sql_tbl[order_details].discount AS productDiscount, $sql_tbl[orders].discount AS totaldiscountonProducts, coupon_discount, $sql_tbl[orders].coupon AS couponcode, shipping_cost, tax, ordertype, ref_discount, $sql_tbl[order_details].quantity_breakup AS quantity_breakup, $sql_tbl[order_details].shipping_amount, $sql_tbl[order_details].shipping_tax, ($sql_tbl[order_details].shipping_amount + $sql_tbl[order_details].shipping_tax) AS net_shipping, $sql_tbl[order_details].tax_rate, $sql_tbl[order_details].taxamount, $sql_tbl[order_details].pos_total_amount as total_amount,$sql_tbl[order_details].total_amount as store_total_amount, $sql_tbl[order_details].promotion_id,$sql_tbl[order_details].addonid,$sql_tbl[order_details].is_customizable, $sql_tbl[orders].pg_discount AS pg_discount, $sql_tbl[orders].payment_surcharge AS payment_surcharge FROM $sql_tbl[order_details], $sql_tbl[orders] WHERE $sql_tbl[order_details].item_status <> 'IC' AND $sql_tbl[order_details].orderid = $sql_tbl[orders].orderid AND $sql_tbl[order_details].orderid = '".$orderid."'");
      elseif( $source_type_id == 11 && $view == 'OD' && $invoicetocustomer ==1)
          $productsInCart = func_query("SELECT product, productid, product_style, product_type, pos_unit_mrp as price, price as store_price, amount, subtotal, total, cod ,$sql_tbl[order_details].discount AS productDiscount, $sql_tbl[orders].discount AS totaldiscountonProducts, coupon_discount, $sql_tbl[orders].coupon AS couponcode, shipping_cost, tax, ordertype, ref_discount, $sql_tbl[order_details].quantity_breakup AS quantity_breakup, $sql_tbl[order_details].shipping_amount, $sql_tbl[order_details].shipping_tax, ($sql_tbl[order_details].shipping_amount + $sql_tbl[order_details].shipping_tax) AS net_shipping, $sql_tbl[order_details].tax_rate, $sql_tbl[order_details].taxamount, $sql_tbl[order_details].pos_total_amount as total_amount,$sql_tbl[order_details].total_amount as store_total_amount, $sql_tbl[order_details].promotion_id,$sql_tbl[order_details].addonid,$sql_tbl[order_details].is_customizable, $sql_tbl[orders].pg_discount AS pg_discount, $sql_tbl[orders].payment_surcharge AS payment_surcharge FROM $sql_tbl[order_details], $sql_tbl[orders] WHERE $sql_tbl[order_details].item_status <> 'IC' AND $sql_tbl[order_details].orderid = $sql_tbl[orders].orderid AND $sql_tbl[order_details].orderid = '".$orderid."'");
      else
          $productsInCart = func_query("SELECT product, productid, product_style, product_type, price, amount, subtotal, total, cod ,$sql_tbl[order_details].discount AS productDiscount, $sql_tbl[orders].discount AS totaldiscountonProducts, coupon_discount, $sql_tbl[orders].coupon AS couponcode, shipping_cost, tax, ordertype, ref_discount, $sql_tbl[order_details].quantity_breakup AS quantity_breakup, $sql_tbl[order_details].shipping_amount, $sql_tbl[order_details].shipping_tax, ($sql_tbl[order_details].shipping_amount + $sql_tbl[order_details].shipping_tax) AS net_shipping, $sql_tbl[order_details].tax_rate, $sql_tbl[order_details].taxamount, $sql_tbl[order_details].total_amount, $sql_tbl[order_details].promotion_id,$sql_tbl[order_details].addonid,$sql_tbl[order_details].is_customizable,$sql_tbl[orders].pg_discount AS pg_discount, $sql_tbl[orders].payment_surcharge AS payment_surcharge FROM $sql_tbl[order_details], $sql_tbl[orders] WHERE $sql_tbl[order_details].item_status <> 'IC' AND $sql_tbl[order_details].orderid = $sql_tbl[orders].orderid AND $sql_tbl[order_details].orderid = '".$orderid."'");
	
      $k=0;$j=0;
      $categories = array();
      $productids = array();
      $discountonoffer = 0;
      foreach($productsInCart as $key => $order)
      {
      	//Getting Size options from quantity_breakup and the unifiedsizes
        $styleid=$productsInCart[$key]['product_style'];
        $unifiedsize=Size_unification::getUnifiedSizeByStyleId($styleid, "array", true);
        $quantity_breakup=$productsInCart[$key]['quantity_breakup'];
        $quantity_breakup_exploded=explode(",",$quantity_breakup);
        $sizeOptions=array();
        foreach($quantity_breakup_exploded as $qty_breakup_key => $qty_breakup_val){
                $size_qty_arr=explode(":", $qty_breakup_val);
                $sizeOptions[$size_qty_arr[0]]=$size_qty_arr[1];
                if($size_qty_arr[1]){
                		$order_size=$size_qty_arr[0];
                        $order_unified_size=$unifiedsize[$size_qty_arr[0]];
                        $order_qty=$size_qty_arr[1];
                }
        }
      	
        $productsInCart[$key]['order_size']=$order_size;
        $productsInCart[$key]['order_unified_size']=$order_unified_size;
        $productsInCart[$key]['order_qty']=$productsInCart[$key]['amount'];
      	
          //Query for retriving the style name from mk_product_style
          $stylename = "SELECT name, styletype FROM $sql_tbl[mk_product_style] WHERE id = '".$productsInCart[$key]['product_style']."'";
          $styleresult = db_query($stylename);
          $rowstyle = db_fetch_array($styleresult);

          //Query for retriving the style options from mk_product_options
          $styleoption = "SELECT $sql_tbl[mk_product_options].name AS optionName, $sql_tbl[mk_product_options].value AS optionValue FROM $sql_tbl[mk_product_options], $sql_tbl[mk_product_options_order_details_rel]
  WHERE $sql_tbl[mk_product_options].id = $sql_tbl[mk_product_options_order_details_rel].option_id
  AND $sql_tbl[mk_product_options].style = ".$productsInCart[$key]['product_style']."
  AND $sql_tbl[mk_product_options_order_details_rel].orderid = '".$orderid."'
  AND $sql_tbl[mk_product_options_order_details_rel].itemid = '".$productsInCart[$key]['productid']."'";


          $optionresult = db_query($styleoption);
          $optionCount = db_num_rows($optionresult);
          $productsInCart[$key]['optionCount'] = $optionCount;
          $i = 0;
          while($optionrow = db_fetch_array($optionresult))
          {
              $productsInCart[$key]['optionNames'][$i] =  $optionrow['optionName'];
              $productsInCart[$key]['optionValues'][$i] =  $optionrow['optionValue'];
              $i++;
          }

          $typename = "SELECT pt.name as productName, pt.label as pLabel, pt.image_t as pImage, ps.name as sName
  					FROM $sql_tbl[producttypes] AS pt, $sql_tbl[mk_product_style] AS ps
  					WHERE pt.id = ps.product_type
  					AND pt.id = ".$productsInCart[$key]['product_type']."
  					AND ps.id=".$productsInCart[$key]['product_style']."";


          $typeresult = db_query($typename);
          $row = db_fetch_array($typeresult);
          $productsInCart[$key]['productStyleName'] = $row['sName'];
          if($productsInCart[$key]['addonid'])
          {
              $addonname = get_style_addon_name($productsInCart[$key]['addonid']);
              if($addonname)
              {
                  $productsInCart[$key]['productStyleName'] .= " ( $addonname ) ";
              }
          }

          $areaId = func_get_all_orientation_for_default_customization_area($productsInCart[$key]['product_style']);

          /*$defaultImage = "SELECT image AS ProdImage
                            FROM $sql_tbl[mk_xcart_order_details_customization_rel] odc
                            WHERE area_id = ".($areaId[0][6])."
                            AND itemid = ".$productsInCart[$key]['productid']."
                            AND orderid = '".$_GET['orderid']."'";*/
          $pid = $productsInCart[$key]['productid'];
          $defaultImage = "SELECT image_portal_t  as ProdImage FROM $sql_tbl[products] WHERE productid =$pid";


          $ImageResult = db_query($defaultImage);
          $ImageRow = db_fetch_array($ImageResult);

          $productsInCart[$key]['productStyleType'] = $rowstyle['styletype'];
          $productsInCart[$key]['product_type'] = $rowstyle['name'];
          $productsInCart[$key]['productTypeLabel'] = $row['pLabel'];
          $productsInCart[$key]['designImagePath'] = $ImageRow['ProdImage'];
          $productsInCart[$key]['totalPrice'] = $productsInCart[$key]['price'] * $productsInCart[$key]['amount'];

          $allCustImagePath = func_load_product_customized_area_rel_details($pid);
          $n = 0;
          foreach($allCustImagePath as $k=>$value)
          {
              if($value['thumb_image'] != "")
              {
                  $productsInCart[$key]['allCustImagePath'][$n] = $value['thumb_image'];
                  $n++;
              }
          }


          //To get the categoryids of the product
          $catidquery = "select $tb_product_category.categoryid from
  	$tb_product_category where $tb_product_category.productid =".$productsInCart[$key]['productid']."";
          $catidresult=db_query($catidquery);

          if($catidresult)
          {
              $row=db_fetch_row($catidresult);
              if(!in_array($row[0], $categoryids) && ($row[0] != ""))
              {
                  $categoryids[$k] = $row[0];
              }
          }

          $k++;

          // create an array of product ids
          $productids[$j] = $productsInCart[$key]['productid'];
          $j++;

          // Count total discount of offer
          $discountonoffer = $discountonoffer + $productsInCart[$key]['productDiscount'];
      }
      //
      //echo "<pre>";
      //print_r($productsInCart[1][promotion]);
      //exit;
      //Coupon discount
      $coupondiscount = $productsInCart[0]['coupon_discount'];
      //Coupon Code
      $couponCode = addslashes($productsInCart[0]['couponcode']);
      if(!empty($couponCode))
      {
          $couponTable        = $sql_tbl["discount_coupons"];
          $queryCouponPercent  = "SELECT c.discount,c.coupon_type from $couponTable c where c.coupon = '$couponCode'
   	  and status = 'A' ";
          $rsCouponPercent  = db_query($queryCouponPercent);
          $rowCouponPercent = db_fetch_array($rsCouponPercent);
          if($rowCouponPercent['coupon_type']=='percent')
              $smarty->assign("couponpercent", $rowCouponPercent['discount']."%");
          else
              $smarty->assign("couponpercent", "Rs. ".$rowCouponPercent['discount']);
      }


      $totalamount = $productsInCart[0]['subtotal'];
      //$amountwitoutcc = $productsInCart[0]['subtotal'];// - $productsInCart[0]['totaldiscountonProducts'];
      $amountafterdiscount = $productsInCart[0]['total'];
      $shipping_cost = $productsInCart[0]['shipping_cost'];
      $cod_charges = $productsInCart[0]['cod'];
      $vatCst = $productsInCart[0]['tax'];
      $ref_discount = $productsInCart[0]['ref_discount'];
      $pg_discount = $productsInCart[0]['pg_discount'];
      $emi_charge = $productsInCart[0]['payment_surcharge'];

      // calculating the final amount to be paid after  calculating all the taxes
      //$vatCst =  (12.50 * floatval($amountafterdiscount))/100;
      $amountafterdiscount = $amountafterdiscount + $emi_charge;
      $productDiscount = $order_data['order']['discount'];

      if($order_data["order"]['payment_method'] == 'cod')
          $amountafterdiscount = $amountafterdiscount + $order_data["order"]['cod'];

      $smarty->assign("vat",$vatCst);

      // Call function to get special offer of productid
      $productOffer = func_get_discount_offers_data($productids, 'P');

      //Call function to get special offer of category id
      $categoryOffer = func_get_discount_offers_data($categoryids, 'C');

      $offers = array_merge($productOffer, $categoryOffer);
      $countoffer = count($offers);

      // Call function to get special offers for total
      $totaloffers = func_get_special_offers_for_Total();
      $counttotaloffer = count($totaloffers);

      // Get special offer for discount on total
      $amountaftersofferdiscount = $totalamount - $discountonoffer;
      $discountOnTotal = func_get_special_offer_discount_for_Total($amountaftersofferdiscount);

      // Count total discount on offer
      $totaloffersdiscount = $discountonoffer + $discountOnTotal;

      for($j=0; $j<3; $j++)
      {
          $optionCounter[$j]= $j;
      }

      //mask last 4 digits of the coupon code for security when showing on the invoice.
      $len=strlen($couponCode);
      if($len<=4)
          $couponCode=str_repeat('X',$len);
      else $couponCode=substr($couponCode,0,($len-4)).str_repeat('X',4);

      $smarty->assign("gift_status",$order_data["order"]['gift_status']);
      $smarty->assign("ref_discount",$ref_discount);
      $smarty->assign("optionCounter",$optionCounter);
      $smarty->assign("counttotaloffer", $counttotaloffer);
      $smarty->assign("countoffers", $countoffer);
      $smarty->assign("couponCode", $couponCode);
      $smarty->assign("coupondiscount", $coupondiscount);
      $smarty->assign("productDiscount", $productDiscount);
      $smarty->assign("offers", $offers);
      $smarty->assign("totaloffers", $totaloffers);
      $smarty->assign("paymentoption", $order_data["order"]['payment_method']);
      $smarty->assign("totaloffersdiscount", number_format($productsInCart[0]['totaldiscountonProducts'],2,".",''));
      $smarty->assign("pg_discount", $pg_discount);
      $smarty->assign("emi_charge", $emi_charge);

      if(empty($productsInCart))
      {
          $productsInCart = 0;
      }

      //$order_data = func_order_data($_GET['orderid']);

      //$order = $order_data["order"];
      $products = $order_data["products"];

      $url = "{$baseurl}/include/class/class.barcode.php?code=" . $orderid . "&text=0&type=.png";
      $smarty->assign("barcodeUrl", $url);

      $sql = "SELECT FROM_UNIXTIME(commentdate) as date, commenttitle, description
                FROM mk_ordercommentslog WHERE commenttype = 'Order Change' AND orderid = " . $orderid;

      $comments = func_query($sql);
      $smarty->assign("orderComments", $comments);

      $smarty->assign("productsInCart", $productsInCart);
      //$totalafterdiscount = $productsInCart[0]['total'] - $totaldiscount;
      $cashdiscount=func_query_first_cell("select cash_redeemed from xcart_orders where orderid=".$orderid);
      $smarty->assign("cashdiscount",$cashdiscount);
      $smarty->assign("shippingRate", number_format($shipping_cost,2,".",''));
      $smarty->assign("cod_charges", number_format($cod_charges,2,".",''));
      $smarty->assign("amountwitoutcc", $totalamount);
      $smarty->assign("grandTotal", number_format($totalamount,2,".",''));
      $smarty->assign("totalafterdiscount", number_format(floor($amountafterdiscount),2,".",''));

	  $address = $warehouse['address'];
	  $address_fields = explode("\n", $address);
	  
      $smarty->assign("address1", $address_fields[0]);
      $smarty->assign("address2", $address_fields[1]);
      if($address_fields[2])
      	$smarty->assign("address3", $address_fields[2]);
      
      $smarty->assign("address4", $warehouse['city']."- ".$warehouse['postal_code'].", ".$warehouse['state'].", ".$warehouse['country']);
      $smarty->assign("companyName", "Vector E-Commerce Pvt Ltd.");

      $smarty->assign("vatId", $warehouse['tin_number']);

      //code for printing change requests for a order
      if(func_get_order_changereqstatus($orderid)=='Y'){
          $sql = "SELECT * FROM mk_ordercommentslog WHERE orderid = $orderid AND commenttype = 'change_req'";
          $comemnts = func_query($sql);
          // $comments=func_getorder_requests_array($orderid);
          $smarty->assign("changestatus","Y");
          $smarty->assign("comments", $comments);
      };
      
      if($offline == 'off')
          $ret = func_display("order_invoice_bulk_partial.tpl", $smarty,false);
      else
          $ret = func_display("order_invoice_partial.tpl", $smarty,false);
  }else{
    $errormessage = "We are sorry, you are not authorized to access this page.";
    $XCART_SESSION_VARS['errormessage'] = $errormessage;
    header("Location:mksystemerror.php");
  }

  $buffer[] = $ret;
}
$output = implode("<br style='display:block; page-break-after:always;' />",$buffer);
$smarty->assign("content", $output);
$offline = 'off';
$weblog->debug($productsInCart);
if($offline == 'off'){
  func_display("order_invoice_bulk.tpl", $smarty);
}else{
  func_display("order_invoice.tpl", $smarty);
}
?>
