:<?php
date_default_timezone_set("Asia/Calcutta");
class Tester{

    private static $servicesToBlock = array (
#		'security',
#		'wms',
#		'wms-portal',
#		'auditor',
#		'entityinfo',
#		'contact',
#		'notification',
#		'tools',
#		'scheduler',
#		'lms',
#		'portal',
#		'discount-engine',
#		'payment-service',
	);    

    /**
     * Set this to false when you want to disable emails
     */
    private static $sendMails = false;
    
    private static function getTargets($service){
        /*if(empty($_SERVER['SERVER_NAME'])){*/
            $server = 'localhost';
        /*}
        else{
            $server = $_SERVER['SERVER_NAME'];
        }*/

        
        $urls = array(
            security => array("http://$server:7008/myntra-security-service/platform/security/users/1" => 400),
            wms => array("http://$server:7007/myntra-wms-service/platform/skus/5500" => 400),
            wms-portal => array("http://$server:7006/myntra-wms-service/platform/skus/5500" => 400),
            auditor => array("http://$server:7001/myntra-auditor-service/platform/audits/types/com.myntra.wms.entities.Lot/entities/1" => 250),
            entityinfo => array("http://$server:7002/myntra-entityinfo-service/platform/entityinfo/attachment/1" => 250),
            contact => array("http://$server:7003/myntra-contact-service/platform/contact/contactaddress/3" => 250),
            notification => array("http://$server:7004/myntra-notification-service/platform/notification/email/1" => 250),
            tools => array("http://$server:7009/myntra-tools-service/platform/tools/states/1" => 200),
            scheduler => array("http://$server:7005/myntra-scheduler-service/platform/scheduler/jobs/1" => 200),
            lms => array("http://$server:7050/myntra-lms-service/deliveryCenter/1" => 200),
            portal => array("http://$server:80/calendar.html" => 200),
            discount-engine => array("http://$server:8080/index.php" => 200),
            payment-service=> array("http://$server:8080/payments/health/check" => 200),
        );
        
        if($service == null || $service == ""){
            $targets = array();
            foreach($urls as $targetGroups){
                $targets = array_merge($targets, $targetGroups);
            }
            return $targets;
        }else return $urls[$service];
    }
    
    public static function hitTargets($service){
	if(in_array($service, Tester::$servicesToBlock)){
		header("HTTP/1.1 404");
		return;
	}
        $mailReceiver="engg_erp@myntra.com";
        //$mailReceiver="sourabh.agarwal@myntra.com";
        $from = "From:erp@myntra.com";
        
        //Fetch the previous Fail Statistics
        $failStats = parse_ini_file("tester.ini");
        
        //Open and truncate file
        $file = fopen("tester.ini", "w+");
        
        $targets = self::getTargets($service);
        if($targets){
            foreach($targets as $url => $timeout){
                $startTime = microtime(true) * 1000;
                $result = self::hitATarget($url, $timeout);
                $endTime = microtime(true) * 1000;
                
                //Service took more than 5 times then what was assigned to it
                $responseTime = round($endTime - $startTime, 1);
                if($result === true &&  $responseTime > $timeout * 5){
                    $result = 404;
                }
                
                if($result !== true){ // Failed
                    if($file){
                        //Write the fail statistic
                        fwrite($file, $url . "=" . ($failStats[$url] ? $failStats[$url] + 1 : 1));
                    }
                    
                    if(Tester::$sendMails === true){
                        $subject = "LB Health Check failed for " . $service . " on " . gethostname();
                        $body = $url ." failed on " . gethostname() . "\n"
                                    . "Response Time recorded : " . $responseTime . " millisecs\n"
                                    . "Failed at : " . date("d-M-Y H:i:s");
                        mail($mailReceiver, $subject, $body, $from);
                    }
                    header("HTTP/1.1 $result");
                }else{
                    if($failStats[$url] && Tester::$sendMails === true){ // If it failed at the previous attempt
                        $subject = "LB Health Check recovered for " . $service . " on " . gethostname();
                        $body = $url ." recovered on " . gethostname() . "\n"
                                . "Response Time recorded : " . $responseTime . " millisecs\n"
                                . "Recovered at : " . date("d-M-Y H:i:s");
                        mail($mailReceiver, $subject, $body, $from);
                    }
                }
            }
        }
        
        if($file)
            fclose($file);
    }
    
    private static function hitATarget($url, $timeout){
        $ch = curl_init();
        
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible;)");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPGET, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
        curl_setopt($ch, CURLOPT_TIMEOUT, 3);

        $authheaders = 'Authorization: Basic '.base64_encode("Heartbeat User"."~"."dummy_heartbeat". ':' ."dummy_heartbeat_pass");
        $contentheaders = 'Content-Type: application/xml';
        $acceptlanguage = 'Accept-Language: en-us';
        $accept = 'Accept: application/json';
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("$authheaders","$acceptlanguage","$accept", "$contentheaders")); 
        
        $response = curl_exec($ch);
        
        $info = curl_getinfo($ch);
        
//        if ($response === false || $info['http_code'] != 200) {
//              return "No cURL data returned for $url [". $info['http_code']. "]";
//        if (curl_error($ch))
//            return $response . "\n". curl_error($ch);
//        }
        if($info['http_code'] == 0){
            return 404;
        }else if ($response === false || $info['http_code'] != 200) {
              return $info['http_code'];
        if (curl_error($ch))
            return 404;
        }
        
        curl_close($ch);

        return true;
    }
}

Tester::hitTargets($_REQUEST['service']);
