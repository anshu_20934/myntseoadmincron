<?php

include_once($xcart_dir."/include/class/oms/OrderProcessingMailSender.php");

use enums\cart\CartContext;
use revenue\payments\gateway\AtomGateway;
use revenue\payments\gateway\CODGateway;
use revenue\payments\gateway\AxisBankGateway;
use revenue\payments\dao\AxisBankDAO;
use revenue\payments\gateway\CCAvenueGateway;
use revenue\payments\gateway\EBSGateway;
use revenue\payments\gateway\HDFCBankGateway;
use revenue\payments\gateway\ICICIBankGateway;
use revenue\payments\gateway\CITIBankGateway;
use revenue\payments\gateway\TekProcessGateway;
use revenue\payments\gateway\BillDeskGateway;

use enums\revenue\payments\CardType;
use enums\revenue\payments\PaymentType;
use enums\revenue\payments\Gateways;

$adapter = CouponAdapter::getInstance();

$redirectToInCaseOfReload = $http_location;

if($_GET['xid']!="") $session_id = $_GET['xid'];
else $session_id = $XCARTSESSID;

if(isset($_GET['admincodloginid'])) {
	$login = $_GET['admincodloginid'];
}

if(empty($giftCardOrderId)) {
	func_header_location($redirectToInCaseOfReload,false);	
}

$orderid=mysql_real_escape_string($giftCardOrderId);
$orderid = strip_tags($orderid);
$orderid = sanitize_int($orderid);
$paymentPageUrlToRedirectTo=$https_locations . "/mkgiftcardpaymentoptions.php?transaction_status=N";

$order_result = \GiftCardsHelper::getGiftCardOrderById($orderid);
$order_result=objectToArray($order_result);

if(empty($login)){
    $errormessage = "Ooops! There was an error in order processing and hence your order could not be placed. If you have been charged, the charged amount would be refunded back to your banking or credit card account within 2 working days. Please contact support@myntra.com for more details.";
    $XCART_SESSION_VARS['errormessage'] = $errormessage;
	func_header_location($redirectToInCaseOfReload,false);
	exit;
}

$mymyntra = new MyMyntra(mysql_real_escape_string($login));
$customer = $mymyntra->getBasicUserProfileData($login);
$customerFName = $customer["firstname"];
$customerMobileNo = $customer["mobile"];

if(empty($customerFName)){
	$customerFName = $order_result["giftCard"]["senderName"];
}

$orderLogin = $order_result["login"];
if(strcasecmp($orderLogin, $login) !=0) {
	$errormessage = "We are sorry, You are trying to access wrong order id.";
    $XCART_SESSION_VARS['errormessage'] = $errormessage;	    
    func_header_location($https_location . "/mkgiftcardpaymentoptions.php?transaction_status=N&loginincorrecto=yes");
    exit;
}
else{
	$orderLogDetails = func_query_first("select * from mk_payments_log where orderid = $orderid");
	$gatewayWentTo = $orderLogDetails["payment_gateway_name"];
	$paymentoption = $order_result["paymentOption"];//paymentDO
	$total = $order_result["total"];
	$subTotal = $order_result["subTotal"];
	$prev_status = ($order_result["status"] == "Queued")?"PP":"PP";
	$couponCode = "";//$order_result[0]['coupon']; - No coupon usage
	$cashCouponCode = $order_result["cashCouponCode"];
	$cashdiscount = $order_result["cashRedeemed"];
	$codCharge = $order_result["codCharge"];//paymentDO - No Cod Charge - No cod option for gifting
	$order_channel= "web";//$order_result[0]['channel'];//web always
	$productDiscount = $amountdetail['discount'];
	$cartDiscount = $amountdetail['cartDiscount'];
	$pg_discount = $amountdetail['paymentGatewayDiscount'];
	$emi_charge = $amountdetail['paymentSurcharge'];
	$isPaymentDataCorrect = false;
	$isPaymentSuccessfull = false;
	//gift charges, coupon discount and payment surcharge not taken into account
	$amountToBePaid = $order_result["subTotal"] - $cashdiscount + $codCharge + $emi_charge  - $productDiscount - $cartDiscount - $pg_discount;
	$amountToBePaid = number_format($amountToBePaid, 2, ".", '');
	$amountToBePaid = $amountToBePaid < 1 ? 0 : $amountToBePaid; //less than 1re payment is not accepted by banks to making the amount to zero in case final amount falls below 1re.	
	$couponCashDetails = array('coupon' => "", 'coupon_discount' => 0, 'cash_coupon_code' => $cashCouponCode, 'cash_redeemed' => $cashRedeemed);
	$amountafterdiscount = $total + $codCharge;
	$amountafterdiscount = number_format($amountafterdiscount,2,".",'');
	include_once($xcart_dir."/mkOrderBookPaymentValidation.php");
	
	include_once("./include/func/func.mk_orderbook.php");
	include_once("./include/func/func.order.php");
	include_once("./include/func/func.mkcore.php");
	include_once("./include/func/func.mail.php");
	include_once("./include/func/func.sms_alerts.php");
	
	if($isPaymentValid) {		
		$payment_method = $paymentoption;
		/**
		 * Defined payment_method for few reasons.
		 * 1. this code is already buggy
		 * 2. $paymentoption is being assigned orderstatus in some cases.
		 */
		
		if($paymentoption == "NP"){//if no payment(order amount 0 by discount or coupon discount)
		   	$orderstatus = "Q";
		   	$smarty->assign("paybyoption",$orderstatus);
		}else{
			if($gateway->isPaymentFlagged()){
				//set status on hold because the transaction is marked as flagged by EBS
				$orderstatus = "OH";
					
				//insert appropriate comment
				$commentArrayToInsert = Array ("orderid" => $orderid,
			      								"commenttype" => "Automated", 
			      								"commenttitle" => "status change", 
			      								"commentaddedby" => "admin", 
			      								"description" => "putting the order on hold as payment is flagged at ebs", 
			      								"commentdate" => time());
			    func_array2insert("mk_ordercommentslog", $commentArrayToInsert);
				    
			    //setting status as Y so that comments are visible
			    $sql = "UPDATE ".$sql_tbl['orders']." SET changerequest_status ='Y'  WHERE orderid = '".intval($orderid)."' ";
				db_query($sql);
				
				//send mail to CC regarding order put on hold
				func_ebs_flagged_put_on_hold_mail($orderid, $toLog['gateway_payment_id']);
			} else {
				$orderstatus = "Q";
				$smarty->assign("paybyoption",$orderstatus);
			}
		
			if($gatewayWentTo==Gateways::AxisBank) {
				$bin_number = $orderLogDetails['bin_number'];
				$putInternationalTransactionOnHold = FeatureGateKeyValuePairs::getBoolean('payments.intlTransOnHold',false);
				$vpc_verStatus3DS = $axisBankDAO->getVer3DSStatus();
				if($putInternationalTransactionOnHold && !bintest($bin_number)&&$vpc_verStatus3DS!='Y') {
					//suspected international transaction					
					$orderstatus ='OH';
					$smarty->assign("paybyoption",$orderstatus);
					//insert appropriate comment
					$commentArrayToInsert = Array ("orderid" => $orderid,
				      								"commenttype" => "Automated", 
				      								"commenttitle" => "status change", 
				      								"commentaddedby" => "admin", 
				      								"description" => "Order on hold as it is a international transaction and has to be manually vefiried by fraud detection team.", 
				      								"commentdate" => time());
				    func_array2insertWithTypeCheck("mk_ordercommentslog", $commentArrayToInsert);
				    
				    //setting status as Y so that comments are visible
				    $sql = "UPDATE ".$sql_tbl['orders']." SET changerequest_status ='Y'  WHERE orderid = '".intval($orderid)."' ";
					db_query($sql);
					$vpc_receiptNo = $axisBankDAO->getReceiptNumber();
					$vpc_3DSStatusDescription = $axisBankDAO->get3DSStatusDescription();
					$vpc_secureLevel3DS = $axisBankDAO->get3DSSecurityLevel();
					$vpc_enrolled3DS = $axisBankDAO->get3DSEnrolled();
					$vpc_status3DS = $axisBankDAO->get3DSStatus();
					$vpc_AVSResultCode = $axisBankDAO->getAVSResultCode();
					$vpc_AVSResultDescription = $axisBankDAO->getAVSResultDescription();
					$vpc_vCSCResultCode = $axisBankDAO->getCSCResultCode();
					$vpc_CSCResponseDescription = $axisBankDAO->getCSCDescription();
					
					$international_trans_on_hold_mail_body = "This order is put on HOLD as it is a international transaction. Please do a verification and queue the order manually.<br/> 
															  Futher details are: <br/>
																	Order ID: $orderid <br/>
																	Customer Name: $customerFName <br/>  
																	RRN (Reciept Number) : $vpc_receiptNo <br/>
																	3DS Verification Status: $vpc_verStatus3DS <br/>
																	3DS Verification Description: $vpc_3DSStatusDescription <br/>
																	ECI Code: $vpc_secureLevel3DS <br/>
																	Card 3DSEnrolled: $vpc_enrolled3DS <br/>
																	3DS Status: $vpc_status3DS <br/>
																	AddressVerificationCode: $vpc_AVSResultCode <br/>
																	AddressVerificationResult: $vpc_AVSResultDescription <br/>
																	Card Security Code Match Status: $vpc_vCSCResultCode <br/>
																	Card Security Code Match Description: $vpc_CSCResponseDescription <br/>
															For even futher details about the codes refer to Mastercard VPC Integration Reference";
					
					if($configMode=="release"||$configMode=="test" ||$configMode=="local") {
						$sendIntTransMailTo = "engg_qa@myntra.com,mitesh.gupta@myntra.com";
					} else {
						$sendIntTransMailTo = "payments.team@myntra.com";
					}
			    		
					$mail_details = array( 
						"to" => "$sendIntTransMailTo",
						"subject" => "International Transaction: $orderid",
						"content" => $international_trans_on_hold_mail_body,
						"mail_type" => MailType::CRITICAL_TXN
					);
					$multiPartymailer = new MultiProviderMailer($mail_details);
					$multiPartymailer->sendMail();
				}				
			}
		}

		#
		#Call function to load the customer details
		#
		//$customer = $order_result[0];//func_customer_order_address_detail($login, $orderid);
		$CustomerFirstName = $customer['s_firstname'];
		$CustomerLastName = $customer['s_lastname'];
		
		#
		#Call function to create the array of all prodcuts of that order
		#
		$mcartFactory= new MCartFactory();
		$myCart= $mcartFactory->getCurrentUserCart(false,CartContext::GiftCardContext);

		$amountdetail = $order_result;
		$order_time = null;//ordertime::get_order_time_string($orderid);
		$order_time_data = null;//ordertime::get_order_time_data($orderid);

		#
		#Check if payment option is not check payment then only coupon status will be updated
		#
		if($paymentoption != "chq" && $paymentoption != "cod" && $paymentoption != "NP"){
			#
			#Call function to check if coupon code id set then update no of times, coupon used by the 
			#customer
			#
			# Coupon updating section moved to mkorderinsert.php
			if($paymentoption == "on") {
				if(!empty($cashCouponCode) && $prev_status != 'Q' && !empty($cashdiscount)) {
					$userName = $XCART_SESSION_VARS['login'];
					$subtotal_after_discount=$amountdetail['subTotal']-$amountdetail['coupon_discount'];
		            $adapter->updateUsageByUser($cashCouponCode,
		            							$userName,
		                                        $subtotal_after_discount,
		                                        $cashdiscount,
		                                        $orderid);
		
					// Unlock the coupon.
		            $adapter->unlockCouponForUser($cashCouponCode, $userName);
		        }dfs;
		 		if(!empty($amountdetail['cashRedeemed'])){
		        	$trs = new MyntCashTransaction($login,MyntCashItemTypes::ORDER, $orderid, MyntCashBusinessProcesses::CASHBACK_USED, 0, 0,$amountdetail['cashRedeemed'],  "Usage on order (Gift card) no. $orderid");
		        	$myntCashUsageResponse = MyntCashService::debitMyntCashForGiftCard($trs,$orderid);
		        }
			}			
			
			$emailto = $login;
			func_gift_card_order_confirm_mail($order_result, $customer);
			
			$giftCardId = $order_result["giftCard"]["id"];
			\GiftCardsHelper::setGiftCardOrderComplete($giftCardId);
			\GiftCardsHelper::sendEmailForGiftCard($giftCardId);
			
			$msg = "Dear ".$customerFName.",Your order ".$orderid." has been placed successfully. Thank you www.myntra.com" ;			
	        //func_send_sms($customerMobileNo,$msg); 
		}

		$invoiceid = func_generate_invoice_id($orderid, $orderstatus, $session_id,$process_cashback,$amountdetail);
		
		$smarty->assign("productsInCart",$productsInCart);
		$smarty->assign("grandTotal",number_format($subTotal,2,".",''));
		$smarty->assign("coupondiscount",number_format($coupondiscount,2,".",''));
		$smarty->assign("couponCode",$couponCode);
		$smarty->assign("totalDiscount",number_format($productDiscount+$coupondiscount+$pg_discount+$cartDiscount,2,".",''));
		$smarty->assign("giftamount",0);
		$smarty->assign("emi_charge",number_format($emi_charge,2,".",''));
		$smarty->assign("userinfo", $customer);
		$smarty->assign("shippingRate", 0);
		$smarty->assign("codCharges", 0);
		$smarty->assign("vat", 0);
		$smarty->assign("ref_amount", 0);
		
		if($paymentoption == "chq" || $paymentoption == "cod" || $paymentoption == "NP"){
			$amount = ($amountdetail['ref_discount'] > '0')?$amtAfterRefDeducton:$amountafterdiscount;
			$msg='';
				
		    if($paymentoption == "NP"){
		        \OrderProcessingMailSender::notifyOrderConfirmation('on',$productsInCart,$amountdetail,$CustomerFirstName,$login,$orderid,$customer);
		        $msg =  "Dear ".$customerFName.",Your order:".$orderid." has been placed successfully.";
		    }		        
		}
			
		$weblog->info("sms on order confirmation order id :: $orderid, mobile :: $customerMobileNo, msg :: $msg");
		//TODO: We are using cellnext sms api for COD confirmation. This sms api has to be tested throughly for other cases too.
		func_send_sms_cellnext($customerMobileNo,$msg);
		
		//retargeting tracking code
		$retargetPixel = retargetingPixelOnType($productsInCart,'PID');
		if(!empty($retargetPixel)){
		    $smarty->assign("retargetPixel",$retargetPixel);    
		}
		//capturing channel(utm_source and utm_medium) info for order
		$eventInfo = array('event_type'=>'order');
	           
	    //to capture an identifiable int value for tracking to optimize audit
	    if(is_int($orderid) || is_numeric($orderid)){
	        $eventInfo += array('event_value'=>$orderid);
	    }
		utmTrackForEvent($eventInfo);
			
		$smarty->assign("amountafterdiscount", number_format($amountafterdiscount,2,".",''));
		$smarty->assign("amtAfterRefDeducton", number_format($amtAfterRefDeducton,2,".",''));
		$smarty->assign("mobile", $customer['mobile']);
		$smarty->assign("address", stripslashes($customer['s_address']));
		$smarty->assign("pincode", $customer['s_zipcode']);
		$smarty->assign("phone", $customer['phone']);
		$smarty->assign("fax", $customer['fax']);
		$smarty->assign("email", $customer['login']);
		$smarty->assign("paymentoption", $paymentoption);
		
		$smarty->assign("djvalue", number_format($amountdetail['total'],2,".",''));
		$smarty->assign("qtyInOrder", $amountdetail['qtyInOrder']);
		
		//setting order details to display
		$today = date("d-m-Y");
		$smarty->assign("today",$today);
		$smarty->assign("invoiceid",$invoiceid);
		$smarty->assign("orderid",$orderid);
		$smarty->assign("payment_method",$payment_method);
			
		// Cleanup all the unnecessary images
		$masterCustomizationArrayD=$XCART_SESSION_VARS['masterCustomizationArrayD'];
			
		//Need to track here as cart will be deleted after this.
		/*if(!is_null($tracker->confirmationData)){
			$tracker->confirmationData->setOrderId($orderid);
			$tracker->confirmationData->setPaymentData(array_merge($orderLogDetails, $toLog,$couponCashDetails));
			if(!empty($productArray)) {
				$tracker->confirmationData->setCartData($productArray);
			}
		}*/
		
		MCartUtils::deleteCart($myCart);
		x_session_unregister('productids'); 
		x_session_unregister('cart'); 
		x_session_unregister('coupondiscount');
		x_session_unregister('couponCode');
		x_session_unregister('cashdiscount');
		x_session_unregister('cashCouponCode');
		x_session_unregister('userCashAmount');
		x_session_unregister('productDiscount');
		x_session_unregister('masterCustomizationArrayD');
		x_session_unregister('offers'); 
		x_session_unregister('totaloffers'); 
		x_session_unregister('totaldiscountonoffer'); 
		x_session_unregister('OrderTotal');
		x_session_unregister('giftdata');
		x_session_unregister('totalQuantity');
				
		x_session_unregister('issuer_voucher_code');//unregistering brandstore voucher code
		x_session_unregister('order_id'); //unregistering the order id
		x_session_unregister("gcorderID");
		
		/*if(x_session_is_registered("lastAddressUsedID")){
			x_session_unregister("lastAddressUsedID");
		}*/
		$uuid = stripslashes(getMyntraCookie('MYNTRA_SHOPPING_ID'));
		setMyntraCookie("MYNTRA_UNQ_COOKIE_ID",substr($uuid,1,20),time() - 3600,'/',$cookiedomain);
		if(x_session_is_registered("chosenProductConfiguration"))
			x_session_unregister("chosenProductConfiguration");
			
		if (x_session_is_registered("masterCustomizationArray"))
			x_session_unregister("masterCustomizationArray");
			
		if (x_session_is_registered("masterCustomizationArrayD"))
		  	x_session_unregister("masterCustomizationArrayD");
			 
		$orderstatus = "Y";
		$smarty->assign("orderstatus",$orderstatus);		
	}
	
	if(!$isPaymentDataCorrect) {
		$message = "Your order has been rejected.";
		$orderstatus = "N";
	    $productsInCart = func_create_array_products_of_order($orderid, $productsInCartTemp);
	    $amountdetail=$order_result;
	    //$sql = "SELECT subtotal,firstname,issues_contact_number, coupon_discount, coupon AS couponcode,discount+cash_discount, cart_discount,shipping_cost, tax, ordertype, ref_discount, gift_status,gift_charges, total, cod, qtyInOrder, additional_info,cash_redeemed,cash_coupon_code FROM $sql_tbl[orders] WHERE orderid = '".$orderid."'";
		//$amountdetail = func_query_first($sql);
	    $couponCode = "";
		$coupondiscount = 0;
		$productDiscount = $amountdetail['discount'];
		$pg_discount = $amountdetail['paymentGatewayDiscount'];
		$emi_charge = $amountdetail['paymentSurcharge'];
        $cartDiscount = $amountdetail['cartDiscount'];
	    $amountafterdiscount = $amountdetail['total'] + $amountdetail['codCharge'];
	    $smarty->assign("grandTotal",number_format($amountdetail['subTotal'],2,".",''));
		$smarty->assign("coupondiscount",number_format($coupondiscount,2,".",''));
		$smarty->assign("totalDiscount",number_format($coupondiscount+$productDiscount+$pg_discount+$cartDiscount,2,".",''));
		$smarty->assign("couponCode",$couponCode);
		$smarty->assign("giftamount",number_format($amountdetail['gift_charges'],2,".",''));
	    $smarty->assign("productsInCart",$productsInCart);
	    $smarty->assign("orderid",$orderid);
		$smarty->assign("orderstatus",$orderstatus);
	    $smarty->assign("qtyInOrder", $amountdetail['qtyInOrder']);
	    $smarty->assign("shippingRate", 0.00);
	    $smarty->assign("amountafterdiscount", number_format($amountafterdiscount,2,".",''));
		//$sql = "UPDATE ".$sql_tbl['orders']." SET status ='F',response_server='$server_name'  WHERE orderid = '".intval($orderid)."' ";  ### Update the table in case fraud happened in EBS gateway
		// TODO to log failure status and from which server it happened
		//db_query($sql);
		
		$paymentOptionSelected = $orderLogDetails['payment_option'];
		$paymentIssuer = $orderLogDetails['payment_issuer'];
		$amountToBeTaken = $amountToBePaid;
		$amountAcutallyPaid = $gateway->getAmountPaid();
		$responseMessageFromGateway = $gateway->getResponseMessage();
		$responseCodeFromGateway = $gateway->getResponseCode();
	    $mail_detail = array(
	                        "to"=>'alrt_ebs_fraud_attempt@myntra.com',
	                        "subject"=>"Urgent! Possible user fraud for Orderid:$orderid",
	                        "content"=>"Based on payment gateway verification a suspect order has been placed by $customerFName  $customerMobileNo, orderID = $orderid. 
	                        This order has been declined and will not be processed. 
	                        		Further Details are: 
	                        		Payment Gateway: $gatewayWentTo 
	                        		Payment Option Selected: $paymentOptionSelected 
	                        		Payment Issuer: $paymentIssuer 
	                        		Amount to be paid: $amountToBeTaken 
	                        		Amount as returned by Gateway: $amountAcutallyPaid
	                        		Gateway Response Code: $responseCodeFromGateway
	                        		Gateway Response Message: $responseMessageFromGateway",
	                        "from_name"=>'Myntra Admin',
	                        "from_email"=>'admin@myntra.com',
	                        "header"=>'',
	                    );
	    if(!empty($couponValidationFailedMessage)){
	    	$mail_detail['content'].="Extra:$couponValidationFailedMessage";
		}
		send_mail_on_domain_check($mail_detail);
		$redirectURL = $https_location . "/mkgiftcardpaymentoptions.php?transaction_status=N";
		$smarty->assign("transaction_status","N");
		$smarty->assign('redirect_url',$redirectURL);
		$smarty->assign('redirect_message',"Payment Failed");
		func_display("cart/orderProcessing.tpl", $smarty);
		exit;
	}
	if(!$isPaymentSuccessfull){
		$card_bank_name = null;
		$cvvToRespond = null;
		$vbvFail = false;
		if($gatewayWentTo==Gateways::ICICIBank) {
			$vbvStatus = $gatewayResponse['status'];
			if($vbvStatus!='Y'){
				// Verification of card has failed.
				$vbvFail=true;
				$issuing_bank = $orderLogDetails['card_bank_name'];
				$card_bank_name = func_query_first_cell("select card_bank_code from mk_cards_domestic_bins where bank_name='$issuing_bank' limit 1");
			} else {
				//Verification of card is successfull but transaction denied due to other reasons.
				 $cvvStatus = $gatewayResponse['gateway_cvv_response_code'];
				 if(!empty($cvvStatus)){
				 	if($cvvStatus=='C'){
				 		//Card expired
				 		$cvvToRespond = "expired";
				 	}
				 }
			}		
		}
		$failureRedirectURL = $https_location . "/mkgiftcardpaymentoptions.php?transaction_status=N";
		$smarty->assign("transaction_status","N");
		if($vbvFail) {
			$failureRedirectURL = $failureRedirectURL . "&vbv=F";
			$smarty->assign("vbvfail",$vbvFail);
		}
		if(!empty($card_bank_name)) {
			$failureRedirectURL = $failureRedirectURL . "&bank_name=$card_bank_name";
		}
		$message = "We are sorry, your payment has not been successful.Please <a href='".$http_location."/mkgiftcardpaymentoptions.php?pagetype=productdetail'>click here</a> to try again";
		$orderstatus = "N";
		$smarty->assign("orderstatus",$orderstatus);
		//$sql = "UPDATE ".$sql_tbl['orders']." SET tf_status ='TF',status='D',response_server='$server_name'  WHERE orderid = '".intval($orderid)."' ";  ### Update the table in case transaction get failed
		// TODO to log failure status and from which server it happened 
		//db_query($sql);
		$userName = $XCART_SESSION_VARS['login'];
		//unlock the coupon in case transaction is declined
		if(!empty($couponCode) && $prev_status != 'Q') {	    
			// Unlock the coupon.
			$adapter->unlockCouponForUser($couponCode, $userName);
		}
			
		if(!empty($cashCouponCode) && $prev_status != 'Q') {
			// Unlock the coupon.
			$adapter->unlockCouponForUser($cashCouponCode, $userName);
		}
		$redirectURL = $failureRedirectURL;
		$smarty->assign('redirect_url',$redirectURL);
		$smarty->assign('redirect_message',"Payment Failed");
		func_display("cart/orderProcessing.tpl", $smarty);
		exit;
	}
	$smarty->assign("message",$message);
	
	//$delivery_date = date("jS M Y", getOrderDeliveryDateInTimestamp($order_result[0]['date']));
	//$smarty->assign("delivery_date",$delivery_date);
	$smarty->assign("login_email",$login);
	//echo "here1";exit;
	$redirectURL = $https_location . "/giftCardConfirmation.php?orderid=$orderid";
	$smarty->assign('redirect_url',$redirectURL);
	$smarty->assign('redirect_message',"Payment Successfull");
	func_display("cart/orderProcessing.tpl", $smarty);
	
	if(extension_loaded('newrelic')){
    	newrelic_custom_metric ('orders', 1.0);
	}
			
	/*$tracker->fireRequest();*/
}
?>
