<?php
include_once("./auth.php");
if ($skin == 'skin2') {
    header('Location: faqs#returns');
    exit;
}

include_once("$xcart_dir/include/class/widget/class.widget.keyvaluepair.php");

$pickup_charges = WidgetKeyValuePairs::getWidgetValueForKey('returnPickupCharges');
$smarty->assign("pickupCharges", number_format($pickup_charges, 2));

$analyticsObj = AnalyticsBase::getInstance();
$analyticsObj->setStaticPageVars(AnalyticsBase::RETURN_POLICY);
$analyticsObj->setTemplateVars($smarty);
func_display("help/myntrareturnpolicy.tpl", $smarty);
?>