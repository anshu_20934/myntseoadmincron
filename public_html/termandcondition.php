<?php
require "./auth.php";

if ($skin == 'skin2') {
    header('Location: termsofuse#top');
    exit;
}

$analyticsObj = AnalyticsBase::getInstance();
$analyticsObj->setStaticPageVars(AnalyticsBase::TERM_CONDITIONS);
$analyticsObj->setTemplateVars($smarty);

func_display("termandcondition.tpl",$smarty);
?>