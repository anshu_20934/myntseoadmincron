<?php

// Date in the past
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
// always modified
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
// HTTP/1.1
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
// HTTP/1.0
header("Pragma: no-cache");

try {
	//error_log("bytes uploaded \r\n", 3, "c:/php_upload.log");
    $request_method = $_SERVER["REQUEST_METHOD"];
    if ($request_method == "GET"){
      $qvars = $_GET;
    } elseif ($request_method == "POST"){
      $qvars = $_POST;
    }

    $uploadID = trim($qvars["id"]);
    
	$tmp = uploadprogress_get_info($uploadID);
	
	$maxFileSize=1024*1024*3;//Limit the image upload to 1MB
    if ($tmp['bytes_total'] < 1) {
		$percent = 0;
	} else {
		$percent = round($tmp['bytes_uploaded'] / $tmp['bytes_total'] * 100, 0);
		print $percent;
	}
	     
} catch (Exception $e) {
	print "exception";
}	
?>
