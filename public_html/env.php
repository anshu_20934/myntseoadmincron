<?php

$configPattern = "env/*.config.php";
while(count($configFiles = glob($configPattern))==0){
	$configPattern = "../".$configPattern;
}

foreach($configFiles as $config_filename) {
  require_once($config_filename);
}


/* Temp location for assigning new config values for old ones
*/

//Host Config

$baseurl = HostConfig::$baseUrl;
$xcart_http_host = HostConfig::$httpHost;
$xcart_https_host = HostConfig::$httpsHost;
$xcart_web_dir = HostConfig::$webDir;
$document_root = HostConfig::$documentRoot;
$server_name = HostConfig::$serverName;
$configMode = HostConfig::$configMode;
$cdn_base = HostConfig::$cdnBase;
$css_path = HostConfig::$cssPath;
$js_path = HostConfig::$jsPath;
$combine_css_path = HostConfig::$combineCssPath;
$combine_js_path = HostConfig::$combineJsPath;
$brandshop_css_path = HostConfig::$brandshopCssPath;
$brandshop_js_path = HostConfig::$brandshopJsPath;
$facebook_app_id = HostConfig::$facebookAppID;
$facebook_app_secret = HostConfig::$facebookAppSecret;
$facebook_app_name = HostConfig::$facebookAppName;
$ga_acc = HostConfig::$gaAccountID;
$secure_cdn_base = HostConfig::$secureCdnBase;
$secure_css_path = HostConfig::$secureCssPath;
$secure_js_path = HostConfig::$secureJsPath;
$admin_js_path = HostConfig::$adminJsPath;
$tracking_system_url = HostConfig::$trackingSystemUrl;
$web_root = HostConfig::$webRoot;
$ftp_root = HostConfig::$ftpRoot;
$portal_service_host = HostConfig::$portalServiceHost;
$cookieprefix = HostConfig::$cookieprefix;
$cookiedomain = HostConfig::$cookiedomain;

//DB config
$sql_ro = DBConfig::$ro;
$sql_rw = DBConfig::$rw;

//SOLR config
$solr_servers = SOLRConfig::$servers;
$solr_port = SOLRConfig::$port;
$solr_version = SOLRConfig::$version;

//System config
//$system_free_shipping_amount = SystemConfig::$freeShippingAmount;
$system_amazon_access_key = SystemConfig::$amazonAccessKey;
$system_amazon_secret_key = SystemConfig::$amazonSecretKey;
$system_cleartrip_offer = SystemConfig::$cleartripOffer;
$system_online_payment_evoucher = SystemConfig::$onlinePaymentEvoucher;
$system_evoucher_minimum_amount = SystemConfig::$evoucherMinimumAmount;
$system_campaign_name = SystemConfig::$campaignName;
$system_users_publish= SystemConfig::$usersPublish;
$system_ptg_campaign= SystemConfig::$ptgCampaign;
$system_ptg_for_campaign= SystemConfig::$ptgForCampaign;
$system_ptg_campaign_threshold= SystemConfig::$ptgCampaignThreshold;
$system_ptg_campaign_threshold_calc= SystemConfig::$ptgCampaignThresholdCalc;

//Analytics config
$analytics_user = AnalyticsConfig::$user;
$analytics_pass = AnalyticsConfig::$pass;
$analytics_profile = AnalyticsConfig::$profile;
$analytics_duration = AnalyticsConfig::$duration;

// Catalogue Config
$catalogue_image_path = CatalogueConfig::$imagePath;

//CCAvenue config
$ccavenue_url = CCAvenueConfig::$url;
$ccavenue_merchant_id = CCAvenueConfig::$merchantId;

//IMint config
$imint_url = IMintConfig::$url;
$imint_conversion_rate = IMintConfig::$conversionRate;
$imint_lmid = IMintConfig::$lmid;
$imint_ltid = IMintConfig::$ltid;

//Order config
$order_check_option_enable = OrderConfig::$checkOptionEnable;

define("CONFIG_INITIALIZED",true);

?>
