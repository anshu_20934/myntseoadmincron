<?php

// used the superglobal $_GET to store the begin time, because, global variables are unset by the "xcart" init
$_GET['__timePageBegin__'] = microtime(true);
require_once __DIR__.'/auth.php';
$smarty->assign("imageFlag",$imageFlag_checkout);
$smarty->assign("signUpPoints",$signUpPoints_checkout);

$absolutServiceBase = isset($_SERVER['HTTPS'])
    ? str_replace('http://' . HostConfig::$httpHost, 'https://' . HostConfig::$httpsHost, HostConfig::$portalAbsolutServiceHost)
    : str_replace('https://' . HostConfig::$httpsHost, 'http://' . HostConfig::$httpHost, HostConfig::$portalAbsolutServiceHost);
require_once $xcart_dir."/components/widget_config.php";

//require_once $xcart_dir.'/include/ChromePhp.php';
//ChromePhp::log('Remote IP: ' . $_SERVER['REMOTE_ADDR']);

$assetsUrl = isset($_SERVER['HTTPS'])? \HostConfig::$secureCdnBase : \HostConfig::$cdnBase;

// Base Tracker (MongoDB)
require_once $xcart_dir."/include/class/instrumentation/BaseTracker.php";
include_once ("$xcart_dir/securesessioncheck.php");
if ($_SERVER['SCRIPT_NAME'] == '/cart.php') {
    $pageName = BaseTracker::CART_PAGE;
    $pageName = 'cart';
}
else {
    $isPromptLogin = isSecureSessionExpired();
    $pageName = BaseTracker::CHECKOUT_PAGE;
    $pageName = 'checkout';
}
include_once $xcart_dir."/webengage.php";
$tracker = new BaseTracker($pageName);
$tracker->fireRequest();


// Below two flags will be set, if in-page payment transaction happens (currently, it is only for COD)
// The flags are exported to javascript and used by the PaymentModule
$isPaymentTransactionFailed = isset($_GET['transaction_status']) && $_GET['transaction_status'] == 'N';
$isPromptLogin = $isPromptLogin || !empty($_GET['promptlogin']);

// flag to control the inclusion of the omniture analytics script in the page footer
// client will make an ajax call to complete the analytics based on the widget
$noInpageAnalytics = true;
$smarty->assign('noInpageAnalytics', $noInpageAnalytics);

$secureSessionExpireMessage = WidgetKeyValuePairs::getWidgetValueForKey('login.secureSessionExpireMessage');
$doFetchCouponsOnCart  = FeatureGateKeyValuePairs::getBoolean('doFetchCouponsOnCart');

//header('Content-Type:text/plain'); $f = get_included_files(); print_r($f); exit;
?>
<!DOCTYPE html>
<html>
<head>
    <?php require_once $xcart_dir."/components/secure_html_head.php" ?>
    <script>
    Myntra.Data.nocombo = "<?php echo HostConfig::$nocombo ?>";
    Myntra.Data.promptLogin = <?php echo $isPromptLogin ? "true" : "false" ?>;
    Myntra.Data.absolutServiceUrl = "<?php echo $absolutServiceBase ?>";
    Myntra.Data.isSecureCartEnabled = <?php echo $isSecureCartEnabled ? "true" : "false" ?>;
    Myntra.Data.vatTooltip = "<?php echo $vatToolTip ? $vatToolTip : false ?>";
    Myntra.Data.secureSessionExpireMessage = "<?php echo $secureSessionExpireMessage ?>";
    Myntra.Data.promoCoupons = "<?php echo $promoCoupons ? $promoCoupons : false ?>";
    Myntra.Data.promoCouponsText = "<?php echo $promoCouponsText ? $promoCouponsText : false ?>";
    Myntra.Data.promoCouponsVariant = "<?php echo $promoCouponsVariant ? $promoCouponsVariant : false ?>";
    Myntra.Data.checkoutPageUrl = "<?php echo 'https://'.HostConfig::$httpsHost.'/checkout.php' ?>";
    Myntra.Data.cartPageUrl = "<?php echo 'http://'.HostConfig::$httpHost.'/cart.php' ?>";
    Myntra.Data.fetchCoupons = <?php echo $doFetchCouponsOnCart ? "true" : "false" ?>;
    <?php if (isset($XCART_SESSION_VARS['payment_address'])): ?>
        Myntra.Data.selectedAddressId = <?php echo $XCART_SESSION_VARS['payment_address'] ?>;
    <?php endif ?>
    <?php if (isset($XCART_SESSION_VARS['mobile']) && strlen($XCART_SESSION_VARS['mobile']) > 0 ): ?>
        Myntra.Data.mobile = <?php echo $XCART_SESSION_VARS['mobile'] ?>;
    <?php endif ?>
    </script>
</head>
<body class="<?php if (isset($_SERVER['HTTPS'])) echo 'secure' ?> checkout">
    <?php $smarty->display('inc/gtm.tpl') ?>
    <?php require_once $xcart_dir."/components/login.php" ?>
    <div class="container">
        <?php require_once $xcart_dir."/components/checkout_header.php" ?>
        
        <div class="grid">
            <div class="cart-section">
                <div class="mk-checkout-header-container"></div>
                <div class="bagContainer cart-page">
                    
                </div>
                <div class="widget-overlay">
                    <img src="<?php echo $cdnBase ?>/skin2/images/loader_150x200.gif">
                </div>
            </div>
            <div class="wishlist-section hide">
                <div class="mk-checkout-header-container"></div>
                <div class="wishlistContainer cart-page"></div>
                <div class="widget-overlay">
                    <img src="<?php echo $cdnBase ?>/skin2/images/loader_150x200.gif">
                </div>
            </div>
            <div class="content hide">
                <div class="address-content hide" id="address-content">
                    <div class="address-section">
                        <div class="mk-checkout-header-container"></div>
                        <div class="add-edit-address-section hide"></div>
                        <div class="addressListContainer"></div>
                    </div>
                    <div class="widget-overlay">
                        <img src="<?php echo $cdnBase ?>/skin2/images/loader_150x200.gif">
                    </div>
                </div>
                <div class="payment-content" id="payment-content"></div>
            </div> 
            <div class="summary hide">
               <div class="address-summary hide" id="address-summary"></div>
               <div class="cart-summary hide" id="cart-summary"></div>
            </div>
        </div>

        <?php require_once $xcart_dir."/components/checkout_footer.php" ?>
    </div>

    <?php require_once $xcart_dir."/components/secure_html_foot.php" ?>
    <!--
    <script src="<?php echo $httpBase ?>/skin2/js/require.min.js"></script>
    -->
    <script>
        require.config({
            shim: {
                'backbone': {
                    deps: ['underscore'],
                    exports: 'Backbone'
                }
            },

            //baseUrl: "skin2/js/v2",
            baseUrl: '<?php echo "$httpBase/skin2/js/v2" ?>',

            paths: {
                underscore: '<?php echo "$cdnBase/skin2/js/underscore-1.4.4-min" ?>',
                backbone: '<?php echo "$cdnBase/skin2/js/backbone-1.0.0-min" ?>',
                text: '<?php echo "$cdnBase/skin2/js/text-2.0.3-min" ?>',
                controller: '<?php echo HostConfig::$nocombo ? "app/controller" : preg_replace('/\.js$/', '', $widgetConfig['checkout']) ?>',
                widgets: 'widgets'
            },

            
            waitSeconds: 15
        });

        require.configuration = require.s.contexts._.config;

        require(['controller'],function (controller) {
            controller.init();
        });

    </script>
    <?php $smarty->display('inc/webengage.tpl') ?>
</body>
</html>
<?php
if (isset($_GET['__timePageBegin__'])) {
    $handle = Profiler::startTiming('checkout_pagetime');
    $handle->setStartTime($_GET['__timePageBegin__']);
    Profiler::endTiming($handle);

    //$serverTime = microtime(true) - $_GET['__timePageBegin__'];
    //$weblog->debug("ServerTime: $serverTime ms URI: " . $_SERVER['REQUEST_URI'] . " SCRIPT: " . $_SERVER['SCRIPT_NAME']);
    //if (extension_loaded('newrelic')) {
    //    newrelic_custom_metric('Custom/ServerTime/' . $_SERVER['SCRIPT_NAME'], $serverTime);
    //}
}
?>
