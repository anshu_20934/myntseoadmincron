<?php
require "./auth.php";
require_once("class.imageconverter.php");
include_once("./include/func/func.mkcore.php");
include_once("./include/func/func.mkmisc.php");
include_once("./include/func/func.mkspecialoffer.php");
include_once("./include/func/func.randnum.php");
include_once("./include/func/func.mkimage.php");
require_once ("include/func/func.RRRedeemPoints.php");
include_once($xcart_dir."/modules/organization/orgaInIt.php");


$chosenProductConfiguration = $XCART_SESSION_VARS['chosenProductConfiguration'];
$masterCustomizationArray = $XCART_SESSION_VARS['masterCustomizationArray'];
if($chosenProductConfiguration["defaultCustomizationId"]){
  $defaultCustId = $chosenProductConfiguration["defaultCustomizationId"];
}

########## Load default orientation id 
if(!empty($chosenProductConfiguration["defaultOrientationId"])){
	$defaultOrientationId = $chosenProductConfiguration["defaultOrientationId"];
}
### Current orientation 
if($chosenProductConfiguration["currentOrientationId"]){
	$currentOrientationId = $chosenProductConfiguration["currentOrientationId"];
}


//Count number of  customization area
// Only Tshirt case is having two customization area later it will extend as the case would extend    
$custAreaCount = 0;
$j=0;

if(isset($chosenProductConfiguration['productId']) && !empty($chosenProductConfiguration['productId']))
{
     $defaultCustomizationAreaImageDetail = func_load_product_customized_area_rel_details($chosenProductConfiguration['productId'], $defaultCustId);
	 $ImagePath =   $defaultCustomizationAreaImageDetail[0]['image_portal_T'];
}
if($chosenProductConfiguration['productId'] == "")
{  
	// check for the paste image or customized txt for all orientation 
	$allOrientation = array();
	foreach($chosenProductConfiguration['productCustomizationAreas'] as $_k => $_custareainfo){
		$designOrientationDetailValue=func_get_orientation_for_customization_area($_custareainfo[0]); // load orientation for customization
		$imagePerCustArea = false;		
		foreach($designOrientationDetailValue as $_k => $_ortinfo){			
			$custInfo =  $masterCustomizationArray[$_ortinfo[6]];
			if(empty($custInfo[$_ortinfo[7]]['pasteimagepath']) && empty($custInfo[$_ortinfo[7]]['customizedtext']) ){
				   unset($masterCustomizationArray[$_ortinfo[6]][$_ortinfo[7]]);
			}else{				
				$allOrientation[$_custareainfo[0]][] = $_ortinfo[7];
				$imagePerCustArea = true;
			}
			
		}
		($imagePerCustArea) ? $custAreaCount = $custAreaCount + 1 : '' ;
	}
	x_session_register('masterCustomizationArray');	 // re register the the master array
	
	//generate the final images for customization area
	generateImagesForProducts($masterCustomizationArray,$chosenProductConfiguration);
	x_session_register('masterCustomizationArray');	

	$imageName='finalcustomizeThumbimage';	
	if(isRRPortal($orgId))
	    $imageName='generatedimage';	
	
	$ImagePath =   '';
	foreach($allOrientation as $_cid => $_orinfo){
		foreach($_orinfo as $val){
	           if($defaultCustId == $_cid){
	             	  if($defaultOrientationId == $val) {
	    	            $ImagePath =   $masterCustomizationArray[$defaultCustId][$defaultOrientationId][$imageName];
	    	             break 2;
	             	  }
	    	          else {
	    	             $ImagePath =   $masterCustomizationArray[$defaultCustId][$val][$imageName]; 
	    	             break 2;
	    	          }
	    	     
	    		}else{
	    			$ImagePath =   $masterCustomizationArray[$_cid][$val][$imageName]; 
	    			break 2;
	    		}
	    }
	}
}

//Load type of product type
$productStyleId =         $chosenProductConfiguration['productStyleId'];
$productStyleTable = $sql_tbl["mk_product_style"];
$queryString = func_query_first("select a.styletype as type,price  from $productStyleTable a where a.id='$productStyleId'");
$productStyleType = $queryString['type'];  
######## Modified for Ibibo #########
$actualPriceOfProduct =   $queryString['price'] ;
$productId =              $chosenProductConfiguration['productId'];
$productTypeId = 		  $chosenProductConfiguration['productTypeId'];
$productStyleName =       $chosenProductConfiguration['productStyleLabel'];
$quantity =               ($promoportal === true)? 1 : $chosenProductConfiguration['quantity']; 
$productPrice =           $chosenProductConfiguration['unitPrice'];  
$productTypeLabel =       $chosenProductConfiguration['productTypeLabel'];
$designImagePath =        $ImagePath;// $chosenProductConfiguration['uploadedImagePath'];
$optionsArray =           $chosenProductConfiguration['optionsArray'];
$sizenamesArray =		  $chosenProductConfiguration['sizename'];
$sizequantityArray =	  $chosenProductConfiguration['sizequantity'];
$totalPrice =       $quantity *   $productPrice;
$optionsNameArray = array_keys($optionsArray);
$optionsValueArray = array_values($optionsArray);
$optionsId= $chosenProductConfiguration['optionsId'];
$template_id = $chosenProductConfiguration['template_id'];
// get the selected the theme id 
//$template_id = $chosenProductConfiguration['selectedTheme'];
$selectedAddon = $chosenProductConfiguration["selectedAddon"];

if( $selectedAddon )
{
	$selectedAddonName = get_style_addon_name($selectedAddon);
	if( $selectedAddonName)
	{
		$productStyleName = $productStyleName . "($selectedAddonName)";
		$productStyleLabel = $productStyleName;
	}
}

$addTo = $_GET['at'];
// if product Id is empty means new product is being added to the cart
if (empty($productId)){
    //generate a unique id for the created product
    //TODO - to be changed later a good unique id generator program.
	$len = 4;
	$time = time();
	$timefirstdigit = substr($time, 0, 3);
	$timelastdigit = substr($time, -3);
    $productId = $timefirstdigit.get_rand_number($len).$timelastdigit;
}

$productTypeData = func_query_first("SELECT location, vat FROM $sql_tbl[mk_product_type] WHERE id='$productTypeId'");

$discount = $quantity * func_get_special_offer_discount_for_product($productId);

$productDetail =   array('productId' => $productId,'producTypeId' => $productTypeId
,'productStyleId' => $productStyleId,'productStyleName' => $productStyleName
,'quantity'=> $quantity ,'productPrice'=>$productPrice,'productTypeLabel' => $productTypeLabel
, 'totalPrice'=>$totalPrice,'optionNames'=>$optionsNameArray,'optionValues'=>$optionsValueArray
,'designImagePath'=>$designImagePath,'discount'=>$discount,'optionsId'=>$optionsId
,'defaultCustomizationId'=>$defaultCustId,'defaultOrientationId'=>$defaultOrientationId,'custAreaCount'=>$custAreaCount,
'productStyleType'=>$productStyleType,'sizenames'=>$sizenamesArray,'sizequantities'=>$sizequantityArray,
'actualPriceOfProduct' => $actualPriceOfProduct, 'operation_location' => $productTypeData['location'],
'vatRate'=>$productTypeData['vat'],'template_id'=>$template_id,'addon_id'=>$selectedAddon);

if(!empty($chosenProductConfiguration['promotion_id'])){
	if(($promotions = verify_promotion_tracking_id($chosenProductConfiguration['promotion_id'])) !== false ){
		$productDetail['promotion'] = $promotions;
	}
}
(!empty($chosenProductConfiguration['yahootpl'])) ? $productDetail['yahootpl']=$chosenProductConfiguration['yahootpl'] : '' ;

#declare an array
$productids = array();   

if(!x_session_is_registered('productsInCart'))
{
    
    x_session_register('productsInCart'); 
      
}
if(!x_session_is_registered('productids')){   
    x_session_register($productids);  
}

/* added by arun for orgid=23(ING MGM)
 * and for orgid=24 (ICICI)
 * to hold single item in cart always
 * as a gift to the ing customer
 */
if($promoportal === true){
	x_session_unregister('productsInCart');
	x_session_unregister('productids');
}

$productsInCart = $XCART_SESSION_VARS['productsInCart'];
$productids = $XCART_SESSION_VARS['productids'];
$productsInCart[$productId] = $productDetail;

x_session_register('productsInCart');  
if(!in_array($productId, $productids)){
	$productids[] = $productId;
	x_session_register('productids');
}

if(!x_session_is_registered('grandTotal')){
	x_session_register('grandTotal');      
}

$affiliateInfo = $XCART_SESSION_VARS['affiliateInfo'];
if(empty($affiliateInfo[0]['affiliate_id']) )
{
	$cartArray['cart'] = $productsInCart;
	$cartArray['products'] = $productids;
	$cartitems = serialize($cartArray['cart']);
	$cartproducts = serialize($cartArray['products']);
	$uuid = stripslashes(getMyntraCookie('MYNTRA_SHOPPING_ID'));
	if($uuid=="")
	{
		$uuid = mysql_result(mysql_query('Select UUID()'),0);
		$expiryDate = time() + $max_cart_validity_time;
		setMyntraCookie("MYNTRA_SHOPPING_ID",$uuid,$expiryDate,'/',$cookiedomain);
		setMyntraCookie("MYNTRA_UNQ_COOKIE_ID",substr($uuid,1,20),$expiryDate,'/',$cookiedomain);
	}
	store_cart_products($uuid,$login,$productsInCart,$productids);
}
#################################################################
for($i =  0; $i < count($productids) ;  $i++)
{
    if($productids[$i] ==$productId  )
    {
        $exist = true;
        break;
    }
}
if($exist != true){    
	$productids[] = $productId;
	x_session_register('productids');
}

$pagetype = $_GET['pagetype'];
header("location:mkmycartmultiple.php?at=".$addTo."&pagetype=".$pagetype);
?>
