<?php
require "auth.php";
include_once("$xcart_dir/include/func/func.mk_orderbook.php");
include_once("$xcart_dir/include/func/func.order.php");
include_once("$xcart_dir/include/func/func.mkcore.php");
include_once("$xcart_dir/modules/apiclient/WarehouseApiClient.php");
include_once("$xcart_dir/modules/apiclient/PincodeApiClient.php");
include_once("$xcart_dir/modules/apiclient/UniqueInvoiceApiClient.php");
include_once($xcart_dir."/include/class/class.html2fpdf.php");
include_once($xcart_dir."/class.imageconverter.php");

if($_SERVER['SERVER_NAME'] == 'www.myntra.com'){
	echo "<meta http-equiv=\"Refresh\" content=\"0;URL=$http_location \" >";
    exit;
}

$invoiceType = $_GET['invoiceType'];
$copies      = $_GET['copies'];
$smarty->assign('invoiceType', $invoiceType);
$smarty->assign('print', $_GET['print']);
$orderids = trim($_GET['orderids']);
$orderids = explode(",", $orderids);
$original_orders = $orderids;
$warehouses = WarehouseApiClient::getAllWarehouses();
$whId2warehouse = array();

foreach($warehouses as $warehouse) {
	$whId2warehouse[$warehouse['id']] = $warehouse;
}

$oldDB = "";
$orderSql = "select orderid from xcart_orders where orderid = " . $orderids[0];
$selectedOrder = func_query($orderSql);
if (!$selectedOrder || $selectedOrder == "") {
	$oldDB = "_old";
	$selectedOrder = func_query("select orderid from xcart_orders_old where orderid = " . $orderids[0]);
	if (!$selectedOrder || $selectedOrder == "") {
		echo "<h1>Shipment not found</h1>";
	}
}

$orders = func_select_orders_archived($orderids, array('F', 'D'));
$orderid2OrderMap=array();
$logins = array();
$group_ids = array();
foreach($orders as $order) {
	if(!in_array($order['login'], $logins))
		$logins[] = $order['login'];
	if(!in_array($order['group_id'], $group_ids))
		$group_ids[] = $order['group_id'];
	
	$orderid2OrderMap[$order['orderid']] = $order;
}

$login2Userinfo = array();
if(!empty($logins)) {
	$userinfos = func_query("Select * from $sql_tbl[customers] where login in ('".implode("','", $logins)."')");
	if(!empty($userinfos)) {
		foreach($userinfos as $userinfo) {
			$login2Userinfo[strtolower($userinfo['login'])] = $userinfo;
		}
	}
}
if(!empty($group_ids)){
$groupid2MoreOrders = array();
$other_orders = func_query("Select orderid, group_id from xcart_orders". $GLOBALS['oldDB'] . " where group_id in (".implode(",",$group_ids).") and status not in ('D', 'F')");
if($other_orders) {
        foreach($other_orders as $order) {
                if(!isset($groupid2MoreOrders[$order['group_id']]))
                        $groupid2MoreOrders[$order['group_id']] = array();

                $groupid2MoreOrders[$order['group_id']][] = $order;

                if(!in_array($order['orderid'], $orderids))
                        $orderids[] = $order['orderid'];
        }
	}
}

$weblog->info("OrderIds & OrderIds in Group - ". implode(",", array_keys($groupid2MoreOrders)));

$order2items = array();
$item_details = func_create_array_products_of_order_archived($orderids);
foreach($item_details as $item) {
	if(!isset($order2items[$item['orderid']]))
		$order2items[$item['orderid']] = array();
	
	$order2items[$item['orderid']][] = $item;
}
$invoices = array();
$counter =0;
foreach($original_orders as $oid) {
	$order = $orderid2OrderMap[$oid];
	if(!$login2Userinfo[strtolower($order['login'])] || !$order2items[$order['orderid']]) {
		continue;
	}
	$counter++;
	$invoice = array();	
	$invoice['invoiceid'] = $order['invoiceid'];
	$invoice['orderid'] = $order['orderid'];	
	$invoice['group_id'] = $order['group_id'];	
	$invoice['gift_status'] = $order['gift_status'];
	$invoice['shipping_method'] = $order['shipping_method'];
	
	if($invoiceType == 'special' && $order['date'] > 1396310400){
		$specialPincodeToWarehouseIdMap = json_decode(WidgetKeyValuePairs::getWidgetValueForKey('specialPincodeToWarehouseIdMap'), true);
		$specialPincodes = array_keys($specialPincodeToWarehouseIdMap);
		if(in_array($order['s_zipcode'], $specialPincodes)){
			$warehouseId = $specialPincodeToWarehouseIdMap["$order[s_zipcode]"];
			$warehouse = $whId2warehouse[$warehouseId];
                        $specialPincodeMap = json_decode(WidgetKeyValuePairs::getWidgetValueForKey('specialPincodeMap'), true);
                        $specialPincodeWhIds = array_keys($specialPincodeMap);
                        if(in_array($warehouseId, $specialPincodeWhIds)){
                            $map = $specialPincodeMap[$warehouseId];
                            $data = array();
                            foreach($map as $key => $value){
                                array_push($data, "$key: $value");
                            }
                            //For kerela orders, getting unique Invoice number

                            $uid = UniqueInvoiceApiClient::getUniqueInvoice($order['orderid'], $warehouseId);
                            if($uid == ''){
                                echo 'Unable to generate Unique Invoice Number';
                                exit;
                            }
                            $invoice['uniqueInvoiceId'] = $uid;
                            $invoice['specialPincodeData'] = $data;
                        }

		}else {
			$warehouse = $whId2warehouse[$order['warehouseid']];
		}
	} else {
		$warehouse = $whId2warehouse[$order['warehouseid']];
	}
	$invoice['warehouse'] = $order['warehouseid'];
	$invoice['weight'] = $order['weight'];
	$invoice['warehouse_tin'] = $warehouse['tinNumber'];
	$address = explode("\n", $warehouse['address']);
	
	if($invoiceType == 'finance'){
		$invoice['companyName'] = "Myntra Designs Pvt Ltd.";
		if($order['warehouseid'] == 1){
			$invoice['warehouse_addressline1'] = "3rd Floor AKR Teck Park";
			$invoice['warehouse_addressline2'] = "7th Mile, Krishna Reddy Industrial Area, Kudlu Gate";
			$invoice['warehouse_city'] = "Bangalore";
			$invoice['warehouse_zipcode'] = "560068";
			$invoice['warehouse_state'] = "Karnataka";
			$invoice['warehouse_tin'] = "29910754899";
		}
		else{
			$invoice['warehouse_addressline2'] = $address[0]." ".$address[1]." ".$address[2];
			$invoice['warehouse_city'] = $warehouse['city'];
			$invoice['warehouse_zipcode'] = $warehouse['postalCode'];
			$invoice['warehouse_state'] = $warehouse['state'];
			$invoice['warehouse_tin'] = "06391939110";
		}
		$invoice['warehouse_country'] = $warehouse['country'];
		$invoice['firstname'] = "Vector E-Commerce Pvt Ltd.";
		$invoice['address'] = $address[0]." ".$address[1]."\n".$address[2];
		$invoice['city'] = $warehouse['city'];
		$invoice['zipcode'] = $warehouse['postalCode'];
		$invoice['state'] = $warehouse['state'];
		$invoice['country'] = $warehouse['country'];
		$invoice['tinNumber'] = $warehouse['tinNumber'];;
		$invoice['invoiceid'] = "MYN".substr($invoice['invoiceid'], 7);
	}else{
		$invoice['companyName'] = "Vector E-Commerce Pvt Ltd.";
		$invoice['warehouse_addressline1'] = $address[0]." ".$address[1];
		$invoice['warehouse_addressline2'] = $address[2];
		$invoice['warehouse_city'] = $warehouse['city'];
		$invoice['warehouse_zipcode'] = $warehouse['postalCode'];
		$invoice['warehouse_state'] = $warehouse['state'];
		$invoice['warehouse_country'] = $warehouse['country'];
		$invoice['firstname'] = $order['s_firstname'];
		$invoice['lastname'] = $order['s_lastname'];
		$invoice['address'] = nl2br($order['s_address']);
		$invoice['locality'] = $order['s_locality'];
		$invoice['city'] = $order['s_city'];
		$invoice['zipcode'] = $order['s_zipcode'];
		$invoice['state'] = $order['s_statename'];
		$invoice['country'] = $order['s_countryname'];
		$invoice['phone'] = $order['mobile'];
		$userinfo = $login2Userinfo[$order['login']];
		$invoice['customer_phone'] = $userinfo['mobile'];
	}
	
	$itemsinorder = $order2items[$order['orderid']];

	if($order['queueddate'] != null) {
            if($order['shipping_method'] == 'XPRESS') {
		$invoice['promiseDate'] = date("dm", getDeliveryDateForOrder($order['queueddate'], $itemsinorder, $order['s_zipcode'], $order['payment_method'],$order['courier_service'], $order['warehouseid'], $order['orderid']));
            } else {
                $invoice['promiseDate'] = date("dm", getDeliveryDateForOrder($order['queueddate'], $itemsinorder, $order['s_zipcode'], $order['payment_method'],$order['courier_service'], $order['warehouseid'], $order['orderid']));
            }
	}
	
	$invoice['payment_method'] = $order['payment_method'];
	$invoice['ordertype'] = $order['ordertype'];
	
	$invoice['order_date'] = date('jS M, Y', $order['queueddate']); 
	if($order['shippeddate'] != NULL && $order['shippeddate'] != 0 && $invoiceType != 'special'){
		$invoice['invoice_date'] = date('jS M, Y', $order['shippeddate']);
    }
	else {
        $invoice['invoice_date'] = date('jS M, Y', $order['queueddate']);
    }
	
	$invoice['payable_amount'] = number_format(round(func_get_order_payable_amount($order)), 2);

	$url = "{$baseurl}/include/class/class.barcode.php?code=" . $order['orderid'] . "&text=0&type=.png";
    $invoice['shipmentbarcodeUrl'] =  $url;
    
    $data = PincodeApiClient::getAreaDetailForPincode($order['courier_service'], trim($order['s_zipcode']));
    $city_area_code = "";
    if (!$data) {
        $city_area_code = $order['s_city'];
    } else {
        $city_area_code = $data[0][cityCode] . "/" . $data[0][areaCode];
        if($order['courier_service'] == 'FD'){
            $city_area_code = $data[0][areaCode];
        }
    }
    $invoice['city_area_code'] = $city_area_code;
    $invoice['courier_code'] = $order['courier_service'];
    $invoice['gift_card_amount'] = $order['gift_card_amount'];
    if(!empty($order['tracking'])) {
    	$invoice['tracking_no'] = $order['tracking'];
        if($order['courier_service'] == 'FD'){
            $astra_barcode = func_query_first_cell("select `value` from order_additional_info where order_id_fk = $order[orderid] and `key` = 'FF_ASTRA_BARCODE'");
            
            $invoice['tracking_code_url'] = "{$baseurl}/include/class/oms/128encoding/fdbarcode.php?code=".$astra_barcode."&type=.png";
            $invoice['tracking_no'] = $astra_barcode;
            
            if ($order['payment_method'] == 'cod') {
                $result = func_query("select `key`,`value` from order_additional_info where order_id_fk = $order[orderid] and `key` in ('COD_REVERSE_TRACKING_BARCODE','COD_REVERSE_TRACKING_FORMID')");
                if ($result && !empty($result)) {
                    $new = array();
                    foreach ($result as $item) {
                        foreach ($item as $key => $value) {
                            if ($key == 'key') {
                                $k = $value;
                            } else {
                                $new[$k] = $value;
                            }
                        }
                    }
                    $invoice['cod_tracking_no'] = $new['COD_REVERSE_TRACKING_BARCODE'];
                    $invoice['cod_tracking_code_url'] = "{$baseurl}/include/class/oms/128encoding/fdbarcode.php?code=" . $invoice['cod_tracking_no'] . "&type=.png";
                    $invoice['codFormId'] = $new['COD_REVERSE_TRACKING_FORMID'];
                }
            }
        } else {
            $invoice['tracking_code_url'] = "{$baseurl}/include/class/class.barcode.php?code=".$order['tracking']."&text=0&type=.png";
        }
    }
    
    $items_in_different_shipments = array();
    $other_orders = $groupid2MoreOrders[$order['group_id']];
    if($other_orders && !empty($other_orders)) {
    	foreach($other_orders as $other_order) {
    		if($other_order['orderid'] != $order['orderid']) {
	    		$other_order_items = $order2items[$other_order['orderid']];
	    		if($other_order_items && !empty($other_order_items)) {
	    			foreach($other_order_items as $item) 
	    				$items_in_different_shipments[] = $item;
	    		}
    		}
    	} 
    }
    $margin = explode(",", WidgetKeyValuePairs::getWidgetValueForKey('marginForWarehouse'));
    $showGovtTax = WidgetKeyValuePairs::getWidgetValueForKey('oms.invoiceShowGovtTax');
    if ($showGovtTax == NULL) {
        $showGovtTax = false;
    }
    $invoice['show_govt_tax'] = $showGovtTax;
    $orderdate = getdate($order['date']);
    $order['loyalty_credit'] = $order['loyalty_points_used']*$order['loyalty_points_conversion_factor'];
    $invoice['items_html'] = create_product_detail_table_new_archived($order2items[$order['orderid']], $order, "email", "", $items_in_different_shipments, $invoiceType, $margin[$order['warehouseid']]);
    
    $smarty->assign("invoice", $invoice);
	$smarty->assign("pageno",$counter);
	$smarty->assign("totalpages",count($orders));
	$invoice_pages[] = func_display("order_invoice_new_archived.tpl", $smarty, false);
} 

if(count($invoice_pages) > 1) {
	$content = implode("<div style='display:block; page-break-after:avoid;'>&nbsp;</div>", $invoice_pages);
} else {
	$content = $invoice_pages[0];
}

$smarty->assign("content", $content);

if($copies == null || $copies == '' || $copies == 0){
    $copies=1;
}
for($i=0;$i<$copies;$i++){
func_display("order_invoice_bulk.tpl", $smarty);
}

function func_select_orders_archived($orderids, $exclude_statuses=false) {
	global $sql_tbl, $config;
	
	$sql = "select o.*, cs.courier_service as courier_service_display, cs.website as courier_website from xcart_orders" . $GLOBALS['oldDB'] . " o LEFT JOIN $sql_tbl[courier_service] cs ON o.courier_service = cs.code WHERE o.orderid in (".implode(",", $orderids).")";
	if(is_array($exclude_statuses) && !empty($exclude_statuses)) {
		$sql .= " and o.status not in ('".implode("','", $exclude_statuses)."')";
	}
	
	$orders = func_query($sql);

	foreach($orders as $key=>$order) {
		$order["discounted_subtotal"] = $order["subtotal"] - $order["discount"] - $order["coupon_discount"] - $order['cart_discount'];
		$order["original_address"] = $order["s_address"];
		list($order["b_address"], $order["b_address_2"]) = explode("\n", $order["b_address"]);
		$order["b_statename"] = func_get_state($order["b_state"], $order["b_country"]);
		$order["b_countryname"] = func_get_country($order["b_country"]);
		//list($order["s_address"], $order["s_address_2"]) = explode("\n", $order["s_address"]);
		$order["s_statename"] = func_get_state($order["s_state"], $order["s_country"]);
		$order["s_countryname"] = func_get_country($order["s_country"]);
		$orders[$key] = $order;
	}
	return $orders;
}

function func_create_array_products_of_order_archived($orderids, $productsInCartTemp=array(), $active_items=true, $itemids = false)
{
	global $sql_tbl;
	global $sqllog;
	global $weblog;
 	if(!is_array($orderids))
 		$orderids = array($orderids);
 
    $totalQtyInOffer = 0;
    $productsInCart = array();

    $showGovtTax = \WidgetKeyValuePairs::getWidgetValueForKey('oms.invoiceShowGovtTax');
    if ($showGovtTax == NULL) {
        $showGovtTax = false;
    } else if ($showGovtTax == "true") {
        if ($active_items) {
            $sql .= "SELECT o.gift_card_amount,o.warehouseid,od.product_style as style_id,od.orderid, od.itemid, od.item_status, od.difference_refund, od.tax_rate, od.cart_discount_split_on_ratio as cart_discount_split_on_ratio, actual_product_price, product, productid AS productId, product_style AS productStyleId, product_type AS producTypeId, price AS productPrice, amount AS quantity, (price * amount) AS totalPrice, total, od.discount, o.discount  AS totaldiscount, od.quantity_breakup, promotion_id, addonid, od.pos_unit_mrp, od.pos_total_amount, od.price as unitPrice, od.taxamount as taxamount, od.coupon_discount_product as coupon_discount, od.item_loyalty_points_used, o.loyalty_points_conversion_factor, o.cash_redeemed as cashdiscount, o.cash_coupon_code as cashCouponCode, o.payment_surcharge as payment_surcharge, total_amount, som.sku_id, od.discount_quantity, od.supply_type, od.seller_id, od.govt_tax_rate, od.govt_tax_amount FROM (((xcart_orders" . $GLOBALS['oldDB'] . " o LEFT JOIN xcart_order_details" . $GLOBALS['oldDB'] . " od on o.orderid = od.orderid) LEFT JOIN mk_order_item_option_quantity" . $GLOBALS['oldDB'] . " oq on od.itemid = oq.itemid) LEFT JOIN mk_styles_options_skus_mapping som on oq.optionid = som.option_id) where od.orderid in (" . implode(",", $orderids) . ") AND od.item_status != 'IC'";
        } else {
            $sql .= "SELECT o.gift_card_amount,o.warehouseid,od.product_style as style_id,od.orderid, od.itemid, od.item_status, od.difference_refund, od.tax_rate, od.cart_discount_split_on_ratio as cart_discount_split_on_ratio, actual_product_price, product, productid AS productId, product_style AS productStyleId, product_type AS producTypeId, price AS productPrice, amount AS quantity, (price * amount) AS totalPrice, total, od.discount, o.discount  AS totaldiscount, od.quantity_breakup, promotion_id, addonid, od.pos_unit_mrp, od.pos_total_amount, od.price as unitPrice, od.taxamount as taxamount, od.coupon_discount_product as coupon_discount, od.item_loyalty_points_used, o.loyalty_points_conversion_factor, o.cash_redeemed as cashdiscount, o.cash_coupon_code as cashCouponCode, o.payment_surcharge as payment_surcharge, total_amount, som.sku_id, od.discount_quantity, od.supply_type, od.seller_id, od.govt_tax_rate, od.govt_tax_amount FROM (((xcart_orders" . $GLOBALS['oldDB'] . " o LEFT JOIN xcart_order_details" . $GLOBALS['oldDB'] . " od on o.orderid = od.orderid) LEFT JOIN mk_order_item_option_quantity" . $GLOBALS['oldDB'] . " oq on od.itemid = oq.itemid) LEFT JOIN mk_styles_options_skus_mapping som on oq.optionid = som.option_id) where od.orderid in (" . implode(",", $orderids) . ") AND od.item_status = 'IC'";
        }
    }
    if (!$showGovtTax || $showGovtTax == "false") {
        if ($active_items) {
            $sql .= "SELECT o.warehouseid,od.product_style as style_id,od.orderid, od.itemid, od.item_status, od.difference_refund, od.tax_rate, od.cart_discount_split_on_ratio as cart_discount_split_on_ratio, actual_product_price, product, productid AS productId, product_style AS productStyleId, product_type AS producTypeId, price AS productPrice, amount AS quantity, (price * amount) AS totalPrice, total, od.discount, o.discount  AS totaldiscount, od.quantity_breakup, promotion_id, addonid, od.pos_unit_mrp, od.pos_total_amount, od.price as unitPrice, od.taxamount as taxamount, od.coupon_discount_product as coupon_discount, od.item_loyalty_points_used, o.loyalty_points_conversion_factor, o.cash_redeemed as cashdiscount, o.cash_coupon_code as cashCouponCode, o.payment_surcharge as payment_surcharge, total_amount, som.sku_id, od.discount_quantity, od.supply_type, od.seller_id FROM (((xcart_orders" . $GLOBALS['oldDB'] . " o LEFT JOIN xcart_order_details" . $GLOBALS['oldDB'] . " od on o.orderid = od.orderid) LEFT JOIN mk_order_item_option_quantity" . $GLOBALS['oldDB'] . " oq on od.itemid = oq.itemid) LEFT JOIN mk_styles_options_skus_mapping som on oq.optionid = som.option_id) where od.orderid in (" . implode(",", $orderids) . ") AND od.item_status != 'IC'";
        } else {
            $sql .= "SELECT o.warehouseid,od.product_style as style_id,od.orderid, od.itemid, od.item_status, od.difference_refund, od.tax_rate, od.cart_discount_split_on_ratio as cart_discount_split_on_ratio, actual_product_price, product, productid AS productId, product_style AS productStyleId, product_type AS producTypeId, price AS productPrice, amount AS quantity, (price * amount) AS totalPrice, total, od.discount, o.discount  AS totaldiscount, od.quantity_breakup, promotion_id, addonid, od.pos_unit_mrp, od.pos_total_amount, od.price as unitPrice, od.taxamount as taxamount, od.coupon_discount_product as coupon_discount, od.item_loyalty_points_used, o.loyalty_points_conversion_factor, o.cash_redeemed as cashdiscount, o.cash_coupon_code as cashCouponCode, o.payment_surcharge as payment_surcharge, total_amount, som.sku_id, od.discount_quantity, od.supply_type, od.seller_id FROM (((xcart_orders" . $GLOBALS['oldDB'] . " o LEFT JOIN xcart_order_details" . $GLOBALS['oldDB'] . " od on o.orderid = od.orderid) LEFT JOIN mk_order_item_option_quantity" . $GLOBALS['oldDB'] . " oq on od.itemid = oq.itemid) LEFT JOIN mk_styles_options_skus_mapping som on oq.optionid = som.option_id) where od.orderid in (" . implode(",", $orderids) . ") AND od.item_status = 'IC'";
        }
    }
    if($itemids){
    	$sql .= " AND od.itemid in (".implode(",", $itemids).")";	
    }
    
    $productsInCart = func_query($sql);

	$productids = array();
	foreach($productsInCart as $key => $order)
	{
		$productsInCart[$key]['totalProductPrice'] = $order['totalPrice'];
		$productsInCart[$key]['couponDiscount'] = $order['coupon_disount'];
		$productsInCart[$key]['cartDiscount'] = $order['cart_discount_split_on_ratio'];
		$productsInCart[$key]['totalMyntCashUsage'] = $order['cashdiscount'];
		$productsInCart[$key]['loyalty_credit'] = $order['item_loyalty_points_used']*$order['loyalty_points_conversion_factor'];
		
                $productsInCart[$key]['discountAmount'] = $order['discount'];
                $govtTaxItemInfo = fetchGovtTaxRelease($order['orderid'],$order['itemid']);
                $productsInCart[$key]['govt_tax_amount'] = round($govtTaxItemInfo['govt_tax_amount'], 2);
                $productsInCart[$key]['govt_tax_rate'] = round($govtTaxItemInfo['govt_tax_rate'], 3);
		//Query for retriving the style name from mk_product_style
		$stylename = "SELECT name, styletype FROM $sql_tbl[mk_product_style] WHERE id = '".$productsInCart[$key]['productStyleId']."'";
		$styleresult = db_query($stylename);
		$rowstyle = db_fetch_array($styleresult);

		//Query for retriving the style options from mk_product_options
		 $styleoption = "SELECT $sql_tbl[mk_product_options].name AS optionName, $sql_tbl[mk_product_options].value AS optionValue FROM $sql_tbl[mk_product_options], $sql_tbl[mk_product_options_order_details_rel]
		WHERE $sql_tbl[mk_product_options].id = $sql_tbl[mk_product_options_order_details_rel].option_id
		AND $sql_tbl[mk_product_options].style = ".$productsInCart[$key]['productStyleId']."
		AND $sql_tbl[mk_product_options_order_details_rel].orderid = '".$_GET['orderid']."'
		AND $sql_tbl[mk_product_options_order_details_rel].itemid = '".$productsInCart[$key]['productId']."'";

		$optionresult = db_query($styleoption);
		$optionCount = db_num_rows($optionresult);
		$productsInCart[$key]['optionCount'] = $optionCount;
		$i = 0;
		while($optionrow = db_fetch_array($optionresult))
		{
			$productsInCart[$key]['optionNames'][$i] =  $optionrow['optionName'];
			$productsInCart[$key]['optionValues'][$i] =  $optionrow['optionValue'];
			$i++;
		}

		$typename = "SELECT pt.name as productName, pt.label as pLabel, pt.image_t as pImage, sp.global_attr_article_type as article_type, 
					ps.name as sName, substr(sp.article_number,1,10) as article_number, sp.product_display_name, mcc.typename 		
			FROM $sql_tbl[producttypes] AS pt, $sql_tbl[mk_product_style] AS ps, mk_style_properties sp, mk_catalogue_classification mcc
			WHERE pt.id = ps.product_type and sp.style_id = ps.id
			AND sp.global_attr_article_type = mcc.id 
			AND pt.id = ps.product_type AND ps.id=".$productsInCart[$key]['productStyleId']."";

			$typeresult = db_query($typename);
			$row = db_fetch_array($typeresult);
			
			if(isset($tempStr))	{
				$tempStr.=", ";	
			}
			$tempStr .= $productsInCart[$key]['productStyleId'];

			$productsInCart[$key]['productStyleName'] = $row['sName'];
			$productsInCart[$key]['productTypeLabel'] = $row['productName'];
			$productsInCart[$key]['article_number'] = $row['article_number'];
			$productsInCart[$key]['article_type'] = $row['article_type'];
			$productsInCart[$key]['product_display_name'] = $row['product_display_name'];
			$productsInCart[$key]['typename'] = $row['typename'];
			
			if($productsInCart[$key]['addonid'])
			{
				$addonname = get_style_addon_name($productsInCart[$key]['addonid']);
				if($addonname)
				{
					$productsInCart[$key]['productStyleName'] .= " ( $addonname ) ";
				}
			}

			$areaId = func_get_all_orientation_for_default_customization_area($productsInCart[$key]['productStyleId']);

			$productsInCart[$key]['defaultCustomizationId'] = $areaId[0][6];

			$pid = $productsInCart[$key]['productId'];
			$defaultImage = "SELECT image_portal_t  as ProdImage FROM $sql_tbl[products] WHERE productid =$pid";

			$ImageResult = db_query($defaultImage);
			$ImageRow = db_fetch_array($ImageResult);
	
			$productsInCart[$key]['productStyleType'] = $rowstyle['styletype'];
			$productsInCart[$key]['productTypeLabel'] = $row['pLabel'];
			$productsInCart[$key]['designImagePath'] = $ImageRow['ProdImage'];
										
			//$productsInCart[$key]['custAreaCount'] = $productsInSess[$productsInCart[$key]['productId']]['custAreaCount'];	
			
			//size and quantity information to be stored with each product
			if(!empty($productsInCartTemp)){
				$flattenSizeOptions = $productsInCartTemp[$order['productId']]["flattenSizeOptions"];
				$sizenamesunified=$productsInCartTemp[$order['productId']]["sizenamesunified"];
				$sizenames=$productsInCartTemp[$order['productId']]["sizenames"];
				$sizequantities=$productsInCartTemp[$order['productId']]["sizequantities"];
	        	foreach($sizequantities as $i => $j){
	            	if($j){
	    	        	if($flattenSizeOptions){
		            		$productsInCart[$key]["final_size"]= $sizenamesunified[$i];
		            		$productsInCart[$key]["final_size_replaced"]= $sizenames[$i];
	            		}
	            		else{
	            			$productsInCart[$key]["final_size"]= $sizenames[$i];
	            			$productsInCart[$key]["final_size_replaced"]= "";
	            		}
	             	   $productsInCart[$key]["final_quantity"] = $j;
	            	}
	        	}
			}
			else{
				//$all_option_qty_details = explode(",", $productsInCart[$key]['quantity_breakup']);
				$unifiedsizes=Size_unification::getUnifiedSizeByStyleId($productsInCart[$key]['productStyleId'], "array", true);
				$options_qty_mapping = func_query_first("Select po.value as size, oq.quantity from mk_order_item_option_quantity" . $GLOBALS['oldDB'] . " oq, mk_product_options po where oq.optionid = po.id and oq.itemid = ".$productsInCart[$key]['itemid']);
				$productsInCart[$key]['final_size'] = $unifiedsizes[$options_qty_mapping['size']];
				$productsInCart[$key]['final_quantity'] = $options_qty_mapping['quantity'];
				$productsInCart[$key]['final_size_replaced'] = $options_qty_mapping['size'];
			}
	}
	
	if($tempStr){
			$getCatQuery = "SELECT sp.style_id as styleId, sp.global_attr_brand as brandName, cc.typename as articleType FROM mk_style_properties as sp,".
			"mk_catalogue_classification as cc WHERE cc.id=sp.global_attr_article_type AND sp.style_id in (". $tempStr.")";
					
		 	$getCatResult = db_query($getCatQuery);
			$productStyleIds = array();
			while($categoryRow = db_fetch_array($getCatResult))
			{
				$productStyleIds[$categoryRow['styleId']] = $categoryRow['articleType']."|".$categoryRow['brandName'];                
			}
			foreach($productsInCart as $key => $order)
			{
				$styleId = $productsInCart[$key]['productStyleId'];
				$productsInCart[$key]['productCatLabel'] = $productStyleIds[$styleId];            

			}
	}
	return $productsInCart;
}

function create_product_detail_table_new_archived($order_products_detail, $order_amount_detail=false, $display_for="email", $action="", $items_in_different_shipments=false, $invoiceType=false, $margin=false, $showGovtTax=true, $total_gift_card_amount_refund=false) {
	global $smarty;
	$smarty->clear_assign("other_shipment_items");
	$smarty->assign("invoiceType", $invoiceType);
	$smarty->assign("margin", $margin);

	$grand_total = 0.00;
	$skuIds = array();
	foreach($order_products_detail as $index=>$item){
		if($item['cart_discount_split_on_ratio']){
			$order_products_detail[$index]['per_item_discount'] = number_format(($item['discount']+$item['cart_discount_split_on_ratio']), 2);
		} else {	
			$order_products_detail[$index]['per_item_discount'] = number_format(($item['discount']+$item['cart_discount']), 2);
		}
		if(!empty($item['sku_id']) && !in_array($item['sku_id'], $skuIds))
			$skuIds[] = $item['sku_id'];
		if($margin){
			//$total_amount_finance = round($item['total_amount'] - $item['coupon_discount'] - ($margin*($item['total_amount']-$item['coupon_discount']))/100, 2);
			$base_price = round((1 - $margin * 0.01) * ($item['govt_tax_amount'] / ($item['govt_tax_rate'] * 0.01)),2);
			$vatamount_finance = round($item['govt_tax_rate'] * $base_price * 0.01, 2);
                        $total_amount_finance = $base_price + $vatamount_finance;
			$order_products_detail[$index]['total_amount_finance'] = number_format($total_amount_finance, 2);
			$order_products_detail[$index]['base_price'] = number_format($base_price, 2);
			$order_products_detail[$index]['vatamount_finance'] = number_format($vatamount_finance, 2);
			$grand_total += $total_amount_finance;
		}
	}
	
	$skuDetails = SkuApiClient::getSkuDetails($skuIds);
	foreach($skuDetails as $sku) {
		$skuId2SkuCode[$sku['id']] = $sku['code'];	
	}
	
	$smarty->assign("grand_total", number_format($grand_total,2));
	$smarty->assign("order_details", $order_products_detail);
	$smarty->assign("sku_code_mapping", $skuId2SkuCode);
	if($order_amount_detail) {
		if($order_amount_detail['cart_discount_split_on_ratio']){
		//invoice page
			$order_amount_detail['discounted_sub_total'] = number_format($order_amount_detail['subtotal'] - $order_amount_detail['discount'] - $order_amount_detail['cart_discount_split_on_ratio'] - $order_amount_detail['coupon_discount'], 2);
		} else {
			$order_amount_detail['discounted_sub_total'] = number_format($order_amount_detail['subtotal'] - $order_amount_detail['discount'] - $order_amount_detail['cart_discount']- $order_amount_detail['coupon_discount'], 2);
		}
		$order_amount_detail['discount'] = number_format($order_amount_detail['discount']+ $order_amount_detail['cart_discount'], 2);
                $order_amount_detail['discounted_amount'] = number_format($order_amount_detail['subtotal'] - $order_amount_detail['discount'] - $order_amount_detail['cart_discount_split_on_ratio'] - $order_amount_detail['cart_discount'] - $order_amount_detail['coupon_discount'],2);
		$order_amount_detail['difference_refund'] = number_format($order_amount_detail['difference_refund'], 2);
		$order_amount_detail['tax'] = number_format($order_amount_detail['tax'], 2);
		$order_amount_detail['loyalty_credit'] = number_format($order_amount_detail['loyalty_credit'], 2);
		$order_amount_detail['payable_amount'] = number_format(round(func_get_order_payable_amount($order_amount_detail)), 2);
		if($action == "refund"){
			if($order_amount_detail['payment_method'] == 'cod'){
				$refundable_amount = $order_amount_detail['cash_redeemed'] - $order_amount_detail['difference_refund'] + $order_amount_detail['gift_card_amount'];
			} else {
				$refundable_amount = $order_amount_detail['total']+$order_amount_detail['payment_surcharge']+$order_amount_detail['cod'] - $order_amount_detail['cashback'] - $order_amount_detail['difference_refund'] + $order_amount_detail['cash_redeemed'];
			}
			if($order_amount_detail['cashback']) {
				$refundable_amount -= $order_amount_detail['cashback'];
			}
			$order_amount_detail['refundable_amount'] = number_format($refundable_amount, 2);
			if($order_amount_detail['ordertype'] == 'ex'){
				$order_amount_detail['refundable_amount'] = 0.00;
			}
		}

        if($total_gift_card_amount_refund > 0.00 && $order_amount_detail['gift_card_amount'] == 0.00){
            $order_amount_detail['gift_card_amount'] = $total_gift_card_amount_refund;
        }
		
		$smarty->assign("amount_details", $order_amount_detail);
	}
	$smarty->assign("display_for", $display_for);
	
	if($items_in_different_shipments && count($items_in_different_shipments) > 0)
		$smarty->assign("other_shipment_items", $items_in_different_shipments);
        $smarty->assign("show_govt_tax", $showGovtTax);
		
	$productdetails = func_display('invoice_email_item_details_archived.tpl', $smarty, false);
	
	return $productdetails;
}

function fetchGovtTaxRelease($releaseId, $itemId) {
    global $login;
    $fetchReleaseInfoQuery = "select * from xcart_orders". $GLOBALS['oldDB'] . " where orderid = " . $releaseId;
    $releaseInfo = func_query_first($fetchReleaseInfoQuery);
    $sourceWarehouseId = $releaseInfo['warehouseid'];
    $destinationPincode = $releaseInfo['s_zipcode'];
    $fetchLineInfoQuery = "select * from xcart_order_details". $GLOBALS['oldDB'] . " where orderid = " . $releaseId . " and itemid = " . $itemId;
    $lineInfo = func_query($fetchLineInfoQuery);
    $lines = array();
    if (!is_array($lineInfo)) {
        $lines = array($lineInfo);
    } else {
        $lines = $lineInfo;
    }
    $releaseTaxInfo = array();
    // Total money paid
    $releaseValue = $releaseInfo['total'] + $releaseInfo['cash_redeemed'] + $releaseInfo['loyalty_points_used'] * $releaseInfo['loyalty_points_conversion_factor'] - $releaseInfo['shipping_cost'] - $releaseInfo['gift_charges'];
    foreach ($lineInfo as $singleLineInfo) {
        if ($singleLineInfo['item_status'] != 'IC') {
            
            $itemid = $singleLineInfo['itemid'];
            $styleId = $singleLineInfo['product_style'];
            $itemValue = $singleLineInfo['price'] * $singleLineInfo['amount'] - $singleLineInfo['discount'] - $singleLineInfo['coupon_discount_product'] - $singleLineInfo['cart_discount_split_on_ratio'] + $singleLineInfo['taxamount'];
            if ($releaseValue > 0) {
                $itemLevelCost = ($itemValue / $releaseValue) * ($releaseInfo['shipping_cost'] + $releaseInfo['gift_charges']);
            }
            $itemCost = $singleLineInfo['price'] * $singleLineInfo['amount'] - $singleLineInfo['discount'] - $singleLineInfo['coupon_discount_product'] - $singleLineInfo['cart_discount_split_on_ratio'] + $singleLineInfo['taxamount'] + $itemLevelCost;
            //$taxInfo = self::fetchGovtTax($styleId, $itemCost, $sourceWarehouseId, $destinationPincode);
            $taxResponse = \TaxEngineClient::fetchGovtTax($sourceWarehouseId, $destinationPincode, $styleId, $itemCost, $login);
            $lineTaxInfo = array();
            if ($taxResponse['status'] == 'success') {
                $govtTaxRate = $taxResponse['data']['govtTaxRate'];
                $govtTaxRuleName = $taxResponse['data']['govtTaxRuleName'];
                $govtTaxAmount = $taxResponse['data']['govtTaxAmount'];
                //$keralaZipcodes = WidgetKeyValuePairs::getWidgetValueForKey('keralaZipcodes');
                //$keralaZipcodesArray = explode(",",$keralaZipcodes);
                $karWarehouses = array("1","3","6","8","19","22","23","24","27","30","33","36");
                $harWarehouses = array("2","38","25","28");
                $delWarehouses = array("7","17","20");
                if (strpos($govtTaxRuleName, 'KER') && $releaseInfo['date'] < 1396310400) {
                    if (in_array($sourceWarehouseId, $karWarehouses) && $govtTaxRate == "14.5") {
                        $govtTaxRate = "14.5";
                    }
                    if (in_array($sourceWarehouseId, $harWarehouses) && $govtTaxRate == "14.5") {
                        $govtTaxRate = "13.125";
                    }
                    if (in_array($sourceWarehouseId, $delWarehouses) && $govtTaxRate == "14.5") {
                        $govtTaxRate = "12.5";
                    }
                    if (in_array($sourceWarehouseId, $karWarehouses) && $govtTaxRate == "5") {
                        $govtTaxRate = "5.5";
                    }
                    if (in_array($sourceWarehouseId, $harWarehouses) && $govtTaxRate == "5") {
                        $govtTaxRate = "5.25";
                    }
                    if (in_array($sourceWarehouseId, $delWarehouses) && $govtTaxRate == "5") {
                        $govtTaxRate = "5.0";
                    }
		    $fetchArticleIdSql = "select global_attr_article_type from mk_style_properties where style_id = " . $styleId;
                    $articleType = func_query_first($fetchArticleIdSql,true);
                    if ((strpos($govtTaxRuleName, 'fabric') && $articleType['global_attr_article_type'] == "48") || (strpos($govtTaxRuleName, 'herbal') && $articleType['global_attr_article_type'] == "101")) {
                        if (in_array($sourceWarehouseId, $karWarehouses)) {
                            $govtTaxRate = "5.5";
                        }
                        if (in_array($sourceWarehouseId, $harWarehouses)) {
                            $govtTaxRate = "5.25";
                        }
                        if (in_array($sourceWarehouseId, $delWarehouses)) {
                            $govtTaxRate = "5.0";
                        }
                    }
                    //echo $articleType['global_attr_article_type'];
		    if ($articleType['global_attr_article_type'] == "127") {
                        if (in_array($sourceWarehouseId, $harWarehouses)) {
                            if ($itemCost < 500) {
                                $govtTaxRate = "5.25";
                            }
                        }
                    }
                    $govtTaxAmount = (0.01 * $govtTaxRate / (1+ 0.01 * $govtTaxRate)) * $itemCost;
                }
                $lineTaxInfo['govt_tax_amount'] = round($govtTaxAmount,2);
                $lineTaxInfo['govt_tax_rate'] = round($govtTaxRate,3);
                $lineTaxInfo['itemid'] = $itemid;
                //$itemUpdateQuery = "update xcart_order_details set govt_tax_rate = " . $taxInfo['govtTaxRate'] . ", govt_tax_amount = " . $taxInfo['govtTaxAmount'] . "where itemid = $itemid";
                //db_query($itemUpdateQuery);
            }
            //array_push($releaseTaxInfo,$lineTaxInfo);
        }
    }
    return $lineTaxInfo;
}
    /*
if($skin === "skin2") {
    $html = $smarty->fetch("order_invoice_bulk.tpl");
} else {
    $html = func_display("order_invoice_bulk.tpl", $smarty,false);
}
require_once("$xcart_dir/include/mpdf/mpdf.php");

$mpdf = new mPDF('en-GB','A4','','',5,5,10,0,5,0);
$mpdf->WriteHTML($html);
$mpdf->Output("invoice.pdf","D");*/
?>

