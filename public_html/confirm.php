<?php
use abtest\MABTest;

include_once(__DIR__."/auth.php");
include_once("$xcart_dir/include/class/class.orders.php");
include_once("$xcart_dir/include/func/func.mk_orderbook.php");
include_once "$xcart_dir/modules/coupon/database/CouponAdapter.php";
include_once \HostConfig::$documentRoot."/include/class/class.mymyntra.php";
include_once \HostConfig::$documentRoot."/modules/coupon/mrp/SmsVerifier.php";
include_once "$xcart_dir/include/func/func.loginhelper.php";
/**
 * BaseTracker moved from mkorderBook to confirmation page - Starts
 */
include_once("$xcart_dir/include/class/instrumentation/BaseTracker.php");
$tracker = new BaseTracker(BaseTracker::CONFIRMATION);
if(MABTest::getInstance()->getUserSegment("checkout_flow_java")==="java"){
	Profiler::increment("checkout_flow_java_orders");
}else{
	Profiler::increment("checkout_flow_php_orders");
}
/**
 * BaseTracker moved from mkorderBook to confirmation page - End
 */

include_once "$xcart_dir/modules/coupon/SpecialCouponGenerator.php";
require_once "$xcart_dir/modules/coupon/mrp/RuleExecutor.php";
use revenue\payments\service\PaymentServiceInterface;

/**
 * BaseTracker moved from mkorderBook to confirmation page - Starts
 */
use enums\base\SizeChartTrackerConstants;
/**
 * BaseTracker moved from mkorderBook to confirmation page - End
 */

unset($XCART_SESSION_VARS['selected_address']);
unset($XCART_SESSION_VARS['shipping.shippingMethod']);




//security checks
if(empty($_GET) || empty($orderid)) {
	func_header_location($http_location,false);
}

if(!isGuestCheckout() && empty($login)) {
	//empty login
	func_header_location($http_location,false);
}

$pdpServiceUrl = 'https://' . HostConfig::$httpsHost.'/myntapi/style/style-service/pdpservice/style/pdp/';
$smarty->assign('pdpServiceUrl',$pdpServiceUrl);

$pageName = 'confirmation';
include_once $xcart_dir."/webengage.php";

$orderid = mysql_real_escape_string($orderid);
$orderInstrumentation = $XCART_SESSION_VARS['orderID'];
$trackOrderData = false;
if(!empty($orderInstrumentation) && $orderInstrumentation == $orderid){
	$trackOrderData = true;
	x_session_unregister("orderID");
}

$order_result = func_query("SELECT * FROM " . $sql_tbl['orders'] . " WHERE group_id='". $orderid ."'");
$orderLogin = $order_result[0]['login'];
$status = $order_result[0]['status'];

// payment service is a hard dependency
//if(PaymentServiceInterface::getPaymentServiceEnabledForUser($login)) {
    $paymentLog = json_decode(PaymentServiceInterface::getPaymentLog($orderid), true);
    $paybyoption = $paymentLog['paymentOption'];
    //SERVICES-27 . To resolve the difference in spelling of creditcard between portal and paymentservice  
    if( $paybyoption == 'creditcard')
    	$paybyoption = 'creditcards';    
    /**
	 * BaseTracker moved from mkorderBook to confirmation page - Start
	 * Populating data needed for the instrumentation calls
	 */
	if($trackOrderData){
	    $toLog = array(
			"time_return" => $paymentLog['returnTime'],
			"is_tampered" => $paymentLog['isTampered'],
			"is_complete" => $paymentLog['isComplete'],
			"is_flagged" => $paymentLog['isFlagged'],
			"amountToBePaid" => $paymentLog['amountToBePaid'],
			"amountPaid" => $paymentLog['amountPaid'],
			"completed_via" => $paymentLog['completedVia']
		);
		$orderLogDetails = array(
			"id" => $paymentLog['id'],
			"orderid" => $paymentLog['orderid'],
			"login" => $paymentLog['login'],
			"amount" => $paymentLog['amount'],
			"user_agent" => $paymentLog['userAgent'],
			"payment_gateway_name" => $paymentLog['gatewayName'],
			"payment_option" => $paymentLog['paymentOption'],
			"payment_issuer" => $paymentLog['paymentIssuer'],
			"is_inline" => $paymentLog['isInline'],
			"time_insert" => $paymentLog['insertTime'],
			"time_return" => $paymentLog['returnTime'],
			"time_transaction" => $paymentLog['transactionTime'],
			"is_tampered" => $paymentLog['isTampered'],
			"is_complete" => $paymentLog['isComplete'],
			"is_flagged" => $paymentLog['isFlagged'],
			"response_code" => $paymentLog['responseCode'],
			"response_message" => $paymentLog['responseMessage'],
			"bank_transaction_id" => $paymentLog['bankTranscationId'],
			"payment_gateway_id" => $paymentLog['gatewayPaymentId'],
			"completed_via" => $paymentLog['completedVia'],
			"amountPaid" => $paymentLog['amountPaid'],
			"amountToBePaid" => $paymentLog['amountToBePaid'],
			"gatewayResponse" => $paymentLog['gatewayResponse'],
			"time_unflagged" => $paymentLog['timeUnflagged'],
			"card_bank_name" => $paymentLog['cardBankName'],		
			"bin_number" => $paymentLog['binNumber'],
			"ip_address" => $paymentLog['ipAddress']
		);
	}
	/**
	 * BaseTracker moved from mkorderBook to confirmation page - End
	 */
//} else {
//    $paymnetlogs = func_query("SELECT payment_option FROM mk_payments_log WHERE orderid='". $orderid ."'");
//    $paybyoption = $paymnetlogs[0]['payment_option'];
//}


if(!isGuestCheckout() && strcasecmp($login, $orderLogin) != 0) {
	//login not matched with user who placed the order
	func_header_location($http_location,false);
}


//security check ends
if($status=="Q" || $status=="WP" || $status=="SH" || $status=="OH" || $status=="DL") {
    

    $smarty->assign('isFirstOrder',0); 
    if($XCART_SESSION_VARS['returningCustomer'] != 1) {
        x_session_unregister("returningCustomer",true);
        x_session_register("returningCustomer", 1); // making user as buyer for vizury;

        setMyntraCookie("isBuyerCookie",$returningCustomer,time()+3600*24*365,'/',$cookiedomain);

        $smarty->assign('isFirstOrder',1);
    }

    $smarty->assign('returningCustomer', $XCART_SESSION_VARS['returningCustomer']);

	foreach($order_result as $order) {
		$orderids[] = $order['orderid'];
		$ordersubtotal += $order['subtotal'];
		$cashdiscount += $order['cash_redeemed'];			
		$codCharge += $order['cod'];	
		$orderTotal += $order['total'];
		$coupondiscount += $order['coupon_discount'];
		$productDiscount += $order['discount'];
		$cartDiscount += $order['cart_discount'];
		$pg_discount += $order['pg_discount'];
		$emi_charge += $order['payment_surcharge'];
		$shippingCharges += $order['shipping_cost'];
		$giftCharges += $order['gift_charges'];
		$amountafterdiscount += $order['total'] + $order['cod'] + $order['payment_surcharge'];
		$finalAmountTobePaid = $amountafterdiscount - $order['gift_card_amount'];
		
		$loyaltyPointsAwarded += $order["loyalty_points_awarded"];
	}
	
	$productsInCart = func_create_array_products_of_order($orderids, null);	
	$highestPriceArticleType = getHighestPriceArticleType($productsInCart);		
	
	$couponCode = $order_result[0]['coupon'];
	$cashCouponCode = $order_result[0]['cash_coupon_code'];
	$paymentoption = $order_result[0]['payment_method'];

	/**
	 * BaseTracker moved from mkorderBook to confirmation page - Start
	 * Populating data needed for the instrumentation calls
	 */
	if($trackOrderData){
		$couponCashDetails = array(
			"coupon" => $order_result[0]['coupon'],
			"cash_coupon_code" => $order_result[0]['cash_coupon_code'],
			"cash_redeemed" => $order['cash_redeemed'],
			"coupon_discount" => $order['coupon_discount']
		);
		$productArray = array();
		$styleIds = array();

		foreach($productsInCart as $key => $val) {
			$styleIds[] = $val["productStyleId"];
			$productArray[$val["productStyleId"]."_".$val["sku_id"]]=$val;
		}

		/*print_r($orderLogDetails);echo "<br/>";
		print_r($toLog);echo "<br/>";
		print_r($couponCashDetails);echo "<br/>";
		print_r($productsInCart);echo "<br/>";
		print_r($styleIds);echo "<br/>";
		exit;*/
	}
	
	if(!is_null($tracker->confirmationData) && $trackOrderData)
	{
		$tracker->confirmationData->setOrderId($orderid);
		$tracker->confirmationData->setPaymentData(array_merge($orderLogDetails, $toLog,$couponCashDetails));
		if(!empty($productArray)) {
			$tracker->confirmationData->setCartData($productArray);
			//PORTAL-2116: Kundan: if this feature gate is enabled, during order confirmation page load, pass the style-ids for which size chart was clicked to Mongo DB
			$sizechartClickTrackingEnabled = FeatureGateKeyValuePairs::getBoolean("sizechart.clicktracking.enabled", false);
			if($sizechartClickTrackingEnabled) {
				$styleArr = getStyleIdsWithSizeChartClicked($styleIds);
				$tracker->confirmationData->setStyleIdsWithNewSizeChartClicked($styleArr["new"]);
				$tracker->confirmationData->setStyleIdsWithOldSizeChartClicked($styleArr["old"]);
				//remove the cookies related to size chart clicks
				removeSizeChartClickedCookies();
			}
		}
	}
	/**
	 * BaseTracker moved from mkorderBook to confirmation page - End
	 */

	/*****registration coupon usage rule executer : begins****/
	
	$couponGroupName = func_query_first("select groupName from ".$sql_tbl['discount_coupons']." where status='U' and coupon='$couponCode'");
	
	if(!empty($couponGroupName)&&$couponGroupName["groupName"]=="FirstLogin"){
		//synchronous call to registraion coupon usage rule
		$ruleExecutor = \RuleExecutor::getInstance();
		$ruleExecutor->runSynchronous("coupon",$couponCode, "7"); // rule number 7 for registraion coupon usage 
	}
	
	/****registration coupon usage rule executer : ends***/
	
	/*** myntra festival coupon generation :begins***/
	$couponGroupName = func_query_first("select groupName from ".$sql_tbl['discount_coupons']." where coupon='$couponCode'");
	if(FeatureGateKeyValuePairs::getBoolean("myntrashoppingfestCoupons")){
		if(empty($couponCode) || (!empty($couponGroupName) && $couponGroupName["groupName"]!=WidgetKeyValuePairs::getWidgetValueForKey('myntraFestCouponGroupName'))){
						
			//GET ALL COUPONS THAT ARE GENERATED
			$festCoupons=SpecialCouponGenerator::getAllSpecialCouponForOrder($orderid);
			$smarty->assign('fest_coupons',$festCoupons);
			$today = strtotime(date("Y-m-d H:i:s"));
			$smarty->assign("today", $today);
			$smarty->assign("coupon_value",$festCoupons[0]['MRPAmount']);
			if(!FeatureGateKeyValuePairs::getBoolean('myntraFestSLabCoupons')){
			$smarty->assign("coupon_percent",$festCoupons[0]['MRPpercentage']);
			}
		}
	}
	/*** myntra festival coupon generation :ends**/
	$smarty->assign("shopingFestFG",FeatureGateKeyValuePairs::getBoolean("myntrashoppingfest"));
	$smarty->assign('confirmationConfigMsg',WidgetKeyValuePairs::getWidgetValueForKey("confirmationConfigMsg"));
	
	//$delivery_date = date("jS M Y", getOrderDeliveryDateInTimestamp($order_result[0]['date']));
	//$delivery_date = date("jS M Y", getDeliveryDateForOrder($order_result[0]['queueddate'], $productsInCart, $order_result[0]['s_zipcode'], $order_result[0]['payment_method'],$order_result[0]['courier_service'],$order_result[0]['warehouseid'],$order_result[0]['orderid']));
	
	
	$cashback_gateway_status = FeatureGateKeyValuePairs::getFeatureGateValueForKey('cashbackenabled');
	$enable_cashback_discounted_products = FeatureGateKeyValuePairs::getFeatureGateValueForKey('enableCBOnDiscountedProducts');
	
	$adapter = CouponAdapter::getInstance();
	$total = $orderTotal + $cashdiscount + $cartDiscount- $shippingCharges - $giftCharges; 
	$cashBackToBeCredited = $adapter->getCashBackToBeCreditedForOrderid($cashback_gateway_status, $enable_cashback_discounted_products, $login, $total, $orderid);
	
	$fireSyncGACall = FeatureGateKeyValuePairs::getBoolean("confirmation.fireSyncGACall",false);
	$recordGACall = FeatureGateKeyValuePairs::getBoolean("confirmation.recordGACall",false);
	
	
	/*****AVAIL IMPLEMENTATION - Create a cookie that has the styleid of last 5 purchased items.*******/
	$rpi_avail=array();
	$temp_rpi_avail = getMyntraCookie('RPI_AVAIL');
	
	if(!empty($temp_rpi_avail)){
		$rpi_avail=explode(',',$temp_rpi_avail);
	}
	
	$styleIdList = array();
    $productList = array();
    $orderProductList = array();
    $cartNanigansString ='';
    $i=0;
	foreach($productsInCart as $products)
	{
		$styleIdList[] = $products['productStyleId'];

        $item = array();
        $item['id'] = $products['style_id'];
        $item['price'] = $products['productPrice'];
        $item['quantity'] = $products['quantity'];
        array_push($productList,$item);
        $orderItem = array();
        $orderItem['identifier'] = $products['style_id'];
        $orderItem['amount'] = $products['productPrice'];
        $orderItem['quantity'] = $products['quantity'];
        $orderItem['currency'] = 'INR';
        array_push($orderProductList, $orderItem);
        $cartNanigansString .= 'qty['.$i.']='.$products['quantity'].'&value['.$i.']='.floor($products['productPrice']).'&sku['.$i.']='.$products['style_id'].'&';
	$i++;
	}
    $smarty->assign('styleIdList',implode(',',$styleIdList));
    $smarty->assign('productsInCart',$productsInCart);
    $smarty->assign('products',json_encode($productList));
    $smarty->assign('orderSociomanticString',  json_encode($orderProductList));
    $smarty->assign('orderNanigansString',  rtrim($cartNanigansString, '&'));
    for($i=0;$i<5;$i++){
	$smarty->assign(('CartProductId'.($i+1)),$productsInCart[$i]?$productsInCart[$i]['style_id']:null);
        $smarty->assign(('CartProductAmount'.($i+1)),$productsInCart[$i]?floor($productsInCart[$i]['productPrice']):null);
        $smarty->assign(('CartProductQuantity'.($i+1)),$productsInCart[$i]?$productsInCart[$i]['quantity']:null);
	
	}	
	$discountDetails = DiscountEngine::getDataForMultipleStyles("confirmation", $styleIdList);
	
	global $_rst;
	$totalItemRevenueOmnitureTrack = 0;
	$cashbackRedeemedOmnitureTrack = 0;
	foreach ($productsInCart as $key => $product) {
		$discountData = $discountDetails->$product['productStyleId'];
		foreach($discountData->displayText->params as $k=>$v) {
			$product["dre_".$k] = $v;
		}
		if(!empty($product["discountAmount"])) {
			if($product["discount_quantity"]>0)
				$pricePerItemAfterDiscount = round($product["productPrice"]-($product["discount"]/$product["discount_quantity"]),1);
			else
				$pricePerItemAfterDiscount = round($product["productPrice"]-$product["discount"],1);
			$product['pricePerItemAfterDiscount'] = $pricePerItemAfterDiscount;
		}
	
		$product["discount_display_text"] = $_rst[$discountData->displayText->id];
		$productsInCart[$key] = $product;
	
		$totalItemRevenueOmnitureTrack+= $product["total_amount"] - $product["coupon_discount"];
		$cashbackRedeemedOmnitureTrack=$product["cashdiscount"];
	}
	x_session_unregister("mrpData");
	x_session_unregister("loyaltyPointsHeaderDetails");


	foreach($productsInCart as $products)
	{
		array_unshift($rpi_avail,$products["productStyleId"]);
		for($i=$products['quantity'];$i>0;$i--)
		{
			$ids[]="".$products['productStyleId'];
			$socialids[]=$products['productStyleId'];
			$prices_t= $products['pricePerItemAfterDiscount'] - (0.1 * $products['pricePerItemAfterDiscount']);
			$prices[]="".$prices_t;
		}
	}
	$id_s=implode(',',$ids);
	$prices_s=implode(',',$prices);
	$smarty->assign("avail_ids",$id_s);
	$smarty->assign("avail_prices",$prices_s);
	$rpi_avail=array_unique($rpi_avail);
	$rpi_avail=array_slice($rpi_avail,0,5);
	$rpi_avail_str=implode(',',$rpi_avail);
	setMyntraCookie('RPI_AVAIL',$rpi_avail_str,time()+3600*24*365,'/',$cookiedomain);
	$avail_session_id_content = $_COOKIE['__avail_session__'];
	if(!empty($avail_session_id_content)){
		$avail_session_id=$avail_session_id_content;
	}

	$avail_params[] = array("method"=>"logPurchase",
			"params"=>array("ProductIDs"=>$ids,"SessionID"=>$avail_session_id,"Prices"=>$prices,"OrderID"=>$orderid,"UserID"=>$orderLogin));
	//availswitchoff
	//$url = Helper::getPropertyValue('recoServiceUrl');
	//post_async(HostConfig::$selfHostURL."/asyncGetPostHandler.php",array("url"=>$url,"method"=>"POST","data"=>json_encode($avail_params),"user"=>$user));
	
	/*****END AVAIL******/

    //mobile verification code for COD
    if( strtolower($paymentoption) == 'cod'){
        $smsVerifier = SmsVerifier::getInstance();
        $mymyntra = new MyMyntra(mysql_real_escape_string($XCART_SESSION_VARS['login']));

        $userProfileData = $mymyntra->getUserProfileData();

        // check for mobile number non-availability(happens in case of user registers from FB)
        if(empty($userProfileData['mobile'])){
            $smarty->assign("verify_mobile_for_COD","false");

        } else {
            if($verifyMobileforCOD){
                $retval = $smsVerifier->checkCODVerifiedStatus($userProfileData['mobile'],$userProfileData['login']);
                if(!$retval){
                    $smarty->assign("mobileToverify", $userProfileData['mobile']);
                    $smarty->assign("verify_mobile_for_COD","true");
                }
            }
        }
    }
    //mobile verification code for COD ends here
    
    //Loyalty Points integration starts
    include_once "$xcart_dir/modules/loyaltyPoints/LoyaltyPointsPortalClient.php";
    include_once "$xcart_dir/modules/loyaltyPoints/LoyaltyPointsPortalHelper.php";

    if($loyaltyEnabled){
    	/*$totalLoyaltyPointsAwarded = 0;
    	$loyaltyPointsAwarded = LoyaltyPointsPortalClient::getConfirmationOrderInfo($orderid);
    	if(!empty($loyaltyPointsAwarded) && (!isset($loyaltyPointsAwarded['activePointsCredited']) || !isset($loyaltyPointsAwarded['inActivePointsCredited']))){
    		$totalLoyaltyPointsAwarded = intval($loyaltyPointsAwarded['activePointsCredited']) + intval($loyaltyPointsAwarded['inActivePointsCredited']);
    	}*/
    	$totalLoyaltyPointsAwarded = round($loyaltyPointsAwarded);
    	$smarty->assign('totalLoyaltyPointsAwarded',$totalLoyaltyPointsAwarded);
    	$smarty->assign('loyaltyDisclamer',WidgetKeyValuePairs::getWidgetValueForKey("loyalty.confirmpage.disclaimer"));
    	//Need to reset the header value in this case
    	LoyaltyPointsPortalHelper::resetLoyaltyPointsHeaderDetails($login); // This will put smarty variable 
    }

    //Loyalty Points integration ends here

// Social INtegrtion
	require_once("$xcart_dir/socialInit.php");
	$socialArray=getFormattedArrayForSocial($socialids);
	$styleDetailsArray = getStyleDataArrayForSocial($socialids);
	$socialPost=0;
	if(!empty($socialArray)){
		$socialPost=1;
	}
	$smarty->assign('socialPost',$socialPost);
	$smarty->assign('socialArray',json_encode($socialArray));	
	$smarty->assign('socialPostStyleDetailsArray',json_encode($styleDetailsArray));
	
	$smarty->assign("orderid", $orderid);
	if($amountafterdiscount<0)
        $amountafterdiscount = (int) $amountafterdiscount;
	if($finalAmountTobePaid<0)
		$finalAmountTobePaid = (int) $finalAmountTobePaid;

	$smarty->assign("finalAmountTobePaid", $finalAmountTobePaid);
    $smarty->assign("shippingCharges", $shippingCharges);
	$smarty->assign("amountafterdiscount", $amountafterdiscount);
	$smarty->assign("highestPriceArticleType",$highestPriceArticleType);
	$smarty->assign("userinfo", $order_result[0]);
	$smarty->assign("paymentoption", $paymentoption);
    $smarty->assign("paybyoption",$paybyoption);
    $smarty->assign("couponCode",$couponCode);
	$smarty->assign("cashCouponCode",$cashCouponCode);
	$smarty->assign("totalcashdiscount",$cashdiscount);			
	$smarty->assign("grandTotal",$ordersubtotal);
	$smarty->assign("eossSavings", $productDiscount);
	$smarty->assign("cartLevelDiscount", $cartDiscount);
	$smarty->assign("totalDiscount",number_format($productDiscount+$coupondiscount+$pg_discount+$cartDiscount,2,".",''));
	$smarty->assign("shippingRate",$order_result[0]['shipping_cost']);
	$smarty->assign("coupon_discount",$coupondiscount);
	$smarty->assign("giftamount",$giftCharges);
	$smarty->assign("codCharges",$codCharge);
	$smarty->assign("productsInCart", $productsInCart);	
	$smarty->assign("totalQuantity", count($productsInCart));
	$smarty->assign("emi_charge",$emi_charge);
	$smarty->assign("delivery_date",$delivery_date);
	$smarty->assign("random_integer",rand());//To avoid expensive rand in smarty, assign from here
	if(!empty($cashBackToBeCredited)) 
		$smarty->assign("cashback_tobe_given",$cashBackToBeCredited);
		
	$smarty->assign("fireSyncGACall",$fireSyncGACall);
	$smarty->assign("recordGACall",$recordGACall);

	$fireSyncOmnitureCall = 'false';
	$fireSyncOmnitureCallFG = FeatureGateKeyValuePairs::getFeatureGateValueForKey("confirmation.fireSyncOmnitureCall");
	if($fireSyncOmnitureCallFG=='true') {
		$fireSyncOmnitureCall=true;
	} else {
		$fireSyncOmnitureCall=false;
	}
	$smarty->assign("fireSyncOmnitureCall",$fireSyncOmnitureCall);

	/**
	 * BaseTracker moved from mkorderBook to confirmation page - Starts
	 */	
	if($trackOrderData){
		$tracker->fireRequest();
	}
	/**
	 * BaseTracker moved from mkorderBook to confirmation page - End
	 */

	
	$analyticsObj = AnalyticsBase::getInstance();
	$analyticsObj->setPurchasedEventVars($productsInCart, $paymentoption, $orderid, $totalItemRevenueOmnitureTrack, $cashbackRedeemedOmnitureTrack);
	$analyticsObj->setContextualPageData(AnalyticsBase::CONFIRMATION);
	$analyticsObj->setTemplateVars($smarty);
} else {
	//order status not proper to show confirmation page
	func_header_location($http_location,false);
}

/**
* Kundan: now we track: out of the order items, which were the styles for which user clicked on their size charts: old or new
* We send this array of styles to Mongo DB
* @param styleIdsInCart array
*/
function getStyleIdsWithSizeChartClicked($styleIdsInCart) {
	$styleIdsWithOldSizeChartClicks = getMatchingStyleIds($styleIdsInCart, SizeChartTrackerConstants::OldSizechartCookie, SizeChartTrackerConstants::Delimiter);
	$styleIdsWithNewSizeChartClicks = getMatchingStyleIds($styleIdsInCart, SizeChartTrackerConstants::NewSizechartCookie, SizeChartTrackerConstants::Delimiter);
	return array("old"=>$styleIdsWithOldSizeChartClicks, "new"=>$styleIdsWithNewSizeChartClicks);
}

function getMatchingStyleIds($styleIdsInCart, $cookieName, $delimiter = "|") {
	$cookieValue = getMyntraCookie($cookieName);
	$matchedStyleIds;
	if(!empty($cookieValue)) {
		$styleIdsInCookie = explode($delimiter, $cookieValue);
		$matchedStyleIds = array_intersect($styleIdsInCart, $styleIdsInCookie);
	}
	return $matchedStyleIds;
}

function removeSizeChartClickedCookies() {
	deleteMyntraCookie(SizeChartTrackerConstants::OldSizechartCookie);
	deleteMyntraCookie(SizeChartTrackerConstants::NewSizechartCookie);
}
?>
<!DOCTYPE html>
<html>
<head>
    <?php require_once $xcart_dir."/components/secure_html_head.php" ?>
</head>
<body class="secure confirm">
    <?php $smarty->display('inc/gtm.tpl') ?>
    <div class="container">
        <?php require_once $xcart_dir."/components/checkout_header.php" ?>
        
        <?php $smarty->display('checkout/confirm.tpl') ?>

        <?php require_once $xcart_dir."/components/checkout_footer.php" ?>
    </div>  
    <?php require_once $xcart_dir."/components/secure_html_foot.php" ?>
    <?php $smarty->display('inc/webengage.tpl') ?>
    <?php $smarty->display('inc/ga.tpl') ?>
</body>
</html>
