<?php
require_once "./auth.php";
global $sql_tbl, $xcart_dir,$http_location;
require_once "$xcart_dir/include/func/func.utilities.php";

$pageName = 'other';
include_once $xcart_dir."/webengage.php";

$analyticsObj = AnalyticsBase::getInstance();
$analyticsObj->setStaticPageVars(AnalyticsBase::RESET_PASSWORD);
$analyticsObj->setTemplateVars($smarty);

$resetpwd_key=$_GET['key'];
if(isset($_POST['action']) && $_POST['action'] == 'FORGOTPASSWORD'){
        $mailto = $_POST['username'];
        $template = mysql_real_escape_string($_POST['template']);
        include_once "$xcart_dir/include/func/func.reset_password.php";
        $returned = sendResetPasswordMail($mailto,$template);
        echo $returned."success";return;
}
if($_GET["msg"] === "1") {
    $smarty->assign("link_status","false");
    $smarty->assign("error_msg","Your password has been changed successfully. <br> Now you can <a href='#signin'>Login</a> or <a href='".$http_location."/' >goto Myntra home</a>");
    func_display("reset_password.tpl", $smarty);
    exit;   
}
if(empty($resetpwd_key) || $resetpwd_key == NULL){
	$smarty->assign("link_status","false");
    $smarty->assign("error_msg","Invalid Request. You are not authorized to view this page.");
    func_display("reset_password.tpl", $smarty);
    exit;	
}
if(isset($_POST['method']) && $_POST['method']== 'change_password'){
	$crypted = "";
	$crypted_md5=addslashes(user\UserAuthenticationManager::encryptToMD5($_POST['password']));
	$username = mysql_real_escape_string($_POST['username']);
	$reset_key = mysql_real_escape_string($_POST['reset_key']);
	$new_reset_key = '';
	
	if(empty($reset_key) || $reset_key == NULL || empty($crypted_md5)) {
		$smarty->assign("link_status","false");
		$smarty->assign("error_msg","Invalid Request. You are not authorized to view this page.");
		func_display("reset_password.tpl", $smarty);
		exit;	
	}
	
    $upd=array("password"=>$crypted,"password_md5"=>$crypted_md5,"resetpwd_key" => $new_reset_key);
	$where="resetpwd_key ='".$reset_key."'"; 
	$userArray = func_query_first("select login from xcart_customers where resetpwd_key='$reset_key'");
    $result_cp=func_array2update('xcart_customers', $upd, $where);
    fireAsync(HostConfig::$portalIDPSessionHost, 'idp/session/clearCache/'.urlencode($userArray['login']));
	if($result_cp){
		
		x_session_register("identifiers",array());
		x_session_register("payment_cc_fields");
		$payment_cc_fields = array();
		func_unset($identifiers,"C");
		
		if (x_session_is_registered("productsInCart")) x_session_unregister("productsInCart");

		if (x_session_is_registered("coupondiscount")) x_session_unregister("coupondiscount");

		if (x_session_is_registered("couponCode")) x_session_unregister("couponCode");

		if (x_session_is_registered("cashCouponCode")) x_session_unregister("cashCouponCode");

		if (x_session_is_registered("cashdiscount")) x_session_unregister("cashdiscount");

		if (x_session_is_registered("offers")) x_session_unregister("offers");

		if (x_session_is_registered("totaloffers")) x_session_unregister("totaloffers");

		if (x_session_is_registered("totaldiscountonoffer")) x_session_unregister("totaldiscountonoffer");

		if (x_session_is_registered("productids")) x_session_unregister("productids");	
		
		if (x_session_is_registered("grandTotal")) x_session_unregister("grandTotal");	
		
		if (x_session_is_registered("amountAfterDiscount")) x_session_unregister("amountAfterDiscount");	

		if (x_session_is_registered("userfirstname")) x_session_unregister("userfirstname");
		if (x_session_is_registered("referer_url")) x_session_unregister("referer_url");
	
		if (x_session_is_registered("mobile")) x_session_unregister("mobile");
		if (x_session_is_registered("mcartid")) x_session_unregister("mcartid");
		if (x_session_is_registered("mcartidsaved")) x_session_unregister("mcartidsaved");
		$XCART_SESSION_VARS["totalQuantity"]=0;
		$XCART_SESSION_VARS['firstname'] ="" ;
		
		$login = "";
		$login_type = "";
		$cart = "";
		$access_status = "";
		$merchant_password = "";
		$logout_user = true;
		
		x_session_unregister("hide_security_warning");
		x_session_unregister("login");
		x_session_unregister("login_type");
	
		$logout_event = "success";
		x_session_register("logout_event");
		x_session_unregister("user_account_status");
		x_session_unregister("user_account_membership_details");
		if (x_session_is_registered("fb_uid")) {
			x_session_unregister("fb_uid");
		}
	        
		if(x_session_is_registered("lastAddressUsedID")) {
			x_session_unregister("lastAddressUsedID");
		}
		$smarty->assign("link_status","false");
		header("Location: $http_location/reset_password.php?msg=1");
		exit;
	}
}

$status = func_query("SELECT * FROM $sql_tbl[customers] WHERE resetpwd_key='".$resetpwd_key."'");
if(empty($status)){
	$link_status="false";
	$smarty->assign("error_msg","Sorry, this link has expired. Please click on the link in the latest email sent in response to your last request for password reset.");
}
else {
	$link_status="true";
}
$smarty->assign("link_status",$link_status);
$smarty->assign("user_details",$status);
$smarty->assign("reset_key",$resetpwd_key);
func_display("reset_password.tpl", $smarty);
//}
?>