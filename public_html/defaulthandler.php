<?php
/**
* @author Abhijeet Dev <abhijeet.dev@myntra.com>
* @version $Id$
*/
include_once './auth.php';
$request = $_SERVER['REQUEST_URI'];
$request =  sanitize_html_string(urldecode($request));


header('HTTP/1.1: 404 OK');
$errordocument = '<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
<html>
<head>
    <title>404 Not Found</title>
</head>
<body>
    <h1>Not Found</h1>
    <p>The requested URL '.$request.' was not found on this server.</p>
    <hr />
    <address>www.myntra.com</address>
</body>
</html>';

echo $errordocument;
exit;
?>
