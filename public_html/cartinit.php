<?php
// Header Cart Total Amount
use pageconfig\manager\CachedBannerManager;
use enums\pageconfig\PageConfigLocationConstants;
use abtest\MABTest;
use enums\pageconfig\PageConfigSectionConstants;

/** Setting login background image */
$loginSignupABTest = MABTest::getInstance()->getUserSegment('loginSignupABTest');
$smarty->assign("loginSignupABTest",$loginSignupABTest);

if($loginSignupABTest == 'test'){
	$loginBigBannersEnabled = FeatureGateKeyValuePairs::getBoolean("loginBigBannersEnabled",false);
	if($loginBigBannersEnabled){
		$newLoginBanners = CachedBannerManager::getCachedBanners(PageConfigLocationConstants::NewLoginPageBig, PageConfigSectionConstants::StaticImage, "");		
	}
	else{
		$newLoginBanners = CachedBannerManager::getCachedBanners(PageConfigLocationConstants::NewLoginPage, PageConfigSectionConstants::StaticImage, "");
	}
	if(empty($newLoginBanners[0]) || empty($newLoginBanners[1])){
		$newLoginBanners = '';		
	}
	$smarty->assign("newLoginBanners",$newLoginBanners);
	$smarty->assign("loginBigBannersEnabled",$loginBigBannersEnabled);
	
}
else{
	$loginImageProps = CachedBannerManager::getCachedBanners(PageConfigLocationConstants::LoginPage, PageConfigSectionConstants::StaticImage, "");
	if(!empty($loginImageProps) && !empty($loginImageProps[0])) {		
		$loginImg = $loginImageProps[0]["image_url"];
	} else {
		$loginImg = "";
	}
	$smarty->assign("loginbg",$loginImg);
}
/** END Login BG Image**/



/** Setting default banner image **/
$topNavBanner = CachedBannerManager::getCachedBanners(PageConfigLocationConstants::TopNavDefault, PageConfigSectionConstants::StaticImage, "");
if(!empty($topNavBanner) && !empty($topNavBanner[0])) {
	$smarty->assign("topNavBannerProperties",$topNavBanner[0]);
}
/** End default banner image **/

$smarty->assign('totalAmountCart',XSessionVars::get("CART:totalAmountCart", 0));
$smarty->assign('cartTotalQuantity', XSessionVars::get("CART:cartTotalQuantity", 0));
$smarty->assign('totalQuantity', XSessionVars::get("CART:totalQuantity", 0));
$smarty->assign('savedItemCount', XSessionVars::get("CART:savedItemCount", 0));

//Header RECENT COUNT excluded current product
$showRecent = FeatureGateKeyValuePairs::getBoolean('recent_viewed.enable');
if($showRecent){
	$recent_views=array();
	$recent_views_cookie_content = getMyntraCookie('RVC001');
	if(!empty($recent_views_cookie_content))
		$recent_views=explode(',',$recent_views_cookie_content);
		$recent_views=array_unique($recent_views);
		$recent_views_count=sizeof($recent_views);
//echo $recent_views_count;
		$smarty->assign('recentViewedCount',$recent_views_count);
		$smarty->assign('recentViewedCount',$recent_views_count);
		$smarty->assign('showRecent',$showRecent);
}

//SAVED
$smarty->assign('savedItemCount', XSessionVars::get("CART:savedItemCount", 0));
$smarty->assign("currentPage",$_SERVER["PHP_SELF"]);
