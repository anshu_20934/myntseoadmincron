<?php
//include_once("include/class/class.feature_gate.keyvaluepair.php");
require_once("./auth.php");
require_once($xcart_dir."/include/func/func.db.php");
require_once($xcart_dir."/include/func/func.utilities.php");
require_once($xcart_dir."/include/func/func_sku.php");
require_once($xcart_dir."/include/func/func.mkcore.php");
require_once($xcart_dir."/modules/apiclient/SkuApiClient.php");
require_once($xcart_dir."/modules/apiclient/ItemApiClient.php");
include_once($xcart_dir."/include/class/class.feature_gate.keyvaluepair.php");

if(isset($_POST["styleid"]) ) {
	$styleid=mysql_real_escape_string(filter_input(INPUT_POST,"styleid", FILTER_SANITIZE_NUMBER_INT));
}
else{
	$styleid=mysql_real_escape_string(filter_input(INPUT_GET,"styleid", FILTER_SANITIZE_NUMBER_INT));
}

$options = RequestVars::getVar("options", null);
if(!empty($options)) {
	$allProductOptionDetails=json_decode($options);
}

$sizeParamsArray=getStyleDetailsForSizeChart(mysql_real_escape_string($styleid));
//echo "<PRE>allProductOptionDetails in sizechart:"; print_r($allProductOptionDetails); echo "</PRE>";
//ECHO THE CONTENT
//Kundan: don't load master category & sub category from mk_style_properties table: it is erroneous at best.load it from article-type-id
$articleTypeId =$sizeParamsArray[0]['global_attr_article_type'];//e.g. 95;
$ageGroup = $sizeParamsArray[0]['age_group'];
$brandid = $sizeParamsArray[0]['brand_id'];
$masterCategoryId = $sizeParamsArray[0]['master_category'];
$subCategoryId = $sizeParamsArray[0]['sub_category'];
$articleTypeName=$sizeParamsArray[0]['article_type_name'];
$sizeRepresentationImageUrl=$sizeParamsArray[0]['size_representation_image_url'];
$productDisplayName=$sizeParamsArray[0]['product_display_name'];
//PORTAL-3202: check for a newly passed flag: sd: style disabled
//if style is disabled, then each of its options should appear as disabled
$isStyleDisabled = filter_input(INPUT_POST, "isd",FILTER_SANITIZE_NUMBER_INT);

$sizeOptionsArray=getScaling($articleTypeId,$masterCategoryId,$ageGroup,$brandid,$styleid,$allProductOptionDetails,$isStyleDisabled);
//echo "<PRE>size options array:";print_r($sizeOptionsArray[0]); echo "</PRE>";

$smarty->assign('styleid',$styleid);
//$smarty->assign('brandid',$brandid);
$smarty->assign('style_attributes',$sizeParamsArray[0]);
$smarty->assign('sizeOptionsArray',$sizeOptionsArray[0]);
$smarty->assign('sizeOptionsLabel',$sizeOptionsArray[1]);
$smarty->assign('sizeOptionMeasurements',json_encode($sizeOptionsArray[2]));
$smarty->assign('styleSizeAttributes',json_encode($sizeOptionsArray[3]));
$smarty->assign('sizearraylength',sizeof($sizeOptionsArray[0][0]));
$smarty->assign('totalMappedSizes',sizeof($sizeOptionsArray[0]));
$smarty->assign("sizeRepresentationImageUrl",$sizeRepresentationImageUrl);
$smarty->assign("productDisplayName",$productDisplayName);
//PORTAL-2116: Kundan: if this feature gate is enabled,for every size chart click,append the style ids to cookie
//& during order confirmation page load, pass the style-ids to Mongo DB
use enums\base\SizeChartTrackerConstants;
$trackSizeChartClicks = FeatureGateKeyValuePairs::getBoolean("sizechart.clicktracking.enabled", false);
$smarty->assign("trackSizeChartClicks",$trackSizeChartClicks);
//Kundan: prepare a json string out of the SizeChartTrackerConstants
if($trackSizeChartClicks) {
	$cookieConstants = array(
		"OldSizechartCookie" => SizeChartTrackerConstants::OldSizechartCookie,
		"NewSizechartCookie" => SizeChartTrackerConstants::NewSizechartCookie,
		"Delimiter" => SizeChartTrackerConstants::Delimiter,
		"Expiry" => SizeChartTrackerConstants::Expiry,
		"MaxStylesPerCookie" => SizeChartTrackerConstants::MaxStylesPerCookie
	);
	$smarty->assign("cookieConstants",$cookieConstants);
}
//Kundan: for getting size measurements related texts
use size\SizeMeasurementDisplay;
$sizeMeasurementDisplay = new SizeMeasurementDisplay($articleTypeId, $articleTypeName, $ageGroup, $masterCategoryId, $subCategoryId);
$smarty->assign("measurementGuideText",$sizeMeasurementDisplay->getMeasurementGuideText());
$smarty->assign("measurementMasterCategory",$sizeMeasurementDisplay->getMasterCategoryText());
$smarty->assign("measurementToleranceText",$sizeMeasurementDisplay->getMeasurementToleranceText());
$smarty->assign("measurementNote",$sizeMeasurementDisplay->getNoteForMasterCategory());
$smarty->assign("masterCategoryId",$masterCategoryId);

//echo "<PRE>";
//print_r($sizeOptionsArray);
//print_r($sizeOptionsArray[2]);
if($skin=='skin2'){
	$div_content=$smarty->fetch("pdp/size_chart.tpl", $smarty);
}
else{
$div_content=$smarty->fetch("product_new_tpl/size_chart.tpl", $smarty);
}
echo $div_content;

function getStyleDetailsForSizeChart($productstyleid){
		$productDetailsQuery="
			select sp.product_display_name, sp.size_representation_image_url, sp.global_attr_brand as brand,
			sp.global_attr_age_group as age_group, typ.typename as article_type_name, sp.global_attr_article_type,
			mf.logo as brand_logo, matv.id as brand_id, mc.id as master_category, sc.id as sub_category
			from mk_style_properties sp
			left join mk_product_style ps on ps.id=sp.style_id
			left join mk_catalogue_classification typ on sp.global_attr_article_type=typ.id
			left join mk_catalogue_classification mc on typ.parent2=mc.id
			left join mk_catalogue_classification sc on typ.parent1=sc.id
			left join mk_filters mf on (mf.logo is not null and mf.filter_name=sp.global_attr_brand)
			left join mk_attribute_type_values matv on (matv.attribute_type = 'Brand' and matv.attribute_value=sp.global_attr_brand)
			where sp.style_id='$productstyleid'";	
		$productDetailsQueryResult = func_query($productDetailsQuery,true);	
		return $productDetailsQueryResult;
}


function getScaling($articleTypeId,$masterCategoryId,$ageGroup,$brandid,$styleid,$allProductOptionDetails,$isStyleDisabled)
{
	if(empty($articleTypeId) || empty($ageGroup)) {
		return 0;
	}
	$output=array();
	// trail is added to end of all queries: mainly to decide the restrictions at the last step on styleid,brandid, article-type-id,age-group
	$trail = getFilterClauseAsSQLTrail($articleTypeId, $masterCategoryId, $ageGroup, $brandid, $styleid);
	
	$SizeGroupNames = array();
	$col = array();
	$Unifiedsize =array();
	
	//valid is value_id
	$query = 	"select distinct id_size_chart_scale_fk as groupid,unifiedSizeValueId as valid ,human_readable_scale_name as unifiedSizeScaleName,sca.size_value as unifiedSizeValueName,unifiedSize
	 			from mk_size_unification_rules as rule 
				join size_chart_scale_value as sca
				on sca.id=unifiedSizeValueId
				join size_chart_scale as scales
				on sca.id_size_chart_scale_fk = scales.id
				where article_type_id = $articleTypeId and age_group = '{$ageGroup}' and brandId = $brandid ";
	$query = $query . $trail;
	$query = $query ."order by sca.id_size_chart_scale_fk,sca.intra_scale_size_order";
	$result = func_query($query,true);
	for($i=0;!empty($result[$i]);$i++) {
		//In Col : transforming resultset to hashmap/assoc array: the size-group id: e.g. id of UK, and size-value-id become keys.
		$col[$result[$i][groupid]][$result[$i][valid]] =$i;		
		$output[$i][0] = "";
		$output[$i][1]=$result[$i][unifiedSizeValueName];//e.g. 8,9,10,10.5
		$output[$i][scalevalue]=$result[$i][valid];//Size value id
	}
	/* echo "<pre>unification rule fetch qry is:". $query.". output:";
	print_r($output);
	echo "<br>result:";print_r($result);
	echo "</pre>";
 */	
	//Kundan: $SizeGroupNames index starts from 1. 
	//$SizeGroupNames[1] stores the unified size scale name e.g. in shoes, its UK/IN Size
	$SizeGroupNames[1]=$result[0][unifiedSizeScaleName];//e.g. UK/IN Size
	$unifiedSizeGroupId;
	$unifiedSizeGroupId = $result[0][groupid];
	if(empty($unifiedSizeGroupId)) {
		$unifiedSizeGroupId=-1;
	}
	//echo"<pre>";print_r($result);exit;
	
	//Get the Size scale names other than Unified Size Scale and store it in $sizeGroupNames
	$query = 	"select distinct id_size_chart_scale_fk as sizeGroupId,human_readable_scale_name as sizeScalename 
				from mk_size_unification_rules as rule 
				join size_chart_scale_value as sca
				on sca.id=sizevalueId
				join size_chart_scale as scales
				on sca.id_size_chart_scale_fk = scales.id
				where article_type_id = $articleTypeId and age_group = '$ageGroup' and brandId = $brandid 
				and $unifiedSizeGroupId!=id_size_chart_scale_fk";
	$query = $query . $trail;
	$result = func_query($query,true);
	$row = array();
	for($i=0;!empty($result[$i]);$i++)
	{
		$row[$result[$i][sizeGroupId]] = $i+2;
		$SizeGroupNames[$i+2]=$result[$i][sizeScalename];
	}
	$numrow = sizeof($row)+1;
	//echo"<pre>size  "  ;print_r(sizeof($row));exit;
	$query = 	"select unifiedSize,unifiedSizeTable.id_size_chart_scale_fk as unifiedSizeGroupId,
				unifiedSizeValueId, size,sizeTable.id_size_chart_scale_fk as sizeGroupId,
				sizeTable.intra_scale_size_order as sizeInternalOrder,
				sizeTable.size_value as sizeValueName from mk_size_unification_rules as rule
				join  size_chart_scale_value as sizeTable on sizeTable.id=rule.sizevalueId
				join  size_chart_scale_value as unifiedSizeTable on unifiedSizeTable.id=rule.unifiedSizeValueId
				and $unifiedSizeGroupId!=sizeTable.id_size_chart_scale_fk 
				where article_type_id = $articleTypeId and age_group = '$ageGroup' and brandId = $brandid ";
	
	
	$query = $query . $trail." order by unifiedSizeTable.id_size_chart_scale_fk,
				unifiedSizeTable.intra_scale_size_order,sizeTable.id_size_chart_scale_fk,sizeTable.intra_scale_size_order";
	$result = func_query($query,true);
	
	//Fill the 2d array	echo"num<pre>num ";print_r($query);exit;
	//output is the matrix of the sizes to be filled
	for($i=0;!empty($result[$i]);$i++)
	{
		$temp_unif_grp = $result[$i][unifiedSizeGroupId];
		$temp_unif_id = $result[$i][unifiedSizeValueId];
		$temp_col = $col[$temp_unif_grp][$temp_unif_id];//this is column index
		$temp_size_grp = $result[$i][sizeGroupId];
		$temp_row =$row[$temp_size_grp];//this is row index
		$output[$temp_col][$temp_row] =$result[$i][sizeValueName];
	}
	
	//Out of stock check, SKU count check, send SKU id
	
	if(!(empty($articleTypeId)) && !(empty($ageGroup)))
	{
		$query = "select distinct sku_id, option_id from mk_styles_options_skus_mapping where style_id = $styleid order by option_id";
		$results = func_query($query,true);
		$count =0;
		//copy results into filter
		$filter=array();
		
		foreach ($results as $row) {  
 			$skuIds[] = $row['sku_id'];
			$count++;
		}
		//echo"hh";print_r($skuIds);exit;
		$sku_available_counts = array();
		$activeCount = $allProductOptionDetails;
		for($i=0;!empty($activeCount[$i]);$i++) {
			$activeCount[$i] = (array) $activeCount[$i];	
			$sku_available_counts[$activeCount[$i][sku_id]] = $activeCount[$i][is_active];
		}
		$checkInStock = FeatureGateKeyValuePairs::getBoolean("checkInStock", true);
		for($i=0;$i<$count;$i++) {
			$skuid = $results[$i]['sku_id'];
			if(!$isStyleDisabled && ($sku_available_counts[$skuid]>0 || $sku_available_counts[$skuid] == true || !$checkInStock)) {
				$results[$i][avail] = "true";
			}
			else {
				$results[$i][avail] = "false";
			}
			$filter[$i]=$results[$i];
		}
		//echo"resulrs are <pre>";print_r($filter);exit;
		//get all available options;
		$option_ids = "(";
		for($i=0;$i<$count;$i++) {
			if($i!=0) {
				$option_ids = $option_ids.",";
			}
			$option_ids = $option_ids."'".$results[$i][option_id]."'";
		}
		$option_ids = $option_ids.")";
		$query =  "select id,value as size,unified_size_value_id,size_representation 
				  from mk_product_options where id in $option_ids and name = 'Size' order by id";
		//add some more info to filter: option_id, originalSize,scalevalue,size_representation
		$results = func_query($query,true);
		for($i=0;$i<$count;$i++) {
			for($j=0;$j<$count;$j++) {
				if($filter[$i][option_id] == $results[$j][id]) {
					$filter[$i][originalSize] = $results[$j][size];				
					$filter[$i][scalevalue] = $results[$j][unified_size_value_id];
					$filter[$i][size_representation] = $results[$j][size_representation];
					break;
				}
			}
		}
	}
	
	$filteredOutput = array();
	$j=0;
	$jsonOutput = array();
	for($i=0;!empty($output[$i]);$i++) {
		for($k=0;!empty($filter[$k]);$k++) {
			if( $filter[$k][scalevalue]== $output[$i][scalevalue]) {
				$filteredOutput[$j]=$output[$i];
				$filteredOutput[$j][0]=$filter[$k][sku_id];
				$filteredOutput[$j][$numrow+1] = $filter[$k][avail];
				if(!empty($filter[$k][size_representation])) {
					$jsonOutput[$j]= json_decode(strtolower($filter[$k][size_representation]));
				}
				else {
					$jsonOutput[$j] = "NA";
				}
				unset($filteredOutput[$j][scalevalue]);
				$j++;
				break;
			}
		}
	}
	
	//Kundan: here, populate another row: for measurements for Footwear
	//Note that $filteredOutput and $SizeGroupNames are passed by reference: they get modified in the function and are reflected back here.
	augmentLengthInSizeChartInFootwear($filteredOutput, $SizeGroupNames, $masterCategoryId, $jsonOutput, $numrow);
	
	//fetch the attributes, e.g. Sleeve: (Short, long, 3/4th)
	$attributes = getStyleAttributes($styleid);
	
	$returnValue = array();
	$returnValue[0] = $filteredOutput;//sizes
	$returnValue[1] = $SizeGroupNames;//labels
	$returnValue[2] = $jsonOutput;//measurements
	$returnValue[3] = $attributes;
	//echo"<pre> result  "  ;print_r($returnValue);echo"</pre>";
	return $returnValue; 
}

function getFilterClauseAsSQLTrail($articleTypeId,$masterCategoryId,$ageGroup,$brandid,$styleid = '') {
	// trail is added to end of all queries: mainly to decide the restrictions at the last step on styleid
	//trail is applied e.g. while fetching unification rule	
	$trail = "";	
	//whether to choose unification rule involving style_id or not
	$query = 	"select count(distinct unifiedSize) from mk_size_unification_rules where article_type_id = $articleTypeId and age_group = '$ageGroup'
				and brandId = $brandid and styleID = $styleid";
	$numUnifiedSizes = func_query_first_cell($query, true);
	if($numUnifiedSizes > 0) {
		$trail = " and styleID = $styleid ";
	}
	else {
		$trail = " and styleID is NULL ";
	}		
	return $trail;
}

function getStyleAttributes($styleid) {
	//fetch the attributes, e.g. Sleeve: (Short, long, 3/4th)
	$attributes = array();
	$query = "select distinct attr.attribute_type as type,map.attribute_value as value
		 from mk_style_properties as sp, mk_style_article_type_attribute_values as st,mk_attribute_type_values as map,mk_attribute_type as attr
		  where st.article_type_attribute_value_id = map.id and map.attribute_type_id = attr.id and sp.style_id=st.product_style_id 
		  and sp.style_id=$styleid";
	$result=func_query($query,true);
	for($i=0;!empty($result[$i]);$i++) {
		$type = $result[$i][type];
		$value = $result[$i][value];
		$attributes[$type] = $value;
	}
	return $attributes;
}

function augmentLengthInSizeChartInFootwear(&$filteredOutput,&$SizeGroupNames, $masterCategoryId, $jsonOutput, $numrow) {
	$measureOrigName = array("to fit foot length");
	$measureName;
	$found = false;
	//PORTAL-3142
	//The first product option would have measurements json in jsonOutput[0]. 
	//iterate over it to find a measurement called: "to fit foot length" or "length" in case insensitive manner
	//assumption is that all product options will have the same measurement name
	foreach(get_object_vars($jsonOutput[0]) as $eachMeasure=>$value){
		if(in_array(strtolower($eachMeasure), $measureOrigName)){
			$found = true;
			$measureName = $eachMeasure;
			break;
		}
	}//Kundan: here, populate another row: for measurements for Footwear
	if($found && $masterCategoryId == 10) {
		$numSizes = count($jsonOutput);
		//check if each of the lengths have some value and whether all of them have the same unit
		$showLength = true;
		$unit = ($jsonOutput[0]->$measureName && $jsonOutput[0]->$measureName->value->unit) ? $jsonOutput[0]->$measureName->value->unit : "";
		foreach($jsonOutput as $eachMeasurement) {
			if(
				!$eachMeasurement->$measureName || $eachMeasurement->$measureName->type != "flat" ||
				empty($eachMeasurement->$measureName->value) || empty($eachMeasurement->$measureName->value->unit) ||
				empty($eachMeasurement->$measureName->value->value) || $eachMeasurement->$measureName->value->unit != $unit
			){
				$showLength = false;
				break;
			}
		}
		if($showLength) {
			$sizeIndex;
			for($sizeIndex=0; $sizeIndex<$numSizes; $sizeIndex++) {
				$filteredOutput[$sizeIndex][$numrow+2] = $filteredOutput[$sizeIndex][$numrow+1];
				$filteredOutput[$sizeIndex][$numrow+1] = $jsonOutput[$sizeIndex]->$measureName->value->value;
			}
			$SizeGroupNames[] = $measureName . ((!empty($unit))?"($unit)":"");
		}
	}
}
?>
