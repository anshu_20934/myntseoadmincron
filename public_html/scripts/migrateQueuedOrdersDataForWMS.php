<?php

chdir(dirname(__FILE__));
require "../auth.php";
include_once("$xcart_dir/include/func/func.order.php");

$orders = func_query("SELECT orderid from xcart_orders where status = 'Q'");

$orderIds = array();
foreach( $orders as $order ) {
    $orderIds[] = $order['orderid'];
}

$update_orders_sql = "UPDATE xcart_orders set status = 'WP' where orderid in (".implode(',',$orderIds).")";
db_query($update_orders_sql);

$order_detail_query = "UPDATE xcart_order_details SET item_status = 'A', assignee=1, assignment_time = ".time()." where orderid in (".implode(",", $orderIds).")";
db_query($order_detail_query);		

foreach($orderIds as $orderid) {
    func_addComment_status($orderid, "WP", "Q", "WMS Flow. Moving directly to WP from Q", "AUTOSYSTEM");
}

?>
