<?php
// https://stackoverflow.com/questions/2867057/how-do-i-turn-off-php-notices
error_reporting(E_ERROR | E_WARNING | E_PARSE);

echo memory_get_usage();
ini_set('memory_limit', '1024M'); 

define('XCART_START',1);
$xcart_dir = realpath(dirname(__FILE__) . "/..");
echo("\nxcart_dir: " . $xcart_dir . "\n");

require_once ($xcart_dir."/config.php");
include_once ($xcart_dir."/prepare.php");
include_once ($xcart_dir."/logger.php");
require_once ($xcart_dir."/include/class/ShutdownHook.php");
include_once ($xcart_dir."/include/class/cache/XCache.php");
$xcache = new XCache();

// class loaders
include_once ("$xcart_dir/include/class/MClassLoader.php");
MClassLoader::addToClassPath(\HostConfig::$documentRoot."/include/class/");
MClassLoader::addToClassPath(\HostConfig::$documentRoot."/modules/");
MClassLoader::addToClassPath(\HostConfig::$documentRoot."/env/");
MClassLoader::addToClassPath(\HostConfig::$documentRoot."/platform/");
MClassLoader::addToClassPath(\HostConfig::$documentRoot."/admin/class/");

require_once ($xcart_dir."/Predis/Autoloader.php");
Predis\Autoloader::register();

include_once ($xcart_dir."/include/bench.php");
require_once ($xcart_dir."/include/func/func.db.php");
require_once ($xcart_dir."/databaseinit.php");

include_once ($xcart_dir."/include/class/seo/SeoAdminCache.php");
echo ("\n\ncron to set the SEO Admin cache has started\n");
$expiry=WidgetKeyValuePairs::getWidgetValueForKey("seoAdminCacheExpiry");
echo ("\n\ncron to get widget keys SEO Admin cache has started\n");
SeoAdminCache::init($expiry);
echo memory_get_usage();
echo ("\n\ncron to get init SEO Admin cache has started\n");
SeoAdminCache::setCacheForAdminData();
echo memory_get_usage();
echo "\ncron to set the the SEO Admin cache has ended\n";
?>
