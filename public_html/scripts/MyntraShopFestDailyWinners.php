<?php

/*
 * This function updates daily_winners table by putting top N winners from shopping_festival_leader_board table
 */
require_once "../admin/auth.php";
include_once("$xcart_dir/include/class/widget/class.widget.keyvaluepair.php");

echo "cron to insert into daily_winners table begins................\n";

$n = WidgetKeyValuePairs::getWidgetValueForKey('fest_topNbuyers');
$nCat = WidgetKeyValuePairs::getWidgetValueForKey('fest_topNCategoryBuyers');

$noDays = WidgetKeyValuePairs::getWidgetValueForKey('fest_daysto_fix_daily_winners');

$overAllCategory = 3;
$womenCategory = 2; 
$menCategory = 1;

$date = array();

for($i=0;$i<$noDays;$i++){
$date[$i] = date("Y-m-d", strtotime( '-'.($i+1).' days'));
}

$query = "";

$date_range = "(";

for($i = 0;$i<$noDays-1;$i++){
	$date_range .="'".$date[$i]."',";
}
	$date_range .="'".$date[$noDays-1]."')";
//print_r($date_range);

echo "\n";
	

$dquery .= "DELETE FROM `daily_winners` where date in $date_range;";

$results = db_query($dquery);
$query = "INSERT INTO `daily_winners` (`name`,`date`,`amount`,`category`,`login`,`position`)";
for($i = 0;$i<$noDays-1;$i++){
	
	$query.= "(select name,date,amount,category,login,@rank".$i." := @rank".$i."+1  as position from ( SELECT name,date,sum(amount) as amount, category, login from shopping_fest_leader_board where date='".$date[$i]."' and category= $overAllCategory group by login order by amount desc limit ".$n.") as b , (select @rank".$i." := 0) r)";
	$query.=" union ";

	$query.= "(select name,date,amount,category,login,@rank2".$i." := @rank2".$i."+1  as position from (SELECT name,date,sum(amount) as amount, category,login from shopping_fest_leader_board where date='".$date[$i]."' and category= $womenCategory group by login order by amount desc limit ".$nCat.") as b , (select @rank2".$i." := 0) r)";
	$query.=" union ";

	$query.= "(select name,date,amount,category,login,@rank1".$i." := @rank1".$i."+1  as position from (SELECT name,date,sum(amount) as amount, category,login from shopping_fest_leader_board where date='".$date[$i]."' and category= $menCategory group by login order by amount desc limit ".$nCat.") as b , (select @rank1".$i." := 0) r)";
	$query.=" union ";
}

	$query.= "(select name,date,amount,category,login,@rank".($noDays-1)." := @rank".($noDays-1)."+1  as position from ( SELECT name,date,sum(amount) as amount, category, login from shopping_fest_leader_board where date='".$date[$noDays-1]."' and category= $overAllCategory group by login order by amount desc limit ".$n.") as b , (select @rank".($noDays-1)." := 0) r)";
	$query.=" union ";

	$query.= "(select name,date,amount,category,login,@rank2".($noDays-1)." := @rank2".($noDays-1)."+1  as position from (SELECT name,date,sum(amount) as amount, category,login from shopping_fest_leader_board where date='".$date[$noDays-1]."' and category= $womenCategory group by login order by amount desc limit ".$nCat.") as b , (select @rank2".($noDays-1)." := 0) r)";
	$query.=" union ";

	$query.= "(select name,date,amount,category,login,@rank1".($noDays-1)." := @rank1".($noDays-1)."+1  as position from (SELECT name,date,sum(amount) as amount, category,login from shopping_fest_leader_board where date='".$date[$noDays-1]."' and category= $menCategory group by login order by amount desc limit ".$nCat.") as b , (select @rank1".($noDays-1)." := 0) r)";

//echo $query;
$results = db_query($query);
//var_dump($results);
echo "cron to insert into daily_winners table ends,,,,,,,,,,,,,,,\n";
?>
