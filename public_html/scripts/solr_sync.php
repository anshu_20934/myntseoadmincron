<?php
chdir(dirname(__FILE__));
include_once("../auth_for_solr_index.php");
include_once("../include/solr/solrProducts.php");

function getStylesForServer($host,$port,$count_options){
	echo "function :: getStylesForServer: host:$host port:$port count_options:$count_options\n<br>\n";
	if(!empty($host) && !empty($port)){
		$ch = curl_init("http://$host:$port/solr/sprod/select/?q=count_options_availbale%3A$count_options&version=1.2&indent=on&start=0&rows=25000&fl=styleid&sort=styleid%20desc&wt=json");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		$style_list_out = curl_exec($ch);
		curl_close($ch);
		$style_list = json_decode($style_list_out);
		$style_array=array();
		foreach ($style_list->response->docs as $style_doc) {
			array_push($style_array, $style_doc->styleid[0]);
		}
		return $style_array;
	}
}

function get_mismatching_styles_for_count_option($count_options_available){
	echo "function :: get_mismatching_styles_for_count_option: $count_options_available\n<br>\n";
	$solr_output_list= array();
	global $solr_servers;
	global $solr_port;
	foreach ($solr_servers as $solr_server) {
		array_push($solr_output_list, getStylesForServer($solr_server,$solr_port,$count_options_available));
	}
	$style_id_list = array();
	$last_list=null;
	foreach ($solr_output_list as $solr_output) {
		if(!empty($last_list)){
			$tmp_array = array_diff($last_list, $solr_output);
			foreach ($tmp_array as $style_value) {
				array_push($style_id_list, $style_value);
			}
		}
		$last_list = $solr_output;
	}
	return $style_id_list;
}

$style_ids_to_update = array_merge(	get_mismatching_styles_for_count_option(0),
									get_mismatching_styles_for_count_option(1),
									get_mismatching_styles_for_count_option(2),
									get_mismatching_styles_for_count_option(3),
									get_mismatching_styles_for_count_option(4));

echo "Now updating ids\n<br>\n";
$solrProducts = new solrProducts();
foreach ($style_ids_to_update as $style_id_to_update) {
	echo $style_id_to_update."\n<br>\n";
		$solrProducts->addStyleToIndex($style_id_to_update);
}
if(!empty($style_ids_to_update)){
	$solrProducts->commitToAllServers();
}
