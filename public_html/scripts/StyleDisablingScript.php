<?php
chdir(dirname(__FILE__));
include_once("../auth_for_solr_index.php");
require_once "./styles_to_change.php";
include_once('../include/solr/solrProducts.php');
include_once('../include/solr/solrUpdate.php');

$styles_csv='';

if(empty($data_array)){
	echo "No Styles";
	exit;
}
$i=0;
foreach ($data_array as $value){
	$i++;
	if($i==count($data_array))
	deleteStyleFromSolrIndex($value);
	else
	deleteStyleFromSolrIndex($value,false);
	$styles_csv.=$value.",";
}
$styles_csv=substr($styles_csv,0,strlen($styles_csv)-1);

db_query("update mk_product_style set styleType='A' where id in ($styles_csv)");
echo "Done";

?>