<?php
include_once(dirname(__FILE__)."/../auth_for_solr_index.php");
$query_truncate = "truncate zip_city_orders";
$query_location = "select s_zipcode as location, 'zip' as type ,count(*) as count from xcart_orders
 where status not in ('PP','PV','D','F') and date > (unix_timestamp(now()) - 30*24*60*60) group by s_zipcode union select s_city as location, 'city' as type, count(*) as count from xcart_orders
 where status not in ('PP','PV','D','F') and date > (unix_timestamp(now()) - 30*24*60*60) group by s_city";

$query_insert_prefix = "insert into `zip_city_orders` (location, location_type, no_of_orders) VALUES (";

$results_query_truncate=func_query($query_truncate);
$result_query_location = func_query($query_location,true);

 foreach($result_query_location as $row)
 {
 	$query_insert = $query_insert_prefix . "'".$row['location']."','".$row['type']."',".$row['count'] . ")";
 	func_query($query_insert);
}

?>
