<?php
/*

Add any more indexes as you please .. 
*/

chdir(dirname(__FILE__));
include("../auth.php");
include("../include/solr/solrProducts.php");
include("../include/solr/solrStatsSearch.php");
set_time_limit(0);

$index = new solrProducts();
$index->optimizeIndex();

$index = new solrStatsSearch();
$index->optimizeIndex();

