<?php
require_once  "../authAPI.php"; 
require_once $xcart_dir.'/modules/RestAPI/RestRequest.php';
require_once $xcart_dir.'/env/Host.config.php';
//search expired notifications
$url = \HostConfig::$notificationsUrl.'/searchbyexpiry';
$method  = 'GET';
$request = new \RestRequest($url, $method, $data);
$headers = array();
$headers[] = 'Accept: application/json';
$headers[] = 'Content-type: application/json';
$headers[] = 'Accept-Language: en-US';
$request->setHttpHeaders($headers);
$request->execute();
$info = $request->getResponseInfo();
$body = $request->getResponseBody();

if($info['http_code'] == 200){
	$response = json_decode($body,true);	
	if($response['NotificationResponse']['status']['statusType'] == 'SUCCESS')
	{
		$tempArr = $response['NotificationResponse']['NotificationList']['data'];
		if($response['NotificationResponse']['status']['totalCount'] == 1){
			delOneByOne($tempArr['id']);
		}
		else{
			foreach ($tempArr as $value)
			{
				delOneByOne($value['id']);
			}
		}
	}
}

function delOneByOne($id)
{
	$dataForBulkDecrement = createJSONForBulkDecrement($id);
	$dataForBulkDecrementTotalCount = createJSONForBulkDecrementTotalCount($id);
	$url = \HostConfig::$notificationsUrl.'/?id='. $id;
	$method = 'DELETE';
	$headers = array();
	$headers[] = 'Accept: application/json';
	$headers[] = 'Content-type: application/json';
	$headers[] = 'Accept-Language: en-US';
	$request = new \RestRequest($url, $method);
	$request->setHttpHeaders($headers);
	$request->execute();
	$info = $request->getResponseInfo();
	$body = $request->getResponseBody();
	if($info['http_code'] == 200){
		$response = json_decode($body,true);				
		if($response['NotificationResponse']['status']['statusType'] == 'SUCCESS'){					
			if(count($dataForBulkDecrement)){
				restForBulkDecrement($dataForBulkDecrement);
			}
			if(count($dataForBulkDecrementTotalCount)){
				restForBulkDecrement($dataForBulkDecrementTotalCount);
			}					
			$users = $response['NotificationResponse']['NotificationList']['data'];
			$dataForBulkPut = createJSONforBulkPut($users);
			restForBulkPut($dataForBulkPut);
		}
	}
}

function restForBulkPut($dataForBulkPut)
{
	$url = \HostConfig::$notificationsUpsUrl.'/putAttr';
	$method = 'POST';
	$data = json_encode($dataForBulkPut);
	$request = new \RestRequest($url, $method, $data);
	$headers = array();
	$headers[] = 'Accept: application/json';
	$headers[] = 'Content-type: application/json';
	$headers[] = 'Accept-Language: en-US';
	$request->setHttpHeaders($headers);
	$request->execute();	
}

function createJSONforBulkPut($response){
	$userList = $response['userList'];
	$jsonArr = array();
	
	if (count($userList) == 1)
	{
		$tempUserId	= $userList['userId'];
		$jsonArr[$tempUserId] = array();
		$jsonArr[$tempUserId]['attribute'] = 'totalNotificationCounter';
		$jsonArr[$tempUserId]['sourceid'] = '3';	
		$jsonArr[$tempUserId]['value'] = '-1';

	}
	else{
		foreach ($userList as $value) {
			$tempUserId	= $value['userId'];
			$jsonArr[$tempUserId] = array();
			$jsonArr[$tempUserId]['attribute'] = 'totalNotificationCounter';
			$jsonArr[$tempUserId]['sourceid'] = '3';	
			$jsonArr[$tempUserId]['value'] = '-1';	
		}
	}
	return $jsonArr;
}

function restForBulkDecrement($dataForBulkDecrement){
	$url = \HostConfig::$notificationsUpsUrl.'/bulkDecrement';
	$method = 'POST';
	$data = json_encode($dataForBulkDecrement);
	$request = new \RestRequest($url, $method, $data);
	$headers = array();
	$headers[] = 'Accept: application/json';
	$headers[] = 'Content-type: application/json';
	$headers[] = 'Accept-Language: en-US';
	$request->setHttpHeaders($headers);
	$request->execute();	
}

function createJSONForBulkDecrement($ntfId){	
	$url = \HostConfig::$notificationsUrl.'/'.$ntfId;
	$method = 'GET';
	$headers = array();
	$headers[] = 'Accept: application/json';
	$headers[] = 'Content-type: application/json';
	$headers[] = 'Accept-Language: en-US';
	$request = new \RestRequest($url, $method);
	$request->setHttpHeaders($headers);
	$request->execute();
	$info = $request->getResponseInfo();
	$body = $request->getResponseBody();
	if($info['http_code'] == 200){
		$response = json_decode($body,true);
		if($response['NotificationResponse']['status']['statusType'] == 'SUCCESS'){
			$tempArr = $response['NotificationResponse']['NotificationList']['data']['userList'];
			$jsonArr = array();
			if(count($tempArr[0]) || !$tempArr['read']){
				if(count($tempArr[0])){
					for($i=0; $i<count($tempArr); $i++){
						if(!$tempArr[$i]['read']){
							$jsonArr[$tempArr[$i]['userId']] = array();
							$jsonArr[$tempArr[$i]['userId']]['attribute'] = 'notificationCounter';
							$jsonArr[$tempArr[$i]['userId']]['sourceid'] = '3';					
						}
					}	
				}
				else if(!$tempArr['read']){
					$jsonArr[$tempArr['userId']] = array();
					$jsonArr[$tempArr['userId']]['attribute'] = 'notificationCounter';
					$jsonArr[$tempArr['userId']]['sourceid'] = '3';
				}				
			}
			return $jsonArr;
		}
	}
}
function createJSONForBulkDecrementTotalCount($ntfId){	
	$url = \HostConfig::$notificationsUrl.'/'.$ntfId;
	$method = 'GET';
	$headers = array();
	$headers[] = 'Accept: application/json';
	$headers[] = 'Content-type: application/json';
	$headers[] = 'Accept-Language: en-US';
	$request = new \RestRequest($url, $method);
	$request->setHttpHeaders($headers);
	$request->execute();
	$info = $request->getResponseInfo();
	$body = $request->getResponseBody();
	if($info['http_code'] == 200){
		$response = json_decode($body,true);
		if($response['NotificationResponse']['status']['statusType'] == 'SUCCESS'){
			$tempArr = $response['NotificationResponse']['NotificationList']['data']['userList'];
			$jsonArr = array();
			if(count($tempArr[0])){
				if(count($tempArr[0])){
					for($i=0; $i<count($tempArr); $i++){
						if(!$tempArr[$i]['read']){
							$jsonArr[$tempArr[$i]['userId']] = array();
							$jsonArr[$tempArr[$i]['userId']]['attribute'] = 'totalNotificationCounter';
							$jsonArr[$tempArr[$i]['userId']]['sourceid'] = '3';					
						}
					}	
				}
				else if(!$tempArr['read']){
					$jsonArr[$tempArr['userId']] = array();
					$jsonArr[$tempArr['userId']]['attribute'] = 'totalNotificationCounter';
					$jsonArr[$tempArr['userId']]['sourceid'] = '3';
				}				
			}
			return $jsonArr;
		}
	}
}
?>