<?php
chdir(dirname(__FILE__));
include("../PHPExcel/PHPExcel.php");
include("../auth_for_solr_index.php");


//get all parameters
$filepath = explode('=',$_SERVER['argv'][1]);
$filepath = trim($filepath[1]);

echo "verify all passed parameter--- \n filepath=".$filepath."\n errorpath=".$errorpath."\n isprod=".$isprod;
echo "\n\nif verified and correct, press- 'y', otherwise- 'n' >";
$input = trim(fgetc(STDIN));
if($input == 'n')
{echo "\n\nbye";exit;}

elseif($input == 'y'){
	if($filepath){ 
		$objReader = PHPExcel_IOFactory::createReader('Excel5');//use xls file, use excel2007 for 2007 format
		$objPHPExcel1 = $objReader->load($filepath);
		$style_sheet=$objPHPExcel1->getSheetByName('Sku Codes');
		$lastRow = $style_sheet->getHighestRow();

		$excel = new PHPExcel();
		$excel->setActiveSheetIndex(0);
		$excel->getActiveSheet()->setTitle('Sku Codes'); //renaming it
		
		//here we fill in the header row
		$excel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
		$excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
		$excel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
		$excel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
		$excel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
		$excel->getActiveSheet()->getColumnDimension('F')->setWidth(5);
		$excel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
		$excel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
		
		$excel->getActiveSheet()->setCellValue('A1', 'Brand');
		$excel->getActiveSheet()->setCellValue('B1', 'Vendor Name');
		$excel->getActiveSheet()->setCellValue('C1', 'Category');
		$excel->getActiveSheet()->setCellValue('D1', 'Article Code');
		$excel->getActiveSheet()->setCellValue('E1', 'Product Description');
		$excel->getActiveSheet()->setCellValue('F1', 'Size');
		$excel->getActiveSheet()->setCellValue('G1', 'remarks');
		$excel->getActiveSheet()->setCellValue('H1', 'Generated Sku Code');
		
		for($i=2;$i<=$lastRow;$i++){
			$article_number = (string)$style_sheet->getCell('D'.$i)->getValue();
			$article_name = (string)$style_sheet->getCell('B'.$i)->getValue();
			$size = (string)$style_sheet->getCell('F'.$i)->getValue();
			$brand = (string)$style_sheet->getCell('A'.$i)->getValue();
			$article_type = (string)$style_sheet->getCell('C'.$i)->getValue();
			$product_desc = (string)$style_sheet->getCell('E'.$i)->getValue();
			// get brand id from brand name
			$brand_id = getBrandId($brand);
			// get article type id from article type
			$article_type_id = getArticleTypeId($article_type);
			$remarks=(string)$style_sheet->getCell('G'.$i)->getValue();

			// Save it to new excel file
			$excel->getActiveSheet()->setCellValue('A'.$i, $brand);
			$excel->getActiveSheet()->setCellValue('B'.$i, $article_name);
			$excel->getActiveSheet()->setCellValue('C'.$i, $article_type);
			$excel->getActiveSheet()->setCellValue('D'.$i, $article_number);
			$excel->getActiveSheet()->setCellValue('E'.$i, $product_desc);
			$excel->getActiveSheet()->setCellValue('F'.$i, $size);
			$excel->getActiveSheet()->setCellValue('G'.$i, $remarks);
			
			if(empty($brand_id) || empty($article_type_id) || empty($size) || empty($article_number) || empty($article_name)){
				echo "\n--------> failed to create sku for $article_number article_name $article_name size $size brand $brand_id article_type_id $article_type_id" ;
				$excel->getActiveSheet()->setCellValue('I'.$i, "failed to create sku because of empty value");
				continue;	
			}else{
				$sku_exist=checkForExistingSKU($brand_id, $article_type_id, $size);
				if($sku_exist == 'yes'){
					$excel->getActiveSheet()->setCellValue('I'.$i, "already exist sku for artcle number: $article_number, size:$size, brand:$brand_id");
				}else{
					$excel->getActiveSheet()->setCellValue('I'.$i, "Created");
				}
			}
			echo "\n--------> Createing sku";
			// Here we have to get the options
			$skuCode = generateSkuCode($brand_id, $article_type_id);
			echo "\n--------> Createing sku done for skuCode:".$skuCode;
			// Insert into db
			$sql = "INSERT INTO mk_skus (brand_id, article_type_id, size, vendor_article_no, vendor_article_name, code, remarks, created_by, lastuser, created_on, last_modified_on,jit_sourced,enabled)
				VALUES ('$brand_id', '$article_type_id', '$size', '$article_number', '$article_name', '$skuCode', '$remarks', 'script', 'script', current_timestamp, current_timestamp,1,1)";
			func_query($sql);
			echo "\n--------> Inserting sku done for skuCode:".$skuCode;
			//--- Insersion done

			$excel->getActiveSheet()->setCellValue('H'.$i, $skuCode);
			
		}
		$objWriter = PHPExcel_IOFactory::createWriter($excel, "Excel5");
		$objWriter->save($filepath);
	}
}
function generateSkuCode($brandId, $articleTypeId) {
	// this function returns the unique skucode
	// for the given article type id and the brand id
	$sql = "SELECT id, attribute_code AS code from mk_attribute_type_values WHERE attribute_type = 'Brand' AND id = " . $brandId;
	$rows = func_query($sql);
	$brandCode = $rows[0]["code"];

	$sql = "SELECT typecode from mk_catalogue_classification where id = " . $articleTypeId;
	$rows = func_query($sql);
	$articleTypeCode = $rows[0]["typecode"];

	do {
		$rand = rand(10000, 99999);
		$skuCode = $brandCode . $articleTypeCode . $rand;

		// check sku code exists or not
		$sql = "SELECT id, code from mk_skus where code = '" . $skuCode . "'";
		$rows = func_query($sql);
	} while (isset($rows[0]));

	return $skuCode;
}
function getBrandId($brand){
	$sql = "SELECT id from mk_attribute_type_values WHERE attribute_type = 'Brand' AND attribute_value = '" . $brand."'";
	$row = func_query_first($sql);
	if(!empty($row)){
		return $row['id'];
	}else{
		return null;
	}
}
function getArticleTypeId($article_type){
	$sql = "SELECT id from mk_catalogue_classification where typename = '" . $article_type."'";
	$row = func_query_first($sql);
	if(!empty($row)){
		return $row['id'];
	}else{
		return null;
	}
}
function checkForExistingSKU($brand_id, $article_type_id, $size){
	$sql = "SELECT * from mk_skus where brand_id='$brand_id' and article_type_id ='$article_type_id' and size = '$size'";
	$row = func_query_first($sql);
	if(empty($row)){
		return 'no';
	}else{
		return 'yes';
	}
}

?>