<?php
/*

Procedure to use this script
php style_index.php 1
param 1 index only non customized styles
param 2 index all jerseys
no param index all styles which are public,active and whose PT is public
*/

chdir(dirname(__FILE__));
include_once("../auth_for_solr_index.php");
include_once("../include/solr/solrProducts.php");
set_time_limit(0);
   $mail_detail = array(
                        "to"=>'nishith.shrivastav@myntra.com',
                        "subject"=>'Solr indexing started on '.$http_location. '<eom>',
                        "content"=>'',
                        "from_name"=>'Myntra Admin',
                        "from_email"=>'admin@myntra.com',
                        "header"=>'',
                    );
    send_mail_on_domain_check($mail_detail);
        echo "Deleting index for Non Customizable styles";
        $solrProducts = new solrProducts();
		$solrProducts->deleteNonCustomizedDisabledStyles();

    $mail_detail['subject'] = 'Solr indexing end on '.$http_location. '<eom>';
    send_mail_on_domain_check($mail_detail);
?>
