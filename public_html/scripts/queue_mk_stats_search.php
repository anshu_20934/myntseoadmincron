<?php
echo "Queue Started\n";
chdir(dirname(__FILE__));
@include_once "../auth.php";
require_once $xcart_dir.'/Cache/CacheQueue.php';

global $weblog;

$r_key = 'search_tracking';

$r = new CacheQueue("queue:mk_stats_search");
$job_successful = true;
$update_data = array();
$count_queue_length = 0;

$start = time();
$runtime = 120;
$queue_len = $r->llen($r_key);

//Execute the job until last job was successful and for 120 seconds and for initial queue length items
while ($job_successful && ($start + $runtime) > time() && $count_queue_length < $queue_len ) {
	$keyword = '';
	$data = $r->lpop($r_key);
	
	if (!$data) {
		$job_successful = false;
		continue;
	}
	$count_queue_length ++;
	$data = unserialize($data);
	if(!isset($data['errors'])) {
		$job_successful = true;
	}
	
	if ($job_successful && $data['keyword'] != '') {
		$keyword = $data['keyword'];
		$update_data[$keyword]['last_search_date'] = $data['last_search_date']; //last fetched date will be latest
		$update_data[$keyword]['keyword'] = $data['keyword'];
		$update_data[$keyword]['orignal_keyword'] = $data['orignal_keyword'];
		$update_data[$keyword]['count_of_products'] = $data['count_of_products'];
		$update_data[$keyword]['is_valid'] = $data['is_valid'];
	if(!isset($update_data[$keyword]['date_added'])) $update_data[$keyword]['date_added'] = $data['date_added'] ; //First fetched date will be date_added for new keywords
		$update_data[$keyword]['count_of_searches'] += $data['count_of_searches'];
		
		continue;
	} else {
		$errorlog->error("queue mk_stats_search error : ".serlialize($data));
		$weblog->error("queue mk_stats_search error : ".serlialize($data));
	}
}
echo "data fetching complete\n";
$count_update_data = 0 ;
$count_updates = 0;
$count_inserts = 0;
$count_update_data = count($update_data);
echo "Count of keywords ".$count_update_data."\n";

//Time for Updates and Inserts
if($count_update_data) {
	$ins_data = array ();
	
	foreach($update_data as $keyword => $data) {
		$stats_search_entry = func_select_first('mk_stats_search',array('keyword' => $keyword));
		if ($stats_search_entry == NULL ) {
			$ins_data[] = $data;
		}
		else {
			$query_data = array(
                "count_of_products" => $data['count_of_products'],
                'count_of_searches' => ($stats_search_entry['count_of_searches'] + $data['count_of_searches']),
				'last_search_date'  => $data['last_search_date']
			);
			//func_array2update("mk_stats_search", $query_data, " key_id = '" . $stats_search_entry['key_id'] . "'");
			$count_updates++;
		}
	}
	$count_inserts = count($ins_data);
	//Club multiple inserts together
	if($count_inserts) {
		global $sql_tbl;
		$keys = array_keys($ins_data[0]);
		$tbl = 'mk_stats_search';
		
		$query .= "INSERT INTO $tbl (" . implode(", ", $keys) . ") VALUES ";
		foreach($ins_data as $data) {
			$sql_part[] = "('" . implode("', '", $data). "')";
		}
		$query = $query . implode(",", $sql_part);
		db_query($query);
	}
}

$weblog->info("mk_stats_search QUEUE SCRIPT : Length of Queue : ".$count_queue_length);
$weblog->info("mk_stats_search QUEUE SCRIPT : Total Unique Keywords : ".$count_update_data);
$weblog->info("mk_stats_search QUEUE SCRIPT : Count of Updated Values in DB : ".$count_updates);
$weblog->info("mk_stats_search QUEUE SCRIPT : Count of Inserted Values in DB : ".$count_inserts);
echo "Script complete\n";
?>
