<?php
include_once("../auth.php");
include_once("$xcart_dir/include/class/class.feature_gate.keyvaluepair.php");
include_once("$xcart_dir/include/func/func.mail.php");
include_once("$xcart_dir/include/func/func.sms_alerts.php");


$run=FeatureGateKeyValuePairs::getBoolean("phpmailsender.run");

if($run==false){
	echo "Mailer is Stopped from Admin Feature Gate ";
	exit;
}

$mailsql="select ms.message_details_id as id,em.sendto,em.cc,em.bcc,em.sender,em.subject,em.body,em.mimetype from myntra_notification_message_details ms,myntra_notification_email_details em where ms.message_details_id=em.email_details_id and ms.sent=1 order by ms.queuedtime asc";
$smssql="select ms.message_details_id as id,sm.sendto,sm.body from myntra_notification_message_details ms,myntra_notification_sms_details sm where ms.message_details_id=sm.sms_details_id and ms.sent=1 order by ms.queuedtime asc";
echo "Mailer Started ... ";


try{
	$mailsRs=func_query($mailsql);
	$smssRs=func_query($smssql);
	foreach ($mailsRs as $mailrow){
		$mail_details = array(
                                               "to" => $mailrow['sendto'],
											   "bcc"=>"myntramailarchive@gmail.com",
                                               "subject" => $mailrow['subject'],
                                               "content" => $mailrow['body'],
                                               "mail_type" => MailType::CRITICAL_TXN
		);
		$multiPartymailer = new MultiProviderMailer($mail_details);
		$result=$multiPartymailer->sendMail();		echo "\nMail Sent To:".$mailrow['sendto']." Status:$result";
		db_query("update myntra_notification_message_details set sent=".($result==true?2:3).",dispatchtime='".date("Y/m/d H:i:s")."' where message_details_id=".$mailrow['id']);
			
	}
	foreach ($smssRs as $smsrow){
		func_send_sms($smsrow['sendto'], $smsrow['body']);
		echo "\nSMS Sent to:".$smsrow['sendto'];
		db_query("update myntra_notification_message_details set sent=2,dispatchtime='".date("Y/m/d H:i:s")."' where message_details_id=".$smsrow['id']);
	}

}catch (Exception $err){
	echo"Error:$err";
}



echo "Mailer Closed";