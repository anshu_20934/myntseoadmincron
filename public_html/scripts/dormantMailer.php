<?php
/*
 * Coupon expiry mailer script to upload mailing list to icubes
 * 
 * 
 * command to run dorman mailers : php dormantMailer.php -d 60 -m 205990 -w 205991 -u dormant
 * 
 * command to run reminder mailers : php dormantMailer.php -d 15 -m 206374 -w 206375 -u reminder
 */

require_once "../admin/auth.php";
require_once $xcart_dir."/include/func/func.core.php";
require_once $xcart_dir."/include/dao/class/mcart/class.MCartDBAdapter.php";
require_once $xcart_dir."/include/class/mcart/class.MCart.php";
require_once $xcart_dir.'/include/class/csvutils/class.csvutils.ArrayCSVWriter.php';
require_once $xcart_dir.'/include/class/class.FTPActions.php';
require_once $xcart_dir.'/include/class/elabsmail/class.elabsmail.mailinglist.php';
include_once($xcart_dir."/Profiler/Profiler.php");

//TODO: add logger support to non-webroot-scripts
echo "==============================begin Mailer==================================\n";
echo "dormantMailer.php: cron started at ".date("r", time())."\n";

$argv = getopt("d:m:w:u:");
print_r($argv);
if(sizeof($argv)!=4){
	
	echo sizeof($argv)."insufficient arguments\n";exit;
	
}else{
	
	
	foreach($argv as $op=>$arg){
			
		if($op!=="u" && !preg_match("/^[0-9]+$/", $arg)){
			echo "error with -$op arguments :$arg\n";
			exit;
		}
			
		
	}
	
}


$mailerName = $argv["u"];



//-------------------------------------------------------------------------------

$time = time();
//from start of the day
$start_stamp = strtotime(date('Y-m-d', time())." -14 day");

$startday=$argv["d"];
$endday=$startday-1;

$query = "select xo.login,gender,firstname,lastname from xcart_customers as xc inner join 
( select login, max(date) as date from xcart_orders WHERE date > unix_timestamp(CURRENT_TIMESTAMP) - $startday*24*60*60 group by login having max(date) < unix_timestamp(CURRENT_TIMESTAMP) - $endday*24*60*60)
 as xo on xc.login=xo.login";
$results = func_query($query,true);

$today = time();
$campaignDate =  date("Ymd",$today);
$localDirname = "/var/mailer_uploads/$mailerName"."_mailer/";
mkdir($localDirname);
//delete all CSV files locally inside this directory
$mask = "*.csv";
array_map("unlink",$localDirname.$mask);
//local file which will be created this time
$fileName[0] = $localDirname.$campaignDate.$mailerName."Men.csv";
$fileName[1] = $localDirname.$campaignDate.$mailerName."Women.csv";






Profiler::increment("205990-men-dormant-mailing-list-csv-generation-started");
$profilerCountMen = 0 ; 	
$writer1 = new ArrayCSVWriter($fileName[0], "|");
$writer1->setHeaderColumns(Array("email","CustomerName", "CampaignDate","recommendationsHTML" ));
$writer1->writeHeader();


Profiler::increment("205991-women-dormant-mailing-list-csv-generation-started");
$profilerCountWomen = 0 ; 
$writer2= new ArrayCSVWriter($fileName[1], "|");
$writer2->setHeaderColumns(Array("email","CustomerName", "CampaignDate","recommendationsHTML" ));
$writer2->writeHeader();


function getCustomerName($row) {
	$customerName = "";
	if(!empty($row["firstname"])) {
		$customerName = $row["firstname"];
	}
	if(!empty($row["lastname"])) {
		$customerName .= " ".$row["lastname"];
	}
	if(empty($customerName)) {
		$customerName = "Customer";
	}
	return $customerName;
}

function getTopSix($query){

	$solrSearchProducts = new SearchProducts($query."-apparels",null,$getStatsInstance);
	$solrSearchProducts->do_search_new();
	$widgetData1 = $solrSearchProducts->products;
		
	$solrSearchProducts = new SearchProducts($query."-footwear",null,$getStatsInstance);
	$solrSearchProducts->do_search_new();
	$widgetData2 = $solrSearchProducts->products;
	
	$solrSearchProducts = new SearchProducts($query."-accessories",null,$getStatsInstance);
	$solrSearchProducts->do_search_new();
	$widgetData3 = $solrSearchProducts->products;

	$widgetArray = array();
	$count = 0 ;
	foreach($widgetData1 as $data){
		$widgetArray[] = WidgetUtility::getWidgetFormattedData($data);
		$count++;
		if ($count==3)break;
	}
	$count = 0 ;
	foreach($widgetData2 as $data){
		$widgetArray[] = WidgetUtility::getWidgetFormattedData($data);
		$count++;
		if ($count==3)break;
	}
	$count = 0 ;
	foreach($widgetData3 as $data){
		$widgetArray[] = WidgetUtility::getWidgetFormattedData($data);
		$count++;
		if ($count==3)break;
	}
	$widgetSKUData = array();
	$widgetSKUData['data'] = $widgetArray;
	
	return $widgetSKUData;

}


$menProducts = getTopSix("mens");
$womenProducts = getTopSix("womens");
$smarty->assign("avail_recommendation",$menProducts);
$smarty->assign("campaignDate",$campaignDate);
$smarty->assign("gender","men");
$smarty->assign("mailer",$mailerName);
$menProductsHtml = $smarty->fetch("mail/dormant_mailer_products.tpl");

$menProductsHtml =  preg_replace("/\r\n/"," ", $menProductsHtml);
$menProductsHtml =  preg_replace("/\n/"," ", $menProductsHtml);
$smarty->assign("campaignDate",$campaignDate);
$smarty->assign("avail_recommendation",$womenProducts);
$smarty->assign("gender","women");
$womenProductsHtml = $smarty->fetch("mail/dormant_mailer_products.tpl");

$womenProductsHtml =  preg_replace("/\r\n/"," ", $womenProductsHtml);
$womenProductsHtml =  preg_replace("/\n/"," ", $womenProductsHtml);


$countEmailsSent =0;


foreach($results as $row){
		
	if(trim($row["gender"]) == "f"||trim($row["gender"]) == "F"){
		$writer2->writeRow(Array("email"=>trim($row["login"]),"CustomerName" =>getCustomerName($row), "CampaignDate"=>$campaignDate,"recommendationsHTML" => $womenProductsHtml));
		$profilerCountMen++;
	}else{
		$writer1->writeRow(Array("email"=>trim($row["login"]),"CustomerName" =>getCustomerName($row), "CampaignDate"=>$campaignDate,"recommendationsHTML" => $menProductsHtml));
		$profilerCountWomen++;
	}
	$countEmailsSent++;
}
$writer1->writeRow(Array("email"=>"myntramailarchive@gmail.com","CustomerName"=>"","CampaignDate"=>$campaignDate,"recommendationsHTML"=>"Men's dormant email campaign sent"));
$writer2->writeRow(Array("email"=>"myntramailarchive@gmail.com","CustomerName"=>"","CampaignDate"=>$campaignDate,"recommendationsHTML"=>"Women's dormant email campaign sent"));
$writer1->closeFile();
$writer2->closeFile();

Profiler::increment("205990-men-dormant-mailing-list-csv-ready", $profilerCountMen);
Profiler::increment("205991-women-dormant-mailing-list-csv-ready", $profilerCountWomen);

//=========================icubes FTP uploader===================================

//credentials of icubes ftp server where we can upload our CSV files
$ftphost = empty($elabsConfig["ftp_host"]) ? 'icubes.in' : $elabsConfig["ftp_host"];
$ftpuser = empty($elabsConfig["ftp_user"]) ? 'myntra' : $elabsConfig["ftp_user"];
$ftppasswd = empty($elabsConfig["ftp_password"]) ? 'M!ntra123' : $elabsConfig["ftp_password"];

//email id to which results of CSV upload should be sent
$emailToSendUploadStatus =  'myntramailarchive@gmail.com' ;
//this is the mailing listid in Lyris/Elabs to which the contacts have to be imported finally: It is named Abandoned Cart
$mailingListId[0] = $argv["m"];//"205990"; // dormant men
$mailingListId[1] = $argv["w"];//"205991"; // dorman women

$uploader = new FTPActions($ftphost, FALSE);
$uploader->setMode(FTP_BINARY);
$uploader->connect();
echo "Logging in user.. Result: ".$uploader->login($ftpuser, $ftppasswd)."\n";

$dirname = "/".$mailerName."_mailer/";
//don't need old CSV files on FTP server so delete them
$uploader->deleteDirectoryRecursively($dirname);
$uploader->mkdir($dirname);
$uploader->chdir($dirname);
//file name on FTP server which would be copied
$remoteFile[0] = $dirname.$campaignDate.$mailerName."Men.csv";
$remoteFile[1]= $dirname.$campaignDate.$mailerName."Women.csv";

for($i=0;$i<2;$i++)
{
	$ftpResult = $uploader->upload($fileName[$i], $remoteFile[$i]) ; 
	echo "Uploading ".$fileName[$i]." to FTP Server with remote path: $remoteFile[$i] ; result: ".$ftpResult."\n";
	if($ftpResult == 1){
		Profiler::increment($mailingListId[$i] . "-dormant-mails-csv-uploaded-to-FTP-SUCCESS");
	}else{
		Profiler::increment($mailingListId[$i] . "-dormant-mails-csv-uploaded-to-FTP-FAILURE");
	}
	$pathToCsvFile = "ftp://$ftpuser:$ftppasswd@$ftphost/".$remoteFile[$i];
	echo "Importing $pathToCsvFile to : $remoteFile[$i] ";
		
	$uploadResult = MailingList::uploadContactsToMailingList($mailingListId[$i], $pathToCsvFile, $emailToSendUploadStatus,'|'); 
	print_r($uploadResult);
	if($uploadResult["result"] == "success"){
		Profiler::increment($mailingListId[$i] . "-dormant-mails-queued-into-icubes-SUCCESS");
	}else{
		Profiler::increment($mailingListId[$i] . "-dormant-mails-queued-into-icubes-FAILURE");
	}
	sleep(1);

}



echo __FILE__.": No of email entries made in icubes: ".$countEmailsSent."\n";
echo "dormantMailer.php: cron finished at ".date("r", time())."\n";
echo "============================end $mailerName Mailer================================\n";





