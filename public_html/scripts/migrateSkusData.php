<?php
chdir(dirname(__FILE__));
require "../auth.php";

$skus_results = func_query("SELECT * from mk_skus");

foreach ($skus_results as $sku) {
	$core_inv_data = array();
	$core_inv_data['sku_id'] = $sku['id'];
	$core_inv_data['warehouse_id'] = 1;
	$core_inv_data['warehouse_name'] = 'Bangalore';
	$core_inv_data['quality'] = 'Q1';
	$core_inv_data['inv_count'] = $sku['wh_count'];
	$core_inv_data['blocked_order_count'] = 0;
	$core_inv_data['blocked_manually_count'] = 0;
	$core_inv_data['blocked_missed_count'] = 0;
	$core_inv_data['created_by'] = 'erpadmin';
	$core_inv_data['created_on'] = date("d-m-y", time());
	$core_inv_data['last_modified_on'] = date("d-m-y", time());
	
	func_array2insert('core_inventory_counts', $core_inv_data);

	$core_inv_data = array();
	$core_inv_data['sku_id'] = $sku['id'];
	$core_inv_data['warehouse_id'] = 1;
	$core_inv_data['warehouse_name'] = 'Bangalore';
	$core_inv_data['quality'] = 'Q2';
	$core_inv_data['inv_count'] = 0;
	$core_inv_data['blocked_order_count'] = 0;
	$core_inv_data['blocked_manually_count'] = 0;
	$core_inv_data['blocked_missed_count'] = 0;
	$core_inv_data['created_by'] = 'erpadmin';
	$core_inv_data['created_on'] = date("d-m-y", time());
	$core_inv_data['last_modified_on'] = date("d-m-y", time());
	
	func_array2insert('core_inventory_counts', $core_inv_data);
	
	$core_inv_data = array();
	$core_inv_data['sku_id'] = $sku['id'];
	$core_inv_data['warehouse_id'] = 1;
	$core_inv_data['warehouse_name'] = 'Bangalore';
	$core_inv_data['quality'] = 'Q3';
	$core_inv_data['inv_count'] = 0;
	$core_inv_data['blocked_order_count'] = 0;
	$core_inv_data['blocked_manually_count'] = 0;
	$core_inv_data['blocked_missed_count'] = 0;
	$core_inv_data['created_by'] = 'erpadmin';
	$core_inv_data['created_on'] = date("d-m-y", time());
	$core_inv_data['last_modified_on'] = date("d-m-y", time());
	
	func_array2insert('core_inventory_counts', $core_inv_data);
}

?>
