<?php
include_once(dirname(__FILE__)."/../auth_for_solr_index.php");
include_once("../include/solr/solrProducts.php");

$sql_log = fopen('rename_image_list.sql','a');
$sql_prefix = "INSERT INTO `seo_image_renaming_details` (styleId, masterCategory, gender) VALUES (";

$solrProducts = new solrProducts();
//get the active styles
$query = "count_options_availbale : [1 TO *]";
$sort = 'styleid asc';
$fields = array ('fl' => 'id,styleid,global_attr_master_category_facet,global_attr_gender');
$numResults = $solrProducts->searchAndSort($query, 0,1, $sort, $fields)->numFound;
$offset = 0;
$limit = 100;
$n_iterations = floor($numResults / $limit) + 1;

// reading the styleids in a group of 100 docs
for($i=0; $i< $n_iterations; $i++) {
$solrDocuments = $solrProducts->searchAndSort($query, $offset , $limit, $sort, $fields);
$products = documents_to_array($solrDocuments->docs);
	foreach ($products as $index => $product) {
		$styleid = truncate($product['styleid']);
		$articleType = truncate($product['global_attr_master_category_facet']);
		$gender = truncate($product['global_attr_gender']);
		if(($articleType == 'Accessories' )|| ($articleType == 'Footwear' )|| ($articleType == 'Apparel')){
			$sql = $sql_prefix . "$styleid, '$articleType', '$gender');";
			fputs($sql_log, $sql);
		}
	}
$offset += $limit;
}

?>
