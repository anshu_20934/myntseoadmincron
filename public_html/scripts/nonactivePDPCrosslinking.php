<?php

$dirName = dirname(__FILE__);
chdir($dirName);
include_once("../auth.php");
require_once '../env/DB.config.php';
require_once($xcart_dir."/include/func/func.utilities.php");
require_once($xcart_dir."/include/func/func.core.php");
include_once \HostConfig::$documentRoot ."/utils/throttleUpdatesDb.php";
define('ALLOWABLETAGS','<h1><h2><h3><h4><h5><h6><em><strong><br/><b><p><br>');
define('REMOVABLETAGS','a');
define('DBSERVER', DBConfig::$rw['host']);
define('DBUSER', DBConfig::$rw['user']);
define('DBPASS', DBConfig::$rw['password']);
define('DBNAME', DBConfig::$rw['db']);
$linkLimitValue = WidgetKeyValuePairs::getWidgetValueForKey("pdpCrosslinkingLimit");
if(!$linkLimitValue){
    $linkLimitValue = 7;
}
define('LINKLIMIT', $linkLimitValue);

function getTotalNumberOfNonactiveProducts(){
    $sql = "select count(id) from mk_product_style where lower(styletype) != 'p'";
    return func_query_first_cell($sql, true);
}

function getProductDescriptions($offset){
    $sql = "SELECT mps.id,mps.description,sp.id as spid, sp.style_note from mk_product_style as mps, mk_style_properties as sp WHERE mps.id = sp.style_id AND lower(mps.styletype) != 'p' ORDER BY mps.id LIMIT 500 OFFSET ".$offset;
    return func_query($sql, true);
}

function getAllHandPickedKeywords(){
     $sql = "(SELECT crosslink_keyword as keyword,IFNULL(crosslink_keyword_url, replace(crosslink_keyword,' ','-')) as url FROM mk_crosslink_keyword WHERE is_active='1') union distinct (SELECT keyword,replace(url,'www.myntra.com/','') FROM adseo_sitemapkeyword where search_volume>0 and pattern != 'b')";
     return func_query($sql, true);
    
}

function buildSearchPattern($keywords){    
     $charsToEscape = "/";
     $search = array();
     foreach($keywords as $index=>$word){
        $kw = $word['keyword'];
        $searchPattern = preg_quote($kw,$charsToEscape);
        $search[$index] = "/\b(".$searchPattern.")\b(?!(?:(?!<\/?[ha].*?>).)*<\/[ha].*?>)(?![^<>]*>)/i";
     }
    return $search;
}

function buildLinkForKeyword($keyword,$url){
    global $http_location_cli;
    $pattern = empty($url) ? str_replace(" ", "-", strtolower($keyword)) : $url;
    return "<a href='".$http_location_cli."/".$pattern."?src=desc' class='seolink'>".$keyword."</a>";
}

function getSortedKeywords(){
	global $replaced;
	$replaced = array();
    $linkedKeywords = array();

    $HK = getAllHandPickedKeywords();
    if($HK){
        foreach($HK as $index=>$val){
            $ckeyword = $val['keyword'];
            if(!empty($ckeyword)){
				$replaced[strtolower($ckeyword)] = buildLinkForKeyword($ckeyword, $val['url']);
                $linkedKeywords[] = array('keyword' => $ckeyword, 'wc' =>str_word_count($ckeyword));
            }
        }
    }
    $sortedKeywordsWithWC = quickSortRecursive( $linkedKeywords, $left = 0 , $right = NULL, 'wc' );
    return array_reverse($sortedKeywordsWithWC); 
}

function getSortedLinkedFilters(){
    $sortedLinkedKeywords = getSortedKeywords();
    return buildSearchPattern($sortedLinkedKeywords);
}

function applyCrosslinking($text,$search){
    global $replacements;
    $replacements = 0;
    $text = preg_replace_callback($search, function($matches) {
        global $replaced, $replacements;
        $returnString = $matches[0];
         if($replacements < LINKLIMIT){
             $replacements++;
             $matched = $replaced[strtolower($matches[0])];
             $returnString =  preg_replace('/>'.$matches[0].'</i','>'.$matches[0].'<',$matched, 1);
         }
         return $returnString;
    }, $text, 1, &$count);
    return $text;
}

function makePDPDescriptionCrossLinked($desc,$id,$columnName, $tableName,$sortedSearchPatterns){
    $crosslinkedDescription= applyCrosslinking($desc,$sortedSearchPatterns);
    $queryData = array($columnName=>$crosslinkedDescription);
    return query_func_array2update($tableName, sanitizeDBValues($queryData),"id='{$id}'");
}
function makeStylePropertiesCrossLinked($prodDescription){
    $colVal=$queryData=array();
    $columns = array('style_note','materials_care_desc','size_fit_desc');
    $id = $prodDescription['spid'];
    foreach($columns as $col){
        if($prodDescription[$col]){
            $strippedDesc=stripOnlyTags($prodDescription[$col],REMOVABLETAGS);
            if(strcmp($strippedDesc,$prodDescription[$col]) != 0){
                $queryData[$col]=$strippedDesc;
            }
        }
    }
    return count($queryData)>0 ? query_func_array2update('mk_style_properties', sanitizeDBValues($queryData), "id='{$id}'") : null;
}

function updateDescriptions($crossLinkedDescriptions){
    $connection = \mysql_connect(DBSERVER, DBUSER, DBPASS);
    mysql_set_charset('utf8',$connection);
    @\mysql_select_db(DBNAME) or die( "Unable to select database");
    $count = 0;
    foreach( $crossLinkedDescriptions as $query ){
        $count++;
        $result = \mysql_query($query,$connection);
        if (!$result) {
            echo $query;
        }
    }
    return $batchDBQuery;
}

function crosslinkPageDescriptions($prodDescriptions,$sortedSearchPatterns){
    $crossLinkedDescriptions = array();
    foreach($prodDescriptions as $prodDescription){
        $desc = stripOnlyTags($prodDescription['description'],REMOVABLETAGS);
        $mpsId = $prodDescription['id'];
        $crossLinkedDescriptions[] =
 makePDPDescriptionCrossLinked($desc,$mpsId,'description','mk_product_style',$sortedSearchPatterns);
        $stylePropetiesUpdate = makeStylePropertiesCrossLinked($prodDescription);
        if($stylePropetiesUpdate){
            $crossLinkedDescriptions[] = $stylePropetiesUpdate ;
		}
        sleep(1); 
    }
    updateDescriptions($crossLinkedDescriptions);
}

function getPageDescAndCrosslink($i,$sortedSearchPatterns){
    $time_desc = microtime(true);
    $pageDescriptions = getProductDescriptions($i);
    $time_taken_desc = microtime(true) - $time_desc;
    $time_cross = microtime(true);
    crosslinkPageDescriptions($pageDescriptions,$sortedSearchPatterns);
    $time_taken_cross = microtime(true) - $time_cross;
    var_dump("Time for crosslinkProductDescriptions : ".$time_taken_cross);
}

var_dump(date("F j, Y, g:i a"));
$start = microtime(true);
$sortedSearchPatterns = getSortedLinkedFilters();
$time_taken_sf = microtime(true) - $start;
var_dump("Time for sorted filters : ".$time_taken_sf);
$desc_count = getTotalNumberOfNonactiveProducts();
var_dump(" Page description count : ".$desc_count);
for($i=62000; $i < $desc_count; $i +=500){
    var_dump("i : ".$i);
    getPageDescAndCrosslink($i,$sortedSearchPatterns);
}
$time_taken = microtime(true) - $start;
var_dump("Total time taken for script : ".$time_taken);
var_dump(date("F j, Y, g:i a"));
