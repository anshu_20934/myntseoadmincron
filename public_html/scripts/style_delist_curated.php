<?php
/*

Procedure to use this script
php php style_delist.php <csv file containing style-ids comma separated without spaces>
*/

chdir(dirname(__FILE__));
include_once("../auth_for_solr_index.php");
include_once("../include/solr/solrProducts.php");
if(empty($argv[1])){
 die("Usage: php style_delist_curated.php <csv file containing style-ids comma separated in single line without spaces> ");
}
$start_time=time();
$solrProducts = new SolrProducts();

$csvfile="$argv[1]";
$file = fopen("$csvfile","r");

$styles = fgetcsv($file);
fclose($file);
foreach($styles as $eachStyle){
 echo($eachStyle."\n");
 $solrProducts->deleteStyleFromIndex($eachStyle,true,false);
}

$end_time=time();

$diff_time= ($end_time - $start_time) / 60;
echo "\n=========== Total time :: ".$diff_time." mins ==================\n";

?>
