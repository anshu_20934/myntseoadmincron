<?php
include_once(dirname(__FILE__)."/../auth.php");
include_once(dirname(__FILE__)."/../modules/discount/DiscountConfig.php");

$optionsQuery = "select sp.global_attr_brand as brand, cc.typename as articletype, "; 
$optionsQuery.= "sp.global_attr_fashion_type as fashion from mk_style_properties sp, mk_catalogue_classification cc "; 
$optionsQuery.= "where sp.global_attr_article_type=cc.id group by brand, articletype, fashion";
$optionResults = func_query($optionsQuery);

foreach($optionResults as $data){
	$brandName = $data['brand'];
	$articleTypeName = $data['articletype'];
	$fashionTypeName = $data['fashion'];
	$res = func_query("select * from mk_discount_rules where brand='$brandName' and articletype='$articleTypeName' and fashion='$fashionTypeName'");
	if($res != null){
		continue;
	}
	$algoid = DiscountConfig::FLAT_DISCOUNT_ID;
	$data = array("isPercentage"=>1, "value"=>0);
	$data = json_encode($data);
	$createRuleQuery = "insert into mk_discount_rules(ruletype,display_name,algoid,data,enabled,start_date,end_date,brand,articletype,fashion) values(0,'$brandName $articleTypeName, $fashionTypeName','$algoid','$data',0,unix_timestamp(now()),unix_timestamp(now()),'$brandName','$articleTypeName','$fashionTypeName')";
	func_query($createRuleQuery);
}
			
	
?>
