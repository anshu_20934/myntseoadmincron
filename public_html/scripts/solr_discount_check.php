<?php
include_once(dirname(__FILE__)."/../auth_for_solr_index.php");
require_once($document_root."/include/class/widget/class.static.functions.widget.php");
include_once($document_root."/include/solr/solrProducts.php");
include_once $document_root."/include/func/func.mail.php";

$search_term = str_replace( " ", "-" , mysql_real_escape_string($q) );
$query = "select d.is_enabled,d.id,dsm.style_id,type,on_buy_amount, on_buy_count, get_count, get_amount, get_percent from discount_style_map dsm
inner join discount d on dsm.discount_id=d.id inner join discount_rule dr on dr.discount_id=d.id
inner join mk_product_style ps on ps.id=dsm.style_id and ps.styletype='P'
where d.is_on_cart=0 and d.started_on<=".time()." and d.expired_on>=".time()." order by style_id asc;";
$fromdb = func_query($query,true); 

$emailBody =  "Got the discount data from db...".count($fromdb)."<BR>";

$styles = array();
$discounted_style_map = array();
$disabled_dsm = array();

function mergeArray($arr1, $arr2)
{
	foreach($arr2 as $v){
		$arr1[$v['styleid']] = $v;
	}
	
	return $arr1;
}

$BATCH_SIZE = 350;

$solrProducts = new solrProducts();
$columns = array("styleid","dre_amount","dre_percent","dre_discountId");
$solrResult = array();

$i=0;
$total = 0;
$resT = 0;
foreach($fromdb as $row){
	if(!empty($row['is_enabled']))
		$discounted_style_map[$row['style_id']] = $row;
	else
		$disabled_dsm[$row['style_id']] = $row;
	array_push($styles, $row['style_id']);
	
	$i++;
	if($i==$BATCH_SIZE){
		$solrResult_batch = $solrProducts->getCustomizedDataForStyles($styles, $columns, '');
		$resT += count($solrResult_batch);
		$solrResult = mergeArray($solrResult, (array)$solrResult_batch);
		$total += $i;
		unset($styles);
		$styles = array();
		$i=0;
	}
}


       if($i>0){
                $solrResult_batch = $solrProducts->getCustomizedDataForStyles($styles, $columns, '');
             	$resT += count($solrResult_batch);
		$solrResult = mergeArray($solrResult, (array)$solrResult_batch);
		$total += $i;
		unset($styles);
                $styles = array();
                $i=0;
        }

$emailBody =  $emailBody . "Got the solr data for $total styles obtained above and found ".count($solrResult)." in solr....<BR>";

//Generate the report

$outOfStock = array();
$inSync = array();
$outOfSyncSD = array();
$outOfSyncDD = array();
$notYetDisabled = array();
$total = 0;$oos = 0; $dids = 0; $didd = 0;
foreach ($discounted_style_map as $sid=>$value) {
	$total ++;
	$res = $solrResult[$sid];
	if(empty($res)){
		$oos++;
		array_push($outOfStock, $sid);
	}
	else if($value['id']==$res['dre_discountId']){
		$dids++;
		if($value['get_amount']>0 && $value['get_amount']==$res['dre_amount'])
			array_push($inSync, $sid);
		else if($value['get_percent']>0 && (abs($value['get_percent'] - $res['dre_percent']) < 0.005))
			array_push($inSync, $sid);
        else if($value['get_count']>0)
            array_push($inSync, $sid);
        else if($value['type']==4 )
            array_push($inSync, $sid);
		else{
			array_push($outOfSyncSD, $sid);
			$solrProducts->addStyleToIndex($sid);
		}
	}else{
		$didd++;
		array_push($outOfSyncDD, $sid);
		$solrProducts->addStyleToIndex($sid);		
	}	
}

foreach($disabled_dsm as $sid=>$value){
	$res = $solrResult[$sid];
	if($value['id']==$res['dre_discountId']){
		array_push($notYetDisabled, $sid);
		$solrProducts->addStyleToIndex($sid);
	}
}

$csv_data = array();
$csv_data[] = array("Out Of Stock,Out of Sync - Diff Discount", "Out of Sync - Diff Discount Attributes", "Styles in Sync");
$csv_data[] = array( count($outOfStock) ,count($outOfSyncDD),count($outOfSyncSD),count($inSync));


//$emailBody =  $emailBody . "<BR>Total = $total;<BR>OOS = $oos;<BR>DIDS = $dids;<BR>DIDD = $didd";
$emailBody =  $emailBody . "<BR><BR><h1>Summary ......</h1><BR>";
$emailBody =  $emailBody . "Out Of Stock - " . count($outOfStock);
$emailBody =  $emailBody . "<BR>Out of Sync - ";
$emailBody =  $emailBody . "<BR>&nbsp;&nbsp;&nbsp; Diff Discount - " . count($outOfSyncDD); 
$emailBody =  $emailBody . "<BR>&nbsp;&nbsp;&nbsp; Diff Discount Attributes - ". count($outOfSyncSD);
$emailBody =  $emailBody . "<BR>&nbsp;&nbsp;&nbsp; Disabled Discount still present in solr - ". count($notYetDisabled);
$emailBody =  $emailBody . "<BR>Styles in Sync - ". count($inSync). "<BR>";
$emailBody =  $emailBody . "<BR><BR><BR> <h2>Styles List .......</h2><BR>";
$emailBody =  $emailBody . "<BR><BR><BR><B>Different Discount -</B> ".implode(',',$outOfSyncDD) ."<BR>";
$emailBody =  $emailBody . "<BR><BR><BR><B>Discount Attributes being different -</B> ".implode(',',$outOfSyncSD) ."<BR>";
//$emailBody =  $emailBody . "<BR><BR><BR><B>Out Of Stock -</B> " . implode(',',$outOfStock) ."<BR>";
$file = "../soldDiscountCron.html";
$initHtml = '<html>';
$closehtml = '</html>';

$exitStatus = 0;
if(count($outOfSyncDD)==0 && count($outOfSyncSD)==0 && count($notYetDisabled)==0){	
	$emailSubject = "Discount data in  Solr is in Sync!!!";
	$htmlContent = $initHtml.'All is well'.$closehtml;
}else{
	$exitStatus = 1;
	$emailSubject = "Discount data in  Solr is Out Of Sync!!!";
	$htmlContent = $initHtml.'SOLR_DIS_FAIL'.$closehtml;
}

$subjectargs = array("TITLE"=>$emailSubject);
$args = array("CONTENT" => $emailBody);
$to = "rpt_solrhealthcheck@myntra.com";
sendMessageDynamicSubject("discount_rule_changes", $args,$to,'MyntraAdmin-DiscountRule', $subjectargs);

//Upload CSV
// Upload :- create file
$csvfilename='healthcheck_'.date("YmdHis").'.csv';
$local_filename='/tmp/'.$csvfilename;
$fpcsv = fopen($local_filename,'w');
foreach($csv_data as $v){
	fputcsv($fpcsv, $v);
}
fclose($fpcsv);

$upload_ip_add = '192.168.70.45';
$upload_user = 'instrumentation';
$upload_password = 'instrumentation123';
$upload_basepath = '/home/instrumentation/solr-health-check/';
$remote_filename = $upload_basepath.$csvfilename;

$connection = ssh2_connect($upload_ip_add, 22);
$b = ssh2_auth_password($connection, $upload_user, $upload_password);
$b = ssh2_scp_send($connection,$local_filename,$remote_filename, 0644);
echo "file upload status:=";
var_dump($b);
echo "\n";
//Upload ends

$nagios = "Solr-Dis-Test\n OutOfStock:".count($outOfStock)."\nOutOfSync-\nDiffId:".count($outOfSyncDD)."\nDiffAtbt:".count($outOfSyncSD)."\nInSync:".count($inSync);
fwrite(STDOUT, $nagios);
file_put_contents($file,$htmlContent);
exit($exitStatus);

