<?php
/*

Procedure to use this script
php php style_delist.php <date in format: 2013-11-25>
This is the date since which each style which is not in active state needs to be removed from solr and its pdp cache bursted
*/

chdir(dirname(__FILE__));
include_once("../auth_for_solr_index.php");
include_once("../include/solr/solrProducts.php");
include_once("../include/func/func.db.php");

if(empty($argv[1])){
 die("Usage: php style_delist_auto.php <date in format: 2013-11-25> ");
}
$start_time = time();
$styles = getSingleColumnAsArray("select ps.id from mk_product_style ps inner join mk_style_properties sp on ps.id = sp.style_id where sp.global_attr_catalog_add_date > unix_timestamp('".$argv[1]."') and styletype !=  'P'",true);
$solrProducts = new SolrProducts();

/*
$csvfile="$argv[1]";
$file = fopen("$csvfile","r");
$styles = fgetcsv($file);
fclose($file);
*/
foreach($styles as $eachStyle){
 echo($eachStyle."\n");
 $solrProducts->deleteStyleFromIndex($eachStyle,true,false);
}

$end_time=time();

$diff_time= ($end_time - $start_time) / 60;
echo "\n ===== Total ".sizeof($styles)." styles were processed. in Total time :: ".$diff_time." mins ==================\n";

?>

