<?php

function fatalErrorHandler2()
{
    # Getting last error
    $error = error_get_last();
	//echo 'Sorry, a serious error has occured in ' . $error['file'];
	//print_r($error);
   # Checking if last error is a fatal error
   if(($error['type'] === E_ERROR) || ($error['type'] === E_USER_ERROR))
  {
      # Here we handle the error, displaying HTML, logging, ...
      echo 'Sorry, a serious error has occured in ' . $error['file'];
 }
}

# Registering shutdown function
register_shutdown_function('fatalErrorHandler2');



include("../auth.php");
include_once $xcart_dir."/include/func/func.utilities.php";
error_reporting(E_ALL);
ini_set('display_errors', '1');


/**
 * 
 * get the details of t-shirts which will be used for polo tshirts
 * 
 */


/** Take back up of following data tables
 * 
 * mk_attribute_type
 * mk_attribute_type_values
 * mk_style_article_type_attribute_values
 * mk_style_properties
 * 
 */

/** CHANGE THE IDs FOR PRODUCTION **/
$tshirt_id = 90;
$polo_tshrit_id = 160;

/**
 * Using the tshirt id and polo tshirt id
 * insert duplicate attribute types for polo tshirt from tshirt's attribute type
 * make pair of these duplicate attribute types of polo tshirt with tshirt
 */

$query_sel_att = "select * from mk_attribute_type where catalogue_classification_id = $tshirt_id";
$tshirt_att = func_query($query_sel_att);
$t_att_id_map_pt_att_id = array();

foreach ($tshirt_att as $t_att){
	$pt_att = array(
				'catalogue_classification_id' => $polo_tshrit_id, 
				'attribute_type' => $t_att['attribute_type'], 
				'is_active' => $t_att['is_active'], 
				'display_order' => $t_att['display_order'], 
				'is_filter' => $t_att['is_filter']
			);
	$insert_pt_att = func_array2insertWithTypeCheck('mk_attribute_type', $pt_att);
	$pt_att_id = db_insert_id();
	$t_att_id_map_pt_att_id[$t_att['id']] = $pt_att_id;
}
echo "Tshirt-attribute-type-id-MAPPING-WITH-Polo-tshirt-attribute-type-id";
print_r($t_att_id_map_pt_att_id);

/**
 * Now duplicate article type attribute type value for polo tshirt with the values from tshirts
 * store the pairs of duplicate attribute type from polo tshirt and tshirt attribute type value
 */
$t_att_val_id_map_pt_att_val_id = array();

foreach ($t_att_id_map_pt_att_id as $tshirt_att_id => $polo_tshirt_att_id){
	$query_sel_att_val = "select * from mk_attribute_type_values where attribute_type_id = $tshirt_att_id";
	$tshirt_att_val_array = func_query($query_sel_att_val);
	foreach($tshirt_att_val_array as $tshirt_att_val){
		$polo_tshrit_att_val = array(
								'attribute_type_id' => $polo_tshirt_att_id,
								'attribute_value' => $tshirt_att_val['attribute_value'],
								'applicable_set' => $tshirt_att_val['applicable_set'],
								'is_active' => $tshirt_att_val['is_active'],
								'filter_order' => $tshirt_att_val['filter_order'],
								'is_featured' => $tshirt_att_val['is_featured'],
								'is_latest' => $tshirt_att_val['is_latest']						
							);
		
		$insert_polo_tshrit_att_val = func_array2insertWithTypeCheck('mk_attribute_type_values', $polo_tshrit_att_val);
		$polo_tshrit_att_val_id = db_insert_id();
		$tshirt_att_val_id = $tshirt_att_val['id'];
		
		$map_array = array(
						'id_tshirt' => $tshirt_att_val['id'],
						'id_polo_tshirt' => $polo_tshrit_att_val_id
					);
		$insert_map = func_array2insert('mk_tshirt_value_polo_tshirt_value', $map_array);
		$t_att_val_id_map_pt_att_val_id[$tshirt_att_val_id] = $polo_tshrit_att_val_id;
	}

}
echo "Tshirt-attribute-type-value-id-MAPPING-WITH-Polo-tshirt-attribute-type-value-id";
print_r($t_att_val_id_map_pt_att_val_id);



?>