<?php
require_once "../auth.php";
require_once $xcart_dir."/include/func/func.db.php";

$fileDirectory = $argv[1];
$fileName = $argv[2];
$mainTable = $argv[3];
$backupTable = $argv[4];
$fieldSeparator = $argv[5];

csvToSolrFeed($fileDirectory, $fileName, $mainTable, $backupTable, $fieldSeparator);


function csvToSolrFeed($fileDirectory, $fileName, $mainTableName, $backupTableName, $fieldSeparator){
        try{
                $mainTableName = "myntra_solr_feed." . $mainTableName;
                $backupTableName = "myntra_solr_feed." . $backupTableName;
                $dataOnlyFile = $fileDirectory . '/'. $fileName;
                echo $dataOnlyFile;
                if(db_query("truncate table $backupTableName")) {
                        if(db_query("load data local infile '" . $dataOnlyFile ."'into table $backupTableName fields terminated by '" . $fieldSeparator . "'  lines terminated by '\n'")) {
                                db_query("rename table $mainTableName to myntra_solr_feed.tmpTable, $backupTableName to $mainTableName, myntra_solr_feed.tmpTable to $backupTableName");
            }
            else {
                print_r("load data failed");
            }
        }
        else {
            print_r("truncating backup table failed");
        }
        }catch(Exception $e){
                echo "\nexception :: ";
                print_r($e->getMessage());
        }
}
