<?php
chdir(dirname(__FILE__));
include_once("../auth.php");
include_once "$xcart_dir/include/func/func.mail.php";
putenv("TZ=Asia/Calcutta");
$time = time();

//run a cumulative report for 24 hours
if(in_array('full',$argv)) {
	$timeDiff = 24 * 3600;
} else {
	$timeDiff = 3600;
}

$sql_tbl = array (
			'orders'			=> 'xcart_orders',
			'discount_coupons'	=> 'xcart_discount_coupons',
			'discount_coupons_login'=> 'xcart_discount_coupons_login'
			);
			
$sql = "select a.cash_coupon_code as coupon
				from 
				{$sql_tbl['orders']} a

		where
			a.status in ('Q','WP','PP','PV','OH','SH','DL','C','D','F') 
		and
			a.date > ".($time- 3600 - $timeDiff)."
		and 
			a.date < ".($time - 3600 )."
		and
			a.channel = 'web'
		and
			a.cash_coupon_code <> '';"; 

$result = func_query($sql,true);
$comma_seperated = "";
foreach ( $result as $row ) {
$str =  "'".$row['coupon']."',";
if(!strstr($comma_seperated,$str)) {
	$comma_seperated .= $str;
}
}
$comma_seperated=substr($comma_seperated,0,strlen($comma_seperated)-1);
if(empty($comma_seperated)) $comma_seperated="''";
$sql = "select a.coupon as coupon, a.status, a.times_locked as locked, a.times_used as used, a.users as users,
			b.times_locked as loginLocked, b.times_used as loginUsed
			from 
			{$sql_tbl['discount_coupons']} a, {$sql_tbl['discount_coupons_login']} b
			where 
			a.coupon=b.coupon 
		and a.users=b.login
		and 
			a.coupon in (".$comma_seperated.");";
$result = func_query($sql,true);
$unlocked_coupons = array();
// Iterate over the coupons and send the reminder mail.
foreach ($result as $row) {
if ($row['status'] != 'A' || $row['locked'] == 1 || $row['loginLocked'] == 1){
 array_push($unlocked_coupons, array('coupon'=>$row['coupon'],'users'=>$row['users']));
}
}
$mail_content = "";
$mail_content .= "<html>";
$mail_content .= "<head></head>";
$mail_content .= "<body>";
$mail_content .= "<br>Report for cashback coupons between ".date("F j, Y, g:i a", $time - 3600 - $timeDiff)." and ".date("F j, Y, g:i a", $time-3600)."<br>";
$mail_content .= "Number of locked cashback coupons: ".count($unlocked_coupons)."<br>";
if(count($unlocked_coupons) > 0 ) {						
    $mail_content .= "<tr style=\"background-color: #ebe7e6;\"><td align=\"center\">Coupon code</td>".
						"<td align=\"center\">Issued To</td>";
	foreach ( $unlocked_coupons as $row ) {
		$mail_content .= "<tr><td align=\"center\">".strtoupper($row['coupon'])."</td>";
		$mail_content .= "<td align=\"center\">".$row['users']."</td></tr>";
	}	
}
$mail_content .= "</body></html>";
  
$to = 'alrt_locked_coupons@myntra.com'; 
if(count($unlocked_coupons) > 0 ) {
	$subject = "Failure - Locked cashback coupons found [".count($unlocked_coupons)."]";
} else {
	$subject = "Success - Locked cashback coupons not found";
}
	
$body = $mail_content;
			
$headers = "Content-Type: text/html; charset=ISO-8859-1\r\n";
$headers .= "From: Mynt Club <no-reply@myntra.com>\r\n";
//$result = @mail($to, $subject, $body, $headers);
?>
