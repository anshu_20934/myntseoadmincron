<?php
chdir(dirname(__FILE__));
include("../auth.php");
include_once("../include/func/func.mkcore.php");
include_once("../include/func/func.randnum.php");
include("../PHPExcel/PHPExcel.php");

set_time_limit(0);

/*
 * Insert data in mk_attribute_type, mk_attribute_value tables to support the article type attributes
 * in our system.
 */

//get all parameters
$filepath = explode('=',$_SERVER['argv'][1]);
$filepath = trim($filepath[1]);

$errorpath = explode('=',$_SERVER['argv'][2]);
$errorpath = trim($errorpath[1]).'promo_user_error_'.time().'.xls';//append file name to the path given in argument

echo "verify all passed parameter--- \n filepath=".$filepath."\n errorpath=".$errorpath;
echo "\n\nif verified and correct, press- 'y', otherwise- 'n' >";

$input = trim(fgetc(STDIN));
if($input == 'n')
{
	echo "\n\nbye";exit;
}elseif($input == 'y'){

	if($filepath && $errorpath){
		//read a xls file
		$objReader = PHPExcel_IOFactory::createReader('Excel2007');//use xls file, use excel2007 for 2007 format
		$objPHPExcel1 = $objReader->load($filepath);
		$attribute_value_sheet = $objPHPExcel1->getSheetByName('Attribute Type  value options');
		$lastRow = $attribute_value_sheet->getHighestRow();

		for($i=2;$i<=$lastRow;$i++){//start from second row ignore heading

			$cell1Obj = ($attribute_value_sheet->getCell('A'.$i));
			$tmp_attribute_type = trim($cell1Obj->getvalue());

			echo "\n------>".$tmp_attribute_type;
			// check if the value is same as the attribute type


			$result=mysql_query("select id,catalogue_classification_id from mk_attribute_type where attribute_type='".$tmp_attribute_type."'");

			if (!$result) {
				$message  = 'Invalid query: ' . mysql_error() . "\n";
				$message .= 'Whole query: ' . $query;
				die($message);
			}


			while ($row = mysql_fetch_assoc($result)) {
				$attribute_type_id = $row[id];
				echo "\n           --> attribute type id: ".$row[id]." catalog:".$row[catalogue_classification_id];
				//insert value into the mk_style_article_type_attribute_values table
				$attribute_value_array = array();
				foreach(range('B','N') as $row_name){
					$attribute_value = trim($attribute_value_sheet->getCell($row_name.$i)->getvalue());
					array_push($attribute_value_array , $attribute_value);
				}
				insert_attribute_type_value($attribute_type_id,$attribute_value_array);
			}


		}


		echo "\n\nnumber of rows=".($attribute_value_sheet->getHighestRow()-1);

	}

}
function insert_attribute_type_value($attribute_type_id,$attribute_value_array){

	$select_query = sprintf("select id,attribute_value from  mk_attribute_type_values where attribute_type_id='".$attribute_type_id."'");
	$select_results=mysql_query($select_query);
	// remove already exisint entries from the array and remove non existing entries in array from db
	while ($select_result = mysql_fetch_assoc($select_results)) {
		if(in_array($select_result['attribute_value'], $attribute_value_array)){
			echo "\n   		->".$select_result['attribute_value']."   keep it in db";
			foreach($attribute_value_array as $tmp_key=>$tmp_value){
				if($select_result['attribute_value'] == $tmp_value){
					unset($attribute_value_array[$tmp_key]);
				}
			}
		}else{
			echo "\n   		->".$select_result['attribute_type']."   removing from db";
			$query = sprintf("select id from mk_attribute_type_values where id='".$select_result['id']."'");
			$deleted_rows = mysql_query($query);
			while ($deleted_row = mysql_fetch_assoc($deleted_rows)) {
				$query = sprintf("delete from mk_style_article_type_attribute_values
						where article_type_attribute_value_id='".$deleted_row['id']."'");
				func_query($query);
			}

			// delete attribute values
			//			echo "\n   			->deleting attribute_values for attribute type id".$select_result['id']."";
			$query = sprintf("delete from mk_attribute_type_values where id='".$select_result['id']."'");
			func_query($query);

		}
	}
	foreach($attribute_value_array as $tmp_key=>$tmp_value){
		if(!empty($tmp_value)){
			echo "\n   		->".$tmp_value."   insert in db";
			$query = sprintf("insert mk_attribute_type_values (
					attribute_value,attribute_type_id,is_active,applicable_set)
					values('%s','%d','%d','%s')",$tmp_value,$attribute_type_id,1,'Default');
			func_query($query);
		}
	}

}
?>