<?php
require_once "../auth.php";
require_once $xcart_dir."/include/func/func.db.php";

class TableToImport {
	const mainTable = "mk_style_visibility_info";
	const backupTable = "mk_style_visibility_info_backup";
}

download_through_ftp(BIConfig::$host, BIConfig::$userName, BIConfig::$password, BIConfig::$fileDirectory, BIConfig::$fileNameFormat, BIConfig::$archiveDirectory,TableToImport::mainTable,TableToImport::backupTable);


function download_through_ftp($host,$login,$passwd,$remoteFileDirectory,$remoteFilePattern,$remoteAchiveDirectory,$mainTableName,$backupTableName){
	try{
		$conn_id = ssh2_connect($host, 22);
		ssh2_auth_password($conn_id, $login, $passwd);

		if (!($stream = ssh2_exec($conn_id, 'ls -ltr ' . $remoteFileDirectory . '/'. $remoteFilePattern . '|tail -1|awk \'{print $NF}\'' ))) {
			echo "fail: unable to execute ls command\n";
		} else {
			stream_set_blocking($stream, true);
			$latestMatchingFileName = "";
			while ($buf = fread($stream,4096)) {
				$latestMatchingFileName .= trim($buf);
			}
			fclose($stream);
			echo("File picked :: $latestMatchingFileName\n");
		}
		if(!empty($latestMatchingFileName)){
			$pathParts = pathinfo($latestMatchingFileName);
			$localFile = $pathParts['basename'];
			$download = ssh2_scp_recv($conn_id, $latestMatchingFileName, $localFile);
			if($download){
				if(!($mvStream = ssh2_exec($conn_id, 'mv '. $latestMatchingFileName . ' '. $remoteAchiveDirectory))){
					echo "fail: unable to execute mv command\n";
				} else {
					stream_set_blocking($mvStream, true);
					while ($buf = fread($mvStream,4096)) {
						echo "\n move::" . trim($buf);
					}
					fclose($mvStream);
				}
				echo "\ndownload is successful\n";
				$calculatedMD5 = trim(shell_exec('head -n -1 ' . $localFile .'|openssl md5|awk \'{print $NF}\''));
				echo "\ncalulated md5 :: $calculatedMD5";
				$incomingLastLine = trim(shell_exec('tail -1 ' . $localFile));
				$incomingMD5 = trim(substr($incomingLastLine, strlen("Checksum:0x"))); 
				echo "\nincomingMD5 :: $incomingMD5";
				if(strcasecmp($incomingMD5, $calculatedMD5) == 0){
					echo "\nMD5's match"; 
					$dataOnlyFile = 'dataFile_' . time() . '.csv';
					shell_exec('head -n -1 ' . $localFile . '|tail -n +2 > ' . $dataOnlyFile);
					unlink($localFile);
					if(db_query("truncate table $backupTableName")){ 
						if(db_query("load data local infile '" . $dataOnlyFile ."'into table $backupTableName fields terminated by ',' lines terminated by '\n'")){ 
							if(db_query("rename table $mainTableName to tmpTable, $backupTableName to $mainTableName, tmpTable to $backupTableName")){;	   
								unlink($dataOnlyFile);
							}
						}
}
				}
			}
		}
	}catch(Exception $e){
echo "\nexception :: ";
		print_r($e->getMessage());
	}
}
