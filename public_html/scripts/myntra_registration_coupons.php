<?php
chdir(dirname(__FILE__));
include_once("../auth_for_solr_index.php");
$ft = fopen("customers.csv","r");
$fp = fopen("missingreferral.sql","w");

x_load('db');

// parse every line of the csv file
// line format: customer, referer 
$customers_email = array();
while(!feof($ft))	{
	$line = fgets($ft);
	if ($line != "") {
		$array = explode(",", trim($line));
		if(array_key_exists($array[0],  $customers_email)) {
			$customers_email[$array[0]] .= ",".$array[1];
		} else {
			$customers_email[$array[0]] = $array[1];
		}
	}
}
fclose($ft);


$mrpCouponConfiguration = array();

$mrpCouponConfiguration['mrp_refRegistration_numCoupons'] =
FeatureGateKeyValuePairs::getInteger("mrp.refRegistration.numCoupons", $mrpDefaultCouponConfiguration['mrp_refRegistration_numCoupons']);
$mrpCouponConfiguration['mrp_refRegistration_mrpAmount'] =
FeatureGateKeyValuePairs::getInteger("mrp.refRegistration.mrpAmount", $mrpDefaultCouponConfiguration['mrp_refRegistration_mrpAmount']);
$mrpCouponConfiguration['mrp_refRegistration_mrpPercentage'] =
FeatureGateKeyValuePairs::getInteger("mrp.refRegistration.mrpPercentage", 0);
$mrpCouponConfiguration['mrp_refRegistration_couponType'] =
FeatureGateKeyValuePairs::getFeatureGateValueForKey("mrp.refRegistration.couponType", 'absolute');
$mrpCouponConfiguration['mrp_refRegistration_validity'] =
FeatureGateKeyValuePairs::getInteger("mrp.refRegistration.validity", $mrpDefaultCouponConfiguration['mrp_refRegistration_validity']);
$mrpCouponConfiguration['mrp_refRegistration_minCartValue'] =
FeatureGateKeyValuePairs::getInteger("mrp.refRegistration.minCartValue", $mrpDefaultCouponConfiguration['mrp_refRegistration_minCartValue']);
 

$mrpCouponConfiguration['mrp_refFirstPurchase_numCoupons'] =
FeatureGateKeyValuePairs::getInteger("mrp.refFirstPurchase.numCoupons", $mrpDefaultCouponConfiguration['mrp_refFirstPurchase_numCoupons']);
$mrpCouponConfiguration['mrp_refFirstPurchase_mrpAmount'] =
FeatureGateKeyValuePairs::getInteger("mrp.refFirstPurchase.mrpAmount", $mrpDefaultCouponConfiguration['mrp_refFirstPurchase_mrpAmount']);
$mrpCouponConfiguration['mrp_refFirstPurchase_mrpPercentage'] =
FeatureGateKeyValuePairs::getInteger("mrp.refFirstPurchase.mrpPercentage", 0);
$mrpCouponConfiguration['mrp_refFirstPurchase_couponType'] =
FeatureGateKeyValuePairs::getFeatureGateValueForKey("mrp.refFirstPurchase.couponType", 'absolute');
$mrpCouponConfiguration['mrp_refFirstPurchase_validity'] =
FeatureGateKeyValuePairs::getInteger("mrp.refFirstPurchase.validity", $mrpDefaultCouponConfiguration['mrp_refFirstPurchase_validity']);
$mrpCouponConfiguration['mrp_refFirstPurchase_minCartValue'] =
FeatureGateKeyValuePairs::getInteger("mrp.refFirstPurchase.minCartValue", $mrpDefaultCouponConfiguration['mrp_refFirstPurchase_minCartValue']);


function generateFirstPurchaseCoupon($referer,$userName,$adapter, $orderid, $firstQueuedDate) {
	global $fp;
	$num_coupons = $mrpCouponConfiguration['mrp_refFirstPurchase_numCoupons'] ;
	$mrpAmount = $mrpCouponConfiguration['mrp_refFirstPurchase_mrpAmount'] ;
	$minCartValue = $mrpCouponConfiguration['mrp_refFirstPurchase_mrpAmount'];
	$validity = $mrpCouponConfiguration['mrp_refFirstPurchase_validity'];	 
	$percentage = $mrpCouponConfiguration['mrp_refFirstPurchase_mrpPercentage'];
	$couponType = $mrpCouponConfiguration['mrp_refFirstPurchase_couponType'];

	for ($i = 0; $i < $num_coupons; $i++) {
            echo "inside for loop\n";
            
            $startDate = strtotime(date('d F Y', time()));
            $endDate = $startDate + $validity * 24 * 60 * 60;
            $desc = "Referral of $userName - Rs. $mrpAmount coupon";

            echo "will generate coupon..\n";
            $couponCode = $adapter->generateSingleCouponForUser("REF", 8, "MrpReferrals", $startDate, $endDate, $referer, $couponType, $mrpAmount, $percentage, $minCartValue, $desc);
		
            echo "generated. adding log..\n";
            // Logging success of referral of Mrp.
			$_curtime = time();
			fwrite($fp,"INSERT INTO mk_mrp_log (ruleid,rulename,timestamp,appliedOn,result,coupon,couponUser) VALUES ('3','referral','$_curtime','$orderid','1','$couponCode','$referer');");

            // Add customer-coupon mapping.
			fwrite($fp,"INSERT INTO mk_mrp_coupons_mapping (login,coupon) VALUES ('$referer','$couponCode');");
            
			if(isset($couponCodeString))	{
				$couponCodeString.=", ";
			}
			$couponCodeString.=$couponCode;
            echo "generated... coupon: >>$couponCode<< for referer: $referer\n";
        }	

		fwrite($fp, "UPDATE mk_referral_log	set dateFirstPurchase='$firstQueuedDate', ordQueuedCoupon='$couponCodeString' where referer='$referer' AND referred='$userName';");
		        // Mail the referer.
        $template = "mrp_referral_successful";
        $referer_tw = str_replace("@", "%40", $referer);
        global $http_location;
        $args = 
            array (
                "REFERER"        => $referer,
                "REFERER_TW"	 => $referer_tw,
                "REFERRED"       => $userName,
                "MRP_AMOUNT"     => $mrpAmount * $num_coupons,
                "REFERANDEARNURL"=> $http_location."/mymyntra.php?view=myreferrals&utm_source=MRP_mailer&utm_medium=referral_first_purchase&utm_campaign=refer_and_earn",
                "MRPLANDINGPAGE" => $http_location."/myntclub?utm_source=MRP_mailer&utm_medium=referral_first_purchase&utm_campaign=mrp_know_more",
            	"NUM_COUPONS"	=> $num_coupons,
        		"EACH_COUPON_VALUE" => $mrpAmount,
            	"VALIDITY"		=> $validity,
        		"MIN_PURCHASE_AMOUNT" => $minCartValue
        );
        
        $subject_args=array ("MRP_AMOUNT"  => $mrpAmount * $num_coupons);
        // sendMessageDynamicSubject($template, $args, $referer, "MRP",$subject_args);


}


function generateCoupon($referer,$refered,$adapter) {
	global $fp;
	
	$num_coupons = $mrpCouponConfiguration['mrp_refRegistration_numCoupons'] ;
	$mrpAmount = $mrpCouponConfiguration['mrp_refRegistration_mrpAmount'] ;
	$minCartValue = $mrpCouponConfiguration['mrp_refRegistration_mrpAmount'];
	$validity = $mrpCouponConfiguration['mrp_refRegistration_validity'];
	$percentage = $mrpCouponConfiguration['mrp_refRegistration_mrpPercentage'];
	$couponType = $mrpCouponConfiguration['mrp_refRegistration_couponType'];
	
	
	$startDate = strtotime(date('d F Y', time()));
	
	$endDate = $startDate + $validity * 24 * 60 * 60;
	$couponCode = $adapter->generateSingleCouponForUser("Reg", "8", "MrpReferrals", $startDate, $endDate, $referer,$couponType, $mrpAmount, $percentage, $minCartValue, "Referral of $refered- Rs. 500 coupon");
		
	// Logging success of first login of mrp.
	/*$toInsert = array();
	$toInsert['ruleid'] = '0'; //ask what to keep here
	$toInsert['rulename'] = 'old_customers';//ask what to keep here
	$toInsert['timestamp'] = time();
	$toInsert['appliedOn'] = $login;
	$toInsert['result'] = '1';
	$toInsert['coupon'] = $couponCode;
	$toInsert['couponUser'] = $login;
	func_array2insert("mk_mrp_log", $toInsert);*/
	$_curtime = time();
	fwrite($fp, "INSERT INTO mk_mrp_log (ruleid,rulename,timestamp,appliedOn,result,coupon,couponUser) VALUES ('1','first_login','$_curtime','$refered','1','$couponCode','$referer');");

	// Add customer-coupon mapping.
	/*$toInsert = array();
	$toInsert['login'] = $login;
	$toInsert['coupon'] = $couponCode;
	func_array2insert("mk_mrp_coupons_mapping", $toInsert);*/
	fwrite($fp,"INSERT INTO mk_mrp_coupons_mapping (login,coupon) VALUES ('$referer','$couponCode');");
	$dateJoined = func_query_first_cell("select first_login from xcart_customers where login='$refered';",true);
    // Update mk_referral table with coupon codes
	fwrite($fp,"UPDATE mk_referral_log
				SET regcompleteCoupon = '$couponCode', success = '1', dateJoined='$dateJoined'
				WHERE referer='$referer' AND referred='$refered';");
		
	fwrite($fp, "UPDATE xcart_customers set referer='$referer' where login='$refered';");		
	// Mail the referral
            $template = "mrp_ref_registration";
			$referer_tw = str_replace("@", "%40", $referer);
				$args = 
            array (
                "REFERER"        => $referer,
                "REFERER_TW"	 => $referer_tw,
                "REFERRED"       => $refered,
                "MRP_AMOUNT"     => 500,
            	"REF_AMOUNT"	 => 1000, 
                "REFERANDEARNURL"=> $http_location."/mymyntra.php?view=myreferrals&utm_source=MRP_mailer&utm_medium=referral_registration&utm_campaign=refer_and_earn",
                "MRPLANDINGPAGE" => $http_location."/myntclub?utm_source=MRP_mailer&utm_medium=referral_registration&utm_campaign=mrp_know_more",
            	"NUM_COUPONS"	=> 1,
        		"EACH_COUPON_VALUE" => 500,
            	"VALIDITY"		=> 15,
        		"MIN_PURCHASE_AMOUNT" => 1500,
				"MRP_AMOUNT"  =>  500
		);
		$subject = array("MRP_AMOUNT"  =>  500);
		// sendMessageDynamicSubject($template, $args, $referer, "MRP",$subject);
							
}

$registration_coupons=0;
$purchase_coupons=0;
$adapter = CouponAdapter::getInstance();

foreach($customers_email as $customer=>$referers) {
	$query = "select orderid, queueddate from xcart_orders where status in ('C','Q','WP','OH','SH','DL') and login='$customer' order by queueddate limit 1;";
	$result = func_query_first($query, true);
	$genPurchaseCoupon = false;
	if(!empty($result)) {
		$orderid=$result['orderid'];
		$queueddate = $result['queueddate'];
		$genPurchaseCoupon = true;
	}
	$refarray = explode(",",$referers);
	foreach ($refarray 	as $referer) {
		generateCoupon($referer, $customer, $adapter); 
		$registration_coupons++;	
		if($genPurchaseCoupon)	{
			generateFirstPurchaseCoupon($referer,$customer,$adapter, $orderid, $queueddate);
			$purchase_coupons+=2;
		}
	}
	echo "Done for customer:".$customer." and referers:".$referers."\n";
}
fclose($fp);
echo "Finished \n";
echo "Registration Coupons Generated: ". $registration_coupons." Coupons!\n";
echo "First Purchase Coupons Generated: ". $purchase_coupons." Coupons!\n";

?>
