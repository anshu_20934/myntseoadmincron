<?php
chdir(dirname(__FILE__));
include_once("../auth_for_solr_index.php");
include_once ("$xcart_dir/modules/coupon/database/CouponAdapter.php");

/**
Validate an email address.
Provide email address (raw input)
Returns true if the email address has the email 
address format and the domain exists.
*/
function validEmail($email)
{
   $isValid = true;
   $atIndex = strrpos($email, "@");
   if (is_bool($atIndex) && !$atIndex)
   {
      $isValid = false;
   }
   else
   {
      $domain = substr($email, $atIndex+1);
      $local = substr($email, 0, $atIndex);
      $localLen = strlen($local);
      $domainLen = strlen($domain);
      if ($localLen < 1 || $localLen > 64)
      {
         // local part length exceeded
         $isValid = false;
      }
      else if ($domainLen < 1 || $domainLen > 255)
      {
         // domain part length exceeded
         $isValid = false;
      }
      else if ($local[0] == '.' || $local[$localLen-1] == '.')
      {
         // local part starts or ends with '.'
         $isValid = false;
      }
      else if (preg_match('/\\.\\./', $local))
      {
         // local part has two consecutive dots
         $isValid = false;
      }
      else if (!preg_match('/^[A-Za-z0-9\\-\\.]+$/', $domain))
      {
         // character not valid in domain part
         $isValid = false;
      }
      else if (preg_match('/\\.\\./', $domain))
      {
         // domain part has two consecutive dots
         $isValid = false;
      }
      else if (!preg_match('/^(\\\\.|[A-Za-z0-9!#%&`_=\\/$\'*+?^{}|~.-])+$/', str_replace("\\\\","",$local)))
      {
         // character not valid in local part unless 
         // local part is quoted
         if (!preg_match('/^"(\\\\"|[^"])+"$/',
             str_replace("\\\\","",$local)))
         {
            $isValid = false;
         }
      }
      if ($isValid && !(checkdnsrr($domain,"MX") || checkdnsrr($domain,"A")))
      {
         // domain not found in DNS
         $isValid = false;
      }
   }
   return $isValid;
}

function generateCoupon($login,$adapter) {
	$mrpAmount = 250.00;
	$minCartValue = 1000.00;
	$startDate = strtotime(date('d F Y', time()));
	$validity = 30;
	$endDate = $startDate + $validity * 24 * 60 * 60;
	$couponCode = $adapter->generateSingleCouponForUser("Mynt", "6", "FirstLogin", $startDate, $endDate, $login, 'absolute', $mrpAmount, '', $minCartValue, "Old Users on Myntra - Rs. 250 coupon");
	
	// Logging success of first login of mrp.
	/*$toInsert = array();
	$toInsert['ruleid'] = '0'; //ask what to keep here
	$toInsert['rulename'] = 'old_customers';//ask what to keep here
	$toInsert['timestamp'] = time();
	$toInsert['appliedOn'] = $login;
	$toInsert['result'] = '1';
	$toInsert['coupon'] = $couponCode;
	$toInsert['couponUser'] = $login;
	func_array2insert("mk_mrp_log", $toInsert);*/
	$_curtime = time();
	$query = "INSERT INTO mk_mrp_log (ruleid,rulename,timestamp,appliedOn,result,coupon,couponUser) VALUES ('0','old_customers','$_curtime','$login','1','$couponCode','$login')";
	mysql_query($query);
	
	// Add customer-coupon mapping.
	/*$toInsert = array();
	$toInsert['login'] = $login;
	$toInsert['coupon'] = $couponCode;
	func_array2insert("mk_mrp_coupons_mapping", $toInsert);*/
	$query = "INSERT INTO mk_mrp_coupons_mapping (login,coupon) VALUES ('$login','$couponCode')";
	mysql_query($query);
}

if(!isset($_SERVER["argv"][1])) {
	echo "Please specify the offset to start from, remember to put the offset from the point where the last execution ended.\n";
	exit;
}

if(!isset($_SERVER["argv"][2])) {
	echo "Please specify the number of users to create coupons for.\n";
	exit;
}

$offset =$_SERVER["argv"][1];
$number_of_users =$_SERVER["argv"][2];
$limit = 1000;
echo "Started with OFFSET: $offset \n";
$totalCustomers = func_query_first_cell("select count(*) from xcart_customers where usertype='C' and last_login>(UNIX_TIMESTAMP('2010-01-01 00:00:00')) and first_login <(UNIX_TIMESTAMP('2011-07-01 00:00:00'))");
$adapter = CouponAdapter::getInstance();
for($j=0;$j<$number_of_users;) {
	echo "Current OFFSET: $offset \n";
	$sql = "select login from xcart_customers where usertype='C' and last_login>(UNIX_TIMESTAMP('2010-01-01 00:00:00')) and first_login <(UNIX_TIMESTAMP('2011-07-01 00:00:00')) order by first_login desc limit $limit offset $offset";
	$customers_email = func_query($sql);
	mysql_query("BEGIN");
	foreach($customers_email as $emails) {
		$email = $emails['login'];
		if(validEmail($email)) {
	    	//echo "$email is a valid email\n";
		    $fb_check = "select count(*) from mk_facebook_user where myntra_login ='$email'";
			$fb_exists = func_query_first_cell($fb_check);
			if($fb_exists>0) {
				//fb user
				for ($i = 0; $i < 3; $i++) {
					generateCoupon($email,$adapter);
				 }
				
			} else {
				//normal myntra user
				 for ($i = 0; $i < 2; $i++) {
					generateCoupon($email,$adapter);
				 }
			}
	    
		} else {
	    	//echo "$email is NOT a valid email\n";
		}
	}
	mysql_query("COMMIT");
	//sleep for 1 seconds
	sleep(1);
	$offset = $offset+$limit;
	$j=$j+$limit;
	if($offset>$totalCustomers) break;
}
mysql_query("COMMIT");
echo "Finished at OFFSET: $offset \n";
?>