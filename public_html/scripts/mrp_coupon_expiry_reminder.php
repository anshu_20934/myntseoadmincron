<?php
chdir(dirname(__FILE__));
include_once("../auth.php");
include_once "$xcart_dir/include/func/func.mail.php";
ini_set("memory_limit",'2500M');

$time = time();
//from start of the day
$start_stamp = strtotime(date('Y-m-d', time())." +1 day");
//to the end of it
$end_stamp = $start_stamp + 86400;

$query = "select users, coupon, couponType, MRPAmount, MRPpercentage, expire, minimum, isInfinite, times_used, maxUsageByUser, times_locked from xcart_discount_coupons where showInMyMyntra=1 and status='A' and expire >= $start_stamp and expire < $end_stamp";
$results = func_query($query,true);
//echo "<pre>"; print_r($results);

$userSpecificCoupons = array();
foreach($results as $key => $value)	{
	if(empty($value['users']) || empty($value['couponType']) || empty($value['coupon'])){
		continue;
	}

	if($value['couponType'] == "absolute")	{
		if(empty($value['MRPAmount']))	
			continue;
	} else if ($value['couponType'] == "percentage")	{
		if (empty($value['MRPpercentage']))
			continue;
	} else {
		if(	empty($value['MRPAmount']) || empty($value['MRPpercentage']))
			continue;
	}	
	if(($value['isInfinite']) && ($value['times_used'] <= $value['maxUsageByUser']) || ($value['isInfinite'] ||(($value['times_used'] + $value['times_locked']) < $value['maxUsageByUser'])))
    {
		$userSpecificCouponValue = array(
								"coupon" => $value['coupon'],
								"couponType" => $value['couponType'],
								"MRPAmount" => $value['MRPAmount'],
								"MRPpercentage" => $value['MRPpercentage'],
								"expire" => $value['expire'],
								"minimum" => $value['minimum']
								);
	

		$userSpecificCoupons[$value['users']][] = $userSpecificCouponValue;
	}
}

//echo "<pre>"; print_r($userSpecificCoupons); 

$couponTablePrefix = "<tr style=\"background-color: #ebe7e6;\"><td align=\"center\">Coupon code</td>".
						"<td align=\"center\">Value</td>".
						"<td align=\"center\">Expires on</td></tr>";


function sendThisMail($user, $couponTable) {
    $args = array(  
        "DISPLAY_COUPON_TABLE" => $couponTable
    );
    
    $mailDetails = array(
            "template" => 'mrp_coupon_expiry_reminder',
            "from_name" => "Mynt Club",
            "from_email" => "no-reply@myntra.com",
            "to" => $user,
            "header" => 'mrp_header',
            "footer" => 'mrp_footer',
            "mail_type" => \MailType::NON_CRITICAL_TXN
        );
    $multiPartymailer = new \MultiProviderMailer($mailDetails, $args);
    // return $multiPartymailer->sendMail();
    return false
}

function computeValue($coupon)	{
	if($coupon['couponType'] == "absolute")	{
		if( !empty($coupon['minimum']) && $coupon['minimum'] >0 )	{
			return "Rs ".round($coupon['MRPAmount'])." off on min. purchase of Rs ".round($coupon['minimum']);
		}
		return "Rs ".$coupon['MRPAmount']." off";
	} else if ($coupon['couponType'] == "percentage") {
		return $coupon['MRPpercentage']."% off";
	} else {
		return $coupon['MRPpercentage']."% upto Rs ".round($coupon['MRPAmount'])." off";
	}
} 

function formatTime($time){
	return date("d F, Y",$time);
}

$countEmailsSent =0;
$emailSentArray = array();						
foreach($userSpecificCoupons as $user=>$couponArray) {
	$couponTable = $couponTablePrefix;		
		foreach($couponArray as $coupon)	{
		
			$couponTable .= "<tr><td align=\"center\">".strtoupper($coupon['coupon'])."</td>"; // add coupon code
			$couponTable .= "<td align=\"center\">".computeValue($coupon)."</td>";
			$couponTable .= "<td align=\"center\">".formatTime($coupon['expire'])."</td>";
			$couponTable .= "</tr>";
			// update table field 'timeReminderEmailSent'
            //NOTE : we're not using this due to resource constraints
			//db_query("update xcart_discount_coupons set timeReminderEmailSent=".$time." where coupon='".$coupon['coupon']."';");
		}

        if (false === sendThisMail($user, $couponTable)) {
            echo "failed to send coupon expiry reminder email for ".$user."\n";
        }
		$emailSentArray[] = "Sent email to ".$user;
		$countEmailsSent++;
}

echo "Cron for coupon expiry reminder started on ". date("F j, Y, g:i a", $time)."\n";
//echo "Cron finished on ".date("F j, Y, g:i a")."\n";
echo __FILE__.": No of emails sent: ".$countEmailsSent."\n";

/*foreach($emailSentArray as $key){
	echo $key."\n";
} */

?>
