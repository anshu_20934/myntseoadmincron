#!/bin/bash
# This script will take svn up for a directory passed as argument
curr_dir=`pwd`
if [ $# -lt 1 ]
then
    echo "Please pass the folder path as argument"
    exit
fi
cd $1 
svn_ps_count=`ps aux | grep 'svn up' | grep -v grep | wc -l`
if test $svn_ps_count -eq 0
then
echo taking svn up for $1 
svn up
fi
cd  $curr_dir
