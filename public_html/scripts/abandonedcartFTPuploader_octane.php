<?php
/*
 * uploader script uploads the present day csv file to octane
 * and initiates a processing of these files.
 */

//credentials of octane ftp server where we can upload our CSV files
$ftphost   =  "119.9.81.80"; 
$ftpuser   =  "smyntra";
$ftppasswd =  "My@nTr@0ct@ne2014";
$ftpport   =  "22";
$usr       =  '78dfed76cb159579779f0b840e669d4b';
$pwd       =  'myntra@api1';      

//This function makes a curl request
function curl_data_post($post, $url) {
	$DATA_POST = curl_init ();
	curl_setopt ( $DATA_POST, CURLOPT_RETURNTRANSFER, true );
	curl_setopt ( $DATA_POST, CURLOPT_URL, $url );
	curl_setopt ( $DATA_POST, CURLOPT_POST, true );
	curl_setopt ( $DATA_POST, CURLOPT_FRESH_CONNECT, true );
	curl_setopt ( $DATA_POST, CURLOPT_POSTFIELDS, $post );
	curl_setopt ( $DATA_POST, CURLOPT_SSL_VERIFYPEER, false);
	return $response = curl_exec ( $DATA_POST );
}

// This function informs our client to process the file in remote directory
function processFileByClient($fileName) {	
	global $usr, $pwd;
	$url = 'https://api.myntmail.in/V2.0/Request/?Send-Request';
	$xmlData = '<?xml version="1.0" encoding="UTF-8"?>
	<SendEmail>
		<Password>'.$pwd.'</Password>
		<ApiKey>'.$usr.'</ApiKey>
		<Subscribers>
			<File>'.$fileName.'</File>
		</Subscribers>
	</SendEmail>';
	
	$post = array ('xmlData' => $xmlData );
	$result =  curl_data_post ( $post, $url );
	echo "Trigger for Processing file.. Response --> \n $result \n";
	if (strpos($result, "<Success>0</Success>") !== false)
		return false;
	else return true;
}

function main() {
	echo "trying to connect..... ";
	global $ftphost, $ftpport, $ftpuser, $ftppasswd;
	
	// Authenticate in the next two steps
	$connection = ssh2_connect($ftphost, $ftpport);
	if (!ssh2_auth_password($connection, $ftpuser, $ftppasswd)) {
		echo "Unable to connect to host.. \n";
		exit;
	}
	echo "Connected to the SFTP host with proper credentials.. \n";
	
	// Create our SFTP resource
	if (!$sftp = ssh2_sftp($connection)) {
		echo "Unable to create SFTP connection..\n";
		exit;
	}
	echo "Successfully created SFTP connection.. \n";
	
	$today = time();
	$campaignDate =  date("Ymd",$today);
	
	$dirname = "/triggers_api/target/";
	$localDirname = "/var/mailer_uploads/abncart/";	
	echo "Starting SFTP uploads...\n";
	
	//Match files that need to be uploaded.
	$fileNames = glob($localDirname.$campaignDate.".*.csv");
	for($j=0;$j<count($fileNames); $j++) {
		$baseFileName = basename($fileNames[$j]);
		echo "Processing $baseFileName now..\n";
		
	    $remoteFile = str_replace($localDirname, $dirname, $fileNames[$j]);
	    $stream = fopen("ssh2.sftp://$sftp$remoteFile", 'w');
	    $file = file_get_contents($fileNames[$j]);
	    $writeSuccess = fwrite($stream, $file);
	    
	    if ($writeSuccess == FALSE) {
	    	echo "SFTP upload failed for file -> $baseFileName \n";
	    	continue;
	    }
	    fclose($stream);	    
	    echo "Now initate a trigger to Octane for this file..\n";
	    if (processFileByClient($baseFileName))
	    	echo "This file will be processed by Octane..\n";
	    else echo "This file will not be processed by Octane\n";
	    sleep(1);
	}

	echo "abandonedcartFTPuploader.php: cron finished at ".date("r", time())."\n";
	echo "============================end abandonedcartMailer================================\n";
}

main();

?>
