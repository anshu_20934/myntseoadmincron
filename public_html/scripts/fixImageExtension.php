<?php
/**To extract query to update images back to original images incase this script fails : 
awk '{if($2 == "baseImages" && $3) {printf "update mk_style_properties set " $3 $4 $5 ; {if ($6) {printf "," $6 $7 $8;}}; {if ($9) {printf "," $9 $10 $11;}}; {if ($12) {printf "," $12 $13 $14;}}; {if ($15) {printf "," $15 $16 $17;}}; {if ($18) {printf "," $18 $19 $20;}}; {if ($21) {printf "," $21 $22 $23;}}; {if ($24) {printf "," $24 $25 $26;}}; {if ($27) {printf "," $27 $28 $29;}}; printf " where style_id = "$1; print ";"}}' <NAME OF THE CSV FILE>
*/ 
chdir(dirname(__FILE__));
include_once '../env/DB.config.php';
include_once '../env/System.config.php';
include_once '../utils/S3.php';
$updateDBPath = false;
if($argc > 1 && strtolower($argv[1]) == "true"){
	$updateDBPath = true;
}

date_default_timezone_set("Asia/Calcutta");
$con = dbConnect();
$bucket = SystemConfig::$amazonS3Bucket;
$s3 = new S3(SystemConfig::$amazonAccessKey, SystemConfig::$amazonSecretKey);

$unreferencedImagePaths = "";
$newImagePaths = "";

$tempDir = sys_get_temp_dir()."/fixImagesExtension";
if(!file_exists($tempDir) && !is_dir($tempDir)){
	$wasSuccessful = mkdir($tempDir);
	if(!$wasSuccessful){
		die("Failed to write the image paths to log file");
	}
}

$toBeDeletedList = "$tempDir/toBeDeleted_".date("Y-m-d_H-i-s").".csv";
$toBeUpdatedList = "$tempDir/toBeUpdated_".date("Y-m-d_H-i-s").".csv";


$valuesToSet = "";
$old_values = "";
$old_mini_values = "";
$old_values_for_resolutions = "";
$old_mini_values_for_resolutions = "";

$defaultResolutionCSV = "1080X1440,540X720,360X480,150X200,96X128,81X108,48X64";//240X320,180X240,
$otherResolutionCSV = "1080X1440,540X720,360X480,48X64,81X108";

fix($toBeDeletedList,$toBeUpdatedList);

if($con){
	mysql_close($con);
}

function writeDataToFile($data,$filePath){
	$fp = fopen($filePath,"a");
	fwrite($fp,$data);
	fclose($fp);
}

function dbConnect(){
	$con = mysql_connect(DBConfig::$rw['host'],DBConfig::$rw['user'],DBConfig::$rw['password']);
	if($con == false){
		die("Cannot open DB connection.. Give up!");
	}
	mysql_select_db(DBConfig::$rw['db'],$con);
	return $con;
}

function fix($toBeDeletedList,$toBeUpdatedList){
	global $con,$unreferencedImagePaths,$newImagePaths,$updateDBPath;

	$baseSql = "
				select style_id,default_image,search_image,front_image,back_image,
				left_image,right_image,top_image,bottom_image,size_representation_image_url 
				from mk_style_properties where BINARY default_image like '%.JPG' or 
				BINARY search_image like '%.JPG' or BINARY left_image like '%.JPG' or BINARY right_image like '%.JPG' or 
				BINARY top_image like '%.JPG' or BINARY bottom_image like '%.JPG' or BINARY front_image like '%.JPG' or 
				BINARY back_image like '%.JPG' or BINARY size_representation_image_url like '%.JPG'";
	//Process 100 styles at a time
	$batchSize = 2;//make this value 100
	$start = 0;

	for($start=0;;){
		$limitClause = " limit $start,$batchSize";
		$sql = $baseSql.$limitClause;
		$res = mysql_query($sql,$con);
		if($res == false){
			break;
		}
		$numRows = 0;
		//clear these string every batch, so that after every batch, file would be appended and closed. this is helpful when we hit Ctrl+C in between 
		$unreferencedImagePaths = "";
		$newImagePaths = "";
		$numRowsSuccessfullyUpdated = 0;
		while($row=mysql_fetch_assoc($res)){
			$numRows++;
			$styleId = $row['style_id'];
			$wasSuccessful = processStyle($row);
			
			if($wasSuccessful){
				echo "\n$styleId : Completed Successfully\n";
				$numRowsSuccessfullyUpdated++;
			}
			else{
				echo "\n$styleId : Processing failed\n";
			}
		}
		if($numRows == 0){
			break;
		}
		//write every batch at once to file
		writeDataToFile($unreferencedImagePaths,$toBeDeletedList);
		writeDataToFile($newImagePaths,$toBeUpdatedList);
		//if db was updated, result set of query will change
		if($updateDBPath){
			$start = $batchSize - $numRowsSuccessfullyUpdated;
		}else{//if db was not updated, result set of query will not change
			$start+=$batchSize;
		}
	}
}

function processStyle($row){
	global $valuesToSet,$old_values,$old_mini_values,$old_values_for_resolutions,$old_mini_values_for_resolutions,$defaultResolutionCSV,$otherResolutionCSV;
	$styleId = $row['style_id'];
	echo "\n================================Processing StyleId: $styleId ===================================\n";
	$valuesToSet = "";
	$old_values = "";
	$old_mini_values = "";
	$old_values_for_resolutions = "";
	$old_mini_values_for_resolutions = "";

	$imageType = "default_image";
	processImageHelper($imageType,$row[$imageType],$styleId,$defaultResolutionCSV);

	$imageType = "search_image";
	processImageHelper($imageType,$row[$imageType],$styleId,null);

	$imageType = "left_image";
	processImageHelper($imageType,$row[$imageType],$styleId,$otherResolutionCSV);

	$imageType = "right_image";
	processImageHelper($imageType,$row[$imageType],$styleId,$otherResolutionCSV);

	$imageType = "top_image";
	processImageHelper($imageType,$row[$imageType],$styleId,$otherResolutionCSV);

	$imageType = "bottom_image";
	processImageHelper($imageType,$row[$imageType],$styleId,$otherResolutionCSV);

	$imageType = "front_image";
	processImageHelper($imageType,$row[$imageType],$styleId,$otherResolutionCSV);

	$imageType = "back_image";
	processImageHelper($imageType,$row[$imageType],$styleId,$otherResolutionCSV);

	$imageType = "size_representation_image_url";
	processImageHelper($imageType,$row[$imageType],$styleId,null);

	return updateDBAndPrintResults($styleId);
}


function processImageHelper($imageType,$imageCdnPath,$styleId,$resolutionCSV){
	global $valuesToSet,$old_values,$old_mini_values;
	if(isset($imageCdnPath) && endsWith($imageCdnPath,".JPG")){
		$result = processImage($imageType,$imageCdnPath,$styleId,$resolutionCSV);
		if(isset($result) && is_array($result)){
			$valuesToSet = $valuesToSet . " $imageType = '" . $result[0] ."' , ";
			$old_values = $old_values . " $imageType = '$result[1]'";
			$old_mini_values = $old_mini_values . " $imageType = '$result[3]'"; 
		}
	}	
}

/**returns false if image copy fails for base image or mini image. Else returns an array with elements baseTargetImage,baseOriginImage,miniTargetImage,miniOriginImage*/
function processImage($imageType,$imageCdnPath,$styleId,$resolutionCSV){
	echo "\nimageType : $imageType ";
	//echo "Copying base image:";
	$ret1 = copyImage($imageType,$imageCdnPath,$styleId);
	$ret2 = copyResolutionImages($imageType,$imageCdnPath,$styleId,$resolutionCSV);
	if(isset($ret1) && is_array($ret1) && $ret2){
		return $ret1;
	}else{
		return false;
	}
}

/**
* Conditionally copy base image if target does not exists. Also copies the minified path corresponding to given imageCdnPath
* @param unknown_type $imageType
* @param unknown_type $imageCdnPath
* @param unknown_type $styleId
* @return if copy is successful, returns an array having full CDN path of the target image (copy destination), original image path, target image path of
* minified image and original image path of minified image.
* If copy failed, then returns false.
*/
function copyImage($imageType,$imageCdnPath,$styleId){
	
	$ret1 = copyBaseImageHelper($imageType,$imageCdnPath,$styleId);
	if(!(isset($ret1) && is_array($ret1))){
		echo "Copy failed ..";
	}else{
		if($ret1[0]){
			echo "Copied successfully..";
		}else{
			echo "Copy was not required, target image already existed..";
		}
	}

	echo "Copying minified image:";
	$ret2 = copyMinifiedImage($imageType,$imageCdnPath,$styleId);
	if(!(isset($ret2) && is_array($ret2))){
		echo "Copy failed ..";
	}else{
		if($ret2[0]){
			echo "Copied successfully..";
		}else{
			echo "Copy was not required, target image already existed..";
		}
	}

	//if any of ret1 or ret2 is not set or is not an array => copy was failed
	if(!(isset($ret1) && is_array($ret1)) || !(isset($ret2) && is_array($ret2)) ){
		return false;
	}else{
		return array($ret1[1],$ret1[2],$ret2[1],$ret2[2]);
	}
}

/**Helper function to copy image from given imageCdnPath to target image path which is generated using imageCdnPath*/
function copyBaseImageHelper($imageType,$imageCdnPath,$styleId){
	$origImageS3Path = getS3Path($imageCdnPath); //image path which is to be deleted
	$targetImageCdnPath = getTargetImagePath($imageCdnPath);
	//get S3 path for image above
	$targetImageS3Path = getS3Path($targetImageCdnPath);//image path to which image is to be updated

	if(!url_exists($targetImageS3Path)){
		$imageRelatviePath = getRelativePath($imageCdnPath);
		$targetImageRelativePath = getRelativePath($targetImageCdnPath);
		$res = copyWithinS3($imageRelatviePath,$targetImageRelativePath);
		return (isset($res['hash']) && url_exists($targetImageS3Path)) ? array(true,$targetImageCdnPath,$origImageS3Path) : false;
	}
	return array(false,$targetImageCdnPath,$origImageS3Path);
}

/**Copies minified path corresponding to the given imageCdnPath to S3*/
function copyMinifiedImage($imageType,$imageCdnPath,$styleId){
	$srcImageMiniCdnPath = getMinifiedImagePath($imageCdnPath);
	return copyBaseImageHelper($imageType,$srcImageMiniCdnPath,$styleId);
}

/**Copies all resolutions specified in the resolution csv along with their minified images*/
function copyResolutionImages($imageType,$imageCdnPath,$styleId,$resolutionCSV){
	global $old_mini_values_for_resolutions,$old_values_for_resolutions;
	$isSuccess = true;
	if(!empty($resolutionCSV)){
		$resolutionArray = explode(",", $resolutionCSV);
		foreach ($resolutionArray as $key => $resolution) {
			$resolutionURL = getScaledPath($imageCdnPath,$resolution);
			$ret1 = copyBaseImageHelper($imageType,$resolutionURL,$styleId);
			if(isset($ret1) && is_array($ret1)){
				echo "copied $resolution..";
				$old_values_for_resolutions = $old_values_for_resolutions . " $imageType$resolution = '$ret1[2]'";
			}else{
				$isSuccess = false;
				echo "copy failed for resolution : " . $resolution . ".. ";
			}

			$ret2 = copyMinifiedImage($imageType,$resolutionURL,$styleId);
			if(isset($ret2) && is_array($ret2)){
				echo "copied mini $resolution..";
				$old_mini_values_for_resolutions = $old_mini_values_for_resolutions . " $imageType$resolution = '$ret2[2]'"; 
			}else{
				$isSuccess = false;
				echo "copy minified failed for resolution : " . $resolution . ".. ";
			}
			
		}
	}
	return $isSuccess;
}

function updateDBAndPrintResults($styleId){
	global $unreferencedImagePaths,$newImagePaths,$updateDBPath;
	global $valuesToSet,$old_values,$old_mini_values,$old_values_for_resolutions,$old_mini_values_for_resolutions;
	//If image path in db also needs to be updated
	if($updateDBPath){
		$rowsUpdated = updateImagePath($valuesToSet,$styleId);
		if($rowsUpdated > 0){
			$unreferencedImagePaths .= "$styleId\tbaseImages\t$old_values\n";
			$unreferencedImagePaths .= "$styleId\tminiImages\t$old_mini_values\n";
			$unreferencedImagePaths .= "$styleId\tbaseImagesResolution\t$old_values_for_resolutions\n";
			$unreferencedImagePaths .= "$styleId\tminiImagesResolution\t$old_mini_values_for_resolutions\n";
			$newImagePaths .="$styleId\t$valuesToSet\n";
			return true;
		}else{
			return false;
		}
	}else{
		echo "\nDb update was set to false, so not updating db...";
		$unreferencedImagePaths .= "$styleId\tbaseImages\t$old_values\n";
		$unreferencedImagePaths .= "$styleId\tminiImages\t$old_mini_values\n";
		$unreferencedImagePaths .= "$styleId\tbaseImagesResolution\t$old_values_for_resolutions\n";
		$unreferencedImagePaths .= "$styleId\tminiImagesResolution\t$old_mini_values_for_resolutions\n";
		$newImagePaths .="$styleId\t$valuesToSet\n";
		return true;
		
	}
}


function getS3Path($cdnPath){
	global $bucket;
	return str_replace("myntra.myntassets.com","$bucket.s3.amazonaws.com",$cdnPath);
}

function getRelativePath($fullPath){
	global $bucket;
	$rep1 = str_replace("http://myntra.myntassets.com/","",$fullPath);
	$rep2 = str_replace("http://$bucket.s3.amazonaws.com/","",$rep1);
	return $rep2;
}

function getTargetImagePath($imagePath){
	if(endsWith($imagePath, ".JPG")){
		$targetImagePath = str_replace(".JPG",".jpg",$imagePath);
	}
	else{
		$targetImagePath = false;
	}
	return $targetImagePath;
}

function getMinifiedImagePath($path){
	if(endsWith($path, ".jpg")){
		$minifiedPath = str_replace(".jpg","_mini.jpg",$path);
	}
	elseif(endsWith($path, ".JPG")){
		$minifiedPath = str_replace(".JPG","_mini.JPG",$path);
	}
	else{
		$minfiedPath = false;
	}
	return $minifiedPath;
}

function copyWithinS3($src,$target){
	global $s3,$bucket;
	$res = $s3->copyObject($bucket,$src,$bucket,$target, S3::ACL_PUBLIC_READ);
	return $res;
}

function updateImagePath($valuesToSet,$styleId){
	if(!empty($valuesToSet)){
		echo "\nUpdating images in DB for styleId : $styleId  ";
		$valuesToSet = rtrim($valuesToSet,", ");
		$sql = "UPDATE mk_style_properties set $valuesToSet where style_id = $styleId";
		global $con;
		//echo $sql;
		mysql_query($sql,$con);
		$rows = mysql_affected_rows($con);
		if($rows > 0){
			echo "Updated successfully ..";
		}else{
			echo "Update failed ..";
		}
		return $rows;
	}
	return false;
}

function getScaledPath($imageCdnPath,$resolution){
	$res = explode("X",$resolution);
	if(count($res) < 1){
		return $imageCdnPath;
	}
	$width = $res[0];
	$height = $res[1];
	$result = str_replace(".jpg", "_".$width."_".$height.".jpg", $imageCdnPath);
	$result = str_replace(".JPG", "_".$width."_".$height.".JPG", $result);
	return $result;
}
function endsWith($haystack, $needle){
	$length = strlen($needle);
	if($length == 0){
		return true;
	}

	$start = $length * -1; //negative
	return (substr($haystack,$start) === $needle);
}

function url_exists($url){
	$hdrs = @get_headers($url);
	return is_array($hdrs) ? preg_match('/^HTTP\\/\\d+\\.\\d+\\s+2\\d\\d\\s+.*$/',$hdrs[0]) : false;
}
?>