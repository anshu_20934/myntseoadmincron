#!/bin/sh

cd /home/myntradomain/public_html/sitemaps
head="myntrawebimages"
listOfFiles=`ls`

for file in $listOfFiles
do
    echo file :: $file
    /usr/bin/s3cmd put --acl-public $file s3://$head/sitemaps/$file
    cp $file /mnt/scribestore/sitemaps
done
