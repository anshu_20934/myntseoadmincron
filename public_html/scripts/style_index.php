<?php
/*

Procedure to use this script
php style_index.php 
no param; index all styles which are public,active and whose PT is public
we no longer support giving arguments like 1,2 for customizable/non-customizable
now 1st argument will be for page_number and 2nd for page_size
*/

chdir(dirname(__FILE__));
include_once("../auth_for_solr_index.php");
include_once("../include/solr/solrProducts.php");

set_time_limit(0);
echo "Building index for Non Customizable styles";

$start_time=time();

// Set Page numbers to default 5

if(!empty($_SERVER["argv"][1])){
	$page_number = $_SERVER["argv"][1];
}else if (!empty($_REQUEST['page_number'])){
	$page_number = $_REQUEST['page_number'];
}else{
	$page_number = -1;
}

// Set Page size to default 4000

if(!empty($_SERVER["argv"][2])){
	$page_size = $_SERVER["argv"][2];
}else if(!empty($_REQUEST['page_size'])){
	$page_size = $_REQUEST['page_size'];
}else{
	$page_size = 5000;
}

// Set default indexing to all the servers

if(empty($_SERVER["argv"][3]) && empty($_REQUEST['local'])){
	$only_local_server = false;
}else{
	$only_local_server = true;
}

$solrProducts = new SolrProducts();
$solrProducts->indexNonCustomizedStyles($page_number,$page_size,$only_local_server);

$end_time=time();

$diff_time= ($end_time - $start_time) / 60;
echo "\n=========== Total time :: ".$diff_time." ==================\n";

?>
