<?php
/*
 * Myntra shopping fest coupons cancellation for cancelled order
 * 
 */


require_once "../auth.php";
include_once "$xcart_dir/include/func/func.order.php";
include_once("$xcart_dir/Cache/CacheKeySet.php");
$argv = getopt("c:g:");
if(!isset($argv["g"])){
	echo "group name not set : ".print_r($argv,true); exit;
}
$couponsGroupName = $argv["g"];//WidgetKeyValuePairs::getWidgetValueForKey('myntraFestCouponGroupName');



$lastTime = func_query_first("SELECT * from myntra_fest_cron_timestamps where name='$couponsGroupName'");
if(empty($lastTime)){
	db_query("insert into myntra_fest_cron_timestamps (name , updatedOn) values ('$couponsGroupName' , ".(time() - 24*60*60)." )");
	echo "inserted new time \n";
	$lastTime = func_query_first("SELECT * from myntra_fest_cron_timestamps where name='$couponsGroupName'");
	print_r($lastTime);
}
$lastTime = $lastTime["updatedOn"];
//$lastTime = func_query_first_cell("select unix_timestamp(UTC_DATE())",true);

echo "got the last time : ".date("r",$lastTime)." \n";

$query = "select coupon_code from myntra_fest_order_coupon_map as map  , xcart_orders as orders, cancellation_codes cc,xcart_discount_coupons coup  where coup.coupon = coupon_code and coup.status != 'D' and orders.cancellation_code = cc.id and cc.cancel_type = 'CCR' and  orders.status = 'F' and canceltime > $lastTime and map.order_id=orders.group_id";

//"select coupon_code from myntra_fest_order_coupon_map where order_id in (SELECT group_id FROM xcart_orders where status = 'F' and canceltime > $lastTime) ";

$time = time();
$coupons = func_query($query);
$count =0;
foreach($coupons as $id=>$coupon ){
	db_query("UPDATE xcart_discount_coupons set status = 'D' where coupon ='".$coupon['coupon_code']."'");
	$count++;
}

db_query("UPDATE myntra_fest_cron_timestamps set updatedOn = $time where name ='$couponsGroupName'  ");
if($argv['c']=="en" || true){

	//$lastTime = 1354320000;
	$query2 = "select xo.orderid,xo.status ,xo.login , c.coupon, xo.subtotal,xo.cash_redeemed ,(coupon_discount)from xcart_orders as xo, xcart_discount_coupons as c where  xo.date > unix_timestamp(UTC_DATE()) and  c.groupName='$couponsGroupName' and c.status = 'D' and xo.coupon = c.coupon and xo.status in ('WP','OH','Q') ";
	
	$orderIDs = func_query($query2);
	$count2 = 0;
	foreach($orderIDs as $id=>$order){

		$orderid = $order["orderid"];
		$cancel_args = array(
                               'cancellation_type'                 => "CCC",
                               'reason'                            => "INVDCOUP",
                               'orderid2fullcancelordermap'        => array($orderid=>true),
                               'generateGoodWillCoupons'           => false,
                               'is_oh_cancellation'                => false
                       );
                  
		

		func_change_order_status($orderid, 'F', "MYNTRA_FEST_COUPON_CRON", "MOMDAY COUPON MISUSE", '', true, '', $cancel_args);
			echo "\n\n cancelled order : $orderid \n";
		$count2++;
	}
}
echo "no of disabled coupons : $count \n";
echo "no of canceled orders : $count2 \n";
echo "set the last time : $time \n";
echo date("r", time())."\n";
