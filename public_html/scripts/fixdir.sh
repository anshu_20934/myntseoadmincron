# mv /home/myntradomain/public_html/images/style/properties/* /home/myntradomain/portal_bulkuploads/properties.old && ln -s /home/myntradomain/portal_bulkuploads/image_style_properties/ /home/myntradomain/public_html/images/style/properties && chown -R apache:apache /home/myntradomain/portal_bulkuploads/image_style_properties/ && mv /home/myntradomain/portal_bulkuploads/properties.old/properties /home/myntradomain/portal_bulkuploads/image_style_properties/

bulkuploaddir="/home/myntradomain/portal_bulkuploads/"
pubhtmldir="/home/myntradomain/public_html/images/"

# Create style directory inside images
if [[ ! -d "$pubhtmldir/style" ]]
then
    echo "Creating directory $pubhtmldir/style";
    mkdir -p "$pubhtmldir/style";
    chown -R apache:apache "$pubhtmldir/style";
fi

function createLinks () {

    if [[ ! -d "$target" ]]
    then
        echo "Fatal Error: $target directory does not exist. You'll not be able to upload corresponding type images";
        return 1;
    fi

    # if image_style_propertes exists as a directory, but symlink does not exist, create symlink
    if [[ ! -L "$src" &&  -d "$target" ]]
    then
        # if pubhtml/image/style/properties exists as a dir instead of symlink
        if [[ -d "$src" ]]
        then
            echo "$src exists as a regular directory, while it should have been a symlink. Moving files and removing the directory";
            mv "$src/*.jpg" "$target/"
            rm -rf "$src"
        fi
        echo "creating symlink for properties by command: ln -s $target $src";
        ln -s "$target" "$src"
        chown -R apache:apache "$target"
    fi

}


operatingdir="style/properties"
targetdir="image_style_properties"

src="$pubhtmldir$operatingdir";
target="$bulkuploaddir$targetdir";

createLinks

## Do the same for a different directory set

operatingdir="style/sizechartimages"
targetdir="sizechartimages"

src="$pubhtmldir$operatingdir";
target="$bulkuploaddir$targetdir";

createLinks

## Do the same for a different directory set

operatingdir="style/style_search_image"
targetdir="style_search_image"

src="$pubhtmldir$operatingdir";
target="$bulkuploaddir$targetdir";

createLinks

## Do the same for a different directory set

operatingdir="banners"
targetdir="banners"

src="$pubhtmldir$operatingdir";
target="$bulkuploaddir$targetdir";

createLinks
