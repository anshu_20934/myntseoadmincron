<?php
require_once  "../authAPI.php"; 
require_once $xcart_dir.'/modules/RestAPI/RestRequest.php';
require_once $xcart_dir.'/env/Host.config.php';
//search notifications starting from today
$url = \HostConfig::$notificationsUrl.'/searchbystart';
$method  = 'GET';
$request = new \RestRequest($url, $method, $data);
$headers = array();
$headers[] = 'Accept: application/json';
$headers[] = 'Content-type: application/json';
$headers[] = 'Accept-Language: en-US';
$request->setHttpHeaders($headers);
$request->execute();
$info = $request->getResponseInfo();
$body = $request->getResponseBody();
if($info['http_code'] == 200){
	$response = json_decode($body,true);	
	if($response['NotificationResponse']['status']['statusType'] == 'SUCCESS')
	{
		$tempArr = $response['NotificationResponse']['NotificationList']['data'];
		if($response['NotificationResponse']['status']['totalCount'] == 1){
			$dataForBulkIncrement = createJSONForBulkIncrement($tempArr['id']);
			if(count($dataForBulkIncrement)){
				restForBulkIncrement($dataForBulkIncrement);
			}	
		}
		else
		{
			echo"else";
			foreach ($tempArr as $value){
				$dataForBulkIncrement = createJSONForBulkIncrement($value['id']);
				if(count($dataForBulkIncrement)){
					restForBulkIncrement($dataForBulkIncrement);
				}
			}								
		}
	}
}

function restForBulkIncrement($dataForBulkIncrement){	
	$url = \HostConfig::$notificationsUpsUrl.'/bulkIncrement';
	$method = 'POST';
	$data = json_encode($dataForBulkIncrement);
	$request = new \RestRequest($url, $method, $data);
	$headers = array();
	$headers[] = 'Accept: application/json';
	$headers[] = 'Content-type: application/json';
	$headers[] = 'Accept-Language: en-US';
	$request->setHttpHeaders($headers);
	$request->execute();	
}


function createJSONForBulkIncrement($ntfId){	
	$url = \HostConfig::$notificationsUrl.'/'.$ntfId;
	$method = 'GET';
	$headers = array();
	$headers[] = 'Accept: application/json';
	$headers[] = 'Content-type: application/json';
	$headers[] = 'Accept-Language: en-US';
	$request = new \RestRequest($url, $method);
	$request->setHttpHeaders($headers);
	$request->execute();
	$info = $request->getResponseInfo();
	$body = $request->getResponseBody();	
	$jsonArr = array();
	if($info['http_code'] == 200){
		$response = json_decode($body,true);		
		if($response['NotificationResponse']['status']['statusType'] == 'SUCCESS'){
			$userList = $response['NotificationResponse']['NotificationList']['data']['userList'];
			if (count($userList) == 1)
			{	
				$tempUserId	= $userList['userId'];
				$jsonArr[$tempUserId] = array();
				$jsonArr[$tempUserId]['attribute'] = 'notificationCounter';
				$jsonArr[$tempUserId]['sourceid'] = '3';	
			}
			else{
				foreach ($userList as $value) {
					$tempUserId	= $value['userId'];
					$jsonArr[$tempUserId] = array();
					$jsonArr[$tempUserId]['attribute'] = 'notificationCounter';
					$jsonArr[$tempUserId]['sourceid'] = '3';		
				}			
			}
		}
	}
	return $jsonArr;
}
?>