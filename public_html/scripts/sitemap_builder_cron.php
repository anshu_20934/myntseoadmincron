<?php
define('USE_TRUSTED_CL_SCRIPT_VARIABLES',1);
require_once("../auth.php");
require_once("$xcart_dir/include/func/func.utilities.php");

function validatePath($path) {
	if (!empty($path)) {
		exec("ls $path 2> /dev/null",$list, $status);
		return !$status;
	}
	return false;
}

/*
option flags
static : 1
update : 2
links  : 4
*/
$_options = array(  'static' => 1,
					'update' => 2,
			);

/* The jobs configured on this file:
 * #: sitemap generation and post that, amazonS3 upload
 * usage: php <SCRIPT> <SITEMAP-SRC-DST> <FLAGS>
 */
if (isset($argv[1]) && validatePath($argv[1])) {
	$sitemapsDirectory = preg_replace('{/$}', '', $argv[1]);

	echo ("cleaning directory $sitemapsDirectory ...\n");
	shell_exec("rm $sitemapsDirectory/*.xml* 2> /dev/null");
} else {
	die("usage: php <SCRIPT> <SITEMAP-SRC-DST> <FLAGS>\n");
}

if (isset($argv[2])) {
	$_opt_static = ($argv[2] & $_options['static'])?true:false;
	$_opt_update = ($argv[2] & $_options['update'])?true:false;
} else {
	$_opt_static = false;
	$_opt_update = false;
}

require_once($xcart_dir."/include/class/search/SearchProducts.php");
require_once $xcart_dir."/include/class/sitemaps/SitemapGenerator.php";
require_once $xcart_dir."/include/class/sitemaps/SitemapUrlDecorators.php";
require_once $xcart_dir."/include/class/sitemaps/SitemapXmlDecorators.php";
require_once $xcart_dir."/include/class/sitemaps/SitemapDataFetcher.php";
require_once($xcart_dir."/include/func/func.mkcore.php");

ini_set('memory_limit','1024M');
$starttime = microtime(true);

$site = SitemapMyntra::SITE_URL;
$url_index = array(); //Contains list of urls of xml files - for sitemap-index file
$lastmod_index = array(); //Contains list of last modification dates of urls of xml files

if ($_opt_static) {
	echo 'Creating static'.PHP_EOL;
	$sitemap = new SitemapFileGenerator('sitemap-static',$sitemapsDirectory);
	$sitemap->addUrl("{$site}");
	$sitemap->addUrl("{$site}faqs");
	$sitemap->addUrl("{$site}faqs#shipping");
	$sitemap->addUrl("{$site}faqs#cancel");
	$sitemap->addUrl("{$site}faqs#returns");
	$sitemap->addUrl("{$site}termsofuse");
	$sitemap->addUrl("{$site}privacypolicy");
	$sitemap->addUrl("{$site}contactus");
	$sitemap->addUrl("{$site}aboutus");
	$sitemap->addUrl("{$site}careers");
	$sitemap->write();
	$url_index = array_merge($url_index,$sitemap->getFileUrls());
	$lastmod_index = array_merge($lastmod_index,$sitemap->getLastmods());
	echo 'Creating static Complete - Count : '.count($sitemap->getFileUrls()).PHP_EOL;
	unset($sitemap);
}


/*echo "Creating Search".PHP_EOL;
$searchSitemap = new SearchSitemapData();
if($run['update'] == true) $searchSitemap->updateOnlyLast = true;
while($searchSitemap->hasMore() == true ) {
$writeSitemapFile = false;
$rows = $searchSitemap->getData();
$filename = 'sitemap-search';
$filename = $searchSitemap->getFileNum() > 0 ? $filename.$searchSitemap->getFileNum() : $filename;
$sitemap = new SitemapFileGenerator($filename);
foreach($rows as $r) {
	$lastmod = date(DATE_ATOM,$r['last_search_date']);
	$formattedUrl = $searchSitemap->getUrlFormatted($r);
	if(!empty($formattedUrl)) {
		$sitemap->addUrl($formattedUrl, array('lastmod'=>$lastmod));
		$writeSitemapFile = true;
	}
}

unset($rows);
if($writeSitemapFile) {
	$sitemap->write();
	$url_index = array_merge($url_index,$sitemap->getFileUrls());
	$lastmod_index = array_merge($lastmod_index,$sitemap->getLastmods());
}
}
echo 'Creating search Complete '.PHP_EOL;
unset($sitemap);
unset($searchSitemap);
*/
//the following code should be uncommented
echo 'Creating products and Images'.PHP_EOL;
$productSitemap = new ProductSitemapData();
if($_opt_update) $productSitemap->updateOnlyLast = true;

do{
	//$imageSitemap = new ImageSitemapData();
	$rows = $productSitemap->getData();
	if(empty($rows)) continue;	
	$fileName = $productSitemap->getFileName();
	$fileType = $productSitemap->getFileType();
	
	$sitemap = new SitemapFileGenerator($fileName,$sitemapsDirectory);
	
	foreach($rows as $r) {
		$lastmod = ($fileType == "pdp")?$r->global_attr_catalog_add_date:date(DATE_ATOM,time());
        $url = $productSitemap->getUrlFormatted($r,$fileType);
        $priority = ($fileType == "pdp") ? "1.0":$r['priority'];
		if(!empty($url)) {
			$sitemap->addUrl($url,array('lastmod'=>$lastmod,'priority'=>$priority)); 
			//$imageSitemap->addProductDetails(array('styleid'=>$styleid, 'url'=>$url));
		}
	} 
	unset($rows);
	$sitemap->write();
	$url_index = array_merge($url_index,$sitemap->getFileUrls());
	$lastmod_index = array_merge($lastmod_index,$sitemap->getLastmods());

	//Image sitemap removed temporarily
	//$rows = $imageSitemap->getData();
	//$sitemap_image = new SitemapImageGenerator("sitemap-product$file_num-images");
	//foreach($rows as $r) {
	//	$sitemap_image->addUrl($r['url'],array('image'=>$r['image']));
	//}
	//unset($rows);
	//$sitemap_image->write();//Can create multiple files
	//$url_index = array_merge($url_index,$sitemap_image->getFileUrls());

	// Code for image to get product sitemap
	/*
	$tmp_fileurls = $sitemap_image->getFileUrls();
	$tmp_lastmod_image_product = $sitemap->getLastmods();
	foreach ($sitemap->getFileUrls() as $prod_url) {
		foreach($tmp_fileurls as $url) {
			$tmp_lastmod_image[$url] = $tmp_lastmod_image_product[$prod_url]; 
		}
	}
	$lastmod_index = array_merge($lastmod_index,$tmp_lastmod_image);
	// Code for image to get product sitemap ends
	unset($sitemap_image);
	*/
} while($productSitemap->hasMore() == true );
echo 'Creating product/images Complete - '.PHP_EOL;


echo 'Creating Landing Pages'.PHP_EOL;
$urls = $productSitemap->getLandingPageUrls();
$limit = SitemapMyntra::MAX_URLS_PER_SITEMAP;
$fileCount = 0;

for ($i = 0, $n = count($urls); $i < $n; $i += $limit) {
    $fileCount += 1;
    $fileName = 'sitemap-land-' . $fileCount;
	$sitemap = new SitemapFileGenerator($fileName, $sitemapsDirectory);

    $m = min($i + $limit, $n);
    for ($j = $i; $j < $m; $j += 1) {
        $url = $urls[$j];
        $sitemap->addUrl($url['url'], array('lastmod' => $url['lastmod'],'priority'=>'1.0')); 
    }

	$sitemap->write();
	$url_index = array_merge($url_index,$sitemap->getFileUrls());
	$lastmod_index = array_merge($lastmod_index,$sitemap->getLastmods());
}

echo 'Creating Landing Pages - Completed'.PHP_EOL;


unset($productSitemap);
//unset($imageSitemap);
unset($sitemap);
//unset($sitemap_images);

if($_opt_update) {
	echo 'Fetching old index file for lsmods'.PHP_EOL;
	$doc = new DOMDocument();
	$a = $doc->load( 'sitemaps/sitemap-index.xml' );
	$books = $doc->getElementsByTagName( "sitemap" );
	foreach ($books as $b) {
		$tmp_url = $b->getElementsByTagName('loc')->item(0)->nodeValue;
		$tmp_url_index[] = $tmp_url;
		$tmp_lastmod_index[$tmp_url] = $b->getElementsByTagName('lastmod')->item(0)->nodeValue;
	}
	echo 'Fetching old index file for lsmods Complete '.PHP_EOL;
	$lastmod_index = array_merge($tmp_lastmod_index,$lastmod_index);
	unset ($books);
	unset($doc);
	foreach($url_index as $key=>$url) {
		if(!in_array($url,$tmp_url_index)) {
			$tmp_url_index[] = $url;
		}
	}
	$url_index = $tmp_url_index;
}

echo "Recreating index file".PHP_EOL;
$siteindex = new SitemapIndexGenerator('sitemap-index',$sitemapsDirectory);
foreach($url_index as $url) {
	$siteindex->addUrl("{$url}", array('lastmod'=>$lastmod_index[$url],'priority'=>'1.0'));	
}
$siteindex->write();
unset($siteindex);
echo "Recreating index file - Complete".PHP_EOL;

//cdn upload
define('S3_PATH_BASE', HostConfig::$cdnBase);
define('SITEMAPS_BUCKET_NAME', SystemConfig::$sitemapsS3Bucket);
define('SITEMAPS_MAX_TRIES', HostConfig::$sitemapsUploadMaxTries);

// push to the S3 *only* from the production environment

if (HostConfig::$configMode == 'prod') {
    echo "Uploading files to bucket ".SITEMAPS_BUCKET_NAME." on ".S3_PATH_BASE.PHP_EOL;
    $xmlFiles = scandir($sitemapsDirectory);
    $uploadCount = 0;
    foreach ($xmlFiles as $file) {
        if (preg_match('/\.xml$/i', $file)) {	//only xml files
            $filePath = $sitemapsDirectory.'/'.$file;

            echo("Compressing file $file ...\n");
            shell_exec("gzip $filePath");
            $filePath .= '.gz';
            $file .= '.gz';

            $fileURI = $file;
            $fileURL = SITEMAPS_BUCKET_NAME.".s3.amazonaws.com/$fileURI";

            echo("Copying file $filePath ...\n");
            $triesLeft = SITEMAPS_MAX_TRIES;
             while($triesLeft-- && 
                 !($uploaded = moveToS3($filePath, SITEMAPS_BUCKET_NAME, $fileURI,array('Content-Type'=>'application/x-gzip')))) {
                 echo("Could not copy $filePath to $fileURL\n");
                 echo("Trying again...\n");
             }

             if ($uploaded) {
                 echo("Copied $file to $fileURL\n");
                 $uploadCount++;
             } else {
                 echo("Could not copy $filePath to $fileURL\n");
             }
        }
    }
    echo "Finished Uploading".PHP_EOL;
}


echo "Script Complete".PHP_EOL;
$endtime = microtime(true);
$totaltime = ($endtime - $starttime);
$mem =  memory_get_peak_usage()/(1024*1024)." MB";

global $weblog;
$weblog->info('Sitemap script :: Script Arguments '.implode(" ",$argv));
$weblog->info('Sitemap script :: Created in '.$totaltime.' seconds');
$weblog->info('Sitemap script :: Peak memory usage'.$mem);
$weblog->info("Sitemap script :: Uploaded $uploadCount files to ".SITEMAPS_BUCKET_NAME." bucket in ".S3_PATH_BASE);
?>
