<?php
chdir(dirname(__FILE__));
include("../auth.php");
include_once("../include/func/func.mkcore.php");
include_once("../include/func/func.randnum.php");
include("../PHPExcel/PHPExcel.php");

set_time_limit(0);

/*
 * Insert data in mk_attribute_type, mk_attribute_value tables to support the article type attributes
 * in our system.
 */

//get all parameters
$filepath = explode('=',$_SERVER['argv'][1]);
$filepath = trim($filepath[1]);

$errorpath = explode('=',$_SERVER['argv'][2]);
$errorpath = trim($errorpath[1]).'promo_user_error_'.time().'.xls';//append file name to the path given in argument

echo "verify all passed parameter--- \n filepath=".$filepath."\n errorpath=".$errorpath;
echo "\n\nif verified and correct, press- 'y', otherwise- 'n' >";

$input = trim(fgetc(STDIN));
if($input == 'n')
{
	echo "\n\nbye";exit;
}elseif($input == 'y'){

	if($filepath && $errorpath){
		//read a xls file
		$objReader = PHPExcel_IOFactory::createReader('Excel2007');//use xls file, use excel2007 for 2007 format
		$objPHPExcel1 = $objReader->load($filepath);
		$attribute_value_sheet = $objPHPExcel1->getSheetByName('Template');
		$lastRow = $attribute_value_sheet->getHighestRow();


		for($i=2;$i<=$lastRow;$i++){//start from second row ignore heading

			$cell1Obj=($attribute_value_sheet->getCell('B'.$i));
			$style_id_value=trim($cell1Obj->getvalue());

			// if the value is empty it means this row contains the attribute types
			if(empty($style_id_value)){
				// add values for the next type of article type
				$attribute_values=array();
				$article_type=trim($attribute_value_sheet->getCell('C'.($i+1))->getvalue());
				foreach(range('D','K') as $row_name){
					$attribute_name=trim($attribute_value_sheet->getCell($row_name.$i)->getvalue());
					if(!empty($attribute_name)){
						$attribute_values[$row_name]=getAttributeId($article_type,$attribute_name);
					}
				}
			}else{
				remove_existing_styles($style_id_value);
				$attribute_values_for_style_table='';
				foreach($attribute_values as $key=>$attribute_type_id){
					$attribute_value=trim($attribute_value_sheet->getCell($key.$i)->getvalue());
					$attribute_value_id=getAttributeValueId($attribute_type_id,$attribute_value);
					if(!empty($attribute_value)){
						$attribute_values_for_style_table.=$attribute_value.",";
						//						echo "\n===== attibute value:".$attribute_value." id:".$attribute_value_id." style id:".$style_id_value;
						insert_style_attribute_type_value($style_id_value,$attribute_value_id);
					}
				}
				if(!empty($attribute_values_for_style_table)){
					//					echo "\nstyle id:".$style_id_value." ".substr($attribute_values_for_style_table,0,-1);
					updateStyleProperties(substr($attribute_values_for_style_table,0,-1),$style_id_value);
				}
			}
		}


		echo "\n\nnumber of rows=".($attribute_value_sheet->getHighestRow()-1);

	}

}
function updateStyleProperties($properties_str,$style_id){
	$query = "update mk_style_properties set article_specific_attributes='".$properties_str."'
	where style_id='".$style_id."'";

	//echo "\n\n\n\ninserting $source_code detail in mk_sources table...";
	//	echo $query;
	$status = mysql_query($query);
	if(!$status){
		$sqllog->debug("SQL Query : ".$query." "." @ line ".__LINE__." in file ".__FILE__);
	}
}
function getAttributeValueId($attribute_type_id,$attribute_value){
	$query = "SELECT id FROM  mk_attribute_type_values
			 	where attribute_type_id='".$attribute_type_id."' and attribute_value='".$attribute_value."'";
	//	echo "\n".$query;
	$result=mysql_query($query);
	if(!result){
		return '';
	}else{
		$row = mysql_fetch_row($result);
		return $row[0];
	}
}
function getAttributeId($article_type,$attribute_name){
	$query = "SELECT at.id as id, at.attribute_type
			 	FROM  mk_attribute_type at 
			 	left join mk_catalogue_classification cc on at.catalogue_classification_id=cc.id    
			 	where at.attribute_type='".$attribute_name."' and cc.typename='".$article_type."'";
	//	echo "\n".$query;
	$result=mysql_query($query);
	if(!result){
		return '';
	}else{
		$row = mysql_fetch_row($result);
		return $row[0];
	}

}

function insert_style_attribute_type_value($product_style_id,$article_type_attribute_value_id){
	echo "\n----Inserting new entries for  product_style_id='".$product_style_id."'";
	$query = sprintf("insert mk_style_article_type_attribute_values (
		                        product_style_id,article_type_attribute_value_id,is_active)
		                        values('%d','%d','%d')",$product_style_id,$article_type_attribute_value_id,1);

	echo "\n----Inserting new entries for  product_style_id='".$product_style_id."' done";
	//echo "\n\n\n\ninserting $source_code detail in mk_sources table...";
	//	echo $query;
	if(!empty($product_style_id) && !empty($article_type_attribute_value_id)){
		$status = mysql_query($query);
		if(!$status){
			$sqllog->debug("SQL Query : ".$query." "." @ line ".__LINE__." in file ".__FILE__);
		}
	}
}
function remove_existing_styles($product_style_id){
	$sql = "DELETE FROM mk_style_article_type_attribute_values where product_style_id='".$product_style_id."'";
	func_query($sql);
}
?>