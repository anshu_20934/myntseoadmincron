<?php
/*
 * uploader script uploads the present day csv file to icubes
 */
require_once "../admin/auth.php";
require_once $xcart_dir.'/include/class/csvutils/class.csvutils.ArrayCSVWriter.php';
require_once $xcart_dir.'/include/class/class.FTPActions.php';
require_once $xcart_dir.'/include/class/elabsmail/class.elabsmail.mailinglist.php';
include_once($xcart_dir."/Profiler/Profiler.php");

//Note that after including auth.php, all the previously defined variables locally get cleared.
$serverinfo_conf=parse_ini_file("../env/serverinfo.ini",true);
$elabsConfig = $serverinfo_conf["ELABS"];
$abnCartConfig = $serverinfo_conf["Abandoned Cart"];

//credentials of icubes ftp server where we can upload our CSV files
$ftphost = empty($elabsConfig["ftp_host"]) ? 'icubes.in' : $elabsConfig["ftp_host"];
$ftpuser = empty($elabsConfig["ftp_user"]) ? 'myntra' : $elabsConfig["ftp_user"];
$ftppasswd = empty($elabsConfig["ftp_password"]) ? 'M!ntra123' : $elabsConfig["ftp_password"];

//email id to which results of CSV upload should be sent
$emailToSendUploadStatus = empty($abnCartConfig["email_upload_status"]) ? 'myntramailarchive@gmail.com' : $abnCartConfig["email_upload_status"];
//this is the mailing listid in Lyris/Elabs to which the contacts have to be imported finally: It is named Abandoned Cart
$mailingListId = empty($abnCartConfig["mailingListId"]) ? "192717" : $abnCartConfig["mailingListId"];

$uploader = new FTPActions($ftphost, FALSE);
$uploader->setMode(FTP_BINARY);
$uploader->connect();
echo "Logging in user.. Result: ".$uploader->login($ftpuser, $ftppasswd)."\n";

$today = time();
$campaignDate =  date("Ymd",$today);

$dirname = "/abncart/";
$localDirname = "/var/mailer_uploads/abncart/";
//don't need old CSV files on FTP server so delete them
$uploader->deleteDirectoryRecursively($dirname);
$uploader->mkdir($dirname);
$uploader->chdir($dirname);

//---------------------------------------------------------------------------

$fileNames = glob($localDirname.$campaignDate.".*.csv");
for($j=0;$j<count($fileNames); $j++) {
    $remoteFile = str_replace($localDirname, $dirname, $fileNames[$j]);
    $ftpResult = $uploader->upload($fileNames[$j], $remoteFile) ; 	
    echo "Uploading ".$fileNames[$j]." to FTP Server with remote path: $remoteFile ; result: ".$ftpResult."\n";
    if($ftpResult == 1){
            Profiler::increment("192717-abandonedcart-csv-uploaded-to-FTP-SUCCESS");
    }else{
            Profiler::increment("192717-abandonedcart-csv-uploaded-to-FTP-FAILURE");
    }
    $pathToCsvFile = "ftp://$ftpuser:$ftppasswd@$ftphost/".$remoteFile;
    echo "Importing $pathToCsvFile to : $remoteFile \n";
    $uploadResult = MailingList::uploadContactsToMailingList($mailingListId, $pathToCsvFile, $emailToSendUploadStatus,'|');
    print_r($uploadResult);
    if($uploadResult["result"] == "success"){
            Profiler::increment("192717-abandonedcart-queued-into-icubes-SUCCESS");
    }else{
            Profiler::increment("192717-abandonedcart-queued-into-icubes-FAILURE");
    }
    sleep(1);
}


$uploader->close();

echo "abandonedcartFTPuploader.php: cron finished at ".date("r", time())."\n";
echo "============================end abandonedcartMailer================================\n";
?>
