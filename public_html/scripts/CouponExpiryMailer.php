<?php
/*
 * Coupon expiry mailer script to upload mailing list to icubes
 * 
 */

require_once "../admin/auth.php";
require_once $xcart_dir."/include/func/func.core.php";
require_once $xcart_dir."/include/dao/class/mcart/class.MCartDBAdapter.php";
require_once $xcart_dir."/include/class/mcart/class.MCart.php";
require_once $xcart_dir.'/include/class/csvutils/class.csvutils.ArrayCSVWriter.php';
require_once $xcart_dir.'/include/class/class.FTPActions.php';
require_once $xcart_dir.'/include/class/elabsmail/class.elabsmail.mailinglist.php';
include_once($xcart_dir."/Profiler/Profiler.php");

//TODO: add logger support to non-webroot-scripts
echo "==============================begin CouponExpiryMailer==================================\n";
echo "CouponExpiryMailer.php: cron started at ".date("r", time())."\n";




//-------------------------------------------------------------------------------

$time = time();
//from start of the day
$start_stamp = strtotime(date('Y-m-d', time())." +2 day");
//to the end of it
$end_stamp = $start_stamp + 86400;

$batchLimit = 100000;
$currentOffset = 0;
$countOfRecords = 0;
$batchNumber = 1;
$countEmailsSent =0;
function computeValue($coupon)  {
        if($coupon['couponType'] == "absolute") {
                if( !empty($coupon['minimum']) && $coupon['minimum'] >0 )       {
                        return "Rs ".round($coupon['MRPAmount'])." off on min. purchase of Rs ".round($coupon['minimum']);
                }
                return "Rs ".$coupon['MRPAmount']." off";
        } else if ($coupon['couponType'] == "percentage") {
                if(!empty($coupon['minimum']) && $coupon['minimum'] >0){
                        return $coupon['MRPpercentage']."% off on min. purchase of Rs ".round($coupon['minimum']);
                }
                else
                {
                        return $coupon['MRPpercentage']."% off";
                }
        } else {
                return $coupon['MRPpercentage']."% upto Rs ".round($coupon['MRPAmount'])." off";
        }
}

function formatTime($time){
        return date("d F, Y",$time);
}

function getTopSix($query){

                $solrSearchProducts = new SearchProducts($query."-apparels",null,$getStatsInstance);
                $solrSearchProducts->do_search_new();
                $widgetData1 = $solrSearchProducts->products;

                $solrSearchProducts = new SearchProducts($query."-footwear",null,$getStatsInstance);
                $solrSearchProducts->do_search_new();
                $widgetData2 = $solrSearchProducts->products;

                $widgetArray = array();
                $count = 0 ;
                foreach($widgetData1 as $data){
                        $widgetArray[] = WidgetUtility::getWidgetFormattedData($data);
                        $count++;
                        if ($count==3)break;
                }
                $count = 0 ;
                foreach($widgetData2 as $data){
                        $widgetArray[] = WidgetUtility::getWidgetFormattedData($data);
                        $count++;
                        if ($count==3)break;
                }

                $widgetSKUData = array();
                $widgetSKUData['data'] = $widgetArray;

                return $widgetSKUData;

        }


for(;;) { 
$query = "select users,gender, coupon, couponType, MRPAmount, MRPpercentage, expire, minimum, isInfinite, times_used, maxUsageByUser, times_locked from xcart_discount_coupons LEFT JOIN xcart_customers ON xcart_discount_coupons.users = xcart_customers.login WHERE showInMyMyntra=1 and xcart_discount_coupons.status='A'  and expire >= $start_stamp and expire < $end_stamp LIMIT $batchLimit  OFFSET $currentOffset";
$results = func_query($query,true);
$countOfRecords = count($results);

$userSpecificCoupons = array();
$userGender=array();
foreach($results as $key => $value)	{
	if(empty($value['users']) || empty($value['couponType']) || empty($value['coupon'])){
		continue;
	}

	if($value['couponType'] == "absolute")	{
		if(empty($value['MRPAmount']))
			continue;
	} else if ($value['couponType'] == "percentage")	{
		if (empty($value['MRPpercentage']))
			continue;
	} else {
		if(	empty($value['MRPAmount']) || empty($value['MRPpercentage']))
			continue;
	}
	if(($value['isInfinite']) && ($value['times_used'] <= $value['maxUsageByUser']) || ($value['isInfinite'] ||(($value['times_used'] + $value['times_locked']) < $value['maxUsageByUser'])))
	{
		$userSpecificCouponValue = array(
				"coupon" => $value['coupon'],
				"couponType" => $value['couponType'],
				"MRPAmount" => $value['MRPAmount'],
				"MRPpercentage" => $value['MRPpercentage'],
				"expire" => $value['expire'],
				"minimum" => $value['minimum']
		);

		
		$userSpecificCoupons[$value['users']][] = $userSpecificCouponValue;
	}
	$userGender[$value["users"]] = $value["gender"];
}


$couponTablePrefix = "<tr style=\"background-color: #ebe7e6;\"><td align=\"center\">Coupon code</td>".
		"<td align=\"center\">Value</td>".
		"<td align=\"center\">Expires on</td></tr>";




$today = time();
$campaignDate =  date("Ymd",$today);
$localDirname = "/var/mailer_uploads/coupon_expiry_remainder/";
mkdir($localDirname,0777,true);
//delete all CSV files locally inside this directory
$mask = "*.csv";
array_map("unlink",$localDirname.$mask);
//local file which will be created this time
$fileName[0] = $localDirname.$campaignDate."coupon1.".$batchNumber.".csv";
$fileName[1] = $localDirname.$campaignDate."coupon2.".$batchNumber.".csv";
$fileName[2]= $localDirname.$campaignDate."coupon3.".$batchNumber.".csv";
//file name on FTP server which would be copied
$remoteFile[0] = $dirname.$campaignDate."coupon1.".$batchNumber.".csv";
$remoteFile[1]= $dirname.$campaignDate."coupon2.".$batchNumber.".csv";
$remoteFile[2]= $dirname.$campaignDate."coupon3.".$batchNumber.".csv";



Profiler::increment("205490-coupon1-expiry-mailing-list-csv-generation-started");
$profilerCount1 = 0;
$writer1 = new ArrayCSVWriter($fileName[0], "|");
$writer1->setHeaderColumns(Array("email","coupon1","coupon_expires_on1","Coupon_text1","product_suggestions_html"));
$writer1->writeHeader();


Profiler::increment("205491-coupon2-expiry-mailing-list-csv-generation-started");
$profilerCount2 = 0;
$writer2= new ArrayCSVWriter($fileName[1], "|");
$writer2->setHeaderColumns(Array("email","coupon1","coupon_expires_on1","Coupon_text1","coupon2","coupon_expires_on2","Coupon_text2","product_suggestions_html"));
$writer2->writeHeader();


Profiler::increment("205492-coupon3-expiry-mailing-list-csv-generation-started");
$profilerCount3 = 0;
$writer3 = new ArrayCSVWriter($fileName[2], "|");
$writer3->setHeaderColumns(Array("email","coupon1","coupon_expires_on1","Coupon_text1","coupon2","coupon_expires_on2","Coupon_text2","coupon3","coupon_expires_on3","Coupon_text3","product_suggestions_html"));
$writer3->writeHeader();


$menProducts = getTopSix("mens");
$womenProducts = getTopSix("womens");
$smarty->assign('coupon_count',(sizeOf($couponArray)>3?3:sizeOf($couponArray)));
$smarty->assign("avail_recommendation",$menProducts);
$menProductsHtml = $smarty->fetch("mail/coupon_expiry_products.tpl");

$menProductsHtml =  preg_replace("/\r\n/"," ", $menProductsHtml);
$menProductsHtml =  preg_replace("/\n/"," ", $menProductsHtml);

$smarty->assign("avail_recommendation",$womenProducts);
$womenProductsHtml = $smarty->fetch("mail/coupon_expiry_products.tpl");

$womenProductsHtml =  preg_replace("/\r\n/"," ", $womenProductsHtml);
$womenProductsHtml =  preg_replace("/\n/"," ", $womenProductsHtml);



foreach($userSpecificCoupons as $user=>$couponArray) {
	
	if(trim($userGender[$user]) == "f"||trim($userGender[$user]) == "F"){
		$productsHtmlBlock = $womenProductsHtml;
	}else{
		$productsHtmlBlock = $menProductsHtml;
	}
	
	switch (sizeOf($couponArray)){
		
		case 1:
				$coupon = $couponArray[0]; 
				$writer1->writeRow(Array("email"=>trim($user),"coupon1"=>strtoupper($coupon['coupon']),"coupon_expires_on1"=>"Expiring on:".formatTime($coupon['expire']),"Coupon_text1"=>computeValue($coupon),"product_suggestions_html" => $productsHtmlBlock));
				$profilerCount1++ ;
			break;
		case 2:
				
				$coupon = $couponArray[0];
				$coupon1 = $couponArray[1];
				$writer2->writeRow(Array("email"=>trim($user),"coupon1"=>strtoupper($coupon['coupon']),"coupon_expires_on1"=>"Expiring on:".formatTime($coupon['expire']),"Coupon_text1"=>computeValue($coupon),"coupon2"=>strtoupper($coupon1['coupon']),"coupon_expires_on2"=>"Expiring on:".formatTime($coupon1['expire']),"Coupon_text2"=>computeValue($coupon1),"product_suggestions_html" => $productsHtmlBlock));
				$profilerCount2++ ;
				break;
		case 3:
			    
				$coupon = $couponArray[0];
				$coupon1 = $couponArray[1];
				$coupon2 = $couponArray[2];
				$writer3->writeRow(Array("email"=>trim($user),"coupon1"=>strtoupper($coupon['coupon']),"coupon_expires_on1"=>"Expiring on:".formatTime($coupon['expire']),"Coupon_text1"=>computeValue($coupon),"coupon2"=>strtoupper($coupon1['coupon']),"coupon_expires_on2"=>"Expiring on:".formatTime($coupon1['expire']),"Coupon_text2"=>computeValue($coupon1),"coupon3"=>strtoupper($coupon2['coupon']),"coupon_expires_on3"=>"Expiring on:".formatTime($coupon2['expire']),"Coupon_text3"=>computeValue($coupon2),"product_suggestions_html" => $productsHtmlBlock));
				$profilerCount3++ ;
				break;
		default:
				if(sizeOf($couponArray) >3){
					$coupon = $couponArray[0];
					$coupon1 = $couponArray[1];
					$coupon2 = $couponArray[2];
					$writer3->writeRow(Array("email"=>trim($user),"coupon1"=>strtoupper($coupon['coupon']),"coupon_expires_on1"=>"Expiring on:".formatTime($coupon['expire']),"Coupon_text1"=>computeValue($coupon),"coupon2"=>strtoupper($coupon1['coupon']),"coupon_expires_on2"=>"Expiring on:".formatTime($coupon1['expire']),"Coupon_text2"=>computeValue($coupon1),"coupon3"=>strtoupper($coupon2['coupon']),"coupon_expires_on3"=>"Expiring on:".formatTime($coupon2['expire']),"Coupon_text3"=>computeValue($coupon2),"product_suggestions_html" => $productsHtmlBlock));
					$profilerCount3++ ;
				}
				break;
		
	}
	
	$countEmailsSent++;
}
$writer1->writeRow(Array("email"=>"myntramailarchive@gmail.com","coupon1"=>strtoupper($coupon['coupon']),"coupon_expires_on1"=>"Expiring on:".formatTime($coupon['expire']),"Coupon_text1"=>computeValue($coupon),"product_suggestions_html" => $productsHtmlBlock));

$writer2->writeRow(Array("email"=>"myntramailarchive@gmail.com","coupon1"=>strtoupper($coupon['coupon']),"coupon_expires_on1"=>"Expiring on:".formatTime($coupon['expire']),"Coupon_text1"=>computeValue($coupon),"coupon2"=>strtoupper($coupon1['coupon']),"coupon_expires_on2"=>"Expiring on:".formatTime($coupon1['expire']),"Coupon_text2"=>computeValue($coupon1),"product_suggestions_html" => $productsHtmlBlock));

$writer3->writeRow(Array("email"=>"myntramailarchive@gmail.com","coupon1"=>strtoupper($coupon['coupon']),"coupon_expires_on1"=>"Expiring on:".formatTime($coupon['expire']),"Coupon_text1"=>computeValue($coupon),"coupon2"=>strtoupper($coupon1['coupon']),"coupon_expires_on2"=>"Expiring on:".formatTime($coupon1['expire']),"Coupon_text2"=>computeValue($coupon1),"coupon3"=>strtoupper($coupon2['coupon']),"coupon_expires_on3"=>"Expiring on:".formatTime($coupon2['expire']),"Coupon_text3"=>computeValue($coupon2),"product_suggestions_html" => $productsHtmlBlock));

$writer1->closeFile();
$writer2->closeFile();
$writer3->closeFile();

echo "Completed Processing batch " .$batchNumber. "\n";

if($countOfRecords < $batchLimit) break;

$currentOffset = $currentOffset + $batchLimit + 1;
$batchNumber = $batchNumber + 1;

echo "Processing batch " .$batchNumber. "\n";

}

Profiler::increment("205490-coupon1-expiry-mailing-list-csv-ready", $profilerCount1);
Profiler::increment("205491-coupon2-expiry-mailing-list-csv-ready", $profilerCount2);
Profiler::increment("205492-coupon3-expiry-mailing-list-csv-ready", $profilerCount3);

echo __FILE__.": No of email entries made in icubes: ".$countEmailsSent."\n";
echo "CouponExpiryMailer.php: cron finished at ".date("r", time())."\n";
echo "============================end CouponExpiryMailer================================\n";
