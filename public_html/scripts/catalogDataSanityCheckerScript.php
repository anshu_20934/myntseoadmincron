<?php
//error_reporting(E_ERROR);
require_once "../auth.php";
require_once "$xcart_dir/include/class/class.mail.amazonsesmailer.php";
require_once("$xcart_dir/include/solr/solrProducts.php");
 
class Style{
	public $styleId,$styleType;
	public $productType,$isActivePT;
	public $description,$styleNote,$productDisplayName;
	public $defaultImage,$searchImage,$frontImage,$backImage,$rightImage,$leftImage,$topImage,$bottomImage;
	public $brand, $agegroup, $gender, $basecolor, $color1, $color2, $fashionType, $season, $year, $usage, $MRP;
	public $articleTypeId,$isActiveAT;
	public $sizeRepresentationUrl,$sizeChart;
	public $inventoryCount = 0;//this is fetched from solr
	/**this is array of objects belonging to class ProductOptions*/
	public $styleProductOptions = array();
	/**this is a map with key as attributeTypeId and value as array of attributeValue (array of attributeValues is kept instead 
	 * of single value to track if style is wrongly mapped to two values of same attribute type*/
	public $articleTypeAttributeToAttributeValueMap = array();		
}

class ProductOptions{
	public $id;
	public $style;
	public $isActive;
	public $unifiedSizeId;
	public $unifiedSize;
	public $sizeRepresentation;
}

class StyleArticleTypeAttributeValue{
	public $attributeTypeId;
	public $attributeValueId;
	public $styleId;
}

class ArticleType{
	public $id;
	public $typeName;
	public $isActive;
	/**This is a map with key as attributeTypeId and value as object of class ArticleTypeAttribute*/
	public $attributeTypes = array();
}

class ArticleTypeAttribute{
	public $id;
	public $isActive;
	public $attributeType;
	public $catalogueClassificationiId;
	/**This is a map with key as attributeValueId and value as object of class ArticleTypeAttributeValue*/
	public $attributeValues = array();
}

class ArticleTypeAttributeValue{
	public $id;
	public $isActive;
	public $attributeValue;
	public $attributeTypeId;
}

class CatalogSanityChecker{
	private $minDescriptionLength;
	private $numOfAcceptableMissingImages;
	private $sendMailTo;
	private $sendMailFrom;
	private $sendMailSubject;
	
	private $host;
	private $username;
	private $password;
	private $dbname;
	private $con;
	private $wmscon;
	
	private   $tb_productType = "mk_product_type";
	private $tb_stylesProperties = "mk_style_properites";
	
	//below variables are used to store csv of styles having issues
	private $stylesWithNoDescriptionHeading = "List of active styles for which description is missing : ";
	private $stylesWithNoDescription = "";
	
	private $stylesWithInsufficientDescriptionHeading;
	private $stylesWithInsufficientDescription = "";
	
	private $stylesWithNoStyleNoteHeading = "List of active styles for which Style Note is missing : ";
	private $stylesWithNoStyleNote = "";
	
	private $stylesWithNoProductDisplayNameHeading = "List of active styles for which Product Discription is missing : ";
	private $stylesWithNoProductDisplayName = "";
	
	private $stylesNotUnifiedHeading = "List of active styles which are not unified : ";
	private $stylesNotUnified = "";
	
	private $stylesShowsUnifiedButAreNotHeading = "List of active styles which shows unified but are not : ";
	private $stylesShowsUnifiedButAreNot = "";
	
	private $stylesWithNoSizeChartHeading = "List of active styles with no size chart (dynamic or static) : ";
	private $stylesWithNoSizeChart = "";
	
	private $stylesWithNoDefaultImageHeading = "List of active styles with missing default image : ";
	private $stylesWithNoDefaultImage = "";
	
	private $stylesWithInsufficientImagesHeading = "List of active styles for which insufficient images are present : ";
	private $stylesWithInsufficientImages = "";
	
	private $stylesWithInactiveArticleTypeHeading = "List of active styles for which article type is inactive : ";
	private $stylesWithInactiveArticleType = "";
	
	private $stylesWithInactiveProductTypeHeading = "List of active styles for which product type is inactive : ";
	private $stylesWithInactiveProductType = "";
	
	private $stylesWithMissingGlobalAttributesHeading = "List of active styles for which some of the global attributes (brand, agegroup, gender, basecolor, color1, color2, fashionType, season, year, usage, MRP) are missing : ";
	private $stylesWithMissingGlobalAttributes = "";
	
	private $stylesWithZeroProductTypeIdHeading = "List of Styles for which product type id is absent or is 0 : ";
	private $stylesWithZeroProductTypeId = "";
	
	private $stylesWithMissingArticleTypeAttributeHeading = "List of active styles for which some of article type attributes are missing : ";
	private $stylesWithMissingArticleTypeAttribute = "";
	
	private $stylesWithExtraArticleTypeAttributeHeading = "List of active styles for which extra article type attributes are present : ";
	private $stylesWithExtraArticleTypeAttribute = "";
	
	private $stylesWithWrongArticleTypeAttributeValueHeading = "List of active styles for which article type attribute value is wrong : ";
	private $stylesWithWrongArticleTypeAttributeValue = "";
	
	private $stylesWithMultipleValueForArticleTypeAttributeHeading = "List of active styles for which multiple values are present for some article type attributes : ";
	private $stylesWithMultipleValueForArticleTypeAttribute = "";
	
	private $productTypeWithProductGroupIdZeroHeading = "List of productTypes for which product group id is zero : ";
	private $productTypeWithProductGroupIdZero = "";
	
	private $bodyOfMail = "";
	
	public function __construct($host,$username,$password,$dbname){
		$this->host = $host;
		$this->username = $username;
		$this->password = $password;
		$this->dbname = $dbname;

		$this->minDescriptionLength = CatalogueConfig::$sanityScriptDescriptionMinLength;
		$this->numOfAcceptableMissingImages = CatalogueConfig::$sanityScriptNumAcceptableMissingImages;
		$this->sendMailSubject = CatalogueConfig::$sanityScriptMailSubject;
		$this->sendMailTo = EmailListsConfig::$sanityScriptSendMailTo;
		$this->sendMailFrom = EmailListsConfig::$sanityScriptSendMailFrom;
		
		$this->stylesWithInsufficientDescriptionHeading = "List of active styles for which description is not of sufficient length (sufficient length is : " . $this->minDescriptionLength . ") : ";
		
	}
	
	/**Creates connection to database*/
	private function setUpConnection(){
		try {
			$dsn = 'mysql:dbname='.$this->dbname.';host='.$this->host;
			$this->con = new PDO($dsn,$this->username,$this->password);
			$this->con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			return true;
		}catch (PDOException $e){
			echo $e->getMessage() ."\n";
			return false;
		}
	}
	
	/**This function creates csv of style ids using database query result*/
	private function extractStyleIds($result){
		if(!empty($result)){
			$styleIdsArray = array();
			foreach ($result as $row){
				array_push($styleIdsArray,$row["id"]);
			}
			$styleIdsCSV = implode(",",$styleIdsArray);
			return $styleIdsCSV;
		}
	}
	
	/**This array object stores data corresponding to styles, where key is style id and value is style Object*/
	private $stylesMap = array();
	
	/**Calls database to fetch style related data and fills $stylesMap array. Iterate over all styles found to check data sanitization*/
	private function processStylesAndReportUnsanitizedStyles(){
		$range = 200;
		$start = -$range;
		$count = 0;
		//$startMem= memory_get_usage(); 
		while (true){
			$start = $start + $range;
			$query = "SELECT ps.id,ps.product_type,pt.is_active as isActivePT, cc.is_active as isActiveAT,
			sp.global_attr_brand, sp.global_attr_age_group, sp.global_attr_gender, sp.global_attr_base_colour, 
			sp.global_attr_colour1, sp.global_attr_colour2, sp.global_attr_fashion_type, sp.global_attr_season, 
			sp.global_attr_year, sp.global_attr_usage,
			sp.global_attr_article_type, ps.styleType,ps.description,sp.style_note, sp.product_display_name, sp.default_image,
			sp.search_image,sp.front_image,	sp.back_image, sp.right_image,sp.left_image,
			sp.top_image,sp.bottom_image, sp.size_representation_image_url, ps.size_chart_image as sizeChart from 
			mk_style_properties sp inner join mk_product_style ps on ps.id = sp.style_id  
			left join mk_catalogue_classification cc on cc.id = sp.global_attr_article_type 
			left outer join mk_product_type pt on pt.id = ps.product_type
			order by ps.id desc limit $start,$range";
			$result = $this->executeQuery($query);
			//echo "max & min : " . $result[0]["id"] . " & " . $result[count($result)-1]["id"] . "\n";
			//echo "\nsize of res : " . count($result) . "\n";
		
			if(empty($result)){
				echo "\nChecking styles for sanitiztion completed\n";
				break;
			}
			
			$this->fillstylesMapData($result);
			$styleIdsCSV = $this->extractStyleIds($result);
			$this->populateATypeAttValuesDataInStyle($styleIdsCSV);
            $this->fillProductOptionsDataInStyle($styleIdsCSV);
            $this->fillInventoryCountFromSolr($result);
			$this->checkStyleForSanity();
			$count = $count + 1;
			echo "\nSanity checked for $count styles.  \n";
			//echo("\n" . count($this->stylesMap));
			$this->unsetStyleObjectMap();
			//echo "\nMem Usage : " . (memory_get_usage() - $startMem);
			//echo("\n" . count($this->stylesMap));
			//echo "size of stylesMap : " . count($this->stylesMap) . "\n";
		}
		$this->sendMail();
	}

	private function unsetStyleObjectMap(){
		if(!empty($this->stylesMap)){
			foreach ($this->stylesMap as $key => $styleObject) {
				unset($this->stylesMap[$key]);
			}
		}
		/*foreach ($this->stylesMap as $key => $styleObject) {
			unset($styleObject->styleProductOptions);
			foreach ($styleObject->articleTypeAttributeToAttributeValueMap as $attTypeId => $styleATAttValue) {
				unset($styleObject->articleTypeAttributeToAttributeValueMap[$attTypeId]);
			}
			unset($styleObject->articleTypeAttributeToAttributeValueMap);
			unset($this->stylesMap[$key]);
		}
		unset($this->stylesMap);*/
	} 
	
	/**Using result of database query, fills stylesMap object */
	private function fillstylesMapData($result){
		if(!empty($result)){
			foreach ($result as $row){
				$id = $row["id"];
				$styleObject = new Style();
				$styleObject->styleId = $id;
				$styleObject->styleType = $row["styleType"];
				
				$styleObject->description = $row["description"];
				$styleObject->styleNote = $row["style_note"];
				$styleObject->productDisplayName = $row["product_display_name"];
				
				//images
				$styleObject->defaultImage= $row["default_image"];
				$styleObject->searchImage= $row["search_image"];
				$styleObject->frontImage= $row["front_image"];
				$styleObject->backImage= $row["back_image"];
				$styleObject->rightImage= $row["right_image"];
				$styleObject->leftImage= $row["left_image"];
				$styleObject->topImage= $row["top_image"];
				$styleObject->bottomImage = $row["bottom_image"];
				
				//global attributes
				$styleObject->brand = $row["global_attr_brand"];
				$styleObject->agegroup = $row["global_attr_age_group"];
				$styleObject->gender = $row["global_attr_gender"];
				$styleObject->basecolor = $row["global_attr_base_colour"];
				$styleObject->color1 = $row["global_attr_colour1"];
				$styleObject->color2 = $row["global_attr_colour2"];
				$styleObject->fashionType = $row["global_attr_fashion_type"];
				$styleObject->season = $row["global_attr_season"];
				$styleObject->year = $row["global_attr_year"];
				$styleObject->usage = $row["global_attr_usage"];
				
				$styleObject->sizeRepresentationUrl = $row["size_representation_image_url"];
				$styleObject->sizeChart = $row["sizeChart"];
				
				$styleObject->productType = $row["product_type"];
				$styleObject->isActivePT = $row["isActivePT"];
				$styleObject->articleTypeId = $row["global_attr_article_type"];
				$styleObject->isActiveAT = $row["isActiveAT"];
				
				$this->stylesMap[$id] = $styleObject;
			}
		}
	}
	
	/**This function fills styleObject with it's article type attribute value*/
	private function populateATypeAttValuesDataInStyle($styleIdsCSV){
		if(!empty($styleIdsCSV)){
			$query = "select sav.id,sav.product_style_id as styleId, sav.article_type_attribute_value_id as 
			attValueId,atv.attribute_type_id as attTypeId 
			from mk_style_article_type_attribute_values sav 
			left outer join mk_attribute_type_values atv on atv.id = sav.article_type_attribute_value_id
			where sav.product_style_id in ($styleIdsCSV)";
	
			$result = $this->executeQuery($query);
			
			$this->fillATypeAttValuesDataInStyle($result);
		}
	}
	//This function reads each row and fills attributeType to attributeValue Map of style object
	private function fillATypeAttValuesDataInStyle($result){
		if(!empty($result)){
			foreach ($result as $row){
				$styleId = $row["styleId"];
				$styleObject;
				//Ideally below case should not occur as we are already filling styleObject before calling this function
				if(empty($this->stylesMap[$styleId])){
					echo "Style with id $styleId not yet created. Ideally it should have already been created before filling article type attribute values map";
					$styleObject = new Style();
				}else{
					$styleObject = $this->stylesMap[$styleId];
				}
				$attTypeId = $row["attTypeId"]; 
				$attributeTypeToAttributeValueMap = $styleObject->articleTypeAttributeToAttributeValueMap;
				if(empty($attributeTypeToAttributeValueMap[$attTypeId])){//
					$attValueArray = array();
				}else{// "Style id $styleId has more than one attribute value for attribute type Id $attTypeId";
					$attValueArray = $attributeTypeToAttributeValueMap[$attTypeId];
				}
					$styleATAttributeValueObject = new StyleArticleTypeAttributeValue();
					$styleATAttributeValueObject->attributeTypeId = $attTypeId; 
					$styleATAttributeValueObject->attributeValueId = $row["attValueId"];; 
					$styleATAttributeValueObject->styleId = $styleId;
					array_push($attValueArray,$styleATAttributeValueObject);
					$attributeTypeToAttributeValueMap[$attTypeId] = $attValueArray; 
				
				//putting back variables which are changed
				$styleObject->articleTypeAttributeToAttributeValueMap = $attributeTypeToAttributeValueMap;
				$this->stylesMap[$styleId] = $styleObject;
			}
		}
	}
	
		/**For all styles in styleIdsCSV, fetches product options from db and fill product option array in styleObject for each of the styles*/
        private function fillProductOptionsDataInStyle($styleIdsCSV){
            $query="select po.id,po.style, po.name, po.value, po.image,po.is_active ,po.unified_size_value_id, po.unifiedSize, po.size_representation
            from mk_product_options po  where po.style in($styleIdsCSV)";

            $result = $this->executeQuery($query);
            if(!empty($result)){
				foreach ($result as $row){
	                    $styleId = $row["style"];
	                    $productOptionObject = new ProductOptions();
	                    $productOptionObject->id                = $row["id"];
	                    $productOptionObject->style             = $styleId;
	                    $productOptionObject->isActive          = $row["is_active"];
	                    $productOptionObject->unifiedSizeId     = $row["unified_size_value_id"];
	                    $productOptionObject->unifiedSize       = $row["unifiedSize"];
	                    $productOptionObject->sizeRepresentation= $row["size_representation"];

	                    $styleObject;
						if(empty($this->stylesMap[$styleId])){
							echo "Style with id $styleId not yet created. Ideally it should have already been created before filling product options";
							$styleObject = new Style();
						}else{
							$styleObject = $this->stylesMap[$styleId];
						}
	                    
	                   
	                    $poArray= $styleObject->styleProductOptions;
	                    array_push($poArray, $productOptionObject);
	                    $styleObject->styleProductOptions   = $poArray ;
	                    $this->stylesMap[$styleId]          = $styleObject;
	                }
            }
        }
	
        private function fillInventoryCountFromSolr($result){
        	//assuming results are sorted in asc order
        	if(!empty($result)){
        		$maxStyleId = $result[0]["id"];
        		$minStyleId = $result[count($result)-1]["id"];
        		$solrDocs = $this->getSolrDataWithInventory($minStyleId,$maxStyleId);
        		//echo "\nsor results : \n";
        		//var_dump($solrDocs);
        		if(!empty($solrDocs)){
	        		foreach ($solrDocs as $eachDoc) {
	        			$styleId = $eachDoc->styleid;
	        			$invCount = $eachDoc->count_options_availbale;
	        			if(!empty($styleId) && !empty($this->stylesMap[$styleId])){
	        				$styleObject = $this->stylesMap[$styleId];
	        				$styleObject->inventoryCount = $invCount;
	        				$this->stylesMap[$styleId] = $styleObject;
	        				//echo $styleObject->styleId . " - " . $styleObject->inventoryCount . "\n";
	        			}
        		}
        		}else{
        			echo "\nEmpty solr docs\n";
        		}
        	}
        }
        
        /**This function returns documents from solr search. Docs will have only fields specified in the fieldList*/
        private function getSolrDataWithInventory($minStyleId,$maxStyleId){
        		$solrProducts = new SolrProducts();
        		$solrQuery = "(styleid_int:[$minStyleId TO $maxStyleId])AND(count_options_availbale:[1 TO *])";
        		echo "Solr Query is : " . $solrQuery;
        		$facetsArr = array();
        		$fieldList = "styleid,count_options_availbale";
        		
        		/*array_push($facetsArr,"styleid");
        		array_push($facetsArr,"count_options_availbale");*/
        		//$solrResult = $solrProducts->multipleFacetedSearch($solrQuery,$facetsArr);
        		$solrResult = $solrProducts->fieldListSearch($solrQuery,$fieldList,0,$maxStyleId-$minStyleId+1);
        		//$solrResult = $solrProducts->facetedSearch($solrQuery,"styleid");
        		return $solrResult;
        }
        
        /*private $skuIdToSKUObjectMap = array();
        
        private function fillInventoryCount($styleIdsCSV){
        	$result = getSkuIdFromStyle($styleIdsCSV);
        	$skuIds = fillSkuObjectAndGetSkuIdCSV($result); 
        }
        
        private function getSkuIdFromStyle($styleIdsCSV){
        	$query="select style_id,sku_id from mk_styles_options_skus_mapping where style_id in ($styleIdsCSV)";
            $result = $this->executeQuery($query);
            return $result;
        }*/
        
        /**This function finds skuids corresponding to each style id and fills skuIdToSKUObjectMap by filling skudId and styleId. It returns csv of all skuids which will be used to fetch inventory count*/
        /*private function fillSkuObjectAndGetSkuId($result){
        	if(!empty($result)){
        		$skuIdsArray = array();
        		foreach ($result as $row) {
        			$styleId = $row["style_id"];
        			$skuId = $row["sku_id"];
        			
        			$skuObject = new SKUStyleInvCount();
        			$skuObject->skuId = $skuId;
        			$skuObject->styleId = $styleId;
        			$this->skuIdToSKUObjectMap[$skuId] = $skuObject;			
        			array_push($skuIdsArray,$skuId);
        		}
        		$skuIdCSV = implode(",",$skuIdsArray);
        		return $skuIdCSV;
        	}else{
        		return null;
        	}
        }
        private function getSKUInventoryCount($skuIdsCSV){
        	if()
        	$query = "select sku_id,inv_count from core_inventory_counts where  quality = 'Q1' and sku_id in $skuIdsCSV";
        	$result = $this->executeWMSQuery($query);
        	return result;
        }
        */
        
    /**This function iterates over stylesMap array and for each styleObject checks for sanity and depending upon sanity issue, adds that style to corresponding list of styles*/
	private function checkStyleForSanity(){
		if(!empty($this->stylesMap)){
			foreach ($this->stylesMap as $styleId => $styleObject) {
				if($styleObject->styleType == "P" && $styleObject->inventoryCount>0){//for active styles with non zero inventory, perform below checks
					//Only if it is required to check for unification, perform the checks, else don't
					if($this->shouldCheckForUnification($styleObject)){
						if($this->isStyleNotUnified($styleObject)){
							$this->stylesNotUnified .= $styleId . ", ";
						}
						if($this->isSizeNotUnifiedButShowingUnified($styleObject)){
							$this->stylesShowsUnifiedButAreNot .= $styleId . ", ";
						}
						if($this->noSizeChartPresent($styleObject)){
							$this->stylesWithNoSizeChart .= $styleId . ", ";
						}
					}
					if($this->descriptionNotPresent($styleObject)){
						$this->stylesWithNoDescription .= $styleId . ", ";
					}
					if($this->descriptionNotOfSpecifiedLength($styleObject)){
						$this->stylesWithInsufficientDescription .= $styleId . ", ";
					}
					if($this->styleNoteNotPresent($styleObject)){
						$this->stylesWithNoStyleNote .= $styleId . ", ";
					}
					if($this->productDisplayNameNotPresent($styleObject)){
						$this->stylesWithNoProductDisplayName .= $styleId . ", ";
					}
					
					if($this->insufficientImagesPresent($styleObject)){
						$this->stylesWithInsufficientImages .= $styleId . ", ";
					}
					if($this->isProductTypeNotActive($styleObject)){
						$this->stylesWithInactiveProductType .= $styleId . ", ";
					}
					if($this->isArticleTypeNotActive($styleObject)){
						$this->stylesWithInactiveArticleType .= $styleId . ", ";
					}
					if($this->globalAttributesNotPresent($styleObject)){
						$this->stylesWithMissingGlobalAttributes .= $styleId . ", ";
					}
					
					if($this->isMultipleAttributeValuePresent($styleObject)){
						$this->stylesWithMultipleValueForArticleTypeAttribute .= $styleId . ", "; 
					}
					if($this->isWrongAttributeValue($styleObject)){
						$this->stylesWithWrongArticleTypeAttributeValue .= $styleId . ", ";
					}
					if($this->isExtraAttributePresent($styleObject)){
						$this->stylesWithExtraArticleTypeAttribute .= $styleId . ", ";
					}
					if($this->isArticleTypeAttributeMissingForStyle($styleObject)){
						$this->stylesWithMissingArticleTypeAttribute .= $styleId . ", ";
					}
					//making this check only for Active styles now
					if($this->isProductTypeIdZero($styleObject)){
						$this->stylesWithZeroProductTypeId .= $styleId . ", ";
					}
				}//above checks were for active style only
				
				if($styleObject->styleType == "P"){//below check is for all active styles (irrespective of inventory)
					if($this->defaultImageNotPresent($styleObject)){
						$this->stylesWithNoDefaultImage .= $styleId . ", ";
					}
				}

				//below checks are for all styles (any status)
							
			}//for loop ends
			
			//This finds all product types for which productGroup id is 0
			$this->productTypeWithProductGroupIdZero = $this->findProductTypeWithProductGroupIdZero();
		}
	}
	
	/**Below functions are for validating individual fields of a style*/
	
	/**Checks if description is empty or not. This check should be used only for active styles*/
	private function descriptionNotPresent($styleObject){
		if(empty($styleObject->description)){
			return true;
		}
		return false;
	}
	
	/**Checks if description is less than specified size*/
	private function descriptionNotOfSpecifiedLength($styleObject){
		if(!empty($styleObject->description)){
			if(strlen($styleObject->description) < $this->minDescriptionLength){
				return true;
			}
		}
		//for empty description, error will be caught in different function
		return false;
	}
	
	/**Checks if style note is empty or not*/
	private function styleNoteNotPresent($styleObject){
		if(empty($styleObject->styleNote)){
			return true;
		}
		return false;
	}
	
	private function productDisplayNameNotPresent($styleObject){
		if(empty($styleObject->productDisplayName)){
			return true;
		}
		return false;
	}
	
	/**Checks if size representation url is present for this style.*/
	private function isStyleNotUnified($styleObject){
		$isExcludedAT = in_array($styleObject->articleTypeId,$this->articleTypesWithNoSizeChart);
		if(!$isExcludedAT && empty($styleObject->sizeRepresentationUrl)){
			return true;
		}else{
			return false;
		}
	}
	
	/**Checks is size representation url is present but one of the product option is not unified*/
	private function isSizeNotUnifiedButShowingUnified($styleObject){
		if(!empty($styleObject->sizeRepresentationUrl)){
			$styleOptions = $styleObject->styleProductOptions;
			if(!empty($styleOptions)){
				$allOptionsUnified = true;
				foreach ($styleOptions as $optionObject){
					if(empty($optionObject->unifiedSizeId) ||
						empty($optionObject->unifiedSize) ||
						empty($optionObject->sizeRepresentation)){
							$allOptionsUnified = false;
							break;
						}
				}
				if(!$allOptionsUnified){
					return true;
				}
			}
		}else{//if size representation url not present, it will be captured in other function
		}
		return false;;
	}
	
	/**
	 * Should we even care about unification of this style
	 * This internally checks: if it is a single size style 
	 * @param unknown_type $styleObject
	 */
	private function shouldCheckForUnification($styleObject){
		$sizes = $styleObject->styleProductOptions;
		if(!isset($sizes)||!is_array($sizes)||count($sizes)<2){
			return false;
		}
		return true;
	}
	
	/**
	 * Checks if both dynamic size chart and static size chart is absent. This check should be used only for active styles
	 * @param unknown_type $styleObject
	 * @return boolean
	 */
	private function noSizeChartPresent($styleObject){
		$isExcludedAT = in_array($styleObject->articleTypeId,$this->articleTypesWithNoSizeChart);
		if(!$isExcludedAT && empty($styleObject->sizeRepresentationUrl) && empty($styleObject->sizeChart)){
			return true;
		}
		return false;
	}
	
	private function defaultImageNotPresent($styleObject){
		if(empty($styleObject->defaultImage)){
			return true;
		}
		return false;
	}
	
	private function insufficientImagesPresent($styleObject){
		$numOfImagesMissing = 0;
		//missing default image will be taken care by other function. This only checks for number of missing images
		if(empty($styleObject->searchImage)){
			$numOfImagesMissing += 1;
		}
		if(empty($styleObject->frontImage)){
			$numOfImagesMissing += 1;
		}
		if(empty($styleObject->backImage)){
			$numOfImagesMissing += 1;
		}
		if(empty($styleObject->rightImage)){
			$numOfImagesMissing += 1;
		}
		if(empty($styleObject->leftImage)){
			$numOfImagesMissing += 1;
		}
		if(empty($styleObject->topImage)){
			$numOfImagesMissing += 1;
		}
		if(empty($styleObject->bottomImage)){
			$numOfImagesMissing += 1;
		}
		if($numOfImagesMissing >= $this->numOfAcceptableMissingImages){
			return true;	
		}
		return false;
	}
	
	/**This function checks if product type for given style is inactive*/
	private function isProductTypeNotActive($styleObject){
		//empty function checks if variable doesn't exists or is false
		if(empty($styleObject->isActivePT)){
			return true;
		}
		return false;
	}
	
	/**This function checks if article type for given style is inactive*/
	private function isArticleTypeNotActive($styleObject){
		if(empty($styleObject->isActiveAT)){
			return true;
		}
		return false;
	}
	
	/**Checks if any of the global attributes (namely brand, agegroup, gender, basecolor, color1, color2, fashionType, season, year, usage, MRP) is missing */
	private function globalAttributesNotPresent($styleObject){
		//if any of the global attribute is null, return true
		if(empty($styleObject->brand)){
			return true;
		}
		if(empty($styleObject->agegroup)){
			return true;
		}
		if(empty($styleObject->gender)){
			return true;
		}
		if(empty($styleObject->basecolor)){
			return true;
		}
		if(empty($styleObject->color1)){
			return true;
		}
		if(empty($styleObject->color2)){
			return true;
		}
		if(empty($styleObject->fashionType)){
			return true;
		}
		if(empty($styleObject->season)){
			return true;
		}
		if(empty($styleObject->year)){
			return true;
		}
		if(empty($styleObject->usage)){
			return true;
		}
		return false;
	}
	
	/**This function returns true if productType id not present or is 0*/
	private function isProductTypeIdZero($styleObject){
		if(empty($styleObject->productType) || ($styleObject->productType == 0)){
			return true;
		}
		return false;
	}
	
	/**This function checks if style does not have values corresponding to some of the attributes of articleType*/
	private function isArticleTypeAttributeMissingForStyle($styleObject){
		$articleType = $styleObject->articleTypeId;
		$styleAttToValueMap = $styleObject->articleTypeAttributeToAttributeValueMap;
		if(!empty($articleType)){
			if(!empty($this->articleTypeData[$articleType])){//if articleType is present
				$articleTypeObject = $this->articleTypeData[$articleType];
				$attTypeMap = $articleTypeObject->attributeTypes;
				if(!empty($attTypeMap)){//attributes are present for this article type
					if(empty($styleAttToValueMap)){//if style does not have any attribute value 
						return true;
					}
					foreach ($attTypeMap as $key => $value) {//iterate over each attribute id and check if style has that attribute
						if(empty($styleAttToValueMap[$key])){//if style does not have value corresponding to attribute type, return true				
							return true;
						}
					}
				}
			}else{}//ignore if article type not present, it will be captured in isActiveArticleType function
		}
		return false;
	}
	
	/**This function checks if style have extra attributes values other then the list of attributes of articleType*/
	private function isExtraAttributePresent($styleObject){
		$articleType = $styleObject->articleTypeId;
		$styleAttToValueMap = $styleObject->articleTypeAttributeToAttributeValueMap;
		if(!empty($styleAttToValueMap)){//if style has some attribute values
			if(empty($articleType) || empty($this->articleTypeData[$articleType])){//article type not present or it has no attributes
				return true;
			}
			$articleTypeObject = $this->articleTypeData[$articleType];
			if(empty($articleTypeObject->attributeTypes)){//no attributes present for article type but style has some attributes value attached to it
				return true;
			}
			foreach ($styleAttToValueMap as $key => $value) {
				if(empty($articleTypeObject->attributeTypes[$key])){//if attribute type style is mapped to is not valid for its article type
					return true;
				}
			}
		}
		return false;
	}
	
	/**This function checks if style have attributes values which are not in the allowed values for attribute of articleType*/
	private function isWrongAttributeValue($styleObject){
		$styleAttToValueMap = $styleObject->articleTypeAttributeToAttributeValueMap;
		/**article type not present or it has no attributes or style is not mapped to any attribute, ignore it here 
		as it will be captured in different function*/
		if(empty($styleObject->articleTypeId) || empty($this->articleTypeData[$styleObject->articleTypeId]) || empty($styleAttToValueMap)){
			return false;//ignore
		}
		$articleTypeObject = $this->articleTypeData[$styleObject->articleTypeId];
		
		foreach ($styleAttToValueMap as $attTypeId => $attValueObjects) {
			 if(!empty($articleTypeObject->attributeTypes[$attTypeId])){
			 	$attValMap = $articleTypeObject->attributeTypes[$attTypeId]->attributeValues;
			 	foreach ($attValueObjects as $styleAttValueObject) {
					$attValId = $styleAttValueObject->attributeValueId;
					if(empty($attValMap[$attValId])){//if attribute value id mapped to style is not present in possible attribute values for given attribute Type 
						return true;
					}
				} 
			 }else{
			 	
			 }//ignore as it will be captured in different function
		}
		return false;
	}
	
	/**This function checks if style have more than one attribute value for same attribute of article type*/
	private function isMultipleAttributeValuePresent($styleObject){
		$styleAttToValueMap = $styleObject->articleTypeAttributeToAttributeValueMap;
		if(!empty($styleAttToValueMap)){
			foreach ($styleAttToValueMap as $key => $value) {
				if(count($value) > 1){//if attribute value array for style has more than one value corresponding to an attribute
					return true;
				}
			}
		}
		return false;
	}
	
	
	/**Above functions are for vallidating individual fields of a style*/
	
	/**For Global Lookup*/
	/*This variable stores article type ids for which size chart is never added*/
	private $articleTypesWithNoSizeChart = array();

	/*Calls database to get from feature_key_value pair table list of article types for which size chart is never defined*/
	private function populateArticleTypesWithNoSizeChart(){
		$query = "select value from mk_feature_gate_key_value_pairs where `key` = 'cms.ArticleTypesWithNoSizeChart'";
		$result = $this->executeQuery($query);
		if(!empty($result)){
			$allATs = $result[0]["value"];
			$this->articleTypesWithNoSizeChart = explode(",", $allATs);
		}
	}

	/** This variable stores object of type articleType class where key is articleType id and value is this object*/
	private $articleTypeData = array();
	
	/**Calls database to get article type data and fills $articleTypeData array*/
	private function populateArticleTypesData(){
		$range = 500;
		$start = -$range;
		while(true){
			$start = $start + $range;
			$query = "select cc.id as ccId,cc.typename as ccName,cc.is_active as isActiveCC,at.id as atId,
			at.attribute_type as atType,at.is_active as isActiveAttT,atv.id as atvId,
			atv.attribute_value as atValue, atv.is_active as isActiveATV 
			from mk_catalogue_classification cc 
			inner join mk_attribute_type at on at.catalogue_classification_id = cc.id 
			inner join mk_attribute_type_values atv on atv.attribute_type_id = at.id 
			where at.is_active = 1 and atv.is_active = 1
			limit $start,$range";
			$result = $this->executeQuery($query);
			if(empty($result)){
				echo "\nDone AT\n";
				break;
			}
			$this->fillArticleTypesData($result);
		}
	}
	
	/**Using result from the database, it populates $articleTypeData array*/
	private function fillArticleTypesData($result){
		if(!empty($result)){
			foreach ($result as $row) {
				$id = $row["ccId"];
				//if articleTypeData array doesn't contain this article type
				if(empty($this->articleTypeData["$id"])){
					$articleTypeObject = new ArticleType();
					$articleTypeObject->id = $id;
					$articleTypeObject->typeName = $row["ccName"];
					$articleTypeObject->isActive = $row["isActiveCC"];
					
					$attributeTypeObject = $this->fillArticleTypeAttributes($row);
					$attributeTypeArray = array();
					$attributeTypeArray[$attributeTypeObject->id] = $attributeTypeObject;
					
					$articleTypeObject->attributeTypes = $attributeTypeArray;
					$this->articleTypeData[$id] = $articleTypeObject;
					
				}else{ //if article type is already present (attribute type array will also be present), 
					//check if attribute type is present in attribute type array
					$attributeTypeArray = $this->articleTypeData[$id]->attributeTypes;
					$attId = $row["atId"];
					if(empty($attributeTypeArray[$attId])){
						$attributeTypeObject = $this->fillArticleTypeAttributes($row);
						$attributeTypeArray[$attId] = $attributeTypeObject;
						$this->articleTypeData["$id"]->attributeTypes =$attributeTypeArray; 
					}
					else{//if attribute type array present, check if this attribute type value is present 
						$attributeTypeObject = $attributeTypeArray[$attId];
						$attributeValueArray = $attributeTypeObject->attributeValues;
						$attValueId = $row["atvId"];
						if(empty($attributeValueArray[$attValueId])){
							$attributeTypeValueObject = $this->fillArticleTypeAttributeValues($row);
							$attributeValueArray[$attValueId] = $attributeTypeValueObject;
							$attributeTypeObject->attributeValues =  $attributeValueArray;
							$attributeTypeArray[$attId] = $attributeTypeObject;	
							$this->articleTypeData["$id"]->attributeTypes =$attributeTypeArray;
						}else{
						//do nothing as this row is already captured in articleType object
						}
					}
				}
			}
		}
	}
	
	/**extract data from row to create attributeType object*/
	private function fillArticleTypeAttributes($row){
		$attributeTypeObject = new ArticleTypeAttribute();
		$attributeTypeObject->id = $row["atId"];
		$attributeTypeObject->isActive = $row["isActiveAttT"];
		$attributeTypeObject->attributeType = $row["atType"];
		//$attributeTypeObject->catalogueClassificationId
		$attributeTypeValueObject = $this->fillArticleTypeAttributeValues($row);		
		$attributeTypeValueArray = array();
		$attributeTypeValueArray[$attributeTypeValueObject->id] = $attributeTypeValueObject;
		
		$attributeTypeObject->attributeValues = $attributeTypeValueArray;
		
		return $attributeTypeObject;
	}
	
	/**extract data from row to create attributeTypeValue object*/
	private function fillArticleTypeAttributeValues($row){
		$attributeTypeValueObject = new ArticleTypeAttributeValue();
		$attributeTypeValueObject->id = $row["atvId"];
		$attributeTypeValueObject->isActive = $row["isActiveATV"];
		$attributeTypeValueObject->attributeValue = $row["atValue"];
		$attributeTypeValueObject->attributeTypeId = $row["atId"];
		return $attributeTypeValueObject;
	}
	
	/**This functions sends mail*/
	private function sendMail(){
		$this->constructMail();
		if(!empty($this->bodyOfMail)){
			$mailer = new AmazonSESMailer();
			$email_details = array();
			$email_details['to'] = $this->sendMailTo;
			$email_details['subject']= $this->sendMailSubject;
			$email_details['content']= $this->bodyOfMail;
			$email_details['from_email'] = $this->sendMailFrom;
			
			//write to a file and send a mail
			echo "\nSending Mail ..... \n";
			//var_dump($email_details);
			$mailSent = $mailer->sendMail($email_details);
			//$mailSent = mail("paroksh.saxena@myntra.com",'Result of Data Sanitization Script',$this->bodyOfMail);
			if($mailSent){
				echo "\nMail successfully sent\n";
			}else{
				echo "\nMail not sent\n";
			}
		}
		//echo $this->bodyOfMail;
	}
	
	/**This function constructs body of the mail by appending error heading and styles associated with that error*/
	private function constructMail(){
		if(!empty($this->stylesWithNoDescription)){
			$this->bodyOfMail .= $this->constructMailContent($this->stylesWithNoDescriptionHeading,$this->stylesWithNoDescription);
		}
		if(!empty($this->stylesWithInsufficientDescription)){
			$this->bodyOfMail .= $this->constructMailContent($this->stylesWithInsufficientDescriptionHeading,$this->stylesWithInsufficientDescription);
		}
		if(!empty($this->stylesWithNoStyleNote)){
			$this->bodyOfMail .= $this->constructMailContent($this->stylesWithNoStyleNoteHeading,$this->stylesWithNoStyleNote);
		}
		if(!empty($this->stylesWithNoProductDisplayName)){
			$this->bodyOfMail .= $this->constructMailContent($this->stylesWithNoProductDisplayNameHeading,$this->stylesWithNoProductDisplayName);
		}
		if(!empty($this->stylesNotUnified)){
			$this->bodyOfMail .= $this->constructMailContent($this->stylesNotUnifiedHeading,$this->stylesNotUnified);
		}
		if(!empty($this->stylesShowsUnifiedButAreNot)){
			$this->bodyOfMail .= $this->constructMailContent($this->stylesShowsUnifiedButAreNotHeading,$this->stylesShowsUnifiedButAreNot);
		}
		if(!empty($this->stylesWithNoSizeChart)){
			$this->bodyOfMail .= $this->constructMailContent($this->stylesWithNoSizeChartHeading,$this->stylesWithNoSizeChart);
		}
		if(!empty($this->stylesWithNoDefaultImage)){
			$this->bodyOfMail .= $this->constructMailContent($this->stylesWithNoDefaultImageHeading,$this->stylesWithNoDefaultImage);
		}
		//Right now,because of market-place model, there are lots of styles which don't have anything more than default image 
		/*
		if(!empty($this->stylesWithInsufficientImages)){
			$this->bodyOfMail .= $this->constructMailContent($this->stylesWithInsufficientImagesHeading,$this->stylesWithInsufficientImages);
		}*/
		if(!empty($this->stylesWithInactiveArticleType)){
			$this->bodyOfMail .= $this->constructMailContent($this->stylesWithInactiveArticleTypeHeading,$this->stylesWithInactiveArticleType);
		}
		if(!empty($this->stylesWithInactiveProductType)){
			$this->bodyOfMail .= $this->constructMailContent($this->stylesWithInactiveProductTypeHeading,$this->stylesWithInactiveProductType);
		}
		if(!empty($this->stylesWithMissingGlobalAttributes)){
			$this->bodyOfMail .= $this->constructMailContent($this->stylesWithMissingGlobalAttributesHeading,$this->stylesWithMissingGlobalAttributes);
		}
		if(!empty($this->stylesWithZeroProductTypeId)){
			$this->bodyOfMail .= $this->constructMailContent($this->stylesWithZeroProductTypeIdHeading,$this->stylesWithZeroProductTypeId);
		}
		if(!empty($this->productTypeWithProductGroupIdZero)){
			$this->bodyOfMail .= $this->constructMailContent($this->productTypeWithProductGroupIdZeroHeading,$this->productTypeWithProductGroupIdZero);
		}
		if(!empty($this->stylesWithMissingArticleTypeAttribute)){
			$this->bodyOfMail .= $this->constructMailContent($this->stylesWithMissingArticleTypeAttributeHeading,$this->stylesWithMissingArticleTypeAttribute);
		}		
		if(!empty($this->stylesWithMultipleValueForArticleTypeAttribute)){
			$this->bodyOfMail .= $this->constructMailContent($this->stylesWithMultipleValueForArticleTypeAttributeHeading,$this->stylesWithMultipleValueForArticleTypeAttribute);
		}
		if(!empty($this->stylesWithWrongArticleTypeAttributeValue)){
			$this->bodyOfMail .= $this->constructMailContent($this->stylesWithWrongArticleTypeAttributeValueHeading,$this->stylesWithWrongArticleTypeAttributeValue);
		}		
		if(!empty($this->stylesWithExtraArticleTypeAttribute)){
			$this->bodyOfMail .= $this->constructMailContent($this->stylesWithExtraArticleTypeAttributeHeading,$this->stylesWithExtraArticleTypeAttribute);
		}
	}
	
	private function constructMailContent($heading,$styleCSV){
		$res = "<br>" . "<strong>" .$heading . "</strong>" . "<br><br>" . rtrim(rtrim($styleCSV),",") . "<br><br><br>";
		return $res;		
	}
	
	/**This function creates mysql statements and executes it. It returns the result of executing the query*/
	private function executeQuery($queryToExecute){
		try{
			$stmt = $this->con->prepare($queryToExecute);
			$stmt->execute();
			$result = $stmt->fetchAll();
			return $result;
		} 
		catch(PDOException $e){
			echo "Error : ". $e->getMessage()."\n";
			die();
		}
	}
	
	/**This function finds all productType ids for which product_type_groupid, which is foreign key refering to id of table mk_product_group, is 0*/
	private function findProductTypeWithProductGroupIdZero(){
		try{
			$query = "SELECT id,product_type_groupid from mk_product_type where product_type_groupid = 0";
			$result = $this->executeQuery($query);
			if(!empty($result)){
				$message = "";
				foreach ($result as $row){
					$id = $row['id'];
					$message .= $id . ", ";
				}
				return $message;
			}else{
				return "";
			}
		} catch(PDOException $e){
			echo "Error : ". $e->getMessage()."\n";
			die();
		}
	}
	
	/**main code to run script*/
	public function run(){
		$result = $this->setUpConnection();
		if($result){
			echo "Connection established\n";
		}else{
			die("Connection not established");
		}
		$start = memory_get_usage();
		
		$this->populateArticleTypesData();
		$this->populateArticleTypesWithNoSizeChart();
		$this->processStylesAndReportUnsanitizedStyles();
		
		echo "\nTotal Mem Usage : " . (memory_get_usage() - $start);
	}
}

/**This part of the code creates an object of type CatalogSanityChecker by passing db credentials. 
It then calls run() method of that object which fetch style related data from db in batches and performs sanity checks on it and
at the end send a mail listing down all errors and corresponding style ids*/

/**Database credentials*/
$host = DBConfig::$reportsro["host"];
$username = DBConfig::$reportsro["user"];
$password = DBConfig::$reportsro["password"];
$dbname = DBConfig::$reportsro["db"];

$dataSanitizationObject = new CatalogSanityChecker($host,$username,$password,$dbname);
$dataSanitizationObject->run();

?>
