<?php
chdir(dirname(__FILE__));
require "../authAPI.php";
require_once $xcart_dir.'/modules/RestAPI/RestRequest.php';
set_time_limit(0);

$query_1 = "select unm.id as start from user_notification_map unm join user_notification un on unm.user_notification_id = un.id where un.start_date <= date(now()) and un.end_date >= date(now()) order by unm.id asc limit 1";
$q_out = func_query_first($query_1,true);
$start = $q_out['start'];

$query_1 = "select unm.id as end from user_notification_map unm join user_notification un on unm.user_notification_id = un.id where un.start_date <= date(now()) and un.end_date >= date(now()) order by unm.id desc limit 1";
$q_out = func_query_first($query_1,true);
$end = $q_out['end'];


$applicableCount = $end-$start;
$pages = ceil($applicableCount/10000);

echo "================== Total Pages = $pages =========================\n";

for($i=0;$i<$pages;$i++) {
    $start_r = $start + $i*10000;
    $end_r = $start_r+10000;
    $query = "select distinct(user_id) from user_notification_map unm join user_notification un on unm.user_notification_id = un.id where un.start_date <= date(now()) and un.end_date >= date(now()) and unm.id between $start_r and $end_r";
    $users = func_query($query,true);
    $userIds = array();
    $userIdsUnread = array();
    $userIdsTotal = array();
    foreach($users as $usertmp){
        $count_query ="select count(*) as count from user_notification_map unm join user_notification un on unm.user_notification_id = un.id where unm.user_id='".$usertmp['user_id']."' and un.start_date <= date(now()) and un.end_date > date(now()) and `read`=0";
        $count_notificaiton = func_query_first($count_query,true);
        if($count_notificaiton['count'] > 0){
                //set notification count
                if(!empty($usertmp['user_id'])){
                        $userIdsUnread[$usertmp['user_id']] = $count_notificaiton['count'];
                }
        }else{
                $userIds[$usertmp['user_id']] = 0;
        }
        // Now update total count

        $total_count_query ="select count(*) as count from user_notification_map unm join user_notification un on unm.user_notification_id = un.id where unm.user_id='".$usertmp['user_id']."' and un.start_date <= date(now()) and un.end_date > date(now())";
        $total_count_notificaiton = func_query_first($total_count_query,true);
        $userIdsTotal[$usertmp['user_id']]=$total_count_notificaiton['count'];

    }
    echo "================== page=$i =========================\n";
    if(!empty($userIdsUnread)){
            echo "bulk updating for users having unread notificaiton<br>\n";
            bulkIncrementService($userIdsUnread);
    }
    if(!empty($userIds)){
            echo "bulk updating for users already read all the notificaiton<br>\n";
            bulkIncrementService($userIds);
    }
    if(!empty($userIdsTotal)){
            echo "bulk updating for users for total notificaiton<br>\n";
            bulkIncrementService($userIdsTotal,"totalNotificationCounter");
    }
}
function bulkIncrementService($userIds,$totalNotificationCounter){
        $url = \HostConfig::$notificationsUpsUrl.'/putAttr';
        $method = 'POST';
        $data = getBulkIncrementJSON($userIds,$totalNotificationCounter);
        $request = new \RestRequest($url, $method, $data);
        $headers = array();
        $headers[] = 'Accept: application/json';
        $headers[] = 'Content-type: application/json';
        $headers[] = 'Accept-Language: en-US';
        $request->setHttpHeaders($headers);
        $request->execute();
}

function getBulkIncrementJSON($userIds,$totalNotificationCounter){
        $jsonArr = array();
        foreach($userIds as $key=>$value){
                $jsonArr[$key] = array();
                if(empty($totalNotificationCounter)){
                    $jsonArr[$key]['attribute'] = 'notificationCounter';
                }else{
                    $jsonArr[$key]['attribute'] = "$totalNotificationCounter";
                }
                
                $jsonArr[$key]['sourceid'] = '3';
                $jsonArr[$key]['value'] = "$value";
        }
//        print_r($jsonArr);
        return json_encode($jsonArr);
}
?>
