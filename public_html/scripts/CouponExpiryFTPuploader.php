<?php
/*
 * Coupon expiry mailer script to upload mailing list to icubes
 * 
 */

require_once "../admin/auth.php";
require_once $xcart_dir."/include/func/func.core.php";
require_once $xcart_dir."/include/dao/class/mcart/class.MCartDBAdapter.php";
require_once $xcart_dir."/include/class/mcart/class.MCart.php";
require_once $xcart_dir.'/include/class/csvutils/class.csvutils.ArrayCSVWriter.php';
require_once $xcart_dir.'/include/class/class.FTPActions.php';
require_once $xcart_dir.'/include/class/elabsmail/class.elabsmail.mailinglist.php';
include_once($xcart_dir."/Profiler/Profiler.php");

//TODO: add logger support to non-webroot-scripts
echo "==============================begin CouponExpiryMailer==================================\n";
echo "CouponExpiryFTPuploader.php: cron started at ".date("r", time())."\n";

//Note that after including auth.php, all the previously defined variables locally get cleared.
$serverinfo_conf=parse_ini_file("../env/serverinfo.ini",true);
$elabsConfig = $serverinfo_conf["ELABS"];
$CouponExpiryMailConfig = $serverinfo_conf["Coupon Expiry Mail"];

//credentials of icubes ftp server where we can upload our CSV files
$ftphost = empty($elabsConfig["ftp_host"]) ? 'icubes.in' : $elabsConfig["ftp_host"];
$ftpuser = empty($elabsConfig["ftp_user"]) ? 'myntra' : $elabsConfig["ftp_user"];
$ftppasswd = empty($elabsConfig["ftp_password"]) ? 'M!ntra123' : $elabsConfig["ftp_password"];

//email id to which results of CSV upload should be sent
$emailToSendUploadStatus = empty($CouponExpiryMailConfig["email_upload_status"]) ? 'myntramailarchive@gmail.com' : $CouponExpiryMailConfig["email_upload_status"];
//this is the mailing listid in Lyris/Elabs to which the contacts have to be imported finally: It is named Abandoned Cart
$mailingListId[0] = "205490";
$mailingListId[1] = "205491";
$mailingListId[2] = "205492";


$uploader = new FTPActions($ftphost, FALSE);
$uploader->setMode(FTP_BINARY);
$uploader->connect();
echo "Logging in user.. Result: ".$uploader->login($ftpuser, $ftppasswd)."\n";

$dirname = "/coupon_expiry_remainder/";
//don't need old CSV files on FTP server so delete them
$uploader->deleteDirectoryRecursively($dirname);
$uploader->mkdir($dirname);
$uploader->chdir($dirname);


//-------------------------------------------------------------------------------

$time = time();
//from start of the day
$start_stamp = strtotime(date('Y-m-d', time())." +2 day");
//to the end of it
$end_stamp = $start_stamp + 86400;


$today = time();
$campaignDate =  date("Ymd",$today);
$localDirname = "/var/mailer_uploads/coupon_expiry_remainder/";
//delete all CSV files locally inside this directory
$mask = "*.csv";
array_map("unlink",$localDirname.$mask);
//local file which will be created this time
$fileName[0] = glob($localDirname.$campaignDate."coupon1.*.csv");
$fileName[1] = glob($localDirname.$campaignDate."coupon2.*.csv");
$fileName[2] = glob($localDirname.$campaignDate."coupon3.*.csv");

for($i=0;$i<3;$i++)
{
	for($j=0;$j<count($fileName[$i]); $j++) {
		$remoteFile = str_replace($localDirname, $dirname, $fileName[$i][$j]);
		$ftpResult = $uploader->upload($fileName[$i][$j], $remoteFile) ; 	
		echo "Uploading ".$fileName[$i][$j]." to FTP Server with remote path: $remoteFile ; result: ".$ftpResult."\n";
		
		if($ftpResult == 1){
			Profiler::increment($mailingListId[$i] . "-couponexpiry-csv-uploaded-to-FTP-SUCCESS");
		}else{
			Profiler::increment($mailingListId[$i] . "-couponexpiry-csv-uploaded-to-FTP-FAILURE");
		}

		$pathToCsvFile = "ftp://$ftpuser:$ftppasswd@$ftphost/".$remoteFile;
		echo "Importing $pathToCsvFile to : $remoteFile "; 
		$uploadResult = MailingList::uploadContactsToMailingList($mailingListId[$i], $pathToCsvFile, $emailToSendUploadStatus,'|'); 
		print_r($uploadResult);
		
		if($uploadResult["result"] == "success"){
			Profiler::increment($mailingListId[$i] . "-couponexpiry-mails-queued-into-icubes-SUCCESS");
		}else{
			Profiler::increment($mailingListId[$i] . "-couponexpiry-mails-queued-into-icubes-FAILURE");
		}
		sleep(1);
	}	
	
}

$uploader->close();

echo "CouponExpiryFTPuploader.php: cron finished at ".date("r", time())."\n";
echo "============================end CouponExpiryMailer================================\n";
