<?php
//Script to process active products added today
$dirName = dirname(__FILE__);
chdir($dirName);
include_once("../auth.php");
require_once '../env/DB.config.php';
require_once($xcart_dir."/include/func/func.utilities.php");
require_once($xcart_dir."/include/func/func.core.php");
include_once \HostConfig::$documentRoot ."/utils/throttleUpdatesDb.php";
define('ALLOWABLETAGS','<h1><h2><h3><h4><h5><h6><em><strong><br/><b><p><br>');
define('REMOVABLETAGS','a');
define('DBSERVER', DBConfig::$rw['host']);
define('DBUSER', DBConfig::$rw['user']);
define('DBPASS', DBConfig::$rw['password']);
define('DBNAME', DBConfig::$rw['db']);
define('SOLRSERVER', SOLRConfig::$searchhost);
define('LINKLIMIT', 6);
    
function getProductStyleIdsHavingWord($word,$solrConnection){
    $word = str_replace(" ","+", $word);
    $q="(descr:\"".$word."\") AND (add_date:[NOW-1DAY TO NOW])";
    return $solrConnection->fieldListSearch($q, "styleid,descr", 0, 5000);
}

function getAllHandPickedKeywords(){
    $sql = "(SELECT crosslink_keyword as keyword,IFNULL(crosslink_keyword_url, replace(crosslink_keyword,' ','-')) as url FROM mk_crosslink_keyword WHERE is_active='1') union distinct (SELECT keyword,replace(url,'www.myntra.com/','') FROM adseo_sitemapkeyword WHERE pattern != 'b')";
    return func_query($sql, true);
}

function getStyleSpecificExtraPDLinks($styleIds){
    $styleIdsString = implode(",",$styleIds);
    $sql = "Select style_id, link_keywords, target_url, css_class from adseo_pdpextralinks where is_active='1' and style_id in (".$styleIdsString.")";
    return func_query($sql, true);
}

function buildKeywordLinkWordCountHash($keyword,$url="", $isRelativeUrl, $cssClass){
    global $http_location_cli;
    if($isRelativeUrl){
            $pattern = empty($url) && !$isAbsoluteUrl? str_replace(" ", "-", strtolower($keyword)) : $url;
            $url = $http_location_cli."/".$pattern;
    }
    $link = constructAnchorTag($url, $cssClass, $keyword); 
    return array('keyword' => $keyword, 'link'=> $link, 'wc' =>str_word_count($keyword));
}

function constructAnchorTag($targetUrl, $cssClass, $keyword){
    return "<a href='".$targetUrl."?src=pd' class='".$cssClass."'>".$keyword."</a>";
}

function getLinkableFilter(){
    $linkedKeywords = array();

    $HK = getAllHandPickedKeywords();
    if($HK){
        foreach($HK as $index=>$val){
            $ckeyword = $val['keyword'];
            if(!empty($ckeyword)){
                $linkedKeywords[] = buildKeywordLinkWordCountHash($ckeyword,$val['url'],true,"seolink");
            }
        }
    }
    return $linkedKeywords;
}

function sortLinkableKeywords($linkedKeywords){
    $sortedKeywords = array();
    $sortedKeywordsWithWC = quickSortRecursive( $linkedKeywords, $left = 0 , $right = NULL, 'wc' );
    $sortedKeywords = array_reverse($sortedKeywordsWithWC);
    return $sortedKeywords; 
}

function getSortedLinkedFilters(){
    $filters = getLinkableFilter();
    $sortedLinkedKeywords = sortLinkableKeywords($filters);
    return $sortedLinkedKeywords;
}
function createStyleKeywordMap($sortedKeywords,$solrConnection){
    $styleKeywordMap = array();
    $productKeywordMap = array();
    foreach($sortedKeywords as $word){
        $prodids = getProductStyleIdsHavingWord($word['keyword'],$solrConnection);
        foreach($prodids as $prod){
            $prodid = $prod->styleid;
            if(!$productKeywordMap[$prodid]){
                $productKeywordMap[$prodid] = array('descr'=>$prod->descr, 'keywords'=>array());
            }
            if(count($productKeywordMap[$prodid]['keywords']) < 6){
                array_push($productKeywordMap[$prodid]['keywords'],$word);
            }
        }
    }
    return createStyleExtraLinksMap($productKeywordMap);
}

function createStyleExtraLinksMap($productKeywordMap){
    $styleIdsList = array_keys($productKeywordMap);
    $extraLinks = getStyleSpecificExtraPDLinks($styleIdsList);
    foreach($extraLinks as $el){
        $styleId = $el['style_id'];
        $keywordLinkWordCountHash = buildKeywordLinkWordCountHash($el['link_keywords'], $el['target_url'], false, $el['css_class']);
        array_unshift($productKeywordMap[$styleId]['keywords'],$keywordLinkWordCountHash);
    }
    return $productKeywordMap;    
}

function applyCrosslinking($text,$keywords){
    global $replaced,$replacements;
       $replaced = array();    
    $charsToEscape = "/";
    $search = array();
    foreach($keywords as $index=>$word){
    $searchPattern = preg_quote($word['keyword'],$charsToEscape);
        $searchPattern = "/\b(".$searchPattern.")\b(?!(?:(?!<\/?[ha].*?>).)*<\/[ha].*?>)(?![^<>]*>)/i";
        $search[$index] = $searchPattern;
        $replaced[strtolower($word['keyword'])] = $word['link'];
    }
    $replacements = 0;
    $text = preg_replace_callback($search, function($matches) {
        global $replaced, $replacements;
        $returnString = $matches[0];
         if($replacements < LINKLIMIT){
             $replacements++;
             $matched = $replaced[strtolower($matches[0])];
             $returnString =  preg_replace('/>'.$matches[0].'</i','>'.$matches[0].'<',$matched, 1);
         }
         return $returnString;
    }, $text, 1, $count);
    return $text;
}

function makePDPDescriptionCrossLinked($desc,$keywords,$id,$columnName, $tableName){
    $linkableText = stripOnlyTags($desc,REMOVABLETAGS);
    $crosslinkedDescription= applyCrosslinking($linkableText, $keywords);
    $queryData = array($columnName=>$crosslinkedDescription);
    return query_func_array2update($tableName, sanitizeDBValues($queryData),"id='{$id}'");
}

function updateDescriptions($crossLinkedDescriptions){
    $connection = \mysql_connect(DBSERVER, DBUSER, DBPASS);
    mysql_set_charset('utf8',$connection);
    @\mysql_select_db(DBNAME) or die( "Unable to select database");
    $count = 0;
    foreach( $crossLinkedDescriptions as $query ){
        $count++;
        $result = \mysql_query($query,$connection);
        if (!$result) {
            echo $query;
        }
    }
    return $batchDBQuery;
}

function crosslinkProductDescriptions($productKeywordMap,$solrConnection){
    $crossLinkedDescriptions = array();
    foreach($productKeywordMap as $style => $productDetails){
        $desc = $productDetails['descr'];
        $keywords = $productDetails['keywords'];
        $crossLinkedDescriptions[] =
            makePDPDescriptionCrossLinked($desc,$keywords,$style,'description','mk_product_style');
    }
    updateDescriptions($crossLinkedDescriptions);
}

function latestProcessIdsPresent($solrConnection){
     return $solrConnection->searchIndex("add_date:[NOW-1DAY TO NOW]",0,1);
}

var_dump(date("F j, Y, g:i a"));
$start = microtime(true);
$solrConnection = new SolrCommon(SOLRSERVER,'sprod');

$processProdIds = latestProcessIdsPresent($solrConnection);
$time_taken_LPI = microtime(true) - $start;
var_dump("Time for last processed Id : ".$time_taken_LPI);

if($processProdIds){
$sortedKeywords = getSortedLinkedFilters();
$time_taken_sf = microtime(true) - $start;
var_dump("Sorted keyword Count : ".count($sortedKeywords));
var_dump("Time for sorted filters : ".$time_taken_sf);

$time_SKM = microtime(true);
$productKeywordMap = createStyleKeywordMap($sortedKeywords,$solrConnection);
$time_taken_SKM = microtime(true) - $time_SKM;
var_dump("Time for IDKeywordMaps : ".$time_taken_SKM);

$time_CPD = microtime(true);
crosslinkProductDescriptions($productKeywordMap);
$time_taken_CPD = microtime(true) - $time_CPD;
var_dump("Time for crosslinkProductDescriptions : ".$time_taken_CPD);
}
else{ var_dump("no products added today");}

$time_taken = microtime(true) - $start;
var_dump("Total time taken for script : ".$time_taken);
