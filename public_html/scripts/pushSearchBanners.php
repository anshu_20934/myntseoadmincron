<?php

/**
 * Kundan: Bulk Upload Search banners from a CSV file
 */

require_once '../auth.php';
include_once \HostConfig::$documentRoot ."/utils/imagetransferS3.php";
use imageUtils\Jpegmini;
if($_SERVER["argc"] < 2){
	echo "Usage: php pushSearchBanners.php <file>.csv";
	exit;
}

$csvFileForBanners = $_SERVER["argv"][1];
$handle = fopen($csvFileForBanners, "r");
if($handle == FALSE){
	echo "Cannot Open file for reading:".$csvFileForBanners;
	exit;
}
$maches;
$filesInError = array();
$searchBanners = array();
while(!feof($handle)){
	$line = fgets($handle);
	if(!empty($line)){
		list ($keyword, $image1, $image2, $targetUrl1, $targetUrl2) = str_getcsv($line);
		$isValid = preg_match_all("/^[a-zA-Z0-9_-]+$/", $keyword, $matches) && preg_match_all("/^[a-zA-Z0-9_-]+.jpg$/", $image1, $matches) && preg_match_all("/^[a-zA-Z0-9_-]+.jpg$/", $image2, $matches);
		echo "line: keyword: $keyword ; image1:$image1 ; image2:$image2 ; targetUrl1:$targetUrl1 ; targetUrl2:$targetUrl2 totalIsValid: ".$isValid."\n";
		if($isValid){
			$image1CdnUrl = copyAndUploadToCdn($image1);
			$image2CdnUrl = copyAndUploadToCdn($image2);
			if(!empty($image1CdnUrl) && !empty($image2CdnUrl)){
				$bannerInserted = insertSearchBanner($keyword, $image1CdnUrl, $image2CdnUrl,$targetUrl1, $targetUrl2);
				array_push($searchBanners, array("keyword"=> $keyword,"image1"=> $image1,"image2"=> $image2,"image1cdn"=>$image1CdnUrl,"image2cdn"=>$image2CdnUrl, "bannersInserted"=>$bannerInserted, "targetUrl1"=>$targetUrl1, "targetUrl2"=>$targetUrl2));
				continue;
			}
		} 
		array_push($filesInError, array("keyword"=>$keyword, "image1"=> $image1,"image2"=> $image2, "targetUrl1"=>$targetUrl1, "targetUrl2"=>$targetUrl2));
	}
}
fclose($handle);
echo "Files Processed and pushed to CDN:"; print_r($searchBanners);
echo "\n\nFiles in error, not processed:"; print_r($filesInError);

function insertSearchBanner($keyword,$image1url,$image2url,$targetUrl1, $targetUrl2){
	$deleted = true;
	$result = func_select_columns_query("page_config_image_reference","max(sort_order) as max_sort_order",array("image_rule_id"=> 6, "page_location"=> $keyword, "deleted"=> 0));
	$maxSortOrderFetched = $result[0]["max_sort_order"];
	
	if(isset($maxSortOrderFetched) && $maxSortOrderFetched >=0){
		$sortOrderToInsert = $maxSortOrderFetched+1;
		//do a soft delete of this row, and then do a fresh insert
		$where = array("image_rule_id"=> 6, "page_location"=> $keyword, "deleted"=> 0, "sort_order"=> $maxSortOrderFetched);
		$set = array("deleted"=>"1");
		$deleted = func_array2updateWithTypeCheck("page_config_image_reference", $set, $where);
	} else {
		$sortOrderToInsert = 0;
	}
	//Now go ahead and insert a new row
	$fashionBannerContent = "<a href='".$targetUrl1."'><img style='padding-right:0' src='".$image1url."'/></a>
							 <a href='".$targetUrl2."'><img src='".$image2url."'/></a>";
	$insert = array("image_rule_id"=> 6, "page_location"=> $keyword, "width"=>792, "height"=> 180, "sort_order"=>$sortOrderToInsert, "fashion_banners"=>$fashionBannerContent , "image_url"=>"");
	$inserted = func_array2insertWithTypeCheck("page_config_image_reference", $insert);
	return $inserted && $deleted;	
}
function copyAndUploadToCdn($image1){
	global $xcart_dir;
	if(!is_dir("$xcart_dir/images/banners/search/") && !mkdir("$xcart_dir/images/banners/search/",0777,true)){
		die("banners search directory does not exist and also failed to create directory: $xcart_dir/images/banners/search/ . Please create this and make it writable by the user who executes this PHP script");
	}
	$image1path = "images/banners/search/".time()."_".$image1;
	$fileUploadedToCDN = false;
	if(copy("../images/banners/$image1", "../$image1path")){
		if(moveToDefaultS3Bucket($image1path)){
			$fileUploadedToCDN = true;
			try {
				Jpegmini::copyCompressedPlaceholderFile($image1path);
				\myntra\utils\cdn\imagetransferS3::compress($image1path);
			} catch(Exception $e){
				global $errorlog;
				$errorlog->error("Error while trying to compress image: either in JpegMini or in Ampq:".$e);
			}
		}
		if($fileUploadedToCDN){
			$image1CdnUrl = \HostConfig::$cdnBase ."/{$image1path}";
			return url_exists($image1CdnUrl) ? $image1CdnUrl : null;
		}
	}else {
		echo "Failed to copy to directory:$xcart_dir/images/banners/search/";
		print_r(error_get_last());
	}
	return $image1CdnUrl;
}