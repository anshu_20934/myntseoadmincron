<?php
require_once(dirname(__FILE__)."/../env/RemoteTimeouts.Config.php");

chdir(dirname(__FILE__));
include_once("../auth.php");
include_once("../include/func/func.mkcore.php");
set_time_limit(0);

/* fit-team/(teams-leagues)-brand-sports-classification
 *
 */
$query = "select fg.group_name,if((f.filter_url <> ' ' && f.filter_url <> 0 && f.filter_url <> null),replace(f.filter_url,' ','-'),replace(f.filter_name,' ','-')) as filter from mk_filters f, mk_filter_group fg
            where f.filter_group_id = fg.id
            and f.is_Active=1
            and fg.id in (2,3,4,5,6)
            order by fg.group_name";//fg.is_active=1 and
$result = db_query($query);
while($row = db_fetch_array($result)){
    $keywords[strtolower($row['group_name'])][] = $row['filter'];

}

//catalogue items
$items = func_query("select replace(typename,' ','-') as typename from  mk_catalogue_classification where is_active=1");
$domain = 'www.myntra.com';

echo "<pre>";
echo "start count=".$count = 0;

//fit team
/*foreach($keywords['fit'] as $filter){
    foreach($keywords['teams / leagues'] as $leagueFilter ){
        $fitTeamF[] = $filter."###".$leagueFilter;
        $count++;
    }
    foreach($keywords['team'] as $teamFilter ){
        $fitTeamF[] = $filter."###".$teamFilter;
        $count++;
    }

}
unset($filter);*/

//fit team brand
foreach($keywords['fit'] as $filter){
    foreach($keywords['brands'] as $brandFilter ){
        $fitTeamBrandF[] = $filter."###".$brandFilter;
        $count++;
    }
}
unset($filter);

//fit team brand sport
/*foreach($fitTeamBrandF as $filter){
    foreach($keywords['sports'] as $sportsFilter ){
        $fitTeamBrandSportsF[] = $filter."###".$sportsFilter;
        $count++;
    }
}
unset($filter);*/

//fit team brand sport item
foreach($fitTeamBrandF as $filter){
    foreach($items as $itemsFilter ){
        $fitTeamBrandSportsItemF[] = $filter."###".$itemsFilter['typename'];
        //echo "<br/>".$filter."###".$itemsFilter['typename'];
        $count++;
    }
}

$noOfStrings = $count;

echo "\nRemaining Strings::".$noOfStrings;
foreach($fitTeamBrandSportsItemF as $string){    
    $noOfStrings--;
    echo "\nRemaining Strings::".$noOfStrings;
    $keywords = explode('###',$string);
    $countOfKeywords = count($keywords);

    //3c1
    $combinations = 3;//5-3-later move to function
    for($i=0;$i<$combinations;$i++){
        //echo "\n3c1::".$keywords[$i];
        if(!in_array($keywords[$i],$finalFormattedKeywords)){            
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,"http://$domain/".$keywords[$i]);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, \RemoteTimeouts::$remoteConnectTimeout);
			curl_setopt($ch, CURLOPT_TIMEOUT, \RemoteTimeouts::$formattedKeywordsTimeout); 
            
            curl_exec($ch);
            curl_close($ch);
            $finalFormattedKeywords[] = $keywords[$i];
        }           
    }
    
    //3c2
    $combinations = 3;//10-3-later move to function
    $firstPointer = 0;
    $nextPointer = $firstPointer+1;
    $lastPointer = $countOfKeywords-1;
    while($combinations){        
        while($nextPointer <= $lastPointer){
            //echo "\n3c2::".$keywords[$firstPointer]."-".$keywords[$nextPointer];
            if(!in_array($keywords[$firstPointer]."-".$keywords[$nextPointer],$finalFormattedKeywords)){
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL,"http://$domain/".$keywords[$firstPointer]."-".$keywords[$nextPointer]);
                curl_setopt($ch, CURLOPT_HEADER, 0);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                
				curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, \RemoteTimeouts::$remoteConnectTimeout);
				curl_setopt($ch, CURLOPT_TIMEOUT, \RemoteTimeouts::$formattedKeywordsTimeout); 
                
                curl_exec($ch);
                curl_close($ch);
                $finalFormattedKeywords[] = $keywords[$firstPointer]."-".$keywords[$nextPointer];
            }

            $nextPointer++;
            $combinations--;
            //sleep(1);
        }
        $firstPointer++;
        $nextPointer = $firstPointer+1;
    }

    //3c3
    $combinations = 1;//10-1-later move to function
    $firstPointer = 0;
    $nextPointer = $firstPointer+1;
    $nextToNextPointer = $nextPointer+1;
    $lastPointer = $countOfKeywords-1;
    while($combinations){
        while($nextPointer<$lastPointer){
            while($nextToNextPointer <= $lastPointer){
                //echo "\n3c3::".$keywords[$firstPointer]."-".$keywords[$nextPointer]."-".$keywords[$nextToNextPointer];
                if(!in_array($keywords[$firstPointer]."-".$keywords[$nextPointer]."-".$keywords[$nextToNextPointer],$finalFormattedKeywords)){
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL,"http://$domain/".$keywords[$firstPointer]."-".$keywords[$nextPointer]."-".$keywords[$nextToNextPointer]);
                    curl_setopt($ch, CURLOPT_HEADER, 0);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                    
					curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, \RemoteTimeouts::$remoteConnectTimeout);
					curl_setopt($ch, CURLOPT_TIMEOUT, \RemoteTimeouts::$formattedKeywordsTimeout); 
                    
                    curl_exec($ch);
                    curl_close($ch);
                    $finalFormattedKeywords[] = $keywords[$firstPointer]."-".$keywords[$nextPointer]."-".$keywords[$nextToNextPointer];
                }
                $nextToNextPointer++;
                $combinations--;
                //sleep(1);
            }
            $nextPointer++;
            $nextToNextPointer = $nextPointer+1;
        }
        $firstPointer++;
        $nextPointer = $firstPointer+1;
        $nextToNextPointer = $nextPointer+1;
    }
}

echo "\n\nstrings count=".$count;
echo "\n\nkeywords count=".count($finalFormattedKeywords);
