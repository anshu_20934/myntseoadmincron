<?php
 require_once '../auth.php';
 require_once '../include/solr/solrProducts.php';
 use vtr\client\VirtualTrialRoomClient;
 $solrProducts = new SolrProducts();
 //Step:1 fetch the styles which are in solr as vtr-enabled
 reindexVtrStylesAlreadyInSolr($solrProducts);
 
 //Step:2 index all vtr-enabled styles 
 indexAllVtrEnabledStyles($solrProducts);
 
 $solrProducts->commitToAllServers();
 
 function reindexVtrStylesAlreadyInSolr($solrProducts){
 	$vtrEnabledStylesInSolr = getVtrEnabledStylesFromSolr($solrProducts);
 	echo "Reindexing existing styles on solr which were VTR enabled:\n";
 	foreach ($vtrEnabledStylesInSolr as $eachStyle){
 		echo $eachStyle." ";
 		$solrProducts->deleteStyleFromIndex($eachStyle);
 		$solrProducts->addStyleToIndex($eachStyle,true,false);
 	}	
 }
 
 function indexAllVtrEnabledStyles($solrProducts){
 	echo "Indexing all vtr-enabled styles on solr:\n";
 	$vtrDetails = VirtualTrialRoomClient::getStyleDataForIndexing($styleIdArrayToIndex);
 	if(!empty($vtrDetails)){
 		indexVtrEnabledStyles($vtrDetails,$solrProducts);
 	}
 	else {
 		echo "Failed: Vtr Service Client didn't respond";
 	}
 }
 
 function indexVtrEnabledStyles($vtrDetails,$solrProducts){
 	foreach ($vtrDetails as $eachStyle => $vtrStyleDetails){
 		echo $eachStyle." ";
 		$solrProducts->addStyleToIndex($eachStyle,true,false,false,null,array($eachStyle => $vtrStyleDetails));
 	}
 }
 
 function getVtrEnabledStylesFromSolr($solrProducts){
 	$query = "(torsos:1)OR(torsos:2)";
 	$search_results = $solrProducts->searchAndSort($query,0,10000);
 	$products = getSingleFieldInDoc($search_results->docs,"styleid");
 	//$products_count = $search_results->numFound;
 	return $products;
 }
 
 function getSingleFieldInDoc($documents,$field){
 	$documents_array = array();
 	foreach($documents as $document) {
 		array_push($documents_array, $document->__get($field));
 	}
 	return $documents_array;
 }
 ?>
