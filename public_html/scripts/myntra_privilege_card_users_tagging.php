<?php
chdir(dirname(__FILE__));
include_once("../auth_for_solr_index.php");
//$ft = fopen("privilegeUsers.csv","r");

$filepath = $_SERVER["argv"][1];
$activity = $_SERVER["argv"][2];
$ft = fopen($filepath,"r");

x_load('db');

// parse every line of the csv file
// line format: login,couponCode
$customers_email = array();
while(!feof($ft))	{
	$line = fgets($ft);
	if ($line != "") {
		$array = explode(",", trim($line));
		//$array[0] = mysql_real_escape_string($array[0]);
		//$array[1] = mysql_real_escape_string($array[1]);
		if(!filter_var($array[0], FILTER_VALIDATE_EMAIL)){
			echo "wrong email address ".$array[0];
			exit();
		}
		$customers_email[$array[0]] = $array[1];
	}
}
fclose($ft);

if($activity == "user_tagging"){
	//mysql_query("BEGIN");
	foreach($customers_email as $login=>$couponCode) {
		//$data['showInMyMyntra'] = 1;
		$data['users'] = $login;
		func_array2updateWithTypeCheck("xcart_discount_coupons",$data,"coupon='$couponCode'");
	}
	//mysql_query("COMMIT");
	
	echo "Ran user tagging for myntra privilege card on ". date("F j, Y, g:i a", time())."<br>";
	echo "No of users tagged: ".count($customers_email)."\n";
}
elseif($activity == "coupon_activation"){
	//mysql_query("BEGIN");
	foreach($customers_email as $login=>$couponCode) {
		//$data['showInMyMyntra'] = 1;
		$data['status'] = 'A';
		func_array2updateWithTypeCheck("xcart_discount_coupons",$data,"coupon='$couponCode'");
	}
	//mysql_query("COMMIT");
	
	echo "Ran coupon activation for myntra privilege card on ". date("F j, Y, g:i a", time())."<br>";
	echo "No of coupons activated: ".count($customers_email)."\n";
}
elseif($activity == "coupon_deactivation"){
	//mysql_query("BEGIN");
	foreach($customers_email as $login=>$couponCode) {
		//$data['showInMyMyntra'] = 1;
		$data['status'] = 'D';
		func_array2updateWithTypeCheck("xcart_discount_coupons",$data,"coupon='$couponCode'");
	}
	//mysql_query("COMMIT");
	
	echo "Ran coupon deactivation for myntra privilege card on ". date("F j, Y, g:i a", time())."<br>";
	echo "No of coupons deactivated: ".count($customers_email)."\n";
}
?>