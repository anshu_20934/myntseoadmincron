#!/bin/sh
hostnames=("192.168.70.11" "192.168.70.13" "192.168.70.55" "192.168.70.15" "192.168.70.57" "192.168.70.58" "192.168.70.59" "192.168.70.26" "192.168.70.27" "192.168.70.28" "192.168.70.29")
username=sitemapuser
path=/home/myntradomain/public_html/
cd $path

for host in ${hostnames[@]}
do
   for file in sitemap*xml
   do
   rsync -avz $file $username@$host:$path
ssh -t $username@$host '( cd /home/myntradomain/public_html/ && /usr/bin/sudo /bin/chown apache.apache sitemap*xml )'
   done
   echo "push complete to host: "$host
done