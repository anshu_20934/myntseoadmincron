#!/bin/bash
# styleIndex.sh for style index in parallel


style_count=16000
page_size=4000

echo "Starting"

PAGES=$((style_count/page_size))
PAGE=0
echo "======== Total Pages :: ".$PAGES." =========="

while [  $PAGE -lt $PAGES ]; do
	PAGE=$((PAGE+1))
	sh styleIndexPage.sh $PAGE $page_size>/home/ashish/log/$PAGE.log &
done
exit
