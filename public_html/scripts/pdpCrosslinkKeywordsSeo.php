<?php
//Script to process active products added before today
$dirName = dirname(__FILE__);
chdir($dirName);
include_once("../auth.php");
require_once '../env/DB.config.php';
require_once($xcart_dir."/include/func/func.utilities.php");
require_once($xcart_dir."/include/func/func.core.php");
include_once \HostConfig::$documentRoot ."/utils/throttleUpdatesDb.php";
define('ALLOWABLETAGS','<h1><h2><h3><h4><h5><h6><em><strong><br/><b><p><br>');
define('REMOVABLETAGS','a');
define('DBSERVER', DBConfig::$rw['host']);
define('DBUSER', DBConfig::$rw['user']);
define('DBPASS', DBConfig::$rw['password']);
define('DBNAME', DBConfig::$rw['db']);
define('SOLRSERVER', SOLRConfig::$searchhost);
define('LASTPROCESSEDIDFILE',"/etc/seoPDPCrosslinking/SeoLastCrosslinkedProdId.txt");
define('LINKLIMIT', 6);

function getProductStyleIdsHavingWord($word,$maxId,$minId,$solrConnection){
    $word = str_replace(" ","+",$word);
    return $solrConnection->fieldListSearch("(descr:".$word.") AND (styleid_int:[$minId TO $maxId])", "styleid,descr", 0, $maxId-$minId+1);
}

function getAllHandPickedKeywords(){
    $sql = "(SELECT crosslink_keyword as keyword,IFNULL(crosslink_keyword_url, replace(crosslink_keyword,' ','-')) as url FROM mk_crosslink_keyword WHERE is_active='1') union distinct (SELECT keyword,replace(url,'www.myntra.com/','') FROM adseo_sitemapkeyword WHERE pattern != 'b')";
    return func_query($sql, true);
}

function getStyleSpecificExtraPDLinks($maxid,$minId){
    $sql = "Select style_id, link_keywords, target_url, css_class from adseo_pdpextralinks where is_active='1' and style_id between $minId and $maxid";
    return func_query($sql, true);
}

function getLatestProdId($solrConnection){
    $result = $solrConnection->searchAndSort("add_date:[* TO NOW-1DAY]", 0, 1,"styleid desc", array("fl=styleid"));
    $docs = $result->docs;
    return $docs[0]->styleid;
}

function buildKeywordLinkWordCountHash($keyword,$url="", $isRelativeUrl, $cssClass){
    global $http_location_cli;
    if($isRelativeUrl){
            $pattern = empty($url) && !$isAbsoluteUrl? str_replace(" ", "-", strtolower($keyword)) : $url;
            $url = $http_location_cli."/".$pattern;
    }
    $link = constructAnchorTag($url, $cssClass, $keyword); 
    return array('keyword' => $keyword, 'link'=> $link, 'wc' =>str_word_count($keyword));
}

function constructAnchorTag($targetUrl, $cssClass, $keyword){
    return "<a href='".$targetUrl."?src=pd' class='".$cssClass."'>".$keyword."</a>";
}

function getLinkableFilter(){
    $linkedKeywords = array();

    $HK = getAllHandPickedKeywords();
    if($HK){
        foreach($HK as $index=>$val){
            $ckeyword = $val['keyword'];
            if(!empty($ckeyword)){
                $linkedKeywords[] = buildKeywordLinkWordCountHash($ckeyword,$val['url'],true,"seolink");
            }
        }
    }
    return $linkedKeywords;
}

function sortLinkableKeywords($linkedKeywords){
    $sortedKeywords = array();
    $sortedKeywordsWithWC = quickSortRecursive( $linkedKeywords, $left = 0 , $right = NULL, 'wc' );
    $sortedKeywords = array_reverse($sortedKeywordsWithWC);
    return $sortedKeywords; 
}

function getSortedLinkedFilters(){
    $filters = getLinkableFilter();
    $sortedLinkedKeywords = sortLinkableKeywords($filters);
    return $sortedLinkedKeywords;
}
function initializeStyleKeywordMapValue($styleId, $description){
    return array('id'=>$styleId, 'descr'=>$description, 'keywords'=>array());
}

function createStyleKeywordMap($sortedKeywords, $processProdIds,$solrConnection,$productKeywordMap){
    $maxId = $processProdIds['maxProdId'];
    $minId = $processProdIds['minProdId'];
    var_dump("creating style keyword map");
    foreach($sortedKeywords as $word){
        $prods = getProductStyleIdsHavingWord($word['keyword'],$maxId,$minId,$solrConnection);
        foreach($prods as $prod){
            $prodid = $prod->styleid;
            if(!$productKeywordMap[$prodid]){
                $productKeywordMap[$prodid] = initializeStyleKeywordMapValue($prodid, $prod->descr);
            }
            if(!$productKeywordMap[$prodid]['descr']){
                $productKeywordMap[$prodid]['descr'] = $prod->descr;
            }
            array_push($productKeywordMap[$prodid]['keywords'],$word);
        }
    }
    return array_filter($productKeywordMap, function($kv){ return $kv['descr'] != NULL; });
}

function createStyleExtraLinksMap($processProdIds,$solrConnection){
    $styleKeywordMap = array();
    $extraLinks = getStyleSpecificExtraPDLinks($processProdIds['maxProdId'], $processProdIds['minProdId']);
    foreach($extraLinks as $el){
        $styleId = $el['style_id'];
        if(!$styleKeywordMap[$styleId]){
            $styleKeywordMap[$styleId] = initializeStyleKeywordMapValue($styleId, NULL);
        }
        $keywordLinkWordCountHash = buildKeywordLinkWordCountHash($el['link_keywords'], $el['target_url'], false, $el['css_class']);
        array_push($styleKeywordMap[$styleId]['keywords'],$keywordLinkWordCountHash);
    }
    return $styleKeywordMap;    
}

function applyCrosslinking($text,$keywords){
    global $replaced,$replacements;
       $replaced = array();    
    $charsToEscape = "/";
    $search = array();
    foreach($keywords as $index=>$word){
        $searchPattern = preg_quote($word['keyword'],$charsToEscape);
        $searchPattern = "/\b(".$searchPattern.")\b(?!(?:(?!<\/?[ha].*?>).)*<\/[ha].*?>)(?![^<>]*>)/i";
        $search[$index] = $searchPattern;
        $replaced[strtolower($word['keyword'])] = $word['link'];
    }
    $replacements = 0;
    $text = preg_replace_callback($search, function($matches) {
        global $replaced, $replacements;
        $returnString = $matches[0];
         if($replacements < LINKLIMIT){
             $replacements++;
             $matched = $replaced[strtolower($matches[0])];
             $returnString =  preg_replace('/>'.$matches[0].'</i','>'.$matches[0].'<',$matched, 1);
         }
         return $returnString;
    }, $text, 1, &$count);
    return $text;
}
function makePDPDescriptionCrossLinked($desc,$keywords,$id,$columnName, $tableName){
    $linkableText = stripOnlyTags($desc,REMOVABLETAGS);
    $crosslinkedDescription= applyCrosslinking($linkableText, $keywords);
    $queryData = array($columnName=>$crosslinkedDescription);
    return query_func_array2update($tableName, sanitizeDBValues($queryData),"id='{$id}'");
}
function updateDescriptions($crossLinkedDescriptions){
    $connection = \mysql_connect(DBSERVER, DBUSER, DBPASS);
    mysql_set_charset('utf8',$connection);
    @\mysql_select_db(DBNAME) or die( "Unable to select database");
    $count = 0;
    foreach( $crossLinkedDescriptions as $query ){
        $count++;
        $result = \mysql_query($query,$connection);
        if (!$result) {
            echo $query;
        }
    }
    var_dump($count);
    return $batchDBQuery;
}

function crosslinkProductDescriptions($productKeywordMap){
    $crossLinkedDescriptions = array();
    $cnt = 0;
    foreach($productKeywordMap as $productDetails){
        $style = $productDetails['id'];
        $desc = $productDetails['descr'];
        $keywords = $productDetails['keywords'];
        $crossLinkedDescriptions[] =
            makePDPDescriptionCrossLinked($desc,$keywords,$style,'description','mk_product_style');
    }
    updateDescriptions($crossLinkedDescriptions);
}
function getLastProcessedId($solrConnection){
    $prodId = intval(file_get_contents(LASTPROCESSEDIDFILE));
    if ($prodId == NULL or empty($prodId) or $prodId <= 1){
        $prodId = intval(getLatestProdId($solrConnection));
    }
    $minProdId = 1;
    if($prodId > 10000){
       $minProdId = $prodId-10000;
    }    
    return array('maxProdId' => $prodId, 'minProdId'=>$minProdId);
}

function writeLastProcessedProdId($minProdId){
    $result = file_put_contents(LASTPROCESSEDIDFILE, $minProdId);
    var_dump($minProdId." : ".$result);
}

var_dump(date("F j, Y, g:i a"));
$start = microtime(true);
$solrConnection = new SolrCommon(SOLRSERVER,'sprod');
$sortedKeywords = getSortedLinkedFilters();
$time_taken_sf = microtime(true) - $start;
var_dump("Sorted keyword Count : ".count($sortedKeywords));
var_dump("Time for sorted filters : ".$time_taken_sf);

for($i=1; $i < 4; $i++){
$time_LPI = microtime(true);
$processProdIds = getLastProcessedId($solrConnection);
var_dump("processing ids", $processProdIds);
$time_taken_LPI = microtime(true) - $time_LPI;
var_dump("Time for last processed Id : ".$time_taken_LPI);

$time_ESKM = microtime(true);
$productKeywordMap = createStyleExtraLinksMap($processProdIds,$solrConnection);
$time_taken_ESKM = microtime(true) - $time_ESKM;
var_dump("Time for IDKeywordMaps : ".$time_taken_ESKM);
 
$time_SKM = microtime(true);
$productKeywordMap = createStyleKeywordMap($sortedKeywords,$processProdIds,$solrConnection,$productKeywordMap);
$time_taken_SKM = microtime(true) - $time_SKM;
var_dump("Time for IDKeywordMaps : ".$time_taken_SKM);

$time_CPD = microtime(true);
crosslinkProductDescriptions($productKeywordMap);
$time_taken_CPD = microtime(true) - $time_CPD;
var_dump("Time for crosslinkProductDescriptions : ".$time_taken_CPD);

writeLastProcessedProdId($processProdIds['minProdId']);
}

$time_taken = microtime(true) - $start;
var_dump("Total time taken for script : ".$time_taken);
