<?php
require_once "../auth.php";
require_once "$xcart_dir/include/class/class.mail.amazonsesmailer.php";

class MigrationSanityChecker{
	private $host;
	private $username;
	private $password;
	private $dbname;
	private $con;

	public function __construct($host,$username,$password,$dbname){
		$this->host = $host;
		$this->username = $username;
		$this->password = $password;
		$this->dbname = $dbname;

		$this->sendMailSubject = "Style Image Migration Report";
		$this->sendMailTo = "cmsoncall@myntra.com";
		$this->sendMailFrom = "admin@myntra.com";	
	}

	public function run(){
		$isConn = $this->setUpConnection();
		if($isConn){
			$result = $this->getUnmigratedStyles();

			if(!empty($result)){
				$styleIdsCSV = $this->extractStyleIds($result);
				$message = "Following Styles are active but are not yet migrated : \n" . $styleIdsCSV;
				$this->sendMail($message);
			}
		}else{
			$this->sendMail("Connection set up failed");
		}
	}

	/**Creates connection to database*/
	private function setUpConnection(){
		try {
			$dsn = 'mysql:dbname='.$this->dbname.';host='.$this->host;
			$this->con = new PDO($dsn,$this->username,$this->password);
			$this->con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			return true;
		}catch (PDOException $e){
			echo $e->getMessage() ."\n";
			return false;
		}
	}

	private function getUnmigratedStyles(){
		$query = "select ps.id from mk_product_style ps left outer join productImageMigration pim on pim.product_id = ps.id where ps.styletype = 'P' and (pim.migrated is null or pim.migrated = false)";

		$result = $this->executeQuery($query);
		return $result;
	}

	/**This function creates csv of style ids using database query result*/
	private function extractStyleIds($result){
		if(!empty($result)){
			$styleIdsArray = array();
			foreach ($result as $row){
				array_push($styleIdsArray,$row["id"]);
			}
			$styleIdsCSV = implode(",",$styleIdsArray);
			return $styleIdsCSV;
		}
	}

	/**This functions sends mail*/
	private function sendMail($bodyOfMail){
		if(!empty($bodyOfMail)){
			$mailer = new AmazonSESMailer();
			$email_details = array();
			$email_details['to'] = $this->sendMailTo;
			$email_details['subject']= $this->sendMailSubject;
			$email_details['content']= $bodyOfMail;
			$email_details['from_email'] = $this->sendMailFrom;
			
			//write to a file and send a mail
			echo "\nSending Mail ..... \n";
			$mailSent = $mailer->sendMail($email_details);
			if($mailSent){
				echo "\nMail successfully sent\n";
			}else{
				echo "\nMail not sent\n";
			}
		}
		//echo $this->bodyOfMail;
	}

	private function executeQuery($queryToExecute){
		try{
			$stmt = $this->con->prepare($queryToExecute);
			$stmt->execute();
			$result = $stmt->fetchAll();
			return $result;
		} 
		catch(PDOException $e){
			echo "Error : ". $e->getMessage()."\n";
			die();
		}
	}

}
/**Database credentials*/
$host = DBConfig::$reportsro["host"];
$username = DBConfig::$reportsro["user"];
$password = DBConfig::$reportsro["password"];
$dbname = DBConfig::$reportsro["db"];

$migration = new MigrationSanityChecker($host,$username,$password,$dbname);
$migration->run();

?>