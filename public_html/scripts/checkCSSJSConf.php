<?php
include_once(dirname(__FILE__)."/../auth.php");
include_once("$xcart_dir/include/class/widget/class.widget.keyvaluepair.php");
include_once "$xcart_dir/include/class/class.feature_gate.keyvaluepair.php";

$cssjsVerificationServersKey="CSSJSVerifyKey";
$servers = FeatureGateKeyValuePairs::getStrArray($cssjsVerificationServersKey);
$skins=array("skin2"=>array());


foreach($skins as $skin=>$configs){
    foreach($servers as $server){
        $conf = WidgetKeyValuePairs::getWidgetValueForKey("$skin-cssjsconf-$server");
        $skins[$skin][$conf] = $skins[$skin][$conf].":".$server;
    }

}
foreach($skins as $skin=>$config){
    if(count($config) > 1){
        $message = "<h2>CSSJS configuration not same for $skin </h2>";
        foreach($config as $value=>$server){
            $message .= " <div>$server : </div><textarea style='height:300px;width:700px'>$value</textarea>";
        }
    }
}
if(!empty($message)){
    echo $message;
    $headers  = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
    @mail(EmailListsConfig::$dbConnectionFailure, "Error : CSS/JS config not in sync", $message, $headers);
}
?>
