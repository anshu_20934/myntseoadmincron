<?php

include_once("auth.php");
include_once("$xcart_dir/include/func/func.order.php");
include_once("$xcart_dir/include/class/oms/EventCreationManager.php");

$orderids = array(7125628,7125701);

$orders = func_query("Select * from xcart_orders where orderid in (".implode(",", $orderids).")");

foreach($orders as $order) {
	EventCreationManager::pushReleaseUpdateEvent($order['orderid'], "LMS", $order['courier_service'], $order['tracking']);
	echo "Pushed order - $order[orderid] \n";
}

?>
