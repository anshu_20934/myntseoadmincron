<?php

    include_once("auth.php");
    include_once("$xcart_dir/include/func/func.order.php");
    include_once($xcart_dir."/modules/apiclient/ReleaseApiClient.php");
    include_once("$xcart_dir/include/class/oms/EventCreationManager.php");

    $orderids = array(6340356,6348530,6348553,6350876,6351912,6352522,6353666,6354452,6354790,6355213,6355969,6356025,6356252,6356489,6356688,6356691,6356825,6356866,6357200,6357937,6358093,6358344,6358398,6359955,6360316,6360362,6360800,6362783,6364353,6365994);

    foreach($orderids as $orderid) {

	$order_query = "UPDATE xcart_orders SET status ='WP' WHERE orderid = $orderid";
	db_query($order_query);
	$order_detail_query = "UPDATE xcart_order_details SET item_status = 'A', assignee=1, assignment_time = ".time()." where orderid = $orderid";
	db_query($order_detail_query);
	func_addComment_status($orderid, "WP", "Q", "Auto moving order to WP state from Q", "AUTOSYSTEM");
        EventCreationManager::pushReleaseStatusUpdateEvent($orderid, "WP","Auto moving order to WP state from Q", time(), "AUTOSYSTEM");
	ReleaseApiClient::pushOrderReleaseToWMS($orderid, "SYSTEM", 'update');

        echo "Moved order $orderid to WIP \n";
		
    }

?>
