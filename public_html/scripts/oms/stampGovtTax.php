<?php

include_once("auth.php");
include_once("$xcart_dir/include/class/oms/OrderWHManager.php");
include_once("$xcart_dir/include/class/oms/EventCreationManager.php");

$ordersWithStampTaxFailure = func_query("select group_concat(orderid), warehouseid, courier_service from xcart_orders where status = 'OH' and on_hold_reason_id_fk = 29", true);
$shipmentids = array();
foreach ($ordersWithStampTaxFailure as $orders) {
    array_push($shipmentids, $orders['orderid']);
}

foreach($shipmentids as $order) {
        echo "\nStamping govt tax for order: $order";
        \OrderWHManager::stampTax($order);
        db_query("update xcart_orders set status = 'Q' where orderid = " . $order);
        \EventCreationManager::pushCompleteOrderEvent($order);
        sleep(2);
        \EventCreationManager::pushReadyForReleaseEventWithQueuedDate($order, time(), 'System');
        usleep(500);
}