<?php 

include_once("auth.php");
use oms\OrderDetailRequestProcessor;

$processor= new OrderDetailRequestProcessor();

$orderids = array(6008206,6074192);

foreach($orderids as $orderid) {
	$response = $processor->checkAvailabilityAndQueueOrder($orderid);
	echo "Response - $orderid - ".$response[0]."\n";
}
?>
