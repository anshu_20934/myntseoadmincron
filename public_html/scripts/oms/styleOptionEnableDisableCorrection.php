<?php

    chdir(dirname(__FILE__));
    require "../auth.php";
    include_once("$xcart_dir/include/func/func.db.php");
    include_once("$xcart_dir/include/func/func.order.php");
    
    echo "Option Sku enable disable started at ".date('j M Y H:i:s', time())."\n";
    
    $sql = "Select option_id, sku_id, is_active from mk_product_options po, mk_styles_options_skus_mapping som where po.id = som.option_id";
    $results = func_query($sql, true);
    if ($results) {
	foreach($results as $row) {
	    if($row['is_active'] && $row['is_active'] == 1)
	    	$activeSkuOptions[] = $row;
	    else
	    	$inactiveSkuOptions[] = $row;
        }
    }

    echo "Active Skus - ".count($activeSkuOptions)."\n";
    echo "InActive Skus - ".count($inactiveSkuOptions)."\n";

//--------------------- WMS Data Read directly from slave database----------------------------------
    $wms_ro = mysql_connect('192.168.70.25','myntraAppDBUser','9eguCrustuBR',TRUE);
    if(mysql_select_db('myntra_wms', $wms_ro ) === false) {
        echo 'Wms db unaccessible'; 
	exit; 
    }

    $sql = "Select sku_id, sum(greatest(ic.inv_count, 0) - greatest(ic.blocked_order_count,0) - greatest(ic.blocked_processing_count,0) - greatest(ic.blocked_missed_count,0) - greatest(ic.blocked_manually_count,0)) as availCount from core_inventory_counts ic JOIN mk_skus s on ic.sku_id = s.id where ic.quality = 'Q1' and ic.warehouse_id in (1,2,4,5) and (s.admin_disabled = 0 and s.enabled = 1) group by sku_id";
    $sku2ActiveStatusMap = array();
    $p_result = mysql_query($sql, $wms_ro);
    if ($p_result) {
        while ($row = mysql_fetch_array($p_result, MYSQL_ASSOC)) {
            if($row['availCount'] > 0)
                $sku2ActiveStatusMap[$row['sku_id']] = true;
            else
                $sku2ActiveStatusMap[$row['sku_id']] = false;
        }

        @mysql_free_result($p_result);
    }

    $activeToBeDisabledOptionIds = array();
    foreach($activeSkuOptions as $row) {
	if(!isset($sku2ActiveStatusMap[$row['sku_id']]) || $sku2ActiveStatusMap[$row['sku_id']] === false)
            $activeToBeDisabledOptionIds[] = $row['option_id'];        
    } 
    echo "Active Options which need to be disabled = ". implode(",", $activeToBeDisabledOptionIds)."\n";
    if(count($activeToBeDisabledOptionIds) > 0) {
	    foreach($activeToBeDisabledOptionIds as $optionId) {
	        db_query("update mk_product_options set is_active = 0 where id = $optionId");
			sleep(3);
	    }
    }
    
    $inactiveToBeEnabledOptionIds = array();
    foreach($inactiveSkuOptions as $row) {
		if(isset($sku2ActiveStatusMap[$row['sku_id']]) && $sku2ActiveStatusMap[$row['sku_id']] === true)
            $inactiveToBeEnabledOptionIds[] = $row['option_id'];        
    } 
    echo "InActive Options which need to be enabled = ". implode(",", $inactiveToBeEnabledOptionIds)."\n";
    if(count($inactiveToBeEnabledOptionIds) > 0) {
	    foreach($inactiveToBeEnabledOptionIds as $optionId) {
	        db_query("update mk_product_options set is_active = 1 where id = $optionId");
			sleep(3);
	    }
    }
    echo "Option Sku enable disable complete at ".date('j M Y H:i:s', time())."\n";
    
    $changedOptionIds = array_merge($activeToBeDisabledOptionIds, $inactiveToBeEnabledOptionIds);
    if(count($changedOptionIds) > 0) {
    	echo "Starting changed styles indexing at ".date('j M Y H:i:s', time())."\n";
	   	$styleRows = func_query("Select distinct style_id from mk_styles_options_skus_mapping where option_id in (".implode(",", $changedOptionIds).")");
	    if($styleRows && !empty($styleRows)){
	    	include_once($xcart_dir.'/include/solr/solrProducts.php');
	    	include_once($xcart_dir.'/include/solr/solrUpdate.php');
	    	foreach ($styleRows as $row) {
	    		@addStyleToSolrIndex($row['style_id'], false, false, false);
	    	}
	    }
	    echo "Styles indexing completed at ".date('j M Y H:i:s', time())."\n";
    }
?>
