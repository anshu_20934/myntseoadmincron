<?php
include_once("auth.php");
include_once("$xcart_dir/include/class/oms/OrderWHManager.php");

$releaseIds = array(17928486);

// This script does not check for feature gate

foreach ($releaseIds as $releaseId) {

    $fetchReleaseInfoQuery = "select warehouseid, s_zipcode from xcart_orders where orderid = ".$releaseId;
        $releaseInfo = func_query_first($fetchReleaseInfoQuery);
        $sourceWarehouseId = $releaseInfo['warehouseid'];
        $destinationPincode = $releaseInfo['s_zipcode'];
        $fetchLineInfoQuery = "select itemid, product_style, price, amount, discount, coupon_discount_product, cart_discount_split_on_ratio from xcart_order_details where orderid = $releaseId";
        $lineInfo = func_query($fetchLineInfoQuery);
        $lines = array();
        if (!is_array($lineInfo)) {
            $lines = array($lineInfo);
        } else {
            $lines = $lineInfo;
        }
        foreach($lineInfo as $singleLineInfo) {
            $itemid = $singleLineInfo['itemid'];
            $styleId = $singleLineInfo['product_style'];
            $itemCost = $singleLineInfo['price'] * $singleLineInfo['amount'] - $singleLineInfo['discount'] - $singleLineInfo['coupon_discount_product'] - $singleLineInfo['cart_discount_split_on_ratio'];
            echo "Tax entry: style: " . $styleId . ", item cost: ". $itemCost . ", wh id: " . $sourceWarehouseId . ", pincode: ".$destinationPincode . "\n";
            $taxInfo = \OrderWHManager::fetchGovtTax($styleId, $itemCost, $sourceWarehouseId, $destinationPincode);
            $itemUpdateQuery = "update xcart_order_details set govt_tax_rate = " . $taxInfo['govtTaxRate'] . ", govt_tax_amount = " . $taxInfo['govtTaxAmount'] . "where itemid = $itemid";
            echo $itemUpdateQuery;
            db_query($itemUpdateQuery);
        }

        \EventCreationManager::pushCompleteOrderEvent($releaseId);
        echo "Syncing to OMS for order - $releaseId \n";
}