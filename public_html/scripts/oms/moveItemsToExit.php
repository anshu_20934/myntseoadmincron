<?php

include_once("auth.php");
include_once("$xcart_dir/modules/apiclient/LocationApiClient.php");
include_once("$xcart_dir/modules/apiclient/ItemApiClient.php");

$warehouseId = 1;
$doQAChecking = false;
$items = array(100005490830);

//Items present in bins other than logistics or QA fulfilment..
if($doQAChecking) {
    if($warehouseId == 1)
	$response = LocationApiClient::moveItemsToBin($items, 'BAFUQU-01-A-01', '14YV', 'sanjay.yadav');
    else
	$response = LocationApiClient::moveItemsToBin($items, 'DEFUQU-01-A-01', '15ZL', 'sanjay.yadav');
}
if($response == "") {
	echo "Successfully moved to QA bin\n";
	if($warehouseId == 1)
		$binLocation2ItemsMap['BAFUQU-01-A-01'] = array('code'=>'14YV', 'items'=>$items);
	else
		$binLocation2ItemsMap['DEFUQU-01-A-01'] = array('code'=>'15ZL', 'items'=>$items);

	$response = LocationApiClient::moveItemsToExitBin($binLocation2ItemsMap, 'sanjay.yadav');
	$failedItemIds = $response[0];
	if(!empty($failedItemIds)) {
		echo("Movement to Exit bin failed for following Items - ".implode(", ", $failedItemIds).". ".$response[1]."\n");
	} else {
        	echo("Successfully moved to exit bin\n");
	} 
} else {
	echo("Failed to move items to QA bin\n");
}

?>
