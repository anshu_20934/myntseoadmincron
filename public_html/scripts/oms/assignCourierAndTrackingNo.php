<?php

include_once("auth.php");
include_once("$xcart_dir/include/class/oms/OrderWHManager.php");
include_once $xcart_dir . "/include/func/func.order.php";

$orderids = array(16143478);

foreach ($orderids as $orderid) {
    $productsInCart = func_create_array_products_of_order($orderid);
    $order_details = func_query_first("select * from xcart_orders where orderid = $orderid");
    $pincode = $order_details['s_zipcode'];
    $serviceType = $order_details['ordertype'];
    if ($order_details['payment_method'] == 'cod') {
        $couriers = func_get_order_serviceabile_courier('cod', $pincode, $productsInCart, $order_details['warehouseid'], $order_details['total'], $serviceType);
        $returnAry['serviceable'] = ($couriers && count($couriers) > 0) ? true : false;
    } else {
        $couriers = func_get_order_serviceabile_courier('on', $pincode, $productsInCart, $order_details['warehouseid'], $order_details['total'], $serviceType);
        $returnAry['serviceable'] = ($couriers && count($couriers) > 0) ? true : false;
    }

    db_query("Update xcart_orders set courier_service = '" . $couriers[0] . "', tracking = null where orderid = $orderid");
    $whCourier2OrderId[$order_details['warehouseid'] . '__' . $couriers[0]] = $orderid;
    $orderid2TrackingNo = \OrderWHManager::assignTrackingNumberForOrders($whCourier2OrderId, $order_details['payment_method'], $pincode);
    echo "Assigned courier: " . $couriers[0] . " for order: " . $orderid;
}
?>