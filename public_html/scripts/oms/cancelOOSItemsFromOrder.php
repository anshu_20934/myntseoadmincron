<?php

include_once("auth.php");
include_once $xcart_dir."/include/func/func.mkcore.php";
include_once $xcart_dir."/include/func/func.mk_orderBook.php";
include_once $xcart_dir."/include/func/func.order.php";
include_once $xcart_dir."/include/func/func.courier.php";
include_once($xcart_dir."/include/class/widget/class.widget.keyvaluepair.php");
include_once("$xcart_dir/include/func/func.returns.php");
include_once("$xcart_dir/modules/apiclient/ItemApiClient.php");
include_once("$xcart_dir/modules/apiclient/LocationApiClient.php");
include_once("$xcart_dir/modules/apiclient/SkuApiClient.php");
include_once("$xcart_dir/modules/apiclient/WarehouseApiClient.php");
include_once("$xcart_dir/include/class/oms/OrderWHManager.php");
include_once("$xcart_dir/modules/apiclient/ReleaseApiClient.php");
include_once("$xcart_dir/include/class/oms/EventCreationManager.php");
include_once("$xcart_dir/include/func/func_sku.php");


$orderids = array(10353462,10353506,10353533,10353610,10353631,10353643,10353696,10353718,10353800,10353807,10353815,10353867,10353896,10353912,10353935,10353938,10353979,10354000,10354003,10354026,10354047,10354066,10354095,10354096,10354103,10354117,10354147,10354175);

foreach($orderids as $orderid) {
    $products = func_create_array_products_of_order($orderid);
    $skuIds = array();
    $hasJitItems = false;
    foreach($products as $row) {
        if ($row['supply_type'] == 'JUST_IN_TIME') {
            $hasJitItems = true;
            break;
        }

        $skuIds[] = $row['sku_id'];
    }

    if ($hasJitItems) {
        continue;
    }

    if(!empty($skuIds)) {
	$allOOS = true;
        $skuDetails = SkuApiClient::getSkuDetailsInvCountMap($skuIds);
        foreach($products as $row) {
            if(isset($skuDetails[$row['sku_id']]) && $skuDetails[$row['sku_id']]['availableItems'] > 0 )
		$allOOS = false;
        }

        if($allOOS) {
	    echo "All items have zero availability\n";
	    // cancel order as all items are OOS
	    $cancel_args = array(
		'cancellation_type' => 'CCC',
		'reason' => 'OOSC',
		'orderid2fullcancelordermap' => array($orderid=>true)
	    );
            echo "Cancelling order no - $orderid as all items are OOS\n";
	    func_change_order_status($orderid, 'F', 'SYSTEM_SCRIPT', 'All items are OOS. Cancelling Order', "", false, '', $cancel_args);
        } else {
	   echo "Some items have availability - \n";
           // do partial cancellations and then queue order
           $itemids = array();
	   $request = array('orderid' => $orderid, 'cancellation_type' => 'CCC', 'cancellation_reason' => 'OOSC', 'login' => 'SYSTEM_SCRIPT');
	   foreach($products as $item) {
		echo "Available - ".$skuDetails[$item['sku_id']]['availableItems']." - Required - ".$item['quantity']."\n";
		if($skuDetails[$item['sku_id']]['availableItems'] < $item['quantity']) {
		    $itemids[] = $item['itemid'];
                    $request['item_quantity_'.$item['itemid']] = ($item['quantity'] - $skuDetails[$item['sku_id']]['availableItems']);
                }
	   }
           if(count($itemids) > 0) {
               $request['itemids'] = implode(",", $itemids);	
               $request['cancel_comment'] = 'Item OOS. Cancelling.';	
               $request['notify_customer_email'] = true;	
               echo "Cancelling OOS Items - ".print_r($request, true)."\n";
	       global $_POST;
	       $_POST = $request;
	       \ItemManager::cancelItems($request);
           }
           
	   $new_products = func_create_array_products_of_order($orderid);
           $new_skuDetails = get_sku_details($new_products);
           $order_details = func_query_first("select * from xcart_orders where orderid = $orderid");
           
           $serviceabilityResponse = \OrderWHManager::loadServiceabilityDetails($new_products, $new_skuDetails, $order_details['s_zipcode'],$order_details['payment_method']);
           if($serviceabilityResponse['status'] != true) {
               echo "Courier Serviceability Failure - ".$serviceabilityResponse['reason']."\n";
               echo "Cancelling order no - $orderid as no courier to service\n";
               $cancel_args = array(
                    'cancellation_type' => 'CCC',
                    'reason' => 'OOSC',
                    'orderid2fullcancelordermap' => array($orderid=>true)
                );
	       func_change_order_status($orderid, 'F', 'SYSTEM_SCRIPT', 'All items are not serviceable. Cancelling Order', "", false, '', $cancel_args);
	   } else {
		$whIdToOrderIds = \OrderWHManager::assignWarehouseForOrder($orderid, $order_details, $new_products, $new_skuDetails,"Order created from $orderid and moved to WP state", true);
		if($whIdToOrderIds) {
		    \OrderWHManager::updateInventoryAfterSplit($orderid, $whIdToOrderIds, "Order created from $orderid and moved to WP state", $login);
		    func_addComment_status($orderid, "WP", "OH", "Moving order to WP state from OH", $login);
		    $releaseIdsToPush = array();
		    foreach($whIdToOrderIds as $whId=>$oids) {
			$releaseIdsToPush = array_merge($releaseIdsToPush, $oids);
			foreach($oids as $oid) {
		            func_array2update('orders', array('status'=>'WP', 'queueddate'=>time()), "orderid = $oid");
			}
		    }
		    \ReleaseApiClient::pushOrderReleaseToWMS($releaseIdsToPush);
		    \EventCreationManager::pushCompleteOrderEvent($orderid);
		    echo "Order Queued Successfully \n";
		}	
	    }
        }
    }
}

?>
