<?php
ini_set('memory_limit', '-1');
echo "Started the comparison programme \n \n";
/*********************************************************************************************************************/
                                      /* Connecting to OMS DB */
$oms_ro = mysql_connect('192.168.70.25','myntraAppDBUser','9eguCrustuBR',TRUE);

if (!$oms_ro) {
    die('Could not connect: ' . mysql_error());
}
echo "Connected successfully to ERPDB. Changing database to myntra_oms \n";

if(mysql_select_db('myntra_oms', $oms_ro ) === false) {
        echo "Oms db unaccessible \n";
        exit;
}
echo "Changed database to myntra_oms \n";

echo "Connected to OMS database \n";

$omsPKOrderReleaseFetchQuery="select oms.id as release_id from myntra_oms.order_release oms join myntra_lms.order_to_ship lms on (lms.order_id=oms.id) where lms.created_on > DATE_SUB(now(), INTERVAL 20 DAY) and lms.status in ('PK','IS') and oms.status_code in ('WP')";
$p_result = mysql_query($omsPKOrderReleaseFetchQuery, $oms_ro);
$omsPKOrdersArray = array();
    if ($p_result) {
        while ($row = mysql_fetch_array($p_result, MYSQL_ASSOC)) {
            array_push($omsPKOrdersArray,$row['release_id']);
        }
        echo "Fetched ".count($omsPKOrdersArray)." orders from OMS / LMS. Orders in WP in OMS\n";
        @mysql_free_result($p_result);
    } else {
	echo "Fetched no orders from OMS / LMS. PK status sync not required\n";
}

$omsSHOrderReleaseFetchQuery="select oms.id as release_id from myntra_oms.order_release oms join myntra_lms.order_to_ship lms on (lms.order_id=oms.id) where lms.created_on > DATE_SUB(now(), INTERVAL 20 DAY) and lms.status in ('SH','FD') and oms.status_code in ('WP','PK')";
$p_result = mysql_query($omsSHOrderReleaseFetchQuery, $oms_ro);
$omsSHOrdersArray = array();
    if ($p_result) {
        while ($row = mysql_fetch_array($p_result, MYSQL_ASSOC)) {
            array_push($omsSHOrdersArray,$row['release_id']);
        }
        echo "Fetched ".count($omsSHOrdersArray)." orders from OMS / LMS. Orders in WP/PK in OMS\n";
        @mysql_free_result($p_result);
    } else {
        echo "Fetched no orders from OMS / LMS. SH status sync not required\n";
}

$omsDLOrderReleaseFetchQuery="select oms.id as release_id from myntra_oms.order_release oms join myntra_lms.order_to_ship lms on (lms.order_id=oms.id) where lms.created_on > DATE_SUB(now(), INTERVAL 20 DAY) and lms.status in ('DL') and oms.status_code in ('WP','PK','SH')";
$p_result = mysql_query($omsDLOrderReleaseFetchQuery, $oms_ro);
$omsDLOrdersArray = array();
    if ($p_result) {
        while ($row = mysql_fetch_array($p_result, MYSQL_ASSOC)) {
            array_push($omsDLOrdersArray,$row['release_id']);
        }
        echo "Fetched ".count($omsDLOrdersArray)." orders from OMS / LMS. Orders in WP/PK/SH in OMS\n";
        @mysql_free_result($p_result);
    } else {
        echo "Fetched no orders from OMS / LMS. DL status sync not required\n";
}

//echo "Entire array from oms: " . print_r($omsOrdersArray);
mysql_close($oms_ro);

echo "\n*********************************************************************************************************** \n";
/*********************************************************************************************************************/
                                      /* Comparing order params across systems*/

$releasesNotInSync = compareOrders($omsPKOrdersArray,$omsSHOrdersArray,$omsDLOrdersArray);
echo "Ended the comparison program \n Number of orders not in sync: " . count($releasesNotInSync) . "\n";
echo "Checked for data sync of ".count(array_keys($releasesNotInSync)). " orders in last 1 day \n";
// message
$message = '
    <html>
    <head>
      <title>Data sync result of orders in last 1 day</title>
    </head>
    <body>
      <p>Data sync result for orders created in last 20 days<br/>
	Fetched '.count($omsPKOrdersArray).' orders for PK not synced <br/>
        Fetched '.count($omsSHOrdersArray).' orders for SH not synced <br/>
        Fetched '.count($omsDLOrdersArray).' orders for DL not synced <br/>
	Following releases are not in sync : '.count($releasesNotInSync).'</p>
    </body>
    </html>';

if(count($releasesNotInSync) > 0){
    //echo print_r($releasesNotInSync);
    // subject
    $subject = 'LMS and OMS Sync - Inconsistency';
    $commaSeparatedOrders = "";
    $arrayForEmail = array();
    foreach ($releasesNotInSync as $releaseid => $paramsNotInSync){
	//echo $paramsNotInSync . "\n";
        $paramsNotInSyncString = str_replace("\n", "", $paramsNotInSync);
	//echo $paramsNotInSyncString . "\n";
        array_push($arrayForEmail,$releaseid . " " . $paramsNotInSyncString);
    }
    $message .= "\r\n\n".implode("<br/>", $arrayForEmail);
	// send email for orders out of sync
	//echo $message;
	$to  = 'pavankumar.at@myntra.com';
	// To send HTML mail, the Content-type header must be set
	$headers  = 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

	// Additional headers
	//$headers .= 'To:  engg_erp_oms@myntra.com' . "\r\n";
	$headers .= 'From: pavankumar.at@myntra.com' . "\r\n";
	$headers .= 'Cc: ' . "\r\n";
	$headers .= 'Bcc: ' . "\r\n";
	
	mail($to, $subject, $message, $headers);
	//updateOrders($releasesNotInSync);
	updateOrders($omsPKOrdersArray,$omsSHOrdersArray,$omsDLOrdersArray);
} else {
    echo "DBs are in sync for orders placed in last one hour \n \n";
    // subject
    $subject = 'Portal and OMS Sync - CORRECT';
    $message .= "DBs are in sync for orders placed in last one hour";
}

/*********************************************************************************************************************/

function compareOrders($omsPKOrdersArray,$omsSHOrdersArray,$omsDLOrdersArray) {
    $releasesNotInSync = array();
    foreach($omsPKOrdersArray as $omsOrder){
        $releasesNotInSync[$omsOrder] = "\n release_status (lms: PK / IS, oms: WP)";
    }
    foreach($omsSHOrdersArray as $omsOrder){
        $releasesNotInSync[$omsOrder] = "\n release_status (lms: SH / FD, oms: WP / PK)";
    }
    foreach($omsDLOrdersArray as $omsOrder){
        $releasesNotInSync[$omsOrder] = "\n release_status (lms: DL, oms: WP / PK / SH)";
    }
    return $releasesNotInSync;
}

function updateOrders($omsPKOrdersArray,$omsSHOrdersArray,$omsDLOrdersArray) {
    updateSetOfOrders($omsPKOrdersArray, 'PK');
    updateSetOfOrders($omsSHOrdersArray, 'SH');
    updateSetOfOrders($omsDLOrdersArray, 'DL');
}

function updateSetOfOrders($releasesNotInSync, $status) {
    
    $releasesToBeUpdatedArrayArray = array_chunk($releasesNotInSync, 500, false);
    foreach ($releasesToBeUpdatedArrayArray as $releasesToBeUpdatedArray) {
        $commaSeperatedReleaseIds = implode(",", $releasesToBeUpdatedArray);
        echo "\n\n Updating last_modified_on for the following releaseIds - " . $commaSeperatedReleaseIds;
        echo "\n";

        /* Connecting to OMS DB */
        $oms_rw = mysql_connect('192.168.70.23', 'myntraAppDBUser', '9eguCrustuBR', TRUE);

        if (!$oms_rw) {
            die('Could not connect: ' . mysql_error());
        }
        echo "Connected successfully to ERPDB. Changing database to myntra_oms \n";

        if (mysql_select_db('myntra_oms', $oms_rw) === false) {
            echo "Oms db unaccessible \n";
            exit;
        }
        echo "Changed database to myntra_oms \n";

        echo "Connected to OMS database \n";
        if ($status == 'PK') {
            $omsOrderReleaseUpdateQuery = "update order_release set packed_on = now(), status_code = 'PK', last_modified_on = now() where id in ($commaSeperatedReleaseIds)";
        } else if ($status == 'SH') {
            $omsOrderReleaseUpdateQuery = "update order_release set shipped_on = now(), status_code = 'SH', last_modified_on = now() where id in ($commaSeperatedReleaseIds)";
        } else if ($status == 'DL') {
            $omsOrderReleaseUpdateQuery = "update order_release set delivered_on = now(), status_code = 'DL', last_modified_on = now() where id in ($commaSeperatedReleaseIds)";
        }
	echo "\nRunning query: ".$omsOrderReleaseUpdateQuery;
        $q_result = mysql_query($omsOrderReleaseUpdateQuery, $oms_rw);

        echo "\n Executed update query and it returned " . $q_result;
        echo "\n \n Sleeping for 1 minute \n";
        mysql_close($oms_rw);
        sleep(60);
    }
}

?>
