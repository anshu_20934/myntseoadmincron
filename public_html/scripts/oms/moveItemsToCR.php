<?php

include_once("auth.php");
include_once("$xcart_dir/modules/apiclient/LocationApiClient.php");
include_once("$xcart_dir/modules/apiclient/ItemApiClient.php");

$warehouseId = 2;
$curr_status = 'A_R';

$shipped_item_barcodes = array(100003601638);

if($curr_status == 'A_R') {
	$response = "";
} else {
	$response = LocationApiClient::acceptItemsAtWarehouse($shipped_item_barcodes, $warehouseId, 'sanjay.yadav');
}

if($response == "") {
	echo ("Items Accepted Successfully\n");
	$success = ItemApiClient::remove_item_order_association(0, $warehouseId, $shipped_item_barcodes, 'CUSTOMER_RETURNED', false, 'sanjay.yadav@myntra.com');
	if($success)
		echo "Successfully moved items to CR \n";
	else
		echo "Failed to update item status to CR\n";
        
} else {
	echo "Accept Returns call failed \n";
}
?>
