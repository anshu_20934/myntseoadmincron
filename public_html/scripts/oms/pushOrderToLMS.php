<?php

include_once("auth.php");
include_once("$xcart_dir/include/func/func.order.php");
include_once("$xcart_dir/modules/apiclient/ShipmentApiClient.php");

$orderids = array(7125629,7128177);

$orders = func_query("Select * from xcart_orders where orderid in (".implode(",", $orderids).")");

foreach($orders as $order) {
	$response = ShipmentApiClient::pushOrderToLMS($order);				
	if($response === true) {
		echo "Order $order[orderid] has been pushed to LMS.\n";
	} else {
		echo "Failed to push order $order[orderid] to LMS. FailureMessage: - $response \n";
	}
}

?>
