<?php 
    require("auth.php");
    include_once("$xcart_dir/include/func/func.order.php");

        $cancel_args = array(
				'cancellation_type' 			=> 'CCC',
				'reason'				=> 'RTOCR',
				'orderid2fullcancelordermap'	=> array($orderid=>true),
				'generateGoodWillCoupons'		=> false,
				'is_oh_cancellation'			=> false
	);

        $orderids = array(4450499,4585735,4597337,4597330,4601729,4601322,4589014,4491712,4564577,4469545,4566160);

        foreach($orderids as $orderid) {
		func_change_order_status($orderid, 'F','System Script', "Cancelling order as the shipment was lost", '', false, '', $cancel_args);
		echo "Cancelled Order - $orderid\n";
        }

?>
