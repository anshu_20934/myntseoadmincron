<?php

include_once("auth.php");

$count = rand(200, 1700); // This should be a random number

echo "\nTrying to insert $count dummy orders";
$getMaxOrderIdQuery = "select max(orderid) maxorder from mk_temp_order";
$maxOrderId = func_query($getMaxOrderIdQuery);
//Incrementing maxOrderId
$maxOrder = $maxOrderId[0]['maxorder'] + $count;
$currentDateString = date('Y-m-d H:i:s');
$query = "insert into mk_temp_order (orderid, sessionid, parameter, order_crc) values (" . $maxOrder . ", 'dummy', '$currentDateString', $count)";
echo "\nExecuting $query . . .\n";
$response = db_query($query);
if ($response >= 1) {
        echo "\nSuccessfully inserted $count dummy orders into mk_temp_order";
}
?>