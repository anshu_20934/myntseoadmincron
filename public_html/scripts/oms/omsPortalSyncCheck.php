<?php
ini_set('memory_limit', '-1');
echo "Started the comparison programme \n \n";
/*********************************************************************************************************************/
                                      /* Connecting to OMS DB */
$oms_ro = mysql_connect('192.168.70.25','myntraAppDBUser','9eguCrustuBR',TRUE);

if (!$oms_ro) {
    die('Could not connect: ' . mysql_error());
}
echo "Connected successfully to ERPDB. Changing database to myntra_oms \n";

if(mysql_select_db('myntra_oms', $oms_ro ) === false) {
        echo "Oms db unaccessible \n";
        exit;
}
echo "Changed database to myntra_oms \n";

echo "Connected to OMS database \n";

$omsOrderReleaseFetchQuery="select r.id as release_id, r.status_code, r.courier_code, r.tracking_no, r.warehouse_id, r.is_on_hold from order_release r where r.created_on < DATE_SUB(now(), INTERVAL 15 MINUTE) and r.created_on > DATE_SUB(now(), INTERVAL 20 DAY) and (r.status_code in ('RFR','WP','PK','SH','DL', 'C' ,'F') and r.last_modified_on < DATE_SUB(now(), INTERVAL 10 MINUTE) and r.last_modified_on > DATE_SUB(now(), INTERVAL 1 DAY));";
$p_result = mysql_query($omsOrderReleaseFetchQuery, $oms_ro);
$omsOrdersArray = array();
    if ($p_result) {
        while ($row = mysql_fetch_array($p_result, MYSQL_ASSOC)) {
            $omsOrdersArray[$row['release_id']] = $row;
        }
        echo "Fetched ".count($omsOrdersArray)." orders from OMS";
        @mysql_free_result($p_result);
    } else {
	echo "Fetched no orders from OMS";
}
//echo "Entire array from oms: " . print_r($omsOrdersArray);
//mysql_close($oms_ro);

echo "\n*********************************************************************************************************** \n";
/*********************************************************************************************************************/
$checkForOrderids = array_keys($omsOrdersArray);
$commaSeperatedOrderids = implode(",", $checkForOrderids);

                                        /* Connecting to Portal DB */
$portal_ro = mysql_connect('192.168.70.22','myntraAppDBUser','9eguCrustuBR',TRUE);

if (!$portal_ro) {
    die('Could not connect: ' . mysql_error());
}
echo "Connected successfully to Portal DB. Changing database to myntra \n";
if(mysql_select_db('myntra', $portal_ro ) === false) {
        echo "Portal db unaccessible \n";
        exit;
    }
echo "Connected to Portal database \n";

$portalOrderReleaseFetchQuery = "select orderid, status, on_hold_reason_id_fk, warehouseid, 
    courier_service, tracking from xcart_orders where orderid in ($commaSeperatedOrderids)";
$p_result = mysql_query($portalOrderReleaseFetchQuery, $portal_ro);
$portalOrdersArray = array();
if ($p_result) {
	while ($row = mysql_fetch_array($p_result, MYSQL_ASSOC)) {
            $portalOrdersArray[$row['orderid']] = $row;
        }
        echo "Fetched ".count($portalOrdersArray)." orders from Portal";
        mysql_free_result($p_result);
} else {
	echo "Fetched no orders from Portal \n";
}
//echo "Entire array from portal: \n".print_r($portalOrdersArray, true);
mysql_close($portal_ro);
echo "\n ********************************************************************************************************* \n";
/*********************************************************************************************************************/
                                      /* Comparing order params across systems*/

$releasesNotInSync = compareOrders($omsOrdersArray,$portalOrdersArray);
echo "Ended the comparison program \n Number of orders not in sync: " . count($releasesNotInSync) . "\n";
echo "Checked for data sync of ".count(array_keys($omsOrdersArray)). " orders in last 1 day \n";
// message
$message = '
    <html>
    <head>
      <title>Data sync result of orders in last 1 day</title>
    </head>
    <body>
      <p>Data sync result for orders created in last 7 day and updated in last 1 day<br/>
	Fetched '.count($omsOrdersArray).' orders from OMS<br/>
        Fetched '.count($portalOrdersArray).' orders from Portal<br/>
	Following releases are not in sync : '.count($releasesNotInSync).'</p>
    </body>
    </html>';

if(count($releasesNotInSync) > 0){
    //echo print_r($releasesNotInSync);
    // subject
    $subject = 'Portal and OMS Sync - Inconsistency';
    $commaSeparatedOrders = "";
    $arrayForEmail = array();
    foreach ($releasesNotInSync as $releaseid => $paramsNotInSync){
	//echo $paramsNotInSync . "\n";
        $paramsNotInSyncString = str_replace("\n", "", $paramsNotInSync);
	//echo $paramsNotInSyncString . "\n";
        array_push($arrayForEmail,$releaseid . " " . $paramsNotInSyncString);
    }
    $message .= "\r\n\n".implode("<br/>", $arrayForEmail);
	// send email for orders out of sync
	//echo $message;
	$to  = 'pavankumar.at@myntra.com';
	// To send HTML mail, the Content-type header must be set
	$headers  = 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

	// Additional headers
	//$headers .= 'To:  engg_erp_oms@myntra.com' . "\r\n";
	$headers .= 'From: pavankumar.at@myntra.com' . "\r\n";
	$headers .= 'Cc: ' . "\r\n";
	$headers .= 'Bcc: ' . "\r\n";
	
	mail($to, $subject, $message, $headers);
	//updateOrders($releasesNotInSync);
	updateOrders($releasesNotInSync);
} else {
    echo "DBs are in sync for orders placed in last one hour \n \n";
    // subject
    $subject = 'Portal and OMS Sync - CORRECT';
    $message .= "DBs are in sync for orders placed in last one hour";
}

/*********************************************************************************************************************/
/*$omsQueuedForLongOrder = "select o.id from ((orders o JOIN order_release r on o.id = r.order_id_fk) JOIN order_additional_info ori on 
    r.order_id_fk = ori.order_id_fk) where ori.`key`= 'ORDER_PROCESSING_FLOW' and ori.value = 'OMS' and r.status_code = 'Q' and r.last_modififed_on < DATE_SUB(now(), INTERVAL 15 MINUTE) and r.is_on_hold = 0 and o.is_on_hold = 0";

$q_result = mysql_query($omsQueuedForLongOrder, $oms_ro);
$queuedOrdersArray = array();
if ($q_result) {
        while ($row = mysql_fetch_array($q_result, MYSQL_ASSOC)) {
            $queuedOrdersArray[] = $row['id'];
        }
        echo "Found following orders to be in Q status for more than 15 mins ".print_r($queudOrdersArray, true);
    // subject
    $subject = 'Portal and OMS Sync - ERROR';
    
    $message = "Following Orders are in queued status for more than 15 minutes and not on hold.\r\n\n".implode("<br />", $queudOrdersArray);

	// send email for orders out of sync
	$to  = 'omsoncall@myntra.com';
	// To send HTML mail, the Content-type header must be set
	$headers  = 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

	// Additional headers
	//$headers .= 'To:  engg_erp_oms@myntra.com' . "\r\n";
	$headers .= 'From: vishal.sharma@myntra.com' . "\r\n";
	$headers .= 'Cc: ' . "\r\n";
	$headers .= 'Bcc: ' . "\r\n";

	mail($to, $subject, $message, $headers);
        mysql_free_result($q_result);
} else {
        echo "No order in Q status for more than 15 mins \n";
}*/
mysql_close($oms_ro);

/*********************************************************************************************************************/

function compareOrders($omsOrdersArray,$portalOrdersArray) {
    $releasesNotInSync = array();
    foreach($omsOrdersArray as $key => $omsRowDetail){
        $portalRowDetail = $portalOrdersArray[$key];
	if ($portalRowDetail == null || !$portalRowDetail) {
		$releasesNotInSync[$key] = "\n release_status (oms: " . $omsRowDetail['status_code'] . ", portal: Release not available)";
		continue;
	}
        //echo "for release id: $key \n";
        //echo "data in OMS db is: \n";
        //echo print_r($omsRowDetail);
        //echo "data in portal db is: \n";
        //echo print_r($portalRowDetail);
        //echo "****************************************************\n";
        /*if( $omsRowDetail['order_onhold'] == 1 || $omsRowDetail['release_onhold'] == 1 ) {
	    if($omsRowDetail['release_onhold'] == 1 && ($omsRowDetail['release_oh_reason'] == 27 || $omsRowDetail['release_oh_reason'] == 10)){
		if($portalRowDetail['status'] != 'OH'){
			$releasesNotInSync[$key][] = "\n release_status (oms: On Hold, portal: $portalRowDetail[status])";
		}
	    } 
	    else {
		if($omsRowDetail['order_onhold'] == 1 && ($omsRowDetail['on_hold_reason_id_fk'] != $portalRowDetail['on_hold_reason_id_fk'])){
            		$releasesNotInSync[$key][] = " on_hold_reason_id_fk (oms: $omsRowDetail[on_hold_reason_id_fk], portal: $portalRowDetail[on_hold_reason_id_fk])";
		}
		if($omsRowDetail['order_onhold'] == 1 && $portalRowDetail['status'] != 'OH') {
			$releasesNotInSync[$key][] = "\n release_status (oms: On Hold, portal: $portalRowDetail[status])";
		}
	    }
        } else {
	    if($omsRowDetail['status_code'] != $portalRowDetail['status']) {
		$releasesNotInSync[$key][] = "\n release_status (oms: $omsRowDetail[status_code], portal: $portalRowDetail[status])";
	    }
	}

        if($omsRowDetail['warehouse_id'] != $portalRowDetail['warehouseid']){
            $releasesNotInSync[$key][] = "\n warehouse_id (oms: $omsRowDetail[warehouse_id], portal: $portalRowDetail[warehouseid])";
        }
        if($omsRowDetail['tracking_no'] != $portalRowDetail['tracking']){
            $releasesNotInSync[$key][] = "\n tracking number (oms: $omsRowDetail[tracking_no], portal: $portalRowDetail[tracking])";
        }
        if($omsRowDetail['courier_code'] != $portalRowDetail['courier_service']){
            $releasesNotInSync[$key][] = "\n courier_service (oms: $omsRowDetail[courier_code], portal: $portalRowDetail[courier_service])";
        }
        if(array_key_exists($key, $releasesNotInSync)){
            $releasesNotInSync[$key] = implode(",", $releasesNotInSync[$key]);
	    $releasesNotInSync[$key] .= "\n****************************************************\n";
        }*/
	if(($omsRowDetail['status_code'] != $portalRowDetail['status'] && $omsRowDetail['status_code'] != 'Q') || ($omsRowDetail['status_code'] == 'Q' && $omsRowDetail['is_on_hold'] == 1 && $portalRowDetail['status'] != 'OH')) {
		$releasesNotInSync[$key] = "\n release_status (oms: " . $omsRowDetail['status_code'] . ", portal: " . $portalRowDetail['status'] . ")";
	    }
    }
    return $releasesNotInSync;
}

function updateOrders($releasesNotInSync) {
	$releasesToBeUpdated = array_keys($releasesNotInSync);
	$releasesToBeUpdatedArrayArray = array_chunk($releasesToBeUpdated, 500, false);
	foreach($releasesToBeUpdatedArrayArray as $releasesToBeUpdatedArray) {
	$commaSeperatedReleaseIds = implode(",", $releasesToBeUpdatedArray);
	echo "\n\n Updating last_modified_on for the following releaseIds - ".$commaSeperatedReleaseIds;
	echo "\n";

	/* Connecting to OMS DB */
	$oms_rw = mysql_connect('192.168.70.23','myntraAppDBUser','9eguCrustuBR',TRUE);

	if (!$oms_rw) {
		die('Could not connect: ' . mysql_error());
	}
	echo "Connected successfully to ERPDB. Changing database to myntra_oms \n";

	if(mysql_select_db('myntra_oms', $oms_rw ) === false) {
        	echo "Oms db unaccessible \n";
        	exit;
	}
	echo "Changed database to myntra_oms \n";

	echo "Connected to OMS database \n";

	$omsOrderReleaseUpdateQuery = "update order_release set last_modified_on = now() where id in ($commaSeperatedReleaseIds)";

	$q_result = mysql_query($omsOrderReleaseUpdateQuery, $oms_rw);

	echo "\n Executed update query and it returned ".$q_result;
	echo "\n \n Sleeping for 2 minutes \n";
	mysql_close($oms_rw);
	sleep(100);
	}
}
?>
