<?php

include_once("auth.php");
include_once("$xcart_dir/modules/apiclient/TatApiClient.php");

include_once("$xcart_dir/include/class/EventCreationManager.php");
$orderids = array(19123159,19123409,19123656,19124009,19124499,19124500,19124503,19124514,19124884);

foreach ($orderids as $orderId) {
        $order_details = func_query_first("select * from xcart_orders where orderid = $orderId");
        echo "Fetching promise date for. zipcode: " . $order_details['s_zipcode'] . ", warehouseid: " . $order_details['warehouseid'] . ", courier: " . $order_details['courier_service'] . " and shipping method: " . $order_details['shipping_method'];
        $promiseDateEntries = \TatApiClient::getPromiseDate($order_details['s_zipcode'], $order_details['warehouseid'], $order_details['courier_service'], $order_details['shipping_method']);
        // Stamp the promise date entries into order_additional_info
        echo "Stamped the following promise date for order: " . $orderId;
        //echo print_r($promiseDateEntries);
        $updatePromiseDateQuery = "insert into order_additional_info(order_id_fk,`key`,value,created_on) values"
                . " ($orderId,'EXPECTED_PICKING_TIME','" . $promiseDateEntries['pickByCutOff'] . "',now()),"
                . " ($orderId,'EXPECTED_QC_TIME','" . $promiseDateEntries['qcByCutOff'] . "',now()),"
                . " ($orderId,'EXPECTED_PACKING_TIME','" . $promiseDateEntries['packByCutOff'] . "',now()),"
                . " ($orderId,'EXPECTED_CUTOFF_TIME','" . $promiseDateEntries['shippingCutOff'] . "',now()),"
                . " ($orderId,'CUSTOMER_PROMISE_TIME','" . $promiseDateEntries['promiseDate'] . "',now()) on duplicate key update `value` = values (`value`)";
        echo $updatePromiseDateQuery;
        db_query($updatePromiseDateQuery);
        \EventCreationManager::pushCompleteOrderEvent($orderId);
}


?>

