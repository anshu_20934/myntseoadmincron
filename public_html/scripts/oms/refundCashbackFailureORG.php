<?php

include_once("../auth.php");
include_once "$xcart_dir/modules/myntCash/MyntCashService.php";
include_once "$xcart_dir/modules/myntCash/MyntCashTransaction.php";

include_once "$xcart_dir/include/func/func.order.php";
include_once "$xcart_dir/include/class/mcart/class.MCart.php";

echo "========================== auto cashback/MyntCash credit script ============================\n";

$startTime =  date("Y-m-d H:i:s", strtotime(time()));

echo "========================== start time : $startTime ============================\n";

$filepath = $_SERVER["argv"][1];

$ft = fopen($filepath,"r");


while(!feof($ft))	{
	$line = fgets($ft);
	if ($line != "") {
		$array = explode(",", trim($line));
		
		if($array[0]==""||empty($array[0])){
			continue;
		}
		$login = $array[0];
		$itemid = $array[1];
		$amount = $array[2];
		$description = $array[3];
		
		$trs = new MyntCashTransaction($login, MyntCashItemTypes::ITEM_ID, $itemid, MyntCashBusinessProcesses::RETURN_PRODUCT ,0 /* earned credit to be credited */,$amount /* store credit to be given */, 0, $description);
		
		//$trs = new MyntCashTransaction($login, MyntCashItemTypes::ITEM_ID, $itemid, MyntCashBusinessProcesses::RETURN_PRODUCT ,200 /* earned credit to be credited */,0 /* store credit to be given */, 0, $description);
		//**********  $trs = new MyntCashTransaction($login, MyntCashItemTypes::GOOD_WILL_ID, 100, MyntCashBusinessProcesses::GOOD_WILL , 0,0, 200 /* debit amount*/, "welcome cashback expiry debit"); //** for debit call

		$resp = MyntCashService::creditToUserMyntCashAccount($trs);
		
		//*****  $resp = MyntCashService::debitFromUserMyntCashAccount($trs); //** for debit call
		
		if($resp){
			echo "success : $login \n";
			print_r($resp);echo"\n";
		}else{
			echo "failure : $login ============\n";
		}
		
		
	}
}
fclose($ft);

$endTime =  date("Y-m-d H:i:s", strtotime(time()));

echo "========================== end time : $endTime ============================\n";

