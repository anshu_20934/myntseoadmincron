<?php
/*

Procedure to use this script
php php style_delist.php <csv file containing style-ids comma separated without spaces>
*/

chdir(dirname(__FILE__));
include_once("../auth_for_solr_index.php");
include_once("../include/solr/solrProducts.php");
if(empty($argv[1])){
 die("Usage: php style_delist_curated.php <csv file containing style-ids comma separated without spaces> ");
}
$solrProducts = new SolrProducts();
$start_time = time();
$csvfile="$argv[1]";
$file = fopen("$csvfile","r");

$styles = fgetcsv($file);
fclose($file);

$stylesCsv = implode(",",$styles);
$sql = "UPDATE mk_product_style ps inner join mk_style_properties sp on ps.id = sp.style_id set ps.styletype = 'A',sp.modified_date=unix_timestamp(now()) where ps.id in ($stylesCsv)";
echo "\nExecuting query: ".$sql;
db_query($sql,false);
echo "\nClearing Cache of Catalog Service:\n";
echo "For E7:". file_get_contents("http://192.168.70.84:7072/myntra-catalog-service/product/clearallcache"). "\n";
echo "For E9:". file_get_contents("http://192.168.70.76:7072/myntra-catalog-service/product/clearallcache") . "\n";
echo "\nReindexing styles in Solr:\n";
foreach($styles as $eachStyle){
 echo($eachStyle." ");
 $solrProducts->deleteStyleFromIndex($eachStyle,true,false);
}

$end_time=time();

$diff_time= ($end_time - $start_time) / 60;
echo "\n=========== Total time :: ".$diff_time." ==================\n";

?>
