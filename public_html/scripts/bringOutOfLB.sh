#! /bin/sh
if [ "$1" == "" ]
then
	service='portal'
else
	service=$1
fi
service_string="[']$service['],"
if [ "$2" == "" ]
then
	file='tester.php'
else
	file=$2
fi
if [ `grep  $service_string $file |grep ^# |wc -l` -ge 1 ]
then
	echo "bringning out $service"
	sed -i -e"/$service_string/s/#\+//" $file
else
	echo "$service already out"
fi
