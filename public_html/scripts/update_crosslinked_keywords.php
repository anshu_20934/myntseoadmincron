<?php
include_once(dirname(__FILE__)."/../auth_for_solr_index.php");
include_once("../include/solr/solrProducts.php");
include_once(dirname(__FILE__)."/..//modules/seo/CrossLinkKeyword.php");

$cKeyword = new CrossLinkKeyword();

function replaceEmpty($val) {
 return ($val != "_empty_");
}

function addIfNotAlreadyExists($keyword) {
	global $cKeyword;
	$k =  preg_replace('/\s+/',' ',trim($keyword));
    $query_data = array("crosslink_keyword"=>$k);
	$keywordInfo = $cKeyword->getKeyword(array("crosslink_keyword"=>$k));
	
	if(empty($keywordInfo))	{
//		echo "Keyword: ".$k."\n";
		$cKeyword->addKeyword($query_data);
	}

}

$solrProducts = new solrProducts();
$categoryList = array();

$facetFields = array('global_attr_master_category_facet',
							'global_attr_sub_category_facet',
							'global_attr_article_type_facet',
							'brands_filter_facet');
	
$out = $solrProducts->multipleFacetedSearch("*:*", $facetFields)->facet_fields;
$out = (array)$out;
$merged = array_merge((array) $out['global_attr_master_category_facet'], (array) $out['global_attr_sub_category_facet']);
$merged = array_merge ((array) $merged, (array) $out['global_attr_article_type_facet']);

// using only master category for now
$categoryList = array_keys($merged);
//var_dump($categoryList);

foreach ($categoryList as $cat) {
	if ($cat == "_empty_") continue;
	addIfNotAlreadyExists(strtolower($cat));
	
	$out= $solrProducts->facetedSearch("global_attr_article_type_facet: $cat OR global_attr_sub_category_facet: $cat OR global_attr_master_category_facet: $cat",'brands_filter_facet');
	$out = (array)$out;
	$brandsList = array_keys((array)$out['brands_filter_facet']);
	$brandsList = array_filter($brandsList, "replaceEmpty");
//	echo "cat: $cat";
//	var_dump($brandsList);	
	foreach ($brandsList as $brand) {
	    addIfNotAlreadyExists(strtolower($brand));
		addIfNotAlreadyExists(strtolower($brand." ".$cat));
	}
}

echo"\n Job Done ";
?>
