<?php
chdir(dirname(__FILE__));
include_once("../auth_for_solr_index.php");
include_once ("$xcart_dir/modules/coupon/database/CouponAdapter.php");
$fp = fopen("invalid_email.log", "w");
$ft = fopen("extraCouponsGenerated","r");

/**
Validate an email address.
Provide email address (raw input)
Returns true if the email address has the email 
address format and the domain exists.
*/
function validEmail($email)
{
   $isValid = true;
   $atIndex = strrpos($email, "@");
   if (is_bool($atIndex) && !$atIndex)
   {
      $isValid = false;
   }
   else
   {
      $domain = substr($email, $atIndex+1);
      $local = substr($email, 0, $atIndex);
      $localLen = strlen($local);
      $domainLen = strlen($domain);
      if ($localLen < 1 || $localLen > 64)
      {
         // local part length exceeded
         $isValid = false;
      }
      else if ($domainLen < 1 || $domainLen > 255)
      {
         // domain part length exceeded
         $isValid = false;
      }
      else if ($local[0] == '.' || $local[$localLen-1] == '.')
      {
         // local part starts or ends with '.'
         $isValid = false;
      }
      else if (preg_match('/\\.\\./', $local))
      {
         // local part has two consecutive dots
         $isValid = false;
      }
      else if (!preg_match('/^[A-Za-z0-9\\-\\.]+$/', $domain))
      {
         // character not valid in domain part
         $isValid = false;
      }
      else if (preg_match('/\\.\\./', $domain))
      {
         // domain part has two consecutive dots
         $isValid = false;
      }
      else if (!preg_match('/^(\\\\.|[A-Za-z0-9!#%&`_=\\/$\'*+?^{}|~.-])+$/', str_replace("\\\\","",$local)))
      {
         // character not valid in local part unless 
         // local part is quoted
         if (!preg_match('/^"(\\\\"|[^"])+"$/',
             str_replace("\\\\","",$local)))
         {
            $isValid = false;
         }
      }
      if ($isValid && !(checkdnsrr($domain,"MX") || checkdnsrr($domain,"A")))
      {
         // domain not found in DNS
         $isValid = false;
      }
   }
   return $isValid;
}

function generateCoupon($login,$adapter) {
	$mrpAmount = 250.00;
	$minCartValue = 1000.00;
	$startDate = strtotime(date('d F Y', time()));
	$validity = 30;
	$endDate = $startDate + $validity * 24 * 60 * 60;
	$couponCode = $adapter->generateSingleCouponForUser("New", "8", "FirstLogin", $startDate, $endDate, $login, 'absolute', $mrpAmount, '', $minCartValue, "Registration on Myntra - Rs. 250 coupon");
	
	// Logging success of first login of mrp.
	/*$toInsert = array();
	$toInsert['ruleid'] = '0'; //ask what to keep here
	$toInsert['rulename'] = 'old_customers';//ask what to keep here
	$toInsert['timestamp'] = time();
	$toInsert['appliedOn'] = $login;
	$toInsert['result'] = '1';
	$toInsert['coupon'] = $couponCode;
	$toInsert['couponUser'] = $login;
	func_array2insert("mk_mrp_log", $toInsert);*/
	$_curtime = time();
	$query = "INSERT INTO mk_mrp_log (ruleid,rulename,timestamp,appliedOn,result,coupon,couponUser) VALUES ('1','first_login','$_curtime','$login','1','$couponCode','$login')";
	mysql_query($query);
	
	// Add customer-coupon mapping.
	/*$toInsert = array();
	$toInsert['login'] = $login;
	$toInsert['coupon'] = $couponCode;
	func_array2insert("mk_mrp_coupons_mapping", $toInsert);*/
	$query = "INSERT INTO mk_mrp_coupons_mapping (login,coupon) VALUES ('$login','$couponCode')";
	mysql_query($query);
}

$customers_email = array();
while(!feof($ft))	{
	$line = fgets($ft);
	if ($line != "") {
		$customers_email[] = trim($line);
	}
}

$generated_coupons = 0;
echo "Started \n";
$adapter = CouponAdapter::getInstance();
mysql_query("BEGIN");
foreach($customers_email as $email) {
	if(validEmail($email)) {
		//echo "$email is a valid email\n";
		$fb_check = "select count(*) from mk_facebook_user where myntra_login ='".mysql_real_escape_string($email)."'";
		$fb_exists = func_query_first_cell($fb_check);
		$coupon_check = "select count(*) from xcart_discount_coupons where groupName='FirstLogin' and users='".mysql_real_escape_string($email)."'";
		$coupon = func_query_first_cell($coupon_check);
		if($fb_exists>0) {
			//fb user
			for ($i = 0; $i < 3 - $coupon; $i++) {
				generateCoupon(mysql_real_escape_string($email),$adapter);
				$generated_coupons++;
			 }
			
		} else {
			//normal myntra user
			 for ($i = 0; $i < 2 - $coupon; $i++) {
				generateCoupon(mysql_real_escape_string($email),$adapter);
				$generated_coupons++;
			 }
		}
	
	} else {
		$line="$email is NOT a valid email\n";
        fputs($fp, $line);
	}
}
mysql_query("COMMIT");
fclose($fp);
fclose($ft);
echo "Finished \n";
echo "Generated ".$generated_coupons." coupons!\n";
?>