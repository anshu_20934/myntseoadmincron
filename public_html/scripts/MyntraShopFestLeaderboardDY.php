<?php
/*
 *Coupon expiry mailer script to upload mailing list to icubes
 * 
 */

require_once "../admin/auth.php";

echo "cron begins ================================\n";

$stDate = (date('Y-m-d', time())." -3 day");
$endDate = (date('Y-m-d', time()));
$startTime = strtotime($stDate);
$endTime = strtotime($endDate);

$delQuery = "DELETE FROM shopping_fest_leader_board where date >= '".$stDate."' and date<'". $endDate."'";

db_query($delQuery);

$query = " INSERT INTO `shopping_fest_leader_board` (`login`,`category`,`name`,`amount`,`order_count`,`date`) ";
$query .= "(select o.login, 1, concat(c.firstname,' ',c.lastname) as name, sum(od.price - od.discount - od.coupon_discount_product) as amount, count(distinct o.orderid) as orders, from_unixtime(date,'%Y-%m-%d') 
from xcart_orders o 
inner join xcart_customers c on o.login = c.login
inner join xcart_order_details od on o.orderid = od.orderid
inner join mk_style_properties sp on od.product_style = sp.style_id
left join cancellation_codes cc on o.cancellation_code=cc.id 
where  date >= $startTime and date < $endTime and
sp.global_attr_gender in ('Men', 'Unisex') and 
(o.status in ('OH','Q','WP','SH','DL','C','PK')  
or (o.status = 'F' and cc.cancel_type!='CCR')) 
group by o.login) 
union 
(select o.login, 2, concat(c.firstname,' ',c.lastname) as name, sum(od.price - od.discount - od.coupon_discount_product) as amount, count(distinct o.orderid) as orders, from_unixtime(date,'%Y-%m-%d') 
from xcart_orders o 
inner join xcart_customers c on o.login = c.login
inner join xcart_order_details od on o.orderid = od.orderid
inner join mk_style_properties sp on od.product_style = sp.style_id
left join cancellation_codes cc on o.cancellation_code=cc.id 
where  date >= $startTime and date < $endTime and
sp.global_attr_gender in ('Women', 'Unisex') and 
(o.status in ('OH','Q','WP','SH','DL','C','PK')  
or (o.status = 'F' and cc.cancel_type!='CCR')) 
group by o.login)
union
(select o.login, 3, concat(c.firstname,' ',c.lastname) as name, sum(od.price - od.discount - od.coupon_discount_product) as amount, count(distinct o.orderid) as orders, from_unixtime(date,'%Y-%m-%d')
from xcart_orders o 
inner join xcart_customers c on o.login = c.login
inner join xcart_order_details od on o.orderid = od.orderid
inner join mk_style_properties sp on od.product_style = sp.style_id
left join cancellation_codes cc on o.cancellation_code=cc.id 
where  date >= $startTime and date < $endTime and
(o.status in ('OH','Q','WP','SH','DL','C','PK')  
or (o.status = 'F' and cc.cancel_type!='CCR')) 
group by o.login);";


$results = db_query($query);

echo "cron ends ===================================================\n";
