<?php
/*

Procedure to use this script
php product_index.php shopmasterid1 shopmasterid2 shopmasterid3   

*/

chdir(dirname(__FILE__));
include("../auth_for_solr_index.php");
include("../include/solr/solrProducts.php");
set_time_limit(0);

if ($_SERVER["argc"] > 1) {
    $mail_detail = array(
                        "to"=>'ramakant.sharma@myntra.com,amit.shukla@myntra.com',
                        "subject"=>'Solr indexing started on '.$http_location. '<eom>',
                        "content"=>'',
                        "from_name"=>'Myntra Admin',
                        "from_email"=>'admin@myntra.com',
                        "header"=>'',
                    );
    send_mail_on_domain_check($mail_detail);
    for ($i = 1; $i < $_SERVER["argc"]; $i++) {
        echo "Building index for shopmaster - ".$_SERVER["argv"][$i];
        $solrProducts = new solrProducts();
        $solrProducts->indexProducts($_SERVER["argv"][$i]);
    }
    $mail_detail['subject'] = 'Solr indexing end on '.$http_location. '<eom>'; 
    send_mail_on_domain_check($mail_detail);
}
?>

