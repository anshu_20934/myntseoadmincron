<?php

$dirName = dirname(__FILE__);
chdir($dirName);
include_once("../auth.php");
require_once '../env/DB.config.php';
require_once($xcart_dir."/include/func/func.utilities.php");
require_once($xcart_dir."/include/func/func.core.php");
include_once \HostConfig::$documentRoot ."/utils/throttleUpdatesDb.php";
define('ALLOWABLETAGS','<h1><h2><h3><h4><h5><h6><em><strong><br/><b><p><br>');
define('REMOVABLETAGS','a');
define('DBSERVER', DBConfig::$rw['host']);
define('DBUSER', DBConfig::$rw['user']);
define('DBPASS', DBConfig::$rw['password']);
define('DBNAME', DBConfig::$rw['db']);
$linkLimitValue = WidgetKeyValuePairs::getWidgetValueForKey("searchpageLinkLimit");
if(!$linkLimitValue){
    $linkLimitValue = 7;
}
define('LINKLIMIT', $linkLimitValue);

function getTotalNumberOfDesc(){
    $sql = "select count(id) from adseo_pagebodywidget where page_description IS NOT NULL and trim(page_description) != '' and block_crosslinking='0'";
    return func_query_first_cell($sql, true);
}

function getPageDescriptions($offset){
    $sql = "SELECT pbw.id,pbw.page_description,pr.url from adseo_pagebodywidget as pbw, adseo_pagerule as pr WHERE pbw.page_description IS NOT NULL AND trim(pbw.page_description) != '' AND pr.id = pbw.pagerule_id and pbw.block_crosslinking='0' ORDER BY pbw.id LIMIT 10 OFFSET ".$offset;
    return func_query($sql, true);
}

function getAllHandPickedKeywords(){
     $sql = "(SELECT crosslink_keyword as keyword,IFNULL(crosslink_keyword_url, replace(crosslink_keyword,' ','-')) as url FROM mk_crosslink_keyword WHERE is_active='1') union distinct (SELECT keyword,replace(url,'www.myntra.com/','') FROM adseo_sitemapkeyword where search_volume>100 and pattern != 'b')";
       return func_query($sql, true);
    
}

function buildKeywordLinkWordCountHash($keyword,$url=""){
    $link = buildLinkForKeyword($keyword, $url);
    return array('keyword' => $keyword, 'link'=> $link, 'wc' =>str_word_count($keyword));
}

function buildLinkForKeyword($keyword,$url){
    global $http_location_cli;
    $pattern = empty($url) ? str_replace(" ", "-", strtolower($keyword)) : $url;
    return "<a href='".$http_location_cli."/".$pattern."?src=desc' class='seolink'>".$keyword."</a>";
}

function getLinkableFilter(){
    $linkedKeywords = array();

    $HK = getAllHandPickedKeywords();
    if($HK){
        foreach($HK as $index=>$val){
            $ckeyword = $val['keyword'];
            if(!empty($ckeyword)){
                $linkedKeywords[] = buildKeywordLinkWordCountHash($ckeyword,$val['url']);
            }
        }
    }
    return $linkedKeywords;
}

function sortLinkableKeywords($linkedKeywords){
    $sortedKeywords = array();
    $sortedKeywordsWithWC = quickSortRecursive( $linkedKeywords, $left = 0 , $right = NULL, 'wc' );
    $sortedKeywords = array_reverse($sortedKeywordsWithWC);
    return $sortedKeywords; 
}

function getSortedLinkedFilters(){
    $filters = getLinkableFilter();
    $sortedLinkedKeywords = sortLinkableKeywords($filters);
    return $sortedLinkedKeywords;
}

function applyCrosslinking($text,$path,$keywords){
    global $replaced,$replacements;
       $replaced = array();    
    $charsToEscape = "/";
    $search = array();
    foreach($keywords as $index=>$word){
                usleep(150);
        $link = $word['link'];
        $kw = $word['keyword'];
        if(strpos($link, $path) == false ){
            $searchPattern = preg_quote($kw,$charsToEscape);
            $searchPattern = "/\b(".$searchPattern.")\b(?!(?:(?!<\/?[ha].*?>).)*<\/[ha].*?>)(?![^<>]*>)/i";
            $search[$index] = $searchPattern;
            $replaced[strtolower($kw)] = $link;
        }
    }
    $replacements = 0;
    $text = preg_replace_callback($search, function($matches) {
        global $replaced, $replacements;
        $returnString = $matches[0];
         if($replacements < LINKLIMIT){
             $replacements++;
             $matched = $replaced[strtolower($matches[0])];
             $returnString =  preg_replace('/>'.$matches[0].'</i','>'.$matches[0].'<',$matched, 1);
         }
         return $returnString;
    }, $text, 1, &$count);
    return $text;
}

function makePDPDescriptionCrossLinked($desc,$keywords,$id,$path,$columnName, $tableName){
    $crosslinkedDescription= applyCrosslinking($desc,$path,$keywords);
    $queryData = array($columnName=>$crosslinkedDescription);
    return query_func_array2update($tableName, sanitizeDBValues($queryData),"id='{$id}'");
}

function updateDescriptions($crossLinkedDescriptions){
    $connection = \mysql_connect(DBSERVER, DBUSER, DBPASS);
    mysql_set_charset('utf8',$connection);
    @\mysql_select_db(DBNAME) or die( "Unable to select database");
    $count = 0;
    foreach( $crossLinkedDescriptions as $query ){
        $count++;
        $result = \mysql_query($query,$connection);
        if (!$result) {
            echo $query;
        }
    }
    return $batchDBQuery;
}

function crosslinkPageDescriptions($pageDescriptions,$keywords){
    $crossLinkedDescriptions = array();
    $cnt = 0;
    foreach($pageDescriptions as $pageDescription){
        $desc = stripOnlyTags($pageDescription['page_description'],REMOVABLETAGS);
        $pbwId = $pageDescription['id'];
        $pageURl = parse_url($pageDescription['url']);
        $path = $pageURl["path"];
        $crossLinkedDescriptions[] =
 makePDPDescriptionCrossLinked($desc,$keywords,$pbwId,$path,'page_description','adseo_pagebodywidget');
    }
    updateDescriptions($crossLinkedDescriptions);
}

function getPageDescAndCrosslink($i,$sortedKeywords){
        $time_desc = microtime(true);
        $pageDescriptions = getPageDescriptions($i);
        $time_taken_desc = microtime(true) - $time_desc;
        $time_cross = microtime(true);
        crosslinkPageDescriptions($pageDescriptions,$sortedKeywords);
        $time_taken_cross = microtime(true) - $time_cross;
        var_dump("Time for crosslinkProductDescriptions : ".$time_taken_cross);
}


var_dump(date("F j, Y, g:i a"));
$start = microtime(true);
$sortedKeywords = getSortedLinkedFilters();
$time_taken_sf = microtime(true) - $start;
var_dump("Time for sorted filters : ".$time_taken_sf);
$desc_count = getTotalNumberOfDesc();
var_dump(" Page description count : ".$desc_count);
for($i=0; $i < $desc_count; $i +=10){
    var_dump("i : ".$i);
    getPageDescAndCrosslink($i,$sortedKeywords);
}
$time_taken = microtime(true) - $start;
var_dump("Total time taken for script : ".$time_taken);
var_dump(date("F j, Y, g:i a"));
