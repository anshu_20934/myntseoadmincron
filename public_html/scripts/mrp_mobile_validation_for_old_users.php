<?php
chdir(dirname(__FILE__));
include_once("../auth_for_solr_index.php");

$offset =0;
$limit = 100;
echo "Started with OFFSET: $offset \n";
$totalNumbers = func_query_first_cell("select count(*) from cash_on_delivery_info_dump");
for($i=$offset;$i<$totalNumbers;) {
	echo "Current OFFSET: $offset \n";
	$sql = "select * from cash_on_delivery_info_dump limit $limit offset $offset";
	$customers_data = func_query($sql);
	foreach($customers_data as $data) {
		$email = $data['login_id'];
		$mobile= $data['contact_number'];
		
		$toMobMapInsert = array();
		$toMobMapInsert['mobile'] = $mobile;
		$toMobMapInsert['login'] = $email;
		$toCustomerUpdate = array();
		$toCustomerUpdate['mobile'] = $mobile;
		
		$check_dups = func_query_first_cell("select count(*) from cash_on_delivery_dups where login1 ='$email'");
		if($check_dups>0) {
			//duplicate entry
			$toMobMapInsert['status'] = "U";
		} else {
			//unique
			$toMobMapInsert['status'] = "V";
			$toMobMapInsert['date_verified'] = time();
		}
		
		$alreadyPresent = func_query_first_cell("select count(*) from mk_mobile_mapping where login ='$email' and status='V'");
		if($alreadyPresent>0) {
			//do the last one to O if it was V
			$updateQuery = "update mk_mobile_mapping set status='O' where login ='$email' and status='V'";
			func_query($updateQuery);
		}
		
		func_array2insert("mk_mobile_mapping", $toMobMapInsert,true);
		func_array2update("xcart_customers", $toCustomerUpdate,"login='$email'");		
	}
	$offset = $offset+$limit;
	$i=$i+$limit;
}
echo "Finished at OFFSET: $offset \n";
?>