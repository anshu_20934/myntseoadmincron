<?php
include_once("../auth.php");
include_once($xcart_dir."/modules/discount/DiscountConfig.php");
require_once $xcart_dir."/include/solr/solrProducts.php";
set_time_limit(0);

echo "Creating new discount rules started\n";
$optionsQuery = "select sp.global_attr_brand as brand, cc.typename as articletype, "; 
$optionsQuery.= "sp.global_attr_fashion_type as fashion from mk_style_properties sp, mk_catalogue_classification cc "; 
$optionsQuery.= "where sp.global_attr_article_type=cc.id group by brand, articletype, fashion";
$optionResults = func_query($optionsQuery);

foreach($optionResults as $data){
	$brandName = $data['brand'];
	$articleTypeName = $data['articletype'];
	$fashionTypeName = $data['fashion'];
	$res = func_query("select * from mk_discount_rules where brand='$brandName' and articletype='$articleTypeName' and fashion='$fashionTypeName'");
	if(!empty($res)){
		continue;
	}
	$algoid = DiscountConfig::FLAT_DISCOUNT_ID;
	$data = array("isPercentage"=>1, "value"=>0);
	$data = mysql_real_escape_string(json_encode($data));
	echo "Created new discount rule $brandName - $articleTypeName - $fashionTypeName\n";
	$createRuleQuery = "insert into mk_discount_rules(ruletype,display_name,algoid,data,enabled,start_date,end_date,brand,articletype,fashion) values(0,'$brandName $articleTypeName, $fashionTypeName','$algoid','$data',0,unix_timestamp(now()),unix_timestamp(now()),'$brandName','$articleTypeName','$fashionTypeName')";
	func_query($createRuleQuery);
}
echo "Creating new discount rules finished\n";
	
echo "Updating dynamic discount rules on styles started\n";
$query = "select * from mk_discount_rules where ruletype='0'";
$dynamic_rules = func_query($query, true);
$styleIdsToUpdateInSolr = array();

foreach($dynamic_rules as $rule){
	$ruleid = mysql_real_escape_string($rule['ruleid']);
	$enabled = mysql_real_escape_string($rule['enabled']);
	$brand = mysql_real_escape_string($rule['brand']);
	$articleType = mysql_real_escape_string($rule['articletype']);
	$fashion = mysql_real_escape_string($rule['fashion']);
	$exceptions = mysql_real_escape_string($rule['exceptionstyles']);
	echo "Updating Discount rule id :#$ruleid - dynamic\n";
	$ruleStyleQuery = "select sp.style_id from mk_style_properties sp, mk_catalogue_classification cc "; 
	$ruleStyleQuery.= " where cc.parent1 !='-1' and cc.parent2 !='-1' and cc.is_active=1"; 
	$ruleStyleQuery.= " and sp.global_attr_article_type=cc.id and sp.global_attr_brand='$brand'"; 
	$ruleStyleQuery.= " and sp.global_attr_fashion_type='$fashion' and cc.typename='$articleType'"; 
	$ruleStyleQuery.= " and (global_attr_year != '2011' or (global_attr_season !='Winter' and global_attr_season != 'Fall'))";
	$ruleStyleQuery.= " and (global_attr_year != '2012' or (global_attr_season !='Spring' and global_attr_season != 'Summer'))";

	if(!empty($exceptions)){
		$ruleStyleQuery .=" and sp.style_id not in ($exceptions)";
	}
	$ruleStyleQuery.= " and (sp.discount_rule is null or sp.discount_rule!='$ruleid')";

	$styleIds = func_query($ruleStyleQuery);
	$styleIdsToUpdate = array();
	foreach($styleIds as $styleId){
		$styleIdsToUpdate[] = $styleId['style_id'];
	}
	$styleIdsStr = implode(",", $styleIdsToUpdate);
	if(!empty($styleIdsStr)){
		$styleUpdateQuery = "update mk_style_properties set discount_rule = '$ruleid' where style_id in ($styleIdsStr)";
		func_query($styleUpdateQuery);
	}
	$styleIdsToUpdateInSolr = array_merge($styleIdsToUpdateInSolr, $styleIdsToUpdate);
}

// Fixing FALL/WINTER 2011 styles on which dynamic rules were accidentally applied. 
$ruleStyleQueryFW2011 = "select sp.style_id from mk_style_properties sp where "; 
$ruleStyleQueryFW2011.= " sp.global_attr_year = '2011' and (sp.global_attr_season ='Winter' or sp.global_attr_season = 'Fall')";
$ruleStyleQueryFW2011.= " and sp.discount_rule>0 and sp.discount_rule in (select dr.ruleid from mk_discount_rules dr where dr.ruletype=0)";

$styleIdsFW2011 = func_query($ruleStyleQueryFW2011);
$styleIdsToUpdateFW2011 = array();
foreach($styleIdsFW2011 as $styleId){
	$styleIdsToUpdateFW2011[] = $styleId['style_id'];
}
$styleIdsStrFW2011 = implode(",", $styleIdsToUpdateFW2011);
if(!empty($styleIdsStrFW2011)){
	$styleUpdateQueryFW2011 = "update mk_style_properties set discount_rule = null where style_id in ($styleIdsStrFW2011)";
	func_query($styleUpdateQueryFW2011);
	$styleIdsToUpdateInSolr = array_merge($styleIdsToUpdateInSolr, $styleIdsToUpdateFW2011);
}

// Fixing SUMMER/Spring 2012 styles on which dynamic rules were accidentally applied. 
$ruleStyleQuerySS2012 = "select sp.style_id from mk_style_properties sp where ";
$ruleStyleQuerySS2012.= " sp.global_attr_year = '2012' and (sp.global_attr_season ='Spring' or sp.global_attr_season = 'Summer')";
$ruleStyleQuerySS2012.= " and sp.discount_rule>0 and sp.discount_rule in (select dr.ruleid from mk_discount_rules dr where dr.ruletype=0)";

$styleIdsSS2012 = func_query($ruleStyleQuerySS2012);
$styleIdsToUpdateSS2012 = array();
foreach($styleIdsSS2012 as $styleId){
        $styleIdsToUpdateSS2012[] = $styleId['style_id'];
}
$styleIdsStrSS2012 = implode(",", $styleIdsToUpdateSS2012);
if(!empty($styleIdsStrSS2012)){
        $styleUpdateQuerySS2012 = "update mk_style_properties set discount_rule = null where style_id in ($styleIdsStrSS2012)";
        func_query($styleUpdateQuerySS2012);
        $styleIdsToUpdateInSolr = array_merge($styleIdsToUpdateInSolr, $styleIdsToUpdateSS2012);
}


echo "Updating dynamic discount rules on styles completed\n";
echo "Updating special discount rules on styles started\n";
$exception_rules_query = "select * from mk_discount_rules where ruletype=1";
$exception_rules = func_query($exception_rules_query, true);
if(!empty($exception_rules)){
	foreach($exception_rules as $rule){
		$ruleid = mysql_real_escape_string($rule['ruleid']);
		$enabled = mysql_real_escape_string($rule['enabled']);
		
		$exceptions = explode(",", $rule['exceptionstyles']);
		$exceptions = array_filter(array_unique(array_map("trim", $exceptions)));
		$exceptions = implode(",", $exceptions);
		$exceptions = mysql_real_escape_string($exceptions);
		
		if(empty($exceptions)){
			continue;
		}
		echo "Updating Discount rule id :#$ruleid - special\n";
		$styleUpdateQuery = "update mk_style_properties set discount_rule = '$ruleid' where style_id in ($exceptions)";
		func_query($styleUpdateQuery);
	}
}
echo "Updating special discount rules on styles completed\n";

if(!empty($styleIdsToUpdateInSolr)){
	echo "Solr index started\n";
	echo "Reindexing style ids : ".implode(",",$styleIdsToUpdateInSolr)."\n";
	$solrProducts = new solrProducts();
	$solrProducts->addStylesToIndex($styleIdsToUpdateInSolr);
	echo "Solr index finished\n";
}
?>
