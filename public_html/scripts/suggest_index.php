<?php
chdir(dirname(__FILE__));
include_once("../auth_for_solr_index.php");
include_once("../include/solr/solrSuggest.php");

set_time_limit(0);

$start_time=time();

$solr = new solrSuggest();
$solr->indexProducts();

$end_time=time();

$diff_time= ($end_time - $start_time) / 60;
echo "<br>\n=========== Total time :: ".$diff_time." ==================\n<br>";

?>
