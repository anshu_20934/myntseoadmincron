<?php
/**
 * Created on 28-Jun-2011
 * Last-modified, checked in on 14-Jun-2012
 * Send abandoned cart mailers to logged in users who added/modified their carts but didn't go to payments page
 * i.e. for users for which no order was created
 * This should be sent by cron-job at 4am IST everyday
 * AbandonedCartMailer.NumDays: how many days looking behind should it track the abandoned cart?
 * The mail would be sent only to users who abandon their carts in one single day
 * This should be run from a Cron Job which executes at 4am IST everyday
 * @author Kundan
 */
require_once "../admin/auth.php";
require_once $xcart_dir."/include/func/func.core.php";
require_once $xcart_dir."/include/dao/class/mcart/class.MCartDBAdapter.php";
require_once $xcart_dir."/include/class/mcart/class.MCart.php";
require_once $xcart_dir.'/include/class/csvutils/class.csvutils.ArrayCSVWriter.php';
require_once $xcart_dir.'/include/class/class.FTPActions.php';
require_once $xcart_dir.'/include/class/elabsmail/class.elabsmail.mailinglist.php';

require_once $xcart_dir."/utils/http/HttpClient.class.php";
include_once "$xcart_dir/include/class/widget/class.widget.skulist.php";
include_once($xcart_dir."/Profiler/Profiler.php");


//TODO: add logger support to non-webroot-scripts
echo "==============================begin abncartmailer==================================\n";
echo "abandonedCartMailer.php: cron started at ".date("r", time())."\n";


$today = time();
$year = date("Y", $today);
$month = date("m", $today);
$day = date("d", $today);
$numDays = FeatureGateKeyValuePairs::getInteger("AbandonedCartMailer.NumDays", 1);
$promotionText = FeatureGateKeyValuePairs::getFeatureGateValueForKey("AbandonedCartMailer.PromotionText");
if(!empty($promotionText)) {
	$promotionText = "<BR/>".$promotionText."<BR/>";
}

$campaignDate =  date("Ymd",$today);

//Start from 12am of numDays before today
$startFrom = mktime(0,0,0,$month, $day-$numDays, $year);
//End upto 12am of the next day after startFrom
$endUpto = $startFrom + 24*3600;

//For batching the mail calls
$batchLimit = 8000;
$currentOffset = 0;
$countOfRecords = 0;
$batchNumber = 1;
$countEmailsSent =0;


$smarty->assign("campaignDate",$campaignDate);


//avail reccomendaions connecion initializations

$client = new HttpClient("localhost");

function getAvailHtml($productIds,$client,$smarty){
	$userId = "";
	session_start();
	$sessionId= session_id();
	$url = 'http://service.sg.avail.net/2009-02-13/dynamic/54ed2a6e-d225-11e0-9cab-12313b0349b4/services/jsonrpc-1.x/';
	$data = array(
		"version"=>"1.1",
		"method"=>"getRecommendations",
		"id"=>"",
		"params" => array(
						"SessionID" => $sessionId,
						"Input" => array(
									
										"ProductId:".$productIds
									
										),
						"TemplateName"	=> "Productpage"
						)
		);
	$post =  json_encode($data);
	$response =json_decode( $client->curl_post($url, $post),true);
	$result = $response["result"]["values"];
	$styles_to_show = array();//"34989","32234","30845","32222","32451","32176","32232","38523","40453");
	
	foreach ($result as $res  ){
		$styles_to_show[]=$res[0];
	}
	$styles_to_show=array_slice($styles_to_show,0,8);
	$styleWidget=new WidgetSKUList('avail recommendation', $styles_to_show);	
	$widgetData = $styleWidget->getData();
	if(sizeof($widgetData["data"])!=8){
		return "";
	}
	$smarty->assign("avail_recommendation",$widgetData);	
	return $smarty->fetch("mail/abandon_cart_products_suggesions.tpl");
}


//avail init ends
$mcartDBAdapter = MCartDBAdapter::getInstance();
$anyAbandonedCartExists = false;

 for(;;) {
        //Look for abandoned carts only in the interval: startFrom upto endUpto
        $abnCartCondition = " (last_modified is not null)" 
        ." and last_modified between $startFrom and $endUpto"
        ." and cart.login is not null"
        ." and cart.context ='default'"
        ." and cart.login != ''" 
        ." and cart.login not in ("
        ."	select login from xcart_orders where date > $startFrom"
        .") order by cart.login, last_modified desc LIMIT $batchLimit OFFSET $currentOffset";

        echo "Now fetching abandoned carts for batch : $batchNumber \n";
        $abnCarts = $mcartDBAdapter->fetchAbandonedCarts($abnCartCondition);
        $countOfRecords = count($abnCarts);
        echo "Number of abandon cart for batch $batchNumber : $countOfRecords \n";
        
        $abnCartExists = !empty($abnCarts);

        if($abnCartExists) {

                $dirname = "/abncart/";

                $localDirname = "/var/mailer_uploads/abncart/";
                mkdir($localDirname);
                //delete all CSV files locally inside this directory
                $mask = "*.csv";
                //array_map("unlink",$localDirname.$mask);
                //local file which will be created this time
                $fileName = $localDirname.$campaignDate.".".$batchNumber.".csv";
                //file name on FTP server which would be copied
                $remoteFile = $dirname.$campaignDate.".".$batchNumber.".csv";

                Profiler::increment("192717-abandonedcart-mailing-list-csv-generation-started");
                $profilerCount = 0; 
                $writer = new ArrayCSVWriter($fileName, "|");
                $writer->setHeaderColumns(Array("email","CustomerName","CampaignDate","PromotionText","CartHTML","mrpRegistrationAmount","mrpFirstPurchaseAmount","recommendationsHTML"));
                $writer->writeHeader();


                foreach($abnCarts as $eachAbnCart) {

                        $customerName = getCustomerName($eachAbnCart);

                        $login = $eachAbnCart["cart"]->getLogin();
                        //$cartId = $eachAbnCart["cart"]->getCartID();//we might need this in future

                        //======================
                        $myCart =  $eachAbnCart["cart"];
                        $additionalCharges = number_format($myCart->getAdditionalCharges(), 2, ".", '');
                        $totalMRP = number_format($myCart->getMrp(), 2, ".", '');
                        $vat = number_format($myCart->getVat(), 2, ".", '');
                        $totalQuantity = number_format($myCart->getItemQuantity(), 0);
                        $couponDiscount = number_format($myCart->getDiscount(), 2, ".", '');
                        $cashDiscount = number_format($myCart->getCashDiscount(), 2, ".", '');
                        $productDiscount = number_format($myCart->getDiscountAmount(), 2, ".", '');
                        $shippingCharge = number_format($myCart->getShippingCharge(), 2, ".",'');
                        $cartLevelDiscount = round(number_format($myCart->getCartLevelDiscount(), 2, ".", ''));
                        $totalAmount = $totalMRP + $additionalCharges - $couponDiscount - $cashDiscount - $productDiscount - $cartLevelDiscount + $shippingCharge;
                        $cashBackDisplayAmount= $totalMRP + $additionalCharges - $productDiscount - $couponDiscount;
                        $coupondiscount = $couponDiscount;
                        $cashdiscount = $cashDiscount;
                        $mrp = $totalMRP;
                        $amount = $totalMRP + $additionalCharges + $shippingCharge;
                        $savings = $productDiscount + $couponDiscount + $cashDiscount + $cartLevelDiscount;

                        $smarty->assign("cartLevelDiscount", $cartLevelDiscount);
                        $smarty->assign("couponDiscount", $couponDiscount);
                        $smarty->assign("cashdiscount", $cashdiscount);
                        $smarty->assign("itemDiscount",$productDiscount);
                        $smarty->assign("totalAmount",$totalAmount);
                        $smarty->assign("amount",$amount);
                        //======================
                        $productsInCart = MCartUtils::convertCartToArray($eachAbnCart["cart"]);
                        $smarty->assign("productsInCart",$productsInCart);

                        $smarty->assign("login",$login);
                        $smarty->assign("customerName",$customerName);
                        $cartHtml = $smarty->fetch("admin/main/abncart.tpl", $smarty);

                        //remove carriage returns and line feeds
                        $cartHtml =  preg_replace("/\r\n/"," ", $cartHtml);
                        $cartHtml =  preg_replace("/\n/"," ", $cartHtml);

                        //avail recommendaions html block
                        $productid;
                        $tempPrice=0.0;
                        foreach($productsInCart as $product){
                                if($product["productPrice"]>$tempPrice){
                                        $productid= $product["productStyleId"];
                                        $tempPrice = $product["productPrice"];
                                }
                        }
                        $smarty->assign("campaignDate",$campaignDate);
                        $productRecommendationHtml = getAvailHtml($productid,$client,$smarty);
                        $productRecommendationHtml =preg_replace("/\r\n/"," ", $productRecommendationHtml);
                        $productRecommendationHtml =preg_replace("/\n/"," ", $productRecommendationHtml);
                        // add the data related to "refer and earn" section
                        global $mrpCouponConfiguration;
                        $mrpRegistrationAmount = $mrpCouponConfiguration['mrp_refRegistration_numCoupons']*$mrpCouponConfiguration['mrp_refRegistration_mrpAmount'];
                        $mrpFirstPurchaseAmount = $mrpCouponConfiguration['mrp_refFirstPurchase_numCoupons']*$mrpCouponConfiguration['mrp_refFirstPurchase_mrpAmount'];

                        $writer->writeRow(Array("email"=>trim($login), "CustomerName" =>$customerName, "CampaignDate"=>$campaignDate, "PromotionText"=>$promotionText, "CartHTML"=>trim($cartHtml), "mrpFirstPurchaseAmount"=>$mrpFirstPurchaseAmount, "mrpRegistrationAmount"=>$mrpRegistrationAmount,"recommendationsHTML" => $productRecommendationHtml));
                        $profilerCount++;	
                }

                
                $writer->writeRow(Array("email"=>"myntramailarchive@gmail.com", "CustomerName" =>"", "CampaignDate"=>$campaignDate, "PromotionText"=>$promotionText, "CartHTML"=>trim($cartHtml), "mrpFirstPurchaseAmount"=>$mrpFirstPurchaseAmount, "mrpRegistrationAmount"=>$mrpRegistrationAmount,"recommendationsHTML" => $productRecommendationHtml));
                Profiler::increment("192717-abandonedcart-mailing-list-csv-ready", $profilerCount);
                $writer->closeFile();
                $countEmailsSent++;
                if(!$anyAbandonedCartExists)
                   $anyAbandonedCartExists = true;
                echo "Completed Processing batch " .$batchNumber. "\n";
                if($countOfRecords < $batchLimit) break;
                $currentOffset = $currentOffset + $batchLimit + 1;
                $batchNumber = $batchNumber + 1;
                echo "Processing batch " .$batchNumber. "\n";
        }
        else {
            break; // No records just break
        }
}

if(!$anyAbandonedCartExists){
    echo "There are no abandoned carts for logged-in users between ". date("r",$startFrom). " and ".date("r",$endUpto)." for whom orders were not generated after ".date("r",$startFrom)."\n";
    exit;
}

echo __FILE__.": No of email entries made in icubes: ".$countEmailsSent."\n";
echo "abandonedCartMailer.php: cron finished at ".date("r", time())."\n";
echo "============================end abncartmailer================================\n";

function getCustomerName($row) {
	$customerName = "";
	if(!empty($row["firstname"])) {
		$customerName = $row["firstname"];
	}
	if(!empty($row["lastname"])) {
		$customerName .= " ".$row["lastname"];
	}
	if(empty($customerName)) {
		$customerName = "Customer";
	} 
	return $customerName;
}

?>
