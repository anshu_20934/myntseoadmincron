
<?php

require_once(dirname(__FILE__)."/../auth.php");

use leaderboard\LeaderBoardCacheSetter;

echo "cron to set the top N buyers cache has started\n";

$lbid = (int)$argv[1];
$expiry = (int)$argv[2];
$lbcs = new LeaderBoardCacheSetter();
$lbcs->setCacheForTopNBuyers($lbid, $expiry);

echo "cron to set the top N buyers cache has ended\n";

?>