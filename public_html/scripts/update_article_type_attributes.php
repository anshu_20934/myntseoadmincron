<?php
chdir(dirname(__FILE__));
include("../auth.php");
include_once("../include/func/func.mkcore.php");
include_once("../include/func/func.randnum.php");
include("../PHPExcel/PHPExcel.php");

set_time_limit(0);

/*
 * Insert data in mk_attribute_type, mk_attribute_value tables to support the article type attributes
 * in our system.
 */

//get all parameters
$filepath = explode('=',$_SERVER['argv'][1]);
$filepath = trim($filepath[1]);

$errorpath = explode('=',$_SERVER['argv'][2]);
$errorpath = trim($errorpath[1]).'promo_user_error_'.time().'.xls';//append file name to the path given in argument

echo "verify all passed parameter--- \n filepath=".$filepath."\n errorpath=".$errorpath;
echo "\n\nif verified and correct, press- 'y', otherwise- 'n' >";

$input = trim(fgetc(STDIN));
if($input == 'n')
{echo "\n\nbye";exit;}

elseif($input == 'y'){
	if($filepath && $errorpath){
		//read a xls file
		$objReader = PHPExcel_IOFactory::createReader('Excel2007');//use xls file, use excel2007 for 2007 format
		$objPHPExcel1 = $objReader->load($filepath);
		$article_type_sheet=$objPHPExcel1->getSheetByName(' Article Type->Disp Category');
		$lastRow = $article_type_sheet->getHighestRow();

		echo "\n\nnumber of rows=".($lastRow-1);

		$j = 1;//index for excel 2

		for($i=2;$i<=$lastRow;$i++){//start from second row ignore heading
			$cell1Obj = ($article_type_sheet->getCell('A'.$i));
			$article_type = trim($cell1Obj->getvalue());//returns the value in the cell

			// get the article type id and attributes for it
			$result=mysql_query("select id from mk_catalogue_classification
									where typename='".$article_type."' and parent1 !='-1' and parent2 !='-1' and is_active=1");

			if (!$result) {
				$message  = 'Invalid query: ' . mysql_error() . "\n";
				$message .= 'Whole query: ' . $query;
				die($message);
			}

			while ($row = mysql_fetch_assoc($result)) {
				$article_type_id = $row[id];
				echo "\n --> ".$article_type." -------article type id: ".$row[id];
			}

			if(mysql_num_rows($result) == 0){
				continue;
			}
			mysql_free_result($result);

			$attribute_type_array = array();
			// get the attribute values for the article_type;
			foreach(range('G', 'N') as $cell_id){
				$cell1Obj = ($article_type_sheet->getCell($cell_id.$i));
				$attribute_type = trim($cell1Obj->getvalue());
				if(!empty($attribute_type)){
					array_push($attribute_type_array,$attribute_type);
				}
			}

			insert_attribute_type($article_type_id,$attribute_type_array);

		}
	}
}
function insert_attribute_type($article_type_id,$attribute_type_array){
	//	print_r($attribute_type_array);
	$select_query = sprintf("select id,attribute_type from  mk_attribute_type where catalogue_classification_id='".$article_type_id."'");
	$select_results=mysql_query($select_query);
	// remove already exisint entries from the array and remove non existing entries in array from db
	while ($select_result = mysql_fetch_assoc($select_results)) {
		if(in_array($select_result['attribute_type'], $attribute_type_array)){
			echo "\n   		->".$select_result['attribute_type']."   keep it in db";
			foreach($attribute_type_array as $tmp_key=>$tmp_value){
				if($select_result['attribute_type'] == $tmp_value){
					unset($attribute_type_array[$tmp_key]);
				}
			}
		}else{
			//			echo "\n   		->".$select_result['attribute_type']."   removing from db";
			$query = sprintf("select id from mk_attribute_type_values where attribute_type_id='".$select_result['id']."'");
			$deleted_rows = mysql_query($query);
			while ($deleted_row = mysql_fetch_assoc($deleted_rows)) {
				$query = sprintf("delete from mk_style_article_type_attribute_values
						where article_type_attribute_value_id='".$deleted_row['id']."'");
				func_query($query);
			}

			// delete attribute values
			//			echo "\n   			->deleting attribute_values for attribute type id".$select_result['id']."";
			$query = sprintf("delete from mk_attribute_type_values where attribute_type_id='".$select_result['id']."'");
			func_query($query);


			// delete style specific values if inserted
			//			echo	"\n   			->deleting attribute_type for attribute type id".$select_result['id']."";
			$query = sprintf("delete from mk_attribute_type where id='".$select_result['id']."'");
			func_query($query);

		}
	}
	foreach($attribute_type_array as $tmp_key=>$tmp_value){
		if(!empty($tmp_value)){
			echo "\n   		->".$tmp_value."   insert in db";
			$query = sprintf("insert mk_attribute_type
							(catalogue_classification_id,attribute_type,is_active) 
							values('%d','%s','%d')",$article_type_id,$tmp_value,1);
			func_query($query);
		}
	}
}
?>