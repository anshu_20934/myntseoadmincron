#!/bin/bash
# PProcessSpawn.sh file pcount psize offset


if [ $# -ne 4 ]
then
    echo "Error in $0 - Invalid Argument Count"
    echo "Required params:: file pcount psize offset"
    exit
fi


echo "Starting"

COUNTER=0
while [  $COUNTER -lt "$2" ]; do
             
             let COUNTER=COUNTER+1 
	     nohup php $1 id=$COUNTER pcount=$2 psize=$3 oset=$4 >> "PP$COUNTER.log" &


done
