<?php
chdir(dirname(__FILE__));
include_once '../env/DB.config.php';
include_once '../env/System.config.php';
include_once "../utils/S3.php";
$updateDBPath = false;
if($argc == 2 && strtolower($argv[1]) == "true"){
	$updateDBPath = true;
}

date_default_timezone_set("Asia/Calcutta");
$con = dbConnect();
$bucket = SystemConfig::$amazonS3Bucket;
$s3 = new S3(SystemConfig::$amazonAccessKey, SystemConfig::$amazonSecretKey);

$unreferencedSearchImagePaths = ""; 
$newSearchImagePaths="";

$tempDir = sys_get_temp_dir()."/fixStyleSearchImages";
if(!file_exists($tempDir) && !is_dir($tempDir)){
	$wasSuccessful = mkdir($tempDir);
	if(!$wasSuccessful){
		die("Failed to write the image paths to be deleted to log file");
	}	
}
$toBeDeletedList = "$tempDir/toBeDeleted_".date("Y-m-d_H-i-s").".csv";
$toBeUpdatedList = "$tempDir/toBeUpdated_".date("Y-m-d_H-i-s").".csv";

fix($toBeDeletedList,$toBeUpdatedList);

if($con){
	mysql_close($con);
}

function writeDataToFile($data,$filePath){
	$fp = fopen($filePath, "a");
	fwrite($fp, $data);
	fclose($fp);
}

function dbConnect(){
	$con = mysql_connect(DBConfig::$rw['host'],DBConfig::$rw['user'],DBConfig::$rw['password']);
	if($con == false){
		die("Cannot open DB Connection.. Give Up!");
	}
	mysql_select_db(DBConfig::$rw['db'],$con);
	return $con;
}


function fix($toBeDeletedList,$toBeUpdatedList) {
	global $con,$unreferencedSearchImagePaths,$newSearchImagePaths,$updateDBPath;
	$baseSql = "
				select style_id,default_image,search_image 
				from mk_style_properties 
				where 
				(search_image != REPLACE(default_image,'.jpg','_180_240.jpg')) 
				AND 
				(search_image != REPLACE(default_image,'.JPG','_180_240.JPG'))			
				order by style_id desc ";
	//Process 100 styles at a time only
	$batchSize = 2;
	$start = 0;
	
	for($start=0;;){
		$limitClause = " limit $start,$batchSize";
		$sql = $baseSql.$limitClause;
		$res = mysql_query($sql,$con);
		if($res == false){
			break;
		}
		$numRows = 0;
		$numRowsSuccessfullyUpdated = 0;
		//clear these string every batch, so that after every batch, file would be appended and closed. this is helpful when we hit Ctrl+C in between 
		$unreferencedSearchImagePaths = "";
		$newSearchImagePaths = "";
		
		while($row=mysql_fetch_assoc($res)){
			$numRows++;
			$styleId = $row['style_id'];
			$defaultImageCdnPath = $row['default_image'];
			$searchImageCdnPath = $row['search_image'];
			$wasSuccessful = processSearchImage($defaultImageCdnPath, $searchImageCdnPath, $styleId);
			if($wasSuccessful){
				echo "$styleId: Completed successfully\n";
				$numRowsSuccessfullyUpdated++;
			}
			else {
				echo "$styleId: Processing failed\n";
			}
		}
		if($numRows == 0){
			break;
		}
		//write every batch at once to file
		writeDataToFile($unreferencedSearchImagePaths,$toBeDeletedList);
		writeDataToFile($newSearchImagePaths,$toBeUpdatedList);
		if($updateDBPath){
			$start = $batchSize - $numRowsSuccessfullyUpdated;
		}else{
			$start += $batchSize;
		}
	}
}

function processSearchImage($defaultImageCdnPath,$searchImageCdnPath,$styleId){
	echo "\n===========================Processing StyleId: $styleId================================\n";
	echo "Copying base image:";
	$ret1 = copyBaseSearchImage($defaultImageCdnPath, $searchImageCdnPath);
	$res1 = updateDBAndPrintResults($styleId, $ret1, true);
	echo "Copying minified image:";
	$ret2 = copyMinifiedSearchImage($defaultImageCdnPath, $searchImageCdnPath);
	$res2 = updateDBAndPrintResults($styleId, $ret2);
	return $res1 && $res2;
}

/**
 * Conditionally copy base search image if target does not exist
 * @param unknown_type $defaultImageCdnPath
 * @param unknown_type $searchImageCdnPath
 * @return if copy is successful, returns the full CDN path of the target image (copy destination). If copy failed, then returns false. If copy was not required, returns true
 */
function copyBaseSearchImage($defaultImageCdnPath,$searchImageCdnPath){
	$origSearchImageS3Path = getS3Path($searchImageCdnPath);
	//extract the target search image CDN path
	$targetSearchImageCdnPath = getSearchImagePath($defaultImageCdnPath);
	//extract the S3 path for the image above
	$targetSearchImageS3Path = getS3Path($targetSearchImageCdnPath);
	if(!url_exists($targetSearchImageS3Path)){
		//get relative paths for the source & target
		$searchImageRelativePath = getRelativePath($searchImageCdnPath);
		$targetSearchImageRelativePath = getRelativePath($targetSearchImageCdnPath);
		//echo "Target search image path: $targetSearchImageS3Path does not exist on s3, so need to copy it within s3\n";
		$res = copyWithinS3($searchImageRelativePath, $targetSearchImageRelativePath);
		return (isset($res['hash']) && url_exists($targetSearchImageS3Path)) ? array(true,$targetSearchImageCdnPath,$origSearchImageS3Path) : false;
	}
	return array(false,$targetSearchImageCdnPath,$origSearchImageS3Path);
}

/**
 *
 * Conditionally copy minified search image if target does not exist
 * @param unknown_type $defaultImageCdnPath
 * @param unknown_type $searchImageCdnPath
 * @return if copy is successful, returns the full CDN path of the target image (copy destination). If copy failed, then returns false. If copy was not required, returns true
 */
function copyMinifiedSearchImage($defaultImageCdnPath,$searchImageCdnPath){

	$srcSearchImageMiniCdnPath = getMinifiedImagePath($searchImageCdnPath);
	$srcSearchImageMiniS3Path = getS3Path($srcSearchImageMiniCdnPath);
	//extract the target search image minified CDN path
	$targetSearchImageMiniCdnPath = getMinifiedImagePath(getSearchImagePath($defaultImageCdnPath));
	//extract the S3 version of the image
	$targetSearchImageMiniS3Path = getS3Path($targetSearchImageMiniCdnPath);
	if(!url_exists($targetSearchImageMiniS3Path)){
		//get relative paths for the source & target
		//source
		$searchImageMiniRelativePath = getRelativePath($srcSearchImageMiniCdnPath);
		//target
		$targetSearchImageMiniRelativePath = getRelativePath($targetSearchImageMiniS3Path);
		$res = copyWithinS3($searchImageMiniRelativePath, $targetSearchImageMiniRelativePath);
		return (isset($res['hash']) && url_exists($targetSearchImageMiniS3Path)) ? array(true,$targetSearchImageMiniCdnPath,$srcSearchImageMiniS3Path):false;
	}
	return array(false,$targetSearchImageMiniCdnPath,$srcSearchImageMiniS3Path);
}

function updateDBAndPrintResults($styleId, $ret, $copyRequired = false){
	global $unreferencedSearchImagePaths,$newSearchImagePaths,$updateDBPath;
	if(isset($ret) && is_array($ret)){
		//either copy is successful or copy was not required
		//in either case, need to update the DB with the image path
		if($ret[0]){
			echo "Copy successful.";
		}
		else {
			echo "Copy was not required, target image already existed..";
		}
		$targetSearchImageCdnPath = $ret[1];
		$originalImageSafeForDeletion = $ret[2];
		if($copyRequired){
			if($updateDBPath){
				//in any case, update the DB
				$rowsAffected = updateSearchImagePath($targetSearchImageCdnPath, $styleId);
				if($rowsAffected>0){
					$unreferencedSearchImagePaths.="$styleId\t$originalImageSafeForDeletion\n";
					$newSearchImagePaths.="$styleId\t$targetSearchImageCdnPath\n";
					return true;
				}
				else {
					//could not update the DB
					//this is failure case, so just echo.. nothing to add to files
					return false;
				}
			}
			else {
				//DB Path updation is not required
				$unreferencedSearchImagePaths.="$styleId\t$originalImageSafeForDeletion\n";
				$newSearchImagePaths.="$styleId\t$targetSearchImageCdnPath\n";
				return true;
			}
		}
		else {
			//image already existed so copy was not required.
			$unreferencedSearchImagePaths.="$styleId\t$originalImageSafeForDeletion\n";
			//$newSearchImagePaths.="$styleId\t$targetSearchImageCdnPath\n";
			return true;
		}
	}
	else {
		echo "Copy failed..";
		return false;
	}
}
function getS3Path($cdnPath){
	global $bucket;
	return str_replace("myntra.myntassets.com","$bucket.s3.amazonaws.com",$cdnPath);
}

function getRelativePath($fullPath){
	global $bucket;
	$rep1 = str_replace("http://myntra.myntassets.com/","", $fullPath);
	$rep2 = str_replace("http://$bucket.s3.amazonaws.com/","", $rep1);
	return $rep2;
}

function getSearchImagePath($defaultImagePath){
	if(endsWith($defaultImagePath, ".JPG")){
		$targetSearchImagePath = str_replace(".JPG","_180_240.JPG",$defaultImagePath);
	}
	elseif (endsWith($defaultImagePath, ".jpg")) {
		$targetSearchImagePath = str_replace(".jpg","_180_240.jpg",$defaultImagePath);
	}
	else {
		$targetSearchImagePath = false;
	}
	return $targetSearchImagePath;
}

function getMinifiedImagePath($path){
	if(endsWith($path, ".jpg")){
		$minifiedPath = str_replace(".jpg","_mini.jpg",$path);
	}
	elseif (endsWith($path,".JPG")){
		$minifiedPath = str_replace(".JPG","_mini.JPG",$path);
	}
	else {
		$minifiedPath = false;
	}
	return $minifiedPath;
}

function deleteWithinS3($uri){
	global $s3,$bucket;
	return $s3->deleteObject($bucket, $uri);
}

function copyWithinS3($src,$target){
	global $s3,$bucket;		
	$res = $s3->copyObject($bucket, $src, $bucket, $target, S3::ACL_PUBLIC_READ);
	return $res;
}

function updateSearchImagePath($searchImagePath,$styleId){
	if(!empty($searchImagePath)) {
		echo "Updating the search image in DB..";
		$sql = "UPDATE mk_style_properties set search_image = '".$searchImagePath."' where style_id = $styleId";
		global $con;
		mysql_query($sql,$con);
		$rows = mysql_affected_rows($con);
		if($rows>0){
			echo "Updated Successfully.";
		}
		else {
			echo "Update Failed.";
		}
		return $rows;
	}
	return false;
}

function endsWith($haystack, $needle) {
	$length = strlen($needle);
	if ($length == 0) {
		return true;
	}

	$start  = $length * -1; //negative
	return (substr($haystack, $start) === $needle);
}
function url_exists($url) {
	$hdrs = @get_headers($url);
	return is_array($hdrs) ? preg_match('/^HTTP\\/\\d+\\.\\d+\\s+2\\d\\d\\s+.*$/',$hdrs[0]) : false;
}
