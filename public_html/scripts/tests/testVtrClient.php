<?php
 require_once '../../auth.php';
 require_once '../../include/solr/solrProducts.php';
 use vtr\client\VirtualTrialRoomClient;
 $solrProducts = new SolrProducts();
 $styleIdArrayToIndex = getSingleColumnAsArray("select distinct(style_id) from vtr_style_variant where is_enabled = true");
 //$styleIdArrayToIndex = array(14367,14448,14468,21616,21642,21645,27405,27406,27407,27410,27414,27420,27424,27425,27426,27428,27432,27469,27471,27497,27515,28897,29905,29906,29912,33825,34727,34834,35785,38206,38207,38762,42972,44423,44454,44455,44458,49761,52341,52353,52357);
 //print_r(VirtualTrialRoomClient::getStyleDataForIndexing($styleIdArrayToIndex));	
 $solrProducts->addStylesToIndex($styleIdArrayToIndex,true,TRUE);
 $solrProducts->commitToAllServers();
 ?>
