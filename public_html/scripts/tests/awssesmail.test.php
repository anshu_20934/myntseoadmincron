<?php
require_once "../../auth.php";
require_once "$xcart_dir/include/class/class.mail.amazonsesmailer.php";
$email_details['to']="kundan.burnwal@myntra.com";
$email_details['subject']="test mail from amazon ses";
$email_details['content']="test mail body from amazon ses";
$email_details['from_email']="no-reply@myntra.com";
$email_details['from_name']="Myntra no-reply";
try {
$mailer = new AmazonSESMailer();
echo "Mailer class:".get_class($mailer)."\nMail details:";
print_r($email_details);
echo "verifying from_email: ";
$mailer->verifyEmail($email_details['from_email']);
echo "daily sent quota:";
print_r($mailer->getDailySendQuota());
echo "Mail sent:".$mailer->sendMail($email_details)."\n";
} catch (Exception $e) {
    echo 'Caught exception: ',  $e->getMessage(), "\n";
    echo $e->getTraceAsString();
}
?>
