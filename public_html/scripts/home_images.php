<?php
chdir(dirname(__FILE__));
require_once("../auth.php");
require_once("../include/func/func.utilities.php");

set_time_limit(0);

$images_result = func_select_query(
    'mk_images',
    array(
        'enabled' => 1,
        'page' => 'home'
    ),
    NULL,
    NULL,
    array('display_order' => 'asc')
);

foreach ($images_result as $image) {
    $list = $images[$image['type']];
    $list[] = $image;
    $images[$image['type']] = $list;
}

foreach ($images as $key => $type) {
    foreach ($type as $t) {
        echo $t['image'] . "\n";
        moveToS3($xcart_dir . '/' . $t['image'], 'myntratestimages', $t['image']);
    }
}

$images = func_select_query(
    'mk_product_type',
    array(
        'is_active' => 1,
        'type' => 'p'
    ),
    NULL,
    NULL,
    array('display_order' => 'asc')
);


foreach ($images as $image) {
    echo $image['image_t'] . "\n";
    moveToS3($xcart_dir . '/' . $image['image_t'], 'myntratestimages', $image['image_t']);
}