<?php

function error() {
	echo "Incorrect format specified, run the script with --help for guidelines \n";
	exit();
}

if ($argv[1] === "--help") {
	echo "\n \tThe way to run this script is simple, you just have to supply two arguments to the script \n
	      The first argument is --
		      type of email (could be abnCart, etc..) 
	      The second argument is --
		      action (could be createCSV*, sendEmail**)
	     
	      EXAMPLE -- php emailSctript.php abnCart sendEmail
	      *  this just creates the CSV file with the placeholder values
	      ** this creates the CSV and also uploads the FTP file and triggers a process \n\n";
	exit();
}
$emailType = $argv[1];
$action    = $argv[2];

switch ($emailType) {
	case "abnCart":
		$command = "php abandonedCartMailer_octane.php ";
		if ($action === "createCSV") $command .= "";
		else if ($action === "sendEmail") $command .= " && php abandonedcartFTPuploader_octane.php";
		else error();
		break;

	// another case ..

	default:
		error();

}

$response = shell_exec($command);
echo $response;

?>

