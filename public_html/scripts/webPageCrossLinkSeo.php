<?php

$dirName = dirname(__FILE__);
chdir($dirName);
include_once("../auth.php");
require_once($xcart_dir."/include/func/func.utilities.php");
require_once($xcart_dir."/include/func/func.core.php");
require_once '../env/AMQP.config.php';
include_once \HostConfig::$documentRoot ."/utils/throttleUpdatesDb.php";
define('ALLOWABLETAGS','<h1><h2><h3><h4><h5><h6><em><strong><br/><b><p><br>');
define('REMOVABLETAGS','a');
define('DBSERVER', AMQPConfig::$dbServer);
define('DBPORT', AMQPConfig::$dbPort);
define('DBUSER', AMQPConfig::$dbUser);
define('DBPASS', AMQPConfig::$dbPass);
define('DBNAME', AMQPConfig::$dbName);


/* get all active filter groups
 * @result:(array) filter groups
 */
function getAllFilterGroups(){
    $sql ="SELECT group_name,group_label FROM  mk_filter_group where is_active='1' and group_name not like '%_size' order by display_order";
    return func_query($sql);
}

/* get all active filters of active groups
 * @result:(array) filters
 */
function getAllFilters(){
	//$sql="select f.id,f.filter_name,f.filter_url,f.about_filter from mk_filters f join mk_filter_group g on f.filter_group_id=g.id where f.is_active='1' and g.is_active='1' and g.group_name not like '%_size'";
    $sql = "select * from adseo_pagebodywidget"; 
    return func_query($sql);
}

/* get all active handpicked keywords
 * @result:(array) handpicked keywords
 */
function getAllHandPickedKeywords(){
    $sql="select id,crosslink_keyword,is_active,crosslink_keyword_url from mk_crosslink_keyword where is_active='1'";
    return func_query($sql);
}

/* 
 * @result:(array) handpicked keywords hashed by crosslink_keyword
 */
function getAllHandPickedKeywordsHashedByKeyword(){
        $sql="select id,crosslink_keyword,is_active,crosslink_keyword_url from mk_crosslink_keyword where is_active='1'";
        $data =  func_query($sql);
        $newData = array();
        foreach($data as $key=>$row) {
            $newData[$row['crosslink_keyword']] = $row;
        }
        return $newData;
}

/* get stats search description
 * @result:(array) descriptions
 */
function getStatsDescription(){
    $sql="select key_id,description from mk_stats_search where description is not null and description!=''";
    return func_query($sql,true);
}

/* get stats search description
 * @result:(array) descriptions
 */
function getStyleDescription(){
    $sql="select id, description, is_customizable from mk_product_style where is_active=1 and iventory_count > 0";
    return func_query($sql);
}

/* make all relevant linking keywords from filter name, filer url, FG  name and FG label
 * @result:(array) linkable and linked keywords generated from FG and F
 */
function getLinkableFilter(){
    global $http_location_cli;
    $linkableKeywords = array();

    //get all FG as keywords
    $FG = getAllFilterGroups();
    if($FG){
        foreach($FG as $index=>$group){
            $groupName = $group['group_name'];
            $groupLabel = $group['group_label'];

            $patternGroupName = $groupName;
            $patternGroupLabel = $groupLabel;

            $replacementGroupName = "<a href='".$http_location_cli."/".$groupName."'>".$groupName."</a>";
            $replacementGroupLabel = "<a href='".$http_location_cli."/".$groupLabel."'>".$groupLabel."</a>";

            //take both group name and label as keywords
            if(strtolower($groupName) == strtolower($groupLabel)){
                $linkableKeywords[] = $patternGroupLabel;
                $linkedKeywords[] = $replacementGroupLabel;
            } else {
                if(!empty($groupName)){
                    $linkableKeywords[] = $patternGroupName;
                    $linkedKeywords[] = $replacementGroupName;
                }
                if(!empty($groupLabel)){
                    $linkableKeywords[] = $patternGroupLabel;
                    $linkedKeywords[] = $replacementGroupLabel;
                }                   
            }
        }
    }

    //get all handpicked keywords
    $HK = getAllHandPickedKeywords();
    if($HK){
        foreach($HK as $index=>$val){
            $ckeyword = $val['crosslink_keyword'];
            $patternKeywordName = $ckeyword;
            if($val['crosslink_keyword_url']) {
                $crosslink_keyword_url = $http_location_cli.'/'.$val['crosslink_keyword_url'];
            } else {
                $crosslink_keyword_url = $http_location_cli.'/'.$ckeyword;
            }
            $replacementKeywordName = "<a href='$crosslink_keyword_url'>".$ckeyword."</a>";

            if(!empty($ckeyword)){
                $linkableKeywords[] = $patternKeywordName;
                $linkedKeywords[] = $replacementKeywordName;
            }
        }
    }

    return array(
                    'linkableFilter' => $linkableKeywords,
                    'linkedFilter' => $linkedKeywords        
                );
}
/* links all search with replace on subject
 * @param:(mixed)$search
 * @param:(mixed)$replace
 * @param:(mixed)$ubject
 * @param:(int)$maxReplacement - no. of same search to replaced by replace on subject
 * @param:(int)$charCase - 0=case insensitive, 1= case sensitive
 */
function createCrossLinkedContent($search, $replace, $subject, $maxReplacement=1, $charCase=0){    
    global $http_location_cli;
    global $replaced;
    $replaced = $replace;    
    $charsToEscape = "/";//preg_quote() by default does not escape a forward '/'.


    if(!is_array($search)){
        $searchPattern = preg_quote($search,$charsToEscape);//preg_replace to work properly(escapes all special chars used for regEx eg:/\[]+- etc..)
        $searchPattern = ($charCase==1)? "/\b(".$searchPattern.")\b(?!(?:(?!<\/?[ha].*?>).)*<\/[ha].*?>)(?![^<>]*>)/" : "/\b(".$searchPattern.")\b(?!(?:(?!<\/?[ha].*?>).)*<\/[ha].*?>)(?![^<>]*>)/i";//replace whole word
        $search = $searchPattern;        

    } else {
        foreach($search as $index=>$searchString){
            $searchPattern = preg_quote($searchString,$charsToEscape);
            $searchPattern = ($charCase==1)? "/\b(".$searchPattern.")\b/(?!(?:(?!<\/?[ha].*?>).)*<\/[ha].*?>)(?![^<>]*>)/" : "/\b(".$searchPattern.")\b(?!(?:(?!<\/?[ha].*?>).)*<\/[ha].*?>)(?![^<>]*>)/i";
            $search[$index] = $searchPattern;
        }
    }
    //making the matched word/s as hyper linked and the callback reference($1) refers to the matched word/s from the $subject
    //$replace = "<a href='".$http_location_cli."/$1'>$1</a>";
    #return $linkedContent = preg_replace($search , $replace , $subject, $maxReplacement);
    return $linkedContent = preg_replace_callback($search , function($matches) {
        global $replaced;
        $matched = $replaced[strtolower($matches[0])];
        return preg_replace('/>'.$matches[0].'</i','>'.$matches[0].'<',$matched);
    }, $subject, $maxReplacement);
}


/* adds word count for the respective keyword
 * @param:(array)single indexed array of keywords
 * @return:(array)two dimensional indexed array of keywords and word count
 */
function keywordsWordCount($keywordsSingleDimensionArray){

    $keywordWCArray = array();
    if(is_array($keywordsSingleDimensionArray)){
        foreach($keywordsSingleDimensionArray as $idx=>$keyword){
            $WC = str_word_count($keyword);
            $keywordWCArray[] = array('keyword'=>$keyword,'wc'=>$WC);
        }
    }
    return $keywordWCArray;
}

/* sorted single dimension indexed keywords(higher the word count, lower the index) array
 * @param:(array)two dimensional array of keyword and word count
 * @return:(array)single dimension indexed array of keywords(longer the phrase earlier in array)
 */
function sortedSingleDimensionKeywords($keywordWCArray){
    $sortedKeywords = array();
    $sortedKeywordsWithWC = quickSortRecursive( $keywordWCArray, $left = 0 , $right = NULL, 'wc' );
    if(is_array($sortedKeywordsWithWC)){
        $count = sizeof($sortedKeywordsWithWC);
        foreach($sortedKeywordsWithWC as $idx=>$val){
            $sortedKeywords[($count-1)-$idx] = $val['keyword'];
        }
    }
    ksort($sortedKeywords);
    return $sortedKeywords;    
}


/* makes all filter descriptions cross linked
 * @result:(array)linked descriptions and updated DB
 */
function makeFilterDescriptionCrossLinked($handPickedKeywords=array()){
    global $http_location_cli;
    $batchDBQuery = array();
    
    //get all descriptions from filters
    $F = getAllFilters();
    $filters = getLinkableFilter();
    $linkableKeywords = $filters['linkableFilter'];
    $linkedKeywords = $filters['linkedFilter'];
    
    //sort the linkable keywords array desc on word count of keyword
    $keywordWCArray = keywordsWordCount($linkableKeywords);
    $sortedLinkableKeywords = sortedSingleDimensionKeywords($keywordWCArray);
    foreach($sortedLinkableKeywords as $idx=>$keyWord){
    	if(isset($handPickedKeywords[$keyWord]) && $handPickedKeywords[$keyWord]['crosslink_keyword_url']) {
    		$sortedLinkedKeywords[strtolower($keyWord)] = "<a href='". strtolower($http_location_cli."/".$handPickedKeywords[$keyWord]['crosslink_keyword_url'])."?src=desc'>".$keyWord."</a>";
    	}
    	else {
    		//replace ' ' with '-' in linked keywords
    		$hyphenatedKeyword = str_replace(' ', '-', $keyWord);
        	$sortedLinkedKeywords[strtolower($keyWord)] = "<a href='". strtolower($http_location_cli."/".$hyphenatedKeyword)."?src=desc'>".$keyWord."</a>";
    	}        
    }
    
    if($F) {
	$count = 0 ;
        foreach($F as $index=>$filter){
		if(!empty($filter['page_description'])){//getAllFilters() may give blank descriptions                
		var_dump($count);
		$count++;
                $linkingDescription = stripOnlyTags($filter['page_description'],REMOVABLETAGS);
                $linkedDescription = createCrossLinkedContent($sortedLinkableKeywords , $sortedLinkedKeywords , $linkingDescription);
                $linkedDescriptionArray['filter'][$index] = $filter['page_description'];
                $queryData = array("page_description"=>$linkedDescription);
                $batchDBQuery [] = query_func_array2update('adseo_pagebodywidget', sanitizeDBValues($queryData),"id='{$filter[id]}'");
            }
	}
    $connection = \mysql_connect(DBSERVER.':'.DBPORT, DBUSER, DBPASS);
    mysql_set_charset('utf8',$connection);
    @\mysql_select_db(DBNAME) or die( "Unable to select database");
	$count = 0;
        foreach( $batchDBQuery as $query ){
	      var_dump($count);
	      $count++;
              $result = \mysql_query($query,$connection);
              if (!$result) {
                   echo $query;
              }
        }

        return $batchDBQuery;
    }
}

$handPickedKeywords = getAllHandPickedKeywordsHashedByKeyword();
$filterDescBatchQuery = makeFilterDescriptionCrossLinked($handPickedKeywords);//result can be used for log
//$statsDescsBatchQuery = makeStatsDescriptionCrossLinked($handPickedKeywords);//result can be used for log
//$styleDescsBatchQuery = makeStyleDescriptionCrossLinked($handPickedKeywords);//result can be used for log

//$retval = myntra\utils\db\throttleUpdatesDb::throttle($styleDescsBatchQuery);
//$retval = myntra\utils\db\throttleUpdatesDb::throttle($filterDescBatchQuery);
//$retval = myntra\utils\db\throttleUpdatesDb::throttle($statsDescsBatchQuery);
