<?php
chdir(dirname(__FILE__));
include_once("../auth_for_solr_index.php");
include_once ("$xcart_dir/modules/coupon/database/CouponAdapter.php");
$fp = fopen("privilege_generated_coupons.log", "a");
$ft = fopen("privilege_email_list.txt","r");

/**
Validate an email address.
Provide email address (raw input)
Returns true if the email address has the email 
address format and the domain exists.
*/
function validEmail($email)
{
   $isValid = true;
   $atIndex = strrpos($email, "@");
   if (is_bool($atIndex) && !$atIndex)
   {
      $isValid = false;
   }
   else
   {
      $domain = substr($email, $atIndex+1);
      $local = substr($email, 0, $atIndex);
      $localLen = strlen($local);
      $domainLen = strlen($domain);
      if ($localLen < 1 || $localLen > 64)
      {
         // local part length exceeded
         $isValid = false;
      }
      else if ($domainLen < 1 || $domainLen > 255)
      {
         // domain part length exceeded
         $isValid = false;
      }
      else if ($local[0] == '.' || $local[$localLen-1] == '.')
      {
         // local part starts or ends with '.'
         $isValid = false;
      }
      else if (preg_match('/\\.\\./', $local))
      {
         // local part has two consecutive dots
         $isValid = false;
      }
      else if (!preg_match('/^[A-Za-z0-9\\-\\.]+$/', $domain))
      {
         // character not valid in domain part
         $isValid = false;
      }
      else if (preg_match('/\\.\\./', $domain))
      {
         // domain part has two consecutive dots
         $isValid = false;
      }
      else if (!preg_match('/^(\\\\.|[A-Za-z0-9!#%&`_=\\/$\'*+?^{}|~.-])+$/', str_replace("\\\\","",$local)))
      {
         // character not valid in local part unless 
         // local part is quoted
         if (!preg_match('/^"(\\\\"|[^"])+"$/',
             str_replace("\\\\","",$local)))
         {
            $isValid = false;
         }
      }
      if ($isValid && !(checkdnsrr($domain,"MX") || checkdnsrr($domain,"A")))
      {
         // domain not found in DNS
         $isValid = false;
      }
   }
   return $isValid;
}

function generateCoupon($login,$adapter) {
	$minCartValue = 0.00;
	$startDate = strtotime(date('d F Y', time()));
	$validity = 60;
	$endDate = strtotime('31-12-2011');
	$couponCode = $adapter->generateMultipleUsableCouponForUser("privilege", "6", "Privilege", '20',$startDate, $endDate, $login, 'percentage', '', '10', '', "Privilege user coupons 10% off till 31st december 2011");	
	return $couponCode;
	
}

$customers_email = array();
while(!feof($ft))	{
	$line = fgets($ft);
	if ($line != "") {
		$customers_email[] = trim($line);
	}
}

$generated_coupons = 0;
echo "Started \n";
$adapter = CouponAdapter::getInstance();
foreach($customers_email as $email) {
	if(validEmail($email)) {
		//echo "$email is a valid email\n";
		$couponCode = generateCoupon(mysql_real_escape_string($email),$adapter,$fp);
		$line="$email:$couponCode\n";	
		$generated_coupons++;
	} else {
		$line="$email: is NOT a valid email\n";	
	}
        fputs($fp, $line);
}
fclose($fp);
fclose($ft);
echo "Finished \n";
echo "Generated ".$generated_coupons." coupons!\n";
?>
