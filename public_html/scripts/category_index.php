<?php
/*

Procedure to use this script
php product_index.php shopmasterid1 shopmasterid2 shopmasterid3   

*/

chdir(dirname(__FILE__));
include("../auth.php");
include("../include/solr/solrCategorySearch.php");
set_time_limit(0);

$solrProducts = new solrCategorySearch();
$solrProducts->emptyIndex();
$solrProducts->indexProducts();
?>

