<?php
/*****************************************************************************\
+-----------------------------------------------------------------------------+
| X-Cart                                                                      |
| Copyright (c) 2001-2006 Ruslan R. Fazliev <rrf@rrf.ru>                      |
| All rights reserved.                                                        |
+-----------------------------------------------------------------------------+
| PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
| FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
| AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
|                                                                             |
| THIS  AGREEMENT  EXPRESSES  THE  TERMS  AND CONDITIONS ON WHICH YOU MAY USE |
| THIS SOFTWARE   PROGRAM   AND  ASSOCIATED  DOCUMENTATION   THAT  RUSLAN  R. |
| FAZLIEV (hereinafter  referred to as "THE AUTHOR") IS FURNISHING  OR MAKING |
| AVAILABLE TO YOU WITH  THIS  AGREEMENT  (COLLECTIVELY,  THE  "SOFTWARE").   |
| PLEASE   REVIEW   THE  TERMS  AND   CONDITIONS  OF  THIS  LICENSE AGREEMENT |
| CAREFULLY   BEFORE   INSTALLING   OR  USING  THE  SOFTWARE.  BY INSTALLING, |
| COPYING   OR   OTHERWISE   USING   THE   SOFTWARE,  YOU  AND  YOUR  COMPANY |
| (COLLECTIVELY,  "YOU")  ARE  ACCEPTING  AND AGREEING  TO  THE TERMS OF THIS |
| LICENSE   AGREEMENT.   IF  YOU    ARE  NOT  WILLING   TO  BE  BOUND BY THIS |
| AGREEMENT, DO  NOT INSTALL OR USE THE SOFTWARE.  VARIOUS   COPYRIGHTS   AND |
| OTHER   INTELLECTUAL   PROPERTY   RIGHTS    PROTECT   THE   SOFTWARE.  THIS |
| AGREEMENT IS A LICENSE AGREEMENT THAT GIVES  YOU  LIMITED  RIGHTS   TO  USE |
| THE  SOFTWARE   AND  NOT  AN  AGREEMENT  FOR SALE OR FOR  TRANSFER OF TITLE.|
| THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY GRANTED BY THIS AGREEMENT.      |
|                                                                             |
| The Initial Developer of the Original Code is Ruslan R. Fazliev             |
| Portions created by Ruslan R. Fazliev are Copyright (C) 2001-2006           |
| Ruslan R. Fazliev. All Rights Reserved.                                     |
+-----------------------------------------------------------------------------+
\*****************************************************************************/

#
# $Id: order.php,v 1.60 2006/01/11 06:55:57 mclap Exp $
#

require "./auth.php";
//require $xcart_dir."/include/security.php";
include_once($xcart_dir."/include/func/func.mk_orderbook.php");

#
# Collect infos about ordered products
#
if(!empty($XCART_SESSION_VARS['identifiers']))
     $auth_type_admin = $XCART_SESSION_VARS['identifiers']['A']; 

if(empty($auth_type_admin)){
    $errormessage = "We are sorry, you are not authorized to access this page. Please login to access the shipping details.";
    $XCART_SESSION_VARS['errormessage'] = $errormessage;
    header("Location:mksystemerror.php");
	exit;
}
$orderid = $_GET['orderid'];
$smarty->assign("orderid", $orderid);

$login = func_query_first("SELECT login FROM $sql_tbl[orders] WHERE orderid='".$orderid."'");
$customerdetail = func_customer_order_address_detail($login['login'], $orderid);

if($customerdetail['s_country'] == 'IN' || $customerdetail['s_country'] == 'US'){
$customerdetail['s_state'] = func_get_state_label($customerdetail['s_state']);
}
$customerdetail['s_country'] = func_get_country_label($customerdetail['s_country']);

$smarty->assign("userinfo", $customerdetail);


//$smarty->assign("main","print_address_label");

# Assign the current location line
//$smarty->assign("location", $location);

//@include $xcart_dir."/modules/gold_display.php";
func_display("print_shipping.tpl",$smarty);
?>
