<?php
require "./auth.php";
include_once("./include/func/func.mkcore.php");

//$smarty->assign("location", $location);
$breadCrumb=array(
		       array('reference'=>'home.php','title'=>'home'),
                array('reference'=>'#','title'=>'site map')
              );
$smarty->assign("breadCrumb",$breadCrumb);

//load product types
$productTypes =func_load_product_type();
$smarty->assign("producttypes", $productTypes); 

$availableProductType=func_get_all_product_types();
$smarty->assign("productType",$availableProductType);


func_display("sitemap.tpl",$smarty);
?>