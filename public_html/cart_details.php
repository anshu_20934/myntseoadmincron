<?php

require_once ("$xcart_dir/include/func/func.mkmisc.php");
require_once ("$xcart_dir/include/class/class.mymyntra.php");
require_once ("$xcart_dir/include/class/mcart/class.MCart.php");
use abtest\MABTest;

use enums\cart\CartContext;

//AB Test for recommendations on cart (price range based)
$recommendationsCart = MABTest::getInstance()->getUserSegment('recCart'); 
$smarty->assign("recommendationsCart", $recommendationsCart);
$cartRecommendationsAmount = WidgetKeyValuePairs::getWidgetValueForKey('cartRecommendationsAmount');
$cartRecommendationsHighLimit = WidgetKeyValuePairs::getWidgetValueForKey('cartRecommendationsHighLimit');

$mcartFactory= new MCartFactory();
if(defined('ONE_CLICK')){
	$myCart= $mcartFactory->getCurrentUserCart(false, CartContext::OneClickContext);
}else {
	$myCart= $mcartFactory->getCurrentUserCart();
}
$smarty->assign("isGiftOrder",0);
if($myCart!=null){
    // Gopi:
    // OMS compares the 'last cart update time' stored in the session against the one comes from the cart object.
    // This time was updated in mkpaymentsoptions_re.php in the session. 
    // But, in the new checkout flow, cart service is used directly on the client side. So, it is updated here
    unset($XCART_SESSION_VARS["LAST_CART_UPDATE_TIME"]);
    x_session_unregister('LAST_CART_UPDATE_TIME');
    x_session_register('LAST_CART_UPDATE_TIME',$myCart->getLastContentUpdatedTime());

	$mCartItems=$myCart->getProducts();
	$isGiftOrder = $myCart->getIsGiftOrder();
	if(!empty($isGiftOrder)){
		$smarty->assign("isGiftOrder",1);
		$gMsg =  $myCart->getGiftMessage();
		$gArray = explode("___",$gMsg);
		$smarty->assign("giftTo",$gArray[0]);
		$smarty->assign("giftFrom",$gArray[1]);
		$smarty->assign("giftMessage",$gArray[2]);	
	}
}

if($recommendationsCart == "test")
{
	$smarty->assign("recProductsInCart","false");
}


$styleIds = '';
// Cart contains items.
if(!empty($mCartItems))
{
	$myCart->refresh(false,false,false,false);
	MCartUtils::persistCart($myCart);
	$productArray = MCartUtils::convertCartToArray($myCart);

    // cart items exposed for re-marketing
    $_cart = array();
    foreach ($productArray as $_id => $_product) {
        $_item = array();
        $styleIds .= $_product['productStyleId'].',';
        $_item['id'] = $_product['productStyleId'];
        $_item['price'] = $_product['productPrice'];
        $_item['qty'] = $_product['quantity'];
        array_push($_cart, $_item);
    }
    $smarty->assign("_cart", $_cart);
    $smarty->assign("_cartItems", json_encode($_cart));


	$additionalCharges = number_format($myCart->getAdditionalCharges(), 2, ".", '');
	$totalMRP = number_format($myCart->getMrp(), 2, ".", '');
	$vat = number_format($myCart->getVat(), 2, ".", '');
	$totalQuantity = number_format($myCart->getItemQuantity(), 0);
	$couponDiscount = number_format($myCart->getDiscount(), 2, ".", '');
	$cashDiscount = number_format($myCart->getCashDiscount(), 2, ".", '');
	$myntCashUsage = number_format($myCart->getTotalMyntCashUsage(), 2, ".", '');
	$productDiscount = number_format($myCart->getDiscountAmount(), 2, ".", '');
	$shippingCharge = number_format($myCart->getShippingCharge(), 2, ".",'');
	$giftCharges = number_format($myCart->getGiftCharge(), 2, ".",'');
	$cartLevelDiscount = round(number_format($myCart->getCartLevelDiscount(), 2, ".", ''));
	$loyaltyPointsUsageInCash = number_format($myCart->getLoyaltyPointsCashUsed(), 2, ".", '');
	
	$totalAmount = $totalMRP + $additionalCharges - $couponDiscount - $myntCashUsage - $productDiscount - $cartLevelDiscount + $shippingCharge + $giftCharges -$loyaltyPointsUsageInCash;
	$cashBackDisplayAmount= $totalMRP + $additionalCharges - $productDiscount - $couponDiscount;
	$coupondiscount = $couponDiscount;
	$cashdiscount = $cashDiscount;
	
	
	
	$mrp = $totalMRP;
	$amount = $totalMRP + $shippingCharge + $giftCharges;
	$savings = $productDiscount + $couponDiscount + $myntCashUsage + $cartLevelDiscount;
	if( $totalAmount < 1) { //less than 1re payment is not accepted by banks to making the amount to zero in case final amount falls below 1re.
		$totalAmount = 0;
	}
    $nonDiscountedItemAmount=$myCart->getNonDiscountedItemsSubtotal();
    $nonDiscountedItemCount=$myCart->getNonDiscountedItemCount();
    $couponAppliedItemsCount=$myCart->getCouponAppliedItemCount();
    $couponAppliedSubtotal=$myCart->getCouponAppliedSubtotal();
    
	$totalAmount = round($totalAmount);
	$vat = ($vat < 0 ? 0 : $vat);

	global $enable_cashback_discounted_products;

	if(!empty($enable_cashback_discounted_products)){
		if($enable_cashback_discounted_products == "true")	{
			$totalCashBackAmount = $totalAmount;
		} else {
			$totalCashBackAmount = $myCart->getTotalCashBackAmount();
		}
	} else {
		$totalCashBackAmount = $myCart->getTotalCashBackAmount();
	}

	if($totalCashBackAmount < 0)	{
		$totalCashBackAmount = 0;
	}

	if(!empty($enable_cashback_discounted_products)){
		if($enable_cashback_discounted_products == "true")	{
			$cashBackAmountDisplayOnCart = $cashBackDisplayAmount;
		} else {
			$cashBackAmountDisplayOnCart = $myCart->getCashBackAmountForDisplay();
		}
	} else {
		$cashBackAmountDisplayOnCart = $myCart->getCashBackAmountForDisplay();
	}

	if($cashBackAmountDisplayOnCart < 0)	{
		$cashBackAmountDisplayOnCart = 0;
	}

	global $system_free_shipping_amount;
	if($shippingCharge > 0 && ($amount-$productDiscount)<$system_free_shipping_amount){
		$smarty->assign("cartMessage", $_rst['Cart_Free_Shipping_Msg']);
	}
	
	//Computations for showing avail recommendations.
	if($recommendationsCart == "test")
	{
		$productid;
		$tempPrice=0.0;
		$gender="Men";
		foreach($productArray as $product){	
			if($product["productPrice"]>=$tempPrice){
				$productid= $product["productStyleId"];
				$tempPrice = $product["productPrice"];
				$gender = $product["gender"];
				$article_id = $product["article_type"];
			}
		}
 		$article_typename_sql = "select typename from mk_catalogue_classification where id = '".mysql_real_escape_string($article_id)."'"; 		
		$out = func_query($article_typename_sql,true);
		if ($out)   	//if the query does not return results
			$article_typename = $out[0][typename];
		else
			$article_typename = "";
		
		$smarty->assign("gender",$gender);
		$smarty->assign("productid",$productid);
		$availRecLowerLimit = 1;		
		$smarty->assign("availRecLowerLimit",$availRecLowerLimit);
		$availRecHigherLimit = ($totalAmount*($cartRecommendationsAmount/100));
		$smarty->assign("cartRecommendationsAmount",$cartRecommendationsAmount);
		$smarty->assign("cartRecommendationsHighLimit",$cartRecommendationsHighLimit);
		$smarty->assign("availRecHigherLimit",$availRecHigherLimit);
		$cartRecommendationsMapping = WidgetKeyValuePairs::getWidgetValueForKey('cartRecommendationsMapping');
		$cartRecommendationsMappingArray = json_decode($cartRecommendationsMapping,true);
		$recommendationCategory = $cartRecommendationsMappingArray[$article_typename];
		
		$avail_dynamic_param_match_with ="";
		for($i = 0; $i < count($recommendationCategory)-1; ++$i) {
			$avail_dynamic_param_match_with = $avail_dynamic_param_match_with.$recommendationCategory[$i].",";
		}
		$avail_dynamic_param_match_with = $avail_dynamic_param_match_with.$recommendationCategory[$i];		 
		$smarty->assign("avail_dynamic_param_match_with",$avail_dynamic_param_match_with);
		$smarty->assign("recProductsInCart","true");
	}
	
    $condNoDiscountCouponFG = FeatureGateKeyValuePairs::getBoolean('condNoDiscountCoupon');
    $smarty->assign('condNoDiscountCouponFG', $condNoDiscountCouponFG);
	
    $styleExclusionFG = FeatureGateKeyValuePairs::getBoolean('styleExclusion');
    $smarty->assign('styleExclusionFG', $styleExclusionFG);
    
	$smarty->assign("vat", $vat);
	$smarty->assign("vatCharge", $additionalCharges);
	$smarty->assign("amount", $amount);
	$smarty->assign("totalAmount", $totalAmount);   // totalAmount and totalMRP are same if there is no discount coupon
	$smarty->assign("totalItems", $totalQuantity);
	$smarty->assign("mrp", $mrp);
	$smarty->assign("couponDiscount", $couponDiscount);
	$smarty->assign("cashdiscount", $cashdiscount);
	$smarty->assign("totalCashBackAmount", $totalCashBackAmount);
	$smarty->assign("cashBackAmountDisplayOnCart", $cashBackAmountDisplayOnCart);
	$smarty->assign("shippingCharge", $shippingCharge);
	$smarty->assign("giftCharge", $giftCharges);
	$smarty->assign("savings", $savings);
	$smarty->assign("cartLevelDiscount", $cartLevelDiscount);

	$smarty->assign("myntCashUsage",$myntCashUsage);


	
    $smarty->assign("couponAppliedItemsCount", $couponAppliedItemsCount);
    $smarty->assign("couponAppliedSubtotal", $couponAppliedSubtotal);
    $smarty->assign("nonDiscountedItemAmount", $nonDiscountedItemAmount);
    $smarty->assign("nonDiscountedItemCount", $nonDiscountedItemCount);
    

	$productsInCart=$productArray;
}
else
{
	//
	// There are no products in the shopping cart.
	//

	// Mark the empty cart (array).
	$productsInCart = array();
	$totalQuantity=0;
	$smarty->assign("productMessage", "Your shopping cart contains no items");
}
if(!empty($productsInCart)){
	foreach($productsInCart as $idx => $product){
		if($product["flattenSizeOptions"]){
			$flattenSizeOptions=true;
			break;
		}
	}
}

$productsInCartByDiscount = array();
if(!empty($productsInCart)){
	foreach($productsInCart as $idx => $product){
		if(!empty($product["discountAmount"])) {
            //NOTE: BuyXGetY change: discountQuantity != quantity
			$pricePerItemAfterDiscount = $product["productPrice"]-($product["discountAmount"]/$product["quantity"]);
			$product['pricePerItemAfterDiscount'] = $pricePerItemAfterDiscount;
			$productsInCart[$idx] = $product;
		}
		if(!empty($product['discountDisplayText'])){

			foreach($product['discountDisplayText']->params as $k=>$v) {
				$product['dre_'.$k] = $v;
			}
			$product['discountDisplayIcon'] = $_rst[$product['discountDisplayText']->icon];
			$product['discountDisplayText'] = $_rst[$product['discountDisplayText']->id];
			$productsInCart[$idx] = $product;
		}
	}
	
	$comboDispData = MCartUtils::getComboDisplayData($myCart);
	
	foreach ($productsInCart as $idx => $product) {
		$comboId = $product["discountComboId"];
		if(empty($comboId)){
			$comboId = 0;
		}
		$productSet = $productsInCartByDiscount[$comboId]['productSet'];
		if(empty($productSet))
		$productSet = array();
		$productSet[$idx] = $product;

		foreach($comboDispData as $cId => $comboData){
			if($cId!=$comboId) continue;
			$productsInCartByDiscount[$comboId]['icon'] = $_rst[$comboData->icon];
			$productsInCartByDiscount[$comboId]['label_text'] = $_rst[$comboData->id];
			$productsInCartByDiscount[$comboId]['is_sorted'] = $comboData->params->isSorted;
			if(!empty($comboData->params)){
				foreach($comboData->params as $k=>$v) {
					$productsInCartByDiscount[$comboId]['dre_'.$k] = $v;
				}
			}
		}

        $productsInCartByDiscount[$comboId]['comboTotal'] += ($product["productPrice"] * $product['quantity']);
		if(!empty($product["discountAmount"])){
			$productsInCartByDiscount[$comboId]['comboSavings'] += 
            (($product["productPrice"]-$product['pricePerItemAfterDiscount'])*$product['quantity']);
		}
        $productsInCartByDiscount[$comboId]['productSet'] = $productSet;
        if (!isset($productsInCartByDiscount[$comboId]['comboSavings'])) {
            $productsInCartByDiscount[$comboId]['comboSavings'] = 0;
        }
	}
}

//sort the combos

foreach ($productsInCartByDiscount as $id => $combo) {
    if ($combo['is_sorted'] === true) {
        $sortOnPrice = array();
        $productSet = $combo['productSet'];
        foreach ($productSet as $idx => $product) {
           $unitPrice = $product['productPrice'];
           $isDiscounted = $product['discountAmount'];
           if (!isset($sortOnPrice[$unitPrice]))
               $sortOnPrice[$unitPrice] = array();
           $sortOnPrice[$unitPrice][$idx] = $isDiscounted;
        }
        krsort($sortOnPrice);
        $newProductSet = array();
        foreach ($sortOnPrice as $priceGroup) {
            asort($priceGroup);
            foreach ($priceGroup as $idx => $value) {
                $newProductSet[$idx] = $productSet[$idx];
            }
        }
        $productsInCartByDiscount[$id]['productSet'] = $newProductSet;
    }
}


$cartDisplayData = MCartUtils::getCartDisplayData($myCart);
$freeChoiceDiscountMessageItemLevel = '';
if(!empty($cartDisplayData)){
	foreach ($cartDisplayData->params as $k => $v) {
		$smarty->assign("dre_cart_".$k, $v);
	}
	$cartDiscountMessageBannerTop ='';
	if($cartDisplayData->id=="Voucher_Cart_CartLevel_CA_NM") {
		$cartDiscountMessageBannerTop = WidgetKeyValuePairs::getWidgetValueForKey('DiscVoucherBannerCartTopNM');
	} else if($cartDisplayData->id=="Voucher_Cart_CartLevel_CM") {
		$cartDiscountMessageBannerTop = WidgetKeyValuePairs::getWidgetValueForKey('DiscVoucherBannerCartTopCM');
	}
	if($cartDisplayData->id == 'Free_Item_CartLevel_CM_MC'){
		$freeChoiceDiscountMessageItemLevel = $_rst['Free_Item_CartLevel_CM_MC_IL'];
	}
	$smarty->assign("cartDiscountMessageBannerTop", $cartDiscountMessageBannerTop);
	$smarty->assign("cartDiscountMessage", $_rst[$cartDisplayData->id]);	
	if($cartDisplayData->tooltip_exclusion_id){
		$smarty->assign("cartDiscountExclusionTT", $_rst[$cartDisplayData->tooltip_exclusion_id]);
	}
	
      //$smarty->assign("cartVoucherLinkTT",$_rst[$cartDisplayData->tooltip_id]);
}

$excludedArticleTypes = WidgetKeyValuePairs::getWidgetValueForKey('cart-tooltip-for-coupon');

$smarty->assign("couponExclusionTT", $excludedArticleTypes);



$cartSlabDisplayData = MCartUtils::getCartSlabDisplayData($myCart);
if(!empty($cartSlabDisplayData)){
	foreach ($cartSlabDisplayData->params as $k => $v) {
		$smarty->assign("dre_cart_slab_".$k, $v);
	}
	$smarty->assign("cartSlabDiscountMessage",$_rst[$cartSlabDisplayData->id]);	
}

$smarty->assign("freeChoiceDiscountMessageItemLevel",$freeChoiceDiscountMessageItemLevel);
$smarty->assign("flattenSizeOptions", $flattenSizeOptions);
$smarty->assign("productsInCart", $productsInCartByDiscount);
$smarty->assign("totalQuantity", $totalQuantity);
$smarty->assign("eossSavings", $productDiscount);
$smarty->assign("itemsCount", count($productsInCart));

$product_availability;
$sku_info = array();
if($myCart!= null){
    if(count($myCart->getProductIdsInCart()))
        $items_out_of_stock=!$myCart->isReadyForCheckout();

	foreach($myCart->getItemsAvailabilityArray() as $key_index=> $availability){
		$product_availability[$key_index]=$availability['stock'];
		if($availability['intotaloos']===true){
			$product_intotal_out_of_stock=true;
		}
	}
    
    foreach ($myCart->getProducts() as $product) {
        $prodId = $product->getProductId();
        $skuArray = $product->getSKUArray();
        $sku_info[$prodId] = $skuArray;
    }
}

//savedItems set in parent php
// BEGIN: saved items list
global $savedItemAction;
use saveditems\action\SavedItemAction;
if(!isset($savedItemAction)) {
    $savedItemAction = new SavedItemAction();
}
$savedItemsAvailability = $savedItemAction->getSavedItemsAvailabilityArray();
$savedItems = $savedItemAction->getProductsInSaved();
foreach ($savedItemsAvailability as $key_index => $availability) {
	$product_availability[$key_index]=$availability['stock'];
}
foreach ($savedItems as $product) {
	$prodId = $product->getProductId();
	$skuArray = $product->getSKUArray();
	$sku_info[$prodId] = $skuArray;
}

$checkInStock=FeatureGateKeyValuePairs::getFeatureGateValueForKey('checkInStock');
$smarty->assign("checkInStock", $checkInStock==='true');
/* code to have sports jersey store products
 * name and number to show in cart
 * added by arun
 */
$smarty->assign('sku_info', $sku_info);

$smarty->assign("items_out_of_stock",$items_out_of_stock);
//$smarty->assign("items_zero_quantity",$items_zero_quantity);
$smarty->assign("product_intotal_out_of_stock",$product_intotal_out_of_stock);
//$smarty->assign("total_consolidated_sale",$total_consolidated_sale);
$smarty->assign("product_availability",$product_availability);
$smarty->assign("styleIds",rtrim($styleIds,','));

