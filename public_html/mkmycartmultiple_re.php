<?php
include_once("./auth.php");

include_once("$xcart_dir/include/class/instrumentation/BaseTracker.php");
$tracker = new BaseTracker(BaseTracker::CART_PAGE);

//MOBILE-90
//setting the skin to session for skin conditional coupons
//we have to set the skin value to session at any place where coupons can be applied
if(x_session_is_registered("SKIN")){
    x_session_unregister("SKIN");   
}
x_session_register('SKIN',$skin);

//AVAIL
include_once(HostConfig::$documentRoot."/include/func/func.utilities.php");
include_once("$xcart_dir/utils/Helper.php");
include_once($xcart_dir."/myntra/setShowStyleDetails.php");
include_once("$xcart_dir/cart_details.php");
include_once "$xcart_dir/include/class/class.feature_gate.keyvaluepair.php";
$shownewui= $_MABTestObject->getUserSegment("productui");

$pageName = 'cart';
include_once $xcart_dir."/webengage.php";

//breadcrumb
$pageType=strip_tags(filter_input(INPUT_GET,'pagetype', FILTER_SANITIZE_STRING));
$pageType=sanitize_paranoid_string($pageType);

$product_custom_display_forcart = $XCART_SESSION_VARS['product_custom_display_forcart'];
$smarty->assign("product_custom_display_forcart",$product_custom_display_forcart);
$smarty->assign("pagetype",$pageType);
$smarty->assign("windowid",filter_input(INPUT_GET,'windowid',FILTER_SANITIZE_STRING));
$smarty->assign("useractionclearcart",$useractionclearcart);
$smarty->assign("selectedaddressid", $XCART_SESSION_VARS['selected_address']);
/*Create a smarty variable to hold the user's myntra credit */

//AVAIL
$avail_cart_abtest= $_MABTestObject->getUserSegment("avail_cart_abtest");
$avail_params = array();
$avail_session_id_content = $_COOKIE['__avail_session__']; //this is not a myntra cookie set by avail and will have this name only
if(!empty($avail_session_id_content)){
    $avail_session_id=$avail_session_id_content;
}
$smarty->assign("avail_cart_abtest", $avail_cart_abtest);
if(!empty($_GET['av_id_added'])){
    $productId = filter_input(INPUT_GET,'av_id_added', FILTER_SANITIZE_STRING);
    $smarty->assign("av_id_added",$productId);
    $avail_params[] = array("method"=>"logAddedToCart","params"=>array("ProductID"=>filter_input(INPUT_GET,'av_id_added', FILTER_SANITIZE_STRING),"SessionID"=>$avail_session_id));
}
if(!empty($_GET['av_id_removed'])){
    $smarty->assign("av_id_removed",filter_input(INPUT_GET,'av_id_removed', FILTER_SANITIZE_STRING));
    $av_id_removed = explode(',',$av_id_removed);
    foreach($av_id_removed as $styleId){
        $avail_params[] = array("method"=>"logRemovedFromCart","params"=>array("ProductID"=>$styleId,"SessionID"=>$avail_session_id));
    }

}
if(!empty($_GET['product'])){
    $productsCount = filter_input(INPUT_GET,'product', FILTER_SANITIZE_STRING);
}
if(!empty($_GET['uq'])){
    $smarty->assign("uq",filter_input(INPUT_GET,'uq', FILTER_SANITIZE_STRING));
}
if(!empty($_GET['uqs'])){
    $smarty->assign("uqs",filter_input(INPUT_GET,'uqs', FILTER_SANITIZE_STRING));
}
if(!empty($_GET['t_code'])){
    $smarty->assign("t_code",filter_input(INPUT_GET,'t_code', FILTER_SANITIZE_STRING));
}

    //availswitchoff    

// if(!empty($avail_params)){
//     $url = Helper::getPropertyValue('recoServiceUrl');
//     post_async(HostConfig::$selfHostURL."/asyncGetPostHandler.php",array("url"=>$url,"method"=>"POST","data"=>json_encode($avail_params),"user"=>$user));
// }
//END AVAIL

$analyticsObj = AnalyticsBase::getInstance();
$analyticsObj->setContextualPageData(AnalyticsBase::CART_PAGE);
if($skin=='skin1') {
    // In old UI, these the product added, and productCount is passed in URL.
    if(!empty($productId)) {
        $analyticsObj->setCartEventVars($productsCount, $productId);
    }
}
elseif ($skin == 'skin2') {
    // For new UI, passing productAdded and Count details as URL parameters from mkretrievedataforcart_re.php
    $productCountinCart = filter_input(INPUT_GET,'productCountinCart', FILTER_SANITIZE_STRING);
    $productAdded = filter_input(INPUT_GET,'productAdded', FILTER_SANITIZE_STRING);
    if(!empty($productAdded)) {
        $cartLog = array();
        $cartLog["product_id"]= $productAdded;
        global $login;
        $cartLog["user_id"] = $login;
        global $telesalesUser;
        $cartLog["agent_id"] = $telesalesUser;
        $cartLog["action"]="productAdded";
        global $telesales;
        if ($telesales){
            func_array2insertWithTypeCheck("Cart_log",$cartLog);
        }
        $analyticsObj->setCartEventVars($productCountinCart, $productAdded);
        $smarty->assign('productAdded',$productAdded);
    }
}
$analyticsObj->setTemplateVars($smarty);

$tracker->fireRequest();
$smarty->assign('redirectSource',filter_input(INPUT_GET,'src',FILTER_SANITIZE_STRING));

// BEGIN: saved items list
global $savedItemAction;
if (empty($savedItemAction)) {
    $savedItemAction = new SavedItemAction();
}

$savedItems = $savedItemAction->getProductsInSaved();
$savedArray = MCartUtils::convertCartToArray($savedItemAction->getSaved());
$totalSavedItemCount = $savedItemAction->getSavedItemCount();
$savedProducts = array();

global $_rst;
if(!empty($savedArray)){
    foreach($savedArray as $idx => $product){
        if(!empty($product["discountAmount"])) {
            $pricePerItemAfterDiscount = round($product["productPrice"]-($product["discountAmount"]/$product["quantity"]));
            $product['pricePerItemAfterDiscount'] = $pricePerItemAfterDiscount;
            $savedArray[$idx] = $product;
        }
        if(!empty($product['discountDisplayText'])){
            foreach($product['discountDisplayText']->params as $k=>$v) {
                $product['dre_'.$k] = $v;
            }
            $product['discountDisplayIcon'] = $_rst[$product['discountDisplayText']->icon];
            $product['discountDisplayText'] = $_rst[$product['discountDisplayText']->id];
            $savedArray[$idx] = $product;
        }
    }

    foreach ($savedArray as $idx => $product) {
        $comboId = $product["discountComboId"];
        if(empty($comboId)){
            $comboId = 0;
        }
        $productSet = $savedProducts[$comboId]['productSet'];
        if(empty($productSet))
        $productSet = array();
        $productSet[$idx] = $product;
        $comboDispData = MCartUtils::getComboDisplayData($savedItemAction->getSaved());
        global $_rst;
        foreach($comboDispData as $cId => $comboData){
            if($cId!=$comboId) continue;
            $savedProducts[$comboId]['icon'] = $_rst[$comboData->icon];
            $savedProducts[$comboId]['label_text'] = $_rst[$comboData->id];
            if(!empty($comboData->params)){
                foreach($comboData->params as $k=>$v) {
                    $savedProducts[$comboId]['dre_'.$k] = $v;
                }
            }
        }
        
        $savedProducts[$comboId]['comboTotal'] = ($product["productPrice"] * $product['quantity']) + $savedProducts[$comboId]['comboTotal'];
        if(!empty($product["discountAmount"])){
            $savedProducts[$comboId]['comboSavings'] = (($product["productPrice"] - $product['pricePerItemAfterDiscount']) * $product['quantity']) + $savedProducts[$comboId]['comboSavings'];
        }
        $savedProducts[$comboId]['productSet'] = $productSet;
        if (!isset($savedProducts[$comboId]['comboSavings'])) {
            $savedProducts[$comboId]['comboSavings'] = 0;
        }
    }
}

$smarty->assign('savedItems', $savedProducts);
// END: saved items list

//cart resources picked from payments page
// Instance of the coupon database liaison.
if($skin=='skinmobile'){
$adapter = CouponAdapter::getInstance();
$mcartFactory= new MCartFactory();
$myCart= $mcartFactory->getCurrentUserCart();
    if($myCart!=null) {
        $cartItems=$myCart->getProducts();
    }
        
    $additionalCharges = number_format($myCart->getAdditionalCharges(), 2, ".", '');
    $totalMRP = number_format($myCart->getMrp(), 2, ".", '');
    $vat = number_format($myCart->getVat(), 2, ".", '');
    $totalQuantity = number_format($myCart->getItemQuantity(), 0);
    $couponDiscount = number_format($myCart->getDiscount(), 2, ".", '');
    $cashDiscount = number_format($myCart->getCashDiscount(), 2, ".", '');
    $myntCashUsage = number_format($myCart->getTotalMyntCashUsage(), 2, ".", '');
    $productDiscount = number_format($myCart->getDiscountAmount(), 2, ".", '');
    $shippingCharge = number_format($myCart->getShippingCharge(), 2, ".",'');
    $cartDiscount = number_format($myCart->getCartLevelDiscount(), 2, ".", '');
    $giftCharges = number_format($myCart->getGiftCharge(), 2, ".",'');
    $totalAmount = $totalMRP + $additionalCharges - $productDiscount - $couponDiscount - $myntCashUsage + $shippingCharge - $cartDiscount + $giftCharges;
    $coupondiscount = $couponDiscount;
    $cashdiscount = $myntCashUsage;
   
    $totalItemsAmount = $totalMRP - $productDiscount;

    $mrp = $totalMRP;
    $amount = $totalMRP;
    $cashback_tobe_given=$myCart->getCashbackToGivenOnCart();
    $nonDiscountedItemAmount=$myCart->getNonDiscountedItemsSubtotal();
    $nonDiscountedItemCount=$myCart->getNonDiscountedItemCount();
    $couponApplicableItemAmount=$myCart->getCouponApplicableItemsSubtotal();
    $couponApplicableItemCount=$myCart->getcouponApplicableItemCount();
    $couponAppliedItemsCount=$myCart->getCouponAppliedItemCount();
    $couponAppliedSubtotal=$myCart->getCouponAppliedSubtotal();

    $amountForCitiDiscount = $totalAmount + $coupondiscount;
    $smarty->assign("vatCharge", $additionalCharges);
    $smarty->assign("amountForCitiDiscount",$amountForCitiDiscount);
    $smarty->assign("amount",$amount);
    $smarty->assign("vat",$vat);
    $smarty->assign("mrp",$mrp);
    $smarty->assign("couponCode",$couponCode);
    $smarty->assign("coupondiscount",$coupondiscount);
    $smarty->assign("couponAppliedItemsCount", $couponAppliedItemsCount);
    $smarty->assign("couponAppliedSubtotal", $couponAppliedSubtotal);
    $smarty->assign("cashdiscount",$cashdiscount);
    $smarty->assign("myntCashUsage",$myntCashUsage);
    $smarty->assign("coupondiscounttodisplay",$coupondiscounttodisplay);
    $smarty->assign("couponpercent", $couponpercent);
    $smarty->assign("totalAmount",$totalAmount);
    $smarty->assign("totalQuantity",$totalQuantity);
    $smarty->assign("productDiscount",$productDiscount);
    $smarty->assign("cartLevelDiscount",$cartDiscount);
    $smarty->assign("totalCashBackAmount", $totalCashBackAmount);
    $smarty->assign("nonDiscountedItemAmount", $nonDiscountedItemAmount);
    $smarty->assign("nonDiscountedItemCount", $nonDiscountedItemCount);
    $smarty->assign("couponApplicableItemAmount", $couponApplicableItemAmount);
    $smarty->assign("couponApplicableItemCount", $couponApplicableItemCount);
    $smarty->assign("totalSavedItemCount", $totalSavedItemCount);
    $smarty->assign("today", time());
    
    $condNoDiscountCouponFG = FeatureGateKeyValuePairs::getBoolean('condNoDiscountCoupon');
    $smarty->assign('condNoDiscountCouponFG', $condNoDiscountCouponFG);

    $styleExclusionFG = FeatureGateKeyValuePairs::getBoolean('styleExclusion');
    $smarty->assign('styleExclusionFG', $styleExclusionFG);
    
    $noStyleExclusionForUserCoupon = FeatureGateKeyValuePairs::getBoolean('noStyleExclusionForUserCoupon');
    $smarty->assign('noStyleExclusionForUserCoupon', $noStyleExclusionForUserCoupon);

    $noStyleExclusionForSomeGroups = FeatureGateKeyValuePairs::getBoolean('noStyleExclusionForSomeGroups');
    $smarty->assign('noStyleExclusionForSomeGroups', $noStyleExclusionForSomeGroups);

    // SMS verification overlay.
    $login = $XCART_SESSION_VARS["login"];
    $query = "SELECT * FROM xcart_customers WHERE login = '$login';";
    $userProfileData = func_query_first($query);
    $smarty->assign("userProfileData", $userProfileData);

    // Mobile verification status.
    $smsVerifier = SmsVerifier::getInstance();
    $mobile_status = $smsVerifier->getStatus($userProfileData['mobile'], $userProfileData['login']);
    global $skipMobileVerification;
    if($mobile_status != 'V' && $skipMobileVerification){
        $smsVerifier->skipVerification($userProfileData['mobile'], $userProfileData['login']);
        $mobile_status = $smsVerifier->getStatus($userProfileData['mobile'], $userProfileData['login']);
    }

    // Show verify button.
    $verified_for_another = 0;
    $verifiedLogin = $smsVerifier->getVerifiedLoginFor($userProfileData['mobile']);
    if ($verifiedLogin !== null && $verifiedLogin != $userProfileData['login']) {
        $verified_for_another = 1;
    }
    $smarty->assign("verified_for_another", $verified_for_another);
    $smarty->assign("mobile_status", $mobile_status);
    $smarty->assign("userMobile",$userProfileData['mobile']);
    $cartDisplayData = MCartUtils::getCartDisplayData($myCart);
    if(!empty($cartDisplayData) && $cartDisplayData->id=="Voucher_Cart_CartLevel_CM"){
        foreach ($cartDisplayData->params as $k => $v) {
            $smarty->assign("dre_cart_".$k, $v);
        }
        $smarty->assign("cartDiscountMessage", $_rst[$cartDisplayData->id]);
        $smarty->assign("cartVoucherLinkTT",$_rst[$cartDisplayData->tooltip_id]);
    }
}
//Kundan: the qty number to show by default in combo box, if widget key value pair is not set
$cartItemMaxQty = WidgetKeyValuePairs::getInteger('cart-item-max-qty',10);
$smarty->assign('cartItemMaxQty', $cartItemMaxQty);

$condNoDiscountCouponFG = FeatureGateKeyValuePairs::getBoolean('condNoDiscountCoupon');
$smarty->assign('condNoDiscountCouponFG', $condNoDiscountCouponFG);


$savedStatus=filter_input(INPUT_GET,'status', FILTER_SANITIZE_NUMBER_INT);
$styleSaved=array(filter_input(INPUT_GET,'style', FILTER_SANITIZE_NUMBER_INT));
$socialPost=0;
$socialArray=array();
$styleDetailsArray=array();
if($savedStatus){
    require_once("$xcart_dir/socialInit.php");
    $socialArray=getFormattedArrayForSocial($styleSaved);
    $styleDetailsArray = getStyleDataArrayForSocial($styleSaved);
    if(!empty($socialArray)){
        $socialPost=1;
    }
}

$smarty->assign('socialPost',$socialPost);
$smarty->assign('socialArray',json_encode($socialArray));
$smarty->assign('socialPostStyleDetailsArray',json_encode($styleDetailsArray));

$giftWrapEnabled = FeatureGateKeyValuePairs::getBoolean('giftwrap.enabled');
$smarty->assign('giftWrapEnabled', $giftWrapEnabled);

$smarty->assign("isCart",true);
$giftingCharges = WidgetKeyValuePairs::getInteger('giftwrap.charges');
$smarty->assign('giftingCharges', $giftingCharges);

$myntraShopFestCoupons = FeatureGateKeyValuePairs::getBoolean("myntrashoppingfestCoupons");
$smarty->assign('myntraShopFestCoupons', $myntraShopFestCoupons);
$smarty->assign('cashBackInfo', WidgetKeyValuePairs::getWidgetValueForKey("cash_back_info"));
$smarty->assign('cashBackDisplay', FeatureGateKeyValuePairs::getBoolean("myntraFestSLabCoupons"));
$smarty->assign('cashBackThreshold', WidgetKeyValuePairs::getWidgetValueForKey("myntraFestCouponPercentDiscountMinOrderValue"));

if($skin=='skinmobile'){
// Coupons widget.
$cashcoupons = $adapter->getMyntraCreditsCoupon($XCART_SESSION_VARS["login"]);
$coupons = $adapter->getCouponsForCartPage($XCART_SESSION_VARS["login"], $myCart, $skin);
$rest_coupons = array();
$top_coupons = array();
$eligible_coupons = array();
$ineligible_coupons = array();
$showVerifyMobileMsg=false;
for ($i = 0; $i < count($coupons); $i++) {
    if(in_array($coupons[$i]['groupName'], array('FirstLogin', 'MrpReferrals'))) {
        $showVerifyMobileMsg=true;
    }

    $couponTypeDescription=getCouponType($coupons[$i]['couponType'],$coupons[$i]['MRPAmount'],$coupons[$i]['MRPpercentage']);
    $coupons[$i]['couponTypeDescription']=$couponTypeDescription;
}

$myntraShopFestCoupons = FeatureGateKeyValuePairs::getBoolean("myntrashoppingfestCoupons");
$smarty->assign('myntraShopFestCoupons', $myntraShopFestCoupons);

//*************** apply on all conditional coupons **********************
/*$condApplyOnAllCoupon = FeatureGateKeyValuePairs::getBoolean('condApplyOnAllCoupon');
$smarty->assign('condApplyOnAllCoupon', $condApplyOnAllCoupon);*/

$specialCoupon = FeatureGateKeyValuePairs::getBoolean('enableSpecialCoupon');
$smarty->assign('enableSpecialCoupon', $specialCoupon);


if(!empty($applied) && !empty($couponCode))
{
        for ($i = 0; $i < count($coupons); $i++)
        {
            if($coupons[$i]['coupon']==$couponCode)
            {
                $top_coupons= $coupons[$i];
                break;
            }
        }
}
for ($i = 0; $i < count($coupons); $i++) {
        if($coupons[$i]['coupon']==$couponCode)continue;

        /*if($styleExclusionFG) {
            if($specialCoupon && Coupon::isSpecialCouponGroup($coupons[$i]['groupName'])) {
                $coupons[$i]["disabled"] = false;
            }
        }
        if($condApplyOnAllCoupon && Coupon::isApplyOnAllCouponGroup($coupons[$i]['groupName'])){        
            $coupons[$i]["disabled"] = false; // identifying the all applicable coupons 
            $coupons[$i]["isApplyOnAllCoupon"] = true;
        }else{
            $coupons[$i]["isApplyOnAllCoupon"] = false;
        }*/
        
        array_push($rest_coupons, $coupons[$i]);
}

$noStyleExclusionForUserCoupon = FeatureGateKeyValuePairs::getBoolean('noStyleExclusionForUserCoupon');
$noStyleExclusionForSomeGroups = FeatureGateKeyValuePairs::getBoolean('noStyleExclusionForSomeGroups');

// Select eligible or ineligible coupons according to feature gate setting
for ($i = 0; $i < count($rest_coupons); $i++) {
        $_totalAmout=0;

        $styleExclusionForThisCoupon = $styleExclusionFG;
        if($noStyleExclusionForSomeGroups && 
                        \Coupon::isCouponGroupExists('noStyleExclusionForSomeGroups', $rest_coupons[$i]['groupName'])) {
            $styleExclusionForThisCoupon = false;
        }
        
        // Ignoring style exclusion for user specific coupons if 'noStyleExclusionForUserCoupon' feature gate is ON.
        if($styleExclusionFG == 1 && !$noStyleExclusionForUserCoupon && $styleExclusionForThisCoupon) {
        	$_totalAmout = $couponApplicableItemAmount;
        }
        else if($condNoDiscountCouponFG == 1) {
            $_totalAmout = $nonDiscountedItemAmount;
        }
        else {
            $_totalAmout = $totalItemsAmount;
        }
        
        /*if($rest_coupons[$i]["isApplyOnAllCoupon"]){
            if($rest_coupons[$i]['minimum'] < $_totalAmout) {
                array_push($eligible_coupons, $rest_coupons[$i]);
            }
            continue;
        }*/
        if($rest_coupons[$i]['minimum'] < $_totalAmout) {
            array_push($eligible_coupons, $rest_coupons[$i]);
        }
        else {
            array_push($ineligible_coupons, $rest_coupons[$i]);
        }
}
$showSearchInHeader=0;
$smarty->assign("showVerifyMobileMsg", $showVerifyMobileMsg);
$smarty->assign("cashcoupons", $cashcoupons[0]);
$smarty->assign("rest_coupons", $rest_coupons);
$smarty->assign("top_coupons", $top_coupons);
$smarty->assign("eligible_coupons", $eligible_coupons);
$smarty->assign("ineligible_coupons", $ineligible_coupons);
$smarty->assign("eligible_coupons_count", count($eligible_coupons));
$smarty->assign("ineligible_coupons_count", count($ineligible_coupons));
$smarty->assign("totalItemsAmount", $totalItemsAmount);
$smarty->assign("showSearchInHeader", $showSearchInHeader);

//coupon widget ends

//resource for coupon widget ---------------------
//repeat the logic to recalculate the price here
}
function getCouponType($type,$MRPAmount,$MRPpercentage)
{
    switch ($type) {
        case "absolute" :
            return "Rs " . $MRPAmount . " off";
            break;
        case "percentage" :
            return $MRPpercentage . "% off";
            break;
        case "dual" :
            return $MRPpercentage . "% off upto Rs "
                . $MRPAmount;
    }
}

//resource for coupon widget ends here-------------------------------{
$smarty->display("checkout/cart.tpl");
