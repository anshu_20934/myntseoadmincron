<?php

/**
 * Page for advanced coupons search.
 * 
 * @author Rohan.Railkar
 */

require "./auth.php";
require $xcart_dir."/include/security.php";
require_once $xcart_dir."/include/func/func.mkcore.php";
require_once ("$xcart_dir/include/func/func.db.php");
require_once ("$xcart_dir/include/func/func.utilities.php");
require_once ("$xcart_dir/modules/coupon/database/CouponAdapter.php");

$couponAdapter = CouponAdapter::getInstance();

$templateUsed = "searchCoupons";
$self = "searchCoupons.php";

// Rows to be displayed on every page.
$rowsPerPage = 50;

// The index of the results page that we are displaying. By default 1.
$pageNum = 1;

// Load active groups.
$groups = func_load_active_coupon_groups();

// Load active channels.
$channels = func_load_active_channels();

if ($REQUEST_METHOD == "POST") {
    
    // Modify the lowest of the selected coupons.
    if ($mode == 'modifyCoupon') {
        if (is_array($posted_data)) {
            foreach ($posted_data as $name=>$v) {
                if (empty($v["selected"])) {
                    continue;
                }
                $coupon = $name;
            }
        }
        
        if (!empty($coupon)) {
            header("location:coupons.php?coupon=$coupon&mode=modifysingle");
        }
        
        exit;
    }
    
    // Deleting coupons.
    if ($mode == 'deleteCoupon') {
        if(is_array($posted_data)) {
            foreach ($posted_data as $coupon=>$v) {
                	
                if (empty($v["selected"])) {
                    continue;
                }
                	
                $couponAdapter->deleteCoupon($coupon);
            }
            $top_message["content"] = 'Successfully deleted selected coupon(s).';
        }
    }
    
    $pageNum = $_POST['page'];

    // Counting the offset for fetching the query.
    $offset = ($pageNum - 1) * $rowsPerPage;
    
    // Search parameters.
    $searchStartDate = trim($_POST['searchStartDate']);
    $sdate = explode("/",$searchStartDate);
    $sdate = mktime(0,0,0, $sdate[0],$sdate[1],$sdate[2]);
    
    $searchEndDate = trim($_POST['searchEndDate']);
    $edate = explode("/",$searchEndDate);
    $edate = mktime(0,0,0, $edate[0],$edate[1],$edate[2]);
    
    $searchGroup = trim($_POST['searchGroup']);
    $searchChannel = trim($_POST['searchChannel']);
    $searchUsageGTE = trim($_POST['searchUsageGTE']);
    $searchUsageLTE = trim($_POST['searchUsageLTE']);
    $searchUsagePerUserGTE = trim($_POST['searchUsagePerUserGTE']);
    $searchUsagePerUserLTE = trim($_POST['searchUsagePerUserLTE']);
    $searchPercentageGTE = trim($_POST['searchPercentageGTE']);
    $searchPercentageLTE = trim($_POST['searchPercentageLTE']);
    $searchAbsGTE = trim($_POST['searchAbsGTE']);
    $searchAbsLTE = trim($_POST['searchAbsLTE']);
    $searchFreeShipping = trim($_POST['searchFreeShipping']);
    $searchMinGTE = trim($_POST['searchMinGTE']);
    $searchMinLTE = trim($_POST['searchMinLTE']);
    $searchMaxGTE = trim($_POST['searchMaxGTE']);
    $searchMaxLTE = trim($_POST['searchMaxLTE']);
    $searchDesc = trim($_POST['searchDesc']);
    $searchAllowedUsers = trim($_POST['searchAllowedUsers']);
    $searchExclUsers = trim($_POST['searchExclUsers']);
    
    /*// Basic query.
    $couponsQuery = 
    	"SELECT * 
    	   FROM xcart_discount_coupons a 
     	  WHERE a.coupon LIKE '%'";
    
    $countQuery = 
        "SELECT count(*) as count
           FROM xcart_discount_coupons a
		  WHERE a.coupon LIKE '%'";
    
    $query = "";
    
    // Search start date.
    if ($sdate != "") {
        $query .= " AND a.startdate = '$sdate'";
    }
    
    // Search end date.
    if ($edate != "") {
        $query .= " AND a.expire = '$edate'";
    }
    
	// Search by group name.
	if ($searchGroup != "") {
	    $query .= " AND a.groupName = '$searchGroup'";
	}
	
	// Search by channel.
	if ($searchChannel != "") {
	    $query .= 
	    	" AND a.groupName 
	    		IN (SELECT groupname 
	    		      FROM mk_coupon_group 
	    		     WHERE channel = '$searchChannel')";
	}
	
	// Usage parameters.
	if ($searchUsageGTE != "") {
	    $query .= " AND (a.times >= $searchUsageGTE OR a.isInfinite = 1)";
	}
	if ($searchUsageLTE != "") {
	    $query .= " AND a.times <= $searchUsageLTE";
	}
	if ($searchUsagePerUserGTE != "") {
	    $query .= " AND (a.maxUsageByUser >= $searchUsagePerUserGTE OR a.isInfinitePerUser = 1)";
	}
	if ($searchUsagePerUserLTE != "") {
	    $query .= " AND a.maxUsageByUser <= $searchUsagePerUserLTE";
	}
	
	// Discount conditions.
	if ($searchPercentageGTE != "") {
	    $query .= " AND a.MRPpercentage >= $searchPercentageGTE";
	}
	if ($searchPercentageLTE != "") {
	    $query .= " AND a.MRPpercentage <= $searchPercentageLTE";
	}
	if ($searchAbsGTE != "") {
	    $query .= " AND a.MRPAmount >= $searchAbsGTE";
	}
	if ($searchAbsLTE != "") {
	    $query .= " AND a.MRPAmount <= $searchAbsLTE";
	}
	
	// Free shipping.
	if ($searchFreeShipping != "") {
	    $query .= " AND a.freeShipping = $searchFreeShipping";
	}
	
	// Cart subtotal conditions.
	if ($searchMinGTE != "") {
	    $query .= " AND a.minimum >= $searchMinGTE";
	}
	if ($searchMinLTE != "") {
	    $query .= " AND a.minimum <= $searchMinLTE";
	}
	if ($searchMaxGTE != "") {
	    $query .= " AND a.maxAmount >= $searchMaxGTE";
	}
	if ($searchMaxLTE != "") {
	    $query .= " AND a.maxAmount <= $searchMaxLTE";
	}
	
	// Description substring.
	if ($searchDesc != "") {
	    $query .= " AND a.description LIKE '%$searchDesc%'";
	}
	
	// Allowed and excluded users.
	if ($searchAllowedUsers != "") {
	    $patterns = trim_all(explode(",", $searchAllowedUsers));
	    
	    $searchPatterns = "(";
	    $addedAny = false;
	    foreach ($patterns as $pattern) {
	        if ($addedAny) {
	            $searchPatterns .= " OR ";
	        }
	        $searchPatterns .= "a.users like '%$pattern%'";
	        $addedAny = true;
	    }
	    $searchPatterns .= ")";
	    
	    $query .= " AND $searchPatterns";
	}
	if ($searchExclUsers != "") {
	    $patterns = trim_all(explode(",", $searchExclUsers));
	    
	    $searchPatterns = "(";
	    $addedAny = false;
	    foreach ($patterns as $pattern) {
	        if ($addedAny) {
	            $searchPatterns .= " OR ";
	        }
	        $searchPatterns .= "a.excludeUsers like '%$pattern%'";
	        $addedAny = true;
	    }
	    $searchPatterns .= ")";
	    
	    $query .= " AND $searchPatterns";
	}
	
	// Total results
	$countQuery = $countQuery . $query;
	$result = func_query($countQuery, true);
	$numOfRows = $result[0]['count'];
	
	// Fetch only 50 rows corresponding to the page.
	$query .= " LIMIT $offset, $rowsPerPage";
    
	// Run the query.
    $coupons = func_query($couponsQuery . $query, true);

    */

    $result = CouponAPIClient::searchCoupons(null,$sdate,$edate,$searchGroup ,$searchChannel,$searchUsageGTE ,
        $searchUsageLTE , $searchUsagePerUserGTE, $searchUsagePerUserLTE,   $searchPercentageGTE,
         $searchPercentageLTE,$searchAbsGTE , $searchAbsLTE , $searchFreeShipping , $searchMinGTE,
          $searchMinLTE,  $searchMaxGTE, $searchMaxLTE, $searchDesc ,$searchAllowedUsers, $searchExclUsers,$offset,$rowsPerPage);
    $numOfRows = $result['count'];
    $coupons = $result['coupons'];
    $maxPage = ceil($numOfRows/$rowsPerPage);
    
    // Page navigation.
    if ($pageNum > 1) {
        $page = $pageNum - 1;
        $prev = "<a href=\"$self?page=$page\">Previous</a> ";
    }
    else {
        $prev  = '';
    }
    
    if ($pageNum < $maxPage) {
        $page = $pageNum + 1;
        $next = "<a href=\"$self?page=$page\">Next</a>";
    }
    else {
        $next = '';
    }
}

$smarty->assign("main",$templateUsed);
$smarty->assign("groups", $groups);
$smarty->assign("channels", $channels);
$smarty->assign("coupons", $coupons);
$smarty->assign("top_message", $top_message);

// Set back the search parameters.
$smarty->assign("searchStartDate", $searchStartDate);
$smarty->assign("searchEndDate", $searchEndDate);
$smarty->assign("searchGroup", $searchGroup);
$smarty->assign("searchChannel", $searchChannel);
$smarty->assign("searchUsageGTE", $searchUsageGTE);
$smarty->assign("searchUsageLTE", $searchUsageLTE);
$smarty->assign("searchUsagePerUserGTE", $searchUsagePerUserGTE);
$smarty->assign("searchUsagePerUserLTE", $searchUsagePerUserLTE);
$smarty->assign("searchPercentageGTE", $searchPercentageGTE);
$smarty->assign("searchPercentageLTE", $searchPercentageLTE);
$smarty->assign("searchAbsGTE", $searchAbsGTE);
$smarty->assign("searchAbsLTE", $searchAbsLTE);
$smarty->assign("searchFreeShipping", $searchFreeShipping);
$smarty->assign("searchMinGTE", $searchMinGTE);
$smarty->assign("searchMinLTE", $searchMinLTE);
$smarty->assign("searchMaxGTE", $searchMaxGTE);
$smarty->assign("searchMaxLTE", $searchMaxLTE);
$smarty->assign("searchDesc", $searchDesc);
$smarty->assign("searchAllowedUsers", $searchAllowedUsers);
$smarty->assign("searchExclUsers", $searchExclUsers);

// Page navigation.
$smarty->assign("page", $pageNum);
$smarty->assign("npage", $pageNum + 1);
$smarty->assign("ppage", $pageNum - 1);
$smarty->assign("previous", $prev);
$smarty->assign("next", $next);
$smarty->assign("totalpages", $maxPage);

@include $xcart_dir."/modules/gold_display.php";
func_display("provider/home.tpl",$smarty);
?>