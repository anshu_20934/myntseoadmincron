<?php
require_once "./auth.php";
require_once $xcart_dir."/include/security.php";


if(!isset($_FILES) && isset($HTTP_POST_FILES)){
    $_FILES = $HTTP_POST_FILES;
}

$upload_dir = "$xcart_dir/var/tmp/".$_FILES['product_capping_file']['name'];
$extension = end(explode(".",$_FILES['product_capping_file']['name']));

if($extension == 'csv'){
    if(!move_uploaded_file($_FILES['product_capping_file']['tmp_name'],$upload_dir)){
        
        exit();
    }
    else{
        $filepath = $upload_dir;//.$_FILES['tagging_file']['name'];
        if (($handle = fopen($filepath, "r")) !== FALSE) {
            $count_rows=0;
            $styles = array();
            $caps_to_be_inserted = array();
            $caps_to_be_updated = array();
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $num = count($data);
                if ($HTTP_POST_VARS['crudOp'] == 'delete') {
                    array_push($styles,$data[0]);
                } else {
                    if ($num != 2) {
                        continue;
                    } 
                    $styles[$data[0]] = $data[1];

                }

                ++$count_rows;
                if ($count_rows%1000 == 0) {
                  if ($HTTP_POST_VARS['crudOp'] == 'insert' || $HTTP_POST_VARS['crudOp'] == 'update') {
                     $getRes =  CouponAPIClient::getDiscountCaps(array_keys($styles));
                        foreach($getRes['newStyles'] as $value) {
                            $caps_to_be_inserted[$value] = $styles[$value];
                         }
                        foreach($getRes['existingData'] as $value) {
                            $caps_to_be_updated[$value['styleid']] = $styles[$value['styleid']];
                        }
                        if (!empty($caps_to_be_inserted)) {
                             CouponAPIClient::insertDiscountCaps($caps_to_be_inserted);
                        }

                        $caps_to_be_inserted = array();
                        if (!empty($caps_to_be_updated)) {
                            CouponAPIClient::updateDiscountCaps($caps_to_be_updated);
                        }
                        $caps_to_be_updated = array();


               } else {
                    CouponAPIClient::deleteDiscountCaps($styles);
                }

                 $styles =array();
                 $count_rows =0;

                }
            }

            if($count_rows >0) {
                if ($HTTP_POST_VARS['crudOp'] == 'insert' || $HTTP_POST_VARS['crudOp'] == 'update') {
                     $getRes =  CouponAPIClient::getDiscountCaps(array_keys($styles));
                        foreach($getRes['newStyles'] as $value) {
                            $caps_to_be_inserted[$value] = $styles[$value];
                         }
                        foreach($getRes['existingData'] as $value) {
                            $caps_to_be_updated[$value['styleid']] = $styles[$value['styleid']];
                        }
                        if (!empty($caps_to_be_inserted)) {
                             CouponAPIClient::insertDiscountCaps($caps_to_be_inserted);
                        }

                        $caps_to_be_inserted = array();
                        if (!empty($caps_to_be_updated)) {
                            CouponAPIClient::updateDiscountCaps($caps_to_be_updated);
                        }
                        $caps_to_be_updated = array();


               } else {
                    CouponAPIClient::deleteDiscountCaps($styles);
                }
            }

        }

    }
}

     func_header_location("viewcoupongroups.php");
?>
