<?php
define("IS_IMPORT", true);
require "./auth.php";
require $xcart_dir."/include/security.php";
include_once("$xcart_dir/modules/coupon/database/couponAPIClient.php");

if(isset($_GET['group']))
   $groupid = $_GET['group'] ;
   
if (isset($_GET['search']))
    $search = $_GET['search'];

if (isset($_GET['mode']))
   $mode = $_GET['mode'];
 
$filename;
if ($mode == 'properties') {
    
   /* if (!empty($groupid)) {
        $query = "SELECT * 
					FROM " . $sql_tbl['discount_coupons'] . " 
				   WHERE groupName = '".$groupid."' 
				ORDER BY lastEdited desc;";
        
        $filename = "coupon_group_$groupid" . "_properties.csv";
    }
    else {
        $query = "SELECT * 
        			FROM " . $sql_tbl['discount_coupons'] . " 
        		   WHERE coupon IS NOT NULL";
        
        $filename = "search_properties.csv";
        if (!empty($search)) {
            $query .= " AND coupon LIKE '%$search%'";
            $filename = "search_$search" . "_properties.csv";
        }
        $query .= " ORDER BY expire DESC";
    }

    $rsCoupons = db_query($query,true);
    */
    $rsCoupons = CouponAPIClient::searchCoupons(empty($groupid) && !empty(search)?$search:null,null,null,!empty($groupid)?$groupid:null ,null,null ,
        null , null, null,   null,null,null ,null , null , null, null,  null, null, null ,null, null);

    $columnname = "COUPON_CODE,CREATED_ON,TYPE,DISCOUNT,USAGE,SUBTOTAL,DISCOUNT_OFFERED,EXPIRES_ON,CREATOR,STATUS,LAST_MODIFIED";
    $data .= $columnname."\n";

    while ($row = $rsCoupons)
    {
        $data .= $row['code'] . ",";
        $data .= date('d-m-Y', $row['timeCreated']) . ",";
        $data .= $row['couponType'] . ",";
        
        if ($row['couponType'] == 'absolute') {
            $data .= "Upto Rs. " . $row['mrpAmount'] . "/- off" . ",";
        }
        else if ($row['couponType'] == 'percentage') {
            $data .= $row['mrpPercentage'] . "% off" . ",";
        }
        else if ($row['couponType'] == 'dual') {
            $data .= $row['mrpPercentage'] . "% off upto Rs. " . $row['mrpAmount'] . "/-" . ",";
        }
        
        $data .= "(" . $row['times_used'] . "+" . $row['times_locked'] . ") / ";
        if ($row['isInfinite']) {
            $data .= "inf.";
        }
        else {
            $data .= $row['times'];
        }
        $data .= ",";
        
        $data .= $row['subtotal'] . ",";
        $data .= $row['discountOffered'] . ",";
        $data .= date('d-m-Y', $row['expire']) . ",";
        $data .= $row['provider'] . ",";
        $data .= $row['status'] . ",";
        $data .= date('d-m-Y H:i:s', $row['lastEdited']);
        $data .= "\n";
    }
}
else if ($mode == 'usage') {

    $columnname = "COUPON,COUPON_STATUS,TIMESTAMP,ORDER_ID,USER_ID,ORDER_STATUS";
    $data .= $columnname."\n";
    $rsCoupons = CouponAPIClient::searchCoupons(empty($groupid) && !empty(search)?$search:null,null,null,!empty($groupid)?$groupid:null ,null,null ,
        null , null, null,   null,null,null ,null , null , null, null,  null, null, null ,null, null);
    $couponCodes = array();
    while ($row = $rsCoupons)
    {
        array_push($coupoCodes, $row['code']);
    }
    $coupons = implode(",",$couponCodes);
    // Iterate over locked orders.
    if (!empty($groupid)) {
        
        // Export.
        $query = "SELECT coupon, date, orderid, login, status
        			FROM " . $sql_tbl['orders'] " 
        		   WHERE coupon IN (".$coupons.")  
        		     AND status = 'PP' 
        		ORDER BY coupon;";
        
        $filename = "coupon_group_$groupid" . "_usage.csv";
    }
    else {
        
        // Search.
        $query = "SELECT coupon, date, orderid, login, status
        			FROM " . $sql_tbl['orders'] . " 
        		   WHERE coupon IN ("..$coupons.") 
        		     AND status = 'PP' 
        		ORDER BY coupon;";
    }

    $lockOrders = db_query($query, true);
    while ($order = db_fetch_array($lockOrders)) {
        $data .= $order['coupon'] . ",";
        $data .= "Lock,";
        $data .= date('d-m-Y H:i:s', $order['date']) . ",";
        $data .= $order['orderid'] . ",";
        $data .= $order['login'] . ",";
        $data .= $order['status'] . "\n";
    }

    // Iterate over used orders.
    if (!empty($groupid)) {
        
        // Export.
        $query = "SELECT coupon, date, orderid, login, status
        			FROM " . $sql_tbl['orders'] . " 
        		   WHERE coupon IN (".$coupons.") 
        		     AND status IN ('PV', 'Q', 'OH', 'PD', 'C') 
        		ORDER BY coupon;";
    }
    else {
        
        // Search.
        $query = "SELECT coupon, date, orderid, login, status
        			FROM " . $sql_tbl['orders'] . " 
        		   WHERE coupon IN  (".$coupons.") 
        		     AND status IN ('PV', 'Q', 'OH', 'PD', 'C') 
        		ORDER BY coupon;";
    }

    $usageOrders = db_query($query, true);
    while ($order = db_fetch_array($usageOrders)) {
        $data .= $order['coupon'] . ",";
        $data .= "Use,";
        $data .= date('d-m-Y H:i:s', $order['date']) . ",";
        $data .= $order['orderid'] . ",";
        $data .= $order['login'] . ",";
        $data .= $order['status'] . "\n";
    }
}

// Output the headers to download the file
header("Content-type: application/x-msdownload");
header("Content-Disposition: attachment; filename=$filename");
header("Pragma: no-cache");
header("Expires: 0");
echo $data;
?>