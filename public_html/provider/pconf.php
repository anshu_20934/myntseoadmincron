<?php
/*****************************************************************************\
+-----------------------------------------------------------------------------+
| X-Cart                                                                      |
| Copyright (c) 2001-2006 Ruslan R. Fazliev <rrf@rrf.ru>                      |
| All rights reserved.                                                        |
+-----------------------------------------------------------------------------+
| PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
| FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
| AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
|                                                                             |
| THIS  AGREEMENT  EXPRESSES  THE  TERMS  AND CONDITIONS ON WHICH YOU MAY USE |
| THIS SOFTWARE   PROGRAM   AND  ASSOCIATED  DOCUMENTATION   THAT  RUSLAN  R. |
| FAZLIEV (hereinafter  referred to as "THE AUTHOR") IS FURNISHING  OR MAKING |
| AVAILABLE TO YOU WITH  THIS  AGREEMENT  (COLLECTIVELY,  THE  "SOFTWARE").   |
| PLEASE   REVIEW   THE  TERMS  AND   CONDITIONS  OF  THIS  LICENSE AGREEMENT |
| CAREFULLY   BEFORE   INSTALLING   OR  USING  THE  SOFTWARE.  BY INSTALLING, |
| COPYING   OR   OTHERWISE   USING   THE   SOFTWARE,  YOU  AND  YOUR  COMPANY |
| (COLLECTIVELY,  "YOU")  ARE  ACCEPTING  AND AGREEING  TO  THE TERMS OF THIS |
| LICENSE   AGREEMENT.   IF  YOU    ARE  NOT  WILLING   TO  BE  BOUND BY THIS |
| AGREEMENT, DO  NOT INSTALL OR USE THE SOFTWARE.  VARIOUS   COPYRIGHTS   AND |
| OTHER   INTELLECTUAL   PROPERTY   RIGHTS    PROTECT   THE   SOFTWARE.  THIS |
| AGREEMENT IS A LICENSE AGREEMENT THAT GIVES  YOU  LIMITED  RIGHTS   TO  USE |
| THE  SOFTWARE   AND  NOT  AN  AGREEMENT  FOR SALE OR FOR  TRANSFER OF TITLE.|
| THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY GRANTED BY THIS AGREEMENT.      |
|                                                                             |
| The Initial Developer of the Original Code is Ruslan R. Fazliev             |
| Portions created by Ruslan R. Fazliev are Copyright (C) 2001-2006           |
| Ruslan R. Fazliev. All Rights Reserved.                                     |
+-----------------------------------------------------------------------------+
\*****************************************************************************/

#
# $Id: pconf.php,v 1.11.2.1 2006/06/01 06:12:34 max Exp $
#

require "./auth.php";
require $xcart_dir."/include/security.php";

if (empty($active_modules['Product_Configurator']))
	func_header_location("error_message.php?access_denied&id=68");

$provider_condition = ($single_mode ? "" : "AND provider='$login'");

$location[] = array(func_get_langvar_by_name("lbl_product_configurator"), "pconf.php");
$pconf_title = func_get_langvar_by_name("lbl_product_configurator");

if ($REQUEST_METHOD != "POST") {
#
# Define data for the navigation within section
#
	$dialog_tools_data = array();

    $dialog_tools_data["right"][] = array("link" => "pconf.php?mode=search", "title" => func_get_langvar_by_name("lbl_pconf_search"));
	$dialog_tools_data["right"][] = array("link" => "product_modify.php?mode=pconf", "title" => func_get_langvar_by_name("lbl_pconf_confproduct"));
	$dialog_tools_data["right"][] = array("link" => "pconf.php?mode=types", "title" => func_get_langvar_by_name("lbl_pconf_define_types"));
    $dialog_tools_data["right"][] = array("link" => "pconf.php?mode=about", "title" => func_get_langvar_by_name("lbl_pconf_about"));

}

if (!in_array($mode, array("types", "search", "about")))
	$mode = "search";

if ($mode == "types") {
	include $xcart_dir."/modules/Product_Configurator/pconf_types.php";
	$location[] = array(func_get_langvar_by_name("lbl_pconf_define_types"), "");
	$pconf_title = func_get_langvar_by_name("lbl_pconf_define_types");
}

if ($mode == "search") {
	include $xcart_dir."/modules/Product_Configurator/pconf_search.php";
	$location[] = array(func_get_langvar_by_name("lbl_search_products"), "");
	$pconf_title = func_get_langvar_by_name("lbl_search_products");
}

if ($mode == "about") {
	$smarty->assign("mode", "about");
	$location[] = array(func_get_langvar_by_name("lbl_pconf_about"), "");
	$pconf_title = func_get_langvar_by_name("lbl_pconf_about");
}

$smarty->assign("main","product_configurator");
$smarty->assign("main_mode","manage");

$smarty->assign("pconf_title", $pconf_title);

# Assign the current location line
$smarty->assign("location", $location);

# Assign the section navigation data
$smarty->assign("dialog_tools_data", $dialog_tools_data);

@include $xcart_dir."/modules/gold_display.php";
func_display("provider/home.tpl",$smarty);
?>

