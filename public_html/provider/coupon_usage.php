<?php

/**
 * Page to view a coupon's usage in the provider interface.
 * 
 * @author Rohan.Railkar
 */

require "./auth.php";
require $xcart_dir."/include/security.php";
require_once ("$xcart_dir/include/func/func.db.php");

global $sql_tbl;

if ($_GET['coupon'] != '') {
    $coupon = $_GET['coupon'];
    
    // Coupon data.
    /*$query = "SELECT * 
    			FROM " . $sql_tbl['discount_coupons'] . " 
    		   WHERE coupon = '$coupon';";
    
    $result = db_query($query);
    $coupon_data = db_fetch_array($result);
    */
    $coupon_data = CouponAPIClient::fetchCoupon($coupon);
    $smarty->assign("coupon_data", $coupon_data);
    
    // Coupon locks.
    $query = "SELECT date, orderid, login, status 
    			FROM " . $sql_tbl['orders'] . " 
    		   WHERE coupon = '$coupon'
    		     AND status = 'PP';";
    
    $lock_orders = func_query($query);
    $smarty->assign("lock_orders", $lock_orders);
    
    // Coupon usage.
    $query = "SELECT date, orderid, login, status 
    			FROM " . $sql_tbl['orders'] . " 
    		   WHERE coupon = '$coupon'
    		     AND status in ('C', 'Q', 'PV', 'OH', 'PD');";
    
    $usage_orders = func_query($query);
    $smarty->assign("usage_orders", $usage_orders);
}

func_display("modules/Discount_Coupons/coupon_usage.tpl",$smarty);

?>