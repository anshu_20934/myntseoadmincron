<?php
/*****************************************************************************\
+-----------------------------------------------------------------------------+
| X-Cart                                                                      |
| Copyright (c) 2001-2006 Ruslan R. Fazliev <rrf@rrf.ru>                      |
| All rights reserved.                                                        |
+-----------------------------------------------------------------------------+
| PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
| FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
| AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
|                                                                             |
| THIS  AGREEMENT  EXPRESSES  THE  TERMS  AND CONDITIONS ON WHICH YOU MAY USE |
| THIS SOFTWARE   PROGRAM   AND  ASSOCIATED  DOCUMENTATION   THAT  RUSLAN  R. |
| FAZLIEV (hereinafter  referred to as "THE AUTHOR") IS FURNISHING  OR MAKING |
| AVAILABLE TO YOU WITH  THIS  AGREEMENT  (COLLECTIVELY,  THE  "SOFTWARE").   |
| PLEASE   REVIEW   THE  TERMS  AND   CONDITIONS  OF  THIS  LICENSE AGREEMENT |
| CAREFULLY   BEFORE   INSTALLING   OR  USING  THE  SOFTWARE.  BY INSTALLING, |
| COPYING   OR   OTHERWISE   USING   THE   SOFTWARE,  YOU  AND  YOUR  COMPANY |
| (COLLECTIVELY,  "YOU")  ARE  ACCEPTING  AND AGREEING  TO  THE TERMS OF THIS |
| LICENSE   AGREEMENT.   IF  YOU    ARE  NOT  WILLING   TO  BE  BOUND BY THIS |
| AGREEMENT, DO  NOT INSTALL OR USE THE SOFTWARE.  VARIOUS   COPYRIGHTS   AND |
| OTHER   INTELLECTUAL   PROPERTY   RIGHTS    PROTECT   THE   SOFTWARE.  THIS |
| AGREEMENT IS A LICENSE AGREEMENT THAT GIVES  YOU  LIMITED  RIGHTS   TO  USE |
| THE  SOFTWARE   AND  NOT  AN  AGREEMENT  FOR SALE OR FOR  TRANSFER OF TITLE.|
| THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY GRANTED BY THIS AGREEMENT.      |
|                                                                             |
| The Initial Developer of the Original Code is Ruslan R. Fazliev             |
| Portions created by Ruslan R. Fazliev are Copyright (C) 2001-2006           |
| Ruslan R. Fazliev. All Rights Reserved.                                     |
+-----------------------------------------------------------------------------+
\*****************************************************************************/

#
# $Id: coupons.php,v 1.34.2.1 2006/05/04 12:36:11 max Exp $
#

define("NUMBER_VARS", "minimum_new,discount_new");
require_once "./auth.php";
require_once $xcart_dir."/include/security.php";
require_once $xcart_dir."/include/func/func.mkcore.php";
require_once $xcart_dir."/include/func/func.randnum.php";

require_once $xcart_dir."/modules/coupon/database/CouponAdapter.php";
$couponAdapter = CouponAdapter::getInstance();

ini_set("display_errors", 1);
/*$product_type = func_load_product_type();
$prod_type = array();
foreach($product_type AS $key=>$value)
{
	$ptype = $product_type[$key];
	$prod_type[$ptype[0]] = $ptype[1];
}*/

$product_type_groups=array();//**** never think of uncommenting this ***func_query("select * from mk_product_group order by display_order desc ");
$prd_type=array();
foreach($product_type_groups as $groups)
{
    $PrdTypes = array(); //**** never think of uncommenting this *** func_query ("SELECT id, name FROM mk_product_type where product_type_groupid=".$groups['id']." ORDER BY name ");
    if (is_array($PrdTypes)) {
        $prd_type[$groups['name']]= $PrdTypes;
    }
}
$smarty->assign("producttypes", $prd_type);

$prod_styles = array();
//foreach ($prod_type as $id=>$name) {
foreach ($prd_type as $grpName=>$prdTypes) {
    foreach ($prdTypes as $prdType) {
        $temp = func_get_productstyles_forproduct($prdType['id']);
        foreach ($temp as $psid=>$psname) {
            $prod_styles[$psid] = $psname;
        }
    }
}

//
// Load available channels.
// NOTE: This contains an element '' for default (governed by coupon-group) and 
// the element 'online' to represent online channel.
//
$channels = func_load_active_channels();

// Load coupon templates.
$templates = $couponAdapter->getCouponTemplates();

// Load active groups.
$groups = func_load_active_coupon_groups();

$location[] = array(func_get_langvar_by_name("lbl_coupons"), "");

include_once $xcart_dir."/include/categories.php";

#
# Use this condition when single mode is disabled
#
$provider_condition = ($single_mode ? "" : "AND provider = '$login'");

function func_get_producttypes(){
$product_type_query="select id ,name from mk_product_type";
$commentresult=db_query($product_type_query);
$comments=array();
if ($commentresult)
	{
		$i=0;

		while ($row=db_fetch_array($commentresult))
		{
			$comments[$i] = $row;
			$i++;
		}
	}
	$searchresults=array();
	foreach($comments as $comment){
			$searchresults[$comment['id']]=$comment['name'];
	}
	return $searchresults;
}
function func_get_productstyles_forproduct($productid){
$style_query="select id,name from mk_product_style where product_type='$productid' and parent_style is NULL ";
$styleresult=db_query($style_query);
$styles=array();
if ($styleresult)
	{
		$i=0;

		while ($row=db_fetch_array($styleresult))
		{
			$styles[$i] = $row;
			$i++;
		}
	}
	$stylesresults=array();
	foreach($styles as $style){
			$stylesresults[$style['id']]=$style['name'];
	}
	return $stylesresults;
}

if ($mode == "addExclIncl") {

	CouponAPIClient::addExclIncl($HTTP_POST_VARS['groups'],$HTTP_POST_VARS['styles'],
		$HTTP_POST_VARS['exclIncl']=="1"?true:false,$HTTP_POST_VARS['crudOp']=="1"?true:false);


	 func_header_location("viewcoupongroups.php");

}



if ($REQUEST_METHOD == "POST") {

	
	//print_r($HTTP_POST_VARS);;
	if ($mode=="delete") {
		#
		# Delete selected coupons
		#
		if (is_array($posted_data)) {
			$deleted = false;
			foreach ($posted_data as $coupon=>$v) {
				if (empty($v["to_delete"]))
					continue;

				$couponAdapter->deleteCoupon($coupon, $provider_condition);
				$deleted = true;
			}

			if ($deleted)
				$top_message["content"] = func_get_langvar_by_name("msg_discount_coupons_del");
		}
	}

	if ($mode == "update") {
		#
		# Update discount table
		#
		if (is_array($posted_data)) {
			foreach ($posted_data as $coupon=>$v) {
				$couponAdapter->changeCouponStatus($coupon, $$v[status], $provider_condition, $login);
			}

			$top_message["content"] = func_get_langvar_by_name("msg_discount_coupons_upd");
		}
	}
	
	if ($mode == "add") {
	     
	    if($ctype == "S") {

	        #
	        # Add new coupon
	        #

	        # Generate timestamp
	        $sdate = explode("/",$startDate);
	        $sdate = mktime(0,0,0, $sdate[0],$sdate[1],$sdate[2]);
	        $edate = explode("/",$endDate);
	        $edate = mktime(0,0,0, $edate[0],$edate[1],$edate[2]);

	        $ptype=$HTTP_POST_VARS['ptype'];
	         
	        $productTypeIds = '';
	        if (!empty($HTTP_POST_VARS['ProductType'])) {
	            $productTypeIds = implode(",", $HTTP_POST_VARS['ProductType']);
	        }
	        $excludeProductTypeIds = '';
	        if (!empty($HTTP_POST_VARS['ExclProductType'])) {
	            $excludeProductTypeIds = implode(",", $HTTP_POST_VARS['ExclProductType']);
	        }
	        
	        $styleIds = '';
	        if (!empty($HTTP_POST_VARS['ProductStyle'])) {
	            $styleIds = implode(",", $HTTP_POST_VARS['ProductStyle']);
	        }
	        $excludeStyleIds = '';
	        if (!empty($HTTP_POST_VARS['ExclProductStyle'])) {
	            $excludeStyleIds = implode(",", $HTTP_POST_VARS['ExclProductStyle']);
	        }
	        
	        // Merge styles from categories and PTs.
	        $catStyleIds = $productid_new;
	        $excludeCatStyleIds = $excl_productid_new;
	         
	        $categoryIds = '';
	        if (!empty($HTTP_POST_VARS['categoryIds'])) {
	            $categoryIds = implode(",", $HTTP_POST_VARS['categoryIds']);
	        }
	        $excludeCategoryIds = '';
	        if (!empty($HTTP_POST_VARS['exclCategoryIds'])) {
	            $excludeCategoryIds = implode(",", $HTTP_POST_VARS['exclCategoryIds']);
	        }
	         
	        $timestamp = time();
	        $isInfiniteNew = $isInfiniteNew ? 1 : 0;
	        $isInfinitePerUserNew = $isInfinitePerUserNew ? 1 : 0;
	        $freeShipping = $freeShipping ? 1 : 0;
	        $showInMyMyntra = $showInMyMyntra ? 1 : 0;
	        $MRPAmountNew = $MRPAmountNew ? $MRPAmountNew : 0.0;
	        $MRPpercentageNew = $MRPpercentageNew ? $MRPpercentageNew : 0.0;
	        $minAmountNew = $minAmountNew ? $minAmountNew : 0.0;
	        $maxAmountNew = $maxAmountNew ? $maxAmountNew : 0.0;
	        
	        $minCountNew = $minCountNew ? $minCountNew : 0;
	        $maxCountNew = $maxCountNew ? $maxCountNew : 0;
	        //echo "<PRE>";print_r($HTTP_POST_VARS);echo "$minCountNew : $maxCountNew";exit;

	        $coupon_error = false;
	        if($ctype == "S" && $couponMode == "Single")
	        {
	        	$couponsFetched = CouponAPIClient::fetchCoupon($couponCodeNew);
	            if (empty($couponCodeNew) || !empty($couponsFetched))//func_query_first_cell("SELECT COUNT(*) FROM xcart_discount_coupons WHERE coupon='$couponCodeNew'") > 0)
	            {
	                $coupon_data = $HTTP_POST_VARS;
	                $coupon_data["endDateNew"] = $endDateNew;
	                x_session_register("coupon_data");
	                $top_message["content"] = func_get_langvar_by_name("msg_err_discount_coupons_add");
	                $top_message["type"] = "E";
	                $coupon_error = true;
	            }
	        }

	        if($coupon_error == false) {
	            $timesUsed = 0;
	            $SKUs = '';
	            $excludeSKUs = '';
	            
	            if ($couponMode == "Single") {
	            
	                //
	                // Add the coupon. Also, increment the count in the group, if any.
	                // Here $groupIdNew is group name.
	                //
	                $couponAdapter->addCoupon($couponCodeNew, $addtogroup,
	                $sdate, $edate, $maxUsageNew, $isInfiniteNew, $maxUsagePerCartNew,
	                $timesUsed, 0, $maxUsageByUserNew, $isInfinitePerUserNew,
	                $minAmountNew, $maxAmountNew, $usersNew, $excludeUsersNew,
	                $timestamp, $creatorNew, $couponTypeNew, $MRPAmountNew,
	                $MRPpercentageNew, $freeShipping, $isActiveNew,
	                $commentsNew, $productTypeIds, $excludeProductTypeIds,
	                $styleIds, $excludeStyleIds, $catStyleIds,
	                $excludeCatStyleIds, $categoryIds, $excludeCategoryIds,
	                $SKUs, $excludeSKUs, $descriptionNew, $showInMyMyntra,$minCountNew,$maxCountNew);
	                 
	                $top_message["content"] = "Coupon created successfully.";
	                 
	                // Redirect.
	                func_header_location("viewcoupongroups.php?searchGroup=$addtogroup");
	            }
	            else if ($couponMode == "Multiple") {
	                
	            	if(!isset($_FILES) && isset($HTTP_POST_FILES)){
	            		$_FILES = $HTTP_POST_FILES;
	            	}
	            	
	            	$upload_dir = "$xcart_dir/var/tmp/".$_FILES['tagging_file']['name'];
	            	$extension = end(explode(".",$_FILES['tagging_file']['name']));
	            	
	            	if($extension == 'csv'){
	            		if(!move_uploaded_file($_FILES['tagging_file']['tmp_name'],$upload_dir)){
	            			echo "can't move\n";
	            			exit();
	            		}
	            		else{
	            			$filepath = $upload_dir;//.$_FILES['tagging_file']['name'];
	            		}
	            		
	            		$outFileName = $couponAdapter->generateBulkCoupons($couponPrefix, $suffixLength, $count, $addtogroup,
								                $sdate, $edate, $maxUsageNew, $isInfiniteNew, $maxUsagePerCartNew,
								                $timesUsed, 0, $maxUsageByUserNew, $isInfinitePerUserNew,
								                $minAmountNew, $maxAmountNew, $usersNew, $excludeUsersNew,
								                $timestamp, $creatorNew, $couponTypeNew, $MRPAmountNew,
								                $MRPpercentageNew, $freeShipping, $isActiveNew,
								                $commentsNew, $productTypeIds, $excludeProductTypeIds,
								                $styleIds, $excludeStyleIds, $catStyleIds,
								                $excludeCatStyleIds, $categoryIds, $excludeCategoryIds,
								                $SKUs, $excludeSKUs, $descriptionNew, $showInMyMyntra,$minCountNew,$maxCountNew,$filepath);
	            		
	            		
	            		unlink($filepath);
	            		$top_message["content"] = "Coupons created successfully.";
	                 
		                // Redirect.
		                //func_header_location("bulk_");
		                func_header_location("viewcoupongroups.php?searchGroup=$addtogroup");
	            		
	            	}else{
	            		$couponAdapter->generateBulkCoupons($couponPrefix, $suffixLength, $count, $addtogroup,
	            		$sdate, $edate, $maxUsageNew, $isInfiniteNew, $maxUsagePerCartNew,
	            		$timesUsed, 0, $maxUsageByUserNew, $isInfinitePerUserNew,
	            		$minAmountNew, $maxAmountNew, $usersNew, $excludeUsersNew,
	            		$timestamp, $creatorNew, $couponTypeNew, $MRPAmountNew,
	            		$MRPpercentageNew, $freeShipping, $isActiveNew,
	            		$commentsNew, $productTypeIds, $excludeProductTypeIds,
	            		$styleIds, $excludeStyleIds, $catStyleIds,
	            		$excludeCatStyleIds, $categoryIds, $excludeCategoryIds,
	            		$SKUs, $excludeSKUs, $descriptionNew, $showInMyMyntra,$minCountNew,$maxCountNew);
	            		//$result = shell_exec("php $xcart_dir/scripts/myntra_privilege_card_users_tagging.php $filepath 'user_tagging'");
	            		//echo $result;
	            		 
	            		
	            		$top_message["content"] = "Coupons created successfully.";
	            		
	            		// Redirect.
	            		func_header_location("viewcoupongroups.php?searchGroup=$addtogroup");
	            	}
	            	
	            	 	
	            	
	            	
	            		
	            	/* var_dump($_FILES);print_r ($upload_dir);echo"<PRE>";print_r($_REQUEST);print_r($_FILES);exit; 
	            	
	            	
	                //
	                // Add the coupons. Also, increment the count in the group.
	                // Here $groupIdNew is group name.
	                //
	                $couponAdapter->addMultipleCoupons($couponPrefix, $suffixLength, $count, $addtogroup,
	                $sdate, $edate, $maxUsageNew, $isInfiniteNew, $maxUsagePerCartNew,
	                $timesUsed, 0, $maxUsageByUserNew, $isInfinitePerUserNew,
	                $minAmountNew, $maxAmountNew, $usersNew, $excludeUsersNew,
	                $timestamp, $creatorNew, $couponTypeNew, $MRPAmountNew,
	                $MRPpercentageNew, $freeShipping, $isActiveNew,
	                $commentsNew, $productTypeIds, $excludeProductTypeIds,
	                $styleIds, $excludeStyleIds, $catStyleIds,
	                $excludeCatStyleIds, $categoryIds, $excludeCategoryIds,
	                $SKUs, $excludeSKUs, $descriptionNew, $showInMyMyntra,$minCountNew,$maxCountNew);
	                
	                $top_message["content"] = "Coupon created successfully.";
	                 
	                // Redirect.
	                func_header_location("viewcoupongroups.php?searchGroup=$addtogroup");*/
	            }
	        }
	         
	    }

	    	
	    if($ctype == "G")
	    {
	        if(!$couponAdapter->existsCouponGroup($groupIdNew))
	        {
	            $timestamp = time();
	            $couponAdapter->createCouponGroup($groupIdNew,
	                                              $channelNew,
	                                              $timestamp,
	                                              $creatorNew,
	                                              $templateNew,$groupTypeNew,
	                                              $applicabelDeviceNew);

                if($groupTypeNew == "pg"){
                    $couponAdapter->createCouponGroupBinRange($groupIdNew,$binRangeNew);
                }

                $top_message["content"] = "Coupon Group created successfully.";
	            func_header_location("viewcoupongroups.php?action=group");

	        }
	        else
	        {
	            $top_message["content"] = "Group already exist, Please try again.";
	        }
	    }
	}
	
	if ($mode == "modify") {
		$couponAdapter->modifyCouponGroup($groupid, $channelNew, $isGroupActiveNew, $templateNew, $login,$groupType,$applicabelDeviceNew);
        if($groupType == "pg"){
            $couponAdapter->modifyCouponGroupBinRange($groupid,$binRange);
        }
		$top_message["content"] = "Coupon Group successfully modified.";
		func_header_location("viewcoupongroups.php?action=group");
	}
		
if ($mode == "modify_single") {
    
		$sdate = explode("/",$startDate);
		$sdate = mktime(0,0,0, $sdate[0],$sdate[1],$sdate[2]);
		$edate = explode("/",$endDate);
		$edate = mktime(0,0,0, $edate[0],$edate[1],$edate[2]);
		
		$isInfinite = $isInfinite ? 1 : 0;
		$isInfinitePerUser = $isInfinitePerUser ? 1 : 0;
		$freeShipping = $freeShipping ? 1 : 0;
		$showInMyMyntra = $showInMyMyntra ? 1 : 0;
		$MRPAmount = $MRPAmount ? $MRPAmount : 0.0;
		$MRPpercentage = $MRPpercentage ? $MRPpercentage : 0.0;
		$minAmount = $minAmount ? $minAmount : 0.0;
		$maxAmount = $maxAmount ? $maxAmount : 0.0;
		
		$minCount = $minCount ? $minCount : 0.0;
		$maxCount = $maxCount ? $maxCount : 0.0;
		
		$productTypeIds = '';
		if (!empty($HTTP_POST_VARS['ProductType'])) {
		    $productTypeIds = implode(",", $HTTP_POST_VARS['ProductType']);
		}
		$excludeProductTypeIds = '';
		if (!empty($HTTP_POST_VARS['ExclProductType'])) {
		    $excludeProductTypeIds = implode(",", $HTTP_POST_VARS['ExclProductType']);
		}
		
		$categoryIds = '';
		if (!empty($HTTP_POST_VARS['categoryIds'])) {
		    $categoryIds = implode(",", $HTTP_POST_VARS['categoryIds']);
		}
		$excludeCategoryIds = '';
		if (!empty($HTTP_POST_VARS['exclCategoryIds'])) {
		    $excludeCategoryIds = implode(",", $HTTP_POST_VARS['exclCategoryIds']);
		}
		
		$ptype=$HTTP_POST_VARS['ptype'];
		
		$styleIdString = implode("," , $HTTP_POST_VARS['ProductStyle']);
		$excludeStyleIdString = implode("," , $HTTP_POST_VARS['ExclProductStyle']);
		
		$couponAdapter->modifyCoupon($HTTP_POST_VARS['coupon_name'], $sdate, $edate,
		    $freeShipping, $couponType, $MRPpercentage, $MRPAmount, $maxUsage, 
		    $isInfinite, $maxUsagePerCart, $maxUsageByUser, $isInfinitePerUser, 
		    $minAmount, $maxAmount, $isActive, $users, 
		    $excludeUsers, $comments, $productTypeIds, $excludeProductTypeIds,
		    $styleIdString, $excludeStyleIdString, $catStyleIds, 
		    $excludeCatStyleIds, $categoryIds, $excludeCategoryIds, $login, $description, $showInMyMyntra,$minCount,$maxCount);
		
		$top_message["content"] = "Coupon modified successfully."; 
		func_header_location("viewcoupongroups.php?searchGroup=$addtogroup");
	}	
	
	
}

$addtogroup = "";
if (isset($_GET["group"]) && !empty($_GET["group"])) {
    $addtogroup = $_GET["group"];

    $query =
            "SELECT * 
               FROM mk_coupon_templates 
              WHERE templateName = (SELECT templateName FROM mk_coupon_group WHERE groupname = '$addtogroup');";

    // Fetch template data in an array.
    $template = func_query_first($query, true);
    
    $template['couponTypeNew'] = $template['couponType'];
    $template['MRPAmountNew'] = $template['MRPAmount'];
    $template['MRPpercentageNew'] = $template['MRPpercentage'];
    $template['minSubtotal'] = $template['minimum'];
    $template['maxSubtotal'] = $template['maxAmount'];
    $template['maxUsageNew'] = $template['times'];
    $template['maxUsagePerCartNew'] = $template['maxUsagePerCart'];
    $template['maxUsageByUserNew'] = $template['maxUsageByUser'];
    $template['comments'] = '';

    $productTypeIds = explode(",", $template['productTypeIds']);
    $template['ptids'] = $productTypeIds;
    $psids = explode(",", $template['styleIds']);
    $template['psids'] = $psids;

    $exclProductTypeIds = explode(",", $template['excludeProductTypeIds']);
    $template['exclptids'] = $exclProductTypeIds;
    $exclpsids = explode(",", $template['excludeStyleIds']);
    $template['exclpsids'] = $exclpsids;

    x_session_register("coupon_data", $template);
	$smarty->assign("coupon_data", $template);
	x_session_unregister("coupon_data");
}

// Empty while adding a new group. Has a value while adding a new coupon to the group.
$smarty->assign("addtogroup", $addtogroup);

/* this logic is added by venu/vaibhav to avoid big select */
$coupons_per_page = 500;
if( isset($_GET["page"]) && !empty($_GET["page"]) ) {
	$page = $_GET["page"];
}
else {
	$page = 1;
}
$page = $page-1;
$limit = "limit ";
$limit .= $page*$coupons_per_page.",".$coupons_per_page;

$coupons = func_query("SELECT *, (expire + ".doubleval($config["Appearance"]["timezone_offset"]).") as endDate FROM xcart_discount_coupons WHERE 1 $provider_condition $limit");

/* to modify the group */
if($mode == 'modify')
{
	  $groupID = $_GET['groupid'];
	  $group_data = $couponAdapter->fetchCouponGroupArray($groupID);
      $bin_data = $couponAdapter->fetchCouponBinRange($groupID);
	  $smarty->assign("group_data",$group_data);
      $smarty->assign("bin_data",$bin_data);
}
if($mode == 'modifysingle')
{
	  $coupon = $_GET['coupon'];
	  $coupondetail = $couponAdapter->fetchCouponArray($coupon);
	  
	  $coupondetail['endDate']= date("m/d/Y",$coupondetail['expire']);
	  $coupondetail['startDate']= date("m/d/Y",$coupondetail['startdate']);
	  
	  $smarty->assign("products",func_get_producttypes());
	  
	  $productTypeIds = explode(",", $coupondetail['productTypeIds']);
	  $coupondetail['ptids'] = $productTypeIds;
	  $excludeProductTypeIds = explode(",", $coupondetail['excludeProductTypeIds']);
	  $coupondetail['excl_ptids'] = $excludeProductTypeIds;
	  
	  $categoryIds = explode(",", $coupondetail['categoryIds']);
	  $coupondetail['catids'] = $categoryIds;
	  $exclCategoryIds = explode(",", $coupondetail['excludeCategoryIds']);
	  $coupondetail['excl_catids'] = $exclCategoryIds;
	  
	  $catStyleIdsArr = explode(",", $coupondetail['catStyleIds']);
	  $catStyleNames = '';
	  foreach ($catStyleIdsArr as $catStyleId) {
	      if (!empty($catStyleId)) {
	          $result = db_query("SELECT product FROM xcart_products WHERE productid = '$catStyleId';");
	          $row = db_fetch_array($result);
	          $catStyleNames .= $row['product'] . ",";
	      }
	  }
	  $coupondetail['catStyleNames'] = $catStyleNames;
	  
	  $exclCatStyleIdsArr = explode(",", $coupondetail['excludeCatStyleIds']);
	  $exclCatStyleNames = '';
	  foreach ($exclCatStyleIdsArr as $catStyleId) {
	      if (!empty($catStyleId)) {
	          $result = db_query("SELECT product FROM xcart_products WHERE productid = '$catStyleId';");
	          $row = db_fetch_array($result);
	          $exclCatStyleNames .= $row['product'] . ",";
	      }
	  }
	  $coupondetail['exclCatStyleNames'] = $exclCatStyleNames;
	  
	  $psids = explode(",", $coupondetail['styleIds']);
	  $coupondetail['psids'] = $psids;
	  $excl_psids = explode(",", $coupondetail['excludeStyleIds']);
	  $coupondetail['excl_psids'] = $excl_psids;
	  
	  $smarty->assign("coupon",$coupondetail);
	  $smarty->assign("addtogroup", $coupondetail['groupName']);
	  //$smarty->assign("styles",func_get_productstyles_forproduct($coupondetail['producttypeid']));
	
}
/* end */

if (x_session_is_registered("coupon_data")) {
	x_session_register("coupon_data");
	$smarty->assign("coupon_data", $coupon_data);
	x_session_unregister("coupon_data");
}

if (!empty($coupons))
	$smarty->assign("coupons", $coupons);
	
$smarty->assign("login", $login);
$smarty->assign("main","coupons");
$smarty->assign("mode",$mode);
$smarty->assign("groupid",$groupID);
$smarty->assign("top_message", $top_message);
$smarty->assign("prod_type", $prod_type);
$smarty->assign("prod_styles", $prod_styles);
$smarty->assign("channels", $channels);
$smarty->assign("templates", $templates);
$smarty->assign("groups", $groups);
# Assign the current location line
$smarty->assign("location", $location);

@include $xcart_dir."/modules/gold_display.php";
func_display("provider/home.tpl",$smarty);
?>
