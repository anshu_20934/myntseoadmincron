<?php
/*****************************************************************************\
+-----------------------------------------------------------------------------+
| X-Cart                                                                      |
| Copyright (c) 2001-2006 Ruslan R. Fazliev <rrf@rrf.ru>                      |
| All rights reserved.                                                        |
+-----------------------------------------------------------------------------+
| PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
| FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
| AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
|                                                                             |
| THIS  AGREEMENT  EXPRESSES  THE  TERMS  AND CONDITIONS ON WHICH YOU MAY USE |
| THIS SOFTWARE   PROGRAM   AND  ASSOCIATED  DOCUMENTATION   THAT  RUSLAN  R. |
| FAZLIEV (hereinafter  referred to as "THE AUTHOR") IS FURNISHING  OR MAKING |
| AVAILABLE TO YOU WITH  THIS  AGREEMENT  (COLLECTIVELY,  THE  "SOFTWARE").   |
| PLEASE   REVIEW   THE  TERMS  AND   CONDITIONS  OF  THIS  LICENSE AGREEMENT |
| CAREFULLY   BEFORE   INSTALLING   OR  USING  THE  SOFTWARE.  BY INSTALLING, |
| COPYING   OR   OTHERWISE   USING   THE   SOFTWARE,  YOU  AND  YOUR  COMPANY |
| (COLLECTIVELY,  "YOU")  ARE  ACCEPTING  AND AGREEING  TO  THE TERMS OF THIS |
| LICENSE   AGREEMENT.   IF  YOU    ARE  NOT  WILLING   TO  BE  BOUND BY THIS |
| AGREEMENT, DO  NOT INSTALL OR USE THE SOFTWARE.  VARIOUS   COPYRIGHTS   AND |
| OTHER   INTELLECTUAL   PROPERTY   RIGHTS    PROTECT   THE   SOFTWARE.  THIS |
| AGREEMENT IS A LICENSE AGREEMENT THAT GIVES  YOU  LIMITED  RIGHTS   TO  USE |
| THE  SOFTWARE   AND  NOT  AN  AGREEMENT  FOR SALE OR FOR  TRANSFER OF TITLE.|
| THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY GRANTED BY THIS AGREEMENT.      |
|                                                                             |
| The Initial Developer of the Original Code is Ruslan R. Fazliev             |
| Portions created by Ruslan R. Fazliev are Copyright (C) 2001-2006           |
| Ruslan R. Fazliev. All Rights Reserved.                                     |
+-----------------------------------------------------------------------------+
\*****************************************************************************/

#
# $Id: coupons.php,v 1.34.2.1 2006/05/04 12:36:11 max Exp $
#

define("NUMBER_VARS", "minimum_new,discount_new");
require "./auth.php";
require $xcart_dir."/include/security.php";

require_once $xcart_dir."/modules/coupon/database/CouponAdapter.php";
$couponAdapter = CouponAdapter::getInstance();

$self = "viewcoupongroups.php";

//$location[] = array(func_get_langvar_by_name("lbl_coupons"), "");
$location[] = array("Coupon Groups", "$self");

//include $xcart_dir."/include/categories.php";
$templateUsed = 'viewcoupongroups';
/************************************************/
// how many rows to show per page
$rowsPerPage =500;

// by default we show first page
$pageNum = 1;
$maxPage = 0;
// if $_GET['page'] defined, use it as page number
if(isset($_GET['page']))
{
    $pageNum = $_GET['page'];
}
  // counting the offset
$offset = ($pageNum - 1) * $rowsPerPage;

// Group page. (Previously known as 'export'.)
if (isset($_GET['searchGroup'])) {
    $searchGroup = $_GET['searchGroup'];
    $mode = 'export';
}

#
# Use this condition when single mode is disabled
#
$provider_condition = ($single_mode ? "" : "AND provider='$login'");
$searchGroup = '';
if ($REQUEST_METHOD == "POST") {

	if ($_POST['pageType'] == 'exclIncl') {
		$smarty->assign("pageType","exclIncl");

		@include $xcart_dir."/modules/gold_display.php";
		func_display("provider/home.tpl",$smarty);
		return;
	}
	if ($_POST['pageType'] == 'productCapping') {
		$smarty->assign("pageType","productCapping");

		@include $xcart_dir."/modules/gold_display.php";
		func_display("provider/home.tpl",$smarty);
		return;
	}


	if ($mode=="export") {
		#
		# Delete selected coupons
		#
		
		$templateUsed = "viewallcoupons";
		$searchGroup = '';
		if (is_array($posted_data)) {
		    //print_r($posted_data);
		    //exit;
			$deleted = false;
			foreach ($posted_data as $group=>$v) {
				if (empty($v["to_delete"])) {
					continue;
				}
                $searchGroup = $group;
                
				// $deleted = true;*/
			}
		}
		if($searchGroup == '')
		{
		      $templateUsed = 'viewcoupongroups';
		      $mode ='';
		      $top_message["content"]="Please select the group name.";
		       
		}
	}

	if ($mode == "update") {
		#
		# Update discount table
		#
		$templateUsed = "viewcoupongroups";
		if (is_array($posted_data)) {
			foreach ($posted_data as $group=>$v) {
				if (empty($v["to_delete"])) {
					continue;
				}
				
				$couponAdapter->changeCouponGroupStatus($group, $v[isActive], $login);
			}

			$top_message["content"] = "Selected coupon group(s) updated successfully.";
			$smarty->assign("top_message", $top_message);
		}
	}
	if ($mode == "delete") {
		#
		# Update discount table
		#
		$templateUsed = "viewcoupongroups";
		if (is_array($posted_data)) {
			foreach ($posted_data as $group=>$v) {
				if (empty($v["to_delete"])) {
					continue;
				}
				
				//$couponAdapter->deleteCouponGroup($group); // disabling coupons delete at group level
			}

			$top_message["content"] = "coupon group(s) cannot be deleted. please contact engineering.";
			$smarty->assign("top_message", $top_message);
		}
	}
if ($mode == "deletesingle") {
		#
		# Update discount table
		#
		$templateUsed = "viewcoupongroups";
		if (is_array($posted_data)) {
			foreach ($posted_data as $coupon=>$v) {
			
				if (empty($v["to_delete"]))
					continue;
					
				$couponAdapter->deleteCoupon($coupon);
			}
			
			if (empty($exportedgroup)) {
                $singlemode='search';
			}
			else {
			    
			    // Coupon deleted form export page. Go back to export page.
			    $searchGroup = $exportedgroup;
			    $mode = 'export';
			}
			
			$top_message["content"] = "Selected coupon(s) deleted succesfully.";
			$smarty->assign("top_message", $top_message);
		}
	}
	if ($mode == "search") 
	{
		#
		# search selected coupons
		#viewcoupongroups
		$templateUsed = "viewcoupongroups";
		$searchGroup = $_POST['cgroup'];
		
		      
	}
	if ($mode == "add") 
	{
		#
		# search selected coupons
		#viewcoupongroups
		/*$templateUsed = "viewcoupongroups";
		$searchGroup = $_POST['cgroup'];*/
		header("location:coupons.php?group=$exportedgroup");
		exit;
		      
	}
if ($mode == "modifysingle") 
	{
		#
		# search selected coupons
		$coupon = "";
		$templateUsed = "viewcoupongroups";
		if (is_array($posted_data)) {
			foreach ($posted_data as $group=>$v) {
				if (empty($v["to_delete"]))
					continue;
				$coupon = 	$group;
				  
			}

		//	$top_message["content"] = func_get_langvar_by_name("msg_discount_coupons_upd");
		}
		//print_r($coupon);
		//exit;
		if(!empty($coupon))
		{
			header("location:coupons.php?coupon=$coupon&mode=modifysingle");
			exit;
		}
		//$top_message["content"]="Please select the group name.";
		
		      
	}
	if ($mode == "modify") 
	{
		#
		# search selected coupons
		$groupid = '';
		$templateUsed = "viewcoupongroups";
		if (is_array($posted_data)) {
			foreach ($posted_data as $group=>$v) {
				if (empty($v["to_delete"]))
					continue;
				$groupid = 	$group;
				  
			}

		//	$top_message["content"] = func_get_langvar_by_name("msg_discount_coupons_upd");
		}
		if($groupid != '')
		{
			header("location:coupons.php?groupid=$groupid&mode=modify");
			exit;
		}
		$top_message["content"]="Please select the group name.";
	}
	
	//func_header_location("coupons.php");
}

if($mode != 'export')
{
   /* $query = "SELECT * 
    			FROM mk_coupon_group as cgroup ";
    
    if (!empty($searchGroup) && $mode == 'search') {
        $query .= "WHERE groupname 
        			LIKE '%" . $searchGroup . "%' ";
    }
    
    $query .= "ORDER BY cgroup.lastEdited desc;";
    
    $couponGroups  = func_query($query,true);
	*/
    $couponGroups = CouponAPIClient::searchCouponGroups($searchGroup);
}

if(isset($_GET['searchGroup']))
{
	 $searchGroup= $_GET['searchGroup'];
}
if($mode == 'export')
{
	/*$query = 
		"SELECT *
		   FROM xcart_discount_coupons
		  WHERE groupName = '".$searchGroup."'		  
		  LIMIT $offset, $rowsPerPage";
	
	$coupons = func_query($query, true);
	
	$noofcoupons = func_query(
		"SELECT count(xcart_discount_coupons.groupName) as ctr 
		   FROM xcart_discount_coupons 
		  WHERE groupName = '".$searchGroup."' ", true);
	*/
	$result = CouponAPIClient::searchCoupons(null,null,null,$searchGroup ,null,null ,
        null , null, null,   null,
         null,null ,null, null , null,
          null,  null, null, null,null, null,$offset,$rowsPerPage);
    $numrows = $result['count'];
    $coupons = $result['coupons'];
	$maxPage = ceil($numrows/$rowsPerPage);
	$templateUsed = "viewallcoupons";
	
	$location[] = array("$searchGroup", "");
}
if($singlemode == 'search')
{
	$searchGroup = $_POST['cgroup'];
	
	/*$query = "SELECT * FROM xcart_discount_coupons WHERE coupon IS NOT NULL ";
	if(!empty($searchGroup)) {
	    $query .= "AND coupon LIKE '".$searchGroup."%' " ;
	}
	        
	$query .= "ORDER BY expire desc LIMIT $offset, $rowsPerPage";
	*/
	$result = CouponAPIClient::searchCoupons($searchGroup,null,null,null,null,null ,
        null , null, null,   null,
         null,null ,null, null , null,
          null,  null, null, null,null, null,$offset,$rowsPerPage);
	//print_r($query);
	//exit;
	//$sqllog->info("Search coupons - $query");
	//$coupons = func_query($query,true);
     $numrows = $result['count'];
    $coupons = $result['coupons'];
	$maxPage = ceil($numrows/$rowsPerPage);

	$templateUsed = "viewcoupongroups";
	
}
$prev='';
$next='';
/* paging code */
if ($pageNum > 1)
{
   $page  = $pageNum - 1;
   $prev  = "<a href=\"$self?page=$page&mode=$mode&searchGroup=$searchGroup\">Previous</a> ";
  
}
else
{
   $prev  = ''; // we're on page one, don't print previous link
   
}

if ($pageNum < $maxPage)
{
   $page = $pageNum + 1;
   $next = " <a href=\"$self?page=$page&mode=$mode&searchGroup=$searchGroup\">Next</a>  ";
 
}
else
{
   $next = ''; // we're on the last page, don't print next link
    // nor the last page link
}
$smarty->assign("page",$pageNum);
$smarty->assign("previous",$prev);
$smarty->assign("next",$next);
$smarty->assign("totalpages",$maxPage);
//$smarty->assign("top_message", $top_message);
$smarty->assign("self", $self);

/* end */



$smarty->assign("main",$templateUsed);
$smarty->assign("coupongroups",$couponGroups);
$smarty->assign("coupons",$coupons);
$smarty->assign("groupname",$searchGroup); /////
$smarty->assign("mode",$mode);
$smarty->assign("search_method","group");
if($singlemode=="search"){
$smarty->assign("mode",$singlemode);
$smarty->assign("search_method","single");

}

global $xcart_dir;
$bulkCouponGenerationOutputFiles = glob("$xcart_dir/var/tmp/bulk_coupons_output/*.csv");
foreach ($bulkCouponGenerationOutputFiles as $id=>$file){
	
	$bulkCouponGenerationOutputFiles[$id]=substr($file,strrpos($file, "/",-1)+1);
	
}

$smarty->assign("bulkCouponGenerationOutputFiles",$bulkCouponGenerationOutputFiles);
# Assign the current location line
$smarty->assign("location", $location);

@include $xcart_dir."/modules/gold_display.php";
func_display("provider/home.tpl",$smarty);
?>
