<?php

/**
 * Page to view/add/modify/delete coupon templates.
 * 
 * Author: Rohan.Railkar
 */

require "./auth.php";
require $xcart_dir."/include/security.php";
require_once ("$xcart_dir/include/func/func.db.php");
require_once $xcart_dir."/modules/coupon/database/CouponAdapter.php";

$couponAdapter = CouponAdapter::getInstance();

function func_get_productstyles_forproduct($productid)
{
    $style_query="SELECT id,name from mk_product_style where product_type='$productid' and parent_style is NULL ";
    $styleresult=db_query($style_query);
    
    $styles=array();
    if ($styleresult)
    {
        $i=0;
        while ($row=db_fetch_array($styleresult)) {
            $styles[$i] = $row;
            $i++;
        }
    }
    
    $stylesresults=array();
    foreach($styles as $style){
        $stylesresults[$style['id']]=$style['name'];
    }
    
    return $stylesresults;
}

// POST REQUEST.
if ($REQUEST_METHOD == 'POST') {
    
    // Delete action.
    if ($action == "deleteTemplate") {
        if(is_array($posted_data)) {
            foreach ($posted_data as $template=>$v) {
                 
                if (empty($v["selected"])) {
                    continue;
                }
                 
                $couponAdapter->deleteTemplate($template);
            }
            $top_message["content"] = 'Successfully deleted selected template(s).';
            header("location:coupon_templates.php");
            exit;
        }
    }
    
    // Edit action.
    if ($action == "modifyTemplate") {
        if (is_array($posted_data)) {
            foreach ($posted_data as $name=>$v) {
                if (empty($v["selected"])) {
                    continue;
                }
                $template = $name;
            }
        }
        
        if (!empty($template)) {
            header("location:coupon_templates.php?mode=single&template=$template");
        }
        
        exit;
    }
    
    // Add action.
    if ($action == 'addTemplate') {
        header("location:coupon_templates.php?mode=single");
        exit;
    }
    
    // Add a template.
    if ($mode == 'add') {
        
        if ($couponAdapter->existsCouponTemplate($name)) {
            
            // Template already exists.
            $top_message["content"] = "Template $name already exists!";
            $top_message["type"] = "E";
            header("location:coupon_templates.php");
            exit;
        }
        else {
            $isInfinite = $isInfinite ? 1 : 0;
            $isInfinitePerUser = $isInfinitePerUser ? 1 : 0;
            $freeShipping = $freeShipping ? 1 : 0;
            $showInMyMyntra = $showInMyMyntra ? 1 : 0;
            
            $productTypeIds = '';
	        if (!empty($HTTP_POST_VARS['ProductType'])) {
	            $productTypeIds = implode(",", $HTTP_POST_VARS['ProductType']);
	        }
	        
	        $styleIds = '';
	        if (!empty($HTTP_POST_VARS['ProductStyle'])) {
	            $styleIds = implode(",", $HTTP_POST_VARS['ProductStyle']);
	        }
	        
	        $excludeProductTypeIds = '';
	        if (!empty($HTTP_POST_VARS['exclProductType'])) {
	            $excludeProductTypeIds = implode(",", $HTTP_POST_VARS['exclProductType']);
	        }
	        
	        $excludeStyleIds = '';
	        if (!empty($HTTP_POST_VARS['exclProductStyle'])) {
	            $excludeStyleIds = implode(",", $HTTP_POST_VARS['exclProductStyle']);
	        }
            
            $couponAdapter->addTemplate($name, $validity, $freeShipping,
                $type, $MRPAmount, $MRPpercentage, $maxUsage, $isInfinite,
                $maxUsagePerCart, $maxUsageByUser, $isInfinitePerUser, $minimum,
                $maxAmount, $users, $excludedUsers, $comments, $productTypeIds,
                $styleIds, $excludeProductTypeIds, $excludeStyleIds, $login, $description, $showInMyMyntra);
            
            $top_message["content"] = "Template <b>$name</b> created successfully!";
            header("location:coupon_templates.php");
            exit;
        }
    }
    
    // Edit a template.
    else if ($mode == 'edit') {
        $isInfinite = $isInfinite ? 1 : 0;
        $isInfinitePerUser = $isInfinitePerUser ? 1 : 0;
        $freeShipping = $freeShipping ? 1 : 0;
        $showInMyMyntra = $showInMyMyntra ? 1 : 0;

        $productTypeIds = '';
        if (!empty($HTTP_POST_VARS['ProductType'])) {
            $productTypeIds = implode(",", $HTTP_POST_VARS['ProductType']);
        }
         
        $styleIds = '';
        if (!empty($HTTP_POST_VARS['ProductStyle'])) {
            $styleIds = implode(",", $HTTP_POST_VARS['ProductStyle']);
        }
         
        $excludeProductTypeIds = '';
        if (!empty($HTTP_POST_VARS['exclProductType'])) {
            $excludeProductTypeIds = implode(",", $HTTP_POST_VARS['exclProductType']);
        }
         
        $excludeStyleIds = '';
        if (!empty($HTTP_POST_VARS['exclProductStyle'])) {
            $excludeStyleIds = implode(",", $HTTP_POST_VARS['exclProductStyle']);
        }

        $couponAdapter->modifyTemplate($name, $validity, $freeShipping,
            $type, $MRPAmount, $MRPpercentage, $maxUsage, $isInfinite,
            $maxUsagePerCart, $maxUsageByUser, $isInfinitePerUser, $minimum,
            $maxAmount, $users, $excludedUsers, $comments, $productTypeIds,
            $styleIds, $excludeProductTypeIds, $excludeStyleIds, $timeCreated, $creator, $login, $description, $showInMyMyntra);

        $top_message["content"] = "Template <b>$name</b> modified successfully!";
        header("location:coupon_templates.php");
        exit;
    }
}

// GET REQUESTS.
if (isset($_GET['mode'])) {
    $mode = $_GET['mode'];
}

// Landing page - view all the templates.
if (empty($mode)) {
    $query =
        "SELECT *
           FROM mk_coupon_templates
       ORDER BY lastEdited desc;";
    
    $templates = func_query($query, true);
    $smarty->assign("templates", $templates);
    
    // Display
    $templateUsed = "viewalltemplates";
}

// Add / Edit a template.
if ($mode == 'single') {
    
    // Populate product types
    $product_type_groups=func_query("select * from mk_product_group order by display_order desc ");
    $prd_type=array();
    foreach($product_type_groups as $groups)
    {
        $PrdTypes = func_query ("SELECT id, name FROM mk_product_type where product_type_groupid=".$groups['id']." ORDER BY name ");
        if (is_array($PrdTypes)) {
            $prd_type[$groups['name']]= $PrdTypes;
        }
    }
    $smarty->assign("producttypes", $prd_type);
    
    // If template is empty, then add new template. Otherwise, edit it.
    if (isset($_GET['template'])) {
        $templateName = $_GET['template'];
    }
    
    if (!empty($templateName)) {
        
        // Edit a template.
        $query = 
            "SELECT * 
               FROM mk_coupon_templates 
              WHERE templateName = '$templateName';";
        
        // Fetch template data in an array.
        $template = func_query_first($query, true);
        
        $productTypeIds = explode(",", $template['productTypeIds']);
        $template['ptids'] = $productTypeIds;
        $psids = explode(",", $template['styleIds']);
        $template['psids'] = $psids;
        
        $exclProductTypeIds = explode(",", $template['excludeProductTypeIds']);
        $template['exclptids'] = $exclProductTypeIds;
        $exclpsids = explode(",", $template['excludeStyleIds']);
        $template['exclpsids'] = $exclpsids;
        
        $smarty->assign("template", $template);
        
        // Populate product styles.
        $prod_styles = array();
        foreach ($prd_type as $grpName=>$prdTypes) {
            foreach ($prdTypes as $prdType) {
                $temp = func_get_productstyles_forproduct($prdType['id']);
                foreach ($temp as $psid=>$psname) {
                    $prod_styles[$psid] = $psname;
                }
            }
        }
        $smarty->assign("prod_styles", $prod_styles);
    }
    
    // Display.
    $templateUsed = "singletemplate";
}

$smarty->assign("main", $templateUsed);
$smarty->assign("login", $login);

@include $xcart_dir."/modules/gold_display.php";
func_display("provider/home.tpl",$smarty);
?>
