<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 * Description of ProfDataHandle
 *
 * @author sudhir
 */
class ProfDataHandle {
    
    private $metricName;
    private $startTime;
    
    public function __construct($metricName) {
        $this->metricName = $metricName;
        $this->startTime = microtime(true);
    }
    public function getMetricName() {
        return $this->metricName;
    }
    
    public function getStartTime() {
        return $this->startTime;
    }

    // Gopi: This is used to inject the start time that is marked before the auth.php inclusion
    // for sampl usage, see the /checkout_common.php
    public function setStartTime($startTime) {
        $this->startTime = $startTime;
    }
}

?>
