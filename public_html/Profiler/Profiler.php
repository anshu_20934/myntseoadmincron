<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

include_once($xcart_dir."/Profiler/ProfDataHandle.php");
require_once($xcart_dir."/env/Profiler.config.php");
require_once($xcart_dir."/env/Host.config.php");


/**
 * Description of Profiler
 *
 * @author sudhir
 */
class Profiler 
{
    private static $defaultContext;
    private static $context;
    private static $prefix;
    private static $startHandle;
    private static $sampleRate;
    
    static function shutdown()
    {
        self::endTiming(self::$startHandle);
    }

    public static function init()
    {
        self::$defaultContext = self::sanitizeScriptName($_SERVER["SCRIPT_NAME"]);
        self::$startHandle = self::startTiming("page_load_time");
        self::$context = self::$defaultContext;
        self::$prefix  = HostConfig::$configMode.".".ProfilerConfig::$app.".x9.";
        self::$sampleRate = ProfilerConfig::$sampleRate;
        ShutdownHook::addHook(array(Profiler,"shutdown"),ShutdownHook::PRECEDENCE_4);
    }
    
    public static function getSamplingRate() {
        return self::$sampleRate;
    }
    
    public static function setContext($contextName) {
        $name = self::validateContextName($contextName);
        if(self::$context == self::$defaultContext && !empty($name))
        {
            self::$context = $name;
        }
    }
    
    public static function startTiming($metricName) 
    {
        return new ProfDataHandle(self::validateName($metricName));
    }
    
    public static function endTiming(ProfDataHandle $handle, $sampleRate=1) 
    {
        $time = microtime(true) - $handle->getStartTime();
        $data = array();
        $key = self::getFullMetricKey($handle->getMetricName());
        $data[$key] = "$time|ms";
        self::send($data, $sampleRate);        
    }
    
    public static function increment($metricName, $count=1) {
        $data = array();
        $data[self::getFullMetricKey($metricName)] = "$count|c";
        self::send($data);
    }
    
    public static function decrement($metricName, $count=1) {
        $data = array();
        $data[self::getFullMetricKey($metricName)] = "-$count|c";
        self::send($data);
    }

    private static function getFullMetricKey ($metricName)
    {
        return  self::$prefix . self::$context . "." . self::validateName($metricName);
    }

    private static function sanitizeScriptName($contextName)
    {
        $name = str_replace(".php", "_php", $contextName);
        $name = preg_replace("/\//", "", $name, 1);
        $name = self::validateContextName($name);
        return $name;
    }

    private static function validateContextName($contextName)
    {
        return self::validateName($contextName);
    }

    private static function validateName($contextName)
    {
        $name = str_replace(":", "-", $contextName);
        return str_replace(".", "-", $name);    
    }

    /*
     * Squirt the metrics over UDP
     **/
    private static function send($data, $sampleRate=1) {
        global $weblog;
        
        if (!ProfilerConfig::$enabled) { return; }

        // sampling
        $sampledData = array();

        if ($sampleRate < 1) {
            foreach ($data as $stat => $value) {
                if ((mt_rand() / mt_getrandmax()) <= $sampleRate) {
                    $sampledData[$stat] = "$value|@$sampleRate";
                }
            }
        } else {
            $sampledData = $data;
        }

        if (empty($sampledData)) { return; }

        // Wrap this in a try/catch - failures in any of this should not propagate to caller
        try {
            $host = ProfilerConfig::$statsDHost;
            $port = ProfilerConfig::$statsDPort;
            $fp = fsockopen("udp://$host", $port, $errno, $errstr);
            if (! $fp) { $weblog->debug("Error opening socket");return; }
            foreach ($sampledData as $stat => $value) {
                fwrite($fp, "$stat:$value");
            }
            fclose($fp);
        } catch (Exception $e) {
	    $weblog->debug("Exception writing to open socket");
        }
    }
}

Profiler::init();
?>
