<?php

if ( !defined('XCART_START') ) {
	header("Location: index.php"); die("Access denied");
}
require_once HostConfig::$documentRoot."/Smarty-3.1.7/libs/SmartyBC.class.php";
$smarty = new SmartyBC();
$smartyDir = array();
$smartyDir[0]=HostConfig::$documentRoot."/$skin";
$smartyDir[1]=HostConfig::$documentRoot."/skincommon";
$smarty->setTemplateDir($smartyDir);
/*
$smarty->setCompileDir(HostConfig::$documentRoot."/var/templates_c_$skin");
$smarty->setConfigDir(HostConfig::$documentRoot."/config");
$smarty->setCacheDir(HostConfig::$documentRoot."/var/cache_$skin");
*/
$smarty->setCompileDir(HostConfig::$smartyRoot."/var/templates_c_$skin");
$smarty->setConfigDir(HostConfig::$smartyRoot."/config");
$smarty->setCacheDir(HostConfig::$smartyRoot."/var/cache_$skin");

//$smarty->debugging = true;
$mail_smarty = $smarty;
global $smartyIsNew;
$smartyIsNew=true;
