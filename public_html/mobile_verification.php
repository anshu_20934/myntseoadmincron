<?php
	if(file_exists("./auth.php")) {
		include_once("./auth.php");
	} else if (file_exists("../auth.php")) {
		include_once("../auth.php"); // happens for secure pages
	} 		

	if(empty($_POST['userinputcaptcha'])) {
		// No captcha entered by online user.. We discard this check in case order is being admin verified.
		echo 'WrongCaptcha'; exit;
	}
	
	$id = $_POST['id']; 
	$sessionCaptcha=null;
	if(!empty($XCART_SESSION_VARS['captchasAtMyntra'][$id])) {
		// Note: Including auth.php removes $_SESSION set user session variables. We store those in POST as auth.php does not unset POST data
		$sessionCaptcha  = $XCART_SESSION_VARS['captchasAtMyntra'][$id];
	}

	$userinputCaptcha = $_POST['userinputcaptcha'];
	if(!empty($userinputCaptcha)) {
		if (empty($sessionCaptcha) || ($userinputCaptcha != $sessionCaptcha)) {
			$captchaVerified = false;
		} else {
		  	unset($XCART_SESSION_VARS['captchasAtMyntra'][$id]);
		   	$captchaVerified = true;
		}
	}
			
	if($captchaVerified)	{
		include_once "$xcart_dir/modules/coupon/mrp/SmsVerifier.php";
		$smsVerifier = SmsVerifier::getInstance();
		$mobile = $_POST['mobile'];
		$login = $_POST['login'];
		//This function is used to increment the number of captcha attempts
		$smsVerifier->updateCaptchaAttempts($mobile, $login);	
		 
		echo 'CaptchaConfirmed';  exit;
		
	}else{
		echo 'WrongCaptcha'; exit;
	}
			
?>
