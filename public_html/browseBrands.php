<?php
require "./auth.php";
//include_once($xcart_dir."/include/class/widget/headerMenu.php");
require_once 'myntra/navhighlighter.php';
use seo\SeoStrategy;
use seo\SeoPageType;
use macros\PageMacros;
$pageName = 'landing';
include_once $xcart_dir."/webengage.php";
include_once $xcart_dir."/gtm.php";
 
$brandsResults=$global_brands;
$brand_logos = $xcache->fetchAndStore(function($brandsResults) {
       $brand_logos = array();
       foreach($brandsResults as $key => $value) {
               $result=$value['result']['brands_filter_facet'];
               foreach($result as $brand => $count) {
                       $logo_sql="select logo from mk_filters where filter_name='".$brand."' and is_active=1";
                   $brand_logo=func_query($logo_sql);
                   $brand_logos[$brand]=$brand_logo[0]["logo"];
               }
       }
       return $brand_logos;
}, array($brandsResults), "browseBrand:getGlobalBrandLogos",1800);
$smarty->assign("global_brands", $brandsResults);
$smarty->assign("brand_logos", $brand_logos);
//Macros
$macros = new PageMacros('STATIC');
$macros->url = currentPageURLWithStripedQueryStringsAndPort();
$macros->page_type = SeoPageType::STATIC_PAGE;
$macros->page_name = 'brands';

$seoEnabled=FeatureGateKeyValuePairs::getBoolean('SeoV3Enabled');
if($seoEnabled){
    try {
    $handle = Profiler::startTiming("seov3_load");
        $seoStrategy = new SeoStrategy($macros);
        Profiler::endTiming($handle);
        $smarty->assign("seoStrategy",$seoStrategy);

    } catch (Exception $e) {
        $seolog->error($staticPage." :: ".$e->getMessage());
    }
}

$analyticsObj = AnalyticsBase::getInstance();
$analyticsObj->setStaticPageVars(AnalyticsBase::MYNTRA_BRANDS);
$analyticsObj->setTemplateVars($smarty);

//Most Popular tags
$widget = new Widget(WidgetMostPopular::$WIDGETNAME);
$widget->setDataToSmarty($smarty);

func_display("browseBrands.tpl", $smarty);
?>