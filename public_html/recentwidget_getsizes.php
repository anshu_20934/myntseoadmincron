<?php

//************************************************************ /
// INCLUDE ALL FILES REQUIRED                                  /
//************************************************************ /
define("QUICK_START", 1);
define("XCART_SESSION_START", 1);
include_once("./authAPI.php");  // For design and create-my pages.
include_once("./sanitizept.inc.php");

if(isset($HTTP_GET_VARS["ids"]) ) {
	$productStyleIds=explode(',', strip_tags($HTTP_GET_VARS["ids"]));
}

$data = array();
foreach ($productStyleIds as $productStyleId) {
    $productStyleId=sanitize_int($productStyleId);

    /*
    // validate productStyleid
    $row = func_query_first("select stl.style_id, stl.target_url from mk_style_properties stl where stl.style_id='".$productStyleId."'");
    //PORTAL-836: Kundan: now make this page work for customizable styles too: simple redirect
    $validStyleId = $row['style_id'];
    $targetUrl = trim($row['target_url']);
    if(empty($validStyleId)) {
        echo "<script>alert('SOLD OUT')</script>";
        exit;
    }
    */

    $checkInStock=FeatureGateKeyValuePairs::getFeatureGateValueForKey('checkInStock');
	$fetchall = (empty($checkInStock) || $checkInStock=='false');

	$sizeOptions = func_load_product_options($productStyleId, 0, $fetchall, true, true);
	$sizeNew="";


	$unifiedSizeOptions =  Size_unification::getUnifiedSizeForOptions($sizeOptions, $productStyleId, "array");
	$sizeOptionSKUMapping =array();
	foreach ($sizeOptions as $options) {
		foreach ($unifiedSizeOptions as $key=>$value) {
			if($options['value'] == $key) {
				$sizeOptionSKUMapping[$options['sku_id']] = $value;
			}
		}
	}


    if (empty($sizeOptionSKUMapping)) $sizeOptionSKUMapping = array('SOLD OUT');
    $data[$productStyleId] = $sizeOptionSKUMapping;
}

header('Content-Type: application/json');
echo json_encode($data);
exit;

?>
