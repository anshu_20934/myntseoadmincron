<?php 
include_once (dirname(__FILE__)). "/top.inc.php";
include_once ($xcart_dir."/env/DB.config.php");
include_once($xcart_dir."/env/Cache.config.php");
include_once($xcart_dir."/Cache/Redis.php");
require_once __DIR__.'/Predis/Autoloader.php';
Predis\Autoloader::register();


echo "checking DB health.....";
echo "<br/>Check RO connection..";
$ro_connection = mysql_connect(DBConfig::$ro['host'], DBConfig::$ro['user'], DBConfig::$ro['password'],TRUE);
if (mysql_select_db(DBConfig::$ro['db'], $ro_connection ) == false){
	echo "ro failed";
    header('HTTP/1.1 404');
    exit;
}
echo "<br/>RO connection fine";
echo "<br/>Check RW connection";
$rw_connection = mysql_connect(DBConfig::$rw['host'], DBConfig::$rw['user'], DBConfig::$rw['password']);
if(mysql_select_db(DBConfig::$rw['db'], $rw_connection ) == false){
    echo "rw failed";
    header('HTTP/1.1 404');
    //@mail(EmailListsConfig::$dbConnectionFailure, 'CRITICAL : Portal database not accessible - RW', 'Failed while initializing read write db connections - '.$_SERVER['REQUEST_URI']);
    exit;
}
echo "<br/>RW connection fine";
echo "<br/>DB connection fine";
echo "<br />-----------------------------------------<br/> Testing Redis...";

try{


    $factory = json_decode(CacheConfig::$factory, true);
    echo "<br>check ro";
    $redisConfig = CacheConfig::$redisRO;
  	$hosts = json_decode($redisConfig, true);
  	$redis = new Predis\Client($hosts, $factory);    
    $redis->ping();
 
}

catch(Exception $e){
	echo "<br/>Redis RO Connectiion cannnot be established".$e.'<br />';
	exit;
}
echo "<br>Redis RO fine";
try{


    echo "<br />check rw";
    $redisConfig = CacheConfig::$redisRW;
    $hosts = json_decode($redisConfig, true);
 
    $redis = new Predis\Client($hosts, $factory);    
    $redis->ping();
 }
catch(Exception $e){
	echo "<br/>Redis RW Connectiion cannnot be established".$e.'<br />';
	exit;
}   

echo "<br/>Redis RW fine";

echo "<br/>Redis connection established";

echo "<br/>all well, go home";

header('HTTP/1.1 200');

exit;