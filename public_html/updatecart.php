<?php
include_once("auth.php");
$cartitem = $HTTP_POST_VARS["cartitem"];
$productsInCart = $XCART_SESSION_VARS["productsInCart"];
if( isset($HTTP_POST_VARS["update"])  )
{
	$uQty = $HTTP_POST_VARS[$cartitem."qty"];
	$uSizeQties = $HTTP_POST_VARS[$cartitem."sizequantity"];

	$thisProductInCart = $productsInCart[$cartitem];
	$thisProductInCart["quantity"] = $uQty;
	$thisProductInCart["sizequantities"] = $uSizeQties;
	$productsInCart[$cartitem] = $thisProductInCart;
}
else
if( isset($HTTP_POST_VARS["remove"]) )
{
	// remove all the temp images for this product
	// delete the original image only if the productid is not published in xcart_products
	$custominfo = $productsInCart[$cartitem]["custominfo"];
	$not_published = ($productsInCart[$cartitem]["is_published"] == 0);
	if( $not_published )
	{
		//iterate through all areas and orientations
		foreach( $custominfo["areas"] as $area)
		{
		/*	if( file_exists($area["area_image_final"]) )
				//@unlink($area["area_image_final"]);
			foreach( $area["orientations"] as $orientation)
			{
				if( file_exists($orientation["original_text"]) )
					//@unlink($orientation["original_text"]);
				if( file_exists($orientation["image_name"]) )
					//@unlink($orientation["image_name"]);
				if( file_exists($orientation["pop_up_image_path"]) )
					//@unlink($orientation["pop_up_image_path"]);
				if( file_exists($orientation["image_portal_thumbnail_160"]) )
					//@unlink($orientation["image_portal_thumbnail_160"]);
				if( file_exists($orientation["image_portal_t"]) )
					//@unlink($orientation["image_portal_t"]);
			}*/
		}
	}
	unset( $productsInCart[$cartitem] );
	sort($productsInCart);
}
else
if( isset($_GET["action"]) && $_GET["action"]=="deleteall" )
{
	$countOfProductsInCart = count($productsInCart);
	for($cartitem = 0; $cartitem < $countOfProductsInCart; $cartitem++ )
	{
		// remove all the temp images for this product
		// delete the original image only if the productid is not published in xcart_products
		$custominfo = $productsInCart[$cartitem]["custominfo"];
		$not_published = ($productsInCart[$cartitem]["is_published"] == 0);
		if( $not_published )
		{
			//iterate through all areas and orientations
			foreach( $custominfo["areas"] as $area)
			{
			/*	if( file_exists($area["area_image_final"]) )
					//@unlink($area["area_image_final"]);
				foreach( $area["orientations"] as $orientation)
				{
					if( file_exists($orientation["original_text"]) )
					//	@unlink($orientation["original_text"]);
					if( file_exists($orientation["image_name"]) )
					//	@unlink($orientation["image_name"]);
					if( file_exists($orientation["pop_up_image_path"]) )
					//	@unlink($orientation["pop_up_image_path"]);
					if( file_exists($orientation["image_portal_thumbnail_160"]) )
					//	@unlink($orientation["image_portal_thumbnail_160"]);
					if( file_exists($orientation["image_portal_t"]) )
					//	@unlink($orientation["image_portal_t"]);
				}*/
			}
		}
		unset( $productsInCart[$cartitem] );
	}
}
if(count($productsInCart) == 0)
{
	func_header_location("clearcart.php");
	exit;
}
$XCART_SESSION_VARS["productsInCart"] = $productsInCart;
func_header_location("mkmycart.php?at=c&pagetype=productdetail");
?>
