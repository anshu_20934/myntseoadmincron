<?php
require "./auth.php";
include_once(\HostConfig::$documentRoot."/include/class/class.mail.multiprovidermail.php");

use feedback\instance\MFBInstance;
use feedback\manage\MFB;

$customerEmail = $_GET['email'];

if(!empty($customerEmail)){
	
	// MFB config array
	$MFBConfigArray = array(
			"CUSTOMER_EMAIL"=>$customerEmail,
			"REFERENCE_ID"=>12345,
			"REFERENCE_TYPE"=>"ORDER",// SHIPMENT,ORDER,RETURN,SR			
			"FEEDBACK_NAME"=>"order"
	);
	
	// instantiate MFB instance
	$MFBInstanceObj = new MFBInstance();
	$MFBContent = $MFBInstanceObj->getMFBMailContent($MFBConfigArray);

	if($MFBContent !== false){		
		//send mail with MFB content appended as (type-critical)		 
		$bodyArgs = array(
				"USER"				=> "Myntra Customer",
				"DECLARATION"       => "This mail is intended only for $customerEmail",
				"MFB_FORM"			=> $MFBContent['MFBMailHTML']
		);
		
		// email template for MFB order
		$template = "voc_order_may2013";
		
		$mail_details = array(
				"template" => $template,
				"to" => $customerEmail,
				"bcc" => "myntramailarchive@gmail.com",
				"header" => 'header',
				"footer" => 'footer',
				"mail_type" => \MailType::CRITICAL_TXN
		);
		$multiPartymailer = new \MultiProviderMailer($mail_details, $bodyArgs);
		$flag = $multiPartymailer->sendMail();
		
		if($flag === false){
			//delete MFB instance in case of mail failure so that to send order MFB mail again later			
			$MFBInstanceObj->deleteMFBInstance($MFBContent['MFBInstanceId']);
		}
		
		// successful message on sending feedback
		echo "<div align='center'><span><strong>Please check your mail to give feedback</strong></span></div>";
		
	
	} else {
		// send mail without MFB appended
	}		
	
} else {
	echo "<div align='center'><span><strong>Please pass your email in query string as follows</strong></span>";
	echo "<br/><br/>";
	echo "http://qa1www12.myntra.com/csat.php?email=your_id@domain.com</div>";
}
?>