<?
include_once $xcart_dir."/include/func/func.loginhelper.php";
use revenue\payments\service\PaymentServiceInterface;

function isSecureSessionExpired(){
	global $isXHR,$login,$XCART_SESSION_VARS;		
	$sessionId = getMyntraCookie('xid');
	$secureSessionId = getMyntraCookie('sxid');
    if (empty($sessionId)) return true;
    if (empty($secureSessionId)) return true;
    if (isGuestCheckout()) return false;
    $isPromptLogin = false;
    $isPromptLogin =(PaymentServiceInterface::showLoginPromptForUser($login) ? $XCART_SESSION_VARS['prompt_login'] : 0);		
	//check for XHR
	if($isXHR && $isPromptLogin){
		header('Content-Type: application/json');
        $response = array('status' => 'ERROR_PROMPT_LOGIN', 'message' => '');
        echo json_encode($response);
        exit;
	}

	return $isPromptLogin;
}