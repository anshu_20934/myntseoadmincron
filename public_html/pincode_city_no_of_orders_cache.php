<?php
use base\dao\BaseDAO;

class PincodeCityOrders extends BaseDAO{
	private static $tab_no_of_orders;
	private static $instance = null;
    private static $locality_no_of_orders_cache;
       
    public static function init() {
	    global $xcart_dir;
        self::$tab_no_of_orders = "zip_city_orders";
        include_once($xcart_dir."/Cache/TableCache.php");
        self::$locality_no_of_orders_cache = TableCache::getTableCache(self::$tab_no_of_orders, 'location');
    }

	/**
	 * Constructor.
	 */
	protected function __construct()
	{
         self::init();
	}


	public static function getInstance(){
		if(self::$instance==null){
			self::$instance= new PincodeCityOrders();
		}
		return self::$instance;
	}

    public function getNoOfOrders($location){
    	$location = mysql_escape_string($location);
    	$noOfOrders = self::$locality_no_of_orders_cache->get($location);
    	return $noOfOrders['no_of_orders'];        
    }
        
        
}
