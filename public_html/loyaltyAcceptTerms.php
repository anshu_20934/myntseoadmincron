<?php
include_once("./auth.php");
include_once "$xcart_dir/modules/loyaltyPoints/LoyaltyPointsPortalClient.php";
require_once $xcart_dir."/include/class/class.mymyntra.php";
include_once "$xcart_dir/include/func/func.utilities.php";

if ($telesales){
    echo "";exit;
}

if(empty($login)){
	$resp = array(
		'status'  => 'ERROR_PROMPT_LOGIN',
		'content' => ""
    );
	header('Content-Type: application/json');
	echo json_encode($resp);
	exit;
}
if (!checkCSRF($_token)){
	$resp = array('status' => 'ERROR','content' => '');
	echo json_encode($resp);
	exit;
}

$loyaltyAgreeTnc = filter_input(INPUT_POST, 'loyalty-agree-tnc', FILTER_SANITIZE_STRING);
$_token = filter_input(INPUT_POST, '_token', FILTER_SANITIZE_STRING);
$resp = array();
if($loyaltyAgreeTnc == 'on'){
    $setTNCResponse = LoyaltyPointsPortalClient::setTNCaccepted($login); //Set TNC accepted for the login
    if(!empty($setTNCResponse) && !empty($setTNCResponse['status']['statusType']) && $setTNCResponse['status']['statusType'] == 'SUCCESS'){
    	$resp = array(
		    'status' => 'ok',
		    'content' => ""
		);
        LoyaltyPointsPortalHelper::resetLoyaltyPointsHeaderDetails($login); // This will put smarty variable 
        $XCART_SESSION_VARS['showLoyaltyWelcomeMessage'] = true;
    }else{
    	$errorlog->error("Loyalty Points Service Error! Accept TNC , setTNCResponse : ". print_r($setTNCResponse,true));
        $resp = array('status' => 'ERROR','content' => '');
    }
}else{
	$resp = array('status' => 'ERROR','content' => '');
}

header('Content-Type: application/json');
echo json_encode($resp);
exit;