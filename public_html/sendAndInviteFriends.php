<?php
include_once("./auth.php");
include_once("./include/func/func.randnum.php");
include_once("./include/func/func.user.php");

include_once ("$xcart_dir/modules/coupon/database/CouponAdapter.php");

function validateUserInputs($HTTP_POST_VARS){
	global $login,$errorMsg;
	$fredEmailArray = array();
	$k =0;
	$error = false;
	for($i=1; $i <= 5; $i++){
		$txtBoxId = "frdemail".$i;
		$emailArray = explode(",",trim($HTTP_POST_VARS[$txtBoxId],","));
		if(!empty($emailArray[0])) {  $fredEmailArray[$k] = $emailArray[0] ; $k++;  }
	}
	if(in_array($login,$fredEmailArray)){
		$errorMsg = "You cannot send coupon to yourself. Please remove your email address from the list";
		$error = true; return $error;
	}
	$arryCount = array_count_values($fredEmailArray);
	foreach($arryCount as $_k=>$_val){
		if($_val > 1){ $error = true ;
		$errorMsg = "You can not send the coupon to your friend more than one time. Please provide different email address";
		break;
		}
	}
	return $error;
}

function func_discount_on_invite($uname,$email,$discount='',$expiryDate='',$couponType='',$usage='',$peruser='',$minAmt='' , $productid='')
{
        global $sql_tbl,$sqllog;
        $couponcode = get_rand_id(10);
        $expirydate = (!empty($expiryDate)) ? $expiryDate : time() + 14*24*60*60;
        $discount   = (!empty($discount)) ? $discount : 50 ;
    $couponType = (empty($couponType)) ? "percentage" : "absolute" ;
    
    if ($couponType == 'percentage') {
        $MRPpercentage = $discount;
    }
    elseif ($couponType == 'absolute') {
        $MRPAmount = $discount;
    }
    
    $usage      = (!empty($usage)) ? $usage : "1" ;
    $peruser    = (!empty($peruser)) ? 'Y' : "N" ;
    $productId  = (empty($productid)) ? '' :  $productid;

    $minimumAmt = (empty($minAmt)) ? '0' :  $minAmt;
    
    if ($peruser == 'Y') {
        $maxUsageByUser = 1;
    }
    else {
        $maxUsageByUser = $usage;
    }
    
    // Generate the coupon using the coupon API
    $couponAdapter = CouponAdapter::getInstance();
    while ($couponAdapter->existsCoupon($couponcode)) {
        $couponcode = get_rand_id(10);
    }
    $couponAdapter->addCoupon($couponcode, 'OldCoupons', time(), $expirydate, 
        $usage, 0, -1, 0, 0, $maxUsageByUser, 0, $minimumAmt, 0.00, $email, 
        '', time(), 'myntraprovider', $couponType, $MRPAmount, $MRPpercentage, 
        0, 'A', 'invite coupon', '', '', '', '', '', '', '', '', $productId, '');
    
    return $couponcode;
}

$errorMsg = '';
$userfirstName = $XCART_SESSION_VARS['userfirstname'];
$headers = "Content-Type: text/html; charset=ISO-8859-1 " . "\n";
$headers .= "Bcc: myntramailarchive@gmail.com " ;
if($HTTP_POST_VARS['reqType'] == "coupon")
{
	$template = 'sendcoupontofriend';
	###### generate coupon code ################
	$expireDate = time() + 30*24*60*60;
	$weblog->info("Generate coupon code  File Name ".__FILE__);
	$couponAmt =  '100' ;
	$couponUsage =  '1' ;
	$couponPerUser =  'Y' ;
	$couponType =  "absolute";
	$minAmt = '500';
	$weblog->info("Passed Args : 1- $login, 2- $couponAmt ,3 $expireDate 4 -  $couponType  6 -$couponUsage  7 -$couponPerUser 8 - $minAmt".__FILE__);

	########################## fetch the email content ###############
	$tb_email_notification  = $sql_tbl["mk_email_notification"];
	$template_query = "SELECT * FROM $tb_email_notification WHERE name = '$template'";
	$sqllog->debug("Query :".$template_query."  File Name ".__FILE__);
	$template_result = db_query($template_query);
	$row = db_fetch_array($template_result);

	$subject = $row['subject'];
	$content = $row['body'];
	if(strpos($content,"[COUPON_AMT]") !== FALSE){ $content = str_replace("[COUPON_AMT]",$couponAmt,$content); }
	if(strpos($content,"[COUPON_USAGE]") !== FALSE) { $content = str_replace("[COUPON_USAGE]",$couponUsage ,$content); }
	if(strpos($content,"[YOUR_NAME]") !== FALSE) { $content = str_replace("[YOUR_NAME]",$userfirstName,$content); }
	$sendMailStatus = false;

	$isError = validateUserInputs($HTTP_POST_VARS);
	//get orderid and currentime
	$current_time=time();
	$orderid=$HTTP_POST_VARS['orderid'];
	if(!$isError){
		###### send coupon code to your friends ################
		for($i=1; $i <= 5; $i++)
		{
			$txtBoxId = "frdemail".$i;
			$txtBoxFrdId = "frdname".$i;
			$emails = trim($HTTP_POST_VARS[$txtBoxId],",");
			$friendName = trim($HTTP_POST_VARS[$txtBoxFrdId]);
			if(strpos($content,"[FRIEND]") !== FALSE){ $emailContent = str_replace("[FRIEND]",$friendName,$content); }

			if(!empty($emails))
			{
				$weblog->info("Friends Email List :".$emails."  File Name ".__FILE__);
				$emailArray = explode(",",trim($emails,","));
				for($j=0; $j < 1; $j++)
				{
					$email = trim($emailArray[$j]);
					########  Passed Args : 1- login, 2- Amount ,3 expire date 4 - coupon type absolute/percent 6 -usage 7 - per user(Y)/or not(N) ##########
					$weblog->info("Calling function : func_discount_on_registration");
					$couponcode = func_discount_on_invite($login,$email,$couponAmt,$expireDate,$couponType,$couponUsage,$couponPerUser,$minAmt);
					$weblog->info("Generated coupon code ".$couponcode."  File Name ".__FILE__);   
					if(strpos($content,"[COUPON_CODE]") !== FALSE) { $couponContent = str_replace("[COUPON_CODE]",$couponcode,$content); }
                    $mail_detail = array(
                                        "to"=>$email,
                                        "subject"=>$subject,
                                        "content"=>$couponContent,
                                        "from_name"=>$userfirstName,
                                        "from_email"=>$login,
                                        "header"=>$headers,
                                    );
                    if(send_mail_on_domain_check($mail_detail)){					
					 $sendMailStatus = true;
					 $weblog->info("Mail sent successfully to :".$email."  File Name ".__FILE__);
					 }
					 else{
					  $weblog->info("Mail not sent to :".$email."  File Name ".__FILE__);
					 }
					//track for all coupon codes sent to email ids
					$query="insert into mk_discount_coupon_capture values('".$orderid."','".$email."','".$couponcode."','".$current_time."')";
					$email_logs = db_query($query);
					
                }
			}
		}
		if($sendMailStatus)
		echo "SUCCESS||You have successfully sent the coupon to your friends";
		else
		echo "FAILED||Unable to send mail to your friend(s).";
	}
	else{
		echo  "FAILED||$errorMsg";
	}
}
else if($HTTP_POST_VARS['reqType'] == "invite")
{
	//$template = 'invitefrdtoseedesign';commented for design contest in wipro
    $template = 'wiproinvitefrdtoseedesign';
	###### generate coupon code ################
	$productId = $HTTP_POST_VARS['productid'];
	$weblog->info(" Product Id :".$productId."  File Name ".__FILE__);
	$expireDate = time() + 30*24*60*60;
	$weblog->info("Generate coupon code for $HTTP_GET_VARS[reqType] File Name ".__FILE__);
	$couponAmt =  '20' ;
	$couponUsage =  '1' ;
	$couponPerUser =  'Y' ;
	$couponType =  "percent";
	$minAmt = '';

	########################## fetch the email content ###############
	$tb_email_notification  = $sql_tbl["mk_email_notification"];
	$template_query = "SELECT * FROM $tb_email_notification WHERE name = '$template'";
	$sqllog->debug("Query :".$template_query."  File Name ".__FILE__);
	$template_result = db_query($template_query);
	$row = db_fetch_array($template_result);


	$subject = $row['subject'];
	$content = $row['body'];
	######## product url #############
    $sql = "SELECT p.name,pr.designer,pr.product FROM mk_product_type as p INNER JOIN xcart_products as pr ON p.id = pr.product_type_id
	WHERE pr.productid =  '".$productId."' " ;
    $p_result = db_query($sql);
	$prType = db_fetch_array($p_result);
	$designType = $prType['name'];
	$designID = $prType['designer'];
    $productName = $prType['product'];
	
	$sql = "SELECT firstname FROM ".$sql_tbl['customers']." WHERE login='$login'";
	$c_result = db_query($sql);
	$c_rows = db_fetch_array($c_result);
	$_designerName = $c_rows['firstname'];
	$_designerURL = $http_location."/designer/".$_designerName."/".$designID;
	
	if(strpos($subject,"[DESIGN_TYPE]") !== FALSE) { $subject = str_replace("[DESIGN_TYPE]",$designType ,$subject); }
	if(strpos($subject,"[YOUR_NAME]") !== FALSE) { $subject = str_replace("[YOUR_NAME]",$userfirstName ,$subject); }
	$productURL = $http_location."/".$productName."/".$designType."/FC/PD/".$productId;
	
	if(strpos($content,"[COUPON_AMT]") !== FALSE){ $content = str_replace("[COUPON_AMT]",$couponAmt."%",$content); }
	if(strpos($content,"[COUPON_USAGE]") !== FALSE) { $content = str_replace("[COUPON_USAGE]",$couponUsage ,$content); }

	if(strpos($content,"[DESIGN_TYPE]") !== FALSE) { $content = str_replace("[DESIGN_TYPE]",$designType ,$content); }
	if(strpos($content,"[YOUR_NAME]") !== FALSE) { $content = str_replace("[YOUR_NAME]",$userfirstName,$content); }
	if(strpos($content,"[URL_OF_DESIGN]") !== FALSE) { $content = str_replace("[URL_OF_DESIGN]",$productURL,$content); }
	if(strpos($content,"[ALL_DESIGNS]") !== FALSE) { $content = str_replace("[ALL_DESIGNS]",$_designerURL,$content); }
	
	$sendMailStatus = false;
	$isError = validateUserInputs($HTTP_POST_VARS);
	
    if(!$isError){
		###### send coupon code to your friends ################
		for($i=1; $i <= 5; $i++)
		{
			$txtBoxId = "frdemail".$i;
			$txtBoxFrdId = "frdname".$i;
			$emails = trim($HTTP_POST_VARS[$txtBoxId],",");
			$friendName = trim($HTTP_POST_VARS[$txtBoxFrdId]);
			if(strpos($content,"[FRIEND]") !== FALSE){ $emailContent = str_replace("[FRIEND]",$friendName,$content); }

			if(!empty($emails))
			{
				$weblog->info("Friends Email List :".$emails."  File Name ".__FILE__);
				$emailArray = explode(",",trim($emails,","));
				for($j=0; $j < 1; $j++)
				{
					$email = trim($emailArray[$j]);
					$weblog->info("Email sent to :".$email."  File Name ".__FILE__);
					$weblog->info("Passed Args : 1- $login, 2- $couponAmt ,3 $expireDate 4 -  $couponType  6 -$couponUsage  7 -$couponPerUser ".__FILE__);
					########  Passed Args : 1- login, 2- Amount ,3 expire date 4 - coupon type absolute/percent 6 -usage 7 - per user(Y)/or not(N) ##########
					$weblog->info("Calling function : func_discount_on_registration");
					$couponcode = func_discount_on_invite($login,$email,$couponAmt,$expireDate,$couponType,$couponUsage,$couponPerUser,$minAmt,$productId);
					if(strpos($content,"[COUPON_CODE]") !== FALSE) { $couponContent = str_replace("[COUPON_CODE]",$couponcode,$emailContent); }
					$mail_detail = array(
                                        "to"=>$email,
                                        "subject"=>$subject,
                                        "content"=>(!empty($couponContent))?$couponContent:$emailContent,
                                        "from_name"=>$userfirstName,
                                        "from_email"=>$login,
                                        "header"=>$headers,
                                    );
                    if(send_mail_on_domain_check($mail_detail))
                    	$sendMailStatus = true;

				}
			}
		}
		if($sendMailStatus)
		echo "SUCCESS||Your invitations have been sent successfully";
		else
		echo "FAILED||Unable to send mail to your friend(s).";
	}else{
		echo  "FAILED||$errorMsg";
	}

}
?>
