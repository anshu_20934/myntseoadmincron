<?php
/**
 * This class contains APIs for omniture (web analytics tool) tracking.
 * @author neha
 *
 */
class AnalyticsBase
{
	// * Static pages PageType
	const TEXT = "static";
	
	// * * Static pages Top Level Category under pageType AnalyticsBase::TEXT
	const MYNTRA_INFORMATION	= "myntra information";
	const CUSTOMER_SERVICE		= "customer service";
	const CUSTOMER_VIEWED		= "customer viewed";
	
	// * * Static Pages Second Level Category under pageType AnalyticsBase::TEXT
	// * * * Under Level1Category AnalyticsBase::MYNTRA_INFORMATION
	const ABOUT_US			= "about us";
	const CAREERS			= "careers";
	const CONTACT_US		= "contact us";
	// * * * Under Level1Category AnalyticsBase::CUSTOMER_SERVICE
	const FAQ				= "faq";
	const RETURN_POLICY		= "return policy";
	const PRIVACY_POLICY	= "privacy policy";
	const SHIPPING_POLICY	= "shipping policy";
	const TERM_CONDITIONS	= "term conditions";
	const MYNTRA_BRANDS		= "all brands page";
	const MRP_LANDING_PAGE	= "mrp landing page";
	// * * * Under Level1Category AnalyticsBase::CUSTOMER_VIEWED
	const RECENTLY_VIEWED	= "recently viewed";
	const ORDER_FEEDBACK	= "order feedback";
	const RESET_PASSWORD	= "reset password";
	
	// * Non transactional PageTypes
	const HOME_PAGE		= "home";
	const SEARCH_PAGE	= "search";
	const LANDING_PAGE	= "browse";
	const PDP			= "pdp";
	const REGISTER		= "register";
	const MYMYNTRA		= "my myntra";

	// * Transactional pageTypes : Cart and Checkout PageTypes
	const CART_PAGE			= "cart";
	const CHECKOUT_LOGIN	= "login";
	const CHECKOUT_SHIPPING	= "checkout: shipping address";
	const PAYMENT_PAGE		= "checkout: billing";
	const CONFIRMATION		= "checkout: confirmation";
	
	
	protected $log = null;
	var $analyticStats = null;
	
	public function __construct() {
		global $errorlog;
		$this->log = $errorlog;
		$this->analyticStats = array();
	}
	
	
	/**
	 * Singleton Instance
	 */ 
	public static function getInstance() {
		static $instance;
		if(!$instance) {
			$instance = new AnalyticsBase();
		}
		return $instance;
	}
	
	/**
	 * Setting eVar assigned for AB Testing in Omniture to appropriate value.
	 * We need to ensure that traffic variable eVarxx where xx is between 25 and 75, has been created as page level traffic variable
	 */
	public function setABTestData($key, $value) {
		if(is_numeric($key)) {
			if($key>=24 and $key<=75) {
				// Safe Guarding ABTest variables to be used in Omniture.
				$this->analyticStats["eVar$key"] = $value;
			}
		}
	}
	
	/**
	 * Context specific omniture code for all pages on site. We set the following variables:
	 * @param $pageType string Function of the page eg., search, home, text, etc.
	 * @param $level1Category string TOP level category of the page
	 * @param $level2Category string 2nd level category of the page
	 * @param $level3Category string 3rd level category of the page
	 * @param $uniqueId string Unique identifier of a page. Example: StyleID if on PDP page
	 */
	public function setContextualPageData($pageType, $level1Category=null, $level2Category=null, $level3Category=null) {
		if( $this->isCategoryDefinitionProper($pageType, $level1Category, $level2Category, $level3Category)== true) {
			$this->analyticStats["pageName"] = $pageType;
			if(!empty($level1Category)) {
				$this->analyticStats["pageName"].= ": " . $level1Category;
			}
			if(!empty($level2Category)) {
				$this->analyticStats["pageName"].= " > " . $level2Category;
			}
			if(!empty($level3Category)) {
				$this->analyticStats["pageName"].= " > " . $level3Category;
			}
			
			$this->analyticStats["channel"]= empty($level1Category) ? $pageType : $level1Category;
					
			$this->analyticStats["prop1"] = $pageType;
			
			$this->analyticStats["prop2"] = $this->analyticStats["channel"];	// if level2Category is null.
			if(!empty($level2Category)) {
				// if level2Category is not null, then append it to prop2
				$this->analyticStats["prop2"].= " > " . $level2Category;
			}
			
			$this->analyticStats["prop3"] = $this->analyticStats["prop2"];		
			if(!empty($level3Category)) {
				// if level3Category is not null, then append it to prop3
				$this->analyticStats["prop3"].= " > " . $level3Category;
			}
			
		} else {
			$this->analyticStats["pageName"]= "none";
			$this->analyticStats["channel"] = "none";
			$this->analyticStats["prop1"]   = "none";
			$this->analyticStats["prop2"]   = "none";
			$this->analyticStats["prop3"]   = "none";
		}
	}
	
	/**
	 * Ensure that level1 category cannot be null if level2 category is present and so on. 
	 */
	private function isCategoryDefinitionProper($pageType, $level1, $level2, $level3){
		if (empty($pageType)) {
			$this->log->error("PageType cannot be null .\n");
			return false;
		} else if (empty($level1) && (!empty($level2) || !empty($level3))) {
			$this->log->error("A level1 category cannot be null when a level2 or level3 category is present.\n");
			return false;
		} else if (empty($level2) && !empty($level3)) { 
			$this->log->error("A level2 category cannot be null when a level3 category is present.\n");
			return false;
		} else {
			return true;
		}
	}

	/**
	 * Setting possible Campaign Parameters from cid / gclid / utm parameters
	 * Function makes use of GET parameters for setting s.campaign parameter.
	 * @param Smarty object $smarty
	 */
	private function setExternalCampaignTracking($smarty) {
		$externalCampaign = "";
		$campaignParams = array();
		
		if(isset($_GET['cid']) && !empty($_GET['cid'])) {
			$externalCampaign = $_GET['cid'];
			$campaignParams['campaign'] = "campaign:".$_GET['cid'];
			
		} else if(isset($_GET['gclid']) && !empty($_GET['gclid'])) {
			$externalCampaign = $_GET['gclid'];
			
			$campaignParams['source'] = "source:google";
			$campaignParams['medium'] = "medium:cpc";
			
			// 01.August.2012 Need lot more investigation for this. 
			// Google generates a new gclid, so its difficult to map to exact internal google campaign.
			// There are lot more internal utm paramters such as utm_ad_group, utm_ad_slot internal to gclid

			// 31.August.2012.. Omniture can't figure out any internal data from gclid parameter which is GA specific.
			// Doing a simple hardcoding of campaign to 'gclid' so that SAINT Classification is easy to create for this key.
			$campaignParams['campaign'] = "campaign:gclid";
			
		} else if(isset($_GET['utm_source']) || isset($_GET['utm_medium']) || isset($_GET['utm_campaign'])) {
			// Fetch all possible utm parameters.
			$externalCampaign = $_GET['utm_campaign'];
			
			$campaignParams['source'] = "source:".$_GET['utm_source'];
			$campaignParams['medium'] = "medium:".$_GET['utm_medium'];
			$campaignParams['campaign'] = "campaign:$externalCampaign";
			
			if(!empty($_GET['utm_term']))
				$campaignParams['term'] = "term:".$_GET['utm_term'];
				
			if(!empty($_GET['utm_content']))
				$campaignParams['content'] = "content:".$_GET['utm_content'];
		}
		
		if(!empty($campaignParams)) {
			// The campaign will be visible in Omniture Sitecatalyst as pipe separated list of fields, 
			// on which we can apply SAINT classification.
			$this->analyticStats['campaign'] = implode("|", $campaignParams);
		}
	}
	
	/**
	 * Pass the variables in a json to the template 
	 * @param $smarty object
	 */
	public function setTemplateVars($smarty){
		global $XCART_SESSION_VARS;
		$login = $XCART_SESSION_VARS['login'];
		
		if(!empty($login)) {
			$this->analyticStats["eVar16"] = "logged in";
			$this->analyticStats["eVar15"] = $login;
		} else {
			$this->analyticStats["eVar16"] = "logged out";
			$this->analyticStats["eVar15"] = "";
		}
		
		// Set external campaign parameters;
		$this->setExternalCampaignTracking($smarty);
		
		$analyticsJson = json_encode($this->analyticStats);
		$smarty->assign("analytics_json", $analyticsJson);
	}

	/**
	 * set omniture events on PDP page
	 * @param $productStyleId string styleId
	 */
	public function setPDPEventVars($productStyleId, $season, $year){
		$this->analyticStats["pageName"] = AnalyticsBase::PDP . ": " . $productStyleId;
		$this->analyticStats["events"] = "prodView";
		$this->analyticStats["products"] = ";" . $productStyleId;
		$productSeason = $season;
		$productYear = $year;
		if(empty($season)) {
			$productSeason = 'seasonNotSet';
		}
		if(empty($year)) {
			$productYear = 'yearNotSet';
		}
		$this->analyticStats["eVar20"] = "$productSeason-$productYear";
	}
	
	/**
	 * Set event on select address page, the First step in the order pipe line (if user not logged in)
	 */
	public function setInternalCampaignTrackingCode($bannerTrackingCode) {
		if(!empty($bannerTrackingCode)) {
			$this->analyticStats["eVar3"] = $bannerTrackingCode;
		}
	}
	
	
	/**
	 * Set event on select address page, the First step in the order pipe line (if user not logged in)
	 */
	public function setChooseShippingLoginEventVars() {
		$this->analyticStats["events"] = "event8";
	}
	
	/**
	 * Set event on select address page, the Second step in the order pipe line
	 */
	public function setChooseShippingAddressEventVars() {
		$this->analyticStats["events"] = "event9";
	}

	/**
	 * Set event on payment page, the Last step in the order pipe line
	 */
	public function setPaymentEventVars(){
		$this->analyticStats["events"] = "event10";
	}
	
	/**
	 * Cart specific omniture code. During addition of an item to Cart we send - event: scAdd,scOpen, products = productID
	 * If the $productStyleId is null that means nothing is added in the cart, its just a cart-view action. So we dont set sc events in this case.
	 * @param $itemsInCart int No. of items in cart
	 * @param $productStyleId string styleId
	 */ 	
	public function setCartEventVars($itemsInCart, $productStyleId) {
		if($productStyleId != null) {
			if (empty($itemsInCart)) { 
				$this->analyticStats["events"] = "scAdd,scOpen";
			}
			else {
				$this->analyticStats["events"] = "scAdd";
			}
			$this->analyticStats["products"] = ";" . $productStyleId;
		}
	}
	
	/**
	 * Set variables after the purchase is made
	 * @param $productsInCart array Array of products
	 * @param $paymentMethod string payment method 
	 * @param $orderid string orderid 
	 */
	public function setPurchasedEventVars($productsInCart, $paymentMethod, $orderid, $totalItemRevenue, $cashbackRedeemed=0) {
		$this->analyticStats["products"] = "";
		foreach($productsInCart as $product) {

			// Pravin - Need to improvise on this code here. What happens if revenue definition changes ?
			$revenueFromProduct = round(($product["total_amount"] - $product["coupon_discount"]) -
				 (($product["total_amount"] - $product["coupon_discount"])/$totalItemRevenue) * $cashbackRedeemed,2);
				 
			if($revenueFromProduct<0) {
				$revenueFromProduct=0;	// Just in case shipping charges tend to make this value negative.
			}
			
			$vatamount = $product['vatamount'];
			$shippingCharge = 0;
			$couponDiscount = $product['coupon_discount'];
			$tradeDiscount = $product['discount'];
			
			if(empty($vatamount) || !is_numeric($vatamount)) {
				$vatamount=0;
			}
			
			if(empty($couponDiscount) || !is_numeric($couponDiscount)) {
				$couponDiscount=0;
			}
			
			if(empty($tradeDiscount) || !is_numeric($tradeDiscount)) {
				$tradeDiscount=0;
			}

			// Need to check if eventN parameters need to be pipe separated
			$this->analyticStats["products"]= 
				$this->analyticStats["products"]. ";" 
				. $product["productStyleId"]	. ";"
				. $product['quantity']			. ";"
				. $revenueFromProduct			. ";"
				. "event11=$vatamount|"			//	event11 used for Tax	
				. "event12=$shippingCharge|"	//	event12 used for Shipping Charge per item. Currently set as 0
				. "event14=$couponDiscount|"	//	event14 used for Coupon Amount
				. "event15=$tradeDiscount,";	//	event15 used for Discount Amount (Trade discount)
		}
		
		$this->analyticStats["products"] = substr($this->analyticStats["products"], 0, -1); //remove the last comma
		$this->analyticStats["eVar12"] = $paymentMethod;
		
		// Someday if we give user options to select shipping preferences (Eg: 2 Day, Overnight, etc), then we need to set eVar13
		// $this->analyticStats["eVar13"] = shipping/fulfillment method chosen by vendor.
		 
		$this->analyticStats["events"] = "purchase,event11,event12,event14,event15";
		$this->analyticStats["purchaseID"] = $orderid;
		
		// Due to some Indian Govt compliance issues, we are currently not capturing customer Billing Address Details.
		// $this->analyticStats["zip"] = $zip;  	// This needs to be set to customer billing zipcode if compliance permits.
		// $this->analyticStats["state"] = $state;  // This needs to be set to customer billing state if compliance permits.
	}
	
	/**
	 * Set variables to track the internal search
	 * @param $searchTerm string The search term entered by user
	 * @param $countSearchResults No. of results returned on that search term
	 */
	public function setSearchEventVars($searchTerm, $countSearchResults){
		if(empty($this->analyticStats["eVar3"])) {
			// In case Internal Campaign code is set in eVar3, then we should not be settings search Term
			$this->analyticStats["prop4"] = $searchTerm;
			$this->analyticStats["prop5"] = empty($countSearchResults) ? "zero" : $countSearchResults;
		}
	}
	
	/**
	 * Set variables to track merchandising category on browse pages.
	 * @param $browsePageCategory string The first level of breadcrumb on browse page
	 */
	public function setMerchandisingBrowseCategoryData($browsePageCategory) {
		if(!empty($browsePageCategory)) {
			$this->analyticStats["eVar4"] = $browsePageCategory;
		}
	}
	
	public function setHeaderLinkVars($headerLink) {
		if(!empty($headerLink)) {
			$this->analyticStats["eVar21"] = $headerLink;
		}
	}
	
	public function getAnalyticsBaseStaticPageName($staticpage) {
		switch($staticpage) {
			case "aboutus"			: return AnalyticsBase::ABOUT_US;
			case "careers"			: return AnalyticsBase::CAREERS;
			case "contactus"		: return AnalyticsBase::CONTACT_US;
			case "faqs"				: return AnalyticsBase::FAQ;
			case "privacypolicy"	: return AnalyticsBase::PRIVACY_POLICY;
			case "recentlyviewed"	: return AnalyticsBase::RECENTLY_VIEWED;
			case "termsofuse"		: return AnalyticsBase::TERM_CONDITIONS;
		}
		return $staticpage;
	}
	
	/**
	 * Set pageType, level1category, level2category for static pages.
	 * @param $staticpage string The Static Page user is on.
	 */
	public function setStaticPageVars($analyticsBasePageName){
		$level1category = "$analyticsBasePageName-unknown";
		$level2category = "$analyticsBasePageName-unknown";
		
		switch ($analyticsBasePageName) {
			case AnalyticsBase::ABOUT_US :
				$level1category = AnalyticsBase::MYNTRA_INFORMATION;
				$level2category = AnalyticsBase::ABOUT_US;
				break;
				
			case AnalyticsBase::CAREERS :
				$level1category = AnalyticsBase::MYNTRA_INFORMATION;
				$level2category = AnalyticsBase::CAREERS; 
				break;
				
			case AnalyticsBase::CONTACT_US :
				$level1category = AnalyticsBase::MYNTRA_INFORMATION;
				$level2category = AnalyticsBase::CONTACT_US;
				break;
				
			case AnalyticsBase::FAQ :
				$level1category = AnalyticsBase::CUSTOMER_SERVICE;
				$level2category = AnalyticsBase::FAQ;
				break;
				
			case AnalyticsBase::PRIVACY_POLICY :
				$level1category = AnalyticsBase::CUSTOMER_SERVICE;
				$level2category = AnalyticsBase::PRIVACY_POLICY;
				break;
				
			case AnalyticsBase::SHIPPING_POLICY :
				$level1category = AnalyticsBase::CUSTOMER_SERVICE;
				$level2category = AnalyticsBase::SHIPPING_POLICY;
				break;
				
			case AnalyticsBase::RETURN_POLICY :
				$level1category = AnalyticsBase::CUSTOMER_SERVICE;
				$level2category = AnalyticsBase::RETURN_POLICY;
				break;
				
			case AnalyticsBase::MYNTRA_BRANDS :
				$level1category = AnalyticsBase::MYNTRA_INFORMATION;
				$level2category = AnalyticsBase::MYNTRA_BRANDS;
				break;
								
			case AnalyticsBase::MRP_LANDING_PAGE :
				$level1category = AnalyticsBase::MYNTRA_INFORMATION;
				$level2category = AnalyticsBase::MRP_LANDING_PAGE;
				break;
				
			case AnalyticsBase::RECENTLY_VIEWED :
				$level1category = AnalyticsBase::CUSTOMER_VIEWED;
				$level2category = AnalyticsBase::RECENTLY_VIEWED;
				break;
				
			case AnalyticsBase::ORDER_FEEDBACK :
				$level1category = AnalyticsBase::CUSTOMER_VIEWED;
				$level2category = AnalyticsBase::ORDER_FEEDBACK;
				break;

			case AnalyticsBase::RESET_PASSWORD :
				$level1category = AnalyticsBase::CUSTOMER_VIEWED;
				$level2category = AnalyticsBase::RESET_PASSWORD;
				break;
				
			case AnalyticsBase::TERM_CONDITIONS	:
				$level1category = AnalyticsBase::CUSTOMER_SERVICE;
				$level2category = AnalyticsBase::TERM_CONDITIONS;
				break;
		}
		
		$this->setContextualPageData(AnalyticsBase::TEXT, $level1category, $level2category);
	}
}
?>