<?php
require_once "$xcart_dir/modules/coupon/mrp/SmsVerifier.php";
require_once "$xcart_dir/myordershelper.php";
require_once($xcart_dir."/modules/giftcards/GiftCardsHelper.php");

$shopFest = FeatureGateKeyValuePairs::getBoolean("myntrashoppingfest");
$nodeCancelEnabled = FeatureGateKeyValuePairs::getBoolean('mymyntra.cancel.enable');

//deeplinking changes
$orderAction = mysql_real_escape_string(filter_input(INPUT_GET,'act',FILTER_SANITIZE_STRING));
$oid = mysql_real_escape_string(filter_input(INPUT_GET,'oid',FILTER_SANITIZE_NUMBER_INT));

$order_action = (string)$orderAction;

$smarty->assign("shopFest",$shopFest);
$smarty->assign("oid",$oid);
$smarty->assign("order_action",$order_action);
$gco_bought=GiftCardsHelper::getGiftCardOrdersByEmail($login);
$giftCardOrdersFlag=false;
$hasAtleastOneGiftCardCompletedOrder=false;
foreach ($gco_bought as $_k => $_v){
	if($_v->status == "Completed"){
		$hasAtleastOneGiftCardCompletedOrder=true;
		break;		
	}
}
$hasAtleastOneGiftCardCompletedOrder=true;
if(count($gco_bought) > 0 && $hasAtleastOneGiftCardCompletedOrder){
	$giftCardOrdersFlag=true;
}
$smarty->assign("giftCardOrdersFlag", $giftCardOrdersFlag);
$smarty->assign("gco_bought", $gco_bought);

$cancellation_codes = func_query("select cancel_reason,cancel_reason_desc from cancellation_codes where cancellation_mode='FULL_ORDER_CANCELLATION' and cancel_type = 'CCR' and is_active = 1",true);
$cancellation_reasons = array();
foreach ($cancellation_codes as $cancellation_code){
	$cancellation_reasons[$cancellation_code['cancel_reason']] = $cancellation_code['cancel_reason_desc']; 
}

/*Shopping Festival order cancellation code*/
if($shopFest)
{
	$fest_cancel_message = WidgetKeyValuePairs::getWidgetValueForKey("fest_cancel_message");
	$smarty->assign("fest_cancel_message",$fest_cancel_message);	
}
/*End of shopfest code*/
$smarty->assign("cancellation_reasons", $cancellation_reasons);
$smarty->assign("nodeCancelEnabled", $nodeCancelEnabled);
$smarty->display("my/orders.tpl");

