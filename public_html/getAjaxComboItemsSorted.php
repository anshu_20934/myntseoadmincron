<?php


include_once("./auth.php");

include_once ("$xcart_dir/include/class/mcart/class.MCart.php");
include_once ("$xcart_dir/include/class/mcart/class.MCartFactory.php");
include_once ("$xcart_dir/include/class/mcart/class.MCartUtils.php");
include_once ("$xcart_dir/include/class/search/SearchProducts.php");
include_once("$xcart_dir/include/class/instrumentation/BaseTracker.php");
include_once($xcart_dir."/include/func/func.mkcore.php");
require_once($xcart_dir."/include/func/func.utilities.php");
require_once($xcart_dir."/include/class/widget/class.static.functions.widget.php");
include_once($xcart_dir."/include/class/search/UserInterestCalculator.php");
include_once($xcart_dir."/include/class/class.feature_gate.keyvaluepair.php");
include_once($xcart_dir."/include/class/notify/class.notify.php");
$publickey  = "6Le_oroSAAAAANGCD-foIE8DPo6zwcU1FaW3GiIX";
$privatekey = "6Le_oroSAAAAAKuArW6iPxhOlk0QVJMwWOTGVQnt";
include_once($xcart_dir."/include/func/func.utilities.php");
require_once($xcart_dir."/tracking/trackingUtil.php");
require_once($xcart_dir."/modules/discount/DiscountEngine.php");
include_once($xcart_dir."/Cache/Cache.php");

use revenue\discounts\views\AjaxComboView; 


$shownewui= $_MABTestObject->getUserSegment("productui");
$productuiABparam=$shownewui;

$smarty->assign("shownewui", $shownewui);
$smarty->assign("productuiABparam", $productuiABparam);



/** Starts here ****/

if(isset($_GET["cid"]) ) {
    $discountId=filter_input(INPUT_GET,"cid", FILTER_SANITIZE_NUMBER_INT);
}
if(isset($_GET["sortby"]) ) {
    $sortby=filter_input(INPUT_GET,"sortby", FILTER_SANITIZE_STRING);
}
if (isset($_GET["all"])){
      $allData = true;
}
else{
    $allData= false;
}
if(isset($_GET["page"]) ) {
            $page=filter_input(INPUT_GET,"page", FILTER_SANITIZE_NUMBER_INT);
}
else{
    $page=-1;
}

$discountId=sanitize_int($discountId);

$params = array("cid"=>$discountId,"mini"=>$mini,'all'=>$allData,"sortby"=>$sortby,'page'=>$page);
$view = new AjaxComboView('pdp', $params);
$view->display();

?>
