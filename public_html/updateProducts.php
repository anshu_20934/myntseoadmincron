<?php
require "./auth.php";
include_once("./include/func/func.mkmisc.php");

	
$masterCustomizationArrayD=$XCART_SESSION_VARS['masterCustomizationArrayD'];
$productsInCart = $XCART_SESSION_VARS['productsInCart'];
$productids = $XCART_SESSION_VARS['productids'];
$CCstyleid = $XCART_SESSION_VARS['CCstyleid'];

$giftcerts = $cart["giftcerts"];
$pageType = $_POST['pagetype'];

if(trim(strtolower($_POST['removeBtn'])) == "remove")
{
		$prid = $_POST['proHide'];
		if($productsInCart[$prid]['productStyleId'] == $CCstyleid)
		{
			x_session_unregister('coupondiscount');
			x_session_unregister('couponCode');
			x_session_unregister('CCstyleid');
		}
		foreach($productids AS $key=>$value)
		{
			$weblog->info("Remove the product with id ".$prid." == ".$productids[$key]);
			$weblog->info("product key value pair ".$key." == ".$value);
			if($productids[$key] == $prid)
			{
				$weblog->info("Unset info of product id ".$prid);
				unset($productids[$key]);
				unset($productsInCart[$prid]);
				if($masterCustomizationArrayD)
				{
					unset($masterCustomizationArrayD[$prid]);
				}
			}
		}
		//$productids = array_values($productids);		
		$uuid = stripslashes(getMyntraCookie('MYNTRA_SHOPPING_ID'));
		if($uuid!=""){
		    db_query("delete from mk_shopping_cart WHERE cookie='".$uuid."' ");
		    //store_cart_products($uuid,$login,$productsInCart,$productids,$masterCustomizationArrayD);
		}
}

if(trim(strtolower($_POST['updateBtn'])) == "update")
{
    $prid = $_POST['proHide'];
	$productsInCart[$prid]['quantity'] = $_POST[$prid."qty"];
	$productsInCart[$prid]['totalPrice'] = $_POST[$prid."qty"] * $productsInCart[$prid]['productPrice'];
	$productsInCart[$prid]['updateddiscount'] = $_POST[$prid."qty"] * $productsInCart[$prid]['discount'];
	$productsInCart[$prid]['sizequantities'] = $_POST[$prid."sizequantity"];
  
}
### added for addon #########

if(!empty($HTTP_POST_VARS['chk_addon']) && $HTTP_POST_VARS['updateAddon'] == "update"){
	
	$weblog->info("Creation of masterCustomizationArrayD array started");

	foreach($HTTP_POST_VARS['chk_addon'] AS $_key=>$_value)
	{
		$checkProductId = "SELECT productid,product_type_id,product_style_id,image_portal_t FROM 
		$sql_tbl[products] WHERE productid='".$_value."'";
		$checkResult  = db_query($checkProductId);
		$checkRow = db_num_rows($checkResult);
		if($checkRow != 0)
		{ 
			 $productStyleTable = $sql_tbl["mk_product_style"];
       		 $productTypeTable = $sql_tbl["mk_product_type"];
       		 $rsProductData = db_fetch_array($checkResult,MYSQL_ASSOC);
       		 
			 $productStyleId = $rsProductData['product_style_id'];
			 $productTypeId = $rsProductData['product_type_id'];
			 $productId =  $rsProductData['productid'];
			 $designImagePath = $rsProductData['image_portal_t'];
			 
			 $query = "SELECT pt.name , pt.id , ps.name as pname,ps.price FROM 
			 $productTypeTable as pt INNER JOIN $productStyleTable as ps ON 
		     pt.id =  ps.product_type WHERE ps.id = '".$productStyleId."' ";
			 $rsPtypeDetails = func_query($query) ;
			 
			$quantity =                     $HTTP_POST_VARS[$productId.'adqty']; 
			$productPrice =                 $rsPtypeDetails[0]['price'];  
			$totalPrice =  $quantity * $productPrice;
			$productTypeLabel =             $rsPtypeDetails[0]['name'];
			$productStyleName =       		$rsPtypeDetails[0]['pname'];
			
			 

        	$query = "select id,name from mk_style_customization_area where isprimary = 1 
        	and style_id = '".$productStyleId."' ";
			$rscustQuery = func_query($query);
			$defaultCustId = $rscustQuery[0]['id'];
			
			$productDetail =   array('productId' => $productId,'producTypeId' => $productTypeId
			,'productStyleId' => $productStyleId,'productStyleName' => $productStyleName
			,'productPrice'=>$productPrice,'productTypeLabel' => $productTypeLabel,
			'quantity'=> $quantity,
			'sizenames' => '','sizequantities'=>'', 'totalPrice'=>$totalPrice,
			'optionNames'=>$optionsNameArray,'optionValues'=>$optionsValueArray
			,'designImagePath'=>$designImagePath,'discount'=>$discount,'optionsId'=>$optionsId
			,'defaultCustomizationId'=>$defaultCustId,'custAreaCount'=>$custAreaCount,'productStyleType'=>'p','displayInCart'=>'N');
			$productsInCart[$productId] = $productDetail;
			    x_session_register('productsInCart');  
	
			 if(!in_array($productId, $productids)){
					$productids[] = $productId;
					x_session_register('productids');
			 }    
		}
	
	}
	
}
if($_POST['updateGcBtn'] == "Update")
{
		$gcIndex = $_POST['gcIndex'];
		$rEmail = $_POST['rEmail'];
		$rName = $_POST['rName'];

		foreach($cart["giftcerts"] AS $key=>$value)
		{
			if(($cart["giftcerts"][$key]['recipient_email'] == $rEmail) && ($cart["giftcerts"][$key]['recipient'] == $rName))
			{
				$cart["giftcerts"][$key]['quantity'] = $_POST[$gcIndex."qty"];
				$cart["giftcerts"][$key]['totalamount'] = $cart["giftcerts"][$key]['quantity'] * $cart["giftcerts"][$key]['amount'];
			}
		}
}

if($_POST['removeGcBtn'] == "Remove")
{
		$gcIndex = $_POST['gcIndex'];
		$rEmail = $_POST['rEmail'];
		$rName = $_POST['rName'];

		foreach($cart["giftcerts"] AS $key=>$value)
		{
			if(($cart["giftcerts"][$key]['recipient_email'] == $rEmail) && ($cart["giftcerts"][$key]['recipient'] == $rName))
			{
				unset($cart["giftcerts"][$key]);
			}
		}
}

x_session_register('productsInCart');
x_session_register('productids');
x_session_register('masterCustomizationArrayD');
x_session_register($cart["giftcerts"]);

//update cart items in cookie
//if(!empty($XCART_SESSION_VARS['productsInCart'])){
    $uuid = stripslashes(getMyntraCookie('MYNTRA_SHOPPING_ID'));
    if($uuid==""){
        $uuid = mysql_result(mysql_query('Select UUID()'),0);
        $expiryDate = time() + $max_cart_validity_time;
        setMyntraCookie("MYNTRA_SHOPPING_ID",$uuid,$expiryDate,'/',$cookiedomain);
        setMyntraCookie("MYNTRA_UNQ_COOKIE_ID",substr($uuid,1,20),$expiryDate,'/',$cookiedomain);
    }
    store_cart_products($uuid,$login,$XCART_SESSION_VARS['productsInCart'],$XCART_SESSION_VARS['productids'],$XCART_SESSION_VARS['masterCustomizationArrayD']);    
//}

##to check product to add from microsites
$chosenProductConfiguration = $XCART_SESSION_VARS['chosenProductConfiguration'];
$affiliateCode = $chosenProductConfiguration['affiliateCode']; 

if(!empty($affiliateCode)){
	header("location:".$http_location."/microsites/affiliatecart.php?pagetype=$pageType");
}else{
	header("location:$http_location/mkmycartmultiple.php?pagetype=$pageType");
}

?>
