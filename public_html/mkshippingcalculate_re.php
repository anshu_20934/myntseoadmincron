<?php
include_once "./auth.php";
include_once "../auth.php";
include_once("$xcart_dir/include/func/func.mk_orderbook.php");
include_once("$xcart_dir/include/func/func.mkcore.php");

include_once ("$xcart_dir/modules/coupon/exception/CouponNotFoundException.php");
include_once ("$xcart_dir/modules/coupon/database/CouponAdapter.php");
$couponAdapter = CouponAdapter::getInstance();

$amount = $XCART_SESSION_VARS["amount"];
$coupondiscount = $XCART_SESSION_VARS["coupondiscount"];
$cart_amount = $amount;

if($HTTP_GET_VARS['action']=='b_country'){
	$b_country = $HTTP_GET_VARS['country'];	

	$b_sql = "SELECT code,state FROM xcart_states WHERE country_code = '".$b_country."' " ;
	
	$label = "<font style=\"font-size:0.8em\">state</font><span class=\"mandatory\">*</span>" ;
	
	$b_state_list = func_query($b_sql);
		
	if(is_array($b_state_list) ){	
	$b_selectbox = '<select name="b_state" id="b_state" class="select">';
	
	foreach ($b_state_list AS $key=>$array){
		$b_selectbox .= '<option value="'.$array[code].'">'.$array[state].'</option>';
	}
	$b_selectbox .= '</select>' ;
	
	echo $b_selectbox . "," . $HTTP_GET_VARS['action'].",".$label;
	
	}else {
		
		$b_selectbox = '<input type="text" name="b_state" id="b_state" value="">';
		echo $b_selectbox . "," . $HTTP_GET_VARS['action'];
	}
}
 
elseif($HTTP_GET_VARS['action']=='s_country'){
	$s_country = $HTTP_GET_VARS['country'];	

	$s_sql = "SELECT code,state FROM xcart_states WHERE country_code = '".$s_country."' " ;
	
	$label = "<font style=\"font-size:0.8em\">state</font><span class=\"mandatory\">*</span>" ;
	
	$s_state_list = func_query($s_sql);
	
	if(is_array($s_state_list) ){	
		$s_selectbox = '<select name="state" id="state" class="select">';
		
		$s_selectbox .= '<option value="">--Select State--</option>';
		foreach ($s_state_list AS $key=>$array){
			$s_selectbox .= '<option value="'.$array[code].'">'.$array[state].'</option>';
		}
		$s_selectbox .= '</select>' ;
	
		echo $s_selectbox . "," . $HTTP_GET_VARS['action'].",".$label;
	
	}else {
		
		$s_selectbox = '<input type="text" name="state" id="state" value="" class="required">';
		echo $s_selectbox . "," . $HTTP_GET_VARS['action'];
	}
	
	
	
}
elseif($HTTP_GET_VARS['action']=='logistic'){
	
	$productsInCart = $XCART_SESSION_VARS['productsInCart'];
	$productids = $XCART_SESSION_VARS['productids'];
	
	$couponCode = $XCART_SESSION_VARS['couponCode'];
	
	$coupon = null;
	if (!empty($couponCode)) {
	    try {
	        $coupon = $couponAdapter->fetchCoupon($couponCode);
	    }
	    catch (CouponNotFoundException $e) {
	        $coupon = null;
	    }
	}
	
	$s_zipcode = $HTTP_GET_VARS['s_zipcode'];
    
	$s_country = $HTTP_GET_VARS['s_country'];
	$s_state = $HTTP_GET_VARS['s_state'];
	$logisticId = $HTTP_GET_VARS['logisticId'];
	
	$s_city = strtolower($HTTP_GET_VARS['s_city']);


	$shippingRate = func_shipping_rate_re($s_country,$s_state,$s_city,$productsInCart, $productids, $coupon,$logisticId,$s_zipcode, $cart_amount);
	
	$returnResult = number_format($shippingRate['totalshippingamount'],2,".",'');
	echo $returnResult;
	
}else{

	$productsInCart = $XCART_SESSION_VARS['productsInCart'];
	$productids = $XCART_SESSION_VARS['productids'];
	
	$totalQuantity = $HTTP_POST_VARS['totalquantity'];
	$totalamount = $HTTP_POST_VARS['totalamount'];
	$checksumkey = $HTTP_POST_VARS['checksumkey'];
	$vatCst = $HTTP_POST_VARS['hidevat'];
	$couponCode = $XCART_SESSION_VARS['couponCode'];
	
	$coupon = null;
	if (!empty($couponCode)) {
	    try {
	        $coupon = $couponAdapter->fetchCoupon($couponCode);
	    }
	    catch (CouponNotFoundException $e) {
	        $coupon = null;
	    }
	}
	
	$s_country = $HTTP_POST_VARS['s_country'];
	$s_state = $HTTP_POST_VARS['s_state'];
	$s_zipcode = $HTTP_POST_VARS['s_zipcode'];
    $s_city = strtolower($HTTP_POST_VARS['s_city']);
    $flag = $HTTP_POST_VARS['flag'];
    $logisticId = $HTTP_POST_VARS['logisticId'];
		
	$shippingRate = func_shipping_rate_re($s_country,$s_state,$s_city,$productsInCart, $productids, $coupon,$logisticId,$s_zipcode, $cart_amount);
	//$vatCst = (12.50 * $totalamount)/100;
	//Fix for 1257: Adding shipping cost to session
	//$shippingTotalAmount = $shippingRate['totalshippingamount'];
	//if( x_session_is_registered("shippingTotalAmount"))
	//{
	//	x_session_unregister("shippingTotalAmount");
	//}
	//x_session_register("shippingTotalAmount");
	
	$totalamount = $totalamount + $shippingRate['totalshippingamount'] +$vatCst;
	
	//Update checksum value
	//$checksumvalue = md5($login.$totalamount);
	//db_query("UPDATE mk_xcart_checksum SET checksumvalue = '$checksumvalue' WHERE checksumkey = '$checksumkey'");
			
	$returnResult = $shippingRate['totalshippingamount'].",".$totalamount.",".$shippingRate['freeShippingStatus'];
	echo $returnResult;
	

}
?>
