<?php
//************************************************************ /
// INCLUDE ALL FILES REQUIRED                                  /
//************************************************************ /
if(file_exists("./auth.php")) {
	include_once("./auth.php");  // For design and create-my pages.
} else if (file_exists("../auth.php")) {
	include_once("../auth.php"); // For sports pages
}
include_once($xcart_dir."/Profiler/Profiler.php");
Profiler::setContext("PDP");

include_once("$xcart_dir/include/class/instrumentation/BaseTracker.php");
$tracker = new BaseTracker(BaseTracker::PDP);

include_once($xcart_dir."/include/func/func.mkcore.php");
include_once($xcart_dir."/include/func/func.mkmisc.php");
require_once($xcart_dir."/include/func/func.core.php");
require_once($xcart_dir."/include/func/func_seo.php");
require_once($xcart_dir."/include/func/func.utilities.php");
require_once($xcart_dir."/include/class/search/SearchProducts.php");
require_once($xcart_dir."/include/class/widget/class.static.functions.widget.php");
include_once($xcart_dir."/include/class/search/UserInterestCalculator.php");
include_once($xcart_dir."/include/class/class.feature_gate.keyvaluepair.php");
include_once($xcart_dir."/include/class/notify/class.notify.php");
include_once($xcart_dir."/include/class/widget/headerMenu.php");
$publickey  = "6Le_oroSAAAAANGCD-foIE8DPo6zwcU1FaW3GiIX";
$privatekey = "6Le_oroSAAAAAKuArW6iPxhOlk0QVJMwWOTGVQnt";

include_once($xcart_dir."/include/fb/func.opengraph.php");
include_once($xcart_dir."/include/func/func.utilities.php");
require_once($xcart_dir."/tracking/trackingUtil.php");
require_once($xcart_dir."/modules/discount/DiscountEngine.php");
include_once($xcart_dir."/Cache/Cache.php");
include_once("$xcart_dir/Cache/CacheKeySet.php");
include_once($xcart_dir."/myntra/setShowStyleDetails.php");

$showtelesalesnumber=$_MABTestObject->getUserSegment("telesales");
$shownewui= $_MABTestObject->getUserSegment("productui");
$productuiABparam=$shownewui;

$smarty->assign("shownewui", $shownewui);
$smarty->assign("productuiABparam", $productuiABparam);

if(isset($HTTP_GET_VARS["id"]) ) {
	$productStyleId=filter_input(INPUT_GET,"id", FILTER_SANITIZE_NUMBER_INT);
}
$cache = new Cache(PDPCacheKeys::$prefix.$productStyleId, PDPCacheKeys::keySet());

$productStyleId=sanitize_int($productStyleId);

// validate productStyleid
$style_properties= $cache->get(function($productStyleId){
	$style_properties_sql="select * from mk_style_properties where style_id=".$productStyleId;
	return func_query($style_properties_sql,true);
}, array($productStyleId), "styleProperties");
$smarty->assign("style_properties",$style_properties);

//PORTAL-836: Kundan: now make this page work for customizable styles too: simple redirect
$validStyleId = $style_properties[0]['style_id'];
$targetUrl = trim($style_properties[0]['target_url']);
if(empty($validStyleId)) {
    $XCART_SESSION_VARS['errormessage']="Requested page was not found on this server";
    func_header_location("mksystemerror.php");
    exit;
}
//target_url is non blank only for customizable products. in such case just redirect
else if(!empty($targetUrl)) {
	func_header_location('/'.$targetUrl);
	exit;
}
$isStyle = true;

include_once($xcart_dir."/include/class/class.recent_viewed_content.php");
$__RECENT_HISTORY_DCONTENT->setGACategory("pdp_strip");
$__RECENT_HISTORY_DCONTENT->genContent();
$__DCONTENT_MANAGER->startCapture();

$__RECENT_HISTORY_DCONTENT->addStyleToRecentViews($productStyleId);

$telesalesNumber=WidgetKeyValuePairs::getWidgetValueForKey('telesalesNumber');
$smarty->assign('showtelesalesNumber',$showtelesalesnumber);
$smarty->assign('telesalesNumber',$telesalesNumber);
// ***************************************************************  /
// Start Filling Smarty data to load view		                    /
// ***************************************************************  /

$productStyleDetails=$cache->get(function($productStyleId){
	return func_load_product_style_details($productStyleId);
}, array($productStyleId), "productStyleDetails");

$styleiddetail = $cache->get(function($productStyleId){
	return func_style_id_detail($productStyleId);
}, array($productStyleId), "styleIdDetails");

$productTypeId=strip_tags($styleiddetail["product_type"]);
$productTypeLabel=strip_tags($styleiddetail["name"]);

$smarty->assign("pageURL",currentPageURLWithStripedQueryStrings());

//NEW SIZE CHART
$dynamicSizeChart=FeatureGateKeyValuePairs::getFeatureGateValueForKey('sizechart.dynamicSizeChart');
$smarty->assign("dynamicSizeChart",$dynamicSizeChart);

// load size chart image
//encode single quotes to %27 if any
$size_chart_image_path = str_replace("'", "%27", $productStyleDetails[0][11]);
$isCustomizable = $productStyleDetails[0][12];
$smarty->assign("size_chart_image_path",$size_chart_image_path);

$enableAllStyles=FeatureGateKeyValuePairs::getBoolean('enableAllStyles');
if(!$enableAllStyles){
	$row_disableStyle = $cache->get(function($productStyleId){
		return func_query_first_cell("select count(*) from mk_product_style where id = '".$productStyleId."' and iventory_count > 0 and styletype='P'", true);
	}, array($productStyleId), "disableStyle"); 
}
$disableProductStyle = false;

if(empty($row_disableStyle) &&(!$enableAllStyles)) {
	$disableProductStyle = true;
	$disableMessage =  	"This product is currently out-of-stock. ";
	$smarty->assign("disableProductStyle",$disableProductStyle);
	$smarty->assign("disableMessage",$disableMessage);
}

$productStyleLabel = $productStyleDetails[0][1];

$smarty->assign("productTypeType",$productStyleDetails['type']);
$smarty->assign("productTypeLabel",$productTypeLabel);
$smarty->assign("productStyleDetails",$productStyleDetails);
$smarty->assign("productStyleLabel",$productStyleLabel);
$smarty->assign("productStyleId",$productStyleId);
$smarty->assign("productTypeId",$productTypeId);

if(!empty($productTypeDetails)){
	$descriptionAndDetails = $productTypeDetails . '<br/>' . $productStyleDetails[0][3];
}
else{
	$descriptionAndDetails = $productStyleDetails[0][3];
}
$smarty->assign("descriptionAndDetails",$descriptionAndDetails);

// Load all product options
$allProductOptionDetails=$cache->get(function($productStyleId){
	return func_load_all_product_options($productStyleId, 0);
}, array($productStyleId), "all_product_options");
$smarty->assign("allProductOptionDetails",json_encode($allProductOptionDetails));
$checkInStock=FeatureGateKeyValuePairs::getFeatureGateValueForKey('checkInStock');
if($checkInStock == 'false') {
	$productOptionDetails = $allProductOptionDetails;
	$inactiveProductOptionDetails = array();
} else {
	$productOptionDetails = array();
	$inactiveProductOptionDetails = array();
	foreach ($allProductOptionDetails as $option_row) {
		if($option_row['is_active'])
			$productOptionDetails[] = $option_row;
		else
			$inactiveProductOptionDetails[] = $option_row;
	}
}

$productOptionNameValues=func_get_option_name_value_pairs($productOptionDetails);

// For size chart info
//$sizeChartDetails=func_load_product_options_new_cust($productStyleId);
//$smarty->assign("sizeChartInfo",$sizeChartDetails);

// Check if there is any discount applied.
$originalprice = $productStyleDetails[0][5];
$discountedprice = $originalprice;
$smarty->assign("originalprice", $originalprice);

/*
if(!empty($style_properties[0]['discount_rule'] )){
	$discountData = DiscountEngine::executeOnProduct($productStyleId, $style_properties[0]['discount_rule'], $originalprice);
	$discountamount = $discountData[$style_properties[0]['discount_rule']]['amount'];
	
	if(!empty($discountamount)) {
		if($discountamount  > $originalprice ){
			$discountamount = $originalprice;
		} 
		$discountDisplayText = $discountData[$style_properties[0]['discount_rule']]['displayText'];
		$discountLabelText = $discountData[$style_properties[0]['discount_rule']]['labelText'];
	    $smarty->assign("discountdisplaytext", $discountDisplayText);
	    $smarty->assign("discountlabeltext", $discountLabelText);	// 10 % Off
		$smarty->assign("discountamount", $discountamount);
		$smarty->assign("discountvalue", $discountamount);
	}
		
	$smarty->assign("discounttype", 'rule');
}*/


$discountData= $cache->get(function($productStyleId){
	return DiscountEngine::getDataForAStyle('pdp', $productStyleId);
}, array($productStyleId), "discountData");
$discountType = $discountData->discountType;
$discountAmount = $discountData->discountAmount;

foreach($discountData->discountToolTipText->params as $k=>$v) {
    $smarty->assign("dre_".$k, $v);
}
foreach($discountData->displayText->params as $k=>$v) {
    $smarty->assign("dre_".$k, $v);
}

$discountSetStyles = $discountData->discountToolTipText->relatedProducts;

if($discountSetStyles!=null && count($discountSetStyles)>0 ) {
    $smarty->assign("show_combo", true);
}else{
    $smarty->assign("show_combo", false);
}


$smarty->assign("discountamount", $discountAmount);
$smarty->assign("discountId", $discountData->discountId);
$smarty->assign("discounttype", $discountType);
$smarty->assign("discountlabeltext", $_rst[$discountData->displayText->id]);
$smarty->assign("discounttooltiptext", $_rst[$discountData->discountToolTipText->id]);


$finalprice  = ($discountedprice>0) ? $discountedprice : $originalprice;

// For Facebook metaDescription.
$meta = 'Buy ' . $style_properties[0]['product_display_name'] . ' online at Rs. ' . $finalprice . ' . ';
$meta.= ' In stock and ready to ship. All India Free Shipping and Cash On Delivery available';


if(empty($isCustomizable))
	$isCustomizable = 0;

if($isCustomizable==0) {
	// New sports styles have properties defined for them.
	$smarty->assign("objectType",getGraphObject($style_properties[0]['filters']));
	$smarty->assign("pageImage", str_replace("./", "/", $idvalue['image_portal_t']));
	
	$smarty->assign("defaultImage",$style_properties[0]['default_image']);
	
	$defaultImage = $style_properties[0]['default_image'];
	$areanames = array('default', 'top', 'front', 'back', 'right', 'left', 'bottom');

	$area_icons =  array();

	foreach($areanames as $singlearea) {
		if(!empty($style_properties[0]["$singlearea" . "_image"]) && $style_properties[0]["$singlearea" . "_image"]!="$cdn_base/") {
			$singleareaproperties = array();
			$imagepath = $style_properties[0]["$singlearea" . "_image"];
				
			$singleareaproperties["image"] = $imagepath;
			$singleareaproperties["name"] = $singlearea;
			
			// With new rectangular images in places, area icons are 48x64 pixels.
			$singleareaproperties["area_icon"] = str_replace('_images.', '_images_48_64.', $imagepath);
				
			$area_icons[] = $singleareaproperties;
		}
	}
	
	$smarty->assign("area_icons", $area_icons);
	$smarty->assign("pageTitle", $style_properties[0]['product_display_name']." | Myntra");
	$smarty->assign("productDisplayName", $style_properties[0]['product_display_name']);

	$smarty->assign("cstatus", "1"); // We assumed it is already customized. The is to by-pass 'Buy Now' checks.
		
	// Fetching related products to show on right column
		
	if(!empty($style_properties[0]['product_tags'])) {
		$producttags= explode(",",$style_properties[0]['product_tags']);
	}

	if(!empty($producttags)) {
		//get related designs based on product tags
		$searchProducts = SearchProducts::getInstance();
		if($shownewui == 'newui'){
			$related_designs=$searchProducts->getRelatedProductsPDP($producttags,"0_style_$productStyleId",0,5);
		}
		else{
			$related_designs=$searchProducts->getRelatedProductsPDP($producttags,"0_style_$productStyleId",0,4);
		}	
		

		$smarty->assign("related_products", $related_designs);

		$keywords = array();
		foreach($producttags as $t) {
			$keywords[] = trim($t);
		}
		
		$keywords [] = 'sportswear';
		$keywords = array_merge($keywords, explode(" ",$style_properties[0]['product_display_name']));
			
		$smarty->assign("metaKeywords", implode(",", $keywords));
			
		$meta = 'Buy ' . $style_properties[0]['product_display_name'].' online at Rs. ';
			
		if($discountedprice>0) {
			$meta.= "$discountedprice";
		} else {
			$meta.= "$originalprice";
		}
	}
}	

if($isStyle && !empty($productStyleId)) {
	UserInterestCalculator::trackAction($productStyleId, UserInterestCalculator::PRODUCT_VIEW);
}
	
$smarty->assign("isreturninguser", ($expiry_time-time())/(30*24*60*60)<=14?"returning":"new");
$buynowdisabled=false;
if(empty($productOptionDetails)) {
	$buynowdisabled=true;
	$smarty->assign("buynowdisabled", true);
	$meta.= '. In stock and ready to ship. All India Free Shipping and Cash On Delivery available';
}else{
	$buynowdisabled=false;
	$smarty->assign("buynowdisabled", false);
}

$smarty->assign("metaDescription", $meta);

$sizeSpecUsed="";

$flattenSizeOptions=false;
$gender=$style_properties[0]["global_attr_gender"];
$brand_name=$style_properties[0]["global_attr_brand"];

$brand_filters=$cache->get(function($brand_name){
	$logo_sql="select filter_name,logo from mk_filters where filter_name='".$brand_name."' and is_active=1";
	return func_query($logo_sql,true);
}, array($brand_name), "brandFilters");
if(!empty($brand_filters[0]['logo'])) {
$smarty->assign("brand_logo", $brand_filters[0]['logo']);
}
$smarty->assign("brandname", $brand_name);
if(empty($gender) || $gender == "NA"){
	$gender="Men";
}

//Retrieving data from mk_catalogue_classification for the product
$catalogData = $cache->get(function($productStyleId){
	$catalogQuery ="select sp.size_representation_image_url,cc1.typename as articletype,cc2.typename as sub_category,cc3.typename as master_category from mk_style_properties sp, mk_catalogue_classification cc1,mk_catalogue_classification cc2 ,mk_catalogue_classification cc3 where  sp.global_attr_article_type=cc1.id and cc1.parent1=cc2.id and cc1.parent2=cc3.id and sp.style_id = $productStyleId";
	return func_query_first($catalogQuery, true);
}, array($productStyleId), "catalogQuery"); 
$articleType = $catalogData["articletype"];
$masterCategory = $catalogData["master_category"];
$sizeRepresentationImageUrl=$catalogData["size_representation_image_url"];

//$sizeOptions=Size_unification::getUnifiedSizeByStyleId($productStyleId, "array");
$sizeOptions=$cache->get(function($productOptionDetails, $productStyleId){
	return Size_unification::getUnifiedSizeForOptions($productOptionDetails, $productStyleId, "array");	
}, array($productOptionDetails, $productStyleId), "sizeOptions");

// not a db call
$flattenSizeOptions = Size_unification::ifSizesUnified($sizeOptions);

//$allSizeOptions=Size_unification::getUnifiedSizeByStyleId($productStyleId, "array", true);
$allSizeOptions=$cache->get(function($allProductOptionDetails, $productStyleId){
	return Size_unification::getUnifiedSizeForOptions($allProductOptionDetails, $productStyleId, "array");
}, array($allProductOptionDetails, $productStyleId), "allSizeOptions");

$flattenSizeOptionsAll = Size_unification::ifSizesUnified($allSizeOptions);
$allSizeKeys=array_keys($allSizeOptions);

foreach ($inactiveProductOptionDetails as $row){
	$unavailabeOptions[$row['value']]=$row;
	if($flattenSizeOptionsAll){
		$unavailabeOptions[$row['value']]['unified']=$allSizeOptions[$row['value']];
	}
}

//$sizeOptionsTooltip=Size_unification::getUnifiedSizeByStyleId($productStyleId, "array", false, true, "tooltip");
$sizeOptionsTooltip=$cache->get(function($productOptionDetails, $productStyleId){
	return Size_unification::getUnifiedSizeForOptions($productOptionDetails, $productStyleId, "array", "tooltip");
}, array($productOptionDetails, $productStyleId), "sizeOptionsTooltip");


$sizeOptionSKUMapping = array();
$sizeOptionsTooltipSKUMapping = array();

foreach ($sizeOptions as $key=>$value) {
	foreach ($productOptionDetails as $options) {
		if($options['value'] == $key) {
			$sizeOptionSKUMapping[$options['sku_id']] = $value;
			$sizeOptionsTooltipSKUMapping[$options['sku_id']] = $sizeOptionsTooltip[$key];
		}
	}
}

$sizeOptionsTooltipHtmlArray=array();

if($flattenSizeOptions){
	foreach ($sizeOptionSKUMapping as $idx => $sizeopt){
		$sizeopt_breakdown=explode(",", $sizeOptionsTooltipSKUMapping[$idx]);
		$str="";
		$str="<table>";
		foreach($sizeopt_breakdown as $sizeopt_breakdown_idx=>$sizeopt_breakdown_val){
			$str .="<tr>";
			if((strpos($sizeopt_breakdown_val, "/") === false) && (strpos($sizeopt_breakdown_val, "-") === false)){
				$alpha_val=ereg_replace("[^A-Z]", "", $sizeopt_breakdown_val);
				$numeric_val=ereg_replace("[^0-9.]", "", $sizeopt_breakdown_val);
				if(!empty($alpha_val)){
					$str .= "<td>" . $alpha_val . "</td>";
				}
				if(!empty($numeric_val)){
					$str .= "<td>" . $numeric_val . "</td>";	
				}
			}
			else{
				$str .= "<td>" . $sizeopt_breakdown_val . "</td>";
			}
			$str .="</tr>";
		}
		$str .= "</table>";
		$sizeOptionsTooltipHtmlArray[$idx]=$str;
		$tooltipTop=count($sizeopt_breakdown) * 25 + 25;		
		$smarty->assign("tooltipTop",$tooltipTop);
	}
}

$sizeScale;
if($flattenSizeOptions)
	$sizeScale=Size_unification::getSizeScaleFromOption($allSizeKeys[0]);
$smarty->assign("sizeScale",$sizeScale);
$smarty->assign("unavailableOptions",$unavailabeOptions);

//Featuregates for sizechart and tooltip
$tooltipFlag=FeatureGateKeyValuePairs::getBoolean('PDPShowTooltip');
$PDPShowTooltip=$flattenSizeOptions && $tooltipFlag;
$renderTooltip=$PDPShowTooltip;	
$renderSizeChart=!$PDPShowTooltip;

//Most Popular tags
$widget = new Widget(WidgetMostPopular::$WIDGETNAME);
$widget->setDataToSmarty($smarty);

//retargeting tracking code
$retargetPixel = retargetingPixelOnType($productTypeId);
if(!empty($retargetPixel)){
    $smarty->assign("retargetPixel",$retargetPixel);
}    
$mobile = $XCART_SESSION_VARS['mobile'];



$smarty->assign("sizeOptionSKUMapping",$sizeOptionSKUMapping);

$smarty->assign("renderTooltip",$renderTooltip);
$smarty->assign("renderSizeChart",$renderSizeChart);
$smarty->assign("sizeSpecUsed",$sizeSpecUsed);
$smarty->assign("gender",$gender);
$smarty->assign("sizeOptionsMapping",$sizeOptionsMapping);
$smarty->assign("sizeOptions",$sizeOptions);
$smarty->assign("sizeOptionsTooltipHtmlArray",$sizeOptionsTooltipHtmlArray);
$smarty->assign("productOptions",$productOptionNameValues);
$smarty->assign("flattenSizeOptions",$flattenSizeOptions);
$smarty->assign("mobile",$mobile);
$smarty->assign("notifymefeatureon",FeatureGateKeyValuePairs::getBoolean(Notifications::$_CUSTOMER_INSTOCK_NOTIFYME.'.enable')==true);

$smarty->assign("sizeRepresentationImageUrl",$sizeRepresentationImageUrl);

$deliveryKnowMoreEnabled = FeatureGateKeyValuePairs::getBoolean('deliveryKnowMoreEnabled');
$deliveryKnowMoreEnabled = $deliveryKnowMoreEnabled && !$buynowdisabled && !$disableProductStyle; 
$smarty->assign("deliveryKnowMoreEnabled", $deliveryKnowMoreEnabled);

$deliveryKnowMoreMsg=WidgetKeyValuePairs::getWidgetValueForKey("deliveryKnowMoreMsg");
if($deliveryKnowMoreMsg != NULL){
	$smarty->assign("deliveryKnowMoreMsg", $deliveryKnowMoreMsg);
}
if(!is_null($tracker->pdpData)){
	$tracker->pdpData->setStyleId($productStyleId);
	$tracker->pdpData->setBrand($brand_name);
	$tracker->pdpData->setArticleType($articleType);
}
$tracker->fireRequest();



/*FOR AVAIL RECOMMENDATION*/
$avail_pdp_abtest= $_MABTestObject->getUserSegment("avail_pdp_abtest");
$smarty->assign("avail_pdp_abtest", $avail_pdp_abtest);
$avail_dynamic_param = "$gender,$masterCategory";
$smarty->assign("avail_dynamic_param",$avail_dynamic_param);
if(!empty($_GET['t_code'])){
$smarty->assign("t_code",filter_input(INPUT_GET, 't_code', FILTER_SANITIZE_STRING));
}

if(!empty($_GET['uq'])){
$smarty->assign("uq",filter_input(INPUT_GET, 'uq', FILTER_SANITIZE_STRING));
}
if(!empty($_GET['searchQuery'])){
$smarty->assign("uqs",filter_input(INPUT_GET, 'searchQuery', FILTER_SANITIZE_STRING));
}

$rcv_avail=array();
if(!empty($_COOKIE[$cookieprefix.'RCV_AVAIL'])){
	$rcv_avail=explode(',',filter_input(INPUT_COOKIE, $cookieprefix.'RCV_AVAIL', FILTER_SANITIZE_STRING));
}
array_unshift($rcv_avail,$productStyleId);
$rcv_avail=array_unique($rcv_avail);
$rcv_avail=array_slice($rcv_avail,0,5);
$rcv_avail_str=implode(',',$rcv_avail);
setMyntraCookie('RCV_AVAIL',$rcv_avail_str,time()+3600*24*365,'/',$cookiedomain);
$smarty->assign("rcv_avail_str",$rcv_avail_str);

$analyticsObj = AnalyticsBase::getInstance();
$analyticsObj->setContextualPageData(AnalyticsBase::PDP, $masterCategory, $catalogData["sub_category"], $articleType, $productStyleId);
$analyticsObj->setPDPEventVars($productStyleId);
$analyticsObj->setTemplateVars($smarty);

func_display("product_new_tpl/product_new.tpl", $smarty);
?>
