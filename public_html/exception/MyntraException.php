<?php

/**
 * Generic exception class.
 * 
 * @author Rohan.Railkar
 */
class MyntraException extends Exception
{
	/**
	 * Constructor.
	 * 
	 * @param String $message Error message.
	 * @param int $code Error code.
	 */
	public function __construct($message, $code = 0, $previous = NULL)
	{
	    parent::__construct($message, $code, $previous);
	}
	
	/**
	 * @see Exception::__toString()
	 */
	public function __toString()
	{
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }
}
