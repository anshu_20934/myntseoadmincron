


<?php

/*****************************************************************************\
+-----------------------------------------------------------------------------+
| X-Cart                                                                      |
| Copyright (c) 2001-2006 Ruslan R. Fazliev <rrf@rrf.ru>                      |
| All rights reserved.                                                        |
+-----------------------------------------------------------------------------+
| PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
| FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
| AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
|                                                                             |
| THIS  AGREEMENT  EXPRESSES  THE  TERMS  AND CONDITIONS ON WHICH YOU MAY USE |
| THIS SOFTWARE   PROGRAM   AND  ASSOCIATED  DOCUMENTATION   THAT  RUSLAN  R. |
| FAZLIEV (hereinafter  referred to as "THE AUTHOR") IS FURNISHING  OR MAKING |
| AVAILABLE TO YOU WITH  THIS  AGREEMENT  (COLLECTIVELY,  THE  "SOFTWARE").   |
| PLEASE   REVIEW   THE  TERMS  AND   CONDITIONS  OF  THIS  LICENSE AGREEMENT |
| CAREFULLY   BEFORE   INSTALLING   OR  USING  THE  SOFTWARE.  BY INSTALLING, |
| COPYING   OR   OTHERWISE   USING   THE   SOFTWARE,  YOU  AND  YOUR  COMPANY |
| (COLLECTIVELY,  "YOU")  ARE  ACCEPTING  AND AGREEING  TO  THE TERMS OF THIS |
| LICENSE   AGREEMENT.   IF  YOU    ARE  NOT  WILLING   TO  BE  BOUND BY THIS |
| AGREEMENT, DO  NOT INSTALL OR USE THE SOFTWARE.  VARIOUS   COPYRIGHTS   AND |
| OTHER   INTELLECTUAL   PROPERTY   RIGHTS    PROTECT   THE   SOFTWARE.  THIS |
| AGREEMENT IS A LICENSE AGREEMENT THAT GIVES  YOU  LIMITED  RIGHTS   TO  USE |
| THE  SOFTWARE   AND  NOT  AN  AGREEMENT  FOR SALE OR FOR  TRANSFER OF TITLE.|
| THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY GRANTED BY THIS AGREEMENT.      |
|                                                                             |
| The Initial Developer of the Original Code is Ruslan R. Fazliev             |
| Portions created by Ruslan R. Fazliev are Copyright (C) 2001-2006           |
| Ruslan R. Fazliev. All Rights Reserved.                                     |
+-----------------------------------------------------------------------------+
\*****************************************************************************/
#
# $Id: auth.php,v 1.30.2.1 2006/06/02 13:54:18 max Exp $
#

use abtest\MABTest;
use web\utils\MobileConstants;
use utils\RandomStringGenerator;
if (defined('AUTH_API_INIT')) {
       return;
}
if (defined('ADMIN_INIT')) {
	return;
}
if (defined('PROVIDER_INIT')) {
        return;
}

if (defined('AUTH_SOLR_INIT')) {
        return;
}
$authStartTime = microtime(true);
define('AREA_TYPE', 'C');
include_once (dirname(__FILE__)). "/top.inc.php";
include_once ($xcart_dir. "/Profiler/Profiler.php");
@include ($xcart_dir."/sanitizept.inc.php");
include_once("$xcart_dir/include/class/widget/class.widget.keyvaluepair.php");
include_once "$xcart_dir/include/class/class.feature_gate.keyvaluepair.php";
include_once "$xcart_dir/modules/coupon/MRPConstants.php";
include_once "$xcart_dir/include/func/func.loginhelper.php";
include_once "$xcart_dir/include/func/func.utilities.php";
if (!defined('DIR_CUSTOMER')) die("ERROR: Can not initiate application! Please check configuration.");
//var_dump(get_user_ip());exit;
#logger initialization moved to separate location
include_once($xcart_dir.'/logger.php');

include_once $xcart_dir."/init.php";

$current_area="C";
$isXHR = isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest';
$smarty->assign('isXHR', $isXHR);
$smarty->assign("enableMostPopular" , FeatureGateKeyValuePairs::getBoolean("enableMostPopular"));
$smarty->assign("enableStoreinStore" , FeatureGateKeyValuePairs::getBoolean("enableStoreinStore"));

require_once ($xcart_dir.'/include/func/func.files.php');

global $REMEMBER_USER;

/*----sanitizing 'xid' from _GET, _POST and _COOKIE arrays -----*/

$sanitizeXid = FeatureGateKeyValuePairs::getBoolean('sanitizeXid');
	
if($sanitizeXid){
    $envXid = $cookieprefix.'xid';
    if(!empty($_GET[$envXid]) && !preg_match('!^[0-9a-zA-Z\-]+$!S', $_GET[$envXid])) 
        $_GET[$envXid] = "";
    if(!empty($_POST[$envXid]) && !preg_match('!^[0-9a-zA-Z\-]+$!S', $_POST[$envXid])) 
        $_POST[$envXid] = "";
    if(!empty($_COOKIE[$envXid]) && !preg_match('!^[0-9a-zA-Z\-]+$!S', $_COOKIE[$envXid])) 
        $_COOKIE[$envXid] = "";
    if(!empty($_SERVER[$envXid]) && !preg_match('!^[0-9a-zA-Z\-]+$!S', $_SERVER[$envXid])) 
        $_SERVER[$envXid] = "";
}

/*--------------------------------------------------------------*/

$expiry = intval(FeatureGateKeyValuePairs::getFeatureGateValueForKey('telesales.loginExpiry'));
if ($expiry == 0)
     $expiry= 30;
//var_dump($ipValid);exit;
global $telesales;
global $telesalesUser;
global $mymyntra;
$mymyntra= false;
$smarty->assign("mymyntra",$mymyntra);
$telesales = false;
$telesalesUser  = "unknown";
global $userToken;
$userToken = $XCART_SESSION_VARS['USER_TOKEN'];
if (isset($XCART_SESSION_VARS['identifiers']['A']['login'])){
     $telesalesUser  = $XCART_SESSION_VARS['identifiers']['A']['login'];
     x_session_register("telesalesAdminName", $telesalesUser);
}
/*else{
    $telesalesUser  = $XCART_SESSION_VARS["telesalesAdminName"];
}*/
 $ipValid = true;
if (isset($XCART_SESSION_VARS['telesalesAgent'])&&isset($XCART_SESSION_VARS['identifiers']['C']['login'])){
    $telesales = true;
    $teleSalesIpFilterOn= FeatureGateKeyValuePairs::getBoolean('telesales.ipFilterOn', "false");
    if ($teleSalesIpFilterOn){
        $ipValid= false;
        $validSpoofLoginIPs = explode(",", FeatureGateKeyValuePairs::getFeatureGateValueForKey('telesales.ipList'));
        $clientIP= get_user_ip();
        foreach ($validSpoofLoginIPs as $ip){
             if (trim($ip)===trim($clientIP))
                $ipValid=true;
        }    
    }
}
//$ipValid = true;
if (isset($XCART_SESSION_VARS['telesalesAgent'])&& ($skin == "skinmobile"||!$ipValid)){
    echo "access not allowed"; exit;
}
$forceLogout = false;
$usern = "";
$passwd = "";
if (isset($_GET["usern"]))
    $usern = $_GET["usern"];
if (isset($_GET["passwd"]))
    $passwd = $_GET["passwd"];
if (isset($_GET["forcelogout"]))
    $forceLogout=true;
//var_dump(($telesalesUser=="unknown"));
//var_dump((isset($XCART_SESSION_VARS['telesalesAgent'])&&($telesalesUser=="unknown")));exit;
if (x_session_is_registered("forcelogout")||(isset($XCART_SESSION_VARS['telesalesAgent'])&&((time()-$XCART_SESSION_VARS['telesalesAgent'])>($expiry * 60)))||($forceLogout)||(isset($XCART_SESSION_VARS['telesalesAgent'])&&($telesalesUser=="unknown"))){
//if (1!=1){
    //echo "here";exit;
    // destroy all the session variable on logout
    //clearSessionVarsOnLogout();
        if (x_session_is_registered("forcelogout"))
            x_session_unregister("forcelogout");
	if (x_session_is_registered("chosenProductConfiguration"))
            x_session_unregister("chosenProductConfiguration");
 	if (x_session_is_registered("express_buy_enabled"))
            x_session_unregister("express_buy_enabled");
 	if(x_session_is_registered("sxid"))
		x_session_unregister("sxid");
	
	if(x_session_is_registered("selected_address"))
		x_session_unregister("selected_address");

	if (x_session_is_registered("masterCustomizationArray"))
		x_session_unregister("masterCustomizationArray");

	if (x_session_is_registered("masterCustomizationArrayD"))
		x_session_unregister("masterCustomizationArrayD");

    	if (x_session_is_registered("productsInCart"))
		x_session_unregister("productsInCart");

	if (x_session_is_registered("coupondiscount"))
		x_session_unregister("coupondiscount");

	if (x_session_is_registered("couponCode"))
		x_session_unregister("couponCode");

    if (x_session_is_registered("cashCouponCode"))
            x_session_unregister("cashCouponCode");

    if (x_session_is_registered("cashdiscount"))
        x_session_unregister("cashdiscount");

	if (x_session_is_registered("offers"))
		x_session_unregister("offers");

	if (x_session_is_registered("totaloffers"))
		x_session_unregister("totaloffers");

	if (x_session_is_registered("totaldiscountonoffer"))
		x_session_unregister("totaldiscountonoffer");

	if (x_session_is_registered("productids"))
		x_session_unregister("productids");	
		
	if (x_session_is_registered("grandTotal"))
		x_session_unregister("grandTotal");	
		
	if (x_session_is_registered("amountAfterDiscount"))
		x_session_unregister("amountAfterDiscount");	
     
	if (x_session_is_registered("vtr"))
		x_session_unregister("vtr");	
     
     if (x_session_is_registered("userfirstname"))
        x_session_unregister("userfirstname");    
     if (x_session_is_registered("referer_url"))
        x_session_unregister("referer_url");    
	
     if (x_session_is_registered("mobile"))
        x_session_unregister("mobile");  
     if (x_session_is_registered("mcartid"))
        x_session_unregister("mcartid");   
     if (x_session_is_registered("mcartidgiftcard"))
    	 x_session_unregister("mcartidgiftcard");
     if (x_session_is_registered("mcartidsaved"))
    	 x_session_unregister("mcartidsaved");
     if (x_session_is_registered("telesalesAgent"))
    	 x_session_unregister("telesalesAgent");
     $XCART_SESSION_VARS["totalQuantity"]=0;
     $XCART_SESSION_VARS['firstname'] ="" ;
     $XCART_SESSION_VARS['identifiers']['C']=array();
}
x_session_register("logout_user");
if (isset($_GET['passwd'])&&isset($_GET['usern'])&&isset($_GET['forcelogout'])){
        //echo "here";exit;
        func_header_location( \HostConfig::$baseUrl."/mymyntra.php?usern=$usern&passwd=$passwd",false);
}
if (x_session_is_registered("oldCheckout")){
    x_session_unregister("oldCheckout");
    func_header_location( \HostConfig::$baseUrl."setTestCookie.php?test=checkout&variant=control",false);
}
if (isset($_GET["ref"])){
    $mrp_referer = mysql_real_escape_string($_GET['ref']);
}
if(!empty($mrp_referer)){
    $smarty->assign("mrp_referer", $mrp_referer);
}
if(!isset($_POST['affiliate']) && !isset($_POST['allowCrossDomainRequest']) && !isset($_GET['allowCrossDomainRequest']))
	require $xcart_dir."/include/nocookie_warning.php";
/*if (!defined('HTTPS_CHECK_SKIP')) {
	@include $xcart_dir.DIR_CUSTOMER."/https.php";
}*/

#
# Browser have disabled/enabled javasript switching
#

/*destroy session if telesales timeout over*/
/**/
x_session_register("js_enabled", "Y");

if (!isset($js_enabled)) $js_enabled="Y";

if (isset($HTTP_GET_VARS["js"])) {
	if ($HTTP_GET_VARS["js"]=="y") {
		$js_enabled = "Y";
		$config['Adaptives']['isJS'] = "Y";
		$adaptives['isJS'] = "Y";
	}
	elseif ($HTTP_GET_VARS["js"]=="n") {
		$js_enabled = "";
	}
}

if ($js_enabled == "Y") {
	$qry_string = ereg_replace("(&*)js=y", "", $QUERY_STRING);
	$js_update_link = $PHP_SELF."?".($qry_string?"$qry_string&":"")."js=n";
}
else {
	$qry_string = ereg_replace("(&*)js=n", "", $QUERY_STRING);
	$js_update_link = $PHP_SELF."?".($qry_string?"$qry_string&":"")."js=y";
}
$GTMRestricted = false;
if ($skin == MobileConstants::MOBILE_SKIN){
    $UACombinedString = FeatureGateKeyValuePairs::getFeatureGateValueForKey('GTMBlockedFor');
    $GTMRestricted = web\utils\DeviceDetector::getInstance()->isGTMRestricted(null,$UACombinedString);
}
global $googleBot;
$BotUACombinedString = FeatureGateKeyValuePairs::getFeatureGateValueForKey('webBots');
$googleBot = web\utils\DeviceDetector::getInstance()->isWebBot(null,$BotUACombinedString);
$smarty->assign("GTMRestricted",$GTMRestricted);
$smarty->assign("usern",$usern);
$smarty->assign("passwd",$passwd);
$smarty->assign("js_update_link", $js_update_link);
$smarty->assign("js_enabled", $js_enabled);
$smarty->assign("telesalesUser",$telesalesUser);
$smarty->assign("telesales",$telesales);
$cat = intval(@$cat);
$page = intval(@$page);

$nocache = $_GET['nocache'];
if (!defined('MCLASS_LOADER')) {
	define('MCLASS_LOADER', true);
	include_once ("$xcart_dir/include/class/MClassLoader.php");
	MClassLoader::addToClassPath("$xcart_dir/include/class/");
	MClassLoader::addToClassPath("$xcart_dir/include/dao/class/");
}
if (!defined('QUICK_START')) {
    include_once "$xcart_dir/seosem.php";
}
include_once "$xcart_dir/include/class/abtest/MABTest.php";
//captcha isn't working with phpab framework. so NO-TOP_NAV shlou beld defined in place like that to avoid conflict
//$megamenutestsegment= $_MABTestObject->getUserSegment('megamenuabtest');
//$smarty->assign("megamenutestsegment",$megamenutestsegment);

// A/B test for New Header
$abHeaderUi = $_MABTestObject->getUserSegment('newHeaderUItest');
$smarty->assign('abHeaderUi', $abHeaderUi);

/*******************************************************************************/
//Nodejs Widgetified Home Page - A B Test // Now used for node search page
/*******************************************************************************/

$abNC = $_MABTestObject->getUserSegment('nck') === 'test';
$abMM = $_MABTestObject->getUserSegment('mymo') === 'test';

$isMobile = web\utils\DeviceDetector::getInstance()->isMobile();
$nck = getMyntraCookie("nck");
$mm = getMyntraCookie("mymo");
$pinjshTimeOut = time() + (15 * 60);

if ($isMobile) {
    if ($abNC)
        setMyntraCookie("nck", 1, $pinjshTimeOut, "/", $cookiedomain, false, true);
    else if ($nck)
        deleteMyntraCookie('nck');

    if ($abMM)
        setMyntraCookie("mymo", 1, $pinjshTimeOut, "/", $cookiedomain, false, true);
    else if ($mm)
        deleteMyntraCookie('mymo');
}
else {
    if ($abNC)
        setMyntraCookie("nck", 1, $pinjshTimeOut, "/", $cookiedomain, false, true);
    else if ($nck)
        deleteMyntraCookie('nck');
    if ($mm)
        deleteMyntraCookie('mymo');
}

/*
$nodejs = $_MABTestObject->getUserSegment('njswHP');

$pinjsh = getMyntraCookie("pinjsh");
$pinjshTimeOut = time() + (15 * 60);
$shouldRedirect = FeatureGateKeyValuePairs::getBoolean('nodejs.shouldRedirect', false);
$isMobile = web\utils\DeviceDetector::getInstance()->isMobile();

if($nodejs!='control' && !$isMobile){
    //if the request is not a mobile one and the cookie is not yet set and the segmentation is 'test'
    //then set the cookie to go to nodejs
    //iS nODE js hOME pAGE - jumbling all the small cased letters (injshp) => (pinjsh)
    setMyntraCookie("pinjsh",1,$pinjshTimeOut,"/",$cookiedomain,false,true);

    if($shouldRedirect=="true"){
        header("Location: http://" . $_SERVER[HTTP_HOST].$_SERVER[REQUEST_URI]);
        exit;
    }
}else {
    if(($nodejs=='control' || $isMobile) && isset($pinjsh)) {
        //iS nODE js hOME pAGE - jumbling all the small cased letters (injshp) => (pinjsh)
        deleteMyntraCookie("pinjsh");
    }
}
*/


/*******************************************************************************/

$smarty->assign('cookiedomain', $cookiedomain);
$smarty->assign('cookieprefix', $cookieprefix);

include_once $xcart_dir.DIR_CUSTOMER."/referer.php";
if (!defined('QUICK_START')) {
    include_once $xcart_dir."/include/check_useraccount.php";
}

$loyaltyEnabled = FeatureGateKeyValuePairs::getBoolean('myntra.loyalty.enabled', false);
if($loyaltyEnabled){
    $loyaltyUsers = WidgetKeyValuePairs::getWidgetValueForKey("loyalty.users");
    if(!empty($loyaltyUsers) && strtolower($loyaltyUsers) == 'all'){
        $loyaltyEnabled = true;
    }elseif(!empty($loyaltyUsers)){
        $loyaltyEnabled = false;
        $enabledUsers = explode(",", $loyaltyUsers);
        $loyaltyEnabled = in_array($login, $enabledUsers) || in_array(substr($login, strpos($login,"@")), $enabledUsers);
    }else{
        $loyaltyEnabled = false;
    }
}

$smarty->assign('loyaltyEnabled', $loyaltyEnabled);
$smarty->assign('defaultMyntcreditTab',FeatureGateKeyValuePairs::getFeatureGateValueForKey('mymyntra.myntcredit.defaultTab'));

include_once($xcart_dir."/tracking/leadTrackingSession.php");
include $xcart_dir.DIR_CUSTOMER."/minicart.php";

$smarty->assign("printable", $printable);
$smarty->assign("logout_user", $logout_user);
$userAgent  = $_SERVER['HTTP_USER_AGENT'];
if(strpos($userAgent,"MSIE") > 0)
{
	$smarty->assign("ie","true");

}

$system_free_shipping_amount = WidgetKeyValuePairs::getWidgetValueForKey('shipping.charges.cartlimit');
$smarty->assign("free_shipping_amount", $system_free_shipping_amount);
$system_shipping_rate = WidgetKeyValuePairs::getWidgetValueForKey('shipping.charges.amount');
$system_free_shipping_firstorder = FeatureGateKeyValuePairs::getBoolean('shipping.charges.free.firstorder');
$smarty->assign("free_shipping_firstorder", $system_free_shipping_firstorder);
$userfirstname = $XCART_SESSION_VARS['userfirstname'];
//echo $userfirstname;
if (isset($XCART_SESSION_VARS['identifiers'])){
    
    if (isset($XCART_SESSION_VARS['identifiers']['C'])){
        if (isset($XCART_SESSION_VARS['identifiers']['C']['firstname'])){
            
            $userfirstname = $XCART_SESSION_VARS['identifiers']['C']['firstname'];
        }
    }
}



//echo $userfirstname;exit;

$smarty->assign("firstname",$userfirstname);
##################Base URL for urlrewriting####################
$smarty->assign("baseurl",$baseurl);
$smarty->assign("css_path",$css_path);
$smarty->assign("js_path",$js_path);
$smarty->assign("secure_css_path",$secure_css_path);
$smarty->assign("secure_js_path",$secure_js_path);
$smarty->assign("secure_cdn_base",$secure_cdn_base);
$smarty->assign("cdn_base",$cdn_base);
$smarty->assign("ga_acc",$ga_acc);
$smarty->assign("combine_css_path",$combine_css_path);
$smarty->assign("combine_js_path",$combine_js_path);
$smarty->assign("analytics_account",AnalyticsConfig::$analyticsAccount);
$smarty->assign("nmServerName", \HostConfig::$serverName);
######################################################
##################Cash on delivery charges####################
$smarty->assign("cod_charges",$cod_charges);
######################################################



$userType = $XCART_SESSION_VARS['login_type'];
$posaccounttype = $XCART_SESSION_VARS['identifiers'][$userType]['account_type'];

//check if some user had logged into this system, used for selecting login/signup
if(!empty($XCART_SESSION_VARS['remember_login']))
    $smarty->assign('showLogin', true);
else
    $smarty->assign('showLogin', false);

$nologin = false;
if(!empty($_GET['show'])&&($_GET['show']==="nologin"))
	                $nologin = true;
$smarty->assign("nologin",$nologin);
//AVAIL - MD5 USERID
$user_first_login = $XCART_SESSION_VARS['identifiers'][$userType]['first_login_date'];
$AVAIL_USER_ID = isGuestCheckout() ? $XCART_SESSION_VARS['glogin']:$XCART_SESSION_VARS['identifiers'][$userType]['login'];
$smarty->assign('AVAIL_USER_ID', $AVAIL_USER_ID);

if (isset($AVAIL_USER_ID) && !empty($AVAIL_USER_ID)) {
    $AVAIL_USER_ID_MD5 = md5($AVAIL_USER_ID);
    $smarty->assign('AVAIL_USER_ID_MD5', $AVAIL_USER_ID_MD5);    // user id to be used by rich relevance RR 
}

$rrSessionId = getMyntraCookie("rr_sid");

if(empty($rrSessionId)){
	include_once($xcart_dir."/include/class/utils/RandomStringGenerator.php"); 
	$rrSessionId = RandomStringGenerator::generateRandomString(10);
	setMyntraCookie("rr_sid",$rrSessionId,time()+2592000 /* 30 days expiry */);
}


$shopMasterId = 0;
//AB test for nudge
$phoneNumRequired = $_MABTestObject->getUserSegment('regV2PhoneRequired');
$smarty->assign("showNudge",true);
if (x_session_is_registered("showNudge"))
	$smarty->assign("showNudge",false);
$nudge = $_MABTestObject->getUserSegment('nudge');
$nudgeShow = $_MABTestObject->getUserSegment("nudgeShow");
$regV2 = $_MABTestObject->getUserSegment('regV2');
$smarty->assign("loginSignupV2ABTest",$regV2);
$smarty->assign("phoneNumNotRequired",$phoneNumRequired);
$splashDelay =  WidgetKeyValuePairs::getWidgetValueForKey("splashDelay");
$smarty->assign("splashDelay",$splashDelay);

$returningCustomer = $XCART_SESSION_VARS['returningCustomer'];
$returningCustomerCookie = getMyntraCookie("isBuyerCookie");
if ($login) {
    if (!x_session_is_registered("returningCustomer") ||
        !x_session_is_registered("returningCustomerTTL") ||
        $XCART_SESSION_VARS["returningCustomerTTL"] < time()) {
        x_session_unregister("returningCustomer");
        x_session_unregister("returningCustomerTTL");

        $returningCustomer = func_query_first_cell("select count(1) as to_be_processed from xcart_orders xo 
                                          left join mk_old_returns_tracking rto on xo.orderid = rto.orderid 
                                          where xo.status in ('PK','SH','DL','C','WP','OH','Q','RFR') 
                          and xo.login = '$login' and rto.orderid is null", true) >= 1 ? 1 : 0;
        x_session_register("returningCustomer", $returningCustomer);
        $ttl = intval(FeatureGateKeyValuePairs::getFeatureGateValueForKey('returningCustomer.TTL'));
        x_session_register("returningCustomerTTL", time() + $ttl);

    }
    setMyntraCookie("isBuyerCookie",$returningCustomer,time()+3600*24*365,'/',$cookiedomain);
    $returningCustomerCookie =  $returningCustomer;
}
else{
    if(is_null($returningCustomerCookie)){
        $emailId = getMyntraCookie($REMEMBER_USER, false, '', true);
        if($emailId) {
            $returningCustomerCookie = func_query_first_cell("select count(1) as to_be_processed from xcart_orders xo
                                          left join mk_old_returns_tracking rto on xo.orderid = rto.orderid
                                          where xo.status in ('PK','SH','DL','C','WP','OH','Q','RFR')
                          and xo.login = '$emailId' and rto.orderid is null", true) >= 1 ? 1 : 0;
        } else {
            $returningCustomerCookie = 0;
        }
        setMyntraCookie("isBuyerCookie",$returningCustomer,time()+3600*24*365,'/',$cookiedomain);
    }
}
$smarty->assign('returningCustomer', $returningCustomer);
$smarty->assign('returningCustomerCookie', $returningCustomerCookie);




if (!$login){
	$smarty->assign("nudgeABTest",$nudge);
	$smarty->assign("nudgeShowABTest",$nudgeShow);
    $nudgeMessage= json_decode(WidgetKeyValuePairs::getWidgetValueForKey('nudgePoint'));
    //    $nudgeMessage= array("click to get","Get 20% off");
        $smarty->assign("nudgeMessage",$nudgeMessage);
}
else{
    $smarty->assign("nudgeABTest","LoggedIn");
}
$showNewSignUpConfirmation=false;
$showFbCoupons = false;

$confMessages=json_decode(WidgetKeyValuePairs::getWidgetValueForKey('confMessages'),true,3);
$smarty->assign("confMessages",$confMessages);

$SignUpSplashPageLoad = FeatureGateKeyValuePairs::getFeatureGateValueForKey('SignUpSplashPageLoad');
$smarty->assign("SignUpSplashPageLoad",$SignUpSplashPageLoad);

$signupOffersTextDesktop = WidgetKeyValuePairs::getWidgetValueForKey('Signup_Offers_Text_Desktop');
$smarty->assign("signupOffersTextDesktop",$signupOffersTextDesktop);
$signupOffersTextMobile = WidgetKeyValuePairs::getWidgetValueForKey('Signup_Offers_Text_Mobile');
$smarty->assign("signupOffersTextMobile",$signupOffersTextMobile);

if (getMyntraCookie("_mklogin")&& strpos(getMyntraCookie("_mklogin") , "*signup")!= false &&  strpos(getMyntraCookie("_mklogin") , "success-signinfrmsignup")== false && $regV2=="test"&& (strpos($_SERVER["PHP_SELF"],"cart.php")==false &&strpos($_SERVER["PHP_SELF"],"mkmycart.php")==false && strpos($_SERVER["PHP_SELF"],"checkout.php")==false&& strpos($_SERVER["PHP_SELF"],"payment")==false && strpos($_SERVER["PHP_SELF"],"address")==false)) {
    $showNewSignUpConfirmation=true;
}

if (getMyntraCookie("_mklogin")&& strpos(getMyntraCookie("_mklogin") , "success_signup")!= false && strpos(getMyntraCookie("_mklogin") , "fb_connect")!= false && $regV2=="test"&& (strpos($_SERVER["PHP_SELF"],"cart.php")==false&& strpos($_SERVER["PHP_SELF"],"mkmycart.php")==false &&strpos($_SERVER["PHP_SELF"],"checkout.php")==false && strpos($_SERVER["PHP_SELF"],"payment")==false && strpos($_SERVER["PHP_SELF"],"address")==false ) ) {
    $showNewSignUpConfirmation=true;
    $showFbCoupons = true;
}

if (getMyntraCookie("ru")){
	$smarty->assign("showRegV2Login",true);
}
else{
	 $smarty->assign("showRegV2Login",false);
}	
$smarty->assign("showNewSignUpConfirmation",$showNewSignUpConfirmation);
$loginPoints=json_decode(WidgetKeyValuePairs::getWidgetValueForKey('loginPoints'));
$signUpPointsJson=json_decode(WidgetKeyValuePairs::getWidgetValueForKey('signupPoints'),true,5);
$signUpPoints_checkoutJson=json_decode(WidgetKeyValuePairs::getWidgetValueForKey('signupPoints2'),true,5);
$imageFlag = $signUpPointsJson["imageFlag"];
$imageFlag_checkout = $signUpPoints_checkoutJson["imageFlag"];
$signUpPoints = $signUpPointsJson["content"];
$signUpPoints_checkout = $signUpPoints_checkoutJson["content"];
$smarty->assign("imageFlag",$imageFlag);
$smarty->assign("loginPoints",$loginPoints);
$smarty->assign("signUpPoints",$signUpPoints);
if ($login ||  $regV2!="test"){
    $showLogin=false;
    $smarty->assign("showLogin",$showLogin);
}
else{
    //$loginPoints = array("Apply coupons & cashback","Place orders easily","Track past orders","Manage WishList");
    //$signUpPoints = array();
    //$signUpPoints[0]=array("heading"=>"GET 20% OFF","subtext"=>"on your first purchase");
    //$signUpPoints[1]=array("heading"=>"GET 20% OFF","subtext"=>"Save and share products you like");    
    $showLogin = true;
    $smarty->assign("showLogin",$showLogin);

}

//get portal node header/footer

include_once "$xcart_dir/include/func.node.import.php";
$nodeHeaderFooter = json_decode(PortalNodeInterface::getCachedNodeHeaderFooter($login));

// Hack to replace cart and wishlist count in headerhtml coming from node
$headerHtml = $nodeHeaderFooter->headerHtml;
$countPlaceholder = '<div class="badge"></div>';

$flyoutCnt = substr_count($headerHtml, $countPlaceholder);
$pos = strpos($headerHtml, $countPlaceholder);  
if($flyoutCnt > 2){
    $pos = strpos($headerHtml, $countPlaceholder, $pos+1);
}

$wishlistPos = 0;
if($pos !== false){
    $wishlistPos = $pos;
    if($XCART_SESSION_VARS['CART:savedItemCount'] > 0){
        $headerHtml = substr_replace($headerHtml, '<div class="badge">'.$XCART_SESSION_VARS['CART:savedItemCount'].'</div>', $pos, strlen($countPlaceholder));
    }
}
$pos = strpos($headerHtml, $countPlaceholder, $wishlistPos + 1);
if($pos !== false){
    if($XCART_SESSION_VARS['CART:totalQuantity'] > 0){
        $headerHtml = substr_replace($headerHtml, '<div class="badge">'.$XCART_SESSION_VARS['CART:totalQuantity'].'</div>', $pos, strlen($countPlaceholder));
    }
}
$smarty->assign("nodeheader",$headerHtml);
$smarty->assign("nodefooter",$nodeHeaderFooter->footerHtml);
$smarty->assign("nodecss",$nodeHeaderFooter->css);
if (!empty($nodeHeaderFooter->scripts)) {
    $smarty->assign("nodescript", $nodeHeaderFooter->scripts);
} else {
    $smarty->assign("nodescript", $nodeHeaderFooter->script);        
}
$userData = array(
            'USER_TOKEN'=>$XCART_SESSION_VARS['USER_TOKEN'],
            'login'=>$XCART_SESSION_VARS['login'],
            'userfirstname'=>$XCART_SESSION_VARS['userfirstname'],
            'userlastname'=>$XCART_SESSION_VARS['userlastname'],
            'fb_uid'=>$XCART_SESSION_VARS['fb_uid'],
            'mobile'=>$XCART_SESSION_VARS['mobile'],
            'gender'=>$XCART_SESSION_VARS['gender'],
            'DOB'=>$XCART_SESSION_VARS['DOB'],
            'CART:savedItemCount'=>$XCART_SESSION_VARS['CART:savedItemCount'],
            'CART:totalQuantity'=>$XCART_SESSION_VARS['CART:totalQuantity'],
            'CART:totalAmountCart'=>$XCART_SESSION_VARS[''],
            'mrpData'=>$XCART_SESSION_VARS['mrpData'],
            'loyaltyPointsHeaderDetails'=>$XCART_SESSION_VARS['loyaltyPointsHeaderDetails'],
            'returningCustomer'=>$XCART_SESSION_VARS['returningCustomer'],
            'isRegistered'=>getMyntraCookie("ru"),
            'loginEnabled'=>MABTest::getInstance()->getUserSegment("loginpopup") == 'splash' ? true : false
    );
$smarty->assign("userData",json_encode($userData));
// for login
$features = array(
    'showLoginCaptcha' => FeatureGateKeyValuePairs::getFeatureGateValueForKey('showLoginCaptcha'),
    'SignUpSplashPageLoad' => FeatureGateKeyValuePairs::getFeatureGateValueForKey('SignUpSplashPageLoad'),
    'smsPromoEnabled' => FeatureGateKeyValuePairs::getFeatureGateValueForKey('smsPromoEnabled')
);
$smarty->assign('features', json_encode($features));
$kvpairs = array(
    'Signup_Offers_Text_Desktop' => WidgetKeyValuePairs::getWidgetValueForKey('Signup_Offers_Text_Desktop'),
    'confMessages' => WidgetKeyValuePairs::getWidgetValueForKey('confMessages'),
    'loginPoints' => WidgetKeyValuePairs::getWidgetValueForKey('loginPoints'),
    'signupPoints' => WidgetKeyValuePairs::getWidgetValueForKey('signupPoints'),
    'signupPoints2' => WidgetKeyValuePairs::getWidgetValueForKey('signupPoints2'),
    'smsPromoText' => WidgetKeyValuePairs::getWidgetValueForKey('smsPromoText'),
    'smsPromoDesc' => WidgetKeyValuePairs::getWidgetValueForKey('smsPromoDesc')
);
$smarty->assign('kvpairs', json_encode($kvpairs));








// A/B test for megamenu
$menu_version = $_MABTestObject->getUserSegment('megamenu');
$smarty->assign('menu_version', $menu_version);
$isMegaMenuEnabled = FeatureGateKeyValuePairs::getBoolean('megamenu.enabled', true);
$smarty->assign('isMegaMenuEnabled', $isMegaMenuEnabled);

//Kuliza Code Start
$isKulizaEchoEnabled = FeatureGateKeyValuePairs::getBoolean('kulizaecho.enabled', true);
$smarty->assign('isKulizaEchoEnabled', $isKulizaEchoEnabled);
$socialPermissionCategoryExcludeText = WidgetKeyValuePairs::getWidgetValueForKey("socialPermissionCategoryExcludeText");
$smarty->assign('socialPermissionCategoryExcludeText', $socialPermissionCategoryExcludeText);
$smarty->assign('kulizaEchoBaseUrl', HostConfig::$kulizaEchoBaseUrl);
$smarty->assign('kulizaClientDomain', HostConfig::$kulizaClientDomain);
$smarty->assign('kulizaClientBaseUrl', HostConfig::$kulizaClientBaseUrl);
//Kuliza Code End

include_once($xcart_dir."/navigation.php");
$smarty->assign("totalQuantity", $XCART_SESSION_VARS['totalQuantity'] );

$searchBoxText=WidgetKeyValuePairs::getWidgetValueForKey("searchBoxText");
if($searchBoxText != NULL){
	$smarty->assign("searchBoxText", trim($searchBoxText));
}

// Auto Suggest FeatureGate
$enableAutoSuggest = FeatureGateKeyValuePairs::getBoolean('autosuggest.enabled');
$smarty->assign('enableAutoSuggest', $enableAutoSuggest);


$customerSupportCall = WidgetKeyValuePairs::getWidgetValueForKey('customerSupportCall');
if($customerSupportCall != NULL) {
	$smarty->assign("customerSupportCall", $customerSupportCall);
	$smarty->assign("csnumber", $customerSupportCall );
}

$contactUsLink = WidgetKeyValuePairs::getWidgetValueForKey('contactUsLink');

if($contactUsLink != NULL) {
	$smarty->assign("contactUsLink", $contactUsLink);
}

$myOrdersLink = WidgetKeyValuePairs::getWidgetValueForKey('myOrdersLink');

if($myOrdersLink != NULL) {
	$smarty->assign("myOrdersLink", $myOrdersLink);
}

$customerSupportTime = WidgetKeyValuePairs::getWidgetValueForKey('customerSupportTime');
if($customerSupportTime != NULL) {
	$smarty->assign("customerSupportTime", $customerSupportTime);
}

$topCpsMessage = WidgetKeyValuePairs::getWidgetValueForKey('topCpsMessage');
if($topCpsMessage != NULL) {
	$smarty->assign("topCpsMessage", $topCpsMessage);
}

$newUiTopCpsMessage = WidgetKeyValuePairs::getWidgetValueForKey('newui.topCpsMessage');
if($newUiTopCpsMessage != NULL) {
	$newUiTopCpsMessage=str_replace("system_free_shipping_amount", $system_free_shipping_amount, $newUiTopCpsMessage);
	$smarty->assign("newUiTopCpsMessage", $newUiTopCpsMessage);
}

$totalNumStyles = WidgetKeyValuePairs::getWidgetValueForKey('totalNumStyles');
if(isset($totalNumStyles)) {
	$smarty->assign("totalNumStyles", $totalNumStyles);
}

$totalNumBrands = WidgetKeyValuePairs:: getWidgetValueForKey('totalNumBrands');
if(isset($totalNumBrands)) {
	$smarty->assign("totalNumBrands", $totalNumBrands);
}

$promoheaderenabled = WidgetKeyValuePairs::getWidgetValueForKey('promoheader.enabled');
if($promoheaderenabled == "true") {
	$smarty->assign("promoHeaderEnabled", $promoheaderenabled);
	$promoHeaderText = WidgetKeyValuePairs::getWidgetValueForKey('promoheader.text');
	$smarty->assign("promoHeaderText", $promoHeaderText);
    $promoHeaderLoggedinText = WidgetKeyValuePairs::getWidgetValueForKey('promoheader.loggedintext');
	$smarty->assign("promoHeaderLoggedinText", $promoHeaderLoggedinText);
}

$returnenabled = WidgetKeyValuePairs::getBoolean('returnenabled', true);
$smarty->assign("return_enabled", $returnenabled);
$exchanges_enabled = FeatureGateKeyValuePairs::getBoolean('exchanges_enabled');
$smarty->assign("exchanges_enabled", $exchanges_enabled);

/** Setting Login and Signup Text */
$signupCouponsMsg = (string) WidgetKeyValuePairs::getWidgetValueForKey("signupCouponsMsg");
$smarty->assign("signupCouponsMsg",$signupCouponsMsg);

$signupAmtMsg = (string) WidgetKeyValuePairs::getWidgetValueForKey("signupAmt");
$smarty->assign("signupAmtMsg",$signupAmtMsg);

$signupAmtMsgMobile = (string) WidgetKeyValuePairs::getWidgetValueForKey("signupAmtMsgMobile");
$smarty->assign("signupAmtMsgMobile",$signupAmtMsgMobile);

$signupAmtFBMsg = (string) WidgetKeyValuePairs::getWidgetValueForKey("signupAmtFB");
$smarty->assign("signupAmtFBMsg",$signupAmtFBMsg);

$loginMsg1 = (string) WidgetKeyValuePairs::getWidgetValueForKey('loginMsg1');
$smarty->assign("loginMsg1",$loginMsg1);

$loginMsg2 = (string) WidgetKeyValuePairs::getWidgetValueForKey('loginMsg2');
$smarty->assign("loginMsg2",$loginMsg2);

$loginMsg3 = (string) WidgetKeyValuePairs::getWidgetValueForKey('loginMsg3');
$smarty->assign("loginMsg3",$loginMsg3);

$loginMsg4 = (string) WidgetKeyValuePairs::getWidgetValueForKey("loginMsg4");
$smarty->assign("loginMsg4",$loginMsg4);

//condition to check id captcha has to be displayed while login
 
$showLoginCaptchaFG = FeatureGateKeyValuePairs::getBoolean('showLoginCaptcha');
 
if($showLoginCaptchaFG){
    $ip_attempt_limit = WidgetKeyValuePairs::getWidgetValueForKey('ip_attempt_limit');
    $ip_table_ttl = WidgetKeyValuePairs::getWidgetValueForKey('ip_table_ttl');    
    
    $clientIPAddr = get_user_ip();
    $showLoginCaptcha = showLoginCaptcha($clientIPAddr, $ip_attempt_limit);
    
    if($showLoginCaptcha){
        Profiler::increment("captcha-shown-to-user");
    }
    
    $smarty->assign("showLoginCaptcha", $showLoginCaptcha);
    $smarty->assign("showLoginCaptchaFG", $showLoginCaptchaFG);
}

/** End of setting Login and Signup Text **/





$cashback_gateway_status = FeatureGateKeyValuePairs::getFeatureGateValueForKey('cashbackenabled');
if($cashback_gateway_status == "internal") {
	$pos = strpos($login, "@myntra.com");
	if($pos === false)
	$cashback_gateway_status = "off";
	else
	$cashback_gateway_status = "on";
}
$smarty->assign("cashback_gateway_status",$cashback_gateway_status);

$cdnJSPath = WidgetKeyValuePairs::getWidgetValueForKey($server_name."-myntraJS");
if(!empty($cdnJSPath)) {
	$cdnJSPath = $cdn_base."/" . $cdnJSPath;
} else {
	$cdnJSPath = $http_location."/" . $js_path;
}
$smarty->assign("cdnJSPath", $cdnJSPath);

$cdnCSSPath = WidgetKeyValuePairs::getWidgetValueForKey($server_name."-myntraCSS");
if(!empty($cdnCSSPath)) {
	$cdnCSSPath = $cdn_base."/" . $cdnCSSPath;
} else {
	$cdnCSSPath = $http_location."/" . $css_path;
}
$smarty->assign("cdnCSSPath", $cdnCSSPath);


$secureCDNJSPath = WidgetKeyValuePairs::getWidgetValueForKey($server_name."-secureMyntraJS");
if(!empty($secureCDNJSPath)) {
	$secureCDNJSPath = $secure_cdn_base."/" . $secureCDNJSPath;
} else {
	$secureCDNJSPath = $https_location."/" . $secure_js_path;
}
$smarty->assign("secureCDNJSPath", $secureCDNJSPath);

$secureCDNCSSPath = WidgetKeyValuePairs::getWidgetValueForKey($server_name."-secureMyntraCSS");
if(!empty($secureCDNCSSPath)) {
	$secureCDNCSSPath = $secure_cdn_base."/" . $secureCDNCSSPath;
} else {
	$secureCDNCSSPath = $https_location."/" . $secure_css_path;
}
$smarty->assign("secureCDNCSSPath", $secureCDNCSSPath);

$topHeaderMessage=WidgetKeyValuePairs::getWidgetValueForKey(topHeaderMessage);
if($topHeaderMessage != NULL){
	$smarty->assign("topHeaderMessage", $topHeaderMessage);
}

$enable_cashback_discounted_products = FeatureGateKeyValuePairs::getFeatureGateValueForKey('enableCBOnDiscountedProducts');
if(!empty($enable_cashback_discounted_products))	{
	$smarty->assign("enableCBOndiscountedProducts",$enable_cashback_discounted_products );
} else	{
	$smarty->assign("enableCBOndiscountedProducts", "false");
}

/*********** smarty values for controlling additional fields on signup page *************/

$newSignupAB = MABTest::getInstance()->getUserSegment('newSignupAB');

$display_name = FeatureGateKeyValuePairs::getBoolean('display_name');
$display_birth_year = FeatureGateKeyValuePairs::getBoolean('display_birth_year');
$is_name_optional = FeatureGateKeyValuePairs::getBoolean('is_name_optional');
$is_birth_year_optional = FeatureGateKeyValuePairs::getBoolean('is_birth_year_optional');
$birth_year_range_low = (int)WidgetKeyValuePairs::getWidgetValueForKey('birth_year_range_low');
$birth_year_range_high = (int)WidgetKeyValuePairs::getWidgetValueForKey('birth_year_range_high');
$birth_year_range = $birth_year_range_high - $birth_year_range_low + 1;

$smarty->assign('newSignupAB', $newSignupAB);
$smarty->assign('display_name', $display_name);
$smarty->assign('display_birth_year', $display_birth_year);
$smarty->assign('is_name_optional', $is_name_optional);
$smarty->assign('is_birth_year_optional', $is_birth_year_optional);
$smarty->assign('birth_year_range', $birth_year_range);
$smarty->assign('birth_year_range_high', $birth_year_range_high);

/*--------------------------------------------------------------------------------------*/

//Gifting Feature gate values
$giftingEnabled = FeatureGateKeyValuePairs::getBoolean('gifting.enabled','false');
if($giftingEnabled != NULL){
	$smarty->assign("giftingEnabled", $giftingEnabled);
}

//Gifting Feature gate limit
$giftingLowerLimit = FeatureGateKeyValuePairs::getInteger('gifting.lower.limit', 500);
if($giftingLowerLimit != NULL){
	$smarty->assign("giftingLowerLimit", $giftingLowerLimit);
}

$giftingUpperLimit = FeatureGateKeyValuePairs::getInteger('gifting.upper.limit', 500);
if($giftingUpperLimit != NULL){
	$smarty->assign("giftingUpperLimit", $giftingUpperLimit);
}

$saleClearanceURL= FeatureGateKeyValuePairs::getFeatureGateValueForKey('saleClearanceURL');
if($saleClearanceURL != NULL){
	$smarty->assign("saleClearanceURL", $saleClearanceURL);
}

// boosted search and curated search
$boostEnabled = FeatureGateKeyValuePairs::getBoolean("boostedSearch.enabled",false);
$curatedSearchEnabled = FeatureGateKeyValuePairs::getBoolean("curatedSearch.enabled",false);

global $_MABTestObject;
$showBoostedSearch= $_MABTestObject->getUserSegment("boostedSearch");
$showCuratedSearch = $_MABTestObject->getUserSegment("CuratedSearch");
$searchPageSortParam;
if(($boostEnabled && ($showBoostedSearch=="enabled")) || ($curatedSearchEnabled && ($showCuratedSearch=="enabled"))) {
	$searchPageSortParam=WidgetKeyValuePairs::getWidgetValueForKey("searchPageSortParamBoosted");
} else {
	$searchPageSortParam=WidgetKeyValuePairs::getWidgetValueForKey("searchPageSortParamNonBoosted");
}
$smarty->assign("searchPageSortParam", strtoupper($searchPageSortParam));

$searchPageDefaultNoOfItems=WidgetKeyValuePairs::getInteger("searchPageDefaultNoOfItems");
if($searchPageDefaultNoOfItems != NULL){
	$smarty->assign("searchPageDefaultNoOfItems", $searchPageDefaultNoOfItems);
}
$saleClearanceSortParam=WidgetKeyValuePairs::getWidgetValueForKey("saleClearanceSortParam");
if($saleClearanceSortParam != NULL){
	$smarty->assign("saleClearanceSortParam", strtoupper($saleClearanceSortParam));
}
$noOfAutoInfiniteScrolls=WidgetKeyValuePairs::getInteger("noOfAutoInfiniteScrolls", 4);
$smarty->assign("noOfAutoInfiniteScrolls", $noOfAutoInfiniteScrolls);

$loginTimeout=WidgetKeyValuePairs::getInteger("loginTimeout", 30);
$smarty->assign("loginTimeout", $loginTimeout);

// Message displayed on login dialog below FB Connect 
$connectFbMsg=WidgetKeyValuePairs::getWidgetValueForKey("connect_fb_message");
$smarty->assign("connectFbMsg", $connectFbMsg);
// Message displayed on login dialog below New user Sign up
$signUpMsg=WidgetKeyValuePairs::getWidgetValueForKey("signup_message");
$smarty->assign("signUpMsg", $signUpMsg);
//Setting rupee symbol to be used across the site
// use &#x20B9; for rupeer symbol
$smarty->assign("rupeesymbol","Rs. ");

//default value is true
$skipMobileVerification = FeatureGateKeyValuePairs::getBoolean('mrp.skipMobileVerification', "false");
$smarty->assign("mrp_skipMobileVerification", intval($skipMobileVerification));

/**AVAIL COOKIES**/

$tmp_rpi_avail = getMyntraCookie('RPI_AVAIL');
if(!empty($tmp_rpi_avail)){
	$smarty->assign("RPI_AVAIL", $tmp_rpi_avail);
}
$tmp_rcv_avail = getMyntraCookie('RCV_AVAIL');
if(!empty($tmp_rcv_avail)){
	$smarty->assign("RCV_AVAIL", $tmp_rcv_avail);
}

const COUPONCONFIGURATION_CACHEKEY = "couponconfiguration_xcache";
global $xcache ;
if($xcache==null){
	$xcache = new XCache();
}
$mrpCouponConfiguration = $xcache->fetchAndStore(function() {
global $mrpDefaultCouponConfiguration;
//$mrpCouponConfiguration = FeatureGateKeyValuePairs::getAssociativeArray('mrp.couponConfiguration',$mrpDefaultCouponConfiguration);
$couponConfiguration['mrp_firstLogin_nonfbreg_numCoupons'] =
	FeatureGateKeyValuePairs::getInteger("mrp.firstLogin.nonfbreg.numCoupons", $mrpDefaultCouponConfiguration['mrp_firstLogin_nonfbreg_numCoupons']);
$couponConfiguration['mrp_firstLogin_nonfbreg_mrpAmount'] =
	FeatureGateKeyValuePairs::getInteger("mrp.firstLogin.nonfbreg.mrpAmount", $mrpDefaultCouponConfiguration['mrp_firstLogin_nonfbreg_mrpAmount']);
$couponConfiguration['mrp_firstLogin_nonfbreg_validity'] =
	FeatureGateKeyValuePairs::getInteger("mrp.firstLogin.nonfbreg.validity", $mrpDefaultCouponConfiguration['mrp_firstLogin_nonfbreg_validity']);
$couponConfiguration['mrp_firstLogin_nonfbreg_minCartValue'] =
	FeatureGateKeyValuePairs::getInteger("mrp.firstLogin.nonfbreg.minCartValue", $mrpDefaultCouponConfiguration['mrp_firstLogin_nonfbreg_minCartValue']);

$couponConfiguration['mrp_firstLogin_fbreg_numCoupons'] =
	FeatureGateKeyValuePairs::getInteger("mrp.firstLogin.fbreg.numCoupons", $mrpDefaultCouponConfiguration['mrp_firstLogin_fbreg_numCoupons']);
$couponConfiguration['mrp_firstLogin_fbreg_mrpAmount'] =
	FeatureGateKeyValuePairs::getInteger("mrp.firstLogin.fbreg.mrpAmount", $mrpDefaultCouponConfiguration['mrp_firstLogin_fbreg_mrpAmount']);
$couponConfiguration['mrp_firstLogin_fbreg_validity'] =
	FeatureGateKeyValuePairs::getInteger("mrp.firstLogin.fbreg.validity", $mrpDefaultCouponConfiguration['mrp_firstLogin_fbreg_validity']);
$couponConfiguration['mrp_firstLogin_fbreg_minCartValue'] =
	FeatureGateKeyValuePairs::getInteger("mrp.firstLogin.fbreg.minCartValue", $mrpDefaultCouponConfiguration['mrp_firstLogin_fbreg_minCartValue']);


$couponConfiguration['mrp_refFirstPurchase_numCoupons'] =
	FeatureGateKeyValuePairs::getInteger("mrp.refFirstPurchase.numCoupons", $mrpDefaultCouponConfiguration['mrp_refFirstPurchase_numCoupons']);
$couponConfiguration['mrp_refFirstPurchase_mrpAmount'] =
	FeatureGateKeyValuePairs::getInteger("mrp.refFirstPurchase.mrpAmount", $mrpDefaultCouponConfiguration['mrp_refFirstPurchase_mrpAmount']);
$couponConfiguration['mrp_refFirstPurchase_validity'] =
	FeatureGateKeyValuePairs::getInteger("mrp.refFirstPurchase.validity", $mrpDefaultCouponConfiguration['mrp_refFirstPurchase_validity']);
$couponConfiguration['mrp_refFirstPurchase_minCartValue'] =
	FeatureGateKeyValuePairs::getInteger("mrp.refFirstPurchase.minCartValue", $mrpDefaultCouponConfiguration['mrp_refFirstPurchase_minCartValue']);


$couponConfiguration['mrp_refRegistration_numCoupons'] =
	FeatureGateKeyValuePairs::getInteger("mrp.refRegistration.numCoupons", $mrpDefaultCouponConfiguration['mrp_refRegistration_numCoupons']);
$couponConfiguration['mrp_refRegistration_mrpAmount'] =
	FeatureGateKeyValuePairs::getInteger("mrp.refRegistration.mrpAmount", $mrpDefaultCouponConfiguration['mrp_refRegistration_mrpAmount']);
$couponConfiguration['mrp_refRegistration_validity'] =
	FeatureGateKeyValuePairs::getInteger("mrp.refRegistration.validity", $mrpDefaultCouponConfiguration['mrp_refRegistration_validity']);
$couponConfiguration['mrp_refRegistration_minCartValue'] =
	FeatureGateKeyValuePairs::getInteger("mrp.refRegistration.minCartValue", $mrpDefaultCouponConfiguration['mrp_refRegistration_minCartValue']);

$couponConfiguration['cust_survey_validity'] =
	FeatureGateKeyValuePairs::getInteger("mrp.cust.survey.validity", $mrpDefaultCouponConfiguration['cust_survey_validity']);
$couponConfiguration['cust_survey_mrpAmount'] =
	FeatureGateKeyValuePairs::getInteger("mrp.cust.survey.mrpAmount", $mrpDefaultCouponConfiguration['cust_survey_mrpAmount']);
$couponConfiguration['cust_survey_minCartValue'] =
	FeatureGateKeyValuePairs::getInteger("mrp.cust.survey.minCartValue", $mrpDefaultCouponConfiguration['cust_survey_minCartValue']);

	return $couponConfiguration;
}, array(), COUPONCONFIGURATION_CACHEKEY);

// Amount earnable by mrp program - to be displayed on the menu strip
$menuDisplayRefAmount = $mrpCouponConfiguration['mrp_refFirstPurchase_numCoupons'] * $mrpCouponConfiguration['mrp_refFirstPurchase_mrpAmount']
  	 			+ $mrpCouponConfiguration['mrp_refRegistration_numCoupons'] * $mrpCouponConfiguration['mrp_refRegistration_mrpAmount'];
$mrp_fbCouponValue = $mrpCouponConfiguration['mrp_firstLogin_fbreg_numCoupons'] * $mrpCouponConfiguration['mrp_firstLogin_fbreg_mrpAmount'];
$mrp_nonfbCouponValue = $mrpCouponConfiguration['mrp_firstLogin_nonfbreg_numCoupons'] * $mrpCouponConfiguration['mrp_firstLogin_nonfbreg_mrpAmount'];
$mrp_nonfbCouponValueLogin = number_format($mrp_nonfbCouponValue);

$smarty->assign("mrp_fbCouponValue",$mrp_fbCouponValue);
$smarty->assign("mrp_nonfbCouponValue",$mrp_nonfbCouponValue);
$smarty->assign("mrp_nonfbCouponValueLogin",$mrp_nonfbCouponValueLogin);
$smarty->assign("mrp_nonfbNumCoupons",$mrpCouponConfiguration['mrp_firstLogin_nonfbreg_numCoupons']);
$smarty->assign("mrp_nonfbMrpAmount",$mrpCouponConfiguration['mrp_firstLogin_nonfbreg_mrpAmount']);

$couponConfig = json_decode(WidgetKeyValuePairs::getWidgetValueForKey("registrationCouponConfig"),true);
$fbCouponConfig = json_decode(WidgetKeyValuePairs::getWidgetValueForKey("fbRegistrationCouponConfig"),true);
if ($showFbCoupons)
	$reg2couponsConfig =  $fbCouponConfig ;
else
	$reg2couponsConfig = $couponConfig;
$couponTypeNum = 0;
$couponAbsolute = false;
$couponAbsoluteValue= 0;
$couponPercentageValue = 0;
$couponDualValue= 0;
$couponPercentage = false;
$couponDual = false;
$couponDualMaxValue = 0;
$numAbsoluteCoupon = 0;
foreach($reg2couponsConfig as $id=>$aCoupon){
	if ($aCoupon["coupon_type"] == "absolute"){
		$couponAbsoluteValue += $aCoupon["amount_discount"];
		$numAbsoluteCoupon += 1;
	}
	if ($aCoupon["coupon_type"] == "percentage"){
		$couponPercentageValue = $aCoupon["percent_discount"]; 
	}
	if ($aCoupon["coupon_type"] == "dual"){
		$couponDualValue = $aCoupon["percent_discount"];
		$couponDualMaxValue  = $aCoupon["amount_discount"];
	}
	if ($aCoupon["coupon_type"] == "absolute"&& !$couponAbsolute){
		$couponAbsolute = true;
		$couponTypeNum = $couponTypeNum + 1;
	}
	if ($aCoupon["coupon_type"] == "percentage"&& !$couponPercentage){
	        $couponPercentage = true;
	        $couponTypeNum = $couponTypeNum + 1;
	}
	if ($aCoupon["coupon_type"] == "dual"&& !$couponDual){
	       $couponDual = true;
	       $couponTypeNum = $couponTypeNum + 1;
	}
}
$smarty->assign("couponTypeNum",$couponTypeNum);
$smarty->assign("couponAbsolute",$couponAbsolute);
$smarty->assign("couponAbsoluteValue",$couponAbsoluteValue);
$smarty->assign("couponPercentageValue",$couponPercentageValue);
$smarty->assign("couponDualValue",$couponDualValue);
$smarty->assign("couponPercentage",$couponPercentage);
$smarty->assign("couponDual",$couponDual);
$smarty->assign("couponDualMaxValue",$couponDualMaxValue);
$smarty->assign("numAbsoluteCoupon",$numAbsoluteCoupon);

$regCouponSignUpOverLayList = "<ol>";

foreach($couponConfig as $id=>$aCoupon){
	if ($aCoupon["coupon_type"] == "absolute"){
		$regCouponSignUpOverLayList=$regCouponSignUpOverLayList ."<li>Rs. ".$aCoupon["amount_discount"]." off on purchase of Rs. ".$aCoupon["minimum_purchase"]." or more.</li>";

	
	}elseif ($aCoupon["coupon_type"] == "percentage"){
		$regCouponSignUpOverLayList=$regCouponSignUpOverLayList ."<li> ".$aCoupon["percent_discount"]."% off on purchase of Rs. ".$aCoupon["minimum_purchase"]." or more.</li>";

	}elseif ($aCoupon["coupon_type"] == "dual"){
		$regCouponSignUpOverLayList=$regCouponSignUpOverLayList ."<li> ".$aCoupon["percent_discount"]." off upto Rs. ".$aCoupon["amount_discount"]." on purchase of Rs. ".$aCoupon["minimum_purchase"]." or more.</li>";

	}
}

$regCouponSignUpOverLayList = $regCouponSignUpOverLayList."</ol>";	

$fBregCouponSignUpOverLayList = "<ol>";
foreach($fbCouponConfig as $id=>$aCoupon){
	if ($aCoupon["coupon_type"] == "absolute"){
		$fBregCouponSignUpOverLayList=$fBregCouponSignUpOverLayList ."<li>Rs. ".$aCoupon["amount_discount"]." off on purchase of Rs. ".$aCoupon["minimum_purchase"]." or more.</li>";

	}elseif ($aCoupon["coupon_type"] == "percentage"){
		$fBregCouponSignUpOverLayList=$fBregCouponSignUpOverLayList ."<li> ".$aCoupon["percent_discount"]."% off on purchase of Rs. ".$aCoupon["minimum_purchase"]." or more.</li>";

	}elseif ($aCoupon["coupon_type"] == "dual"){
		$fBregCouponSignUpOverLayList=$fBregCouponSignUpOverLayList ."<li> ".$aCoupon["percent_discount"]." off upto Rs. ".$aCoupon["amount_discount"]." on purchase of Rs. ".$aCoupon["minimum_purchase"]." or more.</li>";

	}
}
$fBregCouponSignUpOverLayList = $fBregCouponSignUpOverLayList."</ol>";
$smarty->assign("regCouponSignUpOverLayList",$regCouponSignUpOverLayList);
$smarty->assign("fBregCouponSignUpOverLayList",$fBregCouponSignUpOverLayList);


/**
 * Needed for base tracker call in confirmation page - Raghu
 * This data is set for the BI Team
 */
//AB test tracking for adjusted style revenue
$styleRATest = FeatureGateKeyValuePairs::getFeatureGateValueForKey("styleRATest");
$showAdjustedRevenueSearch2 = MABTest::getInstance()->getUserSegment($styleRATest);
/**
 * Needed for base tracker call in confirmation page - Raghu
 * This data is set for the BI Team
 */

// A/B test for splash login dialog
$abLoginPopup = 'splash'; // $_MABTestObject->getUserSegment('loginpopup');
$smarty->assign('abLoginPopup', $abLoginPopup);

// A/B test for showing splash screen with facebook button only
$abSplash = $_MABTestObject->getUserSegment('splash');
$smarty->assign('abSplash', $abSplash);

// A/B test for making the "locality" field mandatory or not in the shipping address
$abLocality = $_MABTestObject->getUserSegment('locality');
$smarty->assign('abLocality', $abLocality);

// A/B test for loginbox
$abLoginbox = $_MABTestObject->getUserSegment('loginbox');
$smarty->assign('abLoginbox', $abLoginbox);

//A/B test for gender in login box
$showgender=$_MABTestObject->getUserSegment("registerGender");
//echo $showgender;exit;
$smarty->assign("showgender",$showgender);

//Include file for 3rd party monitoring (newrelic/tracelytics etc)
include_once (HostConfig::$documentRoot."/monitoring.php");

$cashback_displayOnPDPPage = FeatureGateKeyValuePairs::getBoolean('cashback.displayOnPDPPage', true);
$cashback_displayOnHomePage = FeatureGateKeyValuePairs::getBoolean('cashback.displayOnHomePage', true);
$cashback_displayOnCartPage = FeatureGateKeyValuePairs::getBoolean('cashback.displayOnCartPage', true);
$cashback_displayOnSearchPage = FeatureGateKeyValuePairs::getBoolean('cashback.displayOnSearchPage', true);

$smarty->assign("cashback_displayOnPDPPage",$cashback_displayOnPDPPage);
$smarty->assign("cashback_displayOnHomePage",$cashback_displayOnHomePage);
$smarty->assign("cashback_displayOnCartPage",$cashback_displayOnCartPage);
$smarty->assign("cashback_displayOnSearchPage",$cashback_displayOnSearchPage);

$verifyMobileforCOD = FeatureGateKeyValuePairs::getBoolean('codMobileVerification.enable', true);
$smarty->assign("verifyMobileforCOD",$verifyMobileforCOD);

//Most Popular tags
/*$top_nav = $xcache->fetch($xcart_http_host . '-' . "top_nav-".$abHeaderUi);
if($top_nav == NULL || $nocache == 1){
    // Populate top navigation menu.
    include_once($xcart_dir."/include/class/widget/headerMenu.php");
    //if($megamenutestsegment==='megamenu'){
    	$top_nav = Top_nav::getTopNavMegaMenu($smarty);
    //}else{
    	 //$top_nav = Top_nav::getTopNav($smarty);
    //}

    //$top_nav = func_display("site/menu_topnav.tpl", $smarty, false);
    if($nocache != 1){//Do not store in case nocache param is set
        $xcache->store($xcart_http_host . '-' . "top_nav-".$abHeaderUi, $top_nav);
    }
}*/
global $BTHTTPCookie;
$BTHTTPCookie = FeatureGateKeyValuePairs::getBoolean('baseTracker.HTTPCookie', false);
include_once($xcart_dir."/include/class/widget/class.widget.php");
$widget = new Widget(WidgetMostPopular::$WIDGETNAME);
$widget->setDataToSmarty($smarty);

//quicklook featuregate
$quicklookenabled = FeatureGateKeyValuePairs::getBoolean('quicklook.enabled', true);
$smarty->assign("quicklookenabled",$quicklookenabled);

//Offer on quicklook
$quickLookCouponWidgetText = "";
$quickLookCouponWidgetABTest=MABTest::getInstance()->getUserSegment("quickLookOffer");
if(substr($quickLookCouponWidgetABTest,0,4) == 'test'){
    $quickLookCouponWidgetEnabled = true; 
    $quickLookCouponWidgetText = WidgetKeyValuePairs::getWidgetValueForKey("quickLookCouponWidgetText");
}
else{
    $quickLookCouponWidgetEnabled = false; 
}

$smarty->assign('quickLookCouponWidgetEnabled',$quickLookCouponWidgetEnabled);
$smarty->assign('quickLookCouponWidgetABTest',$quickLookCouponWidgetABTest);
$smarty->assign('quickLookCouponWidgetText',$quickLookCouponWidgetText);

//Offer on quicklook end

// errorception feature gate
$smarty->assign("errorceptionEnabled", FeatureGateKeyValuePairs::getBoolean('errorception.enabled', true));

//wishlist feature gate & buy now button
$wishlistenabled = FeatureGateKeyValuePairs::getBoolean('wishlist.enabled', true);
$buynowlabel=WidgetKeyValuePairs::getWidgetValueForKey("buy-now-button-text");
$smarty->assign("wishlistenabled",$wishlistenabled);
$smarty->assign("buynowlabel",$buynowlabel);

// Telesales AB test and number
$showtelesalesnumber=MABTest::getInstance()->getUserSegment("telesales");
$telesalesNumber=WidgetKeyValuePairs::getWidgetValueForKey('telesalesNumber');
$smarty->assign('showtelesalesNumber', $showtelesalesnumber == 'shownumber');
$smarty->assign('telesalesNumber', $telesalesNumber);

//AB Test for New Layout
$layoutVariant = MABTest::getInstance()->getUserSegment('NewLayout');
$smarty->assign("layoutVariant", $layoutVariant);

//AB Test for Save In Wishlist
$smarty->assign("wishlistVariant", MABTest::getInstance()->getUserSegment('PDPWishlist'));

//AB Test for Notify Me
$smarty->assign("notifyMeVariant", MABTest::getInstance()->getUserSegment('NotifyMe'));

//AB Test for Login Splash
$smarty->assign("loginSplashVariant", MABTest::getInstance()->getUserSegment('LoginSplash'));

//AB Test for Colour Selection PDP & MiniPIP
$smarty->assign("colourSelectVariant", MABTest::getInstance()->getUserSegment('ColourSelect'));

//AB Test for Social Share 
$smarty->assign("socialShareVariant", MABTest::getInstance()->getUserSegment('SocialShare'));

$megaFooterEnabled = FeatureGateKeyValuePairs::getBoolean('megafooter.enabled', true);
$smarty->assign("megaFooterEnabled",$megaFooterEnabled);

//Feature gate got promo-banner
$promoBannerEnabled = FeatureGateKeyValuePairs::getBoolean('promobanner');
$smarty->assign("promoBannerEnabled", $promoBannerEnabled);

//Feature gate for shopping festival
$shoppingFestivalEnabled = FeatureGateKeyValuePairs::getBoolean('myntrashoppingfest');
$smarty->assign('shoppingFestivalEnabled', $shoppingFestivalEnabled);

//Feature gate for shopping festival - cashback coupons
$myntrashoppingfestCoupons = FeatureGateKeyValuePairs::getBoolean('myntrashoppingfestCoupons');
$smarty->assign('myntrashoppingfestCoupons', $myntrashoppingfestCoupons);

//Fetch the image link
$festivalColumnImageLink = WidgetKeyValuePairs::getWidgetValueForKey("festivalColumnImageLink");
$smarty->assign('festivalColumnImageLink', $festivalColumnImageLink);

//Fetch the image link
$shoppingfest_dailyPrizes = WidgetKeyValuePairs::getWidgetValueForKey("shoppingfest_dailyPrizes");
$smarty->assign('shoppingfestDailyPrizes', $shoppingfest_dailyPrizes);

//Fetch the image link
$shoppingfest_assuredandbumperprizes = WidgetKeyValuePairs::getWidgetValueForKey("shoppingfest_assuredandbumperprizes");
$smarty->assign('shoppingfestAssuredandBumperPrizes', $shoppingfest_assuredandbumperprizes);

//Fetch the image link
$leaderBoardId = WidgetKeyValuePairs::getWidgetValueForKey("leaderBoardId");
$smarty->assign('leaderBoardId', $leaderBoardId);

//festival header
$festHeaderImage = \WidgetKeyValuePairs::getWidgetValueForKey('fest_header_image');
$festHeaderImage = json_decode($festHeaderImage,true);

if($festHeaderImage['center']) {
	$smarty->assign('festHeaderImage_Center', $festHeaderImage['center'][0]);
	$smarty->assign('festHeaderImage_Center_Secure', $festHeaderImage['center'][1]);	
}
if($festHeaderImage['left']) {
	$smarty->assign('festHeaderImage_Left', $festHeaderImage['left'][0]);
	$smarty->assign('festHeaderImage_Left_Secure', $festHeaderImage['left'][1]);
}

if($festHeaderImage['right']) {
	$smarty->assign('festHeaderImage_Right', $festHeaderImage['right'][0]);
	$smarty->assign('festHeaderImage_Right_Secure', $festHeaderImage['right'][1]);
}

$festHeaderImageURL = WidgetKeyValuePairs::getWidgetValueForKey("fest_header_image_link");
$smarty->assign('festHeaderImageURL', $festHeaderImageURL);

//cart offer message
$cartOfferMessage = WidgetKeyValuePairs::getWidgetValueForKey("fest_cart_message");
$cartOfferMessage = json_decode($cartOfferMessage,true);
if($cartOfferMessage['offermessage1']) {
	$smarty->assign('cartOfferMessage1', $cartOfferMessage['offermessage1']);	
}

if($cartOfferMessage['offermessage2']) {
	$smarty->assign('cartOfferMessage2', $cartOfferMessage['offermessage2']);
}

//emi in footer enabled
$payment_options = FeatureGateKeyValuePairs::getStrArray("PaymentOptionsOrder", $defaultPaymentOrder);
$smarty->assign('emiEnabled', in_array('emi', $payment_options));

$casback_earnedcredits_enabled = FeatureGateKeyValuePairs::getBoolean('cashback.earnedcredits.enabled',true);
$smarty->assign('cashbackearnedcreditsEnabled', intval($casback_earnedcredits_enabled));

$analyticsOmnitureStatus = FeatureGateKeyValuePairs::getBoolean('omniture.enabled');
$smarty->assign("analyticsStatus", $analyticsOmnitureStatus);

$isNewCheckout = MABTest::getInstance()->getUserSegment('checkout') == 'test';
$smarty->assign("isNewCheckout", $isNewCheckout);

$isSecureCartEnabled = FeatureGateKeyValuePairs::getBoolean('securecart.enabled', true);
$smarty->assign('isSecureCartEnabled', $isSecureCartEnabled);

$vatToolTip = FeatureGateKeyValuePairs::getFeatureGateValueForKey('vat.tooltip');


$cartPageUrl = 'http://'.HostConfig::$httpHost.'/mkmycart.php';
if ($isNewCheckout && $skin!="skinmobile") {
    $cartPageUrl = $isSecureCartEnabled ? 'https://'.HostConfig::$httpsHost.'/checkout.php' : 'http://'.HostConfig::$httpHost.'/cart.php';
}
$smarty->assign('cartPageUrl', $cartPageUrl);

// link should be entered in head section only for production machines
$linkgplusacc = (HostConfig::$configMode == 'prod');
$smarty->assign('linkgplusacc',$linkgplusacc);
// Remarketting for FB and SEO visitors
if(substr($cookieContent['utm_source'], 0, strlen('fb')) === 'fb'){
	$smarty->assign('remarketFBVisitor',true);
}
// medium for seo can be google/yahoo/bing/search
if(((stripos($cookieContent['utm_medium'], 'google') !== false)||
	(stripos($cookieContent['utm_medium'], 'yahoo') !== false) ||
	(stripos($cookieContent['utm_medium'], 'bing') !== false) ||
	(stripos($cookieContent['utm_medium'], 'search') !== false)
	) && $cookieContent['utm_source'] == 'referral'){
	$smarty->assign('remarketSEOVisitor',true);
}

// Guest Login FeatureGate
$enableGuestLogin = FeatureGateKeyValuePairs::getBoolean('guest.login.enabled', true);
$smarty->assign('enableGuestLogin', $enableGuestLogin);

// config mode
$smarty->assign('configMode', $configMode);

// Page End-User Load time FeatureGate
$enableSpy = FeatureGateKeyValuePairs::getBoolean('spy.enabled', true);
$smarty->assign('enableSpy', $enableSpy && !$filter);
$smarty->assign('spyId', $_COOKIE[$cookieprefix . 'xid']);

use leaderboard\LeaderBoard;

if($login){
	//include_once "$xcart_dir/include/class/leaderboard/LeaderBoard.php";
	$leaderBoardObject = new LeaderBoard();
	$lbId = $leaderBoardObject->getApplicableLeaderBoardToUser($login);
	if($lbId !=null){
		$smarty->assign("lbId",$lbId);
	}else{
		$smarty->assign("lbId",0);
	}
}
// Set smarty variable for showing the mobile site link
$deviceName = web\utils\DeviceDetector::getInstance()->getDevice();
$smarty->assign('deviceName', $deviceName);
$deviceType = 'desktop';
$isMobile = web\utils\DeviceDetector::getInstance()->isMobile();
$smarty->assign('isMobile', (int)$isMobile);
if ($isMobile) {
    $deviceType = 'mobile';
}
if($showMobileSiteLink && $isMobile){
    $smarty->assign('showMobileSiteLink', $showMobileSiteLink);
}
$isTablet = web\utils\DeviceDetector::getInstance()->isTablet();
$smarty->assign('isTablet', (int)$isTablet);
if ($isTablet) {
    $deviceType = 'tablet';
}
$smarty->assign('deviceType', $deviceType);


 
// set device channel in the session
$newChannel = web\utils\ChannelType::getChannel($deviceType);

$isMobileApp = web\utils\DeviceDetector::getInstance()->isMobileApp();

if($isMobileApp) {
    $smarty->assign('isMobileApp', (int)$isMobileApp);
    $newChannel = web\utils\ChannelType::getChannel('mobile_app');
}

if(x_session_is_registered("channel")){
	global $XCART_SESSION_VARS;
	if($newChannel != $XCART_SESSION_VARS["channel"]){
		x_session_unregister("channel");
	}
}
x_session_register('channel',$newChannel);

// Quick fix : FOR MOBILE APP ga account is different
if($isMobileApp) {
    $ga_acc = 'UA-1752831-21';
    $smarty->assign("ga_acc",$ga_acc);
}

//FOR SECURE LOGIN 
$secureLoginFeatureGate=\FeatureGateKeyValuePairs::getBoolean('secure.login');
$secureLoginABTest=$_MABTestObject->getUserSegment("secureSignin");
if($secureLoginABTest == 'secure' && $secureLoginFeatureGate){
	$secureLoginEnabled = true; 
}
else{
	$secureLoginEnabled = false; 
}

$smarty->assign('secureLoginEnabled',$secureLoginEnabled);

use revenue\payments\service\PaymentServiceInterface;
if(isset($_SERVER['HTTPS']) && $secureLoginEnabled ){
	$smarty->assign('session_inactive_threshold',FeatureGateKeyValuePairs::getInteger("sessionInactiveThreshold", 15*60));
	$smarty->assign('is_secure',true);
	if(PaymentServiceInterface::showLoginPromptForUser($login)){
		$smarty->assign('prompt_login',$XCART_SESSION_VARS['prompt_login']);
	}
}

if($login){
    $currentTime = time();
    $headerCacheThreshold = FeatureGateKeyValuePairs::getInteger('headerCacheThreshold', 15*60);
    $lastNotificationsCacheTime = $XCART_SESSION_VARS['lastNotificationsCacheTime'] ? $XCART_SESSION_VARS['lastNotificationsCacheTime'] : $currentTime;
    $notificationsCacheThresholdBreached = ($currentTime - $lastNotificationsCacheTime) > $headerCacheThreshold;
	$notificationsEnabled = FeatureGateKeyValuePairs::getBoolean('notifications');
	$smarty->assign('notificationsEnabled',$notificationsEnabled);
	if($notificationsEnabled){
        if(!$notificationsCacheThresholdBreached && !empty($XCART_SESSION_VARS['notificationsData'])) {
            $notificationsData = $XCART_SESSION_VARS['notificationsData'];
            $notificationCount = $notificationsData['notificationCount'];
            $newNotificationCount = $notificationsData['newNotificationCount'];
        }else{
    		require_once $xcart_dir.'/modules/RestAPI/RestRequest.php';
    		$url = HostConfig::$notificationsUpsUrl.'/getAll?userid=' . $login . '&value=-1';
    		$method = 'GET';
    		$data = array();
    		$request = new RestRequest($url, $method, null, 1);
    		$headers = array();
    		$headers[] = 'Accept: application/json';
    		$headers[] = 'Content-type: application/json';
    		$headers[] = 'Accept-Language: en-US';
    		$request->setHttpHeaders($headers);
    		$request->execute();
    		$info = $request->getResponseInfo();
    		$body = $request->getResponseBody();
    		if($info['http_code'] == 200){
    			$body = json_decode($body,true);
    			if($body['errorCode'] == 0){
    				if($body['body']['attributes'] == -1){
                       			 $notificationCount = -1;
    			                    $newNotificationCount = -1;    
    		                }
                    		else{
    		                    $notificationCount = $body['body']['attributes']['totalNotificationCounter'];
                    		    $newNotificationCount = $body['body']['attributes']['notificationCounter'];
    		                }
    			}	
    		}
        }
		$smarty->assign('notificationCount',empty($notificationCount)?-1:$notificationCount);
        $smarty->assign('newNotificationCount',empty($newNotificationCount)?-1:$newNotificationCount);
	}
    //export these variables to session so that this can be used in nodejs
    //in php on accessing auth.php these values are reset in the session
    //in nodejs - a threshold of 15 mins is set on lastSessionTime to reset these values
    $notificationsData = array();
    $notificationsData["notificationsEnabled"] = $notificationsEnabled;
    $notificationsData["notificationCount"] = (!empty($notificationCount))?$notificationCount:-1;
    $notificationsData["newNotificationCount"] = (!empty($newNotificationCount))?$newNotificationCount:-1;
    x_session_register("notificationsData", $notificationsData);
    if($notificationsCacheThresholdBreached && $notificationsEnabled){
        $lastNotificationsCacheTime = time();
        x_session_register("lastNotificationsCacheTime", $lastNotificationsCacheTime);
    }
}

//SAVED BAG RECENT
//cart initialization in newui
include_once($xcart_dir."/cartinit.php");

if(!defined('_CONTROLLER_')){
	include_once("$xcart_dir/include/cssjs.min.php");
}

$_MABTestObject->updateTemplateData();
//exporting cart values to frontend for webengage
$smarty->assign('cart_total_quantity',$XCART_SESSION_VARS['CART:cartTotalQuantity']);
$smarty->assign('cart_total_amount',ceil($XCART_SESSION_VARS['CART:totalAmountCart']));

include_once("$xcart_dir/include/func/func.seo.php");
$utm_source = getUTMSource();
$utm_medium = getUTMMedium();
$utm_campaignName = getUTMCampaign();
$utm_campaignId = getUTMCampaignId();
$utm_octaneEmail = getUTMOctaneEmail();
$smarty->assign('utm_source', $utm_source);
$smarty->assign('utm_medium', $utm_medium);
$smarty->assign('utm_campaignName', $utm_campaignName);
$smarty->assign('utm_campaignId', $utm_campaignId);
$smarty->assign('utm_octaneEmail', $utm_octaneEmail);
if(MABTest::getInstance()->getUserSegment("checkout") ==="test"){
	MABTest::getInstance()->getUserSegment("couponAutoApplyTest");
}

$orderPlacementFlow = 'new';
/*$internalUsersList = FeatureGateKeyValuePairs::getFeatureGateValueForKey('orderplacement.flow.new.internalusers');
if($internalUsersList && !empty($internalUsersList)) {
	if(strtolower($internalUsersList) == 'all') {
		$pos = strpos($login, "@myntra.com");
		if($pos !== false)
			$orderPlacementFlow = 'new';
	} else {
		$enabledUsers = explode(",", $internalUsersList);
		if(in_array($login, $enabledUsers)) {
			$orderPlacementFlow = 'new';
		}
	}

} else {
	$orderplacementABTest = MABTest::getInstance()->getUserSegment('orderplacement');
	if($orderplacementABTest == 'test')
		$orderPlacementFlow = 'new';
}*/

$googlePlusLogin = MABTest::getInstance()->getUserSegment("gplusLogin");
$googleAbTest = array("status" => "", "mab" => "", "data" => array( array("abTestName" => "gplusLogin","segmentValue" => $googlePlusLogin, "cookieValue" => "")));
$googleJsonData = json_encode($googleAbTest);
$smarty->assign('googleJsonData', $googleJsonData);

MABTest::getInstance()->getUserSegment("checkout_flow_java");
MABTest::getInstance()->getUserSegment("coupon_applicability");
$smarty->assign('remember_user',getMyntraCookie($REMEMBER_USER,true));
$promoCouponsVariant = MABTest::getInstance()->getUserSegment("promo_coupons");
$promoCouponsMobileVariant = MABTest::getInstance()->getUserSegment("promo_coupons_mobile");
$cilckForOfferMobile = MABTest::getInstance()->getUserSegment("coupon_offer_mobile");
$smarty->assign('cilckForOfferMobile',$cilckForOfferMobile);
$smarty->assign('promoCouponsVariant', $promoCouponsVariant);
$smarty->assign('promoCouponsMobileVariant', $promoCouponsMobileVariant);
$promoCoupons = WidgetKeyValuePairs::getWidgetValueForKey("promoCoupons");
$smarty->assign('promoCoupons', $promoCoupons);
$promoCouponsText = FeatureGateKeyValuePairs::getFeatureGateValueForKey('promoCouponsText');
?>
