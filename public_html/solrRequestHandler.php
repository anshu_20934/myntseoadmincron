<?php
include_once './auth.php';
include_once "$xcart_dir/include/class/instrumentation/BaseTracker.php";
require_once "$xcart_dir/include/class/class.Search_Synonym_Key.php";
include_once "$xcart_dir/utils/request/class.RequestUtils.php";
$tracker = new BaseTracker(BaseTracker::SEARCH_PAGE);

$requestUri = $_SERVER['REQUEST_URI'];
$requestUri = urldecode($requestUri);
$qpos = strpos($requestUri,"?");
$request = $requestUri;
if ($qpos !== false) {
       $request = substr($requestUri, 0, $qpos);
}

// explode on / to find all the different request parts
$extensions=array("\.html","\.htm","\.png","\.jpeg","\.jpg","\.ttf","\.gif");
$regex = '/(' .implode('|', $extensions) .')/i';
if(preg_match ($regex,$request))
{
        include "./defaulthandler.php";
        exit;
}
$parts = explode('/', $request);

/*if(preg_match ("/\b.html\b/i",$request) || preg_match ("/\b.jpeg\b/i",$request) || preg_match ("/\b.jpg\b/i",$request)|| preg_match ("/\b.png\b/i",$request))
{
	include "./defaulthandler.php";
	exit;
}
$parts = explode('/', $request);*/
// the first element will be empty to we get rid of it
array_shift($parts);
for($i = 0, $n = count($parts); $i < $n; $i += 1) {
    $parts[$i] = filter_var($parts[$i], FILTER_SANITIZE_STRING);
}
$parts[0] = str_replace("%3C", "<", $parts[0]);

$ct=count($parts);
/*if($ct > 3)
{
        include "./defaulthandler.php";
        exit;
}*/

$parts[0] = strtolower($parts[0]);
$parts[0] = str_replace("%2f", "/", $parts[0]);
$parts[0] =html_entity_decode($parts[0],ENT_QUOTES) ;
if(!(RequestUtils::isParametrizedSearchPage($parts[1]) || RequestUtils::isLandingPage($parts[0])))
{
	$synonymArray = SearchSynonymKey::getAllSearchSynonymValuePairs();
	$synonymFlag = false;
	$paramWithSpace = str_replace("-", " ", $parts[0]);
	$words = explode("-",$parts[0]);
	for($numofWord=1;$numofWord<=sizeof($words);$numofWord++)
	{
		for($pos1stWord=0;$pos1stWord<=sizeof($words)-$numofWord;$pos1stWord++)
		{
			$tempStr = "";
			for($k=$pos1stWord;$k<=$pos1stWord+$numofWord-1;$k++)
			{
				$tempStr=$tempStr." ".$words[$k];
			}
			$tempStr = trim($tempStr);
			if(!empty($synonymArray[$tempStr]))
			{
				//Check if word is not already a substring
				//i.e. if all word of corrected word are already in search string just don't do anything
				$tempArray =  explode(" ",$synonymArray[$tempStr]);
				$checkFlag = true;
				for($i=0;$i<sizeof($tempArray);$i++)
				{
					if(strpos($paramWithSpace,$tempArray[$i])=== false)
					{
						$checkFlag = false;
						continue;
					}
				}
				if($checkFlag ==  true)
				{
					continue;
				}
				$paramWithSpace = str_replace($tempStr, $synonymArray[$tempStr], $paramWithSpace);
				$synonymFlag = true;
			}
		}
	}
	
	if($synonymFlag == true)
	{
		$_GET['synonymQuery'] = str_replace("-", " ", $parts[0]);
	}
	$parts[0] = str_replace(" ", "-", $paramWithSpace);
}
$_GET['query']=str_replace(" ","-",$parts[0]);
$narrow_by_u='';
$i_query=explode("-",$_GET['query']);
if($parts[0] == "")
{
	include_once "./index.php";
	exit;
}
$param=$parts[0];
//for some directories call shud not goto solr like adimin , so jst add directory as key and path as value in the following array
$redirect_array=array("admin"=>"./admin/index.php");
if( $redirect_array[$param])
{
	include_once $redirect_array[$param];
	exit;
}
foreach($i_query as $key=>$value)
{
	$narrow_by_u .=",".$value;
}
if(!empty($narrow_by_u))
	$_GET['narrow_by']=$narrow_by_u;
if($ct == 3)
{
	if(!empty($parts[1]) && $parts[1] != 'all')
        	$_GET['sortby']=$parts[1];
        if(!empty($parts[2]) && $parts[2] != 'all')
                $_GET['page']=$parts[2];
}
else if($ct == 2)
{
        if(!empty($parts[1]) && $parts[1] != 'all')
                $_GET['page']=$parts[1];
}

if(!empty($parts[1]) && RequestUtils::isParametrizedSearchPage($parts[1])){
	$_GET[RequestUtils::$REQUEST_TYPE]=RequestType::$PARAMETRIZED;
}else if(RequestUtils::isLandingPage($parts[0])){
	$_GET[RequestUtils::$REQUEST_TYPE]=RequestType::$LANDING_PAGE;
}else{
	if(!empty($parts[1]))
	{
		$parts[1] = intval($parts[1]);
		if($parts[1]<=0 || $parts[1]>10000)
		{
			$parts[1] =1;
		}
		$_GET['page']=$parts[1];
	}
	$_GET[RequestUtils::$REQUEST_TYPE]=RequestType::$NORMAL_SEARCH;
}
include_once "$xcart_dir/myntra/search.php";
?>
