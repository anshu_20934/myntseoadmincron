<?php
/*****************************************************************************\
+-----------------------------------------------------------------------------+
| X-Cart                                                                      |
| Copyright (c) 2001-2006 Ruslan R. Fazliev <rrf@rrf.ru>                      |
| All rights reserved.                                                        |
+-----------------------------------------------------------------------------+
| PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
| FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
| AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
|                                                                             |
| THIS  AGREEMENT  EXPRESSES  THE  TERMS  AND CONDITIONS ON WHICH YOU MAY USE |
| THIS SOFTWARE   PROGRAM   AND  ASSOCIATED  DOCUMENTATION   THAT  RUSLAN  R. |
| FAZLIEV (hereinafter  referred to as "THE AUTHOR") IS FURNISHING  OR MAKING |
| AVAILABLE TO YOU WITH  THIS  AGREEMENT  (COLLECTIVELY,  THE  "SOFTWARE").   |
| PLEASE   REVIEW   THE  TERMS  AND   CONDITIONS  OF  THIS  LICENSE AGREEMENT |
| CAREFULLY   BEFORE   INSTALLING   OR  USING  THE  SOFTWARE.  BY INSTALLING, |
| COPYING   OR   OTHERWISE   USING   THE   SOFTWARE,  YOU  AND  YOUR  COMPANY |
| (COLLECTIVELY,  "YOU")  ARE  ACCEPTING  AND AGREEING  TO  THE TERMS OF THIS |
| LICENSE   AGREEMENT.   IF  YOU    ARE  NOT  WILLING   TO  BE  BOUND BY THIS |
| AGREEMENT, DO  NOT INSTALL OR USE THE SOFTWARE.  VARIOUS   COPYRIGHTS   AND |
| OTHER   INTELLECTUAL   PROPERTY   RIGHTS    PROTECT   THE   SOFTWARE.  THIS |
| AGREEMENT IS A LICENSE AGREEMENT THAT GIVES  YOU  LIMITED  RIGHTS   TO  USE |
| THE  SOFTWARE   AND  NOT  AN  AGREEMENT  FOR SALE OR FOR  TRANSFER OF TITLE.|
| THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY GRANTED BY THIS AGREEMENT.      |
|                                                                             |
| The Initial Developer of the Original Code is Ruslan R. Fazliev             |
| Portions created by Ruslan R. Fazliev are Copyright (C) 2001-2006           |
| Ruslan R. Fazliev. All Rights Reserved.                                     |
+-----------------------------------------------------------------------------+
\*****************************************************************************/

#
# $Id: check_requirements.php,v 1.42.2.5 2006/06/29 07:34:57 max Exp $
#

#
# This script checks requirements
#

function func_html_entity_decode($string) {
	static $trans_tbl = false;

	if ($trans_tbl === false) {
		$trans_tbl = get_html_translation_table(HTML_ENTITIES);
		$trans_tbl = array_flip($trans_tbl);
	}
	return strtr($string, $trans_tbl);
}

function & ini_settings_storage() {
	static $settings = false;

	if (!is_array($settings) && function_exists('ini_get_all')) { # For PHP >= 4.2.0
		$a = ini_get_all();
		foreach ($a as $k=>$v) {
			$value = $v['local_value'];
			$value = func_html_entity_decode($value);
			$value = preg_replace('!Off!Si',false,$value);
			$value = preg_replace('!On!Si',true,$value);
			$value = str_replace("\x00","",$value);
			$settings[$k] = $value;
		}
	}

	if (!is_array($settings)) {
		ob_start();
		phpinfo(INFO_CONFIGURATION);
		$lines = explode("\n",ob_get_contents());
		ob_end_clean();
		foreach ($lines as $line) {
			if (preg_match('!<tr><td class="e">([^<]+)</td><td class="v">([^<]*)</td><td class="v">([^<]*)</td></tr>!Si', $line, $m)) {
				$m[2]=func_html_entity_decode($m[2]);
				$m[2]=preg_replace('!Off!Si',false,@$m[2]);
				$m[2]=preg_replace('!On!Si',true,@$m[2]);
				$m[2]=str_replace("\x00","",$m[2]);
				$settings[$m[1]] = $m[2];
			}
			else if (preg_match('!<td bgcolor="#ccccff"><b>([^<]+)</b><br[^>]*></td><td align="center">([^<]*)</td><td align="center">([^<]*)</td>!Si', $line, $m)) {
				$m[2]=func_html_entity_decode($m[2]);
				$m[2]=preg_replace('!Off!Si',false,@$m[2]);
				$m[2]=preg_replace('!On!Si',true,@$m[2]);
				$m[2]=str_replace("\x00","",$m[2]);
				$settings[$m[1]] = $m[2];
			}
			else if (preg_match('!(.+) => ([^ =]*) => (.*)$!S', $line, $m)) {
				$m[2]=preg_replace('!Off!Si',false,@$m[2]);
				$m[2]=preg_replace('!On!Si',true,@$m[2]);
				$m[2]=str_replace("\x00","",$m[2]);
				$settings[$m[1]] = $m[2];
			}
		}
	}

	return $settings;
}

function ini_get_bool($param) {
	$settings =& ini_settings_storage();

	return isset($settings[$param]) ? $settings[$param] : false;
}
?>
