<?php
use giftcard\GiftCardMessage;
use giftcard\GiftCardOccasion;
use giftcard\GiftCard;
use giftcard\manager\MCartGiftCard;
use giftcard\manager\MCartGiftCardItem;
use enums\cart\CartContext;

require_once "./auth.php";

include_once "$xcart_dir/include/class/class.mymyntra.php";
include_once "$xcart_dir/include/class/mcart/class.MCart.php";
include_once "$xcart_dir/include/class/mcart/class.MCartUtils.php";

if(!$giftingEnabled){
	include_once $xcart_dir."/include/func/func.core.php";
	func_header_location($https_location,false);	
}

$login = $XCART_SESSION_VARS['login'];
if(empty($login)){
	//Page to be accessed only if logged in else redirect to giftards page
	func_header_location($http_location."/giftcards",false);
}
$type = 0;
$quantity = 1;
$set_as_profile_name = filter_input(INPUT_POST,'set_as_profile_name',FILTER_SANITIZE_STRING);
$sender_name = filter_input(INPUT_POST,'sender_name',FILTER_SANITIZE_STRING);
$amount = filter_input(INPUT_POST, 'amount', FILTER_SANITIZE_STRING);
$recipient_email = filter_input(INPUT_POST, 'recipient_email', FILTER_SANITIZE_STRING);
$recipient_name = filter_input(INPUT_POST, 'recipient_name', FILTER_SANITIZE_STRING);
$message = filter_input(INPUT_POST, 'message', FILTER_SANITIZE_STRING);
$occasion_id = filter_input(INPUT_POST, 'occasion_id', FILTER_SANITIZE_STRING);

if(empty($amount)){
	func_header_location($http_location."/giftcards",false);
}

if($amount < $giftingLowerLimit || $amount > $giftingUpperLimit){
	func_header_location($http_location."/giftcards",false);
}

//Get User profile data from mymyntra functions
$mymyntra = new MyMyntra(mysql_real_escape_string($login));
$userProfileData = $mymyntra->getBasicUserProfileData($login);
$sender_name_profile = $userProfileData["firstname"] . " " . $userProfileData["lastname"];
if(empty($sender_name_profile) && $set_as_profile_name){
//if($set_as_profile_name){
	$namearr = MyMyntra::splitFirstLastName($sender_name);
	if (!empty($namearr) && $set_as_profile_name){
	    $data = array (
	    	'firstname' => mysql_real_escape_string($namearr[0]),
		    	'lastname' => mysql_real_escape_string($namearr[1])
		);
   		$actionResult = $mymyntra->updateProfile($data);
	}
}

$mcartFactory= new \MCartFactory();
$myCart= $mcartFactory->getCurrentUserCart(true, CartContext::GiftCardContext);

$giftCard = new GiftCard();
$giftCard->setAmount($amount);
$giftCard->setQuantity($quantity);
$giftCard->setType($type);
$giftCard->setSenderEmail($login);
$giftCard->setRecipientEmail($recipient_email);
$giftCard->setSenderName($sender_name);
$giftCard->setRecipientName($recipient_name);
$giftCardMessage = new GiftCardMessage();
$giftCardMessage->setMessage($message);
$giftCardOccasion = new GiftCardOccasion();
$giftCardOccasion->setId(intval($occasion_id));
$giftCardMessage->setGiftCardOccasion($giftCardOccasion);
$giftCard->setGiftCardMessage($giftCardMessage);

$myCart->clear();
$mcartItem= new MCartGiftCardItem($amount, $quantity);
$mcartItem->setGiftCardDetails($giftCard);
$myCart->addProduct($mcartItem, false, false);

MCartUtils::persistCart($myCart);

func_header_location("$https_location/mkgiftcardpaymentoptions.php");

exit;
?>
