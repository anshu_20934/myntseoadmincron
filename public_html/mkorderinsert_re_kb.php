<?php
if(file_exists("./auth.php")) {
	include_once("./auth.php");
} else if (file_exists("../auth.php")) {
	include_once("../auth.php"); // happens for secure pages
} 
include_once("$xcart_dir/include/class/instrumentation/BaseTracker.php");
$tracker = new BaseTracker(BaseTracker::ORDER_INSERT);
$tracker->fireRequest();			

include_once("$xcart_dir/include/func/func.mk_orderbook.php");
include_once("$xcart_dir/include/func/func.order.php");
include_once("$xcart_dir/include/func/func.mkcore.php");
include_once("$xcart_dir/include/func/func.mkspecialoffer.php");
include_once("$xcart_dir/mkstatesarray.php");

include_once ("$xcart_dir/modules/coupon/database/CouponAdapter.php");
include_once ("$xcart_dir/modules/coupon/CouponValidator.php");

include_once ("$xcart_dir/include/class/search/UserInterestCalculator.php");
include_once ("$xcart_dir/include/class/class.feature_gate.keyvaluepair.php");
include_once ("$xcart_dir/include/class/PaymentGatewayDiscount.php");
require_once ("$xcart_dir/icici/ajax_bintest.php");
include_once ("$xcart_dir/include/class/mcart/class.MCart.php");
require_once ("$xcart_dir/axis/helperAPI.php");
include_once ("$xcart_dir/include/class/netbankingHelper.php");

require_once ("$xcart_dir/libfuncs.php3");
x_load('crypt','mail','user');

function determineCardType($number) {
	// Based on code from here 
	// http://mel-green.com/2008/11/determine-credit-card-type-with-javascript/		
	if(preg_match('/^4/', $number)) {
		return 'VISA';
	} elseif(preg_match('/^5[1-5]/', $number)) {
		return 'MC';
	} elseif(preg_match('/^3(?:0[0-5]|[68])/', $number)) {
		return 'DINERS';
	} else {
		return 'MAEST';
	}
}

function getNetBankingGateway($gateways) {
	//expecting a array having name,weight and bankcode
	if(!is_array($gateways)) return null;
	$arrayElement = array();
	$weightarray = array();
	foreach($gateways as $gateway){
		$weightarray[] = $gateway['weight'];
		$arrayElement[] = array('name' =>$gateway['name'],'gateway_code' => $gateway['gateway_code'] );
	}
	return selectRandomArrayElement($arrayElement,$weightarray);
}


function selectRandomArrayElement($arrayElemets,$weight) {
	if(!is_array($arrayElemets) || !is_array($weight)) return null;
	
	$weightSum = array();
	$weightSum[0]=$weight[0];
	$weightLenght = count($weight);
	for($i=1;$i<$weightLenght;$i++) {
		$weightSum[$i] = $weightSum[$i-1] + $weight[$i];	
	}	
	$k=mt_rand(0, $weightSum[$weightLenght-1]);
	
	for ($i = 0; $k > $weightSum[$i]; $i++) ;
	
	return $arrayElemets[$i];
}


class CartManager {
	private $cart, $productsInCart, $productIds, $cartAmount;
	
	private $subTotal, $totalQuantity, $discountRationSum, $productTypeQuantityArray;
	
	public function __construct() {
		$mcartFactory = new MCartFactory();
		$this->cart = $mcartFactory->getCurrentUserCart();
		if($this->cart != null) {
			$this->productsInCart = MCartUtils::convertCartToArray($this->cart);
			$this->productIds = $this->cart->getProductIdsInCart();
			$this->initParams();
		}		
	}
	
	public function getCart() {
		return $this->cart;
	}
	
	public function getProductsInCart() {
		return $this->productsInCart;
	}
	
	public function getProductIds() {
		return $this->productIds;
	}
	
	public function getCartAmount() {
		return $this->cartAmount;
	}
	
	public function getProductTypeQuantityArray() {
		return $this->productTypeQuantityArray;
	}
	
	protected function initParams() {
		
		$this->cartAmount = XSessionVars::getNumber("amount");
		
		$this->discountRatioSum = 0;
		$this->productTypeQuantityArray = array();
		foreach($this->productsInCart AS $key => $productDetail) {
			//Find the sum of discount ratio
			if($productDetail['discount'] == 0)
				$this->discountRatioSum += $productDetail['totalPrice'];
			$this->subtotal += $productDetail['totalPrice'];
			$this->totalQuantity +=  $productDetail['quantity'];
		
			//Create an array for quantities of product type 
			if(array_key_exists($productDetail['producTypeId'], $this->productTypeQuantityArray)) {
				$this->productTypeQuantityArray[$productDetail[producTypeId]]['qty'] += $productDetail['quantity'];
				$this->productTypeQuantityArray[$productDetail[producTypeId]]['stylecount']++;   
			}
			else {
				$this->productTypeQuantityArray[$productDetail[producTypeId]]['qty'] = $productDetail['quantity'];
				$this->productTypeQuantityArray[$productDetail[producTypeId]]['stylecount'] = 1;
			}
		}
	}
	
	/**
	 * If cart is empty, redirect to cart pages
	 * @param unknown_type $productsInCart
	 */
	public function handleEmptyCart() {
		if(empty($this->productsInCart)) {
			//stale cart , cart is made empty from another tab/window - cannot proceed. Rebuild the cart again
			func_header_location("$http_location/mkmycart.php?at=c&pagetype=productdetail&src=po");
		}
	}
}

class ShippingManager {

	protected $logger, $login;
	protected $cartManager;
	private $address, $logisticId;
	public function __construct($cartManager, $logger) {
		global $XCART_SESSION_VARS, $_POST;
		$this->logger = $logger;
		$this->login = $XCART_SESSION_VARS['login'];
		$this->cartManager = $cartManager;
		$this->logisticId = mysql_real_escape_string($_POST["s_option"]);
	}
	
	
	/**
	 * Get Shipping address from the request. If its empty, then ship to default address. If that's also not present, redirect to customer address page
	 * It may return address if things are fine. Else it may just redirect to address page or system-error page
	 */
	public function getShippingAddress() {
		global $_POST;		
		$address = mysql_real_escape_string($_POST['address']);
		if(empty($address)) {
			$check = func_select_first('mk_customer_address', array('login' => $this->login,'default_address'=>1));
		    if(!empty($check)){
		    	$address = $check['id'];
		    } 
		    else {
				$address = func_select_first('mk_customer_address', array('login' => $this->login),0,1,array('datecreated' => 'desc'));
				if(!empty($address)) {
					$address = $address['id'];
			    } 
			    else {        	
			        //no address present take him to address page
			       func_header_location($https_location . "/mkcustomeraddress.php?change=true",false);
			    }	    	
		    }     
		}
		$this->address = func_select_first('mk_customer_address', array('id' => $address));
		$this->handleBlankAddress($this->address);
		return $this->address;
	}
	
	public function getLogisticId() {
		return $this->logisticId;
	}
	
	public function getShippingRate() {
		$adrs = $this->address;
		//Kundan: we no longer have free shipping coupons supported, so passing coupon as null in shipping rate calculator 
		$shippingRateArray = func_shipping_rate_re($adrs['country'],$adrs['state'],$adrs['city'], 
								$this->cartManager->getProductsInCart(), $this->cartManager->getProductIds(), null, 
								$this->logisticId, $adrs['zipcode'], $this->cartManager->getcartAmount());
		// restore values from session
		$thisShippingRate = $shippingRateArray['totalshippingamount'];
		return $thisShippingRate;
	}
	
	protected function handleBlankAddress($address) {
		$s_country = $address['country'] ;
		$s_state = $address['state'] ;
		$s_city = $address['city'] ;

		//validate shipping address
		if($s_country=="" || $s_state=="" || $s_city=="") {
			$errormessage = "Shipping address missing due to session time out. Please try checkout out your cart again. We apologize for the inconvenience.";
			x_session_register("errormessage");
			$this->logger->log("**************The shipping address has not been recorded. The session is probably lost.  Refer to session dump above form more details.\n");
			$this->logger->close();
		  	header("Location:mksystemerror.php");
			exit;
		}
	}
}

class OrderManager {
	protected $logger, $login;
	public function __construct($logger) {
		global $XCART_SESSION_VARS;
		$this->logger = $logger;
		$this->login = $XCART_SESSION_VARS['login'];
	}
	
	/**
	 * If login is empty, handle the error and redirect
	 */
	public function handleEmptyLogin() {
		if(empty($this->login)) {
			$errormessage = "We do not have your login due to session timeout. We apologize for the inconvenience. Your items are still in the cart. Please try checking out again from your cart";
			x_session_register("errormessage");
			$this->logger->log("**************Surprisingly session has been lost. Its a pity, but it has happened. Refer to session dump above form more details.\n");
			$this->logger->close();
			func_header_location($http_location,false);
		}	
	}
	
	public function isCODOrder() {
		global $_POST;
		return ($_POST['pm'] === 'cod');
	}
	
	public function getCODCharge($address, $cartManager) {
		$cod_charge = 0;
		if($this->isCODOrder()) {
			global $XCART_SESSION_VARS;
			$totalAmount = $XCART_SESSION_VARS["totalAmount"];
			$codElegiblity = verifyCODElegiblityForUser($this->login, $address['mobile'], $address['country'], $totalAmount, strtolower($address['pincode']), 
								$cartManager->getProductsInCart(), $cartManager->getProductIds()
							 );
			$codErrorCode = $codElegiblity['errorCode'];
			
			if($codErrorCode!=0) {
				//user not eligible for cod anymore	as conditions not satisfied.
				func_header_location($https_location . "/mkpaymentoptions.php?address=".$address['id'],false);		
			}
			$codChargeFG = FeatureGateKeyValuePairs::getInteger('codCharge');
			if($codChargeFG>0) {
				$cod_charge = $codChargeFG;
			}
		}
		return $cod_charge;
	}
	
	public function handleGiftOrder() {
		global $XCART_SESSION_VARS;
		if(x_session_is_registered("giftdata")) {
			x_session_unregister("giftdata");
		}
		if($_POST['asgift']) {
			$giftdata = array();
			x_session_register("giftdata");
			$giftdata['asgift'] = "Y";
			$giftdata['gmessage'] = $_POST['gmessage'];
			$giftdata['giftpack'] = "Y";
			$giftdata['giftamount'] = 20;//irrespective of boxcount gift charge is 20rs/order 
			$XCART_SESSION_VARS['giftdata'] = $giftdata;		
		}
	}
	public function getOrderString() {		
		global $_POST;
		$text = "";
	
		foreach($_POST AS $key=>$value) {
			switch ($key) {
				case 'totalquantity': $fieldvalue = sanitize_int(strip_tags($value)); break;
				//Kundan: Warning: thisTotalAmount, thisAmount are never set in code, so they're actually useless
				case 'shippingrate': $fieldvalue = sanitize_float(strip_tags($thisShippingRate)); break;
				case 'totalamount': $fieldvalue = sanitize_float(strip_tags($thisTotalAmount)); break;
				
				case 'Amount': 
				case 'hideref':
				case 'hidevat':
				case 'hidetotamount': $fieldvalue = sanitize_float(strip_tags($value)); break;
				
				case 's_address': 
				case 'b_address': $fieldvalue = mysql_real_escape_string(str_ireplace("\r\n"," ",$value)); break;
				
				case 's_option': 
				case 's_email':
				case 'b_email':	$fieldvalue = $value; break;
				default: $fieldvalue = sanitize_paranoid_string(strip_tags($value));
				break;
			}
			if($text == "") {
				$text .= "$key=>".$fieldvalue;
			} 
			else {
				$text .= "||$key=>".$fieldvalue;
			}
		}		
		$ship2diff = $_POST['billtoship'] ? 'Y' : 'N';
		$text = $text."||ship2diff=>".$ship2diff;
		return $text;
	}

	/**
	 * Kundan: create a hash out of these things:
	 * Cart, Total, CashCouponCode, CouponCode, Shipping Address 
	 * $address, $cashCouponCode, $couponCode, $productsInCart, $myCart, $login
	 */		
	public function getOrderCRC($productsInCart, $login, $couponCode, $cashCouponCode, $address) {
		$arrForCRC = array($productsInCart, $login, $couponCode, $cashCouponCode, $address);
		return crc32(var_export($arrForCRC, true));
	}
	
	/** 
	 * Want to re-use the same order id to the extent possible
	 * If order_crc has not changed from the last(latest) payment attempt by this user, then reuse this order-id
	 * and this order was inserted in last 30 minutes (30*60 seconds)
	 * then reuse this order-id
	 * @kundan
	 */
	public function getOrderId($productsInCart, $login, $couponCode, $cashCouponCode, $address) {
		
		$orderCRC = $this->getOrderCRC($productsInCart, $login, $couponCode, $cashCouponCode, $address);
		
		$last30Mins = (60*30);//seconds
		
		$findExistingOrderSql = "select max(ord.orderid) ".
								"from mk_temp_order tmpord ".
								"inner join xcart_orders ord on ord.orderid = tmpord.orderid ".
								"inner join mk_payments_log pmt on pmt.orderid = tmpord.orderid ".
								"where ".
								"unix_timestamp(now()) - pmt.time_insert < ($last30Mins) and ".
								"tmpord.order_crc = $orderCRC ".
								"and ord.status = 'PP' ".
								"and ord.login = '".$login."'";
		
		$existingOrderid = func_query_first_cell($findExistingOrderSql);
		$reuseOrderid = !empty($existingOrderid);
		
		if($reuseOrderid) {
			$Order_Id = $existingOrderid;
		  	$this->logger->log("Retrieved orderId from mk_payments_log. Order Id is - ".$Order_Id."\n");
		}
		else {
			global $XCARTSESSID;
			$text = $this->getOrderString();
			$checksumkey = uniqid();
			$session_id = $XCARTSESSID;
			// orignal code
			$tempOrderQuery = "INSERT INTO mk_temp_order(sessionid, parameter,checksumkey, order_crc) VALUES('$session_id', '$text','$checksumkey', $orderCRC)";
			$this->logger->log("Inserting order into temp order table. Query is - ".$tempOrderQuery."\n");
			db_query($tempOrderQuery);
			// #1496:remove parameter from where clause and add checksumkey
			$retrieveTempOrderQuery = "SELECT max(orderid) FROM mk_temp_order WHERE sessionid='$session_id' AND checksumkey='$checksumkey' and order_crc = ".$orderCRC;
			
			$this->logger->log("Retrieving orderId from mk_temp_order. Query is - ".$retrieveTempOrderQuery."\n");
			$Order_Id = func_query_first_cell($retrieveTempOrderQuery);
			$this->logger->log("Retrieved orderId from mk_temp_order. Order Id is - ".$Order_Id."\n");
		}
		return $Order_Id;		
	}
}

class DiscountManager {
	private $login;
	private $adapter, $validator;
	private $couponCode, $cashCouponCode;
	private $couponDiscount, $couponPercent; 
	private $cashRedeemed;
	private $productDiscount;
		
	public function __construct() {
		global $XCART_SESSION_VARS;
		
		$this->login = XSessionVars::getString('login');
		$this->couponCode = XSessionVars::getString('couponCode');
		$this->cashCouponCode = XSessionVars::getString('cashCouponCode');

		//--Coupon Discount and Coupon Code
		$this->couponDiscount = XSessionVars::getNumber('coupondiscount', 0);
		$this->couponPercent = XSessionVars::getString('couponpercent');
		if(!isset($this->couponDiscount)) {
			$this->couponCode = '';
			$this->couponPercent = '';
		}
		
		//--Cash Coupon Discount and Cash Coupon Code
		$this->cashRedeemed = XSessionVars::getNumber('cashdiscount', 0);
		if($this->cashRedeemed === 0) {
			$this->cashCouponCode = "";
		}

		//--Product Discount
		$this->productDiscount = XSessionVars::getNumber('productDiscount', 0.0);
		
		if(!empty($this->couponCode) || !empty($this->cashCouponCode)) {
			$this->adapter = CouponAdapter::getInstance();
			$this->validator = CouponValidator::getInstance();
		}
	}
	
	public function getCouponCode() {
		return $this->couponCode;
	}
	
	public function getCashCouponCode() {
		return $this->cashCouponCode;
	}
	
	public function getCashRedeemed() {
		return $this->cashRedeemed;
	}
	
	public function getCouponDiscount() {
		return $this->couponDiscount;
	}
	
	public function getCouponPercent() {
		return $this->couponPercent;
	}
	
	public function getProductDiscount() {
		return $this->productDiscount;
	}
	
	public function lockCoupons() {
		if(!empty($this->couponCode)) {
			# Update the number of times the coupon has been used once the order is placed
			$this->adapter->lockCouponForUser($this->couponCode, $this->login);
		}	
		if(!empty($cashCouponCode)) {
            # Update the number of times the coupon has been used once the order is placed
            $this->adapter->lockCouponForUser($this->cashCouponCode, $this->login);
        }
	}
	
	protected function handleCouponValidity($couponCode) {
		global $weblog;
		if(empty($couponCode))
			return;
	    $couponMessage = "";
	
	    // Find whether the coupon is still valid for the user.
	    $isValid = false;
		try {
	            $coupon = $this->adapter->fetchCoupon($couponCode);
	            try{
	            	$isValid = $this->validator->isCouponValidForUser($coupon, $this->login);
	            }
	            catch (CouponLockedException $ex){
		            $unblocked = findAndCancelPPOrderBlockingCoupon($this->login,"DISCOUNT",$couponCode);
		            
		            if($unblocked){
		            	$coupon = $this->adapter->fetchCoupon($couponCode);
		            	$isValid = $this->validator->isCouponValidForUser($coupon, $this->login);
		            }
		            else {
		            	throw $ex;
		            }
	            }
	            catch (CouponLockedForUserException $ex){
		            $unblocked = findAndCancelPPOrderBlockingCoupon($this->login,"DISCOUNT",$couponCode);
		            if($unblocked){
		            	$coupon = $this->adapter->fetchCoupon($couponCode);
		            	$isValid = $this->validator->isCouponValidForUser($coupon, $this->login);
		            }
		            else {
		            	throw $ex;
		            }
	            }
		}
		catch (CouponException $ex) {
		    $couponMessage = $ex->getMessage();
		    $couponMessage = "<font color=\"red\">$couponMessage</font>";
		}
	    
		//
		// If coupon is invalid, then clean up the coupon 
		// and redirect back to the cart page.
		//
		if (!$isValid) {
		    // XXX: Must find a way to notify the user.
		    // Currently, we are displaying the couponMessage.
		    $redirectedToCart = true;
		    x_session_register("redirectedToCart");
		    $weblog->info("Redirecting back to the payment page. Reason: $couponMessage");
		    func_header_location($https_location . "/mkpaymentoptions.php",false);
		}
	}
	
	public function handleCashbackAndCouponValidity() {
		$this->handleCouponValidity($this->couponCode);
		$this->handleCouponValidity($this->cashCouponCode);
	}
}

class PaymentManager {
	public $paymentMethod;
	function __construct() {
		global $XCART_SESSION_VARS;
		$this->paymentMethod = $XCART_SESSION_VARS['pm'];
	} 
	
	public function getPaymentOption() {
		switch ($this->paymentMethod) {
			case 'otherpayments': 
				global $_POST;
				// Due to legacy this code has been hacked, If this code is removed .. reports will break
				// Basically $paybyoption has two possible values on and chq
				if($_POST['otherpaymentstype'] == 'checkdd') {
					$paymentOption = 'chq';
				}
				break;
			case 'cod':
			    // Pravin - COD Feature. Setting payment option to 'cod'. In xcart_orders, such orders will store payment_method as 'cod'
			    $paymentOption = 'cod';
			    // After SMS Verification is done, this status should be changed to 'OH' (on hold for 12 hours)
				break;
			case 'NP': 
				$paymentOption = 'NP';
				break;
			case 'ivr':
				$paymentOption = 'ivr';
				break;
			default:
				$paymentOption = 'on';				
		}
		return $paymentOption;
	}

	/**
	 * Default Channel is Web. In case of IVR payment, the channel is IVR
	 */
	public function getChannel() {
		return ($this->paymentMethod === 'ivr') ? 'ivr' : 'web';
	}

	/**
	 * In case of Cash on Delivery option, the initial order status should be set to PV which implies "Pending Verification"
	 * otherwise, default order status is PP: Pre-processed 
	 */
	public function getOrderStatus() {
		return ($this->paymentMethod === 'cod') ? 'PV' : 'PP';
	}

	public function getPaymentGatewayDiscount($paybyoption, $total, $amountWithProductDiscount) {
		if($paybyoption == 'debitcard' || $paybyoption == 'creditcards' ) {
		    global $HTTP_POST_VARS;
			$ccNumber = $HTTP_POST_VARS['card_number'];
		    $ccNumber = str_replace('-', '', $ccNumber);    // baseline single quote
			$ccNumber = str_replace(' ', '', $ccNumber);    // baseline single quote
			$bin = substr($ccNumber, 0,6);
			$pg_discount = PaymentGatewayDiscount::getDiscount($total, $amountWithProductDiscount,$bin);
		} 
		elseif($paybyoption == 'netbanking') {
			$activeBanks = NetBankingHelper::getActiveBanksList();
			$bank_id = $_POST['netBankingCards'];
			$bankName = $activeBanks[$bank_id]['name'];
			if(stripos($bankName, "icici") !== false) { //icici bank
				$icici_discount = FeatureGateKeyValuePairs::getInteger('payments.iciciBankCardDiscount');
				if($icici_discount>0) {				
					$pg_discount = $total*$icici_discount/100;
					$pg_discount = round($pg_discount, 2);			
				}
			}
		}
		return $pg_discount;
	}
}


class UnusedParameters {
	
	/**
	 * @deprecated
	 */
	private $membership, $source_id, $ammaGUID, $shopid, $affiliateId, $afforderid, $affdiscount, $orgId, $refferalAmt;   
	
	private static $instance;
	
	public static function getInstance() {
		if(empty(self::$instance)) {
			self::$instance = new self();	
		}
		return self::$instance;
	}
	
	private function __construct() {
		$this->membership = '';
		$this->source_id = RequestVars::getVar("user_source_id");
		$this->shopid = RequestVars::getVar("shopid", 0);
		$this->ammaGUID = RequestVars::getVar("ammaGUID", 0);
		$this->affiliateId = RequestVars::getVar("affiliateId", 0);
		$this->affdiscount = RequestVars::getVar("affdiscount", 0);
		$this->afforderid = RequestVars::getVar("afforderid", 0);
		$this->orgId = RequestVars::getVar("orgId", 0);
		$this->refferalAmt = RequestVars::getGetVar("refferalAmt", 0);
		
	}
	
	public function getInsertParams() {
		return array (
			"membership" => $this->membership,
			"source_id" => $this->source_id,
			"shopid" => $this->shopid,
			"guid" => $this->ammaGUID,
			"affiliateid" => $this->affiliateId,
			"aff_discount" => $this->affdiscount,
			"aff_orderid" => $this->afforderid,
			"orgid" => $this->orgId,
			"ref_discount" => $this->refferalAmt,
		);
	}
}

class OrderInsertManager {
	private $logManager, $orderManager, $cartManager, $discountManager, $shippingManager, $paymentManager;
	private $unusedParamsManager;
	
	public function __construct() {
		$this->logManager = new OrderLogger();
		
		$this->orderManager = new OrderManager($this->logManager);
		$this->cartManager = new CartManager();
		
		$this->discountManager = new DiscountManager();
		
		$this->shippingManager = new ShippingManager($cartManager, $logger);
		
		$this->paymentManager = new PaymentManager();
	}
	
}


//+++++++++++++++ Order Log File for debugging ++++++++++++++//
$mgr = new OrderInsertManager();

$logger = new OrderLogger();
$logger->logSession();

$orderManager = new OrderManager($logger);

//++++++++++++++ If login is empty, handle the error and redirect ++++++++++++//
$orderManager->handleEmptyLogin();

$cartManager = new CartManager();
$productsInCart = $cartManager->getProductsInCart();
$productids = $cartManager->getProductIds();

//++++++++++++++ If cart is empty, redirect +++++++++++++++
$cartManager->handleEmptyCart();

$login = $XCART_SESSION_VARS['login'];

//+++++++++++++ Check if coupon codes are invalid ++++++++++++++
$discountManager = new DiscountManager();
$couponCode = $discountManager->getCouponCode();
$cashCouponCode = $discountManager->getCashCouponCode();
$discountManager->handleCashbackAndCouponValidity();

//+++++++++++++ Handle empty shipping address issues and get it if all is well! +++++++++++++
$shippingManager = new ShippingManager($cartManager, $logger);
$address = $shippingManager->getShippingAddress();
$thisShippingRate = $shippingManager->getShippingRate();

$s_country = $address['country'] ;
$s_state = $address['state'] ;
$s_city = $address['city'] ;
$s_zipcode = strtolower($address['pincode']);
$s_email=$address['email'];

$cod_charge = $orderManager->getCODCharge($address, $cartManager);
//$shippingoptions = func_load_shipping_options();
$logisticId = $shippingManager->getLogisticId();

$cart_amount = $cartManager->getCartAmount();


//Handle sending of item as gift
$orderManager->handleGiftOrder();

$session_id = $XCARTSESSID;

$orderid = $orderManager->getOrderId($productsInCart, $login, $couponCode, $cashCouponCode, $address);

############ START ENTER ORDER DETAILS IN THE SYSTEM IN PREPROCESSED STATE########

$paymentManager = new PaymentManager();
$channel = $paymentManager->getChannel();
$orderstatus = $paymentManager->getOrderStatus();
$paymentOption = $paymentManager->getPaymentOption();


//Database tables
global $state_array;
$tb_order_details = $sql_tbl['order_details'];
$tb_products = $sql_tbl['products'];
$tb_order_options= $sql_tbl['mk_product_options_order_details_rel'];
$tb_order_customization_detail = $sql_tbl['mk_xcart_order_details_customization_rel'];
$tb_order_image_order_detail =   $sql_tbl['mk_xcart_order_customized_area_image_rel'];
$tb_orders = $sql_tbl["orders"];
$tb_giftcerts =  $sql_tbl["giftcerts"];
$tb_customer =  $sql_tbl["customers"];
$tb_temporder =  $sql_tbl["temp_order"];
$tb_wishlist = $sql_tbl['wishlist'];
$tb_product_options=$sql_tbl['mk_product_options'];
$tb_mk_order_item_option_quantity=$sql_tbl['mk_order_item_option_quantity'];

$giftdata = $XCART_SESSION_VARS['giftdata'];
#
#load gift message information
#
if($giftdata['asgift'] == 'Y') {
	$giftstatus = $giftdata['asgift'];
	$giftmessage = addslashes($giftdata['gmessage']);
	$giftpack = $giftdata['giftpack'];
	$giftamount = $giftdata['giftamount'];
}
else {
	$giftstatus = "N";
	$giftpack = "N";
	$giftamount = 0;
}

#Call function to update and return the array of billing and shipping address

$weblog->info("Called func_update_billing_shipping_address for updating the billing and shipping address") ;
$customer = func_update_billing_shipping_address($login, $orderid, $session_id);
$weblog->info("func_update_billing_shipping_address return an array of billing and shipping address");


//Computing the final amount to be paid
#########################################################

$productTypeQuantityArray = $cartManager->getProductTypeQuantityArray();

####################### start insert order details into database #################################
		
$time =time();
$firstname = mysql_escape_string($address['name']);
$lastname = "";

$shipping_option_name = $customer['s_option'];
$saddress = mysql_escape_string(str_ireplace("\r\n"," ", $address['address']));
$scountry = ($address['country'] == 'In')?'India':$customer['s_country'];
$scity = mysql_escape_string($address['city']);
$s_state = mysql_escape_string($address['state']);
$s_zipcode = $address['pincode'];
$phone = $address['phone'];
$mobile =$address['mobile'];
    
// Below fields are not available any more ..
$baddress = mysql_escape_string($customer['b_address']);
$bcountry = mysql_escape_string(($customer['b_country'] == 'In')?'India':$customer['b_country']);
$bcity = mysql_escape_string($customer['b_city']);
$b_state = mysql_escape_string($customer['b_state']);
$b_zipcode = mysql_real_escape_string($customer['b_zipcode']);

$issues_contact_number = $mobile; 

$shipment_preferences = mysql_real_escape_string($HTTP_POST_VARS['shipment_preferences']);

$mrp = $XCART_SESSION_VARS["mrp"];
// mrp is equal to vat + amount ( back calculated in cart page )
$vat = $XCART_SESSION_VARS["vat"];
$amount = $XCART_SESSION_VARS["amount"];

$coupondiscount = $discountManager->getCouponDiscount();
$couponCode = $discountManager->getCouponCode();
$couponpercent = $discountManager->getCouponPercent();

$cash_coupon_code = $discountManager->getCashCouponCode();
$cashredeemed = $discountManager->getCashRedeemed();
$productDiscount = $discountManager->getProductDiscount();

$cod_payment_status='';

$pg_discount = 0; // field to use for payment gateway discounts like citi card discount / online discounts / anyother payment page discount
$final_total_amount = $amount - $coupondiscount + $cod_charge + $thisShippingRate + $giftamount - $cashredeemed - $productDiscount; // used to pass on to ccavenue
$amountWithProductDiscount = $amount - $productDiscount; //use to divide payment gateway discount (pg_discount) propotionally in for xcart_order_details table.      
$subtotal = $amount;
$total = $subtotal-$coupondiscount - $cashredeemed - $productDiscount;
$total = $total < 0 ? 0 : $total;

$pg_discount = $paymentManager->getPaymentGatewayDiscount($paybyoption, $total, $amountWithProductDiscount);

$final_total_amount = $final_total_amount - $pg_discount;
$total = $total - $pg_discount;
$final_total_amount = number_format($final_total_amount, 2, ".", '');
$total = number_format($total, 2, ".", '');
$final_total_amount = $final_total_amount < 0 ? 0 : $final_total_amount;
$total = $total < 0 ? 0 : $total;

/* Pravin Cleanup: All useless fields being set to 0, so that insert query does not break. */ 
if(empty($affiliateId))
	$affiliateId = 0;
if(empty($paymentOptionId))
	$paymentOptionId =0;
if(empty($refferalAmt))
	$refferalAmt = 0;
if(empty($shopid))
	$shopid=0;
if(empty($ammaGUID))
	$ammaGUID = 0;
if(empty($affdiscount))
	$affdiscount =0 ;
if(empty($afforderid))
	$afforderid =0 ;
if(empty($orgId))
		$orgId =0 ;
/* End of useless data reset */


array (
	"orderid" => $orderid,
	"status" =>  $orderstatus,
	"date" => $time,
	"payment_method" => $paymentOption,
	"channel" => $channel,
	"order_name" => $user_source_code."-".$orderid
);

array (
	"shippingid" => 0,
	"shipping_method" => $shipping_option_name,
	"courier_service" => $shipping_option_name,
	"shipment_preferences" =>  $shipment_preferences,
	"warehouseid" => 1
);



array (
	"giftcert_discount" => 0.0,
	"giftcert_ids" => '',
	"details" =>  '',
	"customer_notes" => $notes,
	"extra" => '',
	"flag" => 'N',
	"additional_info" => $additional_info,
	"request_server" => $server_name,
	"tracking" => ''
);

array (
	"gift_status" => $giftstatus,
	"gift_pack" => $giftpack,
	"gift_charges" => $giftamount,
	"notes" => $giftmessage
);

array (
	"cod_payment_date" => NULL,
	"cod_pay_status" => NULL ,
	"cod" => $cod_charge
);

array (
	"b_title" => $customer['b_title'],
	"b_firstname" => $customer['b_firstname'],
	"b_lastname" =>$customer['b_lastname'],
	"b_address" => mysql_escape_string($customer['b_address']),
	"b_city" => mysql_escape_string($customer['b_city']),
	"b_state" => mysql_escape_string($customer['b_state']),
	"b_country" => mysql_escape_string(($customer['b_country'] == 'In')?'India':$customer['b_country']),
	"b_zipcode" => mysql_real_escape_string($customer['b_zipcode']),
	"b_mobile" => $address['mobile'],
	"b_email" => $b_email
);

array (
	"s_title" => $customer['s_title'],
	"s_firstname" => $firstname,
	"s_lastname" => $lastname,
	"s_address" => $saddress,
	"s_city" => $scity,
	"s_state" => $s_state,
	"s_country" => $s_country,
	"s_zipcode" => $s_zipcode,
	"s_email" => $s_email
);

array (
	"login" => $login,
	"customer" => $login,
	"title" => $customer['title'],
	"firstname" => $firstname,
	"lastname" => $lastname,
	"company" => $customer['company'],
	"phone" => $phone,
	"fax" => $customer['fax'],
	"url" => $customer['url'],
	"email" =>   $loginEmail,
	"mobile" => $mobile,
	"issues_contact_number" => $issues_contact_number
);

array (
	"total" => $total,
	"subtotal" => $subtotal,
	"discount" => $productDiscount,
	"shipping_cost" => $thisShippingRate,
	"tax" => $vat,
	"taxes_applied" => $taxApplied
);

array (
	"pg_discount" => $pg_discount,
	"coupon" => $couponCode,
	"coupon_discount" => $coupondiscount,
	"cash_redeemed" => $cashredeemed,
	"cash_coupon_code" => $cash_coupon_code
);
		
		
$query = "insert into $tb_orders
			(orderid, login,membership,total,giftcert_discount,giftcert_ids,subtotal, discount,coupon,
			coupon_discount,shippingid,tracking,shipping_cost,tax,taxes_applied,date,status,payment_method,
			flag,details,customer_notes,notes,extra,customer,title,firstname,lastname,company,

			b_title,b_firstname,b_lastname,b_address,b_city,b_state,b_country,b_zipcode,b_mobile,b_email,
			s_title,s_firstname,s_lastname,s_address,s_city,s_state,s_country,s_zipcode,s_email,
			phone,fax,url,email,paymentid, shipping_method,courier_service,mobile,gift_status,gift_pack,ref_discount, gift_charges, 
			affiliateid, cod,shopid,guid,aff_discount,aff_orderid,orgid,issues_contact_number,shipment_preferences,additional_info,order_name,source_id, 
			cod_payment_date, cod_pay_status,cash_redeemed,cash_coupon_code,pg_discount,channel, request_server, warehouseid)
			values 
			('$orderid', '$login','','$total',0.0,'','$subtotal','$productDiscount','$couponCode',
			'$coupondiscount',0,'','$thisShippingRate','$vat','$taxApplied','$time','$orderstatus','$paymentOption',
			'N','','$notes','$giftmessage','','$login','".$customer['title']."','".$firstname."','".$lastname."','".$customer['company']."',
			'".$customer['b_title']."','".$customer['b_firstname']."','".$customer['b_lastname']."','".$baddress."','".$bcity."','".$b_state."','".$bcountry."','".$b_zipcode."','{$mobile}','{$b_email}',
			'".$customer['s_title']."','".$firstname."','".$lastname."','".$saddress."','".$scity."','".$s_state."','".$s_country."','".$s_zipcode."','{$s_email}',
			'".$phone."','".$customer['fax']."','".$customer['url']."','".$loginEmail."','$paymentOptionId', '".$shipping_option_name."', '".$shipping_option_name."','".$mobile."','".$giftstatus."','".$giftpack."','".$refferalAmt."','".$giftamount."', 
			'".$affiliateId."', '".$cod_charge."','".$shopid."','".$ammaGUID."','".$affdiscount."','".$afforderid."','".$orgId."','".$issues_contact_number."','".$shipment_preferences."','".$additional_info."','".$user_source_code.'-'.$orderid."','".$user_source_id."', 
			NULL, NULL ,'$cashredeemed','$cash_coupon_code','$pg_discount','$channel', '$server_name', 1)";   // Pravin - COD columns added.. (1) cod_payment_date, (2) cod_pay_status

//++++++++ log order query +++++++++
if ($shouldLog) fputs($ORDERLOG,"INSERTING INTO ORDER TABLE - Query is :\n\t".$query."\n");
	$weblog->info("ICICI PROMO CODE check for orderid {$orderid}  in file ".__FILE__." in line ".__LINE__." query => {$query}") ;
db_query($query);


$discountManager->lockCoupons();

$orderId = $orderid;  //What is this ????
$productid ;
$product ='';
$totalQty = 0;
$productTypeNames ='';

foreach($productsInCart as $key=>$value)
{
	$productDetail = $value;

	// Fetch product detail for the product
	$productid = $productDetail['productId'];

	//makeup quntitybreakup string for product
	$qcount = count($productDetail['sizenames']);
	$quantity_breakup = "";
	for($i=0;$i<$qcount;$i++)
	{
		if($i!=$qcount-1)
			$quantity_breakup = $quantity_breakup.$productDetail['sizenames'][$i].":".$productDetail['sizequantities'][$i].",";
		else
			$quantity_breakup = $quantity_breakup.$productDetail['sizenames'][$i].":".$productDetail['sizequantities'][$i];
	}
	#
	#shipping rate for the city
	#
	$logisticId = $HTTP_POST_VARS['s_option'];
	#
	#create an array of products for each elements 
	#
	$productInCart = array();
	$productInCart[] = $productDetail;
	$productid_array = array();
	 
	$shipping_city = strtolower($scity);

	//Kundan: we no longer have free-shipping coupons, so passing null in place of coupon 
	$shippingRateArray = func_shipping_rate_re($scountry,$s_state, $shipping_city, $productInCart, $productid_array, null, $logisticId,$s_zipcode, $cart_amount);
	
	$shippingRate = $shippingRateArray['shiprate'];
	 
	$consignmentCapacity = func_query_first_cell("SELECT consignment_capacity FROM $sql_tbl[shipping_prod_type] WHERE product_type_id='".$productDetail[producTypeId]."'");
	$totalunits = ceil($productTypeQuantityArray[$productDetail[producTypeId]]['qty']/$consignmentCapacity);
	$shippingTotalCost = ($totalunits *  $shippingRate)/$productTypeQuantityArray[$productDetail[producTypeId]]['stylecount'];
             //added by nishith
            if(empty($thisShippingRate)){
                $shippingTaxAmount=0;
                $shippingAmount=0;
            }else{
                $shippingTaxAmount = ($shippingTotalCost * ($vatRate/100));
                $shippingAmount = $shippingTotalCost - $shippingTaxAmount;
	}

	$vatRate = $productDetail["vatRate"];
	$actual_product_price  = $productDetail["actualPriceOfProduct"];
	$cash_redeemed  = $productDetail["cashdiscount"];
	
	$vatAmount = number_format($productDetail["unitVat"] * $productDetail["quantity"],2,".",'');
	$totalAmount = number_format(($productDetail["productPrice"] * $productDetail["quantity"]) - $productDetail['discountAmount'],2,".",'');
	$pg_discount_product = number_format(($totalAmount*$pg_discount)/$amountWithProductDiscount,2,".",'');
	$productPrice = $productDetail["productPrice"];
	$is_customizable = isset($productDetail["is_customizable"])?$productDetail["is_customizable"]:1;
	$coupon_discount = $productDetail['couponDiscount'];
	$cash_coupon_code=$XCART_SESSION_VARS['cashCouponCode'];
	$discountOnProduct  = ($productDetail['discountAmount'] != 0) ? $productDetail['discountAmount'] : $productDetail["systemDiscount"];
	$discountQuantity = $productDetail['discountQuantity'];
	$cartDiscountSplitOnRatio = $productDetail['cartDiscount'];
	$product_discount_rev_id = $productDetail['discountRuleRevId'];
	$product_discount_ruleid = $productDetail['discountRuleId'];
	$product_is_discountable = $productDetail['isDiscounted'];
	$product_is_returnable = $productDetail['isReturnable'];
		
	$styleId = $productDetail["productStyleId"];
	$is_jit_purchase = false;
	for($ii=0;$ii<count($productDetail['sizenames']);$ii++){
		if($productDetail['sizequantities'][$ii]>0){
			$optionid_sql = "select id from mk_product_options where style = $styleId and value = '".$productDetail['sizenames'][$ii]."'";
			$optionid_row = func_query_first($optionid_sql);
			$optionId = $optionid_row['id'];
			$sku_query = "select msom.sku_id from mk_styles_options_skus_mapping msom where msom.style_id = $styleId and msom.option_id = $optionId";
			$sku_row = func_query_first($sku_query);
			$sku_details = SkuApiClient::getSkuDetails($sku_row['sku_id']);
			$warehouseId2CountMap = SkuApiClient::getAvailableInvCountMap($sku_row['sku_id'], 'warehouseId');
			$available_count = 0;
			foreach ($warehouseId2CountMap as $wId=>$count) {
				$available_count += $count;
			}
			if($available_count <= 0 && $sku_details[0]['jitSourced']){
				$is_jit_purchase = true;
			}
		}
	}
	
	/** Pravin Cleanup - resetting useless date */
	if(empty($promotion_code)) 
		$promotion_code = 0;
		
	$style_template = $productDetail['template_id'];
	
	if(empty($style_template)) 
		$style_template = 0;
		
	if(empty($product_discount_ruleid)) 
		$product_discount_ruleid = "null";
		
	$jit_purchase = 0;
	if($is_jit_purchase == true)
		$jit_purchase = 1;
		
	/* End reset data for query correctness */
	
	//Insert data in xcart_order_detail
	$query ="insert into $tb_order_details (
						orderid,productid,price,amount,provider,product,product_style,product_type, discount, discount_quantity, cart_discount_split_on_ratio,
						actual_product_price,quantity_breakup, location, item_status, tax_rate, taxamount, shipping_amount, 
						shipping_tax, total_amount,promotion_id,style_template,is_customizable,coupon_code,
						coupon_discount_product,cash_redeemed,cash_coupon_code,discount_rule_id,discount_rule_rev_id,
						is_discounted, is_returnable,  jit_purchase,pg_discount, product_options, extra_data 
					) values (
						'".$orderId."','".$productid."','".$productPrice."','".$productDetail['quantity']."','".$designer."','".$product."','".$productDetail['productStyleId']."','".$productDetail['producTypeId']."', '".$discountOnProduct."','".$discountQuantity."','".$cartDiscountSplitOnRatio."',
						'".$actual_product_price."', '".$quantity_breakup."','".$productDetail['operation_location']."', 'UA', '".$vatRate."', '".$vatAmount."', '".$shippingAmount."', 
						'".$shippingTaxAmount."', '".$totalAmount."','".$promotion_code."','".$style_template."','".$is_customizable."','".$couponCode."',
						'".$coupon_discount."','".$cash_redeemed."','".$cash_coupon_code."','".$product_discount_ruleid."','".$product_discount_rev_id."',
						'".$product_is_discountable."','".$product_is_returnable."','$jit_purchase','$pg_discount_product', '', '' 
					)";
	db_query($query);
	$last_id_rs = func_query("select last_insert_id() as id");
	$item_id=$last_id_rs[0]['id'];
	$totalQty = $totalQty + $productDetail['quantity'];
	$product_option_rs=func_query("SELECT id as id,trim(value) as value from $tb_product_options where style=".$productDetail['productStyleId']);
	$styleOptions;
	foreach($product_option_rs as $row){
		$styleOptions[$row['value']]=$row['id'];
	}
	for($ii=0;$ii<count($productDetail['sizenames']);$ii++){
		if($productDetail['sizequantities'][$ii]>0) {
			$item_option_id = $styleOptions[trim($productDetail['sizenames'][$ii])];
			$item_size_quanity = trim($productDetail['sizequantities'][$ii]);
			$sql_query="insert into $tb_mk_order_item_option_quantity (itemid,optionid,quantity) values($item_id,$item_option_id,$item_size_quanity)";
			db_query($sql_query);
		}
	}	
}

		  
$UpdatesQtyquery ="UPDATE $tb_orders  SET qtyInOrder=$totalQty WHERE orderid = '".$orderId."'";
db_query($UpdatesQtyquery);

x_session_register("order_id", $orderId);
		
    ############ END ENTER ORDER DETAILS IN THE SYSTEM IN PREPROCESSED STATE #########
    
include_once 'mkpgpmt.php';
