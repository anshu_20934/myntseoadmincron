<?php 
/*
 * This is myntra specific config
 * for xcart see config.php
 */
$configMode = "prod"; // Could be local/test/release/prod
require_once('config/' . $configMode . '.env.php');

if(!defined("CONFIG_INITIALIZED")){
  $masterConfigArray = xcache_get($configMode."_config");

  if($masterConfigArray == null || empty($masterConfigArray)){
    $masterConfigArray = array();

    function var_extract(&$config_array,$prefix=NULL){
      global $configMode,$masterConfigArray;
      $configMode = strtolower($configMode);
      $mode = ($prefix==NULL)?EXTR_OVERWRITE:EXTR_PREFIX_ALL;
      $config = $config_array[$configMode];
      if($configMode == "release-fox7" && !isset($config_array["release-fox7"])) $config = $config_array["prod"];
	  if($configMode == "release-fox8" && !isset($config_array["release-fox8"])) $config = $config_array["prod"];
      foreach($config as $key=>$value){
        if($prefix!=NULL) $key = $prefix."_".$key;
        $masterConfigArray[$key] = $value;
      }
    }

    $configPattern = "config/*.config.php";
    while(count($configFiles = glob($configPattern))==0){
      $configPattern = "../".$configPattern;
    }

    foreach($configFiles as $config_filename) {
      require_once($config_filename);
    }

    xcache_set($configMode."_config",$masterConfigArray);
  }
  extract($masterConfigArray,EXTR_OVERWRITE);
}
define("CONFIG_INITIALIZED",true);
?>
