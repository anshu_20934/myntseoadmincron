<?php
require "./auth.php";

if(empty($login)){
    $errormessage = "We are sorry, you are not authorized to access this page. Please login to access the invoice details.";
    $XCART_SESSION_VARS['errormessage'] = $errormessage;
    header("Location:mksystemerror.php");
}else{
$orderid = $HTTP_GET_VARS['orderid'];

/*
##to have the reebok-ipl invoice with
##customized name and number on jersey t-shirt
*/
$reebok_invoice = $HTTP_GET_VARS['invoice'];

#
#Check if login and orderid is maped
#
$sql = "SELECT count(*) ctr FROM " . $sql_tbl['orders'] . " WHERE login='". $login ."' AND orderid='". $orderid ."'";
$number_of_row = func_query_first_cell($sql);

if($number_of_row != 1){
    $errormessage = "We are sorry, You are trying to access wrong order id.";
    $XCART_SESSION_VARS['errormessage'] = $errormessage;
    header("Location:mksystemerror.php");
}else{
include_once("./include/func/func.mk_orderbook.php");
include_once("./include/func/func.order.php");
include_once("./include/func/func.mkcore.php");
include_once("./include/func/func.mkspecialoffer.php");



$smarty->assign("location", $location);


$order_data = func_order_data($orderid);
$order = $order_data["order"];
$courier_provider_name = get_courier_provider_label($order['shipping_method']);
$userinfo = $order_data["userinfo"];
$giftcerts = $order_data["giftcerts"];

//Tables
$tb_product_category = $sql_tbl["products_categories"];

if(empty($giftcerts))
{
	$giftcerts = 0;
}

$smarty->assign("order",$order);
//$smarty->assign("userinfo",$userinfo);
$smarty->assign("giftcerts",$giftcerts);

$today = date("d/m/Y",$order[date]);
$smarty->assign("today",$today);
switch($order['status'])
{
	case 'P': $status = 'Processed'; break;
	case 'C': $status = 'Complete'; break;
	case 'Q': $status = 'Queued'; break;
	case 'D': $status = 'Declined'; break;
	case 'F': $status = 'Failed'; break;
	case 'PD': $status = 'Pending'; break;
	//case 'PP': $status = 'PP'; break;

}
$smarty->assign("status", $status );
$smarty->assign("invoiceid", $order['invoiceid'] );
$smarty->assign("orderid", $order['orderid'] );
$smarty->assign("deliverymethod", $courier_provider_name );

#
#Call function to load the customer details
#
$customerdetail = func_customer_order_address_detail($order['login'], $order['orderid']);

if($customerdetail['s_country'] == 'IN' || $customerdetail['s_country'] == 'US'){      
$customerdetail['s_state'] = func_get_state_label($customerdetail['s_state']);
}
if($customerdetail['b_country'] == 'IN' || $customerdetail['b_country'] == 'US'){      
$customerdetail['b_state'] = func_get_state_label($customerdetail['b_state']);
}  
    #
    #Load country label
    #
    $sql = "SELECT name, value FROM xcart_languages WHERE topic='Countries'";
    $country_list = func_query($sql);
    foreach($country_list AS $key => $array){
        $name = explode("_", $array['name']);
        if(strstr(trim($customerdetail['s_country']), $name[1])){
            $customerdetail['s_country'] = $array['value'];
        }
        if(strstr(trim($customerdetail['b_country']), $name[1])){
            $customerdetail['b_country'] = $array['value'];
        }
    }
    
$smarty->assign("userinfo", $customerdetail);
$smarty->assign("mobile", $customerdetail['mobile']);

$smarty->assign("address", stripslashes($customerdetail['s_address']));
$smarty->assign("pincode", $customerdetail['s_zipcode']);
$smarty->assign("phone", $customerdetail['phone']);
$smarty->assign("mobile", $customerdetail['mobile']);
$smarty->assign("fax", $customerdetail['fax']);
$smarty->assign("email", $customerdetail['login']);
$smarty->assign("statename", $customerdetail['s_state']);
$smarty->assign("countryname", $customerdetail['s_country']);
$smarty->assign("cityname", $customerdetail['s_city']);

$optionCounter = array();

$productsInCart = func_query("SELECT product, productid, product_style, product_type, price, amount, subtotal, total, $sql_tbl[order_details].discount AS productDiscount, $sql_tbl[orders].discount AS totaldiscountonProducts, coupon_discount, $sql_tbl[orders].coupon AS couponcode, shipping_cost, tax, ordertype, ref_discount, $sql_tbl[order_details].quantity_breakup AS quantity_breakup, $sql_tbl[order_details].shipping_amount, $sql_tbl[order_details].shipping_tax, ($sql_tbl[order_details].shipping_amount + $sql_tbl[order_details].shipping_tax) AS net_shipping, $sql_tbl[order_details].tax_rate, $sql_tbl[order_details].taxamount, $sql_tbl[order_details].total_amount, $sql_tbl[order_details].promotion_id, $sql_tbl[orders].pg_discount AS pg_discount, $sql_tbl[orders].payment_surcharge AS payment_surcharge FROM $sql_tbl[order_details], $sql_tbl[orders] WHERE $sql_tbl[order_details].orderid = $sql_tbl[orders].orderid AND $sql_tbl[order_details].orderid = '".$_GET['orderid']."'");

$k=0;$j=0;
$categories = array();
$productids = array();
$discountonoffer = 0;
foreach($productsInCart as $key => $order)
{

//Query for retriving the style name from mk_product_style
$stylename = "SELECT name, styletype FROM $sql_tbl[mk_product_style] WHERE id = '".$productsInCart[$key]['product_style']."'";
$styleresult = db_query($stylename);
$rowstyle = db_fetch_array($styleresult);

//Query for retriving the style options from mk_product_options
 $styleoption = "SELECT $sql_tbl[mk_product_options].name AS optionName, $sql_tbl[mk_product_options].value AS optionValue FROM $sql_tbl[mk_product_options], $sql_tbl[mk_product_options_order_details_rel]
WHERE $sql_tbl[mk_product_options].id = $sql_tbl[mk_product_options_order_details_rel].option_id
AND $sql_tbl[mk_product_options].style = ".$productsInCart[$key]['product_style']."
AND $sql_tbl[mk_product_options_order_details_rel].orderid = '".$_GET['orderid']."'
AND $sql_tbl[mk_product_options_order_details_rel].itemid = '".$productsInCart[$key]['productid']."'";


$optionresult = db_query($styleoption);
$optionCount = db_num_rows($optionresult);
$productsInCart[$value]['optionCount'] = $optionCount;
$i = 0;
while($optionrow = db_fetch_array($optionresult))
{
	$productsInCart[$key]['optionNames'][$i] =  $optionrow['optionName'];
    $productsInCart[$key]['optionValues'][$i] =  $optionrow['optionValue'];
	$i++;
}

$typename = "SELECT pt.name as productName, pt.label as pLabel, pt.image_t as pImage, ps.name as sName
					FROM $sql_tbl[producttypes] AS pt, $sql_tbl[mk_product_style] AS ps
					WHERE pt.id = ps.product_type
					AND pt.id = ".$productsInCart[$key]['product_type']."
					AND ps.id=".$productsInCart[$key]['product_style']."";


$typeresult = db_query($typename);
$row = db_fetch_array($typeresult);
$productsInCart[$key]['productStyleName'] = $row['sName'];

$areaId = func_get_all_orientation_for_default_customization_area($productsInCart[$key]['product_style']);

/*$defaultImage = "SELECT image AS ProdImage
						FROM $sql_tbl[mk_xcart_order_details_customization_rel] odc
						WHERE area_id = ".($areaId[0][6])."
						AND itemid = ".$productsInCart[$key]['productid']."
						AND orderid = '".$_GET['orderid']."'";*/
$pid = $productsInCart[$key]['productid'];
$defaultImage = "SELECT image_portal_t  as ProdImage FROM $sql_tbl[products] WHERE productid =$pid";


$ImageResult = db_query($defaultImage);
$ImageRow = db_fetch_array($ImageResult);

$productsInCart[$key]['productStyleType'] = $rowstyle['styletype'];
$productsInCart[$key]['product_type'] = $rowstyle['name'];
$productsInCart[$key]['productTypeLabel'] = $row['pLabel'];
$productsInCart[$key]['designImagePath'] = $ImageRow['ProdImage'];
$productsInCart[$key]['totalPrice'] = $productsInCart[$key]['price'] * $productsInCart[$key]['amount'];


$allCustImagePath = func_load_product_customized_area_rel_details($pid);
$n = 0;
foreach($allCustImagePath as $k=>$value)
{
	if($value['thumb_image'] != "") 
	{
		$productsInCart[$key]['allCustImagePath'][$n] = $value['thumb_image'];
		$n++;
	}
	/*
	##added for reebok-ipl to get name and number
	*/
	if($reebok_invoice){
		$jersey_array = explode('@',$value['text']);
		$productsInCart[$key]['jersey_text'] = $jersey_array[0];
		$productsInCart[$key]['jersey_number'] = $jersey_array[1];
	}
		
}


//To get the categoryids of the product
	$catidquery = "select $tb_product_category.categoryid from
	$tb_product_category where $tb_product_category.productid =".$productsInCart[$key]['productid']."";
	$catidresult=db_query($catidquery);

	if($catidresult)
	{
		$row=db_fetch_row($catidresult);
		if(!in_array($row[0], $categoryids) && ($row[0] != ""))
		{
			$categoryids[$k] = $row[0];
		}
	}

   $k++;

   // create an array of product ids
	$productids[$j] = $productsInCart[$key]['productid'];
	$j++;

  // Count total discount of offer
  $discountonoffer = $discountonoffer + $productsInCart[$key]['productDiscount'];

}
//
//echo "<pre>";
//print_r($productsInCart[1][promotion]);
//exit;
 //Coupon discount
  $coupondiscount = $productsInCart[0]['coupon_discount'];
 //Coupon Code
 $couponCode = $productsInCart[0]['couponcode'];
 if(!empty($couponCode))
 {
      $couponTable        = $sql_tbl["discount_coupons"];
	  $queryCouponPercent  = "SELECT c.discount,c.coupon_type from $couponTable c where c.coupon = '$couponCode'
 	  and status = 'A' ";
 	  $rsCouponPercent  = db_query($queryCouponPercent);
 	  $rowCouponPercent = db_fetch_array($rsCouponPercent);
	  if($rowCouponPercent['coupon_type']=='percent')
 		$smarty->assign("couponpercent", $rowCouponPercent['discount']."%");
	  else
		$smarty->assign("couponpercent", "Rs. ".$rowCouponPercent['discount']);
}

$totalamount = $productsInCart[0]['subtotal'];
//$amountwitoutcc = $productsInCart[0]['subtotal'];// - $productsInCart[0]['totaldiscountonProducts'];
$amountafterdiscount = $productsInCart[0]['total'];
$shipping_cost = $productsInCart[0]['shipping_cost'];
$vatCst = $productsInCart[0]['tax'];
$ref_discount = $productsInCart[0]['ref_discount'];
$pg_discount = $productsInCart[0]['pg_discount'];
$emi_charge = $productsInCart[0]['payment_surcharge'];
// calculating the final amount to be paid after  calculating all the taxes
//$vatCst =  (12.50 * floatval($amountafterdiscount))/100;

$amountafterdiscount = $amountafterdiscount + $emi_charge;

$smarty->assign('giftpack_charges',$order_data["order"]['gift_charges']);

if($order_data["order"]['payment_method'] == 'cod')
$amountafterdiscount = $amountafterdiscount + $order_data["order"]['cod'];

$smarty->assign("cod_charges",$order_data["order"]['cod']);

$smarty->assign("vat",$vatCst);

// Call function to get special offer of productid
$productOffer = func_get_discount_offers_data($productids, 'P');

//Call function to get special offer of category id
$categoryOffer = func_get_discount_offers_data($categoryids, 'C');

$offers = array_merge($productOffer, $categoryOffer);
$countoffer = count($offers);

// Call function to get special offers for total
$totaloffers = func_get_special_offers_for_Total();
$counttotaloffer = count($totaloffers);

// Get special offer for discount on total
$amountaftersofferdiscount = $totalamount - $discountonoffer;
$discountOnTotal = func_get_special_offer_discount_for_Total($amountaftersofferdiscount);

// Count total discount on offer
$totaloffersdiscount = $discountonoffer + $discountOnTotal;

for($j=0; $j<3; $j++)
{
	$optionCounter[$j]= $j;
}
$cashdiscount=func_query_first_cell("select cash_redeemed from xcart_orders where orderid=".$orderid);
$smarty->assign("cashdiscount",$cashdiscount);    
$smarty->assign("gift_status",$order_data["order"]['gift_status']);
$smarty->assign("ref_discount",$ref_discount);
$smarty->assign("optionCounter",$optionCounter);
$smarty->assign("counttotaloffer", $counttotaloffer);
$smarty->assign("countoffers", $countoffer);
$smarty->assign("couponCode", $couponCode);
$smarty->assign("coupondiscount", $coupondiscount);
$smarty->assign("offers", $offers);
$smarty->assign("totaloffers", $totaloffers);
$smarty->assign("paymentoption", $order_data["order"]['payment_method']);
$smarty->assign("totaloffersdiscount", number_format($productsInCart[0]['totaldiscountonProducts'],2,".",''));
$smarty->assign("pg_discount", $pg_discount);
$smarty->assign("emi_charge", $emi_charge);

if(empty($productsInCart))
{
	$productsInCart = 0;
}

//$order_data = func_order_data($_GET['orderid']);

//$order = $order_data["order"];
$products = $order_data["products"];

$smarty->assign("productsInCart", $productsInCart);
//$totalafterdiscount = $productsInCart[0]['total'] - $totaldiscount;

$smarty->assign("shippingRate", number_format($shipping_cost,2,".",''));
$smarty->assign("amountwitoutcc", $totalamount);
$smarty->assign("grandTotal", number_format($totalamount,2,".",''));
$smarty->assign("totalafterdiscount", number_format($amountafterdiscount,2,".",''));

$CompanyStreetAdress1 = "7th Mile, Krishna Reddy Industrial Area, Kudlu Gate,";
$CompanyStreetAdress2 ="Opp Macaulay High School, behind Andhra Bank ATM,";
$CompanyStreetAdress3 ="Bangalore - 560068, India";
$CompanyStreetAdress4 ="Vector E-Commerce Pvt Ltd.";

$smarty->assign("address1", $CompanyStreetAdress1);
$smarty->assign("address2", $CompanyStreetAdress2);
$smarty->assign("address3", $CompanyStreetAdress3);
$smarty->assign("address4", $CompanyStreetAdress4);
$smarty->assign("vatId", 'TIN29910754899');

/*reebok invoice*/
$smarty->assign("reebok_invoice",$reebok_invoice);

//code for printing change requests for a order 
if(func_get_order_changereqstatus($orderid)=='Y'){
	$comments=func_getorder_requests_array($orderid);
	$smarty->assign("changestatus","Y");
	$smarty->assign("comments", $comments);
};

if($offline == 'off')
	func_display("order_invoice_bulk.tpl", $smarty);
else
	func_display("order_invoice.tpl", $smarty);
}
}
?>