<?php
/*****************************************************************************\
+-----------------------------------------------------------------------------+
| X-Cart                                                                      |
| Copyright (c) 2001-2006 Ruslan R. Fazliev <rrf@rrf.ru>                      |
| All rights reserved.                                                        |
+-----------------------------------------------------------------------------+
| PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
| FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
| AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
|                                                                             |
| THIS  AGREEMENT  EXPRESSES  THE  TERMS  AND CONDITIONS ON WHICH YOU MAY USE |
| THIS SOFTWARE   PROGRAM   AND  ASSOCIATED  DOCUMENTATION   THAT  RUSLAN  R. |
| FAZLIEV (hereinafter  referred to as "THE AUTHOR") IS FURNISHING  OR MAKING |
| AVAILABLE TO YOU WITH  THIS  AGREEMENT  (COLLECTIVELY,  THE  "SOFTWARE").   |
| PLEASE   REVIEW   THE  TERMS  AND   CONDITIONS  OF  THIS  LICENSE AGREEMENT |
| CAREFULLY   BEFORE   INSTALLING   OR  USING  THE  SOFTWARE.  BY INSTALLING, |
| COPYING   OR   OTHERWISE   USING   THE   SOFTWARE,  YOU  AND  YOUR  COMPANY |
| (COLLECTIVELY,  "YOU")  ARE  ACCEPTING  AND AGREEING  TO  THE TERMS OF THIS |
| LICENSE   AGREEMENT.   IF  YOU    ARE  NOT  WILLING   TO  BE  BOUND BY THIS |
| AGREEMENT, DO  NOT INSTALL OR USE THE SOFTWARE.  VARIOUS   COPYRIGHTS   AND |
| OTHER   INTELLECTUAL   PROPERTY   RIGHTS    PROTECT   THE   SOFTWARE.  THIS |
| AGREEMENT IS A LICENSE AGREEMENT THAT GIVES  YOU  LIMITED  RIGHTS   TO  USE |
| THE  SOFTWARE   AND  NOT  AN  AGREEMENT  FOR SALE OR FOR  TRANSFER OF TITLE.|
| THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY GRANTED BY THIS AGREEMENT.      |
|                                                                             |
| The Initial Developer of the Original Code is Ruslan R. Fazliev             |
| Portions created by Ruslan R. Fazliev are Copyright (C) 2001-2006           |
| Ruslan R. Fazliev. All Rights Reserved.                                     |
+-----------------------------------------------------------------------------+
\*****************************************************************************/

#
# $Id: auth.php,v 1.30.2.1 2006/06/02 13:54:18 max Exp $
#
define('AREA_TYPE', 'C');
if (!defined('AUTH_SOLR_INIT')) {
       define('AUTH_SOLR_INIT', 1);
}
//if ($_GET['s']!=1){
//header("location:sitedown.php");
//}
@include_once (dirname(__FILE__)). "/top.inc.php";

@include ($xcart_dir."/include/func/func.seo.php");
@include ($xcart_dir."/sanitizept.inc.php");
@include ($xcart_dir."/include/func/func.search.php");
include_once("$xcart_dir/include/class/widget/class.widget.keyvaluepair.php");

define('MYNTRA_HOME_DIR',$xcart_dir);
if (!defined('DIR_CUSTOMER')) die("ERROR: Can not initiate application! Please check configuration.");

include_once ($xcart_dir.'/logger.php');
include_once $xcart_dir."/init.php";


$current_area="C";


x_load('files');

x_session_register("logout_user");

if(!isset($_POST['affiliate']))
	require $xcart_dir."/include/nocookie_warning.php";
if (!defined('HTTPS_CHECK_SKIP')) {
	@include $xcart_dir.DIR_CUSTOMER."/https.php";
}

#
# Browser have disabled/enabled javasript switching
#
x_session_register("js_enabled", "Y");

if (!isset($js_enabled)) $js_enabled="Y";

if (isset($HTTP_GET_VARS["js"])) {
	if ($HTTP_GET_VARS["js"]=="y") {
		$js_enabled = "Y";
		$config['Adaptives']['isJS'] = "Y";
		$adaptives['isJS'] = "Y";
	}
	elseif ($HTTP_GET_VARS["js"]=="n") {
		$js_enabled = "";
	}
}

if ($js_enabled == "Y") {
	$qry_string = ereg_replace("(&*)js=y", "", $QUERY_STRING);
	$js_update_link = $PHP_SELF."?".($qry_string?"$qry_string&":"")."js=n";
}
else {
	$qry_string = ereg_replace("(&*)js=n", "", $QUERY_STRING);
	$js_update_link = $PHP_SELF."?".($qry_string?"$qry_string&":"")."js=y";
}

$smarty->assign("js_update_link", $js_update_link);
$smarty->assign("js_enabled", $js_enabled);

x_session_register("top_message");
if (!empty($top_message)) {
	$smarty->assign("top_message", $top_message);
	if ($config['Adaptives']['is_first_start'] != 'Y')
		$top_message = "";

	x_session_save("top_message");
}

$cat = intval(@$cat);
$page = intval(@$page);

include $xcart_dir.DIR_CUSTOMER."/referer.php";

include $xcart_dir."/include/check_useraccount.php";

include $xcart_dir."/include/get_language.php";

$lbl_site_name = func_get_langvar_by_name("lbl_site_title", "", false, true);
$location = array();
$location[] = array((!empty($lbl_site_name) ? $lbl_site_name : $config["Company"]["company_name"]), "home.php");

include $xcart_dir.DIR_CUSTOMER."/minicart.php";

$speed_bar = unserialize($config["speed_bar"]);
if (!empty($speed_bar)) {
	$tmp_labels = array();
	foreach ($speed_bar as $k => $v) {
		$speed_bar[$k] = func_array_map("stripslashes", $v);
		$tmp_labels[] = "speed_bar_".$v['id'];
	}

	$tmp = func_get_languages_alt($tmp_labels);
	foreach ($speed_bar as $k => $v) {
		if (isset($tmp['speed_bar_'.$v['id']]))
			$speed_bar[$k]['title'] = $tmp['speed_bar_'.$v['id']];

		$speed_bar[$k]['link'] = str_replace("&", "&amp;", $v['link']);
	}

	$smarty->assign("speed_bar", $speed_bar);
}

unset($speed_bar);

$smarty->assign("redirect","customer");

if (!empty($active_modules["Feature_Comparison"]) && $config['Feature_Comparison']['fcomparison_show_product_list'] == 'Y') {
	$comparison_list = func_get_comparison_list();
	$smarty->assign("comparison_list",$comparison_list);
}

if (!empty($active_modules["Surveys"])) {
	include_once $xcart_dir."/modules/Surveys/surveys_list.php";
}

$smarty->assign("printable", $printable);
$smarty->assign("logout_user", $logout_user);
$userAgent  = $_SERVER['HTTP_USER_AGENT'];
if(strpos($userAgent,"MSIE") > 0)
{
	$smarty->assign("ie","true");

}

$smarty->assign("free_shipping_amount", $system_free_shipping_amount);
$userfirstname = $XCART_SESSION_VARS['userfirstname'];

$smarty->assign("firstname",$userfirstname);

/*$topHelpMessage = func_query_first("SELECT * FROM mk_widget_key_value_pairs where `key` = 'customerSupportTime'");
if($topHelpMessage!= false) {
	$customer_support_no = $topHelpMessage['value'];
	$smarty->assign("csnumber", $customer_support_no );
}*/
$customer_support_no = WidgetKeyValuePairs::getWidgetValueForKey('customerSupportCall');
$smarty->assign("csnumber", $customer_support_no );

##################Base URL for urlrewriting####################
$smarty->assign("baseurl",$baseurl);
$smarty->assign("css_path",$css_path);  
$smarty->assign("js_path",$js_path);
$smarty->assign("brandshop_css_path",$brandshop_css_path);
$smarty->assign("brandshop_js_path",$brandshop_js_path);//to be removed later once code sync is done
$smarty->assign("cdn_base",$cdn_base);
$smarty->assign("combine_css_path",$combine_css_path);
$smarty->assign("combine_js_path",$combine_js_path);

######################################################
##################Cash on delivery charges####################
$smarty->assign("cod_charges",$cod_charges);
######################################################


 ################## load the title , meta description and keywords  #############
$pageurl = $_SERVER['REQUEST_URI'];
$url_element = explode("/",$pageurl);
$fcindex = array_search("FC",$url_element);
$affiliateInfo = $XCART_SESSION_VARS['affiliateInfo'];

### IF NOT FILTER URL [ IF FC IS PRESENT THEN INDEX.PHP WILL DECIDE THE TITLE ]
if($fcindex === false)
{
	$pageName = basename($_SERVER['SCRIPT_NAME']);
	$pageTitle = getTitleTag($pageName);
	$metaDescription = getMetaDescription($pageName);
	$metaKeywords = getMetaKeywords($pageName);
	$seoExtraContents = getExtraContents($pageName);
	$smarty->assign("external_content", $seoExtraContents['external_content']);
	$smarty->assign("viewcomment", $seoExtraContents['comments']);
	$smarty->assign("rssfeeds", $seoExtraContents['rssfeeds']);
	$smarty->assign("h1tag", $seoExtraContents['h1tag']);
	$smarty->assign("h2tag", $seoExtraContents['h2tag']);
	$smarty->assign("relatedkeywords", $seoExtraContents['relatedkeywords']);
	$smarty->assign("imagesource", $seoExtraContents['imagesource']);
	if($affiliateInfo[0]['title'] != '')
	{
		 $smarty->assign("pageTitle",$affiliateInfo[0]['title']);
	}
	else
	{
		 $smarty->assign("pageTitle",$pageTitle);

	}
	$smarty->assign("metaDescription", $metaDescription);
	$smarty->assign("metaKeywords", $metaKeywords);

}


 ################### end #################################################

 #################################Maintaining Open Nodes########################

 $nodes = $XCART_SESSION_VARS['nodes'];
 $categoriesids = array_keys($nodes);
 $opennodes =array();
 for($i = 0; $i < count($categoriesids); $i++)
 {

	if($nodes[$categoriesids[$i]] == 'OPEN')
	{
			 $subcategorydetail = func_load_subcategories($categoriesids[$i]);
			 $opennodes[$categoriesids[$i]]=$subcategorydetail;
	}
 }
 $smarty->assign('opennodes',$opennodes);
############################################################################
$userType = $XCART_SESSION_VARS['login_type'];
$posaccounttype = $XCART_SESSION_VARS['identifiers'][$userType]['account_type'];

//$userType = $XCART_SESSION_VARS['login_type'];
$shopMasterId = 0;


$accountType=0;

$recently_searched_footer = get_recently_searched('',8);
$smarty->assign('recently_searched_footer',$recently_searched_footer);

//get all filters for sportswear top menu
$main_array=array();
    $filter_groups=array();
    $sql ="SELECT * FROM  mk_filter_group where is_active=1 order by display_order";
    $grouprecords = func_query($sql);
    //echo "<pre>";print_r($grouprecords);die;
    foreach($grouprecords as $groups)
    {
        $filter_sql="select f.*,g.group_name,g.group_label from mk_filters f join mk_filter_group g on f.filter_group_id=g.id where f.is_active=1 and f.filter_group_id=".$groups['id']." order by f.display_order ";
        $filter_by_groups = func_query($filter_sql);
        $main_array[$groups['group_label']]=$filter_by_groups;
        $filter_groups[$groups['group_name']]=$groups['id'];
    }
$smarty->assign("sportswear",$main_array);

$nocache = $_GET['nocache'];
$top_nav = $xcache->fetch("top_nav");
if($top_nav == NULL || $nocache == 1){
    // Populate top navigation menu.
//    include_once($xcart_dir."/include/class/widget/headerMenu.php");
//    $top_nav = func_display("site/menu_topnav.tpl", $smarty, false);
//    if($nocache != 1){//Do not store in case nocache param is set
//        $xcache->store("top_nav", $top_nav);
//    }
}

$smarty->assign("top_nav", $top_nav);

?>
