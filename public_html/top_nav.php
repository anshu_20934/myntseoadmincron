<?php
include_once './auth.php';
require_once("$xcart_dir/include/solr/solrProducts.php");
require_once("$xcart_dir/include/class/search/SearchProducts.php");
include_once($xcart_dir."/include/class/class.feature_gate.keyvaluepair.php");

class Top_nav{

	public static function getTopNav($smarty){
		
		$solrProducts = new SolrProducts();
		
//		$topnav_men_links=$solrProducts->facetedSearch("global_attr_gender:(men OR unisex)", "brands_filter_facet");
//		$topnav_women_links=$solrProducts->facetedSearch("global_attr_gender:(women OR unisex)", "brands_filter_facet");
//		$topnav_kids_links=$solrProducts->facetedSearch("global_attr_age_group:(kids-boys OR kids-girls OR kids-unisex)", "brands_filter_facet");
		$rangeAtoZ = array(0 => array("rangeText" => "A TO Z", "solrQuery" => "brands_filter_facet:[A TO Z] OR brands_filter_facet:[a TO z]"));
		$topnav_men_links=Top_nav::getBrandsFromQuery("global_attr_gender:(men OR unisex)", $rangeAtoZ);
		$topnav_women_links=Top_nav::getBrandsFromQuery("global_attr_gender:(women OR unisex)", $rangeAtoZ);
		$topnav_kids_links=Top_nav::getBrandsFromQuery("global_attr_age_group:(kids-boys OR kids-girls OR kids-unisex)", $rangeAtoZ);
		$mens_display_categories=$solrProducts->facetedSearch("global_attr_gender:(men OR unisex)", "display_category_facet");
		$womens_display_categories=$solrProducts->facetedSearch("global_attr_gender:(women OR unisex)", "display_category_facet");
		$kids_display_categories=$solrProducts->facetedSearch("global_attr_age_group:(kids-boys OR kids-girls OR kids-unisex)", "display_category_facet");
		$global_display_categories = $solrProducts->multipleFacetedSearch('*:* AND display_category:[* TO *]', array("display_category_facet"));
		
		$global_brands = Top_nav::getBrandsFromQuery('*:*', $rangeAtoZ);
		
		
		$smarty->assign("men_brands",$topnav_men_links[0]['result']['brands_filter_facet']);
		$smarty->assign("women_brands",$topnav_women_links[0]['result']['brands_filter_facet']);
		$smarty->assign("kids_brands",$topnav_kids_links[0]['result']['brands_filter_facet']);
		$smarty->assign("mens_display_categories",sortDisplayCategory($mens_display_categories->display_category_facet));
		$smarty->assign("womens_display_categories",sortDisplayCategory($womens_display_categories->display_category_facet));
		$smarty->assign("kids_display_categories",sortDisplayCategory($kids_display_categories->display_category_facet));
		$smarty->assign("global_display_categories",sortDisplayCategory($global_display_categories->display_category_facet));
		$smarty->assign("global_brands",$global_brands[0]['result']['brands_filter_facet']);
		
		$saleClearanceEnabled = FeatureGateKeyValuePairs::getBoolean('saleClearanceEnabled');
		$smarty->assign("saleClearanceEnabled", $saleClearanceEnabled);

		$saleClearanceURL= FeatureGateKeyValuePairs::getFeatureGateValueForKey('saleClearanceURL');
		$smarty->assign("saleClearanceURL", $saleClearanceURL);
		
		$topnav=func_display("site/menu_topnav.tpl", $smarty, false);
		return $topnav;
	}
	
	public static function getTopNavMegaMenu($smarty){
		
		$solrProducts = new SolrProducts();
		$searchProducts = SearchProducts::getInstance();
		
//		$topnav_men_links=$solrProducts->facetedSearch("global_attr_gender:(men OR unisex)", "brands_filter_facet");
//		$topnav_women_links=$solrProducts->facetedSearch("global_attr_gender:(women OR unisex)", "brands_filter_facet");
//		$topnav_kids_links=$solrProducts->facetedSearch("global_attr_age_group:(kids-boys OR kids-girls OR kids-unisex)", "brands_filter_facet");
		$rangeAtoZ = array(0 => array("rangeText" => "A - Z", "solrQuery" => "brands_filter_facet:[A TO Z] OR brands_filter_facet:[a TO z]"));
		//$topnav_men_links=Top_nav::getBrandsFromQuery("global_attr_gender:(men OR unisex)", $rangeAtoZ);
		//$topnav_women_links=Top_nav::getBrandsFromQuery("global_attr_gender:(women OR unisex)", $rangeAtoZ);
		//$topnav_kids_links=Top_nav::getBrandsFromQuery("global_attr_age_group:(kids-boys OR kids-girls OR kids-unisex)", $rangeAtoZ);
		//$kids_display_categories=$solrProducts->facetedSearch("global_attr_age_group:(kids-boys OR kids-girls OR kids-unisex)", "display_category_facet");
		//$mens_articletypes=$solrProducts->facetedSearch("global_attr_gender:(men OR unisex)", "global_attr_article_type_facet");
		//$womens_articletypes=$solrProducts->facetedSearch("global_attr_gender:(men OR unisex)", "global_attr_article_type_facet");
		$mens_display_categories=$solrProducts->facetedSearch("global_attr_gender:(men OR unisex)", "display_category_facet");
		$mens_display_categories=sortDisplayCategory($mens_display_categories->display_category_facet);
		$womens_display_categories=$solrProducts->facetedSearch("global_attr_gender:(women OR unisex)", "display_category_facet");
		$womens_display_categories=sortDisplayCategory($womens_display_categories->display_category_facet);
		
		$mens_articletypes_by_display_category=array();
		foreach($mens_display_categories as $key => $value){
			$resultSet=$solrProducts->facetedSearch("global_attr_gender:(men OR unisex) AND display_category:\"$key\" AND count_options_availbale:[1 TO *]", "global_attr_article_type_facet", 0, 0, 1, "count");
			$mens_articletypes_by_display_category[$key]=$resultSet->global_attr_article_type_facet;
		}
		
		$womens_articletypes_by_display_category=array();
		foreach($womens_display_categories as $key => $value){
			$resultSet=$solrProducts->facetedSearch("global_attr_gender:(women OR unisex) AND display_category:\"$key\" AND count_options_availbale:[1 TO *]", "global_attr_article_type_facet", 0, 0, 1, "count");
			$womens_articletypes_by_display_category[$key]=$resultSet->global_attr_article_type_facet;
		}
		
		$boys_display_categories=$solrProducts->facetedSearch("global_attr_age_group:(Kids-Boys OR Kids-Unisex) AND count_options_availbale:[1 TO *]", "display_category_facet", 0, 0, 1, "count");
		$girls_display_categories=$solrProducts->facetedSearch("global_attr_age_group:(Kids-Girls OR Kids-Unisex) AND count_options_availbale:[1 TO *]", "display_category_facet", 0, 0, 1, "count");
		$boys_display_categories=sortDisplayCategory($boys_display_categories->display_category_facet);
		$girls_display_categories=sortDisplayCategory($girls_display_categories->display_category_facet);
		
		$global_display_categories = $solrProducts->multipleFacetedSearch('*:* AND display_category:[* TO *]', array("display_category_facet"));
		$global_brands = Top_nav::getBrandsFromQuery('*:*', $rangeAtoZ);
		
		//$smarty->assign("mens_display_categories",sortDisplayCategory($mens_display_categories->display_category_facet));
		//$smarty->assign("womens_display_categories",sortDisplayCategory($womens_display_categories->display_category_facet));
		//$smarty->assign("mens_articletypes_by_display_category",$mens_articletypes_by_display_category);
		//$smarty->assign("womens_articletypes_by_display_category",$womens_articletypes_by_display_category);
		$smarty->assign("global_display_categories",sortDisplayCategory($global_display_categories->display_category_facet));
		$smarty->assign("global_brands",$global_brands[0]['result']['brands_filter_facet']);
		
		$saleClearanceEnabled = FeatureGateKeyValuePairs::getBoolean('saleClearanceEnabled');
		$smarty->assign("saleClearanceEnabled", $saleClearanceEnabled);

		$saleClearanceURL= FeatureGateKeyValuePairs::getFeatureGateValueForKey('saleClearanceURL');
		$smarty->assign("saleClearanceURL", $saleClearanceURL);
		
		//Mega Menu title configured from admin Widget - Key Value Pairs
		$megaMenuArticleTypeTitle=WidgetKeyValuePairs::getWidgetValueForKey("megaMenuArticleTypeTitle");
		if($megaMenuArticleTypeTitle != NULL){
			$smarty->assign("megaMenuArticleTypeTitle", strtoupper($megaMenuArticleTypeTitle));
		}
		$megaMenuMensTitle=WidgetKeyValuePairs::getWidgetValueForKey("megaMenuMensTitle");
		if($megaMenuMensTitle != NULL){
			$megaMenuMensTitle=trim($megaMenuMensTitle);
			$smarty->assign("megaMenuMensTitle", strtoupper($megaMenuMensTitle));
		}
		$megaMenuWomensTitle=WidgetKeyValuePairs::getWidgetValueForKey("megaMenuWomensTitle");
		if($megaMenuWomensTitle != NULL){
			$megaMenuWomensTitle=trim($megaMenuWomensTitle);
			$smarty->assign("megaMenuWomensTitle", strtoupper($megaMenuWomensTitle));
		}
		$megaMenuKidsTitle=WidgetKeyValuePairs::getWidgetValueForKey("megaMenuKidsTitle");
		if($megaMenuKidsTitle != NULL){
			$megaMenuKidsTitle=trim($megaMenuKidsTitle);
			$smarty->assign("megaMenuKidsTitle", strtoupper($megaMenuKidsTitle));
		}
		
		$topNavigationData=$smarty->get_template_vars("topNavigationWidget");
		$featuredProducts=$smarty->get_template_vars("featuredProducts");
		$featuredProductsData=array();
		foreach($featuredProducts as $key => $value){
			$_featuredProductsData=array();
			foreach($value as $_key => $_value){
				if($_key != ""){
					$_featuredProductsData[$_key]=WidgetUtility::getWidgetFormattedData($searchProducts->getSingleStyle($_value));
				}
				else{
					$_featuredProductsData=WidgetUtility::getWidgetFormattedData($searchProducts->getSingleStyle($_value));
				}
			}
			$featuredProductsData[$key]=$_featuredProductsData;
		}
			
		//Setting data for Mens, Womens and Kids top nav menus
		$topNavigationMenuData=array();
		foreach($topNavigationData as $idx => $val){
			if($idx === $megaMenuMensTitle){
				foreach($mens_articletypes_by_display_category as $key => $value){
					$topNavigationMenuData[$megaMenuMensTitle][$key]["STYLES"]=$value;
					foreach($topNavigationData[$megaMenuMensTitle][$key] as $_key => $_value){
						$topNavigationMenuData[$megaMenuMensTitle][$key][$_key]=$_value;
					}
				}
			}	
			else if($idx === $megaMenuWomensTitle){
				foreach($womens_articletypes_by_display_category as $key => $value){
					$topNavigationMenuData[$megaMenuWomensTitle][$key]["STYLES"]=$value;
					foreach($topNavigationData[$megaMenuWomensTitle][$key] as $_key => $_value){
						$topNavigationMenuData[$megaMenuWomensTitle][$key][$_key]=$_value;
					}
				}
			}	
			else if($idx === $megaMenuKidsTitle){
				$topNavigationMenuData[$megaMenuKidsTitle]["FOR BOYS"]=$boys_display_categories;
				$topNavigationMenuData[$megaMenuKidsTitle]["FOR GIRLS"]=$girls_display_categories;
				foreach($topNavigationData[$megaMenuKidsTitle] as $_key => $_value){
					foreach($topNavigationData[$megaMenuKidsTitle][$_key] as $__key => $__value){
						$topNavigationMenuData[$megaMenuKidsTitle][$__key]=$__value;	
					}
				}
			}
		}
		
		//Setting data for brands
		$featured_brands_sql="select attribute_value from mk_attribute_type_values where is_featured = 1 and is_active = 1 order by filter_order asc";
		$latest_brands_sql="select attribute_value from mk_attribute_type_values where is_latest = 1 and is_active = 1 order by filter_order asc";
		$featured_brands_data=func_query($featured_brands_sql);
		$latest_brands_data=func_query($latest_brands_sql);
		//brands data directory
		$brands_data_directory = Top_nav::getGlobalBrands();

		//featured brands data
		foreach($featured_brands_data as $key => $value){
			$brandname=$featured_brands_data[$key]["attribute_value"];
			$logo_sql="select logo from mk_filters where filter_name='".$brandname."' and is_active=1";
		    $brand_logo=func_query($logo_sql);	    
		    $featured_brands_data[$key]["logo"]=$brand_logo[0]["logo"];
		}
		
		//latest brands data
		foreach($latest_brands_data as $key => $value){
			$brandname=$latest_brands_data[$key]["attribute_value"];
			$logo_sql="select logo from mk_filters where filter_name='".$brandname."' and is_active=1";
		    $brand_logo=func_query($logo_sql);	    
		    $latest_brands_data[$key]["logo"]=$brand_logo[0]["logo"];
		}
		
		$smarty->assign("featuredProductsData", $featuredProductsData);
		$smarty->assign("brands_data_directory", $brands_data_directory);
		$smarty->assign("latest_brands_data", $latest_brands_data);
		$smarty->assign("featured_brands_data", $featured_brands_data);
		$smarty->assign("topNavigationMenuData", $topNavigationMenuData);

        $abHeaderUi = $topNavigationData=$smarty->get_template_vars("abHeaderUi");
         if($abHeaderUi == 'v3'){
            $topnav=func_display("site/megamenu_B.tpl", $smarty, false);
        }
        else{
            $topnav=func_display("site/megamenu.tpl", $smarty, false);
        }
		return $topnav;
	}
	
	public static function getGlobalCategories(){
		$solrProducts = new SolrProducts();
		
		$global_display_categories = $solrProducts->multipleFacetedSearch('*:* AND display_category:[* TO *]', array("display_category_facet"));
		
		$global_display_categories->display_category_facet=sortDisplayCategory($global_display_categories->display_category_facet);
		
		return $global_display_categories;
	}
	
	public static function getGlobalBrands(){
		$solrProducts = new SolrProducts();

		$global_brands = Top_nav::getBrandsFromQuery('*:*');

		return $global_brands;
	}
	
	public static function getBrandsFromQuery($query, $ranges = array(0 => array("rangeText" => "A - D", "solrQuery" => "brands_filter_facet:[A TO E] OR brands_filter_facet:[a TO e] OR brands_filter_facet:[0 TO 9]"),
						1 => array("rangeText" => "E - L", "solrQuery" => "brands_filter_facet:[E TO M] OR brands_filter_facet:[e TO m]"),
						2 => array("rangeText" => "M - P", "solrQuery" => "brands_filter_facet:[M TO Q] OR brands_filter_facet:[m TO q]"),
						3 => array("rangeText" => "Q - Z", "solrQuery" => "brands_filter_facet:[Q TO Z] OR brands_filter_facet:[q TO z]")
						)){
							
		$dontShowOOSProducts = FeatureGateKeyValuePairs::getFeatureGateValueForKey("dontShowOOSProducts");
		if((!empty($dontShowOOSProducts) && $dontShowOOSProducts == 'true'))
			$query = $query. " count_options_availbale:[1 TO *]";
										
		$solrProducts = new SolrProducts();
		$brandsPresent = array();
		foreach ($ranges as $key=>$value){
			$tmpRes = $solrProducts->multipleFacetedSearch($query . " AND (" . $value['solrQuery'] .")" ,array("brands_filter_facet"),0,0,1,"index");
			$tmpArr = get_object_vars($tmpRes->brands_filter_facet);
			uksort($tmpArr,strcasecmp);
			$brandsPresent[$key]['result']['brands_filter_facet'] = $tmpArr;
			$brandsPresent[$key]['range'] = $value['rangeText'];
			$brandsPresent[$key]['count'] = count((array)$brandsPresent[$key]['result']['brands_filter_facet']);
			
		}
		return $brandsPresent;
	}
}	


include_once($xcart_dir."/include/class/widget/headerMenu.php");
$top_nav = $xcache->fetch($xcart_http_host . '-' . "top_nav-".$abHeaderUi);
if($top_nav == NULL || $nocache == 1){
    $top_nav = Top_nav::getTopNavMegaMenu($smarty);
    if($nocache != 1){//Do not store in case nocache param is set
        $xcache->store($xcart_http_host . '-' . "top_nav-".$abHeaderUi, $top_nav);
    }
}
$global_brands = json_decode($xcache->fetch($xcart_http_host . '-' . "global_brands"), true);
if($global_brands == NULL || $nocache == 1){
    $global_brands = Top_nav::getGlobalBrands();
    if($nocache != 1){//Do not store in case nocache param is set
   		$xcache->store($xcart_http_host . '-' . "global_brands", json_encode($global_brands));
    }
}
$global_categories = json_decode($xcache->fetch($xcart_http_host . '-' . "global_categories"));
if($global_categories == NULL || $nocache == 1){
    $global_categories = Top_nav::getGlobalCategories();
    if($nocache != 1){//Do not store in case nocache param is set
        $xcache->store($xcart_http_host . '-' . "global_categories", json_encode($global_categories));
    }
}

$smarty->assign("top_nav", $top_nav);
$smarty->assign("global_brands", $global_brands->brands_filter_facet);
$smarty->assign("global_display_categories", $global_categories->display_category_facet);

