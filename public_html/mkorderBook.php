<?php

if(file_exists("./auth.php")) {
	include_once("./auth.php");
} else if (file_exists("../auth.php")) {
	include_once("../auth.php"); // happens for secure pages
}
/*include_once("$xcart_dir/include/class/instrumentation/BaseTracker.php");
$tracker = new BaseTracker(BaseTracker::CONFIRMATION);*/
include_once("$xcart_dir/include/class/class.orders.php");
include_once("$xcart_dir/include/func/func.mk_orderbook.php");
include_once("$xcart_dir/include/func/func.RRRedeemPoints.php");
include_once ("$xcart_dir/modules/coupon/database/CouponAdapter.php");
include_once("$xcart_dir/tracking/trackingUtil.php");
include_once ("$xcart_dir/include/class/mcart/class.MCart.php");
include_once ("$xcart_dir/include/class/search/UserInterestCalculator.php");
require_once ("$xcart_dir/icici/ajax_bintest.php");
include_once("$xcart_dir/include/class/oms/OrderWHManager.php");
include_once("$xcart_dir/modules/apiclient/ReleaseApiClient.php");
require_once($xcart_dir."/modules/giftcards/GiftCardsHelper.php");
include_once \HostConfig::$documentRoot."/include/class/class.mymyntra.php";
include_once($xcart_dir."/include/class/shopfest/ShopFest.php");
include_once($xcart_dir."/include/class/oms/OrderProcessingMailSender.php");
include_once("$xcart_dir/include/class/oms/EventCreationManager.php");
include_once "$xcart_dir/modules/myntCash/MyntCashTransaction.php";
include_once "$xcart_dir/modules/myntCash/MyntCashService.php";


use revenue\payments\gateway\AtomGateway;
use revenue\payments\gateway\CODGateway;
use revenue\payments\gateway\AxisBankGateway;
use revenue\payments\dao\AxisBankDAO;
use revenue\payments\gateway\CCAvenueGateway;
use revenue\payments\gateway\EBSGateway;
use revenue\payments\gateway\HDFCBankGateway;
use revenue\payments\gateway\ICICIBankGateway;
use revenue\payments\gateway\TekProcessGateway;
use revenue\payments\gateway\CITIBankGateway;
//use shopfest\ShopFest;


use enums\revenue\payments\CardType;
use enums\revenue\payments\PaymentType;
use enums\revenue\payments\Gateways;

use common\Util;
/*use enums\base\SizeChartTrackerConstants;*/
use abtest\MABTest;

$adapter = CouponAdapter::getInstance();

$redirectToInCaseOfReload = $http_location;

if($_GET['xid']!="") $session_id = $_GET['xid'];

else $session_id = $XCARTSESSID;

if(isset($_GET['admincodloginid'])) {
	$login = $_GET['admincodloginid'];
}

if(x_session_is_registered("codErrorCode")){
		$codErrorCode = $XCART_SESSION_VARS['codErrorCode'];
		$smarty->assign("codErrorCode",$codErrorCode);		
	}

//AB Test for payment failure options
$smarty->assign("payFailOpt", MABTest::getInstance()->getUserSegment('payFailOpt'));
//AB Test for user closing overlay
$smarty->assign("payUserCloseOpt", MABTest::getInstance()->getUserSegment('payUserCloseOpt'));

/* Check if it si gifting ortder and then cast just the order id to see if its a integer or not. */
if(!(strpos($orderid, "G_") === false)) {
       $orderid = str_replace("G_", "", $orderid);
       $orderid=(integer)$orderid;
       $orderid="G_".$orderid;
}
else{
       $orderid=(integer)$orderid;
}

$isGiftCardOrder = false;
$paymentPageUrlToRedirectTo=$https_locations . "/mkpaymentoptions.php?transaction_status=N";
if(empty($orderid)&&!empty($_REQUEST['msg'])) {
	//tekprocess
	$tek_msg=trim($_REQUEST['msg']);
	$tek_msg_array=explode("|",$tek_msg);
	if(!empty($tek_msg_array)) {
		$orderid =$tek_msg_array[1];
		if($configMode=="release"||$configMode=="test" ||$configMode=="local") {
			//we are appending a extra char to identify the environment
			if(!(strpos($orderid, "G_") === false)) {
				$isGiftCardOrder = true;
				$orderid = str_replace("G_", "", $orderid);				
			}
			$orderid = substr($orderid, 1);
		}
	} else {
		func_header_location($redirectToInCaseOfReload,false);
	}		
}

if(empty($orderid)&&!empty($_POST['CititoMall'])) {
	//CITIBank

	$citi_msg=$_POST['CititoMall'];
	$citi_msg_array=explode("|",$citi_msg);
	if(!empty($citi_msg_array)) {
		$orderid =$citi_msg_array[4];

/* 		if($configMode=="release"||$configMode=="test" ||$configMode=="local") {
			//we are appending a extra char to identify the environment
			$orderid = substr($orderid, 1);
		}
 */	} else {
		func_header_location($redirectToInCaseOfReload,false);
	}
}

//Check for gifting and redirect
if($isGiftCardOrder || !(strpos($orderid, "G_") === false)) {
	$isGiftCardOrder=false;
	$giftCardOrderId = $XCART_SESSION_VARS['gcorderID'];
	$orderid = str_replace("G_", "", $orderid);
	$smarty->assign("isGiftCardOrder",$isGiftCardOrder);
	if(!empty($giftCardOrderId) && $orderid == $giftCardOrderId){
		$orderId=$giftCardOrderId;
		$isGiftCardOrder=true;		
		$smarty->assign("isGiftCardOrder",$isGiftCardOrder);
		include_once($xcart_dir."/mkGiftCardOrderBook.php");
		exit;
	}
}

if(empty($orderid)) {
	func_header_location($redirectToInCaseOfReload,false);	
}
$orderid=mysql_real_escape_string($orderid);
//for ebs for myntra.com
$secret_key = "b9e5685ab6439dcaa55013a27f44decf";	 // Your Secret Key
//secret key for testing
//$secret_key="ebskey";
$order_result = func_query("SELECT * FROM " . $sql_tbl['orders'] . " WHERE orderid='". $orderid ."'");	

// capture the session dump in the beginning
##################################################################################

$orderid = strip_tags($orderid);
$orderid = sanitize_int($orderid);

if(empty($login)){
    $errormessage = "Ooops! There was an error in order processing and hence your order could not be placed. If you have been charged, the charged amount would be refunded back to your banking or credit card account within 2 working days. Please contact support@myntra.com for more details.";

    $XCART_SESSION_VARS['errormessage'] = $errormessage;

	func_header_location($redirectToInCaseOfReload,false);
    //header("Location:mksystemerror.php");
} else {
	#
	#Check if login and orderid is maped
	#
	
	$orderLogin = $order_result[0]['login'];
	if(strcasecmp($orderLogin, $login) !=0) {
		$errormessage = "We are sorry, You are trying to access wrong order id.";
	    $XCART_SESSION_VARS['errormessage'] = $errormessage;	    
	    func_header_location($https_location . "/mkpaymentoptions.php?transaction_status=N");
    	//header("Location:mksystemerror.php");
	} else {

		$orderLogDetails = func_query_first("select * from mk_payments_log where orderid = $orderid");
		$gatewayWentTo = $orderLogDetails['payment_gateway_name'];
		$paymentoption = $order_result[0]['payment_method'];
		$prev_status = $order_result[0]['status'];
		$couponCode = $order_result[0]['coupon'];
		$cashCouponCode = $order_result[0]['cash_coupon_code'];
		$cashdiscount = $order_result[0]['cash_redeemed'];
		$codCharge = $order_result[0]['cod'];
		$order_channel=  $order_result[0]['channel'];
		$isPaymentDataCorrect = false;
		$isPaymentSuccessfull = false;
		$amountToBePaid = $order_result[0]['subtotal'] - $order_result[0]['coupon_discount'] - $order_result[0]['cash_redeemed'] + $order_result[0]['cod'] + $order_result[0]['payment_surcharge'] - $order_result[0]['discount'] - $order_result[0]['cart_discount'] - $order_result[0]['pg_discount'];
		$amountToBePaid = number_format($amountToBePaid, 2, ".", '');
		$amountToBePaid = $amountToBePaid < 1 ? 0 : $amountToBePaid; //less than 1re payment is not accepted by banks to making the amount to zero in case final amount falls below 1re.
		
		$couponCashDetails = array('coupon' => $order_result[0]['coupon'], 'coupon_discount' => $order_result[0]['coupon_discount'], 'cash_coupon_code' => $order_result[0]['cash_coupon_code'], 'cash_redeemed' => $order_result[0]['cash_redeemed']);
		include_once($xcart_dir."/mkOrderBookPaymentValidation.php");

		$orderid = strip_tags($orderid);
		$orderid = sanitize_int($orderid);

		include_once("./include/func/func.mk_orderbook.php");
		include_once("./include/func/func.order.php");
		include_once("./include/func/func.mkcore.php");
		include_once("./include/func/func.mail.php");
		include_once("./include/func/func.sms_alerts.php");
		
		#		
		if($isPaymentValid) {
			$mcartFactory = new MCartFactory();
			if($order_channel == 'telesales')
				$myCart= $mcartFactory->getCartForLogin($login, false, 'telesales');
			else if ($order_channel == 'telesalen')
				$myCart= $mcartFactory->getCartForLogin($login, false);
			else
				$myCart= $mcartFactory->getCurrentUserCart();
			
			//if($orderPlacementFlow == 'new'){
				EventCreationManager::pushInventoryUpdateToAtpEvent($orderid);
				EventCreationManager::pushOrderConfirmationEvent($orderid);
				$orderstatus = 'Q';
				$updateSql = "UPDATE xcart_orders SET status='Q',queueddate='".$time."', response_server='$server_name', cashback_processed = 1 WHERE orderid = '".$orderid."'";
				db_query($updateSql);
				
				func_addComment_status($orderid, "Q", $order_result[0]['status'], "Payment Successful. Queuing Order.", "AUTOSYSTEM");
				
				$order_details_for_coupon = $order_result[0];
				if($paymentoption == "cod")	{
					if(!empty($couponCode)) {
						$adapter->updateUsageByUser($couponCode,$login,$order_result[0]['subtotal'], $order_result[0]['coupon_discount'], $orderid);
						$adapter->unlockCouponForUser($couponCode, $login);
					}
			    }
				if($paymentoption == "on") {
					if(!empty($couponCode) && $prev_status != 'Q') {
						$adapter->updateUsageByUser($couponCode,
								$userName,
								$order_result[0]['subtotal'],
								$order_result[0]['coupon_discount'],
								$orderid);
						$adapter->unlockCouponForUser($couponCode, $login);
					}
				}
			/*} else {
				//first debit cashback
				$amountdetail = $order_result[0];
				$orderstatus = 'Q';
				if(!empty($amountdetail['cash_redeemed'])){
					$trs = new MyntCashTransaction($login, MyntCashItemTypes::ORDER, $orderid, MyntCashBusinessProcesses::CASHBACK_USED, 0, 0,$amountdetail['cash_redeemed'],  "Usage on order no. $orderid");
					$myntCashUsageResponse = MyntCashService::debitMyntCashForOrder($trs,$orderid);
					if($myntCashUsageResponse !== true){
						$orderstatus = myntCashDebitfailureProcedure($orderid,$myntCashUsageResponse); // handle failure cases and put the order ON HOLD
					}
				}
				
				/*
				 * Defined payment_method for few reasons.
				 * 1. this code is already buggy
				 * 2. $paymentoption is being assigned orderstatus in some cases.
				 
				$payment_method = $paymentoption;
				
				$onholdVerificationRequired = false;
				if($orderstatus == 'Q') {
					$response = check_order_onhold_rules($orderid, $login, $payment_method, $order_channel, $myCart->getTotalMyntCashUsage(), $gateway, $toLog['gateway_payment_id'], true, $gatewayWentTo, $orderLogDetails['bin_number'], $axisBankDAO);
					if($response['status'] == 'OH') {
						if($response['verificationRequired'] === true) {
							$onholdVerificationRequired = true;
							$curr_time = date('Y-m-d H:i:s');
							$sql = "insert ignore into cod_oh_orders_dispostions(orderid,disposition,reason,trial_number,created_time,created_by) values ($orderid,'OH','".$response['sub_reason']."',0,'$curr_time','SYSTEM')";
							func_query($sql);
						}
						
						$oh_reason = get_oh_reason_for_code($response['reason'], $response['sub_reason']);
						$sql = "update xcart_orders set on_hold_reason_id_fk = ".$oh_reason['id']." where orderid = $orderid";
						func_query($sql);
						$commentArrayToInsert = Array ("orderid" => $orderid, "commenttype" => "Automated", "commenttitle" => "status change", "commentaddedby" => "System", "description" => $response['description'], "commentdate" => time());
						func_array2insert("mk_ordercommentslog", $commentArrayToInsert);
						$orderstatus = 'OH';

					} else {
						$orderstatus = 'Q';
					}
					if ($order_channel == "telesalen"){
						$saleTypeArray =Array("orderid"=>$orderid,
						               "sales_agent"=>$telesalesUser);
						func_array2insertWithTypeCheck("mk_tele_sales",$saleTypeArray);                            
					}
				    if($myCart!=null){                
				    	$productArray=MCartUtils::convertCartToArray($myCart);
				    }
					$productsInCartTemp=$productArray;
					#############LOG USER BEHAVIOUR#############
				    foreach ($productsInCartTemp as $individualProduct)
				    {
				    	UserInterestCalculator::trackAction($individualProduct['productStyleId'], UserInterestCalculator::PURCHASE);
				    }
				    ##########################################
				    
					$productsInCart = func_create_array_products_of_order($orderid, $productsInCartTemp);
				    
					#
					#Query to fetch the order details like couponcode, tax, totalamount etc.
					#
				    $customer = $order_result[0];//func_customer_order_address_detail($login, $orderid);
				    $CustomerFirstName = $customer['firstname'];
				    $CustomerLastName = $customer['lastname'];
					$amountdetail['zipcode'] = $order_result[0]['s_zipcode'];
					$amountdetail['discount'] = $amountdetail['discount'] + $amountdetail['cash_discount'] ;
                    $amountdetail['loyalty_credit'] = $amountdetail['loyalty_points_used']*$amountdetail['loyalty_points_conversion_factor'];
					$cashCouponCode=$amountdetail['cash_coupon_code'];
					$cashdiscount=$amountdetail['cash_redeemed'];
					$couponCode = $amountdetail['coupon'];
					$coupondiscount = $amountdetail['coupon_discount'];
					$productDiscount = $amountdetail['discount'];
					$cartDiscount = $amountdetail['cart_discount'];
					$pg_discount = $amountdetail['pg_discount'];
					$emi_charge = $amountdetail['payment_surcharge'];
					$order_time = null;//ordertime::get_order_time_string($orderid);
					$order_time_data = null;//ordertime::get_order_time_data($orderid);
					$emailto = $login;
					$customerMobileNo = $amountdetail['issues_contact_number'];
					$customerFName = $amountdetail['firstname'];
		
					// determine if cashback should be given on the orderid
					global $casback_earnedcredits_enabled;
					$process_cashback = true;
					if (!$casback_earnedcredits_enabled) {
						//update cashback_processed flag for the order
						func_array2update('orders',array('cashback_processed' => 1), "orderid=".$orderid);
						$process_cashback = false;
					}
					$invoiceid = func_generate_invoice_id($orderid, $orderstatus, $session_id,$process_cashback,$amountdetail);
					for($j=0; $j<3; $j++){
						$optionCounter[$j]= $j;
					}
		   
					$smarty->assign("optionCounter",$optionCounter);
					$smarty->assign("productsInCart",$productsInCart);
					$smarty->assign("grandTotal",number_format($amountdetail['subtotal'],2,".",''));
					$smarty->assign("coupondiscount",number_format($coupondiscount,2,".",''));
					$smarty->assign("couponCode",$couponCode);
					$smarty->assign("totalDiscount",number_format($productDiscount+$coupondiscount+$pg_discount+$cartDiscount,2,".",''));
					$smarty->assign("giftamount",number_format($amountdetail['gift_charges'],2,".",''));
					$smarty->assign("emi_charge",number_format($amountdetail['payment_surcharge'],2,".",''));
				
					$smarty->assign("userinfo", $customer);
					$smarty->assign("shippingRate", number_format($amountdetail['shipping_cost'],2,".",''));
					$smarty->assign("codCharges", number_format($amountdetail['cod'],2,".",''));
					$smarty->assign("vat", number_format($amountdetail['tax'],2,".",''));
					$smarty->assign("ref_amount",number_format($amountdetail['ref_discount'],2,".",''));
				
					#
					#Calculate Net amount to be paid
					#
			
		        	$amountafterdiscount = $amountdetail['total'] + $amountdetail['cod'];
		
					if($amountdetail['ref_discount'] > '0'){
						$amtAfterRefDeducton = $amountdetail['total'] - $amountdetail['ref_discount'];
						$amtAfterRefDeducton = $amtAfterRefDeducton + $amountdetail['cod'];
						$amount = $amtAfterRefDeducton;
					} else {
						$amount = $amountafterdiscount;
					}
					
					if($paymentoption == "cod" && $orderstatus == "OH" && $onholdVerificationRequired) {
						$callNumber = WidgetKeyValuePairs::getWidgetValueForKey('customerSupportCall');
						$msg = "Dear $CustomerFirstName, We need to verify some details to confirm your order with order id $orderid. We will contact you at your phone number $customerMobileNo by the end of day today or tomorrow morning. If you prefer, you can call our customer support at $callNumber at your convenience to confirm this order";
						$amountToPay = $amountdetail['total']+$amountdetail['cod'];
						OrderProcessingMailSender::notifyCodOnHoldOrderManualVerification($productsInCart,$amountdetail,$CustomerFirstName,$login,$orderid,0,$customer);
					} else {
						$delivery_date = date("jS M Y", getDeliveryDateForOrder($amountdetail['date'], $productsInCart, $amountdetail['zipcode'], $paymentoption, false, false, $orderid));
						$msg = "Hi ". $CustomerFirstName. ", Your order with order id ".$orderid." has been successfully placed. It will be shipped from our warehouse within the next 24 hours. Thank you for shopping at Myntra.com";
						if($paymentoption == "chq"){
							OrderProcessingMailSender::notifyOrderConfirmation('check',$productsInCart,$amountdetail,$CustomerFirstName,$login,$orderid,$customer);
						} else if($paymentoption == "NP" || $paymentoption == "on"){
							OrderProcessingMailSender::notifyOrderConfirmation('on',$productsInCart,$amountdetail,$CustomerFirstName,$login,$orderid,$customer);
						} else if($paymentoption == "cod"){
							OrderProcessingMailSender::notifyOrderConfirmation('cod',$productsInCart,$amountdetail,$CustomerFirstName,$login,$orderid,$customer);
						}
					}
					
					$weblog->info("sms on order confirmation order id :: $orderid, mobile :: $customerMobileNo, msg :: $msg");
					func_send_sms_cellnext($customerMobileNo,$msg);
					
					if($paymentoption == "cod")	{
						$couponCode=$amountdetail['coupon'];
						$cashCouponCode=$amountdetail['cash_coupon_code'];
						$cashdiscount=$amountdetail['cash_redeemed'];
						if(!empty($couponCode)) {
							// In case some coupon code was applied, we need to unlock it.
							$adapter->updateUsageByUser($couponCode,$login,$amountdetail['subtotal'], $amountdetail['coupon_discount'], $orderid);
							//Unlock the coupon.
							$adapter->unlockCouponForUser($couponCode, $login);
						}
				    }
					
					if($paymentoption == "on") {
						// Update the coupon usage, if it hasn't already been updated for this order.
						if(!empty($couponCode) && $prev_status != 'Q') {
							$userName = $XCART_SESSION_VARS['login'];
							$adapter->updateUsageByUser($couponCode,
									$userName,
									$amountdetail['subtotal'],
									$amountdetail['coupon_discount'],
									$orderid);
					
							// Unlock the coupon.
							$adapter->unlockCouponForUser($couponCode, $userName);
						}
					}
				
					// AS part of new wms.. the order should be moved immediately to WP state as there is not going to
					// be any manual assignment.
					// also move item_status to assign it to OPS Manager which is going to be id 1.
					if($orderstatus == 'Q') {
						$weblog->info("Move order to WP state. Current Status - $orderstatus.");
						$order_query = "UPDATE $sql_tbl[orders] SET status ='WP' WHERE orderid = $orderid";
						db_query($order_query);
						$order_detail_query = "UPDATE $sql_tbl[order_details] SET item_status = 'A', assignee=1, assignment_time = ".time()." where orderid = $orderid";
						db_query($order_detail_query);
						func_addComment_status($orderid, "WP", "Q", "Auto moving order to WP state from Q", "AUTOSYSTEM");
					}
					
					// All successfully done
					// check if this order needs to be split.
					$courierServiceabilityVersion = FeatureGateKeyValuePairs::getFeatureGateValueForKey('lms.courierserviceability.version', 'old');
					$weblog->info("OrderWHManager: start");
					if($myCart && $myCart != null) {
						$myCart->refresh(false,false,false,true);
						$sku_details = $myCart->getAllSkus();
					}
					if($courierServiceabilityVersion == 'new') {
						$whIdToOrderIds = \OrderWHManager::assignWarehouseForOrder($orderid, $customer, $productsInCart, $sku_details);
						if($whIdToOrderIds) {
							\OrderWHManager::updateInventoryAfterSplit($orderid, $whIdToOrderIds, "Order created from $orderid and moved to WP state", "AUTOSYSTEM");
							if($orderstatus == 'Q') {
								$releaseIdsToPush = array();
								foreach($whIdToOrderIds as $whId=>$oids) {
									$releaseIdsToPush = array_merge($releaseIdsToPush, $oids);
								}
								\ReleaseApiClient::pushOrderReleaseToWMS($releaseIdsToPush);
							}
						}
					} else {
						$whIdToOrderId = \OrderWHManagerOld::assignWarehouseForOrder($orderid, $customer, $productsInCart, $sku_details);
						if($whIdToOrderId) {
							\OrderWHManagerOld::updateInventoryAfterSplit($orderid, $whIdToOrderId, "Order created from $orderid and moved to WP state", "AUTOSYSTEM");
							if($orderstatus == 'Q') {
								\ReleaseApiClient::pushOrderReleaseToWMS(array_values($whIdToOrderId));
							}
						}
					}
					// myntra festival leaderboard data being processed ***
					
					if(FeatureGateKeyValuePairs::getBoolean("myntrashoppingfestleaderboard")){
						ShopFest::updateLeaderBoardData(($amountafterdiscount+$cashdiscount),$login,time(),$productsInCart);
					}
					
					// myntra festival leaderboard data processing ends ***
				}
				 
				$weblog->info("OrderWHManager: end");
				EventCreationManager::pushCompleteOrderEvent($orderid);
				EventCreationManager::pushInventoryUpdateToAtpEvent($orderid);
					
				#
				#If amount data greater than or equal to offer amount
				#
				$amountafterdiscount = number_format($amountafterdiscount,2,".",'');
				
				//retargeting tracking code
				$retargetPixel = retargetingPixelOnType($productsInCart,'PID');
				if(!empty($retargetPixel)){
				    $smarty->assign("retargetPixel",$retargetPixel);    
				}
			
				$smarty->assign("amountafterdiscount", number_format($amountafterdiscount,2,".",''));
				$smarty->assign("amtAfterRefDeducton", number_format($amtAfterRefDeducton,2,".",''));
				$smarty->assign("mobile", $customer['mobile']);
				$smarty->assign("address", stripslashes($customer['s_address']));
				$smarty->assign("pincode", $customer['s_zipcode']);
				$smarty->assign("phone", $customer['phone']);
				$smarty->assign("fax", $customer['fax']);
				$smarty->assign("email", $customer['login']);
				$smarty->assign("paymentoption", $paymentoption);
					
				$smarty->assign("djvalue", number_format($amountdetail['total'],2,".",''));
				$smarty->assign("qtyInOrder", $amountdetail['qtyInOrder']);
					
				//setting order details to display
				$today = date("d-m-Y");
				$smarty->assign("today",$today);
				$smarty->assign("invoiceid",$invoiceid);
				$smarty->assign("orderid",$orderid);
				$smarty->assign("payment_method",$payment_method);
			} */

			//capturing channel(utm_source and utm_medium) info for order
			$eventInfo = array('event_type'=>'order');
			//to capture an identifiable int value for tracking to optimize audit
			if(is_int($orderid) || is_numeric($orderid)){
				$eventInfo += array('event_value'=>$orderid);
			}
			utmTrackForEvent($eventInfo);
			
			// Cleanup all the unnecessary images
			$masterCustomizationArrayD=$XCART_SESSION_VARS['masterCustomizationArrayD'];
			
			$myCart->setOrderID($orderid);
			MCartUtils::deleteCart($myCart, true);
			x_session_unregister('productids');
			x_session_unregister('cart');
			x_session_unregister('coupondiscount');
			x_session_unregister('couponCode');
			x_session_unregister('cashdiscount');
			x_session_unregister('cashCouponCode');
			x_session_unregister('userCashAmount');
			x_session_unregister('productDiscount');
			x_session_unregister('masterCustomizationArrayD');
			x_session_unregister('offers');
			x_session_unregister('totaloffers');
			x_session_unregister('totaldiscountonoffer');
			x_session_unregister('OrderTotal');
			x_session_unregister('giftdata');
			x_session_unregister('totalQuantity');
			
			x_session_unregister('issuer_voucher_code');//unregistering brandstore voucher code
			x_session_unregister('order_id'); //unregistering the order id
			/*x_session_unregister("orderID");*/
				
			if(x_session_is_registered("lastAddressUsedID")){
				x_session_unregister("lastAddressUsedID");
			}
			$uuid = stripslashes(getMyntraCookie('MYNTRA_SHOPPING_ID'));
			setMyntraCookie("MYNTRA_UNQ_COOKIE_ID",substr($uuid,1,20),time() - 3600,'/',$cookiedomain);
			if (x_session_is_registered("chosenProductConfiguration"))
				x_session_unregister("chosenProductConfiguration");
			
			if (x_session_is_registered("masterCustomizationArray"))
				x_session_unregister("masterCustomizationArray");
			
			if (x_session_is_registered("masterCustomizationArrayD"))
				x_session_unregister("masterCustomizationArrayD");
			
			$orderstatus = "Y";
			$smarty->assign("orderstatus",$orderstatus);
		}
	}
	if(!$isPaymentSuccessfull)
	{
		$card_bank_name = null;
		$cvvToRespond = null;
		$vbvFail = false;
		if($gatewayWentTo==Gateways::ICICIBank) {
			$vbvStatus = $gatewayResponse['status'];
			if($vbvStatus!='Y'){
				// Verification of card has failed.
				$vbvFail=true;
				$issuing_bank = $orderLogDetails['card_bank_name'];
				$card_bank_name = func_query_first_cell("select card_bank_code from mk_cards_domestic_bins where bank_name='$issuing_bank' limit 1");
			} else {
				//Verification of card is successfull but transaction denied due to other reasons.
				 $cvvStatus = $gatewayResponse['gateway_cvv_response_code'];
				 if(!empty($cvvStatus)){
				 	if($cvvStatus=='C'){
				 		//Card expired
				 		$cvvToRespond = "expired";
				 	}
				 }
			}		
		}
		$failureRedirectURL = $https_location . "/mkpaymentoptions.php?transaction_status=N";
		$smarty->assign("transaction_status","N");
		if($vbvFail) {
			$failureRedirectURL = $failureRedirectURL . "&vbv=F";
			$smarty->assign("vbvfail",$vbvFail);
		}
		if(!empty($card_bank_name)) {
			$failureRedirectURL = $failureRedirectURL . "&bank_name=$card_bank_name";
		}
		$message = "We are sorry, your payment has not been successful.Please <a href='".$http_location."/mkpaymentoptions.php?pagetype=productdetail'>click here</a> to try again";
		$orderstatus = "N";
		$smarty->assign("orderstatus",$orderstatus);
		$sql = "UPDATE ".$sql_tbl['orders']." SET tf_status ='TF',status='D',response_server='$server_name'  WHERE orderid = '".intval($orderid)."' ";  ### Update the table in case transaction get failed 
		db_query($sql);
		$userName = $XCART_SESSION_VARS['login'];
		//unlock the coupon in case transaction is declined
		if(!empty($couponCode) && $prev_status != 'Q') {	    
			// Unlock the coupon.
			$adapter->unlockCouponForUser($couponCode, $userName);
		}
		
		if(!empty($cashCouponCode) && $prev_status != 'Q') {
			// Unlock the coupon.
			$adapter->unlockCouponForUser($cashCouponCode, $userName);
		}
		$redirectURL = $failureRedirectURL;
		$smarty->assign('redirect_url',$redirectURL);
		$smarty->assign('redirect_message',"Payment Failed");
		$smarty->assign("isSuccessOrder",false);
		func_display("cart/orderProcessing.tpl", $smarty);
		exit;
	}
	$smarty->assign("message",$message);
	
	//get delivery date based on ordertime and logistic feature gate value
	$smarty->assign("login_email",$login);
	
	if(isset($_GET['admincodloginid'])) {
		// Need to logout the user, if we were redirected from Admin to confirm a pending COD order
		unset($XCART_SESSION_VARS['identifiers']['C']);
		unset($identifiers['C']);
		
		if (x_session_is_registered("chosenProductConfigurationEx"))
	            x_session_unregister("chosenProductConfigurationEx");
	            
		if (x_session_is_registered("customizationArrayEx"))
	            x_session_unregister("customizationArrayEx");
	
		if (x_session_is_registered("masterCustomizationArrayEx"))
			x_session_unregister("masterCustomizationArrayEx");
	}
	if(!empty($_GET['codconfirmmode'])&&$_GET['codconfirmmode']=='telesalesconverttocod'){
		   $mcartFactory= new MCartFactory();
	       $myCart= $mcartFactory->getCartForLogin($login,false,'telesales');
	       if($myCart!=null){
	       	$myCart->setOrderID($orderid);
	       	MCartUtils::deleteCart($myCart, true);
	       }
	       /*$tracker->fireRequest();*/
		echo"Converted to COD!";
		exit;
	}else if(!empty($_GET['codconfirmmode'])&&$_GET['codconfirmmode']=='telesalesdirectcod'){
	 	   $mcartFactory= new MCartFactory();
	       $myCart= $mcartFactory->getCartForLogin($login,false,'telesales');
	       if($myCart!=null){
	       	$myCart->setOrderID($orderid);
	       	MCartUtils::deleteCart($myCart, true);
	       }
	       /*$tracker->fireRequest();*/
		echo"COD Order Created!";
		exit;	
	} else if(!empty($_GET['codconfirmmode'])) {
		$url = "$http_location/admin/order.php?orderid=$orderid&trackevent=true&utm_source=admin&utm_medium=pptocod";
		/*$tracker->fireRequest();*/
		func_header_location($url);
		exit;	
	}
	$redirectURL = $https_location . "/confirmation.php?orderid=$orderid";
	$smarty->assign('redirect_url',$redirectURL);
	$smarty->assign('redirect_message',"Payment Successfull");
	$smarty->assign("isSuccessOrder",true);
	func_display("cart/orderProcessing.tpl", $smarty);
}

if(extension_loaded('newrelic')){
	newrelic_custom_metric ('orders', 1.0);
}
/*$tracker->fireRequest();*/

function _calculate_from_notification_matrix($notification_matrix){
	$returnval = array();
	list($returnval['customer']['confemail'],$returnval['customer']['confsms'],$returnval['customer']['shipemail'],$returnval['customer']['shipsms'],$returnval['store']['confemail'],$returnval['store']['confsms'],$returnval['store']['shipemail'],$returnval['store']['shipsms'],$returnval['shopmaster']['confemail'],$returnval['shopmaster']['confsms'],$returnval['shopmaster']['shipemail'],$returnval['shopmaster']['shipsms'],$returnval['myntra']['confemail'],$returnval['myntra']['confsms'],$returnval['myntra']['shipemail'],$returnval['myntra']['shipsms']) = split('[;,]',$notification_matrix);
	return $returnval;
}
/**
* Kundan: now we track: out of the order items, which were the styles for which user clicked on their size charts: old or new
* We send this array of styles to Mongo DB
* @param mcart $cart
*/
/*function getStyleIdsWithSizeChartClicked($cart) {
	$styleIdsInCart = $cart->getStyleIds();
	$styleIdsWithOldSizeChartClicks = getMatchingStyleIds($styleIdsInCart, SizeChartTrackerConstants::OldSizechartCookie, SizeChartTrackerConstants::Delimiter);
	$styleIdsWithNewSizeChartClicks = getMatchingStyleIds($styleIdsInCart, SizeChartTrackerConstants::NewSizechartCookie, SizeChartTrackerConstants::Delimiter);
	return array("old"=>$styleIdsWithOldSizeChartClicks, "new"=>$styleIdsWithNewSizeChartClicks);
}

function getMatchingStyleIds($styleIdsInCart, $cookieName, $delimiter = "|") {
	$cookieValue = getMyntraCookie($cookieName);
	$matchedStyleIds;
	if(!empty($cookieValue)) {
		$styleIdsInCookie = explode($delimiter, $cookieValue);
		$matchedStyleIds = array_intersect($styleIdsInCart, $styleIdsInCookie);
	}
	return $matchedStyleIds;
}

function removeSizeChartClickedCookies() {
	deleteMyntraCookie(SizeChartTrackerConstants::OldSizechartCookie);
	deleteMyntraCookie(SizeChartTrackerConstants::NewSizechartCookie);
}*/

function objectToArray($d) {
	if (is_object($d)) {
		// Gets the properties of the given object
		// with get_object_vars function
		$d = get_object_vars($d);
	}

	if (is_array($d)) {
		/*
		* Return array converted to object
		* Using __FUNCTION__ (Magic constant)
		* for recursive call
		*/
		return array_map(__FUNCTION__, $d);
	}
	else {
		// Return array
		return $d;
	}
}
?>
