<?php
// This is to continue executing script even if user closes the browser or hits esc ..
ignore_user_abort(true);

include_once("./auth.php");
include_once("./include/func/func.randnum.php");
include_once("./include/func/func.mkmisc.php");
include_once("./include/func/func.mkspecialoffer.php");
include_once("./include/class/customization/CustomizationHelper.php");

$window_id = $_GET["windowid"];

$productsInCart = $XCART_SESSION_VARS['productsInCart'];


$thisChosenProductConfiguration = $XCART_SESSION_VARS["chosenProductConfigurationEx"][$window_id];
$thisCustomizationArray = $XCART_SESSION_VARS["customizationArrayEx"][$window_id];
$thisMasterCustomizationArray = $XCART_SESSION_VARS["masterCustomizationArrayEx"][$window_id];
$globalDataArray = $XCART_SESSION_VARS["globalDataArray"];

$params = array("custArray" => $thisCustomizationArray, "masterCustomizationArray" => $thisMasterCustomizationArray, "chosenProductConfiguration" => $thisChosenProductConfiguration, "globalDataArray" => $globalDataArray);
$customizationHelper = new CustomizationHelper($params);


// Actual generation of images ..
$areas = $customizationHelper->getAreaNames();
foreach ($areas as $area)
{
    $customizationHelper->setSelectedAreaName($area);
    $areaInfo = $customizationHelper->getCustomizationArrayForCurrentSelectedArea();
    if ($areaInfo["is_customized"] == 1) {
        $area_key = $areaInfo["areaid"];

        // now generate all orientation images
        $orientations = $customizationHelper->getOrientationNames();
        foreach ($orientations as $orientation)
        {
            $customizationHelper->setSelectedOrientationName($orientation);
            $orientationInfo = $customizationHelper->getCustomizationArrayForCurrentSelectedOrientation();
            $orientation_key = $orientationInfo["orientationid"];
            $allObjects = $productsInCart[count($productsInCart)- 1]["custominfo"]["areas"][$area_key]["orientations"][$orientation_key];

            $customizationHelper->generateOrientationImage($orientation, $orientationInfo, true, $allObjects["image_name"], true);

/*
            $allTgtFiles["300"] = array("width" => 300, "height" => 300, "tgtFile" => $allObjects["pop_up_image_path"]);
            $allTgtFiles["160"] = array("width" => 160, "height" => 160, "tgtFile" => $allObjects["image_portal_thumbnail_160"]);
            $allTgtFiles["100"] = array("width" => 100, "height" => 100, "tgtFile" => $allObjects["image_portal_t"]);

            $allTgtFiles = $customizationHelper->scaleImagesEx($allObjects["image_name"], $allTgtFiles);
*/


        }
    }
}

unset($XCART_SESSION_VARS["chosenProductConfigurationEx"][$windowid]);
unset($XCART_SESSION_VARS["customizationArrayEx"][$windowid]);
unset($XCART_SESSION_VARS["masterCustomizationArrayEx"][$windowid]);
?>
