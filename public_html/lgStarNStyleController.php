<?php
require_once "auth.php";
require_once 'myntra/navhighlighter.php';
require_once($xcart_dir."/modules/lg/LookGoodAPI.php");
use seo\SeoStrategy;
use seo\SeoPageType;
use macros\PageMacros;

$pageName = 'landing';
include_once $xcart_dir."/webengage.php";
include_once $xcart_dir."/gtm.php";

$starId = filter_input(INPUT_GET,"id", FILTER_SANITIZE_STRING);

$starNStyleList=LookGoodAPI::getAllStarNStyle();

$starNStyleCount=count($starNStyleList);

if(empty($starId)){
	$starId=$starNStyleList[0][0];
}
$starNStyleDetails=LookGoodAPI::getStarNStyle($starId);
if(empty($starNStyleList) || empty($starNStyleDetails)){
	$smarty->assign("starNStyleError",1);
}


$starNStyleImageConfig=$starNStyleDetails->starNStyleImageConfig;

$nav_arr=array('STAR \'N\' STYLE'=>'/starnstyle');

//Macros
$macros = new PageMacros('STATIC');
$macros->url = currentPageURLWithStripedQueryStringsAndPort();
$macros->page_type = SeoPageType::STATIC_PAGE;
$macros->page_name = 'StarnStyle';

$seoEnabled=FeatureGateKeyValuePairs::getBoolean('SeoV3Enabled');
if($seoEnabled){
    try {
		$handle = Profiler::startTiming("seov3_load");
        $seoStrategy = new SeoStrategy($macros);
        Profiler::endTiming($handle);
        $smarty->assign("seoStrategy",$seoStrategy);
    } catch (Exception $e) {
        $seolog->error($staticPage." :: ".$e->getMessage());
    }
}

$smarty->assign("nav_selection_arr_url", $nav_arr);


$smarty->assign("starNStyleList",$starNStyleList);
$smarty->assign("starNStyleImageConfig",$starNStyleImageConfig);
$smarty->assign("starNStyleImage",$starNStyleDetails->image);
$smarty->assign("starNStyleName",$starNStyleDetails->name);
$smarty->assign("starNStyleDescription",$starNStyleDetails->description);
$smarty->assign("starNStyleID",$starId);
func_display("lg/star_n_style.tpl", $smarty);
