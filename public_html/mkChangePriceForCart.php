<?php

/*
 * Author : Vaibhav Bajpai
 * This is a ajax call script that return product options for a given style
 *
 * @param windowId string window id as a parameter for ajax call
 * @param style_product_options array product options for a given style id
 *
 * @return style options info
 */ 
 
include("auth.php");
include_once("include/func/func.mkcore.php");
include_once("include/func/func.mkmisc.php");
include_once("include/class/customization/CustomizationHelper.php");

$productOptions = $XCART_SESSION_VARS["chosenProductConfigurationEx"][$_POST['windowid']]['style_product_options'];

$optionCount = count($productOptions);

for($i=0; $i < $optionCount ; $i++) {

	if ($productOptions[$i][2] == $_POST["optionname"]) {

		$XCART_SESSION_VARS["chosenProductConfigurationEx"][$_POST['windowid']]['unitPrice'] = $productOptions[$i][3] ;
		$XCART_SESSION_VARS["chosenProductConfigurationEx"][$_POST['windowid']]['totalQuantity'] = 1;
		echo 'success#'.$productOptions[$i][3];
		exit();
	}
}

echo "Failed to get require price data for product options";
?>
