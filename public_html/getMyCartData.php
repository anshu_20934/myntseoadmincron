<?php
chdir(dirname(__FILE__));
include_once("./auth.php");
$productsInCart = $XCART_SESSION_VARS["productsInCart"];

function getSelectedSize($value){
	if($value['flattenSizeOptions']) {
		$productsizenames= $value['sizenamesunified'];
	} else {
		$productsizenames =$value['sizenames'];
	}
	
	if(!empty($value['sizenames']) && !empty($value['sizequantities'])) {
		if (count($value['sizenames'])>1){
	    	foreach($productsizenames as $item=>$sizename) {
	        	if($value['sizequantities'][$item]) {
	            	$selectedSize=$productsizenames[$item];
	            	break;
	            }
	        }
	    }else{
			$selectedSize=$productsizenames[0];
	    }
	} else {
	    $selectedSize="NA";
	}
	return $selectedSize;
}

$toReturn=array();
$htmlString="";
foreach ($productsInCart as $key=>$value) {
	$htmlString = $htmlString . "<li>";
	$toReturn[$key] = array();
	$tempString = str_replace($cdn_base,$secure_cdn_base,$value['cartImagePath']);
	$tempString = str_replace("_96_128.","_48_64.",$tempString);
	$toReturn[$key]["imageURL"] = str_replace($http_location,$https_location,$tempString);
	$htmlString = $htmlString . "<img src=\"".$toReturn[$key]["imageURL"]."\" alt=\"Image Not Available\" class=\"left\">";
	$htmlString = $htmlString . "<div class=\"right\">";
	$toReturn[$key]["productName"] = $value['productStyleName'];
	$htmlString = $htmlString . $toReturn[$key]["productName"];
	$toReturn[$key]["quantity"] = $value['quantity'];
	$toReturn[$key]["size"] = getSelectedSize($value);
	$htmlString = $htmlString . "<span class=\"qs\"> QTY : ".$toReturn[$key]["quantity"]." &nbsp; &nbsp; SIZE : ".$toReturn[$key]["size"]."</span>";
	$toReturn[$key]["actualPrice"] = $value['productPrice']*$value['quantity'];
	$toReturn[$key]["save"] = $value['discountAmount'];
	$toReturn[$key]["discountedPrice"] = number_format($value['productPrice']*$value['quantity']-$value['discountAmount'], 2, ".", '');
	if(empty($toReturn[$key]["save"])){
		$htmlString = $htmlString . "<span class=\"cart-rs\">Rs ".$toReturn[$key]["actualPrice"]." <b>Rs. ".$toReturn[$key]["discountedPrice"]."</b> </span>";
	} else {
		$htmlString = $htmlString . "<span class=\"cart-rs\"><strike>Rs ".$toReturn[$key]["actualPrice"]."</strike> <i>(save Rs.".$toReturn[$key]["save"].")</i> <b>Rs. ".$toReturn[$key]["discountedPrice"]."</b> </span>";
	}
	$htmlString = $htmlString . "</div>";
	$htmlString = $htmlString . "</li>";
}

echo $htmlString;

?>