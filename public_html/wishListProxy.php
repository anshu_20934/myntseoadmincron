<?php

use enums\cart\CartContext;

include_once ("./auth.php");
include_once("./include/class/mcart/class.MCart.php");
include_once(\HostConfig::$documentRoot."/modules/RestAPI/CartActions.php");
use style\StyleGroup;
use style\builder\CachedStyleBuilder;
use style\builder\StyleBuilder;
use saveditems\action\SavedItemAction;
use saveditems\action\SavedItemAddAction;
use saveditems\action\SavedItemRemoveAction;
use saveditems\action\SavedItemUpdateAction;
use saveditems\action\SavedItemAddActionGeneric;
$_POST["_token"]=$XCART_SESSION_VARS['USER_TOKEN'];
$_REQUEST["_token"] = $XCART_SESSION_VARS['USER_TOKEN'];
define("WISHLIST_PROXY",true);
header('Content-type: application/json');
$_GET["_token"]=$XCART_SESSION_VARS['USER_TOKEN'];//TODO
$_POST["_token"]=$XCART_SESSION_VARS['USER_TOKEN'];//TODO to remove later
$_REQUEST["_token"]=$XCART_SESSION_VARS['USER_TOKEN'];//TODO to remove later
if($action === "add"){
	$requestData=json_decode($_GET["requestData"]);
	$_POST["productSKUID"] = $requestData->skuId;
	$_POST["productStyleId"] = $requestData->styleid;
	$_POST['sizequantity'] = array(0=>$requestData->quantity);
 	$actionHandler = new SavedItemAddAction();
    $val = $actionHandler->run();
	$myCart=$actionHandler->getSaved();
}else if($action==="update"){
	$requestData=json_decode($_GET["requestData"]);
	$_REQUEST['itemId'] = $requestData->itemId;
	$actionHandler = new SavedItemUpdateAction();
	$actionHandler->setItemId($requestData->itemId);
	$actionHandler->setQuantity($requestData->quantity);
	$actionHandler->setSkuId($requestData->skuId);
	$val = $actionHandler->run();
	$myCart=$actionHandler->getSaved();
}else if($action==="delete"){
	$requestData=json_decode($_GET["requestData"]);
	$_REQUEST["itemId"] = $requestData->itemId;
	$actionHandler = new SavedItemRemoveAction();
    $actionHandler->run();
    $myCart=$actionHandler->getSaved();
}else{
	  $savedItemAction = new SavedItemAction();
	  $myCart=$savedItemAction->getSaved();
}
$result = CartActions::cartToArray($myCart);
echo json_encode($result);