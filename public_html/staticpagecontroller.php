<?php
require "./auth.php";
include_once(\HostConfig::$documentRoot."/include/class/class.feature_gate.keyvaluepair.php");
use seo\SeoStrategy;
use seo\SeoPageType;
use macros\PageMacros;
$staticPage = filter_input(INPUT_GET, 'page', FILTER_SANITIZE_STRING);

$pageName = 'static';
include_once $xcart_dir."/webengage.php";
include_once $xcart_dir."/gtm.php";

// Omniture Tracking, new & old UI
$analyticsObj = AnalyticsBase::getInstance();
$analyticsBasePageName = $analyticsObj->getAnalyticsBaseStaticPageName($staticPage);
$analyticsObj->setStaticPageVars($analyticsBasePageName);
$analyticsObj->setTemplateVars($smarty);

//Macros
$macros = new PageMacros('STATIC');
$macros->url = currentPageURLWithStripedQueryStringsAndPort();
$macros->page_type = SeoPageType::STATIC_PAGE;
switch($staticPage) {
    case 'aboutus': $macros->page_name = "About"; break;
    case 'careers': $macros->page_name = "Careers"; break;
    case 'contactus' : $macros->page_name = "Contact"; break;
    case 'faqs' : $macros->page_name = "FAQs"; break;
    case 'privacypolicy' : $macros->page_name = "Privacy Policy"; break;
    case 'termsofuse' : $macros->page_name = "Terms Of Use"; break;
    case 'recentlyviewed' : $macros->page_name = "Recently Viewed"; break;
}

$seoEnabled=FeatureGateKeyValuePairs::getBoolean('SeoV3Enabled');
if($seoEnabled){
    try {
        $handle = Profiler::startTiming("seov3_load");
        $seoStrategy = new SeoStrategy($macros);
        Profiler::endTiming($handle);
        $smarty->assign("seoStrategy",$seoStrategy);
    } catch (Exception $e) {
        $seolog->error($staticPage." :: ".$e->getMessage());
    }
}


// About Us
if ($staticPage == "aboutus") {
	if($skin === "skin2") {
	       $smarty->display("static_pages/about_pages/aboutus.tpl");
	} else {
	       func_display("aboutus.tpl", $smarty);
	}
}

// Careers
elseif ($staticPage == "careers") {
	if($skin === "skin2") {
		$careers_whistletalk = FeatureGateKeyValuePairs::getBoolean('aboutus.careers.whistletalk.enabled');
		$smarty->assign("careers_whistletalk", $careers_whistletalk);
		$smarty->display("static_pages/about_pages/careers.tpl");
	} else {
	       func_display("careers.tpl", $smarty);
	}
}

// Contact Us
elseif ($staticPage == "contactus") {
	if($skin === "skin2") {
        // if FG is true the load dynamic contact-us page
        $dynamicContactUsPage = FeatureGateKeyValuePairs::getFeatureGateValueForKey('Contactus.Form');
        if($dynamicContactUsPage == "true"){
            
            include_once(\HostConfig::$documentRoot."/contact_us.php");
            
            exit;
            
        }

        $smarty->display("static_pages/about_pages/contactus.tpl");
        
	} else {
	       func_display("contact_us.tpl", $smarty);
	}
}

elseif($staticPage == 'myprivilege'){
	 $smarty->display("static_pages/myprivilege.tpl");
	 exit;
}
// FAQs

elseif ($staticPage == "faqs") {
	include_once "./mrp_description.php";
	include_once("$xcart_dir/include/class/widget/class.widget.keyvaluepair.php");
	
	global $cashback_gateway_status, $mrpCouponConfiguration;
	// Add the word describing the number of coupons
	convert_coupon_numbers_to_words($mrpCouponConfiguration);
	
	$nonfbregValue = $mrpCouponConfiguration["mrp_firstLogin_nonfbreg_numCoupons"]*$mrpCouponConfiguration["mrp_firstLogin_nonfbreg_mrpAmount"];
	$fbregValue = $mrpCouponConfiguration["mrp_firstLogin_fbreg_numCoupons"]*$mrpCouponConfiguration["mrp_firstLogin_fbreg_mrpAmount"];
	$refregValue = $mrpCouponConfiguration["mrp_refRegistration_numCoupons"]*$mrpCouponConfiguration["mrp_refRegistration_mrpAmount"];
	$refpurValue = $mrpCouponConfiguration["mrp_refFirstPurchase_numCoupons"]*$mrpCouponConfiguration["mrp_refFirstPurchase_mrpAmount"];
	$pickup_charges = WidgetKeyValuePairs::getWidgetValueForKey('returnPickupCharges');
	
	$icici3MONTHMinEMITotalAmount = \FeatureGateKeyValuePairs::getInteger('payments.icici3MONTHMinEMITotalAmount', 0);
	$smarty->assign('icici3MONTHMinEMITotalAmount', $icici3MONTHMinEMITotalAmount);

	$icici6MONTHMinEMITotalAmount = \FeatureGateKeyValuePairs::getInteger('payments.icici6MONTHMinEMITotalAmount', 0);
	$smarty->assign('icici6MONTHMinEMITotalAmount', $icici6MONTHMinEMITotalAmount);

	$icici3MONTHEMICharge = \FeatureGateKeyValuePairs::getFeatureGateValueForKey('payments.icici3MONTHEMICharge');
	$smarty->assign('icici3MONTHEMICharge',$icici3MONTHEMICharge);

	$icici6MONTHEMICharge = \FeatureGateKeyValuePairs::getFeatureGateValueForKey('payments.icici6MONTHEMICharge');
	$smarty->assign('icici6MONTHEMICharge',$icici6MONTHEMICharge);
	
	$showVatFaq = \FeatureGateKeyValuePairs::getBoolean('vat.enabled');
	$smarty->assign("showVatFaq",$showVatFaq); 
	$vatThreshold =  \FeatureGateKeyValuePairs::getFeatureGateValueForKey('vat.discountthreshold');
	$smarty->assign('vatThreshold',$vatThreshold);
	$vatCommonrate = \FeatureGateKeyValuePairs::getFeatureGateValueForKey('vat.commonrate');
	$smarty->assign('vatCommonrate',$vatCommonrate);
	$vatFaqMessage = WidgetKeyValuePairs::getWidgetValueForKey("vatFaqMessage");
	$smarty->assign('vatFaqMessage',$vatFaqMessage);

	$nextDayDeliveryEnabled = \FeatureGateKeyValuePairs::getBoolean('myntra.nextDayDelivery.enabled');
	$nextDayDeliveryCharges = WidgetKeyValuePairs::getWidgetValueForKey("shipping.charges.nextDayDelivery");
	$nextDayDeliveryCutOffTime = WidgetKeyValuePairs::getWidgetValueForKey("shipping.cutOffTime.nextDayDelivery");


	$sameDayDeliveryEnabled = \FeatureGateKeyValuePairs::getBoolean('myntra.sameDayDelivery.enabled');
	$sameDayDeliveryCharges = WidgetKeyValuePairs::getWidgetValueForKey("shipping.charges.sameDayDelivery");
	$sameDayDeliveryCutOffTime = WidgetKeyValuePairs::getWidgetValueForKey("shipping.cutOffTime.sameDayDelivery");

	$chargeCartLimit = WidgetKeyValuePairs::getWidgetValueForKey("shipping.charges.cartlimit");
	$shippingChargeAmount = WidgetKeyValuePairs::getWidgetValueForKey("shipping.charges.amount");
	
	$nextDayDeliveryCutOffTime = intval($nextDayDeliveryCutOffTime);
	$nextDayDeliveryCutOffTime = $nextDayDeliveryCutOffTime > 12 ? ($nextDayDeliveryCutOffTime - 12). " PM" : $nextDayDeliveryCutOffTime . " AM";

	$sameDayDeliveryCutOffTime = intval($sameDayDeliveryCutOffTime);
	$sameDayDeliveryCutOffTime = $sameDayDeliveryCutOffTime > 12 ? ($sameDayDeliveryCutOffTime - 12). " PM" : $sameDayDeliveryCutOffTime . " AM";

	$smarty->assign('nextDayDeliveryEnabled',$nextDayDeliveryEnabled);
	$smarty->assign('nextDayDeliveryCharges',$nextDayDeliveryCharges);
	$smarty->assign('nextDayDeliveryCutOffTime',$nextDayDeliveryCutOffTime);

	$smarty->assign('sameDayDeliveryEnabled',$sameDayDeliveryEnabled);
	$smarty->assign('sameDayDeliveryCharges',$sameDayDeliveryCharges);
	$smarty->assign('sameDayDeliveryCutOffTime',$sameDayDeliveryCutOffTime);

	$smarty->assign('chargeCartLimit',$chargeCartLimit);
	$smarty->assign('shippingChargeAmount',$shippingChargeAmount);

	$payment_options = FeatureGateKeyValuePairs::getStrArray("PaymentOptionsOrder", $defaultPaymentOrder);
	//enable/disable the emi faqs depending on whether the emi payment option is enabled or not
	if(in_array("emi", $payment_options)){
		$smarty->assign("emiEnabled",true);
	}

		
	$smarty->assign("pickupCharges", number_format($pickup_charges, 2));
	$smarty->assign("shippingRate", $system_shipping_rate);
	$smarty->assign("nonfbregValue", $nonfbregValue);
	$smarty->assign("fbregValue", $fbregValue);
	$smarty->assign("refregValue", $refregValue);
	$smarty->assign("refpurValue", $refpurValue);
	$smarty->assign("mrpCouponConfiguration", $mrpCouponConfiguration);
	
	$couponConfig = json_decode(WidgetKeyValuePairs::getWidgetValueForKey("registrationCouponConfig"),true);
	$fbCouponConfig = json_decode(WidgetKeyValuePairs::getWidgetValueForKey("fbRegistrationCouponConfig"),true);
	
	$smarty->assign("couponConfig", $couponConfig);
	$smarty->assign("fbCouponConfig", $fbCouponConfig);
	
	$codAmountRange = trim(FeatureGateKeyValuePairs::getFeatureGateValueForKey('cod.limit.range'));
	$codAmountRangeArray = explode("-", $codAmountRange);
	$codMinVal = $codAmountRangeArray[0];
	$codMaxVal = $codAmountRangeArray[1];
	$smarty->assign("codMinVal",$codMinVal);
	$smarty->assign("codMaxVal",$codMaxVal);
	
	
	$condNoDiscountCouponFG = FeatureGateKeyValuePairs::getBoolean('condNoDiscountCoupon');
	$smarty->assign('condNoDiscountCouponFG', $condNoDiscountCouponFG);
	$smarty->assign('loyaltyDisableMessage',WidgetKeyValuePairs::getWidgetValueForKey('loyalty.disableMessage'));
	
	if($skin === "skin2") {
	       $smarty->display("static_pages/help_pages/faqs.tpl");
	} else {
	       func_display("mkfaq.tpl", $smarty);
	}
}

// Privacy Policy
elseif ($staticPage == "privacypolicy") {
	if($skin === "skin2") {
	       $smarty->display("static_pages/help_pages/privacypolicy.tpl");
	} else {
	       func_display("privacy_policy.tpl", $smarty);
	}
}

// Terms of Use
elseif ($staticPage == "termsofuse") { 
	if($skin === "skin2") {
		//Configurable parameters starts
		$tierOnePurchaseValue = 3000;
		$tierTwoPurchaseValue = 6000;
		$tierThreePurchaseValue = 9000;
		$customerSupportCall = WidgetKeyValuePairs::getWidgetValueForKey('customerSupportCall');
		
		$sLoyaltyTermsCondition = WidgetKeyValuePairs::getWidgetValueForKey('loyalty.mymyntra.terms');
		$sLoyaltyTermsCondition = str_replace('{#tierOnePurchaseValue}',$tierOnePurchaseValue,$sLoyaltyTermsCondition);
		$sLoyaltyTermsCondition = str_replace('{#tierTwoPurchaseValue}',$tierTwoPurchaseValue,$sLoyaltyTermsCondition);
		$sLoyaltyTermsCondition = str_replace('{#tierThreePurchaseValue}',$tierThreePurchaseValue,$sLoyaltyTermsCondition);
		$sLoyaltyTermsCondition = str_replace('{#customerSupportCall}',$customerSupportCall,$sLoyaltyTermsCondition);

		$loyaltyTermsCondition = json_decode($sLoyaltyTermsCondition,true);
		$smarty->assign('loyaltyTermsCondition',$loyaltyTermsCondition);
	    $smarty->display("static_pages/help_pages/termsofuse.tpl");
	} else {
	       func_display("termandcondition.tpl", $smarty);
	}
}

// Recently Viewed
elseif ($staticPage == "recentlyviewed") {
	if ($skin == "skin2") {
            global $telesales;
            if (!$telesales)
		$smarty->display("static_pages/recentlyviewed.tpl");
            else
                echo "Access not allowed.";exit;
	}
}

// Myntra Policy
elseif ($staticPage == "myntrapolicies") {
	$smarty->display("static_pages/myntrapolicies.tpl");
}

?>
