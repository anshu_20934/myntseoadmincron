<?php
include_once("auth.php");
include_once("include/class/customization/CustomizationHelper.php");

$windowid = $HTTP_GET_VARS["windowid"];

// retrieve customization array and chosen product configuration from session
$thisCustomizationArray = $XCART_SESSION_VARS["customizationArrayEx"][$windowid];
$thisChosenProductConfiguration = $XCART_SESSION_VARS["chosenProductConfigurationEx"][$windowid];
$thisMasterCustomizationArray = $XCART_SESSION_VARS["masterCustomizationArrayEx"][$windowid];
$globalDataArray = $XCART_SESSION_VARS["globalDataArray"];
$productStyleId = $thisChosenProductConfiguration["productStyleId"];


// if custArray and chosenProductConfiguration are not set, FATAL error
if (empty($thisCustomizationArray) || empty($thisChosenProductConfiguration) || empty($thisMasterCustomizationArray)) {
    header("location:mksystemerror.php");
    return;
}

// initialize the customization class 		
$params = array("custArray" => $thisCustomizationArray, "masterCustomizationArray" => $thisMasterCustomizationArray, "chosenProductConfiguration" => $thisChosenProductConfiguration, "globalDataArray" => $globalDataArray);
$customizationHelper = new CustomizationHelper($params);


// if objectid on which action has to be taken exists in the params
if (isset($HTTP_GET_VARS["action"])) {
    // take requested action on the curent selected object in current selected orientation of current selected area
    $customizationHelper->applyCustomizationParams($HTTP_GET_VARS);
}

// generate customization image
if( isset($thisChosenProductConfiguration["productId"]) && !empty($thisChosenProductConfiguration["productId"]) )
{
	// if product details page call
	$final_area_image = $customizationHelper->showFinalCustomization();

	if(!( isset($final_area_image) && !empty($final_area_image) ))  // area image already exists
		$customizationHelper->generateAreaImage();
	
}
else {
	$customizationHelper->generateAreaImage();
}

//store the updated custArray back in session
$thisCustomizationArray = $customizationHelper->getCustomizationArray();
$XCART_SESSION_VARS["customizationArrayEx"][$windowid] = $thisCustomizationArray;

// customized image of current selected area
$finalImage = $customizationHelper->showFinalCustomization();
// returns true if any one area of the productstyle is customized
$cstatus = $customizationHelper->getCustomizationStatus();
// check if the image is smaller for its current customized width & height
$imageBlur = $customizationHelper->getImageBlurStatus();
echo "$cstatus#$finalImage#$imageBlur";
?>
