<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/auth.php';
require_once $xcart_dir.'/vtr/room.php';
require_once $xcart_dir.'/vtr/products.php';

use seo\SeoStrategy;
use seo\SeoPageType;
use macros\PageMacros;

//Macros
$macros = new PageMacros('STATIC');
$macros->url = currentPageURLWithStripedQueryStringsAndPort();
$macros->page_type = SeoPageType::STATIC_PAGE;
$macros->page_name = 'Style Studio';

$seoEnabled=FeatureGateKeyValuePairs::getBoolean('SeoV3Enabled');
if($seoEnabled){
    try {
		$handle = Profiler::startTiming("seov3_load");
        $seoStrategy = new SeoStrategy($macros);
        Profiler::endTiming($handle);
        $smarty->assign("seoStrategy",$seoStrategy);
    } catch (Exception $e) {
        $seolog->error($staticPage." :: ".$e->getMessage());
    }
}

//$smarty->debugging = true;
$smarty->assign('styleStudio',true);
$smarty->assign('pageTitle', 'Style Studio - Myntra');
$styleStudioOgTags['ogTitle'] = 'Myntra Style Studio';
$styleStudioOgTags['ogURL'] = 'www.myntra.com/stylestudio';
$styleStudioOgTags['ogImage'] = 'http://myntra.myntassets.com/images/banners/october2012/1350044842-style-studio-og-img.png';
$styleStudioOgTags['ogDescription'] = "Create looks of your own at the Myntra Style Studio. It's simple and fun. Check it out!";
$smarty->assign('styleStudioOgTags',$styleStudioOgTags);
//$smarty->assign('megaFooterEnabled', false);
$smarty->display('vtr.tpl');
