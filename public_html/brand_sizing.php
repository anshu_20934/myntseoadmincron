<?php
//TODO: filter input, $_REQUEST
require_once 'auth.php';
use size\brandsize\BrandSizeConversionDAO;
$dao = new BrandSizeConversionDAO();
$selected;
$actionType = $_REQUEST["actionType"];
//TODO:set response header: application/json
switch($actionType){
	case "fetchArticleTypes": 
		$selected["ageGroup"] = RequestVars::getVar("ageGroup");
		if(!empty($selected["ageGroup"])){
			echo json_encode($dao->getUnifiedArticleTypes($selected["ageGroup"]));
		}
		break;
	case "fetchBrands":
		$selected["ageGroup"] = RequestVars::getVar("ageGroup");
		$selected["articleTypeId"] = RequestVars::getVar("articleTypeId");
		if(!empty($selected["ageGroup"]) && !empty($selected["articleTypeId"])){
			echo json_encode($dao->getUnifiedBrands($selected["articleTypeId"],$selected["ageGroup"]));
		}
		break;
	case "fetchSizes":
		$selected["ageGroup"] = RequestVars::getVar("ageGroup");
		$selected["articleTypeId"] = RequestVars::getVar("articleTypeId");
		$selected["brand"] = RequestVars::getVar("brand");
		if(!empty($selected["ageGroup"]) && !empty($selected["articleTypeId"]) && !empty($selected["brand"])){
			echo json_encode($dao->getSizes($selected["articleTypeId"],$selected["brand"],$selected["ageGroup"]));
		}
		break;
	case "fetchMeasurementsForSkus":
		$selected["skuids"] = RequestVars::getVar("skuids");
		if(!empty($selected["skuids"])){
			echo json_encode($dao->getMeasurementsForSkus($selected["skuids"]));
		}
		break;
	default:
		$smarty->assign("ageGroups",$dao->getAgeGroups());
		$smarty->assign("selected",$selected);
		$smarty->display("pdp/brand2brandconv.tpl");
}

header("Content-type:application/json");
// seconds, minutes, hours, days
$expires = 60*60*24*5;
header("Cache-Control: maxage=".$expires);
header('Expires: ' . gmdate('D, d M Y H:i:s', time()+$expires) . ' GMT');