<?php
require "./auth.php";
include_once("./include/func/func.mkcore.php");
require_once $xcart_dir."/include/solr/solrStatsSearch.php";
require_once $xcart_dir."/include/solr/solrQueryBuilder.php";
$qstring = $_GET['qstring'];

$arr_qstring = explode("-",$qstring);
$string_first = $arr_qstring[0];
$string_last = $arr_qstring[1];
//$solrSearch=new solrStatsSearch();
//$resultcount=db_query("select count(*) as count_rows from mk_stats_search where is_valid='Y' and count_of_products >3 " );
//$row=db_fetch_array($resultcount);
$query="count_of_products:[1 TO *] AND is_valid:true";
$solrSearch = new solrStatsSearch();
$search_results =  $solrSearch->searchAndSort($query,  $offset, $limit,'');
$limit =$search_results->numFound;

$limit=(int)round(pow($limit,1/3));
//$limit =80;

//if string_first is 0 and string_last is zero it means its first page
	if(($string_first==0)&&($string_last==0)){
		for($i=0;$i<$limit;$i++){
			$arr_display[$i] =$i+1;
		}
		$smarty->assign("condition",1);
		$smarty->assign("arr_display",$arr_display);
		//print_r($arr_display);exit;
	}

//if string_first is not zero and last is zero it measn its second page
if(($string_first!=0)&&($string_last==0)){
		$start=$limit*$limit*($string_first-1)+1;
		$last=$start+$limit*($limit-1);
		$j=0;
		for($i=$start;$i<=$last;$i=$i+$limit){
			$arr_display[$j] =$i;
			$j++;
		}
		$smarty->assign("condition",2);
		$smarty->assign("arr_display",$arr_display);
	}

//if string_first is 0 and string last is not zero it means its last page

if(($string_first==0)&&($string_last!=0)){
	

	//$results=func_query("select orignal_keyword,key_id from mk_stats_search where is_valid='Y' and count_of_products >3 limit ".$string_last." ,".$limit);

	//$offset=$string_last+$limit;

	$query="count_of_products:[1 TO *] AND is_valid:1 ";
	$search_results =  $solrSearch->searchAndSort($query,  $string_last, $limit,'');
	$results = documents_to_array($search_results->docs);
	
	$smarty->assign("condition",3);
	$smarty->assign("string_last",$string_last);
	$smarty->assign("string_last_offset",($string_last+$limit));
	$smarty->assign("results",$results);
		
	}

$breadCrumb=array(
		       array('reference'=>'home.php','title'=>'home'),
                array('reference'=>'#','title'=>'site map')
              );
$smarty->assign("breadCrumb",$breadCrumb);

func_display("sitemap_new.tpl",$smarty);
?>