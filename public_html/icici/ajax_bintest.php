<?php
include_once("../auth.php");

function bintest($binNumber) {
	if(empty($binNumber)) return false;
	$binFound = func_query_first_cell("select count(*) from mk_cards_domestic_bins where bin=$binNumber");
	if($binFound>0) {
		return true;
	} else return false;
}

function getIssuingBank($binNumber){
	if(!empty($binNumber)) return func_query_first_cell("select bank_name from mk_cards_domestic_bins where bin=$binNumber");
	else return false;
}

function getISO3CountryCode($alpha2){
	if(!empty($alpha2))	return func_query_first_cell("select iso_alpha_3 from mk_icici_country_iso_mapping where iso_alpha_2='$alpha2'");
	else return false;
}


function getIssuingBankCode($binNumber){
	if(!empty($binNumber)) return func_query_first_cell("select card_bank_code from mk_cards_domestic_bins where bin=$binNumber");
	else return false;
}

if(isset($HTTP_POST_VARS['ccNumber'])){
 	$ccNumber = str_replace('-', '', $ccNumber);    // baseline single quote
	$ccNumber = str_replace(' ', '', $ccNumber);    // baseline single quote
	$binNumber = substr($ccNumber, 0,6);
	$result = getIssuingBankCode($binNumber);
	if(!empty($result)){
		$msg=array("status"=>'success',"bank_name"=>$result);
		echo json_encode($msg); 
		exit;
	} else {
		$msg=array("status"=>'error');
		echo json_encode($msg); 
		exit;
	}
}

?>