<?php
use enums\revenue\payments\Gateways;
if(!empty($HTTP_POST_VARS)) {
	
	$orderID  = $HTTP_POST_VARS['shoppingcontext'];
	if(!(strpos($orderID, "G_") === false)){
		include_once dirname(__FILE__)."/iciciresponsegifting.php";
		exit;
	}
	
	$response_xid = $HTTP_POST_VARS['xid'];
	unset($xid);
	unset($HTTP_POST_VARS['xid']);
	unset($_POST['xid']);
	$_POST['response_xid'] = $response_xid;
	$HTTP_POST_VARS['response_xid'] = $response_xid;
	require_once ("../auth.php");
	 $orderID=$HTTP_POST_VARS['shoppingcontext'];
	require_once ("$xcart_dir/icici/ajax_bintest.php");
	x_load('crypt');
	include("$xcart_dir/Sfa/BillToAddress.php");
	include("$xcart_dir/Sfa/CardInfo.php");
	include("$xcart_dir/Sfa/Merchant.php");
	include("$xcart_dir/Sfa/MPIData.php");
	include("$xcart_dir/Sfa/ShipToAddress.php");
	
	include("$xcart_dir/Sfa/PGResponse.php");
	
	include("$xcart_dir/Sfa/PostLibPHP.php");
	
	include("$xcart_dir/Sfa/PGReserveData.php");
	
	$redirectToInCaseOfReload = $http_location;
	$login = $XCART_SESSION_VARS['login'];
	//echo "LOGIN: $login<br/>";
	
	$status = $HTTP_POST_VARS['status'];
	$eci = $HTTP_POST_VARS['eci'];
	$cavv = $HTTP_POST_VARS['cavv'];
	
	$purchaseAmount = $HTTP_POST_VARS['purchaseamount'];
	//$orderID  = $HTTP_POST_VARS['shoppingcontext'];
	$currency = $HTTP_POST_VARS['currency'];
	$response_id = $HTTP_POST_VARS['ID'];
	$response_md = $HTTP_POST_VARS['MD'];
	$verses_status = $HTTP_POST_VARS['VERES_STATUS'];
	$message_hash = $HTTP_POST_VARS['message_hash'];
	$mpiErrorCode = $HTTP_POST_VARS['mpiErrorCode'];
	$PARES_STATUS = $HTTP_POST_VARS['PARES_STATUS'];
	$PARES_VERIFIED = $HTTP_POST_VARS['PARES_VERIFIED'];
	
	if(empty($orderID)) {
		func_header_location($redirectToInCaseOfReload,false);
	}
	
	if($configMode=="local"||$configMode=="test"||$configMode=="release") {
		$test_orderID = mysql_real_escape_string(substr($orderID, 5));
		$order_result = func_query("SELECT * FROM " . $sql_tbl['orders'] . " WHERE orderid='". $test_orderID ."'");
		$gateway_name = func_query_first_cell("SELECT payment_gateway_name from mk_payments_log where orderid='". $test_orderID ."'");	
		
		if($configMode=="release"){
			$merchandID = "00002585";
		} else {
			$merchandID = "00003433";
		}
	} else {
		$order_result = func_query("SELECT * FROM " . $sql_tbl['orders'] . " WHERE orderid='". $orderID ."'");
		$gateway_name = func_query_first_cell("SELECT payment_gateway_name from mk_payments_log where orderid='". $orderID ."'");
		$merchandID = "00005357";
	}
	
	if($gateway_name==Gateways::ICICIBank3EMI) {
		$merchandID = "00006566";
	} elseif($gateway_name==Gateways::ICICIBank6EMI) {
		$merchandID = "00006567";
	}
	
	
	//check the previous status of the order proceed only if the status is PP	
 	$prev_status = $order_result[0]['status'];
 	$billingName = $order_result[0]['b_firstname'] ." " . $order_result[0]['b_lastname'];
 	$billingAddress = $order_result[0]['b_address'];
 	$billingCity = $order_result[0]['b_city'];
 	$billingState = $order_result[0]['b_state'];
 	$billingCountry = getISO3CountryCode($order_result[0]['b_country']);
 	$billingZipCode = $order_result[0]['b_zipcode'];
 	$billingEmail = $order_result[0]['b_email'];
 	
 	$shippingName = $order_result[0]['s_firstname'];
 	$shippingAddress = $order_result[0]['s_address'];
 	$shippingCity = $order_result[0]['s_city'];
 	$shippingState = $order_result[0]['s_state'];
 	$shippingCountry = getISO3CountryCode($order_result[0]['s_country']);
 	if(empty($shippingCountry)) $shippingCountry = "IND"; //last resort should not happen
 	$shippingZipCode = $order_result[0]['s_zipcode'];
 	$shippingEmail = $order_result[0]['s_email'];
 	
 	if(empty($billingName)) {
 		$billingName = $shippingName;
 	}
 	
 	if(empty($billingAddress)) {
 		$billingAddress = $shippingAddress;
 	}
 	
 	if(empty($billingCity)) {
 		$billingCity = $shippingCity;
 	}
 	
	if(empty($billingState)) {
 		$billingState = $shippingState;
 	}
 	
	if(empty($billingCountry)) {
 		$billingCountry = $shippingCountry;
 	}
 	
	if(empty($billingZipCode)) {
 		$billingZipCode = $shippingZipCode;
 	}
 	
	if(empty($billingEmail)) {
 		$billingEmail = $shippingAddress;
 	}
 	
	if($prev_status!='PP') {
		func_header_location($redirectToInCaseOfReload,false);
	}
	
	//if($status=='Y'||$status=='A'){
		
		
		if($eci == 5 || $eci == 6 || $eci == 1 ||$eci == 2) {
			$toDecrypt = getMyntraCookie("myntra_secure");
			if (isset($toDecrypt)) {
		  		//echo "Encrypted " . $toDecrypt . "!<br />";
		  		$decrypted = text_decrypt($toDecrypt);
		  		//echo "Decrypted " . $decrypted . "!<br />";
		  		$cardDetails =explode(";", $decrypted);	  		
		  		$cardNumber = $cardDetails[0];
		  		$expiryDate = $cardDetails[1];
		  		$expiryMonth = substr($expiryDate, 0,2);
		  		$expiryYear = "20".substr($expiryDate, 2);
		  		$cardCVV = $cardDetails[2];
		  		$holderName = $cardDetails[3];
		  		$cardIssuer = $cardDetails[4];
		  		$cardType = $cardDetails[5];
		  		
		 	  	$Amount = $purchaseAmount/100;
		  		//print_r($cardDetails);
		  		//echo "Expiry Month = $expiryMonth <br/>";
		  		//echo "Expiry Year = $expiryYear <br/>";
		  		//delete the cookie
				
		  		$oMPI 		= 	new 	MPIData();
				$oCI		=	new	CardInfo();
				$oPostLibphp	=	new	PostLibPHP();
				$oMerchant	=	new	Merchant();
				$oBTA		=	new	BillToAddress();
				$oSTA		=	new	ShipToAddress();
				$oPGResp	=	new	PGResponse();
				$oPGReserveData = new PGReserveData();	  		
		  		
				$billingAddress =trim($billingAddress);
				$billingAddress = sanitize_paranoid_string($billingAddress);
				$billingAddress = substr($billingAddress, 0,50);
				
				$billingName = trim($billingName);
				$billingName = sanitize_paranoid_string($billingName);
				$billingName = substr($billingName, 0,80);				
				
				$billingCity = trim($billingCity);
				$billingCity = sanitize_paranoid_string($billingCity);
				$billingCity = substr($billingCity, 0,30);				
				
				$billingState = trim($billingState);
				$billingState = sanitize_paranoid_string($billingState);
				$billingState = substr($billingState, 0,30);
				
				$billingZipCode = trim($billingZipCode);
				$billingZipCode = sanitize_paranoid_string($billingZipCode);
				$billingZipCode = substr($billingZipCode, 0,10);
				
				$billingEmail = trim($billingEmail);
				$billingEmail = substr($billingEmail, 0,80);
				
				$shippingAddress = trim($shippingAddress);
				$shippingAddress = sanitize_paranoid_string($shippingAddress);
				$shippingAddress = substr($shippingAddress, 0,50);				
				
				$shippingCity = trim($shippingCity);
				$shippingCity = sanitize_paranoid_string($shippingCity);
				$shippingCity = substr($shippingCity, 0,30);
								
				
				$shippingState = trim($shippingState);
				$shippingState = sanitize_paranoid_string($shippingState);
				$shippingState = substr($shippingState, 0,30);								
				
				$shippingZipCode = trim($shippingZipCode);
				$shippingZipCode = sanitize_paranoid_string($shippingZipCode);
				$shippingZipCode = substr($shippingZipCode, 0,10);
				
				$holderName = trim($holderName);
				$holderName = sanitize_paranoid_string($holderName);
				$holderName = substr($holderName,0,80);
				
				$shippingEmail = trim($shippingEmail);
				$shippingEmail = substr($shippingEmail, 0,80);
				
				$merchantTransactionID = $orderID; //This should be unique
				$retryTill = 3;
				$retryTime =0;
				
				//sometime aws sent ip address seprated by , commas
				$ip_address = explode(',', $CLIENT_IP);
				$ip_address = trim($ip_address[0]);
				if(empty($ip_address)) {
					$ip_address = "122.248.251.9"; //we dont want payment to fail because of this ip address being empty so putting a fail safe value.
				}
				$ip_address = substr($ip_address, 0,20);
				do {
					$oMerchant->setMerchantDetails($merchandID,$merchandID,$merchandID,$ip_address."",$merchantTransactionID."",$orderID."","","","INR",$orderID."","req.Sale",$Amount."","","Ext1","Ext2","Ext3","Ext4","Ext5");
		
					$oBTA->setAddressDetails ($login."",$billingName."",$billingAddress . "","","",$billingCity."",$billingState."",$billingZipCode."",$billingCountry."",$billingEmail."");
		
					$oSTA->setAddressDetails ($shippingAddress."","","",$shippingCity."",$shippingState."",$shippingZipCode."",$shippingCountry."",$shippingEmail."");
		
					$oCI->setCardDetails ($cardIssuer."",$cardNumber."",$cardCVV."",$expiryYear."",$expiryMonth."",$holderName."","CREDI");
		
					$oMPI->setMPIResponseDetails  ($eci."",$response_xid."",$status."",$cavv."",$orderID."",$purchaseAmount."",$currency."");
					
			  		$oPGResp=$oPostLibphp->postMOTO($oBTA,$oSTA,$oMerchant,$oMPI,$oCI,$oPGReserveData,null,null,null,null);
			  		
			  		$gateway_response_code = java_values($oPGResp->getRespCode());
			  		$gateway_response_message = java_values($oPGResp->getRespMessage());
			  		$gateway_response_message = trim($gateway_response_message);
			  		$gateway_transaction_id = java_values($oPGResp->getTxnId());
			  		$epg_transaction_id = java_values($oPGResp->getEpgTxnId());
			  		$gateway_auth_code_id = java_values($oPGResp->getAuthIdCode());
			  		$gateway_rrn = java_values($oPGResp->getRRN());
			  		$gateway_cvv_response_code = java_values($oPGResp->getCVRespCode());
			  		$gateway_fdms_score = java_values($oPGResp->getFDMSScore());
			  		$gateway_fdms_result = java_values($oPGResp->getFDMSResult());
			  		$gateway_cookie = java_values($oPGResp->getCookie());
			  		$retryTime++;
			  		$merchantTransactionID = $merchantTransactionID."_".$retryTime;
				} while ( $retryTime < $retryTill && ($gateway_response_code===2||$gateway_response_code==='2') && ($gateway_response_message=='Error while reading data. Transaction cannot be processed'|| $gateway_response_message=='Error while writing data. Transaction cannot be processed'));
	  			/*print "Response Code:".java_values($oPGResp->getRespCode())."<br>";
	
			 	print "Response Message".java_values($oPGResp->getRespMessage())."<br>";
			
			 	print "Transaction ID:".java_values($oPGResp->getTxnId())."<br>";
			
			 	print "Epg Transaction ID:".java_values($oPGResp->getEpgTxnId())."<br>";
			
		 		print "Auth Id Code :".java_values($oPGResp->getAuthIdCode())."<br>";
			
		 		print "RRN :".java_values($oPGResp->getRRN())."<br>";
			
			 	print "CVResp Code :".java_values($oPGResp->getCVRespCode())."<br>";
			
		     	print "FDMS Score:".java_values($oPGResp->getFDMSScore())."<br>";
			
			 	print "FDMS Result:".java_values($oPGResp->getFDMSResult())."<br>";
			
			     # the cookie has to be written to client browser and the same has to be retrieved
			     # and set in session details on further calls to postMoto
			 	print "Cookie:".java_values($oPGResp->getCookie())."<br>";*/
			}
			else {
				$gateway_response_code = "3";
				$gateway_response_message = "Cookie Not Present, Session timedout";
			}		
		} else {
			$gateway_response_code = "3";
			$gateway_response_message = "eci code not favorable to continue, verification of card failed";
			//echo "do not continue with the transaction and show authentication failure response to the cardholder.";
		}
		setMyntraCookie("myntra_secure", "", time()-3600,'/',$cookiedomain, true, true);
	//} else {
	//	echo "Transaction Declined show him error code and Log enteries!<br />";
	//}
	
	
	//echo "<br/>";
	//print_r($_POST);
	
	
	$iciciLog=array();
	if(isset($orderID)) {
		if($configMode=="local"||$configMode=="test"||$configMode=="release") {
			$iciciLog['orderid'] = mysql_escape_string($test_orderID);
		} else {
			$iciciLog['orderid'] = mysql_escape_string($orderID);
		}
	}
	if(!empty($login)) $iciciLog['login'] = mysql_escape_string($login);
	if(isset($status)) $iciciLog['status'] = mysql_escape_string($status);
	if(isset($eci)) $iciciLog['eci'] = mysql_escape_string($eci);
	if(isset($cavv)) $iciciLog['cavv'] = mysql_escape_string($cavv);
	if(isset($purchaseAmount)) $iciciLog['purchaseamount'] = mysql_escape_string($purchaseAmount);
	if(isset($currency)) $iciciLog['currency'] = mysql_escape_string($currency);
	if(isset($response_xid)) $iciciLog['xid'] = mysql_escape_string($response_xid);
	if(isset($response_id)) $iciciLog['response_id'] = mysql_escape_string($response_id);
	if(isset($response_md)) $iciciLog['response_md'] = mysql_escape_string($response_md);
	if(isset($verses_status)) $iciciLog['veres_status'] = mysql_escape_string($verses_status);
	if(isset($message_hash)) $iciciLog['message_hash'] = mysql_escape_string($message_hash);
	if(isset($mpiErrorCode)) $iciciLog['mpi_error_code'] = mysql_escape_string($mpiErrorCode);
	if(isset($PARES_STATUS)) $iciciLog['pares_status'] = mysql_escape_string($PARES_STATUS);
	if(isset($PARES_VERIFIED)) $iciciLog['pares_verified'] = mysql_escape_string($PARES_VERIFIED);
	if(isset($orderID)) $iciciLog['time_insert'] = time();
	if(isset($gateway_response_code)) $iciciLog['gateway_response_code'] = mysql_escape_string($gateway_response_code);
	if(isset($gateway_response_message)) $iciciLog['gateway_response_message'] = mysql_escape_string($gateway_response_message);
	if(isset($gateway_transaction_id)) $iciciLog['gateway_transaction_id'] = mysql_escape_string($gateway_transaction_id);
	if(isset($epg_transaction_id)) $iciciLog['epg_trasaction_id'] = mysql_escape_string($epg_transaction_id);
	if(isset($gateway_auth_code_id)) $iciciLog['gateway_auth_id_code'] = mysql_escape_string($gateway_auth_code_id);
	if(isset($gateway_rrn)) $iciciLog['gateway_rrn'] = mysql_escape_string($gateway_rrn);
	if(isset($gateway_cvv_response_code)) $iciciLog['gateway_cvv_response_code'] = mysql_escape_string($gateway_cvv_response_code);
	if(isset($gateway_fdms_score)) $iciciLog['gateway_fdms_score'] = mysql_escape_string($gateway_fdms_score);
	if(isset($gateway_fdms_result)) $iciciLog['gateway_fdms_result'] = mysql_escape_string($gateway_fdms_result);
	if(isset($gateway_cookie)) $iciciLog['gateway_cookie'] = mysql_escape_string($gateway_cookie);
	if(isset($_POST)) $iciciLog['response'] = mysql_escape_string(print_r($_POST,true));
	func_array2insert('mk_icici_payments_log', $iciciLog);
	if($configMode=="local"||$configMode=="test"||$configMode=="release") {
		$formAction = $https_location."/mkorderBook.php?type=icici&orderid=".$test_orderID;
	} else {
		$formAction = $https_location."/mkorderBook.php?type=icici&orderid=".$orderID;
	}
	func_header_location($formAction,false);
	
} else {
	require_once ("../auth.php");
	$redirectToInCaseOfReload = $http_location;
	func_header_location($redirectToInCaseOfReload,false);
}

?>
