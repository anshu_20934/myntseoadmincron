<?php
include_once './auth.php';
require_once("$xcart_dir/include/solr/solrProducts.php");
require_once("$xcart_dir/include/class/search/SearchProducts.php");
include_once($xcart_dir."/include/class/class.feature_gate.keyvaluepair.php");
include_once($xcart_dir."/include/class/widget/headerMenu.php");

class Top_nav2{
	
public static function getGlobalCategories(){
		$solrProducts = new SolrProducts();
		
		$global_display_categories = $solrProducts->multipleFacetedSearch('*:* AND display_category:[* TO *]', array("display_category_facet"))->facet_fields;
		
		$global_display_categories->display_category_facet=sortDisplayCategory($global_display_categories->display_category_facet);
		
		return $global_display_categories;
	}
	
	public static function getGlobalBrands(){
		$solrProducts = new SolrProducts();

		$global_brands = Top_nav2::getBrandsFromQuery('*:*');

		return $global_brands;
	}
	
	public static function getBrandsFromQuery($query, $ranges = array(0 => array("rangeText" => "A - D", "solrQuery" => "brands_filter_facet:[A TO E] OR brands_filter_facet:[a TO e] OR brands_filter_facet:[0 TO 9]"),
						1 => array("rangeText" => "E - L", "solrQuery" => "brands_filter_facet:[E TO M] OR brands_filter_facet:[e TO m]"),
						2 => array("rangeText" => "M - P", "solrQuery" => "brands_filter_facet:[M TO Q] OR brands_filter_facet:[m TO q]"),
						3 => array("rangeText" => "Q - Z", "solrQuery" => "brands_filter_facet:[Q TO Z] OR brands_filter_facet:[q TO z]")
						)){
							
		$dontShowOOSProducts = FeatureGateKeyValuePairs::getFeatureGateValueForKey("dontShowOOSProducts");
		if((!empty($dontShowOOSProducts) && $dontShowOOSProducts == 'true'))
			$query = $query. " count_options_availbale:[1 TO *]";
										
		$solrProducts = new SolrProducts();
		$brandsPresent = array();
		foreach ($ranges as $key=>$value){
			$tmpRes = $solrProducts->multipleFacetedSearch($query . " AND (" . $value['solrQuery'] .")" ,array("brands_filter_facet"),0,0,1,"index")->facet_fields;
			$tmpArr = get_object_vars($tmpRes->brands_filter_facet);
			uksort($tmpArr,strcasecmp);
			$brandsPresent[$key]['result']['brands_filter_facet'] = $tmpArr;
			$brandsPresent[$key]['range'] = $value['rangeText'];
			$brandsPresent[$key]['count'] = count((array)$brandsPresent[$key]['result']['brands_filter_facet']);
			
		}
		return $brandsPresent;
	}
	
}

$top_nav = func_display('site/menu_topnav2.tpl', $smarty, false);

$global_brands = json_decode($xcache->fetch($xcart_http_host . '-' . "global_brands"), true);
if($global_brands == NULL || $nocache == 1){
    $global_brands = Top_nav2::getGlobalBrands();
    if($nocache != 1){//Do not store in case nocache param is set
   		$xcache->store($xcart_http_host . '-' . "global_brands", json_encode($global_brands));
    }
}
$global_categories = json_decode($xcache->fetch($xcart_http_host . '-' . "global_categories"));
if($global_categories == NULL || $nocache == 1){
    $global_categories = Top_nav2::getGlobalCategories();
    if($nocache != 1){//Do not store in case nocache param is set
        $xcache->store($xcart_http_host . '-' . "global_categories", json_encode($global_categories));
    }
}
$smarty->assign("top_nav", $top_nav);
$smarty->assign("global_brands", $global_brands->brands_filter_facet);
$smarty->assign("global_display_categories", $global_categories->display_category_facet);

