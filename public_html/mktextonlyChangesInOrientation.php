<?php
include("auth.php");
include("include/class/customization/CustomizationHelper.php");
$windowid = $HTTP_GET_VARS["windowid"];	

$orientationText = $_GET;
$orientationNeeded = array();

$prefix = "orientations_";
foreach ($orientationText as $key=>$value) {
	if (strncmp($key, $prefix, strlen($prefix)) == 0) {
		$key2 = substr($key, strlen($prefix));
		$key2 = str_replace("___", " ", $key2);
		$orientationNeeded[$key2] = $value;
	}	
}

$thisCustomizationArray = $XCART_SESSION_VARS["customizationArrayEx"][$windowid];
$params = array("custArray"=>$thisCustomizationArray,"masterCustomizationArray"=>null,"chosenProductConfiguration"=>null,"globalDataArray"=>null);
$customizationHelper = new CustomizationHelper($params );

foreach($orientationNeeded as $orientationName=>$text) {
	$customizationHelper->setSelectedOrientationName($orientationName);
	$customizationHelper->updateTextInfoInCustArea($text);
}

$thisCustomizationArray = $customizationHelper->getCustomizationArray();
$XCART_SESSION_VARS["customizationArrayEx"][$windowid] = $thisCustomizationArray;
echo 'success';
?>
