<?php

require_once "./auth.php";

require_once($xcart_dir."/include/class/class.html2fpdf.php");

require_once($xcart_dir."/class.imageconverter.php");
require_once($xcart_dir."/include/func/func.mkspecialoffer.php");

require_once $xcart_dir."/include/func/func.mk_orderbook.php";
require_once $xcart_dir."/include/func/func.order.php";
require_once $xcart_dir."/mkstatesarray.php";

function parse_quantity_breakup($breakupstr)
{
    $substr_arr = split('[,;]', $breakupstr);
    foreach($substr_arr as $substr)
    {
        $pair_str=split('[:]', $substr);
        if($pair_str[0]!="" && $pair_str[1]!="")
            if($pair_str[1])
             $delim_hash[$pair_str[0]]=$pair_str[1];

    }
    return $delim_hash;
}

function qtybreakup_string_cleanup($breakupstr)
{
    $qty_hash = parse_quantity_breakup($breakupstr);
    $ret="";
    foreach($qty_hash as $key=>$value)
        $ret=$ret.$key.":".$value."; ";
    return $ret;

}

function get_orders_for_assignee($assigneeid)
{
    global $sql_tbl;
    $sql = "select distinct od. orderid from xcart_order_details od,xcart_orders o where o.orderid=od.orderid and  (o.status = 'WP' or o.status = 'OH')
	AND (od.item_status = 'A' OR od.item_status = 'S') and assignee=$assigneeid";
    return func_query($sql);
}

$orderid_get=$_GET["orderid"];
$assigneeid_get=$_GET["assigneeid"];
$format=$_GET["format"];

$pagename="OPS.pdf";
if($assigneeid_get!="")
{
    $orders=get_orders_for_assignee($assigneeid_get);
    $pagename="processing_sheet_Assignee".$assigneeid_get.".pdf";
}
else
{
   $orders[0]['orderid']=$orderid_get;
   $pagename="processing_sheet_$orderid_get".".pdf";
}
$pdfStringArray =array ();
foreach($orders as $order)
{

    $orderid=$order[orderid];


    $order_data = func_order_data($orderid);

    if(!$order_data){
       echo" Order $orderid does not exist !! Please check order ID and try again.";
       exit;
       }
    $order = $order_data["order"];
    $userinfo = $order_data["userinfo"];
    $giftcerts = $order_data["giftcerts"];


    $order_name=$order[order_name];
    $order_courier=$order[courier_service];
    if($order_courier!="")
      $order_courier=func_query_first_cell("select courier_service from mk_courier_service where code='$order_courier'");
    $estimated_completion=func_query_first_cell("select from_unixtime(max(estimated_processing_date)) from xcart_order_details where orderid=$orderid");
    $source_id=$order[source_id];
    $source_name=func_query_first_cell("select source_name from mk_sources where source_id=$source_id ");

    $pastdue=false;
    if(strtotime("now") > strtotime($estimated_completion))
       $pastdue=true;

    //Tables
    $tb_product_category = $sql_tbl["products_categories"];
    $giftneeded="Y";
    if(empty($giftcerts)){
        $giftcerts = 0;
    }

    $today = date("d-m-y h:m:s",$order[queueddate]);

    switch($order['status']){
        case 'P': $status = 'Processed'; break;
        case 'C': $status = 'Complete'; break;
        case 'Q': $status = 'Queued'; break;
        case 'D': $status = 'Declined'; break;
        case 'F': $status = 'Failed'; break;
        case 'PD': $status = 'Pending'; break;
    }

    #
    #Call function to load the customer details
    #
    $customerdetail = func_customer_order_address_detail($order['login'], $order['orderid']);
    $optionCounter = array();
    $productTypeCount = array();
    $productsInCart = func_query("SELECT itemid,assignee,product, productid, product_style, product_type, price, amount, subtotal, total,$sql_tbl[operation_location].location_name as location_name, $sql_tbl[order_details].discount AS productDiscount, $sql_tbl[orders].discount AS totaldiscountonProducts, coupon_discount, $sql_tbl[orders].coupon AS couponcode, shipping_cost, tax, ordertype, ref_discount, $sql_tbl[order_details].quantity_breakup AS quantity_breakup, sp.article_number AS article_number, $sql_tbl[order_details].addonid AS addonid,$sql_tbl[order_details].is_customizable
                              FROM $sql_tbl[orders] left join  $sql_tbl[order_details] on $sql_tbl[order_details].orderid = $sql_tbl[orders].orderid
                              left join mk_style_properties sp on sp.style_id = $sql_tbl[order_details].product_style
                              left join $sql_tbl[operation_location] on $sql_tbl[operation_location].id=$sql_tbl[order_details].location
                              WHERE $sql_tbl[order_details].orderid = '".$orderid."'" );

    $productTypesRes=func_query("select name,id,consignment_capacity from mk_product_type pt,xcart_order_details od,mk_shipping_prod_type spt
                                where pt.id=od.product_type and spt.product_type_id=od.product_type and od.orderid=$orderid");

    foreach($productTypesRes as $rec){
        $productTypeCount[$rec[id]][name]=$rec[name];
        $productTypeCount[$rec[id]][count]=0;
        $productTypeCount[$rec[id]][capacity]=$rec[consignment_capacity];
    }
    for($i=0;$i<count($productsInCart);$i++) {

        $product_type_id_for_count=$productsInCart[$i]['product_type'];
        $productTypeCount[$product_type_id_for_count][count]+=$productsInCart[$i][amount];
        if($productsInCart[$i]['assignee']){
            $assignee_id=$productsInCart[$i]['assignee'];
          $productsInCart[$i]['assignee_name']=func_query_first_cell("select name from mk_assignee where id=$assignee_id");
        }
        else {
            $productsInCart[$i]['assignee_name']="Unassigned";
        }
        $quantity_brk=$productsInCart[$i]['quantity_breakup'];
        $option_skus = "select po.value,osm.sku_id from mk_product_options po, mk_styles_options_skus_mapping osm ".
        "where osm.option_id = po.id and osm.style_id = '".$productsInCart[$i]['product_style']."'";

        $skuresult = func_query($option_skus,true);
        
    	foreach($skuresult as $key1 => $value){
        	$skuIds[$var++] = $value['sku_id']; 	
        }
        $skudetails = SkuApiClient::getSkuDetails($skuIds);
        
        foreach($skucodes as $key2 => $sku){
			foreach($skuresult as $key1 => $value){
				if($sku['id'] == $value['sku_id']){
					$skuresult[$key1]['code'] = $sku['code'];
					break;
				}
			}
        }
        $finalquant="";
        $myarray=explode(",",$quantity_brk);
        for ($j=0;$j<count($myarray);$j++){
            $quant=$myarray[$j];
            if(substr($quant,strlen($quant)-1,strlen($quant))==":"){
                $quant="";
            } else {
            	//Add SKU information if ordered
            	$option = split(":", $quant);
				foreach ($skuresult as $result) {
					if ($result["value"] == $option[0]) {
						$quant .= " [SKU : " . $result["code"] . "]";
						continue;
					}
				}            	
            }
			            
            $finalquant.=$quant;
        }
        $productsInCart[$i]['quantity_breakup']=$finalquant;
    }

    $k=0;$j=0;
    $categories = array();
    $productids = array();
    $discountonoffer = 0;
    foreach($productsInCart as $key => $order)
    {
    //Query for retriving the style name from mk_product_style
    $stylename = "SELECT name, styletype FROM $sql_tbl[mk_product_style] WHERE id = '".$productsInCart[$key]['product_style']."'";
    $styleresult = db_query($stylename);
    $rowstyle = db_fetch_array($styleresult);
    //get design name from xcart_products
    $productname = "SELECT product FROM xcart_products WHERE productid = ".$productsInCart[$key]['productid']."";
	$productresult = db_query($productname);
	$productrow = db_fetch_array($productresult);
    if(!empty($productrow))
	$productsInCart[$key]['designName'] = $productrow['product'];

    //Query for retriving the style options from mk_product_options
    $styleoption = "SELECT $sql_tbl[mk_product_options].name AS optionName, $sql_tbl[mk_product_options].value AS optionValue FROM $sql_tbl[mk_product_options], $sql_tbl[mk_product_options_order_details_rel]
    WHERE $sql_tbl[mk_product_options].id = $sql_tbl[mk_product_options_order_details_rel].option_id
    AND $sql_tbl[mk_product_options].style = ".$productsInCart[$key]['product_style']."
    AND $sql_tbl[mk_product_options_order_details_rel].orderid = '".$orderid."'
    AND $sql_tbl[mk_product_options_order_details_rel].itemid = '".$productsInCart[$key]['productid']."'";

    $optionresult = db_query($styleoption);
    $optionCount = db_num_rows($optionresult);
    $productsInCart[$value]['optionCount'] = $optionCount;
    $i = 0;
    while($optionrow = db_fetch_array($optionresult)){
        $productsInCart[$key]['optionNames'][$i] =  $optionrow['optionName'];
        $productsInCart[$key]['optionValues'][$i] =  $optionrow['optionValue'];
        $i++;
    }

    $typename = "SELECT pt.name as productName, pt.label as pLabel, pt.image_t as pImage, ps.name as sName
                FROM $sql_tbl[producttypes] AS pt, $sql_tbl[mk_product_style] AS ps
                WHERE pt.id = ps.product_type
                AND pt.id = ".$productsInCart[$key]['product_type']."
                AND ps.id=".$productsInCart[$key]['product_style']."";
    $typeresult = db_query($typename);
    $row = db_fetch_array($typeresult);
    $productsInCart[$key]['productStyleName'] = $row['sName'];
	if($productsInCart[$key]['addonid'])
	{
        	$addonname = get_style_addon_name($productsInCart[$key]['addonid']);
        	if($addonname)
        	{
        	        $productsInCart[$key]['productStyleName'] .= " ( $addonname ) ";
        	}
	}	


    $areaId = func_get_all_orientation_for_default_customization_area($productsInCart[$key]['product_style']);

    $pid = $productsInCart[$key]['productid'];
    $defaultImage = "SELECT image_portal_t  as ProdImage FROM $sql_tbl[products] WHERE productid =$pid";

    $ImageResult = db_query($defaultImage);
    $ImageRow = db_fetch_array($ImageResult);

    $productsInCart[$key]['productStyleType'] = $rowstyle['styletype'];
    $productsInCart[$key]['product_type'] = $rowstyle['name'];
    $productsInCart[$key]['productTypeLabel'] = $row['pLabel'];
    $productsInCart[$key]['designImagePath'] = $ImageRow['ProdImage'];
    $productsInCart[$key]['totalPrice'] = $productsInCart[$key]['price'] * $productsInCart[$key]['amount'];

    $allCustImagePath = func_load_product_customized_area_rel_details($pid);
    foreach($allCustImagePath as $k=>$value){
        if($value['thumb_image'] != ""){
            $productsInCart[$key]['allCustImagePath'][$value['area_name']]['thumb_image'] = $value['thumb_image'];
            $productsInCart[$key]['allCustImagePath'][$value['area_name']]['large_image'] = $value['image_name'];
        }
        $productsInCart[$key]['allCustImagePath'][$value['area_name']]['area_id']=$value['area_id'];
    }

    //To get the categoryids of the product
        $catidquery = "select $tb_product_category.categoryid from
        $tb_product_category where $tb_product_category.productid =".$productsInCart[$key]['productid']."";
        $catidresult=db_query($catidquery);

        if($catidresult){
            $row=db_fetch_row($catidresult);
            if(!in_array($row[0], $categoryids) && ($row[0] != "")){
                $categoryids[$k] = $row[0];
            }
        }
       $k++;
       // create an array of product ids
        $productids[$j] = $productsInCart[$key]['productid'];
        $j++;
      // Count total discount of offer
      $discountonoffer = $discountonoffer + $productsInCart[$key]['productDiscount'];
    }
     //Coupon discount
      $coupondiscount = $productsInCart[0]['coupon_discount'];
     //Coupon Code
     $couponCode = addslashes($productsInCart[0]['couponcode']);
     if(!empty($couponCode))
     {
          $couponTable        = $sql_tbl["discount_coupons"];
          $queryCouponPercent  = "SELECT c.discount,c.coupon_type from $couponTable c where c.coupon = '$couponCode'
          and status = 'A' ";
          $rsCouponPercent  = db_query($queryCouponPercent);
          $rowCouponPercent = db_fetch_array($rsCouponPercent);
          if($rowCouponPercent['coupon_type']=='percent')
            $smarty->assign("couponpercent", $rowCouponPercent['discount']."%");
          else
            $smarty->assign("couponpercent", "Rs. ".$rowCouponPercent['discount']);
    }

    $totalamount = $productsInCart[0]['subtotal'];
    $amountafterdiscount = $productsInCart[0]['total'];
    $shipping_cost = $productsInCart[0]['shipping_cost'];
    $vatCst = $productsInCart[0]['tax'];
    $ref_discount = $productsInCart[0]['ref_discount'];

    // calculating the final amount to be paid after  calculating all the taxes
    $amountafterdiscount = $amountafterdiscount + $vatCst;

    if($order_data["order"]['payment_method'] == 'cod')
    {
        $amountafterdiscount = $amountafterdiscount + $order_data["order"]['cod'];
        $paymentoption=$order_data["order"]['payment_method'];
    }

    //Call function to get special offer of productid
    $productOffer = func_get_discount_offers_data($productids, 'P');

    //Call function to get special offer of category id
    $categoryOffer = func_get_discount_offers_data($categoryids, 'C');

    $offers = array_merge($productOffer, $categoryOffer);
    $countoffer = count($offers);

    //Call function to get special offers for total
    $totaloffers = func_get_special_offers_for_Total();
    $counttotaloffer = count($totaloffers);

    //Get special offer for discount on total
    $amountaftersofferdiscount = $totalamount - $discountonoffer;
    $discountOnTotal = func_get_special_offer_discount_for_Total($amountaftersofferdiscount);

    //Count total discount on offer
    $totaloffersdiscount = $discountonoffer + $discountOnTotal;

    if(empty($productsInCart)){
        $productsInCart = 0;
    }

    //Code for printing change requests for a order
    if(func_get_order_changereqstatus($orderid)=='Y'){
        $comments=func_getorder_requests_sheet($orderid);
        $changestatus = "CHANGE REQUEST Pending.";
    }else{
        $changestatus = "No Change Request Pending.";
    }

    /*$affiliate=func_get_order_affiliate($orderid);
    if(!empty($affiliate)){
        $isAffiliate = "Gift Wrapping Needed.";
        $smarty->assign("affiliatename", $affiliate);
    }else{
        $isAffiliate = "No Gift Wrapping Needed.";
    }*/

    if($order_data["order"]['gift_status'] == 'Y')
      $isGiftwrap = 'Gift Wrapping Needed';
    else
      $isGiftwrap = 'No Gift Wrapping Needed';


    $replacements=func_getorder_Replacementlog_sheet($orderid);

    $qaStatus="No QA Comments";
    $qaLog=func_getorder_QAlog_sheet($orderid);
    if($qaLog){
        $qaStatus="See QA FAILURE log";
    }

    if(!$replacements){
        $replacementstatus="No replacement comments";
    }
    else{
        $replacementstatus="Refer to REPLACEMENT LOG";
    }
    $giftmessage = func_query_first("SELECT notes, gift_status FROM $sql_tbl[orders] WHERE orderid='$orderid'");

	$query = "select shop_master_id from mk_sources s left join xcart_orders o on s.source_id=o.source_id where o.orderid='$orderid' " ;
	$shop_master_id = func_query_first_cell($query);

	if(is_numeric($shop_master_id)){
		$query = "select mk_shop_masters.* from xcart_orders,mk_sources,mk_shop_masters 
			where xcart_orders.source_id=mk_sources.source_id and 
			mk_sources.shop_master_id=mk_shop_masters.shop_master_id and xcart_orders.orderid=$orderid";
		$shop_master_details = func_query_first($query);
		if($shop_master_details[specialpacking] == 1){
			$specialpacking= "Yes Special Packaging required.";
			$specialpackingsm= "Special Packaging required as per shop master<b> $shop_master_details[name]</b>.";
		}else{
			$specialpacking= "No Special Packaging required";
		}
		if($shop_master_details[specialpacking] == 1){
		        $addreliancesticker .= "* Please stick the $shop_master_details[name] sticker on this package available at this link <a href='{$baseurl}admin/reliancesticker.php?orderid=$orderid&type=admin'>Sticker</a>";
		}
	}	
    //echo "<pre>";
    //print_r($productsInCart);
    //exit;

    #
    #Creation of pdf format
    #
    if($order['shipment_preferences'] == 'shipped_together'){
        $shippedtogather = "Shipped Together";
    }else{
        $shippedtogather = "Shipped as Available";
    }


    $pdfString="";
    if($format != "html"){
      $pdfString .= "<html>";
      $pdfString .= "<head>";
      $pdfString .= "<title>quote pdf</title>";
      $pdfString .= "<meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' />";
      $pdfString .= "</head>";
      $pdfString .= "<body>";
	}
    $pdfString .= "<p ><center><b> ORDER PROCESSING SHEET </b></center></p>";
    $pdfString .= "<table width='100%' cellpadding='0' cellspacing='2' border='0' >

                        <tr bgcolor='#FF99FF'>
                            <td width='25%' >Order No# $orderid ($order_name)</td>
                            <td width='30%'>Queued Date# $today <BR> (";
    if($pastdue)  $pdfString .="PAST DUE: ";
    $pdfString .="Estimated Ops Completion: $estimated_completion)</td>
                            <td width='45%' >Customer Name:# $customerdetail[b_firstname]&nbsp;$customerdetail[b_lastname]<BR> Source: $source_name</td>
                        </tr>
                        <tr>
 <td class='align_left' colspan='0'>
                                <img src='{$baseurl}/include/class/class.barcode.php?code=".$orderid."&text=0&type=.png' width='150' height='80' />
                                </td>
                            <td ><font color='#FF0000' face='arial' ><b>Contact number for issues::</b></font><font color='#FF0000' face='arial' style='bold'><b>$order[issues_contact_number]</b></font></td>";
                        if(isset($paymentoption) && $paymentoption== 'cod')
                        $pdfString .= "<td><font size='6'><b>Cash on Delivery Order<b></font></td>";

                        $pdfString .="  </tr>
                        <tr>
                            <td ><font color='#660000' face='arial' ><b>Courier::$order_courier</b></font></td>
                            <td colspan='2'><font color='#FF0000' face='arial' ><b>Order is to be::</b></font><font color='#FF0000' face='arial' style='bold'><b>$shippedtogather</b></font></td>
                            
                        </tr>
                        <tr>
                            <td></td>
                            
                        </tr>
                   </table>";
    $pdfString .= "<table width='100%'>
                        <tr>
                            <td width='50%' bgcolor='#FFE049'>$changestatus</td>
                            <td width='50%' bgcolor='#FFFF00'>$replacementstatus</td>
                        </tr>
                        <tr>
                            <td width='50%' bgcolor='#F824C5'>$qaStatus</td>
                            <td width='50%' bgcolor='#F874C7'>$isGiftwrap</td>
                        </tr>
                        <tr>
                            <td width='50%' bgcolor='#FFE049'>$specialpacking</td>
                            <td width='50%' bgcolor='#FFFF00'></td>
                        </tr>
                   </table>";

    if($productsInCart){
        foreach($productsInCart AS $key=>$value){
            $pdfString .= "<table width='100%'>";
            $pdfString .= "<tr >
                                <td bgcolor='#F874C7'>
                                    ItemID# <b>$value[itemid]</b> </td>
                                    <td bgcolor='#F874C7'>Type:<b>$value[productTypeLabel]</b>
                                </td>
                                 <td bgcolor='#F874C7'>
                                    Style:<b>$value[productStyleName]</b> (Article Id: $value[article_number])
                                </td>
                                <td bgcolor='#F874C7'>
                                    Assignee:<b>$value[assignee_name]</b>
                                </td >
                                <td class='align_left' colspan='0'>
                                <img src='{$baseurl}/include/class/class.barcode.php?code=".$value['itemid']."&type=.png' width='100' height='60' />
                                </td>



                          </tr>";

           $pdfString .= "<tr  bgcolor='#A6D3E1' >

                                <td>
                                    Total Qnt:<b>$value[amount]</b>
                                </td>
                                ";
           //html to pdf gets confused about the type - expects a .png .jpeg ...so on. gets confused on looking at a php. so needs to be fooled. fix it later.
           if($value[quantity_breakup]!=""){
               //$breakupstr=qtybreakup_string_cleanup($value[quantity_breakup]);
               $breakupstr=($value[quantity_breakup]);
               $pdfString .=  "<td>
                              <b>$breakupstr</b>
                              </td>";

           }
           $pdfString .= "<td>
                                    Loc:<b>$value[location_name]</b>
                          </td>";


            if($value['is_customizable'] == '0')
               $pdfString .= "<td ><b>Non Personalized Product</b></td>";
               $pdfString .= "</tr>";

           $pdfString .= "</table>";
           $pdfString .= "<table width='100%'><tr><td height='1%' ></td></tr></table>";

           $pdfString .= "<table width='100%' border='2' bordercolor='#CC0000'>";

                    if ($value['allCustImagePath']){
                        foreach($value['allCustImagePath'] AS $area_name=>$array){
                            $absoute_thumb_image_path = $array[thumb_image];
                            $absoute_large_image_path = $http_location.substr($array[large_image],1);

                            if(stristr($absoute_thumb_image_path, '.png')){
                                 $portalimage = new ImageConverter($absoute_thumb_image_path,"jpeg");
                                 $thumbnail_Image = str_ireplace('.png', '.jpg', $absoute_thumb_image_path);
                                 addToTempImagesList($xcart_dir.substr($absoute_thumb_image_path,1));
                            }else{
                                $thumbnail_Image = $absoute_thumb_image_path;
                            }
                            $prodid=$productsInCart[$key][productid];
                            $tempareaid=$value['allCustImagePath'][$area_name]['area_id'];
                            //adding design name
                            $designName=$productsInCart[$key]['designName'];
                            $pdfString .=  "<tr>";
                            //$pdfString .= "<td class='align_left' colspan='0'><b>$area_name::</b>&nbsp;<img src='http://www.myntra.com/include/class/class.barcode.php?code=".$value['itemid']."&type=.png' width='100' /></td>";
                            $pdfString .= "<td class='align_left' colspan='0'><b>$area_name::</b>&nbsp;<img src='$thumbnail_Image' width='150' /></td>";
                            $pdfString .= "<td class='align_left' colspan='0'><b>$area_name::</b>&nbsp;

                                                <!a href='$absoute_large_image_path' target='_blank'><!See Large image><!/a> <BR>
                                                <a href=\"$http_location/admin/view_full_image.php?mode=large&orderid=$orderid&productid=$prodid&areaid=$tempareaid\">[ Download Guiding Image ]</a><BR>
                                                <a href=\"$http_location/admin/view_full_image.php?mode=orignal&orderid=$orderid&productid=$prodid&areaid=$tempareaid\">[ Download Original Image ]</a><BR>";
                             if(!empty($designName)) {
                                $pdfString .= "Design Name: ".$designName;
                             }
                             $pdfString .= "</td>";
                            $pdfString .= "</tr>";
                        }

                    }else{
			 $pdfString .= "<td class='align_left' colspan='0'><img src='".$productsInCart[$key]['designImagePath']."'/></td>";
                    }

            $pdfString .= "</table>";
    }

        //Add shipping information here

        $pdfString .= "<BR>";
        $pdfString .= "<table width='100%'>";
        $pdfString .= "<tr bgcolor='#A9A9C2'>";
        $pdfString .= "<th class='align_left' colspan='3'>Packing Information</th>";
        $pdfString .= "</tr>";
           $pdfString .= "<tr><th height='1%' colspan='3' bgcolor='#FF0000'>$specialpackingsm</th></tr>";
           $pdfString .= "<tr><th height='1%' colspan='3' bgcolor='#FF0000'>$addreliancesticker</th></tr>";

        foreach($productTypeCount as $prod){
		    $num_boxes=ceil($prod[count]/$prod[capacity]);
            $pdfString .= "<tr>
                                <td><b>$prod[count]  $prod[name]</b></td>
                                <td><b>Pack in $num_boxes boxes</b></td>
                                <td><b>Allowed $prod[capacity] per box</b></td>

                          </tr>";
            }

        $pdfString .= "</table>";
		$query = " select total+shipping_cost+tax as total,date from xcart_orders where orderid='$orderid'";
		$totalamount = func_query_first($query);
		
        if($changestatus == "CHANGE REQUEST Pending.")
        {
            $pdfString .= "<table width='100%'>";
            $pdfString .= "<tr bgcolor='#A9A9C2'>";
            $pdfString .= "<th class='align_left' colspan='6'>Change Requests</th>";
            $pdfString .= "</tr>";

            foreach($comments AS $key=>$value){
                $pdfString .= "<tr>
                                    <td><b>$value[date]</b></td>
                                    <td><b>$value[name]</b></td>
                                    <td><b>$value[desc_part]</b></td>
                                    <td><b>$value[newaddress]</b></td>
                                    <td><b>$value[giftwrapmessage]</b></td>
                                    <td><b><a href=\"$http_location/admin/data/$value[attachmentname]\" TARGET = \"_blank\"> $value[attachmentname]</a></b></td>
                              </tr>";
            }

            $pdfString .= "</table>";
        }

        if($replacements){
            $replacementstatus="Refer to REPLACEMENT LOG";
            $pdfString .= "<table width='100%'>";
            $pdfString .= "<tr bgcolor='#A9A9C2'>";
            $pdfString .= "<th class='align_left' colspan='3'>Replacement notes</th>";
            $pdfString .= "</tr>";

            foreach($replacements AS $key=>$value){
                $pdfString .= "<tr>
                                    <td><b>$value[date]</b></td>

                                    <td><b>$value[desc_part]</b></td>
                                     <td><b>$value[commenttitle]</b></td>

                              </tr>";
                }

            $pdfString .= "</table>";
       }

       if($qaLog){

            $pdfString .= "<table width='100%'>";
            $pdfString .= "<tr bgcolor='#A9A9C2'>";
            $pdfString .= "<th class='align_left' colspan='4'>QA Failure Log</th>";
            $pdfString .= "</tr>";

            foreach($qaLog AS $key=>$value){
                $pdfString .= "<tr>
                                    <td><b>$value[date]</b></td>
                                    <td><b>$value[name]</b></td>

                                    <td><b>$value[desc_part]</b></td>
                                     <td><b>$value[commenttitle]</b></td>

                              </tr>";
                }

            $pdfString .= "</table>";
       }


        if($giftmessage['gift_status'] == "Y"){
                $pdfString .= "<table width='100%'>
                                <tr>
                                    <td>
                                        <h3>Gift&nbsp;Message:<br>$giftmessage[notes]</h3>
                                    </td>
                                </tr>
                              </table>";
        }
    }


   if($format != "html"){
      $pdfString .= "</body>";
      $pdfString .= "</html>";
      $pdfString .= "</body>";
      $pdfString .= "</html>";
    }
    $pdfStringArray[$orderid]=$pdfString;

}

if($format == "html"){
      $htmlString = "<html>";
      $htmlString .= "<head>";
      $htmlString .= "<title>quote pdf</title>";
      $htmlString .= "<meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' />";
      $htmlString .= "</head>";
      $htmlString .= "<body>";
	  echo $htmlString;
  foreach($pdfStringArray as $orderId=>$orderInfo)
   echo $orderInfo;
      $htmlString = "</body>";
      $htmlString .= "</html>";
      $htmlString .= "</body>";
      $htmlString .= "</html>";
	  echo $htmlString;
}
else{
  $pdf=new HTML2FPDF();

  foreach($pdfStringArray as $orderId=>$orderInfo){
   $pdf->AddPage();
   $pdf->WriteHTML($orderInfo);
  }

  $pdf->Output("$pagename","D");
}




?>
