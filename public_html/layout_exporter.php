<?php

/* This is the touch point between the nodejs server and the php-fpm.
 * Page Layout (Header, Footer and TopNav) required for the node server will be exposed here */

require_once($xcart_dir."auth.php");

$response = array();

$response['topnav'] = $smarty->fetch("inc/navigation-default.tpl");
$response['header'] = $smarty->fetch("inc/header-new.tpl");
$response['footer'] = $smarty->fetch("inc/footer-new.tpl");
$response['login'] = $smarty->fetch("inc/modal-signup.tpl");
$response['seo'] = $smarty->fetch("inc/head-meta.tpl");
$response['newrelicheader'] = $smarty->fetch("inc/newrelic-header.tpl");
$response['newrelicfooter'] = $smarty->fetch("inc/newrelic-footer.tpl");
$response['signupconfirmation'] = $smarty->fetch("inc/modal-signupconfirmation.tpl");
$response['festiveheader'] = $smarty->fetch("inc/fest-promo.tpl");
$response['nudge'] = $smarty->fetch("inc/nudge.tpl");
$layoutConf = MinifyConfig_skin2::$conf['groups']['layout'];
$response['js'] = $layoutConf['js'];
$response['css'] = $layoutConf['css'];
$response['min_js'] = $layoutConf['min_js'];
$response['min_css'] = $layoutConf['min_css'];
$response['mingz_js'] = $layoutConf['mingz_js'];
$response['mingz_css'] = $layoutConf['mingz_css'];
$response['status']['statusType'] = 'SUCCESS';
//echo '<pre>'; print_r($response); exit;
header("Content-Type:application/json");
echo json_encode($response);
exit;
