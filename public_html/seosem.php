<?php
@include_once ($xcart_dir."/include/func/func.seo.php");
@include_once ($xcart_dir."/include/func/func.search.php");
// We need to include this only for customers accessing the site.
include_once($xcart_dir."/modules/sem/Sem.php");//need to have it included after init.php
require_once 'env/System.config.php';

################## load the title , meta description and keywords  #############
$pageurl = $_SERVER['REQUEST_URI'];
$url_element = explode("/",$pageurl);
$fcindex = array_search("FC",$url_element);
$affiliateInfo = $XCART_SESSION_VARS['affiliateInfo'];

global $xcache ;
if($xcache==null){
	$xcache = new XCache();
}

### IF NOT FILTER URL [ IF FC IS PRESENT THEN INDEX.PHP WILL DECIDE THE TITLE ]
if($fcindex === false)
{
		$pageName = basename($_SERVER['SCRIPT_NAME']);
        $cached_seo_data = $xcache->fetchAndStore(function($pageName){
                $cached_seo_data = array();
                $cached_seo_data['pageTitle'] = getTitleTag($pageName);
                $cached_seo_data['metaDescription'] = getMetaDescription($pageName);
                $cached_seo_data['metaKeywords'] = getMetaKeywords($pageName);
                $cached_seo_data['seoExtraContents'] = getExtraContents($pageName);
				
                return $cached_seo_data;
        }, array($pageName), "seodata:".$pageName.":cached_seo_data",1800);

        $pageTitle = $cached_seo_data['pageTitle'];
	$smarty->assign("pageTitle",$pageTitle);
        $metaDescription = $cached_seo_data['metaDescription'];
        $metaKeywords = $cached_seo_data['metaKeywords'];
        $seoExtraContents = $cached_seo_data['seoExtraContents'];
	$smarty->assign("external_content", $seoExtraContents['external_content']);
	$smarty->assign("viewcomment", $seoExtraContents['comments']);
	$smarty->assign("rssfeeds", $seoExtraContents['rssfeeds']);
	$smarty->assign("h1tag", $seoExtraContents['h1tag']);
	$smarty->assign("h2tag", $seoExtraContents['h2tag']);
	$smarty->assign("relatedkeywords", $seoExtraContents['relatedkeywords']);
	$smarty->assign("imagesource", $seoExtraContents['imagesource']);
	if($affiliateInfo[0]['title'] != '')
	{
		 $smarty->assign("pageTitle",$affiliateInfo[0]['title']);
	}
	else
	{
		 $smarty->assign("pageTitle",$pageTitle);

	}
	$smarty->assign("metaDescription", $metaDescription);
	$smarty->assign("metaKeywords", $metaKeywords);

}

 ################### end #################################################
/*
 * to show SEM cross-promotional banner on specific keyword search
 */
$sem = new Sem();
if(!empty($_GET['semp'])){
    $promoInfo = $sem->getPromo(array("identifier"=>$_GET['semp']));
    if(is_array($promoInfo) && count($promoInfo) == 1){
        //banner to be shown based on both session and timeout cookie
        $sem->setSEMCookie(Sem::SEM_PROMO_SESSION_COOKIE_NAME,'1',0);
        $sem->setSEMCookie(Sem::SEM_PROMO_COOKIE_NAME,$promoInfo,time()+60*30);//sets response header

        /* since setcookie, getcookie sets different headers,
         * in a single page their content may differ(every time the cookie is updated by a new identifier).
         * And at first time cookie is set, getcookie return empty data.
         */
        $smarty->assign('SEMpromo',1);
        $smarty->assign('semPromoHeaderText',$promoInfo[0]['promo_content']);
        $smarty->assign('semPromoUrl',$promoInfo[0]['promo_url']);
    }
} else {
    $promoSessionCookie = $sem->getSEMCookie(Sem::SEM_PROMO_SESSION_COOKIE_NAME);
    $promoSessionCookieData = $promoSessionCookie['content'];
    $promoCookie = $sem->getSEMCookie(Sem::SEM_PROMO_COOKIE_NAME);
    $promoCookieData = $promoCookie['content'];
    if(!empty($promoCookieData) && !empty($promoSessionCookieData)){
        $smarty->assign('SEMpromo',1);
        $smarty->assign('semPromoHeaderText',$promoCookieData[0]['promo_content']);
        $smarty->assign('semPromoUrl',$promoCookieData[0]['promo_url']);
    }
}

/*
 * to show landing page according to SEM routing engine
 */
if($_GET['drte'] == 1){//drte is the paramenter on which routing engine takes the decision
    $uri = $_SERVER['REQUEST_URI'];
    $uri = trim($uri,'/');
    $uriStart = 0;
    $uriEnd = strpos($uri,'?');

    if($uriEnd === false){
        $uriEnd = strlen($uri);
    }
    //substring after domain name and before '?'
    $uriSubStr = substr($uri,$uriStart,$uriEnd);
    $routingInfo = $sem->getRoutingInfo(array("url"=>$uriSubStr));

    if(!empty($routingInfo)){
        if($routingInfo[0]['is_active'] == 1){

            //build routing url using landing-url and query_string(except &dtre=1 or &amp;dtre=1)            
            unset($_GET['drte']);
            $queryString = http_build_query($_GET);
            if(!empty($queryString)){
                $landingUrl = $http_location."/".trim($routingInfo[0]['landing_url'],'/')."?".$queryString;
            } else {
                $landingUrl = $http_location."/".trim($routingInfo[0]['landing_url'],'/');
            }
            
            //302 redirect to new url created
            func_header_location($landingUrl);
        }
    }
}

/**
the utm_track_30 won't be supported anymore but we still need to transform
such client side cookies, if any to the latest form
*/
transformUTM30to1();
$host = getDomain($_SERVER['HTTP_REFERER']);
global $isCPC;
global $isOrganicReferer;
global $seoHost;
$isCPC = false;
$seoHost = $host;
$isOrganicReferer = isOrganicReferer($host);
// if there are either of these query params, update cookie 
if ((!empty($_GET['utm_source']) || !empty($_GET['utm_medium']))) {
    setUTM($_GET['utm_source'], $_GET['utm_medium']);
/* CLEANUP
    setActive($_GET['utm_source']);
 */
} else {
    if (!empty($_GET['utm_source_confirm']) && !empty($_GET['utm_medium_confirm'])) { //payment gateway redirects
        setUTM($_GET['utm_source_confirm'], $_GET['utm_medium_confirm']);
    } else if (!empty($_SERVER['HTTP_REFERER'])) {  // in general: google, facebook etc
        //$host = getDomain($_SERVER['HTTP_REFERER']);

        //to filter out domains myntra, payseal etc
        preg_match ("/[^\.\/]+\.[^\.\/]+$/", $host, $matches);
        $domain = $matches[0];

        if (!empty($domain) && $domain != 'myntra.com' && $domain != 'payseal.com') {   //payseal: icici payment gateway
            //only to track google cpc (gclid -s is the only case when we click on myntra ad in google search result)
            if (((!empty($_GET['gclid']) || !empty($_GET['cpc'])) && !empty($host)) ||
                (stripos($host, 'doubleclick') != false)) {
                $isCPC = true;
                setUTM($host, "cpc");
            } else {
                setUTM("referral", $host);
            }
        } else {
            if (!isUTMValid()) {
                setUTM("direct", "direct");
            }
        }
    } else {
        if (!isUTMValid()) {
            setUTM("direct", "direct");
        }
    }
}


//mythings control(selective tracking) for confirm and rest of the pages
$enableMyThings = FeatureGateKeyValuePairs::getFeatureGateValueForKey('enableMyThings');
if($enableMyThings != NULL && $trackingCookie['aff']['trackstatus'] == 1 && $_SERVER['PHP_SELF'] != '/mkorderBook.php'){    
	$smarty->assign("enableMyThings", $enableMyThings);
    
}else{
    if($_SERVER['PHP_SELF'] == '/mkorderBook.php' && $enableMyThings != NULL && $trackingCookie['aff']['trackstatus'] == 1){
	    $smarty->assign("enableMyThingsConfirmPage", $enableMyThings);
    }
}

/*
 * SEO remarketting logic 
 */ 
$enableGoogleCPC = FeatureGateKeyValuePairs::getFeatureGateValueForKey('enableGoogleCPC');
if($enableGoogleCPC != NULL){
	$smarty->assign("enableGoogleCPC", $enableGoogleCPC);
	$smarty->assign("pageurl", $_SERVER['REQUEST_URI']);
}

$displayRemarkettingtag = false;
$utm_source = getUTMSource();
$utm_medium = getUTMMedium();

if (!empty($_GET['gclid'])) {
	$label = SystemConfig::$googleCPCLabel;
	$color = SystemConfig::$googleCPCColor;
	$displayRemarkettingtag = true;
} else if ($utm_source == 'aff') {
	$label = SystemConfig::$affGCLabel;
	$color = SystemConfig::$affGCColor;
	$displayRemarkettingtag = true;
} else if ( ((stripos($utm_medium, 'google') !== false) ||
             (stripos($utm_medium, 'yahoo')  !== false) ||
             (stripos($utm_medium, 'bing')   !== false) ||
             (stripos($utm_medium, 'search') !== false))
            && $utm_source == 'referral') {
	// medium for seo can have the strings google/yahoo/bing/search
	$label = SystemConfig::$seoGCLabel;
	$color = SystemConfig::$seoGCColor;
	$displayRemarkettingtag = true;
} else if (substr($utm_source, 0, strlen('fb')) === 'fb') {
	// Remarketting for FB and SEO visitors
	$label = SystemConfig::$fbGCLabel;
	$color = SystemConfig::$fbGCColor;
	$displayRemarkettingtag = true;
} else if($_SERVER['REQUEST_URI'] == '/') {
	$label = SystemConfig::$homeGCLabel;
	$color = SystemConfig::$homeGCColor;
	$displayRemarkettingtag = true;
}

$smarty->assign("displayRemarkettingTag",$displayRemarkettingtag);
$smarty->assign('googe_conversion_label',$label);
$smarty->assign('googe_conversion_color',$color);

function isOrganicReferer($host) {
    $organicReferer = false;
    if(stripos($host, 'google') !== false  ||
        stripos($host, 'yahoo') !== false  ||
        stripos($host, 'bing') !== false  ||
        stripos($host, 'babylon') !== false  ||
        stripos($host, 'conduit') !== false  ||
        stripos($host, 'ask') !== false  ||
        stripos($host, 'yandex') !== false  ||
        stripos($host, 'avg') !== false  ||
        stripos($host, 'aol') !== false  ||
        stripos($host, 'comcast') !== false  ||
        stripos($host, 'incredimail') !== false  ||
        stripos($host, 'baidu') !== false  ||
        stripos($host, 'search') !== false) {
        $organicReferer = true;
    }

    return $organicReferer;
}
?>
