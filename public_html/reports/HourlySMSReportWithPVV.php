<?php
require "../authAPI.php";
include_once $xcart_dir."/reports/models/date_calculation.php";
include_once $xcart_dir."/reports/mongodb.config.php";
include_once $xcart_dir."/include/func/func.sms_alerts.php";
if(!defined('DISPLAY_XPM4_ERRORS'))
define('DISPLAY_XPM4_ERRORS', true);
require_once $xcart_dir.'/XPM/MAIL.php';

//$CHANNEL_VISIT_COLLECTION = 'numberOfVisitsPerChannelOutCollection';
$CHANNEL_VISIT_COLLECTION = 'channelDetailsOutCollection';
$smsPrefix  = 'P1';


function format($number, $decimals ,$format=true) {
		if(!$format)
		return round($number,$decimals);
		$format=number_format($number, $decimals, '.', ',');
		$strsplit=split(',',$format);
		$out=$strsplit[count($strsplit)-1];
		$icount=0;
		for($i=count($strsplit)-2;$i>=0;$i--){
			for($j=strlen($strsplit[$i])-1;$j>=0;$j--,$icount++){
				if($icount%2==0){
					$out=','.$out;
				}
				$out=$strsplit[$i][$j].$out;
				
			}
		}
		return $out;
}	
$phone_nums=constant("ORDERS_HOURLY_ALERT");
$phones=split(',', $phone_nums);
$dateslash = date("m/d/Y");
$date_split=split('/', $dateslash);
$current_month=(INT)$date_split[0];
$current_year=(INT)$date_split[2];
$current_date=(INT)$date_split[1];
$curr_fdate= new FDate($current_date, $current_month, $current_year);


$sql="select  count(distinct(group_id)) total,sum(total+gift_charges+shipping_cost) revenue from xcart_orders where date > unix_timestamp('".$curr_fdate->toUnixDateDayStart()."') and status in('C','Q','WP','OH','SH','DL')";

$result =func_query_first($sql, true);

$msg=date("d/m/Y h:i:s A");
if(empty($result)|| $result['total']==0){
	$msg.=" ZERO Orders";
	
}else{
	$msg.=" #Orders:" . $result['total'];
	$msg.=" Sales:".format($result['revenue'],0)."/-";
}

try{
	$CHANNEL_DETAILS_COLLECTION  = 'channelDetailsOutCollection';
	$CHANNEL_VISIT_COLLECTION = 'numberOfVisitsPerChannelOutCollection';
	$UNIQUE_VISIT_COLLECTION = 'uniqueVisitsPerChannelOutCollection';

	//$connection = new Mongo($dbHost,$dbOption);
	$connection = new Mongo($dbHost);
	$db = $connection->$dbName;

	$detailsCollection = $db->$CHANNEL_DETAILS_COLLECTION;
	$visitsCollection = $db->$CHANNEL_VISIT_COLLECTION;
	$uniqueVisitCollection = $db->$UNIQUE_VISIT_COLLECTION;

	$numOfVisits = 0;
	$netRevenue = 0;
	$visitsExcludingAff = 0;
	$revenueExcludingAff = 0;
	$visitsDirect = 0;
	$revenueDirect = 0;
	$detailsCursor = $detailsCollection->find();
	$channelNameArr = array();
	foreach($detailsCursor as $detailCursorResult)
	{
		array_push($channelNameArr,$detailCursorResult["_id"]);
	}
	
	$channelValuesArr = array();
	
	foreach($channelNameArr as $channelName)
	{

		$detailsValues = null; $visitsValues = null;
		$bounces = $uniqueVisitCollection->count(array("_id.channel" => $channelName, "value.count" => 2));
		//$nonBouces = $uniqueVisitCollection->count(array("_id" => $channelName, "value.count" => array('$t' => 2)));

		$query = array("_id" => $channelName);

		$detailsResult = $detailsCollection->findOne($query);
		if(!is_null($detailsResult))
			$detailsValues = $detailsResult["value"];

		$visitsResult = $visitsCollection->findOne($query);
		if(!is_null($visitsResult))
			$visitsValues = $visitsResult["value"];
			
		$detailsValues['bounces'] = $bounces;
		$detailsValues['count'] = $visitsValues['count'];
			

		addDataToChannel($detailsValues, $channelValuesArr, "all");

		if($channelName != 'Affiliates'){
			addDataToChannel($detailsValues, $channelValuesArr, "non-affs");
		}
		if($channelName == 'Facebook' || $channelName == 'CPC' ){
			addDataToChannel($detailsValues, $channelValuesArr, "all-paid");
		}
		if($channelName == 'Facebook'){
			addDataToChannel($detailsValues, $channelValuesArr, "facebook");
		}
		if($channelName == 'Organic'){
			addDataToChannel($detailsValues, $channelValuesArr, "SEO");
		}
		if($channelName == 'Mailer'){
			addDataToChannel($detailsValues, $channelValuesArr, "mailer");
		}
		if($channelName == 'MRP'){
			addDataToChannel($detailsValues, $channelValuesArr, "mrp");
		}
		if($channelName == 'Direct'){
			addDataToChannel($detailsValues, $channelValuesArr, "direct");
		}
	}
	
	$pvv = $channelValuesArr['all']['revnue']/$channelValuesArr['all']['count'];
	$pvvExAff = $channelValuesArr['non-affs']['revnue']/$channelValuesArr['all']['count'];
	$pvvDirect = $channelValuesArr['direct']['revnue']/$channelValuesArr['all']['count'];
	$emailmsg = $msg;
	
	$msg.=" Total visits in last hour : $numOfVisits PVV1: Rs" . format($pvv,4);
	$msg.=" PVV2: Rs" . format($pvvExAff,4);
	$msg.=" PVV3: Rs" . format($pvvDirect,4);
	$msg .= $smsPrefix . $msg;
	
	$emailmsg .= " Conv " . format(100 * $channelValuesArr['all']['confirmation']/$channelValuesArr['all']['count'],3) . "\n";
	$emailmsg .= getPVVTable($channelValuesArr);
			
}catch (Exception $e){
	echo "\nexcpetion while querying mongo";
}

//echo $msg . "\n\n\n" . $emailmsg;exit;

foreach ($phones as $num){
	func_send_sms_cellnext($num, $msg);
}


$m = new MAIL;
$m->From("support@myntra.com", "Myntra Reporting Service");
			

$m->addto(constant('TEMP_GMAIL_ROUTING_EMAIL'), "");
//$m->addto("apoorva.gaurav@myntra.com", "");

$time=time();
$m->Subject("Hourly Order Alert:".date("m/d/Y h:i:s A ",$time)."O#:$result[total] S:".format($result['revenue'],0)."/-");
$m->Html($emailmsg);	
$mailresult=$m->Send('client') ? 'Mail sent !' : 'Error';

if($mailresult=='Error'){
	$mailmsg= "Report::Hourly SMS Report\rE-mail could not be sent due to some bad configuration ";
}
else{
	$mailmsg="Report::Hourly SMS Report\rReport Mailed Succesfully!!!!! ";
}
echo $mailmsg;

function getPVVTable($channelValueArr){
	//print_r($channelValueArr);
	$table = "<table border=2>";
		$table .= "<tr>";
			$table .= "<td>" . "chanel" . "</td>";
			$table .= "<td>" . "traffic" . "</td>";
			$table .= "<td>" . "Bouce %" . "</td>";
			$table .= "<td>" . "pvv" . "</td>";
			$table .= "<td>" . "list -> pdp %" . "</td>";
			$table .= "<td>" . "pdp -> cart %" . "</td>";
			$table .= "<td>" . "cart -> order %" . "</td>";
			$table .= "<td>" . "visits -> order %" . "</td>";
			$table .= "<td>" . "# of orders" . "</td>";
		$table .= "</tr>";
		
		foreach($channelValueArr as $k=>$v){
			$row = getPVVRow($k,$v);
			$table .= $row;	
		}
		
	$table .= "</table>";
	
	return $table;
}

function getPVVRow($name, $valueArr = array()){
	$row = "<tr>";
		$row .= "<td>" . $name . "</td>";
		$row .= "<td>" . $valueArr['count'] . "</td>";
		$row .= "<td>" . format(100 * $valueArr['bounces']  / $valueArr['count'] ,2) . "</td>";
		$row .= "<td>" . format($valueArr['revenue']  / $valueArr['count'] ,2) . "</td>";
		$row .= "<td>" . format(100 * $valueArr['pdpView']  / $valueArr['listPage'] ,2) . "</td>";
		$row .= "<td>" . format(100 * $valueArr['addToCart']  / $valueArr['pdpView'] ,2) . "</td>";
		$row .= "<td>" . format(100 * $valueArr['confirmation']  / $valueArr['addToCart'] ,2) . "</td>";
		$row .= "<td>" . format(100 * $valueArr['confirmation']  / $valueArr['count'] ,3) . "</td>";
		$row .= "<td>" . $valueArr['confirmation'] . "</td>";
	$row .="</tr>";
	return $row;
}

function addDataToChannel($input, &$output, $key = "all"){
	if(!empty($output[$key])){
		$output[$key]['bounces'] += $input['bounces'];
		$output[$key]['count'] += $input['count'];
		$output[$key]['listPage'] += $input['listPage'];
		$output[$key]['pdpView'] += $input['pdpView'];
		$output[$key]['addToCart'] += $input['addToCart'];
		$output[$key]['confirmation'] += $input['confirmation'];
		$output[$key]['revenue'] += $input['revenue'];
	}
	else
		$output[$key] = $input;
}

?>

