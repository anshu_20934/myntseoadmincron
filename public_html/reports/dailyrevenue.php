<?php
define("REPORTS_CONNECTION",true);
include "../admin/auth.php";
//require_once $xcart_dir."/include/security.php";
include_once $xcart_dir."/reports/models/date_calculation.php";

if(!defined('DISPLAY_XPM4_ERRORS'))
	define('DISPLAY_XPM4_ERRORS', true); // display XPM4 errors

require_once $xcart_dir.'/XPM/MAIL.php';
//db_query("SET SESSION time_zone = 'Asia/Calcutta'; ");

function format($number, $decimals ,$format=true) {
		if(!$format)
		return round($number,$decimals);
		$format=number_format($number, 2, '.', ',');
		$strsplit=split(',',$format);
		$out=$strsplit[count($strsplit)-1];
		$icount=0;
		for($i=count($strsplit)-2;$i>=0;$i--){
			for($j=strlen($strsplit[$i])-1;$j>=0;$j--,$icount++){
				if($icount%2==0){
					$out=','.$out;
				}
				$out=$strsplit[$i][$j].$out;
				
			}
		}
		return $out;
}	
$date=date("j-F-Y");
$dateslash = date("m/d/Y");
$date_split=split('/', $dateslash);
$current_month=(INT)$date_split[0];
$current_year=(INT)$date_split[2];
$current_date=(INT)$date_split[1];
$curr_fdate= new FDate($current_date, $current_month, $current_year);

$report="<html>
<head>

</head>
<body id=\"body\">
<div id=\"scroll_wrapper\">
<div id=\"container\">
<div id=\"display\">
<div class=\"center_column\">
<div class=\"print\">
<div class=\"super\">
<p>&nbsp;</p>
</div>
<div class=\"head\" style=\"padding-left: 15px;\">
<b>Myntra Daily Revenue Report: $date</b>
</div>";

$endtime = $curr_fdate->toUnixDateDayStart();
$starttime = $curr_fdate->addDays(-1)->toUnixDateDayStart();
$B2CData;

//BOOKING Yesterday //COD/NONCOD

$sql="select (case xo.payment_method when 'cod' then 'cod' else 'noncod' end) as p_method,count(distinct orderid) as orders,
sum((select sum(xod.amount) from  xcart_order_details xod where xod.orderid=xo.orderid )) as items 
,sum(xo.total+xo.shipping_cost+xo.gift_charges) as 'booking',avg((xo.total+xo.shipping_cost+xo.gift_charges)) as  'avgbooking',
avg((select avg(xod.price*xod.amount) from  xcart_order_details xod where xod.orderid=xo.orderid )) as 'avgitemprice'
from xcart_orders xo
where xo.date >= unix_timestamp('$starttime') and xo.date < unix_timestamp('$endtime') 
and xo.status  in ('Q','WP','OH','C','SH','DL') and xo.source_id=1 and (xo.orgid=0 or xo.orgid is null) group by p_method";

$result =func_query($sql, true);
foreach ($result as $row){
	if($row['p_method']=='noncod'){
		$B2CData['online-direct_pay']['yesterday']['booking']['orders']=$row['orders'];
		$B2CData['online-direct_pay']['yesterday']['booking']['items']=$row['items'];
		$B2CData['online-direct_pay']['yesterday']['booking']['booking']=$row['booking'];
		$B2CData['online-direct_pay']['yesterday']['booking']['avgbooking']=$row['avgbooking'];
		$B2CData['online-direct_pay']['yesterday']['booking']['avgitemprice']=$row['booking']/$row['items'];
	}else if($row['p_method']=='cod'){
		$B2CData['online-cod']['yesterday']['booking']['orders']=$row['orders'];
		$B2CData['online-cod']['yesterday']['booking']['items']=$row['items'];
		$B2CData['online-cod']['yesterday']['booking']['booking']=$row['booking'];
		$B2CData['online-cod']['yesterday']['booking']['avgbooking']=$row['avgbooking'];
		$B2CData['online-cod']['yesterday']['booking']['avgitemprice']=$row['booking']/$row['items'];
	}
}

//BOOKING MTD //COD/NONCOD

$sql="select (case xo.payment_method when 'cod' then 'cod' else 'noncod' end) as p_method,count(distinct orderid) as orders,
sum((select sum(xod.amount) from  xcart_order_details xod where xod.orderid=xo.orderid )) as items 
,sum(xo.total+xo.shipping_cost+xo.gift_charges) as 'booking',avg((xo.total+xo.shipping_cost+xo.gift_charges)) as  'avgbooking',
avg((select avg(xod.price*xod.amount) from  xcart_order_details xod where xod.orderid=xo.orderid )) as 'avgitemprice'
from xcart_orders xo
where DATE_FORMAT(from_unixtime(xo.date),'%m %y')  = DATE_FORMAT(DATE_SUB(CURDATE(),INTERVAL 1 DAY), '%m %y') and xo.date < unix_timestamp('$endtime')
and xo.status  in ('Q','WP','OH','C','SH','DL') and xo.source_id=1 and (xo.orgid=0 or xo.orgid is null) group by p_method";


$result =func_query($sql, true);
foreach ($result as $row){
	if($row['p_method']=='noncod'){
		$B2CData['online-direct_pay']['mtd']['booking']['orders']=$row['orders'];
		$B2CData['online-direct_pay']['mtd']['booking']['items']=$row['items'];
		$B2CData['online-direct_pay']['mtd']['booking']['booking']=$row['booking'];
		$B2CData['online-direct_pay']['mtd']['booking']['avgbooking']=$row['avgbooking'];
		$B2CData['online-direct_pay']['mtd']['booking']['avgitemprice']=$row['booking']/$row['items'];
	}else if($row['p_method']=='cod'){
		$B2CData['online-cod']['mtd']['booking']['orders']=$row['orders'];
		$B2CData['online-cod']['mtd']['booking']['items']=$row['items'];
		$B2CData['online-cod']['mtd']['booking']['booking']=$row['booking'];
		$B2CData['online-cod']['mtd']['booking']['avgbooking']=$row['avgbooking'];
		$B2CData['online-cod']['mtd']['booking']['avgitemprice']=$row['booking']/$row['items'];
	}
}

//BOOKING Yesterday TELEDATA 
$sql ="select count(distinct xo.orderid) as orders,sum((select sum(xod.amount) from  xcart_order_details xod where xod.orderid=xo.orderid )) as items 
,sum(xo.total+xo.shipping_cost+xo.gift_charges) as 'booking',avg((xo.total+xo.shipping_cost+xo.gift_charges)) as  'avgbooking',
avg((select avg(xod.price*xod.amount) from  xcart_order_details xod where xod.orderid=xo.orderid )) as 'avgitemprice'
 from xcart_orders xo,mk_tele_sales ts 
where xo.date>= unix_timestamp('$starttime') and xo.date < unix_timestamp('$endtime') and xo.orderid=ts.orderid 
and xo.status  in ('Q','WP','OH','C','SH','DL')   and xo.source_id=1";
$result =func_query($sql, true);
$row=$result[0];
$B2CData['tele']['yesterday']['booking']['orders']=$row['orders'];
$B2CData['tele']['yesterday']['booking']['items']=$row['items'];
$B2CData['tele']['yesterday']['booking']['booking']=$row['booking'];
$B2CData['tele']['yesterday']['booking']['avgbooking']=$row['avgbooking'];
$B2CData['tele']['yesterday']['booking']['avgitemprice']=$row['booking']/$row['items'];

//BOOKING MTD TELEDATA 
$sql ="select count(xo.orderid) as orders,sum((select sum(xod.amount) from  xcart_order_details xod where xod.orderid=xo.orderid )) as items 
,sum(xo.total+xo.shipping_cost+xo.gift_charges) as 'booking',avg((xo.total+xo.shipping_cost+xo.gift_charges)) as  'avgbooking',
avg((select avg(xod.price*xod.amount) from  xcart_order_details xod where xod.orderid=xo.orderid )) as 'avgitemprice'
 from xcart_orders xo,mk_tele_sales ts 
where DATE_FORMAT(from_unixtime(xo.date),'%m %y')  = DATE_FORMAT(DATE_SUB(CURDATE(),INTERVAL 1 DAY), '%m %y') and xo.date < unix_timestamp('$endtime')
 and xo.orderid=ts.orderid
and xo.status  in ('Q','WP','OH','C','SH','DL')   and xo.source_id=1";
$result =func_query($sql, true);
$row=$result[0];
$B2CData['tele']['mtd']['booking']['orders']=$row['orders'];
$B2CData['tele']['mtd']['booking']['items']=$row['items'];
$B2CData['tele']['mtd']['booking']['booking']=$row['booking'];
$B2CData['tele']['mtd']['booking']['avgbooking']=$row['avgbooking'];
$B2CData['tele']['mtd']['booking']['avgitemprice']=$row['booking']/$row['items'];

//***********************************************************
//JIT, Non-jit Revenue Details

$result_Jit = func_query("select count(mv1.orders) as numorders , sum(mv1.total+mv1.shipping_cost+mv1.gift_charges) as bookingVal from(select  o.orderid as orders , o.total, o.shipping_cost, o.gift_charges
from xcart_orders o, xcart_order_details od where o.orderid=od.orderid and o.date >= unix_timestamp('$starttime') and o.date < unix_timestamp('$endtime') 
and o.status in ('Q','WP','OH','C','SH','DL') and od.item_status != 'IC' and od.jit_purchase=1 group by o.orderid) mv1", true);
$row=$result_Jit[0];
$B2CData['jit']['yesterday']['orders']=$row['numorders'];
$B2CData['jit']['yesterday']['booking']=$row['bookingVal'];
$B2CData['jit']['yesterday']['avgbooking']=$row['bookingVal']/$row['numorders'];

$result_nonJit = func_query("select count(mv1.orders) as numorders, sum(mv1.booking) as bookingVal from (select o.orderid as orders, sum(od.jit_purchase) as jitSum, sum(((od.amount*od.price)/o.subtotal)*(o.total+o.shipping_cost+o.gift_charges)) as 'booking'
from xcart_orders o, xcart_order_details od where o.orderid=od.orderid and o.date >= unix_timestamp('$starttime') and o.date < unix_timestamp('$endtime') and o.status in ('Q','WP','OH','C','SH','DL') and od.item_status != 'IC' 
group by  o.orderid having jitSum = 0) mv1", true);
$row=$result_nonJit[0];
$B2CData['njit']['yesterday']['orders']=$row['numorders'];
$B2CData['njit']['yesterday']['booking']=$row['bookingVal'];
$B2CData['njit']['yesterday']['avgbooking']=$row['bookingVal']/$row['numorders'];


$result_Jit = func_query("select count(orders) as numorders , sum(mv1.total+mv1.shipping_cost+mv1.gift_charges) as bookingVal from(select  o.orderid as orders , o.total, o.shipping_cost, o.gift_charges
from xcart_orders o, xcart_order_details od where o.orderid=od.orderid and DATE_FORMAT(from_unixtime(o.date),'%m %y')  = DATE_FORMAT(DATE_SUB(CURDATE(),INTERVAL 1 DAY), '%m %y')
 and o.date < unix_timestamp('$endtime') 
and o.status in ('Q','WP','OH','C','SH','DL') and od.item_status != 'IC' and od.jit_purchase=1 group by o.orderid) mv1", true);
$row=$result_Jit[0];
$B2CData['jit']['mtd']['orders']=$row['numorders'];
$B2CData['jit']['mtd']['booking']=$row['bookingVal'];
$B2CData['jit']['mtd']['avgbooking']=$row['bookingVal']/$row['numorders'];

$result_nonJit = func_query("select count(mv1.orders) as numorders, sum(mv1.booking) as bookingVal from (select o.orderid as orders, sum(od.jit_purchase) as jitSum, sum(((od.amount*od.price)/o.subtotal)*(o.total+o.shipping_cost+o.gift_charges)) as 'booking'
from xcart_orders o, xcart_order_details od where o.orderid=od.orderid and DATE_FORMAT(from_unixtime(o.date),'%m %y')  = DATE_FORMAT(DATE_SUB(CURDATE(),INTERVAL 1 DAY), '%m %y')
 and o.date < unix_timestamp('$endtime') and od.item_status != 'IC' and o.status in ('Q','WP','OH','C','SH','DL') 
group by  o.orderid having jitSum = 0) mv1", true);
$row=$result_nonJit[0];
$B2CData['njit']['mtd']['orders']=$row['numorders'];
$B2CData['njit']['mtd']['booking']=$row['bookingVal'];
$B2CData['njit']['mtd']['avgbooking']=$row['bookingVal']/$row['numorders'];

//***********************************************************
//Potential LOST Revenue Details

$sql="select mv1.paymethod as p_method, count(distinct mv1.orderid) as orders, sum((select sum(amount) from  xcart_order_details  where orderid=mv1.orderid )) as items, sum(mv1.amount) as 'booking',avg(mv1.amount) as  'avgbooking',
avg((select avg(price*amount) from  xcart_order_details  where orderid=mv1.orderid )) as 'avgitemprice' from 
(select (case xo.payment_method when 'cod' then 'cod' else 'online' end) as paymethod, orderid, max(status) as max, min(status) as min, count(status) as tried, login, qtyInOrder, date(from_unixtime(date)) as orderDate, (total+shipping_cost+gift_charges) as amount
from xcart_orders xo where xo.date >= unix_timestamp('$starttime') and xo.date < unix_timestamp('$endtime') 
group by login, qtyInOrder, orderDate, amount) as mv1 where mv1.max in ('PP','PV') and mv1.min in ('PP','PV') and mv1.tried >=2
group by p_method";

$result = func_query($sql, true);
foreach ($result as $row){
	if($row['p_method']=='online'){
		$B2CData['online-noncod']['yesterday']['revenuelost']['orders']=$row['orders'];
		$B2CData['online-noncod']['yesterday']['revenuelost']['items']=$row['items'];
		$B2CData['online-noncod']['yesterday']['revenuelost']['booking']=$row['booking'];
		$B2CData['online-noncod']['yesterday']['revenuelost']['avgbooking']=$row['avgbooking'];
		$B2CData['online-noncod']['yesterday']['revenuelost']['avgitemprice']=$row['booking']/$row['items'];
	}
	else{
		$B2CData['online-cod']['yesterday']['revenuelost']['orders']=$row['orders'];
		$B2CData['online-cod']['yesterday']['revenuelost']['items']=$row['items'];
		$B2CData['online-cod']['yesterday']['revenuelost']['booking']=$row['booking'];
		$B2CData['online-cod']['yesterday']['revenuelost']['avgbooking']=$row['avgbooking'];
		$B2CData['online-cod']['yesterday']['revenuelost']['avgitemprice']=$row['booking']/$row['items'];
		
	}
}

$sql="select mv1.paymethod as p_method, count(distinct mv1.orderid) as orders, sum((select sum(amount) from  xcart_order_details  where orderid=mv1.orderid )) as items, sum(mv1.amount) as 'booking',avg(mv1.amount) as  'avgbooking',
avg((select avg(price*amount) from  xcart_order_details  where orderid=mv1.orderid )) as 'avgitemprice' from 
(select (case xo.payment_method when 'cod' then 'cod' else 'online' end) as paymethod, orderid, max(status) as max, min(status) as min , count(status) as tried, login, qtyInOrder, date(from_unixtime(date)) as orderDate, (total+shipping_cost+gift_charges) as amount
from xcart_orders xo where DATE_FORMAT(from_unixtime(xo.date),'%m %y')  = DATE_FORMAT(DATE_SUB(CURDATE(),INTERVAL 1 DAY), '%m %y') and xo.date < unix_timestamp('$endtime')  
group by login, qtyInOrder, orderDate, amount) as mv1 where mv1.max in ('PP','PV') and mv1.min in ('PP','PV') and mv1.tried >=2
group by p_method";

$result = func_query($sql, true);
foreach ($result as $row){
	if($row['p_method']=='online'){
		$B2CData['online-noncod']['mtd']['revenuelost']['orders']=$row['orders'];
		$B2CData['online-noncod']['mtd']['revenuelost']['items']=$row['items'];
		$B2CData['online-noncod']['mtd']['revenuelost']['booking']=$row['booking'];
		$B2CData['online-noncod']['mtd']['revenuelost']['avgbooking']=$row['avgbooking'];
		$B2CData['online-noncod']['mtd']['revenuelost']['avgitemprice']=$row['booking']/$row['items'];
	}
	else{
		$B2CData['online-cod']['mtd']['revenuelost']['orders']=$row['orders'];
		$B2CData['online-cod']['mtd']['revenuelost']['items']=$row['items'];
		$B2CData['online-cod']['mtd']['revenuelost']['booking']=$row['booking'];
		$B2CData['online-cod']['mtd']['revenuelost']['avgbooking']=$row['avgbooking'];
		$B2CData['online-cod']['mtd']['revenuelost']['avgitemprice']=$row['booking']/$row['items'];
		
	}
}

//***********************************************************

//Revenue Yesterday //COD/NONCOD

$sql="select (case xo.payment_method when 'cod' then 'cod' else 'noncod' end) as p_method,count(orderid) as orders,
sum((select sum(xod.amount) from  xcart_order_details xod where xod.orderid=xo.orderid )) as items 
,sum(xo.total+xo.shipping_cost+xo.gift_charges) as 'booking',avg((xo.total+xo.shipping_cost+xo.gift_charges)) as  'avgbooking',
avg((select avg(xod.price*xod.amount) from  xcart_order_details xod where xod.orderid=xo.orderid )) as 'avgitemprice'
from xcart_orders xo
where xo.shippeddate >= unix_timestamp('$starttime') and xo.shippeddate < unix_timestamp('$endtime') 
and xo.status  in ('C','SH','DL') and xo.source_id=1 and (xo.orgid=0 or xo.orgid is null) group by p_method";

$result =func_query($sql, true);
foreach ($result as $row){
	if($row['p_method']=='noncod'){
		$B2CData['online-direct_pay']['yesterday']['revenue']['orders']=$row['orders'];
		$B2CData['online-direct_pay']['yesterday']['revenue']['items']=$row['items'];
		$B2CData['online-direct_pay']['yesterday']['revenue']['booking']=$row['booking'];
		$B2CData['online-direct_pay']['yesterday']['revenue']['avgbooking']=$row['avgbooking'];
		$B2CData['online-direct_pay']['yesterday']['revenue']['avgitemprice']=$row['booking']/$row['items'];
	}else if($row['p_method']=='cod'){
		$B2CData['online-cod']['yesterday']['revenue']['orders']=$row['orders'];
		$B2CData['online-cod']['yesterday']['revenue']['items']=$row['items'];
		$B2CData['online-cod']['yesterday']['revenue']['booking']=$row['booking'];
		$B2CData['online-cod']['yesterday']['revenue']['avgbooking']=$row['avgbooking'];
		$B2CData['online-cod']['yesterday']['revenue']['avgitemprice']=$row['booking']/$row['items'];
	}
}

//revenue MTD //COD/NONCOD

$sql="select (case xo.payment_method when 'cod' then 'cod' else 'noncod' end) as p_method,count(orderid) as orders,
sum((select sum(xod.amount) from  xcart_order_details xod where xod.orderid=xo.orderid )) as items 
,sum(xo.total+xo.shipping_cost+xo.gift_charges) as 'booking',avg((xo.total+xo.shipping_cost+xo.gift_charges)) as  'avgbooking',
avg((select avg(xod.price*xod.amount) from  xcart_order_details xod where xod.orderid=xo.orderid )) as 'avgitemprice'
from xcart_orders xo
where DATE_FORMAT(from_unixtime(xo.shippeddate),'%m %y')  = DATE_FORMAT(DATE_SUB(CURDATE(),INTERVAL 1 DAY), '%m %y') and xo.date < unix_timestamp('$endtime') 
and xo.status  in ('C','SH','DL') and xo.source_id=1 and (xo.orgid=0 or xo.orgid is null) group by p_method";


$result =func_query($sql, true);
foreach ($result as $row){
	if($row['p_method']=='noncod'){
		$B2CData['online-direct_pay']['mtd']['revenue']['orders']=$row['orders'];
		$B2CData['online-direct_pay']['mtd']['revenue']['items']=$row['items'];
		$B2CData['online-direct_pay']['mtd']['revenue']['booking']=$row['booking'];
		$B2CData['online-direct_pay']['mtd']['revenue']['avgbooking']=$row['avgbooking'];
		$B2CData['online-direct_pay']['mtd']['revenue']['avgitemprice']=$row['booking']/$row['items'];
	}else if($row['p_method']=='cod'){
		$B2CData['online-cod']['mtd']['revenue']['orders']=$row['orders'];
		$B2CData['online-cod']['mtd']['revenue']['items']=$row['items'];
		$B2CData['online-cod']['mtd']['revenue']['booking']=$row['booking'];
		$B2CData['online-cod']['mtd']['revenue']['avgbooking']=$row['avgbooking'];
		$B2CData['online-cod']['mtd']['revenue']['avgitemprice']=$row['booking']/$row['items'];
	}
}


//revenue Yesterday TELEDATA 
$sql ="select count(xo.orderid) as orders,sum((select sum(xod.amount) from  xcart_order_details xod where xod.orderid=xo.orderid )) as items 
,sum(xo.total+xo.shipping_cost+xo.gift_charges) as 'booking',avg((xo.total+xo.shipping_cost+xo.gift_charges)) as  'avgbooking',
avg((select avg(xod.price*xod.amount) from  xcart_order_details xod where xod.orderid=xo.orderid )) as 'avgitemprice'
 from xcart_orders xo,mk_tele_sales ts 
where xo.shippeddate >= unix_timestamp('$starttime') and xo.shippeddate < unix_timestamp('$endtime') and xo.orderid=ts.orderid
and xo.status in ('C','SH','DL')   and xo.source_id=1";
$result =func_query($sql, true);
$row=$result[0];
$B2CData['tele']['yesterday']['revenue']['orders']=$row['orders'];
$B2CData['tele']['yesterday']['revenue']['items']=$row['items'];
$B2CData['tele']['yesterday']['revenue']['booking']=$row['booking'];
$B2CData['tele']['yesterday']['revenue']['avgbooking']=$row['avgbooking'];
$B2CData['tele']['yesterday']['revenue']['avgitemprice']=$row['booking']/$row['items'];


//revenue MTD TELEDATA 
$sql ="select count(xo.orderid) as orders,sum((select sum(xod.amount) from  xcart_order_details xod where xod.orderid=xo.orderid )) as items 
,sum(xo.total+xo.shipping_cost+xo.gift_charges) as 'booking',avg((xo.total+xo.shipping_cost+xo.gift_charges)) as  'avgbooking',
avg((select avg(xod.price*xod.amount) from  xcart_order_details xod where xod.orderid=xo.orderid )) as 'avgitemprice'
 from xcart_orders xo,mk_tele_sales ts 
where DATE_FORMAT(from_unixtime(xo.shippeddate),'%m %y')  = DATE_FORMAT(DATE_SUB(CURDATE(),INTERVAL 1 DAY), '%m %y') and xo.date < unix_timestamp('$endtime') 
and xo.orderid=ts.orderid 
and xo.status in ('C','SH','DL')   and xo.source_id=1";
$result =func_query($sql, true);
$row=$result[0];
$B2CData['tele']['mtd']['revenue']['orders']=$row['orders'];
$B2CData['tele']['mtd']['revenue']['items']=$row['items'];
$B2CData['tele']['mtd']['revenue']['booking']=$row['booking'];
$B2CData['tele']['mtd']['revenue']['avgbooking']=$row['avgbooking'];
$B2CData['tele']['mtd']['revenue']['avgitemprice']=$row['booking']/$row['items'];


//*************************************************
//PENDING

$sql="select (case xo.payment_method when 'cod' then 'cod' else 'noncod' end) as p_method,count(orderid) as orders,
sum((select sum(xod.amount) from  xcart_order_details xod where xod.orderid=xo.orderid )) as items 
,sum(xo.total+xo.shipping_cost+xo.gift_charges) as 'booking',avg((xo.total+xo.shipping_cost+xo.gift_charges)) as  'avgbooking',
avg((select avg(xod.price*xod.amount) from  xcart_order_details xod where xod.orderid=xo.orderid )) as 'avgitemprice'
from xcart_orders xo
where xo.status  in ('Q','WP','OH') and xo.source_id=1 and (xo.orgid=0 or xo.orgid is null) group by p_method";

$result =func_query($sql, true);
foreach ($result as $row){
	if($row['p_method']=='noncod'){
		$B2CData['online-direct_pay']['yesterday']['pending']['orders']=$row['orders'];
		$B2CData['online-direct_pay']['yesterday']['pending']['items']=$row['items'];
		$B2CData['online-direct_pay']['yesterday']['pending']['booking']=$row['booking'];
		$B2CData['online-direct_pay']['yesterday']['pending']['avgbooking']=$row['avgbooking'];
		$B2CData['online-direct_pay']['yesterday']['pending']['avgitemprice']=$row['booking']/$row['items'];
	}else if($row['p_method']=='cod'){
		$B2CData['online-cod']['yesterday']['pending']['orders']=$row['orders'];
		$B2CData['online-cod']['yesterday']['pending']['items']=$row['items'];
		$B2CData['online-cod']['yesterday']['pending']['booking']=$row['booking'];
		$B2CData['online-cod']['yesterday']['pending']['avgbooking']=$row['avgbooking'];
		$B2CData['online-cod']['yesterday']['pending']['avgitemprice']=$row['booking']/$row['items'];
	}
}

// TELEDATA 
$sql ="select count(xo.orderid) as orders,sum((select sum(xod.amount) from  xcart_order_details xod where xod.orderid=xo.orderid )) as items 
,sum(xo.total+xo.shipping_cost+xo.gift_charges) as 'booking',avg((xo.total+xo.shipping_cost+xo.gift_charges)) as  'avgbooking',
avg((select avg(xod.price*xod.amount) from  xcart_order_details xod where xod.orderid=xo.orderid )) as 'avgitemprice'
 from xcart_orders xo,mk_tele_sales ts 
where status in ('WP','OH','Q')  and xo.source_id=1 and xo.orderid=ts.orderid";
$result =func_query($sql, true);
$row=$result[0];
$B2CData['tele']['yesterday']['pending']['orders']=$row['orders'];
$B2CData['tele']['yesterday']['pending']['items']=$row['items'];
$B2CData['tele']['yesterday']['pending']['booking']=$row['booking'];
$B2CData['tele']['yesterday']['pending']['avgbooking']=$row['avgbooking'];
$B2CData['tele']['yesterday']['pending']['avgitemprice']=$row['booking']/$row['items'];


$column_H=array(0=>'#Orders',1=>'#Items',2=>'Total Booking',3=>'Avg. Bill Value',4=>'Avg. Item Value');
$column_H_orders=array(0=>'#Orders',1=>'Total Booking',2=>'Avg. Bill Value');
$column_A=array(0=>'orders',1=>'items',2=>'booking',3=>'avgbooking',4=>'avgitemprice');
$column_A_orders=array(0=>'orders',1=>'booking',2=>'avgbooking');
$column_IS_RS=array(0=>false,1=>false,2=>true,3=>true,4=>true);
$column_IS_RS_orders=array(0=>false,1=>true,2=>true);
$row_H=array(0=>'B2C - Online - Direct Pay',1=>'B2C - Online - COD');
$row_H_RevLostTable=array(0=>'B2C - Online - NonCOD',1=>'B2C - Online - COD');
$row_H_JitTable=array(0=>'JIT',1=>'Non-Jit');
$row_A=array(0=>'online-direct_pay',1=>'online-cod');
$row_A_RevLostTable=array(0=>'online-noncod',1=>'online-cod');
$row_A_JitTable=array(0=>'jit',1=>'njit');
$report.="<span><br><br><b>Booking Details: Orders Received till Date</b><br><br></span>";
$report.="<table width=\"1000\" border=\"1\" cellspacing=\"0\" cellpadding=\"5\"  bgcolor=\"white\" style=\"text-align:center;\"  >";
$report.="<tr style=\"background-color: #4f81bd; color: white;\"><td COLSPAN=1><font size=\"2\" > <b>Channel</b></font></td><td COLSPAN=5><font size=\"2\" ><b>Yesterday</b></font></td><td COLSPAN=5><font size=\"2\" ><b> Month-to-Date </b></font></td></tr>";
$report.="<tr><td>&nbsp;</td>";
for($j=0;$j<2*count($column_H);$j++){
		$report.="<td> <span style=\"font-size: 10pt; \">".$column_H[$j%count($column_H)]."</span></td>";
}
$report.="</tr>";

$total_Row;

for($i=0;$i<count($row_H);$i++){
	$report.="<tr style=\"background-color:".($i%2==0?"#d0d8e8":"#e9edf4")."\">";
	$report.="<td> <span style=\"font-size: 10pt; \"> <b>".$row_H[$i]." </b></span></td>";
	$k=0;
	for($j=0;$j<count($column_H);$j++){
		$report.="<td> <span style=\"font-size: 10pt;\">".format($B2CData[$row_A[$i]]['yesterday']['booking'][$column_A[$j]],2,$column_IS_RS[$j])."</span></td>";
		$total_Row[$k++]+=$B2CData[$row_A[$i]]['yesterday']['booking'][$column_A[$j]];
	}
	for($j=0;$j<count($column_H);$j++){
		$report.="<td> <span style=\"font-size: 10pt;\">".format($B2CData[$row_A[$i]]['mtd']['booking'][$column_A[$j]],2,$column_IS_RS[$j])."</span></td>";
		$total_Row[$k++]+=$B2CData[$row_A[$i]]['mtd']['booking'][$column_A[$j]];
	}
	$total_Row[3]=$total_Row[2]/$total_Row[0];
	$total_Row[4]=$total_Row[2]/$total_Row[1];
	$total_Row[8]=$total_Row[7]/$total_Row[5];
	$total_Row[9]=$total_Row[7]/$total_Row[6];
	$report.="</tr>";
}
$report.="<tr><td> <span style=\"font-size: 10pt; \"><b> Total </b></span></td>";
for($i=0;$i<count($total_Row);$i++){
	$report.="<td> <span style=\"font-size: 10pt; \"><b>".format($total_Row[$i],2,$column_IS_RS[$i%5])."</b></span></td>";
}
$report.="</tr>";
/*$report.="</table>";

$report.="<table width=\"1000\" border=\"1\" cellspacing=\"0\" cellpadding=\"5\"  bgcolor=\"white\" style=\"text-align:center;\"  >";*/
$report.="<tr style=\"background-color: #4f81bd; color: white;\"><td COLSPAN=1><font size=\"2\" > <b>Channel</b></font></td><td COLSPAN=5><font size=\"2\" ><b>Yesterday</b></font></td><td COLSPAN=5><font size=\"2\" ><b> Month-to-Date </b></font></td></tr>";
$report.="<tr><td>&nbsp;</td>";
for($j=0;$j<2*count($column_H);$j++){
		$report.="<td> <span style=\"font-size: 10pt; \">".$column_H[$j%count($column_H)]."</span></td>";
}
$report.="</tr>";
$report.="<tr style=\"background-color:"."#e9edf4"."\">";
	$report.="<td> <span style=\"font-size: 10pt; \"> <b>B2C - Telesales</b></span></td>";
	$k=0;
	for($j=0;$j<count($column_H);$j++){
		$report.="<td> <span style=\"font-size: 10pt;\">".format($B2CData['tele']['yesterday']['booking'][$column_A[$j]],2,$column_IS_RS[$j])."</span></td>";
	}
	for($j=0;$j<count($column_H);$j++){
		$report.="<td> <span style=\"font-size: 10pt;\">".format($B2CData['tele']['mtd']['booking'][$column_A[$j]],2,$column_IS_RS[$j])."</span></td>";
	}
	$report.="</tr>";
$report.="</table>";

/* Jit/Non-Jit table*/
$total_Row=array();
$report.="<span><br><br><b>JIT / Non-JIT Revenue Details</b><br><br></span>";
$report.="<table width=\"1000\" border=\"1\" cellspacing=\"0\" cellpadding=\"5\"  bgcolor=\"white\" style=\"text-align:center;\"  >";
$report.="<tr style=\"background-color: #4f81bd; color: white;\"><td COLSPAN=1><font size=\"2\" > <b>Channel</b></font></td><td COLSPAN=3><font size=\"2\" ><b>Yesterday</b></font></td><td COLSPAN=3><font size=\"2\" ><b> Month-to-Date </b></font></td></tr>";
$report.="<tr><td>&nbsp;</td>";
for($j=0;$j<2*count($column_H_orders);$j++){
		$report.="<td> <span style=\"font-size: 10pt; \">".$column_H_orders[$j%count($column_H_orders)]."</span></td>";
}
$report.="</tr>";
for($i=0;$i<count($row_H_JitTable);$i++){
	$report.="<tr style=\"background-color:".($i%2==0?"#d0d8e8":"#e9edf4")."\">";
	$report.="<td> <span style=\"font-size: 10pt; \"> <b>".$row_H_JitTable[$i]."</b> </span></td>";
	$k=0;
	for($j=0;$j<count($column_H_orders);$j++){
		$report.="<td> <span style=\"font-size: 10pt; \">".format($B2CData[$row_A_JitTable[$i]]['yesterday'][$column_A_orders[$j]],2,$column_IS_RS_orders[$j])."</span></td>";
		$total_Row[$k++]+=$B2CData[$row_A_JitTable[$i]]['yesterday'][$column_A_orders[$j]];
	}
	for($j=0;$j<count($column_H_orders);$j++){
		$report.="<td> <span style=\"font-size: 10pt;\">".format($B2CData[$row_A_JitTable[$i]]['mtd'][$column_A_orders[$j]],2,$column_IS_RS_orders[$j])."</span></td>";
		$total_Row[$k++]+=$B2CData[$row_A_JitTable[$i]]['mtd'][$column_A_orders[$j]];
	}
	$total_Row[2]=$total_Row[1]/$total_Row[0];
	$total_Row[5]=$total_Row[4]/$total_Row[3];
	$report.="</tr>";
}
$report.="<tr><td> <span style=\"font-size: 10pt; \"> <b>Total</b> </span></td>";
for($i=0;$i<count($total_Row);$i++){
	$report.="<td> <span style=\"font-size: 10pt; \"><b>".format($total_Row[$i],2,$column_IS_RS_orders[$i%5])."</b></span></td>";
}
$report.="</tr>";
$report.="</table>";

/*Potential Lost Revenue table*/
$total_Row=array();
$report.="<span><br><br><b>Potential LOST Revenue Details: Orders Shipped till Date</b><br><br></span>";
$report.="<table width=\"1000\" border=\"1\" cellspacing=\"0\" cellpadding=\"5\"  bgcolor=\"white\" style=\"text-align:center;\"  >";
$report.="<tr style=\"background-color: #4f81bd; color: white;\"><td COLSPAN=1><font size=\"2\" > <b>Channel</b></font></td><td COLSPAN=5><font size=\"2\" ><b>Yesterday</b></font></td><td COLSPAN=5><font size=\"2\" ><b> Month-to-Date </b></font></td></tr>";
$report.="<tr><td>&nbsp;</td>";
for($j=0;$j<2*count($column_H);$j++){
		$report.="<td> <span style=\"font-size: 10pt; \">".$column_H[$j%count($column_H)]."</span></td>";
}
$report.="</tr>";
for($i=0;$i<count($row_H_RevLostTable);$i++){
	$report.="<tr style=\"background-color:".($i%2==0?"#d0d8e8":"#e9edf4")."\">";
	$report.="<td> <span style=\"font-size: 10pt; \"> <b>".$row_H_RevLostTable[$i]."</b> </span></td>";
	$k=0;
	for($j=0;$j<count($column_H);$j++){
		$report.="<td> <span style=\"font-size: 10pt; \">".format($B2CData[$row_A_RevLostTable[$i]]['yesterday']['revenuelost'][$column_A[$j]],2,$column_IS_RS[$j])."</span></td>";
		$total_Row[$k++]+=$B2CData[$row_A_RevLostTable[$i]]['yesterday']['revenuelost'][$column_A[$j]];
	}
	for($j=0;$j<count($column_H);$j++){
		$report.="<td> <span style=\"font-size: 10pt;\">".format($B2CData[$row_A_RevLostTable[$i]]['mtd']['revenuelost'][$column_A[$j]],2,$column_IS_RS[$j])."</span></td>";
		$total_Row[$k++]+=$B2CData[$row_A_RevLostTable[$i]]['mtd']['revenuelost'][$column_A[$j]];
	}
	$total_Row[3]=$total_Row[2]/$total_Row[0];
	$total_Row[4]=$total_Row[2]/$total_Row[1];
	$total_Row[8]=$total_Row[7]/$total_Row[5];
	$total_Row[9]=$total_Row[7]/$total_Row[6];
	$report.="</tr>";
}
$report.="<tr><td> <span style=\"font-size: 10pt; \"> <b>Total</b> </span></td>";
for($i=0;$i<count($total_Row);$i++){
	$report.="<td> <span style=\"font-size: 10pt; \"><b>".format($total_Row[$i],2,$column_IS_RS[$i%5])."</b></span></td>";
}
$report.="</tr>";
$report.="</table>";

$total_Row=array();
$report.="<span><br><br><b>Revenue Details: Orders Shipped till Date</b><br><br></span>";
$report.="<table width=\"1000\" border=\"1\" cellspacing=\"0\" cellpadding=\"5\"  bgcolor=\"white\" style=\"text-align:center;\"  >";
$report.="<tr style=\"background-color: #4f81bd; color: white;\"><td COLSPAN=1><font size=\"2\" > <b>Channel</b></font></td><td COLSPAN=5><font size=\"2\" ><b>Yesterday</b></font></td><td COLSPAN=5><font size=\"2\" ><b> Month-to-Date </b></font></td></tr>";
$report.="<tr><td>&nbsp;</td>";
for($j=0;$j<2*count($column_H);$j++){
		$report.="<td> <span style=\"font-size: 10pt; \">".$column_H[$j%count($column_H)]."</span></td>";
}
$report.="</tr>";
for($i=0;$i<count($row_H);$i++){
	$report.="<tr style=\"background-color:".($i%2==0?"#d0d8e8":"#e9edf4")."\">";
	$report.="<td> <span style=\"font-size: 10pt; \"> <b>".$row_H[$i]."</b> </span></td>";
	$k=0;
	for($j=0;$j<count($column_H);$j++){
		$report.="<td> <span style=\"font-size: 10pt; \">".format($B2CData[$row_A[$i]]['yesterday']['revenue'][$column_A[$j]],2,$column_IS_RS[$j])."</span></td>";
		$total_Row[$k++]+=$B2CData[$row_A[$i]]['yesterday']['revenue'][$column_A[$j]];
	}
	for($j=0;$j<count($column_H);$j++){
		$report.="<td> <span style=\"font-size: 10pt;\">".format($B2CData[$row_A[$i]]['mtd']['revenue'][$column_A[$j]],2,$column_IS_RS[$j])."</span></td>";
		$total_Row[$k++]+=$B2CData[$row_A[$i]]['mtd']['revenue'][$column_A[$j]];
	}
	$total_Row[3]=$total_Row[2]/$total_Row[0];
	$total_Row[4]=$total_Row[2]/$total_Row[1];
	$total_Row[8]=$total_Row[7]/$total_Row[5];
	$total_Row[9]=$total_Row[7]/$total_Row[6];
	$report.="</tr>";
}
$report.="<tr><td> <span style=\"font-size: 10pt; \"> <b>Total</b> </span></td>";
for($i=0;$i<count($total_Row);$i++){
	$report.="<td> <span style=\"font-size: 10pt; \"><b>".format($total_Row[$i],2,$column_IS_RS[$i%5])."</b></span></td>";
}
$report.="</tr>";
/*$report.="</table>";

$report.="<table width=\"1000\" border=\"1\" cellspacing=\"0\" cellpadding=\"5\"  bgcolor=\"white\" style=\"text-align:center;\"  >";
*/$report.="<tr style=\"background-color: #4f81bd; color: white;\"><td COLSPAN=1><font size=\"2\" > <b>Channel</b></font></td><td COLSPAN=5><font size=\"2\" ><b>Yesterday</b></font></td><td COLSPAN=5><font size=\"2\" ><b> Month-to-Date </b></font></td></tr>";
$report.="<tr><td>&nbsp;</td>";
for($j=0;$j<2*count($column_H);$j++){
		$report.="<td> <span style=\"font-size: 10pt; \">".$column_H[$j%count($column_H)]."</span></td>";
}
$report.="</tr>";
$report.="<tr style=\"background-color:"."#e9edf4"."\">";
	$report.="<td> <span style=\"font-size: 10pt; \"> <b>B2C - Telesales</b></span></td>";
	$k=0;
	for($j=0;$j<count($column_H);$j++){
		$report.="<td> <span style=\"font-size: 10pt;\">".format($B2CData['tele']['yesterday']['revenue'][$column_A[$j]],2,$column_IS_RS[$j])."</span></td>";
	}
	for($j=0;$j<count($column_H);$j++){
		$report.="<td> <span style=\"font-size: 10pt;\">".format($B2CData['tele']['mtd']['revenue'][$column_A[$j]],2,$column_IS_RS[$j])."</span></td>";
	}
	$report.="</tr>";
$report.="</table>";

$total_Row=array();

$report.="<span><br><br><b>Total Pending Fulfillment: Orders Received till Date</b><br><br></span>";
$report.="<table width=\"500\" border=\"1\" cellspacing=\"0\" cellpadding=\"5\"  bgcolor=\"white\" style=\"text-align:center;\"  >";
$report.="<tr style=\"background-color: #4f81bd; color: white;\"><td COLSPAN=1><font size=\"2\" > <b>Channel</b></font></td><td COLSPAN=5><font size=\"2\" ><b>Pending Fulfillment</b></font></td></tr>";
$report.="<tr><td>&nbsp;</td>";
for($j=0;$j<count($column_H);$j++){
		$report.="<td> <span style=\"font-size: 10pt; \">".$column_H[$j]."</span></td>";
}
$report.="</tr>";
for($i=0;$i<count($row_H);$i++){
	$report.="<tr style=\"background-color:".($i%2==0?"#d0d8e8":"#e9edf4")."\">";
	$k=0;
	$report.="<td> <span style=\"font-size: 10pt; \"><b> ".$row_H[$i]." </b></span></td>";
	for($j=0;$j<count($column_H);$j++){
		$report.="<td> <span style=\"font-size: 10pt;\">".format($B2CData[$row_A[$i]]['yesterday']['pending'][$column_A[$j]],2,$column_IS_RS[$j])."</span></td>";
		$total_Row[$k++]+=$B2CData[$row_A[$i]]['yesterday']['pending'][$column_A[$j]];
	}
	$total_Row[3]=$total_Row[2]/$total_Row[0];
	$total_Row[4]=$total_Row[2]/$total_Row[1];
	$report.="</tr>";
}

$report.="<tr><td> <span style=\"font-size: 10pt; \"><b> Total</b> </span></td>";
for($i=0;$i<count($total_Row);$i++){
	$report.="<td> <span style=\"font-size: 10pt; \"><b>".format($total_Row[$i],2,$column_IS_RS[$i%5])."</b></span></td>";
}
$report.="</tr>";


/*$report.="</table>";


$report.="<table width=\"500\" border=\"1\" cellspacing=\"0\" cellpadding=\"5\"  bgcolor=\"white\" style=\"text-align:center;\"  >";*/
$report.="<tr style=\"background-color: #4f81bd; color: white;\"><td COLSPAN=1><font size=\"2\" > <b>Channel</b></font></td><td COLSPAN=5><font size=\"2\" ><b>Pending Fulfillment</b></font></td></tr>";
$report.="<tr><td>&nbsp;</td>";
for($j=0;$j<count($column_H);$j++){
		$report.="<td> <span style=\"font-size: 10pt; \">".$column_H[$j]."</span></td>";
}
$report.="</tr>";
$report.="<tr style=\"background-color:"."#e9edf4"."\">";
	$report.="<td> <span style=\"font-size: 10pt; \"> <b>B2C - Telesales</b></span></td>";
	$k=0;
	for($j=0;$j<count($column_H);$j++){
		$report.="<td> <span style=\"font-size: 10pt;\">".format($B2CData['tele']['yesterday']['pending'][$column_A[$j]],2,$column_IS_RS[$j])."</span></td>";
	}
	$report.="</tr>";
$report.="</table>";

$report.="</div>
</div>
</div>
</div>
</div>";
if(isset($_GET['view_report'])&&$_GET['view_report']=='true'){
	echo $report;
	$yesterday = date('d/m/y', mktime(0, 0, 0, date("m") , date("d")-1 , date("Y")));
	$subject ="B2C Online Report :".$yesterday;
	$m = new MAIL();
	$m->From('admin@myntra.com', 'Myntra Admin');
	$m->AddTo('engg_bi@myntra.com');
	$m->Subject($subject);
	$m->Html($report);
	$mailresult=$m->Send('client') ? 'Mail sent !' : 'Error';
}else{
	$yesterday = date('d/m/y', mktime(0, 0, 0, date("m") , date("d")-1 , date("Y")));
	$subject ="B2C Online Report :".$yesterday;
	$m = new MAIL();
	$m->From('admin@myntra.com', 'Myntra Admin');
//  $m->AddTo('rpt_b2conline@myntra.com');
	//$m->AddTo('myntra.next.amazon@gmail.com');
	$m->AddTo('myntra.next.amazon@myntra.com');
	$m->Subject($subject);
	$m->Html($report);
	$mailresult=$m->Send('client') ? 'Mail sent !' : 'Error';
}



?>
