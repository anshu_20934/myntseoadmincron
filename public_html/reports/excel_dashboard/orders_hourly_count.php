<?php
require "../../auth.php";

global $sqllog;
$sql="select test1.hourTag,test1.orders,test1.items,test1.OrderValue,test2.jitorders,test2.jitOrderValue from 
(select  hour(time(from_unixtime(date))) as hourTag, 
        count(distinct orderid) as orders, 
        sum(qtyInOrder) as items, 
        sum(total+shipping_cost+gift_charges) as OrderValue 
from xcart_orders 
where status in (" .$_GET['statusStr'] .") 
and date between unix_timestamp('" .$_GET['from'] ."') and (unix_timestamp('" .$_GET['to'] ."')+(3600*24)) 
group by hourTag) as test1
left join 
(select  hour(time(from_unixtime(xo.date))) as hourTag, count(distinct xo.orderid) as jitorders, sum(xo.total+xo.shipping_cost+xo.gift_charges) as jitOrderValue 
from xcart_orders xo, xcart_order_details xod 
where   xo.orderid=xod.orderid and 
        xo.status in (" .$_GET['statusStr'] .") and
        xod.jit_purchase=1 and
        xo.date between unix_timestamp('" .$_GET['from'] ."') and (unix_timestamp('" .$_GET['to'] ."')+(3600*24))
group by hourTag) as test2
on test1.hourTag=test2.hourTag";
$sqllog->info($sql);
$resultset=func_query($sql, true);
echo "<table>";
foreach ($resultset as $row){
	echo "<tr><td>" .$row['hourTag'] ."</td><td>" .$row['orders'] ."</td><td>" .$row['items'] ."</td><td>" .$row['OrderValue'] ."</td><td>" .$row['jitorders'] ."</td><td>" .$row['jitOrderValue'] ."</td></tr>";
}
echo "</table>";