<?php

require "../../auth.php";

$current_date = isset($_GET['today'])?$_GET['today']:NULL;

//Validate the date input. If the date input is screwed, all the other operations would be.
if(!empty($current_date)) {
	$current_date = trim($current_date);
	if(!preg_match("/^[1-2][0-9]{3}\-[0-1][0-9]-[0-3][0-9]$/",$current_date)) {
		echo "Specify a valid date without any time. Eg., 2011-10-30";
		exit;
	}
}
else {
	echo "Specify date to pull the report for";
        exit;
} 

//Compute the date for the previous day and the date for the same day of last week
$today = new DateTime($current_date);

$yesterday = new DateTime();
$yesterday->setTimestamp($today->getTimestamp()-60*60*24);
$yesterdaydate = $yesterday->format('Y-m-d');

$lastweek = new DateTime();
$lastweek->setTimestamp($today->getTimestamp()-60*60*24*7);
$lastweekdate = $lastweek->format('Y-m-d');

$payment_options = array("cod","netbanking","creditcards","debitcards");
$data_points_conditions = array(
                        "attempts"=>"",
                        "succeeded" => " and is_complete=1"
                ); 
$time_lines = array(
                        "today" => array("start"=>mysql_escape_string($current_date),"end"=>mysql_escape_string($current_date." 23:59:59")),
                        "yesterday" => array("start"=>$yesterdaydate,"end"=>$current_date),
                        "lastweek" => array("start"=>$lastweekdate,"end"=>$current_date)
                );

$data_by_paymentmode = array();
$data_point_names = array();
foreach($payment_options as $payment_mode) {
	$data_by_paymentmode[$payment_mode] = array();
}

function fill_results($timeline, $conditions, $data_point_name) {
	global $data_by_paymentmode, $data_point_names;
	$data_point_names[$data_point_name] = 1;
	$results = func_query("select payment_option, count(*) as attempts from mk_payments_log where ".$timeline.$conditions." group by payment_option");
	for($i=0;$i<count($results);$i++) {
		$row = $results[$i];
		$data_by_paymentmode[$row['payment_option']][$data_point_name] = $row['attempts'];
	}
}

foreach($time_lines as $tkey=>$tvalue) {
	$start_time = $tvalue['start'];
	$end_time = $tvalue['end'];
	$time_condition = "time_insert between unix_timestamp('$start_time') and unix_timestamp('$end_time')";
	foreach($data_points_conditions as $ckey=>$cvalue) {
		fill_results($time_condition, $cvalue, $tkey." ".$ckey);
	}
}

$data_points = array_keys($data_point_names);
echo "<table><th>Payment mode</th>";
foreach($data_points as $name) {
	echo "<th>$name</th>";
}


foreach($payment_options as $payment_mode) {
	echo "<tr>";
	echo "<td>$payment_mode</td>";
	foreach($data_points as $name) {
		echo "<td>".$data_by_paymentmode[$payment_mode][$name]."</td>";
	}
	echo "</tr>";
}
echo "</table>";

?>
