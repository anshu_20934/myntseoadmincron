<?php

class HTMLReportRenderer implements  IReportRenderer{
	public function render($reportModel,$rendererConfig){
		$reportresult;
		global $smarty,$report,$action,$show_filters,$caller,$format,$emailsToList,$report_title,$report_list;
		if($show_filters=='yes' && count($reportModel->getFilters())>0){
			$smarty->assign("decimals",$rendererConfig->getDecimalRound());
			$smarty->assign("reportresult",$reportresult);
			$smarty->assign("filters",$reportModel->getFilters());
			$smarty->assign("report",$report);
			$smarty->assign("action",$action);
			$smarty->assign("format",$format);
			$smarty->assign("emailsToList",$emailsToList);
			$smarty->assign("report_list",$report_list);
			$smarty->assign("reportname",$reportModel->getReportName());

			func_display("HTMLReportRenderer.tpl",$smarty);
			exit;
		}
		foreach ($reportModel->getTableGroups() as $group) {
			if($rendererConfig->isRowPadding()){
				$maxRow=0;
				foreach($group->getTables() as $table){
					if($maxRow<$table->getColumnsCount())
					$maxRow=$table->getColumnsCount();
				}
			}
			foreach($group->getTables() as $tindex=> $table){
				$styleMat=$table->getStyleMatrix();
				$tname = $table->getName();
				if($tname == null){
					$tname = $tindex;
				}
				$reportresult[$group->getName()][$tname]['superheaders']=$table->getSuperHeaders();
				$reportresult[$group->getName()][$tname]['headers']=$table->getHeaders();
				foreach($table->getAllRows() as $rindex=> $row){
					if($rendererConfig->isRowPadding()&& count($row)<=$maxRow){
						for($cc=count($row);$cc<=$maxRow;$cc++){
							$row[$cc]=$rendererConfig->getPad();
						}
					}
					$newRow=array();
					foreach ($row as $cindex=> $cell){
						if(!is_string($cell)){
							$newRow[$cindex]['data']=round($cell,$rendererConfig->getDecimalRound());
							if(isset($styleMat[$rindex][$cindex])){
								if($styleMat[$rindex][$cindex][Table::$FORMAT] == "currency"){	
									$newRow[$cindex]['data']=money_format('%!.0i',$newRow[$cindex]['data']);
								}
							}
						}
						else{
							$newRow[$cindex]['data']=$cell;
						}
						$newRow[$cindex]['style']=$styleMat[$rindex][$cindex];
					}
					$reportresult[$group->getName()][$tname]['rows'][$rindex]=$newRow;
				} 
				$footer	=$table->getFooter();
				foreach ($footer as $findex=>$fval){
					if(!is_string($fval)&&$fval!=null){
						$footer[$findex]=round($fval,$rendererConfig->getDecimalRound());
						if(isset($styleMat[$rindex+1])){
							if($styleMat[$rindex+1][Table::$FORMAT] == "currency"){
								$footer[$findex]=money_format('%!.0i',$footer[$findex]);
							}
						}
					}
				}
				if($table->isFooterVisible())
					$reportresult[$group->getName()][$tname]['footer']=$footer;
			}
		}
		$smarty->assign("decimals",$rendererConfig->getDecimalRound());
		$smarty->assign("report",$report);
		$smarty->assign("action",$action);
		$smarty->assign("format",$format);
		$smarty->assign("emailsToList",$emailsToList);
		$smarty->assign("report_list",$report_list);
		$smarty->assign("reportresult",$reportresult);
		$smarty->assign("filters",$reportModel->getFilters());
		$smarty->assign("reportname",$reportModel->getReportName());
		$smarty->assign("reportnotes",$reportModel->getReportNotes());
		func_display("HTMLReportRenderer.tpl",$smarty);
	}
}
?>