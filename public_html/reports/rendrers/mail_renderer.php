<?php

if(!defined('DISPLAY_XPM4_ERRORS'))
define('DISPLAY_XPM4_ERRORS', true); // display XPM4 errors

// path to 'MAIL.php' file from XPM4 package
include_once REP_DIR.'/models/models.php';
require_once $xcart_dir.'/XPM/MAIL.php';





class MailReportRenderer implements  IReportRenderer{

	public function render($reportModel,$rendererConfig){
		$reportresult;
		global $smarty,$report,$action,$show_filters,$caller,$format,$xcart_dir;
		$tempname = $xcart_dir."/var/tmp/REP".time();
		if($rendererConfig->getFormat() == 'csv'){
			$groups = $reportModel->getTableGroups();
			$tables = $groups[0]->getTables();
			$writer = new NumericArrayCSVWriter($tempname,",","\n");
			$writer->setHeaderColumns($tables[0]->getHeaders());
			$writer->writeHeader();
			$rows =Array();
			foreach($tables[0]->getAllRows() as $tbl_rows){
				$rows[] = $tbl_rows;
			}
			$writer->writeRows($rows);
			$filename = str_replace(" ","_", $reportModel->getReportName()).".csv.gz";
			$contents="\n Please see the attached excel file for details. \n";
		}	
		else{
			foreach ($reportModel->getTableGroups() as $group) {
	
				foreach($group->getTables() as $tindex=> $table){
					$styleMat=$table->getStyleMatrix();
					$tname = $table->getName();
					if($tname == null){
						$tname = $tindex;
					}
					$reportresult[$group->getName()][$tname]['superheaders']=$table->getSuperHeaders();
					$reportresult[$group->getName()][$tname]['headers']=$table->getHeaders();
					foreach($table->getAllRows() as $rindex=> $row){
						$newrow=array();
						foreach ($row as $cindex=> $cell){
							if(!is_string($cell)){ //if number
								$newrow[$cindex]['data']=round($cell,$rendererConfig->getDecimalRound());
								if(isset($styleMat[$rindex][$cindex])){
									if($styleMat[$rindex][$cindex][Table::$FORMAT] == "currency"){
										$newrow[$cindex]['data']=money_format('%!.0i',$newrow[$cindex]['data']);
									}
								}
							}
							else{
								$newrow[$cindex]['data']=$cell;
							}
							$newrow[$cindex]['style']=$styleMat[$rindex][$cindex];
						}
						$reportresult[$group->getName()][$tname]['rows'][$rindex]=$newrow;
						unset($newrow);
					}
					$footer	=$table->getFooter();
					foreach ($footer as $findex=>$fval){
						if(!is_string($fval)&&$fval!=null){
							$footer[$findex]=round($fval,$rendererConfig->getDecimalRound());
							if(isset($styleMat[$rindex+1])){
								if($styleMat[$rindex+1][Table::$FORMAT] == "currency"){
									$footer[$findex]=money_format('%!.0i',$footer[$findex]);
								}
							}
						}
					}
					if($table->isFooterVisible())
					$reportresult[$group->getName()][$tname]['footer']=$footer;
				}
			}
			$smarty->assign("report",$report);
			$smarty->assign("action",$action);
			$smarty->assign("format",$format);
			$smarty->assign("reportname",$reportModel->getReportName());
			$smarty->assign("reportnotes",$reportModel->getReportNotes());
			$smarty->assign("reportresult",$reportresult);
			$smarty->assign("filters",$reportModel->getFilters());
			$smarty->assign("reportname",$reportModel->getReportName());
			if($rendererConfig->getInline() == 'true'){
				$contents=$smarty->fetch("MailReportRenderer.tpl",$smarty);
			}
			else{
				$contents="\n Please see the attached excel file for details. \n";
			}

			$excelRenderer= new ExcelReportRenderer();
			$excelRendererConf=new ExcelRendererConfig();
			$excelRendererConf->setOutputFormat($rendererConfig->getAttachmentFormat());
			$excelRendererConf->setRenderToFile(true);
			$excelRendererConf->setFileName($tempname);
			$excelRendererConf->setDecimalRound($rendererConfig->getDecimalRound());
			$excelRenderer->render($reportModel,$excelRendererConf);
			$filename = str_replace(" ","_", $reportModel->getReportName()).".". $excelRendererConf->getFileExt().".gz";
			
		}	
		$m = new MAIL;
		// set from address and name
		$from=$rendererConfig->getFrom();
		$m->From($from['id'], $from['name']);
			
		$to=$rendererConfig->getTo();
		$hasmailid=false;
		foreach($to as $value){
			// add to address and name
			$m->AddTo($value['id'], $value['name']);
			$hasmailid=true;
		}
			
		$cc=$rendererConfig->getCC();
		foreach($cc as $value){
			// add to address and name
			$m->addcc($value['id'], $value['name']);
			$hasmailid=true;
		}
		// set subject
		$m->Subject($reportModel->getReportName());
		$m->Html($contents);
		$xpmFunc=new FUNC5();
		$xpmMime= new MIME5();
		//$m->Attach(file_get_contents($tempname),  $xpmFunc->mime_type($filename), $filename, null, null, 'inline',$xpmMime->unique());
		$m->Attach(gzencode(file_get_contents($tempname), 9),  $xpmFunc->mime_type($filename), $filename, null, null, 'inline',$xpmMime->unique());
		$mailresult=$m->Send('client') ? 'Mail sent !' : 'Error';
		if($hasmailid==false){
			$mailmsg="e-mail could not be sent as e-mail address not defined for this report ";
		}
		else if($mailresult=='Error'){
			$mailmsg= "Report::".$reportModel->getReportName()."\rE-mail could not be sent due to some bad configuration ";
		}
		else{
			$mailmsg="Report::".$reportModel->getReportName()."\rReport Mailed Succesfully!!!!! ";
		}
		echo $mailmsg;
		@unlink($tempname);
		/*$smarty->assign("mailmsg",$mailmsg);
		 $smarty->assign("mailsent","Y");
		 func_display("view_report.tpl",$smarty);*/
	}
}

?>