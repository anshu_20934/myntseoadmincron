<?php
include_once $xcart_dir.'/PHPExcel/PHPExcel.php';
PHPExcel_Settings::setLocale('en_us');
class ExcelReportRenderer implements  IReportRenderer{
	private $XSL_HEADERS=array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","AA","AB","AC","AD","AE","AF","AG","AH","AI","AJ","AK","AL","AM","AN","AO","AP","AQ","AR","AS","AT","AU","AV","AW","AX","AY","AZ");
	public function render($reportModel,$rendererConfig){
		$objPHPExcel = new PHPExcel();
		$firstTime=true;
		$groupIndex=0;
		$row_editing=1;
		$objPHPExcel->getProperties()->setCreator($rendererConfig->getCreator());
		$objPHPExcel->getProperties()->setTitle($reportModel->getReportName());
		$multiple_sheets=!($rendererConfig->getOutputFormat()==ExcelRendererConfig::$FORMAT_CSV);
		foreach ($reportModel->getTableGroups() as $group) {
			if($groupIndex!=0 && $multiple_sheets){
				$objPHPExcel->addSheet(new PHPExcel_Worksheet($objPHPExcel));
				$row_editing=1;
			}
			$sheet = $objPHPExcel->setActiveSheetIndex($groupIndex);
			$sheet->setTitle($group->getName());
			$sheet->getDefaultColumnDimension()->setAutoSize(true);
			foreach($group->getTables() as $tindex=> $table){
				$headers=$table->getHeaders();
				$styleMat=$table->getStyleMatrix();
				$superHeadersSpanArray=$table->getSuperHeaders();
				$j=0;
				foreach($superHeadersSpanArray as $hvalue=>$span){
					$sheet->setCellValue($this->XSL_HEADERS[$j].$row_editing,$hvalue);
					$sheet->getStyle($this->XSL_HEADERS[$j].$row_editing)->getFont()->setBold(true);
					$sheet->mergeCells($this->XSL_HEADERS[$j].$row_editing.":".$this->XSL_HEADERS[$j+$span-1].$row_editing);
					$j+=$span;
				}
				$row_editing++;
				$j=0;
				foreach ($headers as $hval){
					$sheet->setCellValue($this->XSL_HEADERS[$j].$row_editing,$hval);
					$sheet->getStyle($this->XSL_HEADERS[$j].$row_editing)->getFont()->setBold(true);
					$j++;
				}
				$row_editing++;
				$spanAdjustingcounter=0;
				$spanAdjustingFlag=0;
				foreach($table->getAllRows() as $rindex=> $row){

					foreach ($row as $cindex=> $cell){
						if(!is_string($cell)&& $cell!=null){
							$row[$cindex]=round($cell,$rendererConfig->getDecimalRound());
						}
					}
					if($spanAdjustingcounter != 0)
						$j=1;
					else{
						$j=0;
						$spanAdjustingFlag = 0;
					}
					foreach ($row as $rval){
						if(empty($rval))
							$rval=0;
						$sheet->setCellValue($this->XSL_HEADERS[$j].$row_editing,$rval);
						$sheet->getStyle($this->XSL_HEADERS[$j].$row_editing)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
						if(isset($styleMat[$rindex][$j])){
							if(isset($styleMat[$rindex][$j][Table::$ROWSPAN])){
								$row_onspan=$row_editing + $styleMat[$rindex][$j][Table::$ROWSPAN]-1;
								$sheet->mergeCells($this->XSL_HEADERS[$j].$row_editing.":".$this->XSL_HEADERS[$j].$row_onspan);
								$spanAdjustingcounter=$styleMat[$rindex][$j][Table::$ROWSPAN]; //to adjust for the (cell)index of rows containing rowspanned cell.
								$spanAdjustingFlag = 1;
							}
							if(isset($styleMat[$rindex][$j][Table::$STYLE_EXCEL_FORMULA])){	
								$sheet->setCellValueExplicit($this->XSL_HEADERS[$j].$row_editing,$styleMat[$rindex][$j][Table::$STYLE_EXCEL_FORMULA],PHPExcel_Cell_DataType::TYPE_FORMULA);
							}
							if(isset($styleMat[$rindex][$j][Table::$STYLE_EXCEL_CONDITIONAL_FORMATTING])){
								foreach ($styleMat[$rindex][$j][Table::$STYLE_EXCEL_CONDITIONAL_FORMATTING] as $rule=>$fromatting){
									foreach ($fromatting as $conditions){
										$objConditional = new PHPExcel_Style_Conditional();
										$objConditional->setConditionType(PHPExcel_Style_Conditional::CONDITION_CELLIS);
										if($rule==Table::$EQUALS){
											$objConditional->setOperatorType(PHPExcel_Style_Conditional::OPERATOR_EQUAL);
											$objConditional->addCondition("\"".$conditions[Table::$VALUE]."\"");
										}else if($rule==Table::$GREATER_THAN){
											$objConditional->setOperatorType(PHPExcel_Style_Conditional::OPERATOR_GREATERTHAN);
											$objConditional->addCondition($conditions[Table::$VALUE]);
										}else if($rule==Table::$LESS_THAN){
											$objConditional->setOperatorType(PHPExcel_Style_Conditional::OPERATOR_LESSTHAN);
											$objConditional->addCondition($conditions[Table::$VALUE]);
										}else{
											$objConditional->setOperatorType(PHPExcel_Style_Conditional::OPERATOR_BETWEEN);
											$objConditional->addCondition($conditions[Table::$VALUE][0]);
											$objConditional->addCondition($conditions[Table::$VALUE][1]);
										}
										
										$objConditional->getStyle()->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
										$objConditional->getStyle()->getFill()->getStartColor()->setARGB("FF".$conditions[Table::$STYLE_BG_COLOR]);
										$objConditional->getStyle()->getFill()->getEndColor()->setARGB("FF".$conditions[Table::$STYLE_BG_COLOR]);
										$conditionalStyles =$sheet->getStyle($this->XSL_HEADERS[$j].$row_editing)->getConditionalStyles();
										array_push($conditionalStyles, $objConditional);
										$sheet->getStyle($this->XSL_HEADERS[$j].$row_editing)->setConditionalStyles($conditionalStyles);
									}
										
								}
							}
						}
						$j++;
					}
					$row_editing++;
					if($spanAdjustingFlag == 1){
						$spanAdjustingcounter--;
					}
				}
				$footer	=$table->getFooter();
				foreach ($footer as $findex=>$fval){
					if(!is_string($fval)&&$fval!=null){
						$footer[$findex]=round($fval,$rendererConfig->getDecimalRound());
					}
				}
				$j=0;
				if($table->isFooterVisible()){
					foreach ($footer as $ffval){
						$sheet->setCellValue($this->XSL_HEADERS[$j].$row_editing,$ffval);
						$sheet->getStyle($this->XSL_HEADERS[$j].$row_editing)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
						$sheet->getStyle($this->XSL_HEADERS[$j].$row_editing)->getFont()->setBold(true);
						$j++;
					}
				}
				$row_editing+=3;
				foreach ($this->XSL_HEADERS as $cname){
					$sheet->getColumnDimension($cname)->setAutoSize(true);
				}
				$sheet->setShowGridlines($rendererConfig->isShowGrid());
				if($rendererConfig->getOutputFormat()==ExcelRendererConfig::$FORMAT_PDF){
					$columns_nums=count($headers);
					if($columns_nums<=7){
						$sheet->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
					}else if(count($headers)<=10){
						$sheet->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A3);
					}else{
						$sheet->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A2_PAPER);
					}
				}
			}
			if($multiple_sheets)
				$groupIndex++;
		}
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,$rendererConfig->getOutputFormat() );
		if(!($rendererConfig->getOutputFormat()==ExcelRendererConfig::$FORMAT_CSV)
		&&!($rendererConfig->getOutputFormat()==ExcelRendererConfig::$FORMAT_EXCEL)
		&&!($rendererConfig->getOutputFormat()==ExcelRendererConfig::$FORMAT_EXCEL7))
		$objWriter->writeAllSheets();
		if($rendererConfig->getOutputFormat()==ExcelRendererConfig::$FORMAT_EXCEL7){
			$objWriter->setOffice2003Compatibility(true);
		}
		if($rendererConfig->isRenderToFile())
		$objWriter->save($rendererConfig->getFileName());
		else{
			$filename = str_replace(" ","_", $reportModel->getReportName()).".". $rendererConfig->getFileExt();

			if($rendererConfig->getOutputFormat()==ExcelRendererConfig::$FORMAT_PDF){
				header('Content-Type: application/pdf');
				header("Content-Disposition: attachment;filename=$filename");
				header('Cache-Control: max-age=0');

			}else if($rendererConfig->getOutputFormat()==ExcelRendererConfig::$FORMAT_EXCEL7){
				header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
				header("Content-Disposition: attachment;filename=$filename");
				header('Cache-Control: max-age=0');
			}else if($rendererConfig->getOutputFormat()==ExcelRendererConfig::$FORMAT_EXCEL||$rendererConfig->getOutputFormat()==ExcelRendererConfig::$FORMAT_CSV){
				header('Content-Type: application/vnd.ms-excel');
				header('Content-Disposition: attachment;filename="'.$filename.'"');
				header('Cache-Control: max-age=0');
			}
			$objWriter->save("php://output");
			exit;
		}
	}
}