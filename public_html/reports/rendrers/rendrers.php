<?php
class RendererConfig{
	private $reportName="unnamed";
	private $roundVal=2;
	public function getReportName(){
		return $this->reportName;
	}
	public function setReportName($reportName){
		$this->reportName=$reportName;
	}

	public function setDecimalRound($val){
		$this->roundVal=$val;
	}
	public function getDecimalRound(){
		return $this->roundVal;
	}

}


class HTMLRendererConfig extends  RendererConfig{
	private $rowPadding;
	private $pad='-';
	public function isRowPadding(){
		return $this->rowPadding;
	}
	public function setRowPadding($bool){
		$this->rowPadding=$bool;
	}
	public function getPad(){
		return $this->pad;
	}
	public function setPad($pad){
		$this->pad=$pad;
	}
}



class ExcelRendererConfig extends  RendererConfig{
	public static $FORMAT_PDF="PDF";
	public static $FORMAT_EXCEL="Excel5";
	public static $FORMAT_EXCEL7="Excel2007";
	public static $FORMAT_CSV="CSV";
	private $isRenderToFile=false;
	private $file;
	private $creator;
	private $showGrid=true;
	private $format="Excel2007";
	public function setRenderToFile($toFile){
		$this->isRenderToFile=$toFile;
	}
	public function isRenderToFile(){
		return $this->isRenderToFile;
	}
	public function setFileName($file){
		$this->file=$file;
	}
	public function getFileName(){
		return $this->file;
	}
	public function getCreator(){
		return $this->creator;
	}
	public function setCreator($val){
		$this->creator=$val;
	}
	public function setShowGrid($bool){
		$this->showGrid=$bool;
	}
	public function isShowGrid(){
		return $this->showGrid;
	}
	public function setOutputFormat($format){
		$this->format=$format;
	}
	public function getOutputFormat(){
		return $this->format;
	}
	public function getFileExt(){
		if($this->format==self::$FORMAT_EXCEL7)
		return "xlsx";
		if($this->format==self::$FORMAT_EXCEL)
		return "xls";
		if($this->format==self::$FORMAT_PDF)
		return "pdf";
		if($this->format==self::$FORMAT_CSV)
		return "csv";
	}
}

class MailRendererConfig extends  RendererConfig{
	private $from;
	private $to=array();
	private $cc;
	private $subject;
	private $showGrid=true;
	private $format="Excel2007";
	private $inline=true;
	private $attachment=true;


	public function setShowGrid($bool){
		$this->showGrid=$bool;
	}
	public function isShowGrid(){
		return $this->showGrid;
	}
	public function setAttachmentFormat($format){
		$this->format=$format;
	}
	public function getAttachmentFormat(){
		return $this->format;
	}
	public function setFrom($id,$name){
		$this->from['id']=$id;
		$this->from['name']=$name;
	}
	public function addTo($id,$name){
		$index=count($this->to);
		$this->to[$index]['id']=$id;
		$this->to[$index]['name']=$name;
	}

	public function addCC($id,$name){
		$index=count($this->cc);
		$this->cc[$index]['id']=$id;
		$this->cc[$index]['name']=$name;
	}

	public function setSubject($sub){
		$this->subject=$sub;
	}
	public function getSubject(){
		return $this->subject;
	}
	public function getTo(){
		return $this->to;
	}
	public function getFrom(){
		return $this->from;
	}
	public function getCC(){
		return $this->cc;
	}
	public function setInline($inline){
		$this->inline=$inline;
	}
	public function getInline(){
		return $this->inline;
	}
	public function setFormat($format){
		$this->format=$format;
	}
	public function getFormat(){
		return $this->format;
	}
	public function setAttachment($attachment){
		$this->attachment=$attachment;
	}
	public function getAttachment(){
		return $this->attachment;
	}
}


interface IReportRenderer{
	public function render($reportModel,$rendererConfig);
}


?>