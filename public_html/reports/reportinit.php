<?php
/************************************************
Myntra weekly report generation of following
*************************************************/

require_once "../auth.php";

include ("../jpgraph/src/jpgraph.php" );




//Get weeks
$weeks = array();
$weeks[0] =time()-5*7*24*3600;
$weeks[1] =time()-4*7*24*3600;
$weeks[2] =time()-3*7*24*3600;
$weeks[3] =time()-2*7*24*3600;
$weeks[4] =time()-1*7*24*3600;
$weeks[5] =time();

$fixedmonthlables=array('Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec');
$weeklabels = array('w1','w2','w3','w4','last');
$currentmonthlabels= array();


//Get months
$months = array();

$currentmonth = date('m');
$currentyear = date('Y'); 

$startyear = $currentyear -1;

$startmonth=$currentmonth +1;

if($startmonth > 12)
{
	$startmonth = 1;
	$startyear =$currentyear;

}
for($i=0; $i<12; $i++)
{
	$currentmonthlabels[$i]=$fixedmonthlables[$startmonth-1];
	$months[$i]= $startyear."-".$startmonth."-"."01";
	$startmonth++;
	if($startmonth > 12)
	{
		$startmonth =1;
		$startyear++;

	}

}
$months[12]=date('Y-m-d',time()+86400);

?>

