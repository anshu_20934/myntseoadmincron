<?php
define("REPORTS_CONNECTION",true);
include "../admin/auth.php";
//require_once $xcart_dir."/include/security.php";
$report="<span style='font-size: 15pt; color: teal;'><u>MYNTRA CUSTOMER SATISFACTION REPORT -</u><span style='font-size: 15pt; color: red;'><u>". date("j F, Y")."</span></u></span><BR/><BR/><DIV>";

$starttime = time()-86400;
$endtime =time();

//$report.="<span style='font-size: 12pt; color: red;font-weight:bold;'>Myntra Customer Satisfaction Data:</span><BR/><div style='margin-left:20px;margin-top:10px;'>";
$report.="<div style='margin-left:20px;margin-top:10px;'>";
# Users registered yesterday

$arr_questionid = array();
$arr_avgweightage = array();
$arr_question = array();

$sql ="select a.questionid as questionsids,c.question as questions,avg(b.weightage) as weightageavg from mk_feedback_data a,mk_feedback_options b,mk_feedback_questions c
where b.questionid=a.questionid and a.questionid in (select a.id from mk_feedback_questions a,mk_feedbacks b where a.feedbackid=b.id and b.name='order_feedback2') and a.response=b.optionname and feedback_id in 
(select id from mk_order_feedback where recieved_date <=unix_timestamp(now()) and recieved_date>=unix_timestamp(now())-30*24*3600
)  and a.questionid= c.id   group by a.questionid ";

$result =db_query($sql, true);
$i=0;
$csindex=0;
while($row=db_fetch_array($result)){
	$arr_questionid[$i] = $row['questionsids'];
	$arr_avgweightage[$i] = $row['weightageavg'];
	$arr_question[$i] = $row['questions'];
	$csindex = $csindex + $arr_avgweightage[$i];
	$i++;
}
$report.=" <span style='font-size: 12pt; color: teal;'># Overall Customer satisfaction index (based on last 30 days data:<b> ".number_format($csindex,2, '.', '')."&nbsp;&nbsp;"."</b></span><br/><br/>";

//Quality CSI
$report.=" <span style='font-size: 11pt; color: teal;'><b># Quality Index : ".number_format((($arr_avgweightage[4]/30)*100),2, '.', '')."%&nbsp;&nbsp;"."</b>(<span style='font-size: 12pt; color: red;font-weight:bold;'>".(100-number_format((($arr_avgweightage[4]/30)*100),2, '.', ''))."%</span> had quality issues)</span><br/>";

$report.=" <span style='font-size: 10pt; color: teal;'># ".$arr_question[4].": ".number_format((($arr_avgweightage[4]/30)*100),2, '.', '')."&nbsp;&nbsp;"."(".number_format($arr_avgweightage[4],2, '.', '')." out of 30)</span><br/><br/>";

//Logistics CSI
$report.=" <span style='font-size: 11pt; color: teal;'><b># Delivery Index : ".number_format((($arr_avgweightage[3]/25)*100),2, '.', '')."%&nbsp;&nbsp;"."</b>(<span style='font-size: 12pt; color: red;font-weight:bold;'>".(100-number_format((($arr_avgweightage[3]/25)*100),2, '.', ''))."%</span> delivered late)</span><br/>";
$report.=" <span style='font-size: 10pt; color: teal;'># ".$arr_question[3].": ".number_format((($arr_avgweightage[3]/25)*100),2, '.', '')."&nbsp;&nbsp;"."(".number_format($arr_avgweightage[3],2, '.', '')." out of 25)</span><br/><br/>";


//Engineering CSI
$report.=" <span style='font-size: 11pt; color: teal;'><b># Online Experience Index : ".number_format(((($arr_avgweightage[0]+$arr_avgweightage[1])/20)*100),2, '.', '')."%&nbsp;&nbsp;"."</b></span><br/>";
$report.=" <span style='font-size: 10pt; color: teal;'># ".$arr_question[0].": ".number_format((($arr_avgweightage[0]/10)*100),2, '.', '')."&nbsp;&nbsp;"."(".number_format($arr_avgweightage[0],2, '.', '')." out of 10)</span><br/>";
$report.=" <span style='font-size: 10pt; color: teal;'># ".$arr_question[1].": ".number_format((($arr_avgweightage[1]/10)*100),2, '.', '')."&nbsp;&nbsp;"."(".number_format($arr_avgweightage[1],2, '.', '')." out of 10)</span><br/><br/>";


//CS CSI
$report.=" <span style='font-size: 11pt; color: teal;'><b># Customer Service Index : ".number_format((($arr_avgweightage[2]/15)*100),2, '.', '')."%&nbsp;&nbsp;"."</b></span><br/>";
$report.=" <span style='font-size: 10pt; color: teal;'># ".$arr_question[2].": ".number_format((($arr_avgweightage[2]/15)*100),2, '.', '')."&nbsp;&nbsp;"."(".number_format($arr_avgweightage[2],2, '.', '')." out of 15)</span><br/><br/>";

//Overall Input for myntra CSI
$report.=" <span style='font-size: 11pt; color: teal;'><b># Shopping experience with Myntra : ".number_format((($arr_avgweightage[5]/10)*100),2, '.', '')."&nbsp;&nbsp;"."</b></span><br/>";
$report.=" <span style='font-size: 10pt; color: teal;'># ".$arr_question[5].": ".number_format((($arr_avgweightage[5]/10)*100),2, '.', '')."&nbsp;&nbsp;"."(".number_format($arr_avgweightage[5],2, '.', '')." out of 10)</span><br/><br/>";

$sql="select o.login, o.mobile, response from mk_feedback_data a ,mk_order_feedback b, xcart_orders o where a.questionid=20
and a.feedback_id=b.id  and b.orderid = o.orderid
and b.recieved_date > (unix_timestamp()-7*24*3600) and response <> '' order by b.recieved_date desc";

$result =db_query($sql, true);

$report.="<span style='font-size: 12pt; color: red;font-weight:bold;'>Do you have any feedback or suggestions for the myntra.com?(responses in last 7 days):</span><BR/><div style='margin-left:20px;margin-top:10px;'>";
$counter =0;
$report.="<table width=\"1000\" border=\"1\" cellspacing=\"0\" cellpadding=\"5\"  bgcolor=\"white\" style=\"text-align:center;\"><tr><th style='font-size: 12pt; color: teal;'>Login</th><th style='font-size: 12pt; color: teal;'>Mobile</th><th style='font-size: 12pt; color: teal;'>Response</th></tr>";
while ($row=db_fetch_row($result))
{
	$counter++;
	$report.="<tr><td style='font-size: 10pt; color: teal;'>" .$row['0']. "</td><td style='font-size: 10pt; color: teal;'>" .$row['1']. "</td><td style='font-size: 10pt; color: teal;'>" .$row['2']. "</td></tr>";
}

$report.="</table></DIV>";
echo($report);

$yesterday = date('d/m/y', mktime(0, 0, 0, date("m") , date("d") - 1, date("Y")));
$subject ="Myntra Customer Satisfaction Report ".$yesterday;
//$mailto='rpt_customersatisfaction@myntra.com';
//$mailto='myntra.next.amazon@gmail.com';
$mailto='myntra.next.amazon@myntra.com';
$headers = "Content-Type: text/html; charset=ISO-8859-1 " . "\n";
$headers .= "From: Myntra Admin <admin@myntra.com>" . "\n";
$flag = @mail($mailto, $subject, $report,$headers);


?>
