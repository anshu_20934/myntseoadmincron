<?php
if(!defined("AUTHORIZED_TO_RENDER")){
	die;
}
include $xcart_dir.'/utils/MailUtil.php';
include_once $xcart_dir.'/reports/report_model.php';
include_once $xcart_dir.'/reports/rendrers/rendrers.php';
//putenv("TZ=Asia/Calcutta");
//db_query("SET SESSION time_zone = 'Asia/Calcutta'; ");
//request Params
//show_filters yes/no/allset
//caller scheduler/user
//report name
//format=html/pdf/csv/excel7/excel5
//action=mail/genreport
$report=$_GET['report'];//Id of the report model to render see report_model.php
$action=$_GET['action'];// Action to perform i.e. action=mail to send mail, action=genreport to genrate report on browser
$show_filters=$_GET['prompt_filters'];// if yes it will prompt for filters in browser
$caller=$_GET['caller']; // caller
$format=$_GET['format'];// fromat to generate report in
$decimals=$_GET['decimals'];
$emailsToList=$_GET['emailsToList']; // emailtolist group defined in *.env.php
$report_title=$_GET['report_title']; // emailtolist group defined in *.env.php
$report_list=$_GET['report_list'];   // used in case of grouping
$mail_inline=$_GET['inline'];
$mail_attachment=$_GET['attachment'];
$report_mode=$_GET['mode'];
$brand=$_GET['brand'];
if(!isset($report))
	$report=$_POST['report'];
if(!isset($action))
	$action=$_POST['action'];
if(!isset($show_filters))
	$show_filters=$_POST['prompt_filters'];
if(!isset($caller))
	$caller=$_POST['caller'];
if(!isset($format))
	$format=$_POST['format'];
if(!isset($decimals))
	$decimals=$_POST['decimals'];
if(!isset($emailsToList))
	$emailsToList=$_POST['emailsToList'];
if(!isset($report_title))
	$report_title=$_POST['report_title'];
if(!isset($report_list))
	$report_list=$_POST['report_list'];
if(!isset($mail_inline))
	$mail_inline=$_POST['inline'];
if(!isset($mail_attachment))
	$mail_attachment=$_POST['attachment'];
if(!isset($report_mode))
	$report_mode=$_POST['mode'];	
if(!isset($brand))
	$brand=$_POST['brand'];	
if(!isset($decimals))
	$decimals=0;
if(!isset($mail_inline))
	$mail_inline=true;
if(!isset($mail_attachment))
	$mail_attachment=true;
		
$modelFactory= new ReportModelFactory();
$reportModel=$modelFactory->createModel($report);
$reportModel->loadFilterValuesFrom($_GET,$_POST);
$rendererConfig;
$renderer;
if($action=='genreport'){
	if($format=='html'){
		
		include_once $xcart_dir.'/reports/rendrers/html_rendrer.php';

		$rendererConfig= new HTMLRendererConfig();
		$rendererConfig->setPad('...');
		$rendererConfig->setDecimalRound($decimals);
		$rendererConfig->setRowPadding(false);
		$rendererConfig->setReportName($reportModel->getReportName());
		$renderer = new HTMLReportRenderer();
	}else if($format=='csv'){
		include_once $xcart_dir.'/reports/rendrers/document_rendrer.php';
		$rendererConfig=new ExcelRendererConfig();
		$rendererConfig->setOutputFormat(ExcelRendererConfig::$FORMAT_CSV);
		$rendererConfig->setDecimalRound($decimals);
		$rendererConfig->setCreator("Myntra Reports");
		$rendererConfig->setReportName($reportModel->getReportName());
		$renderer = new ExcelReportRenderer();
	}else if($format=='excel5'){
		include_once $xcart_dir.'/reports/rendrers/document_rendrer.php';
		$rendererConfig=new ExcelRendererConfig();
		$rendererConfig->setOutputFormat(ExcelRendererConfig::$FORMAT_EXCEL);
		$rendererConfig->setDecimalRound($decimals);
		$rendererConfig->setCreator("Myntra Reports");
		$rendererConfig->setReportName($reportModel->getReportName());
		$renderer = new ExcelReportRenderer();
	}else if($format=='pdf'){
		include_once $xcart_dir.'/reports/rendrers/document_rendrer.php';
		$rendererConfig=new ExcelRendererConfig();
		$rendererConfig->setOutputFormat(ExcelRendererConfig::$FORMAT_PDF);
		$rendererConfig->setDecimalRound($decimals);
		$rendererConfig->setCreator("Myntra Reports");
		$rendererConfig->setShowGrid(false);
		$rendererConfig->setReportName($reportModel->getReportName());
		$renderer = new ExcelReportRenderer();
	}else if ($format=='excel7'){
		include_once $xcart_dir.'/reports/rendrers/document_rendrer.php';
		$rendererConfig=new ExcelRendererConfig();
		$rendererConfig->setOutputFormat(ExcelRendererConfig::$FORMAT_EXCEL7);
		$rendererConfig->setDecimalRound($decimals);
		$rendererConfig->setCreator("Myntra Reports");
		$rendererConfig->setReportName($reportModel->getReportName());
		$renderer = new ExcelReportRenderer();
	}
	
	
}else if($action=='mail'){
	include_once $xcart_dir.'/reports/rendrers/document_rendrer.php';
	include_once $xcart_dir.'/reports/rendrers/mail_renderer.php';
	$rendererConfig = new MailRendererConfig();
	$mailIdIterator = new MailIDEnumerator(constant($emailsToList));
	while($mailIdIterator->hasMoreElements()){
		$idname=$mailIdIterator->nextElement();
		$rendererConfig->addTo($idname['id'], $idname['name']);
	}
	$rendererConfig->setSubject($reportModel->getReportName());
	$rendererConfig->setDecimalRound($decimals);
	$rendererConfig->setFrom("support@myntra.com", "Myntra Reporting Service");
	$rendererConfig->setReportName($reportModel->getReportName());
	if($format=='csv'){
		require_once $xcart_dir.'/include/class/csvutils/class.csvutils.NumericArrayCSVWriter.php';
		$rendererConfig->setFormat($format);
		//$rendererConfig->setAttachmentFormat(ExcelRendererConfig::$FORMAT_CSV);
	}else if ($format=='excel7'){
		$rendererConfig->setAttachmentFormat(ExcelRendererConfig::$FORMAT_EXCEL7);
	}else if($format=='pdf'){
		$rendererConfig->setAttachmentFormat(ExcelRendererConfig::$FORMAT_PDF);
	}else{
		$rendererConfig->setAttachmentFormat(ExcelRendererConfig::$FORMAT_EXCEL);
	}
	$rendererConfig->setInline($mail_inline);
	$rendererConfig->setAttachment($mail_attachment);
	$renderer = new MailReportRenderer();
}


if($show_filters=='no' || $show_filters=='allset'){
	$reportModel->buildReport();
}
$renderer->render($reportModel, $rendererConfig);

