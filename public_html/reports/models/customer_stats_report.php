<?php

global $xcart_dir;
include_once $xcart_dir.'/reports/models/models.php';
include_once $xcart_dir.'/reports/rendrers/rendrers.php';
include_once REP_DIR.'/models/date_calculation.php';

class CustomerStatsReportModel extends ReportModel {

	public function __construct(){
		parent::__construct();
		$this->name="Customer Stats Report : " . date("j-M-Y");

	}
	protected function initFilters(){
		$this->addFilter("Date:" ,self::$FILTER_TYPE_DATE);
	}

	public function loadDefaultFilterValues(){
		$time=time();
		$this->setFilterValue("Date:", date("m/d/Y",$time));
	}
	
	public function statsInfo(){
		$table= new Table();
		$headers= array();
		$table->setFooterVisible(false);
		$filtervalue = $this->getFilterValue("Date:");
		$date_split=split('/', $filtervalue);
		$current_month=(INT)$date_split[0];
		$current_year=(INT)$date_split[2];
		$current_date=(INT)$date_split[1];
		$curr_fdate= new FDate($current_date, $current_month, $current_year);
		$fyear = new FYear($current_date, $current_month, $current_year);
		$TOTAL_MONTHS_SPENT=0;
		foreach ($fyear->getFinancialYearIterator() as $date){
			$TOTAL_MONTHS_SPENT++;
		}

		$j=0;

		$table->setHeaderValue($j,"Customer Statistics [Last 6 months]");
		$table->setCellData(0, $j,"Total # Customers" );
		$table->setCellData(1, $j,"# Active Customers (3 months)" );
		$table->setCellData(2, $j,"# Active Customers (6 months)");
		$table->setCellData(3, $j,"# Active Customers (12 months)");
		$table->setCellData(4, $j,"# Repeat Customers");
		$table->setCellData(5, $j,"# Repeat Customers (2 purchases)");
		$table->setCellData(6, $j,"# Repeat Customers (3-5 purchases)");
		$table->setCellData(7, $j,"# Repeat Customers (5+ purchases)");
		$table->setCellData(8, $j,"Avg. Revenue/Repeat Customer");
		$j++;

		$temp_fdate= new FDate($current_date, $current_month, $current_year);
			
		$calc_dates=array();
		$i=0;
		for ($ii=0;$ii<6;$ii++){
			$start_fdate=$temp_fdate->getPrevMonthStartFDate();
			$end_fdate=$start_fdate->getNextMonthStartFDate();
			$calc_dates[$i]['start']=$start_fdate->toUnixDateDayStart();
			$calc_dates[$i]['end']=$end_fdate->toUnixDateDayStart();
			$calc_dates[$i]['msp']=$TOTAL_MONTHS_SPENT-$ii-1;
			$calc_dates[$i]['label']=FYear::$MONTH_NAME[$start_fdate->getMonth()]." ".$start_fdate->getYear();
			$temp_fdate=$start_fdate;
			$i++;
		}
			
		foreach ($calc_dates as $span){
			$table->setHeaderValue($j, $span['label']);
			$active_cust=func_query("select	(case true	when (a.date)>(unix_timestamp('".$span['end']."')-(60*60*24*90)) then '#3'
							when (a.date)>(unix_timestamp('".$span['end']."')-(60*60*24*180)) then '#6'
							when (a.date)>(unix_timestamp('".$span['end']."')-(60*60*24*365)) then '#12'
							else '#MORE'
						end 
						) active, count( a.login) value from (select max(date) as 'date',login as login from xcart_orders  where status in ('OH','C','WP','SH','Q','DL')
				and date <unix_timestamp('".$span['end']."')  group by login)  a group by active", true);
			$purchase=func_query("select	(case true	when (a.orders)>5 then '#5+'
						when (a.orders)>=3 then '#3-5' 
						when (a.orders)=2 then '#2' 
						else '#MORE'
					end 
					) active, count( a.login) value from (select count(orderid) as 'orders',login as login from xcart_orders  
					where status in ('OH','C','WP','SH','Q','DL')
			and date <unix_timestamp('".$span['end']."')  group by login)  a group by active", true);

			$rep=func_query("
			select sum(a.value) as value,count(distinct a.login) as repusers from (
			select login as login,count(login) as cnt,sum(total+shipping_cost+gift_charges)  as value from xcart_orders 
			where status in ('C','Q','WP','OH','SH','DL') and date<unix_timestamp('".$span['end']."') group by login) as a where a.cnt>1", true);

			$temp[$active_cust[0]['active']]=$active_cust[0]['value'];
			$temp[$active_cust[1]['active']]=$active_cust[1]['value'];
			$temp[$active_cust[2]['active']]=$active_cust[2]['value'];
			$temp[$active_cust[3]['active']]=$active_cust[3]['value'];

			$table->setCellData(0, $j, (float)($temp['#3']+$temp['#6']+$temp['#12']+$temp['#MORE']));
			$table->setCellData(1, $j, (float)$temp['#3']);
			$table->setCellData(2, $j, (float)$temp['#6']+(float)$temp['#3']);
			$table->setCellData(3, $j, (float)$temp['#12']+(float)$temp['#6']+(float)$temp['#3']);

			$temp[$purchase[0]['active']]=$purchase[0]['value'];
			$temp[$purchase[1]['active']]=$purchase[1]['value'];
			$temp[$purchase[2]['active']]=$purchase[2]['value'];
			$temp[$purchase[3]['active']]=$purchase[3]['value'];

			$table->setCellData(4, $j, (float)($temp['#5+']+$temp['#3-5']+$temp['#2']));
			$table->setCellData(5, $j, (float)$temp['#2']+(float)$temp['#5+']+(float)$temp['#3-5']);
			$table->setCellData(6, $j, (float)$temp['#3-5']+(float)$temp['#5+']);
			$table->setCellData(7, $j, (float)$temp['#5+']);
			$table->setCellData(8, $j, (float)$rep[0]['value']/(float)$rep[0]['repusers']);
			$j++;
		}
		$table->setRowStyle(8, Table::$FORMAT, "currency");
		return $table;
	}
	
	public function buildReport(){
		global $weblog;
		$statstable= new Table();
		$group= new TableGroup("Customer Stats Report");
		$headers= array();
		$headers[0]= 'Facts';
		$statstable->setFooterVisible(false);
		$filtervalue = $this->getFilterValue("Date:");
		$date_split=split('/', $filtervalue);
		$current_month=(INT)$date_split[0];
		$current_year=(INT)$date_split[2];
		$current_date=(INT)$date_split[1];
		$ReportData;
		//function to calculate the end and start days for various headers.
		$calculated_dates = calculateDates($current_date, $current_month, $current_year);
		for($cnt=0;$cnt<count($calculated_dates);$cnt++){
			// resetting the variables.
			$temp['old'] = 0;
			$temp['new'] = 0;
			$temp['NEW'] = array('type'=>null,'orders'=>0,'value'=>0,'days'=>0);
			$temp['REP'] = array('type'=>null,'orders'=>0,'value'=>0,'days'=>0);
			
			$sql = "select (case true when first_login between unix_timestamp('" . $calculated_dates[$cnt]['start'] . "') and unix_timestamp('" . $calculated_dates[$cnt]['end'] . "') then 'new' else 'old' end) as utype,
			count(login)  as logins from xcart_customers where first_login<unix_timestamp('" . $calculated_dates[$cnt]['end'] . "') group by utype";
			$result=func_query($sql, true);
			$temp[$result[0]['utype']]=$result[0]['logins'];
			$temp[$result[1]['utype']]=$result[1]['logins'];
			$ReportData['TotalReg'][$cnt] = (float)($temp['new'])+(float)($temp['old']);
			$ReportData['NewReg'][$cnt] = (float)($temp['new']);

			$sql = "select b.ctype as type,count(b.orders) as orders,sum(b.value) as value, sum((b.date - b.first_login)/86400) as days
					from (select (case exists (select distinct login from xcart_orders where login=a.login and
				 date< a.date and status in ('C','Q','WP','OH','SH','DL')) when true
				                    then 'REP' else 'NEW'
				                    end ) as ctype,(a.orderid) as orders,(a.login) as logins, (a.total+a.shipping_cost+a.gift_charges) value, c.first_login, a.date  
				from xcart_orders a, xcart_customers c where a.login=c.login and a.status in ('C','Q','WP','OH','SH','DL') and a.date >=unix_timestamp('" . $calculated_dates[$cnt]['start'] . "') 				
				and  a.date <unix_timestamp('" . $calculated_dates[$cnt]['end'] . "')) as b  group by b.ctype";
			$result=func_query($sql, true);
			$result_noPurchase = func_query("select count(*) as nopurchase from xcart_customers c left join  xcart_orders o on o.login=c.login and o.status in  ('OH','C','WP','SH','Q','DL') where c.first_login < unix_timestamp('" . $calculated_dates[$cnt]['end'] . "')
				and o.orderid is null", true);
			$result_Revenue = func_query("select sum(xo.total+xo.shipping_cost+xo.gift_charges) as 'booking' from xcart_orders xo where xo.date >= unix_timestamp('" . $calculated_dates[$cnt]['start'] . "') and xo.date < unix_timestamp('" . $calculated_dates[$cnt]['end'] . "') 
											and xo.status  in ('Q','WP','OH','C','SH','DL')", true);
			$temp[$result[0]['type']]=$result[0];
			$temp[$result[1]['type']]=$result[1];
			$ReportData['FirstTranx'][$cnt] = (float)$temp['NEW']['orders'];
			$ReportData['ASPFirstTranx'][$cnt] = (float)$temp['NEW']['value']/(float)$temp['NEW']['orders'];
			$ReportData['RepeatTranx'][$cnt]= (float)$temp['REP']['orders'];
			$ReportData['ASPRepeatTranx'][$cnt] = (float)$temp['REP']['value']/(float)$temp['REP']['orders'];
			$ReportData['AvgTime'][$cnt] = (float)$temp['NEW']['days']/$temp['NEW']['orders'];
			$ReportData['NoPurchase'][$cnt] = (float)$result_noPurchase[0]['nopurchase'];
			$ReportData['Revenue'][$cnt] = (float)$result_Revenue[0]['booking'];
			$headers[$cnt+1] = $calculated_dates[$cnt]['label'];
		}
		$width = $cnt+1;
		$statstable->setHeaders($headers);
		$statstable->setTableWidth($width);
		$rowIdentifiers = array('TotalReg', 'NewReg', 'FirstTranx', 'ASPFirstTranx', 'RepeatTranx', 'ASPRepeatTranx', 'AvgTime', 'NoPurchase', 'Revenue');
		$rowHeaderValues = array('Total No. of Registrations', 'No. of New Registrations', 'No. of First-Time Transactions', 'ASP of First-Time Transaction', 'No. of Repeat Transactions', 'ASP of Repeat Transactions', 'Average time between Registration & 1st Purchase', 'No. of users who have never purchased', 'Total Revenue');
		$i=0;
		//set report data in cells of table

		foreach($rowIdentifiers as $rowid){
			$j=0;
			$statstable->setCellData($i, $j, $rowHeaderValues[$i]);
			for ($j=1; $j<$width; $j++){
				$statstable->setCellData($i, $j, $ReportData[$rowid][$j-1]);
			}
			$i++;
		}

		//set formatting for columns of tables
		$statstable->setStyleMatrix(0, 1, $i-1, 8, TABLE::$STYLE_COLOR, "#EDF2F8");
		$statstable->setStyleMatrix(0, 9, $i-1, 13, TABLE::$STYLE_COLOR, "#CAD9EB");
		$statstable->setStyleMatrix(0, 14, $i-1, 17, TABLE::$STYLE_COLOR, "#EDF2F8");
		$statstable->setStyleMatrix(0, 18, $i-1, 21, TABLE::$STYLE_COLOR, "#CAD9EB");
		$statstable->setRowStyle(3, Table::$FORMAT, "currency");
		$statstable->setRowStyle(5, Table::$FORMAT, "currency");
		$statstable->setRowStyle(8, Table::$FORMAT, "currency");
		
		//add other table
		$statsInfoTable = $this->statsInfo();
		
		$group->addTable($statstable);
		//$group->addTable($statsInfoTable);
		$this->addTableGroup($group);
		
		$this->notes = "WTD = Stats from 00:00 Monday to 23:59 Yesterday <br> 
						W3 = " . $calculated_dates[9]['start'] . " to " . $calculated_dates[9]['end'] ."<br> 
						W2 = " . $calculated_dates[10]['start'] . " to " . $calculated_dates[10]['end'] ."<br> 
						W1 = " .$calculated_dates[11]['start'] . " to " . $calculated_dates[11]['end'] . "<br> 
						W0 = " . $calculated_dates[12]['start'] . " to " . $calculated_dates[12]['end'] . "<br><br>";
		$this->isBuild=true;
	}


}
?>

