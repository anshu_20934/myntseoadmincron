<?php

global $xcart_dir;
include_once $xcart_dir.'/reports/models/models.php';
include_once $xcart_dir.'/reports/rendrers/rendrers.php';
include_once REP_DIR.'/models/date_calculation.php';

class ConsolidatedStocksAndSalesReportModel extends ReportModel {

	public function __construct(){
		parent::__construct();
		$this->name="Consolidated Stocks and Sales Report : " . date("j-M-Y");

	}
	protected function initFilters(){
		$this->addFilter("Date:" ,self::$FILTER_TYPE_DATE);
	}

	public function loadDefaultFilterValues(){
		$time=time();
		$this->setFilterValue("Date:", date("m/d/Y",$time));
	}
	
	private function getBrandwiseSalesData($startdate, $enddate, $ReportData, $dateIdentifier) {
		
		$sqlBrandwiseSales = 
		"select 
			sp.global_attr_brand as 'brand',
			sum(xod.amount) as 'ItemQuantity',
			sum((xod.amount*xod.actual_product_price) - xod.coupon_discount_product - xod.discount 
		    - xod.pg_discount - xod.cash_redeemed  + ((xo.shipping_cost/xo.qtyInOrder)*xod.amount) + ((xo.gift_charges/xo.qtyInOrder)*xod.amount)
		  	) as 'NetReceivable'
		from 
			xcart_orders xo
			join xcart_order_details xod on xod.orderid=xo.orderid
		  join mk_style_properties sp on sp.style_id=xod.product_style
		where
			xo.status in('C','DL','SH') 
		  and xod.item_status!='IC' and xod.canceltime is null
		  and xo.date >= unix_timestamp('$startdate') and xo.date < unix_timestamp('$enddate')
		group by 
		    brand
		";
		
		$result = func_query($sqlBrandwiseSales, true);
		
		foreach ($result as $row){
			if($row['brand'] == null){
				$row['brand'] = 'Others';
			}
			
			$ReportData[$row['brand']]["ItemQuantity-$dateIdentifier"] = $row['ItemQuantity'];
			$ReportData[$row['brand']]["NetReceivable-$dateIdentifier"] = $row['NetReceivable'];
		}
		
		return $ReportData;
	}

	public function buildReport(){
		global $weblog;
		
		$filtervalue = $this->getFilterValue("Date:");
		$date_split=split('/', $filtervalue);
		$current_month=(INT)$date_split[0];
		$current_year=(INT)$date_split[2];
		$current_date=(INT)$date_split[1];
		$calculated_dates1 = calculateDates($current_date, $current_month, $current_year); //MTD is index:13
		$calculated_dates2 = calculateDates_DaysFormat1($current_date, $current_month, $current_year); //index : 1 and 2
		
		$table= new Table("Brandwise Inventory Report");
		$group= new TableGroup("Brand Sales Stock Report");
		$superHeaders= array('Brands'=>1,'#Active'=>2,'Total Stock in Hand'=>2,'New Stock (MTD)'=>2,
					"Sales Last 7 days"=>2,
					"Sales " . $calculated_dates1[13][label]=>2,	// MTD
					"Sales " . $calculated_dates1[14][label]=>2,	// Previous Month
					"Sales " . $calculated_dates1[15][label]=>2,	// Previous Previous Month
					'#Weekly Cover'=>1);
		$headers= array('','SKUs', 'Styles', '#Portal', '#NotInPortal', '#Items', '#Articles', '#Items', 'Revenue', '#Items', 'Revenue', '#Items', 'Revenue','#Items', 'Revenue','');
		$table->setSuperHeaders($superHeaders);
		$table->setHeaders($headers);
		$table->setFooterVisible(false);
		
		$ReportData;
		
		$sqlActiveStylesSkus = 
		"select
			sp.global_attr_brand as 'brand',
			count(distinct(cic.sku_id)) as 'ActiveSKUs', count(distinct(sp.style_id)) as 'ActiveStyles'
    	from
			core_inventory_counts cic
			join mk_styles_options_skus_mapping skuMapping on cic.sku_id = skuMapping.sku_id
			join mk_style_properties sp on sp.style_id = skuMapping.style_id		
		where
			cic.quality='Q1'			
			and (cic.inv_count - cic.blocked_order_count - cic.blocked_manually_count - cic.blocked_missed_count - cic.blocked_processing_count )>0
		group by
			sp.global_attr_brand
		";
		
		$result = func_query($sqlActiveStylesSkus, true);
		foreach ($result as $row){
			if($row['brand'] == null){
				$row['brand'] = 'Others';
			}
			$ReportData[$row['brand']]['ActiveSKUs'] = $row['ActiveSKUs'];
			$ReportData[$row['brand']]['ActiveStyles'] = $row['ActiveStyles'];
		}
		
		$sqlInventoryCountLiveOnPortal =
		"select
			sp.global_attr_brand as 'brand',
			sum(cic.inv_count - cic.blocked_order_count - cic.blocked_manually_count 
		            - cic.blocked_missed_count - cic.blocked_processing_count ) as 'InventoryCountLiveOnPortal'
		from
			core_inventory_counts cic
			join mk_styles_options_skus_mapping skuMapping on cic.sku_id = skuMapping.sku_id
			join mk_style_properties sp on sp.style_id = skuMapping.style_id		
			join mk_product_style ps on ps.id=skuMapping.style_id
			join mk_product_options po on skuMapping.option_id=po.id
			where
				cic.quality='Q1'
				and ps.styletype='P' and ps.is_active=1 and po.is_active=1
				and cic.inv_count>0
				and (cic.inv_count - cic.blocked_order_count - cic.blocked_manually_count 
	            - cic.blocked_missed_count - cic.blocked_processing_count )>0
		group by
			sp.global_attr_brand
		";

		$result = func_query($sqlInventoryCountLiveOnPortal, true);
		foreach ($result as $row){
			if($row['brand'] == null){
				$row['brand'] = 'Others';
			}
			$ReportData[$row['brand']]['InventoryCountLiveOnPortal'] = $row['InventoryCountLiveOnPortal'];
		}
		
		$sqlInventoryNotLiveOnPortal =
		"select
			sp.global_attr_brand as 'brand',
			sum(cic.inv_count - cic.blocked_order_count - cic.blocked_manually_count - cic.blocked_missed_count - cic.blocked_processing_count ) 
				as 'InventoryCountNOTLiveOnPortal'
		from
			core_inventory_counts cic
			join mk_styles_options_skus_mapping skuMapping on cic.sku_id = skuMapping.sku_id
			join mk_style_properties sp on sp.style_id = skuMapping.style_id		
			join mk_product_style ps on ps.id=skuMapping.style_id
			join mk_product_options po on skuMapping.option_id=po.id
		where
			cic.quality='Q1'
			and ((ps.is_active=0 and ps.styletype='P') or  ps.styletype != 'P')
			and cic.inv_count>0
			and (cic.inv_count - cic.blocked_order_count - cic.blocked_manually_count - cic.blocked_missed_count - cic.blocked_processing_count )>0    
		group by
			sp.global_attr_brand
		";
		
		$result = func_query($sqlInventoryNotLiveOnPortal, true);
		foreach ($result as $row){
			if($row['brand'] == null){
				$row['brand'] = 'Others';
			}
			$ReportData[$row['brand']]['InventoryCountNOTLiveOnPortal'] = $row['InventoryCountNOTLiveOnPortal'];
		}
		
		
		$sqlNewStockMTD =
		"select
			sp.global_attr_brand as brand,
			sum(taba.items) as InwardedItems,
			count(distinct sp.style_id) as InwardedArticles
		from
			(select ci.sku_id, count(*) as items
			from
				myntra_auditor.audit_trail atrail
				join core_items ci
				join core_po_skus posku
			where ci.id=atrail.entityId
				and ci.po_sku_id = posku.id 
				and atrail.entityName = 'com.myntra.framework.entities.Item' and atrail.entityProperty='itemStatus'
				and atrail.entityPropertyNewValue = 'STORED'
				and atrail.actionTime >= ('" . $calculated_dates1[13]['start'] . "') and atrail.actionTime < ('" . $calculated_dates1[13]['end'] . "')
			group by ci.sku_id
			)
			taba
			left join mk_styles_options_skus_mapping osm on osm.sku_id=taba.sku_id
			left join mk_style_properties sp on osm.style_id=sp.style_id 
		group by
			sp.global_attr_brand
		";
		
		$result = func_query($sqlNewStockMTD, true);
		foreach ($result as $row){
			if($row['brand'] == null){
				$row['brand'] = 'Others';
			}
			$ReportData[$row['brand']]['InwardedItems'] = $row['InwardedItems'];
			$ReportData[$row['brand']]['InwardedArticles'] = $row['InwardedArticles'];
		}
		
		
		// Last 7 Days
		$ReportData = $this->getBrandwiseSalesData($calculated_dates2[1]['start'],  $calculated_dates2[1]['end'],  $ReportData, $calculated_dates2[1]['label']);
		$dateLabels[] = $calculated_dates2[1]['label'];
		
		// MTD
		$ReportData = $this->getBrandwiseSalesData($calculated_dates1[13]['start'], $calculated_dates1[13]['end'], $ReportData, $calculated_dates1[13]['label']);
		$dateLabels[] = $calculated_dates1[13]['label'];
		
		// Previous Month
		$ReportData = $this->getBrandwiseSalesData($calculated_dates1[14]['start'], $calculated_dates1[14]['end'], $ReportData, $calculated_dates1[14]['label']);
		$dateLabels[] = $calculated_dates1[14]['label'];
		
		// Previous Previous Month
		$ReportData = $this->getBrandwiseSalesData($calculated_dates1[15]['start'], $calculated_dates1[15]['end'], $ReportData, $calculated_dates1[15]['label']);
		$dateLabels[] = $calculated_dates1[15]['label'];
		
		/*** Put all computed Report Data into Table format ***/
		$i=0;
		$table->setCellData($i, 0, "TOTAL");	// First Row for summarized total.
		
		$i++;
		$totalActiveSKUs = 0;
		$totalActiveStyles =0;
		$totalInventoryCountLiveOnPortal =0;
		$totalInventoryCountNOTLiveOnPortal =0;
		$totalInwardedItems =0;
		$totalInwardedArticles =0;
		
		$totalInventoryCountAll = 0;
		$totalLast7DaysSales = 0;
				
		foreach ($ReportData as $brand => $brandArr){
			$j=0;
			$inventoryCount = 0;
			$last7DaysSales = 0;
			
			$last7DaysLabel = $calculated_dates2[1]['label'];
			
			$table->setCellData($i, $j++, $brand);
			
			$table->setCellData($i, $j++, $brandArr['ActiveSKUs']);
			if(!empty($brandArr['ActiveSKUs'])) 
				$totalActiveSKUs+= $brandArr['ActiveSKUs'];
			
			$table->setCellData($i, $j++, $brandArr['ActiveStyles']);
			if(!empty($brandArr['ActiveStyles'])) 
				$totalActiveStyles+= $brandArr['ActiveStyles'];
				
			$table->setCellData($i, $j++, $brandArr['InventoryCountLiveOnPortal']);
			if(!empty($brandArr['InventoryCountLiveOnPortal'])) {
				$totalInventoryCountLiveOnPortal+= $brandArr['InventoryCountLiveOnPortal'];
				$inventoryCount+= $totalInventoryCountLiveOnPortal;
			}
				
			$table->setCellData($i, $j++, $brandArr['InventoryCountNOTLiveOnPortal']);
			if(!empty($brandArr['InventoryCountNOTLiveOnPortal'])) { 
				$totalInventoryCountNOTLiveOnPortal+= $brandArr['InventoryCountNOTLiveOnPortal'];
				$inventoryCount+= $totalInventoryCountNOTLiveOnPortal;
			}
			
			$table->setCellData($i, $j++, $brandArr['InwardedItems']);
			if(!empty($brandArr['InwardedItems'])) 
				$totalInwardedItems+= $brandArr['InwardedItems'];
				
			$table->setCellData($i, $j++, $brandArr['InwardedArticles']);
			if(!empty($brandArr['InwardedArticles'])) 
				$totalInwardedArticles+= $brandArr['InwardedArticles'];
			
			foreach ($dateLabels as $value) {
				$table->setCellData($i, $j++, $brandArr["ItemQuantity-$value"]);
				if(!empty($brandArr["ItemQuantity-$value"])) 
					$total["ItemQuantity-$value"]+= $brandArr["ItemQuantity-$value"];
				
				$table->setCellData($i, $j++, round($brandArr["NetReceivable-$value"]));
				if(!empty($brandArr["NetReceivable-$value"])) 
					$total["NetReceivable-$value"]+= $brandArr["NetReceivable-$value"];
			}
			
			if(!empty($brandArr["ItemQuantity-$last7DaysLabel"])) {
				$last7DaysSales = $brandArr["ItemQuantity-$last7DaysLabel"];
			}

			if(!empty($inventoryCount))
				$totalInventoryCountAll+= $inventoryCount;
				
			if(!empty($last7DaysSales))
				$totalLast7DaysSales+=$last7DaysSales;
			
			$weekCoverable = "-";
			if($last7DaysSales>0) {
				$weekCoverable = $inventoryCount/$last7DaysSales; 
			}
			
			$table->setCellData($i, $j++, $weekCoverable);
				
			$i++;
		}

		// Setting total values in footer..
		
		$j=0;
		$i=0; // Total on the first Row.
		$overallWeekCover = "-";
		if(!empty($totalLast7DaysSales)) {
			$overallWeekCover = $totalInventoryCountAll / $totalLast7DaysSales;
		}
		$table->setCellData($i, $j++, "TOTAL : ");
		$table->setCellData($i, $j++, $totalActiveSKUs);
		$table->setCellData($i, $j++, $totalActiveStyles);
		$table->setCellData($i, $j++, $totalInventoryCountLiveOnPortal);
		$table->setCellData($i, $j++, $totalInventoryCountNOTLiveOnPortal);
		$table->setCellData($i, $j++, $totalInwardedItems);
		$table->setCellData($i, $j++, $totalInwardedArticles);
		foreach ($dateLabels as $value) {
			$table->setCellData($i, $j++, $total["ItemQuantity-$value"]);
			$table->setCellData($i, $j++, round($total["NetReceivable-$value"]));
		}
		
		$table->setCellData($i, $j++, round($overallWeekCover));
		
		$group->addTable($table);
		$this->addTableGroup($group);
		$this->isBuild=true;
		
		unset($ReportData);
	}
}
?>

