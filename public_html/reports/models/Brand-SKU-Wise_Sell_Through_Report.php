<?php

global $xcart_dir;
include_once $xcart_dir.'/reports/models/models.php';
include_once $xcart_dir.'/reports/rendrers/rendrers.php';
include_once REP_DIR.'/models/date_calculation.php';

class BrandSKUSellThroughReportModel extends ReportModel {

	protected $brand;
	public function __construct($brand){
		parent::__construct();
		$this->name=$brand . " SKU-Wise Sell Through Report : " . date("j-M-Y");
		$this->brand = $brand;
	}

	protected function initFilters(){
		$this->addFilter("Date:" ,self::$FILTER_TYPE_DATE);
	}

	public function loadDefaultFilterValues(){
		$time=time();
		$this->setFilterValue("Date:", date("m/d/Y",$time));
	}

	public function summaryInfo($calculated_dates){
		$table= new Table('Summary');
		$superHeaders = array('Last Week'=>2,'Growth % Over Last Week'=>2, 'MTD'=>2, 'QTD'=>2);
		$headers = array('Qty', 'Val', 'Qty', 'Val', 'Qty', 'Val', 'Qty', 'Val');
		$table->setFooterVisible(false);
		//this will select only the required headers for this table, i.e. Last Week(Mon-Sun), Last to Last Week(Mon-Sun), MTD, QTD
		$calculated_dates_index = array(9,10,13,17);

		$j = 0;
		for($cnt=0;$cnt<count($calculated_dates_index);$cnt++){
			$result = func_query("select sum(mv.amount) as qty , sum(mv.amount * mv.price) as val from (select  od.amount, (select price from mk_product_style where id = od.product_style) as price from xcart_orders o join xcart_order_details od on od.orderid=o.orderid and
			o.date >= unix_timestamp('" .$calculated_dates[$calculated_dates_index[$cnt]]['start'] . "') and o.date < unix_timestamp('" .$calculated_dates[$calculated_dates_index[$cnt]]['end'] . "')
            and o.status in ('WP','Q','OH','SH','C','DL') join mk_style_properties sp on sp.style_id = od.product_style 
            where sp.global_attr_brand = '" . $this->brand . "') mv", true);
			if($cnt==1){ //logic to find out growth % over last week
				$qty_data = ($temp_qty - (float)$result[0]['qty'])*100/(float)$result[0]['qty'];
				$val_data = ($temp_val - (float)$result[0]['val'])*100/(float)$result[0]['val'];
				$table->setCellData(0, $j++, $qty_data);
				$table->setCellData(0, $j++, $val_data);
			}
			else{
				$temp_qty = (float)$result[0]['qty'];
				$temp_val = (float)$result[0]['val'];
				$table->setCellData(0, $j++, $temp_qty);
				$table->setCellData(0, $j++, $temp_val);
			}
		}
		//$table->setTableWidth(count($headers));
		$table->setSuperHeaders($superHeaders);
		$table->setHeaders($headers);
		return $table;
	}

	public function buildReport(){
		global $weblog;
		$table= new Table('SKU Wise Details');
		$group= new TableGroup($this->brand . " SKU Sales");
		$headers= array('Master Category','Article Type','Article Name','Article Code','Brand','Price','Gender','Size',
		'Current Inventory Count','Sales(qty) - Week','Sales(revenue) - Week','Cover Stock - Weekly Sales (Weeks)');
		$table->setHeaders($headers);
		$table->setFooterVisible(false);
		$filtervalue = $this->getFilterValue("Date:");
		$date_split=split('/', $filtervalue);
		$current_month=(INT)$date_split[0];
		$current_year=(INT)$date_split[2];
		$current_date=(INT)$date_split[1];
		$ReportData;
		//function to calculate the end and start day.
		$calculated_dates = calculateDates($current_date, $current_month, $current_year);
		//this will select only the required headers for this table, i.e. Last Week(Mon-Sun)
		$calculated_dates_index = 9;

		$sql = "select sp.style_id, osm.option_id, po.value, (select typename from mk_catalogue_classification where id=sp.global_attr_master_category) masname,
				(select typename from mk_catalogue_classification where id=sp.global_attr_article_type) articletype, ps.name, sp.article_number, sp.global_attr_brand, ps.price, sp.global_attr_gender
                from mk_style_properties sp
				left join mk_product_style ps on ps.id=sp.style_id join mk_styles_options_skus_mapping osm  join mk_skus s join mk_product_options po
                on ps.id=osm.style_id and osm.sku_id=s.id and osm.option_id=po.id where sp.global_attr_brand = '" . $this->brand ."'";

		$result = func_query($sql, true);
		foreach ($result as $row ){
			$ReportData[$row['style_id']."-".$row['option_id']]['master']=$row['masname'];
			$ReportData[$row['style_id']."-".$row['option_id']]['articletype']=$row['articletype'];
			$ReportData[$row['style_id']."-".$row['option_id']]['name']=$row['name'];
			$ReportData[$row['style_id']."-".$row['option_id']]['article_num']=$row['article_number'];
			$ReportData[$row['style_id']."-".$row['option_id']]['brand']=$row['global_attr_brand'];
			$ReportData[$row['style_id']."-".$row['option_id']]['price']=$row['price'];
			$ReportData[$row['style_id']."-".$row['option_id']]['gender']=$row['global_attr_gender'];
			$ReportData[$row['style_id']."-".$row['option_id']]['size']=$row['value'];
		}
			$sql = "select m.style_id, m.option_id, taba.invCount from
	                    (select sku_id, sum(inv_count) as invCount from core_inventory_counts cic group by sku_id) taba join mk_styles_options_skus_mapping m on m.sku_id=taba.sku_id ";

		$result = func_query($sql, true);
		foreach ($result as $row ){
			if(isset($ReportData[$row['style_id']."-".$row['option_id']]))
				$ReportData[$row['style_id']."-".$row['option_id']]['inv_count']=$row['invCount'];
		}
		
		$sql="select od.product_style,oio.optionid,sum(oio.quantity ) as total from xcart_orders o join xcart_order_details od on od.orderid=o.orderid and
			o.date >= unix_timestamp('". $calculated_dates[$calculated_dates_index]['start'] ."') and o.date < unix_timestamp('".$calculated_dates[$calculated_dates_index]['end'] ."') and o.status in ('WP','Q','OH','SH','C','DL') join 
			mk_order_item_option_quantity oio on oio.itemid=od.itemid group by od.product_style,oio.optionid";
		$result = func_query($sql, true);
		foreach ($result as $row ){
			if(isset($ReportData[$row['product_style']."-".$row['optionid']]))
				$ReportData[$row['product_style']."-".$row['optionid']]['7days-sale']=$row['total'];
		}

		$i=0;
		foreach($ReportData as $info){
			$j=0;
			$table->setCellData($i, $j++, $info['master']);
			$table->setCellData($i, $j++, $info['articletype']);
			$table->setCellData($i, $j++, $info['name']);
			$table->setCellData($i, $j++, $info['article_num']);
			$table->setCellData($i, $j++, $info['brand']);
			$table->setCellData($i, $j++, (float)$info['price']);
			$table->setCellData($i, $j++, $info['gender']);
			$table->setCellData($i, $j++, $info['size']);
			$table->setCellData($i, $j++, $info['inv_count']);
			$table->setCellData($i, $j++, $info['7days-sale']);
			$table->setCellData($i, $j++, (float)$info['price']*$info['7days-sale']);
			$table->setCellData($i, $j++, (float)$info['inv_count']/(float)$info['7days-sale']);
			$i++;
		}
		$table->setTableWidth(count($headers));

		//make other table
		$summary_table = $this->summaryInfo($calculated_dates);

		$group->addTable($summary_table);
		$group->addTable($table);
		$this->addTableGroup($group);
		$this->isBuild=true;
		unset($ReportData);
		unset($info);
		unset($result);

	}


}
?>

