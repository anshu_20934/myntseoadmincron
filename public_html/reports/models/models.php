<?php
setlocale(LC_MONETARY, 'en_IN');
class Table{

	public static $STYLE_FONT_FAMILY='font';
	public static $STYLE_COLOR='color';
	public static $STYLE_BG_COLOR='bgcolor';
	public static $STYLE_EXCEL_CONDITIONAL_FORMATTING='excel_conditional_formating';
	public static $STYLE_EXCEL_FORMULA='FORMULA';
	public static $STYLE_BORDER='border';
	public static $STYLE_ALIGN='align';
	public static $ROWSPAN='rowspan';
	public static $FORMAT='FORMAT';
	public static $HYPERLINK='hyperlink';
	public static $FORMATTING_CONDITION='CON';
	public static $GREATER_THAN='GT';
	public static $LESS_THAN='LT';
	public static $BETWEEN='BET';
	public static $EQUALS='EQU';
	public static $VALUE='VAL';
	protected $matrix;//[i][j]
	protected $styleMatrix;
	protected $headers=array();
	protected $superHeaders=array();
	protected $footer=array();
	protected $maxI;
	protected $maxJ;
	protected $footer_visible=true;
	protected $style_enabled=false;
	protected $table_width;
	protected $name;

	public function __construct($name=null, $width=1,$headers=null,$matrix=null){
		$this->table_width=$width;
		$this->name=$name;
		if($headers!=null){
			$this->headers=$headers;
		}else{
			for($j=0;$j<$this->table_width;$j++){
				$this->headers[$j]=null;
				$this->footer[$j]=null;
			}
		}
		if($matrix!=null){
			$this->matrix=$matrix;
		}
	}

	private function addNewRow($i){
		for($j=0;$j<$this->table_width;$j++){
			$this->matrix[$i][$j]=null;
		}
	}

	public function getName(){
		return $this->name;
	}
	
	private function addNewRowHavingSpan($i){
		for($j=0;$j<$this->table_width-1;$j++){
			$this->matrix[$i][$j]=null;
		}
	}

	private function addNewStyleRow($i){
		for($j=0;$j<$this->table_width;$j++){
			$this->styleMatrix[$i][$j]=null;
		}
	}

	public function isStyleEnabled(){
		return $this->style_enabled;
	}

	public function setStyleEnabled($enable){
		if($enable){
			$this->style_enabled=true;
			$styleMatrix=array();
		}else{
			$this->style_enabled=false;
			$styleMatrix=null;
		}
	}

	public function setCellStyle($i,$j,$prop,$prop_value){
		if(!isset($this->styleMatrix[$i])||$this->styleMatrix[$i]==null){
			$this->addNewStyleRow($i);
		}
		$this->styleMatrix[$i][$j][$prop]=$prop_value;
	}

	public function setRowStyle($i,$prop,$prop_value){
		if(!isset($this->styleMatrix[$i])||$this->styleMatrix[$i]==null){
			$this->addNewStyleRow($i);
		}
		for($j=0;$j<$this->table_width;$j++){
			$this->styleMatrix[$i][$j][$prop]=$prop_value;
		}
	}

	public function setCellData($i,$j,$value, $isRowSpanned='n'){
		if(!isset($this->matrix[$i])||$this->matrix[$i]==null){
			if($isRowSpanned == 'y'){
				$this->addNewRowHavingSpan($i);
			}
			else{
				$this->addNewRow($i);
			}
		}
		$this->matrix[$i][$j]=$value;
		if(!is_string($value))
		$this->footer[$j]+=$value;
		if($i>$this->maxI)
		$this->maxI=$i;
		if($isRowSpanned == 'n'){ //dont increase the max. col. in case rowspan is present as the next rows will get an extra col for every rowspan.
			if($j>$this->maxJ)
			$this->maxJ=$j;
		}
	}

	public function getCellData($i,$j){
		return $this->matrix[$i][$j];
	}
	public function getColumnsCount(){
		return $this->maxJ;
	}
	public function getRowsCount(){
		return $this->maxI;
	}
	public function getHeaders(){
		return $this->headers;
	}
	public function setHeaders($headers){
		$this->headers=$headers;
		foreach ($headers as $k=>$v){
			$this->footer[$k]=null;
		}
	}

	public function setHeaderValue($index,$value){
		$this->headers[$index]=$value;
	}

	//$superHeaders argument should be an array of key, value pairs with superheader names as keys and colspan as values.
	public function setSuperHeaders($superHeaders){
		$this->superHeaders=$superHeaders;
	}

	public function getSuperHeaders(){
		return $this->superHeaders;
	}

	public function setColumnData($j,$columnData){
		$i=0;
		foreach ($columnData as $cell){
			$this->matrix[$i++][$j]=$cell;
			if(!is_string($cell))
			$this->footer[$j]+=$cell;
		}
	}
	public function setRowData($i,$rowData){
		$j=0;
		foreach ($rowData as $cell){
			if(!is_string($cell))
			$this->footer[$j]+=$cell;
			$this->matrix[$i][$j++]=$cell;
		}
	}

	public function  getRow($rowIndex){
		return $this->matrix[$rowIndex];
	}
	public function  getAllRows(){
		return $this->matrix;
	}

	public function getFooter(){
		return $this->footer;
	}

	public function setFooterValue($index,$value){
		$this->footer[$index]=$value;
	}

	public function setFooterVisible($bool){
		$this->footer_visible=$bool;
	}

	public function isFooterVisible(){
		return $this->footer_visible;
	}

	public function getStyleMatrix(){
		return $this->styleMatrix;
	}

	public function setStyleMatrix($startrowindex, $startcolindex, $endrowindex, $endcolindex, $prop, $prop_value){
		for($i=$startrowindex ; $i<=$endrowindex ;$i++){
			if(!isset($this->styleMatrix[$i])||$this->styleMatrix[$i]==null)
			$this->addNewStyleRow($i);
			for($j=$startcolindex ; $j<=$endcolindex ; $j++){
				$this->styleMatrix[$i][$j][$prop]=$prop_value;
			}
		}
	}

	public function setTableWidth($width){
		$this->table_width = $width;
	}
}

class TableGroup{
	protected $name;
	protected $tables=array();
	private $tableCount=0;

	public function __construct($name){
		$this->name=$name;
	}
	public function getTableCount(){
		return $this->tableCount;
	}
	public function getTable($tableIndex){
		return $this->tables[$tableIndex];
	}
	public function addTable($table){
		$this->tables[$this->tableCount++]=$table;
	}
	public function getName(){
		return $this->name;
	}
	public function getTables(){
		return $this->tables;
	}
}

interface IReportModel{
	public function buildReport();
	public function getTableGroupCount();
	public function getTableGroup($groupIndex);
	public function getFilters();
	public function loadDefaultFilterValues();
	public function setFilterValue($name,$value);
	public function getFilterValue($name);
}

abstract class ReportModel implements IReportModel{
	public static $FILTER_TYPE_DATE="DAT";
	protected $name;
	protected $notes;
	protected $tablesGroups=array();
	protected $isBuild=false;
	protected $tableGroupLength=0;
	protected $reportConfig;
	protected $filters=array();

	public function __construct(){
		$this->initFilters();
		$this->loadDefaultFilterValues();
		//$this->loadFilterValuesFromRequest();
	}

	public function getTableGroupCount(){
		return $this->tableGroupLength;
	}
	public function getTableGroup($groupIndex){
		if($this->isBuild){
			return $this->tablesGroups[$groupIndex];
		}
		return null;
	}

	public function getReportName(){
		return $this->name;
	}

	public function getReportNotes(){
		return $this->notes;
	}

	public function getReportConfig(){
		return $this->reportConfig;
	}

	protected function addTableGroup($group){
		$this->tablesGroups[$this->tableGroupLength++]=$group;
	}
	public function getTableGroups(){
		return $this->tablesGroups;
	}
	public function getFilters(){
		return $this->filters;
	}
	protected function addFilter($name,$type){
		$this->filters[str_replace(" ","_",$name)]['type']=$type;
		$this->filters[str_replace(" ","_",$name)]['name']=$name;
	}
	public function setFilterValue($name,$value){
		$this->filters[str_replace(" ","_",$name)]['value']=$value;
	}
	public function getFilterValue($name){
		return $this->filters[str_replace(" ","_",$name)]['value'];
	}
	abstract protected function initFilters();

	public function loadFilterValuesFrom($get,$post){
		foreach ($this->filters as $name=> $filter){
			if(isset($get[$name]))
			$this->filters[$name]['value']=$get[$name];
			if(isset($post[$name])&&!isset($get[$name])){
				$this->filters[$name]['value']=$post[$name];
			}
		}
	}


}



function indian_rs_format($number, $decimals ,$format=true) {
	if(!$format)
	return round($number,$decimals);
	$format=number_format($number, 0, '.', ',');
	$strsplit=split(',',$format);
	$out=$strsplit[count($strsplit)-1];
	$icount=0;
	for($i=count($strsplit)-2;$i>=0;$i--){
		for($j=strlen($strsplit[$i])-1;$j>=0;$j--,$icount++){
			if($icount%2==0){
				$out=','.$out;
			}
			$out=$strsplit[$i][$j].$out;

		}
	}
	return $out;
}

?>