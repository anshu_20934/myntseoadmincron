<?php
global $xcart_dir;
include_once $xcart_dir.'/reports/models/models.php';
include_once $xcart_dir.'/reports/rendrers/rendrers.php';

class PaymentOptionsPerformance extends ReportModel {
	
	public function __construct() {
		parent::__construct();
		$this->name="Payment Performance Report";
	}
	
	protected function initFilters() {
		$this->addFilter("Date:" ,self::$FILTER_TYPE_DATE);
	}

	public function loadDefaultFilterValues() {
		$time=time();
		$this->setFilterValue("Date:", date("m/d/Y",$time));
	}
	
	public function getOrdersCountReport($time) {
		$table= new Table('Orders Payments Details');
		$headers = array('Total Orders', 'Cod', 'Online', 'Total Completed', 'Total Declined', 'Total PP/PV', 'Avg. Return Time');
		$table->setHeaders($headers);
		$table->setTableWidth(count($headers));
		$table->setFooterVisible(false);
		$sql = "select count(*) from mk_payments_log where time_insert>($time)";
		$totalOrders = func_query_first_cell($sql, true);
		$sql = "select count(*) from mk_payments_log where time_insert>($time) and payment_option='cod'";
		$totalCODOrders = func_query_first_cell($sql, true);
		$sql = "select count(*) from mk_payments_log where time_insert>($time) and payment_option!='cod'";
		$totalOnlineOrders = func_query_first_cell($sql, true);		
		$sql = "select count(*) from mk_payments_log where time_insert>($time) and is_complete=1";
		$totalOrdersCompleted = func_query_first_cell($sql, true);
		$sql = "select count(*) from mk_payments_log where time_insert>($time) and is_complete =0";
		$totalOrdersDeclined = func_query_first_cell($sql, true);
		$sql = "select count(*) from mk_payments_log where time_insert>($time) and is_complete is null";
		$totalOrdersLost = func_query_first_cell($sql, true);
		$sql = "select avg(time_return-time_insert) from mk_payments_log where time_insert>($time) and (is_complete is not null)";
		$avgReturnTime = func_query_first_cell($sql, true);
		
		$table->setCellData(0, 0, $totalOrders);
		$table->setCellData(0, 1, $totalCODOrders);
		$table->setCellData(0, 2, $totalOnlineOrders);
		$table->setCellData(0, 3, $totalOrdersCompleted);
		$table->setCellData(0, 4, $totalOrdersDeclined);
		$table->setCellData(0, 5, $totalOrdersLost);
		$table->setCellData(0, 6, $avgReturnTime);
		return $table;
	}
	
	public function getCCAvenueCountReport($time) {
		$table= new Table('CCAvenue Payments');
		$headers = array('Total Orders', 'Total Completed', 'Total Declined','Total PP','Avg. Confirmation Time','Avg. Decline Time');
		$table->setHeaders($headers);
		$table->setTableWidth(count($headers));
		$table->setFooterVisible(false);
		$sql = "select count(*) from mk_payments_log where time_insert>($time) and payment_gateway_name='ccavenue'";
		$totalOrders = func_query_first_cell($sql, true);
		$sql = "select count(*) from mk_payments_log where time_insert>($time) and payment_gateway_name='ccavenue' and is_complete=1";
		$totalOrdersCompleted = func_query_first_cell($sql, true);
		$sql = "select count(*) from mk_payments_log where time_insert>($time) and payment_gateway_name='ccavenue' and is_complete =0";
		$totalOrdersDeclined = func_query_first_cell($sql, true);
		$sql = "select count(*) from mk_payments_log where time_insert>($time) and payment_gateway_name='ccavenue' and is_complete is null";
		$totalOrdersLost = func_query_first_cell($sql, true);
		$sql = "select avg(time_return-time_insert) from mk_payments_log where time_insert>($time) and payment_gateway_name='ccavenue' and (is_complete=1)";
		$avgConfirmTime = func_query_first_cell($sql, true);
		$sql = "select avg(time_return-time_insert) from mk_payments_log where time_insert>($time) and payment_gateway_name='ccavenue' and (is_complete=0)";
		$avgDeclineTime = func_query_first_cell($sql, true);
		$table->setCellData(0, 0, $totalOrders);
		$table->setCellData(0, 1, $totalOrdersCompleted);
		$table->setCellData(0, 2, $totalOrdersDeclined);
		$table->setCellData(0, 3, $totalOrdersLost);
		$table->setCellData(0, 4, $avgConfirmTime);
		$table->setCellData(0, 5, $avgDeclineTime);
		
		$sql = "select distinct(payment_issuer) as bank_name,is_complete,count(*) as count,avg(time_return-time_insert) as avgtime  from mk_payments_log where time_insert>($time) and payment_gateway_name='ccavenue' group by payment_issuer,is_complete";
		$distinctBank = func_query($sql, true);
		$detailsData=array();
		$k = 0;
		foreach ($distinctBank as $row){
			if(empty($row['bank_name'])){
				$row['bank_name'] = 'credit card';
			}
			
			if($row[is_complete]=='1') {
				//we have reached end for this bank
				$detailsData[$row['bank_name']]['completecount']=$row['count'];
				$detailsData[$row['bank_name']]['completeavgtime']=$row['avgtime'];
			} else if($row[is_complete]=='0') {
				//we may have another entry where is_complete=1 for this bank
				$detailsData[$row['bank_name']]['declinecount']=$row['count'];
				$detailsData[$row['bank_name']]['declineavgtime']=$row['avgtime'];
			} else {
				//we may have another enrty where is_complete=1 or 0 for this bank
				$detailsData[$row['bank_name']]['ppcount']=$row['count'];
			}
		}
		$i = 1;
		$rowIdentifiers = array('bank_name', 'completecount', 'declinecount', 'ppcount', 'completeavgtime', 'declineavgtime');
		foreach ($detailsData as $key => $rowData){
			$table->setCellData($i, 0, $key);
			for ($j=1; $j<count($headers); $j++){
				if(isset($rowData[$rowIdentifiers[$j]])) $table->setCellData($i, $j, $rowData[$rowIdentifiers[$j]]);
				else $table->setCellData($i, $j, 0);
			}
			$i++;
		}
		return $table;
	}
	
	public function getEBSCountReport($time) {
		$table= new Table('EBS Payments');
		$headers = array('Total Orders', 'Total Completed', 'Total Declined','Total PP','Avg. Confirmation Time','Avg. Decline Time');
		$table->setHeaders($headers);
		$table->setTableWidth(count($headers));
		$table->setFooterVisible(false);
		$sql = "select count(*) from mk_payments_log where time_insert>($time) and payment_gateway_name='ebs'";
		$totalOrders = func_query_first_cell($sql, true);
		$sql = "select count(*) from mk_payments_log where time_insert>($time) and payment_gateway_name='ebs' and is_complete=1";
		$totalOrdersCompleted = func_query_first_cell($sql, true);
		$sql = "select count(*) from mk_payments_log where time_insert>($time) and payment_gateway_name='ebs' and is_complete =0";
		$totalOrdersDeclined = func_query_first_cell($sql, true);
		$sql = "select count(*) from mk_payments_log where time_insert>($time) and payment_gateway_name='ebs' and is_complete is null";
		$totalOrdersLost = func_query_first_cell($sql, true);
		$sql = "select avg(time_return-time_insert) from mk_payments_log where time_insert>($time) and payment_gateway_name='ebs' and (is_complete=1)";
		$avgConfirmTime = func_query_first_cell($sql, true);
		$sql = "select avg(time_return-time_insert) from mk_payments_log where time_insert>($time) and payment_gateway_name='ebs' and (is_complete=0)";
		$avgDeclineTime = func_query_first_cell($sql, true);
		$table->setCellData(0, 0, $totalOrders);
		$table->setCellData(0, 1, $totalOrdersCompleted);
		$table->setCellData(0, 2, $totalOrdersDeclined);
		$table->setCellData(0, 3, $totalOrdersLost);
		$table->setCellData(0, 4, $avgConfirmTime);
		$table->setCellData(0, 5, $avgDeclineTime);
		
		$sql = "select distinct(payment_issuer) as bank_name,is_complete,count(*) as count,avg(time_return-time_insert) as avgtime  from mk_payments_log where time_insert>($time) and payment_gateway_name='ebs' group by payment_issuer,is_complete";
		$distinctBank = func_query($sql, true);
		$detailsData=array();
		$k = 0;
		foreach ($distinctBank as $row){
			if(empty($row['bank_name'])){
				$row['bank_name'] = 'credit card';
			}
			
			if($row[is_complete]=='1') {
				//we have reached end for this bank
				$detailsData[$row['bank_name']]['completecount']=$row['count'];
				$detailsData[$row['bank_name']]['completeavgtime']=$row['avgtime'];
			} else if($row[is_complete]=='0') {
				//we may have another entry where is_complete=1 for this bank
				$detailsData[$row['bank_name']]['declinecount']=$row['count'];
				$detailsData[$row['bank_name']]['declineavgtime']=$row['avgtime'];
			} else {
				//we may have another enrty where is_complete=1 or 0 for this bank
				$detailsData[$row['bank_name']]['ppcount']=$row['count'];
			}
		}
		$i = 1;
		$rowIdentifiers = array('bank_name', 'completecount', 'declinecount', 'ppcount', 'completeavgtime', 'declineavgtime');
		foreach ($detailsData as $key => $rowData){
			$table->setCellData($i, 0, $key);
			for ($j=1; $j<count($headers); $j++){
				if(isset($rowData[$rowIdentifiers[$j]])) $table->setCellData($i, $j, $rowData[$rowIdentifiers[$j]]);
				else $table->setCellData($i, $j, 0);
			}
			$i++;
		}
		
		return $table;
	}
	
public function getTekprocessCountReport($time) {
		$table= new Table('TekProcess Payments');
		$headers = array('Total Orders', 'Total Completed', 'Total Declined','Total PP','Avg. Confirmation Time','Avg. Decline Time');
		$table->setHeaders($headers);
		$table->setTableWidth(count($headers));
		$table->setFooterVisible(false);
		$sql = "select count(*) from mk_payments_log where time_insert>($time) and payment_gateway_name='tekprocess'";
		$totalOrders = func_query_first_cell($sql, true);
		$sql = "select count(*) from mk_payments_log where time_insert>($time) and payment_gateway_name='tekprocess' and is_complete=1";
		$totalOrdersCompleted = func_query_first_cell($sql, true);
		$sql = "select count(*) from mk_payments_log where time_insert>($time) and payment_gateway_name='tekprocess' and is_complete =0";
		$totalOrdersDeclined = func_query_first_cell($sql, true);
		$sql = "select count(*) from mk_payments_log where time_insert>($time) and payment_gateway_name='tekprocess' and is_complete is null";
		$totalOrdersLost = func_query_first_cell($sql, true);
		$sql = "select avg(time_return-time_insert) from mk_payments_log where time_insert>($time) and payment_gateway_name='tekprocess' and (is_complete=1)";
		$avgConfirmTime = func_query_first_cell($sql, true);
		$sql = "select avg(time_return-time_insert) from mk_payments_log where time_insert>($time) and payment_gateway_name='tekprocess' and (is_complete=0)";
		$avgDeclineTime = func_query_first_cell($sql, true);
		$table->setCellData(0, 0, $totalOrders);
		$table->setCellData(0, 1, $totalOrdersCompleted);
		$table->setCellData(0, 2, $totalOrdersDeclined);
		$table->setCellData(0, 3, $totalOrdersLost);
		$table->setCellData(0, 4, $avgConfirmTime);
		$table->setCellData(0, 5, $avgDeclineTime);
		
		$sql = "select distinct(payment_issuer) as bank_name,is_complete,count(*) as count,avg(time_return-time_insert) as avgtime  from mk_payments_log where time_insert>($time) and payment_gateway_name='tekprocess' group by payment_issuer,is_complete";
		$distinctBank = func_query($sql, true);
		$detailsData=array();
		$k = 0;
		foreach ($distinctBank as $row){
			if(empty($row['bank_name'])){
				$row['bank_name'] = 'credit card';
			}
			
			if($row[is_complete]=='1') {
				//we have reached end for this bank
				$detailsData[$row['bank_name']]['completecount']=$row['count'];
				$detailsData[$row['bank_name']]['completeavgtime']=$row['avgtime'];
			} else if($row[is_complete]=='0') {
				//we may have another entry where is_complete=1 for this bank
				$detailsData[$row['bank_name']]['declinecount']=$row['count'];
				$detailsData[$row['bank_name']]['declineavgtime']=$row['avgtime'];
			} else {
				//we may have another enrty where is_complete=1 or 0 for this bank
				$detailsData[$row['bank_name']]['ppcount']=$row['count'];
			}
		}
		$i = 1;
		$rowIdentifiers = array('bank_name', 'completecount', 'declinecount', 'ppcount', 'completeavgtime', 'declineavgtime');
		foreach ($detailsData as $key => $rowData){
			$table->setCellData($i, 0, $key);
			for ($j=1; $j<count($headers); $j++){
				if(isset($rowData[$rowIdentifiers[$j]])) $table->setCellData($i, $j, $rowData[$rowIdentifiers[$j]]);
				else $table->setCellData($i, $j, 0);
			}
			$i++;
		}
		
		return $table;
	}
	
	
	public function getGatewayCountReport($gateway,$time) {
		$table= new Table("$gateway Payments");
		$headers = array('Total Orders', 'Total Completed', 'Total Declined','Total PP','Avg. Confirmation Time','Avg. Decline Time');
		$table->setHeaders($headers);
		$table->setTableWidth(count($headers));
		$table->setFooterVisible(false);
		$sql = "select count(*) from mk_payments_log where time_insert>($time) and payment_gateway_name='$gateway'";
		$totalOrders = func_query_first_cell($sql, true);
		$sql = "select count(*) from mk_payments_log where time_insert>($time) and payment_gateway_name='$gateway' and is_complete=1";
		$totalOrdersCompleted = func_query_first_cell($sql, true);
		$sql = "select count(*) from mk_payments_log where time_insert>($time) and payment_gateway_name='$gateway' and is_complete=0";
		$totalOrdersDeclined = func_query_first_cell($sql, true);
		$sql = "select count(*) from mk_payments_log where time_insert>($time) and payment_gateway_name='$gateway' and is_complete is null";
		$totalOrdersLost = func_query_first_cell($sql, true);
		$sql = "select avg(time_return-time_insert) from mk_payments_log where time_insert>($time) and payment_gateway_name='$gateway' and (is_complete=1)";
		$avgConfirmTime = func_query_first_cell($sql, true);
		$sql = "select avg(time_return-time_insert) from mk_payments_log where time_insert>($time) and payment_gateway_name='$gateway' and (is_complete=0)";
		$avgDeclineTime = func_query_first_cell($sql, true);
		$table->setCellData(0, 0, $totalOrders);
		$table->setCellData(0, 1, $totalOrdersCompleted);
		$table->setCellData(0, 2, $totalOrdersDeclined);
		$table->setCellData(0, 3, $totalOrdersLost);
		$table->setCellData(0, 4, $avgConfirmTime);
		$table->setCellData(0, 5, $avgDeclineTime);
		
		$sql = "select distinct(card_bank_name) as bank_name,is_complete,count(*) as count,avg(time_return-time_insert) as avgtime  from mk_payments_log where time_insert>($time) and payment_gateway_name='$gateway' group by card_bank_name,is_complete";
		$distinctBank = func_query($sql, true);
		$detailsData=array();
		$k = 0;
		foreach ($distinctBank as $row){
			if($row[is_complete]=='1') {
				//we have reached end for this bank
				$detailsData[$row['bank_name']]['completecount']=$row['count'];
				$detailsData[$row['bank_name']]['completeavgtime']=$row['avgtime'];
			} else if($row[is_complete]=='0') {
				//we may have another entry where is_complete=1 for this bank
				$detailsData[$row['bank_name']]['declinecount']=$row['count'];
				$detailsData[$row['bank_name']]['declineavgtime']=$row['avgtime'];
			} else {
				//we may have another enrty where is_complete=1 or 0 for this bank
				$detailsData[$row['bank_name']]['ppcount']=$row['count'];
			}
		}
		$i = 1;
		$rowIdentifiers = array('bank_name', 'completecount', 'declinecount', 'ppcount', 'completeavgtime', 'declineavgtime');
		foreach ($detailsData as $key => $rowData){
			$table->setCellData($i, 0, $key);
			for ($j=1; $j<count($headers); $j++){
				if(isset($rowData[$rowIdentifiers[$j]])) $table->setCellData($i, $j, $rowData[$rowIdentifiers[$j]]);
				else $table->setCellData($i, $j, 0);
			}
			$i++;
		}		
		
		return $table;
	}
	
	public function getNetbankingCountReport($time) {
		$table= new Table('NetBanking Payments');
		$headers = array('Total Orders', 'Total Completed', 'Total Declined','Total PP','Avg. Return Time');
		$table->setHeaders($headers);
		$table->setTableWidth(count($headers));
		$table->setFooterVisible(false);
		$sql = "select count(*) from mk_payments_log where time_insert>($time) and payment_option='netbanking'";
		$totalOrders = func_query_first_cell($sql, true);
		$sql = "select count(*) from mk_payments_log where time_insert>($time) and payment_option='netbanking' and is_complete=1";
		$totalOrdersCompleted = func_query_first_cell($sql, true);
		$sql = "select count(*) from mk_payments_log where time_insert>($time) and payment_option='netbanking' and is_complete =0";
		$totalOrdersDeclined = func_query_first_cell($sql, true);
		$sql = "select count(*) from mk_payments_log where time_insert>($time) and payment_option='netbanking' and is_complete is null";
		$totalOrdersLost = func_query_first_cell($sql, true);
		$sql = "select avg(time_return-time_insert) from mk_payments_log where time_insert>($time) and payment_option='netbanking' and (is_complete is not null)";
		$avgReturnTime = func_query_first_cell($sql, true);
		$table->setCellData(0, 0, $totalOrders);
		$table->setCellData(0, 1, $totalOrdersCompleted);
		$table->setCellData(0, 2, $totalOrdersDeclined);
		$table->setCellData(0, 3, $totalOrdersLost);
		$table->setCellData(0, 4, $avgReturnTime);
		
		
		
		return $table;
	}
	
	public function getCCCountReport($time) {
		$table= new Table('CreditCards Payments');
		$headers = array('Total Orders', 'Total Completed', 'Total Declined','Total PP','Avg. Return Time');
		$table->setHeaders($headers);
		$table->setTableWidth(count($headers));
		$table->setFooterVisible(false);
		$sql = "select count(*) from mk_payments_log where time_insert>($time) and payment_option='creditcards'";
		$totalOrders = func_query_first_cell($sql, true);
		$sql = "select count(*) from mk_payments_log where time_insert>($time) and payment_option='creditcards' and is_complete=1";
		$totalOrdersCompleted = func_query_first_cell($sql, true);
		$sql = "select count(*) from mk_payments_log where time_insert>($time) and payment_option='creditcards' and is_complete =0";
		$totalOrdersDeclined = func_query_first_cell($sql, true);
		$sql = "select count(*) from mk_payments_log where time_insert>($time) and payment_option='creditcards' and is_complete is null";
		$totalOrdersLost = func_query_first_cell($sql, true);
		$sql = "select avg(time_return-time_insert) from mk_payments_log where time_insert>($time) and payment_option='creditcards' and (is_complete is not null)";
		$avgReturnTime = func_query_first_cell($sql, true);
		$table->setCellData(0, 0, $totalOrders);
		$table->setCellData(0, 1, $totalOrdersCompleted);
		$table->setCellData(0, 2, $totalOrdersDeclined);
		$table->setCellData(0, 3, $totalOrdersLost);
		$table->setCellData(0, 4, $avgReturnTime);
		return $table;
	}
	
	public function getDCCountReport($time) {
		$table= new Table('DebitCard Payments');
		$headers = array('Total Orders', 'Total Completed', 'Total Declined','Total PP','Avg. Return Time');
		$table->setHeaders($headers);
		$table->setTableWidth(count($headers));
		$table->setFooterVisible(false);
		$sql = "select count(*) from mk_payments_log where time_insert>($time) and payment_option='debitcard'";
		$totalOrders = func_query_first_cell($sql, true);
		$sql = "select count(*) from mk_payments_log where time_insert>($time) and payment_option='debitcard' and is_complete=1";
		$totalOrdersCompleted = func_query_first_cell($sql, true);
		$sql = "select count(*) from mk_payments_log where time_insert>($time) and payment_option='debitcard' and is_complete =0";
		$totalOrdersDeclined = func_query_first_cell($sql, true);
		$sql = "select count(*) from mk_payments_log where time_insert>($time) and payment_option='debitcard' and is_complete is null";
		$totalOrdersLost = func_query_first_cell($sql, true);
		$sql = "select avg(time_return-time_insert) from mk_payments_log where time_insert>($time) and payment_option='debitcard' and (is_complete is not null)";
		$avgReturnTime = func_query_first_cell($sql, true);
		$table->setCellData(0, 0, $totalOrders);
		$table->setCellData(0, 1, $totalOrdersCompleted);
		$table->setCellData(0, 2, $totalOrdersDeclined);
		$table->setCellData(0, 3, $totalOrdersLost);
		$table->setCellData(0, 4, $avgReturnTime);
		return $table;
	}
	
	public function getCODCountReport($time) {
		$table= new Table('COD Payments');
		$headers = array('Total COD Orders', 'Total Completed', 'Total PV','Avg. Return Time');
		$table->setHeaders($headers);
		$table->setTableWidth(count($headers));
		$table->setFooterVisible(false);
		$sql = "select count(*) from mk_payments_log where time_insert>($time) and payment_option='cod'";
		$totalCODOrders = func_query_first_cell($sql, true);
		$sql = "select count(*) from mk_payments_log where time_insert>($time) and payment_option='cod' and is_complete=1";
		$totalCODOrdersCompleted = func_query_first_cell($sql, true);
		$sql = "select count(*) from mk_payments_log where time_insert>($time) and payment_option='cod' and (is_complete =0 or is_complete is null)";
		$totalCODOrdersLost = func_query_first_cell($sql, true);
		$sql = "select avg(time_return-time_insert) from mk_payments_log where time_insert>($time) and payment_option='cod' and (is_complete is not null)";
		$avgReturnTime = func_query_first_cell($sql, true);
		$table->setCellData(0, 0, $totalCODOrders);
		$table->setCellData(0, 1, $totalCODOrdersCompleted);
		$table->setCellData(0, 2, $totalCODOrdersLost);
		$table->setCellData(0, 3, $avgReturnTime);
		return $table;
	}
	
	public function buildReport() {
		global $weblog;
		$group= new TableGroup("Payments Performance Report");
		
		//make tables
		$time = time() - 3601; // time in seconds for which report needs to be generated. Currently one hour.
		$ordersCountTable = $this->getOrdersCountReport($time);
		$codCountTable = $this->getCODCountReport($time);
		$ccCountTable = $this->getCCCountReport($time);
		$dcCountTable = $this->getDCCountReport($time);
		$nbCountTable = $this->getNetbankingCountReport($time);
		$ccAvenueTable = $this->getCCAvenueCountReport($time);
		$ebsTable = $this->getEBSCountReport($time);
		$tekTable = $this->getTekprocessCountReport($time);
		
		$iciciTable = $this->getGatewayCountReport("icici", $time);		
		$icici3EMITable = $this->getGatewayCountReport("icici3emi", $time);
		$icici6EMITable = $this->getGatewayCountReport("icici6emi", $time);	
		$citi3EMITable = $this->getGatewayCountReport("citi3emi", $time);
		$citi6EMITable = $this->getGatewayCountReport("citi6emi", $time);
		$axisTable = $this->getGatewayCountReport("axis", $time);		
		
		$group->addTable($ordersCountTable);
		$group->addTable($ccAvenueTable);
		$group->addTable($ebsTable);
		$group->addTable($tekTable);
		$group->addTable($axisTable);
		$group->addTable($iciciTable);
		$group->addTable($icici3EMITable);
		$group->addTable($icici6EMITable);
		$group->addTable($citi3EMITable);
		$group->addTable($citi6EMITable);
		$group->addTable($codCountTable);
		$group->addTable($ccCountTable);
		$group->addTable($dcCountTable);
		$group->addTable($nbCountTable);
		$this->addTableGroup($group);

		$this->isBuild=true;
	}
	
}