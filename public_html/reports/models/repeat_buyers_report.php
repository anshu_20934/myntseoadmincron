<?php

global $xcart_dir;
include_once $xcart_dir.'/reports/models/models.php';
include_once $xcart_dir.'/reports/rendrers/rendrers.php';
include_once REP_DIR.'/models/date_calculation.php';

class RepeatBuyersReportModel extends ReportModel {

	public function __construct(){
		parent::__construct();
		$this->name="Repeat Buyers Report : " . date("j-M-Y");

	}
	protected function initFilters(){
		$this->addFilter("Date:" ,self::$FILTER_TYPE_DATE);
	}

	public function loadDefaultFilterValues(){
		$time=time();
		$this->setFilterValue("Date:", date("m/d/Y",$time));
	}


	public function buildReport(){
		global $weblog;
		$table= new Table();
		$group= new TableGroup("Repeat Buyers Report");
		$filtervalue = $this->getFilterValue("Date:");
		$date_split=split('/', $filtervalue);
		$current_month=(INT)$date_split[0];
		$current_year=(INT)$date_split[2];
		$current_date=(INT)$date_split[1];
		$calculated_dates = calculateDates($current_date, $current_month, $current_year);
		$superHeaders= array('Customer Details'=>3,$calculated_dates[14]['label']=>4,$calculated_dates[15]['label']=>4, $calculated_dates[16]['label']=>4);
		$headers= array('Name','Email-id','City','# repeat buys','Brand Affinity','Category Affinity', 'First Payment mode', '# repeat buys','Brand Affinity','Category Affinity', 'First Payment mode', '# repeat buys','Brand Affinity','Category Affinity', 'First Payment mode');
		$table->setSuperHeaders($superHeaders);
		$table->setHeaders($headers);
		$table->setTableWidth(count($headers));
		$table->setFooterVisible(false);
		$ReportData;
		
		for($cnt=14;$cnt<17;$cnt++){
			// To get customer details and no. of repeat buys
			$sql = "select concat(firstname, lastname) as custName, login, s_city, count(orderid) as orderCounts
					from xcart_orders where status in ('C','SH','DL') and date >=unix_timestamp('" .$calculated_dates[$cnt]['start'] . "') and  date <unix_timestamp('" .$calculated_dates[$cnt]['end'] . "') 
					group by login having orderCounts >=2";
			$result=func_query($sql, true);
			foreach ($result as $row){
				$ReportData[$row['login']]['Name']= $row['custName'];
				$ReportData[$row['login']]['Email']= $row['login'];
				$ReportData[$row['login']]['City']= $row['s_city'];
				$ReportData[$row['login']]['RepeatBuys'][$cnt-14]= $row['orderCounts'] - 1;
			}
		}
		for($cnt=14;$cnt<17;$cnt++){
			// To get brand affinity
			$sql = "select tabb.login, tabb.global_attr_brand as brandName from
					(select taba.login, taba.global_attr_brand, count(*) as brandBuys, sum(taba.brandRevenue) as brandRev from
					(select o.login, sp.global_attr_brand, (od.amount*od.price) as brandRevenue from  xcart_orders o join xcart_order_details od on o.orderid=od.orderid left join mk_style_properties sp on od.product_style=sp.style_id 
					where o.status in ('C','SH','DL') and o.date >=unix_timestamp('" .$calculated_dates[$cnt]['start'] . "') and  o.date <unix_timestamp('" .$calculated_dates[$cnt]['end'] . "') 
					) taba group by taba.login, taba.global_attr_brand order by login desc, brandBuys desc, brandRev desc) tabb group by tabb.login";
			$result=func_query($sql, true);
			foreach ($result as $row){
				if(isset($ReportData[$row['login']]))
					$ReportData[$row['login']]['LovedBrand'][$cnt-14]= $row['brandName'];
			}
			// To get category affinity
			$sql = "select tabb.login, (select typename from mk_catalogue_classification where id=tabb.global_attr_sub_category) as categoryName from
					(select taba.login, taba.global_attr_sub_category, count(*) as catBuys, sum(taba.catRevenue) as catRev from
					(select o.login, sp.global_attr_sub_category, (od.amount*od.price) as catRevenue from  xcart_orders o join xcart_order_details od on o.orderid=od.orderid left join mk_style_properties sp on od.product_style=sp.style_id 
					where o.status in ('C','SH','DL') and o.date >=unix_timestamp('" .$calculated_dates[$cnt]['start'] . "') and  o.date <unix_timestamp('" .$calculated_dates[$cnt]['end'] . "') 
					) taba group by taba.login, taba.global_attr_sub_category order by login desc, catBuys desc, catRev desc) tabb group by tabb.login";
			$result=func_query($sql, true);
			foreach ($result as $row){
				if(isset($ReportData[$row['login']]))
					$ReportData[$row['login']]['LovedCategory'][$cnt-14]= $row['categoryName'];
			}
			// To get first payment mode
			$sql = "select taba.login, taba.payment_method as paymethod  from
					(select o.login, o.payment_method, from_unixtime(o.date) as orderDate from  xcart_orders o
					where o.status in ('C','SH','DL') and o.date >=unix_timestamp('" .$calculated_dates[$cnt]['start'] . "') and  o.date <unix_timestamp('" .$calculated_dates[$cnt]['end'] . "') order by login, orderDate
					) taba group by taba.login";
			$result=func_query($sql, true);
			foreach ($result as $row){
				if(isset($ReportData[$row['login']]))
					$ReportData[$row['login']]['firstPayment'][$cnt-14]= $row['paymethod'];
			}			
		}
		
		$i=0;
		foreach($ReportData as $info){
			$j=0;
			$table->setCellData($i, $j++, $info['Name']);
			$table->setCellData($i, $j++, $info['Email']);
			$table->setCellData($i, $j++, $info['City']);
			$table->setCellData($i, $j++, isset($info['RepeatBuys'][0]) ? $info['RepeatBuys'][0] : 0);
			$table->setCellData($i, $j++, isset($info['LovedBrand'][0]) ? $info['LovedBrand'][0] : 0);
			$table->setCellData($i, $j++, isset($info['LovedCategory'][0]) ? $info['LovedCategory'][0] : 0);
			$table->setCellData($i, $j++, isset($info['firstPayment'][0]) ? $info['firstPayment'][0] : 0);
			$table->setCellData($i, $j++, isset($info['RepeatBuys'][1]) ? $info['RepeatBuys'][1] : 0);
			$table->setCellData($i, $j++, isset($info['LovedBrand'][1]) ? $info['LovedBrand'][1] : 0);
			$table->setCellData($i, $j++, isset($info['LovedCategory'][1]) ? $info['LovedCategory'][1] : 0);
			$table->setCellData($i, $j++, isset($info['firstPayment'][1]) ? $info['firstPayment'][1] : 0);
			$table->setCellData($i, $j++, isset($info['RepeatBuys'][2]) ? $info['RepeatBuys'][2] : 0);
			$table->setCellData($i, $j++, isset($info['LovedBrand'][2]) ? $info['LovedBrand'][2] : 0);
			$table->setCellData($i, $j++, isset($info['LovedCategory'][2]) ? $info['LovedCategory'][2] : 0);
			$table->setCellData($i, $j++, isset($info['firstPayment'][2]) ? $info['firstPayment'][2] : 0);
			$i++;
		}
		
		$group->addTable($table);
		$this->addTableGroup($group);
		$this->isBuild=true;
		
	}


}
?>

