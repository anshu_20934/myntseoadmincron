<?php

global $xcart_dir;
include_once $xcart_dir.'/reports/models/models.php';
include_once $xcart_dir.'/reports/rendrers/rendrers.php';
include_once REP_DIR.'/models/date_calculation.php';

class DeliveryDataReportModel extends ReportModel {
	
	protected $mode;
	public function __construct(){
		parent::__construct();
		$this->name="Delivery Data Report : " . date("j-M-Y");
		
	}
	protected function initFilters(){
		$this->addFilter("Date:" ,self::$FILTER_TYPE_DATE);
	}

	public function loadDefaultFilterValues(){
		$time=time();
		$this->setFilterValue("Date:", date("m/d/Y",$time));
	}

	public function ageingInfo($calculated_dates){
		$table= new Table('Avg Ageing of open orders - last 3 months');
		$headers = array('Courier Service', 'MTD <br> (avg. days)', 'M1 <br> (avg. days)', 'M2 <br> (avg. days)', 'Commulative <br> (avg. days)');
		$table->setHeaders($headers);
		$table->setTableWidth(count($headers));
		$table->setFooterVisible(false);
		$dates_required = array(13,14,15); //MTD, M1, M2
		$ReportData;
		for($cnt=0;$cnt<count($dates_required);$cnt++){
			$sql = "select courier_service, avg(unix_timestamp(now()) - shippeddate)/86400 as avgDays
					from xcart_orders where shippeddate is not null and delivereddate is null and shippeddate >= unix_timestamp('" . $calculated_dates[$dates_required[$cnt]]['start'] . "') and shippeddate < unix_timestamp('" . $calculated_dates[$dates_required[$cnt]]['end'] . "')
					and status = 'SH' group by courier_service";
			$result = func_query($sql, true);
			for($i=0;$i<count($result);$i++){
				$ReportData[$result[$i]['courier_service']][$cnt] = (float)$result[$i]['avgDays'];
			}
		}
		$sql = "select courier_service, avg(unix_timestamp(now()) - shippeddate)/86400 as avgDays
					from xcart_orders where shippeddate is not null and delivereddate is null and shippeddate >= unix_timestamp('" . $calculated_dates[15]['start'] . "') and shippeddate < unix_timestamp('" . $calculated_dates[13]['end'] . "')
					and status = 'SH' group by courier_service";
		$result_commulative = func_query($sql, true);
		for($i=0;$i<count($result_commulative);$i++){
			$ReportData[$result_commulative[$i]['courier_service']][$cnt] = (float)$result_commulative[$i]['avgDays'];
		}
		//query to calculate avgAgeing across all couries - monthwise
		for($cnt=0;$cnt<count($dates_required);$cnt++){
			$sql = "select avg(unix_timestamp(now()) - shippeddate)/86400 as avgDays from xcart_orders where shippeddate is not null and delivereddate 
				is null and shippeddate >unix_timestamp('" . $calculated_dates[$dates_required[$cnt]]['start'] . "') and shippeddate < unix_timestamp('" . $calculated_dates[$dates_required[$cnt]]['end'] . "') and status = 'SH'";
			$result_allCouriers = func_query($sql, true);
			$ReportData['Across all couriers'][$cnt] = (float)$result_allCouriers[0]['avgDays'];
		}
		//query to calculate avgAgeing across all couries - commulative
		$sql = "select avg(unix_timestamp(now()) - shippeddate)/86400 as avgDays from xcart_orders where shippeddate is not null and delivereddate 
				is null and shippeddate >unix_timestamp('" . $calculated_dates[15]['start'] . "') and shippeddate < unix_timestamp('" . $calculated_dates[13]['end'] . "') and status = 'SH'";
		$result_allCouriersCommulative = func_query($sql, true);
		$ReportData['Across all couriers'][3] = (float)$result_allCouriersCommulative[0]['avgDays'];
		$i=0;
		foreach($ReportData as $key=>$rowdata){
			$table->setCellData($i, 0, $key);
			for($cnt=0; $cnt<4; $cnt++){
				$table->setCellData($i, $cnt+1, (float)$rowdata[$cnt]);
			}
			$i++;
		}
		return $table;
	}
	
	public function deliveryByCourierInfo($calculated_dates){
		$table= new Table('Delivery performance by courier - last + current month');
		$headers = array('Delivery time bucket');
		$dates_required = array(13,14); //MTD, M1
		//query to find all the couriers active in given time duration. Need to set headers before any data cells because footer calculation doesnot allow otherwise. 
		$sql = "select distinct courier_service from xcart_orders where 
				delivereddate >= unix_timestamp('" . $calculated_dates[14]['start'] . "') and delivereddate < unix_timestamp('" . $calculated_dates[13]['end'] . "') order by courier_service";
		$result_couriers = func_query($sql, true);
		for($cnt=0;$cnt<count($result_couriers); $cnt++){
			$couriers[$cnt] = $result_couriers[$cnt]['courier_service'];
		}
		$headers = array_merge($headers, $couriers);
		$headers = array_merge($headers, array('Commulative'));
		$table->setHeaders($headers);
		$table->setTableWidth(count($headers));
		$table->setFooterVisible(true);		
		$ReportData;
		
		$sql = "select (case when delivereddate - shippeddate <= 2*86400 then '0-2 days' when delivereddate - shippeddate > 2*86400 and delivereddate - shippeddate <= 5*86400 then '3-5 days' 
					when delivereddate - shippeddate > 5*86400 and delivereddate - shippeddate <= 7*86400 then '6-7 days' 
					when delivereddate - shippeddate > 7*86400 and delivereddate - shippeddate <= 15*86400 then '8-15 days' else '>15 days' end) as bucket, courier_service, count(*) as orders
					from xcart_orders where delivereddate >= unix_timestamp('" . $calculated_dates[14]['start'] . "') and delivereddate < unix_timestamp('" . $calculated_dates[13]['end'] . "') 
					group by bucket, courier_service";
		$result = func_query($sql, true);
		$sql = "select courier_service, count(*) as orders
					from xcart_orders where delivereddate >= unix_timestamp('" . $calculated_dates[14]['start'] . "') and delivereddate < unix_timestamp('" . $calculated_dates[13]['end'] . "')
					group by courier_service order by courier_service";
		$result_total = func_query($sql, true);
		for($cnt=0;$cnt<count($result);$cnt++){
			$ReportData[$result[$cnt]['bucket']][$result[$cnt]['courier_service']] = (float)$result[$cnt]['orders'];
		}
		$sql = "select (case when delivereddate - shippeddate <= 2*86400 then '0-2 days' when delivereddate - shippeddate > 2*86400 and delivereddate - shippeddate <= 5*86400 then '3-5 days' 
					when delivereddate - shippeddate > 5*86400 and delivereddate - shippeddate <= 7*86400 then '6-7 days' 
					when delivereddate - shippeddate > 7*86400 and delivereddate - shippeddate <= 15*86400 then '8-15 days' else '>15 days' end) as bucket, count(*) as orders
					from xcart_orders where delivereddate >= unix_timestamp('" . $calculated_dates[14]['start'] . "') and delivereddate < unix_timestamp('" . $calculated_dates[13]['end'] . "') 
					group by bucket";
		$result_bucketsCommulative=func_query($sql, true);
		$sql = "select count(*) as orders
					from xcart_orders where delivereddate >= unix_timestamp('" . $calculated_dates[14]['start'] . "') and delivereddate < unix_timestamp('" . $calculated_dates[13]['end'] . "')";
		$result_totalCommulative=func_query($sql, true);

		$i=0;
		foreach($ReportData as $key=>$rowdata){
			$table->setCellData($i, 0, $key);
			for($cnt=0; $cnt<count($couriers); $cnt++){
				$table->setCellData($i, $cnt+1, (float)$rowdata[$couriers[$cnt]]*100/$result_total[$cnt]['orders']);
			}
			$i++;
		}
		$j=count($couriers)+1;
		for($cnt=0;$cnt<count($result_bucketsCommulative);$cnt++){
			$table->setCellData($cnt, $j, (float)$result_bucketsCommulative[$cnt]['orders']*100/$result_totalCommulative[0]['orders']);
		}
		return $table;
	}

	public function deliveryByCityInfo($calculated_dates){
		$table= new Table('Delivery performance by city (Top 15) - last + current month');
		$headers = array('Delivery time bucket','Ahmedabad', 'Bangalore', 'Bhubaneshwar', 'Chandigarh', 'Guwahati', 'Hyderabad', 'Jaipur', 'Kolkata', 'Lucknow', 'Ludhiana', 'Mumbai', 'Mysore', 'New Delhi', 'Pune', 'Surat', 'Commulative');
		$cities = array_slice($headers, 1, 15);
		$table->setHeaders($headers);
		$table->setTableWidth(count($headers));
		$table->setFooterVisible(true);		
		$dates_required = array(13,14); //MTD, M1
		$ReportData;
		$ReportDataTotal;
		
		$sql = "select (case when delivereddate - shippeddate <= 2*86400 then '0-2 days' when delivereddate - shippeddate > 2*86400 and delivereddate - shippeddate <= 5*86400 then '3-5 days' 
				when delivereddate - shippeddate > 5*86400 and delivereddate - shippeddate <= 7*86400 then '6-7 days' 
				when delivereddate - shippeddate > 7*86400 and delivereddate - shippeddate <= 15*86400 then '8-15 days' else '>15 days' end) as bucket, 
				(case when s_city like'%ahmedabad%' then 'Ahmedabad'
				when s_city like'%bangalore%' then 'Bangalore'
				when s_city like'%bhubaneswar%' then 'Bhubaneshwar'
				when s_city like'%Chandigarh%' then 'Chandigarh'
				when s_city like'%Guwahati%' then 'Guwahati'
				when s_city like'%hyderabad%' then 'Hyderabad'
				when s_city like'%Jaipur%' then 'Jaipur'
				when s_city like'%Kolkata%' then 'Kolkata'
				when s_city like'%lucknow%' then 'Lucknow'
				when s_city like'%ludhiana%' then 'Ludhiana'
				when s_city like'%Mumbai%' then 'Mumbai'
				when s_city like'%mysore%' then 'Mysore'
				when s_city like'%new delhi%' then 'New Delhi'
				when s_city like'%Pune%' then 'Pune'
				when s_city like'%surat%' then 'Surat'
				else null end) as city,
				count(*) as orders
				from xcart_orders where delivereddate >= unix_timestamp('" . $calculated_dates[14]['start'] . "') and delivereddate < unix_timestamp('" . $calculated_dates[13]['end'] . "') 
				group by bucket, city";
		$result = func_query($sql, true);
		$sql = "select (case when s_city like'%ahmedabad%' then 'Ahmedabad'
				when s_city like'%bangalore%' then 'Bangalore'
				when s_city like'%bhubaneswar%' then 'Bhubaneshwar'
				when s_city like'%Chandigarh%' then 'Chandigarh'
				when s_city like'%Guwahati%' then 'Guwahati'
				when s_city like'%hyderabad%' then 'Hyderabad'
				when s_city like'%Jaipur%' then 'Jaipur'
				when s_city like'%Kolkata%' then 'Kolkata'
				when s_city like'%lucknow%' then 'Lucknow'
				when s_city like'%ludhiana%' then 'Ludhiana'
				when s_city like'%Mumbai%' then 'Mumbai'
				when s_city like'%mysore%' then 'Mysore'
				when s_city like'%new delhi%' then 'New Delhi'
				when s_city like'%Pune%' then 'Pune'
				when s_city like'%surat%' then 'Surat'
				else null end) as city, count(*) as orders
				from xcart_orders where delivereddate >= unix_timestamp('" . $calculated_dates[14]['start'] . "') and delivereddate < unix_timestamp('" . $calculated_dates[13]['end'] . "') 
				group by city";
		$result_total_0 = func_query_column($sql, 0, true);
		$result_total_1 = func_query_column($sql, 1, true);
 		for($cnt=0;$cnt<count($result);$cnt++){
			if($result[$cnt]['city'] != null)				
				$ReportData[$result[$cnt]['bucket']][$result[$cnt]['city']] = (float)$result[$cnt]['orders'];
		}
		
		for($cnt=0;$cnt<count($cities);$cnt++){
			$index = array_search($cities[$cnt], $result_total_0);
			//if the city exists in the result_total array
			if(is_int($index)) 
				$ReportDataTotal['total'][$cities[$cnt]] = (float)$result_total_1[$index];		
		}
		
		$i=0;
		foreach($ReportData as $key=>$rowdata){
			$table->setCellData($i, 0, $key);
			for($cnt=0; $cnt<count($headers)-2; $cnt++){
				$table->setCellData($i, $cnt+1, (float)$rowdata[$headers[$cnt+1]]*100/$ReportDataTotal['total'][$headers[$cnt+1]]);
			}
			$i++;
		}
		
		$sql = "select (case when delivereddate - shippeddate <= 2*86400 then '0-2 days' when delivereddate - shippeddate > 2*86400 and delivereddate - shippeddate <= 5*86400 then '3-5 days' 
					when delivereddate - shippeddate > 5*86400 and delivereddate - shippeddate <= 7*86400 then '6-7 days' 
					when delivereddate - shippeddate > 7*86400 and delivereddate - shippeddate <= 15*86400 then '8-15 days' else '>15 days' end) as bucket, count(*) as orders
					from xcart_orders where delivereddate >= unix_timestamp('" . $calculated_dates[14]['start'] . "') and delivereddate < unix_timestamp('" . $calculated_dates[13]['end'] . "')
                    and (s_city like '%ahmedabad%' or s_city like '%bangalore%' or s_city like '%bhubaneswar%' or s_city like '%Chandigarh%' or s_city like '%Guwahati%' or s_city like '%hyderabad%' or s_city like '%Jaipur%' or s_city like '%Kolkata%' or s_city like '%lucknow%' or s_city like '%ludhiana%' or s_city like '%Mumbai%' or s_city like '%mysore%' or s_city like '%new delhi%' or s_city like '%Pune%' or s_city like '%surat%')
					group by bucket";
		$result_bucketsCommulative=func_query($sql, true);
		$sql = "select count(*) as orders
					from xcart_orders where delivereddate >= unix_timestamp('" . $calculated_dates[14]['start'] . "') and delivereddate < unix_timestamp('" . $calculated_dates[13]['end'] . "')
                    and (s_city like '%ahmedabad%' or s_city like '%bangalore%' or s_city like '%bhubaneswar%' or s_city like '%Chandigarh%' or s_city like '%Guwahati%' or s_city like '%hyderabad%' or s_city like '%Jaipur%' or s_city like '%Kolkata%' or s_city like '%lucknow%' or s_city like '%ludhiana%' or s_city like '%Mumbai%' or s_city like '%mysore%' or s_city like '%new delhi%' or s_city like '%Pune%' or s_city like '%surat%')
					";
		$result_totalCommulative=func_query($sql, true);
		$j=count($headers)-1;
		for($cnt=0;$cnt<count($result_bucketsCommulative);$cnt++){
			$table->setCellData($cnt, $j, (float)$result_bucketsCommulative[$cnt]['orders']*100/$result_totalCommulative[0]['orders']);
		}
		return $table;
	}
	
	
	public function buildReport(){
		global $weblog;
		$group= new TableGroup("Delivery Data");
		$deliveryWeeklytable= new Table("Delivery performance weekly trend - last 3 weeks");
		$headers= array('Delivery time bucket', 'WTD <br> (% of orders)', 'W1 <br> (% of orders)', 'W2 <br> (% of orders)', 'Commulative <br> (% of orders)');
		$deliveryWeeklytable->setHeaders($headers);
		$row_headers= array('0-2 days', '3-5 days', '6-7 days', '8-15 days', '>15 days');
		$filtervalue = $this->getFilterValue("Date:");
		$date_split=split('/', $filtervalue);
		$current_month=(INT)$date_split[0];
		$current_year=(INT)$date_split[2];
		$current_date=(INT)$date_split[1];
		//function to calculate the end and start days for various headers.
		$calculated_dates = calculateDates($current_date, $current_month, $current_year);
		$dates_required = array(8,9,10); //WTD, W1, W2
		for($cnt=0;$cnt<count($row_headers);$cnt++){
			$deliveryWeeklytable->setCellData($cnt, 0, $row_headers[$cnt]);
		}
		for($cnt=0;$cnt<count($dates_required);$cnt++){
			$sql = "select (case when delivereddate - shippeddate <= 2*86400 then '0-2 days' when delivereddate - shippeddate > 2*86400 and delivereddate - shippeddate <= 5*86400 then '3-5 days' 
					when delivereddate - shippeddate > 5*86400 and delivereddate - shippeddate <= 7*86400 then '6-7 days' 
					when delivereddate - shippeddate > 7*86400 and delivereddate - shippeddate <= 15*86400 then '8-15 days' else '>15 days' end) as bucket, count(*) as orders
					from xcart_orders where delivereddate >= unix_timestamp('" . $calculated_dates[$dates_required[$cnt]]['start'] . "') and delivereddate < unix_timestamp('" . $calculated_dates[$dates_required[$cnt]]['end'] . "') 
					group by bucket";	
			$result_buckets=func_query($sql, true);
			$sql = "select count(*) as orders
					from xcart_orders where delivereddate >= unix_timestamp('" . $calculated_dates[$dates_required[$cnt]]['start'] . "') and delivereddate < unix_timestamp('" . $calculated_dates[$dates_required[$cnt]]['end'] . "')";
			$result_total=func_query($sql, true);
			for($i=0;$i<count($row_headers);$i++){
				$deliveryWeeklytable->setCellData($i, $cnt+1, (float)$result_buckets[$i]['orders']*100/$result_total[0]['orders']);
			}
		}
		$sql = "select (case when delivereddate - shippeddate <= 2*86400 then '0-2 days' when delivereddate - shippeddate > 2*86400 and delivereddate - shippeddate <= 5*86400 then '3-5 days' 
					when delivereddate - shippeddate > 5*86400 and delivereddate - shippeddate <= 7*86400 then '6-7 days' 
					when delivereddate - shippeddate > 7*86400 and delivereddate - shippeddate <= 15*86400 then '8-15 days' else '>15 days' end) as bucket, count(*) as orders
					from xcart_orders where delivereddate >= unix_timestamp('" . $calculated_dates[10]['start'] . "') and delivereddate < unix_timestamp('" . $calculated_dates[8]['end'] . "') 
					group by bucket";		
		$result_bucketsCommulative=func_query($sql, true);	
		$sql = "select count(*) as orders
					from xcart_orders where delivereddate >= unix_timestamp('" . $calculated_dates[10]['start'] . "') and delivereddate < unix_timestamp('" . $calculated_dates[8]['end'] . "')";
		$result_totalCommulative=func_query($sql, true);
		for($i=0;$i<count($row_headers);$i++){
				$deliveryWeeklytable->setCellData($i, 4, (float)$result_bucketsCommulative[$i]['orders']*100/$result_totalCommulative[0]['orders']);
			}
		$deliveryWeeklytable->setTableWidth(count($headers));
		$deliveryWeeklytable->setFooterVisible(true);
		
		
		//make other tables
		$ageingTable = $this->ageingInfo($calculated_dates);
		$deliveryByCourierTable = $this->deliveryByCourierInfo($calculated_dates);
		$deliveryByCityTable = $this->deliveryByCityInfo($calculated_dates);
		
		$group->addTable($deliveryWeeklytable);
		$group->addTable($ageingTable);
		$group->addTable($deliveryByCourierTable);
		$group->addTable($deliveryByCityTable);
		$this->addTableGroup($group);

		$this->isBuild=true;
	}


}
?>

