<?php
include_once REP_DIR.'/models/models.php';
include_once REP_DIR.'/rendrers/rendrers.php';
include_once REP_DIR.'/models/date_calculation.php';

class DailyCustomerActivityReportModel extends ReportModel{
	public function __construct(){
		parent::__construct();
		$this->name="Customers Activity";

	}
	protected function initFilters(){
		$this->addFilter("Date:" ,self::$FILTER_TYPE_DATE);
	}

	public function loadDefaultFilterValues(){
		$time=time();
		$this->setFilterValue("Date:", date("m/d/Y",$time));
	}

	public function buildReport(){
		if($this->isBuild)
		return;
			
		$filtervalue = $this->getFilterValue("Date:");
		$date_split=split('/', $filtervalue);
		$current_month=(INT)$date_split[0];
		$current_year=(INT)$date_split[2];
		$current_date=(INT)$date_split[1];
		$curr_fdate= new FDate($current_date, $current_month, $current_year);
		$fyear = new FYear($current_date, $current_month, $current_year);
		$group= new TableGroup("Customers Activity");
		$this->addTableGroup($group);
		$table= new Table();
		$group->addTable($table);
		$table->setFooterVisible(false);

		$j=0;

		$table->setHeaderValue($j,"Customer Activity");
		$table->setCellData(0, $j,"# New Customers" );
		$table->setCellData(1, $j,"# Transactions" );
		$table->setCellData(2, $j,"First Trxn. ASP");
		$table->setCellData(3, $j,"Total Revenue");
		$table->setCellData(4, $j,"# Repeat Customers");
		$table->setCellData(5, $j,"# Transactions");
		$table->setCellData(6, $j,"Trxn. ASP");
		$table->setCellData(7, $j,"Total Revenue");
		$table->setCellData(8, $j,"# New Registrations");

		$j++;
		$TOTAL_MONTHS_SPENT=0;
		$calc_dates=array();
		$i=0;
		foreach ($fyear->getFinancialYearIterator() as $date){
			$calc_dates[$i]['start']=$date->toUnixDateDayStart();
			$calc_dates[$i]['end']=$fyear->getNextDateAfterEndOfMonth($date)->toUnixDateDayStart();
			$calc_dates[$i]['label']=$date->getMonthNameString()."-".$date->getYear();
			$i++;
			$TOTAL_MONTHS_SPENT++;
		}
		$calc_dates[$i-1]['label']='MTD';
		$calc_dates[$i]['start']=$fyear->getFinancialYearEndFDate()->addDays(-1)->toUnixDateDayStart();
		$calc_dates[$i]['end']=$fyear->getFinancialYearEndFDate()->toUnixDateDayStart();
		$calc_dates[$i]['label']="Yesterday";

		$calc_dates=array_reverse($calc_dates);
		foreach ($calc_dates as $span){
			$table->setHeaderValue($j, $span['label']);
			$result=func_query("select b.ctype as type,count(b.orders) as orders,count(distinct b.logins) as custs,sum(b.value) as value from (
				select (case exists (select distinct login from xcart_orders where login=a.login and
				 date < a.date and status in ('C','Q','WP','OH','SH','DL')) when true
				                    then 'REP' else 'NEW'
				                    end ) as ctype,(a.orderid) as orders,(a.login) as logins, (a.total+a.shipping_cost+a.gift_charges) value  
				from xcart_orders as a where a.status in ('C','Q','WP','OH','SH','DL') and a.date >=unix_timestamp('".$span['start']."') 
				
				and  a.date <unix_timestamp('".$span['end']."')) as b  group by b.ctype", true);
			$reg_result = func_query("select count(*) as regTotal from xcart_customers where first_login >=unix_timestamp('".$span['start']."') and first_login < unix_timestamp('".$span['end']."')", true);
			
			$temp[$result[0]['type']]=$result[0];
			$temp[$result[1]['type']]=$result[1];
			$table->setCellData(0, $j, (float)$temp['NEW']['custs']);
			$table->setCellData(1, $j, (float)$temp['NEW']['orders']);
			$table->setCellData(2, $j, indian_rs_format((float)$temp['NEW']['value']/(float)$temp['NEW']['orders'], 2));
			$table->setCellData(3, $j, indian_rs_format((float)$temp['NEW']['value'],2));
			$table->setCellData(4, $j, (float)$temp['REP']['custs']);
			$table->setCellData(5, $j, (float)$temp['REP']['orders']);
			$table->setCellData(6, $j, indian_rs_format((float)$temp['REP']['value']/(float)$temp['REP']['orders'], 2));
			$table->setCellData(7, $j, indian_rs_format((float)$temp['REP']['value'],2));
			$table->setCellData(8, $j, $reg_result[0]['regTotal']);
			
			$j++;

		

		}



			
		$this->isBuild=true;
	}

}


class CustomerStatsReportModel extends ReportModel{
	public function __construct(){
		parent::__construct();
		$this->name="Customer Statistics Report";

	}
	protected function initFilters(){
		$this->addFilter("Date:" ,self::$FILTER_TYPE_DATE);
	}

	public function loadDefaultFilterValues(){
		$time=time();
		$this->setFilterValue("Date:", date("m/d/Y",$time));
	}

	public function buildReport(){
		if($this->isBuild)
		return;
			
		$filtervalue = $this->getFilterValue("Date:");
		$date_split=split('/', $filtervalue);
		$current_month=(INT)$date_split[0];
		$current_year=(INT)$date_split[2];
		$current_date=(INT)$date_split[1];
		$curr_fdate= new FDate($current_date, $current_month, $current_year);
		$fyear = new FYear($current_date, $current_month, $current_year);
		$group= new TableGroup("Customer Statistics");
		$this->addTableGroup($group);
		$table= new Table();
		$group->addTable($table);
		$table->setFooterVisible(false);

		$TOTAL_MONTHS_SPENT=0;
		foreach ($fyear->getFinancialYearIterator() as $date){
			$TOTAL_MONTHS_SPENT++;
		}

		$j=0;

		$table->setHeaderValue($j,"Customer Statistics [Last 6 months]");
		$table->setCellData(0, $j,"Total # Customers" );
		$table->setCellData(1, $j,"# Active Customers (3 months)" );
		$table->setCellData(2, $j,"# Active Customers (6 months)");
		$table->setCellData(3, $j,"# Active Customers (12 months)");
		$table->setCellData(4, $j,"# Repeat Customers");
		$table->setCellData(5, $j,"# Repeat Customers (2 purchases)");
		$table->setCellData(6, $j,"# Repeat Customers (3-5 purchases)");
		$table->setCellData(7, $j,"# Repeat Customers (5+ purchases)");
		$table->setCellData(8, $j,"Life Time Value(Total Revenue/Customers)");
		$table->setCellData(9, $j,"Avg. Revenue/Repeat Customer");
		$table->setCellData(10, $j,"Annual Revenue/User");
		$j++;
			


		$temp_fdate= new FDate($current_date, $current_month, $current_year);
			
		$calc_dates=array();
		$i=0;
		for ($ii=0;$ii<6;$ii++){
			$start_fdate=$temp_fdate->getPrevMonthStartFDate();
			$end_fdate=$start_fdate->getNextMonthStartFDate();
			$calc_dates[$i]['start']=$start_fdate->toUnixDateDayStart();
			$calc_dates[$i]['end']=$end_fdate->toUnixDateDayStart();
			$calc_dates[$i]['msp']=$TOTAL_MONTHS_SPENT-$ii-1;
			$calc_dates[$i]['label']=FYear::$MONTH_NAME[$start_fdate->getMonth()]." ".$start_fdate->getYear();
			$temp_fdate=$start_fdate;
			$i++;
		}
			
		//$calc_dates=array_reverse($calc_dates);


		foreach ($calc_dates as $span){
			$table->setHeaderValue($j, $span['label']);
			$active_cust=func_query("select	(case true	when (a.date)>(unix_timestamp('".$span['end']."')-(60*60*24*90)) then '#3'
							when (a.date)>(unix_timestamp('".$span['end']."')-(60*60*24*180)) then '#6'
							when (a.date)>(unix_timestamp('".$span['end']."')-(60*60*24*365)) then '#12'
							else '#MORE'
						end 
						) active, count( a.login) value from (select max(date) as 'date',login as login from xcart_orders  where status in ('OH','C','WP','SH','Q','DL')
				and date <unix_timestamp('".$span['end']."')  group by login)  a group by active", true);
			$purchase=func_query("select	(case true	when (a.orders)>5 then '#5+'
						when (a.orders)>=3 then '#3-5' 
						when (a.orders)=2 then '#2' 
						else '#MORE'
					end 
					) active, count( a.login) value from (select count(orderid) as 'orders',login as login from xcart_orders  
					where status in ('OH','C','WP','SH','Q','DL')
			and date <unix_timestamp('".$span['end']."')  group by login)  a group by active", true);

			$ltv=func_query("select sum(total+shipping_cost+gift_charges)/count(distinct login) as value from xcart_orders
					where status in ('OH','C','WP','SH','Q','DL') and date <unix_timestamp('".$span['end']."')", true);

			$rep=func_query("
			select sum(a.value) as value,count(distinct a.login) as repusers from (
			select login as login,count(login) as cnt,sum(total+shipping_cost+gift_charges)  as value from xcart_orders 
			where status in ('C','Q','WP','OH','SH','DL') and date<unix_timestamp('".$span['end']."') group by login) as a where a.cnt>1", true);

			$project_rep_rev=func_query("select sum(a.value) as value,count(distinct a.login) as repusers from (
			select login as login,count(login) as cnt,sum(total+shipping_cost+gift_charges)*(12/".$span['msp'].")  as value from xcart_orders 
			where status in ('C','Q','WP','OH','SH','DL') and date>unix_timestamp('".$fyear->getFinancialYearStartFDate()->toUnixDateDayStart()."') and date<unix_timestamp('".$span['end']."') group by login) 
			as a where a.cnt>1", true);

			$temp[$active_cust[0]['active']]=$active_cust[0]['value'];
			$temp[$active_cust[1]['active']]=$active_cust[1]['value'];
			$temp[$active_cust[2]['active']]=$active_cust[2]['value'];
			$temp[$active_cust[3]['active']]=$active_cust[3]['value'];

			$table->setCellData(0, $j, (float)($temp['#3']+$temp['#6']+$temp['#12']+$temp['#MORE']));
			$table->setCellData(1, $j, (float)$temp['#3']);
			$table->setCellData(2, $j, (float)$temp['#6']+(float)$temp['#3']);
			$table->setCellData(3, $j, (float)$temp['#12']+(float)$temp['#6']+(float)$temp['#3']);

			$temp[$purchase[0]['active']]=$purchase[0]['value'];
			$temp[$purchase[1]['active']]=$purchase[1]['value'];
			$temp[$purchase[2]['active']]=$purchase[2]['value'];
			$temp[$purchase[3]['active']]=$purchase[3]['value'];

			$table->setCellData(4, $j, (float)($temp['#5+']+$temp['#3-5']+$temp['#2']));
			$table->setCellData(5, $j, (float)$temp['#2']+(float)$temp['#5+']+(float)$temp['#3-5']);
			$table->setCellData(6, $j, (float)$temp['#3-5']+(float)$temp['#5+']);
			$table->setCellData(7, $j, (float)$temp['#5+']);
			$table->setCellData(8, $j, indian_rs_format((float)$ltv[0]['value'], 2));
			$table->setCellData(9, $j, indian_rs_format((float)$rep[0]['value']/(float)$rep[0]['repusers'], 2));
			$table->setCellData(10, $j, indian_rs_format((float)$project_rep_rev[0]['value']/(float)$project_rep_rev[0]['repusers'], 2));
			$j++;

		}

			

			
			
		$this->isBuild=true;
	}
}


class RegistrationsReportModel extends ReportModel{
	public function __construct(){
		parent::__construct();
		$this->name="Registrations Report";

	}
	protected function initFilters(){
		$this->addFilter("Date:" ,self::$FILTER_TYPE_DATE);
	}

	public function loadDefaultFilterValues(){
		$time=time();
		$this->setFilterValue("Date:", date("m/d/Y",$time));
	}

	public function buildReport(){
		if($this->isBuild)
		return;
			
		$filtervalue = $this->getFilterValue("Date:");
		$date_split=split('/', $filtervalue);
		$current_month=(INT)$date_split[0];
		$current_year=(INT)$date_split[2];
		$current_date=(INT)$date_split[1];
		$curr_fdate= new FDate($current_date, $current_month, $current_year);
		$fyear = new FYear($current_date, $current_month, $current_year);
		$group= new TableGroup("Registrations");
		$this->addTableGroup($group);
		$table= new Table();
		$group->addTable($table);
		$table->setFooterVisible(false);

		$j=0;

			



		$table->setHeaderValue($j,"Registrations");
		$table->setCellData(0, $j,"Total # Registered Users" );
		$table->setCellData(1, $j,"New Registrations" );
		$table->setCellData(2, $j,"Registration -> 1st Purchase");
		$table->setCellData(3, $j,"Avg Registration -> Conversion in Days");
		$table->setCellData(4, $j,"# Registered Users - Never Purchased");
		$j++;
			


		$temp_fdate= new FDate($current_date, $current_month, $current_year);
			
		$calc_dates=array();
		$i=0;
		for ($ii=0;$ii<6;$ii++){
			$start_fdate=$temp_fdate->getPrevMonthStartFDate();
			$end_fdate=$start_fdate->getNextMonthStartFDate();
			$calc_dates[$i]['start']=$start_fdate->toUnixDateDayStart();
			$calc_dates[$i]['end']=$end_fdate->toUnixDateDayStart();
			$calc_dates[$i]['msp']=$TOTAL_MONTHS_SPENT-$ii-1;
			$calc_dates[$i]['label']=FYear::$MONTH_NAME[$start_fdate->getMonth()]." ".$start_fdate->getYear();
			$temp_fdate=$start_fdate;
			$i++;
		}

		foreach ($calc_dates as $span){
			$table->setHeaderValue($j, $span['label']);
			$u_regs=func_query("select (case true when first_login between unix_timestamp('".$span['start']."') and unix_timestamp('".$span['end']."') then 'new' else 'old' end) as utype,
			count(login)  as logins from xcart_customers where first_login<unix_timestamp('".$span['end']."') group by utype", true);

			$fisrt_p= func_query("select count(distinct cust.login) cnts from xcart_customers cust,(select login , min(date) firstP from xcart_orders where status in  ('OH','C','WP','SH','Q','DL') and
			date < unix_timestamp('".$span['end']."') group by login) tab where tab.login=cust.login and tab.firstP between  unix_timestamp('".$span['start']."') and unix_timestamp('".$span['end']."')
			and cust.first_login<unix_timestamp('".$span['start']."')", true);
			$never_p=func_query("select count(*) as cnts from xcart_customers where login not in
			(select (login ) from xcart_orders where status in  ('OH','C','WP','SH','Q','DL') and date <  unix_timestamp('".$span['end']."')) and first_login < unix_timestamp('".$span['end']."')", true);
			$conv=func_query("
			select avg((tab.firstP-cust.first_login)/(3600*24)) 
			cnts from xcart_customers cust,(select login , min(date) firstP from xcart_orders where status in  ('OH','C','WP','SH','Q','DL') and 
			date < unix_timestamp('".$span['end']."') group by login) tab where tab.login=cust.login and cust.first_login<unix_timestamp('".$span['end']."') 
			and cust.first_login!=0 and  cust.first_login<tab.firstP", true);

			$temp[$u_regs[0]['utype']]=$u_regs[0]['logins'];
			$temp[$u_regs[1]['utype']]=$u_regs[1]['logins'];
			$table->setCellData(0, $j, (float)($temp['new'])+(float)($temp['old']));
			$table->setCellData(1, $j, (float)($temp['new']));
			$table->setCellData(2, $j, (float)$fisrt_p[0]['cnts']);
			$table->setCellData(3, $j, (float)$conv[0]['cnts']);
			$table->setCellData(4, $j, (float)$never_p[0]['cnts']);
			$j++;
		}
			
		$this->isBuild=true;

	}
}