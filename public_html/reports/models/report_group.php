<?php

include_once REP_DIR.'/models/models.php';
include_once REP_DIR.'/rendrers/rendrers.php';

class ReportGroupModel extends ReportModel{
	
	private $reports= array();
	public function __construct(){
		global $report_list,$report_title;
		if(!isset($report_list))
		exit;
		$strs=split(',', $report_list);
		$factory= new ReportModelFactory();
		$i=0;
		$this->name=$report_title;
		foreach ($strs as $str){
			$this->reports[$i++]= $factory->createModel((INT)trim($str));
		}
		
		$this->initFilters();
		$this->loadDefaultFilterValues();
		
		

	}
	protected function initFilters(){
		foreach ($this->reports as $report){
			foreach ($report->getFilters() as $filter){
				$this->addFilter($filter['name'], $filter['type']);
			}
		}
	}

	public function loadDefaultFilterValues(){
		foreach ($this->reports as $report){
			foreach ($report->getFilters() as $filter){
				$this->setFilterValue($filter['name'], $filter['value']);
			}
		}
	}
	
	
	public function loadFilterValuesFrom($get,$post){
		
		foreach ($this->reports as $report){
			$report->loadFilterValuesFrom($get,$post);
		}
		
		parent::loadFilterValuesFrom($get, $post);
	}
	
	public function buildReport(){
		if($this->isBuild)
			return;
		foreach ($this->reports as $report){
			$report->buildReport();
			foreach($report->getTableGroups() as $group)
				$this->addTableGroup($group);
			$this->notes = $report->getReportNotes();
		}
		$this->isBuild=true;
	}
}
?>