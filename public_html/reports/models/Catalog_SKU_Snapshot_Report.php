<?php

global $xcart_dir;
include_once $xcart_dir.'/reports/models/models.php';
include_once $xcart_dir.'/reports/rendrers/rendrers.php';
include_once REP_DIR.'/models/date_calculation.php';

class CatalogSKUSnapshotReportModel extends ReportModel {

	public function __construct(){
		parent::__construct();
		$this->name="Catalog SKU Snapshot Report : " . date("j-M-Y");

	}
	protected function initFilters(){
		$this->addFilter("Date:" ,self::$FILTER_TYPE_DATE);
	}

	public function loadDefaultFilterValues(){
		$time=time();
		$this->setFilterValue("Date:", date("m/d/Y",$time));
	}


	public function buildReport(){
		global $weblog;
		$table= new Table();
		$group= new TableGroup("Catalog SKU Snapshot Report");
		$superHeaders= array('Brand'=>1,'Total # Articles'=>1,'# Articles (Full Set)'=>1,'# Articles (Broken Set)'=>1,
		'# Articles (No SKU)'=>1,'# SKUs - Current'=>2, 'Current Inventory Count'=>1,'# SKUs - Last 7 Days'=>2,'# SKUs - Last 30 Days'=>2,'# Articles Added – Last 7 Days '=>1,'# Articles Added – Last 30 Days'=>1);
		$headers= array('','','','',
		'','Active','In Active', '','Enabled','Disabled','Enabled','Disabled','','');
		$table->setFooterVisible(true);
		$filtervalue = $this->getFilterValue("Date:");
		$date_split=split('/', $filtervalue);
		$current_month=(INT)$date_split[0];
		$current_year=(INT)$date_split[2];
		$current_date=(INT)$date_split[1];
		$ReportData;
		
		$today= new FDate($current_date, $current_month, $current_year);
		$last7thDay= $today->addDays(-7);
		$today= new FDate($current_date, $current_month, $current_year);
		$last30thDay=$today->addDays(-30);
		$today= new FDate($current_date, $current_month, $current_year);
		
		// gives availablity of skus
		$sql = "select tb.brand as brand,tb.availability_type availibility_type,count(availability) as availability 
 				from (select sp.global_attr_brand as brand, (
				case true when(select count(id) from mk_product_options where style=sp.style_id) = 
				(select count(id) from mk_product_options where style=sp.style_id and is_active=0) then 'NO SKU' 
        		  when (select count(id) from mk_product_options where style=sp.style_id) = 
                  (select count(id) from mk_product_options where style=sp.style_id and is_active=1) then 'FULL SET'
          	    else 'BROKEN SET' end) as availability_type,sp.style_id availability
				from mk_style_properties sp  where  
				sp.style_id in(select distinct style_id from mk_styles_options_skus_mapping) ) tb group by brand,availability_type";
		$result = func_query($sql, true);
		
		foreach ($result as $row){
			$ReportData[$row['brand']]['availability'][$row['availibility_type']]=(float)($row['availability']);
		}
		
		$sql="select sp.global_attr_brand as brand ,op.is_active as active,count(op.id) numbers 
				from mk_style_properties sp  join mk_styles_options_skus_mapping ms
				on ms.style_id=sp.style_id  join mk_product_options op
				on op.id=ms.option_id group by brand, active";
		
		$result = func_query($sql, true);
		
		foreach ($result as $row){
			$ReportData[$row['brand']]['active'][$row['active']]=(float)($row['numbers']);
		}
		
			$sql = "select sp.global_attr_brand as brand , sum(cic.inv_count) as num
					from mk_style_properties sp  join mk_styles_options_skus_mapping ms
					on ms.style_id=sp.style_id left join core_inventory_counts cic on cic.sku_id = ms.sku_id
	                group by brand";

		$result = func_query($sql, true);
		
		foreach ($result as $row){
			$ReportData[$row['brand']]['invCount']=(float)$row['num'];
		}
		
		$sql="select sp.global_attr_brand as brand,sl.value active,count(*) as numbers	from 
			mk_style_properties sp ,mk_skus s,mk_skus_logs sl,mk_styles_options_skus_mapping m,mk_inventory_movement_reasons mr 
			where sp.style_id = m.style_id and
			m.sku_id=s.id and s.id=sl.skuid and mr.code in('MAND','MANE','OSAD','ISAE') and sl.reasonid=mr.id and sl.lastmodified between '".$last7thDay->toUnixDateDayStart()."' and '".$today->toUnixDateDayStart()."'  group by brand,sl.value";
		 
		$result = func_query($sql, true);
		
		foreach ($result as $row){
			$ReportData[$row['brand']]['enabled-disabled']['7days'][$row['active']]=(float)($row['numbers']);
		}
		
		$sql="select sp.global_attr_brand as brand,sl.value active,count(*) as numbers	from 
			mk_style_properties sp ,mk_skus s,mk_skus_logs sl,mk_styles_options_skus_mapping m,mk_inventory_movement_reasons mr 
			where sp.style_id = m.style_id and
			m.sku_id=s.id and s.id=sl.skuid and mr.code in('MAND','MANE','OSAD','ISAE') and sl.reasonid=mr.id and sl.lastmodified between '".$last30thDay->toUnixDateDayStart()."' and '".$today->toUnixDateDayStart()."'  group by brand,sl.value";
		
		$result = func_query($sql, true);
		
		foreach ($result as $row){
			$ReportData[$row['brand']]['enabled-disabled']['30days'][$row['active']]=(float)($row['numbers']);
		}
		
		$sql="select global_attr_brand as brand,count(style_id) as numbers from mk_style_properties 
				where global_attr_catalog_add_date between unix_timestamp('".$last7thDay->toUnixDateDayStart()."') and unix_timestamp('".$today->toUnixDateDayStart()."') group by brand";
		
		
		$result = func_query($sql, true);
		
		foreach ($result as $row){
			$ReportData[$row['brand']]['added']['7days']=(float)($row['numbers']);
		}
		$sql="select global_attr_brand as brand,count(style_id) as numbers from mk_style_properties
				where global_attr_catalog_add_date between unix_timestamp('".$last30thDay->toUnixDateDayStart()."') and unix_timestamp('".$today->toUnixDateDayStart()."') group by brand";
		
	
		$result = func_query($sql, true);
		
		foreach ($result as $row){
			$ReportData[$row['brand']]['added']['30days']=(float)($row['numbers']);
		}
		
		$table->setHeaders($headers);
		$table->setSuperHeaders($superHeaders);
		$i=0;
		foreach($ReportData as $brand => $info){
			$j=0;
			$table->setCellData($i, $j++, $brand);
			$table->setCellData($i, $j++, $info['availability']['NO SKU']+$info['availability']['FULL SET']+$info['availability']['BROKEN SET']);
			$table->setCellData($i, $j++, $info['availability']['FULL SET']);
			$table->setCellData($i, $j++, $info['availability']['BROKEN SET']);
			$table->setCellData($i, $j++, $info['availability']['NO SKU']);
			$table->setCellData($i, $j++, $info['active']['1']);
			$table->setCellData($i, $j++, $info['active']['0']);
			$table->setCellData($i, $j++, $info['invCount']);
			$table->setCellData($i, $j++, $info['enabled-disabled']['7days']['true']);
			$table->setCellData($i, $j++, $info['enabled-disabled']['7days']['false']);
			$table->setCellData($i, $j++, $info['enabled-disabled']['30days']['true']);
			$table->setCellData($i, $j++, $info['enabled-disabled']['30days']['false']);
			$table->setCellData($i, $j++, $info['added']['7days']);
			$table->setCellData($i, $j++, $info['added']['30days']);
			$i++;
		}
		
		$group->addTable($table);
		$this->addTableGroup($group);
		$this->isBuild=true;
		$this->notes = "Full Set – All size options are are available in inventory<br>
			Broken Set – Only some size options are available in inventory<br>
			No SKU – No size options are available in inventory<br>
			Enabled and Disabled counts - The number of distinct SKUs which have been enabled  and disabled in the past 7 days/30 days<br>
			#Articles Added –  “Product Styles” will henceforth be known as “Articles”. The total number of new styles added in the catalog in the last 7 days and last 30 days will form the contents of these columns. ";
	}


}
?>

