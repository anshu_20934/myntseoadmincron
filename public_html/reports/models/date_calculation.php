<?php

final class FYear{

	public  static $FINANCIAL_YEAR_START_MONTH=4;
	public  static $YEAR=array(1=>4,2=>4,3=>4,4=>1,5=>1,6=>1,7=>2,8=>2,9=>2,10=>3,11=>3,12=>3);
	public  static $MONTH_NAME=array(0=>'Dec',1=>'Jan',2=>'Feb',3=>'Mar',4=>'Apr',5=>'May',6=>'Jun',7=>'Jul',8=>'Aug',9=>'Sep',10=>'Oct',11=>'Nov',12=>'Dec');
	public  static $QUARTER_START=array(1=>4,2=>7,3=>10,4=>1);



	private $day;
	private $month;
	private $year;



	/**
	 *
	 * @param unknown_type $day
	 * @param unknown_type $month
	 * @param unknown_type $year
	 */
	public function __construct($day,$month,$year){
		$this->day=$day;
		$this->month=$month;
		$this->year=$year;
	}



	public function getFinancialYearIterator(){
		return new FYearIterator($this);
	}

	public function getFinancialYearStartFDate(){
		if($this->month>=4)
		$date_split=split('/', date("m/d/Y",mktime(0,0,0,4,1,$this->year)));
		else
		$date_split=split('/', date("m/d/Y",mktime(0,0,0,4,1,$this->year-1)));
			
		return new FDate((INT)$date_split[1], (INT)$date_split[0], (INT)$date_split[2]);
	}

	public function getFinancialYearEndFDate(){
		return new FDate($this->day, $this->month, $this->year);
	}

	public function getEndDateOfMonth($fdate){
		if($this->month==$fdate->getMonth()&& $this->year==$fdate->getYear()){
			return new FDate($this->day, $this->month, $this->year);
		}else{
			return $fdate->getMonthEndFDate();
		}
	}

	public function getNextDateAfterEndOfMonth($fdate){
		if($this->month==$fdate->getMonth()&& $this->year==$fdate->getYear()){
			$newDate=new FDate($this->day, $this->month, $this->year);
			$newDate->addDays(1);
			return $newDate;
		}else{
			$fdate=$fdate->getMonthEndFDate();
			$fdate->addDays(1);
			return $fdate;
		}
	}

	public function getNumberOfDaysInMonth($fdate){
		if($this->month==$fdate->getMonth()&& $this->year==$fdate->getYear()){
			return $this->day;
		}else{
			return $fdate->getNumberOfDaysInMonthActual();
		}
	}

	/* get the quarter start date  for current time stamp */

	public function getQuarterStartFDate($fdate) {

		//echo "coming inside function";
		$quarter=self::$YEAR[$fdate->getMonth()] ;
		//echo "month for this quarter is".$quarter;
		//	exit();
		$newDate=new FDate(1, self::$QUARTER_START[$quarter], $fdate->getYear());
		return $newDate;
	}

	public function getQuarterEndFDate($fdate) {
		$quarter=self::$YEAR[$fdate->getMonth()] ;

		//logic to get end date for a quarter
		$quarter = $quarter+1 ;
		if($quarter > 4) {
			$quarter = 1;
		}
		$newDate=new FDate(1, self::$QUARTER_START[$quarter], $this->year);
		return $newDate;
	}
	public function getPrevQuarterStartFdate($fdate){
		//logic to get start date for prev quarter
		$quarter=self::$YEAR[$fdate->getMonth()] ;
		$quarter = $quarter -1;
		if($quarter ==0 ) {
			$quarter =4;
		}
		$newDate=new FDate(1, self::$QUARTER_START[$quarter], $quarter==3?$fdate->getYear()-1:$fdate->getYear());
		return $newDate;

	}
	public function getFDateDiffInDays($startFDate, $endFDate) {
		return ($endFDate->getTime() - $startFDate->getTime())/86400;

	}
}

/**
 *
 * Finantial date to do date calculation
 * @author Myntra
 *
 */

final class FDate{

	private $day;
	private $month;
	private $year;


	/**
	 *
	 * @param unknown_type $day
	 * @param unknown_type $month
	 * @param unknown_type $year
	 */
	public function __construct($day,$month,$year){
		$this->day=$day;
		$this->month=$month;
		$this->year=$year;
	}
	public function getDay(){
		return $this->day;
	}

	public function getMonth(){
		return $this->month;
	}

	public function getYear(){
		return $this->year;
	}
	public function setDay($day){
		$this->day = $day;
	}

	public function setMonth($month){
		$this->month = $month;
	}

	public function setYear($year){
		$this->year = $year;
	}
	/**
	 *
	 * Adds the days to the date
	 * @param unknown_type $days can be a negative value
	 */
	public function addDays($days){

		$date_split=split('/', date("m/d/Y",mktime(0,0,0,$this->month,$this->day+$days,$this->year)));


		$this->month=(INT)$date_split[0];
		$this->year=(INT)$date_split[2];
		$this->day=(INT)$date_split[1];
		return $this;
	}

	public function getNumberOfDaysInMonthActual(){
		return cal_days_in_month(CAL_GREGORIAN, $this->month, $this->year);
	}
	/**
	 *
	 * Gives a date after days
	 * @param unknown_type $days can be negative value
	 */
	public function getFDateAfterDays($days){

		$date_split=split('/', date("m/d/Y",mktime(0,0,0,$this->month,$this->day+$days,$this->year)));
		return new FDate((INT)$date_split[1], (INT)$date_split[0], (INT)$date_split[2]);
	}

	public function getMonthStartFDate(){
		$date_split=split('/', date("m/d/Y",mktime(0,0,0,$this->month,1,$this->year)));
		return new FDate((INT)$date_split[1], (INT)$date_split[0], (INT)$date_split[2]);
	}

	public function getNextMonthStartFDate(){
		$date_split=split('/', date("m/d/Y",mktime(0,0,0,$this->month+1,1,$this->year)));
		return new FDate((INT)$date_split[1], (INT)$date_split[0], (INT)$date_split[2]);
	}


	public function getMonthEndFDate(){
		$date_split=split('/', date("m/d/Y",mktime(0,0,0,$this->month+1,0,$this->year)));
		return new FDate((INT)$date_split[1], (INT)$date_split[0], (INT)$date_split[2]);
	}

	public function getTime(){
		return mktime(0,0,0,$this->month,$this->day,$this->year);
	}

	public function toUnixDateDayStart(){
		return "$this->year-$this->month-$this->day 00:00:00";
	}
	public function toUnixDateDayEnd(){
		return "$this->year-$this->month-$this->day 23:59:59";
	}

	public function getMonthNameString(){
		return FYear::$MONTH_NAME[$this->month];
	}

	/* This function is added by Vaibhav to get time date string  for a given time */
	public function getTimeDateString() {
		return date('Y-m-d H:i:s',mktime(0,0,0,$this->month,$this->day,$this->year));
	}
	public function getPrevMonthStartFDate(){
		$date_split=split('/', date("m/d/Y",mktime(0,0,0,$this->month-1,1,$this->year)));
		return new FDate((INT)$date_split[1], (INT)$date_split[0], (INT)$date_split[2]);
	}

}



class FYearIterator implements Iterator{
	private $curr_Fdate;
	private $fyear;

	public function __construct($fyear) {
		$this->fyear=$fyear;
	}

	function rewind() {
		$this->curr_Fdate = $this->fyear->getFinancialYearStartFDate();
	}

	function current() {
		return $this->curr_Fdate;
	}

	function key() {
		return $this->curr_Fdate->getMonth();
	}

	function next() {
		$this->curr_Fdate=$this->curr_Fdate->getNextMonthStartFDate();
	}

	function valid() {
		return ($this->curr_Fdate->getTime()<=$this->fyear->getFinancialYearEndFDate()->getTime());
	}
}

//function to calculate end and start dates for various headers in report. Days: Yesterday, Thu, Wed, ...., Fri Weeks: WTD, W3, W2, W1, W0. Months: MTD, M3, M2, M1. Quarters: QTD, Q3, Q2, Q1.
function calculateDates($curr_date, $curr_month ,$curr_year, $includetoday=false){
	$curr_fyear = new FYear($curr_date, $curr_month, $curr_year);
	$curr_fdate= new FDate($curr_date, $curr_month, $curr_year);
	$temp_fdate = new FDate($curr_date, $curr_month, $curr_year);
	$calc_dates = array();

	$j=0; 
	
	if($includetoday == true) {
		$curr_fdate2= new FDate($curr_date, $curr_month, $curr_year);
		$calc_dates[$j]['start'] = $curr_fdate2->toUnixDateDayStart(); 
		$calc_dates[$j]['end'] = $curr_fdate2->addDays(1)->toUnixDateDayStart();
		$calc_dates[$j]['label'] = 'Today';
		$j++;
	}
	
	// Days calculation
	for($i=$j;$i<=$j+7;$i++){
		$calc_dates[$i]['end'] = $curr_fdate->toUnixDateDayStart();
		$calc_dates[$i]['start'] = $curr_fdate->addDays(-1)->toUnixDateDayStart();
		$calc_dates[$i]['label'] = date('D', mktime(0,0,0, $curr_fdate->getMonth(), $curr_fdate->getDay(), $curr_fdate->getYear()));
	}
	
	// Renamed yesterday weekday with 'Yest'
	$calc_dates[$j]['label'] = 'Yest';
	
	//restore the current fdate to the current date
	$curr_fdate->setDay($temp_fdate->getDay());
	$curr_fdate->setMonth($temp_fdate->getMonth());
	$curr_fdate->setYear($temp_fdate->getYear());

	// Add 8 days
	$j+=8;
	
	// Weeks calculation
	$calc_dates[$j]['end'] = $curr_fdate->toUnixDateDayStart();
	$calc_dates[$j]['start'] = $curr_fdate->addDays(1 - date('N', mktime(0,0,0, $curr_month, $curr_date, $curr_year)))->toUnixDateDayStart();
	$calc_dates[$j]['label'] = 'WTD';
	
	$j++;
	
	for($i=$j,$indicator=3;$i<=$j+3;$i++,$indicator--){
		$calc_dates[$i]['end'] = $curr_fdate->toUnixDateDayStart();
		$calc_dates[$i]['start'] = $curr_fdate->addDays(-7)->toUnixDateDayStart();
		$calc_dates[$i]['label'] = 'W' . $indicator ;
	}
	//restore the current fdate to the current date
	$curr_fdate->setDay($temp_fdate->getDay());
	$curr_fdate->setMonth($temp_fdate->getMonth());
	$curr_fdate->setYear($temp_fdate->getYear());

	$j+=4;
	
	//Months calculation
	$calc_dates[$j]['end'] = $curr_fdate->toUnixDateDayStart();
	$calc_dates[$j]['start'] = $curr_fdate->getMonthStartFDate()->toUnixDateDayStart();
	$calc_dates[$j]['label'] = 'MTD';
	
	$j++;
	
	for($i=$j;$i<=$j+2;$i++){
		$calc_dates[$i]['end'] = $curr_fdate->getMonthStartFDate()->toUnixDateDayStart();
		$calc_dates[$i]['start'] = $curr_fdate->getPrevMonthStartFDate()->toUnixDateDayStart();
		$calc_dates[$i]['label'] = $curr_fdate->getPrevMonthStartFDate()->getMonthNameString();
		$curr_fdate = $curr_fdate->getPrevMonthStartFDate();
	}
	//restore the current fdate to the current date
	$curr_fdate->setDay($temp_fdate->getDay());
	$curr_fdate->setMonth($temp_fdate->getMonth());
	$curr_fdate->setYear($temp_fdate->getYear());

	$j+=3;
	
	//Quarters calculation
	$calc_dates[$j]['end'] = $curr_fdate->toUnixDateDayStart();
	$calc_dates[$j]['start'] = $curr_fyear->getQuarterStartFDate($curr_fdate)->toUnixDateDayStart();
	$calc_dates[$j]['label'] = 'QTD';
	
	$j++;
	
	for($i=$j,$indicator=3;$i<=$j+2;$i++,$indicator--){
		$qtr_end_date = $curr_fyear->getQuarterStartFDate($curr_fdate);
		$calc_dates[$i]['end'] = $qtr_end_date->toUnixDateDayStart();
		$qtr_start_date = $curr_fyear->getPrevQuarterStartFdate($qtr_end_date);
		$calc_dates[$i]['start'] = $qtr_start_date->toUnixDateDayStart();
		$calc_dates[$i]['label'] = 'Q' . $indicator;
		$curr_fdate = $qtr_start_date;
	}
	return $calc_dates;
}

//function to calculate end and start dates for various headers in report. Headers: WTD, W1, W2, W3, W4.....W12
function calculateDatesWeekly($curr_date, $curr_month ,$curr_year){
	$curr_fyear = new FYear($curr_date, $curr_month, $curr_year);
	$curr_fdate= new FDate($curr_date, $curr_month, $curr_year);
	$temp_fdate = new FDate($curr_date, $curr_month, $curr_year);
	$calc_dates = array();

	// Weeks calculation
	$calc_dates[0]['end'] = $curr_fdate->toUnixDateDayStart();
	$calc_dates[0]['start'] = $curr_fdate->addDays(1 - date('N', mktime(0,0,0, $curr_month, $curr_date, $curr_year)))->toUnixDateDayStart();
	$calc_dates[0]['label'] = 'WTD';
	for($i=1;$i<=12;$i++){
		$calc_dates[$i]['end'] = $curr_fdate->toUnixDateDayStart();
		$calc_dates[$i]['start'] = $curr_fdate->addDays(-7)->toUnixDateDayStart();
		$calc_dates[$i]['label'] = 'W' . $i ;
	}

	return $calc_dates;
}

//function to calculate end and start dates for various headers in report. Days: Last Day, Last 7 days, MTD, M3, M2, M1
function calculateDates_DaysFormat($curr_date, $curr_month ,$curr_year){
	$curr_fyear = new FYear($curr_date, $curr_month, $curr_year);
	$curr_fdate= new FDate($curr_date, $curr_month, $curr_year);
	$temp_fdate = new FDate($curr_date, $curr_month, $curr_year);
	$calc_dates = array();

	// Last Day calculation
	$calc_dates[0]['end'] = $curr_fdate->toUnixDateDayStart();
	$calc_dates[0]['start'] = $curr_fdate->addDays(-1)->toUnixDateDayStart();
	$calc_dates[0]['label'] = 'Last Day';	
	//restore the current fdate to the current date
	$curr_fdate->setDay($temp_fdate->getDay());
	$curr_fdate->setMonth($temp_fdate->getMonth());
	$curr_fdate->setYear($temp_fdate->getYear());

	// Last 7 Days calculation
	$calc_dates[1]['end'] = $curr_fdate->toUnixDateDayStart();
	$calc_dates[1]['start'] = $curr_fdate->addDays(-7)->toUnixDateDayStart();
	$calc_dates[1]['label'] = 'Last 7 Days';
	//restore the current fdate to the current date
	$curr_fdate->setDay($temp_fdate->getDay());
	$curr_fdate->setMonth($temp_fdate->getMonth());
	$curr_fdate->setYear($temp_fdate->getYear());
	
	//Months calculation
	$calc_dates[2]['end'] = $curr_fdate->toUnixDateDayStart();
	$calc_dates[2]['start'] = $curr_fdate->getMonthStartFDate()->toUnixDateDayStart();
	$calc_dates[2]['label'] = 'MTD';
	for($i=3;$i<=5;$i++){
		$calc_dates[$i]['end'] = $curr_fdate->getMonthStartFDate()->toUnixDateDayStart();
		$calc_dates[$i]['start'] = $curr_fdate->getPrevMonthStartFDate()->toUnixDateDayStart();
		$calc_dates[$i]['label'] = $curr_fdate->getPrevMonthStartFDate()->getMonthNameString();
		$curr_fdate = $curr_fdate->getPrevMonthStartFDate();
	}
	return $calc_dates;
}

//function to calculate end and start dates for various headers in report. Days: Last Day, Last 7 days, Last 30 days
function calculateDates_DaysFormat1($curr_date, $curr_month ,$curr_year){
	$curr_fyear = new FYear($curr_date, $curr_month, $curr_year);
	$curr_fdate= new FDate($curr_date, $curr_month, $curr_year);
	$temp_fdate = new FDate($curr_date, $curr_month, $curr_year);
	$calc_dates = array();

	// Last Day calculation
	$calc_dates[0]['end'] = $curr_fdate->toUnixDateDayStart();
	$calc_dates[0]['start'] = $curr_fdate->addDays(-1)->toUnixDateDayStart();
	$calc_dates[0]['label'] = 'Last Day';	
	//restore the current fdate to the current date
	$curr_fdate->setDay($temp_fdate->getDay());
	$curr_fdate->setMonth($temp_fdate->getMonth());
	$curr_fdate->setYear($temp_fdate->getYear());

	// Last 7 Days calculation
	$calc_dates[1]['end'] = $curr_fdate->toUnixDateDayStart();
	$calc_dates[1]['start'] = $curr_fdate->addDays(-7)->toUnixDateDayStart();
	$calc_dates[1]['label'] = 'Last 7 Days';
	//restore the current fdate to the current date
	$curr_fdate->setDay($temp_fdate->getDay());
	$curr_fdate->setMonth($temp_fdate->getMonth());
	$curr_fdate->setYear($temp_fdate->getYear());
	
	//Last 30 days calculation
	$calc_dates[2]['end'] = $curr_fdate->toUnixDateDayStart();
	$calc_dates[2]['start'] = $curr_fdate->addDays(-30)->toUnixDateDayStart();
	$calc_dates[2]['label'] = 'Last 30 days';
	
	return $calc_dates;
}
