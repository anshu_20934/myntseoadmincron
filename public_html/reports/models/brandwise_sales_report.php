<?php

global $xcart_dir;
include_once $xcart_dir.'/reports/models/models.php';
include_once $xcart_dir.'/reports/rendrers/rendrers.php';
include_once REP_DIR.'/models/date_calculation.php';

class BrandWiseSalesReportModel extends ReportModel {

	public function __construct(){
		parent::__construct();
		$this->name="Brand Wise Sales Report : " . date("j-M-Y");

	}
	protected function initFilters(){
		$this->addFilter("Date:" ,self::$FILTER_TYPE_DATE);
	}

	public function loadDefaultFilterValues(){
		$time=time();
		$this->setFilterValue("Date:", date("m/d/Y",$time));
	}


	public function buildReport(){
		global $weblog;
		$table= new Table();
		$group= new TableGroup("Brand Wise Sales Report");
		$headers= array();
		$headers[0]= 'Brands';
		$headers[1]= 'Facts';
		$table->setFooterVisible(false);
		$filtervalue = $this->getFilterValue("Date:");
		$date_split=split('/', $filtervalue);
		$current_month=(INT)$date_split[0];
		$current_year=(INT)$date_split[2];
		$current_date=(INT)$date_split[1];
		$ReportData;
		//function to calculate the end and start days for various headers.
		$calculated_dates = calculateDates($current_date, $current_month, $current_year);
		for($cnt=0;$cnt<count($calculated_dates);$cnt++){
			$sql = "select (case true when mv1.brands is null or mv1.brands ='' then 'Others' else mv1.brands end) as brand, sum(mv1.orders) as orders, sum(mv1.items) as items, sum(mv1.revenue) as revenue from
					(select sp.global_attr_brand as brands ,count(distinct o.orderid) as orders, sum(od.amount) as items , sum(((od.amount*od.price)/o.subtotal)*(o.total+o.shipping_cost+o.gift_charges)) as revenue
         			from xcart_orders o join xcart_order_details od on od.orderid = o.orderid and o.status in ('Q','WP','OH','C','SH','DL') and o.date >= unix_timestamp('" . $calculated_dates[$cnt]['start'] . "') and o.date< unix_timestamp('" . $calculated_dates[$cnt]['end'] . "') 
					left join mk_style_properties sp on sp.style_id = od.product_style group by brands) as mv1 group by brand";
			$result=func_query($sql, true);
			foreach ($result as $row){
				$ReportData[$row['brand']]['Orders'][$cnt]= (float)$row['orders'];
				$ReportData[$row['brand']]['Items'][$cnt]=(float)$row['items'];
				$ReportData[$row['brand']]['Revenue'][$cnt]=(float)$row['revenue'];
				$ReportData[$row['brand']]['ASP'][$cnt]=(float)$row['revenue']/(float)$row['items'];
				$ReportData[$row['brand']]['Name']=$row['brand'];
			}
			$headers[$cnt+2] = $calculated_dates[$cnt]['label'];
		}
		// To get those brands which didnt generate any revenue till date, we first find all the brands in system and then subtract the list from the above ReportData list.
		$sql = "select distinct global_attr_brand from mk_style_properties order by global_attr_brand";
		$result = func_query($sql, true);
		foreach ($result as $row){
			$nullFlag = true;
			if($row['global_attr_brand'] == null)
			continue;
			foreach($ReportData as $brandsData){
				if($row['global_attr_brand'] == $brandsData['Name']){
					$nullFlag = false;
					break;
				}
			}
			if($nullFlag == true)
			$ReportData[$row['global_attr_brand']]['Name']= $row['global_attr_brand'];
		}
		ksort($ReportData);
		//To calculate the total orders, items and revenue
		for($cnt=0;$cnt<count($calculated_dates);$cnt++){
			$sql = "select count(distinct o.orderid) as orders, sum(od.amount) as items , sum(((od.amount*od.price)/o.subtotal)*(o.total+o.shipping_cost+o.gift_charges)) as revenue
         			from xcart_orders o join xcart_order_details od on od.orderid = o.orderid and o.status in ('Q','WP','OH','C','SH','DL') and o.date >= unix_timestamp('" . $calculated_dates[$cnt]['start'] . "') and o.date< unix_timestamp('" . $calculated_dates[$cnt]['end'] . "')
					left join mk_style_properties sp on sp.style_id = od.product_style";
			$result = func_query($sql, true);
			$TotalArray['Orders'][$cnt] = (float)$result[0]['orders'];
			$TotalArray['Items'][$cnt] = (float)$result[0]['items'];
			$TotalArray['Revenue'][$cnt] = (float)$result[0]['revenue'];
			$TotalArray['ASP'][$cnt] = 	$TotalArray['Revenue'][$cnt]/$TotalArray['Items'][$cnt];		
		}
		$width = $cnt+2;
		$table->setHeaders($headers);
		$table->setTableWidth($width);
		$rowHeaderValues = array('Orders', 'Items', 'Revenue', 'ASP');
		$i=0;
		//set report data in cells of table
		$table->setCellData($i, 0, 'Total');
		$table->setCellStyle($i, 0, TABLE::$ROWSPAN, "4");
		$table->setCellStyle($i, 0, TABLE::$STYLE_ALIGN, "right");
		$j=1;
		for($cnt=0; $cnt<count($rowHeaderValues); $cnt++){
			$table->setCellData($i, $j, $rowHeaderValues[$cnt],'y');
			for ($j=1; $j<$width-1; $j++){
				if($cnt%4==0){ //If first row of the 4 row-set, then increase the col. number because of rowspan variable.
					$table->setCellData($i, $j+1, $TotalArray[$rowHeaderValues[$cnt]][$j-1],'y');
				}
				else{
					$table->setCellData($i, $j, $TotalArray[$rowHeaderValues[$cnt]][$j-1]);
				}
			}
			$i++;
			$j=0;
		}
		foreach($ReportData as $rowdata){
			$table->setCellData($i, 0, $rowdata['Name']);
			$table->setCellStyle($i, 0, TABLE::$ROWSPAN, "4");
			$table->setCellStyle($i, 0, TABLE::$STYLE_ALIGN, "right");
			$j=1;
			for($cnt=0; $cnt<count($rowHeaderValues); $cnt++){
				$table->setCellData($i, $j, $rowHeaderValues[$cnt],'y');
				for ($j=1; $j<$width-1; $j++){
					if($cnt%4==0){ //If first of the 4 row-set, then increase the col. number because of rowspan variable.
						$table->setCellData($i, $j+1, $rowdata[$rowHeaderValues[$cnt]][$j-1],'y');
					}
					else{
						$table->setCellData($i, $j, $rowdata[$rowHeaderValues[$cnt]][$j-1]);
					}
				}
				$i++;
				$j=0;
			}
		}
		//set formatting for columns of tables, convoluted because of rowspan logic.
		for($cnt=0;$cnt<$i;$cnt++){
			if($cnt%4==0){ //if first of the 4 row-set then adjust the col number. needed due to rowspan variable.
				$table->setStyleMatrix($cnt, 2, $cnt, 9, TABLE::$STYLE_COLOR, "#EDF2F8");
				$table->setStyleMatrix($cnt, 10, $cnt, 14, TABLE::$STYLE_COLOR, "#CAD9EB");
				$table->setStyleMatrix($cnt, 15, $cnt, 18, TABLE::$STYLE_COLOR, "#EDF2F8");
				$table->setStyleMatrix($cnt, 19, $cnt, 22, TABLE::$STYLE_COLOR, "#CAD9EB");
				$table->setStyleMatrix($cnt, 0, $cnt, $width-1, TABLE::$STYLE_BORDER, "border-top: 1px solid #000000");
			}
			else{
				$table->setStyleMatrix($cnt, 1, $cnt, 8, TABLE::$STYLE_COLOR, "#EDF2F8");
				$table->setStyleMatrix($cnt, 9, $cnt, 13, TABLE::$STYLE_COLOR, "#CAD9EB");
				$table->setStyleMatrix($cnt, 14, $cnt, 17, TABLE::$STYLE_COLOR, "#EDF2F8");
				$table->setStyleMatrix($cnt, 18, $cnt, 21, TABLE::$STYLE_COLOR, "#CAD9EB");
			}
		}
		for($cnt=2;$cnt<$i;$cnt=$cnt+4){
			$table->setRowStyle($cnt, Table::$FORMAT, "currency");
			$table->setRowStyle($cnt+1, Table::$FORMAT, "currency");
		}
		$group->addTable($table);
		$this->addTableGroup($group);
		$this->isBuild=true;
		$this->notes = "WTD = Stats from 00:00 Monday to 23:59 Yesterday <br>
						W3 = " . $calculated_dates[9]['start'] . " to " . $calculated_dates[9]['end'] ."<br> 
						W2 = " . $calculated_dates[10]['start'] . " to " . $calculated_dates[10]['end'] ."<br> 
						W1 = " .$calculated_dates[11]['start'] . " to " . $calculated_dates[11]['end'] . "<br> 
						W0 = " . $calculated_dates[12]['start'] . " to " . $calculated_dates[12]['end'] . "<br><br>";
	}


}
?>

