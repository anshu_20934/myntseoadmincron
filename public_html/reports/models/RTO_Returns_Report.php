<?php

global $xcart_dir;
include_once $xcart_dir.'/reports/models/models.php';
include_once $xcart_dir.'/reports/rendrers/rendrers.php';
include_once REP_DIR.'/models/date_calculation.php';

class RTOReturnsReportModel extends ReportModel {

	protected $mode;
	public function __construct(){
		parent::__construct();
		$this->name="RTO and Returns Daily Report : " . date("j-M-Y");

	}
	protected function initFilters(){
		$this->addFilter("Date:" ,self::$FILTER_TYPE_DATE);
	}

	public function loadDefaultFilterValues(){
		$time=time();
		$this->setFilterValue("Date:", date("m/d/Y",$time));
	}

	public function returnsInfo($calculated_dates, $ordersShipped){
		$table= new Table('Returns');
		$headers[0]= 'Facts';

		$table->setCellData(0, 0, 'No. Queued');
		$table->setCellData(1, 0, 'No. of Closures');
		$table->setCellData(2, 0, 'Returns %');
		for($cnt=0;$cnt<count($calculated_dates);$cnt++){
			$sql = "select count(*) as num from mk_old_returns_tracking where returntype='RT' and queued_date >= '" . $calculated_dates[$cnt]['start'] . "' and queued_date < '" . $calculated_dates[$cnt]['end'] . "'";
			$returns_queued=func_query($sql, true);
			$sql = "select count(*) as num from (select rta.returnid, max(rta.recorddate) as lastActivity from mk_old_returns_tracking rt, mk_old_returns_tracking_activity rta where rt.id=rta.returnid and rt.returntype='RT' and rt.currstate='END' group by rta.returnid having lastActivity >'" . $calculated_dates[$cnt]['start'] . "' and lastActivity < '" . $calculated_dates[$cnt]['end'] . "') taba";
			$returns_closed=func_query($sql, true);
			$headers[$cnt+1] = $calculated_dates[$cnt]['label'];
			$table->setCellData(0, $cnt+1, (float)$returns_queued[0]['num']);
			$table->setCellData(1, $cnt+1, (float)$returns_closed[0]['num']);
			$table->setCellData(2, $cnt+1, (float)$returns_queued[0]['num']*100/$ordersShipped[$cnt]);
		}
		$table->setHeaders($headers);
		$table->setTableWidth(count($headers));
		$table->setFooterVisible(false);
		return $table;
	}

	public function rtoInfo($calculated_dates, $ordersShipped){
		$table= new Table('RTOs');
		$headers[0]= 'Facts';

		$table->setCellData(0, 0, 'No. Queued');
		$table->setCellData(1, 0, 'No. of Closures');
		$table->setCellData(2, 0, 'RTO %');
		for($cnt=0;$cnt<count($calculated_dates);$cnt++){
			$sql = "select count(*) as num from mk_old_returns_tracking where returntype='RTO' and queued_date >= '" . $calculated_dates[$cnt]['start'] . "' and queued_date < '" . $calculated_dates[$cnt]['end'] . "'";
			$returns_queued=func_query($sql, true);
			$sql = "select count(*) as num from (select rta.returnid, max(rta.recorddate) as lastActivity from mk_old_returns_tracking rt, mk_old_returns_tracking_activity rta where rt.id=rta.returnid and rt.returntype='RTO' and rt.currstate='END' group by rta.returnid having lastActivity >'" . $calculated_dates[$cnt]['start'] . "' and lastActivity < '" . $calculated_dates[$cnt]['end'] . "') taba";
			$returns_closed=func_query($sql, true);
			$headers[$cnt+1] = $calculated_dates[$cnt]['label'];
			$table->setCellData(0, $cnt+1, (float)$returns_queued[0]['num']);
			$table->setCellData(1, $cnt+1, (float)$returns_closed[0]['num']);
			$table->setCellData(2, $cnt+1, (float)$returns_queued[0]['num']*100/$ordersShipped[$cnt]);
		}
		$table->setHeaders($headers);
		$table->setTableWidth(count($headers));
		$table->setFooterVisible(false);
		return $table;
	}

	public function buildReport(){
		global $weblog;
		$group= new TableGroup("RTO and Returns Daily Report");
		$overallTable= new Table("Overall");
		$headers[0]= 'Facts';
		$filtervalue = $this->getFilterValue("Date:");
		$date_split=split('/', $filtervalue);
		$current_month=(INT)$date_split[0];
		$current_year=(INT)$date_split[2];
		$current_date=(INT)$date_split[1];
		//function to calculate the end and start days for various headers.
		$calculated_dates = calculateDates_DaysFormat1($current_date, $current_month, $current_year);
		$overallTable->setCellData(0, 0, 'Orders Placed');
		$overallTable->setCellData(1, 0, 'Orders Shipped');
		$ordersShipped;
		for($cnt=0;$cnt<count($calculated_dates);$cnt++){
			$sql = "select count(*) as num from xcart_orders where status in ('Q','WP','OH','C','SH','DL') and date >= unix_timestamp('" . $calculated_dates[$cnt]['start'] . "') and date< unix_timestamp('" . $calculated_dates[$cnt]['end'] . "')";
			$orders_placed=func_query($sql, true);
			$sql = "select count(*) as num from xcart_orders where status in ('C','SH','DL') and shippeddate >= unix_timestamp('" . $calculated_dates[$cnt]['start'] . "') and shippeddate< unix_timestamp('" . $calculated_dates[$cnt]['end'] . "')";
			$orders_shipped=func_query($sql, true);
			$headers[$cnt+1] = $calculated_dates[$cnt]['label'];
			$overallTable->setCellData(0, $cnt+1, (float)$orders_placed[0]['num']);
			$overallTable->setCellData(1, $cnt+1, (float)$orders_shipped[0]['num']);
			$ordersShipped[$cnt]=(float)$orders_shipped[0]['num'];
		}
		$overallTable->setHeaders($headers);
		$overallTable->setTableWidth(count($headers));
		$overallTable->setFooterVisible(false);

		//make other tables
		$returnsTable = $this->returnsInfo($calculated_dates, $ordersShipped);
		$rtoTable = $this->rtoInfo($calculated_dates, $ordersShipped);

		$group->addTable($overallTable);
		$group->addTable($returnsTable);
		$group->addTable($rtoTable);
		$this->addTableGroup($group);

		$this->isBuild=true;
	}


}
?>

