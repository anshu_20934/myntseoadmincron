<?php

global $xcart_dir;
include_once $xcart_dir.'/reports/models/models.php';
include_once $xcart_dir.'/reports/rendrers/rendrers.php';
include_once REP_DIR.'/models/date_calculation.php';

class InventoryReportModel extends ReportModel {

	public function __construct(){
		parent::__construct();
		$this->name="Inventory Report : " . date("j-M-Y");

	}
	protected function initFilters(){
		$this->addFilter("Date:" ,self::$FILTER_TYPE_DATE);
	}

	public function loadDefaultFilterValues(){
		$time=time();
		$this->setFilterValue("Date:", date("m/d/Y",$time));
	}

	/*public function noOutwardsInfo(){
		$table= new Table('Count of SKUs that did not have outwards in the last 30 days');
		$headers = array('Brand','Category','# SKUs');
		$table->setHeaders($headers);
		$table->setFooterVisible(false);

		$sql = "select sp.global_attr_brand, (select typename from mk_catalogue_classification where id= sp.global_attr_sub_category) as subCat, count(*) as numSKUs from
				(select ci.sku_id from myntra_auditor.audit_trail atrail  join myntra.core_items ci where ci.id=atrail.entityId and entityName = 'com.myntra.framework.entities.Item' and 
				entityProperty='itemStatus' and entityPropertyNewValue not in ('ISSUED', 'RETURNED') and actionTime >= ('2011-10-01') and actionTime < ('2011-10-28') group by ci.sku_id 
				) taba left join myntra.mk_styles_options_skus_mapping osm on osm.sku_id=taba.sku_id left join mk_style_properties sp on osm.style_id=sp.style_id group by sp.global_attr_brand, sp.global_attr_sub_category";
		$result=func_query($sql, true);
		$i=0;
		foreach ($result as $row){			
			$table->setCellData($i, 0, $result['global_attr_brand']);
			$table->setCellData($i, 1, $result['subCat']);
			$table->setCellData($i, 2, $result['numSKUs']);
			$i++;
		}
	}*/

	public function buildReport(){
		global $weblog;
		$table= new Table('Yesterday');
		$group= new TableGroup("Inventory Report");
		$superHeaders= array('Inwards'=>3,'Outwards'=>3,'Closing'=>3);
		$headers= array('# Items', '# Articles', 'Value (Cost)', '# Items', '# Articles', 'Value (Cost)', '# Items', '# Articles', 'Value (Cost)');
		$table->setSuperHeaders($superHeaders);
		$table->setHeaders($headers);
		$table->setFooterVisible(false);
		$filtervalue = $this->getFilterValue("Date:");
		$date_split=split('/', $filtervalue);
		$current_month=(INT)$date_split[0];
		$current_year=(INT)$date_split[2];
		$current_date=(INT)$date_split[1];
		$calculated_dates = calculateDates($current_date, $current_month, $current_year);
		$ReportData;
		$dates_required = array(0,13,17);  //array(0,13,17)
		for($cnt=0;$cnt<count($dates_required);$cnt++){
			$sql="select count(*) as items from myntra_auditor.audit_trail where entityName = 'com.myntra.framework.entities.Item' and entityProperty='itemStatus'
				and entityPropertyNewValue = 'STORED' and actionTime >= ('" . $calculated_dates[$dates_required[$cnt]]['start'] . "') and actionTime < ('" . $calculated_dates[$dates_required[$cnt]]['end'] . "')";
			$result_inwardItems=func_query($sql, true);
			$sql="select count(distinct vendor_article_no) as styles from myntra_auditor.audit_trail atrail join myntra.core_items ci join myntra.mk_skus s where ci.id=atrail.entityId and ci.sku_id = s.id and atrail.entityName = 'com.myntra.framework.entities.Item' and atrail.entityProperty='itemStatus'
				and atrail.entityPropertyNewValue = 'STORED' and atrail.actionTime >= ('" . $calculated_dates[$dates_required[$cnt]]['start'] . "') and atrail.actionTime < ('" . $calculated_dates[$dates_required[$cnt]]['end'] . "')";
			$result_inwardStyles=func_query($sql, true);
			$sql="select sum(tbl_outer.price) as value from (select posku.price from myntra_auditor.audit_trail atrail join myntra.core_items ci join myntra.core_po_skus posku where ci.id=atrail.entityId and ci.po_sku_id = posku.id and atrail.entityName = 'com.myntra.framework.entities.Item' and atrail.entityProperty='itemStatus'
				and atrail.entityPropertyNewValue = 'STORED' and atrail.actionTime >= ('" . $calculated_dates[$dates_required[$cnt]]['start'] . "') and atrail.actionTime < ('" . $calculated_dates[$dates_required[$cnt]]['end'] . "') group by ci.id) tbl_outer";
			$result_inwardValue=func_query($sql, true);
				
			$sql="select count(*) as items from myntra_auditor.audit_trail where entityName = 'com.myntra.framework.entities.Item' and entityProperty='itemStatus'
				and entityPropertyOldValue = 'STORED' and actionTime >= ('" . $calculated_dates[$dates_required[$cnt]]['start'] . "') and actionTime < ('" . $calculated_dates[$dates_required[$cnt]]['end'] . "')";
			$result_outwardItems=func_query($sql, true);
			$sql="select count(distinct vendor_article_no) as styles from myntra_auditor.audit_trail atrail join myntra.core_items ci join myntra.mk_skus s where ci.id=atrail.entityId and ci.sku_id = s.id and atrail.entityName = 'com.myntra.framework.entities.Item' and atrail.entityProperty='itemStatus'
				and atrail.entityPropertyOldValue = 'STORED' and atrail.actionTime >= ('" . $calculated_dates[$dates_required[$cnt]]['start'] . "') and atrail.actionTime < ('" . $calculated_dates[$dates_required[$cnt]]['end'] . "')";
			$result_outwardStyles=func_query($sql, true);
			$sql="select sum(tbl_outer.price) as value from (select posku.price from myntra_auditor.audit_trail atrail join myntra.core_items ci join myntra.core_po_skus posku where ci.id=atrail.entityId and ci.po_sku_id = posku.id and atrail.entityName = 'com.myntra.framework.entities.Item' and atrail.entityProperty='itemStatus'
				and atrail.entityPropertyOldValue = 'STORED' and atrail.actionTime >= ('" . $calculated_dates[$dates_required[$cnt]]['start'] . "') and atrail.actionTime < ('" . $calculated_dates[$dates_required[$cnt]]['end'] . "') group by ci.id) tbl_outer";
			$result_outwardValue=func_query($sql, true);
				
			$ReportData[$cnt]['inwardItems'] = $result_inwardItems[0]['items'];
			$ReportData[$cnt]['inwardStyles'] = $result_inwardStyles[0]['styles'];
			$ReportData[$cnt]['inwardValue'] = $result_inwardValue[0]['value'] == null ? 0 : $result_inwardValue[0]['value'];
			$ReportData[$cnt]['outwardItems'] = $result_outwardItems[0]['items'];
			$ReportData[$cnt]['outwardStyles'] = $result_outwardStyles[0]['styles'];
			$ReportData[$cnt]['outwardValue'] = $result_outwardValue[0]['value'] == null ? 0 : $result_outwardValue[0]['value'];
		}

		$sql="select count(*) as items from core_items where item_status in ('STORED', 'FOUND', 'CUSTOMER_RETURNED','RETURN_FROM_OPS','ISSUED', 'PROCESSING')";
		$result_closingItems=func_query($sql, true);
		//Closing Styles: some skus might not be mapped to a style yet , so the closing styles will look lesser than they actually are
		$sql="select count(distinct osm.style_id) as styles from myntra.core_items ci join myntra.mk_styles_options_skus_mapping osm where ci.sku_id = osm.sku_id and 
			ci.item_status in ('STORED', 'FOUND', 'CUSTOMER_RETURNED','RETURN_FROM_OPS','ISSUED', 'PROCESSING') ";
		$result_closingStyles=func_query($sql, true);
		$sql="select sum(posku.price) as value from core_items ci join myntra.core_po_skus posku on ci.po_sku_id=posku.id where ci.item_status in 
			('STORED', 'FOUND', 'CUSTOMER_RETURNED','RETURN_FROM_OPS','ISSUED', 'PROCESSING')";
		$result_closingValue=func_query($sql, true);

		$j=0;
		$table->setCellData(0, $j++, $ReportData[0]['inwardItems']);
		$table->setCellData(0, $j++, $ReportData[0]['inwardStyles']);
		$table->setCellData(0, $j++, (float)$ReportData[0]['inwardValue']);
		$table->setCellData(0, $j++, $ReportData[0]['outwardItems']);
		$table->setCellData(0, $j++, $ReportData[0]['outwardStyles']);
		$table->setCellData(0, $j++, (float)$ReportData[0]['outwardValue']);
		$table->setCellData(0, $j++, $result_closingItems[0]['items']);
		$table->setCellData(0, $j++, $result_closingStyles[0]['styles']);
		$table->setCellData(0, $j++, (float)$result_closingValue[0]['value']);
		$group->addTable($table);

		// Preparing MTD table
		$table_MTD = new Table('MTD');
		$table_MTD->setSuperHeaders($superHeaders);
		$table_MTD->setHeaders($headers);
		$table_MTD->setFooterVisible(false);

		$j=0;
		$table_MTD->setCellData(0, $j++, $ReportData[1]['inwardItems']);
		$table_MTD->setCellData(0, $j++, $ReportData[1]['inwardStyles']);
		$table_MTD->setCellData(0, $j++, (float)$ReportData[1]['inwardValue']);
		$table_MTD->setCellData(0, $j++, $ReportData[1]['outwardItems']);
		$table_MTD->setCellData(0, $j++, $ReportData[1]['outwardStyles']);
		$table_MTD->setCellData(0, $j++, (float)$ReportData[1]['outwardValue']);
		$table_MTD->setCellData(0, $j++, $result_closingItems[0]['items']);
		$table_MTD->setCellData(0, $j++, $result_closingStyles[0]['styles']);
		$table_MTD->setCellData(0, $j++, (float)$result_closingValue[0]['value']);
		$group->addTable($table_MTD);

		// Preparing QTD table
		$table_QTD = new Table('QTD');
		$table_QTD->setSuperHeaders($superHeaders);
		$table_QTD->setHeaders($headers);
		$table_QTD->setFooterVisible(false);

		$j=0;
		$table_QTD->setCellData(0, $j++, $ReportData[2]['inwardItems']);
		$table_QTD->setCellData(0, $j++, $ReportData[2]['inwardStyles']);
		$table_QTD->setCellData(0, $j++, (float)$ReportData[2]['inwardValue']);
		$table_QTD->setCellData(0, $j++, $ReportData[2]['outwardItems']);
		$table_QTD->setCellData(0, $j++, $ReportData[2]['outwardStyles']);
		$table_QTD->setCellData(0, $j++, (float)$ReportData[2]['outwardValue']);
		$table_QTD->setCellData(0, $j++, $result_closingItems[0]['items']);
		$table_QTD->setCellData(0, $j++, $result_closingStyles[0]['styles']);
		$table_QTD->setCellData(0, $j++, (float)$result_closingValue[0]['value']);
		$group->addTable($table_QTD);

		// Make other table for brand-category wise non-outwarded sku count for last month
		/*$noOutwardsTable = $this->noOutwardsInfo();
		$group->addTable($noOutwardsTable);
*/		
		$this->addTableGroup($group);
		$this->isBuild=true;
		unset($ReportData);
		unset($info);
		unset($result);

	}


}
?>

