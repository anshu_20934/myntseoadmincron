<?php

global $xcart_dir;
include_once $xcart_dir.'/reports/models/models.php';
include_once $xcart_dir.'/reports/rendrers/rendrers.php';
include_once REP_DIR.'/models/date_calculation.php';

class CustomerExperienceReportModel extends ReportModel {

	public function __construct(){
		parent::__construct();
		$this->name="Customer Experience Report : " . date("j-M-Y");

	}
	protected function initFilters(){
		$this->addFilter("Date:" ,self::$FILTER_TYPE_DATE);
	}

	public function loadDefaultFilterValues(){
		$time=time();
		$this->setFilterValue("Date:", date("m/d/Y",$time));
	}

	public function openOrders($rowHeaders){
		$table= new Table();
		$headers = array("Open (Shipped Orders)", "Current Status");
		$table->setHeaders($headers);
		$table->setCellData(0, 0, '< 2');
		$table->setCellData(1, 0, '< 4');
		$table->setCellData(2, 0, '< 7');
		$table->setCellData(3, 0, '< 14');
		$table->setCellData(4, 0, '< 21');
		$table->setCellData(5, 0, '> 21');
		$sql = "select count(*) as total, (case true when (unix_timestamp(curdate()) - shippeddate)/(3600*24) <2 then '<2' when (unix_timestamp(curdate()) - shippeddate)/(3600*24) <4 then '<4'
			 when (unix_timestamp(curdate()) - shippeddate)/(3600*24) <7 then '<7' when (unix_timestamp(curdate()) - shippeddate)/(3600*24) <14 then '<14' when (unix_timestamp(curdate()) - shippeddate)/(3600*24) <21 then '<21' 
			 else '>21' end) as daysAfterShipped from xcart_orders 
			where status = 'SH' group by daysAfterShipped ";
		$result_openOrders=func_query($sql, true);
		$temp[$result_openOrders[0]['daysAfterShipped']]=(float)$result_openOrders[0]['total'];
		$temp[$result_openOrders[1]['daysAfterShipped']]=(float)$result_openOrders[1]['total'];
		$temp[$result_openOrders[2]['daysAfterShipped']]=(float)$result_openOrders[2]['total'];
		$temp[$result_openOrders[3]['daysAfterShipped']]=(float)$result_openOrders[3]['total'];
		$temp[$result_openOrders[4]['daysAfterShipped']]=(float)$result_openOrders[4]['total'];
		$temp[$result_openOrders[5]['daysAfterShipped']]=(float)$result_openOrders[5]['total'];
		
		$val = 0;
		for($i=0;$i<count($rowHeaders) ;$i++){
			$val += $temp[$rowHeaders[$i]];
			$table->setCellData($i, 1, $val);
		}
		
		$table->setFooterVisible(false);
		return $table;
	}

	public function buildReport(){
		global $weblog;
		$group= new TableGroup("Customer Experience Report");
		$table= new Table();
		$headers[0]= 'Facts';
		$curr_date = date("m/d/Y",time());
		$date_split=split('/', $curr_date);
		$current_month=(INT)$date_split[0];
		$current_year=(INT)$date_split[2];
		$current_date=(INT)$date_split[1];
		//function to calculate the end and start days for various headers.
		$calculated_dates = calculateDates($current_date, $current_month, $current_year);
		$dates_required = array(15,14,13,9,10,11,12);
		$rowHeaders = array('<2','<4','<7','<14','<21','>21');
		$table->setCellData(0, 0,'# Orders Delivered in below days ');
		$table->setCellData(1, 0, '< 2');
		$table->setCellData(2, 0, '< 4');
		$table->setCellData(3, 0, '< 7');
		$table->setCellData(4, 0, '< 14');
		$table->setCellData(5, 0, '< 21');
		$table->setCellData(6, 0, '> 21');
		$table->setCellData(7, 0, 'Orders queued');
		$table->setCellData(8, 0, 'Orders Shipped');
		$table->setCellData(9, 0, 'Orders Delivered');
		$table->setCellData(10, 0, 'Orders Cancelled');
		$table->setCellData(11, 0, 'Orders in RTO');
		$table->setCellData(12, 0, 'Orders Returned');
		$table->setCellData(13, 0, '# New Customers');
		$table->setCellData(14, 0, '# Repeat Customers');
		$ReportData;
		$temp;
		$custTypeArr;

		for($cnt=0; $cnt <count($dates_required);$cnt++){
			$temp= array();
			$sql1="select count(*) as total, (case true when (delivereddate - shippeddate)/(3600*24) <2 then '<2' when (delivereddate - shippeddate)/(3600*24) <4 then '<4'
 			when (delivereddate - shippeddate)/(3600*24) <7 then '<7' when (delivereddate - shippeddate)/(3600*24) <14 then '<14' when (delivereddate - shippeddate)/(3600*24) <21 then '<21' 
			else '>21' end) as deliverDays from xcart_orders where delivereddate > unix_timestamp('" . $calculated_dates[$dates_required[$cnt]]['start'] ."')
			and delivereddate < unix_timestamp('" . $calculated_dates[$dates_required[$cnt]]['end'] ."') group by deliverDays";
			$result_delivered=func_query($sql1, true);
			$sql ="select count(*) as orders from xcart_orders where queueddate >unix_timestamp('" . $calculated_dates[$dates_required[$cnt]]['start'] ."') and queueddate < unix_timestamp('" . $calculated_dates[$dates_required[$cnt]]['end'] ."')";
			$result_queuedTotal=func_query($sql, true);
			$sql ="select count(*) as orders from xcart_orders where shippeddate >unix_timestamp('" . $calculated_dates[$dates_required[$cnt]]['start'] ."') and shippeddate < unix_timestamp('" . $calculated_dates[$dates_required[$cnt]]['end'] ."')";
			$result_shippedTotal=func_query($sql, true);
			$sql ="select count(*) as orders from xcart_orders where delivereddate >unix_timestamp('" . $calculated_dates[$dates_required[$cnt]]['start'] ."') and delivereddate < unix_timestamp('" . $calculated_dates[$dates_required[$cnt]]['end'] ."')";
			$result_deliveredTotal=func_query($sql, true);
			$sql ="select count(*) as orders from xcart_orders where canceltime >unix_timestamp('" . $calculated_dates[$dates_required[$cnt]]['start'] ."') and canceltime < unix_timestamp('" . $calculated_dates[$dates_required[$cnt]]['end'] ."')";
			$result_cancelTotal=func_query($sql, true);
			$sql ="select count(*) as orders from mk_old_returns_tracking where returntype='RTO' and queued_date >('" . $calculated_dates[$dates_required[$cnt]]['start'] ."') and queued_date < ('" . $calculated_dates[$dates_required[$cnt]]['end'] ."')";
			$result_rtoTotal=func_query($sql, true);
			$sql ="select count(*) as orders from mk_old_returns_tracking where returntype='RT' and queued_date > ('" . $calculated_dates[$dates_required[$cnt]]['start'] ."') and queued_date < ('" . $calculated_dates[$dates_required[$cnt]]['end'] ."')";
			$result_returnsTotal=func_query($sql, true);
			$sql = "select b.ctype as type,count(distinct b.logins) as custs from (
				select (case exists (select distinct login from xcart_orders where login=a.login and
				 date < a.date and status in ('C','Q','WP','OH','SH','DL')) when true
				                    then 'REP' else 'NEW'
				                    end ) as ctype, (a.login) as logins
				from xcart_orders as a where a.status in ('C','Q','WP','OH','SH','DL') and a.date >=unix_timestamp('" . $calculated_dates[$dates_required[$cnt]]['start'] ."') 				
				and  a.date <unix_timestamp('" . $calculated_dates[$dates_required[$cnt]]['end'] ."')) as b  group by b.ctype";
			$result_custTypes=func_query($sql, true);
			$custTypeArr[$result_custTypes[0]['type']] = $result_custTypes[0]['custs'];
			$custTypeArr[$result_custTypes[1]['type']] = $result_custTypes[1]['custs'];
			//$sql="select count(*) as total from xcart_orders  where delivereddate > unix_timestamp('" . $calculated_dates[$dates_required[$cnt]]['start'] ."') and delivereddate < unix_timestamp('" . $calculated_dates[$dates_required[$cnt]]['end'] ."')";
			//$result_totalDelivered=func_query($sql, true);
			$temp[$result_delivered[0]['deliverDays']]=(float)$result_delivered[0]['total'];
			$temp[$result_delivered[1]['deliverDays']]=(float)$result_delivered[1]['total'];
			$temp[$result_delivered[2]['deliverDays']]=(float)$result_delivered[2]['total'];
			$temp[$result_delivered[3]['deliverDays']]=(float)$result_delivered[3]['total'];
			$temp[$result_delivered[4]['deliverDays']]=(float)$result_delivered[4]['total'];
			$temp[$result_delivered[5]['deliverDays']]=(float)$result_delivered[5]['total'];
			/*for($icnt=0; $icnt <count($rowHeaders);$icnt++){
				$ReportData[$cnt][$rowHeaders[$icnt]]=$temp[$rowHeaders[$icnt]]*100/$result_totalDelivered[0]['total'];
				}*/
			$ReportData['DLOrdersBreakup'][$cnt]=$temp;
			$ReportData['QueuedOrders'][$cnt]=$result_queuedTotal[0]['orders'];
			$ReportData['ShippedOrders'][$cnt]=$result_shippedTotal[0]['orders'];
			$ReportData['DeliveredOrders'][$cnt]=$result_deliveredTotal[0]['orders'];
			$ReportData['CancelOrders'][$cnt]=$result_cancelTotal[0]['orders'];
			$ReportData['RTOOrders'][$cnt]=$result_rtoTotal[0]['orders'];
			$ReportData['ReturnOrders'][$cnt]=$result_returnsTotal[0]['orders'];
			$ReportData['NewCusts'][$cnt]=$custTypeArr['NEW'];
			$ReportData['RepeatCusts'][$cnt]=$custTypeArr['REP'];
				
			$headers[$cnt+1] = $calculated_dates[$dates_required[$cnt]]['label'];
		}
		$j=1;
		foreach ($ReportData['DLOrdersBreakup'] as $col){
			$val =0;
			for($i=0;$i<count($rowHeaders) ;$i++){
				$val += $col[$rowHeaders[$i]];
				$table->setCellData($i+1, $j, $val);
			}
			$j++;
		}
		$rowIdentifiers = array('QueuedOrders', 'ShippedOrders', 'DeliveredOrders', 'CancelOrders','RTOOrders', 'ReturnOrders', 'NewCusts', 'RepeatCusts');
		foreach ($rowIdentifiers as $row){
			for ($j=1; $j<=$cnt; $j++){
				$table->setCellData($i+1, $j, $ReportData[$row][$j-1]);
			}
			$i++;
		}

		$table->setHeaders($headers);
		$table->setTableWidth(count($headers));
		$table->setFooterVisible(false);

		//make other table
		$openOrdersTable = $this->openOrders($rowHeaders);
		
		$group->addTable($table);
		$group->addTable($openOrdersTable);
		$this->addTableGroup($group);
		$this->isBuild=true;
	}


}
?>

