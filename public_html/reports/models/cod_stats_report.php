<?php

global $xcart_dir;
include_once $xcart_dir.'/reports/models/models.php';
include_once $xcart_dir.'/reports/rendrers/rendrers.php';
include_once REP_DIR.'/models/date_calculation.php';
require_once $xcart_dir."/admin/codAdminHelper.php";

class CodStatsReportModel extends ReportModel {

	public function __construct(){
		parent::__construct();
		$this->name="COD Stats Report : " . date("j-M-Y");

	}
	protected function initFilters(){
		$this->addFilter("Date:" ,self::$FILTER_TYPE_DATE);
	}

	public function loadDefaultFilterValues(){
		$time=time();
		$this->setFilterValue("Date:", date("m/d/Y",$time));
	}
	
	public function returnInfo() {

		$table= new Table();
		$headers= array();
		$headers[0]= 'Return Rate Metrics';
		$table->setFooterVisible(false);
		$filtervalue = $this->getFilterValue("Date:");
		$date_split=split('/', $filtervalue);
		$current_month=(INT)$date_split[0];
		$current_year=(INT)$date_split[2];
		$current_date=(INT)$date_split[1];
		$returnData;
		//function to calculate the end and start days for various headers.
		$calculated_dates = calculateDates($current_date, $current_month, $current_year);
		for($cnt=0;$cnt<count($calculated_dates);$cnt++){
			$sql = "select count(distinct o.orderid) as orders, sum(o.total+o.shipping_cost+o.gift_charges) as revenue
					from xcart_orders o where o.cod_pay_status !='' AND  o.payment_method ='cod' and 
					o.completeddate >=unix_timestamp('" . $calculated_dates[$cnt]['start'] . "') and o.completeddate < unix_timestamp('" . $calculated_dates[$cnt]['end'] . "') 
					union
					select count(distinct o.orderid) as orders, sum(o.total+o.shipping_cost+o.gift_charges) as revenue
										from xcart_orders o where o.cod_pay_status ='unpaid' AND  o.payment_method ='cod' and 
										o.completeddate >=unix_timestamp('" . $calculated_dates[$cnt]['start'] . "') and o.completeddate < unix_timestamp('" . $calculated_dates[$cnt]['end'] . "') 
					union
					select count(distinct orderid), sum(total+shipping_cost+gift_charges)  from xcart_orders where cod_pay_status !='' and payment_method='cod' and 
					completeddate <= (select max(completeddate) from xcart_orders where cod_pay_status='unpaid' and payment_method='cod' and completeddate >=  unix_timestamp('" . $calculated_dates[$cnt]['start'] . "') and completeddate < unix_timestamp('" . $calculated_dates[$cnt]['end'] . "'))
					and completeddate >=  unix_timestamp('" . $calculated_dates[$cnt]['start'] . "')";
			$result=func_query($sql, true);
			$result1 = func_query("select count(distinct o.orderid) as orders from xcart_orders o where   
					o.completeddate >=unix_timestamp('" . $calculated_dates[$cnt]['start'] . "') and o.completeddate < unix_timestamp('" . $calculated_dates[$cnt]['end'] . "') ", true);
			$result2 = func_query("select sum(case true when (o.s_city) like '%bangalore%' then 1 else 0 end) as orders
					from xcart_orders o where  o.cod_pay_status !='' AND  o.payment_method ='cod' and
					o.completeddate >=unix_timestamp('" . $calculated_dates[$cnt]['start'] . "') and o.completeddate < unix_timestamp('" . $calculated_dates[$cnt]['end'] . "')", true);
			$result3 = func_query("select sum(case true when (o.s_city) like '%bangalore%' then 1 else 0 end) as orders
					from xcart_orders o where  o.payment_method !='cod' and
					o.completeddate >=unix_timestamp('" . $calculated_dates[$cnt]['start'] . "') and o.completeddate < unix_timestamp('" . $calculated_dates[$cnt]['end'] . "')", true);
			$returnData['ShippedCOD'][$cnt] = (float)$result[0]['orders'];
			$returnData['ReturnedOrders'][$cnt] = (float)$result[1]['orders'];
			$returnData['% Returned'][$cnt] = (float)$result[1]['orders'] * 100 /(float)$result[2]['orders'];
			$returnData['ShippedValue'][$cnt] = (float)$result[0]['revenue'];
			$returnData['ReturnedValue'][$cnt] = (float)$result[1]['revenue'];
			$returnData['% ReturnedValue'][$cnt] = (float)$result[1]['revenue'] * 100 / (float)$result[2]['revenue'];
			$returnData['ShippedOrders'][$cnt] = (float)$result1[0]['orders'];
			$returnData['COD_Bangalore'][$cnt] = (float)$result2[0]['orders'];
			$returnData['NonCOD_Bangalore'][$cnt] = (float)$result3[0]['orders'];
			
			$headers[$cnt+1] = $calculated_dates[$cnt]['label'];
		}		
		$width = $cnt+1;
		$table->setHeaders($headers);
		$table->setTableWidth($width);
		$rowIdentifiers = array('ShippedOrders', 'ShippedCOD', 'COD_Bangalore', 'NonCOD_Bangalore','ReturnedOrders', '% Returned', 'ShippedValue', 'ReturnedValue', '% ReturnedValue');
		$rowHeaderValues = array('No. of Orders Shipped', 'No. of COD Orders Shipped', 'No. of COD Orders Shipped in Bangalore', 'No. of Non-COD Orders Shipped in Bangalore','COD Orders Returned', '% Orders Returned', 'Value of COD Orders Shipped', 'Value of Returned Goods', '% COD Value Returned');
		$i=0;
		//set report data in cells of table

		foreach($rowIdentifiers as $rowid){
			$j=0;
			$table->setCellData($i, $j, $rowHeaderValues[$i]);
			for ($j=1; $j<$width; $j++){
				$table->setCellData($i, $j, $returnData[$rowid][$j-1]);
			}
			$i++;
		}

		//set formatting for columns of tables
		$table->setRowStyle(3, Table::$FORMAT, "currency");
		$table->setRowStyle(4, Table::$FORMAT, "currency");
		$table->setRowStyle(5, Table::$FORMAT, "currency");
		
		return $table;

	}
	
	public function currentReceivables(){

		$table= new Table();
		$headers = array("Days since Orders Shipped","Amount Receivable");
		$table->setHeaders($headers);

		$payStatus = "pending,paidtobluedart";
		$Dayslessthe15 = pending_payment_data(0,15, $payStatus, array("nothing"));
		$Daysbetween16and30= pending_payment_data(15,30, $payStatus, array("nothing"));
		$Daysbetween31and60= pending_payment_data(30,60, $payStatus, array("nothing"));
		$Daysbetween61and9999= pending_payment_data(60,9999, $payStatus, array("nothing"));
		$totalPendingPayment= pending_payment_data(0,9999, $payStatus, array("nothing"));

		$table->setCellData(0,0,"LessThen15Days");
		$table->setCellData(1,0,"16-30 Days");
		$table->setCellData(2,0,"31-60 Days");
		$table->setCellData(3,0,"more then 60 Days");
		$table->setCellData(4,0,"Total");

		$table->setCellData(0,1,(float)$Dayslessthe15[0]['total_amount']);
		$table->setCellData(1,1,(float)$Daysbetween16and30[0]['total_amount']);
		$table->setCellData(2,1,(float)$Daysbetween31and60[0]['total_amount']);
		$table->setCellData(3,1,(float)$Daysbetween61and9999[0]['total_amount']);
		$table->setCellData(4,1,(float)$totalPendingPayment[0]['total_amount']);

		//do formatting styles
		$table->setStyleMatrix(0, 1, 4, 1, Table::$FORMAT, "currency");
		$table->setStyleMatrix(0, 1, 4, 1, Table::$STYLE_ALIGN, "right");
		$table->setFooterVisible(false);
		return $table;

	}

	public function pendingPaymentInfo() {

		$table= new Table();
		$headers = array("COD Payment Status", "Amount Receivable");
		$table->setHeaders($headers);
		$table->setCellData(0,0,"Shipped but Not Delivered" );
		$table->setCellData(1,0,"Payment to be realized from Blue Dart" );
		$pending = get_order_number_value("pending", array("nothing"));
		$paidToBD = get_order_number_value("paidtobluedart", array("nothing"));
		$table->setCellData(0, 1, (float)$pending[0]['total_amount']);
		$table->setCellData(1, 1, (float)$paidToBD[0]['total_amount']);
		
		//do formatting styles
		$table->setStyleMatrix(0, 1, 1, 1, Table::$FORMAT, "currency");
		$table->setStyleMatrix(0, 1, 1, 1, Table::$STYLE_ALIGN, "right");
		
		$table->setFooterVisible(false);
		return $table;

	}
	public function buildReport(){
		global $weblog;
		$statstable= new Table();
		$group= new TableGroup("COD Stats Report");
		$headers= array();
		$headers[0]= 'Facts';
		$statstable->setFooterVisible(false);
		$filtervalue = $this->getFilterValue("Date:");
		$date_split=split('/', $filtervalue);
		$current_month=(INT)$date_split[0];
		$current_year=(INT)$date_split[2];
		$current_date=(INT)$date_split[1];
		$ReportData;
		//function to calculate the end and start days for various headers.
		$calculated_dates = calculateDates($current_date, $current_month, $current_year);
		for($cnt=0;$cnt<count($calculated_dates);$cnt++){
			$sql = "select count(distinct o.orderid) as orders, sum(o.total+o.shipping_cost+o.gift_charges) as revenue
					from xcart_orders o where o.cod_pay_status !='' AND  o.payment_method ='cod' and 
					o.date >=unix_timestamp('" . $calculated_dates[$cnt]['start'] . "') and o.date < unix_timestamp('" . $calculated_dates[$cnt]['end'] . "') 
					and o.status NOT IN ('PV','D','F')
					union
					select count(distinct o.orderid) as orders, sum(o.total+o.shipping_cost+o.gift_charges) as revenue
					from xcart_orders o where o.date >=unix_timestamp('" . $calculated_dates[$cnt]['start'] . "') and o.date < unix_timestamp('" . $calculated_dates[$cnt]['end'] . "') 
					and o.status in ('Q','WP','OH','C','SH','DL')";
			$result=func_query($sql, true);
			$ReportData['Orders'][$cnt] = (float)$result[0]['orders'];
			$ReportData['Revenue'][$cnt] = (float)$result[0]['revenue'];
			$ReportData['ASP'][$cnt]=(float)$result[0]['revenue']/(float)$result[0]['orders'];
			$ReportData['TotalOrders'][$cnt] = (float)$result[1]['orders'];
			$ReportData['TotalRevenue'][$cnt] = (float)$result[1]['revenue'];
			$ReportData['TotalASP'][$cnt] = (float)$result[1]['revenue']/(float)$result[1]['orders'];
			$ReportData['OrdersPercent'][$cnt] = $ReportData['Orders'][$cnt]*100/(float)$result[1]['orders'];
			$ReportData['RevenuePercent'][$cnt] = $ReportData['Revenue'][$cnt]*100/(float)$result[1]['revenue'];
			
			$headers[$cnt+1] = $calculated_dates[$cnt]['label'];
		}
		$width = $cnt+1;
		$statstable->setHeaders($headers);
		$statstable->setTableWidth($width);
		$rowIdentifiers = array('TotalOrders', 'Orders', 'OrdersPercent', 'TotalRevenue', 'Revenue', 'RevenuePercent', 'TotalASP', 'ASP');
		$rowHeaderValues = array('Total Orders', 'COD Orders', 'COD Orders as % of B2C', 'Total Revenue Booked', 'Revenue Booked from COD Orders', 'COD Revenue as % of B2C', 'Avg Order Size', 'Average COD Order Size');
		$i=0;
		//set report data in cells of table

		foreach($rowIdentifiers as $rowid){
			$j=0;
			$statstable->setCellData($i, $j, $rowHeaderValues[$i]);
			for ($j=1; $j<$width; $j++){
				$statstable->setCellData($i, $j, $ReportData[$rowid][$j-1]);
			}
			$i++;
		}

		//set formatting for columns of tables
		$statstable->setStyleMatrix(0, 1, $i-1, 8, TABLE::$STYLE_COLOR, "#EDF2F8");
		$statstable->setStyleMatrix(0, 9, $i-1, 13, TABLE::$STYLE_COLOR, "#CAD9EB");
		$statstable->setStyleMatrix(0, 14, $i-1, 17, TABLE::$STYLE_COLOR, "#EDF2F8");
		$statstable->setStyleMatrix(0, 18, $i-1, 21, TABLE::$STYLE_COLOR, "#CAD9EB");
		$statstable->setRowStyle(2, Table::$FORMAT, "currency");
		$statstable->setRowStyle(4, Table::$FORMAT, "currency");

		//add other tables
		$receivableTable = $this->currentReceivables();
		$pendingTable = $this->pendingPaymentInfo();
		$returnTable = $this->returnInfo();

		$group->addTable($statstable);
		//$group->addTable($returnTable);
		$group->addTable($receivableTable);
		$group->addTable($pendingTable);
		$this->addTableGroup($group);
		
		$this->notes = "WTD = Stats from 00:00 Monday to 23:59 Yesterday <br> 
						W3 = " . $calculated_dates[9]['start'] . " to " . $calculated_dates[9]['end'] ."<br> 
						W2 = " . $calculated_dates[10]['start'] . " to " . $calculated_dates[10]['end'] ."<br> 
						W1 = " .$calculated_dates[11]['start'] . " to " . $calculated_dates[11]['end'] . "<br> 
						W0 = " . $calculated_dates[12]['start'] . " to " . $calculated_dates[12]['end'] . "<br><br>";
		$this->isBuild=true;
	}


}
?>

