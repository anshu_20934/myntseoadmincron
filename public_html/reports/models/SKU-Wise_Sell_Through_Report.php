<?php

global $xcart_dir;
include_once $xcart_dir.'/reports/models/models.php';
include_once $xcart_dir.'/reports/rendrers/rendrers.php';
include_once REP_DIR.'/models/date_calculation.php';
ini_set("memory_limit",'6000M');
//trt
class SKUSellThroughReportModel extends ReportModel {

	public function __construct(){
		parent::__construct();
		$this->name="SKU-Wise Sell Through Report : " . date("j-M-Y");

	}
	protected function initFilters(){
		$this->addFilter("Date:" ,self::$FILTER_TYPE_DATE);
	}

	public function loadDefaultFilterValues(){
		$time=time();
		$this->setFilterValue("Date:", date("m/d/Y",$time));
	}



	public function buildReport(){

		global $weblog;
		$table= new Table();
		$group= new TableGroup("SKU-Wise Sell Through Report");
		$headers= array('SKU Code','Article Name','Size','Article Code','Master Category','Sub-Category','Article Type',
		'Brand','Age Group','Gender','Base Colour','Fashion Type','Season','Year','Usage','MRP',
		'Current Inventory Count', 'Effective Inventory Count','Sales - Yesterday','Sales - Last 7 days','Sales - Last 30 days','Revenue - Weekly', 'Revenue - Monthly'
		);
		$table->setHeaders($headers);
		$table->setFooterVisible(false);
		$filtervalue = $this->getFilterValue("Date:");
		$date_split=split('/', $filtervalue);
		$current_month=(INT)$date_split[0];
		$current_year=(INT)$date_split[2];
		$current_date=(INT)$date_split[1];

		$today= new FDate($current_date, $current_month, $current_year);
		$yest= $today->addDays(-1);

		$today= new FDate($current_date, $current_month, $current_year);
		$last7thDay= $today->addDays(-7);
		$today= new FDate($current_date, $current_month, $current_year);
		$last30thDay=$today->addDays(-30);
		$today= new FDate($current_date, $current_month, $current_year);
		
		// gives availablity of skus
		$sql = "select ps.name, ps.price, sp.style_id, sp.article_number,sp.global_attr_brand,
				sp.global_attr_age_group,sp.global_attr_gender,sp.global_attr_base_colour,
				sp.global_attr_fashion_type,sp.global_attr_season,
				sp.global_attr_year,sp.global_attr_usage
				,mas.typename masname,sub.typename subname,typ.typename articletype 
				from mk_style_properties sp left join  mk_catalogue_classification typ on  sp.global_attr_article_type=typ.id 
                left join mk_catalogue_classification mas on  mas.id = typ.parent2 left join mk_catalogue_classification sub 
				on sub.id = typ.parent1 join mk_product_style ps on ps.id=sp.style_id left join mk_attribute_type_values b_atr on b_atr.id=sp.global_attr_brand
			";

		$result = func_query($sql, true);
		foreach ($result as $row ){
			$styles[$row['style_id']]['name']=$row['name'];
			$styles[$row['style_id']]['price']=$row['price'];
			$styles[$row['style_id']]['brand']=$row['global_attr_brand'];
			$styles[$row['style_id']]['age_group']=$row['global_attr_age_group'];
			$styles[$row['style_id']]['gender']=$row['global_attr_gender'];
			$styles[$row['style_id']]['color']=$row['global_attr_base_colour'];
			$styles[$row['style_id']]['fashion']=$row['global_attr_fashion_type'];
			$styles[$row['style_id']]['season']=$row['global_attr_season'];
			$styles[$row['style_id']]['year']=$row['global_attr_year'];
			$styles[$row['style_id']]['usage']=$row['global_attr_usage'];
			$styles[$row['style_id']]['master']=$row['masname'];
			$styles[$row['style_id']]['subname']=$row['subname'];
			$styles[$row['style_id']]['articletype']=$row['articletype'];
			$styles[$row['style_id']]['id']=$row['style_id'];
			$styles[$row['style_id']]['article_number']=$row['article_number'];
		}

		$sql="select s.code,m.style_id,m.option_id,po.value
			from mk_skus s join mk_styles_options_skus_mapping m join mk_product_options po
					on s.id=m.sku_id and po.id=m.option_id group by s.id";
		$result = func_query($sql, true);
		$options;
		foreach ($result as $row ){
			$options[$row['style_id']."-".$row['option_id']]['sku']=$row['code'];
			$options[$row['style_id']."-".$row['option_id']]['size']=$row['value'];
			$options[$row['style_id']."-".$row['option_id']]['style']=$styles[$row['style_id']];
		}
		
			$sql = "select m.style_id, m.option_id, taba.invCount, taba.effectiveInv from
	                    (select sku_id, sum(inv_count) as invCount, sum(inv_count - blocked_order_count - blocked_manually_count - blocked_missed_count) as effectiveInv
	                    from core_inventory_counts cic where quality='Q1' group by sku_id) taba join mk_styles_options_skus_mapping m on m.sku_id=taba.sku_id ";

		$result = func_query($sql, true);
		foreach ($result as $row ){
			if(isset($options[$row['style_id']."-".$row['option_id']]))
			$options[$row['style_id']."-".$row['option_id']]['wh_count']=$row['invCount'];
			$options[$row['style_id']."-".$row['option_id']]['inv_count']=$row['effectiveInv'];
		}

		$sql="select od.product_style,oio.optionid,sum(oio.quantity ) as total from xcart_orders o join xcart_order_details od on od.orderid=o.orderid and
			o.date between unix_timestamp('".$yest->toUnixDateDayStart()."') and unix_timestamp('".$today->toUnixDateDayStart()."') and o.status in ('WP','Q','OH','SH','C','DL') join 
			mk_order_item_option_quantity oio on oio.itemid=od.itemid group by od.product_style,oio.optionid";
		$result = func_query($sql, true);
		foreach ($result as $row ){
			$options[$row['product_style']."-".$row['optionid']]['yest-sale']=$row['total'];
		}
		$sql="select od.product_style,oio.optionid,sum(oio.quantity ) as total, sum(((od.amount*od.price)/o.subtotal)*(o.total+o.shipping_cost+o.gift_charges)) as revenue from xcart_orders o join xcart_order_details od on od.orderid=o.orderid and
			o.date between unix_timestamp('".$last7thDay->toUnixDateDayStart()."') and unix_timestamp('".$today->toUnixDateDayStart()."') and o.status in ('WP','Q','OH','SH','C','DL') join 
			mk_order_item_option_quantity oio on oio.itemid=od.itemid group by od.product_style,oio.optionid";
		$result = func_query($sql, true);
		foreach ($result as $row ){
			$options[$row['product_style']."-".$row['optionid']]['7days-sale']=$row['total'];
			$options[$row['product_style']."-".$row['optionid']]['7days-revenue']=(float)$row['revenue'];
		}

		$sql="select od.product_style,oio.optionid,sum(oio.quantity ) as total, sum(((od.amount*od.price)/o.subtotal)*(o.total+o.shipping_cost+o.gift_charges)) as revenue from xcart_orders o join xcart_order_details od on od.orderid=o.orderid and
			o.date between unix_timestamp('".$last30thDay->toUnixDateDayStart()."') and unix_timestamp('".$today->toUnixDateDayStart()."') and o.status in ('WP','Q','OH','SH','C','DL') join 
			mk_order_item_option_quantity oio on oio.itemid=od.itemid group by od.product_style,oio.optionid";
		$result = func_query($sql, true);
		foreach ($result as $row ){
			$options[$row['product_style']."-".$row['optionid']]['30days-sale']=$row['total'];
			$options[$row['product_style']."-".$row['optionid']]['30days-revenue']=(float)$row['revenue'];
		}

		$i=0;
		foreach($options as $info){
			if(empty($info['sku']))
			continue;
			//'Size','SKU Code','Current Inventory Count','Sales - Yesterday','Sales - Last 7 days','Sales - Last 30 days','Cover Stock - Weekly Sales',
			//'Cover Stock - Monthly Sales','SSell-Through - Weekly Sales','Sell-Through - Monthly Sales'
			$j=0;
			$table->setCellData($i, $j++, $info['sku']);
			$table->setCellData($i, $j++, $info['style']['name']);
			$table->setCellData($i, $j++, $info['size']);
			$table->setCellData($i, $j++, $info['style']['article_number']);
			$table->setCellData($i, $j++, $info['style']['master']);
			$table->setCellData($i, $j++, $info['style']['subname']);
			$table->setCellData($i, $j++, $info['style']['articletype']);
			$table->setCellData($i, $j++, $info['style']['brand']);
			$table->setCellData($i, $j++, $info['style']['age_group']);
			$table->setCellData($i, $j++, $info['style']['gender']);
			$table->setCellData($i, $j++, $info['style']['color']);
			$table->setCellData($i, $j++, $info['style']['fashion']);
			$table->setCellData($i, $j++, $info['style']['season']);
			$table->setCellData($i, $j++, $info['style']['year']);
			$table->setCellData($i, $j++, $info['style']['usage']);
			$table->setCellData($i, $j++, (float)$info['style']['price']);
			$table->setCellData($i, $j++, $info['wh_count']);
			$table->setCellData($i, $j++, $info['inv_count']);
			$table->setCellData($i, $j++, $info['yest-sale']);
			$table->setCellData($i, $j++, $info['7days-sale']);
			$table->setCellData($i, $j++, $info['30days-sale']);
			$table->setCellData($i, $j++, $info['7days-revenue']);
			$table->setCellData($i, $j++, $info['30days-revenue']);
			$i++;
		}
		$table->setTableWidth(count($headers));
		$group->addTable($table);
		$this->addTableGroup($group);
		$this->isBuild=true;
		unset($options);
		unset($info);
		unset($result);
		unset($styles);

	}


}
?>

