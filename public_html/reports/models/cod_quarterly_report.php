<?php

global $xcart_dir;

//include_once('auth.php');
//define("REP_DIR",$xcart_dir."/reports");
include_once ($xcart_dir."/reports/report_model.ph99");
require_once $xcart_dir."/admin/codAdminHelper.php";
include_once $xcart_dir.'/reports/models/models.php';
include_once $xcart_dir.'/reports/rendrers/rendrers.php';
include_once REP_DIR.'/models/date_calculation.php';

class CodQuarterlyReportModel extends ReportModel {

	protected $mtdDaysCount;
	protected $qtdDaysCount;
	
	public function __construct(){
		parent::__construct();
		$this->name="COD Report";

	}
	protected function initFilters(){
		$this->addFilter("Date:" ,self::$FILTER_TYPE_DATE);
	}

	public function loadDefaultFilterValues(){
		$time=time();
		$this->setFilterValue("Date:", date("m/d/Y",$time));
		
	}
	
	public function getPercentage($partialValue, $totalValue) {
		
		return ((float)$partialValue/(float)$totalValue)*100;		
	}
	
	public function getQutrString($dateStr) {
		$dateArr = explode("-", $dateStr);
		$Qutr_NAME=array('00'=>'quarter-4','01'=>'quarter-1','02'=>'quarter-1','03'=>'quarter-1','04'=>'quarter-2','05'=>'quarter-2','06'=>'quarter-2','07'=>'quarter-3','08'=>'quarter-3','09'=>'quarter-3','10'=>'quarter-4','11'=>'quarter-4','12'=>'quarter-4');
		return $Qutr_NAME[$dateArr[1]];
	}
	public function globalReportHeader($baseString,$prev1QutrData, $prev2QutrData, $prev3QutrData ) {
		$header = array ($baseString, $this->getQutrString($prev1QutrData['orderStartDate']), $this->getQutrString($prev2QutrData['orderStartDate']),$this->getQutrString($prev3QutrData['orderStartDate']));
		return $header;
	}
	public function getTranscationInfoTable($prev1QutrData, $prev2QutrData, $prev3QutrData) {
		$table= new Table();
		$header = $this->globalReportHeader("# Transactions", $prev1QutrData, $prev2QutrData, $prev3QutrData);
		$table->setHeaders($header);
		$table->setCellData(0,0,"COD" );
		$table->setCellData(1,0,"NON COD" );
		$table->setCellData(2,0,"%COD" );
		$codTransData = cod_transcation_info($prev1QutrData);
		$table->setCellData(0,1,(float)$codTransData[0]['total_count']);
		$codTransData = cod_transcation_info($prev2QutrData);
		$table->setCellData(0,2,(float)$codTransData[0]['total_count']);
		$codTransData = cod_transcation_info($prev3QutrData);
		$table->setCellData(0,3,(float)$codTransData[0]['total_count']);
		
		$noncodTransData = non_cod_transcation_info($prev1QutrData);
		$table->setCellData(1,1,(float)$noncodTransData[0]['total_count']);
		$noncodTransData = non_cod_transcation_info($prev2QutrData);
		$table->setCellData(1,2,(float)$noncodTransData[0]['total_count']);
		$noncodTransData = non_cod_transcation_info($prev3QutrData);
		$table->setCellData(1,3,(float)$noncodTransData[0]['total_count']);
		
		$table->setCellData(2,1,  $this->getPercentage($table->getCellData(0, 1),$table->getCellData(0, 1)+ $table->getCellData(1, 1)));
		$table->setCellData(2,2,  $this->getPercentage($table->getCellData(0, 2),$table->getCellData(0, 2)+ $table->getCellData(1, 2)));
		$table->setCellData(2,3,  $this->getPercentage($table->getCellData(0, 3),$table->getCellData(0, 3)+ $table->getCellData(1, 3)));
		$table->setFooterVisible(false);
		return $table;
		
	}
	
	public function getValueInfoTable($prev1QutrData, $prev2QutrData, $prev3QutrData) {
		$table= new Table();
		$header =  $this->globalReportHeader(" Value ", $prev1QutrData, $prev2QutrData, $prev3QutrData);
		$table->setHeaders($header);
		$table->setCellData(0,0,"COD" );
		$table->setCellData(1,0,"NON COD" );
		$table->setCellData(2,0,"%COD" );
		$codTransData = cod_value_info($prev1QutrData);
		$table->setCellData(0,1,(float)$codTransData[0]['total_amount']);
		$codTransData = cod_value_info($prev2QutrData);
		$table->setCellData(0,2,(float)$codTransData[0]['total_amount']);
		$codTransData = cod_value_info($prev3QutrData);
		$table->setCellData(0,3,(float)$codTransData[0]['total_amount']);
		
		$noncodTransData = non_cod_value_info($prev1QutrData);
		$table->setCellData(1,1,(float)$noncodTransData[0]['total_amount']);
		$noncodTransData = non_cod_value_info($prev2QutrData);
		$table->setCellData(1,2,(float)$noncodTransData[0]['total_amount']);
		$noncodTransData = non_cod_value_info($prev3QutrData);
		$table->setCellData(1,3,(float)$noncodTransData[0]['total_amount']);
		
		$table->setCellData(2,1,  $this->getPercentage($table->getCellData(0, 1),$table->getCellData(0, 1)+ $table->getCellData(1, 1)));
		$table->setCellData(2,2,  $this->getPercentage($table->getCellData(0, 2),$table->getCellData(0, 2)+ $table->getCellData(1, 2)));
		$table->setCellData(2,3,  $this->getPercentage($table->getCellData(0, 3),$table->getCellData(0, 3)+ $table->getCellData(1, 3)));
		$table->setFooterVisible(false);
		return $table;
		
	}
	public function getAvgOrderSize($transcationTable, $valueTable) {
		$table= new Table();
		//$hearder = array("Avg Order Size(Rs)", "Yesterday",	"QutrlyDailyAvg","QtrlyDailyAvg");
		//$table->setHeaders($hearder);
		$header =  $transcationTable->getHeaders();
		$header[0] = "Avg Order Size";
		$table->setHeaders($header);
		$table->setCellData(0,0,"COD" );
		$table->setCellData(1,0,"NON COD" );
		$table->setCellData(0,1,(float)$valueTable->getCellData(0,1)/(float)$transcationTable->getCellData(0,1));
		$table->setCellData(0,2,(float)$valueTable->getCellData(0,2)/(float)$transcationTable->getCellData(0,2));
		$table->setCellData(0,3,(float)$valueTable->getCellData(0,3)/(float)$transcationTable->getCellData(0,3));
		$table->setCellData(1,1,(float)$valueTable->getCellData(1,1)/(float)$transcationTable->getCellData(1,1));
		$table->setCellData(1,2,(float)$valueTable->getCellData(1,2)/(float)$transcationTable->getCellData(1,2));
		$table->setCellData(1,3,(float)$valueTable->getCellData(1,3)/(float)$transcationTable->getCellData(1,3));								
		$table->setFooterVisible(false);
		return $table;
		
	}
	
	public function getItemPerOrder($prev1QutrData, $prev2QutrData, $prev3QutrData) {
		$table= new Table();
		$header =  $this->globalReportHeader("Items/Order (#)", $prev1QutrData, $prev2QutrData, $prev3QutrData);
		$table->setHeaders($header);
		$table->setCellData(0,0,"COD" );
		$table->setCellData(1,0,"NON COD" );
		$codTransData = cod_item_per_order($prev1QutrData);
		$table->setCellData(0,1,(float)$codTransData[0]['item_number']);
		$codTransData = cod_item_per_order($prev2QutrData);
		$table->setCellData(0,2,(float)$codTransData[0]['item_number']);
		$codTransData = cod_item_per_order($prev3QutrData);
		$table->setCellData(0,3,(float)$codTransData[0]['item_number']);
		
		$noncodTransData = non_cod_item_per_order($prev1QutrData);
		$table->setCellData(1,1,(float)$noncodTransData[0]['item_number']);
		$noncodTransData = non_cod_item_per_order($prev2QutrData);
		$table->setCellData(1,2,(float)$noncodTransData[0]['item_number']);
		$noncodTransData = non_cod_item_per_order($prev3QutrData);
		$table->setCellData(1,3,(float)$noncodTransData[0]['item_number']);

		//print_r($table->getAllRows());
		$table->setFooterVisible(false);
		return $table;
		
	}
	
	public function getPercentageRetrun($prev1QutrData, $prev2QutrData, $prev3QutrData) {
		$table= new Table();
		$header =  $this->globalReportHeader(" %Retrun Info ", $prev1QutrData, $prev2QutrData, $prev3QutrData);
		$table->setHeaders($header);
		$table->setCellData(0,0,"%Return" );
		$prev1QutrShipData['shippingStartDate'] =  $prev1QutrData['orderStartDate'];
		$prev1QutrShipData['shippingEndDate']   =  $prev1QutrData['orderEndDate'];
		$prev2QutrShipData['shippingStartDate'] =  $prev2QutrData['orderStartDate'];
		$prev2QutrShipData['shippingEndDate']   =  $prev2QutrData['orderEndDate'];
		$prev3QutrShipData['shippingStartDate'] =  $prev3QutrData['orderStartDate'];
		$prev3QutrShipData['shippingEndDate']   =  $prev3QutrData['orderEndDate'];
		$table->setCellData(0, 1,  get_percentage_return($prev1QutrShipData));
		$table->setCellData(0, 2,  get_percentage_return($prev2QutrShipData));
		$table->setCellData(0, 3,  get_percentage_return($prev3QutrShipData));		
		$table->setFooterVisible(false);
		return $table;
		
	} 
	public function getAvgPaymentCycleInfo($prev1QutrData, $prev2QutrData, $prev3QutrData) {
		$table= new Table();
		$header =  $this->globalReportHeader(" Avg Payment Cycle Info ", $prev1QutrData, $prev2QutrData, $prev3QutrData);
		$table->setHeaders($header);
		$table->setCellData(0,0,"Avg Payment Cycle" );
		$prev1QutrShipData['paymentStartDate'] =  $prev1QutrData['orderStartDate'];
		$prev1QutrShipData['paymentEndDate']   =  $prev1QutrData['orderEndDate'];
		$prev2QutrShipData['paymentStartDate'] =  $prev2QutrData['orderStartDate'];
		$prev2QutrShipData['paymentEndDate']   =  $prev2QutrData['orderEndDate'];
		$prev3QutrShipData['paymentStartDate'] =  $prev3QutrData['orderStartDate'];
		$prev3QutrShipData['paymentEndDate']   =  $prev3QutrData['orderEndDate'];
		$table->setCellData(0, 1,  (float)get_avg_payment_cycle($prev1QutrShipData));
		$table->setCellData(0, 2,  (float)get_avg_payment_cycle($prev2QutrShipData));
		$table->setCellData(0, 3,  (float)get_avg_payment_cycle($prev3QutrShipData));		
		$table->setFooterVisible(false);
		return $table;
		
	} 
	public function buildReport(){
		global $weblog;
		$table= new Table();
		$group= new TableGroup("COD  Quarterly Report");
		
		$table->setFooterVisible(false);
		$filtervalue = $this->getFilterValue("Date:");
		$date_split=split('/', $filtervalue);
		$current_month=(INT)$date_split[0];
		$current_year=(INT)$date_split[2];
		$current_date=(INT)$date_split[1];
		$curr_fyear = new FYear($current_date, $current_month, $current_year);
		$curr_fdate= new FDate($current_date, $current_month, $current_year);
		$prev1QutrData=array();
		$prev2QutrData= array();
		$prev3QutrData = array();
		$prev1QutrStartDate = $curr_fyear->getQuarterStartFDate($curr_fdate);		
		$prev1QutrEndDate   = $curr_fyear->getQuarterEndFDate($prev1QutrStartDate);
		$prev1QutrData['orderStartDate'] = $prev1QutrStartDate->getTimeDateString();
		$prev1QutrData['orderEndDate']   = $prev1QutrEndDate->getTimeDateString();
		$weblog->info("COD Qutrly Report ::prevQutrStartDate".$prev1QutrStartDate->getTimeDateString());
		$weblog->info("COD Qutrly Report ::prevQutrEndDate".$prev1QutrEndDate->getTimeDateString());
		
		$prev2QutrStartDate = $curr_fyear->getPrevQuarterStartFdate($prev1QutrStartDate);
		$prev2QutrEndDate   = $curr_fyear->getQuarterEndFDate($prev2QutrStartDate);
		$prev2QutrData['orderStartDate'] = $prev2QutrStartDate->getTimeDateString();
		$prev2QutrData['orderEndDate']   = $prev2QutrEndDate->getTimeDateString();
		
		$weblog->info("COD Qutrly Report ::prev2QutrStartDate ".$prev2QutrStartDate->getTimeDateString());
		$weblog->info("COD Qutrly Report ::prev2QutrEndDate".$prev2QutrEndDate->getTimeDateString());
		
		$prev3QutrStartDate = $curr_fyear->getPrevQuarterStartFdate($prev2QutrStartDate);
		$prev3QutrEndDate   = $curr_fyear->getQuarterEndFDate($prev3QutrStartDate);
		$prev3QutrData['orderStartDate'] = $prev3QutrStartDate->getTimeDateString();
		$prev3QutrData['orderEndDate']   = $prev3QutrEndDate->getTimeDateString();
						
		$weblog->info("COD Qutrly Report ::prev3QutrStartDate".$prev3QutrStartDate->getTimeDateString());
		$weblog->info("COD Qutrly Report ::prev3QutrEndDate".$prev3QutrEndDate->getTimeDateString());
		
		
		$transcationInfoTable = $this->getTranscationInfoTable($prev1QutrData, $prev2QutrData, $prev3QutrData);
		$valueInfoTable = $this->getValueInfoTable($prev1QutrData, $prev2QutrData, $prev3QutrData);
		$itemInfoTable = $this->getItemPerOrder($prev1QutrData, $prev2QutrData, $prev3QutrData);
		$avgValueTable  = $this->getAvgOrderSize($transcationInfoTable, $valueInfoTable);
		$percentReturn  = $this->getPercentageRetrun($prev1QutrData, $prev2QutrData, $prev3QutrData);
		$avgPaymentCycle = $this->getAvgPaymentCycleInfo($prev1QutrData, $prev2QutrData, $prev3QutrData);
		/*I am passing here a garbage value as input to function as passing a empty array 
		  that online report should not break because of it */
		$group->addTable($transcationInfoTable);
		$group->addTable($valueInfoTable);
		$group->addTable($avgValueTable);
		$group->addTable($itemInfoTable);		
		$group->addTable($percentReturn);
		$group->addTable($avgPaymentCycle);	
		$this->addTableGroup($group);
		$this->isBuild=true;
	}
	
	
}

//$codDate = new CodDailyReportModel();

//$codDate->buildReport();
?>

