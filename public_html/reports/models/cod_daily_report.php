<?php

global $xcart_dir;

//include_once('auth.php');
//define("REP_DIR",$xcart_dir."/reports");
include_once ($xcart_dir."/reports/report_model.ph99");
require_once $xcart_dir."/admin/codAdminHelper.php";
include_once $xcart_dir.'/reports/models/models.php';
include_once $xcart_dir.'/reports/rendrers/rendrers.php';
include_once REP_DIR.'/models/date_calculation.php';

class CodDailyReportModel extends ReportModel {

	protected $mtdDaysCount;
	protected $qtdDaysCount;
	protected $transData;
	protected $valueData;
	
	public function __construct(){
		parent::__construct();
		$this->name="COD Report";

	}
	protected function initFilters(){
		$this->addFilter("Date:" ,self::$FILTER_TYPE_DATE);
	}

	public function loadDefaultFilterValues(){
		$time=time();
		$this->setFilterValue("Date:", date("m/d/Y",$time));
	}
	
	public function getPercentage($partialValue, $totalValue) {
		
		return ((float)$partialValue/(float)$totalValue)*100;		
	}
	public function getTranscationInfoTable($dailyPostData, $monthlyPostData, $quaterlyPostData) {
		$table= new Table();
		$hearder = array("# Transactions", "Yesterday",	"MonthlyDailyAvg","QtrlyDailyAvg");
		$table->setHeaders($hearder);
		$table->setCellData(0,0,"COD" );
		$table->setCellData(1,0,"NON COD" );
		$table->setCellData(2,0,"%COD" );
		$codTransData = cod_transcation_info($dailyPostData);
		$this->transData[0] = (float)$codTransData[0]['total_count'];
		$table->setCellData(0,1,$this->transData[0]);
		$codTransData = cod_transcation_info($monthlyPostData);
		$this->transData[1] = (float)$codTransData[0]['total_count']/ (float)$this->mtdDaysCount ;
		$table->setCellData(0,2,$this->transData[1]);
		$codTransData = cod_transcation_info($quaterlyPostData);
		$this->transData[2] = (float)$codTransData[0]['total_count']/ (float)$this->qtdDaysCount ;
		$table->setCellData(0,3,$this->transData[2]);
		
		$noncodTransData = non_cod_transcation_info($dailyPostData);
		$this->transData[3] = (float)$noncodTransData[0]['total_count'];
		$table->setCellData(1,1,$this->transData[3]);
		$noncodTransData = non_cod_transcation_info($monthlyPostData);
		$this->transData[4] = (float)$noncodTransData[0]['total_count']/ (float)$this->mtdDaysCount ;
		$table->setCellData(1,2,$this->transData[4]);
		$noncodTransData = non_cod_transcation_info($quaterlyPostData);
		$this->transData[5] = (float)$noncodTransData[0]['total_count']/ (float)$this->qtdDaysCount ;
		$table->setCellData(1,3,$this->transData[5]);
		
		$table->setCellData(2,1,  $this->getPercentage($table->getCellData(0, 1),$table->getCellData(0, 1)+ $table->getCellData(1, 1)));
		$table->setCellData(2,2,  $this->getPercentage($table->getCellData(0, 2),$table->getCellData(0, 2)+ $table->getCellData(1, 2)));
		$table->setCellData(2,3,  $this->getPercentage($table->getCellData(0, 3),$table->getCellData(0, 3)+ $table->getCellData(1, 3)));
		$table->setFooterVisible(false);
		return $table;
		
	}
	
	public function getValueInfoTable($dailyPostData, $monthlyPostData, $quaterlyPostData) {
		$table= new Table();
		$hearder = array("# Total Value", "Yesterday",	"MonthlyDailyAvg","QtrlyDailyAvg");
		$table->setHeaders($hearder);
		$table->setCellData(0,0,"COD" );
		$table->setCellData(1,0,"NON COD" );
		$table->setCellData(2,0,"%COD" );
		$codTransData = cod_value_info($dailyPostData);
		$this->valueData[0] = (float)$codTransData[0]['total_amount'];
		$table->setCellData(0,1,money_format('%!i',$this->valueData[0]));
		$codTransData = cod_value_info($monthlyPostData);
		$this->valueData[1] = (float)$codTransData[0]['total_amount']/ (float)$this->mtdDaysCount ;
		$table->setCellData(0,2,money_format('%!i',$this->valueData[1]));
		$codTransData = cod_value_info($quaterlyPostData);
		$this->valueData[2] = (float)$codTransData[0]['total_amount']/ (float)$this->qtdDaysCount ;
		$table->setCellData(0,3,money_format('%!i',$this->valueData[2]));
		
		$noncodTransData = non_cod_value_info($dailyPostData);
		$this->valueData[3] = (float)$noncodTransData[0]['total_amount'];
		$table->setCellData(1,1,money_format('%!i',$this->valueData[3]));
		$noncodTransData = non_cod_value_info($monthlyPostData);
		$this->valueData[4] = (float)$noncodTransData[0]['total_amount']/ (float)$this->mtdDaysCount ;
		$table->setCellData(1,2,money_format('%!i',$this->valueData[4]));
		$noncodTransData = non_cod_value_info($quaterlyPostData);
		$this->valueData[5] = (float)$noncodTransData[0]['total_amount']/ (float)$this->qtdDaysCount ;
		$table->setCellData(1,3,money_format('%!i',$this->valueData[5]));
		
		$table->setCellData(2,1,  $this->getPercentage($this->valueData[0],$this->valueData[0] + $this->valueData[3]));
		$table->setCellData(2,2,  $this->getPercentage($this->valueData[1],$this->valueData[1]+ $this->valueData[4]));
		$table->setCellData(2,3,  $this->getPercentage($this->valueData[2],$this->valueData[2]+ $this->valueData[5]));
		$table->setFooterVisible(false);
		return $table;
		
	}
	
	public function currentReceivables($dailyPostData){
		
		$table= new Table();
		$hearder = array("Current Receivables","");
		$table->setHeaders($hearder);
		$payStatus = "pending,paidtobluedart";
		$Dayslessthe15 = pending_payment_data(0,15, $payStatus,$dailyPostData);
		$table->setCellData(0,0,"LessThen15Days");
		$table->setCellData(0,1,money_format('%!i',(float)$Dayslessthe15[0]['total_amount']));
		$Dayslessthe15[0]["cod_pay_status"]="LessThen15Days";
		$Daysbetween16and30= pending_payment_data(15,30, $payStatus, $dailyPostData);
		$table->setCellData(1,0,"16-30 Days");
		$table->setCellData(1,1,money_format('%!i',(float)$Daysbetween16and30[0]['total_amount']));
			
		$Daysbetween31and60= pending_payment_data(30,60, $payStatus, $dailyPostData);
		$table->setCellData(2,0,"31-60 Days");
		$table->setCellData(2,1,money_format('%!i',(float)$Daysbetween31and60[0]['total_amount']));

		$Daysbetween61and9999= pending_payment_data(60,9999, $payStatus,$dailyPostData);
		$table->setCellData(3,0,"more then 60 Days");
		$table->setCellData(3,1,money_format('%!i',(float)$Daysbetween61and9999[0]['total_amount']));
		
		$totalPendingPayment= pending_payment_data(0,9999, $payStatus, $dailyPostData);
		//print_r($totalPendingPayment);
		$table->setCellData(4,0,"Total");
		$table->setCellData(4,1,money_format('%!i',(float)$totalPendingPayment[0]['total_amount']));
		$table->setFooterVisible(false);
		return $table;
		
	}
	
	public function getAvgOrderSize($transcationTable, $valueTable) {
		$table= new Table();
		$hearder = array("Avg Order Size(Rs)", "Yesterday",	"MonthlyDailyAvg","QtrlyDailyAvg");
		$table->setHeaders($hearder);
		$table->setCellData(0,0,"COD" );
		$table->setCellData(1,0,"NON COD" );
		$table->setCellData(0,1,(float)$this->valueData[0]/(float)$this->transData[0]);
		$table->setCellData(0,2,(float)$this->valueData[1]/(float)$this->transData[1]);
		$table->setCellData(0,3,(float)$this->valueData[2]/(float)$this->transData[2]);
		$table->setCellData(1,1,(float)$this->valueData[3]/(float)$this->transData[3]);
		$table->setCellData(1,2,(float)$this->valueData[4]/(float)$this->transData[4]);
		$table->setCellData(1,3,(float)$this->valueData[5]/(float)$this->transData[5]);								
		$table->setFooterVisible(false);
		return $table;
		
	}
	
	public function getItemPerOrder($dailyPostData, $monthlyPostData, $quaterlyPostData) {
		$table= new Table();
		$hearder = array("Items/Order (#)", "Yesterday","MonthlyDailyAvg","QtrlyDailyAvg");
		$table->setHeaders($hearder);
		$table->setCellData(0,0,"COD" );
		$table->setCellData(1,0,"NON COD" );
		$codTransData = cod_item_per_order($dailyPostData);
		$table->setCellData(0,1,(float)$codTransData[0]['item_number']);
		$codTransData = cod_item_per_order($monthlyPostData);
		$table->setCellData(0,2,(float)$codTransData[0]['item_number']);
		$codTransData = cod_item_per_order($quaterlyPostData);
		$table->setCellData(0,3,(float)$codTransData[0]['item_number']);
		
		$noncodTransData = non_cod_item_per_order($dailyPostData);
		$table->setCellData(1,1,(float)$noncodTransData[0]['item_number']);
		$noncodTransData = non_cod_item_per_order($monthlyPostData);
		$table->setCellData(1,2,(float)$noncodTransData[0]['item_number']);
		$noncodTransData = non_cod_item_per_order($quaterlyPostData);
		$table->setCellData(1,3,(float)$noncodTransData[0]['item_number']);

		//print_r($table->getAllRows());
		$table->setFooterVisible(false);
		return $table;
		
	}
	public function pendingPaymentInfo() {
		
		$table= new Table();
		$hearder = array("Pending Payments", "");
		$table->setHeaders($hearder);
		$table->setCellData(0,0,"Pending Delivery" );
		$pending = get_order_number_value("pending", array("nothing"));	
		$paidToBD = get_order_number_value("paidtobluedart", array("nothing"));
		$table->setCellData(0, 1, money_format('%!i',$pending[0]['total_amount']));
		$table->setCellData(1,0,"PaidtoBD" );
		$table->setCellData(1, 1, money_format('%!i',$paidToBD[0]['total_amount']));
		$table->setFooterVisible(false);
		return $table;

	}
	public function getPercentageRetrun() {
		$table= new Table();
		$hearder = array("%Retrun Info ","");
		$table->setHeaders($hearder);
		$table->setCellData(0,0,"%Return" );
		$table->setCellData(0, 1,  get_percentage_return());
		$table->setFooterVisible(false);
		return $table;
		
	} 
	public function buildReport(){
		global $weblog;
		$table= new Table();
		$group= new TableGroup("COD  Report");
		
		$table->setFooterVisible(false);
		$filtervalue = $this->getFilterValue("Date:");
		$date_split=split('/', $filtervalue);
		$current_month=(INT)$date_split[0];
		$current_year=(INT)$date_split[2];
		$current_date=(INT)$date_split[1];
		$curr_fyear = new FYear($current_date, $current_month, $current_year);
		$curr_fdate= new FDate($current_date, $current_month, $current_year);
		$dailyPostData=array();
		$currMonthData= array();
		$currQrtrData = array();
		
		//Getting start Day Date and end date  
		$startDate = $curr_fdate->getTime();
		$dailyPostData['orderStartDate'] = $curr_fdate->addDays(-1)->getTimeDateString();
		$dailyPostData['orderEndDate']   = $curr_fdate->addDays(0)->getTimeDateString();
		$currMonthData['orderStartDate'] = $curr_fdate->getMonthStartFDate()->getTimeDateString();
		$currMonthData['orderEndDate']   = $curr_fdate->getTimeDateString();
		$currQrtrData['orderStartDate'] = $curr_fyear->getQuarterStartFDate($curr_fdate)->getTimeDateString();
		$currQrtrData['orderEndDate']   = $curr_fdate->getTimeDateString();
		
		//$weblog->info(var_dump($dailyPostData));
		//$weblog->info(var_dump(currMonthData));
		//$weblog->info(var_dump($currQrtrData));
		
		$this->mtdDaysCount = $curr_fyear->getFDateDiffInDays($curr_fdate->getMonthStartFDate(),$curr_fdate);
		$this->qtdDaysCount = $curr_fyear->getFDateDiffInDays($curr_fyear->getQuarterStartFDate($curr_fdate),$curr_fdate);
		//echo $this->mtdDaysCount;
		//echo $this->qtdDaysCount;
		
		//$weblog->info("cod report mailer::mtdDaysCount".$this->mtdDaysCount);
		//$weblog->info("cod report mailer::qtdDaysCount".$this->qtdDaysCount);
		$transcationInfoTable = $this->getTranscationInfoTable($dailyPostData, $currMonthData, $currQrtrData);
		$valueInfoTable = $this->getValueInfoTable($dailyPostData, $currMonthData, $currQrtrData);
		//$itemInfoTable = $this->getItemPerOrder($dailyPostData, $currMonthData, $currQrtrData);
		//$avgValueTable  = $this->getAvgOrderSize($transcationInfoTable, $valueInfoTable);
		$pendingPayment = $this->pendingPaymentInfo();
		//$percentReturn  = $this->getPercentageRetrun(array("nothing"));
		/*I am passing here a garbage value as input to function as passing a empty array 
		  that online report should not break because of it */
		$currentPending = $this->currentReceivables(array("nothin"));
		$group->addTable($transcationInfoTable);
		$group->addTable($valueInfoTable);
		//$group->addTable($avgValueTable);
		//$group->addTable($itemInfoTable);
		$group->addTable($currentPending);
		$group->addTable($pendingPayment);
		//$group->addTable($percentReturn);
		
		$this->addTableGroup($group);
		$this->isBuild=true;
	}
	
	
}

//$codDate = new CodDailyReportModel();

//$codDate->buildReport();
?>

