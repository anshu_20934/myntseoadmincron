<?php

global $xcart_dir;
include_once $xcart_dir.'/reports/models/models.php';
include_once $xcart_dir.'/reports/rendrers/rendrers.php';
include_once REP_DIR.'/models/date_calculation.php';

class CatalogStatusReportModel extends ReportModel {

	public function __construct(){
		parent::__construct();
		$this->name="Catalog Status Report : " . date("j-M-Y");
	}
	protected function initFilters(){
		$this->addFilter("Date:" ,self::$FILTER_TYPE_DATE);
	}

	public function loadDefaultFilterValues(){
		$time=time();
		$this->setFilterValue("Date:", date("m/d/Y",$time));
	}

	public function buildReport(){
		global $weblog;
		$group= new TableGroup("Catalog Status Report");
		$table= new Table();
		$table->setFooterVisible(false);
		$filtervalue = $this->getFilterValue("Date:");
		$date_split=split('/', $filtervalue);
		$current_month=(INT)$date_split[0];
		$current_year=(INT)$date_split[2];
		$current_date=(INT)$date_split[1];
		$from_date = $current_year . "-" . $current_month . "-". $current_date;
		$end_date = $current_year . "-" . $current_month . "-". ($current_date+1);
		$headers = array('Date','Goods received:-# of Styles','Goods received:-# of Pieces','Inward:# of styles','Inward:# of pieces','Cataloged:#of styles','Cataloged:#of pieces','Pending inwards:#of styles','Pending inwards:#of pieces','Inwarded but not cataloged:# of styles','Inwarded but not cataloged:# of pieces','Live on Portal:# of styles','Live on Portal:# of pieces');
		$table->setHeaders($headers);
		$table->setTableWidth(count($headers));
		$wms_con = mysql_connect('10.130.27.161','MyntraRep0rtUsEr','9eguawKETuBRVEku');
		if(!$wms_con) {
			echo "ERROR MySQL: Connect to WMS Server\n";
			exit;
		}

		$table->setCellData(0, 0, $from_date);
		for($j=1; $j<3; $j++){
			$table->setCellData(0, $j, '-');
		}
		for($j=7; $j<9; $j++){
			$table->setCellData(0, $j, '-');
		}
		
		$skuid_wms = mysql_query("select distinct parent_sku_id from myntra_wms.core_grn_skus where created_on between ('" . $from_date ."') and ('". $end_date ."')", $wms_con);
		$i = 0;
		while($row = mysql_fetch_assoc($skuid_wms)){
			$skuids[$i++] = $row['parent_sku_id'];
		}
		$skuids_csv = implode(",", $skuids);
		$sql="select count(distinct vendor_article_no) as stylesInwarded from mk_skus where id in (" . $skuids_csv .")";
		$result = func_query($sql, true);
		$table->setCellData(0, 3, $result[0]['stylesInwarded']);

		$sql="select sum(accepted_qty) as piecesInwarded from core_grn_skus where created_on between ('" . $from_date ."') and ('". $end_date ."')";
		$result = mysql_fetch_assoc(mysql_query($sql, $wms_con));
		$table->setCellData(0, 4, $result[0]['piecesInwarded']);

		$sql="select count(style_id) as cataloguedStyles from mk_style_properties where add_date >= unix_timestamp('" . $from_date ."') and add_date < unix_timestamp('". $end_date ."')";
		$result = func_query($sql, true);
		$table->setCellData(0, 5, $result[0]['cataloguedStyles']);

		$sql="select sum(cic.inv_count) as cataloguedPieces from mk_styles_options_skus_mapping osm, core_inventory_counts cic, mk_product_options po where cic.sku_id=osm.sku_id and po.id=osm.option_id and po.is_active = 1 and osm.style_id in (select (style_id) as cataloguedStyles from mk_style_properties where add_date >= unix_timestamp('" . $from_date ."') and add_date < unix_timestamp('" . $end_date ."'))";
		$result = func_query($sql, true);
		$table->setCellData(0, 6, $result[0]['cataloguedPieces']);

		$sql="select count(distinct mv.vendor_article_no) as InButNotCatagedStyles from (select s.id, s.vendor_article_no from mk_skus s left join mk_styles_options_skus_mapping osm on s.id=osm.sku_id where osm.style_id is null) as mv join core_inventory_counts cic on cic.sku_id=mv.id where cic.inv_count > 0";
		$result = func_query($sql, true);
		$table->setCellData(0, 9, $result[0]['InButNotCatagedStyles']);

		$sql="select sum(cic.inv_count) as InButNotCatagedPieces from core_inventory_counts cic left join mk_styles_options_skus_mapping osm on cic.sku_id=osm.sku_id
				where osm.style_id is null and cic.quality='Q1'";
		$result = func_query($sql, true);
		$table->setCellData(0, 10, $result[0]['InButNotCatagedPieces']);

		$sql="select count(mv.style_id) as LiveStyles from (select sp.style_id , max(po.is_active) as isActive from mk_style_properties sp, mk_product_options po,  mk_product_style ps where sp.style_id=po.style and ps.id=sp.style_id and ps.styletype='P' group by sp.style_id ) as mv where mv.isActive=1";
		$result = func_query($sql, true);
		$table->setCellData(0, 11, $result[0]['LiveStyles']);

		$sql=" select  sum(cic.inv_count) as LivePieces from core_inventory_counts cic, mk_styles_options_skus_mapping osm ,
			mk_product_style ps,  mk_product_options po where cic.sku_id=osm.sku_id 
			and cic.quality='Q1' and osm.style_id =ps.id and ps.is_active=1 and ps.styletype in ('P') 
			and osm.option_id = po.id and po.is_active=1";
		$result = func_query($sql, true);
		$table->setCellData(0, 12, $result[0]['LivePieces']);

		$group->addTable($table);
		$this->addTableGroup($group);
		$this->isBuild=true;
	}


}
?>

