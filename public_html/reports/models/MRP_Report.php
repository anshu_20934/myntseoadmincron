<?php

global $xcart_dir;
include_once $xcart_dir.'/reports/models/models.php';
include_once $xcart_dir.'/reports/rendrers/rendrers.php';
include_once REP_DIR.'/models/date_calculation.php';

class MRPReportModel extends ReportModel {

	public function __construct(){
		parent::__construct();
		$this->name="MRP Report : " . date("j-M-Y");
	}
	protected function initFilters(){
		$this->addFilter("Date:" ,self::$FILTER_TYPE_DATE);
	}

	public function loadDefaultFilterValues(){
		$time=time();
		$this->setFilterValue("Date:", date("m/d/Y",$time));
	}

	public function referrerStatusInfo($calculated_dates){
		$table= new Table('Referrer Status');
		$table->setFooterVisible(false);
		$j=0;
		$headers[0] = 'Facts';
		$table->setCellData(0, $j, "Total no of referral registrations");
		$table->setCellData(1, $j, "Total no of coupons issued (1x Rs 250) to referrers due to referral's registration");
		$table->setCellData(2, $j, "Total no of coupons consumed (orders) by referrers using coupons issued due to referral's registration");
		$table->setCellData(3, $j, "Total revenue from referrers using coupons issued due to referral's registration");
		$table->setCellData(4, $j, "Average order size of referrers using coupons issued due to referral's registration");
		$table->setCellData(5, $j, "Total no of referrals making 1st purchase");
		$table->setCellData(6, $j, "Total no of coupons issued(2x Rs 500) to referrers due to referral's first purchase");
		$table->setCellData(7, $j, "Total no of coupons consumed (orders) by referrers using coupons issued due to referral's first purchase");
		$table->setCellData(8, $j, "Total revenue from referrers using coupons dissued due to referral's first purchase");
		$table->setCellData(9, $j, "Average order size of referrers using coupons issued due to referral's first purchase");

		for($cnt=0;$cnt<count($calculated_dates);$cnt++){
			$j++;
			$result_referralReg = func_query("select count(referer) as num from mk_referral_log where dateJoined >= unix_timestamp('" . $calculated_dates[$cnt]['start'] . "') and dateJoined < unix_timestamp('" . $calculated_dates[$cnt]['end'] . "')", true);
			$result_firstPur = func_query("select count(referer) as num from mk_referral_log where dateFirstPurchase >= unix_timestamp('" . $calculated_dates[$cnt]['start'] . "') and dateFirstPurchase < unix_timestamp('" . $calculated_dates[$cnt]['end'] . "')", true);

			$result_regRef = func_query("select count(*) as issued from xcart_discount_coupons where groupname = 'MrpReferrals' and coupon like '%Reg%' and timeCreated >= unix_timestamp('" . $calculated_dates[$cnt]['start'] . "') and timeCreated < unix_timestamp('" . $calculated_dates[$cnt]['end'] . "')", true);
			$result_regRefRev = func_query("select count(distinct o.orderid) as orders, sum(o.total +o.shipping_cost+o.gift_charges) as revenue from xcart_discount_coupons dc, xcart_orders o
											where dc.coupon = o.coupon and dc.groupname = 'MrpReferrals' and dc.coupon like '%Reg%' and o.date >= unix_timestamp('" . $calculated_dates[$cnt]['start'] . "') and o.date < unix_timestamp('" . $calculated_dates[$cnt]['end'] . "') and o.status in ('Q','WP','OH','C','SH','DL')", true);
			$result_purchaseRef = func_query("select count(*) as issued from xcart_discount_coupons where groupname = 'MrpReferrals' and coupon like '%REF%' and timeCreated >= unix_timestamp('" . $calculated_dates[$cnt]['start'] . "') and timeCreated < unix_timestamp('" . $calculated_dates[$cnt]['end'] . "')",true);
			$result_purchaseRefRev = func_query("select count(distinct o.coupon) as coupons , sum(o.total +o.shipping_cost+o.gift_charges) as revenue from xcart_discount_coupons dc, xcart_orders o
											where dc.coupon = o.coupon and dc.groupname = 'MrpReferrals' and dc.coupon like '%REF%' and o.date >= unix_timestamp('" . $calculated_dates[$cnt]['start'] . "') and o.date < unix_timestamp('" . $calculated_dates[$cnt]['end'] . "') and o.status in ('Q','WP','OH','C','SH','DL')", true);
				
			
			$table->setCellData(0, $j, $result_referralReg[0]['num']);
			$table->setCellData(1, $j, $result_regRef[0]['issued']);
			$table->setCellData(2, $j, $result_regRefRev[0]['orders']);
			$table->setCellData(3, $j, (float)$result_regRefRev[0]['revenue']);
			$table->setCellData(4, $j, (float)$result_regRefRev[0]['revenue']/(float)$result_regRefRev[0]['orders']);
			$table->setCellData(5, $j, $result_firstPur[0]['num']);
			$table->setCellData(6, $j, $result_purchaseRef[0]['issued']);
			$table->setCellData(7, $j, $result_purchaseRefRev[0]['coupons']);
			$table->setCellData(8, $j, (float)$result_purchaseRefRev[0]['revenue']);
			$table->setCellData(9, $j, (float)$result_purchaseRefRev[0]['revenue']/(float)$result_purchaseRefRev[0]['coupons']);
			$headers[$cnt+1] = $calculated_dates[$cnt]['label']	;
		}
		$table->setHeaders($headers);
		return $table;
	}

	public function referralInfo($calculated_dates){
		$table= new Table('Referral User Statistics');
		$table->setFooterVisible(false);
		$j=0;
		$headers[0] = 'Facts';
		$table->setCellData(0, $j, "Total no of referrals" );
		$table->setCellData(1, $j, "Total no of referral registrations");
		$table->setCellData(2, $j, "Total no of non-referral registrations");
		$table->setCellData(3, $j, "No. of logged in users");
		$table->setCellData(4, $j, "No. of logged in users with verified mobile nos.");
		$table->setCellData(5, $j, "No. of logged in users who verified mobile within time period");

		for($cnt=0;$cnt<count($calculated_dates);$cnt++){
			$j++;
			// Total distinct referrals through any channel (email + others)
			$result_referral = func_query("select count(distinct referred) as num from mk_referral_log where dateReferred >= unix_timestamp('" . $calculated_dates[$cnt]['start'] . "') and dateReferred < unix_timestamp('" . $calculated_dates[$cnt]['end'] . "') or dateReferred = 0", true);
			$result_referralRegs = func_query("select count(*) as num from mk_referral_log where dateJoined >= unix_timestamp('" . $calculated_dates[$cnt]['start'] . "') and dateJoined < unix_timestamp('" . $calculated_dates[$cnt]['end'] . "')", true);
			$result_totalRegs = func_query("select count(*) as num from xcart_customers where first_login >= unix_timestamp('" . $calculated_dates[$cnt]['start'] . "') and first_login < unix_timestamp('" . $calculated_dates[$cnt]['end'] . "')", true);
			$result_lastLogged = func_query("select count(*) as num from xcart_customers where last_login >= unix_timestamp('" . $calculated_dates[$cnt]['start'] . "') and last_login < unix_timestamp('" . $calculated_dates[$cnt]['end'] . "')", true);
			$result_logged_verified = func_query("select count(*) as num from xcart_customers c, mk_mobile_mapping mm where mm.login=c.login and c.last_login >= unix_timestamp('" . $calculated_dates[$cnt]['start'] . "') and c.last_login < unix_timestamp('" . $calculated_dates[$cnt]['end'] . "') and mm.status = 'V'", true);
			$result_logVerifiedIntime = func_query("select count(*) as num from xcart_customers c, mk_mobile_mapping mm where mm.login=c.login and c.last_login >= unix_timestamp('" . $calculated_dates[$cnt]['start'] . "') and c.last_login < unix_timestamp('" . $calculated_dates[$cnt]['end'] . "') and mm.status = 'V'
													and mm.date_verified >= unix_timestamp('" . $calculated_dates[$cnt]['start'] . "') and mm.date_verified < unix_timestamp('" . $calculated_dates[$cnt]['end'] . "')", true);

			$table->setCellData(0, $j, $result_referral[0]['num']);
			$table->setCellData(1, $j, $result_referralRegs[0]['num']);
			$table->setCellData(2, $j, (float)$result_totalRegs - (float)$result_referralRegs);
			$table->setCellData(3, $j, $result_lastLogged[0]['num']);
			$table->setCellData(4, $j, $result_logged_verified[0]['num']);
			$table->setCellData(5, $j, $result_logVerifiedIntime[0]['num']);
			$headers[$cnt+1] = $calculated_dates[$cnt]['label']	;
		}

		$table->setHeaders($headers);
		return $table;
	}

	public function referralIncentiveInfo($calculated_dates){
		$table= new Table('Referral Status (Excludes non-referral registrations)');
		$table->setFooterVisible(false);
		$j=0;
		$headers[0] = 'Facts';
		$table->setCellData(0, $j, "Total no of referral registrations" );
		$table->setCellData(1, $j, "Total no of sign up coupons issued (2x non-fb, 3x fb Rs 250) to referred users");
		$table->setCellData(2, $j, "Total no of sign up coupons consumed by referred users");
		$table->setCellData(3, $j, "Total revenue from referred users (includes all orders, with or without any coupons)");
		$table->setCellData(4, $j, "Total no of orders from referred users (includes all orders, with or without any coupons)");
		$table->setCellData(5, $j, "Average order size of referred users (includes all orders, with or without any coupons)");
		//$table->setCellData(4, $j, "CashBack - Total cashback issued on purchase");
		//$table->setCellData(5, $j, "CashBack - Total cashback consumed while purchase");

		for($cnt=0;$cnt<count($calculated_dates);$cnt++){
			$j++;
			$result_referralReg = func_query("select count(referer) as num from mk_referral_log where dateJoined >= unix_timestamp('" . $calculated_dates[$cnt]['start'] . "') and dateJoined < unix_timestamp('" . $calculated_dates[$cnt]['end'] . "')", true);
			$result_newReg = func_query("select count(*) as issued from xcart_discount_coupons dc, xcart_customers c where dc.users = c.login and dc.groupname = 'FirstLogin' and c.referer != '' and dc.timeCreated >= unix_timestamp('" . $calculated_dates[$cnt]['start'] . "') and dc.timeCreated < unix_timestamp('" . $calculated_dates[$cnt]['end'] . "')", true);
			$result_newRegRev = func_query("select count(distinct o.orderid) as orders, sum(o.total +o.shipping_cost+o.gift_charges) as revenue from xcart_orders o, xcart_customers c where o.login = c.login and c.referer != ''
											and o.date >= unix_timestamp('" . $calculated_dates[$cnt]['start'] . "') and o.date < unix_timestamp('" . $calculated_dates[$cnt]['end'] . "') and o.status in ('Q','WP','OH','C','SH','DL')", true);

			$result_newRegConsumed = func_query("select count(*) as consumed from xcart_discount_coupons dc, xcart_customers c where dc.users = c.login and dc.groupname = 'FirstLogin' and c.referer != '' and dc.times_used > 0 and dc.timeCreated >= unix_timestamp('" . $calculated_dates[$cnt]['start'] . "') and dc.timeCreated < unix_timestamp('" . $calculated_dates[$cnt]['end'] . "')", true);

			//$result_cashBack = func_query("select sum(amount) as issued from mk_cashback_log where txn_type = 'C' and date >= unix_timestamp('" . $calculated_dates[$cnt]['start'] . "') and date < unix_timestamp('" . $calculated_dates[$cnt]['end'] . "')",true);
			//$result_cashBackConsumed = func_query("selec t sum(amount) as consumed from mk_cashback_log where txn_type = 'D' and date >= unix_timestamp('" . $calculated_dates[$cnt]['start'] . "') and date < unix_timestamp('" . $calculated_dates[$cnt]['end'] . "')",true);
			$table->setCellData(0, $j, $result_referralReg[0]['num']);
			$table->setCellData(1, $j, $result_newReg[0]['issued']);
			$table->setCellData(2, $j, $result_newRegConsumed[0]['consumed']);
			$table->setCellData(3, $j, (float)$result_newRegRev[0]['revenue']);
			$table->setCellData(4, $j, $result_newRegRev[0]['orders']);
			$table->setCellData(5, $j, (float)$result_newRegRev[0]['revenue']/(float)$result_newRegRev[0]['orders']);
			//$table->setCellData(4, $j, $result_cashBack[0]['issued']);
			//$table->setCellData(5, $j, $result_cashBackConsumed[0]['consumed']);
			$headers[$cnt+1] = $calculated_dates[$cnt]['label']	;
		}

		$table->setHeaders($headers);
		return $table;
	}

	public function userStats($calculated_dates){
		$table= new Table('Referral program user statistics');
		$table->setFooterVisible(false);
		$j=0;
		$headers[0] = 'Facts';
		$table->setCellData(0, $j, "Total no of users who referred" );
		$table->setCellData(1, $j, "Total no. of invites sent");
		$table->setCellData(2, $j, "Total no. of open invites");
		$table->setCellData(3, $j, "Total no. of invites that registered");
		$table->setCellData(4, $j, "Total no. of invites that registered and made first purchase");

		for($cnt=0;$cnt<count($calculated_dates);$cnt++){
			$j++;
			$result_referer = func_query("select count(distinct referer) as numReferer, count(distinct referred) as numInvites from mk_referral_log where dateReferred >= unix_timestamp('" . $calculated_dates[$cnt]['start'] . "') and dateReferred < unix_timestamp('" . $calculated_dates[$cnt]['end'] . "')", true);
			$result_openInvites = func_query("select count(distinct referred) as num from mk_referral_log where dateReferred >= unix_timestamp('" . $calculated_dates[$cnt]['start'] . "') and dateReferred < unix_timestamp('" . $calculated_dates[$cnt]['end'] . "') and dateJoined is null", true);
			$result_registered = func_query("select count(*) as num from mk_referral_log where dateJoined >= unix_timestamp('" . $calculated_dates[$cnt]['start'] . "') and dateJoined < unix_timestamp('" . $calculated_dates[$cnt]['end'] . "')", true);
			$result_firstPur = func_query("select count(*) as num from mk_referral_log where dateFirstPurchase >= unix_timestamp('" . $calculated_dates[$cnt]['start'] . "') and dateFirstPurchase < unix_timestamp('" . $calculated_dates[$cnt]['end'] . "')", true);

			$table->setCellData(0, $j, $result_referer[0]['numReferer']);
			$table->setCellData(1, $j, $result_referer[0]['numInvites']);
			$table->setCellData(2, $j, $result_openInvites[0]['num']);
			$table->setCellData(3, $j, $result_registered[0]['num']);
			$table->setCellData(4, $j, $result_firstPur[0]['num']);
			$headers[$cnt+1] = $calculated_dates[$cnt]['label']	;
		}
		$table->setHeaders($headers);
		return $table;
	}
	
	public function cashbackStatusInfo($calculated_dates){
		$table= new Table('Cashback Status');
		$table->setFooterVisible(false);
		$j=0;
		$headers[0] = 'Facts';
		$table->setCellData(0, $j, "Total cashback issued" );
		$table->setCellData(1, $j, "Total cashback consumed");
		$table->setCellData(2, $j, "Total revenue using cashback");
		$table->setCellData(3, $j, "Total no of orders using cashback");
		$table->setCellData(4, $j, "Average order size using cashback");
		$table->setCellData(5, $j, "Average cashback used per order");

		for($cnt=0;$cnt<count($calculated_dates);$cnt++){
			$j++;
			$result_issued = func_query("select sum(amount) as issued from mk_cashback_log where txn_type = 'C' and date >= unix_timestamp('" . $calculated_dates[$cnt]['start'] . "') and date < unix_timestamp('" . $calculated_dates[$cnt]['end'] . "')", true);
			$result_consumed = func_query("select sum(amount) as consumed from mk_cashback_log where txn_type = 'D' and date >= unix_timestamp('" . $calculated_dates[$cnt]['start'] . "') and date < unix_timestamp('" . $calculated_dates[$cnt]['end'] . "')", true);
			$result_revenue = func_query("select sum(total+shipping_cost+gift_charges) as revenue from xcart_orders o where date >= unix_timestamp('" . $calculated_dates[$cnt]['start'] . "') and date < unix_timestamp('" . $calculated_dates[$cnt]['end'] . "')
			and cash_coupon_code is not null and cash_coupon_code !='' and status in ('Q','WP','OH','C','SH','DL')", true);
			$result_orders = func_query("select count(*) as orders from xcart_orders o where date >= unix_timestamp('" . $calculated_dates[$cnt]['start'] . "') and date < unix_timestamp('" . $calculated_dates[$cnt]['end'] . "')
			and cash_coupon_code is not null and cash_coupon_code !='' and status in ('Q','WP','OH','C','SH','DL')", true);
			//$result_avgCashback = func_query("select avg(cash_redeemed) as avgCashBack from xcart_orders o where date >= unix_timestamp('" . $calculated_dates[$cnt]['start'] . "') and date < unix_timestamp('" . $calculated_dates[$cnt]['end'] . "')
			//and cash_coupon_code is not null and cash_coupon_code !='' and status in ('Q','WP','OH','C','SH','DL')", true);
			
			$table->setCellData(0, $j, $result_issued[0]['issued']);
			$table->setCellData(1, $j, $result_consumed[0]['consumed']);
			$table->setCellData(2, $j, $result_revenue[0]['revenue']);
			$table->setCellData(3, $j, $result_orders[0]['orders']);
			$table->setCellData(4, $j, ((float)$result_revenue[0]['revenue']/(float)$result_orders[0]['orders']));
			$table->setCellData(5, $j, (float)$result_consumed[0]['consumed']/(float)$result_orders[0]['orders']);
			
			$headers[$cnt+1] = $calculated_dates[$cnt]['label']	;
		}
		$table->setHeaders($headers);
		return $table;
	}

	public function buildReport(){
		global $weblog;
		$group= new TableGroup("MRP Statistics");
		$table= new Table('Referral program revenue statistics');
		$table->setFooterVisible(false);
		$filtervalue = $this->getFilterValue("Date:");
		$date_split=split('/', $filtervalue);
		$current_month=(INT)$date_split[0];
		$current_year=(INT)$date_split[2];
		$current_date=(INT)$date_split[1];
		$calculated_dates = calculateDates_DaysFormat($current_date, $current_month, $current_year);

		$j=0;
		$headers[0] = 'Facts';
		$table->setCellData(0, $j, "Total revenue" );
		$table->setCellData(1, $j, "Total revenue from referral program" );
		$table->setCellData(2, $j, "Total revenue from referral program as % of total revenue");
		$table->setCellData(3, $j, "Total no. of orders from referral program");
		$table->setCellData(4, $j, "Average Order Size from referral program");
		$table->setCellData(5, $j, "Repeat revenue from referral program");
		$table->setCellData(6, $j, "Total no. of Buyers - referral program");
		$table->setCellData(7, $j, "% Repeat Buyers - referral program");

		for($cnt=0;$cnt<count($calculated_dates);$cnt++){
			$j++;
			$result_revenue_overall = func_query("select sum(total+shipping_cost+gift_charges) as revenue from xcart_orders where date >= unix_timestamp('" . $calculated_dates[$cnt]['start'] . "') and date < unix_timestamp('" . $calculated_dates[$cnt]['end'] . "') and status in ('Q','WP','OH','C','SH','DL')", true);
			$result_nonRefered = func_query("select count(*) as num, count(distinct o.login) as cust, sum(o.total+o.shipping_cost+o.gift_charges) as revenue  from xcart_orders o, xcart_customers c where o.login = c.login and c.referer = '' and (o.coupon like 'REF%' or o.coupon like 'Reg%' )
			and o.date >= unix_timestamp('" . $calculated_dates[$cnt]['start'] . "') and o.date < unix_timestamp('" . $calculated_dates[$cnt]['end'] . "') 
			and o.status in ('Q','WP','OH','C','SH','DL')", true);
			$result_Refered = func_query("select count(*) as num, count(distinct o.login) as cust, sum(o.total+o.shipping_cost+o.gift_charges) as revenue  from xcart_orders o, xcart_customers c where o.login = c.login and c.referer != ''
			and o.date >= unix_timestamp('" . $calculated_dates[$cnt]['start'] . "') and o.date < unix_timestamp('" . $calculated_dates[$cnt]['end'] . "') 
			and o.status in ('Q','WP','OH','C','SH','DL')", true);
			//MRP launch date is taken as 2011-07-01 i.e. 1st July 2011
			//Repeat logic - referred: checking whether the logins which are referred and have ordered in the given time period using MRP coupons, have ordered using MRP coupons before also, i.e. time between 1st July 2011 and the given login's order date. As the logins are all referred users they would not have ordered using MRP coupons before 1st July 2011.
			$result_repeats_Referred = func_query("select count(distinct b.logins) as repeatCusts, sum(b.value) as repeatRev from (select (case exists (select distinct login from xcart_orders where login=a.login and
				 date< a.date and date >unix_timestamp('2011-07-01') and status in ('C','Q','WP','OH','SH','DL')) when true
				                    then 'REP' else 'NEW'
				                    end ) as ctype, (a.login) as logins, (a.total+a.shipping_cost+a.gift_charges) value
				from xcart_orders a, xcart_customers c where a.login=c.login and c.referer != '' and a.status in ('C','Q','WP','OH','SH','DL') and a.date >=unix_timestamp('" . $calculated_dates[$cnt]['start'] . "') 				
				and  a.date <unix_timestamp('" . $calculated_dates[$cnt]['end'] . "') ) b where b.ctype='REP'", true);
			//Repeat logic - Nonreferred: checking whether the logins which are non-referred and have ordered in the given time period using MRP coupons, have ordered using MRP coupons before also, i.e. time between 1st July 2011 and the given login's order date. As the logins are all referred users they would not have ordered using MRP coupons before 1st July 2011.
			$result_repeats_nonReferred = func_query("select  count(distinct b.logins) as repeatCusts, sum(b.value) as repeatRev from
					(select (case exists (select distinct login from xcart_orders where login=a.login and
				 	date< a.date and date >unix_timestamp('2011-07-01') and status in ('C','Q','WP','OH','SH','DL')) when true
				                    then 'REP' else 'NEW'
				                    end ) as ctype, (a.login) as logins, (a.total+a.shipping_cost+a.gift_charges) value
				from xcart_orders a, xcart_customers c where a.login=c.login and c.referer = '' and (a.coupon like 'REF%' or a.coupon like 'Reg%' ) and a.status in ('C','Q','WP','OH','SH','DL') and a.date >=unix_timestamp('" . $calculated_dates[$cnt]['start'] . "') 				
				and  a.date <unix_timestamp('" . $calculated_dates[$cnt]['end'] . "') ) b where b.ctype='REP' ", true);
			
			$table->setCellData(0, $j, (float)$result_revenue_overall[0]['revenue']);
			$table->setCellData(1, $j, ((float)$result_nonRefered[0]['revenue'] + (float)$result_Refered[0]['revenue']));
			$table->setCellData(2, $j, ((float)$result_nonRefered[0]['revenue'] + (float)$result_Refered[0]['revenue'])*100/(float)$result_revenue_overall[0]['revenue']);
			$table->setCellData(3, $j, ((float)$result_nonRefered[0]['num'] + (float)$result_Refered[0]['num']));
			$table->setCellData(4, $j, ((float)$result_nonRefered[0]['revenue'] + (float)$result_Refered[0]['revenue'])/((float)$result_nonRefered[0]['num'] + (float)$result_Refered[0]['num']));
			$table->setCellData(5, $j, ((float)$result_repeats_nonReferred[0]['repeatRev']+(float)$result_repeats_Referred[0]['repeatRev']));
			$table->setCellData(6, $j, ((float)$result_nonRefered[0]['cust'] + (float)$result_Refered[0]['cust']));
			$table->setCellData(7, $j, ((float)$result_repeats_Referred[0]['repeatCusts'] + (float)$result_repeats_nonReferred[0]['repeatCusts'])*100/((float)$result_nonRefered[0]['cust'] + (float)$result_Refered[0]['cust']));
			$headers[$cnt+1] = $calculated_dates[$cnt]['label']	;

		}
		$width = $cnt;
		$table->setHeaders($headers);
		$table->setTableWidth($width);

		//make other tables
		$userStatsTable = $this->userStats($calculated_dates);
		$referrerStatusTable = $this->referrerStatusInfo($calculated_dates);
		//$referralTable = $this->referralInfo($calculated_dates);
		$referralIncentiveTable = $this->referralIncentiveInfo($calculated_dates);
		$cashbackStatusTable = $this->cashbackStatusInfo($calculated_dates);


		$group->addTable($table);
		$group->addTable($userStatsTable);
		$group->addTable($referrerStatusTable);
		//$group->addTable($referralTable);
		$group->addTable($referralIncentiveTable);
		$group->addTable($cashbackStatusTable);


		$this->addTableGroup($group);
		$this->notes = "If A invites B to sign up, A is called referrer and B is called referral/referred user <br><br>";

		$this->isBuild=true;
	}


}
?>

