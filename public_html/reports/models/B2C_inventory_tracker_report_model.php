<?php

include_once REP_DIR.'/models/models.php';
include_once REP_DIR.'/rendrers/rendrers.php';
include_once $xcart_dir.'/utils/file_utils.php';

class B2CInventoryTrackerReportModel extends ReportModel{
	public function __construct(){
		parent::__construct();
		$this->name="B2C Apperal Inventory Tracker Report";

	}
	protected function initFilters(){
	}

	public function loadDefaultFilterValues(){
	}
	
	public function buildReport(){
		if($this->isBuild)
		return;

		/*$allStyles=func_query("select TRIM((if(opt.value is null ,pstab.psname,concat(pstab.psname,' -',opt.value)))) as name,pstab.psid from 
				(select ps.name psname,ps.id as psid from mk_product_style ps
				 ,xcart_categories cat,mk_style_category_map cmap,mk_product_type pt where ps.product_type=pt.id and  ps.id=cmap.styleid and cmap.categoryid = cat.categoryid
				and ps.is_active=1 and pt.is_active=1 and pt.type='P' and cat.avail='Y' and cat.categoryid not in 
				(select c1.categoryid from xcart_categories c1 left join xcart_categories c2 on true where 
				(c1.categoryid_path like concat('%/',c2.categoryid,'/%') or c1.categoryid_path like concat(c2.categoryid,'/%')
				or c1.categoryid_path like concat('%/',c2.categoryid))  and c1.categoryid!=c2.categoryid and (c2.avail='N' or c1.avail='N')))
				
				pstab left join mk_product_options opt on pstab.psid=opt.style");*/
		
		$dateString = date("Y-m-d",time());
		/*$itemUsed=func_query("select CEILING((o.date-(unix_timestamp('$dateString')-86400*7*8))/(86400*7)) as weeek,(case trim(quantity_breakup)='' when true then ps.name else concat(ps.name,' -',po.value) end)
			as style,
			CAST(sum(
			    case trim(quantity_breakup)='' 
			    when true then od.amount
			    else
			    CASE  LOCATE(po.value,od.quantity_breakup)
			       when 0 then 0
			       when 1 then (SUBSTRING_INDEX(substr(od.quantity_breakup FROM (LOCATE(po.value,od.quantity_breakup)+1 +length(po.value))), ',',1) )
			       else (CASE LOCATE(CONCAT(',',po.value),od.quantity_breakup)
			                   when 0 then 0
			                   else (SUBSTRING_INDEX(substr(od.quantity_breakup FROM (LOCATE(CONCAT(',',po.value),od.quantity_breakup)+2 +length(po.value))), ',',1) )
			               END)
			    END
			   end)
			as CHAR) as quantity
			from mk_product_style ps left join mk_product_options po on po.style=ps.id join xcart_order_details od on ps.id=od.product_style  join 
			xcart_orders o on o.orderid=od.orderid join mk_product_type pt on pt.id=od.product_type 
			 where o.date  >=(unix_timestamp('$dateString')-86400*7*8) and o.date <=unix_timestamp('$dateString') 
			and pt.type='P' and o.status IN ('C') 
			group by weeek,style,po.value");*/
		$itemUsed=func_query("select (case trim(quantity_breakup)='' when true then trim(ps.name) else concat(trim(ps.name),' -',trim(po.value)) end)
			as style,
			CAST(sum(
			    case trim(quantity_breakup)='' 
			    when true then od.amount
			    else
			    CASE  LOCATE(po.value,od.quantity_breakup)
			       when 0 then 0
			       when 1 then (SUBSTRING_INDEX(substr(od.quantity_breakup FROM (LOCATE(po.value,od.quantity_breakup)+1 +length(po.value))), ',',1) )
			       else (CASE LOCATE(CONCAT(',',po.value),od.quantity_breakup)
			                   when 0 then 0
			                   else (SUBSTRING_INDEX(substr(od.quantity_breakup FROM (LOCATE(CONCAT(',',po.value),od.quantity_breakup)+2 +length(po.value))), ',',1) )
			               END)
			    END
			   end)
			as CHAR) as quantity
			from mk_product_style ps left join mk_product_options po on po.style=ps.id join xcart_order_details od on ps.id=od.product_style  join 
			xcart_orders o on o.orderid=od.orderid join mk_product_type pt on pt.id=od.product_type 
			 where o.date  >=(unix_timestamp('$dateString')-2*86400*7) and o.date <=unix_timestamp('$dateString') 
			and pt.type='P' and o.status IN ('C','SH') 
			group by style,po.value", true);
		
		$itemsDetails;
		foreach ($itemUsed as $row){
			$itemsDetails[$row['style']]=$row['quantity'];
		}
		$itemUsed=null;
		$skuListMap=FileUtils::parse_properties_file(REP_DIR."/models/sku.ini"); 
		$group = new TableGroup("B2C Apperal Inventory");
		$table= new Table(5);
		$group->addTable($table);
		$table-> setHeaderValue(0, "Product");
		$table-> setHeaderValue(1, "Item Details");
		$table-> setHeaderValue(2, "Last Two Week Usage");
		$table-> setHeaderValue(3, "Stock in Hand");
		$table-> setHeaderValue(4, "Status");
		$i=0;
		foreach ($skuListMap as $pname=>$itemname){
			$table->setCellData($i, 0, $pname);
			$table->setCellData($i, 1, $itemname);
			$table->setCellData($i, 2, (float)$itemsDetails[$pname]);
			$table->setCellData($i, 3, 0);
			$col='D'.($i+2);
			$lt=1*$itemsDetails[$pname];
			$gt=5*$itemsDetails[$pname];
			$formula="=IF($col<=$lt,\"red\",IF($col>$gt,\"yellow\",\"green\"))";
			$table->setCellStyle($i, 4, Table::$STYLE_EXCEL_FORMULA, $formula);

			$conditionArray[Table::$EQUALS][0][Table::$VALUE]='red';
			$conditionArray[Table::$EQUALS][0][Table::$STYLE_BG_COLOR]='FF0000';
			$conditionArray[Table::$EQUALS][1][Table::$VALUE]='green';
			$conditionArray[Table::$EQUALS][1][Table::$STYLE_BG_COLOR]='00FF00';
			$conditionArray[Table::$EQUALS][2][Table::$VALUE]='yellow';
			$conditionArray[Table::$EQUALS][2][Table::$STYLE_BG_COLOR]='FFFF00';
			

			$table->setCellStyle($i, 4, Table::$STYLE_EXCEL_CONDITIONAL_FORMATTING, $conditionArray);
			$i++;
		}
		
		$this->addTableGroup($group);
		
		$this->isBuild=true;
	}
}
?>