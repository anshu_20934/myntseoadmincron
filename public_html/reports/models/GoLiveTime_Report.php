<?php
/*
global $xcart_dir;
include_once $xcart_dir.'/reports/models/models.php';
include_once $xcart_dir.'/reports/rendrers/rendrers.php';
include_once REP_DIR.'/models/date_calculation.php';

class GoLiveReportModel extends ReportModel {
	
	protected $mode;
	public function __construct(){
		parent::__construct();
		$this->name="Avg. Go Live Time Report : " . date("j-M-Y");
		
	}
	protected function initFilters(){
		$this->addFilter("Date:" ,self::$FILTER_TYPE_DATE);
	}

	public function loadDefaultFilterValues(){
		$time=time();
		$this->setFilterValue("Date:", date("m/d/Y",$time));
	}

	public function detailsInfo($yesterday, $inventory_module){
		$table= new Table('Article Details which went live yesterday');
		$headers = array('Aricle  No', 'Article Name', 'Brand Name', 'Article Type', 'Go Live Time (Hrs)');
		$table->setHeaders($headers);
		$table->setTableWidth(count($headers));
		$table->setFooterVisible(false);
		
			$sql = "select taba.article_number, taba.product_display_name, taba.global_attr_brand, taba.typeName, avg(taba.CatalogCreatedTime - unix_timestamp(tabb.firstInwardTime))/3600 as avgTime 
				from (select sp.style_id, osm.sku_id, sp.article_number, sp.product_display_name, sp.global_attr_brand, (select  typename from myntra.mk_catalogue_classification where id=sp.global_attr_article_type) as typeName,
			    sp.add_date as CatalogCreatedTime from myntra.mk_style_properties sp left join  myntra.mk_styles_options_skus_mapping osm on sp.style_id = osm.style_id join mk_product_style ps on ps.id=sp.style_id where ps.styletype = 'P' and sp.add_date >= unix_timestamp('" .$yesterday['start'] . "') and sp.add_date < unix_timestamp('" .$yesterday['end'] ."')
							) as taba left join (
			    select ci.sku_id, min(atrail.actionTime) as firstInwardTime from myntra.core_items ci , myntra_auditor.audit_trail atrail where ci.id=atrail.entityId and entityName ='com.myntra.framework.entities.Item' 
			    and atrail.entityProperty= 'itemStatus' and entityPropertyOldValue='NOT_RECEIVED' and entityPropertyNewValue='STORED'
			    group by ci.sku_id) as tabb on taba.sku_id=tabb.sku_id group by taba.style_id";
		}
		
		else {
			$sql = "select mv.article_number, mv.product_display_name, mv.global_attr_brand, mv.typeName, avg(mv.CatalogCreatedTime - mv.InvCreatedTime)/3600 as avgTime from (select sp.style_id, sp.article_number, sp.product_display_name, sp.global_attr_brand, (select  typename from mk_catalogue_classification where id=sp.global_attr_article_type) as typeName,
				(select unix_timestamp(created_on) from mk_skus where id = osm.sku_id ) as InvCreatedTime, sp.add_date as CatalogCreatedTime
				 from mk_style_properties sp left join  mk_styles_options_skus_mapping osm on sp.style_id = osm.style_id join mk_product_style ps on ps.id=sp.style_id where ps.styletype = 'P' and sp.add_date >= unix_timestamp('" .$yesterday['start'] . "') and sp.add_date < unix_timestamp('" .$yesterday['end'] ."')
				) as mv group by mv.style_id";
		}
		$result = func_query($sql, true);
		$detailsData=array();
		$k = 0;
		
		foreach ($result as $row){
			$detailsData[$k]['article'] = $row['article_number'];
			$detailsData[$k]['name'] = $row['product_display_name'];
			$detailsData[$k]['brand'] = $row['global_attr_brand'];
			$detailsData[$k]['type'] = $row['typeName'];
			$detailsData[$k]['time'] = (float)$row['avgTime'];
			$k++;
		}
		$i = 0;
		$rowIdentifiers = array('article', 'name', 'brand', 'type', 'time');
		foreach ($detailsData as $rowData){
			for ($j=0; $j<count($headers); $j++){
				$table->setCellData($i, $j, $rowData[$rowIdentifiers[$j]]);
			}
			$i++;
		}
			
		//$table->setStyleMatrix(0, 0, $i-1, 1, TABLE::$STYLE_ALIGN, "left");
		return $table;
	}

	public function slaOverallInfo($calculated_dates, $inventory_module){
		$table= new Table('Catalog SLA - Overall');
		$headers = array();
		$table->setHeaderValue(0, '');
		$table->setCellData(0, 0, 'No Of Articles Went Live');
		$table->setCellData(1, 0, 'Avg Time To Go Live (Hrs)');
		$table->setFooterVisible(false);
		//this will select only the required headers for this table, i.e. WTD, W3, MTD, M3, M2, M1
		$calculated_dates_index = array(8,9,13,14,15,16);
		
		for($cnt=0;$cnt<count($calculated_dates_index);$cnt++){
			$table->setHeaderValue($cnt+1, $calculated_dates[$calculated_dates_index[$cnt]]['label']);
			$result = func_query("select count(distinct taba.style_id) as numStyles, avg(taba.add_date - unix_timestamp(tabb.firstInwardTime))/3600 as avgTime from 
					(select sp.style_id, osm.sku_id, sp.add_date from myntra.mk_styles_options_skus_mapping osm join myntra.mk_style_properties sp on sp.style_id = osm.style_id join mk_product_style ps on ps.id=sp.style_id  
					where ps.styletype = 'P' and sp.add_date >= unix_timestamp('" .$calculated_dates[$calculated_dates_index[$cnt]]['start'] . "') and sp.add_date < unix_timestamp('" .$calculated_dates[$calculated_dates_index[$cnt]]['end'] . "')) as taba left join
					(select ci.sku_id, min(atrail.actionTime) as firstInwardTime from myntra.core_items ci , myntra_auditor.audit_trail atrail where ci.id=atrail.entityId and entityName ='com.myntra.framework.entities.Item' 
					and atrail.entityProperty= 'itemStatus' and entityPropertyOldValue='NOT_RECEIVED' and entityPropertyNewValue='STORED'
					group by ci.sku_id) as tabb on taba.sku_id=tabb.sku_id", true);

			if($result[0]['avgTime']<0){
				$result[0]['avgTime'] = 0;
			}
			$table->setCellData(0, $cnt+1, (float)$result[0]['numStyles']);
			$table->setCellData(1, $cnt+1, (float)$result[0]['avgTime']);
		}
		//$table->setTableWidth(count($headers));		
		return $table;
	}

	public function buildReport(){
		global $weblog;
		$group= new TableGroup("Daily Catalog Report");
		$slaYesterdaytable= new Table("Catalog SLA - Yesterday");
		$headers= array('Total New Articles Uploaded', 'Avg Time To Go Live(Hrs)');
		$slaYesterdaytable->setHeaders($headers);
		$slaYesterdaytable->setTableWidth(count($headers));
		$slaYesterdaytable->setFooterVisible(false);
		$filtervalue = $this->getFilterValue("Date:");
		$date_split=split('/', $filtervalue);
		$current_month=(INT)$date_split[0];
		$current_year=(INT)$date_split[2];
		$current_date=(INT)$date_split[1];
		//function to calculate the end and start days for various headers.
		$calculated_dates = calculateDates($current_date, $current_month, $current_year);
		
			$sql = "select count(distinct taba.style_id) as numStyles, avg(taba.add_date - unix_timestamp(tabb.firstInwardTime))/3600 as avgTime from 
					(select sp.style_id, osm.sku_id, sp.add_date from myntra.mk_styles_options_skus_mapping osm join myntra.mk_style_properties sp on sp.style_id = osm.style_id  join mk_product_style ps on ps.id=sp.style_id  
					where ps.styletype = 'P' and sp.add_date >= unix_timestamp('" . $calculated_dates[0]['start'] . "') and sp.add_date < unix_timestamp('" . $calculated_dates[0]['end'] . "')) as taba left join
					(select ci.sku_id, min(atrail.actionTime) as firstInwardTime from myntra.core_items ci , myntra_auditor.audit_trail atrail where ci.id=atrail.entityId and entityName ='com.myntra.framework.entities.Item' 
					and atrail.entityProperty= 'itemStatus' and entityPropertyOldValue='NOT_RECEIVED' and entityPropertyNewValue='STORED'
					group by ci.sku_id) as tabb on taba.sku_id=tabb.sku_id";

			$result_slaYesterday=func_query($sql, true);
		
		$slaYesterdaytable->setCellData(0, 0, (float)$result_slaYesterday[0]['numStyles']);
		$slaYesterdaytable->setCellData(0, 1, (float)$result_slaYesterday[0]['avgTime']);
		
		//make other tables
		$detailsTable = $this->detailsInfo($calculated_dates[0], $inventory_module);
		$slaOverallTable = $this->slaOverallInfo($calculated_dates, $inventory_module);
		
		$group->addTable($slaYesterdaytable);
		$group->addTable($detailsTable);
		$group->addTable($slaOverallTable);
		$this->addTableGroup($group);

		$this->isBuild=true;
	}


}*/
?>

