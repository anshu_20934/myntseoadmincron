<?php

global $xcart_dir;

//include_once('auth.php');
//define("REP_DIR",$xcart_dir."/reports");
include_once ($xcart_dir."/reports/report_model.ph99");
require_once $xcart_dir."/admin/codAdminHelper.php";
include_once $xcart_dir.'/reports/models/models.php';
include_once $xcart_dir.'/reports/rendrers/rendrers.php';
include_once REP_DIR.'/models/date_calculation.php';

class CodMonthlyReportModel extends ReportModel {

	protected $mtdDaysCount;
	protected $qtdDaysCount;
	
	public function __construct(){
		parent::__construct();
		$this->name="COD Report";

	}
	protected function initFilters(){
		$this->addFilter("Date:" ,self::$FILTER_TYPE_DATE);
	}

	public function loadDefaultFilterValues(){
		$time=time();
		$this->setFilterValue("Date:", date("m/d/Y",$time));
		
	}
	
	public function getPercentage($partialValue, $totalValue) {
		
		return ((float)$partialValue/(float)$totalValue)*100;		
	}
	
	public function getMonthString($dateStr) {
		$dateArr = explode("-", $dateStr);
		$MONTH_NAME=array('00'=>'Dec','01'=>'Jan','02'=>'Feb','03'=>'Mar','04'=>'Apr','05'=>'May','06'=>'Jun','07'=>'Jul','08'=>'Aug','09'=>'Sep','10'=>'Oct','11'=>'Nov','12'=>'Dec');
		return $MONTH_NAME[$dateArr[1]]."-".$dateArr[0];
	}
	public function globalReportHeader($baseString,$prev1MonthData, $prev2MonthData, $prev3MonthData ) {
		$header = array ($baseString, $this->getMonthString($prev1MonthData['orderStartDate']), $this->getMonthString($prev2MonthData['orderStartDate']),$this->getMonthString($prev3MonthData['orderStartDate']));
		return $header;
	}
	public function getTranscationInfoTable($prev1MonthData, $prev2MonthData, $prev3MonthData) {
		$table= new Table();
		$header = $this->globalReportHeader("# Transactions", $prev1MonthData, $prev2MonthData, $prev3MonthData);
		$table->setHeaders($header);
		$table->setCellData(0,0,"COD" );
		$table->setCellData(1,0,"NON COD" );
		$table->setCellData(2,0,"%COD" );
		$codTransData = cod_transcation_info($prev1MonthData);
		$table->setCellData(0,1,(float)$codTransData[0]['total_count']);
		$codTransData = cod_transcation_info($prev2MonthData);
		$table->setCellData(0,2,(float)$codTransData[0]['total_count']);
		$codTransData = cod_transcation_info($prev3MonthData);
		$table->setCellData(0,3,(float)$codTransData[0]['total_count']);
		
		$noncodTransData = non_cod_transcation_info($prev1MonthData);
		$table->setCellData(1,1,(float)$noncodTransData[0]['total_count']);
		$noncodTransData = non_cod_transcation_info($prev2MonthData);
		$table->setCellData(1,2,(float)$noncodTransData[0]['total_count']);
		$noncodTransData = non_cod_transcation_info($prev3MonthData);
		$table->setCellData(1,3,(float)$noncodTransData[0]['total_count']);
		
		$table->setCellData(2,1,  $this->getPercentage($table->getCellData(0, 1),$table->getCellData(0, 1)+ $table->getCellData(1, 1)));
		$table->setCellData(2,2,  $this->getPercentage($table->getCellData(0, 2),$table->getCellData(0, 2)+ $table->getCellData(1, 2)));
		$table->setCellData(2,3,  $this->getPercentage($table->getCellData(0, 3),$table->getCellData(0, 3)+ $table->getCellData(1, 3)));
		$table->setFooterVisible(false);
		return $table;
		
	}
	
	public function getValueInfoTable($prev1MonthData, $prev2MonthData, $prev3MonthData) {
		$table= new Table();
		$header =  $this->globalReportHeader(" Value ", $prev1MonthData, $prev2MonthData, $prev3MonthData);
		$table->setHeaders($header);
		$table->setCellData(0,0,"COD" );
		$table->setCellData(1,0,"NON COD" );
		$table->setCellData(2,0,"%COD" );
		$codTransData = cod_value_info($prev1MonthData);
		$table->setCellData(0,1,(float)$codTransData[0]['total_amount']);
		$codTransData = cod_value_info($prev2MonthData);
		$table->setCellData(0,2,(float)$codTransData[0]['total_amount']);
		$codTransData = cod_value_info($prev3MonthData);
		$table->setCellData(0,3,(float)$codTransData[0]['total_amount']);
		
		$noncodTransData = non_cod_value_info($prev1MonthData);
		$table->setCellData(1,1,(float)$noncodTransData[0]['total_amount']);
		$noncodTransData = non_cod_value_info($prev2MonthData);
		$table->setCellData(1,2,(float)$noncodTransData[0]['total_amount']);
		$noncodTransData = non_cod_value_info($prev3MonthData);
		$table->setCellData(1,3,(float)$noncodTransData[0]['total_amount']);
		
		$table->setCellData(2,1,  $this->getPercentage($table->getCellData(0, 1),$table->getCellData(0, 1)+ $table->getCellData(1, 1)));
		$table->setCellData(2,2,  $this->getPercentage($table->getCellData(0, 2),$table->getCellData(0, 2)+ $table->getCellData(1, 2)));
		$table->setCellData(2,3,  $this->getPercentage($table->getCellData(0, 3),$table->getCellData(0, 3)+ $table->getCellData(1, 3)));
		$table->setFooterVisible(false);
		return $table;
		
	}
	public function getAvgOrderSize($transcationTable, $valueTable) {
		$table= new Table();
		//$hearder = array("Avg Order Size(Rs)", "Yesterday",	"MonthlyDailyAvg","QtrlyDailyAvg");
		//$table->setHeaders($hearder);
		$header =  $transcationTable->getHeaders();
		$header[0] = "Avg Order Size";
		$table->setHeaders($header);
		$table->setCellData(0,0,"COD" );
		$table->setCellData(1,0,"NON COD" );
		$table->setCellData(0,1,(float)$valueTable->getCellData(0,1)/(float)$transcationTable->getCellData(0,1));
		$table->setCellData(0,2,(float)$valueTable->getCellData(0,2)/(float)$transcationTable->getCellData(0,2));
		$table->setCellData(0,3,(float)$valueTable->getCellData(0,3)/(float)$transcationTable->getCellData(0,3));
		$table->setCellData(1,1,(float)$valueTable->getCellData(1,1)/(float)$transcationTable->getCellData(1,1));
		$table->setCellData(1,2,(float)$valueTable->getCellData(1,2)/(float)$transcationTable->getCellData(1,2));
		$table->setCellData(1,3,(float)$valueTable->getCellData(1,3)/(float)$transcationTable->getCellData(1,3));								
		$table->setFooterVisible(false);
		return $table;
		
	}
	
	public function getItemPerOrder($prev1MonthData, $prev2MonthData, $prev3MonthData) {
		$table= new Table();
		$header =  $this->globalReportHeader("Items/Order (#)", $prev1MonthData, $prev2MonthData, $prev3MonthData);
		$table->setHeaders($header);
		$table->setCellData(0,0,"COD" );
		$table->setCellData(1,0,"NON COD" );
		$codTransData = cod_item_per_order($prev1MonthData);
		$table->setCellData(0,1,(float)$codTransData[0]['item_number']);
		$codTransData = cod_item_per_order($prev2MonthData);
		$table->setCellData(0,2,(float)$codTransData[0]['item_number']);
		$codTransData = cod_item_per_order($prev3MonthData);
		$table->setCellData(0,3,(float)$codTransData[0]['item_number']);
		
		$noncodTransData = non_cod_item_per_order($prev1MonthData);
		$table->setCellData(1,1,(float)$noncodTransData[0]['item_number']);
		$noncodTransData = non_cod_item_per_order($prev2MonthData);
		$table->setCellData(1,2,(float)$noncodTransData[0]['item_number']);
		$noncodTransData = non_cod_item_per_order($prev3MonthData);
		$table->setCellData(1,3,(float)$noncodTransData[0]['item_number']);

		//print_r($table->getAllRows());
		$table->setFooterVisible(false);
		return $table;
		
	}
	
	public function getPercentageRetrun($prev1MonthData, $prev2MonthData, $prev3MonthData) {
		$table= new Table();
		$header =  $this->globalReportHeader(" %Retrun Info ", $prev1MonthData, $prev2MonthData, $prev3MonthData);
		$table->setHeaders($header);
		$table->setCellData(0,0,"%Return" );
		$prev1MonthShipData['shippingStartDate'] =  $prev1MonthData['orderStartDate'];
		$prev1MonthShipData['shippingEndDate']   =  $prev1MonthData['orderEndDate'];
		$prev2MonthShipData['shippingStartDate'] =  $prev2MonthData['orderStartDate'];
		$prev2MonthShipData['shippingEndDate']   =  $prev2MonthData['orderEndDate'];
		$prev3MonthShipData['shippingStartDate'] =  $prev3MonthData['orderStartDate'];
		$prev3MonthShipData['shippingEndDate']   =  $prev3MonthData['orderEndDate'];
		$table->setCellData(0, 1,  get_percentage_return($prev1MonthShipData));
		$table->setCellData(0, 2,  get_percentage_return($prev2MonthShipData));
		$table->setCellData(0, 3,  get_percentage_return($prev3MonthShipData));		
		$table->setFooterVisible(false);
		return $table;
		
	} 
	public function getAvgPaymentCycleInfo($prev1MonthData, $prev2MonthData, $prev3MonthData) {
		$table= new Table();
		$header =  $this->globalReportHeader(" Avg Payment Cycle Info ", $prev1MonthData, $prev2MonthData, $prev3MonthData);
		$table->setHeaders($header);
		$table->setCellData(0,0,"Avg Payment Cycle" );
		$prev1MonthShipData['paymentStartDate'] =  $prev1MonthData['orderStartDate'];
		$prev1MonthShipData['paymentEndDate']   =  $prev1MonthData['orderEndDate'];
		$prev2MonthShipData['paymentStartDate'] =  $prev2MonthData['orderStartDate'];
		$prev2MonthShipData['paymentEndDate']   =  $prev2MonthData['orderEndDate'];
		$prev3MonthShipData['paymentStartDate'] =  $prev3MonthData['orderStartDate'];
		$prev3MonthShipData['paymentEndDate']   =  $prev3MonthData['orderEndDate'];
		$table->setCellData(0, 1,  (float)get_avg_payment_cycle($prev1MonthShipData));
		$table->setCellData(0, 2,  (float)get_avg_payment_cycle($prev2MonthShipData));
		$table->setCellData(0, 3,  (float)get_avg_payment_cycle($prev3MonthShipData));		
		$table->setFooterVisible(false);
		return $table;
		
	} 
	public function buildReport(){
		global $weblog;
		$table= new Table();
		$group= new TableGroup("COD  Monthly Report");
		
		$table->setFooterVisible(false);
		$filtervalue = $this->getFilterValue("Date:");
		$date_split=split('/', $filtervalue);
		$current_month=(INT)$date_split[0];
		$current_year=(INT)$date_split[2];
		$current_date=(INT)$date_split[1];
		$curr_fyear = new FYear($current_date, $current_month, $current_year);
		$curr_fdate= new FDate($current_date, $current_month, $current_year);
		$prev1MonthData=array();
		$prev2MonthData= array();
		$prev3MonthData = array();
		
		$currMonthStartData = 
		//Getting start Day Date and end date  
		$startDate = $curr_fdate->getTime();
		$prev1MonthStartDate = $curr_fdate->getPrevMonthStartFDate();
		$prev1MonthEndDate   = $curr_fyear->getEndDateOfMonth($prev1MonthStartDate);
		$prev1MonthData['orderStartDate'] = $prev1MonthStartDate->getTimeDateString();
		$prev1MonthData['orderEndDate']   = $prev1MonthEndDate->getTimeDateString();
		$weblog->info("COD Monthly Report ::prevMonthStartDate".$prev1MonthStartDate->getTimeDateString());
		$weblog->info("COD Monthly Report ::prevMonthStartDate".$prev1MonthEndDate->getTimeDateString());
		
		$prev2MonthStartDate = $prev1MonthStartDate->getPrevMonthStartFDate();
	

		$prev2MonthEndDate   = $curr_fyear->getEndDateOfMonth($prev2MonthStartDate);
		$prev2MonthData['orderStartDate'] = $prev2MonthStartDate->getTimeDateString();
		$prev2MonthData['orderEndDate']   = $prev2MonthEndDate->getTimeDateString();
		
		$weblog->info("COD Monthly Report ::prev2MonthStartDate ".$prev2MonthStartDate->getTimeDateString());
		$weblog->info("COD Monthly Report ::prev2MonthStartDate".$prev2MonthEndDate->getTimeDateString());
		
		$prev3MonthStartDate = $prev2MonthStartDate->getPrevMonthStartFDate();
		$prev3MonthEndDate   = $curr_fyear->getEndDateOfMonth($prev3MonthStartDate);
		$prev3MonthData['orderStartDate'] = $prev3MonthStartDate->getTimeDateString();
		$prev3MonthData['orderEndDate']   = $prev3MonthEndDate->getTimeDateString();
		
		$weblog->info("COD Monthly Report ::prev3MonthStartDate".$prev3MonthStartDate->getTimeDateString());
		$weblog->info("COD Monthly Report ::prev3MonthStartDate".$prev3MonthEndDate->getTimeDateString());
		
		$transcationInfoTable = $this->getTranscationInfoTable($prev1MonthData, $prev2MonthData, $prev3MonthData);
		$valueInfoTable = $this->getValueInfoTable($prev1MonthData, $prev2MonthData, $prev3MonthData);
		$itemInfoTable = $this->getItemPerOrder($prev1MonthData, $prev2MonthData, $prev3MonthData);
		$avgValueTable  = $this->getAvgOrderSize($transcationInfoTable, $valueInfoTable);
		$percentReturn  = $this->getPercentageRetrun($prev1MonthData, $prev2MonthData, $prev3MonthData);
		$avgPaymentCycle = $this->getAvgPaymentCycleInfo($prev1MonthData, $prev2MonthData, $prev3MonthData);
		/*I am passing here a garbage value as input to function as passing a empty array 
		  that online report should not break because of it */
		$group->addTable($transcationInfoTable);
		$group->addTable($valueInfoTable);
		$group->addTable($avgValueTable);
		$group->addTable($itemInfoTable);		
		$group->addTable($percentReturn);
		$group->addTable($avgPaymentCycle);	
		$this->addTableGroup($group);
		$this->isBuild=true;
	}
	
	
}

//$codDate = new CodDailyReportModel();

//$codDate->buildReport();
?>

