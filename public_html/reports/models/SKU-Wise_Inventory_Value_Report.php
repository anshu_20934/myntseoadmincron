<?php

global $xcart_dir;
include_once $xcart_dir.'/reports/models/models.php';
include_once $xcart_dir.'/reports/rendrers/rendrers.php';
include_once REP_DIR.'/models/date_calculation.php';
ini_set("memory_limit",'4000M');

class SKUInventoryValueReportModel extends ReportModel {

	public function __construct(){
		parent::__construct();
		$this->name="SKU-Wise Inventory Value Report : " . date("j-M-Y");

	}
	protected function initFilters(){
		$this->addFilter("Date:" ,self::$FILTER_TYPE_DATE);
	}

	public function loadDefaultFilterValues(){
		$time=time();
		$this->setFilterValue("Date:", date("m/d/Y",$time));
	}



	public function buildReport(){
		global $weblog;
		$table= new Table();
		$group= new TableGroup("SKU-Wise Inventory Value Report");
		$headers= array('Master Category','Sub-Category','Article Type','Brand','SKU Code', 'JIT',
		'MRP', 'Cost Price','Weighted Average Age (Days)','# units with age 0-30 Days','# units with age 30-60 Days','# units with age 60-90 Days',
		'# units with age 90+ Days');
		$table->setHeaders($headers);
		$table->setTableWidth(count($headers));
		$table->setFooterVisible(false);
		$wms_con = mysql_connect('10.130.27.161','MyntraRep0rtUsEr','9eguawKETuBRVEku');
		if(!$wms_con) {
	        echo "ERROR MySQL: Connect to WMS Server\n";
	        exit;
		}
		$ReportData;

		$sql = "select sp.style_id, sp.global_attr_brand
				,mas.typename masname,sub.typename subname,typ.typename articletype 
				from mk_style_properties sp left join mk_catalogue_classification mas on  sp.global_attr_master_category=mas.id left join mk_catalogue_classification sub 
				on sp.global_attr_sub_category =sub.id left join  mk_catalogue_classification typ on  sp.global_attr_article_type=typ.id";

		$result = func_query($sql, true);
		foreach ($result as $row ){
			$styles[$row['style_id']]['brand']=$row['global_attr_brand'];
			$styles[$row['style_id']]['master']=$row['masname'];
			$styles[$row['style_id']]['subname']=$row['subname'];
			$styles[$row['style_id']]['articletype']=$row['articletype'];
			$styles[$row['style_id']]['id']=$row['style_id'];
		}

		$sql="select s.code, m.style_id, m.option_id, ps.price, s.jit_sourced from mk_skus s join mk_styles_options_skus_mapping m on s.id=m.sku_id
			left join mk_product_style ps  on m.style_id=ps.id";

		$result = func_query($sql, true);
		foreach ($result as $row ){
			$ReportData[$row['style_id']."-".$row['option_id']]['sku']=$row['code'];
			$ReportData[$row['style_id']."-".$row['option_id']]['mrp']=$row['price'];
			$ReportData[$row['style_id']."-".$row['option_id']]['jit']=$row['jit_sourced'];
			$ReportData[$row['style_id']."-".$row['option_id']]['style']=$styles[$row['style_id']];
		}

			$sql="select m.style_id, m.option_id
			, sum(case true when datediff(curdate(), atrail.actionTime) <= 30 then 1 else 0 end) as '0-30 days', 
			sum(case true when datediff(curdate(), atrail.actionTime) between 31 and 60 then 1 else 0 end) as '30-60 days',
			sum(case true when datediff(curdate(), atrail.actionTime) between 61 and 90 then 1 else 0 end) as '60-90 days',
			sum(case true when datediff(curdate(), atrail.actionTime) > 90 then 1 else 0 end) as '90+ days',
			avg(datediff(curdate(),atrail.actionTime)) as 'weightedAvg'
			from myntra.core_items ci , myntra_auditor.audit_trail atrail, myntra.mk_styles_options_skus_mapping m 
			where ci.id=atrail.entityId and ci.sku_id=m.sku_id and entityName ='com.myntra.framework.entities.Item' 
			and atrail.entityProperty= 'itemStatus' and atrail.entityPropertyOldValue='NOT_RECEIVED' and atrail.entityPropertyNewValue='STORED' and ci.item_status != 'SHIPPED'
			group by ci.sku_id";		
			$result = func_query($sql, true);			
			foreach ($result as $row ){
				$ReportData[$row['style_id']."-".$row['option_id']]['0-30D']=$row['0-30 days'];
				$ReportData[$row['style_id']."-".$row['option_id']]['30-60D']=$row['30-60 days'];
				$ReportData[$row['style_id']."-".$row['option_id']]['60-90D']=$row['60-90 days'];
				$ReportData[$row['style_id']."-".$row['option_id']]['90+D']=$row['90+ days'];
				$ReportData[$row['style_id']."-".$row['option_id']]['avgDays']=$row['weightedAvg'];
			}
			$sql="select parent_sku_id as sku_id, sku_unit_price as cp from myntra_wms.core_grn_skus group by parent_sku_id";
			$cost_price_result = mysql_query($sql, $wms_con);	
			while ($row = mysql_fetch_assoc($cost_price_result)){
				$sql="select style_id, option_id from mk_styles_options_skus_mapping where sku_id=".$row['sku_id'];
				$result = func_query($sql, true);
				if(isset($ReportData[$result[0]['style_id']."-".$result[0]['option_id']]))
					$ReportData[$result[0]['style_id']."-".$result[0]['option_id']]['costPrice']=$row['cp'];
			}		
			
		$i=0;
		foreach($ReportData as $info){
			if(empty($info['sku']))
			continue;
			$j=0;
			$table->setCellData($i, $j++, $info['style']['master']);
			$table->setCellData($i, $j++, $info['style']['subname']);
			$table->setCellData($i, $j++, $info['style']['articletype']);
			$table->setCellData($i, $j++, $info['style']['brand']);
			$table->setCellData($i, $j++, $info['sku']);
			$table->setCellData($i, $j++, $info['jit']);
			$table->setCellData($i, $j++, $info['mrp']);
			$table->setCellData($i, $j++, (float)$info['costPrice']);
			$table->setCellData($i, $j++, (float)$info['avgDays']);
			$table->setCellData($i, $j++, $info['0-30D']);
			$table->setCellData($i, $j++, $info['30-60D']);
			$table->setCellData($i, $j++, $info['60-90D']);
			$table->setCellData($i, $j++, $info['90+D']);
			$i++;
		}
		$group->addTable($table);
		$this->addTableGroup($group);
		$this->isBuild=true;
		unset($ReportData);
		unset($info);
		unset($result);

	}


}
?>

