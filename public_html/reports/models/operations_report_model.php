<?php
include_once REP_DIR.'/models/models.php';
include_once REP_DIR.'/rendrers/rendrers.php';
include_once REP_DIR.'/models/date_calculation.php';

class OperationOverALLSLAReportModel extends ReportModel{
	public function __construct(){
		parent::__construct();
		$this->name="Operations Report";

	}
	protected function initFilters(){
		$this->addFilter("Date:" ,self::$FILTER_TYPE_DATE);
	}

	public function loadDefaultFilterValues(){
		$time=time();
		$this->setFilterValue("Date:", date("m/d/Y",$time));
	}

	public function jitOverallSLA($calculated_dates){
		$table= new Table('JIT - Overall SLA');
		$table->setFooterVisible(false);
		$j=0;
		$headers[0]= 'Facts';
		$table->setCellData(0, $j,"No. of Orders Received" );
		$table->setCellData(1, $j,"% Orders Shipped in 24 hours");
		$table->setCellData(2, $j,"% Orders Shipped in 48 hours");
		$table->setCellData(3, $j,"% Orders Shipped in 72 hours");
		$table->setCellData(4, $j,"% Orders Shipped Beyond 72 hours");
		$table->setCellData(5, $j,"% Unshipped Orders (pending verification + not acted by Ops)");
		$table->setCellData(6, $j, "Avg. Order Fulfilment time (in hours)");
		$table->setCellData(7, $j, "No. of Orders Cancelled");
		$table->setCellData(8, $j, "No. of Orders Shipped");
		$table->setCellData(9, $j, "Total No. of Orders Pending (Cumulative)");
		$table->setCellData(10, $j, "# Orders with at least 1 item OOS");
		$table->setCellData(11, $j, "% Orders shipped on same date");
		$table->setCellData(12, $j, "% Orders not shipped on same date");
		$j++;
		$ReportData;
		for($cnt=0; $cnt<count($calculated_dates); $cnt++){
			$resultTotal=func_query("select count(distinct o.orderid) as numoforders from xcart_orders o, xcart_order_details od where o.orderid=od.orderid and o.date >= unix_timestamp('" . $calculated_dates[$cnt]['start'] . "')+43200
					 and o.date< unix_timestamp('" . $calculated_dates[$cnt]['end'] . "')+43200 and o.status in ('OH','C','WP','SH','Q','DL') and od.jit_purchase = 1", true);
			$result=func_query("select (case true when mv1.shippeddate is null then 'Unshipped' when (((mv1.shippeddate - ifnull(mv1.date, mv1.shippeddate))/ 3600) between 0 and 24) then '<24'
					 when (((mv1.shippeddate - ifnull(mv1.date, mv1.shippeddate))/ 3600) between 24 and 48) then '<48'
					 when (((mv1.shippeddate - ifnull(mv1.date, mv1.shippeddate))/ 3600) between 48 and 72) then '<72'
					 else '>72' end) as timeToShip, count(mv1.orders) as numoforders from ( 
					 select  o.orderid as orders, o.shippeddate, o.date from xcart_orders o, xcart_order_details od where o.orderid=od.orderid and
		    		 o.date >=  unix_timestamp('" . $calculated_dates[$cnt]['start'] . "')+43200 and o.date< unix_timestamp('" . $calculated_dates[$cnt]['end'] . "')+43200 
		    		 and o.status in ('OH','C','WP','SH','Q','DL') and od.jit_purchase = 1 group by  o.orderid )  as mv1 group by timeToShip order by timeToShip", true);
			$resultAvgTime = func_query("select avg(mv1.shippeddate - ifnull(mv1.date, mv1.shippeddate))/ 3600 as avgTime from
					(select o.orderid as orders, o.shippeddate, o.date from xcart_orders o, xcart_order_details od where o.orderid=od.orderid and
		    		 o.date >= unix_timestamp('" . $calculated_dates[$cnt]['start'] . "')+43200 and o.date< unix_timestamp('" . $calculated_dates[$cnt]['end'] . "')+43200 
		    		 and o.status in ('C','SH','DL') and  od.jit_purchase = 1 group by  o.orderid ) mv1 ", true);
			$resultFailed = func_query("select count(distinct o.orderid) as orders from xcart_orders o , xcart_order_details od where o.orderid=od.orderid and
					 o.date >= unix_timestamp('" . $calculated_dates[$cnt]['start'] . "')+43200 and o.date< unix_timestamp('" . $calculated_dates[$cnt]['end'] . "')+43200 
					 and o.status in ('F') and od.jit_purchase = 1", true);
			$resultShipped = func_query("select count(distinct o.orderid) as numoforders from xcart_orders o, xcart_order_details od where o.orderid=od.orderid
					 and o.shippeddate >= unix_timestamp('" . $calculated_dates[$cnt]['start'] . "')+43200 and o.shippeddate< unix_timestamp('" . $calculated_dates[$cnt]['end'] . "')+43200 and od.jit_purchase = 1", true);
			$resultPending = func_query(" select count(distinct o.orderid) as numoforders from xcart_orders o, xcart_order_details od where o.orderid=od.orderid
					 and o.date< unix_timestamp('" . $calculated_dates[$cnt]['end'] . "')+43200 and o.status in ('OH','WP','Q') and od.jit_purchase = 1", true);
			$result_oos = func_query("select count(distinct o.orderid) as numoforders from xcart_orders o join xcart_order_details od on o.orderid=od.orderid where o.date >= unix_timestamp('" . $calculated_dates[$cnt]['start'] . "')+43200
					 and o.date< unix_timestamp('" . $calculated_dates[$cnt]['end'] . "')+43200 and o.status in ('OH','C','WP','SH','Q','DL') and od.assignee in ('8','10','33','34') and od.jit_purchase = 1", true);
			$resultShipped_samedate = func_query("select count(distinct o.orderid) as numoforders from xcart_orders o, xcart_order_details od where o.orderid=od.orderid
					 and o.date >= unix_timestamp('" . $calculated_dates[$cnt]['start'] . "')+43200 and o.date< unix_timestamp('" . $calculated_dates[$cnt]['end'] . "')+43200 
					 and o.status in ('OH','C','WP','SH','Q','DL') and date(from_unixtime(o.shippeddate)) = date(from_unixtime(o.date)) and od.jit_purchase = 1", true);
			$resultShipped_otherdate = func_query("select count(distinct o.orderid) as numoforders from xcart_orders o, xcart_order_details od where o.orderid=od.orderid
					 and o.date >= unix_timestamp('" . $calculated_dates[$cnt]['start'] . "')+43200 and o.date< unix_timestamp('" . $calculated_dates[$cnt]['end'] . "')+43200 
					 and o.status in ('OH','C','WP','SH','Q','DL') and (date(from_unixtime(o.shippeddate)) != date(from_unixtime(o.date)) or o.shippeddate is null) and od.jit_purchase = 1", true);

			$ReportData['Total'][$cnt]=(float)$resultTotal[0]['numoforders'];
			$ReportData['Failed'][$cnt]=(float)$resultFailed[0]['numoforders'];
			$ReportData['Shipped'][$cnt]=(float)$resultShipped[0]['numoforders'];
			$ReportData['Pending'][$cnt]=(float)$resultPending[0]['numoforders'];
			$ReportData['AvgTime'][$cnt]=(float)$resultAvgTime[0]['avgTime'];
			$ReportData['OOS'][$cnt]=(float)$result_oos[0]['numoforders'];
			$ReportData['SameDateShipping'][$cnt]=(float)($resultShipped_samedate[0]['numoforders']*100)/$ReportData['Total'][$cnt] ;
			$ReportData['OtherDateShipping'][$cnt]=(float)($resultShipped_otherdate[0]['numoforders']*100)/$ReportData['Total'][$cnt];
			foreach($result as $row){
				$ReportData[$row['timeToShip']][$cnt]=(float)($row['numoforders']*100)/$ReportData['Total'][$cnt];
			}
			$headers[$cnt+1] = $calculated_dates[$cnt]['label'];
		}
		ksort($ReportData);

		$width = $cnt+1;
		$table->setHeaders($headers);
		$table->setTableWidth($width);
		$rowHeaderValues = array('Total', '<24', '<48', '<72', '>72', 'Unshipped','AvgTime', 'Failed', 'Shipped', 'Pending', 'OOS', 'SameDateShipping', 'OtherDateShipping');
		$i=0;
		for($cnt=0; $cnt<count($rowHeaderValues); $cnt++){
			for ($j=1; $j<$width; $j++){
				$table->setCellData($i, $j, $ReportData[$rowHeaderValues[$cnt]][$j-1]);
			}
			$i++;
		}

		return $table;
	}

	public function buildReport(){
		if($this->isBuild)
		return;

		$filtervalue = $this->getFilterValue("Date:");
		$date_split=split('/', $filtervalue);
		$current_month=(INT)$date_split[0];
		$current_year=(INT)$date_split[2];
		$current_date=(INT)$date_split[1];
		$calculated_dates = calculateDates($current_date, $current_month, $current_year);
		$group= new TableGroup("Overall - SLA");
		$this->addTableGroup($group);
		$table= new Table();
		$table->setFooterVisible(false);

		$j=0;
		$headers[0]= 'Facts';
		$table->setCellData(0, $j,"No. of Orders Received" );
		$table->setCellData(1, $j,"% Orders Shipped in 24 hours");
		$table->setCellData(2, $j,"% Orders Shipped in 48 hours");
		$table->setCellData(3, $j,"% Orders Shipped in 72 hours");
		$table->setCellData(4, $j,"% Orders Shipped Beyond 72 hours");
		$table->setCellData(5, $j,"% Unshipped Orders (pending verification + not acted by Ops)");
		$table->setCellData(6, $j, "Avg. Order Fulfilment time (in hours)");
		$table->setCellData(7, $j, "No. of Orders Cancelled");
		$table->setCellData(8, $j, "No. of Orders Shipped");
		$table->setCellData(9, $j, "Total No. of Orders Pending (Cumulative)");
		$table->setCellData(10, $j, "# Orders with at least 1 item OOS");
		$table->setCellData(11, $j, "% Orders shipped on same date");
		$table->setCellData(12, $j, "% Orders not shipped on same date");
		$j++;
		$ReportData;
		for($cnt=0; $cnt<count($calculated_dates); $cnt++){
			$resultTotal=func_query("select count(*) as numoforders from (select o.orderid as orders, sum(od.jit_purchase) as jitSum from xcart_orders o , xcart_order_details od where o.orderid=od.orderid and
				 	 o.date >=  unix_timestamp('" . $calculated_dates[$cnt]['start'] . "')+43200 and o.date< unix_timestamp('" . $calculated_dates[$cnt]['end'] . "')+43200 and o.status in ('OH','C','WP','SH','Q','DL')  group by  o.orderid having jitSum = 0) mv1 ", true);
			$result=func_query("select (case true when mv1.shippeddate is null then 'Unshipped' when (((mv1.shippeddate - ifnull(mv1.date, mv1.shippeddate))/ 3600) between 0 and 24) then '<24'
					 when (((mv1.shippeddate - ifnull(mv1.date, mv1.shippeddate))/ 3600) between 24 and 48) then '<48'
					 when (((mv1.shippeddate - ifnull(mv1.date, mv1.shippeddate))/ 3600) between 48 and 72) then '<72'
					 else '>72' end) as timeToShip, count(mv1.orders) as numoforders from ( 
					 select  o.orderid as orders, o.shippeddate, o.date, sum(od.jit_purchase) as jitSum from   xcart_orders o, xcart_order_details od where o.orderid=od.orderid and
		    		 o.date >=  unix_timestamp('" . $calculated_dates[$cnt]['start'] . "')+43200 and o.date< unix_timestamp('" . $calculated_dates[$cnt]['end'] . "')+43200 
		    		 and o.status in ('OH','C','WP','SH','Q','DL')  group by o.orderid having jitSum = 0)  as mv1 group by timeToShip order by timeToShip", true);
			$resultAvgTime = func_query("select avg(mv1.shippeddate - ifnull(mv1.date, mv1.shippeddate))/ 3600 as avgTime from
					(select o.orderid as orders, o.shippeddate, o.date, sum(od.jit_purchase) as jitSum from xcart_orders o, xcart_order_details od where o.orderid=od.orderid and
		    		 o.date >= unix_timestamp('" . $calculated_dates[$cnt]['start'] . "')+43200 and o.date< unix_timestamp('" . $calculated_dates[$cnt]['end'] . "')+43200 
		    		 and o.status in ('C','SH','DL') group by  o.orderid having jitSum = 0 ) mv1 ", true);
			$resultFailed = func_query("select count(*) as numoforders from (select o.orderid as orders, sum(od.jit_purchase) as jitSum from xcart_orders o , xcart_order_details od where o.orderid=od.orderid and
				     o.date >= unix_timestamp('" . $calculated_dates[$cnt]['start'] . "')+43200 and o.date< unix_timestamp('" . $calculated_dates[$cnt]['end'] . "')+43200 
				 	 and o.status in ('F') group by  o.orderid having jitSum = 0) mv1", true);
			$resultShipped = func_query("select count(*) as numoforders from (select o.orderid as orders, sum(od.jit_purchase) as jitSum from xcart_orders o , xcart_order_details od where o.orderid=od.orderid and
				 	 o.shippeddate >= unix_timestamp('" . $calculated_dates[$cnt]['start'] . "')+43200 and o.shippeddate < unix_timestamp('" . $calculated_dates[$cnt]['end'] . "')+43200 group by  o.orderid having jitSum = 0) mv1", true);
			$resultPending = func_query("select count(*) as numoforders from (select o.orderid as orders, sum(od.jit_purchase) as jitSum from xcart_orders o , xcart_order_details od where o.orderid=od.orderid and
					o.date< unix_timestamp('" . $calculated_dates[$cnt]['end'] . "')+43200 and o.status in ('OH','WP','Q') group by  o.orderid having jitSum = 0) mv1", true);
			$result_oos = func_query("select count(*) as numoforders from (select o.orderid as orders, sum(od.jit_purchase) as jitSum from xcart_orders o , xcart_order_details od where o.orderid=od.orderid and
					 o.date >= unix_timestamp('" . $calculated_dates[$cnt]['start'] . "')+43200 and o.date< unix_timestamp('" . $calculated_dates[$cnt]['end'] . "')+43200 
					 and o.status in ('OH','C','WP','SH','Q','DL') and od.assignee in ('8','10','33','34') group by o.orderid having jitSum = 0) mv1", true);
			$resultShipped_samedate = func_query("select count(*) as numoforders from (select o.orderid as orders, sum(od.jit_purchase) as jitSum from xcart_orders o , xcart_order_details od where o.orderid=od.orderid and
					 o.date >= unix_timestamp('" . $calculated_dates[$cnt]['start'] . "')+43200 and o.date< unix_timestamp('" . $calculated_dates[$cnt]['end'] . "')+43200 
					 and o.status in ('OH','C','WP','SH','Q','DL')  and date(from_unixtime(o.shippeddate)) = date(from_unixtime(o.date)) group by o.orderid having jitSum = 0) mv1", true);
			$resultShipped_otherdate = func_query("select count(*) as numoforders from (select o.orderid as orders, sum(od.jit_purchase) as jitSum from xcart_orders o , xcart_order_details od where o.orderid=od.orderid and
					 o.date >= unix_timestamp('" . $calculated_dates[$cnt]['start'] . "')+43200 and o.date< unix_timestamp('" . $calculated_dates[$cnt]['end'] . "')+43200 
					 and o.status in ('OH','C','WP','SH','Q','DL') and (date(from_unixtime(o.shippeddate)) != date(from_unixtime(o.date)) or o.shippeddate is null) group by o.orderid having jitSum = 0) mv1", true);

			$ReportData['Total'][$cnt]=(float)$resultTotal[0]['numoforders'];
			$ReportData['Failed'][$cnt]=(float)$resultFailed[0]['numoforders'];
			$ReportData['Shipped'][$cnt]=(float)$resultShipped[0]['numoforders'];
			$ReportData['Pending'][$cnt]=(float)$resultPending[0]['numoforders'];
			$ReportData['AvgTime'][$cnt]=(float)$resultAvgTime[0]['avgTime'];
			$ReportData['OOS'][$cnt]=(float)$result_oos[0]['numoforders'];
			$ReportData['SameDateShipping'][$cnt]=(float)($resultShipped_samedate[0]['numoforders']*100)/$ReportData['Total'][$cnt] ;
			$ReportData['OtherDateShipping'][$cnt]=(float)($resultShipped_otherdate[0]['numoforders']*100)/$ReportData['Total'][$cnt];
			foreach($result as $row){
				$ReportData[$row['timeToShip']][$cnt]=(float)($row['numoforders']*100)/$ReportData['Total'][$cnt];
			}
			$headers[$cnt+1] = $calculated_dates[$cnt]['label'];
		}
		ksort($ReportData);

		$width = $cnt+1;
		$table->setHeaders($headers);
		$table->setTableWidth($width);
		$rowHeaderValues = array('Total', '<24', '<48', '<72', '>72', 'Unshipped','AvgTime', 'Failed', 'Shipped', 'Pending', 'OOS', 'SameDateShipping', 'OtherDateShipping');
		$i=0;
		for($cnt=0; $cnt<count($rowHeaderValues); $cnt++){
			for ($j=1; $j<$width; $j++){
				$table->setCellData($i, $j, $ReportData[$rowHeaderValues[$cnt]][$j-1]);
			}
			$i++;
		}

		//make other table
		$jitTable = $this->jitOverallSLA($calculated_dates);
		$group->addTable($table);
		$group->addTable($jitTable);

		$this->notes = "WTD = Stats from 00:00 Monday to 23:59 Yesterday <br>
						W3 = " . $calculated_dates[9]['start'] . " to " . $calculated_dates[9]['end'] ."<br> 
						W2 = " . $calculated_dates[10]['start'] . " to " . $calculated_dates[10]['end'] ."<br> 
						W1 = " .$calculated_dates[11]['start'] . " to " . $calculated_dates[11]['end'] . "<br> 
						W0 = " . $calculated_dates[12]['start'] . " to " . $calculated_dates[12]['end'] . "<br><br>";
		$this->isBuild=true;
	}

}

class OperationSLAReportModel extends ReportModel{
	public function __construct(){
		parent::__construct();
		$this->name = "Operations Report";
	}

	protected function initFilters(){
		$this->addFilter("Date:" ,self::$FILTER_TYPE_DATE);
	}

	public function loadDefaultFilterValues(){
		$time=time();
		$this->setFilterValue("Date:", date("m/d/Y",$time));
	}

	public function jitOperationsSLA($calculated_dates){
		$table= new Table('JIT - Operations SLA');
		$table->setFooterVisible(false);
		$j=0;
		$headers[0]= 'Facts';
		$table->setCellData(0, $j,"No. of Orders Queued" );
		$table->setCellData(1, $j,"% Orders Shipped in 24 hours");
		$table->setCellData(2, $j,"% Orders Shipped in 48 hours");
		$table->setCellData(3, $j,"% Orders Shipped in 72 hours");
		$table->setCellData(4, $j,"% Orders Shipped Beyond 72 hours");
		$table->setCellData(5, $j,"% Unshipped Orders (in process by Ops)");
		$table->setCellData(6, $j, "Avg. Order Fulfilment time (in hours)");
		$table->setCellData(7, $j, "# Orders with at least 1 item OOS");
		$table->setCellData(8, $j, "% Orders shipped on same date");
		$table->setCellData(9, $j, "% Orders not shipped on same date");
		$j++;
		$ReportData;
		// Eg: Report will be run on 15-Oct@7:00am. So start date for computations is 13-Oct@12 and end date is 14-Oct@12noon
		// Based of the cron time at 7:00am next day, the $daysoffset is set to 1.
		$daysoffset = 1 * 86400; // 86400 seconds in a single day
		
		for($cnt=0; $cnt<count($calculated_dates); $cnt++){
			
			$resultTotal=func_query("select count(distinct o.orderid) as numoforders from xcart_orders o, xcart_order_details od 
					 where o.orderid=od.orderid
					 and o.queueddate>=unix_timestamp('" . $calculated_dates[$cnt]['start'] . "')+43200-$daysoffset
					 and o.queueddate< unix_timestamp('" . $calculated_dates[$cnt]['end']   . "')+43200-$daysoffset 
					 and o.status in ('OH','C','WP','SH','Q','DL') and od.jit_purchase = 1", true);
				
			$result=func_query("select (case true when mv1.shippeddate is null then 'Unshipped' 
			
					 when (from_unixtime(mv1.shippeddate, '%Y-%m-%d')=from_unixtime(mv1.queueddate, '%Y-%m-%d')) then '<24'
					 
					 when (hour(from_unixtime(mv1.queueddate))>=12 and 
					 			from_unixtime(mv1.shippeddate, '%Y-%m-%d')=from_unixtime(mv1.queueddate+86400, '%Y-%m-%d')) then '<24'
					 			
					 when (hour(from_unixtime(mv1.queueddate))<12 and
					 			from_unixtime(mv1.shippeddate, '%Y-%m-%d')=from_unixtime(mv1.queueddate+86400, '%Y-%m-%d')) then '<48'
					 			
					 when (hour(from_unixtime(mv1.queueddate))>=12 and
					 			from_unixtime(mv1.shippeddate, '%Y-%m-%d')=from_unixtime(mv1.queueddate+86400*2, '%Y-%m-%d')) then '<48'
					 			
					 when (hour(from_unixtime(mv1.queueddate))<12 and
					 			from_unixtime(mv1.shippeddate, '%Y-%m-%d')=from_unixtime(mv1.queueddate+86400*2, '%Y-%m-%d')) then '<72'
					 			
					 when (hour(from_unixtime(mv1.queueddate))>=12 and
					 			from_unixtime(mv1.shippeddate, '%Y-%m-%d')=from_unixtime(mv1.queueddate+86400*3, '%Y-%m-%d')) then '<72'

					 else '>72' end) as timeToShip,
					 
					 count(mv1.orders) as numoforders from ( 
				 		select  o.orderid as orders, o.shippeddate, o.queueddate from xcart_orders o, xcart_order_details od 
				 			where o.orderid=od.orderid
				 			and o.queueddate>=unix_timestamp('" . $calculated_dates[$cnt]['start'] . "')+43200-$daysoffset
	    		 			and o.queueddate< unix_timestamp('" . $calculated_dates[$cnt]['end']   . "')+43200-$daysoffset
	    		 			and o.status in ('OH','C','WP','SH','Q','DL') and od.jit_purchase = 1 group by  o.orderid) as mv1 
	    				group by timeToShip order by timeToShip", true);
			
			$resultAvgTime = func_query("select avg(mv1.shippeddate - ifnull(mv1.queueddate, mv1.shippeddate))/ 3600 as avgTime from
					(select o.orderid as orders, o.shippeddate, o.queueddate from xcart_orders o, xcart_order_details od where o.orderid=od.orderid
					 and o.queueddate>=unix_timestamp('" . $calculated_dates[$cnt]['start'] . "')+43200-$daysoffset
		    		 and o.queueddate< unix_timestamp('" . $calculated_dates[$cnt]['end']   . "')+43200-$daysoffset
		    		 and o.status in ('C','SH','DL') and  od.jit_purchase = 1 group by  o.orderid ) mv1 ", true);
			
			$result_oos = func_query("select count(distinct o.orderid) as numoforders from xcart_orders o join xcart_order_details od 
					 on o.orderid=od.orderid where 
					     o.queueddate>=unix_timestamp('" . $calculated_dates[$cnt]['start'] . "')+43200-$daysoffset
					 and o.queueddate< unix_timestamp('" . $calculated_dates[$cnt]['end']   . "')+43200-$daysoffset 
					 and o.status in ('OH','C','WP','SH','Q','DL') and od.assignee in ('8','10','33','34') and od.jit_purchase = 1", true);
			
			$resultShipped_samedate = func_query("select count(distinct o.orderid) as numoforders from xcart_orders o, xcart_order_details od
					 where o.orderid=od.orderid
					 and o.queueddate>=unix_timestamp('" . $calculated_dates[$cnt]['start'] . "')+43200-$daysoffset 
					 and o.queueddate< unix_timestamp('" . $calculated_dates[$cnt]['end']   . "')+43200-$daysoffset
					 and o.status in ('OH','C','WP','SH','Q','DL') 
					 and date(from_unixtime(o.shippeddate)) = date(from_unixtime(o.date)) and od.jit_purchase = 1", true);
			
			$resultShipped_otherdate = func_query("select count(distinct o.orderid) as numoforders from xcart_orders o, xcart_order_details od 
					 where o.orderid=od.orderid
					 and o.queueddate >= unix_timestamp('" . $calculated_dates[$cnt]['start'] . "')+43200-$daysoffset 
					 and o.queueddate< unix_timestamp('" . $calculated_dates[$cnt]['end'] . "')+43200-$daysoffset 
					 and o.status in ('OH','C','WP','SH','Q','DL') 
					 and (date(from_unixtime(o.shippeddate)) != date(from_unixtime(o.date)) or o.shippeddate is null) and od.jit_purchase = 1", true);

			$ReportData['Total'][$cnt]=(float)$resultTotal[0]['numoforders'];
			$ReportData['AvgTime'][$cnt]=(float)$resultAvgTime[0]['avgTime'];
			$ReportData['OOS'][$cnt]=(float)$result_oos[0]['numoforders'];
			$ReportData['SameDateShipping'][$cnt]=(float)($resultShipped_samedate[0]['numoforders']*100)/$ReportData['Total'][$cnt] ;
			$ReportData['OtherDateShipping'][$cnt]=(float)($resultShipped_otherdate[0]['numoforders']*100)/$ReportData['Total'][$cnt];
			
			foreach($result as $row){
				$ReportData[$row['timeToShip']][$cnt]=(float)($row['numoforders']*100)/$ReportData['Total'][$cnt];
			}
			$headers[$cnt+1] = $calculated_dates[$cnt]['label'];
		}
		ksort($ReportData);

		$width = $cnt+1;
		$table->setHeaders($headers);
		$table->setTableWidth($width);
		$rowHeaderValues = array('Total', '<24', '<48', '<72', '>72', 'Unshipped','AvgTime', 'OOS', 'SameDateShipping', 'OtherDateShipping');
		$i=0;
		for($cnt=0; $cnt<count($rowHeaderValues); $cnt++){
			for ($j=1; $j<$width; $j++){
				$table->setCellData($i, $j, $ReportData[$rowHeaderValues[$cnt]][$j-1]);
			}
			$i++;
		}

		return $table;
	}

	public function buildReport(){
		if($this->isBuild)
		return;

		$filtervalue = $this->getFilterValue("Date:");
		$date_split=split('/', $filtervalue);
		$current_month=(INT)$date_split[0];
		$current_year=(INT)$date_split[2];
		$current_date=(INT)$date_split[1];
		$calculated_dates = calculateDates($current_date, $current_month, $current_year, true);
		$group= new TableGroup("Operations - SLA");
		$this->addTableGroup($group);
		$table= new Table();
		$table->setFooterVisible(false);

		$j=0;
		$headers[0]= 'Facts';
		$table->setCellData(0, $j,"No. of Orders Queued" );
		$table->setCellData(1, $j,"% Orders Shipped in 24 hours");
		$table->setCellData(2, $j,"% Orders Shipped in 48 hours");
		$table->setCellData(3, $j,"% Orders Shipped in 72 hours");
		$table->setCellData(4, $j,"% Orders Shipped Beyond 72 hours");
		$table->setCellData(5, $j,"% Unshipped Orders (in process by Ops)");
		$table->setCellData(6, $j, "Avg. Order Fulfilment time (in hours)");
		$table->setCellData(7, $j, "# Orders with at least 1 item OOS");
		$table->setCellData(8, $j, "% Orders shipped on same date");
		$table->setCellData(9, $j, "% Orders not shipped on same date");
		$j++;
		$ReportData;
		
		// Eg: Report will be run on 15-Oct@7:00am. So start date for computations is 13-Oct@12 and end date is 14-Oct@12noon
		// Based of the cron time at 7:00am next day, the $daysoffset is set to 1.
		$daysoffset = 1 * 86400; // 86400 seconds in a single day
		
		for($cnt=0; $cnt<count($calculated_dates); $cnt++) {
			
			$resultTotal=func_query("select count(*) as numoforders from (select o.orderid as orders, sum(od.jit_purchase) as jitSum 
					 from xcart_orders o , xcart_order_details od where o.orderid=od.orderid 
					 and o.queueddate>=unix_timestamp('" . $calculated_dates[$cnt]['start'] . "')+43200-$daysoffset
					 and o.queueddate< unix_timestamp('" . $calculated_dates[$cnt]['end']   . "')+43200-$daysoffset 
					 and status in ('OH','C','WP','SH','Q','DL') group by  o.orderid having jitSum = 0) mv1", true);
			
			$result=func_query("select (case true when mv1.shippeddate is null then 'Unshipped'

					 when (from_unixtime(mv1.shippeddate, '%Y-%m-%d')=from_unixtime(mv1.queueddate, '%Y-%m-%d')) then '<24'
					 
					 when (hour(from_unixtime(mv1.queueddate))>=12 and 
					 			from_unixtime(mv1.shippeddate, '%Y-%m-%d')=from_unixtime(mv1.queueddate+86400, '%Y-%m-%d')) then '<24'
					 			
					 when (hour(from_unixtime(mv1.queueddate))<12 and
					 			from_unixtime(mv1.shippeddate, '%Y-%m-%d')=from_unixtime(mv1.queueddate+86400, '%Y-%m-%d')) then '<48'
					 			
					 when (hour(from_unixtime(mv1.queueddate))>=12 and
					 			from_unixtime(mv1.shippeddate, '%Y-%m-%d')=from_unixtime(mv1.queueddate+86400*2, '%Y-%m-%d')) then '<48'
					 			
					 when (hour(from_unixtime(mv1.queueddate))<12 and
					 			from_unixtime(mv1.shippeddate, '%Y-%m-%d')=from_unixtime(mv1.queueddate+86400*2, '%Y-%m-%d')) then '<72'
					 			
					 when (hour(from_unixtime(mv1.queueddate))>=12 and
					 			from_unixtime(mv1.shippeddate, '%Y-%m-%d')=from_unixtime(mv1.queueddate+86400*3, '%Y-%m-%d')) then '<72'
			
					 else '>72' end) as timeToShip, count(mv1.orders) as numoforders from ( 
					 	select  o.orderid as orders, o.shippeddate, o.queueddate, sum(od.jit_purchase) as jitSum from 
						 	xcart_orders o, xcart_order_details od where o.orderid=od.orderid
						 	and o.queueddate>=unix_timestamp('" . $calculated_dates[$cnt]['start'] . "')+43200-$daysoffset
			    		 	and o.queueddate< unix_timestamp('" . $calculated_dates[$cnt]['end']   . "')+43200-$daysoffset 
			    		 	and o.status in ('OH','C','WP','SH','Q','DL') group by o.orderid having jitSum = 0) as mv1 
		    		 group by timeToShip order by timeToShip", true);
			
			$resultAvgTime = func_query("select avg(mv1.shippeddate - ifnull(mv1.queueddate, mv1.shippeddate))/ 3600 as avgTime from 
					 (select o.orderid as orders, o.shippeddate, o.queueddate, sum(od.jit_purchase) as jitSum from xcart_orders o, xcart_order_details od 
					 	where o.orderid=od.orderid
					 	and o.queueddate>=unix_timestamp('" . $calculated_dates[$cnt]['start'] . "')+43200-$daysoffset
		    		 	and o.queueddate< unix_timestamp('" . $calculated_dates[$cnt]['end']   . "')+43200-$daysoffset 
		    		 	and o.status in ('C','SH','DL') group by  o.orderid having jitSum = 0 ) mv1", true);
			
			$result_oos = func_query("select count(*) as numoforders from (select o.orderid as orders, sum(od.jit_purchase) as jitSum 
					 from xcart_orders o , xcart_order_details od where o.orderid=od.orderid
					 	and o.queueddate>=unix_timestamp('" . $calculated_dates[$cnt]['start'] . "')+43200-$daysoffset
					 	and o.queueddate< unix_timestamp('" . $calculated_dates[$cnt]['end']   . "')+43200-$daysoffset
					 	and status in ('OH','C','WP','SH','Q','DL') and od.assignee in ('8','10','33','34') 
					 group by o.orderid having jitSum = 0) mv1", true);
			
			$resultShipped_samedate = func_query("select count(*) as numoforders from (select o.orderid as orders, sum(od.jit_purchase) as jitSum 
					from xcart_orders o , xcart_order_details od where o.orderid=od.orderid
						and o.queueddate>=unix_timestamp('" . $calculated_dates[$cnt]['start'] . "')+43200-$daysoffset
						and o.queueddate< unix_timestamp('" . $calculated_dates[$cnt]['end']   . "')+43200-$daysoffset
						and status in ('OH','C','WP','SH','Q','DL') 
						and date(from_unixtime(o.shippeddate)) = date(from_unixtime(o.queueddate)) 
					group by o.orderid having jitSum = 0) mv1", true);
			
			$resultShipped_otherdate = func_query("select count(*) as numoforders from (select o.orderid as orders, sum(od.jit_purchase) as jitSum 
					from xcart_orders o , xcart_order_details od where o.orderid=od.orderid
						and o.queueddate>=unix_timestamp('" . $calculated_dates[$cnt]['start'] . "')+43200-$daysoffset
						and o.queueddate< unix_timestamp('" . $calculated_dates[$cnt]['end']   . "')+43200-$daysoffset
						and status in ('OH','C','WP','SH','Q','DL') 
						and (date(from_unixtime(o.shippeddate)) != date(from_unixtime(o.queueddate)) or o.shippeddate is null) 
					group by o.orderid having jitSum = 0) mv1", true);

			$ReportData['Total'][$cnt]=(float)$resultTotal[0]['numoforders'];
			$ReportData['AvgTime'][$cnt]=(float)$resultAvgTime[0]['avgTime'];
			$ReportData['OOS'][$cnt]=(float)$result_oos[0]['numoforders'];
			$ReportData['SameDateShipping'][$cnt]=(float)($resultShipped_samedate[0]['numoforders']*100)/$ReportData['Total'][$cnt] ;
			$ReportData['OtherDateShipping'][$cnt]=(float)($resultShipped_otherdate[0]['numoforders']*100)/$ReportData['Total'][$cnt];
			foreach($result as $row){
				$ReportData[$row['timeToShip']][$cnt]=(float)($row['numoforders']*100)/$ReportData['Total'][$cnt];
			}
			$headers[$cnt+1] = $calculated_dates[$cnt]['label'];
		}
		ksort($ReportData);

		$width = $cnt+1;
		$table->setHeaders($headers);
		$table->setTableWidth($width);
		$rowHeaderValues = array('Total', '<24', '<48', '<72', '>72', 'Unshipped','AvgTime', 'OOS', 'SameDateShipping', 'OtherDateShipping');
		$i=0;
		for($cnt=0; $cnt<count($rowHeaderValues); $cnt++){
			for ($j=1; $j<$width; $j++){
				$table->setCellData($i, $j, $ReportData[$rowHeaderValues[$cnt]][$j-1]);
			}
			$i++;
		}
		
		//make other table
		$jitTable = $this->jitOperationsSLA($calculated_dates);
		$group->addTable($table);
		$group->addTable($jitTable);

		$this->notes = "Yesterday = Stats from 12:00 yesterday to 11:59 Today <br>
						WTD = Stats from 12:00 Monday to 11:59 Today <br>
						W3 = " . preg_replace('/00:00:00/', '12:00:00', $calculated_dates[9]['start']) . " to " . preg_replace('/00:00:00/', '11:59:00', $calculated_dates[9]['end']) ."<br> 
						W2 = " . preg_replace('/00:00:00/', '12:00:00', $calculated_dates[10]['start']) . " to " . preg_replace('/00:00:00/', '11:59:00', $calculated_dates[10]['end']) ."<br> 
						W1 = " . preg_replace('/00:00:00/', '12:00:00', $calculated_dates[11]['start']) . " to " . preg_replace('/00:00:00/', '11:59:00', $calculated_dates[11]['end']) . "<br> 
						W0 = " . preg_replace('/00:00:00/', '12:00:00', $calculated_dates[12]['start']) . " to " . preg_replace('/00:00:00/', '11:59:00', $calculated_dates[12]['end']) . "<br><br>";
		$this->isBuild=true;
	}
}

class ReturnsSLAReportModel extends ReportModel{
	public function __construct(){
		parent::__construct();
		$this->name = "Returns Report";
	}

	protected function initFilters(){
		$this->addFilter("Date:" ,self::$FILTER_TYPE_DATE);
	}

	public function loadDefaultFilterValues(){
		$time=time();
		$this->setFilterValue("Date:", date("m/d/Y",$time));
	}

	/*public function jitReturnsSLA($calculated_dates){
		$table= new Table('JIT - Returns SLA');
		$table->setFooterVisible(false);
		$j=0;
		$headers[0]= 'Facts';
		$table->setCellData(0, $j,"No. of Returns received at WH" );
		$table->setCellData(1, $j,"% Returns processed in 3 hours");
		$table->setCellData(2, $j,"% Returns processed in 6 hours");
		$table->setCellData(3, $j,"% Returns processed in 9 hours");
		$table->setCellData(4, $j,"% Returns processed Beyond 9 hours");
		$table->setCellData(5, $j,"% Unprocessed Returns");
		$j++;
		$ReportData;
		
		for($cnt=0; $cnt<7; $cnt++){
			$resultTotal=func_query("select count(*) as numofreturns from (select r.returnid, r.from_status, r.to_status, r.audit_time 
					from mk_return_transit_details r where r.audit_time>=unix_timestamp('" . $calculated_dates[$cnt]['start'] . "')+43200
					and r.audit_time<unix_timestamp('" . $calculated_dates[$cnt]['end'] . "')+43200 and r.to_status='RRC') rt", true);
			$result=func_query("select (case true when mv.protime is null then 'Unprocessed'
					when (((mv.protime - mv.received_time)/ 3600) between 0 and 3) then '<3'
					when (((mv.protime - mv.received_time)/ 3600) between 3 and 6) then '<6'
					when (((mv.protime - mv.received_time)/ 3600) between 6 and 9) then '<9'
					else '>9' end) as timeToProcess, count(mv.returnid) as numofreturns from (
					select returnid, max(case when to_status = 'RRC' then audit_time else null end) as 'received_time', 
					max(case when from_status = 'RRC' and to_status in ('RQP','RQF', '') then audit_time else null end) as 'protime' 
					from mk_return_transit_details r where r.audit_time>=unix_timestamp('" . $calculated_dates[$cnt]['start'] . "')+43200 and 
					r.audit_time<unix_timestamp('" . $calculated_dates[$cnt]['end'] . "')+43200 group by returnid having 
					received_time is not null) mv order by timeToProcess", true);
			
			$ReportData['Total'][$cnt]=(float)$resultTotal[0]['numofreturns'];
			foreach($result as $row){
				$ReportData[$row['timeToProcess']][$cnt]=(float)($row['numofreturns']*100)/$ReportData['Total'][$cnt];
			}
			$headers[$cnt+1] = $calculated_dates[$cnt]['label'];
		}
		ksort($ReportData);

		$width = $cnt+1;
		$table->setHeaders($headers);
		$table->setTableWidth($width);
		$rowHeaderValues = array('Total', '<3', '<6', '<9', '>9', 'Unprocessed');
		$i=0;
		for($cnt=0; $cnt<count($rowHeaderValues); $cnt++){
			for ($j=1; $j<$width; $j++){
				$table->setCellData($i, $j, $ReportData[$rowHeaderValues[$cnt]][$j-1]);
			}
			$i++;
		}
		
		return $table;
	}*/

	public function buildReport(){
		if($this->isBuild)
			return;

		$filtervalue = $this->getFilterValue("Date:");
		$date_split=split('/', $filtervalue);
		$current_month=(INT)$date_split[0];
		$current_year=(INT)$date_split[2];
		$current_date=(INT)$date_split[1];
		$calculated_dates = calculateDates($current_date, $current_month, $current_year);
		$group= new TableGroup("Returns - SLA");
		$this->addTableGroup($group);
		$table= new Table();
		$table->setFooterVisible(false);

		$j=0;
		$headers[0]= 'Facts';
		$table->setCellData(0, $j,"No. of Returns received at WH" );
		$table->setCellData(1, $j,"% Returns processed in 3 hours");
		$table->setCellData(2, $j,"% Returns processed in 6 hours");
		$table->setCellData(3, $j,"% Returns processed in 9 hours");
		$table->setCellData(4, $j,"% Returns processed Beyond 9 hours");
		$table->setCellData(5, $j,"% Unprocessed Returns");
		$j++;
		$ReportData;
		for($cnt=0; $cnt<7; $cnt++){
			$resultTotal=func_query("select count(*) as numofreturns from (select r.returnid, r.from_status, r.to_status, r.audit_time 
					from mk_return_transit_details r where r.audit_time>=unix_timestamp('" . $calculated_dates[$cnt]['start'] . "')+43200
					and r.audit_time<unix_timestamp('" . $calculated_dates[$cnt]['end'] . "')+43200 and r.to_status='RRC') rt", true);
			$result=func_query("select (case true when mv.protime is null then 'Unprocessed'
					when (((mv.protime - mv.received_time)/ 3600) between 0 and 3) then '<3'
					when (((mv.protime - mv.received_time)/ 3600) between 3 and 6) then '<6'
					when (((mv.protime - mv.received_time)/ 3600) between 6 and 9) then '<9'
					else '>9' end) as timeToProcess, count(mv.returnid) as numofreturns from (
					select returnid, max(case when to_status = 'RRC' then audit_time else null end) as 'received_time', 
					max(case when from_status = 'RRC' and to_status in ('RQP','RQF', '') then audit_time else null end) as 'protime' 
					from mk_return_transit_details r where r.audit_time>=unix_timestamp('" . $calculated_dates[$cnt]['start'] . "')+43200 and 
					r.audit_time<unix_timestamp('" . $calculated_dates[$cnt]['end'] . "')+43200 group by returnid having 
					received_time is not null) mv order by timeToProcess", true);
			
			$ReportData['Total'][$cnt]=(float)$resultTotal[0]['numofreturns'];
			foreach($result as $row){
				$ReportData[$row['timeToProcess']][$cnt]=(float)($row['numofreturns']*100)/$ReportData['Total'][$cnt];
			}
			$headers[$cnt+1] = $calculated_dates[$cnt]['label'];
		}
		ksort($ReportData);

		$width = $cnt+1;
		$table->setHeaders($headers);
		$table->setTableWidth($width);
		$rowHeaderValues = array('Total', '<3', '<6', '<9', '>9', 'Unprocessed');
		$i=0;
		for($cnt=0; $cnt<count($rowHeaderValues); $cnt++){
			for ($j=1; $j<$width; $j++){
				$table->setCellData($i, $j, $ReportData[$rowHeaderValues[$cnt]][$j-1]);
			}
			$i++;
		}

		//make other table
		//$jitTable = $this->jitReturnsSLA($calculated_dates);
		$group->addTable($table);
		//$group->addTable($jitTable);

		$this->notes = "Yesterday = Stats from 12:00 yesterday to 11:59 Today <br>
		WTD = Stats from 12:00 Monday to 11:59 Today <br>
		W3 = " . preg_replace('/00:00:00/', '12:00:00', $calculated_dates[9]['start']) . " to " . preg_replace('/00:00:00/', '11:59:00', $calculated_dates[9]['end']) ."<br>
		W2 = " . preg_replace('/00:00:00/', '12:00:00', $calculated_dates[10]['start']) . " to " . preg_replace('/00:00:00/', '11:59:00', $calculated_dates[10]['end']) ."<br>
		W1 = " . preg_replace('/00:00:00/', '12:00:00', $calculated_dates[11]['start']) . " to " . preg_replace('/00:00:00/', '11:59:00', $calculated_dates[11]['end']) . "<br>
		W0 = " . preg_replace('/00:00:00/', '12:00:00', $calculated_dates[12]['start']) . " to " . preg_replace('/00:00:00/', '11:59:00', $calculated_dates[12]['end']) . "<br><br>";
		$this->isBuild=true;
	}
}

class RTOSLAReportModel extends ReportModel{
	public function __construct(){
		parent::__construct();
		$this->name = "RTO Report";
	}

	protected function initFilters(){
		$this->addFilter("Date:" ,self::$FILTER_TYPE_DATE);
	}

	public function loadDefaultFilterValues(){
		$time=time();
		$this->setFilterValue("Date:", date("m/d/Y",$time));
	}

	/*public function jitRTOsSLA($calculated_dates){
		$table= new Table('JIT - RTO SLA');
		$table->setFooterVisible(false);
		$j=0;
		$headers[0]= 'Facts';
		$table->setCellData(0, $j,"No. of RTO's received at WH" );
		$table->setCellData(1, $j,"% RTO's processed in 3 hours");
		$table->setCellData(2, $j,"% RTO's processed in 6 hours");
		$table->setCellData(3, $j,"% RTO's processed in 9 hours");
		$table->setCellData(4, $j,"% RTO's processed Beyond 9 hours");
		$table->setCellData(5, $j,"% Unprocessed RTO's");
		$j++;
		$ReportData;
		for($cnt=0; $cnt<7; $cnt++){
			$resultTotal=func_query("select count(*) as numofrtos from (select r.returnid, r.activitycode, r.recorddate from 
					mk_old_returns_tracking_activity r where activitycode = 'RTOQ' and r.recorddate>=('".preg_replace('/00:00:00/', '12:00:00', $calculated_dates[$cnt]['start'])."') 
					and r.recorddate<('" . preg_replace('/00:00:00/', '11:59:00', $calculated_dates[$cnt]['end']) . "')) rto", true);
			$result=func_query("select (case true when mv.protime is null then 'Unprocessed'
					when (((mv.protime - mv.received_time)/ 3600) between 0 and 3) then '<3'
					when (((mv.protime - mv.received_time)/ 3600) between 3 and 6) then '<6'
					when (((mv.protime - mv.received_time)/ 3600) between 6 and 9) then '<9'
					else '>9' end) as timeToProcess, count(mv.returnid) as numofrtos from (
					select returnid, max(case when activitycode = 'RTOQ' then recorddate else null end) as 'received_time', 
					max(case when activitycode in ('RTORF','RTORSC', 'RTORS') then recorddate else null end) as 'protime' from 
					mk_old_returns_tracking_activity r where r.recorddate>=('".preg_replace('/00:00:00/', '12:00:00', $calculated_dates[$cnt]['start'])."') and 
					r.recorddate<('" . preg_replace('/00:00:00/', '11:59:00', $calculated_dates[$cnt]['end']) . "') group by returnid having received_time is not null) 
					mv order by timeToProcess", true);
				
			$ReportData['Total'][$cnt]=(float)$resultTotal[0]['numofrtos'];
			foreach($result as $row){
				$ReportData[$row['timeToProcess']][$cnt]=(float)($row['numofrtos']*100)/$ReportData['Total'][$cnt];
			}
			$headers[$cnt+1] = $calculated_dates[$cnt]['label'];
		}
		ksort($ReportData);
		$width = $cnt+1;
		$table->setHeaders($headers);
		$table->setTableWidth($width);
		$rowHeaderValues = array('Total', '<3', '<6', '<9', '>9', 'Unprocessed');
		$i=0;
		for($cnt=0; $cnt<count($rowHeaderValues); $cnt++){
			for ($j=1; $j<$width; $j++){
				$table->setCellData($i, $j, $ReportData[$rowHeaderValues[$cnt]][$j-1]);
			}
			$i++;
		}

		return $table;
	}*/

	public function buildReport(){
		if($this->isBuild)
			return;

		$filtervalue = $this->getFilterValue("Date:");
		$date_split=split('/', $filtervalue);
		$current_month=(INT)$date_split[0];
		$current_year=(INT)$date_split[2];
		$current_date=(INT)$date_split[1];
		$calculated_dates = calculateDates($current_date, $current_month, $current_year);
		$group= new TableGroup("RTO - SLA");
		$this->addTableGroup($group);
		$table= new Table();
		$table->setFooterVisible(false);

		$j=0;
		$headers[0]= 'Facts';
		$table->setCellData(0, $j,"No. of RTO's received at WH" );
		$table->setCellData(1, $j,"% RTO's processed in 3 hours");
		$table->setCellData(2, $j,"% RTO's processed in 6 hours");
		$table->setCellData(3, $j,"% RTO's processed in 9 hours");
		$table->setCellData(4, $j,"% RTO's processed Beyond 9 hours");
		$table->setCellData(5, $j,"% Unprocessed RTO's");
		$j++;
		$ReportData;
		for($cnt=0; $cnt<7; $cnt++){
			$resultTotal=func_query("select count(*) as numofrtos from (select r.returnid, r.activitycode, r.recorddate from 
					mk_old_returns_tracking_activity r where activitycode = 'RTOQ' and r.recorddate>=('".preg_replace('/00:00:00/', '12:00:00', $calculated_dates[$cnt]['start'])."') 
					and r.recorddate<('" . preg_replace('/00:00:00/', '11:59:00', $calculated_dates[$cnt]['end']) . "')) rto", true);
			$result=func_query("select (case true when mv.protime is null then 'Unprocessed'
					when (((mv.protime - mv.received_time)/ 3600) between 0 and 3) then '<3'
					when (((mv.protime - mv.received_time)/ 3600) between 3 and 6) then '<6'
					when (((mv.protime - mv.received_time)/ 3600) between 6 and 9) then '<9'
					else '>9' end) as timeToProcess, count(mv.returnid) as numofrtos from (
					select returnid, max(case when activitycode = 'RTOQ' then recorddate else null end) as 'received_time', 
					max(case when activitycode in ('RTORF','RTORSC', 'RTORS') then recorddate else null end) as 'protime' from 
					mk_old_returns_tracking_activity r where r.recorddate>=('".preg_replace('/00:00:00/', '12:00:00', $calculated_dates[$cnt]['start'])."') and 
					r.recorddate<('" . preg_replace('/00:00:00/', '11:59:00', $calculated_dates[$cnt]['end']) . "') group by returnid having received_time is not null) 
					mv order by timeToProcess", true);
				
			$ReportData['Total'][$cnt]=(float)$resultTotal[0]['numofrtos'];
			foreach($result as $row){
				$ReportData[$row['timeToProcess']][$cnt]=(float)($row['numofrtos']*100)/$ReportData['Total'][$cnt];
			}
			$headers[$cnt+1] = $calculated_dates[$cnt]['label'];
		}
		ksort($ReportData);

		$width = $cnt+1;
		$table->setHeaders($headers);
		$table->setTableWidth($width);
		$rowHeaderValues = array('Total', '<3', '<6', '<9', '>9', 'Unprocessed');
		$i=0;
		for($cnt=0; $cnt<count($rowHeaderValues); $cnt++){
			for ($j=1; $j<$width; $j++){
				$table->setCellData($i, $j, $ReportData[$rowHeaderValues[$cnt]][$j-1]);
			}
			$i++;
		}

		//make other table
		//$jitTable = $this->jitRTOsSLA($calculated_dates);
		$group->addTable($table);
		//$group->addTable($jitTable);

		$this->notes = "Yesterday = Stats from 12:00 yesterday to 11:59 Today <br>
		WTD = Stats from 12:00 Monday to 11:59 Today <br>
		W3 = " . preg_replace('/00:00:00/', '12:00:00', $calculated_dates[9]['start']) . " to " . preg_replace('/00:00:00/', '11:59:00', $calculated_dates[9]['end']) ."<br>
		W2 = " . preg_replace('/00:00:00/', '12:00:00', $calculated_dates[10]['start']) . " to " . preg_replace('/00:00:00/', '11:59:00', $calculated_dates[10]['end']) ."<br>
		W1 = " . preg_replace('/00:00:00/', '12:00:00', $calculated_dates[11]['start']) . " to " . preg_replace('/00:00:00/', '11:59:00', $calculated_dates[11]['end']) . "<br>
		W0 = " . preg_replace('/00:00:00/', '12:00:00', $calculated_dates[12]['start']) . " to " . preg_replace('/00:00:00/', '11:59:00', $calculated_dates[12]['end']) . "<br><br>";
		$this->isBuild=true;
	}
}

?>