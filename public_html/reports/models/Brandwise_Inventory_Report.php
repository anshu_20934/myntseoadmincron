<?php

global $xcart_dir;
include_once $xcart_dir.'/reports/models/models.php';
include_once $xcart_dir.'/reports/rendrers/rendrers.php';
include_once REP_DIR.'/models/date_calculation.php';

class BrandwiseInventoryReportModel extends ReportModel {

	public function __construct(){
		parent::__construct();
		$this->name="BrandWise Inventory Report : " . date("j-M-Y");

	}
	protected function initFilters(){
		$this->addFilter("Date:" ,self::$FILTER_TYPE_DATE);
	}

	public function loadDefaultFilterValues(){
		$time=time();
		$this->setFilterValue("Date:", date("m/d/Y",$time));
	}

	public function categoryWiseInfo($calculated_dates1,$calculated_dates2){
		$table= new Table('Categorywise Breakup - MTD');
		$superHeaders= array('Categories'=>1,'Inwards'=>3,'Outwards'=>3,'Closing'=>3,'Last 7 days sales'=>3,'Last 30 days sales'=>3,'Stock Cover'=>2);
		$headers= array('','# Items', '# Articles', 'Value (Cost)', '# Items', '# Articles', 'Value (Cost)', '# Items', '# Articles', 'Value (Cost)','# Items', '# Articles', 'Value (Cost)','# Items', '# Articles', 'Value (Cost)','Based on last 7 days','Based on last 30 days');
		$table->setSuperHeaders($superHeaders);
		$table->setHeaders($headers);
		$table->setFooterVisible(false);		
		$ctgryReportData;

		$sql="select (select typename from mk_catalogue_classification where id=sp.global_attr_sub_category) as category, sum(taba.items) as InwardedItems, count(distinct sp.style_id) as InwardedArticles, sum(taba.items*taba.cp) as value from
			(select ci.sku_id, count(*) as items, avg(posku.price) as cp from myntra_auditor.audit_trail atrail  join myntra.core_items ci join myntra.core_po_skus posku where ci.id=atrail.entityId and ci.po_sku_id = posku.id 
			                and entityName = 'com.myntra.framework.entities.Item' and entityProperty='itemStatus'
							and entityPropertyNewValue = 'STORED' and actionTime >= ('" . $calculated_dates1[13]['start'] . "') and actionTime < ('" . $calculated_dates1[13]['end'] . "') group by ci.sku_id
			) taba left join myntra.mk_styles_options_skus_mapping osm on osm.sku_id=taba.sku_id left join mk_style_properties sp on osm.style_id=sp.style_id 
			group by sp.global_attr_sub_category";
		$result_inwards=func_query($sql, true);
		foreach($result_inwards as $row){
			if($row['category'] == null){
				$row['category'] = 'Others';
			}
			$ctgryReportData[$row['category']][0] += $row['InwardedItems'];
			$ctgryReportData[$row['category']][1] += $row['InwardedArticles'];
			$ctgryReportData[$row['category']][2] += (float)$row['value'];
		}

		$sql="select (select typename from mk_catalogue_classification where id=sp.global_attr_sub_category) as category, sum(taba.items) as outwardItems, count(distinct sp.style_id) as outwardArticles, sum(taba.items*taba.cp) as value from
			(select ci.sku_id, count(*) as items, avg(posku.price) as cp from myntra_auditor.audit_trail atrail  join myntra.core_items ci join myntra.core_po_skus posku where ci.id=atrail.entityId and ci.po_sku_id = posku.id 
			                and entityName = 'com.myntra.framework.entities.Item' and entityProperty='itemStatus'
							and entityPropertyOldValue = 'STORED' and actionTime >= ('" . $calculated_dates1[13]['start'] . "') and actionTime < ('" . $calculated_dates1[13]['end'] . "') group by ci.sku_id
			) taba left join myntra.mk_styles_options_skus_mapping osm on osm.sku_id=taba.sku_id left join mk_style_properties sp on osm.style_id=sp.style_id 
			group by sp.global_attr_sub_category";
		$result_outwards=func_query($sql, true);
		foreach($result_outwards as $row){
			if($row['category'] == null){
				$row['category'] = 'Others';
			}
			$ctgryReportData[$row['category']][3] += $row['outwardItems'];
			$ctgryReportData[$row['category']][4] += $row['outwardArticles'];
			$ctgryReportData[$row['category']][5] += (float)$row['value'];
		}
		$sql="select (select typename from mk_catalogue_classification where id=sp.global_attr_sub_category) as category, sum(taba.items) as ClosingItems, count(distinct sp.style_id) as ClosingArticles, sum(taba.items*taba.cp) as value from
			(select ci.sku_id, count(*) as items, avg(posku.price) as cp from core_items ci join myntra.core_po_skus posku where  ci.po_sku_id = posku.id and ci.item_status in 
			('STORED', 'FOUND', 'CUSTOMER_RETURNED','RETURN_FROM_OPS','ISSUED', 'PROCESSING') group by ci.sku_id
			) taba left join myntra.mk_styles_options_skus_mapping osm on osm.sku_id=taba.sku_id left join mk_style_properties sp on osm.style_id=sp.style_id 
			group by sp.global_attr_sub_category";
		$result_closing=func_query($sql, true);
		foreach($result_closing as $row){
			if($row['category'] == null){
				$row['category'] = 'Others';
			}
			$ctgryReportData[$row['category']][6] += $row['ClosingItems'];
			$ctgryReportData[$row['category']][7] += $row['ClosingArticles'];
			$ctgryReportData[$row['category']][8] += (float)$row['value'];
		}

		$sql="select  (select typename from mk_catalogue_classification where id=taba.category) as category, count(*) as qtyStyles, sum(taba.items) as qtyItems, sum(taba.value) as totalValue from
			(select sp.global_attr_sub_category as category, od.product_style, sum(od.amount ) as items, sum(((od.amount*od.price)/o.subtotal)*(o.total+o.shipping_cost+o.gift_charges)) as value 
            from xcart_orders o join xcart_order_details od on od.orderid=o.orderid and
			o.date between unix_timestamp('" . $calculated_dates2[1]['start'] . "') and unix_timestamp('" . $calculated_dates2[1]['end'] . "') and o.status in ('WP','Q','OH','SH','C','DL') join 
			mk_style_properties sp on sp.style_id=od.product_style group by od.product_style) taba group by taba.category";
		$result_sales7days=func_query($sql, true);
		foreach($result_sales7days as $row){
			if($row['category'] == null){
				$row['category'] = 'Others';
			}
			$ctgryReportData[$row['category']][9] += $row['qtyItems'];
			$ctgryReportData[$row['category']][10] += $row['qtyStyles'];
			$ctgryReportData[$row['category']][11] += (float)$row['totalValue'];
			$ctgryReportData[$row['category']][15] += (float)($ctgryReportData[$row['category']][6]/($row['qtyItems']/7)); // Closing Items/outwards per day
		}

		$sql="select  (select typename from mk_catalogue_classification where id=taba.category) as category, count(*) as qtyStyles, sum(taba.items) as qtyItems, sum(taba.value) as totalValue from
			(select sp.global_attr_sub_category as category, od.product_style, sum(od.amount ) as items, sum(((od.amount*od.price)/o.subtotal)*(o.total+o.shipping_cost+o.gift_charges)) as value 
            from xcart_orders o join xcart_order_details od on od.orderid=o.orderid and
			o.date between unix_timestamp('" . $calculated_dates2[2]['start'] . "') and unix_timestamp('" . $calculated_dates2[2]['end'] . "') and o.status in ('WP','Q','OH','SH','C','DL') join 
			mk_style_properties sp on sp.style_id=od.product_style group by od.product_style) taba group by taba.category";
		$result_sales30days=func_query($sql, true);
		foreach($result_sales30days as $row){
			if($row['category'] == null){
				$row['category'] = 'Others';
			}
			$ctgryReportData[$row['category']][12] += $row['qtyItems'];
			$ctgryReportData[$row['category']][13] += $row['qtyStyles'];
			$ctgryReportData[$row['category']][14] += (float)$row['totalValue'];
			$ctgryReportData[$row['category']][16] += (float)($ctgryReportData[$row['category']][6]/($row['qtyItems']/30)); // Closing Items/outwards per day
		}


		$i=0;
		foreach ($ctgryReportData as $category => $categoryArr){
			$table->setCellData($i, 0, $category);
			for($k=0;$k<17;$k++){
				$table->setCellData($i, $k+1, $categoryArr[$k]);
			}
			$i++;
		}
		return $table;
	}

	public function buildReport(){
		global $weblog;
		$table= new Table('Brandwise Breakup - MTD');
		$group= new TableGroup("Inventory Report");
		$superHeaders= array('Brands'=>1,'Inwards'=>3,'Outwards'=>3,'Closing'=>3,'Last 7 days sales'=>3,'Last 30 days sales'=>3,'Stock Cover'=>2);
		$headers= array('','# Items', '# Articles', 'Value (Cost)', '# Items', '# Articles', 'Value (Cost)', '# Items', '# Articles', 'Value (Cost)','# Items', '# Articles', 'Value (Cost)','# Items', '# Articles', 'Value (Cost)','Based on last 7 days','Based on last 30 days');
		$table->setSuperHeaders($superHeaders);
		$table->setHeaders($headers);
		$table->setFooterVisible(false);
		$filtervalue = $this->getFilterValue("Date:");
		$date_split=split('/', $filtervalue);
		$current_month=(INT)$date_split[0];
		$current_year=(INT)$date_split[2];
		$current_date=(INT)$date_split[1];
		$calculated_dates1 = calculateDates($current_date, $current_month, $current_year); //MTD is index:13
		$calculated_dates2 = calculateDates_DaysFormat1($current_date, $current_month, $current_year); //index : 1 and 2
		$ReportData;

		$sql="select sp.global_attr_brand as brand, sum(taba.items) as InwardedItems, count(distinct sp.style_id) as InwardedArticles, sum(taba.items*taba.cp) as value from
			(select ci.sku_id, count(*) as items, avg(posku.price) as cp from myntra_auditor.audit_trail atrail  join myntra.core_items ci join myntra.core_po_skus posku where ci.id=atrail.entityId and ci.po_sku_id = posku.id 
			                and entityName = 'com.myntra.framework.entities.Item' and entityProperty='itemStatus'
							and entityPropertyNewValue = 'STORED' and actionTime >= ('" . $calculated_dates1[13]['start'] . "') and actionTime < ('" . $calculated_dates1[13]['end'] . "') group by ci.sku_id
			) taba left join myntra.mk_styles_options_skus_mapping osm on osm.sku_id=taba.sku_id left join mk_style_properties sp on osm.style_id=sp.style_id 
			group by sp.global_attr_brand";
		$result_inwards=func_query($sql, true);
		foreach($result_inwards as $row){
			if($row['brand'] == null){
				$row['brand'] = 'Others';
			}
			$ReportData[$row['brand']][0] = $row['InwardedItems'];
			$ReportData[$row['brand']][1] = $row['InwardedArticles'];
			$ReportData[$row['brand']][2] = (float)$row['value'];
		}

		$sql="select sp.global_attr_brand as brand, sum(taba.items) as outwardItems, count(distinct sp.style_id) as outwardArticles, sum(taba.items*taba.cp) as value from
			(select ci.sku_id, count(*) as items, avg(posku.price) as cp from myntra_auditor.audit_trail atrail  join myntra.core_items ci join myntra.core_po_skus posku where ci.id=atrail.entityId and ci.po_sku_id = posku.id 
			                and entityName = 'com.myntra.framework.entities.Item' and entityProperty='itemStatus'
							and entityPropertyOldValue = 'STORED' and actionTime >= ('" . $calculated_dates1[13]['start'] . "') and actionTime < ('" . $calculated_dates1[13]['end'] . "') group by ci.sku_id
			) taba left join myntra.mk_styles_options_skus_mapping osm on osm.sku_id=taba.sku_id left join mk_style_properties sp on osm.style_id=sp.style_id 
			group by sp.global_attr_brand";
		$result_outwards=func_query($sql, true);
		foreach($result_outwards as $row){
			if($row['brand'] == null){
				$row['brand'] = 'Others';
			}
			$ReportData[$row['brand']][3] = $row['outwardItems'];
			$ReportData[$row['brand']][4] = $row['outwardArticles'];
			$ReportData[$row['brand']][5] = (float)$row['value'];
		}
		$sql="select sp.global_attr_brand as brand, sum(taba.items) as ClosingItems, count(distinct sp.style_id) as ClosingArticles, sum(taba.items*taba.cp) as value from
			(select ci.sku_id, count(*) as items, avg(posku.price) as cp from core_items ci join myntra.core_po_skus posku where  ci.po_sku_id = posku.id and ci.item_status in 
			('STORED', 'FOUND', 'CUSTOMER_RETURNED','RETURN_FROM_OPS','ISSUED', 'PROCESSING') group by ci.sku_id
			) taba left join myntra.mk_styles_options_skus_mapping osm on osm.sku_id=taba.sku_id left join mk_style_properties sp on osm.style_id=sp.style_id 
			group by sp.global_attr_brand";
		$result_closing=func_query($sql, true);
		foreach($result_closing as $row){
			if($row['brand'] == null){
				$row['brand'] = 'Others';
			}
			$ReportData[$row['brand']][6] = $row['ClosingItems'];
			$ReportData[$row['brand']][7] = $row['ClosingArticles'];
			$ReportData[$row['brand']][8] = (float)$row['value'];
		}

		$sql="select  taba.brand, count(*) as qtyStyles, sum(taba.items) as qtyItems, sum(taba.value) as totalValue from
			(select sp.global_attr_brand as brand, od.product_style, sum(od.amount ) as items, sum(((od.amount*od.price)/o.subtotal)*(o.total+o.shipping_cost+o.gift_charges)) as value 
            from xcart_orders o join xcart_order_details od on od.orderid=o.orderid and
			o.date >= unix_timestamp('" .$calculated_dates2[1]['start'] . "') and o.date < unix_timestamp('".$calculated_dates2[1]['end'] ."') and o.status in ('WP','Q','OH','SH','C','DL') join 
			mk_style_properties sp on sp.style_id=od.product_style group by od.product_style) taba group by taba.brand";
		$result_sales7days=func_query($sql, true);
		foreach($result_sales7days as $row){
			if($row['brand'] == null){
				$row['brand'] = 'Others';
			}
			$ReportData[$row['brand']][9] = $row['qtyItems'];
			$ReportData[$row['brand']][10] = $row['qtyStyles'];
			$ReportData[$row['brand']][11] = (float)$row['totalValue'];
			$ReportData[$row['brand']][15] = (float)($ReportData[$row['brand']][6]/($row['qtyItems']/7)); // Closing Items/sales per day
		}

		$sql="select  taba.brand, count(*) as qtyStyles, sum(taba.items) as qtyItems, sum(taba.value) as totalValue from
			(select sp.global_attr_brand as brand, od.product_style, sum(od.amount ) as items, sum(((od.amount*od.price)/o.subtotal)*(o.total+o.shipping_cost+o.gift_charges)) as value 
            from xcart_orders o join xcart_order_details od on od.orderid=o.orderid and
			o.date >= unix_timestamp('" .$calculated_dates2[2]['start'] . "') and o.date < unix_timestamp('".$calculated_dates2[2]['end'] ."') and o.status in ('WP','Q','OH','SH','C','DL') join 
			mk_style_properties sp on sp.style_id=od.product_style group by od.product_style) taba group by taba.brand";
		$result_sales30days=func_query($sql, true);
		foreach($result_sales30days as $row){
			if($row['brand'] == null)
			$row['brand'] = 'Others';
			$ReportData[$row['brand']][12] = $row['qtyItems'];
			$ReportData[$row['brand']][13] = $row['qtyStyles'];
			$ReportData[$row['brand']][14] = (float)$row['totalValue'];
			$ReportData[$row['brand']][16] = (float)($ReportData[$row['brand']][6]/($row['qtyItems']/30)); // Closing Items/sales per day
		}


		$i=0;
		foreach ($ReportData as $brand => $brandArr){
			$table->setCellData($i, 0, $brand);
			for($k=0;$k<17;$k++){
				$table->setCellData($i, $k+1, $brandArr[$k]);
			}
			$i++;
		}


		// Make other table for category wise details
		$categoryWiseTable = $this->categoryWiseInfo($calculated_dates1,$calculated_dates2);
		
		$this->addTableGroup($group);
		$group->addTable($table);
		$group->addTable($categoryWiseTable);
		$this->isBuild=true;
		unset($ReportData);
		unset($info);
		unset($result);

	}


}
?>

