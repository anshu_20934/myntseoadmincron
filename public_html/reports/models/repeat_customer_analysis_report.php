<?php
include_once REP_DIR.'/models/models.php';
include_once REP_DIR.'/rendrers/rendrers.php';
include_once REP_DIR.'/models/date_calculation.php';

class RepeatCustomerAnalysisReportModel extends ReportModel{
	public function __construct(){
		parent::__construct();
		$this->name="Repeat Customers Activity";
	}
	
	protected function initFilters(){
		$this->addFilter("Date:" ,self::$FILTER_TYPE_DATE);
	}

	public function loadDefaultFilterValues(){
		$time=time();
		$this->setFilterValue("Date:", date("m/d/Y",$time));
	}
	
	
	private function getAcquiredCustomers($currentyear, $currentMonth) {
		
		$currentMonthInSQLFormat = "$currentyear-" . $currentMonth . "-1 00:00:00";
			
		if($currentMonth<12) {
			$nextMonthInSQLFormat = "$currentyear-" . ($currentMonth+1). "-1 00:00:00";
		} else {
			$nextMonthInSQLFormat = ($currentyear+1) . "-1-1 00:00:00";
		}
	
		
		// Definition of new Customer Acquired: A new Customer is said to be acquired in Jan-2011, if he made first successful purchase in Jan-2011
		// Note: The customer may have registered with myntra.com much earlier, say Nov-2010, but bought first time only in Jan-2011
		$queryNewCustomersAcquiredForCurrentMonth = 
		"select count(distinct login) as newCustomerAcquired from xcart_orders ord1 
		where 	ord1.date>=unix_timestamp('$currentMonthInSQLFormat') and ord1.date<unix_timestamp('$nextMonthInSQLFormat')
				and ord1.status in ('C','SH') and ord1.orgid='0' and ord1.source_id='1'
				and ord1.date=(	select min(ord2.date) from xcart_orders ord2 where ord2.login=ord1.login and ord2.status in ('C', 'SH'))";
		
		$customersAcquired = func_query_first_cell($queryNewCustomersAcquiredForCurrentMonth, true);
		if(empty($customersAcquired)) {
			$customersAcquired = 0;
		}
		
		return $customersAcquired;
	}
	
	
	private function getRepeatCustomerData($currentyear, $currentMonth, $startMonth, $endMonth) {
		
		$currentMonthInSQLFormat = "$currentyear-$currentMonth-1 00:00:00";
		if($currentMonth<12) {
			$nextMonthInSQLFormat = "$currentyear-" . ($currentMonth+1). "-1 00:00:00";
		} else {
			$nextMonthInSQLFormat = ($currentyear+1) . "-1-1 00:00:00";
		}
		
		$monthForRepeatCheckInSQLFormat = "$currentyear-$startMonth-1 00:00:00";
		
		if($endMonth<12) {
			$monthNextForRepeatCheckInSQLFormat = "$currentyear-$endMonth-1 00:00:00";
		} else {
			$monthNextForRepeatCheckInSQLFormat = ($currentyear+1) . "-1-1 00:00:00";
		}
		
		$queryRepeatCustomers = 
		"select count(distinct login) as repeatCustomerCount, sum(total) as repeatRevenue, count(*) as repeatTransactions from xcart_orders ord1 
		where	ord1.date>=unix_timestamp('$monthForRepeatCheckInSQLFormat') and ord1.date<unix_timestamp('$monthNextForRepeatCheckInSQLFormat')
				and ord1.status in ('C','SH') and ord1.orgid='0' and ord1.source_id='1'
				and ord1.date>(	select min(ord2.date) from xcart_orders ord2 where ord2.login=ord1.login and ord2.status in ('C', 'SH'))
				and ((select min(ord2.date) from xcart_orders ord2 where ord2.login=ord1.login and ord2.status in ('C', 'SH')
					 ) between unix_timestamp('$currentMonthInSQLFormat') and unix_timestamp('$nextMonthInSQLFormat')-1)";
		
		$repeatCustomerData = func_query_first($queryRepeatCustomers, true);
		return $repeatCustomerData;
	}

	
	public function buildReport(){
		if($this->isBuild)
			return;
			
		$filtervalue = $this->getFilterValue("Date:");
		$date_split=split('/', $filtervalue);
		$current_month=(INT)$date_split[0];
		$current_year=(INT)$date_split[2];
		$current_date=(INT)$date_split[1];
		
		$curr_fdate= new FDate($current_date, $current_month, $current_year);
		$jan_date= new FDate(1, 1, $current_year);		
		$group= new TableGroup("Repeat Customers Activity");
		$this->addTableGroup($group);
		$table= new Table();
		$group->addTable($table);
		$table->setFooterVisible(false);
		
		$superHeaders = array(	"Month"=>1,
								"# New Customers Acquired" => 1,
								"# Repeat Customers" => 1,
							 	"Repeat Revenue Till Date" => 1,
								"Revenue/Customer/Month" => 1,
								"# Transaction (Till Date)" => 1,
								"# Transactions/Customer/Month" => 1,
						);
						
		$headers = array('','','','','','','');
		
		$i = 1; 
		while ($i<$current_month) { 
			$monthName = $jan_date->getMonthNameString();
			$superHeaders[$monthName] = 4;
			
			$headers[] = "# Repeat Customers";
			$headers[] = "Repeat Revenue";
			$headers[] = "# Repeat Trnx";
			$headers[] = "Avg Order Value";
			
			$table->setCellData($i-1, 0, $monthName);
			
			$jan_date=$jan_date->getNextMonthStartFDate();
			$i++;
		}

		$table->setSuperHeaders($superHeaders);
		$table->setHeaders($headers);
				
		$ReportData = array();
		
		$i=1;
		while ($i<$current_month){
			
			$ReportData[$i]['newCustomersAcquired'] = $this->getAcquiredCustomers($current_year, $i);
			
			$j = $i;
			while($j<$current_month) {
				
				$repeatCustomerData = $this->getRepeatCustomerData($current_year, $i, $j, $j+1);
				
				$repeatCustomerCount = $repeatCustomerData['repeatCustomerCount'];
				$repeatRevenue = $repeatCustomerData['repeatRevenue'];
				$repeatTransactions = $repeatCustomerData['repeatTransactions'];
				$repeatAOV = 0;
				
				if(empty($repeatCustomerCount)) {
					$repeatCustomerCount = 0;
				}					
				if(empty($repeatRevenue)) {
					$repeatRevenue = 0;
				}					
				if(empty($repeatTransactions)) {
					$repeatTransactions = 0;
				}
					
				if(!empty($repeatTransactions)) {
					$repeatAOV = round($repeatRevenue/$repeatTransactions, 2);
				}
				$ReportData[$i]['repeatCustomerCount'][$j] = $repeatCustomerCount;
				$ReportData[$i]['repeatCustomerRevenue'][$j] = $repeatRevenue;
				$ReportData[$i]['repeatTransactions'][$j] = $repeatTransactions;
				$ReportData[$i]['repeatAOV'][$j] = $repeatAOV;
				
				$j++;
			}
			
			$repeatDataTillDate = $this->getRepeatCustomerData($current_year, $i, $i, $current_month);
			$ReportData[$i]['repeatCustomerCountTillDate']= $repeatDataTillDate['repeatCustomerCount'];
			$ReportData[$i]['repeatRevenueTillDate'] = $repeatDataTillDate['repeatRevenue'];
			
			$ReportData[$i]['repeatRevenuePerCustomerPerMonth'] = 
				round(($repeatDataTillDate['repeatRevenue']/$repeatDataTillDate['repeatCustomerCount'])/($current_month-$i), 2);
				
			$ReportData[$i]['repeatTransactionsTillDate'] = $repeatDataTillDate['repeatTransactions'];
			$ReportData[$i]['repeatTransactionsPerCustomerPerMonth'] = 
				round(($repeatDataTillDate['repeatTransactions']/$repeatDataTillDate['repeatCustomerCount'])/($current_month-$i), 2);
			
			$i++;
		}
		
		$i=1;
		while($i<$current_month) {
			
			$row=$i-1;
			$col=1;
			
			$table->setCellData($row, $col++, $ReportData[$i]['newCustomersAcquired']);
			$table->setCellData($row, $col++, $ReportData[$i]['repeatCustomerCountTillDate']);
			$table->setCellData($row, $col++, $ReportData[$i]['repeatRevenueTillDate']);
			$table->setCellData($row, $col++, $ReportData[$i]['repeatRevenuePerCustomerPerMonth']);
			$table->setCellData($row, $col++, $ReportData[$i]['repeatTransactionsTillDate']);
			$table->setCellData($row, $col++, $ReportData[$i]['repeatTransactionsPerCustomerPerMonth']);
			
			$j=1;
			
			while($j<$current_month) {
				if(!empty($ReportData[$i]['repeatCustomerCount'][$j])) {
					$table->setCellData($row, $col++, $ReportData[$i]['repeatCustomerCount'][$j]);
				} else {
					$table->setCellData($row, $col++, '');
				}
				
				if(!empty($ReportData[$i]['repeatCustomerRevenue'][$j])) {
					$table->setCellData($row, $col++, $ReportData[$i]['repeatCustomerRevenue'][$j]);
				} else {
					$table->setCellData($row, $col++, '');
				}
				
				if(!empty($ReportData[$i]['repeatTransactions'][$j])) {
					$table->setCellData($row, $col++, $ReportData[$i]['repeatTransactions'][$j]);
				} else {
					$table->setCellData($row, $col++, '');
				}
				
				if(!empty($ReportData[$i]['repeatAOV'][$j])) {
					$table->setCellData($row, $col++, $ReportData[$i]['repeatAOV'][$j]);
				} else {
					$table->setCellData($row, $col++, '');
				}
				
				$j++;
			}
				
			$i++;
		}
		
		$this->isBuild=true;
	}
}