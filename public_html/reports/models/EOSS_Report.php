<?php
global $xcart_dir;
include_once $xcart_dir.'/reports/models/models.php';
include_once $xcart_dir.'/reports/rendrers/rendrers.php';
include_once REP_DIR.'/models/date_calculation.php';

class EOSSReportModel extends ReportModel {

	public function __construct(){
		parent::__construct();
		$this->name="EOSS Revenue Report : " . date("j-M-Y");
	}
	
	protected function initFilters(){
		$this->addFilter("Date:" ,self::$FILTER_TYPE_DATE);
	}

	public function loadDefaultFilterValues(){
		$time=time();
		$this->setFilterValue("Date:", date("m/d/Y",$time));
	}

	public function buildReport(){
		
		// Report is executed by following format
		// http://<domainname>/admin/AdminReportViewer.php?report_name=/reports/ReportRenderer.php&report=27&format=html&action=genreport&prompt_filters=no&inline=true&attachment=true&decimals=2
				
		global $weblog;
		$table= new Table();
		$group= new TableGroup("EOSS Report");
		$superHeaders= array(''=>1,'Sales before EOSS (Weekly Average)'=>3,'Sales from start of EOSS till date (Weekly Average)'=>6,'Sales for last 7 days (Weekly Average)'=>5);
		$headers= array('Brand','Items','Revenue','Duration','Start Date','Items','% Increase', 'Revenue','% Increase','Average Discount %','Items','Percentage Increase', 'Revenue','% Increase','Average Discount %');
		$table->setFooterVisible(false);
		$filtervalue = $this->getFilterValue("Date:");
		$date_split=split('/', $filtervalue);
		$current_month=(INT)$date_split[0];
		$current_year=(INT)$date_split[2];
		$current_date=(INT)$date_split[1];
		$ReportData;

		$today= new FDate($current_date, $current_month, $current_year);
		$last7thDay= new FDate($current_date, $current_month, $current_year);
		$last7thDay = $last7thDay->addDays(-7);
		
		$today = $today->toUnixDateDayStart();
		$last7thDay = $last7thDay->toUnixDateDayStart();
		
		$brandArticleTypesSQL = "select sp.global_attr_brand as brand, cc.typename as articletype
								 from mk_style_properties sp, mk_catalogue_classification cc
								 where sp.global_attr_article_type=cc.id group by brand, articletype";
		
		$brandArticleTypes = func_query($brandArticleTypesSQL, true);
		
		$startDateList = $this->getStartDates();
		
		//$startDate = "2011-07-01 00:00:00";
		
		foreach ($brandArticleTypes as $brandArticleTypeValue) {
			$startDate = "";
			$brand = $brandArticleTypeValue['brand'];
			$articleType = $brandArticleTypeValue['articletype'];
			
			$startDate = $startDateList[$brand][$articleType]['startdate'];

			if(empty($startDate)) {
				$startDate = $startDateList[$brand]['startdate'];
			}

			if(empty($startDate)) {
				continue;
			}

			// Query for fetching data BEFORE start of EOSS. We find weekly average data for one month before start of EOSS.
			$sql = "select sp.global_attr_brand as brand,cc.typename as articletype, 
					sum(xd.amount) as totalQuantity, sum(xd.total_amount) as totalCost, (sum(xd.total_amount)/sum(xd.amount)) as averageCost,
					datediff(\"$startDate\", from_unixtime(min(xo.date))) as duration
					from mk_style_properties sp, mk_catalogue_classification cc, xcart_order_details xd, xcart_orders xo 
					where sp.global_attr_article_type=cc.id and sp.style_id=xd.product_style 
					and xo.orderid=xd.orderid and xo.status in ('Q', 'WP', 'C', 'SH', 'OH','DL') 
					and xo.date < unix_timestamp(\"$startDate\") and xo.date >= (unix_timestamp(\"$startDate\")-3600*24*30)
					and sp.global_attr_brand='" . mysql_real_escape_string($brand) . "' and cc.typename='" . mysql_real_escape_string($articleType) . "' 
					and xd.item_status in ('D', 'S') and xd.amount > 0
					group by sp.global_attr_brand, cc.typename";
			
			$result = func_query($sql, true);
		
			$durationOfSaleBeforeEOSS = 0;
			$itemsSoldBeforeEOSS = 0;
			$revenueBeforeEOSS = 0;
			$weeklyItemsSoldBeforeEOSS = 0;
			$weeklyRevenueBeforeEOSS = 0;
			
			foreach ($result as $row) {
				$itemsSoldBeforeEOSS = $row['totalQuantity'];
				$revenueBeforeEOSS = $row['totalCost'];
				$durationOfSaleBeforeEOSS = $row['duration'];
				
				$weeklyItemsSoldBeforeEOSS = ($itemsSoldBeforeEOSS*7)/$durationOfSaleBeforeEOSS;
				$weeklyRevenueBeforeEOSS = ($revenueBeforeEOSS*7)/$durationOfSaleBeforeEOSS;
				
				$ReportData[$row['brand']][$row['articletype']]['categoryName'] = $row['articletype'];
				$ReportData[$row['brand']][$row['articletype']]['startdateEOSS'] = $startDate;
				
				// Note all data is computed for weekly average.
				$ReportData[$row['brand']][$row['articletype']]['itemsSoldBeforeEOSS'] = (float)(round($weeklyItemsSoldBeforeEOSS, 2));
				$ReportData[$row['brand']][$row['articletype']]['revenueBeforeEOSS'] = (float)(round($weeklyRevenueBeforeEOSS,2));
				$ReportData[$row['brand']][$row['articletype']]['durationOfSaleBeforeEOSS'] = $durationOfSaleBeforeEOSS;
			}

			
			// Query to fetch data from start of EOSS till current date. 
			$sql = "select sp.global_attr_brand as brand,cc.typename as articletype, 
					datediff(from_unixtime(unix_timestamp(now())+3600*24), \"$startDate\") as duration, 
					sum(xd.amount) as totalQuantity, sum(xd.total_amount) as totalCost, 
					(sum(xd.actual_product_price*xd.amount)-sum(xd.total_amount)) as discount, 
					(sum(xd.actual_product_price*xd.amount)) as mrp,
					(sum(xd.actual_product_price*xd.amount)-sum(xd.total_amount))/(sum(xd.actual_product_price*xd.amount)) as averageDiscountPercentage
					from mk_style_properties sp, mk_catalogue_classification cc, xcart_order_details xd, xcart_orders xo 
					where sp.global_attr_article_type=cc.id and sp.style_id=xd.product_style 
					and xo.orderid=xd.orderid and xo.status in ('Q', 'WP', 'C', 'SH', 'OH','DL')  and xo.date >= unix_timestamp(\"$startDate\") 
					and sp.global_attr_brand='" . mysql_real_escape_string($brand) . "' and cc.typename='" . mysql_real_escape_string($articleType) . "'
					and xd.item_status in ('D', 'S') and xd.amount > 0
					group by sp.global_attr_brand, cc.typename";
			
			$result = func_query($sql, true);
			
			
			$itemsSoldAfterEOSS = 0;
			$revenueAfterEOSS = 0;
			$durationofSaleAfterEOSS = 0;
			$weeklyItemsSoldAfterEOSS = 0;
			$weeklyRevenueAfterEOSS = 0;
			
			foreach ($result as $row){
				$ReportData[$row['brand']][$row['articletype']]['categoryName'] = $row['articletype'];
				$ReportData[$row['brand']][$row['articletype']]['startdateEOSS'] = $startDate;
				
				$itemsSoldAfterEOSS  = $row['totalQuantity'];
				$revenueAfterEOSS = $row['totalCost'];
				$durationofSaleAfterEOSS = $row['duration'];
				
				$weeklyItemsSoldAfterEOSS = ($itemsSoldAfterEOSS*7)/$durationofSaleAfterEOSS;
				$weeklyRevenueAfterEOSS = ($revenueAfterEOSS*7)/$durationofSaleAfterEOSS;
				
				$ReportData[$row['brand']][$row['articletype']]['itemsSoldAfterEOSS'] = (float)(round($weeklyItemsSoldAfterEOSS, 2));
				$ReportData[$row['brand']][$row['articletype']]['revenueAfterEOSS'] = (float)(round($weeklyRevenueAfterEOSS, 2));
				$ReportData[$row['brand']][$row['articletype']]['durationAfterEOSS'] = $durationofSaleAfterEOSS;
				
				$ReportData[$row['brand']][$row['articletype']]['discountAfterEOSS'] = (float)(round($row['discount'], 2));
				$ReportData[$row['brand']][$row['articletype']]['mrpAfterEOSS'] = (float)(round($row['mrp'], 2));
				
				$percentageItemIncreaseAfterEOSS = "";
				if(!empty($itemsSoldBeforeEOSS)) {
					$percentageItemIncreaseAfterEOSS = (float)(round(($weeklyItemsSoldAfterEOSS-$weeklyItemsSoldBeforeEOSS)*100/$weeklyItemsSoldBeforeEOSS, 2));
				}
				$ReportData[$row['brand']][$row['articletype']]['percentageItemIncreaseAfterEOSS'] = $percentageItemIncreaseAfterEOSS;
				
				$percentageRevenueIncreaseAfterEOSS = "";
				if(!empty($revenueBeforeEOSS)) {
					$percentageRevenueIncreaseAfterEOSS = (float)(round(($weeklyRevenueAfterEOSS-$weeklyRevenueBeforeEOSS)*100/$weeklyRevenueBeforeEOSS, 2));
				}
				
				$ReportData[$row['brand']][$row['articletype']]['percentageRevenueIncreaseAfterEOSS'] = $percentageRevenueIncreaseAfterEOSS;
				$ReportData[$row['brand']][$row['articletype']]['averageDiscountPercentageAfterEOSS'] = (float)(round($row['averageDiscountPercentage']*100, 2));
			}

			
			$itemsSoldLastWeek = 0;
			$revenueLastWeek = 0;
			$durationLastWeek = 0;
			$weeklyItemsSoldLastWeek = 0;
			$weeklyRevenueSinceLastWeek = 0;
			 
			// Query to fetch last ONE WEEK data (after start of EOSS).
			$sql = "select sp.global_attr_brand as brand,cc.typename as articletype, 
					sum(xd.amount) as totalQuantity, sum(xd.total_amount) as totalCost, 
					datediff(\"$last7thDay\", \"$startDate\") as duration,
					(sum(xd.actual_product_price*xd.amount)-sum(xd.total_amount)) as discount, 
					(sum(xd.actual_product_price*xd.amount)) as mrp,
					(sum(xd.actual_product_price*xd.amount)-sum(xd.total_amount))/(sum(xd.actual_product_price*xd.amount)) as averageDiscountPercentage
					from mk_style_properties sp, mk_catalogue_classification cc, xcart_order_details xd, xcart_orders xo 
					where sp.global_attr_article_type=cc.id and sp.style_id=xd.product_style 
					and xo.orderid=xd.orderid and xo.status in ('Q', 'WP', 'C', 'SH', 'OH','DL')
					and xo.date < unix_timestamp(\"$today\") and xo.date >= unix_timestamp(\"$last7thDay\") and xo.date >= unix_timestamp(\"$startDate\") 
					and sp.global_attr_brand='" . mysql_real_escape_string($brand) . "' and cc.typename='" . mysql_real_escape_string($articleType) . "'
					and xd.item_status in ('D', 'S') and xd.amount > 0
					group by sp.global_attr_brand, cc.typename";
					
			$result = func_query($sql, true);

			foreach ($result as $row){
				
				$ReportData[$row['brand']][$row['articletype']]['categoryName'] = $row['articletype'];
				$ReportData[$row['brand']][$row['articletype']]['startdateEOSS'] = $startDate;
				
				$itemsSoldLastWeek = $row['totalQuantity'];
				$revenueLastWeek = $row['totalCost'];
				
				$durationLastWeek = $row['duration'];
				if($durationLastWeek<0) {
					// In case the report is executed for results within one week of starting of EOSS for some brand/articletype.
					$durationLastWeek = $durationLastWeek + 7;
				} else {
					$durationLastWeek = 7;
				}
				
				$weeklyItemsSoldLastWeek = ($itemsSoldLastWeek*7)/$durationLastWeek;
				$weeklyRevenueSinceLastWeek = ($revenueLastWeek*7)/$durationLastWeek; 
				
				$ReportData[$row['brand']][$row['articletype']]['itemsSoldLastWeek'] = (float)(round($weeklyItemsSoldLastWeek, 2));
				$ReportData[$row['brand']][$row['articletype']]['revenueLastWeek'] = (float)(round($weeklyRevenueSinceLastWeek, 2));
				
				$percentageItemIncreaseLastWeek = "";
				if(!empty($itemsSoldBeforeEOSS)) {
					$percentageItemIncreaseLastWeek = (float)(round(($weeklyItemsSoldLastWeek-$weeklyItemsSoldBeforeEOSS)*100/$weeklyItemsSoldBeforeEOSS, 2));
				}
				$ReportData[$row['brand']][$row['articletype']]['percentageItemIncreaseLastWeek'] = $percentageItemIncreaseLastWeek;
				
				$percentageRevenueIncreaseLastWeek = "";
				if(!empty($revenueBeforeEOSS)) {
					$percentageRevenueIncreaseLastWeek = (float)(round(($weeklyRevenueSinceLastWeek-$weeklyRevenueBeforeEOSS)*100/$weeklyRevenueBeforeEOSS, 2));
				}
				
				$ReportData[$row['brand']][$row['articletype']]['percentageRevenueIncreaseLastWeek'] = $percentageRevenueIncreaseLastWeek;
				
				$ReportData[$row['brand']][$row['articletype']]['averageDiscountPercentageLastWeek'] = (float)(round($row['averageDiscountPercentage']*100, 2));
				$ReportData[$row['brand']][$row['articletype']]['durationLastWeek'] = $durationLastWeek;
				
				$ReportData[$row['brand']][$row['articletype']]['discountLastWeek'] = (float)(round($row['discount'], 2));
				$ReportData[$row['brand']][$row['articletype']]['mrpLastWeek'] = (float)(round($row['mrp'], 2));
			}
		}
		
		$table->setHeaders($headers);
		$table->setSuperHeaders($superHeaders);
		$i=0;
		
		foreach($ReportData as $brand => $info) {
			$j=0;
			$table->setCellData($i, $j, $brand);
			$table->setRowStyle($i, TABLE::$STYLE_BORDER, "border: 1px solid #000000; background-color: #B0B0B0; font-weight: bold;");
			$table->setRowStyle($i, TABLE::$STYLE_ALIGN,  "left");
			
			$brandRow = $i;
			$i++; // Article types on a new row.
			
			$brandTotalItemsSoldBeforeEOSS = 0;
			$brandTotalItemsSoldAfterEOSS = 0;
			$brandTotalItemsSoldLastWeek = 0;
			
			$brandTotalRevenueBeforeEOSS = 0;
			$brandTotalRevenueAfterEOSS = 0;
			$brandTotalRevenueLastWeek = 0;
			
			$brandTotalDiscountAfterEOSS = 0;
			$brandTotalDiscountLastWeek = 0;
			
			$brandTotalMRPAfterEOSS = 0;
			$brandTotalMRPLastWeek = 0;
			
			foreach($info as $categoryWiseInfo) {
				
				$brandTotalItemsSoldBeforeEOSS+= $categoryWiseInfo['itemsSoldBeforeEOSS'];
				$brandTotalItemsSoldAfterEOSS+= $categoryWiseInfo['itemsSoldAfterEOSS'];
				$brandTotalItemsSoldLastWeek+= $categoryWiseInfo['itemsSoldLastWeek'];
				
				$brandTotalRevenueBeforeEOSS+= $categoryWiseInfo['revenueBeforeEOSS'];
				$brandTotalRevenueAfterEOSS+= $categoryWiseInfo['revenueAfterEOSS'];
				$brandTotalRevenueLastWeek+= $categoryWiseInfo['revenueLastWeek'];
				
				// discountAfterEOSS. mrpAfterEOSS . discountLastWeek . mrpLastWeek

				$brandTotalMRPAfterEOSS+= $categoryWiseInfo['mrpAfterEOSS'];
				$brandTotalMRPLastWeek+= $categoryWiseInfo['mrpLastWeek'];
				
				$brandTotalDiscountAfterEOSS+= $categoryWiseInfo['discountAfterEOSS'];
				$brandTotalDiscountLastWeek+= $categoryWiseInfo['discountLastWeek'];
				
				$j=0;
				$table->setCellData($i, $j, $categoryWiseInfo['categoryName']);
				$table->setCellStyle($i, $j, TABLE::$STYLE_ALIGN,  "right");
				
				// Data before start of EOSS
				$table->setCellData($i, ++$j, $categoryWiseInfo['itemsSoldBeforeEOSS']);
				$table->setCellData($i, ++$j, $categoryWiseInfo['revenueBeforeEOSS']);
				$table->setCellData($i, ++$j, $categoryWiseInfo['durationOfSaleBeforeEOSS']);
								
				// From EOSS start till date EOSS data
				$table->setCellData($i, ++$j, $categoryWiseInfo['startdateEOSS']);
				$table->setCellData($i, ++$j, $categoryWiseInfo['itemsSoldAfterEOSS']);
				$table->setCellData($i, ++$j, $categoryWiseInfo['percentageItemIncreaseAfterEOSS']);
				$table->setCellData($i, ++$j, $categoryWiseInfo['revenueAfterEOSS']);
				$table->setCellData($i, ++$j, $categoryWiseInfo['percentageRevenueIncreaseAfterEOSS']);
				$table->setCellData($i, ++$j, $categoryWiseInfo['averageDiscountPercentageAfterEOSS']);
				
				// Last 7 days data EOSS data
				$table->setCellData($i, ++$j, $categoryWiseInfo['itemsSoldLastWeek']);
				$table->setCellData($i, ++$j, $categoryWiseInfo['percentageItemIncreaseLastWeek']);
				$table->setCellData($i, ++$j, $categoryWiseInfo['revenueLastWeek']);
				$table->setCellData($i, ++$j, $categoryWiseInfo['percentageRevenueIncreaseLastWeek']);
				$table->setCellData($i, ++$j, $categoryWiseInfo['averageDiscountPercentageLastWeek']);
				$i++;
			}
			
			$brandItemsIncreasePercentageAfterEOSS ="";
			if(!empty($brandTotalItemsSoldBeforeEOSS)) {
				$brandItemsIncreasePercentageAfterEOSS = (float)(round((($brandTotalItemsSoldAfterEOSS-$brandTotalItemsSoldBeforeEOSS)/$brandTotalItemsSoldBeforeEOSS)*100,  2));
			}
			
			$brandRevenueIncreasePercentageAfterEOSS ="";
			if(!empty($brandTotalRevenueBeforeEOSS)) {
				$brandRevenueIncreasePercentageAfterEOSS = (float)(round((($brandTotalRevenueAfterEOSS-$brandTotalRevenueBeforeEOSS)/$brandTotalRevenueBeforeEOSS)*100,  2));
			}
			
			$brandDiscountPercentageAfterEOSS = "";
			if(!empty($brandTotalMRPAfterEOSS)) {
				$brandDiscountPercentageAfterEOSS = (float)(round($brandTotalDiscountAfterEOSS*100/$brandTotalMRPAfterEOSS, 2));
			}
			
			$brandItemsIncreasePercentageLastWeek ="";
			if(!empty($brandTotalItemsSoldLastWeek)) {
				$brandItemsIncreasePercentageLastWeek = (float)(round((($brandTotalItemsSoldLastWeek-$brandTotalItemsSoldBeforeEOSS)/$brandTotalItemsSoldBeforeEOSS)*100,  2));
			}
			
			$brandRevenueIncreasePercentageLastWeek ="";
			if(!empty($brandTotalRevenueLastWeek)) {
				$brandRevenueIncreasePercentageLastWeek = (float)(round((($brandTotalRevenueLastWeek-$brandTotalRevenueBeforeEOSS)/$brandTotalRevenueBeforeEOSS)*100,  2));
			}
			
			$brandDiscountPercentageLastWeek="";
			if(!empty($brandTotalMRPLastWeek)) {
				$brandDiscountPercentageLastWeek  = (float)(round($brandTotalDiscountLastWeek*100/$brandTotalMRPLastWeek, 2));
			}
			
			$j=0;
			$table->setCellData($brandRow, ++$j, $brandTotalItemsSoldBeforeEOSS);
			$table->setCellData($brandRow, ++$j, $brandTotalRevenueBeforeEOSS);
			$table->setCellData($brandRow, ++$j, "");
			$table->setCellData($brandRow, ++$j, "");
			$table->setCellData($brandRow, ++$j, $brandTotalItemsSoldAfterEOSS);
			$table->setCellData($brandRow, ++$j, $brandItemsIncreasePercentageAfterEOSS);
			$table->setCellData($brandRow, ++$j, $brandTotalRevenueAfterEOSS);
			$table->setCellData($brandRow, ++$j, $brandRevenueIncreasePercentageAfterEOSS);
			$table->setCellData($brandRow, ++$j, $brandDiscountPercentageAfterEOSS);
			$table->setCellData($brandRow, ++$j, $brandTotalItemsSoldLastWeek);
			$table->setCellData($brandRow, ++$j, $brandItemsIncreasePercentageLastWeek);
			$table->setCellData($brandRow, ++$j, $brandTotalRevenueLastWeek);
			$table->setCellData($brandRow, ++$j, $brandRevenueIncreasePercentageLastWeek);
			$table->setCellData($brandRow, ++$j, $brandDiscountPercentageLastWeek);
		}

		$group->addTable($table);
		$this->addTableGroup($group);
		$this->isBuild=true;
	}
	
	
	private function getStartDates() {
		$brandDates = array(
			"Lee Cooper"		=>	array("startdate" => "2011-07-01 00:00:00"),
			"Numero Uno"		=>	array("startdate" => "2011-07-01 00:00:00"),
			"Ed Hardy"			=>	array("startdate" => "2011-07-01 00:00:00"),
			"ID"				=>	array("startdate" => "2011-07-01 00:00:00"),
			"Urban Yoga"		=>	array("startdate" => "2011-07-07 00:00:00"),
			"Wrangler"			=>	array("startdate" => "2011-07-01 00:00:00"),
			"Lee"				=>	array("startdate" => "2011-07-01 00:00:00"),
			"Inkfruit"			=>	array("startdate" => "2011-07-06 00:00:00"),
			"Status Quo"		=>	array("startdate" => "2011-07-01 00:00:00"),
			"Mr.Men"			=>	array("startdate" => "2011-07-01 00:00:00"),
			"Adidas"			=>	array("startdate" => "2011-07-08 00:00:00" , 
											"Watches" => array("startdate" => "2011-07-01 00:00:00")),
			"Reebok"			=>	array("startdate" => "2011-06-24 00:00:00"),
			"Puma"				=>	array("startdate" => "2011-07-01 00:00:00"),
			"Nike"				=>	array("startdate" => "2011-07-08 00:00:00"),
			"FILA"				=>	array("startdate" => "2011-07-01 00:00:00"),
			"ASICS "			=>	array("startdate" => "2011-07-08 00:00:00"),
			"Skechers"			=>	array("startdate" => "2011-07-08 00:00:00"),
			"Converse"			=>	array("startdate" => "2011-07-08 00:00:00"),
			"Disney"			=>	array("startdate" => "2011-07-01 00:00:00"),
			"Levis Kids"		=>	array("startdate" => "2011-07-02 00:00:00"),
			"Gini and Jony"		=>	array("startdate" => "2011-07-02 00:00:00"),
			"Palm Tree"			=>	array("startdate" => "2011-07-02 00:00:00"),
			"Rockport"			=>	array("startdate" => "2011-06-24 00:00:00"),
			"Carlton London"	=>	array("startdate" => "2011-07-01 00:00:00"),
			"FIFA"				=>	array("startdate" => "2011-07-01 00:00:00")
		);
		
		return $brandDates;
	}
}
?>

