<?php

/*
 * Created By: Raghavan
 * Purpose : To send automated sla returns report mail for OPs/CC team
 *
 */
global $xcart_dir;
include_once $xcart_dir . '/reports/models/models.php';
include_once $xcart_dir . '/reports/rendrers/rendrers.php';
require_once "$xcart_dir/include/func/func.mk_old_returns_tracking.php";

class SLAReturnsReportModelToCC extends ReportModel {

    protected $mode;

    public function __construct() {
        parent::__construct();
        $this->name = "RTO - UD SLA Report";
    }

    protected function initFilters() {
        $this->addFilter();
    }

    public function loadDefaultFilterValues() {
        $time = time();
        $this->setFilterValue();
    }

    public function buildReport() {
        global $weblog;
        $group = new TableGroup("" );
        $rto_table = $this->makeTable("RTO for Customer Connect", $this->getRTRecords(), "CC");
        $ud_table = $this->makeTable("UD for Customer Connect", $this->getUDRecords(), "CC");
        $group->addTable($rto_table);
        $group->addTable($ud_table);
        $this->addTableGroup($group);
        $this->isBuild = true;
    }

    function makeTable($tableName, $slaReturns, $dept) {

        $table = new Table("");
        $headers[0] = $tableName;
        $table->setCellData(0, 0, 'State Code');
        $table->setCellData(0, 1, 'Current count of order');
        $table->setCellData(0, 2, 'SLA');
        $table->setCellData(0, 3, 'No. of orders exceeding SLA');
        $table->setCellData(0, 4, '% of orders exceeding SLA');

        $pointer = 1;
        for ($cnt = 0; $cnt < count($slaReturns); $cnt++) {
            if ($this->isAllowed($dept, $slaReturns[$cnt][currstate])) {
                $table->setCellData($pointer, 0, $this->getFullName($slaReturns[$cnt][currstate]));
                $table->setCellData($pointer, 1, $slaReturns[$cnt][total_sla]);
                $table->setCellData($pointer, 2, $slaReturns[$cnt][maxtime]);
                if ($slaReturns[$cnt][exceeding_sla_time] != null) {
                    $percentageExceedingSla = round(($slaReturns[$cnt][exceeding_sla_time] * 100) / $slaReturns[$cnt][total_sla], 2);
                    $table->setCellData($pointer, 3, $slaReturns[$cnt][exceeding_sla_time]);
                    $table->setCellData($pointer, 4, $percentageExceedingSla);
                } else {
                    $table->setCellData($pointer, 3, 0);
                    $table->setCellData($pointer, 4, 0);
                }                
                $pointer += 1;
            }
        }

        $table->setHeaders($headers);
        $table->setTableWidth(count($headers));
        $table->setFooterVisible(false);
        return $table;
    }

    function getRTRecords() {
        global $sqllog;
        $qry = "
        SELECT X.currstate, total AS total_sla, sla AS maxtime, exceeding_sla AS exceeding_sla_time
        FROM (
        SELECT currstate, sla, COUNT(*) AS total FROM mk_old_returns_tracking a,
       (SELECT mos.prevstate, MAX(mos.maxtime) AS sla
        FROM mk_old_returns_tracking_sla mos GROUP BY mos.prevstate) c
        WHERE a.currstate = c.prevstate AND
        a.returntype = 'RTO'
        GROUP BY currstate) X LEFT JOIN
        (
         SELECT currstate, COUNT(*) exceeding_sla
        FROM mk_old_returns_tracking a,
        mk_old_returns_tracking_activity b,
        (SELECT mos.prevstate, MAX(mos.maxtime) AS sla
        FROM mk_old_returns_tracking_sla mos
        GROUP BY prevstate)c
        WHERE a.id = b.returnid
        AND a.currstate = b.activitycode
        AND a.returntype = 'RTO'
        AND  a.currstate = c.prevstate
        AND TIMESTAMPDIFF(HOUR,b.recorddate, NOW()) > c.sla
        GROUP BY a.currstate) Y
        ON X.currstate = Y.currstate";
        $sqllog->debug($qry);
        $data = func_query($qry);
        return $data;
    }

    function getUDRecords() {
        global $sqllog;
        $qry = "SELECT X.currstate, total_sla, sla AS maxtime, exceeding_sla_time
        FROM (
        SELECT currstate, sla, COUNT(*) AS total_sla FROM mk_old_returns_tracking_ud a,
        (SELECT mos.prevstate, MAX(mos.maxtime) AS sla
        FROM mk_old_returns_tracking_sla mos GROUP BY mos.prevstate) c
        WHERE a.currstate = c.prevstate
        GROUP BY currstate) X LEFT JOIN
        (
        SELECT currstate, COUNT(*) exceeding_sla_time
        FROM mk_old_returns_tracking_ud a,
        mk_old_returns_tracking_ud_activity b,
        (SELECT mos.prevstate, MAX(mos.maxtime) AS sla
        FROM mk_old_returns_tracking_sla mos
        GROUP BY prevstate)c
        WHERE a.id = b.udid
        AND a.currstate = b.activitycode
        AND  a.currstate = c.prevstate
        AND TIMESTAMPDIFF(HOUR,b.recorddate, NOW()) > c.sla
        GROUP BY a.currstate) Y
        ON X.currstate = Y.currstate";
        $sqllog->debug($qry);
        $data = func_query($qry);
        return $data;
    }

    function isAllowed($dept, $currState) {
        $ccAllowed = array("RTOQ", "RTOCAL1", "RTOCAL2", "RTOC", "RTOAI", "UDQ", "UDC1", "UDC2", "UDC3");
        $opAllowed = array("RTOC", "RTORF", "RTORS", "UDCC");

        if ($dept == "CC") {
            return in_array(strtoupper($currState), array_map('strtoupper', $ccAllowed));
        } else if ($dept == "OPs") {
            return in_array(strtoupper($currState), array_map('strtoupper', $opAllowed));
        }
        return false;
    }

    function getFullName($shortName) {
        $rtoStates = _get_old_rto_states();
        $udState = _get_old_ud_states();
        if (array_key_exists(strtoupper($shortName), $rtoStates)) {
            return $rtoStates[strtoupper($shortName)];
        } else if (array_key_exists(strtoupper($shortName), $udState)) {
            return $udState[strtoupper($shortName)];
        }
        return $shortName;
    }

}
?>
