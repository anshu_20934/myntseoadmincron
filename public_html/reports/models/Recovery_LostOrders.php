<?php

global $xcart_dir;
include_once $xcart_dir.'/reports/models/models.php';
include_once $xcart_dir.'/reports/rendrers/rendrers.php';
include_once REP_DIR.'/models/date_calculation.php';

class RecoveryReportModel extends ReportModel {
	
	protected $mode;
	public function __construct($mode){
		parent::__construct();
		$this->name="Potential Lost Orders Report : " . date("j-M-Y H:i:s");
		$this->mode = $mode;

	}
	protected function initFilters(){
		$this->addFilter("Date:" ,self::$FILTER_TYPE_DATE);
	}

	public function loadDefaultFilterValues(){
		$time=time();
		$this->setFilterValue("Date:", date("m/d/Y H:i:s",$time));
	}

	public function ppOrdersInfo($orderids){
		$table= new Table('PP Orders');
		$headers = array('Customer Name', 'Email', 'Phone', 'Order Amount', 'OrderStatus', 'OrderDate', 'OrderId');
		$table->setHeaders($headers);
		$table->setTableWidth(count($headers));
		$table->setFooterVisible(false);
		$i = 0;
		$rowIdentifiers = array('name', 'email', 'phone', 'amount', 'status', 'orderdate', 'orderid');

		foreach ($orderids as $orderid){
			$cust_details = func_query("select concat(firstname,lastname) as name, login as email, ifnull(mobile, phone) as phone, sum(total+shipping_cost+gift_charges) as amount, status, from_unixtime(date) as orderdate, orderid from xcart_orders where orderid ='" . $orderid ."' and status = 'PP'", true);
			if($cust_details[0]['orderid'] == null)
			continue;
			for ($j=0; $j<count($headers); $j++){
				$table->setCellData($i, $j, $cust_details[0][$rowIdentifiers[$j]]);
			}
			$table->setCellStyle($i, $j-1, TABLE::$HYPERLINK, "http://www.myntra.com/admin/order.php?orderid=".$orderid);
			$i++;
		}
		$table->setStyleMatrix(0, 0, $i-1, 1, TABLE::$STYLE_ALIGN, "left");
		return $table;
	}

	public function pvOrdersInfo($orderids){
		$table= new Table('PV Orders');
		$headers = array('Customer Name', 'Email', 'Phone', 'Order Amount', 'OrderStatus', 'OrderDate', 'OrderId');
		$table->setHeaders($headers);
		$table->setTableWidth(count($headers));
		$table->setFooterVisible(false);
		$i = 0;
		$rowIdentifiers = array('name', 'email', 'phone', 'amount', 'status', 'orderdate', 'orderid');

		foreach ($orderids as $orderid){
			$cust_details = func_query("select concat(firstname,lastname) as name, login as email, ifnull(mobile, phone) as phone, sum(total+shipping_cost+gift_charges) as amount, status, from_unixtime(date) as orderdate, orderid from xcart_orders where orderid ='" . $orderid ."' and status = 'PV'", true);
			if($cust_details[0]['orderid'] == null)
			continue;
			for ($j=0; $j<count($headers); $j++){
				$table->setCellData($i, $j, $cust_details[0][$rowIdentifiers[$j]]);
			}
			$i++;
		}
		return $table;
	}

	public function buildReport(){
		global $weblog;
		$group= new TableGroup("Orders List");
		$filtervalue = $this->getFilterValue("Date:");
		$date_split=split('/', $filtervalue);
		$split_space = split(' ', $date_split[2]);
		$time_split = split(':', $split_space[1]);
		$current_month=(INT)$date_split[0];
		$current_year=(INT)$split_space[0];
		$current_date=(INT)$date_split[1];
		$current_hour=(INT)$time_split[0];
		
		if($this->mode == 1){
			$sql = "select mv.orderid as orders
					from 
					(select (case xo.payment_method when 'cod' then 'cod' else 'online' end) as paymethod, orderid, max(status) as max, min(status) as min ,count(status) as tried, login, firstname, lastname, email, mobile, phone, 
					date(from_unixtime(date)) as orderDate, (total+shipping_cost+gift_charges) as amount
					from xcart_orders xo where xo.date > " . mktime($current_hour -1, 0, 0, $current_month, $current_date, $current_year) . " and xo.date <= " .  mktime($current_hour, 0, 0, $current_month, $current_date, $current_year) .
					" group by login, qtyInOrder, orderDate, amount) as mv where mv.max in ('PP','PV') and mv.min in ('PP','PV') and mv.tried >=2 order by orders";
			
		}
		elseif ($this->mode == 16){
			$sql = "select mv.orderid as orders
					from 
					(select (case xo.payment_method when 'cod' then 'cod' else 'online' end) as paymethod, orderid, max(status) as max, min(status) as min , count(status) as tried, login, firstname, lastname, email, mobile, phone, 
					date(from_unixtime(date)) as orderDate, (total+shipping_cost+gift_charges) as amount
					from xcart_orders xo where xo.date > " . mktime(17, 0, 1, $current_month, $current_date-1, $current_year) . " and xo.date <= " .  mktime(09, 0, 0, $current_month, $current_date, $current_year) .
					" group by login, qtyInOrder, orderDate, amount) as mv where mv.max in ('PP','PV') and mv.min in ('PP','PV') and mv.tried >=2 order by orders";
			
		}
		$result=func_query_column($sql);
		
		//add tables to the group
		$ppTable = $this->ppOrdersInfo($result);
		$pvTable = $this->pvOrdersInfo($result);

		$group->addTable($ppTable);
		$group->addTable($pvTable);
		$this->addTableGroup($group);

		$this->isBuild=true;
	}


}
?>

