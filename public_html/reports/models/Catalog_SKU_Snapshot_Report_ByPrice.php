<?php

global $xcart_dir;
include_once $xcart_dir.'/reports/models/models.php';
include_once $xcart_dir.'/reports/rendrers/rendrers.php';
include_once REP_DIR.'/models/date_calculation.php';

class CatalogSKUSnapshotReportByPriceModel extends ReportModel {

	public function __construct(){
		parent::__construct();
		$this->name="CatalogSKUPriceReport:" . date("j-M-Y");

	}
	protected function initFilters(){
		$this->addFilter("Date:" ,self::$FILTER_TYPE_DATE);
	}

	public function loadDefaultFilterValues(){
		$time=time();
		$this->setFilterValue("Date:", date("m/d/Y",$time));
	}


	public function buildReport(){
		global $weblog;
		$table= new Table();
		$group= new TableGroup("Catalog SKU Report By Price");
		$superHeaders= array('Price Range'=>1,'Total # Articles'=>1,'# Articles (Full Set)'=>1,'# Articles (Broken Set)'=>1,
		'# Articles (No SKU)'=>1,'# SKUs - Current'=>2, 'Current Inventory Count'=>1,'# SKUs - Last 7 Days'=>2,'# SKUs - Last 30 Days'=>2,'# Articles Added – Last 7 Days '=>1,'# Articles Added – Last 30 Days'=>1);
		$headers= array('','','','',
		'','Active','In Active', '','Enabled','Disabled','Enabled','Disabled','','');
		$table->setFooterVisible(true);
		$filtervalue = $this->getFilterValue("Date:");
		$date_split=split('/', $filtervalue);
		$current_month=(INT)$date_split[0];
		$current_year=(INT)$date_split[2];
		$current_date=(INT)$date_split[1];
		$ReportData;
		
		$today= new FDate($current_date, $current_month, $current_year);
		$last7thDay= $today->addDays(-7);
		$today= new FDate($current_date, $current_month, $current_year);
		$last30thDay=$today->addDays(-30);
		$today= new FDate($current_date, $current_month, $current_year);
		
		// gives availablity of skus
		$sql = "select  (case true when a.Price between 0 and 499 then '0000-0500' when a.Price between 500 and 999 then '0500-0999' when a.Price between 1000 and 1499 then '1000-1499' 
               when a.Price between 1500 and 1999 then '1500-1999' when a.Price between 2000 and 2499 then '2000-2499' when a.Price between 2500 and 2999 then '2500-2999' 
               when a.Price between 3000 and 3999 then '3000-3999' when a.Price between 4000 and 4999 then '4000-4999' when a.Price between 5000 and 5999 then '5000-5999'
               else '6000+' end) as priceRange, count(a.style) as availability, a.availability_type from
               (select (select price from mk_product_style where id=osm.style_id) as Price, style_id as style,
               (case true when(select count(id) from mk_product_options where style=osm.style_id) = 
			   (select count(id) from mk_product_options where style=osm.style_id and is_active=0) then 'NO SKU' 
        	   when (select count(id) from mk_product_options where style=osm.style_id) = 
               (select count(id) from mk_product_options where style=osm.style_id and is_active=1) then 'FULL SET'
          	   else 'BROKEN SET' end) as availability_type
               from mk_styles_options_skus_mapping osm join mk_product_options po on osm.option_id=po.id join mk_skus s on osm.sku_id=s.id 
               group by osm.style_id) a group by PriceRange, availability_type";
		$result = func_query($sql, true);
		
		foreach ($result as $row){
			$ReportData[$row['priceRange']]['availability'][$row['availability_type']]=(float)($row['availability']);
		}
		
		$sql="select (case true when a.Price between 0 and 499 then '0000-0500' when a.Price between 500 and 999 then '0500-0999' when a.Price between 1000 and 1499 then '1000-1499' 
               when a.Price between 1500 and 1999 then '1500-1999' when a.Price between 2000 and 2499 then '2000-2499' when a.Price between 2500 and 2999 then '2500-2999' 
               when a.Price between 3000 and 3999 then '3000-3999' when a.Price between 4000 and 4999 then '4000-4999' when a.Price between 5000 and 5999 then '5000-5999'
               else '6000+' end) as priceRange, count(*) as numbers, a.active from
               (select (select price from mk_product_style where id=osm.style_id) as Price, po.is_active as active
                from mk_styles_options_skus_mapping osm join mk_product_options po on osm.option_id=po.id join mk_skus s on osm.sku_id=s.id) a group by PriceRange, active";
		
		$result = func_query($sql, true);
		
		foreach ($result as $row){
			$ReportData[$row['priceRange']]['active'][$row['active']]=(float)($row['numbers']);
		}
		
			$sql = "select (case true when a.Price between 0 and 499 then '0000-0500' when a.Price between 500 and 999 then '0500-0999' when a.Price between 1000 and 1499 then '1000-1499' 
	               when a.Price between 1500 and 1999 then '1500-1999' when a.Price between 2000 and 2499 then '2000-2499' when a.Price between 2500 and 2999 then '2500-2999' 
	               when a.Price between 3000 and 3999 then '3000-3999' when a.Price between 4000 and 4999 then '4000-4999' when a.Price between 5000 and 5999 then '5000-5999'
	               else '6000+' end) as priceRange, sum(a.invCount) as num from
	               (select (select price from mk_product_style where id=osm.style_id) as Price, cic.inv_count as invCount
	                from mk_styles_options_skus_mapping osm join core_inventory_counts cic on osm.sku_id=cic.sku_id) a group by PriceRange";

		$result = func_query($sql, true);
		
		foreach ($result as $row){
			$ReportData[$row['priceRange']]['invCount']=(float)$row['num'];
		}
		
		$sql="select (case true when a.Price between 0 and 499 then '0000-0500' when a.Price between 500 and 999 then '0500-0999' when a.Price between 1000 and 1499 then '1000-1499' 
               when a.Price between 1500 and 1999 then '1500-1999' when a.Price between 2000 and 2499 then '2000-2499' when a.Price between 2500 and 2999 then '2500-2999' 
               when a.Price between 3000 and 3999 then '3000-3999' when a.Price between 4000 and 4999 then '4000-4999' when a.Price between 5000 and 5999 then '5000-5999'
               else '6000+' end) as priceRange, count(*) as numbers, a.enabled from
				(select (select price from mk_product_style where id=osm.style_id) as Price, sl.value as enabled
 				from 
			mk_styles_options_skus_mapping osm join mk_product_options po on osm.option_id=po.id join mk_skus s on osm.sku_id=s.id join mk_skus_logs sl on s.id = sl.skuid
			where sl.reasonid in ('14','15','16','17') and sl.lastmodified between '".$last7thDay->toUnixDateDayStart()."' and '".$today->toUnixDateDayStart()."' ) a   group by PriceRange, enabled";
		
		$result = func_query($sql, true);
		
		foreach ($result as $row){
			$ReportData[$row['priceRange']]['enabled-disabled']['7days'][$row['enabled']]=(float)($row['numbers']);
		}
		
		$sql="select (case true when a.Price between 0 and 499 then '0000-0500' when a.Price between 500 and 999 then '0500-0999' when a.Price between 1000 and 1499 then '1000-1499' 
               when a.Price between 1500 and 1999 then '1500-1999' when a.Price between 2000 and 2499 then '2000-2499' when a.Price between 2500 and 2999 then '2500-2999' 
               when a.Price between 3000 and 3999 then '3000-3999' when a.Price between 4000 and 4999 then '4000-4999' when a.Price between 5000 and 5999 then '5000-5999'
               else '6000+' end) as priceRange, count(*) as numbers, a.enabled from
				(select (select price from mk_product_style where id=osm.style_id) as Price, sl.value as enabled
 				from 
			mk_styles_options_skus_mapping osm join mk_product_options po on osm.option_id=po.id join mk_skus s on osm.sku_id=s.id join mk_skus_logs sl on s.id = sl.skuid
			where sl.reasonid in ('14','15','16','17') and sl.lastmodified between '".$last30thDay->toUnixDateDayStart()."' and '".$today->toUnixDateDayStart()."' ) a   group by PriceRange, enabled";
		
		$result = func_query($sql, true);
		
		foreach ($result as $row){
			$ReportData[$row['priceRange']]['enabled-disabled']['30days'][$row['enabled']]=(float)($row['numbers']);
		}
		
		$sql="select (case true when a.Price between 0 and 499 then '0000-0500' when a.Price between 500 and 999 then '0500-0999' when a.Price between 1000 and 1499 then '1000-1499' 
               when a.Price between 1500 and 1999 then '1500-1999' when a.Price between 2000 and 2499 then '2000-2499' when a.Price between 2500 and 2999 then '2500-2999' 
               when a.Price between 3000 and 3999 then '3000-3999' when a.Price between 4000 and 4999 then '4000-4999' when a.Price between 5000 and 5999 then '5000-5999'
               else '6000+' end) as priceRange, count(a.style) as numbers from
               (select (select price from mk_product_style where id=osm.style_id) as Price, sp.style_id as style
                  from 
                 mk_styles_options_skus_mapping osm join mk_product_options po on osm.option_id=po.id join mk_skus s on osm.sku_id=s.id join mk_style_properties sp on osm.style_id=sp.style_id
                                where sp.global_attr_catalog_add_date between unix_timestamp('".$last7thDay->toUnixDateDayStart()."') and unix_timestamp('".$today->toUnixDateDayStart()."')) a group by PriceRange";
		
		
		$result = func_query($sql, true);
		
		foreach ($result as $row){
			$ReportData[$row['priceRange']]['added']['7days']=(float)($row['numbers']);
		}
		$sql="select (case true when a.Price between 0 and 499 then '0000-0500' when a.Price between 500 and 999 then '0500-0999' when a.Price between 1000 and 1499 then '1000-1499' 
               when a.Price between 1500 and 1999 then '1500-1999' when a.Price between 2000 and 2499 then '2000-2499' when a.Price between 2500 and 2999 then '2500-2999' 
               when a.Price between 3000 and 3999 then '3000-3999' when a.Price between 4000 and 4999 then '4000-4999' when a.Price between 5000 and 5999 then '5000-5999'
               else '6000+' end) as priceRange, count(a.style) as numbers from
               (select (select price from mk_product_style where id=osm.style_id) as Price, sp.style_id as style
                  from 
                 mk_styles_options_skus_mapping osm join mk_product_options po on osm.option_id=po.id join mk_skus s on osm.sku_id=s.id join mk_style_properties sp on osm.style_id=sp.style_id
                                where sp.global_attr_catalog_add_date between unix_timestamp('".$last30thDay->toUnixDateDayStart()."') and unix_timestamp('".$today->toUnixDateDayStart()."')) a group by PriceRange";
		
		$result = func_query($sql, true);
		
		foreach ($result as $row){
			$ReportData[$row['priceRange']]['added']['30days']=(float)($row['numbers']);
		}
		
		$table->setHeaders($headers);
		$table->setSuperHeaders($superHeaders);
		$i=0;
		foreach($ReportData as $range => $info){
			$j=0;
			$table->setCellData($i, $j++, $range);
			$table->setCellData($i, $j++, $info['availability']['NO SKU']+$info['availability']['FULL SET']+$info['availability']['BROKEN SET']);
			$table->setCellData($i, $j++, $info['availability']['FULL SET']);
			$table->setCellData($i, $j++, $info['availability']['BROKEN SET']);
			$table->setCellData($i, $j++, $info['availability']['NO SKU']);
			$table->setCellData($i, $j++, $info['active']['1']);
			$table->setCellData($i, $j++, $info['active']['0']);
			$table->setCellData($i, $j++, $info['invCount']);
			$table->setCellData($i, $j++, $info['enabled-disabled']['7days']['true']);
			$table->setCellData($i, $j++, $info['enabled-disabled']['7days']['false']);
			$table->setCellData($i, $j++, $info['enabled-disabled']['30days']['true']);
			$table->setCellData($i, $j++, $info['enabled-disabled']['30days']['false']);
			$table->setCellData($i, $j++, $info['added']['7days']);
			$table->setCellData($i, $j++, $info['added']['30days']);
			$i++;
		}
		
		$group->addTable($table);
		$this->addTableGroup($group);
		$this->isBuild=true;
		$this->notes = "Full Set – All size options are are available in inventory<br>
			Broken Set – Only some size options are available in inventory<br>
			No SKU – No size options are available in inventory<br>
			Enabled and Disabled counts - The number of distinct SKUs which have been enabled  and disabled in the past 7 days/30 days<br>
			#Articles Added –  “Product Styles” will henceforth be known as “Articles”. The total number of new styles added in the catalog in the last 7 days and last 30 days will form the contents of these columns. ";
	}


}
?>

