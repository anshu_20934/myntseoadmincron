<?php
define("REP_DIR",$xcart_dir."/reports");
class ReportModelFactory{

	public static $REPORT_GROUP=0;
	public static $RTO_RETURNS_REPORT=1;
	public static $REPEAT_BUYERS_REPORT=2;
	public static $CATALOG_STATUS_REPORT=3;
	public static $OPERATION_SLA_REPORT=4;
	public static $OPS_SLA_REPORT=5;
	public static $RECOVERY_REPORT=6;
	public static $COD_DAILY_ADMIN_REPORT=7;
	public static $COD_MONTHLY_ADMIN_REPORT=8;
	public static $COD_QUARTERLY_ADMIN_REPORT=9;
	public static $GO_LIVE_REPORT=10;
	public static $INVENTORY_REPORT=11;
	public static $MRP_REPORT=12;
	public static $BRAND_SKU_SELL_THROUGH_REPORT=13;
	public static $MRP_TRACKING_REPORT=14;
	public static $DAILY_CUSTOMER_ACTIVITY_REPORT=15;
	public static $DAILY_CUSTOMER_STATS_REPORT=16;
	public static $DAILY_CUSTOMER_REGS_REPORT=17;
	public static $CATALOG_SKU_SNAPSHOT_REPORT_BY_PRICE=18;
	public static $BRANDWISE_SALES_REPORT=19;
	public static $COD_STATS_REPORT=20;
	public static $CUSTOMER_STATS_REPORT=21;
	public static $CATALOG_SKU_SNAPSHOT_REPORT=22;
	public static $SKU_SELL_THROUGH_REPORT=24;
	public static $BRANDWISE_INVENTORY_REPORT=25;
	public static $CATEGORYWISE_SALES_REPORT=23;
	public static $PAYMENTS_OPTIONS_PERMORMANCE_REPORT=26;
	public static $EOSS_REPORT=27;
	public static $ADMIN_OPERATIONS_SLA_REPORT=28;
	public static $ADMIN_OPS_SLA_REPORT=29;
	public static $CATEGORY_SKU_SNAPSHOT_REPORT=30;
	public static $REPEAT_CUSTOMER_REPORT=31;
	public static $CUSTOMER_EXPERIENCE_REPORT=32;
	public static $CONSOLIDATED_SALES_STOCKS_REPORT=33;
    public static $SLA_RETURNS_REPORT_OPs=34;
	public static $SLA_RETURNS_REPORT_CC=35;
	public static $DELIVERY_DATA_REPORT=36;
	public static $ADMIN_RETURNS_SLA_REPORT=37;
	public static $ADMIN_RTO_SLA_REPORT=38;
	public static $RETURNS_SLA_REPORT=39;
	public static $RTO_SLA_REPORT=40;
	
	public function createModel($modelkey){
		global $xcart_dir;
		switch($modelkey){
			case self::$REPORT_GROUP;{
				include_once REP_DIR.'/models/report_group.php';
				global $emailsToList;
				return new ReportGroupModel();
			}
			break;
			case self::$RTO_RETURNS_REPORT;{
				include_once REP_DIR.'/models/RTO_Returns_Report.php';
				global $emailsToList;
				if(!isset($emailsToList))
				$emailsToList="RTO_RETURNS_REPORT_EMAILS";
				return new RTOReturnsReportModel();
			}
			break;
			case self::$REPEAT_BUYERS_REPORT;{
				include_once REP_DIR.'/models/repeat_buyers_report.php';
				global $emailsToList;
				if(!isset($emailsToList))
				$emailsToList="REPEAT_BUYERS_REPORT_EMAILS";
				return new RepeatBuyersReportModel();
			}
			break;
			case self::$CATALOG_STATUS_REPORT;{
				include_once REP_DIR.'/models/Catalog_Status_Report.php';
				global $emailsToList;
				if(!isset($emailsToList))
				$emailsToList="REPEAT_BUYERS_REPORT_EMAILS";
				return new CatalogStatusReportModel();
			}
			break;
			case self::$OPERATION_SLA_REPORT;{
				include_once REP_DIR.'/models/operations_report_model.php';
				global $emailsToList;
				if(!isset($emailsToList))
				$emailsToList="OPERATIONS_REPORT_TO_EMAILS";
				return new OperationOverALLSLAReportModel();
			}
			break;
			case self::$OPS_SLA_REPORT;{
				include_once REP_DIR.'/models/operations_report_model.php';
				global $emailsToList;
				if(!isset($emailsToList))
				$emailsToList="OPERATIONS_REPORT_TO_EMAILS";
				return new OperationSLAReportModel();
			}
			break;
			case self::$RECOVERY_REPORT;{
				include_once REP_DIR.'/models/Recovery_LostOrders.php';
				global $emailsToList;
				global $report_mode;
				if(!isset($emailsToList))
				$emailsToList="LOST_ORDERS_TO_EMAIL";
				if(!isset($report_mode))
				$report_mode=1;
				return new RecoveryReportModel($report_mode);
			}
			break;
			case self::$COD_DAILY_ADMIN_REPORT;{
				include_once REP_DIR.'/models/cod_daily_report.php';
				global $emailsToList;
				if(!isset($emailsToList))
				$emailsToList="COD_REPORT_TO_EMAILS";
				return new CodDailyReportModel();
			}
			break;
			case self::$COD_MONTHLY_ADMIN_REPORT;{
				include_once REP_DIR.'/models/cod_monthly_report.php';
				global $emailsToList;
				if(!isset($emailsToList))
				$emailsToList="COD_REPORT_TO_EMAILS";
				return new CodMonthlyReportModel();
			}
			break;
			case self::$COD_QUARTERLY_ADMIN_REPORT;{
				include_once REP_DIR.'/models/cod_quarterly_report.php';
				global $emailsToList;
				if(!isset($emailsToList))
				$emailsToList="COD_REPORT_TO_EMAILS";
				return new CodQuarterlyReportModel();
			}
			break;
			case self::$GO_LIVE_REPORT;{
				include_once REP_DIR.'/models/GoLiveTime_Report.php';
				global $emailsToList;
				if(!isset($emailsToList))
				$emailsToList="GO_LIVE_REPORT_TO_EMAILS";
				return new GoLiveReportModel();
			}
			break;
			case self::$INVENTORY_REPORT;{
				include_once REP_DIR.'/models/Inventory_Report.php';
				global $emailsToList;
				if(!isset($emailsToList))
				$emailsToList="SKU_SELL_THROUGH_TO_EMAILS";
				return new InventoryReportModel();
			}
			break;
			case self::$MRP_REPORT;{
				include_once REP_DIR.'/models/MRP_Report.php';
				global $emailsToList;
				if(!isset($emailsToList))
				$emailsToList="MRP_REPORT_TO_EMAILS";
				return new MRPReportModel();
			}
			break;
			case self::$BRAND_SKU_SELL_THROUGH_REPORT;{
				include_once REP_DIR.'/models/Brand-SKU-Wise_Sell_Through_Report.php';
				global $emailsToList;
				global $brand;
				if(!isset($emailsToList))
				$emailsToList="BRAND_SKU_REPORT_TO_EMAILS";
				return new BrandSKUSellThroughReportModel($brand);
			}
			break;
			case self::$MRP_TRACKING_REPORT;{
				include_once REP_DIR.'/models/MRP_Tracking_Report.php';
				global $emailsToList;
				if(!isset($emailsToList))
				$emailsToList="MRP_REPORT_TO_EMAILS";
				return new MRPTrackingReportModel();
			}
			break;
			case self::$DAILY_CUSTOMER_ACTIVITY_REPORT;{
				include_once REP_DIR.'/models/daily_customer_report_model.php';
				global $emailsToList;
				if(!isset($emailsToList))
				$emailsToList="EMAIL_ONLINE_REPORT_TO_EMAILS";
				return new DailyCustomerActivityReportModel();
			}
			break;
			case self::$DAILY_CUSTOMER_STATS_REPORT;{
				include_once REP_DIR.'/models/daily_customer_report_model.php';
				global $emailsToList;
				if(!isset($emailsToList))
				$emailsToList="EMAIL_ONLINE_REPORT_TO_EMAILS";
				return new CustomerStatsReportModel();
			}
			break;
			case self::$DAILY_CUSTOMER_REGS_REPORT;{
				include_once REP_DIR.'/models/daily_customer_report_model.php';
				global $emailsToList;
				if(!isset($emailsToList))
				$emailsToList="EMAIL_ONLINE_REPORT_TO_EMAILS";
				return new RegistrationsReportModel();
			}
			break;
			case self::$CATALOG_SKU_SNAPSHOT_REPORT_BY_PRICE;{
				include_once REP_DIR.'/models/Catalog_SKU_Snapshot_Report_ByPrice.php';
				global $emailsToList;
				if(!isset($emailsToList))
				$emailsToList="SKU_SNAP_TO_EMAILS";
				return new CatalogSKUSnapshotReportByPriceModel();
			}
			break;
			case self::$BRANDWISE_SALES_REPORT;{
				include_once REP_DIR.'/models/brandwise_sales_report.php';
				global $emailsToList;
				if(!isset($emailsToList))
				$emailsToList="EMAIL_BRANDWISE_REPORT_TO_EMAILS";
				return new BrandWiseSalesReportModel();
			}
			break;
			case self::$COD_STATS_REPORT;{
				include_once REP_DIR.'/models/cod_stats_report.php';
				global $emailsToList;
				if(!isset($emailsToList))
				$emailsToList="COD_REPORT_TO_EMAILS";
				return new CodStatsReportModel();
			}
			break;
			case self::$CUSTOMER_STATS_REPORT;{
				include_once REP_DIR.'/models/customer_stats_report.php';
				global $emailsToList;
				if(!isset($emailsToList))
				$emailsToList="EMAIL_ONLINE_REPORT_TO_EMAILS";
				return new CustomerStatsReportModel();
			}
			break;
			case self::$CATALOG_SKU_SNAPSHOT_REPORT;{
				include_once REP_DIR.'/models/Catalog_SKU_Snapshot_Report.php';
				global $emailsToList;
				if(!isset($emailsToList))
				$emailsToList="SKU_SNAP_TO_EMAILS";
				return new CatalogSKUSnapshotReportModel();
			}
			break;
			case self::$SKU_SELL_THROUGH_REPORT;{
				include_once REP_DIR.'/models/SKU-Wise_Sell_Through_Report.php';
				global $emailsToList;
				if(!isset($emailsToList))
				$emailsToList="SKU_SELL_THROUGH_TO_EMAILS";
				return new SKUSellThroughReportModel();
			}
			break;
			case self::$BRANDWISE_INVENTORY_REPORT;{
				include_once REP_DIR.'/models/Brandwise_Inventory_Report.php';
				global $emailsToList;
				if(!isset($emailsToList))
				$emailsToList="SKU_SELL_THROUGH_TO_EMAILS";
				return new BrandwiseInventoryReportModel();
			}
			break;
			case self::$CATEGORYWISE_SALES_REPORT;{
				include_once REP_DIR.'/models/categorywise_sales_report.php';
				global $emailsToList;
				if(!isset($emailsToList))
				$emailsToList="EMAIL_BRANDWISE_REPORT_TO_EMAILS";
				return new CategoryWiseSalesReportModel();
			}
			break;
			case self::$PAYMENTS_OPTIONS_PERMORMANCE_REPORT;{
				include_once REP_DIR.'/models/PaymentOptionsPerformance_Report.php';
				global $emailsToList;
				if(!isset($emailsToList))
				$emailsToList="PAYMENTS_OPTIONS_PERMORMANCE_REPORT_TO_EMAILS";
				return new PaymentOptionsPerformance();
			}
			break;
			case self::$EOSS_REPORT;{
				include_once REP_DIR.'/models/EOSS_Report.php';
				global $emailsToList;
				if(!isset($emailsToList))
				$emailsToList="EOSS_REPORT_EMAILS";
				return new EOSSReportModel();
			}
			break;
			case self::$ADMIN_OPERATIONS_SLA_REPORT;{
				include_once REP_DIR.'/models/operations_dashboard_report.php';
				global $emailsToList;
				if(!isset($emailsToList))
				$emailsToList="OPERATIONS_REPORT_TO_EMAILS";
				return new OperationOverALLSLADashboardReport();
			}
			break;
			case self::$ADMIN_OPS_SLA_REPORT;{
				include_once REP_DIR.'/models/operations_dashboard_report.php';
				global $emailsToList;
				if(!isset($emailsToList))
				$emailsToList="OPERATIONS_REPORT_TO_EMAILS";
				return new OperationSLADashboardReport();
			}
			break;
			case self::$CATEGORY_SKU_SNAPSHOT_REPORT;{
				include_once REP_DIR.'/models/Category_SKU_Snapshot_Report.php';
				global $emailsToList;
				if(!isset($emailsToList))
				$emailsToList="SKU_SNAP_TO_EMAILS";
				return new CategorySKUSnapshotReportModel();
			}
			break;
			case self::$REPEAT_CUSTOMER_REPORT;{
				include_once REP_DIR.'/models/repeat_customer_analysis_report.php';
				global $emailsToList;
				if(!isset($emailsToList))
				$emailsToList="REPEAT_CUSTOMER_REPORT_TO_EMAILS";
				return new RepeatCustomerAnalysisReportModel();
			}
			break;
			case self::$CUSTOMER_EXPERIENCE_REPORT;{
				include_once REP_DIR.'/models/Customer_Experience_Report.php';
				global $emailsToList;
				if(!isset($emailsToList))
				$emailsToList="BI_VALIDATE_TO_EMAILS";
				return new CustomerExperienceReportModel();
			}
			break;
			case self::$CONSOLIDATED_SALES_STOCKS_REPORT;{
				include_once REP_DIR.'/models/ConsolidatedStocksAndSales_Report.php';
				global $emailsToList;
				if(!isset($emailsToList))
				$emailsToList="REPEAT_CUSTOMER_REPORT_TO_EMAILS"; // This has rpt_b2conline@myntra.com
				return new ConsolidatedStocksAndSalesReportModel();
			}
			break;
            case self::$SLA_RETURNS_REPORT_OPs;{
				include_once REP_DIR.'/models/SLA_Returns_Report.php';
				global $emailsToList;
				if(!isset($emailsToList))
				$emailsToList="SLA_RETURNS_REPORT_OPs";
				return new SLAReturnsReportModelToOP();
			}
			break;
            case self::$SLA_RETURNS_REPORT_CC;{
				include_once REP_DIR.'/models/SLA_Returns_Report_CC.php';
				global $emailsToList;
				if(!isset($emailsToList))
				$emailsToList="SLA_RETURNS_REPORT_CC"; 
				return new SLAReturnsReportModelToCC();
			}
			break;
			case self::$DELIVERY_DATA_REPORT;{
				include_once REP_DIR.'/models/DeliveryData_Report.php';
				global $emailsToList;
				if(!isset($emailsToList))
				$emailsToList="DELIVERY_DATA_REPORT_TO_EMAILS";
				return new DeliveryDataReportModel();
			}
			break;
			case self::$RETURNS_SLA_REPORT;{
				include_once REP_DIR.'/models/operations_report_model.php';
				global $emailsToList;
				if(!isset($emailsToList))
					$emailsToList="OPERATIONS_REPORT_TO_EMAILS";
				return new ReturnsSLAReportModel();
			}
			break;
			case self::$RTO_SLA_REPORT;{
				include_once REP_DIR.'/models/operations_report_model.php';
				global $emailsToList;
				if(!isset($emailsToList))
					$emailsToList="OPERATIONS_REPORT_TO_EMAILS";
				return new RTOSLAReportModel();
			}
			break;
			case self::$ADMIN_RETURNS_SLA_REPORT;{
				include_once REP_DIR.'/models/operations_dashboard_report.php';
				global $emailsToList;
				if(!isset($emailsToList))
					$emailsToList="OPERATIONS_REPORT_TO_EMAILS";
				return new ReturnsSLADashboardReport();
			}
			break;
			case self::$ADMIN_RTO_SLA_REPORT;{
				include_once REP_DIR.'/models/operations_dashboard_report.php';
				global $emailsToList;
				if(!isset($emailsToList))
					$emailsToList="OPERATIONS_REPORT_TO_EMAILS";
				return new RTOSLADashboardReport();
			}
			break;
			default:
				echo "SOME ERROR: Model Not Available";
		}
	}
}
