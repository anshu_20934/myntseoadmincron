<?php
define("REPORTS_CONNECTION",true);
include "../admin/auth.php";
//require_once $xcart_dir."/include/security.php";
include_once $xcart_dir."/reports/models/date_calculation.php";
include_once $xcart_dir."/include/func/func.sms_alerts.php";
if(!defined('DISPLAY_XPM4_ERRORS'))
define('DISPLAY_XPM4_ERRORS', true);
require_once $xcart_dir.'/XPM/MAIL.php';


function format($number, $decimals ,$format=true) {
		if(!$format)
		return round($number,$decimals);
		$format=number_format($number, $decimals, '.', ',');
		$strsplit=split(',',$format);
		$out=$strsplit[count($strsplit)-1];
		$icount=0;
		for($i=count($strsplit)-2;$i>=0;$i--){
			for($j=strlen($strsplit[$i])-1;$j>=0;$j--,$icount++){
				if($icount%2==0){
					$out=','.$out;
				}
				$out=$strsplit[$i][$j].$out;
				
			}
		}
		return $out;
}	
$phone_nums=constant("ORDERS_HOURLY_ALERT");
$phones=split(',', $phone_nums);
$dateslash = date("m/d/Y");
$date_split=split('/', $dateslash);
$current_month=(INT)$date_split[0];
$current_year=(INT)$date_split[2];
$current_date=(INT)$date_split[1];
$curr_fdate= new FDate($current_date, $current_month, $current_year);


$sql="select  count(*) total,sum(total+gift_charges+shipping_cost) revenue from xcart_orders where date > unix_timestamp('".$curr_fdate->toUnixDateDayStart()."') and status in('C','Q','WP','OH','SH','DL')";

$result =func_query_first($sql, true);
$msg=date("d/m/Y h:i:s A");
if(empty($result)|| $result['total']==0){
	$msg.=" ZERO Orders";
	
}else{
	$msg.=" #Orders:" . $result['total'];
	$msg.=" Sales:".format($result['revenue'],0)."/-";
}

foreach ($phones as $num){
	func_send_sms_cellnext($num, $msg);
}


$m = new MAIL;
$m->From("support@myntra.com", "Myntra Reporting Service");
			

$m->addto(constant('TEMP_GMAIL_ROUTING_EMAIL'), "");

$time=time();
$m->Subject("Hourly Order Alert:".date("m/d/Y h:i:s A ",$time)."O#:$result[total] S:".format($result['revenue'],0)."/-");
$m->Html($msg);	
$mailresult=$m->Send('client') ? 'Mail sent !' : 'Error';

if($mailresult=='Error'){
	$mailmsg= "Report::Hourly SMS Report\rE-mail could not be sent due to some bad configuration ";
}
else{
	$mailmsg="Report::Hourly SMS Report\rReport Mailed Succesfully!!!!! ";
}
echo $mailmsg;
?>
