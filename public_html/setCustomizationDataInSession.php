<?php
include_once("./auth.php");
include_once($xcart_dir."/include/func/func.mkcore.php");
include_once($xcart_dir."/include/func/func.mkmisc.php");
include_once($xcart_dir."/include/class/customization/customizationData.php");
include_once($xcart_dir."/include/class/customization/CustomizationHelper.php");

$windowId = $_GET["windowid"];
$productStyleId = $_GET["styleid"];

// init chosen Product Configuration
if( isset($XCART_SESSION_VARS["chosenProductConfigurationEx"]) ) {
	$chosenProductConfigurationEx = $XCART_SESSION_VARS["chosenProductConfigurationEx"]; // All Window's
	if( isset($chosenProductConfigurationEx[$windowid]) ) {
		// This Window's
		$thisChosenProductConfiguration = $chosenProductConfigurationEx[$windowid];
	}
}

// init customization array
if( isset($XCART_SESSION_VARS["customizationArrayEx"]) ) {
	$customizationArrayEx = $XCART_SESSION_VARS["customizationArrayEx"]; // All Window's
	if( isset($customizationArrayEx[$windowid]) ) {
		// This Window's
		$thisCustomizationArray = $customizationArrayEx[$windowid];
	}
}

// init master customization array ( please note this is a readonly strcuture and it holds the base informaton of a style, never change this)
if( isset($XCART_SESSION_VARS["masterCustomizationArrayEx"]) ) {
	$masterCustomizationArrayEx = $XCART_SESSION_VARS["masterCustomizationArrayEx"]; // All Window's
	if( isset($masterCustomizationArrayEx[$windowid]) ) {
		// This Window's
		$thisMasterCustomizationArray = $masterCustomizationArrayEx[$windowid];
	}
}


// Create customizationData builder object
$customizationData = new customizationData($productStyleId);


// Fill chosen Product Configuration for this Product Style id
$thisChosenProductConfiguration = $customizationData->initChosenProductConfigurationData($thisChosenProductConfiguration);

// Fill customization Arrays for this Product Style id
$thisCustomizationArray = $customizationData->initCustomizationArrayData($thisCustomizationArray);

// Fill Master Customization Array
$thisMasterCustomizationArray = $customizationData->initMasterCustomizationArrayData($thisMasterCustomizationArray);


// Retrieve data from customization arrays to fill smarty and for use in other functions
$globalDataArray = $XCART_SESSION_VARS["globalDataArray"];
$params = array	(	"custArray"	=>	$thisCustomizationArray,
					"masterCustomizationArray"	=>	$thisMasterCustomizationArray,
					"chosenProductConfiguration"=>	$thisChosenProductConfiguration,
					"globalDataArray"	=>	$globalDataArray
);

$customizationHelper = new CustomizationHelper($params);

// Write back all the data into session vars
$chosenProductConfigurationEx[$windowid] = $thisChosenProductConfiguration;
$customizationArrayEx[$windowid] = $thisCustomizationArray;
$masterCustomizationArrayEx[$windowid] = $thisMasterCustomizationArray;

$XCART_SESSION_VARS["chosenProductConfigurationEx"] = $chosenProductConfigurationEx;
$XCART_SESSION_VARS["customizationArrayEx"] = $customizationArrayEx;
$XCART_SESSION_VARS["masterCustomizationArrayEx"] = $masterCustomizationArrayEx;

$XCART_SESSION_VARS["globalDataArray"] = $globalDataArray;

$XCART_SESSION_VARS["customizationArrayEx"][$windowid] = $customizationHelper->getCustomizationArray();



echo "done";exit;
?>