<?php

require_once($xcart_dir."auth.php");

// Adding appropriate header message for mrp.
include_once "$xcart_dir/modules/coupon/database/CouponAdapter.php";
include_once "$xcart_dir/modules/myntCash/MyntCashService.php";
include_once "$xcart_dir/modules/myntCash/MyntCashRawService.php";
include_once "$xcart_dir/modules/loyaltyPoints/LoyaltyPointsPortalClient.php";

$couponAdapter = CouponAdapter::getInstance();
$myMyntraCredits = $couponAdapter->getTotalCredits($login);

/* fetch balance from MyntCashService  */
/* fetching it from raw service to avoid load on remote service */
$myntCashDetails = MyntCashRawService::getBalance($login);
$myMyntraCredits["cashback"] = $myntCashDetails["balance"]; 
// this to support unseen usage of $myMyntraCredits["cashback"].
// we should deprecate the usage of this variable. 
// instead, we should use $myntCashDetails["balance"].

$response["myntCashDetails"] = $myntCashDetails;
$response["myMyntraCredits"] = $myMyntraCredits;

if($loyaltyEnabled){
	$loyaltyPointsDetails = LoyaltyPointsPortalClient::getAccountInfo($login);
	$response["loyaltyPointsDetails"] = $loyaltyPointsDetails;
}

$response['status']['statusType'] = 'SUCCESS';
header("Content-Type:application/json");
echo json_encode($response);
exit;

?>