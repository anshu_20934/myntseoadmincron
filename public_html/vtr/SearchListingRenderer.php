<?php

class SearchListingRenderer extends BaseRenderer {
    public function render() {
        global $smarty;
        $smarty->assign('products', $this->data);
        $this->markup = $smarty->fetch('vtr/listing.tpl');
    }
}

