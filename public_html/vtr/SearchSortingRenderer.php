<?php

class SearchSortingRenderer extends BaseRenderer {
    public function render() {
        global $smarty;
        $options = array();
        foreach ($this->data['options'] as $opt => $txt) {
            if ($opt == 'PRICE') {
                $opt = $this->data['selected'] == 'PRICEA' ? 'PRICED' : 'PRICEA';
            }
            $options[] = array('val' => $opt, 'url' => $this->qb->makeSortUrl($opt), 'txt' => $txt);
        }

        $optSelected = $this->data['selected'];
        if ($optSelected == 'PRICEA' || $optSelected == 'PRICED') {
            $optSelected = 'PRICE';
        }
        if (empty($optSelected)) {
            list($optSelected, $optSelectedText) = each($this->data['options']);
        }
        else {
            $optSelectedText = $this->data['options'][$optSelected];
        }

        $smarty->assign('sortOptions', $options);
        $smarty->assign('sortSelected', $this->data['selected']);
        $smarty->assign('sortSelectedText', $optSelectedText);
        $this->markup = $smarty->fetch('vtr/sorting.tpl');
    }
}

