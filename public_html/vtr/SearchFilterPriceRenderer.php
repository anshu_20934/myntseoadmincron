<?php

class SearchFilterPriceRenderer extends SearchFilterRenderer {
    protected $lowest;
    protected $highest;
    public function setOptions($values) {
        $this->lowest = $values['rangeMin'];
        $this->highest = $values['rangeMax'];
        $this->options = array();
        foreach ($values['breakUps'] as $breakUp) {
            list($url, $selected) = $this->qb->makePriceUrl($this->name, $breakUp['rangeMin'], $breakUp['rangeMax']);
            $this->options[] = array($breakUp['rangeMin'], $breakUp['rangeMax'], $url, $selected);
        }
    }
    public function exportDataToSmarty() {
        global $smarty;
        parent::exportDataToSmarty();
        $smarty->assign('lowest', $this->lowest);
        $smarty->assign('highest', $this->highest);
    }
    public function render() {
        global $smarty;
        $this->exportDataToSmarty();
        $this->markup = $smarty->fetch('vtr/filter-price.tpl');
    }
}

