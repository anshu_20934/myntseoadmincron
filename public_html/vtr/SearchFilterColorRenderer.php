<?php

class SearchFilterColorRenderer extends SearchFilterRenderer {
    protected $colorSwatchMap;
    public function setColorSwatchMap($map) {
        $this->colorSwatchMap = $map;
    }
    public function exportDataToSmarty() {
        global $smarty;
        parent::exportDataToSmarty();
        $smarty->assign('colorSwatchMap', $this->colorSwatchMap);
    }
    public function render() {
        global $smarty;
        $this->exportDataToSmarty();
        $this->markup = $smarty->fetch('vtr/filter-color.tpl');
    }
}

