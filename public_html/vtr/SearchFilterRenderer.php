<?php

class SearchFilterRenderer extends BaseRenderer {
    protected $title;
    protected $name;
    protected $options;
    protected $hasSearchBox;
    public function setTitle($title) {
        $this->title = $title;
    }
    public function setName($name) {
        $this->name = $name;
    }
    public function setQueryBuilder($qb) {
        $this->qb = $qb;
    }
    public function addSearchBox() {
        $this->hasSearchBox = true;
    }
    public function setOptions($values) {
        $this->options = array();
        foreach ($values as $val => $cnt) {
            list ($url, $selected) = $this->qb->makeFilterUrl($this->name, $val);
            $this->options[] = array($val, $cnt, $url, $selected);
        }
    }
    public function exportDataToSmarty() {
        global $smarty;
        $smarty->assign('filterName', $this->name);
        $smarty->assign('filterTitle', $this->title);
        $smarty->assign('filterOptions', $this->options);
        $smarty->assign('hasSearchBox', $this->hasSearchBox);
    }
    public function render() {
        global $smarty;
        $this->exportDataToSmarty();
        $this->markup = $smarty->fetch('vtr/filter-default.tpl');
    }
}

