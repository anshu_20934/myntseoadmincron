<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/auth.php';
require_once 'UIManager.php';

$httpHeaders = getallheaders();
$isXHR = isset($httpHeaders['X-Requested-With']) && $httpHeaders['X-Requested-With'] == 'XMLHttpRequest';
$smarty->assign('isXHR', $isXHR);

$manager = UIManager::getInstance();
if (!$isXHR) {
    $params = array(
        'torso' => $manager->getTorsoId()
    );
    $data = array(
        'errors'      => $manager->getErrors(),
        'layers'      => $manager->getLayers(),
        'styleIds'    => $manager->getStyleIds(),
        'faces'       => $manager->getFaces(),
        'views'       => $manager->getViews(),
        'viewId'      => $manager->getViewId(),
        'backgrounds' => $manager->getBackgrounds(),
        'uniqueUrl'   => $manager->getUniqueUrl(),
        'name'        => $manager->getName(),
        'bgUrl'       => 'http://myntra.myntassets.com/skin2/images/room/background_default_yellow.jpg'
    );
    //header('Content-Type:text/plain'); print_r($params); print_r($data); exit;
    $smarty->assign('torsoTypes', $manager->getTorsoTypes());
    $smarty->assign('torsoType', $manager->getTorsoType());
    $smarty->assign('jsonParams', json_encode($params));
    $smarty->assign('jsonData', json_encode($data));
    $smarty->assign('markupRoom', $smarty->fetch('vtr/room.tpl'));
    $smarty->assign('scriptRoom', $smarty->fetch('vtr/script-room.tpl'));
}
else {
    $response = array();
    $response['status'] = 'SUCCESS';
    //$response['session'] = $manager->getSession();
    $_action = filter_var($_POST['_action'], FILTER_SANITIZE_STRING);
    $id = filter_var($_POST['id'], FILTER_SANITIZE_NUMBER_INT);

    if ($_action == 'changeview') {
        $manager->changeView($id);
        $response['viewId'] = $manager->getViewId();
    }
    else if ($_action == 'changeface') {
        $manager->changeFace($id);
    }
    else if ($_action == 'changebackground') {
        $response['bgUrl'] = $manager->changeBackground($id);
    }
    else if ($_action == 'addstyle') {
        $manager->addStyle($id);
    }
    else if ($_action == 'removestyle') {
        $manager->removeStyle($id);
    }
    else if ($_action == 'new') {
        $manager->newMannequin();
        $response['viewId'] = $manager->getViewId();
    }
    else if ($_action == 'reset') {
        $manager->reset();
    }
    else if ($_action == 'undo') {
        $manager->undo();
    }
    else if ($_action == 'redo') {
        $manager->redo();
    }
    else if ($_action == 'save') {
        $title = filter_var($_POST['title'], FILTER_SANITIZE_STRING);
        $manager->save($title);
        $response['name'] = $manager->getName();
    }
    else if ($_action == 'saveas') {
        $title = filter_var($_POST['title'], FILTER_SANITIZE_STRING);
        $manager->saveAs($title);
        $response['name'] = $manager->getName();
    }
    else if ($_action == 'share') {
        $manager->share();
        $response['url'] = $manager->getUniqueUrl();
    }
    else {
        $response['status'] = 'ERROR';
        $response['message'] = 'Unknown action';
    }

    if ($response['status'] == 'SUCCESS') {
        if ($_action != 'changebackground') {
            $response['layers'] = $manager->getLayers();
            $response['styleIds'] = $manager->getStyleIds();
        }
    }

    header('Content-Type:application/json');
    echo json_encode($response);
}

