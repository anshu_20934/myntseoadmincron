<?php

class SearchFiltersRenderer extends BaseRenderer {
    public function render() {
        $this->markup = '';
        foreach ($this->data as $name => $filter) {
            $values = $filter['values'];
            if (empty($values) || count($values) === 1) { continue; }

            $obj = null;
            $title = $filter['title'];
            switch ($title) {
            case 'Color':
                $obj = new SearchFilterColorRenderer();
                $obj->setColorSwatchMap($filter['colorSwatchMap']);
                break;
            case 'Price':
                $obj = new SearchFilterPriceRenderer();
                break;
            default:
                $obj = new SearchFilterRenderer();
                break;
            }

            if ($title != 'Price' && count($values) > 10) {
                $obj->addSearchBox();
            }
            $obj->setTitle($title);
            $obj->setName($name);
            $obj->setQueryBuilder($this->qb);
            $obj->setOptions($values);
            $obj->render();
            $this->markup .= $obj->getMarkup();
        }
    }
}

