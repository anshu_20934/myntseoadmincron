<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/auth.php';
require_once $xcart_dir.'/include/class/search/SearchProducts.php';
require_once 'UIManager.php';
require_once 'ProductsDataSource.php';
require_once 'QueryBuilder.php';
require_once 'BaseRenderer.php';
require_once 'SearchListingRenderer.php';
require_once 'SearchSortingRenderer.php';
require_once 'SearchFilterRenderer.php';
require_once 'SearchFilterColorRenderer.php';
require_once 'SearchFilterPriceRenderer.php';
require_once 'SearchFiltersRenderer.php';
require_once 'LookListingRenderer.php';

$httpHeaders = getallheaders();
$isXHR = isset($httpHeaders['X-Requested-With']) && $httpHeaders['X-Requested-With'] == 'XMLHttpRequest';
$smarty->assign('isXHR', $isXHR);

$manager = UIManager::getInstance();
$qb = new QueryBuilder();
$qb->setManager($manager);
$qb->init();
$params = $qb->getParams();
$source = $params['source'];
$smarty->assign('source', $source);
$manager->getProductSourceId();

if ($source == 'search') {
    $dataSource = new ProductsDataSource();
    $dataSource->setManager($manager);
    $filterQuery = $qb->getSolrFilterQuery();
    $dataSource->fetchSearchData($params['query'], $filterQuery, $params['sort'], $params['page'], $params['noofitems']);
    $data = $dataSource->getData();
}
else if ($source == 'recent') {
    $dataSource = new ProductsDataSource();
    $dataSource->setManager($manager);
    $dataSource->fetchRecentlyViewedData($params['page'], $params['noofitems']);
    $data = $dataSource->getData();
}
else if ($source == 'wish') {
    $dataSource = new ProductsDataSource();
    $dataSource->setManager($manager);
    $dataSource->fetchWishlistData($params['page'], $params['noofitems']);
    $data = $dataSource->getData();
}
else if ($source == 'look') {
    $data = $manager->getMyLooks($params['page'], $params['noofitems']);
}
//header('Content-Type:application/json'); echo json_encode($data); exit;

if (!$isXHR) {
    if ($source == 'search' || $source == 'recent' || $source == 'wish') {
        $listing = new SearchListingRenderer();
        $listing->setData($data->products);
        $listing->setQueryBuilder($qb);
        $listing->render();
        $smarty->assign('markupListing', $listing->getMarkup());
    }
    else if ($source == 'look') {
        $listing = new LookListingRenderer();
        $listing->setData($data->products);
        $listing->setQueryBuilder($qb);
        $listing->render();
        $smarty->assign('markupListing', $listing->getMarkup());
    }

    if ($source == 'search') {
        $filters = new SearchFiltersRenderer();
        $filters->setData($data->filters);
        $filters->setQueryBuilder($qb);
        $filters->render();
        $markupFilters = $filters->getMarkup();
        $smarty->assign('markupFilters', $filters->getMarkup());

        $sorting = new SearchSortingRenderer();
        $sorting->setData($data->sort);
        $sorting->setQueryBuilder($qb);
        $sorting->render();
        $smarty->assign('markupSorting', $sorting->getMarkup());
    }

    $smarty->assign('urlSourceSearch', $qb->makeSourceUrl('search'));
    $smarty->assign('urlSourceRecent', $qb->makeSourceUrl('recent'));
    $smarty->assign('urlSourceWish', $qb->makeSourceUrl('wish'));
    $smarty->assign('urlSourceLook', $qb->makeSourceUrl('look'));

    unset($data->products);
    unset($data->filters);
    unset($data->sort);
    $smarty->assign('jsonQueryFilters', json_encode($qb->getFilters()));
    $smarty->assign('jsonQueryParams', json_encode($qb->getParams()));
    $smarty->assign('jsonData', json_encode($data));
    $smarty->assign('markupProducts', $smarty->fetch('vtr/products.tpl'));
    $smarty->assign('scriptProducts', $smarty->fetch('vtr/script-products.tpl'));
}
else {
    $response = array();
    $response['status'] = 'SUCCESS';
    $_action = filter_var($_GET['_action'], FILTER_SANITIZE_STRING);

    if ($source == 'search' || $source == 'recent' || $source == 'wish') {
        $listing = new SearchListingRenderer();
    }
    else if ($source == 'look') {
        $listing = new LookListingRenderer();
    }
    $listing->setData($data->products);
    $listing->setQueryBuilder($qb);
    $listing->render();
    $response['totalPages'] = $data->totalPages;
    $response['totalProducts'] = $data->totalProducts;
    $response['markupListing'] = $listing->getMarkup();

    if ($_action == 'filter' || $_action == 'resetfilters') {
        $filter = new SearchFilterPriceRenderer();
        $filter->setQueryBuilder($qb);
        $filter->setOptions($data->filters['price']['values']);
        $filter->render();
        $response['markupPriceFilter'] = $filter->getMarkup();
        $response['dataFilters'] = $data->filters;
    }
    /*
    else if ($_action == 'sort') {
        $sorting = new SearchSortingRenderer();
        $sorting->setData($data->sort);
        $sorting->setQueryBuilder($qb);
        $sorting->render();
        $response['markupSorting'] = $sorting->getMarkup();
    }
    */
    else if ($_action == 'search') {
        $smarty->assign('isXHR', false);
        
        $filters = new SearchFiltersRenderer();
        $filters->setData($data->filters);
        $filters->setQueryBuilder($qb);
        $filters->render();
        $markupFilters = $filters->getMarkup();
        $response['markupFilters'] = $filters->getMarkup();

        $smarty->assign('isXHR', true);
    }
    else if ($_action == 'changesource' && $source == 'search') {
        $smarty->assign('isXHR', false);
        
        $filters = new SearchFiltersRenderer();
        $filters->setData($data->filters);
        $filters->setQueryBuilder($qb);
        $filters->render();
        $markupFilters = $filters->getMarkup();
        $response['markupFilters'] = $filters->getMarkup();

        $sorting = new SearchSortingRenderer();
        $sorting->setData($data->sort);
        $sorting->setQueryBuilder($qb);
        $sorting->render();
        $response['markupSorting'] = $sorting->getMarkup();

        $smarty->assign('isXHR', true);
    }

    header('Content-Type:application/json');
    echo json_encode($response);
}

