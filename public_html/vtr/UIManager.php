<?php
require_once $xcart_dir.'/modules/RestAPI/RestRequest.php';

class UIManager {
    private static $instance;
    protected $torsoTypes;
    protected $clientTypes;
    protected $mannequin;
    protected $session;
    protected $logger;
    protected $errors;
    protected $uuid;

    const DEFAULT_TORSO_TYPE  = 1;
    const DEFAULT_CLIENT_TYPE = 0;

    public static function getInstance() {
        if (!self::$instance) {
            self::$instance = new UIManager();
        }
        return self::$instance;
    }

    private function __construct() {
        global $XCART_SESSION_VARS, $portalapilog;

        // !IMPORTANT! - Do NOT turn this flag in production environments
        $this->showDebug = false;
        $this->apiCallCount = 0;

        $this->errors = array();
        $this->logger = $portalapilog;
        $this->logger->debug('===== VTR Start =====');
        $this->torsoTypes = array(2 => 'Men', 1 => 'Women');
        $this->clientTypes = array(0 => 'Stand Alone');

        // get the saved/shared mannequin, if uuid exist in the URL
        $this->uuid = filter_input(INPUT_GET, 'uuid', FILTER_SANITIZE_STRING);

        $session = $XCART_SESSION_VARS['vtr'];
        if (!empty($session)) {
            $this->session = json_decode($session);
        }
        //header('Content-Type:text/plain'); print_r($this->session); exit;
    }

    public function getMannequin() {
        if (!empty($this->mannequin)) {
            return $this->mannequin;
        }

        if (!empty($this->uuid)) {
            $this->logger->debug('Get Mannequin from uuid: ' . $this->uuid);
            $this->getSavedSharedMannequin($this->uuid);
            $this->logger->debug('mannId: ' . $this->session->mannId);
            return $this->mannequin;
        }

        if (empty($this->session)) {
            $this->session = new StdClass;
            $this->session->torsoType = self::DEFAULT_TORSO_TYPE;
            $this->session->clientType = self::DEFAULT_CLIENT_TYPE;
            $this->logger->debug('Empty Session. New Mannequin is created');
            $this->newMannequin(true);
            $this->logger->debug('mannId: ' . $this->session->mannId);
        }
        else {
            if (isset($_GET['tt']) && in_array($_GET['tt'], array_keys($this->torsoTypes))) {
                $torsoType = (int)$_GET['tt'];

                // if torsoType got changed in a session, get a new mannequin
                if (isset($this->session->torsoType) && $this->session->torsoType !== $torsoType) {
                    $this->session->torsoType = $torsoType;
                    $this->logger->debug('TorsoType changed. New Mannequin is created');
                    $this->newMannequin(true);
                    $this->logger->debug('mannId: ' . $this->session->mannId);
        
                }
                else {
                    $this->logger->debug('TorsoType not changed. Get Mannequin from session');
                    $this->getSessionMannequin();
                    $this->logger->debug('mannId: ' . $this->session->mannId);
                }
            }
            else {
                $this->logger->debug('Get Mannequin from session');
                $this->getSessionMannequin();
                $this->logger->debug('mannId: ' . $this->session->mannId);
            }
        }

        //header('Content-Type:text/plain'); echo("Mannequin: "); print_r($this->mannequin); exit;
        return $this->mannequin;
    }
    
    public function newMannequin($withDefaultStyles = false) {
        $url = HostConfig::$vtrServiceHost.'/vtr/mannequin?torsoType=' . $this->getTorsoType() . '&clientType=' . $this->getClientType();
        $headers = array();
        if ($withDefaultStyles) {
            $headers[] = 'withdefaultstyles: true';
        }
        $this->mannequin = $this->api('GET', $url, array(), $headers);
        //header('Content-Type:text/plain'); echo("New Mannequin: "); print_r($this->mannequin); exit;
        $this->session->mannId = $this->mannequin->id;
        $this->session->torsoId = $this->mannequin->torsoVariant->torso->id;
        $this->saveSession();
    }

    public function getSavedSharedMannequin($uuid) {
        global $smarty;
        $url = HostConfig::$vtrServiceHost.'/vtr/savedshared/' . $uuid;
        $this->mannequin = $this->api('GET', $url, '', array('clienttype: ' . self::DEFAULT_CLIENT_TYPE), false);
        //header('Content-Type:text/plain'); echo("SavedShared Mannequin: "); print_r($this->mannequin); exit;

        if (isset($this->mannequin->status) && $this->mannequin->status == 'failed') {
            $smarty->assign('errorCode', 'ACCESS_DENIED');
            $smarty->display('vtr/error-page.tpl');
            exit(1);
        }

        $this->session->torsoType = $this->mannequin->torsoVariant->torso->torsoType;
        $this->session->clientType = self::DEFAULT_CLIENT_TYPE;
        $this->session->mannId = $this->mannequin->id;
        $this->session->torsoId = $this->mannequin->torsoVariant->torso->id;
        $this->saveSession();
    }
    
    public function getSessionMannequin() {
        $url = HostConfig::$vtrServiceHost.'/vtr/mannequin/' . $this->session->mannId;
        // whenever we get mannequin from session, we should not halt the program when we get error.
        // we should instead try to get default mannequin if it fails
        $this->mannequin = $this->api('GET', $url, array(), array(), false);
        //header('Content-Type:text/plain'); echo("Session Mannequin: "); print_r($this->mannequin); exit;

        if (isset($this->mannequin->status) && $this->mannequin->status == 'failed') {
            $this->logger->debug('Failed to Get Mannequin from session. mannId: ' . $this->session->mannId 
                .", so creating a new default mannequin");
            $this->session->torsoType = isset($this->session->torsoType) ? self::DEFAULT_TORSO_TYPE : $this->session->torsoType;
            $this->session->clientType = isset($this->session->clientType) ? self::DEFAULT_CLIENT_TYPE : $this->session->clientType;
            $this->newMannequin(true);
            $this->logger->debug('Created a new default Mannequin with mannId: ' . $this->mannequin->id);
        }                   
    }
    
    public function getMyLooks($page, $noofitems) {
        $url = HostConfig::$vtrServiceHost.'/vtr/mannequins';
        $looks = array();
        $looksArr = $this->api('GET', $url);
        foreach ($looksArr as $look) {
            $looks[] = array(
                'uuid'  => $look->uuid, 
                'name'  => $look->name,
                'url'   => $this->getUniqueUrl($look->uuid)
            );
        }
        $count = count($looks);
        $residue = $count % $noofitems;
        $pages = ($count - $residue) / $noofitems;
        if ($residue > 0) $pages += 1;

        $data = new StdClass;
        $data->products = $looks;
        $data->totalPages = $pages;
        $data->totalProducts = $count;
        return $data;
    }
    
    protected function saveSession() {
        global $XCART_SESSION_VARS;
        $XCART_SESSION_VARS['vtr'] = json_encode($this->session);
    }

    public function getProductSourceId() {
        return empty($this->uuid) ? 'search' : 'look';
    }

    public function getSession() {
        return $this->session;
    }

    public function getErrors() {
        return $this->errors;
    }

    public function getTorsoTypes() {
        return $this->torsoTypes;
    }

    public function getTorsoType() {
        return isset($this->session->torsoType) ? $this->session->torsoType : self::DEFAULT_TORSO_TYPE;
    }

    public function getOtherTorsoTypeName($torsoType) {
        return $torsoType == 1 ? $this->torsoTypes[2] : $this->torsoTypes[1];
    }

    public function getClientTypes() {
        return $this->clientTypes;
    }

    public function getClientType() {
        return isset($this->session->clientType) ? $this->session->clientType : self::DEFAULT_CLIENT_TYPE;
    }

    public function getMannId() {
        return isset($this->session->mannId) ? $this->session->mannId : $this->getMannequin()->id;
    }
    
    /**
     * Get VTR enabled Styles for a particular torso(type) 
     * Used by the UIManager (for the solr query for the products listing)
     */
    public function getStyleFilterQuery(){
    	return " torsos:(".$this->getTorsoId().")";
    }
    
    public function getViewId() {
        return $this->getMannequin()->viewAngle->id;
    }
    
    public function getTorsoId() {
        return isset($this->session->torsoId) ? $this->session->torsoId : $this->getMannequin()->torsoVariant->torso->id;
    }

    public function getName() {
        return $this->getMannequin()->name;
    }

    public function getStyleIds() {
        $styleIds = $this->getMannequin()->styleIds;
        if (empty($styleIds)) $styleIds = array();
        $this->logger->debug('StyleIds: ' . print_r($styleIds, true));
        return $styleIds;
    }

    public function getUniqueUrl($uuid = '') {
        if (empty($uuid)) {
            $uuid = $this->getMannequin()->uuid;
        }
        $url = 'http://' . HostConfig::$httpHost . '/stylestudio/' . $uuid;
        return $url;
    }

    public function getLayers() {
        $layers = array();
        foreach ($this->getMannequin()->layers->layers as $key => $val) {
            $val = $val[0];
            $type = $val->{'@type'};

            if ($type == 'body-part') {
                $imgUrl = $val->imageUrl->imageUrl;
                $mskUrl = $val->imageUrl->maskImageUrl;
            }
            else if ($type == 'style' || $type == 'face') {
                $imgUrl = $val->image->imageUrl;
                $mskUrl = $val->image->maskImageUrl;
            }
            else {
                // unknown type
                continue;
            }

            $imgUrl = $this->url($imgUrl);
            $mskUrl = empty($mskUrl) ? '' : $this->url($mskUrl);
            $layers[] = array($imgUrl, $mskUrl, $type);
        }
        $this->logger->debug('Layers: ' . print_r($layers, true));
        return $layers;
    }

    public function getFaces() {
        $url = HostConfig::$vtrServiceHost . '/vtr/mannequin/' . $this->getMannId() . '/faces';
        $data = array();
        $facesArr = $this->api('GET', $url);
        foreach ($facesArr as $id => $faces) {
            foreach ($faces as $face) {
                $data[] = array(
                    'id'    => $face->id, 
                    'name'  => $face->name, 
                    'thumb' => $this->url($face->thumbnailImageUrl->imageUrl)
                );
            }
        }
        return $data;
    }
    
    public function getViews() {
        $url = HostConfig::$vtrServiceHost.'/vtr/views?torsoType=' . $this->getTorsoType() . '&clientType=' . $this->getClientType();
        $views = array();
        $viewsArr = $this->api('GET', $url);
        foreach ($viewsArr as $view) {
            $views[] = array(
                'id'   => $view->id, 
                'name' => $view->name
            );
        }
        return $views;
    }
    
    public function getBackgrounds() {
        $url = HostConfig::$vtrServiceHost.'/vtr/backgrounds';
        $backgrounds = array();
        $backgroundsArr = $this->api('GET', $url);
        foreach ($backgroundsArr as $background) {
            $backgrounds[] = array(
                'id'    => $background->id, 
                'name'  => $background->name,
                'thumb' => $this->url($background->thumbnailImageUrl->imageUrl)
            );
        }
        return $backgrounds;
    }
    
    public function changeView($id) {
        $url = HostConfig::$vtrServiceHost . '/vtr/mannequin/' . $this->getMannId() . '/viewangle/' . $id;
        $this->mannequin = $this->api('PUT', $url);
    }
    
    public function changeFace($id) {
        $url = HostConfig::$vtrServiceHost . '/vtr/mannequin/' . $this->getMannId() . '/facevariant/' . $id;
        $this->mannequin = $this->api('PUT', $url);
    }
    
    public function changeBackground($id) {
        $url = HostConfig::$vtrServiceHost . '/vtr/mannequin/' . $this->getMannId() . '/background/' . $id;
        $response = $this->api('PUT', $url);
        return $this->url($response->imageUrl->imageUrl);
    }
    
    public function addStyle($id) {
        $url = HostConfig::$vtrServiceHost . '/vtr/mannequin/' . $this->getMannId() . '/style/' . $id;
        $this->mannequin = $this->api('POST', $url);
    }
    
    public function removeStyle($id) {
        $url = HostConfig::$vtrServiceHost . '/vtr/mannequin/' . $this->getMannId() . '/style/' . $id;
        $this->mannequin = $this->api('DELETE', $url);
    }
    
    public function save($title) {
        $url = HostConfig::$vtrServiceHost . '/vtr/mannequin/' . $this->getMannId();
        $this->mannequin = $this->api('POST', $url, $title, array('Content-Type: text/plain', 'actiontype: 0'));
    }
    
    public function saveAs($title) {
        $url = HostConfig::$vtrServiceHost . '/vtr/mannequin/' . $this->getMannId();
        $this->mannequin = $this->api('POST', $url, $title, array('Content-Type: text/plain', 'actiontype: 0', 'issaveas: true'));
        $this->session->mannId = $this->mannequin->id;
        $this->saveSession();
    }
    
    public function share() {
        $url = HostConfig::$vtrServiceHost . '/vtr/mannequin/' . $this->getMannId();
        $this->mannequin = $this->api('POST', $url, null, array('Content-Type: text/plain', 'actiontype: 1'));
    }
    
    public function reset() {
        $url = HostConfig::$vtrServiceHost . '/vtr/mannequin/' . $this->getMannId(). '/reset';
        $this->mannequin = $this->api('PUT', $url);
    }
    
    public function undo() {
        $url = HostConfig::$vtrServiceHost . '/vtr/mannequin/' . $this->getMannId() . '/undo';
        $this->mannequin = $this->api('PUT', $url);
    }
    
    public function redo() {
        $url = HostConfig::$vtrServiceHost . '/vtr/mannequin/' . $this->getMannId() . '/redo';
        $this->mannequin = $this->api('PUT', $url);
    }
    
    public function api($method, $url, $data = null, $headersMore = array(), $haltOnError = true) {
        global $XCART_SESSION_VARS, $XCARTSESSID, $isXHR;

        $headers = array();
        $headers[] = 'Accept: application/json';
        $headers[] = 'sessionid: ' . $XCARTSESSID;      
        if (!empty($XCART_SESSION_VARS['login'])) {
            $headers[] = 'user: ' . $XCART_SESSION_VARS['login'];
        }
        foreach ($headersMore as $val) {
            $headers[] = $val;
        }

        if ($this->showDebug) {
            $this->apiCallCount += 1;
            header('x-api-method-' . $this->apiCallCount . ': ' . $method);
            header('x-api-url-' . $this->apiCallCount . ': ' . $url);
            if (!empty($data)) {
                header('x-api-data-' . $this->apiCallCount . ': ' . $data);
            }
            header('x-api-headers-' . $this->apiCallCount . ': ' . json_encode($headers));
        }

        $this->logger->debug($method . ' ' . $url);
        $this->logger->debug('headers: ' . print_r($headers, true));
        if (!empty($data)) {
            $this->logger->debug('data: ' . print_r($data, true));
        }
        
        $request = new RestRequest($url, $method, $data);
        $request->setHttpHeaders($headers);
        $request->execute();
        $info = $request->getResponseInfo();
        $body = $request->getResponseBody();

        if ($info['http_code'] == 200) {
            $response = json_decode($body);
            if (!$haltOnError || !isset($response->status) || $response->status != 'failed') {
                return $response;
            }
        }
        else if (!$haltOnError) {
            return $info['content_type'] == 'application/json' ? json_decode($body) : $body;
        }

        // if we get upto here, there is a Error/Exception
        $msg = "oops! something went wrong";
        $err = array();
        $err['method'] = $method;
        $err['url'] = $url;
        $err['data'] = $data;
        $err['http_code'] = $info['http_code'];
        $err['content_type'] = $info['content_type'];
        $err['headers'] = $headers;

        if ($info['content_type'] == 'application/json') {
            $response = json_decode($body);
            if (isset($response->status) && $response->status == 'failed') {
                $msg = $response->message;
            }
            $err['body'] = $response;
        }
        else if (strpos($info['content_type'], 'text/html') !== false) {
            $parts = explode('<body>', $body);
            $err['body'] = strip_tags($parts[1]);
        }
        else {
            $err['body'] = $body;
        }

        $this->logger->error(print_r($err, true));
        if ($isXHR) {
            if ($this->showDebug) {
                header('Content-Type:text/plain');
                echo $msg;
                print_r($err);
            }
            else {
                header('Content-Type:application/json');
                $response = array('status' => 'ERROR', 'message' => $msg);
                echo json_encode($response);
            }
        }
        else {
            header('Content-Type:text/plain');
            echo $msg;
            if ($this->showDebug) {
                print_r($err);
            }
        }
        exit;
    }
    
    public function url($input) {
        return $input;
        //return str_replace('http://myntra.myntassets.com', '', $input);
    }
}

