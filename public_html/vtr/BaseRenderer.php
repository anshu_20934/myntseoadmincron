<?php

class BaseRenderer {
    protected $data;
    protected $markup;
    protected $script;
    protected $qb;

    public function setData($data) {
        $this->data = $data;
    }
    public function setQueryBuilder($qb) {
        $this->qb = $qb;
    }
    public function render() {
        $this->markup = '';
    }
    public function getMarkup() {
        return $this->markup;
    }
    public function getScript() {
        return $this->script;
    }
}

