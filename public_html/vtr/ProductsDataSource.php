<?php
use saveditems\action\SavedItemAction;

class ProductsDataSource {
    protected $search;
    protected $manager;

    public function fetchSearchData($query, $filterQuery, $sort, $page, $noofitems) {
        // include out-of-stock items in the search results
        $_GET['include_oos'] = 1;

        // if query is just digits, make styleid based search
        if (preg_match('/^\d+$/', $query)) {
            $query = " styleid:($query) ";
            $this->search = new SearchProducts($query, null, false);
            $this->search->getAjaxQueryResult($query, '', $sort, $page, $noofitems);
            return;
        }
        
        $this->search = new SearchProducts($query, null, false);
        $this->search->getAjaxQueryResult('', $filterQuery, $sort, $page, $noofitems);
    }

    public function fetchRecentlyViewedData($page, $noofitems) {
        $cookie = getMyntraCookie('RVC001');
        if (empty($cookie)) {
            return;
        }
        $styleIds = explode(',', getMyntraCookie('RVC001'));
        $query = 'styleid:(' . implode(' OR ', $styleIds) . ')';
        $this->search = new SearchProducts($query, null, false);
        $this->search->getAjaxQueryResult($query, '', '', $page, $noofitems);
    }

    public function fetchWishlistData($page, $noofitems) {
        $savedItems = new SavedItemAction();
        $styleIds = $savedItems->getStyleIdsInSaved();
        if (empty($styleIds)) {
            return;
        }
        $query = 'styleid:(' . implode(' OR ', $styleIds) . ')';
        $this->search = new SearchProducts($query, null, false);
        $this->search->getAjaxQueryResult($query, '', '', $page, $noofitems);
    }

    public function setManager($manager) {
        $this->manager = $manager;
    }

    public function getData() {
        global $xcache, $smarty;
        $data = new \stdclass;

        if (empty($this->search)) {
            $data->filters = array();
            $data->products = array();
            $data->totalPages = 0;
            $data->totalProducts = 0;
            return $data;
        }

        $torsoType = $this->manager->getTorsoType();
        $otherTorsoTypeName = $this->manager->getOtherTorsoTypeName($torsoType);

        $products = array();
        $needles = array('style_search_image', '240_320', '180_240');
        $replaces = array('properties', '81_108', '81_108');
        //header('Content-Type:application/json'); echo json_encode($this->search->products); exit;
        foreach ($this->search->products as $key => $val) {
            $prod = array();
            $prod['styleid'] = $val['styleid'];
            $pos = stripos($val['product'], $val['brands_filter_facet']);
            $prod['title'] = trim(substr($val['product'], $pos + strlen($val['brands_filter_facet']), strlen($val['product'])));
            $prod['image_url'] = str_replace($needles, $replaces, $val['search_image']);
            $prod['brand'] = $val['brands_filter_facet'];
            $prod['mrp'] = $val['price'];
            $prod['price'] = $val['discounted_price'];
            $prod['page_url'] = '/' . $val['landingpageurl'];
            $prod['article_type'] = $val['global_attr_article_type_facet'];

            if (isset($val['torso_types'])) {
                $prod['torso_types'] = $val['torso_types'];
                $prod['is_vtr_compatible'] = true;
                $isTorsoCompatible = strpos($val['torso_types'], (string)$torsoType) !== false;
                $prod['is_torso_compatible'] = $isTorsoCompatible;
                if (!$isTorsoCompatible) {
                    $prod['other_torso_type_name'] = $otherTorsoTypeName;
                }
            }
            else {
                $prod['is_vtr_compatible'] = false;
            }
            //$prod['client_types'] = isset($val['client_types']) ? $val['client_types'] : '';
            $products[] = $prod;
        }
        //header('Content-Type:application/json'); echo json_encode($products); exit;

        $colorSwatchMap = $xcache->fetchAndStore(function(){
            $sql = "select family_name, swatch_position from attribute_type_family";
            $colorSwatchMappingTemp=func_query($sql);
            $mapping = array();
            foreach($colorSwatchMappingTemp as $key => $value){
                $mapping[strtolower($value["family_name"])]=$value["swatch_position"];
            }
            return $mapping;
        }, array(), "color_attribute_type_family");

        $facets = $this->search->faceted_search_results_new;
        $data->products = $products;
        $data->totalPages = $this->search->getTotalPages();
        $data->totalProducts = $this->search->products_count;
        $data->sort = array(
            'options'  => array('POPULAR' => 'Popularity', 'DISCOUNT' => 'Discount', 'RECENCY' => "What's New",
                    'PRICEA' => 'Price Low to High', 'PRICED' => 'Price High to Low'),
            'selected' => $this->search->sort_param
        );
        $data->filters = array();
        $data->filters['category'] = array(
            'title'  => 'Categories', 
            'values' => (array)$facets->global_attr_article_type_facet
        );
        $data->filters['brand'] = array(
            'title'  => 'Brand',
            'values' => (array)$facets->brands_filter_facet
        );
        $data->filters['price'] = array(
            'title'  => 'Price',
            'values' => $this->search->price_rage_details
        );
        $data->filters['color'] = array(
            'title'  => 'Color',
            'values' => (array)$facets->colour_family_list,
            'colorSwatchMap' => $colorSwatchMap
        );
        /* -- gender is not needed in VTR
        $data->filters['gender'] = array(
            'title'  => 'Gender',
            'values' => (array)$facets->global_attr_gender
        );
        */
        $data->filters['size'] = array(
            'title'  => 'Size',
            'values' => (array)$facets->sizes_facet
        );
        $articleTypeAttributes = (array)$facets->article_type_attributes;
        foreach ($articleTypeAttributes as $attrName => $attrDetails) {
            $data->filters[$attrName] = $attrDetails;
        }

        return $data;
    }
}

