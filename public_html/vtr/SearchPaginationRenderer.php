<?php

class SearchPaginationRenderer extends BaseRenderer {
    public function setData($data) {
        $this->pageNo = $data['page'];
        $this->totalPages = $data['totalPages'];
    }
    public function render() {
        global $smarty;

        if ($this->totalPages <= 1) {
            $this->markup = '';
            return;
        }

        $pages = array();
        $beginPage = max(0, $this->pageNo - 10);
        $endPage = min($this->totalPages - 1, $this->pageNo + 10);

        $prevPage = max(0, $this->pageNo - 1);
        if ($prevPage != $this->pageNo) {
            $pages[] = array('Prev', $this->qb->makePaginationUrl($prevPage), false);
        }

        for ($i = $beginPage; $i <= $endPage; $i++) {
            $selected = $i == $this->pageNo;
            $pages[] = array($i + 1, $this->qb->makePaginationUrl($i), $selected);
        }

        $nextPage = min($this->totalPages - 1, $this->pageNo + 1);
        if ($nextPage != $this->pageNo) {
            $pages[] = array('Next', $this->qb->makePaginationUrl($nextPage), false);
        }

        $smarty->assign('pageNo', $this->pageNo);
        $smarty->assign('totalPages', $this->totalPages);
        $smarty->assign('pages', $pages);
        $this->markup = $smarty->fetch('vtr/pagination.tpl');
    }
}

