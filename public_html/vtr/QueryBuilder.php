<?php

class QueryBuilder {
    protected $urlBase;
    protected $filters;
    protected $params;
    protected $manager;

    public function __construct() {
        $this->urlBase = '/vtr.php';
        $this->params = array(
            'source' => 'search', 
            'query'  => '',
            'sort'   => 'POPULAR',
            'page'   => 0,
            'noofitems' => 24
        );
        $this->filters = array();
    }

    public function init() {
        if (!empty($_GET['filters'])) {
            $filters = explode('|', $_GET['filters']);
            foreach ($filters as $filter) {
                list ($name, $values) = explode(':', $filter);
                $name = filter_var($name, FILTER_SANITIZE_STRING);
                $this->filters[$name] = array();
                $values = explode('*', $values);
                foreach ($values as $value) {
                    $this->filters[$name][] = filter_var($value, FILTER_SANITIZE_STRING);
                }
            }
        }

        // the data "source" id is first tried on the query params.
        // fallback to the UI manager, if it is empty
        if (isset($_GET['source'])) {
            $source = filter_var($_GET['source'], FILTER_SANITIZE_STRING);
            $this->params['source'] = empty($source) ? 'search' : $source;
        }
        else {
            $this->params['source'] = 'search'; //$this->manager->getProductSourceId();
        }

        if (isset($_GET['query'])) {
            $this->params['query'] = filter_var($_GET['query'], FILTER_SANITIZE_STRING);
        }
        if (isset($_GET['sort'])) {
            $this->params['sort'] = filter_var($_GET['sort'], FILTER_SANITIZE_STRING);
        }
        if (isset($_GET['page'])) {
            $this->params['page'] = filter_var($_GET['page'], FILTER_SANITIZE_NUMBER_INT);
        }
        if (isset($_GET['noofitems'])) {
            $this->params['noofitems'] = filter_var($_GET['noofitems'], FILTER_SANITIZE_NUMBER_INT);
        }
        //header('Content-Type:text/plain'); print_r($this->params); print_r($this->filters); exit;
    }

    public function setManager($manager) {
        $this->manager = $manager;
    }

    public function getFilters() {
        return $this->filters;
    }

    public function getFilter($name) {
        return isset($this->filters[$name]) ? $this->filters[$name] : array();
    }

    public function setFilter($name, $values) {
        $this->filters[$name] = $values;
    }

    public function getParams() {
        return $this->params;
    }

    public function getParam($name) {
        return isset($this->params[$name]) ? $this->params[$name] : '';
    }

    public function setParam($name, $value) {
        $this->params[$name] = $value;
    }

    protected function buildUrl($filters, $params) {
        $parts = array();
        foreach ($filters as $name => $values) {
            if (!empty($values)) {
                $parts[] = $name . ':' . implode('*', $values);
            }
        }
        $params['filters'] = implode('|', $parts);
        $qparams = http_build_query($params, '', '&');
        unset($params['filters']);
        return $this->urlBase . '?' . $qparams;
    }

    public function makeFilterUrl($name, $value) {
        $filters = $this->filters;
        $params = $this->params;
        if (!isset($filters[$name])) {
            $filters[$name] = array();
        }
        $idx = array_search($value, $filters[$name]);
        if ($idx !== false) {
            $selected = true;
            array_splice($filters[$name], $idx, 1);
        }
        else {
            $selected = false;
            $filters[$name][] = $value;
        }
        $params['page'] = 0;
        $url = $this->buildUrl($filters, $params);
        return array($url, $selected);
    }

    public function makePriceUrl($name, $min, $max) {
        $filters = $this->filters;
        $params = $this->params;
        if (!isset($filters[$name])) {
            $filters[$name] = array();
        }
        $selected = (implode('*', $filters[$name]) == "$min*$max");
        $filters[$name] = array($min, $max);
        $params['page'] = 0;
        $url = $this->buildUrl($filters, $params);
        return array($url, $selected);
    }

    public function makeSortUrl($value) {
        $params = $this->params;
        if ($value == 'PRICE') {
            $params['sort'] = $params['sort'] == 'PRICEA' ? 'PRICED' : 'PRICEA';
        }
        else if ($value == $params['sort']) {
            return '';
        }
        else {
            $params['sort'] = $value;
        }
        $params['page'] = 0;
        $url = $this->buildUrl($this->filters, $params);
        return $url;
    }

    public function makePaginationUrl($value) {
        $params = $this->params;
        $params['page'] = $value;
        $url = $this->buildUrl($this->filters, $params);
        return $url;
    }

    public function makeSourceUrl($source) {
        $filters = array();
        $params = $this->params;
        $params['source'] = $source;
        $params['page'] = 0;
        $url = $this->buildUrl($filters, $params);
        return $url;
    }

    public function getSolrFilterQuery() {
        $mapFields = array(
            'gender'   => 'global_attr_gender',
            'size'     => 'sizes',
            'brand'    => 'brands_filter_facet',
            'category' => 'global_attr_article_type_facet',
            'color'    => 'colour_family_list',
            'price'    => 'discounted_price'
        );
        $query = $this->manager->getStyleFilterQuery();
        foreach ($this->filters as $name => $values) {
            $fieldname = isset($mapFields[$name]) ? $mapFields[$name] : $name;
            if ($name == 'price') {
                $query .= ' +' . $fieldname . ':[' . implode(' TO ', $values) . ']';
            }
            else {
                $query .= ' +' . $fieldname . ':("' . implode('" OR "', $values) . '")';
            }
        }
        return $query;
    }
}

