<?php

include_once("./auth.php");

include_once ("$xcart_dir/include/class/mcart/class.MCart.php");
include_once ("$xcart_dir/include/class/mcart/class.MCartFactory.php");
include_once ("$xcart_dir/include/class/mcart/class.MCartUtils.php");
include_once ("$xcart_dir/include/solr/solrProducts.php");
include_once("$xcart_dir/include/class/instrumentation/BaseTracker.php");
include_once($xcart_dir."/include/func/func.mkcore.php");
require_once($xcart_dir."/include/func/func.utilities.php");
require_once($xcart_dir."/include/class/widget/class.static.functions.widget.php");
include_once($xcart_dir."/include/class/search/UserInterestCalculator.php");
include_once($xcart_dir."/include/class/class.feature_gate.keyvaluepair.php");
include_once($xcart_dir."/include/class/notify/class.notify.php");
$publickey  = "6Le_oroSAAAAANGCD-foIE8DPo6zwcU1FaW3GiIX";
$privatekey = "6Le_oroSAAAAAKuArW6iPxhOlk0QVJMwWOTGVQnt";
include_once($xcart_dir."/include/func/func.utilities.php");
require_once($xcart_dir."/tracking/trackingUtil.php");
require_once($xcart_dir."/modules/discount/DiscountEngine.php");
include_once($xcart_dir."/Cache/Cache.php");

use revenue\discounts\views\ComboOverlayView; 

$shownewui= $_MABTestObject->getUserSegment("productui");
$productuiABparam=$shownewui;

$smarty->assign("shownewui", $shownewui);
$smarty->assign("productuiABparam", $productuiABparam);

$discountId = "id";
$isConditionMet = "icm";
$minMore = "m";
$cType = "ct";
$freeCartC = "freecartc";
$json = filter_input(INPUT_GET, "json" , FILTER_SANITIZE_STRING);

$params = array();
$params[$discountId] = (int)filter_input(INPUT_GET, $discountId, FILTER_SANITIZE_NUMBER_INT);
$params[$isConditionMet] = (bool)filter_input(INPUT_GET, $isConditionMet, FILTER_SANITIZE_STRING);
$params[$minMore] = (int)filter_input(INPUT_GET, $minMore, FILTER_SANITIZE_NUMBER_INT);
$params[$cType] = filter_input(INPUT_GET, $cType, FILTER_SANITIZE_STRING);
$params[$freeCartC] = filter_input(INPUT_GET, $freeCartC , FILTER_SANITIZE_STRING );

$view = new ComboOverlayView('cart', $params);
if($json=="1"){
    echo $view->getJsonData();    
}else{
    $view->display();
}

?>

