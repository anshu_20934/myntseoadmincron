<?php
require_once __DIR__.'/auth.php';
require_once $xcart_dir."/include/class/instrumentation/BaseTracker.php";


$pageName = $_GET["page_name"];

switch ($pageName) {
	case 'cart':
		$trackerPage=BaseTracker::CHECKOUT_PAGE_CART_VIEW;
		break;

	case 'address':
		$trackerPage=BaseTracker::CHECKOUT_PAGE_ADDRESS_VIEW;
		break;

	case 'payment':
		$trackerPage=BaseTracker::CHECKOUT_PAGE_PAYMENT_VIEW;
		break;

	default:
		$trackerPage="";
		break;
}

$tracker = new BaseTracker($trackerPage);
$tracker->fireRequest();
?>

