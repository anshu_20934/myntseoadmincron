<?php
require "./auth.php";
include_once(\HostConfig::$documentRoot."/modules/coupon/database/CouponAdapter.php");
include_once(\HostConfig::$documentRoot."/include/class/class.mail.multiprovidermail.php");
include_once(\HostConfig::$documentRoot."/modules/serviceRequest/ServiceRequest.php");
include_once(\HostConfig::$documentRoot."/include/func/func.order.php");

use feedback\instance\MFBInstance;
use feedback\instance\MFBOrderInstance;
use feedback\manage\MFB;

//domain name for links in feedback survey mail
//$httpLocation = "http://www.myntra.com/";
$httpLocation = "http://qa1www12.myntra.com/";

//set the url to which response to be redirected
$formURL = $httpLocation."order_feedback.php";

//get the feedback instance
$MFBInstanceObj = new MFBInstance();

//params get or post
$MFBInstanceId = ($_POST['MFBInstanceId']) ? $_POST['MFBInstanceId'] : $_GET['MFBInstanceId'];
$MFBInstanceAuthKey = ($_POST['MFBInstanceAuthKey']) ? $_POST['MFBInstanceAuthKey'] : $_GET['MFBInstanceAuthKey'];



//show MFB form pre-populated(handle redirection from email)
if(empty($_POST['MFB_response'])){
    
    //validate post parameters for instance and key
    if(!empty($MFBInstanceId) && !empty($MFBInstanceAuthKey)){

        //validate of auth key against instance id
        if($MFBInstanceObj->isValidMFB($MFBInstanceId, $MFBInstanceAuthKey)){

            //get instance detail
            $MFBInstance = $MFBInstanceObj->getMFBInstances(array("instanceid"=>$MFBInstanceId));

            //get MFB object
            $MFB = new MFB();
            $MFBInfo = $MFB->getMFB(array("feedbackid"=>$MFBInstance[0]['feedback_id']));
            $selectedQuestionOptionArray = $_POST['MFB'];            

            //pre-populate the feedback form based on feedback type(order, SR etc..)
            $MFBActiveQuestionnaire = $MFB->prePopulateMFBQuestionnaire($MFBInstance[0]['feedback_id'], $selectedQuestionOptionArray);

            //wrap MFB HTML questionnaire with <form>
            //and hidden fields required to send with order FB mail
            if($MFBInfo[0]['name'] == 'voc_order'){
                $formHead = "Myntra Order Feedback";
            } else if($MFBInfo[0]['name'] == 'voc_SR'){
                $formHead = "Myntra Service Request Feedback";
            } else if($MFBInfo[0]['name'] == 'voc_order_may2013'){
               	$formHead = "Myntra Website Experience Feedback";
            }else{
                $formHead = null;
            }
            // TODO need to figure out currently selected MFB in much better way

            $submitButtonProperties = array("value"=>"Submit Feedback","type"=>"button");
            $MFB_HTML = $MFBInstanceObj->wrapMFBHTMLQuestionnaire($MFBActiveQuestionnaire['questionnaireHTML'],
                                                        $formURL, $MFBInstance[0]['instanceid'], $MFBInstance[0]['authentic_key'], 
                                                        $formHead, $setResponse=1, $submitButtonProperties);

            //show user confirmation message
            $userConfirmMsg = 'If you are <b>'.$MFBInstance[0]['customer_email'].'</b>., and if you have completed all '.
                                'mandatory questions in your feedback,'.
                                '<br/>please click<br/><input type="button" name="MFB_trigger_submit" id="MFB_trigger_submit" value="Submit Feedback" class="btn primary-btn"/>';
            $smarty->assign("msg",$userConfirmMsg);

            //show pre-populated form
            $smarty->assign("MFB_HTML",$MFB_HTML);

            //initialize client side MFB obj
            $smarty->assign("MFB_JSON",$MFBActiveQuestionnaire['questionnaireJSON']);

        } else {
            $msg="We apologize to you that this request is expired now. <br/>We extremely regret ".
                 "and at the same time appreciate your effort in improving our service. Thank You.";
            $smarty->assign("error_msg",$msg);                        
        }
    } else {
        $msg="You have already submitted your feedback and your feedback is under process.";
        $smarty->assign("error_msg",$msg);
    }

}

//MFB response given by the user
else if(is_numeric($_POST['MFB_response']) && $_POST['MFB_response'] == 1){

    //validate post parameters for instance and key
    if(!empty($MFBInstanceId) && !empty($MFBInstanceAuthKey)){

        //validate of auth key against instance id
        if($MFBInstanceObj->isValidMFB($MFBInstanceId, $MFBInstanceAuthKey)){

            //get instance detail
            $MFBInstance = $MFBInstanceObj->getMFBInstances(array("instanceid"=>$MFBInstanceId));
            
            //get the MFB for question type, survey email etc..
            $MFB = new MFB();
            $MFBInfo = $MFB->getMFB(array("feedbackid"=>$MFBInstance[0]['feedback_id']));
            
            //get all posted answers
            $selectedQuestionOptionArray = $_POST['MFB'];

            $MFBquesion = $MFB->validateMFBQuestionnaire($MFBInstance[0]['feedback_id'], $selectedQuestionOptionArray);

            //if all mandatory questions are answered
            if($MFBquesion === true){

                //insert all MFB instance data(feedback response) into DB
            	$isInstanceDataAdded = $MFBInstanceObj->addMFBInstanceData($MFBInstance[0]['instanceid'], $selectedQuestionOptionArray);

            	switch($MFBInfo[0]['name']){
            		case 'voc_order'    :   
            			//send survey mail only then when survey captured
            			if($isInstanceDataAdded){
            				//get order detail
            				$orderInfo = func_query_first("select o.orderid,o.login,concat(o.firstname,' ',o.lastname)as name,
            						IF((`date` IS NULL OR `date`=0),'',from_unixtime(`date`,'%d-%M-%Y')) as ordered_date,
            						IF((`shippeddate` IS NULL OR `shippeddate`=0),'',from_unixtime(`shippeddate`,'%d-%M-%Y')) as shipped_date,
            						IF((`delivereddate` IS NULL OR `delivereddate`=0),'',from_unixtime(`delivereddate`,'%d-%M-%Y')) as delivered_date,
            						IF((`completeddate` IS NULL OR `completeddate`=0),'',from_unixtime(`completeddate`,'%d-%M-%Y')) as completed_date,
            						payment_method, courier_service, s_city, s_locality, s_zipcode
            						from
            						xcart_orders o
            						where
            						o.orderid = '".$MFBInstance[0]['feedback_reference']."'");

            				//get courier service
            				if(!empty($orderInfo['courier_service'])){
            					$courier_service = func_query_first_cell("select courier_service
            							from
            							mk_courier_service
            							where
            							code='".$orderInfo['courier_service']."'");
            					$orderInfo['courier_service'] = $courier_service;
            				}

            				// get products and amount detail for the order
            				$productDetail = func_create_array_products_of_order($MFBInstance[0]['feedback_reference']);
            				$amountDetail = func_get_order_amount_details($MFBInstance[0]['feedback_reference']);

            				// get the html table for the order with product and amount detail in a table format
            				$orderDetail = create_product_detail_table_new($productDetail, $amountDetail);

            				// shipment tracking detail(gives the track of the order where is it on the way)
            				//$shipmentTrackingInfo = getOrderTrackingDetails($MFBInstance[0]['feedback_reference']);

            				//check for survey mailid exists for MFB, then only send MFB survey
            				if(!empty($MFBInfo[0]['survey_email'])){
            					//survey template
            					$template = "voc_order_survey";

            					//get MFB Survey in HTML
            					$MFB_HTML_SURVEY = $MFBInstanceObj->getMFBSurvey($MFBInstance[0]['feedback_id'],
            							$MFBInstance[0]['instanceid']);

            					//send order MFB survey mail as (type-critical)
            					$subjectArgs = array("USER"=>$orderInfo['name'], "ORDERID"=>$orderInfo['orderid']);
            					$bodyArgs = array(
            							"USER"				=> $orderInfo['name'],
            							"ORDERID"			=> $orderInfo['orderid'],
            							"DATE"		        => $orderInfo['ordered_date'],
            							"SHIP_DATE"		    => $orderInfo['shipped_date'],
            							"DELIVERY_DATE"		=> $orderInfo['delivered_date'],
            							"PAYMENT_METHOD"	=> $orderInfo['payment_method'],
            							"LOGISTIC"			=> $orderInfo['courier_service'],
            							"SHIPCITY"			=> $orderInfo['s_city']." - ".$orderInfo['s_locality'],
            							"SHIPCITYZIP"		=> $orderInfo['s_zipcode'],
            							"ORDER_PRODUCT_AMOUNT_DETAIL_HTML" => $orderDetail,
            							"ORDER_MFB_SURVEY"	=> $MFB_HTML_SURVEY,
            					);

            					$customKeywords = array_merge($subjectArgs, $bodyArgs);
            					$mail_details = array(
            							"template" => $template,
            							"to" => $MFBInfo[0]['survey_email'],
            							"bcc" => "myntramailarchive@gmail.com",
            							"header" => 'header',
            							"footer" => 'footer',
            							//"mail_type" => \MailType::CRITICAL_TXN
            					);
            					$multiPartymailer = new \MultiProviderMailer($mail_details, $customKeywords);
            					$isMailSent = $multiPartymailer->sendMail();
            				}

            				//send coupon on proper feedback response
            				$couponAdapter = CouponAdapter::getInstance();
            				$couponcode = "FEEDBACK".get_rand_id(8).rand(1,2);
            				$edate=mktime(23,0,0, date("n")+1  ,date("d"), date("Y"));


            				// check FG whether to enter transitional instance id
            				$transitionCondition = \FeatureGateKeyValuePairs::getFeatureGateValueForKey('MFB.Order.Transition');
            				if($transitionCondition == 'true'){
            					//get the order feedback instance
            					$MFBOrderInstanceObj = new MFBOrderInstance();
            					$flag = $MFBOrderInstanceObj->isTransitionalInstance($MFBInstance[0]['instanceid']);

            					// send old coupon
            					if(!$flag){
            						$couponAdapter->addCoupon($couponcode, 'FeedbackCoupons', time(),
            								$edate, 1, 0, -1, 0, 0, 1, 0, 2000.0, 0.0, $orderInfo['login'], '', time(),
            								'MFB cron', 'percentage', 0.00, 12.00, 0, 'A', 'Feedback coupon', '',
            								'', '', '', '', '', $categoryid_new, '', $productid_new, '','',1);


            						$couponMsg="<br/>As a token of appreciation here is your discount coupon code ".
              						"<span style='color:red;font-size:20px;'> ".$couponcode.".</span><br/>".
              						"This offers you 12% off on your next minimum purchase of Rs 2000. ".
              						"This coupon is valid for a month from now. Enjoy even more shopping at Myntra! <br/>".
              						"In case you have any more suggestions, please feel free to mail us ".
              						"at:support@myntra.com,<br><br> Myntra Team";

            						//Sending coupon code in email also
            						$template = 'sendfeedbackcoupon';
            						$customKeywords = array("COUPON_CODE"	=> $couponcode, "USER" => $orderInfo['name']);
            						$mail_details = array(
            								"template" => $template,
            								"to" => $orderInfo['login'],
            								"bcc" => "myntramailarchive@gmail.com",
            								"header" => 'header',
            								"footer" => 'footer',
            								"mail_type" => \MailType::CRITICAL_TXN
            						);
            						$multiPartymailer = new \MultiProviderMailer($mail_details, $customKeywords);
            						$isMailSent = $multiPartymailer->sendMail();

            						//track for all coupon codes sent to email ids
            						if($isMailSent === true){
            							$query="insert into mk_discount_coupon_capture values(
            							".mysql_real_escape_string($orderInfo['orderid']).",
            							'".mysql_real_escape_string($orderInfo['login'])."',
            							'".mysql_real_escape_string($couponcode)."',
            							".time().")";

            							$email_logs = db_query($query);
            						}

            						// send new coupon for transitional instance
            					} else {
            						// dont send any coupon but to say survey is successful, making
            						//$isMailSent = true;
            						$isMailSent = true;
            					}

            				} else {
            					// dont send any coupon but to say survey is successful, making
            					//$isMailSent = true;
            					$isMailSent = true;
            				}


            				//update MFB instance data with received date so that to restrict
            				//further submission(page refresh) of feedback
            				$MFBInstanceObj->setMFBInstance($MFBInstance[0]['instanceid']);

            				//to show confirm message
            				$MFBuserName = $orderInfo['name'];
            				$isSurveySuccessful = $isMailSent;
            			}
                                            break;


                    case 'voc_SR'       :   
                    	//send survey mail only then when survey captured
                    	if($isInstanceDataAdded){
                    		//get SR detail based on MFB instance reference id
                    		$SR = new \ServiceRequest();
                    		$SRInfo = $SR->getSR(array('fSRid'=>$MFBInstance[0]['feedback_reference']));

                    		if(!empty($SRInfo[0]['customer_email'])){
                    			$customerEmail = $SRInfo[0]['customer_email'];
                    		}
                    		if(!empty($SRInfo[0]['order_customer_email'])){
                    			$customerEmail = $SRInfo[0]['order_customer_email'];
                    		}

                    		//check for survey mailid exists for MFB, then only send MFB survey
                    		if(!empty($MFBInfo[0]['survey_email'])){
                    			//survey template
                    			$template = "voc_SR_survey";

                    			//get MFB Survey in HTML
                    			$MFB_HTML_SURVEY = $MFBInstanceObj->getMFBSurvey($MFBInstance[0]['feedback_id'],
                    					$MFBInstance[0]['instanceid']);

                    			//send order MFB survey mail as (type-critical)
                    			$SRUser = ($SRInfo[0]['first_name'])?$SRInfo[0]['first_name']:"Myntra Customer";
                    			$subjectArgs = array("USER"=>$SRUser, "SRID"=>$SRInfo[0]['customer_SR_id']);
                    			$bodyArgs = array(
                    					"USER"				=> $SRUser,
                    					"SRID"			    => $SRInfo[0]['customer_SR_id'],
                    					"ORDERID"           => (!empty($SRInfo[0]['order_id']))? $SRInfo[0]['order_id'] : 'NA',
                    					"CATEGORY"	        => $SRInfo[0]['SR_category']."-".$SRInfo[0]['SR_subcategory'],
                    					"SR_CREATE_DATE"    => date('d-F-Y',$SRInfo[0]['createtime']),
                    					"CREATEDBY"         => $SRInfo[0]['reporter'],
                    					"SR_CLOSE_DATE"     => date('d-F-Y',$SRInfo[0]['closetime']),
                    					"CLOSEDBY"          => $SRInfo[0]['resolver'],
                    					"SR_MFB_SURVEY"	    => $MFB_HTML_SURVEY,
                    			);

                    			$customKeywords = array_merge($subjectArgs, $bodyArgs);
                    			$mail_details = array(
                    					"template" => $template,
                    					"to" => $MFBInfo[0]['survey_email'],
                    					"bcc" => "myntramailarchive@gmail.com",
                    					"header" => 'header',
                    					"footer" => 'footer',
                    					//"mail_type" => \MailType::CRITICAL_TXN
                    			);
                    			$multiPartymailer = new \MultiProviderMailer($mail_details, $customKeywords);
                    			$isMailSent = $multiPartymailer->sendMail();
                    		}

                    		//update MFB instance data with received date so that to restrict
                    		//further submission(page refresh) of feedback
                    		$MFBInstanceObj->setMFBInstance($MFBInstance[0]['instanceid']);

                    		//to show confirm message
                    		$MFBuserName = $SRUser;
                    		$isSurveySuccessful = $isMailSent;
                    	}

                    	break;
                                                           
                }
                // TODO need to figure out currently selected MFB in much better way

                if($isSurveySuccessful === true){
                    $msg="<div style='font-size:20px;font-weight:bold;'>Thank you for your feedback!</div>"
                            ."<br/>We hope to incorporate most of your suggestions in order to provide a better"
                            ." experience to all our customers.";                            

                    if($couponMsg){
                        $msg .= $couponMsg;
                    }

                    $smarty->assign("msg",$msg);

                } else {
                    //commenting just not to show error message if mail is not sent
                    /*$msg="We apologize to you that there is an error occurred in capturing your feedback. <br/>We extremely regret "
                            ."and at the same time appreciate your effort in improving our service. Thank You.";
                    $smarty->assign("error_msg",$msg);*/
                    $msg="<div style='font-size:20px;font-weight:bold;'>Thank you for your feedback!</div>"
                            ."<br/>We hope to incorporate most of your suggestions in order to provide a better"
                            ." experience to all our customers.";
                    $smarty->assign("msg",$msg);
                }
                
            //if no mandatory question is answered
            } else {                          

                //to show the MFB pre-populated form
                $MFBActiveQuestionnaire = $MFB->prePopulateMFBQuestionnaire($MFBInstance[0]['feedback_id'], $selectedQuestionOptionArray);
                // TODO can highlight a invalid question

                //wrap MFB HTML questionnaire with <form>
                //and hidden fields required to send with order FB mail
                if($MFBInfo[0]['name'] == 'voc_order'){
                    $formHead = "Myntra Order Feedback";
                } else if($MFBInfo[0]['name'] == 'voc_SR'){
                    $formHead = "Myntra Service Request Feedback";
                } else if($MFBInfo[0]['name'] == 'voc_order_may2013'){
                    	$formHead = "Myntra Website Experience Feedback";                
                }else{
                    $formHead = null;
                }
                // TODO need to figure out currently selected MFB in much better way

                $submitButtonProperties = array("value"=>"Submit Feedback","type"=>"button");
                $MFB_HTML = $MFBInstanceObj->wrapMFBHTMLQuestionnaire($MFBActiveQuestionnaire['questionnaireHTML'],
                                                            $formURL, $MFBInstance[0]['instanceid'], $MFBInstance[0]['authentic_key'],
                                                            $formHead, $setResponse=1, $submitButtonProperties);

                //show pre-populated form
                $smarty->assign("MFB_HTML",$MFB_HTML);

                //initialize client side MFB obj
                $smarty->assign("MFB_JSON",$MFBActiveQuestionnaire['questionnaireJSON']);

                //show error message
                $msg = "Please answer all the mandatory fields (marked with an *) in order to submit your feedback. ".
                        "We appreciate your effort towards improving our service.";
                            
                $smarty->assign("error_msg",$msg);

            }

        //if feedback given already for this reference type
        } else {
            $msg="We apologize to you that this request is expired now. <br/>We extremely regret ".
                 "and at the same time appreciate your effort in improving our service. Thank You.";
            $smarty->assign("error_msg",$msg);
        }

    //if feedback given already for this reference type
    } else {
        $msg="You have already submitted your feedback and your feedback is under process.";
        $smarty->assign("error_msg",$msg);
    }
    
}
//direct url hit message(show message for un-authenticated access)
else{

    $msg="We are sorry to tell you that this is an un-authenticated request and is not being processed.";
    $smarty->assign("error_msg",$msg);  
}

$analyticsObj = AnalyticsBase::getInstance();
$analyticsObj->setStaticPageVars(AnalyticsBase::ORDER_FEEDBACK);
$analyticsObj->setTemplateVars($smarty);

if($skin === "skin2") {
    func_display("feedback.tpl",$smarty);
} else {
    func_display("customer/mkfeedback.tpl",$smarty);
}

?>