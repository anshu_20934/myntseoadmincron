<?php
include_once "./auth.php";
include_once "../auth.php";
include_once "$xcart_dir/include/func/func.mkmisc.php";

include_once ("$xcart_dir/modules/coupon/exception/CouponNotFoundException.php");
include_once ("$xcart_dir/modules/coupon/database/CouponAdapter.php");

if(empty($_SERVER['HTTPS'])){
	//Page to be accessed only over http redirect him to https
	 func_header_location($https_location .$_SERVER['REQUEST_URI'],false);
}

if($_SERVER['HTTP_HOST'] != $xcart_https_host){
	//redirect if someone opened with any other url 
	func_header_location($https_location . $_SERVER['REQUEST_URI'],false);
}


$couponAdapter = CouponAdapter::getInstance();

x_session_register('gotosmsverification', 0);
$gotosmsverification = 0;
$emailLink = $_GET['verify'];

$login = mysql_real_escape_string($XCART_SESSION_VARS['login']);
$orderid = filter_input(INPUT_GET,"orderid", FILTER_SANITIZE_NUMBER_INT);
$orderid = mysql_real_escape_string($orderid);
if(!empty($emailLink)) {
	// COD Email verification.
	// We land here from the email link which was emailed to user, on Cash on Delivery order
	
	$emailloginid 	= filter_input(INPUT_GET,"loginid", FILTER_SANITIZE_STRING);
	$emailcode 		= filter_input(INPUT_GET,"code", FILTER_SANITIZE_STRING);
	$emailorderid 	= filter_input(INPUT_GET,"orderid", FILTER_SANITIZE_NUMBER_INT);
	
	$emailcode = mysql_real_escape_string($emailcode);
	$login = $emailloginid = mysql_real_escape_string($emailloginid);
	$orderid = $emailorderid =mysql_real_escape_string($emailorderid);
	
}
		
/* We evaluate a case, when a user placed a COD order(1) , and had an sms sent to him. 
 * The user does not verify the sms code, and makes some modifications to his cart, and places COD order(2) again.
 * In such case, a new orderid is generated, and a new sms/email is sent.
 * If user clicks on earlier email for order(1), we DONT allow user to confirm any order other than the last placed order(2).
 */
$queryMaxorderid = "select max(orderid) as maxid from xcart_orders where login='" . $login. "' and payment_method='cod'";
$resultMaxorderid = db_query($queryMaxorderid);

$result2 = db_fetch_array($resultMaxorderid);
$maxorderid = $result2['maxid'];

if($maxorderid==null) {
	/* We evaluate a case, when a user tampers email link. The user may never have placed any COD order, but tries to see 
	 * how he can break some logical checks made by the system. Such cases can be useful to tracking fraudulent cases. 
	 */
	$showerror = true;
	$errormessage = "No such order has been placed by the user. The link is incorrect.";
	
} else if($maxorderid!=$orderid) {
	/* As mentioned in earlier comment, about a case where user places two COD orders without confirming sms code on first order.
	 * User clicks on an older email link. However the orderid in that link, will not match the orderid with latest placed cod order
	 * by the user. We simply tell the user, that link has expired.
	 */
	$showerror = true;
	$errormessage = "This order cannot be processed as the link has expired.";
}

if($showerror==true) {
	$XCART_SESSION_VARS['errormessage'] = $errormessage;
	if ($shouldLog) fputs($ORDERLOG,"%%%%%%%%%%%%%%%%%%%%% Error in order processing - Empty login %%%%%%%%%%%%%% \n");
	if ($shouldLog) fclose($ORDERLOG);
	header("Location:mksystemerror.php");
	exit;
}



	
if(!empty($emailLink)) {
	$showerror = false;	// To display any error in case URL is tampered with, or it has expired..
	
	$getShoppingCookieSQL = "select shopping_cookie from cash_on_delivery_info where login_id='" .$emailloginid . "'";
	$getShoppingCookieSQL.= " and order_id='" . $emailorderid . "' and order_confirmed='N'";
	
	/* Note: In the above query, we also check for order_confirmed='N'. 
	 * In case order is already confirmed (order_confirmed='Y'), we need to invalidate the link
	 */ 
	
	$resultShoppingCookie = db_query($getShoppingCookieSQL);
	$resultArray = db_fetch_array($resultShoppingCookie);
	if(empty($resultArray)) {
		// The user has clicked on an email sent link, for an already sms code verified order.
		$showerror = true;
		$errormessage = "This order cannot be processed as the link has expired.";
		
	} else {
		// We retrieve the shopping_cookie for two main reasons.
		// 1. To valid hash code send in email URL.
		// 2. To recreate the shopping cart from where it was left off. 
		$uuid = $resultArray['shopping_cookie'];
		
		if(empty($uuid)) {
			$showerror = true;
			$errormessage = "This order cannot be processed as the link has expired.";
		}
		
		else {
			// The emailed URL contains a code. This code is computed as md5 hash of loginid,orderid, and shopping cookie value in cod_verification.php
	
			/* We create a md5 here, by using login id and order id from parameters passed in URL, and then using the shopping_cookie
			 * by fetching it from cash_on_delivery_info table (each valid orderid/loginid has some shopping cookie info stored.)
			 */
			$verifyEmailCode = md5($emailloginid . $emailorderid . $uuid);
		}
	}
	
	if($showerror!=true) {
		// No error so far. Check for some more error cases.
		
		if(empty($emailloginid) || empty($emailorderid) ||  $emailLink!='email' || $verifyEmailCode!=$emailcode) {
			// If any details are removed from URL link, show error message, or the hash code in URL is incorrect..
			$showerror = true;
			$errormessage = "This order cannot be processed as the link is invalid.";
		} 
		else if(!empty($XCART_SESSION_VARS['login']) && $XCART_SESSION_VARS['login'] != $emailloginid) {
			// If logged in as some other user, to which the login id in emailed URL does not map, show message to logout and relogin..
			$showerror = true;
			$errormessage = "You are not logged in as " . $emailloginid . " Please logout and try the link again";	
		}
	}
	
	if($showerror==true) {
		$XCART_SESSION_VARS['errormessage'] = $errormessage;
		if ($shouldLog) fputs($ORDERLOG,"%%%%%%%%%%%%%%%%%%%%% Error in order processing - Empty login %%%%%%%%%%%%%% \n");
		if ($shouldLog) fclose($ORDERLOG);
		header("Location:mksystemerror.php");
		exit;
	}

	// If we have reached till this point, it implies that user has clicked on valid email link, which should lead him to confirmation
	// of his last placed COD order, that is yet to be confirmed.

	// We set gotosmsverification value as 1 over here, so that if redirecting to login page, then login.php knows how to redirect back to sms
	// confirmation page..
	x_session_register('gotosmsverification', 1);
	$gotosmsverification = 1;
	
	if(empty($XCART_SESSION_VARS['login'])) {
		// In case user is not logged in, we need to redirect to login page.
		// And after the user has logged in, he should see the SMS verification page, and not any other page.
		func_redirect_to_login($http_location, $_SERVER['REQUEST_URI']);
		
		// Note: We have already handled case of some other user already logged, and the login id mentioned in url is different.
		//       For such cases, user sees an message, asking him to logout, and relogin.
		
	} else {
		// The link is valid for the current logged in user. Now we need to recreate the cart from the link..
		$shoppingCartQuery = "select cart_data from cash_on_delivery_info where shopping_cookie='$uuid' and order_id='$emailorderid'";
		$cartdataResults = func_query_first($shoppingCartQuery);
		$cartData = (!empty($cartdataResults['cart_data'])) ? $cartdataResults['cart_data'] : null;
		recreateCartFromShoppingCookie($uuid, $cartData);		
	}
}

// We are making COD slightly more secure at this point by handling this case:
// If a user clears all cookies, when on mkCODVerification page, 
// then we wouldn't want blank shopping_cookie's to be saved in cash_on_delivery_info table.

$productsInCart = $XCART_SESSION_VARS["productsInCart"];

//+++++++++++++++ Order Log File for debugging ++++++++++++++//
$orderLogFile = $var_dirs['log']."/orders.log";

// capture the session dump in the beginning
$shouldLog = true;
##################################################################################
$accountType = 0;

$ORDERLOG = fopen($orderLogFile,"a+");
if (!$ORDERLOG) $shouldLog = false;
if ($shouldLog){
	fputs($ORDERLOG,"Enter mkCODverification.php @ ".date("F j, Y, g:i a")."\n");
	$sessionDumpStr = "";
	ob_start();
	var_dump($XCART_SESSION_VARS);
	$sessionDumpStr = ob_get_contents();
	ob_end_clean();
	fputs($ORDERLOG,"\tSession ID - ".$XCARTSESSID."\n");
	fputs($ORDERLOG,"\tSession Dump - ".$sessionDumpStr."\n");
}
if(empty($login)){
	$errormessage = "Ooops! There was an error in order processing and hence your order could not be placed. Please contact support@myntra.com for more details.";
	$XCART_SESSION_VARS['errormessage'] = $errormessage;
	if ($shouldLog) fputs($ORDERLOG,"%%%%%%%%%%%%%%%%%%%%% Error in order processing - Empty login %%%%%%%%%%%%%% \n");
	if ($shouldLog) fclose($ORDERLOG);
	header("Location:mksystemerror.php");
}
else {
	#
	#Check if login and orderid is mapped
	#
	$sql = "SELECT count(*) ctr FROM " . $sql_tbl['orders'] . " WHERE login='". $login ."' AND orderid='". $orderid ."' AND payment_method='cod' and status='PV'";

	$number_of_row = func_query_first_cell($sql);
	if($number_of_row != 1){
		$errormessage = "This order has already been placed for Cash on Delivery.";
		if(!empty($customerSupportCall)) {
			$errormessage.= "<br/>Please contact Customer Care $customerSupportCall in case of any further queries";
		}
		$XCART_SESSION_VARS['errormessage'] = $errormessage;
		header("Location:mksystemerror.php");
		exit;
	}else{
		include_once("$xcart_dir/include/func/func.mk_orderbook.php");
		include_once("$xcart_dir/include/func/func.order.php");
		include_once("$xcart_dir/include/func/func.mkcore.php");
		include_once("$xcart_dir/include/func/func.mail.php");
		include_once("$xcart_dir/include/func/func.sms_alerts.php");

		
		$totalAmount = $XCART_SESSION_VARS["totalAmount"];
		
		if($totalAmount<300)
		{
			// COD order amount cannot be less than 300/-. User can open some other tabs and change cart/discount options, to reduce amount to less than 300/-
			// We show error here, so that such a COD order cannot be placed.
			$errormessage = "This order cannot be placed though COD, as the total amount to pay is less than Rs. 300/-. Please try other modes of payment on checkout";
			$XCART_SESSION_VARS['errormessage'] = $errormessage;
			if ($shouldLog) fputs($ORDERLOG,"%%%%%%%%%%%%%%%%%%%%% $errormessage %%%%%%%%%%%%%% \n");
			if ($shouldLog) fclose($ORDERLOG);
			header("Location:mksystemerror.php");
			exit;
		}
		
		$codorderid = $orderid;
		x_session_register('codorderid');
		
		
		$orderid = strip_tags($orderid);
		$orderid = sanitize_int($orderid);
		$smarty->assign("orderid", $orderid);

		if($_GET['xid']!="") $session_id = $_GET['xid'];
		else $session_id = $XCARTSESSID;
		
		#
		#Call function to load the customer details
		#
		$customer = func_customer_order_address_detail($login, $orderid);
		$customerFirstName = $customer['s_firstname'];
		$customerMobileNumber = mysql_real_escape_string($customer['mobile']);
		$smarty->assign("codInitialMobileNumber",$customerMobileNumber);
				
		if ($shouldLog) fputs($ORDERLOG,"\t\n ++++++++++++CODE SMS Verification Send for Order ID - ".$orderid."++++++++++++++\n");
		if ($shouldLog) fclose($ORDERLOG);

		$smarty->assign("loginid",$login);
		
		$amount = $XCART_SESSION_VARS["amount"];
		$mrp = $XCART_SESSION_VARS["mrp"];
		$totalQuantity = $XCART_SESSION_VARS["totalQuantity"];
		$totalAmount = $XCART_SESSION_VARS["totalAmount"];
		$vat = $XCART_SESSION_VARS["vat"];
		$couponCode = $XCART_SESSION_VARS["couponCode"];
        $cashCouponCode = $XCART_SESSION_VARS["cashCouponCode"];

        $cashdiscount = $XCART_SESSION_VARS["cashdiscount"];
		$coupondiscount = $XCART_SESSION_VARS["coupondiscount"];
		$productDiscount = $XCART_SESSION_VARS["productDiscount"];
		$couponpercent = $XCART_SESSION_VARS["couponpercent"];
        $gift_data = $XCART_SESSION_VARS["giftdata"];
		$cart_amount = $amount;
		
		$coupon = null;
		if (!empty($couponCode)) {
		    try {
		        $coupon = $couponAdapter->fetchCoupon($couponCode);
		    }
		    catch (CouponNotFoundException $e) {
		        $coupon = null;
		    }
		}
		
		$shippingRate = func_shipping_rate_re($country,$state, $shippingCity, $productsInCart, $productids, $coupon,$logisticId, null ,$cart_amount);
		$shippingTotalAmount = number_format($shippingRate['totalshippingamount'],2,".",'');
		$box_count = $shippingRate['box_count'];
		$smarty->assign("shippingRate", $shippingTotalAmount);
		$smarty->assign("box_count",$box_count);
		
		$totalAmount = $totalAmount + $shippingTotalAmount; 
		
		$smarty->assign("amount",$amount);
		$smarty->assign("cart_amount",$cart_amount);
		$smarty->assign("vat",$vat);
		$smarty->assign("mrp",$mrp);
		$smarty->assign("couponCode",$couponCode);
        $smarty->assign("cashCouponCode",$cashCouponCode);                
        $smarty->assign("cashdiscount",$cashdiscount);
		$smarty->assign("coupondiscount",$coupondiscount);
		$smarty->assign("totalDiscount",$coupondiscount+$productDiscount);
		$smarty->assign("couponpercent", $couponpercent);
		$smarty->assign("totalAmount",$totalAmount);
		$smarty->assign("totalQuantity",$totalQuantity);
        if(!empty($gift_data['giftamount']))
            $smarty->assign("giftamount",$gift_data['giftamount']);
		
		$smarty->assign("address",$address );
				
		$productsInCart = $XCART_SESSION_VARS["productsInCart"];
		$smarty->assign("productsInCart",$productsInCart);

		$codloginid = $login;
		x_session_register('codloginid');
		
		// We are using $gotosmsverification==1 as information to show the sms verification page directly from Emailed url link
		$smarty->assign("gotosmsverification", $gotosmsverification);
		
		

		$countCODVerificationAttempts = func_query_first("select count(*) as count from cash_on_delivery_info where order_id='$orderid'");
		if(empty($countCODVerificationAttempts['count'])) {
			$shoppingCartQuery = "select cart_data from mk_shopping_cart where cookie='$uuid'";
			$cartdataResults = func_query_first($shoppingCartQuery);
			
			if(!empty($cartdataResults['cart_data'])) {
				$cartData = "'" . mysql_real_escape_string($cartdataResults['cart_data']) . "'";
			} else {
				$cartData = 'null';
			}
			
			$randomFourDigitCode = '0000';
			$uuid = stripslashes(getMyntraCookie('MYNTRA_SHOPPING_ID'));
			if(empty($uuid)){
				$uuid = 'none';
			}
			
			
			
			$query = "insert into cash_on_delivery_info (order_id, login_id, verification_code, contact_number, shopping_cookie, order_confirmed, zd_transactiontoken, cart_data) "; 
			$query.= " values ('$orderid', '$login', $randomFourDigitCode, '$customerMobileNumber', '$uuid', 'N', 'CaptchaNotVerified', $cartData)";
			
			// An insert query should happen whenever a wrong captcha is entered.
			db_query($query);
		}
		
		func_display("cart/codverificationCaptcha.tpl", $smarty);
	}
}
?>
