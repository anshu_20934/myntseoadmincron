<?php
namespace myntra\utils\db;

require_once dirname(__FILE__)."/../env/Host.config.php";
require_once \HostConfig::$documentRoot."/env/AMQP.config.php";
require_once \HostConfig::$documentRoot."/logger.php";

final class throttleUpdatesDb{
	private static $amqpConn = NULL;
    private static $channel = null;
    
	private static function initamqp() {
        if (self::$amqpConn) return;

        $rabbitoptions = array(
            'port' => \AMQPConfig::$port,
            'host' => \AMQPConfig::$host,
            'login' => \AMQPConfig::$login,
            'password' => \AMQPConfig::$password,
            'vhost' => \AMQPConfig::$vhost
        );
        try {
            self::$amqpConn = new \AMQPConnection($rabbitoptions["host"],$rabbitoptions["port"],$rabbitoptions["login"],$rabbitoptions["password"],$rabbitoptions["vhost"]);
            self::$channel = self::$amqpConn->channel();
        } catch (\Exception $e) {
            echo "Error in rabbitmq connection:".$e."\n";
            self::$amqpConn = NULL;
        }
	}
	
	 private static function initthrottleexch() {
        if (self::$channel) return;
        self::initamqp();
    }
	
    public static function throttle($batchQuery){
    	self::initthrottleexch();
    	$res = self::$channel->basic_publish(json_encode($batchQuery),"throttleexch","ThrottleDBUpdates");
        return $res;
    }
}

?>
