<?php
chdir(dirname(__FILE__));

define('AMAZON_IMAGE_BUCKET', 'myntrawebimages');
define('AMAZON_S3_BUCKET_URL_SUFFIX', '.s3.amazonaws.com');

define('S3_ACCESS_KEY_ID', 'AKIAILAAXNRLGZ3GKS2A');
define('S3_SECRET_ACCESS_KEY', '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');

define('DEFAULT_BUCKET_NAME', SystemConfig::$amazonS3Bucket);
define('S3_PATH_BASE', HostConfig::$cdnBase.'/');

include_once("../auth.php");
require_once('../AMAZONS3/util/S3.php');
require_once('../include/func/func.utilities.php');
use cache\InvalidateXCache;

function gzip($src, $level = 5, $dst = false){
    if($dst == false){
        $dst = $src.".jgz";
    }
    if(file_exists($src)){
        $filesize = filesize($src);
        $src_handle = fopen($src, "r");
        if(!file_exists($dst)){
            $dst_handle = gzopen($dst, "w$level");
            while(!feof($src_handle)){
                $chunk = fread($src_handle, 2048);
                gzwrite($dst_handle, $chunk);
            }
            fclose($src_handle);
            gzclose($dst_handle);
            return true;
        } else {
            error_log("$dst already exists");
        }
    } else {
        error_log("$src doesn't exist");
    }
    return false;
}

function transferFile($filepath, $uri,$contentType) {
    moveToS3($filepath, DEFAULT_BUCKET_NAME, $uri,$contentType);
}

function copyFile($path,$uri,$contentType){
	$numtries = 3;
	$copiedflag = false;
	while($numtries-- > 0 && !$copiedflag){
		transferFile($path,$uri,$contentType);
		$s3url = S3_PATH_BASE.$uri;
		if(url_exists($s3url)) {
			echo "Copied $path to ".S3_PATH_BASE.$uri."\n";
			$copiedflag = true;
			break;
		}
		usleep(50000);//Sleep for 50ms before checking
	}
	if($copiedflag) {
	
	}else {
		echo "Could not copy $path to ".S3_PATH_BASE.$uri."\n";
	}
	return $copiedflag;
}

if(empty($_SERVER["argv"][1])) {
	echo "Please specify a list of comma seprated server name like web1,web2,web3 to apply the changes to, make sure server name is same as in config.php\n";
	exit;
}

$serverList =explode(",", $_SERVER["argv"][1]);


$http_location = "http://localhost";
$js_path = str_replace("&amp;", "&", $js_path);
$css_path = str_replace("&amp;", "&", $css_path);
$secure_js_path = str_replace("&amp;", "&", $secure_js_path);
$secure_css_path = str_replace("&amp;", "&", $secure_css_path);
echo "Started Uploading JS and CSS\n";
$minMyntraJS = file_get_contents($http_location."/".$js_path);
$minMyntraCSS= file_get_contents($http_location."/".$css_path);
$minSecureMyntraJS = file_get_contents($http_location."/".$secure_js_path);
$minSecureMyntraCSS = file_get_contents($http_location."/".$secure_css_path);

$timeStamp = time();

$JSFileName = "myntra.".$timeStamp.".js";
$CSSFileName = "myntra.".$timeStamp.".css";
$SecureJSFileName = "secure.".$timeStamp.".js";
$SecureCSSFileName = "secure.".$timeStamp.".css";
$PATH = dirname(__FILE__);

$JSFilePath = $PATH . "/.." . "/skin1/js_script/" . $JSFileName;
$CSSFilePath = $PATH . "/.." . "/skin1/css/" . $CSSFileName;

$SecureJSFilePath = $PATH . "/.." . "/skin1/js_script/" . $SecureJSFileName;
$SecureCSSFilePath = $PATH . "/.." . "/skin1/css/" . $SecureCSSFileName;

file_put_contents($JSFilePath, $minMyntraJS);
file_put_contents($CSSFilePath, $minMyntraCSS);
file_put_contents($SecureJSFilePath, $minSecureMyntraJS);
file_put_contents($SecureCSSFilePath, $minSecureMyntraCSS);

if(gzip($JSFilePath)){
	$deleted = unlink($JSFilePath);
	echo "Deleting $JSFilePath = $deleted\n";
	$JSFilePath = $JSFilePath.".jgz";
} else {
	echo "Error: Unable to gzip $JSFilePath\n";
	exit;
}

if(gzip($CSSFilePath)){
	$deleted = unlink($CSSFilePath);
	echo "Deleting $CSSFilePath = $deleted\n";
	$CSSFilePath = $CSSFilePath.".jgz";
} else {
	$deleted = unlink($CSSFilePath);
	echo "Deleting $CSSFilePath = $deleted\n";
	echo "Error: Unable to gzip $CSSFilePath\n";
	exit;
}

if(gzip($SecureJSFilePath)){
	$deleted = unlink($SecureJSFilePath);
	echo "Deleting $SecureJSFilePath = $deleted\n";
	$SecureJSFilePath = $SecureJSFilePath.".jgz";
} else {
	$deleted = unlink($SecureJSFilePath);
	echo "Deleting $SecureJSFilePath = $deleted\n";
	echo "Error: Unable to gzip $SecureJSFilePath\n";
	exit;
}

if(gzip($SecureCSSFilePath)){
	$deleted = unlink($SecureCSSFilePath);
	echo "Deleting $SecureCSSFilePath = $deleted\n";
	$SecureCSSFilePath = $SecureCSSFilePath.".jgz";
} else {
	$deleted = unlink($SecureCSSFilePath);
	echo "Deleting $SecureCSSFilePath = $deleted\n";
	echo "Error: Unable to gzip $SecureCSSFilePath\n";
	exit;
}

$cdnpath = "skin1/js/".$JSFileName.".jgz";
$contentType = array("Content-Encoding"=>"gzip","Content-Type"=>"text/javascript");
$copiedflag = copyFile($JSFilePath,$cdnpath,$contentType);
if($copiedflag) {
	foreach ($serverList as $serverName){
		$serverName = trim($serverName);
		$keyValue = $serverName."-myntraJS";
		func_query("delete from mk_widget_key_value_pairs where `key`='".$keyValue."'");
		func_query("insert into mk_widget_key_value_pairs (`key`,value,description) values ('".$keyValue."','".$cdnpath."','JS File fetched from cdn for portal')");		
	}	
}

$cdnpath = "skin1/css/".$CSSFileName.".jgz";
$contentType = array("Content-Encoding"=>"gzip","Content-Type"=>"text/css");
$copiedflag = copyFile($CSSFilePath,$cdnpath,$contentType);
if($copiedflag) {
	foreach ($serverList as $serverName){
		$serverName = trim($serverName);
		$keyValue = $serverName."-myntraCSS";
		func_query("delete from mk_widget_key_value_pairs where `key`='".$keyValue."'");
		func_query("insert into mk_widget_key_value_pairs (`key`,value,description) values ('".$keyValue."','".$cdnpath."','CSS File fetched from cdn for portal')");		
	}
}

$cdnpath = "skin1/js/".$SecureJSFileName.".jgz";
$contentType = array("Content-Encoding"=>"gzip","Content-Type"=>"text/javascript");
$copiedflag = copyFile($SecureJSFilePath,$cdnpath,$contentType);
if($copiedflag) {
	foreach ($serverList as $serverName){
		$serverName = trim($serverName);
		$keyValue = $serverName."-secureMyntraJS";
		func_query("delete from mk_widget_key_value_pairs where `key`='".$keyValue."'");
		func_query("insert into mk_widget_key_value_pairs (`key`,value,description) values ('".$keyValue."','".$cdnpath."','JS File fetched from cdn for secure pages on portal')");		
	}
}

$cdnpath = "skin1/css/".$SecureCSSFileName.".jgz";
$contentType = array("Content-Encoding"=>"gzip","Content-Type"=>"text/css");
$copiedflag = copyFile($SecureCSSFilePath,$cdnpath,$contentType);
if($copiedflag) {
	foreach ($serverList as $serverName){
		$serverName = trim($serverName);
		$keyValue = $serverName."-secureMyntraCSS";
		func_query("delete from mk_widget_key_value_pairs where `key`='".$keyValue."'");
		func_query("insert into mk_widget_key_value_pairs (`key`,value,description) values ('".$keyValue."','".$cdnpath."','CSS File fetched from cdn for secure pages on portal')");		
	}
}

$xCacheInvalidator = InvalidateXCache::getInstance();
$xCacheInvalidator->invalidateCacheForKey(WidgetKeyValuePairs::WIDGET_KVPAIR_CACHE);

$deleted = unlink($JSFilePath);
echo "Deleting $JSFilePath = $deleted\n";

$deleted = unlink($CSSFilePath);
echo "Deleting $CSSFilePath = $deleted\n";

$deleted = unlink($SecureJSFilePath);
echo "Deleting $SecureJSFilePath = $deleted\n";

$deleted = unlink($SecureCSSFilePath);
echo "Deleting $SecureCSSFilePath = $deleted\n";

echo "Finished Uploading JS and CSS\n";

?>
