<?php
if (PHP_SAPI !== 'cli') {
    die("This script should be run in command line mode\n");
}
require_once '../env/DB.config.php';
require_once '../env/System.config.php';
require_once '../env/Host.config.php';
require_once '../include/SimpleImage.php';
require_once '../AMAZONS3/util/S3.php';

define('S3_BUCKET', SystemConfig::$amazonS3Bucket);
define('CDN_BASE', HostConfig::$cdnBase.'/');

function url_save($url, $file) {
    $ch = curl_init($url);
    $fp = fopen($file, 'wb');
    curl_setopt($ch, CURLOPT_FILE, $fp);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_exec($ch);
    $res_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);
    fclose($fp);
    return $res_code === 200;
}

function url_exists($url) {
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_HEADER, FALSE);
    curl_setopt($ch, CURLOPT_NOBODY, TRUE);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, FALSE);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_exec($ch);
    $res_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);
    return $res_code === 200;
}

$db = mysql_connect(DBConfig::$ro['host'], DBConfig::$ro['user'], DBConfig::$ro['password']);
if (!$db) die('Error! DB read only connection failed. ' . mysql_error() . "\n");

mysql_select_db(DBConfig::$ro['db'], $db) or die('Error! DB select failed. ' . mysql_error() . "\n");

$done = array();
if (file_exists('resize_done.txt')) {
    $contents = file_get_contents('resize_done.txt');
    $ids = explode("\n", $contents);
    foreach ($ids as $id) {
        if (empty($id)) continue;
        $done[$id] = 1;
    }
}

$s3 = new S3(SystemConfig::$amazonAccessKey, SystemConfig::$amazonSecretKey);
$image = new SimpleImage();
$tmp = sys_get_temp_dir();
$headers = array();
$fp_log = fopen('resize_log.txt', 'a');
$fp_done = fopen('resize_done.txt', 'a');
$fp_error = fopen('resize_error.txt', 'a');
$count = 0;

$sql = "SELECT style_id, default_image FROM mk_style_properties ORDER BY style_id DESC";
$res = mysql_query($sql, $db);
$total = mysql_num_rows($res);
if (!$res) die('Error! DB query failed. ' . mysql_error() . "\n");
fputs($fp_log, "Total records to resize: $total \n");
echo "resizing ... \n";
$newImageSizes = array(array("width"=>81, "height"=>108, "name"=>"PDP Thumbnail"),
		 array("width"=>150, "height"=>200, "name"=>"Matchmaker"));
while ($row = mysql_fetch_assoc($res)) {
    $count += 1;
    $styleId = $row['style_id'];
    $process = false;
    foreach($newImageSizes as $imageSize){
        if (!isset($done[$styleId.$imageSize['name']])){
            $process = true;
        }
    }
    if($process == false){
        continue;
    }

    $defaultImage = $row['default_image'];
    if (strpos($defaultImage, CDN_BASE) === false) continue;

    printf("%05s/%05s resize\n", $count, $total);   
    fputs($fp_log, "$styleId downloading default image $defaultImage ... ");
    $originalFile = tempnam($tmp, 'style_');
    if (!url_save($defaultImage, $originalFile))
    {
        fputs($fp_log, "ERROR - image could not be saved\n");
        continue;
    }

    //resizing images
    foreach($newImageSizes as $imageSize)
    {
    	$width = $imageSize['width'];
    	$height = $imageSize['height'];
    	$imageTypeName = $imageSize['name'];
    	$imageNameTail = "_$width"."_$height.jpg";
    	$resizedFile = $originalFile.$imageNameTail;
    	$image->load($originalFile);
    	$image->resize($width, $height);
    	$image->save($resizedFile);
    	
    	$s3path = str_replace(array(CDN_BASE, '.jpg'), array('', $imageNameTail), $defaultImage);
    	$cdn_url = CDN_BASE . $s3path;
    	fputs($fp_log, "pushing $cdn_url ... ");
    	$s3->putObjectFile($resizedFile, S3_BUCKET, $s3path, S3::ACL_PUBLIC_READ, array(), $headers, 31536000);
    	
    	fputs($fp_log, "checking....");
    	if (url_exists($cdn_url)) {
    		fputs($fp_log, "successfully copied....\n");
    		fputs($fp_done, $styleId.$imageTypeName."\n");
    	}
    	else {
    		fputs($fp_log, "not copied....\n");
    		fputs($fp_error, $styleId.$imageTypeName."\n");
    	}
    	unlink($resizedFile);
    }
    unlink($originalFile);
}

mysql_close($db);
fclose($fp_done);
fclose($fp_error);
fclose($fp_log);

echo "\nDONE\n";

