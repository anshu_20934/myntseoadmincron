<?php
include_once("../auth.php");
include_once("../include/func/func.db.php");
include_once("../include/func/func.utilities.php");

$limit = 1000;
$offset = $argv[1]*1000;

date_default_timezone_set('Asia/Calcutta');
$date = date('Ymd');
//If an offset is specified via the command line args then use it else run the script for all products
if(!empty($argv[1])){
    $log = fopen("./logs/images/product_images_verify_$date"."_$offset.txt", "w");
    $query = "select * from mk_style_properties order by style_id desc limit $limit offset $offset";
}else{
    $log = fopen("./logs/images/product_images_verify_$date.txt", "w");
    $query = "select * from mk_style_properties order by style_id desc";
}
//echo "Running query $query";
logMsg("Started processing", "INFO");
$res = db_query($query, true);
$old_cdn_root = "cdn.myntra.com";
$cdn_root = "myntra.myntassets.com";
while($row = mysql_fetch_assoc($res)){
    $id = $row['style_id'];
    $default_image = $row['default_image'];
    $front_image = $row['front_image'];
    $left_image = $row['left_image'];
    $right_image = $row['right_image'];
    $back_image = $row['back_image'];
    $top_image = $row['top_image'];
    $bottom_image = $row['bottom_image'];
    $search_image = $row['search_image'];
    $search_zoom_image = $row['search_zoom_image'];
//echo "Processing style id :$id";
    logMsg("Processing style id : " . $id, "INFO");
    process_image($default_image, 'default_image', $id);
    process_image($front_image, 'front_image', $id);
    process_image($left_image, 'left_image', $id);
    process_image($right_image, 'right_image', $id);
    process_image($back_image, 'back_image', $id);
    process_image($top_image, 'top_image', $id);
    process_image($bottom_image, 'bottom_image', $id);
    process_image($search_image, 'search_image', $id);
//    process_image($search_zoom_image, 'search_zoom_image', $id);
    usleep(100000);
}
$query = "select distinct(size_chart_image) from mk_product_style";
$res = db_query($query, true);
while($row = mysql_fetch_assoc($res)){
    $size_chart_image = $row['size_chart_image'];
    process_image($size_chart_image, 'size_chart_image');
}

$query = "select logo,top_banner from mk_filters";
$res = db_query($query, true);
while($row = mysql_fetch_assoc($res)){
    $logo_image = $row['logo'];
    $top_banner_image = $row['top_banner'];
    process_image($logo_image, 'logo');
    process_image($top_banner_image, 'top_banner');
}

$query = "select image from mk_widget_banners";
$res = db_query($query, true);
while($row = mysql_fetch_assoc($res)){
    $banner_image = $row['image'];
    process_image($banner_image, 'banner_image');
}

function process_image($image_url, $field_name, $id){
    global $log, $old_cdn_root, $cdn_root, $images_root, $cdn_base;
    if(empty($image_url) || $image_url == "$cdn_base"){
        return;
    }
    if(substr($image_url,0,2) == './' || (strpos($image_url, $cdn_root) == false && strpos($image_url, $old_cdn_root) == false)){
        logMsg("Image :".$image_url." not on cdn URL","ERROR");
        return;
    }
    $image_url = str_replace('cdn.myntra','myntra.myntassets',$image_url);
    $sizes = array( 
        'default_image' => array("_images.", "_images_48_64.", "_images_96_128.", "_images_360_480.", "_images_540_720.", "_images_1080_1440."),
        'front_image' => array("_images.", "_images_48_64.", "_images_360_480.", "_images_540_720.", "_images_1080_1440."),
        'left_image' => array("_images.", "_images_48_64.", "_images_360_480.", "_images_540_720.", "_images_1080_1440."),
        'right_image' => array("_images.", "_images_48_64.", "_images_360_480.", "_images_540_720.", "_images_1080_1440."),
        'back_image' => array("_images.", "_images_48_64.", "_images_360_480.", "_images_540_720.", "_images_1080_1440."),
        'top_image' => array("_images.", "_images_48_64.", "_images_360_480.", "_images_540_720.", "_images_1080_1440."),
        'bottom_image' => array("_images.", "_images_48_64.", "_images_360_480.", "_images_540_720.", "_images_1080_1440."),
        'search_image' => array("_images.","_images_180_240","_images_240_320"),
        'search_zoom_image' => array("_images.", "_images_240_320."),
        'banner_image' => array("."),
        'filter_logo_image' => array("."),
        'filter_banner_image' => array("."),
        'size_chart_image' => array("."));

    foreach($sizes[$field_name] as $size){
        $image = str_replace("_images.", $size, $image_url);
        logMsg("Processing : $field_name : $image", "INFO");
        $tries = 1;
        $found = false;
        while($tries<3){
            if(isPresentOnAkamai($image)){
                $found = true;
                break;
            }
            $tries++;
        }
        if($found){
            logMsg("Image $image found on Akamai servers after $tries tries", "SUCCESS");
        }else{
            logMsg("Image $image not found on Akamai servers after $tries tries", "ERROR");
        }
    }
}
fclose($log);

function logMsg($msg, $level){
    global $log;
    fwrite($log, "$level : $msg\n");
}
/**
 * Checks weather the image is present on akamai edge servers.
 * @param  string $url
 * @desc   makes a HEAD request using cURL retrieving the headers. Also adds pragma headers for akamai edge servers to return with extra cache headers
 * @return cache header value in case of cache hit else false on 404 or cache misses
 */
function isPresentOnAkamai($url) {
    if (!extension_loaded('curl_init') || !function_exists('curl_init')) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_TIMEOUT, 3); // times out after 30s
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_NOBODY, 1);
        curl_setopt ($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Pragma: akamai-x-cache-on, akamai-x-cache-remote-on, akamai-x-check-cacheable, akamai-x-get-cache-key, akamai-x-get-extracted-values, akamai-x-get-nonces, akamai-x-get-ssl-client-session-id, akamai-x-get-true-cache-key, akamai-x-serial-no'));
        $response = curl_exec($ch);
        curl_close($ch);
        //  get the status code from HTTP headers 
        if(preg_match('/HTTP\/1\.\d+\s+(\d+)/', $response, $codematches)){
            $code=intval($codematches[1]);
        }else{
            logMsg("Returning false as http status code for $url is not found in the header", "MISSING-IMAGE-ERROR");
            return false;
        }
        if($code == 200 && preg_match('/X-Cache: TCP_(\w+)/', $response, $cachematches)){
            if(strpos($cachematches[1], 'HIT') !== FALSE){
                //echo "returning true as cache matches is hit ".$cachematches[1]."\n";
                return true;
            }else{
                logMsg("Returning false as cache is not hit : ".$cachematches[1], "ERROR_INFO");
                return false;
            }
        }
        return false;
    }else{
        die("Can not proceed as curl is not loaded");
    }
}

