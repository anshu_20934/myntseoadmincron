<?php
class FileUtils{
	public static function parse_properties_file($file){
		$fileHandle=fopen($file,"r");
		$data=fread($fileHandle,filesize($file));
		$splits=split("[\n|\r]",$data);
		$result=array();
		foreach ($splits as $split)
		{
			if(trim($split)==''){
				echo $split;
			continue;
			}
			$split=split('=',$split);
			$result[trim($split[0])]=trim($split[1]);
		}
		fclose($fileHandle);
		return $result;
	}

}
?>