#!/bin/sh
source /home/myntradomain/server_variables
ps aux | grep java | grep "catalina.base=$PORTAL_SERVICE_HOME" |  grep -v grep
if [ $? -ne 0 ]
then
cd $PORTAL_SERVICE_HOME
echo "Shutting down tomcat just to be sure. Ignore the ConnectException."
./bin/shutdown.sh 
echo "Zzzzzzzz for 15 secs"
sleep 15
echo "Starting tomcat."
./bin/startup.sh > /dev/null &
echo "Restarted"
fi
