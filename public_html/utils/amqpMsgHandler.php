<?php 
/**
* $Id$
*
* Message handlers for AMQP consumer
* Definitions in this file use System_Daemon for logging
*/

include_once 'System/Daemon.php';
require_once '../env/AMQP.config.php';
require_once '../env/System.config.php';
require_once '../env/Host.config.php';
require_once '../lib/cloudinary/Cloudinary.php';
require_once '../lib/cloudinary/Uploader.php';
require_once '../env/Cloudinary.config.php';
require_once '../include/func/func.utilities.php';
if(!defined(IMAGES_BASE_FOLDER))
    define('IMAGES_BASE_FOLDER', AMQPConfig::$imagesBaseFolder);
if(!defined(DEFAULT_BUCKET_NAME))
    define('DEFAULT_BUCKET_NAME', SystemConfig::$amazonS3Bucket);
if(!defined(S3_PATH_BASE))
    define('S3_PATH_BASE', HostConfig::$cdnBase."/");
if(!defined(CLOUDINARY_PATH_BASE))
    define('CLOUDINARY_PATH_BASE', CloudinaryConfig::$basePath);
    
use imageUtils\Jpegmini;

/**
* Abstract message handler class
*/
abstract class amqpMsgHandler {
    /**
    * Processes a messages returns NULL or error message
    */
    abstract public function processMessage($msg);
}


/**
* Global settings are to be used here, once those files are available on all machines
*/

define('DBSERVER', AMQPConfig::$dbServer);
define('DBPORT', AMQPConfig::$dbPort);
define('DBUSER', AMQPConfig::$dbUser);
define('DBPASS', AMQPConfig::$dbPass);
define('DBNAME', AMQPConfig::$dbName);

// Message handlers for Myntra message queue

abstract class cdnMsgHandler extends amqpMsgHandler {
    protected function transferFile($filepath, $uri) {
        return moveToS3($filepath, DEFAULT_BUCKET_NAME,$uri,null,7776000); 
    }

    protected function transferFileToCloudinary($filepath, $uri, $uploadOptions, $eagerParams) {
        return moveToCloudinary($filepath, $uri, $uploadOptions, $eagerParams); 
    }

    protected function sanitize_path($path) {
        $ret = $path;
        if ($this->string_begins_with($ret, IMAGES_BASE_FOLDER))
            $ret = substr($ret, strlen(IMAGES_BASE_FOLDER));
        if ($this->string_begins_with($ret, './images'))
            $ret = substr($ret, 2);
        if ($this->string_begins_with($ret, '../images'))
            $ret = substr($ret, 3);     
        if ($this->string_begins_with($ret, '/images'))
            $ret = substr($ret, 1);
        if ($this->string_begins_with($ret, HostConfig::$cdnBase))
            $ret = substr($ret, strlen(HostConfig::$cdnBase)+1);
        if ($this->string_begins_with($ret, CloudinaryConfig::$basePath))
            $ret = substr($ret, strlen(CloudinaryConfig::$basePath));
        return $ret;
    }

    protected function string_begins_with($string, $search) {
        return (strncmp($string, $search, strlen($search)) == 0);
    }

    protected function url_exists($url) { 
        $hdrs = @get_headers($url); 
        return is_array($hdrs) ? preg_match('/^HTTP\\/\\d+\\.\\d+\\s+2\\d\\d\\s+.*$/',$hdrs[0]) : false; 
    }
}

class cdnTransferMsgHandler extends cdnMsgHandler {
    
    private function updateSolr($tablename, $fieldname, $fieldvalue){
        switch($tablename){
            case "mk_style_properties":
                {
                    if($fieldname == "id"){
                        $style_id = func_query_first_cell("select style_id from mk_style_properties where id = " . $fieldvalue);
                        if(!empty($style_id)){
                            fireAsync(HostConfig::$baseUrl, "include/solr/update_style.php?styleid=" . $style_id);
                        }
                    }else if($fieldname == "style_id"){
                        fireAsync(HostConfig::$baseUrl, "include/solr/update_style.php?styleid=" . $fieldvalue);
                    }
                    break;
                }
        }
    }

    private function func_query_first_cell($query) {
	$res = \mysql_query($query);
	if ($res) {
		$result = db_fetch_row($res);
		db_free_result($res);
	}
	return is_array($result) ? $result[0] : false;
    }
    
    public function processMessage($msg) {
    	$msg = $msg->body;
        $params = unserialize($msg);

        \mysql_connect(DBSERVER.':'.DBPORT, DBUSER, DBPASS);
        @\mysql_select_db(DBNAME) or die( "Unable to select database");

        $tablename = \mysql_real_escape_string($params['tablename']);
        $fieldname = \mysql_real_escape_string($params['fieldname']);
        $idfieldvalue = \mysql_real_escape_string($params['idfieldvalue']);
        $idfieldname = \mysql_real_escape_string($params['idfieldname']);
        \System_Daemon::info('Transfer: %s[%s] where %s=%s', 
                $tablename, $fieldname, $idfieldname, $idfieldvalue);

        $query = "SELECT $fieldname FROM $tablename WHERE $idfieldname='$idfieldvalue'";

        $result = \mysql_query($query);
        if (!$result) {
            \System_Daemon::err('Transfer: Error executing %s', $query);
            return false;
        }

        $num = \mysql_numrows($result);
        if ($num != 1) {
            \System_Daemon::err('Transfer: %s results for %s', $num, $query);
            \mysql_free_result($result);
            return false;
        }

        $arr = \mysql_fetch_row($result);
        \mysql_free_result($result);

        $path = $this->sanitize_path($arr[0]);
        $filepath = IMAGES_BASE_FOLDER.$path;
        $s3ImgPath = S3_PATH_BASE.$path;
        if ($this->url_exists($s3ImgPath)) {
            \System_Daemon::info('Transfer: %s found at %s', $filepath, $s3ImgPath);
        } else {
            if (!file_exists($filepath)) {
                \System_Daemon::err('Transfer: File %s not found', $filepath);
                return false;
            }
            if ($this->transferFile($filepath, $path)) {
                \System_Daemon::info('Transfer: : Copied %s to %s', $filepath, $s3ImgPath);
            } else {
                \System_Daemon::err('Transfer: Failed to upload %s', $filepath);
                return false;
            }
        }
        // Replace the entry in table
        $query = "UPDATE $tablename SET $fieldname='$s3ImgPath' WHERE $idfieldname='$idfieldvalue'";
        \mysql_query($query);
        $affected_rows = \mysql_affected_rows();
        if ($affected_rows != 1) {
            \System_Daemon::err('Transfer: [PANIC] Updated %s entries for %s[%s] where %s=%s',
                    $affected_rows, $tablename, $fieldname, $idfieldname, $idfieldvalue);
        } else {
            // Transfer successful, let's update solr index where needed
            $this->updateSolr($tablename, $idfieldname, $idfieldvalue);
        }
        \mysql_close();
    }
}

class cdnBackupMsgHandler extends cdnMsgHandler {
    public function processMessage($msg) {
    	$msg = $msg->body;
        \System_Daemon::info('Backup: %s', $msg);
        
        if ((!isset($msg))||(strlen($msg) == 0)) {
            \System_Daemon::err('Backup: Empty file path %s', $filepath);
            return false;
        }

        $path = $this->sanitize_path($msg);
        $filepath = IMAGES_BASE_FOLDER.$path;

        if (!file_exists($filepath)) {
            \System_Daemon::err('Backup: File %s not found', $filepath);
            return false;
        }
        if ($this->transferFile($filepath, $path)) {
            $s3ImgPath = S3_PATH_BASE.$path;
            \System_Daemon::info("Backup: Copied %s to $s3ImgPath", $filepath);
        } else {
            \System_Daemon::err('Backup: Failed to upload %s', $filepath);
        }
    }
}

class cdnCompressMsgHandler extends cdnMsgHandler {
    public function processMessage($message) {
        $msg = $message->body;
        \System_Daemon::info('Compressing: %s', $msg);
        
        if ((!isset($msg))||(strlen($msg) == 0)) {
            \System_Daemon::err('Compress: Empty file path %s', $filepath);
            $message->delivery_info['channel']->basic_ack($message->delivery_info['delivery_tag']);
            return false;
        }

        $path = $this->sanitize_path($msg);
        $filepath = IMAGES_BASE_FOLDER."/".$path;
        if (!file_exists($filepath)) {
            $s3path = S3_PATH_BASE."/".$path;
            \System_Daemon::info("Compress: Copying file from $s3path to $filepath");
            mkdir(dirname($filepath), 0777, true);
            copy(S3_PATH_BASE."/".$path, $filepath);
        }

        if (!file_exists($filepath)) {
            \System_Daemon::err('Compress: File %s not found', $filepath);
            $message->delivery_info['channel']->basic_ack($message->delivery_info['delivery_tag']);
            return false;
        }
        $compressedFilePath = Jpegmini::getCompressedImagePathFromOriginalPath($filepath);
        $tries = 0;
        $compressed = false;
        //Make three attempts to compress and image and in case it would not compress then set the compressed flad as false
        do{
            if($tries > 0){
                sleep(5);//Wait for 5 seconds in case of a failure
                \System_Daemon::err('Compress: Failed to compress %s retrying %s time', $filepath, $tries);
            }
            $compressed = Jpegmini::compress($filepath);
            $tries++;
        }while($tries < 3 && $compressed == false);

        if($compressed == false){
            \System_Daemon::err('Compress: Failed to compress %s', $filepath);
            $message->delivery_info['channel']->basic_ack($message->delivery_info['delivery_tag']);
            return false;
        }
        $compressedS3Path = Jpegmini::getCompressedImagePathFromOriginalPath($path);
        if ($this->transferFile($compressedFilePath, $compressedS3Path)) {
            $s3ImgPath = S3_PATH_BASE.$compressedS3Path;
            \System_Daemon::info("Compress: Successfully Copied %s to $s3ImgPath", $compressedFilePath);
        } else {
            \System_Daemon::err('Compress: Failed to upload %s', $compressedFilePath);
        }
        $message->delivery_info['channel']->basic_ack($message->delivery_info['delivery_tag']);
    }
}

class cdnCompressToCloudinaryMsgHandler extends cdnMsgHandler {
    public function processMessage($message) {
        $msg = $message->body;
        $params = unserialize($msg);
        \System_Daemon::info('Cloudinary Compressing: %s', print_r($params, true));
        $msg = $params['imagePath'];
        $uploadOptions = $params['uploadOptions'];
        $eagerParams = $params['eagerParams'];
        $resolutions = $params['resolutions'];
        $resolutionsQuality = $params['resolutionsQuality'];
        $eagerResolutions = array();

        if(empty($eagerParams)){
            $eagerParams = array();
        }
        $resolutions = explode(",", $resolutions);
        if(empty($resolutionsQuality)){
            $resolutionsQuality = 95;
        }

        foreach($resolutions as $resolution){
            $resolutionArr = explode("X", $resolution);
            $width = $resolutionArr[0];
            $height = $resolutionArr[1];
            if(empty($width) || empty($height) || !is_int(intval($width)) || !is_int(intval($height))){
                continue;
            }
            $eagerResolution = array("width"=>$width, "height"=>$height, "quality"=>$resolutionsQuality);
            array_push($eagerParams, $eagerResolution);
        }

        if ((!isset($msg))||(strlen($msg) == 0)) {
            \System_Daemon::err('Cloudinary Compress: Empty file path %s', $filepath);
            $message->delivery_info['channel']->basic_ack($message->delivery_info['delivery_tag']);
            return false;
        }

        $path = $this->sanitize_path($msg);
        $filepath = IMAGES_BASE_FOLDER."/".$path;
        if (!file_exists($filepath)) {
            $cdnPath = $msg;
            \System_Daemon::info("Cloudinary Compress: Copying file from $cdnPath to $filepath");
            mkdir(dirname($filepath), 0777, true);
            copy($cdnPath, $filepath);
        }

        if (!file_exists($filepath)) {
            \System_Daemon::err('Cloudinary Compress: File %s not found', $filepath);
            $message->delivery_info['channel']->basic_ack($message->delivery_info['delivery_tag']);
            return false;
        }
        $compressedFilePath = Jpegmini::getCompressedImagePathFromOriginalPath($filepath);
        $tries = 0;
        $compressed = false;
        //Make three attempts to compress and image and in case it would not compress then set the compressed flad as false
        do{
            if($tries > 0){
                sleep(5);//Wait for 5 seconds in case of a failure
                \System_Daemon::err('Cloudinary Compress: Failed to compress %s retrying %s time', $filepath, $tries);
            }
            $compressed = Jpegmini::compress($filepath);
            $tries++;
        }while($tries < 3 && $compressed == false);

        if($compressed == false){
            \System_Daemon::err('Cloudinary Compress: Failed to compress %s', $filepath);
            $message->delivery_info['channel']->basic_ack($message->delivery_info['delivery_tag']);
            return false;
        }
        $compressedCDNPath = Jpegmini::getCompressedImagePathFromOriginalPath($path);

        \System_Daemon::info("Cloudinary Compress: Tansferring file to cloudinary with path %s : uploadOptions '%s' : eagerPrams '%s'", $compressedCDNPath, print_r($uploadOptions, true), print_r($eagerParams, true));
        if ($this->transferFileToCloudinary($compressedFilePath, $compressedCDNPath, $uploadOptions, $eagerParams)) {
            $uploadImgPath = CLOUDINARY_PATH_BASE.$compressedCDNPath;
            \System_Daemon::info("Cloudinary Compress: Successfully Copied %s to $uploadImgPath", $compressedFilePath);
        } else {
            \System_Daemon::err('Cloudinary Compress: Failed to upload %s', $compressedFilePath);
        }
        $message->delivery_info['channel']->basic_ack($message->delivery_info['delivery_tag']);
    }
}

class datebaseThrottleMsgHandler extends cdnMsgHandler {
    public function processMessage($msg) {
    	$msg = $msg->body;
        \System_Daemon::info('Throttling Writes to DB: ');
        
        \mysql_connect(DBSERVER.':'.DBPORT, DBUSER, DBPASS);
        @\mysql_select_db(DBNAME) or die( "Unable to select database");
        
        $updatesPerBatch = AMQPConfig::$updatesPerBatch;
        $sleepTime = AMQPConfig::$sleepTime;
        
        $messageArray = json_decode($msg);
        $count = 0;
        foreach ($messageArray as $query){
        	// finished one slot, sleep and then begin processing
        	if($count == $updatesPerBatch) {
        		usleep($sleepTime);
        		$count = 0;
        	}
        	
        	$result = \mysql_query($query);
        	if (!$result) {
            	\System_Daemon::err('DBThrottle: Error executing %s', $query);
        	}
			$count++;
        }	              
    }
}
?>
