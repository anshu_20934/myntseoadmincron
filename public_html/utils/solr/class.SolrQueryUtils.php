<?php
/**
 * SolrQueryUtils
 * @author lohiya
 *
 */

include_once "$xcart_dir/include/dao/class/search/class.MyntraPageDBAdapter.php";
class SolrQueryUtils{

	public static function getSolrQuery(){
		if (RequestUtils::getRequestType() == RequestType::$PARAMETRIZED ){
			$dbAdapter=MyntraPageDBAdapter::getInstance();
			return $dbAdapter->getSolrQueryForKey(RequestUtils::getParametrizedKey());
				
		}else if(RequestUtils::getRequestType() == RequestType::$LANDING_PAGE){

			$dbAdapter=MyntraPageDBAdapter::getInstance();
			return $dbAdapter->getSolrQueryForLandingPage(RequestUtils::getLandingPageURL());
		}
	}
	
	public static function getSolrQueryForActiveStyle(){
		$tempquery = SolrQueryUtils::getSolrQuery();
		if(!empty($tempquery))
		{
			$tempquery = $tempquery." AND (count_options_availbale:[1 TO *])";
		}
		else 
		{
			$tempquery = $tempquery." (count_options_availbale:[1 TO *])";
		}
		return $tempquery;
	}
	
	/**
	 * this function returns the curated data for a parameterized search
	 * @param string $page_url : landing page url or the original search phrase
	 */
	public static function getCuratedSearchData($page_url = "") {
		if (RequestUtils::getRequestType() == RequestType::$PARAMETRIZED ) {
			$dbAdapter=MyntraPageDBAdapter::getInstance();
			return $dbAdapter->getCuratedSearchDataForKey(RequestUtils::getParametrizedKey());
		} else if(RequestUtils::getRequestType() == RequestType::$LANDING_PAGE) {
			$dbAdapter=MyntraPageDBAdapter::getInstance();
			return $dbAdapter->getCuratedSearchDataForLandingPage(RequestUtils::getLandingPageURL());
		} else {
			$dbAdapter=MyntraPageDBAdapter::getInstance();
			return $dbAdapter->getCuratedSearchDataForLandingPage($page_url);
		}
	}
}


?>