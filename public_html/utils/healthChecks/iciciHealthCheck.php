<?php
use enums\revenue\payments\Gateways;
include_once '../../authAPI.php';
include_once("$xcart_dir/Sfa/Merchant.php");
include_once("$xcart_dir/Sfa/PGResponse.php");
include_once("$xcart_dir/Sfa/PGSearchResponse.php");
include_once("$xcart_dir/Sfa/PostLibPHP.php");

$oneHourBefore = time()-3600;
$sixHourBefore = time()-6*3600;
$oneDayBefore = time()-24*3600;

$orderQuery  = "select gateway_payment_id from mk_payments_log where  time_insert> $sixHourBefore and time_insert< $oneHourBefore and payment_gateway_name='".Gateways::ICICIBank."' and is_complete=1 order by orderid desc limit 1";

$transactionid = func_query_first_cell($orderQuery,true);

if(empty($transactionid)) {
	//try to find orderid in last one day.
	$orderQuery  = "select gateway_payment_id from mk_payments_log where  time_insert> $oneDayBefore and time_insert< $oneHourBefore and payment_gateway_name='".Gateways::ICICIBank."' and is_complete=1 order by orderid desc limit 1";
	$transactionid = func_query_first_cell($orderQuery,true);
}

if(empty($transactionid)) {
	echo "OK";exit;
}


$merchandID = "00005357";

$oPostLibphp    =       new     PostLibPHP();
$oMerchant      =       new     Merchant();
$oPGSearchResp =       new     PGSearchResponse();
try {
	$oMerchant->setMerchantOnlineInquiry($merchandID,$transactionid);
	$oPGSearchResp=$oPostLibphp->postStatusInq($oMerchant);
	$arrayPGRespObj = $oPGSearchResp->getPGResponseObjects();
	echo "OK";exit;
} catch (Exception $e) {
	 echo java_inspect($e);
	 exit;        
}

?>

