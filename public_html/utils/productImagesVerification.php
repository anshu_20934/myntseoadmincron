<?php

if (PHP_SAPI !== 'cli') {
    die("This script should be run in command line mode\n");
}

error_reporting(1);
ini_set('memory_limit','512M');
define("QUICK_START",1);
require_once '../env.php';
$xcart_dir = HostConfig::$documentRoot;
require_once HostConfig::$documentRoot.'/logger.php';
require_once HostConfig::$documentRoot.'/include/SimpleImage.php';
require_once HostConfig::$documentRoot.'/AMAZONS3/util/S3.php';
require_once HostConfig::$documentRoot.'/include/func/func.utilities.php';
include_once HostConfig::$documentRoot.'/include/class/MClassLoader.php';
MClassLoader::addToClassPath(HostConfig::$documentRoot."/include/class/");

use imageUtils\Jpegmini;

define('S3_BUCKET', SystemConfig::$amazonS3Bucket);
define('CDN_BASE', HostConfig::$cdnBase.'/');

$indexDir = 'index';
$imagesIndexRelPath = $indexDir.'/INDEX_TYPE-images-index.txt';
$imagesIndexURLBase = "http://".S3_BUCKET.".s3.amazonaws.com/images/";

// Index variables
$imagesIndex = array();
$imagesIndexFile = '';
$date = date('Ymd');

$log = fopen("./logs/images/productImagesVerification_$date.txt", "w");
$maillogfile = "./logs/images/productImagesVerification_maillog_$date.txt";
$maillog = fopen($maillogfile, "w");
$cdn_root = substr(HostConfig::$cdnBase, strlen("http://"));
$images_root = "/home/myntradomain/public_html";
$s3 = new S3(SystemConfig::$amazonAccessKey, SystemConfig::$amazonSecretKey);
//rrmdir($indexDir);
if(!is_dir($indexDir)){
    mkdir($indexDir);
}

$styleSizeMap = array(
        'default_image' => array("_images.", "_images_48_64.", "_images_81_108.", "_images_96_128.", "_images_150_200.", "_images_360_480.", "_images_540_720.", "_images_1080_1440."),
        'front_image' => array("_images.", "_images_48_64.", "_images_81_108.", "_images_360_480.", "_images_540_720.", "_images_1080_1440."),
        'left_image' => array("_images.", "_images_48_64.", "_images_81_108.", "_images_360_480.", "_images_540_720.", "_images_1080_1440."),
        'right_image' => array("_images.", "_images_48_64.", "_images_81_108.", "_images_360_480.", "_images_540_720.", "_images_1080_1440."),
        'back_image' => array("_images.", "_images_48_64.", "_images_81_108.", "_images_360_480.", "_images_540_720.", "_images_1080_1440."),
        'top_image' => array("_images.", "_images_48_64.", "_images_81_108.", "_images_360_480.", "_images_540_720.", "_images_1080_1440."),
        'bottom_image' => array("_images.", "_images_48_64.", "_images_81_108.", "_images_360_480.", "_images_540_720.", "_images_1080_1440."),
        'search_image' => array("_images."),
        'tex_color_image' => array("_images.")

);

function logMsg($msg, $errorlogFile=null){
    global $log;
    fputs($log, $msg."\n");
    if(!empty($errorlogFile)){
        fputs($errorlogFile, $msg."\n");
    }
}


function getIndex($id, $type){
    global $imagesIndexFile, $imagesIndex, $indexDir, $imagesIndexRelPath, $imagesIndexURLBase, $s3;

    logMsg("DEBUG:$id:$type:Getting index");
    $indexFile = str_replace("INDEX_TYPE",$type, $imagesIndexRelPath);
    if($type == "style"){
        $id = intval($id/1000);
        $indexFile = str_replace(".txt", "$id.txt", $indexFile);
    }
    $indexFileLocationLocal = $indexFile;
    $indexFileLocationS3 = $imagesIndexURLBase.$indexFile;
    $indexFileRelPath = "images/$indexFileLocationLocal";
    if($imagesIndexFile == $indexFile){
        logMsg("DEBUG:Returning as $indexFile is the current index file in use : $imagesIndexFile");
        return $imagesIndex;
    }
    logMsg("DEBUG:Saving current index : $imagesIndexFile and retrieving new one : $indexFileLocationS3");
    saveIndex();
    //Now we have to download the index or use an existing downloaded one
    if(!file_exists($indexFileLocationLocal)){
        $indexContentsS3 = $s3->getObject(S3_BUCKET, $indexFileRelPath, $indexFileLocationLocal);
        if($indexContentsS3 == false){
            //File  not found on local or on S3 so we need to create a new file
            logMsg("DEBUG:Not able to download indexFile from S3 from $indexFileLocationS3 creating a new index file");
        }else {
            logMsg("DEBUG:Successfully downloaded indexFile from S3 from $indexFileLocationS3 ");
        }
    }
    $imagesIndexFile = $indexFileLocationLocal;
    syncIndexFromFile($imagesIndexFile);
    return $imagesIndex;
}

function syncIndexFromFile($fileLocation){
    global $imagesIndex;
    if(empty($fileLocation) || !file_exists($fileLocation)){
        logMsg("ERROR:$fileLocation:file is empty or does not exist");
        return;
    }
    $imagesIndex = unserialize(file_get_contents($fileLocation, FILE_USE_INCLUDE_PATH));
    //logMsg("DEBUG:$fileLocation:imagesIndex is".print_r($imagesIndex,true));
    return $imagesIndex;
}

function saveIndex(){
    global $imagesIndexFile, $imagesIndex;
    if(empty($imagesIndexFile) || empty($imagesIndex)){
        logMsg("DEBUG:Index or index file location is empty returning");
    }
    //Save and Upload the currently active index
    file_put_contents($imagesIndexFile, serialize($imagesIndex));
    if(moveToS3($imagesIndexFile, S3_BUCKET, "images/$imagesIndexFile")){
        logMsg("DEBUG:Successfully moved $imagesIndexFile to S3");
    }
}

function processImage($id,$image,$logPrefix=''){
    global $images_root, $imagesIndex, $s3, $maillog;
    if ( strpos($image, HostConfig::$cdnBase) == false && strpos($image, HostConfig::$cdnBase) != 0 ){
        logMsg("DEBUG:$logPrefix:$image:not a cdn url");
        return;
    }
    if ( (strlen($image) - strlen(HostConfig::$cdnBase)) < 10 ){//Magic number 10 used for checking if image url seems valid or not
        logMsg("DEBUG:$logPrefix:$image:not correct cdn url");
        return;
    }
    $compressedImage = Jpegmini::getCompressedImagePathFromOriginalPath($image);
    $imageRelPath = substr($image, strlen(HostConfig::$cdnBase));
    $compressedImageRelPath = substr($compressedImage, strlen(HostConfig::$cdnBase));
    $imageDiskPath = $images_root . substr($image, strlen(HostConfig::$cdnBase));
    $compressedImageDiskPath = $images_root . substr($compressedImage, strlen(HostConfig::$cdnBase));

    if(!isset($imagesIndex["$id:$imageRelPath"])){
        $imageUrl = str_replace(" ", "%20", $image);
        if(!url_exists($imageUrl)){
            logMsg("$logPrefix:$image:$imageDiskPath:Initiating transfer to s3");
            if ( !file_exists($imageDiskPath) ){
                logMsg("ERROR:$logPrefix:$image:$imageDiskPath:not found on S3 and local disk", $maillog);
                //We can improve this by downloading original image and scaling it to correct resolution and then storing it on S3
            } else {
                if(moveToDefaultS3Bucket($imageRelPath)){
                    logMsg("SUCCESS:$logPrefix:$image:$imageDiskPath:Successfully transfered image from local disk to S3", $maillog);
                    $imagesIndex["$id:$imageRelPath"] = array(); //Setting this as array for future use
                }else{
                    logMsg("ERROR:$logPrefix:$image:$imageDiskPath:Image could not be transfered to S3 from local disk", $maillog);
                }
            }
        } else {
             //Do nothing as image already exists on S3. Just update the index.
             logMsg("SUCCESS:$logPrefix:$image:Image already exists on S3");
             $imagesIndex["$id:$imageRelPath"] = array();
        }
    }else{
        logMsg("SUCCESS:$logPrefix:$image:Image already processed and exists in index");        
        $imagesIndex["$id:$imageRelPath"] = array();
    }
    //For compressed Image
    if(!isset($imagesIndex["$id:$compressedImageRelPath"])){
        $originalInfo = $s3->getObjectInfo(S3_BUCKET, substr($imageRelPath, 1));
        $compressedInfo = $s3->getObjectInfo(S3_BUCKET, substr($compressedImageRelPath, 1));

        // Compress in case the compressed file is not present or if it is present then check for file size to run compression only if it is placeholder file.
        if($compressedInfo == false || $originalInfo['size'] == $compressedInfo['size']){
            logMsg("$logPrefix:$compressedImage:$compressedImageRelPath:Initiating transfer to s3");
            if(Jpegmini::compress($imageDiskPath) && moveToS3($compressedImageDiskPath, S3_BUCKET, substr($compressedImageRelPath, 1))){
                $compressedInfo = $s3->getObjectInfo(S3_BUCKET, substr($compressedImageRelPath, 1));
                if($compressedInfo == false || $originalInfo['size'] == $compressedInfo['size']){
                    logMsg("ERROR:$logPrefix:$compressedImage:Error after compressing image and sending to S3. Either file is not sent to S3 correctly or File sizes are identical. Original size='".$originalInfo['size']."' Compressed file size='".$compressedInfo['size']."'", $maillog);
                } else {
                    logMsg("SUCCESS:$logPrefix:$compressedImage:Successfully compressed and saved to s3");
                    $imagesIndex["$id:$imageRelPath"] = array();
                }
            }else{
                logMsg("ERROR:$logPrefix:$compressedImage:Error while compressing image or sending it to S3", $maillog);
            }
        } else {
             //Do nothing as image already exists on S3. Just update the index.
             logMsg("SUCCESS:$logPrefix:$compressedImage:Image already exists on S3");
             $imagesIndex["$id:$compressedImageRelPath"] = array();
        }
    }else{
        logMsg("SUCCESS:$logPrefix:$compressedImage:Image already processed and exists in index");
        $imagesIndex["$id:$compressedImageRelPath"] = array();
    }
}

function processStyleImage($image_url, $field_name, $id){
    global $styleSizeMap;
    foreach($styleSizeMap[$field_name] as $size){
        $image = str_replace("_images.", $size, $image_url);
        logMsg("DEBUG:$id:$field_name:$image:Processing");
        processImage($id,$image,"$id:$field_name");
    }
}

logMsg("DEUBG:Started processing");
$db = mysql_connect(DBConfig::$ro['host'], DBConfig::$ro['user'], DBConfig::$ro['password']);
if (!$db) {
    logMsg('ERROR: DB read only connection failed. ' . mysql_error(), $maillog);
    die('Error! DB read only connection failed. ' . mysql_error() . "\n");
}
if(!mysql_select_db(DBConfig::$ro['db'], $db)){
    logMsg('ERROR: DB select failed . ' . mysql_error(), $maillog);
    die('Error! DB select failed . ' . mysql_error() . "\n");
}

//Processing style images
$sql = "SELECT style_id,default_image,front_image,back_image,left_image,right_image,top_image,bottom_image,search_image,tex_color_image FROM mk_style_properties ORDER BY style_id DESC";
$res = mysql_query($sql, $db);
$total = mysql_num_rows($res);
if (!$res) die('Error! DB query failed. ' . mysql_error() . "\n");
logMsg("Total records to resize: $total");
logMsg("Processing ...");

while ($row = mysql_fetch_assoc($res)) {
    $styleId = $row['style_id'];
    logMsg("DEBUG:$styleId:Processing started");
    $default_image = $row['default_image'];
    $front_image = $row['front_image'];
    $back_image = $row['back_image'];
    $left_image = $row['left_image'];
    $right_image = $row['right_image'];
    $top_image = $row['top_image'];
    $bottom_image = $row['bottom_image'];
    $search_image = $row['search_image'];
    $tex_color_image = $row['tex_color_image'];
    $index = getIndex($styleId,'style');
    processStyleImage($default_image, 'default_image', $styleId);
    processStyleImage($front_image, 'front_image', $styleId);
    processStyleImage($back_image, 'back_image', $styleId);
    processStyleImage($left_image, 'left_image', $styleId);
    processStyleImage($right_image, 'right_image', $styleId);
    processStyleImage($top_image, 'top_image', $styleId);
    processStyleImage($bottom_image, 'bottom_image', $styleId);
    processStyleImage($search_image, 'search_image', $styleId);
    processStyleImage($tex_color_image, 'tex_color_image', $styleId);
}
//Save and Upload the currently active index
saveIndex();

//Process banner images
$sql = "select id,image from mk_widget_banners";
$res = mysql_query($sql, $db);
$index = getIndex(null,'banner');

while ($row = mysql_fetch_assoc($res)) {
    $id = $row["id"];
    $image = $row["image"];
    processImage($id,$image,"$id");
}
//Save and Upload the currently active index
saveIndex();

//Process filter logo and banner
$sql = "select id,logo,top_banner from mk_filters";
$res = mysql_query($sql, $db);
$index = getIndex(null,'filters');

while ($row = mysql_fetch_assoc($res)) {
    $id = $row["id"];
    $logo = $row["logo"];
    $top_banner = $row["top_banner"];
    processImage($id,$logo,"$id:logo");
    processImage($id,$top_banner,"$id:top_banner");
}
//Save and Upload the currently active index
saveIndex();

//Process page config images
$sql = "select image_id,image_url from page_config_image_reference";
$res = mysql_query($sql, $db);
$index = getIndex(null,'pageconfig');

while ($row = mysql_fetch_assoc($res)) {
    $id = $row["image_id"];
    $image = $row["image_url"];
    processImage($id,$image,"$id");
}
//Save and Upload the currently active index
saveIndex();

mysql_close($db);
fclose($log);
fclose($maillog);

$handle = fopen($maillogfile, 'r');
$size = 0;
$mailLogContents = '';
while (($line = fgets($handle,4096)) !== false) {
    $size += strlen($line);
    $mailLogContents .= $line;
    if($size > 1048576){//If total length is more than 1MB then stop
        break;
    }
}
fclose($handle);

//Notify the results with a mail
if(!empty($mailLogContents)){
    $mailLogContents = "Script found the following errors\n".$mailLogContents;
    @mail(EmailListsConfig::$nocAndPortalPlatform, 'Sync images script results - Error', $mailLogContents);
}else{
    @mail(EmailListsConfig::$nocAndPortalPlatform, 'Sync images script results - Success', 'All is well...');
}

echo "\nDONE\n";
?>
