<?php

class Helper {
	
	private static $serviceUrls = null;
	
	public static function getPropertyValue($key) {
		global $xcart_dir;
		if (self::$serviceUrls == null) {
			self::$serviceUrls = parse_ini_file("$xcart_dir/env/properties.ini");
		}
		return self::$serviceUrls[$key];
	}
	
	public static function isSuccessful($response) {
		if ($response != null && json_decode($response, true) != null) {
			$arr = reset(json_decode($response, true));
			if (isset($arr["status"]["statusType"])) {
				if ($arr["status"]["statusType"] == "SUCCESS") {
					return true;				
				}
				return false;
			}
		} 
		return false;
	}
	
	public static function getPropertyKey($value) {
		global $xcart_dir;
		if (self::$serviceUrls == null) {
			self::$serviceUrls = parse_ini_file("$xcart_dir/env/properties.ini");
		}
		return array_search($value, self::$serviceUrls);
	}
	
	/**
	 * produces a search query by extracting search params from $request
	 * @param unknown_type $request
	 */
	public static function getCommonRequestParams($request) {
		$start =  isset($request['start']) ?  $request['start'] : 0;
		$limit =  isset($request['limit']) ?  $request['limit'] : 50;
		$sort = isset($request['sort']) ? $request['sort'] : "";
		$dir = isset($request['dir']) ? $request['dir'] : "";
		
		$search = self::getCommonSerachParams($request);
		
		$ret = "";
		if ($search != "") {
			$ret = "q=" . $search . "&";	
		}
		return $ret . "start=" . $start . "&fetchSize=" . $limit . "&sortBy=" . $sort . "&sortOrder=" . $dir;
	}
	
	/**
	 * Creates a search string from $request["filter"]
	 * @param unknown_type $request
	 */
	public static function getCommonSerachParams($request) {
		$ret = "";
		// {attributeName}.{operator}:{value to compare}___{attributeName}.{operator}:{value to compare}
		if (isset($request["filter"])) {
			foreach($request["filter"] as $key => $value) {
				if ($value["data"]["type"] == "string") {
					$value["data"]["comparison"] = "like";
				} else if ($value["data"]["type"] == "boolean" || $value["data"]["type"] == "numeric") {
					$value["data"]["comparison"] = "eq";
				} else if($value["data"]["type"] == "list") {
					$value["data"]["comparison"] = "in";
				}
				$ret .= "___" . $value["field"] . "." .  $value["data"]["comparison"] . ":" . urlencode($value["data"]["value"]);
			}
		}
		
		if (strlen($ret) > 1) {
			return substr($ret, 3);	
		} 
		return $ret;
	}
	
	/**
	 * 
	 * Enter description here ...
	 * @param unknown_type $request
	 */
	public static function getCommonValidatorParams($request) {
		return "q=" . $request["field"] . ".eq:" . $request["value"];
	}
	
	/**
	 * 
	 * Method to get the json data from the server and load the specific fields 
	 * and return the json data back
	 * 
	 * @param unknown_type $jsonData
	 * @param unknown_type $fields
	 */
	public static function loadFields($jsonData, $key1, $fields = null) {
		
		$arr = array();
		if (json_decode($jsonData, true) != null) {
			$arr = reset(json_decode($jsonData, true));	
		}
		
		if ($fields == null || count($fields) == 0) {
			return json_encode($arr);
		} 
		
		$total = $arr["status"]["totalCount"];
		
		$ret = array();
		$dataArr = $arr["data"][$key1];		
		for($i = 0 ; $i < $total; $i++) {
			$tmp = array();
			foreach ($fields as $key) {
				// When only 1 element in dataarr, index 0 won't work
				$currentIndex = $dataArr;

				if(!empty($dataArr[$i])) {
					$currentIndex = $dataArr[$i];
				}

				$pos = strpos($key,  ".");
				$new_key = str_replace(".", "_", $key);
				if ($pos === false) {
					$tmp[$new_key] = $currentIndex[$key];
				} else {
					$tmp[$new_key] = $currentIndex[substr($key, 0, $pos)][substr($key, $pos + 1)];
				}
			}
			$ret[count($ret)] = $tmp;
		}
		
		return json_encode(array("success" => true, "data" => $ret));
	} 
	
	public static function getFieldFromResponse($response, $key, $field){
		$arr = array();
		if (json_decode($response, true) != null) {
			$arr = reset(json_decode($response, true));	
		}
		
		$total = $arr["status"]["totalCount"];
		
		$ret = array();
		$dataArr = $arr["data"][$key];
		for($i = 0 ; $i < $total; $i++) {
			// When only 1 element in dataarr, index 0 won't work
			$currentIndex = $dataArr;

			if(!empty($dataArr[$i])) {
				$currentIndex = $dataArr[$i];
			}
			
			$pos = strpos($field,  ".");
			$new_key = str_replace(".", "_", $field);
			if ($pos === false) {
				$ret[count($ret)] = $currentIndex[$new_key];
			} else {
				$ret[count($ret)] = $currentIndex[substr($new_key, 0, $pos)][substr($new_key, $pos + 1)];
			}
		}
		
		return $ret;
	}
	
	public static function decode_response($response_string) { 
		$response = array();
		if ($response_string != null && json_decode($response_string, true) != null) {
			$arr = json_decode($response_string, true);
			if(!isset($arr['status'])) {
				$arr = reset($arr);
			}	
		}
				
		if (isset($arr["status"]["statusType"])) {
			$response["statusCode"] = $arr["status"]["statusCode"];
			if ($arr["status"]["statusType"] == "SUCCESS") {
				$response["success"] = true;
				$response["results"] = $arr;
				if($response["results"]["status"]["totalCount"]==1) {
					$in = $response["results"]["data"];
					foreach($in as $key=>$value) {
						$dummyArr = array();
						array_push($dummyArr,$value);
						$response["results"]["data"][$key]=$dummyArr;
					}
				}
			} else {
				$response["failure"] = true;
				$response["failureMsg"] = $arr["status"]["statusMessage"];	
			}
		} else {
			$response["statusCode"] = 500;
			$response["failure"] = true;
			$response["failureMsg"] = "Status response not found in the response: Check the service response";
		}
		return $response;
	}
}
