#!/usr/bin/php -q
<?php

date_default_timezone_set("Asia/Calcutta");

require_once '../auth.php';
require_once "$xcart_dir/env/AMQP.config.php";
require_once "$xcart_dir/utils/amqpMsgHandler.php";

/**
* Handlers are registered in the following member
* handlers are arrays of (in order):
* 1. Associated queue name
* 2. Associated handler class
*/
$msgHandlers = array (
    /*'IMAGE_TRANSFER' => array(
        'queuename' => 'ImageTransfer', 
        'queue' => NULL,
        'handler' => new cdnTransferMsgHandler(),
        'exchange' => 
    ), 
    'IMAGE_BACKUP' => array(
        'queuename' => 'ImageBackup', 
        'queue' => NULL, 
        'handler' => new cdnBackupMsgHandler()
    ),*/
    'IMAGE_COMPRESS' => array(
        'queuename' => 'ImageCompress', 
        'queue' => NULL, 
        'handler' => new cdnCompressMsgHandler(),
        'exchangename' => 'compressexch'
    ),
    'IMAGE_COMPRESS_TO_CLOUDINARY' => array(
        'queuename' => 'ImageCompressToCloudinary', 
        'queue' => NULL, 
        'handler' => new cdnCompressToCloudinaryMsgHandler(),
        'exchangename' => 'compresstocloudinaryexch'
    )/*,
    'DB_THROTTLE' => array(
        'queuename' => 'ThrottleDBUpdates', 
        'queue' => NULL, 
        'handler' => new datebaseThrottleMsgHandler(),
        'exchangename' => 'throttleexch'
    )*/
);

// Allowed arguments & their defaults 
$runmode = array(
    'no-daemon' => false,
    'help' => false,
    'write-initd' => false,
    'consumer' => false,
);

// Scan command line attributes for allowed arguments
foreach ($argv as $k=>$arg) {
    $key = substr($arg, 2);
    $value = true;
    if(strpos($arg,"=") !== FALSE){
        $pos = strpos($arg,"=");
        $key = substr($arg, 2, $pos - 2);
        $value = substr($arg, $pos + 1);
    }
    if (substr($arg, 0, 2) == '--' && isset($runmode[$key])) {
        $runmode[$key] = $value;
    }
}
// Help mode. Shows allowed argumentents and quit directly
if ($runmode['help'] == true) {
    echo 'Usage: '.$argv[0].' [runmode]' . "\n";
    echo 'Available runmodes:' . "\n";
    foreach ($runmode as $runmod=>$val) {
        echo ' --'.$runmod . "\n";
    }
    die();
}
// Include System Daemon Class
error_reporting(E_ALL);
require_once 'System/Daemon.php';
// Setup
$options = array(
    'appName' => 'amqpconsumer',
    'appDir' => 'dirname(__FILE__)',
    'appDescription' => 'Retrieves queued image transfer requests and execute the requests',
    'authorName' => 'Myntra',
    'authorEmail' => 'alrt_portal_performance@myntra.com',
    'sysMaxExecutionTime' => '0',
    'sysMaxInputTime' => '0',
    'sysMemoryLimit' => '1024M',
    /*'appRunAsGID' => 1000,
    'appRunAsUID' => 1000,*/
);
System_Daemon::setOptions($options);
// This program can also be run in the forground with runmode --no-daemon
if (!$runmode['no-daemon']) {
    System_Daemon::start();
}
// With the runmode --write-initd, this program can automatically write a 
// system startup file called: 'init.d'
// This will make sure your daemon will be started on reboot 
if ($runmode['write-initd']) {
    if (($initd_location = System_Daemon::writeAutoRun()) === false) {
        System_Daemon::notice('unable to write init.d script');
    } else {
        System_Daemon::info(
            'sucessfully written startup script: %s',
            $initd_location
        );
    }
}
// This variable gives your own code the ability to breakdown the daemon:
$runningOkay = true;
$rabbitoptions = array('port' => AMQPConfig::$port,
                 'host' => AMQPConfig::$host,
                 'login' => AMQPConfig::$login,
                 'password' => AMQPConfig::$password,
                 'vhost' => AMQPConfig::$vhost);
define('AMQP_DEBUG', true);
try {

	$conn = new AMQPConnection($rabbitoptions['host'], $rabbitoptions['port'], $rabbitoptions['login'], $rabbitoptions['password'], $rabbitoptions['vhost']);
	
} catch (Exception $e) {
    echo $e->getTraceAsString();
    System_Daemon::stop();
    exit(0);
}
$handlers = $msgHandlers;
if ($runmode['consumer']) {
    if(array_key_exists($runmode['consumer'], $msgHandlers)){
        $handlers = array($runmode['consumer']=>$msgHandlers[$runmode['consumer']]);
    }
}

//print_r($conn);
foreach ($handlers as $msgType => $msgHandler) {
    $exchange = $msgHandler['exchangename'];
    $queue = $msgHandler['queuename'];
    $handlers[$msgType]['channel'] = $conn->channel();
    $handlers[$msgType]['channel']->exchange_declare($exchange, 'direct', false, true, false);
    $handlers[$msgType]['channel']->queue_declare($queue, false, true, false, false);
    $handlers[$msgType]['channel']->queue_bind($queue, $exchange,'myntra.rk');
    $handlers[$msgType]['channel']->basic_consume($queue, $consumer_tag, false, false, false, false, array($handlers[$msgType]['handler'], 'processMessage'));
    echo $queue;
}
echo "system".System_Daemon::isDying();

while (!System_Daemon::isDying() && $runningOkay) {
	foreach ($handlers as $msgType => $msgHandler) {
		echo $msgType;
		echo "#####################$msgType waiting on ".$msgHandler['queuename'];
		$handlers[$msgType]['channel']->wait();
		echo "################";
	}
}

// Shut down the daemon nicely in fatal error condition
System_Daemon::stop();

?>
