<?php
/**
 * RequestUtils
 * @author lohiya
 *
 */

class RequestType {

	static $PARAMETRIZED = 'parametrized';
	static $LANDING_PAGE = 'landing_page';
	static $NORMAL_SEARCH = 'normal_search';
}

class RequestUtils{

	static $REQUEST_TYPE='request_type';

	public static function isLandingPage($pageUrl){
		$dbAdapter=MyntraPageDBAdapter::getInstance();
		return $dbAdapter->isLandingPage($pageUrl);
	}
	public static function isParametrizedSearchPage($parameterKey){
		if(strlen($parameterKey)==36 && substr($parameterKey,-3) =='mnt'){
			return true;
		}
		else{
			return false;
		}
	}

	public static function getRequestType(){
		return $_GET[RequestUtils::$REQUEST_TYPE];
	}

	public static function getParametrizedKey(){
		return substr($_GET['page'],0,-4);
	}

	public static function getLandingPageURL(){
		return $_GET['query'];
	}

}


?>
