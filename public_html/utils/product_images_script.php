<?php
include_once("../auth.php");
include_once("../include/func/func.db.php");
include_once("../include/func/func.utilities.php");

date_default_timezone_set('Asia/Calcutta');
//$query = "select * from mk_style_properties limit 5 offset 500";
//$query = "select * from mk_style_properties where default_image in ('http://myntra.myntassets.com/images/style/properties/e39a7547e5c29d9d605b968b1aec685b_images.jpg')";
$query = "select * from mk_style_properties order by style_id desc";
//$query = "select * from mk_style_properties where global_attr_brand='s.Oliver'";
//$query = "select * from mk_style_properties where style_id = '21113'";
$date = date('Ymd');
$log = fopen("./logs/images/product_images_script_$date.txt", "w");
fwrite($log, "Started processing  \n");
$res = db_query($query, true);
$cdn_root = "myntra.myntassets.com";
$images_root = "/home/myntradomain/public_html";

while($row = mysql_fetch_assoc($res)){
	$id = $row['style_id'];
	$default_image = $row['default_image'];
	$front_image = $row['front_image'];
	$left_image = $row['left_image'];
	$right_image = $row['right_image'];
	$back_image = $row['back_image'];
	$top_image = $row['top_image'];
	$bottom_image = $row['bottom_image'];
	$search_image = $row['search_image'];
	$search_zoom_image = $row['search_zoom_image'];
	fwrite($log, "Processing style id : " . $id . "\n");

	process_image($default_image, 'default_image', $id);
	process_image($front_image, 'front_image', $id);
	process_image($left_image, 'left_image', $id);
	process_image($right_image, 'right_image', $id);
	process_image($back_image, 'back_image', $id);
	process_image($top_image, 'top_image', $id);
	process_image($bottom_image, 'bottom_image', $id);
	process_image($search_image, 'search_image', $id);
	process_image($search_zoom_image, 'search_zoom_image', $id);
}

function process_image($image_url, $field_name, $id){
	global $log, $cdn_root, $images_root, $cdn_base;
	if(substr($image_url,0,2) == './'){
		fwrite($log, "Transfering image::".$image_url." To CDN as it was not uploaded \n");
		if(moveToDefaultS3Bucket($image_url)){
			func_query("update mk_style_properties set $field_name = '$cdn_base/$image_url' where style_id = $id");
		}
	}
	//echo "Processing $field_name with image url : $image_url\n";
	$sizes = array( 
			'default_image' => array("_images.", "_images_48_64.", "_images_96_128.", "_images_360_480.", "_images_540_720.", "_images_1080_1440."),
			'front_image' => array("_images.", "_images_48_64.", "_images_360_480.", "_images_540_720.", "_images_1080_1440."),
			'left_image' => array("_images.", "_images_48_64.", "_images_360_480.", "_images_540_720.", "_images_1080_1440."),
			'right_image' => array("_images.", "_images_48_64.", "_images_360_480.", "_images_540_720.", "_images_1080_1440."),
			'back_image' => array("_images.", "_images_48_64.", "_images_360_480.", "_images_540_720.", "_images_1080_1440."),
			'top_image' => array("_images.", "_images_48_64.", "_images_360_480.", "_images_540_720.", "_images_1080_1440."),
			'bottom_image' => array("_images.", "_images_48_64.", "_images_360_480.", "_images_540_720.", "_images_1080_1440."),
			'search_image' => array("_images."),
			'search_zoom_image' => array("_images.", "_images_240_320."));

	if ( strpos($image_url, $cdn_root) == false ){
		return;
	}
	foreach($sizes[$field_name] as $size){
		$image = str_replace("_images.", $size, $image_url);
		$image_disk_path = $images_root . substr($image, strpos($image, $cdn_root) + 14);
		//echo "image disk path::".$image_disk_path."\n";
		if ( file_exists($image_disk_path) ){
			if(!http_head_curl($image)){
				fwrite($log, "Initiating transfer of $field_name : $image_disk_path for style id : $id \n");
				if(moveToDefaultS3Bucket(substr($image_disk_path, strpos($image_disk_path, '/images/')))){
					fwrite($log, "Successfully transfered $field_name : $image_disk_path for style id : $id \n");
				}
				
			}else{
				//fwrite($log, "Aborting transfer of $field_name for style id : $id as the image : " . $image . " already exist on cdn\n");
			}
		} else {
			//fwrite($log, "Aborting transfer of $field_name for style id : $id as the image : " . $image_disk_path . " does not exist on disk\n");
		}
	}
}
fclose($log);

/**
 * @return boolean false on error, content otherwise
 * @param  string $url
 * @desc   makes a HEAD request using cURL
 */
function http_head_curl($url) {
        $hdrs = @get_headers($url);
        return is_array($hdrs) ? preg_match('/^HTTP\\/\\d+\\.\\d+\\s+2\\d\\d\\s+.*$/',$hdrs[0]) : false;
/*
	if (!extension_loaded('curl_init') || !function_exists('curl_init')) {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30); // times out after 30s
		curl_setopt($ch, CURLOPT_HEADER, 1);
		curl_setopt($ch, CURLOPT_NOBODY, 1);
		curl_setopt ($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$response = curl_exec($ch);
		curl_close($ch);
		//  get the status code from HTTP headers 
		if(preg_match('/HTTP\/1\.\d+\s+(\d+)/', $response, $matches)){
			$code=intval($matches[1]);
		} else {
			return false;
		}
		//echo "Code $code returned for url $url\n";
		// see if code indicates success 
		$ret = (($code>=200) && ($code<400));
		//echo "Code $ret returned for url $url\n";
		return $ret;
	}
	return false;
*/
}

