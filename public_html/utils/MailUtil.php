<?php
interface Enumerator{
	public function hasMoreElements();
	public function nextElement();
}

class MailIDEnumerator implements Enumerator{
	private $ids;
	private $maxI=0;
	private $currI=0;
	public function __construct($idString){
		$splits=split(',',$idString);
		$idCount=0;
		foreach ($splits as $id){
			if(strpos($id, '<')){
				$this->ids[$idCount]['name']=substr($id, 0,strpos($id, '<'));
				$this->ids[$idCount]['id']=substr($id, strpos($id, '<')+1,strpos($id, '>')-strpos($id, '<')-1);
			}else{
				$this->ids[$idCount]['id']=$id;
				$this->ids[$idCount]['name']="";
			}
			$idCount++;
		}
		$this->maxI=$idCount-1;
		
	}
	public function nextElement () {
		return $this->ids[$this->currI++];
	}

	public function hasMoreElements () {
		if($this->currI>$this->maxI)
			return false;
		return true;
	}
}