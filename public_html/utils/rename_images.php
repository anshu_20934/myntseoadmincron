<?php
if (PHP_SAPI !== 'cli') {
    die("This script should be run in command line mode\n");
}
require_once '../env/DB.config.php';
require_once '../env/System.config.php';
require_once '../env/Host.config.php';
require_once '../include/SimpleImage.php';
require_once '../AMAZONS3/util/S3.php';

define('S3_BUCKET', SystemConfig::$amazonS3Bucket);
define('CDN_BASE', HostConfig::$cdnBase.'/');

function url_exists($url) {
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_HEADER, FALSE);
    curl_setopt($ch, CURLOPT_NOBODY, TRUE);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, FALSE);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_exec($ch);
    $res_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);
    return $res_code === 200;
}


$db = mysql_connect(DBConfig::$ro['host'], DBConfig::$ro['user'], DBConfig::$ro['password']);
if (!$db) die('Error! DB read only connection failed. ' . mysql_error() . "\n");

mysql_select_db(DBConfig::$ro['db'], $db) or die('Error! DB select failed. ' . mysql_error() . "\n");

$done = array();
if (file_exists('rename_done.txt')) {
    $contents = file_get_contents('rename_done.txt');
    $ids = explode("\n", $contents);
    foreach ($ids as $id) {
        if (empty($id)) continue;
        $done[$id] = 1;
    }
}

$s3 = new S3(SystemConfig::$amazonAccessKey, SystemConfig::$amazonSecretKey);
$headers = array();
$fp_log = fopen('rename_log.txt', 'a');
$fp_done = fopen('rename_done.txt', 'a');
$fp_error = fopen('rename_error.txt', 'a');
$fp_report = fopen('rename_report.txt', 'w');
$fp_acl = fopen('rename_update_acl.txt','a');
$fp_sql = fopen('rename_sql.sql','a');

$count = 0;

//Pick first batch of styles from category 'Accessories'
$sql = "SELECT * FROM seo_image_renaming_details where masterCategory='Accessories' and renamingFinished=0 limit 100;";

$res = mysql_query($sql, $db);
$total = mysql_num_rows($res);
if (!$res) die('Error! DB query failed. ' . mysql_error() . "\n");
fputs($fp_log, "Total records to rename: $total \n");
$newImageSizes = array(array("width"=>81, "height"=>108, "name"=>"81X108"),
		array("name"=>"1080X1440", "width"=>1080, "height"=>1440),
		array("name"=>"540X720", "width"=>540, "height"=>720),
		array("name"=>"360X480", "width"=>360, "height"=>480),
		array("name"=>"48X64", "width"=>48, "height"=>64),
		);

function generateImageAndPush($originalFile, $width, $height, $imageTypeName, $imageName, $updateColumn, $styleId, $imagePrefix, $isOriginal=false, $appendmini = false, $removemini = false) {
    global $s3, $fp_log, $fp_done, $fp_error, $tmp, $headers, $db, $fp_acl, $fp_sql;

	if(!$isOriginal) {
		$imageNameTail = "_$width"."_$height"."_mini.jpg";
	} else {
		$imageNameTail = "_mini.jpg";
	}
	
	if($appendmini) {
		$imageNameTail = "_$width"."_$height".".jpg";	
	}

	$dotIndex = strrpos($originalFile, ".");
    	$sourceFile = substr($originalFile,0, $dotIndex).$imageNameTail;
	$sourceFileUri = str_replace(array(CDN_BASE), array(''), $sourceFile);
	
	$indexofSlash=strrpos($sourceFileUri,'/');
	$defaultImage = substr($sourceFileUri, 0, $indexofSlash+1).$imagePrefix;
		
	if( ($updateColumn == "search_image") ) {
		$defaultImage .= "_$width"."_$height";
	}
	$defaultImage .= $imageNameTail;

	if($removemini) {
	$defaultImage = str_replace(array("_mini"),array(''), $defaultImage);
	}
	if(!url_exists($sourceFile)) {
		fputs($fp_log, "source does not exist: $sourceFile");
		fputs($fp_error, $styleId.",".$updateColumn.",".$imageTypeName."\n");
	} else {
	
	if($s3->copyObject(S3_BUCKET, $sourceFileUri, S3_BUCKET,$defaultImage)) {
   		fputs($fp_log, "successfully copied $sourceFileUri to $defaultImage\n");
		fputs($fp_acl, $defaultImage."\n");
   		fputs($fp_done, $styleId.",".$updateColumn.",".$imageTypeName."\n");
		$cdn_url = CDN_BASE.$defaultImage;
		$cdn_url = str_replace(array("_mini"), array(''), $cdn_url);
		if($isOriginal) {
			$query= "update mk_style_properties set $updateColumn='$cdn_url' where style_id='$styleId';";
			fputs($fp_sql, $query);
		}
	} else {
   		fputs($fp_log, "not copied $sourceFileUri to $defaultImage\n");
   		fputs($fp_error, $styleId.",".$updateColumn.",".$imageTypeName."\n");
	}
	}	
}


function renameImage($styleId, $prefix, $imagePrefix, $imageName, $updateColumn, $isDefaultImage = false) {
	global $newImageSizes, $s3, $fp_log, $fp_done, $fp_error, $tmp, $headers, $done;
    if (strpos($imageName, CDN_BASE) === false)  return;
  	$originalFile = $imageName; 

	//resizing images
    foreach($newImageSizes as $imageSize)
    {
		if (!isset($done[$styleId.",".$updateColumn.",".$imageSize['name']]))
			generateImageAndPush($originalFile, $imageSize['width'],  $imageSize['height'], $imageSize['name'], $imageName, $updateColumn, $styleId, $imagePrefix);
	}	

	if (!isset($done[$styleId.",".$updateColumn.",main"]))
	generateImageAndPush($originalFile, 0, 0, "main", $imageName, $updateColumn, $styleId, $imagePrefix, true);
	
	if($isDefaultImage) {
	if (!isset($done[$styleId.",".$updateColumn.",96X128"])) {
		generateImageAndPush($originalFile, 96, 128, "96X128", $imageName, $updateColumn, $styleId, $imagePrefix);
		// creating a copy of default image without mini in the file name, for admin and mymyntra pages			
		generateImageAndPush($originalFile, 96, 128, "96X128", $imageName, $updateColumn, $styleId, $imagePrefix, false, true);
		}
		if (!isset($done[$styleId.",".$updateColumn.",150X200"]))
		generateImageAndPush($originalFile, 150, 200, "150X200", $imageName, $updateColumn, $styleId, $imagePrefix);


		//construct the file path for search image
		$slashPos = strrpos($imageName, "/");
		$search_file = CDN_BASE."images/style/style_search_image/".substr($imageName, $slashPos +1);
		if (!isset($done[$styleId.",search_image,search_image"])) {
			$searchFile = str_replace(array(".jpg"), array('_180_240.jpg'), $search_file);
			generateImageAndPush($searchFile, 180, 240, "search_image", $imageName, "search_image", $styleId, $imagePrefix, true);
			generateImageAndPush($search_file, 180, 240, "search_image", $imageName, "search_zoom_image",  $styleId, $imagePrefix, false, false, true);
		}
		if (!isset($done[$styleId.",search_zoom_image,search_zoom_image"]))	{
			$searchFile = $search_file;
			generateImageAndPush($searchFile, 240, 320, "search_zoom_image", $imageName, "search_zoom_image", $styleId, $imagePrefix, false,true);
		}
	}
}

while ($prow = mysql_fetch_assoc($res)) {
    $count += 1;
    $styleId = $prow['styleId'];
	$sql = "SELECT * FROM mk_style_properties where style_id = '$styleId';";
	$result = mysql_query($sql, $db);
	$row = mysql_fetch_assoc($result);
	
	$product_display_name = $row['product_display_name'];	
	$product_display_name = preg_replace("/[\s]+/", "-", $product_display_name);
	$product_display_name = preg_replace("/[^a-zA-Z0-9-_&]/","", $product_display_name);
	$product_display_name = preg_replace("/\./","", $product_display_name);	
	
	$imagePrefix = $product_display_name;
		
	if($row['front_image'] != null)	{			
		renameImage($styleId, $imagePrefix, $imagePrefix.md5(rand().time())."_images", $row['front_image'], "front_image");
	}
	if($row['default_image'] != null)	{
		renameImage($styleId,  $imagePrefix, $imagePrefix.md5(rand().time())."_images", $row['default_image'], "default_image", true);
	}

	if($row['right_image'] != null)	{
		renameImage($styleId,  $imagePrefix, $imagePrefix.md5(rand().time())."_images", $row['right_image'], "right_image");
	}
	if($row['left_image']!= null) {
		renameImage($styleId, $imagePrefix, $imagePrefix.md5(rand().time())."_images", $row['left_image'], "left_image");
	}
	if($row['top_image'] != null) {
		renameImage($styleId, $imagePrefix,  $imagePrefix.md5(rand().time())."_images", $row['top_image'], "top_image");
	}
	if( $row['bottom_image'] != null) {
		renameImage($styleId,  $imagePrefix, $imagePrefix.md5(rand().time())."_images", $row['bottom_image'], "bottom_image");
	}
	if( $row['back_image'] != null) {
		renameImage($styleId,  $imagePrefix, $imagePrefix.md5(rand().time())."_images", $row['back_image'], "back_image");
	}
	
	$query= "update mk_style_properties set renaming_done='1' where style_id='$styleId'";
	fputs($fp_log, "Ran SQL QUERY::$query\n");
	$result = mysql_query($query, $db);

	$time=time();
	$query= "update seo_image_renaming_details set renamingFinished='1', renameTime='$time' where styleId='$styleId'";
	fputs($fp_log, "Ran SQL QUERY::$query\n");
	$result = mysql_query($query, $db);
}

mysql_close($db);
fclose($fp_done);
fclose($fp_error);
fclose($fp_acl);
fclose($fp_log);

echo "\nDONE\n";

