<?php
define('AMAZON_IMAGE_BUCKET', 'myntrawebimages');
define('AMAZON_S3_BUCKET_URL_SUFFIX', '.s3.amazonaws.com');

#define('S3_ACCESS_KEY_ID', 'AKIAJV2QBGIJF4THW74Q');
#define('S3_SECRET_ACCESS_KEY', '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');
# Changed S3 keys as on 9/1/11
define('S3_ACCESS_KEY_ID', 'AKIAILAAXNRLGZ3GKS2A');
define('S3_SECRET_ACCESS_KEY', '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');


require_once('../auth.php');
require_once('../AMAZONS3/util/S3.php');
require_once('../include/func/func.utilities.php');
define('DEFAULT_BUCKET_NAME', SystemConfig::$amazonS3Bucket);
define('S3_PATH_BASE', HostConfig::$cdnBase.'/');

function transferFile($filepath, $uri) {
    moveToS3($filepath, DEFAULT_BUCKET_NAME, $uri);
}

/**
* This is used to store the paths to search and upload images
* keys are base uris that translate into HostConfig::$cdnBase/<key>
* values are actual paths to search
*/
$skin1_dirs = array(
    'skinmobile/images/' => dirname(__FILE__).'/../skinmobile/images',
    'skin2/images/' => dirname(__FILE__).'/../skin2/images',
    'skin2/fonts/' => dirname(__FILE__).'/../skin2/fonts'
);

// Array for storing excluded directories
$excluded_dirs = array ( 
    'images/temp/',
    'images/customized/');

$indexfile = dirname(__FILE__).'/logs/uploadMkimages.idx';
$logfile = dirname(__FILE__).'/logs/uploadMkimages.log';
$hlog = fopen($logfile, 'w');

if (file_exists($indexfile)) {
    $idx = unserialize(file_get_contents($indexfile));
} else {
    $idx = array();
}

$excluded_dirs_pattern = implode("|", $excluded_dirs);
$excluded_dirs_pattern = str_replace("/", "\/", $excluded_dirs_pattern);
$excluded_dirs_pattern = "/" . $excluded_dirs_pattern . "/";
$loop_counter = 0;

foreach ($skin1_dirs as $baseuri => $folderpath) {
    $dit = new RecursiveIteratorIterator(
                new RecursiveDirectoryIterator($folderpath), 
                RecursiveIteratorIterator::LEAVES_ONLY);
    while ($dit->valid()) {
        if ($dit->isDot() || (!(strpos ($dit->getPathname(), "svn") === false))) {
            $dit->next();
        } else {
            $path = $dit->getPathName();
            $uri = $baseuri.$dit->getSubPathName();
            fwrite($hlog, "File $uri processing\n");
            if ( preg_match($excluded_dirs_pattern , $uri) ) {
                fwrite($hlog, "File $uri skipped as it is in an excluded directory\n");
                $dit->next();
            } else {
                    if ((array_key_exists($uri, $idx)) &&($idx[$uri] == $dit->getMTime())) {
                        fwrite($hlog, "File $uri already uploaded as per the index\n");
                    } else {
                        $numtries = 3;
                        $copiedflag = false;
                        while($numtries-- > 0 && !$copiedflag){
                            transferFile($path, $uri);
                            $s3url = S3_PATH_BASE.$uri;
                            if(url_exists($s3url)){
                                fwrite($hlog, "Copied $path to ".S3_PATH_BASE.$uri."\n");
                                $idx[$uri] = $dit->getMTime();
                                $copiedflag = true;
                                break;
                            }
                            usleep(50000);//Sleep for 50ms between each retry
                        }
                        if($copiedflag){
                            $loop_counter++;
                        }else{
                            fwrite($hlog, "Could not copy $path to ".S3_PATH_BASE.$uri."\n");
                        }
                    }
                    $dit->next();
            }
        }
        if( $loop_counter >= 1000 ){
                file_put_contents($indexfile, serialize($idx));
                $loop_counter = 0;
        }   
    }
}

file_put_contents($indexfile, serialize($idx));
function url_exists($url) { 
	$hdrs = @get_headers($url); 
	return is_array($hdrs) ? preg_match('/^HTTP\\/\\d+\\.\\d+\\s+2\\d\\d\\s+.*$/',$hdrs[0]) : false; 
}

?>
