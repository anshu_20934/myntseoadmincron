<?php
if (PHP_SAPI !== 'cli') {
    die("This script should be run in command line mode\n");
}
require_once '../env/DB.config.php';
require_once '../env/System.config.php';
require_once '../env/Host.config.php';
require_once '../include/SimpleImage.php';
require_once '../AMAZONS3/util/S3.php';

define('S3_BUCKET', SystemConfig::$amazonS3Bucket);
define('CDN_BASE', HostConfig::$cdnBase.'/');

function url_save($url, $file) {
    $ch = curl_init($url);
    $fp = fopen($file, 'wb');
    curl_setopt($ch, CURLOPT_FILE, $fp);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_exec($ch);
    $res_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);
    fclose($fp);
    return $res_code === 200;
}

$db = mysql_connect(DBConfig::$ro['host'], DBConfig::$ro['user'], DBConfig::$ro['password']);
if (!$db) die('Error! DB read only connection failed. ' . mysql_error() . "\n");

mysql_select_db(DBConfig::$ro['db'], $db) or die('Error! DB select failed. ' . mysql_error() . "\n");

$done = array();
if (file_exists('resize_verify_done.txt')) {
    $contents = file_get_contents('resize_verify_done.txt');
    $ids = explode("\n", $contents);
    foreach ($ids as $id) {
        if (empty($id)) continue;
        $done[$id] = 1;
    }
}

$s3 = new S3(SystemConfig::$amazonAccessKey, SystemConfig::$amazonSecretKey);
$image = new SimpleImage();
$tmp = sys_get_temp_dir();
$headers = array();
$fp_log = fopen('resize_verify_log.txt', 'w');
$fp_done = fopen('resize_verify_done.txt', 'a');
$fp_error = fopen('resize_verify_error.txt', 'w');
$count = 0;
$sql = "SELECT style_id, default_image FROM mk_style_properties ORDER BY style_id DESC";
$res = mysql_query($sql, $db);
$total = mysql_num_rows($res);
fputs($fp_log, "Total records to verify: $total \n");
echo "Starting verification";
if (!$res) die('Error! DB query failed. ' . mysql_error() . "\n");
$verifyImageSizes = array(array("width"=>81, "height"=>108, "name"=>"PDP Thumbnail"),
		 array("width"=>150, "height"=>200, "name"=>"Matchmaker"));
while ($row = mysql_fetch_assoc($res)) {
    $count += 1;
    $styleId = $row['style_id'];
    $process = false;
    foreach($verifyImageSizes as $imageSize){
        if (!isset($done[$styleId.$imageSize['name']])){
            $process = true;
        }
    }
    if($process == false){
        continue;
    }
    
    $defaultImage = $row['default_image'];
    if (strpos($defaultImage, CDN_BASE) === false) continue;

    printf("%05s/%05s \n", $count, $total);
    //verifying images
    foreach($verifyImageSizes as $imageSize)
    {
    	$width = $imageSize['width'];
    	$height = $imageSize['height'];
    	$imageTypeName = $imageSize['name'];
    	$imageNameTail = "_$width"."_$height.jpg";
    	$imageToVerify = str_replace('.jpg', $imageNameTail, $defaultImage);
    	fputs($fp_log, "$styleId downloading $imageTypeName $imageToVerify ... ");
    	$originalFile = tempnam($tmp, 'style_');
    	if (!url_save($imageToVerify, $originalFile)) {
    		fputs($fp_log, "ERROR - image could not be saved\n");
    		continue;
    	}
    	
    	fputs($fp_log, "verifying ... ");
    	list($imgWidth, $imgHeight, $imgType, $imgAttr) = getimagesize($originalFile);
    	if ($width === $imgWidth && $height === $imgHeight) {
    		fputs($fp_log, "Image verified\n");
    		fputs($fp_done, $styleId.$imageTypeName."\n");
    	}
    	else {
    		fputs($fp_log, "Image not verified\n");
    		fputs($fp_error, $styleId.$imageTypeName."\n");
    	}
    	
    	unlink($originalFile);
    }
}

mysql_close($db);
fclose($fp_done);
fclose($fp_error);
fclose($fp_log);

echo "\nDONE\n";

