<?php

/* Version 0.9, 6th April 2003 - Simon Willison ( http://simon.incutio.com/ )
   Manual: http://scripts.incutio.com/httpclient/
*/

class HttpClient {
    // Request vars
    var $host;
    var $port;
    var $path;
    var $method;
    var $postdata = '';
    var $cookies = array();
    var $referer;
    var $accept = 'text/xml,application/json,application/xhtml+xml,text/html,text/plain,image/png,image/jpeg,image/gif,*/*';
    var $accept_encoding = 'gzip';
    var $accept_language = 'en-us';
    var $user_agent = 'Incutio HttpClient v0.9';
    // Options
    var $timeout = 5;
    var $use_gzip = true;
    var $persist_cookies = true;  // If true, received cookies are placed in the $this->cookies array ready for the next request
                                  // Note: This currently ignores the cookie path (and time) completely. Time is not important, 
                                  //       but path could possibly lead to security problems.
    var $persist_referers = true; // For each request, sends path of last request as referer
    var $debug = false;
    var $handle_redirects = true; // Auaomtically redirect if Location or URI header is found
    var $max_redirects = 5;
    var $headers_only = false;    // If true, stops receiving once headers have been read.
    // Basic authorization variables
    var $username;
    var $password;
    // Response vars
    var $status;
    var $headers = array();
    var $content = '';
    var $errormsg;
    var $original_response = '';
    // Tracker variables
    var $redirect_count = 0;
    var $cookie_host = '';
    var $contentType = 'application/xml';
    
    function HttpClient($host, $port=80) {
    	global $apilog;
        $this->host = $host;
        $this->port = $port;
    }
    
    /**
     * Creates a GET Query String
     * @param $path. GET request path
     * @param $data. Contains query string params in array form. false in case there are no params
     */
    function get($path, $data = false) {
    	$this->path = $path;
        $this->method = 'GET';
        if ($data) {
            $this->path .= '?'.$this->buildQueryString($data);
        }
        return $this->doRequest();
    }
    
    function customPost($path, $data) {
    	$this->path = $path;
        $this->method = 'POST';
        if($this->contentType == '') $this->contentType = "application/xml";
        $this->postdata = $data;
    	return $this->doRequest();
    }
    
    /**
     * Creates a Post data for a POST Request.
     * @param $path. POST request path.
     * @param $data. Contains post parameters in array form.
     */
    function post($path, $data) {
        $this->path = $path;
        $this->method = 'POST';
        $this->postdata = $this->buildQueryString($data);
    	return $this->doRequest();
    }
  	
    /**
     * Creates query string from in the form of key1=value1&key2=value2&....
     * @param $data. The input array from which params are extracted.
     */
    function buildQueryString($data) {
        $querystring = '';
        if (is_array($data)) {
            // Change data in to postable data
    		foreach ($data as $key => $val) {
    			if (is_array($val)) {
    				foreach ($val as $val2) {
    					$querystring .= urlencode($key).'='.urlencode($val2).'&';
    				}
    			} else {
    				$querystring .= urlencode($key).'='.urlencode($val).'&';
    			}
    		}
    		$querystring = substr($querystring, 0, -1); // Eliminate unnecessary &
    	} else {
    	    $querystring = $data;
    	}
    	return $querystring;
    }
    
    /**
     * Sends the request and reads back the response.
     */
    function doRequest() {
    	global $apilog;
    	$time_start = $this->microtime_float();
    	$request = $this->buildRequest();
    	$this->requestId = time();
    	$apilog->info("User ".$this->username." \nRequest Id - ".$this->requestId." \n".$request);
        // Performs the actual HTTP request, returning true or false depending on outcome
        $address = $this->host;
        if($this->port == 443)
        	$address = "ssl://".$address; 
        	
		if (!$fp = @fsockopen($address, $this->port, $errno, $errstr, $this->timeout)) {
		    // Set error message
            switch($errno) {
				case -3:
					$this->errormsg = 'Socket creation failed (-3)';
				case -4:
					$this->errormsg = 'DNS lookup failure (-4)';
				case -5:
					$this->errormsg = 'Connection refused or timed out (-5)';
				default:
					$this->errormsg = 'Connection failed ('.$errno.')';
			    $this->errormsg .= ' '.$errstr;
			    $apilog->info($this->errormsg);
			}
			return false;
        }
        //socket_set_timeout($fp, $this->timeout);
        fwrite($fp, $request);
    	// Reset all the variables that should not persist between requests
    	$this->headers = array();
    	$this->content = '';
    	$this->errormsg = '';
    	// Set a couple of flags
    	$inHeaders = true;
    	$atStart = true;
    	// Now start reading back the response
    	while (!feof($fp)) {
    	    $line = fgets($fp, 4096);
    	    if ($atStart) {
    	    	if (trim($line) == '') {
    	    		continue;
    	    	}
    	        // Deal with first line of returned data
    	        $atStart = false;
    	        if (!preg_match('/HTTP\/(\\d\\.\\d)\\s*(\\d+)\\s*(.*)/', $line, $m)) {
    	            $this->errormsg = "Status code line invalid: ".htmlentities($line);
    	            $this->debug($this->errormsg);
    	            return false;
    	        }
    	        $http_version = $m[1]; // not used
    	        $this->status = $m[2];
    	        $status_string = $m[3]; // not used
    	        $this->debug(trim($line));
    	        continue;
    	    }
    	    if ($inHeaders) {
    	        if (trim($line) == '') {
    	            $inHeaders = false;
    	            $this->debug('Received Headers', $this->headers);
    	            if ($this->headers_only) {
    	                break; // Skip the rest of the input
    	            }
    	            continue;
    	        }
    	        if (!preg_match('/([^:]+):\\s*(.*)/', $line, $m)) {
    	            // Skip to the next header
    	            continue;
    	        }
    	        $key = strtolower(trim($m[1]));
    	        $val = trim($m[2]);
    	        // Deal with the possibility of multiple headers of same name
    	        if (isset($this->headers[$key])) {
    	            if (is_array($this->headers[$key])) {
    	                $this->headers[$key][] = $val;
    	            } else {
    	                $this->headers[$key] = array($this->headers[$key], $val);
    	            }
    	        } else {
    	            $this->headers[$key] = $val;
    	        }
    	        continue;
    	    }
    	    // We're not in the headers, so append the line to the contents
    	    $this->content .= $line;
        }
        fclose($fp);
        $time_end = $this->microtime_float();
		$time = round($time_end - $time_start, 4);
		$apilog->info("Response for Request - ".$this->requestId." - (".$time." secs) for ".$this->path." = ". $this->content);
        // If data is compressed, uncompress it
        if (isset($this->headers['content-encoding']) && $this->headers['content-encoding'] == 'gzip') {
            $this->debug('Content is gzip encoded, unzipping it');
            $this->content = substr($this->content, 10); // See http://www.php.net/manual/en/function.gzencode.php
            $this->content = gzinflate($this->content);
        }
        // If $persist_cookies, deal with any cookies
        if ($this->persist_cookies && isset($this->headers['set-cookie']) && $this->host == $this->cookie_host) {
            $cookies = $this->headers['set-cookie'];
            if (!is_array($cookies)) {
                $cookies = array($cookies);
            }
            foreach ($cookies as $cookie) {
                if (preg_match('/([^=]+)=([^;]+);/', $cookie, $m)) {
                    $this->cookies[$m[1]] = $m[2];
                }
            }
            // Record domain of cookies for security reasons
            $this->cookie_host = $this->host;
        }
        // If $persist_referers, set the referer ready for the next request
        if ($this->persist_referers) {
            $this->debug('Persisting referer: '.$this->getRequestURL());
            $this->referer = $this->getRequestURL();
        }
        // Finally, if handle_redirects and a redirect is sent, do that
        if ($this->handle_redirects) {
            if (++$this->redirect_count >= $this->max_redirects) {
                $this->errormsg = 'Number of redirects exceeded maximum ('.$this->max_redirects.')';
                $this->debug($this->errormsg);
                $this->redirect_count = 0;
                return false;
            }
            $location = isset($this->headers['location']) ? $this->headers['location'] : '';
            $uri = isset($this->headers['uri']) ? $this->headers['uri'] : '';
            if ($location || $uri) {
                $url = parse_url($location.$uri);
                // This will FAIL if redirect is to a different site
                return $this->get($url['path']);
            }
        }
        
        //$this->content = Helper::decode_response($this->content);
        return true;
    }
    
    /**
     * Builds the request headers and appends post data(in case of POST request).
     */
    function buildRequest() {
        $headers = array();
        $headers[] = "{$this->method} {$this->path} HTTP/1.0"; // Using 1.1 leads to all manner of problems, such as "chunked" encoding
        $headers[] = "Host: {$this->host}";
        $headers[] = "User-Agent: {$this->user_agent}";
        $headers[] = "Accept: application/json";
        if ($this->use_gzip) {
            $headers[] = "Accept-encoding: {$this->accept_encoding}";
        }
        $headers[] = "Accept-language: {$this->accept_language}";
        if ($this->referer) {
            $headers[] = "Referer: {$this->referer}";
        }
    	// Cookies
    	if ($this->cookies) {
    	    $cookie = 'Cookie: ';
    	    foreach ($this->cookies as $key => $value) {
    	        $cookie .= "$key=$value; ";
    	    }
    	    $headers[] = $cookie;
    	}
    	//$tmp = $_SESSION["erpSessionUserData"];
    	//$headers[] = 'Authorization: Basic '.base64_encode("email" . ':' . "password");
    	// Basic authentication
    	//if ($this->username && $this->password) {
    	$headers[] = 'Authorization: Basic '.base64_encode($this->username."~".$this->username. ':' .$this->username);

    	//}
    	// If this is a POST, set the content type and length
    	if ($this->postdata) {
    	    //$headers[] = 'Content-Type: application/x-www-form-urlencoded';
    	    $headers[] = empty($this->contentType) ? 'Content-Type: application/x-www-form-urlencoded' : 'Content-Type: '.$this->contentType; 
    	    $headers[] = 'Content-Length: '.strlen($this->postdata);
    	}
    	$request = implode("\r\n", $headers)."\r\n\r\n".$this->postdata;
    	return $request;
    }
    
    
    function getStatus() {
        return $this->status;
    }
    
    
    function getContent() {
        return $this->content;
    }
    
    function getRawResponse() {
    	return $this->original_response;
    }
    
    
    function getHeaders() {
        return $this->headers;
    }
    
    
    function getHeader($header) {
        $header = strtolower($header);
        if (isset($this->headers[$header])) {
            return $this->headers[$header];
        } else {
            return false;
        }
    }
    
    
    function getError() {
        return $this->errormsg;
    }
    function getCookies() {
        return $this->cookies;
    }
    
    /**
     * Builds the complete request URL.
     */
    function getRequestURL() {
        $url = 'http://'.$this->host;
        if ($this->port != 80) {
            $url .= ':'.$this->port;
        }            
        $url .= $this->path;
        return $url;
    }
    // Setter methods
    function setUserAgent($string) {
        $this->user_agent = $string;
    }
    function setAuthorization($username, $password) {
        $this->username = $username;
        $this->password = $password;
    }
    function setCookies($array) {
        $this->cookies = $array;
    }
    // Option setting methods
    function useGzip($boolean) {
        $this->use_gzip = $boolean;
    }
    function setPersistCookies($boolean) {
        $this->persist_cookies = $boolean;
    }
    function setPersistReferers($boolean) {
        $this->persist_referers = $boolean;
    }
    function setHandleRedirects($boolean) {
        $this->handle_redirects = $boolean;
    }
    function setMaxRedirects($num) {
        $this->max_redirects = $num;
    }
    function setHeadersOnly($boolean) {
        $this->headers_only = $boolean;
    }
    function setDebug($boolean) {
        $this->debug = $boolean;
    }
    
    
    // "Quick" static methods
    
    /**
     * Creates and dispatches a GET request.
     * @param $url. The URL from which GET request is created
     */
    function quickGet($url, $user, $data=false) {
        $bits = parse_url($url);
        $host = $bits['host'];
        $port = isset($bits['port']) ? $bits['port'] : 80;
        if($bits['scheme'] == 'https') {
        	$port = 443;
        }
        $path = isset($bits['path']) ? $bits['path'] : '/';
        if (isset($bits['query'])) {
            $path .= '?'.$bits['query'];
        }
        $client = new HttpClient($host, $port);
        $client->setAuthorization($user, $user);
        if (!$client->get($path, $data)) {
            return false;
        } else {
            return $client->getContent();
        }
    }
    
    /**
     * Creates and dispatches a POST request.
     * @param $url. The URL from which post request is created
     * @param $data. The post data
     */
    function quickPost($url, $data , $user, $contentType = "application/xml") {
        $bits = parse_url($url);
        $host = $bits['host'];
        $port = isset($bits['port']) ? $bits['port'] : 80;
        if($bits['scheme'] == 'https') {
        	$port = 443;
        }
        $path = isset($bits['path']) ? $bits['path'] : '/';
		if (isset($bits['query'])) {
            $path .= '?'.$bits['query'];
        }
        $client = new HttpClient($host, $port);
        $client->setAuthorization($user, $user);
		$client->contentType = $contentType;
        if (!$client->customPost($path, $data)) {
            return false;
        } else {
            return $client->getContent();
        }
    }
    
    function customDelete($path) {
    	$this->path = $path;
        $this->method = 'DELETE';
        $this->contentType = "application/xml";
    	return $this->doRequest();
    }
    
	function quickDelete($url) {
        $bits = parse_url($url);
        $host = $bits['host'];
        $port = isset($bits['port']) ? $bits['port'] : 80;
        if($bits['scheme'] == 'https') {
        	$port = 443;
        }
        $path = isset($bits['path']) ? $bits['path'] : '/';
        $client = new HttpClient($host, $port);
        
        if (!$client->customDelete($path)) {
            return false;
        } else {
            return $client->getContent();
        }
    }
    
	function customPut($path, $data) {
    	$this->path = $path;
        $this->method = 'PUT';
        $this->contentType = "application/xml";
        $this->postdata = $data;
    	return $this->doRequest();
    }
    
	function quickPut($url, $data, $user) {
		$bits = parse_url($url);
        $host = $bits['host'];
        $port = isset($bits['port']) ? $bits['port'] : 80;
        if($bits['scheme'] == 'https') {
        	$port = 443;
        }
        $path = isset($bits['path']) ? $bits['path'] : '/';
        $client = new HttpClient($host, $port);
        $client->setAuthorization($user, $user);
        if (!$client->customPut($path, $data)) {
            return false;
        } else {
            return $client->getContent();
        }
    }
    
    function debug($msg, $object = false) {
        if ($this->debug) {
            print '<div style="border: 1px solid red; padding: 0.5em; margin: 0.5em;"><strong>HttpClient Debug:</strong> '.$msg;
            if ($object) {
                ob_start();
        	    print_r($object);
        	    $content = htmlentities(ob_get_contents());
        	    ob_end_clean();
        	    print '<pre>'.$content.'</pre>';
        	}
        	print '</div>';
        }
    }
    
	function microtime_float() {
	   list($usec, $sec) = explode(" ", microtime());
	   return ((float)$usec + (float)$sec);
	}

	function curl_post($url, $post, $username) {
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_VERBOSE, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible;)");
		curl_setopt($ch, CURLOPT_URL, $url);
    	$authheaders = 'Authorization: Basic '.base64_encode($username."~".$username.':'.$username);
		$acceptlanguage = 'Accept-Language: en-us';
		$accept = 'Accept: application/json';
                curl_setopt($ch, CURLOPT_HTTPHEADER, array("$authheaders","$acceptlanguage","$accept")); 

		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post); 
    	$response = curl_exec($ch);
		if(curl_errno($ch)) { //echo curl_error($ch); 
			return false; 
		}
		curl_close($ch); 
		return $response;		
	}
}

?>
