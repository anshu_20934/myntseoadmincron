<?php

class SimpleParser
{
    /**
     * The main function for converting to an XML document.
     * Pass in a multi dimensional array and this recrusively loops through and builds up an XML document.
     *
     * @param array $data
     * @param string $rootNodeName - what you want the root node to be - defaultsto data.
     * @param SimpleXMLElement $xml - should only be used recursively
     * @return string XML
     */
    public static function arrayToXml($data, $rootNodeName = 'data', &$xml=null)
    {
        if (is_null($xml))
        {
            // $xml = simplexml_load_string("");
            $xml = new SimpleXMLElement("<?xml version='1.0' encoding='utf-8' standalone='yes'?><$rootNodeName />");
        }

        // loop through the data passed in.
        foreach($data as $index => $value)
        {
            // no numeric keys in our xml please!
            if (is_numeric($index))
            {
                // make string key...
                $key = "";
            } else {
            	// delete any char not allowed in XML element names
            	$key = preg_replace('/[^a-z0-9\-\_\.\:]/i', '', $index);
            }
            
            // if there is another array found recrusively call this function
            if (is_array($value))
            {
            	if($key != "") {
                	$node = $xml->addChild($key);
                	SimpleParser::arrayToXml($value, $node, $node);
            	} else {
                	SimpleParser::arrayToXml($value, $rootNodeName, $xml);
            	}
            }
            else 
            {
                // add single node.
				$value = preg_replace('#[^\w\s\d()/.%\-&\$!@&\:;,\+\#]#',"",$value);
                $value = htmlentities($value);
                if($key != "") {
                	$xml->addChild($key, $value);
                } else {
                	$xml->addChild($value);
                }
            }
            
        }
        // pass back as string. or simple xml object if you want!
        return $xml->asXML();
    }
    
    
    /**
     * 
     * Enter description here ...
     * @param unknown_type $obj
     * @param unknown_type $arr
     */
	public static function xmlToArray($obj, &$arr = null) {
        if (is_null($arr)) $arr = array();
        if (is_string($obj)) $obj = new SimpleXMLElement($obj);
        
        $children = $obj->children();
        $executed = false;
        
        foreach ($children as $elementName => $node){
            if($arr[$elementName]!=null){
                if($arr[$elementName][0]!==null){
                    $i = count($arr[$elementName]);
                    SimpleParser::xmlToArray($node, $arr[$elementName][$i]);
                }else{
                    $tmp = $arr[$elementName];
                    $arr[$elementName] = array();
                    $arr[$elementName][0] = $tmp;
                    $i = count($arr[$elementName]);
                    SimpleParser::xmlToArray($node, $arr[$elementName][$i]);
                }
            }else{
                $arr[$elementName] = array();
                SimpleParser::xmlToArray($node, $arr[$elementName]);
            }
            $executed = true;
        }
        
        if(!$executed&&$children->getName()==""){
            $arr = (String)$obj;
        }
        return $arr;
    }
}

