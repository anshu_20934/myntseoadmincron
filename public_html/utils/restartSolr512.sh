#!/bin/sh
source /home/myntradomain/server_variables

ps ax | grep java | grep start | grep 512| grep -v grep
if [ $? -ne 0 ]
then
cd $JAVA_BRIDGE_HOME
rm -f solr/sprod/data/index/*.lock
nohup /usr/bin/java -server -Xmx512m -jar start.jar > /dev/null &
echo "trying to restart JavaBridge on $SERVER_NAME" | /bin/mail -s "trying to restart JavaBridge on $SERVER_NAME" noc@myntra.com
fi
