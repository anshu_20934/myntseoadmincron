<?php
/**
* $Id$
*
* This class provides an interface to transfer images from local disk to 
* Amazon S3 storage. 
* @author Abhijeet Dev <abhijeet.dev@myntra.com>
* @version 0.1
*/

namespace myntra\utils\cdn;

require_once dirname(__FILE__)."/../env/Host.config.php";
require_once \HostConfig::$documentRoot."/env/AMQP.config.php";
require_once \HostConfig::$documentRoot."/logger.php";
require_once(\HostConfig::$documentRoot."/lib/php-amqplib/amqp.inc");

/**
* @class imagetransferS3
* @abstract A utility class to request image transfer to S3
* @discussion imagetransferS3 is a utility that works as a client 
*   to Myntra Image Transfer Service that pulls messages from an 
*   AMQP server and executes the transfer/backup requests.
*/
final class imagetransferS3 {
    private static $amqpConn = NULL;
	private static $channel = null;
	private static $compressExch = NULL;
	
    private static function initamqp() {
        if (self::$amqpConn) return;

        $rabbitoptions = array(
            'port' => \AMQPConfig::$port,
            'host' => \AMQPConfig::$host,
            'login' => \AMQPConfig::$login,
            'password' => \AMQPConfig::$password,
            'vhost' => \AMQPConfig::$vhost
        );
        try {
            self::$amqpConn = new \AMQPConnection($rabbitoptions["host"],$rabbitoptions["port"],$rabbitoptions["login"],$rabbitoptions["password"],$rabbitoptions["vhost"]);
            self::$channel = self::$amqpConn->channel();
        } catch (\Exception $e) {
            echo "Error in rabbitmq connection:".$e."\n";
            self::$amqpConn = NULL;
        }
    }
	
    private static function initbackupexch() {
        self::initamqp();
    }

    private static function inittransferexch() {
        self::initamqp();
    }

    private static function initcompressexch(){
        self::initamqp();
    }
        

    /**
    * @function transfer
    * @abstract Transfer image to S3 and update url in database
    * @param tablename string - Name of the table that has image information
    * @param fieldname string - Name of the image path field
    * @param idfieldvalue string - Unique identifier of the row in table
    * @param idfieldname string - Defaults to 'id', name of unique identifier field
    */
    public static function transfer($tablename, $fieldname, $idfieldvalue, $idfieldname = 'id') {
        self::inittransferexch();
        $msg = new \AMQPMessage(serialize(
                array(
                    'tablename' => $tablename, 
                    'fieldname' => $fieldname, 
                    'idfieldvalue' => $idfieldvalue, 
                    'idfieldname' => $idfieldname)),'');
        $res = self::$channel->basic_publish($msg,"transferexch","myntra.rk",true,true); 
    }

    /**
    * @function backup
    * @abstract Creates a backup of image
    * @param imagepath string - Path of the image thati needs to be backed up
    */
    public static function backup($imagepath) {
        self::initbackupexch();
        $msg = new \AMQPMessage($imagepath,'');
        $res = self::$channel->basic_publish($imagepath,"backupexch","myntra.rk",true,true);
        return $res;
    }

    /**
    * @function compress
    * @abstract Compress an image using Jpegmini
    * @param imagepath string - Path of the image that needs to compressed
    */
    public static function compress($imagepathCsv) {
        $weblog;
        try{
            self::initcompressexch();
            $imagepaths = explode(',',$imagepathCsv);
            $res = true;
            foreach ($imagepaths as $imagepath){
                if(empty($imagepath)){
                    continue;
                }
                $msg = new \AMQPMessage($imagepath,array('content_type' => 'text/plain', 'delivery-mode' => 2));
                $res &= self::$channel->basic_publish($msg,"compressexch","myntra.rk");
            }
        }
        catch(\Exception $e){
            echo $e;
            $weblog->debug("Error while compressing '$imagepathCsv'. Error in rabbitmq connection:".$e);
            return false;
        }
        return true;
    }
    /**
    * @function compress
    * @abstract Compress an image using Jpegmini and upload to cloudinary
    * @param imagepath string - Path of the image that needs to compressed
    */
    public static function compressToCloudinary($imagepathArr, $uploadOptions, $eagerParams) {
        $weblog;
        try{
            self::initamqp();
            $res = true;
            foreach ($imagepathArr as $imagepath){
                if(empty($imagepath)){
                    continue;
                }
                $message = array();
                $message['imagePath'] = $imagepath;
                $message['uploadOptions'] = $uploadOptions;
                $message['eagerParams'] = $eagerParams;
                $message = serialize($message);
                $msg = new \AMQPMessage($message,array('content_type' => 'text/plain', 'delivery-mode' => 2));
                $res &= self::$channel->basic_publish($msg,"compresstocloudinaryexch","myntra.rk");
            }
        }
        catch(\Exception $e){
            echo $e;
            $weblog->debug("Error while compressing '$imagepathCsv'. Error in rabbitmq connection:".$e);
            return false;
        }
        return true;
    }
}

?>

