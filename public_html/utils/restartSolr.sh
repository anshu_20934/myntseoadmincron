#!/bin/sh
source /home/myntradomain/server_variables
ps ax | grep java | grep start | grep Xms4096m| grep -v grep
if [ $? -ne 0 ]
then
cd $SOLR_HOME
rm -f solr/sprod/data/index*/*.lock
rm -f solr/suggest/data/index*/*.lock
rm -f solr/stats_search/data/index*/*.lock
nohup /usr/bin/java -server -Xmx4096m -Xms4096m -jar start.jar >/dev/null &
fi

