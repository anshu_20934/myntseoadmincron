#!/bin/sh

ps ax | grep amqpConsumer | grep "consumer=IMAGE_COMPRESS" | grep -v "CLOUDINARY" | grep -v grep
if [ $? -ne 0 ]
then
cd /home/myntradomain/public_html/utils
/usr/bin/php -q amqpConsumer.php --consumer=IMAGE_COMPRESS
fi

ps ax | grep amqpConsumer | grep "consumer=IMAGE_COMPRESS_TO_CLOUDINARY" | grep -v grep
if [ $? -ne 0 ]
then
cd /home/myntradomain/public_html/utils
/usr/bin/php amqpConsumer.php --no-daemon --consumer=IMAGE_COMPRESS_TO_CLOUDINARY &
fi

