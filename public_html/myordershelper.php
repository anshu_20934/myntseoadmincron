<?php
require_once $xcart_dir."/include/class/class.mymyntra.php";
require_once $xcart_dir."/include/func/func.mkcore.php";
use abtest\MABTest;

$mymyntra = new MyMyntra(mysql_real_escape_string($login));

$return_enabled=FeatureGateKeyValuePairs::getFeatureGateValueForKey('returnenabled');
$exchanges_enabled=FeatureGateKeyValuePairs::getFeatureGateValueForKey('exchanges.enabled');
$refund_enabled=FeatureGateKeyValuePairs::getBoolean('return.refund.enabled',false) 
					&& MABTest::getInstance()->getUserSegment('ReturnRefundRatio') == "test";

$orders = $mymyntra->getUserOrderHistory(true, -1); // We need to fetch item details as well, so passing 'true' as parameter.
$orderItems = array();
$orderTotals = array();
$pendingFlag = false;
$completedFlag = false;
$hasReturnableItems = false;

//pickup changes
$returnPickupExcludedlist = WidgetKeyValuePairs::getWidgetValueForKey('pickup.disabled.articletypes');
$returnPickupExcludedArray = explode(",", $returnPickupExcludedlist);

function is_return_pickupexcluded($articleType, $returnPickupExcludedArray){
    return ((in_array($articleType,$returnPickupExcludedArray)) ? true : false);
}

foreach ($orders as $orderId => &$order) {
    $amounts = array('total' => 0, 'subtotal' => 0, 'discount' => 0, 'coupon_discount' => 0, 'cash_redeemed' => 0,
            'pg_discount' => 0, 'shipping_cost' => 0, 'payment_surcharge' => 0,'gift_charges' => 0);
    $order['returnAllowed'] = false;
    $order['canClaimLoyaltyPoints'] = false;
    $order['allItemsReturned'] =  true;
    $trackingUpdateDates = array();
    $order['trackingRequestData'] = array();
    $order['trackingRequestData']['shipmentOrder'] = array();
    $order['showTrackingLink'] = false;
    $shipCount = 0;
    foreach ($order['orders'] as $idx => &$subOrder) {
    	if ($subOrder['delivereddate'] > 0) {
        	$trackingUpdateDates[] = $subOrder['delivereddate'];
        	$subOrder['shipmentNo'] = ++$shipCount; 
        } else if ($subOrder['shippeddate'] > 0) {
        	$trackingUpdateDates[] = $subOrder['shippeddate'];
        	$subOrder['shipmentNo'] = ++$shipCount;
		} else {
        	$trackingUpdateDates[] = 0;
        	$subOrder['shipmentNo'] = 0;
        }
        $order['trackingRequestData']['index'][] = $idx;
        $order['trackingRequestData']['shipmentOrder'][] = $subOrder['orderid'];
        
        $subOrder['expectedDelivery'] = getDeliveryDateForOrder($subOrder['queueddate'], $subOrder['items'], $subOrder['s_zipcode'], $subOrder['pm'],$subOrder['courier_code'], false, $subOrder['orderid']);

        if ($subOrder['showTrackingStatus'] != "purged") {
        	$order['showTrackingLink'] = true;
        }
        foreach ($subOrder['items'] as $item) {
            $subItemId = $item['itemid'];
            $subOrderId = $item['orderid'];
	        $unifiedsize=Size_unification::getUnifiedSizeByStyleId($item['product_style'],"array",true);

            //pickup changes
            $pickup_excluded = false;
            if(is_return_pickupexcluded($item['global_attr_article_type'],$returnPickupExcludedArray)){
                $pickup_excluded=true;
            }

            $orderItems["$subOrderId-$subItemId"] = array(
                'articletype' => $item['global_attr_article_type'],
                'baseOrderId' => $orderId,
                'orderId' => $subOrderId,
                'itemId' => $subItemId,
                'styleName' => $item['stylename'],
                'styleId' => $item['product_style'],
                'option' => $unifiedsize[$item['option']],
                'amtPaid' => (float)$item['final_price_paid'],
            	'pm' => $subOrder['pm'],
            	'orderTotal' => $subOrder['total']+$subOrder['payment_surcharge'], 	
                'qty' => $item['quantity'],
                'loyaltyPointsAwarded' => $item['item_loyalty_points_awarded'],
                'pickup_excluded' => $pickup_excluded
            );
            
            // Loyalty : flag to show getPoints
            $item['canClaimLoyaltyPoints'] = false;
            if (($item['return_criteria'] == 'allowed' || $item['return_criteria'] == 'jewellery') && !$item['returnid'] && !$item['exchange_orderid']) {
            	$order['returnAllowed'] = true;
            	$hasReturnableItems = true;
            	
            	// Loyalty : flag to show getPoints
            	$item['canClaimLoyaltyPoints'] = true;
            }

            // This is a case when not returnable item is there. In this case we will show normal get points flow, as there are changes required in backend
            // This is a quick fix untill the backend is ready
            if($item['return_criteria'] == 'not_returnable' ){
                $item['canClaimLoyaltyPoints'] = true;
            }

            // Loyalty : flag to show getPoints link in completed orders table, for an order.
            if($item['is_returnable'] && $item['item_loyalty_points_awarded'] > 0 && !$item['returnid'] && !$item['exchange_orderid'] && $item['return_criteria'] !='time_exceeded' ){
                $order['canClaimLoyaltyPoints'] = true;
            }
            
            if($order['allItemsReturned'] && $item['returnid'] && !empty($item['is_refunded']) &&  $item['is_refunded']){
                // Returned flow has been completed successfully for this item
                $order['allItemsReturned'] = true;
            }else{
                $order['allItemsReturned'] = false;
            }

        }
        $amounts['total'] += $subOrder['total']+$subOrder['payment_surcharge'];
        $amounts['subtotal'] += $subOrder['subtotal'];
        $amounts['discount'] += $subOrder['discount'];
		$amounts['cart_discount'] += $subOrder['cart_discount'];
        $amounts['coupon_discount'] += $subOrder['coupon_discount'];
        $amounts['cash_redeemed'] += $subOrder['cash_redeemed'];
        $amounts['pg_discount'] += $subOrder['pg_discount'];
        $amounts['shipping_cost'] += $subOrder['shipping_cost'];
        $amounts['gift_charges'] += $subOrder['gift_charges'];
        $amounts['emi_charge'] += $subOrder['payment_surcharge'];
        if($order['orders'][0]['payment_method'] == 'cod'){
        	$amounts['refundable_amount'] += $subOrder['cash_redeemed'];	
        }else{
        	$amounts['refundable_amount'] += $subOrder['total'] + $subOrder['cash_redeemed'];
        }
        
    }
    $orderTotals[$orderId] = $amounts;
    if(!$pendingFlag) {
     	if($order['status'] == "PENDING") {
       		$pendingFlag = true;
       	}
    }
    if(!$completedFlag) {
       	if($order['status'] == "COMPLETED") {
       		$completedFlag = true;
       	}
    }
    if (count($trackingUpdateDates) > 1) {
		array_multisort($trackingUpdateDates, SORT_DESC, $order['trackingRequestData']['shipmentOrder'], SORT_ASC, $order['trackingRequestData']['index']);
    }
    $order['trackingRequestData']['orderid'] = $orderId;
    $order['trackingRequestData']['itemCount'] = $order['count'];
    $order['trackingRequestData']['placedDate'] = $order['orders'][0]['date'];
    $order['trackingRequestData']['shipments'] = array();
    foreach ($order['trackingRequestData']['shipmentOrder'] as $idx => $orderid) {
    	$index = $order['trackingRequestData']['index'][$idx];
    	$order['trackingRequestData']['shipments'][$orderid] = array();
    	$order['trackingRequestData']['shipments'][$orderid]['items'] = array();
    	$order['trackingRequestData']['shipments'][$orderid]['shipmentNo'] = $order['orders'][$index]['shipmentNo'];
    	$order['trackingRequestData']['shipments'][$orderid]['itemCount'] = $order['orders'][$index]['qtyInOrder'];
    	$order['trackingRequestData']['shipments'][$orderid]['trackingWebsite'] = $order['orders'][$index]['website'];
    	$order['trackingRequestData']['shipments'][$orderid]['trackingStatus'] = $order['orders'][$index]['showTrackingStatus'];
    	$order['trackingRequestData']['shipments'][$orderid]['statusMsg'] = $order['orders'][$index]['customerstatusmessage'];
    	foreach ($order['orders'][$index]['items'] as $i => $item) {
    		$order['trackingRequestData']['shipments'][$orderid]['items'][$i]['name'] = $item['stylename'];
    		$order['trackingRequestData']['shipments'][$orderid]['items'][$i]['size'] = $item['option'];
    		$order['trackingRequestData']['shipments'][$orderid]['items'][$i]['qty'] = $item['quantity'];
    	}
    }
    unset($order['trackingRequestData']['shipmentOrder'], $order['trackingRequestData']['index']);
    $order['trackingRequestData'] = json_encode($order['trackingRequestData']);
}
$orderItemsJson = json_encode($orderItems);
$smarty->assign('orderItemsJson', $orderItemsJson);

$userProfileData = $mymyntra->getUserProfileData();
$userProfileData['name'] = trim($userProfileData['firstname'] . ' ' . $userProfileData['lastname']);

$smarty->assign("orders", $orders);
$smarty->assign("orderTotals", $orderTotals);
$smarty->assign("returnenabled",$return_enabled);
$smarty->assign("exchanges_enabled",$exchanges_enabled);
$smarty->assign("refund_enabled",$refund_enabled);
$smarty->assign("userProfileData", $userProfileData);
$smarty->assign("pendingFlag", $pendingFlag);
$smarty->assign("completedFlag", $completedFlag);
$smarty->assign("hasReturnableItems", $hasReturnableItems);
