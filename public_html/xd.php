<?php

// This is used to make communication between http and https served pages in iframe.
// Particularly in login/signup dialog

$msg = filter_input(INPUT_GET, 'msg', FILTER_SANITIZE_STRING);
echo <<<HTML
<!DOCTYPE html>
<html>
    <head>Myntra</head>
    <body>
        Please wait...
        <script>
        top.Myntra.xdMessage("{$msg}");
        </script>
    </body>
</html>
HTML;

