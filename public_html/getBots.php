<?php

require_once($xcart_dir."auth.php");

global $xcache;

$id = "bots";

// Fetch user agents from xcache
$ua = $xcache->fetch($id);

if($ua==null) {
	// The useragents are not yet stored in xcache. We fetch them from DB
	
	$query = "select * from mk_bots";
	$ua = array();
	$res = db_query($query);
	
	if ($res) {
		while ($row = db_fetch_row($res)) {
			$bot_type = $row[1];
			$bot_name = $row[2];
			
			if(array_key_exists($bot_type, $ua)) {
				// In case an array exists, we append new bot to existing array list of values.
				$ua[$bot_type][] = $bot_name;
			} else {
				$ua[$bot_type] = array($bot_name);
			}				
		}
	}

	db_free_result($res);
	
	// We store the newly fetched bots into xcache
	$xcache->store($id, $ua, 2592000);
} 
$response['botsList'] = json_encode($ua);
$response['status']['statusType'] = 'SUCCESS';
header("Content-Type:application/json");
echo json_encode($response);
exit;

?>