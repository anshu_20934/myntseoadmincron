<?php 
define("QUICK_START", 1);
use abtest\MABTest;
include_once("./auth.php");
include_once "$xcart_dir/include/class/widget/class.widget.skulist.php";
include_once ("$xcart_dir/include/class/mcart/class.MCart.php");
if ($telesales){
    echo "";exit;
}
$page = filter_input(INPUT_GET, 'page', FILTER_SANITIZE_STRING);
$type = filter_input(INPUT_GET, 'type', FILTER_SANITIZE_STRING);
$r_count_limit = filter_input(INPUT_GET, 'limit', FILTER_SANITIZE_NUMBER_INT);
$ls_list = filter_input(INPUT_GET, 'lsrv', FILTER_SANITIZE_STRING);

/****
** For new PDP Recently Viewed 
****/

$styleid = filter_input(INPUT_GET, 'styleid', FILTER_SANITIZE_NUMBER_INT);
$rtype = filter_input(INPUT_GET, 'rtype', FILTER_SANITIZE_STRING);
if($rtype === 'json'){
    $solrSearchProducts = SearchProducts::getInstance();
    $product = @$solrSearchProducts->getSingleStyleFormattedSolrData($styleid);
    $similarProducts=$solrSearchProducts->getRelatedProductsPDP(explode(',',$product['keywords']),$styleid,0,6);
    $res = array();
    foreach ($similarProducts as $product){
        $id = $product['styleid'];
        if(!empty($id)){
            array_push($res, $id);
        }
    }
    header('Content-Type:application/json');
    echo json_encode($res);
    exit;
}
/****************************/


$recent_views_cookie_content = getMyntraCookie('RVC001');
//replace it with localstorage value 
if(!empty($ls_list)){
	$recent_views = json_decode($ls_list);
}
else{
	if(!empty($recent_views_cookie_content)) {
		$recent_views=explode(',',$recent_views_cookie_content);
	} else {
		$resp = array(
            	'status'  => 'ERROR',
            	'content' => ''
        	);
    	echo json_encode($resp);
	}
}
$style_ids=array_unique($recent_views);
$recent_viewed_count=count($style_ids);
if(empty($page)){
    if(empty($r_count_limit) || $r_count_limit > 6){//Last if condition kept to ward off abuse of the request parameter
	    $r_count_limit=5;
    }
//FILL SPACE
/****If recently viewed product count less than 5 fill the space with recommended products****/
	if($recent_viewed_count < $r_count_limit){
		$solrSearchProducts = SearchProducts::getInstance();
		$multiStyleSolrData=@$solrSearchProducts->getMultipleStyleFormattedSolrData($style_ids);
		$products = array();
		foreach ($multiStyleSolrData as $singleStyleSolrData){
			if(!empty($singleStyleSolrData)) {
				$singleStyleSolrData = WidgetUtility::getWidgetFormattedData($singleStyleSolrData);
				if(!empty($singleStyleSolrData['count_options_availbale'])&&!$singleStyleSolrData['count_options_availbale']<=0){
					$products[] = $singleStyleSolrData;
				}
			}			
		}
		
		$product = $products[0];
		$similarProducts=$solrSearchProducts->getRelatedProductsPDP(explode(',',$product['keywords']),'0_style_'.$style_ids[0],0,8);
		foreach ($similarProducts as $product){
			if(!empty($product['styleid'])&&!in_array($product['styleid'],$style_ids)){
				$style_ids[]=$product['styleid'];
				if(count($style_ids)==($r_count_limit)){
					break;
				}
			}
		}
	}
}

$widgetData=getWidget($style_ids);
$smarty->assign("recent_viewed_count",$recent_viewed_count);
$smarty->assign('recentWidget',true);
$smarty->assign('widget',$widgetData);

//Most popular enable disable 
$smarty->assign("enableMostPopular" , FeatureGateKeyValuePairs::getBoolean("enableMostPopular"));
//AB Test for Most Populer
$smarty->assign("mostPopularVariant", MABTest::getInstance()->getUserSegment('homeMostPopularWidget'));

if(empty($type)){
	$smarty->assign("ga_component_name", "recent_widget");
}

$div_content = $layoutVariant == 'test'? $smarty->fetch("inc/recent-widget-new.tpl", $smarty) : $smarty->fetch("inc/recent-widget.tpl", $smarty);
	if (!empty($div_content)){
  		 	$resp = array(
            	'status'  => 'SUCCESS',
            	'content' => $div_content
        	);
  		}
  		else {
  			$resp = array(
            	'status'  => 'ERROR',
            	'content' => ''
        	);
  		}
echo json_encode($resp);

function getWidget($styleids){
		$styleWidget=new WidgetSKUList('headerCarouselItems',$styleids);
		$widgetFormattedData = $styleWidget->getData();
		return $widgetFormattedData;
}	