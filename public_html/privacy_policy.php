<?php
require "./auth.php";

if ($skin == 'skin2') {
    header('Location: privacypolicy#top');
    exit;
}

$analyticsObj = AnalyticsBase::getInstance();
$analyticsObj->setStaticPageVars(AnalyticsBase::PRIVACY_POLICY);
$analyticsObj->setTemplateVars($smarty);

func_display("privacy_policy.tpl",$smarty);
?>