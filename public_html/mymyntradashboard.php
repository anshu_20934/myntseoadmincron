<?php
require_once 'auth.php';
include_once $xcart_dir.'/include/class/class.mymyntra.php';
include_once $xcart_dir.'/modules/coupon/database/CouponAdapter.php';
include_once "$xcart_dir/modules/loyaltyPoints/LoyaltyPointsPortalHelper.php";

global $telesales;
if ($telesales){
    echo "{}";exit;
}

$myMyntra = new MyMyntra(mysql_real_escape_string($login));
$couponAdapter = CouponAdapter::getInstance();

$profile = $myMyntra->getBasicUserProfileData($login);

$counts = $myMyntra->getOrderAndReturnsCountsForDashboard($login);

/* fetch balance from MyntCashService */
/* fetching it from raw service to avoid load on remote service */
$myntCashDetails = MyntCashRawService::getBalance($login);


$credits = array();
$credits['couponsCount'] = $couponAdapter->getActiveCouponsCount($login);
$credits['expiring'] = $couponAdapter->getExpiringSoonCoupons($login);

$referrals = $couponAdapter->getReferralsCount($login);

$loyaltyPointsDetails = array();
if($loyaltyEnabled && !empty($login)){
    LoyaltyPointsPortalHelper::setLoyaltyPointsHeaderDetails($login); // This will put smarty variable 
}

$dashValues = array();
$dashValues['profile'] = $profile;
$dashValues['counts'] = $counts;
$dashValues['credits'] = $credits;
$dashValues['referrals'] = $referrals;
$dashValues['myntCashDetails'] = $myntCashDetails;
$dashValues['loyaltyPointsDetails'] = $loyaltyPointsDetails;
$smarty->assign("dashValues", $dashValues);
$content = $smarty->fetch("header/my-myntra-dashboard.tpl", $smarty);

if (!empty($content)){
	$resp = array(
    	'status'  => 'SUCCESS',
        'content' => $content
	);
}
else {
	$resp = array(
    	'status'  => 'ERROR',
        'content' => ''
	);
}

echo json_encode($resp);
//echo "<pre>";print_r($dashValues);