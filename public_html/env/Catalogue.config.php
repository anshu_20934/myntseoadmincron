<?php
class CatalogueConfig {
	public static $imagePath = "/var/bulkupload/";
        public static $sanityScriptDescriptionMinLength = "30";
        public static $sanityScriptNumAcceptableMissingImages = "4";
        public static $sanityScriptMailSubject = "Catalog product data issues";
}
?>
