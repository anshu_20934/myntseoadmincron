<?php
class CacheConfig {
	public static $redisRW = '{"host":"redis-master.myntra.com", "port":6379, "connection_timeout": 1.0, "read_write_timeout": 1.0 }';
	public static $redisRO = '{"host":"redis-read.myntra.com", "port":6379, "connection_timeout": 0.05, "read_write_timeout": 0.05 }';
    public static $redisFooterRW = '{"host":"redis-master.myntra.com", "port":6380, "connection_timeout": 1.0, "read_write_timeout":1.0 }';
    public static $redisFooterRO = '{"host":"redis-cart-master.myntra.com", "port":9011, "connection_timeout": 0.05, "read_write_timeout":
    0.05 }';
	public static $factory = '{"connections":{"tcp": "Predis\Connection\PhpiredisConnection", "unix":"Predis\Connection\PhpiredisStreamConnection"}}';
	public static $enabled = True;
	public static $version = "v2.0";
        public static $seoRedisRW = '{"host":"seo-redis.myntra.com", "port":6003, "connection_timeout": 0.05, "read_write_timeout": 1.0 }';
	public static $seoRedisRO = '{"host":"seo-redis.myntra.com", "port":6002, "connection_timeout": 0.05, "read_write_timeout": 0.05 }';
}
?>
