<?php
class MinifyConfig_skinmobile {
public static $conf = array (
  'groups' => 
  array (
    '3pl' => 
    array (
      'css' => 
      array (
      ),
      'js' => 
      array (
        0 => 'jquery.js',
        1 => 'modernizr-2.6.1.min.js',
        2 => 'modernizr-tests.js',
        3 => 'jquery.ui.core.js',
        4 => 'jquery.ui.widget.js',
        5 => 'jquery.ui.mouse.js',
        6 => 'jquery.ui.position.js',
        7 => 'jquery.ui.autocomplete.js',
        8 => 'jquery.autocomplete.extend.js',
        9 => 'fastbutton.js',
        10 => 'iscroll.js',
        11 => 'hammer.js',
        12 => 'jquery.flexslider.js',
        13 => 'jquery.hammer.js',
        14 => 'jquery.pageslide.js',
        15 => 'jquery.touchSwipe.min.js',
        16 => 'json2.js',
      ),
      'min_css' => '',
      'min_js' => 'skinmobile/js/3pl-f8768047fa65669327267fa0126433f3.min.js',
      'mingz_css' => '',
      'mingz_js' => 'skinmobile/js/3pl-f8768047fa65669327267fa0126433f3.js.jgz',
      'iscdn_css' => '',
      'iscdn_js' => 0,
    ),
    'common' => 
    array (
      'css' => 
      array (
        0 => 'myntra-global.css',
        1 => 'jquery.ui.base.css',
        2 => 'jquery.ui.core.css',
        3 => 'jquery.ui.autocomplete.css',
        4 => 'common.css',
        5 => 'rightmenu.css',
        6 => 'common-320.css',
        7 => 'common-480.css',
        8 => 'common-768.css',
        9 => 'home.css',
        10 => 'home-responsive.css',
        11 => 'footer.css',
        12 => 'nav.css',
        13 => 'lightbox.css',
        14 => 'login.css',
        15 => 'login-responsive.css',
      ),
      'js' => 
      array (
        0 => 'ns.js',
        1 => 'base.js',
        2 => 'cookie.js',
        3 => 'fb.js',
        4 => 'utils.js',
        5 => 'gaq.js',
        6 => 'tooltip.js',
        7 => 'pincode-widget.js',
        8 => 'myntra.global.js',
        9 => 'header.js',
        10 => 'lightbox.js',
        11 => 'nav.js',
        12 => 'myntra.modals.js',
        13 => 'login.js',
        14 => 'tracking.js',
        15 => 'common.js',
        16 => 'rightmenu.js',
        17 => 'fb-actions.js',
        18 => 'ga-conversion.js',
        19 => 'couponinfo-widget.js',
      ),
      'min_css' => 'skinmobile/css/common-05302103f154b6899c06f116d422db14.min.css',
      'min_js' => 'skinmobile/js/common-c42ce91a85e586f16c032cec603dc0d4.min.js',
      'mingz_css' => 'skinmobile/css/common-05302103f154b6899c06f116d422db14.css.jgz',
      'mingz_js' => 'skinmobile/js/common-c42ce91a85e586f16c032cec603dc0d4.js.jgz',
      'iscdn_css' => 0,
      'iscdn_js' => 0,
    ),
    'home' => 
    array (
      'css' => 
      array (
        0 => 'home.css',
        1 => 'home-responsive.css',
        2 => 'slider.css',
        3 => 'flexslider.css',
      ),
      'js' => 
      array (
        0 => 'home.js',
        1 => 'slideshow.js',
        2 => 'recent-viewed-sizes.js',
        3 => 'recent-viewed-load.js',
      ),
      'min_css' => 'skinmobile/css/home-19c8d45ea9defe01231606a463a63813.min.css',
      'min_js' => 'skinmobile/js/home-72f205cd1fee594b89b4b5c2884c1d59.min.js',
      'mingz_css' => 'skinmobile/css/home-19c8d45ea9defe01231606a463a63813.css.jgz',
      'mingz_js' => 'skinmobile/js/home-72f205cd1fee594b89b4b5c2884c1d59.js.jgz',
      'iscdn_css' => 0,
      'iscdn_js' => 0,
    ),
    'pdp' => 
    array (
      'css' => 
      array (
        0 => 'pdp.css',
        1 => 'pdp-responsive.css',
        2 => 'sizechart.css',
        3 => 'flexslider.css',
        4 => 'accordion.css',
      ),
      'js' => 
      array (
        0 => 'sizechart-position.js',
        1 => 'sizechart.js',
        2 => 'pdp.js',
        3 => 'myntra.product-page.js',
        4 => 'pincode-widget.js',
        5 => 'recomendation.js',
        6 => 'couponinfo-widget.js',
      ),
      'min_css' => 'skinmobile/css/pdp-f48f8f00fc18d0111406b738ff255fc5.min.css',
      'min_js' => 'skinmobile/js/pdp-00706ace75e115a315233d61cb9e7aaf.min.js',
      'mingz_css' => 'skinmobile/css/pdp-f48f8f00fc18d0111406b738ff255fc5.css.jgz',
      'mingz_js' => 'skinmobile/js/pdp-00706ace75e115a315233d61cb9e7aaf.js.jgz',
      'iscdn_css' => 0,
      'iscdn_js' => 0,
    ),
    'search' => 
    array (
      'css' => 
      array (
        0 => 'search-results.css',
        1 => 'search-results-responsive.css',
      ),
      'js' => 
      array (
        0 => 'jquery.ba-hashchange.min.js',
        1 => 'jquery.scroll.js',
        2 => 'jquery.inview.js',
        3 => 'jquery.loading.js',
        4 => 'filter-properties.js',
        5 => 'scroll-properties.js',
        6 => 'search-results.js',
      ),
      'min_css' => 'skinmobile/css/search-faaed41bb630a47fddd79b3ad5c45155.min.css',
      'min_js' => 'skinmobile/js/search-1e465e0e5ece068bce7be97106554095.min.js',
      'mingz_css' => 'skinmobile/css/search-faaed41bb630a47fddd79b3ad5c45155.css.jgz',
      'mingz_js' => 'skinmobile/js/search-1e465e0e5ece068bce7be97106554095.js.jgz',
      'iscdn_css' => 0,
      'iscdn_js' => 0,
    ),
    'landing' => 
    array (
      'css' => 
      array (
        0 => 'flexslider.css',
        1 => 'landing.css',
      ),
      'js' => 
      array (
        0 => 'avail.js',
        1 => 'slideshow.js',
      ),
      'min_css' => 'skinmobile/css/landing-a8cc211d16e4ae18e5a6110e1a49e99c.min.css',
      'min_js' => 'skinmobile/js/landing-555cb35efb8f39a1ec2cecd889c0e3c5.min.js',
      'mingz_css' => 'skinmobile/css/landing-a8cc211d16e4ae18e5a6110e1a49e99c.css.jgz',
      'mingz_js' => 'skinmobile/js/landing-555cb35efb8f39a1ec2cecd889c0e3c5.js.jgz',
      'iscdn_css' => 0,
      'iscdn_js' => 0,
    ),
    'checkout-cart' => 
    array (
      'css' => 
      array (
        0 => 'cart.css',
        1 => 'cart-responsive.css',
        2 => 'mod-coupons.css',
        3 => 'giftwrap-responsive.css',
        4 => 'giftwrap.css',
      ),
      'js' => 
      array (
        0 => 'cart.js',
        1 => 'giftwrap.js',
        2 => 'recomendation.js',
      ),
      'min_css' => 'skinmobile/css/checkout-cart-cc87423f6e1f88aa1d698590e87a44f3.min.css',
      'min_js' => 'skinmobile/js/checkout-cart-09f47923199e1c0e5fa5a9c30debb98d.min.js',
      'mingz_css' => 'skinmobile/css/checkout-cart-cc87423f6e1f88aa1d698590e87a44f3.css.jgz',
      'mingz_js' => 'skinmobile/js/checkout-cart-09f47923199e1c0e5fa5a9c30debb98d.js.jgz',
      'iscdn_css' => 0,
      'iscdn_js' => 0,
    ),
    'secure-common' => 
    array (
      'js' => 
      array (
        0 => 'jquery.js',
        1 => 'ns.js',
        2 => 'base.js',
        3 => 'utils.js',
        4 => 'lightbox.js',
        5 => 'tracking.js',
        6 => 'jquery.ui.core.js',
        7 => 'jquery.ui.widget.js',
        8 => 'jquery.ui.position.js',
        9 => 'jquery.ui.autocomplete.js',
        10 => 'jquery.autocomplete.extend.js',
        11 => 'jquery.tappable.js',
        12 => 'jquery.pageslide.js',
        13 => 'jquery.jqtransform.js',
        14 => 'placeholder.jquery.js',
        15 => 'slide-summary.js',
        16 => 'ga-conversion.js',
      ),
      'css' => 
      array (
        0 => 'myntra-global.css',
        1 => 'footer.css',
        2 => 'common-checkout.css',
        3 => 'common-checkout-responsive.css',
        4 => 'lightbox.css',
      ),
      'min_css' => 'skinmobile/css/secure-common-fb554d4a0b909962c13196c7ec7dfb4a.min.css',
      'min_js' => 'skinmobile/js/secure-common-94951faf5e5eebfc617bc98efd9fa3ed.min.js',
      'mingz_css' => 'skinmobile/css/secure-common-fb554d4a0b909962c13196c7ec7dfb4a.css.jgz',
      'mingz_js' => 'skinmobile/js/secure-common-94951faf5e5eebfc617bc98efd9fa3ed.js.jgz',
      'iscdn_css' => 0,
      'iscdn_js' => 0,
    ),
    'checkout-address' => 
    array (
      'js' => 
      array (
        0 => 'address-form.js',
        1 => 'address.js',
        2 => 'address-list.js',
        3 => 'tooltip.js',
      ),
      'css' => 
      array (
        0 => 'address.css',
        1 => 'address-list.css',
        2 => 'address-form.css',
        3 => 'checkout.css',
      ),
      'min_css' => 'skinmobile/css/checkout-address-5a28bf9dcd8ed497287d7a6b8d72894d.min.css',
      'min_js' => 'skinmobile/js/checkout-address-696eced0840e2f6f5e75b8e6a5d6250b.min.js',
      'mingz_css' => 'skinmobile/css/checkout-address-5a28bf9dcd8ed497287d7a6b8d72894d.css.jgz',
      'mingz_js' => 'skinmobile/js/checkout-address-696eced0840e2f6f5e75b8e6a5d6250b.js.jgz',
      'iscdn_css' => 0,
      'iscdn_js' => 0,
    ),
    'checkout-payment' => 
    array (
      'js' => 
      array (
        0 => 'cc-address-list.js',
        1 => 'tooltip.js',
        2 => 'payment.js',
        3 => 'jquery.ui.tabs.js',
        4 => 'jquery.tools.min.js',
      ),
      'css' => 
      array (
        0 => 'payment.css',
        1 => 'payment-responsive.css',
        2 => 'footer.css',
        3 => 'checkout.css',
      ),
      'min_css' => 'skinmobile/css/checkout-payment-7d36ea0e4c2f0e6397ba8dd10780be56.min.css',
      'min_js' => 'skinmobile/js/checkout-payment-ea0e590a53605ff44f1eaa24a65e50ab.min.js',
      'mingz_css' => 'skinmobile/css/checkout-payment-7d36ea0e4c2f0e6397ba8dd10780be56.css.jgz',
      'mingz_js' => 'skinmobile/js/checkout-payment-ea0e590a53605ff44f1eaa24a65e50ab.js.jgz',
      'iscdn_css' => 0,
      'iscdn_js' => 0,
    ),
    'express-buy' => 
    array (
      'css' => 
      array (
        0 => 'checkout.css',
        1 => 'payment.css',
        2 => 'payment-responsive.css',
        3 => 'payment-form.css',
        4 => 'express.css',
        5 => 'address-list.css',
        6 => 'address-form.css',
      ),
      'js' => 
      array (
        0 => 'cc-address-list.js',
        1 => 'address-form.js',
        2 => 'payment-form-validate.js',
        3 => 'payment.js',
        4 => 'checkout.js',
        5 => 'express.js',
      ),
      'min_css' => 'skinmobile/css/express-buy-59bb706235442fc3a564526f97638925.min.css',
      'min_js' => 'skinmobile/js/express-buy-b9cdda43adb21b6cdd5a0e2effa373ec.min.js',
      'mingz_css' => 'skinmobile/css/express-buy-59bb706235442fc3a564526f97638925.css.jgz',
      'mingz_js' => 'skinmobile/js/express-buy-b9cdda43adb21b6cdd5a0e2effa373ec.js.jgz',
      'iscdn_css' => 0,
      'iscdn_js' => 0,
    ),
    'confirmation' => 
    array (
      'css' => 
      array (
        0 => 'confirmation.css',
        1 => 'confirmation-responsive.css',
        2 => 'checkout.css',
      ),
      'js' => 
      array (
      ),
      'min_css' => 'skinmobile/css/confirmation-a0f16924455090e1f97a5494b96f52e6.min.css',
      'min_js' => '',
      'mingz_css' => 'skinmobile/css/confirmation-a0f16924455090e1f97a5494b96f52e6.css.jgz',
      'mingz_js' => '',
      'iscdn_css' => 0,
      'iscdn_js' => '',
    ),
  ),
  'pages' => 
  array (
    '/index.php' => 
    array (
      0 => '3pl',
      1 => 'common',
      2 => 'home',
    ),
    '/home.php' => 
    array (
      0 => '3pl',
      1 => 'common',
      2 => 'home',
    ),
    '/homepage.php' => 
    array (
      0 => '3pl',
      1 => 'common',
      2 => 'home',
    ),
    '/pdp.php' => 
    array (
      0 => '3pl',
      1 => 'common',
      2 => 'pdp',
    ),
    '/search.php' => 
    array (
      0 => '3pl',
      1 => 'common',
      2 => 'search',
    ),
    '/landingPages.php' => 
    array (
      0 => '3pl',
      1 => 'common',
      2 => 'landing',
    ),
    '/cart.php' => 
    array (
      0 => '3pl',
      1 => 'common',
      2 => 'checkout-cart',
    ),
    '/mkmycart.php' => 
    array (
      0 => '3pl',
      1 => 'common',
      2 => 'checkout-cart',
    ),
    '/mkmycartmultiple.php' => 
    array (
      0 => '3pl',
      1 => 'common',
      2 => 'checkout-cart',
    ),
    '/mkmycartmultiple_re.php' => 
    array (
      0 => '3pl',
      1 => 'common',
      2 => 'checkout-cart',
    ),
    '/checkout-cart.php' => 
    array (
      0 => '3pl',
      1 => 'common',
      2 => 'checkout-cart',
    ),
    '/mkpaymentoptions.php' => 
    array (
      0 => 'secure-common',
      1 => 'checkout-payment',
    ),
    '/checkout-address.php' => 
    array (
      0 => 'secure-common',
      1 => 'checkout-address',
    ),
    '/expressbuy.php' => 
    array (
      0 => 'secure-common',
      1 => 'express-buy',
    ),
    '/confirmation.php' => 
    array (
      0 => 'secure-common',
      1 => 'confirmation',
    ),
  ),
);
}
