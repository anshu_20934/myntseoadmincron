<?php
class SOLRConfig {
	public static $servers = array("msolr");
	public static $port = 8984;
	public static $version = 1.4;
	public static $timeout = 1.0;
	public static $searchhost = "solrbackend.myntra.com";
	public static $searchport = 8984;
}
?>
