<?php
ini_set('memory_limit', '1024M');
class HostConfig {

static function extract_hostname_without_domain($fqn) {
	$pos = strpos($fqn, ".bom1.myntra.com");
	$found = False;
	$result = $fqn;
	if ($pos !== false) {
		$found = True;
		$result = substr($fqn, 0, $pos); 
	}
	if (!$found) {
		$pos = strpos($fqn, ".myntra.com");
		if ($pos !== false) {
			$found = True;
			$result = substr($fqn, 0, $pos);
		}
	}
	return $result;
}

static function get_server_name() {
	# Check if this command executes only once during initialization
	$hostname = exec("hostname", $retVal);
/*	
	if ($retVal != 0) {
		$hostname = "xn";
	}
	retVal is not 0.
	*/ 
	return self::extract_hostname_without_domain($hostname);
}

	public static $baseUrl = "http://www.myntra.com/";
	public static $httpHost = "www.myntra.com";
	public static $httpsHost = "secure.myntra.com";
	public static $webDir = "";
	public static $documentRoot = "/home/myntradomain/public_html";
	public static $smartyRoot = "/home/myntradomain/smarty";
	public static $serverName;
	public static $configMode = "prod";
	public static $cdnBase = "http://myntra.myntassets.com";
	public static $cssPath = "min/g=css&v=0.01";
	public static $jsPath = "min/g=js&v=0.01";
	public static $combineCssPath = "http://www.myntra.com/min/b=skin1/css";
	public static $combineJsPath = "http://www.myntra.com/min/b=skin1";
	public static $brandshopCssPath = "min/g=brandshop_css&v=0.1";
	public static $brandshopJsPath = "min/g=brandshop_js&v=0.1";
	public static $facebookAppID = "182424375109898";
	public static $facebookAppSecret = "3b0b2f030c788a1d28acbea447c93c85";
	public static $facebookAppName = "myntrashop";
	public static $facebookAppObjectName = "myntrashop:product";
	public static $gaAccountID = "UA-1752831-1";
	public static $secureCdnBase = "https://d6kkp3rk1o2kk.cloudfront.net";
	public static $secureCssPath = "min/g=secure_css&v=0.92";
	public static $secureJsPath = "min/g=secure_js&v=0.92";
	public static $adminJsPath = "min/index.php?g=admin_js&v=0.92";
	public static $trackingSystemUrl = "http://atlascollector.myntra.com";
	public static $webRoot = "/home/myntradomain/public_html/";
	public static $ftpRoot = "/home/xmukesh/";
	public static $portalServiceHost = "http://pricingnpromotion.myntra.com/pp";
	public static $portalAbsolutServiceHost = "https://secure.myntra.com/myntra-absolut-service/absolut";	
	public static $portalAbsolutServiceInternalHost = "http://absolut.myntra.com/myntra-absolut-service/absolut";	
        public static $portalAbsolutSecuredServiceInternalHost = "http://absolut.myntra.com/myntra-absolut-service/securedabsolut";
	public static $vtrServiceHost = "http://vtr.myntra.com/vtr";
	public static $lookGoodServiceHost = "http://lookgood.myntra.com/lookgood";
	public static $currentMABTestAPIVersion = 3;
	public static $cookieprefix = "";
	public static $cookiedomain = ".myntra.com";
	public static $uuidcookiedomain = ".myntra.com";
	public static $xcachePrefix = "";
	public static $searchSystemUrl = "http://ec2-175-41-161-187.ap-southeast-1.compute.amazonaws.com:8080";
	public static $mongodbHost = "ec2-175-41-161-187.ap-southeast-1.compute.amazonaws.com";
	public static $mongodbPort = "27018";
	public static $mongodb = "default";
	public static $sitemapURL = "http://www.myntra.com/";
	public static $sitemapsUploadMaxTries = 3;
	public static $selfHostURL = "http://localhost";
	public static $searchKeywordTokenLimit = "9";
	public static $paymentServiceHost = "https://secure.myntra.com/payments";
	public static $seoDBHost = "180.179.145.19";
	public static $seoDBPort = "27017";
	public static $seoDB = "seo";
	public static $seoDBCollection = "pagedata";
	public static $seoDBOptions = "";
    public static $paymentServiceInternalHost ="http://payments.myntra.com/payments";
    public static $contentServiceHost ="/myntapi/cms/content";
    public static $cmsServiceHost ="http://cms.myntra.com/myntra-cms-service"; 	
    public static $catalogServiceHost ="http://catalogservice.myntra.com/myntra-catalog-service"; 	
    public static $nocombo = "0";
    public static $iscdn = "1";
    public static $notificationsUrl = "usernotification.myntra.com/myntra-notification-webapp/notification/notification";
    public static $notificationsUpsUrl = "ups.myntra.com";
    public static $userGroupServiceUrl = "usergroups.myntra.com/myntra-user-group-webapp/user/group";
    public static $locationGroupServiceUrl = "usergroups.myntra.com/myntra-user-group-webapp/user/locationGroup";
    public static $bannerServiceUrl = "personalizedbanners.myntra.com/myntra-banner-webapp/banner/personalizedbanner";
    public static $lightEncryptionKeys = "0:694847999";
    public static $kulizaEchoBaseUrl = "https://myntra-echo.kuliza.com/api/";
    public static $kulizaClientDomain = "myntra.com";
    public static $kulizaClientBaseUrl = "http://www.myntra.com/";
    public static $loyaltyPointsService = "http://loyaltyprogramservice.myntra.com/lp";
    public static $styleServieUrl = "http://styleservice.myntra.com/style-service/pdpservice/style";
    public static $searchServiceUrl = "http://searchservice.myntra.com/search-service/searchservice/securesearch";
    public static $portalIDPSessionServiceHost = "http://idp.myntra.com/idp/session/";
    public static $giftServiceHost = "http://giftcard.myntra.com/giftcard-service";
    public static $omsSequenceGeneratorUrl = "http://erpoms.myntra.com/myntra-oms-service/oms/sequenceGeneratorService/";
    public static $omsOrderServiceUrl = "http://erpoms.myntra.com/myntra-oms-service/oms/order/";
    public static $taxationServiceUrl = "http://erpoms.myntra.com/myntra-oms-service/oms/orderline/";
	public static $omsOrderReleaseServiceUrl = "http://erpoms.myntra.com/myntra-oms-service/oms/orderrelease/";
    public static $exchangeOrderServiceUrl = "http://erpoms.myntra.com/myntra-oms-service/exchange/order/";
    public static $portalIDPHost = "http://idp.myntra.com/idp/auth/";
    public static $portalIDPSessionHost = "http://idp.myntra.com";
    public static $couponServiceurl = "http://pp-coupon.myntra.com/coupons";
    public static $ppsServiceUrl = "http://pps-internal.myntra.com/myntra-payment-plan-service";
}


HostConfig::$serverName = HostConfig::get_server_name();
	
?>
