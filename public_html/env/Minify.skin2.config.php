<?php
class MinifyConfig_skin2 {
public static $conf = array (
  'groups' => 
  array (
    '3pl' => 
    array (
      'css' => 
      array (
      ),
      'js' => 
      array (
        0 => 'jquery.common.min.js',
        1 => 'jquery.cycle.js',
        2 => 'jquery.mousewheel.js',
        3 => 'jquery.rs.carousel.js',
        4 => 'jquery.autocomplete.extend.js',
        5 => 'jquery.highestzindex.js',
        6 => 'jquery.hoverIntent.min.js',
        7 => 'jquery.placeholder.js',
        8 => 'underscore-min.js',
      ),
      'min_css' => '',
      'min_js' => 'skin2/js/3pl-9fbdd6071d072f381b4de6faa43720a2.min.js',
      'mingz_css' => '',
      'mingz_js' => 'skin2/js/3pl-9fbdd6071d072f381b4de6faa43720a2.js.jgz',
      'iscdn_css' => '',
      'iscdn_js' => 0,
    ),
    'common' => 
    array (
      'css' => 
      array (
        0 => 'myntra-global.css',
        1 => 'header.css',
        2 => 'base.css',
        3 => 'lightbox.css',
        4 => 'login.css',
        5 => 'navigation.css',
        6 => 'product-grid.css',
        7 => 'modal-addtocart.css',
        8 => 'footer.css',
        9 => 'myntra-custom-form.css',
        10 => 'modal-quicklook.css',
        11 => 'product-detail.css',
        12 => 'jquery.ui.core.css',
        13 => 'jquery.ui.autocomplete.css',
      ),
      'js' => 
      array (
        0 => 'ns.js',
        1 => 'base.js',
        2 => 'cookie.js',
        3 => 'fb.js',
        4 => 'utils.js',
        5 => 'gaq.js',
        6 => 'lightbox.js',
        7 => 'tooltip.js',
        8 => 'login.js',
        9 => 'header.js',
        10 => 'footer.js',
        11 => 'tracking.js',
        12 => 'common.js',
        13 => 'myntra.global.js',
        14 => 'myntra.navigation.js',
        15 => 'myntra.product-actions.js',
        16 => 'myntra.modals.js',
        17 => 'myntra.product-grid.js',
        18 => 'notifications.js',
        19 => 'webengage.js',
        20 => 'ga-conversion.js',
      ),
      'min_css' => 'skin2/css/common-e87c59bc57f9e67c03eeb2a006fe9a17.min.css',
      'min_js' => 'skin2/js/common-56e8868a7403eee5e6c1b3a8a9f11614.min.js',
      'mingz_css' => 'skin2/css/common-e87c59bc57f9e67c03eeb2a006fe9a17.css.jgz',
      'mingz_js' => 'skin2/js/common-56e8868a7403eee5e6c1b3a8a9f11614.js.jgz',
      'iscdn_css' => 0,
      'iscdn_js' => 0,
    ),
    'layout' => 
    array (
      'css' => 
      array (
        0 => 'myntra-global.css',
        1 => 'header.css',
        2 => 'base.css',
        3 => 'navigation.css',
        4 => 'footer.css',
        5 => 'lightbox.css',
        6 => 'login.css',
        7 => 'jquery.ui.core.css',
        8 => 'jquery.ui.autocomplete.css',
      ),
      'js' => 
      array (
        0 => 'jquery.rs.carousel.js',
        1 => 'jquery.hoverIntent.min.js',
        2 => 'ns.js',
        3 => 'base.js',
        4 => 'tooltip.js',
        5 => 'myntra.global.js',
        6 => 'header.js',
        7 => 'footer.js',
        8 => 'myntra.navigation.js',
        9 => 'cookie.js',
        10 => 'fb.js',
        11 => 'utils.js',
        12 => 'gaq.js',
        13 => 'lightbox.js',
        14 => 'login.js',
        15 => 'notifications.js',
        16 => 'webengage.js',
      ),
      'min_css' => 'skin2/css/layout-11506db3bb6e2c62f540b4e92cc97ae0.min.css',
      'min_js' => 'skin2/js/layout-e8004a139087e28d5b267a5c31f989ca.min.js',
      'mingz_css' => 'skin2/css/layout-11506db3bb6e2c62f540b4e92cc97ae0.css.jgz',
      'mingz_js' => 'skin2/js/layout-e8004a139087e28d5b267a5c31f989ca.js.jgz',
      'iscdn_css' => 0,
      'iscdn_js' => 0,
    ),
    'home' => 
    array (
      'css' => 
      array (
        0 => 'feature-slides.css',
      ),
      'js' => 
      array (
        0 => 'jquery.rs.carousel-autoscroll-min.js',
        1 => 'quicklook.js',
        2 => 'most-popular-widget.js',
        3 => 'recent-viewed-sizes.js',
        4 => 'recent-widget.js',
        5 => 'home.js',
        6 => 'add-combo-lb.js',
      ),
      'min_css' => 'skin2/css/home-7bff81187179edc2d79b5a3c70de8661.min.css',
      'min_js' => 'skin2/js/home-80fc17c53bc8dc760921ffb50ce4a52d.min.js',
      'mingz_css' => 'skin2/css/home-7bff81187179edc2d79b5a3c70de8661.css.jgz',
      'mingz_js' => 'skin2/js/home-80fc17c53bc8dc760921ffb50ce4a52d.js.jgz',
      'iscdn_css' => 0,
      'iscdn_js' => 0,
    ),
    'checkout-cart' => 
    array (
      'css' => 
      array (
        0 => 'cart.css',
        1 => 'combo_overlay.css',
        2 => 'feature-slides.css',
        3 => 'giftwrap.css',
      ),
      'js' => 
      array (
        0 => 'jquery.rs.carousel-autoscroll.js',
        1 => 'cart.js',
        2 => 'discount-combo.js',
        3 => 'discount-combo-cl.js',
        4 => 'discount-combo-cl-ui.js',
        5 => 'discount-combo-cl-init.js',
        6 => 'checkout.js',
        7 => 'recent-viewed-sizes.js',
        8 => 'quicklook.js',
        9 => 'giftwrap.js',
        10 => 'add-combo-lb.js',
      ),
      'min_css' => 'skin2/css/checkout-cart-5229cba9a0c3fd416a20820b59edd4f4.min.css',
      'min_js' => 'skin2/js/checkout-cart-fa5885e70ed063dc70ee34f95a63ac5a.min.js',
      'mingz_css' => 'skin2/css/checkout-cart-5229cba9a0c3fd416a20820b59edd4f4.css.jgz',
      'mingz_js' => 'skin2/js/checkout-cart-fa5885e70ed063dc70ee34f95a63ac5a.js.jgz',
      'iscdn_css' => 0,
      'iscdn_js' => 0,
    ),
    'checkout-login' => 
    array (
      'css' => 
      array (
        0 => 'checkout.css',
        1 => 'checkout-login.css',
        2 => 'giftwrap.css',
      ),
      'js' => 
      array (
        0 => 'jquery.jqtransform.js',
        1 => 'checkout-login.js',
        2 => 'checkout.js',
        3 => 'giftwrap.js',
      ),
      'min_css' => 'skin2/css/checkout-login-6ede63536d75c8238ba9d70adfe166bd.min.css',
      'min_js' => 'skin2/js/checkout-login-3598741e85ed1c5720b81f1bab8c136e.min.js',
      'mingz_css' => 'skin2/css/checkout-login-6ede63536d75c8238ba9d70adfe166bd.css.jgz',
      'mingz_js' => 'skin2/js/checkout-login-3598741e85ed1c5720b81f1bab8c136e.js.jgz',
      'iscdn_css' => 0,
      'iscdn_js' => 0,
    ),
    'checkout-address' => 
    array (
      'css' => 
      array (
        0 => 'address-list.css',
        1 => 'address-form.css',
        2 => 'checkout.css',
        3 => 'address.css',
        4 => 'giftwrap.css',
      ),
      'js' => 
      array (
        0 => 'jquery.jqtransform.js',
        1 => 'address-list.js',
        2 => 'address-form.js',
        3 => 'address.js',
        4 => 'checkout.js',
        5 => 'giftwrap.js',
      ),
      'min_css' => 'skin2/css/checkout-address-f134cff1354d69e51d9ea1aa76695fc1.min.css',
      'min_js' => 'skin2/js/checkout-address-b4f4349d6e830e85104e9bc060dbbd60.min.js',
      'mingz_css' => 'skin2/css/checkout-address-f134cff1354d69e51d9ea1aa76695fc1.css.jgz',
      'mingz_js' => 'skin2/js/checkout-address-b4f4349d6e830e85104e9bc060dbbd60.js.jgz',
      'iscdn_css' => 0,
      'iscdn_js' => 0,
    ),
    'checkout-payment' => 
    array (
      'css' => 
      array (
        0 => 'checkout.css',
        1 => 'payment.css',
        2 => 'payment-form.css',
        3 => 'express.css',
        4 => 'giftwrap.css',
      ),
      'js' => 
      array (
        0 => 'payment-form-validate.js',
        1 => 'payment.js',
        2 => 'checkout.js',
        3 => 'giftwrap.js',
        4 => 'express.js',
      ),
      'min_css' => 'skin2/css/checkout-payment-d011b09ee3ed33d527cc634daed7be26.min.css',
      'min_js' => 'skin2/js/checkout-payment-2c16824adc80a7403c4cfe47ee0d833d.min.js',
      'mingz_css' => 'skin2/css/checkout-payment-d011b09ee3ed33d527cc634daed7be26.css.jgz',
      'mingz_js' => 'skin2/js/checkout-payment-2c16824adc80a7403c4cfe47ee0d833d.js.jgz',
      'iscdn_css' => 0,
      'iscdn_js' => 0,
    ),
    'express-buy' => 
    array (
      'css' => 
      array (
        0 => 'checkout.css',
        1 => 'payment.css',
        2 => 'payment-form.css',
        3 => 'express.css',
        4 => 'address-list.css',
        5 => 'address-form.css',
      ),
      'js' => 
      array (
        0 => 'jquery.jqtransform.js',
        1 => 'address-list.js',
        2 => 'address-form.js',
        3 => 'payment-form-validate.js',
        4 => 'payment.js',
        5 => 'checkout.js',
        6 => 'express.js',
      ),
      'min_css' => 'skin2/css/express-buy-b5a6e5c9276d3e52a171eb8aa198bb75.min.css',
      'min_js' => 'skin2/js/express-buy-55f630bd16117cf6c1970e56e9be156b.min.js',
      'mingz_css' => 'skin2/css/express-buy-b5a6e5c9276d3e52a171eb8aa198bb75.css.jgz',
      'mingz_js' => 'skin2/js/express-buy-55f630bd16117cf6c1970e56e9be156b.js.jgz',
      'iscdn_css' => 0,
      'iscdn_js' => 0,
    ),
    'checkout-orderconf' => 
    array (
      'css' => 
      array (
        0 => 'checkout.css',
        1 => 'confirmation.css',
      ),
      'js' => 
      array (
        0 => 'checkout.js',
      ),
      'min_css' => 'skin2/css/checkout-orderconf-5986819da9b584064cc24098b4f3d50f.min.css',
      'min_js' => 'skin2/js/checkout-orderconf-7fc46dbbbe34442511ca053285a51087.min.js',
      'mingz_css' => 'skin2/css/checkout-orderconf-5986819da9b584064cc24098b4f3d50f.css.jgz',
      'mingz_js' => 'skin2/js/checkout-orderconf-7fc46dbbbe34442511ca053285a51087.js.jgz',
      'iscdn_css' => 0,
      'iscdn_js' => 0,
    ),
    'pdp' => 
    array (
      'css' => 
      array (
        0 => 'product-page.css',
        1 => 'sizechart.css',
        2 => 'feature-slides.css',
        3 => 'combo_overlay.css',
        4 => 'pincode-widget.css',
        5 => 'pdpcoupon-widget.css',
        6 => 'buy-look.css',
        7 => 'pdp-looks.css',
      ),
      'js' => 
      array (
        0 => 'json2.js',
        1 => 'pincode-widget.js',
        2 => 'recent-widget.js',
        3 => 'myntra.product-page.js',
        4 => 'myntra.pdp.zoom.js',
        5 => 'sizechart-position.js',
        6 => 'sizechart.js',
        7 => 'cloud-zoom.js',
        8 => 'discount-combo.js',
        9 => 'recent-viewed-sizes.js',
        10 => 'quicklook.js',
        11 => 'add-combo-lb.js',
        12 => 'pdpcoupon-widget.js',
        13 => 'buy-look.js',
        14 => 'pdp-looks.js',
        15 => 'scrolldepth.js',
        16 => 'scroll-tracker.js',
        17 => 'ga-conversion.js',
      ),
      'min_css' => 'skin2/css/pdp-544ca9d702e6d71e0a72dbaad7d28174.min.css',
      'min_js' => 'skin2/js/pdp-08f9dfb6c4668bd2417d14bdf5237c93.min.js',
      'mingz_css' => 'skin2/css/pdp-544ca9d702e6d71e0a72dbaad7d28174.css.jgz',
      'mingz_js' => 'skin2/js/pdp-08f9dfb6c4668bd2417d14bdf5237c93.js.jgz',
      'iscdn_css' => 0,
      'iscdn_js' => 0,
    ),
    'search' => 
    array (
      'css' => 
      array (
        0 => 'search-results.css',
        1 => 'feature-slides.css',
      ),
      'js' => 
      array (
        0 => 'jquery.lazyload.min.js',
        1 => 'jquery.ba-hashchange.min.js',
        2 => 'json2.js',
        3 => 'color-options.js',
        4 => 'search-results.js',
        5 => 'recent-viewed-sizes.js',
        6 => 'recent-widget.js',
        7 => 'quicklook.js',
        8 => 'add-combo-lb.js',
      ),
      'min_css' => 'skin2/css/search-bf9f240af1f9eeac6a2c9de28ff92564.min.css',
      'min_js' => 'skin2/js/search-fced926d35c3eb2bb95ed3cef6a87e3d.min.js',
      'mingz_css' => 'skin2/css/search-bf9f240af1f9eeac6a2c9de28ff92564.css.jgz',
      'mingz_js' => 'skin2/js/search-fced926d35c3eb2bb95ed3cef6a87e3d.js.jgz',
      'iscdn_css' => 0,
      'iscdn_js' => 0,
    ),
    'landing' => 
    array (
      'css' => 
      array (
        0 => 'search-results.css',
        1 => 'landing-page.css',
        2 => 'feature-slides.css',
        3 => 'cat-landing.css',
        4 => 'cat-brands.css',
      ),
      'js' => 
      array (
        0 => 'quicklook.js',
        1 => 'product-sections.js',
        2 => 'landing-pages.js',
        3 => 'color-options.js',
        4 => 'add-combo-lb.js',
      ),
      'min_css' => 'skin2/css/landing-ffd6b8985e5160ddd7ba6ba1c4ff8fd8.min.css',
      'min_js' => 'skin2/js/landing-5249b0a01cd2044322a5010ebf83e119.min.js',
      'mingz_css' => 'skin2/css/landing-ffd6b8985e5160ddd7ba6ba1c4ff8fd8.css.jgz',
      'mingz_js' => 'skin2/js/landing-5249b0a01cd2044322a5010ebf83e119.js.jgz',
      'iscdn_css' => 0,
      'iscdn_js' => 0,
    ),
    'my' => 
    array (
      'css' => 
      array (
        0 => 'modal-orderdetails.css',
        1 => 'address-list.css',
        2 => 'address-form.css',
        3 => 'exchange.css',
        4 => 'wizard.css',
        5 => 'return.css',
        6 => 'sizechart.css',
        7 => 'my.css',
        8 => 'mymynt-loyalty.css',
        9 => 'giftwrap.css',
        10 => 'payment-form.css',
      ),
      'js' => 
      array (
        0 => 'jquery.jqtransform.js',
        1 => 'jquery.timeago.js',
        2 => 'address-list.js',
        3 => 'address-form.js',
        4 => 'wizard.js',
        5 => 'return.js',
        6 => 'neft-account-list.js',
        7 => 'neft-account-form.js',
        8 => 'exchange.js',
        9 => 'sizechart-position.js',
        10 => 'sizechart-exchange.js',
        11 => 'myprofile.js',
        12 => 'myorders.js',
        13 => 'myreturns.js',
        14 => 'mymyntcredits.js',
        15 => 'mymyntloyalty.js',
        16 => 'myreferrals.js',
        17 => 'mygiftcards.js',
        18 => 'my.js',
        19 => 'quicklook.js',
        20 => 'giftwrap.js',
        21 => 'add-combo-lb.js',
      ),
      'min_css' => 'skin2/css/my-d6fc585017d5c60c41f8f2849c786763.min.css',
      'min_js' => 'skin2/js/my-fdf725a417476da30094920c1fb9cad2.min.js',
      'mingz_css' => 'skin2/css/my-d6fc585017d5c60c41f8f2849c786763.css.jgz',
      'mingz_js' => 'skin2/js/my-fdf725a417476da30094920c1fb9cad2.js.jgz',
      'iscdn_css' => 0,
      'iscdn_js' => 0,
    ),
    'static_pages' => 
    array (
      'css' => 
      array (
        0 => 'static-pages.css',
        1 => 'contactus.css',
        2 => 'order-list.css',
        3 => 'feature-slides.css',
      ),
      'js' => 
      array (
        0 => 'jquery.jqtransform.js',
        1 => 'static-pages.js',
        2 => 'jquery.MultiFile.js',
        3 => 'contactus.js',
        4 => 'recent-viewed-sizes.js',
        5 => 'recent-widget.js',
        6 => 'quicklook.js',
        7 => 'add-combo-lb.js',
      ),
      'min_css' => 'skin2/css/static_pages-3ebd85fe2f3d06fa2e0a9a9873bdac12.min.css',
      'min_js' => 'skin2/js/static_pages-7b5bc16f6eededa57b3ab95511394dca.min.js',
      'mingz_css' => 'skin2/css/static_pages-3ebd85fe2f3d06fa2e0a9a9873bdac12.css.jgz',
      'mingz_js' => 'skin2/js/static_pages-7b5bc16f6eededa57b3ab95511394dca.js.jgz',
      'iscdn_css' => 0,
      'iscdn_js' => 0,
    ),
    'reset_password' => 
    array (
      'css' => 
      array (
        0 => 'reset-password.css',
      ),
      'js' => 
      array (
        0 => 'reset-password.js',
        1 => 'quicklook.js',
        2 => 'add-combo-lb.js',
      ),
      'min_css' => 'skin2/css/reset_password-abc34c901fd54182fe26ab2603af98ec.min.css',
      'min_js' => 'skin2/js/reset_password-d5f04fabb607694da15e84e9a7bd0319.min.js',
      'mingz_css' => 'skin2/css/reset_password-abc34c901fd54182fe26ab2603af98ec.css.jgz',
      'mingz_js' => 'skin2/js/reset_password-d5f04fabb607694da15e84e9a7bd0319.js.jgz',
      'iscdn_css' => 0,
      'iscdn_js' => 0,
    ),
    'confirmation' => 
    array (
      'css' => 
      array (
      ),
      'js' => 
      array (
        0 => 'confirmation-tracking.js',
      ),
      'min_css' => '',
      'min_js' => 'skin2/js/confirmation-7af411b613bfc65ab019cbbfcfc19cd8.min.js',
      'mingz_css' => '',
      'mingz_js' => 'skin2/js/confirmation-7af411b613bfc65ab019cbbfcfc19cd8.js.jgz',
      'iscdn_css' => '',
      'iscdn_js' => 0,
    ),
    'vtr' => 
    array (
      'css' => 
      array (
        0 => 'vtr.css',
        1 => 'vtr-products.css',
        2 => 'buy-look.css',
        3 => 'vtr-room.css',
      ),
      'js' => 
      array (
        0 => 'vtr.js',
        1 => 'vtr-products.js',
        2 => 'buy-look.js',
        3 => 'vtr-room.js',
      ),
      'min_css' => 'skin2/css/vtr-269576a832ddde43624567ca2d463579.min.css',
      'min_js' => 'skin2/js/vtr-570b07a00e453d8d0c9762c5aaeba434.min.js',
      'mingz_css' => 'skin2/css/vtr-269576a832ddde43624567ca2d463579.css.jgz',
      'mingz_js' => 'skin2/js/vtr-570b07a00e453d8d0c9762c5aaeba434.js.jgz',
      'iscdn_css' => 0,
      'iscdn_js' => 0,
    ),
    'feedback' => 
    array (
      'css' => 
      array (
        0 => 'feedback.css',
      ),
      'js' => 
      array (
        0 => 'jquery-1.7.1.min.js',
        1 => 'jquery.jqtransform.js',
        2 => 'feedback.js',
      ),
      'min_css' => 'skin2/css/feedback-6adf9fb78949f549bc50699133ebaa33.min.css',
      'min_js' => 'skin2/js/feedback-b492691401e378d682db4f8de9ade43d.min.js',
      'mingz_css' => 'skin2/css/feedback-6adf9fb78949f549bc50699133ebaa33.css.jgz',
      'mingz_js' => 'skin2/js/feedback-b492691401e378d682db4f8de9ade43d.js.jgz',
      'iscdn_css' => 0,
      'iscdn_js' => 0,
    ),
    'lookgood' => 
    array (
      'css' => 
      array (
        0 => 'lookgood.css',
        1 => 'buy-look.css',
      ),
      'js' => 
      array (
        0 => 'quicklook.js',
        1 => 'lookgood.js',
        2 => 'buy-look.js',
        3 => 'add-combo-lb.js',
      ),
      'min_css' => 'skin2/css/lookgood-61944a3da63429e7120ef9274ceb65ba.min.css',
      'min_js' => 'skin2/js/lookgood-a40cd61112a5f290511746b2a0badead.min.js',
      'mingz_css' => 'skin2/css/lookgood-61944a3da63429e7120ef9274ceb65ba.css.jgz',
      'mingz_js' => 'skin2/js/lookgood-a40cd61112a5f290511746b2a0badead.js.jgz',
      'iscdn_css' => 0,
      'iscdn_js' => 0,
    ),
    'star-diary' => 
    array (
      'css' => 
      array (
      ),
      'js' => 
      array (
        0 => 'jquery.ui.core.js',
        1 => 'jquery.ui.widget.js',
        2 => 'jquery.ui.mouse.js',
        3 => 'jquery.ui.draggable.js',
        4 => 'jquery.effects.core.js',
        5 => 'jquery.booklet.min.js',
      ),
      'min_css' => '',
      'min_js' => 'skin2/js/star-diary-5067d660f5498ac1fc02c433fde6503f.min.js',
      'mingz_css' => '',
      'mingz_js' => 'skin2/js/star-diary-5067d660f5498ac1fc02c433fde6503f.js.jgz',
      'iscdn_css' => '',
      'iscdn_js' => 0,
    ),
    'giftcard' => 
    array (
      'css' => 
      array (
        0 => 'giftcard.css',
      ),
      'js' => 
      array (
        0 => 'giftcard.js',
      ),
      'min_css' => 'skin2/css/giftcard-b6e399a3ec4e96fd80074208c0150095.min.css',
      'min_js' => 'skin2/js/giftcard-7a4994adccd00000713e534ea547c8ea.min.js',
      'mingz_css' => 'skin2/css/giftcard-b6e399a3ec4e96fd80074208c0150095.css.jgz',
      'mingz_js' => 'skin2/js/giftcard-7a4994adccd00000713e534ea547c8ea.js.jgz',
      'iscdn_css' => 0,
      'iscdn_js' => 0,
    ),
    'shopfest' => 
    array (
      'css' => 
      array (
        0 => 'shopfest.css',
      ),
      'js' => 
      array (
        0 => 'shopfest.js',
      ),
      'min_css' => 'skin2/css/shopfest-cd8a2b4687678c25fbf8a659a7ab38ac.min.css',
      'min_js' => 'skin2/js/shopfest-262ca1a7f6dfcb1ad10fea5ef2161bcb.min.js',
      'mingz_css' => 'skin2/css/shopfest-cd8a2b4687678c25fbf8a659a7ab38ac.css.jgz',
      'mingz_js' => 'skin2/js/shopfest-262ca1a7f6dfcb1ad10fea5ef2161bcb.js.jgz',
      'iscdn_css' => 0,
      'iscdn_js' => 0,
    ),
    'express-settings' => 
    array (
      'css' => 
      array (
        0 => 'my.css',
        1 => 'payment-form.css',
        2 => 'address-list.css',
        3 => 'address-form.css',
      ),
      'js' => 
      array (
        0 => 'jquery.jqtransform.js',
        1 => 'address-list.js',
        2 => 'address-form.js',
        3 => 'payment-form-validate.js',
        4 => 'express-settings.js',
      ),
      'min_css' => 'skin2/css/express-settings-ce5c41ce9ad9ec20c54192ad4bda4146.min.css',
      'min_js' => 'skin2/js/express-settings-1975bff46fda7dcafe72692434f244bf.min.js',
      'mingz_css' => 'skin2/css/express-settings-ce5c41ce9ad9ec20c54192ad4bda4146.css.jgz',
      'mingz_js' => 'skin2/js/express-settings-1975bff46fda7dcafe72692434f244bf.js.jgz',
      'iscdn_css' => 0,
      'iscdn_js' => 0,
    ),
    'newcheckout' => 
    array (
      'css' => 
      array (
        0 => 'jquery.ui.core.css',
        1 => 'jquery.ui.autocomplete.css',
        2 => 'header.css',
        3 => 'myntra-global.css',
        4 => 'myntra-custom-form.css',
        5 => 'base.css',
        6 => 'lightbox.css',
        7 => 'login.css',
        8 => 'address-form.css',
        9 => 'giftwrap.css',
        10 => 'personalise.css',
        11 => 'v2/base.css',
        12 => 'v2/checkout.css',
        13 => 'v2/cart-summary.css',
        14 => 'v2/address-summary.css',
        15 => 'v2/payment.css',
        16 => 'v2/cart.css',
        17 => 'v2/checkoutBag.css',
      ),
      'js' => 
      array (
        0 => 'ns.js',
        1 => 'base.js',
        2 => 'cookie.js',
        3 => 'fb.js',
        4 => 'utils.js',
        5 => 'gaq.js',
        6 => 'lightbox.js',
        7 => 'login.js',
        8 => 'tooltip.js',
        9 => 'tracking.js',
        10 => 'jquery.mousewheel.js',
        11 => 'v2/checkout.js',
        12 => 'v2/payment.js',
        13 => 'webengage.js',
        14 => 'ga-conversion.js',
      ),
      'min_css' => 'skin2/css/newcheckout-2cf258eacd71d4e592a1ceedc2a3dd31.min.css',
      'min_js' => 'skin2/js/newcheckout-558faef346c68ba8c4ce4ab56ac7a642.min.js',
      'mingz_css' => 'skin2/css/newcheckout-2cf258eacd71d4e592a1ceedc2a3dd31.css.jgz',
      'mingz_js' => 'skin2/js/newcheckout-558faef346c68ba8c4ce4ab56ac7a642.js.jgz',
      'iscdn_css' => 0,
      'iscdn_js' => 0,
    ),
    'newconfirm' => 
    array (
      'css' => 
      array (
        0 => 'header.css',
        1 => 'myntra-global.css',
        2 => 'base.css',
        3 => 'lightbox.css',
        4 => 'socialfeed_style.css',
        5 => 'v2/base.css',
        6 => 'v2/confirm.css',
      ),
      'js' => 
      array (
        0 => 'ns.js',
        1 => 'base.js',
        2 => 'cookie.js',
        3 => 'fb.js',
        4 => 'utils.js',
        5 => 'gaq.js',
        6 => 'lightbox.js',
        7 => 'tracking.js',
        8 => 'checkout.js',
        9 => 'v2/checkout.js',
        10 => 'v2/confirm.js',
        11 => 'webengage.js',
        12 => 'ga-conversion.js',
      ),
      'min_css' => 'skin2/css/newconfirm-606626a1191a4641d99acbf06bdf10e0.min.css',
      'min_js' => 'skin2/js/newconfirm-54c1138f1a65d24d8bd5a79822eae8cd.min.js',
      'mingz_css' => 'skin2/css/newconfirm-606626a1191a4641d99acbf06bdf10e0.css.jgz',
      'mingz_js' => 'skin2/js/newconfirm-54c1138f1a65d24d8bd5a79822eae8cd.js.jgz',
      'iscdn_css' => 0,
      'iscdn_js' => 0,
    ),
    'combo' => 
    array (
      'css' => 
      array (
        0 => 'megalist.css',
        1 => 'static-pages.css',
        2 => 'search-results.css',
        3 => 'feature-slides.css',
        4 => 'combo-page.css',
      ),
      'js' => 
      array (
        0 => 'jquery.lazyload.min.js',
        1 => 'json2.js',
        2 => 'megalist.js',
        3 => 'combo-page.js',
        4 => 'quicklook.js',
        5 => 'add-combo-lb.js',
      ),
      'min_css' => 'skin2/css/combo-448c9e82aa963a33e1938166c8ee0b69.min.css',
      'min_js' => 'skin2/js/combo-31f2225d396a120addf69b07c52f86b2.min.js',
      'mingz_css' => 'skin2/css/combo-448c9e82aa963a33e1938166c8ee0b69.css.jgz',
      'mingz_js' => 'skin2/js/combo-31f2225d396a120addf69b07c52f86b2.js.jgz',
      'iscdn_css' => 0,
      'iscdn_js' => 0,
    ),
    'register-new' => 
    array (
      'css' => 
      array (
      ),
      'js' => 
      array (
        0 => 'register-new.js',
      ),
      'min_css' => '',
      'min_js' => 'skin2/js/register-new-cfa1f79c5c07cba3671b959f18a7e25c.min.js',
      'mingz_css' => '',
      'mingz_js' => 'skin2/js/register-new-cfa1f79c5c07cba3671b959f18a7e25c.js.jgz',
      'iscdn_css' => '',
      'iscdn_js' => 0,
    ),
    'kuliza' => 
    array (
      'css' => 
      array (
        0 => '../../kuliza/css/kuliza_live_feed.css',
      ),
      'js' => 
      array (
        0 => '../../kuliza/js/jquery-1.6.2.min.js',
        1 => '../../kuliza/js/jquery.masonry.min.js',
        2 => '../../kuliza/js/modernizr-2.5.3.min.js',
      ),
      'min_css' => 'skin2/css/kuliza-ec3021cf745d12b12faecd48e5010a3a.min.css',
      'min_js' => 'skin2/js/kuliza-9c23c84d00cdef4d6208bf01bef40f78.min.js',
      'mingz_css' => 'skin2/css/kuliza-ec3021cf745d12b12faecd48e5010a3a.css.jgz',
      'mingz_js' => 'skin2/js/kuliza-9c23c84d00cdef4d6208bf01bef40f78.js.jgz',
      'iscdn_css' => 0,
      'iscdn_js' => 0,
    ),
    'express-buy-confimation' => 
    array (
      'js' => 
      array (
      ),
      'css' => 
      array (
        0 => 'v2/base.css',
        1 => 'express-confirmation.css',
      ),
      'min_css' => 'skin2/css/express-buy-confimation-5442ebfb09770834bc00f2ed2b55d6e8.min.css',
      'min_js' => '',
      'mingz_css' => 'skin2/css/express-buy-confimation-5442ebfb09770834bc00f2ed2b55d6e8.css.jgz',
      'mingz_js' => '',
      'iscdn_css' => 0,
      'iscdn_js' => '',
    ),
  ),
  'pages' => 
  array (
    '/index.php' => 
    array (
      0 => '3pl',
      1 => 'common',
      2 => 'home',
    ),
    '/home.php' => 
    array (
      0 => '3pl',
      1 => 'common',
      2 => 'home',
    ),
    '/homepage.php' => 
    array (
      0 => '3pl',
      1 => 'common',
      2 => 'home',
    ),
    '/mkcommoncustomization.php' => 
    array (
      0 => '3pl',
      1 => 'common',
      2 => 'pdp',
    ),
    '/pdp.php' => 
    array (
      0 => '3pl',
      1 => 'common',
      2 => 'pdp',
    ),
    '/aboutus.php' => 
    array (
      0 => '3pl',
      1 => 'common',
      2 => 'static_pages',
    ),
    '/careers.php' => 
    array (
      0 => '3pl',
      1 => 'common',
      2 => 'static_pages',
    ),
    '/contact_us.php' => 
    array (
      0 => '3pl',
      1 => 'common',
      2 => 'static_pages',
    ),
    '/mkfaq.php' => 
    array (
      0 => '3pl',
      1 => 'common',
      2 => 'static_pages',
    ),
    '/myntrareturnpolicy.php' => 
    array (
      0 => '3pl',
      1 => 'common',
      2 => 'static_pages',
    ),
    '/privacy_policy.php' => 
    array (
      0 => '3pl',
      1 => 'common',
      2 => 'static_pages',
    ),
    '/termandcondition.php' => 
    array (
      0 => '3pl',
      1 => 'common',
      2 => 'static_pages',
    ),
    '/staticpagecontroller.php' => 
    array (
      0 => '3pl',
      1 => 'common',
      2 => 'static_pages',
    ),
    '/landingPages.php' => 
    array (
      0 => '3pl',
      1 => 'common',
      2 => 'landing',
    ),
    '/browseBrands.php' => 
    array (
      0 => '3pl',
      1 => 'common',
      2 => 'landing',
    ),
    '/solrRequestHandler.php' => 
    array (
      0 => '3pl',
      1 => 'common',
      2 => 'search',
    ),
    '/search.php' => 
    array (
      0 => '3pl',
      1 => 'common',
      2 => 'search',
    ),
    '/mkmycart.php' => 
    array (
      0 => '3pl',
      1 => 'common',
      2 => 'checkout-cart',
    ),
    '/mkmycartmultiple.php' => 
    array (
      0 => '3pl',
      1 => 'common',
      2 => 'checkout-cart',
    ),
    '/mkmycartmultiple_re.php' => 
    array (
      0 => '3pl',
      1 => 'common',
      2 => 'checkout-cart',
    ),
    '/mkcustomeraddress.php' => 
    array (
      0 => '3pl',
      1 => 'common',
      2 => 'checkout-address',
    ),
    '/checkout-login.php' => 
    array (
      0 => '3pl',
      1 => 'common',
      2 => 'checkout-login',
    ),
    '/checkout-address.php' => 
    array (
      0 => '3pl',
      1 => 'common',
      2 => 'checkout-address',
    ),
    '/mkpaymentoptions.php' => 
    array (
      0 => '3pl',
      1 => 'common',
      2 => 'checkout-payment',
    ),
    '/mkgiftcardpaymentoptions.php' => 
    array (
      0 => '3pl',
      1 => 'common',
      2 => 'checkout-payment',
    ),
    '/giftCards.php' => 
    array (
      0 => '3pl',
      1 => 'common',
      2 => 'giftcard',
    ),
    '/mkorderBook.php' => 
    array (
      0 => '3pl',
      1 => 'common',
      2 => 'checkout-orderconf',
    ),
    '/confirmation.php' => 
    array (
      0 => '3pl',
      1 => 'common',
      2 => 'checkout-orderconf',
      3 => 'confirmation',
    ),
    '/expressConfirmation.php' => 
    array (
      0 => '3pl',
      1 => 'common',
      2 => 'express-buy-confimation',
    ),
    '/reset_password.php' => 
    array (
      0 => '3pl',
      1 => 'common',
      2 => 'reset_password',
    ),
    '/register.php' => 
    array (
      0 => '3pl',
      1 => 'common',
    ),
    '/mymyntra.php' => 
    array (
      0 => '3pl',
      1 => 'common',
      2 => 'my',
    ),
    '/myprofile.php' => 
    array (
      0 => '3pl',
      1 => 'common',
      2 => 'my',
    ),
    '/vtr.php' => 
    array (
      0 => '3pl',
      1 => 'common',
      2 => 'vtr',
    ),
    '/shopfest.php' => 
    array (
      0 => '3pl',
      1 => 'common',
      2 => 'shopfest',
    ),
    '/mrp_landing.php' => 
    array (
      0 => '3pl',
      1 => 'common',
      2 => 'static_pages',
    ),
    '/order_feedback.php' => 
    array (
      0 => '3pl',
      1 => 'common',
      2 => 'feedback',
    ),
    '/feedback.php' => 
    array (
      0 => '3pl',
      1 => 'common',
      2 => 'feedback',
    ),
    '/giftCardConfirmation.php' => 
    array (
      0 => '3pl',
      1 => 'common',
      2 => 'checkout-orderconf',
      3 => 'confirmation',
    ),
    '/lgLookBookController.php' => 
    array (
      0 => '3pl',
      1 => 'common',
      2 => 'lookgood',
    ),
    '/lgStarNStyleController.php' => 
    array (
      0 => '3pl',
      1 => 'common',
      2 => 'lookgood',
    ),
    '/lgStarDiaryController.php' => 
    array (
      0 => '3pl',
      1 => 'common',
      2 => 'star-diary',
      3 => 'lookgood',
    ),
    '/expressbuy.php' => 
    array (
      0 => '3pl',
      1 => 'common',
      2 => 'express-buy',
    ),
    '/mySavedCards.php' => 
    array (
      0 => '3pl',
      1 => 'common',
      2 => 'express-settings',
    ),
    '/checkout.php' => 
    array (
      0 => '3plcheckout',
      1 => 'newcheckout',
    ),
    '/cart.php' => 
    array (
      0 => '3plcheckout',
      1 => 'newcheckout',
    ),
    '/confirm.php' => 
    array (
      0 => '3plcheckout',
      1 => 'newconfirm',
    ),
    '/comboController.php' => 
    array (
      0 => '3pl',
      1 => 'common',
      2 => 'combo',
    ),
    '/securelogin.php' => 
    array (
      0 => '3pl',
      1 => 'common',
      2 => 'register-new',
    ),
    '/leaderboard.php' => 
    array (
      0 => '3pl',
      1 => 'common',
    ),
    '/kuliza/kuliza_live_feed.php' => 
    array (
      0 => 'kuliza',
    ),
  ),
);
}
