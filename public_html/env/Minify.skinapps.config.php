<?php
class MinifyConfig_skinapps {
public static $conf = array (
  'groups' => 
  array (
    '3pl' => 
    array (
      'css' => 
      array (
      ),
      'js' => 
      array (
        0 => 'jquery.js',
        1 => 'modernizr-2.6.1.min.js',
        2 => 'modernizr-tests.js',
        3 => 'jquery.ui.core.js',
        4 => 'jquery.ui.widget.js',
        5 => 'jquery.ui.mouse.js',
        6 => 'jquery.ui.position.js',
        7 => 'jquery.ui.autocomplete.js',
        8 => 'jquery.autocomplete.extend.js',
        9 => 'fastbutton.js',
        10 => 'iscroll.js',
        11 => 'hammer.js',
        12 => 'jquery.flexslider.js',
        13 => 'jquery.hammer.js',
        14 => 'jquery.pageslide.js',
        15 => 'jquery.touchSwipe.min.js',
        16 => 'json2.js',
      ),
      'min_css' => '',
      'min_js' => 'skinapps/js/3pl-f8768047fa65669327267fa0126433f3.min.js',
      'mingz_css' => '',
      'mingz_js' => 'skinapps/js/3pl-f8768047fa65669327267fa0126433f3.js.jgz',
      'iscdn_css' => '',
      'iscdn_js' => 0,
    ),
    'common' => 
    array (
      'css' => 
      array (
        0 => 'myntra-global.css',
        1 => 'jquery.ui.base.css',
        2 => 'jquery.ui.core.css',
        3 => 'jquery.ui.autocomplete.css',
        4 => 'common.css',
        5 => 'rightmenu.css',
        6 => 'common-320.css',
        7 => 'common-480.css',
        8 => 'common-768.css',
        9 => 'home.css',
        10 => 'home-responsive.css',
        11 => 'footer.css',
        12 => 'nav.css',
        13 => 'lightbox.css',
        14 => 'login.css',
        15 => 'login-responsive.css',
      ),
      'js' => 
      array (
        0 => 'ns.js',
        1 => 'base.js',
        2 => 'cookie.js',
        3 => 'fb.js',
        4 => 'utils.js',
        5 => 'gaq.js',
        6 => 'tooltip.js',
        7 => 'pincode-widget.js',
        8 => 'myntra.global.js',
        9 => 'header.js',
        10 => 'lightbox.js',
        11 => 'nav.js',
        12 => 'myntra.modals.js',
        13 => '/skinapps/js/login.js',
        14 => 'tracking.js',
        15 => 'common.js',
        16 => 'rightmenu.js',
        17 => 'fb-actions.js',
        18 => 'ga-conversion.js',
      ),
      'min_css' => 'skinapps/css/common-05302103f154b6899c06f116d422db14.min.css',
      'min_js' => 'skinapps/js/common-ca075ebb35f8931b5dc8a0ae27687078.min.js',
      'mingz_css' => 'skinapps/css/common-05302103f154b6899c06f116d422db14.css.jgz',
      'mingz_js' => 'skinapps/js/common-ca075ebb35f8931b5dc8a0ae27687078.js.jgz',
      'iscdn_css' => 0,
      'iscdn_js' => 0,
    ),
    'checkout-common-andriod' => 
    array (
      'css' => 
      array (
        0 => '/skinapps/andriod/css/checkout-common.css',
      ),
      'js' => 
      array (
      ),
      'min_css' => 'skinapps/css/checkout-common-andriod-b73082b1a0c39d17710ec67d5fa43ade.min.css',
      'min_js' => '',
      'mingz_css' => 'skinapps/css/checkout-common-andriod-b73082b1a0c39d17710ec67d5fa43ade.css.jgz',
      'mingz_js' => '',
      'iscdn_css' => 0,
      'iscdn_js' => '',
    ),
    'checkout-common-iphone' => 
    array (
      'css' => 
      array (
        0 => '/skinapps/iphone/css/checkout-common.css',
      ),
      'js' => 
      array (
      ),
      'min_css' => 'skinapps/css/checkout-common-iphone-f7b971511ea303bc9e8a0dd5c6e70e57.min.css',
      'min_js' => '',
      'mingz_css' => 'skinapps/css/checkout-common-iphone-f7b971511ea303bc9e8a0dd5c6e70e57.css.jgz',
      'mingz_js' => '',
      'iscdn_css' => 0,
      'iscdn_js' => '',
    ),
    'checkout-cart' => 
    array (
      'css' => 
      array (
        0 => 'cart.css',
        1 => 'cart-responsive.css',
        2 => 'mod-coupons.css',
        3 => 'giftwrap-responsive.css',
        4 => 'giftwrap.css',
      ),
      'js' => 
      array (
        0 => 'cart.js',
        1 => 'giftwrap.js',
      ),
      'min_css' => 'skinapps/css/checkout-cart-cc87423f6e1f88aa1d698590e87a44f3.min.css',
      'min_js' => 'skinapps/js/checkout-cart-32edb299ec1e17062093e3e025f66b81.min.js',
      'mingz_css' => 'skinapps/css/checkout-cart-cc87423f6e1f88aa1d698590e87a44f3.css.jgz',
      'mingz_js' => 'skinapps/js/checkout-cart-32edb299ec1e17062093e3e025f66b81.js.jgz',
      'iscdn_css' => 0,
      'iscdn_js' => 0,
    ),
    'checkout-cart-andriod' => 
    array (
      'css' => 
      array (
        0 => '/skinapps/andriod/css/checkout-cart.css',
      ),
      'js' => 
      array (
      ),
      'min_css' => 'skinapps/css/checkout-cart-andriod-bac0490f169a1287ab8013bcd444d44f.min.css',
      'min_js' => '',
      'mingz_css' => 'skinapps/css/checkout-cart-andriod-bac0490f169a1287ab8013bcd444d44f.css.jgz',
      'mingz_js' => '',
      'iscdn_css' => 0,
      'iscdn_js' => '',
    ),
    'checkout-cart-iphone' => 
    array (
      'css' => 
      array (
        0 => '/skinapps/iphone/css/checkout-cart.css',
      ),
      'js' => 
      array (
      ),
      'min_css' => 'skinapps/css/checkout-cart-iphone-339cb3ad8961d99dd04c28d545855a5f.min.css',
      'min_js' => '',
      'mingz_css' => 'skinapps/css/checkout-cart-iphone-339cb3ad8961d99dd04c28d545855a5f.css.jgz',
      'mingz_js' => '',
      'iscdn_css' => 0,
      'iscdn_js' => '',
    ),
    'secure-common' => 
    array (
      'js' => 
      array (
        0 => 'jquery.js',
        1 => 'ns.js',
        2 => 'base.js',
        3 => 'utils.js',
        4 => 'lightbox.js',
        5 => 'tracking.js',
        6 => 'jquery.ui.core.js',
        7 => 'jquery.ui.widget.js',
        8 => 'jquery.ui.position.js',
        9 => 'jquery.ui.autocomplete.js',
        10 => 'jquery.autocomplete.extend.js',
        11 => 'jquery.tappable.js',
        12 => 'jquery.pageslide.js',
        13 => 'jquery.jqtransform.js',
        14 => 'placeholder.jquery.js',
        15 => 'slide-summary.js',
        16 => 'ga-conversion.js',
      ),
      'css' => 
      array (
        0 => 'myntra-global.css',
        1 => 'footer.css',
        2 => '/skinapps/andriod/css/common-checkout.css',
        3 => 'common-checkout-responsive.css',
        4 => 'lightbox.css',
      ),
      'min_css' => 'skinapps/css/secure-common-0e5ca1d77539a971fa843d321fd0085d.min.css',
      'min_js' => 'skinapps/js/secure-common-94951faf5e5eebfc617bc98efd9fa3ed.min.js',
      'mingz_css' => 'skinapps/css/secure-common-0e5ca1d77539a971fa843d321fd0085d.css.jgz',
      'mingz_js' => 'skinapps/js/secure-common-94951faf5e5eebfc617bc98efd9fa3ed.js.jgz',
      'iscdn_css' => 0,
      'iscdn_js' => 0,
    ),
    'checkout-address' => 
    array (
      'js' => 
      array (
        0 => 'address-form.js',
        1 => 'address.js',
        2 => 'address-list.js',
        3 => 'tooltip.js',
      ),
      'css' => 
      array (
        0 => 'address.css',
        1 => 'address-list.css',
        2 => 'address-form.css',
        3 => 'checkout.css',
      ),
      'min_css' => 'skinapps/css/checkout-address-5a28bf9dcd8ed497287d7a6b8d72894d.min.css',
      'min_js' => 'skinapps/js/checkout-address-696eced0840e2f6f5e75b8e6a5d6250b.min.js',
      'mingz_css' => 'skinapps/css/checkout-address-5a28bf9dcd8ed497287d7a6b8d72894d.css.jgz',
      'mingz_js' => 'skinapps/js/checkout-address-696eced0840e2f6f5e75b8e6a5d6250b.js.jgz',
      'iscdn_css' => 0,
      'iscdn_js' => 0,
    ),
    'checkout-address-andriod' => 
    array (
      'css' => 
      array (
        0 => '/skinapps/andriod/css/checkout-address.css',
      ),
      'js' => 
      array (
      ),
      'min_css' => 'skinapps/css/checkout-address-andriod-85064c21752268ec9b9e1551df8b9749.min.css',
      'min_js' => '',
      'mingz_css' => 'skinapps/css/checkout-address-andriod-85064c21752268ec9b9e1551df8b9749.css.jgz',
      'mingz_js' => '',
      'iscdn_css' => 0,
      'iscdn_js' => '',
    ),
    'checkout-address-iphone' => 
    array (
      'css' => 
      array (
        0 => '/skinapps/iphone/css/checkout-address.css',
      ),
      'js' => 
      array (
      ),
      'min_css' => 'skinapps/css/checkout-address-iphone-70bc532841178b61f6046d245e6d48a3.min.css',
      'min_js' => '',
      'mingz_css' => 'skinapps/css/checkout-address-iphone-70bc532841178b61f6046d245e6d48a3.css.jgz',
      'mingz_js' => '',
      'iscdn_css' => 0,
      'iscdn_js' => '',
    ),
    'checkout-payment' => 
    array (
      'js' => 
      array (
        0 => 'cc-address-list.js',
        1 => 'tooltip.js',
        2 => 'payment.js',
        3 => 'jquery.ui.tabs.js',
        4 => 'jquery.tools.min.js',
      ),
      'css' => 
      array (
        0 => 'payment.css',
        1 => 'payment-responsive.css',
        2 => 'footer.css',
        3 => 'checkout.css',
      ),
      'min_css' => 'skinapps/css/checkout-payment-7d36ea0e4c2f0e6397ba8dd10780be56.min.css',
      'min_js' => 'skinapps/js/checkout-payment-ea0e590a53605ff44f1eaa24a65e50ab.min.js',
      'mingz_css' => 'skinapps/css/checkout-payment-7d36ea0e4c2f0e6397ba8dd10780be56.css.jgz',
      'mingz_js' => 'skinapps/js/checkout-payment-ea0e590a53605ff44f1eaa24a65e50ab.js.jgz',
      'iscdn_css' => 0,
      'iscdn_js' => 0,
    ),
    'checkout-payment-andriod' => 
    array (
      'css' => 
      array (
        0 => '/skinapps/andriod/css/checkout-payment.css',
      ),
      'js' => 
      array (
      ),
      'min_css' => 'skinapps/css/checkout-payment-andriod-4bd67237d9bd3569860c6d07de7ff2be.min.css',
      'min_js' => '',
      'mingz_css' => 'skinapps/css/checkout-payment-andriod-4bd67237d9bd3569860c6d07de7ff2be.css.jgz',
      'mingz_js' => '',
      'iscdn_css' => 0,
      'iscdn_js' => '',
    ),
    'checkout-payment-iphone' => 
    array (
      'css' => 
      array (
        0 => '/skinapps/iphone/css/checkout-payment.css',
      ),
      'js' => 
      array (
      ),
      'min_css' => 'skinapps/css/checkout-payment-iphone-cd3a9cd67ec4fce75b50ad97a0778d72.min.css',
      'min_js' => '',
      'mingz_css' => 'skinapps/css/checkout-payment-iphone-cd3a9cd67ec4fce75b50ad97a0778d72.css.jgz',
      'mingz_js' => '',
      'iscdn_css' => 0,
      'iscdn_js' => '',
    ),
    'express-buy' => 
    array (
      'css' => 
      array (
        0 => 'checkout.css',
        1 => 'payment.css',
        2 => 'payment-responsive.css',
        3 => 'payment-form.css',
        4 => 'express.css',
        5 => 'address-list.css',
        6 => 'address-form.css',
      ),
      'js' => 
      array (
        0 => 'cc-address-list.js',
        1 => 'address-form.js',
        2 => 'payment-form-validate.js',
        3 => 'payment.js',
        4 => 'checkout.js',
        5 => 'express.js',
      ),
      'min_css' => 'skinapps/css/express-buy-59bb706235442fc3a564526f97638925.min.css',
      'min_js' => 'skinapps/js/express-buy-b9cdda43adb21b6cdd5a0e2effa373ec.min.js',
      'mingz_css' => 'skinapps/css/express-buy-59bb706235442fc3a564526f97638925.css.jgz',
      'mingz_js' => 'skinapps/js/express-buy-b9cdda43adb21b6cdd5a0e2effa373ec.js.jgz',
      'iscdn_css' => 0,
      'iscdn_js' => 0,
    ),
    'confirmation' => 
    array (
      'css' => 
      array (
        0 => 'confirmation.css',
        1 => 'confirmation-responsive.css',
        2 => 'checkout.css',
      ),
      'js' => 
      array (
      ),
      'min_css' => 'skinapps/css/confirmation-a0f16924455090e1f97a5494b96f52e6.min.css',
      'min_js' => '',
      'mingz_css' => 'skinapps/css/confirmation-a0f16924455090e1f97a5494b96f52e6.css.jgz',
      'mingz_js' => '',
      'iscdn_css' => 0,
      'iscdn_js' => '',
    ),
    'checkout-confirmation-andriod' => 
    array (
      'css' => 
      array (
        0 => '/skinapps/andriod/css/checkout-confirmation.css',
      ),
      'js' => 
      array (
      ),
      'min_css' => 'skinapps/css/checkout-confirmation-andriod-5f1d0139f1b5ece45ecd553719e3edb5.min.css',
      'min_js' => '',
      'mingz_css' => 'skinapps/css/checkout-confirmation-andriod-5f1d0139f1b5ece45ecd553719e3edb5.css.jgz',
      'mingz_js' => '',
      'iscdn_css' => 0,
      'iscdn_js' => '',
    ),
    'checkout-confirmation-iphone' => 
    array (
      'css' => 
      array (
        0 => '/skinapps/iphone/css/checkout-confirmation.css',
      ),
      'js' => 
      array (
      ),
      'min_css' => 'skinapps/css/checkout-confirmation-iphone-1dd1d9a2d898c1226c7fed7013644728.min.css',
      'min_js' => '',
      'mingz_css' => 'skinapps/css/checkout-confirmation-iphone-1dd1d9a2d898c1226c7fed7013644728.css.jgz',
      'mingz_js' => '',
      'iscdn_css' => 0,
      'iscdn_js' => '',
    ),
  ),
  'clients' => 
  array (
    'myntraandroid' => 
    array (
      'pages' => 
      array (
        '/mkmycart.php' => 
        array (
          0 => '3pl',
          1 => 'common',
          2 => 'checkout-cart',
          3 => 'checkout-common-andriod',
          4 => 'checkout-cart-andriod',
        ),
        '/mkpaymentoptions.php' => 
        array (
          0 => 'secure-common',
          1 => 'checkout-payment',
          2 => 'checkout-common-andriod',
          3 => 'checkout-payment-andriod',
        ),
        '/checkout-address.php' => 
        array (
          0 => 'secure-common',
          1 => 'checkout-address',
          2 => 'checkout-common-andriod',
          3 => 'checkout-address-andriod',
        ),
        '/confirmation.php' => 
        array (
          0 => 'secure-common',
          1 => 'confirmation',
          2 => 'checkout-common-andriod',
          3 => 'checkout-confirmation-andriod',
        ),
      ),
    ),
    'myntraretailandroid' => 
    array (
      'pages' => 
      array (
        '/mkmycart.php' => 
        array (
          0 => '3pl',
          1 => 'common',
          2 => 'checkout-cart',
          3 => 'checkout-common-andriod',
          4 => 'checkout-cart-andriod',
        ),
        '/mkpaymentoptions.php' => 
        array (
          0 => 'secure-common',
          1 => 'checkout-payment',
          2 => 'checkout-common-andriod',
          3 => 'checkout-payment-andriod',
        ),
        '/checkout-address.php' => 
        array (
          0 => 'secure-common',
          1 => 'checkout-address',
          2 => 'checkout-common-andriod',
          3 => 'checkout-address-andriod',
        ),
        '/confirmation.php' => 
        array (
          0 => 'secure-common',
          1 => 'confirmation',
          2 => 'checkout-common-andriod',
          3 => 'checkout-confirmation-andriod',
        ),
      ),
    ),
    'myntraandroid_mobile_4-5_1-0' => 
    array (
      'pages' => 
      array (
        '/mkmycart.php' => 
        array (
          0 => '3pl',
          1 => 'common',
          2 => 'checkout-cart',
          3 => 'checkout-common-andriod',
          4 => 'checkout-cart-andriod',
        ),
        '/mkpaymentoptions.php' => 
        array (
          0 => 'secure-common',
          1 => 'checkout-payment',
          2 => 'checkout-common-andriod',
          3 => 'checkout-payment-andriod',
        ),
        '/checkout-address.php' => 
        array (
          0 => 'secure-common',
          1 => 'checkout-address',
          2 => 'checkout-common-andriod',
          3 => 'checkout-address-andriod',
        ),
        '/confirmation.php' => 
        array (
          0 => 'secure-common',
          1 => 'confirmation',
          2 => 'checkout-common-andriod',
          3 => 'checkout-confirmation-andriod',
        ),
      ),
    ),
    'myntraretailandroid_mobile_4-5_1-0' => 
    array (
      'pages' => 
      array (
        '/mkmycart.php' => 
        array (
          0 => '3pl',
          1 => 'common',
          2 => 'checkout-cart',
          3 => 'checkout-common-andriod',
          4 => 'checkout-cart-andriod',
        ),
        '/mkpaymentoptions.php' => 
        array (
          0 => 'secure-common',
          1 => 'checkout-payment',
          2 => 'checkout-common-andriod',
          3 => 'checkout-payment-andriod',
        ),
        '/checkout-address.php' => 
        array (
          0 => 'secure-common',
          1 => 'checkout-address',
          2 => 'checkout-common-andriod',
          3 => 'checkout-address-andriod',
        ),
        '/confirmation.php' => 
        array (
          0 => 'secure-common',
          1 => 'confirmation',
          2 => 'checkout-common-andriod',
          3 => 'checkout-confirmation-andriod',
        ),
      ),
    ),
    'myntraiphone' => 
    array (
      'pages' => 
      array (
        '/mkmycart.php' => 
        array (
          0 => '3pl',
          1 => 'common',
          2 => 'checkout-cart',
          3 => 'checkout-common-iphone',
          4 => 'checkout-cart-iphone',
        ),
        '/mkpaymentoptions.php' => 
        array (
          0 => 'secure-common',
          1 => 'checkout-payment',
          2 => 'checkout-common-iphone',
          3 => 'checkout-payment-iphone',
        ),
        '/checkout-address.php' => 
        array (
          0 => 'secure-common',
          1 => 'checkout-address',
          2 => 'checkout-common-iphone',
          3 => 'checkout-address-iphone',
        ),
        '/confirmation.php' => 
        array (
          0 => 'secure-common',
          1 => 'confirmation',
          2 => 'checkout-common-iphone',
          3 => 'checkout-confirmation-iphone',
        ),
      ),
    ),
    'myntraretailiphone' => 
    array (
      'pages' => 
      array (
        '/mkmycart.php' => 
        array (
          0 => '3pl',
          1 => 'common',
          2 => 'checkout-cart',
          3 => 'checkout-common-iphone',
          4 => 'checkout-cart-iphone',
        ),
        '/mkpaymentoptions.php' => 
        array (
          0 => 'secure-common',
          1 => 'checkout-payment',
          2 => 'checkout-common-iphone',
          3 => 'checkout-payment-iphone',
        ),
        '/checkout-address.php' => 
        array (
          0 => 'secure-common',
          1 => 'checkout-address',
          2 => 'checkout-common-iphone',
          3 => 'checkout-address-iphone',
        ),
        '/confirmation.php' => 
        array (
          0 => 'secure-common',
          1 => 'confirmation',
          2 => 'checkout-common-iphone',
          3 => 'checkout-confirmation-iphone',
        ),
      ),
    ),
    'myntrawindowsphone' => 
    array (
      'pages' => 
      array (
        '/mkmycart.php' => 
        array (
          0 => '3pl',
          1 => 'common',
          2 => 'checkout-cart',
        ),
        '/mkpaymentoptions.php' => 
        array (
          0 => 'secure-common',
          1 => 'checkout-payment',
        ),
        '/checkout-address.php' => 
        array (
          0 => 'secure-common',
          1 => 'checkout-address',
        ),
        '/confirmation.php' => 
        array (
          0 => 'secure-common',
          1 => 'confirmation',
        ),
      ),
    ),
    'myntraretailwindowsphone' => 
    array (
      'pages' => 
      array (
        '/mkmycart.php' => 
        array (
          0 => '3pl',
          1 => 'common',
          2 => 'checkout-cart',
        ),
        '/mkpaymentoptions.php' => 
        array (
          0 => 'secure-common',
          1 => 'checkout-payment',
        ),
        '/checkout-address.php' => 
        array (
          0 => 'secure-common',
          1 => 'checkout-address',
        ),
        '/confirmation.php' => 
        array (
          0 => 'secure-common',
          1 => 'confirmation',
        ),
      ),
    ),
  ),
);
}
