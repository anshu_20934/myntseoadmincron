<?php
class RemoteTimeouts {

	public static $paymentCurlTimeout = 30;

	/** Sub-second timeouts do not exist , yet **/
	public static $localDCConnectTimeout = 1;
	
	/** Connecting outside the DC **/
	public static $remoteConnectTimeout = 2;
	
	/** Second timeouts . 100 ms is  still way too big for connect within a single DC **/
	public static $localDCConnectTimeoutMs = 100;
	
	/** 1 second - formattedKeywordGeneration.php **/
	public static $formattedKeywordsTimeout = 1;
	
	public static $solrSchemaXmlTimeout = 5;
}
?>
