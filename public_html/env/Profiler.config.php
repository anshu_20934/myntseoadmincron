<?php
class ProfilerConfig {
	public static $app = "portal";
	public static $statsDHost = "statsd-frontend.myntra.com";
	public static $statsDPort = 8125;
	public static $enabled = True;
	public static $sampleRate = 0.0005;
}
?>
