{if $productsInCart != 0}
	<div>
	<table cellpadding="2" class="printtable" width="95%" border='0'>
	<tr class="trhead">
		<td class="align_left" ><strong><p>Item</p></strong></td>
		<td class="align_left" style="text-align:right;"><strong><p>Unit price (Rs.)</p></strong></td>
		<td class="align_left" style="text-align:right;"><strong><p>Qty</p></strong></td>
		<td class="align_left" style="text-align:right;"><strong><p>Subtotal (Rs.)</p></strong></td>
		
		{if (($posaccounttype eq 'P2' or $posaccounttype eq'SM') && ($smarty.get.t eq "u" or $smarty.get.view eq "OD")) or ($smarty.get.type eq 'admin' && $source_type_id eq 11)}{else}
		<td class="align_left" style="text-align:right;"><strong><p>Shipping (Rs.)</p></strong></td>
		<td class="align_left" style="text-align:right;"><strong><p>Tax Rate*</p></strong></td>
		<td class="align_left" style="text-align:center;"><strong><p>Tax(inclusive)<br>(Rs.)</p></strong></td>
		{/if}
		{if (($posaccounttype eq 'P2' or $posaccounttype eq'SM') && ($smarty.get.t eq "u" or $smarty.get.view eq "OD")) or ($smarty.get.type eq 'admin' && $source_type_id eq 11)}
		<td class="align_left" style="text-align:right;"><strong><p>Design Markup (Rs.)</p></strong></td>
		{/if}
		<td class="align_left" style="text-align:right;"><strong><p>Total (Rs.)</p></strong></td>
	</tr>
	
	{assign var=totalamount value=0}
	{assign var=totalamounttobepaid value=0}
	{assign var=vat value=0}
	{*assign var=shippingRate value=0*}
	{assign var=discount value=0}
        {section name=idx loop=$productsInCart}
	
          {assign var='productTypeLabel' value=$productsInCart[idx].productTypeLabel}
          {assign var='productStyleName' value=$productsInCart[idx].productStyleName}
          {assign var='productStyleName' value=$productsInCart[idx].productStyleName}
          {assign var='productStyleType' value=$productsInCart[idx].productStyleType}
          {assign var='allCustImagePath' value=$productsInCart[idx].allCustImagePath}
          {assign var='product_style' value=$productsInCart[idx].product_style}
	  {assign var='quantity_breakup' value=$productsInCart[idx].quantity_breakup}
          {assign var='optionValues' value=$productsInCart[idx].optionValues}
          {assign var='optionNames' value=$productsInCart[idx].optionNames}
          {assign var='productPrice' value=$productsInCart[idx].price}
       	  {assign var='totalPrice' value=$productsInCart[idx].totalPrice}
	  {assign var='productDiscount' value=$productsInCart[idx].productDiscount}
	  {assign var='shipping_amount' value=$productsInCart[idx].shipping_amount}			  
	  {assign var='shipping_tax' value=$productsInCart[idx].shipping_tax}
	  {assign var='net_shipping' value=$productsInCart[idx].net_shipping}
	  {assign var='tax_rate' value=$productsInCart[idx].tax_rate}
	  {assign var='taxamount' value=$productsInCart[idx].taxamount}
	  {assign var='total_amount' value=$productsInCart[idx].total_amount}
	  {assign var='promotion_offer' value=$productsInCart[idx].promotion}
	  {assign var='tot_qty' value=$productsInCart[idx].amount}
	  {assign var='coupon_discount' value=$productsInCart[idx].coupon_discount}

	   {math equation=(x+y) x=$shipping_amount y=$shipping_tax assign=shipping}	
	   
	   {if $reebok_invoice ne ''}
   	  	{assign var='jersey_text' value=$productsInCart[idx].jersey_text}
   	  	{assign var='jersey_number' value=$productsInCart[idx].jersey_number}
   	  {/if}
	  <tr class="trbody">
		<td class="align_left" >
			<p>{$productStyleName}
			    {if $promotion_offer.promotional_offer}<br/><span style='color:#FF0000;'>{$promotion_offer.promotional_offer} {/if}
			    {if $reebok_invoice ne ''}
			    	<br/><span style='color:#000000;'>Name:{$jersey_text}</span>
			    	<br/><span style='color:#000000;'>Number:{$jersey_number}</span>
			    {/if}
			</p>
		</td>
		<td class="align_left" style="text-align:right;"><p>{$productPrice|string_format:"%.2f"} {if $product_style==701 || $product_style==702 ||$product_style==703}(Rs.{$productPrice-399|string_format:"%.2f"} + Rs.399/- customization charge){/if}</p></td>
		<td class="align_left" style="text-align:right;"><p>{if $quantity_breakup}{$quantity_breakup}{else}{$tot_qty}{/if}</p></td>
		<td class="align_left" style="text-align:right;"><p>{$totalPrice|string_format:"%.2f"}</p></td>

		{if (($posaccounttype eq 'P2' or $posaccounttype eq'SM') && ($smarty.get.t eq "u" or $smarty.get.view eq "OD")) or ($smarty.get.type eq 'admin' && $source_type_id eq 11)}{else}
		<td class="align_left" style="text-align:right;"><p>{$net_shipping|string_format:"%.2f"}</p></td>
		<td class="align_left" style="text-align:right;"><p>{$tax_rate}</p></td>
		<td class="align_left" style="text-align:right;"><p>{$taxamount|string_format:"%.2f"}</p></td>
		{/if}
		{if (($posaccounttype eq 'P2' or $posaccounttype eq'SM') && ($smarty.get.t eq "u" or $smarty.get.view eq "OD")) or ($smarty.get.type eq 'admin' && $source_type_id eq 11)}
		<td class="align_left" style="text-align:right;"><p>{math equation="x-y" x=$total_amount y=$totalPrice format="%.2f"}</p></td>
		{/if}
		<td class="align_left" style="text-align:right;"><p>{$total_amount-$productDiscount+$net_shipping|string_format:"%.2f"}</p></td>
	</tr>
	{assign var=discount value=$discount+$productDiscount}
	{*assign var=shippingRate value=$shippingRate+$shipping*}
	{assign var=vat value=$vat+$taxamount}
	{assign var=totalamount value=$totalamount+$totalPrice}	
	{assign var=totalamounttobepaid value=$totalamounttobepaid+$total_amount+$net_shipping}
	{/section}
	{if $ref_discount != 0.00}
	<tr class="trbody">
		<td class="align_left" colspan="7"><strong><p>Referral discount</p></strong></td>
		<td class="align_left" style="text-align:right;">&nbsp;Rs.{$ref_discount|string_format:"%.2f"}</td>
	</tr>
	{math equation="x - y" y=$ref_discount x=$totalamounttobepaid assign ="totalamounttobepaid"}
	 {/if}

	{if $gift_status == 'Y'}
	<tr class="trbody">
		<td class="align_left"{if (($posaccounttype eq 'P2' or $posaccounttype eq'SM') && ($smarty.get.t eq "u" or $smarty.get.view eq "OD")) or ($smarty.get.type eq 'admin' && $source_type_id eq 11)} colspan="6" {else}		colspan="7"{/if}><strong><p>Gift pack charges</p></strong></td>
		<td class="align_left" style="text-align:right;"><p>20.00</p></td>
	</tr>
	{assign var=totalamounttobepaid value=$totalamounttobepaid+20}
	{/if}
    {if $cod_charges != "0"}
	<tr class="trbody">
		<td class="align_left" colspan="7"><strong><p>COD charges</p></strong></td>
		<td class="align_left" style="text-align:right;"><p>{$cod_charges|string_format:"%.2f"}</p></td>
	</tr>
	{if $emi_charge != "0"}
	<tr class="trbody">
		<td class="align_left" colspan="7"><strong><p>EMI charges</p></strong></td>
		<td class="align_left" style="text-align:right;"><p>{$emi_charge|string_format:"%.2f"}</p></td>
	</tr>
	{assign var=totalamounttobepaid value=$totalamounttobepaid+$cod_charges+$emi_charge}
	{/if}
	{if $coupon_discount > 0}
	    <tr class="trbody">
		<td class="align_left" colspan="7"><strong><p>Discount</p></strong></td>
		<td class="align_left" style="text-align:right;"><p>{$coupon_discount|string_format:"%.2f"}</p></td>
		{assign var=totalamounttobepaid value=$totalamounttobepaid-$coupon_discount}
	</tr>
	{/if}
	{if $cashdiscount > 0}
	    <tr class="trbody">
		<td class="align_left" colspan="7"><strong><p>Mynt Credits</p></strong></td>
		<td class="align_left" style="text-align:right;"><p>{$cashdiscount|round|string_format:"%.2f"}</p></td>
		{assign var=totalamounttobepaid value=$totalamounttobepaid-$cashdiscount}
    	</tr>
	{/if}
	{if $pg_discount > 0}
    <tr class="trbody">
		<td class="align_left" colspan="7"><strong><p>Payment Discount</p></strong></td>
		<td class="align_left" style="text-align:right;"><p>{$pg_discount|round|string_format:"%.2f"}</p></td>
		{assign var=totalamounttobepaid value=$totalamounttobepaid-$pg_discount}
    </tr>
	{/if}
	<tr class="trbody">
		<td class="align_left" colspan="3"><strong><p>Total</p></strong></td>
		<td class="align_left" style="text-align:right;"><p>{$totalamount|string_format:"%.2f"}</p></td>
		
		{if (($posaccounttype eq 'P2' or $posaccounttype eq'SM') && ($smarty.get.t eq "u" or $smarty.get.view eq "OD")) or ($smarty.get.type eq 'admin' && $source_type_id eq 11)}{else}
		<td class="align_left" style="text-align:right;"><p>{$shippingRate}</p></td>
		<td class="align_left" style="text-align:right;"><p>&nbsp;</p></td>
		<td class="align_left" style="text-align:right;"><p>{$vat|string_format:"%.2f"}</p></td>
		{/if}
		{if (($posaccounttype eq 'P2' or $posaccounttype eq'SM') && ($smarty.get.t eq "u" or $smarty.get.view eq "OD")) or ($smarty.get.type eq 'admin' && $source_type_id eq 11)}
		<td></td>
		{/if}
		<td class="align_left" style="text-align:right;"><p>{$totalamounttobepaid|string_format:"%.2f"}</p></td>
	</tr>
    </table>
        </div>
     {/if}
