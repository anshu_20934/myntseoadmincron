<html>
<head>
{literal}


<script Language="JavaScript" Type="text/javascript">
		function FrontPage_Form1_Validator(theform)
		{
		  return true;		  
		}
	function popupNEW(fnam){
	  var element = document.getElementById(fnam);
	  var calname = "cal"+fnam;
	  var calname = new calendar2(element);
      calname.year_scroll = true;
      calname.time_comp = false;
	  calname.popup();
	}
	function setexport(type){
		
			document.exportform.exporttype.value=type;
			document.exportform.submit();
		
	}
</script>
{/literal}

<link rel="stylesheet" href="../skin1/mykriti.css" />

</head>
<body id="body">
<div id="scroll_wrapper">
<div id="container">
<div id="display">
<div class="center_column">
<div class="print">


<div class="super">
<p>&nbsp;</p>
</div>

{if $mailsent ne "Y"}

<div class="head" style="padding-left: 15px;">
<p>&nbsp;&nbsp;{$reportname}</p>
</div>
<div class="links">
<p></p>
</div>
<div class="foot"></div>


{include file="main/include_js.tpl" src="main/calendar2.js"}
<form method="POST" name="reportviewform" action="view_report.php">
<input type="hidden" name="mode" value="true" />
<input type="hidden" name="reportid" value="{$reportid}">
<table cellpadding="3" cellspacing="1" width="100%">
<tr>
<td>{$size}</td>
<td>{$cond1}</td>
<td>{$cond2}</td>
<td>{$cond3}</td>
</tr>
	{foreach from=$filters item=filter}
	<tr>
		<td><b>{$filter[0]}<input type="hidden" name="fname[]" value="{$filter[3]}">
		<input type="hidden" name="ftype[]" value="{$filter[1]}"></b></td>
		<td>
		{if $filter[1] eq "DAT"} <input type='text' name="{$filter[3]}" id="{$filter[3]}" value="">&nbsp;<p style="margin-left:135px;margin-top:-19px;"><a
			href='javascript:popupNEW("{$filter[3]}");'><img
			src="../images/cal.gif" width="16" height="16" border="0"
			alt="Click Here to Pick up the date"></a></p>
		{elseif $filter[1] eq "SPL"}
		<select name="{$filter[3]}">
			{foreach from=$filter[2] key=opid item=opname}
			<option value="{$opname}:{$opid}">{$opname}</option>
			{/foreach}
		</select>
		{elseif $filter[1] eq "DPL"}
		<select name="{$filter[3]}">
			{foreach from=$filter[2] key=opid item=opname}
			<option value="{$opname}:{$opid}">{$opname}</option>
			{/foreach}
		</select>
		{else}
		<input type='text' name="{$filter[3]}">
		{/if}
		</td>
	</tr>
	{/foreach}
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
{if !$reportresult }

	<tr>
		<td colspan="2"><input type="submit" value="Run Report" /></td>
	</tr>

	{/if}
</table>
</form>
{if $reportresult }

<form method="post" name="exportform" action="view_report.php">
	<input type="hidden" name="export" value="true">
	<input type="hidden" name="report_name" value="{$reportname}">
	<input type="hidden" name="mailto" value="{$mailto}">
	<input type="hidden" name="exporttype" value="">
<table cellpadding="3" cellspacing="1" width="100%">
<tr>

		<td><input type="button" value="Send By Email" style="background-color:#953735; color:white; width:200px;" onclick="javascript:setexport('mail');"></td>
		<td><input type="button" value="Export to XLS" style="background-color:#953735; color:white; width:200px;" onclick="javascript:setexport('xls');"></td>
		<td><input type="button" value="Export to PDF" style="background-color:#953735; color:white; width:200px;" onclick="javascript:setexport('pdf');"></td>
</tr>
</table>

</form> 
<table cellpadding="3" cellspacing="1" width="100%">
	<tr style="background-color:#4f81bd; color:white;">
	{foreach from=$coloums item=item2}
				<td style="text-align: center;">{$item2}</td>
	{/foreach}
	</tr>
	{foreach from=$reportresult item=reportrow} 
	<tr style="background-color: {cycle values="#d0d8e8,#e9edf4"};">
		{foreach from=$reportrow key=key item=item}
				<td style="text-align: center;">{$item}</td>
		{/foreach}
	</tr>
	{/foreach}
	<tr>
		<td align="center"><input type="button" value="reset" onclick="javascript:window.location='view_report.php?reportid={$reportid}';"></td>
	</tr>
</table>
{/if}
{elseif $mailsent eq "Y"}
		<table>
				<tr>
					<td>{$mailmsg}</td>
					<td>&nbsp;&nbsp;</td>
					<td>&nbsp;&nbsp;</td>
				</tr>
				<tr>
					<td align="center"><input type="button" style="height:20px;width:50px; background-color:black; color:white;" value="Close" onclick="self.close()"></td>
				</tr>
		</table>
		{/if}

</div>
</div>
</div>
</div>
</div>
</body>
</html>

