<form id="nikeform" action="{$http_location}/modules/affiliates/reebok_ipl.php?leadsource=fbapp_widget" method="POST" class="clearfix" target="_blank">
    <input type="hidden" name="styleid" id="style_id" value="{$default_style}">
    <input type="hidden" name="styleprice" id="style_price" value="{$default_price}">
    <input type="hidden" name="personalize" value="on">
    <div class="top_strip clearfix" style="margin-top: 16px;">
		            <div class="product-gallery-head left">
						<div class="left prepend"><span style="font-weight: bold;">{$first_name},</span><span style="font-size: 18px;">&nbsp;&nbsp;Your Name Makes a Difference</span></div>
					</div>
					<div class="type-filter right">
							<a class="invite-more-friends" onClick="topPublish(false,'top');"> <span class="uiButton">Post To Wall</span> </a>
              		</div>
    </div>
	<div class="product-gallery-container clearfix">
			<div class="product-gallery-content clearfix left">
				<div class="product-gallery span-19 left">
					<div class="product-image-tabs blue-bg corners left">
                        {section name=jersey loop=$jerseys}
						    <div>
						        <img class="product-image" src="{$jerseys[jersey].display_image}" alt="Team India Jersey - {$jerseys[jersey].type}">
						        <img class="customize-product-image" src="{$jerseys[jersey].image}" alt="Team India Jersey - {$jerseys[jersey].type}">
						        <select class="customize-product-image" id="select_list_{$jerseys[jersey].style_id}">{$jerseys[jersey].sizes}</select>
						    </div>
						{/section}
					</div>
					<div class="vertical-scroller left clearfix">
						<!-- "previous page" action -->
						<span class="browse-btn"><a class="prev up left"></a></span>
						<div class="vertical-scrollable left">
							<div class="vertical-tabs vertical-items left">
                                    {section name=jersey loop=$jerseys}
                                        <div><a id="tab_{$jerseys[jersey].style_id}" class="first" href="#" onclick="jersey_preview('{$jerseys[jersey].image}','{$jerseys[jersey].price}', '{$jerseys[jersey].style_id}')"><img class="corners" src="{$jerseys[jersey].thumb}" alt="Image Not Available"></a></div>
                                    {/section}
						    </div>
						</div>
						<!-- "next page" action -->
						<span class="browse-btn"><a class="next down left"></a></span>
					</div>
				</div>
			</div>
			<div class="product-details left clearfix">
				<div class="product-details-block corners black-bg5 clearfix" style="height:100px">
					<div class="details clearfix">
						<div class="choose text">
							<label>Name</label>
							<input type="text" name="u_name" id="preview-name" value="Enter a name" class="gallery-block-input toggle-text" maxlength="8" tabindex="5" onChange="productPreview();">
						</div>
						<div class="choose number" >
							<label>Number</label>
							<input type="text" name="u_number" id="preview-number" value="Enter a number" class="gallery-block-input toggle-text" style="display:hidden();" maxlength="2" tabindex="6" onChange="productPreview();">
						</div>
						<div style="margin-left: 75px;margin-top: 10px;margin-bottom: 5px;">
							<a class="invite-more-friends" onClick="topPublish(false,'middle');"> <span class="uiButton">Post To Wall</span> </a>
						</div>
					</div>
					<div class="notes clearfix">
						Support Team India.<br/> Post to your wall and<br/> let your friends know<br/> that your name<br/> makes a difference.
					</div>
				</div>
				<div class="product-details-main clearfix">
					<div class="product-details-main-left left">
						<div class="choose size">
							<label>Size</label>
							<select name="size" id="size_list" class="filter-select filter-size">
                                   {$jerseys[0].sizes}
    						</select>
						</div>
						<div class="choose quantity">
							<label>Quantity</label>
							<select name="qty" class="filter-select filter-quantity">
	    						<option value="1">1</option>
	  							<option value="2">2</option>
	  							<option value="3">3</option>
	    					</select>
						</div>
						<span class="price"><span class="currency">RS</span><span class="product_price">{$default_price}</span></span>
						
						<input type="button" value="Buy Now" class="buy-now corners">						
                                                <div class="user_name"></div>
                                                <div class="user_number"></div>
						
					</div>
					<div class="product-details-main-right left">
						<a class="sizing-chart no-decoration-link" onclick="$('#size-chart-div').slideDown();"> Sizing Chart</a>
						<div class="size-chart right" id="size-chart-div" style="display:none;">
							<div class="sizechart-title"><b>Size Chart</b> (In Centimeters) - for chest</div>
							<div class="close-btn">
								{literal}
								<a onclick="$('#size-chart-div').slideUp();" style="cursor:pointer">
								{/literal}
									<img src="{$cdn_base}/skin1/affiliatetemplates/images/reebok_ipl/close-1.png" alt="close">
								</a>
							</div>
							<div class="sizes">
								<div class="adults">
									<div>ADULTS</div>
									<ul>
										<li style="width:35px"><span class="size-type">XS</span> <span class="size-number">84</span></li>
										<li style="width:35px"><span class="size-type">S</span> <span class="size-number">92</span></li>
										<li style="width:35px"><span class="size-type">M</span> <span class="size-number">100</span></li>
										<li style="width:35px"><span class="size-type">L</span> <span class="size-number">108</span></li>
										<li style="width:35px"><span class="size-type">XL</span> <span class="size-number">116</span></li>
										<li style="width:35px"><span class="size-type">XXL</span> <span class="size-number">124</span></li>
									</ul>
								</div>
							</div>
						</div>
                        <span class="notes">Free Shipping/Cash on Delivery in India</span>
						<span class="notes">International shipping available</span>
					</div>
				</div>
			</div>
		</div>
</form>