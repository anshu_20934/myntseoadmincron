<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN"
        "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en" xmlns:fb="http://www.facebook.com/2008/fbml">
	<head>
		<link rel="stylesheet" href="{$http_location}/skin1/css/jersey_widget.css" type="text/css">
		<link href="{$combine_css_path}&f=thickbox3.css" rel="stylesheet" type="text/css"/>
		{literal}
		<style type="text/css">
		
		.no-decoration-link {
			cursor: pointer;
		}
		
		.ulFriends-list {
			list-style-type: none;
			margin: 0;
			padding: 0;
		}
		
		.uiFriends-list {
			border-width: 1px 0 0;
			border-color: #E9E9E9;
			border-style: solid;
			display:block;
			margin-top: 10px;
			padding-top: 10px;
		}
		
		.uiFriends-list:first-child {
			border-width: 0;
			padding-top: 0;
		}
		
		.product-gallery-container {
			width: 745px;
		}
		
		.product-gallery-content {
			width: 345px;
		}
		
		.product-gallery {
			width: 345px;
		}
		
		.canvas_iframe_util {
			height: 1200px;
		}
		
		.product-gallery-head {
			font-family: arial;
			font-size: 20px;
		}
		
		.product-details {
			margin: 0 0 0 0;
			width: 400px;
		}
		
		.dialog-text {
			margin:0px auto;
			text-align:center;
		}
		
		.top_strip {
			background: url("http://myntra.myntassets.com/skin1/myntra_images/line.png") no-repeat scroll 0 33px transparent;
			margin-bottom: 15px;
		}
		
		.bottom_strip {
			background: url("http://myntra.myntassets.com/skin1/myntra_images/line.png") no-repeat scroll 0 0px transparent;
			margin-bottom: 15px;
			margin-top: 15px;
			height: 2px;
		}
		.dialog-strip {
		    background: url("http://myntra.myntassets.com/skin1/myntra_images/line.png") no-repeat scroll 0 0 transparent;
			height: 2px;
			margin-top: 8px;
			width: 300px;
		}
		.size-chart {
		    background-color: white;
			border: 0px solid red;
			margin-left: -130px;
			margin-top: -86px;
			width: 281px;
		}
		
		#size-chart-div {
			display: none;
			float: left;
			position: absolute;
			z-index: 10;
		}

		.ui-widget-header {
			background: none repeat-x scroll 50% 50% #00FF00;
			border: 1px solid #00FF00;
			color: #FFFFFF;
			font-weight: bold;
		}
		
		.invite-more-friends {
			-moz-box-shadow: 0 1px 0 rgba(0, 0, 0, 0.05);
			background: none repeat scroll 0 0 #4F6AA3;			
			border-bottom-color: #29447E;
			background-color: #5B74A8;
			background-position: 0 -48px;
			border-color: #29447E #29447E #1A356E;
			border-style: solid;
			border-width: 1px;
			color: #3B5998;
			cursor: pointer;
		    font-size: 15px;
			font-weight: bold;
			line-height: normal !important;
			padding-top: 4px;
			padding-bottom: 4px;
			padding-left:	10px;
			padding-right: 10px;
			text-align: center;
			text-decoration: none;
			vertical-align: top;
			white-space: nowrap;
			float: left;
		}
		
		.skip-button {
			-moz-box-shadow: 0 1px 0 rgba(0, 0, 0, 0.05);
			background: none repeat scroll 0 -48px #C0C0C0;
			border-color: #969696 #808080 #808080;
			border-style: solid;
			border-width: 1px;
			color: #3B5998;
			cursor: pointer;
			font-size: 15px;
			line-height: normal !important;
			padding-top: 4px;
			padding-bottom: 4px;
			padding-left:	10px;
			padding-right: 10px;
			text-align: center;
			text-decoration: none;
			vertical-align: top;
			white-space: nowrap;
		}
		
		body {
		font-family: "lucida grande",tahoma,verdana,arial,sans-serif;
		}
		.uiButton{
			background: none repeat scroll 0 0 transparent;
			color: #FFFFFF;
			border: 0 none;
			display: inline-block;
			font-family: 'Lucida Grande',Tahoma,Verdana,Arial,sans-serif;
			font-size: 15px;
			font-weight: bold;
			margin: 0;
			outline: medium none;
			white-space: nowrap;
			border-style: solid;
			cursor: pointer;
			line-height: normal !important;
			text-align: center;
			text-decoration: none;
		}
		
		</style>
		
		<style type="text/css">

		/* New skin styles */
		.orange-bg,#main-menu li a:hover{background:#fd9b06;color:#FFFFFF;}
		.black-bg4{background:#A8A8A8}

		/* Rounded corners*/
		.corners, #main-menu li ul li a:hover{
			-moz-border-radius: 5px !important;
			-webkit-border-radius: 5px !important;
			-khtml-border-radius: 5px !important;
			border-radius: 5px;
		}

		.c-tl, #main-menu li.first a:hover{
			-moz-border-radius: 5px 0px 0px 0 !important;
			-webkit-border-radius: 5px 0px 0px 0 !important;
			-khtml-border-radius: 5px 0px 0px 0 !important;
			border-radius: 5px 0px 0px 0 !important;
		}
		.c-tr, #main-menu li.last a:hover{
			-moz-border-radius: 0px 5px 0px 0 !important;
			-webkit-border-radius: 0px 5px 0px 0 !important;
			-khtml-border-radius: 0px 5px 0px 0 !important;
			border-radius: 0px 5px 0px 0 !important;
		}

		/* Menu */

		#main-menu{margin:0px;padding:0px;float:left;list-style:none outside none;font-size:75%;color:#222;background:#fff;font-family: arial, serif;, Arial, Helvetica, sans-serif;}
		#main-menu li{background-color: #676767;border-right: 1px solid #8a8a8a;float:left;text-align:center; position:relative;}
		#main-menu li a{color: #FFFFFF;display: block;font-size:1em;line-height: 18px;_line-height: 15px;text-decoration: none;padding: 10px 17px;}
		#main-menu li.first{-moz-border-radius:5px 0px 0px 5px; -webkit-border-radius:5px 0px 0px 5px; -khtml-border-radius:5px 0px 0px 5px;}
		#main-menu li.first ul li a:hover{-moz-border-radius:5px !important; -webkit-border-radius:5px !important; -khtml-border-radius:5px !important;}
		#main-menu li.last{-moz-border-radius:0 5px 5px 0; -webkit-border-radius:0 5px 5px 0; -khtml-border-radius:0 5px 5px 0;border-right: 0 none; right:0; position: absolute;}
		#main-menu li.last a:hover{-moz-border-radius:0 5px 5px 0; -webkit-border-radius:0 5px 5px 0; -khtml-border-radius:0 5px 5px 0;border-right: 0 none;}
		#main-menu li.last a:focus{color:yellow;}
		#main-menu li.st1{background:#8d8d8d;}
		#main-menu li.st2{background:#a8a8a8;}
		#main-menu li.st2 a{width:35px;}
		#main-menu li.last a{padding:10px 22px;}
		#main-menu li.last .row ul li a{padding: 1px 5px;width:auto;}
		.nav-menu{position:relative;margin-bottom: 10px;width:750px;}
		
		</style>


		{/literal}
		<title>
			Sample FB
		</title>
	</head>
	<body style="overflow: hidden;">
		<div id="fb-root"></div>
		<img id="myntra_logo" src="{$cdn_base}/skin1/myntra_images/logo.png"></img>
		<div class="nav-menu corners black-bg4 clearfix">
			<ul id="main-menu">
				<li class="first main-menu-item">
					<a href="http://www.myntra.com/cricket-jerseys" target="_blank">Jerseys</a>
				</li>
				<li class="main-menu-item">
					<a href="http://www.myntra.com/sportswear" target="_blank">Sportswear</a>
				</li>
				<li class="main-menu-item">
					<a href="http://www.myntra.com/clothing" target="_blank">Clothing</a>
				</li>
				<li class="main-menu-item">
					<a href="http://www.myntra.com/footwear" target="_blank">Footwear</a>
				</li>
				<li class="main-menu-item">
					<a href="http://www.myntra.com/t-shirts" target="_blank">T-Shirts</a>
				</li>
				<li class="main-menu-item">
					<a href="http://www.myntra.com/accessories" target="_blank">Accessories</a>
				</li>
				<li class="st1 main-menu-item">
					<a href="http://www.myntra.com/brands" target="_blank">Brands</a>
				</li>
				<li class="st2 main-menu-item">
					<a href="http://www.myntra.com/mens-wear" target="_blank">Men</a>
				</li>
				<li class="last st2 main-menu-item">
					<a href="http://www.myntra.com/womens-wear" target="_blank">Women</a>
				</li>
			</ul>
		</div>
		{include file="fb_app/facebook_jersey_widget.tpl" }
		<div class="clearfix" style="margin-top: 30px;">
			<div class="left">
				<div class="top_strip clearfix" style="margin-top: 10px;">
							<div class="product-gallery-head left">
								<div class="left title" style="font-size:18px;font-weight:lighter;" >These names make a difference</div>
							</div>
				</div>
				<div class="clearfix">
					<ul class="ulFriends-list clearfix">
							{foreach from=$usersData key=k item=v}
								<li class="uiFriends-list clearfix" style="clear:both;">
									
										<div class="left"><fb:profile-pic uid="{$v.uid}" linked="false" size="square"></fb:profile-pic></div>
										<span style="color: #666666;font-size: 14px;float:left; padding-left: 16px; padding-right: 16px;padding-top:2px;">
											<fb:name uid="{$v.uid}" linked="false" ></fb:name>
											<div style="padding-top: 14px;">{$v.name}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{$v.number}</div>
										</span>
									
								</li>	
							{/foreach}
					</ul>
				</div>
			</div>
			<div class="right">
				<div class="top_strip clearfix" style="margin-top: 10px;width:400px">
							<div class="product-gallery-head left">
								<div class="left title" style="font-size:18px;font-weight:lighter;margin:0px;">Invite friends</div>
							</div>
				</div>
				<div class="clearfix" style="display:block; margin-top: 40px;" >
					<div style="margin-top: -20px;float:left;">
						<img src="{$cdn_base}/skin1/images/fb_app/Ferrari_Jersey.png" height="120px" style="padding-right:30px"></img>
					</div>
					<div style="margin-top: -10px;">
						<span class="notes" style="margin-left:0px;font-size: 14px;">
							If you invite 10 or more friends you
						</span>
						<span class="notes" style="margin-left:0px;font-size: 14px;">
							may win a ferrari Jacket
						</span>
						<div style="margin-top: 20px;">
							<a class="invite-more-friends" onClick="inviteMoreFriends();"> <span class="uiButton">Invite a few friends</span> </a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="bottom_strip clearfix" style="clear:both;"></div>
		<div class="clearfix footer" style="padding-left: 170px;clear:both;">
			<a class="no-decoration-link" href="{$http_location}" target="_blank">Myntra</a><span>|</span>
			<a class="no-decoration-link" href="{$http_location}/fb_app/contestRules.php" target="_blank">Participation Terms</a><span>|</span>
			<a class="no-decoration-link" href="{$http_location}/termandcondition.php" target="_blank">Terms of use</a><span>|</span>
			<a class="no-decoration-link" href="{$http_location}/privacy_policy.php" target="_blank">Privacy Policy</a>
		</div>
		<div id="fbDialogHiddenContent" style="display:none">
			<h2 class="dialog-text">Did you Know?</h2>
			<div class="dialog-strip dialog-text" >	</div>
			<p class="dialog-text">Your friends who come in through your wall posts<br />also help you inch towards the Ferrari Jacket.</p>
			<p class="dialog-text">Tell your friends that your name makes a difference!</p>
			<p class="dialog-text">
				<a class="invite-more-friends" onClick="topPublish(true,'dialog');" style="float: none;"> 
					<span class="uiButton">Post To Wall</span> 
				</a>
				<a class="skip-button" style=" margin-left: 10px;" onClick="tb_remove();"> 
					<span class="uiButton" style="font-weight: normal;">Skip</span> 
				</a>
			</p>
		</div>
		<!--img id="bottom_invite_friends" src="{$http_location}/skin1/images/fb_app/bottom-banner.jpg" width="740px" style="cursor: pointer;margin: -30px 0 0;"></img-->
		<!--<a href="javascript:void(0);" id="jsSubmitForm" onClick="jsSubmitForm()" style="display:none;">Buy!</a>-->
		<script type="text/javascript" src="http://connect.facebook.net/en_US/all.js"></script>
		<script type="text/javascript" src="{$http_location}/skin1/myntra_js/jquery.min.js"></script>
		<script type="text/javascript" src="{$http_location}/skin1/myntra_js/jquery.tools.min.js"></script>
		<script type="text/javascript" src="{$SkinDir}/js_script/thickbox3.js"></script>
		{literal}
		<script>
		
			function jsSubmitForm(){
				if(validateJerseyForm()) {
					$('#nikeform').submit();
				}
			}
			
			function inviteMoreFriends(){
				_gaq.push(['_trackEvent', 'facebookContest', 'click', 'user_inviting_friends']);
				FB.ui(
						{
							method: 'apprequests', 
							message: 'Sport a Team India Jersey this Cricket worldcup, hail them to victory, and stand to win cool prizes. Your name makes a difference!', 
							filters: ['app_non_users'],
							title: 'Your Name Makes a Difference'
						},
						function(response) {
							if (response && response.request_ids) {
								_gaq.push(['_trackEvent', 'facebookContest', 'click', 'user_invited_friends']);
								{/literal}{if $showMePublish}{literal}
									tb_show("","#TB_inline?height=195&amp;width=300&amp;inlineId=fbDialogHiddenContent&amp;modal=true","");
									$('#TB_ajaxContent').attr('style', "height:auto");
								{/literal}{/if}{literal}
							} else {
								_gaq.push(['_trackEvent', 'facebookContest', 'click', 'user_cancelled_inviting_friends']);
							}
						}
					);			
			}
			
			$("#invite_more_friends").click(function(){
				inviteMoreFriends();
			});
			
			$("#bottom_invite_friends").click(function(){
				inviteMoreFriends();
			});
			
			function productPreview(){
					if(validateJerseyForm()){
						$('div.vertical-tabs').data('tabs').getCurrentTab().trigger('click');
					}
			}
			
			function topPublish(reload,position) {
				_gaq.push(['_trackEvent', 'facebookContest', 'click', 'user_posting_to_wall_'+position]);
				
				if(reload)
					tb_remove();
				FB.ui(
						{
							method: 'feed',
							name: '{/literal}{$first_name}\'s{literal} name makes a difference!',
							link: 'http://apps.facebook.com/{/literal}{$app_name}{literal}/?sender={/literal}{$fb_uid}{literal}',
							picture: '{/literal}{$http_location}{literal}/skin1/images/fb_app/Tshirt_Combo.png',
							caption: 'Come, Make a difference with your name too!',
							description: ' {/literal}{$first_name}{literal} chose to support Team India by sporting a Team India Jersey with their name and favorite number printed on it. Buy your Team India jersey from Myntra and you can cheer in blue too! You could even win prizes!'
						},
						function(response) {
							if (response && response.post_id) {
								//published
								_gaq.push(['_trackEvent', 'facebookContest', 'click', 'user_posted_to_wall_'+position]);
								var enteredName='';
								if ($('#preview-name').val() != 'Enter a name'){
									enteredName = $('#preview-name').val();
								}
								
								var enteredNumber='';
								if ($('#preview-number').val() != 'Enter a number'){
									enteredNumber = $('#preview-number').val()
								}
								
								$.post("{/literal}{$http_location}{literal}/fb_app/index.php", { fb_uid: "{/literal}{$fb_uid}{literal}",name:enteredName,number:enteredNumber},function(data){
									if(reload){
										window.location.href = window.location.href;
									}
								} );
							} else {
								// not published
								_gaq.push(['_trackEvent', 'facebookContest', 'click', 'user_skipped_posting_to_wall_'+position]);
							}
						}
					);
			}
			
			$(".buy-now").click(function(){
				if(validateJerseyForm()) {
					_gaq.push(['_trackEvent', 'facebookContest', 'click', 'buy_now']);
					$('#nikeform').submit();
				}			
			});
			
			//Homepage Jersey Widget
			$(document).ready(function() {
				/* Scrollable */
				$(".vertical-scrollable").scrollable({size:3,vertical:true,keyboard:false,items:".vertical-items",clickable:false});

				//Fix for vertical scrollable to disable next button on initial load if the number of items is exactly equal to the size of the window
				$(".vertical-scroller").each(function() {
					if ($(".vertical-scrollable .vertical-items div", this).length <= 3) {
						$(".next", this).addClass("disabled");
						$(this).addClass("scroller-disabled");
						$(".product-image-tabs").removeClass("corners");
						$(".product-image-tabs").addClass("corners-tl-o-bl-br");
					}
				});

				$(".vertical-scroller.scroller-disabled .vertical-items div a").click(function() {
					if ($(this).hasClass("first")) {
						$(".product-image-tabs").removeClass("corners");
						$(".product-image-tabs").addClass("corners-tl-o-bl-br");
					}
					else {
						$(".product-image-tabs").removeClass("corners-tl-o-bl-br");
						$(".product-image-tabs").addClass("corners");
					}
				});

				/* Product Description Page Gallery */
				$("div.vertical-tabs").tabs("div.product-image-tabs > div");


				/* Change T shirt Type */
				$("#tshirt-type").change(function() {        
					$("#tab_" + $(this).val()).trigger("click");
				});
				
				var toggle_text_elements = {};
				
				
				/* Toggle Text */
				toggle_text_all(".toggle-text");
				$('#preview-number').focus();
				$('#preview-number').val('10');
				{/literal}{if $fb_firstName neq ''}{literal}
					$('#preview-name').focus();
					$('#preview-name').val('{/literal}{$fb_firstName}{literal}');
					$('#preview-number').focus();
					$('#preview-number').val('10');
					productPreview();
				{/literal}{/if}{literal}
				/*
				 * This function finds all HTML elements with "toggle-text" class and binds the "focus"/"blur" events for
				 * toggling the default text in those elements.
				 */
				function toggle_text_all(selector) {
					$(selector).each(function(e) {
						toggle_text_elements[this.id] = $(this).val();
						addToggleText(this);
					});
				}

				function addToggleText(elem) {
					$(elem).addClass("blurred");
					$(elem).focus(function() {
						if ($(this).val() == toggle_text_elements[this.id]) {
							$(this).val("");
							$(this).removeClass("blurred");
						}
					}).blur(function() {
						if ($(this).val() == "") {
							$(this).addClass("blurred");
							$(this).val(toggle_text_elements[this.id]);
							$('div.vertical-tabs').data('tabs').getCurrentTab().trigger('click');
						}
					});
				}

				$('.filter-quantity').change(function(){
			    	var style_price=parseInt($('#style_price').val());
			    	var style_quantity=parseInt($('.filter-quantity').val());
			    	if(!isNaN(style_price) && !isNaN(style_quantity) && style_price != 0 && style_quantity != 0){
			    		$('.product_price').text(style_price * style_quantity);
			    	}
			    });

				if ($.browser.msie && $.browser.version.substr(0,1)<7) {
					alert('The version of the browser you are using is outdated, as a result some of the website functions may not work satisfactorily. Please upgrade in order to experience optimized website behavior.');
				}
				
				FB.init({
						appId   : '{/literal}{$fb_appId}{literal}',
						status  : true, // check login status
						cookie  : true, // enable cookies to allow the server to access the session
						xfbml   : true // parse XFBML
				});
			
				FB.Canvas.setSize({ width: 760, height: 1200 }); // Live in the past
							
			});
			/* $(document).ready function Ends */
			function validateJerseyForm(){
				// In case no sizes are shown, it implies that product is out of stock.
			    var outOfStock = 'This product is currently out of Stock.';
			    outOfStock = outOfStock + '\nTry other available options';
			    if ($('#size_list').val() == null ) {  alert(outOfStock); return false; }
							
				
				var pattern = '^[0-9]{1,2}$';
				if ($('#preview-number').val() == 'Enter a number' || $('#preview-number').val().length <= 0 || $('#preview-number').val().length > 2 || !$('#preview-number').val().match(pattern)) { alert('Please enter a valid number between 0 and 99'); return false; }
				return true;
			}

			function jersey_preview(image, price, style_id) {
				$('.product_price').text(price);
				$('#style_id').val(style_id);
				var selected_size='';
				if($("#size_list").val())
				{
					selected_size=$("#size_list").val();
				}
				var html = $('#select_list_'+ style_id).html(); 
				$('#size_list').html(html);
				if(selected_size !='')
					$("#size_list").val(selected_size);
				$('#tshirt-type').val(style_id);

				if ($('#preview-name').val() == 'Enter a name') { return; }
				if ($('#preview-number').val() == 'Enter a number') { return; }

				$('.product-image').attr('src', image);
				var name = $('#preview-name').val();
				$('.user_name').show().text(name);
				var number = $('#preview-number').val();

				var innerhtml = '';
				for (var i = 1; i <= number.length; i++) {
					var digit = number.substring((i - 1), i);
					innerhtml += '<div style="width:30px;display:inline;"><img src="{/literal}{$cdn_base}{literal}/skin1/images/numbers/'+digit+'.png" style="width: 30px;border:0px;"/></div>';   
				}

				$('.user_number').show().html(innerhtml);    
			}

		</script>
		<script type="text/javascript">
			var _gaq = _gaq || [];

			_gaq.push(['_setAccount', '{$ga_acc}']);

			_gaq.push(['_trackPageview']);
			(function() {

				var ga = document.createElement('script');

				ga.type = 'text/javascript';

				ga.async = true;

				ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';

				var s = document.getElementsByTagName('script')[0];

				s.parentNode.insertBefore(ga, s);

			})();
			_gaq.push(['_trackEvent', 'facebookContest','view', 'app_page']);
		</script>

		{/literal}
	</body>
</html>
