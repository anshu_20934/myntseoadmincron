<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html xml:lang="en" xmlns:fb="http://www.facebook.com/2008/fbml">
<head>
<META http-equiv="Content-Style-Type" content="text/css">
<META http-equiv="Content-Script-Type" content="type/javascript">
<META http-equiv="Content-Type" content="text/html; charset=utf-8">
{include file="site/header.tpl" }
<!--[if lt IE 8]>
    <link rel="stylesheet" href="new-skin-css/ie.css" type="text/css" media="screen, projection"><![endif]-->

</head>

<body>
<div class="container clearfix">
	{include file="site/menu.tpl" } 

	<h1 class="page-title">Contest Rules</h1>
	
	<hr class="space clear mb10">
	<div class="content clearfix">
    	<div class="content-left left span-23 prepend">
			<ol>
				<li>The winner would be randomly chosen every week.</li>
				<li>The draws for the Reebok/Nike shoes would be separate from the draw for Ferrari jacket.</li>
				<li>Users who participated in one draw are still eligible to participate in forthcoming draws.</li>
				<li>A person shouldn't use multiple accounts to participate in the contest.</li>
				<li>People invited by the participants should hold active accounts on Facebook</li>
				<li>Myntra reserves the sole right to reject or choose a winner.</li>
				<li>International participants would need to either bear the shipping charges for the gifts, or provide an Indian shipping address for the gifts to be sent.</li>
				<li>Any user who logged in to myntra through Facebook and visited the application stands a chance to win Nike/Reebok merchandise</li>
				<li>Any user who made at least 10 friends to successfully register with the app stands a chance to win a Puma Ferrari branded jacket.</li>
				<li>For every additional 10 friends that register through a user, the user would get an extra ticket into the draw which would boost the chances of winning.</li>
			</ol>
		</div>
	</div>	
</div>
	<div class="divider">&nbsp;</div>
	{include file="site/footer.tpl"}
</body>
</html>
