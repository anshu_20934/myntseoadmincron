<!DOCTYPE html>
<html lang="en">
    <head>
        {include file="site/header.tpl"}
        {$smarty.capture.tpl_css}
    </head>
    <body  class="{$_MAB_css_classes}">
        {include file="site/menu.tpl"}
        <div class="container clearfix">
            {$tpl_body}
            {include file="site/footer_content.tpl"}
        </div>
        {$smarty.capture.tpl_markup}
        {include file="site/footer_javascript.tpl"}
        {$smarty.capture.tpl_js}
    </body>
</html>
