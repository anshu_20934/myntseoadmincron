{foreach key=key item=item from=$userinfo}
    {if $key eq 'b_firstname'}
    {assign var='firstname' value=$item}
    {/if}
    {if $key eq 'b_lastname'}
    {assign var='lastname' value=$item}
    {/if}

    {if $key eq 'b_address'}
    {assign var='address' value=$item}
    {/if}
    {if $key eq 'b_city'}
    {assign var='city' value=$item}
    {/if}
    {if $key eq 'b_country'}
    {assign var='country' value=$item}
    {/if}
    {if $key eq 'b_zipcode'}
    {assign var='zipcode' value=$item}
    {/if}

    {if $key eq 'phone'}
    {assign var='phone' value=$item}
    {/if}
     {if $key eq 'mobile'}
    {assign var='mobile' value=$item}
    {/if}
    {if $key eq 'email'}
    {assign var='email' value=$item}
    {/if}

    {if $key eq 'b_state'}
    {assign var='state' value=$item}
    {/if}

{/foreach}
{assign var="bill" value="billing"}


<table>

<input type = "hidden" name="posonline" id="posonline" {if $paymentoptionforpos == 'ONL'} value="true" {else} value="false" {/if}> 
	<tr>
		<td>
 			<font style="font-size:0.8em">first name</font><span class="mandatory">*</span>
 		</td>
 		<td >
 			<input type="text" name="b_firstname" id="b_firstname" {if $paymentoptionforpos != 'ONL' && $billingeditable ne 1} value="{$firstname}"  style="background:#cdc3c3" readonly {/if} >
 		</td>
 	</tr>

 	<tr>
		<td >
 			<font style="font-size:0.8em">last name</font><span class="mandatory">*</span>
 		</td>
 		<td >
 			<input type="text" name="b_lastname" id="b_lastname" {if $paymentoptionforpos != 'ONL' && $billingeditable ne 1}value="{$lastname}"   style="background:#cdc3c3" readonly {/if}>
 		</td>
 	</tr>

 	<tr>
		<td valign="top">
 			<font style="font-size:0.8em">address</font><span class="mandatory">*</span>
 		</td>
 		<td >
 		<textarea name="b_address" id="b_address" class="address" onKeyPress=check_length("b_address"); onKeyDown=check_length("b_address"); {if $paymentoptionforpos != 'ONL' && $billingeditable ne 1} style="background:#cdc3c3" readonly >{$address}{else}>{/if}   </textarea><br>
            <input type=hidden size=2 value=255 name="text_num" id="text_num">

 		</td>
 	</tr>

	

 	 <tr>
		<td >
 			<font style="font-size:0.8em">country</font><span class="mandatory">*</span>
 		</td>
 		<td >
 			<select name="b_country" id="b_country" class="select" {if $paymentoptionforpos != 'ONL' && $billingeditable ne 1} style="background:#cdc3c3" readonly {/if}  onchange="javascript:onchange_country('b_country');">
  
 {foreach key=countrycode item=countryname from=$returnCountry}

 				
  {if $countrycode == $billing_country }
 	 <option value="{$countrycode}" selected>{$countryname}</option>
 	{else}
   <option value="{$countrycode}" >{$countryname}</option>
 	{/if}
 {/foreach} 					
</select>


 		</td>
 	</tr>

 	
 	<tr>
		<td >
 			<font style="font-size:0.8em">state</font><span class="mandatory">*</span>
 		</td>
 		<td >
 		<div id="b_state_list">
 		{ if $returnState != ''}
 		 
 			<select name="b_state" id="b_state" class="select"  {if $paymentoptionforpos != 'ONL' && $billingeditable ne 1} style="background:#cdc3c3" readonly {/if}>
			<option value="" selected>--Select State--</option>
 			{section name="statename" loop=$returnState}

 				{if $returnState[statename].code == $b_state && $billingeditable ne 1}
 				<option value={$returnState[statename].code} selected>{$returnState[statename].state}</option>
 				{else}
 				<option value={$returnState[statename].code}>{$returnState[statename].state}</option>
 				{/if}
 			{/section}
 			</select>
 			
 			{else}
 			<input type="test" id="b_state" value="{$b_state_name}" >
 			{/if}
 			</div>
 		</td>
 	</tr>
</div>
   <tr>
		<td >
 			<font style="font-size:0.8em">city</font><span class="mandatory">*</span>
 		</td>
 		<td >
 			<input type="text" name="b_city" id="b_city" {if $paymentoptionforpos != 'ONL' && $billingeditable ne 1} value="{$city}"  style="background:#cdc3c3" readonly {/if}>
 		</td>
 	</tr>
 	
 	<tr>
		<td >
 			<font style="font-size:0.8em">zip/postal code</font><span class="mandatory">*</span>
 		</td>
 		<td >
 			<input type="text" name="b_zipcode" id="b_zipcode" maxlength="6" size="7" {if $paymentoptionforpos != 'ONL' && $billingeditable ne 1}value="{$zipcode}"  style="background:#cdc3c3" readonly {/if}  >
 		</td>
 	</tr>

	<tr>
		<td >
 			<font style="font-size:0.8em">phone</font>
 		</td>
 		<td >
 			<input type="text" name="b_phone" id="b_phone" {if $paymentoptionforpos != 'ONL' && $billingeditable ne 1}value="{$phone}"  style="background:#cdc3c3" readonly {/if} >
 		</td>
 	</tr>

	<tr>
		<td >
 			<font style="font-size:0.8em">mobile</font><span class="mandatory">*</span>
 		</td>
 		<td >
 			<input type="text" name="b_mobile" id="b_mobile" {if $paymentoptionforpos != 'ONL' && $billingeditable ne 1}value="{$mobile}"  style="background:#cdc3c3" readonly {/if} >
 		</td>
 	</tr>

 	<tr>
		<td >
 			<font style="font-size:0.8em">email</font><span class="mandatory">*</span>
 		</td>
 		<td >
 			<input type="text" name="b_email" id="b_email"  {if $paymentoptionforpos != 'ONL' && $billingeditable ne 1}value="{$email}"  style="background:#cdc3c3" readonly {/if}>
 		</td>
 	</tr>

  </tr>
 	</table>
