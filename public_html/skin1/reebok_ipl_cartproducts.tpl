{literal}
<script language="javascript">
var updated = true;
function updateCart(productId, quantity)
{
   if(validateQuantity(productId, quantity))
   {
	document.updatecart.proHide.value = productId;
   }
   else
   {
   	  return false;
   }
}
function isUpdated(login,pagetype)
{
	if( updated )
		return checkLoggedIn(login,pagetype);
	else
		alert("You have made changes to the cart.Please click update button");
}

function confirmDelete(productname, productid, quantity,producttype)
{
	var flag = confirm("Do you really want to delete "+productname+" "+producttype+" from your cart ?");

	if(flag == true)
	{
		document.updatecart.proHide.value = productid;
	}
	else
	{
		return flag;
	}
}


function updategcCart(index, email, name)
{

	document.updatecart.gcIndex.value = index;
	document.updatecart.rEmail.value = email;
	document.updatecart.rName.value = name;
}

function confirmGcDelete(index, email, name)
{
	var flag = confirm("Do you really want to delete the gift certificate of "+name);

	if(flag == true)
	{
		updategcCart(index, email, name);
	}
	else
	{
		return flag;
	}
}

function updateTotalQuantity(prid)
{
	var quantity = 0;
	quantity = quantity*1;
	var cid = prid+"count";
	//alert(cid);
	var count = document.getElementById(cid).value;
	//alert(count);
	for(var i=1; i<count; i++){
		var id = prid+"sizequantity"+i;
		var value = document.getElementById(id).value;
		if( IsNumeric(value) )
			quantity = quantity + parseInt(value);
		else
		{
			document.getElementById(id).value = "";
		}
	}
	var oldQuantity = document.getElementById(prid + "qty").value;
	if( oldQuantity != quantity)
	{ 
		document.getElementById(prid + "qty").value = quantity;
		alert('please click on the update button to change the total amount as per new quantity.');
		updated = false;
	}
}

function validateQuantity(prid, quantity)
{
  var elm = document.getElementById(prid + "qty");
  var qty = elm.value;
  if(!IsNumeric(qty) ||  qty == '')
	{
		alert("Please enter valid quantity.");
		elm.value = quantity;
		return false;
	}
 else
	{
		return true;
	}
}

function clearCart()
{
	var flag = confirm("Are you sure you want to clear your cart ?");

	if(flag == true)
	{
		window.location.href='clearcart.php';

	}
}
function addMore()
{
	window.location.href='mkstartshopping.php';
}

function checkCoupon(login,formaction){
   if((document.getElementById("couponcode").value) == ""){
	alert("Please input a valid coupon code!");
	document.getElementById("couponcode").focus()
	return false;
   }
   else if(!checkValidInput(document.getElementById("couponcode").value)){
	document.getElementById("couponcode").focus()
        return false	  
   }
   else{
   	 document.updatecart.action = formaction;
	 document.updatecart.submit();
	 //return true
   }		  
}

function checkValidInput(alphane){
    var numaric = alphane;
    if (alphane == ""){
        alert("Please input a valid coupon code");
        return false;
    }
    return true;
}
</script>
{/literal}
<form name='updatecart' action='updateProducts.php?' method='post'>
<input type=hidden id="proHide" name="proHide" >
<input type=hidden id="gcIndex" name="gcIndex" >
<input type=hidden id="rEmail" name="rEmail" >
<input type=hidden id="rName" name="rName" >
<input type=hidden id="pagetype" name="pagetype" value="{$pagetype}" >
<input type="hidden" name="ajaxStatus" id= "ajaxStatus" value="0">
<input type="hidden" name="affiliateAuthType" id= "affiliateAuthType" value="{$affiliateAuthType}">

           <div class="super" style="background-image:url({$cdn_base}/skin1/affiliatetemplates/images/reebok_ipl/shopping_cart.gif);background-repeat:no-repeat;"></div>
          <div class="head" style="background-image:url({$cdn_base}/skin1/affiliatetemplates/images/reebok_ipl/products.gif);background-repeat:no-repeat;"></div>
    {assign var=tabindex value=1}
		{foreach name=outer item=product from=$productsInCart}
				{foreach key=key item=item from=$product}
						{if $key eq 'productId'}
						{assign var='productId' value=$item}
						{/if}

						{if $key eq 'producTypeId'}
						{assign var='producTypeId' value=$item}
						{/if}

						{if $key eq 'productStyleId'}
						{assign var='productStyleId' value=$item}
						{/if}

						{if $key eq 'productStyleName'}
						{assign var='productStyleName' value=$item}
						{/if}

						{if $key eq 'quantity'}
						{assign var='quantity' value=$item}
						{/if}


						{if $key eq 'productPrice'}
						{assign var='productPrice' value=$item}
						{/if}

						{if $key eq 'productTypeLabel'}
						{assign var='productTypeLabel' value=$item}
						{/if}

						{if $key eq 'totalPrice'}
						{assign var='totalPrice' value=$item}
						{/if}

						{if $key eq 'optionNames'}
						{assign var='optionNames' value=$item}
						{/if}

						{if $key eq 'optionValues'}
						{assign var='optionValues' value=$item}
						{/if}

						{if $key eq 'optionCount'}
						{assign var='optionCount' value=$item}
						{/if}

						{if $key eq 'sizenames'}
						{assign var='sizenames' value=$item}
						{/if}

						{if $key eq 'sizequantities'}
						{assign var='sizevalues' value=$item}
						{/if}

						{if $key eq 'designImagePath'}
							{if $productStyleId == '602' || $productStyleId == '606'}
								{assign var='designImagePath' value=$http_location|cat:'/skin1/affiliatetemplates/images/reebok_ipl/kkr_front_inactive.png'}
							
							{elseif $productStyleId == '603' || $productStyleId == '607'}
								{assign var='designImagePath' value=$http_location|cat:'/skin1/affiliatetemplates/images/reebok_ipl/new_reebok_ipl/pk/a_front_icon_inactive.jpg'}
							
							{elseif $productStyleId == '604' || $productStyleId == '608'}
								{assign var='designImagePath' value=$http_location|cat:'/skin1/affiliatetemplates/images/reebok_ipl/rc_front_inactive.png'}					
								
							{elseif $productStyleId == '601' || $productStyleId == '605'}
								{assign var='designImagePath' value=$http_location|cat:'/skin1/affiliatetemplates/images/reebok_ipl/csk_front_inactive.png'}
							
							<!--reebok-fan-gear-->
							{elseif $productStyleId == '652' || $productStyleId == '656'}
								{assign var='designImagePath' value=$http_location|cat:'/skin1/affiliatetemplates/images/reebok_ipl/new_reebok_ipl/kn/cf_front_icon_inactive.jpg'}
							
							{elseif $productStyleId == '655' || $productStyleId == '658'}
								{assign var='designImagePath' value=$http_location|cat:'/skin1/affiliatetemplates/images/reebok_ipl/new_reebok_ipl/pk/cf_front_icon_inactive.jpg'}
							
							{elseif $productStyleId == '654' || $productStyleId == '657'}
								{assign var='designImagePath' value=$http_location|cat:'/skin1/affiliatetemplates/images/reebok_ipl/new_reebok_ipl/rc/cf_front_icon_inactive.jpg'}					
								
							{elseif $productStyleId == '653' || $productStyleId == '659'}
								{assign var='designImagePath' value=$http_location|cat:'/skin1/affiliatetemplates/images/reebok_ipl/new_reebok_ipl/csk/cf_front_icon_inactive.jpg'}
							
							<!--reebok-fan-wear-non-customizable-->
							{elseif $productStyleId == '864'}
								{assign var='designImagePath' value=$http_location|cat:'/skin1/affiliatetemplates/images/reebok_ipl/new_reebok_ipl/rc/f_front_icon_inactive.jpg'}
							{elseif $productStyleId == '881'}
								{assign var='designImagePath' value=$http_location|cat:'/skin1/affiliatetemplates/images/reebok_ipl/new_reebok_ipl/dc/f_front_icon_inactive.jpg'}
							{elseif $productStyleId == '880'}
								{assign var='designImagePath' value=$http_location|cat:'/skin1/affiliatetemplates/images/reebok_ipl/new_reebok_ipl/dc/f0_front_icon_inactive.jpg'}
							{else}
								{assign var='designImagePath' value=$item}							
							{/if}
							<!--reebok-fan-gear-->
						{/if}

						{if $key eq 'discount'}
						{assign var='discount' value=$item}
						{/if}

						{if $key eq 'custAreaCount'}
						{assign var='custAreaCount' value=$item}
						{/if}

						{if $key eq 'productStyleType'}
						{assign var='productStyleType' value=$item}
						{/if}

						{if $key eq 'description'}
						{assign var='description' value=$item}
						{/if}

					{/foreach}
	  <div>
              <table cellpadding="2" class="table">

                <tr>
                  <td class="align_left" style="width:85%"><strong>Product Type:</strong>&nbsp;{$productTypeLabel}</td>
		  <td class="align_left" rowspan="3" colspan="2" >
		   {if $designImagePath != ""}
		     {if $ie eq "true"}

				<span style="display:inline-block;filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='{$designImagePath}');
">					<img src="{$designImagePath}"  alt="Design" style="
filter:progid:DXImageTransform.Microsoft.Alpha(opacity=0);"  width=100 ></span>{if $affiliateId ne '379'}<br/ >{/if} <br/>
                {if $custAreaCount gt 1}
                       showing 1 of 2 designs&nbsp;&nbsp;<a href="javascript: custAreaImages('popup', 0,0, '{$productId}');" >View All</a>
               {/if}
		    {else}
			 {if $orgId neq ''}
			   <img src="{$designImagePath}"  width=100 ><br/> &nbsp; &nbsp;product image <br/ >
			  {else}
			   <img src="{$designImagePath}"  width=100 >{if $affiliateId ne '379'}<br/>{/if} <br/ >
			 {/if}	
		       {if $custAreaCount gt 1}
		       		showing 1 of 2 designs&nbsp;&nbsp;<a href="javascript: custAreaImages('popup', 0,0, '{$productId}');" >View All</a>
		       {/if}
		   {/if}
		      {if $product.promotion.promotional_offer}<table cellpadding="0" cellspacing="0" border="0" style=""><tr><td valign="top"><img src='{$product.promotion.icon_img}' alt='promotional offer' style="border:0px;"></td><td valign="middle" style='padding-left:2px;font-size: 0.7em;color:#FF0000;'>{$product.promotion.promotional_offer}</td></tr></table> {/if}
		{else}
			<img src="{$cdn_base}/default_image.gif" width="100" height="100" />
		{/if}

		  </td>
        </tr>

                <tr>
                  <td class="align_left" style="width:85%"><strong>Product Style:</strong>&nbsp;{$productStyleName}</td>
                </tr>
			<tr>
				<td class="align_left" style="width:85%;"><strong>Name to be printed on the back of Jersey:</strong>&nbsp;<b><u>{$reebok_ipl_products.$productId.text}</u></b></td>
			</tr>
			<tr>
				<td class="align_left" style="width:85%;"><strong>Number to be printed on the back of Jersey:</strong>&nbsp;<b><u>{$reebok_ipl_products.$productId.num}</u></b></td>
			</tr>
		 {if  $description }
		 <tr>
			<td class="align_left" colspan="2">
			       <b>Description</b> : {$description}
			</td>
                <tr>
		{/if}

		{if $sizenames}
                <tr><td class="align_left">
				<table><tr>
				<td style="text-align:left;width:60px; font-size:0.7em;"><strong>Size:</strong></td>
				{foreach from=$sizenames item=sizename}
                  	 <td style="text-align:center;width:60px; font-size:0.7em;">&nbsp;{$sizename}</td>
				{/foreach}
				</tr></table>
                </td></tr>

				<tr><td class="align_left">
				<table><tr>
				<td style="text-align:left;width:60px;font-size:0.7em;"><strong>Quantity:</strong></td>
				{assign var=i value=1}
				{foreach from=$sizevalues item=sizevalue}
                  	 <td style="text-align:center;width:60px;"><input type="text" name="{$productId}sizequantity[]" class="quantity" id="{$productId}sizequantity{$i}" value="{$sizevalue}" style="width:30px;" onchange="javascript:updateTotalQuantity({$productId});"></td>
			    {assign var=i value=$i+1}
				{/foreach}
				<input type="hidden" name="{$productId}count" id="{$productId}count" value="{$i}">
				</tr></table>
                </td></tr>
		{else}
				<tr>
					<td class="align_left"></td><td class="align_left"></td>
				</tr>
				<tr>
					<td class="align_left"></td><td class="align_left"></td>
				</tr>
		{/if}
		 <tr>
			<td class="align_left">
			{if $sizenames}<strong>Total Quantity:</strong><input id="{$productId}qty"  name="{$productId}qty" type="text" class="quantity"  value="{$quantity}" maxlength="4" tabindex={$tabindex} style="border:0px;width:30px;" readonly/>{else}<strong>Total Quantity:</strong><input id="{$productId}qty"  name="{$productId}qty" type="text" class="quantity"  value="{$quantity}" maxlength="4" tabindex={$tabindex} style="width:30px;" onchange="javascript:alert('please click on update button to change price as per new quantity.');" />{/if}
				<input type="submit" border="0" value=" update " name="updateBtn" class="submit" onClick="return updateCart({$productId}, {$quantity});" tabindex={$tabindex}/>&nbsp;<input type="submit" border="0" value=" remove " name="removeBtn" class="submit" onClick="return confirmDelete('{$productStyleName|escape:"quotes"}', '{$productId}', '{$quantity}','{$productTypeLabel}')" tabindex={$tabindex}/>
			</td>
			<td class="align_left">
				&nbsp;
			</td>
		 </tr>
              
		{if $productStyleType == 'P'}

		  <td class="align_left" ><strong>Unit Price:</strong>&nbsp;Rs.<span >{$productPrice|string_format:"%.2f"}</span>&nbsp;</td>
		   <td class="align_left" ><strong>Price:</strong>&nbsp;Rs.{$totalPrice|string_format:"%.2f"}</td>
		{else}
		  <td class="align_left" ><strong>Unit Price:</strong>&nbsp;Rs.{$productPrice|string_format:"%.2f"}</td>
		  <td class="align_left" ><strong>Price:</strong>&nbsp;Rs.{$totalPrice|string_format:"%.2f"}</td>
		 {/if}

                </tr>
              </table>
            </div>
            <div class="links"> &nbsp; </div>
          {assign var=tabindex value=$tabindex+1}
      {/foreach}

{*************************Start Gift Certificate*************************}
{if  $giftcerts != "" }
		{assign var=sno value=0}
	{foreach name=outer item=gift from=$giftcerts}
		{foreach key=key item=item from=$gift}
				{if $key eq 'purchaser'}
					{assign var='purchaser' value=$item}
				{/if}
				{if $key eq 'quantity'}
					{assign var='quantity' value=$item}
				{/if}
				{if $key eq 'recipient'}
					{assign var='recipient' value=$item}
				{/if}
				{if $key eq 'amount'}
					{assign var='amount' value=$item}
				{/if}
				{if $key eq 'totalamount'}
					{assign var='totalamount' value=$item}
				{/if}
				{if $key eq 'send_via'}
					{assign var='send_via' value=$item}
				{/if}

				{if $key eq 'recipient_email'}
					{assign var='recipient_email' value=$item}
				{/if}
				{if $key eq 'recipient_firstname'}
					{assign var='recipient_firstname' value=$item}
				{/if}

				{if $key eq 'recipient_lastname'}
					{assign var='recipient_lastname' value=$item}
				{/if}

				{if $key eq 'recipient_address'}
					{assign var='recipient_address' value=$item}
				{/if}

				{if $key eq 'recipient_city'}
					{assign var='recipient_city' value=$item}
				{/if}

				{if $key eq 'recipient_zipcode'}
					{assign var='recipient_zipcode' value=$item}
				{/if}

				{if $key eq 'recipient_county'}
					{assign var='recipient_county' value=$item}
				{/if}

				{if $key eq 'recipient_statename'}
					{assign var='recipient_statename' value=$item}
				{/if}

			{/foreach}

		<div>
              <table cellpadding="2" class="table">

                <tr>

                  <td class="align_left" style="width:70%"><strong>From:</strong>&nbsp;{$purchaser}</td>

                  <!-- <td class="align_left" rowspan="4"></td> -->
		  <td class="align_left" rowspan="3" colspan="2" >

			<img src="{$cdn_base}/skin1/images/gift.gif" hspace="5" vspace="5" alt="" />




		  </td>
                </tr>

                <tr>
                  <td class="align_left"><strong>to:</strong>&nbsp;{$recipient}</td>
                </tr>

                <tr>
			{if $send_via eq 'E'}
				<td class="align_left" ><strong>Recipient Email:</strong>&nbsp;{$recipient_email}</td>
			{/if}
			{if $send_via eq 'P'}
				<td class="align_left" ><strong>Recipient Address:</strong>&nbsp;{$recipient_firstname}&nbsp; {$recipient_lastname}<br/>{$recipient_address}<br/>{$recipient_city}<br/>{$recipient_country}</td>
			{/if}

                </tr>

		 <tr>
			<td class="align_left"><strong>Quantity:</strong>
				 <input id="{$sno}qty"  name="{$sno}qty" type="text" class="quantity" value="{$quantity}"  maxlength="4">
				 <input type="submit" border="0" value="update quantity" name="updateGcBtn" class="submit"  onClick='updategcCart("{$sno}", "{$recipient_email}", "{$recipient}");' />
			</td>
			<td class="align_left" colspan="3">
				<input type="submit" border="0" value="remove" name="removeGcBtn" class="submit"  onClick='return confirmGcDelete("{$sno}", "{$recipient_email}", "{$recipient}")' />
			</td>
                </tr>

                <tr>
		  <td class="align_left"><strong>Unit Price:</strong>&nbsp;Rs.{$amount|string_format:"%.2f"}</td>
		  <td class="align_left" ><strong>Total Price:</strong>&nbsp;Rs.{$totalamount|string_format:"%.2f"}</td>
                </tr>
              </table>
            </div>
	    <div class="links"> &nbsp; </div>
	    {assign var=sno value=$sno+1}
		{/foreach}
{/if}
{*************************End gift certificate*************************}
	
          <div>
            <table cellpadding="2" class="table" border="0">

				<input type="hidden" name="hidetotamount" id="hidetotamount" value="{$totalAfterDiscount}">
								<input type="hidden" name="ibibophotodiscount" id="ibibophotodiscount" value="{$ibibodiscountonphotos|string_format:"%.2f"}">

			    <tr class="font_bold">
					<td class="align_right" colspan="9">Amount:</td>

					<td class="align_right">Rs.{$subTotal|string_format:"%.2f"}</td>
			    </tr>



				{if $coupondiscount != ""}
				      <tr class="font_bold">
					<td class="align_right" colspan="9">Coupon Discount(coupon code: {$couponCode}({$couponpercent})):</td>
					<td class="align_right">Rs.{$coupondiscount|string_format:"%.2f"}</td>
				      </tr>
				 {/if}


			   <!--  <tr class="font_bold">
					<input type="hidden" name="hidevat" id="hidevat" value="{$vat|string_format:"%.2f"}">
					<td class="align_right" colspan="9">Vat/Cst:</td>
					<td class="align_right"><div id="vatdiv">Rs.{$vat|string_format:"%.2f"}</div></td>
			      </tr>-->
			      <tr class="font_bold">
					<td class="align_right" colspan="9">Amount to be paid (shipping cost extra):</td>
					<td class="align_right"><div id="grandtotal">Rs.{$grandTotal|string_format:"%.2f"}&nbsp;(inclusive of VAT/CST)</div></td>
			      </tr>

			     {if $offers}
				      <tr class="font_bold" style='color:red;'>
						<td class="align_right" colspan="10"><div id="grandtotal">{$offers.OFFER_999}</div></td>
				      </tr>
			       {/if}
				<tr><td  colspan="10"  >&nbsp;</td></tr>
				<tr><td  colspan="10"  >&nbsp;</td></tr>
			    <tr class="font_bold">
			    <td colspan="10" align="right" valign="middle" style="padding-bottom:10px;">
			    <!--<form name="couponform" id="couponform" action="mkmycart.php?c=1&pagetype={$pagetype}" method="post">-->
			    	<input type="hidden" id="referral_flag" name="referral_flag" value="">
			    	<div style="float:right;">
				    	<div style="float:left;font-size:14px;font-weight:bold">Got myntra coupon code ?&nbsp;</div>
				    	<div style="float:left;"><input type="text" name="couponcode" id="couponcode" value="{$smarty.post.couponcode}" style="border:1px solid pink;height:21px;width:120px"/></div>
				    	<div style="float:left;padding-left:3px;"><input class="submit" style="background-color:#7CAF78;" type="submit" border="0" name="couponButton" value="redeem" autocomplete="off" onClick="return checkCoupon('{$login}','mkmycart.php?c=1&pagetype={$pagetype}');"/></div>				    	
			    	</div>
			    <!--</form>-->
			    </td>
			    </tr>

			      <tr>
			      		<td>
						<a style='cursor:pointer;text-decoration:underline;font-size:1.2em;' onClick="clearCart()">clear your cart</a>
						</td>
					<td  colspan="9" align='right' >
						{if $affiliateId eq '446'}
							<a href="{$http_location}/reebok-fan-gear" title='add more products' style='font-size:1.2em;color:blue;' >add more products</a><input type="button" border="0" value="buy now >>" name="submitButton" class="submit" style="font-weight:bold;font-size:1.2em;width:100px;background:#CB0029;cursor:pointer;" onClick="return isUpdated('{$login}', '{$pagetype}')" />
						{else}
							<a href="{$http_location}/reebok-store" title='add more products' style='font-size:1.2em;color:blue;' >add more products</a><input type="button" border="0" value="buy now >>" name="submitButton" class="submit" style="font-weight:bold;font-size:1.2em;width:100px;background:#CB0029;cursor:pointer;" onClick="return isUpdated('{$login}', '{$pagetype}')" />
						{/if}
					</td>
			     </tr>
			     <tr><td colspan="10" style="font-size:0.9em;" align="center">The personalized Jerseys will be specially made to order - please allow 5-7 days for processing</td></tr>		
            </table>
          </div>

          <div class="foot"></div>
</form>
