<div>
<table cellpadding="2"  class="printtable">
 <tr class="font_bold">
<th class="align_left" colspan="8">Change Requests</th>
  </tr>
  <tr class="font_bold">
<td class="align_left" colspan="1">Date</td>
<td class="align_left" colspan="1">Title</td>
<td class="align_left" colspan="1">Description</td>
  </tr>
  {foreach from=$orderComments item=comment}
    <tr>
        <td style="font-size:1.0em;"><b>{$comment.date}</b></td>
        <td style="font-size:1.0em;">{$comment.commenttitle}</td>
        <td style="font-size:1.0em;">{$comment.description}</td>
    </tr>
  {/foreach}
</table>
</div>