{* $Id: mykritmeta.tpl,v 1.26 2006/04/10 07:36:17 max Exp $ *}
<head>
<title>
	{if $pageTitle ne ''}{$pageTitle}{elseif $pageHeader ne ''}{$pageHeader}{else}Myntra{/if}
</title>
{*include file="meta.tpl" *}
<meta name="description" content="{$metaDescription|escape}" />
<meta name="keywords" content="{$metaKeywords|escape}" />

<link rel="shortcut icon" href="{$SkinDir}/favicon.ico">
 
 
 
<link rel="stylesheet" href="{$SkinDir}/{#mkCSSFile#}" />
<link rel="stylesheet" href="{$SkinDir}/css/thickbox.css" type="text/css" media="screen" />
<link rel="stylesheet" href="{$SkinDir}/css/tooltip.css" type="text/css" media="screen" />
<script type="text/javascript">
    var http_loc = '{$http_location}'
 </script>
<script type="text/javascript" src="{$SkinDir}/PopupWindow.js"></script>
<script type="text/javascript" src="{$SkinDir}/divpop_js.js"></script>
<script type="text/javascript" src="{$SkinDir}/js_script/jquery.js"></script>
<script type="text/javascript" src="{$SkinDir}/js_script/thickbox.js"></script>
<script type="text/javascript" src="{$SkinDir}/js_script/email_validate.js"></script>
<script type="text/javascript" src="{$SkinDir}/js_script/redirecturl.js"></script>
{if $affiliateId neq '' && $themeid eq 'COBRANDED_UI'}
	<link rel="stylesheet" href="{$SkinDir}/affiliatetemplates/affiliate.css" />
{/if} 
{if $affiliateId eq '21'}

<script type="text/javascript">

var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");

document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));

</script>
	<script type="text/javascript">
  var pageTracker = _gat._getTracker("UA-252486-8");
  pageTracker._initData();
  pageTracker._trackPageview();
 
  pageTracker._addTrans(
    "{$orderid}",                                 // Order ID
    "Myntra.com",                               // Affiliation
    "{$djvalue}",                                // Total
    "{$vat}",                                     // Tax
    "{$shippingRate}",                        // Shipping
    "{$cityname}",                            // City
    "{$statename}",                          // State
    "India"                                       // Country
  );
 {foreach name=outer item=product from=$productsInCart}
	{foreach key=key item=item from=$product}
		{if $key eq 'productId'}
			{assign var='productId' value=$item}
		{/if}
		{if $key eq 'productTypeLabel'}
			{assign var='productTypeLabel' value=$item}
		{/if}
		{if $key eq 'productStyleName'}
			{assign var='productStyleName' value=$item}
		{/if}
		{if $key eq 'productPrice'}
			{assign var='productPrice' value=$item}
		{/if}
		{if $key eq 'quantity'}
			{assign var='quantity' value=$item}
		{/if}
	{/foreach}
	pageTracker._addItem(
    "{$orderid}",                             // Order ID
    "{$productId}",                         // SKU / Product code
    "{$productTypeLabel}",              // Product Name 
    "{$productStyleName}",             // Category
    "{$productPrice}",                     // Price
    "{$quantity}"                            // Quantity
  );

{/foreach}
  
  pageTracker._trackTrans();
</script>
{/if}
      	        
	<base href="{$baseurl}">
</head>