<div class="form">
            <div class="subhead"> 
              <div>{$pageheading}</div>
            </div>
            <div class="text"> 
              <div class="seriallinks"> 
	      {if $categorytags != ""} 
	      {section name=tagidx loop=$categorytags } 
			<a href="categorytag/{$categorytags[tagidx][2]}/{$categorytags[tagidx][0]}/{$categorytags[tagidx][1]}">{$categorytags[tagidx][0]}</a>&nbsp;&nbsp;&nbsp;

	      {/section}
	      {/if}
	      {if $prodtags != ""}
	      {section name=tagidx1 loop=$prodtags }
			{if ($prodtags[tagidx1][4] gte $min) and ($prodtags[tagidx1][4] lte $i1)}
				{assign var='style' value='font-size:1.0em; color:#3399FF;'}
			{elseif ($prodtags[tagidx1][4] gt $i1) and ($prodtags[tagidx1][4] lte $i2)}
				{assign var='style' value='font-size:1.2em; color:#3399FF; font-weight:bold;'}
			{elseif $prodtags[tagidx1][4] gt $i2 and $prodtags[tagidx1][4] lte $i3}
				{assign var='style' value='font-size:1.4em; color:#3399FF; font-weight:bold;'}
			{elseif $prodtags[tagidx1][4] gt $i3 and $prodtags[tagidx1][4] lte $i4}
				{assign var='style' value='font-size:1.6em; color:#3399FF; font-weight:bold;'}
			{else}
				{assign var='style' value='font-size:1.8em; color:#3399FF; font-weight:bold;letter-spacing:-0.6;'}
			{/if}
			<a href="unique/{$prodtags[tagidx1][0]}/{$productlabel}/{$selectedproductypid}/{$prodtags[tagidx1][1]}" style="{$style}" onClick="addcount('{$prodtags[tagidx1][1]}');">{$prodtags[tagidx1][0]|regex_replace:"/[\\\]/":""}</a>&nbsp;&nbsp;&nbsp;

	      {/section}
	      {/if}
            
              </div>
            </div>
            <div class="foot"></div>
 </div>
 {literal}
	<script type="text/javascript">
		function addcount(id){
			 var url = "browseDesignByProductType.php?tcid="+id;
			 sendRequest(request,url);
		}
		function sendRequest(request,url,parameters) {
		  request.onreadystatechange = inc_count;
		  request.open("GET", url, false);
		  request.send(null);
		}
		function inc_count() {
			 if (request.readyState == 4) {
				if (request.status == 200) {
					//donothing
				}
				else {
					alert("Error! Request status is " + request.status);
				}
			}
		}
		function createRequest() {
		  var request = null;
		  try {
			request = new XMLHttpRequest();
		  } catch (trymicrosoft) {
			try {
			  request = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (othermicrosoft) {
			  try {
				request = new ActiveXObject("Microsoft.XMLHTTP");
			  } catch (failed) {
				request = null;
			  }
			}
		  }

		  if (request == null) {
			alert("Error creating request object!");
		  } else {
			return request;
		  }
		}

		var request = createRequest();
	</script>
{/literal}
         
