{include file="codlearnmore.tpl" }

<div class="wrap">
				<!-- the tabs -->
					<ul class="tabs" style="_overflow:hidden;display:none">
						<li id="productsTab"><a href="#">Select Products</a></li>
					</ul>


<div class="pane" id="main_pane" style="border:none;padding:5px;">
	<!--<p>#1</p>
 the tabs -->
 <div class="title-style" id="tab_title">{if $product_style_groups[0].group_name}{$product_style_groups[0].group_name}{/if}</div>
<ul class="tabs sp">
                                      
{section name=num loop=$product_style_groups}
    {assign var=tab_label value=$product_style_groups[num].group_name|escape:"quotes"}
    {assign var=style_group_id value=$product_style_groups[num].style_group_id}
	    {if $productStyleIconImages[$style_group_id][0]}
	        <li style="background:none"> <a href="#" id="tabs_ul_li_a_{$product_style_groups[num].style_group_id}" class="sub" onclick="$('#tab_title').html('{$tab_label}')">{$product_style_groups[num].group_name|truncate:10:"..":true}</a></li>
	    {/if}
{/section}

</ul>
<!-- tab "panes" -->
{section name=num_top loop=$product_style_groups}
{assign var=style_group_id value=$product_style_groups[num_top].style_group_id }
{if $productStyleIconImages[$style_group_id][0]}
<div class="pane" style="padding-left:3px; padding-right:1px;margin:0;">
 <!-- "previous page" action -->
   <a class="prevPage browse left" style="position:relative;"></a>
    <!-- root element for scrollable -->
       <div class="scrollable" style="width:430px">
            <!-- root element for the items -->
             <div class="items">
			 {assign var=style_id value=$product_style_groups[num_top].style_group_id}
				 {section name=num loop=$productStyleIconImages[$style_id][0]}
             <div class="item">
				    <img src="{$cdn_base}/skin1/images/spacer.gif" lazy-src="{$productStyleIconImages[$style_id][0][num].image_i}" class="lazy" id= "sp_{$productStyleIconImages[$style_id][0][num].id}" class="{if $productStyleIconImages[$style_id][0][num].id eq $productStyleId}active{/if}"  style="height:75px;width:75px;margin-right:2px;" ALT="Choose Style  for {$productStyleIconImages[$style_id][0][num].name}" title="{$productStyleIconImages[$style_id][0][num].name}" onclick="location.href='{$http_location}{$currentURL}?productStyleId={$productStyleIconImages[$style_id][0][num].id}'" >
            </div>
				    
		                {/section}
            </div>
      </div>
        <!-- "next page" action -->
     <a class="nextPage browse right"  style="position:relative;"></a>
</div>
{/if}
   {/section}
 </div>


					<div class="clear">&nbsp;</div>
        <form action="mkretrievedataforcart.php?pagetype=productdetail" id="hiddenForm" method="post" onclick="return submitCartForm();">			
				<input type="hidden" id="count" value="{$i}">
				<input type="hidden" name="productStyleId" value="{$productStyleDetails[0][0]}">
				<input type="hidden" name="productId" value="{$productid}">
				<input type="hidden" name="customizationAreaId" id="caId" value="">
				<input type="hidden" name="customizationAreaName" id="custAreaName" value="custArea0">
				<input type="hidden" name="quantity"  id="quantity" value="1">
				<input type="hidden" name="uploadedImagePath" id="imagepath" value="">
				<input type="hidden" name="unitPrice" value="{$productStyleDetails[0][5]}">
				<input type="hidden" name="productId" value="{$productid}">
				<input type="hidden" name="pagetype" value="productdetail">
				<input type="hidden" name="totalOptions" id="totalOptions" value="{$Options}">
				<input type="hidden" name="productName" id="productName" value="{$designer[6]}">
				<input type="hidden" name="productTypeName" id="productTypeName" value="{$productTypeLabel}">
				<input type="hidden" name="windowid" value="{$windowid}" >
				<!--  The codsetting hidden parameter is passed, for handling directly reaching to
								      checkout page, when Pay Cash on Delivery Button is clicked. -->
								<!--  javascript function setcodsetting() handles changing this value -->     
								<input type="hidden"  id="codsetting" name="codoption" value="no" >

								{section name=options loop=$productOptions}
								<input type="hidden" name="option{$smarty.section.options.index}name"  id="option{$smarty.section.options.index}name"  value="{$productOptions[options][0]}">
								<input type="hidden" name="option{$smarty.section.options.index}value"  id= "option{$smarty.section.options.index}value" value="">
								<input type="hidden" name="option{$smarty.section.options.index}id"  id= "option{$smarty.section.options.index}id"  value="" >
								{/section}

								<input type="hidden" name="referrer" value="productDetail">
								<input type="hidden" name="referredTo"  id= "referredTo" value="">
								<input type="hidden" name="quantityMenu" id="quantityMenu" size="3" maxlength="4" value="1">
								
                    <div class="sports-size-chart">
                        <span class="sub-shead">Select Size {if $size_chart_image_path neq '' }<a href="images/style/sizechart/{$size_chart_image_path}" class="thickbox" id="sizechart">Size chart</a>{/if}</span>
                        <ul>
							{assign var='i' value=1}
							{if $productOptions}
                            {section name=options loop=$productOptions }
							{section name=values loop=$productOptions[options][1]}
										{assign var='j' value=$i-1}
                                <li><a class="size_options" id="size_option{$i}" style="cursor:pointer" onclick="selectSize('{$i}')">{$productOptions[options][1][values][0]}</a></li>
				<input type="hidden" name="sizequantity[]" id="sizequantity{$i}" class="sizequantity" onchange="javascript:updateTotalQuantity();">
				<input type="hidden" name="sizename[]" id="sizename{$i}" value="{$productOptions[options][1][values][0]}" class="sizename">
				{assign var='i' value=$i+1}
                            {/section}
							{/section}
							{/if}
				<input type="hidden" name="quantity"  id="quantity" value="1">
                        </ul>
                    </div>
                    <div class="clear">&nbsp;</div>
					{if !$disableProductStyle}
					<div class="MRP-rates">

                                <span class="our-price">Price : <strong>Rs {$productStyleDetails[0][5]}</strong></span>
								{if ($productStyleDetails[0][5] + $cart_amount) gt $free_shipping_amount}
								{/if}

                    </div>
                    <div class="clear">&nbsp;</div>
                    <div class="cart-btns">
                        <div class="cartbtn">
                            <div><a style="cursor:pointer" onClick="setcodsetting('no')" href="javascript:if(notCustomized('{$cstatus}', 'mkretrievedataforcart.php?windowid={$windowid}&amp;at=c;'));">Buy now <span>&raquo;</span></a></div>
                        </div>
                        <div class="codbtn">
                            <ul>
                                <li>FREE Shipping in India</li>
                                <li><span>Cash on Delivery Option Available</span> <a style="cursor:pointer" onclick="showLearnMore()">Learn More</a></li>

                            </ul>
                        </div>

                    </div>
			</form>
					<div class="clear" style="height:10px;">&nbsp;</div>
					{/if}
</div>
{literal}
	<script type="text/javascript">

	   
		function setcodsetting(codvalue) {
			$('#codsetting').val(codvalue);
		}
		function hidechartDiv()
		{
			document.getElementById('chartpopup').style.visibility = 'hidden';
		}
		function showchartDiv()
		{
		    document.getElementById('chartpopup').style.visibility = 'visible'; 
		}
		function updateTotalQuantity()
		{
			var quantity = 0;
			quantity = quantity*1;
			var count = document.getElementById('count').value;
			//alert(count);
			for(var i=1; i<count; i++){
				var id = 'sizequantity'+i;
				var value = document.getElementById(id).value;
				value = value*1;
				quantity = quantity + value;
			}
			document.getElementById("quantityMenu").value = quantity;
		}
		
		
	</script>
{/literal}
