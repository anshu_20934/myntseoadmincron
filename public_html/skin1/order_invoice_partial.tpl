<div class="container"> 
  <div class="wrapper"> 
    <div class="display"> 
      <div class="center_column"> 
        <div class="print"> 
         <div class="super" style="background:none;height:50px;"> 
            <p>{if $smarty.get.view eq 'OD'}Invoice{else}{if $posaccounttype eq 'P2' && $smarty.get.t eq "u"}order details{elseif ($posaccounttype eq 'SM' && $smarty.get.t eq "u") or ($source_type_id eq '11' && $smarty.get.type eq 'admin')}delivery challan{else}invoice{/if}{/if}<br/></p>
          </div>
		{if $smarty.get.t neq "d"}
	
			{if $posaccounttype eq 'P2'|| $source_type_id eq 11}
	           <div class="links" align="right">
	           <p style="float:right;">
		   	<b>Store Details : </b><br/>
			{if $store.source_manager_email ne ''}	Email : {$store.source_manager_email}<br/>{/if}
			{if $store.landline ne ''}Landline : {$store.landline}<br/>{/if}
			{if $store.mobile ne ''}Mobile : {$store.mobile}<br/>{/if}
			{if $store.fax ne ''}Fax : {$store.fax}<br/>{/if}
	
			<p style="float:left;"><b>Store Name : {$store.source_name}</b><br/>
	               <b>Store Code : {$store.source_code}</b>
	            </p>
	          </div> 
	        
	          <div class="foot"></div>
	         <!-- <div class="subhead"></div>-->
		{/if}
		<!-- For CCD bulk orders -->
		{if $outlet.orderid eq $orderid}
	        <div class="links" align="right">
				<p style="float:right;">
					{if $outlet.outlet_code ne ''}Cafe Code : {$outlet.outlet_code}<br/>{/if}
					{if $outlet.outlet_name ne ''}Cafe Name : <b>{$outlet.outlet_name}</b><br/>{/if}
					{if $outlet.outlet_region ne ''}Region : {$outlet.outlet_region}<br/>{/if}
					{if $outlet.outlet_state ne ''}State : {$outlet.outlet_state}<br/>{/if}
					{if $outlet.outlet_city ne ''}City : {$outlet.outlet_city}<br/>{/if}
				</p>
				<p style="float:left;">
					{if $outlet.area_mgr ne ''}Area Sales Manager Name : {$outlet.area_mgr}<br/>{/if}
					{if $outlet.area_mgr_email ne ''}Area Sales Manager Email : {$outlet.area_mgr_email}<br/>{/if}
					{if $outlet.ref_id ne ''}Reference No : {$outlet.ref_id}<br/>{/if}
				</p>
	        </div>
	        <div class="foot"></div>
		{/if}
		{if $smarty.get.view ne 'DOD'}
           <div class="links" align="right">
           <p style="float:right;">
		<font color="#ff0000">ATTN: The Logistics Manager</font><br/>
           <b>{$companyName}<br/>
            {$address1}<br/>
            {$address2}<br/>
            {if $address3 }
                {$address3}<br/>
            {/if}
            {$address4}<br/>
            {if $customerSupportCall}Customer Care: {$customerSupportCall}{/if}<br/>
           	Tin Number:&nbsp;29490587999</b></p>

			{if ($smarty.get.t neq "u" && $smarty.get.type ne 'admin') || $source_type_id neq '11'}
		<p style="float:left;"><b style="float:left;">Invoice number : # {$invoiceid}</b><br/>
            <img src="{$barcodeUrl}" style="display:block;float:left;" width='150' height='60' />
            {if $paymentoption eq 'cod'}<br><br><span style="font-size:24px;font-weight:bold;clear:both;display:block;">Cash on Delivery Order</span>{/if}
            </p>
			{/if}
          </div> 
        
          <div class="foot"></div>
         <!-- <div class="subhead"></div>-->
         <!--
	     <div class="links"> 
            <p align="right"> 
			{if  ($smarty.get.type eq 'admin' && $source_type_id eq 11)}
              <input class="submit" type="submit" name="submitButton" value="print delivery challan"  border="0" onClick="javascript: window.print();">
			{elseif $posaccounttype eq 'SM' && $smarty.get.t eq "u"}
              <input class="submit" type="submit" name="submitButton" value="print delivery challan"  border="0" onClick="javascript: window.print();">
			{elseif ($posaccounttype eq 'P2' && $smarty.get.t eq "u")}
              <input class="submit" type="submit" name="submitButton" value="print order details"  border="0" onClick="javascript: window.print();">
			{else}
              <input class="submit" type="submit" name="submitButton" value="print invoice"  border="0" onClick="javascript: window.print();">
			{/if}

            </p>
        </div>
        -->
		{/if}
          <div> 
            <table cellpadding="0" cellspacing="0" class="printtable">
              <tr> 
                <td class="align_left">Date:</td>
                <td class="align_left">{$today}</td>
                <td  class="align_left border_left" colspan="2" >{$order.s_firstname} {$order.s_lastname}</td>
              </tr>
              <tr > 
                <td  class="align_left">Order Number:</td>
                <td class="align_left">{$orderid}</td>

                <td colspan="2" class="align_left border_left">{$address}{if $locality},&nbsp;{$locality}{/if},&nbsp;{$cityname},&nbsp; {$statename},<br>
                                                    {$countryname} Pincode-{$pincode}</td>
              </tr>
              <tr> 
                <td class="align_left">Contact:</td>
                <td class="align_left">{$user_mobile}</td>
                <td colspan="2" class="align_left border_left">{if $user_mobile neq $mobile }{$mobile}{else}&nbsp;{/if}</td>
              </tr>
              <tr> 
                <td class="align_left">Login:</td>
                <td class="align_left">{$userinfo.login}</td>
                <td colspan="2" class="align_left border_left">{if $userinfo.login neq $email }{$email}{else}&nbsp;{/if}</td>
              </tr>
              
              {if $userinfo.shop_bill_number}
              <tr>
                <td class="align_left">Shop Name/Shop Bill No.</td>
                <td class="align_left" colspan="3">{$userinfo.shop_bill_number}</td>
              </tr>
              {/if}
            </table>
          </div>
          
{*#############Cheque payment block################*}
{if $status =='Pending'}
<!--<div>
    <table cellpadding="2" class="printtable">
      <tr><td class="align_left" colspan=2><strong>Please fill in the following details</strong></td></tr>
      <tr>
	<td class="align_left">Cheque No</td>
	<td class="align_left"><input type="text" name="chqno" readonly></td>
      </tr>
      <tr>
	<td class="align_left">Issuing bank</td>
	<td class="align_left"><input type="text" name="bank" readonly></td>
      </tr>
      </table>
  </div>-->
 
<div>
    <table cellpadding="2" class="printtable">
      <tr><td class="align_left" colspan=2><strong>Payment instructions</strong></td></tr>
      <tr><td class="align_left" colspan=2><p style="color:#FF0033;"><strong>Please make the check payable to "Myntra Designs Pvt. Ltd." and mail at the address mentioned at the top  along with a copy of  invoice. Please also mention the "Order number {$orderid}" at the back of the check.<strong></p></td></tr>
      
      </table>
  </div>
  
  {/if}
{*############################################*}
         {if ($smarty.get.type neq "admin") or ($smarty.get.type eq 'admin' && $source_type_id eq 11)}
          <div> 
            <table cellpadding="2" class="printtable">

              <tr> 
                <td class="align_left"> 
                  <h3>Billing Address</h3>
                </td>
                <td class="align_left"> 
                  <h3>shipping Address</h3>
                </td>
              </tr>
              <tr> 
                <td> 
                  <p>{include file="mkbillingaddress.tpl" }</P></td>

                <td> 
                  <p>{include file="mkshippingaddress.tpl" }</p>
                </td>
              </tr>
            </table>
          </div>
	{/if}
	  {/if}
         {if $changestatus eq "Y"}
          {include file="print_order_changerequest.tpl" }
	  {/if}
	{if ($smarty.get.type eq "admin")}
		{include file="print_product_invoice.tpl" }		

	{else}
	    	   {include file="order_invoice_user.tpl" }		   
	 {/if}
	  {if $smarty.get.type eq 'admin' && $source_type_id eq 11}
	    	   {include file="order_invoice_user.tpl" }		   
	  {/if}
	  <!--
          <div class="links"> 
            <p align="right"> 
			{if  ($smarty.get.type eq 'admin' && $source_type_id eq 11)}
              <input class="submit" type="submit" name="submitButton" value="print delivery challan"  border="0" onClick="javascript: window.print();">
			{elseif ($posaccounttype eq 'P2' && $smarty.get.t eq "u")}
              <input class="submit" type="submit" name="submitButton" value="print order details"  border="0" onClick="javascript: window.print();">
			{elseif $posaccounttype eq 'SM' && $smarty.get.t eq "u"}
              <input class="submit" type="submit" name="submitButton" value="print delivery challan"  border="0" onClick="javascript: window.print();">
			{elseif $smarty.get.view eq "OD"}
              <input class="submit" type="submit" name="submitButton" value="print invoice"  border="0" onClick="javascript: window.print();">
			{else}
              <input class="submit" type="submit" name="submitButton" value="print invoice"  border="0" onClick="javascript: window.print();">
			{/if}
            </p>
          </div>
          <br/>
      -->
        </div>
      </div>
    </div>
    <div class="clearall"></div>
  </div>
  <div class="footer"> 
    <div class="footertext">&nbsp;</div>
    <div class="footercopy"></div>
  </div>
</div>
