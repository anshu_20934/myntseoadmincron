<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
{config_load file="$skin_config"}
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>
        {if $config.SEO.page_title_format eq "A"}
        {section name=position loop=$location}
        {$location[position].0|strip_tags|escape}
        {if not %position.last%} :: {/if}
        {/section}
        {else}
        {section name=position loop=$location step=-1}
        {$location[position].0|strip_tags|escape}
        {if not %position.last%} :: {/if}
        {/section}
        {/if}
</title>
{include file="meta.tpl" }
<link rel="stylesheet" href="{$SkinDir}/{#mkCSSFile#}" />

