{literal}
<script type="text/javascript" language="JavaScript 1.2">
function hideErrorDiv()
{
	document.getElementById('popup').style.visibility = 'hidden'; 
}

function hideWithDelay()
{
	setTimeout("hideErrorDiv()", 7000);
}

function showErrorDiv()
{
	document.getElementById('popup').style.visibility = 'visible'; 
}
</script>
{/literal}
{*******************Popup message for login************************}
{if $login_status == 'success'}
<div id="popup"> 
  <div id="overlay"> </div>
  <div id="message"> 
    <div class="wrapper"> 
      <div class="head"><a href="javascript:hideErrorDiv()">Click to close</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
      <div class="body"> 
        <p><strong>You have successfuly logged into Myntra.</strong><br>

          Welcome {$firstname}<br>
          <br>
          <a href="#">Privacy Policy</a> | <a href="#">Legal</a> | <a href="#">Site 
          Map</a></p>
      </div>
      <div class="foot"></div>

    </div>
  </div>
</div>

{literal}
<script type="text/javascript" language="JavaScript 1.2">
showErrorDiv();
hideWithDelay();
</script>
{/literal}
{/if}

{*******************Popup message for logout************************}

{if $logout_status == 'success'}
<div id="popup"> 
  <div id="overlay"> </div>
  <div id="message"> 
    <div class="wrapper"> 
      <div class="head"><a href="javascript:hideErrorDiv()">Click to close</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
      <div class="body"> 
        <p><strong>You have successfuly logged out from Myntra.</strong><br>

          Thank you for visiting Myntra<br>
          <br>
          <a href="#">Privacy Policy</a> | <a href="#">Legal</a> | <a href="#">Site 
          Map</a></p>
      </div>
      <div class="foot"></div>

    </div>
  </div>
</div>

{literal}
<script type="text/javascript" language="JavaScript 1.2">
showErrorDiv();
hideWithDelay();
</script>
{/literal}
{/if}