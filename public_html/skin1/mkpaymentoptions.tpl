<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
{config_load file="$skin_config"}
<html>
<head>
{include file="mkheadinfo.tpl"}
 {if $orgId != ""}
	<link rel="stylesheet" href="{$SkinDir}/{$org_css_file}" />
	<link rel="stylesheet" href="{$SkinDir}/modules/organization/css/orgaStyles.css" />
	<script type="text/javascript" src="{$SkinDir}/js_script/org_user_js.js"></script>
{/if}
<script type="text/javascript" src="{$SkinDir}/PopupWindow.js"></script>
<script type="text/javascript" src="{$SkinDir}/divpop_js.js"></script>
<script type="text/javascript" src="{$SkinDir}/js_script/redirecturl.js"></script>
<script type="text/javascript" src="{$SkinDir}/js_script/numberformat.js"></script>
<script type="text/javascript" src="{$SkinDir}/order_js.js"></script>
<script language="JavaScript" type="text/javascript" src="{$SkinDir}/shipping_calculate_ajax.js"></script>
<script type="text/javascript" src="{$SkinDir}/js_script/giftmessage.js"></script>
<script type="text/javascript" src="{$SkinDir}/js_script/email_validate.js"></script>
<script type="text/javascript" src="{$SkinDir}/js_script/ajaxObject.js"></script>
<link rel="stylesheet" href="{$SkinDir}/css/tooltip.css" />
<script type="text/javascript" src="{$SkinDir}/js_script/alttxt.js"></script>
<script type="text/javascript">
var shopmasterID='{$shopMasterId}';
</script>
 {literal}
<script type="text/javascript" language="JavaScript 1.2">

function enableAddress(option)
{
	if( option == 'b')
	{
		enableAddressEx('b');
		disableAddressEx('s');
	}
	else
	{
		enableAddressEx('s');
		disableAddressEx('b');
	}
}

function enableAddressEx(option)
{
	document.getElementById(option+"_firstname").readOnly = false;
	document.getElementById(option+"_lastname").readOnly = false;
	document.getElementById(option+"_address").readOnly = false;
	document.getElementById(option+"_country").readOnly = false;
	document.getElementById(option+"_state").readOnly = false;
	document.getElementById(option+"_city").readOnly = false;
	document.getElementById(option+"_zipcode").readOnly = false;
	document.getElementById(option+"_phone").readOnly = false;
	document.getElementById(option+"_mobile").readOnly = false;
	document.getElementById(option+"_email").readOnly = false;
}
function disableAddressEx(option)
{
	document.getElementById(option+"_firstname").readOnly = true;
        document.getElementById(option+"_lastname").readOnly = true;
        document.getElementById(option+"_address").readOnly = true;
        document.getElementById(option+"_country").readOnly = true;
        document.getElementById(option+"_state").readOnly = true;
        document.getElementById(option+"_city").readOnly = true;
        document.getElementById(option+"_zipcode").readOnly = true;
        document.getElementById(option+"_phone").readOnly = true;
        document.getElementById(option+"_mobile").readOnly = true;
        document.getElementById(option+"_email").readonly = true;
}
function verifyShipOption(){
    if(document.getElementById("cod") != null){
		if(document.getElementById('cod').checked!=true){
				
				document.getElementById('codcharge').style.display = 'none';
				document.getElementById('codtext').style.display = 'none';
				document.getElementById('billingaddress').style.display = '';
				document.getElementById('billinglabel').style.display = '';

				if(document.getElementById('s_option').selectedIndex ==2){
				document.getElementById('s_option').selectedIndex = 0;            
				}
		}
		else{
				
				document.getElementById('codcharge').style.display = '';
				document.getElementById('codtext').style.display = '';
				document.getElementById('billingaddress').style.display = 'none';
				document.getElementById('billinglabel').style.display = 'none';

				document.getElementById('s_option').selectedIndex = 2;

		}
    }
	else{
            if(document.getElementById('s_option').selectedIndex ==2){
            document.getElementById('s_option').selectedIndex = 0;            
            }
	}
	
}



function hideSuccessDiv(){
	document.getElementById('messagepopup').style.visibility = 'hidden';
}
function hideWithDelay(){
	setTimeout("hideSuccessDiv()", 7000);
}
function showMessagePopup(){
	document.getElementById('popup').style.visibility = 'visible';
	document.getElementById('popup').style.display = '';
}
function hideMessageDiv(){
	document.getElementById('popup').style.visibility = 'hidden';
	document.getElementById('popup').style.display = 'none';
}

function callShippingMethod(callee){
	if(callee == 'shop')
	{
		document.getElementById("s_firstname").value = document.getElementById("h_firstname").value;
                document.getElementById("s_lastname").value = document.getElementById("h_lastname").value;
                document.getElementById("s_address").value = document.getElementById("h_address").value;
                document.getElementById("s_country").value = document.getElementById("h_country").value;
                document.getElementById("s_state").value = document.getElementById("h_state").value;
                document.getElementById("s_city").value = document.getElementById("h_city").value;
                document.getElementById("s_zipcode").value = document.getElementById("h_zipcode").value;
                document.getElementById("s_phone").value = document.getElementById("h_phone").value;
                document.getElementById("s_mobile").value = document.getElementById("h_mobile").value;
                document.getElementById("s_email").value = document.getElementById("h_email").value;
		
	}else{
		document.getElementById("s_firstname").value = '';
	        document.getElementById("s_lastname").value = '';
        	document.getElementById("s_address").value = '';
	        document.getElementById("s_country").value = '';
	        document.getElementById("s_state").value = '';
	        document.getElementById("s_city").value = '';
	        document.getElementById("s_zipcode").value = '';
	        document.getElementById("s_phone").value = '';
	        document.getElementById("s_mobile").value = '';
	        document.getElementById("s_email").value = '';
	}
}

</script>
{/literal}
</head>
<body id="body" >
{literal}
<script type="text/javascript" language="JavaScript 1.2">
hideWithDelay();
</script>
{/literal}
<div id="view_shipping_details"></div>

<div id="scroll_wrapper">
<div id="container">
{include file="mykrititop.tpl" }
<div id="wrapper" >
 {if $orgId != ""}
	{include file="modules/organization$brandShopDir/breadcrumb_organization.tpl"}
{else}
	{include file="breadcrumb.tpl" }
{/if}

    <div id="display">
	<!-- start left panel -->
	{if $orgId != ""}
		{ include file="modules/organization$brandShopDir/orgInviteeLeftLinks.tpl" }
	{else}
		{if $affiliateleftpanel != ''}
		
		<div class="menu_column">{include file="$affiliateleftpanel" }</div>
		{else}
		<div class="menu_column">{include file="leftpaneMyAccount.tpl" }</div>

		{/if}

	{/if}
      <!-- end -->
		<div class="center_column">
		<form name="paymentform" action="mkorderinsert.php" method="POST">
		<input type="hidden" name="is_shipping_cost_set" id="is_shipping_cost_set" {if $s_city != null}value="1" {else}value="0"{/if}>
		<input type="hidden" name="checksumkey" id="checksumkey" value="{$checksumkey}">
			<div class="form">
		
              <div class="subhead">
                <p>address details</p>
              </div>
          <div>
		<table cellpadding="2" class="table">

	{if $accountType == 1}
              <tr>
                <td class="align_left">
                	<font style="font-size:1.5em"><b>shipping address</b></font>
		 </td>                
                <td class="align_left">
                  <span id="billinglabel">
                	<table><tr><td>
		                  <font style="font-size:1.0em"><b>billing address</b></font>
                		  </td>
                  		  <td>
			                  <table><tr><td>
				                   &nbsp;&nbsp;&nbsp;
					             </td>
						 </tr>
  					  </table>
                  </span>
                  		  </td>
                  		</tr>
		        </table>
		 </td>               
              </tr>
	<tr>
<td><table><tr><td><input type="radio" name="group1" value="shop" onclick="javascript:callShippingMethod('shop');" {if $shopMasterId eq 41}disabled{else}checked{/if}> ship to {if $shopname!=''} {$shopname} {else}shop{/if}<br>
<input type="radio" name="group1" value="customer" onclick="javascript:callShippingMethod('customer');" {if $shopMasterId eq 41}checked{/if}> ship to customer<br>
                   &nbsp;&nbsp;&nbsp;
                  </td></tr></table></td>{if $paymentoptionforpos eq 'ONL'}<td style="width:400px;">You have opted for online payment.<br> Please enter billing address for your payment mode.<td>{/if}
	</tr>
		<tr>
			<td></td>
			<td>
			{if $paymentoptionforpos == 'ONL' || $billingeditable eq 1}
                  <table><tr><td>
                   &nbsp;&nbsp;&nbsp;<input type="checkbox" name="billtoship" id="billtoship" value="btoship"
			class="checkbox" onclick="javascript:flushCall();" > <b>same as shipping</b>
				  </tr></td></table>
			{/if}
			</td>
		</tr>
              <tr>
                <td>
                 <p> {include file="mkshippingselectedpos.tpl" }</P>
                </td>
	        <td>
                    <span id="billingaddress" > <p> {include file="mkbillingselectedpos.tpl" }</P> </span>
                </td>
             </tr>
	{else}
              <tr>
                <td class="align_left">
                	<font style="font-size:1.5em"><b>shipping address</b></font>
		        </td>                
                <td class="align_left">
                  <span id="billinglabel">
                	<table><tr><td>
                  <font style="font-size:1.0em"><b>billing address</b></font>
                  </td>
                  <td>
                  <table><tr><td>
                   &nbsp;&nbsp;&nbsp;<input type="checkbox" name="billtoship" id="billtoship" value="btoship"
			class="checkbox" onclick="javascript:flushCall();" {if $ship2diff == 'Y'}checked{/if}>
        &nbsp;<font style="font-size:0.8em">same as shipping</font>
                  </td></tr></table>
                  </span>
                  </td>
                  </tr>
		        </table>
		       </td>               
              </tr>
              <tr>
                <td>
                 <p> {include file="mkshippingselected.tpl" }</P>
                </td>
		        <td>
                    <span id="billingaddress" > <p> {include file="mkbillingselected.tpl" }</P> </span>
                </td>
            </tr>
	{/if}
	<input type=hidden id=captureuserdetails value="{$captureuserdetails}" name=captureuserdetails />
	{if $captureuserdetails eq 1}
 {literal}
<script type="text/javascript" language="JavaScript 1.2">
function b2c(){
		if(document.getElementById("b2ci").checked == true){
			document.getElementById("c_firstname").value = document.getElementById("b_firstname").value;
			document.getElementById("c_lastname").value = document.getElementById("b_lastname").value;
			document.getElementById("c_address1").value = document.getElementById("b_address").value;
			document.getElementById("c_country").value = document.getElementById("b_country").value;
			document.getElementById("c_state").value = document.getElementById("b_state").value;
			document.getElementById("c_city").value = document.getElementById("b_city").value;
			document.getElementById("c_zip").value = document.getElementById("b_zipcode").value;
			document.getElementById("c_alt_number").value = document.getElementById("b_phone").value;
			document.getElementById("c_mobile").value = document.getElementById("b_mobile").value;
			document.getElementById("c_email").value = document.getElementById("b_email").value;
			document.getElementById("s2ci").checked = false;
		}

}
function s2c(){
		if(document.getElementById("s2ci").checked == true){
			document.getElementById("c_firstname").value = document.getElementById("s_firstname").value;
			document.getElementById("c_lastname").value = document.getElementById("s_lastname").value;
			document.getElementById("c_address1").value = document.getElementById("s_address").value;
			document.getElementById("c_country").value = document.getElementById("s_country").value;
			document.getElementById("c_state").value = document.getElementById("s_state").value;
			document.getElementById("c_city").value = document.getElementById("s_city").value;
			document.getElementById("c_zip").value = document.getElementById("s_zipcode").value;
			document.getElementById("c_alt_number").value = document.getElementById("s_phone").value;
			document.getElementById("c_mobile").value = document.getElementById("s_mobile").value;
			document.getElementById("c_email").value = document.getElementById("s_email").value;
			document.getElementById("b2ci").checked = false;
		}

}
</script>
{/literal}
            <tr><td colspan="2" style="margin-top:15px;"></td></tr>
            <tr>
                <td>
            	    <br>
	                <hr color="#999999" size="1">
	                <br>
	                     <p><strong style="font-size:1.5em">Customer Details</strong>&nbsp;&nbsp;</p><br/>
	            </td>
	            {if $orgId == ""}
	            <td >
	                <br>
	                <hr color="#999999" size="1">
	                <br>
                    <p><strong style="font-size:1.5em">Payment Method</strong>&nbsp;&nbsp;</p><br/>
                </td>
                {/if}
			</tr>
			<tr>
				<td width="50%">
					<p>
						<table>
							<tbody>
                  				<tr><td>
				                   <input type="checkbox" name="s2ci" id="s2ci" value="btoship"
									class="checkbox" onclick="javascript:s2c();" > 
									<b>same as shipping</b>
								</tr></td>
                  				<tr><td>
				                   <input type="checkbox" name="b2ci" id="b2ci" value="btoship"
									class="checkbox" onclick="javascript:b2c();" > 
									<b>same as billing</b>
								</tr></td>
								<tr>
									<td>
										<font style="font-size:0.8em;">first name<span class=mandatory>*</span></font>
									</td>
									<td>
										<input type="text" name="c_firstname" id=c_firstname />
									</td>
								</tr>
								<tr>
									<td>
										<font style="font-size:0.8em;">last name<span class=mandatory>*</span></font>
									</td>
									<td>
										<input type="text" name="c_lastname" id=c_lastname />
									</td>
								</tr>
								<tr>
									<td>
										<font style="font-size:0.8em;">address 1</font>
									</td>
									<td>
										<textarea class = address name=c_address1 id=c_address1></textarea>
									</td>
								</tr>
								<tr>
									<td>
										<font style="font-size:0.8em;">country</font>
									</td>
									<td>
										<select name=c_country class=select id=c_country>
 											{foreach key=countrycode item=countryname from=$returnCountry}
   												<option value="{$countrycode}" >{$countryname}</option>
											{/foreach} 					
										</select>
									</td>
								</tr>
								<tr>
									<td>
										<font style="font-size:0.8em;">state</font>
									</td>
									<td>
										<select name=c_state class=select id=c_state>
 											{section name="statename" loop=$returnState}
 												<option value={$returnState[statename].code}>{$returnState[statename].state}</option>
								 			{/section}
										</select>
									</td>
								</tr>
								<tr>
									<td>
										<font style="font-size:0.8em;">city</font>
									</td>
									<td>
										<input type="text" name="c_city" id=c_city />
									</td>
								</tr>
								<tr>
									<td>
										<font style="font-size:0.8em;">zip/postal code</font>
									</td>
									<td>
										<input type="text" name="c_zip" id=c_zip />
									</td>
								</tr>
								<tr>
									<td>
										<font style="font-size:0.8em;">phone</font>
									</td>
									<td>
										<input type="text" name="c_alt_number" id=c_alt_number />
									</td>
								</tr>
								<tr>
									<td>
										<font style="font-size:0.8em;">mobile<span class=mandatory>*</span></font>
									</td>
									<td>
										<input type="text" name="c_mobile" id=c_mobile />
									</td>
								</tr>
								<tr>
									<td>
										<font style="font-size:0.8em;">email</font>
									</td>
									<td>
										<input type="text" name="c_email" id=c_email />
									</td>
								</tr>
							</tbody>
						</table>
					</p>
				</td>
				{if $orgId == ""}
				<td width="50%"  valign="top">
					<p style="padding-left:20px">
						<table>
							<tbody>

								 <tr>
									<td colspan="2">
										<p style="font-size:0.8em;">Please select the payment method that the customer has used to place this order</p><br/>
									</td>
								</tr>
								<tr>
									<td width="30%">
										<font style="font-size:0.8em;">Cash</font>
									</td>
									<td>
								        <input type="radio" name="payment_method_pos" value="cash" checked="checked" />
									</td>
								</tr>
								<tr>
									<td>
										<font style="font-size:0.8em;">Cheque</font>
									</td>
									<td>
								        <input type="radio" name="payment_method_pos" value="cheque"/>
									</td>
								</tr>
								<tr>
									<td>
										<font style="font-size:0.8em;">Credit Card</font>
									</td>
									<td>
								        <input type="radio" name="payment_method_pos" value="creditcard"/>
									</td>
								</tr>
								<tr>
									<td>
										<font style="font-size:0.8em;">Voucher</font>
									</td>
									<td>
								        <input type="radio" name="payment_method_pos" value="voucher"/>
									</td>
								</tr>
								<tr>
									<td>
										<font style="font-size:0.8em;">Credit</font>
									</td>
									<td>
								        <input type="radio" name="payment_method_pos" value="credit"/>
									</td>
								</tr>
								<tr>
									<td>
										<font style="font-size:0.8em;">&nbsp;</font>
										
									</td>
									<td>
									</td>
								</tr>
							</tbody>
						</table>
					</p>
				</td>
				{/if}
			</tr>
	{/if}
            <tr><td colspan="2" style="margin-top:15px;"></td></tr>
            <tr>
                <td colspan="2">
                <br>
                <hr color="#999999" size="1">
                <br>
                     <p><strong style="font-size:1.5em">contact number for issues</strong>&nbsp;&nbsp;<input type="text" name="issues_contact_number" id="issues_contact_number" value="{$userinfo.mobile}"></p><br/>
                     <p style="font-size:0.8em;color:#F40000;">(This number will be used to contact if there are any issues in fulfilling this order. 
					 {if $RRtype!=1 }	 If this is a gift, please DO NOT enter the gift recipient number here as it may spoil the surprise! ){/if}
					</p>
                </td>
            </tr>
            {if $userinfo.account_type eq 'PU' && $POStype != 1 && $KIOSKtype != 1}
                 <tr>
                    <td colspan="2">
                    <br>
                    <hr color="#999999" size="1">
                    <br>
                         <p><strong style="font-size:1.5em">Enter Order ID</strong>&nbsp;&nbsp;
                        <input type="text" name="order_number" id="order_number" value="" ></p>
            			<input type="hidden" name="cust_account_type" id="cust_account_type" value="{$userinfo.account_type}">
                   </td>
                 </tr>
            {/if}
            <tr><td colspan="2" style="margin-top:15px;"></td></tr>
                        
            <!--delivery preference-->
            <input type="hidden" name="shipment_preferences" id="shipment_preferences" value="shipped_together"/>
            <!--delivery preference-->
        	
            <!--imint-->
            <tr>
	        {if $accountType == 1}
		<td colspan="2">		
			<br>
			<p><strong style="font-size:1.5em">enter shop name/shop bill number</strong>&nbsp;&nbsp;
                        <input type="text" name="shop_bill_number" id="shop_bill_number" value="" ><br/><br/><br/>
						<strong style="font-size:1.5em">enter sales personel name</strong>&nbsp;&nbsp;
                        <input type="text" name="c_order_personel" id="c_order_personel" value="" ></p>
		</td>
		{elseif $RRtype!=1 && $orgId ne '28'}
                <td colspan="2">
                <br>
                <hr color="#999999" size="1">
                <br>
                    <p>
                        <strong style="font-size:1.5em">earn imint reward points</strong>&nbsp;&nbsp;
                        <div style="margin-left:10px;">I want to earn <img style="border:0" src="{$cdn_base}/skin1/mkimages/logo_imint.gif"/> reward points on <input type="text" style="color:grey;" name="imint_card" value="" size="25" /></div>
                        <div style="margin:10px 10px;color:red;font-size:0.8em;">* Members of privileges program from Airtel powered by Imint, key in your 10 digit Airtel mobile no. to earn reward points.</div>
                    </p>
                </td>
		{/if}
            </tr>
            <!--imint-->	        
        </table>
       
	 
{*#####################################################*}
<div class="foot"></div>
</div>
            
<input name='s_option' id ='s_option' type="hidden" value ='{$shippingoptions[0].code}'/>

{assign var="paycheckforpos" value="true"}
{assign var="shopmasterlogged" value="false"}
{if $shopmasterloggedin eq 'true'}
	{assign var="shopmasterlogged" value="true"}
{/if}
{if $RRtype==1 }			  
<!Rewards Customer: Not need of showing other payment options>
	<input type="hidden" name="payby" id="paymentoptionforRR" value="RRP">
    <div style="visibility:hidden;display:none">
	<div class="subhead"><p>payment options&nbsp;<a href="javascript:messagePopup('payment');" style="text-decoration:none;font-size:0.7em;"><img src="./skin1/mkimages/help-icon.gif" style="border:0px;"/></a></p></div>
	<div class="links"><p><input type="radio" name="payby" id="RRP" class="radio" value="RRP" checked>&nbsp;Reward Points</p></div>
	<div class="foot"></div>
    </div>
{else}
	{if $paymentoptionforpos eq 'INM' || $paymentoptionforpos eq 'PPS' || $paymentoptionforpos eq 'ONL'}
	{assign var="paycheckforpos" value="false"}
	<div style="visibility:hidden;display:none">
	{/if}
	
	<div class="subhead"><p>payment options&nbsp;<a href="javascript:messagePopup('payment');" style="text-decoration:none;font-size:0.7em;"><img src="./skin1/mkimages/help-icon.gif" style="border:0px;"/></a></p></div>
		{if $POStype == 1 || $KIOSKtype == 1}
		<div class="links"><p><input type="radio" name="payby" id="inm" class="radio" value="inm" checked>&nbsp;Invoice Me</p></div>
		<div class="links"><p><input type="radio" name="payby" id="net" class="radio" value="net" disabled>&nbsp;net banking /debit cart/credit card/itz cash card/paymate</p></div>
		<div class="links"><p><input type="radio" name="payby" id="ch" class="radio" value="chq" disabled>&nbsp;check <!--<span class='mandatory' style='font-size:1.1em;'>(For Holi orders, please make sure we receive the check by Mar-17. We can ensure delivery before Holi, only if we receive the check by Mar-17.)</span>--></p></div>
		{else}
	    <div class="links">
			<p><input type="radio" name="payby" id="net" class="radio" value="net" {if $cod_enabled} onchange="javascript:verifyShipOption();javascript:updateShipping();"{/if} checked>&nbsp;pay by net-banking/mobile/debit/credit(Amex/JCB)/cash card</p>
		</div>
		<div class="links">
			<p><input type="radio" name="payby" id="ebs" class="radio" value="ebs" checked>&nbsp;pay by debit/credit(Visa/Master/Diners) card</p>
		</div>
			{if $relianceIP ne 1}
			<div class="links"><p><input type="radio" name="payby" id="ch" class="radio" value="chq" {if $cod_enabled} onchange="javascript:verifyShipOption();javascript:updateShipping();" {/if}>&nbsp;check 
			<!--<span class='mandatory' style='font-size:1.1em;'>(For Holi orders, please make sure we receive the check by Mar-17. We can ensure delivery before Holi, only if we receive the check by Mar-17.)</span>-->
			</p></div>
				{if $cod_enabled}
				<div class="links"><p><input type="radio" name="payby" id="cod" class="radio" value="cod" onchange="javascript:verifyShipOption();javascript:updateShipping();">&nbsp;cash on delivery 
				<!--<span class='mandatory' style='font-size:1.1em;'>(For Holi orders, please make sure we receive the check by Mar-17. We can ensure delivery before Holi, only if we receive the check by Mar-17.)</span>-->
				</p></div>
				{/if}
			{/if}
			{if $imint_enabled}
			<div class="links"><p><input type="radio" name="payby" id="imint" class="radio" value="imint">&nbsp;redeem your <img style="border:0" src="{$cdn_base}/skin1/mkimages/logo_imint.gif"/> reward points</p></div>
			{/if}	
		{/if}
	<input type="hidden" name="kotakdiscount" id="kotakdiscount" value="0">
	<input type="hidden" name="optioncount" id="optioncount" value="0">
	<input type="hidden" name="codoptioncount" id="codoptioncount" value="0">
	<div class="foot"></div>
	
	{if $paymentoptionforpos eq 'INM' || $paymentoptionforpos eq 'PPS' || $paymentoptionforpos eq 'ONL'}
	<input type="hidden" name="payby" id="paymentoptionforpos" value="{$paymentoptionforpos}">
	</div>
	{/if}
{/if}
{*#####################################################*}

{*******************************************************}                    
<div class="subhead"><p>order summary</p></div>
 <div>
<table cellpadding="2"  class="table">

{if !($shippingRate == 0 && $vat == 0 && ( $allInclusivePricing==1))}
<tr class="font_bold">
<td class="align_right" colspan="9">amount</td>
<td class="align_right" colspan="1">Rs.{$orderTotal|string_format:"%.2f"}</td>
</tr>
{/if}

<input type="hidden" name="couponcode" id="couponcode" value="{$couponCode}">
{if ($coupondiscount != "") && ($coupondiscount > 0)}
<tr class="font_bold">
<td class="align_right" colspan="9">coupon discount(coupon code: {$couponCode}({$couponpercent}))</td>
<td class="align_right" colspan="1">Rs.{$coupondiscount|string_format:"%.2f"}</td>
</tr>
{/if}


{if !($vat == 0 && $accountType == 1)}
<tr class="font_bold" {if $allInclusivePricing==1} style="display:none;"{/if}>
	<input type="hidden" name="hidevat" id="hidevat" value="{$vat}">
	<td class="align_right" colspan="9">vat/cst</td>
	<td class="align_right" colspan="1"><div id="vatdiv">Rs.{$vat|string_format:"%.2f"}</div></div>
	</td>
</tr>
{/if}

{if !($shippingRate == 0 && ($accountType == 1 || $allInclusivePricing==1))}
 <tr class="font_bold">
	<td class="align_right" colspan="9">shipping cost</td>
	<td class="align_right" colspan="1"><div id="shippingcost"></div>
	<div id="setshippingcost" >Rs.{$shippingRate|string_format:"%.2f"}</div>
	</td>
 </tr>
{/if}

{if $ammaGUID ne '' && $ammaAmount ne ''}
<input type="hidden" name="ammaGUID" id="ammaGUID" value="{$ammaGUID}">
<input type="hidden" name="ammaDiscount" id="ammaDiscount" value="{$ammaAmount}">
<input type="hidden" name="ammaOrderId" id="ammaOrderId" value="{$ammaOrderId}">
  <tr class="font_bold">
	<td class="align_right" colspan="9">payment made at ammas.com</td>
	<td class="align_right" colspan="1">Rs.{$ammaAmount|string_format:"%.2f"}</td>
 </tr>
{/if}

{if !($shippingRate == 0 && $vat == 0 && ($accountType == 1 || $allInclusivePricing==1))}
<!--added for c&w for the order which has shipping cost(for some product)
	in case of shipping cost in c&w product do not show amount 
	to be paid since is redundant to final amount to be paid-->
<tr class="font_bold" {if $allInclusivePricing==1} style="display:none;"{/if}>
	<td class="align_right" colspan="9">amount to be paid</td>
	<td class="align_right" colspan="1"><div id="totalamountajax"></div>
	<div id="settotalamountajax">Rs.{$amountAfterDiscount|string_format:"%.2f"}</div>
	</td>
</tr>
{/if}
<tr class='font_bold' id='codrow' >
	<input type='hidden' name='cash' id='cash' value="{$cod_charges}">
	<td class='align_right' id='codtext' style='display:none;' colspan='9'>cash on delivery charges</td>
	<td class='align_right' id='codcharge' style='display:none;' colspan='1'>Rs.{$cod_charges}</td>
</tr>


{if $RRtype!=1 }		  
<tr class="font_bold">
	<td  class="align_left" colspan="8"  valign="top">
		<input type="checkbox" class="checkbox" name="asgift" id="asgift" onClick="javascript:messagBlock('{$login}');">
		&nbsp;<strong>send as gift</strong>&nbsp;<a href="javascript:messagePopup('gift');" style="text-decoration:none;font-size:0.7em;"><img src="./skin1/mkimages/help-icon.gif" style="border:0px;"/></a>
		<br><textarea name="gmessage" id="messageblock" style="width:300px;visibility:hidden;display:none;" onblur="if(this.value=='') this.value=this.defaultValue;" onfocus="if(this.value==this.defaultValue) this.value='';" >type a gift message here...</textarea>
	</td>
	<td  class="align_right" ><strong>gift packaging charges</strong></td>
	<input type="hidden" name="giftpack_charges" id="giftpack_charges" value="{$giftpack_charges}">
	<input type="hidden" name="giftamount" id="giftamount" value="0.00">
	<td  class="align_right" ><strong><div id="giftprice">Rs.0.00</div></strong></td>
</tr>
{/if}

<!-- showing referral amount  -->
{if $refoption}
	   <tr class="font_bold">
		<input type="hidden" name="isexistref" id="isexistref" value="1">
		<input type="hidden"  class="ref" name="refamt" id="refamt" value="{$ref_money}"  readonly/>
		<input type="hidden" name="hideref" id="hideref" value="0.00">
		<td  class="align_left" colspan="8"  >
		<input type="checkbox" name="refcheck" id="refcheck" class="checkbox" onClick="javascript: deductRefAmount('{$ref_money}',document.getElementById('Amount').value,'{$vat}',document.getElementById('shippingrate').value)" >
			&nbsp;use referal amount (current referal money<span id="refamtdiv"> Rs.{$ref_money}</span>)
		</td>
		<td  class="align_right" colspan="1"><strong>referral discount</strong></td>
		<td  class="align_right" colspan="1"><strong><div id="refdiscount"> Rs.0.00</div></strong></td>
	  </tr>

	<tr class="font_bold" style="font-size:1.2em;color:#FF0000;">
		<td class="align_right" colspan="9">final amount to be paid</td>
		<td class="align_right" colspan="1"><!--<div id="totalamountajax"></div> -->
		<div id="totalamtafterdeduction">Rs.{$amountAfterDiscount|string_format:"%.2f"}</div>
		</td>
	</tr>
{else}
	<input type="hidden" name="isexistref" id="isexistref" value="0">
	{if $POStype eq 1}
	<tr class="font_bold" style="font-size:1.2em;">
		<td class="align_right" colspan="9">final amount to be paid</td>
		<td class="align_right" colspan="1">
		<div id="totalamtaftergiftpack">Rs.{$amountAfterDiscount|string_format:"%.2f"}</div>
		</td>
	</tr>
	<tr class="font_bold" style="font-size:1.2em;color:#FF0000;">
		<td class="align_right" colspan="9">amount to be charged from Customer</td>
		<td class="align_right" colspan="1">
		<input type="hidden" name="finalcostwithoutshippingandgift" id="finalcostwithoutshippingandgift" value= "{$amountWithoutDiscountMinusShipping}"/>
		<div id="totalamtaftergiftpackwodiscount">Rs.{$amountWithoutDiscount}</div>
		</td>
	</tr>
	{else}
	<tr class="font_bold" style="font-size:1.2em;color:#FF0000;">
		<td class="align_right" colspan="9">final amount to be paid</td>
		<td class="align_right" colspan="1">
		<div id="totalamtaftergiftpack">Rs.{$amountAfterDiscount|string_format:"%.2f"}</div>
		</td>
	</tr>
		<input type="hidden" name="finalcostwithoutshippingandgift" id="finalcostwithoutshippingandgift" value= "{$amountWithoutDiscountMinusShipping}"/>
		<div id="totalamtaftergiftpackwodiscount" style="visibility:hidden;display:none">Rs.{$amountWithoutDiscount}</div>
	{/if}
{/if}

{if $RRtype==1}{*$availablePoints>0*}
	<input type="hidden" id="availablepoints_hidden" name="availablepoints_hidden" value="{$availablePoints}"/>
		<tr class="font_bold" style="color:green;font-size:1.2em">
			<td class="align_right" colspan="9">Reward Points Available</td>
			<td class="align_right" colspan="1">
			<div id="availablepoints">Rs.{$availablePoints|string_format:"%.2f"}</div>
			</td>
		</tr>
		<tr class="font_bold" >
			<td class="align_right" colspan="9">Points Remaining after purchase</td>
			<td class="align_right" colspan="1">
			<div id="remainingpoints">Rs.{$remainingPoints|string_format:"%.2f"}</div>
			</td>
		</tr>
{/if}

{if $offers}
      <tr class="font_bold" id="offer_999" style="display:block;">
		<td class="align_right" colspan="10"><span class="mandatory"><strong>{$offers.OFFER_999}</strong></span></td>
      </tr>
 {/if}
	<tr>
		<td  colspan="10" class="align_right" >
		<input type="hidden" name="Amount" id="Amount" value="{$Amount|string_format:"%.2f"}">
		{if $ibibophotodiscount}
		  <input type="hidden" name="ibibophotodiscount" id="ibibophotodiscount" value="{$ibibophotodiscount|string_format:"%.2f"}">
		{/if}
		<!--added for c&w to check for negative account balance after shipping cost is added-->
		{*if $RRtype==1*}
			<!--<input class="submit" type="button"  id="submitButton" name="submitButton"	value="{$paymentButtonText}" border="0" style="font-size:1.1em;width:150px;" onClick="javascript:if(checkAccountBalanceAfterShip(document.getElementById('availablepoints_hidden').value),document.getElementById('totalamount').value,document.getElementById('shippingrate').value){ldelim}goToPaymentOption('{$pagetype}',{$paycheckforpos},{$shopmasterlogged});{rdelim}"/>-->
		{*else*}
			<input class="submit" type="button"  id="submitButton" name="submitButton"	value="{$paymentButtonText}" border="0" style="font-size:1.1em;width:150px;" onClick="goToPaymentOption('{$pagetype}',{$paycheckforpos},{$shopmasterlogged})"/>
		{*/if*}
		</td>
	 </tr>
	 
</table>
</div>
<script >
     // deductRefAmount('{$ref_money}', '{$totalAfterDiscount}','{$vat}');
</script>

<div class="foot"></div>
</form>
	  </div>
   </div>
  </div>
<div class="clearall"></div>
</div>
{include file="footer.tpl" }
</div>
</div>

<!--#### access popup ###-->
{if $show_access_login_popup}
<div id="autologin_popup" style="display:block;visibility:visible">
  <div id="overlay" > </div>
  <div id="messagewide" style="top:200px;">
    <div class="wrapper" style='top:-17%;'>
      <!--div class="head">&nbsp;</div-->
      <div class="body" style="height:200px;padding-top:50px;border-top:5px solid #000000;border-bottom:5px solid #000000;">
	<span> Please enter your access code </span>
		<div align="center" style='padding-top:50px;'>
			<form name="valid_access_form" method="post" action="{$action}">
				<input type="password" name="access_code"/><br/><br/>
				<input type="submit" name="validate_access_code" value="Go"/>
			</form><br/>
			<div id="emailmsg" style="height:auto;text-align:center;color:red">{$validity_message}</div>
		</div>
      </div>
      <!--div class="foot"></div-->
    </div>
  </div>
</div>
{/if}
<!--#### access popup ###-->


<div id="giftpopup" style="visibility:hidden;display:none">
  <div id="overlay"> </div>
  <div id="messagewide">
    <div class="wrapper" style="_top:150px">
      <div class="head"><a href="javascript:hidegiftMessageDiv()" >Click to close</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
      <div class="body"><p>{$giftmess}</p></div>
      <div class="foot"></div>
    </div>
  </div>
</div>

<div id="messagepopup" style="visibility:hidden;display:none">
  <div id="overlay"> </div>
  <div id="message" style="position:relative; left:0px; top:60px;">
    <div class="wrapper">
      <div class="head"><a href="#" onclick="javascript:hideSuccessDiv()">Click to close</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
      <div class="body">
        <p><strong>Shipping cost has been updated to reflect the </p>
	<p><strong>change in your shipping address</strong>
          <br>
          <br>
          </p>
      </div>
      <div class="foot"></div>
    </div>
  </div>
</div>


<div id="paymentoption" style="visibility:hidden;display:none">
  <div id="overlay"> </div>
  <div id="messagewide">
    <div class="wrapper" style="_top:150px">
      <div class="head"><a href="javascript:hidepaymentMessageDiv()" >Click to close</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
      <div class="body">
        <p>
		<table style="font-size: 1.0em;font-weight:100;" border=0 width="95%">
		<tr><td><p><strong>net banking / credit card / itz cash card / paymate:</strong></p></td></tr>
		<tr><td><p><b>-</b> use your net enabled bank account from any major bank to pay directly from your bank account</p></td></tr>
		<tr><td><p><b>-</b> use your VISA, Diners or American Express credit card to make the payment</p></td></tr>
		<tr><td><p><b>-</b> If you have a paymate account, you can simply make the payment using your mobile phone number.</p></td></tr>
		<tr><td><p><b>-</b> using cash card option, you can use your pre-paid ITZ CASH card.</p></td></tr>
		
		<tr><td><p><strong>check:</strong> you can pay by check. Select this option, complete the order and mail the check along with the order receipt.</p></td></tr>
	
		{if $cod_enabled}<tr><td><p><strong>cash on delivery:</strong> If you select this method, we will ship your order directly to you and the payment will be collected from you at the time of delivery.</p></td></tr>{/if}
		</table>
	</p>
      </div>
      <div class="foot"></div>
    </div>
  </div>
</div>

<!--popup-->
<div id="rrpopup" style="display:none;visibility:hidden;">
	<div class="top" style="display:inline-block;filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='{$http_location}/skin1/modules/organization/images/cw/popuptop.png');">
		<img style="border:0px;filter:progid:DXImageTransform.Microsoft.Alpha(opacity=0);" src="{$http_location}/skin1/modules/organization/images/cw/popuptop.png"/>
	</div>
	<div class="middle">
		<div class="close"><img src="{$cdn_base}/skin1/modules/organization/images/cw/cross.jpg" onclick="closeRRPopup('rrpopup');"/></div>	   	    		
		<p class="parahead">Redeem Message</p>
		<p class="paradetail" id="redeem_msg"></p>    							 	    		   	    		
	</div>
	<div class="bottombroad" style="display:inline-block;filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='{$http_location}/skin1/modules/organization/images/cw/popupbottombroad.png');">
		<img style="border:0px;filter:progid:DXImageTransform.Microsoft.Alpha(opacity=0);" src="{$http_location}/skin1/modules/organization/images/cw/popupbottombroad.png"/>
	</div>
</div>   	    
<!--popup-->

<div id="navtxt" class="navtext" style="position:absolute; top:-100px; left:0px; visibility:hidden"></div>
</body>
</html>
