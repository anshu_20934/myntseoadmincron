<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html xml:lang="en" xmlns:fb="http://www.facebook.com/2008/fbml">
<head>
<META http-equiv="Content-Style-Type" content="text/css">
<META http-equiv="Content-Script-Type" content="type/javascript">
<META http-equiv="Content-Type" content="text/html; charset=utf-8">
{include file="site/header.tpl" }
<!--[if lt IE 8]>
    <link rel="stylesheet" href="new-skin-css/ie.css" type="text/css" media="screen, projection"><![endif]-->

</head>

<body>
<div class="container clearfix">
	{include file="site/menu.tpl" } 

	<h1 class="page-title">Sitemap</h1>	
	<hr class="space clear mb10">
    <div class="content clearfix">
    	<div class="product-sitemaps">
		{if $condition eq 1 }
			<p>
			{section name=idx loop=$arr_display}
				<a href='{$http_location}/productsitemaps/{$arr_display[idx]}-0'>{$arr_display[idx]}</a>
			{/section}
			</p>
		{elseif $condition eq 2 }
			<p>
			{section name=idx loop=$arr_display}
				<a href='{$http_location}/productsitemaps/0-{$arr_display[idx]}'>{$arr_display[idx]}</a>
			{/section}
			</p>
		 {elseif $condition eq 3}
			  <p class="recent-searches">All Recent searches {$string_last}-{$string_last_offset}</p>
			  <table>
				<tr>
					<td>
						{section name=idx loop=$results}
						{assign var=half value=$smarty.section.idx.total/2}	
							{if $smarty.section.idx.index neq $half }
								<a href='{$http_location}/{$results[idx].product|replace:" ":"-"}/{$results[idx].typename|replace:" ":"-"}/FC/PD/{$results[idx].productid}'>{$results[idx].product} {$results[idx].typename}</a>&nbsp;&nbsp;	
							{else}
								</td>
								<td>
							{/if}
						{/section}
					</td>	
				</tr>
			  </table>
		 {/if}         
        </div>
	</div>
</div>
	<div class="divider">&nbsp;</div>
	{include file="site/footer.tpl"}
</body>
</html>