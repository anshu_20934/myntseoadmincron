<html>
<head>
{literal}
<script Language="JavaScript" Type="text/javascript">
		function FrontPage_Form1_Validator(theform)
		{
         
         var added=theform.addedby;
         var commentTitle=theform.commenttitle;
         var desc=theform.commentdetails;
		  if (added.value==null||added.value == "")
		  {
			alert("Please enter your name.");
			added.focus();
			return false;
		  }
         if (commentTitle.value==null||commentTitle.value == "")
		  {
			alert("Please enter subject.");
			commentTitle.focus();
			return false;
		  }
		  if (desc.value==null||desc.value == "")
		  {
			alert("Please enter comment details.");
			desc.focus();
			return false;
		  }
		 
		  return true;
		  
		}
		</script>
		{/literal}

<link rel="stylesheet" href="../skin1/mykriti.css"/>

</head>
<body id="body">
<div id="scroll_wrapper">
	<div id="container"> 
	  <div id="wrapper"> 
	    <div id="display"> 
	      <div class="center_column"> 
	        <div class="print"> 
		        <div class="super" > 
	           		 <p>&nbsp;</p>
	          	</div>
	          	{if $commentSuccess ne "Y"}
	          	<div class="head" style="padding-left:15px;"> 
		            <p>&nbsp;&nbsp;{$onholdcomment} &nbsp;(#Order Id : {$orderid})</p>
          		</div>
          		<div class="links"><p></p></div>
          		<div class="foot"></div>
          		
          		
<form method="post" enctype="multipart/form-data" onsubmit="return FrontPage_Form1_Validator(this)" language="JavaScript" name="FrontPage_Form1"  action="saveOrder_comment.php">
			<table>
				<tr>
					<td>Date Added</td>
					<td><input type="text" name="dateadded" value={$date} readonly></td>
				</tr>
				<tr>
					<td>Added By<span style="color:red;">  *</span></td>
					<td><input type="text" name="addedby"></td>
				</tr>
				<tr>
					<td>Type</td>
					<td><input type="text" name="commenttype" value='On Hold' readonly></td>
				<tr>
					<td>Subject<span style="color:red;">  *</span></td>
					<td><input type="text" name="commenttitle" style="width:200px;" value="{$onholdsubject}" readonly></td>
				</tr>
				<tr>
					<td>Details<span style="color:red;">  *</span></td>
					<td><textarea name="commentdetails" cols="50" rows="5"></textarea></td>
				</tr>
				<tr>
					<td>Attachment</td>
					<td>
						<input type="file" name="attachment">
					</td>
				</tr>
				<tr>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				</tr>
				<tr>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td><input type="hidden" name ="orderid" value={$orderid}></td>
					<td><input type="hidden" name ="onhold" value='Y'></td>
					<td><input type="submit" value="addcomment" class="button" ></td>
				</tr>
			</table>
		</form>
		{elseif $commentSuccess eq "Y"}
		<table>
				<tr>
					<td>Comment for on hold added Succesfully!!!!!</td>
					<td>&nbsp;&nbsp;</td>
					<td>&nbsp;&nbsp;</td>
				</tr>
				<tr>
					<td style="font-size:.8em;color:red;"><b>Please Click the close button below to apply changes and refresh back screen</b></td>
				</tr>
				<tr>
					<td align="center"><input type="button" style="height:20px;width:50px; background-color:black; color:white;" value="Close" onclick="opener.document.statusForm.submit();self.close()"></td>
				</tr>
		</table>
		{/if}
		</div> 
	      </div>   	
	     </div> 
	   </div> 
	 </div>  
	</div>
	</body> 
	</html>
    	