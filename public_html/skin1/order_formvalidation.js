function formValidation(mode)
{
	
	if(document.getElementById('firstname').value == "")
	{
		  alert("Please enter customer first name.");
		  document.getElementById('firstname').focus();
		  return false;
		  
	}
	else if(document.getElementById('lastname').value == "")
	{
		  alert("Please enter customer last name.");
		  document.getElementById('lastname').focus();
		   return false;
		  
	}
	else if(document.getElementById('email').value == "")
	{
		  alert("Please enter customer email.");
		  document.getElementById('email').focus();
		   return false;
		  
	}
	else if(document.getElementById('company').value == "")
	{
		  alert("Please enter customer company.");
		  document.getElementById('company').focus();
		   return false;
		  
	}
	else
	{
		document.getElementById('mode').value = mode;
		return true;
	}
 	
	    
}
function sameAsShipping()
{
	 var chkObj = document.getElementById('sameAsShip');
	 var stateObj =  document.getElementById('b_state')
	 if(chkObj.checked)
	 {
	 	//document.getElementById('b_FirstName').value = document.getElementById('s_FirstName').value
	 	//document.getElementById('b_LastName').value = document.getElementById('s_LastName').value
	 	document.getElementById('b_address').value = document.getElementById('s_address').value
	 	document.getElementById('b_city').value = document.getElementById('s_city').value
	 	document.getElementById('b_state').value = document.getElementById('s_state').value
	 	stateObj.options[stateObj.selectedIndex].selected=true
	 	document.getElementById('b_country').value = document.getElementById('s_country').value
		document.getElementById('b_zip').value = document.getElementById('s_zip').value
		//document.getElementById('b_Mobile').value = document.getElementById('s_Mobile').value
	 }
	 else
	 {
	 	//document.getElementById('b_FirstName').value = ""
	 	//document.getElementById('b_LastName').value = ""
	 	document.getElementById('b_address').value = ""
	 	document.getElementById('b_city').value = ""
	 	document.getElementById('b_state').value = ""
	 	stateObj.options[stateObj.selectedIndex].selected= false
	 	document.getElementById('b_country').value = ""
		document.getElementById('b_zip').value = ""
		//document.getElementById('b_Mobile').value = ""
	 	
	 }
}

function loadCustomer()
{
	$email = document.getElementById('ecust').value;
	window.location.href = "offline_orders.php?email="+$email+"&action=exist";
}