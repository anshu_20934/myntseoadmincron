<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html xml:lang="en" xmlns:fb="http://www.facebook.com/2008/fbml">
<head>
<META http-equiv="Content-Style-Type" content="text/css">
<META http-equiv="Content-Script-Type" content="type/javascript">
<META http-equiv="Content-Type" content="text/html; charset=utf-8">
{include file="site/header.tpl" }
<!--[if lt IE 8]>
    <link rel="stylesheet" href="new-skin-css/ie.css" type="text/css" media="screen, projection"><![endif]-->

</head>

<body class="{$_MAB_css_classes}">
	{include file="site/menu.tpl" }
<div class="container clearfix">
 
	<div class="clearfix">
		{foreach key=brandidx item=brandentry from=$global_brands}
		<a name="{$brandentry.range|lower|replace:' ':''}"></a>
		<div class="brands-listing-title p5">{$brandentry.range}</div>
		<div class="icon-line"></div>
		<ul class="brands-listing clearfix">
			{foreach key=brand item=count from=$brandentry.result.brands_filter_facet}
				{if $brand_logos[$brand]}
				<li>
					<a href="{$http_location}/{myntralink brands_filter_facet="$brand" is_landingpage="y"}" class="no-decoration-link">
						<img src="{$brand_logos[$brand]}" width="120" height="70" alt="{$brand}">
						<span>{$brand}</span>
					</a>	
				</li>
				{/if}
			{/foreach}
		</ul>
		{/foreach}
	</div>
</div>
	<div class="divider">&nbsp;</div>
	{include file="site/footer.tpl"}
</body>
</html>