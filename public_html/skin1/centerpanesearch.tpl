{if $popular_search}
<div class='form'>
<div class="subhead"><p>popular searches</p></div>
 <div class="text">
<div class='seriallinks'> 
		{*section name=idx loop=$popular_search*}
			{php}
			 $popular_search = $this->get_template_vars('popular_search');
			 $this->assign('myVars',$myVars); 
			 $http_location = $this->get_template_vars('http_location');
			 
			 {/php}
			
			 {php}
			  foreach($popular_search as $k => $val){
			    
			    $whole_keyword = $val['orignal_keyword'] ;
			    $whole_keyword = preg_replace('#([ ])+#','-',$whole_keyword);
			    $search_id = $val['key_id'] ;
			    
			     echo "<a href='".$http_location."/{$whole_keyword}/search/{$search_id}/'>".$val['filter_keyword']."</a>";
			     echo "&nbsp;&nbsp;"; 
			   }
			   
		  	{/php}
			 	
	        {*/section*}
	</div>
	</div>
	<div class='foot'></div>
</div>
{/if}
{if $recently_searched}
<div class='form'>
<div class="subhead"><p>recent searches</p></div>
 <div class="text">
<div class='seriallinks'> 
		{*section name=idx loop=$recently_searched*}
			{php}
			 $recently_searched = $this->get_template_vars('recently_searched');
			 $this->assign('myVars',$myVars); 
			 $http_location = $this->get_template_vars('http_location');
			 {/php}
			
			 {php}
			  foreach($recently_searched as $k => $val){
			   
			    $whole_keyword = $val['orignal_keyword'] ;
			    $whole_keyword = preg_replace('#([ ])+#','-',$whole_keyword);
			    $search_id = $val['key_id'] ;
			    
			     echo "<a href='".$http_location."/{$whole_keyword}/search/{$search_id}/'>".$val['filter_keyword']."</a>";
			     echo "&nbsp;&nbsp;"; 
			   }
			   
		  	{/php}
			 <!--<a href='mkSearchResult.php?mode=search&searchkey={$recently_searched[idx].keyword|replace:' ':'+'}'>{$recently_searched[idx].keyword}</a>-->
			      <!--<a href="{$http_location}/{$recently_searched[idx].keyword|replace:' ':'+'}/search/">{$recently_searched[idx].keyword} {$recently_searched[idx].additional_keyword} </a>-->
			 	
	        {*/section*}
	</div>
	</div>
	<div class='foot'></div>
</div>
{/if}
{if $recently_added}
<div class='form'>
<div class="subhead"><p>previous searches</p></div>
 <div class="text">
<div class='seriallinks'> 
		{*section name=idx loop=$recently_added*}
			{php}
			 $recently_added = $this->get_template_vars('recently_added');
			 $this->assign('myVars',$myVars); 
			 $http_location = $this->get_template_vars('http_location');
			 {/php}
			
			 {php}
			  foreach($recently_added as $k => $val){
			   
			    $whole_keyword = $val['orignal_keyword'] ;
			    $whole_keyword = preg_replace('#([ ])+#','-',$whole_keyword);
			    $search_id = $val['key_id'] ;
			    
			     echo "<a href='".$http_location."/{$whole_keyword}/search/{$search_id}/'>".$val['filter_keyword']."</a>";
			      echo "&nbsp;&nbsp;"; 
			   }
			   
		  	{/php}
			 	
	        {*/section*}
	</div>
	</div>
	<div class='foot'></div>
</div>
{/if}
