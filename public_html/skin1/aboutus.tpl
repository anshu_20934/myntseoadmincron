<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html xml:lang="en" xmlns:fb="http://www.facebook.com/2008/fbml">
<head>
<META http-equiv="Content-Style-Type" content="text/css">
<META http-equiv="Content-Script-Type" content="type/javascript">
<META http-equiv="Content-Type" content="text/html; charset=utf-8">
{include file="site/header.tpl" }
<!--[if lt IE 8]>
    <link rel="stylesheet" href="new-skin-css/ie.css" type="text/css" media="screen, projection"><![endif]-->

</head>

<body class="{$_MAB_css_classes}">
{include file="site/menu.tpl" }
<div class="container clearfix"> 

	<h1 class="head1">About Myntra</h1>

    <div class="content clearfix">
    	<div class="content-left left span-33 prepend">		 
			<p>Myntra.com is ranked among the leading e-commerce companies in India and is the largest online retailer of lifestyle and fashion products. The company was started by a group of IIT/IIM graduates in 2007 and is headquartered in Bangalore. Funded by top tier Venture Capital Funds, Myntra is among the best funded e-commerce companies in the country today.</p>
			<p>Myntra, which started as an online destination for personalized products back in 2007, has expanded into broader lifestyle and fashion retailing. Today, Myntra is the largest online lifestyle retailer with over 200 national and international brands under its banner.</p>
			<p>Myntra has brought in a new level of professionalism and technology enablement to the e-commerce space in India. For consumers, this translates to superior experience, broader product selection and unmatched efficiency, thus adding to a better purchasing decision.</p>
			<p>The company's unique offerings include the largest in-season product catalogue, 100% authentic products, cash on delivery, and 30 day return policy, making Myntra the preferred online shopping destination in the country.</p>
			<p><strong>Our Awards</strong></p>
			<ul>
				<li>CNBC - TV18 Young Turks Award as one of the "Hottest Internet Companies of the Year 2011-12"</li>
				<li>"Pride of India 2011-2012" for Most Successful Fundraising by IDG Ventures</li>
				<li>"Pride of India 2009-2010" award for Exceptional Business Growth by IDG Ventures</li>
				<li>Red Herring Global 100 winner 2010</li>
			</ul>
			<p><strong>Our Team</strong></p>
			<p>At the helm of Myntra's success is a management team of experienced and high caliber senior management professionals from globally recognized organizations such as Microsoft, Google, Intel, Trilogy and SAP, to name a few. The company has an employee base of over 400+ employees from a mix of corporate, business and premier academic institutions like IIMs and IITs who love to have fun and balance work at the same time.</p>
			<p>Follow us on <a href="http://www.facebook.com/myntra" target="_blank">Facebook</a> and <a href="https://twitter.com/#!/myntra" target="_blank">Twitter</a> for regular updates and on our <a href="http://stylemynt.com/" target="_blank">Style Mynt</a> blog to stay in touch with the latest in fashion.</p>
		</div>
	</div>
</div>
	<div class="divider">&nbsp;</div>
	{include file="site/footer.tpl"}
</body>
</html>
