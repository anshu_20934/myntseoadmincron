<h3>Similar Designs for you</h3>
<div class="text" style='background-image:none;'>
      <div class="seriallist content">
      <div class="product-listing clear mb10">
    			<ul class="clearfix last">
                    {section name=product loop=$related_products max=4 }
					<li class="item">
						<a href="{$http_location}/{$related_products[product].product|replace:" ":"-"}/{$related_products[product].typename|replace:" ":"-"}/FC/PD/{$related_products[product].productid}{if $related_products[product].men_styleid neq $related_products[product].styleid}?productStyleId={$related_products[product].men_styleid}{/if}">
							<span class="item-image item-image-border">
								<img id="rp_{$related_products[product].productid}" title="{$related_products[product].product} {$related_products[product].typename}" alt="{$related_products[product].product} {$related_products[product].typename}" src="{$cdn_base}/skin1/myntra_images/loading_198_198.gif" lazy-src="{$related_products[product].image_men_s}" class="lazy" height="178" width="178" style="margin-top:70px;">
							</span>
							<span class="item-divider icon-line"></span>
							<span class="item-description">
								<span class="name">{$related_products[product].product|truncate:40}</span>
									{if $related_products[product].discount_type && $related_products[product].discount_value neq ''}
									<span class="discount-price highlight">Rs {$related_products[product].discounted_price}</span>
									<span class="original-price strike">Rs {$related_products[product].price}</span>
									{else}
									<span class="discount-price">Rs {$related_products[product].price}</span>
									{/if}
<!--									<span class="availability">AVAILABILITY: {if $related_products[product].sizes neq '' && $related_products[product].sizes neq ' '}{$related_products[product].sizes}{else}OUT OF STOCK{/if}</span>-->
							</span>
							<span class="item-divider icon-line"></span>
						</a>
					</li>
                    {/section}
            	</ul>
    		</div>
 </div>
</div>
