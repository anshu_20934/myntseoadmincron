{literal}
<script type="text/javascript">
function checkIfLoggedIn(login){
	if(login==""){
		if(document.getElementById("errorpopup")!=null){
			document.getElementById("errorpopup").style.visibility="visible";
			document.getElementById("errorpopup").style.display="";
		}
		if(document.getElementById("loginpopup")!=null){
			document.getElementById("loginpopup").style.visibility="visible";
			document.getElementById("loginpopup").style.display="";
		}
		if(document.getElementById("regpopup")!=null){
			document.getElementById("regpopup").style.visibility="visible";
			document.getElementById("regpopup").style.display="";
		}
	return false;
	}
	return true;
}

function submitReview(login){//,postUrl,postArray
   if(checkIfLoggedIn(login))   {
        var form = document.getElementById('reviewform');
		if(form.name.value == '')		{
			alert("Please enter your name.")
			form.name.focus();
			return false;
		}else if(form .msg.value == '')		{
			alert("Please enter your comments.")
			form.msg.focus();
			return false;
		}else{
			//buildAndSubmitPostParam(postUrl,postArray);
			$('#reviewform').trigger('submit');
		}
   } else  {
     return false
   }
}

function limit_textarea(textarea, count, maxlimit){
	if (textarea.value.length > maxlimit) {// if too long...trim it!
		textarea.value = textarea.value.substring(0, maxlimit);
	}
	else
		count.value = maxlimit - textarea.value.length;
}
</script>
{/literal}
<h3>Comments</h3>
 <div>
    {if $reviews}
    <ul class="comment-list">
        {section name=review loop=$reviews}
        {if $reviews[review][2] ne '' && $reviews[review][3] ne ''}
             <li><div>{$reviews[review][2]}:</div><span>{$reviews[review][3]}</span></li>
        {/if}
        {/section}
   </ul>
   {/if}
     <!--div align="right"><a href="#"><span style="font-size:11px; color:#6666cc">View All Comments</span></a> </div-->

     <form id="reviewform" method='post' action="updateCustomerReview.php">

     <!--added by arun to get the element by post-->
     <input type=hidden name='productTypeId' id='productTypeId' value="{$productTypeId}" >
     <input type=hidden name='type' id='type' value="{$productTypeLabel}" >
     <input type=hidden name='productStyleId' id='productStyleId' value="{$productStyleId}" >
     <input type=hidden name='productid' id='productid' value="{$productid}" >
     <input type=hidden name='pageURL' id='pageURL' value="{$url}">
     <!--added by arun to get the element by post-->
     

   <p><strong>Post Comments</strong></p>
    <p><textarea rows="5" cols="9" style="width:385px;" name="msg" id="msg" onKeyDown="limit_textarea(this.form.msg,this.form.remLen,200);" onKeyUp="limit_textarea(this.form.msg,this.form.remLen,200);"></textarea></p>
   <p style="float:right;font-size:11px; padding-right:5px;">
       <span  style="font-size: xx-small; font-family: arial, helvetica, sans-serif; ">
            <input readonly type=text name=remLen size=3 maxlength=3 value="200"> characters left
        </span>

   </p>
   <div style="clear:both">&nbsp;</div>
   <div id="abuse_captcha" style="height:130px;"> </div>
   <div style="clear:both">&nbsp;</div>

     <div style="float:left;">
       <div class="" style="_width:155px; margin-bottom:5px;"><span><a style="cursor:pointer;margin-left:210px;" onclick="submitReview('{$login}');" class="orange-bg corners p5">Add Review</a></span> </div>
   </div>
</form>
</div>

