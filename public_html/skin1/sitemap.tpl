<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html xml:lang="en" xmlns:fb="http://www.facebook.com/2008/fbml">
<head>
<META http-equiv="Content-Style-Type" content="text/css">
<META http-equiv="Content-Script-Type" content="type/javascript">
<META http-equiv="Content-Type" content="text/html; charset=utf-8">
{include file="site/header.tpl" }
<!--[if lt IE 8]>
    <link rel="stylesheet" href="new-skin-css/ie.css" type="text/css" media="screen, projection"><![endif]-->

</head>

<body class="{$_MAB_css_classes}">
{include file="site/menu.tpl"}
<div class="container clearfix">
	<h1 class="page-title">Sitemap</h1>	
	<hr class="space clear mb10">
    <div class="content clearfix">
		<p>This is a quick summary of all the functionality available at Myntra.com. You can simply click on a link 
	  to directly navigate to the section of your choice.</p>
		<table class="sitemap">
			<tr>
				<td>
					<p><strong>Start Shopping</strong></p>
						{section name=producttype loop=$producttypes}
						{if $producttypes[producttype][4] == 'P'}
							<p><a href='{$producttypes[producttype][1]}'>{$producttypes[producttype][1]}</a></p>
						{/if}
						{/section} 
					<p><a href="register.php">Registration</a></p>
				</td>
				<td>
					<p><strong>Corporate Info</strong></p>
					<p><a href="aboutus.php">About Us</a></p>
					<p><a href="aboutus.php#myntrateam">Team</a></p>
					<p><a href="{$http_location}/contactus">Contact Us</a></p>
					<!--<p><a href="#">Jobs at Myntra</a></p>
					<p><a href="#">Partner with Us</a></p>-->
					<p><a href="{$http_location}/contactus#suggestions">Suggestions</a></p>
				</td>
				<td>
					<p><strong>Help</strong></p>
					<p><a href="termandcondition.php">Terms and Conditions</a></p>
					<p><a href="privacy_policy.php">Privacy Policy</a></p>
<!--					<p><a href="license.php">License Agreement</a></p>-->
					<!--p><a href="mkcreateproduct.php">Create Product</a></p-->
					<p><a href="mymyntra.php">My Account</a></p>
					<p><a href="mkfaq.php">Frequently Asked Questions</a></p>
				</td>
                                <td>
                                </td>
			</tr> 
		</table>		
	</div>
</div>
<div class="divider">&nbsp;</div>
{include file="site/footer.tpl"}
</body>
</html>
