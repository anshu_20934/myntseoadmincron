{if $s_country == 'IN' && $logistic_provider == 'DTDC'}
  <div id="overlay"> </div>
  <div id="messagewide">
    <div class="wrapper" style="left:100px;top:10px;">
      <div class="head"><a href="javascript:closePopupDiv('view_shipping_details')" >Click to close</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
      <div class="body">
        <p>
    <table style="font-size: 1.0em;font-weight:100;" border=0 width="95%">
        <tr>
            <td colspan='5'>
                <p>we have partnered with Door to Door Courier (DTDC) which is one of the leading provider of courier services in India.
                It will take us up to 2 business days to fulfill the order at our operations facility. As soon as the order is ready,
                DTDC will pick-up the package from our facilities on the same day.</p>
            </td>
        </tr>
        <tr bgcolor='#66FF33'>
            <td>&nbsp;</td>
            <td align='center'><b>no. of items in one box</b></td><td align='center'><b>bangalore</b></td><td align='center'><b>metro cities</b></td>
            <td align='center'><b>rest of india</b></td>
        </tr>
        <tr bgcolor='#99FFFF'>
            <td align='center'><b>delivery time</b></td>
            <td align='center'>&nbsp;</td>
            <td align='center'><b>1-2 days</b></td>
            <td align='center'><b>1-2 days</b></td>
            <td align='center'><b>3-4 days</b></td>
        </tr>
        <tr bgcolor='#CCFFFF'>
            <td align='center'>t-shirt</td>
            <td align='center'>2</td>
            <td align='center'>Rs.25</td>
            <td align='center'>Rs.40</td>
            <td align='center'>Rs.54</td>
        </tr>
        <tr bgcolor='#CCFFFF'>
            <td align='center'>mug</td>
            <td align='center'>1</td>
            <td align='center'>Rs.25</td>
            <td align='center'>Rs.40</td>
            <td align='center'>Rs.54</td>
        </tr>
        <tr bgcolor='#CCFFFF'>
            <td align='center'>mousepad</td>
            <td align='center'>2</td>
            <td align='center'>Rs.25</td>
            <td align='center'>Rs.40</td>
            <td align='center'>Rs.54</td>
        </tr>
        <tr bgcolor='#CCFFFF'>
            <td align='center'>keychain</td>
            <td align='center'>3</td>
            <td align='center'>Rs.25</td>
            <td align='center'>Rs.40</td>
            <td align='center'>Rs.54</td>
        </tr>
        <tr bgcolor='#CCFFFF'>
            <td align='center'>poster</td>
            <td align='center'>1</td>
            <td align='center'>Rs.25</td>
            <td align='center'>Rs.40</td>
            <td align='center'>Rs.54</td>
        </tr>
        <tr bgcolor='#CCFFFF'>
            <td align='center'>coaster</td>
            <td align='center'>3</td>
            <td align='center'>Rs.25</td>
            <td align='center'>Rs.40</td>
            <td align='center'>Rs.54</td>
        </tr>
        <tr bgcolor='#CCFFFF'>
            <td align='center'>mirror</td>
            <td align='center'>1</td>
            <td align='center'>Rs.25</td>
            <td align='center'>Rs.40</td>
            <td align='center'>Rs.54</td>
        </tr>
   
        <tr>
            <td colspan='5'><p>*these are the estimated times in normal circumstances.</p></td>
        </tr>
        <tr>
            <td colspan='5'><p><b>shipping for 5 t-shirts to Delhi can be calculated like this �</b></p></td>
        </tr>
        <tr>
            <td colspan='5'><p>total packages = (number of t-shirts) / (no. of items in one box)</p></td>
        </tr>
        <tr>
            <td colspan='5'><p><b>for example:&nbsp;</b>total packages = 5 / 2 = 2.5 =3 (packages are whole numbers)</p></td>
        </tr>
        <tr>
            <td colspan='5'><p>shipping cost = (shipping cost per package) X (total no. of packages)</p></td>
        </tr>
        <tr>
            <td colspan='5'><p><b>for example:</b>&nbsp;shipping cost = 25 X 3 = 75</p></td>
        </tr>
        <tr>
            <td colspan='5'><p>actual delivery times may vary in certain situations due to unforeseen situations.</p></td>
        </tr>
        <tr>
            <td colspan='5'><p><span class="mandatory">*these shipping rates do not apply for bulk order.</span>&nbsp;(order quantity greater than 20)</p></td>
        </tr>
    </table>
    </p>
      </div>
      <div class="foot"></div>
    </div>
  </div>

{elseif $s_country == 'IN' && $logistic_provider == 'Blue Dart'}
  <div id="overlay"> </div>
  <div id="messagewide">
    <div class="wrapper" style="left:100px;top:10px;">
      <div class="head"><a href="javascript:closePopupDiv('view_shipping_details')" >Click to close</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
      <div class="body">
        <p>
    <table style="font-size: 1.0em;font-weight:100;" border=0 width="95%">
        <tr>
            <td colspan='5'>
                <p>we have partnered with Blue Dart which is one of the leading provider of courier services in India.
                It will take us up to 2 business days to fulfill the order at our operations facility. As soon as the order is ready,
                Blue Dart will pick-up the package from our facilities on the same day.</p>
            </td>
        </tr>
        <tr bgcolor='#66FF33'>
            <td>&nbsp;</td>
            <td align='center'><b>no. of items in one box</b></td><td align='center'><b>bangalore</b></td><td align='center'><b>metro cities</b></td>
            <td align='center'><b>rest of india</b></td>
        </tr>
        <tr bgcolor='#99FFFF'>
            <td align='center'><b>delivery time</b></td>
            <td align='center'>&nbsp;</td>
            <td align='center'><b>1-2 days</b></td>
            <td align='center'><b>1-2 days</b></td>
            <td align='center'><b>3-4 days</b></td>
        </tr>
        <tr bgcolor='#CCFFFF'>
            <td align='center'>t-shirt</td>
            <td align='center'>2</td>
            <td align='center'>Rs.25</td>
            <td align='center'>Rs.32</td>
            <td align='center'>Rs.32</td>
        </tr>
        <tr bgcolor='#CCFFFF'>
            <td align='center'>mug</td>
            <td align='center'>1</td>
            <td align='center'>Rs.25</td>
            <td align='center'>Rs.32</td>
            <td align='center'>Rs.32</td>
        </tr>
        <tr bgcolor='#CCFFFF'>
            <td align='center'>mousepad</td>
            <td align='center'>2</td>
            <td align='center'>Rs.25</td>
            <td align='center'>Rs.32</td>
            <td align='center'>Rs.32</td>
        </tr>
        <tr bgcolor='#CCFFFF'>
            <td align='center'>keychain</td>
            <td align='center'>3</td>
            <td align='center'>Rs.25</td>
            <td align='center'>Rs.32</td>
            <td align='center'>Rs.32</td>
        </tr>
        <tr bgcolor='#CCFFFF'>
            <td align='center'>poster</td>
            <td align='center'>1</td>
            <td align='center'>Rs.25</td>
            <td align='center'>Rs.32</td>
            <td align='center'>Rs.32</td>
        </tr>
        <tr bgcolor='#CCFFFF'>
            <td align='center'>coaster</td>
            <td align='center'>3</td>
            <td align='center'>Rs.25</td>
            <td align='center'>Rs.32</td>
            <td align='center'>Rs.32</td>
        </tr>
        <tr bgcolor='#CCFFFF'>
            <td align='center'>mirror</td>
            <td align='center'>1</td>
            <td align='center'>Rs.25</td>
            <td align='center'>Rs.32</td>
            <td align='center'>Rs.32</td>
        </tr>
   
        <tr>
            <td colspan='5'><p>*these are the estimated times in normal circumstances.</p></td>
        </tr>
        <tr>
            <td colspan='5'><p><b>shipping for 5 t-shirts to Delhi can be calculated like this �</b></p></td>
        </tr>
        <tr>
            <td colspan='5'><p>total packages = (number of t-shirts) / (no. of items in one box)</p></td>
        </tr>
        <tr>
            <td colspan='5'><p><b>for example:&nbsp;</b>total packages = 5 / 2 = 2.5 =3 (packages are whole numbers)</p></td>
        </tr>
        <tr>
            <td colspan='5'><p>shipping cost = (shipping cost per package) X (total no. of packages)</p></td>
        </tr>
        <tr>
            <td colspan='5'><p><b>for example:</b>&nbsp;shipping cost = 25 X 3 = 75</p></td>
        </tr>
        <tr>
            <td colspan='5'><p>actual delivery times may vary in certain situations due to unforeseen situations.</p></td>
        </tr>
        <tr>
            <td colspan='5'><p><span class="mandatory">*these shipping rates do not apply for bulk order.</span>&nbsp;(order quantity greater than 20)</p></td>
        </tr>
    </table>
    </p>
      </div>
      <div class="foot"></div>
    </div>
  </div>


{else}




  <div id="overlay"> </div>
  <div id="messagewide">
    <div class="wrapper" style="left:100px;top:10px;">
      <div class="head"><a href="javascript:closePopupDiv('view_shipping_details')" >Click to close</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
      <div class="body">
        <p>
    <table style="font-size: 1.0em;font-weight:100;" border=0 width="95%">
        <tr>
            <td colspan='3'>
                <p>It will take us up to 2 business days to fulfill the order at our operations facility. As soon as the order is ready,
                {$logistic_provider} will pick-up the package from our facilities on the same day.</p>
            </td>
        </tr>
        <tr bgcolor='#66FF33'>
            <td>&nbsp;</td>
            <td align='center'><b>no. of items in one box</b></td><td align='center'><b>{$destination}</b></td>
            
        </tr>
        <tr bgcolor='#99FFFF'>
            <td align='center'><b>delivery time</b></td>
            <td align='center'>&nbsp;</td>
            <td align='center'><b>{$shipping_days} days</b></td>
            
        </tr>
        
        {section name=view_details loop=$view_products }
        
        <tr bgcolor='#CCFFFF'>
            <td align='center'>{$view_products[view_details].name}</td>
            <td align='center'>{$view_products[view_details].consignment_capacity}</td>
            <td align='center'>Rs.{$shiprate}</td>
        </tr>
        
        {/section}
   
        <tr>
            <td colspan='5'><p>*these are the estimated times in normal circumstances.</p></td>
        </tr>
        <tr>
            <td colspan='5'><p><b>shipping for 5 t-shirts to {$destination} can be calculated like this �</b></p></td>
        </tr>
        <tr>
            <td colspan='5'><p>total packages = (number of t-shirts) / (no. of items in one box)</p></td>
        </tr>
        <tr>
            <td colspan='5'><p><b>for example:&nbsp;</b>total packages = 5 / 2 = 2.5 =3 (packages are whole numbers)</p></td>
        </tr>
        <tr>
            <td colspan='5'><p>shipping cost = (shipping cost per package) X (total no. of packages)</p></td>
        </tr>
        <tr>
        {math assign=total_ship_rate x=$shiprate y=3 equation=x*y }
            <td colspan='5'><p><b>for example:</b>&nbsp;shipping cost = {$shiprate} X 3 ={$total_ship_rate} </p></td>
        </tr>
        <tr>
            <td colspan='5'><p>actual delivery times may vary in certain situations due to unforeseen situations.</p></td>
        </tr>
        <tr>
            <td colspan='5'><p><span class="mandatory">*these shipping rates do not apply for bulk order.</span>&nbsp;(order quantity greater than 20)</p></td>
        </tr>
    </table>
    </p>
      </div>
      <div class="foot"></div>
    </div>
  </div>



{/if}
