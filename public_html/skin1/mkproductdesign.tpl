<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
{config_load file="$skin_config"}
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>
        {if $config.SEO.page_title_format eq "A"}
        {section name=position loop=$location}
        {$location[position].0|strip_tags|escape}
        {if not %position.last%} :: {/if}
        {/section}
        {else}
        {section name=position loop=$location step=-1}
        {$location[position].0|strip_tags|escape}
        {if not %position.last%} :: {/if}
        {/section}
        {/if}
</title>
{include file="meta.tpl" }
<link rel="stylesheet" href="{$SkinDir}/{#CSSFile#}" />
</head>
{literal}
<script language="JavaScript" type="text/JavaScript">
function MM_jumpMenu(targ,selObj,restore){ //v3.0
  eval(targ+".location='"+selObj.options[selObj.selectedIndex].value+"'");
  if (restore) selObj.selectedIndex=0;
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
</script>
{/literal}
</head>

<body onLoad="MM_preloadImages('{$mkImagesDir}/html_ico_bold_ov.gif','{$mkImagesDir}/html_ico_italic_ov.gif','{$mkImagesDir}/html_ico_underline_ov.gif','{$mkImagesDir}/html_ico_forgroundcolor_ov.gif','{$mkImagesDir}/html_ico_right_ov.gif','{$mkImagesDir}/html_ico_left_ov.gif','{$mkImagesDir}/html_ico_up_ov.gif','{$mkImagesDir}/html_ico_down_ov.gif')">
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td height="78" valign="top">
    {include file="mykritihead.tpl"}
    </td>
  </tr>
  <tr>
    <td height="100%" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td height="4" background="{$mkImagesDir}/leftnav_top_green.jpg"><img src="{$cdn_base}/skin1/mkimages/leftnav_top.jpg" width="1" height="4"></td>
      </tr>
    </table>
    {include file="mkbreadcrumb.tpl"}
          <br>
          <table width="100%" border="0" cellpadding="0" cellspacing="0" class="mainpagecontent">
            <tr>
              <td>
                    <table width="98%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td>
                        {include file="mktableheader.tpl"}
                        </td>
                      </tr>
                      <tr>
                        <td class="listTableTD"><br>
                          <table width="100%"  border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td width="30%" valign="top">
                                  {include file="mkdesignpreview.tpl"}
                                  <br>
                                  {include file="mkcreateproductproductoptions.tpl"}
                              </td>
                              <td valign="top"><table width="98%"  border="0" align="center" cellpadding="0" cellspacing="0" class="textBlack">
                                  <tr>
                                    <td> You can create your own design by adding text, uploading your own image or by using image from our graphics library.</td>
                                  </tr>
                                </table>
                                  <br>
                                  {include file="mkcreateproductdesignorientation.tpl"}
                                  <br>
                                  {include file="mkcreateproductdesigntext.tpl"}
                                <br>
                                {include file="mkcreateproductdesignimage.tpl"}
                                </td>
                                    </tr>
                                </table></td>
                            </tr>
                          </table></td>
                      </tr>
                    </table>
                  <br></td></tr>
          </table></td>
        </tr>
    </table></td></tr>
  <tr>
    <td height="2" align="center" background="{$mkImagesDir}/leftnav_top.jpg"><img src="{$cdn_base}/skin1/mkimages/clear.gif" width="1" height="2"></td>
  </tr>
  <tr>
    <td height="20" align="center" class="bottom">
    {include file="footer.tpl"}
    </td>
  </tr>
</table>
</body>
</html>

