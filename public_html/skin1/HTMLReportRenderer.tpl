<html>
<head>
{literal}


<script Language="JavaScript" Type="text/javascript">
	function FrontPage_Form1_Validator(theform){
		return true;		  
	}
	function popupNEW(fnam){
		  var element = document.getElementById(fnam);
		  var calname = "cal"+fnam;
		  var calname = new calendar2(element);
	      calname.year_scroll = true;
	      calname.time_comp = false;
		  calname.popup();
		}
	function setAction(format,action){
					
		document.exportform.format.value=format;
		document.exportform.action.value=action;
		if(action=='mail'){
			var params="format="+format+"&action="+action;
			params+="&prompt_filters="+document.exportform.prompt_filters.value;
			params+="&report="+document.exportform.report.value;
			params+="&report_title="+document.exportform.report_title.value;
			params+="&report_list="+document.exportform.report_list.value;
			params+="&emailsToList="+document.exportform.emailsToList.value;
			params+="&caller="+document.exportform.caller.value;
			params+="&decimals="+document.exportform.decimals.value;
			params+="&report_name=/reports/ReportRenderer.php";
			document.body.style.cursor = 'wait';
			sendMail(params);
		}else{
			document.exportform.submit();
		}
		
	}

	 function sendMail(params)
     {
     	if (window.XMLHttpRequest)
       {
       	http=new XMLHttpRequest();
       }
     	else
       {
       	http=new ActiveXObject("Microsoft.XMLHTTP");
       }
       params+="{/literal}{foreach from=$filters key=name item=filter}&{$name}={$filter.value}{/foreach}{literal}";
     	http.onreadystatechange=function(){
     		if (http.readyState==4 && http.status==200)
     		{
     			document.body.style.cursor = 'default';
     		    alert(http.responseText);
     		}
    	}; 
     	http.open("POST","{/literal}{$http_location}{literal}/admin/AdminReportViewer.php",true);
     	http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
     	http.setRequestHeader("Content-length", params.length);
     	http.setRequestHeader("Connection", "close");
     	http.send(params);
     }
</script>
{/literal}

<link rel="stylesheet" href="../skin1/mykriti.css" />

</head>
<body id="body">
<div id="scroll_wrapper">
<div id="container"  >
<div id="display"  >
<div class="center_column" >
<div class="print"  >
<div class="super" >
<p>&nbsp;</p>
</div>
<div class="head" style="padding-left: 15px;">
<p>&nbsp;&nbsp;{$reportname}</p>
{$reportnotes}
</div>
<div class="links">
{include file="main/include_js.tpl" src="main/calendar2.js"}
<form method="POST" name="ReportRendererForm"
	action="{$http_location}/admin/AdminReportViewer.php"><input type="hidden" name="report"
	value="{$report}">
	<input type="hidden" name="report_title"
	value="{$reportname}">
	<input type="hidden" name="report_list"
	value="{$report_list}">
	<input type="hidden" name="report_name"
	value="/reports/ReportRenderer.php">
	<input type="hidden" name="report_name"
	value="/reports/ReportRenderer.php">
	<input type="hidden" name="action"
	value="genreport">
	<input type="hidden" name="prompt_filters"
	value="allset">
	<input type="hidden" name="caller"
	value="user">
	<input type="hidden" name="format"
	value="{$format}">
	<input type="hidden" name="report"
	value="{$report}">
	<input type="hidden" name="emailsToList"
	value="{$emailsToList}">
	<input type="hidden" name="decimals"
	value="{$decimals}">
<table cellpadding="3" cellspacing="1" width="100%">
	{foreach from=$filters key=name item=filter}
	<tr>
		<td><b>{$filter.name}</b></td>
		<td>{if $filter.type eq "DAT"} <input type='text' name="{$name}"
			id="{$name}" value="{$filter.value}">&nbsp;
		<p style="margin-left: 135px; margin-top: -19px;"><a
			href='javascript:popupNEW("{$name}");'><img src="../admin/img/cal.gif"
			width="16" height="16" border="0"
			alt="Click Here to Pick up the date"></a></p>
		{/if}</td>
	</tr>
	{/foreach}
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2"><input type="submit" value="Run Report" /></td>
	</tr>
</table>
</form>
</div>
<div class="foot"></div>
{if $reportresult}
<table cellpadding="3" cellspacing="1" width="100%">
	<form method="post" name="exportform" action="{$http_location}/admin/AdminReportViewer.php">
	<input type="hidden" name="report_name"
	value="/reports/ReportRenderer.php">
	{foreach from=$filters key=name item=filter}
	<input type="hidden" name="{$name}"	value="{$filter.value}">
	{/foreach}
	<input type="hidden" name="report_title"
	value="{$reportname}">
	<input type="hidden" name="report_list"
	value="{$report_list}">
	<input type="hidden" name="report"
	value="{$report}">
	<input type="hidden" name="action"
	value="genreport">
	<input type="hidden" name="prompt_filters"
	value="allset">
	<input type="hidden" name="caller"
	value="user">
	<input type="hidden" name="format"
	value="{$format}">
	<input type="hidden" name="emailsToList"
	value="{$emailsToList}">
	<input type="hidden" name="decimals"
	value="{$decimals}">
	<table cellpadding="3" cellspacing="1" width="100%">
		<tr>
			<td><input type="button" value="Send By Email"
				style="background-color: #953735; color: white; width: 200px;"
				onclick="javascript:setAction('excel5','mail');"></td>
			<td><input type="button" value="Export to XLS"
				style="background-color: #953735; color: white; width: 200px;"
				onclick="javascript:setAction('csv','genreport');"></td>
			<td><input type="button" value="Export to PDF"
				style="background-color: #953735; color: white; width: 200px;"
				onclick="javascript:setAction('pdf','genreport');"></td>
		</tr>
	</table>

	</form>
</table>
{/if}
{foreach from=$reportresult key=name item=group}
<h1>{$name}</h1>
{foreach from=$group key=name item=table}
	{if !is_numeric($name)}
		<h4>{$name}</h4>
	{/if}	
	<table cellpadding="3" cellspacing="1" width="100%">
	<tr style="background-color: #4f81bd; color: white;">
		{foreach from=$table.superheaders key=superheader item=span}
		<th style="text-align: center;" colspan="{$span}">{$superheader}</th>
		{/foreach}
	</tr>
	<tr style="background-color: #4f81bd; color: white;">
		{foreach from=$table.headers item=header}
		<td style="text-align:center;  white-space: nowrap;">{$header}</td>
		{/foreach}
	</tr>
	{foreach from=$table.rows key=rowid item=row}
	<tr style="background-color: {cycle values="#d0d8e8,#e9edf4"};">
		{foreach from=$row key=colid item=cell}
		<td style="{if $cell.style.color}background-color:{$cell.style.color};{/if} text-align: {if $cell.style.align} {$cell.style.align} {else} center {/if}; white-space: nowrap; {if $cell.style.border}{$cell.style.border};{/if}" {if $cell.style.rowspan} rowspan="{$cell.style.rowspan}" {/if}>
		{if $cell.style.hyperlink}<a href="{$cell.style.hyperlink}">{$cell.data}</a>{else}{$cell.data}{/if}
		</td>
		{/foreach}
	</tr>
	{/foreach} {foreach from=$table.footer item=foot}
	<th style="text-align: center;center;white-space: nowrap;">{$foot}</th>
	{/foreach}
	<tr>
		<td>&nbsp;&nbsp;</td>
	</tr>	
	</table>
{/foreach}
{/foreach}</div>
</div>
</div>
</div>
</div>

</body>
</html>

