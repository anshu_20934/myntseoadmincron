<form name="uploadImage" method="post" enctype="multipart/form-data" action="{$postToSelf}">
<table width="98%"  border="0" align="center" cellpadding="0" cellspacing="0" class="greyborder">
<tr>
  <td><table border="0" cellpadding="0" cellspacing="0" class="textBlack">
      <tr>
        <td><strong> Add Image{if ($isUploadSuccessful)} - Image uploaded successfully! {/if}</strong></td>
    </tr>
    <br>
        {section name=errors loop=$errors}
        <table border="0" cellpadding="0" cellspacing="0" class="textredbold">
            <tr>
                <td>{$errors[errors]} </td>
            </tr>
        </table>
        {/section}
    </table>
      <br>
      <table width="98%"  border="0" cellpadding="0" cellspacing="0" class="textBlack">
        <tr>
          <td width="300" height="21">If you already have an image, you can upload it.</td>
          <td><table width="100%"  border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="120"><input name="uploadedImage" type="file" class="GtextBox">
              <input type="hidden" name="productTypeId" value="{$productTypeId}">
              <input type="hidden" name="productType" value="{$productTypeLabel}">
              </td>
              <td><table border="0" cellpadding="0" cellspacing="0" >
                <tr>
                  <td> <input name="action" type="submit" value="Upload" alt="upload" class="loginbtnAction" onMouseOver="this.className='loginbtnActionOver';" onMouseOut="this.className='loginbtnAction';">
                  </td>
                </tr>
              </table></td>
            </tr>
          </table>
          </td>
          </form>
        </tr>
	<tr><td height="21" colspan=2><div id='popupmsg'></div></td></tr>
	 <tr>
          <td height="21">Already uploaded an image?</td>
          <td>
          <table border="0" cellpadding="0" cellspacing="0" >
            <tr>
            <td class="loginbtnAction" onMouseOver="this.className='loginbtnActionOver';" onMouseOut="this.className='loginbtnAction';" onClick="javascript: if (checkIfLoggedIn('{$login}')) showImageSelection('imageselectionlayer','anchor_myimage',-200,-140);">Use Image I've already uploaded </td>
            </tr>
          </table>

          </td>
        </tr>
	

    </table>
    </td>
</tr>
</table>
</form>
<a href='#' name="anchor_myimage" id ="anchor_myimage">&nbsp;</a>
<div id="imageselectionlayer" style="VISIBILITY: hidden;  border-style:solid;border-width:0px; display:block ; POSITION: absolute;background:#ffffff;line-height:1.0; width:500px;overflow:auto;
">
</div>

