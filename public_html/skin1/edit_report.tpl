<html>
<head>
{literal}
<script Language="JavaScript" Type="text/javascript">
		function FrontPage_Form1_Validator(theform)
		{
         //alert("Please Generate a valid sku first");
         var reportname=theform.reportname;
         var query=theform.query;
         
          if (reportname.value==null||reportname.value == "")
		  {
			alert("Please enter the report name");
			reportname.focus();
			return false;
		  }
		  if (query.value==null||query.value == "")
		  {
			alert("Please write report query ");
			query.focus();
			return false;
		  }
		  return true;
		  
		}
function addfilter(opts){
	var optstring = opts;
	var ftable = document.getElementById("filtertable");
	ftable.style.visibility='visible';
	var rowlength = ftable.rows.length;
	var rowelement = ftable.insertRow(rowlength);
	rowelement.setAttribute("id",rowlength);
	var tdElement = document.createElement("td");
	var newElement = document.createElement("div");
	//newElement.innerHTML = "<td><input type='text' name='fname[]'></td><td>&nbsp;</td><td><select name='ftype[]' id='"+"s"+rowlength+"' onchange='javascript:showopt("+rowlength+");'>"+optstring+"</select></td><td>&nbsp;</td><td id="+"z"+rowlength+"><input type='hidden' name='fopt[]' value=''/></td><td>&nbsp;</td><td><input type='button' value='remove filter' onclick='deleterow("+rowlength+")'></td>";
	newElement.innerHTML = "<input type='text' name='fname[]'>&nbsp;<select name='ftype[]' id='"+"s"+rowlength+"' onchange='javascript:showopt("+rowlength+");'>"+optstring+"</select>&nbsp;<span id="+"z"+rowlength+"><input type='hidden' name='fopt[]' value=''/></span>&nbsp;<input type='button' value='remove filter' onclick='deleterow("+rowlength+")'>";
	tdElement.appendChild(newElement);
	rowelement.appendChild(tdElement);
}
function showopt(id){
	var sid = "s"+id;
	var zid = "z"+id;
	var selement = document.getElementById(sid);
	var zelement = document.getElementById(zid);
	if(selement.value=="SPL"){
		zelement.innerHTML="<input type='text' name='fopt[]' style='width:200px;' value='enter comma seperated list here'/>";
	}
	else if(selement.value=="DPL"){
		zelement.innerHTML="<input type='text' name='fopt[]' value='enter query to get dynamic list'/>";
	}
}
function deleterow(id){
	var ftable = document.getElementById("filtertable");
	var row = document.getElementById(id);
	var index = row.rowIndex;
	var rows = ftable.rows.length;
	ftable.deleteRow(index);
	if(rows==2){
	ftable.style.visibility='hidden';
	}
}		
		</script>
{/literal}

<link rel="stylesheet" href="../skin1/mykriti.css" />

</head>
<body id="body">
<div id="scroll_wrapper">
<div id="container">
<div id="display">
<div class="center_column">
<div class="print">
<div class="super">
<p>&nbsp;</p>
</div>
{if $commentSuccess eq "Y"}
<div class="head" style="padding-left: 15px;">
<p>&nbsp;&nbsp;{$reportname}</p>
</div>
<div class="links">
<p></p>
</div>
<div class="foot"></div>

<form action="edit_report.php" method="post"
	onsubmit="return FrontPage_Form1_Validator(this)" language="JavaScript"
	name="reportaddform"><input type="hidden" name="editreport"
	value="editreport" /> <input type="hidden" name="reportid"
	value="{$reportid}" />

<table>

	<tr>
		<td style="text-align: left;">Report Name</td>
		<td style="text-align: left;"><input type="text" name="reportname"
			value='{$reportname}' /></td>
	</tr>
	<tr>
		<td style="text-align: left;">Date Modified</td>
		<td style="text-align: left;"><span
			style="font-size: .9em; color: blue;">{$date}</span></td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td style="text-align: left;">Mail To</td>
		<td style="text-align: left;"><input type="text" style="width: 350px;"
			name="mailto" value='{$mailto}' /></td>
	</tr>
	<tr>
		<td style="text-align: left;">Report Group</td>
		<td><select name="reportgroup">
			{foreach from=$group key=gid item=gname} 
			{if $gid eq $grp}
			<option value="{$gid}" selected>{$gname}</option>
			{else}
			<option value="{$gid}">{$gname}</option>
			{/if}
			 {/foreach}
		</select></td>
	</tr>

	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td style="text-align:left;">Report Type</td>
		<td> <select name="reporttype">
			
			{if $reporttype eq "Q"}
				<option value="Q" selected> Query </option> 
			{else}
                                <option value="Q"> Query </option> 
			{/if}
			{if $reporttype eq "SP"}
				<option value="SP" selected> Stored Procedure </option> </select> 
			{else}
				<option value="SP"> Stored Procedure </option> </select> 
			{/if}
		</td>
	</tr>
	<tr>
		<td>Query</td>
		<td><textarea charset="us-ascii" name="query" cols=90 rows=10>{$query}</textarea></td>
	</tr>
	<tr>
		<td>Sum Query (optional)</td>
		<td><textarea name="sumquery" cols=90 rows=10>{$sumquery}</textarea></td>
	</tr>
{if $filters}
	
				<tr style="background-color:#4f81bd;">
					
					<td style="color:white; font-weight:bold; font-size:1.0em;">Filter Name</td>
					<td style="color:white; font-weight:bold; font-size:1.0em;">Data Type</td>
					<td style="color:white; font-weight:bold; font-size:1.0em;">Default Value</td>
					
				</tr>
				{foreach from=$filters item=filter}
				<tr bgcolor="{cycle values="#d0d8e8,#e9edf4"}">					
					<td style="font-size:.8em;">{$filter[0]}</td>
					<td style="font-size:.8em;">{$filter[1]}</td>
					<td style="font-size:.8em;">
					<input type="hidden" name="fname[]" value="{$filter[3]}">
					<input type="hidden" name="fid[]" value="{$filter[4]}">
					<input type="text" name="{$filter[3]}" value='{$filter[2]}' /></td>
				</tr>				
                {/foreach}    
			{/if}


	<tr>
		<td><input type="submit" value="Update report" /></td>
	</tr>

</table>
</form>

{elseif $commentSuccess eq "EDIT"}
<table>
	<tr>
		<td>Report Edited Succesfully!!!!!</td>
		<td>&nbsp;&nbsp;</td>
		<td>&nbsp;&nbsp;</td>
	</tr>
	<tr>
		<td style="font-size: .8em; color: red;"><b>Please Click the close
		button below to refresh data on back screen</b></td>
	</tr>
	<tr>
		<td align="center"><input type="button"
			style="height: 20px; width: 50px; background-color: black; color: white;"
			value="Close" onclick="opener.window.location.reload();self.close()"></td>
	</tr>
</table>
{elseif $commentSuccess eq "DEL"}
<table>
	<tr>
		<td>Report Deleted Succesfully!!!!!</td>
		<td>&nbsp;&nbsp;</td>
		<td>&nbsp;&nbsp;</td>
	</tr>
	<tr>
		<td style="font-size: .8em; color: red;"><b>Please Click the close
		button below to refresh data on back screen</b></td>
	</tr>
	<tr>
		<td align="center"><input type="button"
			style="height: 20px; width: 50px; background-color: black; color: white;"
			value="Close" onclick="opener.window.location.reload();self.close()"></td>
	</tr>
</table>
{elseif $commentSuccess eq "NA"}
<table>
	<tr>
		<td>NOT Authorized to write this query!!!!!</td>
		<td>&nbsp;&nbsp;</td>
		<td>&nbsp;&nbsp;</td>
	</tr>
	<tr>
		<td align="center"><input type="button"
			style="height: 20px; width: 50px; background-color: black; color: white;"
			value="Close" onclick="opener.window.location.reload();self.close()"></td>
	</tr>
</table>
{/if}</div>
</div>
</div>
</div>
</div>
</body>
</html>
