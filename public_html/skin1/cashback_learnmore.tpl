<div id="learnMoreCashback" style="display:none">
	<div class="overlay" style="display:block;_position:absolute;_width:1300px;_height:1500px;" onClick="Javascript:closeLearnMoreCashbackWindow()">&nbsp;</div>
	<div style="left:28%; position:fixed; _position:absolute; width:500px; background:white; top:20%; z-index:1006;border:2px solid #999999;">
		<div id="learnMoreCashbackContent" style="display:none;">
		    <div id="TB_title">
		    <div id="TB_ajaxWindowTitle">Cashback</div>
		    <div id="TB_closeAjaxWindow"><a class="no-decoration-link" href="javascript:closeLearnMoreCashbackWindow()">close</a></div>
		    </div>

			<div class="clear">&nbsp;</div>
			<div id="aboutCashback" style="padding:5px 25px 10px;  text-align:left; font-size:12px" >
				You will get 10% of the post-discount value of the transaction credited to your Mynt credits account after realization of
				your payment and completion of order delivery.
				 <br><br>
				To view more details of our Cashback program <a href="{$http_location}/myntclub" target="_blank" onclick="closeLearnMoreCashbackWindow()">Click here</a>
			</div>
		</div>
	</div>
</div>


{literal}
	<script type="text/javascript">

	function showCashbackLearnMore() {
		$("#learnMoreCashback").css("display","block");
		$("#learnMoreCashbackContent").css("display","block");
		$("#TB_title").css("display","block");
		//gaTrackEvent("sports_pdp_"+gaSuffix,"cod_more_info","");
	}

	function closeLearnMoreCashbackWindow() {
		$("#learnMoreCashback").css("display","none");
		$("#learnMoreCashbackContent").css("display","none");
	}

	</script>
{/literal}

