{include file="site/doctype.tpl"}
<head>
	{include file="site/header.tpl" }
</head>
<body class="{$_MAB_css_classes}">
	{include file="site/menu.tpl" }
	<div class="container clearfix">
		<a href="/paris-calling"><img src="{$cdn_base}/skin1/images/lp_paris_calling.jpg" alt="Paris Calling"></a>
		<p style="padding-bottom:5px;margin-bottom:5px;"></p>
		<h1 class="head1">PARIS CALLING</h1>
		<p style="padding-bottom:5px;margin-bottom:5px;"></p>
    	<div class="content clearfix">
    		<p style="margin-bottom:10px;">Myntra.com presents <strong>PARIS CALLING*</strong>, your chance to visit the fashion capital of the world!</p>
    		<ul style="margin-bottom:10px;">
    			<li><strong>2 lucky winners</strong> will get an <strong>all-inclusive* trip for two to Paris for 3N/4D</strong></li>
				<li>Up to <strong>100 lucky winners</strong> will get an <strong>iPad2</strong>(16GB+3G+WiFi)</li>
				<li>Up to <strong>300 lucky winners</strong> will get an <strong>iPod Nano</strong>(8GB)</li>
				<li>Up to <strong>5000 lucky winners</strong> will get a <strong>Timex watch</strong> worth Rs. 1995</li>
				<li><strong>Assured gift coupons</strong> for everyone</li>
    		</ul> 
    		<p style="margin-bottom:10px;">All you have to do is to shop on Myntra.com for <strong>Rs. 2000 or above</strong> in a single order. Once you receive your order, you will receive a voucher with a unique code. Simply enter this unique code in your "<a href="/mymyntra.php" class="no-decoration-link">My Myntra</a>" section to claim your prize.</p>
    		<p style="margin-bottom:10px;">The more you shop, the higher your chances of winning.</p>
    		<p style="margin-bottom:10px;">So what are you waiting for ... <a href="/paris-calling" class="action-btn">SHOP NOW</a></p>
    		<p style="margin-bottom:10px;">For any queries please send a mail to <a href="mailto:pariscalling@myntra.com" class="no-decoration-link">pariscalling@myntra.com</a></p>
    		<p style="margin-bottom:10px;">*Offer Valid from <strong>March 8th 2012, 8:00 AM to April 8th 2012, Midnight</strong>. To know about the Terms& Conditions of this offer, <a href="/paris-calling-terms-conditions" target="_blank" class="no-decoration-link">Click Here</a>.</p>
    		<p style="padding-bottom:50px;"></p>
		</div>
	</div>
	{include file="site/footer.tpl"}
	 <script>
        _gaq.push(['_trackEvent', 'promocontest', 'paris-calling-page', '']);
        </script>
</body>
</html>
