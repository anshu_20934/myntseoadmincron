{include file="site/doctype.tpl"}
  <head>
    {include file="site/header.tpl" }
  </head>
  <body class="{$_MAB_css_classes}">
  {include file="site/menu.tpl" }
    <div class="container clearfix" id="container">
    <h1 class="h1-title">Shop from the Largest Online Fashion Collection</h1>
       <div class="ct-block">
            
            <div class="w188 mr10 left">
                <div class="h188 mb15 wnb bg"><a href="/mens" class="h188 clearfix"><h3 class="cl-red">Men's</h3><span class="tt">The most comprehensive catalog of men's wear!</span><div class="snbox" style="margin-left:50px">SHOP <b>NOW &raquo;</b></div></a></div>
                <div class="h188 mb15 whb bg"><a href="/womens" class="h188 clearfix"><h3 class="cl-red">Women's</h3><span class="tt">The most exclusive collection of women's wear!</span><div class="snbox" style="margin-left:50px">SHOP <b>NOW &raquo;</b></div></a></div>

                <div class="h188 mb15 mypro">
                    <ul>
                        <li><b>FREE SHIPPING</b> in India </li>
                        <li><b>CASH</b> on Delivery</li>
                        <li><b>30 DAY</b> Returns Policy</li>
                        <li><b>24 HOURS</b> Dispatch Time</li>
                    </ul>
                </div>
            </div>

                <div class="w386 mr10 left">
                     <div class="h188 adbox mb15">
                     <a href="/footwear" class="bxlink">
                        <span class="boxborder"><b></b></span>
                        <span class="boxhead w386">Footwear</span>
                        <span class="snbox">SHOP <b>NOW &raquo;</b></span>
                        <img src="{$cdn_base}/skin1/images/men-footwear-shop.jpg" alt="footwear" class="bgimg">
                     </a>
                     </div>
                     <div class="h188 adbox mb15">
                        <a href="/ethnic-wear" class="bxlink">
                            <span class="boxborder"><b></b></span>
                            <b class="boxhead w386">Ethnic Wear</b>
                            <span class="snbox">SHOP <b>NOW &raquo;</b></span>
                            <img src="{$cdn_base}/skin1/images/women-ethnicWear-shop.jpg" alt="Ethnic Wear" class="bgimg">
                        </a>
                     </div>
                     <div class="h188 adbox mb15">
                        <a href="/formal-wear" class="bxlink">
                            <span class="boxborder"><b></b></span>
                            <b class="boxhead w386">Formal Wear</b>
                            <span class="snbox">SHOP <b>NOW &raquo;</b></span>
                            <img src="{$cdn_base}/skin1/images/men-formalWear-shop.jpg" alt="Formal Wear" class="bgimg">
                        </a>
                     </div>
                </div>

            <div class="w386 right">
                <div class="h188 adbox mb15">
                 <a href="/casual-wear" class="bxlink">
                    <span class="boxborder"><b></b></span>
                    <span class="boxhead w386">Casual Wear</span>
                    <span class="snbox">SHOP <b>NOW &raquo;</b></span>
                    <img src="{$cdn_base}/skin1/images/men-casual-wear-shop.jpg" alt="Casual Wear" class="bgimg">
                 </a>
                 </div>
                 <div class="h188 adbox mb15">
                    <a href="/sports-wear" class="bxlink">
                        <span class="boxborder"><b></b></span>
                        <b class="boxhead w386">Sports Wear</b>
                        <span class="snbox">SHOP <b>NOW &raquo;</b></span>
                        <img src="{$cdn_base}/skin1/images/men-sportsWear-shop.jpg" alt="Sports Wear" class="bgimg">
                    </a>
                 </div>
                 <div class="h188 adbox">
                    <a href="/accessories" class="bxlink">
                        <span class="boxborder"><b></b></span>
                        <b class="boxhead w386">Accessories</b>
                        <span class="snbox">SHOP <b>NOW &raquo;</b></span>
                        <img src="{$cdn_base}/skin1/images/men-accessories-shop.jpg" alt="Accessories" class="bgimg">
                    </a>
                 </div>
            </div>
        </div>
        <div class="bl-block">
            <div class="ct-tilte">SHOP BY BRAND</div>

            <ul class="brandlogos">
                <li>
                    <a class="no-decoration-link" href="{$http_location}/nike">
                        <img width="120" height="70" alt="Nike" src="{$cdn_base}/images/filters/NIKE_brand_logo.jpg">
                    </a>
                </li>
                <li>
                    <a class="no-decoration-link" href="{$http_location}/puma">
                        <img width="120" height="70" alt="Puma" src="{$cdn_base}/images/filters/PUMA_brand_logo.jpg">
                    </a>
                </li>
                 <li>
                     <a class="no-decoration-link" href="{$http_location}/adidas">
                         <img width="120" height="70" alt="Adidas" src="{$cdn_base}/images/filters/ADIDAS_brand_logo.jpg">
                     </a>
                 </li>

                <li>
                    <a class="no-decoration-link" href="{$http_location}/united-colors-of-benetton">
                        <img width="120" height="70" alt="United Colors of Benetton" src="{$cdn_base}/images/filters/ucb.jpg">
                    </a>
                </li>

                <li>
                    <a class="no-decoration-link" href="{$http_location}/lee">
                        <img width="120" height="70" alt="Lee" src="{$cdn_base}/images/filters/lee_brand_logo.jpg">
                    </a>
                </li>
                <li>
                    <a class="no-decoration-link" href="{$http_location}/jealous-21">
                        <img width="120" height="70" alt="Jealous 21" src="{$cdn_base}/images/filters/JEALOUS21_brand_logo.jpg">
                    </a>
                </li>
                <li>
                   <a class="no-decoration-link" href="{$http_location}/reebok">
                        <img width="120" height="70" alt="Reebok" src="{$cdn_base}/images/filters/REEBOK_brand_logo.jpg">
                    </a>
                </li>
                <li class="blink-last">
                   <a class="no-decoration-link" class="shop-link" href="{$http_location}/sales">
                        SHOP <b>ALL BRANDS &raquo;</b>
                    </a>
                </li>
            </ul>
        </div>

    </div>
        <div style="clear:both; height:10px; overflow:hidden"></div>
        {include file="site/footer.tpl" }
  </body>
  </html>
