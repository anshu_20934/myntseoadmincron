{include file="site/doctype.tpl"}
<head>
 {include file="site/header.tpl" }

</head>
<body class="{$_MAB_css_classes}">
{include file="site/menu.tpl" }
 <div class="container clearfix" id="container">
 <h1 class="h1-title">For Women</h1>
     <!-- div class="mainbanner bg999">
         <div class="banner-text">
             <b>For</b>
             <i>Her</i>
             <span class="bt">Shop Women's Footwear, Apparel, and Accessories online.</span>
         </div>
         <div class="banner-img" id="landpage-banner">
             <img src="{$cdn_base}/skin1/images/Women_792x376_1.jpg" alt="womens">
             <img src="{$cdn_base}/skin1/images/Women_792x376_2.jpg" alt="womens">
         </div>
     </div-->

    <div class="ct-block">
         <!-- div class="ct-tilte">SHOP BY CATEGORY</div-->
         <div class="w188 mr10 left">
             <div class="h188 mb15 bg wnb"><a href="/womens#!sortby=RECENCY/page=1" class="h188 clearfix"><h3 class="cl-org f40">What's New?</h3> Latest arrivals for Women <div class="snbox" style="margin-left:50px">SHOP <b>NOW &raquo;</b></div></a></div>
             <div class="h188 mb15 bg whb"><a href="/womens#!sortby=POPULAR/page=1" class="h188 clearfix"><h3 class="cl-red f40">What's Hot?</h3> Popular Women's products <div class="snbox" style="margin-left:50px">SHOP <b>NOW &raquo;</b></div></a></div>
             
             <div class="h188 mb15 mypro">
                 <ul>
                     <li><b>FREE SHIPPING</b> in India </li>
                     <li><b>CASH</b> on Delivery</li>
                     <li><b>30 DAY</b> Returns Policy</li>
                     <li><b>24 HOURS</b> Dispatch Time</li>
                 </ul>
             </div>
         </div>

         <div class="w386 mr10 left">
              <div class="h188 adbox mb15">
              <a href="/women-footwear" class="bxlink">
                 <span class="boxborder"><b></b></span>
                 <span class="boxhead w386">Footwear</span>
                 <span class="snbox">SHOP <b>NOW &raquo;</b></span>
                 <img src="{$cdn_base}/skin1/images/women-footwear-shop.jpg" alt="Footwear" class="bgimg">
              </a>
              </div>
              <div class="h188 adbox mb15">
                 <a href="/women-ethnic-wear" class="bxlink">
                     <span class="boxborder"><b></b></span>
                     <b class="boxhead w386">Ethnic Wear</b>
                     <span class="snbox">SHOP <b>NOW &raquo;</b></span>
                     <img src="{$cdn_base}/skin1/images/women-ethnicWear-shop.jpg" alt="Ethnic Wear" class="bgimg">
                 </a>
              </div>
              <div class="h188 adbox">
                 <a href="/women-accessories" class="bxlink">
                     <span class="boxborder"><b></b></span>
                     <b class="boxhead w386">Accessories</b>
                     <span class="snbox">SHOP <b>NOW &raquo;</b></span>
                     <img src="{$cdn_base}/skin1/images/women-accessories-shop.jpg" alt="Accessories" class="bgimg">
                 </a>
              </div>
         </div>

         <div class="w386 right">
             <div class="h188 adbox mb15">
              <a href="/women-casual-wear" class="bxlink">
                 <span class="boxborder"><b></b></span>
                 <span class="boxhead w386">Casual Wear</span>
                 <span class="snbox">SHOP <b>NOW &raquo;</b></span>
                 <img src="{$cdn_base}/skin1/images/women-casualWear-shop.jpg" alt="Casual Wear" class="bgimg">
              </a>
              </div>
              <div class="h188 adbox mb15">
                 <a href="/women-sports-wear" class="bxlink">
                     <span class="boxborder"><b></b></span>
                     <b class="boxhead w386">Sports Wear</b>
                     <span class="snbox">SHOP <b>NOW &raquo;</b></span>
                     <img src="{$cdn_base}/skin1/images/women-sportsWear-shop.jpg" alt="Sports Wear" class="bgimg">
                  </a>
              </div>
              <div class="h188 adbox redbg">
                 <a href="/sale-womens" class="bxlink">
                     <span class="snbox">SHOP <b>NOW &raquo;</b></span>
                     <span class="boxthead w386">SALE!</span>
                     <b class="boxst">Upto 40% OFF</b>
                 </a>
              </div>
         </div>
     </div>
     {*BRANDS*}
      <div class="bl-block">
            <div class="ct-tilte">FEATURED WOMEN'S BRANDS</div>
            <ul class="brandlogos">
                <li>
                    <a class="no-decoration-link" href="{$http_location}/womens-w#!gender=women/page=1">
                        <img width="120" height="70" alt="W" src="{$cdn_base}/images/filters/w.jpg">
                    </a>
                </li>
                <li>
                    <a class="no-decoration-link" href="{$http_location}/womens-jealous-21#!gender=women/page=1">
						<img width="120" height="70" alt="Jealous 21" src="{$cdn_base}/images/filters/JEALOUS21_brand_logo.jpg">
					</a>
                </li>
                <li>
                    <a class="no-decoration-link" href="{$http_location}/womens-puma#!gender=women/page=1">
						<img width="120" height="70" alt="Puma" src="{$cdn_base}/images/filters/PUMA_brand_logo.jpg">
					</a>
                </li>

                <li>
                    <a class="no-decoration-link" href="{$http_location}/womens-nike#!gender=women/page=1">
						<img width="120" height="70" alt="Nike" src="{$cdn_base}/images/filters/NIKE_brand_logo.jpg">
					</a>
			    </li>

                <li>
                   <a class="no-decoration-link" href="{$http_location}/womens-aurelia#!gender=women/page=1">
						<img width="120" height="70" alt="AURELIA" src="{$cdn_base}/images/filters/Aurelia.jpg">
					</a>
                </li>

                <li>
                    <a class="no-decoration-link" href="{$http_location}/womens-united-colors-of-benetton#!gender=women/page=1">
                        <img width="120" height="70" alt="United Colors of Benetton" src="{$cdn_base}/images/filters/ucb.jpg">
                    </a>
                </li>

                <li>
                    <a class="no-decoration-link" href="{$http_location}/womens-lee#!gender=women/page=1">
						<img width="120" height="70" alt="Lee" src="{$cdn_base}/images/filters/lee_brand_logo.jpg">
					</a>
                </li>

                <li class="blink-last">
                   <a class="no-decoration-link" class="shop-link" href="{$http_location}/mens">SHOP <b>ALL BRANDS &raquo;</b></a>
                </li>
                
            </ul>
        </div>
 </div>
     <div style="clear:both; height:10px; overflow:hidden"></div>
     {include file="site/footer.tpl" }
</body>
</html>
