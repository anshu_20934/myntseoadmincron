{include file="site/doctype.tpl"}
<head>
	{include file="site/header.tpl" }
</head>
<body class="{$_MAB_css_classes}">
{include file="site/menu.tpl" }
<div class="container clearfix" id="container">
        <h1 class="headtitle">Footwear for Him</h1>
		<div class="g5 brands brands1 h590">
			<span class="btitle">Brands</span>
            <ul>
                <li class="b1"><a href="/footwear/#!sortby=POPULAR/gender=men/page=1/brands=Puma"></a></li>
                <li class="b2"><a href="/footwear/#!sortby=POPULAR/gender=men/page=1/brands=Adidas"></a></li>
                <li class="b3"><a href="/footwear/#!sortby=POPULAR/gender=men/page=1/brands=Nike"></a></li>
                <li class="b4"><a href="/footwear/#!sortby=POPULAR/gender=men/page=1/brands=FILA"></a></li>
                <li class="b5"><a href="/footwear/#!sortby=POPULAR/gender=men/page=1/brands=Red Tape"></a></li>
                <li class="b6"><a href="/footwear/#!sortby=POPULAR/gender=men/page=1/brands=Reebok"></a></li>
                <li class="b7"><a href="/footwear/#!sortby=POPULAR/gender=men/page=1/brands=Clarks"></a></li>
                <li class="b8"><a href="/footwear/#!sortby=POPULAR/gender=men/page=1/brands=Converse"></a></li>
                <li class="b9"><a href="/footwear/#!sortby=POPULAR/gender=men/page=1/brands=Ganuchi"></a></li>
                <li class="b10"><a href="/footwear/#!sortby=POPULAR/gender=men/page=1/brands=ASICS"></a></li>
            </ul>
            <a href="/brands" class="all">ALL FOOTWEAR BRANDS &raquo;</a>
        </div>
		<div class="g5 mr10">
			<div class="adbox h290">
				<a href="/footwear#!sortby=RECENCY/page=1/gender=men" class="btitle">What's New?</a>
                <a href="/footwear#!sortby=RECENCY/page=1/gender=men">
				    <img src="{$cdn_base}/skin1/images/whatsnew.png">
                    <span class="prod-name">ID Men Casual Black Shoes</span>
                </a>
                <a href="/footwear#!sortby=RECENCY/page=1/gender=men" class="all">ALL NEW FOOTWEAR &raquo;</a>
			</div>
		</div>
		
		<div class="g5 mr10">
			<a href="/footwear-sports-shoes#!sortby=POPULAR/page=1/gender=men" class="adbox h290">
				<b class="btitle">Sports Shoes</b>
				<img src="{$cdn_base}/skin1/images/mesh.png" class="mesh">
				<img src="{$cdn_base}/skin1/images/SportsFootwear_180x240.png">
				<i class="snb w188">&nbsp;</i>
			</a>
		</div>
		<div class="g5 mr10">
			<a href="/footwear-casual-shoes#!sortby=POPULAR/page=1/gender=men" class="adbox h290">
				<b class="btitle">Casual Shoes</b>
				<img src="{$cdn_base}/skin1/images/mesh.png" class="mesh">
				<img src="{$cdn_base}/skin1/images/CasualFootwear_180x240.png">
				<i class="snb w188">&nbsp;</i>
			</a>
		</div>
		<div class="g5 mr10">
			<a href="/footwear-formal-shoes#!sortby=POPULAR/page=1/gender=men" class="adbox h290">
				<b class="btitle">Formal Shoes</b>
				<img src="{$cdn_base}/skin1/images/mesh.png" class="mesh">
				<img src="{$cdn_base}/skin1/images/FormalFootwear_180x240.png">
				<i class="snb w188">&nbsp;</i>
			</a>
		</div>
		<div class="g5 mr10 mt10">
			<div class="adbox h290">
				<a href="/footwear#!sortby=POPULAR/page=1/gender=men" class="btitle">What's Hot?</a>
                <a href="/footwear#!sortby=POPULAR/page=1/gender=men">
				    <img src="{$cdn_base}/skin1/images/whatshot.png">
                    <span class="prod-name">Adidas Unisex High Can Blue Shoe</span>
                </a>
                <a href="/footwear#!sortby=POPULAR/page=1/gender=men" class="all">ALL POPULAR FOOTWEAR &raquo;</a>
			</div>
		</div>
		<div class="g5 mr10 mt10 store">
			<div class="bd">
				<img src="{$cdn_base}/skin1/images/footwear.png">
                <span class="title">THE MYNTRA FOOTWEAR STORE</span>
                <ul>
                    <li><strong>FREE SHIPPING</strong> IN INDIA</li>
                    <li><strong>CASH</strong> ON DELIVERY</li>
                    <li><strong>30 DAY</strong> RETURN POLICY</li>
                    <li><strong>24 HOURS</strong> DISPATCH TIME</li>
                </ul>
            </div>
		</div>
		<div class="g5 mr10 mt10">
			<a href="/footwear-sandals#!sortby=POPULAR/page=1/gender=men" class="adbox h290">
				<b class="btitle">Sandals</b>
				<img src="{$cdn_base}/skin1/images/mesh.png" class="mesh">
				<img src="{$cdn_base}/skin1/images/Sandals_180x240.png">
				<i class="snb w188">&nbsp;</i>
			</a>
		</div>
		<div class="g5 mr10 mt10">
			<a href="/footwear-slippers#!sortby=POPULAR/page=1/gender=men" class="adbox w188 h140">
				<b class="btitle">Flip-Flops & Slippers</b>
				<i class="snb w188">&nbsp;</i>
			</a>
		</div>		
		<div class="g5 mr10 mt10">
			<a href="/sale-footwear#!sortby=DISCOUNT/page=1/gender=men" class="adbox w188 h140">
				<b class="btitle cred">Sale!</b>
				<b class="btext prl35">Upto 30% Off</b>
				<i class="snb w188">&nbsp;</i>
			</a>
		</div>
	</div>
		<div style="clear:both; height:10px; overflow:hidden"></div>
        {include file="site/footer.tpl" }   
</body>
</html>
