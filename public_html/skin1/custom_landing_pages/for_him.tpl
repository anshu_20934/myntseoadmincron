{include file="site/doctype.tpl"}
  <head>
    {include file="site/header.tpl" }
    
      
  </head>
  <body class="{$_MAB_css_classes}">
  {include file="site/menu.tpl" }
    <div class="container clearfix" id="container">
    <h1 class="h1-title">For Men</h1>
        <!-- div class="mainbanner bg999">
            <div class="banner-text">
                <b>For</b>
                <i>Him</i>
                <span class="bt">Shop Men's Footwear, Apparel, and Accessories online.</span>
            </div>
            <div class="banner-img" id="landpage-banner">
                <img src="{$cdn_base}/skin1/images/Men_792x376_1.jpg" alt="MEN">
                <img src="{$cdn_base}/skin1/images/Men_792x376_2.jpg" alt="MEN">
            </div>
        </div-->

       <div class="ct-block">
            <!-- div class="ct-tilte">SHOP BY CATEGORY</div-->
            <div class="w188 mr10 left">
                <div class="h188 bg mb15 bg wnb"><a href="/mens#!sortby=RECENCY/page=1" class="h188 clearfix"><h3 class="cl-org f40">What's New?</h3>Latest arrivals for Men<div class="snbox" style="margin-left:50px">SHOP <b>NOW &raquo;</b></div></a></div>
                <div class="h188 bg mb15 bg whb"><a href="/mens#!sortby=POPULAR/page=1" class="h188 clearfix"><h3 class="cl-red f40">What's Hot?</h3>Popular Men's products<div class="snbox" style="margin-left:50px">SHOP <b>NOW &raquo;</b></div></a></div>

                <div class="h188 mb15 mypro">
                    <ul>
                        <li><b>FREE SHIPPING</b> in India </li>
                        <li><b>CASH</b> on Delivery</li>
                        <li><b>30 DAY</b> Returns Policy</li>
                        <li><b>24 HOURS</b> Dispatch Time</li>
                    </ul>
                </div>
            </div>

            <div class="w386 mr10 left">
                 <div class="h188 adbox mb15">
                 <a href="/men-footwear" class="bxlink">
                    <span class="boxborder"><b></b></span>
                    <span class="boxhead w386">Footwear</span>
                    <span class="snbox">SHOP <b>NOW &raquo;</b></span>
                    <img src="{$cdn_base}/skin1/images/men-footwear-shop.jpg" alt="Footwear" class="bgimg">
                 </a>
                 </div>
                 <div class="h188 adbox mb15">
                    <a href="/men-formal-wear" class="bxlink">
                        <span class="boxborder"><b></b></span>
                        <b class="boxhead w386">Formal Wear</b>
                        <span class="snbox">SHOP <b>NOW &raquo;</b></span>
                        <img src="{$cdn_base}/skin1/images/men-formalWear-shop.jpg" alt="Formal Wear" class="bgimg">
                    </a>
                 </div>
                 <div class="h188 adbox mb15">
                    <a href="/men-accessories" class="bxlink">
                        <span class="boxborder"><b></b></span>
                        <b class="boxhead w386">Accessories</b>
                        <span class="snbox">SHOP <b>NOW &raquo;</b></span>
                        <img src="{$cdn_base}/skin1/images/men-accessories-shop.jpg" alt="Accessories" class="bgimg">
                    </a>
                 </div>
            </div>

            <div class="w386 right">
                <div class="h188 adbox mb15">
                 <a href="/men-casual-wear" class="bxlink">
                    <span class="boxborder"><b></b></span>
                    <span class="boxhead w386">Casual Wear</span>
                    <span class="snbox">SHOP <b>NOW &raquo;</b></span>
                    <img src="{$cdn_base}/skin1/images/men-casual-wear-shop.jpg" alt="Casual Wear" class="bgimg">
                 </a>
                 </div>
                 <div class="h188 adbox mb15">
                    <a href="/men-sports-wear" class="bxlink">
                        <span class="boxborder"><b></b></span>
                        <b class="boxhead w386">Sports Wear</b>
                        <span class="snbox">SHOP <b>NOW &raquo;</b></span>
                        <img src="{$cdn_base}/skin1/images/men-sportsWear-shop.jpg" alt="Sports Wear" class="bgimg">
                    </a>
                 </div>
                 <div class="h188 adbox redbg">
                    <a href="/sale-mens" class="bxlink">
                        <span class="snbox">SHOP <b>NOW &raquo;</b></span>
                        <span class="boxthead w386">SALE!</span>
                        <b class="boxst">Upto 40% OFF</b>
                    </a>                        
                 </div>
            </div>
        </div>
        <div class="bl-block">
            <div class="ct-tilte">FEATURED MEN'S BRANDS</div>
            <ul class="brandlogos">
                <li>
                    <a class="no-decoration-link" href="{$http_location}/men-nike#!gender=men/page=1">
						<img width="120" height="70" alt="Nike" src="{$cdn_base}/images/filters/NIKE_brand_logo.jpg">
					</a>
			    </li>
			    <li>
                    <a class="no-decoration-link" href="{$http_location}/men-puma#!gender=men/page=1">
						<img width="120" height="70" alt="Puma" src="{$cdn_base}/images/filters/PUMA_brand_logo.jpg">
					</a>
			    </li>
                <li>
                    <a class="no-decoration-link" href="{$http_location}/men-adidas#!gender=men/page=1">
                        <img width="120" height="70" alt="Adidas" src="{$cdn_base}/images/filters/ADIDAS_brand_logo.jpg">
                    </a>
                </li>

                <li>
                    <a class="no-decoration-link" href="{$http_location}/men-wrangler#!gender=men/page=1">
						<img width="120" height="70" alt="Wrangler" src="{$cdn_base}/images/filters/WRANGLER.jpg">
					</a>
                </li>

                <li>
                    <a class="no-decoration-link" href="{$http_location}/men-proline#!gender=men/page=1">
						<img width="120" height="70" alt="Proline" src="{$cdn_base}/images/filters/PROLINE.jpg">
					</a>
                </li>

                <li>
                    <a class="no-decoration-link" href="{$http_location}/men-united-colors-of-benetton#!gender=men/page=1">
						<img width="120" height="70" alt="United Colors of Benetton" src="{$cdn_base}/images/filters/ucb.jpg">
					</a>
                </li>

                <li>
                    <a class="no-decoration-link" href="{$http_location}/men-fila#!gender=men/page=1">
						<img width="120" height="70" alt="FILA" src="{$cdn_base}/images/filters/FILA_brand_logo.jpg">
					</a>
                </li>
                <li class="blink-last">
                   <a class="no-decoration-link" class="shop-link" href="{$http_location}/mens">SHOP <b>ALL BRANDS &raquo;</b></a>
                </li>
            </ul>

        </div>
    </div>
        <div style="clear:both; height:10px; overflow:hidden"></div>
        {include file="site/footer.tpl" }
  </body>
  </html>
