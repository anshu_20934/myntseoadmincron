<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html xml:lang="en" xmlns:fb="http://www.facebook.com/2008/fbml">

<META http-equiv="Content-Style-Type" content="text/css">
<META http-equiv="Content-Script-Type" content="type/javascript">
<META http-equiv="Content-Type" content="text/html; charset=utf-8">
{include file="site/header.tpl" }
<!--[if lt IE 8]>
    <link rel="stylesheet" href="new-skin-css/ie.css" type="text/css" media="screen, projection"><![endif]-->
	{literal}
	<style type="text/css">
		.faqs b{display:block;font-size:12px;color:#000;}
		.faqs i{color: #003399;display: inline;font-style: normal;font-weight: bold;list-style-type: decimal;text-indent: 10px;}
		.faqs ul{list-style-type:decimal;}
		.faqs ul li{line-height:25px;}
		.faqs ul li ul{list-style-type:disc;}
		.faqs ul li ul li{line-height:20px; margin-bottom: 5px;}
		.faqs div{margin-bottom:20px;}
		.bb{padding-bottom:10px;}
		div.bb{padding-bottom:0px;}
		.faqs p strong{padding:10px 0px;font-weight:normal;display:inline;}
	</style>
	{/literal}
</head>

<body class="{$_MAB_css_classes}">
	{include file="site/menu.tpl" }
	<div class="container clearfix">

    <h2 class="h4">Myntra's Shipping Policy</h2>
    {if $shippingRate > 0}
	    <div>
	        We strive to deliver products purchased from Myntra in excellent condition and in the fastest time possible. 
	        For all purchases above Rs. {$free_shipping_amount}/- we will deliver the Order to your doorstep FREE of cost. 
	        A shipping charge of Rs. {$shippingRate}/- will be applicable to all Orders under Rs. {$free_shipping_amount}/-. Read on to know more.
	    </div>
	    <ul>
	        <li style="list-style-type: square; margin: 5px 0px;">If the Order is cancelled, lost or un-delivered to your preferred location, we will refund the complete Order amount including shipping charges.</li>
	        <li style="list-style-type: square; margin: 5px 0px;">If you cancel part of the Order, shipping charges will not be refunded.</li>
	        <li style="list-style-type: square; margin: 5px 0px;">If you return an Order delivered to you, original shipping charges will not be refunded. However if you self-ship your Returns, you will be reimbursed based on Myntra's <a href="myntrareturnpolicy.php">Returns Policy</a>.</li>
	    </ul>
    {else}
        <p>Myntra offers free shipping within India on all products purchased at its website.</p>
    {/if}

	<h1 class="head1">Frequently Asked Questions (FAQs)</h1>
    <div class="content clearfix" style="position:relative;">
    	<div class="content-left left span-23 prepend">
	      <h2 class="head3">Shipping <a name="shipping" ></a></h2>
          
          <p><strong>What are the shipping charges on Myntra products?</strong></p>
          <p>
            {if $shippingRate > 0 }
                Myntra offers free shipping within India on all products if your total Order amount is Rs. {$free_shipping_amount}/- or more. Otherwise Rs. {$shippingRate}/- will be levied as Shipping Charges.
            {else}
                Myntra offers free shipping within India on all products purchased at its website.
            {/if}
          </p>
          
          <p><strong>How long will it take to receive the ordered products?</strong></p>
          <p>We attempt to process every order and ship it within 24 hrs. Exact shipping time depends on the availability of certain products and will be communicated during the process of ordering. Depending upon your location within India, post ordering this should take overall 5-7 days to get delivered. In some cases it may take longer to receive the shipment depending on your location and accessibility.</p>
          <p><strong>Does Myntra deliver products outside India?</strong></p>
          <p>Myntra does not deliver products outside India. You can place an Order on our website from anywhere in the world, however please ensure that the Delivery address is within India.</p>

			<p class="go-to-top"><a href="mkfaq.php#" class="go-to-top-link no-decoration-link">Go to Top</a></p>
          	<p class="placeholder"><a name="payments"></a></p>
	        <h2 class="head3">Payments</h2>
          <p><strong>What are the payment options available at Myntra?</strong></p>
          <p>The payment options we support are:
              <ul>
            	<li> Cash On Delivery</li>
          		<li> Credit card</li>
          		<li> Net banking</li>
          		<li> ITZ Cash payments</li>

          	  </ul>
            We provide option of CCAvenue and EBS payment gateways which process all credit card and net banking transactions over secure encrypted connection</p>
          <!-- <p><strong>Where to send Demand Draft and whom it should be addressed to?</strong></p>
          <p>We request you to address the Demand Draft to "Vector E-commerce Pvt. Ltd" Please mail it to the following address:
             <p>Vector E-commerce Pvt. Ltd</p>
             <p>#1546/47, 19th Main,</p>
             <p>Sector I, HSR Layout,</p>
             <p>Bangalore - 560102, India</p>
             <p>Phone: 080-43540100</p>
             {if $customerSupportCall}
             <p>Customer Service : {$customerSupportCall}</p>
             {/if}
          </p> -->
          <p><strong>Do you provide Cash On Delivery (COD) payment option? How can I buy using COD option?</strong></p>
          <p>Yes, Myntra offers option to pay cash on delivery of products. <br/><br/>Here are steps to place order using COD option: <br/>
            1. You can select 'Pay Cash on Delivery' option while placing the order.<br/>
			2. You will be requested to provide your mobile number for verification.<br/>
			3. You will be provided with a phone number to call. Your call will be auto disconnected immediately after connecting. You will not be charged for this call.<br/>
			4. Once your mobile number is verified, your order will be automatically placed and an email will be sent to you.<br/>
			5. On receiving your consignment, pay the invoice amount in cash to the courier delivery person.<br/>
			<br/>Please have enough cash ready to make the payment.</p>
          <p><strong>Terms & Conditions for Cash On Delivery (COD) payment</strong></p>
          <p>1. COD is available for orders above Rs. 300 and below Rs. 10000 <br/>
			2. Your delivery address should be under serviceable network of our courier partner. Please enter correct pin code only to ensure delivery.<br/>
			3. Please pay in cash only and do NOT offer cheque or DD to the courier staff. <br/>
			4. Please do NOT pay for any additional charges i.e. octroi etc. to courier staff. Your invoice amount is inclusive of all charges.<br/>
			5. <b><i>Cancelling your COD Order: </i></b>Once your mobile phone is successfully verified your order will be automatically placed.<br/>
			If you want to cancel the order, please do so within 10 hours of placing it, post which it will processed and shipped. <br/><br/>
			{if $customerSupportCall}
				Please call on Customer Care : {$customerSupportCall} {if $customerSupportTime}{$customerSupportTime}{/if}</p>
			{/if}
          <p><strong>What should I do if my payment fails?</strong></p>
          <p>In case of successful payment, you will receive an email confirmation from Myntra. In case of payment failure, please retry ensuring:
            <ul>
            	<li>Information passed on to payment gateway is accurate ie. account details, billing address, password(in case net banking)</li>
            	<li>Your internet connection is not disrupted in the process</li>
            </ul>
            If your account has been debited after a payment failure, it will be rolled back within 24 hrs. You can get in touch with <a href="{$http_location}/contactus">Customer Care</a> with your order number or email on <a href="mailto:support@myntra.com ">support@myntra.com </a> for any clarifications. </p>

			<p class="go-to-top"><a href="mkfaq.php#" class="go-to-top-link no-decoration-link">Go to Top</a></p>
			<p class="placeholder"><a name="return"></a></p>
	        <h2 class="head3">Return/Modify/Cancel</h2>
 		  <p><strong>Can I return products I am not fully satisfied with?</strong></p>
          <p>To ensure your shopping experience with Myntra is completely hassle-free, we provide you the option to return products in a convenient and transparent manner.  To be eligible for returns, the products must be in unused and resaleable condition. You can place a request for returns through <a href="{$http_location}/contactus">Customer Care</a> or through My Myntra within 30 days of receiving your product. <br> For returns-related FAQs and more details, please refer to our new <a href="{$http_location}/myntrareturnpolicy.php">Returns Policy and FAQs</a>.</p>
 		  <p><strong>Can I cancel the order after it is placed?</strong></p>
          <p>While there is no option for customer to cancel the order on their own once the payment is made, you can get in touch with <a href="{$http_location}/contactus">Customer Care</a> as soon as possible with your order number. As long as your order has not been processed, we can cancel it and refund your amount.</p>
          <p><strong>Can I modify my order after placing it?</strong></p>
          <p>We understand that in cases, you may want to alter the ordered size or colour option of a product in your placed order. You may even require to have your shipping address updated. These changes are allowed till we have begun processing your order. For such requests, please get in touch with <a href="{$http_location}/contactus">Customer Care</a> immediately.</p>

          <p class="go-to-top"><a href="mkfaq.php#" class="go-to-top-link no-decoration-link">Go to Top</a></p>
          <p class="placeholder"><a name="ordering"></a></p>
	        <h2 class="head3">Ordering & Tracking</h2>

          <p><strong>How do I check the status of my order?</strong></p>
          <p>You can check the status of your order by following steps:
            <ul>
	            <li> Log on to your account by signing in. </li>
	            <li> Click on 'my account' on top right of website header. </li>
	            <li> Click on 'my order history' under the Shopping section.</li>
	            <li> Search the order by date by specifying the duration since the order has been placed.</li>
	            <li> Search results with your order number and the details of the status will appear.</li>
             </ul></p>
          <p><strong>How can I track my product shipment?</strong></p>
          <p>Orders once processed and shipped can be tracked using the consignment/tracking number. A mail is sent to the customer after the order is shipped with this tracking number along with the service provider Blue Dart or DTDC. In order to track the order:
             <ul>
	             <li>Go to website- http://www.bluedart.com/ or http://dtdc.in/</li>
	             <li>Type in the tracking number provided in the mail in the Tracking section of the left panel. A window with the status pops up.</li>
             </ul>
             If your order status shows transit: Click on additional details. Details such as where your order has reached can be viewed.</p>
          <p><strong>How do I redeem a coupon?</strong></p>
          {if $condNoDiscountCouponFG}
          <p>You can redeem a coupon on all purchases of non-discounted items. Here is how you can redeem it:
            <ul>
	           <li>Select the design, product style, and quantity.</li>
	           <li>Click on 'add to cart'. A page with the sections 'discount coupon section' and 'your shopping cart' will appear. The 'discount coupon' section is for redeeming the discount, while the details of order will be shown in the 'your shopping cart' section.</li>
	           <li>Type the coupon code in the 'discount coupon section' and click redeemed.The coupon will be applied on all non-discounted items in your Bag. The amount value changes in 'your shopping cart' section.</li>
	           <li>Click on buy now to proceed and make the payment</li>
            </ul></p>
          {else}
          	 <p><strong>How do I redeem a coupon?</strong></p>
          	<p>You can redeem the coupons while you buy the product. Here is how you can redeem it:
            <ul>
	           <li>Select the design, product style, and quantity.</li>
	           <li>Click on 'add to cart'. A page with the sections 'discount coupon section' and 'your shopping cart' will appear. The 'discount coupon' section is for redeeming the discount, while the details of order will be shown in the 'your shopping cart' section.</li>
	           <li>Type the coupon code in the 'discount coupon section' and click redeemed. The amount value changes in 'your shopping cart' section.</li>
	           <li>Click on buy now to proceed and make the payment</li>
            </ul></p>
          {/if}
          <p><strong>How will I know if order is placed successfully?</strong></p>
          <p>You will receive an email confirmation from Myntra once your order is successfully placed. This mail will have all the details related to your order.</p>
			<p class="go-to-top"><a href="mkfaq.php#" class="go-to-top-link no-decoration-link">Go to Top</a></p>
	
	<div class="faqs">
	<p class="placeholder"><a name="mrp" ></a></p>
	<h2 class="head3">Mynt Club - Rewards and Loyalty Program</h2>
	<p class="bb">
	    <b>How do I become part of Mynt Club?</b>
		To earn the benefits of Mynt Club, all you need to do is register with Myntra.com. If you're registered with Myntra, then you're already a member of the Mynt Club. Get set to enjoy all the benefits of the program.
    </p>
    <p>
	<b>How do I benefit as a member of Mynt Club?</b>
        <i>- Sign up and earn</i><br>
        Register with Myntra to instantly earn Rs {$nonfbregValue}. This will be added to your Mynt Club account as {$mrpCouponConfiguration.mrp_firstLogin_nonfbreg_textNumCoupons} credits of Rs {$mrpCouponConfiguration.mrp_firstLogin_nonfbreg_mrpAmount} each, valid for {$mrpCouponConfiguration.mrp_firstLogin_nonfbreg_validity} days, on purchases of Rs {$mrpCouponConfiguration.mrp_firstLogin_nonfbreg_minCartValue|number_format:0:'':','} and above.<br>
		Alternatively, you could register with Myntra using "Connect with Facebook" option, and instantly earn Rs.{$fbregValue}. This will be added to your Mynt Club account as {$mrpCouponConfiguration.mrp_firstLogin_fbreg_textNumCoupons} credits of Rs {$mrpCouponConfiguration.mrp_firstLogin_fbreg_mrpAmount} each, valid for {$mrpCouponConfiguration.mrp_firstLogin_fbreg_validity} days and on purchases of Rs {$mrpCouponConfiguration.mrp_firstLogin_fbreg_minCartValue|number_format:0:'':','} and above.  <br>
		Please note that you would be getting the benefit based on the method you use to sign up on Myntra for the first time. 
    <p>
        <i>- Refer and Earn</i><br>
                Refer your friends and earn more credits! You earn Rs. {$refregValue} when each of your referral registers with Myntra. This is added to your Mynt Club account as {$mrpCouponConfiguration.mrp_refRegistration_textNumCoupons} credits of Rs.{$mrpCouponConfiguration.mrp_refRegistration_mrpAmount}, valid for {$mrpCouponConfiguration.mrp_refRegistration_validity} days. You can redeem this against any purchase of Rs.{$mrpCouponConfiguration.mrp_refRegistration_minCartValue|number_format:0:'':','} and above.<br>
                You also earn Rs.{$refpurValue} when your referred friends make their first purchase. This is added to your Mynt Club account as {$mrpCouponConfiguration.mrp_refFirstPurchase_textNumCoupons} credits of Rs. {$mrpCouponConfiguration.mrp_refFirstPurchase_mrpAmount} each, valid for {$mrpCouponConfiguration.mrp_refFirstPurchase_validity} days. You can redeem these against purchases of Rs. {$mrpCouponConfiguration.mrp_refFirstPurchase_minCartValue|number_format:0:'':','} and above.
        </p>

	<p class="go-to-top"><a href="mkfaq.php#" class="go-to-top-link no-decoration-link">Go to Top</a></p>
    <div  class="bb">
        <b>How can I refer my friends to join the Mynt Club?</b>
		   Sign in, and go to "My Referrals" tab under "My Myntra" to send out invitations in several easy ways:
        <ul>
            <li>Select contacts from your email address book
                <ul>
                    <li>Click on "Add your contacts from" to choose email addresses of friends from your Gmail, Yahoo mail, AOL mail or Outlook accounts.</li>
                    <li>Follow the steps to select the contacts you wish to invite. You can also manually type in email addresses, separating each one with a comma.</li>
                </ul>
            </li>
            <li>Send invites on Facebook - Click on the Facebook icon, follow the steps to invite your friends through message or wall post.</li>
            <li>Send invites on Twitter - Post your invitation as a tweet by clicking the Twitter icon.</li>
            <li>Distribute your invitation link through your blog, social network, instant messengers, website by pasting your referral link along with a personalised message for your friends.</li>
        </ul>
    </div>
    <p class="bb">
        <b>How do I view my Mynt Club credits?</b>
		Your Mynt Club credits comprise of Sign up and referral bonuses. You can view these in the <strong>My Credits</strong> tab of your My Myntra account. Remember to redeem your credits before they expire!    
    </p>
    
    <p class="bb">
        <b>I have not received my Mynt credits. What should I do?</b>
        Check your spam folder and add <a href="mailto:no-reply@myntra.com">no-reply@myntra.com</a> and <a href="mailto:support@myntra.com">support@myntra.com</a> to your address book to ensure delivery of all mails from Mynt Club.<br>
		If the above did not work, write to us at support@myntra.com or call our Customer Care on +91-80-43541999 anytime of the day for assistance.
		</p>
    <div class="bb">
        <b>How can I redeem my Mynt Club credits?</b>
		You will be required to verify your mobile number before you can start redeeming your credits. On initiating the verification process, you will be sent a code via SMS. Please enter this code on the website to complete the verification.<br>
		Please note that we currently support only Indian mobile numbers on our verification process.<br>
		At checkout, you can choose to apply any of the following benefits -
		<ul>
			<li>Sign up or referral credits</li>
			<li>Any other discount voucher from Myntra.com</li>
		</ul>
	</div>
	<p class="go-to-top"><a href="mkfaq.php#" class="go-to-top-link no-decoration-link">Go to Top</a></p>
    </div>
	
	</div>
			
			
			<div class="content-right right black-bg5 corners last" style="display: block; margin-left: 780px; *margin-left:80px; position: fixed; width: 200px;">
				<ul class="links mt10 mb10">
					<li><a class="no-decoration-link" href="mkfaq.php#shipping">Shipping</a></li>
            		<li><a class="no-decoration-link" href="mkfaq.php#payments">Payments</a></li>
            		<li><a class="no-decoration-link" href="mkfaq.php#return">Return/Modify/Cancel</a></li>
            		<li><a class="no-decoration-link" href="mkfaq.php#ordering">Ordering /Tracking</a></li>
            		<li><a class="no-decoration-link" href="mkfaq.php#mrp">Mynt Club</a></li>
	            </ul>

			</div>
		</div>
		</div>
	<div class="divider">&nbsp;</div>
{include file="site/footer.tpl"}
{literal}
<script>
$(document).ready(function(){
var myBox   = $('.content-right');
var position = $('.mynt-footer').offset();
var bHeight = myBox.height();
var bWidth  = myBox.width();
var wHeight = $(window).height();

$(window).scroll(function () {
    if (myBox.offset().top + 300 > position.top) {
        $(myBox).hide();
    }else{
     $(myBox).show();
    }
});

});
</script>
{/literal}
</body>
</html>
