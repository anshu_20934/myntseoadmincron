<div id='searchbox'>
 <form name="basicsearchform" action='{$http_location}/include/search/' method ='post'>
	<div>
   
    <input type="hidden" name="simple_search" value="Y" />
	<input type="hidden" name="mode" value="search" />
	<input type="hidden" name="posted_data[by_title]" value="Y" />
	<input type="hidden" name="posted_data[by_shortdescr]" value="Y" />
	<input type="hidden" name="posted_data[including]" value="any" />
	<input type="hidden" name="posted_data[by_fulldescr]" value="Y" />
	<!-- make the search context sensitive by product type -->
	<input type="hidden" name="posted_data[by_type]" value="{$selectedproductypid}" />
	<input type="hidden" name="posted_data[by_TypeName]" value="{$selectedproductypename}" />
	<!-- make the search context sensitive by category -->   
	<input type="hidden" name="posted_data[by_category]" value="{$catid}" />
	<input type="hidden" name="posted_data[by_categoryName]" value="{$catname}" />
	<!-- make the search limited to that shop only -->
	<!--  <input type="hidden" name="posted_data[by_shop]" value="{$shop_name}" />-->
    <input style="padding-left:0px;padding-right:0px;" type="text" name="query" id="gosearch" {if $search_key} value="{$search_key}" {else} value="search here" {/if} onfocus="document.getElementById('gosearch').value=''; ">
    <input class="submit" type="submit" style="padding-left:0px;padding-right:0px;" name="submitButton" value="search" border="0" onClick="return formSubmit();">
    <!--&nbsp;&nbsp;Find a unique design from our library of 1000s of design &nbsp;&nbsp;&nbsp;&nbsp;-->
    <br>
   	 <!--<a href="mkadvancedsearch.php">Advanced Search</a>... -->
   
	</div>
	 </form>
	
</div>
{literal}
<script language="javascript">
  function formSubmit(){
	var str = document.getElementById("gosearch").value;
	var str=str.replace(" ","-");
	
	if((str == "")||(str == "search here")||(str == "search other designs here")){
		alert("Please enter search keyword.")	;
		return false
	}else{
		document.basicsearchform.action = http_loc+'/'+str+'/search/'
		document.basicsearchform.submit();
	}
  }

  </script>
{/literal}
