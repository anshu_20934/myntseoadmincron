<!-- shopping cart html starts-->
	<hr class="space clear mb10">
    <div class="content clearfix">
    	<div class="content-left left span-24">
    		<table class="cart-summary">
    			<thead>
    				<tr>
    					<td class="corners-tl-bl black-bg5 prepend" colspan="2">Shopping Cart Items</td>
    					<td class="black-bg5">Size</td>
    					<td class="black-bg5">Quantity</td>
    					<td class="corners-tr-br black-bg5">Price</td>
    				</tr>
    			</thead>
    			<tbody>
    				<tr>
    					<td class="product-details prepend">
    						<div class="product-image left">
    							<img class="left" src="{$http_location}/skin1/myntra_images/shoe.png" alt="Image Not Available">
    							<a href="javascript:void(0);" class="no-decoration-link remove-link left">Remove</a>
    						</div>
    					</td>
    					<td class="product-name append-1">Adidas Black Tshirt</td>
    					<td><select id="t-shirt-size" class="filter-select cart-options">
    							<option value="1">Small</option>
  								<option value="2">Medium</option>
  								<option value="3">Large</option>
  								<option value="3">X-Large</option>
    						</select>	
    					</td>
    					<td><select id="quantity" class="filter-select cart-options">
    							<option value="1">1</option>
  								<option value="2">2</option>
  								<option value="3">3</option>
  								<option value="4">4</option>
    						</select>
    					</td>
    					<td class="pricing-details">
    						<span class="discount-price">Rs 1200</span>
							<span class="original-price strike">Rs 1400</span>
    					</td>
    				</tr>
    				<tr>
    					<td class="product-details prepend">
    						<div class="product-image left">
    							<img class="left" src="{$http_location}/skin1/myntra_images/shoe.png" alt="Image Not Available">
    							<a href="javascript:void(0);" class="no-decoration-link remove-link left">Remove</a>
    						</div>
    					</td>
    					<td class="product-name append-1">Adidas Black Tshirt</td>
    					<td><select id="t-shirt-size" class="filter-select cart-options">
    							<option value="1">Small</option>
  								<option value="2">Medium</option>
  								<option value="3">Large</option>
  								<option value="3">X-Large</option>
    						</select>	
    					</td>
    					<td><select id="quantity" class="filter-select cart-options">
    							<option value="1">1</option>
  								<option value="2">2</option>
  								<option value="3">3</option>
  								<option value="4">4</option>
    						</select>
    					</td>
    					<td class="pricing-details">
    						<span class="discount-price">Rs 1200</span>
							<span class="original-price strike">Rs 1400</span>
    					</td>
    				</tr>
       			</tbody>
    		</table>
    		<div class="right span-8 prepend-1 last">
	    			
    		</div>
    	</div>
    	<div class="content-right right span-8 prepend-1 last">
    		<div class="pay-now span-8 need-help black-bg5 corners clearfix">
    			<div class="title corners-tl-tr">PAY NOW</div>
    			<div class="redeem clearfix">
	    			<span>Have a coupon?</span>
	    			<input type="text" name="coupon-code" id="coupon-code" class="coupon-code left">
	    			<input type="button" class="redeem-btn corners black-bg5 right" value="Redeem">
	    		</div>
	    		<div class="order-summary clearfix">
    				<div class="total clearfix">
    					<div class="clearfix col-first left">TOTAL:</div>
    					<div class="clearfix col-second left">
    						<span class="discount-price">Rs <span class="cost">2400</span></span>
							<span class="original-price strike">Rs 2800</span>
						</div>
    				</div>
    				<div class="make-payment right">
    					<a href="#" class="make-payment-link orange-bg corners">Make Payment</a>
    					<span class="note">Click the button above to make the payment & complete your order.</span>
    				</div>
    			</div>
    		</div>
    	</div>
	</div>
	<hr class="space clear mb10">

<!-- shopping cart html ends -->