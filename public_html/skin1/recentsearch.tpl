{* $Id: mykritihome.tpl,v 1.88.2.3 2006/07/19 10:19:35 max Exp $ *}
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
{config_load file="$skin_config"}
<html>

{include file="mykritimeta.tpl" }

 {literal}
 <style>
.recently_viewed_title{
background:#FFF085 none repeat scroll 0 0;
border-color:#EEE085;
border-style:solid;
border-width:1px 1px 3px;
color:#000000;
cursor:default;
display:block;
font-family:Arial,Verdana,sans-serif;
font-size:85%;
font-weight:bold;
padding:2px 0 2px 10px;
text-align:left;
}

</style>
 {/literal}
<body id="body"  onload="javascript:showaddress('{$addressoption}', '{$termandcondition}', '{$userinfo.termCondition}');">
	<div id="scroll_wrapper">
		<div id="container">
			{include file="mykrititop.tpl" }
			<div id="wrapper">
      				{include file="breadcrumb.tpl" }
    				<div id="display">
					<div class="menu_column">{include file="leftpane.tpl" }</div>	
					<div class="center_column">
						<div class="form">
							<span class="recently_viewed_title">recent searches</span>
							<div class="links">
								<div style="float:right">{$paginator}</div><div class="clearall"></div>
								{section name=keyword loop=$recentsearches}
								<div style="padding:10px 10px 10px 10px">
									<div style="height:190px;background-color:#F3F3F3;padding:10px 10px 10px 10px">
				
										<div><a href="{$http_location}/{$recentsearches[keyword].filter_keyword}/search/">more {$recentsearches[keyword].orignal_keyword} products</a></div> <div class="clearall"></div>
										<div style="padding:10px 10px 10px 10px;height:140px;">
											{section name=design loop=$recentsearches[keyword].designs}
												<div style="float:left;padding-left:10px;text-align: center;background-image: url(./images/b.gif);width: 100px; height: 100px; vertical-align: middle;"><a href="{$http_location}/{$recentsearches[keyword].designs[design][3]|replace:" ":"-"}/{$recentsearches[keyword].designs[design][2]|replace:" ":"-"}/FC/PD/{$recentsearches[keyword].designs[design][0]}"><img src="{$cdn_base}/{$recentsearches[keyword].designs[design][7]}" alt="{$recentsearches[keyword].designs[design][3]}_{$recentsearches[keyword].designs[design][2]}" width="100px" height="100px" style="cursor:pointer"/><div><strong>{$recentsearches[keyword].designs[design][2]}</strong></div></a><div style="height:48px;overflow:hidden">{$recentsearches[keyword].designs[design][8]} </div></div>
											{/section}
										</div>
									</div>
								</div>
								{/section}
								<div style="float:right">{$paginator}</div><div class="clearall"></div>
								<div class="clearall"></div>
							</div>
							<div class="links"></div>
							<div class="foot" ></div>
						</div>
				 	</div>
				</div>
				<div class="clearall"></div>
			</div>
			{include file="footer.tpl" }
		</div>		
	</div>
</body>
</html>
