<!-- Size Chart -->
{if $sizeChartInfo}
     <div class="size-chart-content-block" style="width:340px;display:none;">
        {section name=num loop=$sizeChartInfo}
            {if $smarty.section.num.index eq 0}
            <ul id="tab{$smarty.section.num.index}" class="size-chart-content" >
        {else}
            <ul id="tab{$smarty.section.num.index}" class="size-chart-content" style="display:none">
        {/if}
            <li class="size-chart-title"><b>Size : {$sizeChartInfo[num].2} ({$sizeChartInfo[num].9}")</b></li>
            {if $sizeChartInfo[num].4} <li><div align="center">Chest(W)<br><b>{$sizeChartInfo[num].4}"</b></div></li> {/if}
            {if $sizeChartInfo[num].5} <li><div align="center">Full Length<br><b>{$sizeChartInfo[num].5}"</b></div></li> {/if}
            {if $sizeChartInfo[num].6} <li><div align="center">Sleeve(L)<br><b>{$sizeChartInfo[num].6}"</b></div></li> {/if}
            {if $sizeChartInfo[num].7} <li><div align="center">Sleeve(W)<br><b>{$sizeChartInfo[num].7}"</b></div></li> {/if}
            {if $sizeChartInfo[num].8} <li><div align="center">Shoulder(W)<br><b>{$sizeChartInfo[num].8}"</b></div></li> {/if}
            <li class="tooltip-arrow"><img src="./skin1/images/tooltip-arrow.gif" alt="arrow"></li>
        </ul>
        {/section}
    </div>
    {/if}
<!-- Size Chart -->