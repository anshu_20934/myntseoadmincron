{* $Id: product_magnifier_modify.tpl,v 1.3.2.3 2006/07/11 08:39:31 svowl Exp $ *}
{if $active_modules.Magnifier ne ""}

{include file="main/include_js.tpl" src="modules/Magnifier/popup.js"}

{$lng.txt_zoom_images_top_text}

<br /><br />

{capture name=dialog}
{if $config.General.display_all_products_on_1_page eq 'Y'}<div align="right"><a href="#main">{$lng.lbl_top}</a></div>{/if}

<form action="product_modify.php" method="post" name="uploadform">

<input type="hidden" name="section" value="{$section}" />
<input type="hidden" name="mode" value="product_zoomer" />
<input type="hidden" name="productid" value="{$product.productid}" />
<input type="hidden" name="geid" value="{$geid}" />

<table cellspacing="0" cellpadding="3" width="100%">
{if $geid ne ''}
<tr>
    <td width="15" class="TableSubHead"><img src="{$cdn_base}/skin1/images/spacer.gif" width="15" height="1" alt="" /></td>
    <td class="TableSubHead" colspan="6"><b>* {$lng.lbl_note}:</b> {$lng.txt_edit_product_group}</td>
</tr>
{/if}

<tr class="TableHead">
	{if $geid ne ''}<td width="15" class="TableSubHead">&nbsp;</td>{/if}
	<td width="15" class="DataTable">&nbsp;</td>
	<td width="65" class="DataTable">{$lng.lbl_image}</td>
	<td width="5%" class="DataTable">{$lng.lbl_pos}</td>
	<td width="25%" class="DataTable">{$lng.lbl_availability}</td>
	<td width="50%" nowrap="nowrap">{$lng.lbl_image_size}</td> 
</tr>

{foreach from=$zoomer_images item=image}

<tr{cycle values=", class='TableSubHead'"}>
{if $geid ne ''}<td width="15" class="TableSubHead">&nbsp;</td>{/if}
	<td width="15" class="DataTable"><input type="checkbox" value="Y" name="iids[{$image.imageid}]" /></td>
	<td align="center" class="DataTable">
<a href="javascript: void(0);" onclick="javascript: popup_magnifier('{$productid}', '{#Magnifier_x#}', '{#Magnifier_y#}', '{$image.imageid}');"><img src="{$xcart_web_dir}/images/Z/{$productid}/{$image.imageid}/level_0.jpg" alt="" width="80" /></a>
	</td>
	<td class="DataTable"><input type="text" size="5" maxlength="5" name="zoomer_image[{$image.imageid}][orderby]" value="{$image.orderby}" style="width: 100%;" /></td>
	<td class="DataTable">
<select name="zoomer_image[{$image.imageid}][avail]" style="width: 100%;">
	<option value="Y"{if $image.avail eq "Y"} selected="selected"{/if}>{$lng.lbl_enabled}</option>
	<option value="N"{if $image.avail ne "Y"} selected="selected"{/if}>{$lng.lbl_disabled}</option>
</select>
	</td>
	<td align="center">{$image.image_x}x{$image.image_y}</td>
</tr>
{foreachelse}

<tr>
	{if $geid ne ''}<td width="15" class="TableSubHead">&nbsp;</td>{/if}
	<td colspan="6" align="center">{$lng.txt_no_images}</td>
</tr>

{/foreach}

{if $zoomer_images}

<tr>
	{if $geid ne ''}<td width="15" class="TableSubHead">&nbsp;</td>{/if}
	<td colspan="6">
<input type="button" value="{$lng.lbl_update|strip_tags:false|escape}" onclick="javascript: submitForm(this, 'zoomer_update_availability');" />&nbsp;&nbsp;&nbsp;
<input type="button" value="{$lng.lbl_delete_selected|strip_tags:false|escape}" onclick="javascript: submitForm(this, 'product_zoomer_delete');" />
	</td>
</tr>

{/if}
<tr>
	{if $geid ne ''}<td width="15" class="TableSubHead">&nbsp;</td>{/if}
	<td colspan="6">
<br /><br />

{if !$gd_not_loaded && $gd_config.correct_version}

{include file="main/subheader.tpl" title=$lng.lbl_add_new_image}

<table>
<tr>
	<td><b>{$lng.lbl_preview}:</b></td>
	<td>&nbsp;</td>
	<td><a href="{$xcart_web_dir}/image.php?id={$product.productid}&amp;type=Z&amp;tmp" target="_blank"><img id="edit_image_Z" src="{$xcart_web_dir}/image.php?id=0&amp;type=Z&amp;tmp" alt="" /></a></td>
</tr>
<tr>
	<td colspan="3" style="display: none;" id="edit_image_Z_text"><br /><br />{$lng.txt_save_det_image_note}</td>
</tr>
</table>

	</td>
</tr>
<tr>
	{if $geid ne ''}<td width="15" class="TableSubHead"><input type="checkbox" value="Y" name="fields[new_z_image]" /></td>{/if}
	<td colspan="6">

<table cellpadding="4" cellspacing="0">
<tr>
	<td nowrap="nowrap">{$lng.lbl_select_file}:</td>
	<td><input type="button" value="{$lng.lbl_browse_|strip_tags:false|escape}" onclick="javascript: popup_image_selection('Z', '{$product.productid}', 'edit_image_Z');" /></td>
</tr>
<tr>
	<td colspan="2"><br /><input type="submit" value="{$lng.lbl_upload|strip_tags:false|escape}" /></td>
</tr>
</table>

	</td>
</tr>
</table>

{else}

<font class="ErrorMessage">{$lng.lbl_error_gd_zoomer_upload_images}</font>

{/if}

</form>
{/capture}
{include file="dialog.tpl" title=$lng.lbl_zoom_images content=$smarty.capture.dialog extra='width="100%"'}
{/if}
