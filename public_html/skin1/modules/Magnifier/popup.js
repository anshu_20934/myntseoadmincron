/* $Id: popup.js,v 1.2 2006/03/10 13:17:51 max Exp $ */

/*
	Open popup window
*/
function popup_magnifier(id, max_x, max_y, imageid) {

    max_x = parseInt(max_x);
    max_y = parseInt(max_y);

    if (!max_x)
        max_x = 390;

    if (!max_y)
        max_y = 410;

	if (!imageid)
		imageid = '';
		
    return window.open(xcart_web_dir+'/popup_magnifier.php?productid='+id+'&imageid='+imageid, 'Magnifier', 'width='+max_x+',height='+max_y+',toolbar=no,status=no,scrollbars=no,resizable=no,menubar=no,location=no,direction=no');
}
