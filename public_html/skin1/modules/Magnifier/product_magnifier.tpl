{* $Id: product_magnifier.tpl,v 1.1 2006/03/06 08:55:38 max Exp $ *}
{if $zoomer_images ne ""}
{capture name=dialog}
<center>
<object type="application/x-shockwave-flash" data="{$SkinDir}/modules/Magnifier/magnifier.swf" width="390" height="410" id="zoomer" align="middle">
<param name="allowScriptAccess" value="sameDomain" />
<param name="movie" value="{$SkinDir}/modules/Magnifier/magnifier.swf" />
<param name="quality" value="high" />
<param name="FlashVars" value="xmlImages={$xcart_web_dir}/magnifier_xml.php?{if $imageid ne ""}imageid={$imageid}{else}productid={$productid}{/if}" />
<param name="swLiveConnect" value="true" />
<param name="pluginspage" value="http://www.macromedia.com/go/getflashplayer" />
</object>
</center>
{/capture}
{if $config.Magnifier.magnifier_image_popup eq 'Y' && $js_enabled eq 'Y'}
{$smarty.capture.dialog}
{else}
{include file="dialog.tpl" title=$lng.lbl_x_magnifier content=$smarty.capture.dialog extra='width="100%"'}
{/if}
{/if}
