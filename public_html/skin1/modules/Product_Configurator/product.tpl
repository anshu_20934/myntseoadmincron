{* $Id: product.tpl,v 1.8 2005/11/28 14:19:32 max Exp $ *}
{capture name=dialog}
<table width="100%">
<form>
<tr>
<td valign="top" align="left" width="30%">
{include file="product_thumbnail.tpl" productid=$product.productid image_x=$product.image_x image_y=$product.image_y product=$product.product}
<p />
{if $smarty.get.mode ne "printable"}
<a href="product.php?productid={$product.productid}&mode=printable" target=_blank><img src="{$cdn_base}/skin1/images/print.gif" width="23" height="22" border="0" name="print" alt="imprimer" /></a>
{/if}
</td>
<td valign="top">
<table width="100%" cellpadding="0" cellspacing="0" border="0">
<tr><td>{$lng.lbl_sku}</td><td>{$product.productcode}</td></tr>
<tr><td>{$lng.lbl_category}</td><td>{$product.category_text}</td></tr>
{if $usertype eq "A"}<tr><td>{$lng.lbl_vendor}</td><td>{$product.provider}</td></tr>{/if}
<tr><td>{$lng.lbl_availability}</td><td>{if $product.forsale eq "Y"}{$lng.lbl_avail_for_sale}{elseif $product.forsale eq "B"}{$lng.lbl_pconf_avail_for_sale_bundled}{else}{$lng.lbl_disabled}{/if}</td></tr>
<tr><td colspan="2">
<br />
<br />
<span class="Text">
{$product.descr}
</span>
<br />
<br />
</td></tr>
<tr><td colspan="2"><b><font class="ProductDetailsTitle">{$lng.lbl_product_info}</font></b></td></tr>
<tr><td class="Line" height="1" colspan="2"><img src="{$cdn_base}/skin1/images/spacer.gif" width="1" height="1" border="0" /></td></tr>
<tr><td colspan="2">&nbsp;</td></tr>
<tr><td width="50%">{$lng.lbl_pconf_wiz_steps_defined}:</td><td nowrap="nowrap">{$wizards_count} {$lng.lbl_pconf_steps}</td></tr>
</tr>
</table>
<br />
<br />
{include file="buttons/modify.tpl" href="product_modify.php?productid=`$product.productid`"}
&nbsp;&nbsp;
{include file="buttons/clone.tpl" href="process_product.php?mode=clone&productid=`$product.productid`"}
&nbsp;&nbsp;
{include file="buttons/delete.tpl" href="process_product.php?mode=delete&productid=`$product.productid`"}
</td>
</tr>
<input type="hidden" name="mode" value="add" />
<input type="hidden" name="productid" value="{$product.productid}" />
</form>
</table>
{/capture}
{if $smarty.get.mode eq "printable"}
{include file="dialog.tpl" title=$product.producttitle content=$smarty.capture.dialog extra="width=440"}
{else}
{include file="dialog.tpl" title=$product.producttitle content=$smarty.capture.dialog extra='width="100%"'}
{/if}
