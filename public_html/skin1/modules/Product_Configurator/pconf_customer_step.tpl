{* $Id: pconf_customer_step.tpl,v 1.19 2005/12/07 14:07:28 max Exp $ *}
{if $smarty.get.slot eq ""}
<p />
{capture name=dialog}
<table cellpadding="1" cellspacing="1" width="100%">
<tr>
	<td valign="top" align="left" rowspan="2" width="100">
{include file="product_thumbnail.tpl" productid=$product.productid image_x=$product.image_x image_y=$product.image_y product=$product.product tmbn_url=$product.tmbn_url}&nbsp;
	</td>
	<td valign="top">
<font class="ProductTitle">{$lng.lbl_step} {$step}: {$wizard_data.step_name}</font>
<br /><br />
{$wizard_data.step_descr}
{if $wizard_data.slots}
<p />
<table cellpadding="0" cellspacing="1" width="100%">
<tr>
	<td class="TableHead">
<table cellpadding="3" cellspacing="1" width="100%">
{section name=sl loop=$wizard_data.slots}

<tr>

	<td class="SlotCell">
<font class="ProductTitle">{$wizard_data.slots[sl].slot_name}</font>
<br />
{$wizard_data.slots[sl].slot_descr}
<br /><br />
{if $wizard_data.slots[sl].status eq "M"}
<font class="Message">{$lng.lbl_required}</font>
{/if}
	</td>

	<td class="SlotProductCell">
{if $wizard_data.slots[sl].have_rules}

{if $wizard_data.slots[sl].product}
<font class="ProductTitle">{$wizard_data.slots[sl].product.product}</font>
<br /><br />
{include file="product_thumbnail.tpl" productid=$wizard_data.slots[sl].product.productid image_x=$config.Appearance.thumbnail_width product="#`$wizard_data.slots[sl].product.productid`. `$wizard_data.slots[sl].product.product`" tmbn_url=$wizard_data.slots[sl].product.tmbn_url}

{if $wizard_data.slots[sl].product.product_options ne ""}
<br />
<b>{$lng.lbl_selected_options}:</b>
{include file="modules/Product_Options/display_options.tpl" options=$wizard_data.slots[sl].product.product_options}
{/if}

<br /><br />
<b>{$lng.lbl_price}:</b> <font class="ProductPriceSmall">{include file=currency.tpl value=$wizard_data.slots[sl].product.taxed_price}</font> {include file="customer/main/alter_currency_value.tpl" alter_currency_value=$wizard_data.slots[sl].product.taxed_price}

{if $wizard_data.slots[sl].product.taxes ne ''}
<br />
{include file="customer/main/taxed_price.tpl" taxes=$wizard_data.slots[sl].product.taxes}
{/if}

{if $wizard_data.slots[sl].price_modifier}
<br />
<center>
{if $wizard_data.slots[sl].price_modifier.markup gt 0}
{$lng.lbl_pconf_markup_applied}: 
{else}{$lng.lbl_pconf_discount_applied}: 
{/if}
<font class="Star">
{if $wizard_data.slots[sl].price_modifier.markup_type eq "$"}
{include file="currency.tpl" value=$wizard_data.slots[sl].price_modifier.markup}{else}
{$wizard_data.slots[sl].price_modifier.markup|abs_value}%
{/if}
</font>
</center>
{/if}

<br /><br /><br />

<table cellspacing="1" cellpadding="2">
<tr>
	<td class="ButtonsRow">{include file="buttons/button.tpl" button_title=$lng.lbl_change href="pconf.php?productid=`$product.productid`&slot=`$wizard_data.slots[sl].slotid`"}</td>
	<td class="ButtonsRow">{include file="buttons/delete.tpl" href="pconf.php?productid=`$product.productid`&slot=`$wizard_data.slots[sl].slotid`&mode=delete"}</td>
</tr>
</table>

{else}

<center>
{include file="buttons/button.tpl" button_title=$lng.lbl_select href="pconf.php?productid=`$product.productid`&slot=`$wizard_data.slots[sl].slotid`"}
</center>

{/if}

{else}
&lt;{$lng.lbl_pconf_no_rules_defined}&gt;
{/if}
	</td>

</tr>

{/section}
</table>
	</td>
</tr>
</table>
{/if}
	</td>
</tr>

{if $previous_step || $continue_button}
<tr>
	<td align="right"><br /><br />
<table cellspacing="1" cellpadding="1">
<tr>
{if $previous_step}
	<td>{include file="buttons/button.tpl" button_title=$lng.lbl_go_back style="button" href="pconf.php?productid=`$product.productid`&mode=back"}</td>
	<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
{/if}
{if $continue_button}
	<td>{include file="buttons/button.tpl" button_title=$lng.lbl_continue style="button" href="pconf.php?productid=`$product.productid`&mode=continue"}</td>
{/if}
</tr>
</table>
	</td>
</tr>
{/if}

</table>
{/capture}
{include file="dialog.tpl" title="`$product.producttitle`: `$lng.lbl_step` `$step`" content=$smarty.capture.dialog extra='width="100%"'}

{else}

<p />
{capture name=dialog}
<table cellpadding="2" cellspacing="1">
<tr>
	<td valign="top"><font class="NumberOfArticles">{$lng.lbl_pconf_displaying_products}:</font></td>
	<td valign="top">{section name=or loop=$rules_by_or}
{section name=and loop=$rules_by_or[or].rules_by_and}
{$rules_by_or[or].rules_by_and[and].ptype_name}
{if not %and.last%}
<b>&lt;{$lng.lbl_pconf_and}&gt;</b>
{/if}
{/section}
{if not %or.last%}
<br /><b>&lt;{$lng.lbl_pconf_or}&gt;</b><br />
{/if}
{/section}
{if $filled_slots}
{$lng.lbl_pconf_compatible_with}:<br />
{section name=fs loop=$filled_slots}
{$filled_slots[fs].slot_name}: {$filled_slots[fs].product.product}<br />
{/section}
{/if}
	</td>
</tr>
</table>
<p />
{if $slot_products}
{include file="customer/main/navigation.tpl"}
<br /><br />
{include file="modules/Product_Configurator/products.tpl" products=$slot_products}
{include file="customer/main/navigation.tpl"}
{else}
<hr />
<div align="center"><b>{$lng.lbl_pconf_no_products_found}</b></div>
{/if}
{/capture}
{include file="dialog.tpl" title=$lng.lbl_pconf_select_slot|substitute:"slotname":$slot_data.slot_name content=$smarty.capture.dialog extra='width="100%"'}
{/if}
