{* $Id: pconf_customer_summary.tpl,v 1.23.2.2 2006/06/29 06:45:42 max Exp $ *}
{include file="form_validation_js.tpl"}

{capture name=dialog}

{if $smarty.get.error eq "amount"}
<font class="ErrorMessage">{$lng.err_pconf_avail_exceeded}</font><br /><br />
{/if}

<table cellpadding="1" cellspacing="1" width="100%">
<tr>
	<td valign="top" align="left" rowspan="2" width="100">
{include file="product_thumbnail.tpl" productid=$product.productid image_x=$product.image_x image_y=$product.image_y product=$product.product tmbn_url=$product.tmbn_url}&nbsp;
	</td>
	<td valign="top">
<font class="ProductTitle">{$lng.lbl_summary}:</font>
<br /><br />
<table cellpadding="1" cellspacing="0" width="100%">
<tr>
	<td class="TableHead">

<table cellpadding="0" cellspacing="3" class="PCSummaryTable">
{section name=wz loop=$wizards}
{if not %wz.first%}
<tr>
	<td colspan="2" class="TableHead"><img src="{$cdn_base}/skin1/images/spacer.gif" class="Spc" alt="" /></td>
</tr>
{/if}
<tr>
	<td colspan="2" height="18" class="TableSubHead"><font class="ProductTitle">&nbsp;{$wizards[wz].step_name}</font>
&nbsp;&nbsp;
[ <a href="pconf.php?productid={$product.productid}&amp;step={$wizards[wz].step_counter}&amp;mode=update"><font class="FormButton">{$lng.lbl_modify}</font></a> ]
	</td>
</tr>
<tr>
	<td colspan="2" class="TableHead"><img src="{$cdn_base}/skin1/images/spacer.gif" class="Spc" alt="" /></td>
</tr>
{section name=sl loop=$wizards[wz].slots}
<tr>
	<td width="20%">
	{include file="product_thumbnail.tpl" productid=$wizards[wz].slots[sl].product.productid image_x=$wizards[wz].slots[sl].product.image_x image_y=$wizards[wz].slots[sl].product.image_y product=$wizards[wz].slots[sl].product.product tmbn_url=$wizards[wz].slots[sl].product.tmbn_url}&nbsp;
	</td>
	<td>
	<font class="NumberOfArticles">{$wizards[wz].slots[sl].slot_name}:</font> 
{if $wizards[wz].slots[sl].product}
<br />
&nbsp;&nbsp;&nbsp;<a href="product.php?productid={$wizards[wz].slots[sl].product.productid}" target="productinfo{$wizards[wz].slots[sl].product.productid}">{$wizards[wz].slots[sl].product.product}</a>
<br /><br />
<b>{$lng.lbl_price}:</b> <font class="ProductPriceSmall">{include file=currency.tpl value=$wizards[wz].slots[sl].product.taxed_price}</font> {include file="customer/main/alter_currency_value.tpl" alter_currency_value=$wizards[wz].slots[sl].product.taxed_price}
{if $wizards[wz].slots[sl].product.taxes}
<br />

<table>
<tr>
	<td><img src="{$cdn_base}/skin1/images/spacer.gif" width="40" height="1" alt="" /></td>
	<td>{include file="customer/main/taxed_price.tpl" taxes=$wizards[wz].slots[sl].product.taxes is_subtax=true}</td>
</tr>
</table>

{/if}

{if $wizards[wz].slots[sl].price_modifier}
<br />
{if $wizards[wz].slots[sl].price_modifier.markup gt 0}{$lng.lbl_pconf_markup_applied}: {else}{$lng.lbl_pconf_discount_applied}: {/if}
<font class="Star">
{if $wizards[wz].slots[sl].price_modifier.markup_type eq "$"}
{include file="currency.tpl" value=$wizards[wz].slots[sl].price_modifier.markup}{else}
{$wizards[wz].slots[sl].price_modifier.markup|abs_value}%
{/if}
</font>
{/if}
{else}
{$lng.txt_not_available}
{/if}
	</td>
</tr>
{/section}
{/section}
</table>

	</td>
</tr>
</table>

	</td>
</tr>

<tr>
	<td>
<br /><br />
<font class="ProductPriceTitle">{$lng.lbl_pconf_total_products_cost}:</font> <font class="ProductPrice"><span id="product_price">{include file="currency.tpl" value=$taxed_total_cost}</span></font>
{if $taxes}
<br />
<table>
<tr>
	<td><img src="{$cdn_base}/skin1/images/spacer.gif" width="100" height="1" alt="" /></td>
	<td>{include file="customer/main/taxed_price.tpl" taxes=$taxes}</td>
</tr>
</table>
{/if}

<form name="orderform" method="post" action="cart.php">

<br />
<br />
<input type="hidden" name="mode" value="add" />
<input type="hidden" name="productid" value="{$smarty.get.productid}" />
<input type="hidden" name="price" value="{$total_cost}" />

<table width="100%" cellpadding="0" cellspacing="0">
<tr>
	<td colspan="2">{include file="main/subheader.tpl" title=$lng.lbl_options}</td>
</tr>
{if $active_modules.Product_Options ne "" and $product_options}
{assign_ext var="product[taxes]" value=$taxes}
{include file="modules/Product_Options/customer_options.tpl" product_options=$product_options}
{/if}

{if $max_quantity > $config.Appearance.max_select_quantity}
{math assign="mq" equation="x+1" x=$config.Appearance.max_select_quantity}
{else}
{math assign="mq" equation="x+1" x=$max_quantity}
{/if}
<tr>
	<td width="20%">{$lng.lbl_quantity}</td>
	<td width="80%">
<script type="text/javascript" language="JavaScript 1.2">
<!--
var min_avail = {if $product.min_amount le 1}1{else}{$product.min_amount|default:1}{/if};
var avail = {$mq|default:1};
-->
</script>
{if $product.min_amount le 1}
{assign var="start_quantity" value=1}
{else}
{assign var="start_quantity" value=$product.min_amount}
{/if}
<select id="product_avail" name="amount">
{section name=q loop=$mq start=$start_quantity}
	<option value='{%q.index%}'{if %q.index% eq $amount} selected="selected"{/if}>{%q.index%}</option>
{/section}
</select>
{if $smarty.get.error eq "amount"}<font class="ErrorMessage">&lt;&lt;</font>{/if}
	</td>
</tr>
</table>

<br /><br />

<div align="right">
<table cellpadding="0" cellspacing="0">
<tr>
	<td>{include file="buttons/button.tpl" button_title=$lng.lbl_go_back style="button" href="pconf.php?productid=`$product.productid`&mode=back"}</td>
	<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	<td>{include file="buttons/button.tpl" button_title=$lng.lbl_reset style="button" href="pconf.php?productid=`$product.productid`&mode=reset"}</td>
{if $update}
	<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	<td>{include file="buttons/update.tpl" style="button" href="javascript:if (FormValidation()) `$ldelim`document.orderform.action='pconf.php';document.orderform.mode.value='pconf_update';document.orderform.submit()`$rdelim`"}</td>
{/if}
{if $update eq "" or $update eq "wishlist"}
	<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	<td>{include file="buttons/add_to_cart.tpl" style="button" href="javascript:if (FormValidation()) document.orderform.submit()"}</td>
{/if}
{if $active_modules.Wishlist and $login and ($update eq "" or $update eq "cart")}
	<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	<td>{include file="customer/add2wl.tpl"}</td>
{/if}
</tr>
</table>
</div>

</form>

	</td>
</tr>

</table>


{if $active_modules.Product_Options ne '' && $product_options ne ''}
<script type="text/javascript" language="JavaScript 1.2">
<!--
check_options();
-->
</script>
{/if}
{/capture}
{include file="dialog.tpl" title="`$product.producttitle`: `$lng.lbl_summary`" content=$smarty.capture.dialog extra='width="100%"'}
