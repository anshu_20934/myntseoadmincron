{* $Id: pconf_types.tpl,v 1.13.2.2 2006/07/11 08:39:32 svowl Exp $ *}
<p />
{$lng.txt_pconf_types_desc}
<p />
{capture name=dialog}
<form action="pconf.php" method="post" name="prtypesform">
<input type="hidden" name="mode" value="types" />
<input type="hidden" name="page" value="{$smarty.get.page}" />
<input type="hidden" name="action" value="" />

<table cellpadding="0" cellspacing="0" width="100%">

{if $product_types}

<tr>
	<td colspan="5">
<div align="right">{$lng.lbl_pconf_items_per_page}: <input type="text" size="4" maxlength="5" name="types_per_page1" value="{$ptypes_per_page}" />
<input type="button" value="{$lng.lbl_pconf_display|strip_tags:false|escape}" onclick="document.prtypesform.action.value='update_page';document.prtypesform.submit();" /></div>
<br />
{include file="customer/main/navigation.tpl"}
	</td>
</tr>

<tr class="TableHead">
	<td width="10" height="16"><b>{$lng.lbl_pos}</b></td>
	<td width="55%" colspan="3" height="16"><b>{$lng.lbl_pconf_product_types}</b></td>
	<td width="45%" height="16"><b>{$lng.lbl_pconf_specifications}</b></td>
</tr>
{section name=pt loop=$product_types}
<tr>
<td colspan="5">&nbsp;</td>
</tr>

<tr{cycle values=', class="TableSubHead"'}>
<td valign="top"><input type="text" size="3" name="posted_types[{$product_types[pt].ptypeid}][orderby]" value="{$product_types[pt].orderby}" title="{$lng.lbl_pconf_ptype_pos_hint|escape}" /></td>
<td width="10" valign="top"><input type="checkbox" name="posted_types[{$product_types[pt].ptypeid}][delete]" title="{$lng.lbl_pconf_ptype_tick_del_hint|escape}" /></td>
<td valign="top"><input type="text" size="35" name="posted_types[{$product_types[pt].ptypeid}][ptype_name]" value="{$product_types[pt].ptype_name|escape}" title="{$lng.lbl_pconf_ptype_name_hint|escape}" /></td>
<td width="5">&nbsp;</td>
<td>

<table cellpadding="0" cellspacing="0" width="100%">
{section name=sp loop=$product_types[pt].specifications}
{assign var="spec" value=$product_types[pt].specifications[sp]}
<tr>
<td><input type="text" size="3" name="posted_types[{$product_types[pt].ptypeid}][specifications][{$spec.specid}][orderby]" value="{$spec.orderby}" title="{$lng.lbl_pconf_spec_pos_hint|escape}" /></td>
<td width="10"><input type="checkbox" name="posted_types[{$product_types[pt].ptypeid}][specifications][{$spec.specid}][delete]" title="{$lng.lbl_pconf_spec_tick_del_hint|escape}" /></td>
<td width="100%"><input type="text" size="35" name="posted_types[{$product_types[pt].ptypeid}][specifications][{$spec.specid}][spec_name]" value="{$spec.spec_name|escape}" title="{$lng.lbl_pconf_spec_name_hint|escape}" /></td>
</tr>
{/section}
<tr>
<td colspan="3"><span style="white-space: nowrap;"><b>{$lng.lbl_pconf_spec_new}:</b> <input type="text" size="30" name="posted_types[{$product_types[pt].ptypeid}][new_specification]" title="{$lng.lbl_pconf_spec_name_hint|escape}" /></span></td>
</tr>
</table>
</td>
</tr>

<tr>
	<td colspan="5">&nbsp;</td>
</tr>
{/section}
{/if}
<tr>
	<td colspan="5">&nbsp;</td>
</tr>
<tr{cycle values=" , class=TableSubHead"}>
<td colspan="5"><b>{$lng.lbl_pconf_ptype_add_new}: </b> <input type="text" size="30" name="new_type" title="{$lng.lbl_pconf_ptype_name_hint|escape}" />
</td>
</tr>
<tr>
<td colspan="5"><br /><br />
{if $product_types}
{$lng.lbl_pconf_tick_delete_note}
<br /><br />
<input type="submit" value="{$lng.lbl_add_update|strip_tags:false|escape}" />
&nbsp;&nbsp;&nbsp;&nbsp;
<input type="button" value="{$lng.lbl_delete_selected|strip_tags:false|escape}" onclick="if(confirm('{$lng.lbl_pconf_delete_selected_alert|strip_tags}')){ldelim}document.prtypesform.action.value='delete';document.prtypesform.submit();{rdelim}" />
{else}
<input type="submit" value="{$lng.lbl_pconf_ptype_add|strip_tags:false|escape}" />
{/if}
</td>
</tr>

{if $product_types}

<tr>
<td colspan="5">
<br /><br />
{include file="customer/main/navigation.tpl"}
<div align="right">{$lng.lbl_pconf_items_per_page}: <input type="text" size="4" maxlength="5" name="types_per_page2" value="{$ptypes_per_page}" />
<input type="button" value="{$lng.lbl_pconf_display|strip_tags:false|escape}" onclick="document.prtypesform.action.value='update_page';document.prtypesform.submit();" /></div>
</td>
</tr>

{/if}

</table>
</form>
{/capture}
{include file="dialog.tpl" title=$lng.lbl_pconf_product_types content=$smarty.capture.dialog extra='width="100%"'}
