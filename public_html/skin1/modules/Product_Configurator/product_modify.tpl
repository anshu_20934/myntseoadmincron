{* $Id: product_modify.tpl,v 1.17 2006/01/27 09:31:47 max Exp $ *}
{include file="main/include_js.tpl" src="main/popup_image_selection.js"}
{if $active_modules.HTML_Editor}
{include file="modules/HTML_Editor/editor.tpl"}
{/if}

{if $products && $geid}
{capture name=dialog}
{ include file="customer/main/navigation.tpl" }

<table cellpadding="3" cellspacing="1" width="100%">

<tr class="TableHead">
<td>{$lng.lbl_sku}</td>
<td>{$lng.lbl_product}</td>
</tr>

{foreach from=$products item=v}

<tr{cycle name="ge" values=', class="TableSubHead"'}>
<td>{if $productid eq $v.productid}<b>{else}<a href="product_modify.php?productid={$v.productid}{if $section ne 'main'}&section={$section}{/if}&geid={$geid}">{/if}
{$v.productcode|escape}
{if $productid eq $v.productid}</b>{else}</a>{/if}</td>
<td>{if $productid eq $v.productid}<b>{else}<a href="product_modify.php?productid={$v.productid}{if $section ne 'main'}&section={$section}{/if}&geid={$geid}">{/if}
{$v.product|escape}
{if $productid eq $v.productid}</b>{else}</a>{/if}</td>
</tr>

{/foreach}

</table>
{ include file="customer/main/navigation.tpl" }

{/capture}
{include file="dialog.tpl" content=$smarty.capture.dialog title=$lng.lbl_product_list extra="width='100%'"}

<br />
{/if}

{if $section eq "main"}
{include file="modules/Product_Configurator/product_details.tpl"}

{elseif $section eq "lng"}
{include file="main/products_lng.tpl"}

{elseif $section eq 'options'}
{if ($submode eq 'product_options_add' || $product_options eq '' || $product_option ne '') || $config.General.display_all_products_on_1_page eq 'Y'}
{include file="modules/Product_Options/add_product_options.tpl"}
{else}
{include file="modules/Product_Options/product_options.tpl"}
{/if}

{elseif $section eq "variants"}
{include file="modules/Product_Options/product_variants.tpl"}

{elseif $section eq "upselling"}
{include file="modules/Upselling_Products/product_links.tpl"}

{elseif $section eq "images"}
{include file="modules/Detailed_Product_Images/product_images_modify.tpl"}

{elseif $section eq "reviews"}
{include file="modules/Customer_Reviews/admin_reviews.tpl"}

{elseif $section eq "error"}
{capture name=dialog}
<br />
{$lng.txt_cant_create_product_warning}
<br /><br />
{include file="buttons/button.tpl" button_title=$lng.lbl_register_provider href="user_add.php?usertype=P"}
<br />
{/capture}
{include file="dialog.tpl" content=$smarty.capture.dialog title=$lng.lbl_warning extra="width='100%'"}

{/if}
