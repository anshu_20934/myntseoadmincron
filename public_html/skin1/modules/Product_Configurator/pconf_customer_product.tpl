{* $Id: pconf_customer_product.tpl,v 1.12 2006/01/10 08:29:33 max Exp $ *}
{capture name=dialog}
<form name="orderform" method="post" action="cart.php?mode=add">

<table cellpadding="1" cellspacing="1" width="100%">
<tr>
	<td valign="top" align="left" rowspan="2" width="100">
{include file="product_thumbnail.tpl" productid=$product.productid image_x=$product.image_x image_y=$product.image_y product=$product.product tmbn_url=$product.tmbn_url id="product_thumbnail" type="P}&nbsp;
</td>
<td valign="top">
{if $product.fulldescr ne ""}{$product.fulldescr}{else}{$product.descr}{/if}

{if $active_modules.Product_Options ne "" and $product_options}
<script type="text/javascript" language="JavaScript 1.2">
<!--
var min_avail = {if $product.min_amount le 1}1{else}{$product.min_amount|default:1}{/if};
var avail = 0;
var product_avail = 0;
-->
</script>

<p />
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
	<td colspan="2">{include file="main/subheader.tpl" title=$lng.lbl_options}</td>
</tr>
{ include file="modules/Product_Options/customer_options.tpl"}
</table>
{/if}

	</td>
</tr>

<tr>
	<td>{include file="buttons/button.tpl" button_title=$lng.lbl_pconf_configure href="pconf.php?productid=`$product.productid`"}</td>
</tr>

</table>

<input type="hidden" name="productid" value="{$product.productid}" />
<input type="hidden" name="cat" value="{$smarty.get.cat|escape:"html"}" />
<input type="hidden" name="page" value="{$smarty.get.page|escape:"html"}" />
</form>
{/capture}
{include file="dialog.tpl" title=$product.producttitle content=$smarty.capture.dialog extra='width="100%"'}
