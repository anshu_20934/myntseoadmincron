{* $Id: pconf_customer_cart.tpl,v 1.13.2.1 2006/06/16 10:47:47 max Exp $ *}
<b>{$lng.lbl_pconf_selected_products}:</b>
<table cellpadding="1" cellspacing="0" width="100%">
<tr>
	<td class="TableHead">
<table cellpadding="0" cellspacing="2" width="100%" class="ProductBG">
{assign var="pconf_subtotal" value=0}
{foreach from=$products item=pcitem}
{if $pcitem.hidden eq $main_product.cartid}
{math equation="x+y" x=$pconf_subtotal y=$pcitem.display_price assign="pconf_subtotal"}
<tr>
	<td valign="top"{if $pcitem.product_options eq ''} colspan="2"{/if}>
<a href="product.php?productid={$pcitem.productid}" title="{$pcitem.product|escape}{if $pcitem.product_options}:
{include file="modules/Product_Options/display_options.tpl" options=$pcitem.product_options is_plain="Y"}{/if}" target="productinfo{$pcitem.productid}">{$pcitem.product}</a>
	</td>
{if $pcitem.product_options ne ''}
	<td>
<table cellspacing="0" cellpadding="0">
<tr>
	<td colspan="2" align="left">{include file="main/visiblebox_link.tpl" mark="opt_`$main_product.productid`_`$pcitem.productid`" title=$lng.lbl_product_options}</td>
</tr>
<tr id="boxopt_{$main_product.productid}_{$pcitem.productid}" style="display: none;">
	<td width="15"><img src="{$cdn_base}/skin1/images/spacer.gif" width="15" height="1" alt="" /></td>
	<td>{include file="modules/Product_Options/display_options.tpl" options=$pcitem.product_options}</td>
</tr>
</table>
	</td>
{/if}
{if $pcitem.price_modifier}
	<td valign="top" align="right"><font class="Star">{if $pcitem.price_modifier gt 0}+{else}-{/if} {include file="currency.tpl" value=$pcitem.price_modifier}</font></td>
{else}
	<td>&nbsp;</td>
{/if}
	<td valign="top" align="right">{include file="currency.tpl" value=$pcitem.display_price}</td>
</tr>
<tr>
	<td colspan="4" class="TableHead"><img src="{$cdn_base}/skin1/images/spacer.gif" class="Spc" alt="" /></td>
</tr>
{/if}
{/foreach}
{if $active_modules.Product_Options ne "" and $main_product.product_options}
{math equation="x+y" x=$pconf_subtotal y=$main_product.options_surcharge assign="pconf_subtotal"}
<tr>
	<td><b>{$lng.lbl_pconf_options_surcharge}</b></td>
	<td colspan="2">&nbsp;</td>
	<td align="right">{include file="currency.tpl" value=$main_product.options_surcharge}</td>
</tr>
<tr>
	<td colspan="3" class="TableHead"><img src="{$cdn_base}/skin1/images/spacer.gif" class="Spc" alt="" /></td>
</tr>
{/if}
<tr>
	<td><b>{$lng.lbl_subtotal}</b></td>
	<td class="SectionBox" colspan="2">&nbsp;</td>
	<td align="right">{include file="currency.tpl" value=$pconf_subtotal}</td>
</tr>
</table>
	</td>
</tr>
</table>

<br />

<div align="right">
{if $main eq "wishlist" or $main eq "giftreg"}
{include file="buttons/button.tpl" button_title=$lng.lbl_pconf_reconfigure href="pconf.php?productid=`$main_product.productid`&amp;mode=reconfigure&amp;wlitem=`$main_product.wishlistid`"}
{else}
{include file="buttons/button.tpl" button_title=$lng.lbl_pconf_reconfigure href="pconf.php?productid=`$main_product.productid`&amp;mode=reconfigure&amp;itemid=`$main_product.cartid`"}
{/if}
</div>
