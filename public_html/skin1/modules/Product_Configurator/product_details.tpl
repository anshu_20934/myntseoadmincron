{* $Id: product_details.tpl,v 1.13 2006/01/27 09:31:47 max Exp $ *}

<script type="text/javascript" language="JavaScript 1.2">
<!--
window.name="prodmodwin";

-->
</script>

{capture name=dialog}

{if $product.productid}
<div align="right">{include file="buttons/button.tpl" button_title=$lng.lbl_pconf_manage_confwiz href="product_modify.php?productid=`$product.productid`&mode=pconf&edit=wizard"}</div>

<br />
{/if}

<form action="product_modify.php" method="post" name="modifyform">
<input type="hidden" name="mode" value="pconf" />
<input type="hidden" name="productid" value="{$product.productid}" />
<input type="hidden" name="section" value="main" />
<input type="hidden" name="geid" value="{$geid}" />

<table cellpadding="4" cellspacing="0" width="100%">
{if $geid ne ''}
<tr>
	<td width="15" class="TableSubHead">&nbsp;</td>
	<td class="TableSubHead" colspan="2"><b>* {$lng.lbl_note}:</b> {$lng.txt_edit_product_group}</td>
</tr>
{/if}

{if $config.Appearance.show_thumbnails eq "Y"}

<tr>
	{if $geid ne ''}<td width="15" class="TableSubHead">&nbsp;</td>{/if}
	<td colspan="2">{include file="main/subheader.tpl" title=$lng.lbl_product_thumbnail}</td>
</tr>

<tr> 
	{if $geid ne ''}<td width="15" class="TableSubHead"><input type="checkbox" value="Y" name="fields[thumbnail]" /></td>{/if}
	<td class="ProductDetails" valign="top"><font class="FormButton">{$lng.lbl_thumbnail}</font><br />{$lng.lbl_thumbnail_msg}</td>
	<td class="ProductDetails">{include file="main/edit_image.tpl" type="T" id=$product.productid image_x=$product.image_x image_y=$product.image_y delete_url="product_modify.php?mode=delete_thumbnail&productid=`$product.productid`" button_name=$lng.lbl_save}</td>
</tr>

<tr> 
	{if $geid ne ''}<td width="15" class="TableSubHead"><input type="checkbox" value="Y" name="fields[product_image]" /></td>{/if}
	<td class="ProductDetails" valign="top"><font class="FormButton">{$lng.lbl_product_image}</font></td>
	<td class="ProductDetails">{include file="main/edit_image.tpl" type="P" id=$product.productid image_x=$product.image_x image_y=$product.image_y delete_url="product_modify.php?mode=delete_product_image&productid=`$product.productid`" button_name=$lng.lbl_save idtag="edit_product_image"}</td>
</tr>

{/if}

<tr>
	{if $geid ne ''}<td width="15" class="TableSubHead">&nbsp;</td>{/if}
	<td colspan="2"><br />{include file="main/subheader.tpl" title=$lng.lbl_product_owner}</td>
</tr>

<tr> 
	{if $geid ne ''}<td width="15" class="TableSubHead">&nbsp;</td>{/if}
	<td class="FormButton" width="20%" nowrap="nowrap">{$lng.lbl_provider}:</td>
	<td class="ProductDetails" width="80%">
{if $usertype eq "A" and $new_product eq 1}
	<select name="provider" class="InputWidth">
{section name=prov loop=$providers}
		<option value="{$providers[prov].login}">{$providers[prov].login} ({$providers[prov].title} {$providers[prov].lastname} {$providers[prov].firstname})</option>
{/section}
	</select>
{else}
{$provider_info.title} {$provider_info.lastname} {$provider_info.firstname} ({$provider_info.login})
{/if}
	</td>
</tr>

<tr> 
	{if $geid ne ''}<td width="15" class="TableSubHead">&nbsp;</td>{/if}
	<td colspan="2"><br />{include file="main/subheader.tpl" title=$lng.lbl_classification}</td>
</tr>

<tr> 
	{if $geid ne ''}<td width="15" class="TableSubHead"><input type="checkbox" value="Y" name="fields[categoryid]" /></td>{/if}
	<td class="FormButton" nowrap="nowrap">{$lng.lbl_main_category}:</td>
	<td class="ProductDetails">{include file="main/category_selector.tpl" field="categoryid" extra=' class="InputWidth"' categoryid=$product.categoryid|default:$default_categoryid}
	{if $top_message.fillerror ne "" and $product.categoryid eq ""}<font class="Star">&lt;&lt;</font>{/if}
	</td>
</tr>

<tr>
{if $geid ne ''}<td width="15" class="TableSubHead"><input type="checkbox" value="Y" name="fields[categoryids]" /></td>{/if}
	<td class="FormButton" nowrap="nowrap">{$lng.lbl_additional_categories}:</td>
	<td class="ProductDetails">
	<select name="categoryids[]" class="InputWidth" multiple="multiple" size="8">
{foreach from=$allcategories item=c key=catid}
		<option value="{$catid}"{if $c.productid eq $product.productid and $product.productid ne ""} selected="selected"{/if}>{$c.category_path|escape}</option>
{/foreach}
	</select>
	</td>
</tr>

{if $active_modules.Manufacturers ne ""}
<tr>
	{if $geid ne ''}<td width="15" class="TableSubHead"><input type="checkbox" value="Y" name="fields[manufacturer]" /></td>{/if}
	<td class="FormButton" nowrap="nowrap">{$lng.lbl_manufacturer}</td>
	<td class="ProductDetails"><select name="manufacturerid">
		<option value=''{if $product.manufacturerid eq ''} selected="selected"{/if}>{$lng.lbl_no_manufacturer}</option>
	{foreach from=$manufacturers item=v}
		<option value='{$v.manufacturerid}'{if $v.manufacturerid eq $product.manufacturerid} selected="selected"{/if}>{$v.manufacturer}</option>
	{/foreach}
	</select></td>
</tr>
{/if}

<tr> 
	{if $geid ne ''}<td width="15" class="TableSubHead"><input type="checkbox" value="Y" name="fields[forsale]" /></td>{/if}
	<td class="FormButton" nowrap="nowrap">{$lng.lbl_availability}:</td>
	<td class="ProductDetails">
	<select name="forsale">
		<option value="Y"{if $product.forsale eq "Y"} selected="selected"{/if}>{$lng.lbl_avail_for_sale}</option>
		<option value="H"{if $product.forsale eq "H"} selected="selected"{/if}>{$lng.lbl_hidden}</option>
		<option value="N"{if $product.forsale eq "N"} selected="selected"{/if}>{$lng.lbl_disabled}</option>
	</select>
	</td>
</tr>

<tr> 
	{if $geid ne ''}<td width="15" class="TableSubHead">&nbsp;</td>{/if}
	<td colspan="2"><br />{include file="main/subheader.tpl" title=$lng.lbl_details}</td>
</tr>

<tr> 
	{if $geid ne ''}<td width="15" class="TableSubHead">&nbsp;</td>{/if}
	<td class="FormButton" nowrap="nowrap">{$lng.lbl_sku}:</td>
	<td class="ProductDetails"><input type="text" name="productcode" size="20" value="{$product.productcode}" class="InputWidth" /></td>
</tr>

<tr> 
	{if $geid ne ''}<td width="15" class="TableSubHead"><input type="checkbox" value="Y" name="fields[product]" /></td>{/if}
	<td class="FormButton" nowrap="nowrap">{$lng.lbl_product_name}:</td>
	<td class="ProductDetails"> 
    <input type="text" name="product" size="45" class="InputWidth" value="{$product.product|escape}" />
{if $top_message.fillerror ne "" and $product.product eq ""}<font class="Star">&lt;&lt;</font>{/if}
	</td>
</tr>

<tr> 
	{if $geid ne ''}<td width="15" class="TableSubHead"><input type="checkbox" value="Y" name="fields[descr]" /></td>{/if}
	<td class="FormButton" nowrap="nowrap">{$lng.lbl_short_description}* :</td>
	<td class="ProductDetails"> 
{include file="main/textarea.tpl" name="descr" cols=45 rows=8 class="InputWidth" data=$product.descr width="80%" btn_rows=4}
{if $top_message.fillerror ne "" and $product.descr eq ""}<font class="Star">&lt;&lt;</font>{/if}
	</td>
</tr>

<tr> 
	{if $geid ne ''}<td width="15" class="TableSubHead"><input type="checkbox" value="Y" name="fields[fulldescr]" /></td>{/if}
	<td class="FormButton" nowrap="nowrap">{$lng.lbl_det_description}* :</td>
	<td class="ProductDetails">
{include file="main/textarea.tpl" name="fulldescr" cols=45 rows=12 class="InputWidth" data=$product.fulldescr width="80%" btn_rows=4}
	</td>
</tr>

<tr>
	{if $geid ne ''}<td width="15" class="TableSubHead">&nbsp;</td>{/if}
	<td colspan="2">{$lng.txt_html_tags_in_description}</td>
</tr>

<tr>
	{if $geid ne ''}<td width="15" class="TableSubHead"><input type="checkbox" value="Y" name="fields[min_amount]" /></td>{/if}
	<td class="FormButton" nowrap="nowrap">{$lng.lbl_min_order_amount}</td>
	<td class="ProductDetails"><input type="text" name="min_amount" size="18" value="{if $product.productid eq ""}1{else}{$product.min_amount}{/if}" /></td>
</tr>

<tr> 
	{if $geid ne ''}<td width="15" class="TableSubHead"><input type="checkbox" value="Y" name="fields[membershipids]" /></td>{/if}
	<td class="FormButton" nowrap="nowrap">{$lng.lbl_membership}</td>
	<td class="ProductDetails">{include file="main/membership_selector.tpl" data=$product}</td>
</tr>

{if $active_modules.Extra_Fields ne ""}
{include file="modules/Extra_Fields/product_modify.tpl"}
{/if}

<tr>
	<td colspan="3" align="center"><br /><input type="submit" value=" {$lng.lbl_save} " /></td>
</tr>

</table>
</form>

{/capture}
{include file="dialog.tpl" title=$lng.lbl_product_details content=$smarty.capture.dialog extra='width="100%"'}
