{* $Id: products.tpl,v 1.11.2.1 2006/08/02 07:38:45 max Exp $ *}
{if $config.Appearance.products_per_row ne "" and $config.Appearance.products_per_row gt 0 and $config.Appearance.products_per_row lt 4 and $config.Appearance.featured_only_multicolumn eq "N"}
{include file="modules/Product_Configurator/products_t.tpl" products=$products}
{else}
{if $products}
{section name=product loop=$products}
<table width="100%">
<tr><td width="90" align="center" valign="top">
<a href="product.php?productid={$products[product].productid}&pconf={$product.productid}&slot={$slot}">{include file="product_thumbnail.tpl" productid=$products[product].productid image_x=$config.Appearance.thumbnail_width product=$products[product].product tmbn_url=$products[product].tmbn_url}<br />{$lng.lbl_see_details}</a>
</td>
<td valign="top">
<a href="product.php?productid={$products[product].productid}&pconf={$product.productid}&slot={$slot}"><font class="ProductTitle">{$products[product].product}</font></a>
{if $config.Appearance.display_productcode_in_list eq "Y" and $products[product].productcode ne ""}
<br />
{$lng.lbl_sku}: {$products[product].productcode}
{/if}
<font size="1">
<br />
<br />
{$products[product].descr|truncate:300:"...":true}
<br />
</font>
<hr size="1" noshade="noshade" width="230" align="left" />
{if $products[product].product_type eq "C"}
{include file="buttons/details.tpl" href="product.php?productid=`$products[product].productid`&pconf=`$product.productid`&slot=`$slot`"}
{else}
{if $active_modules.Subscriptions ne "" and $products[product].catalogprice}
{include file="modules/Subscriptions/subscription_info_inlist.tpl"}
{else}
{if $products[product].taxed_price ne 0}
{if $products[product].list_price gt 0 and $products[product].taxed_price lt $products[product].list_price}
{math equation="100-(price/lprice)*100" price=$products[product].taxed_price lprice=$products[product].list_price format="%d" assign=discount}
<font class="MarketPrice">{$lng.lbl_market_price}: <s>
{include file="currency.tpl" value=$products[product].list_price}
</s></font><br />
{/if}
<font class="ProductPrice">{$lng.lbl_our_price}: {include file="currency.tpl" value=$products[product].taxed_price}</font><font class="MarketPrice">{include file="customer/main/alter_currency_value.tpl" alter_currency_value=$products[product].taxed_price}</font>{if $discount gt 0}, {$lng.lbl_save_price} {$discount}%{/if}
{if $products[product].taxes}<br />{include file="customer/main/taxed_price.tpl" taxes=$products[product].taxes}{/if}
{else}
<font class="ProductPrice">{$lng.lbl_enter_your_price}</font>
{/if}
{/if}
<br /><br />
{if ($active_modules.Product_Options and $products[product].product_options_counter eq 0) || $active_modules.Product_Options eq ''}
<form action="pconf.php" method="post" name="form{$products[product].productid}">
<input type="hidden" name="mode" value="add" />
<input type="hidden" name="slot" value="{$slot}" />
<input type="hidden" name="productid" value="{$product.productid}" />
<input type="hidden" name="addproductid" value="{$products[product].productid}" />

{include file="buttons/button.tpl" button_title=$lng.lbl_pconf_add_to_configuration href="javascript:document.form`$products[product].productid`.submit();" js_to_href="Y"}
</form>
{else}
{include file="buttons/details.tpl" href="product.php?productid=`$products[product].productid`&pconf=`$product.productid`&slot=`$slot`"}
{/if}
{/if}
</td></tr>
</table>
<br />
<br />
<br />
{/section}
{else}
{$lng.lbl_pconf_no_products_found}
{/if}
{/if}
