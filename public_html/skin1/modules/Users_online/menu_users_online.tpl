{* $Id: menu_users_online.tpl,v 1.6 2006/02/10 14:27:32 svowl Exp $ *}
{if $users_online}
{if $usertype eq "C"}
{capture name=menu}
{else}
<table cellpadding="0" cellspacing="0" width="100%">
<tr>
	<td class="DialogBorder"><img src="{$cdn_base}/skin1/images/spacer.gif" width="1" height="1" alt="" /></td>
</tr>
<tr>
	<td class="BottomDialogBox">
<b>{$lng.lbl_users_online}:</b>&nbsp;
{/if}
{foreach from=$users_online item=v}
<font class="VertMenuItems">{$v.count}
{strip}
{if $v.usertype eq 'A' || ($v.usertype eq 'P' && $active_modules.Simple_Mode)}
{$lng.lbl_admin_s}
{elseif $v.usertype eq 'P'}
{$lng.lbl_provider_s} 
{elseif $v.usertype eq 'B'}
{$lng.lbl_partner_s} 
{elseif $v.usertype eq 'C' && $v.is_registered eq 'Y'}
{$lng.lbl_registered_customer_s} 
{elseif $v.usertype eq 'C' && $v.is_registered eq 'A'}
{$lng.lbl_anonymous_customer_s}
{elseif $v.usertype eq 'C' && $v.is_registered eq ''}
{$lng.lbl_unregistered_customer_s} 
{/if}
{/strip}
{if $usertype eq "C"}<br />{/if}
{/foreach}
{if $usertype eq "C"}
{/capture}
{ include file="menu.tpl" dingbats="dingbats_authentification.gif" menu_title=$lng.lbl_users_online menu_content=$smarty.capture.menu }
{else}
	</td>
</tr>
</table>
{/if}
{/if}
