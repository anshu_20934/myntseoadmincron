{* $Id: giftcert.tpl,v 1.52.2.2 2006/06/16 10:47:45 max Exp $ *}


{if ($config.Gift_Certificates.allow_customer_select_tpl eq "Y" and $usertype eq "C") or $usertype eq "A" or ($usertype eq "P" and $active_modules.Simple_Mode ne "")}
{assign var="allow_tpl" value='1'}
{else}
{assign var="allow_tpl" value=''}
{/if}

{include file="check_email_script.tpl"}
<script type="text/javascript" language="JavaScript 1.2">
<!--
var txt_recipient_invalid = "{$lng.txt_recipient_invalid|strip_tags|replace:"\n":" "|replace:"\r":" "}";
var txt_amount_invalid = "{$lng.txt_amount_invalid|strip_tags|replace:"\n":" "|replace:"\r":" "}";
var txt_gc_enter_mail_address = "{$lng.txt_gc_enter_mail_address|strip_tags|replace:"\n":" "|replace:"\r":" "}";

{if $usertype eq "C"}
var orig_mode = "gc2cart";
{else}
var orig_mode = "{$smarty.get.mode|escape:"javascript"}";
{/if}
var min_gc_amount = {$min_gc_amount|default:0};
var max_gc_amount = {$max_gc_amount|default:0};
var is_c_area = {if $usertype eq "C"}true{else}false{/if};
var enablePostMailGC = "{$config.Gift_Certificates.enablePostMailGC}";

{literal}
function check_gc_form() {
	if (document.gccreate.recipient.value == "") {
		document.gccreate.recipient.focus();
		alert (txt_recipient_invalid);
		return false;
	}

	var num = convert_number(document.gccreate.amount.value);
	if (!check_is_number(document.gccreate.amount.value) || (is_c_area && (num < min_gc_amount || (max_gc_amount > 0 && num > max_gc_amount)))) {
		document.gccreate.amount.focus();
	    alert (txt_amount_invalid);
		return false;
	}

	if (enablePostMailGC == 'Y') {
		if ((document.gccreate.send_via[0].checked) && (!checkEmailAddress(document.gccreate.recipient_email))) {
			document.gccreate.recipient_email.focus();
			return false;
		}
		if (document.gccreate.send_via[1].checked && (document.gccreate.recipient_firstname.value == "" || document.gccreate.recipient_lastname.value == "" || document.gccreate.recipient_address.value == "" || document.gccreate.recipient_city.value == "" || document.gccreate.recipient_zipcode.value == "")) {
			document.gccreate.recipient_firstname.focus();
			alert (txt_gc_enter_mail_address);
			return false;
		}

	} else if (!checkEmailAddress(document.gccreate.recipient_email)) {
		document.gccreate.recipient_email.focus();
		return false;
	}

	return true;
}

function formSubmit() {
	if (check_gc_form()) {
		document.gccreate.mode.value = orig_mode;
		document.gccreate.target = ''
		document.gccreate.submit();
	}
}
-->
</script>
{/literal}

{if $config.Gift_Certificates.enablePostMailGC eq "Y" and $allow_tpl}
<script type="text/javascript" language="JavaScript 1.2">
<!--
{literal}
function switchPreview() {
	if (document.gccreate.send_via[0].checked) {
		document.getElementById('preview_button').style.display='none';
		document.getElementById('preview_template').style.display='none';
	}
	if (document.gccreate.send_via[1].checked) {
		document.getElementById('preview_button').style.display='';
		document.getElementById('preview_template').style.display='';
	}
}

function formPreview() {
	if (check_gc_form()) {
		document.gccreate.mode.value='preview';
		document.gccreate.target='_blank'
		document.gccreate.submit();
	}
}
{/literal}
-->
</script>
{else}
<script type="text/javascript" language="JavaScript 1.2">
<!--
{literal}
function switchPreview() {
	return false;
}
{/literal}
-->
</script>
{/if}

{include file="check_zipcode_js.tpl"}


{if $login and $usertype eq "C"}




{if $smarty.get.gcid and $gc_array eq ""}
{$lng.err_gc_not_found}
{/if}

{*{if $gc_array}*}
<!-- <hr size="1" noshade="noshade" />
<table>
<tr>
	<td><b>{$lng.lbl_gc_id}:</b></td>
	<td>{$gc_array.gcid}</td>
</tr>
<tr>
	<td><b>{$lng.lbl_amount}:</b></td>
	<td>{include file="currency.tpl" value=$gc_array.amount}</td>
</tr>
<tr>
	<td><b>{$lng.lbl_remain}:</b></td>
	<td>{include file="currency.tpl" value=$gc_array.debit}</td>
</tr>
<tr>
	<td><b>{$lng.lbl_status}:</b></td>
	<td>
{if $gc_array.status eq "P"}{$lng.lbl_pending}
{elseif $gc_array.status eq "A"}{$lng.lbl_active}
{elseif $gc_array.status eq "B"}{$lng.lbl_blocked}
{elseif $gc_array.status eq "D"}{$lng.lbl_disabled}
{elseif $gc_array.status eq "E"}{$lng.lbl_expired}
{elseif $gc_array.status eq "U"}{$lng.lbl_used}
{/if}
	</td>
</tr>
</table> -->
{*{/if}*}

{/if}


{if $amount_error}
{$lng.txt_amount_invalid}
{/if}
{if $usertype eq "C"}

<form name="gccreate" action="giftcert.php" method="post" onsubmit="javascript: return check_gc_form()">
<input type="hidden" name="gcindex" value="{$smarty.get.gcindex|escape:"html"}" />
<input type="hidden" name="mode" value="gc2cart" />

{else}

<form name="gccreate" action="giftcerts.php" method="post" onsubmit="javascript: return check_gc_form()">
<input type="hidden" name="mode" value="{$smarty.get.mode|escape:"html"}" />
<input type="hidden" name="gcid" value="{$smarty.get.gcid|escape:"html"}" />

{/if}


<div class="form"> 
          <div class="super"> 
            <p>gift certificate information</P>

          </div>

          <div class="head"> 
            <p>who are you sending this to?</p>
          </div>
          <div class="links"> 
            <div class="legend"> 
              <p>From (Your Email)<span class="mandatory">*</span> </P>
            </div>

            <div class="field"> <label> 
	    <input type="text" name="purchaser" value="{if $usertype eq "A" && $smarty.get.mode eq 'add_gc'}{$config.Company.company_name}{else}{if $giftcert.purchaser}{$giftcert.purchaser|escape:"html"}{else}{$userinfo.firstname}{if $userinfo.firstname ne ''} {/if}{$userinfo.lastname}{/if}{/if}" />
              </label> </div>
            <div class="clearall"></div>
          </div>

          <div class="links"> 
            <div class="legend"> 
              <p>To (Recepient Name)<span class="mandatory">*</span> </P>

            </div>
            <div class="field"> <label> 
              <input type="text" name="recipient" size="30" value="{$giftcert.recipient|escape:"html"}" /> 
              </label> </div>
            <div class="clearall"></div>
          </div>

          <div class="links"> 
            <div class="legend"> 
              <p>&nbsp;</P>

            </div>
            <div class="field"> 
              <p class="mandatory">The gift certificate will include the sender's 
                name, the recipient's name and a message.</p>
            </div>
            <div class="clearall"></div>
          </div>

          <div class="foot"></div>
          <br>

	  <div class="subhead"> 
            <p>add a message</P>

          </div>

          <div class="links"> 
            <div class="legend"> 
              <p>Message</P>
            </div>
            <div class="field"> <label>
		<textarea name="message" row="4" col="30">{$giftcert.message}</textarea>
              </label>
	      </div>
            <div class="clearall"></div>
          </div>

          
<div class="links"> 
            <div class="legend"> 
              <p>&nbsp;</P>
            </div>
            <div class="field"> 
              <p class="mandatory">Enter the text that the recipient will see 
                in the e-mail</p>
            </div>
            <div class="clearall"></div>

          </div>
          <div class="foot"></div>
          <br>

	  <div class="subhead"> 
            <p>choose an amount</P>
          </div>

          <div class="links"> 
            <div class="legend"> 
              <p>Rs<span class="mandatory">*</span></P>
            </div>

            <div class="field"> 
	    <label> 
	    <input type="text" name="amount" size="10" maxlength="9" value="{$giftcert.amount|formatprice}" />
{*{if $usertype eq "C" and ($min_gc_amount gt 0 or $max_gc_amount gt 0)}{$lng.lbl_gc_amount_msg} {if $min_gc_amount gt 0}{$lng.lbl_gc_from} {$config.General.currency_symbol}{$min_gc_amount|formatprice}{/if} {if $max_gc_amount gt 0}{$lng.lbl_gc_through} {$config.General.currency_symbol}{$max_gc_amount|formatprice}{/if}{/if} *}
             
              </label> </div>
            <div class="clearall"></div>
          </div>

          <div class="links"> 
            <div class="legend"> 
              <p>&nbsp;</P>

            </div>
            <div class="field"> 
              <p class="mandatory">Allowed values from Rs 10.00 onwards.<br>Specify the amount in currency.</p>
            </div>
            <div class="clearall"></div>
          </div>

          <div class="foot"></div>
          <br>

	  <div class="subhead"> 
            <p>choose a delivery method</P>
          </div>

	  <div class="links"> 
		<div class="field"> 

		{if $config.Gift_Certificates.enablePostMailGC eq "Y"}
		<p><label><input id="gc_send_e" class="radio" type="radio" name="send_via" value="E" onclick="switchPreview();"{if $giftcert.send_via ne "P"} checked="checked"{/if} /></label>&nbsp;email</p>
		{else}
		<p><label><input type="hidden" name="send_via" value="E" /></label>&nbsp; email</p>
		{/if}
		</div>
		
	    <div class="clearall"></div>
	</div>

          <div class="links"> 
            <div class="legend"> 

              <p>Email<span class="mandatory">*</span></P>
            </div>
            <div class="field"> <label> 
              <input type="text" name="recipient_email" size="30" value="{$giftcert.recipient_email}" />

              </label> </div>
            <div class="clearall"></div>
          </div>

          <div class="links"> 
            <div class="legend"> 
              <p>&nbsp;</P>
            </div>
            <div class="field"> 
              <p class="mandatory">Email.<br>Enter the Email Address of the person you are sending the <strong>Gift Certificate</strong> to.</p>

            </div>
            <div class="clearall"></div>
          </div>

	  <div class="links"></div>

	{if $config.Gift_Certificates.enablePostMailGC eq "Y"}
        
	<div class="links"> 
		<div class="field"> 

		
		<p><label><input id="gc_send_p" type="radio" class="radio" name="send_via" value="P" onclick="switchPreview();"{if $giftcert.send_via eq "P"} checked="checked"{/if} />&nbsp;Postal Mail</p>
		
		</div>
		
	    <div class="clearall"></div>
	</div>

	<div class="links"> 
            <div class="legend"> 

              <p>First Name<span class="mandatory">*</span></P>
            </div>
            <div class="field"> <label> 
              <input type="text" name="recipient_firstname"  value="{$giftcert.recipient_firstname}" />

              </label> </div>
            <div class="clearall"></div>
          </div>

	  <div class="links"> 
            <div class="legend"> 

              <p>Last Name<span class="mandatory">*</span></P>
            </div>
            <div class="field"> <label> 
              <input type="text" name="recipient_lastname" size="30" value="{$giftcert.recipient_lastname}" />

              </label> </div>
            <div class="clearall"></div>
          </div>

	  <div class="links"> 
            <div class="legend"> 

              <p>Address<span class="mandatory">*</span></P>
            </div>
            <div class="field"> <label> 
              <input type="text" name="recipient_address" size="40" value="{$giftcert.recipient_address}" />

              </label> </div>
            <div class="clearall"></div>
          </div>

	   <div class="links"> 
            <div class="legend"> 

              <p>City<span class="mandatory">*</span></P>
            </div>
            <div class="field"> <label> 
              <input type="text" name="recipient_city" size="30" value="{$giftcert.recipient_city}" />

              </label> </div>
            <div class="clearall"></div>
          </div>

	{if $config.General.use_counties eq "Y"}

		<div class="links"> 
		<div class="legend"> 

		<p>Country<span class="mandatory">*</span></P>
		</div>
		<div class="field"> <label> 
		{include file="main/counties.tpl" counties=$counties name="recipient_county" default=$giftcert.recipient_county}
		</label> </div>
		<div class="clearall"></div>
		</div>
	{/if}

	 <div class="links"> 
            <div class="legend"> 

              <p>State<span class="mandatory">*</span></P>
            </div>
            <div class="field"> <label> 
               {include file="main/states.tpl" states=$states name="recipient_state" default=$giftcert.recipient_state default_country=$giftcert.recipient_country}

              </label> </div>
            <div class="clearall"></div>
          </div>


	  <div class="links"> 
            <div class="legend"> 

              <p>Country<span class="mandatory">*</span></P>
            </div>
            <div class="field"> <label> 
               <select id="recipient_country" name="recipient_country" size="1" onchange="javascript: check_zip_code_field(this, document.gccreate.recipient_zipcode);">
		{section name=country_idx loop=$countries}
			<option value="{$countries[country_idx].country_code}"{if $giftcert.recipient_country eq $countries[country_idx].country_code} selected="selected"{elseif $countries[country_idx].country_code eq $config.General.default_country and $giftcert.recipient_country eq ""} selected="selected"{elseif $countries[country_idx].country_code eq $userinfo.b_country && $giftcert.recipient_country eq ""} selected="selected"{/if}>{$countries[country_idx].country}</option>
		{/section}
		</select>

              </label> </div>
            <div class="clearall"></div>
          </div>

	<div class="links"> 
            <div class="legend"> 

              <p>PIN Code<span class="mandatory">*</span></P>
            </div>
            <div class="field"> <label> 
              <input type="text" name="recipient_zipcode" size="30" value="{$giftcert.recipient_zipcode}" onchange="javascript: check_zip_code_field(document.forms['gccreate'].recipient_country, document.forms['gccreate'].recipient_zipcode);" />

              </label> </div>
            <div class="clearall"></div>
          </div>

	  <div class="links"> 
            <div class="legend"> 

              <p>Phone<span class="mandatory">*</span></P>
            </div>
            <div class="field"> <label> 
              <input type="text" name="recipient_phone" size="30" value="{$giftcert.recipient_phone}" />

              </label> </div>
            <div class="clearall"></div>
          </div>
	

	<div class="links"> 
            <div class="legend"> 
              <p>&nbsp;</P>
            </div>
            <div class="field"> 
              <p class="mandatory"> Postal Mail.<br>Enter the postal address of the person you are sending the  <strong>Gift Certificate</strong> to.</p>

            </div>
            <div class="clearall"></div>
          </div>

     {/if}

  <div class="button">
  {if $allow_tpl}
  <div id="preview_button" {if $giftcert.send_via ne "P"}style="display: none;"{/if}>
	<input class="submit" type="submit" name="preview" value="Preview" border="0" onClick="javascript: void(formPreview());">
  </div>

{/if}
{if $usertype eq "C" or (($usertype eq "A" or ($usertype eq "P" and $active_modules.Simple_Mode ne "")) and $smarty.get.mode eq "modify_gc")}
{if $smarty.get.gcindex ne "" or ($usertype eq "A" and $smarty.get.mode eq "modify_gc")}

{if $active_modules.Wishlist ne "" and $action eq "wl"}
<input class="submit" type="button" name="submitButton" value="add gift certificate to cart" border="0" onClick="javascript: document.gccreate.mode.value='addgc2wl'; formSubmit();">
{else}
<input class="submit" type="button" name="submitButton" value="add gift certificate to cart" border="0" onClick="javascript: formSubmit();">
{/if}
{else}
<input class="submit" type="button" name="add2cart" value="add gift certificate to cart" border="0" onClick="javascript: formSubmit();">
{if $active_modules.Wishlist and $login ne ""}
&nbsp;&nbsp;
<!-- <input class="submit" type="button" name="add2wl" value="add gift certificate to wishlist" border="0" onClick="javascript: orig_mode = 'addgc2wl'; formSubmit();"> -->

{/if}
{/if}
{else}
<input class="submit" type="button" name="submitButton" value="Create Gift Certificate" border="0" onClick="javascript: formSubmit();">
{/if}

 
  </div>
          
	  <div class="foot"></div>
	<br>
</div>

<br/>






</form>


