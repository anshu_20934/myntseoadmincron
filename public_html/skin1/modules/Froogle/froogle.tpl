{* $Id: froogle.tpl,v 1.7.2.1 2006/06/16 10:47:45 max Exp $ *}

{include file="page_title.tpl" title=$lng.lbl_froogle_export}

{$lng.txt_froogle_note}

<br /><br />

<!-- IN THIS SECTION -->

{include file="dialog_tools.tpl"}

<!-- IN THIS SECTION -->

<br />

{capture name=dialog}
<form action="froogle.php" method="post" name="froogleform">
<input type="hidden" name="mode" value="fcreate" />
<table cellspacing="5" cellpadding="0">
<tr>
	<td>{$lng.lbl_filename}</td>
	<td><input type="text" name="froogle_file" value="{$froogle_file|default:$def_froogle_file}" size="25" /></td>
</tr>
<tr>
	<td colspan="2">
	<input type="button" value="{$lng.lbl_export|escape}" onclick="javascript: submitForm(this, 'fcreate');" />&nbsp;
	<input type="button" value="{$lng.lbl_download|escape}" onclick="javascript: submitForm(this, 'fdownload');" />&nbsp;
	{if $is_ftp_module eq 'Y'}
	<input type="button" value="{$lng.lbl_upload|escape}" onclick="javascript: submitForm(this, 'fupload');" />
	{/if}
</td>
</tr>
</table>
</form>
{/capture}
{include file="dialog.tpl" title=$lng.lbl_froogle_export content=$smarty.capture.dialog extra='width="100%"'}

