{config_load file="$skin_config"}
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
{literal}
<script type="text/javascript" language="JavaScript 1.2">

function hideSearchDiv()
{
    document.getElementById('searchpopup').style.visibility = 'hidden';
    document.getElementById('searchbox').style.visibility = 'hidden';
    document.getElementById('searchbox').style.display = 'none';
}
function changeColor(divname)
{
    document.getElementById(divname).style.color = "#000000";
    //document.getElementById(divname).style.background = "#000000";
}
function selectedText()
{
	var selectedradio = document.getElementsByName("selectedtext");
	for(var i =0;  i<selectedradio.length; i++)
	{
		if(selectedradio[i].checked == true)
		{
			var selectedstyle = selectedradio[i].value;
			var styles = selectedstyle.split("-");
			document.getElementById('selctedstyle').value = "";
			document.getElementById('selctedstyle').value = styles[0];
			document.getElementById('hidestyleid').value = styles[1];
		}
	}	
}

function selectedType()
{
	if(document.getElementById('selctedstyle').value == "")
	{
		alert("Please select the one of the listed styles");
		return false;
	}
	else
	{
		document.getElementById('searchpopup').style.visibility = 'hidden';
		document.getElementById('searchbox').style.visibility = 'hidden';
		document.getElementById("searchbox").style.display = "none";
		document.searchform.submit();
	}

}
</script>

{/literal}
<div id="searchpopup" style="visibility:hidden;display:none;">
<div id="overlay"> </div>
  <div id="selection">
   <div class="wrapper">
      <div class="head">
        <p><a href="javascript: hideSearchDiv()"><img src="./skin1/mkimages/click-to-close.gif" border="0"></a>
	search for product styles</p>

      </div>

      <div class="body" style="padding:10">
      <br>
      <p>
	<table style="width:540px;height:auto;border: 1px solid #999999;padding: 2px;">
	<tr>
		<td style="vertical-align: top;">
			<table id="searchbox" style="visibility:visible;width:350px;height:auto;font-size: 0.7em;border: 0px solid #999999;">
			<tr>
				<td>search product styles</td>
				<td style="vertical-align: top;"><input type="text" name="search" id="search" onblur="if(this.value=='') this.value=this.defaultValue;" onfocus="if(this.value==this.defaultValue) this.value='';" value="search text" style="color:#ACACAC;cursor:text;" onkeypress='changeColor("search");'></td>
				<td style="vertical-align: top;"><input type="button" class="submit" name="searchbutton" id="searchbutton" value="search" onClick="javascript:searchResult();"></td>
			</tr>
			</table>
		</td>
	</tr>
	<tr><td>&nbsp;</td></tr>
	<tr>
		<td style="vertical-align: top;">
			<div id="loadresult" style="visibility:hidden;display:none;">
				<table id="loginloader" style="width:540px;height:142px;font-size: 0.7em;border: 1px solid #999999;">
				<tr>
					<td colspan="2" style="vertical-align: middle;"><center><img src="{$cdn_base}/images/popup_login.gif" /></center></td>
				</tr>
                		</table>
			</div>
			<div id="searchresult"></div>
		</td>
	</tr>
	</table>
      </p>
     </div>

      <div class="foot"></div>

    </div>

  </div>
  </div>

