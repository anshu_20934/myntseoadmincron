
{if $login eq ""}
	{include file="main/login_form.tpl"}
{else}

<form name="searchform" action="myrequest.php" method="post">
<input type="hidden" name="mode" id="mode" value="" />
<input type="hidden" id="date_period_null" name="posted_data[date_period_user]" value=""  />	 
<div class="subhead"><p>request history</p></div> 
<div class="links">
<div class="field">
	<label>
	<p>
		<strong>select duration</strong>
		<select name="dateduration" id="dateduration" style='width:130px;' class="select" onChange="javascript:searchRequests('{$login}');">
			<option value="7">&nbsp;Last 7 days</option>
			<option value="1M" selected>&nbsp;Last 1 month</option>
			<option value="3M">&nbsp;Last 3 month</option>
			<option value="1Y">&nbsp;Last 1 year</option>
			<option value="all">&nbsp;All</option>
		</select>
	</p>
	</label>
</div>
<div class="clearall"></div>
</div>

<div id="defaultresult">
	<table cellpadding='2' class='table'>
	<tr class='font_bold'>
	<td class='align_left' style='padding-left:7px;'>request id.</td>
	<td class='align_left'>requested by</td>
	<td class='align_left'>request date</td>
	<td class='align_left'>status</td>
	</tr>

	{section name=request loop=$myrequest}
	<tr>
		<td class='align_left' style='padding-left:7px;'>
			<a href='modules/corporate/mkmanagerequest.php?request=updated&requestid={$myrequest[request].id}{if $myrequest[request].flag}&flag={$myrequest[request].flag}{/if}'><strong>#{$myrequest[request].id}</strong></a>
		</td>
		<td class='align_left' >{$myrequest[request].request_update_by}</td>
		<td class='align_left' >{$myrequest[request].request_update_date}</td>
		<td class='align_left' >
			{if $myrequest[request].request_status == 'S'}submitted{/if}
			{if $myrequest[request].request_status == 'D'}draft{/if}
			{if $myrequest[request].request_status == 'QA'}quote/negotiation{/if}
			{if $myrequest[request].request_status == 'CL'}completed{/if}
			{if $myrequest[request].request_status == 'C'}cancelled{/if}
		</td>
	</tr>
	{/section}
	</table>
</div>
{include file="main/ajax_requestslist.tpl"}
<div class="foot"></div>							
</form>
{/if}
<div class="clearall"></div>
