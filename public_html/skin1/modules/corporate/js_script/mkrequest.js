/* Define url for redirecting to server site page*/
var rand = Math.random();
var URL1= "modules/corporate/mkpopupimage.php?rand="+rand+"&param=";

/*
 * Function Name: getHTTPObject()
 * Purpose: Instantiate object of XMLHttpRequest().
 * Summary: Beauty of this function is, this function instantiate browser independent
 * object of XMLHttpRequest()
 *
*/
function getHTTPObject() {
      var xmlhttp;
      /*@cc_on
          @if (@_jscript_version >= 5)
            try {
          xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
            } catch (e) {
          try {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
          } catch (E) {
            xmlhttp = false;
          }
        }
          @else
          xmlhttp = false;
          @end @*/
          if (!xmlhttp && typeof XMLHttpRequest != 'undefined') {
            try {
          xmlhttp = new XMLHttpRequest();
    } catch (e) {
          xmlhttp = false;
    }
  }
          return xmlhttp;
}


/*
*Function to validate the valid quantity
*Parameter : quantity text box value
*/
function IsNumeric(sText){
	pattern = /^[0-9]*$/;
	if(pattern.test(sText)==false){return false;}
	else if(sText == 0){return false;}
	else{return true;}
}
/*End function IsNumeric()*/

/*
*Function to display and hide the styles of checked and unchecked product type
*/
function openStyleBox(index){
	/*Display the styles of checked product type*/
	var id = 'selectedtype_'+index;
	var styleObj = document.getElementById('style_'+index);
	
	if(document.getElementById('selectedtype_'+index).checked == true){
		styleObj.style.display = 'block';
	}
	/*Hide and uncheck the styles of unchecked product type */
	else if(document.getElementById('selectedtype_'+index).checked == false){
		var stylecount = document.getElementById('stylecount_'+index).value;
		for(j=0;j<stylecount;j++){
			//alert('styles_'+j +"==="+ document.getElementById('styles_'+j).checked);
			if(document.getElementById('styles_'+index+'_'+j).checked == true){
				document.getElementById('styles_'+index+'_'+j).checked = false;
				document.getElementById('textbox_'+index+'_'+j).style.display = 'none';
			}
		}styleObj.style.display = 'none';
	}
}
/*End function openStyleBox()*/

/*
*Function to close the div popup of types and styles
*/
function hideImageDiv(){
	/*Load the number of styles*/
	var indexcount = document.getElementById('indexcount').value;
	/*Hide the div popup*/
	document.getElementById('imagepopup').style.display = 'none';
	/*Loop to hide the open styles*/
	for(i=0;i<indexcount;i++){
		document.getElementById('style_'+i).style.display = 'none';
	}
}
/*End function hideImageDiv()*/

/*
*Function for the validation of product type and product style selected, If selected then post the form
*Parameter : Number of styles
*/
function submitStylesAndTypes(indexcount, param){
	var indexcount1 = document.getElementById('indexcount').value;
	var divcountvalue = document.getElementById('divcount').value;
	var checkcount = 0;
	/*Loop to check is product type selected or not*/
	for(i=0;i<indexcount;i++){
		if(document.getElementById('selectedtype_'+i).checked == true){
			checkcount = checkcount +1;
		}
	}
	/*If not any product type selected then alert a message*/
	if(checkcount == 0){
		alert("Please select product type");
		return false;
	}
	/*If product type selected then check product style selected or not*/
	else{
		for(i=0;i<indexcount1;i++){
			if(document.getElementById('selectedtype_'+i).checked == true){
				var stylecount = document.getElementById('stylecount_'+i).value;
				var radiocount = 0;
				for(j=0;j<stylecount;j++){
					if(document.getElementById('styles_'+i+'_'+j).checked == true){
						radiocount = radiocount +1;
					}
				}
				if(radiocount == 0){
					alert("Please select product style");
					return false;
				}
			}
		}
	}
	/*If no any product styles selected then alert a message*/
	
	/*If product styles selected then validate the quantity text box for the valid quantity*/
	if(radiocount > 0){
		for(i=0;i<indexcount1;i++){
			if(document.getElementById('selectedtype_'+i).checked == true){
				var stylecount = document.getElementById('stylecount_'+i).value;
				var qty_err = false;
				for(j=0;j<stylecount;j++){
					if(document.getElementById('styles_'+i+'_'+j).checked == true){
						var qty = document.getElementById('quantity_'+i+'_'+j);
						if (qty.value == ""){
							alert("Please enter valid quantity");
							qty.focus();
							qty_err = true;
							return false;
						}
						if (IsNumeric(qty.value) == false){
							alert("Please enter valid quantity");
							qty.focus();
							qty_err = true;
							return false;
						}
					}
				}
			}
		}
	}
	/*If quantity is valid for the selected style then post the form*/
	if(qty_err == false){
		if(param == 'mkrequest') var action = 'modules/corporate/mkrequest.php';
		else if(param == 'mkmanage') var action = 'modules/corporate/mkmanagerequest.php';
		document.searchform.action = action; 
		document.searchform.submit(); 
	}
}
/*End function submitStylesAndTypes()*/

/*
*Function to display and hide the quantity textbox on click on style radio button
*Parameter : Number of textbox
*/
function showQuantityBox(jindex, iindex){
	var indexcount1 = document.getElementById('indexcount').value;
	var stylecount = 0;
	stylecount = document.getElementById('stylecount_'+iindex).value;
	for(j=0;j<stylecount;j++){
		if(document.getElementById('styles_'+iindex+'_'+j).checked == true){
			document.getElementById('textbox_'+iindex+'_'+j).style.display = 'block';
		}else if (document.getElementById('styles_'+iindex+'_'+j).checked == false){
			document.getElementById('textbox_'+iindex+'_'+j).style.display = 'none';
		}
	}
}
/*End function showQuantityBox()*/


function corporatecontactdata(param, ajaxForm) {
    var http = new getHTTPObject();
	var error = validateUserAccounts(ajaxForm);
	if(error == false){
	    var strSubmit       = '';
	    var formElem;
	    var strLastElemName = '';
		var error = false;
		 for (i = 0; i < ajaxForm.elements.length; i++){
			formElem = ajaxForm.elements[i];
			elmname = formElem.name;
			switch (formElem.type){
			// Text, select, hidden, password, textarea elements
				case 'text':
					if(formElem.name == 'firstname')
						strSubmit += formElem.name + '=' + escape(formElem.value) + '&';
				   if(formElem.name == 'company')
						strSubmit += formElem.name + '=' + escape(formElem.value) + '&';
				   if(formElem.name == 'email')
						strSubmit += formElem.name + '=' + escape(formElem.value) + '&';
				   if(formElem.name == 'delivery-date')
						strSubmit += formElem.name + '=' + escape(formElem.value) + '&';
				   if(formElem.name == 'phoneno')
						strSubmit += formElem.name + '=' + escape(formElem.value) + '&';
				   if(formElem.name == 'mobileno')
						strSubmit += formElem.name + '=' + escape(formElem.value) + '&';
				   if(formElem.name == 'city')
						strSubmit += formElem.name + '=' + escape(formElem.value) + '&';
				case 'hidden':
					if(formElem.name == 'corporate_id')
						strSubmit += formElem.name + '=' + escape(formElem.value) + '&';
				case 'textarea':
					if(formElem.name == 'description')
						strSubmit += formElem.name + '=' + escape(formElem.value) + '&';
				break;
			}
			if (formElem.tagName == "SELECT") {
					if(formElem.name == 'state')
						strSubmit += formElem.name + "=" + formElem.options[formElem.selectedIndex].value + "&";
			}
		}
		//document.getElementById('staticmessageloader').style.display = 'block';
		http.open('GET', URL1 + escape(param) + "&" + strSubmit, true);
		http.onreadystatechange = function(){
			if(http.readyState==4){
				if(http.status==200){
					//document.getElementById('staticmessageloader').style.display = 'none';
					if((http.responseText).indexOf('datasaved') != -1){
						document.getElementById("accountdetailsavedleft").innerHTML = "<p><strong>account details has been saved successfully</strong></p>";
						document.getElementById("accountdetailsaved").style.display='block';
					}		
				}else{
					alert("Status Code "+ http.status +" "+ http.statusText);
				}
			}
		}
		http.send(null);
	}
}

/*Function for validating the user account*/
function validateUserAccounts(formname){
	var error = false;
	var formlenght = formname.elements.length;

	for (i = 0; i < formlenght; i++) {
		formElem = formname.elements[i];
		elmname = formElem.name;
		if(formElem.name != "phoneno"){
			switch (formElem.type) {
				case 'text':
				if((formElem.name == "mobileno") && (formElem.value == "")){
					alert("please enter mobile number");
					formElem.focus();
					error = true;
					//return false;
				}
				if((formElem.name == "delivery-date") && (formElem.value == "")){
					alert("please enter delivery date");
					formElem.focus();
					var error = true;
					//return false;
				}
				else if((formElem.name == "delivery-date") && (formElem.value == "")){
					var date = formElem.value;
					var datearray = date.split("/");
					var enteredday = datearray[1];
					var enteredmonth = datearray[0];
					var enteredyear = datearray[2];
					var dateobj = new Date();
					var currday = dateobj.getDate();
					var currmonth = dateobj.getMonth()+1;
					var curryear = dateobj.getFullYear();
					var date_error = false;
					
					if(enteredyear < curryear){
						alert("expected delivery year can not be past year.");
						var error = true;
						formElem.focus();
						//return date_error;
					}
					else if(enteredmonth < currmonth && enteredyear <= curryear){
						alert("expected delivery month can not be past month.");
						error = true;
						formElem.focus();
						//return date_error;
					}
					else if(enteredday < currday && enteredmonth <= currmonth && enteredyear <= curryear){
						alert("expected delivery date can not be past date.");
						error = true;
						formElem.focus();
						//return date_error;
					}
					else if(enteredday == currday){
						alert("expected delivery day can not be the same day. please enter correct date");
						error = true;
						formElem.focus();
						//return date_error;
					}
				}
				else if(formElem.name == "email"){
					if(!echeck(formElem.value)){
						formElem.focus();
						error = true;
						//return false;
					}
				}
				else if(formElem.value == ""){
					alert("please enter "+elmname);
					formElem.focus();
					error = true;
					//return false;
				}
				break;	
			}
		}
		if (formElem.tagName == "SELECT") {
            if(formElem.options[formElem.selectedIndex].value == '0'){
				alert("please select your state");
				error = true;
				formElem.focus();
				//return false;
			}
		}
	}
	return error;
}

function changeColor(jindex, iindex){
    document.getElementById('quantity_'+iindex+'_'+jindex).style.color = "#000000";
}

function onBrowseFile(formObj){
	document.getElementById('indicator').style.display = "block";
	document.attachment.submit();
}

function openSubmitEnquiry(){
    var http = new getHTTPObject();
    document.getElementById("messageloaderbar").style.display = 'block';
    var SUBMIT_ENQUIRY_POPUP = http_loc + "/modules/corporate/submit_query.php";
    http.open('GET', SUBMIT_ENQUIRY_POPUP, true);
    http.onreadystatechange = function(){
        if(http.readyState==4){
            if(http.status==200){
                    document.getElementById("messageloaderbar").style.display = 'none';
                    document.getElementById("submit_enquiry").style.display = 'block';
                    document.getElementById("submit_enquiry").innerHTML = http.responseText;
            }else{
                    document.getElementById("messageloaderbar").style.display = 'none';
                    alert("Status Code "+ http.status +" "+ http.statusText);
            }
        }
    }
    http.send(null);
}

function closePopup(divId){
    document.getElementById(divId).style.display='none';
    document.getElementById("submit_enquiry").innerHTML = "";
    if(document.getElementById('datepicker').style.display =='block'){
        document.getElementById('datepicker').style.visibility='hidden';
        document.getElementById('datepicker').style.display='none';
        document.getElementById('datepickeriframe').style.visibility='hidden';
        document.getElementById('datepickeriframe').style.display='none';
    }
}

function submitQuery(){
    var http = new getHTTPObject();
    
    var action = http_loc + "/modules/corporate/submit_query.php";
	var form= document.getElementById('submit_enquiry_frm_01');
	form.action=action;
	form.method="post";
    var user_name = document.getElementById("user_name").value;
    var cnumber = document.getElementById("cnumber").value;
    var contact_email = document.getElementById("contact_email").value;
    var timelines = document.getElementById("timelines").value;
    var enquiry_details = document.getElementById("enquiry_details").value;
    
    var error = false;
    if(!echeck(document.getElementById("contact_email").value)){
        document.getElementById("contact_email").focus();
        error = true;
        return;
    }else if(document.getElementById("cnumber").value==""){
        alert("Please enter your contact number");
        document.getElementById("cnumber").focus();
        error = true;
        return;
    }

    if(error == false){
		form.submit();
    }
}

function validateDiwaliOrder()
{
	var name = document.getElementById("id_name");
	var email = document.getElementById("id_contact_email");
	var cnumber = document.getElementById("id_cnumber");
	var enquiry_details = document.getElementById("id_enquiry_details");
	var organization = document.getElementById("id_organization");

	var errortd = document.getElementById("errortd");
	var error=false;
	if( name.value.trim() == "" )
	{
		name.focus();
		//errortd.style.visibility = "visible";
		//return;
		error = true;
	}
	if( !error && cnumber.value.trim().length <=8 ) //isNaN(cnumber.value.trim()) )
	{
		cnumber.focus();
		error = true;
	}
	if( !error && !IsNumeric(cnumber.value.trim()) )
	{
		cnumber.focus();
		error = true;
	}
	if( !error && !echeck(email.value.trim()  ))
	{
		email.focus();
		error = true;
	}
	if( !error && enquiry_details.value.trim() == "")
	{
		enquiry_details.focus();
		error = true;
	}
	if( !error && organization.value.trim() == "")
	{
		organization.focus();
		error = true;
	}

	if( error == true )
	{
		errortd.style.visibility = "visible";
	}
	else
	{
		errortd.style.visibility = "hidden";
		document.forms["enqForm"].submit();
	}

}
String.prototype.trim = function() 
{
	a = this.replace(/^\s+/, '');
	return a.replace(/\s+$/, '');
};

function IsNumeric(sText)
{
   var ValidChars = "0123456789";
	var IsNumber=true;
	var Char;

		  
	for (i = 0; i < sText.length && IsNumber == true; i++) 
	{ 
		Char = sText.charAt(i); 
		if (ValidChars.indexOf(Char) == -1) 
		{
			IsNumber = false;
		}
	}
	return IsNumber;																      
}


