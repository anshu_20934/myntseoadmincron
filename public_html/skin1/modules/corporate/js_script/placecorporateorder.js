/*
Function to check breakup quantity
*/
function checkBalancedQuantity(i_index, j_index)
{
	var icount = document.getElementById('icount_'+j_index).value;
	var jcount = document.getElementById('jcount').value;
	var orderquantity = document.getElementById('orderquantity_'+j_index).value
	var breakup_qty = document.getElementById('breakup_'+j_index+'_'+i_index).value;
	var j = j_index;
	
	if (IsQuantity(breakup_qty) == false)
	{
		alert("Please enter valid quantity");
		document.getElementById('breakup_'+j_index+'_'+i_index).style.backgroundColor = "#FF0000";
		return false;
	}
	else
	{
		var totalbreakup_quantity = 0;
		for(i=0; i<icount; i++)
		{
			var breakup = document.getElementById('breakup_'+j+'_'+i).value;
			
			totalbreakup_quantity = parseInt(totalbreakup_quantity) + parseInt(breakup);
		}

		if(totalbreakup_quantity > orderquantity)
		{
			alert("sum of expected break-up quantities must be equal to ordered quantity. please check break-up quantities");
			for(i=0; i<icount; i++)
			{
				var breakup = document.getElementById('breakup_'+j+'_'+i);
				document.getElementById('breakup_'+j+'_'+i).style.backgroundColor = "#FF0000";
			}
			document.getElementById('breakup_'+j+'_'+i_index).focus();
			return false;
		}
		else if((parseInt(icount - 1) == i_index) && (totalbreakup_quantity < orderquantity))
		{
			alert("sum of expected break-up quantities must be equal to ordered quantity. please check break-up quantities");
			for(i=0; i<icount; i++)
			{
				var breakup = document.getElementById('breakup_'+j+'_'+i);
				document.getElementById('breakup_'+j+'_'+i).style.backgroundColor = "#FF0000";
			}
			document.getElementById('breakup_'+j+'_'+i_index).focus();
			return false;
			
		}
		else
		{
			for(i=0; i<icount; i++)
			{
				var breakup = document.getElementById('breakup_'+j+'_'+i);
				document.getElementById('breakup_'+j+'_'+i).style.backgroundColor = "#FFFFFF";
			}
			return true;
		}
	}

}

/*
*Function to validate the valid quantity
*Parameter : quantity text box value
*/
function IsQuantity(sText)
 {
	pattern = /^[0-9]*$/;
	if(pattern.test(sText)==false)
	{
	 	 return false;
	}
	else if(sText == 0)
	{
		return false;
	}
	else
	{
	 	return true;
	}
}


/*
Function for placing the order of created quote
*/

function placeOrderForQuote(ajaxForm)
{
	var strSubmit       = '';
    var formElem;
    var strLastElemName = '';

    if(window.XMLHttpRequest) {
	req = new XMLHttpRequest(); 
     } else if (window.ActiveXObject) {
	req  = new ActiveXObject("Microsoft.XMLHTTP"); 
     }
	var SAVEORDERURL = "modules/corporate/placecorporateorder.php";

	for (i = 0; i < ajaxForm.elements.length; i++) 
	{
		formElem = ajaxForm.elements[i];
		switch (formElem.type) 
		{
		// Text, select, hidden, password, textarea elements
			case 'text':
			case 'hidden':
			
				strSubmit += formElem.name +
				'=' + escape(formElem.value) + '&'

			break;
		}
		 if (formElem.type == "radio") {
			if (formElem.checked) {
				strSubmit += formElem.name + "=" + formElem.value + "&";
			}
		}
		

	}
	
	//alert(strSubmit);
	req.onreadystatechange = function() 
	{ 
		if(req.readyState == 4) 
		{ 
			  if(req.status == 200) 
			  { 
				var result = req.responseText;
				
				if(result)
				{
					document.getElementById('placeorderloader').style.visibility = 'hidden';
					document.getElementById('placeorderloader').style.display = 'none';
					document.getElementById('corporateorder').style.visibility = 'hidden';
					document.getElementById('corporateorder').style.display = 'none';
					document.getElementById('orderid').innerHTML = result;
					document.getElementById('successcorporateorder').style.visibility = 'visible';
					document.getElementById('successcorporateorder').style.display = '';
					
				}
				else
				{
					document.getElementById('placeorderloader').style.visibility = 'hidden';
					document.getElementById('placeorderloader').style.display = 'none';
					
					alert("data can not be saved. please contact to administrator!!");
				}
				
			  }
			  else
					alert("status is " + req.status);
		}
	 }

	 //document.getElementById('quoteaddressblock').style.visibility = 'hidden';
	 //document.getElementById('quoteaddressblock').style.display = 'none';
	// document.getElementById('quotecreateblock').style.visibility = 'hidden';
	 //document.getElementById('quotecreateblock').style.display = 'none';
	 document.getElementById('placeorderloader').style.visibility = 'visible';
	 document.getElementById('placeorderloader').style.display = '';
	 
	 req.open("POST", SAVEORDERURL, true); 
	 req.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	 req.send(strSubmit);
}

/*
Function to display the image
*/

function viewImage(image)
{
	newwindow = window.open( http_loc+"/modules/corporate/viewproductimage.php?image="+image, "corporatequote", "width=800,height=500,toolbar=no,status=no,scrollbars=yes,resizable=yes,menubar=no,location=no,direction=no");
	if (window.focus) {newwindow.focus();}
	
}