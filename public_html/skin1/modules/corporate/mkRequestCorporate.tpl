<div class="form" > 
{if $login eq ""}
	{include file="main/login_form.tpl"}
{else}

<form name="request" action="modules/corporate/mkrequest.php" method="post">
<input type="hidden" name="mode" id="mode"  />
<input type="hidden" name="corporate_id" id="corporate_id" value="{$corporate_id}"  />	
  <div class="subhead" ><p>request form</p></div>
  	<div class="links"></div>
  	<div class="links"> 
	<table width="95%" class='formtable' cellpadding='2' cellspacing='2' border=0 style='border: 1px solid #999999;' >
		<tr class="trhead" >
			<td width='90%' colspan="5"  >
				<div style="width:540px;"><p><strong>account details</strong></p></div>
			</td>
		</tr>
		<tr style="height:20px;">
			<td width='90%' colspan="5"  >
				<div id="accountdetailsaved" class="trhead" style="display:none;"><div id="accountdetailsavedleft" style="float:left;width:300px;"></div><div id="accountdetailsavedright" style="float:right;margin-right:3px;width:auto;cursor:pointer;" onClick="javascript:document.getElementById('accountdetailsaved').style.display='none'"><strong>hide<strong><div></div>
			</td>
		</tr>
		<tr>
			<td tabindex='1'><p>contact name<span class="mandatory">*</span></P></td>
			<td><input type="text" name="firstname" id="firstname" value="{$fname} {$lname}"></td>
			<td tabindex='2'><p>company name<span class="mandatory">*</span></P></td>
			<td><input type="text" name="company" id="company" value="{$company}" ></td>
		</tr>
		<tr><td colspan="2"></td></tr>
		<tr>
			<td tabindex='3'><p>contact email<span class="mandatory">*</span></P></td>
			<td><input type="text" name="email" id="email" value="{$email}" ></td>
			<td tabindex='4'><p>expecting delivery by<span class="mandatory">*</span></p></td>
			<td style="vertical-align:middle;">
			<div style="width:120px;height:20px;">
				<div style="float:left;margin-top:2px;width:70px;height:20px;">
					<input type="text" name="delivery-date" id="delivery-date" size="7" MaxLength="11" value="{$deliverydate}"  readonly>
				</div>
				<div style="float:left;width:35px;height:20px;">
					<img src="{$cdn_base}/images/cal.gif"  border="0" alt="Click Here to Pick up the date" onclick="displayDatePicker('delivery-date');">
				</div>
			</div>
			</td>
		</tr>
		<tr><td colspan="2"></td></tr>
		<tr>
			 <td><p>phone</P></td>
			<td><input type="text" name="phoneno" id="phoneno" value="{$phone}"></td>
			 <td><p>moblie<span class="mandatory">*</span></P></td>
			<td><input type="text" name="mobileno" id="mobileno" value="{$mobile}"></td>
		</tr>
		<tr><td colspan="2"></td></tr>
		<tr>
			 <td><p>city<span class="mandatory">*</span></p></td>
			 <td><input type="text" name="city" id="city" value="{$city}"></td>
			 <td><p>state<span class="mandatory">*</span></p></td>
			 <td>
			 <select name="state" id="state" class="select" >
				<option value="0" selected>--Select State--</option>
				{section name="statename" loop=$returnState}
					{if $returnState[statename].code == $statecode}
					<option value={$returnState[statename].code} selected>{$returnState[statename].state}</option>
					{else}
					<option value={$returnState[statename].code}>{$returnState[statename].state}</option>
					{/if}
				{/section}
 			 </select>
			
			</td>
		</tr>
		<tr><td colspan="2"></td></tr>
		<tr>
			<td><p>description</P></td>
			<td colspan="3"><textarea name="description" id="description" style="width:200px;height:50px;" >{$description}</textarea></td>
		</tr>
		
		<tr>
        <td colspan='4' align='right'>
            <input class='submit' type='button' name="saveaccountdetails" id="saveaccountdetails" value='save account details' onClick="javascript:corporatecontactdata('accountdetails',this.form);">&nbsp; 
        </td>
        </tr>
	</table>
		</div>

	<div class="links"> 
	 <br/>
	 <table width='95%' border='0' class='formtable' cellpadding='2' cellspacing='2' style='border: 1px solid #999999;' >
			{if $styles}
			<tr class="trhead" >
				<td width='90%' colspan="5"  >
					<div style="width:540px;"><p><strong>products in my request</strong>
					<a href="javascript:staticPopupMessage('request_help');" style="font-size:1.0em;color:#FFFFFF;padding-left:380px;">
					<img  style="border: 0px none ;" src="./skin1/mkimages/help-icon.gif"/></a></p></div>
				</td>
				<td width='5%' align="right"></td>
			</tr>
			  <tr class="trhead">
				<td width='10%' align="center"><p><input type="checkbox" class="checkbox" id="select" name="select" onClick="javascript:selectAll('parent');"></p></td>
				<td width='25%' align="left"><p><strong>product name</strong></p></td>
				<td width='15%' align="left"><p><strong>quantity</strong></p></td>
				<td width='25%' align="left"><p><strong>your designs</strong></p></td>
				<td width='20%' align="left"><p><strong>action</strong></p></td>
			  </tr>
			{assign var=i value=0}
			{foreach name=outer item=style from=$styles}
			{foreach key=key item=item from=$style}
				{if $key eq 'styleid'}
					{assign var='styleid' value=$item}
				{/if}
				{if $key eq 'stylename'}
					{assign var='selctedstyle' value=$item}
				{/if}
				{if $key eq 'quantity'}
					{assign var='quantity' value=$item}
				{/if}
				{if $key eq 'suggestion'}
					{assign var='suggestion' value=$item}
				{/if}
			 {/foreach}
			  <tr class="trbody">
				<td width='10%' align="center"><p><input type="checkbox" class="checkbox" id="delstyle" name="delstyle[]" value="{$styleid}" onClick="javascript:selectAll('child');"></p></td>
				<td width='20%' align="left"><p>{$selctedstyle}</p></td>
				<td width='15%' align="left"><p>
					<table width='100%'>
						<tr>
							<td width='50%'><input type='text' id="quantity_{$i}" name="quantity_{$i}" style='width:50px;' maxlenght='5' value='{$quantity}' onBlur="javascript: updateQunatity('{$i}', '{$styleid}', 'updateqty');"></td>
							<td width='50%'><img src="{$cdn_base}/images/order_loader.gif"  id="loader_{$i}" style="visibility:hidden;display:none;"/></td>
						</tr>
					</table></p>
				</td>    
				<td width='30%' align="left"><p>
				<table border='0' width='85%' style="font-size:1.0em;color:#666666;">
					{assign var=adddesign value="add new design"}
					{assign var=nodesign value="no design added"}
					{foreach key=parentkey item=parent from=$productname}
							
						{if $parentkey eq $styleid}
							{assign var=adddesign value="add more"}	
							{section name=pname loop=$parent}
								{assign var=nodesign value=$parent[pname]}
								{if $parent[pname] != 'suggestion'}
								<tr>
									<td align="left">
									<a href="javascript:createArray('{$parentkey}', '{$smarty.section.pname.index}', '', 'create');">{$nodesign}</a>
									</td>
								</tr>
								{else}
								<tr>
									<td align="left"><a href="javascript:showSuggestion('{$parentkey}','{$smarty.section.pname.index}', 'create');">{$nodesign}</a></td>
								</tr>
								{/if}
							{/section}
						{/if}	
						
					{/foreach}

					 {if $nodesign == 'no design added'}
						<tr>
							<td align="left" class="mandatory">{$nodesign}</td>
						</tr>
					 {/if}
					
				</table></p>
				</td>
				
				<td width='20%' align="left"><p><a href='create-my/{$selctedstyle}/{$styleid}?cstatus=notcustomized&design=corporate&from=mkrequest'>{$adddesign}</a></p></td>
				
				
			  </tr>
				{assign var=i value=$i+1}
			   {/foreach}
			  <tr><td colspan='5'>&nbsp;</td></tr>
			  <tr><td colspan='5'>&nbsp;</td></tr>
			  <tr>
			 
				<td  colspan='1' valign="middle">
					<input class='submit' type='button' name="deleteproduct" id="deleteproduct" value='remove' onClick="javascript: deleteAll('mkrequest');"> &nbsp; 
				</td>
				<td  colspan='4' align='right' >
					<input class='submit' type='button' name="addproduct" id="addproduct" value='add product' onClick="javascript:custArea('mkrequest', this.form);"> &nbsp; 
				</td>
			  </tr> 
			  {else}
			  <tr class="trhead">
				<td colspan="5" align="right"><p><a href="javascript:staticPopupMessage('request_help');" style="font-size:1.0em;color:#FFFFFF;"><img  style="border: 0px none ;" src="./skin1/mkimages/help-icon.gif"/></a></p></td>
			</td>
			   <tr>
				<td colspan='5' class="mandatory">no products have been added to this quote. </td>
			  </tr>
			  <tr>
				<td  colspan='1' valign="middle">
					<input class='submit' type='button' name="deleteproduct" id="deleteproduct" value='remove' onClick="javascript: deleteAll('mkrequest');" disabled> &nbsp; 
				</td>
				<td  colspan='4' align='right' >
					<input class='submit' type='button' name="addproduct" id="addproduct" value='add product' onClick="javascript:custArea('mkrequest', this.form);"> &nbsp; 
				</td>
			  </tr> 
			  {/if}
			   <input type="hidden" name="numrecord" id="numrecordid" value="{$i}"  />
		 </table>
		 {if $styles || $attachment_array}
			<input type="hidden" name="data_added" id="data_added" value="1"  />
		{else}
			<input type="hidden" name="data_added" id="data_added" value="0"  />
		{/if}
</form>
		<div class="links">&nbsp;</div>
	 	  <table width='95%' class='formtable' cellpadding='0' cellspacing='2' border='0' style='border: 1px solid #999999;' >
			<tr class="trhead">
				<td colspan="5"><p><strong>attachment&nbsp;<a href="javascript:staticPopupMessage('attachments');" style="font-size:1.0em;color:#FFFFFF;margin-left:625px;"><img  style="border: 0px none ;" src="./skin1/mkimages/help-icon.gif"/></a></strong></p></td>
			</tr>
			{if $smarty.get.upload == 'yes'}
			<!--<tr><td width='100%' colspan='4' style="background-color:#FFFFFF;height:2px;" ></tr>-->
			<tr class="trhead">
				<td colspan="5"><p><strong>{$smarty.get.f|base64_decode} uploaded successfully</strong></p></td>
			</tr>
			{/if}
			<tr>
				<td width='100%' colspan='4'>&nbsp;</td>
			</tr>  
			<tr>
				<td width='100%' colspan='4'> 
					<iframe name="iframe" src="modules/corporate/attachmentupload.php" style="width:540px;height:70px;border: 0px solid #FFFFFF;" frameborder='0' scrolling="no"> </iframe>
				</td>
			</tr>  
			<tr>
				<td width='100%' colspan='4'>&nbsp;</td>
			</tr>
			{section name=attach loop=$attachment_array}
			<tr>
				<td width='70%' colspan='2' style="background-color:#FFCD9B;height:15px;" >
				<div id="uploaded_file_path_{$smarty.section.attach.index}" style="color:#333333;font-size:1.0em;">
				<p><strong>{$attachment_array[attach]}</strong></p>
				</div>
				
				</td>
				<td width='30%' colspan='2' style="background-color:#FFCD9B;height:15px;" align="center">
				<div style="background-color:#FFCD9B;width:100px;color:#333333;font-size:1.0em;"><strong>
				<a href="javascript: void(0);" onClick="javascript: deleteUploadedFile({$smarty.section.attach.index});" style="margin-left">delete</a></strong>
				</div>
				</td>
			</tr> 
			<!--<tr><td width='100%' colspan='4' style="background-color:#FFFFFF;height:2px;" ></tr>-->
			{/section}
		 </table>
	  <div class="links">&nbsp;</div>
	  <div style='float:right;margin-right:15px;'>
		<input class='submit' type='button' name='savedraft' value='save request' onClick="return isDesignAdded(document.request, 'savedraft', 'requestconfirm', '{$nodesign}')"> 
		<input class='submit' type='button' name='sendrequest' value='send request for quote' onClick="return isDesignAdded(document.request, 'requestconfirm', 'requestconfirm', '{$nodesign}')"> 
	  </div>
	  </div>
	  <div class="foot"></div>
	{/if}
</div>