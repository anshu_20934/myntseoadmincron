<div class="form" >
   <div class="super" > 
	<p>manage request</p>			
 </div>
  <div class="head" > 
	<p>request details</p>			
 </div>
<div class="links"> 
	 <table width='95%' class='formtable' cellpadding='0' cellspacing='2' border='0' >
		  <tr>
			<td width='25%'><b>request id</td>
			<td width='25%'>{$requestid}</td>
			<td width='25%'><b>request date</td>
			<td width='25%'>{$requestdate}</td>
		  </tr>
		  <tr>
			<td width='25%'><b>requestor</td>
			<td width='25%'>{$name}</td>
			<td width='25%'><b>status</td>
			<td width='25%'>
				{if $request_status == 'S'} submitted {/if}	
				{if $request_status == 'D'} draft {/if}
				{if $request_status == 'QA'} quote/negotiation {/if}
				{*{if $request_status == 'CW'} closed-won {/if}*}
				{if $request_status == 'CL'} completed {/if}
				{if $request_status == 'C'} cancelled {/if}
			</td>
		  </tr>
		  <tr>
			<td colspan='4'>&nbsp;</td>
		  </tr>
	 </table>
</div>


<div class="foot"></div>
</div>