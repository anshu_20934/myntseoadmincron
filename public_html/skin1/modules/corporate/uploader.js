var http=createRequestObject();
var uploader="";
var uploadDir="";
var dirname="";
var filename="";
var timeInterval="";
var idname="";

function createRequestObject() {
    var obj;
    var browser = navigator.appName;
    if(browser == "Microsoft Internet Explorer"){
    	return new ActiveXObject("Microsoft.XMLHTTP");
    }
    else{
    	return new XMLHttpRequest();
    }   
}
function traceUpload() {
   http.onreadystatechange = handleResponse;
   http.open("GET", 'modules/corporate/imageupload.php?uploadDir='+uploadDir+'&dirname='+dirname+'&filename='+filename+'&uploader='+uploader); 
   http.send(null);   
}
function handleResponse() {
	
	if(http.readyState == 4){
		var response=http.responseText.split("|"); 

		if(response.indexOf("File uploaded") != -1){
			clearTimeout(timeInterval);
		}
		
		if(document.getElementById("imagecheck").value != "")
		{
			document.getElementById("loadedimage"+response[1]).style.visibility="hidden";
			document.getElementById("loadedimage"+response[1]).style.display='none';
		}
		//document.getElementById("showimage"+response[1]).style.visibility="visible";
		//document.getElementById("showimage"+response[1]).style.display='';
		
        document.getElementById("showimage"+response[1]).innerHTML=response[0];
		document.getElementById("filename_"+response[1]).value=response[2];
    }
}
function uploadFile(obj, dname, index) 
{
	uploadDir=obj.value;
	idname=obj.name;
	dirname=dname;
	filename=uploadDir.substr(uploadDir.lastIndexOf('\\')+1);
	uploaderId = obj.name;
	uploader = obj.name;
	document.getElementById('mydesign_'+index).submit();
	timeInterval=setTimeout("traceUpload()", 1500);
}