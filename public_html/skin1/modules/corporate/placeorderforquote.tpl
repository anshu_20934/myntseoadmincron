{config_load file="$skin_config"}
<html>
<head>
<title>myntra - request quote</title>
{include file="mykritimeta.tpl" }
<script type="text/javascript" src="{$SkinDir}/modules/corporate/js_script/placecorporateorder.js" ></script>
</head>
<body id="body" >

 <div id="placeorderloader" style="visibility:hidden;display:none">
  <div id="overlay"></div>
  <div id="messagewide" style="position:absolute; left:-100px; top:0px;">
    <div class="wrapper">
     <div class="head"><a href="javascript:hidestaticmessageDiv('staticmessage')" >Click to close</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
      <div class="body" style="height:100px;">please wait... your order is being processed<p><img src='{$cdn_base}/images/image_loading.gif' style="position:absolute;top:50px;left:180px;" ></p></div>
     <div class="foot"></div>
    </div>
  </div>
</div>
<div id="scroll_wrapper"> 
  <div id="container_860"> 
	{include file="mykrititop.tpl" }
  <div id="wrapper_860"> 
	<div id="display"> 
	<div class="center_column_850"> 
		<div id="corporateorder">
		<div class="form_840" > 
			<div class="longsubhead_840" ><p>request details</p></div>
				<div class="longtext_840"> 
				 <table width='95%' class='formtable' cellpadding='0' cellspacing='2' border='0' >
					  <tr>
						<td width='25%'><b>request id</td>
						<td width='25%'>{$requestid}</td>
						<td width='25%'><b>request date</td>
						<td width='25%'>{$requestdate}</td>
					  </tr>
					  <tr>
						<td width='25%'><b>requestor</td>
						<td width='25%'>{$name}</td>
						<td width='25%'><b>status</td>
						<td width='25%'>
							{if $request_status == 'S'} submitted {/if}	
							{if $request_status == 'D'} draft {/if}
							{if $request_status == 'QA'} quote/negotiation {/if}
							{if $request_status == 'CO'} closed-won {/if}
							{if $request_status == 'CA'} closed-lost {/if}
							{if $request_status == 'C'} cancelled {/if}
						</td>
					  </tr>
					  <tr>
						<td colspan='4'>&nbsp;</td>
					  </tr>
				 </table>
			</div>
			<div class="bigfoot_840"></div>
	        </div>
{*===============================================================================*}
{*==================================Place order block================================*}
{*===============================================================================*}
	<form name="orderquote" id="orderquote" method="post">
		<input type="hidden" name="requestid" id="requestid" value='{$requestid}'>
		<input type="hidden" name="quotenumber" id="quotenumber" value='{$quotenumber}'>
		<div class="form_840" > 
			<div class="longsubhead_840" ><p>details of quote number {$quotenumber}</p></div>
				<div class="longtext_840"> 
					<table width='95%' class='formtable' cellpadding='0' cellspacing='2' border='0' >
						  <tr class="trhead">
							<td width='15%' ><p><strong>item</strong></p></td>
							<td width='25%' ><p><strong>select design</strong></p></td>
							<td width='40%' ><p><strong>quantity break-up</strong></p></td>
							<td width='5%' ><p><strong>total quantity</strong></p></td>
							<td width='5%' ><p><strong>unit price</strong></p></td>
							<td width='5%' ><p><strong>total price</strong></p></td>
						  </tr>
						  {assign var=tabindex value=0}
						  {assign var=j value=0}
						{section name=quote loop=$quote_details}
							
						  <tr class='trbody'>
							<td width='15%' class="extratd"><p>{$quote_details[quote].stylename}</p></td>
								
							<td width='35%' class="extratd">
								{assign var=productarray value=$quote_details[quote].products}
								<table width='100%'  cellpadding='0' cellspacing='1' border='0' style="background-color:#EFF7FF;font-size:0.9em;color:#02679C;border: 1px solid #02679C;">
									 <tr style="background-color:#336699;color:#FFFFFF;font-size:1.0em;font-weight:100;">
										<td width='15%' ><p><strong>select</strong></p></td>
										<td width='40%' ><p><strong>design name</strong></p></td>
										<td width='35%' ><p><strong>design view</strong></p></td>
									</tr>
									{assign var=i value=0}
									{section name=product loop=$productarray}
									<tr >
										<td width='15%' align='center'><input type="radio" name="design[{$quote_details[quote].styleid}]" id="design_{$productarray[product].productid}" value='{$productarray[product].productid}' class="radio" style="width:10px;" {if $i==0}checked{/if}></td>
										<td width='40%' ><p><strong>{$productarray[product].product}</strong></p></td>
										<td width='35%' ><p><a href="javascript: viewImage('{$productarray[product].image_name|base64_encode}')" >view</a></p></td>
									</tr>
									{assign var=i value=$i+1}
									{/section}
								</table>
							</td>

							<td width='35%' class="extratd">
								{assign var=optarray value=$quote_details[quote].options}
								{if $optarray|is_array}
									<table width='95%' class='formtable' cellpadding='0' cellspacing='1' border='0' style="background-color:#EFF7FF;font-size:0.9em;color:#02679C;border: 1px solid #02679C;">
										<tr >
											{assign var=i value=0}
											{section name=opt loop=$optarray}
												<input type="hidden" name="optionnamearray[{$quote_details[quote].styleid}_{$i}]" id="optionnamearray" value='{$optarray[opt]}'>
												<td width='10%' rowspan='2'>
												<p>{$optarray[opt]}</p>
												<p><input type="text" name="breakup[{$quote_details[quote].styleid}_{$i}]" id="breakup_{$j}_{$i}" value='0' size="6" maxlenght="6" tabindex='{$tabindex}' onBlur="javascript: checkBalancedQuantity('{$i}', '{$j}');"></p>
												</td>
											{assign var=i value=$i+1}
											{assign var=tabindex value=$tabindex+1}
											{/section}
											<input type="hidden" name="icount_{$j}" id="icount_{$j}" value='{$i}'>
											<input type="hidden" name="{$quote_details[quote].styleid}_count" id="icount_{$j}" value='{$i}'>
										</tr>
									</table>
								{else}
									<table width='95%' class='formtable' cellpadding='0' cellspacing='1' border='0' style="background-color:#EFF7FF;font-size:1.0em;color:#02679C;border: 1px solid #02679C;">
										<tr >
											<td width='10%' rowspan='2'>
											<p><strong>{$quote_details[quote].total_quantity}</strong>
												<input type="hidden" name="withoutbreakup[{$quote_details[quote].styleid}]" id="withoutbreakup" value='{$quote_details[quote].total_quantity}' size="6" maxlenght="6" readonly>
											</p>
											</td>
										</tr>
									</table>
								{/if}
								
							</td>
							<td width='5%' class="extratd">
							<p><strong>{$quote_details[quote].total_quantity}</strong>
								<input type="hidden" name="orderquantity[{$quote_details[quote].styleid}]" id="orderquantity_{$j}" value='{$quote_details[quote].total_quantity}' size="6" maxlenght="6" readonly>
							</p>
							</td>
							<td width='5%' class="extratd">
							<p><strong>Rs.{$quote_details[quote].price}</strong>
								<input type="hidden" name="unitprice[{$quote_details[quote].styleid}]" id="unitprice_{$j}" value='{$quote_details[quote].price}' size="6" maxlenght="6" readonly>
							</p>
							</td>
							<td width='5%' class="extratd">
							<p><strong>Rs.{$quote_details[quote].totalprice}</strong>
								<input type="hidden" name="totalprice[{$quote_details[quote].styleid}]" id="totalprice_{$j}" value='{$quote_details[quote].totalprice}' size="6" maxlenght="6" readonly>
							</p>
							</td>
						</tr>
						 {assign var=j value=$j+1}
						{/section} 
						<input type="hidden" name="jcount" id="jcount" value='{$j}'>
						   <tr class='trbody'>
							<td  colspan='5' align='right'>Subtotal</td>
							<td  colspan='1'>
							<p><strong>Rs.{$quoteresult.sub_total}</strong>
								<input type="hidden" name="subtotal" id="subtotal" value='{$quoteresult.sub_total}' size="6" maxlenght="6" readonly>
							</p>
							</td>
						  </tr>  
						  <tr class='trbody'>
							<td  colspan='5' align='right' class="mandatory">Discount</td>
							<td  colspan='1'>
							<p><strong>Rs.{$quoteresult.discount}</strong>
								<input type="hidden" name="discount" id="discount" value='{$quoteresult.discount}' size="6" maxlenght="6" readonly>
							</p>
							</td>
						  </tr> 
						  <tr class='trbody'>
							<td  colspan='5' align='right' class="mandatory">Total after discount</td>
							<td  colspan='1'>
							<p><strong>Rs.{$quoteresult.totalafterdiscount}</strong>
								<input type="hidden" name="totalafterdiscount" id="totalafterdiscount" value='{$quoteresult.totalafterdiscount}' size="6" maxlenght="6" readonly>
							</p>
							</td>
						  </tr> 
						  <tr class='trbody'>
							<td  colspan='5' align='right' class="mandatory">VAT/CST</td>
							<td  colspan='1'>
							<p><strong>Rs.{$quoteresult.vat}</strong>
								<input type="hidden" name="vat" id="vat" value='{$quoteresult.vat}' size="6" maxlenght="6" readonly>
							</p>
							</td>
						  </tr> 
						  <tr class='trbody'>
							<td  colspan='5' align='right' class="mandatory">Shipping</td>
							<td   colspan='1'>
							<p><strong>Rs.{$quoteresult.shipping}</strong>
								<input type="hidden" name="shipping" id="shipping" value='{$quoteresult.shipping}' size="6" maxlenght="6" readonly>
							</p>
							</td>
							
						  </tr> 
						  <tr class='trbody'>
							<td  colspan='5' align='right' class="mandatory"><p><strong>Grand Total</strong></p></td>
							<td  colspan='1'>
							<p><strong>Rs.{$quoteresult.grand_total}</strong>
								<input type="hidden" name="grand_total" id="grand_total" value='{$quoteresult.grand_total}' size="6" maxlenght="6" readonly>
							</p>
							</td>
						  </tr> 
						   <tr class='trbody'><td  colspan='6' >&nbsp;</td></tr>	
						  <tr class='trbody'>
							
							<td colspan='6' align='right'>
								<p>
								<input class='submit' type='button' name="closewindow" value='close window' onClick="javascript:window.close();">&nbsp;&nbsp;&nbsp;
								{if $ostatus == 'O'}
									<input class='submit' type='button' name="placeorder" value='reorder for this quote' onClick="javascript:placeOrderForQuote(this.form)">
								{else}
									<input class='submit' type='button' name="placeorder" value='place my order' onClick="javascript:placeOrderForQuote(this.form)">
								{/if}
								</p>
							</td>
						  </tr>
					 </table>
				</div>
			<div class="bigfoot_840"></div>
	        </div>
	</form>
	   </div>
{*===============================================================================*}
{*==================================End Place order block=============================*}
{*===============================================================================*}
	<div id="successcorporateorder" style="visibility:hidden;display:none"> 
	<div class="form_840" > 
		<div class="longsubhead_840" ><p>congratulations!!</p></div>
			<div class="longtext_840"> 
			 <table width='95%' class='formtable' cellpadding='0' cellspacing='2' border='0' >
				  <tr>
					<td width='25%'><b>Thank you for placing your order at myntra.com. Your order id is <span id="orderid"></span>. Someone from our fulfilment team will get in touch with you shortly to discuss the next steps for your order. If you have any questions, you can email us at support@myntra.com</td>
				  </tr>
			 </table>
			</div>
		<div class="bigfoot_840"></div>
	</div>	
	</div>

	</div>
    <div class="clearall"></div>
  </div>
  </div>
   {include file="footer_nonlink.tpl" }
{include file="site/javascript.tpl"}
</body>
</html>