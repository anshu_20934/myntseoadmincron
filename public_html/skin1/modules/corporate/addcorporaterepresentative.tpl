{* $Id: register.tpl,v 1.51.2.2 2006/06/29 13:31:45 svowl Exp $ *}
{config_load file="$skin_config"}
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<div class="subhead"><p>invite friends&nbsp;<a href="javascript:staticPopupMessage('invite_friends');" style="font-size:1.0em;color:#FFFFFF;margin-left:435px;"><img  style="border: 0px none ;" src="{$cdn_base}/skin1/mkimages/help-icon.gif"/></a></p></div>
<div>
	<form name="addaccess" method="post">
	<input type="hidden" name="mode" id="mode" value="">
	<input type="hidden" name="requestid" id="requestid" value="{$requestid}">
	<input type="hidden" name="corporate_id" id="corporate_id" value="{$corporate_id}"  />
		<input type="hidden" name="usertype" id="usertype" value="C"/>
		<table width='90%' class='table' >
			<tr id="errormessage1" style="visibility:hidden;display:none;">
				<td colspan="2" align="center">
					<span class="mandatory"><div id="displayerror1"></div></span>
				</td>
			</tr>
			<tr>
				<td colspan='2'><p>Ask your team or friends to help you decide among various designs or suggest modifications. Enter email address of your friends and click on Invite Friends. Your friends will get a personalized email with instructions to log into this page to start helping you with the design process.</p></td>
			</tr>
			<tr><td colspan='2'>&nbsp;</td></tr>
			<tr>
				<td><p>first name&nbsp;<input type="text" name="firstname[]" id="firstname_0" style="width:120px;" />&nbsp;&nbsp;&nbsp;&nbsp;last name&nbsp;<input type="text" name="lastname[]" id="lastname_0" size="20" style="width:120px;" />&nbsp;&nbsp;&nbsp;&nbsp;e-mail&nbsp;<input type="text" name="email[]" id="email_0" /></p></td>
				<td><p></p></td>
			</tr>
			<tr>
				<td><p>first name&nbsp;<input type="text" name="firstname[]" id="firstname_1" style="width:120px;" />&nbsp;&nbsp;&nbsp;&nbsp;last name&nbsp;<input type="text" name="lastname[]" id="lastname_1" size="20" style="width:120px;" />&nbsp;&nbsp;&nbsp;&nbsp;e-mail&nbsp;<input type="text" name="email[]" id="email_1" /></p></td>
				<td><p></p></td>
			</tr>
			<tr>
				<td><p>first name&nbsp;<input type="text" name="firstname[]" id="firstname_2" style="width:120px;" />&nbsp;&nbsp;&nbsp;&nbsp;last name&nbsp;<input type="text" name="lastname[]" id="lastname_2" size="20" style="width:120px;" />&nbsp;&nbsp;&nbsp;&nbsp;e-mail&nbsp;<input type="text" name="email[]" id="email_2" /></p></td>
				<td><p></p></td>
			</tr>
			<tr>
				<td><p>first name&nbsp;<input type="text" name="firstname[]" id="firstname_3" style="width:120px;" />&nbsp;&nbsp;&nbsp;&nbsp;last name&nbsp;<input type="text" name="lastname[]" id="lastname_3" size="20" style="width:120px;" />&nbsp;&nbsp;&nbsp;&nbsp;e-mail&nbsp;<input type="text" name="email[]" id="email_3" /></p></td>
				<td><p></p></td>
			</tr>
			<tr>
				<td><p>first name&nbsp;<input type="text" name="firstname[]" id="firstname_4" style="width:120px;" />&nbsp;&nbsp;&nbsp;&nbsp;last name&nbsp;<input type="text" name="lastname[]" id="lastname_4" size="20" style="width:120px;" />&nbsp;&nbsp;&nbsp;&nbsp;e-mail&nbsp;<input type="text" name="email[]" id="email_4" /></p></td>
				<td><p></p></td>
			</tr>
			<tr><td colspan="2">&nbsp;</td></tr>
			<tr>
				<td colspan="2" align="right">
					<input type="button" border="0" value="invite friends" name="submitButton" class="submit" onclick="return validateCorporateRepresentative(this.form, 'addrep', 'mycorporateaccount');"/>
				</td>
			</tr>
		</table>
	</form>
</div>
<div class="links">&nbsp;</div>
<div class="links">
	{if $resultset}
	<table width='95%' class='formtable' style='border: 1px solid #999999;' >
		<tr class='trhead'>
			<td colspan="3"><p><strong>my invited friends for the request number {$requestid}</strong></p></td>
		</tr>
		<tr class='trhead'>
			<td ><p><strong>email</strong></p></td>
			<td ><p><strong>name</strong></p></td>
			<td ><p><strong>remove access</strong></p></td>
		</tr>
		{section name=result loop=$resultset}
		<tr class='trbody'>
			<td class='align_left'><p>{$resultset[result].email}</p></td>
			<td class='align_left'><p>{$resultset[result].firstname}&nbsp;{$resultset[result].lastname}</p></td>
			<td class='align_left'><p><a href='modules/corporate/mkmanagerequest.php?mode=delete&id={$resultset[result].email|base64_encode}&cid={$resultset[result].corporate_id|base64_encode}&requestid={$requestid}' style="text-decoration: none;cursor: pointer;color:#FF0000;">Remove</a></p></td>
		</tr>
		{/section}
	</table>
	{/if}
</div>
<div class="foot"></div>
