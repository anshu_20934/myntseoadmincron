<div id="alertmessage" style="visibility:hidden;display:none">
  <div id="overlay"></div>
  <div id="messagewide" style="position:absolute;left:-100px; top:0px;">
    <div class="wrapper">
     <div class="head"><a href="javascript:hidealertmessageDiv('alertmessage')" >Click to close</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
      <div class="body" id="amessage"></div>
     <div class="foot"></div>
    </div>
  </div>
</div>
<div id="alertmessageloader" style="visibility:hidden;display:none">
  <div id="overlay"></div>
  <div id="messagewide" style="position:absolute;left:-100px; top:0px;">
    <div class="wrapper">
     <div class="head"><a href="javascript:hidealertmessageDiv('alertmessageloader')" >Click to close</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
      <div class="body" style="height:100px;"><p><img src='{$cdn_base}/images/image_loading.gif' style="position:absolute;top:50px;left:180px;" ></p></div>
     <div class="foot"></div>
    </div>
  </div>
</div>