<div class="form" > 
{if $login eq ""}
	{include file="main/login_form.tpl"}
{else}

<form name="request" action="modules/corporate/mkrequest.php" method="post">
<input type="hidden" name="mode" id="mode"  />
<input type="hidden" name="corporate_id" id="corporate_id" value="{$corporate_id}"  />	
  <div class="subhead" ><p>request form</p></div>
  	<div class="links"></div>
  	<div class="links"> 
	<table width="95%" class='formtable' cellpadding='2' cellspacing='2' border=0 style='border: 1px solid #999999;' >
		<tr class="trhead" >
			<td width='90%' colspan="5"  >
				<div style="width:540px;"><p><strong>account details</strong></p></div>
			</td>
		</tr>
		<tr style="height:20px;">
			<td width='90%' colspan="5"  >
				<div id="accountdetailsaved" class="trhead" style="display:none;"><div id="accountdetailsavedleft" style="float:left;width:300px;"></div><div id="accountdetailsavedright" style="float:right;margin-right:3px;width:auto;cursor:pointer;" onClick="javascript:document.getElementById('accountdetailsaved').style.display='none'"><strong>hide<strong><div></div>
			</td>
		</tr>
		<tr>
			<td tabindex='1'><p>contact name<span class="mandatory">*</span></P></td>
			<td><input type="text" name="firstname" id="firstname" value="{$fname} {$lname}"></td>
			<td tabindex='2'><p>company name<span class="mandatory">*</span></P></td>
			<td><input type="text" name="company" id="company" value="{$company}" ></td>
		</tr>
		<tr><td colspan="2"></td></tr>
		<tr>
			<td tabindex='3'><p>contact email<span class="mandatory">*</span></P></td>
			<td><input type="text" name="email" id="email" value="{$email}" ></td>
			<td tabindex='4'><p>expecting delivery by<span class="mandatory">*</span></p></td>
			<td style="vertical-align:middle;">
			<div style="width:120px;height:20px;">
				<div style="float:left;margin-top:2px;width:70px;height:20px;">
					<input type="text" name="delivery-date" id="delivery-date" size="7" MaxLength="11" value="{$deliverydate}"  readonly>
				</div>
				<div style="float:left;width:35px;height:20px;">
					<img src="{$cdn_base}/images/cal.gif"  border="0" alt="Click Here to Pick up the date" onclick="displayDatePicker('delivery-date');">
				</div>
			</div>
			</td>
		</tr>
		<tr><td colspan="2"></td></tr>
		<tr>
			 <td><p>phone</P></td>
			<td><input type="text" name="phoneno" id="phoneno" value="{$phone}"></td>
			 <td><p>moblie<span class="mandatory">*</span></P></td>
			<td><input type="text" name="mobileno" id="mobileno" value="{$mobile}"></td>
		</tr>
		<tr><td colspan="2"></td></tr>
		<tr>
			 <td><p>city<span class="mandatory">*</span></p></td>
			 <td><input type="text" name="city" id="city" value="{$city}"></td>
			 <td><p>state<span class="mandatory">*</span></p></td>
			 <td>
			 <select name="state" id="state" class="select" >
				<option value="0" selected>--Select State--</option>
				{section name="statename" loop=$returnState}
					{if $returnState[statename].code == $statecode}
					<option value={$returnState[statename].code} selected>{$returnState[statename].state}</option>
					{else}
					<option value={$returnState[statename].code}>{$returnState[statename].state}</option>
					{/if}
				{/section}
 			 </select>
			
			</td>
		</tr>
		<tr><td colspan="2"></td></tr>
		<tr>
			<td><p>description</P></td>
			<td colspan="3"><textarea name="description" id="description" style="width:200px;height:50px;" >{$description}</textarea></td>
		</tr>
		
		<tr>
        <td colspan='4' align='right'>
            <input class='submit' type='button' name="saveaccountdetails" id="saveaccountdetails" value='save account details' onClick="javascript:corporatecontactdata('accountdetails',this.form);">&nbsp; 
        </td>
        </tr>
	</table>
		</div>

	<div class="links"> 

</form>
		
 	  <table width='95%' class='formtable' cellpadding='0' cellspacing='2' border='0' style='border: 1px solid #999999;' >
		<tr class="trhead">
			<td colspan="5"><p><strong>upload artwork/image&nbsp;
			<a href="javascript:staticPopupMessage('attachments');" style="font-size:1.0em;color:#FFFFFF;margin-left:390px;"><img  style="border: 0px none ;" src="./skin1/mkimages/help-icon.gif"/></a></strong></p></td>
		</tr>
		{if $smarty.get.upload == 'yes'}
		<tr class="trhead">
			<td colspan="5"><p><strong>{$smarty.get.f|base64_decode} uploaded successfully</strong></p></td>
		</tr>
		{/if}
		<tr>
			<td width='100%' colspan='4'>&nbsp;</td>
		</tr>  
		<tr>
			<td width='100%' colspan='4'> 
				<iframe name="iframe" src="modules/corporate/attachmentupload.php" style="width:540px;height:70px;border: 0px solid #FFFFFF;" frameborder='0' scrolling="no"> </iframe>
			</td>
		</tr>  
		<tr>
			<td width='100%' colspan='4'>&nbsp;</td>
		</tr>
		{section name=attach loop=$attachment_array}
			<tr>
				<td width='70%' colspan='2' style="background-color:#FFCD9B;height:15px;" >
				<div id="uploaded_file_path_{$smarty.section.attach.index}" style="color:#333333;font-size:1.0em;">
				<p><strong>{$attachment_array[attach]}</strong></p>
				</div>
				
				</td>
				<td width='30%' colspan='2' style="background-color:#FFCD9B;height:15px;" align="center">
				<div style="background-color:#FFCD9B;width:100px;color:#333333;font-size:1.0em;"><strong>
				<a href="javascript: void(0);" onClick="javascript: deleteUploadedFile({$smarty.section.attach.index});" style="margin-left">delete</a></strong>
				</div>
				</td>
			</tr> 
		{/section}
	 </table>
	  <div class="links">&nbsp;</div>
	  <div style='float:right;margin-right:15px;'>
		<input class='submit' type='button' name='sendrequest' value='send request for quote' onClick="return isDesignAddedForOrganization(document.request, 'requestconfirm', 'requestconfirm', '{$nodesign}')"> 
	  </div>
	  </div>
	  <div class="foot"></div>
	{/if}
</div>