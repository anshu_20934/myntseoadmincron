{literal}
<script type="text/javascript" language="JavaScript 1.2">

function sendMessageToAdmin(requestid)
{
    document.getElementById('sendmessgae').style.visibility = 'visible';
    document.getElementById('sendmessgae').style.display = '';
    document.getElementById('messagebox').style.visibility = 'visible';
    document.getElementById('messagebox').style.display = '';
    document.getElementById('requestid').value = requestid;
}

function hideSendMessgaeDiv(closediv)
{
    document.getElementById(closediv).style.visibility = 'hidden';
    document.getElementById(closediv).style.display = 'none';
    document.getElementById('messagebox').style.visibility = 'hidden';
    document.getElementById('messagebox').style.display = 'none';
}

function validateMessage(ajaxForm)
{	
	if(document.getElementById('sendmessagetext').value == "write your message here")
	{
		alert("Please input your message or close the popup");
		document.getElementById('sendmessagetext').focus();
		return false;
	}
	else
	{
		sendMessage(ajaxForm);
	}
}



function sendMessage(ajaxForm)
{
    var strSubmit       = '';
    var formElem;
    var strLastElemName = '';

    if(window.XMLHttpRequest) {
	req = new XMLHttpRequest(); 
     } else if (window.ActiveXObject) {
	req  = new ActiveXObject("Microsoft.XMLHTTP"); 
     }
	var SENDMESSAGEURL = "modules/corporate/sendmessage.php";

	for (i = 0; i < ajaxForm.elements.length; i++) 
	{
		formElem = ajaxForm.elements[i];
		switch (formElem.type) 
		{
		// Text, select, hidden, password, textarea elements
			case 'text':
			case 'hidden':
			case 'password':
			case 'textarea':

				strSubmit += formElem.name +
				'=' + escape(formElem.value) + '&'

			break;
		}

	}
	
	req.onreadystatechange = function() 
	{ 
		if(req.readyState == 4) 
		{ 
			  if(req.status == 200) 
			  { 
				document.getElementById('sendmessgae').style.visibility = 'visible';
				document.getElementById('sendmessgae').style.display = '';
				document.getElementById('messagebox').style.visibility = 'visible';
				document.getElementById('messagebox').style.display = '';
				document.getElementById('messageboxloader').style.visibility = 'hidden';
				document.getElementById('messageboxloader').style.display = 'none';
				
				var result = req.responseText.split(",");
				
				if(result[0] == 1)
				{
					document.getElementById('successmessgae').style.visibility = 'visible';
					document.getElementById('successmessgae').style.display = '';
					window.location = "modules/corporate/mkmanagerequest.php?requestid="+result[1];
				}
				else
				{
					alert("data can not be saved. please contact to administrator!!");
				}
			  }
		}
	 }

	 document.getElementById('messagebox').style.visibility = 'hidden';
	 document.getElementById('messagebox').style.display = 'none';
	 document.getElementById('messageboxloader').style.visibility = 'visible';
	 document.getElementById('messageboxloader').style.display = '';
	 req.open("POST", SENDMESSAGEURL, true); 
	 req.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	 req.send(strSubmit);
}
</script>

{/literal}

<div id="sendmessgae" style="visibility:hidden;display:none;">
<div id="overlay"> </div>
  <div id="selection">
   <div class="wrapper">
      <div class="head">
        <p><a href="javascript: hideSendMessgaeDiv('sendmessgae')"><img src="./skin1/mkimages/click-to-close.gif" border="0"></a>
	  send your message</p>
      </div>
      <div class="body" style="padding:10">
      <br>
      <p>
	<table style="width:540px;height:auto;border: 1px solid #999999;padding: 2px;">
	<tr>
		<td style="vertical-align: top;">
			<form method="post" name="messageform">
				<input type="hidden" name="requestid" id="requestid" value=""  />
				<input type="hidden" name="addmessage" id="modeid" value="addmessage"  />
				<table id="messagebox" style="visibility:hidden;display:none;width:540px;height:auto;border: 0px solid #999999;">
				<tr>
					<td style="visibility:hidden;display:none;font-size:1.0em;color:#FF3300;" align="left" colspan="2" id="successmessgae"><p>your message has been sent!</p></td>
				</tr>
				<tr>
					<td style="font-size:0.7em;color:#000000;vertical-align: top;" ><p><strong>message</strong></p></td>
					<td style="vertical-align: top;">
					<textarea name="sendmessagetext" id="sendmessagetext" onblur="if(this.value=='') this.value=this.defaultValue;" onfocus="if(this.value==this.defaultValue) this.value='';" style="color:#000000;width:458px;height:100px;">write your message here</textarea>
					</td>
				</tr>
				<tr>
					<td style="vertical-align: top;" align="right" colspan="2">
						<input type="button" class="submit" name="messagebutton" id="messagebutton" value="send message" onClick="javascript:validateMessage(this.form);">
					</td>
				</tr>
				</table>
			</form> 
			<table id="messageboxloader" border=1 style="visibility:hidden;display:none;width:540px;height:130px;border: 0px solid #999999;">
			<tr>
				<td ><p><img src='{$cdn_base}/images/image_loading.gif' style="margin-left:250px;border:0px;" border=0></p></td>
			</tr>
			</table>
		</td>
	</tr>
	<tr><td>&nbsp;</td></tr>
	</table>
      </p>
     </div>
      <div class="foot"></div>
    </div>
  </div>
  </div>