		{* $Id: coupons.tpl,v 1.36.2.1 2006/06/16 10:47:43 max Exp $ *}
	{include file="page_title.tpl" title=$lng.lbl_store_coupons_title}
	{include file="main/include_js.tpl" src="main/calendar2.js"}
	{include file="main/include_js.tpl" src="main/popup_product.js"}
	<script type="text/javascript" language="JavaScript 1.2">
		var http_loc = '{$http_location}';
	</script>
	<script type="text/javascript" src="{$http_location}/skin1/myntra_js/jquery.min.js"></script>
	{literal}
		<script type="text/javascript" language="JavaScript 1.2">
			var URL= "ajax_select_style.php?typeid=";
			function getHTTPObject() 
			{
			      var xmlhttp;
			      /*@cc_on
				  @if (@_jscript_version >= 5)
				    try {
				  xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
				    } catch (e) {
				  try {
				    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
				  } catch (E) {
				    xmlhttp = false;
				  }
				}
				  @else
				  xmlhttp = false;
				  @end @*/
				  if (!xmlhttp && typeof XMLHttpRequest != 'undefined') {
				    try {
				  xmlhttp = new XMLHttpRequest();
			    } catch (e) {
				  xmlhttp = false;
			    }
			  }
				  return xmlhttp;
			}
			var http = new getHTTPObject();
			//var request = createRequest();
			
			var added_pts = new Array();
			var added_excl_pts = new Array();
	
			// Array converter to object. Useful for in operations.
			function convertArray(arr)
			{
			  var o = {};
			  for(var i=0; i<arr.length; i++)
			  {
			    o[arr[i]]='';
			  }
			  return o;
			}
			
			function getproductstyles(){
				 request = createRequest();
				 var url = http_loc + "/admin/register_sku.php";
				 var pr_id = document.getElementById("ptype").value;
	
				 //alert(added_pts);
				 if (pr_id in convertArray(added_pts)) {
					 return;
				 }
				 added_pts.push(pr_id);
	
				 var poststr = "pr_id=" + encodeURI(pr_id);
				 //alert(poststr);
				 sendRequest(request,url,poststr,show_styles);
			}
			function getexclproductstyles() {
			     request = createRequest();
				 var url = http_loc + "/admin/register_sku.php";
				 var pr_id = document.getElementById("exclptype").value;
	
				 //alert(added_pts);
				 if (pr_id in convertArray(added_excl_pts)) {
					 return;
				 }
				 added_excl_pts.push(pr_id);
	
				 var poststr = "pr_id=" + encodeURI(pr_id);
				 //alert(poststr);
				 sendRequest(request,url,poststr,show_excl_styles);
			}
			function show_styles() {
				 if (request.readyState == 4) {
					if (request.status == 200) {
						var response = request.responseText;
						document.getElementById("styleid").innerHTML += response;
					}
					else {
						alert("Error! Request status is " + request.status);
					}
				}
			}
			function show_excl_styles() {
				 if (request.readyState == 4) {
					if (request.status == 200) {
						var response = request.responseText;
						document.getElementById("exclstyleid").innerHTML += response;
					}
					else {
						alert("Error! Request status is " + request.status);
					}
				}
			}
			
			function sendRequest(request,url,parameters,func_name) {
			  request.onreadystatechange = func_name;
			  request.open("POST", url, true);
			  request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			  request.setRequestHeader("Content-length", parameters.length);
			  request.setRequestHeader("Connection", "close");
			  request.send(parameters);
			}
			function createRequest() {
			  var request = null;
			  try {
				request = new XMLHttpRequest();
			  } catch (trymicrosoft) {
				try {
				  request = new ActiveXObject("Msxml2.XMLHTTP");
				} catch (othermicrosoft) {
				  try {
					request = new ActiveXObject("Microsoft.XMLHTTP");
				  } catch (failed) {
					request = null;
				  }
				}
			  }
	
			  if (request == null) {
				alert("Error creating request object!");
			  } else {
				return request;
			  }
			}
	
			function selectStyle()
			{
				var id = document.getElementById("ptype").value;
				 http.open("GET", URL + escape(id), true);
				 http.onreadystatechange = myGetFunction;
				 http.send(null); 
			}
			function myGetFunction()
			{
				if (http.readyState == 4)
				{
					   var result=http.responseText.split(","); 
					  
					   if(result.length == 0)
					   {
						alert("There is not items in this type.");
					   } 
				   
						var len = document.getElementById("pstyle").options.length;
					  if(len >= 1)
					 {   
					    for(i=len-1; i >= 1 ; i--)
					    {
					       document.getElementById("pstyle").options[i] = null;
					    }
				         }
					    for(i=1; i <result.length; i++)
					    { 
					       textValue = result[i].split("||");
					       
					    document.getElementById("pstyle").options[i] = new Option(textValue[1],textValue[0]);
	
					     }
				   }
			}
	
			function couponDisplay()
			{
				if(document.getElementById("ctype").value == "S")
				{
					document.getElementById("couponInfo").style.visibility = "visible";
					document.getElementById("couponInfo").style.display = "";
					document.getElementById("groupDropdown").style.visibility = "visible";
					document.getElementById("groupDropdown").style.display = "";
					document.getElementById("group").style.visibility = "hidden";
					document.getElementById("group").style.display = "none";
					document.getElementById("groupname").style.visibility = "hidden";
					document.getElementById("groupname").style.display = "none";
					document.getElementById("channelRow").style.visibility = "hidden";
					document.getElementById("channelRow").style.display = "none";
					document.getElementById("templateRow").style.visibility = "hidden";
					document.getElementById("templateRow").style.display = "none";
				}
				if(document.getElementById("ctype").value == "G")
				{
					document.getElementById("group").style.visibility = "visible";
					document.getElementById("group").style.display = "";
					document.getElementById("groupname").style.visibility = "visible";
					document.getElementById("groupname").style.display = "";
					document.getElementById("channelRow").style.visibility = "visible";
					document.getElementById("channelRow").style.display = "";
					document.getElementById("templateRow").style.visibility = "visible";
					document.getElementById("templateRow").style.display = "";
					document.getElementById("couponInfo").style.visibility = "hidden";
					document.getElementById("couponInfo").style.display = "none";
					document.getElementById("groupDropdown").style.visibility = "hidden";
					document.getElementById("groupDropdown").style.display = "none";
				}
			}
	
			function typeDisplay()
			{
				if (document.getElementById("couponTypeNew").value == "percentage" 
				|| document.getElementById("couponTypeNew").value == "max_percentage") {
				if(  document.getElementById("MRPpercentageRow") !== null
						&&   document.getElementById("MRPAmountRow") !== null){
					document.getElementById("MRPpercentageRow").style.visibility = "visible";
					document.getElementById("MRPpercentageRow").style.display = "";
					document.getElementById("MRPAmountRow").style.visibility = "hidden";
					document.getElementById("MRPAmountRow").style.display = "none";
					}
				}
				if (document.getElementById("couponTypeNew").value == "absolute") {
					if(  document.getElementById("MRPpercentageRow") !== null
						&&   document.getElementById("MRPAmountRow") !== null){
						document.getElementById("MRPpercentageRow").style.visibility = "hidden";
						document.getElementById("MRPpercentageRow").style.display = "none";
						document.getElementById("MRPAmountRow").style.visibility = "visible";
						document.getElementById("MRPAmountRow").style.display = "";
						document.getElementById("MRPAmountRow").getElementsByClassName("FormButton")[0].innerHTML="Discount amount:";	
					}	
					
					if(  document.getElementById("MRPpercentageNew") !== null
						&&   document.getElementById("MRPAmountNew") !== null){
						document.getElementById("MRPpercentageNew").style.visibility = "hidden";
						document.getElementById("MRPpercentageNew").style.display = "none";
						document.getElementById("MRPAmountNew").style.visibility = "visible";
						document.getElementById("MRPAmountNew").style.display = "";
						document.getElementById("MRPAmountNew").getElementsByClassName("FormButton")[0].innerHTML="Discount amount:";
					}
						
				}
				if (document.getElementById("couponTypeNew").value == "dual" ) {
					
					if( document.getElementById("MRPpercentageRow") !== null
						&&  document.getElementById("MRPAmountRow") !== null){
					document.getElementById("MRPpercentageRow").style.visibility = "visible";
					document.getElementById("MRPpercentageRow").style.display = "";
					document.getElementById("MRPAmountRow").style.visibility = "visible";
					document.getElementById("MRPAmountRow").style.display = "";
					document.getElementById("MRPAmountRow").getElementsByClassName("FormButton")[0].innerHTML="Discount amount:";
					}
					
					if(document.getElementById("MRPpercentageNew") !== null
						&&  document.getElementById("MRPAmountNew") !== null){
					document.getElementById("MRPpercentageNew").style.visibility = "visible";
					document.getElementById("MRPpercentageNew").style.display = "";
					document.getElementById("MRPAmountNew").style.visibility = "visible";
					document.getElementById("MRPAmountNew").style.display = "";
					document.getElementById("MRPAmountNew").getElementsByClassName("FormButton")[0].innerHTML="Discount amount:";
					}
					
				}
				
				if (document.getElementById("couponTypeNew").value == "max_percentage_dual" ) {
					if( document.getElementById("MRPpercentageRow") !== null
						&&  document.getElementById("MRPAmountRow") !== null){
					document.getElementById("MRPpercentageRow").style.visibility = "visible";
					document.getElementById("MRPpercentageRow").style.display = "";
					document.getElementById("MRPAmountRow").style.visibility = "visible";
					document.getElementById("MRPAmountRow").style.display = "";
					document.getElementById("MRPAmountRow").getElementsByClassName("FormButton")[0].innerHTML="Percentage Discount Limit:";
					}
					
					if(  document.getElementById("MRPpercentageNew") !== null
						&&   document.getElementById("MRPAmountNew") !== null){
					document.getElementById("MRPpercentageNew").style.visibility = "visible";
					document.getElementById("MRPpercentageNew").style.display = "";
					document.getElementById("MRPAmountNew").style.visibility = "visible";
					document.getElementById("MRPAmountNew").style.display = "";
					document.getElementById("MRPAmountNew").getElementsByClassName("FormButton")[0].innerHTML="Percentage Discount Limit:";
					}
					
				}
				
			}
	
			function generationMode()
			{
				if (document.getElementById("couponMode").value == "Single") {
					document.getElementById("multipleCouponTable").style.visibility = "hidden";
					document.getElementById("multipleCouponTable").style.display = "none";
					document.getElementById("couponCodeRow").style.visibility = "visible";
					document.getElementById("couponCodeRow").style.display = "";
				}
				if (document.getElementById("couponMode").value == "Multiple") {
					document.getElementById("couponCodeRow").style.visibility = "hidden";
					document.getElementById("couponCodeRow").style.display = "none";
					document.getElementById("multipleCouponTable").style.visibility = "visible";
					document.getElementById("multipleCouponTable").style.display = "";
				}
			}
	
			function dateChanged()
			{
				var validity = document.getElementById("validity").value;
				if (validity == 0)
					return;
	
				var startDate = new Date(document.getElementById("startDate").value);
				var endDate = new Date(startDate.valueOf() + validity * 24 * 60 * 60 * 1000);
				var month = endDate.getMonth() + 1;
				if (month < 10) {
					month = "0" + month;
				}
				var date = endDate.getDate();
				if (date < 10) {
					date = "0" + date;
				}
	
				document.getElementById("endDate").value = month  + "/" + date + "/" + endDate.getFullYear();
			}
	
			function validateCoupon()
			{
				if(document.getElementById('creatorNew').value == "")
				{
					alert("Please YOUR name.");
					document.getElementById('creatorNew').focus();
					return false;
				}
				
				var MRPAmount = document.getElementById('MRPAmountNew').value*1;
				var MRPpercentage = document.getElementById('MRPpercentageNew').value*1;
				var type = document.getElementById('couponTypeNew').value;
			
				if(document.getElementById('addtogroup').value == '')
				{
					var pattern = /^[a-zA-Z0-9]+$/g;
					var string = document.getElementById('groupIdNew').value;
					if (string == "" || !pattern.test(string))
					{
						alert("Please enter a valid alphanumeric group name.");
						document.getElementById('groupIdNew').focus();
						return false;
					}
				}
				else {
	
				if (document.getElementById('couponMode').value == "Single") {
					var pattern = /^[a-zA-Z0-9]+$/g;
					var string = document.getElementById('couponCodeNew').value;
					if (string == "" || !pattern.test(string)) {
						alert("Please enter a valid alphanumeric Coupon Name.");
						document.getElementById('couponCodeNew').focus();
						return false;
					}
				}
				if (document.getElementById('couponMode').value == "Multiple") {
					var pattern = /^[a-zA-Z0-9]+$/g;
					var string = document.getElementById('couponPrefix').value;
					if (string == "" || !pattern.test(string)) {
						alert("Please enter a valid alphanumeric Coupon Prefix.");
						document.getElementById('couponPrefix').focus();
						return false;
					}
	
					/*pattern = /^[0-9]+$/g;
					string = document.getElementById('suffixLength').value;
					if (string == "" || !pattern.test(string)) {
						alert("Please enter a valid numeric suffix length.");
						document.getElementById('suffixLength').focus();
						return false;
					}
	
					pattern = /^[0-9]+$/g;
					string = document.getElementById('count').value;
					if (string == "" || !pattern.test(string)) {
						alert("Please enter a valid numeric coupon count.");
						document.getElementById('count').focus();
						return false;
					}*/
				}
				
				if (document.getElementById('groupId').value == "") {
					alert("Please select a group.");
					document.getElementById('groupDropdown').focus();
					return false;
				}
				if(document.getElementById('startDate').value == "")
				{
					alert("Please enter valid start date.");
					document.getElementById('startDate').focus();
					return false;
				}
				if(document.getElementById('endDate').value == "")
				{
					alert("Please enter valid end date.");
					document.getElementById('endDate').focus();
					return false;
				}
				
				
				var ed = (document.getElementById('endDate').value).split("/") ;
	            var sd = (document.getElementById('startDate').value).split("/") ;
	                        
	            if (Date.parse(ed[2]+"-"+ed[0]+"-"+ed[1]) < Date.parse(sd[2]+"-"+sd[0]+"-"+sd[1])) {
	
					alert("Coupon start date should be before its end date!");
					document.getElementById('startDate').focus();
					return false;
				}
	
				if (document.getElementById('commentsNew').value == "") {
					alert("Please enter a comment.");
					document.getElementById('commentsNew').focus();
					return false;
				}
	
				if (document.getElementById('descriptionNew').value == "") {
					alert("Please enter a description.");
					document.getElementById('descriptionNew').focus();
					return false;
				}
	
				if(document.getElementById('maxUsagePerCartNew').value == "" || document.getElementById('maxUsagePerCartNew').value == 0)
				{
					alert("Please enter non-zero value for Max Usage Per Cart.");
					document.getElementById('maxUsagePerCartNew').focus();
					return false;
				}
				if(document.getElementById('maxUsagePerCart').value == "" || document.getElementById('maxUsagePerCart').value == 0)
				{
					alert("Please enter non-zero value for Max Usage Per Cart.");
					document.getElementById('maxUsagePerCart').focus();
					return false;
				}
				
				if(MRPAmount < 0 || MRPpercentage < 0)
				{
					alert("Discount can not be zero or less than zero.");
					document.getElementById('MRPAmount').focus();
					return false;
				}
				
				if(MRPAmount > 1000 && (type != "percentage"))
				{
					if (confirm("Do you really want to offer a discount of " +MRPAmount+ " Rs")){
					return true;
					}
					else {
					return false;
					}
				}
				if(MRPpercentage > 100 && type != "absolute")
				{
					alert("Discount can not be more than 100 %");
					document.getElementById('MRPpercentageNew').focus();
					return false;
				}
				if(MRPpercentage == 100 && type != "absolute")
				{
					if (confirm("Do you really want to give the Products for free")){
					return true;
					}
					else {
					return false;
					}
				}
				if(MRPpercentage == 0 && type != "absolute")
				{
					alert("Please enter a non-zero discount!");
					document.getElementById('MRPpercentageNew').focus();
					return false;
				}
				
				if(MRPAmount == 0 && type != "max_percentage_dual")
				{
					alert("Please enter a non-zero discount percentage limit !");
					document.getElementById('MRPAmountNew').focus();
					return false;
				}
				
				if(MRPAmount == 0 && type != "percentage")
				{
					alert("Please enter a non-zero discount!");
					document.getElementById('MRPAmountNew').focus();
					return false;
				}
				else
				{
					return true;
				}
				
				}
			}
	
			$(document).ready(
				function()
				{
                    showBinRange();
					if(document.getElementById("addtogroup").value != "")
					{
						try{
						document.getElementById("couponInfo").style.visibility = "visible";
						document.getElementById("couponInfo").style.display = "";
						document.getElementById("groupDropdown").style.visibility = "visible";
						document.getElementById("groupDropdown").style.display = "";
						document.getElementById("group").style.visibility = "hidden";
						document.getElementById("group").style.display = "none";
						document.getElementById("groupname").style.visibility = "hidden";
						document.getElementById("groupname").style.display = "none";
						document.getElementById("channelRow").style.visibility = "hidden";
						document.getElementById("channelRow").style.display = "none";
						document.getElementById("templateRow").style.visibility = "hidden";
						document.getElementById("templateRow").style.display = "none";
						}catch(err){
						 // nothing
						}
					}
					else
					{
						try{
						document.getElementById("group").style.visibility = "visible";
						document.getElementById("group").style.display = "";
						document.getElementById("groupname").style.visibility = "visible";
						document.getElementById("groupname").style.display = "";
						document.getElementById("channelRow").style.visibility = "visible";
						document.getElementById("channelRow").style.display = "";
						document.getElementById("templateRow").style.visibility = "visible";
						document.getElementById("templateRow").style.display = "";
						document.getElementById("couponInfo").style.visibility = "hidden";
						document.getElementById("couponInfo").style.display = "none";
						document.getElementById("groupDropdown").style.visibility = "hidden";
						document.getElementById("groupDropdown").style.display = "none";
						}catch(err){
						 // nothing
						}
					}
	
					typeDisplay();
				}
			);

            function showBinRange(){
                if($("#groupType").val() == "pg"){
                    $("#binRangeRow").show();
                }
                else{
                    $("#binRangeRow").hide();
                }

                if($("#groupTypeNew").val() == "pg"){
                    $("#binRangeNewRow").show();
                }
                else{
                    $("#binRangeNewRow").hide();
                }
            }
		</script>
	{/literal}
	
	
	<script type="text/javascript" language="JavaScript 1.2">
	
	function changeBox(id, status) {ldelim}
		var elm = document.getElementById("box"+id);
		if (!elm)
			return false;
		
		elm.style.display = (status == true) ? "" : "none";
	{rdelim}
	
	</script>
	
	
	
	{capture name=dialog}
	
	
	<form action="coupons.php" method="post" name="coupon_form" enctype="multipart/form-data">
	<input type="hidden" name="mode" value="add" />
	<input type="hidden" name="groupid" value="{$groupid}" />
	<input type="hidden" name="coupon_old" value="{$coupon.coupon}" />
	<input type="hidden" name="addtogroup" id="addtogroup" value="{$addtogroup}" />
	<input type="hidden" name="ctype" id="ctype" {if $addtogroup eq ""}value="G"{else}value="S"{/if} />
	<input type="hidden" name="validity" id="validity" {if $coupon_data.validity eq null}value="0"{else}value="{$coupon_data.validity}"{/if} />
	
	<table cellpadding="3" cellspacing="1">
	{if $mode ne 'modifysingle'}
	{if $mode ne 'modify'}
	
	</table>
	
	
	<table cellpadding="3" cellspacing="1">
	
	<tr><td width="10" colspan="3" height=20>&nbsp;</td></tr>
	
	<tr id="group"><td width="10" colspan="3" height=20>
	<font class="FormButton">Coupon Group Details:</font>
	<hr />
	</td></tr>
	
	<tr style="visibility:visible;">
		<td colspan="3">
			<table cellpadding="3" cellspacing="1" border=0 bordercolor="#660099" width="100%">
				<tr>
					<td class="FormButton" width="50%">Group Name:</td>
					<td><font class="Star">*</font></td>
					<td id="groupname" width="100%"><input type="text" size="25" maxlength="16" name="groupIdNew" id="groupIdNew" value="{$smarty.post.groupIdNew}" /></td>
					<td id="groupDropdown" style="visibility:hidden;display:none">
						<select id="groupId" name="groupId" {if $addtogroup neq ""}disabled="true"{/if}>
							{foreach from=$groups key=id item=name}
								<option value="{$name}" {if $name eq $addtogroup}selected="true"{/if}>{$name}</option>
			    			{/foreach}
						</select>
					</td>
				</tr>

				<tr>
					<td class="FormButton" width="50%">Applicable Device:</td>
					<td><font class="Star"></font></td>
					<td id="devicename" width="100%"><input type="text" size="25" maxlength="100" name="applicabelDeviceNew" id="applicabelDeviceNew" value="{$smarty.post.appicableDeviceIdNew}" /></td>
					<td id="deviceDropdown" style="visibility:hidden;display:none">
					</td>
				</tr>


                <tr id="groupTypeRow">
                    <td class="FormButton" width="50%">Group Type:</td>
                    <td><font class="Star">*</font></td>
                    <td>
                        <select name="groupTypeNew" id="groupTypeNew" onchange="showBinRange();">
                            <option value="">Default</option>
                            <option value="pg">Payment Gateway Discount</option>
                            <option value="deviceTag">Device ID Coupon</option>
                            <option value="ds_driven">Intelligence Driven</option>
                        </select>
                    </td>
                </tr>

                <tr id="binRangeNewRow">
                    <td class="FormButton" width="50%">Bin Range:</td>
                    <td><font class="Star"></font></td>
                    <td>
                        <textarea cols="50" rows="5" name="binRangeNew" id="binRangeNew"></textarea>
                    </td>
                </tr>

				
				<tr id="channelRow">
					<td class="FormButton" width="50%">Channel:</td>
					<td><font class="Star">*</font></td>
					<td>
						<select name="channelNew" id="channelNew">
							{foreach from=$channels key=id item=name}
								<option value="{$name}">{$name}</option>
			    			{/foreach}
						</select>
					</td>
				</tr>
				
				<tr id="templateRow">
					<td class="FormButton" width="50%">Template:</td>
					<td><font class="Star">*</font></td>
					<td>
						<select name="templateNew" id="templateNew">
							<option value="">None</value>
							{foreach from=$templates key=id item=name}
								<option value="{$name}">{$name}</option>
			    			{/foreach}
						</select>
					</td>
				</tr>
				
				<tr>
					<td class="FormButton">Created/Modified By:</td>
					<td><font class="Star">*</font></td>
					<td><input readonly id="creatorNew" type="text" size="25" name="creatorNew" value="{$login}" /></td>
				</tr>
				
			</table>
		</td>
	</tr>
	
	{else}
	<!-- edit the details -->
	<tr>
		<td class="FormButton" width="20%">Group Name:</td>
		<td width="10">&nbsp;</td>
		<td width="100%"><input readonly type="text" size="25" maxlength="16" name="groupIdNew" id="groupIdNew" value="{$group_data.groupname}" /></td>
	</tr>

	<tr>
		<td class="FormButton" width="20%">Applicable Device:</td>
		<td width="10">&nbsp;</td>
		<td width="100%"><input type="text" size="25" maxlength="100" name="applicabelDeviceNew" id="applicabelDeviceNew" value="{$group_data.applicableDevice}" /></td>
	</tr>


    <tr id="groupTypeRow">
        <td class="FormButton" width="50%">Group Type:</td>
        <td><font class="Star">*</font></td>
        <td>
            <select name="groupType" id="groupType" onchange="showBinRange();">
                <option value="">Default</option>
                <option value="pg" {if $group_data.groupType eq 'pg'} selected="selected"{/if}>Payment Gateway Discount</option>
                <option value="deviceTag" {if $group_data.groupType eq 'deviceTag'} selected="selected"{/if}>Device ID Coupon</option>
                <option value="ds_driven" {if $group_data.groupType eq 'ds_driven'} selected="selected"{/if}>Intelligence Driven</option>
            </select>
        </td>
    </tr>

    <tr id="binRangeRow">
        <td class="FormButton" width="50%">Bin Range:</td>
        <td><font class="Star">*</font></td>
        <td>
            <textarea cols="50" rows="5" name="binRange" id="binRange">{$bin_data}</textarea>
        </td>
    </tr>


        <tr>
		<td class="FormButton" width="20%">Channel:</td>
		<td width="10">&nbsp;</td>
		<td>
			<select name="channelNew" id="channelNew">
				{foreach from=$channels key=id item=name}
					<option value="{$name}" {if $group_data.channel eq $name} selected="selected"{/if}>{$name}</option>
			    {/foreach}
			</select>
		</td>
	</tr>
	
	<tr>
		<td class="FormButton" width="20%">Template:</td>
		<td width="10">&nbsp;</td>
		<td>
			<select name="templateNew" id="templateNew">
				<option value="" {if $group_data.templateName eq ""} selected="selected"{/if}>None</value>
				{foreach from=$templates key=id item=name}
					<option value="{$name}" {if $group_data.templateName eq $name} selected="selected"{/if}>{$name}</option>
			    {/foreach}
			</select>
		</td>
	</tr>
	
	<tr>
		<td class="FormButton" width="20%">Status:</td>
		<td width="10">&nbsp;</td>
		<td>
			<select name="isGroupActiveNew" id="isGroupActiveNew">
				<option value="A"{if $group_data.active eq "A"} selected="selected"{/if}>{$lng.lbl_coupon_active}</option>
				<option value="D"{if $group_data.active eq "D"} selected="selected"{/if}>{$lng.lbl_coupon_disabled}</option>
				<option value="U"{if $group_data.active eq "U"} selected="selected"{/if}>{$lng.lbl_coupon_used}</option>
			</select>
		</td>
	</tr>
	
	<tr>
		<td colspan="3"><br />
		<input type="submit" value="{$lng.lbl_update|escape} Group"
			onClick="javascript:document.coupon_form.mode.value='modify';document.coupon_form.submit();" />
		</td>
	</tr>
	
	<!-- end -->
	{/if}
	
	</table>
	<table id="couponInfo" cellpadding="3" cellspacing="1" style="visibility:hidden;display:none">
	
	{if $mode ne 'modify'}
	<tr><td width="10" colspan="3" height=20>&nbsp;</td></tr>
	<tr><td width="10" colspan="3" height=20>
	<font class="FormButton">Coupon Details:</font>
	<hr />
	</td></tr>
	{/if}
	<tr>
		<td colspan="3">
		{if $mode ne 'modify'}
			<table cellpadding="3" cellspacing="1" border=0 bordercolor="#660099" width="100%">
			
				<tr>
					<td class="FormButton">Coupon Generation Mode:</td>
					<td><font class="Star">*</font></td>
					<td><select id="couponMode" name="couponMode" onChange="javascript:generationMode();">
							<option value="Single" selected="selected">Single</option>
							<option value="Multiple">Multiple</option>
						</select>
					</td>
				</tr>
				
				<tr id="couponCodeRow">
					<td class="FormButton">Coupon Name:</td>
					<td><font class="Star">*</font></td>
					<td><input type="text" size="25" name="couponCodeNew" id="couponCodeNew" value="{if $coupon_data.couponCodeNew}{$coupon_data.couponCodeNew}{/if}" /></td>
				</tr>
				
				<tr id="multipleCouponTable" style="visibility:hidden;display:none">
				<td colspan="3">
				<table>
					<tr>
						<td class="FormButton">Coupon Code Prefix:</td>
						<td><font class="Star">*</font></td>
						<td><input type="text" size="25" id="couponPrefix" name="couponPrefix" /></td>
					</tr>
					
					<tr>
						<td class="FormButton">Coupon Code Suffix Length:</td>
						<td><font class="Star">*</font></td>
						<td><input type="text" size="25" id="suffixLength" name="suffixLength" /></td>
					</tr>
					
					<tr>
						<td class="FormButton">Number of coupons:</td>
						<td><font class="Star">*</font></td>
						<td><input type="text" size="25" id="count" name="count" /></td>
					</tr>
				</table>
				</td>
				</tr>
	
				<tr>
					<td class="FormButton" width="20%">Start Date:</td>
					<td width="10"><font class="Star">*</font></td>
					<td width="100%"><input type="text" id='startDate' name='startDate'
						value="" size="15" onChange="javascript:dateChanged();" /> <a href="javascript:cal4.popup();"><img
						src="../images/cal.gif" width="16" height="16" border="0"
						alt="Click Here to Pick up the date"></a></td>
				</tr>
				<tr>
					<td class="FormButton" width="20%">End Date:</td>
					<td width="10"><font class="Star">*</font></td>
					<td width="100%"><input type="text" id='endDate' name='endDate'
						value="" size="15" /> <a href="javascript:cal5.popup();"><img
						src="../images/cal.gif" width="16" height="16" border="0"
						alt="Click Here to Pick up the date"></a></td>
				</tr>
	
				<tr>
					<td class="FormButton" width="20%">Show in My Myntra:</td>
					<td width="10">&nbsp;</td>
					<td><input id="showInMyMyntra" type="checkbox" name="showInMyMyntra" {if $coupon_data.showInMyMyntra} checked="checked"{/if} />Show in My Myntra</td>
				</tr>
				
				<tr>
					<td class="FormButton" width="20%">Shipping Option:</td>
					<td width="10">&nbsp;</td>
					<td><input id="freeShipping" type="checkbox" name="freeShipping" {if $coupon_data.freeShipping} checked="checked"{/if} />Free Shipping</td>
				</tr>
				
				<tr>
					<td class="FormButton">{$lng.lbl_coupon_type}:</td>
					<td>&nbsp;</td>
					<td>
					<!--  <select name="couponTypeNew" id="couponTypeNew" onchange="javascript: if (this.value == 'absolute') changeBox('1', true); else changeBox('1', false); if (this.value == 'free_ship') changeBox('4', false); else changeBox('4', true);"> -->
					<select name="couponTypeNew" id="couponTypeNew" onchange="javascript:typeDisplay();">
					<option value="percentage"{if $coupon_data.couponTypeNew eq "percentage"} selected="selected"{/if}>{$lng.lbl_coupon_type_percent}</option>
					<option value="absolute"{if $coupon_data.couponTypeNew eq "absolute"} selected="selected"{/if}>Upto {$config.General.currency_symbol} {$lng.lbl_coupon_type_absolute}</option>
					<option value="dual"{if $coupon_data.couponTypeNew eq "dual"} selected="selected"{/if}>% off upto {$config.General.currency_symbol}</option>
					<option value="max_percentage"{if $coupon_data.couponTypeNew eq "max_percentage"} selected="selected"{/if}>upto max {$lng.lbl_coupon_type_percent}</option>
					<option value="max_percentage_dual"{if $coupon_data.couponTypeNew eq "max_percentage_dual"} selected="selected"{/if}> {$lng.lbl_coupon_type_percent} upto maximum {$lng.lbl_coupon_type_percent}</option>
				
					</select>
					</td>
				</tr>
				
				<tr id="MRPpercentageRow">
					<td class="FormButton">Percentage discount:</td>
					<td><font class="Star">*</font></td>
					<td><input type="text" size="25" name="MRPpercentageNew" id="MRPpercentageNew" value="{if $coupon_data.MRPpercentageNew}{$coupon_data.MRPpercentageNew|formatprice}{else}{$zero}{/if}" /></td>
				</tr>
				
				<tr id="MRPAmountRow" style="visibility:hidden;display:none">
					<td class="FormButton">Discount Amount:</td>
					<td><font class="Star">*</font></td>
					<td><input type="text" size="25" name="MRPAmountNew" id="MRPAmountNew" value="{if $coupon_data.MRPAmountNew}{$coupon_data.MRPAmountNew|formatprice}{else}{$zero}{/if}" /></td>
				</tr>
	
				<tr>
					<td class="FormButton">Max Usage:</td>
					<td>&nbsp;</td>
					<td>
						<table cellpadding="1" cellspacing="1">
						<tr>
							<td><input id="maxUsageNew" type="text" size="8" name="maxUsageNew" value="{$coupon_data.maxUsageNew|default:"1"}" /></td>
							<td>&nbsp;</td>
							<td><input id="isInfiniteNew" type="checkbox" name="isInfiniteNew" {if $coupon_data.isInfinite} checked="checked"{/if} /></td>
							<td>Infinite Usage</td>
						</tr>
						</table>
					</td>
				</tr>
				
				<tr>
					<td class="FormButton">Max. Usage Per Cart:</td>
					<td>&nbsp;</td>
					<td><input type="text" size="25" name="maxUsagePerCartNew" id="maxUsagePerCartNew" value="{$coupon_data.maxUsagePerCartNew|default:"-1"}" /></td>
				</tr>
				
				<tr>
					<td class="FormButton">Max Usage Per User:</td>
					<td>&nbsp;</td>
					<td>
						<table cellpadding="1" cellspacing="1">
						<tr>
							<td><input id="maxUsageByUserNew" type="text" size="8" name="maxUsageByUserNew" value="{$coupon_data.maxUsageByUserNew|default:"1"}" /></td>
							<td>&nbsp;</td>
							<td><input id="isInfinitePerUserNew" type="checkbox" name="isInfinitePerUserNew"{if $coupon_data.isInfinitePerUser} checked="checked"{/if} /></td>
							<td>Infinite Usage Per User</td>
						</tr>
						</table>
					</td>
				</tr>
				
				<tr>
					<td class="FormButton">Minimum Subtotal:</td>
					<td>&nbsp;</td>
					<td>
						<table cellpadding="1" cellspacing="1">
						<tr>
							<td>{$config.General.currency_symbol}</td>
							<td><input id="minAmountNew" type="text" size="25" name="minAmountNew" value="{if $coupon_data.minSubtotal}{$coupon_data.minSubtotal|formatprice}{else}{$zero}{/if}" /></td>
						</tr>
						</table>
					</td>
				</tr>
				
				<tr>
					<td class="FormButton">Maximum Subtotal:</td>
					<td>&nbsp;</td>
					<td>
						<table cellpadding="1" cellspacing="1">
						<tr>
							<td>{$config.General.currency_symbol}</td>
							<td><input id="maxAmountNew" type="text" size="25" name="maxAmountNew" value="{if $coupon_data.maxSubtotal}{$coupon_data.maxSubtotal|formatprice}{else}{$zero}{/if}" /></td>
						</tr>
						</table>
					</td>
				</tr>
				
				<tr>
					<td class="FormButton">Minimum item Count:</td>
					<td>&nbsp;</td>
					<td>
						<table cellpadding="1" cellspacing="1">
						<tr>
							
							<td><input id="minCountNew" type="text" size="25" name="minCountNew" value="{if $coupon_data.minCount}{$coupon_data.minCount}{else}{$zero}{/if}" /></td>
						</tr>
						</table>
					</td>
				</tr>
				
				<tr>
					<td class="FormButton">Maximum item Count:</td>
					<td>&nbsp;</td>
					<td>
						<table cellpadding="1" cellspacing="1">
						<tr>
							
							<td><input id="maxCountNew" type="text" size="25" name="maxCountNew" value="{if $coupon_data.maxCount}{$coupon_data.maxCount}{else}{$zero}{/if}" /></td>
						</tr>
						</table>
					</td>
				</tr>
	 
				
	
				<tr>
					<td class="FormButton">{$lng.lbl_status}:</td>
					<td>&nbsp;</td>
					<td>
						<select id="isActiveNew" name="isActiveNew">
							<option value="A"{if $coupon_data.isActive eq "A"} selected="selected"{/if}>{$lng.lbl_coupon_active}</option>
							<option value="D"{if $coupon_data.isActive eq "D"} selected="selected"{/if}>{$lng.lbl_coupon_disabled}</option>
						</select>
					</td>
				</tr>
				
				<tr>
				
					<td class="FormButton">
						UserIDs list in CSV format : 
					</td><td>&nbsp;</td>
					<td>				
					<input type="file" name="tagging_file" id="tagging_file"/>                     
					</td>
				</tr>
				
				<tr>
					<td class="FormButton">Allowed Users (2048 char):</td>
					<td>&nbsp;</td>
					<td><textarea id="usersNew" name="usersNew" rows="2" cols="100">{if $coupon_data.users}{$coupon_data.users}{else}{""}{/if}</textarea></td>
				</tr>
				
				<tr>
					<td class="FormButton">Excluded Users (2048 char):</td>
					<td>&nbsp;</td>
					<td><textarea id="excludeUsersNew" name="excludeUsersNew" rows="2" cols="100">{if $coupon_data.excludeUsers}{$coupon_data.excludeUsers}{else}{""}{/if}</textarea></td>
				</tr>
				
				<tr>
					<td class="FormButton">Comments:</td>
					<td><font class="Star">*</font></td>
					<td><textarea id="commentsNew" name="commentsNew" rows="2" cols="100">{if $coupon_data.comments}{$coupon_data.comments}{else}{""}{/if}</textarea></td>
				</tr>
				
				<tr>
					<td class="FormButton">Description:</td>
					<td><font class="Star">*</font></td>
					<td><textarea id="descriptionNew" name="descriptionNew" rows="2" cols="100">{if $coupon_data.description}{$coupon_data.description}{else}{""}{/if}</textarea></td>
				</tr>
	
				<tr>
					<td class="FormButton" valign="top">Apply coupon on:</td>
					<td>&nbsp;</td>
					<td>
	
					<table cellpadding="1" cellspacing="1">
						
						<tr>
							<td>Select Product Types...<br />
							<table cellpadding="1" cellspacing="1">
								<tr>
									<td>
									<!-- <select name="ProductType[]" MULTIPLE id="ProductType" style="height:50px;">
										{if isset($smarty.post.ptype)} {html_options options=$prod_type
										selected=$smarty.post.ptype} {else} {html_options
										options=$prod_type } {/if}
									</select> --> 
									<select name='ProductType[]' MULTIPLE id="ProductType" style="height:100px;"> 
										{foreach from=$producttypes key=group item=types}
											<optgroup label="{$group}">
												{foreach from=$types item=type}
													<option value="{$type.id}"
														{foreach from=$coupon_data.ptids item=ptid}
															{if $ptid eq $type.id}
																selected="selected"
															{/if}
														{/foreach}
													>{$type.name}</option>
												{/foreach}
										{/foreach}
									</select>
									</td>
								</tr>
							</table>
							</td>
						</tr>
						
						<tr><td>&nbsp;</td></tr>
						
						<tr>
							<td>Select Product Styles belonging to Product Types...<br />
							<table cellpadding="1" cellspacing="1">
								<tr>
									<td>Product type:</td>
									<td><!-- <select name="ptype" id="ptype" style="height:50px;" MULTIPLE
										onChange="javascript:getproductstyles();">
										{if isset($smarty.post.ptype)} {html_options options=$prod_type
										selected=$smarty.post.ptype} {else} {html_options
										options=$prod_type } {/if}
									</select>-->
									<select name='ptype' id="ptype" {if $coupon_data.psids eq null}onChange="javascript:getproductstyles();"{else}{/if} MULTIPLE style="height:100px;"> 
										{foreach from=$producttypes key=group item=types}
											<optgroup label="{$group}">
												{foreach from=$types item=type}
													<option value="{$type.id}">{$type.name}</option>
												{/foreach}
										{/foreach}
									</select>
									</td>
									<td style="text-align: left;">Product Style</td>
	
									<td style="text-align: left;">
									{if $coupon_data.psids eq null}
									<select name="ProductStyle[]" MULTIPLE style="width:200px;" onchange="getstyleoptionsmap();" id="styleid">
									</select>
									{else}
									<select name="ProductStyle[]" MULTIPLE style="width:200px;" onchange="getstyleoptionsmap();" id="styleid">
										{foreach from=$prod_styles key=id item=name}
											<option value="{$id}"
												{foreach from=$coupon_data.psids key=i item=psid}
													{if $psid eq $id}
														selected="selected"
													{/if}
												{/foreach}
											>{$name}</option>
										{/foreach}
									</select>
									{/if}
									</td>
								</tr>
							</table>
							</td>
						</tr>
						
						<tr><td>&nbsp;</td></tr>
						
						<!-- <tr>
							<td>{$lng.lbl_coupon_apply_category}<br />
								{include file="main/category_selector_multiple.tpl" field="categoryIds[]"}<br />
							</td>
						</tr>
						
						<tr><td>&nbsp;</td></tr>
						
						<tr>
							<td>{$lng.lbl_coupon_apply_product}<br />
							<input type="hidden" name="productid_new" value="{$coupon_data.productid_new}" />
							<textarea type="text" readonly="readonly" cols="50" rows="2" name="productname">{$coupon_data.productname}</textarea>
							<input type="button" onclick="javascript: popup_product('coupon_form.productid_new','coupon_form.productname');" value="{$lng.lbl_browse_|escape}" />
							</td>
						</tr> -->
	
					</table>
					</td>
				</tr>
				
				<tr>
					<td class="FormButton" valign="top">Don't apply coupon on:</td>
					<td>&nbsp;</td>
					<td>
	
					<table cellpadding="1" cellspacing="1">
						
						<tr>
							<td>Select Product Types...<br />
							<table cellpadding="1" cellspacing="1">
								<tr>
									<td><!-- <select name="ExclProductType[]" MULTIPLE id="ExclProductType" style="height:50px;">
										{if isset($smarty.post.ptype)} {html_options options=$prod_type
										selected=$smarty.post.ptype} {else} {html_options
										options=$prod_type } {/if}
									</select> -->
									<select name='ExclProductType[]' MULTIPLE id="ExclProductType" style="height:100px;"> 
										{foreach from=$producttypes key=group item=types}
											<optgroup label="{$group}">
												{foreach from=$types item=type}
													<option value="{$type.id}"
														{foreach from=$coupon_data.exclptids item=exclptid}
															{if $exclptid eq $type.id}
																selected="selected"
															{/if}
														{/foreach}
													>{$type.name}</option>
												{/foreach}
										{/foreach}		
									</select>
									</td>
								</tr>
							</table>
							</td>
						</tr>
						
						<tr><td>&nbsp;</td></tr>
						
						<tr>
							<td>Select Product Styles belonging to Product Types...<br />
							<table cellpadding="1" cellspacing="1">
								<tr>
									<td>Product type:</td>
									<td><!-- <select name="exclptype" id="exclptype" style="height:50px;" MULTIPLE
										onChange="javascript:getexclproductstyles();">
										{if isset($smarty.post.ptype)} {html_options options=$prod_type
										selected=$smarty.post.ptype} {else} {html_options
										options=$prod_type } {/if}
									</select> -->
									<select name='exclptype' id="exclptype" {if $coupon_data.exclpsids eq null}onChange="javascript:getexclproductstyles();"{else}{/if} MULTIPLE style="height:100px;"> 
										{foreach from=$producttypes key=group item=types}
											<optgroup label="{$group}">
												{foreach from=$types item=type}
													<option value="{$type.id}">{$type.name}</option>
												{/foreach}
										{/foreach}
									</select>
									</td>
									<td style="text-align: left;">Product Style</td>
	
									<td style="text-align: left;">
									{if $coupon_data.exclpsids eq null}
									<select name="ExclProductStyle[]" MULTIPLE style="width:200px;" onchange="getstyleoptionsmap();" id="exclstyleid">
									</select>
									{else}
									<select name="ExclProductStyle[]" MULTIPLE style="width:200px;" onchange="getstyleoptionsmap();" id="exclstyleid">
										{foreach from=$prod_styles key=id item=name}
											<option value="{$id}"
												{foreach from=$coupon_data.exclpsids key=i item=exclpsid}
													{if $exclpsid eq $id}
														selected="selected"
													{/if}
												{/foreach}
											>{$name}</option>
										{/foreach}
									</select>
									{/if}
									</td>
								</tr>
							</table>
							</td>
						</tr>
						
						<tr><td>&nbsp;</td></tr>
						
						<!-- <tr>
							<td>{$lng.lbl_coupon_apply_category}<br />
								{include file="main/category_selector_multiple.tpl" field="exclCategoryIds[]"}<br />
							</td>
						</tr>
						
						<tr><td>&nbsp;</td></tr>
						
						<tr>
							<td>{$lng.lbl_coupon_apply_product}<br />
							<input type="hidden" name="excl_productid_new" value="{$coupon_data.excl_productid_new}" />
							<textarea type="text" readonly="readonly" cols="50" rows="2" name="excl_productname">{$coupon_data.excl_productname}</textarea>
							<input type="button" onclick="javascript: popup_product('coupon_form.excl_productid_new','coupon_form.excl_productname');" value="{$lng.lbl_browse_|escape}" />
							</td>
						</tr> -->
	
					</table>
					</td>
				</tr>
	
			</table>
			      
				<tr>
					<td colspan="3">
						
					</td>
				</tr>
				{/if}
				
	       {else}
	       
	    <tr>
			<td class="FormButton" width="20%">Coupon Name:</td>
			<td width="10"><font class="Star">*</font></td>
			<td width="100%"><input readonly type="text" size="25" maxlength="16" name="coupon_name" id="group_new" value="{$coupon.code}" /></td>
		</tr>
	
		<tr>
			<td class="FormButton" width="20%">Group Name:</td>
			<td>&nbsp;</td>
			<td id="groupDropdown" name="groupDropdown">
				<select id="groupname" name="groupname" disabled="true">
					{foreach from=$groups key=id item=name}
						<option value="{$name}" {if $coupon.groupName eq $name} selected="true"{/if}>{if $name eq ''}None{else}{$name}{/if}</option>
					{/foreach}
				</select>
			</td>
		</tr>
	
		<tr>
			<td class="FormButton" width="20%">Start Date:</td>
			<td width="10"><font class="Star">*</font></td>
			<td width="100%">
				<input type="text" id='startDate' name='startDate' value="{$coupon.startDate}" size="15"/>
				<a href="javascript:cal4.popup();" ><img src="{$cdn_base}/images/cal.gif" width="16" height="16" border="0" alt="Click Here to Pick up the date"></a>
			</td>
	  	</tr>
	       
	 	<tr>
			<td class="FormButton" width="20%">End Date:</td>
			<td width="10"><font class="Star">*</font></td>
			<td width="100%">
				<input type="text" id='endDate' name='endDate' value="{$coupon.endDate}" size="15"/>
				<a href="javascript:cal5.popup();" ><img src="{$cdn_base}/images/cal.gif" width="16" height="16" border="0" alt="Click Here to Pick up the date"></a>
			</td>
	  	</tr>
	  
	    <tr>
			<td class="FormButton" width="20%">Shipping option:</td>
			<td width="10">&nbsp;</td>
			<td width="100%">
				<input type="checkbox" name="freeShipping" id="freeShipping" value="Y" {if $coupon.freeship eq 1} checked="checked"{/if}/>&nbsp;&nbsp;Free shipping
			</td>
		</tr>
		
	  	<tr>
			<td class="FormButton" width="20%">Show in My Myntra:</td>
			<td width="10">&nbsp;</td>
			<td width="100%">
				<input type="checkbox" name="showInMyMyntra" id="showInMyMyntra" value="Y" {if $coupon.showInMyMyntra} checked="checked"{/if}/>&nbsp;&nbsp;Show in My Myntra
			</td>
		</tr>
	
		<tr>
			<td class="FormButton">{$lng.lbl_coupon_type}:</td>
			<td>&nbsp;</td>
			<td><!--  <select name="couponTypeNew" id="couponTypeNew" onchange="javascript: if (this.value == 'absolute') changeBox('1', true); else changeBox('1', false); if (this.value == 'free_ship') changeBox('4', false); else changeBox('4', true);"> -->
			<select name="couponType" id="couponTypeNew" onchange="javascript:typeDisplay();">
				<option value="percentage" {if $coupon.couponType eq "percentage"} selected="selected"{/if}>{$lng.lbl_coupon_type_percent}</option>
				<option value="absolute" {if $coupon.couponType eq "absolute"} selected="selected"{/if}>Upto {$config.General.currency_symbol} {$lng.lbl_coupon_type_absolute}</option>
				<option value="dual" {if $coupon.couponType eq "dual"} selected="selected"{/if}>% off upto {$config.General.currency_symbol}</option>
				<option value="max_percentage"{if $coupon.couponType eq "max_percentage"} selected="selected"{/if}> upto max {$lng.lbl_coupon_type_percent}</option>
				<option value="max_percentage_dual"{if $coupon.couponType eq "max_percentage_dual"} selected="selected"{/if}> {$lng.lbl_coupon_type_percent} upto maximum {$lng.lbl_coupon_type_percent}</option>
				
			</select>
			</td>
		</tr>
	
		<tr id="MRPpercentageNew">
			<td class="FormButton">Percentage Discount:</td>
			<td><font class="Star">*</font></td>
			<td><input type="text" size="25" name="MRPpercentage" 
				value="{if $coupon.mrpPercentage}{$coupon.mrpPercentage|formatprice}{else}{$zero}{/if}" />
			</td>
		</tr>
	
		<tr id="MRPAmountNew">
			<td class="FormButton">Discount Amount:</td>
			<td><font class="Star">*</font></td>
			<td><input type="text" size="25" name="MRPAmount"
				value="{if $coupon.mrpAmount}{$coupon.mrpAmount|formatprice}{else}{$zero}{/if}" /></td>
		</tr>
	
		<tr>
			<td class="FormButton">Max Usage:</td>
			<td>&nbsp;</td>
			<td>
			<table cellpadding="1" cellspacing="1">
				<tr>
					<td><input id="maxUsage" type="text" size="8" name="maxUsage"
						value="{$coupon.times|default:" 1"}" /></td>
					<td>&nbsp;</td>
					<td><input id="isInfinite" type="checkbox" name="isInfinite"
						{if $coupon.isInfinite} checked="checked" {/if} /></td>
					<td>Infinite Usage</td>
				</tr>
			</table>
			</td>
		</tr>
	
		<tr>
			<td class="FormButton">Max. Usage Per Cart:</td>
			<td>&nbsp;</td>
			<td><input type="text" size="25" name="maxUsagePerCart"
				id="maxUsagePerCart"
				value="{$coupon.maxUsagePerCart}" /></td>
		</tr>
	
		<tr>
			<td class="FormButton">Max Usage Per User:</td>
			<td>&nbsp;</td>
			<td>
			<table cellpadding="1" cellspacing="1">
				<tr>
					<td><input id="maxUsageByUser" type="text" size="8"
						name="maxUsageByUser"
						value="{$coupon.maxUsageByUser|default:" 1"}" /></td>
					<td>&nbsp;</td>
					<td><input id="isInfinitePerUser" type="checkbox"
						name="isInfinitePerUser"
						{if $coupon.isInfinitePerUser} checked="checked" {/if} /></td>
					<td>Infinite Usage Per User</td>
				</tr>
			</table>
			</td>
		</tr>
	
		<tr>
			<td class="FormButton">Minimum Subtotal:</td>
			<td>&nbsp;</td>
			<td>
			<table cellpadding="1" cellspacing="1">
				<tr>
					<td>{$config.General.currency_symbol}</td>
					<td><input id="minAmount" type="text" size="25"
						name="minAmount"
						value="{if $coupon.minimum}{$coupon.minimum|formatprice}{else}{$zero}{/if}" /></td>
				</tr>
			</table>
			</td>
		</tr>
	
		<tr>
			<td class="FormButton">Maximum Subtotal:</td>
			<td>&nbsp;</td>
			<td>
			<table cellpadding="1" cellspacing="1">
				<tr>
					<td>{$config.General.currency_symbol}</td>
					<td><input id="maxAmount" type="text" size="25"
						name="maxAmount"
						value="{if $coupon.maxAmount}{$coupon.maxAmount|formatprice}{else}{$zero}{/if}" /></td>
				</tr>
			</table>
			</td>
		</tr>
	
			
		<tr>
			<td class="FormButton">Minimum item Count:</td>
			<td>&nbsp;</td>
			<td>
				<table cellpadding="1" cellspacing="1">
				<tr>
					
					<td><input id="minCount" type="text" size="25" name="minCount" value="{if $coupon.min_count}{$coupon.min_count}{else}{0}{/if}" /></td>
				</tr>
				</table>
			</td>
		</tr>
				
		<tr>
			<td class="FormButton">Maximum item Count:</td>
			<td>&nbsp;</td>
			<td>
				<table cellpadding="1" cellspacing="1">
				<tr>
					
					<td><input id="maxCount" type="text" size="25" name="maxCount" value="{if $coupon.max_count}{$coupon.max_count}{else}{0}{/if}" /></td>
				</tr>
				</table>
			</td>
		</tr>
	 
	
		<!-- <tr>
			<td class="FormButton">Times locked:</td>
			<td>&nbsp;</td>
			<td>
			<table cellpadding="1" cellspacing="1">
				<tr>
					<td><input id="timesLocked" type="text" size="8"
						name="timesLocked" value="{$coupon.times_locked|default:"0"}" /></td>
				</tr>
			</table>
			</td>
		</tr> -->
	
		<tr>
			<td class="FormButton">{$lng.lbl_status}:</td>
			<td>&nbsp;</td>
			<td><select id="isActive" name="isActive"
				{if $coupon.status eq "U"}
					disabled="true">
					<option value="U" selected="selected">{$lng.lbl_coupon_used}</option>
				{else}
					>
					<option value="A" {if $coupon.status eq "A"} selected="selected"{/if}>{$lng.lbl_coupon_active}</option>
					<option value="D" {if $coupon.status eq "D"} selected="selected"{/if}>{$lng.lbl_coupon_disabled}</option>
				{/if}
			</select></td>
		</tr>
	
		<tr>
			<td class="FormButton">Allowed Users (2048 char):</td>
			<td>&nbsp;</td>
			<td><textarea id="users" name="users" rows="2" cols="100">{if $coupon.users}{$coupon.users}{else}{""}{/if}</textarea></td>
		</tr>
	
		<tr>
			<td class="FormButton">Excluded Users (2048 char):</td>
			<td>&nbsp;</td>
			<td><textarea id="excludeUsers" name="excludeUsers" rows="2"
				cols="100">{if $coupon.excludeUsers}{$coupon.excludeUsers}{else}{""}{/if}</textarea></td>
		</tr>
	
		<tr>
			<td class="FormButton">Comments:</td>
			<td><font class="Star">*</font></td>
			<td><textarea id="comments" name="comments" rows="2" cols="100">{if $coupon.comments}{$coupon.comments}{else}{""}{/if}</textarea></td>
		</tr>
		
		<tr>
			<td class="FormButton">Description:</td>
			<td><font class="Star">*</font></td>
			<td><textarea id="description" name="description" rows="2" cols="100">{if $coupon.description}{$coupon.description}{else}{""}{/if}</textarea></td>
		</tr>
	
	</table>
	
	<table>
		<tr>
			<td colspan="3"><br />
			<input type="submit" value="{$lng.lbl_update|escape} Coupon"
				onClick="javascript:document.coupon_form.mode.value='modify_single';document.coupon_form.submit();" />
			</td>
		</tr>
	</table>
		
	{/if}
	
	{if $mode ne 'modify'}
		<script language="javascript">
			  var cal4 = new calendar3(document.forms['coupon_form'].elements['startDate'], document.forms['coupon_form'].elements['endDate'], document.getElementById('validity').value);
		      cal4.year_scroll = true;
		      cal4.time_comp = false;
	
		      var cal5 = new calendar2(document.forms['coupon_form'].elements['endDate']);
		      cal5.year_scroll = true;
		      cal5.time_comp = false;
		</script>
	{/if}
			
			<table>
				<tr>
				  {if $mode ne 'modify' && $mode ne 'modifysingle'}
					<td colspan="3"><br />{$lng.txt_coupon_note}<br /><br />
						<input type="submit" name="submit" value="Add" onClick="this.style.visibility='hidden'; return validateCoupon();"/>
	
					</td>
				  {/if}
				</tr>
			</table>
	
	</form>
	
	{/capture}
	{if $mode ne 'modifysingle'}
	{include file="dialog.tpl" title=$lng.lbl_store_coupons_title content=$smarty.capture.dialog extra='width="100%"'}
	{else}
	{include file="dialog.tpl" title="Modify coupon" content=$smarty.capture.dialog extra='width="100%"'}
	{/if}
	
