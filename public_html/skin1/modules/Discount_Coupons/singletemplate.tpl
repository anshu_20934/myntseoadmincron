{include file="page_title.tpl" title="Add / Edit Coupon Template"}

<script type="text/javascript" language="JavaScript 1.2">
	var http_loc = "{$http_location}";
</script>
<script type="text/javascript" src="{$http_location}/skin1/myntra_js/jquery.min.js"></script>
{literal}
<script type="text/javascript" language="JavaScript 1.2">

	// Array of added product type IDs.
	var added_pts = new Array();
	var added_exclpts = new Array();

	// Array converter to object. Useful for in operations.
	function convertArray(arr)
	{
		var o = {};
	  	for(var i=0; i<arr.length; i++)
	  	{
	    	o[arr[i]]='';
	  	}
	  	return o;
	}

	function getproductstyles()
	{
		request = createRequest();
		var url = http_loc + "/admin/register_sku.php";
		var pr_id = document.getElementById("ptype").value;

		if (pr_id in convertArray(added_pts)) {
			return;
		}

		added_pts.push(pr_id);

		var poststr = "pr_id=" + encodeURI(pr_id);
		sendRequest(request,url,poststr,show_styles);
	}

	function getexclproductstyles()
	{
		request = createRequest();
		var url = http_loc + "/admin/register_sku.php";
		var pr_id = document.getElementById("exclptype").value;

		if (pr_id in convertArray(added_exclpts)) {
			return;
		}

		added_exclpts.push(pr_id);

		var poststr = "pr_id=" + encodeURI(pr_id);
		sendRequest(request,url,poststr,show_exclstyles);
	}

	function show_styles()
	{
		if (request.readyState == 4) {
			if (request.status == 200) {
				var response = request.responseText;
				document.getElementById("styleid").innerHTML += response;
			}
			else {
				alert("Error! Request status is " + request.status);
			}
		}
	}

	function show_exclstyles()
	{
		if (request.readyState == 4) {
			if (request.status == 200) {
				var response = request.responseText;
				document.getElementById("exclstyleid").innerHTML += response;
			}
			else {
				alert("Error! Request status is " + request.status);
			}
		}
	}

	function sendRequest(request,url,parameters,func_name)
	{
		request.onreadystatechange = func_name;
		request.open("POST", url, true);
		request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		request.setRequestHeader("Content-length", parameters.length);
		request.setRequestHeader("Connection", "close");
		request.send(parameters);
	}

	function createRequest()
	{
		var request = null;
		try {
			request = new XMLHttpRequest();
		}
		catch (trymicrosoft) {
			try {
			  	request = new ActiveXObject("Msxml2.XMLHTTP");
			} 
			catch (othermicrosoft) {
			  	try {
					request = new ActiveXObject("Microsoft.XMLHTTP");
			  	} 
			  	catch (failed) {
					request = null;
			  	}
			}
		}

		if (request == null) {
			alert("Error creating request object!");
		} 
		else {
			return request;
		}
	}

	function validateTemplate()
	{
		var pattern = /^[a-zA-Z0-9]+$/g;
		var string = document.getElementById('name').value;
		if (string == "" || !pattern.test(string)) {
			alert("Please enter a valid alphanumeric Template name.");
			document.getElementById('name').focus();
			return false;
		}

		if (document.getElementById('comments').value == "") {
			alert("Please enter comments.");
			document.getElementById('comments').focus();
			return false;
		}

		return true;
	}
</script>
{/literal}

<form action="coupon_templates.php" name="templateForm" method="post">
<input type="hidden" name="mode" id="mode" {if $template eq null}value="add"{else}value="edit"{/if} />
{if $template neq null}
<input type="hidden" name="timeCreated" id="timeCreated" value="{$template.timeCreated}" />
<input type="hidden" name="creator" id="creator" value="{$template.creator}" />
{/if}

<h1>Template Details:</h1>

<table>
	<tr>
		<td><strong>Template Name:</strong></td>
		<td><strong><font color="red">*</font></strong></td>
		<td><input {if $template neq null}readonly{/if} type="text" id="name" name="name" value="{$template.templateName}" /></td>
	</tr>
	<tr>
		<td><strong>Created/Edited By:</strong></td>
		<td><strong><font color="red">*</font></strong></td>
		<td><input readonly type="text" id="lastEditedBy" name="lastEditedBy" value="{$login}" /></td>
	</tr>
	<tr>
		<td><strong>Validity:</strong> (in days)</td>
		<td>&nbsp;</td>
		<td><input type="text" id="validity" name="validity" value="{$template.validity}" /></td>
	</tr>
	<tr>
		<td><strong>Show in My Myntra:</strong></td>
		<td>&nbsp;</td>
		<td><input type="checkbox" id="showInMyMyntra" name="showInMyMyntra" {if $template.showInMyMyntra}checked="checked"{/if} /></td>
	</tr>
	<tr>
		<td><strong>Shipping Option:</strong></td>
		<td>&nbsp;</td>
		<td><input type="checkbox" id="freeShipping" name="freeShipping" {if $template.freeShipping}checked="checked"{/if} /></td>
	</tr>
	<tr>
		<td><strong>Type:</strong></td>
		<td>&nbsp;</td>
		<td>
			<select id="type" name="type">
				<option value="absolute" {if $template.couponType eq "absolute"}selected="selected"{/if}>Upto Rs. Off</option>
				<option value="percentage" {if $template.couponType eq "percentage"}selected="selected"{/if}>% off</option>
				<option value="dual" {if $template.couponType eq "dual"}selected="selected"{/if}>% off Upto Rs.</option>
			</select>
		</td>
	</tr>
	<tr>
		<td><strong>Absolute discount:</strong></td>
		<td>&nbsp;</td>
		<td><input type="text" id="MRPAmount" name="MRPAmount" value="{$template.MRPAmount}"></td>
	</tr>
	<tr>
		<td><strong>Percentage discount:</strong></td>
		<td>&nbsp;</td>
		<td><input type="text" id="MRPpercentage" name="MRPpercentage" value="{$template.MRPpercentage}"></td>
	</tr>
	<tr>
		<td><strong>Max. Usage:</strong></td>
		<td>&nbsp;</td>
		<td>
			<input type="text" id="maxUsage" name="maxUsage" value="{$template.times}" />
			<input type="checkbox" id="isInfinite" name="isInfinite" {if $template.isInfinite}checked="checked"{/if} />
			Infinite Usage
		</td>
	</tr>
	<tr>
		<td><strong>Max. Usage Per Cart:</strong></td>
		<td>&nbsp;</td>
		<td><input type="text" id="maxUsagePerCart" name="maxUsagePerCart" value="{$template.maxUsagePerCart}"></td>
	</tr>
	<tr>
		<td><strong>Max. Usage Per User:</strong></td>
		<td>&nbsp;</td>
		<td>
			<input type="text" id="maxUsageByUser" name="maxUsageByUser"  value="{$template.maxUsageByUser}" />
		    <input type="checkbox" id="isInfinitePerUser" name="isInfinitePerUser" {if $template.isInfinitePerUser}checked="checked"{/if} />
		    Infinite Usage
		</td>
	</tr>
	<tr>
		<td><strong>Min. Subtotal:</strong></td>
		<td>&nbsp;</td>
		<td><input type="text" id="minimum" name="minimum" value="{$template.minimum}" /></td>
	</tr>
	<tr>
		<td><strong>Max. Subtotal:</strong></td>
		<td>&nbsp;</td>
		<td><input type="text" id="maxAmount" name="maxAmount" value="{$template.maxAmount}" /></td>
	</tr>
	<tr>
		<td><strong>Allowed Users:</strong> (2048 chars.)</td>
		<td>&nbsp;</td>
		<td><textarea id="users" name="users" rows="2" cols="100">{$template.users}</textarea></td>
	</tr>
	<tr>
		<td><strong>Excluded Users:</strong> (2048 chars.)</td>
		<td>&nbsp;</td>
		<td><textarea id="excludedUsers" name="excludedUsers" rows="2" cols="100">{$template.excludeUsers}</textarea></td>
	</tr>
	<tr>
		<td><strong>Comments:</strong></td>
		<td><strong><font color="red">*</font></strong></td>
		<td>
			<textarea id="comments" name="comments" rows="2" cols="100">{$template.comments}</textarea>
		</td>
	</tr>
	<tr>
		<td><strong>Description:</strong></td>
		<td><strong><font color="red">*</font></strong></td>
		<td>
			<textarea id="description" name="description" rows="2" cols="100">{$template.description}</textarea>
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td><strong>Allowed Product Types:</strong>
		<td>&nbsp;</td>
		<td>
			<select name='ProductType[]' MULTIPLE id="ProductType" style="height:100px;"> 
				{foreach from=$producttypes key=group item=types}
					<optgroup label="{$group}">
						{foreach from=$types item=type}
							<option value="{$type.id}"
								{foreach from=$template.ptids item=ptid}
									{if $ptid eq $type.id}
										selected="selected"
									{/if}
								{/foreach}
							>{$type.name}</option>
						{/foreach}
					</optgroup>
				{/foreach}
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td><strong>Allowed Product Styles:</strong></td>
		<td>&nbsp;</td>
		<td>
			<table cellpadding="1" cellspacing="1">
				<tr>
					<td>Select Product Type:</td>
					<td>
						<select name='ptype' id="ptype" onChange="javascript:getproductstyles();" MULTIPLE style="height:100px;"> 
							{foreach from=$producttypes key=group item=types}
								<optgroup label="{$group}">
									{foreach from=$types item=type}
										<option value="{$type.id}">{$type.name}</option>
									{/foreach}
								</optgroup>
							{/foreach}
						</select>
					</td>
					<td style="text-align: left;">Select Product Style</td>
					<td style="text-align: left;">
						<select name="ProductStyle[]" MULTIPLE style="width:200px;" id="styleid">
							{if $template neq null}
								{foreach from=$prod_styles key=id item=name}
									<option value="{$id}"
										{foreach from=$template.psids key=i item=psid}
											{if $psid eq $id}
												selected="selected"
											{/if}
										{/foreach}
									>{$name}</option>
								{/foreach}
							{/if}
						</select>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td><strong>Excluded Product Types:</strong>
		<td>&nbsp;</td>
		<td>
			<select name='exclProductType[]' MULTIPLE id="exclProductType" style="height:100px;"> 
				{foreach from=$producttypes key=group item=types}
					<optgroup label="{$group}">
						{foreach from=$types item=type}
							<option value="{$type.id}"
								{foreach from=$template.exclptids item=ptid}
									{if $ptid eq $type.id}
										selected="selected"
									{/if}
								{/foreach}
							>{$type.name}</option>
						{/foreach}
					</optgroup>
				{/foreach}
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td><strong>Excluded Product Styles:</strong></td>
		<td>&nbsp;</td>
		<td>
			<table cellpadding="1" cellspacing="1">
				<tr>
					<td>Select Product Type:</td>
					<td>
						<select name='exclptype' id="exclptype" onChange="javascript:getexclproductstyles();" MULTIPLE style="height:100px;"> 
							{foreach from=$producttypes key=group item=types}
								<optgroup label="{$group}">
									{foreach from=$types item=type}
										<option value="{$type.id}">{$type.name}</option>
									{/foreach}
								</optgroup>
							{/foreach}
						</select>
					</td>
					<td style="text-align: left;">Select Product Style</td>
					<td style="text-align: left;">
						<select name="exclProductStyle[]" MULTIPLE style="width:200px;" id="exclstyleid">
							{if $template neq null}
								{foreach from=$prod_styles key=id item=name}
									<option value="{$id}"
										{foreach from=$template.exclpsids key=i item=psid}
											{if $psid eq $id}
												selected="selected"
											{/if}
										{/foreach}
									>{$name}</option>
								{/foreach}
							{/if}
						</select>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td><input type="submit" {if $template eq null}value="Add Template"{else}value="Update Template"{/if} onClick="return validateTemplate();"/></td>
	</tr>
</table>

</form>
