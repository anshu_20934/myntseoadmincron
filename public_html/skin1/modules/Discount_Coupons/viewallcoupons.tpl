{* $Id: search_result.tpl,v 1.72.2.2 2006/06/16 10:47:41 max Exp $ *}
{include file="page_title.tpl" title='Coupon Management'}

{include file="dialog_tools.tpl"}

{literal}
<SCRIPT TYPE="text/javascript">
<!--
	function popup(mylink, windowname)
	{
		if (! window.focus)return true;
			var href;
		if (typeof(mylink) == 'string')
   			href=mylink;
		else
   			href=mylink.href;
		window.open(href, windowname, 'width=800,height=500,scrollbars=yes');
		return false;
	}
//-->
</SCRIPT>
{/literal}

<form action="viewcoupongroups.php" method="post" name="exportCouponsForm">
<input type="hidden" name="mode" id="mode" />
<input type="hidden" name="exportedgroup" id="exportedgroup" value="{$groupname}" />

<a name="results"/>



{if $mode eq "export"}

{capture name=dialog}

<div align="right">{if $previous}{$previous}{/if}|{if $next}{$next}{/if}(Total Pages:{$totalpages})</div>

<!--<form action="import.php" method="post" name="processproductform">
<input type="hidden" name="mode" value="update" />
<input type="hidden" name="navpage" value="{$navpage}" /> -->

<table cellpadding="0" cellspacing="0" width="100%">

<tr>
	<td>

<!-- show all the available  coupom here  -->
{if $coupons ne ""}

<table cellpadding="3" cellspacing="1" width="100%">

<tr class="TableHead">

	<td width="3%">&nbsp;</td>
	<td width="10%">COUPON</td>
	<td width="8%">TYPE</td>
	<td width="10%">DISCOUNT</td>
	<td width="10%">TIMES</td>
	<td width="10%">SUBTOTAL</td>
	<td width="12%">DISCOUNT OFFERED</td>
	<td width="10%">EXPIRES ON</td>
	<td width="7%">STATUS</td>
	<td width="10%">LAST MODIFIED</td>
	<td width="10%">LAST MODIFIED BY</td>
</tr>

{section name=prod_num loop=$coupons}
	
	{if $coupons[prod_num].times_used + $coupons[prod_num].times_locked gte $coupons[prod_num].times}  
		{assign var="color" value="red"} 
	{else}
		{assign var="color" value=""} 	   
	{/if}
	
	{if $coupons[prod_num].isInfinite eq 1}
		{assign var="color" value=""}
	{/if}

<tr{cycle values=", class='TableSubHead'" advance=false} >
	
	<td><input type="checkbox" name="posted_data[{$coupons[prod_num].code}][to_delete]" /></td>
	<td style="color:{$color};"><b>{$coupons[prod_num].code}</b></td>
	<td align="center">{$coupons[prod_num].couponType}</td>
	<td align="center">
		{if $coupons[prod_num].couponType eq "absolute"}
	 		Upto {include file="currency.tpl" value=$coupons[prod_num].mrpAmount} off
	 	{elseif $coupons[prod_num].couponType eq "percentage"}
	 		{$coupons[prod_num].mrpPercentage}% off
	 	{elseif $coupons[prod_num].couponType eq "dual"}
	 		{$coupons[prod_num].mrpPercentage}% off upto {include file="currency.tpl" value=$coupons[prod_num].mrpAmount}
	 	{else}
	 		{assign var="discount" value="Free shipping"}
	 	{/if}
	</td>
	<td align="center"><a href="coupon_usage.php?coupon={$coupons[prod_num].coupon}" onClick="return popup(this, 'Coupon Usage')">
		(<font color="green">{$coupons[prod_num].times_used}</font> + <font color="red">{$coupons[prod_num].times_locked}</font>)/{if $coupons[prod_num].isInfinite eq 1}-1{else}{$coupons[prod_num].times}{/if}
	</a></td>
	<td align="center">{$coupons[prod_num].subtotal}</td>
	<td align="center">{$coupons[prod_num].discountOffered}</td>
	<td align="center" nowrap="nowrap">{if $coupons[prod_num].expire neq 0}{$coupons[prod_num].expire|date_format:$config.Appearance.date_format}{/if}</td>
	<td align="center">{if $coupons[prod_num].status eq "A"}Active{elseif $coupons[prod_num].status eq "D"}Disabled{elseif $coupons[prod_num].status eq "U"}Used{else}Unknown{/if}</td>
	<td align="center" nowrap="nowrap">{if $coupons[prod_num].lastEdited neq 0}{$coupons[prod_num].lastEdited|date_format:$config.Appearance.datetime_format}{/if}</td>
	<td align="center">{$coupons[prod_num].lastEditedBy}</td>
</tr>

<!--<tr{cycle values=", class='TableSubHead'"}>
	<td colspan="7">
{if $coupons[prod_num].productid ne 0}
{$lng.lbl_coupon_contains_product|substitute:"productid":$coupons[prod_num].productid}
{elseif $coupons[prod_num].categoryid ne 0}
{if $active_modules.Simple_Mode}
{if $coupons[prod_num].recursive eq "Y"}
{$lng.lbl_coupon_contains_products_cat_rec_href|substitute:"categoryid":$coupons[prod_num].categoryid:"path":$catalogs.admin}
{else}
{$lng.lbl_coupon_contains_products_cat_href|substitute:"categoryid":$coupons[prod_num].categoryid:"path":$catalogs.admin}
{/if}
{else}
{if $coupons[prod_num].recursive eq "Y"}
{$lng.lbl_coupon_contains_products_cat_rec|substitute:"categoryid":$coupons[prod_num].categoryid}
{else}
{$lng.lbl_coupon_contains_products_cat|substitute:"categoryid":$coupons[prod_num].categoryid}
{/if}
{/if}
{else}
{capture name=minamount}{include file="currency.tpl" value=$coupons[prod_num].minimum}{/capture}
{$lng.lbl_coupon_greater_than|substitute:"amount":$smarty.capture.minamount}
{/if}
{if $coupons[prod_num].coupon_type eq "absolute" and ($coupons[prod_num].productid ne 0 or $coupons[prod_num].categoryid ne 0)}
<br />
{if $coupons[prod_num].productid ne 0}
{if $coupons[prod_num].apply_product_once eq "Y"}
{$lng.lbl_coupon_apply_once}
{else}
{$lng.lbl_coupon_apply_each_item}
{/if}
{elseif $coupons[prod_num].categoryid ne 0}
{if $coupons[prod_num].apply_product_once eq "Y" and $coupons[prod_num].apply_category_once eq "Y"}
{$lng.lbl_coupon_apply_once}
{elseif $coupons[prod_num].apply_product_once eq "N" and $coupons[prod_num].apply_category_once eq "N"}
{$lng.lbl_coupon_apply_each_item_cat}
{else}
{$lng.lbl_coupon_apply_each_title_cat}
{/if}
{/if}
{/if}
	</td>
</tr> -->

{/section}

</table>
</td>
</tr>
{/if}


<tr>
<td>

<!-- end -->
<br />



<br />

<!--<input type="button" value="{$lng.lbl_delete_selected|escape}" onclick="javascript: if (checkMarks(this.form, new RegExp('productids\[[0-9]+\]', 'gi'))) if (confirm('{$lng.txt_delete_products_warning|strip_tags}')) submitForm(document.processproductform, 'delete');" /> 
&nbsp;&nbsp;&nbsp;&nbsp;
<input type="submit" value="{$lng.lbl_update|escape}" />
&nbsp;&nbsp;&nbsp;&nbsp; 
<input type="button" value="{$lng.lbl_modify_selected|escape}" onclick="javascript: if (checkMarks(this.form, new RegExp('productids\[[0-9]+\]', 'gi'))) {ldelim} document.processproductform.action='product_modify.php'; submitForm(document.processproductform, 'list'); {rdelim}" />-->

<br /><br />
<!--<input type="button" value="{$lng.lbl_export|escape}" onclick="javascript: submitForm(document.processproductform, 'export');" /> -->
<input type="submit" value="Add New Coupon(s)" onclick="javascript:document.exportCouponsForm.mode.value='add';document.searchCouponsForm.submit();"/>
<input type="submit" value="Modify Coupon" onclick="javascript:document.exportCouponsForm.mode.value='modifysingle';document.searchCouponsForm.submit();"/>
<input type="submit" value="Delete Coupon(s)" onclick="javascript:document.exportCouponsForm.mode.value='deletesingle';document.searchCouponsForm.submit();" />
<input type="button" value="Export Properties" onclick="javascript:window.location.href='exportfile.php?group={$groupname}&mode=properties';"  target="_blank" />
<input type="button" value="Export Usage" onclick="javascript:window.location.href='exportfile.php?group={$groupname}&mode=usage';"  target="_blank" /> 
&nbsp;&nbsp;&nbsp;&nbsp;
<!--<input type="button" value="{$lng.lbl_export_all_found|escape}" onclick="javascript: self.location='search.php?mode=search&amp;export=export_found';" /> -->

<br /><br /><br />

{*$lng.txt_operation_for_first_selected_only*}

<br /><br />

<!--<input type="button" value="{$lng.lbl_preview_product|escape}" onclick="javascript: if (checkMarks(this.form, new RegExp('productids\[[0-9]+\]', 'gi'))) submitForm(document.processproductform, 'details');" />
&nbsp;&nbsp;&nbsp;&nbsp;
<input type="button" value="{$lng.lbl_clone_product|escape}" onclick="javascript: if (checkMarks(this.form, new RegExp('productids\[[0-9]+\]', 'gi'))) submitForm(document.processproductform, 'clone');" />
&nbsp;&nbsp;&nbsp;&nbsp;
<input type="button" value="{$lng.lbl_generate_html_links|escape}" onclick="javascript: if (checkMarks(this.form, new RegExp('productids\[[0-9]+\]', 'gi'))) submitForm(document.processproductform, 'links');" /> -->

	</td>
</tr>

</table>
<!--</form> -->

<br />

{/capture}
{include file="dialog.tpl" title=$lng.lbl_search_results content=$smarty.capture.dialog extra='width="100%"'}

{/if}

<br /><br />

</form>

