<h3>Coupon details:</h3>
<table border="1" cellpadding="3">
	<tr bgcolor="gray">
		<td>COUPON</td>
		<td>TIMES LOCKED</td>
		<td>TIMES USED</td>
		<td>MAX. USAGE</td>
		<td>SUBTOTAL</td>
		<td>DISCOUNT</td>
	</tr>
	<tr>
		<td>{$coupon_data.coupon}</td>
		<td>{$coupon_data.times_locked}</td>
		<td>{$coupon_data.times_used}</td>
		<td>{if $coupon_data.isInfinite eq 1}inf.{else}{$coupon_data.times}{/if}</td>
		<td>{$coupon_data.subtotal}</td>
		<td>{$coupon_data.discountOffered}</td>
	</tr>
</table>

&nbsp;

<h3>Coupon locks:</h3>
<table border="1" cellpadding="3">
	<tr bgcolor="gray">
		<td>TIMESTAMP</td>
		<td>ORDER ID</td>
		<td>USER ID</td>
		<td>ORDER STATUS</td>
	</tr>
	{section name=prod_num loop=$lock_orders}
		<tr>
			<td>{$lock_orders[prod_num].date|date_format:$config.Appearance.datetime_format}</td>
			<td>{$lock_orders[prod_num].orderid}</td>
			<td>{$lock_orders[prod_num].login}</td>
			<td>{$lock_orders[prod_num].status}</td>
		</tr>
	{/section}
</table>

&nbsp;

<h3>Coupon usage:</h3>
<table border="1" cellpadding="3">
	<tr bgcolor="gray">
		<td>TIMESTAMP</td>
		<td>ORDER ID</td>
		<td>USER ID</td>
		<td>ORDER STATUS</td>
	</tr>
	{section name=prod_num loop=$usage_orders}
		<tr>
			<td>{$usage_orders[prod_num].date|date_format:$config.Appearance.datetime_format}</td>
			<td>{$usage_orders[prod_num].orderid}</td>
			<td>{$usage_orders[prod_num].login}</td>
			<td>{$usage_orders[prod_num].status}</td>
		</tr>
	{/section}
</table>
