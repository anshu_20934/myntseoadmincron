<!-- 
  Page for user-specific coupons search.
  Author: Rohan.Railkar
 -->
{include file="main/include_js.tpl" src="main/calendar2.js"}
{include file="page_title.tpl" title='Coupon Management'}

{literal}
<script type="text/javascript" language="JavaScript 1.2">

	function validateSearch()
	{
		document.getElementById('page').value = 1;
		document.getElementById('npage').value = 2;
		return true;
	}

	function getPrevPage()
	{
		document.getElementById('page').value = document.getElementById('ppage').value;
	}

	function getNextPage()
	{
		document.getElementById('page').value = document.getElementById('npage').value;
	}

</script>
{/literal}

<form action="searchCoupons.php" method="post" name="searchCouponsForm" id="searchCouponsForm">
<input type="hidden" name="mode" id="mode" />
<input type="hidden" name="page" id="page" value="{$page}" />
<input type="hidden" name="npage" id="npage" value="{$npage}" />
<input type="hidden" name="ppage" id="ppage" value="{$ppage}" />
<table cellpadding="0" cellspacing="0" width="100%">


	<tr><h1>Search Coupons</h1></tr>
	<tr>
		<td> <!-- Form -->
			<table>
				<tr>
					<td><strong>Start Date:</strong></td>
					<td>
						<input type="text" id="searchStartDate" name="searchStartDate" value="{$searchStartDate}" size="15" />
						<a href="javascript:cal1.popup();">
							<img src="{$cdn_base}/images/cal.gif" width="16" height="16" border="0" alt="Click Here to Pick up the date"> 
						</a>
					</td>
				</tr>
				<tr>
					<td><strong>End Date:</strong></td>
					<td>
						<input type="text" id="searchEndDate" name="searchEndDate" value="{$searchEndDate}" size="15" />
						<a href="javascript:cal2.popup();">
							<img src="{$cdn_base}/images/cal.gif" width="16" height="16" border="0" alt="Click Here to Pick up the date"> 
						</a>
					</td>
				</tr>
				
				<script language="javascript">
					var cal1 = new calendar2(document.forms['searchCouponsForm'].elements['searchStartDate']);
    				cal1.year_scroll = true;
    				cal1.time_comp = false;

    				var cal2 = new calendar2(document.forms['searchCouponsForm'].elements['searchEndDate']);
    				cal2.year_scroll = true;
    				cal2.time_comp = false;
				</script>
				
				<tr>
					<td><strong>Coupon Group:</strong></td>
					<td>
						<select id="searchGroup" name="searchGroup">
							<option value="" {if $searchGroup eq ""}selected="true"{/if}">All</option>
							{foreach from=$groups key=id item=name}
								<option value="{$name}" {if $searchGroup eq $name}selected="true"{/if}">{$name}</option>
							{/foreach}
						</select>
					</td>
				</tr>
				<tr>
					<td><strong>Channel:</strong></td>
					<td>
						<select id="searchChannel" name="searchChannel">
							<option value="" {if $searchChannel eq ""}selected="true"{/if}">All</option>
							{foreach from=$channels key=id item=name}
								<option value="{$name}" {if $searchChannel eq $name}selected="true"{/if}">{$name}</option>
							{/foreach}
						</select>
					</td>
				</tr>
				<tr>
					<td><strong>Usage:</strong></td>
					<td>
						<input type="text" id="searchUsageGTE" name="searchUsageGTE" value={$searchUsageGTE}></input>
						&nbsp; &le; Usage &le; &nbsp;
						<input type="text" id="searchUsageLTE" name="searchUsageLTE" value={$searchUsageLTE}></input>
					</td>
				</tr>
				<tr>
					<td><strong>Per User Usage:</strong></td>
					<td>
						<input type="text" id="searchUsagePerUserGTE" name="searchUsagePerUserGTE" value={$searchUsagePerUserGTE}></input>
						&nbsp; &le; Per User Usage &le; &nbsp;
						<input type="text" id="searchUsagePerUserLTE" name="searchUsagePerUserLTE" value={$searchUsagePerUserLTE}></input>
					</td>
				</tr>
				<tr>
					<td><strong>Percentage Discount:</strong></td>
					<td>
						<input type="text" id="searchPercentageGTE" name="searchPercentageGTE" value={$searchPercentageGTE}></input>
						% &nbsp; &le; Percentage Discount &le; &nbsp;
						<input type="text" id="searchPercentageLTE" name="searchPercentageLTE" value={$searchPercentageLTE}></input>
						%
					</td>
				</tr>
				<tr>
					<td><strong>Absolute Discount:</strong></td>
					<td>
						Rs. <input type="text" id="searchAbsGTE" name="searchAbsGTE" value={$searchAbsGTE}></input>
						&nbsp; &le; Absolute Discount &le; &nbsp;
						Rs. <input type="text" id="searchAbsLTE" name="searchAbsLTE" value={$searchAbsLTE}></input>
					</td>
				</tr>
				<tr>
					<td><strong>Free shipping:</strong></td>
					<td>
						<select id="searchFreeShipping" name="searchFreeShipping">
							<option value="" {if $searchFreeShipping eq ""}selected="true"{/if}>Any</option>
							<option value="1" {if $searchFreeShipping eq "1"}selected="true"{/if}>Free Shipping</option>
							<option value="0" {if $searchFreeShipping eq "0"}selected="true"{/if}>No Free Shipping</option>
						</select>
					</td>
				</tr>
				<tr>
					<td><strong>Min. Cart Amount:</strong></td>
					<td>
						Rs. <input type="text" id="searchMinGTE" name="searchMinGTE" value={$searchMinGTE}></input>
						&nbsp; &le; Min. Cart Amount &le; &nbsp;
						Rs. <input type="text" id="searchMinLTE" name="searchMinLTE" value={$searchMinLTE}></input>
					</td>
				</tr>
				<tr>
					<td><strong>Max. Cart Amount:</strong></td>
					<td>
						Rs. <input type="text" id="searchMaxGTE" name="searchMaxGTE" value={$searchMaxGTE}></input>
						&nbsp; &le; Max. Cart Amount &le; &nbsp;
						Rs. <input type="text" id="searchMaxLTE" name="searchMaxLTE" value={$searchMaxLTE}></input>
					</td>
				</tr>
				<tr>
					<td><strong>Allowed User Patterns:</strong></td>
					<td><input type="text" id="searchAllowedUsers" name="searchAllowedUsers" value={$searchAllowedUsers}></input></td>
				</tr>
				<tr>
					<td><strong>Excluded User Patterns:</strong></td>
					<td><input type="text" id="searchExclUsers" name="searchExclUsers" value={$searchExclUsers}></input></td>
				</tr>
				<tr>
					<td><strong>Description contains:</strong></td>
					<td><input type="text" id="searchDesc" name="searchDesc" value={$searchDesc}></input></td>
				</tr>
				<tr>
					<td><input type="submit" value="Search" onClick="return validateSearch();"></td>
				</tr>
			</table>
		</td>
		<td>
			&nbsp;
		</td>
	</tr>
	<tr>
		<td align="right">
			{if $previous}<input type="submit" value="Previous" onClick="getPrevPage();">{/if}
			| (Current page: {$page}/{$totalpages}) 
			| {if $next}<input type="submit" value="Next" onClick="getNextPage();">{/if}
		</td>
	</tr>

</table>

&nbsp;
<hr />

<!-- The results table. -->
<table cellpadding="3" cellspacing="1" width="100%">
	<tr class="TableHead">
		<td width="3%">&nbsp;</td>
		<td width="10%">COUPON</td>
		<td width="10">GROUP</td>
		<td width="10%">USERS</td>
		<td width="5%">TYPE</td>
		<td width="10%">DISCOUNT</td>
		<td width="10%">TIMES</td>
		<td width="10%">SUBTOTAL</td>
		<td width="10%">DISCOUNT OFFERED</td>
		<td width="9%">EXPIRES ON</td>
		<td width="4%">STATUS</td>
		<td width="9%">LAST EDITED</td>
	</tr>
	
{section name=prod_num loop=$coupons}
	
	{if $coupons[prod_num].times_used + $coupons[prod_num].times_locked gte $coupons[prod_num].times}  
	    {assign var="color" value="red"} 
	 {else}
	 	{assign var="color" value=""} 	   
	 {/if}

<tr{cycle values=", class='TableSubHead'" advance=false} >
	
	<td><input type="checkbox" name="posted_data[{$coupons[prod_num].code}][selected]" /></td>
	<td style="color:{$color};"><b>{$coupons[prod_num].code}</b></td>
	<td align="center">{$coupons[prod_num].groupName}</td>
	<td align="center">{$coupons[prod_num].users}</td>
	<td align="center">{$coupons[prod_num].couponType}</td>
	<td align="center">
		{if $coupons[prod_num].couponType eq "absolute"}
	 		Upto {include file="currency.tpl" value=$coupons[prod_num].mrpAmount} off
	 	{elseif $coupons[prod_num].couponType eq "percentage"}
	 		{$coupons[prod_num].mrpPercentage}% off
	 	{elseif $coupons[prod_num].couponType eq "dual"}
	 		{$coupons[prod_num].mrpPercentage}% off upto {include file="currency.tpl" value=$coupons[prod_num].mrpAmount}
	 	{else}
	 		{assign var="discount" value="Free shipping"}
	 	{/if}
	</td>
	<td align="center">(<font color="green">{$coupons[prod_num].times_used}</font> + <font color="red">{$coupons[prod_num].times_locked}</font>)/{$coupons[prod_num].times}</td>
	<td align="center">{$coupons[prod_num].subtotal}</td>
	<td align="center">{$coupons[prod_num].discountOffered}</td>
	<td align="center" nowrap="nowrap">{$coupons[prod_num].expire|date_format:$config.Appearance.date_format}</td>
	<td align="center">{if $coupons[prod_num].status eq "A"}Active{elseif $coupons[prod_num].status eq "A"}Disabled{else}Unknown{/if}</td>
	<td align="center" nowrap="nowrap">{$coupons[prod_num].lastEdited|date_format:$config.Appearance.time_format}</td>
	
</tr>
{/section}

<tr>
	<td colspan="12"><br />
		<input type="submit" value="Delete Coupon(s)" onclick="javascript:document.searchCouponsForm.mode.value='deleteCoupon';document.searchCouponsForm.submit();" />
		&nbsp;
		<input type="submit" value="Modify Coupon" onclick="javascript:document.searchCouponsForm.mode.value='modifyCoupon';document.searchCouponsForm.submit();"/>
	</td>
</tr>
	
</table>

</form>

