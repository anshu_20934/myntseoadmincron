<!-- 
  Page for viewing all coupon templates.
  Author: Rohan.Railkar
 -->

{include file="page_title.tpl" title='Coupon Templates'}

<form action="coupon_templates.php" name="templatesForm" method="post">
<input type="hidden" name="action" id="action" />

<table cellpadding="3" cellspacing="1" width="100%">
	<tr class="TableHead">
		<td width="3%">&nbsp;</td>
		<td width="10%">TEMPLATE</td>
		<td width="10%">VALIDITY</td>
		<td width="10%">TYPE</td>
		<td width="10%">DISCOUNT</td>
		<td width="10%">PER USER / TOTAL USAGE</td>
		<td width="12%">MIN / MAX CART VALUE</td>
		<td width="15%">COMMENTS</td>
		<td width="10%">LAST MODIFIED</td>
		<td width="10%">LAST MODIFIED BY</td>
	</tr>
	
{section name=prod_num loop=$templates}

<tr{cycle values=", class='TableSubHead'" advance=false} >
	
	<td><input type="checkbox" name="posted_data[{$templates[prod_num].templateName}][selected]" /></td>
	<td style="color:{$color};"><b>{$templates[prod_num].templateName}</b></td>
	<td align="center">{$templates[prod_num].validity}</td>
	<td align="center">{$templates[prod_num].couponType}</td>
	<td align="center">
		{if $templates[prod_num].couponType eq "absolute"}
	 		Upto {include file="currency.tpl" value=$templates[prod_num].MRPAmount} off
	 	{elseif $templates[prod_num].couponType eq "percentage"}
	 		{$templates[prod_num].MRPpercentage}% off
	 	{elseif $templates[prod_num].couponType eq "dual"}
	 		{$templates[prod_num].MRPpercentage}% off upto {include file="currency.tpl" value=$templates[prod_num].MRPAmount}
	 	{else}
	 		{assign var="discount" value="Free shipping"}
	 	{/if}
	</td>
	<td align="center">{if $templates[prod_num].isInfinitePerUser}inf.{else}{$templates[prod_num].maxUsageByUser}{/if} / {if $templates[prod_num].isInfinite}inf.{else}{$templates[prod_num].times}{/if}</td>
	<td align="center">{$templates[prod_num].minimum} / {$templates[prod_num].maxAmount}</td>
	<td align="center">{$templates[prod_num].comments}</td>
	<td align="center" nowrap="nowrap">{$templates[prod_num].lastEdited|date_format:$config.Appearance.datetime_format}</td>
	<td align="center">{$templates[prod_num].lastEditedBy}</td>
	
</tr>
{/section}

<tr>
	<td colspan="12"><br />
		<input type="submit" value="Delete Template(s)" onclick="javascript:document.templatesForm.action.value='deleteTemplate';document.templatesForm.submit();" />
		&nbsp;
		<input type="submit" value="Modify template" onclick="javascript:document.templatesForm.action.value='modifyTemplate';document.templatesForm.submit();"/>
		&nbsp;
		<input type="submit" value="Add New Template" onClick="javascript:document.templatesForm.action.value='addTemplate';document.templatesForm.submit();" />
	</td>
</tr>
	
</table>
</form>
