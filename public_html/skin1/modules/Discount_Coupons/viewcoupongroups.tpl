{* $Id: coupons.tpl,v 1.36.2.1 2006/06/16 10:47:43 max Exp $ *}
{include file="page_title.tpl" title=$lng.lbl_store_coupons_title}
{*include file="main/include_js.tpl" src="main/popup_product.js"*}
{literal}
<script type="text/javascript" language="JavaScript 1.2">
//<!--

function formValid(mode) 
{
	var elm = document.getElementById("cgroup");
	var hiddObj = document.getElementById("mode");
	hiddObj.value = mode;
	if (elm.value == "")
	{
		if (confirm("show all coupon groups ??")){
		return true;
		}
		else{
		elm.focus();
		return false;
		}
	}
	
	//elm.style.display = (status == true) ? "" : "none";
}
function formValidsingle(mode) 
{
	var elm = document.getElementById("cgroup");
	var hiddObj = document.getElementById("singlemode");
	hiddObj.value = mode;
	if (elm.value == "")
	{
		if (confirm("show all coupons??")){
		return true;
		}
		else{
		elm.focus();
		return false;
		}
	}
	
	//elm.style.display = (status == true) ? "" : "none";
}

function popup(mylink, windowname)
{
	if (! window.focus)return true;
		var href;
	if (typeof(mylink) == 'string')
			href=mylink;
	else
			href=mylink.href;
	window.open(href, windowname, 'width=800,height=500,scrollbars=yes');
	return false;
}

//-->
</script>
{/literal}
{*$lng.txt_discountcoupons_desc*}

<br /><br />

{capture name=dialog}

<form action="viewcoupongroups.php" method="post" name="couponsform">
<input type="hidden" name="mode" id="mode"  >
<input type="hidden" name="pageType" id="pageType"  >

<input type="hidden" name="singlemode" id="singlemode"  >

<!-- filter to search for groups -->

<table cellpadding="3" cellspacing="1" width="100%">
     <tr >
			<td width="30%"><strong> Search by pattern  :</strong> </td>
			<td width="40%">
			       <input type="text" name="cgroup" id="cgroup"  size="20" />
 		  	</td>
 		  	<td width="30%"><strong><a href="searchCoupons.php">Advanced Coupons Search</a></strong></td>
	</tr>
 	
 	 <tr >
			<td width="20%">
			   <input  type="submit" name="search" value=" search coupon groups "   onclick="return formValid('search');" />
      	 	</td>
      	 	<td width="20%">
			   <input  type="submit" name="search" value="search coupons"   onclick="return formValidsingle('search');" />
      	 	</td>
      	 	<td width="30%"><strong><a href="coupon_templates.php">Coupon Templates</a></strong></td>
	</tr>
 	
</table>
<br/>
</br>
<!-- end -->
<!-- display group listing -->
{ if $search_method eq "group"}
<table cellpadding="3" cellspacing="1" width="100%">

<tr class="TableHead">
	<td width="5%">&nbsp;</td>
	<td width="15%">NAME</td>
	<td width="10%" align="center">CHANNEL</td>
	<td width="15" align="center">TEMPLATE</td>
	<td width="10%">STATUS</td>
	<td width="15%" align="center">COUNT</td>
	<td width="15%" align="center">LAST MODIFIED</td>
	<td width="15%" align="center">LAST MODIFIED BY</td>
</tr>


	{section name=prod_num loop=$coupongroups}
	
	<tr{cycle values=", class='TableSubHead'" advance=false}>
		<td><input type="checkbox" name="posted_data[{$coupongroups[prod_num].groupname}][to_delete]" /></td>
		<td><b><a href="{$self}?searchGroup={$coupongroups[prod_num].groupname}">{$coupongroups[prod_num].groupname}</a></b></td>
		<td align="center">{$coupongroups[prod_num].channel}</td>
		<td align="center">{$coupongroups[prod_num].templateName}</td>
		<td align="center">
			<select name="posted_data[{$coupongroups[prod_num].groupname}][isActive]"
				{if $coupongroups[prod_num].active eq "U"}
					disabled="true">
					<option value="U" selected="selected">{$lng.lbl_coupon_used}</option>
				{else}
					>
					<option value="A"{if $coupongroups[prod_num].active eq "A"} selected="selected"{/if}>{$lng.lbl_coupon_active}</option>
					<option value="D"{if $coupongroups[prod_num].active eq "D"} selected="selected"{/if}>{$lng.lbl_coupon_disabled}</option>
				{/if}
			</select>
		</td>
		<!--  td align="center">{$coupongroups[prod_num].active_count} / {$coupongroups[prod_num].count}</td -->
		<td align="center">{$coupongroups[prod_num].count}</td>
		<td align="center">{if $coupongroups[prod_num].lastEdited neq 0}{$coupongroups[prod_num].lastEdited|date_format:$config.Appearance.datetime_format}{/if}</td>
		<td align="center">{$coupongroups[prod_num].lastEditedBy}</td>
	</tr>
{/section}

	<tr>
		<td colspan="7"><br />
		<input type="submit" value="Add New Group" onclick="javascript:document.couponsform.mode.value='add';document.couponsform.submit();"/>
		<input type="submit" value="Modify Group" onclick="javascript:document.couponsform.mode.value='modify';document.couponsform.submit();"/>
		<!--<input type="button" value="Delete Group" onclick="javascript:document.couponsform.mode.value='delete';document.couponsform.submit();" />-->
		<input type="submit" value="Update Status" onclick="javascript:document.couponsform.mode.value='update';document.couponsform.submit();"/>
		<!-- <input type="submit" value="Explore Group"  onclick="javascript:document.couponsform.mode.value='export';document.couponsform.submit();" /> -->
		<input type="submit" value="Add Styles Excl Incl Group" onclick="javascript:document.couponsform.pageType.value='exclIncl';document.couponsform.submit();"/>
		<input type="submit" value="Product Capping Actions" onclick="javascript:document.couponsform.pageType.value='productCapping';document.couponsform.submit();"/>
		</td>
	</tr>



 {if $coupongroups eq ''}
	<tr>
		<td colspan="6" align="center"><br />{$lng.txt_no_discount_coupons}</td>
	</tr>

{/if}

</table>
{elseif $search_method eq "single"}
<table cellpadding="3" cellspacing="1" width="100%">

<tr class="TableHead">
    <td width="">&nbsp;</td>
	<td width="">COUPON</td>
	<td width="">GROUP</td>
	<td width="">TYPE</td>
	<td width="">DISCOUNT</td>
	<td width="">TIMES</td>
	<td width="">EXPIRES ON</td>
	<td width="">STATUS</td>
	<td width="">LAST MODIFIED</td>
	<td width="">LAST MODIFIED BY</td>
</tr>

{section name=prod_num loop=$coupons}
	
	 {if $coupons[prod_num].times_used + $coupons[prod_num].times_locked gte $coupons[prod_num].times}  
	    {assign var="color" value="red"} 
	 {else}
	 	{assign var="color" value=""} 	   
	 {/if}
	 
	 {if $coupons[prod_num].isInfinite eq 1}
	 	{assign var="color" value=""}
	 {/if}

<tr{cycle values=", class='TableSubHead'" advance=false} >
	<td><input type="checkbox" name="posted_data[{$coupons[prod_num].code}][to_delete]" /></td>
	<td style="color:{$color};"><b>{$coupons[prod_num].code}</b></td>
	<td>{$coupons[prod_num].groupName}</td>
	<td align="center">{$coupons[prod_num].couponType}</td>
	<td align="center">
		{if $coupons[prod_num].couponType eq "absolute"}
	 		Upto {include file="currency.tpl" value=$coupons[prod_num].mrpAmount} off
	 	{elseif $coupons[prod_num].couponType eq "percentage"}
	 		{$coupons[prod_num].mrpPercentage}% off
	 	{elseif $coupons[prod_num].couponType eq "dual"}
	 		{$coupons[prod_num].mrpPercentage}% off upto {include file="currency.tpl" value=$coupons[prod_num].mrpAmount}
	 	{else}
	 		{assign var="discount" value="Free shipping"}
	 	{/if}
	</td>
	<td align="center"><a href="coupon_usage.php?coupon={$coupons[prod_num].code}" onClick="return popup(this, 'Coupon Usage')">
		(<font color="green">{$coupons[prod_num].times_used}</font> + <font color="red">{$coupons[prod_num].times_locked}</font>)/{if $coupons[prod_num].isInfinite}-1{else}{$coupons[prod_num].times}{/if}
	</a></td>
	<td align="center" nowrap="nowrap">{if $coupons[prod_num].expire neq 0}{$coupons[prod_num].expire|date_format:$config.Appearance.date_format}{/if}</td>
	<td align="center">{if $coupons[prod_num].status eq "A"}Active{elseif $coupons[prod_num].status eq "D"}Disabled{elseif $coupons[prod_num].status eq "U"}Used{else}Unknown{/if}</td>
	<td align="center" nowrap="nowrap">{if $coupons[prod_num].lastEdited neq 0}{$coupons[prod_num].lastEdited|date_format:$config.Appearance.datetime_format}{/if}</td>
	<td align="center">{$coupons[prod_num].lastEditedBy}</td>
</tr>

<!--<tr{cycle values=", class='TableSubHead'"}>
	<td colspan="7">
{if $coupons[prod_num].productid ne 0}
{$lng.lbl_coupon_contains_product|substitute:"productid":$coupons[prod_num].productid}
{elseif $coupons[prod_num].categoryid ne 0}
{if $active_modules.Simple_Mode}
{if $coupons[prod_num].recursive eq "Y"}
{$lng.lbl_coupon_contains_products_cat_rec_href|substitute:"categoryid":$coupons[prod_num].categoryid:"path":$catalogs.admin}
{else}
{$lng.lbl_coupon_contains_products_cat_href|substitute:"categoryid":$coupons[prod_num].categoryid:"path":$catalogs.admin}
{/if}
{else}
{if $coupons[prod_num].recursive eq "Y"}
{$lng.lbl_coupon_contains_products_cat_rec|substitute:"categoryid":$coupons[prod_num].categoryid}
{else}
{$lng.lbl_coupon_contains_products_cat|substitute:"categoryid":$coupons[prod_num].categoryid}
{/if}
{/if}
{else}
{capture name=minamount}{include file="currency.tpl" value=$coupons[prod_num].minimum}{/capture}
{$lng.lbl_coupon_greater_than|substitute:"amount":$smarty.capture.minamount}
{/if}
{if $coupons[prod_num].coupon_type eq "absolute" and ($coupons[prod_num].productid ne 0 or $coupons[prod_num].categoryid ne 0)}
<br />
{if $coupons[prod_num].productid ne 0}
{if $coupons[prod_num].apply_product_once eq "Y"}
{$lng.lbl_coupon_apply_once}
{else}
{$lng.lbl_coupon_apply_each_item}
{/if}
{elseif $coupons[prod_num].categoryid ne 0}
{if $coupons[prod_num].apply_product_once eq "Y" and $coupons[prod_num].apply_category_once eq "Y"}
{$lng.lbl_coupon_apply_once}
{elseif $coupons[prod_num].apply_product_once eq "N" and $coupons[prod_num].apply_category_once eq "N"}
{$lng.lbl_coupon_apply_each_item_cat}
{else}
{$lng.lbl_coupon_apply_each_title_cat}
{/if}
{/if}
{/if}
	</td>
</tr> -->

{/section}
<tr>
		<td colspan="7"><br />
		<input type="submit" value="{$lng.lbl_modify_selected|escape}" onclick="javascript:document.couponsform.mode.value='modifysingle';document.couponsform.submit();"/>
		<input type="button" value="{$lng.lbl_delete_selected|escape}" onclick="javascript:document.couponsform.mode.value='deletesingle';document.couponsform.submit();" />
		<input type="button" value="Export Properties" onclick="javascript:window.location.href='exportfile.php?search={$groupname}&mode=properties';"  target="_blank" />
		<input type="button" value="Export Usage" onclick="javascript:window.location.href='exportfile.php?search={$groupname}&mode=usage';"  target="_blank" />
		</td>
	</tr>
</table>
{/if}

</form>

<table cellpadding="3" cellspacing="1" width="100%">
<tr><td><b>Download bulk generated coupon-list with user tagging:</b></td></tr>
{foreach from=$bulkCouponGenerationOutputFiles item=fileName}
	<tr><td>
	<a href="/admin/operations/download.php?filename={$fileName}&type=bulk_coupon_generation_output" target="_blank">{$fileName}</a></br>
	</td></tr>
{/foreach}
</table>
{/capture}
{include file="dialog.tpl" title=$lng.lbl_store_coupons_title content=$smarty.capture.dialog extra='width="100%"'}
