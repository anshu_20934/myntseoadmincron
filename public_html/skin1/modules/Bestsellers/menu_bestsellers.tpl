{* $Id: menu_bestsellers.tpl,v 1.12 2005/11/17 06:55:40 max Exp $ *}
{if $bestsellers}
{capture name=menu}
<table cellpadding="0" cellspacing="0" width="100%">

{section name=num loop=$bestsellers}

<tr>
	<td class="VertMenuItems" height="16">{math equation="value+1" value=$smarty.section.num.index}.
<a href="product.php?productid={$bestsellers[num].productid}&amp;cat={$cat}&amp;bestseller=Y" class="VertMenuItems">{$bestsellers[num].product}</a><br />
	</td>
</tr>
{if not $smarty.section.num.last}
<tr>
	<td class="VertMenuLine"><img src="{$cdn_base}/skin1/images/spacer.gif" width="1" height="1" alt="" /></td>
</tr>
{/if}

{/section}

</table>
{/capture}
{ include file="menu.tpl" dingbats="dingbats_categorie.gif" menu_title=$lng.lbl_bestsellers menu_content=$smarty.capture.menu }
{/if}
