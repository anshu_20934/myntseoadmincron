<html>
<head>
{literal}


<script Language="JavaScript" Type="text/javascript">
		function FrontPage_Form1_Validator(theform)
		{
         
         var thresholdcount=theform.thresholdcount;
         var thresholdemail=theform.thresholdemail;
         var unitprice=theform.unitprice;
         var notes=theform.notes;
         var vendor=theform.vendor;
         
         if (vendor.value==null||vendor.value == "")
		  {
			alert("Please enter vendor");
			vendor.focus();
			return false;
		  }
		  if (thresholdcount.value==null||thresholdcount.value == "")
		  {
			alert("Please enter thresholdcount.");
			thresholdcount.focus();
			return false;
		  }
		  if (thresholdemail.value==null||thresholdemail.value == "")
		  {
			alert("Please enter thresholdemail.");
			thresholdemail.focus();
			return false;
		  }
		 
		 if (unitprice.value==null||unitprice.value == "")
		  {
			alert("Please enter unitprice.");
			unitprice.focus();
			return false;
		  }
		 
		 if (notes.value==null||notes.value == "")
		  {
			alert("Please enter notes.");
			notes.focus();
			return false;
		  }
		 
		 
		  return true;
		  
		}
		function delschedule(scid,rid){
		var del="delschedule";
		  if(confirm("Do you really want to delete this schedule")){
		  
		  window.open('edit_report.php?schid='+scid+'&reportid='+rid+'&action='+del+'','null', 'menubar=1,resizable=1,toolbar=1,width=800,height=500');
		  
		  }
		}
		</script>
		{/literal}

<link rel="stylesheet" href="../skin1/mykriti.css"/>

</head>
<body id="body">

	<div id="container"> 
	    <div id="display"> 
	      <div class="center_column"> 
	        <div class="print"> 
		        <div class="super" > 
	           		 <p>&nbsp;</p>
	          	</div>
	          	{if $commentSuccess ne "Y"}
	          	<div class="head" style="padding-left:15px;"> 
		            <p>&nbsp;&nbsp;Report Schedules</p>
          		</div>
          		<div class="links"><p></p></div>
          		<div class="foot"></div>
   {include file="main/include_js.tpl" src="main/popup_product.js" src="main/overlib.js"}
{popup_init src="main/overlib.js"}       		
          		
<form method="POST"  language="JavaScript" name="delform" >
	
	{if $comments}
	<table style="border:solid 1px black; width:100%;">
				<tr style="background-color:#4f81bd;">
					
					<td style="color:white; font-weight:bold; font-size:1.0em;">Report Name</td>
					<td style="color:white; font-weight:bold; font-size:1.0em;">Schedule Type</td>
					<td style="color:white; font-weight:bold; font-size:1.0em;">Run Period</td>
					<td style="color:white; font-weight:bold; font-size:1.0em;">Last Run Time</td>
					<td style="color:white; font-weight:bold; font-size:1.0em;">Next Run Time</td>
					<td style="color:white; font-weight:bold; font-size:1.0em;">To be Mailed to</td>
					<td style="color:white; font-weight:bold; font-size:1.0em;">Action</td>
					
				</tr>
				{foreach from=$comments item=comment}
				<tr bgcolor="{cycle values="#d0d8e8,#e9edf4"}">					
					<td style="font-size:.8em;">{$comment.reportname}</td>
					<td style="font-size:.8em;">{$comment.type}</td>
					<td style="font-size:.8em;">{$comment.period}</td>
					<td style="font-size:.8em;">{$comment.lastrun}</td>
					<td style="font-size:.8em;">{$comment.nextrun}</td>
					<td style="font-size:.8em;">{$comment.mailto}</td>
					<td style="font-size:.8em;"><a href="#"  onClick="javascript:delschedule('{$comment.id}','{$reportid}');">delete</td>
					
				</tr>				
                {/foreach}    
			</table>
			</form>

			{/if}
		{elseif $commentSuccess eq "Y"}
		<table>
				<tr>
					<td>Schedule edited Succesfully!!!!!</td>
					<td>&nbsp;&nbsp;</td>
					<td>&nbsp;&nbsp;</td>
				</tr>
				<tr>
					<td style="font-size:.8em;color:red;"><b>Please Click the close button below to refresh data on back screen</b></td>
				</tr>
				<tr>
					<td align="center"><input type="button" style="height:20px;width:50px; background-color:black; color:white;" value="Close" onclick="opener.window.location.reload();self.close()"></td>
				</tr>
		</table>
		{/if}
        {if $reportid != ""}
          			<a href="#"  onClick="javascript:window.open('edit_report.php?reportid={$reportid}&action=schedule','null', 'menubar=0,resizable=1,toolbar=0,width=900,height=700')">Add Schedule</a>
        {/if}
		</div> 
	      </div>   	
	     </div> 
	   </div> 
	</body> 
	</html>
    	