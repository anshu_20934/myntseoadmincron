<form id="nikeform" action="{$http_location}/modules/affiliates/reebok_ipl.php" method="POST" class="clearfix">
    <input type="hidden" name="styleid" id="style_id" value="{$default_style}">
    <input type="hidden" name="styleprice" id="style_price" value="{$default_price}">
    <input type="hidden" name="personalize" value="on">
    <div class="top_strip clearfix">
		              <div class="product-gallery-head left">
              					<div class="title left prepend">Team India Jersey</div>
              					<div class="type-filter left">
              						<select id="tshirt-type" class="filter-select filter-tshirt-type">
              	  						{section name=jersey loop=$jerseys}
                                                  <option value="{$jerseys[jersey].style_id}">{$jerseys[jersey].type}</option>
                                          {/section}
              	    				</select>
              					</div>
              					<span class="price"><span class="currency">RS</span><span class="product_price">{$default_price}</span></span>
              				</div>
              				<div class="title right">Choose a jersey to buy</div>

    </div>
	<div class="product-gallery-container clearfix">
			<div class="product-gallery-content clearfix left">
				<div class="product-gallery span-19 left">
					<div class="product-image-tabs blue-bg corners left">
                        {section name=jersey loop=$jerseys}
						    <div>
						        <img class="product-image" src="{$jerseys[jersey].display_image}" alt="Team India Jersey - {$jerseys[jersey].type}">
						        <img class="customize-product-image" src="{$jerseys[jersey].image}" alt="Team India Jersey - {$jerseys[jersey].type}">
						        <select class="customize-product-image" id="select_list_{$jerseys[jersey].style_id}">{$jerseys[jersey].sizes}</select>
						    </div>
						{/section}
					</div>
					<div class="vertical-scroller left clearfix">
						<!-- "previous page" action -->
						<span class="browse-btn"><a class="prev up left"></a></span>
						<div class="vertical-scrollable left">
							<div class="vertical-tabs vertical-items left">
                                    {section name=jersey loop=$jerseys}
                                        <div><a id="tab_{$jerseys[jersey].style_id}" class="first" href="#" onclick="jersey_preview('{$jerseys[jersey].image}','{$jerseys[jersey].price}', '{$jerseys[jersey].style_id}')"><img class="corners" src="{$jerseys[jersey].thumb}" alt="Image Not Available"></a></div>
                                    {/section}
						    </div>
						</div>
						<!-- "next page" action -->
						<span class="browse-btn"><a class="next down left"></a></span>
					</div>
				</div>
			</div>
			<div class="product-details left">
				<div class="product-details-heading"><span style="float:left;">Personalize</span> <span class="red" style="float:right" >Free Personalization worth Rs. 399/-</span></div>
				<div class="product-details-block corners black-bg5 clearfix" >
					<div class="details">
					<div class="choose text">
						<label>Text</label>
						<input type="text" name="u_name" id="preview-name" value="Enter a name" class="gallery-block-input toggle-text" maxlength="8" tabindex="5">
					</div>
					<div class="choose number" >
						<label>Number</label>
						<input type="text" name="u_number" id="preview-number" value="Enter a number" class="gallery-block-input toggle-text" maxlength="2" tabindex="6">
					</div>
					</div>					
                    <input type="button" value="Preview" class="product-preview corners">
				</div>
				<div class="product-details-main clearfix">
					<div class="product-details-main-left left">
						<div class="choose size">
							<label>Size</label>
							<select name="size" id="size_list" class="filter-select filter-size">
                                   {$jerseys[0].sizes}
    						</select>
						</div>
						<div class="choose quantity">
							<label>Quantity</label>
							<select name="qty" class="filter-select filter-quantity">
	    						<option value="1">1</option>
	  							<option value="2">2</option>
	  							<option value="3">3</option>
	    					</select>
						</div>
						<span class="price"><span class="currency">RS</span><span class="product_price">{$default_price}</span></span>

                        <span class="notes red">Free Personalization worth Rs. 399/-</span>
						<span class="notes">Free Shipping and Cash on Delivery in India</span>
						<span class="notes">International shipping available</span>
                                                <div class="user_name"></div>
                                                <div class="user_number"></div>
						
					</div>
					<div class="product-details-main-right right">
						<a class="sizing-chart no-decoration-link" onclick="$('#size-chart-div').slideDown();"> Sizing Chart</a>
						<div class="size-chart" id="size-chart-div" style="display:none;">
	<div class="sizechart-title"><b>Size Chart</b> (In Centimeters) - for chest</div>
					<div class="close-btn">
					{literal}
					<a onclick="$('#size-chart-div').slideUp();" style="cursor:pointer">
					{/literal}
						<img src="{$cdn_base}/skin1/affiliatetemplates/images/reebok_ipl/close-1.png" alt="close">
					</a>
					</div>
					<div class="sizes">
						<div class="adults">
						<div>ADULTS</div>
						<ul>
							<li style="width:35px"><span class="size-type">XS</span> <span class="size-number">84</span></li>
							<li style="width:35px"><span class="size-type">S</span> <span class="size-number">92</span></li>
							<li style="width:35px"><span class="size-type">M</span> <span class="size-number">100</span></li>
							<li style="width:35px"><span class="size-type">L</span> <span class="size-number">108</span></li>
							<li style="width:35px"><span class="size-type">XL</span> <span class="size-number">116</span></li>
							<li style="width:35px"><span class="size-type">XXL</span> <span class="size-number">124</span></li>
						</ul>
						</div>
					</div>
</div>
                        <input type="button" value="Buy Now" class="buy-now corners" onclick="{literal}if(validateJerseyForm('')) {$('#nikeform').submit();}{/literal}">						
					</div>
				</div>
			</div>
			<div class="other-teams right">
					<a href="{$http_location}/kolkata-knight-riders-jersey" class="kkr link">Kolkata Knight Riders</a>
					<a href="{$http_location}/deccan-chargers-jersey" class="dc link">Deccan Chargers</a>
					<a href="{$http_location}/chennai-super-kings-jersey" class="csk link">Chennai Super Kings</a>
					<a href="{$http_location}/mumbai-indians-jersey" class="mi link">Mumbai Indians</a>
					<a href="{$http_location}/delhi-daredevils-jersey" class="dd link">Delhi Daredevils</a>
					<a href="{$http_location}/rajasthan-royals-jersey" class="rr link">Rajasthan Royals</a>
					<a href="{$http_location}/kings-xi-punjab-jersey" class="kp link">Kings XI Punjab</a>
					<a href="{$http_location}/royal-challengers-bangalore-jersey" class="brc link">B'lore Royal Challengers</a>
				</div>
		</div>
</form>