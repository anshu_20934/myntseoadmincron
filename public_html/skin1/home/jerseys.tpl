   <div class="flash" >
              <div class="panes" id="jersey">
            <h2>{$properties.home.sports_jersey_title.value}</h2>
            <h3>{$properties.home.section_jersey_related.value}:
              {foreach from=$properties.jersey_tags key=k item=v}
                   <a href="{$v.value}">{$k}</a>
              {/foreach}
                 </h3>
            <div class="scrollable-box">
                     <a class="prevPage browse left"></a>
                    <div class="scrollable" >
                         <div class="items">
                          {section name=idx loop=$images.sports_jerseys}
                            <div>
                                 <a href="{$images.sports_jerseys[idx].url|replace:" ":"%20"}">
                                 <img src="{$cdn_base}/skin1/images/spacer.gif" lazy-src="{$cdn_base}/{$images.sports_jerseys[idx].image}" class="lazy" width="140" height="140" id="sj_{$images.sports_jerseys[idx].id}" alt="{$images.sports_jerseys[idx].alt}" title="{$images.sports_jerseys[idx].title}">
                                 </a>
                                 <span>{$images.sports_jerseys[idx].name}</span>
                            </div>
                           {/section}
                        </div>
                    </div>
                    <a class="nextPage browse right"></a>
                     <div class="tabs_more"><a href="{$http_location}/cricket-jerseys"> See More Sports Jerseys </a></div>
                <!-- "next page" action -->
                    </div></div> </div>
