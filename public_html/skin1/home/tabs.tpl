 <div class="flash">
          <!-- the tabs -->
          <div class="tabs">
              <ul>
              {foreach from=$tabs key=k item=v}
                    <li><a href="#">{$v.name}</a></li>
              {/foreach}
              </ul>
          </div>
          <!-- tab "panes" -->
          <div class="pane">

    {foreach from=$tabs_content key=k item=v}
                     <div class="panes">
              <div id="tab-{$k}" class="panes-box" >
                  <h2>{$tabs[$k].name}</h2>
                  <h3>{$properties.home.section_tabs_related.value}:

                  {assign var="tags" value=","|explode:$tabs[$k].tags}
                  {section name=i loop=$tags}
                    <a href="{$http_location}/{$tags[i]|trim|replace:' ':'-'|strtolower}/search/">{$tags[i]}</a>
                 {/section}

                  </h3>
                  <div class="scrollable-box">
                     <a class="prevPage browse left"></a>
                    <div class="scrollable">
                         <div class="items">
                         {section name=i loop=$v}
                         <div>
                         <a href="{$http_location}/{$v[i].product|replace:" ":"-"}/T-Shirts/FC/PD/{$v[i].productid}"                                                                                                                                                                                                                                                                                             style="color: rgb(12, 94, 148);text-decoration:none;">
                            <img src="{$cdn_base}/skin1/images/spacer.gif" lazy-src="{$v[i].image_portal_thumbnail_160|replace:'./':''}" class="lazy" height="140" width="140"  id="tb_{$v[i].productid}" alt="{$v[i].product}" title="{$v[i].product}">
                            </a>
                            </div>
                        {/section}
                        </div>
                    </div>
                    <a class="nextPage browse right"></a>
                <!-- "next page" action -->
                <div class="tabs_more"><a href="{$http_location}/{$tabs[$k].name|trim|replace:' ':'-'|strtolower}/search/">See More {$tabs[$k].name}</a></div>
                    </div>
                </div>
            </div>

              {/foreach}

          </div>
          </div>