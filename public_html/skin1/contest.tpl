{include file="site/doctype.tpl"}
<head>
	{include file="site/header.tpl" }
</head>
<body class="{$_MAB_css_classes}">
	{include file="site/menu.tpl" }
	<div class="container clearfix">
		<h1 class="h4">TERMS &amp; CONDITIONS OF “PARIS CALLING” OFFER</h1>
	    <div class="content clearfix" style="position:relative;">
			<p><b>Please read the following terms and conditions very carefully as your use of service is subject to your acceptance of and compliance with the following terms and conditions ("Terms").</b></p>
			<p>By subscribing to or using any of our services you agree that you have read, understood and are bound by the Terms, regardless of how you subscribe to or use the services. If you do not want to be bound by the Terms, you must not subscribe to or use our services.</p>
			<p>In these Terms, references to "you", "User" hall mean the end user accessing the Website, its contents and using the Services offered through the Website, "Service Providers" mean independent third party service providers, and "we", "us" and "our" shall mean Vector E-commerce Private Limited and its affiliates.</p>
			<ul>
				<li>All orders placed between 8th March 2012 and 8th April 2012 will be eligible for this offer, excluding orders from the State of Tamil Nadu, where this offer is not applicable.</li>
				<li>The offer is valid only for individual orders worth Rs.2000 or more. Combining multiple orders is not permitted.</li>
				<li>The winning orders shall not be eligible for cash refund/ credit to bank account for that specific order upon returning.</li>
				<li>Myntra.com shall under no circumstance exchange the prize won, for cash. Provided however, in the unlikely event of Myntra.com not being able to provide any of the mentioned prize(s), Myntra.com shall provide an equivalent replacement as generally available in the Indian markets.</li> 
				<li>All the participants shall be required to ensure that the mobile number(s), e‐mail address and/or other details provided by them to Myntra.com via the Website or otherwise are true, accurate and in use. Any liability, consequence or claim arising on account of any incorrect or outdated information provided by the participants to Myntra.com shall solely be borne by the affected/concerned participants. Myntra.com shall at no time be required to verify the accuracy and/or correctness of the information so provided by the participants.</li> 
				<li>Further, you are responsible for maintaining the confidentiality of your mobile phones, e‐mail accounts and passwords.</li> 
				<li>Myntra.com may at its sole discretion choose to publish the name of the winner(s) on its website and/or any other publicity media that Myntra.com may, at its sole discretion, choose to employ without any further compensation to the winning participant. You waive your right to object to such publication and by participating in this offer; you grant Myntra.com the right to use your name for the above mentioned purpose.</li>
				<li>The winner of the Paris trip and his/her partner should hold a valid Indian passport to avail the prize.</li>
				<li>Myntra.com will only cover the economy-class airfare, airport transfers in Paris and accommodation expenses. Any other expense will have to be borne by the individual.</li>
				<li>The travel to Paris will be ex-Mumbai or ex-Delhi. Myntra.com will not bear the travel expense from the residence city of the winning individual to Delhi/Mumbai.</li>
				<li>The winner of the Paris trip will have to inform Myntra.com of the travel dates within 7 days of winning the prize.</li>
				<li>Paris trip can be availed only between May 1st 2012 and May 31st 2012.</li> 
				<li>In case of cancellation/ rescheduling of the trip, the charges will have to borne by the individual</li>
				<li>All travel terms & conditions as per the travel partner will be applicable. For more details, please refer <a href="http://myntramailer.s3.amazonaws.com/march2012/1331039190MyntraT&C.pdf" target="_blank">here</a>.</li>
				<li>Myntra.com reserves the right to amend, modify, cancel, update or withdraw this offer at any time without notice.</li> 
				<li>By participating in this deal, all eligible participants agree to be bound by these Terms and Conditions, the Terms of Use, Privacy Policy and other relevant documentation that are available on Myntra.com including any modifications, alterations or updates that we make.</li> 
				<li>Myntra.com employees and relatives of the employees are not eligible to participate in this offer.</li> 
				<li>Myntra.com reserves the right, in its sole discretion, to disqualify any participant that tampers or attempts to tamper with the deal or violates these Terms and Conditions or acts in a disruptive manner</li>
				<li>Myntra.com shall not be liable to for any indirect, punitive, special, incidental or consequential damages arising out of or in connection with the offer.</li>
				<li>The images of the products shown are for visual representation only and may vary from the actual product.</li>
				<li>Apple and Timex are not affiliated with or endorsing, sponsoring, or supporting this offer. They are the sole owners of their patents, copyrights and trademarks connected with their respective products and brands.</li>
				<li>Myntra.com doesn’t provide the guarantee/warranty of the prizes provided. All the guarantee/warranty for the prizes is as per manufacturer terms.</li>
				<li>Offer is subject to the laws of India and all disputes arising hereunder shall be subject to the jurisdiction of Courts of Bangalore.</li> 
				<li>References to  “Myntra.com” mean Vector E-commerce Pvt. Ltd., situated at<br/> 
				Vector E-Commerce Pvt Ltd.<br/>
				7th Mile, Krishna Reddy Industrial Area, Kudlu Gate,<br/>
				Opp Macaulay High School, behind Andhra Bank ATM,<br/>
				Bangalore - 560068<br/>
				India</li>
			</ul>		   	
		</div>
	</div>
	{include file="site/footer.tpl"}
</body>
</html>
