{* $Id: mykritihome.tpl,v 1.88.2.3 2006/07/19 10:19:35 max Exp $ *}
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
{config_load file="$skin_config"}
<html>
<head>
{include file="mykritimeta.tpl"}
<script type="text/javascript" src="{$SkinDir}/js_script/numberformat.js"></script>
<script type="text/javascript" src="{$SkinDir}/order_js.js"></script>
<script language="JavaScript" type="text/javascript" src="{$SkinDir}/shipping_calculate_ajax.js"></script>
<script type="text/javascript" src="{$SkinDir}/js_script/giftmessage.js"></script>
<script type="text/javascript" src="{$SkinDir}/js_script/email_validate.js"></script>

<script type="text/javascript" src="{$SkinDir}/js_script/ajaxObject.js"></script>
<link rel="stylesheet" href="{$SkinDir}/css/tooltip.css" />
<script type="text/javascript" src="{$SkinDir}/js_script/alttxt.js"></script>
<script type="text/javascript">
var check_option_enable_amount = '{$check_option_enable}';
</script>
 {literal}
<script type="text/javascript" language="JavaScript 1.2">

function func_update_gateway(){
if(document.getElementById("netbanking").checked==true)
	document.getElementById("is_netbanking").value="netbanking";
else
	document.getElementById("is_netbanking").value="";

if(document.getElementById("net").checked==true ||document.getElementById("netbanking").checked==true ||document.getElementById("mobilepayment").checked==true||document.getElementById("cashcard").checked==true )
	document.getElementById("gateway").value="ccavenue";
else if(document.getElementById("ebs").checked==true || document.getElementById("ebs_debit").checked==true)
	document.getElementById("gateway").value="ebs";
else
	document.getElementById("gateway").value="other";

}
function updateTotalAmount()
{
	var amount = parseFloat(document.getElementById("amount").innerHTML);
	var vat = parseFloat(document.getElementById("vat").innerHTML);
	var coupondiscount = 0;
	if( document.getElementById("coupondiscount"))
		coupondiscount = parseFloat(document.getElementById("coupondiscount").innerHTML);
	var shippingcost = parseFloat(document.getElementById("shippingcost").innerHTML);
	if(shippingcost == 0.00){
	   document.getElementById("freeshipping").innerHTML = "(FREE Shipping in India)";
	} else {
	   document.getElementById("freeshipping").innerHTML = "";
	}
	var cod = 0;
	if(document.getElementById('cod') && document.getElementById('cod').checked)
		cod = parseFloat(document.getElementById("cash").value);
	var giftprice = parseFloat(document.getElementById("giftprice").innerHTML) * parseInt(document.getElementById("box_count").value); 

	var finaltotal = amount + vat - coupondiscount + shippingcost + cod + giftprice;
	if(finaltotal < 0){
	   finaltotal = 0 ;
	}
	document.getElementById("totalAmount").innerHTML = numberformat(finaltotal,2);
	checkOptionEnable(finaltotal);
}

function enableAddress(option)
{
	if( option == 'b')
	{
		enableAddressEx('b');
		disableAddressEx('s');
	}
	else
	{
		enableAddressEx('s');
		disableAddressEx('b');
	}
}

function enableAddressEx(option)
{
	document.getElementById(option+"_firstname").readOnly = false;
	document.getElementById(option+"_lastname").readOnly = false;
	document.getElementById(option+"_address").readOnly = false;
	document.getElementById(option+"_country").readOnly = false;
	document.getElementById(option+"_state").readOnly = false;
	document.getElementById(option+"_city").readOnly = false;
	document.getElementById(option+"_zipcode").readOnly = false;
	document.getElementById(option+"_phone").readOnly = false;
	document.getElementById(option+"_mobile").readOnly = false;
	document.getElementById(option+"_email").readOnly = false;
}
function disableAddressEx(option)
{
	document.getElementById(option+"_firstname").readOnly = true;
        document.getElementById(option+"_lastname").readOnly = true;
        document.getElementById(option+"_address").readOnly = true;
        document.getElementById(option+"_country").readOnly = true;
        document.getElementById(option+"_state").readOnly = true;
        document.getElementById(option+"_city").readOnly = true;
        document.getElementById(option+"_zipcode").readOnly = true;
        document.getElementById(option+"_phone").readOnly = true;
        document.getElementById(option+"_mobile").readOnly = true;
        document.getElementById(option+"_email").readonly = true;
}
function verifyShipOption(){
    if(document.getElementById("cod") != null){
    if(document.getElementById('cod').checked!=true){
            
            document.getElementById('codcharge').style.display = 'none';
            document.getElementById('codtext').style.display = 'none';
            document.getElementById('billingaddress').style.display = 'block';
            document.getElementById('billinglabel').style.display = 'block';

            if(document.getElementById('s_option').selectedIndex ==2){
            document.getElementById('s_option').selectedIndex = 0;            
            }
    }
    else{
            
            document.getElementById('codcharge').style.display = 'block';
            document.getElementById('codtext').style.display = 'block';
            document.getElementById('billingaddress').style.display = 'none';
            document.getElementById('billinglabel').style.display = 'none';

            document.getElementById('s_option').selectedIndex = 2;

    }
    }
	else{
            if(document.getElementById('s_option').selectedIndex ==2){
            document.getElementById('s_option').selectedIndex = 0;            
            }
	}
	
}



function hideSuccessDiv(){
	document.getElementById('messagepopup').style.visibility = 'hidden';
}
function hideWithDelay(){
	setTimeout("hideSuccessDiv()", 7000);
}
function showMessagePopup(){
	document.getElementById('popup').style.visibility = 'visible';
	document.getElementById('popup').style.display = '';
}
function hideMessageDiv(){
	document.getElementById('popup').style.visibility = 'hidden';
	document.getElementById('popup').style.display = 'none';
}

function callShippingMethod(callee){
	if(callee == 'shop')
	{
		document.getElementById("s_firstname").value = document.getElementById("h_firstname").value;
                document.getElementById("s_lastname").value = document.getElementById("h_lastname").value;
                document.getElementById("s_address").value = document.getElementById("h_address").value;
                document.getElementById("s_country").value = document.getElementById("h_country").value;
                document.getElementById("s_state").value = document.getElementById("h_state").value;
                document.getElementById("s_city").value = document.getElementById("h_city").value;
                document.getElementById("s_zipcode").value = document.getElementById("h_zipcode").value;
                document.getElementById("s_phone").value = document.getElementById("h_phone").value;
                document.getElementById("s_mobile").value = document.getElementById("h_mobile").value;
                document.getElementById("s_email").value = document.getElementById("h_email").value;
		
	}else{
		document.getElementById("s_firstname").value = '';
	        document.getElementById("s_lastname").value = '';
        	document.getElementById("s_address").value = '';
	        document.getElementById("s_country").value = '';
	        document.getElementById("s_state").value = '';
	        document.getElementById("s_city").value = '';
	        document.getElementById("s_zipcode").value = '';
	        document.getElementById("s_phone").value = '';
	        document.getElementById("s_mobile").value = '';
	        document.getElementById("s_email").value = '';
	}
}

</script>
{/literal}
</head>
<body id="body" >
{literal}
<script type="text/javascript" language="JavaScript 1.2">
hideWithDelay();
</script>
{/literal}
	<div id="view_shipping_details"></div>
	<div id="scroll_wrapper">
		<div id="container">
			{include file="mykrititop.tpl" }
			<div id="wrapper" >
				{include file="breadcrumb.tpl" }
    			<div id="display">
					<!-- start left panel -->
					<div class="menu_column">{include file="leftpaneMyAccount.tpl" }</div>
      				<!-- end -->
					<div class="center_column">
						<form name="paymentform" action="mkorderinsert.php" method="POST">
							<input type="hidden" name="is_shipping_cost_set" id="is_shipping_cost_set" {if $s_city != null}value="1" {else}value="0"{/if}>
							<input type="hidden" name="checksumkey" id="checksumkey" value="{$checksumkey}">
							<div class="form">
								<div class="subhead">
									<p>address details</p>
								</div>
								<div>
									<table cellpadding="2" class="table">
										<tr>
											<td class="align_left"><font style="font-size:1.5em"><b>shipping address</b></font></td>                
											<td class="align_left">
												<span id="billinglabel">
													<table>
														<tr>
															<td>
																<font style="font-size:1.0em"><b>billing address</b></font>
															</td>
															<td>
																<table>
																	<tr>
																		<td>
																			&nbsp;&nbsp;&nbsp;<input type="checkbox" name="billtoship" id="billtoship" value="btoship" class="checkbox" onclick="javascript:flushCall();" {if $ship2diff == 'Y'}checked{/if}>&nbsp;<font style="font-size:0.8em">same as shipping</font>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
													</table>
												</span>
											</td>               
										</tr>
										<tr>
											<td>
												<p> {include file="mkshippingselected_re.tpl" }</P>
											</td>
											<td>
												<span id="billingaddress" > <p> {include file="mkbillingselected.tpl" }</P> </span>
											</td>
										</tr>
										<input type=hidden id=captureuserdetails value="{$captureuserdetails}" name=captureuserdetails />
										{if $captureuserdetails eq 1}
											{literal}
												<script type="text/javascript" language="JavaScript 1.2">
													function b2c()
													{
														if(document.getElementById("b2ci").checked == true)
														{
															document.getElementById("c_firstname").value = document.getElementById("b_firstname").value;
															document.getElementById("c_lastname").value = document.getElementById("b_lastname").value;
															document.getElementById("c_address1").value = document.getElementById("b_address").value;
															document.getElementById("c_country").value = document.getElementById("b_country").value;
															document.getElementById("c_state").value = document.getElementById("b_state").value;
															document.getElementById("c_city").value = document.getElementById("b_city").value;
															document.getElementById("c_zip").value = document.getElementById("b_zipcode").value;
															document.getElementById("c_alt_number").value = document.getElementById("b_phone").value;
															document.getElementById("c_mobile").value = document.getElementById("b_mobile").value;
															document.getElementById("c_email").value = document.getElementById("b_email").value;
															document.getElementById("s2ci").checked = false;
														}
													}
													function s2c()
													{
														if(document.getElementById("s2ci").checked == true)
														{
															document.getElementById("c_firstname").value = document.getElementById("s_firstname").value;
															document.getElementById("c_lastname").value = document.getElementById("s_lastname").value;
															document.getElementById("c_address1").value = document.getElementById("s_address").value;
															document.getElementById("c_country").value = document.getElementById("s_country").value;
															document.getElementById("c_state").value = document.getElementById("s_state").value;
															document.getElementById("c_city").value = document.getElementById("s_city").value;
															document.getElementById("c_zip").value = document.getElementById("s_zipcode").value;
															document.getElementById("c_alt_number").value = document.getElementById("s_phone").value;
															document.getElementById("c_mobile").value = document.getElementById("s_mobile").value;
															document.getElementById("c_email").value = document.getElementById("s_email").value;
															document.getElementById("b2ci").checked = false;
														}
													}
												</script>
											{/literal}
											<tr><td colspan="2" style="margin-top:15px;"></td></tr>
											<tr>
												<td colspan="2">
													<br><hr color="#999999" size="1"><br>
													<p><strong style="font-size:1.5em">customer details</strong>&nbsp;&nbsp;</p><br/>
												</td>
											</tr>
											<tr>
												<td>
													<p>
														<table>
															<tbody>	
																<tr>
																	<td>
																		<input type="checkbox" name="s2ci" id="s2ci" value="btoship" class="checkbox" onclick="javascript:s2c();" > <b>same as shipping</b>
																	</td>
																</tr>
																<tr>
																	<td>
																		<input type="checkbox" name="b2ci" id="b2ci" value="btoship" class="checkbox" onclick="javascript:b2c();" > <b>same as billing</b>
																	</td>
																</tr>
																<tr>
																	<td>
																		<font style="font-size:0.8em;">first name<span class=mandatory>*</span></font>
																	</td>	
																	<td>
																		<input type="text" name="c_firstname" id=c_firstname />
																	</td>
																</tr>
																<tr>	
																	<td>	
																		<font style="font-size:0.8em;">last name<span class=mandatory>*</span></font>	
																	</td>
																	<td>	
																		<input type="text" name="c_lastname" id=c_lastname />
																	</td>
																</tr>
																<tr>
																	<td>
																		<font style="font-size:0.8em;">address 1</font>
																	</td>
																	<td>
																		<textarea class = address name=c_address1 id=c_address1></textarea>
																	</td>
																</tr>
																<tr>
																	<td>
																		<font style="font-size:0.8em;">country</font>
																	</td>
																	<td>
																		<select name=c_country class=select id=c_country>
																			{foreach key=countrycode item=countryname from=$returnCountry}
								   												<option value="{$countrycode}" >{$countryname}</option>
																			{/foreach}
																		</select>
																	</td>
																	</tr>
																<tr>
																	<td>
																		<font style="font-size:0.8em;">state</font>
																	</td>
																	<td>
																		<select name=c_state class=select id=c_state>
																			{section name="statename" loop=$returnState}
																				<option value={$returnState[statename].code}>{$returnState[statename].state}</option>
																			{/section}
																		</select>
																	</td>
																</tr>
																<tr>
																	<td>
																		<font style="font-size:0.8em;">city</font>
																	</td>
																	<td>
																		<input type="text" name="c_city" id=c_city />
																	</td>
																</tr>
																<tr>
																	<td>
																		<font style="font-size:0.8em;">zip/postal code</font>
																	</td>
																	<td>
																		<input type="text" name="c_zip" id=c_zip />
																	</td>
																</tr>
																<tr>
																	<td>
																		<font style="font-size:0.8em;">phone</font>
																	</td>
																	<td>
																		<input type="text" name="c_alt_number" id=c_alt_number />
																	</td>
																</tr>
																<tr>
																	<td>
																		<font style="font-size:0.8em;">mobile<span class=mandatory>*</span></font>
																	</td>
																	<td>
																		<input type="text" name="c_mobile" id=c_mobile />
																	</td>
																</tr>
																<tr>
																	<td>
																		<font style="font-size:0.8em;">email</font>
																	</td>
																	<td>
																		<input type="text" name="c_email" id=c_email />
																	</td>
																</tr>
															</tbody>
														</table>
													</p>
												</td>
												<td>
													<p>
														<table>
															<tbody>
																<tr>
																	<td>
																		<font style="font-size:0.8em;">&nbsp;</font>
																	</td>
																	<td>
																
																	</td>
																</tr>
																<tr>
																	<td>
																		<font style="font-size:0.8em;">&nbsp;</font>
																	</td>
																	<td>
																	</td>
																</tr>
															</tbody>
														</table>
													</p>
												</td>
											</tr>
										{/if}
										<tr><td colspan="2" style="margin-top:15px;"></td></tr>
										<tr>
											<td colspan="2">
												<br>
												<hr color="#999999" size="1">
												<br>
												<p><strong style="font-size:1.5em">contact number for issues</strong>&nbsp;&nbsp;<input type="text" name="issues_contact_number" id="issues_contact_number" value="{$userinfo.mobile}"></p><br/>
												<p style="font-size:0.8em;color:#F40000;">
													(This number will be used to contact if there are any issues in fulfilling this order. {if $RRtype!=1 }	 If this is a gift, please DO NOT enter the gift recipient number here as it may spoil the surprise! ){/if}
												</p>
											</td>
										</tr>
										<tr><td colspan="2" style="margin-top:15px;"></td></tr>
										
										<!--delivery preference-->
										<input type="hidden" name="shipment_preferences" id="shipment_preferences" value="shipped_together"/>
										<!--delivery preference-->
										
										<!--imint-->
										<tr>
											<td colspan="2">
												<br>	
												<hr color="#999999" size="1">
												<br>
												<p>
													<strong style="font-size:1.5em">earn imint reward points</strong>&nbsp;&nbsp;
													<div style="margin-left:10px;">I want to earn <img style="border:0" src="{$cdn_base}/skin1/mkimages/logo_imint.gif"/> reward points on <input type="text" style="color:grey;" name="imint_card" value="" size="25" /></div>
													<div style="margin:10px 10px;color:red;font-size:0.8em;">* Members of privileges program from Airtel powered by Imint, key in your 10 digit Airtel mobile no. to earn reward points.</div>
												</p>
											</td>
										</tr>
										<!--imint-->	        
									</table>
								<div class="foot"></div>
							</div>
							{if $accountType == 1 || $showShippingOptions eq "0" }
								<input name='s_option' id ='s_option' type="hidden" value ='{$shippingoptions[0].code}'/>
							{else}
							<div class="subhead">
							    <p>choose shipping option</p>
							</div>
			  
							<div class="links">
								<p>
									<select name ='s_option' id='s_option' class=select onchange="javascript:verifyShipOption();javascript:updateShipping();">
										{section name=option loop=$shippingoptions }
											<option value='{$shippingoptions[option].code}'>{$shippingoptions[option].courier_service}</option>
										{/section}
									</select>
									&nbsp;<a href="javascript:view_shipping_detail();">view detail</a>
								</p>
								<div id="rate"></div>
							</div>
							<div class="foot"></div>
							{/if}
							
							<div class="subhead"><p>order summary</p></div>
							<div>
								<table cellpadding="2"  class="table">
									<tr class="font_bold">
										<td class='align_right' colspan="2"  width="60%"><span>amount:</span></td>
										<td class='align_right'>Rs.<span id="amount">{$amount}</span></td>
									</tr>
									<tr class="font_bold">
										<td class='align_right' colspan="2"  width="60%"><span>vat/cst:</span></td>
										<td class='align_right'>Rs.<span id="vat">{$vat}</span></td>
									</tr>
									<!--tr class="font_bold">
										<td class='align_right' colspan="2"  width="60%"><span>MRP:</span></td>
										<td class='align_right'>Rs.<span id="mrp">{$mrp}</span></td>	
									</tr-->
									{if $couponCode}
										<tr class="font_bold">
											<td class='align_right' colspan="2"  width="60%"><span>coupon discount:(coupon code: {$couponCode}({$couponpercent})):</span></td>
											<td class='align_right'>Rs.<span id="coupondiscount">{$coupondiscount}</span></td>
										</tr>
									{/if}
									<tr class='font_bold' id='codrow' >
										<td class='align_right' colspan="2" ><span id='codtext' style='display:none;'>cash on delivery charges:</span></td>
										<td class='align_right'><span id='codcharge' style='display:none;'>Rs.{$cod_charges}</span></td>
									</tr>
									<tr class="font_bold">
										<td class='align_right' colspan="2"  width="60%">
										<span id="freeshipping" style="font-size:12px; color:red; font-weight:bold; text-align:right;">
										{if ($cart_amount) gt $free_shipping_amount}
                                           (FREE Shipping in India)
	                                    {/if}
										</span>
										<span>shipping cost:</span></td>
										<td class='align_right'>Rs.<span id="shippingcost">{$shippingRate}</span></td>
									</tr>
									<tr class="font_bold">
										<td  class="align_left" valign="top">
											<input type="checkbox" class="checkbox" name="asgift" id="asgift" onClick="javascript:messageBlock_re('{$login}');">
											&nbsp;<strong>send as gift</strong>&nbsp;<a href="javascript:messagePopup('gift');" style="text-decoration:none;font-size:0.7em;"><img src="./skin1/mkimages/help-icon.gif" style="border:0px;"/></a>
											<br><textarea name="gmessage" id="messageblock" style="width:300px;visibility:hidden;display:none;" onblur="if(this.value=='') this.value=this.defaultValue;" onfocus="if(this.value==this.defaultValue) this.value='';" >type a gift message here...</textarea>
										</td>
										<td  class="align_right" ><strong>gift packaging charges:</strong></td>
										<input type="hidden" name="giftpack_charges" id="giftpack_charges" value="{$giftpack_charges|string_format:"%.2f"}">
										<input type="hidden" name="giftamount" id="giftamount" value="0.00">
										<td  class="align_right" ><strong>Rs.<span id="giftprice">0.00</span></strong></td>
									</tr>

									<tr class='font_bold' id='codrow' style="color:red">
										<td class='align_right' colspan="2" >total amount to be paid:</td>
										<td class='align_right'>Rs.<span id="totalAmount">{$totalAmount}</span></td>
									</tr>
									<!--<tr>
										<td colspan="3" class="align_right">
											
										</td>
									</tr>-->
								</table>
							</div>
							<input type='hidden' name='cash' id='cash' value="{$cod_charges}">
							<div class="foot"></div>
							<div class="subhead">
								<p>payment options&nbsp;<a href="javascript:messagePopup('payment');" style="text-decoration:none;font-size:0.7em;"><img src="./skin1/mkimages/help-icon.gif" style="border:0px;"/></a></p>
							</div>
							<div class="links" >
							   	 <div style='margin-bottom:5px;margin-left:5px;float:left;width:60%;'><input type="radio" name="payby" id="ebs" class="radio" checked onclick="javascript:verifyShipOption();javascript:updateShipping_re();func_update_gateway();">Pay by Credit Card ( Visa/Master/Diners)</div>
								 <div style='float:left;width:33%;'><input type="radio" name="payby" id="net" class="radio" onclick="javascript:verifyShipOption();javascript:updateShipping_re();func_update_gateway();">&nbsp;Pay by Credit Card (Amex/JCB)</div>							  
							</div>
							<div class="links">
							    <div style='margin-bottom:5px;margin-left:5px;float:left;width:60%;'><input type="radio" name="payby" id="cashcard" class="radio"  onclick="javascript:verifyShipOption();javascript:updateShipping_re();func_update_gateway();">Pay By Cash Card</div>
							    <div style='float:left;width:33%;'><input type="radio" name="payby" id="ebs_debit" class="radio"  onclick="javascript:verifyShipOption();javascript:updateShipping_re();func_update_gateway();">Pay by Debit Card</div>							  
							  </div>
							<div class="links">
							    <div style='margin-bottom:5px;margin-left:5px;float:left;width:60%;'><input type="radio" name="payby" id="netbanking" class="radio"  onclick="javascript:verifyShipOption();javascript:updateShipping_re();func_update_gateway();">Pay by NetBanking 
							   <select size="1" name="netBankingCards">
								<option value="" selected="selected">Select Bank</option>
								   <option value = 'ABN_N'>ABN AMRO Bank</option>  
								   <option value = 'UTI_N'>AXIS Bank</option> 
								   <option value = 'BOBCO_N'>Bank of Baroda Corporate Accounts</option>  
								   <option value = 'BOB_N'>Bank of Baroda Retail Accounts</option>  
								   <option value = 'BOI_N'>Bank of India</option>  
								   <option value = 'BOM_N'>Bank of Maharashtra</option>  
								   <option value = 'BOR_N'>Bank of Rajasthan</option>  
								   <option value = 'CBIBAN_N'>Citibank Bank Account Online</option>  
								   <option value = 'CITIUB_N'>City Union Bank</option>  
								   <option value = 'COP_N'>Corporation Bank</option>  
								   <option value = 'DEUNB_N'>Deutsche Bank</option>  
								   <option value = 'FDEB_N'>Federal Bank</option>  
								   <option value = 'HDEB_N'>HDFC Bank</option>  
								   <option value = 'IDEB_N'>ICICI Bank</option>  
								   <option value = 'IDBI_N'>IDBI Bank</option>  
								   <option value = 'NIIB_N'>IndusInd Bank</option>  
								   <option value = 'ING_N' >ING Vysya Bank</option>
								   <option value = 'JKB_N'>Jammu & Kashmir Bank</option>  
								   <option value = 'KTKB_N'>Karnataka Bank</option>  
								   <option value = 'KVB_N'>Karur Vysya Bank</option>  
								   <option value = 'NKMB_N'>Kotak Mahindra Bank</option>  
								   <option value = 'LVB_N'>Lakshmi Vilas Bank NetBanking</option>  
								   <option value = 'OBC_N'>Oriental Bank of Commerce</option>  
								   <option value = 'PNBCO_N'>Punjab National Bank Corporate Accounts</option>  
								   <option value = 'NPNB_N'>Punjab National Bank Retail Accounts</option>  
								   <option value = 'SIB_N'>South Indian Bank</option>  
								   <option value = 'SCB_N' >Standard Chartered Bank</option>
								   <option value = 'SBI_N'>State Bank of India</option>  
								   <option value = 'SBT_N'>State Bank of Travancore</option>  
								   <option value = 'SYNBK_N'>Syndicate Bank</option>  
								   <option value = 'UNI_N'>Union Bank of India</option>  
								   <option value = 'VJYA_N'>Vijaya Bank</option>  
								   <option value = 'YES_N'>YES Bank</option>
								</select></div>
							   <div style='float:left;width:33%;'><input type="radio" name="payby" id="mobilepayment" class="radio" onclick="javascript:verifyShipOption();javascript:updateShipping_re();func_update_gateway();">&nbsp;Pay by Mobile Payment</div>
							 </div>
							<div class="links">
							  <div id="chq_enable" {if $totalAmount <= $check_option_enable}style="display:none;"{else}style="margin-bottom:5px;margin-left:5px;float:left;width:60%;"{/if}>
									<input type="radio" name="payby" id="ch" class="radio" value="chq" onclick="javascript:verifyShipOption();javascript:updateShipping_re();func_update_gateway();">&nbsp;Pay by cheque/dd &nbsp;(only INR/Rs cheque payable in India)
							  </div>
							</div>
							<input type="hidden" name="is_netbanking" id="is_netbanking" value=''>
							 <input type="hidden" name="gateway" id="gateway" value='ebs'>
							<input type="hidden" name="kotakdiscount" id="kotakdiscount" value="0">
							<input type="hidden" name="optioncount" id="optioncount" value="0">
							<input type="hidden" name="codoptioncount" id="codoptioncount" value="0">
							{if $cod_enabled}
								<div class="links">
									<p>
										<input type="radio" name="payby" id="cod" class="radio" value="cod" onchange="javascript:verifyShipOption();javascript:updateShipping_re();">&nbsp;cash on delivery 
									</p>
								</div>
							{/if}
							<div class="links" align='right'><input type="button" class="submit" value="proceed to payment" style="margin-right:5px;font-weight:bold;font-size:1.1em;width:170px;background:#CB0029;cursor:pointer;" onClick="goToPaymentOption('{$pagetype}',true,false)" /><!--added true, false to show alert on shipping cost 0--></div>
							<input type="hidden" name="codoptioncount" id="codoptioncount" value="0">
							<input type="hidden" id="box_count" value="{$box_count}"/>
							<div class="foot"></div>
							{*******************************************************}                    
							
						</form>
					</div>
				</div>
			</div>
			<div class="clearall"></div>
		</div>
		{include file="footer.tpl" }
	</div>
</div>
<div id="giftpopup" style="visibility:hidden;display:none">
	<div id="overlay"> </div>
	<div id="messagewide">
		<div class="wrapper" style="_top:150px">
			<div class="head"><a href="javascript:hidegiftMessageDiv()" >Click to close</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
			<div class="body"><p>{$giftmess}</p></div>
			<div class="foot"></div>
		</div>
	</div>
</div>

<div id="messagepopup" style="visibility:hidden;display:none">
	<div id="overlay"> </div>
	<div id="message" style="position:relative; left:0px; top:60px;">
		<div class="wrapper">
			<div class="head"><a href="#" onclick="javascript:hideSuccessDiv()">Click to close</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
			<div class="body">
				<p><strong>Shipping cost has been updated to reflect the </p>
				<p>
					<strong>change in your shipping address</strong>
					<br>
					<br>
				</p>	
			</div>
			<div class="foot"></div>
		</div>
	</div>
</div>


<div id="paymentoption" style="visibility:hidden;display:none">
	<div id="overlay"> </div>
	<div id="messagewide">
		<div class="wrapper" style="_top:150px">
			<div class="head"><a href="javascript:hidepaymentMessageDiv()" >Click to close</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
			<div class="body">
				<p>
					<table style="font-size: 1.0em;font-weight:100;" border=0 width="95%">
						<tr><td><p><strong>net banking / credit card / itz cash card / paymate:</strong></p></td></tr>
						<tr><td><p><b>-</b> use your net enabled bank account from any major bank to pay directly from your bank account</p></td></tr>
						<tr><td><p><b>-</b> use your VISA, Diners or American Express credit card to make the payment</p></td></tr>
						<tr><td><p><b>-</b> If you have a paymate account, you can simply make the payment using your mobile phone number.</p></td></tr>
						<tr><td><p><b>-</b> using cash card option, you can use your pre-paid ITZ CASH card.</p></td></tr>		
						<tr><td><p><strong>check:</strong> you can pay by check. Select this option, complete the order and mail the check along with the order receipt.</p></td></tr>
						{if $cod_enabled}<tr><td><p><strong>cash on delivery:</strong> If you select this method, we will ship your order directly to you and the payment will be collected from you at the time of delivery.</p></td></tr>{/if}
					</table>
				</p>
			</div>
			<div class="foot"></div>
		</div>
	</div>
</div>
<div id="navtxt" class="navtext" style="position:absolute; top:-100px; left:0px; visibility:hidden"></div>
</body>
</html>
