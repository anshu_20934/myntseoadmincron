function showImageSelection(div, offsetx,offsety,pagenum){

	//alert(URL);
	var divPopUp= new PopupWindow(div);
	var anchor = null;
	var req = null; 
	if(window.XMLHttpRequest) {
		req = new XMLHttpRequest(); 
	  } else if (window.ActiveXObject) {
			req  = new ActiveXObject("Microsoft.XMLHTTP"); 
	  }

	  req.onreadystatechange = function() { 
		if(req.readyState == 4) 
		{ 
		  if(req.status == 200) 
		  { 

			var response = req.responseText;
			document.getElementById("ajaxStatus").value = "1"
			var elm = document.getElementById(div);
			elm.innerHTML = response;
			divPopUp.offsetX= offsetx;
			divPopUp.offsetY=offsety; 
			divPopUp.showPopup(anchor);
		  }
		}
	   }
	  
     req.open("GET", "myUploadedImages.php?page="+pagenum, true); 
	 
     req.setRequestHeader("Content-Type", "text/xml");
     req.send(null);
   
}


/********************************************************************************/
function custAreaImages(div,offsetx,offsety,productid)
{
   var status = document.getElementById("ajaxStatus").value;	
   if(status == 0)
   {	
   	  showCustArea(div,offsetx,offsety,productid)
   }
   else
   {
   	  document.getElementById("popup").style.visibility='visible';
   }
	
}

function showCustArea(div,offsetx,offsety,productid){

	var divPopUp= new PopupWindow(div);
	var anchor = null;
	var req = null; 
	if(window.XMLHttpRequest) {
		req = new XMLHttpRequest(); 
	  } else if (window.ActiveXObject) {
			req  = new ActiveXObject("Microsoft.XMLHTTP"); 
	  }

	  req.onreadystatechange = function() { 
		if(req.readyState == 4) 
		{ 
		  if(req.status == 200) 
		  { 

			var response = req.responseText;
			document.getElementById("ajaxStatus").value = "1"
			var elm = document.getElementById(div);
			elm.innerHTML = response;
			divPopUp.offsetX= offsetx;
			divPopUp.offsetY=offsety; 
			
			divPopUp.showPopup(anchor);
		  }
		}
	   }
	   
     req.open("GET", "mkAllAreaImage.php?productid="+productid, true); 
	 req.setRequestHeader("Content-Type", "text/xml");
     req.send(null);
   
}
/***************************************************************************************/

function hideImageSelection(id){
    var x = document.getElementById(id);
    x.style.visibility = "hidden";
 
}
function getImagesFromLibrary(div,offsetx,offsety,pagenum)
{
   var status = document.getElementById("ajaxStatus").value;	
   if(status == 0)
   {	
	  showImageSelection(div, offsetx,offsety,pagenum)
   }
   else
   {
   	  document.getElementById("popup").style.visibility='visible';
   }
}



