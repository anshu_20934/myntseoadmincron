<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html xml:lang="en" xmlns:fb="http://www.facebook.com/2008/fbml">
<head>
    
    <META http-equiv="Content-Style-Type" content="text/css">
    <META http-equiv="Content-Script-Type" content="type/javascript">
    <META http-equiv="Content-Type" content="text/html; charset=utf-8">
	{include file="site/header.tpl" }
    <!--[if lt IE 8]>
    <link rel="stylesheet" href="new-skin-css/ie.css" type="text/css" media="screen, projection"><![endif]-->
    <link rel="canonical" href="{$http_location}">

</head>

<body  class="{$_MAB_css_classes}">
{include file="site/menu.tpl"}
<div class="container clearfix">
    <div>
    	<table cellpadding="2" class="printtable">
        	<tr> 
            	<td>
                <p align="center">
					<strong>{$errormessage}</strong>
				</p>
				</td>
          </tr>
        </table>
    </div>
</div>
{include file="footer.tpl" }

</body>
</html>