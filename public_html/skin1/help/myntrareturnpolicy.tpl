<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html  xml:lang="en" xmlns:fb="http://www.facebook.com/2008/fbml">
	<head>
		<META http-equiv="Content-Style-Type" content="text/css">
		<META http-equiv="Content-Script-Type" content="type/javascript">
		<META http-equiv="Content-Type" content="text/html; charset=utf-8">

		{include file="site/header.tpl" }
		
		{literal}
		<style type="text/css">
		    h5{font-family:Helvetica, Arial, sans-serif;font-size:13px;font-weight:bold;color:#333;margin:10px 0 5px 0;}
		    p{margin:0 0 1em;}
		    ol li{margin-left:20px;margin-bottom:10px;}
		    a:hover{text-decoration:underline}
		</style>
		{/literal}

	</head>
	<body class="{$_MAB_css_classes}">
		{include file="site/menu.tpl" }
	    <div class="container clearfix">
	    	<h2 class="h4">Myntra's 30 Day Returns Policy</h2>

		    <div>
		    	We would like you to have complete peace of mind when buying products from Myntra. You can return all items <strong>except innerwear, socks, deodorants, perfumes and free items</strong> within 30 days of receipt for a full refund of the item purchased. This includes items purchased on sale. The refund amount will be credited back into your My Myntra account <strong>in the form of cashback</strong>.
		    </div>
		    <ul>
				<li style="list-style-type: square; margin: 5px 0px;">All items to be returned must be <strong>unused and in their original condition</strong> with all original tags and packaging intact (for e.g. shoes must be packed in the original shoe box as well and returned).</li>
				<li style="list-style-type: square; margin: 5px 0px;">Please do take care that that you pack items securely and safely to prevent any loss or damage during transit. For all self shipped returns, we recommend you use a reliable courier service.</li>
				<li style="list-style-type: square; margin: 5px 0px;">Once your return is received by us, you will be reimbursed Rs 100 towards shipping costs if self shipped. This is subject to your return having met the requirements of being unused, returned within 30 days and not consisting of innerwear, socks, deodorants, perfumes or free items.</li>
			</ul>
			<p>For specific queries, please read the FAQs below.</p> 

		    <h2 class="h4">FAQs</h2>

		    <h5>How do I return products to Myntra?</h5>
		    <div>
		    	<p>It's a simple step by step process</p>
				<p><strong>Step 1a</strong>: You can place a Return Request by using our interactive Returns Self-Service Wizard. Login to your <a href="/mymyntra.php" target="_blank">My Myntra</a> account and go to the <a href="/mymyntra.php?view=myorders" target="_blank">Orders</a> section. In the 'Completed Orders' table, click on the order and you will be shown the order details where you will find a 'Return Item' link for each of the items in your order. Click on this link to open the Returns Self-Service Wizard and fill in the following information:</p>
				<ul>
					<li>Select a Reason and Quantity for returns. Additionally, you can also provide any other details you want to convey to us. Click on 'Next'.</li>
					<li>Please read and confirm your acceptance of the terms of our Returns Policy agreement.</li>
					<li>Select your return shipment mode as either Self-Shipping or Myntra Pickup. If you select the return mode as Myntra Pickup, then on clicking 'Next' you will be asked to select an address for pickup. You will only be able to select those addresses which are serviceable for pickups. Alternatively, you can create a new address for pickup provided it is serviceable.</li>
					<li>The Summary displays your return details as well as your refund amount. Click on 'Submit' once you have verified all the information. Your Return Request has been created.</li>
				</ul>
				<p><strong>Please note your Return Request Number for future reference.</strong></p>
				<p style="text-align:center;"><strong>OR</strong></p>
				<p><strong>Step 1b</strong>: Alternatively, you can get in touch with our <a href="contactus" target="_blank">Customer Support</a> to place your returns request. After guiding you through the process, our Customer Support Executive will inform you whether we can organize a pickup of your return or if you would be required to arrange for shipment of your returns yourself. Our executive will also provide you with a returns request number.</p>
				<p><strong>Step 2</strong>: Please fill out the Returns Request Form shipped along with your original order with the details provided by our Customer Support Executive. In fact, we have made it easier for you by making a printable version of the Return Request form available for download from the Returns Self-Service Wizard while creating the Return Request, or from the <a href="/mymyntra.php?view=myreturns" target="_blank">Returns</a> section of My Myntra. The form becomes available as soon as you place the Return Request with us and is already filled in with your Return Request details. This helps us process your returns faster.</p>
				<p><strong>Step 3</strong>: Put the Returns Form and the item(s) you wish to return inside the plastic returns bag you received with the original order. Pull off the tape protecting the tamper-proof adhesive seal and seal the package in one smooth action. Inscribe the returns address on the package and self-ship it or hand it over to our pickup team.</p>
				<p><strong>Note:</strong></p>
				<ol>
					<li>In case you have opted for Myntra to organize the pickup of your return, our logistics team will organize the pickup within 24-48 hours of placement of the pickup request. Please do ensure your availability and shipment readiness for handing over the return to the pickup team. On pickup, you will receive a receipt with the tracking number of your returns consignment. Please retain this till we confirm receipt of your consignment.</li>
					<li>If your pincode is not covered by our pickup services, you would need to self-ship the return item. For all self-shipped returns, you will be reimbursed Rs 100 towards your shipping costs. This is subject to your returns being inspected upon receipt at our end.</li>
				</ol>
				<p>In case you are self-shipping your returns back to us, please retain your shipping receipt and update the tracking details on My Returns so as to enable us to track the delivery of your returns to us.</p>
		    
		    </div>

		    <h5>What can I return?</h5>
			<p>You can return products ordered from Myntra within 30 days of delivery. Returned products must be unused and unwashed, and returned with all original packaging and tags that they were shipped with.</p>
			<p>To ensure that your returns are accepted by our Quality team, please ensure you take adequate measures to ensure your product trial does not affect the original condition of the products in any way. Therefore, it is recommended that you try on your apparel indoors and footwear on a clean surface, preferably carpeted as soon you receive them.</p>

		    <h5>What locations are covered for pickups?</h5>
			<p>Returns pickups are currently supported in Bangalore and Delhi. We are constantly endeavouring to increase coverage of locations where pickup service is available. You can find out whether your location is covered by contacting our <a href="contactus" target="_blank">Customer Connect</a> team in case you wish to have your returns picked up.</p>

		    <h5>Where do I send my return?</h5>
			<p>Please make sure that the following address is clearly printed/written on your returns parcel in cases of both pickup and self shipping:</p>
			<p>The Returns Desk<br />
			Myntra Designs Pvt Ltd.<br />
			7th Mile, Krishna Reddy Industrial Area, Kudlu Gate<br />
			Opp Macaulay High School, behind Andhra Bank ATM<br />
			Bangalore - 560068<br />
			Phone: 080-49023100</p>

		    <h5>How long will the entire process take?</h5>
			<p>It can take up to 14 days for us to receive your return, depending on your location and the courier service you use. Once we receive your returned item, we will inspect and process the same within 48 hours to ensure you receive your refund as quickly as possible.</p>
			<p>So that you are kept informed, we will send you an email confirming the action we have taken, as soon as we are confident the goods are in their original condition and the refund has been allocated to your Mynt Credits.</p>
			<p>You can track your returns through the various stages of processing on the <a href="mymyntra.php?view=myreturns" target="_blank">My Returns</a> page.</p>
            

            <h5>Do I get charged for returning products?</h5>
			<p>We do not charge any fees for pickups arranged for, and serviced by us.</p>
			<p>If you are required to ship your returns back to us yourself, a shipping cost of Rs 100 would be reimbursed as part of your return refund amount as compensation for the inconvenience.</p>

            <h5>How will I be refunded for the return?</h5>
			<p>On successful processing refund, the refund amount of item value plus self-shipping reimbursement (if you have shipped your return back yourself) will be credited to your Myntra cashback account. You will be able to use this credit on any of your subsequent purchases on Myntra.</p>

			<h5>I received a damaged product!</h5>
			<p>This happens very rarely. But if you do receive a damaged product, you can return the product to us and we will refund the amount you paid, into your Myntra cashback account. You can send the images of the product at <a href="mailto:returns@myntra.com">returns@myntra.com</a> and we will contact you. You can return the product, for which you will get the refund in your Mynt Credits Cashback account and you can place a fresh order. We don't replace any products, we only give refunds.</p>
			
		</div>
        <div class="divider">&nbsp;</div>
       	{include file="site/footer.tpl"}
	</body>
</html>
