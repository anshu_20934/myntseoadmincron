<table width="100%" cellpadding="0" cellspacing="0" align="center">
<tr><td>
<b>What do you need to do for successful importing? </b>
<p />
FedEx rate estimation in X-cart is based on rate tables, so before using rate estimation for FedEx you need to download rate tables from FedEx site and import them in X-cart.
<p />
First you need to obtain the following files from FedEx:

<ol>
<li>Download .csv file for the following services from <a href="http://www.fedex.com/us/rates/downloads/" target="_new"><b>http://www.fedex.com/us/rates/downloads/</b></a>
<p />
First Overnight<br />
Priority Overnight<br />
Standard Overnight<br />
2Day<br />
Express Saver<br />
International Economy<br />
International Priority<br />
US to Puerto Rico<br />
Ground<br />
Home Delivery<br />
<p />
<li>Please make sure you have entered your origination zipcode at the "General settings -&gt; Company options" section. After that please download the ASCII zone chart by clicking the button:
<p />
<form action="http://www.fedex.com/cgi-bin/regionlocatortxt.cgi" method="get" target="_new">
<table>
<tr>
	<td><b>Enter your zip code:</b></td>
	<td><input type="text" size="10" maxlength="7" name="zipcode" value="{$config.Company.location_zipcode}" /></td>
	<td><input type="submit" value="Go" /></td>
</tr>
</table>
</form>
<p />
or use the link below:
<p />
<table>
<tr>
	<td><a href="http://www.fedex.com/cgi-bin/regionlocatortxt.cgi?zipcode={$config.Company.location_zipcode}" target="_new"><b>http://www.fedex.com/cgi-bin/regionlocatortxt.cgi?zipcode=&lt;YOUR_ZIP_CODE&gt;</b></a></td>
</tr>
</table>
</ol>
<p />
Put all these files on your server where X-cart is installed in the directory specified in the  'Server path to files' parameter. Then press 'Import FedEx Rates' button. Please do not forget to update rate tables and  'Fuel surcharges' regulary to keep it up to date.
</td>
</tr>
</table>
