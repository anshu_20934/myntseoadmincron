<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html  xml:lang="en" xmlns:fb="http://www.facebook.com/2008/fbml">
    <head>
        <META http-equiv="Content-Style-Type" content="text/css">
        <META http-equiv="Content-Script-Type" content="type/javascript">
        <META http-equiv="Content-Type" content="text/html; charset=utf-8">

        {include file="site/header.tpl" }
        
        {literal}
        <style type="text/css">
            h5{font-family:Helvetica, Arial, sans-serif;font-size:13px;font-weight:bold;color:#333;margin:10px 0 5px 0;}
            p{margin:0 0 1em;}
            ol li{margin-left:20px;margin-bottom:10px;}
            a:hover{text-decoration:underline}
        </style>
        {/literal}

    </head>
    <body class="{$_MAB_css_classes}">
        {include file="site/menu.tpl" }
        <div class="container clearfix">
            <h2 class="h4">Myntra's Shipping Policy</h2>

            <div>
                We strive to deliver products purchased from Myntra in excellent condition and in the fastest time possible. 
                For all purchases above Rs. {$free_shipping_amount}/- we will deliver the Order to your doorstep FREE of cost. 
                A shipping charge of Rs. {$shippingCharge}/- will be applicable to all Orders under Rs. {$free_shipping_amount}/-. Read on to know more.
            </div>
            <ul>
                <li style="list-style-type: square; margin: 5px 0px;">If the Order is cancelled, lost or un-delivered to your preferred location, we will refund the complete Order amount including shipping charges.</li>
                <li style="list-style-type: square; margin: 5px 0px;">If you cancel part of the Order, shipping charges will not be refunded.</li>
                <li style="list-style-type: square; margin: 5px 0px;">If you return an Order delivered to you, original shipping charges will not be refunded. However if you self-ship your Returns , you will be reimbursed based on Myntra's <a href="myntrareturnpolicy.php">Return Policy</a>.</li>
            </ul>

            <h2 class="h4">FAQs</h2>

            <h5>What are the shipping charges on Myntra products?</h5>
            <p>
                Myntra offers free shipping within India on all products if your total Order amount is Rs. {$free_shipping_amount}/- or more. Otherwise Rs. {$shippingCharge}/- will be levied as Shipping Charges.                
            </p>
        </div>
        <div class="divider">&nbsp;</div>
        {include file="site/footer.tpl"}
    </body>
</html>