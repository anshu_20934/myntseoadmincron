<table width="100%" cellpadding="0" cellspacing="0" align="center">
<tr><td>
<b>Merchant account types description</b>
<p />
UPS has several classifications based on your account-type choice:
<p />
<i>Daily Pickup</i> - a UPS driver will come to your location each day (weekly service charge applies)
<p />
<i>Occasional Shipper</i> - It is the responsibility of the merchant to drop the package into the UPS System
<p />
<i>Suggested Retail Rate</i> - Paying for the shipments at the UPS Store (Label is actually created by the UPS Store)
</td>
</tr>
</table>
