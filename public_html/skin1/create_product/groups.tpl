<div class="form" style="border-bottom: 1px solid #999999; margin-bottom:10px;">
        <div class="subhead">
		   <h1 style='margin-left:4px;font-size:20px;'>{$tableHeading}</h1>
		 </div>
		 <div class="text">
		     {section name=product loop=$product_groups }
		        {if (($smarty.section.product.index ) gt 0 && ($smarty.section.product.index )  % 3 eq 0)}
                    <div class="dottedLine"></div>
                {/if}
                 <div class="productGroupBox">
                    <a href="{$http_location}/{$product_groups[product].url}{if $qryStringKey  ne ''}?{$qryStringKey}={$qryStringValue} {/if}">
                        <img src="{$cdn_base}/skin1/images/spacer.gif" lazy-src="{$product_groups[product].image}" class="lazy" alt="{$product_groups[product].name}" id="im_{$smarty.section.product.index}">                         
                    </a>
                        <div class="productGroupDetails">
                        <div class="productGroupName" align="center">
                         <a href="{$http_location}/{$product_groups[product].url}{if $qryStringKey  ne ''}?{$qryStringKey}={$qryStringValue} {/if}">
                            {$product_groups[product].name}
                            </a>
                        </div>
                        </div>
                </div>
            {/section}
    </div>
</div>