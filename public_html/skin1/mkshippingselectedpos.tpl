{if $shopMasterId != 41}
{foreach key=key item=item from=$userinfo}
    {if $key eq 's_firstname'}
    {assign var='firstname' value=$item}
    {/if}
    {if $key eq 's_lastname'}
    {assign var='lastname' value=$item}
    {/if}

    {if $key eq 's_address'}
    {assign var='address' value=$item}
    {/if}
    {if $key eq 's_city'}
    {assign var='city' value=$item}
    {/if}
    {if $key eq 's_country'}
    {assign var='country' value=$item}
    {/if}
    {if $key eq 's_zipcode'}
    {assign var='zipcode' value=$item}
    {/if}

    {if $key eq 'phone'}
    {assign var='phone' value=$item}
    {/if}
     {if $key eq 'mobile'}
    {assign var='mobile' value=$item}
    {/if}
    {if $key eq 'email'}
    {assign var='email' value=$item}
    {/if}

    {if $key eq 's_state'}
    {assign var='state' value=$item}
    {/if}

{/foreach}
{/if}
{assign var="bill" value="billing"}

<table>


	<tr>
		<td>
 			<font style="font-size:0.8em">first name</font><span class="mandatory">*</span>
 		</td>
 		<td >
 			<input type="text" name="s_firstname" id="s_firstname" value="{$firstname}"  >
 			<input type="hidden" name="h_firstname" id="h_firstname" value="{$firstname}"  >
 		</td>
 	</tr>

 	<tr>
		<td >
 			<font style="font-size:0.8em">last name</font><span class="mandatory">*</span>
 		</td>
 		<td >
 			<input type="text" name="s_lastname" id="s_lastname" value="{$lastname}"  >
 			<input type="hidden" name="h_lastname" id="h_lastname" value="{$lastname}"  >
 		</td>
 	</tr>

 	<tr>
		<td valign="top">
 			<font style="font-size:0.8em">address</font><span class="mandatory">*</span>
 		</td>
 		<td >
 		<textarea name="s_address" id="s_address" class="address" onKeyPress=check_length("s_address"); onKeyDown=check_length("s_address");  >{$address}</textarea><br>
 		<input type="hidden" name="h_address" id="h_address" class="address" value="{$address}">
            <input type=hidden size=2 value=255 name="text_num" id="text_num">

 		</td>
 	</tr>

	

 	 <tr>
		<td >
 			<font style="font-size:0.8em">country</font><span class="mandatory">*</span>
 		</td>
 		<td >
 			<input type="hidden" name="h_country" id="h_country" value="{$countryname}">
 			<select name="s_country" id="s_country" class="select" {if $ship2diff == 'Y'} readonly {/if}  onchange="javascript:onchange_country('s_country');">
 {foreach key=countrycode item=countryname from=$returnCountry}

 				
  {if $countrycode == $billing_country }
 	 <option value="{$countrycode}" selected>{$countryname}</option>
 	{else}
   <option value="{$countrycode}" >{$countryname}</option>
 	{/if}
 {/foreach} 					
</select>


 		</td>
 	</tr>

 	
 	<tr>
		<td >
 			<font style="font-size:0.8em">state</font><span class="mandatory">*</span>
 		</td>
 		<td >
 		<div id="s_state_list">
 		{ if $returnState != ''}
 			<input type="hidden" name="h_state" id="h_state" value="{$s_state}">
 			<select name="s_state" id="s_state" class="select"  >
			<option value="" selected>--Select State--</option>
 			{section name="statename" loop=$returnState}

 				{if $returnState[statename].code == $s_state}
 				<option value={$returnState[statename].code} selected>{$returnState[statename].state}</option>
 				{else}
 				<option value={$returnState[statename].code}>{$returnState[statename].state}</option>
 				{/if}
 			{/section}
 			</select>
 			
 			{else}
 			<input type="test" id="s_state" value="{$s_state_name}" >
 			{/if}
 			</div>
 		</td>
 	</tr>
</div>
   <tr>
		<td >
 			<font style="font-size:0.8em">city</font><span class="mandatory">*</span>
 		</td>
 		<td >
 			<input type="text" name="s_city" id="s_city" value="{$city}"  >
 			<input type="hidden" name="h_city" id="h_city" value="{$city}"  >
 		</td>
 	</tr>
 	
 	<tr>
		<td >
 			<font style="font-size:0.8em">zip/postal code</font><span class="mandatory">*</span>
 		</td>
 		<td >
 			<input type="text" name="s_zipcode" id="s_zipcode" value="{$zipcode}" maxlength="6" size="7"  >
 			<input type="hidden" name="h_zipcode" id="h_zipcode" value="{$zipcode}" maxlength="6" size="7"  >
 		</td>
 	</tr>

	<tr>
		<td >
 			<font style="font-size:0.8em">phone</font>
 		</td>
 		<td >
 			<input type="text" name="s_phone" id="s_phone" value="{$phone}"  >
 			<input type="hidden" name="h_phone" id="h_phone" value="{$phone}"  >
 		</td>
 	</tr>

	<tr>
		<td >
 			<font style="font-size:0.8em">mobile</font><span class="mandatory">*</span>
 		</td>
 		<td >
 			<input type="text" name="s_mobile" id="s_mobile" value="{$mobile}"  >
 			<input type="hidden" name="h_mobile" id="h_mobile" value="{$mobile}"  >
 		</td>
 	</tr>

 	<tr>
		<td >
 			<font style="font-size:0.8em">email</font><span class="mandatory">*</span>
 		</td>
 		<td >
 			<input type="text" name="s_email" id="s_email" value="{$email}"  >
 			<input type="hidden" name="h_email" id="h_email" value="{$email}"  >
 		</td>
 	</tr>

{if $accountType != 1}
    <td colspan="2">
        <input type="checkbox" name="updatebill" id="updatebill" value="upbill" class="checkbox"  >
        &nbsp;<font style="font-size:0.8em">update billing address in my account</font>

    </td>
{/if}
  </tr>
 	</table>
