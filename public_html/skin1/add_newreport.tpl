<html>
<head>
{literal}
<script Language="JavaScript" Type="text/javascript">
		function FrontPage_Form1_Validator(theform)
		{
         //alert("Please Generate a valid sku first");
         var reportname=theform.reportname;
         var query=theform.query;
         var mailto=theform.mailto;
          if (reportname.value==null||reportname.value == "")
		  {
			alert("Please enter the report name");
			reportname.focus();
			return false;
		  }
		  if (query.value==null||query.value == "")
		  {
			alert("Please write report query ");
			query.focus();
			return false;
		  }
		  if (mailto.value==null||mailto.value == "")
		  {
			alert("Please give the email address to which this report shall be E-mailed ");
			mailto.focus();
			return false;
		  }
		  return true;
		  
		}
function addfilter(opts){
	var optstring = opts;
	var ftable = document.getElementById("filtertable");
	ftable.style.visibility='visible';
	var rowlength = ftable.rows.length;
	var rowelement = ftable.insertRow(rowlength);
	rowelement.setAttribute("id",rowlength);
	var tdElement = document.createElement("td");
	var newElement = document.createElement("div");
	//newElement.innerHTML = "<td><input type='text' name='fname[]'></td><td>&nbsp;</td><td><select name='ftype[]' id='"+"s"+rowlength+"' onchange='javascript:showopt("+rowlength+");'>"+optstring+"</select></td><td>&nbsp;</td><td id="+"z"+rowlength+"><input type='hidden' name='fopt[]' value=''/></td><td>&nbsp;</td><td><input type='button' value='remove filter' onclick='deleterow("+rowlength+")'></td>";
	newElement.innerHTML = "<input type='text' name='fname[]'>&nbsp;<select name='ftype[]' id='"+"s"+rowlength+"' onchange='javascript:showopt("+rowlength+");'>"+optstring+"</select>&nbsp;<input type='text' name='fdefault[]'>&nbsp;<span id="+"z"+rowlength+"><input type='hidden' name='fopt[]' value=''/></span>&nbsp;<input type='button' value='remove filter' onclick='deleterow("+rowlength+")'>";
	tdElement.appendChild(newElement);
	rowelement.appendChild(tdElement);
}
function showopt(id){
	var sid = "s"+id;
	var zid = "z"+id;
	var selement = document.getElementById(sid);
	var zelement = document.getElementById(zid);
	if(selement.value=="SPL"){
		zelement.innerHTML="<input type='text' name='fopt[]' style='width:200px;' value='enter comma seperated list here'/>";
	}
	else if(selement.value=="DPL"){
		zelement.innerHTML="<input type='text' name='fopt[]' style='width:200px;' value='enter query to get dynamic list'/>";
	}
	else{
	zelement.innerHTML="";
	}
}
function deleterow(id){
	var ftable = document.getElementById("filtertable");
	var row = document.getElementById(id);
	var index = row.rowIndex;
	var rows = ftable.rows.length;
	ftable.deleteRow(index);
	if(rows==2){
	ftable.style.visibility='hidden';
	}
}		
		</script>
{/literal}

<link rel="stylesheet" href="../skin1/mykriti.css"/>

</head>
<body id="body">
<div id="scroll_wrapper">
	<div id="container"> 
	    <div id="display"> 
	      <div class="center_column"> 
	        <div class="print"> 
		        <div class="super" > 
	           		 <p>&nbsp;</p>
	          	</div>
	          	{if $commentSuccess ne "Y"}
	          	<div class="head" style="padding-left:15px;"> 
		            <p>&nbsp;&nbsp;Add New Report</p>
          		</div>
          		<div class="links"><p></p></div>
          		<div class="foot"></div>
          		
<form action="add_new_report.php" method="post" onsubmit="return FrontPage_Form1_Validator(this)" language="JavaScript" name="reportaddform">
<input type="hidden" name="addreport" value ="addreport"/>
<input type="hidden" name="hid" value="{$helpid}" />

<table>

    <tr>
         <td style="text-align:left;">Report Name</td>
         <td style="text-align:left;"><input type="text" name="reportname" /></td>
    </tr>
    <tr>
    <td style="text-align:left;">Date Added</td>
		<td style="text-align:left;"><span style="font-size:.9em; color:blue;">{$date}</span></td>
		</tr>
	<tr>
         <td colspan="2">&nbsp;</td>        
    </tr>
<tr>
         <td style="text-align:left;">E-Mail To</td>
         <td style="text-align:left;"><input type="text" style="width:350px;" name="mailto" /></td>
    </tr>
	<tr>
         <td style="text-align:left;">Report Group</td>
         <td>
		 <select name="reportgroup">
		 {foreach from=$group key=gid item=gname}
				<option value="{$gid}">{$gname}</option>
		{/foreach}
		</select>
		</td>
    </tr>

	<tr>
         <td colspan="2">&nbsp;</td>        
    </tr>
    
    <tr>
         <td style="text-align:left;">Report Type</td>
         <td style="text-align:left;"><select name="reporttype"> <option value="Q"> Query </option><option value="SP"> Stored Procedure </option> </select><span style="font-size:8pt"> Note : If stored procedure, make sure you have added the stored proc</span></td>
    </tr>
    <tr>
    <tr>
         <td>Query</td>
         <td><textarea name="query" cols=50 rows=5></textarea><div style="font-size:8pt"> Note : Incase of stored procedure, just add <span style="color:GREEN">call &lt;stored-proc name&gt;</span></div></td>
    </tr>
    <tr>
         <td>Sum Query (optional)</td>
         <td><textarea name="sumquery" cols=50 rows=5></textarea></td>
    </tr>

	<tr>
         <td colspan="2">&nbsp;</td>         
	</tr>

	<tr>
		<td colspan="2">
			<input type="button" value="addfilter" onclick="javascript:addfilter('{$optstring}');" />	    
		</td>
	</tr>

	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>

	<tr>
		<td colspan="2">
			<table id="filtertable" style="visibility:hidden; border:1px solid black;">
				<tr>
					<th style="text-align:left;">Filter Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Data Type&nbsp;&nbsp;&nbsp;&nbsp;
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Default Value<th>
				</tr>
			</table>
		</td>
	</tr>

	<tr>
		<td><input type="hidden" name="count" value=0><input type="submit" value="create report" /></td>
		<td><input type="reset" value="cancel" onclick="javascript:window.location='../admin/admin_reports.php';" /></td>
	</tr>

</table>
</form>

		{elseif $commentSuccess eq "Y"}
		<table>
				<tr>
					<td>{$commentmsg1}</td>
					<td>&nbsp;&nbsp;</td>
					<td>&nbsp;&nbsp;</td>
				</tr>
				<tr>
					<td style="font-size:.8em;color:red;"><b>{$commentmsg2}</b></td>
				</tr>
				<tr>
					<td align="center"><input type="button" style="height:20px;width:50px; background-color:black; color:white;" value="Close" onclick="opener.window.location.reload();self.close()"></td>
				</tr>
		</table>
		{/if}
		</div> 
	      </div>   	
	     </div> 
	   </div> 
	   </div>
	</body> 
	</html>
    	
