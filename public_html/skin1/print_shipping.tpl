{* $Id: featured_products.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
 <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
{config_load file="$skin_config"}

{foreach key=key item=item from=$userinfo}
   {if $key eq 's_firstname'}
    {assign var='firstname' value=$item}
    {/if}
    {if $key eq 's_lastname'}
    {assign var='lastname' value=$item}
    {/if}
    {if $key eq 's_address'}
    {assign var='address' value=$item}
    {/if}
    {if $key eq 's_city'}
    {assign var='city' value=$item}
    {/if}
    {if $key eq 's_country'}
    {assign var='country' value=$item}
    {/if}
    {if $key eq 's_zipcode'}
    {assign var='zipcode' value=$item}
    {/if}

    {if $key eq 'phone'}
    {assign var='phone' value=$item}
    {/if}
    {if $key eq 'mobile'}
    {assign var='mobile' value=$item}
    {/if}
    {if $key eq 'email'}
    {assign var='email' value=$item}
    {/if}
    {if $key eq 's_state'}
    {assign var='state' value=$item}
    {/if}

{/foreach}

<html>
{include file="mykritimeta.tpl" }
<body id="body">
  <div id="scroll_wrapper">
	<div id="container"> 
	  <div id="wrapper"> 
	    <div id="display"> 
	      <div class="center_column"> 
	        <div class="print"> 
		        <div class="super" > 
	           		 <p>&nbsp;</p>
	          	</div>
	          	
	          	<div class="head" style="padding-left:15px;"> 
		            <p>&nbsp;&nbsp;Shipping Address &nbsp;(#Order Id : {$orderid})</p>
          		</div>
          		<div class="foot"></div>
          		 <div style="padding-left:15px;"> 
          		    <table cellpadding="2" class="printtable">
          		       <tr> 
               
		                <td  class="align_left"><strong>Customer Name:&nbsp;</strong></td>
		                <td class="align_left">{$firstname}&nbsp;{$lastname}</td>
		               </tr>
		               <tr>
		                <td   class="align_left"><strong>Address:&nbsp;</strong></td>
		                <td class="align_left">{$address}</td>
		
		              </tr>
		               <tr>
		                <td   class="align_left"><strong>City:&nbsp;</strong></td>
		                <td class="align_left">{$city}</td>
		
		              </tr>

			       <tr>
		                <td   class="align_left"><strong>State:&nbsp;</strong></td>
		                <td class="align_left">{$state}</td>
		
		              </tr>
		              
		               <tr>
		                <td   class="align_left"><strong>Country:&nbsp;</strong></td>
		                <td class="align_left">{$country}</td>
		
		              </tr>
		              
		              <tr>
		                <td   class="align_left"><strong>Zip/Postal Code</strong></td>
		                <td class="align_left">{$zipcode}</td>
		
		              </tr>
		              <tr>
		                <td   class="align_left"><strong>Mobile:&nbsp;</strong></td>
		                <td class="align_left">{$mobile}</td>
		
		              </tr>
		              
		              <tr>
		                <td   class="align_left"><strong>Phone:&nbsp;</strong></td>
		                <td class="align_left">{$phone}</td>
		
		              </tr>
		              
		              <tr>
		                <td   class="align_left"><strong>Email:&nbsp;</strong></td>
		                <td class="align_left">{$email}</td>
		
		              </tr>
		               <tr><td colspan="2">
		             &nbsp;</td>
		              <tr><td colspan="2">
		              <input class="submit" type="button" name="submitButton" value="print shipping address" border="0" onClick="javascript: window.print();"></td>
		              
          		    </table>
          		 </div>
          		
	          	
	         </div> 
	      </div>   	
	     </div> 
	   </div> 
	 </div>  
	</div>
     {include file="site/javascript.tpl"}
</body>
</html>    	


