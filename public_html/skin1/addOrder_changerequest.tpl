<html>
<head>
{literal}
<script type="text/javascript" src="../skin1/js_script/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="../skin1/js_script/jquery.autocomplete.js"></script>
<link rel="stylesheet" href="../skin1/skin1_admin.css" />
<style type="text/css">
	.ac_results {
		padding: 0px;
		border: 1px solid black;
		background-color: white;
		overflow: hidden;
		z-index: 99999;
	}
	.ac_results ul {
		width: 100%;
		list-style-position: outside;
		list-style: none;
		padding: 0;
		margin: 0;
	}
	.ac_results li {
		margin: 0px;
		padding: 2px 5px;
		cursor: default;
		display: block;
		font: menu;
		font-size: 12px;
		line-height: 16px;
		overflow: hidden;
	}
	.ac_loading {
		background: white url('indicator.gif') right center no-repeat;
	}
	.ac_odd {
		background-color: #eee;
	}
	.ac_over {
		background-color: #0A246A;
		color: white;
	}
</style>
{/literal}
<script Language="JavaScript" Type="text/javascript">
	var stateZipsJson = {$stateZipsJson};
	var customer_address_json = '{$customer_addresses_list}';
	var payment_method = '{$payment_method}';
	var quickShop=false;
	var selectedAddressIndex = "";
	var numericReg  =  /(^-?\d\d*\.\d*$)|(^-?\d\d*$)|(^-?\.\d\d*$)/;
	$(".addressLabel").show();
	$(".addressForm").hide();
		{literal}
		function FrontPage_Form1_Validator(theform)
		{
         
         var added=theform.addedby;
         var commentTitle=theform.commenttitle;
         var desc=theform.commentdetails;
         var email= theform.email;
         var email_pattern =  new RegExp("[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+.[a-zA-Z]{2,4}","i");
         if(quickShop == true){
         	return true;
         }
         if(email.value !=null && email.value !="" && !email_pattern.test(email.value)){
         	alert("Please enter a valid email address:");
         	return false;
         }
         
         if (commentTitle.value==null||commentTitle.value == "")
		  {
			alert("Please enter subject.");
			commentTitle.focus();
			return false;
		  }
		  if (desc.value==null||desc.value == "")
		  {
			alert("Please enter comment details.");
			desc.focus();
			return false;
		  }
		 
		  return true;
		  
		}
		
	$(document).ready(function(){	
		$(".state-text").hide();

		$(".country-select").change(function(){
		        var countryVal=$('option:selected',this).val();
		        if(countryVal != "IN"){
                		$(".state-select").hide();
		                $(".state-text").show();
		        }
		        else{
                		$(".state-text").hide();
		                $(".state-select").show();
		        }
	        });

    		$(".pincode").keypress(function(){
				$(".modifybutton").attr("disabled",true);	
            });
            $(".pincode").blur(function(){
            	var index = $(this).attr("index"); 
            	var pcode = $(this).attr("value");
            	pcode = pcode.trim();
            	if(pcode.length != 6 || !numericReg.test(pcode)){
                	alert('Enter a valid pincode');
                	return false;
            	}
            	$.ajax({
        			url: "/myntra/ajax_pincode_locality.php?pincode=" + pcode,
        			dataType: "json",
        			beforeSend: function() {
        				$("#locality_"+index).attr('disabled', 'disabled');
        			},
        			success: function(response) {
        				$("#locality_"+index).removeAttr('disabled', 'disabled');
        				$("#locality_"+index).val('');
        				$("#locality_"+index).autocomplete(response.locality,{minChars : 0});
        				$("#locality_"+index).focus(function(){
       						$(this).trigger('keydown.autocomplete');
        				});
        			},
        			complete: function(xhr, status) {
        				$("#locality_"+index).removeAttr('disabled');
        			}
        		});
            });
            
	});
		function modifyAddress(index){
			selectedAddressIndex = index;
			$("#customerAddressFormDiv"+index).show();
			$("#customerAddressDiv"+index).hide();
			$('.modifyAddress').attr('disabled', 'disabled');
			$(".shipTo").attr('disabled', 'disabled');
		}
		function readOnlyAddress(index){
			$("#customerAddressFormDiv"+index).hide();
			$("#customerAddressDiv"+index).show();
			$(".modifyAddress").removeAttr('disabled');
			$(".shipTo").removeAttr('disabled');
			selectedAddressIndex = "";
		}
		function shipTo(index, count){
			selectedAddressIndex = index;
			quickShop = true;
			if(count > 0)
				propagateToChildOrders(index);
			else
			    $("#customerAddressForm"+index).submit();
		}
		function hidePopupOverlay(){
			$("#uploadstatuspopup").css("display","none");
			return false;
		}
		function propagateToChildOrders(id){
			$("#uploadstatuspopup").css("display","block");
			$("#uploadstatuspopup-content").css("display","block");
			$("#uploadstatuspopup_title").css("display","block");
			
			var oThis = $("#uploadstatuspopup-main");
			var topposition = ( $(window).height() - oThis.height() ) / 2;
			if ( topposition < 10 ) 
				topposition = 10;
			oThis.css("top", ( topposition + $(window).scrollTop() + "px"));
			return false;
		}
		
		function submitAddressChangeForm() {
			var selectedChildOrders = "";
			$("input:checkbox[name=childOrderId]:checked").each(function(){
				selectedChildOrders += selectedChildOrders==""?""+$(this).val():","+$(this).val();
			});
			$("[name='childorderids']").get(selectedAddressIndex).value = selectedChildOrders;
			$("#customerAddressForm"+selectedAddressIndex).submit();
		} 
		
		function verifyServiceability(id){
			
			var pincode = document.getElementById("pincode_"+id).value;
			var orderid = document.getElementById("orderid_"+id).value;
			
			var state = document.getElementById("state-filter_"+id).value;
			
			var isPinValid = isPincodeValidForState('IN', pincode, state);
			if(!isPinValid){
				alert("pincode and state do not match");
				$("#modifysubmit_"+id).attr("disabled",true);
				return false;
			}
				
			var locality = document.getElementById("locality_"+id).value;
			if(locality == ''){
				alert('Please enter locality');
				$("#modifysubmit_"+id).attr("disabled",true);
				return false;
			}
			
			$.ajax({
				type: "POST",
				url: "saveOrder_comment.php",
				data: "action=validate_zipcode&pincode="+pincode+"&orderid="+orderid+"&payment_method="+payment_method,
				success: function(msg) {
					var result = jQuery.parseJSON(msg);
					if(result.serviceable){
						$("#modifysubmit_"+id).removeAttr("disabled");		
						alert("pincode is serviceable, Please submit");
					}else{
						alert("pincode is not serviceable");
						$("#modifysubmit_"+id).attr("disabled",true);
					}						
				}
			});
		}
		
		var validateZipState = (typeof stateZipsJson == 'undefined' || stateZipsJson == null) ? false: true;
		
		function isPincodeValidForState(countryVal,pincodeVal,stateVal) {
			if (countryVal == 'IN' && validateZipState && stateVal != - 1) {
				var zips = stateZipsJson["IN-" + stateVal];
				var pass = false;
				if(typeof zips == 'undefined' || zips == null){
					pass = true;
				}else{
					for (index in zips) {
						var eachZip = zips[index];
						if (eachZip == pincodeVal.substring(0, eachZip.length)) {
							pass = true;
							return true;
						}
					}
					pass = false;
				}
			}
			else { //if country is not india, then pass this validation anyways
				pass = true;
			}
			return pass;
		}
		{/literal}
		
		</script>
<link rel="stylesheet" href="../skin1/mykriti.css"/>

</head>
<body>
<div id="scroll_wrapper">
	<div id="container"> 
	  <div id="wrapper"> 
	    <div id="display">
	    {if $errorMsg }
	    	<div style="font-size: 14px; color:red; font-weight: bold;">{$errorMsg}</div>
	    {else} 
	      <div class="center_column"> 
	        <div class="print"> 
		        <div class="super" > 
	           		 <p>&nbsp;</p>
	          	</div>
	          	
				{if $currentorderstatus ne "I"}
                {if $commentSuccess eq "N"}
		       <table>
				<tr>
					<td>Could not add request since the order is shipped/in process!!!!!</td>
					<td>&nbsp;&nbsp;</td>
					<td>&nbsp;&nbsp;</td>
				</tr>
				<tr>
					<td style="font-size:.8em;color:red;"><b>Please Click the close button below to refresh data on back screen</b></td>
				</tr>
				<tr>
					<td align="center"><input type="button" style="height:20px;width:50px; background-color:black; color:white;" value="Close" onclick="opener.window.location.reload();self.close()"></td>
				</tr>
		       </table>
		        
	          	{elseif $commentSuccess ne "Y"}
	          	<div class="head" style="padding-left:15px;"> 
		            <p>&nbsp;&nbsp;Address Change Request&nbsp;(#Order Id : {$orderid})</p>
          		</div>
          		<div class="links"><p></p></div>
          		<div class="foot"></div>
          		
          		

            <input type="hidden" name="type" value='Order Change Request'>
            <div class="head"> 
	            <p>&nbsp;&nbsp;Customer's Addresses</p>
      		</div>
            <table border=1>
            	{foreach from=$customer_addresses_list item=customer_address name=status}
            	<tr>
					<td width="5%" align=top>
						 {$smarty.foreach.status.index+1}
					</td>
					<td width="70%">		
						<div id="customerAddressDiv{$smarty.foreach.status.index}" style="display:block;" class="addressLabel">
					          		<div id=name>{$customer_address.name}</div>
					          		<div id=address>{$customer_address.address}</div>
					          		<div id=city>{$customer_address.city}</div>
					          		<div id=state>{$customer_address.state}</div>
					          		<div id=country>{$customer_address.value}</div>
					          		<div id=locality>Locality : {$customer_address.locality}</div>
					          		<div id=pincode>{$customer_address.pincode}</div>
					          		<div id=mobile>{$customer_address.mobile}</div>
					          		<div id=email>{$customer_address.email}</div>
				        </div>
				        <div id="customerAddressFormDiv{$smarty.foreach.status.index}" style="display: none;" class="addressForm">
				        	<form id="customerAddressForm{$smarty.foreach.status.index}" method="post" enctype="multipart/form-data" onsubmit="return FrontPage_Form1_Validator(this)" language="JavaScript" name="FrontPage_Form1"  action="saveOrder_comment.php">
				        		<table>
				        			<tr>
				        			<td>Date Added</td>
									<td><input type="text" name="dateadded" value={$date} readonly></td>
				        			</tr>
				        			<tr>
										<td>Added By<span style="color:red;">  *</span></td>
										<td><input type="text" name="addedby" value={$login} disabled=true></td>
									</tr>
				        			<tr><td>Name:</td>
					          		<td><input type=text name="recipient_name" value="{$customer_address.name}"></td>
					          		</tr><tr><td>Address:</td>
					          		<td><textarea cols="20" rows="4" name="new_address">{$customer_address.address}</textarea></td>
					          		</tr><tr><td>Country:</td>
					          		<td>
					          			<input type=text value="{$customer_address.value}" disabled="true">
					          			<input type=hidden name="country" value="{$customer_address.country}">
					          		</td>
					          		</tr><tr><td>State:</td>
					          		<td>
					          		{if $customer_address.country ne 'IN'}
					          			<input type=text value="{$customer_address.state}" name=state>
					          		{else}
					          		<select class="state-select" id="state-filter_{$smarty.foreach.status.index}" name="state" size="1" onchange="verifyServiceability('{$smarty.foreach.status.index}');">
										<option value=""></option>
										{section name=state_idx loop=$states}
											{if $customer_address.state_code eq $states[state_idx].state_code}
		                                	<option value="{$states[state_idx].state_code}" selected>{$states[state_idx].state}</option>
		                                	{else}
		                                	<option value="{$states[state_idx].state_code}">{$states[state_idx].state}</option>
		                                	{/if}
                		                {/section}
                      	            </select>
                      	            {/if}
					          		</tr><tr><td>City</td>
					          		<td><input type=text name="city" value="{$customer_address.city}"></td>
					          		</tr>
					          		<tr><td>Pincode:</td>
					          		<td>
					          			<input type=text id= "pincode_{$smarty.foreach.status.index}" index="{$smarty.foreach.status.index}" name="pincode" value="{$customer_address.pincode}" class="pincode">
					          			<span style="font-size:10px;">Select pincode and tab out to fetch localities for zipcode</span>
					          		</td>
					          		</tr>
					          		<tr><td>Locality:</td>
					          		<td>
					          			<input type=text id= "locality_{$smarty.foreach.status.index}" name="locality" value="{$customer_address.locality}" class="locality" value="{$customer_address.locality}">
					          		</td>
					          		</tr>
					          		<tr><td>Mobile:</td>
					          		<td><input type=text name="mobile" value="{$customer_address.mobile}"></td>
					          		</tr><tr><td>Recepient's Email:</td>
					          		<td><input type=text name="email" value="{$customer_address.email}"></td>
					          		</tr>
									<tr>
										<td>Additional Comments<span style="color:red;">  *</span></td>
										<td><textarea name="commentdetails" cols="30" rows="4" value=""></textarea></td>
									</tr>
									<tr>
                                        <td>Add this to user's list of shipping addresses<span style="color:red;"></td>
                                        <td><input class="addCheck" type="checkbox" name="addToShippingList" value="true" checked></td>
                                	</tr>
									<tr>
										<td>
										</td>
										<td>
											<input type="button" value="Verify Zipcode Serviceability" class="button" onclick="verifyServiceability('{$smarty.foreach.status.index}');">
											{if $splitOrders|@count gt 0}
												<input id="modifysubmit_{$smarty.foreach.status.index}" type="submit" onclick="return propagateToChildOrders({$smarty.foreach.status.index});" class="button modifybutton" disabled>
											{else}
												<input id= "modifysubmit_{$smarty.foreach.status.index}" type="submit" value="Submit" class="button modifybutton" disabled>
											{/if} 	
											<input type=reset value="Cancel" class=button onclick="readOnlyAddress({$smarty.foreach.status.index})">
										</td>
										<input type="hidden" id="orderid_{$smarty.foreach.status.index}" name ="orderid" value={$orderid}>
										<input type="hidden" name ="orderid" value={$orderid}>
										<input type="hidden" name="childorderids" value="">
										<input type="hidden" name ="changerequest" value='Y'>
					                    <input type="hidden" name ="commenttitle" value='Address Change Request'>
					                    <input type="hidden" name ="commenttype" value='Order Change'>
					                    <input type="hidden" name="type" value='Order Change Request'>
					                    <input type="hidden" name="addedby" value="{$login}">
					                </tr>
					          	</table>
					        </form>
				        </div>
				    </td>
				    <td width="25%" align=center>
				    	{if $customer_address.serviceable}
				    	<button type=button id="shipIndex{$smarty.foreach.status.index}" class="button shipTo" onclick="shipTo({$smarty.foreach.status.index}, {$splitOrders|@count})">Ship to</button>
				    	{else}
				    		Cannot ship to this address. Zipcode is not serviceable
				    	{/if}
				    	<br><br>
					{if $order.shipping_method neq 'SDD'}
                                        
				    		<button id="modifyIndex{$smarty.foreach.status.index}" class="button modifyAddress" onclick="modifyAddress({$smarty.foreach.status.index})">Modify Address</button>
                                        {/if}
                                        </td>
				</tr>
				{/foreach}
            </table>
		</form>
		{if $splitOrders|@count gt 0 }
	        <div id="uploadstatuspopup" style="display:none">
				<div class="overlay" style="display:block;_position:absolute;_width:1300px;_height:1500px;">&nbsp;</div>
				<div id="uploadstatuspopup-main" style="position:absolute; left:0; width:450px; background:white; z-index:1006;border:2px solid #999999;">
					<div id="uploadstatuspopup-content" style="display:none;">
					    <div id="uploadstatuspopup_title" class="popup_title">
					    	<div id="uploadstatuspopup_ajaxWindowTitle" class="popup_ajaxWindowTitle">Select OrderIds</div>
							<div class="clear">&nbsp;</div>
						</div>
						<div style="padding:5px 25px; text-align:left; font-size:12px" >
							<div> 
							    <font face='verdana, arial, helvetica, san-serif' size='2'>
									<p><b>This order was split into the following orders. Modify the address for these orders as well? <br /><br /></b></p>
									{section name="idx" loop=$splitOrders}
										<input type='checkbox' checked='checked' style='vertical-align:middle' value="{$splitOrders[idx]}" id="childOrderId_{$splitOrders[idx]}" name="childOrderId"> {$splitOrders[idx]} <br />
									{/section} 
									<br />
								</font>
								<input id="childordersubmit_{$smarty.foreach.status.index}" type="button" value="Submit" onclick="submitAddressChangeForm();">
								<input id="childordercancel_{$smarty.foreach.status.index}" type="button" value="Cancel" onclick="hidePopupOverlay();">
							</div>
						</div>
						<div class="clear">&nbsp;</div>
					</div>
				</div>
			</div>
		{/if}
		{elseif $commentSuccess eq "Y"}
		<table>
				<tr>
					<td>Change Request added Succesfully!!!!!</td>
					<td>&nbsp;&nbsp;</td>
					<td>&nbsp;&nbsp;</td>
				</tr>
				<tr>
					<td style="font-size:.8em;color:red;"><b>Please Click the close button below to refresh data on back screen</b></td>
				</tr>
				<tr>
					<td align="center"><input type="button" style="height:20px;width:50px; background-color:black; color:white;" value="Close" onclick="opener.window.location.reload();self.close()"></td>
				</tr>
		</table>
		{/if}
		{else}
			<font color="#ff0000">
			 Work is in Progress on this order. You cannot add any more change requests
			</font>
		{/if}
		</div> 
	      </div>
	     {/if}   	
	     </div> 
	   </div> 
	 </div>  
	</div>
</body> 
</html>
    	
