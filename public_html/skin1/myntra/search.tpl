{include file="site/doctype.tpl"}
<head>
	{include file="site/header.tpl" }
</head>
<body  class="{$_MAB_css_classes}">
<script>
Myntra.Data.pageName="search";
</script>
{include file="site/menu.tpl" }

<div class="container clearfix">
	<div class="ajax-loading-msg left" style="display:none;">
		<div class="ajax-loading-msg-loader"></div>
	</div>
    <div class="content clearfix search-page-content">
    <div class="pagination-block pagemain-title">
        <h1 class="title left">{if $synonymQuery && $synonymQuery|trim neq ""}{$synonymQuery}{else}{if $h1 && $h1|trim neq ""}{$h1}{else}{$query|truncate:40|replace:"-":" "|capitalize}{/if}{/if}</h1><span class="left title-re"> - </span>
        <div class="title title-re left"><span class="products-count">{if $total_items}{$total_items}{else}no{/if} Products</span> found</div>
    </div>
		<div class="content-left left last search-page-content-left">
		{if $total_items ge 1}
			<div class="go-to-top-section" style="display:none;">
				<span class="go-to-top-arrow"></span>
				<button type="submit" class="go-to-top-btn action-btn">GO TO TOP</button>
			</div>
		{/if}
		{if $global_display_categories}
		<div class="display-category-filters">
			<div class="filter-type-block clearfix">
				<div class="filter-title clearfix">
					<span class="filter-title-text left">CATEGORIES</span>
					<span class="right div-block-plus" {if $total_items ge 1}{else}style="display:none;"{/if}></span>
					<span class="right div-block-minus" {if $total_items ge 1}style="display:none;"{/if}></span>
				</div>
				<div class="div-block-content {if $total_items lt 1}no_result_disp_categories{/if}" {if $total_items ge 1}style="display:none;"{/if}>
				<ul class="filter-type-content category-filter-box">
					{foreach key=category item=count from=$global_display_categories}
						<li class="filter-box {if $query|lower|replace:' ':''|replace:'-':'' eq $category|lower|replace:' ':''|replace:'-':''}highlight{/if}">
							<a href="{$http_location}/{myntralink display_category_facet="$category" is_landingpage="y"}" class="filter">{$category|lower|capitalize}</a>
						<!--span class="count"> ({$count})</span--></li>
					{/foreach}
				</ul>
				</div>
			</div>
		</div>
		{/if}
		{if $total_items ge 1}
			{include file="site/leftnav_filters.tpl"}
		{/if}
		</div>
    	<div class="content-right right last search-page-content-right" id="search-page-contents">
       		<!--promotion-->
			{if $promotion_content}
    		<div class="summary-block clearfix mb10">
					<div>{$promotion_content}</div>
					<div style="clear:both;padding-bottom:10px;"></div>
    		</div>
    		{/if}
			<!--promotion-->

			{if $total_items ge 24}
			<!--AVAIL-->
			<div class="related-products left clearfix last" >
				<div id="avail-container"></div>
			</div>
			<!-- END AVAIL-->
			
    		{if $query_level=='FULL'}
    		<div class="full-search-message-block span-26">
    			<div>We did not find any results for your query. Showing results for partial matches instead.</div>
				<br>	
    		</div>
    		{/if}
			{/if}
    		{if $total_items ge 1}
    		<div class="pagination-block mb10 clear">
    			<div class="filter-block mb10">
    				<div class="sort-by-filters">
    					<span class="h4sortfilter">Sort By: </span>
    					<a href="{$http_location}/{$query}/POPULAR/{$current_page}" rel="/{$query}/POPULAR/{$current_page}" class="filter no-decoration-link popular-sort-filter {if $sort_param_in_url eq 'POPULAR' || $sort_param_in_url eq ''}sort-selected{/if}" onclick="return Myntra.Search.Utils.setSortBy('POPULAR', this);">What's Hot</a>
    					<a href="{$http_location}/{$query}/RECENCY/{$current_page}" rel="/{$query}/RECENCY/{$current_page}" class="filter no-decoration-link recency-sort-filter {if $sort_param_in_url eq 'RECENCY'}sort-selected{/if}" onclick="return Myntra.Search.Utils.setSortBy('RECENCY', this);">What's New</a>
    					<a href="{$http_location}/{$query}/DISCOUNT/{$current_page}" rel="/{$query}/DISCOUNT/{$current_page}" class="filter no-decoration-link discount-sort-filter {if $sort_param_in_url eq 'DISCOUNT'}sort-selected{/if}" onclick="return Myntra.Search.Utils.setSortBy('DISCOUNT', this);">Discount</a>
    					<a href="{$http_location}/{$query}/PRICEA/{$current_page}" rel="/{$query}/PRICEA/{$current_page}" class="filter no-decoration-link pricea-sort-filter {if $sort_param_in_url eq 'PRICEA'}sort-selected{/if}" onclick="return Myntra.Search.Utils.setSortBy('PRICEA', this);">Price low-to-high</a>
    					<a href="{$http_location}/{$query}/PRICED/{$current_page}" rel="/{$query}/PRICED/{$current_page}" class="filter no-decoration-link priced-sort-filter {if $sort_param_in_url eq 'PRICED'}sort-selected{/if}" onclick="return Myntra.Search.Utils.setSortBy('PRICED', this);">Price high-to-low</a>
    				</div>
    			</div>
    			{if $searchpagescrolltype eq "control"}
    			<div class="clearfix">
	    			<div class="left pagination-info">Page <span class="currentpage">{$current_page}</span> of <span class="pagecount">{$page_count}</span> pages, at&nbsp; 
	                    <a href="javascript:void(0);" class="link-24 noofitems-links {if $noofitems eq 24}selected{/if}" onclick="Myntra.Search.Utils.selectNoOfItems(24, this)">24</a>
	                    <a href="javascript:void(0);" class="link-48 noofitems-links {if $noofitems eq 48}selected{/if}" onclick="Myntra.Search.Utils.selectNoOfItems(48, this)">48</a>
	                    <a href="javascript:void(0);" class="link-96 noofitems-links {if $noofitems eq 96}selected{/if}" onclick="Myntra.Search.Utils.selectNoOfItems(96, this)">96</a>
	    			items per page
	    			</div>
	                <div class="pagination-links right">
	                    {if $current_page neq 1}
	                        <a href="{if $current_page-1 ge 1 && $current_page le $page_count}{$http_location}/{$query}{if $sort_param_in_url}/{$sort_param_in_url}{/if}/{$current_page-1}{/if}" rel="{if $current_page-1 ge 1 && $current_page le $page_count}/{$query}{if $sort_param_in_url}/{$sort_param_in_url}{/if}/{$current_page-1}{/if}" class="no-decoration-link pagination-prev" onclick="return Myntra.Search.Utils.setPagePrev(this);" rel="prev">&lsaquo; Prev</a>
	                    {/if}
	                    {section name=foo start=$start_page loop=$end_page+1 step=1}
	                    {if $smarty.section.foo.index eq $current_page}
	                    <span>{$smarty.section.foo.index}</span>
	                    {else}
	                    <a href="{$http_location}/{$query}{if $sort_param_in_url}/{$sort_param_in_url}{/if}/{$smarty.section.foo.index}" rel="/{$query}{if $sort_param_in_url}/{$sort_param_in_url}{/if}/{$smarty.section.foo.index}" class="no-decoration-link" onclick="return Myntra.Search.Utils.setPage('{$smarty.section.foo.index-1}', this);">{$smarty.section.foo.index}</a>
	                    {/if}
	                    {/section}
	                    {if $current_page neq $page_count}
	                    <a href="{if $current_page+1 le $page_count && $current_page ge 1}{$http_location}/{$query}{if $sort_param_in_url}/{$sort_param_in_url}{/if}/{$current_page+1}{/if}" rel="{if $current_page+1 le $page_count && $current_page ge 1}/{$query}{if $sort_param_in_url}/{$sort_param_in_url}{/if}/{$current_page+1}{/if}" class="no-decoration-link {if $page_count eq 1}hide{/if} pagination-next" onclick="return Myntra.Search.Utils.setPageNext(this);" rel="next">Next &rsaquo;</a>
	                    {/if}
	                </div>
                </div>
                {/if}
    		</div>
			{/if}
    		<!-- no search results -->
    		{if $total_items lt 1}
				<div id="no_result">
					<p class="no_result_title" style=''>There are no results available for your search "{$query|replace:"-":" "}" </p>
						<b>Please Browse</b>
						{if $global_display_categories}
							{foreach key=category item=count from=$global_display_categories name=foo}{if not $smarty.foreach.foo.first}, {/if}<a href="{$http_location}/{if $category|lower eq 't shirt' || $category|lower eq 't shirts' || $category|lower eq 't-shirt' || $category|lower eq 't-shirts'}{$category|replace:' ':''|replace:'-':''|lower}{else}{$category|replace:' ':'-'|lower}{/if}" class="no-decoration-link">{$category|lower|capitalize}</a>{/foreach}
						{/if}
				</div>
			{/if}

    		<!-- no search results ends -->
    		{if $total_items ge 1}
			<div class="product-listing mb10 search-listing">
				{if $shownewui eq 'newui'}
    				{include file="site/ajax_search_results_B.tpl"}
    			{else}
    				{include file="site/ajax_search_results.tpl"}
    			{/if}	
    				{if $searchpagescrolltype eq "infinitescroll"}
    				<div class="more-products-section">
				    	<a class="more-products-link"href="{if $current_page+1 le $page_count && $current_page ge 1}{$http_location}/{$query}{if $sort_param_in_url}/{$sort_param_in_url}{/if}/{$current_page+1}{/if}" rel="{if $current_page+1 le $page_count && $current_page ge 1}/{$query}{if $sort_param_in_url}/{$sort_param_in_url}{/if}/{$current_page+1}{/if}" onclick="return Myntra.Search.Utils.triggerManualInfiniteScroll();">Show more products <span class="products-left-content">(<span class="products-left-count"></span>)</span></a>
				    	<div class="more-products-loading-indicator" style="display:none;">
				    		<img src="{$cdn_base}/skin1/images/loader_100x12.gif">
				    	</div>
				    </div>
				    {/if}
    		</div>
    		{if $searchpagescrolltype eq "control"}
    		<div class="pagination-links right">
    			{if $current_page neq 1}
			    	<a href="{if $current_page-1 ge 1 && $current_page le $page_count}{$http_location}/{$query}{if $sort_param_in_url}/{$sort_param_in_url}{/if}/{$current_page-1}{/if}" rel="{if $current_page-1 ge 1 && $current_page le $page_count}/{$query}{if $sort_param_in_url}/{$sort_param_in_url}{/if}/{$current_page-1}{/if}" class="no-decoration-link pagination-prev" onclick="return Myntra.Search.Utils.setPagePrev(this);" rel="prev">&lsaquo; Prev</a>
			    {/if}
			    {section name=foo start=$start_page loop=$end_page+1 step=1}
			    {if $smarty.section.foo.index eq $current_page}
				    <span>{$smarty.section.foo.index}</span>
				{else}
					<a href="{$http_location}/{$query}{if $sort_param_in_url}/{$sort_param_in_url}{/if}/{$smarty.section.foo.index}" rel="/{$query}{if $sort_param_in_url}/{$sort_param_in_url}{/if}/{$smarty.section.foo.index}" class="no-decoration-link" onclick="return Myntra.Search.Utils.setPage('{$smarty.section.foo.index-1}', this);">{$smarty.section.foo.index}</a>
				{/if}
				{/section}
				{if $current_page neq $page_count}
				   	<a href="{if $current_page+1 le $page_count && $current_page ge 1}{$http_location}/{$query}{if $sort_param_in_url}/{$sort_param_in_url}{/if}/{$current_page+1}{/if}" rel="{if $current_page+1 le $page_count && $current_page ge 1}/{$query}{if $sort_param_in_url}/{$sort_param_in_url}{/if}/{$current_page+1}{/if}" class="no-decoration-link {if $page_count eq 1}hide{/if} pagination-next" onclick="return Myntra.Search.Utils.setPageNext(this);" rel="next">Next &rsaquo;</a>
				{/if}
			</div>
			{/if}
			{/if}
    	</div>
    	{if $total_items ge 1}
    	<script type="text/javascript">
			document.getElementById("search-page-contents").style.visibility="hidden";
		</script>
		{/if}
    <div class="related-products left clearfix last span-33">
   	{literal}
	{RECENT_WIDGET_PLACEHOLDER}
    {/literal}
   	</div>
		{if $description && $page<2 && $total_items ge 1 && $description neq " "}
			<div class="mb10 last left span-33" id="search_description">
				<h2 class="h2g bold last">About {$query|replace:"-":" "|capitalize}</h2>
				<div class="icon-line mb10"></div>
				<p>{$description}</p>
			</div>
		{/if}
   	</div>
    <div class="divider">&nbsp;</div>


  <!--Popular tags-->
{*
<div class="clearfix p10">
		<h2 class="h2b prepend" id="popular-title">Popular</h2>
	    <div class="icon-line"></div>
	    <ul class="tags">
	    	{foreach name=tagtype item=fixedorDynamic from=$mostPopularWidget}
	        	{foreach name=uniquetag item=tag from=$fixedorDynamic}
	            	<li><a href="{$http_location}/{$tag.link_url}">{$tag.link_name}</a></li>
	            {/foreach}
	        {/foreach}
	    </ul>
	</div>
*}
    <div class="divider">&nbsp;</div>

</div>
  {include file="site/footer_content.tpl" }
  
  
{literal}
<script type="text/javascript">
	var total_pages={/literal}{$page_count}{literal};
	var total_pages_temp=total_pages;		
	var show_additional_product_details = "{/literal}{$show_additional_product_details}{literal}"; 
	var query="{/literal}{$query|lower}{literal}";
        var parameter_query='{/literal}{$parameter_query}{literal}';
        var boosted_query='{/literal}{$boosted_query}{literal}';    
	var pageTitleNoPage="{/literal}{$pageTitleNoPage}{literal}";
	var metaDescNoPage="{/literal}{$metaDescriptionNoPage}{literal}";
	var sortByDefault="";
	var pageDefault={/literal}{$current_page}-1{literal};
    var current_page={/literal}{$current_page}-1{literal};
	if(pageDefault > total_pages){
		pageDefault=0;
	}
	pageName="search";
	avail_cookie_ids={/literal}'{$avail_cookie_ids}';
	AVAIL_USER_ID='{$AVAIL_USER_ID}'{literal};
	var configArray=new Object();
	configArray["url"]=query;
    configArray["parameter_query"]=parameter_query;
    configArray["boosted_query"]=boosted_query;
    configArray["page"]={/literal}{$current_page}-1{literal};
	configArray["noofitems"]={/literal}{$noofitems}{literal};
	configArray["gender"]=[];
	configArray["sizes"]=[];
	configArray["brands"]=[];
	configArray["pricerange"]={"rangeMin" : "{/literal}{$rangeMin}{literal}", "rangeMax" : "{/literal}{$rangeMax}{literal}"};
	var scrollConfigArray=new Object();
	scrollConfigArray["scrollPos"]=0;
	scrollConfigArray["scrollPage"]=0;
	scrollConfigArray["scrollProcessed"]=false;
  	var filterProperties=new Object();
  	var scrollProperties=new Object();
  	var searchpagescrolltype="{/literal}{$searchpagescrolltype}{literal}";
  	var priceRangeMin=parseInt("{/literal}{$rangeMin}{literal}");
	var priceRangeMax=parseInt("{/literal}{$rangeMax}{literal}");
	var batchProcess=false;
	var onBeforeUnloadFired = false;
	var pageSetToFirst=false;
	var filterClicked=false;
	var onPageLoad=true;
	sortByDefault="{/literal}{if $sort_param_in_url}{$sort_param_in_url}{/if}{literal}";
	var shownewui="{/literal}{$shownewui}{literal}";
	var default_noofitems={/literal}{$noofitems}{literal};
	configArray["sortby"]=sortByDefault;
	configArray["uq"]={/literal}'{if $userQuery == true}{$userQuery}{else}false{/if}'{literal};
	var trackingBooleans=new Object();
	trackingBooleans["gender"]=false;
	trackingBooleans["sizes"]=false;
	trackingBooleans["brands"]=false;
	trackingBooleans["pricerange"]=false;
	trackingBooleans["noofitems"]=false;
	var priceRangeMin=parseInt("{/literal}{$rangeMin}{literal}");
	var priceRangeMax=parseInt("{/literal}{$rangeMax}{literal}");
	var articleTypeAttributesList=[];
	var innerhtml="";
	var noOfAutoInfiniteScrolls="{/literal}{$noOfAutoInfiniteScrolls}{literal}";
	var ajaxCallFunc="";
	var heightFromLast=1170;
	var noResultsPage=true;
</script>
{/literal}
  
{if $total_items ge 1}
<script type="text/javascript">
	noResultsPage=false;
</script>
{if $article_type_attributes}
	<script type="text/javascript">
	{foreach key=attribute item=props from=$article_type_attributes}
		configArray["{$props.title|replace:' ':'_'}_article_attr"]=[];
		trackingBooleans["{$props.title|replace:' ':'_'}_article_attr"]=false;
		articleTypeAttributesList.push("{$props.title|replace:' ':'_'}_article_attr");
	{/foreach}
	</script>
{/if}

{if $article_type_attributes}
	<script type="text/javascript">
	{foreach key=attribute item=props from=$article_type_attributes}
		{literal}
		filterProperties["{/literal}{$props.title|replace:' ':'_'}_article_attr{literal}"]={
			getURLParam: function(){
				var ataStr=configArray["{/literal}{$props.title|replace:' ':'_'}_article_attr{literal}"].join(":");
				return (ataStr != "")?"{/literal}{$props.title|replace:' ':'_'}_article_attr{literal}"+"="+ataStr:"";
			},
			processURL: function(str){
				configArray["{/literal}{$props.title|replace:' ':'_'}_article_attr{literal}"]=str.split(":");
				return true;
			},
			updateUI: function(){
				//unselect all brands
				$("{/literal}{$props.title|replace:' ':'_'}_article_attr{literal}"+"-ata-filter .filter-box").removeClass("checked").addClass("unchecked");

				//update UI selection for brands in configArray
				for(var attr=0;attr<configArray["{/literal}{$props.title|replace:' ':'_'}_article_attr{literal}"].length;attr++){
					var selector=configArray["{/literal}{$props.title|replace:' ':'_'}_article_attr{literal}"][attr].toLowerCase().replace(/\W+/g, "") + "-ata-filter";
					$("."+selector).removeClass("unchecked").addClass("checked");
				}
				return true;
			},
			setToDefault: function(){
				configArray["{/literal}{$props.title|replace:' ':'_'}_article_attr{literal}"]=[];
				return true;
			},
			resetParams: function(){
				configArray["page"]=0;
				return true;
		}};
		{/literal}
	{/foreach}
	</script>
{/if}
{/if}
{include file="site/footer_javascript.tpl"}
<!--retarget pixel tracking-->
{if $retargetPixel.retargetHttp}
	<script type="text/javascript">
		$(window).load(function () {ldelim}
			$('#retargetSpan').html("{$retargetPixel.retargetHttp}");
		{rdelim});
	</script>
	<span id="retargetSpan" style="display:none;"></span>
{/if}
<!--retarget pixel tracking-->
</body>
</html>
