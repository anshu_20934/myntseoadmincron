<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html xml:lang="en" xmlns:fb="http://www.facebook.com/2008/fbml">
<head>
	{include file="site/header.tpl" }
    <!--[if lt IE 8]>
    <link rel="stylesheet" href="{$http_location}/skin1/myntra_css/ie.css" type="text/css" media="screen, projection"><![endif]-->
    
    <link rel="stylesheet" href="{$cdn_base}/skin1/css/jquery-ui-slider.custom.1.0.min.css.jgz" type="text/css"></link>

    {* literal}

  <style type="text/css">

    .content ul li.item .item-description{width: 180px !important; padding: 0px !important;height: 40px;}
    .content ul li.item .item-image {width: 180px !important; height: 247px !important;}
    .h2g{font-size: 14px; color:#808080}
    .content ul li.item .name {color:#808080;height: 45px;padding-bottom:0px !important;display:block;text-align:center;}

    .item-box{font-size: 12px;width: 180px; height:350px;padding: 4px; background: #F2F2F2;}
    .item-box img, .item-image{width: 180px !important; height: 240px;}
    .item-box div{color:#808080; float: none; text-align: center;}
    .noncashback {margin: 0 !important;}
    .dp{color: #000 !important;}
    .op{}
    .dpv{color:#EC8609 !important;position:relative;}
    .content ul li.item {margin-right: 10px !important;height: auto;}
    .scrollable {height: 390px !important;}
    .scrollable .items {margin: 0 !important;}
    .content ul li.item .availability{white-space:nowrap;z-index:1000;border:0px solid red;position:absolute;width:180px !important;color:#999;visibility:hidden;display:block;margin-top:0px;font-size:11px;line-height:20px;clear:both;text-align: center;}
    .content ul li.item:hover .availability{visibility:visible;width:180px !important;}
    .content ul li.item .item-box:hover{background: #e6e6e6}
    .content .product-listing ul li.item {margin-bottom: 30px !important;}
    #leftSlider { width: 135px !important;}
    
    .cbtip {
  background: none repeat scroll 0 0 #FFFFCC;
  border: 1px solid #999999;
  color: #666666;
  font-size: 11px;
  left: 3px;
  padding: 5px 6px;
  position: absolute;
  text-align: left;
  top: -160px !important;
  width: 160px;
  z-index: 100;
}
.cb {
  border-bottom: 1px dotted #006699;
  font-weight: normal;
  color:#EC8609 !important;
  font-size:12px !important; ;
}
  </style>

    {/literal *}
</head>
<body class="{$_MAB_css_classes}">
{include file="site/menu.tpl" }
<div class="container clearfix">
	<div class="ajax-loading-msg left" style="display:none;">
		<div class="ajax-loading-msg-loader"></div>
	</div>	
    <div class="content clearfix search-page-content">
		<div class="content-left left span-6 last">
		{if $global_display_categories}
		<div class="display-category-filters corners-tl-tr">
			<div class="filter-type-block">
				<div class="filter-title corners-tl-tr"><span class="filter-block-minus" {if $total_items ge 1}style="display:none;"{else}style="display:block;"{/if}></span><span class="filter-block-plus" {if $total_items ge 1}style="display:block;"{else}style="display:none;"{/if}></span><span class="filter-title-text">Shop by Category</span></div>
				<ul class="filter-type-content category-filter-box" {if $total_items ge 1}style="display:none;"{/if}>
					{foreach key=category item=count from=$global_display_categories}
						<li class="filter-box {if $query|lower|replace:' ':''|replace:'-':'' eq $category|lower|replace:' ':''|replace:'-':''}highlight{/if}"><a href="{$http_location}/{if $category|lower eq 't shirt' || $category|lower eq 't shirts' || $category|lower eq 't-shirt' || $category|lower eq 't-shirts'}{$category|replace:' ':''|replace:'-':''|lower}{else}{$category|replace:' ':'-'|lower}{/if}" class="filter">{$category|lower|capitalize}</a><!--span class="count"> ({$count})</span--></li>
					{/foreach}
				</ul>
			</div>
		</div>
		{/if}
		{if $total_items ge 1}
			{include file="site/leftnav_filters.tpl"}
		{/if}
		</div>
    	<div class="content-right right span-27 last search-page-content-right" id="search-page-contents">
    		<!--promotion-->
			{if $promotion_content}
    		<div class="summary-block clearfix mb10">
					<div>{$promotion_content}</div>
					<div style="clear:both;padding-bottom:10px;"></div>
    		</div>
    		{/if}
			<!--promotion-->
    		{if $total_items ge 1}
    		<div class="pagination-block span-27 mb10">
    			<div class="span-27">
    				<h1 class="title left">{if $h1 && $h1|trim neq ""}{$h1}{else}{$query|truncate:30|replace:"-":" "|capitalize}{/if}</h1><span class="left title"> - </span>
    				<div class="title left"><span class="products-count">{$total_items} Products</span> found</div>
    				<select class="right noofitems-select">
    					<option value="24" selected="selected">24 per page</option>
    					<option value="48">48 per page</option>
    					<option value="96">96 per page</option>
    				</select>
    				<div class="right pagination-info">Showing <span class="currentpage">{$current_page}</span> of <span class="pagecount">{$page_count}</span> Pages</div>
    			</div>
    			<div class="span-27">
    				<div class="sort-by-filters left">
    					<span>Sort By: </span>
    					<a href="{$http_location}/{$query}/PRICEA/{$current_page}" rel="/{$query}/PRICEA/{$current_page}" class="filter no-decoration-link pricea-sort-filter {if $sort_param_in_url eq 'PRICEA'}sort-selected{/if}" onclick="return setSortBy('PRICEA', this);">Price Low to High</a> |
    					<a href="{$http_location}/{$query}/PRICED/{$current_page}" rel="/{$query}/PRICED/{$current_page}" class="filter no-decoration-link priced-sort-filter {if $sort_param_in_url eq 'PRICED'}sort-selected{/if}" onclick="return setSortBy('PRICED', this);">Price High to Low</a> |
    					<a href="{$http_location}/{$query}/DISCOUNT/{$current_page}" rel="/{$query}/DISCOUNT/{$current_page}" class="filter no-decoration-link discount-sort-filter {if $sort_param_in_url eq 'DISCOUNT'}sort-selected{/if}" onclick="return setSortBy('DISCOUNT', this);">Discount</a> |
    					<a href="{$http_location}/{$query}/RECENCY/{$current_page}" rel="/{$query}/RECENCY/{$current_page}" class="filter no-decoration-link recency-sort-filter {if $sort_param_in_url eq 'RECENCY'}sort-selected{/if}" onclick="return setSortBy('RECENCY', this);">What's New</a> |
    					<a href="{$http_location}/{$query}/POPULAR/{$current_page}" rel="/{$query}/POPULAR/{$current_page}" class="filter no-decoration-link popular-sort-filter {if $sort_param_in_url eq 'POPULAR'}sort-selected{/if}" onclick="return setSortBy('POPULAR', this);">Popular</a>
    				</div>
    				<div class="pagination-links right">
    					{if $current_page neq 1}
					    	<a href="{if $current_page-1 ge 1 && $current_page le $page_count}{$http_location}/{$query}{if $sort_param_in_url}/{$sort_param_in_url}{/if}/{$current_page-1}{/if}" rel="{if $current_page-1 ge 1 && $current_page le $page_count}/{$query}{if $sort_param_in_url}/{$sort_param_in_url}{/if}/{$current_page-1}{/if}" class="no-decoration-link pagination-prev" onclick="return setPagePrev(this);">&lsaquo; Prev</a>
					    {/if}
					    {section name=foo start=$start_page loop=$end_page+1 step=1}
					    {if $smarty.section.foo.index eq $current_page}
					    <span>{$smarty.section.foo.index}</span>
					    {else}
					    <a href="{$http_location}/{$query}{if $sort_param_in_url}/{$sort_param_in_url}{/if}/{$smarty.section.foo.index}" rel="/{$query}{if $sort_param_in_url}/{$sort_param_in_url}{/if}/{$smarty.section.foo.index}" class="no-decoration-link" onclick="return setPage('{$smarty.section.foo.index-1}', this);">{$smarty.section.foo.index}</a>
					    {/if}
					    {/section}
					    {if $current_page neq $page_count}
					    	<a href="{if $current_page+1 le $page_count && $current_page ge 1}{$http_location}/{$query}{if $sort_param_in_url}/{$sort_param_in_url}{/if}/{$current_page+1}{/if}" rel="{if $current_page+1 le $page_count && $current_page ge 1}/{$query}{if $sort_param_in_url}/{$sort_param_in_url}{/if}/{$current_page+1}{/if}" class="no-decoration-link {if $page_count eq 1}hide{/if} pagination-next" onclick="return setPageNext(this);">Next &rsaquo;</a>
					    {/if}
					</div>
    			</div>
    			<div class="icon-line mt10 span-27"></div>
    		</div>
			{/if}
    		<!-- no search results -->
    		{if $total_items lt 1}
				<div id="no_result">
					<p class="no_result_title" style=''>There are no results available for your search "{$query|replace:"-":" "}" </p>
						<b>Please Browse</b>
						{if $global_display_categories}
							{foreach key=category item=count from=$global_display_categories name=foo}{if not $smarty.foreach.foo.first}, {/if}<a href="{$http_location}/{if $category|lower eq 't shirt' || $category|lower eq 't shirts' || $category|lower eq 't-shirt' || $category|lower eq 't-shirts'}{$category|replace:' ':''|replace:'-':''|lower}{else}{$category|replace:' ':'-'|lower}{/if}" class="no-decoration-link">{$category|lower|capitalize}</a>{/foreach}
						{/if}
				</div>
			{/if}

    		<!-- no search results ends -->
    		<div class="product-listing clear mb10" style="margin-left: 5px;">
    			<ul class="clearfix last">
                    {section name=product loop=$products }
					<li class="item show_details" data-id="prod_{$products[product].id}">
					{if $show_additional_product_details eq 'y'}
						<div class="style-details" style="display:none"></div>
					{/if}
					{if $products[product].discount_label}<div class="discount-label val"><span>{$products[product].discount_label}</span></div>{/if}
						<a href="{if $products[product].landingpageurl}{$http_location}/{$products[product].landingpageurl}{/if}">
                            <div class="item-box">
                                <span class="item-image item-image-border">
                                    <img src="{$cdn_base}/skin1/myntra_images/loading_240_320.gif" class="jquery-lazyload" alt="{$products[product].product}" height="240" width="180" original="{$products[product].searchimagepath}">
                                    <!--img src="{$cdn_base}/skin1/myntra_images/loading_240_320.gif" lazy-src="{$products[product].searchimagepath}" id="im_search_res_{$products[product].id}" class="lazy" alt="{$products[product].product}" height="320" width="240"-->
                                </span>

                                <div class="item-description">
                                    	<span class="name">{$products[product].product|truncate:55}</span>
                                        <div class="noncashback" style={if $cashback_gateway_status eq 'on' && $products[product].showcashback eq 'true'}"display:none;"{else}"display:block;"{/if}>
                                           {if $products[product].discount_type && $products[product].discount_value neq ''}
                                                <div class="dp">&#x20B9;{$products[product].discounted_price}</div>
                                                <div class="op">MRP &#x20B9;{$products[product].price}</div>
                                               {if $products[product].discount_label}
                                                    <div class="dpv">&#x20B9;{$products[product].price-$products[product].discounted_price} {$products[product].discount_label} Discount</span>
                                               {/if}
                                           {else}
                                            <div>&#x20B9;{$products[product].price}</div>
                                           {/if}
                                        </div>

									<div class="cashbackblock clearfix" style={if $cashback_gateway_status eq 'on' && $products[product].showcashback eq 'true'}"display:block;"{else}"display:none;"{/if}>
										<div class="pblock">
											{if $products[product].discount_type && $products[product].discount_value neq ''}
												<span class="discount-price highlight">Rs {$products[product].discounted_price}</span>
												<span class="original-price strike">Rs {$products[product].price}</span>
											{else}
												<span class="discount-price"><b class="smalltext">You Pay</b> Rs {$products[product].price}</span>
											{/if}
										</div>
										<div class="cb-block" style={if $products[product].cashback neq 0}"display:block;"{else}"display:none;"{/if}>
											<span class="cashtitle">Get 10% Cashback</span><span class="cashrs">Rs {$products[product].cashback|round|string_format:"%d"} <b class="cb">What's This?</b></span>
                                            <span class="cbtip hide"><b style="display: block;">Cashback</b> <img src="{$cdn_base}/skin1/images/cart-removebtn.png" alt="close" class="tipclose">On purchasing this product, you will receive a cashback credit upto the amount indicated here. Cashback is calculated on item value excluding any coupon discounts. You can redeem this on any of your subsequent purchases at Myntra.</span>
										</div>
									</div>
                                </div>

                            </div>
							{if $products[product].generic_type neq 'design'}
                                <span class="availability">AVAILABILITY: {if $products[product].sizes neq '' && $products[product].sizes neq ' '}{$products[product].sizes|replace:",":", "}{else}SOLD OUT{/if}</span>
                            {/if}
						</a>
					</li>
                    {/section}
            	</ul>
    		</div>
    		{if $total_items ge 1}
    		<div class="pagination-links right">
    					{if $current_page neq 1}
					    	<a href="{if $current_page-1 ge 1 && $current_page le $page_count}{$http_location}/{$query}{if $sort_param_in_url}/{$sort_param_in_url}{/if}/{$current_page-1}{/if}" rel="{if $current_page-1 ge 1 && $current_page le $page_count}/{$query}{if $sort_param_in_url}/{$sort_param_in_url}{/if}/{$current_page-1}{/if}" class="no-decoration-link pagination-prev" onclick="return setPagePrev(this);">&lsaquo; Prev</a>
					    {/if}
					    {section name=foo start=$start_page loop=$end_page+1 step=1}
					    {if $smarty.section.foo.index eq $current_page}
					    <span>{$smarty.section.foo.index}</span>
					    {else}
					    <a href="{$http_location}/{$query}{if $sort_param_in_url}/{$sort_param_in_url}{/if}/{$smarty.section.foo.index}" rel="/{$query}{if $sort_param_in_url}/{$sort_param_in_url}{/if}/{$smarty.section.foo.index}" class="no-decoration-link" onclick="return setPage('{$smarty.section.foo.index-1}', this);">{$smarty.section.foo.index}</a>
					    {/if}
					    {/section}
					    {if $current_page neq $page_count}
					    	<a href="{if $current_page+1 le $page_count && $current_page ge 1}{$http_location}/{$query}{if $sort_param_in_url}/{$sort_param_in_url}{/if}/{$current_page+1}{/if}" rel="{if $current_page+1 le $page_count && $current_page ge 1}/{$query}{if $sort_param_in_url}/{$sort_param_in_url}{/if}/{$current_page+1}{/if}" class="no-decoration-link {if $page_count eq 1}hide{/if} pagination-next" onclick="return setPageNext(this);">Next &rsaquo;</a>
					    {/if}
					</div>
			{/if}
    	</div>
    	{if $total_items ge 1}
    	<script type="text/javascript">
			document.getElementById("search-page-contents").style.visibility="hidden";
		</script>
		{/if}
    <div class="related-products left clearfix last span-33">
   	{literal}
	{RECENT_WIDGET_PLACEHOLDER}
    {/literal}
   	</div>
		{if $description && $page<2 && $total_items ge 1 && $description neq " "}
			<div class="mb10 last left span-33" id="search_description">
				<h2 class="h2g bold last">About {$query}</h2>
				<div class="icon-line mb10"></div>
				<p>{$description}</p>
			</div>
		{/if}
   	</div>
    <div class="divider">&nbsp;</div>
  
  
  <!--Popular tags-->
{*
<div class="clearfix p10">
		<h2 class="h2b prepend" id="popular-title">Popular</h2>
	    <div class="icon-line"></div>
	    <ul class="tags">
	    	{foreach name=tagtype item=fixedorDynamic from=$mostPopularWidget}
	        	{foreach name=uniquetag item=tag from=$fixedorDynamic}
	            	<li><a href="{$http_location}/{$tag.link_url}">{$tag.link_name}</a></li>
	            {/foreach}
	        {/foreach}
	    </ul>
	</div>
*}
    <div class="divider">&nbsp;</div>

</div>
  {include file="site/footer.tpl" }	 
{if $total_items ge 1}
<!--retarget pixel tracking-->
{if $retargetPixel.retargetHttp}
	<script type="text/javascript">
		$(window).load(function () {ldelim}
			$('#retargetSpan').html("{$retargetPixel.retargetHttp}");
		{rdelim});
	</script>
	<span id="retargetSpan" style="display:none;"></span>
{/if}
<!--retarget pixel tracking-->

{literal}
<script type="text/javascript">
	var total_pages={/literal}{$page_count}{literal};
	var query="{/literal}{$query|lower}{literal}";
	var pageTitleNoPage="{/literal}{$pageTitleNoPage}{literal}";
	var metaDescNoPage="{/literal}{$metaDescriptionNoPage}{literal}";
	var sortByDefault="";
	var pageDefault={/literal}{$current_page}-1{literal};
	if(pageDefault > total_pages){
		pageDefault=0;
	}
	var configArray=new Object();
	configArray["url"]=query;
	configArray["page"]={/literal}{$current_page}-1{literal};
	configArray["noofitems"]=24;
	configArray["gender"]=[];
	configArray["sizes"]=[];
	configArray["brands"]=[];
	configArray["pricerange"]={"rangeMin" : "{/literal}{$rangeMin}{literal}", "rangeMax" : "{/literal}{$rangeMax}{literal}"};
	sortByDefault="{/literal}{if $sort_param_in_url}{$sort_param_in_url}{/if}{literal}";
	/*if(query.indexOf(saleClearanceURL) >= 0 && sortByDefault == ""){
		sortByDefault=saleClearanceSortParam;
	}
	if(searchPageSortParam != "" && sortByDefault == ""){
		sortByDefault=searchPageSortParam;
	}*/
	configArray["sortby"]=sortByDefault;
	var trackingBooleans=new Object();
	trackingBooleans["gender"]=false;
	trackingBooleans["sizes"]=false;
	trackingBooleans["brands"]=false;
	trackingBooleans["pricerange"]=false;
	trackingBooleans["noofitems"]=false;
	var priceRangeMin=parseInt("{/literal}{$rangeMin}{literal}");
	var priceRangeMax=parseInt("{/literal}{$rangeMax}{literal}");
	var articleTypeAttributesList=[];
	var filterProperties=new Object();
	var innerhtml="";   
</script>
{/literal}
<script type="text/javascript">
	{if $article_type_attributes}
	{foreach key=attribute item=props from=$article_type_attributes}
		configArray["{$props.title|replace:' ':'_'}_article_attr"]=[];
		trackingBooleans["{$props.title|replace:' ':'_'}_article_attr"]=false;
		articleTypeAttributesList.push("{$props.title|replace:' ':'_'}_article_attr");
	{/foreach}
	{/if}
</script>
<script type="text/javascript">
	{if $article_type_attributes}
	{foreach key=attribute item=props from=$article_type_attributes}
		{literal}
		filterProperties["{/literal}{$props.title|replace:' ':'_'}_article_attr{literal}"]={
			getURLParam: function(){
				var ataStr=configArray["{/literal}{$props.title|replace:' ':'_'}_article_attr{literal}"].join("-");
				return (ataStr != "")?"{/literal}{$props.title|replace:' ':'_'}_article_attr{literal}"+"="+ataStr:"";
			},
			processURL: function(str){
				configArray["{/literal}{$props.title|replace:' ':'_'}_article_attr{literal}"]=str.split("-");
				return true;
			},
			updateUI: function(){
				//unselect all brands
				$("{/literal}{$props.title|replace:' ':'_'}_article_attr{literal}"+"-ata-filter .filter-box").removeClass("checked").addClass("unchecked");

				//update UI selection for brands in configArray
				for(var attr=0;attr<configArray["{/literal}{$props.title|replace:' ':'_'}_article_attr{literal}"].length;attr++){
					var selector=configArray["{/literal}{$props.title|replace:' ':'_'}_article_attr{literal}"][attr].toLowerCase().replace(/ /g, "-") + "-ata-filter";
					$("."+selector).removeClass("unchecked").addClass("checked");
				}
				return true;
			},
			setToDefault: function(){
				configArray["{/literal}{$props.title|replace:' ':'_'}_article_attr{literal}"]=[];
				return true;
			},
			resetParams: function(){
				configArray["page"]=0;
				return true;
		}};	
		{/literal}
	{/foreach}
	{/if}
</script>
{literal}
<!-- script type="text/javascript" src="{/literal}{$cdn_base}{literal}/skin1/js/myntra.search.min.2.0.js.jgz"></script-->
<script type="text/javascript" src="{/literal}{$http_location}{literal}/skin1/myntra_js/myntra.search.js"></script>
{/literal}
{/if}
</body>
</html>
