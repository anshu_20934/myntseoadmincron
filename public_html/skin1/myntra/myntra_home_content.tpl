  <div class="container clearfix">
    {if $homepageBannerType eq "static"}
    	{assign var=margintopval value=0}
    {else}
    	{assign var=margintopval value=-10}
    {/if}
    <div class="homepage-banners-col clearfix" style='margin-top:{$margintopval}px'>
    	{if $homepageBannerType eq "static"}
    	<div class="category-listing left theme-bg span-6 last">
    		<div class="title">SEE OUR RANGE IN</div>
    		{foreach key=display_category item=count from=$global_display_categories}
    			<a href="{$http_location}/{if $display_category|lower eq 't shirt' || $display_category|lower eq 't shirts' || $display_category|lower eq 't-shirts' || $display_category|lower eq 't-shirt'}{$display_category|lower|replace:' ':''|replace:'-':''}{else}{$display_category|lower|replace:' ':'-'}{/if}" class="no-decoration-link">{$display_category|lower|capitalize}</a>
    		{/foreach}
    	</div>
	    <div class="home-page-banners right span-27 last">
	    	{foreach name=bannerimages item=bannerimage from=$images}
	    		<a href="{$http_location}{$bannerimage.url}"><img id="im_banner_img_{$smarty.foreach.bannerimages.index}" src="{if $bannerimage.image|strpos:$cdn_base !== false}{$bannerimage.image}{else}{$http_location}/{$bannerimage.image}{/if}" alt="{$bannerimage.alt}" title="{$bannerimage.anchor}"></a>
	    	{/foreach}
	    </div>
	    {else}
	    <div class="slideshow-container span-33 left last">
	        <div id="slideshow" class="left">
	                {foreach name=bannerimages item=bannerimage from=$images}
	                    {if $smarty.foreach.bannerimages.index lt 2}
	                        <div class="flashbanner"><a href="{$bannerimage.url}"><img id="im_banner_img_{$smarty.foreach.bannerimages.index}" src="{if $bannerimage.image|strpos:$cdn_base !== false}{$bannerimage.image}{else}{$http_location}/{$bannerimage.image}{/if}" alt="{$bannerimage.alt}" title="{$bannerimage.anchor}"></a></div>
	                    {else}
	                        <div class="flashbanner"><a href="{$bannerimage.url}"><img id="im_banner_img_{$smarty.foreach.bannerimages.index}" src="{$cdn_base}/skin1/myntra_images/loading-graphic.gif" lazy-src="{if $bannerimage.image|strpos:$cdn_base !== false}{$bannerimage.image}{else}{$http_location}/{$bannerimage.image}{/if}" class="lazy" alt="{$bannerimage.alt}" title="{$bannerimage.anchor}"></a></div>
	                    {/if}
	                {/foreach}
	        </div>
	        <div id="nav" class="left last"></div>
	     </div>
	     <div class="span-7 last category-listing-sliding">
	     	<div class="title-block clearfix">
	     		<div class="title-top left"></div>
    			<div class="title left">SEE OUR RANGE IN</div>
    		</div>
    		<div class="category-listing-sliding-content">
    		{foreach key=display_category item=count from=$global_display_categories}
    			<div><span>&rsaquo;</span>
    			<a href="{$http_location}/{if $display_category|lower eq 't shirt' || $display_category|lower eq 't shirts' || $display_category|lower eq 't-shirts' || $display_category|lower eq 't-shirt'}{$display_category|lower|replace:' ':''|replace:'-':''}{else}{$display_category|lower|replace:' ':'-'}{/if}" class="no-decoration-link">{$display_category|lower|capitalize}</a>
    			</div>
    		{/foreach}
    		</div>
    	</div>
	    {/if}
     </div>
	{include file="site/brandlogos.tpl"}
    <!-- @Block2 -->
    <div class="content clearfix mt15">
      	{if $shownewui eq 'newui'} 
        	{include file="site/widget_B.tpl"}
        {else}
        	{include file="site/widget.tpl"}
        {/if}		
    </div>
    </div>
{include file="site/footer_content.tpl"}
