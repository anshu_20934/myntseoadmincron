<div class="preview">

         <div style="width:450px;height:450px;" id="preview_image_div">
                
                <img  src="{$http_location}/skin1/images/spacer.gif" alt="loading product preview" width="390" height="390" id="finalimage" style="margin:30;pxposition:relative;z-index:1;">
                <div id="ajax_load"><img src="{$cdn_base}/skin1/mkimages/ajax-loader.gif" alt="loader"></div>

            </div>



    <div class="preview-styles">
        {section name="area" loop=$area_icons}

                {if $area_icons[area].name eq $curr_sel_area}
		<div style="float:left;border:none;">
		     <span>{$area_icons[area].name}</span>
                     <div class="selected" id="area_{$area_icons[area].name}">
                        <p>
                            <a href="javascript:void(0)" onclick="javascript:changeAreaEx('{$area_icons[area].name}')">
                            <img width="40" height="40" src="{$area_icons[area].area_icon}" alt="img" >
                            </a>
                        </p>
                      </div>
		</div>
                {else}
		<div style="float:left;border:none;">
		     <span>{$area_icons[area].name}</span>		
                     <div id="area_{$area_icons[area].name}">
                        <p>
                            <a href="javascript:void(0)" onclick="javascript:changeAreaEx('{$area_icons[area].name}')">
                                <img width="40" height="40" src="{$area_icons[area].area_icon}" alt="">
                            </a>
                        </p>
                     </div>
		</div>

                {/if}
            
        {/section}




    </div>
    <input type="hidden" id="styledescription" name="description" value="({$productTypeDetails|escape}) {$productStyleDetails[0][3]|escape}" >
</div>
<script type="text/javascript">
	var currSelectedArea = "{$curr_sel_area}";
</script>
{literal}
	<script>
		function changeAreaEx_CB(data)
		{
			//check whether the area is customizable or not(has orientation or not)
			var res = data.trim();
			var result = res.split("##");
			if(result[1] == 1){
				showCustomizePanel();
			}else{
				showProductsPanel();
			}
			changeDisplayOrientations(windowid,curr_style_id);
			//ajaxCounter--;trackCustProgress();
		}
		function changeAreaEx(areaname)
		{
			if(currSelectedArea == areaname)
			{
				return;
			}
			$("#area_"+currSelectedArea).removeClass("selected");
			$("#area_"+areaname).addClass("selected");
			currSelectedArea = areaname;
			ajaxCall.get("change_area","./mkchangeselectedarea.php?windowid="+windowid+"&name="+areaname,changeAreaEx_CB);
		}
		//changed funtion
		function showCustomizePanel(){
			$("#customizeTab").show();
			if($("#customizeTab").children().hasClass("current")){
				$("#productsPanel").hide();
				$("#customizePanel").show();
			}
			else
				$("#productsTab a").trigger("click");
		}
		function showProductsPanel(){
			if($("#customizeTab").css("display") != 'none')
				$("#productsPanel").show();
			$("#customizeTab").hide();
			$("#customizePanel").hide();
			$("#productsTab a").trigger("click");
		}
		//end
	</script>
{/literal}
