<div id="learnMoreCashOnDelivery" style="display:none">
	<div class="overlay" style="display:block;_position:absolute;_width:1300px;_height:1500px;" onClick="Javascript:closeLearnMoreWindow()">&nbsp;</div>
	<div style="left:28%; position:fixed; _position:absolute; width:500px; background:white; top:20%; z-index:1006;border:2px solid #999999;">
		<div id="learnMoreCODContent" style="display:none;">
		    <div id="TB_title">
		    <div id="TB_ajaxWindowTitle">Cash on Delivery Option {if ($codErrorCode>0)} is Disabled {/if}</div>
		    <div id="TB_closeAjaxWindow"><a class="no-decoration-link" href="javascript:closeLearnMoreWindow()">close</a></div>
		    </div>
			
			<div class="clear">&nbsp;</div>
			<div id="aboutCOD" style="padding:5px 25px; text-align:left; font-size:12px" >
				{if $codErrorCode==7}
				Cash on delivery option is applicable only for shipping addresses in India.
				
				{elseif ($codErrorCode<=2)} 
				Cash on Delivery option allows you to pay cash against the delivery at your shipping address.<br/><br/>
				    You will get this option for payment only if your shipping address is serviceable and order value is between Rs. 300 and Rs. 10000<br><br>
				{elseif ($codErrorCode==6)}
					You have placed orders using COD option in recent past and there are outstanding payment(s) due for the same.<br/><br/> 
					Please order within available credit limit or user alternative payment modes.<br/><br/>
					Your credit will be extended again once you make the outstanding payments.
				{elseif ($codErrorCode==3)}
					There are certain products in your cart, for which COD option is not available. <br/>
				{else}
					There is an outstanding payment against your credentials.<br/><br/> 
					In order to enable this option please get in touch with customer support.
				{/if} 
			</div>
		</div>
	</div>
</div>


{literal}
	<script type="text/javascript">
	
	function showLearnMore() {
		$("#learnMoreCashOnDelivery").css("display","block");
		$("#learnMoreCODContent").css("display","block");
		$("#TB_title").css("display","block");
		gaTrackEvent("sports_pdp_"+gaSuffix,"cod_more_info","");
	}

	function closeLearnMoreWindow() {
		$("#learnMoreCashOnDelivery").css("display","none");
		$("#learnMoreCODContent").css("display","none");
	}
	
	</script>
{/literal}

