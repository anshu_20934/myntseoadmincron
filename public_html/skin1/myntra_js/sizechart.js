/******SIZE CHART *********/
Myntra.SizeChartBox = function(id, cfg) {
	var cfg = {isModal:false};
	var obj = Myntra.LightBox(id,cfg);
	var infoOpen,
	infoClose,
	infoBlock,
	sizeScroll,
	sizeSelLi,
	sizeSelBlock,	
	addToBtn,
	passObj,
	close,
	formToSubmit,
	sizeOutOfStock,
	sizeImgCont,
	equiSizeUL,
	equiSizeLI,
	loaded=false,
	mLinImg,
	countLi=0,
	noToScroll=5,
	initScroll=true,
	sizetoselect='',
	inOfSel=0;
	

	var addtocart=function(e,selSizeName){
		formToSubmit.find('.productSKUID').val(selSizeName);
		formToSubmit.find('.sizename').val(selSizeName);
		formToSubmit.trigger('submit');
	}
	
	obj.onAfterShow = function (){
		if(loaded){
			equiSizeLI.each(function(){
				var sizeSplit=$(this).html();
				//alert(sizeSplit.search('/')>0)
				if(sizeSplit.search('/')>0){
					sizeSplit=sizeSplit.replace('/','<br />')
					$(this).html(sizeSplit);
				}
			});
		
			$(equiSizeUL).each(function(){
				var h_=$(this).height();
			//	alert(h_);
				$(this).parent().height(h_);
			});
			inOfSel=sizeSelBlock.find('li.size-val-sel').index();
            if(inOfSel==-1){
                    inOfSel=sizeSelBlock.find('li.size-val-sel-inactive').index();
            }
		//inti scroll after load	
			if(sizeScroll.length){
				sizeScroll.scrollable({size:noToScroll,keyboard:false,clickable:false});
				if(countLi==noToScroll){$('#sizes-next').addClass('disabled');}
			}
		}
		
	}
	
	var p_show = obj.show;
	obj.show = function(sizeObj){
			sizetoselect = sizeObj.text;
			if(loaded){
				obj.sizeUpdate(sizeObj);
			}
			p_show.call(this);
			obj.onAfterShow();
			obj.scrollTo();
		
			
	};
	obj.scrollTo = function(){
		if(loaded){
			// Call for size scroller
				
				if(countLi>0){
				//SCROLL PAGE DETERMINE HERE
					if(inOfSel>=noToScroll && initScroll){
							sizeScroll.each(function(){
							var idSc= $(this).attr('id');
							var api =$('#'+idSc).data('scrollable');
							api.seekTo(inOfSel);
						
						});
					}
				}

			
			initScroll=false;
		}
	};
		
	//INIT METHOD 
	obj.init = function() {

		infoOpen=$('#info-open'),
		infoClose=$('#info-close'),
		infoBlock=$('#info-block'),
		sizeScroll=$(".size-scrollable"),
		sizeSelLi=$("#size-select li.size-available"),
		sizeOutOfStock=$("#size-select li.size-inactive"),
		sizeSelBlock=$("#size-select"),	
		addToBtn=$('#sub-btn .sub-btn'),
		close=$(id+' .close'),
		equiSizeLI=$('.equi-sizes li'),
		equiSizeUL='.equi-sizes',
		sizeImgCont=$('#measurement-box'),
		mLinImg=$('.size-image'),
		passObj={
			"text":sizeSelBlock.find('li.size-val-sel').text(),
			"flag":"fromPop",
			"val":sizeSelBlock.find('li.size-val-sel').length?sizeSelBlock.find('li.size-val-sel').attr('data-value'):''
		},
		formToSubmit=$('#sizeForm-'+curr_style_id);
		//alert('createMeasureBlocks');
		if(typeof sizePositionObj[Myntra.Data.ageGroup.toLowerCase()] !== 'undefined'){
			if(typeof sizePositionObj[Myntra.Data.ageGroup.toLowerCase()][Myntra.Data.articleType.toLowerCase()] !== 'undefined'){
				obj.createMeasureBlocks(sizePositionObj[Myntra.Data.ageGroup.toLowerCase()][Myntra.Data.articleType.toLowerCase()]);
			}
		}
		infoOpen.bind('click',function(){
			infoOpen.removeClass('active').addClass('inactive');
			infoClose.removeClass('inactive').addClass('active')
			infoBlock.slideDown();
		});
		
		infoClose.bind('click',function(){
			infoOpen.removeClass('inactive').addClass('active');
			infoClose.removeClass('active').addClass('inactive')
			infoBlock.slideUp();
		});
  

		sizeSelLi.bind("click",function(){
			_gaq.push(['_trackEvent','sizechart','sizechart', 'sizechart-size-available']);
			var sizeInd=$(this).index();
			var selVal=$(this).text();
			var jsonObj=Myntra.Data.sizeMeasurementsObj[sizeInd];
			obj.switchMeasurements(jsonObj,selVal);
			$(this).parent().find('li').removeClass('size-val-sel size-val-sel-inactive');	
			$(this).addClass('size-val-sel');
			passObj.val=$(this).attr('data-value');
			passObj.text=$(this).text();
			addToBtn.css('cursor','pointer').addClass('primary-btn').removeClass('disabled-btn');
			$('.out-of-stock').removeClass('visible');
			
		});
		sizeOutOfStock.bind("click",function(){
			_gaq.push(['_trackEvent','sizechart','sizechart', 'sizechart-size-unavailable']);
			var sizeInd=$(this).index();
			var selVal=$(this).text();
			var jsonObj=Myntra.Data.sizeMeasurementsObj[sizeInd];
			obj.switchMeasurements(jsonObj,selVal);
			$(this).parent().find('li').removeClass('size-val-sel size-val-sel-inactive');$(this).addClass('size-val-sel-inactive');
			passObj.val='';
			passObj.text='';
			addToBtn.css('cursor','not-allowed').addClass('disabled-btn').removeClass('primary-btn');
			$('.out-of-stock').addClass('visible');
		});
		
		addToBtn.bind('click',function(){
			_gaq.push(['_trackEvent','sizechart','sizechart', 'sizechart-buy-button']);
			if(sizeSelBlock.find('li.size-val-sel').length){
				passObj.val=sizeSelBlock.find('li.size-val-sel').attr('data-value');
			}
			if(passObj.val!=''){
				$(document).trigger('myntra.sizechart.addtocart',passObj.val);
			}
			else{
				return;
			}
		});
		//set width of the size li here
		//calculte the width
		
		countLi=sizeSelBlock.find('li').length;
		var liWidth=sizeSelBlock.find('li').width();
		if(countLi<noToScroll){
			var ulWidthToSet=(liWidth*countLi)+(countLi-1);
			//calculte the margin inorder to align to center
			var marginToSet=((168-ulWidthToSet)/2)-4;
			sizeScroll.css({'width':ulWidthToSet+'px','margin-left':marginToSet+'px','margin-right':marginToSet+'px'});
		}
		//Update Background of Equivalent sizes
		$(equiSizeUL+':odd').addClass('odd');
		//alert('init end');
		
	};
	
	obj.switchMeasurements = function(jsonObj,selVal){
		for(var key in jsonObj){
		
				if(jsonObj.hasOwnProperty(key)){
				switch(jsonObj[key]['type']){
					case "flat":
						if(jsonObj[key]['value']['value']!=null && jsonObj[key]['value']['value']!='' && jsonObj[key]['value']['value']!="null" )
						{		//show/hide the illustration lines here
							$('.'+key.replace(/ /g,'-').toLowerCase()+'-dotted').css({'visibility':'visible'});		
							  if($('span.'+key.replace(/ /g,'-').toLowerCase()).length){
								$('span.'+key.replace(/ /g,'-').toLowerCase()).hide();
								$('span.'+key.replace(/ /g,'-').toLowerCase()).html('<span >'+key+'</span><br/>'+jsonObj[key]['value']['value']+' '+(jsonObj[key]['value']['unit']=='Inches'?'inches':jsonObj[key]['value']['unit'])).fadeIn(1200);
							}
						//Give info incase of footwear
						if(Myntra.Data.articleTypeId==127 || Myntra.Data.articleTypeId==92 || Myntra.Data.articleTypeId==93 || Myntra.Data.articleTypeId==94 || Myntra.Data.articleTypeId==95 || Myntra.Data.articleType==96)
						 {  
							var infoText='* Foot size '+jsonObj[key]['value']['value']+' '+(jsonObj[key]['value']['unit']=='Inches'?'inches':jsonObj[key]['value']['unit'])+' fits Indian Size <b>'+selVal+'</b>';
							if($('.foot-size').length){
								$('.foot-size').fadeOut(function(){$(this).html(infoText).fadeIn();});
							}
							else{
								$('<span></span>').addClass('foot-size').html(infoText).appendTo(sizeImgCont);
							}
						 }	
						}
						break;
					case "variable":
						var variableKey=Myntra.Data.styleSizeAttributes[key];
						sizeImgCont.find('.'+key.replace(/ /g,'-').toLowerCase()).hide();
						sizeImgCont.find('.'+key.replace(/ /g,'-').toLowerCase()).html('<span>'+key+'</span><br/>'+jsonObj[key][variableKey]['value']+' '+(jsonObj[key][variableKey]['unit']=='Inches'?'inches':jsonObj[key]['value']['unit'])).fadeIn(1200);
						break;
					default:
						break
				}
			}
		}
	};
	
	obj.createMeasureBlocks = function(mPosObj){
		var blockHeight=400;
		var index=1;
		for(var key in mPosObj){
			if(mPosObj.hasOwnProperty(key)){
				//console.log('key->',key,'<===> value->',mPosObj[key]);
				$('<span></span>').addClass('measurement '+key.replace(/ /g,'-').toLowerCase()).css({'left':mPosObj[key][0]+'px','top':mPosObj[key][1]+'px'}).appendTo(sizeImgCont);
				//Create Illustration block and position it use sizePositionObj[indexed]
				$('<span></span>').addClass('size-image '+key.replace(/ /g,'-').toLowerCase()+'-dotted').css({'background-position':'0 -'+(blockHeight*index)+'px','visibility':'hidden'}).appendTo(sizeImgCont);
			}
			index=index+1;
		}
		
	};
	
	obj.sizeUpdate = function(sizeObj){
		var selCount=0;
		//alert('sizeupdate');
		if(sizeObj.text!=''){
			sizeSelLi.each(function(){
				
				$(this).removeClass('size-val-sel');
				sizeOutOfStock.removeClass('size-val-sel-inactive');
				if($(this).text()==sizeObj.text){
					$(this).addClass('size-val-sel');
					var sizeInd=$(this).index();
					var selVal=$(this).text();
					var jsonObj=Myntra.Data.sizeMeasurementsObj[sizeInd];
					obj.switchMeasurements(jsonObj,selVal);
					passObj.val=$(this).attr('data-value');
					addToBtn.css('cursor','pointer').addClass('primary-btn').removeClass('disabled-btn');
					$('.out-of-stock').removeClass('visible');
				}
			
			});
		}
		else 
		{ 
			if(!sizeSelBlock.find('li.size-val-sel-inactive').length){
				selCount=sizeSelLi.length;
				if(selCount>1){
					var midVal=Math.ceil(selCount/2);
					var sizeInd=sizeSelBlock.find('li.size-available:eq('+(midVal-1)+')').index();
					var selVal=sizeSelBlock.find('li.size-available:eq('+(midVal-1)+')').text();
					var jsonObj=Myntra.Data.sizeMeasurementsObj[sizeInd];
					obj.switchMeasurements(jsonObj,selVal);
					sizeSelLi.removeClass('size-val-sel size-val-sel-inactive');
					sizeSelBlock.find('li.size-available:eq('+(midVal-1)+')').addClass('size-val-sel');
				}
				else if(selCount==1){
					var sizeInd=sizeSelLi.index();
					var selVal=sizeSelLi.text();
					var jsonObj=Myntra.Data.sizeMeasurementsObj[sizeInd];
					obj.switchMeasurements(jsonObj,selVal);
					sizeSelLi.addClass('size-val-sel');
				}
			}
		}
		
	};
	
	//Override beforeHide method
	obj.beforeHide = function(){
		if(sizeSelBlock.find('li.size-val-sel').length){
			passObj.text=sizeSelBlock.find('li.size-val-sel').text();
			passObj.val=sizeSelBlock.find('li.size-val-sel').text();
			$(document).trigger('myntra.sizechart.done', passObj);
		}
		else if(sizeSelBlock.find('li.size-val-sel-inactive').length){
			passObj.text=sizeSelBlock.find('li.size-val-sel-inactive').text();
			$(document).trigger('myntra.sizechart.done', passObj);
		}
	};
	
	obj.loadMainImage = function(){
		$('.size-image').css('background-image','url('+Myntra.Data.sizeImg+')');
	};
	
	obj.loadOnPage =function(sizeObj){
		//var sizeChartUrl=http_loc+"/size_scaling.php?styleid="+sizeObj.styleid+"&options="+JSON.stringify(allProductOptionDetails);
	    	$.ajax({
			type:"POST",
			url:http_loc+"/size_scaling.php",
			data:"styleid="+sizeObj.styleid+"&options="+JSON.stringify(allProductOptionDetails),
			beforeSend:function(){
				//alert('beforesend');
				obj.showLoading();
			},
			success: function(data){
				//alert('success');
				//obj.show();
				if(data)
				{	
					if(!$('#lb-sizechart .size-scrollable-box').length){
						$('#lb-sizechart .mod').append(data);
						obj.init();
						obj.center();
						obj.loadMainImage();
								
					}
				}
			},
			complete: function(){
				obj.hideLoading();
				sizeObj.text=sizetoselect;
				obj.sizeUpdate(sizeObj);
				loaded=true;
				obj.onAfterShow();
				
			}
		});
		
	};

	
	//EVENTS FOR COMMUNICATION WITH BASE PAGE
	//$(document).bind('myntra.sizechart.selectsize',selectSize);
	$(document).bind('myntra.sizechart.addtocart',addtocart);
		return obj;
};
	
Myntra.InitSizeChart = function(styleObj) {
//alert('InitSizechart');
	$('body').append('<div id="lb-sizechart" class="lightbox lb-sizechart"><div class="mod"></div></div>');
	var obj = Myntra.SizeChartBox("#lb-sizechart");
	//Handle the size from PDP to popuo here
	//FIRE this event while calling the sizechart, Send the selcted size to the popup
	$(document).bind('myntra.sizechart.show', function(e,data) {
		obj.show(data);
	});	
	$(document).bind('myntra.sizechart.load', function(e,data) {
		obj.loadOnPage(data);
	});	
};


Myntra.InitPDP = function() {
var passObj ={};
	$("#sizechart-new").click(function(e){
		e.preventDefault();
		_gaq.push(['_trackEvent','sizechart','sizechart', 'sizechart-pdp-link']);
		passObj = {
			"text":$('#size-options .selected').length?$('#size-options .selected').text():'',
			"val":'',
			"styleid":curr_style_id
		};
		$(document).trigger('myntra.sizechart.show', passObj);//Firing the event to show popup also send selcted data along with it.
	});
	passObj = {
			"text":$('#size-options .selected').length?$('#size-options .selected').text():'',
			"val":'',
			"styleid":curr_style_id
		};
	$(document).trigger('myntra.sizechart.load', passObj);//load sizechart behind on page load

	$(document).bind('myntra.sizechart.done', function(e,data) {
		//Handle the size from POPUP to PDP here
		//FIRE this event inside Popup
		var carry=false;
		$('.zoom-flat-size-options a').removeClass('selected');
		$('#size-options a').each(function(){
			$(this).removeClass('selected');
			if($(this).text()==data.text){
				$(this).addClass('selected');
				$(this).trigger('click');
				carry=true;
			}
		});
		if(!carry){
			$('#productSKUID').val('');
		}
	});
};

$(document).ready(function() {
	//alert('ready');
	Myntra.InitSizeChart();
	Myntra.InitPDP();
	
});


