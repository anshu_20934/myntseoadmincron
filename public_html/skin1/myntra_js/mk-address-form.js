
Myntra.AddressFormBox = function(id, cfg) {
    cfg = cfg || {};
    cfg.css = cfg.css || '';

    var isInited = false, form, errors, elements, cache = {}, inprogress = {},
        a_id, a_name, yourname, address, locality, selectedLocality, city, 
        state, stateTxt, country, pincode, pinerr, email, mobile,
        errPincode, pinwarn,
        idstr = id.substr(0, 1) === '#' ? id.substr(1) : id,
        obj,
        markup = [
        '<div id="' + idstr + '" class="mk-lightbox address-form-box ' + cfg.css + '">',
        '<div class="mod">',
        '  <div class="hd">',
        '    <div class="title"></div>',
        '    <div class="subtitle"></div>',
        '  </div>',
        '  <div class="bd">',
        '  </div>',
        '  <div class="ft">',
        '    <span class="note"></span>',
        '    <button class="btn normal-btn btn-save">Save</button>',
        '    <button class="btn normal-btn btn-cancel">Cancel</button>',
        '  </div>',
        '</div>',
        '</div>'
    ].join('');
    $('body').append(markup);
    obj = Myntra.LightBox(id, cfg);
    obj.root = $(id);
    obj.set('ajax_url', '/myntra/ajax_address.php');

    var hd = $('.mod > .hd', obj.root),
        bd = $('.mod > .bd', obj.root),
        ft = $('.mod > .ft', obj.root),
        title = $('.mod > .hd > .title', obj.root),
        subTitle = $('.mod > .hd > .subtitle', obj.root),
        ftNote = $('.mod > .ft > .note', obj.root),
        btnSave = $('.btn-save', obj.root),
        btnCancel = $('.btn-cancel', obj.root);

    obj.setTitle = function(markup) { title.html(markup) };
    obj.setSubTitle = function(markup) { subTitle.html(markup) };
    obj.setBody = function(markup) { bd.html(markup) };
    obj.setFootNote = function(markup) { ftNote.html(markup) };

    obj.enableSave = function() { btnSave.removeAttr('disabled') };
    obj.disableSave = function() { btnSave.attr('disabled', 'disabled') };
    obj.showSave = function() { btnSave.removeClass('hide') };
    obj.hideSave = function() { btnSave.addClass('hide') };
    obj.setSaveText = function(txt) { btnSave.text(txt) };

    btnSave.click(function(e) {
        obj.onSaveClick();
    });

    btnCancel.click(function(e) {
        obj.hide();
    });

    var lockCityState = function() {
        state.addClass('hide');
        stateTxt.removeClass('hide');
        stateTxt.attr('disabled', 'true');
        city.attr('disabled', 'true');
    };

    var unlockCityState = function() {
        state.removeClass('hide');
        stateTxt.addClass('hide');
        stateTxt.removeAttr('disabled');
        city.removeAttr('disabled');
    };

    var stateName = function() {
        return $('select[name="state"] option:selected', obj.root).text();
    };

	var locSuccess = function(data) {
		var loc = locality.val();
        if (loc.length && $.inArray(loc, data.locality) === -1) {
            locality.val('');
        }
		locality.setOptions({
			data: data.locality
		});
		city.val(data.city);
		state.val(data.state);
        stateTxt.val($('select[name="state"] option:selected', obj.root).text());
        if (!data.locality.length) {
            unlockCityState();
        }
	};

    var locFetch = function(pcode) {
        if (inprogress[pcode]) return;
        inprogress[pcode] = 1;
		$.ajax({
			url: "/myntra/ajax_pincode_locality.php?pincode=" + pcode,
			dataType: "json",
			beforeSend: function() {
				locality.attr('disabled', 'disabled');
				locality.addClass('ac_loading');
                lockCityState();
			},
			success: function(data) {
				cache[pcode] = data;
				locSuccess(data);
				locality.trigger('showautocomplete');
			},
			complete: function(xhr, status) {
				locality.removeAttr('disabled');
				locality.removeClass('ac_loading');
                delete inprogress[pcode];
			}
		});
    };

    var locInit = function() {
        locality.autocomplete([], {
            minChars: 0
        });
        pincode.blur(function() {
            var pcode = pincode.val();
            if (!/\d{6}/.test(pcode)) {
                return
            }
            if ($(this).data('pcode') === pcode) {
                return;
            }
            $(this).data('pcode', pcode);
            if (pcode in cache) {
                locSuccess(cache[pcode]);
                locality.trigger('showautocomplete');
                return;
            }
            locality.setOptions({data:[]});
            locFetch(pcode);
        });
        locality.bind('result', function(e, data) {
            selectedLocality.val(data[0]);
        });
        locality.focus(function(e) {
            var pcode = pincode.val();
            if (pcode in cache) {
                locality.trigger('showautocomplete');
            }
            else {
                $(this).setOptions({data:[]});
                locFetch(pcode);
            }
        });
        // fetch the locality in Edit mode
        var pcode = pincode.val();
        if (pcode) { locFetch(pcode); }
    };

    var resetPinErr = function() {
        pinwarn.hide();
        pinerr.val('0');
        errPincode.text('');
        obj.setSaveText('Save');
    };

    var toggleYourName = function(flag) {
        if (flag) {
            $('.copyname', obj.root).show();
            $('.yourname', obj.root).show();
        }
        else {
            $('.copyname', obj.root).hide();
            $('.yourname', obj.root).hide();
        }
    };

    var validatePincode = function() {
        var pincodeVal = pincode.val(),
            stateVal = state.val(),
            pass = false;

        if (stateVal && /\d{6}/.test(pincodeVal)) {
            var zips = window.stateZipsJson["IN-" + stateVal];
            for (var index in zips) {
                var eachZip = zips[index];
                if (eachZip == pincodeVal.substring(0, eachZip.length)) {
                    pass = true;
                    break;
                }
            }
        }
        else {
            pass = true;
        }

        if (!pass) {
            errPincode.text('Pincode is invalid for state');
            $('p', pinwarn).text("The pincode you've entered doesn't seem to belong to " + stateName() + ". If the pincode is not correct, your delivery might get delayed.");
            pinwarn.show();
            pinerr.val('1');
            obj.setSaveText('Save Anyways');
        }
        else {
            resetPinErr();
        }

        return pass;
    }

    obj.onSaveClick = function() {
        if (pinerr.val() == '0') {
            if (!validatePincode()) {
                _gaq.push(['_trackEvent', 'address', 'address_save_failed', 'pincode_validation_error']);
                return;
            }
        }
        var params = {_view:'address-form', _type:cfg._type};
        elements.each(function(i) {
            var el = $(this);
            if (el.attr('name') == 'state-txt') {
                return;
            }
            else if (el.attr('name') == 'copy-name') {
                if ($('.copyname', obj.root).is(':hidden')) {
                    return;
                }
                else if (!el.prop('checked')) {
                    return;
                }
            }
            params[el.attr('name')] = el.val();
        });
        obj.ajax(params, 'submit', 'POST');
    };

    obj.onAjaxError = function(resp, name) {
        if (name === 'submit') {
            if (resp.errors) {
                errors.text('');
                elements.removeClass('error');
                $.each(resp.errors, function(key, val) {
                    $('.err-' + key, obj.root).text(val);
                    $('.' + key, form).addClass('error');
                });
            }
            else {
                alert(resp.message);
            }
        }
    };

    obj.onAjaxSuccess = function(resp, name) {
        if (name === 'init') {
            isInited = true;
            
            obj.setBody(resp.markup);
            obj.setFootNote('Need help? ' + resp.customerSupportTime + ' on <em>' + resp.customerSupportCall + '</em>');
            if (resp.stateZips) {
                window.stateZipsJson = resp.stateZips;
            }

            form = $('form', obj.root);
            errors = $('.err', obj.root);
            pinwarn = $('.pin-warn', obj.root);
            errPincode = $('.err-pincode', obj.root);
            
            a_id = $('input[name="id"]', obj.root);
            a_name = $('input[name="name"]', obj.root);
            yourname = $('input[name="yourname"]', obj.root);
            address = $('textarea[name="address"]', obj.root);
            city = $('input[name="city"]', obj.root);
            email = $('input[name="email"]', obj.root);
            mobile = $('input[name="mobile"]', obj.root);
            pincode = $('input[name="pincode"]', obj.root);
            locality = $('input[name="locality"]', obj.root);
            selectedLocality = $('input[name="selected-locality"]', obj.root);
            state = $('select[name="state"]', obj.root);
            stateTxt = $('input[name="state-txt"]', obj.root);
            pinerr = $('input[name="pinerr"]', obj.root);

            elements = $('input, select, textarea', obj.root);
            elements.each(function(i) {
                var el = $(this),
                    er = $('.err-' + el.attr('name'), obj.root);
                el.data('err', er);
            });
            elements.blur(function() {
                var el = $(this);
                if (el.val().length > 0) {
                    el.removeClass('error');
                    el.data('err').text('');
                }
            });

            toggleYourName(resp.promptYourName);
            locInit();
            $(pincode).change(function(e) {
                resetPinErr();
            });
            $(state).change(function(e) {
                resetPinErr();
            });
            obj.center();
            try { pincode.focus() } catch (ex) {}
        }
        else if (name === 'load') {
            var d = resp.data;
            errors.each(function(i, el) { el.innerHTML = ''; });
            resetPinErr();
            toggleYourName(resp.promptYourName);

            a_id.val(d.id);
            a_name.val(d.name);
            yourname.val(d.yourname);
            address.val(d.address);
            locality.val(d.locality);
            city.val(d.city);
            state.val(d.state);
            stateTxt.val(stateName());
            pincode.val(d.pincode);
            email.val(d.email);
            mobile.val(d.mobile);

            obj.center();
            try { pincode.focus() } catch (ex) {}
        }
        else if (name === 'submit') {
            obj.hide();
            $(document).trigger('myntra.address.form.done', resp);
        }
    };

    var p_show = obj.show;
    obj.show = function(a_cfg) {
        $.extend(cfg, a_cfg);
        obj.setTitle('Create a ' + cfg._type + ' address')
        obj.setSubTitle('&nbsp;');
        p_show.apply(obj, arguments);

        var params = {_view:'address-form', _action:cfg._action};
        if (cfg._action == 'edit') { params.id = cfg.id; }
        if (!window.stateZipsJson) { params['state-zips'] = 1; }
        params.markup = isInited ? 'no' : 'yes';
        
        isInited ? obj.ajax(params, 'load') : obj.ajax(params, 'init');
    };

    return obj;
};

$(document).ready(function() {
    var obj;
    $(document).bind('myntra.address.form.show', function(e, cfg) {
        obj = obj || Myntra.AddressFormBox('#lb-address-form', cfg);
        obj.show(cfg);
    });
});

