$(document).ready(function(){
	/* Scrollable */
	$(".vertical-scrollable").scrollable({size:3,vertical:true,keyboard:false});
	
	//Fix for vertical scrollable to disable next button on initial load if the number of items is exactly equal to the size of the window
	$(".vertical-scroller").each(function(){
		if($(".vertical-scrollable .items div",this).length <= 3){
			$(".next",this).addClass("disabled");
			$(this).addClass("scroller-disabled");
			$(".product-image-tabs").removeClass("corners");
			$(".product-image-tabs").addClass("corners-tl-o-bl-br");
		}
	});
		
	$(".vertical-scroller.scroller-disabled .items div a").click(function(){
		if($(this).hasClass("first")){
			$(".product-image-tabs").removeClass("corners");
			$(".product-image-tabs").addClass("corners-tl-o-bl-br");
		}
		else{
			$(".product-image-tabs").removeClass("corners-tl-o-bl-br");
			$(".product-image-tabs").addClass("corners");
		}
	});
	
    /* Product Description Page Gallery */
    $("div.tabs").tabs("div.product-image-tabs > div");
    
    /* Change Player Name Dropdown */
    $("#preview-player").change(function() {
    	
    });
    
    /* Change T shirt Type */
    $("tshirt-type").change(function() {
    	
    });
    
    /* Selectbox Custon Styles */
    //$(".filter-select").selectbox();
    
    var toggle_text_elements = {};
    
    /* Toggle Text */
    toggle_text_all(".toggle-text");
 
    /* 
     * This function finds all HTML elements with "toggle-text" class and binds the "focus"/"blur" events for
     * toggling the default text in those elements.
     */
    function toggle_text_all(selector){
    	$(selector).each(function(e){
    		toggle_text_elements[this.id] = $(this).val();
    		addToggleText(this);
    	});
    }

    function addToggleText(elem){
    	$(elem).addClass("blurred");
    	$(elem).focus(function(){
    		if($(this).val() == toggle_text_elements[this.id]){
    			$(this).val("");
    			$(this).removeClass("blurred");
    		}
    	}).blur(function(){
    		if($(this).val() == ""){
    			$(this).addClass("blurred");
    			$(this).val(toggle_text_elements[this.id]);
    		}
    	});
    }


});