
$(window).load(function(){$.getScript(('https:' == document.location.protocol ? 'https://' : 'http://') + 'apis.google.com/js/plusone.js');});
$(document).ready(function(){
	
	_gaq.push(['_trackEvent','pdp_page_visit', window.location.toString(), null, null, true]);
	
	/* 24 hrs delivery popup */
	$(".deliveryknowmore-link").click(function(){
		$(".deliveryknowmore-popup").show();
		$(".deliveryknowmore-overlay").show();
	});

	/*  30 Days return policy popup */
	$(".ReturnPolicy-popup-link").click(function(){
		$(".ReturnPolicy-popup").show();
		$(".ReturnPolicy-overlay").show();
	});
	
	/*  article type disclaimer popup */
	$(".articletype-disclaimer-link").click(function(){
		$(".articletype-disclaimer-popup").show();
		$(".articletype-disclaimer-overlay").show();
	});

	/* Cashback message top popup */
	$('.poplink').click(function(){
		$(".cashback-popup").show();
		$(".cashback-overlay").show();
    });

	$(".articletype-disclaimer-overlay, .deliveryknowmore-overlay, .ReturnPolicy-overlay").click(function(){
		closeDeliveryKnowMore();
	});
        
	$("#disc_tool_tip").click(function(){
        gaTrackEvent('pdp_discount_combo_set_'+gaSuffix,'scroll_to','');
        $(".pdp-tooltip").fadeOut();
		Myntra.PDP.Utils.scrollToBottom($(".discount_product_set_h2").offset().top);
	});   
        
        Myntra.InitPDPComboOverlay();

});

Myntra.InitPDPComboOverlay = function(){        
	$(".combo-overlay-btn").click(function(){
                var styleId = $(this).attr("data-styleid");
                //get all styleIds and pass it to 
		if(typeof styleId != "undefined" || styleId != ""){
			Myntra.PDPComboOverlay.loadOverlay(styleId);
        	}
	});
}

Myntra.PDPComboOverlay = (function(){
    var obj = {};
    var lb;
    
    obj.loadOverlay = function(id){
    	lb = Myntra.LightBox('#cart-combo-overlay');
    	obj.getData(id);
    };
    
    obj.getData = function(id){
    	lb.show();
    	lb.clearPopupContent();
    	lb.showLoading();
        $.get( http_loc+"/getPdpComboOverlay.php?sid="+id, null, function(data){
		lb.hideLoading();
                $("#cart-combo-overlay .bd").html(data);
    			lb.center();
                Myntra.Combo.ComboOverlay.Init();
    	});
    };
    
    return obj;
})();


Myntra.PDP.Utils.scrollToBottom = function(target_bottom){
	Myntra.PDP.scrollInProgress=true;
	$('html, body').animate({scrollTop:target_bottom}, 800, function(){
		Myntra.PDP.scrollInProgress=false;
	});
}

function closeDeliveryKnowMore(){
	$(".deliveryknowmore-popup").hide();
	$(".deliveryknowmore-overlay").hide();

	$(".ReturnPolicy-popup").hide();
	$(".ReturnPolicy-overlay").hide();

	$(".cashback-popup").hide();
	$(".cashback-overlay").hide();
	
	$(".articletype-disclaimer-popup").hide();
	$(".articletype-disclaimer-overlay").hide();
}


function sizeSelect(id,optidx,el){
	selectSizeNew(id,optidx,el);
	changeSizeNew(id,optidx,el);
}

function selectSizeNew(id,optidx,el){
	$(".flat-size-options a").removeClass("selected");
	$(".flat-size-options a:nth("+(optidx-1)+")").addClass("selected");
	if($(".size-options-error-msg").css("display")=="block"){
		$(".size-options-error-msg").slideUp();
		if($(".flat-size-options").hasClass("size-options-error")){
			$(".flat-size-options").removeClass("size-options-error");
		}
	}
	$(".sizequantity").val('');
	var quantity = document.getElementById("quantity").value;
	$("#sizequantity"+id).val(quantity);
	$("#productSKUID").val(id);
}

function changeSizeNew(id,optidx,el){
	$(".zoom-flat-size-options a").removeClass("selected");
	$(".zoom-flat-size-options a:nth("+(optidx-1)+")").addClass("selected");
	if($(".zoom-flat-size-options").hasClass("size-options-error")){
		$(".zoom-flat-size-options").removeClass("size-options-error");
	}
	$(".sizequantity").val('');
	var quantity = document.getElementById("quantity").value;
	$("#sizequantity"+id).val(quantity);
	$("#productSKUID").val(id);
}
/*
function selectSize(id){
	if(id>0){
		$(".sizequantity").val('');
		var quantity = document.getElementById("quantity").value;
		$("#sizequantity"+id).val(quantity);
		$("#productSKUID").val(id);
		$(".zoom-pdp-select-size").val($(".pdp-select-size "+"#size_option"+id).attr("value"));
	}
	else{
		$(".zoom-pdp-select-size").val("select");
	}
	gaTrackEvent("sports_pdp_"+gaSuffix,"select_size",$(".pdp-select-size :selected").text());
}

function changeSize(id){
	if(id>0){
		$(".pdp-select-size").val($(".pdp-select-size "+"#size_option"+id).attr("value"));
		selectSize(id);
	}
	else{
		$(".pdp-select-size").val("select");
	}
}
*/
function ifSizeOptionSelected(){
		if($("#productSKUID").val() == ""){
			$(".size-options-error-msg").slideDown();
			if(!$(".flat-size-options").hasClass("size-options-error")){
				$(".flat-size-options").addClass("size-options-error");
			}
			if(!$(".zoom-flat-size-options").hasClass("size-options-error")){
				$(".zoom-flat-size-options").addClass("size-options-error");
			}
			return false;
		}
		else{
			return true;
		}
}

$(document).ready(function() {

	$(".size-chart-content").hide();

	$(".cart-btn").click(function(event){
		if(ifSizeOptionSelected()){
			$('#hiddenForm').trigger('submit');
			//_gaq.push(['_trackEvent', 'sports_pdp_'+gaSuffix, 'buy_now_'+zoomMode, '{$isreturninguser}']);
			_gaq.push(['_trackEvent', 'buy_now', Myntra.Data.pageName, zoomMode]);
		}

		event.stopPropagation();
	});
	$(".zoom-buy-now-block").click(function(event){
		event.stopPropagation();
	});

});

function changePreviewImage(url,el)
{
	if(el.hasClass("selected")){
		return;
	}
	$(".thumb-views .selected").removeClass("selected");
	el.parent().addClass("selected");
	$("#finalimage").attr("src",url);
}


var docHeight;
var docWidth;
var winWdith;
var winHeight;
var iZoomWidth=540;
var iZoomHeight=720;
var sZoomWidth=980;
var sZoomHeight=1306;
//var superZoomMaxHeight = sZoomHeight-($(window).height()-zoomBuyNowBlockHeight);
var superZoomMaxHeight = sZoomHeight-($(window).height());

$(document).ready(function(){
	winWidth=$(window).width();
	winHeight=$(window).height();
	docHeight=$(document).height();
	docWidth=$(document).width();
});
function changeProductImage(imgsrc){
	$("#finalimage").attr("src",imgsrc);
}
function showOverlay(){
	$(".zoom-overlay").removeClass("hide").addClass("show");
}
function showZoomBlock(){
	$(".zoom-block").removeClass("hide").addClass("show");
}
function hideOverlay(){
	$(".zoom-overlay").removeClass("show").addClass("hide");
}
function hideZoomBlock(){
	$(".zoom-block").removeClass("show").addClass("hide");
}
function setZoomWindowSize(){
	var containerWidth=winWidth/2.5;
	var winHeight=$(window).height();
	var ratio=(iZoomHeight/winHeight);
	var iZoomWidthTemp=iZoomWidth/ratio;
	$(".zoom-container").css({"width":iZoomWidthTemp+"px", "height":winHeight});
	$(".zoom-buy-now-block").css("width",iZoomWidthTemp+"px");
	$(".zoom-buy-now-block").css("left","0px");
	$(".zoom-container .izoom").css({"width":iZoomWidthTemp+"px", "height":winHeight});
}

function setSuperZoomWindowSize(){
	var superZoomHeight=(docHeight>sZoomHeight)?sZoomHeight:docHeight;
	$(".zoom-buy-now-block").css("width",$(".zoom-container").width()+"px");
	$(".zoom-buy-now-block").css("left",((sZoomWidth-$(".zoom-container").width())/2)+"px");
	$(".zoom-container").css({"width":sZoomWidth+"px", "height":winHeight+"px"});
}

function setZoomMode(mode){
	zoomMode=mode;
}
var zoomMode="normal";
var noOfViews;
var thumbimginfo = new Object();
$(document).ready(function(){
	$(this).keydown(function(event){
		if (document.activeElement.nodeName == 'TEXTAREA' || document.activeElement.nodeName == 'INPUT')
			return true;
    	if(event.keyCode == '27'){//esckey pressed
			event.preventDefault();
			if(zoomMode!="normal"){
				closeAllZoom();
			}
    	}
    	else if(event.keyCode == '37'){//leftkey pressed
    		event.preventDefault();
    		if(zoomMode=="normal"){
        		if(!$(".preview-prev").hasClass("hide")){
    				var nextview=returnPrevViewToLoad("preview");
        			previewLoadView(nextview);
        		}
            }
    		else{
    			if(!$(".prev").hasClass("hide")){
    				var nextview=returnPrevViewToLoad();
        			loadView(nextview);
    			}
        	}
        }
    	else if(event.keyCode == '39'){//rightkey pressed
    		event.preventDefault();
    		if(zoomMode=="normal"){
        		if(!$(".preview-next").hasClass("hide")){
    				var nextview=returnNextViewToLoad("preview");
        			previewLoadView(nextview);
        		}
            }
    		else{
    			if(!$(".next").hasClass("hide")){
    				var nextview=returnNextViewToLoad();
        			loadView(nextview);
    			}
        	}
        }
    });

	$(window).resize(function(){
		if(zoomMode=="izoom"){
			setZoomWindowSize();
		}
	});
	noOfViews=$(".thumb-views a").length-1;
	initNextPrevButtons();
	initPreviewNextPrevButtons();
	$("#finalimage").click(function(){
		loadIZoom();
		$("html").css("overflow","hidden");
		$("body").css("overflow","hidden");
		//remove the shifting of content behind the overlay. overflow hidden property for body removes scrollbars
		$("body").css("margin-left","-17px");
	});
	$(".close-zoom-overlay").click(function(){
		closeAllZoom();
	});

	function closeAllZoom(){
		hideOverlay();
		hideZoomBlock();
		$(".superzoom").hide();
		$(".izoom").hide();
		setZoomMode("normal");
		var selectedval=$(".v-tabs a.selected").attr("id");
		$(".loadedimage").val(selectedval);
		$("html").css("overflow","auto");
		$("body").css("overflow","visible");
		//remove the extra margin added to prevent shifting of content behind the overlay. overflow hidden property for body removes scrollbars
		$("body").css("margin-left","0px");
	}

	$.each($(".thumb-views a"),function(){
		if(typeof thumbimginfo[$(this).attr("id")] == "undefined"){
			thumbimginfo[$(this).attr("id")] = $.parseJSON('{'+$(this).attr("rel")+'}');
		}
	});
	$(".thumb-views a").click(function(){
		var thumbIdx=$(this).attr("id");
		if(typeof thumbimginfo[thumbIdx] == "undefined"){
			thumbimginfo[thumbIdx] = $.parseJSON('{'+$(this).attr("rel")+'}');
		}
		$(".preview-loading").show();
		updatePreviewNextPrevButtons(thumbIdx.substr(thumbIdx.indexOf("_")+1, thumbIdx.length));
		loadPreviewImg(thumbimginfo[thumbIdx].prodimage, thumbIdx);
		gaTrackEvent("sports_pdp_"+gaSuffix,"switch_view","");
	});

	$(".izoom").click(function(){
		loadSuperZoom();
		scrollToCenter();
	});

	$(".superzoom").click(function(){
			setZoomWindowSize();
			$(".superzoom").hide();
			$(".izoom").fadeIn(250);
			setZoomMode("izoom");
			$("html").css("overflow","hidden");
			$("body").css("overflow","hidden");
			//remove the shifting of content behind the overlay. overflow hidden property for body removes scrollbars
			$("body").css("margin-left","-17px");
	});

	$(".superzoom").mousemove(function(event){
		var yPos=event.pageY;
			//yPos=(yPos*(superZoomMaxHeight/(winHeight-zoomBuyNowBlockHeight)));
			yPos=(yPos*(superZoomMaxHeight/(winHeight)));
			$(this).css({"top":-yPos,"left":0});
	});
	$(".prev").click(function(){
		var nextview=returnPrevViewToLoad();
		loadView(nextview);
	});
	$(".next").click(function(){
		var nextview=returnNextViewToLoad();
		loadView(nextview);
	});
	$(".preview-prev").click(function(){
		var nextview=returnPrevViewToLoad("preview");
		previewLoadView(nextview);
	});
	$(".preview-next").click(function(){
		var nextview=returnNextViewToLoad("preview");
		previewLoadView(nextview);
	});
	$(".zoom-block").click(function(){closeAllZoom();});
	$(".zoom-container").click(function(event){event.stopPropagation();});
});

function scrollToCenter(){
	var topVal=-(superZoomMaxHeight/2);
	$(".zoom-container .superzoom").css("top",topVal+"px");
}

function loadPreviewImg(imgUrl, thumbIdx){
	if(imgUrl == ""){
		$(".preview-loading").hide();
		return;
	}
	var img = new Image();
	img.onload = function(){
		$("#finalimage").attr("src",imgUrl);
		$(".preview-loading").hide();
		$(".thumb-views .selected").removeClass("selected");
		$("#"+thumbIdx).addClass("selected");
		$(".loadedimage").val(thumbIdx);
	};
	img.src = imgUrl;
}

function loadImg(imgUrl, selector, onLoadCallback){
	if(imgUrl == ""){
		$(".overlay-loading").hide();
		return;
	}
	var img = new Image();
	img.onload = function(){
		$(selector).attr("src",imgUrl);
		$(".overlay-loading").hide();
		if(typeof(onLoadCallback) != "undefined"){
			onLoadCallback();
		}
	};
	img.src = imgUrl;
}

function loadSuperZoom(){
	var selectedval=$(".loadedimage").val();
	setSuperZoomWindowSize();
	setZoomMode("szoom");
	$(".izoom").hide();
	$(".superzoom").fadeIn(50);
	$(".overlay-loading").show();
	$("body").css("margin-left","0px");
	//$(".next, .prev").css("top","50%");
	loadImg(thumbimginfo[selectedval].fullimage, ".zoom-block .superzoom");
	gaTrackEvent("sports_pdp_"+gaSuffix,"zoom","super");
	//$(".zoom-block .superzoom").attr("src",thumbimginfo[selectedval].fullimage);
//	updateNextPrevButtons(selectedval.substr(selectedval.indexOf("_")+1, selectedval.length));
}

var zoomImagesPreloaded=false;

function loadIZoom(){
	var selectedval=$(".loadedimage").val();
	scrollToTop();
	showOverlay();
	showZoomBlock();
	setZoomWindowSize();
	setZoomMode("izoom");
	$(".izoom").fadeIn(50);
	$(".overlay-loading").show();
	if(!zoomImagesPreloaded){
		loadImg(thumbimginfo[selectedval].intermediatezoomimage, ".zoom-block .izoom");
		loadImg(thumbimginfo[selectedval].fullimage, ".zoom-block .izoom", preloadZoomImages);
	}else{
		loadImg(thumbimginfo[selectedval].intermediatezoomimage, ".zoom-block .izoom");
	}
	//$(".zoom-block .izoom").attr("src",thumbimginfo[selectedval].intermediatezoomimage);
	updateNextPrevButtons(selectedval.substr(selectedval.indexOf("_")+1, selectedval.length));
	gaTrackEvent("sports_pdp_"+gaSuffix,"zoom","intermediate");
}

function preloadZoomImages(){
	for(idx in thumbimginfo){
		if(typeof thumbimginfo[idx].intermediatezoomimage != "undefined"){
			preLoadImage(thumbimginfo[idx].intermediatezoomimage);
		}
	}
	for(idx in thumbimginfo){
		if(typeof thumbimginfo[idx].fullimage != "undefined"){
			preLoadImage(thumbimginfo[idx].fullimage);
		}
	}
	zoomImagesPreloaded=true;
}

function preLoadImage(imgUrl){
	if(imgUrl == ""){
		$(".overlay-loading").hide();
		return;
	}
	var img = new Image();
	img.src = imgUrl;
}

function loadView(idx){
	$(".loadedimage").val("thumb_"+idx);
	var selectedval=$(".loadedimage").val();
	$(".overlay-loading").show();
	loadImg(thumbimginfo[selectedval].intermediatezoomimage, ".zoom-block .izoom");
	loadImg(thumbimginfo[selectedval].fullimage, ".zoom-block .superzoom");
	gaTrackEvent("sports_pdp_"+gaSuffix,"switch_view","");
}

//when user clicks on zoom set w
function scrollToTop(){
	$(window).scrollTop(0);
	$(window).trigger("scroll");
}

function previewLoadView(idx){
	$("#thumb_"+idx).trigger("click");
}

function returnNextViewToLoad(type){
	var selectedval=$(".loadedimage").val();
	var currIdx=parseInt(selectedval.substr(selectedval.indexOf("_")+1, selectedval.length));
	var nextIdx=0;
	if(currIdx+1 > noOfViews){
		return;
	}
	if(currIdx < noOfViews){
		nextIdx=++currIdx;
	}
	if(type == "preview"){
		updatePreviewNextPrevButtons(nextIdx);
	}
	else{
		updateNextPrevButtons(nextIdx);
	}
	return nextIdx;
}

function updateNextPrevButtons(idx){

	 if(noOfViews==0){
	        if(!$(".prev").hasClass("hide")){$(".prev").addClass("hide")};
	        if(!$(".next").hasClass("hide")){$(".next").addClass("hide")};
	        return;
	 }
	if(idx == 0){
		$(".prev").addClass("hide");
		$(".next").removeClass("hide");
	}
	if(idx == noOfViews){
		$(".next").addClass("hide");
		$(".prev").removeClass("hide");
	}
	if(idx > 0 && idx < noOfViews){
		$(".next").removeClass("hide");
		$(".prev").removeClass("hide");
	}

}

function updatePreviewNextPrevButtons(idx){

	 if(noOfViews==0){
	        if(!$(".preview-prev").hasClass("hide")){$(".preview-prev").addClass("hide")};
	        if(!$(".preview-next").hasClass("hide")){$(".preview-next").addClass("hide")};
	        return;
	 }

	if(idx == 0){
		$(".preview-prev").addClass("hide");
		$(".preview-next").removeClass("hide");
	}
	if(idx == noOfViews){
		$(".preview-next").addClass("hide");
		$(".preview-prev").removeClass("hide");
	}
	if(idx > 0 && idx < noOfViews){
		$(".preview-next").removeClass("hide");
		$(".preview-prev").removeClass("hide");
	}
}


function returnPrevViewToLoad(type){
	var selectedval=$(".loadedimage").val();
	var currIdx=parseInt(selectedval.substr(selectedval.indexOf("_")+1, selectedval.length));
	var nextIdx=0;
	if(currIdx-1 < 0){
		return;
	}
	if(currIdx > 0){
		nextIdx=--currIdx;
	}
	if(type == "preview"){
		updatePreviewNextPrevButtons(nextIdx);
	}
	else{
		updateNextPrevButtons(nextIdx);
	}
	return nextIdx;
}
function initNextPrevButtons(){
	if(parseInt(noOfViews) == 0){
		$(".next , .prev").addClass("hide");
	}
	if(parseInt(noOfViews) > 0){
		$(".prev").addClass("hide");
	}
}
function initPreviewNextPrevButtons(){
	if(parseInt(noOfViews) == 0){
		$(".preview-next , .preview-prev").addClass("hide");
	}
	if(parseInt(noOfViews) > 0){
		$(".preview-prev").addClass("hide");
	}
}


function opennotifyme(){

	if(useremail==null || $.trim(useremail)==""){
		if($("#notifyme-form-unlogged").css("display")=="none"){
    		$("#notifyme-form-unlogged").show();
    		$(".notifyme-overlay").show();
    	}
    	else{
    		$("#notifyme-form-unlogged").hide();
    		$(".notifyme-overlay").hide();
    	}

	}else{

		$("#notifymeEmail").val(useremail);
		if($.trim(usermobile)==""){
			$(".notifymeMobileComp").css("display","none");
		}else{
			$("#notifyonmobile").attr("disabled",false);
			$("#notifymeMobile").val("+91-"+usermobile);
		}
		if($("#notifyme-form-logged").css("display")=="none"){
    		$("#notifyme-form-logged").show();
    		$(".notifyme-overlay").show();
    	}
    	else{
    		$("#notifyme-form-logged").hide();
    		$(".notifyme-overlay").hide();
    	}
	}



}
function closenotifyme(){
	$(".notifyme-form").hide();
	$(".notifyme-overlay").hide();
}

function switchSize(size){
	var suffix="";
	if(useremail==null || $.trim(useremail)==""){
		suffix='U';
	}else{
		suffix='L';
	}
	if(size=='I'){
		$("#unifiedsize"+suffix).show();
		$("#nonunifiedsize"+suffix).hide();
	}else{
		$("#unifiedsize"+suffix).hide();
		$("#nonunifiedsize"+suffix).show();
	}
}

function sizeNotifyMeSelect(sku,el){
	selectedFlatSize=sku;
	if(sku==0)
		return;
	if(useremail==null || $.trim(useremail)==""){
		suffix='U';
	}else{
		suffix='L';
	}
	$(".flat-size-options-n a").removeClass("selected");
	$(el).addClass("selected");
	$("#otherSize"+suffix).val("Other");
	$("#otherSize"+suffix).addClass('blurred');
}



var smsLink=false;
function notifyViaSms(){
	if(smsLink==false){
		$("#notifyViaSmsLinkU").html("[-]Want to get notified via SMS?");
		smsLink=true;
		$("#notifymeSigninDiv").show();
	}else{
		$("#notifyViaSmsLinkU").html("[+]Want to get notified via SMS?");
		smsLink=false;
		$("#notifymeSigninDiv").hide();
	}
}


function submitNotifyForm(){
	var form;
	var suffix;
	var formObj;
	if(useremail==null || $.trim(useremail)==""){
		form=$("#notifyme-unlogged");
		formObj=document.getElementById('notifyme-unlogged');
		suffix="U";
	}else{
		form=$("#notifyme-logged");
		formObj=document.getElementById('notifyme-logged');
		suffix="L";
	}
	if(flattenSizeOptions){
			if(selectedFlatSize==0 && ($.trim($("#otherSize"+suffix).val())=="" || $.trim($("#otherSize"+suffix).val())=="Other")){
				$(".notify-size-error").css("display",'block');
				$(".flat-size-options-n").css("border","2px solid red");
				return false;
			}else{
				$(".flat-size-options-n").css("border","");
			}
			if(selectedFlatSize==0){
				selectedSize=$("#otherSize"+suffix).val();
			}

	}
	else{
				if($('#nonunifiedsize'+suffix).length){
					if ($("#nonunifiedsize"+suffix).val()=='Select'  ){
						$("#nonunifiedsize"+suffix).css("border-color","red");
						$(".notify-size-error").css("display",'block');
						return false;
					}else{
						$("#nonunifiedsize"+suffix).css("border-color","");
					}
					if($("#nonunifiedsize"+suffix).val()=='Others'){
						selectedSize=$("#otherSize"+suffix).val();
					}
					if($("#nonunifiedsize"+suffix).val()=='Others'&& $.trim($("#otherSize"+suffix).val())==""){
						$("#otherSize"+suffix).css("border-color","red");
						$(".notify-size-error").css("display",'block');
						return false;
					}else{
						$("#otherSize"+suffix).css("border-color","");
					}
				}else{
					if($.trim($("#otherSize"+suffix).val())==""){
						$("#otherSize"+suffix).css("border-color","red");
						$(".notify-size-error").css("display",'block');
						return false;
					}else{
						$("#otherSize"+suffix).css("border-color","");
						selectedSize=$("#otherSize"+suffix).val();
					}
				}

	}

	if(suffix=="U"&&($.trim(formObj.notifymeEmail.value)=="" || !emailReg.test(formObj.notifymeEmail.value))){
		$(".notify-email-error").css("display",'block');
		$("#notifymeEmail"+suffix).css("border-color","red");
		return false;
	}else{
		$("#notifymeEmail"+suffix).css("border-color","");
	}
	$(".notifyme-form .loading-overlay").removeClass("hide").addClass("show");
	_gaq.push(['_trackEvent', 'size_notify_dialog', 'notify_me', 'size_'+selectedSize]);
	$.post(form.attr("action"), form.serialize()+"&styleid="+curr_style_id+"&notifymeMobile="+usermobile+(flattenSizeOptions==true?"&selectedsizeN="+selectedFlatSize:"") , function(data){
		$(".notifyme-form .loading-overlay").removeClass("show").addClass("hide");
		closenotifyme();
		$(".notifyme-overlay").show();
		$("#notifyme-done").show();
	});
}




