//AVAIL
function availOnLoad() {
	$.getScript("http://service.sg.avail.net/2009-02-13/dynamic/54ed2a6e-d225-11e0-9cab-12313b0349b4/emark.js", function() {
		var emark = new Emark();
		switch (pageName) {
		case "home":
			//DO NOTHING
			break;
		case "pdp":
			//PAHSE-1-->getRecommendation-Presentation,lgClickedOn, If added to cart call logAddedToCart
			avail_dynamic_param = "append andcat in subtemplate ALL with " + avail_dynamic_param;
			var t_code = getQSVal("t_code");
			if (t_code != '') {
				emark.logClickedOn(curr_style_id, t_code);
			}
			var product = "ProductID:" + curr_style_id;
			var recProducts = emark.getRecommendations("Productpage", [product], [avail_dynamic_param]);
			//do the logClickedonHere on PDP page load
			emark.commit(function() {
				//do presentation here
				displayAvail(recProducts.values, recProducts.trackingcode);

			});

			break;
		case "search":
			//PAHSE-1-->getRecommendation-Presentation,lgClickedOn, SaveSearch
			/*SECTION FOR CATEGORY COMMENTED UNTILL CATEGORY PAGES ARE IMPLEMENTED
				var uq=getQSVal("userQuery");
				if(uq)
				{   
					var phrase="Phrase:"+query;
					recProducts=emark.getRecommendations("SearchPage",[phrase]);
				}
				else
				{	
					
					products="ProductIDs:"+avail_cookie_ids;
					AVAIL_USER_ID="UserID:"+AVAIL_USER_ID;
					recProducts=emark.getRecommendations("CategoryPage",[AVAIL_USER_ID,products]);
					
					var phrase="Phrase:"+query;
					recProducts=emark.getRecommendations("SearchPage",[phrase]);
				}*/
			var curUrl = ("'" + window.location + "'").search('#!');
			var pageFilter = -1;
			if (curUrl != -1) {
				pageFilter = ("'" + window.location + "'").search('#!page');
			}
			var phrase = "Phrase:" + query;
			var recProducts = emark.getRecommendations("SearchPage", [phrase]);
			emark.commit(function() {
				if (curUrl == -1) { //displayAvail(recProducts.values,recProducts.trackingcode);
				}
				else if (pageFilter != -1) { //displayAvail(recProducts.values,recProducts.trackingcode);
				}
			});
			break;
		case "cart":
			//PAHSE-1-->logAddedtoCart,logRemovedFromCart, PAHSE-2-->getRecommendation-Presentation,lgClickedOn, 
			var uq = getQSVal("uq");
			var t_code = getQSVal("t_code");
			uqs = uqs.replace(/-/g, ' ');
			if (av_id_added != '') {
				//logAddedToCart
				//if(uq != 'false'){ COMMENTED FOR MAKING ALL SEARCH, SWITCH TO CATEGORY WHEN PARAMETERIZED SEARCH IS IMPLEMENTED
				emark.saveSearch(uqs, av_id_added);
				//}
			}
			var recProducts = emark.getRecommendations("ShoppingCart");
			emark.commit(function() {
				if (avail_cart_abtest == 'avail_cart') displayAvail(recProducts.values, recProducts.trackingcode);
			});

			break;
		}

	});
}
function displayAvail(styleIds, trackingCode) {
	var rec_url = http_loc + "/avail_ajax_recommendation.php?avail_ids=" + styleIds + "&avail_track=" + trackingCode + "&page=" + pageName;
	$.ajax({
		type: "GET",
		url: rec_url,
		success: function(data) {
			if ($('#avail-container').is(':empty')) {
				if (data != 0) {
					if (pageName == 'cart') $('#avail-container').addClass('content');
					$('#avail-container').hide().append(data).show('slow');
				}
			}
			Myntra.Utils.showAdminStyleDetails();
		}
	});

}
function getQSVal(qs) {
	var qs_temp = window.location.search.substring(1);
	var qs_arr = qs_temp.split("&");
	var qs_ret = '';
	for (i = 0; i < qs_arr.length; i++) {
		var qs_split = qs_arr[i].split("=");
		if (qs_split[0] == qs) {
			qs_ret = qs_split[1];
		}
	}
	return qs_ret;
}

//END AVAIL

