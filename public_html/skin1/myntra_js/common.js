
if(typeof(loginTimeout) == "undefined"){
    Myntra.Data.loginTimeout = 30000;
}else {
    Myntra.Data.loginTimeout = loginTimeout*1000;
}

Myntra.Utils.trackOmnitureLoginAttempt = function() {
	if(typeof(s_analytics) != "undefined") {
		s_analytics.linkTrackVars='events';
		s_analytics.linkTrackEvents='event7';
		s_analytics.events='event7';
		s_analytics.tl(this,'o', 'Login Popup');
	}
}

Myntra.Utils.trackOmnitureLoginSuccessful = function() {
	if(typeof(s_analytics) != "undefined") {
		s_analytics.linkTrackVars='events';
		s_analytics.linkTrackEvents='event21';
		s_analytics.events='event21';
		s_analytics.tl(true, 'o', 'Login Success');
	}
}

Myntra.Utils.showAdminStyleDetails = function(){
	if(Myntra.Data.showAdminAdditionalStyleDetails == 'y') {
		$('.item').each( function(){
			if($(this).attr('data-id')){
				var style_id= $(this).attr('data-id').replace('prod_0_style_','');
				var styleDetailsElem=$(this).children('.style-details');
				if($(styleDetailsElem).length){
					$.get(http_loc+'/myntra/style_details.php',{format:'json',style_id:style_id}, function(data) {
						$(styleDetailsElem).html(data.innerhtml);
						$(styleDetailsElem).show();
					});
				}
			}
		});
	}

};

Myntra.Utils.formatNumber = function(num) {
    num = num.toFixed(2).split(".")[0];
    if (num.length <= 3) { return num; }
    var m = /(\d+)(\d{3})$/.exec(num);
    var inp = m[1];
    var last = m[2];
    var out = [];
    if (inp.length % 2 === 1) {
        out.push(inp.substr(0,1));
        inp = inp.substr(1);
    }
    var rx = /\d{2}/g;
    while (m = rx.exec(inp)) {
        out.push(m[0]);
    }
    out.push(last);
    return out.join(',');
};

Myntra.Utils.getQueryParam = function(name) {
    var params = {};
    var qstr = location.search;
    if (!qstr) { return ''; }
    qstr = qstr.replace(/^\?/, '');
    var nvPairs = qstr.split('&');
    for (var i = 0, n = nvPairs.length; i < n; i += 1) {
        var parts = nvPairs[i].split('=');
        params[parts[0]] = parts[1];
    }
    Myntra.Utils.getQueryParam = function(name) {
        return (params[name] || '');
    }
    return (params[name] || '');
};

Myntra.InitTextInputHint = function(el, hint) {
    el = $(el);
    el.val(hint).addClass('hint');
    el.blur(function() {
        if (!el.val()) {
            el.val(hint).addClass('hint');
        }
    });
    el.focus(function() {
        if (el.val() === hint) {
            el.val('').removeClass('hint');
        }
    });
};

Myntra.FBConnect = function(referer) {
    FB.login(function(response) {
        if (response.authResponse) {
            // user successfully connected with FB
            $(document).trigger('myntra.fblogin.auth');

            // now do the post actions in our end
            var url = '/myntra/ajax_facebook_login.php';
            if (referer) {
                url += '?ref=' + referer;
            }
            $.post(url,
                {menu_usertype:"C", menu_redirect:"1", menu_filetype:"myhome"},
                function(data){
                    if (data.status == "success") {
                        data.fb_action = data.fb_action || 'login';
                        $(document).trigger('myntra.fblogin.done', data);
                        if(typeof(s_analytics) != "undefined") {
                        	s_analytics.eVar18='success';
                        }
                        Myntra.Utils.trackOmnitureLoginSuccessful();
                    }
                    else{
                        $(document).trigger('myntra.fblogin.done', {status:'failure'});
                    }
                },
                "json"
            );
        }
        else {
            $(document).trigger('myntra.fblogin.done', {status:'access-denied'});
        }
    },
    {scope:'user_birthday,user_interests,user_likes,user_location,user_relationships,email,friends_birthday,friends_interests,friends_relationships,read_stream'}
    );
};

Myntra.InitTooltip = function(tip, ctx, cfg) {
    tip = $(tip);
    ctx = $(ctx);
    if (!tip || !ctx) { return; }
    if (tip.length > 1) { return; }

    cfg = cfg || {};
    cfg.side = cfg.side || 'below';

    if (!tip.parent().is('body')) {
        tip.appendTo('body');
    }

    if (cfg.side === 'below') {
        tip.prepend('<div class="arrow arrow-top-outer"></div><div class="arrow arrow-top-inner"></div>');
    }
    else if (cfg.side === 'above') {
        tip.prepend('<div class="arrow arrow-bottom-outer"></div><div class="arrow arrow-bottom-inner"></div>');
    }
    else if (cfg.side === 'left') {
        tip.prepend('<div class="arrow arrow-right-outer"></div><div class="arrow arrow-right-inner"></div>');
    }
    else if (cfg.side === 'right') {
        tip.prepend('<div class="arrow arrow-left-outer"></div><div class="arrow arrow-left-inner"></div>');
    }

    var arrow = tip.find('.arrow'),
        x, y, p, th, tw, ch, cw,
        cached = false,
        gap = 10,
        tmr = null;

    ctx.mouseenter(function(e) {
        if (tmr) {
            clearTimeout(tmr);
            tmr = null;
        }
        if (!cached) {
            tip.css('top', '-500px');
            tip.css('display', 'block');
            th = tip.outerHeight();
            tw = tip.outerWidth();
            cached = true;
        }

        var el = cfg.element ? $(cfg.element, this) : $(this);
        ch = el.outerHeight();
        cw = el.outerWidth();
        p = el.offset();
        //console.log(tw, th, cw, ch, p);

        if (cfg.side === 'left' || cfg.side === 'right') {
            y = Math.round(th / 2) - 12;
            arrow.css('top', y + 'px');
        }
        else if (cfg.side === 'above' || cfg.side === 'below') {
            x = Math.round(tw / 2) - 12;
            arrow.css('left', x + 'px');
        }

        if (cfg.side === 'below') {
            p.top = p.top + ch + gap;
            p.left = p.left - tw / 2 + cw / 2;
        }
        else if (cfg.side === 'above') {
            p.top = p.top - th - gap;
            p.left = p.left - tw / 2 + cw / 2;
        }
        else if (cfg.side === 'left') {
            p.top = p.top - th / 2 + ch / 2;
            p.left = p.left - tw - gap;
        }
        else if (cfg.side === 'right') {
            p.top = p.top - th / 2 + ch / 2;
            p.left = p.left + cw + gap;
        }

        tip.css('left', p.left + 'px');
        tip.css('top', p.top + 'px');
        tip.fadeIn();
    });
    ctx.mouseleave(function(e) {
        tmr = setTimeout(function() { tip.fadeOut() }, 100);
    });
    tip.mouseenter(function(e) {
        if (tmr) {
            clearTimeout(tmr);
            tmr = null;
        }
    });
    tip.mouseleave(function(e) {
        tmr = setTimeout(function() { tip.fadeOut() }, 100);
    });
};

Myntra.InitTooltipFromTitle = function(ctx, cfg) {
    $(ctx).each(function(i, el) {
        var node = $(el),
            txt = node.attr('title');
        if (!txt) { return; }
        node.removeAttr('title');
        var tip = $('<div class="myntra-tooltip">' + txt + '</div>').appendTo('body');
        Myntra.InitTooltip(tip, node, cfg);
    });
};

Myntra.MsgBox = (function(){
    var obj, title, bd;

    var init = function() {
        var h = '';
        h += '<div id="lb-msg-box" class="lightbox">';
        h += '<div class="mod">';
        h += '    <div class="hd">';
        h += '      <h2></h2>';
        h += '  </div>';
        h += '  <div class="bd">';
        h += '  </div>';
        h += '  <div class="ft">';
        h += '      <button class="action-btn btn-ok">OK</button>';
        h += '  </div>';
        h += '</div>';
        h += '</div>';
        $('body').append(h);
        obj = Myntra.LightBox('#lb-msg-box');
        title = $('#lb-msg-box h2');
        bd = $('#lb-msg-box .bd');
        $('#lb-msg-box .btn-ok').click(function(e) { obj.hide() });
    };

    return {
        show: function(a_title, content, cfg) {
            if (!obj) { init(); }
            title.text(a_title);
            bd.html(content);
            obj.show();
            $('#lb-msg-box .btn-ok').focus();
            if (cfg && cfg.autohide) {
                setTimeout(function() { obj.hide() }, cfg.autohide);
            }
        },

        hide: function() {
            obj.hide();
        }
    };
})();

Myntra.xdMessage = function(msg) {
    $(document).trigger('myntra.xdmsg', msg);
};

Myntra.LoginBox = function(id, cfg) {
    return $('body').hasClass('mab-loginbox-v3') ? Myntra.LoginBoxB(id, cfg) : Myntra.LoginBoxA(id, cfg);
};

Myntra.LoginBoxA = function(id, cfg) {
    cfg = cfg || {};
    var obj = Myntra.LightBox(id, cfg),
        logo = $(id + ' .hd img'),
        hdTitle = $(id + ' .hd h2'),
        form = $(id + ' form'),
        fbDiv = $(id + ' .facebook'),
        fpDiv = $(id + ' .forgot-pass-msg'),
        errFb = $(id + ' .err-fb'),
        fpDivEmail = $(id + ' .forgot-pass-msg .email'),
        user = form.find('input[name="email"]'),
        pass = form.find('input[name="password"]'),
        mobile = form.find('input[name="mobile"]'),
        errUser = form.find('.err-user'),
        errPass = form.find('.err-pass'),
        errMobile = form.find('.err-mobile'),
        mode = form.find('input[name="mode"]'),
        title, tmrTimeout;

    $('<iframe id="mklogin-iframe" name="mklogin-iframe" src="javascript:void(0)"></iframe')
        .css({'position':'absolute', 'height':'0', 'top':'-100px'})
        .appendTo('body');
    form.attr('target', 'mklogin-iframe');

    var onmessage = function(e) {
        (e.origin === http_loc) && $(document).trigger('myntra.xdmsg', e.data);
    };
    window.addEventListener ? window.addEventListener('message', onmessage, false) : window.attachEvent ? window.attachEvent('onmessage', onmessage) : null;

    var hideErrors = function() {
        errUser.hide();
        errPass.hide();
        errMobile.hide();
        errFb.hide();
    };
    mobile.keypress(function(e) {
        if (13 == e.keyCode) {
            e.preventDefault();
        }
    });

    var validateUser = function(e) {
        if (!user.val()) {
            errUser.text('Please enter email address').slideDown();
            return false;
        }
        else if (!/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/.test(user.val())) {
            errUser.text('Please enter a valid email address').slideDown();
            return false;
        }
        else {
            errUser.text('').slideUp();
            return true;
        }
    };

    var validatePass = function(e) {
        if (!pass.val()) {
            errPass.text('Please enter password').slideDown();
            return false;
        }
        else if (pass.val().length > 0 && pass.val().length < 6) {
            errPass.text('Minimum 6 characters required').slideDown();
            return false;
        }
        else {
            errPass.text('').slideUp();
            return true;
        }
    };

    var validateMobile = function() {
        if (!mobile.val()) {
            errMobile.text('Please enter mobile number').slideDown();
            return false;
        }
        else if (!/^\d{10}$/.test(mobile.val())) {
            errMobile.text('Enter a valid mobile number').slideDown();
            return false;
        }
        else {
            errMobile.text('').slideUp();
            return true;
        }
    };

    var validate = function() {
        var r1 = validateUser();
        var r2 = validatePass();

        if (cfg.action === 'signin') {
            return r1 && r2;
        }

        var r3 = validateMobile();
        return r1 && r2 && r3;
    };

    $(document).bind('myntra.xdmsg', function(e, msg) {
        //console.log('myntra.xdmsg', msg);
        var data = {};
        var kvPairs = msg.split(';');
        for (var i = 0, n = kvPairs.length; i < n; i += 1) {
            var kv = kvPairs[i].split('=');
            data[kv[0]] = kv[1];
        }
        if (!(data.type === 'signin' || data.type === 'signup')) {
            return;
        }

        if (tmrTimeout) {
            clearTimeout(tmrTimeout);
            tmrTimeout = null;
            Myntra.Utils.Cookie.del('_loginerr');
        }

        if (data.status === 'auth-error') {
            Myntra.Utils.Cookie.set('_loginerr', cfg.invoke + '*' + cfg.action + '*authfail');
            _gaq.push(['_trackEvent', cfg.invoke, cfg.action, 'auth_error']);
            location.reload();
        }
        else if (data.status === 'data-error') {
            obj.hideLoading();
            _gaq.push(['_trackEvent', cfg.invoke, cfg.action, 'data_error']);
            alert('Please enter valid inputs');
        }
        else if (data.status === 'success') {
            Myntra.Utils.Cookie.set('_mklogin', cfg.invoke + '*' + cfg.action + '*success');
            Myntra.Utils.trackOmnitureLoginSuccessful();
            if (location.pathname == '/mkmycartmultiple_re.php' || location.pathname == '/mkmycart.php') {
                location.href = https_loc + '/mkpaymentoptions.php';
            }
            else {
                location.reload();
            }
        }
        else if (data.status === 'error') {
            obj.hideLoading();
            if (data.errorcode === '1') {
                if (cfg.action === 'signup') {
                    errUser.html('Username already exists.').slideDown();
                }
                else if (cfg.action === 'signin') {
                    _gaq.push(['_trackEvent', cfg.invoke, cfg.action, 'failure_login_not_found']);
                    errUser.html('Either Username/ Password were incorrect.').slideDown();
                }
            }
            else if (data.errorcode === '2') {
                if (cfg.action === 'signin') {
                    _gaq.push(['_trackEvent', cfg.invoke, cfg.action, 'failure_password_invalid']);
                    errUser.html('Either Username/ Password were incorrect.').slideDown();
                }
            }
        }
    });

    form.submit(function(e) {
        if (!validate()) {
            e.preventDefault();
            return;
        }

        obj.showLoading();
        cfg.action = cfg.action || 'login';
        mode.val(cfg.action); // set the mode in the hidden form element

        tmrTimeout = setTimeout(function() {
            Myntra.Utils.Cookie.set('_loginerr', cfg.invoke + '*' + cfg.action + '*timeout');
            location.reload();
        }, Myntra.Data.loginTimeout);
    });

    $(id + ' .btn-signin').click(function(e) {
        cfg.action = 'signin';
        errMobile.html('').slideUp();
    });
    $(id + ' .btn-signup').click(function(e) {
        cfg.action = 'signup';
    });

    // forgot password interaction
    var showForgotPassMsg = function() {
        hdTitle.text('No Worries!');
        fpDivEmail.html(user.val());
        form.hide();
        fbDiv.hide();
        fpDiv.show();
        obj.center();
        if (cfg.isPopup) {
            try { $(id + ' .btn-forgot-pass-ok').focus(); } catch(ex) {}
        }
    };
    var hideForgotPassMsg = function() {
        hdTitle.text(title);
        form.show();
        fbDiv.show();
        fpDiv.hide();
        obj.center();
    };
    $(id + ' .btn-return-to-login').click(function(e) {
        hideForgotPassMsg();
        try { user.focus() } catch(ex) {}
    });
    $(id + ' .btn-forgot-pass').click(function(e) {
        e.preventDefault();
        errPass.html('').slideUp();
        errMobile.html('').slideUp();
        if (!validateUser()) { return; }
        $.ajax({
            type: "POST",
            url: "/reset_password.php",
            data: "action=FORGOTPASSWORD&username=" + user.val(),
            beforeSend:function(){
                obj.showLoading();
            },
            success: function(msg){
                msg = $.trim(msg);
                if (msg === 'success') {
                    showForgotPassMsg();
                }
                else {
                    errUser.html(user.val() + " is not registered with Myntra.com").slideDown();
                }
            },
            complete: function(xhr, status) {
                obj.hideLoading();
            }
        });
    });

    if (cfg.isPopup) {
        $(id + ' .btn-forgot-pass-ok').click(function(e) {
            obj.hide();
        });
    }
    else {
        $(id + ' .btn-forgot-pass-ok').hide();
    }

    // facebook connect interaction
    $(id + ' .btn-fb-connect').click(function() {
        _gaq.push(['_trackEvent', cfg.invoke, 'fb_connect', 'click']);
        var referer = $(this).attr('data-referer');
        Myntra.FBConnect(referer);
    });
    $(document).bind('myntra.fblogin.auth', function(e, data) {
        obj.showLoading();
    });
    $(document).bind('myntra.fblogin.done', function(e, data) {
        obj.hideLoading();
        switch (data.status) {
        case 'success':
            if (cfg.isPopup) { obj.hide(); }
            Myntra.Utils.Cookie.set('_mklogin', cfg.invoke + '*fb_connect*success_' + data.fb_action);
            Myntra.Utils.trackOmnitureLoginSuccessful();
            if (location.pathname == '/mkmycartmultiple_re.php' || location.pathname == '/mkmycart.php') {
                location.href = https_loc +'/mkpaymentoptions.php';
            }
            else {
                location.reload();
            }
            break;
        case 'failure':
            _gaq.push(['_trackEvent', cfg.invoke, 'fb_connect', 'failure']);
            errFb.html('Connect with Facebook failed').show();
            break;
        case 'access-denied':
            _gaq.push(['_trackEvent', cfg.invoke, 'fb_connect', 'access-denied']);
            errFb.html('Connect with Facebook failed. Permission denied').show();
        }
    });

    obj.showErrMsg = function(err) {
        errUser.text(err.message).slideDown();
        _gaq.push(['_trackEvent', cfg.invoke, err.action, err.name]);
    };

    // override show method
    var p_show = obj.show;
    obj.show = function(a_action) {
        cfg.action = a_action || 'login';
        hideForgotPassMsg();
        hideErrors();
        p_show.call(this);
        try { user.focus(); } catch(ex) {}
    };

    obj.setTitle = function(a_title) {
        hdTitle.text(a_title);
        title = a_title;
    };
    obj.getTitle = function() {
        return hdTitle.text();
    };
    obj.setInvokeType = function(val) {
        cfg.invoke = val;
    };
    obj.showLogo = function() {
        logo.show();
    };
    obj.hideLogo = function() {
        logo.hide();
    };

    obj.adjustHeight = function() {
    };

    return obj;
};

Myntra.LoginBoxB = function(id, cfg) {
    cfg = cfg || {};
    var obj = Myntra.LightBox(id, cfg),
        mainDiv = $(id + ' .main-content'),
        fpDiv = $(id + ' .forgot-pass-msg'),
        fpDivEmail = $(id + ' .forgot-pass-msg .email'),
        fbErr = $(id + ' .err-fb'),
        frmLogin = $(id + ' .frm-login'),
        frmSignup = $(id + ' .frm-signup'),
        loginUser = frmLogin.find('input[name="email"]'),
        loginPass = frmLogin.find('input[name="password"]'),
        signupUser = frmSignup.find('input[name="email"]'),
        signupPass = frmSignup.find('input[name="password"]'),
        signupMobile = frmSignup.find('input[name="mobile"]'),
        loginUserErr = frmLogin.find('.err-user'),
        loginPassErr = frmLogin.find('.err-pass'),
        signupUserErr = frmSignup.find('.err-user'),
        signupPassErr = frmSignup.find('.err-pass'),
        signupMobileErr = frmSignup.find('.err-mobile'),
        loginBtn = frmLogin.find('.btn-signup'),
        signupBtn = frmSignup.find('.btn-signup'),
        hdTitle = $(id + ' .hd h2'),
        title = hdTitle.text(),
        tmrTimeout;

    $('<iframe id="mklogin-iframe" name="mklogin-iframe" src="javascript:void(0)"></iframe')
        .css({'position':'absolute', 'height':'0', 'top':'-100px'})
        .appendTo('body');
    frmLogin.attr('target', 'mklogin-iframe');
    frmSignup.attr('target', 'mklogin-iframe');

    var onmessage = function(e) {
        (e.origin === http_loc) && $(document).trigger('myntra.xdmsg', e.data);
    };
    window.addEventListener ? window.addEventListener('message', onmessage, false) : window.attachEvent ? window.attachEvent('onmessage', onmessage) : null;

    var hideErrors = function() {
        loginUserErr.html('');
        loginPassErr.html('');
        signupUserErr.html('');
        signupPassErr.html('');
        signupMobileErr.html('');
        fbErr.html('');
    };
    var validateUser = function(user, errUser) {
        if (!user.val()) {
            errUser.text('Please enter email address');
            return false;
        }
        else if (!/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/.test(user.val())) {
            errUser.text('Please enter a valid email address');
            return false;
        }
        else {
            errUser.text('');
            return true;
        }
    };

    var validatePass = function(pass, errPass) {
        if (!pass.val()) {
            errPass.text('Please enter password');
            return false;
        }
        else if (pass.val().length > 0 && pass.val().length < 6) {
            errPass.text('Minimum 6 characters required');
            return false;
        }
        else {
            errPass.text('');
            return true;
        }
    };

    var validateMobile = function(mobile, errMobile) {
        if (!mobile.val()) {
            errMobile.text('Please enter mobile number');
            return false;
        }
        else if (!/^\d{10}$/.test(mobile.val())) {
            errMobile.text('Enter a valid mobile number');
            return false;
        }
        else {
            errMobile.text('');
            return true;
        }
    };

    $(document).bind('myntra.xdmsg', function(e, msg) {
        //console.log('myntra.xdmsg', msg);
        var data = {};
        var kvPairs = msg.split(';');
        for (var i = 0, n = kvPairs.length; i < n; i += 1) {
            var kv = kvPairs[i].split('=');
            data[kv[0]] = kv[1];
        }
        if (!(data.type === 'signin' || data.type === 'signup')) {
            return;
        }

        if (tmrTimeout) {
            clearTimeout(tmrTimeout);
            tmrTimeout = null;
            Myntra.Utils.Cookie.del('_loginerr');
        }

        if (data.status === 'auth-error') {
            Myntra.Utils.Cookie.set('_loginerr', cfg.invoke + '*' + cfg.action + '*authfail');
            _gaq.push(['_trackEvent', cfg.invoke, cfg.action, 'auth_error']);
            location.reload();
        }
        else if (data.status === 'data-error') {
            obj.hideLoading();
            _gaq.push(['_trackEvent', cfg.invoke, cfg.action, 'data_error']);
            alert('Please enter valid inputs');
        }
        else if (data.status === 'success') {
            Myntra.Utils.Cookie.set('_mklogin', cfg.invoke + '*' + cfg.action + '*success');
            Myntra.Utils.trackOmnitureLoginSuccessful();
            _gaq.push(['_trackEvent', cfg.invoke, cfg.action, 'success']);
            if (location.pathname == '/mkmycartmultiple_re.php' || location.pathname == '/mkmycart.php') {
                location.href = https_loc + '/mkpaymentoptions.php';
            }
            else {
                location.reload();
            }
        }
        else if (data.status === 'error') {
            obj.hideLoading();
            if (data.errorcode === '1') {
                if (cfg.action === 'signup') {
                    signupUserErr.html('Username already exists.');
                    _gaq.push(['_trackEvent', cfg.invoke, cfg.action, 'failure_username_already_exists']);
                }
                else if (cfg.action === 'signin') {
                    _gaq.push(['_trackEvent', cfg.invoke, cfg.action, 'failure_login_not_found']);
                    loginUserErr.html('Either Username/ Password were incorrect.');
                }
            }
            else if (data.errorcode === '2') {
                if (cfg.action === 'signin') {
                    _gaq.push(['_trackEvent', cfg.invoke, cfg.action, 'failure_password_invalid']);
                    loginUserErr.html('Either Username/ Password were incorrect.');
                }
            }
        }
    });

    var fadeSignup = function() {
        frmLogin.fadeTo('fast', 1);
        frmSignup.fadeTo('slow', 0.5);
    };
    var fadeLogin = function() {
        frmSignup.fadeTo('fast', 1);
        frmLogin.fadeTo('slow', 0.5);
    };
    loginUser.focus(fadeSignup);
    loginPass.focus(fadeSignup);
    signupUser.focus(fadeLogin);
    signupPass.focus(fadeLogin);
    signupMobile.focus(fadeLogin);

    var handleTimeout = function() {
        tmrTimeout = setTimeout(function() {
            Myntra.Utils.Cookie.set('_loginerr', cfg.invoke + '*' + cfg.action + '*timeout');
            location.reload();
        }, Myntra.Data.loginTimeout);
    };

    frmLogin.submit(function(e) {
        var r1 = validateUser(loginUser, loginUserErr),
            r2 = validatePass(loginPass, loginPassErr);
        if (r1 && r2) {
            cfg.action = 'signin';
            obj.showLoading();
            handleTimeout();
        }
        else {
            e.preventDefault();
            fadeSignup();
            return;
        }
    });

    frmSignup.submit(function(e) {
        var r1 = validateUser(signupUser, signupUserErr),
            r2 = validatePass(signupPass, signupPassErr),
            r3 = validateMobile(signupMobile, signupMobileErr);
        if (r1 && r2 && r3) {
            cfg.action = 'signup';
            obj.showLoading();
            handleTimeout();
        }
        else {
            e.preventDefault();
            fadeLogin();
            return;
        }
    });

    // forgot password interaction
    var showForgotPassMsg = function() {
        var btnOk = $(id + ' .btn-forgot-pass-ok');
        hdTitle.text('No Worries!');
        fpDivEmail.html(loginUser.val());
        mainDiv.hide();
        fpDiv.show();
        obj.center();
        if (cfg.isPopup) {
            try { btnOk.focus() } catch(ex) {}
        }
    };
    var hideForgotPassMsg = function() {
        hdTitle.text(title);
        mainDiv.show();
        fpDiv.hide();
        obj.center();
    };
    $(id + ' .btn-return-to-login').click(function(e) {
        hideForgotPassMsg();
        try { loginUser.focus() } catch(ex) {}
    });
    $(id + ' .btn-forgot-pass').click(function(e) {
        e.preventDefault();
        if (!validateUser(loginUser, loginUserErr)) { return; }
        $.ajax({
            type: "POST",
            url: "/reset_password.php",
            data: "action=FORGOTPASSWORD&username=" + loginUser.val(),
            beforeSend:function(){
                obj.showLoading();
            },
            success: function(msg){
                msg = $.trim(msg);
                if (msg === 'success') {
                    showForgotPassMsg();
                }
                else {
                    loginUserErr.html(user.val() + " is not registered with Myntra.com");
                }
            },
            complete: function(xhr, status) {
                obj.hideLoading();
            }
        });
    });

    if (cfg.isPopup) {
        $(id + ' .btn-forgot-pass-ok').click(function(e) {
            obj.hide();
        });
    }
    else {
        $(id + ' .btn-forgot-pass-ok').hide();
    }

    // facebook connect interaction
    $(id + ' .btn-fb-connect').click(function() {
        _gaq.push(['_trackEvent', cfg.invoke, 'fb_connect', 'click']);
        var referer = $(this).attr('data-referer');
        Myntra.FBConnect(referer);
    });
    $(document).bind('myntra.fblogin.auth', function(e, data) {
        obj.showLoading();
    });
    $(document).bind('myntra.fblogin.done', function(e, data) {
        obj.hideLoading();
        switch (data.status) {
        case 'success':
            if (cfg.isPopup) { obj.hide(); }
            Myntra.Utils.Cookie.set('_mklogin', cfg.invoke + '*fb_connect*success_' + data.fb_action);
            Myntra.Utils.trackOmnitureLoginSuccessful();
            if (location.pathname == '/mkmycartmultiple_re.php' || location.pathname == '/mkmycart.php') {
                location.href = https_loc + '/mkpaymentoptions.php';
            }
            else {
                location.reload();
            }
            break;
        case 'failure':
            _gaq.push(['_trackEvent', cfg.invoke, 'fb_connect', 'failure']);
            fbErr.html('Connect with Facebook failed');
            break;
        case 'access-denied':
            _gaq.push(['_trackEvent', cfg.invoke, 'fb_connect', 'access-denied']);
            fbErr.html('Connect with Facebook failed. Permission denied');
        }
    });

    obj.adjustHeight = function() {
        frmLogin.height(frmSignup.height());
    };

    obj.showErrMsg = function(err) {
        if (err.action == 'signin') {
            loginUserErr.html(err.message);
        }
        else {
            signupUserErr.html(err.message);
        }
        _gaq.push(['_trackEvent', cfg.invoke, err.action, err.name]);
    };

    // override show method
    var p_show = obj.show;
    obj.show = function() {
        hideForgotPassMsg();
        hideErrors();
        p_show.call(this);
        obj.adjustHeight();
        try { loginUser.focus(); } catch(ex) {}
    };

    // just empty methods to keep the object interface same as loginBoxA
    obj.setTitle = function(a_title) {
    };
    obj.getTitle = function() {
    };
    obj.setInvokeType = function(val) {
        cfg.invoke = val;
    };
    obj.showLogo = function() {
    };
    obj.hideLogo = function() {
    };

    return obj;
};

Myntra.ShowSplashMin = function() {
    var obj, errFb;
    init = function() {
        obj = Myntra.LightBox('#lb-splash-min');
        errFb = $('#lb-splash-min .err-fb');
        $('#lb-splash-min .btn-fb-connect').click(function() {
            _gaq.push(['_trackEvent', 'splash', 'fb_connect', 'click', 'min']);
            var referer = $(this).attr('data-referer');
            Myntra.FBConnect(referer);
        });
        $(document).bind('myntra.fblogin.auth', function(e, data) {
            obj.showLoading();
        });
        $(document).bind('myntra.fblogin.done', function(e, data) {
            obj.hideLoading();
            switch (data.status) {
            case 'success':
                obj.hide();
                Myntra.Utils.Cookie.set('_mklogin', 'splash' + '*fb_connect*success_' + data.fb_action + '*min');
                Myntra.Utils.trackOmnitureLoginSuccessful();
                location.reload();
                break;
            case 'failure':
                _gaq.push(['_trackEvent', 'splash', 'fb_connect', 'failure', 'min']);
                errFb.html('Connect with Facebook failed').show();
                break;
            case 'access-denied':
                _gaq.push(['_trackEvent', 'splash', 'fb_connect', 'access-denied', 'min']);
                errFb.html('Connect with Facebook failed. Permission denied').show();
            }
        });
    };

    obj || init();
    obj.show();
};

Myntra.InitLogin = function(id) {
    if (!$(id).length) { return false; }
    if (location.pathname == '/register.php') {
        return false;
    }

    var abtestSplash = $('body').hasClass('mab-splash-fblogin');
    var cfg = {isModal:false};
    var obj = Myntra.LoginBox(id, cfg);

    $(document).bind('myntra.login.show', function(e, data) {
        var invoke = data ? data.invoke : 'header';
        obj.setTitle('Login / Sign Up');
        obj.hideLogo();
        obj.setInvokeType(invoke);

        Myntra.Utils.trackOmnitureLoginAttempt();
        obj.show();
        if (data && data.showerr) {
            var act = data.action == 'signin' ? 'login' : 'sign up';
            data.message = 'Something went wrong. Please ' + act + ' again';
            obj.showErrMsg(data);
        }
    });

    // exclude some pages from showing the splash screen
    if (location.pathname == '/mksystemerror.php') {
        return true;
    }

    var ckLoginErr = Myntra.Utils.Cookie.get('_loginerr');
    if (ckLoginErr) {
        Myntra.Utils.Cookie.del('_loginerr');
        var parts = ckLoginErr.split('*');
        $(document).trigger('myntra.login.show', {showerr:1, invoke:parts[0], action:parts[1], name:parts[2]});
    }

    if (!Myntra.Utils.Cookie.get('splash')) {
        // condition: query param ".nsp" should NOT present AND user is NOT logged in
        if (!Myntra.Utils.getQueryParam('.nsp') && !Myntra.Data.userEmail) {
            if (!abtestSplash) {
                obj.setTitle('Welcome!');
                obj.showLogo();
                obj.setInvokeType('splash');
                obj.show();
                
                Myntra.Utils.trackOmnitureLoginAttempt();
            }
            else {
                Myntra.ShowSplashMin();
            }
        }
        Myntra.Utils.Cookie.set('splash', 1, 30);
    }

    return true;
};

Myntra.InitRegLogin = function(id) {
    if (!$(id).length) { return false;  }
    var cfg = {isPopup:false};
    var obj = Myntra.LoginBox(id, cfg);

    obj.setTitle('Login / Sign Up');
    obj.hideLogo();
    obj.setInvokeType('reg_page');
    obj.adjustHeight();

    var ckLoginErr = Myntra.Utils.Cookie.get('_loginerr');
    if (ckLoginErr) {
        Myntra.Utils.Cookie.del('_loginerr');
        var parts = ckLoginErr.split('*');
        var err = {showerr:1, invoke:parts[0], action:parts[1], name:parts[2]};
        var act = err.action == 'signin' ? 'login' : 'sign up';
        err.message = 'Something went wrong. Please ' + act + ' again';
        obj.showErrMsg(err);
    }

    return true;
};

Myntra.InitHeaderButtons = function() {
    $(".btn-login").click(function(e) {
        e.stopPropagation();
        _gaq.push(['_trackEvent', 'header', 'signin', 'click']);
        $(document).trigger('myntra.login.show');
    });

    Myntra.InitTooltip('.tooltip-myntcre', '.popup-trigger', {side:'below'});
    Myntra.InitTooltip('.pdp-tooltip', '.pdp-trigger', {side:'below'});
}

Myntra.InitQuickLook = function(){
	$(".quick-look").live('click', function(event){
		var styleid=$(this).attr("data-styleid");
		var styleurl=$(this).attr("data-href");
                var fromCart = 0;
		if(typeof styleid != "undefined" || styleid != ""){
			Myntra.QuickLook.loadQuickLook(styleid, styleurl, fromCart);
			if(typeof $(this).attr("data-widget") != "undefined"){
				_gaq.push(['_trackEvent', 'quicklook', Myntra.Data.pageName, $(this).attr("data-widget"), parseInt(styleid)]);
			}
			else{
				_gaq.push(['_trackEvent', 'quicklook', window.location.toString(), styleid]);
			}
		}
		event.stopPropagation();
		return false;
	});

	$(".category .item, .product-listing .item").live("mouseover", function(){
		$(this).find(".quick-look").show();
	});

	$(".category .item, .product-listing .item").live("mouseout", function(){
		$(this).find(".quick-look").hide();
	});

	$(".quick-look-cart-btn").die().live("click", function(event){
		if(Myntra.MiniPDP.ifSizeOptionSelected()){
			$('#hiddenFormMiniPIP').trigger('submit');
			//_gaq.push(['_trackEvent', 'minipip', 'buynow', '']);
			_gaq.push(['_trackEvent', 'buy_now', Myntra.Data.pageName, 'quicklook']);
            event.stopPropagation();
		}
	});

	$(".more-details").die().live("click", function(event){
		//_gaq.push(['_trackEvent', 'minipip', 'moredetails', '']);
		_gaq.push(['_trackEvent', 'pdp_click', Myntra.Data.pageName, 'quicklook_view_details']);
        event.stopPropagation();
	});



}

Myntra.QuickLook = (function(){
	var obj = {};
	var lb;
	var quicklookthumbimginfo;

    obj.loadQuickLook = function(id, url, fromCart){
    	lb = Myntra.LightBox('#quick-look-mod');
    	obj.getData(id, url, fromCart);
    	obj.quicklookthumbimginfo = new Object();
        return lb;
    };

    obj.getData = function(id, url, fromCart){
    	lb.show();
    	lb.clearPopupContent();
    	lb.showLoading();
    	$.get( http_loc+"/getMiniPIP.php?id="+id+"&fc="+fromCart, null, function(data){
    		lb.hideLoading();
    		$("#quick-look-mod .bd").html(data);
    		$("#quick-look-mod .more-details").attr("href", url);
    		Myntra.MiniPDP.NavButtons.processImages();
    		Myntra.MiniPDP.NavButtons.InitNavButtons();
    		Myntra.PDP.SizesTooltip.InitTooltip();
    		//Myntra.InitTooltip('.pdp-tooltip', '.pdp-trigger', {side:'below'});
    		lb.center();
    	});
    }

    return obj;

})();

$(document).ready(function() {
    if (Myntra.InitLogin('#lb-login')) {
        Myntra.InitHeaderButtons();
    }
    Myntra.InitTooltip('.pdp-tooltip', '.pdp-trigger', {side:'below'});
    Myntra.PDP.SizesTooltip.InitTooltip();
    Myntra.InitQuickLook();
    Myntra.InitTooltip('.free-voucher-cart-link-tt', '.free-voucher-cart-link', {side:'below'});
});

Myntra.MiniPDP.sizeSelect = function(id,optidx,el){
	Myntra.MiniPDP.selectSizeOption(id,optidx,el);
	Myntra.MiniPDP.setOptionsAndQuantity(id);
}

Myntra.MiniPDP.selectSizeOption = function(id,optidx,el){
	$(".quick-look-flat-size-options a").removeClass("selected");
	$(".quick-look-flat-size-options a:nth("+(optidx-1)+")").addClass("selected");
	if($(".quick-look-size-options-error-msg").css("display")=="block"){
		$(".quick-look-size-options-error-msg").slideUp();
		if($(".quick-look-flat-size-options").hasClass("size-options-error")){
			$(".quick-look-flat-size-options").removeClass("size-options-error");
		}
	}
}

Myntra.MiniPDP.setOptionsAndQuantity = function(id){
	$("#quick-look-mod .sizequantity").val('');
	var quantity = $("#quick-look-mod #quantity").val();
	$("#quick-look-mod #sizequantity"+id).val(quantity);
	$("#quick-look-mod #productSKUIDMiniPIP").val(id);
}

Myntra.MiniPDP.ifSizeOptionSelected = function(){
	var skuid=$("#quick-look-mod #productSKUIDMiniPIP").val();
	if(typeof skuid == "undefined" || $("#quick-look-mod #productSKUIDMiniPIP").val() == ""){
		$(".quick-look-size-options-error-msg").slideDown();
		if(!$(".quick-look-flat-size-options").hasClass("size-options-error")){
			$(".quick-look-flat-size-options").addClass("size-options-error");
		}
		return false;
	}
	else{
		return true;
	}
}

Myntra.MiniPDP.NavButtons = (function(){
	var obj = {};
	var nextButton,
		previousButton,
		quicklookNoOfViews;
	var firstCall=true;

    obj.InitNavButtons = function(){
    	quicklookNoOfViews=$(".quick-look-thumb-views a").length-1;
    	nextButton=$("#quick-look-mod .quick-look-next");
    	previousButton=$("#quick-look-mod .quick-look-prev");
    	previewImageContainer=$("#quick-look-mod .quick-look-preview");
    	$(nextButton).click(function(){
    		obj.Next();
    		if(firstCall){
    			_gaq.push(['_trackEvent', 'minipip', 'nav', '']);
    			firstCall=false;
    		}
    	});
    	$(previousButton).click(function(){
    		obj.Previous();
    		if(firstCall){
    			_gaq.push(['_trackEvent', 'minipip', 'nav', '']);
    			firstCall=false;
    		}
    	});
    	obj.initNextPrevButtons();
    };

    obj.getNextIndex = function(){
    	var selectedval=$(".quicklookloadedimage").val();
    	var currIdx=parseInt(selectedval.substr(selectedval.indexOf("_")+1, selectedval.length));

    	var nextIdx=0;
    	if(currIdx+1 > quicklookNoOfViews){
    		return;
    	}
    	if(currIdx < quicklookNoOfViews){
    		nextIdx=++currIdx;
    	}

    	return nextIdx;
    };

    obj.getPreviousIndex = function(){
    	var selectedval=$(".quicklookloadedimage").val();
    	var currIdx=parseInt(selectedval.substr(selectedval.indexOf("_")+1, selectedval.length));
    	var nextIdx=0;
    	if(currIdx-1 < 0){
    		return;
    	}
    	if(currIdx > 0){
    		nextIdx=--currIdx;
    	}
    	return nextIdx;
    };

    obj.updateNextPreviousButtons = function(idx){
    	if(quicklookNoOfViews==0){
   	        if(!previousButton.hasClass("hide")){previousButton.addClass("hide")};
   	        if(!nextButton.hasClass("hide")){nextButton.addClass("hide")};
   	        return;
   	 	}
   	 	if(idx == 0){
   	 		previousButton.addClass("hide");
   	 		nextButton.removeClass("hide");
   	 	}
   	 	if(idx == quicklookNoOfViews){
   	 		nextButton.addClass("hide");
   	 		previousButton.removeClass("hide");
   	 	}
   	 	if(idx > 0 && idx < quicklookNoOfViews){
   	 		nextButton.removeClass("hide");
   	 		previousButton.removeClass("hide");
   	 	}
    };

    obj.Next = function(){
    	var idx=obj.getNextIndex();
    	obj.updateNextPreviousButtons(idx);
    	obj.loadImage(idx);
    };

    obj.Previous = function(){
    	var idx=obj.getPreviousIndex();
    	obj.updateNextPreviousButtons(idx);
    	obj.loadImage(idx);
    };

    obj.initNextPrevButtons = function(){
    	if(parseInt(quicklookNoOfViews) == 0){
    		nextButton.addClass("hide");
    		previousButton.addClass("hide");
    	}
    	if(parseInt(quicklookNoOfViews) > 0){
    		previousButton.addClass("hide");
    	}
    }

    obj.loadImage = function(idx){
    	$(".quicklookloadedimage").val("quicklookthumb_"+idx);
    	var thumbIdx=$(".quicklookloadedimage").val();
    	$(".preview-loading").show();

    	var imgUrl=Myntra.QuickLook.quicklookthumbimginfo[thumbIdx].prodimage;
    	if(imgUrl == ""){
    		return;
    	}
    	var img = new Image();
    	img.onload = function(){
    		$(".quick-look-preview").attr("src",imgUrl);
    		$(".preview-loading").hide();
    		$(".quick-look-thumb-views .selected").removeClass("selected");
    		$("#"+thumbIdx).addClass("selected");
    		$(".quicklookloadedimage").val(thumbIdx);
    	};
    	img.src = imgUrl;
    }

    obj.processImages = function(){
    	$.each($(".quick-look-thumb-views a"),function(){
    		if(typeof Myntra.QuickLook.quicklookthumbimginfo[$(this).attr("id")] == "undefined"){
    			Myntra.QuickLook.quicklookthumbimginfo[$(this).attr("id")] = $.parseJSON('{'+$(this).attr("rel")+'}');
    		}
    	});

    	$(".quick-look-thumb-views a").click(function(){
    		var thumbIdx=$(this).attr("id");
    		if(typeof Myntra.QuickLook.quicklookthumbimginfo[thumbIdx] == "undefined"){
    			Myntra.QuickLook.quicklookthumbimginfo[thumbIdx] = $.parseJSON('{'+$(this).attr("rel")+'}');
    		}
    		$(".preview-loading").show();
    		var idx=thumbIdx.substr(thumbIdx.indexOf("_")+1, thumbIdx.length);
    		obj.updateNextPreviousButtons(idx);
    		obj.loadImage(idx);
    	});
    }

    return obj;
})();

Myntra.Combo = {};
Myntra.Combo.ComboOverlay = {};
Myntra.Combo.isConditionMet = false;
Myntra.Combo.selectedStyles = [];
Myntra.Combo.min = 0;
Myntra.Combo.cartSavings = 0;
Myntra.Combo.cartTotal = 0;
Myntra.Combo.products = {};
Myntra.Combo.selectedStylesQntyChanged = [];
Myntra.Combo.addedStyles = {};
Myntra.Combo.removedStyles = {};
Myntra.Combo.quantity_in_combo = 0;
Myntra.Combo.isRemoved = false;
Myntra.Combo.allSelectedStyles = Object();
Myntra.Combo.solrProducts = [];
Myntra.Combo.ctype = "";
Myntra.Combo.isAmount = false;


Myntra.Combo.ReInitializeVariables = function () {
    //these commented variables are set in the
//    Myntra.Combo.isConditionMet = {$isConditionMet|@json_encode};
//    Myntra.Combo.selectedStyles = {$selectedStyles|@json_encode};
//    Myntra.Combo.minMore = {$minMore};
//    Myntra.Combo.cartSavings = {$comboSavings};
//    Myntra.Combo.cartTotal = {$comboTotal};
//    Myntra.Combo.products = {$products|@json_encode};

    Myntra.Combo.selectedStylesQntyChanged = [];
    Myntra.Combo.addedStyles = {};
    Myntra.Combo.removedStyles = {};
    Myntra.Combo.quantity_in_combo = 0;
    Myntra.Combo.isRemoved = false;
    Myntra.Combo.allSelectedStyles = Object();
    Myntra.Combo.minMore = Myntra.Combo.min;
	Myntra.Combo.imagesDownloaded = false;
    //set the width of the ul
    $(".items").css("width",($(".items li").length*240 + 50) + "px");
}

Myntra.Combo.SelectStyles = function () {
    for(var i=0; i<Myntra.Combo.selectedStyles.length; i++){
        $(".item_"+Myntra.Combo.selectedStyles[i]+" input:checkbox").attr("checked","true");
        Myntra.Combo.SelectStyle($(".item_"+Myntra.Combo.selectedStyles[i]+" input:checkbox")[0], true);
        $(".cart-widget-items").prepend($(".item_" + Myntra.Combo.selectedStyles[i]));
    }
    //Myntra.Combo.SortStyles();
}

Myntra.Combo.SelectStyle = function(input, firstRun) {
    var styleId = input.value;
    var skuid = parseInt($(input).attr("data-skuid"));
    if($(input).is(":checked")){//if selected
        Myntra.Combo.LoadSizes(styleId);
        if(Myntra.Combo.removedStyles[styleId]!=null)
        Myntra.Combo.removedStyles[styleId][skuid] = 0;

        $(".cow-item-box-" + styleId).each(function(index){
            if(parseInt($(this).find("select").attr("data-skuid"))==skuid)
                $(this).addClass("cart-widget-item-box-selected");
        });

        $(".size-selection-row-"  + styleId).each(function(index){
            if(parseInt($(this).find("select").attr("data-skuid"))==skuid)
                $(this).css("visibility", "visible");
        });

        var qnty = 1;
        $(".quantity-selection-row-"  + styleId).each(function(index){
            if(parseInt($(this).find("input").attr("data-skuid"))==skuid){
                $(this).css("visibility", "visible");
                if(!firstRun)
                    $(this).find("input").val("1");
                else
                    qnty = parseInt($(this).find("input").val());
            }
        });


        if(Myntra.Combo.allSelectedStyles[styleId]==null) Myntra.Combo.allSelectedStyles[styleId] = [];
        Myntra.Combo.allSelectedStyles[styleId][skuid] = qnty;
        if(!firstRun){
            if(Myntra.Combo.addedStyles[styleId]==null) Myntra.Combo.addedStyles[styleId] = [];
            Myntra.Combo.addedStyles[styleId][skuid] = qnty;
        }
        if(!firstRun){
            Myntra.Combo.cartTotal += Myntra.Combo.solrProducts[styleId].price;
        }

        Myntra.Combo.quantity_in_combo += qnty;
        Myntra.Combo.amount_in_combo = Myntra.Combo.cartTotal;

        if(!Myntra.Combo.isAmount)
            Myntra.Combo.minMore = Myntra.Combo.min - Myntra.Combo.quantity_in_combo;
        else
            Myntra.Combo.minMore = Myntra.Combo.min - Myntra.Combo.amount_in_combo;

        Myntra.Combo.isConditionMet = Myntra.Combo.minMore<=0?true:false;

        if(!firstRun){
            //{if $isConditionMet and $product.discountAmount>0}{math equation="(c + (p * q) - d)" p=$product.productPrice c=$comboSavings q=$product.quantity d=$product.discountAmount assign="comboSavings"}{/if}
            if(Myntra.Combo.isConditionMet && Myntra.Combo.solrProducts[styleId].dre_amount>0){
                Myntra.Combo.cartSavings = Myntra.Combo.solrProducts[styleId].dre_amount;
            }
            if(Myntra.Combo.isConditionMet && Myntra.Combo.solrProducts[styleId].dre_percent>0){
                Myntra.Combo.cartSavings = Myntra.Combo.cartTotal * Myntra.Combo.solrProducts[styleId].dre_percent / 100;
            }
        }


        //Myntra.Combo.cartSavings = 0;
        //Myntra.Combo.cartTotal = 0;
    } else {//if removed
        for(var i=0; i<Myntra.Combo.selectedStyles.length; i++){
            if(Myntra.Combo.selectedStyles[i]==styleId){
                Myntra.Combo.isRemoved = true;
                break;
            }
        }

        if(Myntra.Combo.addedStyles[styleId]!=null) Myntra.Combo.addedStyles[styleId][skuid] = null;
        if(Myntra.Combo.allSelectedStyles[styleId]!=null) Myntra.Combo.allSelectedStyles[styleId][skuid] = null;

        $(".cow-item-box-" + styleId).each(function(index){
            if(parseInt($(this).find("select").attr("data-skuid"))==skuid)
                $(this).removeClass("cart-widget-item-box-selected");
        });

        $(".size-selection-row-"  + styleId).each(function(index){
            if(parseInt($(this).find("select").attr("data-skuid"))==skuid)
                $(this).css("visibility", "hidden");
        });

        var qnty = 0;
        $(".quantity-selection-row-"  + styleId).each(function(index){
            if(parseInt($(this).find("input").attr("data-skuid"))==skuid){
                $(this).css("visibility", "hidden");
                qnty = parseInt($(this).find("input").val());
            }
        });

	$(".item_"+styleId+" select[data-skuid]").val('-');

        if(Myntra.Combo.removedStyles[styleId]==null) Myntra.Combo.removedStyles[styleId] = [];
        Myntra.Combo.removedStyles[styleId][skuid] = qnty;

        Myntra.Combo.cartTotal -= (Myntra.Combo.solrProducts[styleId].price * qnty);

        Myntra.Combo.quantity_in_combo -= qnty;
        Myntra.Combo.amount_in_combo = Myntra.Combo.cartTotal;

        if(Myntra.Combo.ctype=='c') Myntra.Combo.minMore = Myntra.Combo.min - Myntra.Combo.quantity_in_combo;
        if(Myntra.Combo.ctype=='a') Myntra.Combo.minMore = Myntra.Combo.min - Myntra.Combo.amount_in_combo;
        Myntra.Combo.isConditionMet = Myntra.Combo.minMore<=0?true:false;

        if(!Myntra.Combo.isConditionMet){
            Myntra.Combo.cartSavings = 0;
        }
        if(Myntra.Combo.isConditionMet && Myntra.Combo.solrProducts[styleId].dre_percent>0){
            Myntra.Combo.cartSavings = Myntra.Combo.cartTotal * Myntra.Combo.solrProducts[styleId].dre_percent / 100;
        }
    }

}

Myntra.Combo.ModifySize = function(selectElement) {
    var prevSkuid = $(selectElement).attr("data-skuid");
    var skuid = $(selectElement).find("option:selected").val();
    $(selectElement).attr("data-skuid", skuid);
    $(selectElement).parents(".cart-widget-item-container").find("input").attr("data-skuid", skuid);
    var styleId = $(selectElement).parents(".cart-widget-item-container").find("input:checkbox[name=selStyle]").val();

    Myntra.Combo.allSelectedStyles[styleId][skuid] = Myntra.Combo.allSelectedStyles[styleId][prevSkuid];
    Myntra.Combo.addedStyles[styleId][skuid] = Myntra.Combo.addedStyles[styleId][prevSkuid];

    delete Myntra.Combo.allSelectedStyles[styleId][prevSkuid] ;
    delete Myntra.Combo.addedStyles[styleId][prevSkuid];
}

Myntra.Combo.ModifyQuantity = function(inputElement) {
    var styleId = inputElement.getAttribute("data-styleId");
    var skuid = inputElement.getAttribute("data-skuid");
    var qnty = Myntra.Combo.allSelectedStyles[styleId][skuid];
    if($(inputElement).val() == '') $(inputElement).val(1);
    var changedQnty = parseInt(inputElement.value);
    if(changedQnty < 1) {
	changedQnty = 1;
	$(inputElement).val(1);
    }

    //if there is no change in quantity
    if(qnty==changedQnty) return;

    var qntyReducedBy = qnty-changedQnty;
    Myntra.Combo.cartTotal -= (Myntra.Combo.solrProducts[styleId].price * qntyReducedBy);

    Myntra.Combo.quantity_in_combo -= qntyReducedBy;
    Myntra.Combo.amount_in_combo = Myntra.Combo.cartTotal;

    if(!Myntra.Combo.isAmount)
        Myntra.Combo.minMore = Myntra.Combo.min - Myntra.Combo.quantity_in_combo;
    else
        Myntra.Combo.minMore = Myntra.Combo.min - Myntra.Combo.amount_in_combo;
    Myntra.Combo.isConditionMet = Myntra.Combo.minMore<=0?true:false;
    if(Myntra.Combo.solrProducts[styleId].dre_amount>0){
        if(!Myntra.Combo.isConditionMet)
            Myntra.Combo.cartSavings = 0;
        else
            Myntra.Combo.cartSavings = Myntra.Combo.solrProducts[styleId].dre_amount;
    }
    if(!Myntra.Combo.isConditionMet) Myntra.Combo.cartSavings = 0;
    if(Myntra.Combo.isConditionMet && Myntra.Combo.solrProducts[styleId].dre_percent>0){
        if(!Myntra.Combo.isConditionMet)
            Myntra.Combo.cartSavings = 0;
        else
            Myntra.Combo.cartSavings = Myntra.Combo.cartTotal * Myntra.Combo.solrProducts[styleId].dre_percent / 100;
    }

    if(Myntra.Combo.addedStyles[styleId]!=null) Myntra.Combo.addedStyles[styleId][skuid] = changedQnty;
    if(Myntra.Combo.allSelectedStyles[styleId]!=null) Myntra.Combo.allSelectedStyles[styleId][skuid] = changedQnty;

    if(Myntra.Combo.removedStyles[styleId]!=null){
        if(qntyReducedBy>=0) {
            Myntra.Combo.removedStyles[styleId][skuid] = qntyReducedBy;
        }else{
            Myntra.Combo.removedStyles[styleId][skuid] = 0;
        }
    }

};

Myntra.Combo.ComboOverlay.RefreshUI = function() {
    var completionMessage = "";
    var discountedTotal = Myntra.Combo.cartTotal - Myntra.Combo.cartSavings;
    var comboOverlayTotalMessage = "<div class=\"cart-discount left\">TOTAL <span class=\"cost\" style=\"color: #D20000;\">Rs. <strong>";
    if(Myntra.Combo.cartSavings>0){
        comboOverlayTotalMessage += "" + Math.round(discountedTotal) + "</strong>&nbsp;<span class=\"strike\"><b>" + Math.round(Myntra.Combo.cartTotal) + "</b></span>";
    } else {
        comboOverlayTotalMessage += "" + Math.round(Myntra.Combo.cartTotal) + "</strong>"
    }
    comboOverlayTotalMessage += "</span></div><BR>";
    comboOverlayTotalMessage += "<div class=\"cart-discount left\">SAVINGS <span style=\"color:#d20000\"> Rs. <strong>" + Math.round(Myntra.Combo.cartSavings) + "</strong></span></div><BR>";

    if(Myntra.Combo.isConditionMet){
        completionMessage = "<span class=\"tick-small-icon\">&nbsp;</span>Combo complete!";// <i>Modify Combo</i>";
    } else {
        if(Myntra.Combo.isAmount){
            completionMessage = "<span class=\"warning-small-icon\">&nbsp;</span>Combo incomplete. <em>Rs. "+Math.round(Myntra.Combo.minMore)+"/- more to go!</em>";
        } else {
            completionMessage = "<span class=\"warning-small-icon\">&nbsp;</span>Combo incomplete. <em>"+Math.round(Myntra.Combo.minMore)+" more to go!</em>";
        }
        comboOverlayTotalMessage += "<div class=\"cart-discount left\">Complete the combo to save!</div>";
    }

    var addToCartBtnText = "";
    if(Myntra.Combo.isConditionMet){
        addToCartBtnText = "ADD COMBO TO BAG <span style=\"font-size:15px;\">&raquo;</span>";
    }else {
        addToCartBtnText = "ADD COMBO TO BAG <span style=\"font-size:15px;\">&raquo;</span>";
    }

/*    if(Myntra.Combo.addedStyles.length==0 && Myntra.Combo.removedStyles.length==0){//diable the add to cart btn
        //$(".add-to-cart").attr("enabled", "true");
    }else {
        //$(".add-to-cart").attr("enabled", "false");
    }
*/
 /*   if(Myntra.Combo.isRemoved){
        $(".cow-replace-item-msg").html("You have replaced your original item(s).");
    } else {
        $(".cow-replace-item-msg").html("&nbsp;");
    }
   */
    $(".combo-add-to-cart-button").html(addToCartBtnText);
    $(".completion-message").html(completionMessage);
    $(".combo-overlay-total").html(comboOverlayTotalMessage);

};

Myntra.Combo.LoadSizes = function(styleId) {
    var objId="select#cow-size-select-"+styleId;
    //if($(objId).attr('disabled')) return;
    $(objId).attr('disabled','true');
    var rURL=http_loc+"/recentwidget_getsizes.php?ids="+styleId;
    $.ajax({
        type:"GET",
        url:rURL,
        beforeSend:function(){
            $(objId).empty().append('<option value="-">-</option>');
        },
        success: function(data){
            $(objId).empty().append('<option value="-">-</option>');
            	//result=$.parseJSON(data);
		result=data;
                $.each(result, function(id, sizes) {
                    id = "select[id^=cow-size-select-" + id +"]";
                    $(id).each(function(index){
                        var sid = $(this);
                        var skuid = $(this).attr("data-skuid");
                        $.each(sizes, function(val, txt) {
                            if (val == 0) {
                                $(sid).empty().removeClass('widget-size-select');
				//$(sid).addClass('cow-size-select-disabled');
				//$("#"+styleId+"_cow_qty").attr('disabled','true');
			    }
                            var option = $('<option></option>').val(val);
                            option.html(txt);
                            if(parseInt(skuid)!='NaN' && val==parseInt(skuid)) option.attr("selected","selected");
                            $(sid).append(option);
                        });
                    });
                });
	    if($(objId).hasClass('cow-size-select-disabled')) $(objId).attr('disabled','true');
            else $(objId).removeAttr('disabled');
        }
    });
};

Myntra.Combo.AddSkuFromMiniPip = function(styleid, skuid){
    $(".item_"+styleid).each(function(index){
        var sid = parseInt($(this).find(":checkbox").attr("data-skuid"));
	var prevSkuid = $(this).find("select[data-skuid]").val();
	if(prevSkuid != '-'){
		$(this).find(":checkbox").removeAttr("checked");
                Myntra.Combo.SelectStyle($(this).find(":checkbox")[0], false);
	}

        //if(sid==0){
            $(this).find("input[data-skuid]").attr("data-skuid",skuid);
            $(this).find("select[data-skuid]").attr("data-skuid",skuid);
	    $(this).find("select[data-skuid]").val(skuid);
        //}
            if(!$(this).find(":checkbox").is(":checked")){
                $(this).find(":checkbox").attr("checked", "true");
                Myntra.Combo.SelectStyle($(this).find(":checkbox")[0], false);
                Myntra.Combo.ComboOverlay.RefreshUI();
            } else {
		$(this).find(":checkbox").attr("checked", "true");

	    }

    });
}

Myntra.Utils.loadImageQueue = function(){
    var lazy_images = $('.lazy');
    $.each(lazy_images, function() {
    	$(this).attr("src", $(this).attr("data-lazy-src"));
    	$(this).removeAttr("data-lazy-src");
    	$(this).removeClass("lazy");
    });
};

Myntra.Combo.RegisterEvents = function () {

    $(".cart-widget-scrollable-box .nextPage").die().live('click',function(event) {
        if(!Myntra.Combo.imagesDownloaded){
            Myntra.Utils.loadImageQueue();
            Myntra.Combo.imagesDownloaded = true;
        }
    });

    $(".cart-widget-scrollable-box .prevPage").die().live('click',function(event) {
        if(!Myntra.Combo.imagesDownloaded){
            Myntra.Utils.loadImageQueue();
            Myntra.Combo.imagesDownloaded = true;
        }
    });
	
    //checkbox on click event
    $(":checkbox").click(function(event) {
        var input = $(this)[0];
        if($(input).is(':checked')){
        	_gaq.push(['_trackEvent', 'combo', 'select_style', 'check', parseInt(input.value)]);
        } else {
        	_gaq.push(['_trackEvent', 'combo', 'select_style', 'uncheck', parseInt(input.value)]);
        }
        Myntra.Combo.SelectStyle(input, false);
        Myntra.Combo.ComboOverlay.RefreshUI();
    });

    lb = Myntra.LightBox('#cart-combo-overlay');

    //cancel the pop-up event
    $(".combo-overlay-cancel").click(function(e){
    	_gaq.push(['_trackEvent', 'combo', 'cancel', Myntra.Data.pageName,parseInt(Myntra.Combo.ComboId)]);
    	lb.hide();
    });

    //initialize tool-tip
    $("input:checkbox").each(function(index){
        var styleId = $(this).attr("value");
        Myntra.InitTooltip('#cow-tooltip-'+styleId, '#cow-price-'+styleId, {side:'below'});
    });

    //bind on change to the quantity
    $("input:text").change(function(event){
        Myntra.Combo.ModifyQuantity($(this)[0]);
        Myntra.Combo.ComboOverlay.RefreshUI();
    });

    $("select").change(function(event){
        Myntra.Combo.ModifySize($(this)[0]);
        Myntra.Combo.ComboOverlay.RefreshUI();
    });

    $('.quantity-box').keydown(function(event) {
            var key = event.charCode || event.keyCode || 0;
            return (
            key == 8 || key == 9 || key == 46 || key == 13 || (key >= 48 && key <= 57)) && (!event.shiftKey);
    });

    $(".quantity-box").focus(function() {
        $(this).css({
                border: '2px solid black',
                background: "#eee"
        });
    });


    $('.combo-add-to-cart-button').click(function(){
        var isValidated = true;
        $("input:checked").parents(".cart-widget-item-container").find("select option:selected").each(function(index){
            if($(this).val()=="-"){
                alert('please select a size');
                isValidated = false;
                return false;
            }
            if($(this).val()==0){
                isValidated = false;
                return false;
            }
        });
        if(!isValidated)
            return false;

        var postArray = [];

        for(var iStyleId in Myntra.Combo.addedStyles){
            for(var iSkuId in Myntra.Combo.addedStyles[iStyleId]){
                var postItem = Object();
                postItem["skuid"] = iSkuId;
                postItem["quantity"] = Myntra.Combo.addedStyles[iStyleId][iSkuId];
                if(postItem["quantity"]!=null && postItem["quantity"] != 0) postArray.push(postItem);
            }
        }
        var postRemoveArray = [];
        var postRemoveItem = Object();
        for(var iStyleId in Myntra.Combo.removedStyles){
            for(var iSkuId in Myntra.Combo.removedStyles[iStyleId]){
                postRemoveItem["skuid"] = iSkuId;
                postRemoveItem["quantity"] = Myntra.Combo.removedStyles[iStyleId][iSkuId];
                postRemoveArray.push(postItem);
            }
        }
        _gaq.push(['_trackEvent', 'buy_now', Myntra.Data.pageName, 'combo_overlay', parseInt(Myntra.Combo.ComboId)]);
        $.ajax({
            type: 'POST',
            url: "mkretrievedataforcart.php?pagetype=productdetail&lead=recently_viewed",
            data:     {
                'fromComboOverlay' : 1,
                'cartPostData' : JSON.stringify(postArray),
                '_token' : $('input:hidden[name=_token]').val()
            },
            beforeSend:function(){
                Myntra.LightBox('#cart-combo-overlay').showLoading();
            },
            success: function(data){
                window.parent.location = document.location.protocol+'//'+document.location.hostname+"/mkmycart.php";
            },
            error: function(data){
                alert(data);
                alert('Error in saving. Please try again later.');
                return false;
            },
            complete: function(){
                return false;
            }
        });

        return false;
    });
}

Myntra.Combo.InitQuickLook = function() {


    //$(".quick-look").die("click");
    //$(".category .item, .product-listing .item").die("mouseover");
    //$(".category .item, .product-listing .item").die("mouseout");

    var lb = null;
    $(".cart-quick-look").live('click', function(){
            var styleid=$(this).attr("data-styleid");
            var styleurl=$(this).attr("data-href");
            var fromCart=1;
            if(typeof styleid != "undefined" || styleid != ""){
                lb = Myntra.QuickLook.loadQuickLook(styleid, styleurl, fromCart);
                if(typeof $(this).attr("data-widget") != "undefined"){
    				_gaq.push(['_trackEvent', 'quicklook', Myntra.Data.pageName, $(this).attr("data-widget"), parseInt(styleid)]);
    			}
    			else{
    				_gaq.push(['_trackEvent', 'quicklook', window.location.toString(), parseInt(styleid)]);
    			}
            }

    });


    $(".cart-combo-widget-image").live("mouseover", function(event){
	    if(!$(this).hasClass("cart-widget-item-box-pre-added")){
                $(this).find(".cart-quick-look").show();
	    }
    });

    $(".cart-combo-widget-image").live("mouseout", function(event){
	if(!$(this).hasClass("cart-widget-item-box-pre-added")){
            $(this).find(".cart-quick-look").hide();
	}
    });

    $("#back-to-combo-actn-btn").live("click", function(event){
        lb.hide();
		_gaq.push(['_trackEvent', 'combo', 'cancel_quicklook', '']);
        event.stopPropagation();
    });
    $(".quick-look-combo-btn").live("click", function(event){
        if(Myntra.MiniPDP.ifSizeOptionSelected()){
            var styleid = $('#hiddenFormMiniPIP').find('input[name="productStyleId"]').val();
            var skuid = parseInt($('#hiddenFormMiniPIP').find('input[name="productSKUID"]').val());
            lb.hide();
			_gaq.push(['_trackEvent', 'combo', 'select_style', 'quicklook', parseInt(styleid)]);	
            Myntra.Combo.AddSkuFromMiniPip(styleid, skuid);
        }
        event.stopPropagation();
    });



    //Myntra.InitQuickLook();

}

Myntra.Combo.ComboOverlay.Init = function () {
    Myntra.Combo.ReInitializeVariables();
    Myntra.Combo.RegisterEvents();
    Myntra.Combo.ComboScroll();
    Myntra.Combo.SelectStyles();
    Myntra.Combo.ComboOverlay.RefreshUI();
    Myntra.Combo.InitQuickLook();
	_gaq.push(['_trackEvent', 'combo', 'click', Myntra.Data.pageName, parseInt(Myntra.Combo.ComboId)]);
}

Myntra.Combo.ComboScroll = function () {
    $(".cart-widget-scrollable").scrollable({
        size:5,
        keyboard:false,
        clickable:false});

    var api = $(".cart-widget-scrollable").data('scrollable');
    // Fix for scrollable to disable next button on initial load if the number
    // of items is exactly equal to the size of the window
    $(".cart-widget-scrollable-box").each(function(){
        if($(".items .item",this).length == 5){
            $(".nextPage",this).addClass("disabled");
        }

    });


    api.onSeek(function(e, index) {
        var from_index = index+1;
        var to_index = index+5;
        if(to_index>api.getSize())
            to_index = api.getSize();
        $(".pagination-start").html(from_index);
        $(".pagination-end").html(to_index);
    });
}

Myntra.PDP.SizesTooltip = (function(){
	var obj = {};
	var xOffset=0;
	var yOffset=0;
	var text="";
	var top=0;
	var left=0;

	obj.InitTooltip = function(){
		$(".vtooltip").unbind().hover(
				function(e){
		            text = $(this).attr("rel");
		            yOffset = $(this).attr("data-tooltipTop");
					var elProp = $(e.target).offset();
		            top = (elProp.top - yOffset);
		            left = (elProp.left + xOffset);

		            if(text != "" || typeof text != "undefined"){
						$('body').append( '<div id="vtooltip">' + text + '</div>' );
		            	$('div#vtooltip').css("top", top+"px").css("left", left+"px").fadeIn("fast");
		            }
				},
				function(e){
					$("div#vtooltip").fadeOut("fast").remove();
				}
		);

	}

	return obj;

})();


// Search autocomeplete
if(Myntra.Data.showAutoSuggest == 1) {
    $(document).ready(function() {
        function formatItem(row) {
            return row[0] + "<span style=\"float:right\">" + row[1] + "</span>";
        }

        $("#search_query").autocomplete("/myntra/auto_suggest.php", {
            max: 12,
            scroll: false,
            width: 582,
            delay: 100,
            matchSubset:true,
            matchContains:true,
            selectFirst:false,
            formatItem: formatItem,
            highlight: function(value, term) {
		        return value.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + term.replace(/([\^\$\(\)\[\]\{\}\*\.\+\?\|\\])/gi, "\\$1") + ")(?![^<>]*>)(?![^&;]+;)", "gi"), "<span style=\"color:#000\">$1</span>");
	        }
        });
        $("#search_query").result(function(event, data, formatted) {
            if (data)
            $(this).val(data[2]);
            menuSearchFormSubmit();
        });
    });
}
