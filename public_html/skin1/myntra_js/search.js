/*
 * Custom Search Page JS code added by Raghu
 */

filterProperties["brands"]={
	getURLParam: function(){
		var brandsStr=configArray["brands"].join(":");
		return (brandsStr != "")?"brands="+brandsStr:"";
	},
	processURL: function(str){
		configArray["brands"]=Myntra.Search.getFilterValues(str);
		return true;
	},
	updateUI: function(){
		//unselect all brands
		$(".brand-filters .filter-box").removeClass("checked").addClass("unchecked");

		//update UI selection for brands in configArray
		for(var brand=0;brand<configArray["brands"].length; brand++){
			var selector=configArray["brands"][brand].toLowerCase().replace(/\W+/g, "") + "-brand-filter";
			$("."+selector).removeClass("unchecked").addClass("checked");
		}
		return true;
	},
	setToDefault: function(){
		configArray["brands"]=[];
		return true;
	},
	resetParams: function(){
		return true;
	}};
filterProperties["sizes"]={
	getURLParam: function(){
		var sizesStr=configArray["sizes"].join(":");
		return (sizesStr != "")?"sizes="+sizesStr:"";
	},
	processURL: function(str){
		configArray["sizes"]=Myntra.Search.getFilterValues(str);
		return true;
	},
	updateUI: function(){
		//unselect all sizes
		$(".size-filters .filter").removeClass("selected").addClass("unselected");
		
		//update UI selection for sizes in configArray
		for(var size=0;size<configArray["sizes"].length;size++){
			var selector=configArray["sizes"][size].toLowerCase().replace(/\W+/g, "") + "-size-filter";
			$("."+selector).removeClass("unselected").addClass("selected");
		}
		return true;
	},
	setToDefault: function(){
		configArray["sizes"]=[];
		return true;
	},
	resetParams: function(){
		return true;
	}};
filterProperties["gender"]={
	getURLParam: function(){
		var genderStr=configArray["gender"].join(":");
		return (genderStr != "")?"gender="+genderStr:"";
	},
	processURL: function(str){
		configArray["gender"]=Myntra.Search.getFilterValues(str);
		return true;
	},
	updateUI: function(){
		//unselect all gender
		$(".gender-filters .filter-box").removeClass("checked").addClass("unchecked");
								
		//update UI selection for gender in configArray
		for(var gender=0;gender<configArray["gender"].length;gender++){
			var selector=configArray["gender"][gender].toLowerCase().replace(/\W+/g, "") + "-gender-filter";
			$("."+selector).removeClass("unchecked").addClass("checked");
		}
		return true;
	},
	setToDefault: function(){
		configArray["gender"]=[];
		return true;
	},
	resetParams: function(){
		return true;
	}};
filterProperties["noofitems"]={
	getURLParam: function(){
		var noofitems=parseInt(configArray["noofitems"]);
		return (!isNaN(noofitems))?"noofitems="+noofitems:24;
	},
	processURL: function(str){
		configArray["noofitems"]=str;
		return true;
	},
	updateUI: function(){
		//set noofitems dropdown
		$(".noofitems-select").val(configArray["noofitems"]);
		return true;
	},
	setToDefault: function(){
		configArray["noofitems"]=default_noofitems;
		return true;
	},
	resetParams: function(){
		configArray["page"]=0;
		return true;
	}};
filterProperties["page"]={
	getURLParam: function(){
		var pagenum=parseInt(configArray["page"])+1;
		return (!isNaN(pagenum))?"page="+pagenum:0;
	},
	processURL: function(str){
		configArray["page"]=parseInt(str)-1;
		if(configArray["page"] == 0){
		    Myntra.Search.InfiniteScroll.pageSetToFirst=true;
		}
		Myntra.Search.InfiniteScroll.pageSetFromHash=true;
		return true;
	},
	updateUI: function(){
		return true;
	},
	setToDefault: function(){
		configArray["page"]=pageDefault;
		Myntra.Search.InfiniteScroll.pageSetToFirst=true;
		Myntra.Search.InfiniteScroll.pageSetFromHash=true;
		onPageLoad=true;
		return true;
	},
	resetParams: function(){
		return true;
	}};
filterProperties["sortby"]={
	getURLParam: function(){
		return "sortby="+configArray["sortby"];
	},
	processURL: function(str){
		configArray["sortby"]=str;
		return true;
	},
	updateUI: function(){
		//set sortyby param
		$(".sort-by-filters .filter").removeClass("sort-selected");
		var sortParam=configArray["sortby"].toLowerCase();
		if(configArray["sortby"] != ""){
			$(".sort-by-filters ."+sortParam+"-sort-filter").addClass("sort-selected");
		}
		return true;
	},
	setToDefault: function(){
		configArray["sortby"]=sortByDefault;
		return true;
	},
	resetParams: function(){
		configArray["page"]=0;
		return true;
	}};
filterProperties["pricerange"]={
	getURLParam: function(){
		return "pricerange="+configArray["pricerange"].rangeMin+":"+configArray["pricerange"].rangeMax;
	},
	processURL: function(str){
		pricerange_values=Myntra.Search.getFilterValues(str);
		var minVal=parseInt(pricerange_values[0]);
		var maxVal=parseInt(pricerange_values[1]);
		if(!isNaN(minVal) && !isNaN(maxVal) && minVal >= priceRangeMin && minVal <= priceRangeMax && maxVal >= priceRangeMin && maxVal <= priceRangeMax){
			if(minVal<maxVal){
				configArray["pricerange"].rangeMin=minVal;
				configArray["pricerange"].rangeMax=maxVal;
			}
			else{
				configArray["pricerange"].rangeMin=maxVal;
				configArray["pricerange"].rangeMax=minVal;
			}
		}
		return true;
	},
	updateUI: function(){
		//unselect all price range
		$(".price-filters .filter-box").removeClass("highlight");
								
		//update UI selection for price in configArray
		var selector=configArray["pricerange"].rangeMin + "-" + configArray["pricerange"].rangeMax + "-price-filter";
		
		$("."+selector).addClass("highlight");
		Myntra.Search.Utils.setPriceSlider(configArray["pricerange"].rangeMin,configArray["pricerange"].rangeMax);
		return true;
	},
	setToDefault: function(){
		configArray["pricerange"].rangeMin=priceRangeMin;
		configArray["pricerange"].rangeMax=priceRangeMax;
		return true;
	},
	resetParams: function(){
		return true;
	}};
scrollProperties["scrollPage"]={
		getURLParam: function(){
			return "scrollPage="+configArray["page"];
		},
		processURL: function(str){
			scrollConfigArray["scrollPage"]=parseInt(str);
		},
		updateUI: function(){
		},
		setToDefault: function(){
		},
		resetParams: function(){
		}};
scrollProperties["scrollPos"]={
		getURLParam: function(){
			return "scrollPos="+$(window).scrollTop();
		},
		processURL: function(str){
			scrollConfigArray["scrollPos"]=parseInt(str);
		},
		updateUI: function(){
		},
		setToDefault: function(){
		},
		resetParams: function(){
		}};


$(document).ready(function(){
	if(!noResultsPage){
		if(searchpagescrolltype=="control"){
			Myntra.Search.InitPagination();
			ajaxCallFunc="Pagination";
		}
		else{
			Myntra.Search.InitInfiniteScroll();
			ajaxCallFunc="InfiniteScroll";
		}
		Myntra.Search.InitCommon();
	}
	else{
		Myntra.Search.InitNoResultsPage();
	}
	
	_gaq.push(['_trackEvent','search_page_visit', window.location.toString(), null, null, true]);
});

Myntra.Search.getFilterValues = function(str){
    return str.replace(/-/g, ":").replace(/~:/g, "~-").split(":");   
}


Myntra.Search.InitNoResultsPage = function(){
	//Tie GA Calls to Clicks on display categories in no results page
	$("#no_result a, .no_result_disp_categories a").live("click", function(){
		if($(this).attr('href') != "undefined"){
			_gaq.push(['_trackEvent', 'no_results_page', 'browse', $(this).attr('href')]);
		}
	});
	
}

Myntra.Search.InitInfiniteScroll = function() {

	var scrollTimerID;
	
	$(window).scroll(function() {
		if(scrollTimerID){
			clearTimeout(scrollTimerID);
			scrollTimerID=null;
		}
		scrollTimerID=setTimeout(function(){Myntra.Search.InfiniteScroll.timerToDetectScroll();}, 330);
		return false;
	});
	
	var filterScrollTimerID;
	var filterSectionTop=$(".search-page-content-left").offset().top;
 	var filterSectionHeight=$(".search-page-content-left").height();
	
	$(window).scroll(function() {
		if(filterScrollTimerID){
			clearTimeout(filterScrollTimerID);
			filterScrollTimerID=null;
		}
		filterScrollTimerID=setTimeout(function(){Myntra.Search.FilterScroller.timerToDetectScroll(filterSectionTop + filterSectionHeight);}, 100);
	});
	
	$(".go-to-top-btn").click(function(){
		Myntra.Search.FilterScroller.hideGoToTop();
		Myntra.Search.Utils.scrollToTop(0);
	});

	/*$("a").live("click", function(e) {
		if(!$(this).hasClass("more-products-link") && !$(this).hasClass("filter") && !$(this).hasClass("filter-box") && !$(this).hasClass("logout-link") && !$(this).hasClass("nextPage") && !$(this).hasClass("prevPage") && !$(this).hasClass("next") && !$(this).hasClass("prev") && !$(this).hasClass("quick-look") && !$(this).hasClass("quick-look-cart-btn") && !$(this).hasClass("btn-disabled") && !$(this).hasClass("pdp-action-btn")){
			e.preventDefault();
			onBeforeUnloadFired=true;
			filterClicked=true;
			Myntra.Search.Utils.setScrollOptionsInUrlHash(["scrollPage", "scrollPos"]);
			window.location=$(this).attr("href");
			return false;
		}
	});
	
	$("li").live("click", function (e) {
		if(!$(this).hasClass("filter") && !$(this).hasClass("filter-box") && !$(this).hasClass("size-filter-box")){
			e.preventDefault();
			onBeforeUnloadFired=true;
			filterClicked=true;
			Myntra.Search.Utils.setScrollOptionsInUrlHash(["scrollPage", "scrollPos"]);
		}
	});*/
	
	return true;
};

Myntra.Search.InfiniteScroll = (function(){
	var obj = {};
	
	var prevScrollPos=0,
		prevScrollPos=0,
		curScrollPos=0,
		infScrollCallInProgress=false,
		calltype="searchpagefilters";
	
	obj.pageSetToFirst=true;
	obj.pageSetFromHash=false;
	
	var productListingUlElem=$('.search-listing ul'),
		searchPageContentsElem=$("#search-page-contents"),
		moreProductsElem=$(".more-products-link"),
		moreProductsAjaxLoaderElem=$(".more-products-loading-indicator"),
		ajaxLoaderElem=$(".ajax-loading-msg"),
		productsCountElem=$(".products-count"),
		paginationLinksContainerElem=$(".pagination-links"),
		paginationInfoLinksAndSortbyElem=$(".pagination-info, .pagination-links, .sort-by-filters"),
		productsLeftCountElem=$(".products-left-count"),
		moreProductsSectionElem=$(".more-products-section"),
		productsTotalCountElem=$(".total-products-count");
	
	obj.timerToDetectScroll = function() {
		if($(window).scrollTop() >= ($(".search-listing ul").offset().top + $(".search-listing ul").height() - heightFromLast)){
	       	curScrollPos=$(window).scrollTop();
	    	if(curScrollPos-prevScrollPos > 0){
	    		if(!infScrollCallInProgress && !this.isLimitReached() && this.triggerAutoInfiniteScroll() && !batchProcess){
	    			this.ajaxSearchCall();
	    		}
	    	}
	    	prevScrollPos=curScrollPos;
	    }
		
		return false;
	}
	
	obj.triggerManualInfiniteScroll = function(){
		if(!infScrollCallInProgress && !this.isLimitReached()){
			this.ajaxSearchCall();
		}
	};
	
	obj.isLimitReached = function(){
		return (current_page > total_pages)?true:false;
	}
	
	obj.triggerAutoInfiniteScroll = function(){
		return (current_page<pageDefault+noOfAutoInfiniteScrolls)?true:false;
	}
	
	obj.ajaxSearchCall = function(){
		if(!this.pageSetToFirst){
			configArray["page"]=current_page+1;
			page_to_be_loaded = current_page+1;
		}
		else{
			page_to_be_loaded = 0;
		}
		if(!this.isNextPageOutOfLimit()){
			infScrollCallInProgress=true;
			this.pageSetToFirst=false;
			var querystr=JSON.stringify(configArray);
			this.showLoadingGraphic();
			Myntra.Search.Utils.ajaxCall.getData(calltype, http_loc + "/myntra/ajax_search.php?productui="+shownewui+"&showadditionaldetail="+show_additional_product_details, querystr, function(data){obj.updateSearchPageUI(data);});
		}
		return false;
	};
	
	obj.isNextPageOutOfLimit = function(){
		return (configArray["page"] + 1 > total_pages)?true:false;
	};

	obj.updateSearchPageUI = function(data){
		
		var result=$.parseJSON(data);
		
		this.updateProductCount(result);

		this.setPageCounters(result);
		
		this.updateNumberOfProductsCount(result);

		this.hideLoadingGraphic();
		
		if(current_page == 0 || (this.pageSetFromHash && onPageLoad)){
			this.replaceProductsHtml(result);
		}
		else{
			this.appendProductsHtml(result);
		}
		
		this.pageSetFromHash=false;
		
		this.showProductListing();
		
		if(filterClicked){
			Myntra.Search.Utils.scrollToTop(0);
			filterClicked=false;
		}
		
		Myntra.Search.Utils.TriggerLazyLoad();
		
		Myntra.Search.Utils.attachPDPRedirectCalls();
		
		//show the additional details for the styles.
		Myntra.Utils.showAdminStyleDetails();
		
		infScrollCallInProgress=false;
	
		onPageLoad=false;
		
		return false;
	};
	
	obj.replaceProductsHtml = function(result){
		productListingUlElem.html( $(result.html).find('li'));
	};
	
	obj.appendProductsHtml = function(result){
		productListingUlElem.append( $(result.html).find('li'));
	};
	
	obj.showLoadingGraphic = function(){
		if(page_to_be_loaded == 0){
			ajaxLoaderElem.show();
		}
		moreProductsElem.hide();
		moreProductsAjaxLoaderElem.show();
	};
	
	obj.hideLoadingGraphic = function(){
		if(page_to_be_loaded == 0){
			ajaxLoaderElem.hide();
		}
		moreProductsElem.show();
		moreProductsAjaxLoaderElem.hide();
	};
	
	obj.showProductListing = function(){
		searchPageContentsElem.css("visibility","visible");
	};
	
	obj.hideProductListing = function(){
		searchPageContentsElem.css("visibility","hidden");
	};
	
	obj.updateProductCount = function(result){
		if(result.product_count != null){
			paginationInfoLinksAndSortbyElem.show();
			productsCountElem.html(result.product_count + ((result.product_count == 1)?" Product":" Products"));
			if($(".full-search-message-block")[0]){ 
				$(".full-search-message-block:hidden").show();
			}
		}	
		else{
			paginationInfoLinksAndSortbyElem.hide();
			productsCountElem.html("No Products");
			if($(".full-search-message-block")[0]){ 
				$(".full-search-message-block:visible").hide();
			}
		}
	};
	
	obj.setPageCounters = function(result){
		current_page=parseInt(result.current_page);
		total_pages=parseInt(result.page_count);
	};
	
	obj.updateNumberOfProductsCount = function(result){
		var total_products=parseInt(result.product_count);
		//if(page_to_be_loaded == 0){}
		products_left_count = total_products - ((current_page+1)*default_noofitems);
		if(products_left_count > 0){
			moreProductsSectionElem.show();
			productsLeftCountElem.html(products_left_count);
			//productsTotalCountElem.html(result.product_count);
		}
		else{
			moreProductsSectionElem.hide();
		}
	};
	
	return obj;
})();

Myntra.Search.RecursiveLoader = (function(){
	var obj = {};
	obj.pagecount;
	obj.pagecount_temp;
	obj.scrollpos;
	obj.counter=0;
	obj.newConfigArray;

	var moreProductsSectionElem=$(".more-products-section"),
	productsTotalCountElem=$(".total-products-count"),
	productsLeftCountElem=$(".products-left-count");
	
	obj.initBatchRequestParams = function(newConfigArray){
		$(window).scrollTop(0);
		batchProcess=true;
		obj.newConfigArray=newConfigArray;
		obj.pagecount=scrollConfigArray["scrollPage"];
		obj.pagecount_temp=scrollConfigArray["scrollPage"];
		obj.scrollpos=scrollConfigArray["scrollPos"];
		obj.makeBatchRequest();
	}
	
	obj.makeBatchRequest = function(){
		/*if((obj.pagecount)>4){
			obj.setParamsForNextCall(obj.pagecount);
			obj.makeBatchAjaxRequest(true);
		}
		else{*/
			obj.setParamsForNextCall(obj.pagecount);
			if(obj.pagecount>=0){
				obj.makeBatchAjaxRequest(true);
			}
			else{
				obj.makeBatchAjaxRequest(false);
			}
		/*}*/
	}
	
	obj.makeBatchAjaxRequest = function(doNextCall){
		var querystr=JSON.stringify(obj.newConfigArray);
		Myntra.Search.Utils.ajaxCall.getData("test", http_loc + "/myntra/ajax_search.php?productui="+shownewui+"&showadditionaldetail="+show_additional_product_details, querystr, function(data){
			obj.updateSearchPageUI(data);
			obj.counter++;
			if(doNextCall){
				obj.makeBatchRequest();
			}
			else{
				obj.setOriginalConfig();
				batchProcess=false;
				filterClicked=false;
			}
		});
		
		return false; 
	}
	
	obj.setParamsForNextCall = function(pagecount){
		/*if((pagecount)>4){
			obj.pagecount=obj.pagecount-5;
			obj.newConfigArray["noofitems"]=5*24;
			obj.newConfigArray["page"]=obj.counter;
		}
		else{*/
			obj.pagecount=obj.pagecount-1;
			obj.newConfigArray["noofitems"]=24;
			obj.newConfigArray["page"]=scrollConfigArray["scrollPage"]-(obj.pagecount+1);
		/*}*/
	}
	
	obj.setOriginalConfig = function(){
		var next_page=parseInt(scrollConfigArray["scrollPage"]);
		if(next_page>total_pages){
			configArray["page"]=total_pages;
		}
		else{
			configArray["page"]=next_page;
		}
		if(total_pages-1 == next_page-1){
			moreProductsSectionElem.hide();
		}
		scrollConfigArray["scrollProcessed"]=true;
		Myntra.Search.Utils.scrollToTop(scrollConfigArray["scrollPos"]);
		Myntra.Search.InfiniteScroll.pageSetToFirst=false;
		Myntra.Search.InfiniteScroll.pageSetFromHash=false;
	}
	
	obj.updateNumberOfProductsCount = function(result){
		var total_products=parseInt(result.product_count);
		products_left_count = total_products - ((scrollConfigArray["scrollPage"]+1)*24);
		
		if(products_left_count > 0){
			moreProductsSectionElem.show();
			productsLeftCountElem.html(products_left_count);
		}
		else{
			moreProductsSectionElem.hide();
		}
	}
	
	obj.updateSearchPageUI = function(data){
		
		var result=$.parseJSON(data);
		
		Myntra.Search.InfiniteScroll.updateProductCount(result);

		Myntra.Search.InfiniteScroll.setPageCounters(result);
		
		obj.updateNumberOfProductsCount(result);

		if(obj.newConfigArray["page"] == 0){
			Myntra.Search.InfiniteScroll.replaceProductsHtml(result);
		}
		else{
			Myntra.Search.InfiniteScroll.appendProductsHtml(result);
		}
		
		Myntra.Search.InfiniteScroll.showProductListing();
		
		Myntra.Search.Utils.TriggerLazyLoad();
		
		Myntra.Search.Utils.attachPDPRedirectCalls();
		
		//show the additional details for the styles.
		Myntra.Utils.showAdminStyleDetails();
		
		infScrollCallInProgress=false;
		
		return false;
	}
	
	obj.replaceProductsHtml = function(result){
		productListingUlElem.html( $(result.html).find('li'));
	};
	
	obj.appendProductsHtml = function(result){
		productListingUlElem.append( $(result.html).find('li'));
	};
	
	obj.showLoadingGraphic = function(){
		if(page_to_be_loaded == 0){
			ajaxLoaderElem.show();
		}
		moreProductsElem.hide();
		moreProductsAjaxLoaderElem.show();
	};
	
	obj.hideLoadingGraphic = function(){
		if(page_to_be_loaded == 0){
			ajaxLoaderElem.hide();
		}
		moreProductsElem.show();
		moreProductsAjaxLoaderElem.hide();
	};
	
	obj.showProductListing = function(){
		searchPageContentsElem.css("visibility","visible");
	};
	
	obj.hideProductListing = function(){
		searchPageContentsElem.css("visibility","hidden");
	};	
		
	return obj;
})();

Myntra.Search.InitCommon = function(){

	$slider = $("#slider");//Caching slider object
	$amount = $("#amount");//Caching amount object
	
	if(typeof $slider != "undefined"){
	    $slider.slider({
	    	range: true, // necessary for creating a range slider
	        min: priceRangeMin, // minimum range of slider
			max: priceRangeMax, //maximimum range of slider
			values: [priceRangeMin, priceRangeMax], //initial range of slider
			slide: function(event, ui) { // This event is triggered on every mouse move during slide.
				$amount.html('Rs ' + ui.values[0] + ' - Rs ' + ui.values[1]);//set value of  amount span to current slider values
			},
			stop: function(event, ui){//This event is triggered when the user stops sliding.
					$(".price-filters li").removeClass("highlight");
					Myntra.Search.Utils.priceRange(ui.values[0], ui.values[1]);
	        }
		});
	    $amount.html('Rs ' + $slider.slider("values", 0) + ' - Rs ' + $slider.slider("values", 1));
	}
	
	//brands=Rockport/sizes=/gender=/pricerange=100-18099/noofitems=24/sortby=/page=1
	$(window).hashchange( function(event){
		if(!onBeforeUnloadFired){
			filterClicked=true;
			var hash_str=location.hash;
			for(prop in filterProperties){
				filterProperties[prop].setToDefault();
				filterProperties[prop].updateUI();
			}
			for(prop in scrollProperties){
				scrollProperties[prop].setToDefault();
				scrollProperties[prop].updateUI();
			}
			
			if(hash_str != "" && hash_str != "#!"){
				if(hash_str.indexOf("scrollPos") >= 0 && hash_str.indexOf("scrollPage") >= 0 && !scrollConfigArray["scrollProcessed"] && ajaxCallFunc=="InfiniteScroll"){
					var query_str=(location.hash).substring((location.hash).indexOf("#!")+2, (location.hash).length);
					var query_options=query_str.replace(/\//g, "|").replace(/~\|/g,"~/").split("|");
					
					var optionslength=query_options.length;
					for(var option=0;option<optionslength;option++){
						var token=query_options[option];
						var keyvalue=token.split("=");
						if(filterProperties[keyvalue[0]] !== undefined && typeof keyvalue[1] != "undefined" && keyvalue[1] != ""){
							filterProperties[keyvalue[0]].processURL(keyvalue[1]);
							filterProperties[keyvalue[0]].updateUI();					
						}
						if(scrollProperties[keyvalue[0]] !== undefined && typeof keyvalue[1] != "undefined" && keyvalue[1] != ""){
							scrollProperties[keyvalue[0]].processURL(keyvalue[1]);
							scrollProperties[keyvalue[0]].updateUI();					
						}
					}
					
					configArray["filteredByUser"] = true;
					var newConfigArray = $.extend(true, {}, configArray);
					Myntra.Search.RecursiveLoader.initBatchRequestParams(newConfigArray);
					scrollConfigArray["scrollProcessed"]=true;
				}
				else{
					var query_str=(location.hash).substring((location.hash).indexOf("#!")+2, (location.hash).length);
					var query_options=query_str.replace(/\//g, "|").replace(/~\|/g,"~/").split("|");
					
					var optionslength=query_options.length;
					for(var option=0;option<optionslength;option++){
						var token=query_options[option];
						var keyvalue=token.split("=");
						if(filterProperties[keyvalue[0]] !== undefined && typeof keyvalue[1] != "undefined" && keyvalue[1] != ""){
							filterProperties[keyvalue[0]].processURL(keyvalue[1]);
							filterProperties[keyvalue[0]].updateUI();					
						}
					}
					
					configArray["filteredByUser"] = true;
					Myntra.Search[ajaxCallFunc].ajaxSearchCall();
				}		
			}
			else{
				for(prop in filterProperties){
					filterProperties[prop].updateUI();
				}
				configArray["filteredByUser"] = true;
				Myntra.Search[ajaxCallFunc].ajaxSearchCall();
			}
			//onPageLoad=false;
		}
	});

	$(window).hashchange();
	
	Myntra.Search.Utils.TriggerLazyLoad();
	
	$(".filter-title-text").click(function(){
		if($(this).parent().find(".filter-block-minus").css("display") == "block"){
			$(this).parent().find(".filter-block-minus").trigger("click");
		}
		else{
			$(this).parent().find(".filter-block-plus").trigger("click");
		}
	});
	
	//Collapse individual filter section
	$(".filter-block-minus").click(function(){
		$(this).parent().parent().find(".filter-type-content").slideUp(50);
		$(this).parent().find(".filter-block-plus").show();
		$(this).hide();
	});
	
	//Expand individual filter section
	$(".filter-block-plus").click(function(){
		$(this).parent().parent().find(".filter-type-content").slideDown(50);
		$(this).parent().find(".filter-block-minus").show();
		$(this).hide();
	});
	
	//Collapse filters
	$(".div-block-minus").click(function(){
		$(this).parent().parent().find(".div-block-content").slideUp(50);
		$(this).parent().find(".div-block-plus").show();
		$(this).hide();
	});
	
	//Expand filters
	$(".div-block-plus").click(function(){
		$(this).parent().parent().find(".div-block-content").slideDown(50);
		$(this).parent().find(".div-block-minus").show();
		$(this).hide();
	});
	
	//Checkbox functionality in filters
	$(".cbx-filters .filter-box").click(function(){
		if($(this).hasClass("checked")){
			$(this).removeClass("checked").addClass("unchecked");
		}
		else if($(this).hasClass("unchecked")){
			$(this).removeClass("unchecked").addClass("checked");
		}
	});
	
	//click functionality for size filters
	$(".size-filters .filter").click(function(){
		if($(this).hasClass("selected")){
			$(this).removeClass("selected").addClass("unselected");
		}
		else if($(this).hasClass("unselected")){
			$(this).removeClass("unselected").addClass("selected");
		}
	});
	
	//click handler for size filters
	$(".size-filters li").click(function(){
		var size=$(this).attr("data-size");
		if($(this).find(".filter").hasClass("selected")){
			if($.inArray(size,configArray.sizes) == -1){
				configArray.sizes.push(size);
			}
		}
		else if($(this).find(".filter").hasClass("unselected")){
			if($.inArray(size,configArray.sizes) != -1){
				configArray.sizes = Myntra.Search.Utils.removeValueFromArray(size, configArray.sizes);
			}
		}
		Myntra.Search.Utils.setPageToFirst();
		Myntra.Search.Utils.setFilterOptionsInUrlHash(["sizes", "page"]);
		Myntra.Search.Utils.gaTrackingForAjaxFilters("sizes","size_filter");
	});
	
	//click handler for gender filters
	$(".gender-filters .filter-box").click(function(){
		var gender=$(this).attr("data-gender");
		if($(this).hasClass("checked")){
			if($.inArray(gender,configArray.gender) == -1){
				configArray.gender.push(gender);
			}
		}
		else{
			if($.inArray(gender,configArray.gender) != -1){
				configArray.gender = Myntra.Search.Utils.removeValueFromArray(gender, configArray.gender);
			}
		}
		Myntra.Search.Utils.setPageToFirst();
		Myntra.Search.Utils.setFilterOptionsInUrlHash(["gender", "page"]);
		Myntra.Search.Utils.gaTrackingForAjaxFilters("gender","gender_filter");
	});
	
	//click handler for brand filters 
	$(".brand-filters .filter-box").click(function(){
		var brand=$(this).attr("data-brand").replace(/-/g,"~-").replace(/\//g,"~/");
		if($(this).hasClass("checked")){
			if($.inArray(brand,configArray.brands) == -1){
				configArray.brands.push(brand);
			}
		}
		else{
			if($.inArray(brand,configArray.brands) != -1){
				configArray.brands = Myntra.Search.Utils.removeValueFromArray(brand, configArray.brands);
			}
		}
		Myntra.Search.Utils.setPageToFirst();
		Myntra.Search.Utils.setFilterOptionsInUrlHash(["brands", "page"]);
		Myntra.Search.Utils.gaTrackingForAjaxFilters("brands","brand_filter");
		
	});

	//click handler for price filters	
	$(".price-filters li.filter-box").click(function(){
		var minVal=$(this).attr("data-rangeMin");
		var maxVal=$(this).attr("data-rangeMax");
		if($(this).hasClass("highlight")){
			$(this).removeClass("highlight");
			Myntra.Search.Utils.priceRange(priceRangeMin, priceRangeMax);
		}
		else{
			$(".price-filters li.filter-box").removeClass("highlight");
			$(this).addClass("highlight");
			Myntra.Search.Utils.priceRange(minVal, maxVal);
			Myntra.Search.Utils.setPriceSliderToDefault();	
		}
	});

	//click handler for attribute filters
	$(".attrib-filters .filter-box").click(function(){
		var attrib_name=$(this).parent().find(".attrib-name").val();		
		var attrib_data=$(this).attr("data-ata").replace(/-/g,"~-").replace(/\//g,"~/");
		if($(this).hasClass("checked")){
			if($.inArray(attrib_data,configArray[attrib_name]) == -1){
				configArray[attrib_name].push(attrib_data);
			}
		}
		else{
			if($.inArray(attrib_data,configArray[attrib_name]) != -1){
				configArray[attrib_name] = Myntra.Search.Utils.removeValueFromArray(attrib_data, configArray[attrib_name]);
			}
		}
		Myntra.Search.Utils.setPageToFirst();
		Myntra.Search.Utils.setFilterOptionsInUrlHash([attrib_name,"page"]);
		Myntra.Search.Utils.gaTrackingForAjaxFilters(attrib_name,attrib_name);
	});
	
	return true;
};

Myntra.Search.InitPagination = function(){	
	return true;
};

Myntra.Search.Pagination = (function(){

	var obj = {};
	
	var calltype="searchpagefilters",	
		productListingUlElem=$('.search-listing ul'),
		ajaxLoaderElem=$(".ajax-loading-msg"),
		searchPageContentsElem=$("#search-page-contents"),
		productsCountElem=$(".products-count"),
		pageCountElem=$(".pagecount"),
		currentPageElem=$(".currentpage"),
		paginationLinksContainerElem=$(".pagination-links"),
		paginationInfoLinksAndSortbyElem=$(".pagination-info, .pagination-links, .sort-by-filters");

	obj.ajaxSearchCall = function(){
		var querystr=JSON.stringify(configArray);
		this.showLoadingGraphic();
		Myntra.Search.Utils.ajaxCall.getData(calltype, http_loc + "/myntra/ajax_search.php?productui="+shownewui+"&showadditionaldetail="+show_additional_product_details, querystr, function(data){obj.updateSearchPageUI(data);});
	}
	
	obj.selectNoOfItemsIndicator = function(){
		$(".noofitems-links").removeClass("selected");
		$(".link-" + configArray["noofitems"]).addClass("selected");
	}

	obj.updateSearchPageUI = function(data){

		var result=$.parseJSON(data);
		
		this.updateProductCount(result);
		
		this.selectNoOfItemsIndicator();

	    var currentPage=this.updatePageNumber(result);
		
		this.hideLoadingGraphic();
		
		this.changePaginationLinks(result.page_count,result.current_page);
		
		this.replaceProductsHtml(result);
		
		this.showProductListing();
		
		if(filterClicked){
			Myntra.Search.Utils.scrollToTop(0);
			filterClicked=false;
		}
		
		//attach IE fix for newly added items
		Myntra.Search.Utils.attachPDPRedirectCalls();
		
		//trigger lazy load call
		Myntra.Search.Utils.TriggerLazyLoad()
		
		//show the additional details for the styles.
		Myntra.Utils.showAdminStyleDetails();

		//to show page number in title and meta tag
		Myntra.Search.Utils.pageDrivenTitleMeta(currentPage, result.page_count);

		//show hide page description(show only for page 1)
		Myntra.Search.Utils.toggleDescription(currentPage);
		
	}
	
	obj.changePaginationLinks = function(pages, current_page){
		var str="";
		var prefix_str="";
		var ga_prefix_str="";
		if(configArray["sortby"] != ""){
			prefix_str=http_loc+"/"+query+"/"+configArray["sortby"];
			ga_prefix_str="/"+query+"/"+configArray["sortby"];
		}
		else{
			prefix_str=http_loc+"/"+query;
			ga_prefix_str="/"+query;
		}
		current_page=parseInt(current_page)+1;
		pages=parseInt(pages);
		var begin = current_page - 4;
		while(begin < 1){
			begin++;
		}
		var end = current_page + 4;
		while(end > pages){
		    end--;
		}    
		if(current_page > 1){    
			if(current_page > 1 && current_page <= end){
				str+="<a href='"+prefix_str+"/"+(current_page-1)+"' rel='"+ga_prefix_str+"/"+(current_page-1)+
					"' class='no-decoration-link pagination-prev' onclick='return Myntra.Search.Utils.setPagePrev(this);' rel='prev'>&lsaquo; Prev</a> ";
			}
		}
		for(i=begin;i<=end;i++){
			if(i==current_page){
				str+=" <span>"+(i)+"</span> ";
			}
			else{
				str+=" <a href='"+prefix_str+"/"+(i)+"' rel='"+ga_prefix_str+"/"+(i)+
					"' class='no-decoration-link pagination-only' dataid='"+i+"'>"+i+"</a> ";
			}
		}
		if(current_page < pages){
			if(current_page < end){
				str+=" <a href='"+prefix_str+"/"+(current_page+1)+"' rel='"+ga_prefix_str+"/"+(current_page+1)+
					"' class='no-decoration-link pagination-next' onclick='return Myntra.Search.Utils.setPageNext(this);' rel='next'>Next &rsaquo;</a>";
			}	
		}	
		
		paginationLinksContainerElem.html(str);
		
		$(".pagination-links a.pagination-only").each(function(){
			$(this).click(function(event){
				event.preventDefault();
				Myntra.Search.Utils.setPage($(this).attr("dataid")-1, $(this));
				return false;
			});
		});
		
		if(current_page == 0 && current_page == pages){
			$(".pagination-prev").addClass("hide");
			$(".pagination-next").addClass("hide");
		}
		else if(current_page == pages){
			$(".pagination-next").addClass("hide");
			$(".pagination-prev").removeClass("hide");
		}
		else if(current_page == 0 && pages > 0){
			$(".pagination-prev").addClass("hide");
			$(".pagination-next").removeClass("hide");
		}
		else{
			$(".pagination-next").removeClass("hide");
			$(".pagination-prev").removeClass("hide");
		}
	};

	
	obj.replaceProductsHtml = function(result){
		productListingUlElem.html( $(result.html).find('li'));
	} 
	
	obj.appendProductsHtml = function(result){
		productListingUlElem.append( $(result.html).find('li'));
	}
	
	obj.showLoadingGraphic = function(){
		ajaxLoaderElem.show();
	}
	
	obj.hideLoadingGraphic = function(){
		ajaxLoaderElem.hide();
	}
	
	obj.showProductListing = function(){
		searchPageContentsElem.css("visibility","visible");
	}
	
	obj.hideProductListing = function(){
		searchPageContentsElem.css("visibility","hidden");
	}
	
	obj.updateProductCount = function(result){
		if(result.product_count != null){
			paginationInfoLinksAndSortbyElem.show();
			productsCountElem.html(result.product_count + ((result.product_count == 1)?" Product":" Products"));
			if($(".full-search-message-block")[0]){ 
				$(".full-search-message-block:hidden").show();
			}
		}	
		else{
			paginationInfoLinksAndSortbyElem.hide();
			productsCountElem.html("No Products");
			if($(".full-search-message-block")[0]){ 
				$(".full-search-message-block:visible").hide();
			}
		}
	}
	
	obj.updatePageNumber = function(result){
		var currentPage = parseInt(result.current_page)+1;
		currentPageElem.html(currentPage);
		pageCountElem.html(result.page_count);
		return currentPage;
	}
	
	return obj;
})();

Myntra.Search.Utils.setPriceSlider = function(min, max){
	var hs=$("#slider");
	hs.slider('values', 0, min);
	hs.slider('values', 1, max);
	$("#amount").html('Rs ' + $slider.slider("values", 0) + ' - Rs ' + $slider.slider("values", 1));
};

Myntra.Search.Utils.setPriceSliderToDefault = function(){
	var hs=$("#slider");
	hs.slider('values', 0, priceRangeMin);
	hs.slider('values', 1, priceRangeMax);
	$("#amount").html('Rs ' + $slider.slider("values", 0) + ' - Rs ' + $slider.slider("values", 1));
};

//show page info in title and meta desc if pgage>1
Myntra.Search.Utils.pageDrivenTitleMeta = function(currentPage, totalPages){
    if(currentPage > 1){
        document.title = pageTitleNoPage+' | Page '+currentPage+'/'+totalPages;
        $('meta[name="description"]').attr('content',metaDescNoPage+' Page '+currentPage+' of '+totalPages);
    } else {
        document.title = pageTitleNoPage;
        $('meta[name="description"]').attr('content',metaDescNoPage);
    }
};

//show description for page 1 only
Myntra.Search.Utils.toggleDescription = function(currentPage){
    var descriptionObj = $('#search_description');
    if(descriptionObj.length){
        if(currentPage > 1){
            descriptionObj.hide();
        } else {
            descriptionObj.show();
        }
    }
};

Myntra.Search.Utils.gaTrackingForAjaxFilters = function(property, name){
	if(!trackingBooleans[property]){
		Myntra.GA.gaTrackEventAjax("search_page_filters",name,"");
		Myntra.Search.Utils.updateTrackingBooleans(property);		
	}
};

Myntra.Search.Utils.setAllTrackingBooleansToFalse = function(){
	for (var property in trackingBooleans) {
		trackingBooleans[property] = false;
	}
};

Myntra.Search.Utils.updateTrackingBooleans = function(property){
	if(!trackingBooleans[property]){
		trackingBooleans[property]=true;
	}	
};

Myntra.Search.Utils.triggerManualInfiniteScroll = function(){
	Myntra.Search.InfiniteScroll.triggerManualInfiniteScroll();
	return false;
}

Myntra.Search.Utils.ajaxCall = (function(){
	var obj = {};
	
	obj.callList = new Object;
	
	obj.getData = function(type, url, querystr, callback)
	{
		if( this.callList[type] != null )
		{
			this.callList[type].abort();
			this.callList[type] = null;
		}
		this.callList[type] = $.get( url, {"query" : querystr}, callback);
	}
	
	return obj;
})();


Myntra.Search.Utils.priceRange = function(minVal, maxVal){
	if(configArray.pricerange.rangeMin != minVal || configArray.pricerange.rangeMax != maxVal ){
		configArray.pricerange.rangeMin=minVal;
		configArray.pricerange.rangeMax=maxVal;
		Myntra.Search.Utils.setPageToFirst();
		Myntra.Search.Utils.setFilterOptionsInUrlHash(["pricerange", "page"]);
		Myntra.Search.Utils.gaTrackingForAjaxFilters("pricerange","price_filter");
	}
};

Myntra.Search.Utils.setSortBy = function(sortby, el){
	$(".sort-by-filters a").removeClass("sort-selected");
	$(el).addClass("sort-selected");
	configArray["sortby"]=sortby;
	Myntra.Search.Utils.setPageToFirst();
	Myntra.Search.Utils.setFilterOptionsInUrlHash(["sortby", "page"]);
	Myntra.GA.gaTrackPageViewAjax($(el).attr("rel"));
	return false;
};

Myntra.Search.Utils.setPage = function(page, el){
	configArray.page=page;
	Myntra.Search.Utils.setFilterOptionsInUrlHash(["page"]);
	Myntra.GA.gaTrackPageViewAjax($(el).attr("rel"));
	return false;
};

Myntra.Search.Utils.setPageToFirst = function(){
	Myntra.Search.InfiniteScroll.pageSetToFirst=true;
	configArray.page=0;
};

Myntra.Search.Utils.setPagePrev = function(el){
	var page_num=parseInt(configArray.page)-1;
	if(page_num >= 0 && page_num < total_pages){ 
		Myntra.Search.Utils.setPage(parseInt(configArray.page)-1, el);
	}
	return false;
};

Myntra.Search.Utils.setPageNext = function(el){
	var page_num=parseInt(configArray.page)+1;
	if(page_num >= 0 && page_num < total_pages){
		Myntra.Search.Utils.setPage(parseInt(configArray.page)+1, el);
	}	
	return false;
};

Myntra.Search.Utils.scrollToTop = function(target_top){
	Myntra.Search.FilterScroller.scrollInProgress=true;
	$('html, body').animate({scrollTop:target_top}, 800, function(){
		Myntra.Search.FilterScroller.scrollInProgress=false;
	});
}

Myntra.Search.Utils.removeValueFromArray = function(brand, array){
	var i = array.indexOf(brand);
	if(i != -1) array.splice(i, 1);
	return array;
}

Myntra.Search.Utils.TriggerLazyLoad = function(){
	$(".jquery-lazyload").lazyload({ 
	    placeholder : cdn_base + "/skin1/images/loader_180x240.gif",
	    effect : "fadeIn"
	});
	$(".jquery-lazyload").removeClass("jquery-lazyload");
};

Myntra.Search.Utils.attachPDPRedirectCalls = function(){
	$(".search-listing .item").click(function(e){
		if(!$(e.target).hasClass("quick-look") && !$(e.target).hasClass("quick-look-icon")){
		  filterClicked=true;
		  onBeforeUnloadFired=true;
		  var idx=$(".search-listing ul li").index($(this));
		  var page=configArray["page"];
		  var noofitems=configArray["noofitems"];
		  if(ajaxCallFunc=="Pagination"){
			  var position=(page*noofitems)+(idx+1);
		  }
		  else{
			  var position=idx+1;
		  }
		  Myntra.Search.Utils.setScrollOptionsInUrlHash(["scrollPage", "scrollPos"]);
		  var pdpUrl=$(this).find("a").attr("href");
		  _gaq.push(['_trackEvent', 'pdp_click', Myntra.Data.pageName, 'search_results']);
		  if(pdpUrl.indexOf("?") != -1){
			  window.location=pdpUrl + "&searchQuery=" + configArray["url"] + "&serp=" + position+"&uq="+configArray["uq"];
		  }
		  else{
			  window.location=pdpUrl + "?searchQuery=" + configArray["url"] + "&serp=" + position+"&uq="+configArray["uq"];
		  } 
		  return false;
		}
		
	});
};

Myntra.Search.Utils.setScrollOptionsInUrlHash = function(filter_types){
	
	var current_hash_str=location.hash;
	var current_query_str="";
	var query_hash=new Array();
	var query_obj=new Object();
	if(current_hash_str != ""){
		current_query_str=(current_hash_str).substring((current_hash_str).indexOf("#!")+2, (current_hash_str).length);
		query_hash=current_query_str.replace(/\//g, "|").split("|");
	}
	
	if(filter_types.length==0){
		query_hash=[];
	}
	else{
		for(var filteridx=0;filteridx<filter_types.length;filteridx++){
	
			var filter_type=filter_types[filteridx];
			var newstr="";
			var arr_idx=0;
			if(scrollProperties[filter_type] !== undefined ){
				newstr=scrollProperties[filter_type].getURLParam();
				scrollProperties[filter_type].resetParams();
			}
			
			for(var idx=0;idx<query_hash.length;idx++){
				var keyvalsplit=query_hash[idx].split("=");
				if(keyvalsplit[0] == filter_type){
					query_hash.splice(idx,1);
					arr_idx=idx;
				}
			}
					
			if(newstr != ""){
				if(arr_idx==0){
					query_hash.push(newstr);
				}
				else{
					query_hash.splice(arr_idx, 0, newstr);
				}	
			}	
		}
	}
	var query_hash_str="";
	query_hash_str=query_hash.join("|");
	
	//if(filter_types!='page' && filter_types!='sortby,page' && filter_types!='noofitems,page'){$('#avail-container').hide();}
	
	window.location.hash= "#!" + query_hash_str;
}

//set config array in one place and construct url every time
//brands=Nike/gender=Men/sizes=XL/pricerange=100-500/sortby=DISCOUNT/noofitems=24/page=1
Myntra.Search.Utils.setFilterOptionsInUrlHash = function(filter_types){

	var current_hash_str=location.hash;
	var current_query_str="";
	var query_hash=new Array();
	var query_obj=new Object();
	if(current_hash_str != ""){
		current_query_str=(current_hash_str).substring((current_hash_str).indexOf("#!")+2, (current_hash_str).length);
		query_hash=current_query_str.replace(/\//g, "|").replace(/~\|/g,"~/").split("|");
	}
	
	if(filter_types.length==0){
		query_hash=[];
	}
	else{
		for(var filteridx=0;filteridx<filter_types.length;filteridx++){
	
			var filter_type=filter_types[filteridx];
			var newstr="";
			var arr_idx=0;
			if(filterProperties[filter_type] !== undefined ){
				newstr=filterProperties[filter_type].getURLParam();
				filterProperties[filter_type].resetParams();
			}
			
			for(var idx=0;idx<query_hash.length;idx++){
				var keyvalsplit=query_hash[idx].split("=");
				if(keyvalsplit[0] == filter_type){
					query_hash.splice(idx,1);
					arr_idx=idx;
				}
			}
					
			if(newstr != ""){
				if(arr_idx==0){
					query_hash.push(newstr);
				}
				else{
					query_hash.splice(arr_idx, 0, newstr);
				}	
			}	
		}
	}
	var query_hash_str="";
	query_hash_str=query_hash.join("|");
	//if(filter_types!='page' && filter_types!='sortby,page' && filter_types!='noofitems,page'){$('#avail-container').hide();}
	
	window.location.hash= "#!" + query_hash_str;
}

Myntra.Search.Utils.selectNoOfItems = function(val, el){
    configArray.noofitems=val;
    Myntra.Search.Utils.setPageToFirst();
    Myntra.Search.Utils.setFilterOptionsInUrlHash(["noofitems", "page"]);
	Myntra.Search.Utils.gaTrackingForAjaxFilters("noofitems","no_of_items_filter");
	$(".noofitems-links").removeClass("selected");
	$(el).addClass("selected");
}

if (!Array.prototype.indexOf) {
	Array.prototype.indexOf = function(obj, start) {
	     for (var i = (start || 0), j = this.length; i < j; i++) {
	         if (this[i] === obj) { return i; }
	     }
	     return -1;
	}
}

Myntra.Search.FilterScroller = (function(){
	var obj = {};
	
	var currScrollPos=0,
		prevScrollPos=0,
		goToTopElem=$(".go-to-top-section");
	
	obj.scrollInProgress=false;
	
	obj.showGoToTop = function(){
		if(!this.scrollInProgress){
			goToTopElem.show();
			goToTopElem.css({
				"position":"fixed",
				"top":0
			});
		}
	}
	
	obj.hideGoToTop = function(){
		goToTopElem.removeAttr("style");
		goToTopElem.hide();
	}
	
	obj.endOfFilterScroll = function(){
		return $(".search-page-content-right").offset().top + $(".search-page-content-right").height() - 100;
	}
	
	obj.timerToDetectScroll = function(filterSectionOffset) {
		var scrollTopPosition=$(window).scrollTop();
		currScrollPos=scrollTopPosition;
		
		if(currScrollPos != prevScrollPos){
			if(currScrollPos > filterSectionOffset && currScrollPos < this.endOfFilterScroll()){
				this.showGoToTop();
			}
			else{
				this.hideGoToTop();
			}
			prevScrollPos=currScrollPos;
		}
	}
	
	return obj;
})();
