
Myntra.Utils.Cookie = {
    get: function(name) {
    	name = Myntra.Data.cookieprefix + name;
        var result = new RegExp('(?:^|; )' + encodeURIComponent(name) + '=([^;]*)').exec(document.cookie);
        return result ? result[1] : null;
    },

    set: function(name, value, days, path) {
    	name = Myntra.Data.cookieprefix + name;
        var cookie = encodeURIComponent(name) + '=' + encodeURIComponent(value);
        if (days) {
            var today = new Date();
            var expDate = new Date(today.getTime() + (days * 24 * 60 * 60 * 1000));
            cookie += '; expires=' + expDate.toGMTString();
        }
        if (!path) { path = '/'; }
        cookie += '; path=' + path;
        cookie += '; domain=' +  Myntra.Data.cookiedomain;
        document.cookie = cookie;
    },

    del: function(name) {
        this.set(name, '', -1);
    }
};

