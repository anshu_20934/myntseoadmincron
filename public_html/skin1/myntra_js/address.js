(function() {
	var cache = {};
	var inprogress = {};
	var success = function(data) {
		var loc = $('#locality').val();
        if (loc.length && $.inArray(loc, data.locality) === -1) {
            $('#locality').val('');
        }
		$('#locality').setOptions({
			data: data.locality
		});
		$('#city').val(data.city);
		$('#state_select').val(data.state);
		$('#city').removeClass('error').next().find('label').css('display', 'none');
		$('#state_select').removeClass('error').parent().next().find('label').css('display', 'none');
	};
    var fetch = function(pcode) {
        if (inprogress[pcode]) return;
        inprogress[pcode] = 1;
		$.ajax({
			url: "/myntra/s_ajax_pincode_locality.php?pincode=" + pcode,
			dataType: "json",
			beforeSend: function() {
				$('#locality').attr('disabled', 'disabled');
				$('#locality').addClass('ac_loading');
			},
			success: function(data) {
				cache[pcode] = data;
				success(data);
				$('#locality').trigger('showautocomplete');
			},
			complete: function(xhr, status) {
				$('#locality').removeAttr('disabled');
				$('#locality').removeClass('ac_loading');
                delete inprogress[pcode];
			}
		});
    };
	$('#locality').autocomplete([], {
		minChars: 0
	});
	$('#pincode').blur(function() {
		var pcode = $(this).val();
		if (!/\d{6}/.test(pcode)) {
			return
		}
        if ($(this).data('pcode') === pcode) {
            return;
        }
        $(this).data('pcode', pcode);
		if (pcode in cache) {
			success(cache[pcode]);
			$('#locality').trigger('showautocomplete');
			return;
		}
		$('#locality').setOptions({data:[]});
        fetch(pcode);
	});
	$('#locality').bind('result', function(e, data) {
		$('#selected-locality').val(data[0]);
	});
	$('#locality').focus(function(e) {
		var pcode = $('#pincode').val();
		if (pcode in cache) {
			$('#locality').trigger('showautocomplete');
		}
        else {
            $(this).setOptions({data:[]});
            fetch(pcode);
        }
	});
})()

function gaTracker(type) {
	switch (type) {
	case 'useaddress':
		_gaq.push(['_trackEvent', 'address', 'use_existing', '']);
		break;
	case 'createaddress':
		_gaq.push(['_trackEvent', 'address', 'create_new', '']);
		break;
	case 'validationerror':
		_gaq.push(['_trackEvent', 'address', 'create_new', 'validation_error']);
		break;
	case 'removeaddress':
		_gaq.push(['_trackEvent', 'address', 'remove', '']);
		break;
	default:
	}
}

function removeThisAddress() {
	if (confirm('Are you sure you want to remove this address')) {
		gaTracker("removeaddress");
		return true;
	}
	else {
		return false;
	}
}

var validateZipState = (typeof stateZipsJson == 'undefined' || stateZipsJson == null) ? false: true;

var clearZipErr = function() {
	$("#pincode").removeClass("error");
	$("#pincode").siblings(".status").html("").addClass("hide");
	$(".zip-err-warning-ship-addrs").addClass("hide");
	$("input[name=pinerr]").val("0");
	$(".save-btn").text("Save & Use this address");
};

var showZipErr = function(addressForm) {
	var countryVal = $("[name=country]", addressForm).val();
	var pincodeVal = $("[name=pincode]", addressForm).val();
	var stateText = (countryVal == 'IN') ? $("#state_select option:selected", addressForm).text() : $(":input.required[name=state]", addressForm).val();
	$("#pincode");
	$("#pincode").siblings(".status").html("Pincode Invalid for State").removeClass("hide");
	$(".zip-err-text-ship-addrs", addressForm).html("The pincode you've entered doesn't seem to belong to " + stateText + ". If the pincode is not correct, your delivery might get delayed.");
	$(".zip-err-warning-ship-addrs", addressForm).removeClass("hide").addClass("clearfix");
	$(".save-btn", addressForm).text("Save Anyways & Use this address");
};

$("[name=country]").change(clearZipErr);
$("#state_select").change(clearZipErr);
$("#pincode").change(clearZipErr);

function saveAndUseThisAddress() {
	if (!validateZipState) return true;
	var addressForm = $("#shipping_address");
	var pinErrVal = $("input[name=pinerr]", addressForm).val();

	if ($("[name=pincode]", addressForm).val().length <= 0) return;
	if ($("#state_select", addressForm).val().length <= 0) return;

	if (pinErrVal == "0") {
		var retval = isDataCorrect(addressForm);
		if (retval) {
			gaTracker("createaddress");
		}
		else {
			_gaq.push(['_trackEvent', 'address', 'address_save_failed', 'pincode_validation_error']);
		}
		return retval;
	}
	else if (pinErrVal == "1") {
		_gaq.push(['_trackEvent', 'address', 'create_new', 'pincode_override']);
		gaTracker("createaddress");
		return true;
	}
	$('#shipping_address').trigger('submit');
}

function isPincodeValidForState(addressForm) {
	var countryVal = $("[name=country]", addressForm).val();
	var pincodeVal = $("[name=pincode]", addressForm).val();

	var stateVal = (countryVal == 'IN') ? $("#state_select", addressForm).val() : $(":input.required[name=state]", addressForm).val();
	//only if country selected is india
	if (countryVal == 'IN' && stateVal != - 1) {
		var zips = stateZipsJson["IN-" + stateVal];
		var pass = false;
		for (index in zips) {
			var eachZip = zips[index];
			if (eachZip == pincodeVal.substring(0, eachZip.length)) {
				pass = true;
				return true;
			}
		}
		pass = false;
	}
	else { //if country is not india, then pass this validation anyways
		pass = true;
	}
	return pass;
}

function isDataCorrect(addressForm) {
	var pinErr = $("input[name=pinerr]", addressForm);
	var pass = isPincodeValidForState(addressForm);

	if (!pass) {
		showZipErr(addressForm);
		pinErr.val("1");
	}
	else {
		pinErr.val("0");
	}
	var finalpass = pass; //have more data validations in future: combine individual passes
	return finalpass;
}

function useThisAddress(id) {
	gaTracker("useaddress");
	$("#previous_address_id").val('').val(id);
	$("#previous_address").trigger('submit');
}
$.validator.addMethod('customEmailValidator', function(value) {
	return /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/.test(value);
},
'Please enter a valid email');

/*mobile regEx validation for non 0 prefix*/
$.validator.addMethod('regExMobile', function(value) {
	return /^[1-9]{1}[0-9]{9}$/.test(value);
},
"Please do not prefix with a '0' in mobile number");

$(document).ready(function() {
	var localityRequired = $('body').hasClass('mab-locality-yes');
	$("#shipping_address").validate({
		rules: {
			yourname: {
				required: function() {
					return $(".copy-as-acc-name") && ! $(".copy-as-acc-name").attr("checked");
				}
			},
			name: "required",
			address: "required",
			locality: {
				required: function() {
					return localityRequired
				}
			},
			city: "required",
			country: "required",
			state: "required",
			pincode: {
				required: true,
				digits: true,
				minlength: 6,
				maxlength: 6
			},
			email: {
				required: true,
				customEmailValidator: true
			},
			mobile: {
				required: true,
				digits: true,
				minlength: 10,
				regExMobile: true

			}
		},
		messages: {
			name: "This field is required.",
			yourname: "This field is required.",
			address: "This field is required.",
			locality: "This field is required.",
			city: "This field is required.",
			country: "This field is required.",
			state: "This field is required.",
			pincode: {
				required: "This field is required.",
				digits: "Please enter valid numbers."
			},
			email: {
				required: "This field is required.",
				customEmailValidator: "Please enter valid email."
			},
			mobile: {
				required: "This field is required.",
				digits: "Please enter only numbers for mobile.",
				minlength: "Please enter 10 digit mobile number."
			}
		},
		errorPlacement: function(error, element) {
			//The field: state, whether select or input:text is in a separate div called #state
			//its error-span is a sibling of its parent div
			if (element.attr('name') == 'state') error.appendTo(element.parent().next());
			else if (element.is(":radio")) error.appendTo(element.parent().next().next());
			else if (element.is(":checkbox")) error.appendTo(element.next());
			else if (element.is("input")) error.appendTo(element.next());
			else if (element.is("textarea")) error.appendTo(element.next());
			else error.appendTo(element.next());
		},
		submitHandler: function(form) {
			form.submit();
		}
	});

	$("#country").change(function() {
		var url = https_loc + "/mkshippingcalculate_re.php?country=" + $(this).val() + "&action=s_country";
		$.get(url, function(data) {
			$("#state").html(data.split(",")[0]);
		});
		if ($("#country").val() == "IN") {
			// Here I have to have the validations for the pincode
			$("#pincode").rules('add', {
				digits: true,
				minlength: 6,
				maxlength: 6
			});
		} else {
			$("#pincode").rules('remove', "digits");
			$("#pincode").rules('remove', "minlength");
			$("#pincode").rules('remove', "maxlength");
		}
	});

	$("#customer-name").validate({
		rules: {
			yourname: "required"
		},
		messages: {
			name: "This field is required.",
			yourname: "This field is required."
		},
		errorPlacement: function(error, element) {
			if (element.is(":radio")) error.appendTo(element.parent().next().next());
			else if (element.is(":checkbox")) error.appendTo(element.next());
			else error.appendTo(element.next());
		},

		submitHandler: function(form) {
			form.submit();
		}
	});

	$(".copy-as-acc-name").click(function() {
		var addAddressForm = $("#shipping_address");
		var showRecipientChkBx = $(".copy-as-acc-name", addAddressForm);
		var yourNameDiv = $(".your-name-div", addAddressForm);
		if (showRecipientChkBx.attr("checked") == false) {
			yourNameDiv.slideDown();
		} else {
			yourNameDiv.slideUp();
		}
	});

	$("#copy_default_address").click(function() {
		if ($("#copy_default_address").attr("checked")) {
			$("#name").val($("#d_name").html());
			$("#address").val($("#d_address").html());
			$("#city").val($("#d_city").html());
			$("#country").val($("#d_country").html());
			$("#state_select").val($("#d_state").html());
			$("#pincode").val($("#d_pincode").html());
			$("#mobile").val($("#d_mobile").html());
			$("#email").val($("#d_email").html());
		}
		else {
			$("#name").val('');
			$("#address").val('');
			$("#city").val('');
			$("#country").val("IN");
			$("#state_select").val('AN');
			$("#pincode").val('');
			$("#mobile").val('');
			$("#email").val('');
		}
	});
});

