var sizePositionObj = {	
	"adults-men":{
		"tshirts"      :{"collar":[20,47],"shoulder":[308,45],"chest":[20,151],"sleeve":[312,120],"waist":[20,279],"body length":[118,360]},
		"t shirts"     :{"collar":[20,47],"shoulder":[308,45],"chest":[20,151],"sleeve":[312,120],"waist":[20,279],"body length":[118,360]},
		"shirts"       :{"collar":[20,47],"shoulder":[308,45],"chest":[20,145],"sleeve":[312,156],"waist":[20,279],"body length":[118,360]},
		"casual shoes" :{"length":[150,365]},
		"sports shoes" :{"length":[150,365]},
		"sandals"      :{"length":[150,365]},
		"trousers"     :{"waist":[20,47],"rise":[20,110],"inseam length":[20,281],"outseam length":[312,155]},
		"formal shoes" :{"length":[150,365]},
		"kurtas"       :{"collar":[20,47],"shoulder":[308,45],"chest":[20,117],"sleeve":[312,156],"waist":[20,170],"body length":[118,360],"neck drop":[20,65],"arm hole":[312,120],"hips":[20,216]},//83//pending
		"slippers"     :{"length":[150,365],"width":[196,338]},
		"jeans"        :{"waist":[20,47],"rise":[20,110],"inseam length":[20,281],"outseam length":[312,155]},
		"shorts"       :{"waist":[20,106],"rise":[20,169],"inseam length":[20,238],"outseam length":[312,185]},
		"sweatshirts"  :{"collar":[20,47],"shoulder":[312,45],"chest":[20,156],"sleeve":[312,156],"waist":[20,279],"body length":[118,360]},
		"tops"         :{"collar":[20,47],"shoulder":[308,45],"chest":[20,145],"sleeve":[312,120],"waist":[20,227],"body length":[118,342]},
		"track pants"  :{"waist":[20,47],"rise":[20,110],"inseam length":[20,281],"outseam length":[312,155]},
		"sweaters"     :{"collar":[20,47],"shoulder":[312,45],"chest":[20,156],"sleeve":[312,156],"waist":[20,279],"body length":[118,360]},
		"sunglasses"   :'',
		"flip flops"   :{"length":[150,365],"width":[196,338]},
		"watches"      :'',
		"jackets"      :{"collar":[20,47],"shoulder":[312,45],"chest":[20,156],"sleeve":[312,156],"waist":[20,279],"body length":[118,360]},
		"kurtis"       :{"collar":[20,47],"shoulder":[308,45],"chest":[20,148],"sleeve":[312,156],"waist":[20,223],"body length":[118,360],"neck drop":[20,65],"arm hole":[312,120],"hips":[20,287]},//84
		"jerseys"      :{"collar":[20,47],"shoulder":[308,45],"chest":[20,151],"sleeve":[312,120],"waist":[20,279],"body length":[118,360]},
		"belts"        :{"belt length":[20,47],"belt width":[308,45],"to fit waist":[20,151]},
		"backpacks"    :'',
		"socks"        :'',
		"handbags"     :'',
		"capris"       :{"waist":[20,47],"rise":[20,110],"inseam length":[20,281],"outseam length":[312,155]},
		"cargos"       :{"waist":[20,47],"rise":[20,110],"inseam length":[20,281],"outseam length":[312,155]},
		"churidar"     :{"waist":[20,47],"rise":[20,110],"inseam length":[20,281],"outseam length":[312,155]},
		"lounge pants" :{"waist":[20,47],"rise":[20,110],"inseam length":[20,281],"outseam length":[312,155]},
		"pyjamas"      :{"waist":[20,47],"rise":[20,110],"inseam length":[20,281],"outseam length":[312,155]},
		"sweatpant"    :{"waist":[20,47],"rise":[20,110],"inseam length":[20,281],"outseam length":[312,155]},
		"swimwear"     :{"across shoulder":[0,0], "length":[0,0], "chest":[0,0], "sleeve length":[0,0], "to fit waist":[0,0], "inseam length":[0,0], "outseam length":[0,0]},
		"wallets"      :'',
		"caps"         :{"circumference":[150,365]},
		"hat"         :{"circumference":[150,365]},
		"bags"         :'',
		"tracksuits"   :'',
		"polo tshirts" :{"collar":[20,47],"shoulder":[308,45],"chest":[20,151],"sleeve":[312,120],"waist":[20,279],"body length":[118,360]},//160
		"hoodies"      :{"chest":[15,200],"sleeve length":[294,92]},
		"briefs"       :{"to fit waist":[3,112],"rise":[10,125],"inseam length":[20,281],"outseam length":[312,155],"hips":[10,149]},
		"boxers"       :{"to fit waist":[10,112],"rise":[10,125],"inseam length":[20,281],"outseam length":[312,155],"hips":[10,149]},
		"innerwear vests" :{"To fit Chest":[20,151],"body length":[20,281]},
		"innerwear t-shirt" :{"collar":[20,47],"shoulder":[308,45],"chest":[8,151],"sleeve":[312,120],"waist":[20,279],"body length":[118,360]},
		"blazers"	:{"collar":[20,47],"shoulder":[308,24],"chest":[8,152],"sleeve length":[312,160],"waist":[8,228],"body length":[118,369],"cuff":[118,400]}
	},
	"adults-women":{
		"tshirts"      :{"collar":[20,47],"shoulder":[308,45],"bust":[20,151],"sleeve":[312,120],"waist":[20,279],"body length":[118,360]},
		"t shirts"     :{"collar":[20,47],"shoulder":[308,45],"bust":[20,151],"sleeve":[312,120],"waist":[20,279],"body length":[118,360]},
		"shirts"       :{"collar":[20,47],"shoulder":[308,45],"bust":[20,145],"sleeve":[312,156],"waist":[20,279],"body length":[118,360]},
		"casual shoes" :{"length":[150,365]},
		"sports shoes" :{"length":[150,365]},
		"sandals"      :{"length":[150,365]},
		"swimwear"	   :{"to fit bust":[0,0], "to fit waist":[0,0], "to fit hip":[0,0]},
		"tops"         :{"collar":[20,47],"shoulder":[308,45],"bust":[20,145],"sleeve":[312,120],"waist":[20,227],"body length":[118,342]},
		"trousers"     :{"waist":[20,47],"rise":[20,110],"inseam length":[20,281],"outseam length":[312,155]},
		"formal shoes" :{"length":[150,365]},
		"kurtas"       :{"collar":[20,47],"shoulder":[308,45],"bust":[20,117],"sleeve":[312,156],"waist":[20,164],"body length":[118,360],"neck drop":[20,65],"arm hole":[312,120],"hips":[20,216]},//83//pending
		"slippers"     :{"length":[150,365],"width":[196,338]},
		"jeans"        :{"waist":[20,47],"rise":[20,110],"inseam length":[20,281],"outseam length":[312,155]},
		"shorts"       :{"waist":[20,106],"rise":[20,169],"inseam length":[20,238],"outseam length":[312,185]},
		"sweatshirts"  :{"collar":[20,47],"shoulder":[312,45],"bust":[20,156],"sleeve":[312,156],"waist":[20,279],"body length":[118,360]},
		"track pants"  :{"waist":[20,47],"rise":[20,110],"inseam length":[20,281],"outseam length":[312,155]},
		"sweaters"     :{"collar":[20,47],"shoulder":[312,45],"bust":[20,156],"sleeve":[312,156],"waist":[20,279],"body length":[118,360]},
		"sunglasses"   :'',
		"flip flops"   :{"length":[150,365],"width":[196,338]},
		"watches"      :'',
		"innerwear vests" :'',
		"saree blouse" :{"across shoulder":[0,0], "length":[0,0], "bust":[0,0]},
		"jackets"      :{"collar":[20,47],"shoulder":[312,45],"bust":[20,156],"sleeve":[312,156],"waist":[20,279],"body length":[118,360]},
		"kurtis"       :{"collar":[20,47],"shoulder":[308,45],"bust":[20,148],"sleeve":[312,156],"waist":[20,223],"body length":[118,360],"neck drop":[20,65],"arm hole":[312,120],"hips":[20,287]},//84
		"jerseys"      :{"collar":[20,47],"shoulder":[308,45],"bust":[20,151],"sleeve":[312,120],"waist":[20,279],"body length":[118,360]},
		"belts"        :{"belt length":[20,47],"belt width":[308,45],"to fit waist":[20,151]},
		"backpacks"    :'',
		"socks"        :'',
		"handbags"     :'',
		"dresses"      :{"collar":[20,47],"shoulder":[308,45],"bust":[20,112],"sleeve":[312,156],"waist":[20,164],"body length":[118,360],"neck drop":[20,65],"arm hole":[312,120],"hips":[20,212]},//79
		"capris"       :{"waist":[20,47],"rise":[20,110],"inseam length":[20,281],"outseam length":[312,155]},
		"churidar"     :{"waist":[20,47],"rise":[20,110],"inseam length":[20,281],"outseam length":[312,155]},
		"leggings"     :{"waist":[20,47],"rise":[20,110],"inseam length":[20,281],"outseam length":[312,155]},
		"pyjamas"      :{"waist":[20,47],"rise":[20,110],"inseam length":[20,281],"outseam length":[312,155]},
		"tunics"       :{"collar":[20,47],"shoulder":[308,45],"bust":[20,112],"sleeve":[312,156],"waist":[20,164],"body length":[118,360],"neck drop":[20,65],"arm hole":[312,120],"hips":[20,212]},//91
		"wallets"      :'',
		"skirts"       :{"waist":[20,106],"outseam length":[312,185]},
		"hat"         :{"circumference":[150,365]},
		"caps"         :{"circumference":[150,365]},
		"bags"         :{"circumference":[150,365]},
		"tracksuits"   :'',
		"polo tshirts" :{"collar":[20,47],"shoulder":[308,45],"bust":[20,151],"sleeve":[312,120],"waist":[20,279],"body length":[118,360]},
		"hoodies"      :{"chest":[15,200],"sleeve length":[294,92]},
		"tights"       :{"waist":[20,47],"rise":[20,110],"inseam length":[20,281],"outseam length":[312,155]},
		"sweatpant"    :{"waist":[20,47],"rise":[20,110],"inseam length":[20,281],"outseam length":[312,155]},
		"camisoles"    :{"collar":[20,47],"shoulder":[308,45],"bust":[20,145],"sleeve":[312,120],"waist":[20,227],"body length":[118,342]},
		"bra"          :{"overbust":[10,154],"underbust":[310,203]},
		"briefs"       :{"waist":[10,113],"rise":[10,125],"inseam length":[20,281],"outseam length":[312,155],"hips":[310,155]},
		"panties"      :{"waist":[10,113],"rise":[10,125],"inseam length":[20,281],"outseam length":[312,155],"hips":[310,155]},
		"lingerie set" :{"overbust":[24,136],"underbust":[280,150],"waist":[10,100],"rise":[10,125],"hips":[300,151]}
	},
	
	"adults-unisex":{
		"tshirts"      :{"collar":[20,47],"shoulder":[308,45],"chest":[20,151],"sleeve":[312,120],"waist":[20,279],"body length":[118,360]},//90
		"t shirts"     :{"collar":[20,47],"shoulder":[308,45],"chest":[20,151],"sleeve":[312,120],"waist":[20,279],"body length":[118,360]},//90
		"shirts"       :{"collar":[20,47],"shoulder":[308,45],"chest":[20,145],"sleeve":[312,156],"waist":[20,279],"body length":[118,360]},//85
		"casual shoes" :{"length":[150,365]},
		"sports shoes" :{"length":[150,365]},
		"sandals"      :{"length":[150,365]},
		"tops"         :{"collar":[20,47],"shoulder":[308,45],"chest":[20,145],"sleeve":[312,120],"waist":[20,227],"body length":[118,360]},//89//pending
		"trousers"     :{"waist":[20,47],"rise":[20,110],"inseam":[20,281],"outseam":[312,155]},
		"formal shoes" :{"length":[150,365]},
		"kurtas"       :{"collar":[20,47],"shoulder":[308,45],"chest":[20,117],"sleeve":[312,156],"waist":[20,212],"body length":[118,360],"neck drop":[20,65],"arm hole":[312,120],"hips":[20,216]},//83//pending
		"slippers"     :{"length":[150,365],"width":[196,338]},
		"jeans"        :{"waist":[20,47],"rise":[20,110],"inseam":[20,281],"outseam":[312,155]},
		"shorts"       :{"waist":[20,106],"rise":[20,169],"inseam":[20,238],"outseam":[312,185]},
		"sweatshirts"  :{"collar":[20,47],"shoulder":[312,45],"chest":[20,156],"sleeve":[312,156],"waist":[20,279],"body length":[118,360]},
		"track pants"  :{"waist":[20,47],"rise":[20,110],"inseam length":[20,281],"outseam length":[312,155]},
		"briefs"       :'',
		"sweaters"     :{"collar":[20,47],"shoulder":[312,45],"chest":[20,156],"sleeve":[312,156],"waist":[20,279],"body length":[118,360]},
		"sunglasses"   :'',
		"flip flops"   :{"length":[150,365],"width":[196,338]},
		"watches"      :'',
		"innerwear vests" :'',
		"jackets"      :{"collar":[20,47],"shoulder":[312,45],"chest":[20,156],"sleeve":[312,156],"waist":[20,279],"body length":[118,360]},
		"kurtis"       :{"collar":[20,47],"shoulder":[308,45],"chest":[20,148],"sleeve":[312,156],"waist":[20,223],"body length":[118,360],"neck drop":[20,65],"arm hole":[312,120],"hips":[20,287]},//84
		"jerseys"      :{"collar":[20,47],"shoulder":[308,45],"chest":[20,151],"sleeve":[312,120],"waist":[20,279],"body length":[118,360]},
		"belts"        :{"belt length":[20,47],"belt width":[308,45],"to fit waist":[20,151]},
		"backpacks"    :'',
		"socks"        :'',
		"swimwear"     :{"across shoulder":[0,0], "length":[0,0], "chest":[0,0], "sleeve length":[0,0], "to fit waist":[0,0], "inseam length":[0,0], "outseam length":[0,0]},
		"handbags"     :'',
		"dresses"      :{"collar":[20,47],"shoulder":[308,45],"chest":[20,156],"sleeve":[312,156],"waist":[20,220],"body length":[118,360],"neck drop":[20,65],"arm hole":[312,120],"hips":[20,280]},//79
		"capris"       :'',
		"tunics"       :{"collar":[20,47],"shoulder":[308,45],"chest":[20,156],"sleeve":[312,156],"waist":[20,220],"body length":[118,360],"neck drop":[20,65],"arm hole":[312,120],"hips":[20,280]},//91
		"wallets"      :'',
		"skirts"       :'',
		"caps"         :{"circumference":[150,365]},
		"hat"         :{"circumference":[150,365]},
		"bags"         :'',
		"tracksuits"   :'',
		"bra"          :'',
		"polo tshirts" :{"collar":[20,47],"shoulder":[308,45],"chest":[20,151],"sleeve":[312,120],"waist":[20,279],"body length":[118,360]},//160
		"hoodies"      :{"chest":[15,200],"sleeve length":[294,92]}
	},
	
	"kids-boys":{
		"tshirts"      :{"collar":[20,47],"shoulder":[308,45],"chest":[20,151],"sleeve":[312,120],"waist":[20,279],"body length":[118,360]},//90
		"t shirts"     :{"collar":[20,47],"shoulder":[308,45],"chest":[20,151],"sleeve":[312,120],"waist":[20,279],"body length":[118,360]},//90
		"shirts"      :{"collar":[20,47],"shoulder":[308,45],"chest":[20,151],"sleeve":[312,120],"waist":[20,279],"body length":[118,360]},
		"casual shoes" :{"length":[150,365]},
		"sports shoes" :{"length":[150,365]},
		"sandals"      :{"length":[150,365]},
		"tops"         :{"collar":[20,47],"shoulder":[308,45],"chest":[20,156],"sleeve":[312,156],"waist":[20,279],"body length":[118,360]},//89//pending
		"trousers"     :{"waist":[20,47],"rise":[20,110],"inseam":[20,281],"outseam":[312,155]},
		"formal shoes" :{"length":[150,365]},
		"kurtas"       :{"chest":[20,136],"shoulder width":[232,39],"waist":[20,242],"sleeve":[300,132],"body length":[118,330]},//83//pending
		"slippers"     :{"length":[150,365],"width":[196,338]},
		"jeans"        :{"waist":[20,47],"rise":[20,110],"inseam":[20,281],"outseam":[312,155]},
		"shorts"       :{"waist":[20,106],"rise":[20,169],"inseam":[20,238],"outseam":[312,185]},
		"sweatshirts"  :{"collar":[20,47],"shoulder":[312,45],"chest":[20,156],"sleeve":[312,156],"waist":[20,279],"body length":[118,360]},//87
		"track pants"  :{"waist":[20,47],"rise":[20,110],"inseam":[20,281],"outseam":[312,155]},
		"briefs"       :'',
		"sweaters"     :{"collar":[20,47],"shoulder":[312,45],"chest":[20,156],"sleeve":[312,156],"waist":[20,279],"body length":[118,360]},//88
		"sunglasses"   :'',
		"flip flops"   :{"length":[150,365],"width":[196,338]},
		"watches"      :'',
		"innerwear vests" :'',
		"swimwear"     :{"across shoulder":[0,0], "length":[0,0], "chest":[0,0], "sleeve length":[0,0], "to fit waist":[0,0], "inseam length":[0,0], "outseam length":[0,0]},
		"jackets"      :{"collar":[20,47],"shoulder":[312,45],"chest":[20,156],"sleeve":[312,156],"waist":[20,279],"body length":[118,360]},//82
		"kurtis"       :{"chest":[20,136],"shoulder width":[232,39],"waist":[20,242],"sleeve":[300,132],"body length":[118,330]},//84
		"jerseys"      :{"collar":[20,47],"shoulder":[308,45],"chest":[20,151],"sleeve":[312,120],"waist":[20,279],"body length":[118,360]},
		"belts"        :{"belt length":[20,47],"belt width":[308,45],"to fit waist":[20,151]},
		"backpacks"    :'',
		"socks"        :'',
		"handbags"     :'',
		"dresses"      :{"chest":[20,136],"shoulder width":[232,39],"waist":[20,242],"sleeve":[300,132],"body length":[118,330]},//79
		"capris"       :'',
		"tunics"       :{"chest":[20,136],"shoulder width":[232,39],"waist":[20,242],"sleeve":[300,132],"body length":[118,330]},//91
		"wallets"      :'',
		"skirts"       :'',
		"caps"         :{"circumference":[150,365]},
		"hat"         :{"circumference":[150,365]},
		"bags"         :'',
		"tracksuits"   :'',
		"bra"          :'',
		"polo tshirts" :{"collar":[20,47],"shoulder":[308,45],"chest":[20,151],"sleeve":[312,120],"waist":[20,279],"body length":[118,360]},
		"hoodies"      :{"chest":[15,200],"sleeve length":[294,92]},
		"bodysuit"     : {"across shoulder":[0,0], "length":[0,0], "chest":[0,0], "waist":[0,0], "sleeve length":[0,0]},
		"sleepsuit"     : {"across shoulder":[0,0], "length":[0,0], "chest":[0,0], "waist":[0,0], "sleeve length":[0,0]}

	},
	
	"kids-girls":{
		"tshirts"      :{"collar":[20,47],"shoulder":[308,45],"chest":[20,151],"sleeve":[312,120],"waist":[20,279],"body length":[118,360]},//90
		"t shirts"     :{"collar":[20,47],"shoulder":[308,45],"chest":[20,151],"sleeve":[312,120],"waist":[20,279],"body length":[118,360]},//90
	    "shirts"      :{"collar":[20,47],"shoulder":[308,45],"chest":[20,151],"sleeve":[312,120],"waist":[20,279],"body length":[118,360]},
		"casual shoes" :{"length":[150,365]},
		"sports shoes" :{"length":[150,365]},
		"sandals"      :{"length":[150,365]},
		"swimwear"	   :{"to fit bust":[0,0], "to fit waist":[0,0], "to fit hip":[0,0]},
		"tops"         :{"collar":[20,47],"shoulder":[308,45],"chest":[20,145],"sleeve":[312,120],"waist":[20,227],"body length":[118,342]},
		"trousers"     :{"waist":[20,47],"rise":[20,110],"inseam":[20,281],"outseam":[312,155]},
		"formal shoes" :{"length":[150,365]},
		"kurtas"       :{"chest":[20,136],"shoulder width":[232,39],"waist":[20,242],"sleeve":[300,132],"body length":[118,330]},//83//pending
		"slippers"     :{"length":[150,365],"width":[196,338]},
		"jeans"        :{"waist":[20,47],"rise":[20,110],"inseam":[20,281],"outseam":[312,155]},
		"shorts"       :{"waist":[20,106],"rise":[20,169],"inseam":[20,238],"outseam":[312,185]},
		"sweatshirts"  :{"collar":[20,47],"shoulder":[312,45],"chest":[20,156],"sleeve":[312,156],"waist":[20,279],"body length":[118,360]},//87
		"track pants"  :{"waist":[20,47],"rise":[20,110],"inseam":[20,281],"outseam":[312,155]},
		"briefs"       :'',
		"saree blouse" :{"across shoulder":[0,0], "length":[0,0], "bust":[0,0]},
		"sweaters"     :{"collar":[20,47],"shoulder":[312,45],"chest":[20,156],"sleeve":[312,156],"waist":[20,279],"body length":[118,360]},//88
		"sunglasses"   :'',
		"flip flops"   :{"length":[150,365],"width":[196,338]},
		"watches"      :'',
		"innerwear vests" :'',
		"jackets"      :{"collar":[20,47],"shoulder":[312,45],"chest":[20,156],"sleeve":[312,156],"waist":[20,279],"body length":[118,360]},//82
		"kurtis"       :{"chest":[20,136],"shoulder width":[232,39],"waist":[20,242],"sleeve":[300,132],"body length":[118,330]},//84
		"jerseys"      :{"collar":[20,47],"shoulder":[308,45],"chest":[20,151],"sleeve":[312,120],"waist":[20,279],"body length":[118,360]},//125
		"belts"        :{"belt length":[20,47],"belt width":[308,45],"to fit waist":[20,151]},
		"backpacks"    :'',
		"socks"        :'',
		"handbags"     :'',
		"dresses"      :{"collar":[20,47],"shoulder":[308,45],"chest":[20,112],"sleeve":[312,156],"waist":[20,164],"body length":[118,360],"neck drop":[20,65],"arm hole":[312,120],"hips":[20,212]},
		"capris"       :'',
		"tunics"       :{"chest":[20,136],"shoulder width":[232,39],"waist":[20,242],"sleeve":[300,132],"body length":[118,330]},//91
		"wallets"      :'',
		"skirts"       :'',
		"caps"         :{"circumference":[150,365]},
		"hat"         :{"circumference":[150,365]},
		"bags"         :'',
		"tracksuits"   :'',
		"bodysuit"     : {"across shoulder":[0,0], "length":[0,0], "chest":[0,0], "waist":[0,0], "sleeve length":[0,0]},
		"bra"          :'',
		"polo tshirts" :{"collar":[20,47],"shoulder":[308,45],"chest":[20,151],"sleeve":[312,120],"waist":[20,279],"body length":[118,360]},
		"hoodies"      :{"chest":[15,200],"sleeve length":[294,92]},
		"sleepsuit"     : {"across shoulder":[0,0], "length":[0,0], "chest":[0,0], "waist":[0,0], "sleeve length":[0,0]}
	},

	"kids-unisex":{
		"tshirts"      :{"collar":[20,47],"shoulder":[308,45],"chest":[20,151],"sleeve":[312,120],"waist":[20,279],"body length":[118,360]},//90
		"t shirts"     :{"collar":[20,47],"shoulder":[308,45],"chest":[20,151],"sleeve":[312,120],"waist":[20,279],"body length":[118,360]},//90
		"shirts"       :{"collar":[20,47],"shoulder":[308,45],"chest":[20,145],"sleeve":[312,156],"waist":[20,279],"body length":[118,360]},//85
		"casual shoes" :{"length":[150,365]},
		"sports shoes" :{"length":[150,365]},
		"sandals"      :{"length":[150,365]},
		"tops"         :{"collar":[20,47],"shoulder":[308,45],"chest":[20,156],"sleeve":[312,156],"waist":[20,279],"body length":[118,360]},//89//pending
		"trousers"     :{"waist":[20,47],"rise":[20,110],"inseam":[20,281],"outseam":[312,155]},
		"formal shoes" :{"length":[150,365]},
		"kurtas"       :{"chest":[20,136],"shoulder width":[232,39],"waist":[20,242],"sleeve":[300,132],"body length":[118,330]},//83//pending
		"slippers"     :{"length":[150,365],"width":[196,338]},
		"jeans"        :{"waist":[20,47],"rise":[20,110],"inseam":[20,281],"outseam":[312,155]},
		"shorts"       :{"waist":[20,106],"rise":[20,169],"inseam":[20,238],"outseam":[312,185]},
		"sweatshirts"  :{"collar":[20,47],"shoulder":[312,45],"chest":[20,156],"sleeve":[312,156],"waist":[20,279],"body length":[118,360]},//87
		"track pants"  :{"waist":[20,47],"rise":[20,110],"inseam":[20,281],"outseam":[312,155]},
		"swimwear"     :{"across shoulder":[0,0], "length":[0,0], "chest":[0,0], "sleeve length":[0,0], "to fit waist":[0,0], "inseam length":[0,0], "outseam length":[0,0]},
		"briefs"       :'',
		"sweaters"     :{"collar":[20,47],"shoulder":[312,45],"chest":[20,156],"sleeve":[312,156],"waist":[20,279],"body length":[118,360]},//88
		"sunglasses"   :'',
		"flip flops"   :{"length":[150,365],"width":[196,338]},
		"watches"      :'',
		"innerwear vests" :'',
		"jackets"      :{"collar":[20,47],"shoulder":[312,45],"chest":[20,156],"sleeve":[312,156],"waist":[20,279],"body length":[118,360]},//82
		"kurtis"       :{"chest":[20,136],"shoulder width":[232,39],"waist":[20,242],"sleeve":[300,132],"body length":[118,330]},//84
		"jerseys"      :{"collar":[20,47],"shoulder":[308,45],"chest":[20,151],"sleeve":[312,120],"waist":[20,279],"body length":[118,360]},
		"belts"        :{"belt length":[20,47],"belt width":[308,45],"to fit waist":[20,151]},
		"backpacks"    :'',
		"socks"        :'',
		"handbags"     :'',
		"dresses"      :{"chest":[20,136],"shoulder width":[232,39],"waist":[20,242],"sleeve":[300,132],"body length":[118,330]},//79
		"capris"       :'',
		"tunics"       :{"chest":[20,136],"shoulder width":[232,39],"waist":[20,242],"sleeve":[300,132],"body length":[118,330]},//91
		"wallets"      :{"circumference":[150,365]},
		"skirts"       :'',
		"caps"         :{"circumference":[150,365]},
		"hat"         :{"circumference":[150,365]},
		"bags"         :'',
		"tracksuits"   :'',
		"bra"          :'',
		"polo tshirts" :{"collar":[20,47],"shoulder":[308,45],"chest":[20,151],"sleeve":[312,120],"waist":[20,279],"body length":[118,360]},//160
		"hoodies"      :{"chest":[15,200],"sleeve length":[294,92]}
	},
	
	"na":{
	
}
	
	
	
};
