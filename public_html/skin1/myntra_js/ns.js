// global namespace
var Myntra = Myntra || {};
Myntra.Utils = Myntra.Utils || {};
Myntra.Search = Myntra.Search || {};
Myntra.Search.Utils = Myntra.Search.Utils || {};
Myntra.MiniPDP = Myntra.MiniPDP || {};
Myntra.PDP = Myntra.PDP || {};
Myntra.PDP.Utils = Myntra.PDP.Utils || {};
