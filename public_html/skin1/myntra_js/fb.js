
function validateFacebookSignupForm(){
	var hasError=false;
	passwordVal = $("#fbfooterpasswd1").val();
	if(passwordVal == '') {
		$("#fbfooterpasswdError").html("This field is required");
		$("#fbfooterpasswd1").focus();
		hasError = true;
	}
	else if(passwordVal.length < 6) {
		$("#fbfooterpasswdError").html("Minimum 6 characters required.");
		$("#fbfooterpasswd1").focus();
		hasError = true;
	}
	
	return hasError;
	
}
function validateFacebookSignupFormRegisterPage(){
	var hasError=false;
	passwordVal = $("#fbfooterpasswdregister1").val();
	if(passwordVal == '') {
		$("#fbfooterpasswdregisterError").html("This field is required");
		$("#fbfooterpasswdregister1").focus();
		hasError = true;
	}
	else if(passwordVal.length < 6) {
		$("#fbfooterpasswdregisterError").html("Minimum 6 characters required.");
		$("#fbfooterpasswdregister1").focus();
		hasError = true;
	}

	return hasError;

}

function facebookFooterCompleteRegistration(validate,redirect){
	error =false;
	if(validate=='true')
		error = validateFacebookSignupForm();
	if(!error){
		tb_remove();
		$.post(http_loc+"/myntra/ajax_facebook_login.php", { fbRegistration:"1", passwd1: $("#fbfooterpasswd1").val()},
			function(data){
				var responseObj=data;
				var status=responseObj.status;
				$(".signinw .loading-overlay").removeClass("show").addClass("hide");
				if(status=="success"){
					$(".login-form").hide();
					if(!$(".signup").hasClass("corners-bl-br")){
						$(".signup").addClass("corners-bl-br");
					}
					var user=responseObj.username;
					var fbuid=responseObj.fb_uid;
					var toDisplay=responseObj.toDisplay;
					var cashbackon=responseObj.cashback;
					if(cashbackon == "on")	{
						$(".cashbackblock").show();
						$(".noncashback").hide();
					}				
					var refAndEarnAmount=responseObj.refAmount;
					fbPostLoginExperience(user,fbuid,toDisplay,refAndEarnAmount);
					if(redirect=='true') document.location = http_loc + "/register.php";
				}else{
					var usernameErrorElem=$(".username-error");
					var usernameInput=$("#username");
					usernameErrorElem.html("Connect with Facebook failed.").slideDown();
					usernameInput.addClass("error-input");
				}		
			}, "json");
	}
}
function facebookFooterCompleteRegistrationRegisterPage(validate,redirect){
	error =false;
	if(validate=='true')
		error = validateFacebookSignupFormRegisterPage();
	if(!error){
		tb_remove();
        var fb_mrp_referrer=$.trim($("#mrp_referrer_footer").val());
		$.post(http_loc+"/myntra/ajax_facebook_login.php", { fbRegistration:"1", passwd1: $("#fbfooterpasswdregister1").val(),mrp_referrer:fb_mrp_referrer},
			function(data){
				var responseObj=data;
				var status=responseObj.status;
				$(".signinw .loading-overlay").removeClass("show").addClass("hide");
				if(status=="success"){
					$(".login-form").hide();
					if(!$(".signup").hasClass("corners-bl-br")){
						$(".signup").addClass("corners-bl-br");
					}
					var user=responseObj.username;
					var fbuid=responseObj.fb_uid;
					var toDisplay=responseObj.toDisplay;
					var cashbackon=responseObj.cashback;
					if(cashbackon == "on")	{
						$(".cashbackblock").show();
						$(".noncashback").hide();
					}
					var refAndEarnAmount=responseObj.refAmount;
					fbPostLoginExperience(user,fbuid,toDisplay,refAndEarnAmount);
					if(redirect=='true') document.location = http_loc + "/register.php";
				}else{
					var usernameErrorElem=$(".username-error");
					var usernameInput=$("#username");
					usernameErrorElem.html("Connect with Facebook failed.").slideDown();
					usernameInput.addClass("error-input");
				}
			}, "json");
	}
}

