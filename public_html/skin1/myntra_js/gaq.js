
$(document).ready(function() {
    // After successfull login/signup, the page is refreshed.
    // So, cookie is used to trigger the *after* events like GA tracking and showing msg.
    window._gaq = window._gaq || [];
    var ckLogin = Myntra.Utils.Cookie.get('_mklogin');
    if (ckLogin) {
        var parts = ckLogin.split('*');
        Myntra.Utils.Cookie.del('_mklogin');
        _gaq.push(['_trackEvent', parts[0], parts[1], parts[2]]);
        if (parts[1] === 'signup' && location.protocol === 'http:') {
            var msg = [
            '<p>You have successfully created a new Myntra account.</p>',
            '<p>You have <strong class="price">Rs. '+ Myntra.Data.nonFBRegCouponValue +'</strong> worth of Mynt Credits that you can redeem when you checkout.</p>'
            ].join('');
            Myntra.MsgBox.show('Congratulations!', msg, {autohide:5000});
        }
    }
});

