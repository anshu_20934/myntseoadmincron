
Myntra.MkBase = function() {
    var obj = {}, 
        props = {},
        _data = {};
    
    obj.set = function(key, val) {
        props[key] = val;
    };
    
    obj.get = function(key) {
        return props[key];
    };
    
    obj.data = function(key, val) {
        if (typeof key === 'object') {
            $.extend(true, _data, key);
            return obj;
        }
        if (typeof key === 'undefined') {
            return _data;
        }
        if (typeof val === 'undefined') {
            return _data[key];
        }
        _data[key] = val;
    };

    obj.showLoading = function(){};
    obj.hideLoading = function(){};

    obj.onAjaxError = function(resp, name) {
    };
    
    obj.onAjaxSuccess = function(resp, name) {};
    
    obj.ajax = function(params, name, method) {
        method = method || "GET";
        $.ajax({
            type: method,
            url: obj.get("ajax_url"),
            data: params,
            dataType: "json",
            beforeSend:function() {
                obj.showLoading();
            },
            success: function(resp) {
                if (!resp) {
                    alert('Invalid JSON data returned from the Server');
                    return;
                }
                (resp.status === 'SUCCESS') ? obj.onAjaxSuccess(resp, name) : obj.onAjaxError(resp, name);
            },
            complete: function(xhr, status) {
                obj.hideLoading();
            }
        });
    };

    return obj;
}

