Myntra.LightBox = function(id, cfg) {
    cfg = cfg || {};

    // if a template is used in a popup and in a page
    cfg.isPopup = (typeof cfg.isPopup === 'undefined') ? true : cfg.isPopup;
    cfg.isModal = (typeof cfg.isModal === 'undefined') ? false : cfg.isModal;
    cfg.disableCloseButton = (typeof cfg.disableCloseButton === 'undefined') ? false : cfg.isModal;

    // move the dialog into immediate child of the body.
    if (cfg.isPopup && !$(id).parent().is('body')) {
        $('body').append($(id));
    }

    var obj = Myntra.MkBase(),
        panel = $(id),
        mod = $(id + ' .mod'),
        close = $(id + ' .close'),
        progress = null,
        shim = null,
        body = $(id + ' .mod .bd');

    if (!shim) {
        if(!$("#"+ panel.attr('id')+ "-shim").length){
            shim = $('<div class="lightbox-shim" id="' + panel.attr('id') + '-shim"></div>').appendTo('body');
        }
        else{
            shim=$("#"+ panel.attr('id')+ "-shim");
            shim.show();
        }
    }

    if (cfg.isPopup) {
        if (!close.length && !cfg.disableCloseButton) {
            mod.prepend('<div class="close"></div>');
            close = $(id + ' .close');
            close.click(function(e) {
                obj.hide();
            });
        }
        obj.center = function() {
            var dy = Math.round(($(window).height() - mod.outerHeight(false)) / 2);
            if (dy < 0) { dy = 0; }
            mod.css('margin-top', dy + 'px');
        };
        $(window).resize(function() {
            obj.center();
        });
    }
    else {
        obj.center = function(){};
    }

    if (!cfg.isModal) {
        panel.click(function(e) {
            $(e.target).is(panel) && obj.hide();
        });
    }

    var escHandler = function(e) {
        (27 === e.keyCode) && obj.hide();
    };

    obj.showLoading = function() {
        if (!$(id + ' .loading').length) {
            mod.append('<div class="loading"></div>');
        }
        if (!progress) {
            progress = $(id + ' .loading');
        }
        obj.showLoading = function() {
            progress.show();
        }
        progress.show();
    };

    obj.hideLoading = function() {
        progress && progress.hide();
    };

    obj.beforeShow = function() {
        if (typeof cfg.beforeShow === 'function') {
            cfg.beforeShow.call(obj);
            delete cfg.beforeShow;
        }
        else if (cfg.evtPrefix) {
            $(document).trigger(cfg.evtPrefix + '.beforeShow');
        }
    };

    obj.afterShow = function() {
        if (typeof cfg.afterShow === 'function') {
            cfg.afterShow.call(obj);
            delete cfg.afterShow;
        }
        else if (cfg.evtPrefix) {
            $(document).trigger(cfg.evtPrefix + '.afterShow');
        }
    };

    obj.show = function(a_cfg) {
        $.extend(cfg, a_cfg);
        obj.beforeShow();
        panel.fadeIn();
        this.center();
        shim.show();
        if (!cfg.isModal) {
            $(document).bind('keydown', escHandler);
        }
        obj.afterShow();
    };

    obj.beforeHide = function() {
        if (typeof cfg.beforeHide === 'function') {
            cfg.beforeHide.call(obj);
            delete cfg.beforeHide;
        }
        else if (cfg.evtPrefix) {
            $(document).trigger(cfg.evtPrefix + '.beforeHide');
        }
    };

    obj.afterHide = function() {
        if (typeof cfg.afterHide === 'function') {
            cfg.afterHide.call(obj);
            delete cfg.afterHide;
        }
        else if (cfg.evtPrefix) {
            $(document).trigger(cfg.evtPrefix + '.afterHide');
        }
    };

    obj.hide = function() {
        obj.beforeHide();
        if (progress) { progress.hide(); }
        panel.fadeOut();
        shim.hide();
        if (!cfg.isModal) {
            $(document).unbind('keydown', escHandler);
        }
        obj.afterHide();
    };

    obj.tmpShow = function() {
        panel.show();
        shim.show();
        this.center();
    };

    obj.tmpHide = function() {
        panel.hide();
        shim.hide();
    };

    obj.clearPopupContent = function(){
        body.html('');
    }

    return obj;
};
