$(document).ready(function(){
    /* Slideshow */
    if($("#slideshow").length){
    	$("#slideshow").cycle({
    		pager: '#nav',
    		fx:    'fade',
    		speed: 250,
    		pause: 1
    	});
    }
	// homepage load images only on the click of next button
	$(".nextPage").click(function(){
		var listname = $(this).attr("id");
		var items_to_be_loaded=$(".items_"+listname+" li.item a.to-be-loaded");
		// to load the next four items in the list onclick of the next button in
		// the slider
		if(items_to_be_loaded.length){
			for(i=0;i<items_to_be_loaded.length;i++){
				$("img", items_to_be_loaded[i]).attr("src", lists_image_srcs[listname][$("img", items_to_be_loaded[i]).attr("id")]);
				$(items_to_be_loaded[i]).removeClass("to-be-loaded").addClass("loaded");
				if(i==3){break;}
			}
		}
		// alternative way to load rest of the product item images in the list
		// $.each($(".items_"+listname+" li.item a.to-be-loaded"),function(){
			// $("img",this).attr("src", lists_image_srcs[listname][$("img",
			// this).attr("id")]);
		// });
	});
	
	// Call for brand logos scroller
	if($(".brand-logos-scrollable").length){
		$(".brand-logos-scrollable").scrollable({size:8,keyboard:false,clickable:false});
	}
	
/*

	// homepage load images only on the click of next button
	$(".nextPage").click(function(){
		var listname = $(this).attr("id");
		var items_to_be_loaded=$(".items_"+listname+" li.item a.to-be-loaded");
		// to load the next four items in the list onclick of the next button in
		// the slider
		if(items_to_be_loaded.length){
			for(i=0;i<items_to_be_loaded.length;i++){
				$("img", items_to_be_loaded[i]).attr("src", lists_image_srcs[listname][$("img", items_to_be_loaded[i]).attr("id")]);
				$(items_to_be_loaded[i]).removeClass("to-be-loaded").addClass("loaded");
				if(i==3){break;}
			}
		}
		// alternative way to load rest of the product item images in the list
		// $.each($(".items_"+listname+" li.item a.to-be-loaded"),function(){
			// $("img",this).attr("src", lists_image_srcs[listname][$("img",
			// this).attr("id")]);
		// });
	});
	
	// Call for brand logos scroller
	if($(".brand-logos-scrollable").length){
		$(".brand-logos-scrollable").scrollable({size:8,keyboard:false,clickable:false});
	}
    */
});
