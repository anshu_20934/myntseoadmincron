var numericReg  =  /(^-?\d\d*\.\d*$)|(^-?\d\d*$)|(^-?\.\d\d*$)/;
var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
var alphaReg = /^[a-zA-Z][a-zA-Z ]*$/;

/* My Myntra Page JS - Change Password */
$(".change-password-link").live('click', function() {
	_gaq.push(['_trackEvent', 'my_profile', 'change_password', 'click']);
	$(".change-password").toggle();
	$(".notification.error").hide();
});

$(".cancel-change-password").live('click', function() {
	clearChangePasswordErrorFields();
	$(".change-password").hide();
});

$(".old-password").live('blur', function() {
	var oldPasswordVal = $(".old-password").val();
	if (oldPasswordVal == "") {
		$(".old-password-error").html("Enter a valid Password").slideDown();
	}
	else {
		$(".old-password-error").html("").slideUp();
	}
});

$(".new-password").live('blur', function() {
	var newPasswordVal = $(".new-password").val();
	if (newPasswordVal == "") {
		$(".new-password-error").html("Enter a valid Password").slideDown();
	}
	else if (newPasswordVal.length < 6) {
		$(".new-password-error").html("Minimum 6 characters required.").slideDown();
	}
	else {
		$(".new-password-error").html("").slideUp();
	}
});

$(".confirm-new-password").live('blur', function() {
	var newPasswordVal = $(".new-password").val();
	var confirmnewPasswordVal = $(".confirm-new-password").val();
	if (newPasswordVal == "" || newPasswordVal.length < 6 || newPasswordVal != confirmnewPasswordVal) {
		$(".confirm-new-password-error").html("Passwords must match.").slideDown();
	}
	else {
		$(".confirm-new-password-error").html("").slideUp();
	}
});

$(".save-change-password").live('click', function() {
	var hasError = validateChangePasswordForm();
	if (!hasError) {
		form = $("#change-password-form");
		form.submit();
	}
});

function validateChangePasswordForm() {
	var oldPasswordVal = $(".old-password").val();
	var newPasswordVal = $(".new-password").val();
	var confirmnewPasswordVal = $(".confirm-new-password").val();
	var error = false;

	clearChangePasswordErrorFields();

	if (oldPasswordVal == "") {
		error = "true";
		$(".old-password-error").html("Enter a valid password.").slideDown();
	}

	if (newPasswordVal == "") {
		error = "true";
		$(".new-password-error").html("Enter a valid password.").slideDown();
	}

	if (newPasswordVal.length < 6) {
		error = "true";
		$(".new-password-error").html("Minimum 6 characters required.").slideDown();
	}

	if (confirmnewPasswordVal == "") {
		error = "true";
		$(".confirm-new-password-error").html("Enter a valid password.").slideDown();
	}

	if (confirmnewPasswordVal != newPasswordVal) {
		error = "true";
		$(".confirm-new-password-error").html("Passwords must match.").slideDown();
	}

	return error;

}

function clearChangePasswordErrorFields() {
	$(".old-password-error").html("").slideUp();
	$(".new-password-error").html("").slideUp();
	$(".confirm-new-password-error").html("").slideUp();
}

/* My Myntra Page JS - Change Profile */
$(".edit-profile-link").live('click', function() {
	_gaq.push(['_trackEvent', 'my_profile', 'edit_profile', 'click']);
	var changeProfileForm = $("#change-profile");
	$(".mobile-lbl").hide();
	$(".empty-mobile-lbl").hide();
	$(".name-lbl").hide();
	$(".status-lbl").hide();
	$(".edit-profile-link").hide();
	$(".verify-mobile-code").hide();

	$(".mobile-code-entry").hide();
	$(".profile-name").show();
	$(".profile-mobile").show();
	$("#save_cancel_button_container").show();
	$(".save-profile").show();
	$(".cancel-save-profile").show();

	try {
		$('.profile-name').focus();
	} catch(ex) {}
});

function verifyMobileCommon(id) {
	$(".edit-profile-link").hide();
	$(".unverified-mobile-lbl").hide();
	$(".status-lbl").hide();
	var profileForm = $("#change-profile");
	var mobile = $("#mobile", profileForm).val();
	var login = $("#login", profileForm).val();
	$.ajax({
		type: "POST",
		url: "mymyntra.php",
		data: "_token=" + Myntra.Data.token + "&mode=generate-code&mobile=" + $("#mobile", profileForm).val() + "&login=" + $("#login", profileForm).val(),
		success: function(msg) {
			msg = $.trim(msg);
			if (msg == "showCaptcha") {
				// if(count > 0) {
				$("#captcha").attr("src", http_loc + '/captcha/captcha.php?id=' + id + '&rand=' + Math.random());
				// }
				$("#captcha-form").val("");
				$(".captcha-block").show();

				$("#verify-mobile").hide();
				$(".change-password-link").hide();
				count++;
			} else if (msg == "verifyFailed") {
				$("#verify-mobile").hide();
				$(".verify-mobile-manually").show();
				$(".edit-profile-link").show();
			}
			else {

				$(".verify-mobile-code").show();
				$(".mobile-code-entry").show();
				$("#mobile-message").html("Please enter the verification code that was sent to " + mobile);
				$("#mobile-message").show();
			}

		}
	});
}

$(".verify-mobile-profile-page").live('click', function() {
	verifyMobileCommon("myProfilePage");
});

$(".verify-mobile-cart-page").live('click', function() {
	verifyMobileCommon("cartPage");
});

var counter = 0;
var status = '';
var regcounter = 0;
var resendcounter = 0;
var email_list = '';
var email_ctr = 0;
var email_list_arr = new Array();
var email_list_arr_ctr = 0;
var referralDefault = "Please enter email addresses separated by commas";
$(".invite-friends").live('click', function() {
	counter = 0;
	status = '';
	regcounter = 0;
	resendcounter = 0;
	email_list = '';
	email_ctr = 0;
	email_list_arr = new Array();
	email_list_arr_ctr = 0;
	if ($.trim($("#new_fullname").val()) == '') {
		showUsernameMissingWindow();
		return;
	}
	var inviteForm = $("#invite-form");
	var recipients = $("#recipient_list").val();
	if ($.trim(recipients) == '' || $.trim(recipients) == referralDefault) {
		alert("Please enter email ids");
		return;
	}
	$("#recipient_list").val("");
	var recipientsArr = recipients.split(/[\n\r;,]+/);
	for (i in recipientsArr) {
		recipient = recipientsArr[i].replace(/^\s+|\s+$/g, "");
		//Value += perInviteProgress;
		// empty.
		if (recipient == "") {
			//$("#progressbar").progressbar({ value: Value });
			continue;
		}
		var email;
		if (recipient.search("<") == - 1) {
			// only email present.
			email = recipient;
		}
		else {
			// name and email present.
			email = recipient.substring(recipient.search("<") + 1, recipient.search(">"));
		}
		email_list += email + ',';
		if (email_ctr % 100 == 0 && email_ctr != 0) {
			email_list_arr[email_list_arr_ctr] = email_list;
			email_list = '';
			email_list_arr_ctr++;
		}
		email_ctr++;
	}
	if (email_list != '') email_list_arr[email_list_arr_ctr] = email_list;
	$(".ui-progressbar-value").animate({
		width: "1%"
	});
	$("#progressbar").show();
	var l = 1;
	var offset = Math.ceil(email_ctr / 100);
	var p_offset = 100 / offset;
	sendEmailInBatches(email_list_arr, p_offset, l);
});
function sendEmailInBatches(email_list_arr, p_offset, l) {
	$.ajax({
		type: "POST",
		url: "mymyntra.php",
		data: "&mode=invite-multiple-friends&email_list=" + email_list_arr[l - 1] + '&_token=' + $('#invite-form input[name="_token"]').val(),
		async: false,
		success: function(data) {
			var msg = jQuery.parseJSON(data);
			counter = counter + msg.sent;
			status = msg.status;
			regcounter = regcounter + msg.already_registered;
			resendcounter = resendcounter + msg.already_sent;

			var prog_status = l * p_offset;
			//alert(prog_status);
			//console.log(prog_status+"==="+p_offset+"====="+l);
			l++;
			var html_status_msg = Math.ceil(prog_status);
			if (html_status_msg > 100) html_status_msg = 100;
			$("#p_status").html(html_status_msg);
			$(".ui-progressbar-value").animate({
				width: prog_status + "%"
			},
			'slow', function() {
				if (l <= email_list_arr.length) {
					sendEmailInBatches(email_list_arr, p_offset, l);
				}
				else {
					var msg = "Successfully sent " + counter + " invitation(s)!";
					_gaq.push(['_trackEvent', 'my_referrals', 'send_invites', 'success', counter]);

					if (regcounter > 0) {
						msg += "\n" + regcounter + " of the above contact(s) have already registered.";
						_gaq.push(['_trackEvent', 'my_referrals', 'send_invites', 'failure_user_registered', regcounter]);
					}

					if (resendcounter > 0) {
						msg += "\n" + resendcounter + " of the above contact(s) were sent an email earlier today.";
						_gaq.push(['_trackEvent', 'my_referrals', 'send_invites', 'failure_user_referred_today', resendcounter]);

					}
					alert(msg);
					$("#progressbar").hide();

					if (counter > 0) {
						// refresh the myrefferals page
						$.ajax({
							type: "POST",
							url: http_loc + "/mymyntra.php",
							data: "mode=myreferrals",
							success: function(msg) {
								var data = msg.split("#####");
								$("#" + data[1]).html(data[0]);
								if (data[1] == 'myreferrals_tab') {
									// When page loads...
									$(".reff-content").hide(); // Hide all content
									$("ul.right-mytabs li:first").addClass("active"); // Activate
									// first
									// tab
									$(".reff-content:first").show(); // Show first tab
									// content
									toggle_text_all(".toggle-text");
								}
							}
						});
					}
				}
			});
		}
	});
}

$(".cancel-submit-code").live('click', function() {
	$(".edit-profile-link").show();
	$(".status-lbl").show();
	$(".unverified-mobile-lbl").show();
	$(".verify-mobile-btn").show();
	$(".verify-mobile-code").hide();
	$(".mobile-code-entry").hide();
});

$(".submit-code").live('click', function() {
	var profileForm = $("#verify-mobile-code");
	var codeStr = $("#mobile-code", profileForm).val();
	if (codeStr != "") {
		profileForm.submit();
	}
});

$(".save-profile").live('click', function() {
	var profileForm = $("#change-profile");
	$(".profile-mobile-error", profileForm).html("").slideUp();
	var error = validateProfileFields(profileForm);
	if (!error) {
		profileForm.submit();
	}
});

$(".cancel-save-profile").live('click', function() {
	$(".profile-mobile").hide();
	$(".profile-name").hide();
	$(".profile-mobile-error").hide();
	$(".profile-name-error").hide();
	$("#save_cancel_button_container").hide();

	$(".save-profile").hide();
	$(".cancel-save-profile").hide();

	$(".verify-mobile-code").hide();
	$(".mobile-code-entry").hide();
	$(".mobile-lbl").show();
	$(".empty-mobile-lbl").show();
	$(".name-lbl").show();
	$(".status-lbl").show();
	$(".edit-profile-link").show();

});

function validateProfileFields(profileForm) {
	var error = false;
	var mobileVal = $(".profile-mobile", profileForm).val();
	var nameVal = $.trim($(".profile-name", profileForm).val());
	if (mobileVal == "" || mobileVal.length < 10 || ! numericReg.test(mobileVal)) {
		error = true;
		$(".profile-mobile-error", profileForm).text("Enter a valid mobile number.").slideDown();
	}
	if (nameVal == "") {
		error = true;
		$(".profile-name-error", profileForm).text("This field is required.").slideDown();
	}

	if (!alphaReg.test(nameVal)) {
		error = true;
		$(".profile-name-error", profileForm).text("Only alphabets are allowed").slideDown();
	}
	return error;

}

/* My Myntra Page JS - Edit Address */
$(".edit-address-link").live('click', function() {
	_gaq.push(['_trackEvent', 'my_profile', 'edit_address', 'click']);
	var addAddressForm = $("#add-address");
	clearAddressErrorFields(addAddressForm);
	$(".add-address").hide();
	var editAddressForm = $("#edit-address");
	clearAddressErrorFields(editAddressForm);
	var addressListing = $(this).parent().parent().parent().parent();
	prePopulateEditFormFields(editAddressForm, addressListing);
	$(".edit-address").toggle();
	var loc_href = location.href.split("#");
	var o_loc_href = loc_href[0];
	window.location.href = o_loc_href + "#add_new_shipping_address";
	$(document).trigger('myntra.editaddress.loaded');
});
$(".cancel-edit-address").live('click', function() {
	var editAddressForm = $("#edit-address");
	clearAddressErrorFields(editAddressForm);
	$(".edit-address").hide();
});

/* My Myntra Page JS - Delete Address */
$(".delete-address-link").live('click', function() {
	_gaq.push(['_trackEvent', 'my_profile', 'delete_address', 'click']);
	var editAddressForm = $("#delete-address");
	var parent = $(this).parent();
	var id = $("#id", parent).val();
	var r = confirm("Are you sure you want to delete this address");
	if (r == true) {
		if (!id || id != '') {
			form = $("#delete-address");
			$("#id", form).val(id);
			form.submit();
		}
	}
});

/* My Myntra Page JS - Set Default Address */
$(".set-default-address").live('click', function() {
	_gaq.push(['_trackEvent', 'my_profile', 'set_default_address', 'click']);
	var editAddressForm = $("#set-default-address");
	var parent = $(this).parent();
	var id = $("#id", parent).val();
	if (!id || id != '') {
		form = $("#set-default-address");
		$("#id", form).val(id);
		form.submit();
	}
});

/* My Myntra Page JS - Add Address */
$(".add-address-link").live('click', function() {
	_gaq.push(['_trackEvent', 'my_profile', 'add_address', 'click']);
	var editAddressForm = $("#edit-address");
	clearAddressErrorFields(editAddressForm);
	var addAddressForm = $("#add-address");
	clearAddressErrorFields(addAddressForm);
	prePopulateAddFormFields(addAddressForm);
	$(".edit-address").hide();
	$(".add-address").toggle();
});

$(".copy-as-acc-name").live('click', function() {
	var addAddressForm = $("#add-address");
	var showRecipientChkBx = $(".copy-as-acc-name", addAddressForm);
	var yourNameDiv = $(".your-name-div", addAddressForm);
	if (showRecipientChkBx.attr("checked") == false) {
		yourNameDiv.slideDown();
	} else {
		yourNameDiv.slideUp();
	}
});

function prePopulateAddFormFields(addAddressForm) {
	$(".country-select option[value='-1']", addAddressForm).attr("selected", "selected");
	$(".state-select option[value='-1']", addAddressForm).attr("selected", "selected");
	$(".state-text").val("");
	$(".state-text").hide();
	$(".state-select").show();

}

$(".cancel-add-address").live('click', function() {
	var addAddressForm = $("#add-address");
	clearAddressErrorFields(addAddressForm);
	$(".add-address").hide();
});

/* My Myntra Page JS */
$(".country-select").change(function() {
	var countryVal = $('option:selected', this).val();
	if (countryVal != "IN") {
		$(".state-select").hide();
		$(".state-text").show();
	}
	else {
		$(".state-text").hide();
		$(".state-select").show();
	}
});

$(".save-add-address").live('click', function() {
	var addAddressForm = $("#add-address");
	var hasError = validateAddressFields(addAddressForm);
	if (!hasError) {
		addAddressForm.submit();
	}
});

$(".save-edit-address").live('click', function() {
	var editAddressForm = $("#edit-address");
	var hasError = validateAddressFields(editAddressForm);

	if (!hasError) {
		editAddressForm.submit();
	}
});

function prePopulateEditFormFields(addressForm, addressListing) {
	$("#id", addressForm).val($("#id", addressListing).val());
	$(".name", addressForm).val($(".name", addressListing).html().trim());
	$(".address-textarea", addressForm).val($(".address", addressListing).html().replace(/<br>/gi, ''));
	$(".locality", addressForm).val($(".locality", addressListing).html());
	$(".city", addressForm).val($(".city", addressListing).html());
	$(".pincode", addressForm).val($(".pincode", addressListing).html());
	$(".mobile", addressForm).val($(".mobile-no", addressListing).html());
	$(".email", addressForm).val($(".email", addressListing).html());
	$(".phone", addressForm).val($(".phone", addressListing).html());
	$(".state", addressForm).val($(".state", addressListing).html());
	$(".country", addressForm).val($(".country", addressListing).html());
}

function clearAddressErrorFields(addressForm) {
	$(".name-error", addressForm).html("").slideUp();
	$(".your-name-error", addressForm).html("").slideUp();
	$(".address-error", addressForm).html("").slideUp();
	$(".locality-error", addressForm).html("").slideUp();
	$(".city-error", addressForm).html("").slideUp();
	$(".pincode-error", addressForm).html("").slideUp();
	$(".mobile-error", addressForm).html("").slideUp();
	$(".email-error", addressForm).html("").slideUp();
	$(".country-error", addressForm).html("").slideUp();
	$(".state-error", addressForm).html("").slideUp();
}

var clearZipErr = function() {
	$(".pincode-error").html("").hide();
	$(".zip-err-warning-my-myntra").addClass("hide");
	$(".zip-err-text-my-myntra").html("");
	$("input[name=pinerr]").each(function() {
		$(this).val("0");
	});
	$(".save-edit-address").val("Save");
	$(".save-add-address").val("Save");
};

$(".country-select").each(function() {
	$(this).change(clearZipErr);
});
$(".state-select").each(function() {
	$(this).change(clearZipErr);
});
$(".pincode").each(function() {
	$(this).change(clearZipErr);
});

var validateZipState = (typeof stateZipsJson == 'undefined' || stateZipsJson == null) ? false: true;
function isPincodeValidForState(addressForm) {
	var countryVal = $(".country-select", addressForm).val();
	var pincodeVal = $(".pincode", addressForm).val();
	var stateVal = (countryVal == 'IN') ? $(".state-select", addressForm).val() : $(".state-text", addressForm).val();

	if (countryVal == 'IN' && validateZipState && stateVal != - 1) {
		var zips = stateZipsJson["IN-" + stateVal];
		var pass = false;
		for (index in zips) {
			var eachZip = zips[index];
			if (eachZip == pincodeVal.substring(0, eachZip.length)) {
				pass = true;
				return true;
			}
		}
		pass = false;
	}
	else { //if country is not india, then pass this validation anyways
		pass = true;
	}
	return pass;
}
function isDataCorrect(addressForm) {
	var pass = isPincodeValidForState(addressForm);
	var pinErr = $("input[name=pinerr]", addressForm);
	if (!pass) {
		var countryVal = $(".country-select", addressForm).val();
		var stateText = (countryVal == 'IN') ? $(".state-select option:selected", addressForm).text() : $(".state-text", addressForm).val();

		$(".pincode-error", addressForm).html("Pincode invalid for state.").slideDown();
		$(".zip-err-text-my-myntra", addressForm).html("The pincode you've entered doesn't seem to belong to " + stateText + ". If the pincode is not correct, your delivery might get delayed.");
		$(".zip-err-warning-my-myntra", addressForm).removeClass("hide").addClass("clearfix");
		$(".save-btn", addressForm).val("Save Anyways");
		pinErr.val("1");
	}
	else {
		pinErr.val("0");
	}
	var finalpass = pass; //have more data validations in future: combine individual passes
	return pass;
}

function validateAddressFields(addressForm) {
	var error = false;
	var nameVal = $(".name", addressForm).val();
	var showRecipientVal = $(".copy-as-acc-name", addressForm).attr("checked");
	var yourNameVal = $(".your-name", addressForm).val();
	var addressVal = $(".address-textarea", addressForm).val();
	var localityVal = $(".locality", addressForm).val();
	var cityVal = $(".city", addressForm).val();
	var pincodeVal = $(".pincode", addressForm).val();
	var mobileVal = $(".mobile", addressForm).val();
	var emailVal = $(".email", addressForm).val();
	var countryVal = $(".country-select", addressForm).val();
	var stateVal = (countryVal == 'IN') ? $(".state-select", addressForm).val() : $(".state-text", addressForm).val();

	var pinErrVal = $("input[name=pinerr]", addressForm).val();

	clearAddressErrorFields(addressForm);

	if (nameVal == "") {
		error = true;
		$(".name-error").text("Enter a name.").slideDown();
	}

	if (showRecipientVal == false && yourNameVal == "") {
		error = true;
		$(".your-name-error").text("Enter a name.").slideDown();
	}
	if (addressVal == "") {
		error = true;
		$(".address-error").text("Enter a valid address.").slideDown();
	}

	if (pincodeVal == "") {
		error = true;
		$(".pincode-error").text("Enter a valid pincode.").slideDown();
	}
	else if (countryVal == 'IN' && (!numericReg.test(pincodeVal) || pincodeVal.length != 6)) {
		error = true;
		$(".pincode-error").text("Pincode must be exactly 6 digits").slideDown();
	}

	if ($('body').hasClass('mab-locality-yes') && localityVal == "") {
		error = true;
		$(".locality-error").text("Enter a valid locality.").slideDown();
	}

	if (cityVal == "") {
		error = true;
		$(".city-error").text("Enter a valid city.").slideDown();
	}

	if (mobileVal == "" || mobileVal.length < 10 || ! numericReg.test(mobileVal)) {
		error = true;
		$(".mobile-error").text("Enter a valid mobile number.").slideDown();
	}

	if (emailVal == "" || ! emailReg.test(emailVal)) {
		error = true;
		$(".email-error").text("Enter a valid email address.").slideDown();
	}

	if (countryVal == "" || countryVal == "-1") {
		$(".country-error").text("Enter a valid country.").slideDown();
		error = true;
	}
	if (stateVal == "" || stateVal == "-1") {
		error = true;
		$(".state-error").text("Enter a valid state.").slideDown();
	}

	if (error) {
		return error;
	}
	if (pinErrVal == "0") {
		var adrsValid = isDataCorrect(addressForm);
		if (!adrsValid) {
			_gaq.push(['_trackEvent', 'address', 'address_save_failed', 'pincode_validation_error']);
		}
		return ! adrsValid;
	}
	else if (pinErrVal == "1") {
		return false;
	}
}

// [myprofile.tpl]
function validateZipAgainstState() {
	//only if country selected is india
	if ($("#country-select").val() == "IN") {
		var stateCode = "IN-" + $('#state-select').val();
		var zips = stateZipsJson[stateCode];
		var selectedZip = $("#pincode").val();
		var pass = false;
		for (index in zips) {
			var eachZip = zips[index];
			if (eachZip == selectedZip.substring(0, eachZip.length)) {
				pass = true;
			}
		}
		return pass;
	}
	return true;
}
// [end myprofile.tpl]

// [myreturns.tpl]
function showCourierDetailsWindow(returnid, itemid) {
	if ($("#courier_update_div_" + returnid + "_" + itemid).css("display") == 'none') {
		$("#courier_update_href_" + returnid).hide();
		var d = new Date();
		$.ajax({
			type: 'POST',
			url: 'mymyntra.php',
			data: "itemid=" + itemid + "&returnid=" + returnid + "&mode=return-courierdetails-div&date=" + d.getTime(),
			success: function(response) {
				$("#courier_update_div_" + returnid + "_" + itemid).html(response);
				$("#courier_update_div_" + returnid + "_" + itemid).show();
			},
			error: function() {
				$("#courier_update_div_" + returnid + "_" + itemid).html("There seems to be a technical error! Please try again later");
				$("#courier_update_div_" + returnid + "_" + itemid).show();
			}
		});
	} else {
		closeCourierDetailsWindow(returnid, itemid);
	}
	return true;
}

function closeCourierDetailsWindow(returnid, itemid) {
	$("#courier_update_div_" + returnid + "_" + itemid).hide();
	$("#courier_update_div_" + returnid + "_" + itemid).html("");
	$("#courier_update_href_" + returnid).show();
}

function updateCourierServiceSelection(courier) {
	if (courier == "other") {
		$("#courier_service_custom_div").css('display', "block");
	} else {
		$("#courier_service_custom_div").css('display', "none");
	}
}

function updateTrackingDetails() {
	var courier = $("#courier_service").val();
	var trackingno = $("#tracking_no").val();
	var returnId = $("#current_return_id").val();
	var itemId = $("#current_item_id").val();

	if (courier == "other") {
		courier = $("#courier_service_custom").val();
	}

	if (courier == "" || trackingno == "") {
		alert("Please enter all the details first");
		return;
	}

	$("#courierdetails-progress").css("display", "block");
	var d = new Date();
	$.ajax({
		type: 'POST',
		url: 'mymyntra.php',
		dataType: 'json',
		data: {
			'courier_service': courier,
			'tracking_no': trackingno,
			'itemid': itemId,
			'returnid': returnId,
			'mode': 'return-update-courierdetails',
			'date': d.getTime(),
			'_token': Myntra.Data.token
		},
		success: function(response) {
			if (response[0] == 'SUCCESS') {
				$("#courier_update_href_" + returnId).hide();
				result = "Thanks for sharing your shipping details. <br/>We'll let you know once we've recived your returns.";
				result = result + "<br/><br/>Your shipping vendor: " + response[1];
				result = result + "<br/>Tracking Number: " + response[2];
				$("#return_message_" + returnId).html(result);
				$("#courier_update_div_" + returnId + "_" + itemId).hide();
				$("#courier_update_div_" + returnId + "_" + itemId).html("");
			} else {
				$("#courier_update_div_" + returnId + "_" + itemId).hide();
				$("#courier_update_div_" + returnId + "_" + itemId).html("");
				$("#courierdetails-errorMsg").css("display", "block");
			}
			$("#courierdetails-progress").css("display", "none");
		},
		error: function(xmlHttpRequest, textStatus, errorThrown) {
			$("#courierdetails-errorMsg").css("display", "block");
			$("#courierdetails-progress").css("display", "none");
		}
	});
}
// [end myreturns.tpl]
// [mymyntra.tpl]
var currentlocationid = "";
var initPincodeLocality = function() {
	var cache = {};
    var inprogress = {};
	var successAdd = function(data) {
		var loc = $('#add-address .locality').val();
        if (loc.length && $.inArray(loc, data.locality) === -1) {
            $('#add-address .locality').val('');
        }
		$('#add-address .locality').setOptions({
			data: data.locality
		});
		$('#add-address .city').val(data.city);
		$('#add-address .state-select').val(data.state);
		$('#add-address .city-error').hide();
		$('#add-address .state-error').hide();
	};
	var successEdit = function(data) {
		var loc = $('#edit-address .locality').val();
        if (loc.length && $.inArray(loc, data.locality) === -1) {
            $('#edit-address .locality').val('');
        }
		$('#edit-address .locality').setOptions({
			data: data.locality
		});
		$('#edit-address .city').val(data.city);
		$('#edit-address .state-select').val(data.state);
		$('#edit-address .city-error').hide();
		$('#edit-address .state-error').hide();
	};
    var fetchData = function(pcode, mode) {
        if (inprogress[pcode]) return;
        inprogress[pcode] = 1;

        var locality = mode == 'add' ? $('#add-address .locality') : $('#edit-address .locality');
		$.ajax({
			url: "/myntra/ajax_pincode_locality.php?pincode=" + pcode,
			dataType: "json",
			beforeSend: function() {
                locality.attr('disabled', 'disabled');
				locality.addClass('ac_loading');
			},
			success: function(data) {
				cache[pcode] = data;
				mode == 'add' ? successAdd(data) : successEdit(data);
				locality.trigger('showautocomplete');
			},
			complete: function(xhr, status) {
                locality.removeAttr('disabled');
				locality.removeClass('ac_loading');
                delete inprogress[pcode];
			}
		});
    }

	$('#add-address .locality').autocomplete([], {
		minChars: 0
	});
	$('#add-address .pincode').blur(function() {
		var pcode = $(this).val();
		if (!/\d{6}/.test(pcode)) {
			return
		}
        if ($(this).data('pcode') === pcode) {
            return;
        }
        $(this).data('pcode', pcode);
		if (pcode in cache) {
			successAdd(cache[pcode]);
			$('#add-address .locality').trigger('showautocomplete');
			return;
		}
		$('#add-address .locality').setOptions({data:[]});
        fetchData(pcode, 'add');
	});
	$('#add-address .locality').bind('result', function(e, data) {
		$('#add-address .selected-locality').val(data[0]);
	});
	$('#add-address .locality').focus(function(e) {
		var pcode = $('#add-address .pincode').val();
		if (pcode in cache) {
			$(this).trigger('showautocomplete');
		}
        else {
            $(this).setOptions({data:[]});
            fetchData(pcode, 'add');
        }
	});

	$('#edit-address .locality').autocomplete([], {
		minChars: 0
	});
	$('#edit-address .pincode').blur(function() {
		var pcode = $(this).val();
		if (!/\d{6}/.test(pcode)) {
			return
		}
        if ($(this).data('pcode') === pcode) {
            return;
        }
        $(this).data('pcode', pcode);
		if (pcode in cache) {
			successEdit(cache[pcode]);
			$('#edit-address .locality').trigger('showautocomplete');
			return;
		}
		$('#edit-address .locality').setOptions({data:[]});
        fetchData(pcode, 'edit');
	});
	$('#edit-address .locality').bind('result', function(e, data) {
		$('#edit-address .selected-locality').val(data[0]);
	});
	$('#edit-address .locality').focus(function(e) {
		var pcode = $('#edit-address .pincode').val();
		if (pcode in cache) {
			$(this).trigger('showautocomplete');
		}
        else {
            $(this).setOptions({data:[]});
            fetchData(pcode, 'edit');
        }
	});

	$(document).bind('myntra.editaddress.loaded', function(e) {
		$('#edit-address .pincode').triggerHandler('blur');
	});
};

function verifyCaptcha() {
	if ($(".verify-captcha").hasClass("btn-disabled")) {
		return;
	}
	if ($('#captcha-form').val() == "") {
		$(".captcha-message").html("Please enter the text.");
		$(".captcha-message").addClass("error");
		$(".captcha-message").slideDown();
		return;
	}
	$(".captcha-loading").show();
	$(".verify-captcha").addClass("btn-disabled")
	$(".captcha-message").html("").removeClass("error");
	$(".captcha-message").slideUp();
	$.ajax({
		type: "POST",
		url: "/mobile_verification.php",
		data: "mobile=" + $('#mobile').val() + "&userinputcaptcha=" + $('#captcha-form').val() + "&login=" + $('#login').val() + "&id=myProfilePage",
		success: function(msg) {

			var resp = $.trim(msg);
			$(".captcha-loading").hide();
			$(".verify-captcha").removeClass("btn-disabled");
			if (resp == 'CaptchaConfirmed') {
				$(".captcha-block").hide();
				$(".verify-mobile-profile-page").trigger("click");

			} else if (resp == 'WrongCaptcha') {
				$(".captcha-message").html('Wrong Code entered');
				$(".captcha-message").addClass('error');
				$(".captcha-message").slideDown();
			}
		}
	});
}
function hideMessage() {
	$(".notification").hide();
}

$(document).ready(function() {
	$('#captcha-form').keydown(function(event) {
		if (event.keyCode == '13') {
			event.preventDefault();
			$(".verify-captcha").trigger("click");
		}
	});

	$("#accordion  li  div").click(function() {
		$("#bc1").html("");
		$("#bc2").html("");
		$("#accordion  li").removeClass("selected");
		$(this).parent().addClass("selected");
		if (false == $(this).next().is(':visible')) {
			$('#accordion ul').slideUp(300);
		}
		if ($(this).find("a").attr("href") != null) {
			var activeTab = $(this).find("a").attr("href");
			var activeID = $(this).attr("id");
			$("#bc1").html(" &raquo; " + $(this).find("a").html());
			loadView(activeID, activeTab);
		} else {
			$("#bc1").html(" &raquo; " + $(this).html());
			$(this).parent().find("ul").find("li:first").find("a").click();
		}
		$(this).next().slideToggle(300);
	});

	$("#accordion ul li a").click(function() {
		$(this).parent().parent().find('a').removeClass("active");
		$(this).addClass("active");
		var activeTab = $(this).attr("href");
		var activeID = $(this).parent().attr("id");
		$("#bc2").html(" &raquo; " + $(this).attr("title"));
		loadView(activeID, activeTab);
	});

	$("ul.innertabs li").live('click', function() {
		$("ul.innertabs li").removeClass("active");
		$(this).addClass("active");
		$(".inner-reff-content").hide();
		var currentTab = $(this).find("a").attr("class");
		$(currentTab).show();
		return false;
	});
	$("ul.right-mytabs li").live('click', function() {
		$("ul.right-mytabs li").removeClass("active");
		$(this).addClass("active");
		$(".reff-content").hide();
		var currentTab = $(this).find("a").attr("class");
		$(currentTab).show();
		return false;
	});

	if (tab_view != '' && tab_view != null && tab_view != 'myprofile') {
		var dom = $('#' + tab_view).get(0);
		if (dom.tagName != 'DIV') {
			$('#' + tab_view).parent().parent().find("div:first").click();
		}
		$('#' + tab_view).find("a").click();
		if (error != '') {
			$(".error").show();
		}
		if (message != '') {
			$(".success").show();
		}

	}
	if (tab_view == 'myprofile') {
		initPincodeLocality();
	}
    else if (tab_view == 'myorders') {
		initOrderTooltip();
	}
    else if (tab_view == 'myreturns') {
		initReturnsTooltip();
	}
});

function showReturns(cust_end_state, returnid) {
	currentlocationid = returnid;
	$("#returns").click();
	if (cust_end_state == 1) {
		$("#mycompletedreturns").find("a").click();
	}
	$("#" + currentlocationid + "_top").focus();
}

function showOrders(status, orderid) {
	currentlocationid = orderid;
	$("#orders").click();
	if (status == 'C' || status == 'DL') {
		$("#mycompletedorders").find("a").click();
	}
	$("#" + currentlocationid + "_top").focus();
}

function initReturnsTooltip(){
	$('.mk-return-number').hover(function(e) {
        var orderid = parseInt($(this).text());

        var tip;
        if (!$(this).data('tip')) {
            tip = $(this).next().appendTo('body');
            $(this).data('tip', tip);
        }
        tip = $(this).data('tip');
        tip.e = e;
        tip.show();
        tip.css({'left': (tip.e.pageX + 0) + 'px', 'top': (tip.e.pageY - tip.height() - 47) + 'px'});

  		},
  		function() {
  			$(this).data('tip').hide();
  		}
	);

	var boxes = {};
	$('.mk-return-number').click(function(e) {
        var id = 'return-' + $(this).text();
        if (!boxes[id]) {
            boxes[id] = Myntra.LightBox('#' + id);
        };
        boxes[id].show();
    });

	$(".refund-details .ico").click(function(){
		var ico = $(this),
        pnode = ico.closest(".refund-details"),
        refunddetails = pnode.find(".refund-details-expand");
		finalprice = pnode.find(".final-price");

		if (ico.hasClass('ico-expand')) {
			refunddetails.show();
			ico.removeClass('ico-expand').addClass('ico-collapse');
			finalprice.addClass("final-price-border");
		}
		else if (ico.hasClass('ico-collapse')) {
			refunddetails.hide();
			ico.removeClass('ico-collapse').addClass('ico-expand');
			finalprice.removeClass("final-price-border");
		}
	});

	$('.myreturnbox .final-price .ico').click(function(e) {
        var ico = $(this),
            pnode = ico.parent().parent(),
            mrp = pnode.find('.mrp'),
            discounts = pnode.find('.discounts');
            finalprice = pnode.find(".final-price");

        if (!mrp.length && !discounts.length) { return }
        if (ico.hasClass('ico-expand')) {
            mrp.removeClass('hide');
            discounts.removeClass('hide');
            ico.removeClass('ico-expand').addClass('ico-collapse');
            finalprice.addClass("final-price-border");
        }
        else if (ico.hasClass('ico-collapse')) {
            mrp.addClass('hide');
            discounts.addClass('hide');
            ico.removeClass('ico-collapse').addClass('ico-expand');
            finalprice.removeClass("final-price-border");
        }
    });
}

function initOrderTooltip() {
	$('.mk-order-number').hover(function(e) {
            var orderid = parseInt($(this).text());

            var tip;
            if (!$(this).data('tip')) {
                tip = $(this).next().appendTo('body');
                $(this).data('tip', tip);
            }
            tip = $(this).data('tip');
            tip.e = e;
            loadDiscountData(orderid, null, null, tip);
            tip.show();
            tip.css({'left': (tip.e.pageX + 0) + 'px', 'top': (tip.e.pageY - tip.height() - 47) + 'px'});

      	},
      	function() {
        	$(this).data('tip').hide();
      	}
	);

    var boxes = {};
	$('.mk-order-number').click(function(e) {
        var id = 'order-' + $.trim($(this).text());
        if (!boxes[id]) {
            boxes[id] = Myntra.LightBox('#' + id);
        };
        loadDiscountData($(this).text(), null, boxes[id]);
    });

    $('.orderbox .final-price .ico').click(function(e) {
        var ico = $(this),
            pnode = ico.parent().parent(),
            mrp = pnode.find('.mrp'),
            discounts = pnode.find('.discounts');
            finalprice = pnode.find(".final-price");

        if (!mrp.length && !discounts.length) { return }
        if (ico.hasClass('ico-expand')) {
            mrp.removeClass('hide');
            discounts.removeClass('hide');
            ico.removeClass('ico-expand').addClass('ico-collapse');
            finalprice.addClass("final-price-border");
        }
        else if (ico.hasClass('ico-collapse')) {
            mrp.addClass('hide');
            discounts.addClass('hide');
            ico.removeClass('ico-collapse').addClass('ico-expand');
            finalprice.removeClass("final-price-border");
        }
    });
    	
    $('.mk-cancel-order').click(function(e) {
            var id = 'cancel-order-' + $(this).attr('orderid');
            if (!boxes[id]) {
            	$('#reason-validation-div-'+id).css('display','none');
            	$('#remarks-validation-div-'+id).css('display','none');
            	$("#msg-box-"+id).css('display','none');
            	$("#cancel-buttons-"+id).css('display','block');
                boxes[id] = Myntra.LightBox('#' + id);
            };
            boxes[id].show();
    });
    	
    $('.cancel-submit').click(function (){
    		$("#cancel-buttons-"+orderid).css('display','none');
    		var orderid = $(this).attr('orderid');
    		var reason = $('#cancel-reason-'+orderid).val();
    		var details = $('#details-'+orderid).val();
    		var error = false;
    		if(reason == '0'){
    			$('#reason-validation-div-'+orderid).css('display','block');
    			error = true;
    		}
    		if(details == ''){
    			$('#remarks-validation-div-'+orderid).css('display','block');
    			error = true;
    		}
    		if(error){
    			$("#cancel-buttons-"+orderid).css('display','block');
    			return false;
    		}
    		$('#cancel-panel-'+orderid).css('display','none');
    		$('#loading').css('display','block');
    		$.ajax({
                type: 'POST',
                url: "mymyntra.php",
                data: "_token=" + Myntra.Data.token + "&mode=cancel-order&orderid=" + orderid,
                success: function(data){
                    data = jQuery.parseJSON(data);
                    if(data.result == 'success'){
                    	$('#loading-'+orderid).css('display','none');
                    	$("#msg-box-"+orderid).html("Order has been cancelled successfully");
                    	$("#msg-box-"+orderid).css('display','block');
                    }
                },
                error: function(data){
                	$('#loading-'+orderid).css('display','none');
                	$("#msg-box-"+orderid).html("There is an unexpected technical error. Please get in touch with our customer care team");
                	$("#msg-box-"+orderid).css('display','block');
                    return false;
                }
            });
    		return false;
    	});
    	
    $('.cancel-no').click(function (){
    		var id = 'cancel-order-' + $(this).attr('orderid');
    		$('.cancel-valid').css('display','none');
    		boxes[id].hide();
    });
    	
    $('.orderbox .btn-return').click(function(e) {
        var btn = $(this),
            key = btn.data('return-key');
        if (!Myntra.Data.orderItems) {
            var json = $('#order-items-json').html();
            try {
                Myntra.Data.orderItems = $.parseJSON(json);
            }
            catch (ex) {
                alert('Order items json is invalid');
                return;
            }
        }
        var data = Myntra.Data.orderItems[key],
            lbox = boxes['order-' + data.baseOrderId],
            conf = {
                cfg: {
                    isModal:true,
                    _type:'return',
                    beforeShow:function(){ lbox.tmpHide() },
                    afterHide:function(){ lbox.tmpShow() }
                },
                data: data
            };
        $(document).trigger('myntra.return.show', conf);
    });

    $(document).bind('myntra.return.done', function(e, data) {
        var key = data.orderId + '-' + data.itemId,
            btn = $('.btn-return[data-return-key="' + key + '"]');
        btn.prev().removeClass('hide');
        btn.remove();
    });
}

function initDiscountEvents(orderid){
    $(".myorder-discount-submit-"+orderid).click(function(event){
       if($(this).parents(".discount-area").find("#discount-tnc-"+orderid).is(':checked')){
           var lb = Myntra.LightBox('#order-' + orderid);
           var vCode = $(this).parents(".discount-area").find("#voucher-inp-order-"+orderid).val();
           loadDiscountData(orderid, vCode, lb);
       }else{
           alert("Please accept the Terms and Conditions to proceed");
       }
    });
}

function loadDiscountData(orderid, vCode, lb, tip) {
        var url = "getVoucherData.php?pagetype=myorder&orderid="+orderid;
        if(vCode!=null){
            url = url + "&code=" + vCode;
        }
        $.ajax({
            type: 'GET',
            url: url,
            success: function(data){
                data = jQuery.parseJSON(data);;
                $(".orderbox .bd .discount-area").html(data.main);
                $(".discount-tool-tip-"+orderid).html(data.tooltip);
                if(lb!=null) {
                    lb.show();
                    initDiscountEvents(orderid);
                }
                if(tip!=null && data.tooltip!=null && data.tooltip!=''){
                    $(".discount-tool-tip-"+orderid).css("border-top", "1px solid #E5E5E5");
                    $(".discount-tool-tip-"+orderid).css("padding-top", "5px");
                }

            },
            error: function(data){
                return false;
            }
        });

}

function loadView(viewname, activeTab) {
	for (var i = 0; i < get_view.length; i++) {
		if (get_view[i] == viewname) {
			$(".tab_content").hide(); //Hide all tab content
			$(activeTab).show(); //Active ID content
			return;
		}
	}
	$(".mymyntra-ajax-loader").show();
	var d = new Date();
	$.ajax({
		type: "POST",
		url: "/mymyntra.php",
		data: "mode=" + viewname + "&date=" + d.getTime(),
		success: function(msg) {
			var data = msg.split("#####");
			$("#" + data[1]).html(data[0]);
			if (data[1] == 'myreferrals_tab') {
				$(".reff-content").hide();
				$("ul.right-mytabs li:first").addClass("active");
				$(".reff-content:first").show();
				toggle_text_all(".toggle-text");
			}
			get_view.push(viewname);
			$(".mymyntra-ajax-loader").hide();
			vtip();
			$(".tab_content").hide();
			$(activeTab).show();
			if (data[1] == 'myprofile_tab') {
				initPincodeLocality();
			}
            else if (data[1] == 'myorders_tab') {
				initOrderTooltip();
			}
            else if (data[1] == 'myreturns_tab') {
            	initReturnsTooltip();
			}
		}
	});
}

function showUsernameMissingWindow() {
	$("#username_missing_div").css("display", "block");
	$("#UsernameMissingContent").css("display", "block");
	$("#TB_title").css("display", "block");

}

function closeUsernameMissingWindow() {
	$("#username_missing_div").css("display", "none");
	$("#UsernameMissingContent").css("display", "none");
}
function updateUsername() {
	var username = $.trim($("#new_fullname").val());
	if (username == '') {
		alert("please enter full name");
		return;
	}
	$.ajax({
		type: "POST",
		url: http_loc + "/mymyntra.php",
		data: "_token=" + Myntra.Data.token + "&mode=update-profile-username&username=" + username,
		success: function(msg) {
			closeUsernameMissingWindow();
			$(".invite-friends").trigger('click');
		}
	});
}

function showShipmentTracking(order_id) {
	$.ajax({
		type: "POST",
		url: http_loc + "/order_shipment_tracking.php",
		data: "orderId=" + order_id,
		success: function(result) {
			Myntra.MsgBox.show("", result);
		}
	});
}

function showAmountBreakupDetails(entityId, entityName) {
	$("#show_" + entityName + "details_link_" + entityId).css("display", "none");
	$("#" + entityName + "_amount_details_" + entityId).css("display", "block");
	$("#hide_" + entityName + "details_link_" + entityId).css("display", "inline-block");
}

function hideAmountBreakupDetails(entityId, entityName) {
	$("#show_" + entityName + "details_link_" + entityId).css("display", "inline-block");
	$("#" + entityName + "_amount_details_" + entityId).css("display", "none");
	$("#hide_" + entityName + "details_link_" + entityId).css("display", "none");
}
// [end mymyntra.tpl]
// Function to share referral link on facebook.
function fb_share_referral(login) {
	_gaq.push(['_trackEvent', 'my_referrals', 'fb_referral_share', 'click']);
	var uri = http_loc + "/register.php?ref=" + login + "&utm_source=MRP&utm_medium=FBpost&utm_campaign=refjoin";
	window.open("http://www.facebook.com/sharer.php?u=" + encodeURIComponent(uri), '', 'width=600,height=400,left=100,top=100');
}

// Share referral link on twitter.
function twitter_share_referral(login, num_coupons, value_coupons) {
	//login = login.replace("@", "%40");
	// login = login.replace("%", "%25");
	_gaq.push(['_trackEvent', 'my_referrals', 'twitter_referral_share', 'click']);
	var longUrl = encodeURIComponent(http_loc + "/register.php/?ref=" + login + "&utm_source=MRP&utm_medium=Twitterpost&utm_campaign=refjoin");
	var shortUrl = longUrl; // in case of error condition
	$.ajax({
		url: http_loc + "/urlshortener.php",
		//this is the php script above
		dataType: "json",
		type: "POST",
		async: false,
		data: {
			url: longUrl
		},
		success: function(data) {
			if (data.status_txt === "OK") {
				shortUrl = data.data.url;
			}
		},
		error: function(xhr, error, message) {
			//no success, fallback to the long url
			shortUrl = longUrl;
		}
	});

	window.open("http://twitter.com/intent/tweet?text=Thanks%20to%20me,%20you%20now%20have%20" + num_coupons + "%20@myntra%20coupons%20of%20" + value_coupons + "%20each.%20All%20you%27ve%20to%20do%20is%20click%20here%20and%20register%20" + shortUrl + "%20%23notspam");
}

function resendFriendInvite(email) {
	$.ajax({
		type: "POST",
		url: "mymyntra.php",
		data: "_token=" + Myntra.Data.token + "&mode=invite-friends&email=" + email,
		async: false,
		success: function(msg) {
			msg = $.trim(msg);
			if (msg == "success") {
				_gaq.push(['_trackEvent', 'my_referrals', 'send_invites', 'success']);
				alert("Successfully sent invitation");
			} else if (msg == "custfailure") {
				_gaq.push(['_trackEvent', 'my_referrals', 'send_invites', 'failure_user_registered']);
				alert("Invitee has already registered");
			} else if (msg == "timestampfailure") {
				_gaq.push(['_trackEvent', 'my_referrals', 'send_invites', 'failure_user_referred_today']);
				alert("Email sent earlier today");
			}
			if (msg == "success") {
				// refresh the myrefferals page
				$.ajax({
					type: "POST",
					url: http_loc + "/mymyntra.php",
					data: "mode=myreferrals",
					success: function(msg) {
						var data = msg.split("#####");
						$("#" + data[1]).html(data[0]);
						if (data[1] == 'myreferrals') {
							// When page loads...
							$(".reff-content").hide(); // Hide all content
							$("ul.right-mytabs li:first").addClass("active"); // Activate
							// first
							// tab
							$(".reff-content:first").show(); // Show first tab
							// content
						}
					}
				});
			}

		}
	});

}

function cleanTextArea() {
	$("#recipient_list").val('');
	$("#recipient_list").removeClass("toggle-text");
	$("#recipient_list").removeClass("blurred");
	$("#recipient_list").unbind();
}

/* Tooltip */
this.vtip = function() {
	this.xOffset = 15; // x distance from mouse
	this.yOffset = - 20; // y distance from mouse
	$(".vtip").unbind().hover(
	function(e) {
		this.t = this.title;
		this.title = '';
		this.top = (e.pageY + yOffset);
		this.left = (e.pageX + xOffset);

		$('body').append('<div id="vtip">' + this.t + '</div>');

		// $('div#vtip #vtipArrow').attr("src", 'images/vtip_arrow.png');
		$('div#vtip').css("top", this.top + "px").css("left", this.left + "px").fadeIn("slow");

	},
	function() {
		this.title = this.t;
		$("div#vtip").fadeOut("slow").remove();
	}).mousemove(
	function(e) {
		this.top = (e.pageY + yOffset);
		this.left = (e.pageX + xOffset);

		$("div#vtip").css("top", this.top + "px").css("left", this.left + "px");
	});

};

jQuery(document).ready(function($) {
	vtip();
});

// Gopi:
// trigger the add/update address forms with the erros messages and the user entered form data
// the error messages comes from the php side exported as json object (Myntra.Data.my)
if (Myntra.Data.my) {
    var addressForm,
        err = Myntra.Data.my.errors,
        dat = Myntra.Data.my.post;

    if ('add-address' === Myntra.Data.my.mode) {
        addressForm = $("#add-address");
        clearAddressErrorFields(addressForm);
        $(".add-address").toggle();
    }
    else if ('update-address' === Myntra.Data.my.mode) {
        addressForm = $("#edit-address");
        clearAddressErrorFields(addressForm);
        $(".edit-address").toggle();
    }

    $(".name", addressForm).val(dat.name);
    $(".your-name", addressForm).val(dat['your-name']);
    $(".address-textarea", addressForm).val(dat.address);
    $(".locality", addressForm).val(dat.locality);
    $(".city", addressForm).val(dat.city);
    $(".pincode", addressForm).val(dat.pincode);
    $(".mobile", addressForm).val(dat.mobile);
    $(".email", addressForm).val(dat.email);
    $(".country", addressForm).val(dat.country);
    $(".state-select", addressForm).val(dat.state);
    $(".pinerr", addressForm).val(dat.pinerr);

    (dat['copy-as-acc-name'] == 'on') && $(".copy-as-acc-name", addressForm).attr('checked', 'checked');
    dat.id && $('input[name="id"]', addressForm).val(dat.id);

    err.name && $(".name-error", addressForm).html(err.name).slideDown();
    err['your-name'] && $(".your-name-error", addressForm).html(err['your-name']).slideDown();
    err.address && $(".address-error", addressForm).html(err.address).slideDown();
    err.locality && $(".locality-error", addressForm).html(err.locality).slideDown();
    err.city && $(".city-error", addressForm).html(err.city).slideDown();
    err.pincode && $(".pincode-error", addressForm).html(err.pincode).slideDown();
    err.mobile && $(".mobile-error", addressForm).html(err.mobile).slideDown();
    err.email && $(".email-error", addressForm).html(err.email).slideDown();
    err.country && $(".country-error", addressForm).html(err.country).slideDown();
    err.state && $(".state-error", addressForm).html(err.state).slideDown();

    //$(".pincode", addressForm).triggerHandler('blur');
}

