
Myntra.AddressListBox = function(id, cfg) {
    cfg = cfg || {};
    cfg.css = cfg.css || '';

    var isInited = false, selectedAddress, addresses = {},
        idstr = id.substr(0, 1) === '#' ? id.substr(1) : id,
        obj,
        markup = [
        '<div id="' + idstr + '" class="mk-lightbox address-list-box ' + cfg.css + '">',
        '<div class="mod">',
        '  <div class="hd">',
        '    <div class="title"></div>',
        '    <div class="subtitle"></div>',
        '  </div>',
        '  <div class="bd">',
        '    <button class="link-btn btn-create"></button>',
        '    <div class="list"></div>',
        '  </div>',
        '  <div class="ft">',
        '    <span class="note"></span>',
        '    <button class="btn normal-btn btn-save">Done</button>',
        '    <button class="btn normal-btn btn-cancel">Cancel</button>',
        '  </div>',
        '</div>',
        '</div>'
    ].join('');
    $('body').append(markup);
    obj = Myntra.LightBox(id, cfg);
    obj.root = $(id);
    obj.set('ajax_url', '/myntra/ajax_address.php');

    var hd = $('.mod > .hd', obj.root),
        bd = $('.mod > .bd', obj.root),
        ft = $('.mod > .ft', obj.root),
        title = $('.mod > .hd > .title', obj.root),
        subTitle = $('.mod > .hd > .subtitle', obj.root),
        ftNote = $('.mod > .ft > .note', obj.root),
        btnSave = $('.btn-save', obj.root),
        btnCancel = $('.btn-cancel', obj.root),
        btnCreate = $('.btn-create', obj.root),
        list = $('.mod > .bd > .list', obj.root),
        items;

    obj.setTitle = function(markup) { title.html(markup) };
    obj.setSubTitle = function(markup) { subTitle.html(markup) };
    obj.setBody = function(markup) { bd.html(markup) };
    obj.setFootNote = function(markup) { ftNote.html(markup) };

    obj.enableSave = function() { btnSave.removeAttr('disabled') };
    obj.disableSave = function() { btnSave.attr('disabled', 'disabled') };
    obj.showSave = function() { btnSave.removeClass('hide') };
    obj.hideSave = function() { btnSave.addClass('hide') };
    obj.setSaveText = function(txt) { btnSave.text(txt) };

    obj.buildItem = function(address, css) {
        var tpl = [
        '<tr data-id="{id}" class="', css, '">',
        '    <td class="opt' + (address.is_servicable ? '' : ' disabled') + '">',
        '        <input type="radio" name="address-sel" value="{id}"' + (address.is_servicable ? '' : 'disabled="true"') + '>',
        '    </td>',
        '    <td class="address">',
        '        <strong>{name}</strong><br>',
        '        {address}<br>',
        '        {locality}<br>',
        '        {city} - {pincode}<br>',
        '        {state}<br>',
        '        <strong>Mobile:</strong> {mobile}',
        '    </td>',
        '</tr>'
        ].join('');

        var markup = tpl.replace(/\{(.*?)\}/g, function(match, s1) {
            if (s1 == 'address') {
                return address[s1].replace("\n", "<br>");
            }
            return address[s1];
        });

        return markup;
    };

    obj.buildList = function() {
        var markups = [], count = 0, css,
            tip = $('<div class="myntra-tooltip">Address not serviceable</div>').appendTo('body');

        list.html('');
        items = $('<table class="items"></table>').appendTo(list);
        for (var i in addresses) {
            count += 1;
            css = count % 2 == 0 ? 'odd' : 'even';
            markups.push(obj.buildItem(addresses[i], css));
        }
        items.html(markups.join(''));
        $('tr', items).click(function(e) { obj.onItemClick(e) });

        var disabledItems = $('.opt.disabled', items);
        Myntra.InitTooltip(tip, disabledItems, {side:'below', element:'input[type="radio"]'});

        if (disabledItems.length === count) {
            obj.setSubTitle('Sorry none of these addresses are serviceable');
            obj.hideSave();
        }
        else {
            obj.setSubTitle('&nbsp;');
            obj.showSave();
        }
    };

    btnCreate.click(function(e) {
        var conf = {_type:cfg._type, _action:'create', isModal:true, afterHide:$.proxy(obj.tmpShow, obj), beforeShow:$.proxy(obj.tmpHide, obj)};
        $(document).trigger('myntra.address.form.show', conf);
    });

    btnSave.click(function(e) {
        obj.hide();
        $(document).trigger('myntra.address.list.done', selectedAddress);
    });

    btnCancel.click(function(e) {
        obj.hide();
    });

    $(document).bind('myntra.address.form.done', function(e, resp) {
        if (resp._action == 'create') {
            addresses[resp.data.id] = resp.data;
            items ? obj.addItem(resp.data) : obj.buildList();
            // if we come from return wizard, just notify back and close
            if (cfg._type == 'return') {
                obj.hide();
                $(document).trigger('myntra.address.list.done', resp.data);
            }
            else {
                obj.center();
            }
        }
        else if (resp._action == 'edit') {
            obj.updateItem(resp.data);
        }
    });

    obj.addItem = function(data) {
        var firstItem = items.find('tr').first(),
            css = firstItem.hasClass('odd') ? 'even' : 'odd',
            markup = obj.buildItem(data, css),
            item = $(markup).insertBefore(firstItem);

        console.log(firstItem, item);
        // note: only serviceable address can be added. so, we can safely show the save button
        obj.showSave();
        obj.setSubTitle('&nbsp;');
        item.click(function(e){ obj.onItemClick(e) });
    };

    obj.updateItem = function(data) {
        var markup = obj.buildItem(data),
            item = $('> tr[data-id="' + data.id + '"]', items);
        item.html($(markup).html());
        item.click(function(e){ obj.onItemClick(e) });
    };

    obj.deleteItem = function(addrId) {
        var item, idx
            listItems = items.find('> tr'),
            lastIndex = listItems.length - 1;
        listItems.each(function(i, el) {
            if ($(el).attr('data-id') == addrId) {
                item = $(el);
                idx = i;
                return;
            }
        });
        if (!item) { return }
        item.animate({opacity:0}, 600, function() {
            item.remove();
            if (selectedAddress && selectedAddress.id == addrId) {
                selectedAddress = null;
                obj.disableSave();
            }
        });
    };

    obj.onItemClick = function(e) {
        var addrId = $(e.currentTarget).attr('data-id');
        switch (e.target.nodeName.toLowerCase()) {
            case 'button':
                /*
                if ($(e.target).hasClass('btn-edit')) {
                    var conf = {_type:cfg._type, _action:'edit', isModal:true, id:addrId,
                            afterHide:$.proxy(obj.tmpShow, obj), beforeShow:$.proxy(obj.tmpHide, obj)};
                    $(document).trigger('myntra.address.form.show', conf);
                }
                else if ($(e.target).hasClass('btn-delete')) {
                    var params = {_view:'address-delete', id:addrId, _token:Myntra.Data.token};
                    obj.load(params, 'delete', 'POST');
                }
                */
                break;
            case 'input':
                selectedAddress = addresses[addrId];
                obj.enableSave();
                break;
        }
    };

    obj.onAjaxSuccess = function(resp, name) {
        if (name === 'init') {
            isInited = true;
            obj.disableSave();
            cfg._type === 'return' ? btnCreate.text('Create a pickup address') : btnCreate.text('Create a shipping address');
            obj.setFootNote('Need help? ' + resp.customerSupportTime + ' on <em>' + resp.customerSupportCall + '</em>');
            if (resp.count > 0) {
                addresses = resp.addresses;
                obj.buildList();
                obj.center();
            }
            else {
                list.html('<div class="no-data">There is no address. Please create one by clicking the above link</div>');
            }
        }
        else if (name === 'delete') {
            obj.deleteItem(resp.id);
        }
    };

    var p_show = obj.show;
    obj.show = function(a_cfg) {
        $.extend(cfg, a_cfg);
        obj.setTitle('Select an address for ' + (cfg._type === 'return' ? 'pickup' : 'shipping'));
        obj.setSubTitle('&nbsp;');
        p_show.apply(obj, arguments);
        var params = {_view:'address-list', _type:cfg._type};
        !isInited && obj.ajax(params, 'init');
    };

    return obj;
};

$(document).ready(function() {
    var obj;
    $(document).bind('myntra.address.list.show', function(e, cfg) {
        obj = obj || Myntra.AddressListBox('#lb-address-list', cfg);
        obj.show(cfg);
    });
});

