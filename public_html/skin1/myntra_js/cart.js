function disableMakePaymentBlock() {
	$(".pay-now .title").after('<div class="block-overlay"></div>');
}

// Cart Page update quantity JS.
var numericReg = /(^-?\d\d*\.\d*$)|(^-?\d\d*$)|(^-?\.\d\d*$)/;
function updateTotalQuantity(prid, obj) {

	var quantity = 0;
	quantity = quantity * 1;
	if (document.getElementById(prid + "count") != null) {
		var cid = prid + "count";
		var count = document.getElementById(cid).value;
		for (var i = 0; i < count; i++) {
			var id = prid + "sizequantity" + i;
			var value = document.getElementById(id).value;
			if (numericReg.test(value)) quantity = quantity + parseInt(value);
			else {
				document.getElementById(id).value = "";
			}
		}
	} else {
		quantity = document.getElementById(prid + "qty").value;
	}
	if (!numericReg.test(quantity) || quantity == '' || quantity == 0) {
		alert("Quantity should atleast be one.");
		obj.value = 1;
		obj.focus();
		document.getElementById(prid + "qty").value = obj.value;
		updated = false;
	} else {
		document.getElementById(prid + "qty").value = quantity;
		// alert('please click on the update button to change the total amount
		// as per new quantity.');
		updated = false;
	}
	$('#cartform_' + prid).submit();
}

/*
$(".quantity-box").keypress(function(event){
	if($.browser.msie){
		var key = event.charCode || event.keyCode || 0;
		if(key == 13){
			$(this).trigger("change");
		}
	}	
});
*/

function updateTotalQuantityNoDialog(prid, obj) {
	if ($(obj).val() == "") {
		$(obj).val(0);
	}
	var quantity = 0;
	quantity = quantity * 1;
	if (document.getElementById(prid + "count") != null) {
		var cid = prid + "count";
		var count = document.getElementById(cid).value;
		for (var i = 0; i < count; i++) {
			var id = prid + "sizequantity" + i;
			var value = document.getElementById(id).value;
			if (numericReg.test(value)) quantity = quantity + parseInt(value);
			else {
				document.getElementById(id).value = "";
			}
		}
	} else {
		quantity = document.getElementById(prid + "qty").value;
	}{
		document.getElementById(prid + "qty").value = quantity;
		// alert('please click on the update button to change the total amount
		// as per new quantity.');
		updated = false;
	}
	$('#cartform_' + prid).submit();
}

$(document).ready(function() {
	
	var newCartCreatedTime = Myntra.Utils.Cookie.get("ncc");
	if(typeof newCartCreatedTime != "undefined" && newCartCreatedTime != "null"){
		_gaq.push(['_trackEvent', 'new_cart_created', newCartCreatedTime, null, null, true]);
	}
	Myntra.Utils.Cookie.del("ncc");
	
	Myntra.InitTooltip('.tooltip-coupons', '.tiplink', {
		side: 'below'
	});
	Myntra.InitTooltipFromTitle($('.cart-discount-exclusion-tt'));
	$('#loginFromCart').click(function(event) {
		$('html, body').animate({
			scrollTop: 0
		},
		'slow');
		$(document).trigger('myntra.login.show', {
			invoke: 'cart'
		});
		event.stopPropagation();
	});

	$('.quantity-box').keydown(function(event) {
		var key = event.charCode || event.keyCode || 0;
		return (
		key == 8 || key == 9 || key == 46 || key == 13 || (key >= 48 && key <= 57)) && (!event.shiftKey);
	});

	$(".quantity").focus(function() {
		$(".make-payment-link").css("opacity", "0.3");
		$(".make-payment-link").css("cursor", "not-allowed");
		$(this).css({
			border: '2px solid black',
			background: "#eee"
		});
	});

	$(".quantity").blur(function() {
		$(".make-payment-link").removeAttr("style");
		$(this).removeAttr("style");
	});
        
        Myntra.InitCartComboOverlay();
        
});



Myntra.InitCartComboOverlay = function(){        
	$(".combo-completion-btn").live('click', function(){
                //get all styleIds and pass it to 
        	var discountId=$(this).attr("discountId");
                var min=$(this).attr("min");
                var ctype=$(this).attr("ctype");
                var isConditionMet=$(this).attr("isConditionMet");
		if(typeof discountId != "undefined" || discountId != ""){
			Myntra.CartComboOverlay.loadOverlay(discountId, isConditionMet, min, ctype);
        	}
	});
}

Myntra.CartComboOverlay = (function(){
    var obj = {};
    var lb;
    
    obj.loadOverlay = function(id, isConditionMet,  min, ctype){
    	lb = Myntra.LightBox('#cart-combo-overlay');
    	obj.getData(id, isConditionMet,  min, ctype);
    };
    
    obj.getData = function(id, isConditionMet,  min, ctype){
    	lb.show();
    	lb.clearPopupContent();
    	lb.showLoading();
        $.get( http_loc+"/getCartComboOverlay.php?id="+id+"&icm="+isConditionMet+"&m="+min+"&ct="+ctype, null, function(data){
    		lb.hideLoading();
                $("#cart-combo-overlay .bd").html(data);
    			lb.center();
                Myntra.Combo.ComboOverlay.Init();
    	});
    };
    
    return obj;
})();

