$(document).ready(function(){
	Myntra.Utils.showAdminStyleDetails();
	function ddmenu_show(){ $(this).addClass("hover");$(this).find("li.f2").removeClass("active");$(this).find("li.f2:first").addClass("active"); $(this).find(".subnav").removeClass('hide').addClass('show'); }
	function ddmenu_hide(){ $(this).removeClass("hover");$(this).find(".subnav").removeClass('show').addClass('hide'); }

		/* Mega Menu JS - Start */

		// top nav menu hover intent plugin integration. This plugin check whether
		// the user intent is to hover on the
		// dropdown items or next menu item by using delays.
		$("#topnav li.f1").hoverIntent({
			interval: 150, // milliseconds delay before onMouseOver
			over: ddmenu_show,
			timeout: 150, // milliseconds delay before onMouseOut
			out: ddmenu_hide
		});

		$(".menu-bar .menu-bar-item").removeClass("no-js");

		$(".menu-bar .menu-list").hoverIntent({
			interval: 150, // milliseconds delay before onMouseOver
			// over: ddmenu_show,
			over: function(){
				if($(".category-listing-sliding").length){
					$(".category-listing-sliding").fadeTo('fast', 0.1);
				}
			},
			timeout: 150, // milliseconds delay before onMouseOut
			// out: ddmenu_hide
			out: function(){
				if($(".category-listing-sliding").length){
				 		$(".category-listing-sliding").fadeTo('fast', 1);
				}
			}
		});

	    $('#topnav .subnav li.f2').mouseover(function(){
	    	$('#topnav .subnav li').removeClass("active");
	        $(this).addClass("active");
	    });
	    /* Mega Menu JS - End */

	    $('.subnav a, .nav3-toplevel a').click(function(){
	    	if(typeof $(this).attr("href") != "undefined"){
	    		_gaq.push(['_trackEvent', 'megamenu', Myntra.Data.pageName, $(this).attr("href")]);
	    	}
	    });

    $(".scrollable").scrollable({size:5,keyboard:false,clickable:false});
    // Fix for scrollable to disable next button on initial load if the number
    // of items is exactly equal to the size of the window
    $(".scrollable-box").each(function(){
        if($(".items .item",this).length == 5){
            $(".nextPage",this).addClass("disabled");
        }
    });


    /* signup box */
    $(".search_page_menu_box, .search_page_menu_search").keydown(function(event){
    	if(event.keyCode == '13'){
    		event.preventDefault();
    		$(".search_page_menu_search").trigger("click");
    	}
    });

    $(".search, #menu_search").keydown(function(event){
    	if(event.keyCode == '13'){
    		event.preventDefault();
    		$("#menu_search").trigger("click");
    	}
    });


    /*
    $('.popup-trigger').live('click',function(){$('.span-pop-up').show();});
    $('.span-pop-up .close-button').live('click',function(){$('.span-pop-up').hide();});
    */

    $("#logout_from_account").click(function(){
        $("#toplogoutform").trigger("submit");
    });


});

// Footer Testimonials Slider
if($('#testimonials-slide').length){
	$('#testimonials-slide').cycle({
	    fx:    'fade',
	    speed: 350,
	    timeout:  12500,
	    prev:    '#prev',
	    next:    '#next'
	});
}

function afterOnLoad(){
	trackingOmniture();
	tracking();
	loadImageQueue();
	FBInit();
	availOnLoad();
}

function trackingOmniture(){
	if(typeof(s_analytics_firesync) != "undefined" && s_analytics_firesync) {
		// Sync call already triggered. Check analytics_data_collection.. 
		return;
	}
	
	// Omniture page call. Using s_analytics as s is getting unset by some piece of code and is not visible here
	if(typeof(s_analytics) != "undefined"){
		s_analytics.t();
	} 
}

/*
 * Function to contain all analytics code that needs to be executed on each page
 * load
 */
function tracking(){
	// GA tracking page load call
	var ga = document.createElement('script');
	ga.type = 'text/javascript';
	ga.async = true;
	ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	var s = document.getElementsByTagName('script')[0];
	s.parentNode.insertBefore(ga, s);
	// Mediamind page load call
	var mm = document.createElement('script');
	mm.type = 'text/javascript';
	mm.async = true;
	var mmsrc = "";
	if('https:' == document.location.protocol){
		mmsrc = "HTTPS://bs.serving-sys.com/BurstingPipe/ActivityServer.bs?cn=as&ActivityID=112557&rnd=" + ebRand;
	}
	else{
		mmsrc = "HTTP://bs.serving-sys.com/BurstingPipe/ActivityServer.bs?cn=as&ActivityID=112557&rnd=" + ebRand;
	}
	mm.src = mmsrc;
	var s = document.getElementsByTagName('script')[0];
	s.parentNode.insertBefore(mm, s);

    //User Tracking Pixel Code for tyroo
    trackTyrooAdNetwork();
       if(enableMyThings){
    	   $.browser.chrome = /chrome/.test(navigator.userAgent.toLowerCase());
 	   if(($.browser.chrome) || (($.browser.mozilla) && (parseInt($.browser.version,10)>=2)) || (($.browser.msie)&&(parseInt($.browser.version,10)>=6)))
 	   {_triggerMyThings();}
    }

}

function loadImageQueue(){
    var lazy_images = $('.lazy');
    $.each(lazy_images, function() {
    	$(this).attr("src", $(this).attr("lazy-src"));
    	$(this).removeAttr("lazy-src");
    	$(this).removeClass("lazy");
    });
}

function FBInit() {
    window.fbAsyncInit = function() {
      FB.init({
        appId      : facebook_app_id,
        status     : true,
        cookie     : true,
        xfbml      : true,
        oauth      : true
      });
    };
    (function(d){
       var js, id = 'facebook-jssdk'; if (d.getElementById(id)) {return;}
       js = d.createElement('script'); js.id = id; js.async = true;
       js.src = "//connect.facebook.net/en_US/all.js";
       d.getElementsByTagName('head')[0].appendChild(js);
     }(document));
}


function trackTyrooAdNetwork(){
    $('body').append('<img src="http://ads1.tyroo.com/utrack/3.0/1067/0/0/0/BeaconId=10520;rettype=img;subnid=1;" width="1" height="1" style="display:none">');
}


$(window).load(function(){
	$(".category .item a, .product-listing .item a").live('click',function(){
	    window.location=$(this).attr("href"); return false;
	});

	$("#menu_search").click(function(){
		menuSearchFormSubmit();
	});

	$('.cb').live('hover',function(event){
    	showCashbackToolTip($(this).closest(".item-box"), $('.cbtip'));
        $(this).css("color","red");
    });

    $('.cb').live('mouseout',function(event){
    	$('.cbtip').hide();
        $(this).css("color","#333333");
    });

});

function showCashbackToolTip(parentElem, cbElem){
	var parentElemOffset=parentElem.offset();
	cbElem.css({
		"top":parentElemOffset.top+150,
		"left":parentElemOffset.left+7
	});
	cbElem.show();
}


$(document).ready(function() {
    // Footer login/register link hide and show
    if(Myntra.Data.userEmail != ''){//looged in
        $('.foot-login').hide();
    }else{
        $('.foot-login').show();
    }
});


function menuSearchFormSubmit()
{
    var str=$.trim($("#search_query").val());
    if(str == searchBoxText){
        return false;
    }
	if(str != '')
    {
        _gaq.push(['_trackEvent', 'search_button', Myntra.Data.pageName, str]);
        str = str.replace(/\s+/g, '-').replace(/\//g, '__');
        window.location=http_loc+"/"+encodeURIComponent(str)+"?userQuery=true";
    }
    else{
        return false;
    }
}
function formSubmitNoResults()
{
        var str=$.trim($("#query_nores").val());
        var str=str.replace(" ","-");
        if(str != '')
        {
        		_gaq.push(['_trackEvent', 'search_button', 'search_no_result',str]);
                $("#searchformnoresults").attr("action",http_loc+"/"+str);
                $("#searchformnoresults").trigger('submit');
        }
        else
                return false;
}



// Homepage Jersey Widget
$(document).ready(function() {
    /* Toggle Text */
    toggle_text_all(".toggle-text");
});
var toggle_text_elements = {};

    /*
	 * This function finds all HTML elements with "toggle-text" class and binds
	 * the "focus"/"blur" events for toggling the default text in those
	 * elements.
	 */
    function toggle_text_all(selector) {
        $(selector).each(function(e) {
            toggle_text_elements[this.id] = $(this).val();
            addToggleText(this);
        });
    }

    function addToggleText(elem) {
        $(elem).addClass("blurred");
        $(elem).focus(function() {
            if ($(this).val() == toggle_text_elements[this.id]) {
                $(this).val("");
                $(this).removeClass("blurred");
            }
        }).blur(function() {
            if ($(this).val() == "") {
                $(this).addClass("blurred");
                $(this).val(toggle_text_elements[this.id]);
            }
        });
    }

	function gaTrackEvent(category, action, label){
		_gaq.push(['_trackEvent', category, action, label]);
		return true;
	}

	function gaTrackPageView(url){
		_gaq.push(['_trackPageview', url]);
		return true;
	}

	function gaTrackEventAjax(category, action, label){
		_gaq.push(['_trackEvent', category, action, label]);
		return false;
	}

	function gaTrackPageViewAjax(url){
		_gaq.push(['_trackPageview', url]);
		return false;
	}


function _mt_ready(){
   if (typeof(MyThings) != "undefined") {
       MyThings.Track({
           EventType: MyThings.Event.Visit,
               Action: "300"
       });
   }
}
function _triggerMyThings()
{
	var mtHost = (("https:" == document.location.protocol) ? "https" : "http") + "://rainbow-in.mythings.com";
	var mtAdvertiserToken = "1428-100-in";
	var nurl=mtHost+'/c.aspx?atok='+mtAdvertiserToken;
    $.getScript(nurl);
}

//rececent widget size selction
jQuery(document).ready(function($){
	if($(".new-scrollable").length){
		$(".new-scrollable").scrollable({size:3,keyboard:false,clickable:false});
		if($(".new-scrollable ul").children("li").length<=3){$(".recent-cont .nextPage").addClass("disabled");}
		else {$(".recent-cont .nextPage").removeClass("disabled");}
	}
	if($(".new-single-scrollable").length){
		$(".new-single-scrollable").scrollable({size:1,keyboard:false,clickable:false});
	}
	$(".single-scroll").click(function(){
		var listname = $(this).attr("id");
		var items_to_be_loaded=$(".items_"+listname+" li.item a.to-be-loaded");
		// to load the next four items in the list onclick of the next button in
		// the slider
		if(items_to_be_loaded.length){
			for(i=0;i<items_to_be_loaded.length;i++){
				$("img", items_to_be_loaded[i]).attr("src", lists_image_srcs[listname][$("img", items_to_be_loaded[i]).attr("id")]);
				$(items_to_be_loaded[i]).removeClass("to-be-loaded").addClass("loaded");
				if(i==3){break;}
			}
		}


	});


$('.recent-viewed-size-loader').live('mouseenter', function(){
	$(this).removeClass('recent-viewed-size-loader');
	$(this).find('.widget-size-select').attr('disabled','disabled');
	var objId=$(this).find('.widget-size-select').attr("id");
	styleId=objId.substring(objId.indexOf("-")+1,objId.length);
	objId="select#"+objId;
	if($(objId+" option:selected").val()==''){
		var rURL=http_loc+"/recentwidget_getsizes.php?ids="+styleId;
		$.ajax({
			type:"GET",
			url:rURL,
			beforeSend:function(){
			$(objId).empty().append('<option value="">loading...</option>');
			},
			success: function(data){
				$(objId).empty().append('<option value="">select size</option>');
				 result=data;
                 for(rs in result){
                     id = '#style-' + rs;
                     sizes=result[rs];
                     for(sz in sizes){
                          if (sz == 0) {
                             $(id).empty().removeClass('widget-size-select');
                             //$(id).parent().append('<div class="btn-overlay"></div>');
                         }
                         $(id).append($('<option></option>').val(sz).html(sizes[sz]));
                     }
                 }

				$(objId).removeAttr('disabled');
			}
		});
	}
});

$('.add-to-cart-button').live("click",function(){
	if($(this).parent().find("select.size-select option:selected").val()==''){
		alert('please select a size');
		return false;
	}
	else if ($(this).parent().find("select.size-select option:selected").val()==0){
        return false;
    }
	else {
		$(this).parent().find('.productSKUID').val($(this).parent().find("select.size-select option:selected").val());
		$(this).parent().find('.sizename').val($(this).parent().find("select.size-select option:selected").val());
		//$(this).parent().parent().find("form").trigger('submit');
	}

});

//to set special styles for low resolution screens
if(screen.width <= 1024 && screen.height <= 768){
	$("body").addClass("lowres");
}

$('.itemsizes').live('mouseleave',function(e){
	$(this).find('.availability').css("visibility", "hidden");
	var sizetext=$(this).find('.availability').text();
	if(sizetext != ""){
		$(this).find('.availability').html(sizetext);
	}
});

$('.itemsizes').live('mouseenter',function(e){
$(this).find('.availability').css("visibility", "visible");
    var sizelength=$(this).find('.availability').text().length;
    var sizetext=$(this).find('.availability').text();
     if(sizelength>30) {
         $(this).find('.availability').html(sizetext);
		 $(this).find('.availability').marquee("availability");
     }
 });

 $('.re-itemsizes').live('mouseleave',function(e){
	$(this).find('.availability-size').css("visibility", "hidden");
	var sizetext=$(this).find('.availability-size').text();
	if(sizetext != ""){
		$(this).find('.availability-size').html(sizetext);
	}
});

$('.re-itemsizes').live('mouseenter',function(e){
	$(this).find('.availability-size').removeAttr("style");
	$(this).find('.availability-size').css("visibility", "visible");
   var sizelength=$(this).find('.availability-size').text().length;
   var sizetext=$(this).find('.availability-size').text();
    if(sizelength>30) {
        $(this).find('.availability-size').html(sizetext);
		$(this).find('.availability-size').marquee("availability-size");
    }
});
});

//GA related calls
Myntra.GA = Myntra.GA || {};

Myntra.GA.gaTrackEventAjax = function(category, action, label){
	_gaq.push(['_trackEvent', category, action, label]);
	return false;
}

Myntra.GA.gaTrackPageViewAjax = function(url){
	_gaq.push(['_trackPageview', url]);
	return false;
}

