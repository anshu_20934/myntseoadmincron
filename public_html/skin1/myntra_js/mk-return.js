
Myntra.WizardReturnReason = function() {
    var content, wiz,
        isReset = true, reason, details, qty, lqty,
        obj = Myntra.WizardStep();
    obj.set('title', 'Reason');
    obj.set('ajax_url', '/myntra/ajax_return.php');

    obj.init = function() {
        content = obj.get('content');
        wiz = obj.get('wiz');
        obj.ajax({_view:'reason'}, 'init');
    };

    obj.update = function() {
        // update the UI only after a reset
        if (!isReset) {
            return;
        }
        var val = wiz.data('qty');
        if (val > 1) {
            var markup = ['<option value="">Select a quantity</option>'];
            for (var i = 1; i <= val; i += 1) {
                markup[markup.length] = '<option value="' + i + '">' + i + '</option>';
            }
            qty.html(markup.join(' '));
            qty.show();
            lqty.show();
        }
        else {
            qty.html('<option value="1" selected="true">1</option>');
            qty.hide();
            lqty.hide();
        }
        isReset = false;
        obj.validate();
    };

    obj.onAjaxSuccess = function(resp, name) {
        $(resp.markup).appendTo(content);
        wiz.data('amtCredit', resp.amtCredit);
        wiz.setFootNote('Need help? ' + resp.customerSupportTime + ' on <em>' + resp.customerSupportCall + '</em>');

        reason = content.find('select[name="reason"]');
        details = content.find('textarea[name="details"]');
        qty = content.find('select[name="qty"]');
        lqty = content.find('.lbl-qty');

        reason.change(function() { obj.validate(); });
        qty.change(function() { obj.validate(); });
        obj.update();
    };

    obj.reset = function() {
        reason.val('');
        details.val('');
        qty.val('');
        isReset = true;
    };

    obj.validate = function() {
        if (!reason.val()) {
            wiz.disableNext();
        }
        else if (wiz.data('qty') > 1 && !qty.val()) {
            wiz.disableNext();
        }
        else {
            wiz.enableNext();
        }
    };

    obj.next = function() {
        wiz.data('reason', reason.val());
        wiz.data('details', details.val());
        wiz.data('qty', qty.val());
        wiz.notify('next');
    };

    return obj;
};


Myntra.WizardReturnAgree = function() {
    var content, wiz, agree, items,
        obj = Myntra.WizardStep();
    obj.set('title', 'Policy Agreement');
    obj.set('ajax_url', '/myntra/ajax_return.php');

    obj.init = function() {
        content = obj.get('content');
        wiz = obj.get('wiz');

        $([
        '<div class="step-agree">',
        '    <p>Please read and confirm the conditions for returning ',
        '            this item, as per <a href="/myntrareturnpolicy.php" target="_blank">Our Return Policy</a></p>',
        '   <ol class="statements">',
        '       <li>',
        '           <input type="checkbox" name="i-agree" value="1">',
        '           I agree that product being returned is unused and unwashed.',
        '       </li>',
        '       <li>',
        '           <input type="checkbox" name="i-agree" value="2">',
        '           I agree that product has all tags/stickers/accompanying material that the product was shipped with originally.',
        '       </li>',
        '       <li>',
        '           <input type="checkbox" name="i-agree" value="3">',
        '           I agree to ship the original order invoice and the packaging for the product.',
        '       </li>',
        '       <li>',
        '           <input type="checkbox" name="i-agree" value="4">',
        '           I have read and I am in agreement with all terms and conditions in ',
        '           <a href="/myntrareturnpolicy.php" target="_blank">Myntra’s Returns Policy</a>.',
        '       </li>',
        '    </ol>',
        '</div>'
        ].join('')).appendTo(content);

        agree = content.find('input[name="i-agree"]');
        items = content.find('.statements > li');
        agree.click(function() {
            var li = $(this).parent();
            li.next().length && li.next().removeClass('hide');
            obj.update();
        });
        obj.reset();
        obj.update();
    };

    obj.update = function() {
        agree.filter(':not(:checked)').length ? wiz.disableNext() : wiz.enableNext();
    };

    obj.reset = function() {
        agree.removeAttr('checked');
        items.addClass('hide').eq(0).removeClass('hide');
    };

    obj.next = function() {
        wiz.data('i-agree', 'yes');
        wiz.notify('next');
    };

    return obj;
};


Myntra.WizardReturnShipping = function() {
    var content, wiz,
        isReset = true,
        obj = Myntra.WizardStep();
    obj.set('title', 'Shipping Details');
    obj.set('ajax_url', '/myntra/ajax_return.php');

    obj.init = function() {
        content = obj.get('content');
        wiz = obj.get('wiz');

        $([
        '<div class="step-shipping">',
        '   <p class="how">How do you want to return the item?</p>',
        '   <ul>',
        '       <li>',
        '           <input type="radio" name="ship-opt" value="self" class="ship-opt-self">',
        '           <div class="ship-type">ship it yourself</div>',
        '           <div class="ship-note">get a Self-Shipping Credit of <span class="rupees">Rs.</span> 100.00</div>',
        '       </li>',
        '       <li>',
        '           <input type="radio" name="ship-opt" value="pickup" class="ship-opt-normal">',
        '           <div class="ship-type">Select an address for pickup</div>',
        '           <div class="ship-note">where serviceable</div>',
        '       </li>',
        '   </ul>',
        '</div>'
        ].join('')).appendTo(content);

        content.find(':radio').click(function() {
            wiz.enableNext();
            wiz.data('ship-opt', $(this).val());
        });

        $(document).bind('myntra.address.list.done', function(e, data) {
            wiz.data('address', data);
            wiz.notify('next');
        });

        obj.update();
    };

    obj.update = function() {
        if (!content.find(':radio').is(':checked')) {
            wiz.disableNext();
        }
    };

    obj.reset = function() {
        content.find(':radio').prop('checked', false);
    };

    obj.next = function() {
        if (wiz.data('ship-opt') == 'self') {
            wiz.notify('next');
        }
        else {
            var conf = {_type:'return', isModal:true,
                    afterHide:$.proxy(wiz.tmpShow, wiz), beforeShow:$.proxy(wiz.tmpHide, wiz)};
            $(document).trigger('myntra.address.list.show', conf);
        }
    };

    return obj;
};


Myntra.WizardReturnSummary = function() {
    var content, wiz,
    cacheRefund = {},
    obj = Myntra.WizardStep();

    obj.set('title', 'Summary');
    obj.set('ajax_url', '/myntra/ajax_return.php');

    obj.init = function() {
        content = obj.get('content');
        wiz = obj.get('wiz');
        $([
            '<div class="step-summary">',
            '   <div class="title"></div>',
            '   <div class="address"></div>',
            '   <div class="amount"></div>',
           '</div>'
        ].join('')).appendTo(content);
        obj.update();
    };

    obj.onAjaxSuccess = function(resp, name) {
        var ckey = [resp.itemId, resp.qty, resp.shipOpt].join('-');
        cacheRefund[ckey] = resp.amtRefund;
        obj.update();
    };

    var num = function(amount) {
        return Myntra.Utils.formatNumber(amount) + '.00';
    }

    obj.update = function() {
        var address = wiz.data('address'),
            addrMarkup = [],
            amtMarkup = [];

        var ckey = [wiz.data('itemId'), wiz.data('qty'), wiz.data('ship-opt')].join('-');
        if (!cacheRefund[ckey]) {
            obj.ajax({_view:'amt-refund', itemid:wiz.data('itemId'), qty:wiz.data('qty'), 'shipopt':wiz.data('ship-opt')}, 'amt-refund');
            return;
        }
        else {
            wiz.data('amtRefund', cacheRefund[ckey]);
        }

        content.find('.title').html('Return request summary (' + wiz.data('qty') + ' no.)');

        if (wiz.data('ship-opt') == 'self') {
            addrMarkup.push("Self shipping");
        }
        else {
            addrMarkup.push('<label>Pickup from</label>');
            addrMarkup.push(address.name);
            addrMarkup.push(address.address.replace("\n", "<br>"));
            address.locality && addrMarkup.push(address.locality);
            addrMarkup.push(address.city + ' - ' + address.pincode);
            addrMarkup.push(address.state + ', India');
            addrMarkup.push('<label>Mobile</label> ' + address.mobile);
        }
        content.find('.address').html(addrMarkup.join('<br>'));

        amtMarkup.push('Refund amount <span class="rupees">Rs.</span> ', num(wiz.data('amtRefund')));
        if (wiz.data('ship-opt') == 'self') {
            amtMarkup.push('<br>Includes a Self-Shipping Credit of <span class="rupees">Rs.</span> ', num(wiz.data('amtCredit')));
        }
        content.find('.amount').html(amtMarkup.join(' '));
    };

    return obj;
};


Myntra.WizardReturnConfirm = function() {
    var content, wiz,
        obj = Myntra.WizardStep();
    obj.set('title', '');
    obj.set('ajax_url', '/myntra/ajax_return.php');
    obj.set('hide_title', 1);

    obj.init = function() {
        content = obj.get('content');
        wiz = obj.get('wiz');
        obj.update();
    };
    
    obj.onAjaxError = function(resp, name) {
        content.html(resp.markup);
        wiz.notify('error');
    };
    
    obj.reset = function() {
        content.html('');
    };

    obj.onAjaxSuccess = function(resp, name) {
        content.html(resp.markup);
        wiz.notify('done');
        $(document).trigger('myntra.return.done', resp.data);
    };

    obj.update = function() {
        var params = $.extend({_view:'confirm'}, wiz.data());
        if (params.address) {
            params.addressId = params.address.id;
            delete params.address;
        }
        obj.ajax(params, 'next', 'POST');
    };

    return obj;
};


Myntra.WizardReturnBox = function(conf) {
    var cfg = conf.cfg || {};
    cfg.css = 'returnbox';

    var id = '#lb-item-return',
        obj = Myntra.WizardBox(id, cfg),
        step1 = Myntra.WizardReturnReason(),
        step2 = Myntra.WizardReturnAgree(),
        step3 = Myntra.WizardReturnShipping(),
        step4 = Myntra.WizardReturnSummary();
        step5 = Myntra.WizardReturnConfirm();

    obj.add(step1);
    obj.add(step2);
    obj.add(step3);
    obj.add(step4);
    obj.add(step5);

    var p_show = obj.show;
    obj.show = function(conf) {
        p_show.apply(obj, arguments);
        obj.setTitle('Return Item');
        obj.setSubTitle(obj.data('styleName'));
    };

    return obj;
};


$(document).ready(function() {
    var obj;
    $(document).bind('myntra.return.show', function(e, cfg) {
        obj = obj || Myntra.WizardReturnBox(cfg);
        obj.show(cfg);
    });
});

