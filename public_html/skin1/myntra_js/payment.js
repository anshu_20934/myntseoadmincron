var Myntra = Myntra || {};

function formatNumber(num) {
    num = num.toFixed(2).split(".")[0];
    if (num.length <= 3) { return num; }
    var m = /(\d+)(\d{3})$/.exec(num);
    var inp = m[1];
    var last = m[2];
    var out = [];
    if (inp.length % 2 === 1) {
        out.push(inp.substr(0,1));
        inp = inp.substr(1);
    }
    var rx = /\d{2}/g;
    while (m = rx.exec(inp)) {
        out.push(m[0]);
    }
    out.push(last);
    return out.join(',');
};

$(function() {
    var $items = $('#vtab>.vNav>ul>li');
    if(total_amount==0){
    $items.click(function() {
		return false;
    }).eq(1).mouseover();
    } else {
	    $items.click(function() {
	        $items.removeClass('selected');
	        $(this).addClass('selected');
	        var index = $items.index($(this));
	        var formtoSubmit = $('#vtab>#payment_form>form').hide().eq(index).show().attr("id");
			$("#paymentFormToSubmit").val(formtoSubmit);

			$(".cart-btn").show();

			if(is_icici_gateway_up) {
				var buttonText = "<b>Pay Now</b>";
			} else {
				var buttonText = "<b>Proceed to Payment</b>";
			}

			if(citi_discount>0) {
				$("#discounts_section").text("Rs. 0");
				$(".total_charges").text(formatNumber(total_amount));
				$("#additional_discount").slideUp();
				$("#citi_discount_div").html('Avail ' + citi_discount +'% discount by paying with your Citibank <a href="#pay_by_dc" onClick="$(\'#pay_by_dc\').click();return false;">Debit Card</a>');
			}

			if(icici_discount>0) {
				$("#discounts_section").text("Rs. 0");
				$(".total_charges").text(formatNumber(total_amount));
				$("#additional_discount").slideUp();
				$("#icici_discount_div").html(icici_discount_text);
			}

			if(codCharge>0) {
				$("#cod_additional_charge").slideUp();
				$(".total_charges").text(formatNumber(total_amount));
			}
			if(formtoSubmit == 'credit_card') {
				if(is_icici_gateway_up)	showUserCardType(document.getElementById('credit_card').card_number.value);
				_gaq.push(['_trackEvent', 'payment_page', 'switch_payment_mode', 'creditcard']);
			} else if (formtoSubmit == 'net_banking') {
				// Pay with Net Banking -or- Pay with Cash Card option is selected
				buttonText = "<b>Proceed to Payment</b>";
				netBankingOptionChange();
				_gaq.push(['_trackEvent', 'payment_page', 'switch_payment_mode', 'netbanking']);
			} else if (formtoSubmit == 'debit_card') {
				if(is_icici_gateway_up) showUserCardType(document.getElementById('debit_card').card_number.value);
				_gaq.push(['_trackEvent', 'payment_page', 'switch_payment_mode', 'debitcard']);
			} else if(formtoSubmit == 'cod') {
				if(codCharge>0) {
					$("#cod_charge_section").text("Rs. " +formatNumber(codCharge)).css("color","#d20000");
					$("#cod_additional_charge").css("background-color","#f93");
					$("#cod_additional_charge").slideDown('slow',function(){
						$("#cod_additional_charge").css("background-color","#ffffff");
						$("#cod_charge_section").fadeOut('slow',function(){
							$(this).text("Rs. " +formatNumber(codCharge)).fadeIn();
						});
					});
					var finalAmount = total_amount + codCharge;
					finalAmount = finalAmount<0?0:finalAmount;
					$(".total_charges").text(formatNumber(finalAmount));
				}
				// Cash on delivery option is selected
				buttonText = "<b>Place the Order</b>";
				$(".cart-btn").hide();
				if(codErrorCode>0) {
					if(!codTabClicked){
						 _gaq.push(['_trackEvent', 'payment_page', 'cod_error_message', codErrorCode]);
						 if(codErrorCode==2){
								_gaq.push(['_trackEvent', 'payment_page', 'cod_unserviceable_zipcode', codPincode]);
						 }
					}
				}
				_gaq.push(['_trackEvent', 'payment_page', 'switch_payment_mode', 'cod']);
				codTabClicked = true;
			} else if(formtoSubmit == 'phone_payment') {
				$(".cart-btn").hide();
				_gaq.push(['_trackEvent', 'payment_page', 'switch_payment_mode', 'pay_by_phone']);
			} else if (formtoSubmit == 'emi') {
				 if(total_amount < iciciMinEMITotalAmount) $(".cart-btn.submitButton").hide();
				_gaq.push(['_trackEvent', 'payment_page', 'switch_payment_mode', 'emi']);
			} 
			
			if (formtoSubmit != 'emi') {
				$('.content-right-block .order-totals .grand-total .total_charges').html($('.content-right-block .order-totals .grand-total .total_charges').attr('data-original-value'));
				$('.content-right-block .order-totals .emi-charges').hide();
			} else if ($('form#emi .emi-duration input[checked]').length) {
				var row = $('form#emi .emi-duration  input[checked]').closest('tr');
		    	$('.content-right-block .order-totals .emi-charges').show().find('.value').html(row.find('.fee').html());
		    	$('.content-right-block .order-totals .grand-total .total_charges').html(row.find('.total').html().replace('Rs. ', ''));
			}

			$(".submitButton").html(buttonText);

	    }).eq(1).mouseover();

    	if(oneClickCod)  {
    		$('#pay_by_cod').click();
    	}
    }
});

$(document).ready(function(){
	/* Toggle Text */
	toggle_text_all(".toggle-text");

    //var obj = Myntra.LightBox('#splash-mobile-verify');
       /*$('.mynt-verify-btn').click(function(){
    	   var mobinum = $('input.mobile-number').val();
    	   $('#mynt-verify-btn').addClass("disable-btn");
    	   $('#edit-btn-mobile-verify').attr("disabled", "true");
    	   $.ajax({
   			type: "POST",
   			url: "mkpaymentoptions.php",
   			data: "&mode=verify-mobile&mobile="+mobinum+"&login="+loginid,
   			success: function(data) {
           		var resp=$.trim(data);
           		if(resp == "0")	{
           			$('.mynt-success').show();
           			$('.your-number').hide();
           			$('.edit-your-number').hide();
           			$('.verify-block').hide();
           			$('.warning').hide();

           			$('#confirmCodOrderBtn').removeClass("disable-btn");
           			$('#confirmCodOrderBtn').addClass("mynt-process-btn");
           			$('#confirmCodOrderBtn').removeAttr('disabled');
           		}
           		if(resp == "2"){
           			$('.warning').hide();
         			$('.your-number').hide();
         			$('.edit-your-number').show();
         			$('.num-error').show();
         			$('.verify-block').hide();
           		}
           		if(resp == "1"){
                    obj.show();
           		}

           		$('#mynt-verify-btn').removeClass("disable-btn");
           		$('#edit-btn-mobile-verify').removeAttr('disabled');
           	},
   			error:function (xhr, ajaxOptions, thrownError){
           		 $('.warning').show();
           		$('#mynt-verify-btn').removeClass("disable-btn");
           		$('#edit-btn-mobile-verify').removeAttr('disabled');
           	}
           });



       });*/

    /*$('.ok-btn').click(function(){
          $('#splash-mobile-verify').hide();
          $('#lightbox-shim').hide();
          $('#mynt-verify-btn').addClass('disable-btn');
          $('#edit-btn-mobile-verify').attr("disabled","true");
   	   	var mobinum = $('input.mobile-number').val();
          $.ajax({
     			type: "POST",
     			url: "mkpaymentoptions.php",
     			data: "&mode=verify-mobile&mobile="+mobinum+"&login="+loginid,
     			success: function(data) {
             		var resp=$.trim(data);
             		if(resp == "0")	{
             			$('.mynt-success').show();
             			$('.your-number').hide();
             			$('.edit-your-number').hide();
             			$('.verify-block').hide();
             			$('.warning').hide();

             			$('#confirmCodOrderBtn').removeClass("disable-btn");
             			$('#confirmCodOrderBtn').addClass("mynt-process-btn");
             			$('#confirmCodOrderBtn').removeAttr('disabled');
             		}
             		if(resp == "2"){
             			$('.warning').hide();
             			$('.your-number').hide();
             			$('.edit-your-number').show();
             			$('.num-error').show();
             			$('.verify-block').hide();
             		}
             		$('#mynt-verify-btn').removeClass('disable-btn');
             		$('#edit-btn-mobile-verify').removeAttr('disabled');
             	},
     			error:function (xhr, ajaxOptions, thrownError){
             		 $('.warning').show();
             		$('#mynt-verify-btn').removeClass('disable-btn');
             		$('#edit-btn-mobile-verify').removeAttr('disabled');
             	}
             });

     });*/

    $('.cancel-payment-btn').click(function() {
    	if(paymentProcessingPopup!=null) {
    		if(checkPaymentWindow !=null) {
    			checkPaymentWindow = window.clearInterval(checkPaymentWindow);
    		}
    		if(paymentChildWindow!=null) {
    			paymentChildWindow.close();
    		}
    		paymentProcessingPopup.hide();
    		window.focus();
    		$.ajax({
    			type: "POST",
     			url: "declineLastOrder.php",
     			data: "reason_code=2",
     			success: function(data) {
     			},
     			error:function (xhr, ajaxOptions, thrownError){
     			}
    		});
    	}
    });

    $('form#emi .emi-duration table tr').live('click', function() {
    	$(this).closest('table').find('input').removeAttr('checked');
    	$(this).find('input').attr('checked','checked');
    	$('.content-right-block .order-totals .emi-charges').show().find('.value').html($(this).find('.fee').html());
    	$('.content-right-block .order-totals .grand-total .total_charges').html($(this).find('.total').html().replace('Rs. ',''));
    });

    $('form#emi select#emi-bank').live('change', function() {
    	//TODO: update hidden form field for EMI Bank here
    });
    

});

/*
 * This function finds all HTML elements with "toggle-text" class and binds the "focus"/"blur" events for
 * toggling the default text in those elements.
 */
function toggle_text_all(selector) {
    $(selector).each(function(e) {
        toggle_text_elements[this.id] = $(this).val();
        addToggleText(this);
    });
}

function addToggleText(elem) {
    $(elem).addClass("blurred");
    $(elem).focus(function() {
        if ($(this).val() == toggle_text_elements[this.id]) {
            $(this).val("");
            $(this).removeClass("blurred");
        }
    }).blur(function() {
        if ($(this).val() == "") {
            $(this).addClass("blurred");
            $(this).val(toggle_text_elements[this.id]);
        }
    });
}

function getCardType(cardNumber){
	cardNumber = cardNumber.toString();
	var tempCardNumber=cardNumber;
	tempCardNumber=cardNumber.replace(/-/g,"");
	tempCardNumber=tempCardNumber.replace(/ /g,"");
	cardNumber=tempCardNumber.valueOf();
	if(cardNumber.match(/\D/)){
		return false ;
	}
	if(cardNumber===null||cardNumber.length<13||cardNumber.search(/[a-zA-Z]+/)!=-1){
		return false;
	}
	returnValue=true;
	if(returnValue){
		var cardIdentifiers={1:{identifier:[4],numlength:[13,14,15,16]},2:{identifier:[51,52,53,54,55],numlength:[16]},3:{identifier:[34,37],numlength:[15]},4:{identifier:[300,301,302,303,304,305,36,38],numlength:[14]},5:{identifier:[502260,504433,5044339,504434,504435,504437,504645,504753,504775,504809,504817,504834,504848,504884,504973,504993,508159,600206,603123,603845,622018,508227,508192,508125,508126],numlength:[16,17,18,19]}};

		var cardType=false;
		for(ruleId in cardIdentifiers){
			var temp=cardIdentifiers[ruleId];
			if($.inArray(cardNumber.length,temp.numlength)>-1){
				for(i=0;i<temp.identifier.length;i++){
					if(cardNumber.substr(0,(temp.identifier[i]+"").length)==temp.identifier[i]){
						return ruleId;
						break;
					}
				}
				if(ruleId==5){
					cardType="others";
				}
			}
		}
		if(cardType){
			return cardType;
		}else{
			return false;
		}
	}
	return false;
}



function showUserCardType(cardNumber) {
	var cardID = getCardType(cardNumber);
	$(".cardType").removeClass("fade");
	$(".maestro-msg").hide();
	$(".ostar").show();

	if(citi_discount>0) {
		if(cardID) {
			var bin = cardNumber.substring(0,6);
			if($.inArray(bin,citiCardArray) > -1) {
				var citiDiscount = amountToDiscount*citi_discount/100;
				citiDiscount = Math.round(citiDiscount);
				$("#discounts_section").text("Rs. -" +formatNumber(citiDiscount));
				$("#additional_discount").css("background-color","#f93");
				$("#additional_discount").slideDown('slow',function(){
					$("#additional_discount").css("background-color","#ffffff");
					$("#discounts_section").fadeOut('slow',function(){
						$(this).text("Rs. -" +formatNumber(citiDiscount)).fadeIn();
					});
				});
				var finalAmount = total_amount - citiDiscount;
				finalAmount = finalAmount<0?0:finalAmount;
				$(".total_charges").text(formatNumber(finalAmount));
				$("#citi_discount_div").html("Congrats! A discount of <span style='color:#d20000'>Rs. " + formatNumber(citiDiscount) +"</span> has been applied");
			}	else {
				$("#discounts_section").text("Rs. 0");
				$(".total_charges").text(formatNumber(total_amount));
				$("#citi_discount_div").html('Avail ' + citi_discount +'% discount by paying with your Citibank <a href="#pay_by_dc" onClick="$(\'#pay_by_dc\').click();return false;">Debit Card</a>');
			}
		} else {
			$("#discounts_section").text("Rs. 0");
			$(".total_charges").text(formatNumber(total_amount));
			$("#citi_discount_div").html('Avail ' + citi_discount +'% discount by paying with your Citibank <a href="#pay_by_dc" onClick="$(\'#pay_by_dc\').click();return false;">Debit Card</a>');
		}
	}

	if(icici_discount>0) {
		if(cardID) {
			var bin = cardNumber.substring(0,6);
			if($.inArray(bin,iciciCardArray) > -1) {
				var iciciDiscount = amountToDiscount*icici_discount/100;
				iciciDiscount = Math.round(iciciDiscount);
				$("#discounts_section").text("Rs. -" +formatNumber(iciciDiscount));
				$("#additional_discount").css("background-color","#f93");
				$("#additional_discount").slideDown('slow',function(){
					$("#additional_discount").css("background-color","#ffffff");
					$("#discounts_section").fadeOut('slow',function(){
						$(this).text("Rs. -" +formatNumber(iciciDiscount)).fadeIn();
					});
				});
				var finalAmount = total_amount - iciciDiscount;
				finalAmount = finalAmount<0?0:finalAmount;
				$(".total_charges").text(formatNumber(finalAmount));
				$("#icici_discount_div").fadeOut('slow',function(){
					$("#icici_discount_div").css("background-color","#ffffcc");
					$("#icici_discount_div").html("Congrats! A discount of <span style='color:#d20000'>Rs. " + formatNumber(iciciDiscount) +"</span> has been applied").fadeIn(3000,function() {
						$("#icici_discount_div").css("background-color","#ffffff");
					});
				});
			}	else {
				$("#discounts_section").text("Rs. 0");
				$(".total_charges").text(formatNumber(total_amount));
				$("#icici_discount_div").html(icici_discount_text);
			}
		} else {
			$("#discounts_section").text("Rs. 0");
			$(".total_charges").text(formatNumber(total_amount));
			$("#icici_discount_div").html(icici_discount_text);
		}
	}




	if(cardID==1){
		//visa
		$(".master").addClass("fade");
		$(".maestro").addClass("fade");
	} else if(cardID==2){
		//masterCard
		$(".visa").addClass("fade");
		$(".maestro").addClass("fade");
	} else if(cardID==5){
		//maestro
		$(".visa").addClass("fade");
		$(".master").addClass("fade");
		$(".maestro-msg").show();
		$(".ostar").hide();
	} else if(cardID=="others"){
		$(".maestro-msg").show();
		$(".ostar").hide();
		$(".cardType").removeClass("fade");
	} else{
		$(".cardType").removeClass("fade");
	}
}


function validatePaymentForm(formID){
	if(formID=='net_banking') {
		if($("#netBanking").val() == '') {
			alert("Please select a valid Netbanking option");
			return false;
		}
	} else if (formID=='credit_card'||formID=='emi') {
		var form = document.getElementById(formID);
		var card_number = form.card_number.value;

		var cardID = getCardType(card_number);
		if(!cardID){
			alert("Please enter a valid credit card number");
			return false;
		}
		
		if(formID=='emi') {
			var bin = card_number.substring(0,6);
			if($.inArray(bin,iciciEMICardArray) > -1) {
				
			} else {
				alert("Card number provided is not eligible for EMI. Please pay using any other payment mode or try with a different card number.");
				return false;
			}
		}

		var card_month = form.card_month.value;
		if(card_month == null || card_month == '') {
			alert("Please select card expiry month");
			return false;
		}


		var card_year = form.card_year.value;
		if(card_year == null || card_year == '') {
			alert("Please select card expiry year");
			return false;
		}

		var cvv_code = form.cvv_code.value;
		if((/\D/.test(cvv_code)) || cvv_code.length < 3){
			alert("Please enter a valid card CVV number");
			return false;
		}

		var bill_name = form.bill_name.value;
		if(bill_name==null || bill_name == '' ){
			alert("Please enter the name as it appears on card");
			return false;
		}

		var b_firstname = form.b_firstname.value;

		if(b_firstname==null || b_firstname == '' || b_firstname == 'Name'){
			alert("Please enter a billing name");
			return false;
		}

		var b_address = form.b_address.value;
		if(b_address==null || b_address == '' || b_address == 'Enter your billing address here'){
			alert("Please enter a billing address");
			return false;
		}

		var b_city = form.b_city.value;
		if(b_city==null || b_city == '' || b_city == 'City'){
			alert("Please enter a billing city");
			return false;
		}

		var b_state = form.b_state.value;
		if(b_state==null || b_state == '' || b_state == 'State'){
			alert("Please enter a billing state");
			return false;
		}

		var b_zipcode = form.b_zipcode.value;
		if(b_zipcode==null || b_zipcode == '' || b_zipcode == 'PinCode' ||(/\D/.test(b_zipcode))){
			alert("Please enter a valid PinCode/ZipCode");
			return false;
		}

	} else if (formID=='debit_card') {

		var form = document.getElementById(formID);
		var card_number = form.card_number.value;
		var cardID = getCardType(card_number);
		if(!cardID){
			alert("Please enter a valid debit card number");
			return false;
		}

		if(cardID==1 || cardID == 2) {
			var card_month = form.card_month.value;
			if(card_month == null || card_month == '') {
				alert("Please select card expiry month");
				return false;
			}


			var card_year = form.card_year.value;
			if(card_year == null || card_year == '') {
				alert("Please select card expiry year");
				return false;
			}

			var cvv_code = form.cvv_code.value;
			if((/\D/.test(cvv_code)) || cvv_code.length < 3){
				alert("Please enter a valid card CVV number");
				return false;
			}
		}

		var bill_name = form.bill_name.value;
		if(bill_name==null || bill_name == '' ){
			alert("Please enter the name as it appears on card");
			return false;
		}

	} else if (formID=='cashcard') {
		return true;
	} else if(formID=='cod') {
		return true;
	}
	return true;
}


function submitPaymentForm(validate){
	var elem = $('#paymentFormToSubmit').val();
	var form = $('#'+elem);

	if(validate){
		if(validatePaymentForm(elem)){

		} else{
			return false;
		}
	}

	if(validate){
		_gaq.push(['_trackEvent', 'payment_page', 'proceed_to_pay', elem]);
	} else {
		_gaq.push(['_trackEvent', 'payment_page', 'use_intl_creditcard']);
	}
	if(total_amount>0) {
		var wdh=800;//0.75*screen.width;//800
		var hgt=400;//0.75*screen.height;//400
		var st="location=1,status=1,scrollbars=1,top=150,left=200,width="+wdh+",height="+hgt;
		paymentChildWindow=window.open('',"MyntraPayment",st);

		if(paymentChildWindow!=null){
			var html = '<html><head><title>Myntra Payments</title></head><body><div><center>';
			html = html + '<h2>Processing Payments</h2>';
			html = html + '<img src="https://d6kkp3rk1o2kk.cloudfront.net/images/image_loading.gif">';
			html = html + '<div>Enter your card/bank authentication details in the window once it appears on your screen. <br/>';
			html = html + 'Please do not refresh the page or click the back button of your browser.<br/>';
			html = html + 'You will be redirected back to order confirmation page after the authentication.<br/><br/></div>';
			html = html+ '</center></div></body></html>';
			try {
				paymentChildWindow.document.open();
				paymentChildWindow.document.write(html);
				paymentChildWindow.document.close();
			} catch (err){

			}
			paymentChildWindow.focus();
			if(checkPaymentWindow !=null) checkPaymentWindow = window.clearInterval(checkPaymentWindow);
			checkPaymentWindow = window.setInterval("checkClose()",1000);
		}
		cfg = {"isModal":true,"disableCloseButton":true};
		paymentProcessingPopup = Myntra.LightBox('#splash-payments-processing',cfg);
		paymentProcessingPopup.show();
	}
	form.submit();
	return false;
}

function checkClose() {
	if(paymentChildWindow!=null) {
		if(paymentChildWindow.closed) {
			checkPaymentWindow = window.clearInterval(checkPaymentWindow);
			$.ajax({
    			type: "POST",
     			url: "declineLastOrder.php",
     			data: "reason_code=1",
     			success: function(data) {
     				var resp=$.trim(data);
     				if(resp=="redirect") {
     					paymentProcessingPopup.hide();
     					window.focus();
     				}
     			},
     			error:function (xhr, ajaxOptions, thrownError){
     				paymentProcessingPopup.hide();
 					window.focus();
     			}
    		});
		}
	}
}

function copycontactaddress(emi) {
	if (emi) {
		$("form#emi #cc_b_firstname").val($('#customer-shipping-name').text());
		$("form#emi #cc_b_firstname").removeClass("blurred");
		$("form#emi #cc_b_address").val($('#customer-shipping-address').text());
		$("form#emi #cc_b_address").removeClass("blurred");
		$("form#emi #cc_b_city").val($('#customer-shipping-city').text());
		$("form#emi #cc_b_city").removeClass("blurred");
		$("form#emi #cc_b_state").val($('#customer-shipping-state').text());
		$("form#emi #cc_b_state").removeClass("blurred");
		$("form#emi #cc_b_zipcode").val($('#customer-shipping-pincode').text());
		$("form#emi #cc_b_zipcode").removeClass("blurred");
		$('form#emi #cc_b_country option[value=' + shipping_countrycode + ']').attr('selected', 'selected');
	} else {
		$("#cc_b_firstname").val($('#customer-shipping-name').text());
		$("#cc_b_firstname").removeClass("blurred");
		$("#cc_b_address").val($('#customer-shipping-address').text());
		$("#cc_b_address").removeClass("blurred");
		$("#cc_b_city").val($('#customer-shipping-city').text());
		$("#cc_b_city").removeClass("blurred");
		$("#cc_b_state").val($('#customer-shipping-state').text());
		$("#cc_b_state").removeClass("blurred");
		$("#cc_b_zipcode").val($('#customer-shipping-pincode').text());
		$("#cc_b_zipcode").removeClass("blurred");
		$('#cc_b_country option[value=' + shipping_countrycode + ']').attr('selected', 'selected');
	}
	_gaq.push(['_trackEvent', 'payment_page', 'copy_shipping_address', 'success']);
}

$(document).ready(function(){
	$('#captcha-form').keydown(function(event) {
		if (event.keyCode == '13') {
		event.preventDefault();
		$("#cod-verify-captcha").trigger("click");
		}
	});
});

function verifyCaptcha() {
	if($("#cod-verify-captcha").hasClass("btn-disabled")){
		return;
	}
	if($('#captcha-form').val() == ""){
		$("#cod-captcha-message").html("Please enter the text.");
		$("#cod-captcha-message").addClass("error");
		$("#cod-captcha-message").slideDown();
		return;
	}
	$("#cod-captcha-loading").show();
	$("#cod-verify-captcha").addClass("btn-disabled")
	$("#cod-captcha-message").html("").removeClass("error");
	$("#cod-captcha-message").slideUp();
	$.ajax({
		type: "POST",
		url: https_loc+"/cod_verification.php",
		data: "codloginid="+ loginid + "&condition=clickverifycaptcha&confirmmode=customerpvqueued&mobile=" + $('#cod-mobile').val() + "&userinputcaptcha=" + $('#captcha-form').val() ,
		success: function(msg){

			var resp=$.trim(msg);
			$("#cod-captcha-loading").hide();
			$("#cod-verify-captcha").removeClass("btn-disabled");
			if(resp=='CaptchaConfirmed') {
				$("#cod-verify-captcha").hide();//addClass("btn-disabled");
				$("#cod-captcha-loading").show();
				$("#cod-captcha-message").html('Code verified. Please wait while your order is being processed.');
				$("#cod-captcha-message").addClass('success');
				$("#cod-captcha-message").slideDown();
				$("#cod").trigger("submit");
				_gaq.push(['_trackEvent', 'payment_page', 'cod_captcha', 'success']);
			} else if(resp=='WrongCaptcha') {
				$("#cod-captcha-message").html('Wrong Code entered');
				$("#cod-captcha-message").addClass('error');
				$("#cod-captcha-message").slideDown();
				_gaq.push(['_trackEvent', 'payment_page', 'cod_captcha', 'failure']);
			}
		},
		error:function (xhr, ajaxOptions, thrownError) {
			$("#cod-captcha-loading").hide();
			$("#cod-verify-captcha").removeClass("btn-disabled");
			$("#cod-captcha-message").html('Wrong Code entered');
			$("#cod-captcha-message").addClass('error');
			$("#cod-captcha-message").slideDown();
		}
	});
}

/* Cart Page counpon redeem */
function redeemCoupon(){

	var couponForm = $("#couponform");
	var string = $("#othercouponcode", couponForm).val();
	var pattern = /^[a-zA-Z0-9\s]+$/g;
    if(string == '' || !pattern.test(string) || string == "Enter a Coupon") {
        alert("Please enter a valid coupon code to redeem.");
        return;
    }
    _gaq.push(['_trackEvent', 'coupon_widget', 'coupon_redeem', 'click']);
    $("#couponcode", couponForm).val(string);
    document.couponform.submit();
}
function redeemCashCoupon(){
	var userCashAmount =  parseFloat($("#userCashAmount").val());
	if(userCashAmount !='' && userCashAmount>0) {
		_gaq.push(['_trackEvent', 'coupon_widget', 'redeem_cashcoupon', 'click']);
		$("#redeemCashCouponButton").hide();
		document.cashcouponform.submit();
	}
}
function removeCoupon()
{
	_gaq.push(['_trackEvent', 'coupon_widget', 'remove_coupon', 'click']);
    var couponForm = $("#couponform");
	$("#couponcode", couponForm).val('');
    $("#removecouponcode", couponForm).val('remove');
	document.couponform.submit();
}

function removeCashCoupon()
{
	_gaq.push(['_trackEvent', 'coupon_widget', 'remove_cashcoupon', 'click']);
    	var couponForm = $("#couponform");
	$("#removecashcoupon", couponForm).val('removecoupon');
	var appliedCoupon=$("#c-code").val();
	if(appliedCoupon != ''){
	  $("#code", couponForm).val(appliedCoupon);
	}else{
		 $("#code", couponForm).val('');
	}
	document.couponform.submit();
}

$("#discount_coupon_message").hover(
  function (is) {
    $(this).find("span").find("a").attr("style","color:#FF0000");
  },
  function () {
    $(this).find("span").find("a").attr("style","color:#COCOCO");
  }
);

$("#othercouponcode").live('blur', function() {
	var coupon=$("#othercouponcode").val();
	if(coupon != '') {
		redeemCoupon();
	}
});

function clearTextArea(){
	$("#othercouponcode").val('');
}
$("#discount_cashcoupon_message").hover(
  function (is) {
    $(this).find("span").find("a").attr("style","color:#FF0000");
  },
  function () {
    $(this).find("span").find("a").attr("style","color:#COCOCO");
  }
);

$(".select-coupons li").hover(
  function () {
    $(".select_this_coupon",this).css("visibility","visible");
  },
  function () {
    $(".select_this_coupon",this).css("visibility","hidden");
  }
);

function redeemCoupon2(couponCode){
	var couponForm = $("#couponform");
	var string = couponCode;
	$("#couponcode").val(couponCode);
	var pattern = /^[a-zA-Z0-9\s]+$/g;
    if(string == '' || !pattern.test(string)) {
        alert("Please enter a valid coupon code to redeem.");
        return;
    }
    _gaq.push(['_trackEvent', 'coupon_widget', 'coupon_select', 'click']);
    document.couponform.submit();
}

function verifyMobileCommon(id)	{
	$("#verify-mobile").hide();
	$("#verify-mobile-code").slideUp();
	var profileForm=$("#change-profile");
	$("#mobile-verify-loading").show();
    var mobile = $("#mobile", profileForm).val();
    var login = $("#login", profileForm).val();
    $("#mobile",profileForm).attr("disabled","disabled");
    $.ajax({
    	type: "POST",
        url: "mymyntra.php",
        data: "&mode=check-mobile-user&mobile=" + $("#mobile", profileForm).val() + "&login=" + $("#login", profileForm).val()+"&_token="+$("#_token", profileForm).val(),
        success: function(msg){
			msg = $.trim(msg);
			if (msg == "sendSMS") {
   				$.ajax({
   			        type: "POST",
   			        url: "mymyntra.php",
   			        data: "&mode=generate-code&mobile=" + $("#mobile", profileForm).val() + "&login=" + $("#login", profileForm).val()+"&_token="+$("#_token", profileForm).val(),
   			        success: function(msg){
   						msg = $.trim(msg);
   						$("#mobile-verify-loading").hide();
   			   			if (msg == "showCaptcha")	{
	   			   			$("#v-code-note-message").hide();
	   		   				$("#v-code-note-message").removeClass("error");
	   		   				$("#v-code-note-message").text("Note: Please verify your specified contact number to be able to use any user-specific coupons that you may have been issued through Myntra. We may use this number to contact you for issues related to any of your transactions on Myntra.").fadeIn('slow');
   			   				$("#verify-mobile").attr("value","Re-Send")
   		   					$("#mobile-captcha").attr("src", https_loc+'/captcha/captcha.php?id='+id+'&rand='+Math.random());
   			   				$("#mobile-captcha-form").val("");
   			   				$("#mobile-captcha-block").show();
   			    			$(".change-password-link").hide();
   			    		} else if (msg == "verifyFailed"){
   			    			$("#verify-mobile").show();
   			    			$("#mobile",profileForm).removeAttr("disabled");
   			    			$("#verify-mobile").attr("value","VERIFY");
   			    			$("#v-code-note-message").hide();
   			   				$("#v-code-note-message").addClass("error");
   			   				$("#v-code-note-message").text("You have exceeded the maximum no. of attempts to verify this mobile number. If you have still not received the verification code or are facing any other difficulties, please provide an alternate number and retry. Or, try again after 6 hours.").fadeIn('slow');
   			    		}
   			    		else{
   			    			$("#verify-mobile").show();
   			    			$("#mobile",profileForm).removeAttr("disabled");
   			    			$("#v-code-note-message").hide();
   	   		   				$("#v-code-note-message").removeClass("error");
   	   		   				$("#v-code-note-message").text("Note: Please verify your specified contact number to be able to use any user-specific coupons that you may have been issued through Myntra. We may use this number to contact you for issues related to any of your transactions on Myntra.").fadeIn('slow');
   			    			mobileVerificationSubmitCount=0;
   			    			$("#verify-mobile").attr("value","Re-Send")
   			                $("#verify-mobile-code").slideDown();
   			    			$(".mobile-code-entry").show();
   							$("#mobile-message").html("Please enter the verification code that was sent to <b> (+91) "+ mobile +"</b>");

   							$("#mobile-message").show();
   							$("#mobile-code").val("");
   			    			$("#mobile-verification-error").removeClass("error");
   			    			$("#mobile-verification-error").text("If you have not received your verification code, please wait for atleast 2 minutes before pressing submit button");
   			    		}

   					},
   			   		error:function (xhr, ajaxOptions, thrownError) {
   			    		$("#mobile-verify-loading").hide();
   						$("#verify-mobile").show();
   						$("#mobile",profileForm).removeAttr("disabled");
   						$("#v-code-note-message").hide();
   						$("#v-code-note-message").addClass("error");
   						$("#v-code-note-message").text("An error has occured while sending the sms please retry again").fadeIn('slow');
   			   		}
   				});
   			} else if (msg=="exceed"){
   	   			//number of attempts to be tried exceeds disable the edit button and show him the message to retry after 6 hours
   	   			$("#mobile",profileForm).removeAttr("disabled");
   				$("#mobile-verify-loading").hide();
   				$("#verify-mobile").show();
   				$("#v-code-note-message").hide();
   				$("#v-code-note-message").addClass("error");
   				$("#v-code-note-message").text("You have exceeded the maximum no. of attempts to verify this mobile number. If you have still not received the verification code or are facing any other difficulties, please provide an alternate number and retry. Or, try again after 6 hours.").fadeIn('slow');

   			} else if (msg == "verified"){
   	   			//The number is already verified by some other user
   	   			$("#mobile-verify-loading").hide();
   				$("#verify-mobile").show();
   				$("#mobile",profileForm).removeAttr("disabled");
   				$("#v-code-note-message").hide();
   				$("#v-code-note-message").addClass("error");
   				$("#v-code-note-message").text("This number has been verified with us by another user. Please use another mobile number.").fadeIn('slow');
   			} else if (msg == "skipVerify"){
   				$("#mobile-verify-loading").hide();
   				$("#v-code-note-message").hide();
   				$("#verify-mobile").hide();
   				window.location.reload();

   			}else {
   	   			//Some other error has occured asked him to retry.
   	   			$("#mobile-verify-loading").hide();
   				$("#verify-mobile").show();
   				$("#mobile",profileForm).removeAttr("disabled");
   				$("#v-code-note-message").hide();
   				$("#v-code-note-message").addClass("error");
   				$("#v-code-note-message").text("An error has occured while sending the sms please retry again").fadeIn('slow');
   			}
    	},
   		error:function (xhr, ajaxOptions, thrownError) {
    		$("#mobile-verify-loading").hide();
			$("#verify-mobile").show();
			$("#mobile",profileForm).removeAttr("disabled");
			$("#v-code-note-message").hide();
			$("#v-code-note-message").addClass("error");
			$("#v-code-note-message").text("An error has occured while sending the sms please retry again").fadeIn('slow');
   		}
    });
    _gaq.push(['_trackEvent', 'coupon_widget', 'verify_mobile', mobile]);
}

function validateMobileNumber() {
	var numericReg  =  /(^[1-9]\d*$)/;
	var error = false;
	var profileForm=$("#change-profile");
	var mobileVal=$("#mobile", profileForm).val();
	$("#mobile-error", profileForm).hide();
	if(mobileVal == "" || mobileVal.length < 10 || !numericReg.test(mobileVal)){
		error=true;
		$("#mobile-error", profileForm).text("Enter a valid mobile number.").slideDown();
	}
	return error;

}



function verifyMobileCaptcha() {
	if($("#mobile-verify-captcha").hasClass("btn-disabled")){
		return;
	}
	if($('#mobile-captcha-form').val() == ""){
		$("#mobile-captcha-message").html("Please enter the text.");
		$("#mobile-captcha-message").addClass("error");
		$("#mobile-captcha-message").slideDown();
		return;
	}
	$("#mobile-captcha-loading").show();
	$("#mobile-verify-captcha").addClass("btn-disabled")
	$("#mobile-captcha-message").html("").removeClass("error");
	$("#mobile-captcha-message").slideUp();
	$.ajax({
		type: "POST",
		url: https_loc+"/mobile_verification.php",
		data: "mobile=" + $('#mobile').val() + "&userinputcaptcha=" + $('#mobile-captcha-form').val() + "&login=" + $('#mobile-login').val() + "&id=paymentpage",
		success: function(msg){

			var resp=$.trim(msg);
			$("#mobile-captcha-loading").hide();
			$("#mobile-verify-captcha").removeClass("btn-disabled");
			if(resp=='CaptchaConfirmed') {
				$("#mobile-captcha-block").hide();
				$(".verify-mobile-cart-page").trigger("click");

			} else if(resp=='WrongCaptcha') {
				$("#mobile-captcha-message").html('Wrong Code entered');
				$("#mobile-captcha-message").addClass('error');
				$("#mobile-captcha-message").slideDown();
		   }
		},
		error:function (xhr, ajaxOptions, thrownError){
			$("#mobile-captcha-loading").hide();
			$("#mobile-verify-captcha").removeClass("btn-disabled");
			$("#mobile-captcha-message").html('Internal Error Occured. Please Retry again');
			$("#mobile-captcha-message").addClass('error');
			$("#mobile-captcha-message").slideDown();
		}
	});
	_gaq.push(['_trackEvent', 'coupon_widget', 'verify_mobile_captcha', 'click']);
}

function netBankingOptionChange() {
		var selectText = $("#netBanking option:selected").text();
		selectText  = selectText.toLowerCase();
		if(selectText.indexOf('icici') != -1) {
			if(icici_discount>0) {
				var iciciDiscount = amountToDiscount*icici_discount/100;
				iciciDiscount = Math.round(iciciDiscount);
				$("#discounts_section").text("Rs. -" +formatNumber(iciciDiscount));
				$("#additional_discount").css("background-color","#f93");
				$("#additional_discount").slideDown('slow',function(){
					$("#additional_discount").css("background-color","#ffffff");
					$("#discounts_section").fadeOut('slow',function(){
						$(this).text("Rs. -" +formatNumber(iciciDiscount)).fadeIn();
					});
				});
				var finalAmount = total_amount - iciciDiscount;
				finalAmount = finalAmount<0?0:finalAmount;
				$(".total_charges").text(formatNumber(finalAmount));
				$("#icici_discount_div").fadeOut('slow',function() {
					$("#icici_discount_div").css("background-color","#ffffcc");
					$("#icici_discount_div").html("Congrats! A discount of <span style='color:#d20000'>Rs. " + formatNumber(iciciDiscount) +"</span> has been applied").fadeIn(3000,function() {
						$("#icici_discount_div").css("background-color","#ffffff");
					});
				});
			}
		} else {
			$("#discounts_section").text("Rs. 0");
			$(".total_charges").text(formatNumber(total_amount));
			$("#icici_discount_div").html(icici_discount_text);
		}
}

$(document).ready(function() {

	$('.your-coupons h2').click(function() {
		if($('ul.select-coupons').length > 0){
			if($('ul.select-coupons').css('display') == "none")	{
				_gaq.push(['_trackEvent', 'coupon_widget', 'coupon_dropdown', 'click']);
			}
			$('ul.select-coupons').slideToggle("slow");
		}
	});

	$('ul.select-coupons li').hover(
		function (){
		  $(this).addClass('couponSelect');
		},
		function (){
			$('ul.select-coupons li').removeClass('couponSelect');
		}
	);

	$("#lmore").click(function(){
		$('.lmore').toggleClass("hide");
	});

	$(".code-enter .coupon-code").keydown(function(event){
		if(event.keyCode == '13'){
			event.preventDefault();
			redeemCoupon();
		}
	});

	$('#mobile').keydown(function(event) {
		if (event.keyCode == '13') {
			event.preventDefault();
			if ($(".verify-mobile-cart-page").css("display") != "none") {
				$(".verify-mobile-cart-page").trigger("click");
			}
		}
	});

	$('#mobile-captcha-form').keydown(function(event) {
		if (event.keyCode == '13') {
		event.preventDefault();
		$("#mobile-verify-captcha").trigger("click");
		}
	});

	$(".verify-mobile-cart-page").live('click',function(){
		if(!validateMobileNumber()) {
			verifyMobileCommon("paymentpage");
		}
	});

	$(".submit-code").live('click',function(){
		var profileForm=$("#verify-mobile-code");
		var codeStr = $("#mobile-code", profileForm).val();
		if (codeStr != "") {
			$("#submit-code").hide();
			$("#code-verify-loading").show();
			$.ajax({
				type: "POST",
				url: "mkpaymentoptions.php",
				data: "&mode=verify-mobile-code&mobile-code=" +codeStr,
				success: function(data) {
					var responseObj=$.parseJSON(data);
					var status=responseObj.status;
					var message = responseObj.message;
					var num_attempts = responseObj.attempts;
					if(status==1){
						//reload the page verification was successful.
						$("#mobile-verification-error").hide();
						$("#mobile-verification-error").removeClass("error");
						$("#mobile-verification-error").text(message).fadeIn('slow');
						window.location.reload();
					} else {
						$("#submit-code").show();
						$("#code-verify-loading").hide();
						mobileVerificationSubmitCount++;
						if(mobileVerificationSubmitCount>5) {
							//hide and ask him to resend the sms.
							$("#verify-mobile-code").slideUp();
							$("#v-code-note-message").hide();
							$("#v-code-note-message").addClass("error");
							$("#v-code-note-message").text("You have exceeded the maximum no. of attempts to validate the SMS code. To try verifying your mobile again please try with a new verification code.").fadeIn('slow');
						} else {
							$("#mobile-verification-error").text(message);
							$("#mobile-verification-error").addClass("error");
						}
					}
				},
				error:function (xhr, ajaxOptions, thrownError){
					$("#submit-code").show();
					$("#code-verify-loading").hide();
					$("#mobile-verification-error").text("Internal Error occured. Please retry again.");
					$("#mobile-verification-error").addClass("error");
				}
			});
			_gaq.push(['_trackEvent', 'coupon_widget', 'submit_mobile_code', 'click']);
		}
	});
	$("#order-summary").hover(function(){
		//$(this).toggleClass('black-bg4');
		$(this).toggleClass("bgcolor");
	});
	$("#order-summary").live('click',function(){
		$("#order-summary").toggleClass('downarrow', 'uparrow');
		$("#order-cart-items").toggle(400);
		if(!orderSummaryDislplayed) {
			$("#cart-pre-loading").show();
			orderSummaryDislplayed = true;
			$.ajax({
				type: "POST",
				url: "getMyCartData.php",
				data: "",
				success: function(data) {
					 $("#cart-pre-loading").hide();
					if(data!=null) $("#order-cart-items").html(data);
				},
				error:function (xhr, ajaxOptions, thrownError){
					orderSummaryDislplayed = false;
					 $("#cart-pre-loading").hide();
				}
			});
			_gaq.push(['_trackEvent', 'payment_page', 'order_summary', 'click']);
		}

	});

	$("#phone-order-confirm").live('click',function(){
		$("#phone_payment").submit();
		_gaq.push(['_trackEvent', 'payment_page', 'pay_by_phone', 'confirm_order']);
	});
//When page loads...
    //On Click Event
    $("span.discount_option input").click(function() {
           $(".tab_content").hide();
            var activeTab = $(this).attr("class");
            $(activeTab).fadeIn();
        });

    });

// Cash on Delivery Mobile verification
$('.num-edit-btn').click(function(){
    $(this).hide();
    $('.edit-your-number').show();
    $('.your-number').hide();
    $('.verify-block').hide();
});

$('.cancel-btn').click(function(){
    $('.edit-your-number').hide();
    $('.num-error').hide();
    $('.warning').show();
    $('.num-edit-btn').show();
    $('.your-number').show();
    $('.verify-block').show();

});


$('.save-btn').click(function(){
//alert($('input.mobile-number').val());
    var mobinum = $('input.mobile-number').val();
    var numericReg  =  /(^-?\d\d*\.\d*$)|(^-?\d\d*$)|(^-?\.\d\d*$)/;

    if((mobinum == userMobileNumber) ||
    	(mobinum == '') ||
    	(!numericReg.test(mobinum)) ||
    	(mobinum.length < 10)
    	){
    		return;
    }else{

    	$('.your-number #userMobile').html(mobinum);
    	$('.verify-sms #userMobile').html(mobinum);
        $('.verify-block').show();
        $('.num-edit-btn').show();
        $('.your-number').show();
        $('.edit-your-number').hide();

        $('#mynt-verify-btn').addClass('disable-btn');
        $('#edit-btn-mobile-verify').attr('disabled','true');
        $.ajax({
			type: "POST",
			url: "mkpaymentoptions.php",
			data: "&mode=update-mobile-number&new-mobile-number="+mobinum+"&login="+loginid,
			success: function(data) {
        		var resp=$.trim(data);
        		if(resp == "0")	{
        			//prompt for verification
        			$('.num-error').hide();
        			$('.warning').show();
        		}
        		if(resp == "1"){
        			$('.warning').hide();
        			$('.num-error').show();
        		}
        		userMobileNumber = mobinum;
        		$('#mynt-verify-btn').removeClass('disable-btn');
        		$('#edit-btn-mobile-verify').removeAttr('disabled');
        	},
			error:function (xhr, ajaxOptions, thrownError){
        		 $('.warning').show();
        		 $('#mynt-verify-btn').removeClass('disable-btn');
        		 $('#edit-btn-mobile-verify').removeAttr('disabled');
        	}
        });
    }

    try { $(this).blur(); } catch (ex) {}
});


