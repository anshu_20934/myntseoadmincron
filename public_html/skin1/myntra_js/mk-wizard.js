
Myntra.WizardBox = function(id, cfg) {
    cfg = cfg || {};
    var idstr = id.substr(0, 1) === '#' ? id.substr(1) : id,
        steps = [null], currentStep,
        obj,
        markup = [
        '<div id="' + idstr + '" class="mk-lightbox wizardbox ' + cfg.css + '">',
        '<div class="mod">',
        '  <div class="hd">',
        '    <div class="title"></div>',
        '    <div class="subtitle"></div>',
        '  </div>',
        '  <div class="bd">',
        '    <ol class="step-titles"></ol>',
        '    <div class="steps-wrap"><ol class="steps"></ol></div>',
        '  </div>',
        '  <div class="ft">',
        '    <span class="note"></span>',
        '    <button class="btn normal-btn btn-prev">Previous</button>',
        '    <button class="btn normal-btn btn-next">Next</button>',
        '    <button class="btn normal-btn btn-done hide">Done</button>',
        '  </div>',
        '</div>',
        '</div>'
    ].join('');
    $('body').append(markup);
    obj = Myntra.LightBox(id, cfg);
    obj.root = $(id);

    var hd = $('.mod > .hd', obj.root),
        bd = $('.mod > .bd', obj.root),
        ft = $('.mod > .ft', obj.root),
        title = $('.mod > .hd > .title', obj.root),
        subTitle = $('.mod > .hd > .subtitle', obj.root),
        ftNote = $('.mod > .ft > .note', obj.root),
        stepTitles = $('.mod > .bd > .step-titles', obj.root),
        stepList = $('.mod > .bd > .steps-wrap > .steps', obj.root),
        btnPrev = $('.btn-prev', obj.root),
        btnNext = $('.btn-next', obj.root),
        btnDone = $('.btn-done', obj.root);

    obj.setTitle = function(markup) { title.html(markup) };
    obj.setSubTitle = function(markup) { subTitle.html(markup) };
    obj.setFootNote = function(markup) { ftNote.html(markup) };

    obj.disableNext = function() { btnNext.attr('disabled', 'true') }
    obj.enableNext = function() { btnNext.removeAttr('disabled') }

    btnPrev.click(function(e) { currentStep.item.prev() });
    btnNext.click(function(e) { currentStep.item.next() });

    btnDone.click(function(e) {
        obj.hide();
    });

    var showDoneBtn = function() {
        btnDone.removeClass('hide');
        btnPrev.addClass('hide');
        btnNext.addClass('hide');
        stepTitles.addClass('hide');
    };
    var hideAllButtons = function(){
    	btnDone.addClass('hide');
        btnPrev.addClass('hide');
        btnNext.addClass('hide');
        stepTitles.addClass('hide');
    }
    var hideDoneBtn = function() {
        btnDone.addClass('hide');
        btnPrev.removeClass('hide');
        btnNext.removeClass('hide');
        stepTitles.removeClass('hide');
    };

    obj.add = function(item) {
        var index = steps.length,
            content = $('<li class="step"></li>').appendTo(stepList),
            title = $([
            '<li><span class="ico"></span>', index, '. ', item.get('title'), '</li>'
        ].join('')).appendTo(stepTitles);

        if (item.get('hide_title')) {
            title.hide();
        }

        item.set('index', index);
        item.set('wiz', obj);
        item.set('content', content);
        steps[index] = {
            item: item,
            content: content,
            index: index,
            title: title,
            isInited: false
        };
    };

    obj.notify = function(evt, data) {
    	if (evt === 'error') {
            hideAllButtons();
        }else if (evt === 'done') {
            showDoneBtn();
        }
        else if (evt === 'next') {
            currentStep.title.addClass('completed');
            if (currentStep.index < steps.length - 1) {
                obj.move(currentStep.index + 1);
            }
        }
        else if (evt === 'prev') {
            if (currentStep.index > 1) {
                obj.move(currentStep.index - 1);
            }
        }
    };

    obj.start = function() {
        $.each(steps, function(i, step) {
            if (i === 0) { return }
            step.title.removeClass('completed selected');
            step.content.css('visibility', 'hidden');
            if (step.isInited) {
                step.item.reset();
            }
        });
        stepList.css('left', '0');
        hideDoneBtn();
        currentStep = steps[1];
        obj.move(1);
    };

    obj.move = function(index) {
        var step = steps[index];

        currentStep.content.css('visibility', 'hidden');
        currentStep.title.removeClass('selected');
        step.content.css('visibility', 'visible');
        step.title.addClass('selected');
        obj.enableNext();
        
        if (index >= currentStep.index) {
            if (!step.isInited) {
                step.item.init();
                step.isInited = true;
            }
            else {
                step.item.update();
            }
        }
        currentStep = step;
        stepList.animate({left: -step.content.position().left}, 600, 'swing', function() {
        });

        (index + 1 >= steps.length) ? btnNext.addClass('hide') : btnNext.removeClass('hide');
        (index - 1 <= 0) ? btnPrev.addClass('hide') : btnPrev.removeClass('hide');
        (index + 1 == steps.length - 1) ? btnNext.html('Submit') : btnNext.html('Next');
    };

    var p_show = obj.show;
    obj.show = function(conf) {
        $.extend(cfg, conf.cfg);
        obj.data({}).data(conf.data);
        p_show.apply(obj, arguments);
        obj.start();
    };

    return obj;
};


Myntra.WizardStep = function() {
    var obj = Myntra.MkBase();
    
    obj.init = function() {};
    obj.update = function() {};
    obj.reset = function() {};

    obj.showLoading = function() {
        obj.get('wiz').showLoading();
    };

    obj.hideLoading = function() {
        obj.get('wiz').hideLoading();
    };

    obj.next = function() {
        obj.get('wiz').notify('next');
    };
    
    obj.prev = function() {
        obj.get('wiz').notify('prev');
    };

    return obj;
}

