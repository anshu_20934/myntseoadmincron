<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
{config_load file="$skin_config"}
<html>
{include file="common_meta.tpl" }

<body id="body">

<div id="scroll_wrapper"> 
<div id="container">

{include file="mykrititop.tpl" }

<div id="wrapper" >
      {include file="breadcrumb.tpl" }
    <div id="display">

	 <div class="menu_column">&nbsp;</div>

      <div class="center_column" style='float:none;padding-left:112px;'>

        <div class="content">

          <div class="super"><p>Site Map for Name Page</p> </div>
          <div class="head"><p>&nbsp;</p></div>
          <div class="links">
		{if $condition eq 1 }
			<p>
			{section name=idx loop=$arr_display}
				<a href='{$http_location}/sitemapnamepage/{$arr_display[idx]}-0'>{$arr_display[idx]}</a>
			{/section}
			</p>
		{elseif $condition eq 2 }
			<p>
			{section name=idx loop=$arr_display}
				<a href='{$http_location}/sitemapnamepage/0-{$arr_display[idx]}'>{$arr_display[idx]}</a>
			{/section}
			</p>
		 {elseif $condition eq 3}
			  <p>All Recent searches {$string_last}-{$string_last_offset}</p>
			  <table  class="table" style="font-size:12px;width:100%;">
				<tr>
					<td style='verticle-align:top;width:50%;'>
						{section name=idx loop=$results}
							{assign var=half value=$smarty.section.idx.total/2}	
							{if $smarty.section.idx.index neq $half }
								<a href='{$http_location}/{$results[idx].name|replace:' ':'+'}/name/'>{$results[idx].name}</a>&nbsp;&nbsp;	
							{else}
								</td>
								<td style='verticle-align:top;width:50%;'>
							{/if}
						{/section}
					
					</td>	
				</tr>
			  </table>
		 {/if}
		</div>
          <div class="foot"></div>         
        </div>
      </div> 
     </div>
   <div class="clearall"></div>
</div>
{include file="footer.tpl" }
</div>
</div>
</body>
</html>