
<div class="subhead">
            <p>products contained in order</p>
          </div>

	  {if $productsInCart != 0}
	  {foreach name=outer k=key item=product from=$productsInCart}
	    {foreach key=key item=item from=$product}
						{if $key eq 'productId'}
						{assign var='productId' value=$item}
						{/if}

						{if $key eq 'producTypeId'}
						{assign var='producTypeId' value=$item}
						{/if}

						{if $key eq 'productStyleId'}
						{assign var='productStyleId' value=$item}
						{/if}

						{if $key eq 'productStyleName'}
						{assign var='productStyleName' value=$item}
						{/if}

						{if $key eq 'quantity'}
						{assign var='quantity' value=$item}
						{/if}

						{if $key eq 'productPrice'}
						{assign var='productPrice' value=$item}
						{/if}

						{if $key eq 'productTypeLabel'}
						{assign var='productTypeLabel' value=$item}
						{/if}

						{if $key eq 'totalPrice'}
						{assign var='totalPrice' value=$item}
						{/if}

						{if $key eq 'optionNames'}
						{assign var='optionNames' value=$item}
						{/if}

						{if $key eq 'optionCount'}
						{assign var='optionCount' value=$item}
						{/if}

						{if $key eq 'quantity_breakup'}
						{assign var='quantity_breakup' value=$item}
						{/if}

						{if $key eq 'optionValues'}
						{assign var='optionValues' value=$item}
						{/if}

						{if $key eq 'designImagePath'}
							{include file="affiliatetemplates/reebok_ipl/cricket_jersey_cart_images.tpl" assign='designImagePath'}
						{/if}

						{if $key eq 'discount'}
						{assign var='discount' value=$item}
						{/if}

						{if $key eq 'custAreaCount'}
						{assign var='custAreaCount' value=$item}
						{/if}

						{if $key eq 'productStyleType'}
						{assign var='productStyleType' value=$item}
						{/if}
						
					{/foreach}


	  <div>
              <table cellpadding="2" class="table">

                <tr>
                  <td class="align_left" style="width:70%"><strong>Product Type:</strong>&nbsp;{$productTypeLabel}</td>
		  <td class="align_left" rowspan="4" colspan="2" >
		  {if $designImagePath != ""}
			<img src="{$designImagePath}"  class="greyborder"><br/ >
		       {if $custAreaCount gte 2}
		       		showing 1 of 2 designs&nbsp;&nbsp;<a href="#" onClick="javascript: showCustArea('popup', 0,0, '{$productId}');">View All</a>
		       {/if}
		        
		      		  
		  {else}
			<img src="{$cdn_base}/default_image.gif"   />
		   {/if}
            {if $product.promotion.promotional_offer}<span style='color:#FF0000;'>{$product.promotion.promotional_offer}</span> {/if} 
		  </td>
                </tr>

                <tr>
                  <td class="align_left" style="width:70%"><strong>Product Style:</strong>&nbsp;{$productStyleName}</td>
                </tr>
				<!--to show name and number for jerseys added by arun-->
				{if $product_custom_display_forcart.$productId.text ne ''}
					<tr>
					  <td class="align_left" style="width:70%;"><strong>Name to be printed on the back of Jersey:</strong>&nbsp;{$product_custom_display_forcart.$productId.text}</td>
					</tr>
				{/if}
				{if $product_custom_display_forcart.$productId.num ne ''}
					<tr>
					  <td class="align_left" style="width:70%;"><strong>Number to be printed on the back of Jersey:</strong>&nbsp;{$product_custom_display_forcart.$productId.num}</td>
					</tr>                
				{/if}
				<!--to show name and number for jerseys added by arun-->
<!--
		{section name=options loop=$optionNames}
                <tr>
                  	 <td class="align_left"><strong>{$optionNames[options]}:</strong>&nbsp;{$optionValues[options]}</td>

                </tr>
		{/section}

		{if $optionCount lt 3}
			{math equation="( x - y )" x=3 y=$optionCount assign="loopValue" }
			{section name=option loop=$optionCounter start=0 max=$loopValue}
			<tr>
				<td class="align_left" >&nbsp;</td>
			</tr>
			{/section}

		{/if}
-->
		
		<tr>			
			<td class="align_left" style="width:90%"><strong>Quantity Breakup:</strong>&nbsp;{$quantity_breakup}</td>
			<td class="align_left" style="width:10%">&nbsp;</td>
		</tr>

		 <tr>
			<td class="align_left" style="width:90%"><strong>Quantity:</strong>&nbsp;{$quantity}</td>
			<td class="align_left" style="width:10%">&nbsp;</td>

                </tr>

                <tr>
		{if $productStyleType == 'P'}
		  <td class="align_left" ><strong>Unit Price:</strong>&nbsp;Rs.<span>{$productPrice|string_format:"%.2f"} {if $productStyleId == '703' || $productStyleId == '702' || $productStyleId == '701'}
(Rs.{$productPrice-399|string_format:"%.2f"} + Rs.399/- customization charge){/if}</span>&nbsp;</td>
		  <td class="align_left" ><strong>Price:</strong>&nbsp;Rs.{$totalPrice|string_format:"%.2f"}</td>
		  {else}
		  <td class="align_left" ><strong>Unit Price:</strong>&nbsp;Rs.{$productPrice|string_format:"%.2f"} {if $productStyleId == '703' || $productStyleId == '702' || $productStyleId == '701'}
(Rs.{$productPrice-399|string_format:"%.2f"} + Rs.399/- customization charge){/if}</td>
		  <td class="align_left" ><strong>Price:</strong>&nbsp;Rs.{$totalPrice|string_format:"%.2f"}</td>
		 {/if}
                </tr>
              </table>
            </div>
            <div class="links"> &nbsp; </div>
           {/foreach}
	   {/if}

	{if $giftcerts != 0}
	{assign var=sno value=0}
	{foreach name=outer item=gift from=$giftcerts}
		{foreach key=key item=item from=$gift}
				{if $key eq 'purchaser'}
					{assign var='purchaser' value=$item}
				{/if}
				{if $key eq 'quantity'}
					{assign var='quantity' value=$item}
				{/if}
				{if $key eq 'recipient'}
					{assign var='recipient' value=$item}
				{/if}
				{if $key eq 'amount'}
					{assign var='amount' value=$item}
				{/if}
				{if $key eq 'totalamount'}
					{assign var='totalamount' value=$item}
				{/if}
				{if $key eq 'send_via'}
					{assign var='send_via' value=$item}
				{/if}

				{if $key eq 'recipient_email'}
					{assign var='recipient_email' value=$item}
				{/if}
				{if $key eq 'recipient_firstname'}
					{assign var='recipient_firstname' value=$item}
				{/if}

				{if $key eq 'recipient_lastname'}
					{assign var='recipient_lastname' value=$item}
				{/if}

				{if $key eq 'recipient_address'}
					{assign var='recipient_address' value=$item}
				{/if}

				{if $key eq 'recipient_city'}
					{assign var='recipient_city' value=$item}
				{/if}

				{if $key eq 'recipient_zipcode'}
					{assign var='recipient_zipcode' value=$item}
				{/if}

				{if $key eq 'recipient_county'}
					{assign var='recipient_county' value=$item}
				{/if}

				{if $key eq 'recipient_statename'}
					{assign var='recipient_statename' value=$item}
				{/if}

			{/foreach}
		<div>
              <table cellpadding="2" class="table">

                <tr>

                  <td class="align_left" style="width:70%"><strong>From:</strong>&nbsp;{$purchaser}</td>

                  <!-- <td class="align_left" rowspan="4"></td> -->
		  <td class="align_left" rowspan="3" colspan="2" >

			<img src="{$cdn_base}/skin1/images/gift.gif" alt="" />


		  </td>
                </tr>

                <tr>
                  <td class="align_left"><strong>To:</strong>&nbsp;{$recipient}</td>
                </tr>

                <tr>
			{if $send_via eq 'E'}
				<td class="align_left" ><strong>Recipient Email:</strong>&nbsp;{$recipient_email}</td>
			{/if}
			{if $send_via eq 'P'}
				<td class="align_left" ><strong>Recipient Address:</strong>&nbsp;{$recipient_firstname}&nbsp; {$recipient_lastname}<br/>{$recipient_address}<br/>{$recipient_city}<br/>{$recipient_country}</td>
			{/if}

                </tr>

		 <tr>
			<td class="align_left" style="width:90%"><strong>Quantity:</strong>&nbsp;{$quantity}</td>

                </tr>

                <tr>
		  <td class="align_left" ><strong>Unit Price:</strong>&nbsp;Rs.{$amount|string_format:"%.2f"}</td>
		  <td class="align_left" ><strong>Total Price:</strong>&nbsp;Rs.{$totalamount|string_format:"%.2f"}</td>
                </tr>
              </table>
            </div>
	    <div class="links"> &nbsp; </div>
	    {assign var=sno value=$sno+1}
	    {/foreach}
	    {/if}

          <div>
            <table cellpadding="2" class="table">

				
			     <tr class="font_bold">
				<td class="align_right" colspan="8">Amount </td>
				<td class="align_right">Rs.{$grandTotal|string_format:"%.2f"}</td>
			    </tr>

			  {if $coupondiscount != "0.00"}
			      <tr class="font_bold">
				<td class="align_right" colspan="8">Coupon discount(Coupon code: {$couponCode}({$couponpercent})):</td>
				<td class="align_right">Rs.{$coupondiscount|string_format:"%.2f"}</td>
			      </tr>
			 {/if}
			{if $posaccounttype ne 'P2'} 
			 <tr class="font_bold">
				<td class="align_right" colspan="8">VAT/CST</td>
				<td class="align_right">Rs.{$vat}</div>
				</td>
		          </tr>

			  <tr class="font_bold">
				<td class="align_right" colspan="8">Shipping Cost  </td>
				<td class="align_right"><div id="shipcost">Rs.{$shippingRate}</div></td>
			   </tr>
			{/if}

			{if $paymentoption == 'cod'}
			   <tr class="font_bold">
				<td class="align_right" colspan="8">Cash on delivery charges  </td>
				<td class="align_right"><div id="shipcost">Rs.{$cod_charges}</div></td>
			   </tr>
			{/if}

			{if $ammaDiscount != 0.00}
			   <tr class="font_bold">
				<td class="align_right" colspan="8">Redeemed points for ammas.com</td>
				<td class="align_right"><div id="shipcost">Rs.{$ammaDiscount}</div></td>
			   </tr>
			 {/if}
			 {if $giftamount != 0.00}
			   <tr class="font_bold">
				<td class="align_right" colspan="8">Gift packaging charges  </td>
				<td class="align_right"><div id="shipcost">Rs.{$giftamount}</div></td>
			   </tr>
			 {/if}
			 
			      <tr class="font_bold">
				<td class="align_right" colspan="8">Amount to be paid </td>
				<td class="align_right">Rs.{$amountafterdiscount|string_format:"%.2f"}</td>
			      </tr>
			   {if $ref_amount >0}
				 <tr class="font_bold">
					<td class="align_right" colspan="8">Referral discount </td>
					<td class="align_right">&nbsp;Rs.{$ref_amount|string_format:"%.2f"}</td>
				 </tr>
		         <tr class="font_bold">
					<td class="align_right" colspan="8">Amount after referral deduction </td>
					<td class="align_right">&nbsp;Rs.{$amtAfterRefDeducton|string_format:"%.2f"}</td>
		         </tr>	  
			  {/if}
		     {if $offers}
		     	<tr class="font_bold">
					<td class="align_right" colspan="9"><span class="mandatory">{$offers.OFFER_999}</span></td>					
		        </tr>
			{/if}
			{if $promotional_offer}
			  <tr class="font_bold">
					<td class="align_right" colspan="9"><span class="mandatory">{$promotional_offer}</span></td>					
		         </tr>
			{/if}
			
			   <tr class="font_bold">
				<td class="align_right" colspan="9">
			{if $posaccounttype eq 'P2'} 
            	<input class="submit" type="button" name="submitButton" value="view/print order details" border="0"  onClick="javascript:window.open('order_invoice.php?orderid={$orderid}&t=u','null','scrollbars=1,width=900,height=900,resizable=1');">
			{else}
            	<input class="submit" type="button" name="submitButton" value="view/print invoice" border="0"  onClick="popUpWindow('mk_final_invoice.php?orderid={$orderid}&t=u');">
			{/if}
				</td>
			   </tr>

			     {if $affiliateId}  
			    <!-- if data is to be posted on affiliate server -->
				{if $postprocess eq 'Y'}
				    
					 {assign var='psript' value=$postscript}
					 {assign var='oid' value=$orderid}
					  <script type="text/javascript" language="javascript">
					   	 postRequestonAffiliate('{$psript}',"{$oid}")
	  				 </script>
	  				  				
	  			 {/if}	
				 <tr class="font_bold">
					   <td class="align_right" colspan="9">&nbsp;</td>
				 </tr> 
				 <tr class="font_bold">
				 {if $affiliateId == '95'}
					   <td class="align_right" colspan="9"><a href='http://indyarocks.com/photos/photos_print.php'><span style="font-size:1.2em">Click here to go back to {$affiliatename}</span></a></td>
				{elseif  $affiliateId eq '379'}
					<td class="align_right" colspan="9"><a href='http://www.bollywoodhungama.com/ghajini-statue.htm'><span style="font-size:1.2em">Click here to go back to {$affiliatename}</span></a></td>
				{else}
					  <td class="align_right" colspan="9"><a href='{$referer_page}'><span style="font-size:1.2em">Click here to go back to {$affiliatename}</span></a></td>

				{/if}

				 </tr>
			  {/if}   
            </table>
          </div>
          
          <div class="foot">
  </div>

