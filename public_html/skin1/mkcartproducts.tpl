{if $orgId}<script language="javascript" type="text/javascript">var orgId='{$orgId}';</script>{/if}
{literal}
<script language="javascript">
var updated = true;

function updateCart(productId, quantity){
	if(validateQuantity(productId, quantity)){		
		document.updatecart.proHide.value = productId;
		if(synchronousRedeemCheck(orgId,document.getElementById('styleid').value,'',document.getElementById(productId+"qty").value,productId))
			return true;		
		else
			return false;
   	}else
   		return false;   
}

function isUpdated(login,pagetype)
{
	if( updated )
		return checkLoggedIn(login,pagetype);
	else
		alert("You have made changes to the cart.Please click update button");
}

function confirmDelete(productname, productid, quantity,producttype)
{
	var flag = confirm("Do you really want to delete "+productname+" "+producttype+" from your cart ?");

	if(flag == true)
	{
		document.updatecart.proHide.value = productid;
	}
	else
	{
		return flag;
	}
}


function updategcCart(index, email, name)
{

	document.updatecart.gcIndex.value = index;
	document.updatecart.rEmail.value = email;
	document.updatecart.rName.value = name;
}

function confirmGcDelete(index, email, name)
{
	var flag = confirm("Do you really want to delete the gift certificate of "+name);

	if(flag == true)
	{
		updategcCart(index, email, name);
	}
	else
	{
		return flag;
	}
}

function updateTotalQuantity(prid,obj)
{
	var quantity = 0;
	quantity = quantity*1;
	
	if(document.getElementById(prid+"count") != null){	
		var cid = prid+"count";
		var count = document.getElementById(cid).value;
		for(var i=1; i<count; i++){
			var id = prid+"sizequantity"+i;
			var value = document.getElementById(id).value;
			if( IsNumeric(value) )
				quantity = quantity + parseInt(value);
			else
			{
				document.getElementById(id).value = "";
			}
		}
	}else
		quantity = document.getElementById(prid+"qty").value;
	
	if(!IsNumeric(quantity) ||  quantity == ''){
		alert("Quantity should atleast be one.");
		obj.value = 1;
		obj.focus();
		document.getElementById(prid+"qty").value = obj.value;
		updated = false;
		return false;
	}else{		
		document.getElementById(prid+"qty").value = quantity;
		alert('please click on the update button to change the total amount as per new quantity.');
		updated = false;		
	}
}

function validateQuantity(prid, quantity){
	var elm = document.getElementById(prid + "qty");
	var qty = elm.value;
	
	if(!IsNumeric(qty) ||  qty == ''){
		alert("Please enter valid quantity.");
		elm.value = quantity;
		return false;
	}else
		return true;
}

function clearCart()
{
	var flag = confirm("Are you sure you want to clear your cart ?");

	if(flag == true)
	{
		window.location.href='clearcart.php';

	}
}
function addMore()
{
	window.location.href='mkstartshopping.php';
}
</script>
{/literal}
<form name='updatecart' action='updateProducts.php?' method='post'>
<input type=hidden id="proHide" name="proHide" >
<input type=hidden id="gcIndex" name="gcIndex" >
<input type=hidden id="rEmail" name="rEmail" >
<input type=hidden id="rName" name="rName" >
<input type=hidden id="pagetype" name="pagetype" value="{$pagetype}" >
<input type="hidden" name="ajaxStatus" id= "ajaxStatus" value="0">
<input type="hidden" name="affiliateAuthType" id= "affiliateAuthType" value="{$affiliateAuthType}">

           <div class="super">
            <p>Your shopping cart</p>
          </div>
          <div class="head">
            <p>Products in your shopping cart	&nbsp;&nbsp;
{if $affiliateId eq 107}
<input type="button" border="0" value="add more products" name="add" class="submit" onClick="window.location.href='http://www.myntra.com/sports/1'" />
{/if}
</p>
          </div>
    {assign var=tabindex value=1}
		{foreach name=outer item=product from=$productsInCart}
				{foreach key=key item=item from=$product}
						{if $key eq 'productId'}
						{assign var='productId' value=$item}
						{/if}

						{if $key eq 'producTypeId'}
						{assign var='producTypeId' value=$item}
						{/if}

						{if $key eq 'productStyleId'}
						{assign var='productStyleId' value=$item}
						{/if}

						{if $key eq 'productStyleName'}
						{assign var='productStyleName' value=$item}
						{/if}
									
						{if $key eq 'productPrice'}
						{assign var='productPrice' value=$item}
						{/if}
						
						{if $key eq 'quantity'}
						{assign var='quantity' value=$item}
						{/if}


						{if $key eq 'actualPriceOfProduct'}
						{assign var='actualPriceOfProduct' value=$item}
						{/if}

						{if $key eq 'productTypeLabel'}
						{assign var='productTypeLabel' value=$item}
						{/if}

						{if $key eq 'totalPrice'}
						{assign var='totalPrice' value=$item}
						{/if}

						{if $key eq 'optionNames'}
						{assign var='optionNames' value=$item}
						{/if}

						{if $key eq 'optionValues'}
						{assign var='optionValues' value=$item}
						{/if}

						{if $key eq 'optionCount'}
						{assign var='optionCount' value=$item}
						{/if}

						{if $key eq 'sizenames'}
						{assign var='sizenames' value=$item}
						{/if}

						{if $key eq 'sizequantities'}
						{assign var='sizevalues' value=$item}
						{/if}

						{if $key eq 'designImagePath'}
							{include file="affiliatetemplates/reebok_ipl/cricket_jersey_cart_images.tpl" assign='designImagePath'}							
						{/if}

						{if $key eq 'discount'}
						{assign var='discount' value=$item}
						{/if}

						{if $key eq 'custAreaCount'}
						{assign var='custAreaCount' value=$item}
						{/if}

						{if $key eq 'productStyleType'}
						{assign var='productStyleType' value=$item}
						{/if}

						{if $key eq 'description'}
						{assign var='description' value=$item}
						{/if}

					{/foreach}
	  <div>
              <table cellpadding="2" class="table">

                <tr>
                  <td class="align_left" style="width:85%"><strong>product type:</strong>&nbsp;{$productTypeLabel}</td>
		  <td class="align_left" rowspan="3" colspan="2" >
		   {if $designImagePath != ""}
		     {if $ie eq "true"}

				<span style="width:100px;display:inline-block;filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='{$designImagePath}', sizingMethod='scale');
">					<img src="{$designImagePath}"  alt="Design" style="
filter:progid:DXImageTransform.Microsoft.Alpha(opacity=0);"  width=100 ></span>{if $affiliateId ne '379'}<br/ >image(s) to be printed on product{/if} <br/>
                {if $custAreaCount gt 1}
                       showing 1 of 2 designs&nbsp;&nbsp;<a href="javascript: custAreaImages('popup', 0,0, '{$productId}');" >View All</a>
               {/if}
		    {else}
			 {if $orgId neq ''}
			   <img src="{$designImagePath}"  width=100 ><br/> &nbsp; &nbsp;product image <br/ >
			  {else}
			   <img src="{$designImagePath}"  width=100 >{if $affiliateId ne '379'}<br/>image(s) to be printed on product{/if} <br/ >
			 {/if}	
		       {if $custAreaCount gt 1}
		       		showing 1 of 2 designs&nbsp;&nbsp;<a href="javascript: custAreaImages('popup', 0,0, '{$productId}');" >View All</a>
		       {/if}
		   {/if}
		      {if $product.promotion.promotional_offer}<table cellpadding="0" cellspacing="0" border="0" style=""><tr><td valign="top"><img src='{$product.promotion.icon_img}' alt='promotional offer' style="border:0px;"></td><td valign="middle" style='padding-left:2px;font-size: 0.7em;color:#FF0000;'>{$product.promotion.promotional_offer}</td></tr></table> {/if}
		{else}
			<img src="{$cdn_base}/default_image.gif" width="100" height="100" />
		{/if}

		  </td>
        </tr>

		<tr>
		  <td class="align_left" style="width:85%"><strong>product style:</strong>&nbsp;{$productStyleName}</td>
		</tr>
		{if $reebok_ipl_products.$productId.text ne ''}
			<tr>
				<td class="align_left" style="width:85%;"><strong>Name to be printed on the back of Jersey:</strong>&nbsp;<b><u>{$reebok_ipl_products.$productId.text}</u></b></td>
			</tr>
		{/if}
		{if $reebok_ipl_products.$productId.num ne ''}
			<tr>
				<td class="align_left" style="width:85%;"><strong>Number to be printed on the back of Jersey:</strong>&nbsp;<b><u>{$reebok_ipl_products.$productId.num}</u></b></td>
			</tr>
		{/if}

		{if  $description }
		 <tr>
			<td class="align_left" colspan="2">
			       <b>Description</b> : {$description}
			</td>
                <tr>
		{/if}

		{if $sizenames}
                <tr><td class="align_left">
				<table>
				 {foreach from=$styletype key=key item=item}
					<tr>
					<td style="text-align:left;width:60px; font-size:0.7em;"><strong>{$item}</strong></td>
					{foreach from=$sizenames item=sizename}
                  				 <td style="text-align:center;width:60px; font-size:0.7em;">&nbsp;{$sizename}</td>
					{/foreach}
					</tr>
				{/foreach}
				</table>
                </td></tr>

				<tr><td class="align_left">
				<table><tr>
				<td style="text-align:left;width:60px;font-size:0.7em;"><strong>quantity:</strong></td>
				{assign var=i value=1}
				{foreach from=$sizevalues item=sizevalue}
                  	 <td style="text-align:center;width:60px;">
                  	 	<input type="text" name="{$productId}sizequantity[]" class="quantity" id="{$productId}sizequantity{$i}" value="{$sizevalue}" style="width:30px;" onblur="javascript:updateTotalQuantity({$productId},this);"/>
                  	 </td>
			    {assign var=i value=$i+1}
				{/foreach}
				<input type="hidden" name="{$productId}count" id="{$productId}count" value="{$i}">								
				</tr></table>
                </td></tr>
		{else}
				<tr>
					<td class="align_left"></td><td class="align_left"></td>
				</tr>
				<tr>
					<td class="align_left"></td><td class="align_left"></td>
				</tr>
		{/if}
		 <tr>
			<td class="align_left">
			{if $sizenames}
				<strong>total quantity:</strong>
				<input id="{$productId}qty"  name="{$productId}qty" type="text" class="quantity"  value="{$quantity}" maxlength="4" tabindex="{$tabindex}" style="border:0px;width:30px;" readonly/>
			{else}
				<strong>total quantity:</strong>
				<input id="{$productId}qty"  name="{$productId}qty" type="text" class="quantity"  value="{$quantity}" maxlength="4" tabindex="{$tabindex}" style="width:30px;" onblur="javascript:updateTotalQuantity({$productId},this);" />
			{/if}
				<input type="hidden" name="styleid" id="styleid" value="{$productStyleId}"/>
				<input type="submit" border="0" value=" update " name="updateBtn" class="submit" onClick="return updateCart({$productId}, {$quantity});" tabindex="{$tabindex}"/>&nbsp;
				<input type="submit" border="0" value=" remove " name="removeBtn" class="submit" onClick="return confirmDelete('{$productStyleName|escape:"quotes"}', '{$productId}', '{$quantity}','{$productTypeLabel}')" tabindex="{$tabindex}"/>
			</td>
			<td class="align_left">
				&nbsp;
			</td>
		 </tr>
                {if $accountType == 1 && ($totalPrice-$productPrice*$quantity) != 0 }
                              <tr>
				<td class="align_left" ><strong>total designer markup:</strong>&nbsp;Rs.<span >{$totalPrice-$productPrice*$quantity|string_format:"%.2f"}</span>&nbsp;</td>
                              </tr>
		{/if}
		{if $productStyleType == 'P'}

		  <td class="align_left" ><strong>unit price:</strong>&nbsp;Rs.<span >{$actualPriceOfProduct|string_format:"%.2f"} {if $productStyleId == '703' || $productStyleId == '702' || $productStyleId == '701'}
(Rs.{$productPrice-399|string_format:"%.2f"} + Rs.399/- customization charge){/if}</span>&nbsp;</td>
		   <td class="align_left" ><strong>price:</strong>&nbsp;Rs.{$totalPrice|string_format:"%.2f"}</td>
		{else}
		  <td class="align_left" ><strong>unit price:</strong>&nbsp;Rs.{$actualPriceOfProduct|string_format:"%.2f"} {if $productStyleId == '703' || $productStyleId == '702' || $productStyleId == '701'}
(Rs.{$productPrice-399|string_format:"%.2f"} + Rs.399/- customization charge){/if}</td>
		  <td class="align_left" ><strong>price:</strong>&nbsp;Rs.{$totalPrice|string_format:"%.2f"}</td>
		 {/if}

                </tr>
                {if $orgId eq ''}
                <tr>
                    <td class="align_left">
                        <strong>Type of Fulfillment</strong> &nbsp;&nbsp;<input type="radio" name="{$productId}type_fulfillment" value="Instant" checked="checked"> &nbsp;Instant
                        &nbsp;&nbsp;<input type="radio" name="{$productId}type_fulfillment" value="Delayed"> &nbsp;Delayed

                    </td>
                </tr>
                {/if}
              </table>
            </div>
            <div class="links"> &nbsp; </div>
          {assign var=tabindex value=$tabindex+1}
      {/foreach}

{*************************Start Gift Certificate*************************}
{if  $giftcerts != "" }
		{assign var=sno value=0}
	{foreach name=outer item=gift from=$giftcerts}
		{foreach key=key item=item from=$gift}
				{if $key eq 'purchaser'}
					{assign var='purchaser' value=$item}
				{/if}
				{if $key eq 'quantity'}
					{assign var='quantity' value=$item}
				{/if}
				{if $key eq 'recipient'}
					{assign var='recipient' value=$item}
				{/if}
				{if $key eq 'amount'}
					{assign var='amount' value=$item}
				{/if}
				{if $key eq 'totalamount'}
					{assign var='totalamount' value=$item}
				{/if}
				{if $key eq 'send_via'}
					{assign var='send_via' value=$item}
				{/if}

				{if $key eq 'recipient_email'}
					{assign var='recipient_email' value=$item}
				{/if}
				{if $key eq 'recipient_firstname'}
					{assign var='recipient_firstname' value=$item}
				{/if}

				{if $key eq 'recipient_lastname'}
					{assign var='recipient_lastname' value=$item}
				{/if}

				{if $key eq 'recipient_address'}
					{assign var='recipient_address' value=$item}
				{/if}

				{if $key eq 'recipient_city'}
					{assign var='recipient_city' value=$item}
				{/if}

				{if $key eq 'recipient_zipcode'}
					{assign var='recipient_zipcode' value=$item}
				{/if}

				{if $key eq 'recipient_county'}
					{assign var='recipient_county' value=$item}
				{/if}

				{if $key eq 'recipient_statename'}
					{assign var='recipient_statename' value=$item}
				{/if}

			{/foreach}

		<div>
              <table cellpadding="2" class="table">

                <tr>

                  <td class="align_left" style="width:70%"><strong>from:</strong>&nbsp;{$purchaser}</td>

                  <!-- <td class="align_left" rowspan="4"></td> -->
		  <td class="align_left" rowspan="3" colspan="2" >

			<img src="{$cdn_base}/skin1/images/gift.gif" hspace="5" vspace="5" alt="" />




		  </td>
                </tr>

                <tr>
                  <td class="align_left"><strong>to:</strong>&nbsp;{$recipient}</td>
                </tr>

                <tr>
			{if $send_via eq 'E'}
				<td class="align_left" ><strong>recipient email:</strong>&nbsp;{$recipient_email}</td>
			{/if}
			{if $send_via eq 'P'}
				<td class="align_left" ><strong>recipient address:</strong>&nbsp;{$recipient_firstname}&nbsp; {$recipient_lastname}<br/>{$recipient_address}<br/>{$recipient_city}<br/>{$recipient_country}</td>
			{/if}

                </tr>

		 <tr>
			<td class="align_left"><strong>quantity:</strong>
				 <input id="{$sno}qty"  name="{$sno}qty" type="text" class="quantity" value="{$quantity}"  maxlength="4">
				 <input type="submit" border="0" value="update quantity" name="updateGcBtn" class="submit"  onClick='updategcCart("{$sno}", "{$recipient_email}", "{$recipient}");' />
			</td>
			<td class="align_left" colspan="3">
				<input type="submit" border="0" value="remove" name="removeGcBtn" class="submit"  onClick='return confirmGcDelete("{$sno}", "{$recipient_email}", "{$recipient}")' />
			</td>
                </tr>

                <tr>
		  <td class="align_left"><strong>unit price:</strong>&nbsp;Rs.{$amount|string_format:"%.2f"}</td>
		  <td class="align_left" ><strong>total price:</strong>&nbsp;Rs.{$totalamount|string_format:"%.2f"}</td>
                </tr>
              </table>
            </div>
	    <div class="links"> &nbsp; </div>
	    {assign var=sno value=$sno+1}
		{/foreach}
{/if}
{*************************End gift certificate*************************}
	{if  $affiliateId eq '107'}
		{include file='modules/affiliates/addonipl.tpl'}
	{/if}


          <div>
            <table cellpadding="2" class="table" border="0">

				<input type="hidden" name="hidetotamount" id="hidetotamount" value="{$totalAfterDiscount}">
								<input type="hidden" name="ibibophotodiscount" id="ibibophotodiscount" value="{$ibibodiscountonphotos|string_format:"%.2f"}">

			    {if !($vat==0 && $allInclusivePricing==1)}
			    <tr class="font_bold">
					<td class="align_right" colspan="9">amount:</td>

					<td class="align_right">Rs.{$subTotal|string_format:"%.2f"}</td>
			    </tr>
				{/if}



				{if $coupondiscount != "" }
				      <tr class="font_bold">
					<td class="align_right" colspan="9">coupon discount(coupon code: {$couponCode}({$couponpercent})):</td>
					<td class="align_right">Rs.{$coupondiscount|string_format:"%.2f"}</td>
				      </tr>
				 {/if}

			     {if !($vat == 0 && ($accountType == 1 || $allInclusivePricing==1))}
			     <tr class="font_bold">
					<input type="hidden" name="hidevat" id="hidevat" value="{$vat|string_format:"%.2f"}">
					<td class="align_right" colspan="9">vat/cst:</td>
					<td class="align_right"><div id="vatdiv">Rs.{$vat|string_format:"%.2f"}</div></td>
			      </tr>
			     {/if}
			      <tr class="font_bold">
			     {if ($vat==0 && $allInclusivePricing == 1 )}
					<td class="align_right" colspan="9">Total Cost of Products</td>
			     {elseif !($vat == 0 && $accountType == 1 )}
					<td class="align_right" colspan="9">amount to be paid {if  $affiliateId ne '107'}(shipping cost extra):{/if}</td>
			     {else}
					<td class="align_right" colspan="9">amount to be paid (Inclusive of all)</td>
			     {/if}
					<td class="align_right"><div id="grandtotal">Rs.{$grandTotal|string_format:"%.2f"}</div></td>
			      </tr>
			
			     {if ($rewardPointBalance > 0)}
			     <tr class="font_bold">
					<td class="align_right" style='color:green;' colspan="9">Available Reward Points:</td>
					<td class="align_right"><div id="vatdiv" style='color:green;'>Rs.{$rewardPointBalance|string_format:"%.2f"}</div></td>
			      </tr>
			     <tr class="font_bold">
					<td class="align_right" style='color:green;' colspan="9">Points Remaining After Redemption:</td>
					<td class="align_right"><div id="vatdiv" style='color:green;'>Rs.{$remainingRewardPoints|string_format:"%.2f"}</div></td>
			      </tr>
			     {/if}
			     {if $offers}
				      <tr class="font_bold" style='color:red;'>
						<td class="align_right" colspan="10"><div id="grandtotal">{$offers.OFFER_999}</div></td>
				      </tr>
			       {/if}

			    <tr><td  colspan="10"  >&nbsp;</td></tr>

			      <tr>
			      	{if $promotion_key ne 'promotion'}
						<td>
						<!--<input type="button" border="0" value="clear your cart" name="btnClearCart" class="submit" onClick="clearCart()" />-->
						<a style='cursor:pointer;text-decoration:underline;font-size:1.2em;' onClick="clearCart()">clear your cart</a>
						</td>
					{else}
						<td>
						<!--<input type="button" border="0" value="clear your cart" name="btnClearCart" class="submit" onClick="clearCart()" />-->
						<a style='cursor:pointer;text-decoration:underline;font-size:1.2em;' href="{$http_location}/clearcart.php">clear your cart</a>
						</td>
					{/if}
					<td  colspan="9" align='right' >

					{if $orgId != ""}
						<a href="{$http_location}/modules/organization/orgBrowseAllProducts.php" title='add more products' style='font-size:1.2em;color:blue;' >add more products</a>
					{elseif $affiliateId eq '' }
							<a href="javascript:addMore();">add more products</a>					
			        {/if}

					<input type="button" border="0" value="buy now >>" name="submitButton" class="submit" style="font-weight:bold;font-size:1.2em;width:100px;background:#CB0029;cursor:pointer;" onClick="return isUpdated('{$login}', '{$pagetype}')" />
					
					</td>
			     </tr>
			      {if $ibibousersdiscount != "" || $affiliateId eq '95'}
			              <tr class="font_bold" >
						<td colspan="2">&nbsp;</td>
				      </tr>
{assign var=totalsaving value=$coupondiscount+$ibibousersdiscount}
				      <tr class="font_bold" >
					<td class="align_right" colspan="9"  style="color:red;font-size:1.2em;">Dear user, your total saving in this bill is :</td>
					<td class="align_left" style="font-size:1.2em;">Rs.{$totalsaving|string_format:"%.2f"}</td>
				      </tr>
			     {/if}
				{if $ibibodiscountonphotos != ""}
                                      <tr class="font_bold">


                                        <td class="align_right" colspan="9">discount(first 10 photo prints of 4X6 free) :</td>
                                        <td class="align_left">Rs.{$ibibodiscountonphotos|string_format:"%.2f"}</td>

                                      </tr>
                                 {/if}

				 {if $ibiboDiscountProducts != ""}
                                      <tr class="font_bold">
                                         <td class="align_right" colspan="9">discount on coffee mug/calendar:</td>
                                        <td class="align_left">Rs.{$ibiboDiscountProducts|string_format:"%.2f"}</td>
                                      </tr>
                                 {/if}
				 {if $affiliateId eq '95'}

				   <tr class="font_bold">
                                         <td class="align_right" colspan="9">coupon discount:</td>
                                        <td class="align_left">Rs.{$coupondiscount|string_format:"%.2f"}</td>
                                      </tr>

				 {/if}
            </table>
          </div>

          <div class="foot"></div>
</form>
