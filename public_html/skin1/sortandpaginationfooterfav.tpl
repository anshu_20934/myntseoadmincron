{if $allfavproducts != 'empty'}
<div class="links"> <br/>
	<div class="navigator">
		<div class="left">
			{assign var=sortparam1 value=USERRATING}
			{assign var=sortparam2 value=BESTSELLING}
			{assign var=sortparam3 value=MOSTRECENT}
			{assign var=sortparam4 value=EXPERTRATING}
			<p style="font-size: 0.9em;">
			Sort By :
			  {if $sortMethod eq MOSTRECENT}
				  <a href="javascript:sortProducts('{$sortparam4}','{$sorturl}','1','{$http_location}')">Expert Rating</a>&nbsp;|&nbsp;<a href="javascript:sortProducts('{$sortparam1}','{$sorturl}','1','{$http_location}')">User Rating</a>&nbsp;|&nbsp;<a href="javascript:sortProducts('{$sortparam2}','{$sorturl}','1','{$http_location}')">Best Selling</a>&nbsp;|&nbsp;Just Arrived
				{/if}   
				{if $sortMethod eq BESTSELLING}
				<a href="javascript:sortProducts('{$sortparam4}','{$sorturl}','1','{$http_location}')">Expert Rating</a>&nbsp;|&nbsp;<a href="javascript:sortProducts('{$sortparam1}','{$sorturl}','1','{$http_location}')">User Rating</a>&nbsp;|&nbsp;Best Selling&nbsp;|&nbsp;<a href="javascript:sortProducts('{$sortparam3}','{$sorturl}','1','{$http_location}')">Just Arrived</a>
				{/if}
				{if $sortMethod eq USERRATING}
				<a href="javascript:sortProducts('{$sortparam4}','{$sorturl}','1','{$http_location}')">Expert Rating</a>&nbsp;|&nbsp;User Rating&nbsp;|&nbsp;<a href="javascript:sortProducts('{$sortparam2}','{$sorturl}','1','{$http_location}')">Best Selling</a>&nbsp;|&nbsp;<a href="javascript:sortProducts('{$sortparam3}','{$sorturl}','1','{$http_location}')">Just Arrived</a>
				{/if}
				{if $sortMethod eq EXPERTRATING}
					Expert Rating&nbsp;|&nbsp;<a href="javascript:sortProducts('{$sortparam1}','{$sorturl}','1','{$http_location}')">User Rating</a>&nbsp;|&nbsp;<a href="javascript:sortProducts('{$sortparam2}','{$sorturl}','1','{$http_location}')">Best Selling</a>&nbsp;|&nbsp;<a href="javascript:sortProducts('{$sortparam3}','{$sorturl}','1','{$http_location}')">Just Arrived</a>
				{/if}		 
			</p>
		</div>

		<div class="right">
			<div style="font-size: 0.9em;">
				{if $favtotalpages > 1}{$favpaginator}{/if}
			</div>
		</div>
		<div class="clearall"></div>
	</div>
</div>
{/if}

<div class="foot"></div>
<div id="popupimage" name="popupimage" style="visibility:hidden;position:absolute;background:#FFFFFF;line-height:1.0;text-align:justify;z-index: 999999;">
</div>
</div>
