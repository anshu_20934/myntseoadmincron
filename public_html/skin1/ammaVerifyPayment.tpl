{* $Id: mykritihome.tpl,v 1.88.2.3 2006/07/19 10:19:35 max Exp $ *}
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
{config_load file="$skin_config"}
<html>
<head>
{include file="mkheadinfo.tpl"}
</head>
{literal}
<script type="text/javascript" language="JavaScript 1.2">
function verifyAmma(guid){
	document.verifyform.action = "https://www.ammas.com/secure/paymentgateway/verify.cfm"
	document.verifyform.submit();
}
function proceedToPayment()
{
	document.verifyform.action = "mkpaymentoptions.php"
	document.verifyform.submit();
}
</script>
{/literal}
</head>
<body id="body" >
<div id="scroll_wrapper">
<div id="container">
{include file="mykrititop.tpl" }
<div id="wrapper" >
    <div id="display">
	<div class="menu_column">{include file="leftpaneMyAccount.tpl" }</div>
		<div class="center_column">
		<form name="verifyform" action="" method="POST">
		<input type="hidden" name="ammaAmount" id="ammaAmount" value="{$ammaAmount}">
		<input type="hidden" name="GUID" id="GUID" value="{$GUID}">
		<input type="hidden" name="AmmaOrderID" id="AmmaOrderID" value="{$ammaOrderID}">
		{if $hidevat ne ''}<input type="hidden" name="hidevat" id="hidevat" value="{$hidevat}">{/if} 		
			<div class="form">
			 <!-- <div class="foot"></div>
			
          	<div class="foot"></div> -->
 
           
<div class="subhead"><p>Please verify your payment at ammas.com</p></div>
<div>
<table cellpadding="2"  class="table">

<tr class="font_bold">
	<td class="align_center" colspan="4">Transaction  Id at ammas.com</td>
	<td class="align_left" colspan="7">{$ammaOrderID}</td>
</tr>


<!--<tr class="font_bold">
<td class="align_right" colspan="5">GUID</td>
<td class="align_right" colspan="5">{$GUID}</td>
</tr> -->

 <tr class="font_bold">
	<td class="align_center" colspan="4">Amount</td>
	<td class="align_left" colspan="6">Rs.{$ammaAmount|string_format:"%.2f"}</td>
 </tr>
<!-- showing referral amount  -->

	<tr>
		<td  colspan="10" class="align_right" >
		{if $verified eq 'N'}
		 <input class="submit" type="button" id="submitButton" name="submitButton"
		 value="Verify" border="0" style="font-size:1.1em;width:100px;" onClick="verifyAmma('{$GUID}');">
		{elseif $verified eq 'Y'}
		     {literal}
		         <script language="Javascript">
		           proceedToPayment();
                         </script>
		    {/literal}
		{/if} 
	    </td>
	 </tr>
</table>
</div>

<div class="foot"></div>
</form>
	  </div>
   </div>
  </div>
<div class="clearall"></div>
</div>
{include file="footer.tpl" }
</div>
</div>

</body>
</html>
