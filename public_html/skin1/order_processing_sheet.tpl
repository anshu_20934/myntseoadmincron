{* $Id: mykritihome.tpl,v 1.88.2.3 2006/07/19 10:19:35 max Exp $ *}
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
{config_load file="$skin_config"}
<html>
{include file="mykritimeta.tpl" }
<body id="body">
<div id="scroll_wrapper">
<div id="container"> 
  <div id="wrapper"> 
    <div id="display"> 
      <div class="center_column"> 
        <div class="print">  
          
		  <div class="links"> 
            <p align="right"><input class="submit" type="submit" name="submitButton" value="print processing sheet"  border="0" onClick="javascript: window.print();"></p>
          </div>
          <div> 
            <table class="printtable">			  
			  <tr> 
                <td style="text-align:center;" colspan=3><span style="color:red;font-size:16px;"><b>Please follow the instructions provided in the instructions sheet</b></span></td>
              </tr>			
			  <tr style="background-color:F2F2F2;"> 
                <td style="text-align:center;font-size:16px;">Order#:{$orderid}</td>
				<td style="text-align:center;font-size:16px;">Queued Date:{$today}</td>
				<td style="text-align:center;font-size:16px;">Customer Name:{$customer_name}</td>
              </tr>
            </table>
          </div>
		  <div style="background-color:#b5db1b">
			<p><strong style="font-size: 15px; color: rgb(19, 16, 163);">Contact number for issues::</strong>&nbsp;&nbsp;<strong style="font-size: 15px; color: rgb(142, 4, 4);">{$order.issues_contact_number}</strong></p>
		  </div>
		  <div style="background-color:#b5db1b">
			<p>
				<strong style="font-size: 15px; color: rgb(19, 16, 163);">
					Order is to be
				</strong>
				<strong style="font-size: 15px; color: rgb(142, 4, 4);"> 
					{if $order.shipment_preferences eq 'shipped_together'}
						Shipped Together
						{else}
						Shipped as Available
					{/if}
				</strong>
				</p>
		  </div>
                   
          <div> 
            <table class="printtable">
              <tr > 
                <td style="text-align:center;background-color:FFFF00;"> 
                  <h3>{if $changestatus eq "Y"}<span>Change Request Pending.</span>{else}<span>No Change Request Pending.</span>{/if}</h3>
                </td>
                <td style="text-align:center;background-color:FF99FF;"> 
                  <h3>{if $gift_status eq "Y"}<span>Gift Wrapping Needed.</span>{else}<span>No Gift Wrapping Needed.</span>{/if}</h3>
                </td>
              </tr>
			  {if $isAffiliate eq "Y"}
			  <tr style="background-color:0066FF;"> 
                <td style="text-align:center;" colspan=2> 
                  <h3>This order is from affiliate site :&nbsp;{$affiliatename}&nbsp;</h3>
                </td>
              </tr>
			  {/if}
            </table>
          </div>
		  <div>
		  {if $productsInCart != 0}
			{section name=idx loop=$productsInCart}	
			  {assign var='productTypeLabel' value=$productsInCart[idx].productTypeLabel}
			  {assign var='productStyleName' value=$productsInCart[idx].productStyleName}
			  {assign var='productStyleType' value=$productsInCart[idx].productStyleType}
			  {assign var='allCustImagePath' value=$productsInCart[idx].allCustImagePath}
			  {assign var='quantity_breakup' value=$productsInCart[idx].quantity_breakup}
			  {assign var='optionValues' value=$productsInCart[idx].optionValues}
			  {assign var='optionNames' value=$productsInCart[idx].optionNames}
			  {assign var='productPrice' value=$productsInCart[idx].price}
			  {assign var='totalPrice' value=$productsInCart[idx].totalPrice}
			<table class="printtable" style="border: medium double rgb(0,255,0);">
              <tr > 
                <td style="text-align:center;font-size:14px;width:15%;"> 
                 Type:<b>{$productTypeLabel}</b>
                </td>
                <td style="text-align:center;font-size:14px"> 
                  Style:<b>{$productStyleName}<b>
                </td>
				<td style="text-align:center;font-size:14px"> 
                  Total Quantity:<b>{$productsInCart[idx].amount}<b>
                </td>  
              </tr>
	      {if $productsInCart[idx].quantity_breakup neq ""}
	
		       <tr>
			 <td style="text-align:right;font-size:14px" colspan=3> 
			  Quantity Breakup:<b>{$productsInCart[idx].quantity_breakup}<b>
			</td>
		       </tr>
	       {/if}  
			  <tr> 
                <td style="text-align:center;" colspan=4> 
				<table>
			    <tr>
               	    {if $allCustImagePath ne ''}
				  	{section name=img loop=$allCustImagePath}
				     <td class="align_left" rowspan="4" colspan="0" >
					 	<img src="{$allCustImagePath[img]}"  class="greyborder">
					 </td>
					{/section}	
				    {else}
					<td class="align_left" rowspan="4" colspan="0" >
					    <img src="{$cdn_base}/default_image.gif"  />
					</td>
				    {/if}
                </tr>
				</table>
                </td>
              </tr>
            </table>
			{/section}
			{/if}
		  </div>
          {if $changestatus eq "Y"}
          {include file="print_order_process_cr.tpl" }
		  {/if}
		  {if $gift_status eq "Y"}
		  <div > 
            <table style="background-color:FF99FF" class="printtable">
              <tr > 
                <td style="text-align:left;"> 
                  <h3>Gift&nbsp;Message:<br>{$giftmessage}</h3>
                </td>
              </tr>
            </table>
          </div>
		  {/if}
		  <div> 
            <table class="printtable">
              <tr> 
                <td style="text-align:left;"> 
                  Fullfilled&nbsp;By:
                </td>
                <td style="text-align:left;"> 
                  Approved&nbsp;By:
                </td>
              </tr>
            </table>
          </div>
          <div class="links"> 
            <p align="right"><input class="submit" type="submit" name="submitButton" value="print processing sheet" border="0"  onClick="javascript: window.print();"></p>
          </div>
          <br/>
        </div>
      </div>
    </div>
    <div class="clearall"></div>
  </div>
  <div id="footer"> 
    <div class="footertext">&nbsp; 
    </div>
    <div class="footercopy">Copyright&copy; Myntra. All rights reserved.&nbsp;&nbsp;&nbsp;</div>
  </div>
</div>
</div>
{include file="site/javascript.tpl"}
</body>

</html>
