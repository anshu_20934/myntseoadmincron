<div class="wrap">
				<!-- the tabs -->
					<ul class="tabs" style="_overflow:hidden;display:none">
						<li id="productsTab"><a href="#">Select Products</a></li>
					</ul>


<div class="pane" id="main_pane">
	<!--<p>#1</p> the tabs
 <div class="title-style" id="tab_title">{if $product_style_groups[0].group_name}{$product_style_groups[0].group_name}{/if}</div> -->
<ul class="tabs sp">
                                      
{section name=num loop=$product_style_groups}
    {assign var=tab_label value=$product_style_groups[num].group_name|escape:"quotes"}
    {assign var=style_group_id value=$product_style_groups[num].style_group_id}
	    {if $productStyleIconImages[$style_group_id][0]}
	        <li style="background:none"> 
	        	<a 	href="#" id="tabs_ul_li_a_{$product_style_groups[num].style_group_id}" 
	        		class="sub" onclick="$('#tab_title').html('{$tab_label}')">
	        		{$product_style_groups[num].group_name|truncate:6:"":true}
	        	</a>
	        </li>
	    {/if}
{/section}

</ul>
<!-- tab "panes" -->
{section name=num_top loop=$product_style_groups}
{assign var=style_group_id value=$product_style_groups[num_top].style_group_id }
{if $productStyleIconImages[$style_group_id][0]}
<div class="pane">
 <!-- "previous page" action -->
   <a class="prevPage browse left" style="position:relative;"></a>
    <!-- root element for scrollable -->
       <div class="scrollable">
            <!-- root element for the items -->
             <div class="items">
			 {assign var=style_id value=$product_style_groups[num_top].style_group_id}
				 {section name=num loop=$productStyleIconImages[$style_id][0]}
             <div class="item {if $productStyleIconImages[$style_id][0][num].id eq $productStyleId}active{/if}">
				    <img src="{$cdn_base}/skin1/images/spacer.gif"  id= "sp_{$productStyleIconImages[$style_id][0][num].id}"
				    	lazy-src="{$productStyleIconImages[$style_id][0][num].image_i}" class="lazy"    
				    	ALT="Choose Style  for {$productStyleIconImages[$style_id][0][num].name}" 
				    	title="{$productStyleIconImages[$style_id][0][num].name}" 
				    	onclick="location.href='{$http_location}{$currentURL}?productStyleId={$productStyleIconImages[$style_id][0][num].id}'" >
            </div>
				    
		                {/section}
            </div>
      </div>
        <!-- "next page" action -->
     <a class="nextPage browse right"  style="position:relative;"></a>
</div>
{/if}
   {/section}
 </div>


					

</div>
{literal}
	<script type="text/javascript">

	   
		function setcodsetting(codvalue) {
			$('#codsetting').val(codvalue);
		}
		function hidechartDiv()
		{
			document.getElementById('chartpopup').style.visibility = 'hidden';
		}
		function showchartDiv()
		{
		    document.getElementById('chartpopup').style.visibility = 'visible'; 
		}
		function updateTotalQuantity()
		{
			var quantity = 0;
			quantity = quantity*1;
			var count = document.getElementById('count').value;
			//alert(count);
			for(var i=1; i<count; i++){
				var id = 'sizequantity'+i;
				var value = document.getElementById(id).value;
				value = value*1;
				quantity = quantity + value;
			}
			document.getElementById("quantityMenu").value = quantity;
		}
		
		
	</script>
{/literal}
