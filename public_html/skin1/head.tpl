{* $Id: head.tpl,v 1.58 2006/03/17 08:50:44 svowl Exp $ *}

<table cellpadding="0" cellspacing="0" width="100%">
<tr> 
{*** Remove this line if you want to replace a flash movie with an image ***
	<td valign="top"><A href="{$http_location}/"><img src="{$cdn_base}/skin1/images/custom/mountain_logo.jpg" width="341" height="206" alt="" /></a></td>
*** Remove this line if you want to replace a flash movie with an image ***}
{*** Remove this part of code if you want to replace a flash movie with an image ***}
	<td valign="top">
<object type="application/x-shockwave-flash" data="{$ImagesDir}/custom/flash_logo.swf" width="341" height="206" id="flash_logo" align="top">
<param name="allowScriptAccess" value="sameDomain" />
<param name="movie" value="{$ImagesDir}/custom/flash_logo.swf" />
<param name="menu" value="false" />
<param name="loop" value="false" />
<param name="quality" value="high" />
<param name="bgcolor" value="#ffffff" />
<param name="FlashVars" value="not_home_page={if $main eq "catalog" && $current_category.category eq ""}0{else}1{/if}&amp;final_image={#FinalImage#}&amp;company_name_path={$ImagesDir}/custom/company_name.swf&amp;home_path={$http_location}/customer/home.php" />
<param name="pluginspage" value="http://www.macromedia.com/go/getflashplayer" />
<param name="swLiveConnect" value="true" />
</object>
	</td>
{*** Remove this part of code if you want to replace a flash movie with an image ***}
	<td class="BackgroundRepeatX" style="BACKGROUND-IMAGE: URL({$ImagesDir}/custom/head_bg.gif);" align="right" width="100%" valign="top">
	<table cellpadding="0" cellspacing="0" width="100%" style="BACKGROUND-IMAGE: URL({$ImagesDir}/custom/head_base.jpg);" class="BackgroundNoRepeat">
	<tr>
		<td align="right" class="HeadText" colspan="2">{if $config.Company.company_phone}{$lng.lbl_phone_1_title}: {$config.Company.company_phone}{/if}{if $config.Company.company_phone_2}&nbsp;&nbsp;&nbsp;{$lng.lbl_phone_2_title}: {$config.Company.company_phone_2}{/if}</td>
		<td><img src="{$cdn_base}/skin1/images/spacer.gif" width="10" height="20" alt="" /></td>
	</tr>
	<tr>
		<td rowspan="2"><img src="{$cdn_base}/skin1/images/spacer.gif" width="120" height="1" alt="" /></td>
		<td height="79" align="right">{include file="customer/top_menu.tpl"}</td>
		<td class="SpeedBarBox"><img src="{$cdn_base}/skin1/images/spacer.gif" width="1" height="79" alt="" /></td>
	</tr>
	<tr>
		<td align="right" valign="top">{if $main ne "fast_lane_checkout"}{include file="customer/search.tpl"}{else}&nbsp;{/if}</td>
		<td><img src="{$cdn_base}/skin1/images/spacer.gif" width="1" height="35" alt="" /></td>
	</tr>
	<tr>
		<td align="right" colspan="2">
{if $main ne "fast_lane_checkout"}
		<table cellpadding="0" cellspacing="0">
		<tr>
			<td>{if $login eq "" }{ include file="auth.tpl" }{else}{ include file="authbox.tpl" }{/if}</td>
			<td><img src="{$cdn_base}/skin1/images/spacer.gif" width="19" height="1" alt="" /></td>
			<td>{include file="customer/main/minicart.tpl" short_mode="short"}</td>
		</tr>
		</table>
{else}
<form action="{$xcart_web_dir}/include/login.php" method="post" name="toploginform">
<input type="hidden" name="mode" value="logout" />
<input type="hidden" name="redirect" value="{$redirect}" />
<table cellpadding="0" cellspacing="0" width="100%">
<tr>
	<td class="FLCAuthPreBox">
	{if $active_modules.SnS_connector and $sns_collector_path_url ne '' && $config.SnS_connector.sns_display_button eq 'Y'}
<img src="{$cdn_base}/skin1/images/rarrow.gif" alt="" valign="middle" /><b>{include file="modules/SnS_connector/button.tpl" text_link="Y"}</b>
{else}
<img src="{$cdn_base}/skin1/images/spacer.gif" class="Spc" alt="" />
{/if}
	</td>
{if $login ne ""}
	<td align="right" nowrap="nowrap"><b>{$userinfo.firstname} {$userinfo.lastname}</b> {$lng.txt_logged_in}</td>
	<td class="FLCAuthBox">
{if $js_enabled}
{include file="buttons/button.tpl" button_title=$lng.lbl_logoff href="javascript: document.toploginform.submit();" js_to_href="Y"}
{else}
{include file="buttons/logout_menu.tpl"}
{/if}
	</td>
{/if}
</tr>
</table>
</form>
{/if}
		</td>
		<td><img src="{$cdn_base}/skin1/images/spacer.gif" width="1" height="52" alt="" /></td>
	</tr>
	<tr>
		<td align="right" colspan="2">
		{if $js_enabled}
		<a href="{$js_update_link|amp}" class="SmallNote">{$lng.txt_javascript_disabled}</a>
		{else}
		<a href="{$js_update_link|amp}" class="SmallNote">{$lng.txt_javascript_enabled}</a>
		{/if}
		</td>
		<td><img src="{$cdn_base}/skin1/images/spacer.gif" width="1" height="20" alt="" /></td>
	</tr>
	</table>
	
	</td>
</tr>
</table>
{if $main ne "fast_lane_checkout"}
{if ((($main eq 'catalog' && $cat ne '') || $main eq 'product' || ($main eq 'comparison' && $mode eq 'compare_table') || ($main eq 'choosing' && $smarty.get.mode eq 'choose')) && $config.Appearance.enabled_printable_version eq 'Y') || ($all_languages_cnt gt 1)}
<table cellpadding="0" cellspacing="0" width="100%">
<tr>

{if $all_languages_cnt gt 1}

	<td>
	
	<form action="home.php" method="get" name="sl_form">
	<input type="hidden" name="redirect" value="{$smarty.server.PHP_SELF}?{$smarty.server.QUERY_STRING|amp}" />

	<table cellpadding="0" cellspacing="0" width="171">
	<tr>
		<td align="right" nowrap="nowrap">&nbsp;&nbsp;<font class="SelectSubtitle">{$lng.lbl_select_language}:</font>&nbsp;
		<select name="sl" onchange="javascript: document.sl_form.submit()">
		{section name=ai loop=$all_languages}
			<option value="{$all_languages[ai].code}"{if $store_language eq $all_languages[ai].code} selected="selected"{/if}>{$all_languages[ai].language}</option>
		{/section}
		</select>
		</td>
	</tr>
	</table>

	</form>
	
	</td>

{/if}

{if (($main eq 'catalog' && $cat ne '') || $main eq 'product' || ($main eq 'comparison' && $mode eq 'compare_table') || ($main eq 'choosing' && $smarty.get.mode eq 'choose')) && $config.Appearance.enabled_printable_version eq 'Y'}

	<td width="100%" valign="middle" align="right">{include file="printable.tpl"}</td>
	<td><img src="{$cdn_base}/skin1/images/spacer.gif" width="10" height="1" alt="" /></td>

{/if}

</tr>
</table>
{/if}
<br />
{else}
<br />
{/if}
