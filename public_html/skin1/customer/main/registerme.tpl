{* $Id: register.tpl,v 1.51.2.2 2006/06/29 13:31:45 svowl Exp $ *}
{if $av_error eq 1}
	{include file="modules/UPS_OnLine_Tools/register.tpl"}
{else}
	{if $js_enabled eq 'Y'}
		{include file="check_zipcode_js.tpl"}
		{include file="generate_required_fields_js.tpl"}
		{include file="check_required_fields_js.tpl"}
		{if $config.General.use_js_states eq 'Y'}
		{include file="change_states_js.tpl"}
		{*{include file="check_email_script.tpl"}*}
		{/if}
	{/if}

	{if $registered eq ""}
	
	<script type="text/javascript" language="JavaScript 1.2">
	
	<!--
	var is_run = false;
	function logRegisterEventInAnalytics()
	{ldelim}
		var req = null;
		if(window.XMLHttpRequest)
		{ldelim}
		   req = new XMLHttpRequest(); 
		{rdelim} 
		else if (window.ActiveXObject){ldelim}
				req  = new ActiveXObject("Microsoft.XMLHTTP"); 
		{rdelim}
	      
		 req.open('GET', "trackregistration.php", true);
		 req.send(null);
						    	
	{rdelim}
	
	
	
	function check_registerform_fields()
	{ldelim}
	    if(is_run)
			return false;
		/* Track Google register event analytics */	
	    logRegisterEventInAnalytics() ;
	 	is_run = true;
	 	if (check_zip_code() && checkRequired(requiredFields) && check_term_condition('{$termandcondition}'))
		{ldelim}
		
		     document.registerform.submit();
			 return true;
	
		{rdelim}
		is_run = false;
		return false;
		{rdelim}
	-->
	</script>
	{literal}
	<script type="text/javascript" language="JavaScript 1.2" src="./skin1/order_js.js"></script>  
	<script type="text/javascript" language="JavaScript 1.2">
	
	function check_term_condition(term)
	{
		var chkbox = document.getElementById("termCondition");
	    
		/*if (!/(\.(gif|jpg|jpeg|bmp|png))$/i.test(document.registerform.myimage.value) && (document.getElementById("myimage").value != ""))
		 {
		            alert("Please browse an image ")
			    document.getElementById("myimage").focus();
			    return false
		 } */
		if(!chkbox.checked)
		{
			if(term != 1)
			{
			alert("If you agree to the Terms and Conditions, please select the check box so that your registration can be completed.");
			return false;
			}
		}
		if(!chkbox.checked && term == 1)
		{
	
			alert("If you agree to the Terms and Conditions, please select the check box so that your registration can be completed.");
			return false;
	
		}
	
		return true;
	
	}
	
	function popUpWindow(URL)
	{
	
		window.open(URL,"", "menubar=1,resizable=1,toolbar=1,width=800,height=500");
	}
	
	function hideErrorDiv()
	{
		document.getElementById('popup').style.visibility = 'hidden';
	}
	
	function hideWithDelay()
	{
		setTimeout("hideErrorDiv()", 7000);
	}
	
	function showErrorDiv()
	{
	
		document.getElementById('popup').style.visibility = 'visible';
	}
	
	function checkImageFile(fileObj)
	{
		  if (!/(\.(gif|jpg|jpeg|bmp|png))$/i.test(fileObj.value))
		  {
			alert("Please attach an image.");
			fileObj.focus();
			return false;
		}
		else if(fileObj.value == "")
		{
		     alert("Please browse an image ")
		     fileObj.focus();
		     return false
	        }
		else
		{
			document.registerform.submit();
			return true;
		}
	}
	
	function deleteUserImage(image)
	{
		document.registerform.action = "register.php?action=imagedelete&image="+image;
		document.registerform.submit();
	}
	
	</script>
	{/literal}
	
	
	{*******************Popup message for login************************}
	{if $login_status == 'success'}
	<div id="popup">
	  <div id="overlay"> </div>
	  <div id="message">
	    <div class="wrapper" style="position:absolute; top:250px; left:400px;">
	      <div class="head"><a href="javascript:hideErrorDiv()">Click to close</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
	      <div class="body">
	        <p><strong>You have successfuly logged into Myntra.</strong><br>
	
	          Welcome {$firstname}<br>
	          <br>
	          <!--<a href="#">Privacy Policy</a> | <a href="#">Legal</a> | <a href="#">Site
	          Map</a> --></p>
	      </div>
	      <div class="foot"></div>
	
	    </div>
	  </div>
	</div>
	
	{literal}
	<script type="text/javascript" language="JavaScript 1.2">
	showErrorDiv();
	hideWithDelay();
	</script>
	{/literal}
	{/if}
	{********************************************************************}
	
	{**************Popup message for creating or updating the profile****************}
	{if $success == 'I' || $success == 'U'}
	
	<div id="popup" >
	  <div id="overlay"> </div>
	  <div id="message">
	    <div class="wrapper" style="position:absolute; top:250px; left:400px;">
	      <div class="head"><a href="javascript:hideErrorDiv()">Click to close</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
	      <div class="body">
	        <p><strong>Your profile is updated successfully.</strong><br>
	
	          Welcome {$firstname}<br>
	          <br>
	          <!--<a href="#">Privacy Policy</a> | <a href="#">Legal</a> | <a href="#">Site
	          Map</a> --></p>
	      </div>
	      <div class="foot"></div>
	
	    </div>
	  </div>
	</div>
	{literal}
	<script type="text/javascript" language="JavaScript 1.2">
	showErrorDiv();
	hideWithDelay();
	</script>
	{/literal}
	{/if}
	
	{********************************************************************}
	{if $reg_error}
	{if $reg_error eq "E" }
	
	<div id="popup">
	  <div id="overlay"></div>
	  <div id="error">
	    <div class="wrapper" style="position:absolute; top:200px; left:400px;">
	      <div class="head"><a href="javascript:hideErrorDiv()">Click to close</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
	      <div class="body">
	         <p>
	              <strong>Email id already exist. Please try again...</strong><br>
	         </p>
	
	      </div>
	      <div class="foot"></div>
	    </div>
	  </div>
	</div>
	{literal}
	<script type="text/javascript" language="JavaScript 1.2">
	showErrorDiv();
	</script>
	{/literal}
	
	{elseif $reg_error eq "U" }
	
	<div id="popup" >
	  <div id="overlay"></div>
	  <div id="error">
	    <div class="wrapper" style="position:absolute; top:200px; left:400px;">
	      <div class="head"><a href="javascript:hideErrorDiv()">Click to close</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
	      <div class="body">
	        <p>
	              <strong>E-mail already exist. Please try again...</strong><br>
	        </p>
	
	      </div>
	      <div class="foot"></div>
	    </div>
	  </div>
	</div>
	{literal}
	<script type="text/javascript" language="JavaScript 1.2">
	showErrorDiv();
	</script>
	{/literal}
	{/if}
	
	
	
	{/if}
	<!--regform style-->
	{literal}
	<style>
	.regform .regtop{background:url({$cdn_base}/skin1/mkimages/980/reg-top.jpg) no-repeat;width:965px;height:49px;}
	.regform .regbottom{background:url({$cdn_base}/skin1/mkimages/980/reg-bottom.jpg) no-repeat;width:965px;height:8px;}
	.regform .regmiddle{background:url({$cdn_base}/skin1/mkimages/980/reg-middle.jpg);width:965px;height:auto;}
	.regform .regmiddle .regwrapper{padding:10px 10px 0px 10px;text-align:right;color:#979797;font-size:10pt;}
	.regform .regmiddle .regwrapper table{width:100%;}
	.regform .regmiddle .regwrapper td{color:#545454;font-size:15px;font-weight:bold;height:45px;vertical-align:bottom;}
	.regform .regmiddle .regwrapper td input{border:1px solid #b8bfc9;width:195px;height:22px;}
	.regform .regmiddle .regwrapper td textarea{width:350px;height:65px;border:1px solid #b8bfc9;}
	.regform .regmiddle .regwrapper ul,.regform .regmiddle .regwrapper li{list-style:none;line-height:17pt;}
	.regform .regmiddle .regwrapper td.alignleft{text-align:left;padding-left:20px;"}
	</style>
	{/literal}
	<!--regform style-->
	
	<form action="{$register_script_name}?myAcc={$myAcc}&frmcheckout={$checkout}&{$smarty.server.QUERY_STRING|amp}" method="post" name="registerform"  enctype = "multipart/form-data" >
		{if $config.Security.use_https_login eq "Y"}
			<input type="hidden" name="{$XCARTSESSNAME}" value="{$XCARTSESSID}" />
		{/if}
		{if $referer}
		    <input type="hidden" name="addreferer" value="{$referer}" />
		{/if}
		<input type="hidden" name="image=" id="image"  />
		{if $userinfo.myimage  neq ""}
			<input type="hidden" name="myoldimage" value="{$userinfo.myimage}" />
		{/if}
		<input type="hidden" name="addressoption" id="addressoption" {if $addressoption != ""} value="{$addressoption}" {else} 
		value="P" {/if} />
		
		<!--registration form-->
		<div class="regtop"></div>
		<div class="regmiddle">	
			<div class="regwrapper">
				{include file="main/personal_info.tpl" userinfo=$userinfo}
			</div>
			<div class="regwrapper" style="font-size:8pt;text-align:left;"><span class="mandatory">*</span>&nbsp;Mandatory fields</div>
			<div style="padding:20px 4px 20px 4px;color:#c6c6c6"><hr/></div>
			<div class="regwrapper">
				{include file="main/register_address_info.tpl" userinfo=$userinfo}
			</div>
			<div style="padding:20px 4px 20px 4px;color:#c6c6c6"><hr/></div>
			<div class="regwrapper">
				<p>
					<input type="checkbox" id="dailydesign" name="dailydesign" value= "1" class="address" {if $userinfo.dailydesign eq '1'}checked {else}{/if}/>
					&nbsp;Subscribe daily design.
				</p>
				<p>
					<!--<span class="mandatory">-->
						{if $termandcondition == 1 || $userinfo.termCondition == 1}
							<input type="checkbox" class="checkbox" name="termCondition" id="termCondition" value="1" checked>
						{else}
							<input type="checkbox" class="checkbox" name="termCondition" id="termCondition" value="1">
						{/if}
						&nbsp;If you agree to the <a href="javascript: popUpWindow('termconditionpopup.php?case=popup');">Terms and Conditions</a>, please select the
							check box so that your registration can be completed.
					<!--</span>-->
				</p>
				<p style="padding-top:10px;"><input class="submit" type="submit" name="submitButton" value="     Submit     " border="0" onClick= "javascript: return check_registerform_fields();"></p>			    				
			</div>
			
			<div style="padding:20px 4px 20px 4px;color:#c6c6c6"><hr/></div>					
			
			<div class="regwrapper" style="text-align:left;">
			<a href="{$http_location}/winnerlist.php"><p style="color: rgb(255, 110, 33); font-size: 14pt;"><u>Winner List</u></p></a>
				<p style="color:#ff6e21;font-size:14pt;">Rules</p>
				<p>
					<ul>
						<li><b>1. The "Register & Win" promotion is open from 4th September to 25th September 2009 between 9 am to 9 pm from Monday to Saturday.</b></li>
						<li>2. All registrations on <a href="www.myntra.com">www.myntra.com</a> during this period will qualify for the Free Wrangler t-shirt.</li>
						<li><b>3. All fields marked as compulsory in the Registration Form need to be completely filled, in order to qualify for the Free t-shirt.</b></li>
						<li>4. Registrations will be grouped on an hourly basis and one winner would be selected each hour.</li>
						<li>5. A customer service executive from Myntra.com will get in touch with the winner on the mobile number provided by the winner and take down the t-shirt choice of the winner in terms of colour, size and type of t-shirt (men's or ladies).</li>
						<li>6. Round neck Wrangler t-shirts in a limited number of colours will be given away as free t-shirts.</li>
						<li>7. No personalization would be done on the free t-shirts.</li>
						<li>8. The free t-shirt will be shipped within 7 days from the date of receipt of inputs from the winner.</li>
						<li>9. In case the Myntra customer service executive is not  able to get in touch with a winner, he / she will contact the winner through email on the email id provided by the winner.</li>
						<li>10. In case there is no response by the winner to the email sent by the Myntra customer service executive, Myntra will put the despatch of the free t-shirt on hold. Myntra will not be responsible for any delays in the despatch of the free t-shirt.</li>
						<li>11. No product warranties or guarantees would apply on the free t-shirt sent by Myntra and Myntra will not be liable for the quality of the free t-shirts.</li>
						<li>12. No replacement or exchange requests will be entertained for the free t-shirts sent by Myntra.</li>
						<li>13. Participation in the"Register & Win" promotion implies agreement and compliance with the Terms  & Conditions governing this promotion.</li>
						<li>14. Myntra's decision on the winners of the free t-shirts is final and binding.</li>
					<ul>
				</p>
			</div>
		</div><!--regmiddle-->
		<div class="regbottom"></div>	
		<!--registration form-->
	
		{if $smarty.get.mode eq "update"}
			<input type="hidden" name="mode" value="update" />
		{/if}		
		<input type="hidden" name="anonymous" value="{$anonymous}" />
		<input type="hidden" name="usertype" value="{if $smarty.get.usertype ne ""}{$smarty.get.usertype|escape:"html"}{else}{$usertype}{/if}" />
	</form>
	{/if}
{/if}