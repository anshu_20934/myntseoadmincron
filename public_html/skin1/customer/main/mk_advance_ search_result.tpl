{* $Id: search_result.tpl,v 1.49.2.2 2006/06/16 10:47:41 max Exp $ *}
{* include file="page_title.tpl" title=$lng.lbl_advanced_search *}

<!-- IN THIS SECTION -->

{*include file="dialog_tools.tpl"*}

<!-- IN THIS SECTION -->

{include file="main/include_js.tpl" src="reset.js"}
<script type="text/javascript">
<!--
var searchform_def = new Array();
searchform_def[0] = new Array('posted_data[category_main]', true);
searchform_def[1] = new Array('posted_data[search_in_subcategories]', true);
searchform_def[2] = new Array('posted_data[by_title]', true);
searchform_def[3] = new Array('posted_data[by_shortdescr]', true);
searchform_def[4] = new Array('posted_data[by_fulldescr]', true);
searchform_def[5] = new Array('posted_data[by_keywords]', true);
searchform_def[6] = new Array('posted_data[price_min]', '{$zero}');
searchform_def[7] = new Array('posted_data[avail_min]', '0');
searchform_def[8] = new Array('posted_data[weight_min]', '{$zero}');
-->
</script>

<div class="form">
<form name="searchform" action="search.php?advance=true" method="post">
<input type="hidden" name="mode" value="search" />
<input type="hidden" name="advance" value="true" />
<input type="hidden" name="posted_data[by_title]" value="Y" />
<input type="hidden" name="posted_data[by_shortdescr]" value="Y" />
<input type="hidden" name="posted_data[by_fulldescr]" value="Y" />
<!--<input type="hidden" name="posted_data[category_main]" value="true" /> -->


          <!-- start -->
          <div class="super">
            <p>advanced search
          </div>
	          <div class="head">
	            <p>search for products</p>
	          </div>

	          <div class="links">
	            <div class="legend">
	              <p>{$lng.lbl_search_for_pattern}:</P>
	            </div>
	            <div class="field">
		          <label>
			   			 <input type="text" name="posted_data[substring]" size="30"  
							value="{$search_prefilled.substring}" />
	             </label>
	                 <!-- <input class="submit" type="submit" name="submitButton" 
								value="{$lng.lbl_search|escape}"  border="0" > -->
	            </div>
	            <div class="clearall"></div>
	
	          </div>

          <div class="links">
            <div class="legend">
              <p>&nbsp;</P>
            </div>
	    {if $config.General.allow_search_by_words eq 'Y'}
		    <div class="field">
			 <input class="radio" type="radio" id="including_any" name="posted_data[including]" value="any"{if $search_prefilled.including eq 'any'} checked="checked"{/if} />
			 {$lng.lbl_any_word} |
			 <input class="radio" type="radio" id="including_all" name="posted_data[including]" value="all"{if $search_prefilled eq "" or $search_prefilled.including eq '' or $search_prefilled.including eq 'all'} checked="checked"{/if} />
			{$lng.lbl_all_word} |
			  <input class="radio" type="radio" id="including_phrase" name="posted_data[including]" value="phrase"{if $search_prefilled.including eq 'phrase'} checked="checked"{/if} />
 		        {$lng.lbl_exact_phrase} </div>
		    <div class="clearall"></div>
	     {/if}
	    </div>
	    {*if $config.Search_products.search_products_category eq 'Y'*}
		  <div class="links">
	            <div class="legend">
	              <p>Search in product type{*$lng.lbl_search_in_category*}: </P>
	            </div>
	            <div class="field">
	
			 <!-- <select name="posted_data[categoryid]" > --><!--onChange="MM_jumpMenu('parent',this,0);"> -->
				 <select name="posted_data[producttype]" class="select" >
				    <option value="">--select product type--</option>
				    {section name=pid loop=$producttypes}
						<option value="{$producttypes[pid][0]}" {if $search_prefilled.producttype eq $producttypes[pid][0]}selected{/if}>{$producttypes[pid][1]}</option>
				    {/section}
				  </select>
		
		  		 </div>
	          <div class="clearall"></div>
	       </div>
		{*/if*}
		<div class="links">
	            <div class="legend">
	              <p>Search by designer{*$lng.lbl_search_in_category*}: </P>
	            </div>
	            <div class="field">
	
			 <!-- <select name="posted_data[categoryid]" > --><!--onChange="MM_jumpMenu('parent',this,0);"> -->
				 <select name="posted_data[designer]" class="select" >
				    <option value="">--select designer--</option>
				    {section name=id loop=$designers}
						<option value="{$designers[id].id}" {if $search_prefilled.designer eq $designers[id].id}selected{/if}>{$designers[id].f}&nbsp;{$designers[id].l}</option>
				    {/section}
				  </select><!--&nbsp;<br>&nbsp;&nbsp;&nbsp;Or enter designer name/userid<br/>
				  <input type="text" name="posted_data[designertxt]" size="30"  
				 	value="{$search_prefilled.designertxt}" /> -->
		
		  		 </div>
	          <div class="clearall"></div>
	   </div>
	 {*if $config.Search_products.search_products_price eq 'Y'*}
		<div class="links">
            <div class="legend">
              <p>{$lng.lbl_price} ({$config.General.currency_symbol})</P>
            </div>
            <div class="field"> <label>
              <input type="text" size="10" maxlength="15" name="posted_data[price_min]" value="{$search_prefilled.price_min}" /></label> to <label><input type="text" size="10" maxlength="15" name="posted_data[price_max]" value="{$search_prefilled.price_max}" />
              </label>
              	<br/><br/><input class="submit" type="submit" name="submitButton" value="{$lng.lbl_search|escape}" border="0" > 
            </div>

            <div class="clearall"></div>
          </div>
      {*/if*}
       
	 <div class="foot"></div>
	<br/>

    <!-- end  -->
	


    </form>

 </div>










