{* $Id: register.tpl,v 1.51.2.2 2006/06/29 13:31:45 svowl Exp $ *}
{if $av_error eq 1}

	{include file="modules/UPS_OnLine_Tools/register.tpl"}

{else}

	{if $js_enabled eq 'Y'}
		{include file="check_zipcode_js.tpl"}
		{include file="generate_required_fields_js.tpl"}
		{include file="check_required_fields_js.tpl"}
		{if $config.General.use_js_states eq 'Y'}
		{include file="change_states_js.tpl"}
		{*{include file="check_email_script.tpl"}*}
	{/if}
{/if}

{if $registered eq ""}

<script type="text/javascript" language="JavaScript 1.2">

<!--
var is_run = false;
function check_registerform_fields()
{ldelim}
    
	if(is_run)
		return false;

 	is_run = true;
 	if (check_zip_code() && checkRequired(requiredFields) && check_term_condition('{$termandcondition}'))
	{ldelim}
	
	         document.registerform.submit();
		 return true;

	{rdelim}
	is_run = false;
	return false;
	{rdelim}
-->
</script>
{literal}
<script type="text/javascript" language="JavaScript 1.2" src="./skin1/order_js.js"></script>  
<script type="text/javascript" language="JavaScript 1.2">

function check_term_condition(term)
{
	var chkbox = document.getElementById("termCondition");
    
	/*if (!/(\.(gif|jpg|jpeg|bmp|png))$/i.test(document.registerform.myimage.value) && (document.getElementById("myimage").value != ""))
	 {
	            alert("Please browse an image ")
		    document.getElementById("myimage").focus();
		    return false
	 } */
	if(!chkbox.checked)
	{
		if(term != 1)
		{
		alert("If you agree to the Terms and Conditions, please select the check box so that your registration can be completed.");
		return false;
		}
	}
	if(!chkbox.checked && term == 1)
	{

		alert("If you agree to the Terms and Conditions, please select the check box so that your registration can be completed.");
		return false;

	}

	return true;

}

function popUpWindow(URL)
{

	window.open(URL,"", "menubar=1,resizable=1,toolbar=1,width=800,height=500");
}

function hideErrorDiv()
{
	document.getElementById('popup').style.visibility = 'hidden';
}

function hideWithDelay()
{
	setTimeout("hideErrorDiv()", 7000);
}

function showErrorDiv()
{

	document.getElementById('popup').style.visibility = 'visible';
}

function checkImageFile(fileObj)
{
	  if (!/(\.(gif|jpg|jpeg|bmp|png))$/i.test(fileObj.value))
	  {
		alert("Please attach an image.");
		fileObj.focus();
		return false;
	}
	else if(fileObj.value == "")
	{
	     alert("Please browse an image ")
	     fileObj.focus();
	     return false
        }
	else
	{
		document.registerform.submit();
		return true;
	}
}

function deleteUserImage(image)
{
	document.registerform.action = "register.php?action=imagedelete&image="+image;
	document.registerform.submit();
}

</script>
{/literal}


{*******************Popup message for login************************}
{if $login_status == 'success'}
<div id="popup">
  <div id="overlay"> </div>
  <div id="message">
    <div class="wrapper" style="position:absolute; top:250px; left:400px;">
      <div class="head"><a href="javascript:hideErrorDiv()">Click to close</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
      <div class="body">
        <p><strong>You have successfuly logged into Myntra.</strong><br>

          Welcome {$firstname}<br>
          <br>
          <!--<a href="#">Privacy Policy</a> | <a href="#">Legal</a> | <a href="#">Site
          Map</a> --></p>
      </div>
      <div class="foot"></div>

    </div>
  </div>
</div>

{literal}
<script type="text/javascript" language="JavaScript 1.2">
showErrorDiv();
hideWithDelay();
</script>
{/literal}
{/if}
{********************************************************************}

{**************Popup message for creating or updating the profile****************}
{if $success == 'I' || $success == 'U'}

<div id="popup" >
  <div id="overlay"> </div>
  <div id="message">
    <div class="wrapper" style="position:absolute; top:250px; left:400px;">
      <div class="head"><a href="javascript:hideErrorDiv()">Click to close</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
      <div class="body">
        <p><strong>Your profile is created/updated successfully.</strong><br>

          Welcome {$firstname}<br>
          <br>
          <!--<a href="#">Privacy Policy</a> | <a href="#">Legal</a> | <a href="#">Site
          Map</a> --></p>
      </div>
      <div class="foot"></div>

    </div>
  </div>
</div>
{literal}
<script type="text/javascript" language="JavaScript 1.2">
showErrorDiv();
hideWithDelay();
</script>
{/literal}
{/if}

{********************************************************************}
{if $reg_error}
{if $reg_error eq "E" }

<div id="popup">
  <div id="overlay"></div>
  <div id="error">
    <div class="wrapper" style="position:absolute; top:200px; left:400px;">
      <div class="head"><a href="javascript:hideErrorDiv()">Click to close</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
      <div class="body">
         <p>
              <strong>Email id already exist. Please try again...</strong><br>
         </p>

      </div>
      <div class="foot"></div>
    </div>
  </div>
</div>
{literal}
<script type="text/javascript" language="JavaScript 1.2">
showErrorDiv();
</script>
{/literal}

{elseif $reg_error eq "U" }

<div id="popup" >
  <div id="overlay"></div>
  <div id="error">
    <div class="wrapper" style="position:absolute; top:200px; left:400px;">
      <div class="head"><a href="javascript:hideErrorDiv()">Click to close</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
      <div class="body">
        <p>
              <strong>E-mail already exist. Please try again...</strong><br>
        </p>

      </div>
      <div class="foot"></div>
    </div>
  </div>
</div>
{literal}
<script type="text/javascript" language="JavaScript 1.2">
showErrorDiv();
</script>
{/literal}
{/if}



{/if}

<form action="{$register_script_name}?myAcc={$myAcc}&frmcheckout={$checkout}&{$smarty.server.QUERY_STRING|amp}" method="post" name="registerform"  enctype = "multipart/form-data" >
{if $config.Security.use_https_login eq "Y"}
<input type="hidden" name="{$XCARTSESSNAME}" value="{$XCARTSESSID}" />
{/if}
{if $referer}
          <input type="hidden" name="addreferer" value="{$referer}" />
{/if}
<input type="hidden" name="image=" id="image"  />
{if $userinfo.myimage  neq ""}
     <input type="hidden" name="myoldimage" value="{$userinfo.myimage}" />
{/if}
<input type="hidden" name="addressoption" id="addressoption" {if $addressoption != ""} value="{$addressoption}" {else} 
value="P" {/if} />


{include file="main/register_personal_info.tpl" userinfo=$userinfo}


{*{include file="main/register_account.tpl" userinfo=$userinfo} *}

<div id="addressinfo" name="addressinfo" style="visibility:hidden;display:none">
{include file="main/register_shipping_address.tpl" userinfo=$userinfo}

{include file="main/register_billing_address.tpl" userinfo=$userinfo}
</div>

{*{include file="main/register_additional_info.tpl" section='A'}*}

<div class="subhead"><p>&nbsp;</p></div>

<div class="links" id="tc" style="visibility:visible;display:">
	<p><span class="mandatory">
	{if $termandcondition == 1 || $userinfo.termCondition == 1}
	<input type="checkbox" class="checkbox" name="termCondition" id="termCondition" value="1" checked>
	{else}
	<input type="checkbox" class="checkbox" name="termCondition" id="termCondition" value="1">
	{/if}

		&nbsp;If you agree to the <a href="javascript: popUpWindow('termconditionpopup.php?case=popup');">Terms and Conditions</a>, please select the
		check box so that your registration can be completed.

	</span></p>
	</div>
         <div class="button">


            <input class="submit" type="submit" name="submitButton" value="Submit" border="0" onClick= "javascript: return check_registerform_fields();">
          </div>
	 <div class="foot"></div>
	  <br>


{if $smarty.get.mode eq "update"}
<input type="hidden" name="mode" value="update" />
{/if}
{if $marty.get.mode eq "update"}
<input type="hidden" name="mode" value="update" />
{/if}

<input type="hidden" name="anonymous" value="{$anonymous}" />


<input type="hidden" name="usertype" value="{if $smarty.get.usertype ne ""}{$smarty.get.usertype|escape:"html"}{else}{$usertype}{/if}" />
</form>
{else}
{/if}
{/if}
