{* $Id: minicart.tpl,v 1.17 2006/03/28 08:21:07 max Exp $ *}

{if $short_mode eq "short"} {* Crystal_Blue *}

<table cellpadding="0" cellspacing="3">
<tr>
	<td>{if $minicart_total_items > 0}<a href="cart.php"><img src="{$cdn_base}/skin1/images/custom/cart_top_full.gif" width="19" height="16" alt="" /></a>{else}<img src="{$cdn_base}/skin1/images/custom/cart_top_empty.gif" width="19" height="16" alt="" />{/if}
	</td>
	<td nowrap="nowrap"><a href="cart.php" class="MainSubtitleLink">{$lng.lbl_view_cart}</a></td>
</tr>
<tr>
	<td colspan="2"><img src="{$cdn_base}/skin1/images/spacer.gif" width="1" height="1" alt="" /></td>
</tr>
<tr>
	<td><a href="cart.php?mode=checkout"><img src="{$cdn_base}/skin1/images/custom/checkout_arr.gif" width="16" height="14" alt="" align="middle" /></a></td>
	<td nowrap="nowrap"><a href="cart.php?mode=checkout" class="MainSubtitleLink">{$lng.lbl_checkout}</a></td>
</tr>
</table>

{else} {* Crystal_Blue *}

<table cellpadding="1" cellspacing="0">
{if $minicart_total_items > 0}
<tr>
	<td rowspan="2" width="23"><a href="cart.php"><img src="{$cdn_base}/skin1/images/cart_full.gif" width="19" height="16" alt="" /></a></td>
	<td class="VertMenuItems"><b>{$lng.lbl_cart_items}: </b></td>
	<td class="VertMenuItems">{$minicart_total_items}</td>
</tr>
<tr>
	<td class="VertMenuItems"><b>{$lng.lbl_total}: </b></td>
	<td class="VertMenuItems">{include file="currency.tpl" value=$minicart_total_cost}
</td>
</tr>
{else}
<tr>
	<td width="23"><img src="{$cdn_base}/skin1/images/cart_empty.gif" width="19" height="16" alt="" /></td>
	<td class="VertMenuItems" align="center"><b>{$lng.lbl_cart_is_empty}</b></td>
</tr>
{/if}
</table>
<hr class="VertMenuHr" size="1" />

{/if} {* Crystal_Blue *}
