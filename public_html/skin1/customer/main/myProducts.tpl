{* $Id: myProducts.tpl,v 1.55.2.1 2006/06/27 08:20:37 svowl Exp $ *}
{literal}
<script language="JavaScript">
function deleteProduct(prodId)
{
	if (confirm("Are you sure you want to delete the product?"))
	{
		document.getElementById("productid").value = prodId;
		document.frmProduct.action= "process_product.php";
		document.frmProduct.submit();
	}


}
</script>
{/literal}
<form name="frmProduct" method="post" action="process_product.php">
<INPUT type="hidden" id="productid" name="productid"></INPUT>
<input type="hidden" name="mode" value="deleteproduct" />

<div class="super"><p>{$firstname}'s creations</p></div>
          <div class="head">&nbsp;</div>
	  <div>

            <table cellpadding="2" class="table">
              <tr class="font_bold">
                <td class="align_left">Product</td>
                <td class="align_left">Date Added </td>
                <td class="align_left">Product</td>
                <td class="align_left">Status</td>
                <td class="align_left">Sold</td>
                <td class="align_left">Action</td>
                <td class="align_right">User Ratings</td>
              </tr>

		{assign var=i value=1}
		{section name=product loop=$myProducts}

			<tr class="row_color">
				<td class="align_left">
				{if $myProducts[product][12] }
				  {if $ie eq "true"}
				  <a href="{$http_location}/{$myProducts[product][1]|replace:" ":"-"}/{$myProducts[product][8]}/FC/PD/{$myProducts[product][0]}">
				      <div style="background-image: url({$myProducts[product][12]});
					background-position: center; background-repeat: no-repeat;
					filter:progid:DXImageTransform.Microsoft.Alpha(opacity=0);
					filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='{$myProducts[product][12]}');
					border:1px solid #CCCCCC;" >
					<div style="text-align: center;background-image: url(./images/b.gif);
			        	width: 100px; height:100px;  vertical-align: middle;">
						<img src="{$cdn_base}/images/b.gif"  alt=""  style='border: 0px solid #000000;'/>
					 </div>
				   </div>
				   </a>
				  {else}
					<a href="{$http_location}/{$myProducts[product][1]|replace:" ":"-"}/{$myProducts[product][8]}/FC/PD/{$myProducts[product][0]}">
					     <div style="background-image: url({$myProducts[product][12]});
							background-position: center; background-repeat: no-repeat;
							border:1px solid #CCCCCC;" >
							<div style="text-align: center;background-image: url(./images/b.gif);
					        	width: 90px; height:90px; vertical-align: middle;">
								<img src="{$cdn_base}/images/b.gif"  alt=""  style='border: 0px solid #000000;'/>
							 </div>
				        </div>
						</a>
				   {/if}
                  {else}
				         <img  src="{$xcart_web_dir}/default_logo.gif"   alt="No Image"/>

				 {/if}
				</td>
				{if $myProducts[product][11] == ''}
				<td class="align_left">&nbsp;</td>
				{else}
				<td class="align_left">{$myProducts[product][11]}</td>
				{/if}
				<td class="align_left">{$myProducts[product][8]}</td>
				<td class="align_left">{$myProducts[product][10]}</td>
				<td class="align_left">
				{ if  $myProducts[product][2]}
				          {$myProducts[product][2]}
				{else}
					    0
                                 {/if}
					</td>
				<td class="align_left">
				<a href="{$http_location}/{$myProducts[product][1]|replace:" ":"-"}/{$myProducts[product][8]}/FC/PD/{$myProducts[product][0]}">Modify</a>&nbsp;|&nbsp;<a href="javascript:deleteProduct('{$myProducts[product][0]}');">
				Delete</a>&nbsp;|&nbsp;<a href="mkMyAccount.php?favproductid={$myProducts[product][0]}&acPage={$acPage}&login={$login}&page={$page}">Add to favourite</a>
				</td>
				<td class="align_left">
			        {if $myProducts[product][3] == 0}No Rating</td>{/if}
			        {if $myProducts[product][3] == 1}
			        <img src="{$cdn_base}/skin1/mkimages/grade01.gif" >{/if}
			        {if $myProducts[product][3] == 2}
			        <img src="{$cdn_base}/skin1/mkimages/grade02.gif" >{/if}
			        {if $myProducts[product][3] == 3}
			        <img src="{$cdn_base}/skin1/mkimages/grade03.gif" >{/if}
			        {if $myProducts[product][3] == 4}
			        <img src="{$cdn_base}/skin1/mkimages/grade04.gif" >{/if}
			        {if $myProducts[product][3] == 5}
			        <img src="{$cdn_base}/skin1/mkimages/grade05.gif" >{/if}
                </td>


			</tr>
		{/section}
		 <tr class="row_color">
		       <td  class="align_right" colspan="8"><b>{$firstPage}&nbsp;|&nbsp;{$prev}&nbsp;|&nbsp;{$next}&nbsp;|&nbsp;{$lastPage}
		       (Total designs :: {$totalDesigns})</b></td>
		 </tr>


      </table>

     <div class="links">
          <p align="right"><input class="submit" type="button" name="submitButton"
        	 	 value="upload new product" border="0" ]
           		 onClick="window.location.href='mkstartsellinghome.php'">
          </p>
        </div>
      </div>
      <div class="foot"></div>
</form>
{literal}
	<script type="text/javascript">
		function hideErrorDiv()
		{
			document.getElementById('errorpopup').style.visibility = 'hidden';
		}
		function hideSuccessDiv()
		{
		    document.getElementById('successpopup').style.visibility = 'hidden'; 
		}
		
		function hideWithDelay()
		{
		    setTimeout("hideSuccessDiv()", 7000);
		}
		
		function showSuccessDiv()
		{
		    document.getElementById('successpopup').style.visibility = 'visible'; 
		}
	</script>
{/literal}