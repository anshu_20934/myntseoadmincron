<div class="form"> 
<form name="searchform" action="search.php" method="post">
<input type="hidden" name="mode" value="search" />
 <input type="hidden" name="simple_search" value="Y" />
	
<input type="hidden" name="posted_data[by_title]" value="Y" />
<input type="hidden" name="posted_data[by_shortdescr]" value="Y" />
<input type="hidden" name="posted_data[by_fulldescr]" value="Y" />
<!-- make the search context sensitive by product type -->
	    <input type="hidden" name="posted_data[by_type]" value="{$search_prefilled.by_type}" />
	    <input type="hidden" name="posted_data[by_TypeName]" value="{$search_prefilled.by_TypeName}" />
	 <!-- make the search context sensitive by category -->   
	 <input type="hidden" name="posted_data[by_category]" value="{$search_prefilled.by_category}" />
	 <input type="hidden" name="posted_data[by_categoryName]" value="{$search_prefilled.by_categoryName}" />


      
          <!-- start -->
          <div class="super"> 
            <p>search 
          </div>
          <div class="head"> 
            <p>search for products</p>
          </div>

          <div class="links"> 
            <div class="legend"> 
              <p>{$lng.lbl_search_for_pattern}:</P>
            </div>
            <div class="field">
	          <label> 
		    <input type="text" name="posted_data[substring]" size="30"  value="{$search_prefilled.substring}" />
                  </label> 
                  <input class="submit" type="submit" name="submitButton" value="{$lng.lbl_search|escape}"  border="0" >
            </div>
            <div class="clearall"></div>

          </div>

          <div class="links"> 
            <div class="legend"> 
              <p>&nbsp;</P>
            </div>
	    {if $config.General.allow_search_by_words eq 'Y'}
		    <div class="field"> 
			 <input  class="checkbox" type="radio" id="including_any" name="posted_data[including]" value="any"{if $search_prefilled.including eq 'any'} checked="checked"{/if} />
			 {$lng.lbl_any_word} | 
			 <input class="checkbox" type="radio" id="including_all" name="posted_data[including]" value="all"{if $search_prefilled eq "" or $search_prefilled.including eq '' or $search_prefilled.including eq 'all'} checked="checked"{/if} />
			{$lng.lbl_all_word} | 
			  <input  class="checkbox"  type="radio" id="including_phrase" name="posted_data[including]" value="phrase"{if $search_prefilled.including eq 'phrase'} checked="checked"{/if} />
 		        {$lng.lbl_exact_phrase} </div>
		    <div class="clearall"></div>
	     {/if}
	    </div>
          <!-- <div class="links"> 
            <div class="legend"> 
              <p>Search In: </P>
            </div>
            <div class="field"> 
              <input type="checkbox" id="posted_data_by_title" name="posted_data[by_title]"{*if $search_prefilled eq "" or $search_prefilled.by_title*} checked="checked"{*/if*} />
             {*$lng.lbl_product_title*} | 
              <input type="checkbox" id="posted_data_by_shortdescr" name="posted_data[by_shortdescr]"{*if $search_prefilled eq "" or $search_prefilled.by_shortdescr*} checked="checked"{*/if*} />
              {*$lng.lbl_short_description*} </div>
            <div class="clearall"></div>
          </div> -->
		  <div class="foot"></div>
	 <br/>
</div>