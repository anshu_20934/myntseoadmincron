{* $Id: myCommision.tpl,v 1.55.2.1 2006/06/27 08:20:37 svowl Exp $ *}

<form name="searchDates" method="post" action="mkMyAccount.php?acPage=4">


<div class="super"><p>{$firstname}'s commissions</p></div>
<div class="head"><p>date range</p></div>
  <div class="links">
  <div class="field">
  <label>
    <p><input  name="txtStartDate" id="txtStartDate" type="text" value="{$startDate}" >&nbsp;
    <a href="javascript:cal4.popup();" ><img src="{$cdn_base}/images/cal.gif"  border="0"   alt="Click Here to Pick up the date"></a>
  </label>
    &nbsp;  to &nbsp;
<label>
    <input  name="txtEndDate" id="txtEndDate" type="text"  value="{$endDate}" > &nbsp;<a href="javascript:cal5.popup();" ><img src="{$cdn_base}/images/cal.gif"  valign="middle"  border="0" alt="Click Here to Pick up the date"></a></p>
    </label>
    <div class="button">
    <input class="submit" type="submit" name="submitButton" value="search" border="0" onClick="return submitSearch();">
    </div>
    </div>   
    <div class="clearall"></div>
  </div>
  <div class="foot"></div>
  <br>  
  </form>

<script language="javascript">
      var cal4 = new calendar2(document.forms['searchDates'].elements['txtStartDate']);
      cal4.year_scroll = true;
      cal4.time_comp = false;

      var cal5 = new calendar2(document.forms['searchDates'].elements['txtEndDate']);
      cal5.year_scroll = true;
      cal5.time_comp = false;


</script>


      
<div class="subhead"><p>search results</p></div>
          <div>
            {if $myCommision }
	    
	    
	    <table cellpadding="2" class="table">
		      <tr class="font_bold">
			<td class="align_right">Sl.no.</td>
			<td class="align_left">Product</td>
			<td class="align_left">Date Sold </td>
			<td class="align_right">Sales Price (Rs) </td>
			<td class="align_right">Commission (Rs) </td>
			<td class="align_right">Status</td>
		      </tr>

			{assign var=i value=0}
			{assign var=total value=0}
			{assign var=comm value=0}
			{section name=commision loop=$myCommision}
			{assign var=comm value=$myCommision[commision][7]}

			<tr class="row_color">
				<td class="align_right">{$i+1}</td>
				<td class="align_left">
				{if $myCommision[commision][2]}
					<img src="{$myCommision[commision][2]}"  border="0">
				{else}
				  <img id="{$idtag}" src="{$xcart_web_dir}/image.php?type={$type}&amp;id={$id}&amp;ts={$smarty.now}{if $already_loaded}&amp;tmp=Y{/if}"  height='80' width='80' alt="{include file="main/image_property.tpl"}"/>
				{/if}
				</td>
				<td class="align_left">{$myCommision[commision][3]}</td>
				<td class="align_right">Rs.<span>{$myCommision[commision][4]|string_format:"%.2f"}</span></td>
				<td class="align_right">{$myCommision[commision][7]|string_format:"%.2f"}</td>
				<td class="align_right">{$myCommision[commision][6]}</td>
			</tr>
		
			{assign var=i value=$i+1}
			{assign var=total value=$total+$comm}
			{/section}

			<tr class="font_bold">
				<td class="align_right">&nbsp;</td>
				<td class="align_left">&nbsp;</td>
				<td class="align_left">&nbsp;</td>
				<td class="align_right">Total</td>
				<td class="align_right">Rs.{$total|string_format:"%.2f"}</td>
				<td class="align_right">&nbsp;</td>
			</tr>

           </table> 
	   {else}
                  <div class="links">
                    <p>No commission has been generated during the selected date range.</p>
		   </div>  

	   {/if}
	   </div>
	<div class="foot"></div>
          <br>
