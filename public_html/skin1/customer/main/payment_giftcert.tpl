{* $Id: payment_giftcert.tpl,v 1.12 2006/04/10 07:36:17 max Exp $ *}
<script type="text/javascript">
<!--
requiredFields[0] = ["gcid", "{$lng.lbl_gift_certificate}"];
-->
</script>
<div class="subhead"> 
   <p>payment method: gift certificate</p>
</div>

<div class="link">
<div class="legend"></div>
<p>{$gcerr}</p>
</div>

<div class="links"> 
    <div class="legend"> 
      <p> Gift certificate:<span class="mandatory">*</span></p>
    </div>

    <div class="field"> 
      <p> <input type="text"  id="gcid" name="gcid" /> </p>
    </div>
    <div class="clearall"></div>
  </div>
  <div class="foot"></div>
 <br/>    