{* $Id: navigation_new.tpl,v 1.16.2.1 2011/07/12 10:47:41 max Exp $ *}

<div style="text-align: left; padding: 10px 0px;">
	{if $current_page gt 1}
		<a href="{$navigation_script}&amp;page={math equation="page-1" page=$current_page}"><img title="Previous Page" src="{$cdn_base}/skin1/images/larrow.gif" class="NavigationArrow" alt="{$lng.lbl_prev_page|escape}" /></a>
	{/if}
	&nbsp; &nbsp;
	Showing Page {$current_page}
	&nbsp;&nbsp;
	<a href="{$navigation_script}&amp;page={math equation="page+1" page=$current_page}"><img title="Next Page" src="{$cdn_base}/skin1/images/rarrow.gif" class="NavigationArrow" alt="{$lng.lbl_next_group_pages|escape}" /></a>	
</div>
