{* $Id: navigation.tpl,v 1.16.2.1 2006/06/16 10:47:41 max Exp $ *}
{literal}
<script language="javascript">
	function sortSearchProducts(url)
	{
		window.location.href =url ;

	}
</script>
{/literal}
{assign var="navigation_script" value=$navigation_script|amp}

<div class="right"> 
		{assign var=sortparam1 value=USERRATING}
		{assign var=sortparam2 value=BESTSELLING}
		{assign var=sortparam3 value=MOSTRECENT}
		{assign var=sortparam4 value=EXPERTRATING}
		{if $navigation_script}
		<p>



				{if $sortMethod eq EXPERTRATING}
				<span style="color: rgb(29, 107, 179);font-weight: 600;line-height:27px;font-size:11px;">Expert Rating</span>
                {else}
				<a rel="nofollow" style="line-height:27px;font-size:11px;color:#1D6BB3;text-decoration:none;" href="{$http_location}/{$search_key_hyphenated}/search/?sortby=EXPERTRATING&page=1">Expert Rating</a>
				{/if}
				 &nbsp;|&nbsp;
				 {if $sortMethod eq USERRATING}
				 <span style="color: rgb(29, 107, 179);font-weight: 600;line-height:27px;font-size:11px;">User Rating</span>
				 {else}
				 <a rel="nofollow" style="line-height:27px;font-size:11px;color:#1D6BB3;text-decoration:none;" href="{$http_location}/{$search_key_hyphenated}/search/?sortby=USERRATING&page=1">User Rating</a>
				{/if}
				 &nbsp;|&nbsp;
				  {if $sortMethod eq BESTSELLING}
				 <span style="color: rgb(29, 107, 179);font-weight: 600;line-height:27px;font-size:11px;">Best Selling</span>
				 {else}
				 <a rel="nofollow" style="line-height:27px;font-size:11px;color:#1D6BB3;text-decoration:none;" href="{$http_location}/{$search_key_hyphenated}/search/?sortby=BESTSELLING&page=1">Best Selling</a>
				{/if}
				 &nbsp;|&nbsp;
				  {if $sortMethod eq MOSTRECENT}
				 <span style="color: rgb(29, 107, 179);font-weight: 600;line-height:27px;font-size:11px;">Just Arrived</span>
				 {else}
				 <a rel="nofollow" style="line-height:27px;font-size:11px;color:#1D6BB3;text-decoration:none;" href="{$http_location}/{$search_key_hyphenated}/search/?sortby=MOSTRECENT&page=1">Just Arrived</a> 
				{/if}
		</p>
		{/if}
</div>



 <div class="right"> 
 <div style="padding-top: 7px;font-size:11px;">
 {$paginator}
 </div>
</div> 

