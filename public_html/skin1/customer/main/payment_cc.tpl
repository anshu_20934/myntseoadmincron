

<div class="subhead"> 
   <p>payment method: credit card</p>
</div>
<div class="links"> 
    <div class="legend"> 
      <p> 
	<!--<input class="radio" type="radio" name="radiobutton" value="radiobutton"> -->
	Credit Card Type:<span class="mandatory">*</span></p>
    </div>

    <div class="field"> 
      <p> 
	<select name="menu1" class="GtextBox" onChange="MM_jumpMenu('parent',this,0)">
	  <option value="#" selected>Visa</option>
	  <option value="#">Master Card</option>
	</select>
      </p>
    </div>
    <div class="clearall"></div>
  </div>

  <div class="links"> 
    <div class="legend"> 
      <p> 
	<!--<input class="radio" type="radio" name="radiobutton" value="radiobutton">-->
	Card Holder's Name:<span class="mandatory">*</span></p>
    </div>
    <div class="field"> 
      <p> 
      <input name="ccHolderName"  id="ccHolderName"  type="text" >
      </p>
    </div>
    <div class="clearall"></div>
</div>
<div class="links"> 
    <div class="legend"> 
      <p> 
	<!--<input class="radio" type="radio" name="radiobutton" value="radiobutton"> -->
	Credit Card Number:<span class="mandatory">*</span></p>

    </div>
    <div class="field"> 
      <p> 
	<input name="ccNumber"   id="ccNumber" type="text"  >
	&nbsp;(No spaces or dashes)</p>
    </div>
    <div class="clearall"></div>
</div>

<div class="links"> 
    <div class="legend"> 
      <p> 
	<!-- <input class="radio" type="radio" name="radiobutton" value="radiobutton">-->

	Expiration Date:<span class="mandatory">*</span></p>
    </div>
    <div class="field"> 
      <p> 
	<select name="ccExpMonth"  id= "ccExpMonth"  ><!--onChange="MM_jumpMenu('parent',this,0)">-->
	  <option value="01" >01</option>
	  <option value="02">02</option>
	  <option value="03">03</option>
	  <option value="04">04</option>
	  <option value="05">05</option>
	  <option value="06">06</option>
	  <option value="07">07</option>
	  <option value="08">08</option>
	  <option value="09">09</option>
	  <option value="10">10</option>
	  <option value="11">11</option>
	  <option value="12" >12</option>
	</select>
	&nbsp; 
        {html_select_date prefix="ccExpire" time=$time 
         end_year="+2" display_days=false display_months=false}

	<!--<select name="ccExpYear" id= "ccExpYear" class="select" onChange="MM_jumpMenu('parent',this,0)" >
		 <option value="{$smarty.now|date_format:"%Y"}" selected>{$smarty.now|date_format:"%Y"}</option>
		 
	</select> -->
	&nbsp;(month/year)</p>
    </div>
    <div class="clearall"></div>
</div>
<div class="links"> 
    <div class="legend"> 
      <p> 
	<!-- <input class="radio" type="radio" name="radiobutton" value="radiobutton">-->
	CVV2<span class="mandatory">*</span></p>

    </div>
    <div class="field"> 
      <p> 
      <input name="ccvNo" id="ccvNo" type="text" >
	
      </p>
    </div>
    <div class="clearall"></div>
</div>
<div class="links">&nbsp;</div>
 <!--  <div class="links"> 
    <p> 
      <input class="submit" type="button" name="submitButton" value="edit" border="0" onClick="#">
    </p>
  </div> -->
<div class="foot"></div>
 <br/>                     