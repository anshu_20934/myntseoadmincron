{* $Id: home.tpl,v 1.88.2.3 2006/07/19 10:19:35 max Exp $ *}
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
{if $printable ne ''}
{include file="customer/home_printable.tpl"}
{else}
{config_load file="$skin_config"}
<html xml:lang="en" xmlns:fb="http://www.facebook.com/2008/fbml">
<head>
<title>
{if $config.SEO.page_title_format eq "A"}
{section name=position loop=$location}
{$location[position].0|strip_tags|escape}
{if not %position.last%} :: {/if}
{/section}
{else}
{section name=position loop=$location step=-1}
{$location[position].0|strip_tags|escape}
{if not %position.last%} :: {/if}
{/section}
{/if}
</title>
{include file="meta.tpl" }
<link rel="stylesheet" href="{$SkinDir}/{#CSSFile#}" />
<link rel="stylesheet" href="{$SkinDir}/{#mkCSSFile#}" />

{if $js_enabled}
{include file="main/include_js.tpl" src="customer/main/swap_categories.js"}
{/if}
</head>
<body{$reading_direction_tag}{if $js_enabled} onload="javascript: MM_preloadImages({section loop=8 name='cat_images'}'{$ImagesDir}/custom/cat_item{$smarty.section.cat_images.iteration}','{$ImagesDir}/custom/cat_itemon{$smarty.section.cat_images.iteration}'{if not $smarty.section.cat_images.last},{/if}{/section}); {if $body_onload ne ''}{$body_onload}{/if}"{/if}>
{assign var="fancy_left" value=9}
{assign var="fancy_width" value=140}
{assign var="fancy_ie_background_color" value="D9E9F5"}
{include file="rectangle_top.tpl" }
{include file="head.tpl" }
{if $active_modules.SnS_connector}
{include file="modules/SnS_connector/header.tpl"}
{/if}
<!-- main area -->
<table width="100%" cellpadding="0" cellspacing="0" align="center">
<tr>
<td width="6">&nbsp;</td>
<td width="165" valign="top">
{if $categories ne "" and ($active_modules.Fancy_Categories ne "" or $config.General.root_categories eq "Y" or $subcategories ne "")}
{include file="customer/categories.tpl" }
{/if}
{if $active_modules.Bestsellers ne "" and $config.Bestsellers.bestsellers_menu eq "Y"}
{include file="modules/Bestsellers/menu_bestsellers.tpl" }
{/if}
{if $active_modules.Manufacturers ne "" and $config.Manufacturers.manufacturers_menu eq "Y"}
{include file="modules/Manufacturers/menu_manufacturers.tpl" }
{/if}
{include file="customer/menu_cart.tpl" }
{if $active_modules.Feature_Comparison ne "" && $comparison_products ne ''}
{include file="modules/Feature_Comparison/product_list.tpl" }
{/if}
{if $active_modules.Users_online ne "" and $users_online}
{include file="modules/Users_online/menu_users_online.tpl" }
{/if}
{include file="customer/special.tpl"}
{if $active_modules.Survey && $menu_surveys}
{foreach from=$menu_surveys item=menu_survey}
{include file="modules/Survey/menu_survey.tpl"}
{/foreach}
{/if}

{if $active_modules.XAffiliate ne ""}
{include file="partner/menu_affiliate.tpl" }
{/if}

{if $active_modules.Interneka ne ""}
{include file="modules/Interneka/menu_interneka.tpl" }
{/if}
{if $active_modules.SnS_connector && $config.SnS_connector.sns_display_button eq 'Y' && $sns_collector_path_url ne ''}
<br />
<div align="center">
{include file="modules/SnS_connector/button.tpl"}
</div>
{/if}
{include file="poweredby.tpl" }
<img src="{$cdn_base}/skin1/images/spacer.gif" width="150" height="1" alt="" />
</td>
<td width="20">&nbsp;</td>
<td valign="top">
<!-- central space -->
{if $main ne "catalog" or $current_category.category ne ""}
{include file="location.tpl"}
{/if}

{include file="dialog_message.tpl"}

{if $active_modules.Special_Offers ne ""}
{include file="modules/Special_Offers/customer/new_offers_message.tpl"}
{/if}

{include file="customer/home_main.tpl"}
<!-- /central space -->
&nbsp;
</td>
<td width="20">&nbsp;</td>
</tr>
</table>
{include file="rectangle_bottom.tpl" }
</body>
</html>
{/if}
