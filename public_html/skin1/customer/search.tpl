{* $Id: search.tpl,v 1.9 2005/11/17 06:55:37 max Exp $ *}
<form method="post" action="search.php" name="productsearchform">
<input type="hidden" name="simple_search" value="Y" />
<input type="hidden" name="mode" value="search" />
<input type="hidden" name="posted_data[by_title]" value="Y" />
<input type="hidden" name="posted_data[by_shortdescr]" value="Y" />
<input type="hidden" name="posted_data[by_fulldescr]" value="Y" />
<table cellpadding="0" cellspacing="2">  
<tr>
	<td width="20">&nbsp;</td>
	<td class="SearchSubtitle">{$lng.lbl_search}:&nbsp;</td>
	<td valign="middle">
		<input type="text" name="posted_data[substring]" size="16" value="{$search_prefilled.substring|escape}" />
	</td>
	<td valign="middle">&nbsp;
		<a href="javascript: document.productsearchform.submit();">{include file="buttons/search_head.tpl"}</a></td>
	<td width="20">&nbsp;</td>
	<td><a href="search.php" class="Search"><u>{$lng.lbl_advanced_search}</u></a></td>
</tr>
</table>
</form>
