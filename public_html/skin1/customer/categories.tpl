{* $Id: categories.tpl,v 1.26 2005/11/17 06:55:37 max Exp $ *}
{capture name=menu}
{if $active_modules.Fancy_Categories ne ""}
{include file="modules/Fancy_Categories/categories.tpl"}
{assign var="fc_cellpadding" value="0"}
{else}

<table cellpadding="0" cellspacing="0" width="100%">

{if $config.General.root_categories eq "Y"}
{assign var="_categories" value=$categories}
{else}
{assign var="_categories" value=$subcategories}
{/if}

{foreach from=$_categories key=catid item=c name="cats"}

<tr style="CURSOR: pointer;" class="MenuItemOff" {if $js_enabled}onmouseover="ShowMenyItem(this,'On'); MM_swapImage('cat_img{$catid}','','{if $full_url}{$http_host}{$ImagesDir|replace:"..":""}{else}{$ImagesDir}{/if}/custom/cat_itemon{cycle name='on_images' values='8,1,2,3,4,5,6,7'}.gif',1)" onmouseout="ShowMenyItem(this,'Off'); MM_swapImgRestore()"{/if}>
	<td><img src="{$cdn_base}/skin1/images/spacer.gif" width="7" height="1" alt="" /></td>
	<td height="17">&nbsp;&nbsp;<img id="cat_img{$catid}" src="{$ImagesDir}/custom/cat_item{cycle values="8,1,2,3,4,5,6,7"}.gif" alt="" class="CatImage" />&nbsp;&nbsp;<a href="home.php?cat={$catid}" class="CategoriesList">{$c.category}</a></td>
	<td><img src="{$cdn_base}/skin1/images/spacer.gif" width="10" height="1" alt="" /></td>
</tr>
{if not $smarty.foreach.cats.last}
<tr>
	<td><img src="{$cdn_base}/skin1/images/spacer.gif" width="7" height="1" alt="" /></td>
	<td class="VertMenuLine"><img src="{$cdn_base}/skin1/images/spacer.gif" width="1" height="1" alt="" /></td>
	<td><img src="{$cdn_base}/skin1/images/spacer.gif" width="10" height="1" alt="" /></td>
</tr>
{/if}

{/foreach}

</table>

{/if}
{/capture}
{ include file="menu.tpl" dingbats="dingbats_categorie.gif" menu_title=$lng.lbl_categories menu_content=$smarty.capture.menu cellpadding=$fc_cellpadding menu_style="categories"}
