{if $printable ne ''}
{include file="customer/top_menu_printable.tpl"}
{elseif $speed_bar}
{assign var="total" value="0"}
{section name=sb loop=$speed_bar}
{if $speed_bar[sb].active eq "Y"}{math equation="x+1" assign="total" x=$total}{/if}
{/section}
<table cellpadding="0" cellspacing="0">
<tr>
{if $total >= "4"}
{math assign="first_color" equation="x-3" x=$total}
{else}
{section name=sp_images loop="4" start=$total}
	<td class="SpeedBarBox{math equation="y+3-x" y=$total x=$smarty.section.sp_images.index}"><img src="{$cdn_base}/skin1/images/spacer.gif" width="60" height="79" alt="" /></td>
{/section}
{assign var="first_color" value="1"}
{/if}
{assign var="current_row" value="1"}
{section name=sb loop=$speed_bar}
{if $speed_bar[sb].active eq "Y"}
{if $current_row >= $first_color}
{math equation="x-y" x=$total y=$current_row assign="colorid"}
	<td>
	
	<table cellpadding="0" cellspacing="0" class="SpeedBarBox{$colorid}">
	<tr>
		<td><img src="{$cdn_base}/skin1/images/spacer.gif" width="60" height="12" alt="" /></td>
	</tr>
{else}
{assign var="colorid" value="3"}
	<td>
	
	<table cellpadding="0" cellspacing="0">
	<tr>
		<td><img src="{$cdn_base}/skin1/images/spacer.gif" width="1" height="12" alt="" /></td>
	</tr>
{/if}
	<tr>
		<td valign="top" height="67">
		
		<table cellpadding="0" cellspacing="0" width="100%">
		<tr>
			<td class="SpeedLink{$colorid}" align="center" width="100%"><a class="SpeedBar" href="{$speed_bar[sb].link}">{$speed_bar[sb].title}</a></td>
			<td class="SpeedLinkBorder"><img src="{$cdn_base}/skin1/images/spacer.gif" width="1" height="23" alt="" /></td>
		</tr>
		<tr>
			<td class="SpeedLinkBorder"><img src="{$cdn_base}/skin1/images/spacer.gif" width="1" height="1" alt="" /></td>
			<td><img src="{$cdn_base}/skin1/images/spacer.gif" width="1" height="1" alt="" /></td>
		</tr>
		</table>
		
		</td>
	</tr>
	</table>
	
	</td>
{math equation="x+1" assign="current_row" x=$current_row}
{/if}
{/section}
</tr>
</table>
{else}
<table cellpadding="0" cellspacing="0">
<tr>
{section name=sp_images loop="4"}
	<td class="SpeedBarBox{math equation="3-x" x=$smarty.section.sp_images.index}"><img src="{$cdn_base}/skin1/images/spacer.gif" width="60" height="79" alt="" /></td>
{/section}
</tr>
</table>
{/if}
