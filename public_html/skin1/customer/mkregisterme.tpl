<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN"
        "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en" xmlns:fb="http://www.facebook.com/2008/fbml">
<head>
	{include file="site/header.tpl"}
	{literal}
	<style type="text/css">
		.button-img {
				background: none repeat scroll 0 0 #FD9B06;
				border: 0 none;
				color: #FFFFFF;
				cursor: pointer;
				font-size: 14px;
				height: 20px;
				padding-bottom: 5px;
				padding-top: 5px;
				text-align: center;
				width: 50px;
		}	
	#change-image{margin-top:25px;}
    #captcha-form{font-size: 16px;height: 30px;line-height: 30px;width: 150px;margin:7px;}
    .verify-captcha{padding: 5px 15px;margin:7px;background:#FD9B06;}
    .captcha-block{margin-top:05px; display: none; align: center;}
    .captcha-loading{background: url("{/literal}{$cdn_base}{literal}/skin1/myntra_images/captcha-loading.gif") no-repeat; width:16px;height:16px;margin-left:5px;margin-top:15px;}
    .captcha-entry{clear:both;}
    .captcha-entry span {display:block;}
    .cod-page-notifications{text-align:center;}
    .captcha-message{padding:2px;width:145px;}
    .verify-captcha.btn-disabled{background:#a8a8a8; cursor:not-allowed;}
    #resend-verification-code.btn-disabled{background:#a8a8a8; cursor:not-allowed;}
    #submit-code.btn-disabled{background:#a8a8a8; cursor:not-allowed;} 

	</style>
	{/literal}
    <!--[if lt IE 8]>
    <link rel="stylesheet" href="{$http_location}/skin1/myntra_css/ie.css" type="text/css" media="screen, projection"><![endif]-->
<link href="{$cdn_base}/skin1/css/thickbox3.css.jgz" rel="stylesheet" type="text/css"/>

<meta property="og:title" content="Shop @Myntra" />
<meta property="og:type" content="company" />
<meta property="og:image" content="{$cdn_base}/skin1/myntra_images/MYNTRA1.png" />
<meta property="og:site_name" content="Myntra.com" />
<meta property="og:url" content="{$http_location}/register.php">
<meta name="description" content="In case you're planning to buy apparel or footwear, here are {$mrp_nonfbNumCoupons} coupons of Rs.{$mrp_nonfbCouponValue} each that you can use @Myntra . All you've got to do is click here and register! " />
<link rel='image_src' href='{$cdn_base}/skin1/myntra_images/MYNTRA1.png'>

</head>
<body class="{$_MAB_css_classes}">

<img src="{$cdn_base}/skin1/myntra_images/MYNTRA1.png" style="display:none"></img>
{include file="site/menu.tpl" }
<div class="container clearfix">
    <!-- Block2 -->
    <!-- @Block2 -->

          <div class="content clearfix">
		 <div class="login-signup clearfix">
		<div style="padding:0 0 0 6px"><img src="{$cdn_base}/skin1/myntra_images/register-banner.jpg"></div>
            	<div class="login-box right {if $mrp_referer neq '' } hide {/if}" >
                	<form method="post" id="forgot-password-form" name="forgot-password" action="forgot-password.php">
                		<div class="forgot-password-form-left span-15 left">
                			<div class="span-13 last mt10 mb10">
                   				<label class="font-black-1 span-3">Email</label>
                   				<input type="text"  value="{if $reg_username_entered}{$reg_username_entered}{else}Your Email Address{/if}" name="username" id="reg-fp-username" class="{if $reg_username_entered}{else}blurred{/if}">
                   				<div class="prepend-3 reg-fp-username-error error"></div>
                   			</div>
                   			<div class="prepend-3 span-10 last mb10">
                   				<input type="submit"  value="Reset Password" id="reg_resetpassword" class="resetpassword sbtn orange-bg left">
                   				<a href="javascript:void(0)" class="si-return-to-login no-decoration-link">Return to Login</a>
                   			</div>
                   		</div>
                	</form>

                	<form method="post" id="login_form" name="signin" action="{$http_location}/include/login.php">
						<input type="hidden" name="usertype" value="C" />
						<input type="hidden" name="redirect" value="1" />
						<input type="hidden" name="mode" value="login" />
						<input type="hidden" name="filetype" value="home" />
						<input type="hidden" id="cfbconnect_login" name="fbconnect_login" value="0" />
                		<div class="signin-form-left span-15 left">
							<div class="title">Log in to myntra.com</div>
                  			<div class="last mt10 mb10">
                   				<label class="font-black-1 span-3">Email</label>
                   				<input type="text"  value="{if $reg_username_entered}{$reg_username_entered}{else}Your Email Address{/if}" name="username" id="reg_username" class="{if $reg_username_entered}{else}blurred{/if}">
                   				<div class="prepend-3 reg-username-error error"></div>
                   			</div>
                   			<div class="last mb10">
                   				<label class="font-black-1 span-3">Password</label>
                   				<input type="password"  value="" name="password" id="reg_password">
                   				<div class="prepend-3 reg-password-error error"></div>
								
                   			</div>
                   			<div class="prepend-3 last mb10 clearfix">
                   				<input type="submit"  value="Login"  class="existinguser sbtn orange-bg left" id="reg_existinguser" >

                   				<a href="javascript:void(0)" class="si-forgot-password no-decoration-link">Forgot Password?</a>
                   			</div>
                   		</div>
                	</form>
            	</div>
		
		<div align="center" {if $mrp_referer eq '' }class="hide"{/if}>
			<a class="no-decoration-link left" target="_blank" href="{$http_location}/myntclub">
			{include file="site/mrp_banner_reg_page.tpl"}
			</a>
		 </div>
		
		<div class="login-box sign-up-onpage clearfix register-page-signin span-14 {if $mrp_referer neq ''}right{else}left{/if}">
			<div class="loading-overlay hide" style=" height: 320px;margin:-10px 0 0 -40px;width: 460px;">
				<img src="{$cdn_base}/skin1/myntra_images/loading-graphic.gif">
			</div>
			<form method="post" id="signup_form" name="signup" action="myntra/ajax_signup.php?myAcc={$myAcc}&frmcheckout={$checkout}&{$smarty.server.QUERY_STRING|amp}">
			{if $config.Security.use_https_login eq "Y"}
				<input type="hidden" name="{$XCARTSESSNAME}" value="{$XCARTSESSID}" />
			{/if}
			{if $referer}
				<input type="hidden" name="addreferer" value="{$referer}" />
			{/if}
			{if $userinfo.myimage  neq ""}
				<input type="hidden" name="myoldimage" value="{$userinfo.myimage}" />
			{/if}
			<input type="hidden" name="addressoption" id="addressoption" {if $addressoption != ""} value="{$addressoption}" {else} 
			value="P" {/if} />
			<input type="hidden" name="gender" value="M" />
			{if $smarty.get.mode eq "update"}
				<input type="hidden" name="mode" value="update" />
			{/if}
			<input type="hidden" name="mrp_referer" id="mrp_referer" value="{$mrp_referer}" />
			<input type="hidden" name="image=" id="image"  />
			<input type="hidden" name="anonymous" value="{$anonymous}" />
			<input type="hidden" name="menu_usertype" value="{if $smarty.get.usertype ne ""}{$smarty.get.usertype|escape:'html'}{else}{$usertype}{/if}" />
			<input type="hidden" name="mode" value="signup" />
			<input type="hidden" value="1" name="menu_redirect" />
				<div class="signup-form-left right">
				{if $mrp_referer neq '' }	
				<div class="mt10 clearfix" style="padding-left: 10px;"><strong>{if $ref_name neq $mrp_referer}{$ref_name}{else}{$mrp_referer}{/if} has invited you to join Myntra</strong></div>
				{/if}	
					<div class="title mt10">{if $mrp_referer eq '' }New user? {/if}Create your Myntra account</div>
					<div class="last mt10 mb10">
						<label class="font-black-1 span-3">Email</label>
						<input type="text"  value="Your Email Address" name="menu_username" id="reg_su-username" class="blurred">
						<div class="prepend-3 reg-su-username-error error"></div>
					</div>
					<div class="last mb10">
						<label class="font-black-1 span-3">Password</label>
						<input type="password"  value="" name="menu_password" id="reg_su-password"/>
 						<div class="prepend-3 reg-su-password-error error"></div>
					</div>
					<div class="last mb10">
						<label class="font-black-1 span-3">Mobile (+91)</label>
						<input type="text"  value="" name="menu_mobile" id="reg_su-mobile" maxlength=10/>
						<div class="prepend-3 reg_su-mobile-error error hide"></div>
						{if $mrp_referer neq ''}
						<div style="padding:4px 0 0 90px;" >Why am I being asked for my mobile no <a style="cursor:pointer" class="vtooltip" rel="<div style='padding:5px'>To maintain the exclusivity of the Mynt Club, all users are required to verify their mobile numbers before they can sign up and enjoy the benefits of membership. Your mobile number will not be shared with any third party or for sending you any unsolicited promotional communication. We may use it to contact you regarding your orders placed with us, but only if absolutely necessary.</div>"><img style="margin-bottom:-6px" src="{$cdn_base}/skin1/images/question.gif"></a></div> 
						
						<div class="invalid-mobile hide" align="center"><font color=red>Please enter a valid mobile number and try again</font></div>
						<div class="incorrect-mobile-code hide" align="center"><font color="red">The entered code is incorrect. Please recheck and enter again. If you have not received your verification code, please wait for atleast 2 minutes before sending a new verification code to your mobile.</font></div>
						<div class="mobile-code-verified hide" style="align: center;">Your mobile has been verified. You will be directed to myntra in 2 seconds.</div>
						<div class="excedded-attempts hide" align="center"><font color="red">You have excedded the maximum no. of attempts to verify this mobile number. If you have still not received the verification code or are facing any other difficulties, please provide an alternate number and retry.</font></div>
						{/if}
					</div>
					<div class="myntra-agreement" align="center" style="margin-bottom:10px;">By signing up, I indicate that I am in agreement with Myntra's Terms of Use and Privacy Policy<br></div>
					<!-- IE7 hack The following div is put to remove 	the duplicate error text being shown to the user 
						 Done because of shortage of time and should be fixed more gracefully -->
					<div class="last mb10 hide">
						<label class="font-black-1 span-3">Password Dummy</label>
						<input type="text"  value="" name="menu_password_dummy" id="reg_su-password_dummmy"/>
						<div class="prepend-3 reg-su-password-error-dummy error"></div>
					</div>
					<!-- End of IE7 hack -->
					<div class="last mb20" align="center">
					{if $mrp_referer eq ''}
						<input type="submit"  value="Signup" id="reg_newuser" class="sbtn orange-bg " onclick="">
					{else}
					<div style="clear: both;"> </div><input type="submit"  value="Signup" class="reg-verify-mobile orange-bg  sbtn" id="signupbtn" onclick="verifyMobile()">
						<input type="button" onclick="" class="sbtn orange-bg hide" id="reg_newuser" value="Signup" >
					{/if}
					</div>		
					<div class="mobile-code-entry hide" align="center">
						<div id="mobile-message" class="hide"></div>
						<p><br>Verification Code:<input id="mobile-code" name="mobile-code" size="4" style="height: 25px; margin-left: 5px; width: 50px;"/>
						&nbsp;&nbsp;&nbsp;
						<input type="button" value="Submit" id="submit-code" class="submit-code sbtn orange-bg " onclick="verifyCode()" />
						<!--<input type="button" value="Cancel" id="cancel-submit-code" class="cancel-submit-code sbtn orange-bg " />-->
					</div>
					<div align="center">
					<div class="captcha-block hide" >
					<!--<div class="span-12 captcha-block hide" style="padding-left: 55px;"> -->
						<div class="notification-success"></div>
						<div class="notification-error"></div>
						<form id="captchaform" method="post" action="">
						<div class="clearfix captcha-details" align="center">
						<div class="p5  black-bg4" style="width: 200px;">
						<img src="{$cdn_base}/skin1/images/spacer.gif" id="captcha" />
						</div>
						<a href="javascript:void(0)" onclick="document.getElementById('captcha').src='{$http_location}/captcha/captcha.php?id=registrationPage&rand='+Math.random();document.getElementById('captcha-form').focus();"
						id="change-image" class="no-decoration-link ">Change text</a>
						</div>
						
						<div class="clearfix mt10 captcha-entry">								
						<span>Type the text you see in the image above to generate verification code.</span>
						<input type="text" name="userinputcaptcha" id="captcha-form" />
							<a href="javascript:void(0)" class="next-button orange-bg  p5 verify-captcha " onClick="verifyCaptcha()">Submit</a>
							<div class="captcha-loading " style="display:none;"></div>
						</div>
						<div class="captcha-message hide"></div>
						</form>
					</div>
					</div>
					<div class="resend-verification-entry hide clear" align="center">	
					<div class="common-message">If you have not received your verification code, please wait for at least 2 minutes before sending a new verification code to your mobile.</div>
					{if $mrp_skipMobileVerification neq 1}
					<div style="padding-top:20px;">
                        In case you do not receive your verification code from Myntra, please call us at our Customer Support number at <b>080-43541999</b> anytime and get your number verified instantly.
                    </div>
                    {/if}
						<div class="last mb20 clearfix">
						<div style="clear:both;"> </div>	
						<input type="button" value="Resend verification code" class="reg-verify-mobile orange-bg  sbtn" id="resend-verification-code" onclick="verifyviaMobile()" style="width: 150px;margin-top: 3px;">
						</div>
					</div>
			</div>
		</div>
		<div style="display:block;clear:both;">&nbsp;</div>
		    <div class="fb-block">
		    <span class="or">OR</span>
			        {if $fb_logged eq 0}

        				<div class="signin-form-right" align="center">
        					<div class="last clearfix">
        						<a class="fb-connect" href="javascript:fbLogin('{$mrp_referer}','1')" onclick="_gaq.push(['_trackEvent', 'reg_page_form_signup', 'fb_connect', 'click']);"></a>
        					</div>
        				</div>
        				<div class="font-black-1 mb10" align="center">Get Rs {$extraCredit} extra(or Rs {$mrp_fbCouponValue} worth of Mynt Credits)<br> when you register on Myntra using your Facebook credentials.</div>
        			{else}
        			{/if}
        	</div>
		
<div id="fbUserLoggingHiddenContent" style="display:none;"> 
<h4> Welcome back {$reg_firstname_entered}</h4>
<hr />
<img style="float: left;margin: 4px;" src="{$cdn_base}/skin1/icons/indicator.gif" /><br/><span style="text-align:center">Logging you into Myntra</span> 
</div> 
<div id="fbUserFoundHiddenContent" style="display:none"> 
<h2 style="margin-top : 10px; padding-bottom: 6px; background: url('{$cdn_base}/skin1/myntra_images/line.png') no-repeat scroll 0 20px transparent;">Welcome {$reg_firstname_entered}</h2>
<br />
<h4 style="text-align:center">We found that you are already registered on Myntra</h4>
<div style="text-align:center">Your account has been linked with your facebook account</div><br />
	<table cellspacing="1" cellpadding="1" border="0" width="100%" class="table-box">
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td align="center">
				<div class="button-img"><span><a style="cursor:pointer" onclick="facebookLogin()" >&nbsp;&nbsp;OK&nbsp;&nbsp;</a></span></div>
			</td>
		</tr>
	</table> 
</div> 
<div id="fbNewUserHiddenContent" style="display:none">
	<div>
		<!--div style="font-weight: bold; text-align: center; padding-top: 2px; padding-bottom: 2px; background: none repeat scroll 0% 0% #627AAD; border-bottom: 1px solid #627AAD; color: rgb(255, 255, 255); font-size: 14px;">
			Complete Registration
		</div-->
	  <div style="text-align: left; padding-left: 0pt; margin-top : 10px; padding-bottom: 6px; background: url('{$cdn_base}/skin1/myntra_images/line.png') no-repeat scroll 0 22px transparent;"><strong style="font-size: 16px;">One final step! Create a password for Myntra</strong></div>
			<form enctype="multipart/form-data" method="post"  id="fbregister_form" action="{$register_script_name}?myAcc={$myAcc}&frmcheckout={$checkout}&{$smarty.server.QUERY_STRING|amp}" >
					{if $config.Security.use_https_login eq "Y"}
						<input type="hidden" name="{$XCARTSESSNAME}" value="{$XCARTSESSID}" />
					{/if}
					{if $referer}
						<input type="hidden" name="addreferer" value="{$referer}" />
					{/if}
					<input type="hidden" name="image=" id="image"  />
					{if $userinfo.myimage  neq ""}
						<input type="hidden" name="myoldimage" value="{$userinfo.myimage}" />
					{/if}
					<input type="hidden" name="addressoption" id="addressoption" {if $addressoption != ""} value="{$addressoption}" {else} 
					value="P" {/if} />
					<input type="hidden" name="gender" value="M" />
					{if $smarty.get.mode eq "update"}
						<input type="hidden" name="mode" value="update" />
					{/if}		
					<input type="hidden" name="anonymous" value="{$anonymous}" />
					<input type="hidden" name="fbRegistration" value="1" />
					<input type="hidden" id="fbRegistrationCancel" name="fbRegistrationCancel" value="0" />
					<input type="hidden" id="fbfirstname" name="firstname" class="formtextbox" size="35" value="{$reg_firstname_entered}"/>
					<input type="hidden" id="fblastname" name="lastname" class="formtextbox" size="35" value="{$reg_lastname_entered}"/>
					<input id="fbemail"  type="hidden" class="formtextbox" size="35" name="uname" value="{$reg_username_entered}"/>
					<input type="hidden" name="passwd2" id="fbpasswd2" class="formtextbox" size="35" />
					<input type="hidden" name="usertype" value="{if $smarty.get.usertype ne ""}{$smarty.get.usertype|escape:"html"}{else}{$usertype}{/if}" />
					<table cellspacing="1" cellpadding="1" border="0" width="430px" class="table-box" style="margin-left: 10px; margin-top: 14px;">
						<tbody>
							<tr>
								<th align="right"><span>Login Id</span> *</th>
								<td><label>{$reg_username_entered}</label><span class="status"></span></td>
							</tr>
							<tr>
								<th align="right"><span>New Password</span> *</th>
								<td><input type="password"  size="35" class="formtextbox" name="passwd1" id="fbpasswd1"><div style="font-size:85%;"></div></td>
							</tr>
						</tbody>
					</table>
					<div class="button-img" style="margin-left:auto; margin-right: auto;">
						<span>
							<a  onclick="facebookCompleteRegistration()" style="cursor: pointer; color: white;">
								Done
							</a>
						</span>
					</div>
				</form>
			<div style="margin-top:18px;text-align: right; background: url('{$cdn_base}/skin1/myntra_images/line.png') no-repeat scroll 0 -3px transparent;">
				<a  onclick="facebookRegisterUser()" style="font-size: 80%;cursor:pointer">
					Skip
				</a>
			</div>
	</div>
</div>
</div>
    {include file="site/footer.tpl" }
    <div class="tooltip-box hide"></div>
{literal}
<script type="text/javascript">
	var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	var numericReg  =  /(^-?[1-9]\d*\.\d*$)|(^-?[1-9]\d*$)|(^-?\.\d\d*$)/;
	var alphaReg = /^[a-zA-Z][a-zA-Z ]*$/;
	var reg_type_newuser=3;
	var usernameDefault="Your Email Address";
	
function validateSigninForm(usertype){
    	var hasError=false;
    	
    	var emailaddressVal;
    	var passwordVal;
    	var usernameErrorElem;
    	var passwordErrorElem;
		var mobileErrorElem;
    	var usernameInput;
    	var passwordInput;
		var mobileVal;
    	var mobileInput;	
	
		if((usertype==reg_type_newuser)){
    		emailaddressVal = $("#reg_su-username").val();
    		passwordVal = $("#reg_su-password").val();
    		usernameErrorElem=$(".reg-su-username-error");
    		passwordErrorElem=$(".reg-su-password-error");
    		usernameInput=$("#reg_su-username");
    		passwordInput=$("#reg_su-password");
			mobileVal = $("#reg_su-mobile").val();
			mobileErrorElem=$(".reg_su-mobile-error");
			mobileInput=$("#reg_su-mobile");
		}
		
		if(!emailaddressVal || emailaddressVal == usernameDefault) {
    		usernameErrorElem.html("Please enter username").slideDown();
    		usernameInput.addClass("error-input");
            hasError = true;
        }
    	else if(!emailReg.test(emailaddressVal)){
    		usernameErrorElem.html("Please use a valid email address.").slideDown();
    		usernameInput.addClass("error-input");
    		hasError = true;
    	}
    	else{
    		usernameErrorElem.html("").slideUp();
    		usernameInput.removeClass("error-input");
    	}

    	if(usernameInput.hasClass("error-input")){
    		if((usertype==reg_type_newuser)){
    			_gaq.push(['_trackEvent', 'reg_page_form_signup', 'signup', 'failure_login_invalid']);
    		}
        }
    	
        if(passwordVal == '') {
        	passwordErrorElem.html("Please enter a password.").slideDown();
        	passwordInput.addClass("error-input");
            hasError = true;
        }
        else if(passwordVal.length < 6){
        	passwordErrorElem.html("Minimum 6 characters required.").slideDown();
        	passwordInput.addClass("error-input");
            hasError = true;
        }
        else{
        	passwordErrorElem.html("").slideUp();
        	passwordInput.removeClass("error-input");
        }
        if(passwordInput.hasClass("error-input")){
        	if((usertype==reg_type_newuser)){
    			_gaq.push(['_trackEvent', 'reg_page_form_signup', 'signup', 'failure_password_invalid']);
    		}
        }
		if((usertype==reg_type_newuser)){
			if(mobileVal == "" || mobileVal.length < 10 || !numericReg.test(mobileVal)){
				hasError=true;
				//$(".invalid-mobile").html("<font color='red'>Please enter a valid mobile number and try again</font>");
				//$(".invalid-mobile").show();
				mobileErrorElem.html("Please enter a valid mobile number and try again").slideDown();
				mobileInput.addClass("error-input");
			} else {
				mobileErrorElem.html("").slideUp();
				mobileErrorElem.html("").slideUp();
				mobileInput.removeClass("error-input");
			}
			if(mobileInput.hasClass("error-input"))	{
				if((usertype==reg_type_newuser)){
	    			_gaq.push(['_trackEvent', 'reg_page_form_signup', 'signup', 'failure_mobile_invalid']);
	    		}
			} 
		}
        return hasError;
 }
    

 $(".cancel-submit-code").live('click',function(){
    	$(".reg-verify-mobile").show();
    	$(".mobile-code-entry").hide();
}); 
     
function verifyviaMobile() 
{
	if($("#resend-verification-code").hasClass("btn-disabled")){
		return;
	}
	$("#submit-code").removeClass("btn-disabled");
	verifyMobile();
}
	 
	
//var count=0;
function verifyMobile()
{
	
	$("#signupbtn").hide();
	$(".invalid-mobile").hide();
	$(".excedded-attempts").hide();
	$(".incorrect-mobile-code").hide();
	$(".myntra-agreement").hide();
	$("#submit-code").removeClass("btn-disabled");

	var hasError=validateSigninForm(reg_type_newuser);
	
	if(!hasError) {
		var displayExistingUserMesg = true;
		$.ajaxq("testqueue",{
			type: "POST",
			url: "feature_gate_settings.php",
			data: "",
			success: function(msg){
				msg = $.trim(msg);
				if(msg == "1")	{
					$("#signupbtn").show();
					displayExistingUserMesg = false;
					$("#reg_newuser").trigger("click");
					return;
				}
		}});
		
		var mobile = $("#reg_su-mobile").val();
		var login = $("#reg_su-username").val();
		$.ajaxq("testqueue",{
		type: "POST",
		url: "verifymobile.php",
		data: "&mode=generate-code&mobile=" + mobile + "&login=" + login,
		success: function(msg){
			msg = $.trim(msg);
			if (!displayExistingUserMesg) return;
			if (msg == "showCaptcha")
			{
				//if(count > 0) {
					$("#captcha").attr("src", http_loc+'/captcha/captcha.php?id=registrationPage&rand='+Math.random());
				//}
				$("#captcha-form").val("");
				$(".captcha-block").show();
				$("#signupbtn").hide();
				$("#mobile-message").hide();
				$(".mobile-code-entry").hide();
				$("#resend-verification-code").addClass("btn-disabled");
				count++;
			} else if (msg == "verifyFailed"){
				// try another number.show;
				//$(".reg_su-mobile-error").html("Try a different number or after 6 hours").slideDown();
				$(".excedded-attempts").show();
				$("#signupbtn").show();
				$(".mobile-code-entry").hide();
				$(".resend-verification-entry").hide();

				
			} else if (msg == "existingUser") {
				$(".reg-su-username-error").html("Username already exists").slideDown();
				$("#signupbtn").show();

			} else if (msg == "existingNumber") {
					$(".invalid-mobile").html("<font color=red>Your mobile number has already been verified on an existing user account. Please use a different number to sign up.");
					$(".invalid-mobile").show();
					$("#signupbtn").show();
			}
			else{
				$("#mobile-code").val("");
				$("#mobile-message").html("Please enter the verification code that was sent to "+ mobile);
				$("#mobile-message").show();
				$(".mobile-code-entry").show();
				$(".resend-verification-entry").show();
			}
		}
		});
	} 
	else {
		$("#signupbtn").show();
	}
	
}
 
function verifyCode(){
	var mobile = $("#reg_su-mobile").val();
	var login = $("#reg_su-username").val();
	var mobilecode = $("#mobile-code").val();
	$("#signupbtn").hide();
	$(".invalid-mobile").hide();
	$(".common-message").hide();
	$(".incorrect-mobile-code").hide();	
					
	if(mobilecode != '') {
		$.ajax({
		type: "POST",
		url: "verifymobile.php",
		data: "&mode=verify-mobile-code&mobile=" + mobile + "&login=" + login + "&mobile-code=" + mobilecode ,
		success: function(msg){
			msg = $.trim(msg);
			if(msg == "success")	{
				$(".mobile-code-entry").hide();
				$("#submit-code").addClass("btn-disabled");
				$(".mobile-code-verified").show();
				$("#resend-verification-code").addClass("btn-disabled");
				//$(".reg-verify-mobile").hide();
				_gaq.push(['_trackEvent', 'reg_page_form_signup', 'verification_code', 'success']);
				$("#reg_newuser").trigger("click");
			} /*else if(msg == "exceedsAttempts") {
				$(".excedded-attempts").show();
				$("#signupbtn").show();
				$(".mobile-code-entry").hide();
				$(".resend-verification-entry").hide();
			}*/
			else {
				//alert(msg);
				$(".incorrect-mobile-code").show();
				$("#resend-verification-code").show();
				$(".mobile-code-entry").show();
				$("#mobile-message").hide();
				_gaq.push(['_trackEvent', 'reg_page_form_signup', 'verification_code', 'failure']);
			}
			}
			
		});
	}
	else {
		alert("Please enter a mobile code");
		//$(".reg-verify-mobile").show();
	}
}

function verifyCaptcha() {
	if($(".verify-captcha").hasClass("btn-disabled")){
		return;
	}
	if($('#captcha-form').val() == ""){
		$(".captcha-message").html("Please enter the text.");
		$(".captcha-message").addClass("error");
		$(".captcha-message").slideDown();
		return;
	}
	$(".captcha-loading").show();
	$(".verify-captcha").addClass("btn-disabled");
	$(".captcha-message").html("").removeClass("error");
	$(".captcha-message").slideUp();
	$.ajax({
		type: "POST",
		url: "{/literal}{$http_location}{literal}/mobile_verification.php",
		data: "mobile=" + $('#reg_su-mobile').val() + "&userinputcaptcha=" + $('#captcha-form').val() + "&login=" + $('#reg_su-username').val() + "&id=registrationPage",
		success: function(msg){
			
			var resp=$.trim(msg);
			$(".captcha-loading").hide();
			$(".verify-captcha").removeClass("btn-disabled");
			if(resp=='CaptchaConfirmed') {
				$(".captcha-block").hide();
				$("#submit-code").removeClass("btn-disabled");
				$("#resend-verification-code").removeClass("btn-disabled");
				verifyMobile();
			} else if(resp=='WrongCaptcha') {
				$(".captcha-message").html('Wrong Code entered');
				$(".captcha-message").addClass('error');
				$(".captcha-message").slideDown();
		   } 
		}
	});
}

$(document).ready(function() {

	$('#captcha-form').keydown(function(event) {
		if (event.keyCode == '13') {
		event.preventDefault();
		$(".verify-captcha").trigger("click");
		}
	});
});
</script>			
{/literal}
	<script type="text/javascript" src="{$cdn_base}/skin1/js/thickbox-validate.js.jgz"></script>
    <script type="text/javascript">
    {if $smarty.get.message eq 'login_incorrect'}
    {literal}
        $(document).ready(function(){
            _gaq.push(['_trackEvent', 'reg_page_form_signin', 'signin', 'failure_login_not_found']);
            loginUserNameError('Either Username / Password were incorrect')
        });
    {/literal}
    {/if}

    {if $smarty.get.message eq 'password_incorrect'}
    {literal}
        $(document).ready(function(){
            _gaq.push(['_trackEvent', 'reg_page_form_signin', 'signin', 'failure_incorrect_password']);
            loginUserNameError('Either Username / Password were incorrect')
    });
    {/literal}
    {/if}
    {if $smarty.get.regerror eq 'U'}
    {literal}
    $(document).ready(function(){

    signupUserNameError('Username already exists.')
    });
    {/literal}
    {/if}
    </script>
	{literal}
	<script type="text/javascript">
	
	$(document).ready(function(){
	var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	$("#fbregister_form").validate({
     rules:{
				passwd1:{
						required: true,
						minlength:6
				}
            },
            messages: {
				passwd1:{
						required: "This field is required.",
						minlength: "Should be minimum of 6 characters"
				}
            },
            errorPlacement: function(error, element) {
                    if ( element.is(":radio") )
                            error.appendTo( element.parent().next().next() );
                    else if ( element.is(":checkbox") )
                            error.appendTo ( element.next().next() );
                    else
                            error.appendTo( element.next() );
            },
            submitHandler: function(form) {
                    form.submit();
            }
	});

});



function randomString() {
	var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";
	var string_length = 10;
	var randomstring = '';
	for (var i=0; i<string_length; i++) {
		var rnum = Math.floor(Math.random() * chars.length);
		randomstring += chars.substring(rnum,rnum+1);
	}
	return randomstring;
}

function facebookLogin(){
	$('#reg_username').val('{/literal}{$reg_username_entered}{literal}');
	$('#reg_password').val('Password'); 
	$('#cfbconnect_login').val('1');
	$('#login_form').trigger('submit');
}

function facebookRegisterUser() {
	var randPasswd = randomString();
	$('#fbpasswd1').val(randPasswd);
	$('#fbpasswd2').val(randPasswd);
	$('#fbRegistrationCancel').val('1');
	$('#fbregister_form').trigger('submit');
}

function facebookCompleteRegistration(){
	$('#fbpasswd2').val($('#fbpasswd1').val());
	$('#fbregister_form').trigger('submit');
}

 </script>
	{/literal}
	{literal}
    <script type="text/javascript">
    $(document).ready(function(){
        {/literal}
        {if $fb_username_found eq 2}
        {literal}
                tb_show("","#TB_inline?height=115&amp;width=220&amp;inlineId=fbUserLoggingHiddenContent&amp;modal=true","");
                $('#TB_ajaxContent').attr('style', "width:auto; height:auto");
                facebookLogin();
        {/literal}
            {elseif $fb_username_found eq 1}
        {literal}
            // Dialog
                tb_show("","#TB_inline?height=195&amp;width=500&amp;inlineId=fbUserFoundHiddenContent&amp;modal=true","");
                $('#TB_ajaxContent').attr('style', "width:auto; height:auto");
        {/literal}
            {elseif $fb_new_user eq 1}
            var auto_logged_fbuser='{$reg_username_entered}';
        {literal}
            $("#fbFooterNewUserHiddenContentRegister-Name").html(auto_logged_fbuser);
            tb_show("","#TB_inline?height=215&amp;width=480&amp;inlineId=fbFooterNewUserHiddenContentRegister&amp;modal=true","");
            $('#TB_ajaxContent').attr('style', "width:auto; height:auto");
        {/literal}
            {/if}
     });

     {literal}
     this.vtooltip = function() {
        this.xOffset = 20; // x distance from mouse
        this.yOffset = 20; // y distance from mouse

        $(".vtooltip").unbind().hover(
            function(e) {
                if(this.title != "undefined"){
                    this.t = $(this).attr("rel");
                }
                else{
                    this.t = "";
                }
                var elProp = $(e.target).offset();
                this.top = (elProp.top + yOffset); this.left = (elProp.left + xOffset);

                $('body').append( '<div id="vtooltip" style="border:1px solid #FD9B06;filter:alpha(opacity=100);opacity:1;">' + this.t + '</div>' );

                //$('div#vtip #vtipArrow').attr("src", 'images/vtip_arrow.png');
                $('div#vtooltip').css("top", this.top+"px").css("left", this.left+"px").fadeIn("fast");

            },
            function() {
                $("div#vtooltip").fadeOut("fast").remove();
            }
        );

    };
    jQuery(document).ready(function($){vtooltip();})
    {/literal}

    </script>

    <script type="text/javascript">
    var axel = Math.random() + "";
    var a = axel * 10000000000000;
    document.write('<img src="http://ad.doubleclick.net/activity;src=2994863;type=pagev820;cat=regis144;ord=' + a + '?" width="1" height="1" alt=""/>');
    </script>
    <noscript>
        <img src="http://ad.doubleclick.net/activity;src=2994863;type=pagev820;cat=regis144;ord=1?" width="1" height="1" alt="">
    </noscript>
</body>
</html>
