{* $Id: mykritihome.tpl,v 1.88.2.3 2006/07/19 10:19:35 max Exp $ *}
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">


{config_load file="$skin_config"}
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
{include file="site/header.tpl"}
<script type="text/javascript" src="{$SkinDir}/common_js.js"></script>
<script type="text/javascript" src="{$SkinDir}/PopupWindow.js"></script>
<script type="text/javascript" src="{$SkinDir}/divpop_js.js"></script>
<script type="text/javascript" src="{$SkinDir}/myimage.js"></script>
{if $affiliateId neq ''}
<script type="text/javascript" src="{$SkinDir}/js_script/affiliateRegistrationLogin.js"></script>
{/if}
<script type="text/javascript" src="{$SkinDir}/js_script/redirecturl.js"></script>
<script type="text/javascript" src="{$SkinDir}/js_script/numberformat.js"></script>
<script type="text/javascript" src="{$SkinDir}/js_script/email_validate.js"></script>

<script type="text/javascript" src="{$SkinDir}/js_script/ajaxObject.js"></script>
<link rel="stylesheet" href="{$SkinDir}/css/tooltip.css" />
<script type="text/javascript" src="{$SkinDir}/js_script/alttxt.js"></script>
<script type="text/javascript" src="{$SkinDir}/js_script/org_user_js.js"></script>
{literal}
<script language="JavaScript" type="text/JavaScript">
<!--
function showPopup(){
document.getElementById('registerpopup').style.visibility = 'visible';
document.getElementById('registerpopup').style.display = '';
}
function hideMessagePopupDelay(){
 setTimeout("hidePopup()", 3000);
}
function hidePopup(){
 document.getElementById('registerpopup').style.visibility = 'hidden';
 document.getElementById('popup').style.display = 'none';
}
//-->
</script>
<style type="text/css">
.sidemenu p {
  font-size:20px;
}
#logosection .search .searchtextbox {_height:22px;}
#regpopup{font-size:1em ;text-align:left;}
#regpopup td{font-size:1em }

#loginpopup{font-size:1em ;text-align:left;}
#loginpopup td{font-size:1em ;}
#selection .wrapper .body {overflow:hidden;}
#overlay{_height:200%;}
</style>
{/literal}
</head>
<body id="body" >
{if $login_event == 'mycart' && $login}
	{include file="popup_login.tpl" }
{/if}

{if ($register_success == 'created') && $login}
    {include file="popup_register.tpl" }
{/if}
<!-- discount coupon message -->
<div id="registerpopup" >
  <div id="overlay"> </div>
  <div id="{$divid}">
    <div class="wrapper" style="position:absolute; top:300px; left:600px;">
      <div class="head"><a href="javascript:hidePopup();">Click to close</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
      <div class="body">
        <p><strong>{$couponMessage}</strong><br></p>
      </div>
      <div class="foot"></div>
    </div>
  </div>
</div>
{if $couponMessage ne ""}
{literal}
<script type="text/javascript" language="JavaScript 1.2">
	showPopup();
</script>
{/literal}
{/if}
{include file="mkquickregistration.tpl" }
<div id="popup"></div>
<div id="scroll_wrapper">
<div id="container">
{include file="site/top.tpl" }
<div id="wrapper" >
{include file="breadcrumb.tpl" }
    <div id="display">
		<div class="menu_column">{include file="leftpaneMyAccount.tpl" }</div>
		<div class="center_column">
			<div class="form">
				{if $productsInCart == "" &&  $giftcerts == "" }
					<div class="super">
						<p>shopping cart</p>
					</div>
				    <div class="links">
						<p><br/><strong>{$productMessage}</strong></p>
				   		<p align="right" style="font-size:1.1em"><a href="{$http_location}/mkstartshopping.php" title='add more products'>add items</a></p>
					</div>
					<div class="foot"></div>
		    	{else}
					{if $noDiscountCoupons neq '1'}
						{include file="discountcoupon_re.tpl" }
					{/if}
                        {include file="mkcartproducts_re.tpl" } 
				{/if}
			</div>
		</div>
	</div>
</div>
</div>
</div>
<div class="clearall"></div>
{include file="site/footer.tpl" }
    {if $windowid ne ''}
    <script language="JavaScript" type="text/JavaScript">
    jQuery.get('generatecartimages.php?windowid={$windowid}&product={$product}');
    </script>
    {/if}
<div id="navtxt" class="navtext" style="position:absolute; top:-100px; left:0px; visibility:hidden"></div>
<!--popup-->
</body>
</html>
