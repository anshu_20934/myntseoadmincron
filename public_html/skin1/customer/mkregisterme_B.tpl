<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en" xmlns:fb="http://www.facebook.com/2008/fbml">
<head>
	{include file="site/header.tpl" }
    <!--[if lt IE 8]>
    <link rel="stylesheet" href="{$http_location}/skin1/myntra_css/ie.css" type="text/css" media="screen, projection">
    <![endif]-->

<meta property="og:title" content="Shop @Myntra" />
<meta property="og:type" content="company" />
<meta property="og:image" content="{$cdn_base}/skin1/myntra_images/MYNTRA1.png" />
<meta property="og:site_name" content="Myntra.com" />
<meta property="og:url" content="{$http_location}/register.php">
<meta name="description" content="In case you're planning to buy apparel or footwear, here are {$mrp_nonfbNumCoupons} coupons of Rs.{$mrp_nonfbMrpAmount} each that you can use @Myntra . All you've got to do is click here and register! " />
<link rel='image_src' href='{$cdn_base}/skin1/myntra_images/MYNTRA1.png'>

</head>
<body class="{$_MAB_css_classes}">
<img src="{$cdn_base}/skin1/myntra_images/MYNTRA1.png" style="display:none"></img>
{include file="site/menu.tpl" }
<div class="container clearfix">
    <div id="reg-login">
        <div class="lightbox lb-login">
            {include file="site/mod_login.tpl"}
        </div>
    </div>
</div>
    {include file="site/footer.tpl" }
<script type="text/javascript">
    {literal}
    $(document).ready(function() {
        Myntra.InitRegLogin('#reg-login');
    });
    {/literal}
    var axel = Math.random() + "";
    var a = axel * 10000000000000;
    document.write('<img src="http://ad.doubleclick.net/activity;src=2994863;type=pagev820;cat=regis144;ord=' + a + '?" width="1" height="1" alt=""/>');
</script>
<noscript>
    <img src="http://ad.doubleclick.net/activity;src=2994863;type=pagev820;cat=regis144;ord=1?" width="1" height="1" alt="">
</noscript> 

</body>
</html>
