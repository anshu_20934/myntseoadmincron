{* $Id: mykritihome.tpl,v 1.88.2.3 2006/07/19 10:19:35 max Exp $ *}
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

{config_load file="$skin_config"}
<html xml:lang="en" xmlns:fb="http://www.facebook.com/2008/fbml">
<head>
{if $orgId != ""}
	<link rel="stylesheet" href="{$SkinDir}/{$org_css_file}" />
	<link rel="stylesheet" href="{$SkinDir}/modules/organization/css/orgaStyles.css" />
	<script type="text/javascript" src="{$SkinDir}/js_script/org_user_js.js"></script>
{/if}
{include file="mkheadinfo.tpl"}
<script type="text/javascript" src="{$SkinDir}/PopupWindow.js"></script>
<script type="text/javascript" src="{$SkinDir}/myimage.js"></script>
{if $affiliateId neq ''}
	<script type="text/javascript" src="{$SkinDir}/js_script/affiliateRegistrationLogin.js"></script>
{/if}
<script type="text/javascript" src="{$SkinDir}/js_script/numberformat.js"></script>
<script type="text/javascript" src="{$SkinDir}/js_script/email_validate.js"></script>
<link rel="stylesheet" href="{$SkinDir}/css/tooltip.css" />
<script type="text/javascript" src="{$SkinDir}/js_script/alttxt.js"></script>

{literal}
<script language="JavaScript" type="text/JavaScript">
<!--
function showPopup(){
document.getElementById('registerpopup').style.visibility = 'visible';
document.getElementById('registerpopup').style.display = '';
}
function hideMessagePopupDelay(){
 setTimeout("hidePopup()", 3000);
}
function hidePopup(){
 document.getElementById('registerpopup').style.visibility = 'hidden';
 document.getElementById('popup').style.display = 'none';
}
//-->
</script>
{/literal}
</head>
<body id="body" >
{if $login_event == 'mycart' && $login}
	{include file="popup_login.tpl" }
{/if}

{if ($register_success == 'created') && $login}
    {include file="popup_register.tpl" }
{/if}
<!-- discount coupon message -->
<div id="registerpopup" >
  <div id="overlay"> </div>
  <div id="{$divid}">
    <div class="wrapper" style="position:absolute; top:300px; left:600px;">
      <div class="head"><a href="javascript:hidePopup();">Click to close</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
      <div class="body">
        <p><strong>{$couponMessage}</strong><br></p>
      </div>
      <div class="foot"></div>
    </div>
  </div>
</div>
{if $couponMessage ne ""}
{literal}
<script type="text/javascript" language="JavaScript 1.2">
	showPopup();
</script>
{/literal}
{/if}
{if $affiliateId ne '' && $affiliateAuthType ne 'AF_MYNTRA_REGISTRATION'}
	{include  file="modules/affiliates/affiliateemailprovider.tpl" }
{else}
	{include file="mkquickregistration.tpl" }
{/if}
<div id="popup"></div>
<div id="scroll_wrapper">
<div id="container">
{include file="mykrititop.tpl" }
<div id="wrapper" >
{if $orgId != ""}
	{include file="modules/organization$brandShopDir/breadcrumb_organization.tpl"}
{else}
	{include file="breadcrumb.tpl" }
{/if}
    <div id="display">
      <!-- start left panel -->
	{if $orgId != ""}
		{ include file="modules/organization$brandShopDir/orgInviteeLeftLinks.tpl" }
	{elseif $affiliateleftpanel != ''}
		<div class="menu_column">{include file="$affiliateleftpanel" }</div>	
	{else}
		<div class="menu_column">{include file="leftpaneMyAccount.tpl" }</div>
	{/if}
      <!-- end -->
    
	    <div class="center_column">
		 <div class="form">
		    {if $productsInCart == "" &&  $giftcerts == "" }
			       <div class="super">
				    <p>shopping cart</p>
				 </div>
				    <div class="links"><p><br/><strong>{$productMessage}</strong></p>
						{if $affiliateId eq '' && $orgId eq '' && $promotion_key eq ''}
							<p align="right" style="font-size:1.1em"><a href="{$http_location}/mkstartshopping.php" title='add more products'>add items</a></p>
						{/if}
						{if $orgId neq ''}
							<p align="right" style="font-size:1.1em"><a href="{$http_location}/modules/organization/orgBrowseAllProducts.php" title='add more products'>add items</a></p>
						{/if}					
					</div>
				    
				  <div class="foot"></div>
		    {else}
				
		                {if $affiliateId eq '21'}
				        {include file="modules/affiliates/ibibodiscountmsg.tpl" }
				{else}

					{if $noDiscountCoupons neq '1'}
					{include file="discountcoupon.tpl" }
					{/if}
				{/if}
				
				{include file="mkcartproducts.tpl" } 
				
		    {/if}
		</div>
	</div>

  </div>
<div class="clearall"></div>
</div>
   </div>
</div>

{if $RRtype}
	{include file="modules/organization$brandShopDir/footer.tpl"}
	<script type="text/javascript" src="{$SkinDir}/common_js.js"></script>
{else}
	{include file="footer.tpl" }
{/if}

   <div id="navtxt" class="navtext" style="position:absolute; top:-100px; left:0px; visibility:hidden"></div>
   
<!--popup-->
<div id="rrpopup" style="display:none;visibility:hidden;">
	<div class="top" style="display:inline-block;filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='{$http_location}/skin1/modules/organization/images/cw/popuptop.png');">
		<img style="border:0px;filter:progid:DXImageTransform.Microsoft.Alpha(opacity=0);" src="{$http_location}/skin1/modules/organization/images/cw/popuptop.png"/>
	</div>
	<div class="middle">
		<div class="close"><img src="{$cdn_base}/skin1/modules/organization/images/cw/cross.jpg" onclick="closeRRPopup('rrpopup');"/></div>	   	    		
		<p class="parahead">Redeem Message</p>
		<p class="paradetail" id="redeem_msg"></p>    							 	    		   	    		
	</div>
	<div class="bottombroad" style="display:inline-block;filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='{$http_location}/skin1/modules/organization/images/cw/popupbottombroad.png');">
		<img style="border:0px;filter:progid:DXImageTransform.Microsoft.Alpha(opacity=0);" src="{$http_location}/skin1/modules/organization/images/cw/popupbottombroad.png"/>
	</div>
</div>   	    
<!--popup-->
</body>
</html>
