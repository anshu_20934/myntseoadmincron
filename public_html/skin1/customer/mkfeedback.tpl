{* $Id: mykritihome.tpl,v 1.88.2.3 2006/07/19 10:19:35 max Exp $ *}
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xml:lang="en" xmlns:fb="http://www.facebook.com/2008/fbml">
{include file="site/header.tpl" }
{literal}
    <style>
        html{
            overflow: auto;
        }
        
        .MFBmsg{
            display:block;margin:20px;padding:20px;background-color:#cade7a;border:1px solid #5ca631;font-size:16px;
        }

        .MFBerrMsg{
            display:block;margin:20px;padding:20px;background-color:#facbcc;border:1px solid #a71014;font-size:16px;
        }
    </style>
{/literal}

<body  class="{$_MAB_css_classes}" style="margin:0px;padding:0px;font-size:12px;">
{include file="site/top.tpl" }
<div id="scroll_wrapper" style="overflow:hidden">
    <div id="container">
        <div id="display">
            <div class="center_column" style="padding-left:5px">
                <div id="feedbackform" class="form">

                    {include file="main/feedback_info.tpl"}

                </div>
            </div>
        </div>
    </div>
</div>
        <div class="clearall"></div>
        {include file="site/footer.tpl"}        

{literal}
<script type="text/javascript">
    //MFB Questionnaire
    var MFB = $.parseJSON('{/literal}{$MFB_JSON}{literal}');

    //to start with [a-z0-9_] and followed by anychar other than ~@$^
    var MFBtextRegEx = /^[\w]{1}[^\@\$\^\~]*$/;    

	$(document).ready(function(){

        //trigger form submission on click of top button
	    $('#MFB_trigger_submit').click(function(){
	        $('#MFB_submit').trigger('click');
	    });

	    //bind all text,textboxes for mouseout event
	    $.each(MFB,function(qId,qValue){
            if(qValue.q_type == 'text' || qValue.q_type == 'textbox'){

                //get all the options of id starting with MFB[qid]
                answerElements = $('[id^="MFB['+qId+']"]');

                $.each(answerElements,function(key, ele){

                    $(ele).bind('blur', function() {

                        //validate content for maxchars and adjust
                        adjustTextInteractively(this,qId,qValue);
                        
                        //validate for special chars
                        if(isValidAgainstRegex(MFBtextRegEx, $(ele).val())){
                            deHighlightMandatoryQuestion(qId);
                            
                        } else {
                            highlightMandatoryQuestion(qId);
                            alert("Starting letter can only be a-z, 0-9 or _. And there should not be ~@$^ used.");
                        }

                    });

                    $(ele).bind('keydown keyup', function() {
                        adjustTextInteractively(this,qId,qValue);
                    });
                });
            }
        });




        //validate MFB form on submission
	    $('#MFB_submit').click(function(){

            var hasError=false;
            
	        $.each(MFB,function(qId,qValue){                

                //get all the options of id starting with MFB[qid]
                answerElements = $('[id^="MFB['+qId+']"]');
                var local_box_error=true;
                switch(qValue.q_type){

                    case 'continuousradio'    :
                    case 'radio'    :   if(qValue.mandatory==1){
                                            $.each(answerElements,function(key, ele){
                                                if($(ele).attr('checked')){
                                                    local_box_error = false;
                                                    deHighlightMandatoryQuestion(qId);
                                                    return false;//break
                                                }
                                            });

                                            if(local_box_error == true){
                                                highlightMandatoryQuestion(qId);
                                                hasError = true;

                                            }
                                        }
                                        break;

                    case 'checkbox' :   if(qValue.mandatory==1){
                                            $.each(answerElements,function(key, ele){
                                                if($(ele).attr('checked')){
                                                    local_box_error = false;
                                                    deHighlightMandatoryQuestion(qId);
                                                    return false;//break
                                                }
                                            });

                                            if(local_box_error == true){
                                                highlightMandatoryQuestion(qId);
                                                hasError = true;

                                            }
                                        }
                                        break;

                    case 'text'     :   //assuming, always there would only be one textarea for a question
                                        $.each(answerElements,function(key, ele){

                                            if(qValue.mandatory==1){
                                                if(isValidAgainstRegex(MFBtextRegEx, $(ele).val()) && $(ele).val().length <= qValue.maxchars){
                                                    local_box_error = false;
                                                    deHighlightMandatoryQuestion(qId);
                                                    return false;//break
                                                }
                                            } else if($(ele).val() != ''){
                                                if(isValidAgainstRegex(MFBtextRegEx, $(ele).val()) && $(ele).val().length <= qValue.maxchars){
                                                    local_box_error = false;
                                                    deHighlightMandatoryQuestion(qId);
                                                    return false;//break
                                                }
                                            //in case of non-mandatory text on empty don't show error
                                            } else {
                                                local_box_error = false;
                                                return false;//break
                                            }


                                        });

                                        if(local_box_error == true){
                                            highlightMandatoryQuestion(qId);
                                            hasError = true;

                                        }

                                        break;

                    case 'textbox'  :   //assuming, always there would only be one textbox for a question
                                        $.each(answerElements,function(key, ele){

                                            if(qValue.mandatory==1){
                                                if(isValidAgainstRegex(MFBtextRegEx, $(ele).val()) && $(ele).val().length <= qValue.maxchars){
                                                    local_box_error = false;
                                                    deHighlightMandatoryQuestion(qId);
                                                    return false;//break
                                                }
                                            } else if($(ele).val() != ''){
                                                if(isValidAgainstRegex(MFBtextRegEx, $(ele).val()) && $(ele).val().length <= qValue.maxchars){
                                                    local_box_error = false;
                                                    deHighlightMandatoryQuestion(qId);
                                                    return false;//break
                                                }
                                            //in case of non-mandatory textbox on empty don't show error
                                            } else {
                                                local_box_error = false;
                                                return false;//break
                                            }


                                        });

                                        if(local_box_error == true){
                                            highlightMandatoryQuestion(qId);
                                            hasError = true;

                                        }

                                        break;

                }
            });
            
            if(hasError){
                $('.MFBmsg').hide();
                alert("Please select all mandatory questions (marked as *) and check for correct text entered.")
                return false;
                
            } else {
                $('#MFB_form').trigger('submit');
            }

        });
	});

	/* validates string against regEx
     * @param:(string)regEx
     * @param:(string)data
     * @return:(bool)
     */
    function isValidAgainstRegex(regEx, data){
        return regEx.test(data);
    }

    /* highlight error questions
     * @param:(int)questionId
     */
    function highlightMandatoryQuestion(qId){
        $('#MFBQ_'+qId+'').css({border:'1px solid red',padding:'10px',backgroundColor:'#FACBCC'});
        $('.MFBerrMsg').html('Please answer all the mandatory fields '
                                +'(marked with an *) and take care of character limitations in order to submit your '
                                +'feedback. We appreciate your effort towards '
                                +'improving our service. ').show();
    }

    /* highlight non-error questions
     * @param:(int)questionId
     */
    function deHighlightMandatoryQuestion(qId){
        $('#MFBQ_'+qId+'').css({border:'1px solid red',padding:'10px',backgroundColor:'#cade7a'});
    }

    /* adjsut text(textarea) content for max chars and regEx validation
     * @param:(obj)textEle
     * @param:(int)questionId
     * @param:(obj)questionObj
     */
    function adjustTextInteractively(textEle,qId,qObj){

        var maxChars = parseInt(qObj.maxchars);
        var TextLength = parseInt($(textEle).val().length);
        var charsLeft = maxChars - TextLength;
        var upperLimit = 0;

        if(charsLeft <= 0){
        
            charsLeft = 0;
            upperLimit = maxChars-1;
            
            $(textEle).val($(textEle).val().substring(0,upperLimit));
            $(textEle).next().text("max "+charsLeft+" chars");

            highlightMandatoryQuestion(qId);
            alert("Number of characters should not exceed "+qObj.maxchars+" characters" );
                
        } else {
            deHighlightMandatoryQuestion(qId);
            $(textEle).next().text("max "+charsLeft+" chars");
        }
    }
</script>
{/literal}


</body>
</html>