 {if $shopInfo}  
  <form name="affiliatefrm1" id="affiliatefrm1" action="" method="post" enctype="multipart/form-data" onsubmit="return validate.validateShopForm();">
			  <input type="hidden" name="action" id="action" value="editshop"  />
				  <input type="hidden" name="event" id="event" value="EDIT" />
				  <input type="hidden" name="uploadType" id="uploadType"  />
				   <input type="hidden" name="shopID" id="shopID" value="{$shopId}"  />
<div class="form" > 
				  <div class="subhead" >
					  <p>edit your shop</p>
					 
				 </div>
				 
				 <div>
				   

				     <table  class="shoptable"  cellpadding="2" border=0>

						   <tr>
							   <td valign="top" width="25%"><p><b>shop name</b></p></td>
							   <td>{$shopInfo.name}</td>
						  </tr>
						  <tr >
							   <td colspan="2" height="5px;"></td>
						  </tr>
						   <tr>
							   <td valign="top"><p><b>shop url</b></p></td>
							   <td><a href="{$http_location}/shop/{$shopInfo.name}" alt="shop url">{$http_location}/shop/{$shopInfo.name}</a></td>
						  </tr>
						 	 <tr >
							   <td colspan="2" height="5px;"></td>
							 </tr>
							 <input type="hidden" name="titlehelp" id="titlehelp" value="{$helpText.shop_title_help}" />
							 <tr>
							   <td ><p><b>title</b><span class="mandatory">*</span></p></td>
							   <td ><input type="text" name="shoptitle" id="shoptitle"  value="{$shopInfo.title}" size="31" /><a href="javascript:helpPopupObj.showHelpPopupDiv(document.getElementById('titlehelp'));" style="text-decoration:none;" title="help">
									  <img src="{$cdn_base}/skin1/mkimages/help-icon.gif"  style="border:0px solid #CCCCCC;"/>
								  </a>
							   </td>
						     </tr>   
						    <tr >
							   <td colspan="2" height="5px;"></td>
							 </tr>
							  <input type="hidden" name="shoplogohelp" id="shoplogohelp" value="{$helpText.shop_logo_help}" />
						    <tr>
							   <td ><p><b>logo</b><span class="mandatory">*</span>&nbsp;(60*60 px)</p></td>
							   <td>
							      {if $shopInfo.logo_img ne ""}
								     <div id="slogo1" style="visibility:visible;">
								   		<img src="{$cdn_base}/{$shopInfo.logo_img}"  ALT="logo" width="50" />&nbsp;&nbsp;<a href="javascript:uploadNew(document.getElementById('slogo1'),document.getElementById('slogo2'));" >Change</a> 
								   	 </div>
								   	 <div id="slogo2" style="visibility:hidden;display:none;">
								   	   <input type="file"   name="slogo" id="slogo"  />&nbsp;&nbsp;<a href="javascript:uploadFile(document.getElementById('slogo'),'logo');" >Upload Logo</a>&nbsp;&nbsp;<a href="javascript:cancelUpload(document.getElementById('slogo1'),document.getElementById('slogo2'));" >Cancel</a>
								   	 &nbsp;<a href="javascript:helpPopupObj.showHelpPopupDiv(document.getElementById('shoplogohelp'));" style="text-decoration:none;" title="help">
							      <img src="{$cdn_base}/skin1/mkimages/help-icon.gif"  style="border:0px solid #CCCCCC;"/>
					                   </a></div>	
							   											             
							   {/if}
							   
							   
							   </td>
							</tr>
							<tr >
							   <td colspan="2" height="5px;"></td>
							 </tr>
							 <input type="hidden" name="shopbannerhelp" id="shopbannerhelp" value="{$helpText.shop_banner_help}" />
							<tr>
							   <td valign="top"><p><b>banner</b>&nbsp;</p></td>
							   <td  >
							    {if $shopInfo.image ne ""}
								     <div id="sbanner1" style="visibility:visible;">
								   		<img src="{$cdn_base}/{$shopInfo.image}"  ALT="banner" width="100" />&nbsp;&nbsp;<a href="javascript:uploadNew(document.getElementById('sbanner1'),document.getElementById('sbanner2'));" >Change</a> 
								   	 </div>
								   	 <div id="sbanner2" style="visibility:hidden;display:none;">
								   	   <input type="hidden" name="imgpath" id="imgpath" value="{$http_location}">
							       <select name="headbanner" id="headbanner" class="select"  style="width:130px" >
							           <option value="">--select--</option>
							           {section name="hindex"  loop=$shopthemes}
								        <option value="{$shopthemes[hindex].themeid}" {if $shopInfo.head_themeid eq $shopthemes[hindex].themeid} selected {/if}>{$shopthemes[hindex].theme_name}</option>              							         
								   {/section}
							        </select> 
							        {section name="idx"  loop=$shopthemes}
									 <input type="hidden" name="bimg_{$shopthemes[idx].themeid}" id="bimg_{$shopthemes[idx].themeid}" value="{$shopthemes[idx].header_img}" />		
							        {/section}
							        &nbsp;<a style="cursor:pointer;text-decoration:underline;" onClick="javascript:showPreview();">preview</a>&nbsp;&nbsp;
							        &nbsp; <a href="javascript:cancelUpload(document.getElementById('sbanner1'),document.getElementById('sbanner2'));" >Cancel</a>&nbsp;&nbsp;&nbsp;<a href="javascript:helpPopupObj.showHelpPopupDiv(document.getElementById('shopheaderhelp'));" style="text-decoration:none;" title="help">
							      <img src="{$cdn_base}/skin1/mkimages/help-icon.gif"  style="border:0px solid #CCCCCC;"/> </a> <br/><input type="checkbox" name="chkOwnBanner" id="chkOwnBanner"  class="checkbox" value="Y" onclick="javascript:showAndHideBanner();"/>&nbsp;or change your own banner <br/><br/>
							     
								   	<div id="showOwnBanner" style="visibility:hidden;display:none;">
								   	   <input type="file"   name="shopbanner" id="shopbanner"  />&nbsp;&nbsp;<a href="javascript:uploadFile(document.getElementById('shopbanner'),'banner');" >Upload banner</a>&nbsp;&nbsp;<a href="javascript:helpPopupObj.showHelpPopupDiv(document.getElementById('shopbannerhelp'));" style="text-decoration:none;" title="help">
							      <img src="{$cdn_base}/skin1/mkimages/help-icon.gif"  style="border:0px solid #CCCCCC;"/>
					                   </a>&nbsp;(760*76 px)&nbsp;</div>
					                   
								   	 </div>	
							   											             
							   {/if}
							   
							   </td>
						   </tr>
						   <tr >
							   <td colspan="2" height="5px;"></td>
							 </tr>
							 
							  <input type="hidden" name="shopheaderhelp" id="shopheaderhelp" value="{$helpText.shop_header_help}" />
                                
						     
						   <tr >
							   <td colspan="2" height="5px;"></td>
							 </tr>
							  
                            <tr>
							    <td valign="top"><p><b>description</b></p></td>
                            	<td>
							        {include file="main/textarea.tpl" name="shopdesc"  data=$shopInfo.description wid="540" rows="7"}
							   
							   </td>
						    </tr>  							



						    <tr >
							   <td colspan="2" height="5px;"></td>
						   </tr> 
						   <tr>
							   <td ><p><b>shop types</b></p></td>
							   <td ><p>
							   		{section name=idx loop=$shoptypes}
							   		  <input type="radio" name="shoptype"  class="radio" value="{$shoptypes[idx].typeid}"  {if $selectShopType eq $shoptypes[idx].typeid} checked {/if }/>&nbsp;{$shoptypes[idx].shop_type}&nbsp;&nbsp;   
							   		{/section}
							   </p></td>
						   </tr>

						    <tr >
							   <td colspan="2" height="5px;"></td>
						   </tr>  



							
						   <tr>
							   <td ><p><b>configuration</b></p></td>
							   <td ><p style='padding-left:6px;'><input type="checkbox" name="adddesign" id="adddesign" value="Y" class="checkbox"  {if $shopInfo.designs_status eq 'Y'} checked {/if}/>  Add my designs to shop</p></td>
							   
						   </tr>
						   <tr>
							   <td colspan="2" height="5px;">&nbsp;</td>
						   </tr>


						  <tr>
							      <td colspan="2" align="right" style="padding-right:25px;">
							      <input type="submit" name="updyourdesign"  id="updyourdesign" value="update shop" class="submit"/></td>
						   </tr>

							<tr >
							   <td colspan="2" height="5px;"></td>
						   </tr>  

						</table> 
					    </div>	
					  <div class="foot"></div>
					 </div>
                       </form>
 {else}
          <div class="form" > 
			   <div class="subhead" >
				 <p>&nbsp;</p>
  			    </div>
			    <div class="links"> 
			      <p class="mandatory">Your shop is not activated yet.</p>
			     </div>
			      <div class="foot"></div>
			</div>

 {/if}