<script language="javascript" type="text/javascript" src="{$SkinDir}/main/ajax_orderhistory.js"></script> 
{if $inactivationMsg eq ''}
	          <div class="form" > 
				 
				    <div class="subhead" > 
					  <p>Affiliate transactions</p>
				 </div>
				
				 <div class="links">
				  <div class="field">
					<label>
					 <p>
						<strong>Show transactions for</strong>
						<select name="dateduration" id="dateduration" class="select" onChange="javascript:searchAffiliateCommission(this.value, '{$affiliateId}');">
							<option value="0">--select duration--</option>
							<option value="7">&nbsp;Last 7 days</option>
							<option value="1M">&nbsp;Last 1 month</option>
							<option value="3M">&nbsp;Last 3 month</option>
							<option value="1Y">&nbsp;Last 1 year</option>
							<option value="all">&nbsp;All</option>
						</select>
					   </p>
					</label>
				    </div>   
 
				     <div class="clearall"></div>
                     <div id="loadingpopup" style="margin-top:5px;margin-left:188px;visibility:hidden;display:none">
				       <p><img src="{$cdn_base}/images/order_loader.gif"/></p>
				    </div>
				</div>
				
			     <div id="commissionlist"></div>
			
				 <div class="foot"></div>
		</div>
		<br/>
	  	 <!-- affiliate payment list --> 
				 {include file="modules/affiliates/affiliate_paymenthistory.tpl"}
		 <!-- end -->
	   {else}
	            <div class="form" > 
			   <div class="subhead" >
				 <p>&nbsp;</p>
  			    </div>
			    <div class="links"> 
			      <p class="mandatory">{$inactivationMsg}</p>
			     </div>
			      <div class="foot"></div>
			</div>
	   {/if}