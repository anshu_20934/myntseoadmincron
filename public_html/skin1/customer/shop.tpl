{* $Id: mykritihome.tpl,v 1.88.2.3 2006/07/19 10:19:35 max Exp $ *}
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
{config_load file="$skin_config"}
<html>
<head>
{include file="mkheadinfo.tpl" }
<script type="text/javascript" src="{$SkinDir}/js_script/jquery.js"></script>
<script type="text/javascript" src="{$SkinDir}/js_script/thickbox.js"></script>
<script type="text/javascript" src="./skin1/js_script/alttxt.js"></script>
<script type="text/javascript" language="JavaScript 1.2" src="{$SkinDir}/common_js.js"></script>
<script type="text/javascript" language="JavaScript 1.2" src="{$SkinDir}/js_script/contentfilter.js"></script>
<script type="text/javascript" language="JavaScript 1.2" src="{$SkinDir}/js_script/quickregistration.js"></script>
<script type="text/javascript" src="{$SkinDir}/js_script/email_validate.js"></script>
<script type="text/javascript" src="{$SkinDir}/js_script/redirecturl.js"></script>
{literal}
<script type="text/javascript" >
function showMessage(text, divid1, divid2){
	if(text = "full"){
		document.getElementById(divid1).style.visibility = "visible";
		document.getElementById(divid1).style.display = "";
		document.getElementById(divid2).style.visibility = "hidden";
		document.getElementById(divid2).style.display = "none";
	}
	if(text = "short"){
		document.getElementById(divid1).style.visibility = "visible";
		document.getElementById(divid1).style.display = "";
		document.getElementById(divid2).style.visibility = "hidden";
		document.getElementById(divid2).style.display = "none";
	}
}
</script>
<!-- Google Code for default Conversion Page -->
<script language="JavaScript" type="text/javascript">
<!--
var google_conversion_type = 'landing';
var google_conversion_id = 1068346998;
var google_conversion_language = "en_GB";
var google_conversion_format = "1";
var google_conversion_color = "FFFFFF";
//-->
</script>
<style type="text/css">
  iframe {
   height: 0px;
  }
</style>

<script language="JavaScript" src="http://www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<img height=1 width=1 border=0 src="http://www.googleadservices.com/pagead/conversion/1068346998/extclk?script=0">
</noscript>
{/literal}
</head>

<body id="body" >
		<div id="errorpopup1" style="visibility:hidden;">
		  <div id="overlay"></div>
		  <div id="error" style="position:absolute; left:0px; top:60px;">
			<div class="wrapper">
			  <div class="head"><a href="javascript:hideErrorDiv1()">Click to close</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
			  <div class="body">
				<p id="abcd"></p>
			  </div>
			  <div class="foot"></div>
			</div>
		  </div>
		</div>
		<div id="successpopup1" style="visibility:hidden;"> 
		  <div id="overlay"></div>
			  <div id="message"> 
				<div class="wrapper"> 
				  <div class="head"><a href="javascript:hideSuccessDiv1()">Click to close</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
				  <div class="body"> 
					<p><strong>Shop rated successfully.</strong></p>
				  </div>
				  <div class="foot"></div>					
				 </div>
			  </div>
		  </div>
{literal}
<!-- <div id="dhtmltooltip"></div> -->
<script type="text/javascript" src="./skin1/js_script/alttxt.js"></script>
{/literal}

<!-- special offer pop up -->
	<div id="popup"></div>
<!-- end -->
<div id="scroll_wrapper">
<div id="container">
   {include file="shoptop.tpl" }

 {literal}
	     <script language='javascript'>
		 if (navigator.appName=="Microsoft Internet Explorer")
		{
			document.write("<div id='wrapper' style='margin-top:-3px;'>");
		}
		else
		{
			document.write("<div id='wrapper' style='margin-top:0px;'>");
		}
		
	      </script>
{/literal}
	{if $ratings gt 4.5 and $ratings lte 5.0}
		{assign var='starimagename' value='rating_5.0.gif'}
	{elseif $ratings gt 4.0 and $ratings lte 4.5}
		{assign var='starimagename' value='rating_4.5.gif'}
	{elseif $ratings gt 3.5 and $ratings lte 4.0}
		{assign var='starimagename' value='rating_4.0.gif'}
	{elseif $ratings gt 3.0 and $ratings lte 3.5}
		{assign var='starimagename' value='rating_3.5.gif'}
	{elseif $ratings gt 2.5 and $ratings lte 3.0}
		{assign var='starimagename' value='rating_3.0.gif'}
	{elseif $ratings gt 2.0 and $ratings lte 2.5}
		{assign var='starimagename' value='rating_2.5.gif'}
	{elseif $ratings gt 1.5 and $ratings lte 2.0}
		{assign var='starimagename' value='rating_2.0.gif'}
	{elseif $ratings gt 1.0 and $ratings lte 1.5}
		{assign var='starimagename' value='rating_1.5.gif'}
	{elseif $ratings gt 0.5 and $ratings lte 1.0}
		{assign var='starimagename' value='rating_1.0.gif'}
	{elseif $ratings gt 0.0 and $ratings lte 0.5}
		{assign var='starimagename' value='rating_0.5.gif'}
	{else}
		{assign var='starimagename' value='rating_0.0.gif'}
	{/if}
	{if $shopdetail != "empty"}
		<img src='{$shopbanner}'>
               {*include file="breadcrumb.tpl" *}
		<div class="shop_center_column">
			<div style='float:left;width:10px;'>&nbsp;</div>
			<div style='float:left;background-image: url(./skin1/mkimages/shop_name_background_left.jpg);width:7px;height:35px;'></div>
			<div style='float:left;background-image: url(./skin1/mkimages/shop_name_background_repeat.jpg);	background-repeat: repeat-x;font-size:1.3em;height:35px;'>{$shoptitle}</div>	
			<div style='float:left;background-image: url(./skin1/mkimages/shop_name_background_right.jpg);width:7px;height:35px;'></div>
			<div style="padding-top:5px;padding-bottom:5px;padding-right:5px; text-align:right;">
				<div id="rating">
					{if $ratings ne ""}<img src="{$cdn_base}/skin1/mkimages/{$starimagename}" alt="Comment Image" style="border:0px;">{/if}
					<p style="color:#288da9;"><i>average rating:</i>{if $ratings ne ""}{$ratings|string_format:"%.1f"}{else}no rating{/if}</p>
				</div>
				<p>
					<form name="ratingform" action="{$http_location}/shop/{$shop_name}" method="post">
					{if $login eq ""}
			
				<span style="font-size:0.9em; font-weight:bold;">Please <input type="button" value="login" class="submit" onclick="javascript:return checkIfLogged('{$login}', 'shoppage', '{$shop_name}')"> to rate this Shop.</span></td>
			
			{else}
					<span style="color:#288da9;"><i>rate this shop:</i></span>
					<select name="rating" onchange="javascript:rate('{$shop_name}','{$pg}','{$sortMethod}');" style="width:90px; font-size:0.7em;">
					<option value="" selected>select rating</option><option value="1">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
					<option value="4">4</option>
					<option value="5">5</option>
					</select>
					{/if}
					</form>
				</p>
			</div>
	   </div>

		
				<div class="shopinfo">
					<div class="shophead">about designer{if $login eq $shopdetail.ownerid}&nbsp;&nbsp;(<a href="mkMyAccount.php" style="color:#ffffff;"><b>edit</b></a>){/if}</div>
					<div style='float:left;width:20px;'>&nbsp;</div>
					<div class="shophead">about shop</div/>

					<div class="shopdetail">
					  {if $myimage ne ''}
						<div style='float:left;'><img src="{$cdn_base}/images/designer/T/{$myimage}" style='border:1px solid #cccccc'></div>
					  {else}
					    <div style='float:left;'><img src="{$cdn_base}/default_image.jpg" style='border:0px;'></div>
					  {/if}	 
							<div style='float:left;width:280px;padding-left:5px;'>
							<div style='float:left;font-weight:bold;text-decoration:underline;'>{$mynname}</div><br/>
							<div style='float:left;font-weight:normal;font-size:0.8em;'><i>{$mycity}, {$mystate}</i></div><br/>
							<div style='float:left;font-weight:normal;font-size:0.8em;'><i>Total designs {$totalproducts}</i></div><br/>
							{$aboutme}</div>

					</div/>
					<div style='float:left;width:20px;'>&nbsp;</div>
					<div class="shopdetail">
						<div style='float:left;'><img src='{$shoplogo}' style='border:1px solid #cccccc;'></div>
						<div style='float:left;width:280px;padding-left:5px;'>{$shopdescription}</div>

					</div>
					<div class="shopdesignhead">
					designs in my shop
					</div>
					{include file="shopsortpagination.tpl" }


					
					<div class="display">
					
						{section name=allproducts loop=$allproducts}
						     {if $allproducts[allproducts][10] }
		  						{assign var="portal_img" value=$allproducts[allproducts][10]}
		 					 {else}
		   						 {assign var="portal_img" value=$allproducts[allproducts][8]}
		 					 {/if}	
							<div class="products">
								
								<a href='{$allproducts[allproducts][1]|replace:" ":"-"}/{$allproducts[allproducts][5]|replace:" ":"-"}/FC/PD/{$allproducts[allproducts][0]}?shopid={$shopid}'>
			
								<div style="background-image: url({$portal_img});background-position: center; background-repeat: no-repeat; margin:5px;" >
								<div style="text-align: center;background-image: url(./images/b.gif);width: 160px; height: 160px; vertical-align: middle;border:0px;">
										<img src="{$cdn_base}/images/b.gif"  alt="{$allproducts[allproducts][1]} {$allproducts[allproducts][5]}" style="border:0px;" />
								 </div>
								</div>
								</a>
								<div style="float:left;width:170px;">
								<div style='color:#333333;font-size:0.6em;font-weight:normal;width:170px;'><a href='{$allproducts[allproducts][1]|replace:" ":"-"
}/{$allproducts[allproducts][5]|replace:" ":"-"
}/FC/PD/{$allproducts[allproducts][0]}?shopid={$shopid}'>{$allproducts[allproducts][1]|replace:"-":" "}</a></div>
								 <a href="?img={$allproducts[allproducts][11]|substr:21:70}" class="thickbox" onmouseover="writetxt('see large image')" onmouseout="writetxt(0)">
								<div style="float: right; width: 20px; height: 15px; border: 0px #cccccc solid; margin-top:-12px;  background-image: url({$cdn_base}/skin1/mkimages/icon_fullsize.gif); background-repeat: no-repeat; background-position: top right;margin-right:2px;"></div></a></div>
								
								<div style='color:#333333;font-size:0.6em;font-weight:normal;'>{$allproducts[allproducts][5]}</div>
								<div style='color:#333333;font-size:0.6em;font-weight:normal;'>{$allproducts[allproducts][2]}
        
								</div>
		
							</div>
							
						{/section}
					</div>
					{include file="shopcomments.tpl" }
				</div>		

	{else}

				
		
		<p><strong>Your shop is not activated yet.</strong></p>		
			
	{/if}

	
		

	<div class="clearall"></div>
</div>

{include file="footer.tpl" }


</div>
</div>
<div id="navtxt" class="navtext" style="position:absolute; top:-100px; left:0px; visibility:hidden"></div>
<div id="errorpopup" style="visibility:hidden;display:none;">
<div id="overlay"> </div>
  <div id="selection">
   <div class="wrapper">
      <div class="head">
               <p><a href="javascript: hideErrorDiv()"><img src="{$cdn_base}/skin1/mkimages/click-to-close.gif" border="0"></a>
    please login or register to proceed</p>
    </div>

      <div class="body" style="padding:10">
      <br>
        <p>
        <table style="width:540px;height:auto;border: 0px solid #999999;padding: 2px;">
            <tr>
                <td style="vertical-align: top;">
                <form action="" method="post" name="loginform">
                <input type="hidden" name="coupon" id="coupon"  />
                    <table id="loginpopup" style="visibility:visible;width:205px;height:auto;font-size: 0.7em;border: 1px solid #999999;">
                        <tr>
                            <td colspan="2" style="font-size:1.3em;color:#808080"><strong>registered users login here</strong></td>
                        </tr>
                         <tr><td colspan="2">&nbsp;</td></tr>
                        <tr id="loginerror" style="visibility:hidden;display:none;">
                            <td colspan="2" align="center">
                                <span class="mandatory">username and password do not match. please try again</span>
                            </td>
                        </tr>

                        <tr>
                            <td>e-mail</td>
                            <td><input type="text" name="username" id="username" onblur="if(this.value=='') this.value=this.defaultValue;" onfocus="if(this.value==this.defaultValue) this.value='';" value="e-mail" style="color:#ACACAC;cursor:text;" onkeypress='changeColor("username");' autocomplete="off"/></td>
                        </tr>
                        <tr>
                            <td>password</td>
                            <td><input type="password" name="password" id="password" onblur="if(this.value=='') this.value=this.defaultValue;" onfocus="if(this.value==this.defaultValue) this.value='';" value="password" style="color:#ACACAC" onkeypress='changeColor("password");'/></td>
                        </tr>
                        <tr><td colspan="2">&nbsp;</td></tr>
                        <tr>
                            <td colspan="2" align="left">
                            <input type="button" border="0" value="login" name="submitButton" class="submit"
                            onclick="javascript:loginData2QueryString(this.form);"/>
                            </td>
                        </tr>
                    </table>
                    {if $referer}
                        <input type="hidden" name="addreferer" value="{$referer}" />
                    {/if}
		    <input type="hidden" name="redirect_page" id="redirect_page_login" value="" />
                    <input type="hidden" name="refid" id="refid" value="" />
                    <input type="hidden" name="usertype" value="C" />
                    <input type="hidden" name="redirect" value="1" />
                    <input type="hidden" name="mode" value="login" />
                    <input type="hidden" name="filetype" value="{$pagename}">
                    </form>
                     <table id="loginloader" style="visibility:hidden;display:none;width:205px;height:142px;font-size: 0.7em;border: 1px solid #999999;">
                        <tr>
                            <td colspan="2" style="vertical-align: middle;"><center><img src="{$cdn_base}/images/popup_login.gif" /></center></td>
                        </tr>
		    </table>
                </td>
                <td style="vertical-align: top;">&nbsp;</td>
                <td style="vertical-align: top;">
                <form  method="post" name="ajaxregisterform" >
 				  <input type="hidden" name="rcoupon" id="rcoupon"  />
                    <input type="hidden" name="usertype" id="usertype" value="C" />
		    <input type="hidden" name="redirect_page" id="redirect_page_reg" value="" />
                    <input type="hidden" name="refid" id="reg_refid" value="" />
                    {if $referer}
                       <input type="hidden" name="addreferer" value="{$referer}" />
                    {/if}
                    <table id="regpopup" style="visibility:visible;width:335px;height:auto;font-size: 0.7em;border: 1px solid #999999;">
                        <tr>
                            <td colspan="2" style="font-size:1.3em;color:#808080"><strong>new users - quick register</strong></td>
                        </tr>
                        <tr><td colspan="2">&nbsp;</td></tr>
                        <tr id="errormessage" style="visibility:hidden;display:none;">
                            <td colspan="2" align="center">
                                <span class="mandatory"><div id="displayerror"></div></span>
                            </td>
                        </tr>
                        <tr>
                            <td>first name<span class="mandatory">*</span></td>
                            <td><input type="text" name="firstname" id="firstname" onblur="if(this.value=='') this.value=this.defaultValue;" onfocus="if(this.value==this.defaultValue) this.value='';" value="firstname" style="color:#ACACAC" onkeypress='changeColor("firstname");'/></td>
                        </tr>
                        <tr>
                            <td>last name<span class="mandatory">*</span></td>
                            <td><input type="text" name="lastname" id="lastname" onblur="if(this.value=='') this.value=this.defaultValue;" onfocus="if(this.value==this.defaultValue) this.value='';" value="lastname" style="color:#ACACAC" onkeypress='changeColor("lastname");'/></td>
                        </tr>
                        <tr>
                            <td>e-mail<span class="mandatory">*</span>&nbsp;<span class="greymandatory">(username)</span></td>
                            <td><input type="text" name="uname" id="uname" onblur="if(this.value=='') this.value=this.defaultValue;" onfocus="if(this.value==this.defaultValue) this.value='';" value="e-mail" style="color:#ACACAC" onkeypress='changeColor("uname");'/>
                            </td>
                        </tr>
                        <tr>
                            <td>password<span class="mandatory">*</span>&nbsp;<span class="greymandatory">(min 6 characters)</span></td>
                            <td><input type="password" name="passwd1" id="passwd1"  onblur="if(this.value=='') this.value=this.defaultValue;" onfocus="if(this.value==this.defaultValue) this.value='';" value="password"/style="color:#ACACAC" onkeypress='changeColor("passwd1");'></td>
                        </tr>
                        <tr>
                            <td>confirm password<span class="mandatory">*</span></td>
                            <td><input type="password" name="passwd2" id="passwd2" onblur="if(this.value=='') this.value=this.defaultValue;" onfocus="if(this.value==this.defaultValue) this.value='';" value="password" style="color:#ACACAC" onkeypress='changeColor("passwd2");'/></td>
                        </tr>
                        <tr><td colspan="2">&nbsp;</td></tr>
                        <tr>
                            <td colspan="2" align="left">
                            <input type="button" border="0" value="register" name="submitButton" class="submit"
                            onclick="javascript:formValidation(this.form);"/>
                            </td>
                        </tr>
                    </table>
                    </form>
                    <table id="regloader" style="visibility:hidden;display:none;width:335px;height:174px;font-size: 0.7em;border: 1px solid #999999;">
                        <tr>
                            <td colspan="2" style="vertical-align: middle;"><center><img src="{$cdn_base}/images/popup_login.gif" /></center></td>
                        </tr>
                	</table>
                </td>
            </tr>
        </table>
        </p>

     </div>

      <div class="foot"></div>

    </div>

  </div>
  </div>
{literal}
	<script type="text/javascript">
		function hideErrorDiv1()
		{
			document.getElementById('errorpopup1').style.visibility = 'hidden';
		}
		function hideSuccessDiv1()
		{
		    document.getElementById('successpopup1').style.visibility = 'hidden'; 
		}
		
		function hideWithDelay1()
		{
		    setTimeout("hideSuccessDiv()", 7000);
		}
		
		function showSuccessDiv1()
		{
		    document.getElementById('successpopup1').style.visibility = 'visible'; 
		}
		function showErrorDiv1()
		{
		    document.getElementById('errorpopup1').style.visibility = 'visible'; 
		}
		function rate(shopname,pg,param){
			 var url = "shop.php";
			 var rating = document.ratingform.rating.value;
			 var poststr = "srating=srating&shopname2=" + encodeURI(shopname) +"&pg2=" + encodeURI(pg)+"&param2="+encodeURI(param)+"&rating="+encodeURI(rating);
			 request = createRequest();
			 sendRequest(request,url,poststr);
		}
		function sendRequest(request,url,parameters) {

		  request.onreadystatechange = show_rating;
		  request.open("POST", url, true);
		  request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		  request.setRequestHeader("Content-length", parameters.length);
		  request.setRequestHeader("Connection", "close");
		  request.send(parameters);
		}
		function show_rating() {
			 if (request.readyState == 4) {
				if (request.status == 200) {
					var response = request.responseText;
					var array = response.split("###");

					var text = array[0];
					var ard = array[1];
					var cry = array[2];
					var error = "<strong>"+ard+"</strong><br><strong>"+cry+"</strong>";
					var Element = document.getElementById("rating");
					var pelement = document.getElementById("abcd");

					if((cry == "")&&(ard == "")){
						Element.innerHTML = text;
						showSuccessDiv1();
					}
					else{
						pelement.innerHTML = error;
						showErrorDiv1();
					}
				}
				else {
					alert("Error! Request status is " + request.status);
				}
			}
		}
		function createRequest() {
		  var request = null;
		  try {
			request = new XMLHttpRequest();
		  } catch (trymicrosoft) {
			try {
			  request = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (othermicrosoft) {
			  try {
				request = new ActiveXObject("Microsoft.XMLHTTP");
			  } catch (failed) {
				request = null;
			  }
			}
		  }

		  if (request == null) {
			alert("Error creating request object!");
		  } else {
			return request;
		  }
		}

		var request = createRequest();
	</script>
{/literal}

</body>
</html>
