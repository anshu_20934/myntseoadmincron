<table border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td  class="textblack"><strong>Top 5 Reasons to design your own product</strong></td>
  </tr>
</table>
  <br>
  <table width="100%"  border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td height="21"  class="textblack"><strong>&raquo; </strong>Have a great idea you want to share with your friends.</td>
    </tr>
    <tr>
      <td height="21"  class="textblack"><strong>&raquo; </strong>Create a personalizes gift for someone you care about.</td>
    </tr>
    <tr>
      <td height="21"  class="textblack"><strong>&raquo; </strong>Have something funny to say? Wear it on a T-shirt.</td>
    </tr>
    <tr>
      <td height="21"  class="textblack"><strong>&raquo; </strong>Got any creative idea? Make money selling your designs.</td>
    </tr>
    <tr>
      <td height="21"  class="textblack"><strong>&raquo; </strong>Use your spare time to get in touch with your inner artist!</td>
    </tr>
</table>