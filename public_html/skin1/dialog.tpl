{* $Id: dialog.tpl,v 1.25 2005/12/20 08:50:49 max Exp $ *}
{if $printable ne ''}
{include file="dialog_printable.tpl"}
{else}

{if $usertype eq "C"} {* Crystal_Blue *}

 <table cellpadding="0" cellspacing="0" {$extra}>
<tr>
<td><table cellpadding="0" cellspacing="0" width="100%" class="DialogTitleBox">
<tr>
<td valign="top" class="DialogTitleBorder"><img src="{$cdn_base}/skin1/mkimages/spacer.gif" width="1" height="21" alt="" /></td>
<td  width="100%" height="21" valign="middle">&nbsp;&nbsp;&nbsp;&nbsp;<font class="DialogTitleFont"></font></td>
<td valign="top" class="DialogTitleBorder"><img src="{$cdn_base}/skin1/mkimages/spacer.gif" width="1" height="21" alt="" /></td>
</tr>
</table></td>
</tr> 

{else} {* Crystal_Blue *}

<table cellspacing="0" {$extra}>
<tr> 
<td class="DialogTitle">{$title}</td>
</tr> 

{/if} {* Crystal_Blue *}

 <tr><td class="DialogBorder"><table cellspacing="1" class="DialogBox">
<tr><td class="DialogBox" valign="{$valign|default:"top"}">{$content}
&nbsp;
</td></tr>
</table></td></tr>
</table> 
{/if}
