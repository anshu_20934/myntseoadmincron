<form id="ratingreviewform" method="post" action="">
	<div id="addReviewRatingWindow" style="display:none">
			<div id="pro-review">
				{if $login eq ''}
					<h2 class="h2b"><p style="float:left;margin:0;">Product Review</p><a href="javascript:void(0);" class="no-decoration-link right" onClick="removeCommentsPopup()">Close</a></h2>
					<div class="icon-line">
					<div>User not logged in. Please <a class="no-decoration-link" href="{$http_location}/register.php">login</a> to rate and review the product.</div>
				{else}
					<h2 class="h2b"><p style="float:left;margin:0;">Product Review</p><a href="javascript:void(0);" class="no-decoration-link right" onClick="closeDisclosures()">Close</a></h2>
					<div class="icon-line">
					<div class="user-info clearfix"><label>User</label>{$login}</div>
					<div class="product-rating-div clearfix">
						<label>Product Rating</label>
						<div class="ratingbuttons">
							<input type="radio" name="ratingvalue" value="5" /><label>Great</label>
							<input type="radio" name="ratingvalue" value="4" /><label>Good</label>
							<input type="radio" name="ratingvalue" value="3" checked /><label>Average</label> 
							<input type="radio" name="ratingvalue" value="2" /><label>Poor </label>
							<input type="radio" name="ratingvalue" value="1" /><label>Bad</label>
						</div>
					</div>
					<div class="product-review-div clearfix">
						<label>Product Review</label>
						<textarea class="review-textarea" id="review-textarea"></textarea>
					</div>
					<div class="submit-btn-div">
						<a id="submit_rating_form" class="button-img">Submit</a>
					</div>
				{/if}
			</div>
		</div>
</form>

<script>
	var generictype='{$generictype}';
	var generictypeid='{$generictypeid}';
	var sortmode='none';
</script>