
<div class="subhead">
            <p>products contained in order</p>
          </div>
	{if $productsInCart != 0}
	 {foreach name=outer item=product from=$productsInCart}
	{foreach key=key item=item from=$product}
		{if $key eq 'productId'}
		{assign var='productId' value=$item}
		{/if}

		{if $key eq 'producTypeId'}
		{assign var='producTypeId' value=$item}
		{/if}

		{if $key eq 'productStyleId'}
		{assign var='productStyleId' value=$item}
		{/if}

		{if $key eq 'productStyleName'}
		{assign var='productStyleName' value=$item}
		{/if}

		{if $key eq 'amount'}
		{assign var='quantity' value=$item}
		{/if}


		{if $key eq 'price'}
		{assign var='productPrice' value=$item}
		{/if}

		{if $key eq 'productTypeLabel'}
		{assign var='productTypeLabel' value=$item}
		{/if}

		{if $key eq 'totalPrice'}
		{assign var='totalPrice' value=$item}
		{/if}

		{if $key eq 'optionNames'}
		{assign var='optionNames' value=$item}
		{/if}

		{if $key eq 'optionValues'}
		{assign var='optionValues' value=$item}
		{/if}

		{if $key eq 'designImagePath'}
		{assign var='designImagePath' value=$item}
		{/if}

		{if $key eq 'discount'}
		{assign var='discount' value=$item}
		{/if}

	{/foreach}



	  <div>
              <table cellpadding="2" class="printtable">

                <tr>
                  <td class="align_left" style="width:70%"><strong>Product Type:</strong>&nbsp;{$productTypeLabel}</td>
		  <td class="align_left" rowspan="4" colspan="2" >
		  {if $designImagePath != ""}
			<img src="{$designImagePath}"  class="greyborder"></td>
		 {else}
			<img src="{$cdn_base}/default_image.gif"  />
		{/if}

		  </td>
                </tr>

                <tr>
                  <td class="align_left" style="width:70%"><strong>Product Style:</strong>&nbsp;{$productStyleName}</td>
                </tr>
		{section name=options loop=$optionValues}
                <tr>
               <!--    <td class="align_left" >
		  <strong>Color:</strong> White -->


				 <td class="align_left"><strong>{$optionNames[options]}:&nbsp;</strong>{$optionValues[options]}</td>

		</td>

                </tr>
		{/section}


		 <tr>
			<td class="align_left" style="width:90%"><strong>Quantity:</strong>&nbsp;{$quantity}</td>

                </tr>

                <tr>
		  
		  <td class="align_left" ><strong>Unit Price:</strong>&nbsp;Rs.{$productPrice|string_format:"%.2f"}</td>
		  <td class="align_left" ><strong>Price:</strong>&nbsp;Rs.{$totalPrice|string_format:"%.2f"}</td>
                </tr>
              </table>
            </div>
            <div class="links"> &nbsp; </div>

                            {/foreach}
			    {/if}

	{if $giftcerts != 0}
	{assign var=sno value=0}
	{foreach name=outer item=gift from=$giftcerts}
	{foreach key=key item=item from=$gift}
				{if $key eq 'purchaser'}
					{assign var='purchaser' value=$item}
				{/if}
				{if $key eq 'quantity'}
					{assign var='quantity' value=$item}
				{/if}
				{if $key eq 'recipient'}
					{assign var='recipient' value=$item}
				{/if}
				{if $key eq 'amount'}
					{assign var='amount' value=$item}
				{/if}
				{if $key eq 'totalamount'}
					{assign var='totalamount' value=$item}
				{/if}
				{if $key eq 'send_via'}
					{assign var='send_via' value=$item}
				{/if}

				{if $key eq 'recipient_email'}
					{assign var='recipient_email' value=$item}
				{/if}
				{if $key eq 'recipient_firstname'}
					{assign var='recipient_firstname' value=$item}
				{/if}

				{if $key eq 'recipient_lastname'}
					{assign var='recipient_lastname' value=$item}
				{/if}

				{if $key eq 'recipient_address'}
					{assign var='recipient_address' value=$item}
				{/if}

				{if $key eq 'recipient_city'}
					{assign var='recipient_city' value=$item}
				{/if}

				{if $key eq 'recipient_zipcode'}
					{assign var='recipient_zipcode' value=$item}
				{/if}

				{if $key eq 'recipient_county'}
					{assign var='recipient_county' value=$item}
				{/if}

				{if $key eq 'recipient_statename'}
					{assign var='recipient_statename' value=$item}
				{/if}

			{/foreach}

		<div>
              <table cellpadding="2" class="printtable">

                <tr>

                  <td class="align_left" style="width:70%"><strong>From:</strong>&nbsp;{$purchaser}</td>

                  <!-- <td class="align_left" rowspan="4"></td> -->
		  <td class="align_left" rowspan="3" colspan="2" >

			<img src="{$cdn_base}/skin1/images/gift.gif" alt="" />


		  </td>
                </tr>

                <tr>
                  <td class="align_left"><strong>To:</strong>&nbsp;{$recipient}</td>
                </tr>

                <tr>
			{if $send_via eq 'E'}
				<td class="align_left" ><strong>Recipient Email:</strong>&nbsp;{$recipient_email}</td>
			{/if}
			{if $send_via eq 'P'}
				<td class="align_left" ><strong>Recipient Address:</strong>&nbsp;{$recipient_firstname}&nbsp; {$recipient_lastname}<br/>{$recipient_address}<br/>{$recipient_city}<br/>{$recipient_country}</td>
			{/if}

                </tr>

		 <tr>
			<td class="align_left" style="width:90%"><strong>Quantity:</strong>&nbsp;{$quantity}</td>
                </tr>

                <tr>
		  <td class="align_left" ><strong>Unit Price:</strong>&nbsp;Rs.{$amount|string_format:"%.2f"}</td>
		  <td class="align_left" ><strong>Price:</strong>&nbsp;Rs.{$totalamount|string_format:"%.2f"}</td>
                </tr>
              </table>
            </div>
	    <div class="links"> &nbsp; </div>
	    {assign var=sno value=$sno+1}
		{/foreach}
		{/if}


          <div>
            <table cellpadding="2"  class="printtable">
			
		     <tr class="font_bold">
			<td class="align_right" colspan="8">Amount:</td>
			<td class="align_right">Rs.{$grandTotal|string_format:"%.2f"}</td>
		      </tr>
		      
		       {if ($totaloffersdiscount != "") && ($totaloffersdiscount gt 0)}
		       <tr class="font_bold">
			<td class="align_right" colspan="8">Total discount</td>
			<td class="align_right">Rs.{$totaloffersdiscount|string_format:"%.2f"}</td>
		      </tr>
		      {/if}
		
		 <tr class="font_bold">
				<td class="align_right" colspan="8">Tax</td>
				<td class="align_right">Rs.{$vat|string_format:"%.2f"}</td>
		 </tr>
		 <tr class="font_bold">
				<td class="align_right" colspan="8">Shipping Cost  </td>
				<td class="align_right"><div id="shipcost">Rs.{$shippingRate}</div></td>
			 </tr>
		      <tr class="font_bold">
			<td class="align_right" colspan="8">Amount to be paid:</td>
			<td class="align_right">Rs. {$totalafterdiscount|string_format:"%.2f"}</td>
		      </tr>
            </table>
          </div>

          <div class="foot"></div>

