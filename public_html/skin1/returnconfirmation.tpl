<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en" xmlns:fb="http://www.facebook.com/2008/fbml">
	<head>
		<META http-equiv="Content-Style-Type" content="text/css">
		<META http-equiv="Content-Script-Type" content="type/javascript">
		<META http-equiv="Content-Type" content="text/html; charset=utf-8">
		{include file="site/header.tpl" }
	</head>
	<body>
	    <div class="container clearfix">
	    	{include file="site/menu.tpl" }
	    	<div class="section-heading prepend">Return Submitted</div>
	    	<div class="blue-divider"></div>
	    	<div class="divider">&nbsp;</div>
	    	<div id="return-details" class="left" style="width: 580px; padding: 10px;">
	    		<div>Your return request has been submitted. Please note your <b>Return Number #{$returnid}</b>.</div>
	    		<div class="divider">&nbsp;</div>
	    		<div>Following are the details of your return request</div>
	    		<div class="item-img">
                 	<img src="{$return_req.designImagePath}"  style="border:1px solid #CCCCCC;">
                </div>
                <div class="item-info" style="width: 400px;">
                    <ul class="cart-order">
                        {if $return_req.productStyleName}<li><div class="order-label"></div><div><b>{$return_req.productStyleName}</b></div></li> {/if}
                        <li> <b>Size</b>: { $return_req.sizeoption } </li>
                        <li> <b>Quantity</b>: { $return_req.quantity } </li>
                        <li> <b>Reason</b>: { $return_req.reason } </li>
                       	<li> <b>Details</b>: { $return_req.description } </li>
                    </ul>
                </div> 
                <div class="divider">&nbsp;</div>
            </div>
            <div class="divider">&nbsp;</div>
			<div id="next-steps" class="left next-steps">
				<h3>Next Steps</h3>
				<ul>
					<li>Open the <a href="mymyntra.php?view=mypendingreturns">My Returns</a> section under My Myntra.</li>
					<li>Click the “Download returns form”.</li>
					<li>Print out the downloaded form, fill out the required details and sign the form.
						   <p>No-printer option: Please note the following particulars on a sheet of a paper to serve 
						   as a returns form:</p>
						    <p>a. Order No</p>
							<p>b. Return No</p>
							<p>c. Customer Login</p>
							<p>d. Request Date</p>
					</li>
					<li>Re-pack the product you want to return making sure to include all accompanying material the item was originally delivered to you with.</li>
					<li>Paste the returns form on the reverse side of package such that the entire form is visible.</li>
					<li>Download and print address label from My Return Requests section on My Myntra.
							<p>No-printer option: Please inscribe the following address on the consignment:</p>
							<p>The Logistics Manager,<br>
							Myntra Designs Pvt Ltd.,<br>
							7th Mile, Krishna Reddy Industrial Area, Kudlu Gate,<br>
							Opp Macaulay High School, Behind Andhra Bank ATM,<br>
							Bangalore 560 068<br>
							080-4902-3100
					</li>
	            	{if $return_req.return_mode eq 'pickup' }
	            		<li>Please hand over the consignment to our logistics partner, when they visit your doorstep for pickup. Please retain the receipt (docket no.) till we have confirmed receipt of the consignment.</li>
		    		{else}
		    			<li>Ship the package and retain the receipt (docket no.) till we have confirmed receipt of the consignment.</li>
		    		{/if}
		    	</ul>
	    	</div>
		    <div class="divider">&nbsp;</div>
		    <div id="link-section">
		    	<a href="mymyntra.php?view=mypendingreturns">Go To My Returns &raquo;</a>
		    </div>
		</div>
		    <div class="divider">&nbsp;</div>
    		{include file="site/footer.tpl"}
			<script type="text/javascript" src="http://www.plaxo.com/css/m/js/util.js"></script>
			<script type="text/javascript" src="http://www.plaxo.com/css/m/js/basic.js"></script>
			<script type="text/javascript" src="http://www.plaxo.com/css/m/js/abc_launcher.js"></script>
	</body>
</html>