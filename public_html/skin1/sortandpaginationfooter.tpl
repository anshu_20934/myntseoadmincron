{if $allproducts != 'empty'}
<div class="links"> <br/>
	<div class="navigator">
		<div class="left">
			{assign var=sortparam1 value=USERRATING}
			{assign var=sortparam2 value=BESTSELLING}
			{assign var=sortparam3 value=MOSTRECENT}
			{assign var=sortparam4 value=EXPERTRATING}
			<p>
			Sort By :
			 {if $sortMethod eq MOSTRECENT}
				<a rel="nofollow" href="{$http_location}/{$sorturl}/{$sortparam4}/1">Expert Rating</a>&nbsp;|&nbsp;<a rel="nofollow" href="{$http_location}/{$sorturl}/{$sortparam1}/1">User Rating</a>&nbsp;|&nbsp;<a rel="nofollow" href="{$http_location}/{$sorturl}/{$sortparam2}/1">Best Selling</a>&nbsp;|&nbsp;Just Arrived
				{/if}   
				{if $sortMethod eq BESTSELLING}
				<a rel="nofollow" href="{$http_location}/{$sorturl}/{$sortparam4}/1">Expert Rating</a>&nbsp;|&nbsp;<a rel="nofollow" href="{$http_location}/{$sorturl}/{$sortparam2}/1">User Rating</a>&nbsp;|&nbsp;Best Selling&nbsp;|&nbsp;<a rel="nofollow" href="{$http_location}/{$sorturl}/{$sortparam3}/1">Just Arrived</a>
				{/if}
				{if $sortMethod eq USERRATING}
				<a rel="nofollow" href="{$http_location}/{$sorturl}/{$sortparam4}/1">Expert Rating</a>&nbsp;|&nbsp;User Rating&nbsp;|&nbsp;<a rel="nofollow" href="{$http_location}/{$sorturl}/{$sortparam2}/1">Best Selling</a>&nbsp;|&nbsp;<a rel="nofollow" href="{$http_location}/{$sorturl}/{$sortparam3}/1">Just Arrived</a>
				{/if}
				{if $sortMethod eq EXPERTRATING}
				Expert Rating&nbsp;|&nbsp;<a rel="nofollow" href="{$http_location}/{$sorturl}/{$sortparam1}/1">User Rating</a>&nbsp;|&nbsp;<a rel="nofollow" href="{$http_location}/{$sorturl}/{$sortparam2}/1">Best Selling</a>&nbsp;|&nbsp;<a rel="nofollow" href="{$http_location}/{$sorturl}/{$sortparam3}/1">Just Arrived</a>
			{/if}			 
			</p>
		</div>

		<div class="right">
			<p>
				{if $totalpages > 1}{$paginator}{/if}
			</p>
		</div>
		<div class="clearall"></div>
	</div>
</div>
{/if}

<div class="foot"></div>
<div id="popupimage" name="popupimage" style="visibility:hidden;position:absolute;background:#FFFFFF;line-height:1.0;text-align:justify;z-index: 999999;">
</div>
</div>
