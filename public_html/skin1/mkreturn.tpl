<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en" xmlns:fb="http://www.facebook.com/2008/fbml">
	<head>
		<META http-equiv="Content-Style-Type" content="text/css">
		<META http-equiv="Content-Script-Type" content="type/javascript">
		<META http-equiv="Content-Type" content="text/html; charset=utf-8">
		{include file="site/header.tpl" }
	</head>
	<body>
	    <div class="container clearfix">
	    	{include file="site/menu.tpl" }
	    	<div class="section-heading prepend">Return Item from Order: #{$orderid}</div>
	    	<div class="blue-divider"></div>
	    	<div class="divider">&nbsp;</div>
		    {if $return_id }
		    	<div class="errornotification p5"><img src="/skin1/mkimages/unchecked.gif"> &nbsp; &nbsp; This item has already been returned. <a href="mymyntra.php?view=myreturns#{$return_id}">Click here</a> to check your return details.</div>
		    {else}
		    	{if $singleitem }
		    		<div class="divider">&nbsp;</div>
		    		<div class="divider">&nbsp;</div>
		    		<form id="returnsubmitform" action="mkreturninsert.php" method="POST">
		    			<input type="hidden" name="orderid" value="{$orderid}">
	    				<input type="hidden" name="itemid" value="{$singleitem.itemid}">
	    				<input type="hidden" name="item_option" value="{$singleitem.option}">
	    				<input type="hidden" name="customer_login" id="customer_login" value="{$singleitem.login}">
	    				<script type="text/javascript">
	    					var itemprice = "{$singleitem.price}";
	    					var itemdiscount = "{$singleitem.discount}";
	    					var itemcoupondiscount = "{$singleitem.coupon_discount_product}";
	    					var itemcashback = "{$singleitem.cashback}";
	    					var itemqty = "{$singleitem.amount}";
	    					var pickupcost = "{$pickupcharges}";
	    					var selfDeliveryCredit = "{$selfDeliveryCredit}";
	    				</script>
			    		<div style="min-height: 250px;">
				    		<div class="left return-item-details">
				    			<div class="item-img">
					    			{if $singleitem.default_image}
					    				<img src="{$singleitem.default_image}">
				                    {elseif $singleitem.image_portal_t}
				                    	<img src="{$singleitem.image_portal_t}">
				                    {/if}
				                </div>
			                    <div class="divider"></div>
			                    <ul>
			                    	{if $singleitem.stylename}
			                    		<li><div><b>{$singleitem.stylename}</b></div></li> 
			                    	{/if}
			                        {* if $singleitem.product}
			                        	<li><div>{$singleitem.product}</div></li> 
			                        {/if *}
			                        {if $singleitem.price}<li><div>Rs {$singleitem.price}</div></li> {/if}
			                    </ul>
				    		</div>
			    			<div class="left return-reason-details">
				    			<table border=0 cellspacing=0 cellpadding=0 width=100%>
				    				<tr>
				    					<td width=30% align=right style="vertical-align: top;">Size:</td>
				    					<td width=70% align=left style="vertical-align: top;">{$singleitem.size}</td>
				    				</tr>
				    				<tr>
				    					<td width=30% align=right style="vertical-align: top;">Quantity:</td>
				    					<td width=70% align=left style="vertical-align: top;">
				    						<select name="quantity" id="quantity" class="return-select">
					    					{section name=qtyVal loop=$singleitem.quantity }
			  									<option value="{$smarty.section.qtyVal.iteration}">{$smarty.section.qtyVal.iteration}</option>
											{/section}
										</select>
				    					</td>
				    				</tr>
				    				<tr>
				    					<td width=30% align=right style="vertical-align: top;">Reason<em style="color:red;">*</em>:</td>
				    					<td width=70% align=left style="vertical-align: top;">
				    						<select id="return-reason" name="return-reason" class="return-select">
						    					<option value="0">Select Reason</option>
						    					{section name=idx loop=$reasons}
						    						<option value="{$reasons[idx].code}">{$reasons[idx].displayname}</option>
						    					{/section}
						    				</select>
				    					</td>
				    				</tr>
				    				<tr>
				    					<td width=40% align=right style="vertical-align: top;">Details<em style="color:red;">*</em>:</td>
				    					<td width=60% align=left style="vertical-align: top;">
				    						<textarea id="return-details" name="return-details" class="return-details-box"></textarea>
				    					</td>
				    				</tr>
				    			</table>
				    		</div>
				    		<div class="right return-address-details">
		               			<div class="section-heading">Return Shipment Options</div>
		               			<div id="return-method-choice">
		               				<div>
		               					<input type=radio name="return-method" value="self" {if $returnMode eq 'self'} checked {/if}> Self-Shipping 
		               					<a href="javascript:void(0);">
		               					<img src="{$cdn_base}/skin1/images/custom/question_button.gif"  title="We encourage you to ship using the Myntra Logistics option 
		               					for us to provide better security and trackability of your return consignment. Add to that the convenience of a pickup 
		               					arranged from your doorstep at a time convenient to you." class="vtip"> </a>
		               				</div>
	               					<div>
	               						<input type=radio name="return-method" value="pickup" {if $returnMode eq 'pickup'} checked {/if}> Myntra Pickup &nbsp; &nbsp; 
	               						{if $pickupcharges != 0.00}
	               							<span class="caption">(Convenience charge of Rs {$pickupcharges} applicable)</span>
	               						{/if}
	               					</div>
		               			</div>
		               			<div class="divider"></div>
		               			<input type=hidden name="default-address-id" id="default-address-id" value="{$defaultAddressId}">
		               			<div id="selected-address-details">
		               				{if $selectedAddressId && $selectedAddressId neq '' }
		               					<input type=hidden name="selected-address-id" id="selected-address-id" value="{$selectedAddressId}">
		               					<input type=hidden id="pickup-available" value="{$selectedAddress.pickup}">
		               					<div id="address-details" class="addressblock">
				               				<div class="left" style="width: 300px;">
												<div id="no-pickup-available" class="hide">Pickup not available for this address</div>			               				
					               				<div class="name setname">
					                            	{$selectedAddress.name}
					                           	</div>
					                           	<div class="address">{$selectedAddress.address}</div>
					                           	<div class="city">{$selectedAddress.city} - {$selectedAddress.pincode}</div>
					                           	<div class="state">{$selectedAddress.state_display}, {$selectedAddress.country_display}</div>
					                           	<div class="mobile-no"><b>Mobile</b>: {$selectedAddress.mobile}</div>
					                           	<div class="email"><b>Email</b>: {$selectedAddress.email}</div>
					                        </div>
				                           	<a href="javascript:void(0);" class="editAddress" id="change-address-link" name="change-address-link" title="Change Address"></a> &nbsp;&nbsp;
				                        	<div class="divider"></div>
				                        </div>
			                           	<div class="divider"></div>
		               				{ else }
		               					<div id="address-details" class="addressblock">
		               						<a href="javascript:void(0);" class="editAddress" id="change-address-link" name="change-address-link" title="Change Address"></a> &nbsp;&nbsp;
		               						No Address selected. Please select your address.
		               					</div>
		               					<div class="divider"></div>
		               				{/if}
	               				</div>
					    		<div class="divider">&nbsp;</div>
					    		{include file="mkreturn_address.tpl" address=$selectedAddress}
					    	</div>
				    	</div>
				    	<div class="divider">&nbsp;</div>
			    		<div class="left" style="width: 600px;">
			    			<input type=checkbox id="check1" name="check1" value="true">
			    				I have the original order invoice and the packaging that the product was shipped to
								me with and I agree to send these back on the return shipment.
			    			<br/><br/>
			    			<input type=checkbox id="check2" name="check2" value="true">
			    				The product being returned is in unused, unwashed and all tags/stickers/
								accompanying material that the product was shipped with are as shipped to me originally.
			    			<br/><br/>
			    			<input type=checkbox id="check3" name="check3" value="true">
			    				I undertake that I have read and am in agreement with all terms and conditions in <a href="myntrareturnpolicy.php" target="_blank">Myntra’s Returns Policy</a>. 
		                </div>
	                    <div class="refunds-section-outer right">
	                    	<div class="refunds-section-inner"></div>
	                    </div>
		                <div class="divider">&nbsp;</div>
		    			<div><input class="orange-bg corners p5 clearfix right disabled-btn" type=submit id=return-confirm-btn name=return-confirm-btn value="Confirm Return" disabled></div>
		    			<div class="divider"></div>
			    	</form>
				{ else }
					<div class="errornotification p5"><img src="/skin1/mkimages/unchecked.gif"> &nbsp; &nbsp; No items are present in this order which can be returned.</div>
				{/if}
			{/if}
			<div class="divider"></div>
	    	{include file="site/footer.tpl"}
	    	{literal}
			<script type="text/javascript">
				var policyChecker = function policyCheck() {
					if( $("#check1").is(":checked") && $("#check2").is(":checked") && $("#check3").is(":checked")){
						$("#return-confirm-btn").attr('disabled', false);
						$("#return-confirm-btn").removeClass('disabled-btn');
						$("#return-confirm-btn").addClass('btn');
					} else {
						$("#return-confirm-btn").attr('disabled', true);
						$("#return-confirm-btn").removeClass('btn');
						$("#return-confirm-btn").addClass('disabled-btn');
					}
				};

				function getRefundDisplay() {
					var unitCashback = itemcashback/itemqty;
					var unitDiscount = itemdiscount/itemqty;
					var unitCouponDiscount = itemcoupondiscount/itemqty;
					
					var selectedQty = $("#quantity").val();
					var final_refund = (itemprice - unitDiscount - unitCashback - unitCouponDiscount)*selectedQty;
					var returnMode = $("input[name='return-method']:checked").val();
					if(returnMode == 'pickup') {
						final_refund -= parseFloat(pickupcost);
					} else {
						final_refund += parseFloat(selfDeliveryCredit);
					}	
					if(final_refund < 0){
						final_refund = 0;
					}			
					var html = '<div class="label">Item Price:</div><div class="field">'+(itemprice*selectedQty).toFixed(2)+'</div>';
					html += '<div class="clear"></div>';
					if(unitDiscount != 0) {
						html += '<div class="label">Item Discount:</div><div class="field">-'+(unitDiscount*selectedQty).toFixed(2)+'</div>';
						html += '<div class="clear"></div>';
					}
					if(unitCouponDiscount != 0) {
						html += '<div class="label">Coupon Discount:</div><div class="field">-'+(unitCouponDiscount*selectedQty).toFixed(2)+'</div>';
						html += '<div class="clear"></div>';
					}
					if(unitCashback != 0) {
						html += '<div class="label">*Cashback Reversal:</div><div class="field">-'+(unitCashback*selectedQty).toFixed(2)+'</div>';
						html += '<div class="clear"></div>';
					}
					if(returnMode == 'pickup' && pickupcost != 0.00) {
						html += '<div class="label">**Convenience Charge:</div><div class="field">-'+pickupcost+'</div>';
						html += '<div class="clear"></div>';
					}
					if(returnMode == 'self' && selfDeliveryCredit != 0.00) {
						html += '<div class="label">Self-Shipping Credit:</div><div class="field">'+selfDeliveryCredit+'</div>';
						html += '<div class="clear"></div>';
					}
					html += '<div style="border-bottom: 1px dashed #CCCCCC;"></div>';
					html += '<div class="grandlabel">Refund Amount:</div><div class="grandfield">'+final_refund.toFixed(2)+'</div>';
					html += '<div class="clear"></div>';
					return html;
				}

			 	$(document).ready(function() {

			 		$("#check1").click(policyChecker);
				    $("#check2").click(policyChecker);
				    $("#check3").click(policyChecker);

				    $("input[name='return-method']").click(function(){
				    	if( $(this).val() == "self" ) {
				    		$("#no-pickup-available").css('display', 'none');
				    		//$("#address-details").css('display', 'block');
				    	} else {
				    		if($("#pickup-available").val() != "1") {
					    		//$("#address-details").css('display', 'none');
					    		$("#no-pickup-available").css('display', 'block');
				    		}	
						}
						$(".refunds-section-inner").html(getRefundDisplay());
				    });
				    
				    $("#change-address-link").click(function(e){
				    	var returnMode = $("input[name='return-method']:checked").val();
				    	if(returnMode == "pickup") {
							$("input[name='select-address-pickup']").each(function(){
								var eleId = $(this).attr('id');
								var idSplitArr = eleId.split("-");
								var id = idSplitArr[3];
								if($(this).val() == '1') {
									$("#select-address-"+id).attr('disabled', false);
									$("#pickup-msg-"+id).css('display', "none");
								} else {
									$("#select-address-"+id).attr('disabled', true);
									$("#pickup-msg-"+id).css('display', "inline");
								}
							});
				    	} else {
				    		$("input[name='select-address-pickup']").each(function(){
				    			var eleId = $(this).attr('id');
								var idSplitArr = eleId.split("-");
								var id = idSplitArr[3];
								$("#select-address-"+id).attr('disabled', false);
				    		});
				    	}
				    	showSelectAddressWindow(e);
					});

				    $("#newaddress-link").click(function(e){
				    	closeSelectAddressWindow();
				    	showNewAddressWindow(e);
				    });

				    $("#new_country").change(function() {
						var country = $(this).val();
						if(country == 'IN') {
							$("#new_state_list").css('display', 'block');
							$("#new_state").css('display', 'none');
						} else {
							$("#new_state_list").css('display', 'none');
							$("#new_state").css('display', 'block');
						}
				    });
				 	
					$("#returnsubmitform").submit(function() {
						if($("#return-reason").val() == "0") {
							alert("Please select reason for return");
							return false;
						}
						if($("#return-details").val() == "") {
							alert("Please describe issues/defects in the product");
							return false;
						}

						if( !$("#selected-address-id").val() || $("#selected-address-id").val() == "" ) {
							alert("You have not specified an address. Please select one.");
							return false;
						}

						var returnMode = $("input[name='return-method']:checked").val();

						if(returnMode == 'pickup' && $("#pickup-available").val() != "1") {
							alert("Please select an address with pickup availability");
							return false;
						}

						return true;
					});

					$("#quantity").change(function() {
						$(".refunds-section-inner").html(getRefundDisplay());
					});

					if($("#new_country").val() == "IN") {
						$("#new_state").css("display","none");	
					} else {
						$("#new_state_list").css("display","block");
					}

					var returnMode = $("input[name='return-method']:checked").val();
					if( returnMode == "self" ) {
			    		$("#no-pickup-available").css('display', 'none');
			    	} else {
			    		if($("#pickup-available").val() != "1") {
				    		$("#no-pickup-available").css('display', 'block');
			    		}	
					}
					$(".refunds-section-inner").html(getRefundDisplay());
					
				 });
			</script>
			{/literal}
		</div>
	</body>
</html>