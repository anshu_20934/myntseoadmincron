{* $Id: mykritihome.tpl,v 1.88.2.3 2006/07/19 10:19:35 max Exp $ *}
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
{if $posaccounttype eq 'P2' || $posaccounttype eq 'SM' || $source_type_id eq '11'}
<link rel="stylesheet" href="{$http_location}/skin1/mykriti_invoice.css" />
{else}
<link rel="stylesheet" href="{$http_location}/skin1/mykriti.css" />
{/if}
</head>
<body id="body">
<style>
{literal}
    body#body,div#scroll_wrapper1{overflow:visible;}  
    div.container{ border-top:1px solid #999999; padding: 0 0 30px 0;}
{/literal}
</style>
<div id="scroll_wrapper">
{$content}
</div>
</body>
</html>