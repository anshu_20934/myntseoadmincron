 function goToPaymentOption(page,nocheckpaymentpos, shopmasterloggedin){
	var rbuttons = document.getElementsByName("rbtn");
	var soption = document.getElementById("s_option");
	var paybyoption = document.getElementsByName("payby");
	var selectedOption;
	var selectedTpl="";
	var optionName="";
	var optionId="";
	var rbutton ;

    if( shopmasterloggedin == true)
    {
	       alert("You cannot place an order because you are currently logged in as a Shop Master. Please login as a Store to continue.");
               return;
    }
	if(document.getElementById("captureuserdetails").value == 1){
		if(document.getElementById("c_firstname").value == ''){
			alert("customer firstname must be entered");
			document.getElementById("c_firstname").focus();
			return;
		}
		if(document.getElementById("c_lastname").value == ''){
			alert("customer lastname must be entered");
			document.getElementById("c_lastname").focus();
			return;
		}
		if(document.getElementById("c_mobile").value == ''){
			alert("customer mobile must be entered");
			document.getElementById("c_mobile").focus();
			return;
		}
		if(document.getElementById("c_order_personel").value == ''){
			alert("Personel name must be entered");
			document.getElementById("c_order_personel").focus();
			return;
		}

	}
    if(soption.value == "0"){
	       alert("Please choose shipping option.");
		   soption.focus();
	       return;
	}
  	var optioncount = 0;
	for(var i =0;  i<paybyoption.length; i++){
		if(paybyoption[i].checked == true){
			optioncount = optioncount+1;
		}
	}    
	var s_option = document.getElementById("s_option");
    	if( nocheckpaymentpos != false)
	{
     	if(optioncount == 0){
		 alert("Please select payment option.");
	       return;
	 }
	for(var i =0;  i < rbuttons.length; i++){
		rbutton = rbuttons[i];
		if(rbutton.checked == true){
			chkvalue = rbutton.value;
			splitValue = chkvalue.split(",");
			selectedTpl = splitValue[1];
			optionName=  splitValue[2];
			optionId = splitValue[0];
			break;
		}
	}
	if(s_option.type != "hidden"){
		var index=  s_option.selectedIndex;
		var soptionid = s_option.options[index].sid;
		if(soptionid == '0'){
			alert(" Please select shipping option");
			s_option.focus();
			return;
		}
	}
	}
	//emailRe = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*\.(\w{2}|(com|net|org|edu|int|mil|gov|arpa|biz|aero|name|coop|info|pro|museum))$/;
	fieldRe = /([!$%&''""/?{}[];:<>.@~]+)$/;
	var mobileRegEx = /^[0-9]{10}$/;
	var zipRegEx = /^[0-9a-zA-Z]{6,16}$/;
	/******************************************************/
    /*******Billing and shipping address validation******/
 	if(document.getElementById('s_firstname').value == ""){
		alert("Please input shipping first name");
		document.getElementById('s_firstname').focus();
		return;
	}
	if(document.getElementById('s_lastname').value == ""){
		alert("Please input shipping last name");
		document.getElementById('s_lastname').focus();
		return;
	}
	if(document.getElementById('s_address').value == ""){
		alert("Please input shipping address");
		document.getElementById('s_address').focus();
		return;
	}
	if(document.getElementById('s_city').value == ""){
		alert("Please input shipping city");
		document.getElementById('s_city').focus();
		return;
	}
	if(document.getElementById('s_state').value == ""){
		alert("Please select shipping state");
		document.getElementById('s_state').focus();
		return;
	}
	if(document.getElementById('s_country').value == ""){
		alert("Please input shipping country");
		document.getElementById('s_country').focus();
		return;
	}
	if(document.getElementById('s_zipcode').value == ""){
		alert("Please input valid shipping zipcode");
		document.getElementById('s_zipcode').focus();
		return;
	}else if(!zipRegEx.test(document.getElementById('s_zipcode').value)){
		alert("Please input valid shipping zipcode");
		document.getElementById('s_zipcode').focus();
		return;
	}
	if(document.getElementById('s_mobile').value == ""){
		alert("Please input valid shipping mobile number");
		document.getElementById('s_mobile').focus();
		return;
	}else if(!mobileRegEx.test(document.getElementById('s_mobile').value)){
		alert("Please input valid shipping mobile number");
		document.getElementById('s_mobile').focus();
		return;
	}
	if(document.getElementById('s_email').value == ""){
		alert("Please input shipping email");
		document.getElementById('s_email').focus();
		return;
	}
	if(!echeck(document.getElementById('s_email').value, "shipping")){
		document.getElementById('s_email').focus();
		return;
	}
/*****************************************************/
if( nocheckpaymentpos != false)
{

if(document.getElementById('billtoship').checked == false) {
        if(document.getElementById('b_firstname').value == ""){
            alert("Please input billing first name");
            document.getElementById('b_firstname').focus();
            return ;
        }
        if(document.getElementById('b_lastname').value == ""){
            alert("Please input billing last name");
            document.getElementById('b_lastname').focus();
            return ;
        }
        if(document.getElementById('b_address').value == ""){
            alert("Please input billing address");
            document.getElementById('b_address').focus();
            return ;
        }
        if(document.getElementById('b_city').value == ""){
            alert("Please input billing city");
            document.getElementById('b_city').focus();
            return;}
        if(document.getElementById('b_state').value == ""){
            alert("Please select billing state");
            document.getElementById('b_state').focus();
            return;}
        if(document.getElementById('b_country').value == ""){
            alert("Please input billing country");
            document.getElementById('b_country').focus();
            return ;}
        if(document.getElementById('b_zipcode').value == ""){
            alert("Please input valid billing zipcode");
            document.getElementById('b_zipcode').focus();
            return ;
		}else if(!zipRegEx.test(document.getElementById('b_zipcode').value)){
			alert("Please input valid billing zipcode");
            document.getElementById('b_zipcode').focus();
            return ;
		}
        if(document.getElementById('b_mobile').value == ""){
            alert("Please input billing mobile number");
            document.getElementById('b_mobile').focus();
            return;
		}else if(!mobileRegEx.test(document.getElementById('b_mobile').value)){
			alert("Please input valid billing mobile number");
            document.getElementById('b_mobile').focus();
            return;
		}
        if(document.getElementById('b_email').value == ""){
            alert("Please input billing email");
            document.getElementById('b_email').focus();
            return;}
        if(!echeck(document.getElementById('b_email').value, "billing")){
            alert("Please input valid billing email");
            document.getElementById('b_email').focus();
            return;}
    }
}else{
	if((document.getElementById('posonline')) && document.getElementById('posonline').value == 'true'){
        if(document.getElementById('b_firstname').value == ""){
            alert("Please input billing first name");
            document.getElementById('b_firstname').focus();
            return ;
        }
        if(document.getElementById('b_lastname').value == ""){
            alert("Please input billing last name");
            document.getElementById('b_lastname').focus();
            return ;
        }
        if(document.getElementById('b_address').value == ""){
            alert("Please input billing address");
            document.getElementById('b_address').focus();
            return ;
        }
        if(document.getElementById('b_city').value == ""){
            alert("Please input billing city");
            document.getElementById('b_city').focus();
            return;}
        if(document.getElementById('b_state').value == ""){
            alert("Please select billing state");
            document.getElementById('b_state').focus();
            return;}
        if(document.getElementById('b_country').value == ""){
            alert("Please input billing country");
            document.getElementById('b_country').focus();
            return ;}
        if(document.getElementById('b_zipcode').value == ""){
            alert("Please input valid billing zipcode");
            document.getElementById('b_zipcode').focus();
            return ;
		}else if(!zipRegEx.test(document.getElementById('b_zipcode').value)){
			alert("Please input valid billing zipcode");
            document.getElementById('b_zipcode').focus();
            return ;
		}
        if(document.getElementById('b_mobile').value == ""){
            alert("Please input billing mobile number");
			document.getElementById('b_mobile').focus();
            return;
		}else if(!mobileRegEx.test(document.getElementById('b_mobile').value)){
			alert("Please input valid billing mobile number");
            document.getElementById('b_mobile').focus();
            return;
		}
        if(document.getElementById('b_email').value == ""){
            alert("Please input billing email");
            document.getElementById('b_email').focus();
            return;}
        if(!echeck(document.getElementById('b_email').value, "billing")){
            alert("Please input valid billing email");
            document.getElementById('b_email').focus();
            return;}
	}
    if( shopmasterID == 47 && document.getElementById('shop_bill_number').value == ""){
        alert("Please provide shop name/shop bill no");
        document.getElementById('shop_bill_number').focus();
        return;
	}
}
    if(document.getElementById('issues_contact_number').value == ""){
        alert("Please provide contact number for issues");
        document.getElementById('issues_contact_number').focus();
        return;
	}else if(!mobileRegEx.test(document.getElementById('issues_contact_number').value)){
		alert("Please provide valid contact number for issues");
        document.getElementById('issues_contact_number').focus();
        return;
	}

    if(document.getElementById('order_number')){
		   var order_number_obj = document.getElementById('order_number');
			if(!IsNumeric(order_number_obj.value)){
				  alert("Please enter numeric value for order id"); 
				  order_number_obj.focus();
				  return false;
			}
	}
	
    var check_flag = true;
    if(document.getElementById("ch")!=null && document.getElementById("ch").checked == true){
		var check_flag = confirm("It takes 3-4 days for us to receive the check. This will add minimum 5 days to the delivery time. Are you sure that you want to make the payment by check? Click on OK to place the order by check. Click on Cancel to change the payment method to credit card or net banking ");
	}

    var cod_flag = true;
    if(document.getElementById("cod") != null && document.getElementById("cod").checked == true){
		var cod_flag = confirm("With the Cash On Delivery option, our customer care would need to verify your shipping address and contact details to ensure quality service. \n\n Are you sure you want to proceed with Cash on Delivery? Click on OK to place the order by COD. Click on Cancel to change the payment method to credit card /net banking or check.");
	}

    if(document.getElementById("cod") != null && document.getElementById("cod").checked == false && document.getElementById("s_option").value=="AR"){
		alert("Aramex courier service is only applicable for the 'Cash on Delivery' method. Cannot place the order.");
        return false;
	}

    if(document.getElementById("cod") != null && document.getElementById("cod").checked == true && document.getElementById("s_option").value!="AR"){
		alert("The courier service selected does not cater to Cash on Delivery");
        return false;
	}
	var is_shipping_cost_set = true;
	if( nocheckpaymentpos != false){
	if(document.getElementById('is_shipping_cost_set').value == 0){
        /*  alert("The address yo have entered is not serviceable by the courier service you selected. Please check your address once again. If you still face problems, please contact support@myntra.com or call our customer care for immediate assistance. We apologize for the inconvenience");
            document.getElementById('s_city').focus();
			is_shipping_cost_set =  false;
            return;*/
	}
	}
    if(check_flag && cod_flag && is_shipping_cost_set){
	if( nocheckpaymentpos != false && s_option.type!="hidden")
		var soptionname=s_option.options[index].text;
	else
		var soptionname="Blue Dart";
	document.paymentform.submit();
	}
 }
 function IsNumeric(orderNumber){
    pattern=/^[0-9]*$/;
    if(pattern.test(orderNumber)==false){
         return false; }
    else if(orderNumber==0){
         return false;
     }else if(orderNumber == '' || orderNumber ==  null){ 
		   return false;
   }else{
        return true;
    }
}
 function showShippingOption(url,params){
      window.open(url,null,params) 
}
function submitOrder(){
  document.order.notes.value =document.getElementById("usercomment").value;
  document.order.submit();
}

function check_length(divname){
maxLen = 68; // max number of characters allowed
if (document.getElementById(divname).value.length >= maxLen) {
var msg = "You have reached your maximum limit of address allowed";
document.getElementById(divname).value = document.getElementById(divname).value.substring(0, maxLen);
return;
}
else{ // Maximum length not reached so update the value of my_text counter
document.getElementById("text_num").value = maxLen - document.getElementById(divname).value.length;}
} 
function imposeMaxLength(Object, MaxLen){
  return (Object.value.length <= MaxLen);
}

function checkOptionEnable(totalAmount){
	if(totalAmount > parseFloat(check_option_enable_amount))
		document.getElementById('chq_enable').style.display = '';
	else
		document.getElementById('chq_enable').style.display = 'none';
}