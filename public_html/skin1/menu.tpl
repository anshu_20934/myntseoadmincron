{if $usertype eq "C"} {* Crystal_Blue *}

<table cellpadding="0" cellspacing="0" width="100%">
<tr>
	<td width="100%">
	
	<table cellpadding="0" cellspacing="0" width="100%" class="VertMenuTitleBox">
	<tr>
		<td class="VertMenuTitleBorder" valign="top"><img src="{$cdn_base}/skin1/images/spacer.gif" width="1" height="22" alt="" /></td>
		<td class="VertMenuTitleShadow" align="center" width="100%">
		{if $link_href}
		<a href="{$link_href}"><font class="VertMenuTitle">{$menu_title}</font></a>
		{else}
		<font class="VertMenuTitle">{$menu_title}</font>
		{/if}
		</td>
		<td class="VertMenuTitleBorder" valign="top"><img src="{$cdn_base}/skin1/images/spacer.gif" width="1" height="22" alt="" /></td>
	</tr>
	<tr>
		<td class="VertMenuBottomBorder" valign="top"><img src="{$cdn_base}/skin1/images/spacer.gif" width="1" height="2" alt="" /></td>
		<td class="VertMenuBottom" valign="top"><img src="{$cdn_base}/skin1/images/spacer.gif" width="1" height="1" alt="" /></td>
		<td class="VertMenuBottomBorder" valign="top"><img src="{$cdn_base}/skin1/images/spacer.gif" width="1" height="2" alt="" /></td>
	</tr>
	</table>
	
	</td>
</tr>
<tr>
	<td><img src="{$cdn_base}/skin1/images/spacer.gif" width="1" height="3" alt="" /></td>
</tr>
<tr>
	<td class="VertMenuBox">
	
	<table {if $menu_style eq "categories"} cellspacing="0" cellpadding="0"{else}cellpadding="5" cellspacing="5"{/if} width="100%">
	<tr>
		<td>{$menu_content}</td>
	</tr>
    </table>
	
	</td>
</tr>
<tr>
	<td><img src="{$cdn_base}/skin1/images/spacer.gif" width="1" height="4" alt="" /></td>
</tr>
</table>

{else} {* Crystal_Blue *}

<table cellspacing="1" width="100%" class="VertMenuBorder">
<tr>
<td class="VertMenuTitle">
<table cellspacing="0" cellpadding="0" width="100%"><tr>
<td>{$link_begin}<img src="{$cdn_base}/skin1/images/{if $dingbats ne ''}{$dingbats}{else}spacer.gif{/if}" class="VertMenuTitleIcon" alt="{$menu_title|escape}" />{$link_end}</td>
<td width="100%">{if $link_href}<a href="{$link_href}">{/if}<font class="VertMenuTitle">{$menu_title}</font>{if $link_href}</a>{/if}</td>
</tr></table>
</td>
</tr>
<tr> 
<td class="VertMenuBox">
<table cellpadding="{$cellpadding|default:"5"}" cellspacing="0" width="100%">
<tr><td>{$menu_content}<br /></td></tr>
</table>
</td></tr>
</table>

{/if} {* Crystal_Blue *}
