<html>
<head>
{literal}
<script Language="JavaScript" Type="text/javascript" src="../skin1/main/validation_inv.js"></script>
<script Language="JavaScript" Type="text/javascript">
function setTotalPrice(theform)
		{
		 var quantity=theform.quantity_added;
         var unitprice=theform.unitprice;
         var tax=theform.tax;
         var shipping=theform.shipping;
         var totalprice=theform.totalprice;
         if (quantity.value==null||quantity.value == ""||!(isPositiveInteger(quantity.value)))
		  {
			alert("Please enter quantity to be added >=0.");
			quantity.focus();
			
		  }
		 else if (unitprice.value==null||unitprice.value == ""||!(isValidNumber(unitprice.value)))
		  {
			alert("Please enter a valid unit price value.");
			unitprice.focus();
			
		  }
		 else if (tax.value==null||tax.value == ""||!(isPositiveInteger(tax.value)))
		  {
			alert("Please enter tax percentage.");
			tax.focus();
			
		  }
		 
		else if (shipping.value==null||shipping.value == ""||!(isValidNumber(shipping.value)))
		  {
			alert("Please enter shipping cost >=0.");
			shipping.focus();
			
		  }
		else{
		var total=(quantity.value)*(unitprice.value);
		var taxpercent=tax.value/100;
		totalprice.value=total+total*taxpercent+(shipping.value)*1;
		}
		
		
		}
		function FrontPage_Form1_Validator(theform)
		{
         
         var added=theform.addedby;
         var title=theform.title;
         var vendor=theform.vendor;
         var quantity=theform.quantity_added;
         var unitprice=theform.unitprice;
         var tax=theform.tax;
         var shipping=theform.shipping;
         
		  if (added.value==null||added.value == "")
		  {
			alert("Please enter your name.");
			added.focus();
			return false;
		  }
		  if (title.value==null||title.value == "")
		  {
			alert("Please enter title.");
			title.focus();
			return false;
		  }
		  if (vendor.value==null||vendor.value == "")
		  {
			alert("Please enter vendor name.");
			vendor.focus();
			return false;
		  }
		  if (quantity.value==null||quantity.value == ""||!(isPositiveInteger(quantity.value)))
		  {
			alert("Please enter quantity to be added >=0.");
			quantity.focus();
			return false;
		  }
		  if (unitprice.value==null||unitprice.value == ""||!(isValidNumber(unitprice.value)))
		  {
			alert("Please enter a valid unit price value.");
			unitprice.focus();
			return false;
		  }
		  if (tax.value==null||tax.value == ""||!(isValidNumber(tax.value)))
		  {
			alert("Please enter tax.");
			tax.focus();
			return false;
		  }
		 
		 if (shipping.value==null||shipping.value == ""||!(isValidNumber(shipping.value)))
		  {
			alert("Please enter shipping cost >=0.");
			unitprice.focus();
			return false;
		  }
		  return true;
		  
		}
		</script>
		{/literal}

<link rel="stylesheet" href="../skin1/mykriti.css"/>

</head>
<body id="body">
	<div id="container"> 
	    <div id="display"> 
	      <div class="center_column"> 
	        <div class="print"> 
		        <div class="super" > 
	           		 <p>&nbsp;</p>
	          	</div>
	          	{if $commentSuccess ne "Y"}
	          	<div class="head" style="padding-left:15px;"> 
		            <p>&nbsp;&nbsp;Add Inventory for {$sku_name}</p>
          		</div>
          		<div class="links"><p></p></div>
          		<div class="foot"></div>
          		
          		
<form method="post" enctype="multipart/form-data" onsubmit="return FrontPage_Form1_Validator(this)" language="JavaScript" name="addform" action="add_sku_inventory.php">
<input type="hidden" name="addinventory" value="true" />
<input type="hidden" name="skuid" value="{$skuid}" />
<input type="hidden" name="sku_count" value="{$sku_count}" />
<input type="hidden" name="sku_unitprice" value="{$sku_unit}" />
		<table>
			<tr>
				<td style="text-align:left;">SKU#</td>
				<td style="text-align:left;"><input type="text" name="sku" value="{$sku_name}" readonly></td>
				<td style="text-align:left;">&nbsp;</td>
				<td style="text-align:left;">&nbsp;</td>
			</tr>
			<tr>
				<td style="text-align:left;">Add date</td>
				<td style="text-align:left;"><input type="text" name="date_added" value="{$date}" readonly></td>
				<td style="text-align:left;">Added by</td>
				<td style="text-align:left;"><input type="text" name="addedby" ></td>
				<td style="text-align:left;">&nbsp;</td>
				<td style="text-align:left;">&nbsp;</td>
			</tr>
			<tr>
				<td style="text-align:left;">Subject</td>
				<td style="text-align:left;"><input type="text" name="title"></td>
				<td style="text-align:left;">&nbsp;</td>
				<td style="text-align:left;">&nbsp;</td>
			</tr>
			<tr>
				<td style="text-align:left;">Vendor</td>
				<td style="text-align:left;"><input type="text" name="vendor" value="{$sku_vendor}"></td>
				<td style="text-align:left;">&nbsp;</td>
				<td style="text-align:left;">&nbsp;</td>
			</tr>
			<tr>
				<td style="text-align:left;">Quantity Added</td>
				<td style="text-align:left;"><input type="text" name="quantity_added" onchange="setTotalPrice(document.addform)"></td>
				
				<td style="text-align:left;">Unit Price</td>
				<td style="text-align:left;"><input type="text" name="unitprice" value="{$sku_unit}" onchange="setTotalPrice(document.addform)"></td>
			</tr>
			<tr>
				<td style="text-align:left;">Tax (%)</td>
				<td style="text-align:left;"><input type="text" name="tax" onchange="setTotalPrice(document.addform)"></td>
				<td style="text-align:left;">Shipping</td>
				<td style="text-align:left;"><input type="text" name="shipping" onblur="setTotalPrice(document.addform)"></td>
			</tr>
			<tr>
				<td style="text-align:left;">Total Price</td>
				<td style="text-align:left;"><input type="text" name="totalprice" readonly style="color:red;border:0px;"></td>
			    <td style="text-align:left;">&nbsp;</td>
				<td style="text-align:left;">&nbsp;</td>
			</tr>
			
			<tr>
				<td style="text-align:left;">Notes</td>
				<td style="text-align:left;" colspan=3><textarea name="notes" cols='50' rows='5'></textarea></td>
			</tr>
			<tr>
				<td style="text-align:left;">Attachment</td>
				<td style="text-align:left;" colspan=2><input type="file" name="attachment"></td>
				<td style="text-align:left;">&nbsp;</td>
			</tr>
			<tr>
				<td style="text-align:left;" colspan=3>&nbsp;</td>
				<td style="text-align:right;"><input type="submit" value="Add" style="background-color:#f2f2f2; color:#000;"></td>
			</tr>
		</table>
	</form>
		{elseif $commentSuccess eq "Y"}
		<table>
				<tr>
					<td>Inventory added Succesfully!!!!!</td>
					<td>&nbsp;&nbsp;</td>
					<td>&nbsp;&nbsp;</td>
				</tr>
				<tr>
					<td style="font-size:.8em;color:red;"><b>Please Click the close button below</b></td>
				</tr>
				<tr>
					<td align="center"><input type="button" style="height:20px;width:50px; background-color:black; color:white;" value="Close" onclick="opener.window.location.reload();self.close()"></td>
				</tr>
		</table>
		{/if}
		</div> 
	      </div>   	
	     </div> 
	   </div> 
	</body> 
	</html>
    	