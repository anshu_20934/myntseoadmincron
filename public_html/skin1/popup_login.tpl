{literal}
<script type="text/javascript" language="JavaScript 1.2">
function hideSuccessDiv()
{
	document.getElementById('successpopup').style.visibility = 'hidden';
}

function hideWithDelay()
{
	setTimeout("hideSuccessDiv()", 7000);
}

function showSuccessDiv()
{
	document.getElementById('successpopup').style.visibility = 'visible';
}
</script>
{/literal}
{*******************Popup message for login************************}

<div id="successpopup">
  <div id="overlay"> </div>
  <div id="message" style="position:relative; left:0px; top:60px;">
    <div class="wrapper">
      <div class="head"><a href="javascript:hideSuccessDiv()">Click to close</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
      <div class="body">
        <p><strong>You have successfuly logged into Myntra.</strong><br>

          Welcome {$firstname}<br>
          <br>
         <!-- <a href="#">Privacy Policy</a> | <a href="#">Legal</a> | <a href="#">Site
          Map</a>--></p>
      </div>
      <div class="foot"></div>

    </div>
  </div>
</div>

{literal}
<script type="text/javascript" language="JavaScript 1.2">
showSuccessDiv();
hideWithDelay();
</script>
{/literal}


