{literal}
<script language="javascript">
  	function sortProducts(id,url,pg,httplocation)
	{
		
		    //var urlLoc = httplocation +"/"+url +"/"+id+"/"+pg;
			var urlLoc = url +"/"+id+"/"+pg;
			window.location = urlLoc;
	}
</script>
{/literal}


<div class="form2"> 
          <div class="subhead"> 
            <p style="background-color:#99cd00; color:#fff; font-size: 0.9em; font-weight:bold;">{$favheading|lower}</p>            
          </div>
	  
	  {if $allfavproducts != 'empty'}

	   <div class="links"> 
            <div class="navigator"> 
              <div class="left"> 

			{assign var=sortparam1 value=USERRATING}
			{assign var=sortparam2 value=BESTSELLING}
			{assign var=sortparam3 value=MOSTRECENT}
			{assign var=sortparam4 value=EXPERTRATING}
		<p style="font-size:0.9em;">
		  Sort By :
				{if $favsortMethod eq MOSTRECENT}
				  <a href="javascript:sortProducts('{$sortparam4}','{$favsorturl}','1','{$http_location}')">Expert Rating</a>&nbsp;|&nbsp;<a href="javascript:sortProducts('{$sortparam1}','{$favsorturl}','1','{$http_location}')">User Rating</a>&nbsp;|&nbsp;<a href="javascript:sortProducts('{$sortparam2}','{$favsorturl}','1','{$http_location}')">Best Selling</a>&nbsp;|&nbsp;Just Arrived
				{/if}   
				{if $favsortMethod eq BESTSELLING}
				<a href="javascript:sortProducts('{$sortparam4}','{$favsorturl}','1','{$http_location}')">Expert Rating</a>&nbsp;|&nbsp;<a href="javascript:sortProducts('{$sortparam1}','{$favsorturl}','1','{$http_location}')">User Rating</a>&nbsp;|&nbsp;Best Selling&nbsp;|&nbsp;<a href="javascript:sortProducts('{$sortparam3}','{$favsorturl}','1','{$http_location}')">Just Arrived</a>
				{/if}
				{if $favsortMethod eq USERRATING}
				<a href="javascript:sortProducts('{$sortparam4}','{$favsorturl}','1','{$http_location}')">Expert Rating</a>&nbsp;|&nbsp;User Rating&nbsp;|&nbsp;<a href="javascript:sortProducts('{$sortparam2}','{$favsorturl}','1','{$http_location}')">Best Selling</a>&nbsp;|&nbsp;<a href="javascript:sortProducts('{$sortparam3}','{$favsorturl}','1','{$http_location}')">Just Arrived</a>
				{/if}
				{if $favsortMethod eq EXPERTRATING}
					Expert Rating&nbsp;|&nbsp;<a href="javascript:sortProducts('{$sortparam1}','{$favsorturl}','1','{$http_location}')">User Rating</a>&nbsp;|&nbsp;<a href="javascript:sortProducts('{$sortparam2}','{$favsorturl}','1','{$http_location}')">Best Selling</a>&nbsp;|&nbsp;<a href="javascript:sortProducts('{$sortparam3}','{$favsorturl}','1','{$http_location}')">Just Arrived</a>
				{/if}		 
		 </p> 
    
              </div>
              <div class="right" > 
			
               <div style="font-size: 0.9em;">
		         
                {if $favtotalpages > 1}{$favpaginator}{/if}
			</div>
                <!--this is a number bar that will be used universally-->

              </div>
              <div class="clearall"></div>
            </div>
	   
          </div>
     {/if}


