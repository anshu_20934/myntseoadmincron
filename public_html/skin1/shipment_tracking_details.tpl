<div class="clearfix">
{if is_array($shipment_tracking_details) && count($shipment_tracking_details) > 0}
	<div class="shipment-tracking-header">TRACKING DETAILS - Order No.{$order_id} </div>
	<div>Courier:{$courier_operator} |  Tracking No:{$tracking_no}</div>
	<div style="height:325px; overflow:auto">
        <table cellspacing=0 cellpadding=0 border=0 width=100% style="border-collapse: collapse;">
            <tr class="shipment-tracking-title">
        		<td>DATE</td>
            	<td>REMARKS</td>
                <td>LOCATION</td>
            </tr>
            {section name=rows loop=$shipment_tracking_details}
                <tr class="{cycle values="shipment-tracking-detail-even-position,shipment-tracking-detail-odd-position"}">
                    <td>{$shipment_tracking_details[rows].scanDate|date_format:"%d %B, %Y %I:%M %p"}</td>
                    <td>{$shipment_tracking_details[rows].remarks}</td>
                    <td>{$shipment_tracking_details[rows].scannedlocation}</td>
                </tr>
            {/section} 
       </table>
	</div>
{else}
<table>
    <tr>
        <td class="shipment-tracking-header">Shipping Not Yet Started</td>
    </tr>
</table>
{/if}    
</div>
<br/> 
<div style="background:#2F2F2F2; color:#808080; font-family:'Arial';font-style:Regular/Bold;font-size:12px">Need help? {$customerSupportTime} 24/7 on {$customerSupportCall} </div>
