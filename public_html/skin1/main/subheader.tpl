{* $Id: subheader.tpl,v 1.9 2005/12/07 14:07:27 max Exp $ *}
{if $class eq 'grey'}
<table cellspacing="0" class="SubHeaderGrey">
<tr>
	<td class="SubHeaderGrey">{$title}</td>
</tr>
<tr>
	<td class="SubHeaderGreyLine"><img src="{$cdn_base}/skin1/images/spacer.gif" class="Spc" alt="" /></td>
</tr>
</table>
{elseif $class eq "red"}
<table cellspacing="0" class="SubHeaderRed">
<tr>
	<td class="SubHeaderRed">{$title}</td>
</tr>
<tr>
	<td class="SubHeaderRedLine"><img src="{$cdn_base}/skin1/images/spacer.gif" class="Spc" alt="" /><br /></td>
</tr>
</table>
{elseif $class eq "black"}
<table cellspacing="0" class="SubHeaderBlack">
<tr>
	<td class="SubHeaderBlack">{$title}</td>
</tr>
<tr>
	<td class="SubHeaderBlackLine"><img src="{$cdn_base}/skin1/images/spacer.gif" class="Spc" alt="" /><br /></td>
</tr>
</table>
{else}
<table cellspacing="0" class="SubHeader">
<tr>
	<td class="SubHeader">{$title}</td>
</tr>
<tr>
	<td class="SubHeaderLine"><img src="{$cdn_base}/skin1/images/spacer.gif" class="Spc" alt="" /><br /></td>
</tr>
</table>
{/if}

