{* $Id: register_shipping_address.tpl,v 1.38.2.2 2006/06/22 12:01:32 max Exp $ *}
{if $is_areas.S eq 'Y'}
	{if $action eq "cart"}
		<input type="hidden" name="action" value="cart" />
		<input type="hidden" name="paymentid" value="{$paymentid}" />
	{/if}

	<table>
		<tr>
			<td style="width:175px;" rowspan="2">
				{if $default_fields.s_address.avail eq 'Y'}
					<p>Your Address:</p>							
				{/if}
			</td>
			<td style="width:220px;" rowspan="2">
				{if $default_fields.s_address.avail eq 'Y'}				
					<p><textarea style="width:195px;" id="s_address" tabindex="17" name="s_address">{$userinfo.s_address}</textarea><!--onKeyPress=check_length("s_address"); onKeyDown=check_length("s_address");-->
					<input type=hidden size=2 value=255 name="text_num" id="text_num"></p>			
				{/if}
			</td>
			<td style="width:175px;">
				{if $default_fields.s_city.avail eq 'Y'}
					<p>City:</p>				
				{/if}			
			</td>
			<td class="alignleft" style="width:385px;">
				{if $default_fields.s_city.avail eq 'Y'}				
					<p><input  type="text" id="s_city" name="s_city"  value="{$userinfo.s_city}" tabindex="20"/></p>
				{/if}
			</td>
		</tr>	
	
		<tr>
			<!--<td>&nbsp;</td>
			<td>&nbsp;</td>-->
			<td>
				{if $default_fields.s_zipcode.avail eq 'Y'}
					<p>Zip/Postal Code:</p>								
				{/if}			
			</td>
			<td class="alignleft">
				{if $default_fields.s_zipcode.avail eq 'Y'}				
					<p><input  type="text" id="s_zipcode" name="s_zipcode"  value="{$userinfo.s_zipcode}" onchange="check_zip_code()" style="width:100px;" maxlength="6" tabindex="21"/>
					<input class="checkbox" type="checkbox" id="ship2diff" name="ship2diff" value="Y" style="display:none;"/></p>				
				{/if}
			</td>		
		</tr>
		
		<tr>
			<td>
				{if $default_fields.s_country.avail eq 'Y'}
					<p>Country:</p>				
				{/if}
			</td>
			<td>
				{if $default_fields.s_country.avail eq 'Y'}
					<p>
						<select name="s_country" id="s_country" size="1" onchange="check_zip_code()" tabindex="18">
							{section name=country_idx loop=$countries}
								<option value="{$countries[country_idx].country_code}"{if $userinfo.s_country eq $countries[country_idx].country_code} selected="selected"{elseif $countries[country_idx].country_code eq $config.General.default_country and $userinfo.s_country eq ""} selected="selected"{/if}>{$countries[country_idx].country}</option>
							{/section}
						</select>
					</p>
				{/if}
			</td>
			<td>
				{if $default_fields.phone.avail eq 'Y'}
					<p>Telephone:</p>			    			    		
				{/if}		
			</td>
			<td class="alignleft">
				{if $default_fields.phone.avail eq 'Y'}				
				    <p><input type="text" id="phone" name="phone"  value="{$userinfo.phone}" tabindex="22"/></p>			    		
				{/if}			
			</td>
		</tr>
		
		<tr>
			<td>
				{if $default_fields.s_state.avail eq 'Y'}
					<p>State:</p>				
				{/if}
			</td>
			<td style="text-align:left;padding-left:10px">
				{if $default_fields.s_state.avail eq 'Y'}				
					<p>{include file="main/states.tpl" states=$states name="s_state" default=$userinfo.s_state default_country=$userinfo.s_country country_name="s_country"}</p>
				{/if}
			</td>
			<td>
				&nbsp;		
			</td>
			<td class="alignleft">
				&nbsp;			
			</td>
		</tr>
		
		<tr>
			<td colspan="3" class="alignleft">
				{if $default_fields.s_state.avail eq 'Y' && $default_fields.s_country.avail eq 'Y' && $js_enabled eq 'Y' && $config.General.use_js_states eq 'Y'}
					<div class="field_980" style="display: none;">
						{include file="main/register_states.tpl" state_name="s_state" country_name="s_country" county_name="s_county" state_value=$userinfo.s_state county_value=$userinfo.s_county}
					</div>
				{/if}
			</td>		
		</tr>
	</table>
{/if}