{include file="main/include_js.tpl" src="main/popup_product.js"}
{config_load file="$skin_config"}
<html>
<head>
	<title>Add offline product</title>
	<link rel="stylesheet" href="{$SkinDir}/skin1_admin.css" />
</head>
<body align="center" style="margin-left:60px;">
<br><br>
{capture name=dialog}
	<table width='95%' class='formtable' cellpadding='0' cellspacing='2' border='0' >
		  <tr style="background-color:#91DDE6;">
			<td width='15%' class="SubmitBox"><p><strong>item</strong></p></td>
			<td width='40%' class="SubmitBox"><p><strong>quantity break-up</strong></p></td>
			<td width='10%' class="SubmitBox"><p><strong>total quantity</strong></p></td>
			<td width='15%' class="SubmitBox"><p><strong>unit price</strong></p></td>
			<td width='15%' class="SubmitBox"><p><strong>total price</strong></p></td>
		  </tr>
		  {assign var=i value=0}
		{section name=quote loop=$quote_details}
			
		  <tr class='TableSubHead'>
			<td width='15%' class="SubmitBox"><p>{$quote_details[quote].stylename}</p></td>
			<td width='40%' class="SubmitBox"><p><strong>{if $quote_details[quote].quantity_breakup} {$quote_details[quote].quantity_breakup} {else} quantity break-up is not possible{/if}</strong></p></td>
			<td width='10%' class="SubmitBox"><p>{$quote_details[quote].total_quantity}</p></td>
			<td width='15%' class="SubmitBox"><p>{$quote_details[quote].price}</p></td>
			<td width='15%' class="SubmitBox"><p>{$quote_details[quote].totalprice}</p></td>
		</tr>
		 {assign var=i value=$i+1}
		{/section} 
		<input type="hidden" name="count" id="count" value="{$i}">
		   <tr class='TableSubHead'>
			<td class="SubmitBox" colspan='4' align='right'>Subtotal</td>
			<td class="SubmitBox" width='100%' colspan='1'><p>{$quoteresult.sub_total}</p></td>
		  </tr>  
		  <tr class='TableSubHead'>
			<td class="SubmitBox" colspan='4' align='right' class="mandatory">Discount</td>
			<td class="SubmitBox" colspan='1'><p>{$quoteresult.discount}</p></td>
		  </tr> 
		  <tr class='TableSubHead'>
			<td class="SubmitBox" colspan='4' align='right' class="mandatory">Total after discount</td>
			<td class="SubmitBox" width='100%' colspan='1'><p>{$quoteresult.totalafterdiscount}</p></td>
		  </tr> 
		  <tr class='TableSubHead'>
			<td class="SubmitBox" colspan='4' align='right' class="mandatory">VAT/CST</td>
			<td class="SubmitBox" colspan='1'><p>{$quoteresult.vat}</p></td>
		  </tr> 
		  <tr class='TableSubHead'>
			<td class="SubmitBox" colspan='4' align='right' class="mandatory">Shipping</td>
			<td  class="SubmitBox" colspan='1'><p>{$quoteresult.shipping}</p></td>
			
		  </tr> 
		  <tr class='TableSubHead'>
			<td class="SubmitBox" colspan='4' align='right' class="mandatory"><p><strong>Grand Total</strong></p></td>
			<td class="SubmitBox" colspan='1'><p>{$quoteresult.grand_total}</p></td>
		  </tr> 
		   <tr class='TableSubHead'><td  colspan='5' class="SubmitBox">&nbsp;</td></tr>	
		  <!--<tr class='TableSubHead'>
			<td width='100%' colspan='4' align='right' class="mandatory">
				<p><input class='submit' type='button' name="sendquote" value='Save Quote' onClick="javascript: saveRequestQuote('{$quote_id}', this.form)">&nbsp;&nbsp;&nbsp;
				<input class='submit' type='button' name="savequote" value='Send  Quote' onClick="javascript: saveRequestQuote('{$quote_id}', this.form)"></p>
			</td>
			
		  </tr>-->
	 </table>

{/capture}
{include file="dialog.tpl" title="quote detail of $quotenumber" content=$smarty.capture.dialog extra='width="90%"' }
</body>
</html>