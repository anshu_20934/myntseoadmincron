{* $Id: cancelorders.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
{include file="main/include_js.tpl" src="main/popup_product.js"}

<link rel="stylesheet" href="{$SkinDir}/skin1_admin_operations.css" />
{literal}
<style>
.list-form tr{
	height:5px;
}
</style>
{/literal}
<script type="text/javascript">
	var progressKey = '{$progresskey}';
</script>
{capture name=dialog}
<div>
	{if $message}<font color='green' size='2'>{$message}</font><br/><br/>{/if}
	{if $errormessage}<font color='red' size='2'>{$errormessage}</font><br/><br/>{/if}
	<form action="" method="post" name="skuexcelfileform" enctype="multipart/form-data">
		<table border=0 cellspacing=0 cellpadding=0 width=100%>
			<tr>
				<td>Upload the file:<br>(Excel file. <br>First row will be ignored as header)</td>
				<td><input type="file" name="skusfile" size=100></td>
			</tr>
			<tr><td colspan=2 align=left><input type='submit' name='uploadxls' value="Upload"></td></tr>
		</table>
		<input type='hidden' name='mode' value='upload'>
		<input type='hidden' name='login' value='{$login}'>
	</form>
</div>
<div>
<form action="" method="post" name="mapping-add-form" class="list-form" onsubmit="return showUploadStatusWindow();">
	<input type="hidden" name="mode"  value="save"/>
	<input type="hidden" name="progresskey"  value="{$progresskey}"/>
	<table cellpadding="3" cellspacing="1" width="100%">
	<thead>
	<tr class="TableHead">
		<th>Update</th>
		<th>Error</th>
		<th>Style Id</th>
		<th>Vendor Article Number</th>
		
	</tr>
	</thead>
	<tbody>
{if $upload_styles}
	{foreach name=dataloop key=k item=row from=$upload_styles}
	<tr class="style_row">
		<input type="hidden" name=posted_data[{$k}][article_type_attribute_ids] value="{$row.article_type_attribute_ids}">
		<input type="hidden" name="posted_data[{$k}][validated_style]" value="{$row.validated}">
		<td><input type="checkbox" name="posted_data[{$k}][validated_style_chk]" {if $row.validated eq 'Y'}checked{/if}  disabled></td>
		<td style="color:red">{$row.validated_error}</td>
		<td><input type="text" name="posted_data[{$k}][style_id]" value="{$row.style_id}" readonly="readonly"></td>
		<td><input type="text" name="posted_data[{$k}][article_number]" value="{$row.vendor_article_number}" readonly="readonly"></td>
		
	</tr>
	{/foreach}
{/if}	
	</tbody>
	</table>
<input type="submit" value="Upload Styles">
</form>
<div id="uploadstatuspopup" style="display:none">
			<div class="overlay" style="display:block;_position:absolute;_width:1300px;_height:1500px;">&nbsp;</div>
			<div id="uploadstatuspopup-main" style="position:absolute; left: 20%; width:450px; background:white; z-index:1006;border:2px solid #999999;">
				<div id="uploadstatuspopup-content" style="display:none;">
				    <div id="uploadstatuspopup_title" class="popup_title">
				    	<div id="uploadstatuspopup_ajaxWindowTitle" class="popup_ajaxWindowTitle">Upload Status</div>
						<div class="clear">&nbsp;</div>
					</div>
					<div style="padding:5px 25px; text-align:left; font-size:12px" >
						<div id="progress_container"> 
						    <div id="progress_bar">
						    	<div id="progress_completed"></div> 
						    </div>
						</div>
						<span id="progress_text" style="font-size: 14px; color: green; padding: 0 0 0 5px;">0</span> % Complete
					</div>
					<div class="clear">&nbsp;</div>
				</div>
			</div>
		</div>
</div>
{/capture}
<script type="text/javascript" src="{$http_location}/skin1/myntra_js/jquery.min.js"></script>
{literal}
<script type="text/javascript">

	
	function showUploadStatusWindow() {
		$("#uploadstatuspopup").css("display","block");
		$("#uploadstatuspopup-content").css("display","block");
		$("#uploadstatuspopup_title").css("display","block");
		
		var oThis = $("#uploadstatuspopup-main");
		var topposition = ( $(window).height() - oThis.height() ) / 2;
		if ( topposition < 10 ) 
			topposition = 10;
		oThis.css("top", ( topposition + $(window).scrollTop() + "px"));
		oThis.css("left", ( $(window).width() - oThis.width() ) / 2+$(window).scrollLeft() + "px");

		setInterval(trackStatus, 1000);

		return true;
	}
	
	function trackStatus() {
		$.get("getprogressinfo.php?progresskey="+progressKey,
			function(percent) {
				if(percent != "false") {
					$("#progress_bar").css('width', percent+"%");
					$("#progress_text").html(percent);
				}
			}
		);
	}
</script>	
{/literal}
{include file="dialog.tpl" title='Upload Products' content=$smarty.capture.dialog extra='width="100%"'}