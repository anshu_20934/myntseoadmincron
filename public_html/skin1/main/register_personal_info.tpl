{* $Id: register_personal_info.tpl,v 1.12 2006/01/31 14:24:05 svowl Exp $ *}
{if $is_areas.P eq 'Y'}
{if $hide_header eq ""}

<div class="super"><p>{$firstname} account information</p> </div>
<div class="head"><p>fill your login information</p></div>
{/if}
<div class="links_980"><div class="legend_980"><p>(<span class="mandatory">*</span>)&nbsp;<span class="mandatory">Mandatory fields</span></p></div></div>
{if $default_fields.firstname.avail eq 'Y'}



<div class="links_980"><div class="legend_980"><p>{$lng.lbl_first_name}<span class="mandatory">*</span></p></div>
	<div class="registerfield_980">
		<label>
			<input type="text" id="firstname" name="firstname"  value="{$userinfo.firstname}" class="inputtextbox" tabindex="0"/>
		</label> 
	</div><div class="clearall"></div>
</div>
{/if}

{if $default_fields.lastname.avail eq 'Y'}
<div class="links_980"><div class="legend_980"><p>{$lng.lbl_last_name}<span class="mandatory">*</span></p></div>
	<div class="registerfield_980">
	<label>
		<input type="text" id="lastname" name="lastname"  value="{$userinfo.lastname}" class="inputtextbox" tabindex="1" />
		{if $reg_error ne "" and $userinfo.lastname eq "" && $default_fields.lastname.required eq 'Y'}{/if}
	</label>
	</div><div class="clearall"></div>
</div>
{/if}

<div class="links_980"><div class="legend_980"><p>{$lng.lbl_email}<span class="mandatory">*</span></p></div>
<div class="registerfield_980"><label>
{if $userinfo.login ne "" || ($login eq $userinfo.uname && $login ne '')}
<b>{$userinfo.login|default:$userinfo.uname}</b>
<input type="hidden" name="uname" value="{$userinfo.login|default:$userinfo.uname}" />
{else}
<input  type="text" id="uname" name="uname"  value="{if $userinfo.uname}{$userinfo.uname}{else}{$userinfo.login}{/if}" class="inputtextbox" />
&nbsp;<span class="greymandatory">(your e-mail will be your username)</span>
{/if}
</label></div><div class="clearall"></div>
</div>

<div class="links_980"><div class="legend_980"><p>{$lng.lbl_password}<span class="mandatory">*</span></p></div>
<div class="registerfield_980"><label>
<input  type="password" id="passwd1" name="passwd1"  value="{$userinfo.passwd1}" class="inputtextbox" tabindex="3" />
&nbsp;<span class="greymandatory">(minimum 6 character)</span>
{if $reg_error ne "" and $userinfo.passwd1 eq ""}{/if} 
</label></div><div class="clearall"></div>
</div>

<div class="links_980"><div class="legend_980"><p>{$lng.lbl_confirm_password}<span class="mandatory">*</span></p></div>
<div class="registerfield_980"><label>
<input type="password" id="passwd2" name="passwd2"  value="{$userinfo.passwd2}" class="inputtextbox" tabindex="4"/>
{if $reg_error ne "" and $userinfo.passwd2 eq ""}{/if} 
</label></div><div class="clearall"></div>
</div>

<div class="links_980"><div class="legend_980"><p>Gender<span class="mandatory">*</span></p></div>
<div class="registerfield_980"><label>
	<select name="gender">
		{if $userinfo.gender eq 'M'}					
			<option value="M" selected>Male</option>
			<option value="F">Female</option>
		{else}
			<option value="M" selected>Male</option>
			<option value="F" >Female</option>
		{/if}
	</select>	
</label></div><div class="clearall"></div>
</div>

<div class="links_980"><div class="legend_980"><p>Date of Birth</p></div>
<div class="registerfield_980">
	{php}
		$userinfo = $this->get_template_vars('userinfo');	
		$year = date("Y");
		$month = 12;
		$day = 31; 
		$dayselect = "<select style='font-weight:normal;font-size:9pt;width:50px;' name='day'><option value=''>day</option>";
		for($i=1;$i<=$day;$i++){
			if($userinfo['DOB_day'] == $i)
				$dayselect .= "<option value='$i' selected>$i</option>";
			else
				$dayselect .= "<option value='$i'>$i</option>";
		}
		$dayselect .= "</select>";
		$i=1;
		$monthselect = " <select style='font-weight:normal;font-size:9pt;width:60px;' name='month'><option value=''>month</option>";
		for($i=1;$i<=$month;$i++){
			if($userinfo['DOB_month'] == $i)
				$monthselect .= "<option value='$i' selected>$i</option>";
			else
				$monthselect .= "<option value='$i'>$i</option>";
		}		
		$monthselect .= "</select>";
		$i=1;
		$yearselect = " <select style='font-weight:normal;font-size:9pt;width:60px;' name='year'><option value=''>year</option>";
		for($i=$year;$i>=1900;$i--){
			if($userinfo['DOB_year'] == $i)
				$yearselect .= "<option value='$i' selected>$i</option>";
			else
				$yearselect .= "<option value='$i'>$i</option>";
		}
		$yearselect .= "</select>";
		
		echo $dayselect.$monthselect.$yearselect;	 	
	{/php}		
</div><div class="clearall"></div>
</div>	

<!-- added in later stage for about me, interest and  links -->

<div class="links_980"><div class="legend_980"><p> About  Me</p></div>
<div class="registerfield_980">
<label> 
<textarea id="about_me" name="about_me" class="address">{$userinfo.about_me}</textarea>
</label>
</div><div class="clearall"></div>
</div>

<div class="links_980"><div class="legend_980"><p>My Interests</p></div>
<div class="registerfield_980"><label>
<textarea id="interests" name="interests" class="address">{$userinfo.interests}</textarea>
</label></div><div class="clearall"></div>
</div>

<div class="links_980"><div class="legend_980"><p>My Links </p></div>
<div class="registerfield_980"><label>
<textarea id="mylinks" name="mylinks" class="address">{$userinfo.mylinks}</textarea>
</label></div><div class="clearall"></div>
</div>
<!-- end -->

{if $default_fields.image.avail eq 'Y'}
<div class="links_980"><div class="legend_980"><p>Upload Your image</p></div>
      <div class="registerfield_980">
      <label>
           <input type="file" id="myimage" name="myimage" >
	   </label>
              <!-- <input class="submit" type="submit" name="submitButton" value="Upload" border="0"  onClick="return checkImageFile(document.registerform.myimage); "> -->
	      
	
       </div> <div class="clearall"></div>         
</div>
{/if}

{if $userinfo.myimage  ne "" }

<div class="links_980"><div class="legend_980"><p>Your uploaded image</p></div>

<div class="registerfield_980">
<!-- <span class="tip" onmouseover='doTooltip(event, "./images/designer/T/{$userinfo.myimage}")' onmouseout="hideTip()"> -->
           <p><img src="{$cdn_base}/images/designer/T/{$userinfo.myimage}"   />
           <input class="submit" type="button" name="deleteImage" value="Delete image" border="0" 
	   onClick= "javascript:deleteUserImage('{$userinfo.myimage}');"></p>
<!-- </span> -->
</div> 
	<div class="clearall"></div>         
</div>
{/if}
<div class="links_980">
	<div class="legend_980"><p>Subscribe daily design</p> </div>
	<div class="field">
		<label> 
			<input type="checkbox" id="dailydesign" name="dailydesign" value= "1" class="address" {if $userinfo.dailydesign eq '1'}checked {else}{/if}/>
		</label>
	</div>
	
	<div class="clearall"></div>
</div>


<!-- <div class="links_980"><div class="legend_980"><p>{$lng.lbl_email}<span class="mandatory">*</span></p></div>
<div class="registerfield_980">
<input  type="text" id="email" name="email"  value="{$userinfo.email}" tabindex="2" />
{if $emailerror ne "" or ($reg_error ne "" and $userinfo.email eq "" and $default_fields.email.required eq 'Y')}{/if}
</label></div><div class="clearall"></div>
</div> -->
<div class="foot"></div>

<div class='addressbox'>
<div class="addressminusimage" id="addressminusimage" style="cursor:pointer;visibility:hidden;display:none" onclick='javascript: hideAddress("addressinfo");'></div>
<div class="addressplusimage" id="addressplusimage" style='cursor:pointer;' onclick='javascript: displayAddress("addressinfo");'></div>&nbsp;
<strong style="vertical-align:top;">shipping and billing address</strong>&nbsp;<span class="greymandatory">(optional)</span>
</div>

	
<br>
<br>

{*{include file="main/register_additional_info.tpl" section="P"}*}
{/if}
