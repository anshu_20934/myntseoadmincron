$(function() {
	$('#revert-cancel-form').submit(function(){
		var itemids = '';
		$('input:checkbox[name="cancelled_item"]:checked').each(function(){
			if(itemids!='') { itemids += ','}
			itemids += $(this).val();
		});
		if(itemids != '') {
			$("#itemids").val(itemids);
		} else {
			alert('Please select an item to add to order');
			return false;
		}
		return true;
	});
	
	$("#cancel-items-form").submit(function() {
		$("#cancel-items-yes").attr('disabled','disabled');
		$("#msgBar").html("");
		var errors = "";
		if( $("#cancel_reason").val() == "0") {
			errors += "Please select a reason to cancel<br/>";
		}
		if($("#cancel_comment").val() == "") {
			errors += "Please add a remark for cancelling items<br/>";
		}
		if(errors != "") {
			$("#msgBar").html(errors);
			$("#cancel-items-yes").removeAttr('disabled','');
			return false;
		}
		return true;
	});
	
	$("#split-order-form").submit(function() {
		$("#msgBar").html("");
		var errors = "";
		if( $("#split_reason").val() == "0") {
			errors += "Please select a reason for split<br/>";
		}
		if($("#split_comment").val() == "") {
			errors += "Please add a remark for splitting the order<br/>";
		}
		if(errors != "") {
			$("#msgBar").html(errors);
			return false;
		}
		return true;
	});
});