{* $Id: orders_list.tpl,v 1.18.2.2 2006/06/16 10:47:41 max Exp $ *}



{***********************End user block*************************}

{******************************************************************}
{**************Below block display the order list in admin interface**************}
{******************************************************************}


{if $usertype eq "A" || $usertype eq "P"}
{if  $mode == "search"}

{assign var="total" value=0.00}
{assign var="total_paid" value=0.00}

{capture name=dialog}

<div align="right">{include file="buttons/button.tpl" button_title=$lng.lbl_search_again href="orders.php"}</div>

{include file="customer/main/navigation.tpl"}

{include file="main/check_all_row.tpl" form="processorderform" prefix="orderids"}

<form action="process_order.php" method="post" name="processorderform">
<input type="hidden" name="mode" value="" />


<table cellpadding="2" cellspacing="1" width="100%" >

{assign var="colspan" value=6}

<tr class="TableHead"  >
	<td width="5">&nbsp;</td>
	<td width="5%" nowrap="nowrap">{if $search_prefilled.sort_field eq "orderid"}{include file="buttons/sort_pointer.tpl" dir=$search_prefilled.sort_direction}&nbsp;{/if}<a href="orders.php?mode=search&amp;sort=group_id">Order ID</a></td>
	<td width="5%" nowrap="nowrap">{if $search_prefilled.sort_field eq "orderid"}{include file="buttons/sort_pointer.tpl" dir=$search_prefilled.sort_direction}&nbsp;{/if}<a href="orders.php?mode=search&amp;sort=orderid">Shipment ID</a></td>
	<td width="3%" nowrap="nowrap">{if $search_prefilled.sort_field eq "status"}{include file="buttons/sort_pointer.tpl" dir=$search_prefilled.sort_direction}&nbsp;{/if}<a href="orders.php?mode=search&amp;sort=status">{$lng.lbl_status}</a></td>
	<td width="5%" nowrap="nowrap">{if $search_prefilled.sort_field eq "status"}{include file="buttons/sort_pointer.tpl" dir=$search_prefilled.sort_direction}&nbsp;{/if}<a href="orders.php?mode=search&amp;sort=status">{$lng.lbl_payment_mode}</a></td>
	<td width="30%" nowrap="nowrap">{if $search_prefilled.sort_field eq "customer"}{include file="buttons/sort_pointer.tpl" dir=$search_prefilled.sort_direction}&nbsp;{/if}<a href="orders.php?mode=search&amp;sort=customer">{$lng.lbl_customer}</a></td>
{if $usertype eq "A" and $single_mode eq ""}
{assign var="colspan" value=7}
	<td width="30%" nowrap="nowrap">{if $search_prefilled.sort_field eq "contact"}{include file="buttons/sort_pointer.tpl" dir=$search_prefilled.sort_direction}&nbsp;{/if}<a href="orders.php?mode=search&amp;sort=contact">Contact No.</a></td>
	<td width="5%" nowrap="nowrap">
	{if $search_prefilled.sort_field eq "qtyInOrder"}
		{include file="buttons/sort_pointer.tpl" dir=$search_prefilled.sort_direction}&nbsp;{/if}
			<a href="orders.php?mode=search&amp;sort=qtyInOrder">{$lng.lbl_qtyInOrder}</a></td>
  	{/if}
<td width="5%" nowrap="nowrap">{if $search_prefilled.sort_field eq "gift_status"}{include file="buttons/sort_pointer.tpl" dir=$search_prefilled.sort_direction}&nbsp;{/if}<a href="orders.php?mode=search&amp;sort=gift_status">Gift</a></td>
<td width="20%" nowrap="nowrap">{if $search_prefilled.sort_field eq "date"}{include file="buttons/sort_pointer.tpl" dir=$search_prefilled.sort_direction}&nbsp;{/if}<a href="orders.php?mode=search&amp;sort=date">{$lng.lbl_date}</a></td>
<td width="20%" nowrap="nowrap">{if $search_prefilled.sort_field eq "date"}{include file="buttons/sort_pointer.tpl" dir=$search_prefilled.sort_direction}&nbsp;{/if}<a href="orders.php?mode=search&amp;sort=date">Queued Date</a></td>
<td width="5%" nowrap="nowrap">{if $search_prefilled.sort_field eq "date"}{include file="buttons/sort_pointer.tpl" dir=$search_prefilled.sort_direction}&nbsp;{/if}<a href="orders.php?mode=search&amp;sort=date">{$lng.lbl_payment_status}</a></td>
<td width="10%" align="right" nowrap="nowrap">{if $search_prefilled.sort_field eq "total"}{include file="buttons/sort_pointer.tpl" dir=$search_prefilled.sort_direction}&nbsp;{/if}<a href="orders.php?mode=search&amp;sort=total">{$lng.lbl_total}</a></td>
</tr>
{if $orders}
{section name=oid loop=$orders}

{assign var=amount value=$orders[oid].total}
{assign var=shipcost value=$orders[oid].shipping_cost}
{assign var=ref_discount value=$orders[oid].ref_discount}
{assign var=gift_charges value=$orders[oid].gift_charges}
{assign var=cod value=$orders[oid].cod}
{assign var=amounttobepaid value=$amount+$cod}
{assign var=amounttobepaid value=$amounttobepaid-$ref_discount}
{assign var=codneedscsverification value=0}


{math equation="x + ordertotal" x=$total ordertotal=$amounttobepaid assign="total"}
{if $orders[oid].status eq "P" or $orders[oid].status eq "C"}
{math equation="x + ordertotal" x=$total_paid ordertotal=$amounttobepaid assign="total_paid"}
{/if}

{if $orders[oid].payment_method == 'cod' and $orders[oid].cod_needs_cs_verification == 1} 
{assign var=codneedscsverification value=1}
{/if}

{if $orders[oid].cod_onhold_title_text}
	{assign var=codonholdalttext value=$orders[oid].cod_onhold_title_text}
{else}
	{assign var=codonholdalttext value=''}
{/if}

{if $orders[oid].cod_onhold_title_text}
<tr style="background-color:white" {if $orders[oid].product_style && ($orders[oid].product_style eq '1092' || $orders[oid].product_style eq '1093')}style="background-color:red;font-weight:normal;"{else}{cycle values=", class='TableSubHead'"}{/if}>
{elseif $orders[oid].payment_method == 'cod'}
<tr style="background-color:#FFCC33" {if $orders[oid].product_style && ($orders[oid].product_style eq '1092' || $orders[oid].product_style eq '1093')}style="background-color:red;font-weight:normal;"{else}{cycle values=", class='TableSubHead'"}{/if}>
{else}
<tr {if $orders[oid].product_style && ($orders[oid].product_style eq '1092' || $orders[oid].product_style eq '1093')}style="background-color:red;font-weight:normal;"{else}{cycle values=", class='TableSubHead'"}{/if}>
{/if}
	<td width="5"><input type="checkbox" name="orderids[{$orders[oid].orderid}]" /></td>
	<td>
    <a href="order.php?orderid={$orders[oid].group_id}" target="_blank">
		#{$orders[oid].group_id}
	</a>
	</td>
	<td>
    {if $orders[oid].is_customizable eq '0'}<span style="background-color:red;font-weight:bold;padding-left:4px;padding-right:4px;">NP</span>{/if}
    <a href="order.php?orderid={$orders[oid].orderid}" target="_blank" {$codonholdalttext} >
        #{$orders[oid].orderid}
    </a>
    </td>
	<td nowrap="nowrap" align="center">

{if $usertype eq "A" or ($usertype eq "P" and $active_modules.Simple_Module ne "")}
{*$orders[oid].status*}
<!--<input type="hidden" name="order_status_old[{$orders[oid].orderid}]" value="{$orders[oid].status}" />-->
{include file="main/order_status.tpl" status=$orders[oid].status mode="static" }
{else}
<a href="order.php?orderid={$orders[oid].orderid}" {$codonholdalttext}>
	<b>{include file="main/order_status.tpl" status=$orders[oid].status mode="static"}</b></a>
{/if}

{if $active_modules.Stop_List ne '' && $orders[oid].blocked  eq 'Y'}
<img src="{$cdn_base}/skin1/images/no_ip.gif" style="vertical-align: middle;" alt="" />
{/if}
	</td>
	<td align="center">
	{if $orders[oid].payment_method == 'chq'} Check
		{elseif $orders[oid].payment_method == 'cod'} Cash
		{elseif $orders[oid].payment_method == 'cash'} Cash
		{elseif $orders[oid].payment_method == 'inm'} Invoice Me
		{else} CC/Net Banking
	{/if}
	</td>
	<td>{$orders[oid].firstname} {$orders[oid].lastname} ({$orders[oid].login})</td>
	<td align="center">{$orders[oid].issues_contact_number}</td>
{if $usertype eq "A" and $single_mode eq ""}
	<td align="center">{$orders[oid].qtyInOrder}</td>
{/if}
	<td nowrap="nowrap" align="center"><a href="order.php?orderid={$orders[oid].orderid}" {$codonholdalttext}>{$orders[oid].gift_status}</a></td>
	<td nowrap="nowrap" align="center"><a href="order.php?orderid={$orders[oid].orderid}" {$codonholdalttext}>{$orders[oid].date|date_format:$config.Appearance.datetime_format}</a></td>
	<td nowrap="nowrap" align="center">
		{if $codneedscsverification eq 1} 
			Pending Customer Care Verification
		{else}
			<a href="order.php?orderid={$orders[oid].orderid}" {$codonholdalttext}>{$orders[oid].queueddate|date_format:$config.Appearance.datetime_format}</a>
		{/if}
	</td>
	<td align="center"><input type="checkbox" name="ccavenue[{$orders[oid].orderid}]"  {if $orders[oid].ccavenue == 'Y'} checked='checked'{/if}/></td>
	<td nowrap="nowrap" align="right">
	<a href="order.php?orderid={$orders[oid].orderid}" {$codonholdalttext}>{include file="currency.tpl" value=$amounttobepaid}</a>
	</td>
</tr>
<input type="hidden" name="order_email[{$orders[oid].orderid}]" value="{$orders[oid].email}" />
{/section}
<tr>
	<td colspan="8"><img src="{$cdn_base}/skin1/images/spacer.gif" width="100%" height="1" alt="" /></td>
</tr>

<tr>
	<td colspan="8" align="right">{$lng.lbl_gross_total}: <b>{include file="currency.tpl" value=$total}</b></td>
</tr>

<tr>
	<td colspan="8" align="right">{$lng.lbl_total_paid}: <b>{include file="currency.tpl" value=$total_paid}</b></td>
</tr>

{else}
<tr><td colspan="8" align="center"><strong>No order found for the given criteria</strong></td></tr>
{/if}


<tr>
	<td colspan="8"><br />

{include file="customer/main/navigation.tpl"}

{if $usertype eq "A" or ($usertype eq "P" and $active_modules.Simple_Mode)}
<!--<input type="button" value="{$lng.lbl_update_status|escape}" onclick="javascript: submitForm(this, 'update');" /> 
&nbsp;&nbsp;&nbsp;&nbsp;
<br /><br />-->
{/if}

<!--<input type="button" value="{$lng.lbl_invoices_for_selected|escape}" onclick="javascript: if (checkMarks(this.form, new RegExp('orderids\[[0-9]+\]', 'gi'))) {ldelim} document.processorderform.target='invoices'; submitForm(this, 'invoice'); document.processorderform.target=''; {rdelim}" />-->
<input type="button" value="{$lng.lbl_upd_selected|escape}" onclick="javascript: if (checkMarks(this.form, new RegExp('ccavenue\[[0-9]+\]', 'gi'))) submitForm(this, 'update'); " />
&nbsp;&nbsp;&nbsp;&nbsp;
{if $usertype ne "C"}
<input type="button" value="{$lng.lbl_labels_for_selected|escape}" onclick="javascript: if (checkMarks(this.form, new RegExp('orderids\[[0-9]+\]', 'gi'))) {ldelim} document.processorderform.target='labels'; submitForm(this, 'label'); document.processorderform.target=''; {rdelim}" />
&nbsp;&nbsp;&nbsp;&nbsp;
{/if}
{if $usertype eq "A" or ($usertype eq "P" and $active_modules.Simple_Mode)}
<!--<input type="button" value="{$lng.lbl_delete_selected|escape}" onclick="javascript: if (checkMarks(this.form, new RegExp('orderids\[[0-9]+\]', 'gi'))) if (confirm('{$lng.txt_delete_selected_orders_warning|strip_tags}')) submitForm(this, 'delete');" />
&nbsp;&nbsp;&nbsp;&nbsp; -->
{/if}
{if $active_modules.Shipping_Label_Generator ne '' && ($usertype eq 'A' || ($usertype eq 'P' && $active_modules.Simple_Mode))}
<input type="button" value="{$lng.lbl_get_shipping_labels|escape}" onclick="javascript: if (checkMarks(this.form, new RegExp('orderids\[[0-9]+\]', 'gi'))) {ldelim} document.processorderform.action='generator.php'; submitForm(this, ''); {rdelim}" />
&nbsp;&nbsp;&nbsp;&nbsp;
{/if}

{if $usertype ne "C"}
<br />
<br />
<br />
{include file="main/subheader.tpl" title=$lng.lbl_export_orders}
{$lng.txt_export_all_found_orders_text}
<br /><br />
{$lng.lbl_export_file_format}:<br />
<select id="export_fmt" name="export_fmt">
	<option value="std">{$lng.lbl_standart}</option>
	<option value="csv_tab">{$lng.lbl_40x_compatible}: CSV {$lng.lbl_with_tab_delimiter}</option>
	<option value="csv_semi">{$lng.lbl_40x_compatible}: CSV {$lng.lbl_with_semicolon_delimiter}</option>
	<option value="csv_comma">{$lng.lbl_40x_compatible}: CSV {$lng.lbl_with_comma_delimiter}</option>
{if $active_modules.QuickBooks eq "Y"}
{include file="modules/QuickBooks/orders.tpl"}
{/if}
</select>
<br />
<br />
<input type="button" value="{$lng.lbl_export|escape}" onclick="javascript: if (checkMarks(this.form, new RegExp('orderids\[[0-9]+\]', 'gi'))) submitForm(this, 'export');" />&nbsp;&nbsp;&nbsp;
<input type="button" value="{$lng.lbl_export_all_found|escape}" onclick="javascript: self.location='orders.php?mode=search&amp;export=export_found&amp;export_fmt='+document.getElementById('export_fmt').value;" />
{/if}
</td>
</tr>

</table>
</form>

{/capture}
{include file="dialog.tpl" title=$lng.lbl_search_results content=$smarty.capture.dialog extra='width="100%"'}


{/if}
{/if}

{***********************End admin block*************************}
