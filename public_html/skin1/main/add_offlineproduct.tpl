{include file="main/include_js.tpl" src="main/ajax_quantityprice.js"}
{config_load file="$skin_config"}
<html>
<head>
	<title>Add offline product</title>
	<link rel="stylesheet" href="{$SkinDir}/skin1_admin.css" />
    
</head>
<body>
<br><br>
{capture name=dialog}

<form name="offlineproduct" id="offlineproduct" action="add_offlineproduct.php" method=post>
    <input type="hidden" name="custid" id="custid" value="{$custid}">
    <input type="hidden" name="styleid" id="styleid" >
    <input type="hidden" name="typeid" id="typeid" >
    <input type="hidden" name="provider" id="provider">
    <input type="hidden" name="updatevalues" id="updatevalues" >
    <input type="hidden" name="autoid" id="autoid" >
    
<table cellpadding="4" cellspacing="0" width="100%" align="center">

<tr> 
	<td class="FormButton" nowrap="nowrap" >Product id<span class="Star">*</span></td>
	<td class="ProductDetails"><input type="text" name="productid" id="productid" tabindex=1 onblur="callForQuantity('productid')"/></td>
</tr>
<tr> 
	<td class="FormButton" nowrap="nowrap" >Select options<span class="Star">*</span></td>
	<td class="ProductDetails"><div name="typeoptions" id="typeoptions"></div></td>
</tr>
<tr> 
	<td class="FormButton" nowrap="nowrap">Product name<span class="Star">*</span></td>
	<td class="ProductDetails"> <input type="text" name="pname" id="pname" readonly/></td>
</tr>
<tr> 
	
	<td class="FormButton" nowrap="nowrap">List price<span class="Star">*</span></td>
	<td class="ProductDetails"><b>(Rs.)</b><input type="text" name="price" id="price" /></td>
</tr>
<tr> 
	<td class="FormButton" nowrap="nowrap" >Quantity<span class="Star">*</span></td>
	<td class="ProductDetails"> <input type="text" name="quantity" id="quantity" tabindex=2/></td>
</tr>
<tr> 
	<td class="FormButton" nowrap="nowrap" >Notes</td>
	<td class="ProductDetails">
            <textarea name="notes" id="notes" cols=40 rows=5  tabindex=3></textarea>
        </td>
</tr>
<tr> 
	<td class="FormButton" nowrap="nowrap" >
        <input type="submit" value=" {$lng.lbl_save} "  onClick="return validateProductForm(this.form);" tabindex=4/>
        </td>
        <td class="FormButton" nowrap="nowrap" >
        <input type="submit" value=" Cancel "  
	onClick="javascript:window.close();" tabindex=4/>
        </td>
	
</tr>
</table>
</form>




<form method="post" name="productform">

<table cellpadding="3" cellspacing="1" width="100%">

<tr class="TableHead">
    <td width="20%">Product Id</td>
    <td width="35%" align="center">Product Name</td>
    <td width="10%" align="center">Quantity</td>
    <td width="10%" align="center">Unit Price</td>
    <td width="10%" align="center">Amount</td>
    <td width="25%" align="center">Action</td>
</tr>

{if $products}

{assign var="qtytotal" value="0"}
{assign var="pricetotal" value="0"}
{assign var="totalamount" value="0"}
{section name=prod_num loop=$products}

<tr{cycle values=", class='TableSubHead'"}>
    <td>{$products[prod_num].productid}</td>
    <td align="center">{$products[prod_num].pname}</td>
    <td align="center">{$products[prod_num].quantity}</td>
    <td align="center">{$products[prod_num].price}</td>
    {math equation="( x * y )" x=$products[prod_num].quantity y=$products[prod_num].price assign="amount" }
    <td align="center">{$amount}</td>
    <td align="center">
    <input type="button" name="pupdate" value="Update" onClick="javascript:proUpdate('{$products[prod_num].productid}','{$products[prod_num].custid}');">
    <input type="button" name="pdelete" value="Delete" onClick="return proDelete('{$products[prod_num].productid}','{$products[prod_num].custid}','{$products[prod_num].pname}');">
    </td>
</tr>
{assign var="qtytotal" value=$qtytotal+$products[prod_num].quantity}
{assign var="pricetotal" value=$pricetotal+$products[prod_num].price}
{assign var="totalamount" value=$totalamount+$amount}
{/section}

<tr class='TableSubHead'>
    <td colspan="2" class="SubmitBox"><b>Total</b></td>
    <td colspan="1" class="SubmitBox"><input type="text" name="totqty" id="totqty" size="10" value={$qtytotal} readonly/></td>
    <td colspan="1" class="SubmitBox"><input type="text" name="totprice" id="totprice" size="10" value={$pricetotal} readonly/></td>
    <td colspan="1" class="SubmitBox"><input type="text" name="totamount" id="totamount" size="10" value={$totalamount} readonly/></td>
    <td colspan="1" class="SubmitBox">&nbsp;</td>  
</tr>
<tr class='TableSubHead'>
    <td colspan="1" class="SubmitBox"><b>Discount</b></td>
    <td colspan="1" class="SubmitBox">
    <input type="text" name="discount" id="discount" size="8" />&nbsp;
    <input type="radio" name="distype" id="distype" value="ABS" onClick="javascript:calculateDiscount('ABS');" ><b>(ABS)</b>
    &nbsp;&nbsp;<input type="radio" name="distype" id="distype" value="PST" onClick="javascript:calculateDiscount('PST');" ><b>(PCT)</b>
    </td>
    <td colspan="2" class="SubmitBox"><b>Total discount</b>
    <input type="text" name="disamount" id="disamount" size="8" readonly/>
    </td>
    <td colspan="2" class="SubmitBox"><b>Discounted amount</b>
        <input type="text" name="discountedamount" id="discountedamount" size="8" readonly/>
    </td>  
</tr>
<tr class='TableSubHead'>
    <td colspan="1" class="SubmitBox"><b>Tax</b></td>
    <td colspan="1" class="SubmitBox" align="center">
    <input type="radio" name="tax" id="tax" value="4" onClick="javascript:calculateTax('4');" ><b>(4%)</b>
    &nbsp;&nbsp;<input type="radio" name="tax" id="tax" value="12" onClick="javascript:calculateTax('12');" ><b>(12.36%)</b>
    </td>
    <td colspan="2" class="SubmitBox"><b>Total tax</b>
    <input type="text" name="tottax" id="tottax" size="10" readonly/></td>
    <td colspan="2" class="SubmitBox"><b>Amount after tax</b>
        <input type="text" name="aftertax" id="aftertax" size="8" readonly/>
    </td>  
</tr>
<tr class='TableSubHead'>
    <td colspan="2" class="SubmitBox"><b>Shipping Charges</b></td>
    <td colspan="2" class="SubmitBox"><input type="text" name="shiprate" id="shiprate" size="10" onblur="javascript:calculateTax('grd');"/></td>
    <td colspan="2" class="SubmitBox">&nbsp;</td>  
</tr>
<tr class='TableSubHead'>
    <td colspan="2" class="SubmitBox"><b>Grand total</b></td>
    <td colspan="2" class="SubmitBox"><input type="text" name="gtotal" id="gtotal" size="10"/></td>
    <td colspan="2" class="SubmitBox">&nbsp;</td>  
</tr>
<tr class='TableSubHead'>
	<td colspan="6" class="SubmitBox">
	<input type="button" value="Place order" onclick="return placedOrder('{$custid}');"/>
	</td>
</tr>

{else}

<tr>
<td colspan="4" align="center">Record is not available</td>
</tr>

{/if}
</table>
</form>

{/capture}
{include file="dialog.tpl" title="Product List" content=$smarty.capture.dialog extra='width="90%"'}

</body>
</html>