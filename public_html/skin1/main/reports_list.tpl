{capture name=gdialog}
{literal}
<script Language="JavaScript" Type="text/javascript">
		function FrontPage_Form1_Validator(theform)
		{
         //alert("Please Generate a valid sku first");
         var groupname=theform.groupname;
          if (groupname.value==null||groupname.value == "")
		  {
			alert("Please enter the group name");
			groupname.focus();
			return false;
		  }
		  
		  return true;
		  
		}
		function delReport(reportid){
		var del="delete";
		  if(confirm("Do you really want to delete this report")){
		  
		  window.open('edit_report.php?reportid='+reportid+'&action='+del+'','null', 'menubar=1,resizable=1,toolbar=1,width=800,height=500');
		  
		  }
		
		}
	
		</script>
{/literal}
<form action="reports_management.php" method="post" onsubmit="return FrontPage_Form1_Validator(this)" language="JavaScript" name="groupaddform">
<input type="hidden" name="reports_mode" value="addgroup" />
<table cellpadding="3" cellspacing="1" width="100%">
	<tr>
	     <td><b>{$grpmsg}</b></td>
         <td><b>Group Name</b></td>
         <td><input type="text" name="groupname" size="50"  /></td>
    </tr>
	<tr>
		<td colspan=2><input type="submit" style="background-color:#eeeddd;font-size:10px;" value="Add group" /></td>
	</tr>
</table>
</form>
{/capture}
{include file="dialog.tpl" title='Add New Group' content=$smarty.capture.gdialog extra='width="100%"'}
<br>
{capture name=dialog}

<table cellpadding="3" cellspacing="1" width="100%">
<tr >
		
		<td>
			<input type="button" style="background-color:#eeeddd;font-size:10px;" value="Add new Report" onClick="javascript:window.open('add_new_report.php','null', 'menubar=0,resizable=1,toolbar=0,width=900,height=700')">
		<input type="button" style="background-color:#eeeddd;font-size:10px;" value="View All Schedules" onClick="javascript:window.open('edit_report.php?action=viewschedules','null', 'menubar=0,resizable=1,toolbar=0,width=900,height=700')">
		</td>
	</tr>

{if $reports }

{foreach from=$reports key=reportgroup item=report}

<tr style="background-color:#d0ddff">
	<td style="font-size:14px;color:#000077"><b>{$reportgroup}</b></td>
</tr>
{if $report}
<tr>
	<td>
	<table width=40%>		 
{foreach from=$report key=reportid item=reportname}
{if $reportname }
			<tr style="background-color:#eeeddd;padding:5px;margin-bottom:2px;">
			<td style="font-size:12px;">{$reportname}</td>
			<td style="font-size:12px;width:50px;">
			<a href="#"  onClick="javascript:window.open('view_report.php?reportid={$reportid}','null', 'menubar=0,resizable=1,toolbar=0,width=800,height=550')">Run</a>
			</td>
			<td style="font-size:12px;width:50px;">
			<a href="#"  onClick="javascript:window.open('edit_report.php?reportid={$reportid}&action=edit','null', 'menubar=0,resizable=1,toolbar=0,width=900,height=700')">edit</a>
			</td>
			<td style="font-size:12px;width:50px;">
			<a href="#"  onClick="javascript:delReport({$reportid})">delete</a>
			</td>
			<td style="font-size:12px;width:50px;">

            <a href="#"  onClick="javascript:window.open('edit_report.php?action=viewschedules&reportid={$reportid}','null', 'menubar=0,resizable=1,toolbar=0,width=900,height=700')">schedules</a>
		
			</td>
			</tr>
{/if}
{/foreach}
	</table>	
	</td>
</tr>
{/if}
{/foreach}

{else}

<tr>
<td colspan="5" align="center">No Reports Found</td>
</tr>

{/if}

</table>

{/capture}
{include file="dialog.tpl" title='Reports' content=$smarty.capture.dialog extra='width="100%"'}

