{* $Id: register_shipping_address.tpl,v 1.38.2.2 2006/06/22 12:01:32 max Exp $ *}
{if $is_areas.S eq 'Y'}

{if $hide_header eq ""}
<div class="subhead"><p>shipping address</p></div>
{/if}

{if $action eq "cart"}

<input type="hidden" name="action" value="cart" />
<input type="hidden" name="paymentid" value="{$paymentid}" />

{/if}





{if $default_fields.s_address.avail eq 'Y'}



<div class="links_980"><div class="legend_980"><p>Address</p></div>
    <div class="field_980"><label>
     <textarea id="s_address" name="s_address" class="address" onKeyPress=check_length("s_address"); onKeyDown=check_length("s_address");>{$userinfo.s_address}</textarea>
     <input type=hidden size=2 value=255 name="text_num" id="text_num">
      <!-- <input  type="text" id="s_address" name="s_address"  value="{$userinfo.s_address}" /> -->
      {if $reg_error ne "" and $userinfo.s_address eq "" and $default_fields.s_address.required eq 'Y'}{/if}
     </label> </div><div class="clearall"></div>
</div>

{/if}

{*{if $default_fields.s_address_2.avail eq 'Y'}*}

<!-- <div class="links_980"><div class="legend_980"><p>Address (Line2)</p></div>
    <div class="field_980"><label>
      
       <input  type="text" id="s_address_2" name="s_address_2"  value="{$userinfo.s_address_2}" />
	{if $reg_error ne "" and $userinfo.s_address_2 eq "" and $default_fields.s_address_2.required eq 'Y'}{/if}
    </label> </div><div class="clearall"></div>
</div>-->

{*{/if}*}

{if $default_fields.s_country.avail eq 'Y'}

<div class="links_980"><div class="legend_980"><p>Country</p> </div>
	<div class="field_980">

<select name="s_country" id="s_country" size="1" class="countryselect" onchange="check_zip_code()">
{section name=country_idx loop=$countries}
<option value="{$countries[country_idx].country_code}"{if $userinfo.s_country eq $countries[country_idx].country_code} selected="selected"{elseif $countries[country_idx].country_code eq $config.General.default_country and $userinfo.s_country eq ""} selected="selected"{/if}>{$countries[country_idx].country}</option>
{/section}
</select>
{if $reg_error ne "" and $userinfo.s_country eq "" and $default_fields.s_country.required eq 'Y'}{/if}

	</div><div class="clearall"></div>
</div>

{/if}

{if $default_fields.s_state.avail eq 'Y'}

<div class="links_980"><div class="legend_980"><p>State</p> </div>
    <div class="field_980"> 
    <label>

	    {include file="main/states.tpl" states=$states name="s_state" default=$userinfo.s_state default_country=$userinfo.s_country country_name="s_country"}
	    {if $error eq "s_statecode" || ($reg_error ne "" && $userinfo.s_state eq "" && $default_fields.s_state.required eq 'Y')}{/if}

   </label> 
   </div>
   <div class="clearall"></div>
  </div>


{/if}


{if $default_fields.s_city.avail eq 'Y'}

<div class="links_980"><div class="legend_980"><p>City</p> </div>
    <div class="field_980"><label>
      
<input  type="text" id="s_city" name="s_city"  value="{$userinfo.s_city}" />
{if $reg_error ne "" and $userinfo.s_city eq "" and $default_fields.s_city.required eq 'Y'}{/if}

    </label> </div><div class="clearall"></div>
</div>

{/if}


{if $default_fields.s_state.avail eq 'Y' && $default_fields.s_country.avail eq 'Y' && $js_enabled eq 'Y' && $config.General.use_js_states eq 'Y'}
<div class="field_980" style="display: none;">
{include file="main/register_states.tpl" state_name="s_state" country_name="s_country" county_name="s_county" state_value=$userinfo.s_state county_value=$userinfo.s_county}
	</div>
{/if}

{if $default_fields.mobile.avail eq 'Y'}

<div class="links_980"><div class="legend_980"><p>Mobile</p> </div>
    <div class="field_980"> <label> 
      
      <input type="text" id="mobile" name="mobile"  value="{$userinfo.mobile}" maxlength="11" />
    </label> </div><div class="clearall"></div>
  </div>

{/if}

{if $default_fields.phone.avail eq 'Y'}

<div class="links_980"><div class="legend_980"><p>Phone</p> </div>
    <div class="field_980"> <label> 
      
      <input type="text" id="phone" name="phone"  value="{$userinfo.phone}" />
    </label> </div><div class="clearall"></div>
  </div>

{/if}


{if $default_fields.s_zipcode.avail eq 'Y'}

<div class="links_980"><div class="legend_980"><p>Zip/Postal Code</p></div>

            <div class="field_980"><label>
              
	      <input  type="text" id="s_zipcode" name="s_zipcode"  value="{$userinfo.s_zipcode}" onchange="check_zip_code()"  size="7" maxlength="6" />
	      </label>
	      {if $reg_error ne "" and $userinfo.s_zipcode eq "" and $default_fields.s_zipcode.required eq 'Y'}{/if}
              &nbsp;&nbsp;
	      <input class="checkbox" type="checkbox" id="ship2diff" name="ship2diff" value="Y" onclick="javascript: ship2diffOpen();"{if $ship2diff} checked="checked"{/if} />
             
              &nbsp;Billing <!--Ship--> to Different Address </div><div class="clearall"></div>
          </div>

{/if}







<div class="foot"></div>
<!--</div> -->
 <br>


{*include file="main/register_additional_info.tpl" section="S"*}
{/if}

