{* $Id: history_order.tpl,v 1.83.2.6 2006/06/27 08:08:06 max Exp $ *}
{include file="page_title.tpl" title="Override Price For Items"}

<script type="text/javascript">
    var orderId = "{$orderid}";
</script>
{include file="main/include_js.tpl" src="/admin/main/order_details.js"}

{capture name=dialog}
{if $action eq "override"}
    {if $errorMsg && $errorMsg ne ''}
        <div style="color:red; font-size:14px; font-weight: bold;">{$errorMsg}</div>
    {else}
		Override the price and enter remarks
		{if $usertype ne "C"}
				<table width="100%"  border="0" cellspacing="0" cellpadding="0">
					<thead style="background-color: #DFDFDF;">
					<div id="msgBar" style="text-align:center; font-weight: bold; color: red;">&nbsp;</div>
						<th width="12%" align=left style="padding: 10px 5px;">Product Image</th>
						<th width="33%" align=left style="padding: 10px 5px;">Style</th>
						<th align="center" width="5%" align=left style="padding: 10px 5px;">Quantity</th>
						<th align="left" width="20%" style="padding: 10px 5px;">Price</th>
						<th align="left" style="padding: 10px 5px;">&nbsp;</th>
					</thead>
				    {section name=idx loop=$productsInCart}
						<tr height="75">
							<td>
								{if $productsInCart[idx].designImagePath != ""}
									<img src="{$productsInCart[idx].designImagePath}"  style="border:1px solid #CCCCCC;">
								{else}
									<img  src="{$xcart_web_dir}/image.php?type={$type}&amp;id={$id}&amp;ts={$smarty.now}" width="100"  height="70" hspace="5" vspace="5" />
								{/if}
							</td>
							<td>{$productsInCart[idx].productStyleLabel} (Article Id: {$productsInCart[idx].article_number})</td>
							<td align="center">{$productsInCart[idx].amount }</td>
							<td colspan=2>
								<text style="padding-right: 50px;">{$productsInCart[idx].price}</text>
								<input type="button" {if $productsInCart[idx].difference_refund != 0.00} disabled value="price overridden already" {else} value="Edit Price"{/if} onclick="priceOverrideSection_{$productsInCart[idx].itemid}.style.display='table-row'"></input>
							</td>
						</tr>
						<tr id="priceOverrideSection_{$productsInCart[idx].itemid}" align="middle" style="display: none;">
							<td colspan=3 vertical-align="top">
								<td width="30%" align="left">
								<div>
									<label>New Price:(<em style="color:red;">*</em>):</label>
									<input price="{$productsInCart[idx].price}" id="newPrice_{$productsInCart[idx].itemid}" name="newPrice_{$productsInCart[idx].itemid}" style="width: 100px;"></input></br></br>
								</div>
								<div vertical-align="top">
									<label>Remarks (<em style="color:red;">*</em>):</label>
									<textarea  id="overrideComment_{$productsInCart[idx].itemid}" name="overrideComment_{$productsInCart[idx].itemid}" style="width: 400px; height: 50px;"></textarea>
								</div>
								</td>
								<input type=hidden name=itemid value="{$productsInCart[idx].itemid}" />
								<td>
						    		<input type=button class="priceOverRideSubmit" itemid = {$productsInCart[idx].itemid} name="price-override_{$productsInCart[idx].itemid}" id="price-override_{$productsInCart[idx].itemid}" value="Override Price" />
						    	</td>
				    		</td>
						</tr>
						<tr height=20><td colspan=6><hr/></td></tr>
					{/section}
						<input type=hidden name=orderid value="{$orderid}" />
						<input type=hidden name=login value="{$login}" />
				</table>
		{/if}
	{/if}
	{/capture}
	{include file="dialog.tpl" title=$lng.lbl_products_details content=$smarty.capture.dialog extra='width="100%"'}
	<div align="right"><input type=button name="return-btn" id="return-btn" value="Close" style="padding:5px 10px;" onclick="opener.window.location.reload(); self.close();"/></div>
{else}
	{if $errorMsg && $errorMsg ne ''}
		<div style="color:red; font-size:14px; font-weight: bold;">{$errorMsg}</div>
	{else}
		<input type=button name="return-btn" id="return-btn" value="Close" style="padding:5px 10px;" onclick="opener.window.location.reload(); self.close();"/>
	{/if}
{/if}