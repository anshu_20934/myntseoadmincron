<script type="text/javascript" src="{$http_location}/skin1/js_script/jquery-1.4.2.min.js"></script>
{literal}
<script type="text/javascript">
	function array2json(arr) {
	    var parts = [];
	    var is_list = (Object.prototype.toString.apply(arr) === '[object Array]');
	
	    for(var key in arr) {
	    	var value = arr[key];
	        if(typeof value == "object") { //Custom handling for arrays
	            if(is_list) parts.push(array2json(value)); /* :RECURSION: */
	            else parts[key] = array2json(value); /* :RECURSION: */
	        } else {
	            var str = "";
	            if(!is_list) str = '"' + key + '":';
	
	            //Custom handling for multiple data types
	            if(typeof value == "number") str += value; //Numbers
	            else if(value === false) str += 'false'; //The booleans
	            else if(value === true) str += 'true';
	            else str += '"' + value + '"'; //All other things
	            parts.push(str);
	        }
	    }
	    var json = parts.join(",");
	    
	    if(is_list) return '[' + json + ']';//Return numerical JSON
	    return '{' + json + '}';//Return associative JSON
	}

	function displayVals() {
      var multipleValues = $("#wms_type").val() || [];
      $("#new_permissions").text("Loading...");
      $.post('/user/permission/', {data: array2json(multipleValues)},
              function(data){
				$("#new_permissions").text(data);
              }, 'json');
    }

	$(document).ready(function(){
    	$("#wms_type").change(displayVals);
	});
</script>
{/literal}

<tr>
<td colspan="3" class="RegSectionTitle">Assign WMS Roles<hr size="1" noshade="noshade" /></td>
</tr>

<tr>
	<td align="right" >Assign Roles </td>
	<td align="left">
		<table cellspacing="1" cellpadding="2" width="100%" border="1">
			<tbody>
				<tr><td colspan="2" align="center">(Can select multiple roles by pressing control)</td></tr>
				<tr>
					<th>Current Persmissions</th>
					<th>New Permissions</th>
				</tr>
				<tr>
					<td>{$current_permissions}</td>
					<td><span id="new_permissions">{$current_permissions}</span></td>
				</tr>
			</tbody>
		</table>
	</td>
	<td nowrap="nowrap">
		<select name="wms_type[]" id="wms_type" size="4" multiple="multiple" style="width:300px;">
		{foreach from=$all_roles key=key item=item}
			{if $wms_user_role_ids.$key eq 1}
				<option name="{$key}" value="{$key}" selected>{$item}</option>
			{else}
				<option name="{$key}" value="{$key}">{$item}</option>
			{/if}
		{/foreach}
		</select>
	</td>
</tr>