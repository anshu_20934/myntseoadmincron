{* $Id: banner_info.tpl,v 1.16.2.3 2006/08/08 08:02:51 max Exp $ *}
{include file="page_title.tpl" title=$lng.lbl_banners_statistics}
{$lng.txt_banner_stats_note}<br /><br />

<!-- IN THIS SECTION -->

{include file="dialog_tools.tpl"}

<!-- IN THIS SECTION -->
<br />
 
{capture name=dialog}
<form action="banner_info.php" method="post">
<table>
<tr>
	<td>{$lng.lbl_period_from}:</td>
	<td>{html_select_date prefix="Start" time=$search.start_date|default:$month_begin start_year=$config.Company.start_year end_year=$config.Company.end_year}</td>
</tr>
<tr>
    <td>{$lng.lbl_period_to}:</td>
    <td>{html_select_date prefix="End" time=$search.end_date start_year=$config.Company.start_year end_year=$config.Company.end_year}</td>
</tr>
{if $usertype eq 'A' || ($usertype eq 'P' && $active_modules.Simple_Mode ne '')}
<tr>
    <td>{$lng.lbl_partner}:</td>
    <td>
	<select name="search[partner]">
		<option value=''{if $search.partner eq ''} selected="selected"{/if}>{$lng.lbl_all}</option>
	{if $partners ne ''}
	{foreach from=$partners item=v}
		<option value='{$v.login}'{if $search.partner eq $v.login} selected="selected"{/if}>{$v.login} ({$v.firstname} {$v.lastname})</option>
	{/foreach}
	{/if}
	</select>
	</td>
</tr>
{/if}
<tr>
	<td>&nbsp;</td>
	<td><input type="submit" value="{$lng.lbl_search|strip_tags:false|escape}" /></td>
</tr>
</table>
</form>
{/capture}
{include file="dialog.tpl" content=$smarty.capture.dialog title=$lng.lbl_search extra='width="100%"'}

{if $banners ne ''}
<br />

{capture name=dialog}
<table width="100%" cellpadding="2" cellspacing="2">
<tr class="TableHead">
	<td>{$lng.lbl_banner}</td>
	<td>{$lng.lbl_clicks}</td>
	<td>{$lng.lbl_views}</td>
	<td nowrap="nowrap">{$lng.lbl_click_rate}</td>
</tr>
{foreach from=$banners item=v}
<tr>
	<td>{if $v.bannerid > 0}{if $usertype ne 'B' && $v.banner}<a href="partner_banners.php?bannerid={$v.bannerid}">{/if}{$v.banner|default:$lng.lbl_deleted_banner}{if $usertype ne 'B' && $v.banner}</a>{/if}{else}{$lng.lbl_default_banner}{/if}{if $v.productid > 0} ({$lng.lbl_product}: <a href="product.php?productid={$v.productid}">{$v.product|truncate:50}</a>){if $v.class eq 1}, {$lng.lbl_detailed}{elseif $v.class eq 2}, {$lng.lbl_normal}{elseif $v.class eq 3}, {$lng.lbl_compact}{/if}{/if}</td>
    <td align="right">{$v.clicks}</td>
    <td align="right">{$v.views}</td>
	<td align="right">{$v.click_rate|formatprice}</td>
</tr>
{/foreach}
<tr>
    <td colspan="4" height="1"><hr size="1" /></td>
</tr>
<tr>
	<td><b>{$lng.lbl_total}:</b></td>
    <td align="right">{$total.clicks}</td>
    <td align="right">{$total.views}</td>
    <td align="right">{$total.click_rate|formatprice}</td>
</tr>
</table>
{/capture}
{include file="dialog.tpl" content=$smarty.capture.dialog title=$lng.lbl_banners_statistics extra='width="100%"'}
{/if}
