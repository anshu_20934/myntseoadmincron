{* $Id: history_order.tpl,v 1.83.2.6 2006/06/27 08:08:06 max Exp $ *}
{include file="page_title.tpl" title=$lng.lbl_order_details_label}

<script type="text/javascript">
    var orderId = "{$orderid}";
</script>
{include file="main/include_js.tpl" src="main/cancel_items.js"}

{if $action eq "view"}
	List of cancelled items for Order No. #{$orderid}
	{capture name=dialog}
		{if $usertype ne "C"}
			<table width="100%"  border="0" cellspacing="0" cellpadding="0">
				{*foreach name=outer item=product from=$productsInCart*}
			    {section name=idx loop=$productsInCart}
					<tr height="50">
						<td width="25"><input type=checkbox name="cancelled_item" value="{$productsInCart[idx].itemid}" /></td>
						<td width="75" align=center>
							{if $productsInCart[idx].designImagePath != ""}
								<img src="{$productsInCart[idx].designImagePath}"  style="border:1px solid #CCCCCC;">
							{else}
								<img  src="{$xcart_web_dir}/image.php?type={$type}&amp;id={$id}&amp;ts={$smarty.now}" width="100"  height="70" hspace="5" vspace="5" />
							{/if}
						</td>
						<td width="50"><strong>Product:</strong> {$productsInCart[idx].productTypeLabel} </td>
						<td width="200"><strong>Style:</strong> {$productsInCart[idx].productStyleLabel} (Article Id: {$productsInCart[idx].article_number})</td>
						<td width="70"><strong>Quantity(s):</strong> {$productsInCart[idx].quantity_breakup} </td>
						<td width="100">
							<strong>Total Price    :</strong> {$productsInCart[idx].totalPrice} <br>							
							<strong>System Discount:</strong> {$productsInCart[idx].discount} <br>
							<strong>Coupon Discount:</strong> {$productsInCart[idx].coupon_discount_product}<br>
							<strong>Cart Discount:</strong> {$productsInCart[idx].cart_discount}<br>
							<strong>PG Discount:</strong> {$productsInCart[idx].pg_discount}<br>
							<strong>Cash Redeemed:</strong> {$productsInCart[idx].cash_redeemed}<br>
							<strong>Loyalty Credit Redeemed:</strong> {$productsInCart[idx].item_loyalty_credit}<br>
                            <strong>Gift Card Amount:</strong> {$productsInCart[idx].gift_card_amount}<br><br>
                            <strong>Final Amount:</strong> {$productsInCart[idx].final_amount}<br>
							<strong>Vat amount:</strong> {$productsInCart[idx].taxamount}<br>
							<strong>Price Mismatch Refund:</strong> {$productsInCart[idx].difference_refund}<br><br>
							<strong>Cashback Credited:</strong> {$productsInCart[idx].cashback}<br>
						</td>
					</tr>
					<tr><td colspan=6><hr></td></tr>
				{/section}
				{*/foreach*}
			</table>
		{/if}
	{/capture}
	{include file="dialog.tpl" title=$lng.lbl_products_details content=$smarty.capture.dialog extra='width="100%"'}
	
	<div style="text-align: right;">
		<form id="revert-cancel-form" action="" method="POST">
			<input type=hidden name="action" value="revert-cancel" />
			<input type=hidden id="itemids" name="itemids" value="" />
			<input type=hidden name="orderid" value="{$orderid}" />
			<input type=hidden name="login" value="{$login}" />
			<!--  <input type=submit id="add-to-order" value="Add Back to order" /> -->
		</form>
	</div>
{else}
	<p>{$message}</p>
	<p>Click on close button to refresh data on back page</p>
	<input type=button name="return-btn" id="return-btn" value="Close" style="padding:5px 10px;" onclick="opener.window.location.reload(); self.close();"/>
{/if}
