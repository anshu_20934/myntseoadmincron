<div class="section">
    <div class="section-head toggleHead">
        <a onclick="loadSectionData(this, {$order.orderid}, 'orderitems');" class="divOpen" href="javascript: void(0);"><p class="arrow">&nbsp;</p> <p>Item Details</p></a>
    </div>
    <div class="section-details" id="section-details-orderitems-{$order.orderid}">
        <div class="contentPane">
            {if $shipments } 
                {foreach from=$shipments key=id item=shipment}
                    <div class="titlePane">Shipment - {$shipment.orderid} &nbsp; &nbsp; Current Status - ({include file="main/order_status.tpl" status=$shipment.status mode="static"}) &nbsp; &nbsp;{if $shipment.orderid neq $orderid} <a href="/admin/order.php?orderid={$shipment.orderid}" target="_blank">View Details</a>{/if}</div>
                    <div class="detailsPane">
                        {if $order.orderid eq $shipment.orderid}
                            <table width="100%" border="0" cellpadding="0" cellspacing="0" class="textBlack">
                                {if $gift_status=='Y'}
                                    <tr height="20">
                                        <td><p  style="font-size:1.5em;color:#FF0000;background-color:#FFFFCC; text-align: center"><strong>This order has to be shipped as a gift.</strong></p></td>
                                    </tr>
                                {/if}
                                {if $customized_order=='Y'}
                                    <tr height="20">
                                        <td><p  style="font-size:1.5em;color:#FF0000;background-color:#FFFFCC; text-align: center"><strong>This order has to be customized as per details.</strong></p></td>
                                    </tr>
                                {/if}
                                                            
                                <tr height="20" valign="middle">
                                    <td>
                                        {*
                                        {if $status eq 'Q' or $status eq 'WP' or ($status eq 'OH' and ($order.on_hold_reason_id_fk eq 27 or $order.on_hold_reason_id_fk eq 31 or $order.on_hold_reason_id_fk eq 32 or $order.on_hold_reason_id_fk eq 33 or $order.on_hold_reason_id_fk eq 34))}
                                            <input type="button" id="cancel-items-btn" value="Cancel Selected Items" disabled/> &nbsp;
                                            {if $order.ordertype neq 'ex'}
                                                <input type="button" id="split-order-btn" value="Split Order" disabled/> &nbsp;
                                            {/if}
                                            {if $order.shipping_method neq 'SDD'}
                                            <input type="button" id="assign-wh-btn" value="Re-Assign Warehouse" disabled/>&nbsp;
                                            {/if}
                                            <input type="button" id="price-ovrrd-btn" value="Price Override" disabled/>&nbsp;
                                        {else}
                                            <input type="button" id="cancel-items-btn" value="Cancel Selected Items" disabled/>&nbsp;
                                            {if $order.ordertype neq 'ex'}
                                                <input type="button" id="split-order-btn" value="Split Order" disabled/> &nbsp;
                                            {/if}
                                            {if $order.shipping_method neq 'SDD'}
                                            <input type="button" id="assign-wh-btn" value="Re-Assign Warehouse" disabled/>&nbsp;
                                            {/if}
                                            <input type="button" id="price-ovrrd-btn" value="Price Override" disabled/>&nbsp;
                                        {/if}
                                        *}
                                        {if $status eq 'DL' or $status eq 'C'}
                                            {if $returnable eq 'true'}
                                                {*<input type="button" id="return-items-btn" value="Return Items" />*}
                                                {if $exchangesEnabled}
                                                    <input type="button" id="exchange-items-btn" value="Exchange Items" />
                                                {/if}
                                            {else}
                                                {*<input type="button" id="return-items-btn" value="Return Items" disabled title="Cannot Return as order has been delivered before 30 days"/>*}
                                                <input type="button" id="exchange-items-btn" value="Exchange Items" disabled title="Cannot create an Exchange as order has been delivered before 30 days"/> 
                                            {/if}
                                        {else}
                                            {*<input type="button" id="return-items-btn" value="Return Items" disabled/>*}
                                            <input type="button" id="exchange-items-btn" value="Exchange Items" disabled/>
                                        {/if}
                                        <input type="hidden" name="warehouseid" id="warehouseid" value="{$order.warehouseid}">
                                    </td>
                                </tr>
                            </table>                        {/if}
                        <table width="100%"  border="0" cellspacing="0" cellpadding="0">
                            {assign var=productsInCart value=$shipment.items}
                            {section name=idx loop=$productsInCart}
                                {if $productsInCart[idx].returnid != '' && $productsInCart[idx].exchange_orderid == ''}
                                    <tr>
                                        <td colspan=5 style="padding-top:25px;padding-bottom:3px;font-weight:bold;color:red"> Return <a href="/admin/returns/return_request.php?id={$productsInCart[idx].returnid}" target="_blank">{$productsInCart[idx].returnid}</a> has been filed on this item  </td>
                                    </tr>
                                {/if}
                                {if $productsInCart[idx].exchange_orderid != ''}
                                    <tr>
                                        <td colspan=5 style="padding-top:25px;padding-bottom:3px;font-weight:bold;color:red"> Exchange <a href="/admin/order.php?orderid={$productsInCart[idx].exchange_orderid}" target="_blank">{$productsInCart[idx].exchange_orderid}</a> 
                                            {if $productsInCart[idx].returnid != ''} 
                                                and Return <a href="/admin/returns/return_request.php?id={$productsInCart[idx].returnid}" target="_blank">{$productsInCart[idx].returnid}</a> have 
                                            {else} 
                                                has 
                                            {/if} 
                                            been created for this item  </td>
                                    </tr>
                                {/if}
                                {if $productsInCart[idx].is_returnable == 0}
                                    <tr>
                                        <td colspan=5 style="padding-top:25px;padding-bottom:3px;color:red"> Points have been activated by the customer for this item </td>
                                    </tr>
                                {/if}
                                <tr valign="middle" {if $productsInCart[idx].is_customizable eq '0'}style="background-color:#ffbb00"{/if}>
                                    <td width="20">
                                        {if $shipment.orderid eq $orderid}
                                            {if $status eq 'Q' or $status eq 'WP'}  
                                                <input type=checkbox name="item" value="{$productsInCart[idx].itemid}" data-pickup-excluded="{$productsInCart[idx].pickup_excluded}"/>
                                            {else}
                                                <input type=checkbox name="item" value="{$productsInCart[idx].itemid}" {if $productsInCart[idx].returnid != '' || $productsInCart[idx].exchange_orderid != '' || $productsInCart[idx].returnable eq 'false'} disabled = true {/if} data-pickup-excluded="{$productsInCart[idx].pickup_excluded}"/>
                                            {/if}
                                        {else}&nbsp;{/if}
                                    </td>
                                    {if $productsInCart[idx].designImagePath != ""}
                                        <td width="130" align="center">
                                            <img src="{$productsInCart[idx].designImagePath}"  style="border:1px solid #CCCCCC;"><br><br>
                                            <a href="javascript:void(0);" onClick="javascript:window.open('{$productsInCart[idx].default_image}','', 'width=1000,height=800');">View Large Image</a>
                                        </td>
                                    {else}
                                        <td width="130" align="center">
                                            <img  src="{$xcart_web_dir}/image.php?type={$type}&amp;id={$id}&amp;ts={$smarty.now}" width="100"  height="70" hspace="5" vspace="5" />
                                        </td>
                                    {/if}
                                    <td width=35%>
                                        <table cellspacing=0 cellpadding=0 border=0 width=100%>
                                            <tr height=20>
                                                <td width=30%>Style:</td>
                                                <td>{$productsInCart[idx].productStyleLabel}</td>
                                            </tr>
                                            <tr height=20>
                                                <td width=30%>Article Id:</td>
                                                <td>{$productsInCart[idx].article_number}</td>
                                            </tr>
                                            <tr height=20>
                                                <td width=30%>Size-Quantity:</td>
                                                <td>{$productsInCart[idx].size}:{$productsInCart[idx].amount}</td>
                                            </tr>
                                            <tr height=20>
                                                <td width=30%>Sku Code:</td>
                                                <td>{$productsInCart[idx].sku_code}</td>
                                            </tr>
                                            <tr height=20>
                                                <td width=30%>Style Id:</td>
                                                <td>{$productsInCart[idx].style_id}</td>
                                            </tr>
                                            <tr height=20>
                                                <td width=30%>Product Tags :</td>
                                                <td>{$productsInCart[idx].tags}</td>
                                            </tr>
                                            {if $productsInCart[idx].productName ne ''}
                                                <tr height=20>
                                                    <td width=30%><b>Design Name :</b></td>
                                                    <td><span style='font-size:12px;font-weight:bold'> {$productsInCart[idx].productName}</span></td>
                                                </tr>
                                            {/if}
                                            {if $productsInCart[idx].yahootpl ne ''}
                                                <tr height=20>
                                                    <td width=30% style="font-size:1.0em;font-weight:bold;color:#FF0033;">Yahoo template :</td>
                                                    <td>{$productsInCart[idx].yahootpl}</td>
                                                </tr>
                                            {/if}
                                            {if $productsInCart[idx].promotion ne ''}
                                                <tr height=20>
                                                    <td width=30% style="font-size:1.0em;font-weight:bold;color:#FF0033;">Promotion template :</td>
                                                    <td>{$productsInCart[idx].promotion}</td>
                                                </tr>
                                            {/if}
                                            {if $productsInCart[idx].item_cashback ne 0.00}
                                                <tr height=20>
                                                    <td width=30%>Cashback Credited :</td>
                                                    <td>{$productsInCart[idx].item_cashback}</td>
                                                </tr>
                                            {/if}
                                            <tr height=20>
                                                <td colspan=2>
                                                    {if $productsInCart[idx].is_customizable eq 1}
                                                        <a href="view_print_design.php?productId={$productsInCart[idx].productid}&orderid={$orderid}">View & Print Designs</a> &nbsp; &nbsp;&nbsp;&nbsp;
                                                    {/if}
                                                    {if $status eq 'WP' && $productsInCart[idx].item_status neq 'A' }
                                                        <b>Item processed. Current Status {$productsInCart[idx].item_status}</b>
                                                    {/if}
                                                </td>
                                            </tr>
                                        </table> 
                                    </td>                                   
                                    <td>
                                        {if $productsInCart[idx].extra_data ne ''}
                                            {assign var="fifa_product" value=$productsInCart[idx]}
                                            {assign var="fifa_data" value=$fifa_product.extra_data|html_entity_decode}
                                            {assign var="fifa_json" value=$fifa_data|json_decode:1}
                                            <table cellspacing=0 cellpadding=0 border=0 width=100% style="background-color:yellow;">
                                                <tr>
                                                    <td><b> Position </b></td>
                                                    <td><b> Type </b></td>
                                                    <td><b> Color </b></td>
                                                    <td><b> Value </b></td>
                                                </tr>
                                               {foreach from=$fifa_json key=name item=nvalue}
                                                    {assign var=position value=$name}
                                                    {if $name eq 'le'}
                                                        {assign var=position value='left exterior'}
                                                    {/if}
                                                    {if $name eq 'li'}
                                                        {assign var=position value='left interior'}
                                                    {/if}
                                                    {if $name eq 're'}
                                                        {assign var=position value='right exterior'}
                                                    {/if}
                                                    {if $name eq 'ri'}
                                                        {assign var=position value='right interior'}
                                                    {/if}
                                                    {if $name eq 'ba'}
                                                        {assign var=position value='back name'}
                                                    {/if}
                                                    {if $name eq 'bu'}
                                                        {assign var=position value='back number'}
                                                    {/if}

                                                    {assign var=itemType value='-'}
                                                    {assign var=jsnItem value=$nvalue.t}
                                                    {assign var=jsnColor value=$nvalue.c}
                                                    {assign var=jsnValue value=$nvalue.v}
                                                     {if $jsnItem eq 'f'}
                                                         {assign var=itemType value='flag'}
                                                    {/if}
                                                    {if $jsnItem eq 'a'}
                                                        {assign var=itemType value='name'}
                                                    {/if}
                                                    {if $jsnItem eq 'u'}
                                                        {assign var=itemType value='number'}
                                                    {/if}
                                                    <tr>
                                                        <td>{$position}</td>
                                                        <td>{$itemType}</td>
                                                        <td bgcolor="{$jsnColor}">{$jsnColor}</td>
                                                        <td>{$jsnValue}</td>
                                                    </tr>

                                                {/foreach}
                                            </table>
                                         {/if}
                                    </td>
                                    <td height="21" align="right">
                                        <table border="0" cellpadding="0" cellspacing="0" class="textBlack">
                                            <tr>
                                                <td><b>Price:</b></td>
                                                <td align="right">
                                                    {$productsInCart[idx].price}&nbsp;x&nbsp;
                                                    <input id="{$productId}qty"  size="5" name="{$productId}qty" type="text" value={$productsInCart[idx].amount}  readonly>
                                                    =<b>Rs.{$productsInCart[idx].totalPrice}</b>
                                                </td>
                                            </tr>                                            
                                            {if $productsInCart[idx].productDiscount != 0.00 }
                                                <tr>
                                                    <td><b>Item Discount:</b></td>
                                                    <td align="right"><b>Rs.{$productsInCart[idx].productDiscount}</b></td>
                                                </tr>
                                            {/if}
                                            {if $productsInCart[idx].cartDiscount != 0.00 }
                                                <tr>
                                                    <td><b>Cart Discount:</b></td>
                                                    <td align="right"><b>Rs.{$productsInCart[idx].cartDiscount}</b></td>
                                                </tr>
                                            {/if}
                                            {if $productsInCart[idx].item_coupon_discount != 0.00 }
                                                <tr>
                                                    <td><b>Coupon Discount:</b></td>
                                                    <td align="right"><b>Rs.{$productsInCart[idx].item_coupon_discount}</b></td>
                                                </tr>
                                            {/if}
                                            {if $productsInCart[idx].item_pg_discount != 0.00 }
                                                <tr>
                                                    <td><b>PG Discount:</b></td>
                                                    <td align="right"><b>Rs.{$productsInCart[idx].item_pg_discount}</b></td>
                                                </tr>
                                            {/if}
                                            {if $productsInCart[idx].item_cashdiscount != 0.00 }
                                                <tr>
                                                    <td><b>Cash Redeemed:</b></td>
                                                    <td align="right"><b>Rs.{$productsInCart[idx].item_cashdiscount}</b></td>
                                                </tr>
                                            {/if}
                                            {if $productsInCart[idx].item_loyalty_points_used != 0.00 }
                                                <tr>
                                                    <td><b>Loyalty Credits:</b></td>
                                                    <td align="right"><b>({$productsInCart[idx].item_loyalty_points_used}*{$productsInCart[idx].loyalty_points_conversion_factor}) = Rs.{$productsInCart[idx].loyalty_credits}</b></td>
                                                </tr>
                                            {/if}
                                            {if $productsInCart[idx].taxamount != 0.00 }
                                                <tr>
                                                    <td><b>Vat Amount:</b></td>
                                                    <td align="right"><b>Rs.{$productsInCart[idx].taxamount}</b></td>
                                                </tr>
                                            {/if}
                                            {if $productsInCart[idx].gift_card_amount != 0.00 }
                                                <tr>
                                                    <td><b>Gift Card Amount:</b></td>
                                                    <td align="right"><b>Rs.{$productsInCart[idx].gift_card_amount}</b></td>
                                                </tr>
                                            {/if}
                                            <tr>
                                                <td><b>Final Amount Paid:</b></td>
                                                <td align="right"><b>Rs.{$productsInCart[idx].amount_paid}</b></td>
                                            </tr>
                                            {if $productsInCart[idx].difference_refund != 0.00 }
                                                <tr>
                                                    <td><b>Price Mismatch Refund </b> </td>
                                                    <td align="right"><b>Rs.{$productsInCart[idx].difference_refund}</b></td>
                                                </tr>
                                            {/if}
                                        </table>
                                    </td>
                                </tr>
                            {/section}
                            {assign var=i value=0}
                            {foreach name=outer item=gift from=$giftcerts}
                                 {assign var=i value=$i+1}
                            {/foreach}
                        </table>
                        {if $shipment.orderid eq $orderid }
                            <table border="0" cellpadding="0" cellspacing="0" class="textBlack right">
                                <tr bgcolor="#f1f1f1">
                                    <td height="21" align="left"><strong>&nbsp;Total Amount:&nbsp;
                                        </strong>
                                    </td>
                                    <td height="21" align="left"><strong>&nbsp;Rs.{$grandTotal}
                                        &nbsp; </strong>
                                    </td>
                                </tr>
                                <tr bgcolor="#f1f1f1">
                                    <td height="21" align="left">
                                        <strong>&nbsp;Special offer item discount:&nbsp; </strong>
                                    </td>
                                    <td align="right" class="textGreen">
                                        <strong>Rs.{$specialofferdiscount}</strong>
                                    </td>
                                </tr>
                                <tr bgcolor="#f1f1f1">
                                    <td height="21" align="left">
                                        <strong>&nbsp;Special offer bag discount:&nbsp; </strong>
                                    </td>
                                    <td align="right" class="textGreen">
                                        <strong>Rs.{$cartDiscount}</strong>
                                    </td>
                                </tr>
                                <tr bgcolor="#f1f1f1">
                                    <td height="21" align="left">
                                        <strong>&nbsp;Coupon discount(Coupon code:{$couponCode}):&nbsp;
                                        </strong></td>
                                    <td align="right" class="textGreen"><strong>Rs.{$coupondiscount}
                                        </strong>
                                </td>
                                </tr>
                                {if $cashdiscount gt 0}
                                <tr bgcolor="#f1f1f1">
                                    <td height="21" align="left">
                                        <strong>&nbsp;Mynt Credits:&nbsp;
                                        </strong></td>
                                    <td align="right" class="textGreen"><strong>Rs.{$cashdiscount}
                                        </strong>
                                </td>
                                </tr>
                                {/if}
                                {if $loyalty_credits gt 0}
                                <tr bgcolor="#f1f1f1">
                                    <td height="21" align="left">
                                        <strong>&nbsp;Loyalty Credit:&nbsp;
                                        </strong></td>
                                    <td align="right" class="textGreen"><strong>Rs.{$loyalty_credits}
                                        </strong>
                                    </td>
                                </tr>
                                {/if}
                                {if $pg_discount gt 0}
                                <tr bgcolor="#f1f1f1">
                                    <td height="21" align="left">
                                        <strong>&nbsp;Payments Discount:&nbsp;
                                        </strong></td>
                                    <td align="right" class="textGreen"><strong>Rs.{$pg_discount}
                                        </strong>
                                </td>
                                </tr>
                                {/if}
                                <tr bgcolor="#f1f1f1">
                                    <td height="21" align="left">
                                        <strong>&nbsp;Shipping Cost:&nbsp; </strong>
                                    </td>
                                    <td align="right" class="textGreen">
                                        <strong>Rs.{$shippingcost}</strong>
                                    </td>
                                </tr>
                                <tr bgcolor="#f1f1f1">
                                    <td height="21" align="left">
                                        <strong>&nbsp;EMI Charges:&nbsp; </strong>
                                    </td>
                                    <td align="right" class="textGreen">
                                        <strong>Rs.{$emi_charge}</strong>
                                    </td>
                                </tr>
                                {if $payment_method =='cod'}
                                <tr bgcolor="#f1f1f1">
                                    <td height="21" align="left">
                                        <strong>&nbsp;Cash on delivery:&nbsp; </strong>
                                    </td>
                                    <td align="right" class="textGreen">
                                        <strong>Rs.{$cod}</strong>
                                    </td>
                                </tr>
                                {/if}
                            
                                {if $gift_status=='Y'}
                                <tr bgcolor="#f1f1f1">
                                    <td height="21" align="left">
                                        <strong>&nbsp;Gift packaging charges:&nbsp; </strong>
                                    </td>
                                    <td align="right" class="textGreen">
                                        <strong>Rs.{$gift_charges}</strong>
                                    </td>
                                </tr>
                                {/if}
                                
                                <tr bgcolor="#f1f1f1">
                                    <td height="21" align="left">
                                        <strong>&nbsp;VAT Amount:&nbsp; </strong>
                                    </td>
                                    <td align="right" class="textGreen">
                                        <strong>Rs.{$vat}</strong>
                                    </td>
                                </tr>

                                {if $gift_card_amount != ''}
                                <tr bgcolor="#f1f1f1">
                                    <td height="21" align="left">
                                        <strong>&nbsp;Gift Card Amount:&nbsp; </strong>
                                    </td>
                                    <td align="right" class="textGreen">
                                        <strong>Rs.{$gift_card_amount}</strong>
                                    </td>
                                </tr>
                                {/if}

                                <tr bgcolor="#f1f1f1">
                                    <td height="21" align="left">
                                        <strong>&nbsp;Amount to be paid:&nbsp; </strong>
                                    </td>
                                    <td align="right" class="textGreen">
                                            <strong>Rs.{$amounttobepaid}</strong>
                                    </td>
                                </tr>
                            
                                {if $ref_discount > 0}
                                <tr bgcolor="#f1f1f1">
                                    <td height="21" align="left"><strong>&nbsp;Referal discount&nbsp; </strong></td>
                                    <td align="right" class="textGreen"><strong>&nbsp;Rs.{$ref_discount}</strong></td>
                                </tr>
                                <tr bgcolor="#f1f1f1">
                                    <td height="21" align="left"><strong>Amount to be paid after referral discount&nbsp; </strong></td>
                                    <td align="right" class="textGreen"><strong>&nbsp;Rs.{$amtAfterRefDeducton}</strong></td>
                                </tr>
                                {/if}
                                {if $order_cashback > 0}
                                <tr bgcolor="#f1f1f1">
                                    <td height="21" align="left">
                                        <strong>&nbsp;Cashback Credited:&nbsp; </strong>
                                    </td>
                                    <td align="right" class="textGreen">
                                        <strong>Rs.{$order_cashback}</strong>
                                    </td>
                                </tr>
                                {/if}
                            </table>
                        {/if}
                        <table height="25" width="100%" border="0" cellpadding="0" cellspacing="0"  valign="bottom">
                            <tr>
                                <td><a href="javascript:void(0);" onClick="javascript:window.open('/admin/cancelled_items.php?orderid={$shipment.orderid}','Cancelled Items', 'menubar=1,resizable=1,toolbar=1,width=800,height=500');"><b>View Cancelled Items</b></a></td>
                            </tr>
                        </table>
                    </div>
                {/foreach}
            {/if}
        </div>
    </div>
</div>

