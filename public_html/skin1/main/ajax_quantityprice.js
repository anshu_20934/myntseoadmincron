// JavaScript Document
/* Define url for redirecting to server site page*/

var URL= "ajax_quantityprice.php?pid=";


/*
 * Function Name: getHTTPObject()
 * Purpose: Instantiate object of XMLHttpRequest().
 * Summary: Beauty of this function is, this function instantiate browser independent
 * object of XMLHttpRequest()
 *
*/
function getHTTPObject() {
      var xmlhttp;
      /*@cc_on
          @if (@_jscript_version >= 5)
            try {
          xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
            } catch (e) {
          try {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
          } catch (E) {
            xmlhttp = false;
          }
        }
          @else
          xmlhttp = false;
          @end @*/
          if (!xmlhttp && typeof XMLHttpRequest != 'undefined') {
            try {
          xmlhttp = new XMLHttpRequest();
    } catch (e) {
          xmlhttp = false;
    }
  }
          return xmlhttp;
}
var http = new getHTTPObject();

/* 
 * Function Name: saveAjax
 * Parameter: String of name/value pair of form elements (strSubmit).
 * Purpose: 
 * Summary: This function retrive the name/value pair of form elements and concatenate 
 * them into a single string. 
 * Note: Redio button and Checkbox cases is yet to implement.  
 *     
*/


function callForQuantity(pid)
{
   
    var id = document.getElementById(pid).value;
    http.open("GET", URL + escape(id) + "&action=reterive", true);
    http.onreadystatechange = myGetFunction;
    http.send(null);    
}

function myGetFunction()
{
    if(http.readyState==4)
	{
            var result=http.responseText;
            if(result=='failed')
            {
                  alert("Product Id is not valid. Please try again");
                  document.getElementById("productid").focus();
            }
            else
            {
                  //var result=http.responseText;
                  var returnResult = result.split(",")
                  var pname = returnResult[0];
                  var listprice = returnResult[1];
                  var styleid = returnResult[2];
                  var typeid = returnResult[3];
                  var provider = returnResult[4];
				  var option = returnResult[5];
        
                  document.getElementById("pname").value = pname;
                  document.getElementById("price").value = listprice;
                  document.getElementById("styleid").value = styleid;
                  document.getElementById("typeid").value = typeid;
                  document.getElementById("provider").value = provider;
				  document.getElementById("typeoptions").innerHTML = option;
            }
	}
}

function proDelete(id,cid,pname)
{
	$flag = confirm("Do you really want to delete product "+pname);
	if($flag == true)
	{
		window.location="add_offlineproduct.php?custid="+cid+"&pid="+id+"&action=delete";
	}
	else
	{
		return false;
	}
}
/* 
 * Function Name: formData2QueryString
 * Parameter: form name(ajaxForm) 
 * Purpose: Concatenate the name/value pair of form elements 
 * Summary: This function retrive the name/value pair of form elements and concatenate 
 * them into a single string. 
 * Note: Redio button and Checkbox cases is yet to implement.  
 *     
*/

function proUpdate(pid,cid)
{
    http.open("GET", URL + escape(pid) + "&cid="+cid+"&action=update", true);
    http.onreadystatechange = myUpdateFunction;
    http.send(null);
}
 


/* 
 * Function Name: saveAjax
 * Parameter: String of name/value pair of form elements (strSubmit).
 * Purpose: 
 * Summary: This function retrive the name/value pair of form elements and concatenate 
 * them into a single string. 
 * Note: Redio button and Checkbox cases is yet to implement.  
 *     
*/

function myUpdateFunction()
{
      if(http.readyState==4)
      {
            var result=http.responseText;
      
            var returnResult = result.split(",")
            var productid = returnResult[0];
            var pname = returnResult[1];
            var quantity = returnResult[2];
            var notes = returnResult[3];
            var p_style_id = returnResult[4];
            var p_type_id = returnResult[5];
            var provider = returnResult[6];
            var price = returnResult[7];
            var custid = returnResult[8];
            var id = returnResult[9];
			var option = returnResult[10];
			
            document.getElementById("productid").value = productid;
            document.getElementById("pname").value = pname;
            document.getElementById("quantity").value = quantity;
            document.getElementById("notes").value = notes;
            document.getElementById("price").value = price;
            document.getElementById("styleid").value = p_style_id;
            document.getElementById("typeid").value = p_type_id;
            document.getElementById("custid").value = custid;
            document.getElementById("provider").value = provider;
            document.getElementById("updatevalues").value = "updatevalues";
            document.getElementById("autoid").value = id;
			document.getElementById("typeoptions").innerHTML = option;
           
      }
}

function calculateDiscount(distype)
{
      var discount = document.getElementById("discount").value;
      var totamount = document.getElementById("totamount").value;
      //var distype = document.getElementById("distype").value;
      
      if(distype == "ABS"){
            var dis_amount = discount;
            var discountedamount = totamount - discount;
      }
      if(distype == "PST"){      
            var dis_amount = ((totamount * discount)/100);
            var discountedamount = (totamount - ((totamount * discount)/100));
      }
      document.getElementById("disamount").value = dis_amount;
      document.getElementById("discountedamount").value = discountedamount;
}
function calculateTax(tax)
{
      if(tax != 'grd')
      {
      var discountedamount = document.getElementById("discountedamount").value;
      if(tax == "4"){
            var taxamount = ((discountedamount * 4)/100) ;
            var discountedamount = (parseFloat(discountedamount) + parseFloat(taxamount));
      }
      if(tax == "12"){      
            var taxamount = ((discountedamount * 12.36)/100);
            var discountedamount = (parseFloat(discountedamount) + parseFloat(taxamount));
      }
      document.getElementById("aftertax").value = discountedamount;
      document.getElementById("tottax").value = taxamount;
      }
      else
      {
            var aftertax = parseFloat(document.getElementById("aftertax").value);
            var shiprate = parseFloat(document.getElementById("shiprate").value);
            var grdTotal = (aftertax+shiprate);
            document.getElementById("gtotal").value = grdTotal;
      }
}

function placedOrder(cid)
{
	$flag = confirm("Please check your order detail before placing it permanently. Click OK if order is right else click Cancel to edit your order.");
	if($flag == true)
	{
      document.productform.action = "add_offlineproduct.php?email="+cid+"&action=order"
      document.productform.submit();
	  return $flag;
	}
	else
	{
		return $flag;
	}
     // window.close();
}

function validateProductForm(ajaxForm)
{
	for (i = 0; i < ajaxForm.elements.length; i++) {
    formElem = ajaxForm.elements[i]; 

	 switch (formElem.type) {
    // Text, select, hidden, password, textarea elements
    case 'text':
				var textname = formElem.name;
				if(textname == "productid")
				{
					if(formElem.value == ""){
						alert("Please input Product Id");
						document.getElementById(textname).focus();
						return false;
					}
				}
				if(textname == "quantity")
				{
					if(formElem.value == ""){
						alert("Please input Product quantity");
						document.getElementById(textname).focus();
						return false;
					}
				}    
    break;
  }

		/*if (formElem.tagName == "SELECT")
		{
			var selectname = formElem.name;
			if(formElem.options[formElem.selectedIndex].value == "0")
			{
				alert("Please select the product "+selectname);
				document.getElementById(selectname).focus();
				return false;

			}
		}*/

	}
}









