{* $Id: history_order.tpl,v 1.83.2.6 2006/06/27 08:08:06 max Exp $ *}
{include file="page_title.tpl" title=$lng.lbl_order_details_label}

<script type="text/javascript">
    var orderId = "{$orderid}";
</script>
{include file="main/include_js.tpl" src="/main/history_order.js"}
{include file="main/include_js.tpl" src="/admin/main/order_details.js"}

{capture name=dialog}
{if $action eq "confirm"}
	Select the no of pieces to be deleted for follwing items:
	<div id="msgBar" style="text-align:center; font-weight: bold; color: red;">{$errorMsg}</div>
	{if $usertype ne "C"}
		<form id="cancel-items-form" action="" method="POST">
			{assign var=allowCancellation value=true}
			<table width="100%"  border="0" cellspacing="0" cellpadding="0">
				<thead style="background-color: #DFDFDF;">
					<th align=left style="padding: 10px 5px;">Product Image</th>
					<th align=left style="padding: 10px 5px;">Product</th>
					<th align=left style="padding: 10px 5px;">Style</th>
					<th align=left style="padding: 10px 5px;">Quantity</th>
					<th align=center style="padding: 10px 5px;">Price</th>
					<th align=left style="padding: 10px 5px;" width=20>&nbsp;</th>
				</thead>
				{*foreach name=outer item=product from=$productsInCart*}
			    {section name=idx loop=$productsInCart}
					<tr height="75">
						<td>
							{if $productsInCart[idx].designImagePath != ""}
								<img src="{$productsInCart[idx].designImagePath}"  style="border:1px solid #CCCCCC;">
							{else}
								<img  src="{$xcart_web_dir}/image.php?type={$type}&amp;id={$id}&amp;ts={$smarty.now}" width="100"  height="70" hspace="5" vspace="5" />
							{/if}
						</td>
						<td>{$productsInCart[idx].productTypeLabel} </td>
						<td>{$productsInCart[idx].productStyleLabel} (Article Id: {$productsInCart[idx].article_number})</td>
						{if $productsInCart[idx].item_status == 'A' || $productsInCart[idx].item_status == 'RP'}
							<td>
								<select name="item_quantity_{$productsInCart[idx].itemid}" style="width:40px;">
									{section name=quantity loop=$productsInCart[idx].amount }
	  									<option value="{$smarty.section.quantity.iteration}">{$smarty.section.quantity.iteration}</option>
									{/section}
								</select>
								<br/>
							</td>
							<td align=center>{$productsInCart[idx].price}</td>
						{else}
							{assign var=allowCancellation value=false}
							<td/>
							<td style="color:red;">Cancellation not allowed post QA done.</td>
						{/if}
						<td>&nbsp;</td>
					</tr>
					<tr height=20><td colspan=5><hr/></td></tr>
				{/section}
				{*/foreach*}
				{if $allowCancellation eq true}
					<tr>
						<td colspan=2 valign=top>
	                        <b>Cancellation Type <em style="color:red;">*</em>:</b>
	                        <select name="cancellation_type" id="cancellation_type" onchange="showReasonsOnrejected();">
	                            <option value="">--Select Cancellation Type--</option>
	                            {foreach from=$cancellation_types key=typeKey item=typeValue}
	                                <option value="{$typeKey}">{$typeValue}</option>
	                            {/foreach}
	                        </select>
							<br/>
							<b>Reason: <em style="color:red;">*</em>:</b>
							{foreach from=$cancellation_types key=typeKey item=typeValue}
	                            <select name="cancellation_reason" id="cancellation_reason_{$typeKey}" class="cancel_reason" style="display:none;">
	                                {foreach from=$cancellation_reasons.$typeKey key=reasonKey item=reasonValue}
	                                    <option value="{$reasonKey}">{$reasonValue}</option>
	                                {/foreach}
	                            </select>
	                        {/foreach}
						</td>
						<td colspan=2>
							<label>Remarks (<em style="color:red;">*</em>):</label><br/>
							<textarea id="cancel_comment" name="cancel_comment" style="width: 300px; height: 50px;"></textarea>
						</td>
						<td align=right valign=top><input type=checkbox name="notify_customer_email" value="true" checked> Notify Cancellation to User via e-mail</td>
					</tr>
				{/if}
			</table>
			<br/>
			<input type=hidden name=action value="cancel" />
			<input type=hidden name=itemids value="{$itemids}" />
			<input type=hidden name=orderid value="{$orderid}" />
			<input type=hidden name=login value="{$login}" />
			<div style="width:100%; text-align:right; padding:5px;">
				{if $allowCancellation eq true}
					<input type=submit name="cancel-items-yes" id="cancel-items-yes" value="Cancel Items" style="padding:5px 10px;"/>
				{else}
					<div style="color:red; text-align:center">Please remove the non-cancelable items from selection.</div>
					<input type=button name="return-btn" id="return-btn" value="Close" style="padding:5px 10px;" onclick="opener.window.location.reload(); self.close();"/>
				{/if}
			</div>
		</form>
	{/if}
	
	{/capture}
	{include file="dialog.tpl" title=$lng.lbl_products_details content=$smarty.capture.dialog extra='width="100%"'}
{else}
	{if $errorMsg != ''}
		<p style="color:red">{$errorMsg} </p>
	{/if}
	{if $successMsg != ''}
		<p style="color:green">{$successMsg}</p>	
	{/if}
	<p>Click on close button to refresh data on back page</p>
	<input type=button name="return-btn" id="return-btn" value="Close" style="padding:5px 10px;" onclick="opener.window.location.reload(); self.close();"/>
{/if}