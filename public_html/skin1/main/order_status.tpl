{* $Id: order_status.tpl,v 1.7 2005/11/30 13:29:35 max Exp $ *}
{if $extended eq "" and $status eq ""}
    {$lng.lbl_wrong_status}
{elseif $mode eq "select"}
	<select name="{$name}" id="{$name}" {$extra} {if $action eq 1}onchange="javascript:showOrHideCommentBox(this.value);"{/if} disabled="disabled">
		{if $extended ne ""}<option value="">--Select order status--</option>{/if}
		<option value="Q"{if $status eq "Q"} selected="selected"{/if}>{$lng.lbl_queued}</option>
		<option value="WP"{if $status eq "WP"} selected="selected"{/if}>{$lng.lbl_wip}</option>
		<option value="D"{if $status eq "D"} selected="selected"{/if}>{$lng.lbl_declined}</option>
		<option value="F"{if $status eq "F"} selected="selected"{/if}>{$lng.lbl_failed}</option>
		<option value="C"{if $status eq "C"} selected="selected"{/if}>{$lng.lbl_complete}</option>
		<option value="PP"{if $status eq "PP"} selected="selected"{/if}>{$lng.lbl_preprocessed}</option>
		<option value="PV"{if $status eq "PV"} selected="selected"{/if}>{$lng.lbl_cod}</option>
		<option value="OH"{if $status eq "OH"} selected="selected"{/if}>{$lng.lbl_onhold}</option>
		<option value="DL"{if $status eq "DL"} selected="selected"{/if}>Delivered</option>
		<option value="FD"{if $status eq "FD"} selected="selected"{/if}>{$lng.lbl_delivery_failed}</option>
		<option value="PK"{if $status eq "PK"} selected="selected"{/if}>Packed</option>
		<option value="SH"{if $status eq "SH"} selected="selected"{/if}>{$lng.lbl_order_shipped}</option>
		<option value="L"{if $status eq "L"} selected="selected"{/if}>Lost</option>
		<option value="RFR"{if $status eq "RFR"} selected="selected"{/if}>Ready For Release</option>
        <option value="RTO"{if $status eq "RTO"} selected="selected"{/if}>RTO</option>
	</select>
{elseif $mode eq "static"}
	{if $status eq "I"}{$lng.lbl_not_finished}
	{elseif $status eq "Q"}{$lng.lbl_queued}
	{elseif $status eq "P"}{$lng.lbl_processed}
	{elseif $status eq "D"}{$lng.lbl_declined}
	{elseif $status eq "OH"}{$lng.lbl_onhold}
	{elseif $status eq "B"}{$lng.lbl_backordered}
	{elseif $status eq "F"}{$lng.lbl_failed}
	{elseif $status eq "C"}{$lng.lbl_complete}
	{elseif $status eq "PD"}{$lng.lbl_pending}
	{elseif $status eq "PP"}{$lng.lbl_preprocessed}
	{elseif $status eq "CD"}{$lng.lbl_cod}
	{elseif $status eq "FD"}{$lng.lbl_delivery_failed}
	{elseif $status eq "WP"}{$lng.lbl_wip}
	{elseif $status eq "PV"}{$lng.lbl_cod}
	{elseif $status eq "PK"}{$lng.lbl_order_packed}
	{elseif $status eq "SH"}{$lng.lbl_order_shipped}
	{elseif $status eq "L"}Lost
	{elseif $status eq "RFR"}Ready For Release
	{elseif $status eq "DL"}Delivered
    {elseif $status eq "RTO"}RTO
    {/if}
{elseif $mode eq "oos-select"}
    <select name="{$name}" id="{$name}" {$extra} {if $action eq 1}onchange="javascript:showOrHideCommentBox(this.value);"{/if} disabled>
        <option value="OH"{if $status eq "OH"} selected="selected"{/if}>{$lng.lbl_onhold}</option>
        <option value="F"{if $status eq "F"} selected="selected"{/if}>{$lng.lbl_failed}</option>
    </select>
{/if}