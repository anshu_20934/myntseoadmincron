{include file="../skin1/main/include_js.tpl" src="../skin1/main/calendar2.js"}
<script type="text/javascript">
	var orderId = '{$order.orderid}';
</script>
<!--<link rel="stylesheet" type="text/css" href="{$http_location}/admin/extjs/resources/css/ext-all.css"/>
<script type="text/javascript" src="{$http_location}/admin/extjs/adapter/ext/ext-base.js"></script>
<script type="text/javascript" src="{$http_location}/admin/extjs/ext-all.js"></script>
<script type="text/javascript" src="{$http_location}/admin/operations/order_replacement.js"></script> -->
<hr/>

<table cellspacing="0" cellpadding="0" width="100%">
    {if $products[prod_num].deleted eq "" and ($active_modules.Product_Configurator eq "" or $products[prod_num].extra_data.pconf.parent eq "")}
        <tr>
            <td colspan="2">
                {if $statusErrorMsg}
                    <div id="error_div" style="color:red; font-size: 120%; font-weight: bold; text-align:center;">{$statusErrorMsg}</div>
                {/if}
                {if $statusSuccessMsg}
                    <div id="success_div" style="color:green; font-size: 120%; font-weight: bold; text-align:center;">{$statusSuccessMsg}</div>
                {/if}
            </td>
        </tr>
        <tr>
            <td>
                <p style="font-size:1.5em;color:#FF0033;">
                    <input type="hidden" id="payby" name="payby" value="{$order.payment_option}"></span>
                    <strong>Payment method is
                        {if $order.payment_method eq "chq"}Check Payment
                        {elseif $order.payment_method eq "cod"}COD Payment
                        {elseif $order.payment_method eq "cash"}Cash Payment
                        {elseif $order.payment_method eq "ivr"}IVR
                        {else $order.payment_method eq "P"}Credit Card/ Net Banking
                        {/if}
                    </strong>
                </p>
            </td>
        </tr>

        <tr>
            <td>
                <div><b>Sender :</b> {$order.sender_name}</div>
                <div>&nbsp;</div>
                <div><b>Sender Email :</b> {$order.sender_email}</div>
                <div>&nbsp;</div>
                <div><b>Recipient :</b> {$order.recipient_name}</div>
                <div>&nbsp;</div>
                <div><b>Recipient Email :</b> {$order.recipient_email}</div>
                <div>&nbsp;</div>
                <div><b>Order Status :</b> {$order.gift_order_status_display}</div>
            </td>
            <td>
                <div><b>Gift Card Type :</b> {$order.gift_card_type}</div>
                <div>&nbsp;</div>
                <div><b>Gift Card Status :</b> {$order.gift_card_status_display}</div>
                <div>&nbsp;</div>
                <div><b>Gift Card Activated :</b> { if $order.gift_card_status == 3 } Yes {else} No {/if}</div>
                <div>&nbsp;</div>
                <div>
                    <table width=70% border=0>
                        <tr><td width=40%><b>Mrp Value:</b></td><td>Rs. {$order.sub_total}</td></tr>
                        {if $order.discount gt 0}
                            <tr><td width=40%><b>Discount:</b></td><td>- Rs. {$order.discount}</td></tr>
                        {/if}
                        {if $order.cart_discount gt 0}
                            <tr><td width=40%><b>Cart Discount:</b></td><td>- Rs. {$order.cart_discount}</td></tr>
                        {/if}
                        {if $order.pg_discount gt 0}
                            <tr><td width=40%><b>PG Discount:</b></td><td>- Rs. {$order.pg_discount}</td></tr>
                        {/if}
                        {if $order.cash_redeemed gt 0}
                            <tr><td width=40%><b>Cash Redeemed:</b></td><td>- Rs. {$order.cash_redeemed}</td></tr>
                        {/if}
                        {if $order.payment_surcharge gt 0}
                            <tr><td width=40%><b>EMI Charge:</b></td><td>+ Rs. {$order.payment_surcharge}</td></tr>
                        {/if}
                        {if $order.cod_charge gt 0}
                            <tr><td width=40%><b>COD Charge:</b></td><td>+ Rs. {$order.cod_charge}</td></tr>
                        {/if}
                        <tr><td colspan=2><hr/></td></tr>
                        <tr><td width=40%><b>Amount Paid:</b></td><td>Rs. {$order.total}</td></tr>
                    </table>
                </div>
            </td>
        </tr>
        <tr><td>&nbsp;</td></tr>
        <tr><td>&nbsp;</td></tr>
        <tr>
             <td colspan="2">
                {if $order.gift_order_status eq 1 && $order.gift_card_status neq 3 }
                    <input type="button" value="Send Gift Card Email" name="sendemail" onclick="showPopupOverlay1();"/>
                {/if}
                <!--
                {if $order.gift_order_status eq 0}
                    <input type="button" value="Complete Order" name="completeorder" onclick="updateGiftOrder('ordercomplete', {$order.gift_card_id}, 'adminuser={$login}');"/>
                {/if}
                -->
                {if $order.gift_card_status neq 3 && $order.gift_order_status eq 1}
                    <input type="button" value="Activate Card" name="activatecard" onclick="showPopupOverlay2()"/>
                {/if}
            </td>
        </tr>
    {/if}
</table>
<div id="giftstransactionpopup1" style="display:none;">
	<div class="overlay" style="display:block;position:absolute;width:1300px;height:1500px;">&nbsp;</div>
	<div id="giftstransactionpopup1-main" style="position:absolute; left:450; width:450px; background:white; z-index:1006;border:2px solid #999999;">
		<div id="giftstransactionpopup1-content" style="display:block;">
		    <div id="giftstransactionpopup1_title" class="popup_title" style="display:block;">
		    	<div id="giftstransactionpopup1_ajaxWindowTitle" class="popup_ajaxWindowTitle" style="text-align:center">Send Mail to Sender?</div>
				<div class="clear">&nbsp;</div>
			</div>
			<div style="padding:5px 25px; text-align:left; font-size:12px" >
				<div> 
				    <font face='verdana, arial, helvetica, san-serif' size='2'>
						<p><b>Comments</b></p>
						<input type="text" id="giftstransactionpopup1-comments" style="width:400px; height: 100px;">
						<div style="text-align:right">
							<input type="button" name="giftstransactionpopup1-okbtn" value="OK" style="width:80px; height: 30px;" onclick="addCommentsAndUpdateGiftOrder('sendemail', {$order.orderid}, {$order.gift_card_id}, 'is_admin=true&adminuser={$login}');">
							<input type="button" name="giftstransactionpopup1-cancelbtn" value="Cancel" style="width:80px; height: 30px;" onclick="hidePopupOverlay1();">
						</div>
					</font>
				</div>
			</div>
			<div class="clear">&nbsp;</div>
		</div>
	</div>
</div>
<div id="giftstransactionpopup2" style="display:none;">
	<div class="overlay" style="display:block;position:absolute;width:1300px;height:1500px;">&nbsp;</div>
	<div id="giftstransactionpopup2-main" style="position:absolute; left:450; width:450px; background:white; z-index:1006;border:2px solid #999999;">
		<div id="giftstransactionpopup2-content" style="display:block;">
		    <div id="giftstransactionpopup2_title" class="popup_title" style="display:block;">
		    	<div id="giftstransactionpopup2_ajaxWindowTitle" class="popup_ajaxWindowTitle" style="text-align:center">Activate Gift Card?</div>
				<div class="clear">&nbsp;</div>
			</div>
			<div style="padding:5px 25px; text-align:left; font-size:12px" >
				<div> 
				    <font face='verdana, arial, helvetica, san-serif' size='2'>
						<p><b>Comments</b></p>
						<input type="text" id="giftstransactionpopup2-comments" style="width:400px; height: 100px;">
						<div style="text-align:right">
							<input type="button" name="giftstransactionpopup2-okbtn" value="OK" style="width:80px; height: 30px;" onclick="addCommentsAndUpdateGiftOrder('activate', {$order.orderid}, {$order.gift_card_id}, 'type=id&login_to_activate={$order.sender_email}&adminuser={$login}');">
							<input type="button" name="giftstransactionpopup2-cancelbtn" value="Cancel" style="width:80px; height: 30px;" onclick="hidePopupOverlay2();">
						</div>
					</font>
				</div>
			</div>
			<div class="clear">&nbsp;</div>
		</div>
	</div>
</div>
{literal}
	<script type="text/javascript">
		$(document).ready(function() {
			showReasonsOnrejected(); 
			showSubResolutions();
		});
		function hidePopupOverlay1(){
			$("#giftstransactionpopup1-comments").val("");
			$("#giftstransactionpopup1").css("display","none");
			return false;
		}
		function showPopupOverlay1(){
			$("#giftstransactionpopup1").css("display","block");
		}
		function hidePopupOverlay2(){
			$("#giftstransactionpopup2-comments").val("");
			$("#giftstransactionpopup2").css("display","none");
			return false;
		}
		function showPopupOverlay2(){
			$("#giftstransactionpopup2").css("display","block");
		}
		function addCommentsAndUpdateGiftOrder(mode, orderId, giftCartId, extraQuery){
			var comments = "";
			if(mode == "sendemail"){
				comments = $("#giftstransactionpopup1-comments").val();
				hidePopupOverlay1();
			} else if(mode == "activate"){
				comments = $("#giftstransactionpopup2-comments").val();
				hidePopupOverlay2();
			}
			if(comments == ""){
				alert("No comments entered");
				return false;
			} else {
				extraQuery += "&comments="+comments;
				updateGiftOrder(mode, orderId, giftCartId, extraQuery);
			}
		}
	</script>
{/literal}
<!--google analytics-->
{if ($order.payment_method eq chq and $cheque_recieved eq true) or ($trackevent eq true)}
    <script type="text/javascript">
        var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
        document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
    </script>

    <script type="text/javascript">
    try {ldelim}
        var pageTracker = _gat._getTracker("{$ga_acc}");
        pageTracker._trackPageview();

        pageTracker._addTrans(
        "{$order.orderid}",                                     // Order ID
        "{$userinfo.b_firstname} {$userinfo.b_lastname}", //cust name
        "{$amountafterdiscount|string_format:'%.2f'}",	  // Total
        "{$vat}",                                     	  // Tax
        "{$shippingRate}",                                // Shipping
        "{$userinfo.s_city}",                             // City
        "{$userinfo.s_state}",                            // State
        "{$userinfo.s_country}"                              // Country
        );
        
        {foreach name=outer k=key item=product from=$productsInCart}
            {foreach key=key item=item from=$product}
                {if $key eq 'productid'}
                {assign var='productId' value=$item}
                {/if}

                {if $key eq 'productStyleName'}
                {assign var='productStyleName' value=$item}
                {/if}

                {if $key eq 'amount'}
                {assign var='quantity' value=$item}
                {/if}

                {if $key eq 'productTypeLabel'}
                {assign var='productTypeLabel' value=$item}
                {/if}

                {if $key eq 'totalPrice'}
                {assign var='totalPrice' value=$item}
                {/if}
            {/foreach}

            pageTracker._addItem(
            "{$order.orderid}",			// Order ID
            "{$productId}",         //pid
            "{$productStyleName}",  //style
            "{$productTypeLabel}",  //type
            "{$totalPrice}",        //Price(qty*productprice)
            "{$quantity}"           // Quantity
            );
        {/foreach}
        pageTracker._trackTrans();
        {rdelim} catch(err) {ldelim}{rdelim}
    </script>
{/if}
<!--google analytics-->
