<html>
<head>
	<title>upload attachment</title>
	<link rel="stylesheet" href="{$SkinDir}/skin1_admin.css" />
{literal}
<script>
function validateUpload()
{
	if(document.getElementById('attach').value == "")
	{
		alert("please browse file to upload!!");
		document.getElementById('attach').focus();
		return false;
	}

	if(document.getElementById('attachment_description').value == "")
	{
		alert("please enter description for the uploaded file!!");
		document.getElementById('attachment_description').focus();
		return false;
	}
}
</script>
{/literal}
</head>
<body align="center" style="margin-left:60px;">
<br><br>
{capture name=dialog}
<form name="uploadattachment" action="uploadrequestattachment.php" method="post" enctype="multipart/form-data">
<input type="hidden" name="mode" id="mode" value="attachment"  />
<input type="hidden" name="requestid" id="requestid" value="{$requestid}" />
	<br/>
	<table width='95%' class='formtable' cellpadding='2' cellspacing='2' style='border: 1px solid #999999;'>
		
		<tr style="background-color:#91DDE6;">
			<td width='95%' align="left" colspan='2'><p><strong>upload attachment</strong></p></td>
		</tr>
		<tr class='TableSubHead'>
			<td width='100%' colspan='2' class="SubmitBox">
			Upload attachment&nbsp;
			<input type="file" name="attach" id="attach">&nbsp;&nbsp;&nbsp;
			</td>
		  </tr> 
		  <tr class='TableSubHead'>
			<td width='15%' class="SubmitBox" valign="top">
			description*
			</td>
			<td width='85%' class="SubmitBox" valign="top">
			<TEXTAREA NAME="attachment_description" id="attachment_description" ROWS="10" COLS="80"></TEXTAREA>
			</td>
		  </tr> 
		  <tr class='TableSubHead'>
			<td width='100%' colspan='2' class="SubmitBox" align="center">
			<input type="submit" class="submit" name="submitattach" id="submitattach" value="upload" onclick="return validateUpload();">
			</td>
		  </tr> 
	 </table>
  </form>
{/capture}
{include file="dialog.tpl" title="Request Alerts" content=$smarty.capture.dialog extra='width="90%"'}
</body>
</html>