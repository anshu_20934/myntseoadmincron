{* $Id: designer_selector.tpl,v 1.3 2006/04/10 06:42:21 max Exp $ *}
{if $display_field eq ''}{assign var="display_field" value="category_path"}{/if}
<select MULTIPLE style="height:100px;" name="{$field|default:"categoryid"}"{$extra}>
{if $display_empty eq 'P'}
<option value="">{$lng.lbl_please_select_category}</option>
{elseif $display_empty eq 'E'}
<option value="0">----select category----</option>
{else}
<option value="0">----select category----</option>
{/if}
{foreach from=$allcategories item=c key=catid}
	<option value="{$catid}"
		{foreach from=$coupon.catids key=i item=cid}
			{if $catid eq $cid}
				selected="selected"
			{/if}
		{/foreach}
	>{$c.$display_field}</option>
{/foreach}
</select>

