{* $Id: referred_sales.tpl,v 1.15.2.2 2006/07/11 08:39:27 svowl Exp $ *}
{include file="page_title.tpl" title=$lng.lbl_referred_sales}
{$lng.txt_reffered_sales_note}<br /><br />

<!-- IN THIS SECTION -->

{include file="dialog_tools.tpl"}

<!-- IN THIS SECTION -->
<br />

{capture name=dialog}
<form action="referred_sales.php" method="post" name="referredsalesform">

<table>
<tr>
	<td>{$lng.lbl_period_from}:</td>
	<td>{html_select_date prefix="Start" time=$search.start_date|default:$month_begin start_year=$config.Company.start_year end_year=$config.Company.end_year}</td>
</tr>
<tr>
    <td>{$lng.lbl_period_to}:</td>
    <td>{html_select_date prefix="End" time=$search.end_date start_year=$config.Company.start_year end_year=$config.Company.end_year}</td>
</tr>
<tr>
    <td>{$lng.lbl_sku}:</td>
    <td><input type="text" name="search[productcode]" size="20" value="{$search.productcode}" /></td>
</tr>
<tr>
    <td>{$lng.lbl_show_top_products}</td>
    <td><input type="checkbox" name="search[top]" value="Y"{if $search.top eq 'Y'} checked="checked"{/if} /></td>
</tr>
{if $usertype eq 'A' || ($usertype eq 'P' && $active_modules.Simple_Mode ne '')}
<tr>
    <td>{$lng.lbl_partner}:</td>
    <td>
	<select name="search[partner]">
		<option value=''{if $search.partner eq ''} selected="selected"{/if}>{$lng.lbl_all}</option>
	{foreach from=$partners item=v}
		<option value='{$v.login}'{if $search.partner eq $v.login} selected="selected"{/if}>{$v.firstname} {$v.lastname}</option>
	{/foreach}
	</select>
	</td>
</tr>
{/if}
<tr>
    <td>{$lng.lbl_status}</td>
    <td>
	<select name="search[status]">
		<option value=''{if $search.status eq ''} selected="selected"{/if}>{$lng.lbl_all}</option>
	    <option value='N'{if $search.status eq 'N'} selected="selected"{/if}>{$lng.lbl_pending}</option>
	    <option value='Y'{if $search.status eq 'Y'} selected="selected"{/if}>{$lng.lbl_paid}</option>
	</select>
	</td>
</tr>
{if $search.top eq 'Y'}
<tr>
    <td>{$lng.lbl_sort_by}</td>
    <td>
	<select name="search[sort_by]">
		<option value='total'{if $search.sort_by eq 'total' || $search.status eq ''} selected="selected"{/if}>{$lng.lbl_total}</option>
    	<option value='amount'{if $search.sort_by eq 'amount'} selected="selected"{/if}>{$lng.lbl_quantity}</option>
	    <option value='product_commission'{if $search.sort_by eq 'product_commission'} selected="selected"{/if}>{$lng.lbl_commissions}</option>
	</select>
	</td>
</tr>
{/if}
<tr>
	<td>&nbsp;</td>
	<td><input type="submit" value="{$lng.lbl_search|strip_tags:false|escape}" /></td>
</tr>
</table>
</form>
{/capture}
{include file="dialog.tpl" content=$smarty.capture.dialog title=$lng.lbl_search extra='width="100%"'}

{if $sales ne ''}
<br />

{capture name=dialog}
<table width="100%" cellspacing="1" cellpadding="2">
<tr class="TableHead">
{if ($usertype eq 'A' || ($usertype eq 'P' && $active_modules.Simple_Mode ne '')) && $search.top ne 'Y'}
    <td rowspan="2">{$lng.lbl_partner}</td>
    <td rowspan="2">{$lng.lbl_partner_parent}</td>
{/if}
	<td rowspan="2">{$lng.lbl_product}</td>
{if $search.top ne 'Y'}
	<td colspan="2" align="center">{$lng.lbl_order}</td>
{/if}
	<td rowspan="2" align="center">{$lng.lbl_quantity}</td>
{if $config.XAffiliate.partner_allow_see_total eq 'Y' || $usertype ne 'B'}
	<td rowspan="2" align="center">{$lng.lbl_total}</td>
{/if}
    <td rowspan="2" align="center">{$lng.lbl_commission}</td>
{if $search.top ne 'Y'}
	<td rowspan="2" align="center">{$lng.lbl_status}</td>
{/if}
</tr>
<tr class="TableHead">

{if $search.top ne 'Y'}
    <td align="center">#</td> 
    <td align="center">{$lng.lbl_date}</td>
{/if}

</tr>
{assign var="total_amount" value=0}
{assign var="total_total" value=0}
{assign var="total_product_commissions" value=0}
{foreach from=$sales item=v}
<tr>
{if ($usertype eq 'A' || ($usertype eq 'P' && $active_modules.Simple_Mode ne '')) && $search.top ne 'Y'}
	<td><a href="user_modify.php?user={$v.login|escape:"url"}&amp;usertype=B">{$v.login}</a></td>
	<td>{if $v.parent ne ''}<a href="user_modify.php?user={$v.parent|escape:"url"}&amp;usertype=B">{$v.parent}</a>{/if}</td>
{/if}
	<td>{if $usertype eq 'A' || ($usertype eq 'P' && $active_modules.Simple_Mode ne '')}<a href="product_modify.php?productid={$v.productid}">{/if}{$v.product}{if $usertype eq 'A' || ($usertype eq 'P' && $active_modules.Simple_Mode ne '')}</a>{/if}</td>
{if $search.top ne 'Y'}
    <td>{if $usertype eq 'A' || ($usertype eq 'P' && $active_modules.Simple_Mode ne '')}<a href="order.php?orderid={$v.orderid}">{/if}{$v.orderid}{if $usertype eq 'A' || ($usertype eq 'P' && $active_modules.Simple_Mode ne '')}</a>{/if}</td>
	<td nowrap="nowrap">{$v.add_date|date_format:$config.Appearance.date_format}</td>
{/if}
	<td>{$v.amount}</td>
{math assign="total_amount" equation="x+y" x=$total_amount y=$v.amount}
{if $config.XAffiliate.partner_allow_see_total eq 'Y' || $usertype ne 'B'}
	<td align="right" nowrap="nowrap">{include file="currency.tpl" value=$v.total}</td>
{math assign="total_total" equation="x+y" x=$total_total y=$v.total}
{/if}
	<td align="right" nowrap="nowrap">{include file="currency.tpl" value=$v.product_commission}</td>
{math assign="total_product_commissions" equation="x+y" x=$total_product_commissions y=$v.product_commission}
{if $search.top ne 'Y'}
	<td>{if $v.paid eq 'Y'}{$lng.lbl_paid}{else}{$lng.lbl_pending}{/if}</td>
{/if}
</tr>
{/foreach}
{assign var="colspan_count" value=3}
{if ($usertype eq 'A' || ($usertype eq 'P' && $active_modules.Simple_Mode ne '')) && $search.top ne 'Y'}{math assign="colspan_count" equation="x+2" x=$colspan_count}{/if}
{if $search.top ne 'Y'}{math assign="colspan_count" equation="x+2" x=$colspan_count}{/if}
{if $usertype eq 'B' && $search.top ne 'Y'}
<tr>
	<td colspan="{$colspan_count}">{$lng.lbl_pending_aff_commissions}</td>
	<td align="right" nowrap="nowrap">{include file="currency.tpl" value=$parent_pending}</td>
</tr>
{math assign="total_product_commissions" equation="x+y" x=$total_product_commissions y=$parent_pending|default:0}
<tr> 
    <td colspan="{$colspan_count}">{$lng.lbl_paid_aff_commissions}</td>
	<td align="right" nowrap="nowrap">{include file="currency.tpl" value=$parent_paid}</td>
{math assign="total_product_commissions" equation="x+y" x=$total_product_commissions y=$parent_paid|default:0}
</tr>
{/if}
{if $search.top ne 'Y'}{math assign="colspan_count" equation="x+1" x=$colspan_count}{/if}
<tr>
    <td colspan="{math equation="x+1" x=$colspan_count}" height="1"><hr size="1" /></td>
</tr>

<tr>
{assign var="colspan_count" value=1}
{if ($usertype eq 'A' || ($usertype eq 'P' && $active_modules.Simple_Mode ne '')) && $search.top ne 'Y'}{math assign="colspan_count" equation="x+2" x=$colspan_count}{/if}
{if $search.top ne 'Y'}{math assign="colspan_count" equation="x+2" x=$colspan_count}{/if}
<td colspan="{$colspan_count}"><b>{$lng.lbl_total}:</b></td>
	<td>{$total_amount}</td>
{if $config.XAffiliate.partner_allow_see_total eq 'Y' || $usertype ne 'B'}
	<td align="right" nowrap="nowrap">{include file="currency.tpl" value=$total_total|default:"0"}</td>
{/if}
	<td align="right" nowrap="nowrap">{include file="currency.tpl" value=$total_product_commissions|default:"0"}</td>
</tr>
</table>

{/capture}
{include file="dialog.tpl" content=$smarty.capture.dialog title=$lng.lbl_sales extra='width="100%"'}
{/if}
