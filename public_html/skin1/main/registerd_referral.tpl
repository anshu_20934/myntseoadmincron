<div class="subhead"><p> {$firstname}'s referred friends</p></div>
{if $result != "NORESULT"}
<div>
	<table cellpadding='2' class='table'>
		<tr class='font_bold'>
			<td class='align_right'>Customer Name</td>
			<td class='align_right'>Referred Date</td>
			<td class='align_right'>Registration Date</td>
			<td class='align_right'>Friend's email</td>
		</tr>
			{section name=pname loop=$result}
				<tr >
				<td class='align_right'>{$result[pname].firstname}&nbsp;{$result[pname].lastname}</td>
				<td class='align_right'>{$result[pname].refdate}</td>
				<td class='align_right'>{$result[pname].regdate}</td>
				<td class='align_right'>{$result[pname].email}</td>
				</tr>
			{/section}
	</table>	
</div>
{else}
	<div class="links"><p>You have not referred your friend(s) OR none of your firend has been registered yet, those were referred by you.</p></div>
{/if}

  
     <div class="clearall"></div>
<div class="foot"></div>
        
</form>

