{* $Id: orders.tpl,v 1.62.2.2 2006/06/16 10:47:41 max Exp $ *}

{*{if $mode ne "search" or $orders eq ""}*}

{if $orders ne ""}
{if $usertype eq "A" or ($usertype eq "P" and $active_modules.Simple_Mode)}
{$lng.txt_adm_search_orders_result_header}
{elseif $usertype eq "P"}
{$lng.txt_search_orders_header}
{/if}
{/if}

{include file="main/include_js.tpl" src="reset.js"}
<script language="javascript" type="text/javascript" src="{$http_location}/skin1/main/ajax_orderhistory.js"></script>
<script type="text/javascript">
<!--
var searchform_def = new Array();
searchform_def[0] = new Array('posted_data[date_period]', '');
searchform_def[1] = new Array('StartDay', '{$search_prefilled.start_date|default:$smarty.now|date_format:"%d"}');
searchform_def[2] = new Array('StartMonth', '{$search_prefilled.start_date|default:$smarty.now|date_format:"%m"}');
searchform_def[3] = new Array('StartYear', '{$search_prefilled.start_date|default:$smarty.now|date_format:"%Y"}');
searchform_def[4] = new Array('EndDay', '{$search_prefilled.end_date|default:$smarty.now|date_format:"%d"}');
searchform_def[5] = new Array('EndMonth', '{$search_prefilled.end_date|default:$smarty.now|date_format:"%m"}');
searchform_def[6] = new Array('EndYear', '{$search_prefilled.end_date|default:$smarty.now|date_format:"%Y"}');
searchform_def[7] = new Array('posted_data[total_min]', '{$zero}');
searchform_def[8] = new Array('posted_data[by_title]', true);
searchform_def[9] = new Array('posted_data[by_options]', true);
searchform_def[10] = new Array('posted_data[price_min]', '{$zero}');
searchform_def[11] = new Array('posted_data[address_type]', '');

{literal}


function managedate(type, status) {
	if (type != 'date')
		var fields = ['posted_data[city]','posted_data[state]','posted_data[country]','posted_data[zipcode]'];
	else
		var fields = ['StartDay','StartMonth','StartYear','EndDay','EndMonth','EndYear'];
	
	for (i in fields)
		if (document.searchform.elements[fields[i]])
			document.searchform.elements[fields[i]].disabled = status;
}

function dateValidate()
{

	if(document.searchform.StartYear.disabled == true)
	{
		document.searchform.submit();
	}
	else
	{
		var startYear = document.searchform.StartYear.value;
		var startMonth = document.searchform.StartMonth.value;
		var startDay = document.searchform.StartDay.value;
		var endYear = document.searchform.EndYear.value;
		var endMonth = document.searchform.EndMonth.value;
		var endDay = document.searchform.EndDay.value;

		if(startYear == endYear)
		{
			if(startMonth <= endMonth)
			{
				if(startMonth == endMonth)
				{
					if(startDay <= endDay)
					{
						return true;
					}
					else
					{
						alert("Start date can not greater then end date");
						return false;
					}
				}
				document.searchform.submit();
			}
			else
			{
				alert("Start month can not greater then end month");
				return false;
			}
		}
		

		if(startYear > endYear)
		{
			alert("Start year can not greater then end year");
			return false;
		}
		else
		{
			document.searchform.submit();
		}

		
		document.searchform.submit();
	}
}
function checkDate()
{
     dateObj1 = document.getElementById('startdate');
     dateObj2 = document.getElementById('enddate');
        
	 if(dateObj1.value != "" &&  dateObj2.value != "")
	 {
	     if(!isDate(dateObj1.value)) 
	     {
	     	  dateObj1.focus();
	     	  return false;
	     }
	     else if (!isDate(dateObj2.value))
	     {
	     	 dateObj2.focus();
	     	 return false;
	     }
	     else
	     {
	     
			var startArray = (dateObj1.value).split("/")
			var endArray = (dateObj2.value).split("/")
			
			var flag = dateDiff(startArray[2],startArray[0],startArray[1],endArray[2],endArray[0],endArray[1])
			if(flag)
			{
			   document.getElementById('date_period_null').value = "true"
			}
			return flag
	     }
	     
	 }
	 else if ((dateObj1.value == "" ||  dateObj2.value == "") &&  document.getElementById('odId').value == "")
      {
         alert("Please input invoice number or valid date range to search.")
	   	 return false;
      }
      else
        return true;
     
}

function popUpWindow(URL)
{
	
	window.open(URL,"Invoice", "menubar=1,resizable=1,toolbar=1,width=800,height=500");
}
{/literal}
-->
</script>


{if $usertype eq "C"}
<form name="searchform" action="orders.php" method="post">

<input type="hidden" name="mode" id="mode" value="" />

<div class="super"><p>{$firstname}'s order history</p></div>
<div class="head"><p>search order history</p></div>

<div class="links">
<div class="field">
	<label>
	<p>
		<strong>Search by date</strong>
		<select name="dateduration" id="dateduration" class="select" onChange="javascript:searchOrder('dateduration', '{$login}');">
			<option value="0">--select duration--</option>
			<option value="7">&nbsp;Last 7 days</option>
			<option value="1M">&nbsp;Last 1 month</option>
			<option value="3M">&nbsp;Last 3 month</option>
			<option value="1Y">&nbsp;Last 1 year</option>
			<!-- <option value="all">&nbsp;All</option> -->
		</select>
	</p>
	</label>
    </div>   

<div class="clearall"></div>

</div>


<div class="foot"></div>
        
</form>


{/if}

{*{if $mode == "ajaxsearch"}*}
{if $usertype eq "C"}
{include file="main/ajax_orderslist.tpl"}
{/if}
{*{/if}*}

{if $mode == "search"}
{include file="main/orders_list.tpl"}
{/if}

{if $search_prefilled.need_advanced_options}

{/if}


{if ($usertype eq "A" || $usertype eq "P") and $mode ne "search"}


{******************************}
{capture name=dialog}
<form name="searchform" action="orders.php" method="post">
<input type="hidden" name="mode" value="" />

<table cellpadding="0" cellspacing="0" width="100%">

<tr>
	<td>

<table cellpadding="1" cellspacing="5" width="100%" >

<tr>
	<td colspan="3">
{$lng.txt_search_orders_text}
<br /><br />
	</td>
<td >
<input type="button" value="Add offline order" onclick="javascript: window.location='offline_orders.php'" />
<br /><br />
	</td>
</tr>

<tr>
	<td class="FormButton" nowrap="nowrap">{$lng.lbl_date_period}:</td>
	<td width="10">&nbsp;</td>
	<td>
<table cellpadding="0" cellspacing="0">
<tr>
	<td width="5"><input type="radio" id="date_period_M" name="posted_data[date_period]" value="M"{if $search_prefilled.date_period eq "M" || $search_prefilled.date_period eq ""} checked="checked"{/if} onclick="javascript:managedate('date',true)" /></td>
	<td class="OptionLabel"><label for="date_period_M">{$lng.lbl_this_month}</label></td>

	<td width="5"><input type="radio" id="date_period_W" name="posted_data[date_period]" value="W"{if $search_prefilled.date_period eq "W"} checked="checked"{/if} onclick="javascript:managedate('date',true)" /></td>
	<td class="OptionLabel"><label for="date_period_W">{$lng.lbl_this_week}</label></td>

	<td width="5"><input type="radio" id="date_period_D" name="posted_data[date_period]" value="D"{if $search_prefilled.date_period eq "D"} checked="checked"{/if} onclick="javascript:managedate('date',true)" /></td>
	<td class="OptionLabel"><label for="date_period_D">{$lng.lbl_today}</label></td>
	<!--
	<td width="5"><input type="radio" id="date_period_A" name="posted_data[date_period]" value="A"{if $search_prefilled eq "A" or $search_prefilled.date_period eq "A"} checked="checked"{/if} onclick="javascript:managedate('date',true)" /></td>
	<td class="OptionLabel"><label for="date_period_A">{$lng.lbl_all_dates}</label></td>
	-->
</tr>
<tr>
	<td width="5"><input type="radio" id="date_period_C" name="posted_data[date_period]" value="C"{if $search_prefilled.date_period eq "C"} checked="checked"{/if} onclick="javascript:managedate('date',false)" /></td>
	<td colspan="7" class="OptionLabel"><label for="date_period_C">{$lng.lbl_specify_period_below}</label></td>
</tr>
</table>
</td>
</tr>

<tr> 
	<td class="FormButton" nowrap="nowrap">{$lng.lbl_order_date_from}:</td>
	<td width="10">&nbsp;</td>
	<td> 
	{html_select_date prefix="Start" time=$search_prefilled.start_date start_year=$config.Company.start_year end_year=$config.Company.end_year}
	</td>
</tr>

<tr> 
	<td class="FormButton" nowrap="nowrap">{$lng.lbl_order_date_through}:</td>
	<td width="10">&nbsp;</td>
	<td> 
	{html_select_date prefix="End" time=$search_prefilled.end_date start_year=$config.Company.start_year end_year=$config.Company.end_year display_days=yes}
	</td>
</tr>
<!-- added later to filter by orderid and status -->
<tr> 
	<td class="FormButton" nowrap="nowrap">{$lng.lbl_order_id}:</td>
	<td width="10">&nbsp;</td>
	<td> 
	  <input type="text" name="posted_data[orderid]" size="15" maxlength="15" 
		value="{$search_prefilled.orderid}" />
	</td>
</tr>
<tr> 
	<td class="FormButton" nowrap="nowrap">Customer Login:</td>
	<td width="10">&nbsp;</td>
	<td> 
	  <input type="text" name="posted_data[login]" size="50" maxlength="100" 
		value="{$search_prefilled.login}" />
	</td>
</tr>
<tr> 
	<td class="FormButton" nowrap="nowrap">Customer Phone No.:</td>
	<td width="10">&nbsp;</td>
	<td> 
	  <input type="text" name="posted_data[issues_contact_number]" size="20" maxlength="100" 
		value="{$search_prefilled.issues_contact_number}" />
	</td>
</tr>
<tr> 
	<td class="FormButton" nowrap="nowrap">{$lng.lbl_order_status}:</td>
	<td width="10">&nbsp;</td>
	<td> 
	  {include file="main/order_status.tpl" status=$search_prefilled.orderstatus mode="select" 
	   name="posted_data[orderstatus]" extended="Y" extra="style='width:70%'"}
	</td>
</tr>
<!-- 
<tr> 
	<td class="FormButton" nowrap="nowrap">Order type:</td>
	<td width="10">&nbsp;</td>
	<td> 
	  <select name="posted_data[ordertype]" id="ordertype">
		<option value="0">---select ordertype---</option>
		<option value="on">Online</option>
		<option value="off">Offline</option>
	  </select>
	</td>
</tr>
 -->
<tr> 
	<td class="FormButton" nowrap="nowrap">Payment Method:</td>
	<td width="10">&nbsp;</td>
	<td> 
	  <select name="posted_data[payment_method]" id="payment_method">
		<option value="0">---select payment method---</option>
		<option value="on">Online</option>
		<option value="cod">Cash On Delivery</option>
	  </select>
	</td>
</tr>

<tr>
	<td class="FormButton" nowrap="nowrap">Courier Operator:</td>
	<td width="10">&nbsp;</td>
	<td>
	  <select name="posted_data[courier_service]" id="courier_service">
            <option value="">---Select courier operator---</option>
	  {section name="courDet" loop=$courier_details}
	  	<option value="{$courier_details[courDet].code}">{$courier_details[courDet].courier_service}</option>
	  {/section}
	  </select>
	</td>
</tr>

<tr>
    <td class="FormButton" nowrap="nowrap">Ops Location:</td>
    <td width="10">&nbsp;</td>
    <td>
        <select name="posted_data[warehouseid]" id="warehouseid">
            <option value="">---Select Ops Location---</option>
            {section name=idx loop=$warehouses}
                <option value="{$warehouses[idx].id}">{$warehouses[idx].name}</option>
            {/section}
        </select>
    </td>
</tr>

<!-- 
<tr> 
	<td class="FormButton" nowrap="nowrap">{$lng.lbl_ccavenue_status}:</td>
	<td width="10">&nbsp;</td>
	<td> 
	  <select name="posted_data[ccavenue]" id="ordertype">
		<option value="0">---select status---</option>
		<option value="Y">Paid</option>
		<option value="N">Not Paid</option>
	  </select>
	</td>
</tr>

<tr> 
	<td class="FormButton" nowrap="nowrap">Select Organization:</td>
	<td width="10">&nbsp;</td>
	<td> 
	
	  <select name="posted_data[orgid]" id="orgid">
	  <option value="0">---select organization---</option>
	  {section name="org" loop=$organization}
	  	<option value="{$organization[org].id}">{$organization[org].subdomain}</option>
	  {/section}
	  </select>
	</td>
</tr>


-->
<!-- end -->
<tr>
	<td colspan="2"></td>
	<td>
	<hr />
<table cellpadding="0" cellspacing="0">
<tr>
	<td><input type="checkbox" id="posted_data_is_export" name="posted_data[is_export]" value="Y" /></td>
	<td>&nbsp;</td>
	<td class="FormButton" nowrap="nowrap"><label for="posted_data_is_export">{$lng.lbl_search_and_export}</label></td>
</tr>
</table>
	</td>
</tr>

<tr>
	<td colspan="2">&nbsp;</td>
	<td colspan="3" class="SubmitBox">
	<input type="submit" value="{$lng.lbl_search|escape}" onclick="javascript: document.searchform.mode.value=''; return dateValidate();" />

{if $search_prefilled.date_period ne "C"}
<script type="text/javascript" language="JavaScript 1.2">
<!--
managedate('date',true);
-->
</script>
{/if}
	</td>
</tr>

</table>

<br />

<!-- 
{include file="main/visiblebox_link.tpl" mark="1" title=$lng.lbl_advanced_search_options}

<br />
 -->

<table cellpadding="1" cellspacing="5" width="100%"{if $js_enabled eq 'Y'} style="display: none;"{/if} id="box1">

<tr>
	<td colspan="3"><br />{include file="main/subheader.tpl" title=$lng.lbl_advanced_search_options}</td>
</tr>

<tr>
	<td colspan="3">{$lng.txt_adv_search_orders_text}<br /><br /></td>
</tr>

<tr>
	<td width="25%" class="FormButton" nowrap="nowrap">{$lng.lbl_order_id}:</td>
	<td width="10">&nbsp;</td>
	<td width="75%">
<input type="text" name="posted_data[orderid1]" size="10" maxlength="15" value="{$search_prefilled.orderid1}" />
-
<input type="text" name="posted_data[orderid2]" size="10" maxlength="15"value="{$search_prefilled.orderid2}" />
	</td>
</tr>

{if $usertype ne "C"}
<tr>
	<td class="FormButton" nowrap="nowrap">{$lng.lbl_order_total} ({$config.General.currency_symbol}):</td>
	<td width="10">&nbsp;</td>
	<td>

<table cellpadding="0" cellspacing="0">
<tr>
	<td><input type="text" size="10" maxlength="15" name="posted_data[total_min]" value="{if $search_prefilled eq ""}{$zero}{else}{$search_prefilled.total_min|formatprice}{/if}" /></td>
	<td>&nbsp;-&nbsp;</td>
	<td><input type="text" size="10" maxlength="15" name="posted_data[total_max]" value="{$search_prefilled.total_max|formatprice}" /></td>
</tr>
</table>

	</td>
</tr>
<!-- 
<tr>
	<td class="FormButton" nowrap="nowrap">{$lng.lbl_payment_method}:</td>
	<td width="10">&nbsp;</td>
	<td>
	<select name="posted_data[payment_method]" style="width:70%">
		<option value=""></option>
{section name=pm loop=$payment_methods}
		<option value="{$payment_methods[pm].payment_method}"{if $search_prefilled.payment_method eq $payment_methods[pm].payment_method} selected="selected"{/if}>{$payment_methods[pm].payment_method}</option>
{/section}
	</select>
	</td>
</tr>
 -->
<tr>
	<td class="FormButton" nowrap="nowrap">{$lng.lbl_delivery}:</td>
	<td width="10">&nbsp;</td>
	<td>
	<select name="posted_data[shipping_method]" style="width:70%">
		<option value=""></option>
{section name=sm loop=$shipping_methods}
		<option value="{$shipping_methods[sm].shippingid}"{if $search_prefilled.shipping_method eq $shipping_methods[sm].shippingid} selected="selected"{/if}>{$shipping_methods[sm].shipping|trademark}</option>
{/section}
	</select>
	</td>
</tr>

{/if}

<tr> 
	<td class="FormButton" nowrap="nowrap">{$lng.lbl_order_status}:</td>
	<td width="10">&nbsp;</td>
	<td>{include file="main/order_status.tpl" status=$search_prefilled.status mode="select" name="posted_data[status]" extended="Y" extra="style='width:70%'"}</td>
</tr>

{if $usertype ne "C"}
{if $usertype eq "A"}
<tr>
	<td class="FormButton" nowrap="nowrap">{$lng.lbl_provider}:</td>
	<td width="10">&nbsp;</td>
	<td>
	<input type="text" name="posted_data[provider]" size="30" value="{$search_prefilled.provider}" style="width:70%" />
	</td>
</tr>
{/if}

<tr> 
	<td class="FormButton" nowrap="nowrap">{$lng.lbl_order_features}:</td>
	<td width="10">&nbsp;</td>
	<td>
{assign var="features" value=$search_prefilled.features}
	<select name="posted_data[features][]" multiple="multiple" size="7" style="width:70%">
		<option value="gc_applied"{if $features.gc_applied} selected="selected"{/if}>{$lng.lbl_entirely_or_partially_payed_by_gc|strip_tags}</option>
		<option value="discount_applied"{if $features.discount_applied} selected="selected"{/if}>{$lng.lbl_global_discount_applied|strip_tags}</option>
		<option value="coupon_applied"{if $features.coupon_applied} selected="selected"{/if}>{$lng.lbl_discount_coupon_applied|strip_tags}</option>
		<option value="free_ship"{if $features.free_ship} selected="selected"{/if}>{$lng.lbl_free_shipping|strip_tags}</option>
		<option value="free_tax"{if $features.free_tax} selected="selected"{/if}>{$lng.lbl_tax_exempt|strip_tags}</option>
		<option value="gc_ordered"{if $features.gc_ordered} selected="selected"{/if}>{$lng.lbl_gc_purchased|strip_tags}</option>
		<option value="notes"{if $features.notes} selected="selected"{/if}>{$lng.lbl_orders_with_notes_assigned|strip_tags}</option>
	</select><br />
{$lng.lbl_hold_ctrl_key}
	</td>
</tr>

{/if}

{if $usertype ne "C"}

<tr>
	<td colspan="3"><br />{include file="main/subheader.tpl" title=$lng.lbl_search_by_ordered_products class="grey"}</td>
</tr>

<tr>
	<td class="FormButton" nowrap="nowrap">{$lng.lbl_search_for_pattern}:</td>
	<td width="10"><font class="CustomerMessage"></font></td>
	<td>
	<input type="text" name="posted_data[product_substring]" size="30" value="{$search_prefilled.product_substring}" style="width:70%" />
	</td>
</tr>

<tr>
	<td class="FormButton" nowrap="nowrap">{$lng.lbl_search_in}:</td>
	<td width="10"><font class="CustomerMessage"></font></td>
	<td>

<table cellpadding="0" cellspacing="0">
<tr>
	<td width="5"><input type="checkbox" id="posted_data_by_title" name="posted_data[by_title]"{if $search_prefilled eq "" or $search_prefilled.by_title} checked="checked"{/if} /></td>
	<td nowrap="nowrap"><label for="posted_data_by_title">{$lng.lbl_product_title}</label>&nbsp;&nbsp;</td>

	<td width="5"><input type="checkbox" id="posted_data_by_options" name="posted_data[by_options]"{if $search_prefilled eq "" or $search_prefilled.by_options} checked="checked"{/if} /></td>
	<td nowrap="nowrap"><label for="posted_data_by_options">{$lng.lbl_options}</label></td>
</tr>
</table>

	</td>
</tr>

<tr>
	<td class="FormButton" nowrap="nowrap">{$lng.lbl_sku}:</td>
	<td width="10"><font class="CustomerMessage"></font></td>
	<td>
	<input type="text" maxlength="64" name="posted_data[productcode]" value="{$search_prefilled.productcode}" style="width:70%" />
	</td>
</tr>

<tr>
	<td class="FormButton" nowrap="nowrap">{$lng.lbl_productid}#:</td>
	<td width="10"><font class="CustomerMessage"></font></td>
	<td>
	<input type="text" maxlength="64" name="posted_data[productid]" value="{$search_prefilled.productid}" style="width:70%" />
	</td>
</tr>

<tr> 
	<td class="FormButton" nowrap="nowrap">{$lng.lbl_price} ({$config.General.currency_symbol}):</td>
	<td width="10">&nbsp;</td>
	<td>
<table cellpadding="0" cellspacing="0">
<tr>
	<td><input type="text" size="10" maxlength="15" name="posted_data[price_min]" value="{if $search_prefilled eq ""}{$zero}{else}{$search_prefilled.price_min|formatprice}{/if}" /></td>
	<td>&nbsp;-&nbsp;</td>
	<td><input type="text" size="10" maxlength="15" name="posted_data[price_max]" value="{$search_prefilled.price_max|formatprice}" /></td>
</tr>
</table>
	</td>
</tr>

{/if}

{if $usertype ne "C"}

<tr>
	<td colspan="3"><br />{include file="main/subheader.tpl" title=$lng.lbl_search_by_customer class="grey"}</td>
</tr>

<tr> 
	<td class="FormButton" nowrap="nowrap">{$lng.lbl_customer}:</td>
	<td width="10">&nbsp;</td>
	<td><input type="text" name="posted_data[customer]" size="30" value="{$search_prefilled.customer}" style="width:70%" /></td>
</tr>

<tr>
	<td class="FormButton">{$lng.lbl_search_in}:</td>
	<td width="10">&nbsp;</td>
	<td>
<table cellspacing="0" cellpadding="0">
<tr>
    <td width="5"><input type="checkbox" id="posted_data_by_username" name="posted_data[by_username]"{if $search_prefilled eq "" or $search_prefilled.by_username} checked="checked"{/if} /></td>
    <td nowrap="nowrap"><label for="posted_data_by_username">{$lng.lbl_username}</label>&nbsp;&nbsp;</td>

	<td width="5"><input type="checkbox" id="posted_data_by_firstname" name="posted_data[by_firstname]"{if $search_prefilled eq "" or $search_prefilled.by_firstname} checked="checked"{/if} /></td>
	<td nowrap="nowrap"><label for="posted_data_by_firstname">{$lng.lbl_first_name}</label>&nbsp;&nbsp;</td>
	<!-- 
	<td width="5"><input type="checkbox" id="posted_data_by_lastname" name="posted_data[by_lastname]"{if $search_prefilled eq "" or $search_prefilled.by_lastname} checked="checked"{/if} /></td>
	<td nowrap="nowrap"><label for="posted_data_by_lastname">{$lng.lbl_last_name}</label></td>
	-->
</tr>
</table>
	</td>
</tr>

<tr>
	<td class="FormButton" nowrap="nowrap">{$lng.lbl_search_by_address}:</td>
	<td width="10"><font class="CustomerMessage"></font></td>
	<td>
<table cellpadding="0" cellspacing="0">
<tr>
	<td width="5"><input type="radio" id="address_type_null" name="posted_data[address_type]" value=""{if $search_prefilled eq "" or $search_prefilled.address_type eq ""} checked="checked"{/if} onclick="javascript:managedate('address',true)" /></td>
	<td class="OptionLabel"><label for="address_type_null">{$lng.lbl_ignore_address}</label></td>

	<td width="5"><input type="radio" id="address_type_B" name="posted_data[address_type]" value="B"{if $search_prefilled.address_type eq "B"} checked="checked"{/if} onclick="javascript:managedate('address',false)" /></td>
	<td class="OptionLabel"><label for="address_type_B">{$lng.lbl_billing}</label></td>

	<td width="5"><input type="radio" id="address_type_S" name="posted_data[address_type]" value="S"{if $search_prefilled.address_type eq "S"} checked="checked"{/if} onclick="javascript:managedate('address',false)" /></td>
	<td class="OptionLabel"><label for="address_type_S">{$lng.lbl_shipping}</label></td>

	<td width="5"><input type="radio" id="address_type_both" name="posted_data[address_type]" value="Both"{if $search_prefilled.address_type eq "Both"} checked="checked"{/if} onclick="javascript:managedate('address',false)" /></td>
	<td class="OptionLabel"><label for="address_type_both">{$lng.lbl_both}</label></td>
</tr>
</table>
	</td>
</tr>

<tr>
	<td class="FormButton" nowrap="nowrap">{$lng.lbl_city}:</td>
	<td width="10"><font class="CustomerMessage"></font></td>
	<td><input type="text" maxlength="64" name="posted_data[city]" value="{$search_prefilled.city}" style="width:70%" /></td>
</tr>

<tr>
	<td class="FormButton" nowrap="nowrap">{$lng.lbl_state}:</td>
	<td width="10"><font class="CustomerMessage"></font></td>
	<td>{include file="main/states.tpl" states=$states name="posted_data[state]" default=$search_prefilled.state required="N" style="style='width:70%'"}</td>
</tr>

<tr>
	<td class="FormButton" nowrap="nowrap">{$lng.lbl_country}:</td>
	<td width="10"><font class="CustomerMessage"></font></td>
	<td>
	<select name="posted_data[country]" style="width:70%">
		<option value="">[{$lng.lbl_please_select_one}]</option>
{section name=country_idx loop=$countries}
	<option value="{$countries[country_idx].country_code}"{if $search_prefilled.country eq $countries[country_idx].country_code} selected="selected"{/if}>{$countries[country_idx].country}</option>
{/section}
	</select>
	</td>
</tr>

<tr>
	<td class="FormButton" nowrap="nowrap">{$lng.lbl_zip_code}:</td>
	<td width="10"><font class="CustomerMessage"></font></td>
	<td>
<input type="text" maxlength="32" name="posted_data[zipcode]" value="{$search_prefilled.zipcode}" style="width:70%" />
{if $search_prefilled eq "" or $search_prefilled.address_type eq ""}
<script type="text/javascript" language="JavaScript 1.2">
<!--
managedate('address',true);
-->
</script>
{/if}
	</td>
</tr>

<tr>
	<td class="FormButton" nowrap="nowrap">{$lng.lbl_phone}/{$lng.lbl_fax}:</td>
	<td width="10"><font class="CustomerMessage"></font></td>
	<td><input type="text" maxlength="32" name="posted_data[phone]" value="{$search_prefilled.phone}" style="width:70%" /></td>
</tr>

<tr>
	<td class="FormButton" nowrap="nowrap">{$lng.lbl_email}:</td>
	<td width="10">&nbsp;</td>
	<td><input type="text" maxlength="128" name="posted_data[email]" value="{$search_prefilled.email}" style="width:70%" /></td>
</tr>

{/if}

<tr>
	<td colspan="2">&nbsp;</td>
	<td>
	<br /><br />
	<input type="submit" value="{$lng.lbl_search|escape}" onclick="javascript: submitForm(this, '');" />
	&nbsp;&nbsp;&nbsp;
	<input type="button" value="{$lng.lbl_reset|escape}" onclick="javascript: reset_form('searchform', searchform_def);" />
	</td>
</tr>

</table>

	</td>
</tr>

</table>
</form>

{if $search_prefilled.need_advanced_options}
<script type="text/javascript" language="JavaScript 1.2">
<!--
visibleBox('1');
-->
</script>
{/if}


{/capture}

{if $mode eq "search"}
<br /><br />
{if $total_items >= "1"}
{$lng.txt_N_results_found|substitute:"items":$total_items}<br />
{$lng.txt_displaying_X_Y_results|substitute:"first_item":$first_item:"last_item":$last_item}
{else}
{$lng.txt_N_results_found|substitute:"items":0}
{/if}
{/if}


{******************************}

{if $usertype eq "A" or $active_modules.Simple_Mode ne ""}
{include file="dialog.tpl" title=$lng.lbl_export_delete_orders content=$smarty.capture.dialog extra='width="100%"'}
{else}
{include file="dialog.tpl" title=$lng.lbl_export_orders content=$smarty.capture.dialog extra='width="100%"'}
{/if}

{/if}

