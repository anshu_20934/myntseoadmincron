{* $Id: product_delete_confirmation.tpl,v 1.24 2005/11/30 13:29:35 max Exp $ *}
<!-- {include file="page_title.tpl" title=$lng.lbl_delete_products} -->
<!-- IN THIS SECTION -->


<!-- IN THIS SECTION -->
{literal}
<script language="JavaScript">
function redirect()
{
	 window.location.href="mkMyAccount.php?acPage=3";
}
</script>
{/literal}



<div class="head">
        <span class="Text">{$lng.lbl_product_delete_confirmation_header}?</span>
 </div>

 <div class="links">
<form action="process_product.php" method="post" name="processform">

<input type="hidden" name="section" value="{$section}" />
<input type="hidden" name="cat" value="{$cat}" />

<input type="hidden" name="mode" value="" />
<input type="hidden" name="confirmed" value="Y" />
<input type="hidden" name="navpage" value="{$navpage}" />

  


<ul>
<strong>
{section name=prod loop=$products}
<li><p><span class="ProductPriceSmall">{$products[prod].productcode} {$products[prod].product} - {include file="currency.tpl" value=$products[prod].price}</span><br></p>
</li>
{/section}
</strong>
</ul>

<br />

&nbsp;&nbsp;{$lng.txt_operation_not_reverted_warning}

{if $search_return ne ''}
{assign var="url_to" value=$search_return} 
{elseif $section eq "category_products"}
{assign var="url_to" value="category_products.php?cat=`$cat`&page=`$navpage`"}
{else}
{assign var="url_to" value="search.php?mode=search&page=`$navpage`"}
{/if}

<br /><br />
    
         {$lng.txt_are_you_sure_to_proceed}
	  
	
	{*{include file="buttons/yes.tpl" href="javascript:document.processform.mode.value='delete'; document.processform.submit()" js_to_href="Y"} *}

	<input class="submit" type="submit" name="submitButton" value="Yes" border="0" onclick="javascript: document.processform.mode.value='delete'; document.processform.submit();">

	{*{include file="buttons/no.tpl" href=$url_to}*}
	<input class="submit" type="button" name="no" value="No" border="0" onclick="javascript:redirect();"> 


</form>
</div>
<div class="foot"></div>

