{* $Id: register_personal_info.tpl,v 1.12 2006/01/31 14:24:05 svowl Exp $ *}
{if $is_areas.P eq 'Y'}
	<table>
		<tr>
			<td style="width:175px;">
				{if $default_fields.firstname.avail eq 'Y'}
					<p><span class="mandatory">*</span>{$lng.lbl_first_name}:</p>							
				{/if}
			</td>
			<td style="width:220px;">
				{if $default_fields.firstname.avail eq 'Y'}
					<p><input type="text" id="firstname" name="firstname"  value="{$userinfo.firstname}" tabindex="1"/></p>				
				{/if}
			</td>
			<td style="width:175px;"><p>About Me:</p></td>
			<td style="width:385px;"><p><textarea id="about_me" name="about_me" tabindex="11">{$userinfo.about_me}</textarea></p></td>
		</tr>	
	
		<tr>
			<td>
				{if $default_fields.lastname.avail eq 'Y'}
					<p><span class="mandatory">*</span>{$lng.lbl_last_name}:</p>								
				{/if}
				
			</td>
			<td>
				{if $default_fields.lastname.avail eq 'Y'}
					<p><input type="text" id="lastname" name="lastname"  value="{$userinfo.lastname}" tabindex="2"/></p>				
				{/if}
			</td>
			<td>
				{if $default_fields.image.avail eq 'Y'}
					<p>Upload Your image:</p>			
				{/if}					
			</td>
			<td class="alignleft">
				{if $default_fields.image.avail eq 'Y'}
					<p><input type="file" id="myimage" name="myimage" tabindex="12"></p>
				{/if}
			</td>
		</tr>
	
		<tr>
			<td style="vertical-align:top;padding-top:10px">
				<p><span class="mandatory">*</span>{$lng.lbl_email}:</p>						
			</td>
			<td>
				{if $userinfo.login ne "" || ($login eq $userinfo.uname && $login ne '')}
					<p style="text-align:left;padding:0px 0px 15px 12px;"><b>{$userinfo.login|default:$userinfo.uname}</b>
					<input type="hidden" name="uname" value="{$userinfo.login|default:$userinfo.uname}" /><p>
				{else}
					<p><input  type="text" id="uname" name="uname" tabindex="3" value="{*if $userinfo.uname*}{*$userinfo.uname*}{*else*}{*$userinfo.login*}{*/if*}"/>
					<br/><span class="greymandatory" style="font-size:7pt;">(your e-mail will be your username)</span></p>
				{/if}
			</td>
			<td rowspan="2">
				<p>My Interests:</p>			
			</td>
			<td rowspan="2"><p><textarea id="interests" name="interests" tabindex="13">{$userinfo.interests}</textarea></p></td>
			
		</tr>
		
		<tr>
			<td style="vertical-align:top;padding-top:10px">
				<p><span class="mandatory">*</span>{$lng.lbl_password}:</p>					
			</td>
			<td>
				<p><input  type="password" id="passwd1" name="passwd1"  value="" class="inputtextbox" tabindex="4"/>
				<br/><span class="greymandatory" style="font-size:7pt;">(minimum 6 character)</span></p>
			</td>		
			<!--<td>&nbsp;</td>
			<td>&nbsp;</td>-->
		</tr>
		
		<tr>
			<td>
				<p><span class="mandatory">*</span>{$lng.lbl_confirm_password}:</p>			
			</td>
			<td>
				<p><input type="password" id="passwd2" name="passwd2"  value="" class="inputtextbox" tabindex="5"/></p>
			</td>
			<td rowspan="2">
				<p>My Links:</p>			
			</td>
			<td rowspan="2"><p><textarea id="mylinks" name="mylinks" tabindex="14">{$userinfo.mylinks}</textarea></p></td>
		</tr>
		
		<tr>
			<td>
				{if $default_fields.mobile.avail eq 'Y'}
					<p>Mobile:</p>			    			    			
				{/if}		
			</td>
			<td>
				{if $default_fields.mobile.avail eq 'Y'}				
				    <p><input type="text" id="mobile" name="mobile"  value="{$userinfo.mobile}" maxlength="11" tabindex="6"/></p>			    			
				{/if}			
			</td>		
			<!--<td>&nbsp;</td>
			<td>&nbsp;</td>-->
		</tr>
		
		<tr>
			<td>
				<p><span class="mandatory">*</span>Gender:</p>			
			</td>
			<td style="text-align:left;padding-left:10px">
				<p>
					<select name="gender" tabindex="7">
						{if $userinfo.gender eq 'F'}					
							<option value="F" selected>Female</option>
							<option value="M">Male</option>
						{else}
							<option value="M" selected>Male</option>
							<option value="F">Female</option>
						{/if}
					</select>
				</p>
			</td>
			<td rowspan="2">
				{if $userinfo.myimage  ne "" }
					<p>Your uploaded image:</p>				
				{/if}			
			</td>
			<td rowspan="2" class="alignleft">
				{if $userinfo.myimage  ne "" }
					<p><img src="{$cdn_base}/images/designer/T/{$userinfo.myimage}" />
				    <input class="submit" style="width:auto;height:auto;" type="button" name="deleteImage" value="Delete image" border="0" 
					   onClick= "javascript:deleteUserImage('{$userinfo.myimage}');" tabindex="16"></p>			
				{/if}
			</td>
		</tr>
		
		<tr>
			<td>
				<p>Date of Birth:</p>									
			</td>
			<td class="alignleft" style="padding-left:10px">
				<p>
				{php}
					$userinfo = $this->get_template_vars('userinfo');
					$year = date("Y");
					$month = 12;
					$day = 31; 
					$dayselect = "<select style='font-weight:normal;font-size:9pt;' name='day' tabindex='8'><option value=''>day</option>";
					for($i=1;$i<=$day;$i++){
						if($userinfo['DOB_day'] == $i || $userinfo['day'] == $i)
							$dayselect .= "<option value='$i' selected>$i</option>";
						else
							$dayselect .= "<option value='$i'>$i</option>";
					}
					$dayselect .= "</select>";
					
					$monthselect = " <select style='font-weight:normal;font-size:9pt;' name='month' tabindex='9'><option value=''>month</option>";
					for($i=1;$i<=$month;$i++){
						if($userinfo['DOB_month'] == $i || $userinfo['month'] == $i)
							$monthselect .= "<option value='$i' selected>$i</option>";
						else
							$monthselect .= "<option value='$i'>$i</option>";
					}		
					$monthselect .= "</select>";
					
					$yearselect = " <select style='font-weight:normal;font-size:9pt;' name='year' tabindex='10'><option value=''>year</option>";
					for($i=$year;$i>=1900;$i--){
						if($userinfo['DOB_year'] == $i || $userinfo['year'] == $i)
							$yearselect .= "<option value='$i' selected>$i</option>";
						else
							$yearselect .= "<option value='$i'>$i</option>";
					}
					$yearselect .= "</select>";
					
					echo $dayselect.$monthselect.$yearselect;
					 	
				{/php}
				</p>				
			</td>		
			<!--<td>&nbsp;</td>
			<td>&nbsp;</td>-->
		</tr>
	</table>
{/if}