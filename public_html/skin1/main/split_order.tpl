{* $Id: history_order.tpl,v 1.83.2.6 2006/06/27 08:08:06 max Exp $ *}
{include file="page_title.tpl" title=$lng.lbl_order_details_label}

<script type="text/javascript">
    var orderId = "{$orderid}";
</script>
{include file="main/include_js.tpl" src="/admin/main/order_details.js"}

{capture name=dialog}
{if $action eq "confirm"}
    {if $errorMsg && $errorMsg ne ''}
        <div style="color:red; font-size:14px; font-weight: bold;">{$errorMsg}</div>
    {else}
		Select the no of pieces to be split:
		<div id="msgBar" style="text-align:center; font-weight: bold; color: red;">&nbsp;</div>
		{if $usertype ne "C"}
			<form id="split-order-form" action="" method="POST">
				<table width="100%"  border="0" cellspacing="0" cellpadding="0">
					<thead style="background-color: #DFDFDF;">
						<th align=left style="padding: 10px 5px;">Product Image</th>
						<th align=left style="padding: 10px 5px;">Product</th>
						<th align=left style="padding: 10px 5px;">Style</th>
						<th align=left style="padding: 10px 5px;">Quantity</th>
						<th align=center style="padding: 10px 5px;">Price</th>
						<th align=left style="padding: 10px 5px;" width=20>&nbsp;</th>
					</thead>
				    {section name=idx loop=$productsInCart}
						<tr height="75">
							<td>
								{if $productsInCart[idx].designImagePath != ""}
									<img src="{$productsInCart[idx].designImagePath}"  style="border:1px solid #CCCCCC;">
								{else}
									<img  src="{$xcart_web_dir}/image.php?type={$type}&amp;id={$id}&amp;ts={$smarty.now}" width="100"  height="70" hspace="5" vspace="5" />
								{/if}
							</td>
							<td>{$productsInCart[idx].productTypeLabel} </td>
							<td>{$productsInCart[idx].productStyleLabel} (Article Id: {$productsInCart[idx].article_number})</td>
							<td>
								<select name="item_quantity_{$productsInCart[idx].itemid}" style="width:40px;">
									{section name=quantity loop=$productsInCart[idx].amount }
		                                <option value="{$smarty.section.quantity.iteration}">{$smarty.section.quantity.iteration}</option>
		                            {/section}
		                        </select>
							</td>
							<td align=center>{$productsInCart[idx].price}</td>
							<td>&nbsp;</td>
						</tr>
						<tr height=20><td colspan=6><hr/></td></tr>
					{/section}
					<tr>
						<td colspan=2 valign=top>
							Reason: <br/>
							<select id="split_reason" name="split_reason">
							<option value="0">Select Reason</option>
							{section name=index loop=$reasons}
								<option value="{$reasons[index].reason}">{$reasons[index].reason_display_name}</option> 
							{/section}
							</select>
						</td>
						<td colspan=2>
							<label>Remarks (<em style="color:red;">*</em>):</label><br/>
							<textarea id="split_comment" name="split_comment" style="width: 300px; height: 50px;"></textarea>
						</td>
					</tr>
				</table>
				
				<input type=hidden name=action value="split" />
				<input type=hidden name=itemids value="{$itemids}" />
				<input type=hidden name=orderid value="{$orderid}" />
				<input type=hidden name=login value="{$login}" />
				<div style="width:100%; text-align:right; padding:5px;">
					<input type=submit name="split-order-yes" id="split-order-yes" value="Split these items" style="padding:5px 10px;"/>
				</div>
			</form>
		{/if}
	{/if}
	{/capture}
	{include file="dialog.tpl" title=$lng.lbl_products_details content=$smarty.capture.dialog extra='width="100%"'}
{else}
	{if $errorMsg && $errorMsg ne ''}
		<div style="color:red; font-size:14px; font-weight: bold;">{$errorMsg}</div>
	{else}
		<p>Item(s) have been successfully <strong>SPLIT!</strong>. You new orderid is {$neworderid}</p>
		<p>Click on close button to refresh data on back page</p>
		<input type=button name="return-btn" id="return-btn" value="Close" style="padding:5px 10px;" onclick="opener.window.location.reload(); self.close();"/>
	{/if}
{/if}