{* $Id: featured_products.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
{include file="main/include_js.tpl" src="main/popup_product.js"}

<link rel="stylesheet" href="{$SkinDir}/skin1_admin.css" />
 
{if $assigneeid != ""}
<div style="margin-left:70px;">
        <div class="DIV_TABLE" style="width:860px;">
	<div class="DIV_TH" style="width:860px;height:auto;">
            <p style="margin-left:10px;font-size:1.2em;"><strong>Item assignee: {$assignee_name|upper}</strong></p>
	    <p style="margin-left:10px;font-size:1.2em;"><strong>Total assigned item: {$total_assigned_item}</strong></p>
        </div>
	<div class="ITEM_SEARCH_BUTTON_DIV" style="width:860px;">
            <input type="button" value="print sheet" style="margin-left : 320px;" onclick="javascript:window.print();" />
        </div>
        <div class="DIV_TH" style="width:860px;">
            <div class="DIV_TD" style="width:130px;">order Name</div>
            <div class="DIV_TD" style="width:65px;">order id</div>
            <div class="DIV_TD" style="width:55px;">Item Id</div>
            <div class="DIV_TD" style="width:85px;">status</div>
            <div class="DIV_TD" style="width:125px;">item name</div>
            <div class="DIV_TD" style="width:70px;">quantity</div>
	    <div class="DIV_TD" style="width:100px;">location</div>
            <div class="DIV_TD" style="width:190px;">assignment date</div>
        </div>
        
        {if $itemResult }
        
        {section name=item loop=$itemResult}
         <div class="DIV_TR" style="width:860px;">
            <div class="DIV_TR_TD" style="width:130px;"><p>{$itemResult[item].order_name}</p></div>
            <div class="DIV_TR_TD" style="width:65px;"><p>{$itemResult[item].orderid}</p></div>
            <div class="DIV_TR_TD" style="width:55px;"><p>{$itemResult[item].itemid}</p></div>
            <div class="DIV_TR_TD" style="width:85px;"><p>
            {assign var=i_status value=$itemResult[item].item_status}
                
            {if $i_status == 'N'} New {/if}
            {if $i_status == 'A'} Assigned {/if}
            {if $i_status == 'D'} Done {/if}
            {if $i_status == 'F'} Failed {/if}
            {if $i_status == 'H'} On Hold {/if}
            {if $i_status == 'S'} Sent Outside {/if}
            </p>
            </div>
            <div class="DIV_TR_TD" style="width:125px;"><p>{$itemResult[item].stylename}</p></div>
            <div class="DIV_TR_TD" style="width:70px;"><p>{$itemResult[item].amount}</p></div>
	    <div class="DIV_TR_TD" style="width:100px;"><p>{$itemResult[item].location_name}</p></div>
            <div class="DIV_TR_TD" style="width:190px;">{$itemResult[item].ass_date}</div>
         </div>
         
        {/section}
        <div class="ITEM_SEARCH_BUTTON_DIV" style="width:725px;">
            <input type="button" value="print sheet" style="margin-left : 320px;" onclick="javascript:window.print();" />
        </div>
        {else}
        <div class="DIV_TR" style="width:630px;">
            <div class="DIV_TR_TD" style="width:630px;"><strong>task has not been assigned</strong></div>
        </div>
        {/if}
        
        </div>
</div>
        
{/if}
    
