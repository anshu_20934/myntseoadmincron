<div style="width:100%;">
<p style="background-color:#f2f2f2; color:#000; border:solid 1px #385d8a; width:20%;font-weight:bold; font-size:1.4em;">Inventory Overview {$type_cond}</p>

<div style="width:100%;">
	<form method="post" action="inventory_management.php">
	<input type="hidden" name="inv_mode" value="inv_search" />
	
		<table style="border:solid 1px #385d8a; width:100%;">
			<tr>
				<td style="text-align:left;">Product Type</td>
				<td style="text-align:left;">
					<select name="pr_type[]" size="4" multiple="multiple" style="width:300px;">
					<option value="all">All</option>
					{foreach from=$types key=key item=item}
						<option name="{$key}" value="{$key}">{$item}</option>
					{/foreach}
					</select>
				</td>
				<td style="text-align:left;">Product Style</td>
				<td style="text-align:left;">
					<select name="style[]" size="4" multiple="multiple" style="width:300px;">
					<option value="all">All</option>
					{foreach from=$styles key=key item=item}
						<option value="{$key}">{$item}</option>
					{/foreach}
					</select>
				</td>
			</tr>
			<tr>
				<td style="text-align:left;">SKU#</td>
				<td style="text-align:left;">
					<select name="sku[]" size="3" multiple="multiple" style="width:300px;">
					{foreach from=$skuresults key=key item=item}
						<option value="{$key}">{$item}</option>
					{/foreach}
					</select>
				</td>
				<td style="text-align:right;"><input type="submit" value="Search" style="background-color:#f2f2f2; color:#000;"></td>
			</tr>
		</table>
	</form>
</div>
<div style="width:100%;">
	
	<table style="border:solid 1px white; width:100%;">
	<tr >
		
		<td>
			<input type="button" value="Register new SKU" onClick="javascript:window.open('register_sku.php','null', 'menubar=0,resizable=1,toolbar=0,width=800,height=600')">
		</td>
	</tr>
		<tr style="background-color:#4f81bd;">
			<td style="color:white; font-weight:bold; font-size:13px;">Type</td>
			<td style="color:white; font-weight:bold; font-size:13px;">Product Style</td>
			<td style="color:white; font-weight:bold; font-size:13px;">Options</td>
			<td style="color:white; font-weight:bold; font-size:13px;">SKU</td>
			<td style="color:white; font-weight:bold; font-size:13px;">Status</td>
			<td style="color:white; font-weight:bold; font-size:13px;">Threshold Count</td>
			<td style="color:white; font-weight:bold; font-size:13px;">Current Count</td>
			<td style="color:white; font-weight:bold; font-size:13px;">Unit Price</td>
			<td style="color:white; font-weight:bold; font-size:13px;">Current Value</td>
			<td style="color:white; font-weight:bold; font-size:13px;">Action</td>
		</tr>
		{foreach from=$searchresult item=sku}
		<tr bgcolor="{cycle values="#d0d8e8,#e9edf4"}">
			<td>{$sku.type}</td>
			<td>{$sku.style}</td>
			<td>{$sku.sku_options}</td>
			<td><a href="#"  onClick="javascript:window.open('edit_sku_details.php?skuid={$sku.id}','null', 'menubar=0,resizable=1,toolbar=0,width=900,height=900')">{$sku.sku_name}</a></td>
			<td>{$sku.sku_status}</td>
			<td>{$sku.sku_thresh}</td>
			<td>{$sku.sku_count}</td>
			<td>{$sku.sku_unit}</td>
			<td>{$sku.sku_value}</td>
			<td><a href="#"  onClick="javascript:window.open('add_sku_inventory.php?skuid={$sku.id}','null', 'menubar=0,resizable=1,toolbar=0,width=800,height=500')">Add</a>&nbsp;&nbsp;<a href="#"  onClick="javascript:window.open('reduce_sku_inventory.php?skuid={$sku.id}','null', 'menubar=0,resizable=1,toolbar=0,width=800,height=500')">Reduce</a></td>
		</tr>
		{/foreach}
		<tr style="background-color:#d0d8e8; color:#f00;">
			<td colspan=8>Total value</td>
			<td style="color:red; font-weight:bold; font-size:14px;">{$total} Rs</td>
			<td></td>
		</tr>
	</table>
</div>
</div>