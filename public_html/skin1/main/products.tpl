{* $Id: products.tpl,v 1.42.2.1 2006/05/15 12:24:08 max Exp $ *}
{if $products ne ""}
{include file="main/check_all_row.tpl" style="line-height: 170%;" form="processproductform" prefix="productids"}

{literal}
<script type="text/javascript">
var txt_pvariant_edit_note_list = "{$lng.txt_pvariant_edit_note_list|escape:javascript|replace:'"':'\"'}";
function pvAlert(obj) {
	if (obj.pvAlertFlag)
		return false;

	alert(txt_pvariant_edit_note_list);
	obj.pvAlertFlag = true;
	return true;
}
</script>
{/literal}

{literal}
<script type="text/javascript">
function getHTTPObject(){
	if(window.XMLHttpRequest){
		http=new XMLHttpRequest();}
	else if(window.ActiveXObject){
		http=new ActiveXObject("Microsoft.XMLHTTP");}
	return http;
}

function updaterating(productid,rating){
	var http = new getHTTPObject();
	//var usertype = 'C';
	var params = "?productid="+productid+"&rating="+rating;
	var SizeSelectionURL = "../include/updaterating.php"+params;
	//http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	//http.setRequestHeader("Content-length", params.length);
	//http.setRequestHeader("Connection", "close");
	
	http.open("GET", SizeSelectionURL, true);
	http.onreadystatechange = function(){
   		if(http.readyState==4){
    		if(http.status==200){
            	var result=http.responseText;
//				alert(result);
			}
		}    	
    }
    http.send(null);
}

</script>
{/literal}

<table cellpadding="2" cellspacing="1" width="100%">

{if $main eq "category_products"}
{assign var="url_to" value="category_products.php?cat=`$cat`&page=`$navpage`"}
{else}
{assign var="url_to" value="search.php?mode=search&page=`$navpage`"}
{/if}

<tr class="TableHead">
	<td width="5">&nbsp;</td>
	<td nowrap="nowrap">{if $search_prefilled.sort_field eq "productcode"}{include file="buttons/sort_pointer.tpl" dir=$search_prefilled.sort_direction}&nbsp;{/if}<a href="{$url_to|amp}&amp;sort=sku&amp;sort_direction={if $search_prefilled.sort_field eq "sku"}{if $search_prefilled.sort_direction eq 1}0{else}1{/if}{else}{$search_prefilled.sort_direction}{/if}">{$lng.lbl_sku}</a></td>
	<td width="100%" nowrap="nowrap">{if $search_prefilled.sort_field eq "title"}{include file="buttons/sort_pointer.tpl" dir=$search_prefilled.sort_direction}&nbsp;{/if}<a href="{$url_to|amp}&amp;sort=title&amp;sort_direction={if $search_prefilled.sort_field eq "title"}{if $search_prefilled.sort_direction eq 1}0{else}1{/if}{else}{$search_prefilled.sort_direction}{/if}">{$lng.lbl_product}</a></td>
<!----------- display style header -->
<td nowrap="nowrap">Product Image</td>
<td width="10%" nowrap="nowrap">{if $search_prefilled.sort_field eq "style"}{include file="buttons/sort_pointer.tpl" dir=$search_prefilled.sort_direction}&nbsp;{/if}<a href="{$url_to|amp}&amp;sort=style&amp;sort_direction={if $search_prefilled.sort_field eq "style"}{if $search_prefilled.sort_direction eq 1}0{else}1{/if}{else}{$search_prefilled.sort_direction}{/if}">{$lng.lbl_productstyle}</a></td>
<!--  end -->

<td width="10%" nowrap="nowrap">{if $search_prefilled.sort_field eq "qtysold"}{include file="buttons/sort_pointer.tpl" dir=$search_prefilled.sort_direction}&nbsp;{/if}<a href="{$url_to|amp}&amp;sort=qtysold&amp;sort_direction={if $search_prefilled.sort_field eq "qtysold"}{if $search_prefilled.sort_direction eq 1}0{else}1{/if}{else}{$search_prefilled.sort_direction}{/if}">{$lng.lbl_qtysold}/<br>Sales Stats</a></td>

	<td nowrap="nowrap">{if $search_prefilled.sort_field eq "status"}{include file="buttons/sort_pointer.tpl" dir=$search_prefilled.sort_direction}&nbsp;{/if}<a href="{$url_to|amp}&amp;sort=status&amp;sort_direction={if $search_prefilled.sort_field eq "status"}{if $search_prefilled.sort_direction eq 1}0{else}1{/if}{else}{$search_prefilled.sort_direction}{/if}">{$lng.lbl_status}</a></td>
{if $main eq "category_products"}
	<td nowrap="nowrap">{if $search_prefilled.sort_field eq "orderby"}{include file="buttons/sort_pointer.tpl" dir=$search_prefilled.sort_direction}&nbsp;{/if}<a href="{$url_to|amp}&amp;sort=orderby&amp;sort_direction={if $search_prefilled.sort_field eq "orderby"}{if $search_prefilled.sort_direction eq 1}0{else}1{/if}{else}{$search_prefilled.sort_direction}{/if}">{$lng.lbl_pos}</a></td>
{/if}

	<td nowrap="nowrap">Myntra Rating</td>
	<!-- header for sales -->
	<td nowrap="nowrap">{if $search_prefilled.sort_field eq "date"}{include file="buttons/sort_pointer.tpl" dir=$search_prefilled.sort_direction}&nbsp;{/if}<a href="{$url_to|amp}&amp;sort=add_date&amp;sort_direction={if $search_prefilled.sort_field eq "add_date"}{if $search_prefilled.sort_direction eq 1}0{else}1{/if}{else}{$search_prefilled.sort_direction}{/if}">Add Date{*$lng.lbl_price*}</a></td>

</tr>

{section name=prod loop=$products}

<tr{cycle values=', class="TableSubHead"'}>
	<td width="5"><input type="checkbox" name="productids[{$products[prod].productid}]" /></td>
	<td><a href="product_modify.php?productid={$products[prod].productid}{if $navpage}&page={$navpage}{/if}">{$products[prod].productcode}</a></td>
	<td width="100%">{if $products[prod].main eq "Y" or $main ne "category_products"}<b>{/if}<a href="product_modify.php?productid={$products[prod].productid}{if $navpage}&page={$navpage}{/if}">{$products[prod].product|truncate:35:"...":false}</a>{if $products[prod].main eq "Y" or $main ne "category_products"}</b>{/if}</td>
	<td width="50%"><img src='{$products[prod].image_portal_t}' ></td>
	<td width="50%">{$products[prod].stylename}</td>
	<td >{$products[prod].quantity_sold}</td>


	{if $products[prod].statusid eq "1008"}<td><a href="product_modify.php?productid={$products[prod].productid}{if $navpage}&page={$navpage}{/if}">Approved</a></td>{/if}
	{if $products[prod].statusid eq "1002"}<td><a href="product_modify.php?productid={$products[prod].productid}{if $navpage}&page={$navpage}{/if}">Pending</a></td>{/if}
	{if $products[prod].statusid eq "1009"}<td><a href="product_modify.php?productid={$products[prod].productid}{if $navpage}&page={$navpage}{/if}">Rejected</a></td>{/if}
	{if $products[prod].statusid eq "1010"}<td><a href="product_modify.php?productid={$products[prod].productid}{if $navpage}&page={$navpage}{/if}">Retired</a></td>{/if}

{if $main eq "category_products"}
	<td><input type="text" size="9" maxlength="10" name="posted_data[{$products[prod].productid}][orderby]" value="{$products[prod].orderby}" /></td>
{/if}
	<!--<td align="center"><input type="text" size="9" maxlength="10" name="posted_data[{$products[prod].productid}][avail]" value="{$products[prod].avail}"{if $products[prod].is_variants eq 'Y'} readonly="readonly" onclick="javascript: pvAlert(this);"{/if} /></td>
	<td><input type="text" size="9" maxlength="15" name="posted_data[{$products[prod].productid}][price]" value="{$products[prod].price|formatprice}"{if $products[prod].is_variants eq 'Y'} readonly="readonly" onclick="javascript: pvAlert(this);"{/if} /></td>
	-->
	<td>
		<div id="{$products[prod].productid}">
		
			<select name="myntrarating{$products[prod].productid}" onchange="javascript:updaterating('{$products[prod].productid}',this.value);">
				{section name=rating loop=$arr_rating}
					{if $arr_rating[rating] eq $products[prod].myntra_rating}
					<option value="{$products[prod].myntra_rating}" selected>{$products[prod].myntra_rating}</option>
					{else}
					<option value="{$arr_rating[rating]}" >{$arr_rating[rating]}</option>
					{/if}	
				{/section}
			</select>			
		</div>
	</td>
	<td>{$products[prod].add_date|date_format:$config.Appearance.date_format}</td>

</tr>

{/section}

</table>
{/if}
