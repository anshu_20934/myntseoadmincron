{* $Id: history_order.tpl,v 1.83.2.6 2006/06/27 08:08:06 max Exp $ *}
{include file="page_title.tpl" title=$lng.lbl_order_details_label}
<base href="{$http_location}"> 
{$lng.txt_order_details_top_text}

<script type="text/javascript">
    var orderId = "{$orderid}";
    var countries = '{$countries}';
    var states = '{$states}';
    var oldstatus = '{$order.status}';
</script>

<p align="center"><font color="red" size="30">Page disabled! Please contact CRM team. Mail to crmoncall@myntra.com</font></p>
{*
<br /><br />h
{if $invalidorder and $invalidorder eq 'true'}
	<div style="clear:both;">
		<span style="color: red;font-size:16px;">Order id - {$orderid} is invalid!!!!</span>
	</div>
	<br/><br/>
{/if}
{if $usertype eq 'A' && $is_merchant_password ne 'Y' && $config.Security.blowfish_enabled eq 'Y'}
	{capture name=dialog}
	<form action="{$catalogs.admin}/merchant_password.php" method="post" name="mpasswordform">
		<input type="hidden" name="orderid" value="{$orderid}" />
		{$lng.txt_enter_merchant_password_note}
		<br /><br />
		<table>
		<tr>
			<td><font class="VertMenuItems">{$lng.lbl_merchant_password}</font></td>
			<td><input type="password" name="mpassword" size="16" /></td>
			<td><input type="submit" value="{$lng.lbl_enter_mpassword|escape}" /></td>
		</tr>
		</table>
	</form>
	{/capture}
	{include file="dialog.tpl" title=$lng.lbl_enter_merchant_password content=$smarty.capture.dialog extra='width="100%"'}
	<br />
{/if}

{capture name=dialog}
<table width="100%" style="font-family: georgia,timesNewRoman,serif;">
	<tr>
		<td valign="top">
            {if $usertype ne "C"}
            	{if $order_type eq 'gift_order'}
            		<div style="float:left; width: 48%;">
					    <div style="font-weight:bold; margin:10px 3px;">Order No: {$order.orderid} </div>
	                    <div align="left" style="margin:10px 3px;">Order Created from Server: {$order.request_server}</div>
	                    <div align="left" style="margin:10px 3px;">Order Queued from Server: {$order.response_server}</div>
					</div>
					<div style="float:left; width: 48%;">
						<div align="left" style="margin:10px 3px;">Order Date: {$order.created_on|date_format:$config.Appearance.datetime_format}</div>
						<div align="left" style="margin:10px 3px;">Updated Date: {if $order.updated_on} {$order.updated_on|date_format:$config.Appearance.datetime_format}{/if}</div>
						<div align="left" style="margin:10px 3px;">Activated Date: {if $order.activated_date} {$order.activated_date|date_format:$config.Appearance.datetime_format}{/if}</div>
						<div align="left" style="margin:10px 3px;">Last Email Sent Date: {if $order.email_last_sent} {$order.email_last_sent|date_format:$config.Appearance.datetime_format}{/if}</div>
					</div>
					<br/> <br/><br/> <br/><br/> <br/>
            	{else}
	                <div style="float:left; width: 48%;">
					    <div style="font-weight:bold; margin:10px 3px;">Order No: {$order.orderid} </div>
					    <div style="font-weight:bold; margin:10px 3px;">Warehouse: {$warehouse}</div>
					    <div align="left" style="margin:10px 3px;">Expected Delivery Promise Date: {$expected_delivery_date}</div>
					    <div align="left" style="margin:10px 3px;">Actual Delivery Promise Date: {$actual_delivery_date}</div>
	                    <div align="left" style="margin:10px 3px;">Order Created from Server: {$order.request_server}</div>
	                    <div align="left" style="margin:10px 3px;">Order Queued from Server: {$order.response_server}</div>
	                    <!--div align="left" style="margin:10px 3px;">Express Promise Date: {$express_delivery_date}</div-->
					</div>
					<div style="float:left; width: 48%;">
						<div align="left" style="margin:10px 3px;">Order Date: {$order.date|date_format:$config.Appearance.datetime_format}</div>
						<div align="left" style="margin:10px 3px;">Order Queued Date: {if $order.queueddate} {$order.queueddate|date_format:$config.Appearance.datetime_format} {/if}</div>
						<div align="left" style="margin:10px 3px;">Order Packed Date: {if $order.packeddate} {$order.packeddate|date_format:$config.Appearance.datetime_format}{/if}</div>
						<div align="left" style="margin:10px 3px;">Order Shipped Date: {if $order.shippeddate} {$order.shippeddate|date_format:$config.Appearance.datetime_format}{/if}</div>
						<div align="left" style="margin:10px 3px;">Order Delivered Date: {if $order.delivereddate} {$order.delivereddate|date_format:$config.Appearance.datetime_format}{/if}</div>
						<div align="left" style="margin:10px 3px;">{$lng.lbl_completion_date}: {if $order.completeddate} {$order.completeddate|date_format:$config.Appearance.datetime_format}{/if}</div>
					</div>
				{/if}
			{/if}
			<table cellspacing="1" cellpadding="2" class="ButtonsRow">
				<tr>
					{if $usertype eq "P"}
						<td class="ButtonsRow">{include file="buttons/button.tpl" button_title=$lng.lbl_print_order target="_blank" href="order.php?orderid=`$order.orderid`&mode=printable"}</td>
					{/if}

					{if $active_modules.RMA ne ''}
						{if ($usertype eq  'C' || $usertype eq 'A' || ($usertype eq 'P' && $active_modules.Simple_Mode)) && $return_products ne ''}
							<td class="ButtonsRow">{include file="buttons/button.tpl" button_title=$lng.lbl_create_return href="#returns"}</td>
						{/if}
						{if ($usertype eq  'C' || $usertype eq 'A' || ($usertype eq 'P' && $active_modules.Simple_Mode)) && $order.is_returns}
							<td class="ButtonsRow">{include file="buttons/button.tpl" button_title=$lng.lbl_order_returns href="returns.php?mode=search&search[orderid]=`$order.orderid`"}</td>
						{/if}
					{/if}

					{if $active_modules.Shipping_Label_Generator ne '' && ($usertype eq 'A' || ($usertype eq 'P' && $active_modules.Simple_Mode))}
						<td class="ButtonsRow">{include file="buttons/button.tpl" button_title=$lng.lbl_shipping_label href="generator.php?orderid=`$orderid`"}</td>
					{/if}

					{if $order_type neq 'gift_order'}
                                            {if $special_pincode neq 'true'}
						<td class="ButtonsRow">
                            {*
						   <input type="button" onClick="javascript:window.open(http_loc+'/order_invoice_new.php?orderids={$order.orderid}&type=admin&copies=1','null', 'menubar=0,resizable=1,toolbar=0,width=800,height=500')" {if $order.status eq "F" } disabled {/if} value="Print Invoice" name="Print Invoice"/>
						   *}{*{*
						</td>
                                            {else}
						<td class="ButtonsRow">
                            {*
						   <input type="button"
                                                          {if $skipSpecialPincodeRestriction neq 'true' and  ($order.status eq "PK" or $order.status eq "WP") and $order.courier_service eq "ML"}
                                                              onClick="javascript:window.alert('This order belongs to a special zipcode. Invoice printing is possible only at the DC. Please print shipping label for this order.')"
                                                          {else}
                                                              onClick="javascript:window.open(http_loc+'/order_invoice_new.php?orderids={$order.orderid}&type=admin&invoiceType=special','null', 'menubar=0,resizable=1,toolbar=0,width=800,height=500')"
                                                          {/if}
                                                          {if $order.status eq "F" } disabled {/if} value="Print Invoice" name="Print Invoice"/>
                            *}{*{*
						</td>
                                            {/if}
                                            <td class="ButtonsRow">
                                                {*
                                                <input type="button" onClick="javascript:window.open(http_loc+'/order_invoice_new.php?orderids={$order.orderid}&type=admin&invoiceType=finance','null', 'menubar=0,resizable=1,toolbar=0,width=800,height=500')" {if $order.status eq "F" } disabled {/if} value="Finance Invoice" name="Finance Invoice"/>
                                                *}{*{*
                                            </td>
                                            {if $special_pincode eq 'true' and $skipSpecialPincodeRestriction neq 'true' }
                                                <td class="ButtonsRow">
                                                    {*
                                                    <input type="button" onClick="javascript:window.open(http_loc+'/shipping_label.php?orderid={$order.orderid}&type=special','null','scrollbars=1,width=900,height=900,resizable=1')" {if $order.status eq "F" } disabled {/if} value="Shipping Label" name="Shipping Label"/>
                                                    *}{*{*
                                                </td>
                                            {/if}
					{/if}
					<!--
					<td class="ButtonsRow"><a href="../orderprocess_sheet.php?orderid={$order.orderid}&type=admin" target="_blank">PDF Order Processing Sheet</a></td>
					<td class="ButtonsRow"><a href="../orderprocess_sheet.php?orderid={$order.orderid}&type=admin&format=html" target="_blank">HTML Order Processing Sheet</a></td>
					-->
					{if ($usertype eq "A" or ($usertype eq "P" and $active_modules.Simple_Mode)) and $active_modules.Advanced_Order_Management}
						<td class="ButtonsRow">{include file="buttons/button.tpl" button_title=$lng.lbl_modify href="order.php?orderid=`$order.orderid`&mode=edit"}</td>
					{/if}
				</tr>
			</table>
			<br/><br/>

			<table>
				{if $curr_ordertype eq 'ex' }
					<tr><td>This order is placed as an exchange for Order : <a href="/admin/order.php?orderid={$parent_orderid}">{$parent_orderid}</a></td></tr>
				{/if}
			</table>

			<table>
				{if $group_main_orderid ne '' and $group_main_orderid ne 0 }
					<tr><td>This order is a part of Order : <a href="/admin/order.php?orderid={$group_main_orderid}">{$group_main_orderid}</a></td></tr>
				{/if}
				{if $group_sub_orderids }
					<tr><td>
							Some of the items of this order have been moved to following orders :
							{section name=index loop=$group_sub_orderids}
								<a href="/admin/order.php?orderid={$group_sub_orderids[index]}">{$group_sub_orderids[index]}</a> &nbsp;
							{/section}
						</td>
					</tr>
				{/if}
			</table>

			{if $usertype eq "C"}
				<hr />
				{include file="mail/html/order_invoice.tpl" is_nomail='Y'}
			{else}
				{if $order_type eq 'gift_order'}
					{include file="main/giftorder_info_new.tpl"}
				{else}
					{include file="main/order_info_new.tpl"}
				{/if}
			{/if}
		</td>
	</tr>

	<tr>
		<td height="1" valign="top">
		<script type="text/javascript">
		<!--
		var details_mode = false;
		var details_fields_labels = new Object();
		{foreach from=$order_details_fields_labels key=dfield item=dlabel}
		details_fields_labels["{$dfield|escape:javascript}"] = "{$dlabel|escape:javascript}";
		{/foreach}
		-->
		</script>
		{include file="main/include_js.tpl" src="main/history_order.js"}
		{include file="main/include_js.tpl" src="/admin/main/order_details.js"}

		<form action="/admin/order.php?orderid={$order.orderid}" method="post">
			{if $usertype eq "A" or ($usertype eq "P" and $active_modules.Simple_Mode)}
				{if $order.extra.ip ne ''}
					{$lng.lbl_ip_address}: {$order.extra.ip}<br />
					{if $active_modules.Stop_List ne ''}
						{if $order.blocked eq 'Y'}
						<font class="Star">{$lng.lbl_ip_address_blocked}</font><br />
						{else}
						<input type="button" value="{$lng.lbl_block_ip_address|escape}" onclick="javascript: self.location='order.php?mode=block_ip&amp;orderid={$orderid}'" />
						{/if}
					{/if}
				{/if}

				{if $active_modules.Anti_Fraud ne ''}
				<input type="button" value="{$lng.lbl_af_lookup_address|escape}" onclick="javascript: window.open('{$catalogs.admin}/anti_fraud.php?mode=popup&amp;ip={$order.extra.ip}&amp;proxy_ip={$order.extra.proxy_ip}','AFLOOKUP','width=600,height=460,toolbar=no,status=no,scrollbars=yes,resizable=no,menubar=no,location=no,direction=no');" />
				{/if}
			{/if}

			</br>
			{if $rto_queued == "true"}
				{include file="main/subheader.tpl" title="Returned shipment (RTO)"}
				This order shipment has been returned to origin and is now in <label style="font-weight: bold;">{$rto_state}</label> state. Click <a style="font-weight: bold;" href="/admin/old_returns_tracking/old_rto_tracking.php?order_id={$orderid}" target="_blank">here</a> to view the order entry on the RTO tracker.
			{/if}

			{if $active_modules.Special_Offers ne "" && ($usertype eq "A" or $usertype eq "P")}
				<br /><br /><br />
				{include file="modules/Special_Offers/order_extra_data.tpl" data=$order.extra}
			{/if}

			{if ($usertype eq "A" or ($usertype eq "P" and $active_modules.Simple_Mode)) && $active_modules.Anti_Fraud}
				<br /><br /><br />
				{include file="modules/Anti_Fraud/extra_data.tpl" data=$order.extra.Anti_Fraud}
			{/if}

			{if ($usertype eq 'A' || ($usertype eq 'P' && $active_modules.Simple_Mode)) && $order.is_egood ne '' && $active_modules.Egoods}
				<input type="button" value="{if $order.is_egood eq 'Y'}{$lng.lbl_prolong_ttl}{else}{$lng.lbl_regenerate_ttl}{/if}" onclick="javascript: self.location='order.php?mode=prolong_ttl&amp;orderid={$orderid}'" /><br />
				{$lng.txt_prolong_ttl}
			{/if}

			<input type="hidden" name="mode" value="status_change" />
			<input type="hidden" name="orderid" value="{$order.orderid}" />
		</form>

		{if $usertype eq "P" and $order.status ne "C"}
			<form action="/admin/order.php?orderid={$order.orderid}" method="post">
				<input type="hidden" name="mode" value="complete_order" />
				<input type="submit" value="{$lng.lbl_complete_order|escape}" /><br />
				{$lng.txt_complete_order}
				<input type="hidden" name="orderid" value="{$order.orderid}" />
			</form>
		{/if}

		{if $active_modules.Order_Tracking ne "" and $order.tracking ne ""}
			<br /><br /><br />

			{include file="main/subheader.tpl" title=$lng.lbl_tracking_order}
			{assign var="postal_service" value=$order.shipping|truncate:3:"":true}
			{$lng.lbl_tracking_number}: {$order.tracking}
			<br /><br />

			{if $postal_service eq "UPS"}
				{include file="modules/Order_Tracking/ups.tpl"}
			{elseif $postal_service eq "USP"}
				{include file="modules/Order_Tracking/usps.tpl"}
			{elseif $postal_service eq "Fed"}
				{include file="modules/Order_Tracking/fedex.tpl"}
			{/if}
		{/if}
		</td>
	</tr>

	{if $gift_status neq 'Y'}
		{if $order.status eq "PP"}
		<tr>
			<td valign="top">
				<br/><br/><br/>
				{include file="main/subheader.tpl" title="COD Conversion. Convert Credit Card failed (PP) order into COD order ?"}
				<br/>
				{if $codErrorMessage}
					{$codErrorMessage}
				{else}
				<form id="pptocod_form" action="" method="post">
					<input type="hidden" name="mode" value="pptocodqueued" />
					<input type="hidden" name="orderid" value="{$order.orderid}" />
					<input type="Submit" name="ConvertCOD" value="Change order to COD and Confirm" />
				</form>
				{/if}
			</td>
		</tr>
		{/if}
	{/if}

	{if $order.status eq "PV" || $order.status eq "D"}
	<tr>
		<td valign="top">
			<br/><br/><br/>
			{if $order.status eq "PV"}
				{include file="main/subheader.tpl" title="COD Conversion. Confirm COD order from Pending Verification (PV) to Queued State ?"}
			{else if $order.status eq "D"}
				{include file="main/subheader.tpl" title="COD Conversion. Confirm COD order from Declined (D) to Queued State ?"}
			{/if}
			<br/>
			{if $codErrorMessage}
				{$codErrorMessage}
			{else}
			<form id="pp_to_cod_form" action="{$http_location}/cod_verification.php" method="get">
				<input type="hidden" name="adminconfirmed" value="true">
				<input type="hidden" name="codloginid" value="{$order.login}">
				<input type="hidden" name="codorderid" value={$order.orderid}>
				{if $order.status eq "PV"}
					<input type="hidden" name="confirmmode" value="pvtocodqueued">
				{else if $order.status eq "D"}
					<input type="hidden" name="confirmmode" value="declinedtocodqueued">
				{/if}
				<input type="submit" value="Confirm COD Order">
			</form>
			{/if}
		</td>
	</tr>
	{/if}
</table>
{/capture}

{include file="dialog.tpl" title=$lng.lbl_order_details_label content=$smarty.capture.dialog extra='width="100%"'}

<br/>
{if $order_type neq 'gift_order'}

	<div class="section">
	    <div class="section-head toggleHead">
	        <a onclick="loadSectionData(this, {$order.orderid}, 'wmsitems');" class="divClose" href="javascript: void(0);"><p class="arrow">&nbsp;</p> <p>Issued Items Section</p></a>
	        <div class="clear"></div>
	    </div>
	    <div class="section-details" id="section-details-wmsitems-{$order.orderid}" style="display:none;">
	        <div class="contentPane" id="wmsitems-{$order.orderid}"></div>
	    </div>
	</div>
	{include file="main/order_detail_info.tpl" status=$order.status}

	<div class="section">
	    <div class="section-head toggleHead">
	        <a onclick="loadSectionData(this, {$order.orderid}, 'comments');" class="divClose" href="javascript: void(0);"><p class="arrow">&nbsp;</p> <p>Order Comments Section</p></a>
	    </div>
	    <div class="section-details" id="section-details-comments-{$order.orderid}" style="display:none;">
	        <div class="contentPane" id="comments-{$order.orderid}"></div>
	    </div>
	</div>

	<div class="section">
	    <div class="section-head toggleHead">
	        <a onclick="loadSectionData(this, {$order.orderid}, 'paymentlogs');" class="divClose" href="javascript: void(0);"><p class="arrow">&nbsp;</p> <p>Payment Logs Section</p></a>
	    </div>
	    <div class="section-details" id="section-details-paymentlogs-{$order.orderid}" style="display:none;">
	        <div class="contentPane" id="paymentlogs-{$order.orderid}"></div>
	    </div>
	</div>

	<div class="section">
	    <div class="section-head toggleHead">
	        <a onclick="loadSectionData(this, {$order.orderid}, 'returns');" class="divClose" href="javascript: void(0);"><p class="arrow">&nbsp;</p> <p>Returns Section</p></a>
	    </div>
	    <div class="section-details" id="section-details-returns-{$order.orderid}" style="display:none;">
	        <div class="contentPane" id="returns-{$order.orderid}"></div>
	    </div>
	</div>

	<div class="section">
	    <div class="section-head toggleHead">
	        <a onclick="loadSectionData(this, {$order.orderid}, 'sr');" class="divClose" href="javascript: void(0);"><p class="arrow">&nbsp;</p> <p>Service Requests Section</p></a>
	    </div>
	    <div class="section-details" id="section-details-sr-{$order.orderid}" style="display:none;">
	        <div class="contentPane" id="sr-{$order.orderid}"></div>
	    </div>
	</div>

	<div class="section">
	    <div class="section-head toggleHead">
	        <a onclick="loadSectionData(this, {$order.orderid}, 'ohtrial');" class="divClose" href="javascript: void(0);"><p class="arrow">&nbsp;</p> <p>On Hold Trial Section</p></a>
	    </div>
	    <div class="section-details" id="section-details-ohtrial-{$order.orderid}" style="display:none;">
	        <div class="contentPane" id="ohtrial-{$order.orderid}"></div>
	    </div>
	</div>

	<div class="section">
	    <div class="section-head toggleHead">
	        <a onclick="loadSectionData(this, {$order.orderid}, 'tracking');" class="divClose" href="javascript: void(0);"><p class="arrow">&nbsp;</p> <p>Shipment Tracking Section</p></a>
	    </div>
	    <div class="section-details" id="section-details-tracking-{$order.orderid}" style="display:none;">
	        <div class="contentPane" id="tracking-{$order.orderid}"></div>
	    </div>
	</div>
	{if $order.courier_service eq "ML"}
	    <div class="section">
	        <div class="section-head toggleHead">
	            <a onclick="loadSectionData(this, {$order.orderid}, 'trip');" class="divClose" href="javascript: void(0);"><p class="arrow">&nbsp;</p> <p>Trip Details Section</p></a>
	        </div>
	        <div class="section-details" id="section-details-trip-{$order.orderid}" style="display:none;">
	            <div class="contentPane" id="trip-{$order.orderid}"></div>
	        </div>
	    </div>
	{/if}
{else}
	<div class="section">
	    <div class="section-head toggleHead">
	        <a onclick="loadSectionData(this, {$order.orderid}, 'paymentlogs');" class="divClose" href="javascript: void(0);"><p class="arrow">&nbsp;</p> <p>Payment Logs Section</p></a>
	    </div>
	    <div class="section-details" id="section-details-paymentlogs-{$order.orderid}" style="display:none;">
	        <div class="contentPane" id="paymentlogs-{$order.orderid}"></div>
	    </div>
	</div>

	<div class="section">
	    <div class="section-head toggleHead">
	        <a onclick="loadSectionData(this, {$order.orderid}, 'giftordercomments', {$order.gift_card_id});" class="divClose" href="javascript: void(0);"><p class="arrow">&nbsp;</p> <p>Order Comments Section</p></a>
	    </div>
	    <div class="section-details" id="section-details-giftordercomments-{$order.orderid}" style="display:none;">
	        <div class="contentPane" id="giftordercomments-{$order.orderid}"></div>
	    </div>
	</div>
{/if}
*}
