// $Id: popup_product.js,v 1.1 2005/11/30 13:29:35 max Exp $
function popup_product (field_productid, field_product, query) {
	   	window.open ("popup_product.php?field_productid="+field_productid+"&field_product="+field_product+"&query="+query, "selectproduct", "width=600,height=550,toolbar=no,status=no,scrollbars=yes,resizable=no,menubar=no,location=no,direction=no");
}

function popup_product_shop (field_productid, field_product,field_product_type, query) {
	   	window.open ("popup_producttype.php?field_productid="+field_productid+"&field_product="+field_product+"&field_product_type="+field_product_type+"&query="+query, "selectproduct", "width=600,height=550,toolbar=no,status=no,scrollbars=yes,resizable=no,menubar=no,location=no,direction=no");
}

function popup_all_shops(field_productid, field_product,field_product_type, query)
{
	 	   	window.open ("browseallshops.php?field_productid="+field_productid+"&field_product="+field_product+"&field_product_type="+field_product_type+"&query="+query, "selectproduct", "width=600,height=550,toolbar=no,status=no,scrollbars=yes,resizable=no,menubar=no,location=no,direction=no");
	
}

function uploadWindow(type,id,imageid)
{
	if(type == 'thumb')
	{

		document.getElementById("hrefload").style.visibility = "hidden";
		document.getElementById("hrefload").style.display = "none";
		document.getElementById("reload").style.visibility = "visible";
		document.getElementById("reload").style.display = "";
	}
	if(type == 'detail')
	{
		document.getElementById("hrefload1").style.visibility = "hidden";
		document.getElementById("hrefload1").style.display = "none";
		document.getElementById("detailreload").style.visibility = "visible";
		document.getElementById("detailreload").style.display = "";
	}
	if(type == 'size')
	{
		document.getElementById("hrefload2").style.visibility = "hidden";
		document.getElementById("hrefload2").style.display = "none";
		document.getElementById("sizereload").style.visibility = "visible";
		document.getElementById("sizereload").style.display = "";
	}
	      var url = "changeUpload.php?imgType="+type+"&id="+id+"&imageid="+imageid;
		  window.open(url,"null","width=500,height=300");
}

function uploadWin(type,id)
{
	if(type == 'thumb')
	{

		document.getElementById("hrefload").style.visibility = "hidden";
		document.getElementById("hrefload").style.display = "none";
		document.getElementById("reload").style.visibility = "visible";
		document.getElementById("reload").style.display = "";
	}
	if(type == 'detail')
	{
		document.getElementById("hrefload1").style.visibility = "hidden";
		document.getElementById("hrefload1").style.display = "none";
		document.getElementById("detailreload").style.visibility = "visible";
		document.getElementById("detailreload").style.display = "";
	}
	 
	 if(type == 'otherimg')
	 {
		  var url = "uploadmoreimage.php?imgType="+type+"&id="+id;
		  window.open(url,"null","width=500,height=300");
			
	 }
	 else
	 {
	      var url = "changeUpload.php?imgType="+type+"&id="+id;
		  window.open(url,"null","width=500,height=300");
	 }
}

function validateWinnerPrizes(formObj,formMode)
{
	 if (!formObj)
		return false;
	 if (formObj.tagName != "FORM") {
		if (!formObj.form)
			return false;
		formObj = formObj.form;

	}

	var flag = confirm("Are you sure you wish to continue?");
	if (!flag)
	{
		return false ;
	}


	if (formObj.mode)
		formObj.mode.value = formMode;

	formObj.submit();

}




/* validate product type form fields */

function validateProductType(formObj)
{
	if (!formObj)
		 return false;

	if (formObj.tagName != "FORM") {
		if (!formObj.form)
			return false;
		formObj = formObj.form;

	}
	if (formObj.type_name.value == "" )
	{
		 alert("Please enter  name.");
		  return false;
	}

	if (formObj.type_label.value == "" )
	{
	      alert("Please enter label.");
		  return false;
	}

	 return true;


}

// creating ajax object

/*function confirmWinner(formObj,formMode){


    if(window.XMLHttpRequest){
        req = new XMLHttpRequest();
    }
    else if (window.ActiveXObject){
        req  = new ActiveXObject("Microsoft.XMLHTTP");
    }

    var formurl=document.getElementById("customizationForm").action;

    req.onreadystatechange = function(){
        if (req.readyState == 4)
	{

            document.getElementById("customizedImage123").innerHTML = req.responseText;

        }
    };

    req.open('GET',formurl+"?x="+startX+"&y="+startY+"&w="+width+"&h="+height+"&caId="+caId+"&oId="+oId, true);
    //alert(formurl+"?x="+startX+"&y="+startY+"&w="+width+"&h="+height);
    req.setRequestHeader("Content-Type", "text/xml");
    req.send(null);


}*/

function popup_outer_shop(field_productid, field_product, query)
{
	window.open ("popup_shopproduct.php?field_productid="+field_productid+"&field_product="+field_product+"&query="+query, "selectproduct", "width=600,height=550,toolbar=no,status=no,scrollbars=yes,resizable=no,menubar=no,location=no,direction=no");
}
function popup_theme(field_productid, field_product, query)
{
	window.open ("popup_theme.php?field_productid="+field_productid+"&field_product="+field_product+"&query="+query, "selectproduct", "width=600,height=550,toolbar=no,status=no,scrollbars=yes,resizable=no,menubar=no,location=no,direction=no");
}
function popup_offline_product (custid) 
{
	window.open ("add_offlineproduct.php?custid="+custid, "selectproduct", "width=600,height=550,toolbar=no,status=no,scrollbars=yes,resizable=yes,menubar=no,location=no,direction=no");
}

function popup_corporate_quote(requestid,corporateid,quotenumber) 
{
	if(quotenumber)
		var URL = "corporatecreate_quote.php?requestid="+requestid+"&corporateid="+corporateid+"&quotenumber="+quotenumber;
	else
		var URL = "corporatecreate_quote.php?requestid="+requestid+"&corporateid="+corporateid;

	window.open (URL, "corporatequote", "width=800,height=700,toolbar=no,status=no,scrollbars=yes,resizable=yes,menubar=no,location=no,direction=no");
}

function popup_quote_detail(requestid, quotenumber) 
{
	window.open ("createdquotedetails.php?requestid="+requestid+"&quotenumber="+quotenumber, "corporatequote", "width=800,height=700,toolbar=no,status=no,scrollbars=yes,resizable=yes,menubar=no,location=no,direction=no");
}

function popup_upload_attachment(requestid)
{
	window.open ("uploadrequestattachment.php?requestid="+requestid, "corporatequote", "width=800,height=500,toolbar=no,status=no,scrollbars=yes,resizable=yes,menubar=no,location=no,direction=no");
} 

function viewDesign(styleid, productid, requestid, url)
{
	window.open(url+"/modules/corporate/viewyourdesign.php?requestid="+requestid+"&styleid="+styleid+"&productid="+productid, "corporatequote", "width=800,height=500,toolbar=no,status=no,scrollbars=yes,resizable=yes,menubar=no,location=no,direction=no");
}

function printAssigneeTaskList(assigneeid)
{
	window.open ("printAssigneeTaskList.php?assigneeid=" + assigneeid, "corporatequote", "width=800,height=500,toolbar=no,status=no,scrollbars=yes,resizable=yes,menubar=no,location=no,direction=no");
}

function printLocationTaskList(locationid)
{
    window.open("printLocationTaskList.php?locationid=" + locationid, "corporatequote", "width=800,height=500,toolbar=no,status=no,scrollbars=yes,resizable=yes,menubar=no,location=no,direction=no");
}

function orgGetUploadedImage(type, orgId, url)
{
    window.open("orgGetUploadedImage.php?t=" + type + "&orgId=" + orgId + "&url=" + url, "privateaccount", "width=800,height=500,toolbar=no,status=no,scrollbars=yes,resizable=yes,menubar=no,location=no,direction=no");
}

/* for set  commission  rate*/

var request = null;
function validaterate(obj)
{
	  if(obj.value == '' || isNaN(obj.value))
	  {
		   alert("Enter numeric value");
		   obj.focus()
		   return false;
	  }
	  else
		  return true;

}


function showCommissionon(selObj)
{
	   var selecttext = selObj.value;
       if(selObj.value == "")
	   {
		    alert("Please select the option");
			return false;

	    }

	   if(window.XMLHttpRequest){
		  request = new XMLHttpRequest();
       }
	   else if (window.ActiveXObject){
			  request  = new ActiveXObject("Microsoft.XMLHTTP");
		}
 

    request.onreadystatechange = function(){
	    if (request.readyState == 4)
		{
             var result=request.responseText;
			  if(result != '')
			  {
                   document.getElementById('showtypes').innerHTML = result ;
			  } 
			  else
			  {
				    document.getElementById('showtypes').innerHTML = 'No result found' ;
			  }

			
        }
    };
     
    request.open('GET','admincommissionon.php?type='+selecttext+"&rand="+Math.random(), true);
    request.setRequestHeader("Content-Type", "text/xml");
    request.send(null);
	   
}

function validateInventoryParams(mode)
{
	//javascript: document.optionsform.mode.value = 'updateoptionGroup'; document.optionsform.submit();"
	var ddReceivedate = document.getElementById("received_date");
	var txtInventory = document.getElementById("ivt_qty");
    var txtThreshold = document.getElementById("threshold_qty");	
	if(ddReceivedate.value == "")
	{
		alert("Please enter date");
		ddReceivedate.focus()
		return false
	}
	else if(txtInventory.value == "" ||  isNaN(txtInventory.value) || txtInventory.value == 0)
	{
		alert("Please enter valid inventory count");
		txtInventory.focus()
		return false
		 
	}
	else if(txtThreshold.value == "" ||  isNaN(txtThreshold.value) || txtThreshold.value == 0)
	{
		alert("Please enter valid threshold quantity");
		txtThreshold.focus()
		return false
		
	}
	else if(parseInt(txtThreshold.value) < parseInt(txtInventory.value) )
	{
		 alert("Inventory must be greater than threshold quantity.") 
		 return false
	}
	else
	{     
		  document.optionsform.mode.value = mode;
		  document.optionsform.submit();
		
	}
	
}
// creating ajax object
var req = null;
function getProductTypes(type)
{
	 if(window.XMLHttpRequest){
        req = new XMLHttpRequest();
    }
    else if (window.ActiveXObject){
        req  = new ActiveXObject("Microsoft.XMLHTTP");
    }
 

    req.onreadystatechange = function(){
	    if (req.readyState == 4)
		{
           var result=req.responseText.split(","); 
			if(result.length == 1){
				alert("There is not items in this type.");
			}	
			
			var len = document.getElementById("searchByStyle").options.length;
			if(len > 0)
			{			
				for(i=len-1; i >= 1 ; i--){
				   document.getElementById("searchByStyle").options[i] = null;
				  }
            }
            for(i=0; i < result.length; i++){	
               textValue = result[i].split("|");
			   document.getElementById("searchByStyle").options[i] = new Option(textValue[1],textValue[0]);

			}
			 

        }
    };

    req.open('GET','getstyleoptions.php?type='+type, true);
    req.setRequestHeader("Content-Type", "text/xml");
    req.send(null);
	
}
function validateInventory()
{
	var frmObj = document.inventoryform
	var ddReceivedate = document.getElementById("received_date");
	var txtInventory = document.getElementById("ivt_qty");
	var curQty = document.getElementById("curQty");
	if(ddReceivedate.value == "")
	{
		alert("Please enter date");
		ddReceivedate.focus()
		return false
	}
	else if(txtInventory.value == "" ||  isNaN(txtInventory.value) || txtInventory.value == 0)
	{
		alert("Please enter valid inventory count");
		txtInventory.focus()
		return false
		 
	}
	else if(parseInt(txtInventory.value) > parseInt(curQty.value))
	{
		alert("Please enter Quantity less than current quantity");
		txtInventory.focus()
		return false
	}
	else
	{
	  	 
		frmObj.mode.value = 'modify'
		return true
	}
	 
}

function checkStyle()
{
	var frmObj = document.inventoryform
	var radioBtnObj  = frmObj.type;
	var styleObj  = document.getElementById("searchByStyle");
	var flag = false;
	for (i = radioBtnObj.length - 1; i > -1; i--) 
	 {
		if (radioBtnObj[i].checked) 
		{
			 flag = true ;
			 // break;
	    }
	 }
	 if(!flag)
	 {
	 	 alert("Please select type of product");
	 	 radioBtnObj[0].focus();
	 	 return false;
	 }
	 else if(styleObj.value == 0)
	 {
	 	 alert("Please select product style or consumable product.")
	 	 styleObj.focus();
	 	 return false;
	 }
	 else
	 {
	 	 frmObj.mode.value = 'search'
	 	 frmObj.submit();
	 }
	 
	
}

function validateRejectParams(mode)
{
	//javascript: document.optionsform.mode.value = 'updateoptionGroup'; document.optionsform.submit();"
	var ddReceivedate = document.getElementById("received_date");
	var txtInventory = document.getElementById("ivt_qty");
    var txtThreshold = document.getElementById("threshold_qty");	
	if(ddReceivedate.value == "")
	{
		alert("Please enter date");
		ddReceivedate.focus()
		return false
	}
	else if(txtInventory.value == "" ||  isNaN(txtInventory.value) || txtInventory.value == 0)
	{
		alert("Please enter valid inventory count");
		txtInventory.focus()
		return false
		 
	}
	else if(txtThreshold.value == "" ||  isNaN(txtThreshold.value) || txtThreshold.value == 0)
	{
		alert("Please enter valid threshold quantity");
		txtThreshold.focus()
		return false
		
	}
	else if(parseInt(txtThreshold.value) > parseInt(txtInventory.value) )
	{
		 alert("Inventory must be greater than threshold quantity.") 
		 return false
	}
	else
	{     
		 document.optionsform.mode.value = mode;
		  document.optionsform.submit();
		
	}
	
}


	
