{* $Id: register_billing_address.tpl,v 1.27.2.1 2006/06/02 07:03:45 svowl Exp $ *}
{if $is_areas.B eq 'Y'}

{if $hide_header eq ""}


	

{literal}
<script type="text/javascript">
function ship2diffOpen() {
	var obj = document.getElementById('ship2diff');
	var box = document.getElementById('ship_box');
	if (!obj || !box)
		return;

	box.style.display = obj.checked ? "" : "none";
	if (obj.checked && window.start_js_states && document.getElementById('s_country') && localBFamily == 'Opera')
		start_js_states(document.getElementById('b_country'));
}
</script>
{/literal}

{/if}


<div id="ship_box" {if !$ship2diff} style="display: none;"{/if}>
 <div class="subhead"><p>billing address</p></div>




{if $default_fields.b_address.avail eq 'Y'}

<div class="links_980"><div class="legend_980"><p>Address</p> </div>
    <div class="field_980"> <label>
    <textarea id="b_address" name="b_address" class="address" onKeyPress=check_length("b_address"); onKeyDown=check_length("b_address");>{$userinfo.b_address}</textarea>
    <input type=hidden size=2 value=255 name="text_num" id="text_num">
      <!-- <input type="text" id="b_address" name="b_address"  value="{$userinfo.b_address}" /> -->
      {if $reg_error ne "" and $userinfo.b_address eq "" and $default_fields.b_address.required eq 'Y'}{/if}
    </label> </div><div class="clearall"></div>
</div>

{/if}


{*{if $default_fields.b_address_2.avail eq 'Y'}*}

<!-- <div class="links_980"><div class="legend_980"><p>Address (Line2)</p></div>
    <div class="field_980"><label>
      <input  type="text" id="b_address_2" name="b_address_2"  value="{$userinfo.b_address_2}" />
       {if $reg_error ne "" and $userinfo.b_address_2 eq "" and $default_fields.b_address_2.required eq 'Y'}{/if}

    </label> </div><div class="clearall"></div>
</div> -->

{*{/if}*}

{if $default_fields.b_country.avail eq 'Y'}

<div class="links_980"><div class="legend_980"><p>Country</p> </div>
	<div class="field_980">
		<select name="b_country" id="b_country" class="countryselect" onchange="check_zip_code()">
		{section name=country_idx loop=$countries}
			<option value="{$countries[country_idx].country_code}"{if $userinfo.b_country eq $countries[country_idx].country_code} selected="selected"{elseif $countries[country_idx].country_code eq $config.General.default_country and $userinfo.b_country eq ""} selected="selected"{/if}>{$countries[country_idx].country}</option>
		{/section}
		</select>
		{if $reg_error ne "" and $userinfo.b_country eq "" and $default_fields.b_country.required eq 'Y'}{/if}
	</div><div class="clearall"></div>
</div>

{/if}

{if $default_fields.b_state.avail eq 'Y'}

<div class="links_980"><div class="legend_980"><p>State</p> </div>
    <div class="field_980"><label>
      
      {include file="main/states.tpl" states=$states name="b_state" default=$userinfo.b_state default_country=$userinfo.b_country country_name="b_country"}
{if $error eq "b_statecode" || ($reg_error ne "" && $userinfo.b_state eq "" && $default_fields.b_state.required eq 'Y')}{/if}

    </label> </div><div class="clearall"></div>
  </div>

{/if}



{if $default_fields.b_city.avail eq 'Y'}

<div class="links_980"><div class="legend_980"><p>City</p> </div>
    <div class="field_980"><label>
      <input  type="text" id="b_city" name="b_city"  value="{$userinfo.b_city}" />
{if $reg_error ne "" and $userinfo.b_city eq "" and $default_fields.b_city.required eq 'Y'}<font class="Star">&lt;&lt;</font>{/if}

    </label> </div><div class="clearall"></div>
</div>

{/if}



{if $default_fields.b_county.avail eq 'Y' and $config.General.use_counties eq "Y"}


<!-- <div class="links_980"><div class="legend_980"><p>Country<span class="mandatory">*</span></p> </div>
 <div class="field_980"> <label> 
{include file="main/counties.tpl" counties=$counties name="b_county" default=$userinfo.b_county country_name="b_country"}
{if ($reg_error ne "" and $userinfo.b_county eq "" and $default_fields.b_county.required eq 'Y') or $error eq "b_county"}{/if}
</label> </div><div class="clearall"></div>
</div> -->

{/if}





{if $default_fields.b_state.avail eq 'Y' && $default_fields.b_country.avail eq 'Y' && $js_enabled eq 'Y' && $config.General.use_js_states eq 'Y'}
<div class="field_980" style="display: none;">
{include file="main/register_states.tpl" state_name="b_state" country_name="b_country" county_name="b_county" state_value=$userinfo.b_state county_value=$userinfo.b_county}
	</div>
{/if}

{if $default_fields.phone.avail eq 'Y'}

<div class="links_980"><div class="legend_980"><p>Phone</p> </div>
    <div class="field_980"> <label> 
      
      <input type="text" id="b_phone" name="b_phone"  value="{$userinfo.phone}" />
    </label> </div><div class="clearall"></div>
  </div>

{/if}


{if $default_fields.b_zipcode.avail eq 'Y'}

<div class="links_980"><div class="legend_980"><p>Zip/Postal Code</p></div>

  

            <div class="field_980"><label>
              
	     <input  type="text" id="b_zipcode" name="b_zipcode"  value="{$userinfo.b_zipcode}" onchange="check_zip_code()" size="7" maxlength="6"/>
{if $reg_error ne "" and $userinfo.b_zipcode eq "" and $default_fields.b_zipcode.required eq 'Y'}{/if}

          </label></div><div class="clearall"></div>
       </div> 



 
{/if}

<div class="foot"></div>
</div>
 <br>

{include file="main/register_additional_info.tpl" section="B"}
{/if}
