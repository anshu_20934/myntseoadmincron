{* $Id: login_form.tpl,v 1.6 2006/03/13 08:46:35 svowl Exp $ *}
{if $config.Security.use_https_login eq "Y" and $usertype eq "C"}
{assign var="form_url" value=$https_location}
{else}
{assign var="form_url" value=$current_location}
{/if}

{if $referer neq ''}
{assign var="redirect" value=$referer}
{/if}

<form action="/include/login.php" method="post" name="errorform">
<input type="hidden" name="is_remember" value="{$is_remember}" />
<input type="hidden" name="filetype" value="{$pagename}">

         <div class="super"><p>account information</p> </div>
	<div class="head"><p>please login </p></div>
	<div class="links"><div class="legend"><p>{$lng.lbl_login}<span class="mandatory">*</span></p></div>
            <div class="field"><label>
              <input  type="text" name="username"  value="{#default_login#|default:$remember_login:escape:'htmlall'}" />
            </label> </div><div class="clearall"></div>
          </div>

	<div class="links"><div class="legend"><p>{$lng.lbl_password}<span class="mandatory">*</span></p></div>
            <div class="field"><label>
              <input  type="password" name="password"  value="{#default_password#}" />
            </label> </div><div class="clearall"></div>

          </div>



{if $active_modules.Simple_Mode ne "" and $usertype ne "C" and $usertype ne "B"}
<input type="hidden" name="usertype" value="P" />
{else}
<input type="hidden" name="usertype" value="{$usertype}" />
{/if}
<input type="hidden" name="redirect" value="{$redirect}" />
<input type="hidden" name="appreferer" value="{$referer}" />
<input type="hidden" name="encodedUrl" value="{$encodedUrl}" />

<input type="hidden" name="mode" value="login" />



	{*{if $main eq "login_incorrect"}<div class="links"><p><strong>{$lng.err_invalid_login}</strong></p></div>{/if}*}


{if $js_enabled}

<div class="button">
         <input class="submit" type="submit" name="signinButton" value="signin" border="0"  onClick="javascript:document.errorform.submit()">
	 <br><br>
	 {if $encodedUrl}
		New user - register <input class="submit" type="button" name="registerButton" value="here" border="0"  onClick="javascript:window.location.href= 'register.php?redirecturl={$encodedUrl}' ">
	{else}
                New user - register <input class="submit" type="button" name="registerButton" value="here" border="0"  onClick="javascript:window.location.href= 'register.php' ">
	{/if}

</div>
<div class="foot"></div>

{else}

{include file="submit_wo_js.tpl" value=$lng.lbl_submit}
{/if}

</form>

<div class="subhead"></div>

<div class="links">
<p>{$lng.lbl_recover_password}
<input class="submit" type="button" name="submitButton" value="go" border="0"  onClick="javascript:window.location.href='help.php?section=Password_Recovery'"></p>
</div>
<div class="foot"></div>
