{* $Id: affiliates.tpl,v 1.11.2.2 2006/07/11 08:39:27 svowl Exp $ *}
{include file="page_title.tpl" title=$lng.lbl_affiliates_tree}
{$lng.txt_affiliates_tree_note}<br /><br />

<!-- IN THIS SECTION -->

{include file="dialog_tools.tpl"}

<!-- IN THIS SECTION -->
<br />
 
{if $usertype ne 'B'}
{capture name=dialog}
<form action="affiliates.php" method="get">
<table>
<tr>
	<td>{$lng.lbl_partner_as_root}</td>
	<td><select name="affiliate" size="5">
	{foreach from=$partners item=v}
	<option value="{$v.login}"{if $v.login eq $affiliate} selected="selected"{/if}>{$v.firstname} {$v.lastname}</option>
	{/foreach}
	</select></td>
</tr>
<tr>
	<td>&nbsp;</td>
	<td><input type="submit" value="{$lng.lbl_select|strip_tags:false|escape}" /></td>
</tr>
</table>
</form>
{/capture}
{include file="dialog.tpl" content=$smarty.capture.dialog title=$lng.lbl_select extra='width="100%"'}

<br />

{/if}
{if $affiliate || $usertype eq 'B'}
{capture name=dialog}
<b>{$lng.lbl_note}:</b> 
{if $usertype ne 'B'}
{$lng.txt_affiliates_tree_comment_a}
{else}
{$lng.txt_affiliates_tree_comment_b}
{/if}
<br /><br />
<table cellspacing="1" cellpadding="2" width="100%">
<tr class="TableHead">
    <td width="100%">{$lng.lbl_partner}</td>
    <td align="center">{$lng.lbl_commission}</td>
    <td align="center" nowrap="nowrap">{$lng.lbl_affiliate_commission}</td>
</tr>
<tr>
    <td height="19" nowrap="nowrap">&nbsp;{$parent_affiliate.firstname} {$parent_affiliate.lastname}</td>
	<td align="right">{include file="currency.tpl" value=$parent_affiliate.sales|default:0}</td>
	<td align="right">{include file="currency.tpl" value=$parent_affiliate.childs_sales}</td>
</tr>
<tr>
    <td>{include file="main/affiliate_list.tpl" affiliates=$affiliates level=0 type="1"}</td>
    <td>{include file="main/affiliate_list.tpl" affiliates=$affiliates level=0 type="2"}</td>
    <td>{include file="main/affiliate_list.tpl" affiliates=$affiliates level=0 type="3"}</td>
</tr>
</table>
{/capture}
{include file="dialog.tpl" content=$smarty.capture.dialog title=$lng.lbl_affiliates extra='width="100%"'}
{/if}
