/* $Id: history_order.js,v 1.1 2006/03/10 12:05:41 max Exp $ */

function switch_details_mode(edit_mode, cur_btn, old_btn) {
	var dv = document.getElementById("details_view");
	var de = document.getElementById("details_edit");

	if (!dv || !de || edit_mode == details_mode)
		return;

	if (edit_mode) {
		dv.style.display = 'none';
		de.style.display = '';

	} else {
    	var rval = de.value;
	    for (var of in details_fields_labels) {
    	    var re = new RegExp(of, "g");
        	rval = rval.replace(re, details_fields_labels[of]);
	    }
    	dv.value = rval;

		dv.style.display = '';
		de.style.display = 'none';
	}

	details_mode = edit_mode;
	cur_btn.style.fontWeight = 'bold';
	old_btn.style.fontWeight = '';
}

function confirmItemRemoveAction(skuItemCode) {
	var agree=confirm("Are you sure you want to remove Sku Item Piece with code "+skuItemCode+" from this order");
	if(agree) {
		var overlay = new ProcessingOverlay();
	    overlay.show();
		return true;
	}
	else
		return false;
}

var DefaultStatusChangeMessage = "Reason for status change...";

function showOrHideCommentBox(val) {
	var commentBox=document.getElementById("state_change_comment");
	document.getElementById('oh_reasons_div').style.display="none";
	if(val == 'F')
	{
         document.getElementById('state_change_to_rejected').style.display="inline";
         if(document.getElementById('reject_full_order_div')) {
            document.getElementById('reject_full_order_div').style.display="block";
         }
         document.getElementById("state_change_comment").style.display="inline";
         setStateChangeCommentMessage();
	}else if(val == 'OH'){
		document.getElementById('state_change_to_rejected').style.display="none";
		if(document.getElementById('reject_full_order_div')) {
		  document.getElementById('reject_full_order_div').style.display="none";
		}
		document.getElementById('oh_reasons_div').style.display="inline";
		commentBox.style.display="inline";
		var selectedReasonTypeEle = $("#oh_reason option:selected");
		document.getElementById("state_change_comment_msg").value = "";
	}else{
        document.getElementById('state_change_to_rejected').style.display="none";
        if(document.getElementById('reject_full_order_div')) {
            document.getElementById('reject_full_order_div').style.display="none";
        }	    
	    if (val != oldstatus) {
		    commentBox.style.display="inline";
            document.getElementById("state_change_comment_msg").value = window.DefaultStatusChangeMessage;
        } else {
            commentBox.style.display="none";
        }
	}
} 


function commentFieldInFocus(){
	var commentMsg=document.getElementById("state_change_comment_msg");
	if (commentMsg.value == window.DefaultStatusChangeMessage) {
		commentMsg.value='';
	}		
}

function commnetFieldOutOfFocus() {
	var commentElement=document.getElementById("state_change_comment_msg");
	var commentMsg = commentElement.value.replace(/^\s+|\s+$/g, '');
	if (commentMsg.length == 0) {
		commentElement.value = window.DefaultStatusChangeMessage;
	} else {
		commentElement.value = commentMsg;
	}
}


function setStateChangeCommentMessage(){
	var selectedReasonTypeEle = $("#cancellation_type option:selected");
	var reason_type = selectedReasonTypeEle.val();
	var reason = $("#cancellation_reason_"+reason_type+" option:selected").text();
	messageTOSend = selectedReasonTypeEle.text()+" - "+reason;
}

var messageTOSend;

function checkstatus()
{
	var status=document.forms['statusForm'].status;
	var oldstatus=document.forms['statusForm'].oldstatus;
	var orderid=document.forms['statusForm'].orderid.value;

	if (oldstatus.value == 'C' && status.value != 'L') {
		alert("Can't move an order from Completed state to any other state");
		return false;
	}

	if (status.value == 'C' && oldstatus.value != 'DL') {
		alert("Can't move an order to Complete state without it being Delivered");
		return false;
	}

	if (status.value == 'DL') {
		alert("Marking Delivered from here is not allowed");
		return false;
	}
	
	if (status.value == 'SH') {
		alert("Marking Shipped from here is not allowed");
		return false;
	}

	if (status.value == 'D'){
		alert("Can't move an order to Declined manually");
		return false;
	}

   	if (oldstatus.value == 'PV') {
    	alert("You cant change order status from COD Pending Verification to any other status");
		return false;
    }

   	if (oldstatus.value == 'PP') {
    	alert("You cant change order status from Pre-Processed to any other status");
		return false;
    }

    if ((status.value == 'Q') && (oldstatus.value!='OH')) {
    	alert("You cant change the status of an order to Queued");
		return false;
    }  	

    if(status.value == 'C' && document.forms['statusForm'].ccavenue.value == 'N' ){
        alert("Please change the status of Check payment status to Paid");
        document.forms['statusForm'].ccavenue.focus();
        return false;
    }

    if((status.value!=oldstatus.value)&&(status.value=='WP')){
       alert("You cannot change the status of an order to Work in Progress here.\n Please use Item Assignment form to assign items");
       return false;
    }

    if(document.getElementById("payby").value == "cod")
    if((oldstatus.value=='CD' || oldstatus.value=='PP')&&(status.value=='Q')){
       var cod_verified=confirm("Please confirm if you have called the customer and verified the shipping address. \n COD Orders can be queued only after verifying the shipping address.");
         if(!cod_verified)
          return false;
    }

    if((status.value!=oldstatus.value)&&((status.value == 'WP' || status.value == 'C') && oldstatus.value == 'OH')){
        alert("You can change from \"On Hold\" status only to queued status");
        return false;
    }
    
    if(status.value == 'F' && (oldstatus.value == 'SH' || oldstatus.value == 'DL' || oldstatus.value == 'C') ) {
    	alert("Shipped orders cannot be Rejected from here");
    	return false;
    }
    
    if(status.value == 'F') {
    	if($("#reject_full_order_div").length > 0 && !$("input[name='cancel_complete_order']:checked").val()) {
    		alert("Please select what parts you want to cancel");
    		return false;
    	}
    }
    
    var commentElement=document.getElementById("state_change_comment_msg");
	var commentMsg = commentElement.value.replace(/^\s+|\s+$/g, '');

    if (status.value!=oldstatus.value) {
		if ((commentMsg.length == 0) || (commentMsg == window.DefaultStatusChangeMessage)) {
			alert("Please enter a valid comment for order status change.");
			return false;
		}
	}
    
    if (status.value == 'F'&&((commentMsg.length == 0) || (commentMsg == window.DefaultStatusChangeMessage))) {
		alert("Please enter a valid comment for order status change.");
		return false;
	}
	if(status.value == 'F') {
    	document.getElementById("state_change_comment_msg").value = messageTOSend+"\n\n"+commentMsg;
	}
	
	var overlay = new ProcessingOverlay();
    overlay.show();
	
	document.forms['statusForm'].submit();
}

function popupNEW(fnam){
	  var element = document.getElementById(fnam);
	  var calname = "cal"+fnam;
	  var calname = new calendar2(element);
      calname.year_scroll = true;
      calname.time_comp = false;
	  calname.popup();
}
	
function delayNotification(login,orderid){
	window.open("delay_notification.php?userid="+login+"&orderid="+orderid, "delaynotification", "width=1000,height=550,toolbar=no,status=no,scrollbars=yes,resizable=yes,menubar=no,location=no,direction=no");
}
function sendReplacements(login,orderid){
	// TODO Here we have to show the ExtJS dialog box
	showAddReplacementWindow();
	//window.open ("send_replacements.php?orderid="+orderid, "sendreplacements", "width=1000,height=550,toolbar=no,status=no,scrollbars=yes,resizable=yes,menubar=no,location=no,direction=no");
}

function showReasonsOnrejected()
{
	if(document.getElementById('cancellation_type')) {
	    var r_val = document.getElementById('cancellation_type').value;
		$(".cancel_reason").css("display","none");
		$(".cancel_reason").attr('disabled', 'disabled');
		
		$("#cancellation_reason_"+r_val).css("display","inline");
		$("#cancellation_reason_"+r_val).removeAttr('disabled');
	    
	    if(r_val == 'CCR')
	    {
	        var commentBox=document.getElementById("state_change_comment");
			setStateChangeCommentMessage('Customer CR - Delayed delivery cancellation');
	    }
	    
	    if(r_val == 'CCC')
	    {
	        var commentBox=document.getElementById("state_change_comment");
			setStateChangeCommentMessage('CC cancellation - OOS cancellation');
	    }
	}
}

function showShipmentTrackingInfo(){
	if (document.getElementById("trackinginfo").text == "Show Tracking Info") {
		document.getElementById("trackinginfo").innerHTML = "Hide Tracking Info";
		document.getElementById('tracking_details').style.display="inline";				
	} else {
		document.getElementById("trackinginfo").innerHTML = "Show Tracking Info";
		document.getElementById('tracking_details').style.display="none";
	}
	return false;
}

function showSubResolutions(){
	$(".oh_child_res").hide();
	$(".oh_child_res").attr('disabled', 'disabled');
	if(document.getElementById('oh_res') != null){
		oh_val = document.getElementById('oh_res').value;
		$("#oh_sub_res_"+oh_val).show();
		$("#oh_sub_res_"+oh_val).removeAttr('disabled');
	}	
}
function validateOhResForm(){
	if(document.getElementById('oh_res_comment').value == null || document.getElementById('oh_res_comment').value == '' ){
		alert('Please enter a comment');
		return false;
	}
	var overlay = new ProcessingOverlay();
    overlay.show();
	return true;
}

// Hide show sectionsssss in order details page..

function loadSectionData(linkEle, orderid, sectionName, extraData) {
	if($("#"+sectionName+"-"+orderid).html() == "") {
		$("#"+sectionName+"-"+orderid).html("Loading.....");
		
		var postUrl = "/admin/order.php?orderid="+orderid;
		if(extraData && extraData != "")
			postUrl += "&extradata="+extraData;
		
		$.ajax({
			type: "POST",
			url: postUrl,
			data: { 'mode': 'get_data', 'view': sectionName },
			success: function(response)
			{
				var data = response.split("#####");
				$("#"+sectionName+"-"+orderid).html(data[0]);
			}
		});
	}
	
	if($(linkEle).attr("class") == "divClose") {
		$(linkEle).attr("class", "divOpen");
		$("#section-details-"+sectionName+"-"+orderid).slideDown('slow');
	} else {
		$(linkEle).attr("class", "divClose");
		$("#section-details-"+sectionName+"-"+orderid).slideUp('slow');
	}
}

function updateGiftOrder(mode, orderId, giftCartId, extraQuery) {
	var updateUrl = "giftCardsAjaxHelper.php?mode="+mode+"&orderId="+orderId+"&id="+giftCartId;
	if(extraQuery != '') {
		updateUrl += "&"+extraQuery;
	}
	
	var overlay = new ProcessingOverlay();
    overlay.show();
	
	$.ajax({
		type: "GET",
		url: updateUrl,
		success: function(response) {
			response = $.parseJSON(response);
			if(response.status == 'success') {
				alert("Your action has been completed successfully");
				window.location.reload();
			} else {
				alert("Failed to perform action - "+response.message);
				overlay.hide();
			}
		}
	});
}
