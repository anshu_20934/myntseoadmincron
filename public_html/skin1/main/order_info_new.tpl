{include file="../skin1/main/include_js.tpl" src="../skin1/main/calendar2.js"}
<script type="text/javascript">
	var orderStatus = '{$order_status}';
	var paymentMethod = '{$order.payment_method}';
	var orderId = '{$order.orderid}';
	var userName = '{$login}';
	var httpLocation = '{$http_location}';
</script>
<!--<link rel="stylesheet" type="text/css" href="{$http_location}/admin/extjs/resources/css/ext-all.css"/>
<script type="text/javascript" src="{$http_location}/admin/extjs/adapter/ext/ext-base.js"></script>
<script type="text/javascript" src="{$http_location}/admin/extjs/ext-all.js"></script>
<script type="text/javascript" src="{$http_location}/admin/operations/order_replacement.js"></script> -->
<hr/>
<form name="statusForm" action="" method="post">
    <table cellspacing="0" cellpadding="0" width="100%">
        {if $products[prod_num].deleted eq "" and ($active_modules.Product_Configurator eq "" or $products[prod_num].extra_data.pconf.parent eq "")}
            <tr>
                <td colspan="2">
                    {if $statusErrorMsg}
                        <div id="error_div" style="color:red; font-size: 120%; font-weight: bold; text-align:center;">{$statusErrorMsg}</div>
                    {/if}
                    {if $statusSuccessMsg}
                        <div id="success_div" style="color:green; font-size: 120%; font-weight: bold; text-align:center;">{$statusSuccessMsg}</div>
                    {/if}
                </td>
            </tr>
            <tr>
                <td colspan="2" align="right">
                    <!-- <input type="button" name="delay_notification" id="delay_notification" value="Delay notification" onclick="javascript:delayNotification('{$order.login}','{$order.orderid}');"> &nbsp; -->
                    <input type="button" onClick="javascript:window.open(http_loc+'/admin/manageOrderSR.php?orderid={$orderid}&mode=addServieRequest','null', 'menubar=0,resizable=1,toolbar=0,width=800,height=500')" value="Add Service Request" name="Add Service Request"/>
                </td>
            </tr>
            {if $gift_status=='Y'}
	            <tr>
	            	<td>
	                	<p  style="font-size:1.5em;color:#FF0033;background-color:#FFFFCC; text-align: right"><strong>This is a gift Order</strong></p>
	                </td>
	            </tr>
	        {/if}
	        {if $order.ordertype eq 'ex'}
                <tr>
                    <td colspan="2">
                        <p><strong style="font-size:15px;color:#000000;">This is an exchange order</strong></p>
                    </td>
                </tr>
            {/if}
            {if $order.shipping_method eq 'XPRESS'}
                <tr>
                    <td colspan="2">
                        <p><strong style="font-size:15px;color:#000000;">This is a Next Day Delivery order</strong></p>
                    </td>
                </tr>
            {/if}
            {if $order.shipping_method eq 'SDD'}
                <tr>
                    <td colspan="2">
                        <p><strong style="font-size:15px;color:#000000;">This is a Same Day Delivery order</strong></p>
                    </td>
                </tr>
            {/if}

            <tr>
                <td>
                    <p style="font-size:1.5em;color:#FF0033;">
                        <input type="hidden" id="payby" name="payby" value="{$order.payment_method}"></span>
                        <strong>Payment method is
                            {if $order.payment_method eq "chq"}Check Payment
                            {elseif $order.payment_method eq "cod"}COD Payment
                            {elseif $order.payment_method eq "cash"}Cash Payment
                            {elseif $order.payment_method eq "ivr"}IVR
                            {else $order.payment_method eq "P"}Credit Card/ Net Banking
                            {/if}
                        </strong>
                    </p>
                </td>
            </tr>

            {if $order.issues_contact_number}
                <tr>
                    <td colspan="2">
                        <p><strong style="font-size:15px;color:#1310a3;">Contact number for issues::</strong>&nbsp;&nbsp;<strong style="font-size:15px;color:#8e0404;">{$order.issues_contact_number}</strong></p>
                    </td>
                </tr>
            {/if}

            
            {if $usertype ne "C"}
                
                <tr>
	                <td>
	                	<table>
		                	<tr>
			               		<td valign="top">
			                   		<b>Customer :</b> {if $customer.default_fields.firstname}{$customer.firstname}{/if}
					             		{if $customer.default_fields.lastname}{$customer.lastname}{/if}
					       		</td>
		                	</tr>
		                	<tr>
			               		<td valign="top"><b>LoginID :</b> {$order.login}</td>
		                	</tr>
		                	<tr>
			               		<td valign="top"><b>Shipping Email :</b> {$order.s_email}</td>
		                	</tr>
	                	</table>
	                </td>
	                <td>
	                	<table>
	                		<tr>
	                			<td valign="top"><b>Shipping Address</b>:</td>
	                			<td valign="top">   {$order.s_firstname} {$order.s_lastname} <br>
	                								{$order.s_address}, {$order.s_locality}<br>
	                								{$order.s_city}, {$order.s_state} <br>
	                								{$order.s_zipcode}
	                			</td>
	                		</tr>
	                		<tr>
	                			<td valign="top"><b>Shipping Mobile</b>:</td><td valign="top">{$order.mobile}</td>
	                		</tr>
	                	</table>
	                </td>
                </tr>
                
				{if $channel eq 'telesales'}
					<tr>
						<td valign="top"><b>Order Channel :</b> Telesales</td>
					</tr>
					<tr>
						<td valign="top"><b>CC Executive :</b> {$cclogin}</td>
					</tr>
				{/if}
            {/if}
            <tr><td colspan="2">&nbsp;</td></tr>
            <tr>
                <td>&nbsp;</td>
                <td valign="top">
                    <b>{$lng.lbl_courier_service}:</b>
                    <select name="courier_service" id="courier_service" disabled>
                        <option value="0">Select Courier Service</option>
                        {section name="courier" loop=$courier_services}
                            <option value="{$courier_services[courier].code}" {if $order.courier_service==$courier_services[courier].code} selected {/if}>{$courier_services[courier].display_name}</option>
                        {/section}
                    </select>
                </td>
            </tr>
			<tr>
				<td valign="top">
					<b>{$lng.lbl_status}:</b>
					{if $usertype eq "A"}
						{if $order_status == 'OH' && $oh_reason != null && $oh_reason.is_manual_change == 1} 
							 <b>{include file="main/order_status.tpl" status=$order_status mode="static" action=1}</b>
						{else}
						     {if $oh_reason.is_manual_change == 1 && 
						     ($oh_reason.reason_code == 'OPE' || $oh_reason.reason_code == 'RPE' 
                                                                                                                    || $oh_reason.reason_code == 'LPDE' || $oh_reason.reason_code == 'LPAE' || $oh_reason.reason_code == 'LDNA'
				||	$oh_reason.reason_code == 'CVF'	     || $oh_reason.reason_code == 'CDE' || $oh_reason.reason_code == 'CSFE' || $oh_reason.reason_code == 'LSFE')}
                                {include file="main/order_status.tpl" status=$order_status mode="oos-select" name="status" action=1}							     
							 {else}
							     {include file="main/order_status.tpl" status=$order_status mode="select" name="status" action=1}
							 {/if}
						{/if}
					{else}
                        <b>{include file="main/order_status.tpl" status=$order_status mode="static"}</b>
					{/if}
					<br/>
					{if $order_status eq 'OH' }
						<b>OH Reason : </b> {$oh_reason.reason_desc} {if $oh_reason.subreason_desc} - {$oh_reason.subreason_desc} {/if}
                                                {if $oh_reason.reason_code eq 'OPE'}
						      <br>
						      <input type=button class="error_queue_order" id="queue_oos_order" name="queue_oos_order" value="Check Availability and Queue">
						{/if}
						{if $oh_reason.reason_code eq 'RPE'}
						      <br>
						      <input type=button class="error_queue_order" id="queue_oos_release" name="queue_oos_release" value="Check Availability and Queue">
						{/if}
						{if $oh_reason.reason_code eq 'CDE' || $oh_reason.reason_code == 'CSFE'}
							<br>
						      <input type=button class="error_queue_order" id="queue_cberr_order" name="queue_cberr_order" value="Check Cash back and Queue">
						{/if}
					{/if}
				</td>
				<td valign="top">
			       	<b>{$lng.lbl_tracking_number}:</b>
                    <input type="text" id="tracking" name="tracking" value="{$order.tracking}"{if $usertype eq 'C'} readonly="readonly"{/if} />
                    <br/>
                    <!--
                    {if $order_status eq 'WP' or $order_status eq 'SH'}
                        <a href="/admin/order.php?orderid={$orderid}&mode=resetcouriertracking" >Reset Courier Tracking</a>
                    {/if}
                    -->
			    </td>
			</tr>

			<tr colspan="1">
				<td valign="top">
				    <div id="state_change_to_rejected" style="display:none;" >
				    <br>
						<b>Cancellation Type <em style="color:red;">*</em>:</b>
						<select name="cancellation_type" id="cancellation_type" onchange="showReasonsOnrejected(); setStateChangeCommentMessage();">
							{foreach from=$cancellation_types key=typeKey item=typeValue}
							 <option value="{$typeKey}">{$typeValue}</option>
							{/foreach}
						</select>
			            &nbsp;<b>Reason <em style="color:red;">*</em>:</b>
			            {foreach from=$cancellation_types key=typeKey item=typeValue}
			            	<select name="cancellation_reason" id="cancellation_reason_{$typeKey}" class="cancel_reason" onchange="setStateChangeCommentMessage();">
								{foreach from=$cancellation_reasons.$typeKey key=reasonKey item=reasonValue}
								<option value="{$reasonKey}">{$reasonValue}</option>
								{/foreach}
							</select>
			            {/foreach}
					</div><br><br>
					
					<div id="oh_reasons_div" style="display:none;" >
				    <br>
						<b>OH Reason <em style="color:red;">*</em>:</b>
						<select name="oh_reason" id="oh_reason">
							{foreach from=$oh_reasons key=reasonId item=reasonValue}
							<option value="{$reasonId}">{$reasonValue}</option>
							{/foreach}
						</select>
					</div><br><br>
					
					{if $order.orderid eq $order.group_id && $group_sub_orderids|@count > 0}
						<div id="reject_full_order_div" style="display:none;">
						   <input type="radio" name="cancel_complete_order" id="cancel_complete_order_true" value="true"> Cancel all parts (Including orders - {section name=index loop=$group_sub_orderids}{$group_sub_orderids[index]},{/section})
						   <input type="radio" name="cancel_complete_order" id="cancel_complete_order_false" value="false"> Cancel only this part
						   <br><br>
						</div>
					{/if}
					
					<div id="state_change_comment" style="display:none;">
						<b>Comment <em style="color:red;">*</em>: </b>
						<textarea rows="4" cols="50" name="status_change_comment_msg" id="state_change_comment_msg" onfocus="javascript:commentFieldInFocus();" onblur="javascript:commnetFieldOutOfFocus();"></textarea>
					</div>
				</td>
				&nbsp;
			</tr>
            <tr>
                <td> <b>Medium of Sale</b> 
                    <select name="sale_type" id="sale_type">
                        <option value="0">-Select Medium-</option>
                        <option value="DS" {if $saletype eq "DS"}selected{/if} >-Direct Online Sale-</option>
                        <option value="TS" {if $saletype eq "TS"} selected {/if}>-Tele Sale-</option>
                    </select>
                    {if $saletype eq "TS"}
                        <b>Sales Agent : </b>
                        <input type="text" name="sales_agent" id="sales_agent" value="{$saleagent}" />
                    {/if}
                </td>
                <td>
	                {if $order.payment_method eq "cod"}
			        <b>{$lng.lbl_cash_payment}</b>
		            {elseif $order.payment_method eq "cash"}
			        <b>{$lng.lbl_cash_payment}</b>
		            {elseif $order.payment_method eq "chq"}
			        <b>{$lng.lbl_check_payment}</b>
			        {elseif $order.payment_method eq "ivr"}
			        <b>IVR payment status: </b>
		            {else}
			        <b>{$lng.lbl_ccavenue_status}:</b>
		            {/if}
			        <select name="ccavenue" id="ordertype">
	                    <option value="0">-select status-</option>
	                    <option value="Y" {if $order.ccavenue == 'Y'} selected='selected' {/if}>Paid</option>
	                    <option value="N" {if $order.ccavenue == 'N'} selected='selected' {/if}>Not Paid</option>
	                </select>
                </td>
            </tr>
            <tr><td></td></tr>
            <tr>
                <td colspan="2" align="left">
                    {if $usertype eq "A" || $usertype eq "P"}
                        <input type="button" value="{$lng.lbl_apply_changes|escape}" onClick="return checkstatus();"/><br />
                        {if $usertype eq "A"}
                            {$lng.txt_change_order_status}
                        {else}
                            {$lng.txt_apply_changes}
                        {/if}
                    {/if}
                </td>
            </tr>

			<input type="hidden" name="mode" value="status_change" />
			<input type="hidden" name="login" value="{$login}" />
			<input type="hidden" name="oldstatus" value="{$order_status}" />
			<input type="hidden" name="orderid" value="{$order.orderid}" />
        </form>
    {/if}
</table>

{if $order_status eq 'OH' && $oh_reason != null && $oh_reason.is_manual_change == 1 && false}
	<div id= "oh_disp" style="margin-top:10px;border-top:1px solid grey;">
		<div style="padding:10px;"><span style="font-weight:bold; font-size:14px; color:red">Manage OH State</span></div>
		{if $order.orderid eq $order.group_id}
			<form id="oh_disp_form" action="" method="post" onsubmit="return validateOhResForm();">
			<input type="hidden" name="mode" value = "oh_resolution_update" />
			<table>
				<tr>
					<td> <b>No of trials so far :</b> </td>
					<td> {$oh_curr_disp.trial_number} </td>
				</tr>
				<tr>
					<td> <b>Current State :</b></td>
					<td>  {$oh_curr_disp.primary_reason} </td>
				</tr>
				
				<tr>
					<td> <b>Current reason :</b></td>
					<td>  {$oh_curr_disp.reason_display_name} </td>
				</tr>
				
				<tr>
					<td>OH - Resolution</td>
					<td>
						<select name="oh_res" id="oh_res" onchange="showSubResolutions();">
							{foreach from=$oh_master_disp key=reasonKey item=reasonValue}
							<option value="{$reasonKey}">{$reasonValue}</option>
							{/foreach}
						</select>
					</td>
				</tr>
				<tr>
					<td> OH - Sub Resolution</td>
					<td>
						{foreach from=$oh_master_disp key=reasonKey item=reasonValue}
			            	<select name="oh_sub_res" id="oh_sub_res_{$reasonKey}" class="oh_child_res" onchange="setStateChangeCommentMessage();">
								{foreach from=$oh_child_disp.$reasonKey key=reasonKey item=reasonValue}
								<option value="{$reasonKey}">{$reasonValue}</option>
								{/foreach}
							</select>
		            	{/foreach}
					</td>
				</tr>
				<tr> 
					<td> Comments :</td>
					<td><textarea rows="3" cols="30" name="oh_res_comment" id="oh_res_comment"></textarea> </td>
				</tr>
				<tr>
					<td> &nbsp;</td>
					<td style="padding-top:5px; padding-left:150px;"> 
						<input type="submit" name="Submit" value="Submit" />
					</td>
				</tr>
				<input type="hidden" name="orderid" value="{$order.orderid}"/>
				
			</table>
			</form>
		{else}
		   Please use following link to change OH status of this order - <a href="/admin/order.php?orderid={$order.group_id}">Group Order {$order.group_id}</a>
		{/if}
	</div>
{/if}

{literal}
	<script type="text/javascript">
		$(document).ready(function() {
			showReasonsOnrejected(); 
			showSubResolutions();
		});
	</script>
{/literal}
<!--google analytics-->
{if ($order.payment_method eq chq and $cheque_recieved eq true) or ($trackevent eq true)}
    <script type="text/javascript">
        var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
        document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
    </script>

    <script type="text/javascript">
    try {ldelim}
        var pageTracker = _gat._getTracker("{$ga_acc}");
        pageTracker._trackPageview();

        pageTracker._addTrans(
        "{$order.orderid}",                                     // Order ID
        "{$userinfo.b_firstname} {$userinfo.b_lastname}", //cust name
        "{$amountafterdiscount|string_format:'%.2f'}",	  // Total
        "{$vat}",                                     	  // Tax
        "{$shippingRate}",                                // Shipping
        "{$userinfo.s_city}",                             // City
        "{$userinfo.s_state}",                            // State
        "{$userinfo.s_country}"                              // Country
        );
        
        {foreach name=outer k=key item=product from=$productsInCart}
            {foreach key=key item=item from=$product}
                {if $key eq 'productid'}
                {assign var='productId' value=$item}
                {/if}

                {if $key eq 'productStyleName'}
                {assign var='productStyleName' value=$item}
                {/if}

                {if $key eq 'amount'}
                {assign var='quantity' value=$item}
                {/if}

                {if $key eq 'productTypeLabel'}
                {assign var='productTypeLabel' value=$item}
                {/if}

                {if $key eq 'totalPrice'}
                {assign var='totalPrice' value=$item}
                {/if}
            {/foreach}

            pageTracker._addItem(
            "{$order.orderid}",			// Order ID
            "{$productId}",         //pid
            "{$productStyleName}",  //style
            "{$productTypeLabel}",  //type
            "{$totalPrice}",        //Price(qty*productprice)
            "{$quantity}"           // Quantity
            );
        {/foreach}
        pageTracker._trackTrans();
        {rdelim} catch(err) {ldelim}{rdelim}
    </script>
{/if}
<!--google analytics-->
