/*
* URL 
*/
	var URL = "ajax_orderhistory.php?param=";
	var URL1 = "ajax_referalhistory.php?param=";
	/*********** URL for affiliate *****************************/
	var affiliateURL = "modules/affiliates/getAffiliateCommission.php?duration=";
	var dtdcCurrentStatusURL = "ajaxDtdcCurrentStatusXML.php?trackid=";
/*
 * Function Name: getHTTPObject()
 * Purpose: Instantiate object of XMLHttpRequest().
 * Summary: Beauty of this function is, this function instantiate browser independent
 * object of XMLHttpRequest()
 *
*/
function getHTTPObject() {
      var xmlhttp;
      /*@cc_on
          @if (@_jscript_version >= 5)
            try {
          xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
            } catch (e) {
          try {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
          } catch (E) {
            xmlhttp = false;
          }
        }
          @else
          xmlhttp = false;
          @end @*/
          if (!xmlhttp && typeof XMLHttpRequest != 'undefined') {
            try {
          xmlhttp = new XMLHttpRequest();
    } catch (e) {
          xmlhttp = false;
    }
  }
          return xmlhttp;
}
var http = new getHTTPObject();

/*
* Function to send the ajax request to server
*/

function searchOrder(selectid, login)
{
	document.getElementById("loadingpopup").style.visibility = "visible";
	document.getElementById("loadingpopup").style.display = "";
	//document.getElementById("mode").value = "search";
    var id = document.getElementById(selectid).value;
    http.open("GET", URL + escape(id) + "&action=reterive&login="+login, true);
    http.onreadystatechange = myGetFunction;
    http.send(null);    
}

/*
* Function to accept the ajax response from the
*/

function myGetFunction()
{
    if(http.readyState==4)
	{
			document.getElementById("loadingpopup").style.visibility = "hidden";
			document.getElementById("loadingpopup").style.display = "none";

            var result=http.responseText;
			if(result=='0')
			{
				document.getElementById("orderlist").innerHTML = "";
			}
			else
			{
				document.getElementById("orderlist").innerHTML = result;
			}
	}
}


function referalHistory(selectid, login)
{
	document.getElementById("loadingpopup").style.visibility = "visible";
	document.getElementById("loadingpopup").style.display = "";
	//document.getElementById("mode").value = "search";
    var id = document.getElementById(selectid).value;
    http.open("GET", URL1 + escape(id) + "&action=reterive&login="+login, true);
    http.onreadystatechange = myRefFunction;
    http.send(null); 
}

/*
* Function to accept the ajax response from the
*/

function myRefFunction()
{
    if(http.readyState==4)
	{
			document.getElementById("loadingpopup").style.visibility = "hidden";
			document.getElementById("loadingpopup").style.display = "none";

            var result=http.responseText;
            if(result == 0)
			{
			 		document.getElementById("historylist").innerHTML = "<div class=\"links\"><p>No referral commission earned in selected duration.</p> </div>";
			}
            else
            {
            	document.getElementById("historylist").innerHTML = "";
                document.getElementById("historylist").innerHTML = result;
            }
	}
}

/********* function for affiliate request **************/
function searchAffiliateCommission(selectItemValue, affiliateid)
{
	document.getElementById("loadingpopup").style.visibility = "visible";
	document.getElementById("loadingpopup").style.display = "";
	//document.getElementById("mode").value = "search";
   // var id = document.getElementById(selectid).value;
   if(selectItemValue == "")
	{
	   alert("select the date duration");
       return false;
	}
    http.open("GET", affiliateURL + escape(selectItemValue) + "&action=reterive&affiliateid="+affiliateid, true);
    http.onreadystatechange = affiliateResponseFunction;
    http.send(null);    
}

function affiliatePayment(selectItemValue, affiliateid)
{
	document.getElementById("loadingpopup1").style.visibility = "visible";
	document.getElementById("loadingpopup1").style.display = "";
	if(selectItemValue == "")
	{
	   alert("select the date duration");
       return false;
	}
    http.open("GET", affiliateURL + escape(selectItemValue) + "&action=payment&affiliateid="+affiliateid, true);
    http.onreadystatechange = affiliatePaymentResponse;
    http.send(null);    
}

function affiliateResponseFunction()
{
    if(http.readyState==4)
	{
			document.getElementById("loadingpopup").style.visibility = "hidden";
			document.getElementById("loadingpopup").style.display = "none";

           var result=http.responseText;
           if(result.indexOf('Failed') != '-1')
			{
				document.getElementById("commissionlist").innerHTML = "<div class='links'><span style='margin-left:10px;'>No transaction made during the selected range.</span></div>";
			}
            else
            {
                document.getElementById("commissionlist").innerHTML = result;
            }
	}
}

function affiliatePaymentResponse()
{
    if(http.readyState==4)
	{
			document.getElementById("loadingpopup1").style.visibility = "hidden";
			document.getElementById("loadingpopup1").style.display = "none";

           var result=http.responseText;
            if(result.indexOf('Failed') != '-1')
			{
				document.getElementById("paymenthistory").innerHTML = "<div class='links'><span style='margin-left:10px;'>No payment has been made during the selected range.</span></div>";
			}
            else
            {
                document.getElementById("paymenthistory").innerHTML = result;
            }
	}
}

function trackDTDCStatus(trackid){
	
	document.getElementById("messageloaderbar").style.display = "block";
    http.open("GET", dtdcCurrentStatusURL + escape(trackid), true);
    http.onreadystatechange = function(){
    	if(http.readyState==4){
			if(http.status==200){
				if(http.responseText == 1){
					//document.getElementById("messageloaderbar").style.display = "none";
					parseXML(trackid);
				}				
			}
		}
    }
    http.send(null); 
}

function parseXML(trackid){
	document.getElementById("messageloaderbar").style.display = "none";
	try{//Internet explorer
		var xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
	}catch(e){
		try{
			var xmlDoc =document.implementation.createDocument("","",null);
		}catch(e){
			alert(e.message);
			return;
		}
	}
	xmlDoc.async = false;
	xmlDoc.load(http_loc + "/trackdtdcstatus/" + trackid + ".xml");
	
	if( xmlDoc.getElementsByTagName('FIELD')[0] !=null)
	document.getElementById('shipmentno').innerHTML = xmlDoc.getElementsByTagName('FIELD')[0].getAttribute("VALUE");
	
	if( xmlDoc.getElementsByTagName('FIELD')[1] !=null)
	document.getElementById('refno').innerHTML = xmlDoc.getElementsByTagName('FIELD')[1].getAttribute("VALUE");
	
	if( xmlDoc.getElementsByTagName('FIELD')[2] !=null)
	document.getElementById('bmode').innerHTML = xmlDoc.getElementsByTagName('FIELD')[2].getAttribute("VALUE");

	if( xmlDoc.getElementsByTagName('FIELD')[3] !=null)
	var rfrom = xmlDoc.getElementsByTagName('FIELD')[3].getAttribute("VALUE").split(",");

	document.getElementById('rform').innerHTML = rfrom[0];

	if( xmlDoc.getElementsByTagName('FIELD')[4] !=null)
	document.getElementById('bookedon').innerHTML = xmlDoc.getElementsByTagName('FIELD')[4].getAttribute("VALUE");

	if( xmlDoc.getElementsByTagName('FIELD')[5] !=null)
	document.getElementById('nopieces').innerHTML = xmlDoc.getElementsByTagName('FIELD')[5].getAttribute("VALUE");

	if( xmlDoc.getElementsByTagName('FIELD')[6] !=null)
	document.getElementById('weight').innerHTML = xmlDoc.getElementsByTagName('FIELD')[6].getAttribute("VALUE");

	if( xmlDoc.getElementsByTagName('FIELD')[7] !=null)
	document.getElementById('status').innerHTML = xmlDoc.getElementsByTagName('FIELD')[7].getAttribute("VALUE");

	if( xmlDoc.getElementsByTagName('FIELD')[8] !=null)
	document.getElementById('dilon').innerHTML = xmlDoc.getElementsByTagName('FIELD')[8].getAttribute("VALUE");

	if( xmlDoc.getElementsByTagName('FIELD')[9] !=null)
	document.getElementById('dilat').innerHTML = xmlDoc.getElementsByTagName('FIELD')[9].getAttribute("VALUE");

	if( xmlDoc.getElementsByTagName('FIELD')[10] !=null)
	document.getElementById('destination').innerHTML = xmlDoc.getElementsByTagName('FIELD')[10].getAttribute("VALUE");

	if( xmlDoc.getElementsByTagName('FIELD')[11] !=null)
	document.getElementById('receivedby').innerHTML = xmlDoc.getElementsByTagName('FIELD')[11].getAttribute("VALUE");

	document.getElementById('details').innerHTML = "<strong>" + xmlDoc.getElementsByTagName('DETAILS')[0].childNodes[0].nodeValue + "</strong>";
	document.getElementById("dtdcstatus").style.display = "block";
}

function hideMessageLoaderDiv(divId){
	document.getElementById("messageloaderbar").style.display = "none";
}
function hideDtdcDiv(){
	document.getElementById("dtdcstatus").style.display = "none";
}


