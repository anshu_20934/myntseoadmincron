<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html xml:lang="en" xmlns:fb="http://www.facebook.com/2008/fbml">
<head>
{include file="site/header.tpl" }
{literal}
<style>

		.form-screen{ 
			border:0px solid red; 
			margin:0 auto; 
			width:500px;
			min-height:225px; 
			margin:0 auto; 
			padding:10px 0 16px; 
			font-family: Arial, Helvetica, sans-serif;
			font-size:12px;
			text-align:left;
		}
		.form-screen h1{
			color:#333;
			font-size:18px;
			font-weight:bold;
			margin:30px 0 6px 0;
		}
		.table-box td{padding:5px; font-family: Arial, Helvetica, sans-serif; font-size:12px;}
		.table-box td strong{
			font-size:18px;
		}
		.table-box td strong span{
			font-size:18px;
			font-weight:normal;
		}
		.table-box td.icon-line {background-position:-264px 38px;background-repeat:no-repeat;padding:8px 5px 15px}
		.table-box .formtextbox {height:25px;width:253px;padding:2px 5px;}
		.table-box a.orange-bg{padding:8px;}
		.table-box th{
			font-size:13px;
			font-weight:bold;
			text-align:left;
		}
		span.status {float: left; color:#CC3300;font-size:12px;font-weight:bold;vertical-align: top; display:none;;padding:0;margin:0;border:none;background:none;}
		</style>
{/literal}
 </head>
<body class="{$_MAB_css_classes}" id="body">
{include file="site/top.tpl"}
<div id="scroll_wrapper">
	<div id="container">		
	   <div id="wrapper">
<div align="center">
			<div class="form-screen">
			
				<div class="reg-users ">
					<div class="reg-box"  style="padding:0px 10px;background:#fff;"> 
						{if $link_status eq 'true'}
							<form action="" method="post" id="reset_form">
							<input type="hidden" value="C" name="usertype">
							<input type="hidden" value="1" name="redirect">
							<input type="hidden" value="login" name="mode">
							<input type="hidden" value="home" name="filetype">
							<input type="hidden" name="reset_key" value="{$reset_key}" />
							<input type="hidden" name="method" value="change_password" />
									<table cellspacing="1" cellpadding="1" border="0" width="100%" class="table-box">
										<tr>
											<td colspan="2" class="icon-line"><strong>Password Recovery</strong></td>
											
										</tr>
										<tr>
											
											<th width="180px">Email : </th>
											<td>
												<label>{$user_details[0].login}</label>
											</td>
										</tr>
										<tr>
											<th>New Password : </th>
											<td><input type="password" name="password"  class="formtextbox" id="rp-password" value="" size="35" /><span class="rp-status status" ></span></td>
										</tr>
										<tr>
											<th>Retype New Password : </th>
											<td><input type="password" name="confirm_pwd"  class="formtextbox" id="confirm-password" value="" size="35" /><span class="confirm-status status" ></span></td>
										</tr>
										
										<tr>
										<td>&nbsp;</td>
											<td>
											
												<div class="" style="_width:185px;cursor:pointer;"><span><a class="orange-bg corners p5" id="reset-submit">Change Pasword</a></span></div>
											
											</td>
										</tr>
										
									</table>
									</form>
							{else}
								{$error_msg}
							{/if}
					</div>
				</div>
			</div>
		</div>
	   </div>
	</div>
</div>	
	{include file="site/footer.tpl" }
 	   
{literal}
<script>
$(document).ready(function(){
    
	function rp_val(){
		var newPasswordVal=$("#rp-password").val();
    	if(newPasswordVal == ""){
    		$(".table-box .rp-status").html("Enter a valid Password").slideDown('slow');return false;
    	}
    	else if(newPasswordVal.length < 6){
    		$(".table-box .rp-status").html("Minimum 6 characters required.").slideDown('slow');return false;
        	}
    	else{
    		$(".table-box .rp-status").slideUp('slow',function(){$(this).html("");});return true;
    		}
		}
	function rp_conf_val(){
		var newPasswordVal=$("#rp-password").val();
    	var confirmnewPasswordVal=$("#confirm-password").val();
    	if(newPasswordVal == "" || newPasswordVal.length < 6 || newPasswordVal != confirmnewPasswordVal){
    		$(".table-box .confirm-status").html("Passwords must match.").slideDown('slow');return false;
        	}
    	else{
    		$(".table-box .confirm-status").slideUp('slow',function(){$(this).html("");});return true;
        	}
	
	}
	$("#rp-password").blur(function(){
    	rp_val();
    });
	 $("#confirm-password").blur(function(){
		rp_conf_val();
	 });

	 $("#reset-submit").live("click",function(){
		 if(rp_val()&& rp_conf_val()){
			 $('#reset_form').trigger('submit');
		 }
		 else{return false;}
	});

	

});
</script>
{/literal}

</body>
</html>
