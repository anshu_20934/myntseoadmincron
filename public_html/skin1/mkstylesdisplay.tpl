{* $Id: mykritihome.tpl,v 1.88.2.3 2006/07/19 10:19:35 max Exp $ *}
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
{config_load file="$skin_config"}
<html>
<head>
{include file="mkheadinfo.tpl" }
{literal}
<style>
.stylelist{margin: 0px;}
.stylelist .listbox{ height:440px;width:310px;_width:310px;float:left;border:1px solid #E3E3E3;margin:10px 9px 10px 0px;text-align:center;}
.stylelist .listbox .imagebox{   margin:5px;text-align:center;height:auto;border:0px;width:100%;} 
.stylelist .listbox .imagebox img{   border:0px}
.stylelist .listbox .textbox{   text-align:center;width:100%;height:auto;font-size:1.8em;text-transform:none;letter-spacing:-0.03em;}
.stylelist .listbox .textbox a:link{   text-decoration:underline;}

</style>
{/literal}
<script type="text/javascript" src="./skin1/js_script/alttxt.js"></script>

</head>
<body id="body" >
{if $login_event == 'productstyle' && $login}
	{include file="popup_login.tpl" }
{/if}
<div id="scroll_wrapper">
<div id="container">
{include file="mykrititop.tpl" }
<div id="wrapper" >

    <div id="display">
  
	  <div class="home_column">
	     <div style="width:100%;">
      		 <img src='{$cdn_base}/skin1/mkimages/Myntra-Create-tee.jpg' alt='Product Banner'/>
    	</div>
        <div class="clearall"></div>
            
    {if $orgId != ""}
		{include file="modules/organization/breadcrumb_organization.tpl"}
	{else}
	     <div id="breadcrumb" style="margin-top:0px;padding-left:5px;">
	        <div style='width:740px;min-height:30px;height:auto !important;height:20px;margin-bottom:5px;'>
	           <div style="width:330px;float:left;min-height:30px;height:auto !important;height:33px;">
				{section name=bc loop=$breadCrumb}
					{if !$smarty.section.bc.last}
					  {if $breadCrumb[bc].title ne ''}
						  <div style="float:left;">
						      <a href={$breadCrumb[bc].reference}>{$breadCrumb[bc].title}</a>&nbsp;&gt;&nbsp;
						 </div>
					  {/if}
					 {else}
						{$breadCrumb[bc].title}
					{/if}
				{/section}
			  </div>
	        </div>
			
		 </div>
	 {/if}
	  <div class="clearall"></div>
	<div><h1 class="browsedesignheading" style='color:#000000;'>Custom {$productTypeLabel}, Personalized {$productTypeLabel}, Create your own {$productTypeLabel} with photos or designs.</h1></div>
       {if $stylegroupinfo ne ''}
                   {include file="mkstyledetails.tpl"}
			
  	   {else}	
			<div class="designercontainer">
				<p><strong>This is not supported now.</strong></p>
			</div>

    {/if}
    <div>
    <h2 style='font-size:14px;font-weight:bold;'>Related tags</h2>
		 <a style='font-size:12px;color:#1B5D89;' href='{$http_location}/create/{$productTypeLabel}/{$productTypeId}'>Create {$productTypeLabel}</a>
		 &nbsp;&nbsp;
		 <a style='font-size:12px;color:#1B5D89;'  href='{$http_location}/create/{$productTypeLabel}/{$productTypeId}'>Customized {$productTypeLabel}</a>
		 &nbsp;&nbsp;
		 <a style='font-size:12px;color:#1B5D89;' href='{$http_location}/create/{$productTypeLabel}/{$productTypeId}'>Design {$productTypeLabel}</a>
		 &nbsp;&nbsp;
		 <a style='font-size:12px;color:#1B5D89;' href='{$http_location}/create/{$productTypeLabel}/{$productTypeId}'>Personalized {$productTypeLabel}</a>
		 &nbsp;&nbsp;
		 <a style='font-size:12px;color:#1B5D89;' href='{$http_location}/create/{$productTypeLabel}/{$productTypeId}'>Personalised {$productTypeLabel}</a>
		 &nbsp;&nbsp;
		 <a style='font-size:12px;color:#1B5D89;' href='{$http_location}/create/{$productTypeLabel}/{$productTypeId}'>Make {$productTypeLabel}</a>
		 &nbsp;&nbsp;
		 <a style='font-size:12px;color:#1B5D89;' href='{$http_location}/create/{$productTypeLabel}/{$productTypeId}'>Customise {$productTypeLabel}</a>
		 &nbsp;&nbsp;
		 <a style='font-size:12px;color:#1B5D89;' href='{$http_location}/create/{$productTypeLabel}/{$productTypeId}'>Custom-make {$productTypeLabel}</a>
		 &nbsp;&nbsp;
		 <a style='font-size:12px;color:#1B5D89;' href='{$http_location}/create/{$productTypeLabel}/{$productTypeId}'>Custom {$productTypeLabel}</a>
		 &nbsp;&nbsp;
		  <a style='font-size:12px;color:#1B5D89;' href='{$http_location}/create/{$productTypeLabel}/{$productTypeId}'>Create Your Own {$productTypeLabel}</a>
		 &nbsp;&nbsp;
		 <a style='font-size:12px;color:#1B5D89;' href='{$http_location}/create/{$productTypeLabel}/{$productTypeId}'>Create Custom {$productTypeLabel}</a>
		 &nbsp;&nbsp;
		  <a style='font-size:12px;color:#1B5D89;' href='{$http_location}/create/{$productTypeLabel}/{$productTypeId}'>Create {$productTypeLabel} Designs</a>
		 &nbsp;&nbsp;
		  <a style='font-size:12px;color:#1B5D89;' href='{$http_location}/create/{$productTypeLabel}/{$productTypeId}'>Make and print your own {$productTypeLabel} Designs</a>
		 &nbsp;&nbsp;
		  <a style='font-size:12px;color:#1B5D89;' href='{$http_location}/create/{$productTypeLabel}/{$productTypeId}'>Print your own {$productTypeLabel} Designs</a>
		 &nbsp;&nbsp;
		 {section name=idx loop=$arr_seo}
			{if $arr_seo[idx][1]  eq '/buy/T-Shirts'}
				<a style='font-size:12px;color:#1B5D89;' href='{$http_location}{$arr_seo[idx][3]}'>{$arr_seo[idx][2]}</a>&nbsp;&nbsp;
			{/if}
		{/section}

    </div>
   </div>
 </div>
 <div class="clearall"></div>
</div>
{include file="footer.tpl" }
</div>
</div>
<div id="navtxt" class="navtext" style="position:absolute; top:-100px; left:0px; visibility:hidden;width:210px;">
</div>
</body>
</html>