{literal}
<script language="JavaScript" type="text/javascript">
function submitReview(login){//,postUrl,postArray
   if(checkIfLoggedIn(login))   {
		if(document.reviewform.name.value == '')		{
			alert("Please enter your name.")
			document.reviewform.name.focus();
			return false;
		}else if(document.reviewform.msg.value == '')		{
			alert("Please enter your comments.")
			document.reviewform.msg.focus();
			return false;
		}else if(document.reviewform.captcha.value == '')		{
			alert("Please enter letters displayed above.")
			document.reviewform.captcha.focus();
			return false;
		}else{			
			//buildAndSubmitPostParam(postUrl,postArray);
			return true;
		}
   } else  {
     return false
   }
}

function limit_textarea(textarea, count, maxlimit){	
	if (textarea.value.length > maxlimit) {// if too long...trim it!
		textarea.value = textarea.value.substring(0, maxlimit);		
	}
	else 
		count.value = maxlimit - textarea.value.length;
}
</script>
{/literal}

{section name=review loop=$reviews}
  {if $reviews[review][2] ne '' && $reviews[review][3] ne ''}
	 <div class="links">
		    <p><strong>{$reviews[review][2]}:</strong> &nbsp;{$reviews[review][3]}</p>
	</div>
  {/if}
 {/section}
  <div class="super">
	<p>write your review</p>
</div>

<form name='reviewform' method='post' action="updateCustomerReview.php">

<!--added by arun to get the element by post-->
<input type=hidden name='productTypeId' id='productTypeId' value="{$productTypeId}" />
<input type=hidden name='type' id='type' value="{$productTypeLabel}" />
<input type=hidden name='productStyleId' id='productStyleId' value="{$productStyleId}" />
<input type=hidden name='productid' id='productid' value="{$productid}" />
<input type=hidden name='pageURL' id='pageURL' value="{$url}">
<!--added by arun to get the element by post-->

<div class="links">
	<div class="legend">
		<p>
			Your Message:
			<span class="mandatory">*</span>
		</p>
	</div>
	<div class="field">
		<div style="width:200px;">
			<font size="1" face="arial, helvetica, sans-serif">( You may enter up to 200 characters. )</font>
		</div>
		
		<textarea rows="" cols="" name="msg" id="msg" onKeyDown="limit_textarea(this.form.msg,this.form.remLen,200);" style="width:225px;"onKeyUp="limit_textarea(this.form.msg,this.form.remLen,200);"></textarea>
		<div style="width:200px;">
			<font size="1" face="arial, helvetica, sans-serif">
			<input readonly type=text name=remLen size=3 maxlength=3 value="200"> characters left
			</font>
		</div>
	</div>
	<div class="clearall"></div>
</div>

<!--code for image captcha-->
<div class="links">
	<div class="legend">
		<p>
			Write the word shown below:
			<span class="mandatory">*</span>
		</p>
	</div>
	<div class="field">
		<input type="text" name="captcha" id="captcha" size="38"  onpaste="return false;"/><br/>
		<img src="./skin1/mkimages/spacer.gif" id="captcha-image"/><br/>
		<div style="cursor:pointer;font-size:0.8em;" onmouseout="document.getElementById('change-image').style.textDecoration='none';document.getElementById('change-image').style.color='#666666';" onclick="javascript:get_captcha_image('captcha-image','review');" onmouseover="document.getElementById('change-image').style.textDecoration='underline';document.getElementById('change-image').style.color='red';" id="change-image">Not readable? Change text.</div>		
	</div>
	<div class="clearall"></div>
</div>
<!--code for image captcha-->

<div class="links">
	<div class="legend">
		<p></p>
	</div>
	<div class="field">
		<input class="submit" type="submit" border="0" value="add review" name="reviewButton" onClick="return submitReview('{$login}');"/>
		<!--<input class="submit" type="button" border="0" value="add review" name="reviewButton" onClick="submitReview('{$login}','updateCustomerReview.php',Array('productTypeId','type','productStyleId','productid','msg','captcha','pageURL'));"/>-->
	</div>
	<div class="clearall"></div>
</div>
<div class="foot"></div>
</form>