{* $Id: home.tpl,v 1.34 2006/02/10 14:27:32 svowl Exp $ *}
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
{ config_load file="$skin_config" }
<html>
<head>
<title>{$lng.txt_site_title}</title>
{ include file="meta.tpl" }
<link rel="stylesheet" href="{$SkinDir}/skin1_admin.css" />
</head>
<body{$reading_direction_tag}>
{ include file="rectangle_top.tpl" }
{ include file="partner/head_partner.tpl" }
<!-- main area -->
<table width="100%" cellpadding="0" cellspacing="0" align="center">
<tr>
<td class="VertMenuLeftColumn">
{if $login eq "" }
{ include file="auth.tpl" }
{else}
{ include file="partner/menu.tpl" }
<br />
{ include file="menu_profile.tpl" }
{/if}
<br />
{ include file="help.tpl" }
<img src="{$cdn_base}/skin1/images/spacer.gif" width="150" height="1" alt="" />
</td>
<td valign="top">
<!-- central space -->
{include file="location.tpl"}

{include file="dialog_message.tpl"}

{if $main eq "stats"}
{include file="partner/main/stats.tpl"}

{elseif $main eq "module_disabled"}
{include file="partner/main/module_disabled.tpl"}

{elseif $main eq "banner_info"}
{include file="main/banner_info.tpl"}

{elseif $main eq "referred_sales"}
{include file="main/referred_sales.tpl"}

{elseif $main eq "register"}
{include file="partner/main/register.tpl"}

{elseif $main eq "payment_history"}
{include file="partner/main/payment_history.tpl"}

{elseif $main eq "affiliates"}
{include file="main/affiliates.tpl"}

{elseif $main eq "howto"}
{include file="partner/main/howto.tpl"}

{elseif $main eq "products"}
{include file="partner/main/search_result.tpl"}

{elseif $main eq "product"}
{include file="partner/main/product.tpl"}

{elseif $main eq "home" and $login ne ""}
{include file="partner/main/promotions.tpl"}

{elseif $main eq "home" && $mode eq 'profile_created'}
{include file="partner/main/welcome_queued.tpl"}

{elseif $main eq "home"}
{include file="partner/main/welcome.tpl"}

{elseif $main eq "secure_login_form"}
{include file="partner/main/secure_login_form.tpl"}

{elseif $main eq "change_password"}
{include file="customer/main/change_password.tpl"}

{else}
{include file="common_templates.tpl"}
{/if}

<!-- /central space -->
&nbsp;
</td>
<td class="VertMenuRightColumn">
{if $login eq "" }
{ include file="news.tpl" }
{else}
{ include file="authbox.tpl" }
{/if}
<br />
{ include file="poweredby.tpl" }
<br />
<img src="{$cdn_base}/skin1/images/spacer.gif" width="150" height="1" alt="0" />
</td>
</tr>
</table>
{ include file="rectangle_bottom.tpl" }
</body>
</html>
