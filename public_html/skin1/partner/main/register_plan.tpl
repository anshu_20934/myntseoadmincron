{* $Id: register_plan.tpl,v 1.6 2006/01/17 13:05:51 svowl Exp $ *}
{if $plans}

<tr> 
	<td height="20" colspan="3"><b>{$lng.lbl_affiliate_plans}</b><hr size="1" noshade="noshade" /></td>
</tr>

{if $usertype eq "A" or ($usertype eq "P" and $active_modules.Simple_Mode ne "")}

<tr>
	<td align="right">{$lng.lbl_affiliate_plan}</td>
	<td>&nbsp;</td>
	<td nowrap="nowrap">
	<select name="plan_id">
{foreach from=$plans item=v}
		<option value="{$v.plan_id}"{if $userinfo.plan_id eq $v.plan_id} selected="selected"{/if}>{$v.plan_title}</option>
{/foreach}
	</select>
	</td>
</tr>

{else}

<input type="hidden" name="plan_id" value="{$userinfo.plan_id}" />

{/if}

<tr>
	<td align="right">{$lng.lbl_signup_for_partner_plan}</td>
	<td>&nbsp;</td>
	<td nowrap="nowrap">
	<select name="pending_plan_id">
{foreach from=$plans item=v}
		<option value="{$v.plan_id}"{if $userinfo.pending_plan_id eq $v.plan_id} selected="selected"{/if}>{$v.plan_title}</option>
{/foreach}
	</select>
	</td>
</tr>

{/if}
