{* $Id: module_disabled.tpl,v 1.5 2005/12/03 11:05:37 max Exp $ *}
<h3><font color="red">{$lng.lbl_partner_area_is_temporary_disabled}</font></h3>
<p align="justify">
{$lng.txt_partner_area_is_temporary_disabled_note|substitute:"email":$config.Company.users_department}
</p>
