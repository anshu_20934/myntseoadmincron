{* $Id: howto.tpl,v 1.21 2006/01/17 13:05:51 svowl Exp $ *}
{include file="page_title.tpl" title=$lng.lbl_banner_html_code}
{$lng.txt_banner_html_code_note}<br /><br />
 
<!-- IN THIS SECTION -->
 
{include file="dialog_tools.tpl"}
 
<!-- IN THIS SECTION -->
<br />
 {if $config.XAffiliate.display_as_iframe eq 'Y'}{assign var="local_type" value="iframe"}{else}{assign var="local_type" value="js"}{/if}
 
{if $config.XAffiliate.partner_enable_level eq 'Y'}
<p />
{$lng.txt_banner_html_code_register_note}<br />
<b>{$lng.lbl_link}:</b><br />
<textarea cols="60" rows="3" readonly="readonly">&lt;a href="{$catalogs.partner}/register.php?parent={$login}"&gt;{$lng.lbl_register}&lt;/a&gt;</textarea>
<br />
{/if}

<p align="justify">
{$lng.txt_banner_html_code_comment}
</p>

{capture name=dialog}
<table cellspacing="1" cellpadding="2" width="100%">
{if $banners ne ''}
{foreach from=$banners item=v}
<tr>
	<td colspan="2" class="AdminTitle">{$v.banner}</td>
</tr>
<tr valign="top">
	<td colspan="2">{include file="main/display_banner.tpl" assign="ban" banner=$v type=$local_type partner=$login}{$ban|amp}</td>
</tr>
<tr>
	<td colspan="2"><b>{$lng.lbl_iframe_code}:</b></td>
</tr>
<tr>
    <td colspan="2"><textarea cols="60" rows="5" readonly="readonly">{include file="main/display_banner.tpl" assign="ban" banner=$v type="iframe" partner=$login current_location=$http_location}{$ban|escape}</textarea></td>
</tr>
<tr>
	{if $v.banner_type eq 'G'}
	<td colspan="2"><b>{$lng.lbl_html_code}:</b></td>
</tr>
<tr>
	<td colspan="2"><textarea cols="60" rows="5" readonly="readonly">{include file="main/display_banner.tpl" assign="ban" banner=$v type="js" partner=$login current_location=$http_location}{$ban|escape}</textarea></td>
	{else}
    <td><b>{$lng.lbl_javascript_version}:</b></td>
	<td><b>{$lng.lbl_ssi_version}:</b></td>
</tr>
<tr>
	<td><textarea cols="35" rows="5" readonly="readonly">{include file="main/display_banner.tpl" assign="ban" banner=$v type="js" partner=$login current_location=$http_location}{$ban|escape}</textarea></td>
    <td><textarea cols="35" rows="5" readonly="readonly">{include file="main/display_banner.tpl" assign="ban" banner=$v type="ssi" partner=$login current_location=$http_location}{$ban|escape}</textarea></td>
	{/if}
</tr>
{/foreach}
{else}
<tr>
	<td colspan="2" align="center">{$lng.lbl_no_banners_have_been_defined}</td>
</tr>
{/if}
</table>
{/capture}
{include file="dialog.tpl" content=$smarty.capture.dialog title=$lng.lbl_available_banners extra='width="100%"'}
