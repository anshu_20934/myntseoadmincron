{* $Id: register.tpl,v 1.41.2.2 2006/07/11 08:39:36 svowl Exp $ *}
{if $js_enabled eq 'Y'}
{include file="check_email_script.tpl"}
{include file="check_zipcode_js.tpl"}
{include file="generate_required_fields_js.tpl"}
{include file="check_required_fields_js.tpl"}
{if $config.General.use_js_states eq 'Y'}
{include file="change_states_js.tpl"}
{/if}
{/if}

{if $newbie eq "Y"}
{if $login ne ""}
{assign var="title" value=$lng.lbl_modify_profile}
{else}
{assign var="title" value=$lng.lbl_create_profile}
{/if}
{else}
{if $main eq "user_add"}
{assign var="title" value=$lng.lbl_create_partner_profile}
{else}
{assign var="title" value=$lng.lbl_modify_partner_profile}
{/if}
{/if}

{include file="page_title.tpl" title=$title}

<!-- IN THIS SECTION -->

{if $newbie ne "Y"}
{include file="dialog_tools.tpl"}
{/if}

<!-- IN THIS SECTION -->

<font class="Text">

{if $usertype ne "B"}
<br />
{if $main eq "user_add"}
{$lng.txt_create_partner_profile}
{else}
{$lng.txt_modify_partner_profile}
{/if}
{else}
{$lng.txt_create_profile_msg_partner}
{/if}
<br /><br />

{$lng.txt_fields_are_mandatory}

</font>

<br /><br />

{capture name=dialog}

{if $newbie ne "Y" and $main ne "user_add" and ($usertype eq "P" and $active_modules.Simple_Mode eq "Y" or $usertype eq "A")}
<div align="right">{include file="buttons/button.tpl" button_title=$lng.lbl_return_to_search_results href="users.php?mode=search"}</div>
{/if}

{assign var="reg_error" value=$top_message.reg_error}
{assign var="error" value=$top_message.error}
{assign var="emailerror" value=$top_message.emailerror}

{if $registered eq ""}
{if $reg_error}
<font class="Star">
{if $reg_error eq "F" }
{$lng.txt_registration_error}
{elseif $reg_error eq "E" }
{$lng.txt_email_already_exists}
{elseif $reg_error eq "U" }
{$lng.txt_user_already_exists}
{/if}
</font>
<br />
{/if}

{if $error ne ""}
<font class="Star">
{if $error eq "b_statecode"}
{$lng.err_billing_state}
{elseif $error eq "s_statecode"}
{$lng.err_shipping_state}
{elseif $error eq "email"}
{$lng.txt_email_invalid}
{else}
{$error}
{/if}
</font>
<br />
{/if}

<script type="text/javascript" language="JavaScript 1.2">
<!--
var is_run = false;
function check_registerform_fields() {ldelim}
	if(is_run)
		return false;
	is_run = true;
	if (check_zip_code(){if $default_fields.email.required eq 'Y'} && checkEmailAddress(document.registerform.email){/if} && checkRequired(requiredFields)) {ldelim}
		document.registerform.submit();
		return true;
	{rdelim}
	is_run = false;
	return false;
{rdelim}
-->
</script>
 
<table cellspacing="1" cellpadding="2" width="100%">

<form action="{$register_script_name}?{$smarty.server.QUERY_STRING}" method="post" name="registerform" onsubmit="check_registerform_fields(); return false;">

<input type="hidden" name="parent" value="{$parent|default:$userinfo.parent}" />

{if $config.Security.use_https_login eq "Y"}
<input type="hidden" name="{$XCARTSESSNAME}" value="{$XCARTSESSID}" />
{/if}

{include file="main/register_personal_info.tpl" userinfo=$userinfo}

{include file="main/register_billing_address.tpl" userinfo=$userinfo}

{include file="main/register_shipping_address.tpl" userinfo=$userinfo}

{include file="main/register_contact_info.tpl" userinfo=$userinfo}

{include file="main/register_additional_info.tpl" section='A'}

{include file="partner/main/register_plan.tpl" userinfo=$userinfo}

{include file="main/register_account.tpl" userinfo=$userinfo}


{if $active_modules.News_Management and $newslists}
{include file="modules/News_Management/register_newslists.tpl" userinfo=$userinfo}
{/if}


<tr>
<td colspan="3" align="center">
<br /><br />
{if $newbie eq "Y"}
{$lng.txt_terms_and_conditions_newbie_note}
{/if}
</td>
</tr>

<tr>
<td colspan="2">&nbsp;</td>
<td>

<font class="FormButton">
{if $smarty.get.mode eq "update"}
<input type="hidden" name="mode" value="update" />
{/if}

{if $js_enabled and $usertype eq "B"}
{include file="buttons/submit.tpl" type="input" style="button" href="javascript: return check_registerform_fields();"}
{else}
<input type="submit" value=" {$lng.lbl_save|strip_tags:false|escape} " />
{/if}

</td>
</tr>

<input type="hidden" name="usertype" value="{if $smarty.get.usertype ne ""}{$smarty.get.usertype|escape:"html"}{else}{$usertype}{/if}" />

</form>

</table>

<br /><br />

{if $newbie eq "Y"}
{$lng.txt_newbie_registration_bottom}
<br /><a href="help.php?section=conditions"><font class="Text"><b>{$lng.lbl_terms_n_conditions}</b>&nbsp;</font>{include file="buttons/go.tpl"}</a>
{else}
{$lng.txt_user_registration_bottom}
{/if}

<br />

{else}

{if $smarty.post.mode eq "update" or $smarty.get.mode eq "update"}
{$lng.txt_profile_modified}
{elseif $smarty.get.usertype eq "B"  or $usertype eq "B"}
{$lng.txt_partner_created}
{else}
{$lng.txt_profile_created}
{/if}
{/if}

{/capture}
{include file="dialog.tpl" title=$lng.lbl_profile_details content=$smarty.capture.dialog extra='width="100%"'}

{if $userinfo.status eq "Q" && $usertype ne "B"}

<br />

{capture name=dialog}

<form action="{$register_script_name}?{$smarty.server.QUERY_STRING}" method="post" name="decisionform">

<input type="hidden" name="mode" value="" />

{$lng.txt_partner_profile_is_not_approved}
<br /><br />

<textarea name="reason" cols="40" rows="5"></textarea>

<br />

<input type="button" value="{$lng.lbl_approved|strip_tags:false|escape}" onclick="javascript: document.decisionform.mode.value='approved'; document.decisionform.submit();" />
&nbsp;&nbsp;&nbsp;&nbsp;
<input type="button" value="{$lng.lbl_declined|strip_tags:false|escape}" onclick="javascript: document.decisionform.mode.value='declined'; document.decisionform.submit();" />

</form>

{/capture}
{include file="dialog.tpl" title=$lng.lbl_approve_or_decline_partner_profile content=$smarty.capture.dialog extra='width="100%"'}

{/if}

