{* $Id: payment_history.tpl,v 1.25 2006/01/17 13:05:51 svowl Exp $ *}
{include file="page_title.tpl" title=$lng.lbl_payment_history}
{$lng.txt_payment_history_note}<br /><br />
 
<!-- IN THIS SECTION -->
 
{include file="dialog_tools.tpl"}
 
<!-- IN THIS SECTION -->
<br />
 
{include file="customer/main/navigation.tpl"}
{capture name=dialog}
<form method="get" action="payment_history.php" name="searchform">
<input type="hidden" name="mode" value="go" />

<table>
<tr>
	<td height="10" class="FormButton" nowrap="nowrap">{$lng.lbl_search_using_date}:</td>
	<td height="10" width="10">&nbsp;</td>
	<td height="10" class="FormButton" nowrap="nowrap"><input type="checkbox" value='Y' name="use_date"{if $smarty.get.use_date eq "Y" or $smarty.get.mode eq ""} checked="checked"{/if} /></td>
</tr>
<tr>
	<td height="10" class="FormButton" nowrap="nowrap">{$lng.lbl_date_from}:</td>
	<td height="10" width="10">&nbsp;</td>
	<td>{html_select_date prefix="Start" time=$start_date start_year=$config.Company.start_year end_year=$config.Company.end_year display_days=yes}</td>
</tr>
<tr>
	<td height="10" class="FormButton" nowrap="nowrap">{$lng.lbl_date_through}:</td>
	<td height="10" width="10">&nbsp;</td>
	<td>{html_select_date prefix="End" time=$end_date start_year=$config.Company.start_year end_year=$config.Company.end_year display_days=yes}</td>
</tr>
<tr>
	<td height="10" class="FormButton" nowrap="nowrap">{$lng.lbl_use_paging}:</td>
	<td height="10" width="10">&nbsp;</td>
	<td height="10"><input type="checkbox" name="use_paging" value='Y'{if $smarty.get.use_paging eq "Y" or $smarty.get.mode eq ""} checked="checked"{/if} /></td>
</tr>
<tr>
	<td colspan="3" class="SubmitBox">{include file="buttons/search.tpl" href="javascript: document.searchform.submit()" js_to_href="Y"}</td>
</tr>
</table>

</form>
{/capture}
{include file="dialog.tpl" content=$smarty.capture.dialog title=$lng.lbl_search extra='width="100%"'}

{if $smarty.get.mode ne ""}
<br />

{capture name=dialog}
{if $payments eq ""}
{$lng.lbl_no_records_found}<br />
{else}
<table cellpadding="2" cellspacing="1">
<tr class="TableHead">
	<td><b>{$lng.lbl_date}</b></td>
	<td><b>{$lng.lbl_amount}</b></td>
</tr>
{section name=pi loop=$payments}
<tr>
	<td>{$payments[pi].add_date|date_format:$config.Appearance.datetime_format}</td>
	<td>{include file="currency.tpl" value=$payments[pi].commissions}</td>
</tr>
{/section}
</table>
{/if}
<br />
<b>{$lng.lbl_paid_total}: {include file="currency.tpl" value=$paid_total}</b>
<br />
{/capture}
{include file="dialog.tpl" content=$smarty.capture.dialog title=$lng.lbl_payment_history extra='width="100%"'}
{/if}
