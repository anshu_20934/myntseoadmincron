{* $Id: promotions.tpl,v 1.13 2005/12/03 11:05:37 max Exp $ *}
{capture name=dialog}
<h3>{$lng.lbl_you_are_in_your_personal_partner_area}</h3>
<p align="justify">
{$lng.txt_partner_promotion_note}
</p>
<p />
<img src="{$cdn_base}/skin1/images/rarrow.gif" width="9" height="9" alt="" /> <b><a href="banner_info.php">{$lng.lbl_banners_statistics}</a></b><br />
{$lng.txt_partner_promotion_section_banners}
<p />
<img src="{$cdn_base}/skin1/images/rarrow.gif" width="9" height="9" alt="" /> <b><a href="stats.php">{$lng.lbl_summary_statistics}</a></b><br />
{$lng.txt_partner_promotion_section_summary}
<p />
<img src="{$cdn_base}/skin1/images/rarrow.gif" width="9" height="9" alt="" /> <b><a href="payment_history.php">{$lng.lbl_payment_history}</a></b><br />
{$lng.txt_partner_promotion_section_payments}
<p />
<img src="{$cdn_base}/skin1/images/rarrow.gif" width="9" height="9" alt="" /> <b><a href="howto.php">{$lng.lbl_banner_html_code}</a></b><br />
{$lng.txt_partner_promotion_section_banner_code}
<p />
<img src="{$cdn_base}/skin1/images/rarrow.gif" width="9" height="9" alt="" /> <b><a href="search.php">{$lng.lbl_product_html_code}</a></b><br />
{$lng.txt_partner_promotion_section_product_code}
{/capture}
{include file="dialog.tpl" title=$lng.lbl_partner_menu content=$smarty.capture.dialog extra='width="100%"'}
<p />
