{* $Id: product.tpl,v 1.30 2006/03/21 07:17:19 svowl Exp $ *}
{include file="page_title.tpl" title=$lng.lbl_product_html_code}
{$lng.txt_product_html_code_note}<br /><br />
 
<!-- IN THIS SECTION -->
 
{include file="dialog_tools.tpl"}
 
<!-- IN THIS SECTION -->
<br />
 
{capture name=dialog}
<table width="100%">
<tr>
	<td valign="top" align="left" rowspan="2" width="100">
{include file="product_thumbnail.tpl" productid=$product.productid image_x=$product.image_x image_y=$product.image_y product=$product.product tmbn_url=$product.tmbn_url type="P"}&nbsp;
	</td>
	<td valign="top">
{if $product.fulldescr ne ""}{$product.fulldescr}{else}{$product.descr}{/if}
<p />
<table width="100%" cellpadding="0" cellspacing="0" border="0">
<tr>
	<td colspan="2"><b><font class="ProductDetailsTitle">{$lng.lbl_details}</font></b></td>
</tr>
<tr>
	<td class="Line" height="1" colspan="2"><img src="{$cdn_base}/skin1/images/spacer.gif" class="Spc" alt="" /></td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
{if $config.Appearance.show_in_stock eq "Y" and $product.distribution eq ""}
<tr>
	<td width="30%">{$lng.lbl_in_stock}</td>
	<td nowrap="nowrap">{if $product.avail gt 0}{$lng.txt_items_available|substitute:"items":$product.avail}{else}{$lng.lbl_no_items_available}{/if}</td>
</tr>
{/if}
<tr>
	<td width="30%">{$lng.lbl_weight}</td>
	<td nowrap="nowrap">{$product.weight} {$config.General.weight_symbol}</td>
</tr>
{if $active_modules.Extra_Fields ne ""}
{include file="modules/Extra_Fields/product.tpl"}
{/if}
{if $active_modules.Subscriptions ne "" and $subscription}
{include file="modules/Subscriptions/subscription_info.tpl"}
{else}
<tr>
	<td class="ProductPriceConverting">{$lng.lbl_price}:</td>
	<td>
{if $product.taxed_price ne 0}
<font class="ProductDetailsTitle">{include file="currency.tpl" value=$product.taxed_price}</font><font class="MarketPrice"> {include file="customer/main/alter_currency_value.tpl" alter_currency_value=$product.taxed_price}</font>
{if $product.taxes}<br />{include file="customer/main/taxed_price.tpl" taxes=$product.taxes}{/if}
{else}
<input type="text" size="7" name="price" />
{/if}
	</td>
</tr>
{/if}
</table>
	</td>
</tr>
</table>
{/capture}
{include file="dialog.tpl" title=$product.producttitle content=$smarty.capture.dialog extra='width="100%"'}

{if $active_modules.Detailed_Product_Images ne ""}
<br />
{include file="modules/Detailed_Product_Images/product_images.tpl" }
{/if}

<p />
{capture name=dialog}
<p />
{$lng.txt_product_html_code_comment}
<p align="center"><b>{$catalogs.customer}/product.php?productid={$product.productid}&amp;partner={$login}</b></p>

{if $banners ne ''}
<center>
<table cellpadding="2" cellspacing="3" width="100%">
{foreach from=$banners item=v}
<tr>
	<th class="TableHead">{$v.banner}</th>
</tr>
<tr>
	<td align="center">
{capture name="html_1"}{include file="main/display_banner.tpl" banner=$v type="js" partner=$login productid=$product.productid}{/capture}
<p>{$smarty.capture.html_1|amp}</p>

	</td>
</tr>
<tr> 
    <td align="center"><b>{$lng.lbl_iframe_code}:</b></td>
</tr> 
<tr>
    <td align="center"><textarea cols="65" rows="6">{include file="main/display_banner.tpl" assign="ban" banner=$v type="iframe" partner=$login productid=$product.productid current_location=$http_location}{$ban|escape}</textarea></td>
</tr>
<tr>
	<td align="center"><b>{$lng.lbl_javascript_version}:</b></td>
</tr>
<tr>
	<td align="center"><textarea cols="65" rows="6">{$smarty.capture.html_1|escape}</textarea></td>
</tr>
<tr>
	<td><hr size="1" noshade="noshade" align="center" /></td>
</tr>
{/foreach}
</table>
</center>
{/if}
{/capture}
{include file="dialog.tpl" title=$lng.lbl_product_html_code content=$smarty.capture.dialog extra='width="100%"'}
<p />
{if $active_modules.Upselling_Products ne ""}
<br />
{include file="modules/Upselling_Products/related_products.tpl" }
{/if}
