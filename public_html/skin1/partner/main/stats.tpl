{* $Id: stats.tpl,v 1.15 2005/12/03 11:05:37 max Exp $ *}
{include file="page_title.tpl" title=$lng.lbl_summary_statistics}
{$lng.txt_summary_stats_note}<br /><br />

<!-- IN THIS SECTION -->

{include file="dialog_tools.tpl"}

<!-- IN THIS SECTION -->
<br />

{capture name=dialog}
<table cellpadding="0" cellspacing="2">
<tr>
	<td nowrap="nowrap"><b>{$lng.lbl_total_sales}</b></td>
	<td>{$stats_info.total_sales}</td>
</tr>
<tr>
	<td nowrap="nowrap"><b>{$lng.lbl_total_unapproved_sales}</b></td>
	<td>{$stats_info.unapproved_sales}</td>
</tr>
<tr>
	<td nowrap="nowrap"><b>{$lng.lbl_pending_sale_commissions}</b></td>
	<td>{include file="currency.tpl" value=$stats_info.pending_commissions}</td>
</tr>
<tr>
	<td nowrap="nowrap"><b>{$lng.lbl_approved_sale_commissions}</b></td>
	<td>{include file="currency.tpl" value=$stats_info.approved_commissions}</td>
</tr>
<tr>
	<td nowrap="nowrap"><b>{$lng.lbl_paid_sales_commissions}</b></td>
	<td>{include file="currency.tpl" value=$stats_info.paid_commissions}</td>
</tr>
</table>
{/capture}
{include file="dialog.tpl" content=$smarty.capture.dialog title=$lng.lbl_summary_statistics extra='width="100%"'}
