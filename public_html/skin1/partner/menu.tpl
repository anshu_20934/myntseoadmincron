{* $Id: menu.tpl,v 1.17 2005/12/03 11:05:36 max Exp $ *}
{capture name=menu}
<a href="banner_info.php" class="VertMenuItems">{$lng.lbl_banners_statistics}</a><br />
<a href="referred_sales.php" class="VertMenuItems">{$lng.lbl_referred_sales}</a><br />
<a href="stats.php" class="VertMenuItems">{$lng.lbl_summary_statistics}</a><br />
<a href="payment_history.php" class="VertMenuItems">{$lng.lbl_payment_history}</a><br />
<a href="howto.php" class="VertMenuItems">{$lng.lbl_banner_html_code}</a><br />
<a href="search.php" class="VertMenuItems">{$lng.lbl_product_html_code}</a><br />
{if $config.XAffiliate.partner_enable_level eq 'Y'}
<a href="affiliates.php" class="VertMenuItems">{$lng.lbl_affiliates_tree}</a><br />
{/if}
{/capture}
{ include file="menu.tpl" dingbats="dingbats_categorie.gif" menu_title=$lng.lbl_management menu_content=$smarty.capture.menu }
