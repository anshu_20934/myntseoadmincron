// $Id: reset.js,v 1.2 2005/12/01 14:28:19 max Exp $

function reset_form(formname, localDef) {
	var x, y, z, obj, localDef, selectedItems, hash_radio;
	var form = document.forms.namedItem(formname);
	if (!form)
		return false;

	var hash_radio = new Array();

	for (x = 0; x < form.elements.length; x++) {
		obj = form.elements[x];
		if ((obj.tagName == 'INPUT' || obj.tagName == 'SELECT' || obj.tagName == 'TEXTAREA') && obj.name != '' && obj.type != 'hidden') {
			var reset_value = '';
			var found = false;
			for (y = 0; y < localDef.length; y++) {
				if (obj.name == localDef[y][0] || obj.id == localDef[y][0]) {
					reset_value = localDef[y][1];
					found = true;
					break;
				}
			}

			if (!found)
				continue;

			if (obj.tagName == 'SELECT') {
				obj.selectedIndex = obj.multiple ? -1 : 0;
				reset_value = reset_value.valueOf();
				selectedItems = new Array();
				if (reset_value.length > 0)
					selectedItems = reset_value.split(',');
				for (z = 0; z < obj.options.length; z++) {
					for (y = 0; y < selectedItems.length; y++) {
						if (obj.options[z].value == selectedItems[y] || obj.options[z].text == selectedItems[y]) {
							obj.options[z].selected = true;
						}
					}
				}
			} else if (obj.tagName == 'INPUT' && obj.type == 'radio') {
				var is_found = false;
				for (z = 0; z < hash_radio.length; z++) {
					if (hash_radio[z][0] == obj.name) {
						is_found = hash_radio[z][1];
						break;
					}
				}
				if (is_found == 'F')
					continue;
				obj.checked = (obj.value == reset_value || is_found === false);
				hash_radio[hash_radio.length] = new Array(obj.name, (obj.value == reset_value) ? "F" : "");
			} else if (obj.tagName == 'INPUT' && obj.type == 'checkbox') {
				obj.checked = reset_value;
			} else {
				obj.value = reset_value;
			}

			if (!obj.disabled) {
				if(obj.onclick)
					obj.onclick.apply(obj);
				if (obj.onchange)
					obj.onchange.apply(obj);
			}
		}
	}
}

/* date validation for order history */
/**
 * DHTML date validation script. Courtesy of SmartWebby.com (http://www.smartwebby.com/dhtml/)
 */
// Declaring valid date character, minimum year and maximum year
var dtCh= "/";
var minYear=1900;
var maxYear=2100;

function isInteger(s){
	var i;
    for (i = 0; i < s.length; i++){   
        // Check that current character is number.
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) return false;
    }
    // All characters are numbers.
    return true;
}

function stripCharsInBag(s, bag){
	var i;
    var returnString = "";
    // Search through string's characters one by one.
    // If character is not in bag, append to returnString.
    for (i = 0; i < s.length; i++){   
        var c = s.charAt(i);
        if (bag.indexOf(c) == -1) returnString += c;
    }
    return returnString;
}

function daysInFebruary (year){
	// February has 29 days in any year evenly divisible by four,
    // EXCEPT for centurial years which are not also divisible by 400.
    return (((year % 4 == 0) && ( (!(year % 100 == 0)) || (year % 400 == 0))) ? 29 : 28 );
}
function DaysArray(n) {
	for (var i = 1; i <= n; i++) {
		this[i] = 31
		if (i==4 || i==6 || i==9 || i==11) {this[i] = 30}
		if (i==2) {this[i] = 29}
   } 
   return this
}

function isDate(dtStr){
	var daysInMonth = DaysArray(12)
	var pos1=dtStr.indexOf(dtCh)
	var pos2=dtStr.indexOf(dtCh,pos1+1)
	var strMonth=dtStr.substring(0,pos1)
	var strDay=dtStr.substring(pos1+1,pos2)
	var strYear=dtStr.substring(pos2+1)
	strYr=strYear
	if (strDay.charAt(0)=="0" && strDay.length>1) strDay=strDay.substring(1)
	if (strMonth.charAt(0)=="0" && strMonth.length>1) strMonth=strMonth.substring(1)
	for (var i = 1; i <= 3; i++) {
		if (strYr.charAt(0)=="0" && strYr.length>1) strYr=strYr.substring(1)
	}
	month=parseInt(strMonth)
	day=parseInt(strDay)
	year=parseInt(strYr)
	if (pos1==-1 || pos2==-1){
		alert("The date format should be : mm/dd/yyyy")
		return false
	}
	if (strMonth.length<1 || month<1 || month>12){
		alert("Please enter a valid month")
		return false
	}
	if (strDay.length<1 || day<1 || day>31 || (month==2 && day>daysInFebruary(year)) || day > daysInMonth[month]){
		alert("Please enter a valid day")
		return false
	}
	if (strYear.length != 4 || year==0 || year<minYear || year>maxYear){
		alert("Please enter a valid 4 digit year ") //between "+minYear+" and "+maxYear)
		return false
	}
	if (dtStr.indexOf(dtCh,pos2+1)!=-1 || isInteger(stripCharsInBag(dtStr, dtCh))==false){
		alert("Please enter a valid date")
		return false
	}
	return true
}
function dateDiff(yr1,mon1,day1,yr2,mon2,day2){
	var from=new Date(yr1,mon1,day1)
	var to=new Date(yr2,mon2,day2)
	if (from.getTime() >  to.getTime())
	{
		 alert("Starting date cannot be greater than end date");
		 return false;
	}
	else
	   return true;
}	
	



