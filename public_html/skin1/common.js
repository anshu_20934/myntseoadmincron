// $Id: common.js,v 1.12.2.4 2006/06/02 13:54:18 max Exp $

//
// Enviroment identificator
//
var localIsDOM = document.getElementById?true:false;
var localIsJava = navigator.javaEnabled();
var localIsStrict = document.compatMode=='CSS1Compat';
var localPlatform = navigator.platform;
var localVersion = "0";
var localBrowser = "";
var localBFamily = "";
if (window.opera && localIsDOM) {
	localBFamily = localBrowser = "Opera";
	if (navigator.userAgent.search(/^.*Opera.([\d.]+).*$/) != -1)
		localVersion = navigator.userAgent.replace(/^.*Opera.([\d.]+).*$/, "$1");
	else if (window.print)
		localVersion = "6";
	else
		localVersion = "5";
} else if (document.all && document.all.item)
	localBFamily = localBrowser = 'MSIE';
if (navigator.appName=="Netscape") {
	localBFamily = "NC";
	if (!localIsDOM) {
		localBrowser = 'Netscape';
		localVersion = navigator.userAgent.replace(/^.*Mozilla.([\d.]+).*$/, "$1");
		if(localVersion != '')
			localVersion = "4";
	} else if(navigator.userAgent.indexOf("Safari") >= 0)
		localBrowser = 'Safari';
	else if (navigator.userAgent.indexOf("Netscape") >= 0)
		localBrowser = 'Netscape';
	else if (navigator.userAgent.indexOf("Firefox") >= 0)
		localBrowser = 'Firefox';
	else 
		localBrowser = 'Mozilla';
	
}
if (navigator.userAgent.indexOf("MSMSGS") >= 0)
	localBrowser = "WMessenger";
else if (navigator.userAgent.indexOf("e2dk") >= 0)
	localBrowser = "Edonkey";
else if (navigator.userAgent.indexOf("Gnutella") + navigator.userAgent.indexOf("Gnucleus") >= 0)
	localBrowser = "Gnutella";
else if (navigator.userAgent.indexOf("KazaaClient") >= 0)
	localBrowser = "Kazaa";

if (localVersion == '0' && localBrowser != '') {
	var rg = new RegExp("^.*"+localBrowser+".([\\d.]+).*$");
	localVersion = navigator.userAgent.replace(rg, "$1");
}
var localIsCookie = ((localBrowser == 'Netscape' && localVersion == '4')?(document.cookie != ''):navigator.cookieEnabled);

//
// Opener/Closer HTML block
//
function visibleBox(id,skipOpenClose) {
	elm1 = document.getElementById("open"+id);
	elm2 = document.getElementById("close"+id);
	elm3 = document.getElementById("box"+id);

	if(!elm3)
		return false;

	if (skipOpenClose) {
		elm3.style.display = (elm3.style.display == "")?"none":"";
	} else if(elm1) {
		if (elm1.style.display == "") {
			elm1.style.display = "none";
			if(elm2)
				elm2.style.display = "";
			elm3.style.display = "none";
		} else {
			elm1.style.display = "";
			if(elm2)
				elm2.style.display = "none";
			elm3.style.display = "";
		}
	}
}

/*
	URL encode
*/
function urlEncode(url) {
	return url.replace(/\s/g, "+").replace(/&/, "&amp;").replace(/"/, "&quot;")
}

/*
	Math.round() wrapper
*/
function round(n, p) {
	if (isNaN(n))
		n = parseFloat(n);
	if (!p || isNaN(p))
		return Math.round(n);
	p = Math.pow(10, p);
	return Math.round(n*p)/p;
}

/*
	Price format
*/
function price_format(price) {
	if (number_format_point > 0) {
		precision = number_format_point;
		precision = Math.pow(10, precision);
		price = Math.round(price*precision)/precision;
		var top = Math.floor(price);
		var bottom = Math.round((price-top)*precision)+precision;
	} else {
		var top = Math.round(price);
		var bottom = 0;
	}
	top = top+"";
	bottom = bottom+"";
	var cnt = 0;
	for (var x = top.length; x >= 0; x--) {
		if (cnt % 3 == 0 && cnt > 0 && x > 0)
			top = top.substr(0, x)+number_format_th+top.substr(x, top.length);
		cnt++;
	}
	if (bottom > 0)
		return top+number_format_dec+bottom.substr(1, bottom.length);
	else
		return top;
}

/*
	Substitute
*/
function substitute(lbl) {
var x, rg;
	for(x = 1; x < arguments.length; x+=2) {
		if(arguments[x] && arguments[x+1]) {
			rg = new RegExp("\\{\\{"+arguments[x]+"\\}\\}", "gi");
			lbl = lbl.replace(rg,  arguments[x+1]);
			rg = new RegExp('~~'+arguments[x]+'~~', "gi");
			lbl = lbl.replace(rg,  arguments[x+1]);
		}
	}
	return lbl;
}

function getWindowWidth(w) {
	if (!w)
		w = window;
    if (localBFamily == "MSIE")
		return w.document.body.clientWidth;
    else 
		return w.innerWidth;
}

function getWindowHeight(w) {
	if (!w)
		w = window;
    if (localBFamily == "MSIE")
		return w.document.body.clientHeight;
	else
		return w.innerHeight;
}

function getDocumentHeight(w){
	if (!w)
		 w = window;
	if (localBFamily == "MSIE" || (localBFamily == "Opera" && localVersion >= 7 && localVersion < 8))
		return isStrict ? w.document.documentElement.scrollHeight : w.document.body.scrollHeight;
	if (localBFamily == "NC")
		return w.document.height
	if (localBFamily == "Opera")
		return w.document.body.style.pixelHeight
}

/*
	Check list of checkboxes
*/
function checkMarks(form, reg, lbl) {
var is_exist = false;

	if (form.elements.length == 0)
		return true;

	for (var x = 0; x < form.elements.length; x++) {
		if (form.elements[x].name.search(reg) == 0 && form.elements[x].type == 'checkbox' && !form.elements[x].disabled) {
			is_exist = true;
			if (form.elements[x].checked)
				return true;
		}
	}

	if (!is_exist)
		return true;
	else if (lbl)
		alert(lbl);
	else if (lbl_no_items_have_been_selected)
		alert(lbl_no_items_have_been_selected);
	return false;
}

/*
	Submit form with specified value of 'mode' parmaeters
*/
function submitForm(formObj, formMode) {
	if (!formObj)
		return false;

	if (formObj.tagName != "FORM") {
		if (!formObj.form)
			return false;
		formObj = formObj.form;
	}
	if (formObj.mode)
		formObj.mode.value = formMode;

	formObj.submit();
}

/*
	Analogue of PHP function sleep()
*/
function sleep(msec) {
	var then = new Date().getTime()+msec;
	while (then >= new Date().getTime()){
	}
}

/*
	Convert number from current format
	(according to 'Input and display format for floating comma numbers' option)
	to float number
*/
function convert_number(num) {
	var regDec = new RegExp(reg_quote(number_format_dec), "gi");
	var regTh = new RegExp(reg_quote(number_format_th), "gi");
	var pow = Math.pow(10, parseInt(number_format_point));
	num = parseFloat(num.replace(" ", "").replace(regTh, "").replace(regDec, "."));
	return Math.round(num*pow)/pow;
}

/*
	Check string as number
	(according to 'Input and display format for floating comma numbers' option)
*/
function check_is_number(num) {
	var regDec = new RegExp(reg_quote(number_format_dec), "gi");
	var regTh = new RegExp(reg_quote(number_format_th), "gi");
	num = num.replace(" ", "").replace(regTh, "").replace(regDec, ".");
	return (num.search(/^[0-9]+(\.[0-9]+)?$/) != -1);
}

/*
	Qutation for RegExp class
*/
function reg_quote(s) {
	return s.replace(/\./g, "\\.").replace(/\//g, "\\/").replace(/\*/g, "\\*").replace(/\+/g, "\\+").replace(/\[/g, "\\[").replace(/\]/g, "\\]");
}

function setCookie(name, value) {
	var date = new Date();
	date.setFullYear(date.getYear()+1);
	document.cookie = name+"="+escape(value)+"; expires="+date.toGMTString();
}

function deleteCookie(name) {
	document.cookie = name+"=0; expires=Fri, 31 Dec 1999 23:59:59 GMT;";
}

/*
	Clone object
*/
function cloneObject(orig) {
	var r = {};
	for (var i in orig) {
		r[i] = orig[i];
	}

	return r;
}

/*
	Get first checkbox and redirect to URL
*/
function getFirstCB(form, reg) {

	while (form.tagName && form.tagName.toUpperCase() != 'FORM')
		form = form.parentNode;

	if (!form.tagName || form.tagName.toUpperCase() != 'FORM' || form.elements.length == 0)
        return false;

	var selectedChk = false;
    for (var x = 0; x < form.elements.length; x++) {
        if (form.elements[x].name.search(reg) == 0 && form.elements[x].type == 'checkbox' && !form.elements[x].disabled && form.elements[x].checked) {
			selectedChk = form.elements[x];
			break;
        }
    }
    
    if (!selectedChk) {
		if (lbl_no_items_have_been_selected)
			alert(lbl_no_items_have_been_selected);

        return false;
	}

    return selectedChk;
}
/* new function added */
function  checkFields(Obj){
   
	if(Obj.value == ""){
       alert("Product type name cannot be left blank!!");
	   return false;
	}

}

// date validation

/**
 * DHTML date validation script. Courtesy of SmartWebby.com (http://www.smartwebby.com/dhtml/)
 */
// Declaring valid date character, minimum year and maximum year
var dtCh= "/";
var minYear=1900;
var maxYear=2100;

function isInteger(s){
	var i;
    for (i = 0; i < s.length; i++){   
        // Check that current character is number.
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) return false;
    }
    // All characters are numbers.
    return true;
}

function stripCharsInBag(s, bag){
	var i;
    var returnString = "";
    // Search through string's characters one by one.
    // If character is not in bag, append to returnString.
    for (i = 0; i < s.length; i++){   
        var c = s.charAt(i);
        if (bag.indexOf(c) == -1) returnString += c;
    }
    return returnString;
}

function daysInFebruary (year){
	// February has 29 days in any year evenly divisible by four,
    // EXCEPT for centurial years which are not also divisible by 400.
    return (((year % 4 == 0) && ( (!(year % 100 == 0)) || (year % 400 == 0))) ? 29 : 28 );
}
function DaysArray(n) {
	for (var i = 1; i <= n; i++) {
		this[i] = 31
		if (i==4 || i==6 || i==9 || i==11) {this[i] = 30}
		if (i==2) {this[i] = 29}
   } 
   return this
}

function isDate(dtStr){
	var daysInMonth = DaysArray(12)
	var pos1=dtStr.indexOf(dtCh)
	var pos2=dtStr.indexOf(dtCh,pos1+1)
	var strMonth=dtStr.substring(0,pos1)
	var strDay=dtStr.substring(pos1+1,pos2)
	var strYear=dtStr.substring(pos2+1)
	strYr=strYear
	if (strDay.charAt(0)=="0" && strDay.length>1) strDay=strDay.substring(1)
	if (strMonth.charAt(0)=="0" && strMonth.length>1) strMonth=strMonth.substring(1)
	for (var i = 1; i <= 3; i++) {
		if (strYr.charAt(0)=="0" && strYr.length>1) strYr=strYr.substring(1)
	}
	month=parseInt(strMonth)
	day=parseInt(strDay)
	year=parseInt(strYr)
	if (pos1==-1 || pos2==-1){
		alert("The date format should be : mm/dd/yyyy")
		return false
	}
	if (strMonth.length<1 || month<1 || month>12){
		alert("Please enter a valid month")
		return false
	}
	if (strDay.length<1 || day<1 || day>31 || (month==2 && day>daysInFebruary(year)) || day > daysInMonth[month]){
		alert("Please enter a valid day")
		return false
	}
	if (strYear.length != 4 || year==0 || year<minYear || year>maxYear){
		alert("Please enter a valid 4 digit year between "+minYear+" and "+maxYear)
		return false
	}
	if (dtStr.indexOf(dtCh,pos2+1)!=-1 || isInteger(stripCharsInBag(dtStr, dtCh))==false){
		alert("Please enter a valid date")
		return false
	}
return true
}

function ValidateEventForm(frmobj){
	var dt=frmobj
	if (isDate(dt.evtdate.value)==false){
		dt.evtdate.focus()
		return false
	}else if(dt.eventname.value == ""){
	     alert("Event name must be entered ")
	     dt.eventname.focus();
		 return false
	}
	dt.mode.value = 'add';
    return true
 }

function validateCustumization(frmobj,val){
	var dt=frmobj
	if (dt.area_name.value==""){
		alert("Area name must be entered")
		dt.area_name.focus()
		return false
	}else if(dt.area_label.value == ""){
	     alert("Area label must be entered ")
	     dt.area_label.focus();
		 return false
	}
	dt.mode.value = val;
    return true
 }


//to  manage multiple popup windows
popupWins = new Array();
function windowOpener(url, name, args) {

    if(typeof popupWins[name] != "object" ){
        popupWins[name] = window.open(url,name,args);
    } else {
        if (!popupWins[name].closed){
            popupWins[name].location.href = url;
        } else {
            popupWins[name] = window.open(url, name,args);
        }
    }

    popupWins[name].focus();
}