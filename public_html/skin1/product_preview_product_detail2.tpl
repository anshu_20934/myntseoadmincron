<div class="product-block">
	<div class="product-image-preview blue-bg left" id="preview_image_div">
		<div class="imageframe">
		{if $productid>0}
		<img  src="{$http_location}/skin1/images/spacer.gif" alt="loading product preview" 
			  width="390" height="390" id="finalimage" style="margin:30;pxposition:relative;z-index:1;">
			  
		<div id="ajax_load">
			<img src="{$cdn_base}/skin1/mkimages/ajax-loader.gif" alt="loader">
		</div>
		
		{else}
		<img  src="{$defaultImage}" alt="loading product preview" 
			  width="390" height="390" id="finalimage" style="margin:30;pxposition:relative;z-index:1;">
		{/if}
		</div>
	</div>
	

    <div class="left clearfix">
		<div class="v-tabs">
			<div class="left">
			{section name="area" loop=$area_icons}
				<!-- <span>{$area_icons[area].name}</span> -->
				<div id="area_{$area_icons[area].name}" {if $area_icons[area].name eq $curr_sel_area} class="selected" {/if}>
					<!-- a href="javascript:void(0)" onclick="javascript:changeAreaEx('{$area_icons[area].name}')" -->
					<a href="javascript:void(0)" onclick="javascript:changePreviewImage('{$area_icons[area].image}')">
						<img width="40" height="40" src="{$area_icons[area].area_icon}" alt="">
					</a>
				</div>
			{/section}
			</div>
		</div>
    </div>
    <input type="hidden" id="styledescription" name="description" value="({$productTypeDetails|escape}) {$productStyleDetails[0][3]|escape}" >
</div>

<div class="">{include file="select_products_product_detail2.tpl" }</div>
<script type="text/javascript">
	var currSelectedArea = "{$curr_sel_area}";
</script>
{literal}
	<script>
		function changeAreaEx_CB(data)
		{
			//check whether the area is customizable or not(has orientation or not)
			var res = data.trim();
			var result = res.split("##");
			if(result[1] == 1){
				showCustomizePanel();
			}else{
				showProductsPanel();
			}
			changeDisplayOrientations(windowid,curr_style_id);
			//ajaxCounter--;trackCustProgress();
		}
		function changeAreaEx(areaname)
		{
			if(currSelectedArea == areaname)
			{
				return;
			}
			$("#area_"+currSelectedArea).removeClass("selected");
			$("#area_"+areaname).addClass("selected");
			currSelectedArea = areaname;
			ajaxCall.get("change_area","./mkchangeselectedarea.php?windowid="+windowid+"&name="+areaname,changeAreaEx_CB);
		}
		//changed funtion
		function showCustomizePanel(){
			$("#customizeTab").show();
			if($("#customizeTab").children().hasClass("current")){
				$("#productsPanel").hide();
				$("#customizePanel").show();
			}
			else
				$("#productsTab a").trigger("click");
		}
		function showProductsPanel(){
			if($("#customizeTab").css("display") != 'none')
				$("#productsPanel").show();
			$("#customizeTab").hide();
			$("#customizePanel").hide();
			$("#productsTab a").trigger("click");
		}
		//end
	</script>
{/literal}
