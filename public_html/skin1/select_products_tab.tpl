
<!--<p>#1</p>
 the tabs -->
 <div class="title-style" id="tab_title">{if $product_style_groups[0].group_name}{$product_style_groups[0].group_name}{/if}</div>
<ul class="tabs sp">

{section name=num loop=$product_style_groups}
    {assign var=tab_label value=$product_style_groups[num].group_name|escape:"quotes"}
    {assign var=style_group_id value=$product_style_groups[num].style_group_id}
	    {if $productStyleIconImages[$style_group_id][0]}
	        <li> <a href="#" id="tabs_ul_li_a_{$product_style_groups[num].style_group_id}" class="sub" onclick="$('#tab_title').html('{$tab_label}')">{$product_style_groups[num].group_name|truncate:10:"..":true}</a></li>
	    {/if}
{/section}

</ul>
<!-- tab "panes" -->
{section name=num_top loop=$product_style_groups}
{assign var=style_group_id value=$product_style_groups[num_top].style_group_id }
{if $productStyleIconImages[$style_group_id][0]}
<div class="pane" style="padding-left:3px; padding-right:1px;">
 <!-- "previous page" action -->
   <a class="prevPage browse left"></a>
    <!-- root element for scrollable -->
       <div class="scrollable">
            <!-- root element for the items -->
             <div class="items">
			 {assign var=style_id value=$product_style_groups[num_top].style_group_id}
				 {section name=num loop=$productStyleIconImages[$style_id][0]}
				    <img  class="{if $productStyleIconImages[$style_id][0][num].id eq $productStyleId}active{/if}" src="{$cdn_base}/{$productStyleIconImages[$style_id][0][num].image_i}" width="40" height="40" ALT="Choose Style  for {$productStyleIconImages[$style_id][0][num].name}" title="{$productStyleIconImages[$style_id][0][num].name}" onclick="javascript:changeStyle({$productStyleIconImages[$style_id][0][num].id});trackStyleChange({$productStyleIconImages[$style_id][0][num].id});" >
		         {/section}
            </div>
      </div>
        <!-- "next page" action -->
     <a class="nextPage browse right"></a>
</div>
{/if}
   {/section}
<div class="description">
	{if $productTypeDetails}

	<div >
	    {$productTypeDetails}
	</div>
    <div style="text-align:right;padding-top:15px;cursor:pointer"><span><a  onclick="$('#stylepopup').show();_gaq.push(['_trackEvent', 'personalize_pdp_{$producttypelabel}'],'read_more_description');" >Read more</a></span></div>
	{/if}
{if $isPrintDescription}
	<div>
	<span>Print Description</span> :  This 100% knitted cotton collared t-shirt is of 240 GSM and is available in various sizes such as Small, Medium, Large, 
	Extra Large and XXL. Please refer to size chart for more details.
	<span><a href="#">Read more.</a></span>
	</div>
{/if}
</div>

{if ($l_image_width ne 0 && $l_image_height ne 0 && $isImageAllowed)}
    <div class="description">
	    <div style="margin:0 0 5px;">
		        <p style="margin:0px" id="image_desc_acc_orientation">(For best results, upload an image of {$l_image_width}px * {$l_image_height}px (width*height) or greater value. For any queries please feel free to mail- support@myntra.com)
  </p>
 </div>
</div>

{/if}
<div class="clear"></div>
{literal}
	<script type="text/javascript">
	var stylecount=1;
	function trackStyleChange(styleid){
		{/literal}
			_gaq.push(['_trackEvent', 'personalize_pdp_{$producttypelabel}', 'switch_style_'+(stylecount++),styleid]);
		{literal}
	}
	</script>
{/literal}