{literal}
 <script type="text/javascript">
function changePreview(key,price,prvimg,stylename,styleid){
	  document.getElementById('stylename').value=stylename; 
      document.getElementById('styleid').value=styleid;
      document.getElementById('gkey').value=key;
	  document.getElementById('bg_image_'+key).style.backgroundImage = "url("+http_loc+"/"+prvimg+")";
	  document.getElementById('price_'+key).innerHTML = "Rs. "+price+"/-";
}
function createyourown(urlkey,k,styleid,name){
	if(parseInt(k) == parseInt(document.getElementById('gkey').value) && document.getElementById('gkey').value != ""){
	    name = document.getElementById('stylename').value;
		styleid = document.getElementById('styleid').value;
	}
	window.location.href=http_loc+"/"+urlkey+"/"+name+"/"+styleid;
}
 </script>
{/literal}

<div class="stylelist" style="margin:0px;"> 
{foreach key=key name=outer item=outer from=$stylegroupinfo}
    {assign var="price" value=0}
  	{assign var="image" value=""}
  	{assign var="groupname" value=""}
  	{foreach key=innerkey item=inner from=$outer}
     {if $inner.is_default eq 1}
        {assign var="price" value=$inner.price}  	
	    {assign var="image" value=$inner.image_path}
	    {assign var="groupname" value=$inner.group_name}
	    {assign var="stylename" value=$inner.stylename|replace:' ':'-'}
	    {assign var="styleid" value=$inner.style_id}
        {if $inner.description ne ''}
	     	{assign var="showdesc" value=$inner.description|regex_replace:'/[\r\t\n]/':' '|escape:'quotes'|escape:'html'}
	    {else}
	     	{assign var="showdesc" value=0}
	    {/if}
        
     {/if}
  {/foreach}  
   <div  class="listbox" >
   	   <div class="textbox" style="font-size:1.2em;font-weight:bold;width:100%;text-align:center;color:#020209">{$groupname}</div>
	   <div   class="imagebox" >
		 
		         <div  id="bg_image_{$key}" style="background-image: url({$http_location}/{$image});background-position: center;background-repeat: no-repeat;height:269px;width:276px;position:relative;margin-left:10px;_margin-left:0px;"  onmouseover="javascript:writetxt('{$showdesc}');" onmouseout="javascript:writetxt(0);">
				    <div style="position:absolute;top:60px;left:82px;"><img src="{$cdn_base}/skin1/mkimages/custom_message.gif" /></div>
				 </div>
	  </div>
	  <div class="clearall"></div>
      <div class="textbox" >
      <p><span id="price_{$key}" style="font-size:0.7em;font-weight:bold;color:#020209">Rs. {$price|replace:'.00':''}/-</span></p>
       <p><span style="font-size:0.5em;font-weight:bold;color:#a5a4a4;">select style:</span>&nbsp;
       {section name=idx loop=$outer}
          {assign var="otherstyle" value=$outer[idx].stylename|replace:' ':'-'}	
	      <span style="font-size:0.5em;font-weight:bold"><a id="a_{$key}_{$outer[idx].style_id}" href="javascript:void(0);" onclick="javascript:changePreview({$key},{$outer[idx].price|replace:'.00':''},'{$outer[idx].image_path}','{$otherstyle}',{$outer[idx].style_id});return false;" style="color:#020209;">{$outer[idx].style}</a></span>
	      { if $smarty.section.idx.last}
	        <span style="font-size:0.5em;padding:0px;color:#3b3c3c;"></span>
	      {else}
	      	<span style="font-size:0.5em;padding:0px;color:#3b3c3c;">|</span>
	      {/if}	
	   {/section}
	   </p>
	  <p style="padding-top:7px;">
	   	 <input type="button" class="submit" name="create" value="create now" style="font-size:0.6em;_font-size:0.5em;background-color:#CB0029;" onclick="javascript:createyourown('{$url_key}',{$key},{$styleid},'{$stylename}');return false;" />
	 </p>
	  		  
	</div>
	
</div>

{/foreach}
 </div>
 <form name="hidFrm" action="">
	<input type="hidden" name="stylename" id="stylename" value="" />
 	<input type="hidden" name="styleid" id="styleid" vlue="" />
 	<input type="hidden" name="gkey" id="gkey" vlue="" />
</form>