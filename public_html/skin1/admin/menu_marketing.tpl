{* $Id: menu.tpl,v 1.68.2.2 2006/06/20 05:52:05 svowl Exp $ *}
{capture name=menu}
<li><a href="{$catalogs.admin}/upload_mailer.php" class="VertMenuItems">Upload Mailer Images</a></li>
<li><a href="{$catalogs.admin}/upload_banner.php" class="VertMenuItems">Upload Banner Images</a></li>
<li><a href='{$http_location}/admin/sem/semPromo.php' class="VertMenuItems">Manage SEM Promos</a></li>
<li><a href='{$http_location}/admin/sem/semRouting.php' class="VertMenuItems">Manage Display Ad URLs</a></li>
<li><a href='{$http_location}/admin/leaderboard/manageleaderboard.php' class="VertMenuItems">Manage Leader Boards</a></li>
{/capture}
{include file="menu.tpl" dingbats="dingbats_categorie.gif" menu_title=Marketing menu_content=$smarty.capture.menu }
