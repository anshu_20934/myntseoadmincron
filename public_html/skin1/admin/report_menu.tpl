{* $Id: menu_admin.tpl,v 1.34 2005/11/17 06:55:36 max Exp $ *}
{capture name=menu}
<a href="{$catalogs.admin}/report_sales.php" class="VertMenuItems">{$lng.lbl_sales_report}</a><br />
<a href="{$catalogs.admin}/report_orders.php" class="VertMenuItems">{$lng.lbl_order_report}</a><br />
<a href="{$catalogs.admin}/report_commission.php" class="VertMenuItems">{$lng.lbl_comm_report}</a><br />
<a href="{$catalogs.admin}/report_users.php" class="VertMenuItems">{$lng.lbl_user_report}</a><br />
<a href="{$catalogs.admin}/report_seg_byprofile.php" class="VertMenuItems">{$lng.lbl_mktsegment_profile_report}</a><br />
<a href="{$catalogs.admin}/report_seg_bycity.php" class="VertMenuItems">{$lng.lbl_mktsegment_city_report}</a><br />
<a href="{$catalogs.admin}/report_discount.php" class="VertMenuItems">{$lng.lbl_discount_report}</a><br />
<a href="{$catalogs.admin}/report_promotion.php" class="VertMenuItems">{$lng.lbl_promotion_report}</a><br />
{/capture}
{include file="menu.tpl" dingbats="dingbats_categorie.gif" menu_title=$lng.lbl_reports menu_content=$smarty.capture.menu }