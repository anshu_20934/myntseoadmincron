{ config_load file="$skin_config" }
<html>
<head>
<title>{$lng.txt_site_title}</title>
{ include file="meta.tpl" }
<link rel="stylesheet" href="{$SkinDir}/skin1_admin.css" />
<script type="text/javascript">
    var http_loc = '{$http_location}'
 </script>
</head>
<body {$reading_direction_tag}>
{ include file="rectangle_top.tpl" }
{ include file="head_admin.tpl" }
<!-- main area -->
<table width="100%" cellpadding="0" cellspacing="0" align="center">
<tr>
<td class="VertMenuLeftColumn">


{if $login eq "" }
{ include file="auth.tpl" }
<br />
{else }
{assign var=user_count value=1}
{assign var=operation value=1}
{assign var=order_types value=1}
{foreach from=$roles item=user}
{if ($user.role eq "SA" || $user.role eq "MA"||$user.role eq "CS")&& $user_count==1 }
{assign var=user_count value=$user_count+1}
<br />
{/if}

<!--order search-->
{if ($user.role eq "AD" || $user.role eq "CS" || $user.role eq "OP") && $order_types eq 1}
	{include file="admin/order_type_search.tpl" }
    {assign var=order_types value=$order_types+1}
<br/>
{/if}
<!--order search-->

{if ($user.role eq "OP"|| $user.role eq "CS" ) && $operation==1}
{ include file="admin/menu_operations.tpl" }
{assign var=operation value=$operation+1}
<br />
{/if}

{if $user.role eq "CS"}
{ include file="admin/customer_care_admin.tpl" }
<br />
{/if}

{if $user.role eq "AD"}
{ include file="admin/myntra_admin.tpl" }
<br />
{ include file="admin/system_admin.tpl" }
<br />
{/if}

{if $user.role eq "FI"}
{ include file="admin/menu_reports.tpl" }
<br />
{/if}



{if $user.role eq "MA"}
{ include file="admin/menu_marketing.tpl" }
<br />
{ include file="admin/menu_reports.tpl" }
<br />
{ include file="admin/website_content.tpl" }
<br />
{ include file="admin/website_seo.tpl" }
<br />
{/if}
{if $user.role eq "PM"}
{ include file="admin/product_mgmt.tpl" }
{/if}
{/foreach}
{if $active_modules.XAffiliate ne ''}
{ include file="admin/menu_affiliate.tpl" }
{/if}
{*include file="menu_profile.tpl" *}
<br />
{/if}
{ *include file="admin/help.tpl" *}
<br />
<img src="{$cdn_base}/skin1/images/spacer.gif" width="150" height="1" alt="" />
</td>
<td valign="top">

<!-- central space -->
{include file="location.tpl"}

{include file="dialog_message.tpl"}

{if $smarty.get.mode eq "subscribed"}
{include file="main/subscribe_confirmation.tpl"}
{elseif $auth_status eq "NotAuthorized"}
{include file="main/access_notallowed.tpl"}
 {else}
{* start of actual page content *}
{php}
 $messages = $this->get_template_vars('messages');
 foreach($messages as $message) {
  echo '<p> '.$message.'</p>';
 }
{/php}
<form enctype="multipart/form-data" action="/admin/order_dump.php" method="POST">
    Choose a file to upload:
    <br />
    <br />
    <table>
    <tr>
    <td>
    Collection file :
    </td>
    <td>
    <input name="collection_file" type="file"/>
    </td>
    </tr>
    <tr>
    <td>
    Refund file :
    </td>
    <td>
     <input name="refund_file" type="file"/><br/>
    </td>
    </tr>
    </table> 
 
 <br />
    <select name="gateway">
        <option value="">Select a gateway</option>
        <option value="ccavenue" {if $gateway eq 'ccavenue'} selected="true"{/if} >CC Avenue</option>
        <option value="ebs" {if $gateway eq 'ebs'} selected="true"{/if}>EBS</option>
        <option value="icici" {if $gateway eq 'icici'} selected="true"{/if}>ICICI</option>
        <option value="axis" {if $gateway eq 'axis'} selected="true"{/if}>Axis</option>
        <option value="atom" {if $gateway eq 'atom'} selected="true"{/if}>Atom</option>
        <option value="tekprocess" {if $gateway eq 'tekprocess'} selected="true"{/if}>Tekprocess</option>
        <option value="hdfc" {if $gateway eq 'hdfc'} selected="true"{/if}>HDFC</option>
    </select>
<br />
<br />
     <input type="submit" value="Upload"/>
<br />
 <br />

</form>
{* end of actual page content *}
{/if}
