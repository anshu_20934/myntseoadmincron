$(document).ready(function(){
	if(Myntra.ntfFlag == 'true'){
		$('h1.ntfSuccess').show();
	}
	$('#deleteNotifications .genDel a').on('click',function(evt){
		$('#deleteNotifications form input').val($(evt.target).attr('data-notification-id'));
		if(window.confirm('Are you sure you want to delete this notification?')){
			$('#deleteNotifications form').submit();
		}	
	});
	$('#deleteNotifications .tdMessage a').attr('target','_blank');
});