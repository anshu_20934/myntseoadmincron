Myntra.notifications = (function(){
	function notifications(){
		var _this = this,
			lightbox = Myntra.LightBox('#createTemplate'),
			groupData;

		_this.init = function(){			
			_this.rowEqualHeight();
			$('#createTemplate').find('.resetBtn').trigger('click');
			_this.bindEvents();
			//_this.getGroups();			
		};

		_this.getGroups = function(){
			$.ajax({
				method: 'GET',
				url: '/admin/getNotificationGroups.php',
				cache: false,
				dataType: 'json',
				complete: function(data,status){
					if(status == 'success'){
						groupData = JSON.parse(data.responseText);
						_this.prepareGroupData();
					}
				}
			});
		};

		_this.prepareGroupData = function(){
			var dropDown = $('#createTemplate').find('.ntfGroups select'),
				newOption;
			for(var i=0; i<groupData.length; i++){
				newOption = '<option title="'+ groupData[i].group_desc +'" value="'+ i +'">' + groupData[i].group_name + '</option>';
				dropDown.append(newOption);		
			}
		};

		_this.createRTE = function(obj){
			var config = {
					toolbar:'Basic',
					resize_enabled: false,
					height: 80	
			};
			$(obj).ckeditor(config); 
		};

		_this.validateForm = function(form){
			var jqform = $(form),
				validationFunctions = [],
				validationStatus = true;
			
			validationFunctions.push(function(){
				var textLength = jqform.find('.ntfTitle input').val().length;
				if(!textLength){
					jqform.find('.ntfTitle .error').css('display','block');
					validationStatus = false;	
				}
				else{
					jqform.find('.ntfTitle .error').hide();					
				}
			});

			validationFunctions.push(function(){
				var textLength,
					mockObj = $('#ntfMock'),
					header,message;
				mockObj.html(jqform.find('.editor').val());
				if(!mockObj.text().length){
					validationStatus = false;
					jqform.find('.ntfMessage .error').css('display','block');
				}
				else{
					jqform.find('.ntfMessage .error').hide();
				}
			});

			validateImageExtension = function(){
					var imageExt = arguments[0].split('\\');
					imageExt = imageExt[imageExt.length-1];
					imageExt = imageExt.substring(imageExt.lastIndexOf('.')+1);
					if(imageExt == "jpg" || imageExt == "jpeg" || imageExt == "gif" || imageExt == "bmp" || imageExt== 'png'){
						return true;
					}
					return false;
			};

			validationFunctions.push(function(){
				var cdnImageUrl = jqform.find('.ntfImageUrl input').val();
				if(!cdnImageUrl.length){
					validationStatus = false;
					jqform.find('.ntfImageUrl .error').css('display','block');
					return true;
				}
				else{
					if(!validateImageExtension(cdnImageUrl)){
						validationStatus = false;
						jqform.find('.ntfImageUrl .error').css('display','block');
						return true;
					}
				}
				jqform.find('.ntfImageUrl .error').hide();						
			});

			validationFunctions.push(function(){
				startDate = jqform.find('.sDate').val(),
					endDate = jqform.find('.eDate').val();
				if(endDate==''){
					validationStatus = false;
					jqform.find('.error.e2').css('display','block');
					jqform.find('.error.e1').hide();
				}
				else{
					startDate = startDate.split('-');
					endDate = endDate.split('-');
					if(new Date(startDate[0],startDate[1],startDate[2]) > new Date(endDate[0],endDate[1],endDate[2])){
						validationStatus = false;
						jqform.find('.error.e1').css('display','block');
						jqform.find('.error.e2').hide();
					}
					else{
						jqform.find('.error.e1').hide();
						jqform.find('.error.e2').hide();
					}
				}
			});

			/*validationFunctions.push(function(){
				var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
					userIds = jqform.find('.userIds').val();
				userIds = userIds.split(',');
				for(var i=0; i<userIds.length; i++){
					if(!re.test(userIds[i])){
						console.log(userIds[i],i);
						validationStatus = false;
						jqform.find('.ntfIds .error').css('display','block');
						//return true;
					}
				}
				jqform.find('.ntfIds .error').hide();
			});*/

			for(var i=0; i<validationFunctions.length; i++){
				if(typeof validationFunctions[i] === 'function'){
					validationFunctions[i]();
				}
			}

			if(validationStatus){
				return true;
			}			
			else{
				return false;
			}
		};

		_this.populateMessage = function(){
			var messageWrap = $('#createTemplate').find('.ntfMessage'),
				messageMockWrap = $('#ntfMock');
			messageWrap.find('.filteredMessage').val(encodeURI(messageMockWrap.find('p').html()));
		};

		_this.bindEvents = function(){
			$('.createNewNotification').on('click',function(evt){
				evt.preventDefault();
				_this.createRTE('#createTemplate .editor');
				$('.sdate').datepicker({
					dateFormat: 'yy-mm-dd',
					gotoCurrent: true,
					minDate: new Date()
				});
				$('.sdate').datepicker('setDate',new Date());
				var dateObj = new Date();
				$('.edate').datepicker({
					dateFormat: 'yy-mm-dd',
					gotoCurrent: true,
					minDate: new Date(dateObj.getFullYear(), dateObj.getMonth(), dateObj.getDate()+1)
				});
				lightbox.show();				
			});
			$('#createTemplate form').on('submit',function(evt){
				evt.preventDefault();
				if(_this.validateForm('#notificationForm')){
					_this.populateMessage();
					evt.currentTarget.submit();
				}
			});
			$('#createTemplate .refresh').on('click',_this.refreshMockView);
			//$('#createTemplate').find('.ntfGroups select').on('change',_this.populateGroupData);
		};

		_this.refreshMockView = function(evt){
			var formWrapper = $('#createTemplate'),
				mockElement = $('#mockView');
			mockElement.find('h3').text(formWrapper.find('.ntfTitle input').val()); 
			mockElement.find('img').attr('src',formWrapper.find('.ntfImageUrl input').val());
			mockElement.find('.mockNotificationMsg').html(formWrapper.find('.editor').val());
			mockElement.find('p.msg').html(mockElement.find('.mockNotificationMsg p').html()); 
		};

		_this.populateGroupData = function(evt){
			var changeIndex = $(this).val();
			if(changeIndex > -1){
				$('#createTemplate').find('.ntfIds textarea').val(groupData[changeIndex].user_ids);	
			}
			else{
				$('#createTemplate').find('.ntfIds textarea').val('');
			}			
		};

		_this.rowEqualHeight = function(){
			if(!arguments.length){
				var parent = $('.td').parent();
				parent.each(function(i){
					var maxHeight = 0,
						currElement = $(this).find('.td');
					currElement.each(function(){
						if($(this).height() > maxHeight){
							maxHeight = $(this).height();
						}
					});
					currElement.css('height',maxHeight+'px');
				});
			}
			else if($(arguments[0]).parent().hasClass('.tr')){
				var currElement = $(arguments[0]),
					maxHeight = currElement.height(),
					flag = false,
					siblings = currElement.siblings();
				siblings.each(function(i){
					if($(this).height() > maxHeight){
						maxHeight = $(this).height();
						flag = true;
					}
				});
				siblings.css('height',maxHeight+'px');
				if(flag){
					currElement.css('height',maxHeight+'px');
				}
			}			
		};
	}

	return new notifications();

})();

$(document).ready(function(){
	if(Myntra.ntfFlag == 'true'){
		$('h1.ntfSuccess').show();
	}
	Myntra.notifications.init();
});