var validateForm = function(form){
	var jqForm = $(form),
		validationFunctions = [],
		validationStatus = true;
	validationFunctions.push(function(){
		var currElement = jqForm.find('.groupName'),
			groupText = currElement.find('input').val(),
			re = /([^\s])/;
		if(!re.test(groupText)){
			validationStatus = false;
			currElement.find('.error').css('display','block');
		}
		else{
			currElement.find('.error').hide();			
		}
	});

	validationFunctions.push(function(){
		var currElement = jqForm.find('.csvFile'),
			csvFile = currElement.find('input').val();
		if(csvFile.length){
			csvFile = csvFile.split('\\');
			csvFile = csvFile[csvFile.length-1];
			csvFile = csvFile.substring(csvFile.lastIndexOf('.')+1);
			if(csvFile == 'csv'){
				currElement.find('.error').hide();
			}
			else{
				validationStatus = false;
				currElement.find('.error').css('display','block');	
			}
		}
		else{
			validationStatus = false;
			currElement.find('.error').css('display','block');
		}		
	});

	/*validationFunctions.push(function(){
		var currElement = jqForm.find('.groupType'),
			groupval = currElement.find('.selectBox').val();
		if(groupval == '-1'){
			validationStatus = false;
			currElement.find('.error').css('display','block');
		}
		else{
			currElement.find('.error').hide();
		}
	});*/

	for(var i=0; i<validationFunctions.length; i++){
		if(typeof validationFunctions[i] === 'function'){
			validationFunctions[i]();
		}
	}

	if(validationStatus){
		return true;
	}			
	else{
		return false;
	}
};

$(document).ready(function(){
	if(Myntra.ntfFlag == 'true'){
		$('h1.ntfSuccess').show();
		$('h1.ntfSuccess span').text(Myntra.groupId);
	}
	$('#ugCreateForm').on('submit',function(evt){
		evt.preventDefault();
		if(validateForm('#ugCreateForm')){
			evt.currentTarget.submit();
		}	
	});
	$('#deleteUserGroup .genDel a').on('click',function(evt){
		$('#deleteUserGroup form input').val($(evt.target).attr('data-group-id'));
		if(window.confirm('Are you sure you want to delete this user group?')){
			$('#deleteUserGroup form').submit();
		}
	});

});