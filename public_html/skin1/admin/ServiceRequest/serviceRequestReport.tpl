{* $Id: featured_products.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
<script type="text/javascript" src="/skin1/js_script/calender.js" ></script>
<link rel="stylesheet" href="/skin1/css/calender.css" type="text/css" media="screen" />

{capture name=sr_report}
    <div>
        <div style="float:left;background-color:#f0f0f0;">
            <form name="searchSR"  action="{$http_location}/admin/ServiceRequest/serviceRequestReport.php">
            <input type="hidden" name="pagelimit" value="{$pagelimit}">
            <table cellpadding="3" cellspacing="1" width="100%">
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td colspan="2">Filter By</td>
                                <td colspan="2">
                                    <select name="gfilter">
                                        <option value="0">--select--</option>
                                        {section name=idx loop=$generalFilter}
                                            {if $smarty.get.gfilter eq $generalFilter[idx].num}
                                                <option value="{$generalFilter[idx].num}" selected="selected">{$generalFilter[idx].name}</option>
                                            {else}
                                                <option value="{$generalFilter[idx].num}">{$generalFilter[idx].name}</option>
                                            {/if}
                                        {/section}
                                    </select>
                                </td>
                            </tr>
                            <tr>
                               <td colspan="4">OR</td>
                            </tr>                            
                            <tr>
                                <td>From Date</td>
                                <td>
                                    <input type="text" name="fStartCrDate" readonly="readonly" id="fStartCrDate" value="{$smarty.get.fStartCrDate}"/>
                                    <img border="0" src="{$cdn_base}/images/cal.gif" alt="Click Here to Pick up the date" onclick="displayDatePicker('fStartCrDate');">
                                </td>
                                <td>To Date</td>
                                <td>
                                    <input type="text" name="fEndCrDate" readonly="readonly" id="fEndCrDate" value="{$smarty.get.fEndCrDate}"/>
                                    <img border="0" src="{$cdn_base}/images/cal.gif" alt="Click Here to Pick up the date" onclick="displayDatePicker('fEndCrDate');">
                                </td>
                            </tr>

                            <tr>
                               <td colspan="4">&nbsp;</td>                                
                            </tr>

                            <tr>
                               <td colspan="2">&nbsp;</td>
                                <td colspan="2"><input type="submit" value="Update Dashboard" class="button"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            </form>
        </div>
        <div style="clear:both;"></div>
    </div>

    <br/><hr>
   
    <div style="width:1125px;">
        <table cellpadding="3" cellspacing="1" width="100%">
            <tr class="TableHead">
                <th rowspan="2">Sr Type</th>
                <th colspan="3">SR Closure Rate</th>
                <th colspan="2">Complaint Ratio</th>
                <th colspan="3">Compensated closures summary</th>
            </tr>
            <tr style="background-color:#f0f0f0;" align="center">
                <th>SR registered</th>
                <th>SR Closed</th>
                <th>% SR closed</th>

                <th>Count of Order</th>
                <th>% Complaint Ratio</th>
                
                <th>Count of orders closed with compensation</th>
                <th>% SRs closed with compensation</th>
                <th>Total Compensation amount</th>
            </tr>
           <tr style="background-color:#f0f0f0;" align="center">
                <td>Order</td>
                <td>{$countOrderSR}</td>
                <td>{$countClosedOrderSR}</td>
                <td>{$closeOrderSRPercent}</td>

                <td>{$countOrder}</td>
                <td>{$complaintRatioPercent}</td>

                <td>{$countOrderSRWithComp}</td>
                <td>{$countOrderSRWithCompPercent}</td>
                <td>
                    <table>
                        <tr>
                            <td>Credit =</td>
                            <td align="right">{$compAmountCredit}</td>
                        </tr>
                        <tr>
                            <td>Debit =</td>
                            <td align="right">{$compAmountDebit}</td>
                        </tr>
                        <tr>
                            <td>Total Paid =</td>
                            <td align="right">{$compAmount}</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr style="background-color:#f0f0f0;" align="center">
                <td>Non-Order</td>
                <td>{$countNonOrderSR}</td>
                <td>{$countClosedNonOrderSR}</td>
                <td>{$closeNonOrderSRPercent}</td>

                <td colspan="5"></td>
            </tr>
            <tr style="background-color:#f0f0f0;" align="center">
                <td>Total</td>
                <td>{$countSR}</td>
                <td>{$countClosedSR}</td>
                <td>{$closeSRPercent}</td>

                <td colspan="5"></td>
            </tr>
        </table>
        <table>
             <tr>
                <td align="left"><br/>
                    <a href="{$http_location}/admin/ServiceRequest/serviceRequestReport.php?mode=exportSRReport&{$query_string}">Export to XLS</a>
                </td>
            </tr>
        </table>
    </div>
{/capture}
{include file="dialog.tpl" title="Service Requests Report" content=$smarty.capture.sr_report extra='width="100%"'}