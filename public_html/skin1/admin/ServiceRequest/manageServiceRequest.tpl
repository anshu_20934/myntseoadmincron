{* $Id: featured_products.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
<link href="{$http_location}/skin1/myntra_css/jquery-ui-datepicker.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="{$http_location}/skin1/myntra_js/jquery-ui-datepicker.js"></script>

{literal}    
<style type="text/css">
    .selectTR{background-color:#f0c620}
</style>
<script type="text/javascript">

    $(document).ready(function(){


        //initializing datepicker
        $( "#fStartCrDate, #fEndCrDate, #fStartDuDate, #fEndDuDate, #fStartClDate, #fEndClDate" ).datepicker({
            dateFormat:'mm/dd/yy',
            changeMonth: true,
            changeYear: true,
            showOn: 'both',
            buttonImageOnly: true,
            buttonImage: 'http://myntra.myntassets.com/images/cal.gif',
            buttonText: 'Calendar'
        });

        //initializing datetimepicker
        $( "#fStartCallback, #fEndCallback" ).datetimepicker({
            dateFormat:'mm/dd/yy',
            changeMonth: true,
            changeYear: true,
            showOn: 'both',
            buttonImageOnly: true,
            buttonImage: 'http://myntra.myntassets.com/images/cal.gif',
            buttonText: 'Calendar'
        });
        
        
        $('#SRListTable tr').toggle(function(){
            if(!$(this).hasClass('TableHead')){
                $(this).addClass('selectTR');
            }
        }, function(){
            if(!$(this).hasClass('TableHead')){
                $(this).removeClass('selectTR');

            }

        });

        $('#SRListTable tr a').click(function(){
            location.href = $(this).attr('href');
        });

        $('#exportSR').click(function(){
        
            var crDateObj = $('#fStartCrDate');
            var endDateObj = $('#fEndCrDate');

            if(crDateObj.val() == '' || endDateObj.val() == ''){
                alert("please enter 'Create Date Start' and 'Create Date End' to generate the report");

                if(crDateObj.val() == ''){
                    crDateObj.focus();
                } else {
                    endDateObj.focus();
                }
                return false;

            } else {
                location.href = $(this).attr('href')+'&fStartCrDate='+crDateObj.val()+'&fEndCrDate='+endDateObj.val();
                return false;
            }
            
        });



    });

    function deleteNonOrderSR(nonOrderSRId){
        if(confirm('Are you sure to delete?')){
            makePostAjax(http_loc+'/admin/ServiceRequest/manageNonOrderSR.php', 'mode=deleteNonOrderServieRequest&non_order_sr_id='+nonOrderSRId, false, deleteSRCallBack);
        } else {
            return false;
        }
    }

    function deleteOrderSR(orderSRId){
        if(confirm('Are you sure to delete?')){
            makePostAjax(http_loc+'/admin/manageOrderSR.php', 'mode=deleteServieRequest&order_sr_id='+orderSRId, false, deleteSRCallBack);
        } else {
            return false;
        }
    }

    function makePostAjax(url, inputData, cache, callBackFunc){
        $.ajax({
            type:'POST',
            url: url,
            data: inputData,
            cache:cache,
            success: callBackFunc
        });
    }

    function deleteSRCallBack(data){
        alert(data);
        location.reload();
    }
</script>
{/literal}
{capture name=dialog}
    <div>
        <div style="float:left;background-color:#f0f0f0;">
            <form name="searchSR"  action="{$http_location}/admin/ServiceRequest/manageServiceRequest.php">
            <input type="hidden" name="pagelimit" value="{$pagelimit}">
            <table cellpadding="3" cellspacing="1" width="100%">
                <tr>
                    <td>
                        <table>

                            <tr>
                                <td>Callback Time Start</td>
                                <td>
                                    <input type="text" name="fStartCallback" readonly="readonly" id="fStartCallback" value="{$smarty.get.fStartCallback}"/>
                                </td>
                                <td>Callback Time End</td>
                                <td>
                                    <input type="text" name="fEndCallback" readonly="readonly" id="fEndCallback" value="{$smarty.get.fEndCallback}"/>
                                </td>
                            </tr>

                            <tr>
                                <td>Create Date Start</td>
                                <td>
                                    <input type="text" name="fStartCrDate" readonly="readonly" id="fStartCrDate" value="{$smarty.get.fStartCrDate}"/>
                                </td>
                                <td>Create Date End</td>
                                <td>
                                    <input type="text" name="fEndCrDate" readonly="readonly" id="fEndCrDate" value="{$smarty.get.fEndCrDate}"/>                                    
                                </td>
                            </tr>

                            <tr>
                                <td>Due Date Start</td>
                                <td>
                                    <input type="text" name="fStartDuDate" readonly="readonly" id="fStartDuDate" value="{$smarty.get.fStartDuDate}"/>
                                </td>
                                <td>Due Date End</td>
                                <td>
                                    <input type="text" name="fEndDuDate" readonly="readonly" id="fEndDuDate" value="{$smarty.get.fEndDuDate}"/>                                    
                                </td>
                            </tr>

                            <tr>
                                <td>Close Date Start</td>
                                <td>
                                    <input type="text" name="fStartClDate" readonly="readonly" id="fStartClDate" value="{$smarty.get.fStartClDate}"/>                                    
                                </td>
                                <td>Close Date End</td>
                                <td>
                                    <input type="text" name="fEndClDate" readonly="readonly" id="fEndClDate" value="{$smarty.get.fEndClDate}"/>                                    
                                </td>
                            </tr>

                            <tr>
                                <td colspan="2">SR ID</td>
                                <td colspan="2"><input type="text" name="fSRid" maxlength="11" value="{$smarty.get.fSRid}"/></td>
                            </tr>

                            <tr>
                                <td colspan="2">Customer SR ID</td>
                                <td colspan="2"><input type="text" name="fCSRid" maxlength="10" value="{$smarty.get.fCSRid}"/></td>
                            </tr>

                            <tr>
                                <td colspan="2">Order Id</td>
                                <td colspan="2"><input type="text" name="fSROid" maxlength="11" value="{$smarty.get.fSROid}"/>&nbsp; (relevant only for order SRs..)</td>
                            </tr>

                            <tr>
                                <td colspan="2">Refund</td>
                                <td colspan="2">
                                    <select name="fSRRef">
                                        <option value="0">--All--</option>
                                        {if $smarty.get.fSRRef eq 'Y'}
                                            <option value="Y" selected="selected">Y</option>
                                            <option value="N">N</option>
                                        {elseif $smarty.get.fSRRef eq 'N'}
                                            <option value="Y">Y</option>
                                            <option value="N" selected="selected">N</option>
                                        {else}
                                            <option value="Y">Y</option>
                                            <option value="N">N</option>
                                        {/if}
                                    </select>&nbsp; (relevant only for order SRs..)
                                </td>
                            </tr>

                            <tr>
                                <td colspan="2">Compensation</td>
                                <td colspan="2">
                                    <select name="fSRComp">
                                        <option value="0">--All--</option>
                                        {if $smarty.get.fSRComp eq 'Y'}
                                            <option value="Y" selected="selected">Y</option>
                                            <option value="N">N</option>
                                        {elseif $smarty.get.fSRComp eq 'N'}
                                            <option value="Y">Y</option>
                                            <option value="N" selected="selected">N</option>
                                        {else}
                                            <option value="Y">Y</option>
                                            <option value="N">N</option>
                                        {/if}
                                    </select>&nbsp; (relevant only for order SRs..)
                                </td>
                            </tr>

                            <tr>
                                <td colspan="2">SR Type</td>
                                <td colspan="2">
                                    <select name="fSRType">
                                        <option value="0">--All--</option>
                                        {if $smarty.get.fSRType eq 'order'}
                                            <option value="order" selected="selected">Order</option>
                                            <option value="non-order">Non-Order</option>
                                        {elseif $smarty.get.fSRType eq 'non-order'}
                                            <option value="order">Order</option>
                                            <option value="non-order" selected="selected">Non-Order</option>
                                        {else}
                                            <option value="order">Order</option>
                                            <option value="non-order">Non-Order</option>
                                        {/if}
                                    </select>
                                </td>
                            </tr>

                            <tr>
                                <td colspan="2">Category</td>
                                <td colspan="2">
                                    <select name="fCat">
                                        <option value="0">--All--</option>
                                        {section name=category loop=$sr_category}
                                            {if $sr_category[category].cat_active eq 'Y'}
                                                {if $smarty.get.fCat eq $sr_category[category].id}
                                                    <option value="{$sr_category[category].id}" selected="selected">{$sr_category[category].SR_category}-({$sr_category[category].category_type})</option>
                                                {else}
                                                    <option value="{$sr_category[category].id}">{$sr_category[category].SR_category}-({$sr_category[category].category_type})</option>
                                                {/if}
                                            {/if}
                                        {/section}
                                    </select>
                                </td>
                            </tr>

                            <tr>
                                <td colspan="2">Sub Category</td>
                                <td colspan="2">
                                    <select name="fSCat">
                                        <option value="0">--All--</option>
                                        {section name=category loop=$sr_category}
                                            {if $sr_category[category].cat_active eq 'Y'}
                                                <optgroup label="{$sr_category[category].SR_category}-({$sr_category[category].category_type})"></optgroup>

                                                {assign var=subcategories value=$sr_category[category].subCategories}
                                                {section name=index loop=$subcategories}
                                                    {if $subcategories[index].subcat_active eq 'Y'}
                                                        {if $smarty.get.fSCat eq $subcategories[index].id}
                                                            <option value="{$subcategories[index].id}" selected="selected">{$subcategories[index].SR_subcategory}</option>
                                                        {else}
                                                            <option value="{$subcategories[index].id}">{$subcategories[index].SR_subcategory}</option>
                                                        {/if}
                                                    {/if}
                                                {/section}
                                            {/if}
                                        {/section}
                                    </select>
                                </td>
                            </tr>

                            <tr>
                                <td colspan="2">Department</td>
                                <td colspan="2">
                                    <select name="SR_department">
                                        <option value="0">--All--</option>
                                        {section name=department loop=$sr_department}
                                            {if $smarty.get.SR_department eq $sr_department[department]}
                                                <option value="{$sr_department[department]}" selected="selected">{$sr_department[department]}</option>
                                            {else}
                                                <option value="{$sr_department[department]}">{$sr_department[department]}</option>
                                            {/if}
                                        {/section}
                                    </select>
                                </td>
                            </tr>

                            <tr>
                                <td colspan="2">Channel</td>
                                <td colspan="2">
                                    <select name="SR_channel">
                                        <option value="0">--All--</option>
                                        {section name=channel loop=$sr_channel}
                                            {if $smarty.get.SR_channel eq $sr_channel[channel]}
                                                <option value="{$sr_channel[channel]}" selected="selected">{$sr_channel[channel]}</option>
                                            {else}
                                                <option value="{$sr_channel[channel]}">{$sr_channel[channel]}</option>
                                            {/if}
                                        {/section}
                                    </select>
                                </td>
                            </tr>

                            <tr>
                                <td colspan="2">Ageing</td>
                                <td colspan="2">
                                    <select name="fAge">
                                        <option value="0">--All--</option>
                                        {section name=index loop=$ageingarray}
                                            {if $smarty.get.fAge eq $ageingarray[index]}
                                                <option value="{$ageingarray[index]}" selected="selected">{$ageingarray[index]} or more days from created</option>
                                            {else}
                                                <option value="{$ageingarray[index]}">{$ageingarray[index]} or more days from created</option>
                                            {/if}
                                        {/section}
                                    </select>
                                </td>
                            </tr>

                            <tr>
                                <td colspan="2">Status</td>
                                <td colspan="2">
                                    <select name="fStat">
                                        <option value="0">--All--</option>
                                        {section name=index loop=$sr_status}
                                            {if $sr_status[index].status_active eq 'Y'}
                                                {if $smarty.get.fStat eq $sr_status[index].id}
                                                    <option value="{$sr_status[index].id}" selected="selected">{$sr_status[index].SR_status}</option>
                                                {else}
                                                    <option value="{$sr_status[index].id}">{$sr_status[index].SR_status}</option>
                                                {/if}
                                            {/if}
                                        {/section}
                                    </select>
                                </td>
                            </tr>

                            <tr>
                                <td colspan="2">Priority</td>
                                <td colspan="2">
                                    <select name="fPri">
                                        <option value="0">--All--</option>
                                        {section name=index loop=$sr_priority}
                                            {if $sr_priority[index].priority_active eq 'Y'}
                                                {if $smarty.get.fPri eq $sr_priority[index].id}
                                                    <option value="{$sr_priority[index].id}" selected="selected">{$sr_priority[index].SR_priority}</option>
                                                {else}
                                                    <option value="{$sr_priority[index].id}">{$sr_priority[index].SR_priority}</option>
                                                {/if}
                                            {/if}
                                        {/section}
                                    </select>
                                </td>
                            </tr>

                            <tr>
                                <td colspan="2">Creator</td>
                                <td colspan="2"><input type="text" name="fRepo" maxlength="100" value="{$smarty.get.fRepo}"/></td>
                            </tr>

                            <tr>
                                <td colspan="2">Resolver</td>
                                <td colspan="2"><input type="text" name="fReso" maxlength="100" value="{$smarty.get.fReso}"/></td>
                            </tr>

                            <tr>
                                <td colspan="2">Customer</td>
                                <td colspan="2"><input type="text" name="fCust" maxlength="100" value="{$smarty.get.fCust}"/></td>
                            </tr>

                            <tr>
                               <td colspan="2">&nbsp;</td>
                                <td colspan="2"><input type="submit" value="Search Service Request" class="button"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            </form>
        </div>
        <div style="clear:both;"></div>
    </div>

    <br/>
    <hr>
    <table>
        <tr>
            <td align="left">
                <input type="button" name="Add Non-Order Service Request" value="Add Non-Order Service Request" class="button" onClick="javascript:window.open(http_loc+'/admin/ServiceRequest/manageNonOrderSR.php?mode=addNonOrderServieRequest','null', 'menubar=0,resizable=1,toolbar=0,width=800,height=500')">
                &nbsp;|&nbsp;<a href="{$http_location}/admin/ServiceRequest/manageServiceRequest.php?pg={$pg}&pagelimit={$pagelimit}">Clear Sort and Search</a>
                &nbsp;|&nbsp;<a href="{$http_location}/admin/ServiceRequest/manageServiceRequest.php?mode=exportSR&{$query_string}" id="exportSR">Export to XLS</a>
            </td>
        </tr>
        <tr>
            <td>
                <table cellpadding="2" cellspacing="1" width="100%">
                    <tr>
                        <td width="100px">Records In Page:</td>
                        <td align="left">
                            <select name="pagelimit" onchange="location.href='{$url}&pg=1&pagelimit='+$(this).val();">
                                {section name=idx loop=$limitarray}
                                    {if $smarty.get.pagelimit eq $limitarray[idx]}
                                        <option value="{$limitarray[idx]}" selected=selected>{$limitarray[idx]}</option>
                                    {else}
                                        <option value="{$limitarray[idx]}">{$limitarray[idx]}</option>
                                    {/if}
                                {/section}
                            </select>
                        </td>
                        <td width="700px" align="center">page <b>{$pg}</b> of <b>{$totalpages}</b>&nbsp;[showing <b>{$offset_start}-{$offset_end}</b> of <b>{$total_record}]</b></td>
                        <td align="right">{$paginator}</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <div style="width:1125px;overflow:scroll;height:500px;">
        <table cellpadding="2" cellspacing="1" width="100%" title="scroll horizontally to view all columns" id="SRListTable">

            <tr class="TableHead">
                <th style="padding: 5px;">
                    {if $smarty.get.obSRid eq 'desc'}
                        <img width="7" height="6" alt="" src="{$cdn_base}/skin1/images/r_bottom.gif">
                        <a href="{$url}&sbSRid=SR_id&obSRid=asc" >Id</a>
                    {elseif $smarty.get.obSRid eq 'asc'}
                        <img width="7" height="6" alt="" src="{$cdn_base}/skin1/images/r_top.gif">
                        <a href="{$url}&sbSRid=SR_id&obSRid=desc" >Id</a>
                    {else}
                        <a href="{$url}&sbSRid=SR_id&obSRid=desc" >Id</a>
                    {/if}
                </th>

                <th style="padding: 5px;">
                    Customer SR Id
                </th>

                <th style="padding: 5px;">
                    {if $smarty.get.obCat eq 'desc'}
                        <img width="7" height="6" alt="" src="{$cdn_base}/skin1/images/r_bottom.gif">
                        <a href="{$url}&sbCat=SR_category&obCat=asc" >Category</a>
                    {elseif $smarty.get.obCat eq 'asc'}
                        <img width="7" height="6" alt="" src="{$cdn_base}/skin1/images/r_top.gif">
                        <a href="{$url}&sbCat=SR_category&obCat=desc" >Category</a>
                    {else}
                        <a href="{$url}&sbCat=SR_category&obCat=desc" >Category</a>
                    {/if}
                </th>

                <th style="padding: 5px;">
                    {if $smarty.get.obSCat eq 'desc'}
                        <img width="7" height="6" alt="" src="{$cdn_base}/skin1/images/r_bottom.gif">
                        <a href="{$url}&sbSCat=SR_subcategory&obSCat=asc" >Sub Category</a>
                    {elseif $smarty.get.obSCat eq 'asc'}
                        <img width="7" height="6" alt="" src="{$cdn_base}/skin1/images/r_top.gif">
                        <a href="{$url}&sbSCat=SR_subcategory&obSCat=desc" >Sub Category</a>
                    {else}
                        <a href="{$url}&sbSCat=SR_subcategory&obSCat=desc" >Sub Category</a>
                    {/if}
                </th>

                <th style="padding: 5px;">
                    {if $smarty.get.obPri eq 'desc'}
                        <img width="7" height="6" alt="" src="{$cdn_base}/skin1/images/r_bottom.gif">
                        <a href="{$url}&sbPri=SR_priority&obPri=asc" >Priority</a>
                    {elseif $smarty.get.obPri eq 'asc'}
                        <img width="7" height="6" alt="" src="{$cdn_base}/skin1/images/r_top.gif">
                        <a href="{$url}&sbPri=SR_priority&obPri=desc" >Priority</a>
                    {else}
                        <a href="{$url}&sbPri=SR_priority&obPri=desc" >Priority</a>
                    {/if}
                </th>

                <th style="padding: 5px;">
                    {if $smarty.get.obStat eq 'desc'}
                        <img width="7" height="6" alt="" src="{$cdn_base}/skin1/images/r_bottom.gif">
                        <a href="{$url}&sbStat=SR_status&obStat=asc" >Status</a>
                    {elseif $smarty.get.obStat eq 'asc'}
                        <img width="7" height="6" alt="" src="{$cdn_base}/skin1/images/r_top.gif">
                        <a href="{$url}&sbStat=SR_status&obStat=desc" >Status</a>
                    {else}
                        <a href="{$url}&sbStat=SR_status&obStat=desc" >Status</a>
                    {/if}
                </th>

                <th style="padding: 5px;">
                    Department
                </th>

                <th style="padding: 5px;">
                    Channel
                </th>

                <th style="padding: 5px;">
                    {if $smarty.get.obCbDate eq 'desc'}
                        <img width="7" height="6" alt="" src="{$cdn_base}/skin1/images/r_bottom.gif">
                        <a href="{$url}&sbCbDate=callbacktime&obCbDate=asc" >Callback Time</a>
                    {elseif $smarty.get.obCbDate eq 'asc'}
                        <img width="7" height="6" alt="" src="{$cdn_base}/skin1/images/r_top.gif">
                        <a href="{$url}&sbCbDate=callbacktime&obCbDate=desc" >Callback Time</a>
                    {else}
                        <a href="{$url}&sbCbDate=callbacktime&obCbDate=desc" >Callback Time</a>
                    {/if}
                </th>

                <th style="padding: 5px;">
                    {if $smarty.get.obCrDate eq 'desc'}
                        <img width="7" height="6" alt="" src="{$cdn_base}/skin1/images/r_bottom.gif">
                        <a href="{$url}&sbCrDate=created_date&obCrDate=asc" >Create Date</a>
                    {elseif $smarty.get.obCrDate eq 'asc'}
                        <img width="7" height="6" alt="" src="{$cdn_base}/skin1/images/r_top.gif">
                        <a href="{$url}&sbCrDate=created_date&obCrDate=desc" >Create Date</a>
                    {else}
                        <a href="{$url}&sbCrDate=created_date&obCrDate=desc" >Create Date</a>
                    {/if}
                </th>

                <th style="padding: 5px;">
                    Ageing
                </th>

                <th style="padding: 5px;">
                    {if $smarty.get.obDuDate eq 'desc'}
                        <img width="7" height="6" alt="" src="{$cdn_base}/skin1/images/r_bottom.gif">
                        <a href="{$url}&sbDuDate=due_date&obDuDate=asc" >Due Date</a>
                    {elseif $smarty.get.obDuDate eq 'asc'}
                        <img width="7" height="6" alt="" src="{$cdn_base}/skin1/images/r_top.gif">
                        <a href="{$url}&sbDuDate=due_date&obDuDate=desc" >Due Date</a>
                    {else}
                        <a href="{$url}&sbDuDate=due_date&obDuDate=desc" >Due Date</a>
                    {/if}
                </th>

                <th style="padding: 5px;">
                    Deviation
                </th>

                <th style="padding: 5px;">
                    {if $smarty.get.obRepo eq 'desc'}
                        <img width="7" height="6" alt="" src="{$cdn_base}/skin1/images/r_bottom.gif">
                        <a href="{$url}&sbRepo=reporter&obRepo=asc" >Creator</a>
                    {elseif $smarty.get.obRepo eq 'asc'}
                        <img width="7" height="6" alt="" src="{$cdn_base}/skin1/images/r_top.gif">
                        <a href="{$url}&sbRepo=reporter&obRepo=desc" >Creator</a>
                    {else}
                        <a href="{$url}&sbRepo=reporter&obRepo=desc" >Creator</a>
                    {/if}
                </th>

                <th style="padding: 5px;">Summary</th>

                <th style="padding: 5px;">
                    Updated by
                </th>

                <th style="padding: 5px;">
                    Modified Date
                </th>

                <th style="padding: 5px;">
                    {if $smarty.get.obCust eq 'desc'}
                        <img width="7" height="6" alt="" src="{$cdn_base}/skin1/images/r_bottom.gif">
                        <a href="{$url}&sbCust=customer_email&obCust=asc" >Customer</a>
                    {elseif $smarty.get.obCust eq 'asc'}
                        <img width="7" height="6" alt="" src="{$cdn_base}/skin1/images/r_top.gif">
                        <a href="{$url}&sbCust=customer_email&obCust=desc" >Customer</a>
                    {else}
                        <a href="{$url}&sbCust=customer_email&obCust=desc" >Customer</a>
                    {/if}
                </th>

                <th style="padding: 5px;">Name</th>
                <th style="padding: 5px;">Mobile</th>

                <th style="padding: 5px;">
                    {if $smarty.get.obClDate eq 'desc'}
                        <img width="7" height="6" alt="" src="{$cdn_base}/skin1/images/r_bottom.gif">
                        <a href="{$url}&sbClDate=closed_date&obClDate=asc" >Close Date</a>
                    {elseif $smarty.get.obClDate eq 'asc'}
                        <img width="7" height="6" alt="" src="{$cdn_base}/skin1/images/r_top.gif">
                        <a href="{$url}&sbClDate=closed_date&obClDate=desc" >Close Date</a>
                    {else}
                        <a href="{$url}&sbClDate=closed_date&obClDate=desc" >Close Date</a>
                    {/if}
                </th>

                <th style="padding: 5px;">
                    {if $smarty.get.obResol eq 'desc'}
                        <img width="7" height="6" alt="" src="{$cdn_base}/skin1/images/r_bottom.gif">
                        <a href="{$url}&sbResol=resolver&obResol=asc" >Resolver</a>
                    {elseif $smarty.get.obResol eq 'asc'}
                        <img width="7" height="6" alt="" src="{$cdn_base}/skin1/images/r_top.gif">
                        <a href="{$url}&sbResol=resolver&obResol=desc" >Resolver</a>
                    {else}
                        <a href="{$url}&sbResol=resolver&obResol=desc" >Resolver</a>
                    {/if}
                </th>

                <th style="padding: 5px;">Close Summary</th>
                <th style="padding: 5px;">Order</th>
                <th style="padding: 5px;">Order Status</th>
                <th style="padding: 5px;">Payment Method</th>                
                <th style="padding: 5px;">Delivery Ageing</th>
                <th style="padding: 5px;">Warehouse ID</th>
                <th style="padding: 5px;">Courier</th>
                <th style="padding: 5px;">Waybill Number</th>
                <th style="padding: 5px;">Address</th>
                <th style="padding: 5px;">State</th>
                <th style="padding: 5px;">City</th>
                <th style="padding: 5px;">Pin Code</th>
                <th style="padding: 5px;">DC Info</th>
                <th style="padding: 5px;">Refund</th>
                <th style="padding: 5px;">Refund Type</th>
                <th style="padding: 5px;">Refund Val</th>
                <th style="padding: 5px;">Compensation</th>
                <th style="padding: 5px;">Compensation Type</th>
                <th style="padding: 5px;">Comp Value</th>
                <th style="padding: 5px;">Action</th>
            </tr>
            {foreach item=index from=$all_sr name=SR}
                <tr {cycle values=", class='TableSubHead'"}>
                    <td style="padding: 5px;">{$index.SR_id}</td>
                    <td style="padding: 5px;">{$index.customer_SR_id}</td>
                    <td style="padding: 5px;">{$index.SR_category}</td>
                    <td style="padding: 5px;"><textarea>{$index.SR_subcategory}</textarea></td>
                    <td style="padding: 5px;">{$index.SR_priority}</td>
                    <td style="padding: 5px;">
                        {if $index.SR_status eq 'close'}
                            closed
                        {else}
                            {$index.SR_status}
                        {/if}
                    </td>
                    <td style="padding: 5px;">{$index.SR_department}</td>
                    <td style="padding: 5px;">{$index.SR_channel}</td>
                    <td style="padding: 5px;">{$index.callbacktime_date}</td>
                    <td style="padding: 5px;">{$index.created_date}</td>
                    <td style="padding: 5px;">{$index.ageing}</td>
                    <td style="padding: 5px;">{$index.due_date}</td>
                    <td style="padding: 5px;">{$index.deviation}</td>
                    <td style="padding: 5px;">{$index.reporter}</td>
                    <td style="padding: 5px;"><pre><textarea>{$index.create_summary}</textarea></pre></td>
                    <td style="padding: 5px;">{$index.updatedby}</td>
                    <td style="padding: 5px;">{$index.modified_date}</td>
                    <td style="padding: 5px;">
                        {if $index.order_id}
                            {$index.order_customer_email}
                        {else}
                            {$index.customer_email}
                        {/if}
                    </td>
                    <td style="padding: 5px;">{$index.first_name}</td>
                    <td style="padding: 5px;">{$index.mobile}</td>
                    <td style="padding: 5px;">{$index.closed_date}</td>
                    <td style="padding: 5px;">{$index.resolver}</td>
                    <td style="padding: 5px;"><textarea>{$index.close_summary}</textarea></td>
                    <td style="padding: 5px;">
                        {if $index.order_id}
                            <a href="{$http_location}/admin/order.php?orderid={$index.order_id}">{$index.order_id}</a>
                        {else}
                            {$index.order_id}
                        {/if}
                    </td>
                    <td style="padding: 5px;">
                        {if $index.order_id}
                            <a href="{$http_location}/admin/order.php?orderid={$index.order_id}">{$index.status}</a>
                        {else}
                            {$index.status}
                        {/if}
                    </td>
                    <td style="padding: 5px;">{$index.payment_method}</td>
                    <td style="padding: 5px;">{$index.delivery_ageing}</td>
                    <td style="padding: 5px;">{$index.warehouseid}</td>
                    <td style="padding: 5px;">{$index.courier_service}</td>
                    <td style="padding: 5px;">{$index.tracking}</td>
                    <td style="padding: 5px;"><textarea>{$index.order_address}</textarea></td>
                    <td style="padding: 5px;">{$index.order_state}</td>
                    <td style="padding: 5px;">{$index.order_city}</td>
                    <td style="padding: 5px;">{$index.order_zipcode}</td>
                    <td style="padding: 5px;">{$index.DC_info}</td>
                    <td style="padding: 5px;">{$index.refund}</td>
                    <td style="padding: 5px;">{$index.refund_type}</td>
                    <td style="padding: 5px;">{$index.refund_value}</td>
                    <td style="padding: 5px;">{$index.compensation}</td>
                    <td style="padding: 5px;">{$index.compensation_type}</td>
                    <td style="padding: 5px;">{$index.compensation_value}</td>
                    <td style="padding: 5px;">
                    
                        {if $index.SR_status neq 'close'}
                            {if !$index.order_id}
                                {if $SR_admin}
                                    <a href="javascript:void(0);"  onClick="javascript:windowOpener('{$http_location}/admin/ServiceRequest/manageNonOrderSR.php?non_order_sr_id={$index.id}&mode=updateNonOrderServieRequest','{$index.SR_id}', 'menubar=0,resizable=1,toolbar=0,width=800,height=500')">Edit</a>
                                    &nbsp;|&nbsp;
                                {/if}
                                <a href="javascript:void(0);"  onClick="javascript:windowOpener('{$http_location}/admin/ServiceRequest/manageNonOrderSR.php?non_order_sr_id={$index.id}&mode=closeNonOrderServieRequest','{$index.SR_id}', 'menubar=0,resizable=1,toolbar=0,width=800,height=500')">Close</a>
                                &nbsp;|&nbsp;
                                
                            {else}
                                {if $SR_admin}
                                    <a href="javascript:void(0);"  onClick="javascript:windowOpener('{$http_location}/admin/manageOrderSR.php?orderid={$index.order_id}&order_sr_id={$index.order_sr_id}&mode=updateServieRequest','{$index.SR_id}', 'menubar=0,resizable=1,toolbar=0,width=800,height=500');">Edit</a>
                                    &nbsp;|&nbsp;
                                {/if}
                                <a href="javascript:void(0);"  onClick="javascript:windowOpener('{$http_location}/admin/manageOrderSR.php?orderid={$index.order_id}&order_sr_id={$index.order_sr_id}&mode=closeServieRequest','{$index.SR_id}', 'menubar=0,resizable=1,toolbar=0,width=800,height=500');">Close</a>
                                &nbsp;|&nbsp;
                            {/if}
                        {/if}
                        
                        {if $SR_admin}
                            {if !$index.order_id}
                                <a href="javascript:void(0)" onclick="javascript:deleteNonOrderSR('{$index.id}');">Delete</a>
                            {else}
                                <a href="javascript:void(0)" onclick="javascript:deleteOrderSR('{$index.order_sr_id}');">Delete</a>
                            {/if}
                        {/if}
                    </td>
                </tr>
            {foreachelse}
                <tr>
                    <td colspan="12" align="center">No data is available!</td>
                </tr>
            {/foreach}
        </table>
    </div>

    <table cellpadding="2" cellspacing="1" width="100%">
        <tr>
            <td width="100px">Records In Page:</td>
            <td align="left">
                <select name="pagelimit" onchange="location.href='{$url}&pg=1&pagelimit='+$(this).val();">
                    {section name=idx loop=$limitarray}
                        {if $smarty.get.pagelimit eq $limitarray[idx]}
                            <option value="{$limitarray[idx]}" selected=selected>{$limitarray[idx]}</option>
                        {else}
                            <option value="{$limitarray[idx]}">{$limitarray[idx]}</option>
                        {/if}
                    {/section}
                </select>
            </td>
            <td width="700px" align="center">page <b>{$pg}</b> of <b>{$totalpages}</b>&nbsp;[showing <b>{$offset_start}-{$offset_end}</b> of <b>{$total_record}]</b></td>            
            <td align="right">{$paginator}</td>
        </tr>
    </table>

{/capture}
{include file="dialog.tpl" title="Service Requests" content=$smarty.capture.dialog extra='width="100%"'}