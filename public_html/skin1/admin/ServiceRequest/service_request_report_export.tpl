<table cellpadding="3" cellspacing="1" width="100%">
    <tr class="TableHead">
        <th rowspan="2">Sr Type</th>
        <th colspan="3">SR Closure Rate</th>
        <th colspan="2">Complaint Ratio</th>
        <th colspan="3">Compensated closures summary</th>
    </tr>
    <tr style="background-color:#f0f0f0;" align="center">
        <th>SR registered</th>
        <th>SR Closed</th>
        <th>% SR closed</th>

        <th>Count of Order</th>
        <th>% Complaint Ratio</th>

        <th>Count of orders closed with compensation</th>
        <th>% SRs closed with compensation</th>
        <th>Total Compensation amount</th>
    </tr>
   <tr style="background-color:#f0f0f0;" align="center">
        <td>Order</td>
        <td>{$countOrderSR}</td>
        <td>{$countClosedOrderSR}</td>
        <td>{$closeOrderSRPercent}</td>

        <td>{$countOrder}</td>
        <td>{$complaintRatioPercent}</td>

        <td>{$countOrderSRWithComp}</td>
        <td>{$countOrderSRWithCompPercent}</td>
        <td>
            <table>
                <tr>
                    <td>Credit =</td>
                    <td align="right">{$compAmountCredit}</td>
                </tr>
                <tr>
                    <td>Debit =</td>
                    <td align="right">{$compAmountDebit}</td>
                </tr>
                <tr>
                    <td>Total Paid =</td>
                    <td align="right">{$compAmount}</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr style="background-color:#f0f0f0;" align="center">
        <td>Non-Order</td>
        <td>{$countNonOrderSR}</td>
        <td>{$countClosedNonOrderSR}</td>
        <td>{$closeNonOrderSRPercent}</td>

        <td colspan="5"></td>
    </tr>
    <tr style="background-color:#f0f0f0;" align="center">
        <td>Total</td>
        <td>{$countSR}</td>
        <td>{$countClosedSR}</td>
        <td>{$closeSRPercent}</td>

        <td colspan="5"></td>
    </tr>
</table>