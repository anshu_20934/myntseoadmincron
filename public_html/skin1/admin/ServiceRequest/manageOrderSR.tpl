<html>
<head>
{ include file="meta.tpl" }
<link rel="stylesheet" href="{$http_location}/skin1/mykriti.css"/>
<link href="{$http_location}/skin1/myntra_css/jquery-ui-datepicker.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="{$http_location}/skin1/myntra_js/jquery-ui-datepicker.js"></script>

{literal}
    <style type="text/css">
        #closeCouponDetail ,#closeCouponValidateError,#addRefCouponDetail ,#addRefCouponValidateError,#editRefCouponDetail ,#editRefCouponValidateError,#closeRefCouponDetail ,#closeRefCouponValidateError{font-size:10px;}
    </style>
    <script type="text/javascript">
        var orderCustomer = '{/literal}{$order_customer[0].login}{literal}';
        var SRNumber = /^(([1-9]{1}[0-9]{0,6})|([1-9]{1}[0-9]{0,6}\.[0-9]{1,2}))$/;//integer(7 digit) or float(7 digit, 2 decimal digit)
        var SRAlphaNumericSpace = /^[\w]{1}[\w\s]{0,49}$/;//50 chars max
        var SRAlphaNumeric = /^[\w]{1,15}$/;//15 chars max

        $(document).ready(function(){

            //initializing datepicker
            $( "#ref_cash_date, #ref_cashback_date, #ref_cheque_date, #ref_online_date, #ref_cash_date_edit, #ref_cashback_date_edit, #ref_cheque_date_edit, #ref_online_date_edit, #ref_cash_date_close, #ref_cashback_date_close, #ref_cheque_date_close, #ref_online_date_close" ).datepicker({
                dateFormat:'mm/dd/yy',
                changeMonth: true,
                changeYear: true,
                showOn: 'both',
                buttonImageOnly: true,
                buttonImage: cdn_base+'/images/cal.gif',
                buttonText: 'Calendar'
            });


            //initializing datetimepicker
            $( "#callbacktime, #callbacktime_edit" ).datetimepicker({
                dateFormat:'mm/dd/yy',
                changeMonth: true,
                changeYear: true,
                showOn: 'both',
                buttonImageOnly: true,
                buttonImage: cdn_base+'/images/cal.gif',
                buttonText: 'Calendar'
            });

            //manage callback time
            //add
            $("#addCallback").change(function(){
                onchangeCallback("add",$(this));
            });
            //edit
            $("#editCallback").change(function(){
                onchangeCallback("edit",$(this));
            });
            //manage callback time


            //manage refund and refund types
            //add
            $("#addRef").change(function(){
                onchangeRefund("add",$(this));
            });

            $("#addRefType select").change(function(){
                onchangeRefType('add', $(this));
            });

            $("#addRefCouponCode").blur(function(){
                var coupon = $(this).val();
                makePostAjax(http_loc+'/admin/manageOrderSR.php', 'mode=getCompCoupon&coupon='+coupon, false, isCouponExist, 'addRef');
            });

            $("#validateAddRefCoupon").click(function(){
                $("#addRefCouponCode").trigger('blur');
            });

            //edit
            $("#editRef").change(function(){
                onchangeRefund("edit",$(this));
            });

            $("#editRefType select").change(function(){
                onchangeRefType('edit', $(this));
            });

            $("#editRefCouponCode").blur(function(){
                var coupon = $(this).val();
                makePostAjax(http_loc+'/admin/manageOrderSR.php', 'mode=getCompCoupon&coupon='+coupon, false, isCouponExist, 'editRef');
            });

            $("#validateEditRefCoupon").click(function(){
                $("#editRefCouponCode").trigger('blur');
            });

            //close
            $("#closeRef").change(function(){
                onchangeRefund("close",$(this));
            });

            $("#closeRefType select").change(function(){
                onchangeRefType('close', $(this));
            });

            $("#closeRefCouponCode").blur(function(){
                var coupon = $(this).val();
                makePostAjax(http_loc+'/admin/manageOrderSR.php', 'mode=getCompCoupon&coupon='+coupon, false, isCouponExist, 'closeRef');
            });

            $("#validateCloseRefCoupon").click(function(){
                $("#closeRefCouponCode").trigger('blur');
            });
            //manage refund and refund types ends

            //manage compensation and comp types
            $("#closeComp").change(function(){
                onchangeCompensation("close",$(this));
            });

            $("#closeCompType select").change(function(){
                onchangeCompType('close', $(this));
            });

            $("input[name='coupon_code']").blur(function(){
                var coupon = $(this).val();
                makePostAjax(http_loc+'/admin/manageOrderSR.php', 'mode=getCompCoupon&coupon='+coupon, false, isCouponExist, 'close');
            });

            $("#validateCoupon").click(function(){
                $("input[name='coupon_code']").trigger('blur');
            });
            //manage compensation and comp types ends

            //validate add form
            $("#add_service_request").click(function(){            
                var flag = validateSRForm('add');
    
                if(flag){
                    $("form[name='addSR']").submit();
                }
            });

            //validate edit form
            $("#edit_service_request").click(function(){
                var flag = validateSRForm('edit');

                if(flag){
                    $("form[name='editSR']").submit();
                }
            });

            //validate close form
            $("#close_service_request").click(function(){
                var flag = validateSRForm('close');

                if(flag){
                    $("form[name='closeSR']").submit();
                }
            });
        });


        function onchangeCallback(prefixId,callbackObj){
            if(callbackObj.val() == 'Y'){
                $("#"+prefixId+"CallbackTime").show();

            } else {
                $("#"+prefixId+"CallbackTime").hide();                
            }
        }

        function onchangeRefund(prefixId,refObj){
            if(refObj.val() == 'Y'){
                $("#"+prefixId+"RefType").show();
                onchangeRefType(prefixId, $("#"+prefixId+"RefType select"));
            } else {
                $("#"+prefixId+"RefType").hide();
                $("#"+prefixId+"RefValue_coupon").hide();
                $("#"+prefixId+"RefValue_cash").hide();
                $("#"+prefixId+"RefValue_cashback").hide();
                $("#"+prefixId+"RefValue_cheque").hide();
                $("#"+prefixId+"RefValue_online").hide();
            }
        }

        function onchangeRefType(prefixId, refTypeObj){
            //var optionObjs = refTypeObj.option;
            var optionObjs = $("#"+prefixId+"RefType select option");
            optionObjs.each(function(key, value){
                if($(value).attr('selected')){
                    $("#"+prefixId+"RefValue_"+$(value).val()).show();
                } else {
                    $("#"+prefixId+"RefValue_"+$(value).val()).hide();
                }
            });

        }

        function onchangeCompensation(prefixId,compObj){
            if(compObj.val() == 'Y'){
                $("#"+prefixId+"CompType").show();
                onchangeCompType(prefixId, $("#"+prefixId+"CompType select"));
            } else {
                $("#"+prefixId+"CompType").hide();
                $("#"+prefixId+"CompValue_coupon").hide();
                $("#"+prefixId+"CompValue_gift").hide();
            }
        }

        function onchangeCompType(prefixId, compTypeObj){
            //var optionObjs = compTypeObj.option;
            var optionObjs = $("#"+prefixId+"CompType select option");
            optionObjs.each(function(key, value){
                if($(value).attr('selected')){
                    $("#"+prefixId+"CompValue_"+$(value).val()).show();
                } else {
                    $("#"+prefixId+"CompValue_"+$(value).val()).hide();
                }
            });

        }

        function makePostAjax(url, inputData, cache, callBackFunc, param){
            $.ajax({
                type:'POST',
                url: url,
                data: inputData,
                cache:cache,
                success: function(data){callBackFunc(data,param);}
            });
        }

        function isCouponExist(data, prefix){
            if(!data){
                $('#'+prefix+'CouponDetail').text('Coupon does not exist!').css({border:'1px solid red',padding:'5px'});
                $('#'+prefix+'CouponValidateError').hide();
            } else {

                //show coupon detail
                var data = $.parseJSON(data);
                var couponInfoHTML = '';

                $.each(data,function(key,value){                
                    couponInfoHTML += '<div><span>'+key+ ':</span><span style="font-weight:bold">'+value+'</span></div>';
                    $('#'+prefix+'CouponDetail').html(couponInfoHTML).css({border:'1px solid green',padding:'5px'});
                });

                //validate coupon
                isCouponValid(data.coupon, prefix);
            }
        }

        function isCouponValid(coupon, prefix){
            makePostAjax(http_loc+'/admin/manageOrderSR.php', 'mode=validateCoupon&coupon='+coupon+'&orderCustomer='+orderCustomer, false, isCouponValidCallBack, prefix);
        }

        function isCouponValidCallBack(data, prefix){
                  
            $('#'+prefix+'CouponValidateError').show();
            if(data != true){
                $('#'+prefix+'CouponDetail').css({border:'1px solid red',padding:'5px'});

                if(!data){
                    $('#'+prefix+'CouponValidateError').text('Coupon is not valid!').css({border:'1px solid red',padding:'5px'});
                } else {
                    $('#'+prefix+'CouponValidateError').text(data).css({border:'1px solid red',padding:'5px',color:'red'});
                }

            } else {
                $('#'+prefix+'CouponDetail').css({border:'1px solid green',padding:'5px'});
                $('#'+prefix+'CouponValidateError').text('Coupon is valid').css({border:'1px solid #669900',padding:'5px',color:'#669900'});
            }
        }

        /*
         * validates add, edit and close SR form fields
         */
        function validateSRForm(prefix){

            //validate if refund is selected
            var refundSelected = $('#'+prefix+'Ref').val();            
            if(refundSelected == 'Y'){

                //validate only for selected refund types
                var hasError = false;
                var optionObjs = $("#"+prefix+"RefType select option");
                optionObjs.each(function(key, value){

                    if($(value).attr('selected')){

                        //common fields to be validated for add. edit and close
                        if($(value).val() == 'cash'){
                            var cashAmountObj = $("form[name='"+prefix+"SR'] input[name='ref_cash_amount']");

                            if(!validateAgainstRegex(SRNumber, cashAmountObj.val())){
                                alert("Please enter valid cash amount eg: 350, 90.5, 1000 etc...");
                                cashAmountObj.focus();
                                hasError = true;
                                return false;
                            }
                        }

                        if($(value).val() == 'cashback'){
                            var cashBackAmountObj = $("form[name='"+prefix+"SR'] input[name='ref_cashback_amount']");

                            if(!validateAgainstRegex(SRNumber, cashBackAmountObj.val())){
                                alert("Please enter valid cashback amount eg: 350, 90.5, 1000 etc...");
                                cashBackAmountObj.focus();
                                hasError = true;
                                return false;
                            }
                        }

                        if($(value).val() == 'cheque'){
                            var chequeNameObj = $("form[name='"+prefix+"SR'] input[name='ref_cheque_name']");

                            if(!validateAgainstRegex(SRAlphaNumericSpace, chequeNameObj.val())){
                                alert("Please enter valid cheque name");
                                chequeNameObj.focus();
                                hasError = true;
                                return false;
                            }

                            var chequeNumberObj = $("form[name='"+prefix+"SR'] input[name='ref_cheque_number']");
                            if(!validateAgainstRegex(SRAlphaNumeric, chequeNumberObj.val())){
                                alert("Please enter valid cheque number");
                                chequeNumberObj.focus();
                                hasError = true;
                                return false;
                            }

                            var chequeAmountObj = $("form[name='"+prefix+"SR'] input[name='ref_cheque_amount']");
                            if(!validateAgainstRegex(SRNumber, chequeAmountObj.val())){
                                alert("Please enter valid cheque amount eg: 350, 90.5, 1000 etc...");
                                chequeAmountObj.focus();
                                hasError = true;
                                return false;
                            }
                        }

                        if($(value).val() == 'online'){
                            var onlineAmountObj = $("form[name='"+prefix+"SR'] input[name='ref_online_amount']");

                            if(!validateAgainstRegex(SRNumber, onlineAmountObj.val())){
                                alert("Please enter valid online transfer amount eg: 350, 90.5, 1000 etc...");
                                onlineAmountObj.focus();
                                hasError = true;
                                return false;
                            }
                        }
                    
                    }
                });

                return !hasError;           

            }
            //if no refund
            return true;      
        }

        function validateAgainstRegex(regEx, data){
            return regEx.test(data);
        }
    </script>
{/literal}
</head>

<body id="body">
<div id="scroll_wrapper"><div id="container"><div id="wrapper"><div id="display"><div class="center_column">
    <div class="print">
        <div class="super" ><p>&nbsp;</p></div>
        {if $form_type eq 'addServieRequest'}
            {if $success ne "Y"}
                <div class="head" style="margin:0px 0px 20px;">
                    <p>Add Service Request for order : #{$orderid}</p>
                    <p style="color:red">{$message}</p>
                </div>
                <div class="links"><p></p></div>
                <div class="foot"></div>
                <div style="height:580px;overflow:scroll">
                <form method="post" name="addSR"  action="{$http_location}/admin/manageOrderSR.php">
                    <input type="hidden" name="mode" value="{$form_type}"/>
                    <input type="hidden" name="sr_post_mode" value="1"/>
                    <input type="hidden" name="orderid" value="{$orderid}"/>
                    <table>
                        <tr>
                            <td width="200px">Category<span style="color:red;">&nbsp;*</span></td>
                            <td>
                                <select name="subcategory">
                                    {section name=category loop=$sr_category}
                                        {if $sr_category[category].category_type eq 'order' && $sr_category[category].cat_active eq 'Y'}
                                            <optgroup label="{$sr_category[category].SR_category}-({$sr_category[category].SLA_string})"></optgroup>

                                            {assign var=subcategories value=$sr_category[category].subCategories}
                                            {section name=index loop=$subcategories}
                                                {if $subcategories[index].subcat_active eq 'Y'}
                                                    <option value="{$subcategories[index].id}">{$subcategories[index].SR_subcategory}-({$subcategories[index].SLA_string})</option>
                                                {/if}
                                            {/section}
                                        {/if}
                                    {/section}
                                </select>
                            </td>
                        </tr>

                        <tr>
                            <td>Priority<span style="color:red;">&nbsp;*</span></td>
                            <td>
                                <select name="priority">
                                    {section name=index loop=$sr_priority}
                                        {if $sr_priority[index].priority_active eq 'Y'}
                                            {if $sr_priority[index].priority_default eq 'Y'}
                                                <option value="{$sr_priority[index].id}" selected="selected">{$sr_priority[index].SR_priority}</option>
                                            {else}
                                                <option value="{$sr_priority[index].id}">{$sr_priority[index].SR_priority}</option>
                                            {/if}                                        
                                        {/if}
                                    {/section}
                                </select>
                            </td>
                        </tr>

                        <tr>
                            <td>Department<span style="color:red;">&nbsp;*</span></td>
                            <td>
                                <select name="department">
                                    {section name=index loop=$sr_department}
                                        <option value="{$sr_department[index]}">{$sr_department[index]}</option>
                                    {/section}
                                </select>
                            </td>
                        </tr>

                        <tr>
                            <td>Channel<span style="color:red;">&nbsp;*</span></td>
                            <td>
                                <select name="channel">
                                    {section name=index loop=$sr_channel}
                                        <option value="{$sr_channel[index]}">{$sr_channel[index]}</option>
                                    {/section}
                                </select>
                            </td>
                        </tr>

                        <tr>
                            <td>Items</td>
                            <td>
                                <select name="item[]" multiple="multiple">
                                    {section name=index loop=$sr_items}
                                        <option value="{$sr_items[index].id}">{$sr_items[index].id} - {$sr_items[index].name}(({$sr_items[index].item_status}))</option>
                                    {/section}
                                </select>
                            </td>
                        </tr>

                        <tr>
                            <td>Summary<span style="color:red;">&nbsp;*</span></td>
                            <td><textarea name="create_summary" cols="50" rows="3">{$smarty.post.create_summary}</textarea></td>
                        </tr>

                        <tr>
                            <td>Callback<span style="color:red;">&nbsp;*</span></td>
                            <td>
                                <select name="callback" id="addCallback">
                                    <option value="N" selected="selected">N</option>
                                    <option value="Y">Y</option>
                                </select>
                            </td>
                        </tr>

                         <tr id="addCallbackTime" style="display:none;">
                            <td>Callback Time<span style="color:red;">&nbsp;*</span></td>
                            <td>
                                <input type="text" name="callbacktime" id="callbacktime" value="{$smarty.post.callbacktime}" readonly="readonly"/>
                            </td>
                        </tr>

                        <tr>
                            <td>Refund<span style="color:red;">&nbsp;*</span></td>
                            <td>
                                <select name="refund" id="addRef">
                                    <option value="N" selected="selected">N</option>
                                    <option value="Y">Y</option>
                                </select>
                            </td>
                        </tr>

                        <tr id="addRefType" style="display:none;">
                            <td>Refund Type<span style="color:red;">&nbsp;*</span></td>
                            <td>
                                <select name="refund_type[]" multiple="multiple">
                                    {foreach key=key item=item from=$refund_types}
                                        <option value="{$key}">{$item}</option>
                                    {/foreach}
                                </select>
                            </td>
                        </tr>

                        <tr id="addRefValue_coupon" style="display:none;">
                            <td colspan="2">
                                <table>
                                    <tr>
                                        <td width="200px">Coupon Code<span style="color:red;">&nbsp;*</span></td>
                                        <td><input type="text" name="ref_coupon_code" id="addRefCouponCode" value="{$smarty.post.ref_coupon_code}"/>&nbsp;<a href="javascript:void(0);" id="validateAddRefCoupon" style="font-size:12px;font-weight:bold;">Validate</a></td>
                                    </tr>
                                    <tr>
                                        <td>Coupon Detail<span style="color:red;">&nbsp;*</span></td>
                                        <td>
                                            <div id="addRefCouponValidateError"></div>
                                            <div id="addRefCouponDetail"></div>
                                        </td>
                                    </tr>
                                </table>
                            </td>

                        </tr>

                        <tr id="addRefValue_cash" style="display:none;">
                            <td colspan="2">
                                <table>
                                    <tr>
                                        <td width="200px">Cash Amount<span style="color:red;">&nbsp;*</span></td>
                                        <td><input type="text" name="ref_cash_amount" value="{$smarty.post.ref_cash_amount}"/></td>
                                    </tr>
                                    <tr>
                                        <td>Cash Date<span style="color:red;">&nbsp;*</span></td>

                                        <td>
                                            <input type="text" name="ref_cash_date" id="ref_cash_date" value="{$smarty.post.ref_cash_date}" readonly="readonly"/>                                            
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Cash Comment</td>
                                        <td><textarea name="ref_cash_description">{$smarty.post.ref_cash_description}</textarea></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                        <tr id="addRefValue_cashback" style="display:none;">
                            <td colspan="2">
                                <table>
                                    <tr>
                                        <td width="200px">Cashback Transaction<span style="color:red;">&nbsp;*</span></td>
                                        <td>
                                            <select name="ref_cashback_transaction">
                                                <option value="credit" selected="selected">Credit</option>
                                                <option value="debit">Debit</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Cashback Amount<span style="color:red;">&nbsp;*</span></td>
                                        <td><input type="text" name="ref_cashback_amount" value="{$smarty.post.ref_cashback_amount}"/></td>
                                    </tr>
                                    <tr>
                                        <td>Cashback Date<span style="color:red;">&nbsp;*</span></td>
                                        <td>
                                            <input type="text" name="ref_cashback_date" id="ref_cashback_date" value="{$smarty.post.ref_cashback_date}" readonly="readonly"/>                                            
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                        <tr id="addRefValue_cheque" style="display:none;">
                            <td colspan="2">
                                <table>
                                    <tr>
                                        <td width="200px">Cheque Name<span style="color:red;">&nbsp;*</span></td>
                                        <td><input type="text" name="ref_cheque_name" value="{$smarty.post.ref_cheque_name}"/></td>
                                    </tr>
                                    <tr>
                                        <td>Cheque Number<span style="color:red;">&nbsp;*</span></td>
                                        <td><input type="text" name="ref_cheque_number" value="{$smarty.post.ref_cheque_number}"/></td>
                                    </tr>
                                    <tr>
                                        <td>Cheque Amount<span style="color:red;">&nbsp;*</span></td>
                                        <td><input type="text" name="ref_cheque_amount" value="{$smarty.post.ref_cheque_amount}"/></td>
                                    </tr>
                                    <tr>
                                        <td>Cheque Date<span style="color:red;">&nbsp;*</span></td>
                                        <td>
                                            <input type="text" name="ref_cheque_date" id="ref_cheque_date" value="{$smarty.post.ref_cheque_date}" readonly="readonly"/>                                            
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Cheque Comment</td>
                                        <td><textarea name="ref_cheque_description">{$smarty.post.ref_cheque_description}</textarea></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                        <tr id="addRefValue_online" style="display:none;">
                            <td colspan="2">
                                <table>
                                    <tr>
                                        <td width="200px">Online Amount<span style="color:red;">&nbsp;*</span></td>
                                        <td><input type="text" name="ref_online_amount" value="{$smarty.post.ref_online_amount}"/></td>
                                    </tr>
                                    <tr>
                                        <td>Online Date<span style="color:red;">&nbsp;*</span></td>
                                        <td>
                                            <input type="text" name="ref_online_date" id="ref_online_date" value="{$smarty.post.ref_online_date}" readonly="readonly"/>                                            
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Online Comment</td>
                                        <td><textarea name="ref_online_description">{$smarty.post.ref_online_description}</textarea></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                        <tr>
                            <td>&nbsp;</td>
                            <td><br/><input type="button" value="Add Service Request" id="add_service_request" class="button" >&nbsp;<input type="button" value="Cancel" class="button" onclick="window.close();"></td>
                        </tr>
                        <tr><td>&nbsp;</td></tr>
                        <tr><td>&nbsp;</td></tr>
                        <tr><td>&nbsp;</td></tr>
                    </table>
                </form>
				<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
                </div>
            {elseif $success eq "Y"}
                <table>
                    <tr>
                        <td>Service Request added succesfully!</td>
                        <td>&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;</td>
                    </tr>
                    <tr><td style="font-size:.8em;color:red;"><b>Please Click the close button below to refresh data on back screen</b></td></tr>
                    <tr><td align="center"><input type="button" style="height:20px;width:50px; background-color:black; color:white;" value="Close" onclick="opener.window.location.reload();self.close()"></td></tr>
                </table>
            {/if}

        {elseif $form_type eq 'updateServieRequest'}

            {if $success ne "Y"}
                <div class="head" style="margin:0px 0px 20px;">
                    <p>Edit Service Request for order : #{$orderid}</p>
                    <p style="color:red;">{$message}</p>
                </div>
                <div class="links"><p></p></div>
                <div class="foot"></div>
                <div style="height:580px;overflow:scroll">
                <form method="post" name="editSR"  action="{$http_location}/admin/manageOrderSR.php">
                    <input type="hidden" name="mode" value="{$form_type}"/>
                    <input type="hidden" name="sr_post_mode" value="1"/>
                    <input type="hidden" name="orderid" value="{$orderid}"/>
                    <input type="hidden" name="order_sr_id" value="{$order_sr_id}"/>
                    <table>
                        <tr>
                            <td width="200px">Category<span style="color:red;">&nbsp;*</span></td>
                            <td>
                                <select name="subcategory">
                                    {section name=category loop=$sr_category}
                                        {if $sr_category[category].category_type eq 'order' && $sr_category[category].cat_active eq 'Y'}
                                            <optgroup label="{$sr_category[category].SR_category}-({$sr_category[category].SLA_string})"></optgroup>

                                            {assign var=subcategories value=$sr_category[category].subCategories}
                                            {section name=index loop=$subcategories}
                                                {if $subcategories[index].subcat_active eq 'Y'}
                                                    {if $sr_order[0].sub_category_id eq $subcategories[index].id}
                                                        <option value="{$subcategories[index].id}" selected="selected">{$subcategories[index].SR_subcategory}-({$subcategories[index].SLA_string})</option>
                                                    {else}
                                                        <option value="{$subcategories[index].id}">{$subcategories[index].SR_subcategory}-({$subcategories[index].SLA_string})</option>
                                                    {/if}
                                                {/if}
                                            {/section}
                                        {/if}
                                    {/section}
                                </select>
                            </td>
                        </tr>

                        <tr>
                            <td>Priority<span style="color:red;">&nbsp;*</span></td>
                            <td>
                                <select name="priority">
                                    {section name=index loop=$sr_priority}
                                        {if $sr_priority[index].priority_active eq 'Y'}
                                            {if $sr_order[0].priority_id eq $sr_priority[index].id}
                                                <option value="{$sr_priority[index].id}" selected="selected">{$sr_priority[index].SR_priority}</option>
                                            {else}
                                                <option value="{$sr_priority[index].id}">{$sr_priority[index].SR_priority}</option>
                                            {/if}
                                        {/if}
                                    {/section}
                                </select>
                            </td>
                        </tr>

                        <tr>
                            <td>Status<span style="color:red;">&nbsp;*</span></td>
                            <td>
                                <select name="status">                                    
                                    {foreach key=key item=item from=$sr_status}
                                        {if $item.status_active eq 'Y'}
                                            {if $sr_order[0].status_id eq $item.id}
                                                <option value="{$item.id}" selected="selected">{$item.SR_status}</option>
                                            {else}
                                                <option value="{$item.id}">{$item.SR_status}</option>
                                            {/if}
                                        {/if}
                                    {/foreach}
                                </select>
                            </td>
                        </tr>

                        <tr>
                            <td>Department<span style="color:red;">&nbsp;*</span></td>
                            <td>
                                <select name="department">
                                    {section name=index loop=$sr_department}
                                        {if $sr_order[0].SR_department eq $sr_department[index]}
                                            <option value="{$sr_department[index]}" selected="selected">{$sr_department[index]}</option>
                                        {else}
                                            <option value="{$sr_department[index]}">{$sr_department[index]}</option>
                                        {/if}
                                    {/section}
                                </select>
                            </td>
                        </tr>

                        <tr>
                            <td>Channel<span style="color:red;">&nbsp;*</span></td>
                            <td>
                                <select name="channel">
                                    {section name=index loop=$sr_channel}
                                        {if $sr_order[0].SR_channel eq $sr_channel[index]}
                                            <option value="{$sr_channel[index]}" selected="selected">{$sr_channel[index]}</option>
                                        {else}
                                            <option value="{$sr_channel[index]}">{$sr_channel[index]}</option>
                                        {/if}
                                    {/section}
                                </select>
                            </td>
                        </tr>

                        <tr>
                            <td>Items</td>
                            <td>
                                <select name="item" multiple="multiple">
                                    {assign var=items value=$sr_order[0].items}
                                    {section name=index loop=$sr_items}
                                        {section name=selectedindex loop=$items}
                                            {if $items[selectedindex] eq $sr_items[index].id}
                                                <option value="{$sr_items[index].id}" selected="selected">{$sr_items[index].id} - {$sr_items[index].name}({$sr_items[index].item_status})</option>
                                            {else}
                                                <option value="{$sr_items[index].id}">{$sr_items[index].id} - {$sr_items[index].name}({$sr_items[index].item_status})</option>
                                            {/if}
                                        {/section}
                                    {/section}
                                </select>
                            </td>
                        </tr>

                        <tr>
                            <td>Summary<span style="color:red;">&nbsp;*</span></td>
                            <td><textarea name="create_summary" cols="50" rows="3" readonly>{if $smarty.post.create_summary}{$smarty.post.create_summary}{else}{$sr_order[0].create_summary}{/if}</textarea></td>
                        </tr>

                        <tr>
                            <td>Comment</td>
                            <td><textarea name="comment" cols="50" rows="3">{if $smarty.post.comment}{$smarty.post.comment}{/if}</textarea></td>
                        </tr>

                        <tr>
                            <td>Callback<span style="color:red;">&nbsp;*</span></td>
                            <td>
                                <select name="callback" id="editCallback">
                                    {if !$callbackTime}
                                        <option value="N" selected="selected">N</option>
                                        <option value="Y">Y</option>
                                    {else}
                                        <option value="N">N</option>
                                        <option value="Y" selected="selected">Y</option>
                                    {/if}
                                </select>
                            </td>
                        </tr>

                        <tr id="editCallbackTime" {if !$callbackTime}style="display:none;"{/if}>
                            <td>Callback Time<span style="color:red;">&nbsp;*</span></td>
                            <td>
                                <input type="text" name="callbacktime" id="callbacktime_edit" value="{if $smarty.post.callbacktime}{$smarty.post.callbacktime}{else}{$callbackTime}{/if}" readonly="readonly"/>
                            </td>
                        </tr>

                        <tr>
                            <td>Refund<span style="color:red;">&nbsp;*</span></td>
                            <td>
                                <select name="refund" id="editRef">
                                    {if $refund eq 'N'}
                                        <option value="N" selected="selected">N</option>
                                        <option value="Y">Y</option>
                                    {else}
                                        <option value="N">N</option>
                                        <option value="Y" selected="selected">Y</option>
                                    {/if}
                                </select>
                            </td>
                        </tr>

                        <tr id="editRefType" {if $refund eq 'N'}style="display:none;"{/if}>
                            <td>Refund Type<span style="color:red;">&nbsp;*</span></td>
                            <td>                            
                                <select name="refund_type[]" multiple="multiple">
                                    {foreach key=key item=item from=$refund_types}
                                        <option value="{$key}" {if $refund_type_detail.$key} selected="selected" {/if}>{$item}</option>
                                    {/foreach}
                                </select>
                            </td>
                        </tr>

                        <tr id="editRefValue_coupon" {if $refund_type_detail.coupon.coupon eq ''}style="display:none;"{/if}>
                            <td colspan="2">
                                <table>
                                    <tr>
                                        <td width="200px">Coupon Code<span style="color:red;">&nbsp;*</span></td>
                                        <td><input type="text" name="ref_coupon_code" id="editRefCouponCode" value="{if $smarty.post.ref_coupon_code}{$smarty.post.ref_coupon_code}{else}{$refund_type_detail.coupon.coupon}{/if}"/>&nbsp;<a href="javascript:void(0);" id="validateEditRefCoupon" style="font-size:12px;font-weight:bold;">Validate</a></td>
                                    </tr>
                                    <tr>
                                        <td>Coupon Detail<span style="color:red;">&nbsp;*</span></td>
                                        <td>
                                            <div id="editRefCouponValidateError"></div>
                                            <div id="editRefCouponDetail"></div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                        <tr id="editRefValue_cash" {if $refund_type_detail.cash.amount eq ''}style="display:none;"{/if}>
                            <td colspan="2">
                                <table>
                                    <tr>
                                        <td width="200px">Cash Amount<span style="color:red;">&nbsp;*</span></td>
                                        <td><input type="text" name="ref_cash_amount" value="{if $smarty.post.ref_cash_amount}{$smarty.post.ref_cash_amount}{else}{$refund_type_detail.cash.amount}{/if}"/></td>
                                    </tr>
                                    <tr>
                                        <td>Cash Date<span style="color:red;">&nbsp;*</span></td>
                                        <td>
                                            <input type="text" name="ref_cash_date_edit" id="ref_cash_date_edit" value="{if $smarty.post.ref_cash_date_edit}{$smarty.post.ref_cash_date_edit}{else}{$refund_type_detail.cash.trans_date}{/if}" readonly="readonly"/>                                            
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Cash Comment</td>
                                        <td><textarea name="ref_cash_description">{if $smarty.post.ref_cash_description}{$smarty.post.ref_cash_description}{else}{$refund_type_detail.cash.summary}{/if}</textarea></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                        <tr id="editRefValue_cashback" {if $refund_type_detail.cashback.amount eq ''}style="display:none;"{/if}>
                            <td colspan="2">
                                <table>
                                    <tr>
                                        <td width="200px">Cashback Transaction<span style="color:red;">&nbsp;*</span></td>
                                        <td>
                                            <select name="ref_cashback_transaction">
                                                {if $refund_type_detail.cashback.transaction eq 'credit'}
                                                    <option value="credit" selected="selected">Credit</option>
                                                    <option value="debit">Debit</option>
                                                {else}
                                                    <option value="credit">Credit</option>
                                                    <option value="debit" selected="selected">Debit</option>
                                                {/if}
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Cashback Amount<span style="color:red;">&nbsp;*</span></td>
                                        <td><input type="text" name="ref_cashback_amount" value="{if $smarty.post.ref_cashback_amount}{$smarty.post.ref_cashback_amount}{else}{$refund_type_detail.cashback.amount}{/if}"/></td>
                                    </tr>
                                    <tr>
                                        <td>Cashback Date<span style="color:red;">&nbsp;*</span></td>
                                        <td>
                                            <input type="text" name="ref_cashback_date_edit" id="ref_cashback_date_edit" value="{if $smarty.post.ref_cashback_date_edit}{$smarty.post.ref_cashback_date_edit}{else}{$refund_type_detail.cashback.trans_date}{/if}" readonly="readonly"/>                                            
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                        <tr id="editRefValue_cheque" {if $refund_type_detail.cheque.amount eq ''}style="display:none;"{/if}>
                            <td colspan="2">
                                <table>
                                    <tr>
                                        <td width="200px">Cheque Name<span style="color:red;">&nbsp;*</span></td>
                                        <td><input type="text" name="ref_cheque_name" value="{if $smarty.post.ref_cheque_name}{$smarty.post.ref_cheque_name}{else}{$refund_type_detail.cheque.cheque_name}{/if}"/></td>
                                    </tr>
                                    <tr>
                                        <td>Cheque Number<span style="color:red;">&nbsp;*</span></td>
                                        <td><input type="text" name="ref_cheque_number" value="{if $smarty.post.ref_cheque_number}{$smarty.post.ref_cheque_number}{else}{$refund_type_detail.cheque.cheque_number}{/if}"/></td>
                                    </tr>
                                    <tr>
                                        <td>Cheque Amount<span style="color:red;">&nbsp;*</span></td>
                                        <td><input type="text" name="ref_cheque_amount" value="{if $smarty.post.ref_cheque_amount}{$smarty.post.ref_cheque_amount}{else}{$refund_type_detail.cheque.amount}{/if}"/></td>
                                    </tr>
                                    <tr>
                                        <td>Cheque Date<span style="color:red;">&nbsp;*</span></td>
                                        <td>
                                            <input type="text" name="ref_cheque_date_edit" id="ref_cheque_date_edit" value="{if $smarty.post.ref_cheque_date_edit}{$smarty.post.ref_cheque_date_edit}{else}{$refund_type_detail.cheque.trans_date}{/if}" readonly="readonly"/>                                            
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Cheque Comment</td>
                                        <td><textarea name="ref_cheque_description">{if $smarty.post.ref_cheque_description}{$smarty.post.ref_cheque_description}{else}{$refund_type_detail.cheque.summary}{/if}</textarea></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                        <tr id="editRefValue_online" {if $refund_type_detail.online.amount eq ''}style="display:none;"{/if}>
                            <td colspan="2">
                                <table>
                                    <tr>
                                        <td width="200px">Online Amount<span style="color:red;">&nbsp;*</span></td>
                                        <td><input type="text" name="ref_online_amount" value="{if $smarty.post.ref_online_amount}{$smarty.post.ref_online_amount}{else}{$refund_type_detail.online.amount}{/if}"/></td>
                                    </tr>
                                    <tr>
                                        <td>Online Date<span style="color:red;">&nbsp;*</span></td>
                                        <td>
                                            <input type="text" name="ref_online_date_edit" id="ref_online_date_edit" value="{if $smarty.post.ref_online_date_edit}{$smarty.post.ref_online_date_edit}{else}{$refund_type_detail.online.trans_date}{/if}" readonly="readonly"/>                                            
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Online Comment</td>
                                        <td><textarea name="ref_online_description">{if $smarty.post.ref_online_description}{$smarty.post.ref_online_description}{else}{$refund_type_detail.online.summary}{/if}</textarea></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                        <tr>
                            <td>&nbsp;</td>
                            <td><br/><input type="button" value="Update Service Request" id="edit_service_request" class="button" >&nbsp;<input type="button" value="Cancel" class="button" onclick="window.close();"></td>
                        </tr>
                        <tr><td>&nbsp;</td></tr>
                        <tr><td>&nbsp;</td></tr>
                        <tr><td>&nbsp;</td></tr>
                    </table>
                </form>
				<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
                </div>
            {elseif $success eq "Y"}
                <table>
                    <tr>
                        <td>Service Request updated succesfully!</td>
                        <td>&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;</td>
                    </tr>
                    <tr><td style="font-size:.8em;color:red;"><b>Please Click the close button below to refresh data on back screen</b></td></tr>
                    <tr><td align="center"><input type="button" style="height:20px;width:50px; background-color:black; color:white;" value="Close" onclick="opener.window.location.reload();self.close()"></td></tr>
                </table>
            {/if}


        {elseif $form_type eq 'closeServieRequest'}
            {if $success ne "Y"}
                <div class="head" style="margin:0px 0px 20px;">
                    <p>Close Service Request for order : #{$orderid}</p>
                    <p style="color:red;">{$message}</p>
                </div>
                <div class="links"><p></p></div>
                <div class="foot"></div>
                <div style="height:580px;overflow:scroll">
                <form method="post" name="closeSR"  action="{$http_location}/admin/manageOrderSR.php">
                    <input type="hidden" name="mode" value="{$form_type}"/>
                    <input type="hidden" name="sr_post_mode" value="1"/>
                    <input type="hidden" name="orderid" value="{$orderid}"/>
                    <input type="hidden" name="order_sr_id" value="{$order_sr_id}"/>
                    <table>
                        <tr>
                            <td>Category</td>
                            <td>
                                {section name=category loop=$sr_category}
                                    {if $sr_category[category].category_type eq 'order' && $sr_category[category].cat_active eq 'Y'}
                                        {if $sr_order[0].category_id eq $sr_category[category].id}
                                            <span style="font-size:12px;">{$sr_category[category].SR_category}-({$sr_category[category].SLA_string})</span>
                                        {/if}
                                    {/if}
                                {/section}
                            </td>
                        </tr>

                        <tr>
                            <td>Sub-Category</td>
                            <td>
                                {section name=category loop=$sr_category}
                                    {if $sr_category[category].category_type eq 'order' && $sr_category[category].cat_active eq 'Y'}
                                        {assign var=subcategories value=$sr_category[category].subCategories}
                                        {section name=index loop=$subcategories}
                                            {if $subcategories[index].subcat_active eq 'Y'}
                                                {if $sr_order[0].sub_category_id eq $subcategories[index].id}
                                                    <span style="font-size:12px;">{$subcategories[index].SR_subcategory}-({$subcategories[index].SLA_string})</span>
                                                {/if}                                                                                                
                                            {/if}
                                        {/section}
                                    {/if}
                                {/section}
                            </td>
                        </tr>

                        <tr>
                            <td>Priority</td>
                            <td>
                                {section name=index loop=$sr_priority}
                                    {if $sr_priority[index].priority_active eq 'Y'}
                                        {if $sr_order[0].priority_id eq $sr_priority[index].id}
                                            <span style="font-size:12px;">{$sr_priority[index].SR_priority}</span>
                                        {/if}
                                    {/if}
                                {/section}                                
                            </td>
                        </tr>

                        <tr>
                            <td>Refund<span style="color:red;">&nbsp;*</span></td>
                            <td>
                                <select name="refund" id="closeRef">
                                    {if $refund eq 'N'}
                                        <option value="N" selected="selected">N</option>
                                        <option value="Y">Y</option>
                                    {else}
                                        <option value="N">N</option>
                                        <option value="Y" selected="selected">Y</option>
                                    {/if}
                                </select>
                            </td>
                        </tr>

                        <tr id="closeRefType" {if $refund eq 'N'}style="display:none;"{/if}>
                            <td>Refund Type<span style="color:red;">&nbsp;*</span></td>
                            <td>
                                <select name="refund_type[]" multiple="multiple">
                                    {foreach key=key item=item from=$refund_types}
                                        <option value="{$key}" {if $refund_type_detail.$key} selected="selected" {/if}>{$item}</option>
                                    {/foreach}
                                </select>
                            </td>
                        </tr>

                        <tr id="closeRefValue_coupon" {if $refund_type_detail.coupon.coupon eq ''}style="display:none;"{/if}>
                            <td colspan="2">
                                <table>
                                    <tr>
                                        <td width="200px">Coupon Code<span style="color:red;">&nbsp;*</span></td>
                                        <td><input type="text" name="ref_coupon_code" id="closeRefCouponCode" value="{if $smarty.post.ref_coupon_code}{$smarty.post.ref_coupon_code}{else}{$refund_type_detail.coupon.coupon}{/if}"/>&nbsp;<a href="javascript:void(0);" id="validateCloseRefCoupon" style="font-size:12px;font-weight:bold;">Validate</a></td>
                                    </tr>
                                    <tr>
                                        <td>Coupon Detail<span style="color:red;">&nbsp;*</span></td>
                                        <td>
                                            <div id="closeRefCouponValidateError"></div>
                                            <div id="closeRefCouponDetail"></div>
                                        </td>
                                    </tr>
                                </table>
                            </td>

                        </tr>

                        <tr id="closeRefValue_cash" {if $refund_type_detail.cash.amount eq ''}style="display:none;"{/if}>
                            <td colspan="2">
                                <table>
                                    <tr>
                                        <td width="200px">Cash Amount<span style="color:red;">&nbsp;*</span></td>
                                        <td><input type="text" name="ref_cash_amount" value="{if $smarty.post.ref_cash_amount}{$smarty.post.ref_cash_amount}{else}{$refund_type_detail.cash.amount}{/if}"/></td>
                                    </tr>
                                    <tr>
                                        <td>Cash Date<span style="color:red;">&nbsp;*</span></td>
                                        <td>
                                            <input type="text" name="ref_cash_date_close" id="ref_cash_date_close" value="{if $smarty.post.ref_cash_date_close}{$smarty.post.ref_cash_date_close}{else}{$refund_type_detail.cash.trans_date}{/if}" readonly="readonly"/>                                            
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Cash Comment</td>
                                        <td><textarea name="ref_cash_description">{if $smarty.post.ref_cash_description}{$smarty.post.ref_cash_description}{else}{$refund_type_detail.cash.summary}{/if}</textarea></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                        <tr id="closeRefValue_cashback" {if $refund_type_detail.cashback.amount eq ''}style="display:none;"{/if}>
                            <td colspan="2">
                                <table>
                                    <tr>
                                        <td width="200px">Cashback Transaction<span style="color:red;">&nbsp;*</span></td>
                                        <td>
                                            <select name="ref_cashback_transaction">
                                                {if $refund_type_detail.cashback.transaction eq 'credit'}
                                                    <option value="credit" selected="selected">Credit</option>
                                                    <option value="debit">Debit</option>
                                                {else}
                                                    <option value="credit">Credit</option>
                                                    <option value="debit" selected="selected">Debit</option>
                                                {/if}
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Cashback Amount<span style="color:red;">&nbsp;*</span></td>
                                        <td><input type="text" name="ref_cashback_amount" value="{if $smarty.post.ref_cashback_amount}{$smarty.post.ref_cashback_amount}{else}{$refund_type_detail.cashback.amount}{/if}"/></td>
                                    </tr>
                                    <tr>
                                        <td>Cashback Date<span style="color:red;">&nbsp;*</span></td>
                                        <td>
                                            <input type="text" name="ref_cashback_date_close" id="ref_cashback_date_close" value="{if $smarty.post.ref_cashback_date_close}{$smarty.post.ref_cashback_date_close}{else}{$refund_type_detail.cashback.trans_date}{/if}" readonly="readonly"/>                                            
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                        <tr id="closeRefValue_cheque" {if $refund_type_detail.cheque.amount eq ''}style="display:none;"{/if}>
                            <td colspan="2">
                                <table>
                                    <tr>
                                        <td width="200px">Cheque Name<span style="color:red;">&nbsp;*</span></td>
                                        <td><input type="text" name="ref_cheque_name" value="{if $smarty.post.ref_cheque_name}{$smarty.post.ref_cheque_name}{else}{$refund_type_detail.cheque.cheque_name}{/if}"/></td>
                                    </tr>
                                    <tr>
                                        <td>Cheque Number<span style="color:red;">&nbsp;*</span></td>
                                        <td><input type="text" name="ref_cheque_number" value="{if $smarty.post.ref_cheque_number}{$smarty.post.ref_cheque_number}{else}{$refund_type_detail.cheque.cheque_number}{/if}"/></td>
                                    </tr>
                                    <tr>
                                        <td>Cheque Amount<span style="color:red;">&nbsp;*</span></td>
                                        <td><input type="text" name="ref_cheque_amount" value="{if $smarty.post.ref_cheque_amount}{$smarty.post.ref_cheque_amount}{else}{$refund_type_detail.cheque.amount}{/if}"/></td>
                                    </tr>
                                    <tr>
                                        <td>Cheque Date<span style="color:red;">&nbsp;*</span></td>
                                        <td>
                                            <input type="text" name="ref_cheque_date_close" id="ref_cheque_date_close" value="{if $smarty.post.ref_cheque_date_close}{$smarty.post.ref_cheque_date_close}{else}{$refund_type_detail.cheque.trans_date}{/if}" readonly="readonly"/>                                            
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Cheque Comment</td>
                                        <td><textarea name="ref_cheque_description">{if $smarty.post.ref_cheque_description}{$smarty.post.ref_cheque_description}{else}{$refund_type_detail.cheque.summary}{/if}</textarea></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                        <tr id="closeRefValue_online" {if $refund_type_detail.online.amount eq ''}style="display:none;"{/if}>
                            <td colspan="2">
                                <table>
                                    <tr>
                                        <td width="200px">Online Amount<span style="color:red;">&nbsp;*</span></td>
                                        <td><input type="text" name="ref_online_amount" value="{if $smarty.post.ref_online_amount}{$smarty.post.ref_online_amount}{else}{$refund_type_detail.online.amount}{/if}"/></td>
                                    </tr>
                                    <tr>
                                        <td>Online Date<span style="color:red;">&nbsp;*</span></td>
                                        <td>
                                            <input type="text" name="ref_online_date_close" id="ref_online_date_close" value="{if $smarty.post.ref_online_date_close}{$smarty.post.ref_online_date_close}{else}{$refund_type_detail.online.trans_date}{/if}" readonly="readonly"/>                                            
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Online Comment</td>
                                        <td><textarea name="ref_online_description">{if $smarty.post.ref_online_description}{$smarty.post.ref_online_description}{else}{$refund_type_detail.online.summary}{/if}</textarea></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                        <tr>
                            <td width="200px">Close Summary<span style="color:red;">&nbsp;*</span></td>
                            <td>
                                <textarea name="close_summary" cols="50" rows="3">{$smarty.post.close_summary}</textarea>
                            </td>
                        </tr>

                        <tr>
                            <td width="200px">Feedback Mail<span style="color:red;">&nbsp;*</span></td>
                            <td>
                                <input type="checkbox" name="MFBSendStatus" value="1" checked="checked"/>&nbsp;uncheck, not to send feedback mail
                            </td>
                        </tr>

                        <tr>
                            <td>Compensation<span style="color:red;">&nbsp;*</span></td>
                            <td>
                                <select name="compensation" id="closeComp">
                                    <option value="N" selected="selected">N</option>
                                    <option value="Y">Y</option>
                                </select>
                            </td>
                        </tr>

                        <tr id="closeCompType" style="display:none;">
                            <td>Compensation Type<span style="color:red;">&nbsp;*</span></td>
                            <td>
                                <select name="compensation_type[]" multiple="multiple">
                                    <option value="coupon">Coupon</option>
                                    <option value="gift">Gift</option>
                                </select>
                            </td>
                        </tr>

                        <tr id="closeCompValue_coupon" style="display:none;">
                            <td colspan="2">
                                <table>
                                    <tr>
                                        <td width="200px">Coupon Code<span style="color:red;">&nbsp;*</span></td>
                                        <td><input type="text" name="coupon_code" value="{$smarty.post.coupon_code}"/>&nbsp;<a href="javascript:void(0);" id="validateCoupon" style="font-size:12px;font-weight:bold;">Validate</a></td>
                                    </tr>
                                    <tr>
                                        <td>Coupon Detail<span style="color:red;">&nbsp;*</span></td>
                                        <td>
                                            <div id="closeCouponValidateError"></div>
                                            <div id="closeCouponDetail"></div>
                                        </td>
                                    </tr>
                                </table>
                            </td>

                        </tr>

                        <tr id="closeCompValue_gift" style="display:none;">
                            <td>Gift Description<span style="color:red;">&nbsp;*</span></td>
                            <td><textarea name="gift_description">{$smarty.post.gift_description}</textarea></td>
                        </tr>                        

                        <tr>
                            <td>&nbsp;</td>
                            <td><br/><input type="button" value="Close Service Request" id="close_service_request" class="button" >&nbsp;<input type="button" value="Cancel" class="button" onclick="window.close();"></td>
                        </tr>
                        <tr><td>&nbsp;</td></tr>
                        <tr><td>&nbsp;</td></tr>
                        <tr><td>&nbsp;</td></tr>
                    </table>
                </form>
				<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
                </div>
            {elseif $success eq "Y"}
                <table>
                    <tr>
                        <td>Service Request closed succesfully!</td>
                        <td>&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;</td>
                    </tr>
                    <tr><td style="font-size:.8em;color:red;"><b>Please Click the close button below to refresh data on back screen</b></td></tr>
                    <tr><td align="center"><input type="button" style="height:20px;width:50px; background-color:black; color:white;" value="Close" onclick="opener.window.location.reload();self.close()"></td></tr>
                </table>
            {/if}
        {/if}
    </div>
</div></div></div></div></div>
</body>
</html>
