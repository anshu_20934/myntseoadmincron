{literal}
<script type="text/javascript">

$(document).ready(function(){
    /*function makePostAjax(url, inputData, cache, callBackFunc){
        $.ajax({
            type:'POST',
            url: url,
            data: inputData,
            cache:cache,
            success: callBackFunc
        });
    }*/

    $('input[name="uploadxls"]').click(function(){
        /*var uploadFile = $('input[name="updateSRFile"]').val();

        if(confirm('Are you sure to upload?')){
            $("#SRLoadImage").show();
            makePostAjax(http_loc+'/admin/ServiceRequest/bulkSRUpdate.php', 'updateSRFile='+uploadFile, false, bulkSRUpdateCallBack);

        } else {
            $("#SRLoadImage").hide();
            return false;
        }*/

        $("#error").html('');
    });

    /*function bulkSRUpdateCallBack(data){
        $("#error").html(data);
        $("#SRLoadImage").hide();
        //location.reload();
    }*/

});
</script>
{/literal}

{capture name=dialog}
<form action="" method="post" name="updateSR" enctype="multipart/form-data" >
    * Input file format : <font color='blue'>SRId, SR_category, SR_subcategory, SR_priority, SR_status, Department, Channel, Callback Date, Comment, Customer Email, Name, Mobile</font>
    <br/>
    * First line of xls/x file is ignored. First line should be the column names.
    <br />
    <!--* If remark contains comma then give remark in double quotes.-->
    <br /><br />
    Upload the xls/x file(file size not grater than 2MB) : <input type="file" name="updateSRFile" >
    <!--input type='button'  name='uploadxls'  value="Upload" -->
    <input type='submit'  name='uploadxls'  value="Upload" >
    <br/>
    {*{if $errormessage}<span style="color:red" id="error">{$errormessage}</span>{/if}*}    
    {if $errormessage}<span style="color:red" id="error">{$errormessage}</span>{/if}    
</form>
{/capture}

{include file="dialog.tpl" title="Upload SR list to update" content=$smarty.capture.dialog extra='width="100%"'}
<div id="SRLoadImage" style="display:{$display_loading}">
    <div class="overlay" style="display:block;_position:absolute;_width:1300px;_height:1500px;">&nbsp;</div>
    <div style="position: absolute; left: 50%; top: 50%;"><img src="{$cdn_base}/skin1/icons/indicator.gif"></div>
</div>