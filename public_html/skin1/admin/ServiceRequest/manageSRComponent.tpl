{* $Id: featured_products.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
{literal}
<style type="text/css">
textarea{height:28px;width:230px;}
</style>
{/literal}
{capture name=priority}
	<form action="{$http_location}/admin/ServiceRequest/manageSRComponent.php" method="post" name="addSRPriority">
		<input type="hidden" name="mode" value="addSRPriority"/>
		<table cellpadding="3" cellspacing="1" width="100%">
            <tr>
                <td align="center"><label>Priority:</label><input name="sr_priority" type="text" value="" maxlength="5"/></td>
                <td align="center"><label>Description:</label><textarea name="sr_priority_desc"></textarea></td>
                <td align="center"><label>Active:</label>
                    <select name="sr_priority_active">
                        <option value="Y" selected="selected">Y</option>
                        <option value="N">N</option>
                    </select>
                </td>
                <td align="center"><label>Default:</label>
                    <select name="sr_priority_default">
                        <option value="N" selected="selected">N</option>
                        <option value="Y">Y</option>
                    </select>
                </td>
                <td align="center"><input type="submit" value="Add"/></td>
            </tr>			
		</table>
    </form>

    <table cellpadding="3" cellspacing="1" width="100%">
        <tr class="TableHead">
            <th>Priority</th>
            <th>Description</th>
            <th>Active</th>
            <th>Default</th>
            <th>Action</th>            
        </tr>

        {section name=idx loop=$sr_priority}
            <form name="editSRPriority" method="post" action="{$http_location}/admin/ServiceRequest/manageSRComponent.php">
                <input type="hidden" name="mode" value="editSRPriority"/>
                <input type="hidden" name="sr_priority_id" value="{$sr_priority[idx].id}"/>
                <tr {cycle values=", class='TableSubHead'"}>
                    <td align="center"><input name="sr_priority" type="text" value="{$sr_priority[idx].SR_priority}" maxlength="5"/></td>
                    <td align="center"><textarea name="sr_priority_desc">{$sr_priority[idx].priority_description}</textarea></td>
                    <td align="center">
                        <select name="sr_priority_active">
                            {if $sr_priority[idx].priority_active eq 'Y'}
                                <option value="Y" selected=selected>Y</option>
                                <option value="N">N</option>
                            {else}
                                <option value="Y" >Y</option>
                                <option value="N" selected=selected>N</option>
                            {/if}
                        </select>
                    </td>
                    <td align="center">
                        <select name="sr_priority_default">
                            {if $sr_priority[idx].priority_default eq 'Y'}
                                <option value="Y" selected=selected>Y</option>
                                <option value="N">N</option>
                            {else}
                                <option value="Y" >Y</option>
                                <option value="N" selected=selected>N</option>
                            {/if}
                        </select>
                    </td>
                    <td align="center">
                        <input type="submit" value="Update" name="update"/>                        
                    </td>
                </tr>
            </form>
        {sectionelse}
            <tr>
                <td colspan="2" align="center">No data is available!</td>
            </tr>
        {/section}
    </table>
{/capture}

{capture name=status}
	<form action="{$http_location}/admin/ServiceRequest/manageSRComponent.php" method="post" name="addSRStatus">
		<input type="hidden" name="mode" value="addSRStatus"/>
		<table cellpadding="3" cellspacing="1" width="100%">
            <tr>
                <td align="center"><label>Status:</label><input name="sr_status" type="text" value="" maxlength="30"/></td>
                <td align="center"><label>Description:</label><textarea name="sr_status_desc"></textarea></td>
                <td align="center"><label>Active:</label>
                    <select name="sr_status_active">
                        <option value="Y" selected="selected">Y</option>
                        <option value="N">N</option>
                    </select>
                </td>
                <td align="center"><input type="submit" value="Add"/></td>
            </tr>
		</table>
    </form>

    <table cellpadding="3" cellspacing="1" width="100%">
        <tr class="TableHead">
            <th>Status</th>
            <th>Description</th>
            <th>Active</th>
            <th>Action</th>
        </tr>

        {section name=idx loop=$sr_status}
            <form name="editSRStatus" method="post" action="{$http_location}/admin/ServiceRequest/manageSRComponent.php">
                <input type="hidden" name="mode" value="editSRStatus"/>
                <input type="hidden" name="sr_status_id" value="{$sr_status[idx].id}"/>
                <tr {cycle values=", class='TableSubHead'"}>
                    <td align="center"><input name="sr_status" type="text" value="{$sr_status[idx].SR_status}" maxlength="30"/></td>
                    <td align="center"><textarea name="sr_status_desc">{$sr_status[idx].status_description}</textarea></td>
                    <td align="center">
                        <select name="sr_status_active">
                            {if $sr_status[idx].status_active eq 'Y'}
                                <option value="Y" selected=selected>Y</option>
                                <option value="N">N</option>
                            {else}
                                <option value="Y" >Y</option>
                                <option value="N" selected=selected>N</option>
                            {/if}
                        </select>
                    </td>
                    <td align="center">
                        {if $sr_status[idx].SR_status eq 'open' || $sr_status[idx].SR_status eq 'close'}
                            can not update
                        {else}
                            <input type="submit" value="Update" name="update"/>
                        {/if}
                    </td>
                </tr>
            </form>
        {sectionelse}
            <tr>
                <td colspan="2" align="center">No data is available!</td>
            </tr>
        {/section}
    </table>
{/capture}

{capture name=category}
	<form action="{$http_location}/admin/ServiceRequest/manageSRComponent.php" method="post" name="addSRCategory">
		<input type="hidden" name="mode" value="addSRCategory"/>
		<table cellpadding="3" cellspacing="1" width="100%">
            <tr>
                <td align="center"><label>Category:</label><input name="sr_category" type="text" value="" maxlength="100"/></td>
                <td align="center"><label>Description:</label><textarea name="sr_category_desc"></textarea></td>
                <td align="center"><label>Type:</label>
                    <select name="sr_category_type">
                        <option value="order" selected="selected">Order</option>
                        <option value="non-order">Non-Order</option>
                    </select>
                </td>
                <td align="center"><label>SLA:</label>
                    <select name="sr_category_SLA_day">
                        {section loop=$SLA_days name=idx}
                            <option value="{$SLA_days[idx]}">{$SLA_days[idx]} days</option>
                        {/section}
                    </select>
                    <select name="sr_category_SLA_hour">
                        {section loop=$SLA_hours name=idx}
                            <option value="{$SLA_hours[idx]}">{$SLA_hours[idx]} hours</option>
                        {/section}
                    </select>
                </td>
                <td align="center"><label>Active:</label>
                    <select name="sr_category_active">
                        <option value="Y" selected="selected">Y</option>
                        <option value="N">N</option>
                    </select>
                </td>
                <td align="center"><input type="submit" value="Add"/></td>
            </tr>
		</table>
    </form>

    <table cellpadding="3" cellspacing="1" width="100%">
        <tr class="TableHead">
            <th>Category</th>
            <th>Description</th>
            <th>Type</th>
            <th>SLA</th>
            <th>Active</th>
            <th>Action</th>
        </tr>

        {section name=idx loop=$sr_category}
            <form name="editSRCategory" method="post" action="{$http_location}/admin/ServiceRequest/manageSRComponent.php">
                <input type="hidden" name="mode" value="editSRCategory"/>
                <input type="hidden" name="sr_category_id" value="{$sr_category[idx].id}"/>
                <tr {cycle values=", class='TableSubHead'"}>
                    <td align="center"><input name="sr_category" type="text" value="{$sr_category[idx].SR_category}" maxlength="100"/></td>
                    <td align="center"><textarea name="sr_category_desc">{$sr_category[idx].category_description}</textarea></td>
                    <td align="center">
                        <select name="sr_category_type">
                            {if $sr_category[idx].category_type eq 'order'}
                                <option value="order" selected="selected">Order</option>
                                <option value="non-order">Non-Order</option>
                            {else}
                                <option value="order">Order</option>
                                <option value="non-order" selected="selected">Non-Order</option>
                            {/if}
                        </select>
                    </td>
                    <td align="center">
                        <select name="sr_category_SLA_day">
                            {section loop=$SLA_days name=idx1}
                                {if $sr_category[idx].SLA_day eq $SLA_days[idx1]}
                                    <option value="{$SLA_days[idx1]}" selected="selected">{$SLA_days[idx1]} days</option>
                                {else}
                                    <option value="{$SLA_days[idx1]}">{$SLA_days[idx1]} days</option>
                                {/if}
                            {/section}
                        </select>
                        <select name="sr_category_SLA_hour">
                            {section loop=$SLA_hours name=idx2}
                                {if $sr_category[idx].SLA_hour eq $SLA_hours[idx2]}
                                    <option value="{$SLA_hours[idx2]}" selected="selected">{$SLA_hours[idx2]} hours</option>
                                {else}
                                    <option value="{$SLA_hours[idx2]}">{$SLA_hours[idx2]} hours</option>
                                {/if}
                            {/section}
                        </select>
                    </td>
                    <td align="center">
                        <select name="sr_category_active">
                            {if $sr_category[idx].cat_active eq 'Y'}
                                <option value="Y" selected=selected>Y</option>
                                <option value="N">N</option>
                            {else}
                                <option value="Y" >Y</option>
                                <option value="N" selected=selected>N</option>
                            {/if}
                        </select>
                    </td>
                    <td align="center">
                        <input type="submit" value="Update" name="update"/>
                    </td>
                </tr>
            </form>
        {sectionelse}
            <tr>
                <td colspan="2" align="center">No data is available!</td>
            </tr>
        {/section}
    </table>
{/capture}

{capture name=subcategory}
	<form action="{$http_location}/admin/ServiceRequest/manageSRComponent.php" method="post" name="addSRSubCategory">
		<input type="hidden" name="mode" value="addSRSubCategory"/>
		<table cellpadding="3" cellspacing="1" width="100%">
            <tr>
                <td align="center"><label>Sub-Category:</label><input name="sr_subcategory" type="text" value="" maxlength="200"/></td>
                <td align="center"><label>Description:</label><textarea name="sr_subcategory_desc"></textarea></td>
                <td align="center"><label>Category:</label>
                    <select name="sr_category_id">
                        {section loop=$sr_category name=catidx}
                            <option value="{$sr_category[catidx].id}">{$sr_category[catidx].SR_category}-({$sr_category[catidx].category_type})</option>                        
                        {/section}
                    </select>
                </td>
                <td align="center"><label>SLA:</label>
                    <select name="sr_subcategory_SLA_day">
                        {section loop=$SLA_days name=idx}
                            <option value="{$SLA_days[idx]}">{$SLA_days[idx]} days</option>
                        {/section}
                    </select>
                    <select name="sr_subcategory_SLA_hour">
                        {section loop=$SLA_hours name=idx}
                            <option value="{$SLA_hours[idx]}">{$SLA_hours[idx]} hours</option>
                        {/section}
                    </select>
                </td>
                <td align="center"><label>Active:</label>
                    <select name="sr_subcategory_active">
                        <option value="Y" selected="selected">Y</option>
                        <option value="N">N</option>
                    </select>
                </td>
                <td align="center"><input type="submit" value="Add"/></td>
            </tr>
		</table>
    </form>

    <table cellpadding="3" cellspacing="1" width="100%">
        <tr class="TableHead">
            <th>Sub-Category</th>
            <th>Description</th>
            <th>Category</th>
            <th>SLA</th>
            <th>Active</th>
            <th>Action</th>
        </tr>

        {section name=idx loop=$sr_subcategory}
            <form name="editSRSubCategory" method="post" action="{$http_location}/admin/ServiceRequest/manageSRComponent.php">
                <input type="hidden" name="mode" value="editSRSubCategory"/>
                <input type="hidden" name="sr_subcategory_id" value="{$sr_subcategory[idx].subcat_id}"/>
                <tr {cycle values=", class='TableSubHead'"}>
                    <td align="center"><input name="sr_subcategory" type="text" value="{$sr_subcategory[idx].SR_subcategory}" maxlength="30"/></td>
                    <td align="center"><textarea name="sr_subcategory_desc">{$sr_subcategory[idx].subcategory_description}</textarea></td>
                    <td align="center">
                        <select name="sr_category_id">
                            {section loop=$sr_category name=catidx}
                                {if $sr_category[catidx].id eq $sr_subcategory[idx].cat_id}
                                    <option value="{$sr_category[catidx].id}" selected="selected">{$sr_category[catidx].SR_category}-({$sr_category[catidx].category_type})</option>
                                {else}
                                    <option value="{$sr_category[catidx].id}">{$sr_category[catidx].SR_category}-({$sr_category[catidx].category_type})</option>
                                {/if}
                            {/section}
                        </select>
                    </td>
                    <td align="center">
                        <select name="sr_subcategory_SLA_day">
                            {section loop=$SLA_days name=idx1}
                                {if $sr_subcategory[idx].SLA_day eq $SLA_days[idx1]}
                                    <option value="{$SLA_days[idx1]}" selected="selected">{$SLA_days[idx1]} days</option>
                                {else}
                                    <option value="{$SLA_days[idx1]}">{$SLA_days[idx1]} days</option>
                                {/if}
                            {/section}
                        </select>
                        <select name="sr_subcategory_SLA_hour">
                            {section loop=$SLA_hours name=idx2}
                                {if $sr_subcategory[idx].SLA_hour eq $SLA_hours[idx2]}
                                    <option value="{$SLA_hours[idx2]}" selected="selected">{$SLA_hours[idx2]} hours</option>
                                {else}
                                    <option value="{$SLA_hours[idx2]}">{$SLA_hours[idx2]} hours</option>
                                {/if}
                            {/section}
                        </select>
                    </td>
                    <td align="center">
                        <select name="sr_subcategory_active">
                            {if $sr_subcategory[idx].subcat_active eq 'Y'}
                                <option value="Y" selected=selected>Y</option>
                                <option value="N">N</option>
                            {else}
                                <option value="Y" >Y</option>
                                <option value="N" selected=selected>N</option>
                            {/if}
                        </select>
                    </td>
                    <td align="center">
                        <input type="submit" value="Update" name="update"/>
                    </td>
                </tr>
            </form>
        {sectionelse}
            <tr>
                <td colspan="2" align="center">No data is available!</td>
            </tr>
        {/section}
    </table>
{/capture}

{include file="dialog.tpl" title="SR Priority" content=$smarty.capture.priority extra='width="100%"'}
<br/>
{include file="dialog.tpl" title="SR Status" content=$smarty.capture.status extra='width="100%"'}
<br/>
{include file="dialog.tpl" title="SR Category" content=$smarty.capture.category extra='width="100%"'}
<br/>
{include file="dialog.tpl" title="SR Sub-Category" content=$smarty.capture.subcategory extra='width="100%"'}