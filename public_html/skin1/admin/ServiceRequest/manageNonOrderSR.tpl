<html>
<head>
{ include file="meta.tpl" }
<link rel="stylesheet" href="{$http_location}/skin1/mykriti.css"/>
<link href="{$http_location}/skin1/myntra_css/jquery-ui-datepicker.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="{$http_location}/skin1/myntra_js/jquery-ui-datepicker.js"></script>

{literal}
    <script type="text/javascript">    
        var SREmail = /^[\w\-\.]+@[\w\-\.]+\.+[\w\-]{2,4}?$/;//email
        var SRPhone = /^[0-9]+[\s0-9]*$/;//phone number        
        
    $(document).ready(function(){

        //initializing datetimepicker
        $( "#callbacktime, #callbacktime_edit" ).datetimepicker({
            dateFormat:'mm/dd/yy',
            changeMonth: true,
            changeYear: true,
            showOn: 'both',
            buttonImageOnly: true,
            buttonImage: cdn_base+'/images/cal.gif',
            buttonText: 'Calendar'
        });

        //manage callback time
        //add
        $("#addCallback").change(function(){
            onchangeCallback("add",$(this));
        });
        //edit
        $("#editCallback").change(function(){
            onchangeCallback("edit",$(this));
        });
        //manage callback time

        $("#add_service_request").click(function(){
            var flag = validateEmailAndPhone('add');

            if(flag){
                $("form[name='addNonSR']").submit();
            }
        });

        $("#edit_service_request").click(function(){
            var flag = validateEmailAndPhone('edit');

            if(flag){
                $("form[name='updateNonSR']").submit();
            }
        });

    });

    function onchangeCallback(prefixId,callbackObj){
        if(callbackObj.val() == 'Y'){
            $("#"+prefixId+"CallbackTime").show();

        } else {
            $("#"+prefixId+"CallbackTime").hide();
        }
    }

    /*checks for any one among email and phone exists
     *or both and validates email and phone for regex
     */
    function validateEmailAndPhone(prefix){
        var emailObj = $("#"+prefix+"_customer_email");
        var phoneObj = $("#"+prefix+"_customer_phone");

        //check for both emoty email and phone
        if(emailObj.val() == '' && phoneObj.val() == ''){
            alert("Please enter either customer email or phone number.");
            return false;
        }

        //check for email validity if not empty
        if(emailObj.val() != ''){
            if(!validateAgainstRegex(SREmail, emailObj.val())){
                alert("Please enter valid email id eg: xxx@yyy.com");
                return false;
            }
        }

        //check for phone validity if not empty
        if(phoneObj.val() != ''){
            if(!validateAgainstRegex(SRPhone, phoneObj.val())){
                alert("Please enter valid phone number eg: 080 23457864, 23457864, 91 9874562563, 9874562563 etc..");
                return false;
            }
        }

        return true;
    }

    function validateAgainstRegex(regEx, data){
        return regEx.test(data);
    }

    </script>
{/literal}

</head>
<body id="body">
<div id="scroll_wrapper"><div id="container"><div id="wrapper"><div id="display"><div class="center_column">
    <div class="print">
        <div class="super" ><p>&nbsp;</p></div>
        {if $form_type eq 'addNonOrderServieRequest'}
            {if $success ne "Y"}
                <div class="head" style="margin:0px 0px 20px;">
                    <p>Add Service Request</p>
                    <p style="color:red">{$message}</p>
                </div>
                <div class="links"><p></p></div>
                <div class="foot"></div>
                <div style="height:580px;overflow:scroll">
                <form method="post" name="addNonSR"  action="{$http_location}/admin/ServiceRequest/manageNonOrderSR.php">
                    <input type="hidden" name="mode" value="{$form_type}"/>
                    <input type="hidden" name="sr_post_mode" value="1"/>

                    <table>
                        <tr>
                            <td>Category<span style="color:red;">&nbsp;*</span></td>
                            <td>
                                <select name="subcategory">
                                    {section name=category loop=$sr_category}
                                        {if $sr_category[category].category_type eq 'non-order' && $sr_category[category].cat_active eq 'Y'}
                                            <optgroup label="{$sr_category[category].SR_category}-({$sr_category[category].SLA_string})"></optgroup>

                                            {assign var=subcategories value=$sr_category[category].subCategories}
                                            {section name=index loop=$subcategories}
                                                {if $subcategories[index].subcat_active eq 'Y'}
                                                    <option value="{$subcategories[index].id}">{$subcategories[index].SR_subcategory}-({$subcategories[index].SLA_string})</option>
                                                {/if}
                                            {/section}
                                        {/if}
                                    {/section}
                                </select>
                            </td>
                        </tr>

                        <tr>
                            <td>Priority<span style="color:red;">&nbsp;*</span></td>
                            <td>
                                <select name="priority">                                    
                                    {section name=index loop=$sr_priority}
                                        {if $sr_priority[index].priority_active eq 'Y'}
                                            {if $sr_priority[index].priority_default eq 'Y'}
                                                <option value="{$sr_priority[index].id}" selected="selected">{$sr_priority[index].SR_priority}</option>
                                            {else}
                                                <option value="{$sr_priority[index].id}">{$sr_priority[index].SR_priority}</option>
                                            {/if}
                                        {/if}
                                    {/section}
                                </select>
                            </td>
                        </tr>

                        <tr>
                            <td>Department<span style="color:red;">&nbsp;*</span></td>
                            <td>
                                <select name="department">
                                    {section name=index loop=$sr_department}                                        
                                        <option value="{$sr_department[index]}">{$sr_department[index]}</option>                                                 
                                    {/section}
                                </select>
                            </td>
                        </tr>

                        <tr>
                            <td>Channel<span style="color:red;">&nbsp;*</span></td>
                            <td>
                                <select name="channel">
                                    {section name=index loop=$sr_channel}
                                        <option value="{$sr_channel[index]}">{$sr_channel[index]}</option>
                                    {/section}
                                </select>
                            </td>
                        </tr>

                        <tr>
                            <td>Callback<span style="color:red;">&nbsp;*</span></td>
                            <td>
                                <select name="callback" id="addCallback">
                                    <option value="N" selected="selected">N</option>
                                    <option value="Y">Y</option>
                                </select>
                            </td>
                        </tr>

                         <tr id="addCallbackTime" style="display:none;">
                            <td>Callback Time<span style="color:red;">&nbsp;*</span></td>
                            <td>
                                <input type="text" name="callbacktime" id="callbacktime" value="{$smarty.post.callbacktime}" readonly="readonly"/>
                            </td>
                        </tr>

                        <tr>
                            <td>Summary<span style="color:red;">&nbsp;*</span></td>
                            <td><textarea name="create_summary" cols="50" rows="3">{$smarty.post.create_summary}</textarea></td>
                        </tr>

                        <tr>
                            <td>Customer Email<span style="color:red;">&nbsp;**</span></td>
                            <td><input type="text" name="customer_email" id="add_customer_email" maxlength="100" value="{$smarty.post.customer_email}"/></td>
                        </tr>

                        <tr>
                            <td>Customer Name</td>
                            <td><input type="text" name="customer_name" maxlength="45" value="{$smarty.post.customer_name}"/></td>
                        </tr>

                        <tr>
                            <td>Customer Phone<span style="color:red;">&nbsp;**</span></td>
                            <td><input type="text" name="customer_phone" id="add_customer_phone" maxlength="15" value="{$smarty.post.customer_phone}"/></td>
                        </tr>

                        <tr>
                            <td colspan="2" style="font-size:10px;"><span style="color:red;">**</span>&nbsp;One among them is mandatory</td>
                        </tr>

                        <tr>
                            <td>&nbsp;</td>
                            <td><br/><input type="button" value="Add Service Request" id="add_service_request" class="button" >&nbsp;<input type="button" value="Cancel" class="button" onclick="window.close();"></td>
                        </tr>
                        <tr><td>&nbsp;</td></tr>
                        <tr><td>&nbsp;</td></tr>
                        <tr><td>&nbsp;</td></tr>
                    </table>
                </form>
                </div>
            {elseif $success eq "Y"}
                <table>
                    <tr>
                        <td>Service Request added succesfully!</td>
                        <td>&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;</td>
                    </tr>
                    <tr><td style="font-size:.8em;color:red;"><b>Please Click the close button below to refresh data on back screen</b></td></tr>
                    <tr><td align="center"><input type="button" style="height:20px;width:50px; background-color:black; color:white;" value="Close" onclick="opener.window.location.reload();self.close()"></td></tr>
                </table>
            {/if}

        {elseif $form_type eq 'updateNonOrderServieRequest'}

            {if $success ne "Y"}
                <div class="head" style="margin:0px 0px 20px;">
                    <p>Edit Service Request : #{$non_order_sr_id}</p>
                    <p style="color:red;">{$message}</p>
                </div>
                <div class="links"><p></p></div>
                <div class="foot"></div>
                <div style="height:580px;overflow:scroll">
                <form method="post" name="updateNonSR"  action="{$http_location}/admin/ServiceRequest/manageNonOrderSR.php">
                    <input type="hidden" name="mode" value="{$form_type}"/>
                    <input type="hidden" name="sr_post_mode" value="1"/>
                    <input type="hidden" name="non_order_sr_id" value="{$non_order_sr_id}"/>
                    <table>
                        <tr>
                            <td>Category<span style="color:red;">&nbsp;*</span></td>
                            <td>
                                <select name="subcategory">
                                    {section name=category loop=$sr_category}
                                        {if $sr_category[category].category_type eq 'non-order' && $sr_category[category].cat_active eq 'Y'}
                                            <optgroup label="{$sr_category[category].SR_category}-({$sr_category[category].SLA_string})"></optgroup>

                                            {assign var=subcategories value=$sr_category[category].subCategories}
                                            {section name=index loop=$subcategories}
                                                {if $subcategories[index].subcat_active eq 'Y'}
                                                    {if $non_order_sr[0].sub_category_id eq $subcategories[index].id}
                                                        <option value="{$subcategories[index].id}" selected="selected">{$subcategories[index].SR_subcategory}-({$subcategories[index].SLA_string})</option>
                                                    {else}
                                                        <option value="{$subcategories[index].id}">{$subcategories[index].SR_subcategory}-({$subcategories[index].SLA_string})</option>
                                                    {/if}
                                                {/if}
                                            {/section}
                                        {/if}
                                    {/section}
                                </select>
                            </td>
                        </tr>

                        <tr>
                            <td>Priority<span style="color:red;">&nbsp;*</span></td>
                            <td>
                                <select name="priority">
                                    {section name=index loop=$sr_priority}
                                        {if $sr_priority[index].priority_active eq 'Y'}
                                            {if $non_order_sr[0].priority_id eq $sr_priority[index].id}
                                                <option value="{$sr_priority[index].id}" selected="selected">{$sr_priority[index].SR_priority}</option>
                                            {else}
                                                <option value="{$sr_priority[index].id}">{$sr_priority[index].SR_priority}</option>
                                            {/if}
                                        {/if}
                                    {/section}
                                </select>
                            </td>
                        </tr>

                        <tr>
                            <td>Status<span style="color:red;">&nbsp;*</span></td>
                            <td>
                                <select name="status">
                                    {foreach key=key item=item from=$sr_status}
                                        {if $item.status_active eq 'Y'}
                                            {if $non_order_sr[0].status_id eq $item.id}
                                                <option value="{$item.id}" selected="selected">{$item.SR_status}</option>
                                            {else}
                                                <option value="{$item.id}">{$item.SR_status}</option>
                                            {/if}
                                        {/if}
                                    {/foreach}
                                </select>
                            </td>
                        </tr>

                        <tr>
                            <td>Department<span style="color:red;">&nbsp;*</span></td>
                            <td>
                                <select name="department">
                                    {section name=index loop=$sr_department}
                                        {if $non_order_sr[0].SR_department eq $sr_department[index]}
                                            <option value="{$sr_department[index]}" selected="selected">{$sr_department[index]}</option>
                                        {else}
                                            <option value="{$sr_department[index]}">{$sr_department[index]}</option>
                                        {/if}
                                    {/section}
                                </select>
                            </td>
                        </tr>

                        <tr>
                            <td>Channel<span style="color:red;">&nbsp;*</span></td>
                            <td>
                                <select name="channel">
                                    {section name=index loop=$sr_channel}
                                        {if $non_order_sr[0].SR_channel eq $sr_channel[index]}
                                            <option value="{$sr_channel[index]}" selected="selected">{$sr_channel[index]}</option>
                                        {else}
                                            <option value="{$sr_channel[index]}">{$sr_channel[index]}</option>
                                        {/if}
                                    {/section}
                                </select>
                            </td>
                        </tr>

                        <tr>
                            <td>Callback<span style="color:red;">&nbsp;*</span></td>
                            <td>
                                <select name="callback" id="editCallback">
                                    {if !$callbackTime}
                                        <option value="N" selected="selected">N</option>
                                        <option value="Y">Y</option>
                                    {else}
                                        <option value="N">N</option>
                                        <option value="Y" selected="selected">Y</option>
                                    {/if}
                                </select>
                            </td>
                        </tr>

                        <tr id="editCallbackTime" {if !$callbackTime}style="display:none;"{/if}>
                            <td>Callback Time<span style="color:red;">&nbsp;*</span></td>
                            <td>
                                <input type="text" name="callbacktime" id="callbacktime_edit" value="{if $smarty.post.callbacktime}{$smarty.post.callbacktime}{else}{$callbackTime}{/if}" readonly="readonly"/>
                            </td>
                        </tr>

                        <tr>
                            <td>Summary<span style="color:red;">&nbsp;*</span></td>
                            <td><textarea name="create_summary" cols="50" rows="3" readonly>{if $smarty.post.create_summary}{$smarty.post.create_summary}{else}{$non_order_sr[0].create_summary}{/if}</textarea></td>
                        </tr>

                        <tr>
                            <td>Comment</td>
                            <td><textarea name="comment" cols="50" rows="3">{if $smarty.post.comment}{$smarty.post.comment}{/if}</textarea></td>
                        </tr>

                        <tr>
                            <td>Customer Email<span style="color:red;">&nbsp;**</span></td>
                            <td><input type="text" name="customer_email" id="edit_customer_email" maxlength="100" value="{if $smarty.post.customer_email}{$smarty.post.customer_email}{else}{$non_order_sr[0].customer_email}{/if}"/></td>
                        </tr>                        

                        <tr>
                            <td>Customer Name</td>
                            <td><input type="text" name="customer_name" maxlength="45" value="{if $smarty.post.customer_name}{$smarty.post.customer_name}{else}{$non_order_sr[0].first_name}{/if}"/></td>
                        </tr>

                        <tr>
                            <td>Customer Phone<span style="color:red;">&nbsp;**</span></td>
                            <td><input type="text" name="customer_phone" id="edit_customer_phone" maxlength="15" value="{if $smarty.post.customer_phone}{$smarty.post.customer_phone}{else}{$non_order_sr[0].mobile}{/if}"/></td>
                        </tr>

                        <tr>
                            <td colspan="2" style="font-size:10px;"><span style="color:red;">**</span>&nbsp;One among them is mandatory</td>
                        </tr>

                        <tr>
                            <td>&nbsp;</td>
                            <td><br/><input type="button" value="Update Service Request" id="edit_service_request" class="button" >&nbsp;<input type="button" value="Cancel" class="button" onclick="window.close();"></td>
                        </tr>
                        <tr><td>&nbsp;</td></tr>
                        <tr><td>&nbsp;</td></tr>
                        <tr><td>&nbsp;</td></tr>
                    </table>
                </form>
                </div>
            {elseif $success eq "Y"}
                <table>
                    <tr>
                        <td>Service Request updated succesfully!</td>
                        <td>&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;</td>
                    </tr>
                    <tr><td style="font-size:.8em;color:red;"><b>Please Click the close button below to refresh data on back screen</b></td></tr>
                    <tr><td align="center"><input type="button" style="height:20px;width:50px; background-color:black; color:white;" value="Close" onclick="opener.window.location.reload();self.close()"></td></tr>
                </table>
            {/if}


        {elseif $form_type eq 'closeNonOrderServieRequest'}

            {if $success ne "Y"}
                <div class="head" style="margin:0px 0px 20px;">
                    <p>Close Service Request : #{$non_order_sr_id}</p>
                    <p style="color:red;">{$message}</p>
                </div>
                <div class="links"><p></p></div>
                <div class="foot"></div>
                <div style="height:580px;overflow:scroll">
                <form method="post" name="closeNonSR"  action="{$http_location}/admin/ServiceRequest/manageNonOrderSR.php">
                    <input type="hidden" name="mode" value="{$form_type}"/>
                    <input type="hidden" name="sr_post_mode" value="1"/>
                    <input type="hidden" name="non_order_sr_id" value="{$non_order_sr_id}"/>
                    <table>

                        <tr>
                            <td>Close Summary<span style="color:red;">&nbsp;*</span></td>
                            <td>
                                <textarea name="close_summary" cols="50" rows="3">{$smarty.post.close_summary}</textarea>
                            </td>
                        </tr>

                        <tr>
                            <td>Feedback Mail<span style="color:red;">&nbsp;*</span></td>
                            <td>
                                <input type="checkbox" name="MFBSendStatus" value="1" checked="checked"/>&nbsp;uncheck, not to send feedback mail
                            </td>
                        </tr>

                        <tr>
                            <td>&nbsp;</td>
                            <td><br/><input type="submit" value="Close Service Request" class="button" >&nbsp;<input type="button" value="Cancel" class="button" onclick="window.close();"></td>
                        </tr>
                        <tr><td>&nbsp;</td></tr>
                        <tr><td>&nbsp;</td></tr>
                        <tr><td>&nbsp;</td></tr>
                    </table>
                </form>
                </div>
            {elseif $success eq "Y"}
                <table>
                    <tr>
                        <td>Service Request closed succesfully!</td>
                        <td>&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;</td>
                    </tr>
                    <tr><td style="font-size:.8em;color:red;"><b>Please Click the close button below to refresh data on back screen</b></td></tr>
                    <tr><td align="center"><input type="button" style="height:20px;width:50px; background-color:black; color:white;" value="Close" onclick="opener.window.location.reload();self.close()"></td></tr>
                </table>
            {/if}

        {/if}
    </div>
</div></div></div></div></div>
</body>
</html>