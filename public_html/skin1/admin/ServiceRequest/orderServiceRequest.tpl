{* $Id: order_returns.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
{literal}
<style type="text/css">
.selectTR{background-color:#f0c620}
</style>
<script type="text/javascript">

    $(document).ready(function(){
         $('#SRListTable tr').toggle(function(){
            if(!$(this).hasClass('SRTableHead')){
                $(this).addClass('selectTR');
            }
        }, function(){
            if(!$(this).hasClass('SRTableHead')){
                $(this).removeClass('selectTR');
            }
        }); 
    });

    function deleteSR(orderSRId){
        if(confirm('Are you sure to delete?')){
            makePostAjax(http_loc+'/admin/manageOrderSR.php', 'mode=deleteServieRequest&order_sr_id='+orderSRId, false, deleteSRCallBack);
        } else {
            return false;
        }
    }

    function makePostAjax(url, inputData, cache, callBackFunc){
        $.ajax({
            type:'POST',
            url: url,
            data: inputData,
            cache:cache,
            success: callBackFunc
        });
    }

    function deleteSRCallBack(data){
        alert(data);
        location.reload();
    }


</script>
{/literal}
<br /><br />
{capture name=dialog}
	<div style="width:1125px;overflow-x:scroll;">
		<div style="font-size:12px; padding: 5px;">List of Service Requests of order:</div>
		<div style="font-size:15px; padding: 5px;" align="center">{$SRmessage}</div>
		<table cellspacing=0 cellpadding=0 border=1 width="100%" style="border: 1px solid black; border-collapse: collapse;" title="scroll horizontally to view all columns" id="SRListTable">
			<tr height=30 style="background-color: #ccffff" class="SRTableHead">
				<th style="font-size:14px; padding: 5px;">Id</th>
				<th style="font-size:14px; padding: 5px;">Customer SR Id</th>
				<th style="font-size:14px; padding: 5px;">Category</th>
				<th style="font-size:14px; padding: 5px;">Sub Category</th>
				<th style="font-size:14px; padding: 5px;">Priority</th>
				<th style="font-size:14px; padding: 5px;">Status</th>
				<th style="font-size:14px; padding: 5px;">Department</th>
				<th style="font-size:14px; padding: 5px;">Channel</th>
				<th style="font-size:14px; padding: 5px;">Callback Time</th>
				<th style="font-size:14px; padding: 5px;">Create Date</th>
				<th style="font-size:14px; padding: 5px;">Ageing</th>
				<th style="font-size:14px; padding: 5px;">Due Date</th>
				<th style="font-size:14px; padding: 5px;">Deviation</th>
				<th style="font-size:14px; padding: 5px;">Creator</th>
				<th style="font-size:14px; padding: 5px;">Summary</th>
				<th style="font-size:14px; padding: 5px;">Updated by</th>
				<th style="font-size:14px; padding: 5px;">Close Date</th>
				<th style="font-size:14px; padding: 5px;">Resolver</th>
				<th style="font-size:14px; padding: 5px;">Close Summary</th>
				<th style="font-size:14px; padding: 5px;">Order Status</th>
                <th style="font-size:14px; padding: 5px;">Payment Method</th>
                <th style="font-size:14px; padding: 5px;">Delivery Ageing</th>
                <th style="font-size:14px; padding: 5px;">Warehouse ID</th>
                <th style="font-size:14px; padding: 5px;">Courier</th>
                <th style="font-size:14px; padding: 5px;">Waybill Number</th>				
				<th style="font-size:14px; padding: 5px;">Address</th>
				<th style="font-size:14px; padding: 5px;">State</th>
				<th style="font-size:14px; padding: 5px;">City</th>
				<th style="font-size:14px; padding: 5px;">Zip</th>
				<th style="font-size:14px; padding: 5px;">DC Info</th>
				<th style="font-size:14px; padding: 5px;">Refund</th>
				<th style="font-size:14px; padding: 5px;">Refund Type</th>
				<th style="font-size:14px; padding: 5px;">Refund Val</th>
				<th style="font-size:14px; padding: 5px;">Compensation</th>
				<th style="font-size:14px; padding: 5px;">Comp Type</th>
				<th style="font-size:14px; padding: 5px;">Comp Val</th>
				<th style="font-size:14px; padding: 5px;">Action</th>
			</tr>
			{section name=index loop=$order_sr}
			<tr bgcolor="{cycle values='#eeddff,#d0ddff'}">
				<td style="font-size:12px; padding: 5px;">{$order_sr[index].SR_id}</td>
				<td style="font-size:12px; padding: 5px;">{$order_sr[index].customer_SR_id}</td>
				<td style="font-size:12px; padding: 5px;">{$order_sr[index].SR_category}</td>
				<td style="font-size:12px; padding: 5px;"><textarea>{$order_sr[index].SR_subcategory}</textarea></td>
				<td style="font-size:12px; padding: 5px;">{$order_sr[index].SR_priority}</td>
				<td style="font-size:12px; padding: 5px;">{$order_sr[index].SR_status}</td>
				<td style="font-size:12px; padding: 5px;">{$order_sr[index].SR_department}</td>
				<td style="font-size:12px; padding: 5px;">{$order_sr[index].SR_channel}</td>
				<td style="font-size:12px; padding: 5px;">{$order_sr[index].callbacktime_date}</td>
				<td style="font-size:12px; padding: 5px;">{$order_sr[index].created_date}</td>
				<td style="font-size:12px; padding: 5px;">{$order_sr[index].ageing}</td>
				<td style="font-size:12px; padding: 5px;">{$order_sr[index].due_date}</td>
				<td style="font-size:12px; padding: 5px;">{$order_sr[index].deviation}</td>
				<td style="font-size:12px; padding: 5px;">{$order_sr[index].reporter}</td>
				<td style="font-size:12px; padding: 5px;"><textarea>{$order_sr[index].create_summary}</textarea></td>
				<td style="font-size:12px; padding: 5px;">{$order_sr[index].updatedby}</td>
				<td style="font-size:12px; padding: 5px;">{$order_sr[index].closed_date}</td>
				<td style="font-size:12px; padding: 5px;">{$order_sr[index].resolver}</td>
				<td style="font-size:12px; padding: 5px;"><textarea>{$order_sr[index].close_summary}</textarea></td>
				<td style="font-size:12px; padding: 5px;">{$order_sr[index].status}</td>
				<td style="font-size:12px; padding: 5px;">{$order_sr[index].payment_method}</td>
				<td style="font-size:12px; padding: 5px;">{$order_sr[index].delivery_ageing}</td>
				<td style="font-size:12px; padding: 5px;">{$order_sr[index].warehouseid}</td>
				<td style="font-size:12px; padding: 5px;">{$order_sr[index].courier_service}</td>
				<td style="font-size:12px; padding: 5px;">{$order_sr[index].tracking}</td>
				<td style="font-size:12px; padding: 5px;"><textarea>{$order_sr[index].order_address}</textarea></td>
				<td style="font-size:12px; padding: 5px;">{$order_sr[index].order_state}</td>
				<td style="font-size:12px; padding: 5px;">{$order_sr[index].order_city}</td>
				<td style="font-size:12px; padding: 5px;">{$order_sr[index].order_zipcode}</td>
				<td style="font-size:12px; padding: 5px;">{$order_sr[index].DC_info}</td>
				<td style="font-size:12px; padding: 5px;">{$order_sr[index].refund}</td>
				<td style="font-size:12px; padding: 5px;">{$order_sr[index].refund_type}</td>
				<td style="font-size:12px; padding: 5px;">{$order_sr[index].refund_value}</td>
				<td style="font-size:12px; padding: 5px;">{$order_sr[index].compensation}</td>
				<td style="font-size:12px; padding: 5px;">{$order_sr[index].compensation_type}</td>
				<td style="font-size:12px; padding: 5px;">{$order_sr[index].compensation_value}</td>
				<td style="font-size:12px; padding: 5px;">
				{if $order_sr[index].SR_status neq 'close'}
				    {if $SR_admin}
                        <a href="javascript:void(0);"  onClick="javascript:windowOpener(http_loc+'/admin/manageOrderSR.php?orderid={$orderid}&order_sr_id={$order_sr[index].id}&mode=updateServieRequest','{$order_sr[index].SR_id}', 'menubar=0,resizable=1,toolbar=0,width=800,height=500');">Edit</a>
                        &nbsp;|&nbsp;
				    {/if}
				    <a href="javascript:void(0);"  onClick="javascript:windowOpener(http_loc+'/admin/manageOrderSR.php?orderid={$orderid}&order_sr_id={$order_sr[index].id}&mode=closeServieRequest','{$order_sr[index].SR_id}', 'menubar=0,resizable=1,toolbar=0,width=800,height=500');">Close</a>
				    &nbsp;|&nbsp;<!--a href="#"  onClick="javascript:window.open('manageOrderSRComment.php?orderid={$orderid}&mode=addServieRequest','null', 'menubar=0,resizable=1,toolbar=0,width=800,height=500')">Comment</a-->
                {/if}
                {if $SR_admin}
                    <a href="javascript:void(0)" onclick="javascript:deleteSR('{$order_sr[index].id}');">Delete</a>
                {/if}
                </td>
			</tr>
			{/section}
		</table>
	</div>
{/capture}
{include file="dialog.tpl" title='Order Service Request' content=$smarty.capture.dialog extra='width="99%"'}
