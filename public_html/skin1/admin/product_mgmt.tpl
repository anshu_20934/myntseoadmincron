{* $Id: menu.tpl,v 1.68.2.2 2006/06/20 05:52:05 svowl Exp $ *}
{capture name=menu}
<li><a href="{$catalogs.admin}/admin_product_types.php" class="VertMenuItems">{$lng.lbl_producttypes}</a></li>
<li><a href="{$catalogs.admin}/filtergroups.php" class="VertMenuItems">Filter Groups</a></li>
<li><a href="{$catalogs.admin}/addfilters.php" class="VertMenuItems">Add Filters</a></li>
<li><a href="{$catalogs.admin}/shipping.php" class="VertMenuItems">{$lng.lbl_shipping_methods}</a></li>
{/capture}
{include file="menu.tpl" dingbats="dingbats_categorie.gif" menu_title='Product Mgmt' menu_content=$smarty.capture.menu }