Myntra.MultiBanner = (function(){
	function _multibanner(){
		var _this = this,
			markUp = '<li data-zone-index=""><h3>Zone 1</h3><div class="coord"><label>X1:<sup>*</sup> </label><input class="coordinateValue" type="text" value="0"/></div><div class="coord"><label>Y1:<sup>*</sup> </label><input class="coordinateValue" type="text" value="0"/></div><div class="coord"><label>X2:<sup>*</sup> </label><input class="coordinateValue" type="text" value="100"/></div><div class="coord"><label>Y2:<sup>*</sup> </label><input type="text" class="coordinateValue" value="100"/></div><div class="url"><label>URL:<sup>*</sup> </label><input class="urlValue" type="text" /></div><div class="title"><label>Title: </label><input class="titleValue" type="text" /></div><div class="addRemoveZone"><a href="javascript:void(0);" class="removeZone">Remove Zone</a></div></li>';
		_this.init = function(){			
			_this.attachEventHandlers();
			_this.attachValidation();
		};

		_this.getResizeZones = function(){
			$('.bannerItemWrapper').each(function(){
				var targetObject = $(this).find('.resizeWrapper img.banner'),
					targetWidth = targetObject.attr('data-width'),
					targetHeight = targetObject.attr('data-height'),
					sourceWidth = targetObject.width(),
					sourceHeight = targetObject.height(),
					heightRatio = targetHeight/sourceHeight,
					widthRatio = targetWidth/sourceWidth;
				if($(this).find('.zone-map > li').length){
					$(this).find('.zone-map > li').each(function(i){
						var currObj = $(this),
							left = currObj.find('.coord').eq(0).find('.coordinateValue').val(),
							top = currObj.find('.coord').eq(1).find('.coordinateValue').val(),
							width = Math.abs(currObj.find('.coord').eq(2).find('.coordinateValue').val()-left),
							height = Math.abs(currObj.find('.coord').eq(3).find('.coordinateValue').val()-top),
							maxTargetHeight = targetHeight/heightRatio,
							maxTargetWidth = targetWidth/widthRatio;
							left = left/widthRatio;
							top = top/heightRatio;
							height = height/widthRatio;
							width = width/widthRatio;							
						if(height>1&&width>1){							
							targetObject.parents('.resizeWrapper').append('<div style="top: '+top+'px; left: '+left+'px; width: '+width+'px; height: '+height+'px; position:absolute;" class="resizeIndex resizeDiv'+i+'"></div>');
							targetObject.parents('.resizeWrapper').find('.resizeDiv'+i).draggable({
								stop: function(evt,ui){
									_this.attachNewPosition($(this),$(this).parents('.bannerItemWrapper'),evt,ui, heightRatio, widthRatio);
								}
							}).resizable({
								maxHeight: sourceHeight,
								maxWidth: sourceWidth,
								stop: function(evt, ui){
									_this.attachNewPosition($(this),$(this).parents('.bannerItemWrapper'),evt,ui, heightRatio, widthRatio);
								}
							});
						}
					});
				}				
			});
		}

		_this.attachEventHandlers = function(){
			$('.DIV_TABLE').delegate('click','.cb-multi',function(event){		
				if($(event.target).is(':checked')){
					_this.enableMultiBanner($(event.target));
				}
				else{
					_this.disableMultiBanner($(event.target));
				}
			});
			$('.DIV_TABLE').delegate('click','.addZone',_this.addZone);
			$('.DIV_TABLE').delegate('click','.removeZone',_this.removeZone);
			$('.addVariant').on('click',_this.openPopupForVariant);
			$('.editVariant').on('click',_this.editVariant);
		};

		_this.editVariant = function(evt){
			evt.preventDefault();
			formObj = $(evt.currentTarget).parents('.DIV_TABLE').find('form.variantUpdateForm');
			formObj.find('.parentImageId').val($(evt.currentTarget).attr('data-parent-id'));
			formObj.find('.variantFlag').val('true');
			formObj.submit();
		};

		_this.openPopupForVariant = function(evt){
			evt.preventDefault();
			var lightBoxObj = $('#'+$(evt.currentTarget).attr('data-lightbox-id')),
				lightBoxIndex = $(evt.currentTarget).attr('data-lightbox-index');
			Myntra.variantDialog[lightBoxIndex].show();
			lightBoxObj.find('.userGroup').show();
			lightBoxObj.find('.userGroup .variantFlag').val('true');
			lightBoxObj.find('.userGroup .parentImage').val($(evt.currentTarget).attr('data-parent-id'));
		};

		_this.enableMultiBanner = function(evtObj){
			var parentWrapper = evtObj.parents('.bannerItem');
			evtObj.val(1);	
			evtObj.parents('.multi-check').siblings('.multi-config').addClass('show');
		};

		_this.disableMultiBanner = function(evtObj){
			var parentWrapper = evtObj.parents('.bannerItem');
			evtObj.val(0);
			evtObj.parents('.multi-check').siblings('.multi-config').removeClass('show');
		};

		_this.addResizeable = function(wrapper){			
			var siblings = wrapper.find('.resizeIndex').length,
				targetObject = wrapper.find('img.banner'), 
				targetWidth = targetObject.attr('data-width'),
				targetHeight = targetObject.attr('data-height'),
				sourceWidth = targetObject.width(),
				sourceHeight = targetObject.height(),
				heightRatio = targetHeight/sourceHeight,
				widthRatio = targetWidth/sourceWidth;
			wrapper.append('<div class="resizeIndex resizeDiv'+siblings+'"></div>');
			wrapper.find('.resizeDiv'+siblings).draggable({
				stop: function(evt, ui){
					_this.attachNewPosition($(this),$(this).parents('.bannerItemWrapper'),evt,ui, heightRatio, widthRatio);
				}
			}).resizable({
				maxHeight: sourceHeight,
				maxWidth: sourceWidth,
				stop: function(evt, ui){
					_this.attachNewPosition($(this),$(this).parents('.bannerItemWrapper'),evt,ui, heightRatio, widthRatio);		
				}
			}).css({				
				position: 'absolute'
			});
		};

		_this.removeResizeable = function(objParent,index){
			objParent.find('.resizeIndex').eq(index).remove();
		}

		_this.updateNameAttrs = function(obj,lastElement){
			var index = parseInt(lastElement.attr('data-zone-index'),10),
				imageId = parseInt(lastElement.parents('.zone-map').attr('data-image-id')),
				currentItem = obj.parents('.multi-config').find('.zone-map li:last');
			if(!index){
				index = 0;
				imageId = obj.parents('.multi-config').find('.zone-map').attr('data-image-id');
			}
			index++;
			currentItem.attr('data-zone-index',index);
			currentItem.find('.coordinateValue').each(function(i){
				$(this).attr('name','image['+imageId+'][multi]['+index+']['+i+']');
			});
			currentItem.find('.titleValue').attr('name','image['+imageId+'][multi]['+index+'][title]');
			currentItem.find('.urlValue').attr('name','image['+imageId+'][multi]['+index+'][target]');
		};

		_this.addZone = function(event){
			var objParent = $(event.target).parents('.multi-config').find('.zone-map'),
				lastElement = objParent.find('>li:last');
			objParent.append(markUp);
			_this.updateZoneNumbers(objParent);
			_this.updateNameAttrs($(event.target), lastElement);
			_this.addResizeable(objParent.parents('.bannerItemWrapper').find('.resizeWrapper'));
		};

		_this.removeZone = function(event){
			var objParent = $(event.target).parents('li'),
				objParentWrapper = objParent.parent();					
			_this.removeResizeable(objParent.parents('.bannerItemWrapper').find('.resizeWrapper') ,objParentWrapper.find('li').index(objParent));
			objParent.remove();
			_this.updateZoneNumbers(objParentWrapper);			
		}

		_this.attachNewPosition = function(obj ,objParent, evt, ui, heightRatio, widthRatio){
			var index = objParent.find('.resizeIndex').index(obj),
				mapElement = objParent.find('.zone-map li').eq(index),
				top = parseInt(ui.position.top*heightRatio,10),
				left = parseInt(ui.position.left*widthRatio,10),
				bottom, right;
			mapElement.find('.coord').eq(0).find('.coordinateValue').val(left);
			mapElement.find('.coord').eq(1).find('.coordinateValue').val(top);
			if(ui.size){
				bottom = top+parseInt(ui.size.height*heightRatio,10);
				right = left+parseInt(ui.size.width*widthRatio,10);								
			}
			else{
				bottom = top+parseInt(obj.height()*heightRatio,10);
				right = left+parseInt(obj.width()*widthRatio,10);
			}
			mapElement.find('.coord').eq(2).find('.coordinateValue').val(right);
			mapElement.find('.coord').eq(3).find('.coordinateValue').val(bottom);
		};

		_this.updateZoneNumbers = function(obj){
			obj.find('li').each(function(i){
				$(this).find('h3').text('Zone '+(i+1));
			});
		};

		_this.validateCoords = function($form){
			var maxHeight = parseInt($form.find('img.banner').attr('data-height'),10),
				maxWidth = parseInt($form.find('img.banner').attr('data-width'),10),
				zoneMap = $form.find('.zone-map'),
				flag = true;
			zoneMap.find('>li').each(function(i){
				var currHeight,	currWidth;
				$(this).find('.coordinateValue').each(function(j){
					if(j%2==0){
						var tempX = parseInt($(this).val(),10);
						if(j==0){							
							currWidth=tempX;
						}
						if(j==2){
							currWidth=Math.abs(tempX-currWidth);
						}
						if(tempX<0||tempX>maxWidth||tempX!=$(this).val()||currWidth>maxWidth){
							alert('Enter proper coordinate value for Zone'+(i+1));
							flag = false;
						}						
						if(currHeight>maxHeight){

						}
					}
					else{
						var tempY = parseInt($(this).val(),10);
						if(j==1){							
							currWidth=tempY;
						}
						if(j==3){
							currWidth=Math.abs(tempY-currHeight);
						}
						if(tempY<0||tempY>maxHeight||tempY!=$(this).val()||currHeight>maxHeight){
							alert('Enter proper coordinate values');
							flag = false;	
						}
					}
					if(!flag){
						return flag;
					}
				});
				if(!flag){
					return flag;
				}  
			});		
			return flag;
		};

		_this.validateUrl = function($form){
			var flag = true;
			$form.find('.zone-map > li').each(function(i){
				if(!$(this).find('.urlValue').val()){
					alert('Enter proper target url');
					flag = false;
				}
				if(!flag){
					return flag;
				}
			});
			return flag;
		};

		_this.attachValidation = function(){
			$('.DIV_TABLE form:visible .btnUpdate').on('click',function(evt){
				evt.preventDefault();
				var $form = $(evt.target).parents('form');
				if(_this.validateCoords($form)&&_this.validateUrl($form)){
					$form.submit();
				}
			});
		};

		_this.init();
	}
	return new _multibanner();	
}());

$(window).load(function(){
	Myntra.MultiBanner.getResizeZones();
});