{ config_load file="$skin_config" }
<html>
<head>
<title>{$lng.txt_site_title}</title>
{ include file="meta.tpl" }
<link rel="stylesheet" href="{$SkinDir}/skin1_admin.css" />
<script type="text/javascript">
    var http_loc = '{$http_location}'
 </script>
</head>
<body {$reading_direction_tag}>
{ include file="rectangle_top.tpl" }
{ include file="head_admin.tpl" }
<!-- main area -->
<table width="100%" cellpadding="0" cellspacing="0" align="center">
<tr>
<td class="VertMenuLeftColumn">


{if $login eq "" }
{ include file="auth.tpl" }
<br />
{else }
{assign var=user_count value=1}
{assign var=operation value=1}
{assign var=order_types value=1}
{foreach from=$roles item=user}
{if ($user.role eq "SA" || $user.role eq "MA"||$user.role eq "CS")&& $user_count==1 }
{assign var=user_count value=$user_count+1}
<br />
{/if}

<!--order search-->
{if ($user.role eq "AD" || $user.role eq "CS" || $user.role eq "OP") && $order_types eq 1}
	{include file="admin/order_type_search.tpl" }
    {assign var=order_types value=$order_types+1}
<br/>
{/if}
<!--order search-->

{if ($user.role eq "OP"|| $user.role eq "CS" ) && $operation==1}
{ include file="admin/menu_operations.tpl" }
{assign var=operation value=$operation+1}
<br />
{/if}

{if $user.role eq "CS"}
{ include file="admin/customer_care_admin.tpl" }
<br />
{/if}

{if $user.role eq "AD"}
{ include file="admin/myntra_admin.tpl" }
<br />
{ include file="admin/system_admin.tpl" }
<br />
{/if}

{if $user.role eq "FI"}
{ include file="admin/menu_reports.tpl" }
<br />
{/if}



{if $user.role eq "MA"}
{ include file="admin/menu_marketing.tpl" }
<br />
{ include file="admin/menu_reports.tpl" }
<br />
{ include file="admin/website_content.tpl" }
<br />
{ include file="admin/website_seo.tpl" }
<br/>
{/if}
{if $user.role eq "PM"}
{ include file="admin/product_mgmt.tpl" }
{/if}
{/foreach}
{if $active_modules.XAffiliate ne ''}
{ include file="admin/menu_affiliate.tpl" }
{/if}
{*include file="menu_profile.tpl" *}
<br />
{/if}
{ *include file="admin/help.tpl" *}
<br />
<img src="{$cdn_base}/skin1/images/spacer.gif" width="150" height="1" alt="" />
</td>
<td valign="top">

<!-- central space -->
{include file="location.tpl"}

{include file="dialog_message.tpl"}

{if $smarty.get.mode eq "subscribed"}
{include file="main/subscribe_confirmation.tpl"}
{elseif $auth_status eq "NotAuthorized"}
{include file="main/access_notallowed.tpl"}
 {else}
{* start of actual page content *}
{php}
 $messages = $this->get_template_vars('messages');
 foreach($messages as $message) {
  echo '<p> '.$message.'</p>';
 }
{/php}
<div style="margin-bottom:20px;">
<br />
<a href="{$http_location}/admin/banners.php"> Add a image </a>
</div>
<form  enctype="multipart/form-data" action="{$http_location}/admin/banners.php?action={if $action ne ''}{$action}{/if}{if $id ne ''}&id={$id}{/if}" method="POST">
    Choose a file to upload:
    <br />
    <br />
    <table>
    <tr>
    <td>
    Image file
    </td>
    <td>
     : <input name="image" type="file"/>
    </td>
    </tr>
    <tr>
        <td>
        Name
        </td>
        <td>
          : <input name="name" type="text"  {if isset($banner)} value="{$banner.name}"{/if}/><br/>
        </td>
    </tr>
    <tr>
        <td>
        Url
        </td>
        <td>
          : <input name="url" type="text"  {if isset($banner)} value="{$banner.url}"{/if}/><br/>
        </td>
    </tr>
     <tr>
        <td>
        Type 
        </td>
        <td>
          :  {html_options name=type options=$dropdown selected=$banner.type}
        </td>
    </tr>
    <tr>
        <td>
        Page
        </td>
        <td>
          :  <input name="page" type="text"  {if isset($banner)} value="{$banner.page}"{/if}/><br/>
        </td>
    </tr>
        <tr>
            <td>
            Display Order
            </td>
            <td>
              : <input name="display_order" type="text" {if isset($banner)} value="{$banner.display_order}"{/if}  /><br/>
            </td>
        </tr>

        <tr>
            <td>
            Enabled
            </td>
            <td>
              : <input name="enabled"  value="1" type="checkbox"  {if isset($banner) && $banner.enabled == 1} checked="true"{/if} /><br/>
            </td>
        </tr>

        <tr>
            <td>
            Alt
            </td>
            <td>
              : <input name="alt" type="text"  {if isset($banner)} value="{$banner.alt}"{/if} /><br/>
            </td>
        </tr>
         <tr>
            <td>
            Title
            </td>
            <td>
              : <input name="title" type="text" {if isset($banner)} value="{$banner.title}"{/if}/><br/>
            </td>
        </tr>

    </table> 
 


<br />
<br />
     <input type="submit" value="Upload"/>
<br />
 <br />


 <table style="margin: 0 50px 100px 50px;" border="1">
 <tr>
 <td width="20%">
 <b> Name</b>
 </td>
 <td width="10%">
 <b> Type </b>
 </td>
 <td width="10%">
 <b> Page</b>
 </td>
  <td width="10%">
 <b> Enabled </b>
 </td>
 <td width="10%">
 <b>Action</b>
 </td>
 </tr>
{section name=idx loop=$banners}
<tr> 
         <td>
    {$banners[idx].name}
         </td>
           <td>
    {$banners[idx].type}
         </td>
            <td>
    {$banners[idx].page}
         </td>
           <td>
    {$banners[idx].enabled}
         </td>
          <td>
    <a href="{$http_location}/admin/banners.php?id={$banners[idx].id}&action=edit">edit</a>
         </td>
 </tr>
{/section}
 </table>
</form>
{* end of actual page content *}
{/if}
