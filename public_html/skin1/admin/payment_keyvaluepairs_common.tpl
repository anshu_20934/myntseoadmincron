{* $Id: featured_products.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
{include file="main/include_js.tpl" src="main/popup_product.js"}



<script type="text/javascript">


{literal}
function delete_keyvaluepair(id,keyvalue){
   	var r=confirm("Are you sure you want to delete this key value pair?");
    if(r == true){
    	document.updatekeyvaluepairs.mode.value = 'delete';
        document.updatekeyvaluepairs.delete_id.value = id;
        document.updatekeyvaluepairs.delete_name.value = keyvalue;
        document.updatekeyvaluepairs.submit();
    }
}

function add_keyvaluepair(){
    if ( document.updatekeyvaluepairs.key_field_new == '' || 
    	    document.updatekeyvaluepairs.value_field_new.value == '') {
        alert('Key or Value cannot be empty');
        return;
    } 
    document.updatekeyvaluepairs.mode.value = 'add';
    document.updatekeyvaluepairs.submit();
}

function update_keyvaluepair(id){

	name = document.updatekeyvaluepairs["name_"+id].value;
	value = document.updatekeyvaluepairs["value_"+id].value;
	description =  document.updatekeyvaluepairs["desc_"+id].value;
	
    if ( name == '' || value == '') {
        alert('Key or Value cannot be empty');
        return;
    } 
    document.updatekeyvaluepairs.mode.value = 'update';
    document.updatekeyvaluepairs.update_id.value = id;
    document.updatekeyvaluepairs.update_name.value = name;
    document.updatekeyvaluepairs.update_value.value = value;
    document.updatekeyvaluepairs.update_desc.value = description;
    document.updatekeyvaluepairs.submit();
}


</script>
{/literal}

<div id="contentPanel"></div>


{capture name=dialog}
<form action="{$payment_action_php}" method="post" name="updatekeyvaluepairs">
<input type="hidden" name="mode" />
<input type="hidden" name="delete_id" />
<input type="hidden" name="delete_name" />
<input type="hidden" name="update_id" />
<input type="hidden" name="update_name" />
<input type="hidden" name="update_value" />
<input type="hidden" name="update_desc" />
<table cellpadding="3" cellspacing="1" width="100%">
<tr class="TableHead">
	<td width="20%">Key</td>
	<td width="30%">Value</td>
	<td width="30%">Description</td>
</tr>

{if $payment_keyvaluepairs}
{section name=key_num loop=$payment_keyvaluepairs}
<tr>
	
	<td align="center" style="left-padding:30px"><input id="name_{$payment_keyvaluepairs[key_num].id}" name="update_key_field[{$payment_keyvaluepairs[key_num].id}]" type="text" value="{$payment_keyvaluepairs[key_num].name}" ></td>
	<td align="center" style="left-padding:30px"><input id="value_{$payment_keyvaluepairs[key_num].id}" name="update_value_field[{$payment_keyvaluepairs[key_num].id}]" type="text" value="{$payment_keyvaluepairs[key_num].value|escape}" /></td>
	
	<td align="center" style="left-padding:30px"><input id="desc_{$payment_keyvaluepairs[key_num].id}" name="update_description_field[{$payment_keyvaluepairs[key_num].id}]" type="text" value="{$payment_keyvaluepairs[key_num].description|escape}" /></td>
	<td align="center">
				<input type="button" value="Delete" onclick="javascript: delete_keyvaluepair('{$payment_keyvaluepairs[key_num].id}','{$payment_keyvaluepairs[key_num].name}')" />
	</td>
	<td align="center">
				<input type="button" value="Update" onclick="javascript: update_keyvaluepair({$payment_keyvaluepairs[key_num].id})" />
	</td>	
</tr>
{/section}
{else}
<tr>
<td colspan="2" align="center">No Key Value Pairs added as yet</td>
</tr>
{/if}

<tr>
	<td align="center" style="left-padding:30px"><input name="key_field_new" type="text" value=""></td>
	<td align="center" style="left-padding:30px"><input name="value_field_new" type="text" value="" /></td>
	<td align="center" style="left-padding:30px"><input name="description_field_new" type="text" value="" /></td>
	<td align="center">
		<input type="button" value="Add" onclick="javascript: add_keyvaluepair()" />
	</td>
</tr>
<tr>
	<td></td>
	<td></td>
	<td></td>
	<td></td>
</tr>
</table>
</form>

{/capture}
{include file="dialog.tpl" title="Payment Key-Value Pairs" content=$smarty.capture.dialog extra='width="100%"'}




