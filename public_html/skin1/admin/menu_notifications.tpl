{* $Id: menu.tpl,v 1.68.2.2 2006/06/20 05:52:05 svowl Exp $ *}

{capture name=menu}
<li><a href="{$catalogs.admin}/notifications_create.php" class="VertMenuItems">Create Notification</a></li>
<li><a href="{$catalogs.admin}/notifications_delete.php" class="VertMenuItems">Delete Notification</a></li>
<li><a href="{$catalogs.admin}/user_group_create.php" class="VertMenuItems">Create User Group</a></li>
<li><a href="{$catalogs.admin}/user_group_delete.php" class="VertMenuItems">Delete User Group</a></li>
{/capture}
{include file="menu.tpl" dingbats="dingbats_categorie.gif" menu_title="User Personalisation" menu_content=$smarty.capture.menu }
