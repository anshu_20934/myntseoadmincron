{* $Id: menu.tpl,v 1.68.2.2 2006/06/20 05:52:05 svowl Exp $ *}
{capture name=menu}
<li><a href="{$catalogs.admin}/metatags_management.php" class="VertMenuItems">Manage Meta Tags</a></li>
<li><a href="{$catalogs.admin}/configuration.php" class="VertMenuItems">{$lng.lbl_general_settings}</a></li>
<li><a href="{$catalogs.admin}/manageBots.php" class="VertMenuItems">Manage Bots</a></li>
{/capture}
{include file="menu.tpl" dingbats="dingbats_categorie.gif" menu_title='Website Content' menu_content=$smarty.capture.menu }