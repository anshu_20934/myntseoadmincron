function CommonUtils() {}

CommonUtils.showErrorMsg = function(msg, sub_id) {
	var msgBarId = (sub_id==""?"msgBar":"msgBar_"+sub_id);
	$("#"+msgBarId).css("color", "red");
	$("#"+msgBarId).html(msg);
}

CommonUtils.clearMessage = function(sub_id) {
	var msgBarId = (sub_id==""?"msgBar":"msgBar_"+sub_id);
	$("#"+msgBarId).html('');
}

CommonUtils.showSuccessMsg = function(msg, sub_id) {
	var msgBarId = (sub_id==""?"msgBar":"msgBar_"+sub_id);
	$("#"+msgBarId).css("color", "green");
	$("#"+msgBarId).html(msg);
}

// Processing Overlay Component..............

function ProcessingOverlay() {}
ProcessingOverlay.prototype = {
    show : function(){
    	if($("#processingpopup").length == 0) {
    		this.createDiv();
    		$("#processingpopup-loader").append('<img src="'+cdn_base+'/skin1/icons/indicator.gif">');
    	}
    	$("#processingpopup").show();
    },
    
    hide : function(){
    	$("#processingpopup").hide();
    },
    
    createDiv : function() {
    	$('<div id="processingpopup" style="display:none"><div class="overlay" style="display:block;_position:absolute;_width:1300px;_height:1500px;z-index:999;">&nbsp;</div><div id="processingpopup-loader" style="position: absolute; top: 40%; left: 50%;"></div></div>').appendTo("body");
    }
}