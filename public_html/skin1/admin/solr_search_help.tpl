{* $Id: featured_products.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
{include file="main/include_js.tpl" src="main/popup_product.js"}
{include file="admin/widgets/header.tpl"}



<h1><b>Search KeyWord Help Menu</b></h1>
<break>
<h2>Left column of table has keywords which are used for solr search.<br>Right column has all indexed value of keyword in a comma seperated format. </h2>

<table cellpadding="14" cellspacing="15">
  <tr>
    <th>KeyWords</th>
    <th>Values</th>
  </tr>
  {foreach  item=row from=$ultimateArray}
    <tr class="TableSubHead">  
        <td>{$row.keyIndex}</td>
        <td colspan="5" >{$row.keyValue}</td>
   </tr>
   {/foreach}
</table>

