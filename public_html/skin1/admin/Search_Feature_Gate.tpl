{* $Id: featured_products.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
{include file="main/include_js.tpl" src="main/popup_product.js"}
{include file="admin/widgets/header.tpl"}

{capture name=dialog}
<div class="notification-message">{$message}</div>
{literal}
  <script language="Javascript">
	function addRule()
	{
		var $out = '{/literal}{$output}{literal}';
		document.frmG.submit();
	}
  </script>
{/literal}
<a name="featured" />

{$lng.txt_featured_products}


  
<h1> Enter Values For Boosted Search Specific Admin Parameters</h1>
<h2>Enter For Field Which have positions also</h2>
<table cellpadding="14" cellspacing="15" >
<tr><td> Field Value All Natural number including 0 - such sum of all field value should be less than equal to 24</td></tr>
<tr><td>Positions in a comma seperated format, such as all position for all fields should be unique and for a single field positions 
should be integer and there count same as field value and positions can only be natural numbers including 0 </td></tr>
</table>
 <form name="frmG" action="{$form_action}" method="post" enctype="multipart/form-data" >
<input type="hidden" name="mode" value="Update" />
<table cellpadding="14" cellspacing="15" >

	<tr>
		<td><h3>Field Name</td>
        <td><h3>Field Value</td>
        <td><h3>Positions</td>
        <td colspan="5" ><h3>Comments</td>
	</tr>
   
   {foreach  item=row from=$searchresult}
    <tr>  
    <input type="hidden" name="output[{$row.id}][field_name]" value="{$row.field_name}"  width = "20"> 
        <td>{$row.field_name}</td>
        <td><input type="text" name="output[{$row.id}][field_value]" value="{$row.field_value}"  width = "20"> </td>
        <td><input type="text" name="output[{$row.id}][position]" value="{$row.position}"  width = "20"> </td>
        <td colspan="5" >{$row.comments}</td>
   </tr>
   {/foreach}
	</table>
<h2>Enter For Field Which don't have positions also</h2>
<table cellpadding="14" cellspacing="15" >
<tr><td> Field Value All Natural number including 0 </td></tr>
</table>
<table cellpadding="14" cellspacing="15" >
	
	<tr>
		<td><h3>Field Name</td>
        <td><h3>Field Value</td>
        <td colspan="5" ><h3>Comments</td>
	</tr>
	{foreach  item=row from=$otherresult}
    <tr>  
    <input type="hidden" name="out[{$row.id}][field_name]" value="{$row.field_name}"  width = "20"> 
        <td>{$row.field_name}</td>
        <td><input type="text" name="out[{$row.id}][field_value]" value="{$row.field_value}"  width = "20"> </td>
        <td colspan="5" >{$row.comments}</td>
   </tr>
   {/foreach}
   
    <tr>
    <td>
    <input type="button" value="UpdateAll" name="sbtFrm" onclick="addRule()" />
    </td>
    </tr> 

</table>
</form>



{/capture}
{include file="dialog.tpl" title=" Search Promotion Fields" content=$smarty.capture.dialog extra='width="100%"'}


