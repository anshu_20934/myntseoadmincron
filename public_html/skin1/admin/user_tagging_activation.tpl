{include file="main/include_js.tpl" src="main/popup_product.js"}
{capture name=dialog}
{if $output}
	<div style="color: blue;">{$output}</div><br>
{/if}
{if $error}
	<div style="color: red; font-weight: bold;">{$error}</div><br>
{/if}

<i>It takes around 20sec to tag/activate/deactivate 20k coupons and 
90 sec to tag/activate/deactivate 1lakh coupons. <br>
Kindly have paitience <br> 
Mail to paroksh.saxena@myntra.com in case of any confusion <br><br> </i>
<form action="../../admin/user_tagging_activation.php" method="post" enctype="multipart/form-data" name="user_tagging">
<b>User Tagging</b> : <br>
Each line of csv should have user,coupon <br>
example:<br> user1,couponcode1 <br> user2,couponcode2 <br>
Upload the csv file : <input type="file" name="tagging_file" >
                      <input type="submit" name="upload1" value="upload For Tagging" >

</form>
<br> <br>
<form action="user_tagging_activation.php" method="post" enctype="multipart/form-data" name="coupon_activation">
<b>Coupon Activation</b> : <br>
Each line of csv should have user,coupon. CSV used for user tagging can also be used here <br>
example:<br> user1,couponcode1 <br> user2,couponcode2 <br>
Upload the csv file : <input type="file" name="activation_file" >
                      <input type="submit" name="upload2" value="upload For Activation" >

</form>

<br> <br>
<form action="user_tagging_activation.php" method="post" enctype="multipart/form-data" name="coupon_deactivation">
<b>Coupon DeActivation</b> : <br>
Each line of csv should have user,coupon. CSV used for user tagging can also be used here <br>
example:<br> user1,couponcode1 <br> user2,couponcode2 <br>
Upload the csv file : <input type="file" name="deactivation_file" >
                      <input type="submit" name="upload4" value="upload For DeActivation" >

</form>

<br> <br>
<b>Update LeaderBoard Table</b> : <br>
<form action="user_tagging_activation.php" method="post" enctype="multipart/form-data" name="leader_board_table_update">
<i>For Leader Board Table</i><br>
Date formate is DD-MM-YYYY-HR-MIN-SEC. if HR,MIN,SEC are not provided in input,
they are assumed to be 0,0,0 (if start date) or 23,59,59 (if end date) <br>
<table>
<tr>
<td>Leader Board Name:</td><td><input type="text" name="name" required="required" value = '{$aUserData.name}' ></td>
</tr>
<tr>
<td>Start Time:</td><td><input type="text" name="start_time" required="required" value = {$aUserData.start_time}></td><td>Ex: 21-12-2008</td>
</tr>
<tr>
<td>End Time:</td><td><input type="text" name="end_time" required="required" value = {$aUserData.end_time}></td> <td>Ex: 21-12-2008</td>
</tr>
<tr>
<td>Display Start Time:</td><td><input type="text" name="display_start_time" required="required" value = {$aUserData.display_start_time}></td><td>Ex: 21-12-2008-16-0-0</td>
</tr>
<tr>
<td>Display End Time:</td><td><input type="text" name="display_end_time" required="required" value = {$aUserData.display_end_time} ></td><td>Ex: 21-12-2008-17-59-59</td>
</tr>
<tr>
<td>Created By:</td><td><input type="text" name = "created_by" required="required" value = '{$aUserData.created_by}' ></td>
</tr>

<tr>
<td>Qualifying Statment:</td><td><input type="text" name = "qualifying_statement" value = '{$aUserData.qualifying_statement}' ></td><td>Use $ in this sentence, If left blank, this will not be shown</td>
</tr>

<tr>
<td>Post Qualifying Statment:</td><td><input type="text" name = "post_qualifying_statement" value = '{$aUserData.post_qualifying_statement}' ></td><td>This will be shown, when the user is eligible for the prize</td>
</tr>

<tr>
<td>Qualifying Amount:</td><td><input type="text" name = "qualifying_amount" value = '{$aUserData.qualifying_amount}' ></td><td>If left blank, this is 0</td>
</tr>

<tr>
<td>Refresh Duration(in seconds)</td><td><input type="text" name = "refresh_duration" value = '{$aUserData.refresh_duration}' ></td>
</tr>

<tr>
<td>Type of Leaderboard:</td>
    <td><select name="type" id="type" style="width: 114px;">
        <option value="OVERALL"> Only Overall </option>
        <option value="BOTH"> Both Men and Women</option>
        <option value="MEN"> Only Men </option>
        <option value="WOMEN"> Only Women </option>
    </select></td>
</tr>

<tr><td><i>For Leader Board Criteria table</i></td></tr>
<tr>
<td>Rule</td><td><input type="text" name = "rule" required="required" value = {$aUserData.rule} ></td><td>input can be rule/list/global. Use global for all users.</td>
</tr>
<tr>
<td>Rule String</td><td><input type="text" name = "rule_string" value = {$aUserData.rule_string}></td>
</tr>
<tr><td>For Leader Board Ticker message</td></tr>
<tr>
<td>Ticker message</td><td><input type="text" name = "ticker_msg" value = '{$aUserData.ticker_msg}' ></td>
</tr>

<tr>
<td>Leaderboard Title</td><td><input type="text" name = "title" value = '{$aUserData.title}' ></td>
</tr>

<tr>
<td>Banner Location</td><td><input type="text" name = "banner_url" value = '{$aUserData.banner_url}' ></td>
</tr>

<tr>
<td>Banner Href</td><td><input type="text" name = "banner_href" value = '{$aUserData.banner_href}' ></td>
</tr>

</table>

<!-- 
- 1st line of csv should have name,start time,end time,display start time, display end time,created by for leader_board table. <br>
 Date formate is DD-MM-YYYY-HR-MIN-SEC. if HR,MIN,SEC are not provided in input, they are assumed to be 0,0,0 (if start date) or 23,59,59 (if end date) <br>
eg: "happy hour1",24-12-2012,26-12-2012,25-12-2012-16-0-0,25-12-2012-18-0-0,"Paroksh" <br>
- 2nd line should have rule and rule string for leader_board_criteria <br>
- From 3rd line onwards, it should have logins on each line which are qualified for this happy hour <br>
eg: <br>paroksh <br>raghav <br>
 -->
<i>For Leader Board User table:</i> <br>
(each line of this file should have logins)<br>
example:<br>
abc@gmail.com<br>xyz@myntra.com<br>
Upload the csv file : <input type="file" name="entryInTable_file" >
                      <input type="submit" name="upload3" value="upload For Entry In Table" >

</form>
{/capture}
{include file="dialog.tpl" title="Upload Coupon csv" content=$smarty.capture.dialog extra='width="100%"'}
