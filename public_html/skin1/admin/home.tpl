{ config_load file="$skin_config" }
<html>
<head>
<title>{$lng.txt_site_title}</title>
{ include file="meta.tpl" }
<link rel="stylesheet" href="{$SkinDir}/skin1_admin.css" />
</head>
<body {$reading_direction_tag}>
{ include file="rectangle_top.tpl" }
{ include file="head_admin.tpl" }
<!-- main area -->
<table width="100%" cellpadding="0" cellspacing="0" align="center">
<tr>
<td class="VertMenuLeftColumn">


{if $login eq "" }
{ include file="auth.tpl" }
<br />
{else }
{assign var=user_count value=1}
{assign var=operation value=1}
{assign var=seo_types value=1}
{assign var=report_types value=1}
{assign var=eoss_types value=1}
{assign var=user_ad_pm value=1}
{assign var=ad_pm_types value=1}
{assign var=ad_pm_op_types value=1}
{assign var=ad_pm_cs_types value=1}
{assign var=cs_types value=1}
{assign var=cst_ad_types value=1}
{assign var=shopping_fest value=1}
{foreach from=$roles item=user}
	{if ($user.role eq "SA" || $user.role eq "MA"||$user.role eq "CS")&& $user_count==1 }
	{assign var=user_count value=$user_count+1}
	<br />
	{/if}
	
	<!-- Operations, Admin and PM related blocks -->
	{if ($user.role eq "CS" || $user.role eq "AD" || $user.role eq "PM" || $user.role eq "OP") && $ad_pm_op_types eq 1}
		{assign var=ad_pm_op_types value=$ad_pm_op_types+1}
		{include file="admin/order_type_search.tpl"}<!--Orders Block-->
		<br/>
		{include file="admin/menu_operations.tpl"}<!-- Operations Block -->
		<br/>
		{include file="admin/menu_returns.tpl"}<!-- Returns Block -->
		<br/>
		{include file="admin/menu_cod.tpl"}<!-- COD Block -->
	    <br/>
	    {include file="admin/menu_logistics.tpl"}<!-- Logistics Block -->
		<br/>
	{/if}
	
	<!-- Operations, Admin and PM related blocks -->
	{if  ($user.role eq "CST" || $user.role eq "AD") && $cst_ad_types eq 1}
		{assign var=cst_ad_types value=$cst_ad_types+1}
  		{include file="admin/communication_engine.tpl"} 
	{/if}
	
	<!-- Admin and PM related blocks -->
	{if ($user.role eq "AD" || $user.role eq "PM") && $ad_pm_types==1}
		{assign var=ad_pm_types value=$ad_pm_types+1}
		{include file="admin/payments.tpl"}<!-- Payment Block -->
		<br/>
		{include file="admin/payments_service.tpl"}<!-- Payment Block -->
		<br/>
		{include file="admin/product_mgmt.tpl"}<!-- Product Mangament Block -->
		<br/>
		{include file="admin/catalogue_mgmt.tpl"}<!-- Catalogue Mangament Block -->
		<br/>
		{include file="admin/myntra_admin.tpl"}<!-- Myntra Admin -->
		<br/>
		{include file="admin/system_admin.tpl"}<!-- System Admin -->
		<br/>
	{/if}
	<!-- Admin and PM related blocks -->
	
	<!-- Customer Care, Admin and  PM related blocks -->
	{if ($user.role eq "AD" || $user.role eq "PM" || $user.role eq "CS") && $ad_pm_cs_types==1}
		{assign var=ad_pm_cs_types value=$ad_pm_cs_types+1}
		{include file="admin/customer_care_admin.tpl"}<!-- Customer Care -->
		<br/>
	{/if}
	<!-- Customer Care, Admin and  PM related blocks -->
	
	<!-- Reports Block --><!-- Accessible to AD, PM, Finance, Sales -->
	{if ($user.role eq "AD" || $user.role eq "PM" || $user.role eq "FI" || $user.role eq "SA") && $report_types==1}
		{assign var=report_types value=$report_types+1}
		{include file="admin/menu_reports.tpl"}
		<br/>
	{/if}
	<!-- Reports Block -->
	
	<!-- Marketing and SEO Block --><!-- Accessible to AD, PM, Marketing, Sales -->
	{if ($user.role eq "AD" || $user.role eq "PM" || $user.role eq "MA" || $user.role eq "SA") && $seo_types==1}
		{assign var=seo_types value=$seo_types+1}
		{include file="admin/menu_marketing.tpl"}<!-- Marketing -->
		<br/>
		{include file="admin/website_seo.tpl"}<!-- SEO Admin -->
		<br/>
		{include file="admin/site_admin.tpl"}<!-- Site Admin -->
		<br/>
		{include file="admin/pageconfig/pageconfig-menu.tpl"}
		<br/>
	{/if}
	<!-- Marketing and SEO Block -->
	
	<!-- EOSS Admin --><!-- Accessible to AD, PM, Marketing, Sales -->
	{if ($user.role eq "AD" || $user.role eq "PM" || $user.role eq "SA") && $eoss_types==1}
		{assign var=eoss_types value=$eoss_types+1}
		{include file="admin/menu_eoss.tpl"}
		<br/>
	{/if}
	<!-- EOSS Admin -->
	<!--Shopping Fest Admin --><!-- Accessible to AD, PM, Marketing, Sales -->
	{if ($user.role eq "AD" || $user.role eq "PM" || $user.role eq "SA") && $shopping_fest==1}
		{assign var=shopping_fest value=$shopping_fest+1}
		{include file="admin/menu_shopping_fest.tpl"}
		<br/>
	{/if}
	<!-- Shopping Fest Admin -->
	
	<!-- Useful links for Customare Service -->
	{if ($user.role neq "CS") && $cs_types==1}
		{assign var=cs_types value=$cs_types+1}
	{/if}
	
	<!-- set var for Misc. URLs if not admin/pm list of single urls -->
	{if ($user.role eq "AD" || $user.role eq "PM" || $user.role eq "PA" || $user.role eq "CS" || $user.role eq "CT" || $user.role eq "IT")}
		{assign var=user_ad_pm value=$user_ad_pm+1}
	{/if}	

{/foreach}

{include file="admin/menu_notifications.tpl"}
<br/>

<!-- Misc. URLs if not admin/pm list of single urls --> 
{if $user_ad_pm==1}
	{include file="admin/misc_urls.tpl}
{/if}

<!-- Misc. URLs if cs list of single urls -->
{if $cs_types==1}
	{include file="admin/misc_cc_urls.tpl"}
{/if}
{if $active_modules.XAffiliate ne ''}
{ include file="admin/menu_affiliate.tpl" }
{/if}
{*include file="menu_profile.tpl" *}
<br />
{/if}


{ *include file="admin/help.tpl" *}
<br />
<img src="{$cdn_base}/skin1/images/spacer.gif" width="150" height="1" alt="" />
</td>
<td valign="top">

<!-- central space -->
{include file="location.tpl"}

{include file="dialog_message.tpl"}

{if $smarty.get.mode eq "subscribed"}
{include file="main/subscribe_confirmation.tpl"}

{elseif $auth_status eq "NotAuthorized"}
{include file="main/access_notallowed.tpl"}

{elseif $main eq "import_export"}
{include file="main/import_export.tpl"}

{elseif $main eq "froogle_export"}
{include file="modules/Froogle/froogle.tpl"}

{elseif $main eq "snapshots"}
{include file="admin/main/snapshots.tpl"}

{elseif $main eq "titles"}
{include file="admin/main/titles.tpl"}

{elseif $main eq "taxes"}
{include file="admin/main/taxes.tpl"}

{elseif $main eq "wishlists"}
{include file="modules/Wishlist/wishlists.tpl"}

{elseif $main eq "wishlist"}
{include file="modules/Wishlist/display_wishlist.tpl"}

{elseif $main eq "tax_edit"}
{include file="admin/main/tax_edit.tpl"}

{elseif $main eq "ups_registration"}
{include file="modules/UPS_OnLine_Tools/ups.tpl"}

{elseif $main eq "order_edit"}
{include file="modules/Advanced_Order_Management/order_edit.tpl"}

{elseif $main eq "manufacturers"}
{include file="modules/Manufacturers/manufacturers.tpl"}

{elseif $main eq "user_profile"}
{include file="main/admin_register.tpl"}

{elseif $main eq "stop_list"}
{include file="modules/Stop_List/stop_list.tpl"}

{elseif $main eq "returns"}
{include file="modules/RMA/returns.tpl"}

{elseif $main eq "benchmark"}
{include file="modules/Benchmark/bench.tpl"}

{elseif $main eq "slg"}
{include file="modules/Shipping_Label_Generator/generator.tpl"}

{elseif $main eq "register"}
{include file="admin/main/register.tpl"}

{elseif $main eq "product_links"}
{include file="admin/main/product_links.tpl"}

{elseif $main eq "general_info"}
{include file="admin/main/general.tpl"}

{elseif $main eq "tools"}
{include file="admin/main/tools.tpl"}

{elseif $main eq "change_mpassword"}
{include file="admin/main/change_mpassword.tpl"}

{elseif $main eq "page_edit"}
{include file="admin/main/page_edit.tpl"}

{elseif $main eq "shipping_options"}
{include file="admin/main/shipping_options.tpl"}

{elseif $main eq "shopCartView"}
{include file="admin/main/shopCartView.tpl"}

{elseif $main eq "product_style_to_category"}
{include file="admin/main/product_style_to_category.tpl"}

{elseif $main eq "subscriptions"}
{include file="modules/Subscriptions/subscriptions_admin.tpl"}

{elseif $main eq "languages"}
{include file="admin/main/languages.tpl"}

{elseif $main eq "memberships"}
{include file="admin/main/memberships.tpl"}

{elseif $main eq "card_types"}
{include file="admin/main/card_types.tpl"}

{elseif $main eq "banner_info"}
{include file="main/banner_info.tpl"}

{elseif $main eq "referred_sales"}
{include file="main/referred_sales.tpl"}

{elseif $main eq "affiliates"}
{include file="main/affiliates.tpl"}

{elseif $main eq "partner_top_performers"}
{include file="admin/main/partner_top_performers.tpl"}

{elseif $main eq "partner_adv_stats"}
{include file="admin/main/partner_adv_stats.tpl"}

{elseif $main eq "partner_adv_campaigns"}
{include file="admin/main/partner_adv_campaigns.tpl"}

{elseif $main eq "partner_level_commissions"}
{include file="admin/main/partner_level_commissions.tpl"}

{elseif $main eq "partner_orders"}
{include file="admin/main/partner_orders.tpl"}

{elseif $main eq "partner_report"}
{include file="admin/main/partner_report.tpl"}

{elseif $main eq "partner_banners"}
{include file="admin/main/partner_banners.tpl"}

{elseif $main eq "partner_plans"}
{include file="admin/main/partner_plans.tpl"}

{elseif $main eq "partner_plans_edit"}
{include file="admin/main/partner_plans_edit.tpl"}

{elseif $main eq "commissions"}
{include file="admin/main/partner_commissions.tpl"}

{elseif $main eq "payment_upload"}
{include file="admin/main/payment_upload.tpl"}

{elseif $smarty.get.mode eq "unsubscribed"}
{include file="main/unsubscribe_confirmation.tpl"}

{elseif $main eq "search"}
{include file="main/search_result.tpl" products=$products}

{elseif $main eq "inventory_search"}
{include file="main/inventory_search.tpl" products=$products}

{elseif $main eq "reports_list"}
{include file="main/reports_list.tpl" }

{elseif $main eq "homepage"}
{include file="admin/main/admin_homepage.tpl"}

{elseif $main eq "modules"}
{include file="admin/main/modules.tpl"}

{elseif $main eq "payment_methods"}
{include file="admin/main/payment_methods.tpl"}

{elseif $main eq "cc_processing"}
{include file="admin/main/cc_processing_main.tpl" processing_module=$processing_module }

{elseif $main eq "statistics"}
{include file="admin/main/statistics.tpl"}

{elseif $main eq "configuration"}
{include file="admin/main/configuration.tpl"}

{elseif $main eq "shipping"}
{include file="admin/main/shipping.tpl"}

{elseif $main eq "db_backup"}
{include file="admin/main/db_backup.tpl"}

{elseif $main eq "states_edit"}
{include file="admin/main/states.tpl"}

{elseif $main eq "countries_edit"}
{include file="admin/main/countries.tpl"}

{elseif $main eq "cancel-lost-orders"}
{include file="admin/main/cancel-lost-orders.tpl"}

{elseif $main eq "users"}
{include file="admin/main/users.tpl"}

{elseif $main eq "featured_products"}
{include file="admin/main/featured_products.tpl"}

{elseif $main eq "category_modify"}
{include file="admin/main/category_modify.tpl"}

{elseif $main eq "category_products"}
{include file="admin/main/category_products.tpl"}

{elseif $main eq "category_delete_confirmation"}
{include file="admin/main/category_del_confirmation.tpl"}

{elseif $main eq "user_delete_confirmation"}
{include file="admin/main/user_delete_confirmation.tpl"}

{elseif $main eq "user_add"}
{include file="$tpldir/main/register.tpl"}

{elseif $main eq "product"}
{include file="main/product.tpl" product=$product}

{elseif $main eq "top_info"}
{include file="admin/main/main.tpl"}

{elseif $main eq "promo"}
{include file="admin/main/promotions.tpl"}

{elseif $main eq "home"}
{include file="admin/main/welcome.tpl"}

{elseif $main eq "ratings_edit"}
{include file="admin/main/ratings_edit.tpl"}

{elseif $main eq "images_location"}
{include file="admin/main/images_location.tpl"}

{elseif $main eq "speed_bar"}
{include file="admin/main/speed_bar.tpl"}

{elseif $main eq "secure_login_form"}
{include file="admin/main/secure_login_form.tpl"}

{elseif $main eq "change_password"}
{include file="main/change_password.tpl"}

{elseif $main eq "test_pgp"}
{include file="admin/main/test_pgp.tpl"}

{elseif $main eq "product_configurator"}
{include file="modules/Product_Configurator/pconf_common.tpl"}

{* new templates added from here *}

{elseif $main eq "logs"}
{include file="admin/main/logs.tpl"}

{elseif $main eq "admin_product_type"}
{include file="admin/main/admin_product_type.tpl"}

{elseif $main eq "admin_seo_search"}
{include file="admin/main/admin_seo_search.tpl"}

{elseif $main eq "admin_customization_technique"}
{include file="admin/main/admin_customization_technique.tpl"}

{elseif $main eq "ptype_action"}
{include file="admin/main/product_type_action.tpl"}

{elseif $main eq "admin_corporate_request"}
{include file="admin/main/admin_corporate_request.tpl"}

{elseif $main eq "admin_corporate_req_confirm"}
{include file="admin/main/admin_request_confirm.tpl"}

<!-- When admin clicks on the hyperlink of the request a page similar to managereq is open -->
{elseif $main eq "admin_corporate_manage_request"} 
{include file="admin/main/admin_corporate_manage_request.tpl"}

{elseif $main eq "corporaterequest_search"} 
{include file="admin/main/corporaterequest_search.tpl"}

{elseif $main eq "prd_style_action"}
{include file="admin/main/productstyle_action.tpl"}

{elseif $main eq "prd_style_new"}
{include file="admin/main/productstyle_action_new.tpl"}

{elseif $main eq "styleedit"}
{include file="admin/main/styleedit.tpl"}

{elseif $main eq "admin_product_option_addons"}

{include file="admin/main/addons.tpl"}

{elseif $main eq "upload_bulk_order"}
{include file="admin/main/updatebulkorder.tpl"}

{elseif $main eq "upload_bulk_returns"}
{include file="admin/main/updatebulkreturns.tpl"}

{elseif $main eq "bluedart_servicability"}
{include file="admin/main/bluedart_servicability.tpl"}

{elseif $main eq "courier_serviceability"}
{include file="admin/main/courier_serviceability.tpl"}

{elseif $main eq "courier_status_info"}
{include file="admin/main/courier_status_info.tpl"}

{elseif $main eq "cod_mobile_verification_code"}
{include file="admin/main/cod_mobile_verification_code.tpl"}

{elseif $main eq "cod_bulk_change_on_hold_orders"}
{include file="admin/main/cod_bulk_change_on_hold_orders.tpl"}

{*elseif $main eq "mark_shipped_orders_as_delivered"*}
{*include file="admin/main/mark_shipped_orders_as_delivered.tpl"*}

{elseif $main eq "update_delivered_orders"}
{include file="admin/main/update_delivered_orders.tpl"}

{elseif $main eq "cod_reconcile"}
{include file="admin/main/cod_reconcile.tpl"}

{elseif $main eq "eoss_discount_rules"}
{include file="admin/main/eoss_discount_rules.tpl"}

{elseif $main eq "discount_engine"}
{include file="$cntr_tpl"}

{elseif $main eq "rewards_employee"}
{include file="admin/main/rewards_employee.tpl"}

{elseif $main eq "cod_order_report"}
{include file="admin/main/cod_order_report.tpl"}


{elseif $main eq "upload_payment_completed_orders"}
{include file="admin/main/upload_payment_completed_orders.tpl"}

{elseif $main eq "upload_payment_completed_giftorders"}
{include file="admin/main/upload_payment_completed_giftorders.tpl"}

{elseif $main eq "cust_tech_action"}
{include file="admin/main/customizationtechnique_action.tpl"}

{elseif $main eq "prd_style_custumization"}
{include file="admin/main/prd_style_custimization.tpl"}

{elseif $main eq "admin_product_group"}
{include file="admin/main/admin_product_group.tpl"}

{elseif $main eq "promotion"}
{include file="admin/main/report_promotion.tpl"}

{elseif $main eq "discount"}
{include file="admin/main/report_discount.tpl"}

{elseif $main eq "segby_city"}
{include file="admin/main/report_seg_bycity.tpl"}

{elseif $main eq "segby_profile"}
{include file="admin/main/report_seg_byprofile.tpl"}

{elseif $main eq "user_report"}
{include file="admin/main/report_users.tpl"}

{elseif $main eq "report_orders"}
{include file="admin/main/report_orders.tpl"}

{elseif $main eq "report_sales"}
{include file="admin/main/report_sales.tpl"}

{elseif $main eq "changeimage"}
{include file="admin/main/changeUploadImage.tpl"}

{elseif $main eq "left_search_links"}
{include file="admin/main/left_search_links.tpl"}

{elseif $main eq "helpaction"}
{include file="admin/main/adminhelp_action.tpl"}

{elseif $main eq "event_action"}
{include file="admin/main/specialevent_action.tpl"}

{elseif $main eq "view_print_design"}
{include file="admin/main/view_print_design.tpl"}

{elseif $main eq "print_order_label"}
{include file="admin/main/print_order_label.tpl"}

{elseif $main eq "print_address_label"}
{include file="admin/main/print_address_label.tpl"}

{elseif $main eq "offlineOrders"}
{include file="admin/main/offline_orders.tpl"}

<!-- added for tag management -->
{elseif $main eq "metatags"}
{include file="admin/main/metatags_management.tpl"}

<!-- added for referral program -->
{elseif $main eq "referral"}
{include file="admin/main/admin_referral.tpl"}

{elseif $main eq "addreferral"}
{include file="admin/main/admin_addreferral.tpl"}

{elseif $main eq "style_inventory"}
{include file="admin/main/admin_style_inventory.tpl"}

{elseif $main eq "inventorylog"}
{include file="admin/main/inventorylog.tpl"}

{elseif $main eq "admin_consumables"}
{include file="admin/main/admin_consumables.tpl"}

{elseif $main eq "affiliatepayment"}
{include file="admin/main/affiliatepayment.tpl"}

{elseif $main eq "affiliate_comm_update"}
{include file="admin/main/affiliate_comm_update.tpl"}

{elseif $main eq "referralpayment"}
{include file="admin/main/referralpayment.tpl"}

{elseif $main eq "myntra_logs"}
{include file="admin/main/myntralogs.tpl"}

{elseif $main eq "edit_myntra_log_level"}
{include file="admin/main/editmyntraloglevel.tpl"}

{elseif $main eq "affiliateusers"}
{include file="admin/main/affiliateusers.tpl"}

{elseif $main eq "affiliate_profile"}
{include file="admin/main/affiliate_profile.tpl"}

{elseif $main eq "productdetails"}
{include file="admin/main/productdetails.tpl" }

{elseif $main eq "sku_picklist_report"}
{include file="admin/main/sku_picklist_report.tpl" }

{elseif $main eq "sku_edit_list"}
{include file="admin/main/sku_edit_list.tpl" }

{elseif $main eq "item_assignment"}
{include file="admin/main/item_assignment.tpl" }

{elseif $main eq "new_item_assignment"}
{include file="admin/main/new_item_assignment.tpl" }

{elseif $main eq "new_return_requests"}
{include file="admin/main/new_return_requests.tpl" }

{elseif $main eq "return_requests"}
{include file="admin/main/return_requests.tpl" }

{elseif $main eq "return_request_details_cc"}
{include file="admin/main/return_request_details_cc.tpl" }

{elseif $main eq "old_returns_tracking"}
{include file="admin/main/old_returns_tracking.tpl" }

{elseif $main eq "old_undelivered_order_tracking"}
{include file="admin/main/old_undelivered_order_tracking.tpl" }

{elseif $main eq "old_returnedorders_tracking"}
{include file="admin/main/old_returnedorders_tracking.tpl" }

{elseif $main eq "old_rto_tracking"}
{include file="admin/main/old_rto_tracking.tpl" }

{elseif $main eq "old_rt_tracking"}
{include file="admin/main/old_rt_tracking.tpl" }

{elseif $main eq "old_ud_tracking"}
{include file="admin/main/old_ud_tracking.tpl" }

{elseif $main eq "shipment_tracking_reports"}
{include file="admin/main/shipment_tracking_reports.tpl" }

{elseif $main eq "upload_uds_to_queue"}
{include file="admin/main/upload_uds_to_queue.tpl"}

{elseif $main eq "upload_udlss_to_queue"}
{include file="admin/main/upload_udlss_to_queue.tpl"}

{elseif $main eq "bulk_rto_queueing"}
{include file="admin/main/bulk_rto_queueing.tpl"}

{elseif $main eq "upload_rtos_to_queue"}
{include file="admin/main/upload_rtos_to_queue.tpl" }

{elseif $main eq "upload_rtos_preq"}
{include file="admin/main/upload_rtos_preq.tpl" }

{elseif $main eq "bulk_rto_cancellation"}
{include file="admin/main/bulk_rto_cancellation.tpl" }

{elseif $main eq "old_rto_rt_sla"}
{include file="admin/main/old_rto_rt_sla.tpl" }

{elseif $main eq "return_request_details"}
{include file="admin/main/return_request_details.tpl" }

{elseif $main eq "ready_to_pickup_report"}
{include file="admin/main/ready_to_pickup_report.tpl" }

{elseif $main eq "ready_to_reship"}
{include file="admin/main/ready_to_reship_report.tpl" }

{elseif $main eq "create_sku"}
{include file="admin/main/create_sku.tpl" }

{elseif $main eq "processing_groups"}
{include file="admin/main/processing_groups.tpl" }

{elseif $main eq "editors"}
{include file="admin/main/editors.tpl" }

{elseif $main eq "downloader_dashboard"}
{include file="admin/main/downloader_dashboard.tpl" }

{elseif $main eq "downloader_schedules"}
{include file="admin/main/downloader_schedules.tpl" }

{elseif $main eq "service_codes"}
{include file="admin/main/service_codes.tpl" }

{elseif $main eq 'operations_dashboard'}
{include file='admin/main/operations_dashboard.tpl' }

{elseif $main eq "item_search"}
{include file="admin/main/item_search.tpl" }

{elseif $main eq "orders_search_new"}
{include file="admin/main/orders_search_new.tpl"}

{elseif $main eq "operation_location"}
{include file="admin/main/operation_location.tpl" }

{elseif $main eq "item_assignee"}
{include file="admin/main/item_assignee.tpl" }

{elseif $main eq "assignee_worksheet"}
{include file="admin/main/assignee_worksheet.tpl" }

{elseif $main eq "qa_worksheet"}
{include file="admin/main/qa_worksheet.tpl" }

{elseif $main eq "location_worksheet"}
{include file="admin/main/location_worksheet.tpl" }

{elseif $main eq "assigned_items_report"}
{include file="admin/main/assigned_items_report.tpl"}

{elseif $main eq "all_queued_orders"}
{include file="admin/main/all_queued_orders.tpl"}

{elseif $main eq "upload_qa_report"}
{include file="admin/main/upload_qa_report.tpl"}

{elseif $main eq "ready_to_ship_report"}
{include file="admin/main/ready_to_ship_report.tpl"}

{elseif $main eq "shipped_orders"}
{include file="admin/main/shipped_orders.tpl"}

{elseif $main eq "orders_ship_report"}
{include file="admin/main/orders_ship_report.tpl"}

{elseif $main eq "late_order_report"}
{include file="admin/main/late_order_report.tpl"}

{elseif $main eq "manage_resources"}
{include file="admin/main/manage_resources.tpl" }

{elseif $main eq "manage_locations"}
{include file="admin/main/manage_locations.tpl" }

{elseif $main eq "manage_order_sources"}
{include file="admin/main/manage_order_sources.tpl" }

{elseif $main eq "shop_master_management"}
{include file="admin/main/shop_master_management.tpl" }

{elseif $main eq "approve_transactions"}
{include file="admin/main/approvetransactions.tpl" }
{elseif $main eq "approve_deposits"}
{include file="admin/main/approvedeposits.tpl" }

{elseif $main eq "manageBots"}
{include file="admin/main/manageBots.tpl" }

{elseif $main eq "pos_employees"}
{include file="admin/main/pos_employee_details.tpl" }

{elseif $main eq "support_tickets_mgmt"}
{include file="admin/main/supportticketmgmt.tpl" }

{elseif $main eq "support_ticket_view"}
{include file="admin/main/viewsupportticket.tpl" }

{elseif $main eq "corporate_testimonials"}
{include file="admin/main/corporate_testimonials.tpl" }

{elseif $main eq "item_reassignment"}
{include file="admin/main/item_reassignment.tpl" }

{elseif $main eq "uploadmailer"}
{include file="admin/main/upload_mailer.tpl" }

{elseif $main eq "uploadbanner"}
{include file="admin/main/upload_banner.tpl" }

{elseif $main eq "distress_orders"}
{include file="admin/main/distress_orders.tpl" }

{elseif $main eq "outlet_orders"}
{include file="admin/main/outlet_orders.tpl" }

{elseif $main eq "style_group_map"}
{include file="admin/main/arrangegroupstylemap.tpl" }

{elseif $main eq "arrangebestsellingproductstyle"}
{include file="admin/main/arrangebestsellingproductstyle.tpl" }

{elseif $main eq "template"}
{include file="admin/main/template.tpl" }

{elseif $main eq "add_search_keywords"}
{include file="admin/main/add_search_keywords.tpl" }

{elseif $main eq "style_template"}
{include file="admin/main/style_template.tpl" }

{elseif $main eq "style_template_action"}
{include file="admin/main/style_template_action.tpl" }

{elseif $main eq "revenue_report"}
{include file="admin/main/revenue_report.tpl" }

{elseif $main eq "imint_report"}
{include file="admin/main/imint_report.tpl" }

{elseif $main eq "reports_link_list"}
{include file="reports_link_list.tpl" }

{elseif $main eq "search_mobile_coupon"}
{include file="admin/main/search_mobile_coupon.tpl" }

{elseif $main eq "catalog_classification"}
{include file="admin/main/catalog_classification.tpl" }

{elseif $main eq "attribute_values"}
{include file="admin/main/attribute_values.tpl" }

{elseif $main eq "filtergroups"}
{include file="admin/main/filtergroups.tpl" }

{elseif $main eq "addfilters"}
{include file="admin/main/addfilters.tpl" }

{elseif $main eq "sports_home_category_admin"}
{include file="admin/main/sports_home_category_details.tpl" }

{elseif $main eq "popular_widget"}
{include file="admin/widgets/popular_widget.tpl" }
{elseif $main eq "topnavigation_widget"}
{include file="admin/widgets/topnavigation_widget.tpl" }
{elseif $main eq "topnavigation_widget_v2"}
{include file="admin/widgets/topnavigation_widget_v2.tpl" }
{elseif $main eq "topnavigation_widget_v3"}
{include file="admin/widgets/topnavigation_widget_v3.tpl" }
{elseif $main eq "most_popular"}
{include file="admin/widgets/most_popular.tpl" }
{elseif $main eq "homepagelists_widget"}
{include file="admin/widgets/homepagelists_widget.tpl" }
{elseif $main eq "size_unification_admin_new"}
{include file="admin/size_unification_admin_new.tpl" }
{elseif $main eq "vtr_enable_disable_styles"}
{include file="admin/vtr_enable_disable_styles.tpl" }
{elseif $main eq "solr_search"}
{include file="admin/solr_search.tpl" }
{elseif $main eq "solr_search_help"}
{include file="admin/solr_search_help.tpl" }
{elseif $main eq "search_synonym_admin"}
{include file="admin/Search_Synonym_Admin.tpl" }
{elseif $main eq "search_gate"}
{include file="admin/Search_Feature_Gate.tpl" }
{** elseif $main eq "size_unification_filter_order_admin" **}
{** include file="admin/size_unification_filter_order_admin.tpl" **}
{elseif $main eq "banners_widget"}
{include file="admin/widgets/banners_widget.tpl" }
{elseif $main eq "banners_widget_new"}
{include file="admin/widgets/banners_widget_new.tpl" }
{elseif $main eq "set_solr_fields_priority"}
{include file="admin/main/set_solr_fields_priority.tpl" }
{elseif $main eq "keyvaluepairs_widget"}
{include file="admin/widgets/keyvaluepairs_widget.tpl" }
{elseif $main eq "keyvaluepairs_common"}
{include file="admin/keyvaluepairs_common.tpl" }
{elseif $main eq "payment_keyvaluepairs_common"}
{include file="admin/payment_keyvaluepairs_common.tpl" }
{elseif $main eq "pl_report"}
{include file="admin/payment_logs.tpl"}
{elseif $main eq "abtesting_tests"}
{include file="admin/abtesting_tests.tpl" }
{elseif $main eq "attribute_groupings"}
{include file="admin/main/attribute_grouping.tpl" }
{elseif $main eq "sem_promo"}
{include file="admin/sem/semPromo.tpl" }
{elseif $main eq "sem_routing"}
{include file="admin/sem/semRouting.tpl" }
{elseif $main eq "set_sorting_order"}
{include file="admin/main/set_sorting_order.tpl" }
{elseif $main eq "crosslink_keyword_manager"}
{include file="admin/seo/crosslinkKeywordManager.tpl" }
{elseif $main eq "cashbackaccounts"}
{include file="admin/main/cashbackaccounts.tpl" }
{elseif $main eq "cashbackaccountsnew"}
{include file="admin/main/cashbackaccountsnew.tpl" }
{elseif $main eq "loyaltyPoints"}
{include file="admin/main/loyaltyPointsAccount.tpl" }
{elseif $main eq "loyaltyPointsOrders"}
{include file="admin/main/loyaltyPointsOrders.tpl" }
{elseif $main eq "loyaltyPointsTier"}
{include file="admin/main/loyaltyPointsTier.tpl" }
{elseif $main eq "cancelorders"}
{include file="admin/main/cancelordersbulk.tpl" }
{elseif $main eq "display_categories"}
{include file="admin/main/display_categories.tpl" }
{elseif $main eq "add_display_categories"}
{include file="admin/main/add_display_categories.tpl" }
{elseif $main eq "cancellation_history"}
{include file="admin/main/cancellation_history.tpl" }
{elseif $main eq "orders_comments"}
{include file="admin/main/orders_comments.tpl" }
{elseif $main eq "manage_sr_component"}
{include file="admin/ServiceRequest/manageSRComponent.tpl" }
{elseif $main eq "manage_sr"}
{include file="admin/ServiceRequest/manageServiceRequest.tpl" }
{elseif $main eq "sr_report"}
{include file="admin/ServiceRequest/serviceRequestReport.tpl" }
{elseif $main eq "bulkSRUpdate"}
{include file="admin/ServiceRequest/bulkSRUpdate.tpl"}
{elseif $main eq "telesales_view"}
{include file="admin/telesales_view.tpl" }
{elseif $main eq "telesales_view_new"}
{include file="admin/telesales_view_new.tpl" }
{elseif $main eq "oosordersreport"}
{include file="admin/main/oosordersreport.tpl" }
{elseif $main eq "style_bulk_upload"}
{include file="admin/main/style_bulk_upload.tpl" }
{elseif $main eq "qa_actions"}
{include file="admin/main/qa_actions.tpl" }
{elseif $main eq "process_item_exit"}
{include file="admin/main/process_item_exit.tpl" }
{elseif $main eq "return_item"}
{include file="admin/main/return_item.tpl" }
{elseif $main eq "exchange_item"}
{include file="admin/main/exchange_item.tpl" }
{elseif $main eq "returns_sla_dashboard"}
{include file="admin/main/returns_sla_dashboard.tpl" }
{elseif $main eq "returns_sla_editor"}
{include file="admin/main/returns_sla_editor.tpl" }
{elseif $main eq "recievables_analysis"}
{include file="admin/recievables/recievables_analysis.tpl"}
{elseif $main eq "cod_recievables_report"}
{include file="admin/recievables/cod_recievables_report.tpl"}
{elseif $main eq "cod_recievables_adjustments"}
{include file="admin/recievables/cod_recievables_adjustments.tpl"}
{elseif $main eq "cod_collections_report"}
{include file="admin/recievables/cod_collections_report.tpl"}
{elseif $main eq "netbanking_bank_list"}
{include file="admin/netbanking/bank_list.tpl" }
{elseif $main eq "pmt_netbanking_bank_list"}
{include file="admin/netbanking_service/pmt_bank_list.tpl" }
{elseif $main eq "netbanking_bank_gateway"}
{include file="admin/netbanking/add_bank_gateway.tpl" }
{elseif $main eq "pmt_netbanking_bank_gateway"}
{include file="admin/netbanking_service/pmt_add_bank_gateway.tpl" }
{elseif $main eq "netbanking_add_gateway"}
{include file="admin/netbanking/add_gateway.tpl" }
{elseif $main eq "netbanking_service_add_gateway"}
{include file="admin/netbanking_service/pmt_add_gateway.tpl" }
{elseif $main eq "netbanking_gateway_bank"}
{include file="admin/netbanking/add_gateway_bank.tpl" }
{elseif $main eq "netbanking_service_gateway_bank"}
{include file="admin/netbanking_service/pmt_add_gateway_bank.tpl" }
{elseif $main eq "netbanking_gateway_mapping"}
{include file="admin/netbanking/activate_gateway.tpl" }
{elseif $main eq "pmt_netbanking_gateway_mapping"}
{include file="admin/netbanking_service/pmt_activate_gateway.tpl" }
{elseif $main eq "update_core_items"}
{include file="admin/main/update_core_items.tpl" }
{elseif $main eq "update_core_items1"}
{include file="admin/main/update_core_items1.tpl" }
{elseif $main eq "admin_roles_access"}
{include file="admin/main/admin_roles_access.tpl" }
{elseif $main eq "update_attributes"}
{include file="admin/main/update_attributes.tpl" }
{elseif $main eq "myntra_landing_page"}
{include file="admin/main/myntra_landing_page.tpl" }
{elseif $main eq "returns_reports"}
{include file="admin/main/returns_reports.tpl" }
{elseif $main eq "cod_oh_orders"}
{include file="admin/main/cod_on_hold_orders.tpl" }
{elseif $main eq "xcache_common"}
{include file="admin/xcache_common.tpl" }
{elseif $main eq "solr_indexing"}
{include file="admin/main/solr_indexing.tpl" }
{elseif $main eq "photoshoot_issue_rules"}
{include file="admin/main/photoshoot_issue_rules.tpl" }
{elseif $main eq "reassign_warehouse"}
{include file="admin/main/reassign_warehouse.tpl" }
{elseif $main eq "price_override"}
{include file="main/price_override.tpl"}
{elseif $main eq "rts_batch_info"}
{include file="admin/main/rts_batch_info.tpl" }
{elseif $main eq "search_styles"}
{include file="admin/main/search_styles.tpl" }
{** PORTAL-2811: Size Unification Admin UI**}
{elseif $main eq "size_scales_unif"}
{include file="admin/catalog/sizeunification/size_scales_unif.tpl" }
{elseif $main eq "size_mapping_unif"}
{include file="admin/catalog/sizeunification/size_mapping_unif.tpl" }
{elseif $main eq "size_measurement_unif"}
{include file="admin/catalog/sizeunification/size_measurement_unif.tpl" }

{elseif $main eq "payment_reconciliation"}
{include file="admin/payment_reconciliation.tpl" }
{elseif $main eq "receive_returns"}
{include file="admin/main/receive_returns.tpl" }
{elseif $main eq "contactus_controller"}
{include file="admin/contactus/contactUsViewController.tpl" }
{elseif $main eq "style_bulk_update"}
{include file="admin/main/styleBulkUpdate.tpl" }
{elseif $main eq "style_content_update"}
{include file="admin/main/styleContentUpdate.tpl" }
{elseif $main eq "jpegmini_config"}
{include file="admin/main/jpegmini_config.tpl" }
{elseif $main eq "mobileUAWhitelist"}
{include file="admin/main/mobileUAWhitelist.tpl" }
<!-- storyboard page configuration -->
{elseif $main eq "pageconfig"}
{include file="admin/pageconfig/pageconfig.tpl" }
<!-- end -->
{elseif $main eq "lpcontroller"}
{include file="admin/landingpage/lpcontroller.tpl" }
{elseif $main eq "sectioncontroller"}
{include file="admin/landingpage/sectioncontroller.tpl" }

{elseif $main eq "payment_reconciliation"}
{include file="admin/payment_reconciliation.tpl" }
{elseif $main eq "style_bulk_update"}
{include file="admin/main/styleBulkUpdate.tpl" }
{elseif $main eq "style_content_update"}
{include file="admin/main/styleContentUpdate.tpl" }
{elseif $main eq "cod_onhold_settings" }
{include file="admin/main/cod_onhold_settings.tpl" }
{elseif $main eq "bulk_orders_comments"}
{include file="admin/main/bulk_orders_comments.tpl" }
{elseif $main eq "style_specific_attribute"}
{include file="admin/main/styleSpecificAttribute.tpl" }
{elseif $main eq "email_editor"}
{include file="admin/main/email_editor.tpl"}
{elseif $main eq "cod_oh_bulk_update" }
{include file="admin/main/cod_oh_bulk_update.tpl" }
{elseif $main eq "email_sender"}
{include file="admin/main/email_sender.tpl"}
{elseif $main eq "cod_onhold_settings" }
{include file="admin/main/cod_onhold_settings.tpl" }
{elseif $main eq "styleUploadImages" }
{include file="admin/main/catalog/styleUploadImages.tpl" }
{elseif $main eq "manage_leaderboard"}
{include file="admin/main/manage_leaderboard.tpl"}
{elseif $main eq "message"}
{include file="admin/main/messages.tpl"}
{elseif $main eq "shopping_fest_coupon"}
{include file="admin/main/shopping_fest/coupon_config.tpl"}
{elseif $main eq "shopping_fest_leader"}
{include file="admin/main/shopping_fest/leader_config.tpl"}
{elseif $main eq "shopping_fest_general"}
{include file="admin/main/shopping_fest/general_config.tpl"}
{elseif $main eq "user_tagging_activation" }
{include file="admin/user_tagging_activation.tpl" }
{elseif $main eq "manual_style_solr_indexing" }
{include file="admin/manual_style_solr_indexing.tpl" }
{elseif $main eq "timer" }
{include file="admin/catalog/timer.tpl" }
{elseif $main eq "timer_meta" }
{include file="admin/catalog/timerMeta.tpl" }
{elseif $main eq "new_signup_control" }
{include file="admin/new_signup_control.tpl" }
{elseif $main eq "myntcash_IGCC_repots" }
{include file="admin/myntcash_IGCC_repots.tpl" }
{elseif $main eq "myntcash_IGCC_repots_agent" }
{include file="admin/myntcash_IGCC_repots_agent.tpl" }
{elseif $main eq "notifications_create" }
{include file="admin/main/notifications_create.tpl" }
{elseif $main eq "notifications_delete" }
{include file="admin/main/notifications_delete.tpl" }
{elseif $main eq "user_group_create" }
{include file="admin/main/user_group_create.tpl" }
{elseif $main eq "imageUploader" }
{include file="admin/main/catalog/imageUploader.tpl" }
{elseif $main eq "user_group_delete" }
{include file="admin/main/user_group_delete.tpl" }
{elseif $main eq "bulk_return_decline"}
{include file="admin/main/bulk_return_decline.tpl" }
{elseif $main eq "bulk_move_items_cr"}
{include file="admin/main/bulk_move_items_cr.tpl" }
{elseif $main eq "bulk_upload_store_returns"}
{include file="admin/main/bulk_upload_store_returns.tpl" }
{elseif $main eq "deprecated_redirect"}
{include file="admin/main/deprecatedPage.tpl" }
{elseif $main eq "deprecatedENP_redirect"}
{include file="admin/main/deprecatedENPPage.tpl" }
{else}
{include file="common_templates.tpl"}
{/if}

<!-- /central space -->
&nbsp;
</td>
<td>
<img src="{$cdn_base}/skin1/images/spacer.gif" width="20" height="1" alt="" />
</td>
</tr>
</table>
{ include file="rectangle_bottom.tpl" }
{literal}
<script type="text/javascript">
$(document).ready(function(){
	// $(".VertMenuBox table").hide();
});

var menuEl=$('.VertMenuTitle table');
$.each(menuEl, function(){
    $(this).click(function(){
        $(this).parent().parent().parent().find(".VertMenuBox table").slideToggle();
    });
});
</script>
{/literal}

</body>
</html>
