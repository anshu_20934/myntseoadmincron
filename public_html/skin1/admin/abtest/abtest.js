//Setting Panels
var idCounter=-1;
function createNewAbtestObject(){
	var obj={
			name:"",
			source_type:"",
			variations:new Object(),
			seg_algo:"",
			filters:"",
			id:idCounter--,
			ga_slot:'',
			omni_slot:''
	};
	
	var variant=createNewAbtestVariantObject();
	variant.name='control';
	obj.variations[variant.name]= variant;
	variant=createNewAbtestVariantObject();
	obj.variations[variant.name]= variant;
	return obj;
}

function createNewAbtestVariantObject(){

	var varId=idCounter--;
	var obj = {
			id:varId,
			name:"variant "+(-varId),
			p_prob:0,
			inline_html:"",
			final_variant:0,
			config:"",
			algo_config:""
			
	};
	return obj;
}

var RandomAlgoSettingCell = Ext.extend(Ext.Panel, { 
	constructor : function(config) {
		this.variant=config.variant;
		this.disableAll=config.disableAll==true;
		this.items = this.generateItems();

		config = Ext.apply({
			header : false,
			border : false,
			labelPad : 10,
			bodyStyle : 'padding:0px 5px 0px 5px',
			autoScroll : true,
			header : false,
			labelWidth : 100,
			validateOnBlur : true,
			fileUpload : false,
			timeout : 100,
			width : "100",
			layout:'form'
		}, config);

		RandomAlgoSettingCell.superclass.constructor.call(this, config);
	},

	generateItems : function() {
		var items = [];
		var i = 0;
		var totalProb = 0;
		var myObject=this;
		items[i++] = {
					xtype : 'numberfield',
					name : 'variantprob',
					allowBlank : false,
					fieldLabel:"Probability",
					maxValue : 100,
					minValue : 0,
					width : 30,
					value : this.variant.p_prob,
					enableKeyEvents: true,
					disabled:this.disableAll,
					resetVal:function (){
						this.value = myObject.variant.p_prob;
					}
		};
		return items;
	},resetVal:function (){
			for(var i=0;i<this.items.length;i++){
				this.items[i].resetVal();
			}
	}
});


var NewUserOldUserRandomAlgoSettingCell = Ext.extend(Ext.Panel, { 
	constructor : function(config) {
		this.variant=config.variant;
		this.disableAll=config.disableAll==true;
		this.items = this.generateItems();

		config = Ext.apply({
			header : false,
			border : false,
			labelPad : 10,
			bodyStyle : 'padding:0px 5px 0px 5px',
			autoScroll : true,
			header : false,
			labelWidth : 100,
			validateOnBlur : true,
			fileUpload : false,
			timeout : 100,
			width : "100",
			layout:'form'
		}, config);

		NewUserOldUserRandomAlgoSettingCell.superclass.constructor.call(this, config);
	},

	generateItems : function() {
		var items = [];
		var i = 0;
		var totalProb = 0;
		var myObject=this;
		
		items[i++] = {
					xtype : 'numberfield',
					name : 'newuserprob',
					allowBlank : false,
					fieldLabel:"New User Probability",
					maxValue : 100,
					minValue : 0,
					width : 30,
					value : this.variant.algo_config['new'],
					enableKeyEvents: true,
					disabled:this.disableAll
		};items[i++] = {
				xtype : 'numberfield',
				name : 'olduserprob',
				allowBlank : false,
				fieldLabel:"Old User Probability",
				maxValue : 100,
				minValue : 0,
				width : 30,
				value :  this.variant.algo_config['old'],
				enableKeyEvents: true,
				disabled:this.disableAll
	};
		return items;
	},resetVal:function (){
			for(var i=0;i<this.items.length;i++){
				this.items[i].resetVal();
			}
	}
});

var InlineHtmlSettingCell = Ext.extend(Ext.Panel, {
	constructor : function(config) {
		this.variant=config.variant;
		this.disableAll=config.disableAll==true;
		this.items = this.generateItems();
		// -----------------//

		config = Ext.apply({
			header : false,
			border : false,
			labelPad : 10,
			autoScroll : true,
			header : false,
			labelWidth : 100,
			validateOnBlur : true,
			fileUpload : false,
			timeout : 100,
			width : "100%",
			bodyStyle : 'padding:0px 10px 0px 10px',
			layout:'form'

		}, config);

		InlineHtmlSettingCell.superclass.constructor.call(
				this, config);
	},

	generateItems : function() {
		var items = [];
		var i = 0;
		var totalProb = 0;
		var myObject=this;
		this.oldValue=myObject.variant.inner_html;
		items[i++] = {
				xtype : 'button',
				cls : "orange-bg",
				fieldLabel:"Html",
				text : 'Change Html',
				width : 100,
				handler:function(){
					var dialog= new EditHtmltDialog({variant:myObject.variant,disableAll:myObject.disableAll});
					dialog.show();
				}
		
		};
		return items;
	},
	resetVal:function (){
		var myObject=this;
						myObject.variant.inner_html=this.oldValue;
				}
});

var InlineJSCSSSettingCell = Ext.extend(Ext.Panel, {
	constructor : function(config) {
		this.abtest=config.abtest;
		this.variant=config.variant;
		
		this.items = this.generateItems();
		// -----------------//

		config = Ext.apply({
			header : false,
			border : false,
			labelPad : 10,
			autoScroll : true,
			header : false,
			labelWidth : 100,
			validateOnBlur : true,
			fileUpload : false,
			timeout : 100,
			width : "100%",
			bodyStyle : 'padding:0px 10px 0px 10px',
			layout:'form'

		}, config);

		InlineJSCSSSettingCell.superclass.constructor.call(
				this, config);
	},

	generateItems : function() {
		var items = [];
		var i = 0;
		var totalProb = 0;
		var myObject=this;
		this.oldValue=myObject.variant.inner_html;
		items[i++] = {
				xtype : 'button',
				cls : "orange-bg",
				fieldLabel:"Html",
				text : 'Change Html',
				width : 100,
				handler:function(){
					var dialog= new EditHtmlJSCSSDialog({abtest:myObject.abtest,variant:myObject.variant,disableAll:myObject.disableAll});
					dialog.show();
				}
		
		};
		return items;
	},
	resetVal:function (){
		var myObject=this;
						myObject.variant.inner_html=this.oldValue;
				}
});

var ConfigSettingCell = Ext.extend(Ext.Panel, {
	constructor : function(config) {
		this.abtest=config.abtest;
		this.variant=config.variant;
		
		this.items = this.generateItems();
		// -----------------//

		config = Ext.apply({
			header : false,
			border : false,
			labelPad : 10,
			autoScroll : true,
			header : false,
			labelWidth : 100,
			validateOnBlur : true,
			fileUpload : false,
			timeout : 100,
			width : "100%",
			bodyStyle : 'padding:0px 10px 0px 10px',
			layout:'form'

		}, config);

		ConfigSettingCell.superclass.constructor.call(
				this, config);
	},

	generateItems : function() {
		var items = [];
		var i = 0;
		var totalProb = 0;
		var myObject=this;
		this.oldValue=myObject.variant.inner_html;
		items[i++] = {
				xtype : 'button',
				cls : "orange-bg",
				fieldLabel:"Config",
				text : 'Change Config',
				width : 100,
				handler:function(){
					var dialog= new EditConfigJsonDialog({abtest:myObject.abtest,variant:myObject.variant,disableAll:myObject.disableAll});
					dialog.show();
				}
		
		};
		return items;
	},
	resetVal:function (){
		var myObject=this;
						myObject.variant.inner_html=this.oldValue;
				}
});

var panelConfig =  new Array();
panelConfig['abtest\\algos\\impl\\RandomSegmentationAlgo']=function(config){
	return new RandomAlgoSettingCell(config);
};
panelConfig['abtest\\algos\\impl\\NewUserOldUserRandomSegmentationAlgo']=function(config){
	return new NewUserOldUserRandomAlgoSettingCell(config);
};
panelConfig['inline_html']=function(config){
	return new InlineHtmlSettingCell(config);
};
panelConfig['inline_js_css_html']=function(config){
	return new InlineJSCSSSettingCell(config);
};
panelConfig['tpl']=function(config){
	return new Ext.Panel(config);
};
panelConfig['config']=function(config){
	return new ConfigSettingCell(config);
};
//***********

function saveOrAddABTest(dialog,abtest,comment1,comment2){
	Ext.Ajax.request({
		url : 'abtesting_gate.php?mode=saveupdatenew',
		loadMask: {msg: 'Saving ABTest: '+abtest.name},
		params : {"abtest" : Ext.util.JSON.encode(abtest),
			"comment" : comment1+"<br>Extra: "+comment2
		},
	    success : function(response, action) {
	    	if(response.responseText==null){
	    		Ext.MessageBox.alert('Error', 'Connection Problem !');
	    		return;
	    	}
			var jsonObj = Ext.util.JSON.decode(response.responseText);
			
			if (jsonObj.results == 'success') {
				Ext.MessageBox.alert('Success', jsonObj.msg);
				window.location.reload();
			} else {
				Ext.MessageBox.alert('Error', jsonObj.msg);
			}
		}
	});
}


//Wizard start
var currentPage=0;
var wizardFlowArray = [ function(config) {
			return new WizardDialogBasic(config);
		}, function(config) {
			return new WizardDialogSettings(config);
		},function(config) {
			return new WizardDialogComments(config);
		} ];


function wizardHandler(dialog,action,abtest,comments){
	if(action=='launch'){
		currentPage=0;
		var config ={title:"Create A/B Test",abtest:abtest};
		if ( typeof abtest == 'undefined' || abtest==null ){
			abtest = createNewAbtestObject();
			config.abtest = abtest;
		}else{
			config.title = 'Update '+abtest.name;
		}
		var wizard = wizardFlowArray[currentPage](config);

		wizard.show();
	}else if(action=='next'){
		currentPage++;
		var wizard = wizardFlowArray[currentPage]({abtest:abtest});
		dialog.close();
		wizard.show();
	}else if(action=='back'){
		currentPage--;
		var wizard = wizardFlowArray[currentPage]({abtest:abtest});
		dialog.close();
		wizard.show();
	}else if(action=='finish'){
		saveOrAddABTest(dialog,abtest,comments[0].comment1,comments[0].comment2)
	}
}


var BasicTestDetailForm = Ext.extend(Ext.form.FormPanel, { 
	constructor : function(config) {
		this.abtest=config.abtest;
	
		this.items = this.generateItems();

		config = Ext.apply({
			header : false,
			border : false,
			frame:true,
			labelPad : 10,
			bodyStyle : 'padding:0px 5px 0px 5px',
			autoScroll : true,
			header : false,
			labelWidth : 100,
			validateOnBlur : true,
			fileUpload : false,
			timeout : 100,
			width : "100%",
			layout:'form'
		}, config);

		BasicTestDetailForm.superclass.constructor.call(this, config);
	},

	generateItems : function() {
		var myObject = this;

		var segAlgoStore = new Ext.data.JsonStore({
			id : "segAlgoStore",
			fields : [ 'algo_name','algo_class' ],
			url : "/admin/abtesting_gate.php?mode=get_seg_algos",
			totalProperty : "totalCount",
			remoteSort : false,
			root : "searchResults"
		});
		
		var filtersStore = new Ext.data.JsonStore({
			id : "segFilterStore",
			fields : [ 'filter_name','filter_class' ],
			url : "/admin/abtesting_gate.php?mode=get_available_filters",
			totalProperty : "totalCount",
			remoteSort : false,
			root : "searchResults"
		});
		
		var items = [ {
				xtype : "textfield",
				allowBlank : false,
				name : "test_name",
				width : "200",
				fieldLabel : "Test Name *",
				msgTarget: 'side',
				value:myObject.abtest.name
			}, {
				xtype : 'combo',
				fieldLabel : "Segmentation Algo *",
				name : "algo_class",
				emptyText : 'Select Segmentation Algo...',
				store : segAlgoStore,
				displayField : 'algo_name',
				valueField : 'algo_class',
				forceSelection : true,
				editable : false,
				allowBlank : false,
				value:myObject.abtest.seg_algo,
				width : "150",
				triggerAction : 'all',
				msgTarget: 'side'
			}, {
				xtype : 'combo',
				name : 'test_type',
				fieldLabel : "Test Type *",
				emptyText : 'Select Test Type...',
				store : [['tpl','Tpl'],['inline_html','Inline Html'],['inline_js_css_html','Inline html Js CSS'],['config','Config']],
				forceSelection : true,
				editable : false,
				allowBlank : false,
				value:myObject.abtest.source_type,
				width : "150",
				triggerAction : 'all',
				mode: 'local',
				msgTarget: 'side'

			},
			{
				allowBlank:true,
				msgTarget: 'title',
				name:'test_filters',
				xtype:'superboxselect',
				fieldLabel: 'Filters',
				resizable: true,
				anchor:'100%',
				store: filtersStore,
				displayField: 'filter_name',
				displayFieldTpl: '{filter_name}',
				valueField: 'filter_class',
				navigateItemsWithTab: false,
				triggerAction : 'all',
				value:myObject.abtest.filters
			} , {
				xtype : "numberfield",
				allowBlank : true,
				id : "ga_slot",
				maxValue : 10,
				minValue : 0,
				width : 30,
				fieldLabel : "GA Slot",
				value:myObject.abtest.ga_slot
			}, {
				xtype : "numberfield",
				allowBlank : true,
				id : "omni_slot",
				maxValue : 75,
				minValue : 0,
				width : 30,
				fieldLabel : "Omniture Slot",
				value:myObject.abtest.omni_slot
			}];
			
		return items;
	},validate:function(){
		var values=this.getForm().getValues();
		if(values.test_name.indexOf(' ')>0){
			Ext.MessageBox.alert('Error', "Test Name cannot have spaces");
			return false;
		}
		return true;
	},resetVal:function (){
						myObject.variant.inner_html=this.oldValue;
	}
});


var BasicTestSettingsForm = Ext.extend(Ext.form.FormPanel, { 
	constructor : function(config) {
		this.abtest=config.abtest;
		this.disableAll=config.disableAll==true||this.abtest.completed=='1';
		this.config = config;
		this.items = this.generateItems();
		config = Ext.apply({
			header : false,
			border : false,
			frame:true,
			labelPad : 10,
			bodyStyle : 'padding:0px 5px 0px 5px',
			autoScroll : true,
			header : false,
			labelWidth : 100,
			validateOnBlur : true,
			fileUpload : false,
			timeout : 100,
			layout:'form'
		}, config);

		BasicTestSettingsForm.superclass.constructor.call(this, config);
	},

	generateItems : function() {
		var disableName = this.config.disableNameChange==true;
		var myObject = this;
		var items =[];
		var i=0;
	    for(var key in myObject.abtest['variations'] ){
	    	if(typeof myObject.abtest['variations'][key] == 'function'){
	    		continue;
	    	}
	    	var p1 = panelConfig[this.abtest.seg_algo]({abtest:myObject.abtest,variant:myObject.abtest['variations'][key],disableAll:myObject.disableAll});
	    	var p2=  panelConfig[this.abtest.source_type]({abtest:myObject.abtest,variant:myObject.abtest['variations'][key],disableAll:myObject.disableAll});
	    	p1.columnWidth=.3;
	    	p2.columnWidth=.3;
	    	items[i++] = {
	    		xtype:'panel',
	    		layout:'column',
		    	frame:true,
		    	items:[ {
		    		xtype:'panel',
		    		layout:'form',
		    		bodyStyle : 'padding:0px 10px 0px 10px',
		    		 columnWidth:.4,
		    		items:[{
	                    xtype:'textfield',
	                    fieldLabel:"variant name",
	                    width : "100",
	                    value: myObject.abtest['variations'][key].name,
	                    name: 'variantname',
	                    labelStyle: 'font-weight:bold;',
	                    allowBlank:false,
	                    disabled:disableName
	                 },{
		                    xtype:'textfield',
		                    hidden:true,
		                    width:0,
		                    value: myObject.abtest['variations'][key].id,
		                    name: 'variantid'
		               },
	    		       {
		                    xtype:'textfield',
		                    width:0,
		                    value: myObject.abtest['variations'][key].name,
		                    name: 'oldvariantname',
		                    hidden:true
		                 }]
		    	},p1, p2 ]
		    };
	    }
	    
		
		
		return items;
	},
	addNewRow:function(){
		var myObject = this;
		var variant=createNewAbtestVariantObject();
		var p1 = panelConfig[this.abtest.seg_algo]({abtest:myObject.abtest,variant:variant});
    	var p2=  panelConfig[this.abtest.source_type]({abtest:myObject.abtest,variant:variant});
    	this.abtest['variations'][variant.name]=variant;
    	p1.columnWidth=.3;
    	p2.columnWidth=.3;
    	var newRow = {
    		xtype:'panel',
    		layout:'column',
	    	frame:true,
	    	items:[ {
	    		xtype:'panel',
	    		layout:'form',
	    		bodyStyle : 'padding:0px 10px 0px 10px',
	    		 columnWidth:.4,
	    		items:[{
                    xtype:'textfield',
                    fieldLabel:"variant name",
                    width : "100",
                    value: variant.name,
                    name: 'variantname',
                    labelStyle: 'font-weight:bold;',
                    allowBlank:false
                 },{
	                    xtype:'textfield',
	                    hidden:true,
	                    value: variant.id,
	                    name: 'variantid'
	               },{
	                    xtype:'textfield',
	                    value: variant.name,
	                    name: 'oldvariantname',
	                    hidden:true
	                 }]
	    	},p1, p2 ]
	    };
		this.add(newRow);
		this.doLayout();
		this.doLayout();
	},	validate : function(){
		var myObject = this;
		var values = this.getForm().getValues();
		if(this.config.disableNameChange==true){
			values.variantname=values.oldvariantname;
		}
		var foundControl=false;
		var nameInvalid = false;
		var totalProbSum=0;
		var temp = new Object();
		for(var i=0;i<values.variantid.length;i++){
			if(values.variantname[i].indexOf(' ')>0){
				nameInvalid=true;
			}
			if(values.variantname[i]=='control'){
				foundControl=true;
			}
			if(temp[values.variantname[i]]==1){
				Ext.MessageBox.alert('Error', "Duplicate "+values.variantname[i]+" variant name!");
				return false;
			}
			temp[values.variantname[i]]=1;
		}
		if(nameInvalid){
			Ext.MessageBox.alert('Error', "Variant name cannot have spaces");
			return false;
		}
		if(!foundControl){
			Ext.MessageBox.alert('Error', "Atleast one variant should be control");
			return false;
		}
		
		if(myObject.abtest.seg_algo=='abtest\\algos\\impl\\RandomSegmentationAlgo'){
		  for(var i=0;i<values.variantid.length;i++){
			  totalProbSum+=Number(values.variantprob[i]);
		  }
		  
		  if(totalProbSum!=100 ){
			  Ext.MessageBox.alert('Error', "Sum of probability for all variants should be exacty 100");
			  return false;
		  }
		}

		if(myObject.abtest.seg_algo=='abtest\\algos\\impl\\NewUserOldUserRandomSegmentationAlgo'){
		  
		  totalProbSum=0;
		  for(var i=0;i<values.newuserprob.length;i++){
			  totalProbSum+=Number(values.newuserprob[i]);
		  }
		  
		  if(totalProbSum!=100 ){
			  Ext.MessageBox.alert('Error', "Sum of New User probability for all variants should be exacty 100");
			  return false;
		  }
		  totalProbSum=0;
		  for(var i=0;i<values.olduserprob.length;i++){
			  totalProbSum+=Number(values.olduserprob[i]);
		  }
		  
		  if(totalProbSum!=100 ){
			  Ext.MessageBox.alert('Error', "Sum of Old User probability for all variants should be exacty 100");
			  return false;
		  }
		  
		}
		return true;
	}
});


var TestUpdateCommentForm = Ext.extend(Ext.form.FormPanel, { 
	constructor : function(config) {
		this.abtest=config.abtest;
	
		this.items = this.generateItems();

		config = Ext.apply({
			header : false,
			border : false,
			frame:true,
			labelPad : 10,
			bodyStyle : 'padding:0px 5px 0px 5px',
			autoScroll : true,
			header : false,
			labelWidth : 100,
			validateOnBlur : true,
			fileUpload : false,
			timeout : 100,
			width : "100%",
			layout:'form'
		}, config);

		TestUpdateCommentForm.superclass.constructor.call(this, config);
	},

	generateItems : function() {
		var myObject = this;

		var items = [{
    		xtype:"textarea",
    		allowBlank : false,
    		name:"comment1",
    		width:"95%",
    		height:"40%",
    		fieldLabel:"Whats the rational behind the change? *"},
    		{
    		xtype:"textarea",
    		width:"95%",
    		name:"comment2",
    		height:"40%",
    		allowBlank : true,
    		fieldLabel:"Additional Comments"}];
			
		return items;
	},validate:function(){
		return true;
	}
});


var WizardDialogBasic = Ext.extend(Ext.Window, {
	constructor : function(config) {
		this.abtest=config.abtest;
		this.items = this.generateItems();
		var myObject = this;
		config = Ext.apply({
			header : true,
			border : true,
			labelPad : 10,
			autoScroll : true,
			header : false,
			modal : true,
			frame : true,
			labelWidth : 100,
			validateOnBlur : true,
			timeout : 100,
			width : "500",
			autoHeight:true,
			defaults : {
				selectOnFocus : true,
				msgTarget : 'side'
			},buttons : [ {
				text : "Cancel",
				handler : function() {
					myObject.close();
				}
			},{
				text : "Next",
				handler : function() {
					if(myObject.basicInfoForm.getForm().isValid()){
						if(myObject.validateAndUpdate(true)){
							wizardHandler(myObject,'next',myObject.abtest);
						}
					}	
					
				}
			}
			]

		}, config);

		WizardDialogBasic.superclass.constructor.call(this, config);
	},
	generateItems : function() {
		var myObject = this;

		this.basicInfoForm = new BasicTestDetailForm({abtest:myObject.abtest});

		var items = [];

		var i = 0;
		items[i++] = this.basicInfoForm;

		return items;
	},
	validateAndUpdate:function(validate){
		var myObject=this;
		var form = myObject.basicInfoForm.getForm();
		if(validate==true && !myObject.basicInfoForm.validate()){
			return false;
		}
		myObject.abtest.name=form.findField("test_name").getValue();
		myObject.abtest.source_type=form.findField("test_type").getValue();
		myObject.abtest.seg_algo=form.findField("algo_class").getValue();
		myObject.abtest.filters=form.findField("test_filters").getValue();
		myObject.abtest.ga_slot=form.findField("ga_slot").getValue();
		myObject.abtest.omni_slot=form.findField("omni_slot").getValue();
		return true;
	}
}); 


var WizardDialogSettings = Ext.extend(Ext.Window, {
	constructor : function(config) {
		this.abtest=config.abtest;
		var myObject = this;
		this.items = this.generateItems();
		config = Ext.apply({
			title : " Create New A/B Test",
			header : true,
			border : true,
			labelPad : 10,
			autoScroll : true,
			header : false,
			modal : true,
			frame : true,
			labelWidth : 100,
			validateOnBlur : true,
			timeout : 100,
			width : "820",
			autoHeight:true,
			defaults : {
				selectOnFocus : true,
				msgTarget : 'side'
			},
			buttons:[ {
				text : "Cancel",
				handler : function() {
					myObject.close();
				}
			},{
				text : "Back",
				handler : function() {
					if(myObject.validateAndUpdate(false)){
						wizardHandler(myObject,'back',myObject.abtest);
					}	
					
				}
			},{
				text : "Next",
				handler : function() {
					if(myObject.testSettingsPanel.getForm().isValid()){
						if(myObject.validateAndUpdate(true)){
							wizardHandler(myObject,'next',myObject.abtest);
						}
					}	
					
				}
			}
			]

		}, config);

		WizardDialogSettings.superclass.constructor.call(this, config);
	},
	generateItems : function() {
		var myObject = this;
		   
		this.testSettingsPanel =	new BasicTestSettingsForm({abtest:myObject.abtest})

		var items = [];
		var i=0;
		items[i++] = {
					xtype : 'button',
					text : 'Click to Add Variant',
					width : 100,
					handler:function(){
						myObject.testSettingsPanel.addNewRow();
					}
		    };


		items[i++] = this.testSettingsPanel;

		return items;
	},
	validateAndUpdate:function(validate){
		var myObject=this;
		var valid = true;
		if(validate){
			if(!myObject.testSettingsPanel.validate())
				return false;
		}
		
		var values = myObject.testSettingsPanel.getForm().getValues();
		
		for(var i=0;i<values.variantid.length;i++){
			if(values.oldvariantname[i]!=values.variantname[i]){
				myObject.abtest.variations[values.variantname[i]]=myObject.abtest.variations[values.oldvariantname[i]];
				delete(myObject.abtest.variations[values.oldvariantname[i]]);
				values.oldvariantname[i]=values.variantname[i];
			}
			myObject.abtest.variations[values.oldvariantname[i]].name=values.variantname[i];
			
			if(myObject.abtest.seg_algo=='abtest\\algos\\impl\\RandomSegmentationAlgo'){
				  for(var j=0;j<values.variantid.length;j++){
					myObject.abtest.variations[values.oldvariantname[i]].p_prob=values.variantprob[j];
				  }
			}

			if(myObject.abtest.seg_algo=='abtest\\algos\\impl\\NewUserOldUserRandomSegmentationAlgo'){
				if(myObject.abtest.variations[values.oldvariantname[i]].algo_config=="" ||myObject.abtest.variations[values.oldvariantname[i]].algo_config ==null)
					myObject.abtest.variations[values.oldvariantname[i]].algo_config=new Object();
				myObject.abtest.variations[values.oldvariantname[i]].algo_config['new']=values.newuserprob[i]; 
				myObject.abtest.variations[values.oldvariantname[i]].algo_config.old=values.olduserprob[i];
		  
			}
		}
		return true;
	}
}); 


var WizardDialogComments = Ext.extend(Ext.Window, {
	constructor : function(config) {
		this.abtest=config.abtest;
		this.items = this.generateItems();
		var myObject= this;
		config = Ext.apply({
			header : true,
			border : true,
			labelPad : 10,
			autoScroll : true,
			header : false,
			modal : true,
			frame : true,
			labelWidth : 100,
			validateOnBlur : true,
			timeout : 100,
			width : "500",
			autoHeight:true,
			defaults : {
				selectOnFocus : true,
				msgTarget : 'side'
			},buttons:[ {
				text : "Cancel",
				handler : function() {
					myObject.close();
				}
			},{
				text : "Back",
				handler : function() {
					if(myObject.validateAndUpdate(false)){
						wizardHandler(myObject,'back',myObject.abtest);
					}
					
				}
			},{
				text : "Finish",
				handler : function() {
					if(myObject.commentPanel.getForm().isValid()){
						if(myObject.validateAndUpdate(true)){
							wizardHandler(myObject,'finish',myObject.abtest,[myObject.commentPanel.getForm().getValues()]);
						}
					}	
					
				}
			}
			]

		}, config);

		WizardDialogComments.superclass.constructor.call(this, config);
	},
	generateItems : function() {
		var myObject = this;
		
		this.commentPanel = new TestUpdateCommentForm({title:"Comments "}); 
		var items = [];
		var i=0;
		items[i++] = this.commentPanel;

		return items;
	},
	validateAndUpdate:function(validate){
		return true;
	}
}); 




// Wizard End

var EditHtmltDialog = Ext.extend(Ext.Window, {
	constructor : function(config) {
		this.disableAll=config.disableAll==true;
		this.variant=config.variant;
		this.items = this.generateItems();
		var myObject=this;
		config = Ext.apply({
			//waitMsgTarget: true,
			title:" Inline HTML",
			header : true,
			border : true,
			// bodyStyle : 'padding:10px 10px 5px 10px',
			labelPad : 10,
			autoScroll : true,
			header : false,
			modal:true,
			validateOnBlur : true,
			fileUpload : false,
			frame:true,
			timeout : 100,
			width : "500",
			height: "200",
			//y:100,
			defaults : {
				selectOnFocus: true,
				msgTarget: 'side'
			},
			buttons:[{
            	text : "Cancel",
            	handler : function() {
            		myObject.close();
            	}
            }, {
            	text : "Done",
            	disabled:this.disableAll,
            	handler : function() {
            		
            		 if(!myObject.myForm.getForm().isValid()){
            				Ext.MessageBox.alert('Error', "Invalid Inline Html");
            		}else{
            			myObject.variant.inline_html=myObject.myForm.getForm().findField("inline_html").getValue();
            			myObject.close();
            		}	
                	
            		
            	}
            }]
			
		}, config);

		EditHtmltDialog.superclass.constructor.call(this, config);
	},
	generateItems : function() {
		var myObject=this;
		var items = [];
    	this.myForm=new Ext.form.FormPanel({
	    	frame:true,
	    	items:[{
	    		xtype:"textarea",
	    		name:"inline_html",
	    		allowBlank : false,
	    		fieldLabel:"Html or Text *",
	    		width:"95%",
	    		height:"95%",
	    		value:myObject.variant.inline_html,
	    		disabled:myObject.disableAll
	    	}
	    		]
	    });
		
	    var i=0;
	    items[i++] = this.myForm;
	    
	   
		return items;
	}
}); 

var EditConfigJsonDialog = Ext.extend(Ext.Window, {
	constructor : function(config) {
		this.disableAll=config.disableAll==true;
		this.variant=config.variant;
		this.items = this.generateItems();
		this.abtest = config.abtest;
		var myObject=this;
		config = Ext.apply({
			//waitMsgTarget: true,
			title:" Config Json",
			header : true,
			border : true,
			// bodyStyle : 'padding:10px 10px 5px 10px',
			labelPad : 10,
			autoScroll : true,
			header : false,
			modal:true,
			validateOnBlur : true,
			frame:true,
			timeout : 100,
			width : "500",
			height: "300",
			//y:100,
			defaults : {
				selectOnFocus: true,
				msgTarget: 'side'
			},
			buttons:[{
            	text : "Cancel",
            	handler : function() {
            		myObject.close();
            	}
            }, {
            	text : "Done",
            	disabled:this.disableAll,
            	handler : function() {
            		
            		 if(!myObject.myForm.getForm().isValid()){
            				Ext.MessageBox.alert('Error', "Invalid Json Text");
			  }else{
			    
			      try {
				  var jsonData = Ext.util.JSON.decode(myObject.myForm.getForm().findField("json_text").getValue());
				  myObject.variant.config=jsonData;
				  myObject.close();
				}
				catch (err) {
					Ext.MessageBox.alert('ERROR', 'Invalid Json Text ');
				}
       
            		}	
                	
            		
            	}
            }]
			
		}, config);

		EditConfigJsonDialog.superclass.constructor.call(this, config);
	},
	generateItems : function() {
		var myObject=this;
		var items = [];
		
		var json_text=Ext.encode(myObject.variant.config);
	  this.myForm=new Ext.form.FormPanel({
	    	frame:true,
	    	fileUpload:true,
	    	items:[{
	    		xtype:"textarea",
	    		name:"json_text",
	    		allowBlank : false,
	    		fieldLabel:"JSON Text *",
	    		width:"95%",
	    		height:"150",
	    		value:json_text,
	    		disabled:myObject.disableAll
	    	}
	    	]
	    });
		
	    var i=0;
	    items[i++] = this.myForm;
	    
	   
		return items;
	}
}); 

var EditHtmlJSCSSDialog = Ext.extend(Ext.Window, {
	constructor : function(config) {
		this.disableAll=config.disableAll==true;
		this.variant=config.variant;
		this.items = this.generateItems();
		this.abtest = config.abtest;
		var myObject=this;
		config = Ext.apply({
			//waitMsgTarget: true,
			title:" Inline HTML JS CSS",
			header : true,
			border : true,
			// bodyStyle : 'padding:10px 10px 5px 10px',
			labelPad : 10,
			autoScroll : true,
			header : false,
			modal:true,
			validateOnBlur : true,
			frame:true,
			timeout : 100,
			width : "500",
			height: "300",
			//y:100,
			defaults : {
				selectOnFocus: true,
				msgTarget: 'side'
			},
			buttons:[{
            	text : "Cancel",
            	handler : function() {
            		myObject.close();
            	}
            }, {
            	text : "Done",
            	disabled:this.disableAll,
            	handler : function() {
            		
            		 if(!myObject.myForm.getForm().isValid()){
            				Ext.MessageBox.alert('Error', "Invalid Inline Html");
            		}else{
            			myObject.variant.inline_html=myObject.myForm.getForm().findField("inline_html").getValue();
            		//	
            			form_action=1;
            			
            			myObject.myForm.getForm().submit({
            				                url: '/admin/abtesting_gate.php?mode=uploadjscss&abtestname='+myObject.abtest.name,
            					                waitMsg: 'Sending Files',
            					                success: function(form,action){
            					                	if(Ext.util.Format.trim(action.result.uFileJs)!="")
            					                		myObject.variant.jsFile = action.result.uFileJs;
            					                	if(Ext.util.Format.trim(action.result.uFileCss)!="")
            					                		myObject.variant.cssFile = action.result.uFileCss;
            					                	myObject.close();
            	                                },
            	                                failure:function(form,action){
            	                                	Ext.MessageBox.alert('Error', action.result.msg);
            	                                }
            					            });
            			
            		}	
                	
            		
            	}
            }]
			
		}, config);

		EditHtmlJSCSSDialog.superclass.constructor.call(this, config);
	},
	generateItems : function() {
		var myObject=this;
		var items = [];
		this.uFileJs={
		        xtype: 'fileuploadfield',
		        emptyText: 'Select a Js file to upload',
		        fieldLabel: 'JS',
		        buttonText: 'Select a Js File',
		        id:"ufileJs",
		        value:myObject.variant.jsFile,
		        disabled:this.disableAll
		};
		this.uFileCss={
		        xtype: 'fileuploadfield',
		        emptyText: 'Select a CSS file to upload',
		        fieldLabel: 'CSS',
		        buttonText: 'Select a Css File',
		        id:"ufileCss",
		        value:myObject.variant.cssFile,
		        disabled:this.disableAll
	    };
    	this.myForm=new Ext.form.FormPanel({
	    	frame:true,
	    	fileUpload:true,
	    	items:[{
	    		xtype:"textarea",
	    		name:"inline_html",
	    		allowBlank : false,
	    		fieldLabel:"Html or Text *",
	    		width:"95%",
	    		height:"150",
	    		value:myObject.variant.inline_html,
	    		disabled:myObject.disableAll
	    	},this.uFileJs,
	    	this.uFileCss
	    	]
	    });
		
	    var i=0;
	    items[i++] = this.myForm;
	    
	   
		return items;
	}
}); 


var MABCompleteCommmentDialog = Ext.extend(Ext.Window, {
	constructor : function(config) {
		this.abtest=config.abtest;
		this.items = this.generateItems();
		var myObject=this;
		config = Ext.apply({
			// waitMsgTarget: true,
			title:this.abtest.name+" Complete Comments",
			header : true,
			border : true,
			bodyStyle : 'padding:10px 10px 5px 10px',
			labelPad : 10,
			autoScroll : true,
			frame:true,
			modal:true,
			labelWidth : 100,
			width : "500",
			height: "250",
			//y:100,
			
			buttons:[{
            	text : "Cancel",
            	handler : function() {
            		myObject.close();
            	}
            }, {
            	text : "Done",
            	handler : function() {
            	   		if(Ext.getCmp(myObject.abtest.name+"-completeform").getForm().isValid()){
            			Ext.Ajax.request({
            				url : 'abtesting_gate.php?mode=completetest',
            				loadMask: {msg: 'Completing ABTest: '+myObject.abtest.name},
            				params : {"abtest" : myObject.abtest.name,
            					"comment" : Ext.getCmp(myObject.abtest.name+"-completeform").getForm().findField('reasoning').getValue(),
            					"rolledvariation":Ext.getCmp(myObject.abtest.name+"-completeform").getForm().findField('completed').getValue().inputValue
            				},
            			    success : function(response, action) {
            			    	if(response.responseText==null){
            			    		Ext.MessageBox.alert('Error', 'Connection Problem !');
            			    		return;
            			    	}
            					var jsonObj = Ext.util.JSON.decode(response.responseText);
            					
            					if (jsonObj.results == 'success') {
            						Ext.MessageBox.alert('Success', jsonObj.msg);
            						window.location.reload();

            					} else {
            						Ext.MessageBox.alert('Error', jsonObj.msg);
            					}
            				}
            			});

            			myObject.close();
            		}
            		
            	}
            }]
			
		}, config);

		MABCompleteCommmentDialog.superclass.constructor.call(this, config);
	},
	generateItems : function() {
		var myObject=this;
		var items = [];
		
	    var i=0,j=0;
	    var radioItems=[];
	    for(var key in this.abtest['variations'] ){
	    	radioItems[j++]={xtype:'radio',boxLabel: key, name: "completed",inputValue:key};
	    }
	    
	    
	    items[i++] = new Ext.form.FormPanel({
	    	id:this.abtest.name+"-completeform",
	    	frame:true,
	    	items:[{
	    		xtype:"radiogroup",
	    		fieldLabel: 'Which variant was rolled out to everyone?',
	    		allowBlank : false,
	    		id:this.abtest.name+"-completed",
	    		name:'completed',
	    		columns:3,
	    		vertical: true,
	    		
	    		layout:"fit",
	    		height:'50%',
	    		width:'100%',
	    		items:radioItems},
	    		{
	    		xtype:"textarea",
	    		name:'reasoning',
	    		width:"95%",
	    		fieldLabel: 'Reasoning *',
	    		id:this.abtest.name+"-complete-comment",
	    		//height:"40%",
	    		allowBlank : false}
	    		]
	    });
	    
	   
		return items;
	}
}); 




var MABChangeCommmentDialog = Ext.extend(Ext.Window, {
	constructor : function(config) {
		
		this.abtest=config.abtest;
		this.items = this.generateItems();
		var myObject=this;
		config = Ext.apply({
			//waitMsgTarget: true,
			title:this.abtest.name+" Change Comments",
			header : true,
			border : true,
			// bodyStyle : 'padding:10px 10px 5px 10px',
			labelPad : 10,
			autoScroll : true,
			header : false,
			modal:true,
			frame:true,
			labelWidth : 100,
			validateOnBlur : true,
			fileUpload : false,
			//layout:"form",
			timeout : 100,
			width : "500",
			//height: "100%",
			//y:100,
			defaults : {
				selectOnFocus: true,
				msgTarget: 'side'
			},
			buttons:[{
            	text : "Cancel",
            	handler : function() {
            		myObject.close();
            	}
            }, {
            	text : "Done",
            	handler : function() {
            		//
            		if(Ext.util.Format.trim(Ext.getCmp(myObject.abtest.name+"-comment1").getValue())!=""){
            			saveOrAddABTest(myObject,myObject.abtest,Ext.getCmp(myObject.abtest.name+"-comment1").getValue(),Ext.getCmp(myObject.abtest.name+"-comment2").getValue());
            			myObject.close();
            		}else{
            			Ext.MessageBox.alert('Error', "Comment is Required !");
            			Ext.getCmp(myObject.abtest.name+"-comment1").focus();
            		}
            		
            	}
            }]
			
		}, config);

		MABChangeCommmentDialog.superclass.constructor.call(this, config);
	},
	generateItems : function() {
		var myObject=this;
		var items = [];
		
	    var i=0;
	    items[i++] = new Ext.form.FormPanel({
	    	frame:true,
	    	items:[{
	    		xtype:"textarea",
	    		allowBlank : false,
	    		id:this.abtest.name+"-comment1",
	    		width:"95%",
	    		height:"40%",
	    		fieldLabel:"Whats the rational behind the change? *"},
	    		{
	    		xtype:"textarea",
	    		width:"95%",
	    		id:this.abtest.name+"-comment2",
	    		height:"40%",
	    		allowBlank : true,
	    		fieldLabel:"Additional Comments"}
	    		]
	    });
	    
	   
		return items;
	}
}); 


var MABauditDialog = Ext.extend(Ext.Window, {
	constructor : function(config) {
		
		this.abtest=config.abtest;
		this.items = this.generateItems();
		config = Ext.apply({
			title:this.abtest.name+" Audit Logs",
			header : true,
			border : true,
			modal:true,
			autoScroll : false,
			header : false,
			width : "60%",
			height: "auto",
			layout:"anchor"
			
		}, config);

		MABauditDialog.superclass.constructor.call(this, config);
	},
	generateItems : function() {
		var myObject=this;
		var items = [];
		var store = new Ext.data.Store({
			url: "abtesting_gate.php?mode=auditlogs&abtestid="+myObject.abtest.id,
			pruneModifiedRecords : true,
			reader: new Ext.data.JsonReader({
		  	  	totalProperty: 'count',
		  	  	root:'auditlogs',
		        fields: ['login', {name:'lastmodified', type:'date', dateFormat:'Y-m-d H:i:s'},'test','details',{name:'comment',type:'html'}]
			}),
			sortInfo:{field : 'lastmodified', direction:'desc'},
			remoteSort: true
		});
		var pagingBar = new Ext.PagingToolbar({
			pageSize : 30,
		    store: store,
		    width:"100%",
		    displayInfo: true
		});
		/*new Ext.data.JsonStore({
	       
	        root:'auditlogs',
	        fields: ['name', {name:'lastmod', type:'date', dateFormat:'timestamp'},'detail','comments']
		});*/
	    store.load({params:{start:0, limit:30}});
	    var i=0;
	    items[i++] = new Ext.grid.GridPanel({
	        store: store,
	        store: store,
	        sortable: true,
	        emptyText: 'Nothing to display',
	        width:"100%",
	        height:300,
	        autoScroll : true,
	        region:'center',
	        viewConfig: {
	            forceFit:true
	        },listeners: {
	            itemmouseenter: function(view, record, item) {
	                Ext.fly(item).set({'data-qtip': 'My tooltip: ' + record.get('name')});
	            }
	        },
	        columnLines: true,
	        bbar:pagingBar,
	        anchor: '-7, 0',
	        columns: [{
	            header: 'Modified by',
	            sortable: true,
	            dataIndex: 'login',
	            width:50
	        },{
	            header: 'Modified at',
	            xtype: 'datecolumn',
	            format: 'd-m-y h:i a',
	            sortable: true,
	            dataIndex: 'lastmodified',
	            width:60
	        },{
	        	header: 'Test',
	            sortable: true,
	            dataIndex: 'test',
	            width:60
	        },{
	            header: 'Details',
	            cls:'listViewColumnWrap',
	            dataIndex: 'details',
	            renderer:function (value, metadata, record, rowIndex, colIndex, store){
	                metadata.attr = 'ext:qtip="' + Ext.util.Format.htmlEncode(value) + '"';
	                return "<div style=\"max-height:300;overflow-y:auto;overflow-x:hidden\">"+value+"</div>";
	            }
	        },{
	            header: 'Comments',
	            dataIndex: 'comment',
	            renderer:function (value, metadata, record, rowIndex, colIndex, store){
		               metadata.attr = 'ext:qtip="' + Ext.util.Format.htmlEncode(value) + '"';
		               return "<div style=\"max-height:300;overflow-y:auto;overflow-x:hidden\">"+value+"</div>";
	            },
	            cls:'listViewColumnWrap'
	        }]
	    });
	   
		return items;
	}
}); 

//Panel having control button like enable/update/completed/edit by wizard/view audit logs.
var MABControlPanel = Ext.extend(Ext.Panel, {
	constructor : function(config) {
		this.abtest = config.abtest;
		// Pre-Render tasks //
		this.items = this.generateItems();
		// -----------------//

		config = Ext.apply({
			header : false,
			border : false,
			// bodyStyle : 'padding:10px 10px 5px 10px',
			frame:true,
			labelPad : 10,
			autoScroll : true,
			header : false,
			labelWidth : 100,
			validateOnBlur : true,
			fileUpload : false,
			timeout : 100,
			width : "100%",
			layout : 'table',
			layoutConfig : {
				columns : 1,
				tableAttrs : {
					style : {
						padding : 5
					}
				}
			},
			cls : "button-table"
		}, config);

		MABControlPanel.superclass.constructor.call(this, config);
	},

	generateItems : function() {
		var items = [];
		var i = 0;
		var totalProb = 0;
		var myObject=this;
		items[i++] = {
			xtype : 'checkbox',
			id:this.abtest.name+'-enabled',
			cls : "orange-bg",
			boxLabel : 'Enabled',
			margins : "5 5 5 5 5",
			allowBlank : false,
			width : "100%",
			checked:this.abtest.enabled=='1',
			listeners: {
			    'change': function(c,e){
			    	myObject.abtest.enabled=Ext.getCmp(myObject.abtest.name+ '-enabled').getValue();
			    }
			  }

		};
		if(this.abtest.completed=='1'){
			items[i-1].disabled=true;
		}
		items[i++] = {
			xtype : 'button',
			cls : "orange-bg",
			text : 'Update',
			margins : "5 5 5 5 5",
			allowBlank : false,
			width : "100%",
			handler: function(){
				
				var mainForm = Ext.getCmp("MABSegFormPanel-"+myObject.abtest.name);
				if (mainForm.validateAndUpdate()) {
					var dialog = new MABChangeCommmentDialog({
						abtest : myObject.abtest
					});
					dialog.show();
				}
            }

		};
		if(this.abtest.completed=='1'){
			items[i-1].disabled=true;
		}
		if(this.abtest.completed=='0'){
			items[i++] = {
				xtype : 'button',
				text : 'Reset',
				allowBlank : false,
				width : "100%",
				hidden:true,
				handler: function(){
					resetComponent(myObject.abtest);
				}
	
			};
		}
		// Complete
		items[i++] = {
			xtype : 'button',
			allowBlank : false,
			text : "Completed",
			width : "100%",
			handler:function(){
				var dialog=new MABCompleteCommmentDialog({abtest:myObject.abtest});
				dialog.show();
			}
		};
		if (this.abtest.completed == '1') {
			items[i - 1].cls = 'green-bg';
			items[i-1].disabled=true;
		} else {
			items[i - 1].cls = 'orange-bg';
		}

		items[i++] = {
				xtype : 'button',
				allowBlank : false,
				text : "Edit By Wizard",
				width : "100%",
				handler: function(){
					wizardHandler(null,'launch',Ext.util.JSON.decode(Ext.util.JSON.encode(myObject.abtest)));
	            }
		};
		if(this.abtest.completed=='1'){
			items[i-1].disabled=true;
		}
		
		// Audit logs
		items[i++] = {
			xtype : 'button',
			allowBlank : false,
			text : "View Audit Logs",
			width : "100%",
			handler: function(){
				var auditLogDialog=new MABauditDialog({abtest:myObject.abtest});
				auditLogDialog.show();
            }
		};
		return items;
	}
});


var MABTestBasicSettingPanel = Ext.extend(Ext.FormPanel, {
	constructor : function(config) {
		this.abtest = config.abtest;
		// Pre-Render tasks //
		this.items = this.generateItems();
		// -----------------//

		config = Ext.apply({
			header : false,
			border : false,
			// bodyStyle : 'padding:10px 10px 5px 10px',
			labelPad : 10,
			autoScroll : true,
			header : false,
			labelWidth : 100,
			validateOnBlur : true,
			fileUpload : false,
			timeout : 100,
			width : "100%"
		}, config);

		MABTestBasicSettingPanel.superclass.constructor.call(this, config);
	},

	generateItems : function() {
		var items = [];
		var i = 0;
		var myObject=this;
		
		items[i++] = {
			xtype : 'numberfield',
			fieldLabel : "GA Slot",
			name : 'ga_slot',
			allowBlank : true,
			value : this.abtest.ga_slot,
			maxValue : 100,
			minValue : 0,
			width : 30,
			enableKeyEvents: true
		};
		items[i++] = {
				xtype : 'numberfield',
				fieldLabel : "Omniture Slot",
				name : 'omni_slot',
				allowBlank : true,
				value : this.abtest.omni_slot,
				maxValue : 75,
				minValue : 0,
				width : 30,
				enableKeyEvents: true
			};
		if(this.abtest.completed=='1'){
			items[i-1].disabled=true;
		}
			
		return items;
	},resetVal:function(){
		
	}
});

//Panel having all the sub panels basing setting panel/ configuration test variant setting panel and main controlpanel
var MABSegFormPanel = Ext
		.extend(
				Ext.Panel,
				{
					constructor : function(config) {
						this.abtest = config.abtest;
						this.config = config;
						// Pre-Render tasks //
						this.items = this.generateItems();
						// -----------------//

						config = Ext.apply({
							id:"MABSegFormPanel-"+this.abtest.name,
							header : false,
							border : false,
							// bodyStyle : 'padding:10px 10px 5px 10px;',
							frame:true,
							layout:"column",
							labelPad : 10,
							autoScroll : true,
							header : false,
							labelWidth : 100,
							validateOnBlur : true,
							fileUpload : false,
							timeout : 100,
							widht : "100%"
						}, config);

						MABSegFormPanel.superclass.constructor.call(this,
								config);
					},

					generateItems : function() {
							this.config.disableNameChange=true;
							this.basicTestSettingsForm = new BasicTestSettingsForm(this.config);
							this.basicTestSettingsForm.columnWidth = .7;
							this.basicTestSettingsForm.id="basic-settings-"+this.abtest.name;
							this.settingsPanel = new MABTestBasicSettingPanel(this.config) ;
							this.settingsPanel.id="general-settings-"+this.abtest.name;
							this.settingsPanel.columnWidth = .18;
							this.controlPanel = new MABControlPanel(this.config);
							this.controlPanel.columnWidth = .12;
							var items = [ this.basicTestSettingsForm
										, this.settingsPanel
										, this.controlPanel];
						
						return items;
					}
				,validateAndUpdate:function(validate){
					var myObject=this;
					if(!myObject.basicTestSettingsForm.validate()){
						return false;
					}
					
					if(!myObject.settingsPanel.getForm().isValid()){
						return false;
					}
					var form = myObject.settingsPanel.getForm();
					myObject.abtest.ga_slot=form.findField("ga_slot").getValue();
					myObject.abtest.omni_slot=form.findField("omni_slot").getValue();
					
					
					var values = myObject.basicTestSettingsForm.getForm().getValues();
					
					if(myObject.abtest.seg_algo=='abtest\\algos\\impl\\RandomSegmentationAlgo'){
					  for(var i=0;i<values.variantid.length;i++){
						myObject.abtest.variations[values.oldvariantname[i]].p_prob=values.variantprob[i];
					  }
					}

					if(myObject.abtest.seg_algo=='abtest\\algos\\impl\\NewUserOldUserRandomSegmentationAlgo'){
					  for(var i=0;i<values.newuserprob.length;i++){
						myObject.abtest.variations[values.oldvariantname[i]].algo_config['new']=values.newuserprob[i];
					  }
					  for(var i=0;i<values.olduserprob.length;i++){
						myObject.abtest.variations[values.oldvariantname[i]].algo_config.old=values.olduserprob[i];
					  }
					  
					}
					
					
					return true;
				}
				}
		
		
		);

Ext
		.onReady(function() {
			Ext.QuickTips.init();
			var myMask = new Ext.LoadMask(Ext.getBody(), {msg: "Loading..."});
			Ext.Ajax.on('beforerequest', myMask.show, myMask);
			Ext.Ajax.on('requestcomplete', myMask.hide, myMask);
			Ext.Ajax.on('requestexception', myMask.hide, myMask);
			var searchPanel = new Ext.form.FormPanel(
					{
						renderTo : 'contentPanel',
						title : 'AB Tests',
						id : 'searchPanel',
						autoHeight : true,
						collapsible : true,
						border : true,
						frame:true,
						width : "800",
						labelSeparator : ' :',
						labelWidth : 100,
						layout : "hbox",
						layoutConfig : {
							defaultMargins : {
								top : 10,
								right : 10,
								bottom : 10,
								left : 10
							}

						},
						keys : [ {
							key : [ Ext.EventObject.ENTER ],
							handler : function() {
								
								 Ext.getCmp('test-search-button').handler();
								
							}
						} ],
						items : [
								{
									fieldLabel : "Search",
									xtype : 'textfield',
									id : "query",
									allowBlank : false,
									name : 'query',
									width : "35%",
									height : 20,
									emptyText : 'Search by Test or Variant name'
									  
								},
								{
									xtype : 'button',
									id:"test-search-button",
									formBind : true,
									disabled : false,

									text : 'Search',
									width : "10%",
									height : 20,
									handler : function() {

										var form = searchPanel.getForm();
										if (Ext.util.Format
												.trim(form.getValues("query").query) == ""
												|| Ext.util.Format
														.trim(form
																.getValues("query").query) == 'Search by Test or Variant name') {
											for ( var i = 0; i < abTestPanelItems.length; i++) {
												Ext.getCmp(
														abTestPanelItems[i].id)
														.show();
												Ext.getCmp(
														abTestPanelItems[i].id)
														.expand();
											
											}
										} else {
											for ( var i = 0; i < abTestPanelItems.length; i++) {
												if (abTestPanelItems[i].search
														.toLowerCase()
														.indexOf(
																Ext.util.Format
																		.trim(
																				form
																						.getValues("query").query)
																		.toLowerCase()) >= 0) {
													Ext
															.getCmp(
																	abTestPanelItems[i].id)
															.show();
													Ext
															.getCmp(
																	abTestPanelItems[i].id)
															.expand();
													/*
													 * Ext .getCmp(
													 * abTestPanelItems[i].id)
													 * .enable();
													 */
												} else {
													Ext
															.getCmp(
																	abTestPanelItems[i].id)
															.hide();
													/*
													 * Ext .getCmp(
													 * abTestPanelItems[i].id)
													 * .disable();
													 */
												}

											}
										}
									}
								},
								{
									xtype : 'button',
									formBind : true,
									disabled : false,
									defaults : {
										margins : '10 15 10 10'
									},
									text : 'Clear Search',
									width : "10%",
									height : 20,
									handler : function() {
										var form = searchPanel.getForm();
										for ( var i = 0; i < abTestPanelItems.length; i++) {
											Ext.getCmp(abTestPanelItems[i].id)
													.show();
											/*
											 * Ext.getCmp(abTestPanelItems[i].id)
											 * .enable();
											 */
											Ext.getCmp(abTestPanelItems[i].id)
													.expand();
										}
										Ext
												.getCmp('query')
												.setValue(
														"Search by Test or Variant name");
									}
								},
								{
									xtype : 'button',
									formBind : true,
									disabled : false,
									defaults : {
										margins : '10 15 10 10'
									},
									text : 'Create New A/B Test',
									width : "10%",
									height : 20,
									handler : function() {
										wizardHandler(null,'launch',null);
									}
								},{
									xtype : 'button',
									formBind : true,
									disabled : false,
									defaults : {
										margins : '10 15 10 10'
									},
									text : 'View Global Audit Logs',
									width : "10%",
									height : 20,
									handler : function() {
										var auditLogDialog=new MABauditDialog({abtest:{name:"Global",id:"global"}});
										auditLogDialog.show();
									}
								}  ]
					});

			var abTestPanelItems = [];
			var i = 0;
			//modifiedabtests=jQuery.extend(true, {}, abtests);
			for ( var key in abtests) {
					if(typeof abtests[key] == 'function'){
			    		continue;
			    	}
					var variations = [];
					var v = 0;
					var config = {
						abtest : abtests[key]
					};
					

					abTestPanelItems[i++] = {
						id : 'abtest-' + abtests[key].name,
						xtype : 'fieldset',
						width : "95%",
						title : abtests[key].name,
						collapsible : true,
						autoHeight : true,
						animCollapse : true,
						autoHeight : true,
						hidden : false,
						items : [ new MABSegFormPanel(config) ]

					};
					abTestPanelItems[i - 1].search = abtests[key].search;
				
				
			}
			var searchResultPanel = new Ext.Panel({
				renderTo : 'contentPanel',
				id : 'searchResultPanel',
				height : 'auto',
				collapsible : true,
				border : false,
				width : "1200",
				header : false,
				labelSeparator : ' :',
				labelWidth : 100,
				padding : 10,
				items : abTestPanelItems
			});

		});


