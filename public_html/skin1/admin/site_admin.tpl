{* $Id: menu.tpl,v 1.68.2.2 2006/06/20 05:52:05 svowl Exp $ *}
{capture name=menu}
<li><a href="{$catalogs.admin}/configuration.php" class="VertMenuItems">{$lng.lbl_general_settings}</a></li>
<li><a href="{$catalogs.admin}/manageBots.php" class="VertMenuItems">Manage Bots</a></li>
<li><a href="{$catalogs.admin}/set_solr_fields_priority.php" class="VertMenuItems">Set Solr Priority</a></li>
<li><a href="{$catalogs.admin}/Search_Synonym_Admin.php" class="VertMenuItems">Search Synonym</a></li>
<li><a href="{$catalogs.admin}/set_sorting_order.php" class="VertMenuItems">Set Sorting Order</a></li>
<br>
<li><a href="{$catalogs.admin}/widgets/popular_widget.php" class="VertMenuItems">Most Popular Links</a></li>
<li><a href="{$catalogs.admin}/widgets/banners_widget.php" class="VertMenuItems">Home Page Banners</a></li>
<li><a href="{$catalogs.admin}/widgets/homepagelists_widget.php" class="VertMenuItems">Home Page Scrollable Lists</a></li>
<li><a href="{$catalogs.admin}/widgets/topnavigation_widget.php" class="VertMenuItems">Top Navigation Menus</a></li>
<li><a href="{$catalogs.admin}/widgets/topnavigation_widget_v2.php" class="VertMenuItems">Top Navigation Menus V2</a></li>
<li><a href="{$catalogs.admin}/widgets/topnavigation_widget_v3.php" class="VertMenuItems">Top Navigation Menus V3</a></li>
<li><a href="{$catalogs.admin}/widgets/keyvaluepairs_widget.php" class="VertMenuItems">Widget Key Value Pairs</a></li>
<li><a href="{$catalogs.admin}/Search_Feature_Gate.php" class="VertMenuItems">Boosted Search Parameters</a></li>
<li><a href="{$catalogs.admin}/solr_search.php" class="VertMenuItems">Add solr Search</a></li>
<!-- <li><a href="{$catalogs.admin}/size_unification_filter_order.php" class="VertMenuItems">Size Filter Order</a></li> -->
<br>
<li><a href="{$catalogs.admin}/feature_gate.php?mode=view&object=keyvaluepairs" class="VertMenuItems">Feature Gate</a></li>
<br>
<li><a href="{$catalogs.admin}/abtesting_gate.php?mode=view&object=list" class="VertMenuItems">AB Tests List</a></li>
<br>
<li><a href="{$catalogs.admin}/xcache_common.php" class="VertMenuItems">XCache</a></li>
<br>
<li><a href="{$catalogs.admin}/jpegminiConfig.php" class="VertMenuItems">Jpegmini Config</a></li>
<br>
<li><a href="{$catalogs.admin}/mobileUAWhitelist.php" class="VertMenuItems">Mobile UA Whitelist</a></li>
<br>
{/capture}
{include file="menu.tpl" dingbats="dingbats_categorie.gif" menu_title='Site Admin' menu_content=$smarty.capture.menu }
