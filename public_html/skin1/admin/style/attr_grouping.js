function productCheckBoxClickHandle(obj,styleid){
	var style_group_csv=Ext.getCmp("style_csv").getValue();
	if(obj.checked==true){
		if(style_group_csv==''){
			style_group_csv=styleid;
		}else{
			var found=false;
			var str=style_group_csv;
			var arrayStr=str.split(',');
			for(var i=0;i<arrayStr.length;i++){
				if($.trim(arrayStr[i])==styleid){
					found=true;
				}
			}
			if(!found)
				style_group_csv=style_group_csv+","+styleid;
		}
	}else{
		var str=style_group_csv;
		var arrayStr=str.split(',');
		var temp=[];
		var j=0;
		for(var i=0;i<arrayStr.length;i++){
			if($.trim(arrayStr[i])!=styleid){
				temp[j++]=$.trim(arrayStr[i]);
			}
		}
		style_group_csv=temp.join(',');
		
	}
	Ext.getCmp("style_csv").setValue(style_group_csv);
}

var StylePickerDialog = Ext.extend(Ext.Window, {
	constructor : function(config) {
		
		this.selectedStyles=config.selectedStyles;
		this.renderMode=config.renderMode;
		this.selectHandler=config.selectHandler;
		this.vendorarticle=config.vendorarticle;
		this.items = this.generateItems();
		var myObject=this;
		config = Ext.apply({
					
			header : true,
			border : true,
			labelPad : 10,
			autoScroll : false,
			header : false,
			modal:true,
			frame:true,
			labelWidth : 100,
			validateOnBlur : true,
			fileUpload : false,
			timeout : 100,
			width : "550",
			height:'400',
			layout:"anchor",
			resizable:false,
			defaults : {
				selectOnFocus: true,
				msgTarget: 'side'
			},
			buttons:[{
            	text : "Done",
            	handler : function() {
            		myObject.close();
            	}
            }]
			
		}, config);

		StylePickerDialog.superclass.constructor.call(this, config);
	},
	generateItems : function() {
		var myObject=this;
		var items = [];
		store=new Ext.data.JsonStore({
			fields: ['style_id','image_url','name','article_number','checked'],
			url: "/admin/attribute_grouping.php?mode=get_matching_styles&linkStyleids="+this.selectedStyles+"&vendorarticle="+this.vendorarticle,
			totalProperty:"totalCount",
			remoteSort: false,
			root:"searchResults"
		});
		store.load();
	    var i=0;
	    items[i++] = new Ext.grid.GridPanel({
			store : store,
			border : false,
			header : false,
			width: 'auto',
		    height: 380,
		    frame: true,
			labelSeparator : ' :',
			labelWidth : 100,
			padding : 10,
	        sortable: true,
	        emptyText: 'Nothing to display',
	        autoScroll : true,
	        
	        columnLines: true,
	        columns: [{
	            header: 'Style Image',
	            width: 130,
	            sortable: true,
	            dataIndex: 'image_url',
	            renderer:function (val) {
	                return '<img src="' + val + '" height="160" width="120" >';
	            }

	        },{
	        	width: 200,
	            header: 'Style Name',
	            sortable: true,
	            dataIndex: 'name'
	        },
	        {
	        	width: 100,
	            header: 'Article No',
	            sortable: true,
	            dataIndex: 'article_number'
	        },
	        {
	        	width: 50,
	        	header: 'Control',
	        	renderer:function (val,cell,record) {
	        		if(record.data.checked==0){
	        			return '<div style="text-align:center;width:100%;height:100%" ><input type="checkbox" onclick="productCheckBoxClickHandle(this,'+val+')"></div>';
	        		}
	        		return '<div style="text-align:center;width:100%;height:100%" ><input type="checkbox" onclick="productCheckBoxClickHandle(this,'+val+')" checked="checked"></div>';
		        },
		        dataIndex: 'style_id'
	        }]
	    
		});

		return items;
	}
}); 


var GroupDialog = Ext.extend(Ext.Window, {
	constructor : function(config) {
		
		this.stylegroup=config.stylegroup;
		if(singleStyleMode==true && config.stylegroup.styles==null){
			config.stylegroup.styles=""+ownerStyleId;
		}
		stylegroup = config.stylegroup;
		this.items = this.generateItems();
		var myObject=this;
		config = Ext.apply({
					
			header : true,
			border : true,
			labelPad : 10,
			autoScroll : true,
			header : false,
			modal:true,
			frame:true,
			labelWidth : 100,
			validateOnBlur : true,
			fileUpload : false,
			timeout : 100,
			width : "500",
			defaults : {
				selectOnFocus: true,
				msgTarget: 'side'
			},
			buttons:[{
            	text : "Cancel",
            	handler : function() {
            		myObject.close();
            	}
            }, {
            	text : "Save",
            	handler : function() {
            		var submitform=Ext.getCmp('save_group_form').getForm();
            		if(submitform.isValid()){
            			Ext.Ajax.request({
            				url : '/admin/attribute_grouping.php?mode=save_matching_styles',
            				loadMask: {msg: 'Saving Group: '+Ext.getCmp("pattern_name").getValue()},
            				params : {"group_id" : stylegroup.group_id,"linkStyleids":Ext.getCmp("style_csv").getValue(),
            					'pattern_name':Ext.getCmp("pattern_name").getValue(),'group_type':Ext.getCmp("group_type_combo").getValue()
            				},
            			    success : function(response, action) {
            			    	if(response.responseText==null){
            			    		Ext.MessageBox.alert('Error', 'Connection Problem !');
            			    		return;
            			    	}
               					var jsonObj = Ext.util.JSON.decode(response.responseText);
            					
            					if (jsonObj.result == 'success') {
            						Ext.MessageBox.alert('Success', jsonObj.msg);
            						lastOptions = searchStore.lastOptions;
            						searchStore.reload(lastOptions);
            						myObject.close();
            					} else {
            						Ext.MessageBox.alert('Error', jsonObj.msg);
            					}
            					
            				}
            			});
            		}
            		
            	}
            }]
			
		}, config);

		GroupDialog.superclass.constructor.call(this, config);
	},
	generateItems : function() {
		var myObject=this;
		var items = [];
		store=new Ext.data.JsonStore({
			id: "groupTypesStore",
			fields: ['group_type'],
			url: "/admin/attribute_grouping.php?mode=get_group_types",
			totalProperty:"totalCount",
			remoteSort: false,
			root:"searchResults"
		});
		
	    var i=0;
	    this.submitform = new Ext.form.FormPanel({
	    	frame:true,
	    	id:"save_group_form",
	    	items:[{
	    		xtype:"textfield",
	    		allowBlank : false,
	    		id:"pattern_name",
	    		width:"150",
	    		fieldLabel:"Pattern Name *",
	    		value:this.stylegroup.pattern_name
	    		},
	    		{
			        xtype: 'combo',
			        id: 'group_type_combo',
			        fieldLabel:"Pattern Type *",
			        emptyText: 'Select Group Type...',
			        store:store,
			        displayField: 'group_type',
			        valueField: 'group_type',
			        forceSelection: true,
			        editable: false,
			        allowBlank : false,
			        width:"150",
			        triggerAction: 'all',
			        value:this.stylegroup.group_type
			      },
			      {
				        xtype: 'textfield',
				        id: 'style_csv',
				        allowBlank : false,
				        fieldLabel:"Style Ids(CSV) *",
				        value:this.stylegroup.styles,
				        width:"90%"
				   },
				   { 
					   xtype: 'fieldset',
					   layout: 'column',
					   items:[
					          {
							   xtype: 'label',
							   allowBlank : true,
						       text:"  Or Search by Article number  :",
						    	   width:"100"
							},{
							   xtype: 'textfield',
							   id: 'vendor_article',
						       allowBlank : true,
							},
							 {
								   xtype: 'button',
							       text:"Search",
							       width:"100",
							       handler:function(){
							    	   if($.trim(Ext.getCmp("vendor_article").getValue())==""){
							    		   Ext.MessageBox.alert('Error', "Enter Article/Vendor Article number to Search");
							    		   return;
							    	   }
							    	   var dialog= new StylePickerDialog({vendorarticle:Ext.getCmp("vendor_article").getValue(),selectedStyles:Ext.getCmp("style_csv").getValue()});
							    	   dialog.show();
							      }
							  }
						]
					   
				   }
				   
	    		]
	    });
	    
	    items[i++] = this.submitform ;
	   
		return items;
	}
}); 

var searchStore;
Ext
		.onReady(function() {
			Ext.QuickTips.init();
			var myMask = new Ext.LoadMask(Ext.getBody(), {msg: "Loading..."});
			Ext.Ajax.on('beforerequest', myMask.show, myMask);
			Ext.Ajax.on('requestcomplete', myMask.hide, myMask);
			Ext.Ajax.on('requestexception', myMask.hide, myMask);
			var searchPanel = new Ext.Panel(
					{
						renderTo : 'contentPanel',
						title : 'Style Attribute Grouping',
						id : 'searchPanel',
						autoHeight : true,
						collapsible : true,
						border : true,
						frame:true,
						width : panelWidth,
						labelSeparator : ' :',
						labelWidth : 100,
						layout : "hbox",
						layoutConfig : {
							defaultMargins : {
								top : 10,
								right : 10,
								bottom : 10,
								left : 10
							}

						},
						keys : [ {
							key : [ Ext.EventObject.ENTER ],
							handler : function() {
								
								 Ext.getCmp('test-search-button').handler();
								
							}
						} ],
						items : [
								{
									fieldLabel : "Search",
									xtype : 'textfield',
									id : "query",
									allowBlank : false,
									name : 'query',
									width : "40%",
									height : 20,
									emptyText : 'Search Text',
									hidden:singleStyleMode
									  
								},
								{
							        xtype: 'combo',
							        id: 'query_type',
							        emptyText: 'Select Search Type...',
							        store: 
							          new Ext.data.SimpleStore({
							            fields: ['query_type','name'],
							            data: [
							              ["pattern_name","Pattern Name"],["group_type","Pattern Type"],["style","Style ID"]                
							                  ]
							        }), // end of Ext.data.SimpleStore
							        displayField: 'name',
							        valueField: 'query_type',
							        selectOnFocus: true,
							        mode: 'local',
							        typeAhead: true,
							        editable: false,
							        triggerAction: 'all',
							        value: 'pattern_name',
							        hidden:singleStyleMode
							      }
								,
								{
									xtype : 'button',
									id:"test-search-button",
									formBind : true,
									disabled : false,

									text : 'Search',
									width : "10%",
									height : 20,
									handler : function() {
										if (Ext.getCmp("query").getValue()!="") {
											searchStore.baseParams.query_str=Ext.getCmp("query").getValue();
											searchStore.baseParams.query_type=Ext.getCmp("query_type").getValue();
											searchStore.load({params:{start:0, limit:30 ,query_str:Ext.getCmp("query").getValue(),query_type:Ext.getCmp("query_type").getValue()}});
										} 
									},
									hidden:singleStyleMode
								},
								{
									xtype : 'button',
									id:"test-create-new-button",
									formBind : true,
									disabled : false,

									text : 'New',
									width : "10%",
									height : 20,
									handler : function() {

										var groupDialog = new GroupDialog({stylegroup:{group_id:0,pattern_name:"",group_type:""}});
							        	groupDialog.show();
									}
								}
								  ]
					});

			
			searchStore =  new Ext.data.Store({
				id:"search_grid_store",
				url: "/admin/attribute_grouping.php?mode=search",
				baseParams:{query_str:"",query_type:""},
				pruneModifiedRecords : true,
				reader: new Ext.data.JsonReader({
			  	  	totalProperty: 'count',
			  	  	root:'searchResults',
			        fields: ['pattern_name', 'group_type','group_id','styles']
				}),
				sortInfo:{field : 'pattern_name', direction:'desc'},
				remoteSort: true
			});
			if(singleStyleMode==true){
				searchStore.baseParams.query_str=ownerStyleId;
				searchStore.baseParams.query_type="style";
				searchStore.load({params:{start:0, limit:30 ,query_str:ownerStyleId,query_type:"style"}});
			}
			var pagingBar = new Ext.PagingToolbar({
				pageSize : 30,
			    store: searchStore,
			    width:"100%",
			    displayInfo: true
			});

			
			var searchResultPanel = new Ext.grid.GridPanel({
				id:"search_grid",
				store : searchStore,
				renderTo : 'contentPanel',
				id : 'searchResultPanel',
				height : 'auto',
				frame:true,
				collapsible : true,
				border : false,
				width : panelWidth,
				header : false,
				labelSeparator : ' :',
				labelWidth : 100,
				padding : 10,
		        sortable: true,
		        emptyText: 'Nothing to display',
		        height:resultPanelHeight,
		        autoScroll : true,
		        region:'center',
		        viewConfig: {
		            forceFit:true
		        },
		        sm: new Ext.grid.RowSelectionModel({singleSelect:true}),
		        columnLines: true,
		        bbar:pagingBar,
		        anchor: '-7, 0',
		        columns: [{
		            header: 'Pattern Name',
		            sortable: true,
		            dataIndex: 'pattern_name'
		        },{
		            header: 'Pattern Type',
		            sortable: true,
		            dataIndex: 'group_type'
		        },
		        {
		            header: 'Styles',
		            sortable: false,
		            dataIndex: 'styles'
		        }],
		        listeners: {
			        rowdblclick: function(grid,rowindex){
			        	var groupDialog = new GroupDialog({stylegroup:grid.getStore().getAt(rowindex).data});
			        	groupDialog.show();
			        }
		        }
		    
			});
			

		});


