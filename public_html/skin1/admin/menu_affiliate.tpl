{* $Id: menu_affiliate.tpl,v 1.29 2005/11/18 12:01:07 max Exp $ *}
{capture name=menu}
<a href="{$catalogs.admin}/partner_plans.php" class="VertMenuItems">{$lng.lbl_affiliate_plans}</a><br />
<a href="{$catalogs.admin}/partner_commissions.php" class="VertMenuItems">{$lng.lbl_commissions}</a><br />
<a href="{$catalogs.admin}/partner_orders.php" class="VertMenuItems">{$lng.lbl_partners_orders}</a><br />
<a href="{$catalogs.admin}/partner_report.php" class="VertMenuItems">{$lng.lbl_partner_accounts}</a><br />
<a href="{$catalogs.admin}/payment_upload.php" class="VertMenuItems">{$lng.lbl_payment_upload}</a><br />
<a href="{$catalogs.admin}/partner_banners.php" class="VertMenuItems">{$lng.lbl_banners}</a><br />
<a href="{$catalogs.admin}/partner_level_commissions.php" class="VertMenuItems">{$lng.lbl_multi_tier_affiliates}</a><br />
<a href="{$catalogs.admin}/banner_info.php" class="VertMenuItems">{$lng.lbl_affiliate_statistics}</a><br />
<a href="{$catalogs.admin}/partner_adv_campaigns.php" class="VertMenuItems">{$lng.lbl_advertising_campaigns}</a><br />
{/capture}
{ include file="menu.tpl" dingbats="dingbats_categorie.gif" menu_title=$lng.lbl_affiliates menu_content=$smarty.capture.menu }
<br />
