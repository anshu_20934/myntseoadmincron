
{literal}
<style>
#static_ul {width:100%;list-style:none;margin:20px 0;}
#static_ul li{width:80%;padding:20px 0;}
#static_ul li .label{width:20%;display:inline-block;float:left;}
#edit_page {display:inline-block;padding:4px 20px ;background:#999;cursor:pointer;}

</style>

{/literal}

{if $mode eq 'add'}
<div>ADD page</div>
{elseif $mode eq 'edit'}
<div>EDIT page</div>
{/if}

{if $mode eq 'done'}
{$message}

Add/Edit a page? 

<a href="myntra_static_page.php?mode=add">Add Another</a> <a href="myntra_static_page.php?mode=edit"> Edit</a>
{elseif $mode eq 'add' || $mode eq 'edit'}

<form action="myntra_static_page.php" method="post" name="addupdatestaticpages" id="addupdatestaticpages">
<input type="hidden" name="action" id="action"/>
<input type="hidden" name="mode" id="mode"/>
{if $mode eq 'edit'}<input type="hidden" name="up_id" id="up_id"/>{/if}

<ul width="100%" id="static_ul">
	{if $mode eq 'edit'}
	<li>
		<span class="label"> Select a page to edit</span>
		<select id="av-pages" name="av-pages">
			{foreach item=pages_item name=pages_n from=$pages}
			<option value="{$pages_item.id}">{$pages_item.name}</option>
			{/foreach}
		</select>
	
	</li>
	{/if}
	<li>
		<span class="label">Page Name</span><input type="text" name="name_field_{if $mode eq 'add'}new{else}edit{/if}" id="name_field"/>
	</li>
	<li>
		<span class="label">Page CSS</span><textarea rows="5" cols="90" id="css_field" name="css_field_{if $mode eq 'add'}new{else}edit{/if}" ></textarea>
	</li>
	<li>
		<span class="label">Page Content</span><textarea rows="18" cols="90" id="content_field" name="content_field_{if $mode eq 'add'}new{else}edit{/if}" id="content_field"></textarea>
	</li>
	<li>
		<span class="label">&nbsp;</span><input class="add-edit-btn " type="button" value="{if $mode eq 'add'}ADD{else}UPDATE{/if}" name="btn_{if $mode eq 'add'}new{else}edit{/if}" id="btn_{if $mode eq 'add'}new{else}edit{/if}" />
	</li>
	
</ul>
 	
</form>
{else}
I need some parameter to understand what action you want to perform !! 
{/if}
<script src="http://www.localhost.local/skin1/myntra_js/adminjs.js"></script>
<script>

{literal}

$(document).ready(function(){
	var c_mode={/literal}'{$mode}';{literal}
	//alert(c_mode);
	var j_pages={/literal}{if $jpages}{$jpages}{else}''{/if}{literal};
	if(c_mode=='edit'){
		$("#name_field").val(j_pages[0]['name']);
		$("#css_field").val(j_pages[0]['css']);
		$("#content_field").val(j_pages[0]['content']);
		$("#up_id").val(j_pages[0]['id']);
	}
	$('.add-edit-btn').click(function(){		
		if($('#name_field').val()==''){
			alert('enter name');
			return false;
		}
		else if($('#content_field').val()==''){
			alert('enter content');
			return false;
		}else
		{
			if(c_mode=='edit')
			{
				$('#action').val('edit');
				$('#mode').val('edit');
			}
			else if(c_mode=='add'){
				$('#action').val('add');
				$('#mode').val('add');
			}
			$('#addupdatestaticpages').trigger('submit');
		}
	});

	$('#av-pages').change(function(){
//		/alert($('#av-pages').val());
		
		for(var key in j_pages){
			if(j_pages.hasOwnProperty(key)){
				if(j_pages[key]['id']==$('#av-pages').val()){
					//alert(j_pages[key]['id'])
					$("#name_field").val(j_pages[key]['name']);
					$("#css_field").val(j_pages[key]['css']);
					$("#content_field").val(j_pages[key]['content']);
					$("#up_id").val(j_pages[key]['id']);
										
				}
			}
		}
	});	
	
});
{/literal}
</script>
