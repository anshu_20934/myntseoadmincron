{* $Id: featured_products.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
{include file="main/include_js.tpl" src="main/popup_product.js"}
{include file="admin/widgets/header.tpl"}

{capture name=dialog}
<div class="notification-message">{$message}</div>
{literal}
  <script language="Javascript">
	function addRule()
	{
		document.frmG.submit();
	}
  </script>
{/literal}
<a name="featured" />

{$lng.txt_featured_products}



{if $looktype == "Search"}
<form action="Search_Synonym_Admin.php" method="post" name="mapping-add-form" class="mapping-add-form">
<input type="hidden" name="mode" class="mode" value="AddMode"/>
<input type="submit" value="Go to Add Mode">
</form>
<table cellpadding="3" cellspacing="1" width="100%">
	<tr>
        <td><h2>Search Field</h2></td>
        <td>&nbsp;</td>
   </tr>
   <form action="Search_Synonym_Admin.php" method="post" name="mapping-add-form" class="mapping-add-form">
	<input type="hidden" name="mode" class="mode" value="StringSearch"/>
	<input type="hidden" name="offset" class="mode" value="{$offset}"/>	
	<input type="text" name="searchLanding" class="mode" value="{$searchLanding}"/>
	<input type="submit" value="Search String">
   <tr>
   </tr>
   <tr>
   	<td colspan="2">
   	{if $offset>0}
   	<form action="Search_Synonym_Admin.php" method="post" name="mapping-add-form" class="mapping-add-form">
	<input type="hidden" name="mode" class="mode" value="Previous"/>
	<input type="hidden" name="searchLanding" class="mode" value="{$searchLanding}"/>
	<input type="hidden" name="offset" class="mode" value="{$offset}"/>
	<input type="submit" value="Previous">
	{/if}
</form>
   	
   	</td>
   	<td colspan="2">
   	{if $offset< $maxoffset}
   	<form action="Search_Synonym_Admin.php" method="post" name="mapping-add-form" class="mapping-add-form">
	<input type="hidden" name="mode" class="mode" value="Next"/>
	<input type="hidden" name="searchLanding" class="mode" value="{$searchLanding}"/>
	<input type="hidden" name="offset" class="mode" value="{$offset}"/>
   	<P align="right">
	<input type="submit" value="Next">
   	</p>
	{/if}
   	</form>
   	</td>
   </tr>
   <tr>
   <td><b>Original</b></td>
   <td><b>Corrected</b></td>
   <td></td>
   <td></td>
   </tr>
   <break>
   <tr>
   	{foreach name=dataloop item=row from=$searchresult}
   	<form action="Search_Synonym_Admin.php" method="post" name="mapping-add-form" class="mapping-add-form">
	<tr class="TableSubHead">
	<td><input type="text" name="output[{$row.id}][original]" value="{$row.original}"  width = "80"></td>
	<td><input type="text" name="output[{$row.id}][corrected]" value="{$row.corrected}"  width = "80"></td>
	<td>
		<form name="frmG" action="{$form_action}" method="post" enctype="multipart/form-data" >
		<input type="hidden" name="mode" class="mode" value="update"/>
		<input type="hidden" name="searchLanding" class="mode" value="{$searchLanding}"/>
		<input type="hidden" name="offset" class="mode" value="{$offset}"/>
		<input type="hidden" name="id" class="mode" value="{$row.id}" />
	   	<P align="left">
		<input type="submit" value="Update">
		</P>
		</form>
	</td>
	<td>
		<form action="Search_Synonym_Admin.php" method="post" name="mapping-add-form" class="mapping-add-form">
		<input type="hidden" name="mode" class="mode" value="delete"/>
		<input type="hidden" name="searchLanding" class="mode" value="{$searchLanding}"/>
		<input type="hidden" name="offset" class="mode" value="{$offset}"/>
		<input type="hidden" name="id" class="mode" value="{$row.id}"/>
	   	<P align="right">
		<input type="submit" value="Delete">
		</P>
		</form>
	</td>
	</tr>
	{/foreach}
   </tr>
   </table>
   {/if}
   {if $looktype == "Add"}
   
 <form action="Search_Synonym_Admin.php" method="post" name="mapping-add-form" class="mapping-add-form">
<input type="hidden" name="mode" class="mode" value="SearchMode"/>
<input type="submit" value="Go to Search Mode">
</form>  
 <form action="Search_Synonym_Admin.php" method="post" name="mapping-add-form" class="mapping-add-form">
<input type="hidden" name="mode" value="addgroup" />
<input type="hidden" name="searchLanding" class="mode" value="{$searchLanding}"/>
<input type="hidden" name="offset" class="mode" value="{$offset}"/>
<h2>Enter Values in both the columns, you don't need to fill all the rows</h2>
<table cellpadding="3" cellspacing="1" width="100%">
   <td><b>Original</b></td>
   <td><b>Corrected</b></td>
	{foreach name=dataloop item=row from=$addArray}
	<tr>
	<td><input type="text" name="addOutput[{$row}][original]" class="mode"}"/></td>
	<td><input type="text" name="addOutput[{$row}][corrected]" class="mode"}"/></td>
	</tr>
	{/foreach}
</table>
 <input type="submit" value="Add">

</form>
{/if}



{/capture}
{include file="dialog.tpl" title=" Paramaterised Search Fields" content=$smarty.capture.dialog extra='width="100%"'}

{literal}
  <script language="Javascript">
     $(document).ready(function(){
    	 
    	 $('#landing-check').change(function(){
        	 if($(this).is(':checked')){
				$('input[name=landingPage]').removeAttr('disabled');
        	 }
        	 else{
        		 $('input[name=landingPage]').attr('disabled','disabled');
           	 }
         });
 			
    	 
     })
  </script>
{/literal}
