{* $Id: featured_products.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
{include file="main/include_js.tpl" src="main/popup_product.js"}
{include file="admin/widgets/header.tpl"}
{literal}
<style>
.lblleft {float:left; width:100px;}
</style>
<script type="text/javascript">
function delete_banner(id){
    var r=confirm("Are you sure you want to delete this banner");
    if(r == true){
        document.update.mode.value = 'delete';
        document.update.banner_id.value = id;
        document.update.submit();
    } 
}

function add_banner(){
    if ( !document.add.url || document.add.url.value == '' ||
            !document.add.alt || document.add.alt.value == '' ||
            !document.add.anchor || document.add.alt.value == '' ||
            !document.add.image || document.add.image.value == ''
            ){
        alert('One or more of url, alt, anchor or image is empty');
        return;
    }
    document.add.mode.value = 'add'; 
    document.add.submit();
}
</script>
{/literal}
{capture name=dialog}
<form action="banners_widget.php" method="post" name="update">
<input type="hidden" name="mode" />
<input type="hidden" name="banner_id" />
	<div class="DIV_TABLE">
		{if $banners}
			{foreach name=banners item=banner from=$banners}
				<div class="DIV_TR" style="clear:both; height:250px">
					<div class="DIV_TR_TD" style="width:30px; height:100%">{$smarty.foreach.banners.iteration}</div>
					<div class="DIV_TR_TD" style="width:600px; height:100%;"><img style="width:100%;height:100%;" src="{if $banner.image|strpos:$cdn_base !== false}{$banner.image}{else}{$http_location}/{$banner.image}{/if}" alt="{$banner.alt}" title="{$banner.anchor}" /></div>
					<div class="DIV_TR_TD" style="width:300px; height:100%">	
						<div style="height:30px"></div>
						<div style="text-align:left; padding:10px">
							<div class="lblleft" style="text-transform:none">Anchor Text</div>
							<input type="text" name="banner[{$banner.id}][anchor]" value="{$banner.anchor}" size="30"/>
						</div> 
						<div style="text-align:left; padding:10px">
							<div class="lblleft" style="text-transform:none">Alt Text</div>
							<input type="text" name="banner[{$banner.id}][alt]" value="{$banner.alt}" size="30"/>
						</div> 
					</div>
					<div align="DIV_TR_TD" style="width:200px; height:100%; float:left">
						<div style="padding:10px">
							<div style="text-align:center; padding:10px"><input type="submit" value="Up" onclick="javascript: document.update.mode.value = 'move_up';  document.update.banner_id.value = '{$banner.id}'; document.update.submit();" /></div>
							<div style="text-align:center; padding:10px"><input type="submit" value="Down" onclick="javascript: document.update.mode.value = 'move_down';  document.update.banner_id.value = '{$banner.id}'; document.update.submit();" /></div>
							<div style="text-align:center; padding:10px"><input type="button" value="Delete" onclick="javascript: delete_banner('{$banner.id}')" /></div>
						</div>
					</div>	
				</div>
				<div class="DIV_TR" style="clear:both;">
					<div class="DIV_TR_TD" style="width:100%">
						<div style="padding:10px">
							<span style="text-transform:none">URL </span><input type="text" name="banner[{$banner.id}][url]" value="{$banner.url}" size="100"/>
						</div>
					</div>
				</div>
				<div style="clear:both"></div>
			{/foreach}
		{/if}
		<div style="clear:both"></div>
		
		<div>
			<div class="SubmitBox" align="right">
			   <input type="button" value="Save" onclick="javascript: document.update.mode.value = 'save'; document.update.submit();" />
			</div>
		</div>
	</div>
</form>
<form enctype="multipart/form-data" action="banners_widget.php" method="POST" name="add">
	<input type="hidden" name="mode" />
	<a href="#addBannerDiv" onclick="javascript: document.getElementById('addBannerDiv').style.display = 'block'">Add one more banner >></a>
	<div id="addBannerDiv" style="display:none; padding:20px; background-color:#D6D7D9">
		<div>
			<div class="lblleft" style="">URL </div><input type="text" size="50" name="url" value=""/>
		</div>
		<div>
			<div class="lblleft">Alt Text</div><input type="text" size="50" name="alt" value=""/>
		</div>
		<div>
			<div class="lblleft">Anchor Text</div><input type="text" size="50" name="anchor" value=""/>
		</div>
		<div>
			<div class="lblleft">Image field</div><input name="image" type="file" id="image"/>
		</div>
		<div>
			<input type="button" onclick="javascipt: add_banner();" value="Add"/>
		</div>
	</div>
</form>

