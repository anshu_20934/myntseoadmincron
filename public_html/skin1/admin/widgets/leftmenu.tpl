{* $Id: menu.tpl,v 1.68.2.2 2006/06/20 05:52:05 svowl Exp $ *}
{capture name=menu}
<li><a href="{$catalogs.admin}/widgets/popular_widget.php" class="VertMenuItems">Most Popular Links</a></li>
<li><a href="{$catalogs.admin}/widgets/banners_widget.php" class="VertMenuItems">Home Page Banners</a></li>
<li><a href="{$catalogs.admin}/widgets/homepagelists_widget.php" class="VertMenuItems">Home Page Scrollable Lists</a></li>
<li><a href="{$catalogs.admin}/widgets/topnavigation_widget.php" class="VertMenuItems">Top Navigation Menus</a></li>
<li><a href="{$catalogs.admin}/widgets/keyvaluepairs_widget.php" class="VertMenuItems">Key Value Pairs</a></li>
<li><a href="{$catalogs.admin}/size_unification_admin.php" class="VertMenuItems">Size Unification</a></li>
<li><a href="{$catalogs.admin}/size_unification_filter_order.php" class="VertMenuItems">Size Filter Order</a></li>
<li><a href="{$catalogs.admin}/style_bulk_upload.php" class="VertMenuItems">Styles Bulk Upload</a></li>
{/capture}
{include file="menu.tpl" dingbats="dingbats_categorie.gif" menu_title='Widgets' menu_content=$smarty.capture.menu }