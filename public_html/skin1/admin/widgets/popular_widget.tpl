{* $Id: featured_products.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
{include file="main/include_js.tpl" src="main/popup_product.js"}
{include file="admin/widgets/header.tpl"}

{literal}
<script type="text/javascript">
function delete_perm_link(id){
    var r=confirm("Are you sure you want to delete this link");
    if(r == true){
        document.updatepermlinks.mode.value = 'delete';
        document.updatepermlinks.update_link_id.value = id;
        document.updatepermlinks.submit();
    }
}

function add_perm_link(){
    if ( !document.updatepermlinks.link_name || document.updatepermlinks.link_name.value == '' ||
         !document.updatepermlinks.link_url || document.updatepermlinks.link_url.value == '' ){
        alert('Link name or Link URL is empty');
        return;
    } 
    document.updatepermlinks.mode.value = 'add';
    document.updatepermlinks.submit();
}

function delete_nonperm_link(id){
    var r=confirm("Are you sure you want to delete this link");
    if(r == true){
        document.updatenonpermlinks.mode.value = 'delete';
        document.updatenonpermlinks.update_link_id.value = id;
        document.updatenonpermlinks.submit();
    }
}

function add_nonperm_link(){
    if ( !document.updatenonpermlinks.link_name || document.updatenonpermlinks.link_name.value == '' ||
         !document.updatenonpermlinks.link_url || document.updatenonpermlinks.link_url.value == '' ){
        alert('Link name or Link URL is empty');
        return;
    } 
    document.updatenonpermlinks.mode.value = 'add';
    document.updatenonpermlinks.submit();
}
</script>
{/literal}

{capture name=dialog}
<form action="popular_widget.php" method="post" name="updatepermlinks">
<input type="hidden" name="mode" />
<input type="hidden" name="update_link_id" />
<input type="hidden" name="is_permanent" value="1" />
<table cellpadding="3" cellspacing="1" width="100%">
<tr class="TableHead">
	<td width="15%">Link name</td>
	<td width="30%">Link</td>
	<td width="30%">Action</td>
</tr>

{if $popularlinks_perm}

{section name=link_num loop=$popularlinks_perm}
<tr{cycle values=", class='TableSubHead'"}>
		
	<td align="center" style="left-padding:30px"><input name="link_name[{$popularlinks_perm[link_num].id}][link_name]" type="text" value="{$popularlinks_perm[link_num].link_name}"  maxlength=40/></td>
	<td align="center" style="left-padding:30px"><input name="link_url[{$popularlinks_perm[link_num].id}][link_url]" type="text" value="{$popularlinks_perm[link_num].link_url}" /></td>
	<td align="center">
		<input type="button" value="Delete" onclick="javascript: delete_perm_link('{$popularlinks_perm[link_num].id}')" />
	</td>	
</tr>
{/section}
{else}
<tr>
<td colspan="2" align="center">No Permanent Links added as yet</td>
</tr>
{/if}
{ if $popularlinks_perm|@count < 5 }
<tr>
	<td align="center" style="left-padding:30px"><input name="link_name" type="text" value=""  maxlength=40/></td>
	<td align="center" style="left-padding:30px"><input name="link_url" type="text" value="" /></td>
	<td align="center">
		<input type="button" value="Add" onclick="javascript: add_perm_link()" />
	</td>
</tr>
{/if}	
<tr>
	<td></td>
	<td></td>
	<td></td>
	<td class="SubmitBox" align="right">
	   <input type="button" value="{$lng.lbl_upd_selected|escape}" onclick="javascript: document.updatepermlinks.mode.value = 'save'; document.updatepermlinks.submit();" />
	</td>
	<td></td>
</tr>
</table>
</form>

{/capture}
{include file="dialog.tpl" title="Permanent Links" content=$smarty.capture.dialog extra='width="100%"'}

{capture name=dialog}
<form action="popular_widget.php" method="post" name="updatenonpermlinks">
<input type="hidden" name="mode" />
<input type="hidden" name="update_link_id" />
<input type="hidden" name="is_permanent" value="0" />
<table cellpadding="3" cellspacing="1" width="100%">
<tr class="TableHead">
	<td width="15%">Link name</td>
	<td width="30%">Link</td>
	<td width="30%">Action</td>
</tr>

{if $popularlinks_nonperm}

{section name=link_num loop=$popularlinks_nonperm}
<tr{cycle values=", class='TableSubHead'"}>
		
	<td align="center" style="left-padding:30px"><input name="link_name2[{$popularlinks_nonperm[link_num].id}][link_name]" type="text" value="{$popularlinks_nonperm[link_num].link_name}" maxlength=40/></td>
	<td align="center" style="left-padding:30px"><input name="link_url2[{$popularlinks_nonperm[link_num].id}][link_url]" type="text" value="{$popularlinks_nonperm[link_num].link_url}" /></td>
	<td align="center">
		<input type="button" value="Delete" onclick="javascript: delete_nonperm_link('{$popularlinks_nonperm[link_num].id}')" />
	</td>	
</tr>
{/section}
{else}
<tr>
<td colspan="2" align="center">No Non Permanent Links added as yet</td>
</tr>
{/if}
{ if $popularlinks_nonperm|@count < 30 }
<tr>
	<td align="center" style="left-padding:30px"><input name="link_name" type="text" value=""  maxlength=40/></td>
	<td align="center" style="left-padding:30px"><input name="link_url" type="text" value="" /></td>
	<td align="center">
		<input type="button" value="Add" onclick="javascript: add_nonperm_link();" />
	</td>
</tr>
{/if}	
<tr>
	<td></td>
	<td></td>
	<td></td>
	<td class="SubmitBox" align="right">
	   <input type="submit" value="{$lng.lbl_upd_selected|escape}" onclick="javascript: document.updatenonpermlinks.mode.value = 'save'; document.updatenonpermlinks.submit();" />
	</td>
	<td></td>
</tr>

</table>
</form>

{/capture}
{include file="dialog.tpl" title="Dynamic Links" content=$smarty.capture.dialog extra='width="100%"'}
