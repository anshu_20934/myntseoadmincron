{* $Id: featured_products.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
{include file="main/include_js.tpl" src="main/popup_product.js"}

{literal}
<script type="text/javascript">
function delete_keyvaluepair(id){
    var r=confirm("Are you sure you want to delete this key value pair");
    if(r == true){
        document.updatekeyvaluepairs.mode.value = 'delete';
        document.updatekeyvaluepairs.keyvalue_id.value = id;
        document.updatekeyvaluepairs.submit();
    }
}

function add_keyvaluepair(){
    if ( document.updatekeyvaluepairs.key_field_new.value == '' || 
    	    document.updatekeyvaluepairs.value_field_new.value == '') {
        alert('Key or Value cannot be empty');
        return;
    } 
    document.updatekeyvaluepairs.mode.value = 'add';
    document.updatekeyvaluepairs.submit();
}

function check_submit() {
	var x = document.querySelectorAll("input.select-checkbox[type=checkbox]");
	var checked = [].some.call(x, function(inp){
		return inp.checked;
	});
	if(!checked) {
		alert("Select atleast one Key Value Pair to update");
		return;
	}
	document.updatekeyvaluepairs.mode.value = 'update';
	document.updatekeyvaluepairs.submit();
}

</script>
{/literal}

{capture name=dialog}
<form action="keyvaluepairs_widget.php" method="post" name="updatekeyvaluepairs">
<input type="hidden" name="mode" />
<input type="hidden" name="keyvalue_id" />
<table cellpadding="3" cellspacing="1" width="100%">
<tr class="TableHead">
	<td width="20%">Key</td>
	<td width="30%">Value</td>
	<td width="30%">Description</td>
</tr>

{if $keyvaluepairs}
{section name=key_num loop=$keyvaluepairs}
<tr>
	<td align="center" style="left-padding:30px"><input class="select-checkbox" name="update_checkbox_field[{$keyvaluepairs[key_num].id}]" type="checkbox" value="on"><input name="update_key_field[{$keyvaluepairs[key_num].id}]" type="text" value="{$keyvaluepairs[key_num].key}"></td>
	<td align="center" style="left-padding:30px"><textarea name="update_value_field[{$keyvaluepairs[key_num].id}]" >{$keyvaluepairs[key_num].value|escape:'html'} </textarea></td>
	<td align="center" style="left-padding:30px">
		<textarea name="update_description_field[{$keyvaluepairs[key_num].id}]" >{$keyvaluepairs[key_num].description} </textarea>
	</td>
	<td align="center">
		<input type="button" value="Delete" onclick="javascript: delete_keyvaluepair('{$keyvaluepairs[key_num].id}')" />
	</td>	
</tr>
{/section}
{else}
<tr>
<td colspan="2" align="center">No Key Value Pairs added as yet</td>
</tr>
{/if}

<tr>
	<td align="center" style="left-padding:30px"><input name="key_field_new" type="text" value=""></td>
	<td align="center" style="left-padding:30px"><textarea name="value_field_new"></textarea></td>
	<td align="center" style="left-padding:30px"><textarea name="description_field_new"></textarea></td>
	<td align="center">
		<input type="button" value="Add" onclick="javascript: add_keyvaluepair()" />
	</td>
</tr>
<tr>
	<td></td>
	<td></td>
	<td></td>
	<td class="SubmitBox" align="right">
	   <input type="button" value="{$lng.lbl_upd_selected|escape}" onclick="javascript: check_submit()" />
	</td>
	<td></td>
</tr>
</table>
</form>

{/capture}
{include file="dialog.tpl" title="Key-Value Pairs" content=$smarty.capture.dialog extra='width="100%"'}