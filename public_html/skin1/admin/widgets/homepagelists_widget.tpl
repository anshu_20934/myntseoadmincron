{* $Id: featured_products.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
{include file="main/include_js.tpl" src="main/popup_product.js"}
{include file="admin/widgets/header.tpl"}

{capture name=dialog}
{literal}
<style>
    .show { display:block;}
    .hide { display:none;}
</style>
<script type="text/javascript">
function add_list_submit(){
	var list_name = document.update.widget_name.value ;
	var list_type = document.update.widget_type.value ;
	document.update.mode.value = 'add_list'; 
	if( list_name == '' || list_type == -1 ){
		alert('List name or type not entered');
		return;
	} 
	if( list_type == "tag" ) {
		
		if ( document.update.widget_tag.value == '' || document.update.widget_limit.value == -1 ){
			alert ('Tag name or limit not entered for list ');
			return;
		}
	}

	if (list_type == "query") {
		if ( document.update.widget_query.value == ''){
			alert ('Query not specified.');
			return;
		}
	}
	document.update.submit();
}

function delete_list(widget_id){
    var r=confirm("Are you sure you want to delete this list");
    if(r == true){
        document.update.mode.value = 'delete_list';
        document.update.widget_id.value = widget_id; 
        document.update.submit();
    }
}
function add_sku(widget_id){
    var sku = document.update['add_sku['+widget_id+']'];
    if( !sku || sku == '' || sku.value == '' ){
        alert('No sku id entered for adding');
        return;
    }
    document.update.mode.value = 'add_sku';  
    document.update.widget_id.value = widget_id;
    document.update.skuid.value = sku.value;
    document.update.submit();
}

function delete_sku(widget_id, skuid){
    var r=confirm("Are you sure you want to delete this sku from list");
    if(r == true){
        document.update.mode.value = 'delete_sku';  
        document.update.widget_id.value = widget_id; 
        document.update.skuid.value = skuid; 
        document.update.submit();
    }
}

function select_widget_type(){
	document.getElementById('widget_tag_data').className ='hide';
	document.getElementById('widget_query_data').className ='hide';
	
    if(document.update.widget_type.value == 'tag'){
        document.getElementById('widget_tag_data').className ='show';
        
    } else if(document.update.widget_type.value == 'query'){
        document.getElementById('widget_query_data').className ='show'
    }
}
</script>
{/literal}
<form action="homepagelists_widget.php" method="post" name="update">
<input type="hidden" name="mode" />
<input type="hidden" name="widget_id" />
<input type="hidden" name="skuid" />
<table cellpadding="3" cellspacing="1" width="100%">
<tr class="TableHead">
	<td width="5%">S No</td>
	<td width="60%">List</td>
	<td width="35%">Action</td>
</tr>

{if $data}
	{foreach name=lists item=list from=$data}
		<tr class='TableSubHead'>
				
			<td>{$smarty.foreach.lists.iteration}</td>
			<td><div style="float:left; width:100px">List Name</div><input name="list[{$list.widget_id}][list_name]" type="text" value="{$list.widget_name}" size="50" /></td>
			<td align="center">
				<input type="submit" value="Up" onclick="javascript: document.update.mode.value = 'move_list_up';  document.update.widget_id.value = '{$list.widget_id}'; document.update.submit();" />
				<input type="submit" value="Down" onclick="javascript: document.update.mode.value = 'move_list_down';  document.update.widget_id.value = '{$list.widget_id}'; document.update.submit();" />
				<input type="button" value="Delete" onclick="javascript: delete_list('{$list.widget_id}')" />
			</td>	
		</tr>
		{if $list.widget_type == "sku"}
			{foreach name=skus item=sku from=$list.skus}
				<tr class='TableSubHead'}>
					<td align="center"></td>
					<td style=""><div style="float:left; width:100px">SKU ID</div><input name="list[{$list.widget_id}][skus][{$sku.skuid}]" type="text" value="{$sku.sku}" size="50" /></td>
					<td align="center">
						<input type="submit" value="Up" onclick="javascript: document.update.mode.value = 'move_sku_up';  document.update.widget_id.value = '{$list.widget_id}'; document.update.skuid.value = '{$sku.skuid}'; document.update.submit();" />
						<input type="submit" value="Down" onclick="javascript: document.update.mode.value = 'move_sku_down';  document.update.widget_id.value = '{$list.widget_id}'; document.update.skuid.value = '{$sku.skuid}'; document.update.submit();" />
						<input type="button" value="Delete" onclick="javascript: delete_sku('{$list.widget_id}', '{$sku.skuid}');"/>
					</td>	
				</tr>
			{/foreach}
				<tr class="TableSubHead">
					<td>Add an SKU to list</td>
					<td><div style="float:left; width:100px">SKU ID</div><input name="add_sku[{$list.widget_id}]" type="text" value="" size="50" /></td>
					<td align="center">
						<input type="button" value="Add" onclick="javascript: add_sku('{$list.widget_id}');" />
					</td>
				</tr>
		{elseif $list.widget_type == "tag"}
			<tr class='TableSubHead'}>
				<td align="center"></td>
				<td style=""><div style="float:left; width:100px">Tag Name</div><input name="list[{$list.widget_id}][tag][{$list.tagid}][tag]" type="text" value="{$list.tag}" size="50" /></td>
				<td></td>
			</tr>
			<tr class='TableSubHead'}>
				<td align="center"></td>
				<td style=""><div style="float:left; width:100px">Limit</div>
					<select name="list[{$list.widget_id}][tag][{$list.tagid}][limit]" type="text" value="{$list.limit}">
						<option value="-1">--Select List Limit--</option>
						<option value="5" {if $list.limit == "5"} selected="selected"{/if}>5</option>
						<option value="10" {if $list.limit == "10"} selected="selected"{/if}>10</option>
						<option value="15" {if $list.limit == "15"} selected="selected"{/if}>15</option>
						<option value="20" {if $list.limit == "20"} selected="selected"{/if}>20</option>
						<option value="25" {if $list.limit == "25"} selected="selected"{/if}>25</option>
						<option value="30" {if $list.limit == "30"} selected="selected"{/if}>30</option>
					</select>
				</td>
				<td></td>
			</tr>	
		{elseif $list.widget_type == "query"}
			<tr class='TableSubHead'}>
				<td align="center"></td>
				<td style=""><div style="float:left; width:100px">Query</div>
				
				<textarea name="list[{$list.widget_id}][query]" type="text" rows="4" cols="50" />{$list.query}</textarea>
				</td>
				<td></td>
			</tr>
		{/if}
	{/foreach}
{/if}
<tr>
	<td></td>
	<td></td>
	<td class="SubmitBox" align="right">
	   <input type="button" value="Save" onclick="javascript: document.update.mode.value = 'save'; document.update.submit();" />
	</td>
	<td></td>
</tr>
<tr class='TableSubHead'>
	<td colspan="2"><b>Enter data for new list</b></td>
	<td></td>
</tr>	
<tr class='TableSubHead'>
	<td>List name <input name="widget_name" value="" /></td>
	<td></td>
	<td></td>
</tr>	
<tr class='TableSubHead'>
	<td>
		<select name="widget_type" onchange="select_widget_type();">
			<option value="-1">--Select List Type--</option>
			<option value="tag">Tag</option>
			<option value="sku">SKU List</option>
			<option value="query">Query</option>
		</select>
		
		<div id="widget_tag_data" class="hide">
			<div>Tag Name<input name="widget_tag" value="" /></div>
			<div>
				<select name="widget_limit">
					<option value="-1">--Select List Limit--</option>
					<option value="5">5</option>
					<option value="10">10</option>
					<option value="15">15</option>
					<option value="20">20</option>
					<option value="25">25</option>
					<option value="30">30</option>
				</select>
			</div>
		</div>
		<div id="widget_query_data" class="hide">
			<div>Query<textarea name="widget_query" rows="5" cols="50"></textarea></div>
		</div>
	</td>
	<td></td>
	<td></td>
</tr>
<tr class='TableSubHead'>
	<td align="center">
		<input type="button" value="Add List" onclick="javascript: add_list_submit();" />
	</td>
	<td></td>
	<td></td>
</tr>
</table>
</form>

{/capture}
{include file="dialog.tpl" title="Home Page Lists" content=$smarty.capture.dialog extra='width="100%"'}
