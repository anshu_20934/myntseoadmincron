{* $Id: featured_products.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
{include file="main/include_js.tpl" src="main/popup_product.js"}
{include file="admin/widgets/header.tpl"}

{literal}
<style>
    .lblleft {float:left; width:100px;}
    .smllblleft {float:left; width:50px;}
</style>
<script type="text/javascript">
function link_delete(id, parent_id){
    var r=confirm("Are you sure you want to delete this item");
    if( r == true ){
        document.update.mode.value = 'link_delete';
        document.update.move_link_id.value = id;
        document.update.move_link_parent_id.value = parent_id;
        document.update.submit();
    }
}

function add_link(link_name, link_url, parent_id){
    document.update.mode.value = 'add_link';
    document.update.parent_id.value = parent_id ; 
    document.update.link_name.value = link_name;  
    document.update.link_url.value = link_url; 
    document.update.submit();

}
function add_list(){
    var link_name = document.update['add_link[link_name]'].value ;
    var link_url = document.update['add_link[link_url]'].value ;
    if ( !link_name || link_name == '' || !link_url || link_url == '' ){
        alert('Link name or Link URL is empty');
        return;
    }
    add_link(link_name, link_url, '-1');
}

function add_header(topnavid){
    var link_name = document.update['add_link[' + topnavid + '][link_name]'].value ;
    var link_url = document.update['add_link[' + topnavid + '][link_url]'].value ;
    alert('adding sub header with link_name= ' + link_name + ' link_url= ' + link_url + ' topnavid= ' + topnavid );
    if ( !link_name || link_name == '' || !link_url || link_url == '' ){
        alert('Link name or Link URL is empty');
        return;
    }
    add_link(link_name, link_url, topnavid);
}

function add_sub_header(topnavid, headerid){
    var link_name = document.update['add_link[' + topnavid + '][' + headerid + '][link_name]'].value ;
    var link_url = document.update['add_link[' + topnavid + '][' + headerid + '][link_url]'].value ;
    alert('adding sub header with link_name= ' + link_name + ' link_url= ' + link_url + ' topnavid= ' + topnavid + ' headerid= ' + headerid);
    if ( !link_name || link_name == '' || !link_url || link_url == '' ){
        alert('Link name or Link URL is empty');
        return;
    }
    add_link(link_name, link_url, headerid);
    
}

</script>
{/literal}


{capture name=dialog}
<form action="topnavigation_widget_v2.php" method="post" name="update">
<input type="hidden" name="test" />
<input type="hidden" name="mode" />
<input type="hidden" name="parent_id" />
<input type="hidden" name="link_name" />
<input type="hidden" name="link_url" />
<input type="hidden" name="display_order" />
<input type="hidden" name="move_link_id" />
<input type="hidden" name="move_link_parent_id" />

<table cellpadding="3" cellspacing="1" width="100%">
<tr class="TableHead">
</tr>

{if $data}

{foreach name=topnavs item=topnav from=$data}
<tr style="background-color:grey">
    <td> {$smarty.foreach.topnavs.iteration} </td>
    <td>Link Name : <input type="text" name="link_names[{$topnav.id}][link_name]" value="{$topnav.link_name}" /></td>
    <td>URL : <input type="text" name="link_urls[{$topnav.id}][link_url]" value="{$topnav.link_url}" /></td>
    <td align="center">
        <input type="button" value="Up" onclick="javascript: document.update.mode.value = 'move_link_up';  document.update.move_link_id.value = '{$topnav.id}'; document.update.move_link_parent_id.value = '-1'; document.update.submit();" />
        <input type="button" value="Down" onclick="javascript: document.update.mode.value = 'move_link_down';  document.update.move_link_id.value = '{$topnav.id}'; document.update.move_link_parent_id.value = '-1'; document.update.submit();" />
        <input type="button" value="Delete" onclick="javascript: link_delete('{$topnav.id}', '-1'); " />
    </td>    
</tr>
    {foreach name=headers item=header from=$topnav.child}
    <tr style="background-color:#D6D7D9">
        <td></td>
        <td>
            <div class="smllblleft">&nbsp;</div>
            <div class="lblleft">Header Link Name : </div>
            <input type="text" name="link_names[{$header.id}][link_name]" value="{$header.link_name}" />
        </td>
        <td>URL : <input type="text" name="link_urls[{$header.id}][link_url]" value="{$header.link_url}" /></td> 
        <td align="center">
            <input type="button" value="Up" onclick="javascript: document.update.mode.value = 'move_link_up';  document.update.move_link_id.value = '{$header.id}'; document.update.move_link_parent_id.value = '{$topnav.id}'; document.update.submit();" />
            <input type="button" value="Down" onclick="javascript: document.update.mode.value = 'move_link_down';  document.update.move_link_id.value = '{$header.id}'; document.update.move_link_parent_id.value = '{$topnav.id}'; document.update.submit();" />
            <input type="button" value="Delete" onclick="javascript: link_delete('{$header.id}', '{$topnav.id}');" />
        </td>    
    </tr>
        {foreach name=links item=link from=$header.child}
    <tr style="background-color:#EEEDDD">
        <td></td>
        <td>
            <div class="lblleft">&nbsp;</div>
            <div class="lblleft">Link Name : </div>
            <input type="text" name="link_names[{$link.id}][link_name]" value="{$link.link_name}" />
        </td>
        <td>URL : <input type="text" name="link_urls[{$link.id}][link_url]" value="{$link.link_url}" /></td> 
        <td align="center">
            <input type="button" value="Up" onclick="javascript: document.update.mode.value = 'move_link_up';  document.update.move_link_id.value = '{$link.id}'; document.update.move_link_parent_id.value = '{$header.id}'; document.update.submit();" />
            <input type="button" value="Down" onclick="javascript: document.update.mode.value = 'move_link_down';  document.update.move_link_id.value = '{$link.id}'; document.update.move_link_parent_id.value = '{$header.id}'; document.update.submit();" />
            <input type="button" value="Delete" onclick="javascript: link_delete('{$link.id}', '{$header.id}');" />
        </td>    
    </tr>
        {/foreach}
    <tr>
        <td></td>
        <td>
            <div class="lblleft">&nbsp;</div>
            <div class="lblleft">Link Name : </div>
            <input name="add_link[{$topnav.id}][{$header.id}][link_name]" type="text" value="" />
        </td>
        <td>URL : <input id="add_link[{$topnav.id}][{$header.id}][link_url]" type="text" value="" /></td> 
        <td align="center">
            <input type="button" value="Add Sub Header" onclick="javascript: add_sub_header('{$topnav.id}', '{$header.id}'); " />
        </td>
    </tr>
    {/foreach}
    <tr>
        <td></td>
        <td>            
            <div class="smllblleft">&nbsp;</div>
            <div class="lblleft">Header Link Name : </div>
            <input name="add_link[{$topnav.id}][link_name]" type="text" value="" />
        </td>
        <td>URL : <input id="add_link[{$topnav.id}][link_url]" type="text" value="" /></td> 
        <td align="center">
            <input type="button" value="Add Header" onclick="javascript: add_header('{$topnav.id}');" />
        </td>
    </tr>
{/foreach}
{else}
<tr>
<td colspan="2" align="center">No nav links added</td>
</tr>
{/if}
<tr style="height:50px"></tr>
<tr>
    <td align="center">
        <input type="button" value="Add Link" onclick="javascript: add_list();" />
    </td>
    <td>Link Name : <input name="add_link[link_name]" type="text" value="" /></td>
    <td>URL : <input id="add_link[link_url]" type="text" value="" /></td> 
</tr>
<tr>
    <td></td>
    <td></td>
    <td></td>
    <td class="SubmitBox" align="right">
       <input type="button" value="Update All" onclick="javascript: document.update.mode.value = 'save'; document.update.submit();" />
    </td>
    <td></td>
</tr>
</table>
</form>

{/capture}
{include file="dialog.tpl" title="Top Navigation" content=$smarty.capture.dialog extra='width="100%"'}
