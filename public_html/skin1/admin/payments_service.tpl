{* $Id: menu.tpl,v 1.68.2.2 2006/06/20 05:52:05 svowl Exp $ *}

{capture name=menu}
<li><a href="{$catalogs.admin}/netbanking_service/pmt_activate_gateway.php" class="VertMenuItems">Edit NetBanking Options</a></li>
<li><a href="{$catalogs.admin}/netbanking_service/pmt_bank_list.php" class="VertMenuItems">Add/Edit NetBanking Bank</a></li>
<li><a href="{$catalogs.admin}/netbanking_service/pmt_add_gateway.php" class="VertMenuItems">Add/Edit NetBanking Gateway</a></li>
<li><a href="{$catalogs.admin}/payment_feature_gate.php?mode=view&object=keyvaluepairs" class="VertMenuItems">Payment Feature Gate</a></li>
<li><a href="{$catalogs.admin}/payment_logs.php" class="VertMenuItems">Payment Logs</a></li>
{/capture}
{include file="menu.tpl" dingbats="dingbats_categorie.gif" menu_title='Payments Service' menu_content=$smarty.capture.menu }
