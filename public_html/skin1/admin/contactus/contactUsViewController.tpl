{* $Id: featured_products.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
{capture name=dialog}       
	<table>
		<tr>
			<td align="left">
				<input type="button" name="Add New Issue" value="Add New Contact-Us Issue" class="button" onClick="javascript:window.open(http_loc+'/admin/contactus/contactUsDataController.php?mode=addContactUsIssue','null', 'menubar=0,resizable=1,toolbar=0,width=800,height=500')">						
			</td>
		</tr>
	</table>
    <br/>
    <hr>
    
    <div style="width:1125px;overflow:scroll;height:500px;">
        <table cellpadding="2" cellspacing="1" width="100%" title="scroll horizontally to view all columns" id="SRListTable">

            <tr class="TableHead">                
                <th style="padding: 5px;">
                    Issue Level_1
                </th>
                
                <th style="padding: 5px;">
                    Issue Level_2
                </th>

                <th style="padding: 5px;">
                    Issue Level_3
                </th>

                <th style="padding: 5px;">
                    Support Email
                </th>

                <th style="padding: 5px;">
                    Action
                </th>
               
                <th style="padding: 5px;">
                    Active
                </th>

                <th style="padding: 5px;">
                    <table width="100%">
                        <tr><th colspan="4">Fields</th></tr>
                        <tr>
                            <td width="10%">Field ID</td>
                            <td width="30%">Name</td>
                            <td width="30%">Type</td>
                            <td width="10%">maxchar</td>
                            <td width="10%">Action</td>
                            <td width="10%">Active</td></tr>
                    </table>
                </th>
             
                <th style="padding: 5px;">Action</th>
            </tr>

            {foreach item=index from=$issue_detail name=issue}
                <tr {cycle values=", class='TableSubHead'"}>
                    <td style="padding: 5px;">{$index.issue_level_1}</td>
                    <td style="padding: 5px;">{$index.issue_level_2}</td>
                    <td style="padding: 5px;">{$index.issue_level_3}</td>
                    <td style="padding: 5px;">{$index.support_email}</td>
                    <td style="padding: 5px;">{$index.action}</td>
                    <td style="padding: 5px;">{if $index.is_active eq 1}Active{elseif $index.is_active eq '0'}In-Active{/if}</td>
                    <td style="padding: 5px;">

                        <table width="100%">
                            {foreach item=field from=$index.field}
                                <tr>
                                    <td width="10%">{$field.issue_field_id}</td>
                                    <td width="30%">{$field.field_label}</td>
                                    <td width="30%">{$field.field_type}</td>
                                    <td width="10%">{$field.maxchar}</td>
                                    <td width="10%">{$field.action}</td>
                                    <td width="10%">{if $field.is_active eq 1}Active{elseif $field.is_active eq '0'}In-Active{/if}</td>
                                </tr>
                            {/foreach}
                        </table>
                    </td>

                    <td style="padding: 5px;">
						<a href="javascript:void(0);"  onClick="javascript:windowOpener('{$http_location}/admin/contactus/contactUsDataController.php?contactus_issue_id={$index.issue_id}&mode=editContactUsIssue','{$index.issue_id}', 'menubar=0,resizable=1,toolbar=0,width=800,height=500');">Edit</a>						                                         
                    </td>
                </tr>
            {foreachelse}
                <tr>
                    <td colspan="12" align="center">No data is available!</td>
                </tr>
            {/foreach}
        </table>
    </div>
{/capture}
{include file="dialog.tpl" title="Contact-Us Issues" content=$smarty.capture.dialog extra='width="100%"'}