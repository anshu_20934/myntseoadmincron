<html>
<head>
{ include file="meta.tpl" }
{literal}
    <script type="text/javascript">
        var otherIssue = /^(other|others)$/i;

        $(document).ready(function(){

            // create fields
            $("#add_field_button").click(function(){
                createDynamicIssueField("add");
            });

            $("#edit_field_button").click(function(){
                createDynamicIssueField("edit");
            });


            // remove fields
            $("input[name='remove_field']").live('click',function(){
                $(this).parents('table.dynamic').remove();
            });

            // mark an issue as 'other'
            $("input[name='mark_as_other_button']").toggle( markField,unMarkField );


            $("input[name='add_issue']").blur(function(){            
                if(validateAgainstRegex(otherIssue, $(this).val())){

                    markField();
                }
            });

            // handle 'other' in edit form
            $("input[name='edit_issue']").blur(function(){
                if(validateAgainstRegex(otherIssue, $(this).val())){                
                    alert("The issue can not be changed to 'Other', please add a new issue as 'Other'!");
                    $(this).focus();
                }
            });


            //validate add form
            $("#add_issue_button").click(function(){
                var flag = validateIssueForm('add');
    
                if(flag){
                    $("form[name='add_form']").submit();
                }
            });

            //validate edit form
            $("#edit_issue_button").click(function(){
                var flag = validateIssueForm('edit');

                if(flag){
                    $("form[name='edit_form']").submit();
                }
            });

            /*
             * create issue fields dynamically
             */
            function createDynamicIssueField(prefix){
                var DHTML = '<table class="dynamic"><tr><td>Field Name&nbsp;<input type="text" name="field_name[]" maxlength="100"/></td><td>Type&nbsp;<select name="field_type[]"><option value="mandatory">mandatory</option><option value="optional" selected="selected">optional</option></select></td><td>Max char&nbsp;<input type="text" name="maxchar[]" maxlength="3" style="width:25px"/></td><td>Action&nbsp;<input type="text" name="field_action[]" maxlength="250" style="width:200px"/></td><td>Active&nbsp;<input type="checkbox" name="is_active_field[]" checked="checked" value="1"/></td><td><input type="button" name="remove_field" value=" -- "></td></tr></table>';

                // append() used instead of html() because append does not clear the DOM value
                $("#"+prefix+"_field_container").append(DHTML);
            }

            /*
             * mark an issue as 'other'
             */
            function markField(){
                $("#add_field_container").hide();
                $("#add_field_button_container").hide();

                $("input[name='add_issue']").val('Other').attr('readonly',true);
                $("input[name='mark_as_other_button']").val('Unmark');

            }

            /*
             * unmark an issue from 'other'
             */
            function unMarkField(){
                $("#add_field_container").show();
                $("#add_field_button_container").show();
                $("input[name='add_issue']").val('').attr('readonly',false);
                $("input[name='mark_as_other_button']").val("Mark as 'Other'");
            }

            /*
             * validates add, edit contactus issue form
             */
            function validateIssueForm(prefix){
                return true;
            }

            function validateAgainstRegex(regEx, data){
                return regEx.test(data);
            }
        });        
    </script>

    <style type="text/css">
        table{width:100%;}
    </style>
{/literal}
</head>

<body id="body">
<div id="scroll_wrapper"><div id="container"><div id="wrapper"><div id="display"><div class="center_column">
    <div class="print">
        <div class="super" ><p>&nbsp;</p></div>
        {if $form_type eq 'addContactUsIssue'}
            {if $success ne "Y"}
                <div class="head" style="margin:0px 0px 20px;">
                    <p>Add Contact-Us Issue</p>
                    <p style="color:red">{$message}</p>
                </div>
                
                <form method="post" name="add_form"  action="{$http_location}/admin/contactus/contactUsDataController.php">
                    <input type="hidden" name="mode" value="{$form_type}"/>
                    <input type="hidden" name="post_mode" value="1"/>
                    
                    <table>
                        <tr>
                            <td width="200px">Issue Level 1</td>
                            <td>
                                <select name="issue_level_1">
                                    <option value="" selected="selected">--select level 1--</option>
                                    {section name=index loop=$issue_level_1}
                                        {if $issue_level_1[index].issue_level_1 ne 'Other' && $issue_level_1[index].issue_level_1 ne 'Others'}
                                            {if $issue_level_1[index].is_active eq 1}
                                                {assign var=label value='(active)'}
                                            {else}
                                                {assign var=label value='(in-active)'}
                                            {/if}
                                            <option value="{$issue_level_1[index].issue_level_1}">{$issue_level_1[index].issue_level_1} - {$label}</option>
                                        {/if}
                                    {/section}
                                </select>
                            </td>
                        </tr>

                        <tr>
                            <td>Issue Level 2</td>
                            <td>
                                <select name="issue_level_2">
                                    <option value="" selected="selected">--select level 2--</option>
                                    {section name=index loop=$issue_level_2}
                                        {if $issue_level_2[index].issue_level_2 ne 'Other' && $issue_level_2[index].issue_level_2 ne 'Others'}
                                            {if $issue_level_2[index].is_active eq 1}
                                                {assign var=label value='(active)'}
                                            {else}
                                                {assign var=label value='(in-active)'}
                                            {/if}
                                            <option value="{$issue_level_2[index].issue_level_2}">{$issue_level_2[index].issue_level_1} - {$issue_level_2[index].issue_level_2} - {$label}</option>
                                        {/if}
                                    {/section}
                                </select>
                            </td>
                        </tr>
                       
                        <tr>
                            <td>New Issue<span style="color:red;">&nbsp;*</span></td>
                            <td>
                                <input type="text" name="add_issue" value="{$smarty.post.add_issue}" maxlength="200" >&nbsp;
                                <input type="button" name="mark_as_other_button" value="Mark as 'Other'" >
                            </td>
                        </tr>

                        <tr>
                            <td>Support Email</td>
                            <td>
                                <input type="text" name="support_email" value="{$smarty.post.support_email}" maxlength="250" >                                
                            </td>
                        </tr>

                        <tr>
                            <td>Action</td>
                            <td>
                                <input type="text" name="issue_action" value="{$smarty.post.issue_action}" maxlength="250" >                                
                            </td>
                        </tr>

                        <tr>
                            <td>Active<span style="color:red;">&nbsp;*</span></td>
                            <td>
                                <input type="checkbox" name="is_active_issue" checked="checked" value="1">                                
                            </td>
                        </tr>

                        <tr>
                            <td colspan="2" id="add_field_button_container">
                                <hr>
                                Add Fields&nbsp;
                                <input type="button" name="add_field" id="add_field_button" value=" + ">
                            </td>
                        </tr>

                        <tr>
                            <td colspan="2" id="add_field_container">                                                        
                            </td>                            
                        </tr>

                        <tr>
                            <td>&nbsp;</td>
                            <td><br/><input type="button" value="Add Issue" id="add_issue_button" class="button" >&nbsp;<input type="button" value="Cancel" class="button" onclick="window.close();"></td>
                        </tr>
                    </table>
                </form>

            {elseif $success eq "Y"}
                <table>
                    <tr>
                        <td>Issue added succesfully!</td>
                        <td>&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;</td>
                    </tr>
                    <tr><td style="font-size:.8em;color:red;"><b>Please Click the close button below to refresh data on back screen</b></td></tr>
                    <tr><td align="center"><input type="button" style="height:20px;width:50px; background-color:black; color:white;" value="Close" onclick="opener.window.location.reload();self.close()"></td></tr>
                </table>
            {/if}

        {elseif $form_type eq 'editContactUsIssue'}

            {if $success ne "Y"}
                <div class="head" style="margin:0px 0px 20px;">
                    <p>Edit Contact-Us Issue</p>
                    <p style="color:red;">{$message}</p>
                </div>
                <div class="links"><p></p></div>
                <div class="foot"></div>
                <form method="post" name="edit_form"  action="{$http_location}/admin/contactus/contactUsDataController.php">
                    <input type="hidden" name="mode" value="{$form_type}"/>
                    <input type="hidden" name="post_mode" value="1"/>
                    <input type="hidden" name="contactus_issue_id" value="{$contactus_issue_id}"/>

                    <table>
                        <tr>
                            <td width="200px">Issue<span style="color:red;">&nbsp;*</span></td>
                            <td>
                                {if $issue_level eq "level_1"}
                                    {assign var=issue_name value=$issue_detail.issue_level_1}
                                    <input type="text" name="edit_issue" value="{if $issue_detail.issue_level_1}{$issue_detail.issue_level_1}{else}{$smarty.post.edit_issue}{/if}" maxlength="200" >
                                {elseif $issue_level eq "level_2"}
                                    {assign var=issue_name value=$issue_detail.issue_level_2}
                                    <input type="text" name="edit_issue" value="{if $issue_detail.issue_level_2}{$issue_detail.issue_level_2}{else}{$smarty.post.edit_issue}{/if}" maxlength="200" >
                                {elseif $issue_level eq "level_3"}
                                    {assign var=issue_name value=$issue_detail.issue_level_3}
                                    <input type="text" name="edit_issue" value="{if $issue_detail.issue_level_3}{$issue_detail.issue_level_3}{else}{$smarty.post.edit_issue}{/if}" maxlength="200" >
                                {/if}                         
                            </td>
                        </tr>

                        <tr>
                            <td>Support Email</td>
                            <td>
                                <input type="text" name="support_email" {if $issue_detail.support_email}{$issue_detail.support_email}{else}{$smarty.post.support_email}{/if} value="{$issue_detail.support_email}">
                            </td>
                        </tr>

                        <tr>
                            <td>Action</td>
                            <td>
                                <input type="text" name="issue_action" {if $issue_detail.action}{$issue_detail.action}{else}{$smarty.post.action}{/if} value="{$issue_detail.action}">
                            </td>
                        </tr>

                        <tr>
                            <td>Active<span style="color:red;">&nbsp;*</span></td>
                            <td>
                                <input type="checkbox" name="is_active_issue" {if $issue_detail.is_active eq 1}checked="checked"{/if} value="{$issue_detail.is_active}">
                            </td>
                        </tr>

                        <tr {if $issue_name eq 'Other' || $issue_name eq 'Others'}style="display:none;"{/if}>
                            <td colspan="2">
                                <hr>
                                Add Fields&nbsp;
                                <input type="button" name="edit_field" id="edit_field_button" value=" + ">
                            </td>
                        </tr>

                        <tr>
                            <td colspan="2" id="edit_field_container">
                                {if $issue_field_detail|@count > 0}
                                    {section name=index loop=$issue_field_detail}
                                        <table class="dynamic">
                                            <tr>
                                                <td>ID&nbsp;:&nbsp;{$issue_field_detail[index].issue_field_id}</td>
                                                <td>Field Name&nbsp;
                                                    <input type="text" name="field_name_edit[]" value="{$issue_field_detail[index].field_label}" maxlength="100"/>
                                                    <input type="hidden" name="field_id_edit[]" value="{$issue_field_detail[index].issue_field_id}"/>
                                                </td>
                                                <td>Type&nbsp;
                                                    <select name="field_type_edit[]">
                                                    {if $issue_field_detail[index].field_type eq 'mandatory'}
                                                        <option value="mandatory" selected="selected">mandatory</option>
                                                        <option value="optional">optional</option>
                                                    {else}
                                                        <option value="mandatory">mandatory</option>
                                                        <option value="optional" selected="selected">optional</option>
                                                    {/if}
                                                    </select>

                                                </td>
                                                <td>Max char&nbsp;<input type="text" name="maxchar_edit[]" value="{$issue_field_detail[index].maxchar}" maxlength="3" style="width:25px"/></td>
                                                <td>Action&nbsp;<input type="text" name="field_action_edit[]" value="{$issue_field_detail[index].action}" maxlength="250" style="width:200px"/></td>
                                                <td>Active&nbsp;<input type="checkbox" name="is_active_field_edit[]" {if $issue_field_detail[index].is_active eq 1}checked="checked"{/if} value="{$issue_field_detail[index].is_active}"/></td>
                                                <td>&nbsp;</td>
                                            </tr>
                                        </table>
                                    {/section}
                                {/if}
                            </td>
                        </tr>
                        

                        <tr>
                            <td>&nbsp;</td>
                            <td><br/><input type="button" value="Update Issue" id="edit_issue_button" class="button" >&nbsp;<input type="button" value="Cancel" class="button" onclick="window.close();"></td>
                        </tr>
                    </table>
                </form>

            {elseif $success eq "Y"}
                <table>
                    <tr>
                        <td>Issue updated succesfully!</td>
                        <td>&nbsp;&nbsp;</td>
                        <td>&nbsp;&nbsp;</td>
                    </tr>
                    <tr><td style="font-size:.8em;color:red;"><b>Please Click the close button below to refresh data on back screen</b></td></tr>
                    <tr><td align="center"><input type="button" style="height:20px;width:50px; background-color:black; color:white;" value="Close" onclick="opener.window.location.reload();self.close()"></td></tr>
                </table>
            {/if}        
        {/if}
    </div>
</div></div></div></div></div>
</body>
</html>