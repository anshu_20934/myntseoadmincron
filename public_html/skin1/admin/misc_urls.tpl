{* $Id: menu.tpl,v 1.68.2.2 2006/06/20 05:52:05 svowl Exp $ *}
{capture name=menu}
<li><a href="{$catalogs.admin}/orders.php" class="VertMenuItems">Order Search</a></li>
<li><a href="{$catalogs.admin}/cod_order_report.php" class="VertMenuItems">COD Reports</a></li>
<li><a href="{$catalogs.admin}/users.php" class="VertMenuItems">{$lng.lbl_users}</a></li>
<li><a href="{$catalogs.admin}/cancelordersinbulk.php" class="VertMenuItems">Cancel Orders in bulk</a></li>
<li><a href="{$catalogs.admin}/cancellation_history.php" class="VertMenuItems">Cancellation History</a></li>
<li><a href="{$catalogs.admin}/addfilters.php" class="VertMenuItems">Add Filters</a></li>
<li><a href="{$catalogs.admin}/catalog/display_categories.php" class="VertMenuItems">Display Categories</a></li>
{/capture}
{include file="menu.tpl" dingbats="dingbats_categorie.gif" menu_title='Other Useful URLs' menu_content=$smarty.capture.menu }