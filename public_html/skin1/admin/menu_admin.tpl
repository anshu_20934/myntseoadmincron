{* $Id: menu_admin.tpl,v 1.34 2005/11/17 06:55:36 max Exp $ *}
{capture name=menu}
<li><a href="{$catalogs.admin}/admin_product_types.php" class="VertMenuItems">{$lng.lbl_producttypes}</a></li>
<li><a href="{$catalogs.admin}/admin_product_styles.php" class="VertMenuItems">{$lng.lbl_productstyles}</a></li>
<!-- Inventory management -->
<!-- <a href="{$catalogs.admin}/admin_style_inventory.php" class="VertMenuItems">Style Inventory</a><br /> -->
<!--<a href="{$catalogs.admin}/admin_offline_orders.php" class="VertMenuItems">Offline Orders Inventory Management</a><br /> -->
<!-- order rejected -->
<!--<a href="{$catalogs.admin}/admin_consumables.php" class="VertMenuItems">Cosumables Items</a><br /> -->
<!-- community admin menu -->
<li><a href="{$catalogs.admin}/adminaffiliates.php" class="VertMenuItems">{$lng.lbl_affiliate}</a></li>
<li><a href="{$catalogs.admin}/myntralogs.php" class="VertMenuItems">Myntra Logs</a></li>
<li><a href="{$catalogs.admin}/editmyntraloglevel.php" class="VertMenuItems">Edit Myntra Log Level</a></li>
{/capture}
{include file="menu.tpl" dingbats="dingbats_categorie.gif" menu_title=$lng.lbl_administration menu_content=$smarty.capture.menu }