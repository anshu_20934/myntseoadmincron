{* $Id: featured_products.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
{include file="main/include_js.tpl" src="main/popup_product.js"}
{include file="admin/widgets/header.tpl"}

{capture name=dialog}
<div class="notification-message">{$message}</div>
{foreach from=$errorMessages item=errorMessage}
<div class="notification-message">{$errorMessage}</div>
{/foreach}
{literal}
  <script language="Javascript">
     var url = '{/literal}{$url_open}{literal}';
     if (url != 'blank')
     {
    	 showUrl();
     }
	function addRule()
	{
        if($("#landing-page-type").val() == 'NA'){
            alert("Please select an option for landing page type");
            return false;
        }

        if($("#landing-page-expiry").val() == ''){
            alert("Please select an expiry date");
            return false;
        }

        var date_obj1 = new Date($("#landing-page-expiry").val());
        var date_obj2 = new Date();

        if(date_obj2 >= date_obj1){
            alert("Expiry date can't be less than todays date!");
            return false;
        }

        var one_day = 24 * 60 * 60 * 1000;
        var diff_days = Math.round(Math.abs((date_obj1.getTime() - date_obj2.getTime())/(one_day)));
        var expiry_days = $("#landing-page-type option:selected").data('expiry-days');

        if(diff_days >= expiry_days){
            var alert_msg = "Landing page type - ";
            var lp_type = $("#landing-page-type").val();

            alert_msg = alert_msg.concat(lp_type);
            alert_msg = alert_msg.concat(" expiry date can be at max ");
            alert_msg = alert_msg.concat(expiry_days);
            alert_msg = alert_msg.concat(" days from now");

            alert(alert_msg);
            return false;
        }

		document.frmG.submit();
	}
	function showUrl()
	{
		var a = '{/literal}{$url_open}{literal}';
		var b = window.location.href;
		a = b+a;
		window.open(a,'name');
	}
    function createInput(){
        var ids=[];
        $(".deletebox").each(function(index){
            if($(this).is(":checked")){
                ids.push(this.id);
            }
        });
        $(".delete-ids").val(ids.join());
        return true;
    }
  </script>
{/literal}
<a name="featured" />

{$lng.txt_featured_products}



{if $looktype == "Search"}
<form action="solr_search.php" method="post" name="mapping-add-form" class="mapping-add-form">
<input type="hidden" name="mode" class="mode" value="AddMode"/>
<input type="submit" value="Go to Add Mode">
</form>

<h2>Search Field</h2>
<form action="solr_search.php" method="post" name="mapping-add-form" class="mapping-add-form">
<input type="checkbox" name="urlsearch" class="mode">URL Search</input>
<input type="hidden" name="mode" class="mode" value="StringSearch"/>
<input type="hidden" name="offset" class="mode" value="{$offset}"/>	
<input type="text" name="searchLanding" class="mode" value="{$searchLanding}"/>
<input type="submit" value="Search String">
</form>

<table cellpadding="3" cellspacing="1" width="100%">
   <tr>
   	<td colspan="2">
   	{if $offset>0}
   	<form action="solr_search.php" method="post" name="mapping-prev-form" class="mapping-prev-form">
	<input type="hidden" name="mode" class="mode" value="Previous"/>
    <input type="hidden" name="urlsearch" class="mode" value="{$urlsearch}"/>
	<input type="hidden" name="searchLanding" class="mode" value="{$searchLanding}"/>
	<input type="hidden" name="offset" class="mode" value="{$offset}"/>
	<input type="submit" value="Previous">
    </form>
	{/if}
   	
   	</td>
   	<td colspan="2">
   	{if $offset< $maxoffset}
   	<form action="solr_search.php" method="post" name="mapping-next-form" class="mapping-next-form">
	<input type="hidden" name="mode" class="mode" value="Next"/>
    <input type="hidden" name="urlsearch" class="mode" value="{$urlsearch}"/>
	<input type="hidden" name="searchLanding" class="mode" value="{$searchLanding}"/>
	<input type="hidden" name="offset" class="mode" value="{$offset}"/>
   	<P align="right">
	<input type="submit" value="Next">
   	</p>
   	</form>
	{/if}
   	</td>
   </tr>
   <tr>
   <td><b>Delete</b></td>
   <td><b>Page Url</b></td>
   <td><b>Page Key</b></td>
   <td colspan="3"><b>Search Field</b></td>
   <td><b>Curated List</b><td>
   </tr>
   <break>
   <tr>
   	{foreach name=dataloop item=row from=$searchresult}
	<tr class="TableSubHead">
	<td><input type="checkbox" name="deleteid" id="{$row.pagekey}" class="deletebox"></td>
	<td>{$row.page_url}</td>
	<td>{$row.pagekey}</td>
	<td colspan="2">{$row.searchstring}</td>
	<td>
		<form action="solr_search.php" method="post" name="mapping-add-form" class="mapping-add-form">
		<input type="hidden" name="mode" class="mode" value="EditMode"/>
		<input type="hidden" name="updateid" class="mode" value="{$row.id}"/>
		<input type="hidden" name="landingpage_url" class="mode" value="{$row.page_url}"/>
	   	<P align="right">
		<input type="submit" value="edit">
		</P>
		</form>
	</td>

	<td>
		<form action="solr_search.php" method="post" name="mapping-add-form" class="mapping-add-form">
		<input type="hidden" name="mode" class="mode" value="CuratedEditMode"/>
		<input type="hidden" name="updateid" class="mode" value="{$row.id}"/>
		<input type="hidden" name="landingpage_url" class="mode" value="{$row.page_url}"/>
	   	<P align="right">
		<input type="submit" value="edit">
		</P>
		</form>
	</td>
	</tr>
	{/foreach}
   </tr>
   </table>
<form action="solr_search.php" method="post" name="mapping-prev-form" class="mapping-prev-form" onsubmit="return createInput()">
<input type="hidden" name="deleteIds" class="delete-ids">
<input type="hidden" name="mode" class="mode" value="Delete"/>
<input type="submit" value="delete">
</form>
{/if}
{if $looktype == "CuratedEdit"}
	<form action="solr_search.php" method="post" name="mapping-add-form" class="mappint-add-form">
		<input type="hidden" name="mode" class="mode" value="SearchMode"/>
		<input type="submit" value="Go to Search mode">
	</form>
	<form name="frmG" action="{$form_action}" method="post" enctype="multipart/form-data" >	
	<input type="hidden" name="mode" value="CuratedEditGroup" />
	<input type="hidden" name="parameter_page_id" value="{$parameter_page_id}" />
	<table cellpadding="3" cellspacing="1" width="80%" >
	<tr>  
        	<td>Number of Slots    :</td>
		<td><input type="text" name="noSlots" value="{$form_data.no_slots}" /></td>
	</tr>
	<tr>  
        	<td>Style Id Csv    :</td>
		<td><input type="text" name="styleIdCsv" value="{$form_data.style_id_csv}" /></td>
	</tr>
	<tr>
		<td>Rule for Remaining Slots </td>
		<td>
        		<select name="Rule" style="width:100px" >
				{if empty($form_data.remaining_slots_rule) }
        				<option value="NA" style="width:100px" >NA</option>
				{else} <option value="{$form_data.remaining_slots_rule}" style="width:100px" >{$form_data.remaining_slots_rule}</option>
				{/if}
				{section name=num loop=$rules}
                			<option value="{$rules[num]}">{$rules[num]}</option>
            			{/section}
        		</select>
        	</td>
	</tr>

	</table>
	<input type="button" value="{$lng.lbl_save|escape}" name="sbtFrm" onclick="addRule()" />
	</form>
{/if}


{if $looktype == "Add" || $looktype == "Edit"}
   
 <form action="solr_search.php" method="post" name="mapping-add-form" class="mapping-add-form">
<input type="hidden" name="mode" class="mode" value="SearchMode"/>
<input type="submit" value="Go to Search Mode">
</form>  
 <form name="frmG" action="{$form_action}" method="post" enctype="multipart/form-data" >
{if $looktype == "Add"}
<input type="hidden" name="mode" value="addgroup" />
{/if}
{if $looktype == "Edit"}
<input type="hidden" name="mode" value="editgroup" />
<input type="hidden" name="parameter_page_id" value="{$parameter_page_id}" />
{/if}
<input type="hidden" name="searchLanding" class="mode" value="{$searchLanding}"/>
<input type="hidden" name="offset" class="mode" value="{$offset}"/>
<table cellpadding="3" cellspacing="1" width="80%" >
   <tr>
   {if $looktype == "Add"}
        <td><h2>Add Search Field</h2><h3>(For Multiple Select use Ctrl key , Also for removing single option ctrl and click)</h3></td>
   {else}
        <td><h2>Edit Search Field</h2><h3>(For Multiple Select use Ctrl key , Also for removing single option ctrl and click)</h3></td>
   {/if}
        <td>&nbsp;</td>
   </tr>
   
   <tr>
        <td >Brand : </td><td>
        <select name="brand[]" style="width:200px;height:300px" multiple="multiple" >
            {section name=num loop=$brandli}
                <option value="{$brandli[num].attribute_value}" style="width:200px" {if in_array($brandli[num].attribute_value, $selected_values.brands_filter_facet)} selected="selected"{/if} >{$brandli[num].attribute_value}</option>
            {/section}
            
        </select>
        </td><td >Age Group : </td><td>
        <select name="ageGroup[]" style="width:200px" multiple="multiple">
            {section name=num loop=$ageGroup}
                <option value="{$ageGroup[num].attribute_value}" style="width:200px" {if in_array($ageGroup[num].attribute_value, $selected_values.global_attr_age_group)} selected="selected"{/if} >{$ageGroup[num].attribute_value}</option>
            {/section}
            
        </select>
        
        </td>
        </tr><tr>
        <td >Gender : </td><td>
        <select name="gender[]" style="width:200px" multiple="multiple">
            {section name=num loop=$gender}
                <option value="{$gender[num].attribute_value}" style="width:200px" {if in_array($gender[num].attribute_value, $selected_values.global_attr_gender)} selected="selected"{/if} >{$gender[num].attribute_value}</option>
            {/section}
            
        </select>
        </td><td >Fashion Type : </td><td>
        <select name="fashionType[]" style="width:200px" multiple="multiple">
            {section name=num loop=$fashionType}
                <option value="{$fashionType[num].attribute_value}" style="width:200px" {if in_array($fashionType[num].attribute_value, $selected_values.global_attr_fashion_type)} selected="selected"{/if} >{$fashionType[num].attribute_value}</option>
            {/section}
         
        </select>
        </td>
        </tr><tr>
        <td >usage : </td><td>
        <select name="usage[]" style="width:200px" multiple="multiple">
            {section name=num loop=$usage}
                <option value="{$usage[num].attribute_value}" style="width:200px" {if in_array($usage[num].attribute_value, $selected_values.global_attr_usage)} selected="selected"{/if} >{$usage[num].attribute_value}</option>
            {/section}
            
        </select>
        </td><td >Season : </td><td>
        <select name="season[]" style="width:200px" multiple="multiple">
            {section name=num loop=$season}
                <option value="{$season[num].attribute_value}" style="width:200px" {if in_array($season[num].attribute_value, $selected_values.global_attr_season)} selected="selected"{/if} >{$season[num].attribute_value}</option>
            {/section}
        </select>
        </td>
        </tr><tr>
        <td >Colour : </td><td >
        <select name="colour[]" style="width:200px;height:300px" multiple="multiple">
            {section name=num loop=$colour}
                <option value="{$colour[num].attribute_value}" style="width:200px" {if in_array($colour[num].attribute_value, $selected_values.global_attr_base_colour)} selected="selected"{/if} >{$colour[num].attribute_value}</option>
            {/section}
        </select>
        </td>
       
        
        <td >Article Type : </td><td>
        <select name="article_types[]" style="width:200px;height:300px" multiple="multiple">
            {section name=num loop=$article_types}
                <option value="{$article_types[num].typename}" style="width:200px" {if in_array($article_types[num].typename, $selected_values.global_attr_article_type_facet)} selected="selected"{/if} >{$article_types[num].typename}</option>
            {/section}
            
        </select>
        </td>
        </tr><tr>
        <td >Sub Category : </td><td>
        <select name="sub_category[]" style="width:200px;height:200px" multiple="multiple">
            {section name=num loop=$sub_categories}
                <option value="{$sub_categories[num].typename}" style="width:200px" {if in_array($sub_categories[num].typename, $selected_values.global_attr_sub_category)} selected="selected"{/if} >{$sub_categories[num].typename}</option>
            {/section}
        </select>
        </td><td >Master Category : </td><td>
        <select name="master_category[]" style="width:200px;height:200px" multiple="multiple">
            {section name=num loop=$master_categories}
                <option value="{$master_categories[num].typename}" style="width:200px" {if in_array($master_categories[num].typename, $selected_values.global_attr_master_category)} selected="selected"{/if} >{$master_categories[num].typename}</option>
            {/section}
        </select>
        </td>
    </tr>
    <tr>  
        <td>Price </td><td>Min:  <input type="text" name="Price[0]" {if $selected_values.price} value="{$selected_values.price.from}" {/if} /> </td><td>Max:  <input type="text" name="Price[1]" {if $selected_values.price} value="{$selected_values.price.to}" {/if}/></td>
   </tr>
    <tr>  
        <td>Discount Price </td><td>Min:  <input type="text" name="DP[0]" {if $selected_values.discounted_price} value="{$selected_values.discounted_price.from}" {/if}/> </td><td>Max:  <input type="text" name="DP[1]" {if $selected_values.discounted_price} value="{$selected_values.discounted_price.to}" {/if}/></td>
   </tr>
    <tr>  
        <td>Year </td><td>
        Min: 
        <select name="Year[0]" style="width:200px" >
        <option value="NA" style="width:200px" >NA</option>
            {section name=num loop=$year}
                <option value="{$year[num].attribute_value}" style="width:200px" {if $year[num].attribute_value eq $selected_values.global_attr_year.from} selected="selected"{/if} >{$year[num].attribute_value}</option>
            {/section}
        </select>
         </td>
        <td>
        Max:
        
        <select name="Year[1]" style="width:200px" >
        <option value="NA" style="width:50px" >NA</option>
            {section name=num loop=$year}
                <option value="{$year[num].attribute_value}" style="width:200px" {if $year[num].attribute_value eq $selected_values.global_attr_year.to} selected="selected"{/if} >{$year[num].attribute_value}</option>
            {/section}
        </select>
   </tr>
   <tr>
        <td>Tags</td><td><input type="text" name="keywords" {if $selected_values.keywords} value="{","|implode:$selected_values.keywords}" {/if}/> </td>
   </tr>
   <tr>
   <td colspan="2" align="center">
   
   </td>
   </tr>

</table>
<INPUT type="button" value="Add String Field Row" onclick="addRow('dataStringTable')" />
 <a href="solr_search_help.php"  target="_blank">Not Sure what options to enter</a>
    <TABLE id="dataStringTable" width="350px" >
        <TR>
             <td class="dataTableSelectors">
        	<select name="stringFieldName[]"  style="width:200px" >
            {section name=num loop=$stringFieldArray}
                <option value="{$stringFieldArray[num]}" style="width:200px" >{$stringFieldArray[num]}</option>
            {/section}
        </select>
        </td>
        <td><input type="text" name="stringFieldValue[]" /> </td>
        </TR>
        {if $looktype == "Edit"}
        {foreach from=$stringFieldArray item=stringFieldTmp}
        {if $selected_values[$stringFieldTmp]}
        <TR>
             <td class="dataTableSelectors">
        	<select name="stringFieldName[]"  style="width:200px" >
            {section name=num loop=$stringFieldArray}
                <option value="{$stringFieldArray[num]}" style="width:200px" {if $stringFieldArray[num] eq $stringFieldTmp} selected="selected"{/if} >{$stringFieldArray[num]}</option>
            {/section}
        </select>
        </td>
        <td><input type="text" name="stringFieldValue[]" value="{","|implode:$selected_values[$stringFieldTmp]}" /> </td>
        </TR>
        {/if}
        {/foreach}
        {/if}
    </TABLE>
<INPUT type="button" value="Add Text Field Row" onclick="addRow('dataRangeTable')" />
 
    <TABLE id="dataRangeTable" width="350px" >
        <TR>
             <td class="dataTableSelectors">
        	<select name="rangeFieldName[]"  style="width:200px" >
            {section name=num loop=$rangeFieldArray}
                <option value="{$rangeFieldArray[num]}" style="width:200px" >{$rangeFieldArray[num]}</option>
            {/section}
        </select>
        </td>
        <td>Min: <input type="text" name="rangeFieldValueLeft[]" /> </td>
        <td>Max: <input type="text" name="rangeFieldValueRight[]" /> </td>
        </TR>
        {if $looktype == "Edit"}
        {foreach from=$rangeFieldArray item=rangeFieldTmp}
        {if $selected_values[$rangeFieldTmp]}
        <TR>
             <td class="dataTableSelectors">
        	<select name="rangeFieldName[]"  style="width:200px" >
            {section name=num loop=$rangeFieldArray}
                <option value="{$rangeFieldArray[num]}" style="width:200px" {if $rangeFieldArray[num] eq $rangeFieldTmp} selected="selected"{/if} >{$rangeFieldArray[num]}</option>
            {/section}
        </select>
        </td>
        <td>Min: <input type="text" name="rangeFieldValueLeft[]" value="{$selected_values[$rangeFieldTmp].from}" /> </td>
        <td>Max: <input type="text" name="rangeFieldValueRight[]" value="{$selected_values[$rangeFieldTmp].to}" /> </td>
        </TR>
        {/if}
        {/foreach}
        {/if}
</TABLE>
<TABLE>
   <tr>
        <td><input type="checkbox" name="landingCheck" id="landing-check" {if $page_url} checked {/if}> : Is having a Landing Page </td>
        <td>URL :</td><td>  <input type="text" name="landingPage" {if $page_url} value="{$page_url}" {else}disabled="disabled"{/if}/></td>
   </tr>

   <tr>
        <td><input type="checkbox" name="isactive" id="landing-page-active-check" {if $isactive} checked {/if}> : Is Active </td>
   </tr>

   <tr>
        <td><input type="date" name="expiryDate" id="landing-page-expiry" {if $expiryDate} value="{$expiryDate}" {/if}> <span class="error">* <?php echo "This field is required";?></span> : Expiry date for this landing page </td>
   </tr>

    <tr>
        <td>Landing Page Type</td><td>
        <select name="landingPageType" style="width:200px" id="landing-page-type">
        <option value="NA" style="width:200px" >NA</option>
            {section name=num loop=$lpCategories}
                <option value="{$lpCategories[num].category}" data-expiry-days="{$lpCategories[num].max_expiry_days}" style="width:200px" {if $lpCategories[num].category eq $landingPageType} selected="selected"{/if} >{$lpCategories[num].category}</option>
            {/section}
        </select>
        </td>
   </tr>

</TABLE>
<input type="button" value="{$lng.lbl_save|escape}" name="sbtFrm" onclick="addRule()" />
</form>
{/if}



{/capture}
{include file="dialog.tpl" title=" Paramaterised Search Fields" content=$smarty.capture.dialog extra='width="100%"'}

{literal}
  <script language="Javascript">
     $(document).ready(function(){
    	 
    	 $('#landing-check').change(function(){
        	 if($(this).is(':checked')){
				$('input[name=landingPage]').removeAttr('disabled');
        	 }
        	 else{
        		 $('input[name=landingPage]').attr('disabled','disabled');
           	 }
         });
 			
     })
  </script>
  <SCRIPT language="javascript">
        function addRow(tableID) {
            console.log(tableID);
        	 $('#'+tableID+' tr:last').after('<tr>'+$('#'+tableID+' tr:first').html()+'</tr>');
        }
 
    </SCRIPT>
{/literal}
