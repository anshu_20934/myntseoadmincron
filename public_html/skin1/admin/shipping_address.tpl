<div style="border: 2px solid black;float: left;width: 100%;">
	<div><input type="button" value="Add New Address" id="newaddresstogglebutton" style="padding-left: 15px 15px; margin: 15px 15px 0;background-color: orange;color: white;" onclick="$('#newAddressDiv').toggle();{literal}if($('#newaddresstogglebutton').val()=='Add New Address'){$('#newaddresstogglebutton').val('Back');}else{$('#newaddresstogglebutton').val('Add New Address');}{/literal}"></div>
	
	<div id="newAddressDiv" style="display: none;">
	<Div style="font-size: 15px; margin: 15px 15px 0;">Enter a <strong>New Shipping address</strong></div>
	<hr style=" background-color: black; color: black; height: 5px; margin: 0 10px;"/>
		<div style="margin-left: 25%; width: 50%;">
			<div>
				
				<div >
					<table>
						<tr>
							<td>
								<span>Recipient's Name <b>*</b></span>
							</td>
							<td>
								<input type="text" name="name" id="newaddressname" value="">
							</td>
						</tr>
						<tr>
							<td>
								<span>Address  <b>*</b></span>
							</td>
							<td>
								<textarea name="address" id="newaddressaddress"></textarea>
							</td>
						</tr>
						<tr>
							<td>
								<span>City  <b>*</b></span>
							</td>
							<td>
								<input type="text" name="city" id="newaddresscity">
							</td>
						</tr>
						<tr>
							<td>
								<span>Country <b>*</b></span>
							</td>
							<td>
								<select name="country" id="newaddresscountry" onchange="{literal}if($('#newaddresscountry').val()=='IN'){$('#state_select').show();$('#newaddressstateinput').hide();}else{$('#state_select').hide();$('#newaddressstateinput').show();}{/literal}">
                                    <option value="IN" selected="true">India</option>
                                    {* commenting out showing countries other than india
                                    {foreach key=countrycode item=countryname from=$countries}
                                    <option value="{$countrycode}" {if 'IN' eq $countrycode}selected="true"{/if}>{$countryname}</option>
                                    {/foreach}*}
							</select>
							</td>
						</tr>
						<tr>
							<td>
								<span>State <b>*</b></span>
							</td>
							<td>
								<span id="newaddressstate">
								<select name="state" id="state_select" class="required">
									<option value="">--Select State--</option>
									{section name="statename" loop=$states}
									<option value="{$states[statename].code}" {if $default.state eq $states[statename].code}selected="true"{/if}>{$states[statename].state}</option>
									{/section}
								</select>
								<input type="text" value="" id="newaddressstateinput" name="state" style="display: none;" >
								</span>
							</td>
						</tr>
						<tr>
							<td>
								<span>Pin Code <b>*</b></span>
							</td>
							<td>
								<input type="text" name="pincode" id="newaddresspincode" maxlength="6">
							</td>
						</tr>
						<tr>
							<td>
								<span>Email <b>*</b></span>
							</td>
							<td>
								<input type="text" name="email" id="newaddressemail">
							</td>
						</tr>
						<tr>
							<td>
								<span>Mobile <b>*</b></span>
							</td>
							<td>
								<input type="text" name="mobile" id="newaddressmobile" maxlength="10">
							</td>
						</tr>
						<tr>
							<td>
								<span>Telephone</span>
							</td>
							<td>
								<input type="text" name="telephone" id="newaddresstelephone" maxlength="15">
							</td>
						</tr>
					
					</table>
 					
						
					 <div><input type="button" value="Save Address" style="padding-left: 15px 15px; margin-left: 15px;background-color: orange;color: white;" onclick="saveNewAddress();">&nbsp;
					 <input type="button" value="Cancel" style="padding-left: 15px 15px; margin-left: 15px;background-color: orange;color: white;" onclick="$('#newAddressDiv').toggle();{literal}if($('#newaddresstogglebutton').val()=='Add New Address'){$('#newaddresstogglebutton').val('Back');}else{$('#newaddresstogglebutton').val('Add New Address');}{/literal}"></div>
				
			</div>
	</div>
</div>
</div>
<Div style="font-size: 15px; margin: 15px 15px 0;"><b>Select an existing address</b></Div>
<hr style=" background-color: black; color: black; height: 5px; margin: 0 10px;"/>
	{section name="index" loop=$addresses}
                                       
										
											<div id="addressDiv{$addresses[index].id}"style="padding: 10px; text-align: center; width: 150px;float:left;">
												<div style="background-color: Orange;border-radius: 10px 10px 10px 10px;font-size: 12px;{if $addresses[index].country_code eq 'IN'}display:none;{/if}">International Shipping</div>
												<div><strong>{$addresses[index].name}</strong></div>
												<div>{$addresses[index].address}</div>
												<div> {$addresses[index].city}, {$addresses[index].state}</div>
												<div> {$addresses[index].country}, {$addresses[index].pincode}</div>
												<div>Mobile: {$addresses[index].mobile}</div>
												{if $addresses[index].isServiceable} 
													<input type="button" value="Select Address"style="cursor:pointer;padding-left: 15px 15px; background-color: orange;color: white;" onclick="selectAddress('{$addresses[index].id}')">
												{else}
													Address not serviceable
												{/if}
											</div>
											
										
										
	{/section}



