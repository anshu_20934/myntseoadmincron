{* $Id: menu.tpl,v 1.68.2.2 2006/06/20 05:52:05 svowl Exp $ *}
{capture name=menu}
<li><a href="{$catalogs.admin}/orders_search.php?mode=search&type=5" class="VertMenuItems">Online Orders</a></li>
<li><a href="{$catalogs.admin}/orders.php" class="VertMenuItems">Order Search</a></li>
<li><a href="{$catalogs.admin}/old_returns_tracking/old_returnedorders_tracking.php" class="VertMenuItems">RTO Tracking</a></li>
<li><a href="{$catalogs.admin}/old_returns_tracking/old_returns_tracking.php" class="VertMenuItems">Returns Tracking</a></li>
<li><a href="{$catalogs.admin}/returns/return_requests.php" class="VertMenuItems">Manage Returns</a></li>
<li><a href="{$catalogs.admin}/courier_serviceability.php" class="VertMenuItems">Courier Serviceability</a></li>
{/capture}
{include file="menu.tpl" dingbats="dingbats_categorie.gif" menu_title='Other Useful URLs' menu_content=$smarty.capture.menu }