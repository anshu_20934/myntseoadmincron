{* $Id: featured_products.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}

{literal}
<script type="text/javascript">
$(document).ready(function(){

	/*
	 * onclick event to select all data
	 */	
	$("#selectalldata").click(function(){
		if($("#selectalldata").attr("checked")){
			$("form input:checkbox").attr("checked",true);
		} else {
			$("form input:checkbox").attr("checked",false);
		}
		
	});	
});

/*
 * selects a data and sets hidden field to selected data id
 */
function selectData(selectedItemId){
	$("form input:checkbox").attr("checked",false);
	$("#routing_"+selectedItemId+"_selector").attr("checked",true);
	$("#selecteditem").val(selectedItemId);
}

/*
 * validates an elemet value for empty and shows the error message on empty
 * @param:(object)checkElementObj - element to be checked
 * @param:(string)msgShowId - error element id
 * @return:(bool)
 */
function validateElementForEmpty(checkElementObj,msgShowId){
	if($(checkElementObj).val()==''){
		$(checkElementObj).focus();
		$("#"+msgShowId).text('The field can not be empty');
		return false;
		
	} else {
		$("#"+msgShowId).text('');
		return true;
	}	
}

/*
 * validates all the text elemets under parentObj
 * @param:(object)parentObj - element under which all the input:text will be validated
 * @param:(string)msgShowId - error element id
 * @return:(bool)false /(void)
 */
function validateAddForm(parentId,msgShowId){
	var hasError=false;
	$("#"+parentId+" input:text").each(function(){		
		if(!validateElementForEmpty(this,msgShowId)){
			hasError=true;
			return false;//break
		}		
	});
	if(!hasError){
		$("#mode").val("add");
		$("#semrouting").trigger('submit');
	}
}

/*
 * validates all the text elemets under parentObj
 * @param:(object)parentObj - element under which all the input:text will be validated
 * @param:(string)msgShowId - error element id
 * @return:(bool)false /(void)
 */
function validateUpdateForm(parentId,dataId){
	var hasError=false;
	//update single data
	if (dataId){	
		$("#"+parentId+" input[name$='["+dataId+"]']").each(function(){		
			if(!validateElementForEmpty(this,'routing_'+dataId+'_message')){
				hasError=true;
				return false;//break from loop
			}		
		});
		if(!hasError){
			$('#mode').val('update');
			selectData(dataId);
			$("#semrouting").trigger('submit');
		}
		
	} else {//update multiple data	
		$("#"+parentId+" input:text").each(function(){
			if(!validateElementForEmpty(this,'routing_all_update_message')){
				hasError=true;
				return false;//break from loop
			}		
		});
		if(!hasError){
			$('#mode').val('updateall');			
			$("#semrouting").trigger('submit');
		}
	}

}
</script>
{/literal}

{capture name=dialog}
	<form action="{$http_location}/admin/sem/semRouting.php" method="post" name="semrouting" id="semrouting">
		<input type="hidden" name="mode" id="mode"/>
		<input type="hidden" name="selecteditem" id="selecteditem"/>		
		<table cellpadding="3" cellspacing="1" width="100%">		
			<tr><td align="center" style="font-size:15px;">{$message}</td></tr>
			<tr>				
				<td>
					<table id="addFormTable">
						<tr><td id="add_message" style="color:red"></td></tr>
						<tr>
							<td align="center"><label>URL:</label><input name="url" type="text" value="" maxlength="100"/></td>
							<td align="center"><label>Landing-URL:</label><input name="landing_url" type="text" value="" maxlength="100"/></td>							
							<td align="center"><input type="button" value="Add" onclick="javascript:validateAddForm('addFormTable','add_message');"/></td>							
						</tr>
					</table><br/><br/>
				</td>
			</tr>
			<tr><td style="color:green;">Note: example url structure for these routing to work: http://www.myntra.com/URL?drte=1&... ; will be routed to http://www.myntra.com/landing-URL</td></tr>
		</table>
		<table cellpadding="3" cellspacing="1" width="100%" id="updateFormTable">	
			<tr class="TableHead">
				<th>Select All<input type="checkbox" name="selectalldata" id="selectalldata"/></th>
				<th width="30%">URL</th>				
				<th width="30%">landing-URL</th>
				<th width="10%">Date Created</th>
				<th width="10%">Status</th>
				<th width="15%">Action</th>
			</tr>			
			<tr><td colspan="6" id="routing_all_update_message" style="color:red"></td></tr>
			{section name=idx loop=$data}			
			<tr><td colspan="6" id="routing_{$data[idx].id}_message" style="color:red"></td></tr>
			<tr {cycle values=", class='TableSubHead'"}>		
				<td align="center"><input name="routing[selector][{$data[idx].id}]" id="routing_{$data[idx].id}_selector" type="checkbox"/></td>
				<td align="center"><input name="routing[url][{$data[idx].id}]" type="text" value="{$data[idx].url}" style="width:150px;" maxlength="100"/></td>
				<td align="center"><input name="routing[landing_url][{$data[idx].id}]" type="text" value="{$data[idx].landing_url}" style="width:150px;" maxlength="100"/></td>
				<td align="center"><label>{$data[idx].created_date}</label></td>
				<td align="center">
					<select name="routing[status][{$data[idx].id}]"/>
						{if $data[idx].is_active eq 1}
							<option value="1" selected=selected>Active</option>
							<option value="0">Inactive</option>
						{else}
							<option value="1" >Active</option>
							<option value="0" selected=selected>Inactive</option>
						{/if}
					</select>				
				</td>								
				<td align="center">
					<table>
						<tr>
							<td><input type="button" value="Update" onclick="if(confirm('Are you sure to update?')){ldelim}validateUpdateForm('updateFormTable','{$data[idx].id}');{rdelim}else{ldelim}return false;{rdelim}"/></td>
							<td><input type="submit" value="Delete" onclick="if(confirm('Are you sure to delete?')){ldelim}$('#mode').val('delete');selectData('{$data[idx].id}');{rdelim}else{ldelim}return false;{rdelim}"/></td>
						</tr>
					</table>
				</td>	
			</tr>			
			{sectionelse}
				<tr>
					<td colspan="2" align="center">No data is available!</td>
				</tr>
			{/section}
			
			{if $data|@count>1}
			<tr>
				<td colspan="5">&nbsp;</td>
				<td align="center"><input type="button" value="Update All Selected Data" onclick="if(confirm('Are you sure to update all data?')){ldelim}validateUpdateForm('updateFormTable','');{rdelim}else{ldelim}return false;{rdelim}" /><!--$(this).attr('disabled','disabled');$(this).val('Updating all data, Please wait...');-->
				</td>
				<td></td>
			</tr>
			{/if}
		</table>
	</form>
	
{/capture}
{include file="dialog.tpl" title="SEM Routing Engine" content=$smarty.capture.dialog extra='width="100%"'}