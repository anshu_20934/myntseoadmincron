{* $Id: featured_products.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}

{literal}
<script type="text/javascript">
$(document).ready(function(){

	/*
	 * onclick event to select all promos
	 */	
	$("#selectallpromo").click(function(){
		if($("#selectallpromo").attr("checked")){
			$("form input:checkbox").attr("checked",true);
		} else {
			$("form input:checkbox").attr("checked",false);
		}
		
	});	
});

/*
 * selects a promo and sets hidden field to selected promo id
 */
function selectPromo(selectedPromoId){
	$("form input:checkbox").attr("checked",false);
	$("#promo_"+selectedPromoId+"_selector").attr("checked",true);
	$("#selectedpromo").val(selectedPromoId);
}

/*
 * validates an elemet value for empty and shows the error message on empty
 * @param:(object)checkElementObj - element to be checked
 * @param:(string)msgShowId - error element id
 * @return:(bool)
 */
function validateElementForEmpty(checkElementObj,msgShowId){
	if($(checkElementObj).val()==''){
		$(checkElementObj).focus();
		$("#"+msgShowId).text('The field can not be empty');
		return false;
		
	} else {
		$("#"+msgShowId).text('');
		return true;
	}	
}

/*
 * validates all the text elemets under parentObj
 * @param:(object)parentObj - element under which all the input:text will be validated
 * @param:(string)msgShowId - error element id
 * @return:(bool)false /(void)
 */
function validateAddForm(parentId,msgShowId){
	var hasError=false;
	$("#"+parentId+" input:text").each(function(){		
		if(!validateElementForEmpty(this,msgShowId)){
			hasError=true;
			return false;//break
		}		
	});
	if(!hasError){
		$("#mode").val("addpromo");
		$("#addpromo").trigger('submit');
	}
}

/*
 * validates all the text elemets under parentObj
 * @param:(object)parentObj - element under which all the input:text will be validated
 * @param:(string)msgShowId - error element id
 * @return:(bool)false /(void)
 */
function validateUpdateForm(parentId,promoId){
	var hasError=false;
	//update single promo
	if (promoId){	
		$("#"+parentId+" input[name$='["+promoId+"]']").each(function(){		
			if(!validateElementForEmpty(this,'promo_'+promoId+'_message')){
				hasError=true;
				return false;//break from loop
			}		
		});
		if(!hasError){
			$('#mode').val('updatepromo');
			selectPromo(promoId);
			$("#addpromo").trigger('submit');
		}
		
	} else {//update multiple promos	
		$("#"+parentId+" input:text").each(function(){
			if(!validateElementForEmpty(this,'promo_all_update_message')){
				hasError=true;
				return false;//break from loop
			}		
		});
		if(!hasError){
			$('#mode').val('updateallpromo');			
			$("#addpromo").trigger('submit');
		}
	}

}
</script>
{/literal}

{capture name=dialog}
	<form action="{$http_location}/admin/sem/semPromo.php" method="post" name="addpromo" id="addpromo">
		<input type="hidden" name="mode" id="mode"/>
		<input type="hidden" name="selectedpromo" id="selectedpromo"/>		
		<table cellpadding="3" cellspacing="1" width="100%">		
			<tr><td align="center" style="font-size:15px;">{$promomessage}</td></tr>
			<tr>				
				<td>
					<table id="addFormTable">
						<tr><td id="promo_add_message" style="color:red"></td></tr>
						<tr>
							<td align="center"><label>Identifier:</label><input name="identifier" type="text" value=""/></td>
							<td align="center"><label>Content:</label><input name="promo_content" type="text" value="" maxlength="130"/></td>
							<td align="center"><label>URL:</label><input name="promo_url" type="text" value="" /></td>
							<td align="center"><input type="button" value="Add New Promo" onclick="javascript:validateAddForm('addFormTable','promo_add_message');"/></td>							
						</tr>
					</table><br/><br/>
				</td>
			</tr>
			<tr><td style="color:green;">Note: example url structure for these identifiers to work: http://www.myntra.com?semp=identifier&... </td></tr>
		</table>
		<table cellpadding="3" cellspacing="1" width="100%" id="updateFormTable">	
			<tr class="TableHead">
				<th>Select All<input type="checkbox" name="selectallpromo" id="selectallpromo"/></th>
				<th width="10%">Identifier</th>
				<th width="40%">Content</th>
				<th width="30%">URL</th>
				<th width="10%">Date Created</th>
				<th width="10%">Action</th>
			</tr>			
			<tr><td colspan="6" id="promo_all_update_message" style="color:red"></td></tr>
			{section name=idx loop=$promos}			
			<tr><td colspan="6" id="promo_{$promos[idx].id}_message" style="color:red"></td></tr>
			<tr {cycle values=", class='TableSubHead'"}>		
				<td align="center"><input name="promo[selector][{$promos[idx].id}]" id="promo_{$promos[idx].id}_selector" type="checkbox"/></td>
				<td align="center"><input name="promo[identifier][{$promos[idx].id}]" type="text" value="{$promos[idx].identifier}"/></td>
				<td align="center"><input name="promo[promo_content][{$promos[idx].id}]" type="text" value="{$promos[idx].promo_content}" style="width:350px;" maxlength="130"/></td>
				<td align="center"><input name="promo[promo_url][{$promos[idx].id}]" type="text" value="{$promos[idx].promo_url}" style="width:300px;"/></td>
				<td align="center"><label>{$promos[idx].created_date}</label></td>				
				<td align="center">
					<table>
						<tr>
							<td><input type="button" value="Update" onclick="if(confirm('Are you sure to update?')){ldelim}validateUpdateForm('updateFormTable','{$promos[idx].id}');{rdelim}else{ldelim}return false;{rdelim}"/></td>
							<td><input type="submit" value="Delete" onclick="if(confirm('Are you sure to delete?')){ldelim}$('#mode').val('deletepromo');selectPromo('{$promos[idx].id}');{rdelim}else{ldelim}return false;{rdelim}"/></td>
						</tr>
					</table>
				</td>	
			</tr>			
			{sectionelse}
				<tr>
					<td colspan="2" align="center">No Promo is available!</td>
				</tr>
			{/section}
			
			{if $promos|@count>1}
			<tr>
				<td colspan="5">&nbsp;</td>
				<td align="center"><input type="button" value="Update All Selected Promos" onclick="if(confirm('Are you sure to update all promos?')){ldelim}validateUpdateForm('updateFormTable','');{rdelim}else{ldelim}return false;{rdelim}" /><!--$(this).attr('disabled','disabled');$(this).val('Updating all promos, Please wait...');-->
				</td>
				<td></td>
			</tr>
			{/if}
		</table>
	</form>
	
{/capture}
{include file="dialog.tpl" title="SEM Promos" content=$smarty.capture.dialog extra='width="100%"'}