{* $Id: featured_products.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}

{literal}
<script type="text/javascript">
$(document).ready(function(){

	/*
	 * onclick event to select all data
	 */	
	$("#selectalldata").click(function(){
		if($("#selectalldata").attr("checked")){
			$("form input:checkbox").attr("checked",true);
		} else {
			$("form input:checkbox").attr("checked",false);
		}
		
	});	
});

/*
 * selects a data and sets hidden field to selected data id
 */
function selectData(selectedItemId){
	$("form input:checkbox").attr("checked",false);
	$("#"+selectedItemId+"_selector").attr("checked",true);
	$("#selecteditem").val(selectedItemId);
}

/*
 * validates an elemet value for empty and shows the error message on empty
 * @param:(object)checkElementObj - element to be checked
 * @param:(string)msgShowId - error element id
 * @return:(bool)
 */
function validateElementForEmpty(checkElementObj,msgShowId){
	if($.trim($(checkElementObj).val())==''){
		$(checkElementObj).focus();
		$("#"+msgShowId).text('The field can not be empty');
		return false;
		
	} else {
		$("#"+msgShowId).text('');
		return true;
	}	
}

/*
 * validates all the text elemets under parentObj
 * @param:(object)parentObj - all elements except "search_keyword" under which all the input:text will be validated
 * @param:(string)msgShowId - error element id
 * @return:(bool)false /(void)
 */
function validateAddForm(parentId,msgShowId){
	var hasError=false;
	$("#"+parentId+" input:text").each(function(){
		if(this.id == "search_keyword") return true;		
		if(!validateElementForEmpty(this,msgShowId)){
			hasError=true;
			return false;//break
		}		
	});
	if(!hasError){
		$("#mode").val("add");
		$("#add").trigger('submit');
	}
}

/*
 * validates all the text elemets under parentObj
 * @param:(object)parentObj - element under which all the input:text will be validated
 * @param:(string)msgShowId - error element id
 * @return:(bool)false /(void)
 */
function validateUpdateForm(parentId,dataId){
	var hasError=false;
	//update single data
	if (dataId){	
		$("#"+parentId+" input[name$='["+dataId+"]']").each(function(){		
                        if($(this).attr('class')=='keyword') {
			if(!validateElementForEmpty(this,dataId+'_message')){
				hasError=true;
				return false;//break from loop
			}		
                    }
		});
		if(!hasError){
			$('#mode').val('update');
			selectData(dataId);
			$("#add").trigger('submit');
		}
		
	} else {//update multiple data	
		$("#"+parentId+" input:text").each(function(){
                        if($(this).attr('class')=='keyword') {
			if(!validateElementForEmpty(this,'all_update_message')){
				hasError=true;
				return false;//break from loop
			}		
                        }
		});
		if(!hasError){
			$('#mode').val('updateall');			
			$("#add").trigger('submit');
		}
	}

}

function validateSearchForm(elementId,msgShowId){
    var hasError=false;
    var element = $("#"+elementId);
	
    if(!validateElementForEmpty(element,msgShowId)){
        hasError=true;
        return false;//break
    }		
	
   if(!hasError){
		$("#mode").val("search");
		$("#add").trigger('submit');
	}
}
</script>
{/literal}

{capture name=dialog}
	<form action="{$http_location}/admin/seo/crosslinkKeywordManager.php" method="post" name="add" id="add">
		<input type="hidden" name="mode" id="mode"/>
		<input type="hidden" name="selecteditem" id="selecteditem"/>		
		<table cellpadding="3" cellspacing="1" width="100%">		
			<tr><td align="center" style="font-size:15px;">{$message}</td></tr>
			<tr>				
				<td>
					<table id="addFormTable">
						<tr><td id="add_message" style="color:red"></td></tr>
						<tr>
							<td align="center"><label>Search Keyword:</label><input id="search_keyword" name="search_keyword" type="text" value="" maxlength="100"/></td>			
							<td align="center"><input type="button" value="Search" onclick="javascript:validateSearchForm('search_keyword','add_message');"/></td>							
						</tr>
						<tr>
							<td align="center"><label>Keyword:</label><input name="crosslink_keyword" type="text" value="" maxlength="100"/></td>			
							<td align="center"><input type="button" value="Add" onclick="javascript:validateAddForm('addFormTable','add_message');"/></td>							
						</tr>
						
					</table><br/><br/>
				</td>
			</tr>			
		</table>
		
		Page : 
		{section name=pagei start=1 loop=$totalKeywordsCount/$limit+2 step=1}
			{if $page == $smarty.section.pagei.index }
			<span style="color:black;weight:bold;">{$smarty.section.pagei.index}</span> &nbsp;
			{else}
			{if $mode == "search" }
			<a href="/admin/seo/crosslinkKeywordManager.php?page={$smarty.section.pagei.index}&mode={$mode}&search_keyword={$search_keyword}">{$smarty.section.pagei.index}</a> &nbsp;
			{else} <a href="/admin/seo/crosslinkKeywordManager.php?page={$smarty.section.pagei.index}">{$smarty.section.pagei.index}</a> &nbsp;
			{/if}
			{/if}		  
		{/section}
		<br/><br/>
		Showing Results from {$start+1} to {$pageEnd}<br/><br/>
		<table cellpadding="3" cellspacing="1" width="100%" id="updateFormTable">	
			<tr class="TableHead">
				<th>Select All<input type="checkbox" name="selectalldata" id="selectalldata"/></th>
				<th width="30%">Keyword</th>				
				<th width="20%">Relative Url</th>
				<th width="20%">Status</th>
				<th width="20%">Action</th>
			</tr>			
			<tr><td colspan="4" id="all_update_message" style="color:red"></td></tr>
			{section name=idx loop=$data}			
			<tr><td colspan="4" id="{$data[idx].id}_message" style="color:red"></td></tr>
			<tr {cycle values=", class='TableSubHead'"}>		
				<td align="center"><input name="keyword[selector][{$data[idx].id}]" id="{$data[idx].id}_selector" type="checkbox"/></td>
				<td align="center"><input class="keyword" name="keyword[crosslink_keyword][{$data[idx].id}]" type="text" value="{$data[idx].crosslink_keyword}" style="width:150px;" maxlength="100"/></td>
				<td align="center"><input name="keyword[crosslink_keyword_url][{$data[idx].id}]" type="text" value="{$data[idx].crosslink_keyword_url|escape}" style="width:250px;" maxlength=254"/></td>
				<td align="center">
					<select name="keyword[status][{$data[idx].id}]"/>
						{if $data[idx].is_active eq 1}
							<option value="1" selected=selected>Active</option>
							<option value="0">Inactive</option>
						{else}
							<option value="1" >Active</option>
							<option value="0" selected=selected>Inactive</option>
						{/if}
					</select>				
				</td>								
				<td align="center">
					<table>
						<tr>
							<td><input type="button" value="Update" onclick="if(confirm('Are you sure to update?')){ldelim}validateUpdateForm('updateFormTable','{$data[idx].id}');{rdelim}else{ldelim}return false;{rdelim}"/></td>
							<td><input type="submit" value="Delete" onclick="if(confirm('Are you sure to delete?')){ldelim}$('#mode').val('delete');selectData('{$data[idx].id}');{rdelim}else{ldelim}return false;{rdelim}"/></td>
						</tr>
					</table>
				</td>	
			</tr>			
			{sectionelse}
				<tr>
					<td colspan="4" align="center">No data is available!</td>
				</tr>
			{/section}
			
			{if $data|@count>1}
			<tr>
				<td colspan="3">&nbsp;</td>
				<td align="center"><input type="button" value="Update All Selected Data" onclick="if(confirm('Are you sure to update all data?')){ldelim}validateUpdateForm('updateFormTable','');{rdelim}else{ldelim}return false;{rdelim}" />
				</td>
				<td></td>
			</tr>
			{/if}
		</table>
	</form>
	
{/capture}
{include file="dialog.tpl" title="Cross-linkable Keywords" content=$smarty.capture.dialog extra='width="100%"'}
