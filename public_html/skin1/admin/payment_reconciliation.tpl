{php}
 $messages = $this->get_template_vars('messages');
 foreach($messages as $message) {
  echo '<p> '.$message.'</p>';
 }
{/php}
<form enctype="multipart/form-data" action="/admin/payment_reconciliation.php" method="POST">
	Following values will be overwritten if already present : <br/>
	<input type="checkbox" name="overwriteCapturedAmount" value="1" {if $unselect_capture}{else}checked{/if} /> Capture Amount 
	<input type="checkbox" name="overwriteRefundAmount" value="1" {if $unselect_refund}{else}checked{/if} /> Refund Amount 
	<input type="checkbox" name="overwriteCBAmount" value="1" {if $unselect_chargeback}{else}checked{/if} /> ChargeBack Amount 
	<input type="checkbox" name="overwriteTDR" value="1" {if $unselect_tdr}{else}checked{/if} /> TDR 
	<input type="checkbox" name="overwriteServiceTax" value="1" {if $unselect_tax}{else}checked{/if} /> Service Tax 
	<input type="checkbox" name="overwriteGatewayName" value="1" {if $unselect_gateway}{else}checked{/if} /> Gateway name 
	<input type="checkbox" name="overwriteCapturedDate" value="1" {if $unselect_date}{else}checked{/if} /> Captured Date  
	<br />
    Choose a file to upload:
    <br />
    <br />
    <table>
    <tr>
    <td>
    Reconciliation file :
    </td>
    <td>
    <input name="reconciliation_file" type="file"/>
    </td>
    </tr>
    </table> 
 <br />
<br />
     <input type="submit" value="Upload"/>
<br />
 <br />
</form>
 