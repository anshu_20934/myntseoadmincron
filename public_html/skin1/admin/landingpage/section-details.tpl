{literal}<style>
	#section-details {
		padding:50px;
	}

	#section-details h3{
		font-size:18px;
	}

	#section-details h3 span {
		color:red;
		text-transform: uppercase;
	}

	#section-details .row {
		padding:10px;
	}
	#section-details .row label{
		width:150px;
		display: inline-block;
		vertical-align: top;
	}
	
	#section-details .row input.text{
		width:250px;
	}
	#section-details .row textarea{
		width:250px;
	}

	.status-message {
		color:green;
		font-size:13px;
		text-align: center;
	}

	.section-block {
		padding:20px 0;
		border-bottom:solid 1px #ddd;
	}
{/literal}
</style>
<input type="button" name="lplanding" class="lplanding" value="Back to Landing page"/>
<div id="section-details">

	<h3 >Product Setions for <span>{$lpDetails.lp_name}</span></h3>
	{if $status}
		<div class="row">
			<p class="status-message">{$status}</p>
		</div>
	{/if}
	{if $sectionDetails}
	{foreach name=sectionDetail key=index item=sectionDetail from=$sectionDetails}
	<div class="section-block">
		<div class="row">
			<label>Title*</label>
			<input type="text" name="s_title" class="text s_title" value="{$sectionDetail.title}"/> <br />
		</div>
		<div class="row">
			<label>Description</label>
			<textarea rows="8" name="s_desc" class="s_desc"/>{$sectionDetail.description}</textarea> <br />
		</div>
		<div class="row">
			<label>Link for Title *</label>
			<input type="text" name="s_titlelink" class="text s_titlelink" value="{$sectionDetail.target_url}"/> <br />
		</div>
		<div class="row">
			<label>Solr Link *</label>
			<input type="text" name="s_solrlink" class="text s_solrlink" value="{$sectionDetail.url}"/> <span>e.g: <b>fabindia?sortby=RECENCY</b> (Do not apply filters using #! in the URL) </span><br />
		</div>
		<div class="row">
			<label>No of products to fetch *</label>
			<input type="text" name="s_noofproducts" class="text s_noofproducts" value="{$sectionDetail.no_of_products}"/> <span>No of products to be displayed in multiples of 5 </span><br />
		</div>
		<div class="row">
			<input type="checkbox" name="s_carousel" class="s_carousel" {if $sectionDetail.is_carousel}checked{else}{/if}><label> isCarousel</label> <br />
		</div>
		
		<div class="row">
			<input type="button" name="s_update" value="save" class="s_update" /> 
			<input type="button" name="s_cancel" class="s_cancel" value="cancel"/>
			{if !$smarty.foreach.sectionDetail.first}<input type="button" name="s_moveup" value="move up" class="s_moveup" /> {/if}
			{if !$smarty.foreach.sectionDetail.last}<input type="button" name="s_movedown" class="s_movedown" value="move down"/> {/if}
			<input type="button" name="s_delete" class="s_delete" value="delete"/>
		</div>

		<input type="hidden" name="s_id" class="s_id" value="{$sectionDetail.id}"/>
		<input type="hidden" name="s_sortorder"  class="s_sortorder" value="{$sectionDetail.sort_order}"/>
		

	</div>
	{/foreach}
	{/if}

		<div class="section-block">
		<div class="row">
			<label>Title*</label>
			<input type="text" name="s_title" class="text s_title" value=""/> <br />
		</div>
		<div class="row">
			<label>Description</label>
			<textarea rows="8"  name="s_desc" class="s_desc"/></textarea> <br />
		</div>
		<div class="row">
			<label>Link for Title *</label>
			<input type="text" name="s_titlelink" class="text s_titlelink" value=""/>  <br />
		</div>
		<div class="row">
			<label>Solr Link *</label>
			<input type="text" name="s_solrlink" class="text s_solrlink" value=""/> <span>e.g: <b>fabindia?sortby=RECENCY</b> (Do not apply filters using #! in the URL) </span><br />
		</div>
		<div class="row">
			<label>No of products to fetch *</label>
			<input type="text" name="s_noofproducts" class="text s_noofproducts" value=""/> <span>No of products to be displayed in multiples of 5 </span> <br />
		</div>
		<div class="row">
			<input type="checkbox" name="s_carousel" class="s_carousel" ><label> isCarousel</label> <br />
		</div>
		
		<div class="row">
			<input type="button" name="s_save" value="save" class="s_save" /> 
			<input type="button" name="s_cancel" class="s_cancel" value="cancel"/>
		</div>
		<input type="hidden" name="s_sortorder" class="s_sortorder" value="{$sortOrderForNextSection}"/>

	</div>
	<form  id="section-form" name="section-form" method="POST" action="{$postBackURL}?lpid={$lpid}">
		
		<input type="hidden" name="actionType" value="add"/>
		<input type="hidden" name="lpid" value="{$lpid}"/>
		<input type="hidden" name="title" value=""/>
		<input type="hidden" name="target_url" value=""/>
		<input type="hidden" name="url" value=""/>
		<input type="hidden" name="description" value=""/>
		<input type="hidden" name="no_of_products" value=""/>
		<input type="hidden" name="sort_order" value="{$sortOrderForNextSection}"/>
		<input type="hidden" name="is_carousel" value="0"/>
		<input type="hidden" name="section_id" value="0"/>
		<input type="hidden" name="section_id_to_exchange" value="0"/>
		<input type="hidden" name="new_sort_order" value="0"/>
		
	</form>
</div>
{literal}
<script>
$(document).ready(function(){
	var formToSubmit = $('#section-form');
	$('.s_save').on('click',function(){
		if(sectionValidate($(this))){
			setFormValuesAndSubmit($(this),'add');
		}
	});
	
	$('.s_update').on('click',function(){
		if(sectionValidate($(this))){
			setFormValuesAndSubmit($(this),'update');
		}
	});

	$('.s_cancel').on('click',function(){
		window.location=window.location;
	});

	$('.s_moveup').on('click',function(){

		setSortOrderAndSubmit($(this),'moveUp');
	});

	$('.s_movedown').on('click',function(){
		setSortOrderAndSubmit($(this),'moveDown');
	});


	$('.s_delete').on('click',function(){
		setFormValuesAndSubmit($(this),'delete');
	});

	var setSortOrderAndSubmit = function(elm,mode){
		var detailsBlock = elm.closest('.section-block');
		var new_sort_order,swp_sid,swpBlock;	

		formToSubmit.find('input[name="actionType"]').val(mode);
		if(mode=='moveUp'){
			swpBlock = $('.section-block').eq(detailsBlock.index()-2);

		}
		else{
			console.log('moveDown',detailsBlock.index());
			swpBlock = $('.section-block').eq(detailsBlock.index());

		}

		new_sort_order = swpBlock.find('.s_sortorder').val();
		swp_sid = swpBlock.find('.s_id').val();
		//console.log(detailsBlock.find('input[name="s_id"]').val());
		//console.log(swp_sid);
		formToSubmit.find('input[name="section_id"]').val(detailsBlock.find('.s_id').val());
		formToSubmit.find('input[name="new_sort_order"]').val(new_sort_order);
		formToSubmit.find('input[name="section_id_to_exchange"]').val(swp_sid);
		formToSubmit.find('input[name="sort_order"]').val(detailsBlock.find('.s_sortorder').val());
		

		formToSubmit.trigger('submit');
	};

	var setFormValuesAndSubmit = function(elm,mode){
		var detailsBlock = elm.closest('.section-block');

		formToSubmit.find('input[name="actionType"]').val(mode);
		formToSubmit.find('input[name="title"]').val(detailsBlock.find('.s_title').val());
		formToSubmit.find('input[name="target_url"]').val(detailsBlock.find('.s_titlelink').val());
		formToSubmit.find('input[name="url"]').val(detailsBlock.find('.s_solrlink').val());
		formToSubmit.find('input[name="description"]').val(detailsBlock.find('.s_desc').val());
		formToSubmit.find('input[name="no_of_products"]').val(detailsBlock.find('.s_noofproducts').val());
		formToSubmit.find('input[name="sort_order"]').val(detailsBlock.find('.s_sortorder').val());
		formToSubmit.find('input[name="is_carousel"]').val((detailsBlock.find('.s_carousel').is(':checked'))?1:0);
		formToSubmit.find('input[name="section_id"]').val(detailsBlock.find('.s_id').val());
		
		formToSubmit.trigger('submit');

	}
	var sectionValidate = function(elm){
		var detailsBlock = elm.closest('.section-block');
		if(detailsBlock.find('.s_title').val() &&  
				detailsBlock.find('.s_titlelink').val() &&
					detailsBlock.find('.s_solrlink').val() &&
						detailsBlock.find('.s_noofproducts').val()) {
				if(/^\d/.test(detailsBlock.find('.s_noofproducts').val())){
					return true;
				} 
		}
		return false;		

	}

	$('.lplanding').on('click',function(){
		window.location = http_loc+'/admin/landingpage/lpController.php?lpid='+formToSubmit.find('input[name="lpid"]').val();
	});

});
</script>
{/literal}