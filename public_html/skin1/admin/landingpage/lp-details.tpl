{literal}<style>
	#lp-details {
		padding:50px;
	}
	#lp-details .row {
		padding:10px;
	}
	#lp-details .row label{
		width:150px;
		display: inline-block;
		vertical-align: top;
	}
	
	#lp-details .row input.text{
		width:250px;
	}
	#lp-details .row textarea{
		width:250px;
	}

	.status-message {
		color:green;
		font-size:13px;
		text-align: center;
	}
{/literal}
</style>

<div id="lp-details">
	<h3> Create New/Edit</h3>

	<form  id="lp-details-form" name="lp-details-form" method="POST" action="{$postBackURL}">
		{if $status}
			<div class="row">
				<p class="status-message">{$status}</p>
			</div>
		{/if}
		<div class="row">
			<label>Title</label>
			<input type="text" name="lptitle" class="text lptitle"/> <br />
		</div>
		<div class="row">
			<label>Landing Page Name *</label>
			<input type="text" name="lpname" class="text lpname"/> <br />
		</div>
		<div class="row">
			<label>Subtitle</label>
			<input type="text" name="lpstitle" class="text lpstitle"/> <br />
		</div>
		<div class="row">
			<label>Description</label>
			<textarea rows="8" name="lpdesc" class="lpdesc"/></textarea> <br />
		</div>
		<div class="row">
			<label>Image URL</label>
			<input type="text" name="lpimage" class="text lpimage"/> <br />
		</div>
		
		<div class="row">
			<input type="button" name="lpsave" value="save" class="lpsave" /> 
			<input type="button" name="lpcancel" class="lpcancel" value="cancel"/>
		</div>

		<input type="hidden" name="lpid" value="" />
		<input type="hidden" name="actionType" value="add"/>

		<div class="row redirect-btns hide">
			<input type="button" name="lpbanner" value="add banners" class="lpbanner" /> 
			<input type="button" name="lpproducts" class="lpproducts" value="Add Product Sections"/>
		</div>
	</form>
</div>
{literal}
<script>
$(document).ready(function(){
	var locationsJson = {/literal}{$locationsJson}{literal};
	var locationsKVJson = {/literal}{$locationsKVJson}{literal};
	var lpid = {/literal}{if $pageLocationID}{$pageLocationID}{else}''{/if}{literal};	
			
	$(".page-location-autocomplete").autocomplete({
		source: locationsJson,
		appendTo: "#resultsDiv"
	});
	$(".page-locations-head .show-hide").bind("click",function(){
		$(".page-locations").toggle();
	});
	$(".location-link").bind("click",function(){
		$(".page-location-selected").val($(this).data('id'));
		$("#SetPageLocationForm").submit();
	});
	$("#resultsDiv ul li.ui-menu-item a.ui-corner-all").live("click", function(){
		
		for(key in locationsKVJson){
			if(locationsKVJson[key]==$(this).text()){
				$(".page-location-selected").val(key);
				break;
			}
		}
		
		$("#SetPageLocationForm").trigger("submit");
	});	

	//Create A new LP
	$('.page-locations-head .new').on('click',function(){
		//show new form with no data
		window.location = location.pathname;
	});


	var lpd=$('#lp-details-form');
	var lpDetails = {/literal}{if $lpDetails}{$lpDetails}{else}''{/if}{literal};
	
	if(lpid){

		//set the fields with value
		lpd.find('.lptitle').val(lpDetails.title);
		lpd.find('.lpname').val(lpDetails.lp_name);//.attr('disabled','true');
		lpd.find('.lpstitle').val(lpDetails.subtitle);//.attr('disabled','true');
		lpd.find('.lpimage').val(lpDetails.image);
		lpd.find('.lpdesc').val(lpDetails.description);
		lpd.find('input[name="lpid"]').val(lpDetails.id);
		lpd.find('input[name="actionType"]').val('update');

		//Also show the Banner and Product section button
		$('.redirect-btns').show();
	}

	//bind save and cancel

	$('.lpsave').on('click',function(){
		if(($('.lpname').val())){
			$('#lp-details-form').trigger('submit');
			return true;
		}
		return false;
	});
	$('.lpcancel').on('click',function(){
		window.location=window.location;
	});

	$('.lpbanner').on('click',function(){
		window.location = http_loc+'/admin/pageconfig/landingpage.php?pageLocation='+lpDetails.lp_name+'&pageLocationId='+lpDetails.id;
	});

	$('.lpproducts').on('click',function(){
		window.location = http_loc+'/admin/landingpage/sectionController.php?lpid='+lpDetails.id;
	});
});
</script>
{/literal}