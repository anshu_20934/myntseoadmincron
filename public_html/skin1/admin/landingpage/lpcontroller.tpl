<!-- Super block: 
includes (1) header: contains only CSS & JS andpagelocation chooser block
		 (2) multiple rules (section) blocks: 1 update block (containing multiple images), and 1 image-dialog within each rule 		   
-->
<div><H1>Landing Page Configuration</H1></div>
{include file="admin/landingpage/lp-header.tpl"}
{include file=admin/landingpage/lp-location-chooser.tpl}
{include file=admin/landingpage/lp-details.tpl}
{if $errorMessage neq ''}
<div class="errorDiv">
	<span class="errorMsg">{$errorMessage}</span>
	<span class="dismiss"> <a href="#" onclick="javascript: $('.errorDiv').hide()">Dismiss</a></span>
</div>
{/if}

