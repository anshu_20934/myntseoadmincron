{* $Id: menu.tpl,v 1.68.2.2 2006/06/20 05:52:05 svowl Exp $ *}

{capture name=menu}
<li><a href="{$catalogs.admin}/pageconfig/homepage.php" class="VertMenuItems">Home Page</a></li>
<li><a href="{$catalogs.admin}/pageconfig/newhomepage.php" class="VertMenuItems">New Home Page(nodejs)</a></li>
<li><a href="{$catalogs.admin}/pageconfig/newhomefeatures.php" class="VertMenuItems">New Home Features</a></li>
<li><a href="{$catalogs.admin}/pageconfig/newhomepagesubbanners.php" class="VertMenuItems">New Home Sub-Banners</a></li>
<li><a href="{$catalogs.admin}/pageconfig/searchpage.php" class="VertMenuItems">Search Page</a></li>
<li><a href="{$catalogs.admin}/landingpage/lpController.php" class="VertMenuItems">Landing Page</a></li>
<li><a href="{$catalogs.admin}/pageconfig/storyboard.php" class="VertMenuItems">Storyboard</a></li>
<li><a href="{$catalogs.admin}/pageconfig/loginpage.php" class="VertMenuItems">Login Page</a></li>
<li><a href="{$catalogs.admin}/pageconfig/topnav.php" class="VertMenuItems">Topnav Default Banner</a></li>
<li><a href="{$catalogs.admin}/pageconfig/fashionstorypage.php" class="VertMenuItems">Fashion Story Page</a></li>
<li><a href="{$catalogs.admin}/pageconfig/fashiontrendspage.php" class="VertMenuItems">Fashion Trends Page</a></li>
<li><a href="{$catalogs.admin}/pageconfig/mobilehomepage.php" class="VertMenuItems">Mobile Home Page</a></li>
<li><a href="{$catalogs.admin}/pageconfig/topnav_images.php" class="VertMenuItems">TopNav Images</a></li>
<li><a href="{$catalogs.admin}/pageconfig/new_signup_control.php" class="VertMenuItems">Signup Controls</a></li>
<li><a href="{$catalogs.admin}/pageconfig/new_login_page.php" class="VertMenuItems">New Login Page</a></li>
<li><a href="{$catalogs.admin}/pageconfig/new_login_page_big.php" class="VertMenuItems">New Login Page Big Banners</a></li>
{/capture}
{include file="menu.tpl" dingbats="dingbats_categorie.gif" menu_title="Page Configurators" menu_content=$smarty.capture.menu }
