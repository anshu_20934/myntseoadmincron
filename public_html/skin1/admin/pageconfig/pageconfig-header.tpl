{literal}
<link rel="stylesheet" type="text/css" href="/skin1/myntra_css/lightbox_admin.css">
<link rel="stylesheet" type="text/css" href="/skin1/myntra_css/pageconfig.css">
<script language="javascript" src="/skin1/js_script/jquery.validate.js"> </script>
<script language="javascript" src="/skin1/myntra_js/ns.js"> </script>
<script language="javascript" src="/skin1/myntra_js/mk-base.js"> </script>
<script language="javascript" src="/skin1/myntra_js/lightbox.js"> </script>
<script language="javascript" src="/skin1/myntra_js/jquery.ui.js"> </script>
<script type="text/javascript">
$(document).ready(function() {
	var numRules = {/literal}{$rules|@count}{literal};
	Myntra.dialogObj = [];
	Myntra.variantDialog = [];
	for(var i=0; i<numRules; i++) {
		Myntra.dialogObj[i] = Myntra.LightBox("#addBannerDiv-"+i);
		Myntra.variantDialog[i] = Myntra.LightBox('#addBannerDivVariant-'+i);
	}
	$(".addBannerLink").bind("click",function() {
		Myntra.dialogObj[$(this).attr("data-lightbox-id")].show();
	});
	$('form[name="add"]').validate({
		rules: {
			title: "required",
			alt_text: "required",
			target_url: "required",
			image: {
				required: "#image_url: blank"
			}
		}
	});
	$(".btndelete").bind("click",function() {
		
		var form = $(this).closest("form");
		var step = form.find("[name=step]").val();
		var section = form.find("[name=section]").val();
						
		var imageIds2del = "";
		var selectedCBs = form.find('input.cb-delete[type=checkbox]:checked');
		if(selectedCBs.length != step) {
			alert("You can delete only "+step+" images at a time in section: "+section);
			return;
		} 
		selectedCBs.each(function(i,el){
			if(imageIds2del === '') {
				imageIds2del = imageIds2del+$(el).val();
			} else {
				imageIds2del = imageIds2del+","+ $(el).val();
			}
		});
		var r=confirm("Are you sure you want to delete these banners?");
	    if(r == true){
	        form.find("input[name=actionType]").val({/literal}"{$deleteAction}"{literal});
	        form.find("input[name=imageId]").val(imageIds2del);
	        form.submit();
	    }; 
	});

	var parent_image_reduced_by = $(".parent_image").width()/$(".parent_image").attr("data-width");
	$(".child_image").parents(".DIV_TR_TD").css("width",$(".parent_image").width() + "px");
	$(".child_image").height($(".child_image").attr("data-height")*parent_image_reduced_by);
	$(".child_image").width($(".child_image").attr("data-width")*parent_image_reduced_by);
	$(".child_image").parent().css("top",374*parent_image_reduced_by+"px");
	$(".child_image").parent().css("left",150*parent_image_reduced_by+"px");	
});	


</script>
{/literal}
{if $pageLocationId}
<div class="link-lpcontroller">
	<a href="/admin/landingpage/lpController.php?lpid={$pageLocationId}"/> Back to Landing Page</a>
	
</div>
{else}
	{include file=admin/pageconfig/pageconfig-location-chooser.tpl}
{/if}

{if $errorMessage neq ''}
<div class="errorDiv">
	<span class="errorMsg">{$errorMessage}</span>
	<span class="dismiss"> <a href="#" onclick="javascript: $('.errorDiv').hide()">Dismiss</a></span>
</div>
{/if}

