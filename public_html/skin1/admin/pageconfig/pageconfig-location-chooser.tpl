<div><H1>{if $variantPage}Varient Configurator{else}Page Configuration{/if} for <span style="text-transform: capitalize;">{$locationType} - {$pageLocation}</span></H1></div>
{if !$variantPage}
{if $showLocation}
{literal}
<script language="javascript">
	$(document).ready(function(){
		var locationsJson = {/literal}{$locationsJson}{literal};
				
		$(".page-location-autocomplete").autocomplete({
			source: locationsJson,
			appendTo: "#resultsDiv"
		});
		$(".page-locations-head").bind("click",function(){
			$(".page-locations").toggle();
		});
		$(".location-link").bind("click",function(){
			$(".page-location-autocomplete").val($(this).text());
			$("#SetPageLocationForm").submit();
		});
		$("#resultsDiv ul li.ui-menu-item a.ui-corner-all").live("click", function(){
			$("#SetPageLocationForm").trigger("submit");
		});		
	});
</script>
{/literal}
<div class="section location-choose-head">
	<div class="section-title">
	{if $pageLocation eq ""}
	Choose a Page Location
	{else}
	Selected Page Location: {$pageLocation}
	{/if}
	</div>
	<div style="width:100%;">
		<form id="SetPageLocationForm" method="GET" action="{$postBackURL}">
			<div class="page-locations-head"> 
				<span class="show-hide"></span>
				<span style="padding-left: 10px">All Page-Locations</span>
				<span style="margin-left: 50px">
					<input class="page-location-autocomplete" type="text" name="pageLocation" value="{$pageLocation}" style="font-size: 15px"/>
					<input type="submit" value="Set Location" style="font-size: 15px"/>
				</span>	
			</div>
		</form>	
		<div class="page-locations hide">
			{foreach key=i name=locations item=location from=$locations}
				<span style="margin: 10px"><a class="location-link">{$location}</a></span> 
			{/foreach}
		</div>
	</div>
	<div id="resultsDiv" style="width: 300px; font-size: 1.2em;">
	</div>	
</div>
<div class="divider"></div>
{/if}
{/if}