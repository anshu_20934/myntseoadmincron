{if $rules}
	{assign var=i value=-1}
	{foreach name=rules key=index item=rule from=$rules}
	<div class="DIV_TABLE">
	{assign var=i value=$i+1}
	{include file="admin/pageconfig/pageconfig-imagerules.tpl"}
	{include file="admin/pageconfig/pageconfig-imagedialog.tpl"}
	{include file="admin/pageconfig/pageconfig-imagedialog-variant.tpl"}
	{if $imagesSets}
	{assign var=images value=$imagesSets.$i}
		{if $images}
		{if !$variantPage}
		<form enctype="multipart/form-data" action="/admin/pageconfig/editVariant.php" method="post" name='variant_update_{$i}' class='variantUpdateForm'>
			<input type="hidden" name="locationType" value="{$rule.location_type}"/>
			<input type="hidden" name="section" value="{$rule.section}"/>
			<input type="hidden" name="pageLocation" value="{$pageLocation}"/>
			<input type="hidden" name="imageRuleId" value="{$rule.image_rule_id}" />			
			<input type="hidden" class='parentImageId' name="parentImageId" value=""/>			
			<input type="hidden" class='variantFlag' name="variantFlag" value=""/>			
		</form>
		{/if} 
		<form enctype="multipart/form-data" action="{if !$variantPage}{$postBackURL}{else}/admin/pageconfig/editVariant.php{/if}" method="post" name="update_{$i}">
			{include file="admin/pageconfig/pageconfig-button-panel.tpl"}
			<input type="hidden" name="locationType" value="{$rule.location_type}"/>
			<input type="hidden" name="section" value="{$rule.section}"/>
			<input type="hidden" name="step" value="{if !$variantPage}{$rule.step}{else}1{/if}"/>
			<input type="hidden" name="pageLocation" value="{$pageLocation}"/>
			<input type="hidden" name="pageLocationId" value="{$pageLocationId}"/>
			<input type="hidden" name="actionType" value="{$updateAction}"/>
			<input type="hidden" name="imageId" />
			{if $variantPage}
			<input type='hidden' name='variantFlag' value='{$sVariantFlag}' />
			<input type='hidden' name='parentImageId' value='{$sParentImageId}' />
			<input type='hidden' name='imageRuleId' value='{$sImageRuleId}' />
			{/if}
				<div style="clear:both;"></div>
				{foreach name=banners item=image from=$images}
					<div class="DIV_TR bannerItemWrapper" style="float:none; height:100%">
						<div class="DIV_TR_TD" style="width:30px; height:100%">
							{$smarty.foreach.banners.iteration}
							<input type="hidden" name="image[{$image.image_id}][image_id]" value="{$image.image_id}"/>
							<input type="hidden" name="image[{$image.image_id}][image_rule_id]" value="{$image.image_rule_id}"/>
							{if $image.parent_image_id gt 0}
							<input type="hidden" name="image[{$image.image_id}][parent_image_id]" value="{$image.parent_image_id}"/>
							{/if}
						</div>
						{if $image.parent_image_id gt 0 && !$variantPage}
						{assign var=rule_id value=$rule.parent_rule_id}
						<div class="DIV_TR_TD" style="position:relative;">
						<div style="position: absolute; z-index: 1;">
                            <img class="banner parent_image" 
                                src="{if $image.parent_image_url|strpos:http !== false}{$image.parent_image_url}{else}{$http_location}/{$image.parent_image_url}{/if}" 
                            	data-height="{$rules.$rule_id.height}"
                                data-width="{$rules.$rule_id.width}"/>
                        </div>
						{/if}
						<div {if $image.parent_image_id gt 0 && !$variantPage}style="position: absolute; z-index: 2;"{else}class="DIV_TR_TD resizeWrapper"{/if}>
							<img class="banner {if $image.parent_image_id gt 0}child_image{/if}" 
								src="{if $image.image_url|strpos:http !== false}{$image.image_url}{else}{$http_location}/{$image.image_url}{/if}" 
								alt="{$image.alt_text}" 
								title="{$image.title}" 
								data-height="{$image.height}"
								data-width="{$image.width}"/>
							{if !$variantPage}
							<p>
								<a href='javascript:void(0);' class='addVariant' data-lightbox-id='addBannerDivVariant-{$i}' title='Add Variant' data-parent-id='{$image.image_id}' data-lightbox-index='{$i}'>Add Variant</a>
								<a href='#' class='editVariant' title='Edit Variants' data-parent-id='{$image.image_id}'>Edit Variants</a>
							</p>
							<div class='variantThumbnail' id='variantThumbnailId{$image.image_id}'>
								<h1>Variants</h1>
								{foreach item=variantItem from=$variantImages}
									{if $image.image_id == $variantItem.parent_image_id}
										<img src='{$variantItem.image_url}' alt='Variant Image' title='Group: {$variantItem.group_id}' />
									{/if}
								{/foreach}
							</div>
							{/if}
						</div>
						{if $image.parent_image_id gt 0  && !$variantPage}
						</div>
						{/if}
						<div class="DIV_TR_TD bannerItem" style="width:350px; height:100%">
							<div class="banner-details" style="padding-top: 2px">
								<span class="lblleft">Browse File to change Image:</span>
								<input name="image[{$image.image_id}][image_file]" type="file"/>
							</div>	
							<div class="banner-details">
								<div class="lblleft">Source URL</div>
								<input type="text" name="image[{$image.image_id}][image_url]" value="{$image.image_url}" size="40"/>
							</div>
							{if $variantPage}
							<div class='banner-details'>
								<div class="lblleft">User Group</div>
								<select name='image[{$image.image_id}][group_id]'>
									<option selected="selected" value='-1'>Select Group</option>
									{foreach item=userGroupItem from=$userGroup}
										<option value='{$userGroupItem.id}' {if $image.group_id == $userGroupItem.id}selected='selected'{/if}>{$userGroupItem.title}</option>
									{/foreach}
								</select> 
							</div>
							<div class='banner-details'>
								<div class="lblleft">Location Group</div>
								<select name='image[{$image.image_id}][location_group_id]'>
									<option selected="selected" value='-1'>Select Group</option>
									{foreach item=locationGroupItem from=$locationGroup}
										<option value='{$locationGroupItem.id}' {if $image.location_group_id == $locationGroupItem.id}selected='selected'{/if}>{$locationGroupItem.title}</option>
									{/foreach}
								</select> 
							</div>
							{if $rule.section == 'slideshow'}
							<div class='banner-details'>
								<div class='lblleft'>Variant Location</div>
								<select name="image[{$image.image_id}][variant_location]">
									<option value='replace' {if $image.variant_location == 'replace'}selected='selected'{/if}>Replace Image</option>
									<option value='first' {if $image.variant_location == 'first'}selected='selected'{/if}>Add As First Image</option>			
								</select>
							</div>
							{/if}
							{/if}							
							<div class="multi-check"><input class="cb-multi" name="image[{$image.image_id}][multiFlag]" type="checkbox" value="{if $image.multiclick eq 1}1{else}0{/if}" {if $image.multiclick eq 1}checked{/if}/><label>MultiClick Banner:</label></div>
							<div class="multi-config clear {if $image.multiclick eq 1}show{/if}">
								<a href="javascript:void(0);" class="addZone">Add Another Zone</a>				
								<ul class="zone-map" data-image-id="{$image.image_id}">
									{foreach from=$multiBannerData[$image.image_id] key=k item=imageMap}
										{if $imageMap.image_id}
										<li data-zone-index="{$k}">
											<h3>Zone {$k+1}</h3>
											<div class="coord"><label>X1:<sup>*</sup> </label><input class="coordinateValue" name="image[{$image.image_id}][multi][{$k}][0]}"  type="text" value="{$imageMap.coordinates[0]}" /></div>
											<div class="coord"><label>Y1:<sup>*</sup> </label><input class="coordinateValue" name="image[{$image.image_id}][multi][{$k}][1]}" type="text" value="{$imageMap.coordinates[1]}" /></div>
											<div class="coord"><label>X2:<sup>*</sup> </label><input class="coordinateValue" name="image[{$image.image_id}][multi][{$k}][2]}" type="text" value="{$imageMap.coordinates[2]}" /></div>
											<div class="coord"><label>Y2:<sup>*</sup> </label><input class="coordinateValue" name="image[{$image.image_id}][multi][{$k}][3]}" type="text" value="{$imageMap.coordinates[3]}" /></div>
											<div class="url"><label>URL:<sup>*</sup> </label><input class="urlValue" name="image[{$image.image_id}][multi][{$k}][target]}"type="text" value="{$imageMap.target_url}" /></div>
											<div class="title"><label>Title: </label><input type="text" class="titleValue" name="image[{$image.image_id}][multi][{$k}][title]}" value="{$imageMap.title}" /></div>
											<div class="addRemoveZone">											
												<a href="javascript:void(0);" class="removeZone">Remove Zone</a>				
											</div> 
										</li>
										{/if}
									{/foreach}
									</ul>
									<a href="javascript:void(0);" class="addZone">Add Another Zone</a>
							</div>
							<div class="banner-details singleTarget">
								<div class="lblleft">Target URL </div>
								<input type="text" name="image[{$image.image_id}][target_url]" value="{$image.target_url}" size="40"/>
							</div>
							<div class="banner-details">
								<div class="lblleft">Alt Text</div>
								<input type="text" name="image[{$image.image_id}][alt_text]" value="{$image.alt_text}" size="40"/>
							</div>							
							<div class="banner-details">
								<div class="lblleft">Title</div>
								<input type="text" name="image[{$image.image_id}][title]" value="{$image.title}" size="40"/>
							</div> 
							<div class="banner-details">
								<div class="lblleft">Description</div>
								<textarea name="image[{$image.image_id}][description]" cols="38" rows="4">{$image.description}</textarea>
								
							</div>
							<div class="banner-details">
								<div class="lblleft">Fashion Banners (Only for Search Pages)</div>
								<textarea name="image[{$image.image_id}][fashion_banners]" cols="38" rows="4">{$image.fashion_banners}</textarea>
							</div>
							{if $image.parent_image_id gt 0 && !$variantPage}
								<div class="banner-details"><input class="cb-delete" name="image[{$image.image_id}][delete]" type="checkbox" value="{$image.image_id}" text="Delete"/> <label>Delete:</label></div>
							{/if}
						</div>
						<div class="DIV_TR_TD" style="width:100px; height:100%;">
							<div style="padding:10px">
								<div class="banner-action"><input type="submit" value="Up" onclick="javascript: document.update_{$i}.actionType.value = '{$moveUpAction}';  document.update_{$i}.imageId.value = '{$image.image_id}'; document.update_{$i}.variantFlag.value = '{$variantPage}'; document.update_{$i}.submit();" /></div>
								<div class="banner-action"><input type="submit" value="Down" onclick="javascript: document.update_{$i}.actionType.value = '{$moveDownAction}';  document.update_{$i}.imageId.value = '{$image.image_id}'; document.update_{$i}.variantFlag.value = '{$variantPage}'; document.update_{$i}.submit();" /></div>
								<div class="banner-action"><label>Delete:</label><input class="cb-delete" name="image[{$image.image_id}][delete]" type="checkbox" value="{$image.image_id}"/></div>								
							</div>
						</div>
						{if $variantPage}
							<div class="DIV_TR_TD" style="width:100px; height:100%;">
								<div style="padding:10px">
									<div class="banner-action"><label>Delete:</label><input class="cb-delete" name="image[{$image.image_id}][delete]" type="checkbox" value="{$image.image_id}"/></div>								
								</div>
							</div>
						{/if}
					</div>
					<div style="clear:both;"></div>
				{/foreach}
				{include file="admin/pageconfig/pageconfig-button-panel.tpl"}
		</form>
		
		{/if}
	{/if}
	</div>
	<div class="divider"></div>
	{/foreach}
{/if}
<link rel="stylesheet" type="text/css"
    href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.1/themes/base/jquery-ui.css"/>
<script type="text/javascript" src="{$http_location}/skin1/admin/multiclick/multiclick.js"></script>