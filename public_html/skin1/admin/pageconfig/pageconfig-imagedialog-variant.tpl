{assign var=count value=0}
<div id="addBannerDivVariant-{$i}" class="lightbox">
	<div class="mod" style="width: 480px">
		<div class="hd"><H2>Add Variant Images</H2></div>
		<form enctype="multipart/form-data" action="{$postBackURL}" method="POST" name="add">
			<input type="hidden" name="locationType" value="{$rule.location_type}"/>
			<input type="hidden" name="pageLocation" value="{$pageLocation}"/>
			<input type="hidden" name="section" value="{$rule.section}"/>
			<input type="hidden" name="actionType" value="add"/>
			<table style="width: 100%">
				<tr>
					<td colspan="2"><hr/></td>
				</tr>
				<tr>
					<td colspan="2">{$count+1}. Enter Image Details</td>
				</tr>
				{if $rule.parent_images}
				<tr>
					<td>Select the {$rule.location_type}>>{assign var=rule_id value=$rule.parent_rule_id}{$rules.$rule_id.section} Image </td>
					<td>
						<select name="image[{$count}][parent_image_id]">
							{foreach name=parent_images item=parent_image_url key=parent_image_id from=$rule.parent_images}
								<option value="{$parent_image_id}">{$parent_image_id}</option>
							{/foreach}
						</select>
					</td>
				</tr>
				{/if}
				<tr>
					<td>Image File to upload</td>
					<td><input name="image[{$count}][image_file]" type="file" id="image_{$count}"/></td>
				</tr>
				<tr>
					<td colspan="2" style="text-align: center">OR</td>
				</tr>
				<tr>
					<td>Image Source URL (CDN)</td>
					<td><input type="text" id="image_url_{$count}" name="image[{$count}][image_url]" size="40"/></td>
				</tr>
				<tr>
					<td>Alt Text</td>
					<td><input type="text" name="image[{$count}][alt_text]" size="40"/></td>
				</tr>
				<tr>
					<td>Target URL(should start with /)</td>
					<td><input type="text" name="image[{$count}][target_url]" size="40"/></td>
				</tr>
				<tr class='userGroup'>
					<td>User Group</td>
					<td>
						<select name="image[{$count}][group_id]">
						<option selected="selected" value='-1'>Select Group</option>
						{foreach item=usg from=$userGroup}
							<option value='{$usg.id}'>{$usg.title}</option>								
						{/foreach}
						<input type='hidden' class='variantFlag' value='false' name='image[{$count}][variant_flag]' />
						<input type='hidden' class='parentImage' name='image[{$count}][parent_image_id]' />
					</select>
					</td> 
				</tr>
				<tr class='userGroup'>
					<td>Location Group</td>
					<td>
						<select name="image[{$count}][location_group_id]">
						<option selected="selected" value='-1'>Select Group</option>
						{foreach item=usg from=$locationGroup}
							<option value='{$usg.id}'>{$usg.title}</option>								
						{/foreach}
					</select>
					</td> 
				</tr>
				<tr>
					<td>Title</td>
					<td><input type="text" name="image[{$count}][title]" size="40"/></td>
				</tr>
				<tr>
					<td>Select Variant Location</td>
					<td>
						<select name="image[{$count}][variant_location]">
							<option value='replace' selected="selected">Replace Image</option>
							{if $rule.section == 'slideshow'}
							<option value='first'>Add As First Image</option>
							{/if}
						</select>
					</td> 
				</tr>
				<tr>
					<td>Description</td>
					<td><textarea name="image[{$count}][description]" rows="5" cols="38" style="border: 1px solid lightgray"> </textarea></td>
				</tr>
				<tr>
					<td>Fashion Banners (Only for Search Pages)</td>
					<td><textarea name="image[{$count}][fashion_banners]" rows="5" cols="38" style="border: 1px solid lightgray"> </textarea></td>
				</tr>
				<tr>
					<td colspan="2"><hr/></td>
				</tr>	
				<tr>
					<td colspan="2" align="center">
						<input type="submit" value="Add" style="font-size: larger"/>
					</td>
				</tr>				
			</table>
		</form>
	</div>
</div>
<div class="divider">&nbsp;</div>
