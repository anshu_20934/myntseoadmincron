<div id="addBannerDiv-{$i}" class="lightbox">
	<div class="mod" style="width: 480px">
		<div class="hd"><H2>Add {if $rule.parent_rule_id>0}{assign var=rule_id value=$rule.parent_rule_id}{$rules.$rule_id.section} >> {/if}{$rule.section} image(s) for {$rule.location_type} - {$pageLocation} </H2></div>
		<form enctype="multipart/form-data" action="{$postBackURL}" method="POST" name="add">
			<input type="hidden" name="locationType" value="{$rule.location_type}"/>
			<input type="hidden" name="pageLocation" value="{$pageLocation}"/>
			<input type="hidden" name="section" value="{$rule.section}"/>
			<input type="hidden" name="actionType" value="add"/>
			<table style="width: 100%">
				<tr>
					<td colspan="2"><hr/></td>
				</tr>
				{section name=dialogfields loop=$rule.step}
					{assign var=count value=$smarty.section.dialogfields.index}
				<tr>
					<td colspan="2">{$count+1}. Enter Image Details</td>
				</tr>
				{if $rule.parent_images}
				<tr>
					<td>Select the {$rule.location_type}>>{assign var=rule_id value=$rule.parent_rule_id}{$rules.$rule_id.section} Image </td>
					<td>
						<select name="image[{$count}][parent_image_id]">
							{foreach name=parent_images item=parent_image_url key=parent_image_id from=$rule.parent_images}
								<option value="{$parent_image_id}">{$parent_image_id}</option>
							{/foreach}
						</select>
					</td>
				</tr>
				{/if}
				<tr>
					<td>Image File to upload</td>
					<td><input name="image[{$count}][image_file]" type="file" id="image_{$count}"/></td>
				</tr>
				<tr>
					<td colspan="2" style="text-align: center">OR</td>
				</tr>
				<tr>
					<td>Image Source URL (CDN)</td>
					<td><input type="text" id="image_url_{$count}" name="image[{$count}][image_url]" size="40"/></td>
				</tr>
				<tr>
					<td>Alt Text</td>
					<td><input type="text" name="image[{$count}][alt_text]" size="40"/></td>
				</tr>
				<tr>
					<td>Target URL(should start with /)</td>
					<td><input type="text" name="image[{$count}][target_url]" size="40"/></td>
				</tr>
				<tr>
					<td>Title</td>
					<td><input type="text" name="image[{$count}][title]" size="40"/></td>
				</tr>
				<tr>
					<td>Description</td>
					<td><textarea name="image[{$count}][description]" rows="5" cols="38" style="border: 1px solid lightgray"> </textarea></td>
				</tr>
				<tr>
					<td>Fashion Banners (Only for Search Pages)</td>
					<td><textarea name="image[{$count}][fashion_banners]" rows="5" cols="38" style="border: 1px solid lightgray"> </textarea></td>
				</tr>
				<tr>
					<td colspan="2"><hr/></td>
				</tr>	
				{/section}
				<tr>
					<td colspan="2" align="center">
						<input type="submit" value="Add" style="font-size: larger"/>
					</td>
				</tr>				
			</table>
		</form>
	</div>
</div>
<div class="divider">&nbsp;</div>
