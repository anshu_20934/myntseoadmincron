<div class="section">
	{if !$variantPage}
	<div class="section-title">{$rule.location_type} 
	>>
{if $rule.parent_rule_id>0}{assign var=rule_id value=$rule.parent_rule_id}{$rules.$rule_id.section} >> {/if} 
{if $pageLocation}
{$pageLocation} >>
{/if} 
	{$rule.section}  <br/> 
		<a class="addBannerLink" data-lightbox-id="{$i}" href="#">Add more banner(s) >></a>
	</div>
	{/if}
		<!-- <input type="button" value="Add more banner(s)" class="addBannerLink" data-lightbox-id="{$i}" /> -->
	<div class="section-body">Config rules for {$rule.location_type} - Section: {$rule.section}<br/>
	
	{if $rule.height != -1}
	Image Dimensions: {$rule.width} x {$rule.height} pixels ; Max file-size: {$rule.size/1024} KB
	{else}
	Image Dimensions: {$rule.width} x (variable height) pixels ; Max file-size: {$rule.size/1024} KB
	Each batch of images uploaded need to have the same height in pixels
	{/if}
	{if !$variantPage}<br/>Number of images:  Min: {$rule.min}, Max: {$rule.max} in steps of {$rule.step}<br/>{/if}
	</div>
</div>
