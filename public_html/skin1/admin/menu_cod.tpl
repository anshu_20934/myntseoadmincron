{* $Id: menu.tpl,v 1.68.2.2 2006/06/20 05:52:05 svowl Exp $ *}
{capture name=menu}
<li><a href="{$catalogs.admin}/cod_reconcile.php" class="VertMenuItems">COD Admin </a></li>
<li><a href="{$catalogs.admin}/cod_order_report.php" class="VertMenuItems">COD Reports</a></li>
<li><a href="{$catalogs.admin}/cod_on_hold_orders.php" class="VertMenuItems">COD On-Hold Orders</a></li>
<li><a href="{$catalogs.admin}/cod_oh_bulk_update.php" class="VertMenuItems">Update COD On-Hold Orders Bulk</a></li>
<li><a href="{$catalogs.admin}/cod_onhold_settings.php" class="VertMenuItems">COD On-Hold Settings</a></li>
{/capture}
{include file="menu.tpl" dingbats="dingbats_categorie.gif" menu_title="Cash on Delivery" menu_content=$smarty.capture.menu }