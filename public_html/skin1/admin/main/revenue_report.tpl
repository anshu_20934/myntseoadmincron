{* $Id:orders_search.tpl, v1.00.0.0, 2008/09/23 6:08:00 max Exp $ *}
{include file="main/include_js.tpl" src="main/popup_product.js"}

{capture name=dialog}
<form name="revenueform" action="revenue_report.php" method="GET">
	<table cellpadding="3" cellspacing="1" width="100%">
		<tr>
			<td width="15%"><b>Revenue Report for </b></td>
			<td width="15%">
				{if $smarty.post.term}
				    {assign var=period value=$smarty.get.term}
				{elseif $smarty.get.term}
				    {assign var=period value=$smarty.get.term}
				{/if} 				
				<select name="term">
					<option value="daily" {if $period eq "daily"}selected{/if}>--today--</option>            	
				    <option value="weekly" {if $period eq "weekly"}selected{/if}>--this week--</option>
				    <option value="monthly" {if $period eq "monthly"}selected{/if}>--this month--</option>
				    <option value="quarterly" {if $period eq "quarterly"}selected{/if}>--this quarter--</option>
				    <!--<option value="halfyearly">this half</option>-->
				    <option value="yearly" {if $period eq "yearly"}selected{/if}>--this year--</option>                
				</select>
			</td>        
			<td width="10%"><b>Revenue by: </b></td>
			<td width="10%">
				{if $smarty.post.group_by}
				    {assign var=g_by value=$smarty.get.group_by}
				{elseif $smarty.get.group_by}
				    {assign var=g_by value=$smarty.get.group_by}
				{/if}
				<select name="group_by">
					<option value="source_name" {if $g_by eq "source_name"}selected{/if}>--source--</option>            	
				    <option value="source_type" {if $g_by eq "source_type"}selected{/if}>--source type--</option>
				    <option value="source_location" {if $g_by eq "source_location"}selected{/if}>--source location--</option>				    
				</select>
			</td>
			<td><input type="submit" name="generate_revenue" value="Generate"/></td>
		</tr>
		</table>
</form>
<br/><br/>
	<form action="" method="get" name="revenuereport">		
		<table cellpadding="2" cellspacing="1" width="100%">
			<tr class="tablehead">
				<th width="5%" nowrap="nowrap">
					<a href="{$navigation_script}&amp;order_by=source_type{if $smarty.get.sort_order=='desc' && $smarty.get.order_by=='source_type'}&amp;sort_order=asc{else}&amp;sort_order=desc{/if}">
						Source Type
						{if $smarty.get.sort_order=='desc' && $smarty.get.order_by=='source_type'}
							<img src="{$cdn_base}/skin1/images/r_bottom.gif" />
							{elseif $smarty.get.sort_order=='asc' && $smarty.get.order_by=='source_type'}
							<img src="{$cdn_base}/skin1/images/r_top.gif"/>
						{else}
						{/if}
					</a>
				</th>
				
				<th width="5%" nowrap="nowrap">
					<a href="{$navigation_script}&amp;order_by=source_name{if $smarty.get.sort_order=='desc' && $smarty.get.order_by=='source_name'}&amp;sort_order=asc{else}&amp;sort_order=desc{/if}">
						Source
						{if $smarty.get.sort_order=='desc' && $smarty.get.order_by=='source_name'}
							<img src="{$cdn_base}/skin1/images/r_bottom.gif" />
							{elseif $smarty.get.sort_order=='asc' && $smarty.get.order_by=='source_name'}
							<img src="{$cdn_base}/skin1/images/r_top.gif"/>
						{else}
						{/if}
					</a>
				</th>
				<th width="5%" nowrap="nowrap">
					<a href="{$navigation_script}&amp;order_by=source_location{if $smarty.get.sort_order=='desc' && $smarty.get.order_by=='source_location'}&amp;sort_order=asc{else}&amp;sort_order=desc{/if}">
						Location
						{if $smarty.get.sort_order=='desc' && $smarty.get.order_by=='source_location'}
							<img src="{$cdn_base}/skin1/images/r_bottom.gif" />
							{elseif $smarty.get.sort_order=='asc' && $smarty.get.order_by=='source_location'}
							<img src="{$cdn_base}/skin1/images/r_top.gif"/>
						{else}
						{/if}
					</a>
				</th>
				
				<th width="5%" nowrap="nowrap">
					<a href="{$navigation_script}&amp;order_by=revenue{if $smarty.get.sort_order=='desc' && $smarty.get.order_by=='revenue'}&amp;sort_order=asc{else}&amp;sort_order=desc{/if}">
						revenue
						{if $smarty.get.sort_order=='desc' && $smarty.get.order_by=='revenue'}
							<img src="{$cdn_base}/skin1/images/r_bottom.gif" />
							{elseif $smarty.get.sort_order=='asc' && $smarty.get.order_by=='revenue'}
							<img src="{$cdn_base}/skin1/images/r_top.gif"/>
						{else}
						{/if}
					</a>
				</th>					
			</tr>
			{section name=order loop=$report_result}
				<tr class="{cycle values=",TableSubHead"}">
					<td align="center">
					{if $g_by eq "source_location"}
						
					{else}
						{$report_result[order].type_name}
					{/if}						
					</td>
					<td align="center">
					{if ($g_by eq "source_type" or $g_by eq "source_location")}
						{if $g_by eq "source_type"}
							Revenue from all sources of "<b>{$report_result[order].type_name}</b>"
						{else}
							Revenue from all sources of "<b>{$report_result[order].location_name}</b>"
						{/if}
					{else}
						{$report_result[order].source_name}
					{/if}
					</td>
					<td align="center">
					{if $g_by eq "source_type"}
					
					{else}
						{$report_result[order].location_name}
					{/if}
					</td>
					<td align="center">
						{$report_result[order].revenue}
					</td>					
				</tr>
			{/section}			
		</table>
	</form>
	{*include file="customer/main/navigation.tpl"*}
{/capture}
{include file="dialog.tpl" title="Revenue Report" content=$smarty.capture.dialog extra='width="100%"'}

