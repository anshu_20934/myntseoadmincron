{* $Id: statistics.tpl,v 1.24.2.1 2006/06/16 10:47:41 max Exp $ *}
{include file="main/include_js.tpl" src="main/calendar2.js"}
{capture name=dialog}

<form action="" method="post" name="reportbycityform">
<input type="hidden" name="mode"  value="search" />

<table cellpadding="1" cellspacing="1">


<tr>
	<th align="right">{$lng.lbl_reporttype}:</th>
	<td>
	     <select name='report_type'>
	     <option value="week"  { if $reportType == "week"} selected {/if} >Weekly</option>
              <option value="month" { if $reportType == "month"} selected {/if} >Monthly</option> 
	      
	            
           </select>
	</td>
	<th align="right">{$lng.lbl_region}:</th>
	<td>
	    <select name='region'>
	      <option value="all"  {if  $rgn eq 'all'} selected {/if} >All</option>
               {section name=idx loop=$regions}
                  <option value="{$regions[idx].code}" {if $regions[idx].code eq $rgn} selected {/if} >{$regions[idx].state}</option>
	       {/section}
           </select>
	</td>
</tr>

<tr>
	<th align="right">{$lng.lbl_report_from}:</th>
	<td><input type="text" name='Startdate' value="{$SDate}"/><a href="javascript:cal4.popup();" ><img src="{$cdn_base}/images/cal.gif" width="16" height="16" border="0" alt="Click Here to Pick up the date"></a></td>
	<th align="right">{$lng.lbl_report_to}:</th>
	<td><input type="text" name='Enddate'  value="{$EDate}" /><a href="javascript:cal5.popup();" class=""><img src="{$cdn_base}/images/cal.gif" width="16" height="16" border="0" alt="Click Here to Pick up the date"></a></td>
</tr>


<tr>
	<td colspan="4"><input type="submit" value="{$lng.lbl_submit|escape}" /></td>
</tr>
<script language="javascript">
      var cal4 = new calendar2(document.forms['reportbycityform'].elements['Startdate']);
      cal4.year_scroll = true;
      cal4.time_comp = false;

       var cal5 = new calendar2(document.forms['reportbycityform'].elements['Enddate']);
      cal5.year_scroll = true;
      cal5.time_comp = false;


</script>

</table>
</form>

{/capture}
{include file="dialog.tpl" title='' content=$smarty.capture.dialog extra='width="100%"'}

<br />
<br />
{if $mode}
{capture name=dialog}
<table cellpadding="1" cellspacing="1" width="100%">

<tr class="TableHead">
		
	<td >{$lng.lbl_duration}</td>
	<td >{$lng.lbl_city}</td>
	<td >{$lng.lbl_orders}</td>
	<td >{$lng.lbl_sales}(Rs)</td>
	
</tr>


{if $segmentby_city}

{section name=num loop=$segmentby_city}

           
    {section name=idx loop=$segmentby_city[num]}

<tr{cycle values=', class="TableSubHead"'}>
	
        <td align="center">{$segmentby_city[num][idx].duration1}-{$segmentby_city[num][idx].duration2}</td>
	<td align="center">{$segmentby_city[num][idx].s_city}</td>
	<td align="center">{$segmentby_city[num][idx].num_of_orders}</td>
	<td align="center">{$segmentby_city[num][idx].price * $segmentby_city[num][idx].num_of_orders}</td>
</tr>
{/section}
{/section}
<tr{cycle values=', class="TableSubHead"'}>
	
        <td align="center"><b>Total</td>
	<td align="center">&nbsp;</td>
	<td align="center"><b>{$totalOrders}</b></td>
	<td align="center"><b>{$totalSales}</b></td>
</tr>

{else}

<tr>
	<td colspan="4" align="center">{$lng.txt_no_result}</td>
</tr>

{/if}


</table>


</form>

<br />

{/capture}
{include file="dialog.tpl" title='' content=$smarty.capture.dialog extra='width="100%"'}
{/if}