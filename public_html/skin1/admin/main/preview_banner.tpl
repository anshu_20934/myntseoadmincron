{* $Id: preview_banner.tpl,v 1.13 2005/12/07 14:07:21 max Exp $ *}
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
{ config_load file="$skin_config" }
<html>
<head>
	<title>{$lng.lbl_preview_banner}</title>
	{ include file="meta.tpl" }
	<link rel="stylesheet" href="{$SkinDir}/{#CSSFile#}" />
</head>
{if $mode eq ''}
<body onload="javascript: if (window.opener && window.opener.document.getElementById('banner_body')) document.previewform.preview.value = window.opener.document.getElementById('banner_body').value; document.previewform.submit();"{$reading_direction_tag}>
<form action="preview_banner.php" method="post" name="previewform">
<input type="hidden" name="type" value="preview" />
<input type="hidden" name="preview" value="" />
</form>
{else}
<body{$reading_direction_tag}>
<table cellpadding="0" cellspacing="0" class="PopupContainer">
<tr>
	<td class="PopupTitle">{$lng.lbl_preview_banner}</td>
</tr>
<tr>
	<td height="1"><img src="{$cdn_base}/skin1/images/spacer.gif" class="Spc" alt="" /></td>
</tr>
<tr>
	<td class="PopupBG" height="1"><img src="{$cdn_base}/skin1/images/spacer.gif" class="Spc" alt="" /></td>
</tr>

<tr>
	<td height="380" valign="top">

<table width="100%" cellpadding="15" cellspacing="0">
<tr>
	<td valign="middle" align="center" height="350">

<table cellspacing="1" cellpadding="0" bgcolor="#000000">
<tr bgcolor="#ffffff">
	<td>{$banner}</td>
</tr>
</table>

	</td>
</tr>
</table>

	</td>
</tr>

<tr>
	<td>{include file="popup_bottom.tpl"}</td>
</tr>
</table>
{/if}
</body>
</html>
