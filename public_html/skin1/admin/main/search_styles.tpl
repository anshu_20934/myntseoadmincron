<script type="text/javascript">
    var brandsData         = {$brands};
    var ageGroupsData      = {$ageGroups};
    var gendersData        = {$genders};
    var fashionTypesData   = {$fashionTypes};
    var coloursData        = {$colours};
    var seasonsData        = {$seasons};
    var yearsData          = {$years};
    var usagesData         = {$usages};
    
    var masterCategoryData = {$masterCategory};
	var subCategoryData    = {$subCategory};
	var articleTypeData    = {$articleType};

	var styleStatusData    = {$styleStatus};
	var progresskey        = '{$progresskey}';
    var httpLocation       = '{$http_location}';
    
</script>

<link rel="stylesheet" type="text/css" href="{$http_location}/admin/extjs/resources/css/ext-all.css"/>
<script type="text/javascript" src="{$http_location}/admin/extjs/adapter/ext/ext-base.js"></script>
<script type="text/javascript" src="{$http_location}/admin/extjs/ext-all.js"></script>
<script type="text/javascript" src="{$http_location}/admin/extjs/plugins/superselectbox/SuperBoxSelect.js"></script>
<link rel="stylesheet" type="text/css" href="{$http_location}/admin/extjs/plugins/superselectbox/superboxselect.css"/>
<script type="text/javascript" src="search_styles.js"></script>

<div id="contentPanel"></div>