{* $Id: register.tpl,v 1.51.2.2 2006/06/29 13:31:45 svowl Exp $ *}
{literal}
   <script language="Javascript" >
    		function uploadNew(divObj1,divObj2)
		{
			  divObj1.style.visibility = "hidden";
			  divObj1.style.display = "none";

			  divObj2.style.visibility = "visible";
		      divObj2.style.display = "";
			  
		}
		function cancelUpload(divObj1,divObj2)
		{
			 divObj1.style.visibility = "visible";
			 divObj1.style.display = "";

			  divObj2.style.visibility = "hidden";
		      divObj2.style.display = "none";
		}
		function uploadFile(fileObj,uploadType)
                 {
	
			if (!/(\.(jpg|jpeg|png|gif))$/i.test(fileObj.value))
			{
				alert("Please upload  (jpg | jpeg | png | gif) image.");
				fileObj.focus();
				//return false;
			}
		        else if(fileObj.value == "")
			{
			     alert("Please browse an image.")
			     fileObj.focus();
			     //return false
		        }
		        else
			{
				
				 document.getElementById("action").value = "Upload";
				 document.registerform.submit();
			}
    
              }
   </script>
{/literal}
{capture name=dialog}

<form action="" method="post" name="registerform" enctype="multipart/form-data">
  <input type="hidden" name="affId" id="affId" value="{$affId}" />
  <input type="hidden" name="mode" id="mode" value="{$mode}" />
  <input type="hidden" name="action" id="action"  />

	<table cellspacing="1" cellpadding="2" width="100%">
	<tbody>
	    
	     {if $mode ne 'modify'}
		
		<tr>
		<td align="right"><b>Company Name</b></td>
		<td>&nbsp;</td>
		<td nowrap="nowrap">
		{$userinfo.company_name}
		
		</td>
		</tr>
	
		<tr>
		<td align="right"><b>Contact Name</b></td>
		<td>&nbsp;</td>
		<td nowrap="nowrap">
		 {$userinfo.contact_name}
		</td>
		</tr>
	
		<tr>
		<td align="right"><b>Contact Email</b></td>
		<td>&nbsp;</td>
		<td nowrap="nowrap">
		{$userinfo.contact_email}
		</td>
		</tr>
	
		<tr>
		<td align="right"><b>Contact Phone1</b></td>
		<td>&nbsp;</td>
		<td nowrap="nowrap">
		{$userinfo.contact_phone1}
		</td>
		</tr>
	
	
		<tr>
		<td align="right"><b>Contact Phone2</b></td>
		<td>&nbsp;</td>
		<td nowrap="nowrap">
		 {$userinfo.contact_phone2}
		</td>
		</tr>
		<tr>
		<td align="right"><b>Affiliate URL</b></td>
		<td>&nbsp;</td>
		<td nowrap="nowrap">
		  {$userinfo.affiliate_return_url}
		</td>
		</tr>
		
		<tr>
		<td align="right"><b>Title</b></td>
		<td>&nbsp;</td>
		<td nowrap="nowrap">
		 {$userinfo.title}
		</td>
		</tr>
		
		
		<tr>
		<td align="right"><b>Template</b></td>
		<td>&nbsp;</td>
		<td nowrap="nowrap">
		 {$userinfo.themid}
		</td>
		</tr>
		
		<tr>
		<td align="right"><b>Auth Mechanism</b></td>
		<td>&nbsp;</td>
		<td nowrap="nowrap">
		  {$userinfo.aff_auth_mechanism}
		</td>
		</tr>
		
		<tr>
		<td align="right"><b>Joined Date</b></td>
		  <td>&nbsp;</td>
		<td nowrap="nowrap">
		   {$userinfo.joined_date|date_format}
		</td>
		</tr>
		
		<tr>
		<td align="right"><b>Logo</b></td>
		  <td>&nbsp;</td>
		<td nowrap="nowrap">
		   <img src="{$cdn_base}/images/affiliates/{$userinfo.affiliate_id}/logo/{$userinfo.logo}" />
		</td>
		</tr>

		
		
		<tr>
		<td align="right"><b>Content Provider</b></td>
		<td>&nbsp;</td>
		<td nowrap="nowrap">
		 {$userinfo.contentprovider}
		</td>
		</tr>

		<tr>
		<td align="right"><b>Active</b></td>
		<td>&nbsp;</td>
		<td nowrap="nowrap">
		     <select name="act_status"> 
			 <option value='Y' {if $userinfo.active eq 'Y'}selected {/if} >Yes</option>
			 <option value='N'  {if $userinfo.active eq 'N'} selected {/if}>No</option>
			  <option value='C'  {if $userinfo.active eq 'C'} selected {/if}>Rejected</option>
		      </select>
		</td>
		</tr>
	
		<tr>
		<td colspan="2">&nbsp;</td>
		<td>
          {else}

	        <tr>
		   <td colspan="6"><span style="font-size:1.2em;font-weight:bold;">Contact Info</span></td>
		</tr>
                
		 <tr>
		<td align="right"><b>Company Name</b></td>
		<td>&nbsp;</td>
		<td nowrap="nowrap"><input type="text" name="company" id="company"  value="{$userinfo.company_name}"  size="25"/>
		
		</td>
		<td align="right"><b>Name</b></td>
		<td>&nbsp;</td>
		<td nowrap="nowrap">
		 <input type="text"  name="contact" id="contact"  value="{$userinfo.contact_name}"  size="25"/>
		</td>
		</tr>
	
		
	
		<tr>
		<td align="right"><b>Email</b></td>
		<td>&nbsp;</td>
		<td nowrap="nowrap">
		  <input type="text" name="email" id="email"  value="{$userinfo.contact_email}" size="25"/>
		</td>

		<td align="right"><b>Phone1</b></td>
		<td>&nbsp;</td>
		<td nowrap="nowrap">
		<input type="text"  name="phone1" id="phone1"  value="{$userinfo.contact_phone1}" size="25" />
		</td>
		</tr>
	
		
	
		<tr>
		<td align="right"><b>Phone2</b></td>
		<td>&nbsp;</td>
		<td nowrap="nowrap">
		<input type="text" name="phone2" id="phone2"  value="{$userinfo.contact_phone2}"  size="25"/> 
		</td>

		<td align="right"><b>URL</b></td>
		<td>&nbsp;</td>
		<td nowrap="nowrap">
		  <input type="text" name="url" id="url"  value="{$userinfo.affiliate_return_url}"  size="25" />
		</td>
		</tr>
		
		   <tr>
		       <td colspan="6"><br/><span style="font-size:1.2em;font-weight:bold;">Personalize</span></td>
		    </tr>

		<tr>
		<td align="right"><b>Title</b></td>
		<td>&nbsp;</td>
		<td nowrap="nowrap">
		 <input type="text"  name="title" id="title"  value="{$userinfo.title}"  size="25"/> 
		</td>

		<td align="right"><b>Template</b></td>
		<td>&nbsp;</td>
		<td nowrap="nowrap">
		<select name="theme" id="theme">
		 {section name="theme" loop=$themes }
					     <option value="{$themes[theme].KEY}" {if $userinfo.themeid eq $themes[theme].KEY} selected {/if}>{$themes[theme].VALUE}</option>
					      {/section}
		   </select>
		</td>
		</tr>
		
		
		
		<tr>
		<td align="right"><b>Authentication  Mode</b></td>
		<td>&nbsp;</td>
		<td nowrap="nowrap">
		     <select name="authmode" id="authmode">
		                  {section name=authidx loop = $userAuthDetail }
						 <option value="{$userAuthDetail[authidx].affiliate_auth_type}" {if $userinfo.aff_auth_mechanism eq $userAuthDetail[authidx].affiliate_auth_type} selected  {/if}>{$userAuthDetail[authidx].aff_authtype_desc}</option>
				  {/section}
                   </select>
		</td>

		<td align="right"><b>Logo</b></td>
		  <td>&nbsp;</td>
		<td nowrap="nowrap">
		   
		    <div id="affLogo1" style="visibility:visible;">
					<img src="{$cdn_base}/images/affiliates/{$userinfo.affiliate_id}/logo/{$userinfo.logo}"  ALT="logo" />&nbsp;&nbsp;<a href="javascript:uploadNew(document.getElementById('affLogo1'),document.getElementById('affLogo2'));" >Change</a> 
		    </div>
		     <div id="affLogo2" style="visibility:hidden;display:none;">
				   <input type="file"   name="affiliatelogo" id="affiliatelogo"  />&nbsp;&nbsp;<a href="javascript:uploadFile(document.getElementById('affiliatelogo'),'logo');" >Upload Logo</a>&nbsp;<a href="javascript:cancelUpload(document.getElementById('affLogo1'),document.getElementById('affLogo2'));" >Cancel</a>
		    </div>	
		
		</td>
		</tr>
		
	
		<tr>
		<td align="right"><b>Content Provider</b></td>
		<td>&nbsp;</td>
		<td nowrap="nowrap">
		     <select name="cprovider"> 
			 <option value='Y' {if $userinfo.contentprovider eq 'Y'}selected {/if} >Yes</option>
			 <option value='N'  {if $userinfo.contentprovider eq 'N'} selected {/if}>No</option>

		      </select>
		    
		</td>

		<td align="right"><b>Active</b></td>
		<td>&nbsp;</td>
		<td nowrap="nowrap">
		     <select name="act_status"> 
			 <option value='Y' {if $userinfo.active eq 'Y'}selected {/if} >Yes</option>
			 <option value='N'  {if $userinfo.active eq 'N'} selected {/if}>No</option>
			 <option value='C'  {if $userinfo.active eq 'C'} selected {/if}>Rejected</option>
		      </select>
		</td>
		</tr>

		<tr>
		<td align="right"><b>Post Process</b></td>
		<td>&nbsp;</td>
		<td nowrap="nowrap">
		     <select name="postprocess"> 
			 <option value='Y' {if $userinfo.postprocess eq 'Y'}selected {/if} >Yes</option>
			 <option value='N'  {if $userinfo.postprocess eq 'N'} selected {/if}>No</option>
		      </select>
		 
		</td>

		<td align="right"><b>Post script</b></td>
		<td>&nbsp;</td>
		<td nowrap="nowrap">
		     <input type="text" name="postscript" id="postscript"  value=" {$userinfo.postscript}"  size="25" />  
		</td>
		</tr>

		<tr>
		<td align="right"><b>Custom Template</b></td>
		<td>&nbsp;</td>
		<td nowrap="nowrap">
		    <input type="text" name="customtpl" id="customtpl"  value=" {$userinfo.customtemplate}"  size="25" />  
		 
		</td>

		<td align="right"><b>Reason of rejection</b></td>
		<td>&nbsp;</td>
		<td nowrap="nowrap">
		        <textarea name="reasonforrej"  cols="70" rows="7"></textarea>
		  </td>
		</tr>
		
	
		<tr>
		<td colspan="2">&nbsp;</td>
		<td>

                {include file="admin/main/contentinfo.tpl"}



     
	  {/if}

                <tr>
		<td colspan="3" align="center"><input type="submit" value="    {$lng.lbl_save}      "  /></td>
		<td>
                 </td>
	         </tr>

</tbody>
</table>

</form>








{/capture}
{include file="dialog.tpl" title=$lng.lbl_profile_details content=$smarty.capture.dialog extra='width="100%"'}


