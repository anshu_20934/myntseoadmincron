{* $Id: featured_products.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
{include file="main/include_js.tpl" src="main/popup_product.js"}

<a name="featured" />

{$lng.txt_featured_products}

<br /><br />

{capture name=dialog}

{if $products ne ""}
{include file="main/check_all_row.tpl" style="line-height: 170%;" form="featuredprodform" prefix="posted_data.+to_delete"}
{/if}

<form action="{$formaction}" method="post" name="featuredprodform">
<input type="hidden" name="mode"  />

<table cellpadding="3" cellspacing="1" width="100%">

<tr class="TableHead">
	<td width="5%%">&nbsp;</td>
	<td width="70%">{$lng.lbl_product_name}</td>
	<td width="15%" align="center">{$lng.lbl_pos}</td>
	<td width="10%" align="center">{$lng.lbl_active}</td>
</tr>

{if $products}

{section name=prod_num loop=$products}

<tr{cycle values=", class='TableSubHead'"}>

	<td><input type="checkbox" name="posted_data[{$products[prod_num].product_id}][to_delete]" /></td>
	<td><b><a href="product.php?productid={$products[prod_num].product_id}" target="_blank">{$products[prod_num].product}</a></b></td>
	<td align="center"><input type="text" name="posted_data[{$products[prod_num].product_id}][display_order]" size="5" value="{$products[prod_num].display_order}" /></td>
	<td align="center"><input type="checkbox" name="posted_data[{$products[prod_num].product_id}][is_active]"{if $products[prod_num].is_active eq "1"} checked="checked"{/if} /></td>

</tr>

{/section}

<tr>
	<td colspan="4" class="SubmitBox">

	<input type="button" value="{$lng.lbl_upd_selected|escape}"  onclick="javascript: document.featuredprodform.mode.value = 'update'; document.featuredprodform.submit();"/>
		<input type="button" value="{$lng.lbl_delete_selected|escape}" onclick="javascript: document.featuredprodform.mode.value = 'delete'; document.featuredprodform.submit();" />
	</td>
</tr>


{else}

<tr>
<td colspan="4" align="center">{$lng.txt_no_featured_products}</td>
</tr>

{/if}

<tr>
<td colspan="4"><br /><br />{include file="main/subheader.tpl" title=$lng.lbl_add_product}</td>
</tr>

<tr>

	<td colspan=2 align="left">
		<input type="hidden" name="newproductid" />

		<input type="text"  size="35" name="newproduct" />

		<input type="button" value="{$lng.lbl_browse_|escape}"
		onclick="javascript: popup_product('featuredprodform.newproductid', 'featuredprodform.newproduct');" />
	</td>

	<td align="center">
	    <input type="text" name="neworder" size="5" /></td>
	<td align="center">
	      <input type="checkbox" name="newavail" checked="checked" />
	  </td>
</tr>
<tr>
    <td colspan="4">Browse OR Enter Product ID <input type="text"  size="20" name="p_productid" /></td>
</tr>

<tr>
	<td colspan="4" class="SubmitBox">
	<input type="submit" value="{$lng.lbl_add_new_|escape}" onclick="javascript: document.featuredprodform.mode.value = 'add'; document.featuredprodform.submit();"/>
	</td>
</tr>

</table>
</form>

{/capture}
{include file="dialog.tpl" title=$lng.lbl_featured_products content=$smarty.capture.dialog extra='width="100%"'}

