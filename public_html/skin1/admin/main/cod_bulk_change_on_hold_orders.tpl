{* $Id: featured_products.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
{include file="main/include_js.tpl" src="main/popup_product.js"}

<link href="{$http_location}/skin1/myntra_css/jquery-ui-datepicker.css" rel="stylesheet" type="text/css">
{literal}
<style type="text/css">
/* This is used for date picker for start and end dates of a discount rule. */
.ui-timepicker-div .ui-widget-header{ margin-bottom: 8px; }
.ui-timepicker-div dl{ text-align: left; }
.ui-timepicker-div dl dt{ height: 25px; }
.ui-timepicker-div dl dd{ margin: -25px 10px 10px 65px; }
.ui-timepicker-div td { font-size: 90%; }

ul{border:0; margin:0; padding:0;}

#pagination-flickr li{
	border:0; margin:0; padding:0;
	font-size:11px;
	list-style:none;
}
#pagination-flickr a{
	border:solid 1px #DDDDDD;
	margin-right:2px;
}
#pagination-flickr .previous-off,
#pagination-flickr .next-off {
	color:#666666;
	display:block;
	float:left;
	font-weight:bold;
	padding:3px 4px;
}
#pagination-flickr .next a,
#pagination-flickr .previous a {
	font-weight:bold;
	border:solid 1px #FFFFFF;
}
#pagination-flickr .active{
	color:#ff0084;
	font-weight:bold;
	display:block;
	float:left;
	padding:4px 6px;
}
#pagination-flickr a:link,
#pagination-flickr a:visited {
	color:#0063e3;
	display:block;
	float:left;
	padding:3px 6px;
	text-decoration:none;
}
#pagination-flickr a:hover {
	border:solid 1px #666666;
}

</style>
{/literal}

{capture name=dialog}
<div> 
	This page displays only those COD orders, which are moved directly to ON-HOLD status on Captcha verification.
</div>
{/capture}
{include file="dialog.tpl" title="A note about this page" content=$smarty.capture.dialog extra='width="100%"'}

<br/><br/>
{capture name=dialog}
<form action="cod_bulk_change_on_hold_orders.php" method="post">
<div> 
	Start Date : <input type="text" name="startDate" id="startDate" value="{$startDate}">
	End Date : <input type="text" name="endDate" id="endDate" value="{$endDate}">
	<input type="submit" value="Search COD on-hold orders" id="searchorders">
</div>
{/capture}
{include file="dialog.tpl" title="Data Range to find COD On-hold orders" content=$smarty.capture.dialog extra='width="100%"'}

<br/><br/>
{capture name=dialog}

<input type='hidden' id='mode' name='mode' value=''>

<table cellpadding="3" cellspacing="1" width="100%">
	<thead>
		<tr>
			<td colspan=4>
			<ul id="pagination-flickr">
				{assign var="previouspagenum" value=$pagenum-1}
				{if $pagenum>1}
					<li class="previous"><a href="?page=1&startDate='{$startDate}'&endDate='{$endDate}'" title="Orders: {$orderList.1}">&laquo; First</a></li>
					<li class="previous"><a href="?page={$pagenum-1}&startDate='{$startDate}'&endDate='{$endDate}'" title="Orders: {$orderList.$previouspagenum}">&laquo; Previous</a></li>
				{elseif $totalPages>1}
					<li class="previous-off">&laquo; First</li>
					<li class="previous-off">&laquo; Previous</li>
				{/if}
				{section name=foo loop=$totalPages}
					{if $smarty.section.foo.iteration eq $pagenum}
						<li class="active">{$smarty.section.foo.iteration}</li>
					{else}
    					<li><a href="?page={$smarty.section.foo.iteration}&startDate='{$startDate}'&endDate='{$endDate}'" 
    						title="Orders: {$orderList[$smarty.section.foo.iteration]}">{$smarty.section.foo.iteration}</a></li>
    				{/if}
				{/section}
				
				{assign var="nextpagenum" value=$pagenum+1}
				{if $pagenum<$totalPages}
					<li class="next"><a href="?page={$pagenum+1}&startDate='{$startDate}'&endDate='{$endDate}'" title="Orders: {$orderList.$nextpagenum}">Next &raquo;</a></li>
					<li class="next"><a href="?page={$totalPages}&startDate='{$startDate}'&endDate='{$endDate}'" title="Orders: {$orderList.$totalPages}">Last &raquo;</a></li>
				{elseif $totalPages>1}
					<li class="next-off">Next &raquo;</li>
					<li class="next-off">Last &raquo;</li>
				{/if}
			</ul>
		</td>
	</tr>
	
	</thead>
	
	<tfoot>
		<tr>
			<td colspan=4>
			<ul id="pagination-flickr">
				{assign var="previouspagenum" value=$pagenum-1}
				{if $pagenum>1}
					<li class="previous"><a href="?page=1&startDate='{$startDate}'&endDate='{$endDate}'" title="Orders: {$orderList.1}">&laquo; First</a></li>
					<li class="previous"><a href="?page={$pagenum-1}&startDate='{$startDate}'&endDate='{$endDate}'" title="Orders: {$orderList.$previouspagenum}">&laquo; Previous</a></li>
				{elseif $totalPages>1}
					<li class="previous-off">&laquo; First</li>
					<li class="previous-off">&laquo; Previous</li>
				{/if}
				{section name=foo loop=$totalPages}
					{if $smarty.section.foo.iteration eq $pagenum}
						<li class="active">{$smarty.section.foo.iteration}</li>
					{else}
    					<li><a href="?page={$smarty.section.foo.iteration}&startDate='{$startDate}'&endDate='{$endDate}'" 
    						title="Orders: {$orderList[$smarty.section.foo.iteration]}">{$smarty.section.foo.iteration}</a></li>
    				{/if}
				{/section}
				
				{assign var="nextpagenum" value=$pagenum+1}
				{if $pagenum<$totalPages}
					<li class="next"><a href="?page={$pagenum+1}&startDate='{$startDate}'&endDate='{$endDate}'" title="Orders: {$orderList.$nextpagenum}">Next &raquo;</a></li>
					<li class="next"><a href="?page={$totalPages}&startDate='{$startDate}'&endDate='{$endDate}'" title="Orders: {$orderList.$totalPages}">Last &raquo;</a></li>
				{elseif $totalPages>1}
					<li class="next-off">Next &raquo;</li>
					<li class="next-off">Last &raquo;</li>
				{/if}
			</ul>
		</td>
	</tr>
	
	</tfoot>
	
	<tr class="TableHead">
		<td width="10%">Order id</td>
		<td width="20%">Order Total</td>
		<td width="22%" colspan=2>User Details</td>
		<td width="20%">Address</td>
		<td width="8%">Queue ?</td>
		<td width="8%">Cancel ?</td>
	</tr>
	
	<tr>
		<td colspan=5></td>
		<td align="center"><input type="checkbox" class="selectAllQueue"><br/>Select All</td>
		<td align="center"><input type="checkbox" class="selectAllCancel"><br/>Select All</td>
	</tr>
	
	<tr><td colspan=7><hr/></td>
	
	{foreach from=$codonholdorders item=singleresult}
	<tr align="center">
		<td>
			<a href="order.php?orderid={$singleresult.orderid}" target="_blank">{$singleresult.orderid}</a><br/><br/>
			{$singleresult.placeddate}<br/>
		</td>
		
		<td align="left">
			<b>Customer Pays : {$singleresult.total}</b>
			
			{if $singleresult.eossdiscount>0 || $singleresult.coupon_discount>0 || $singleresult.cash_redeemed>0}
			<br/><br/> Actual Amount : {$singleresult.actualamount}<br/><br/> 
				<i>Discounts Applicable :</i><br/>
				{if $singleresult.eossdiscount>0}
				Auto-Discount :  {$singleresult.eossdiscount}<br/>
				{/if}
				{if $singleresult.coupon_discount>0}
				Coupon:  {$singleresult.coupon_discount} ({$singleresult.coupon})<br/>
				{/if}
				{if $singleresult.cash_redeemed>0}
				CashBack:  {$singleresult.cash_redeemed} ({$singleresult.cash_coupon_code})<br/>
				{/if}
				{if $singleresult.totalcoupondiscount>0}
				<b>Total Discount Redeemed : {$singleresult.totalcoupondiscount}</b>
				{/if}
			{/if}
		</td>
		
		<td align="left" colspan=2>
			Login : {$singleresult.login}<br/>
			Name : {$singleresult.firstname}<br/>
			Call : {$singleresult.mobile}
		</td>
		<td>
			{$singleresult.s_address}<br/>
			{$singleresult.s_city} - {$singleresult.s_state} - {$singleresult.s_country}<br/>
			{if $singleresult.s_zipcode}Pincode : {$singleresult.s_zipcode} {/if}
			</br> <span style="color: red">{$singleresult.pin_comments}</span>	
			<input type="hidden" class="pin_comments" name="pin_comments" id="pin_comments{$singleresult.orderid}" value="{$singleresult.pin_comments}" />
		</td>
		<td>
			<input type="checkbox" name="orderids[]" value="{$singleresult.orderid}" class="selectedOrder" {if $singleresult.pin_comments neq ''}disabled{/if}>
		</td>
		<td>
			<input type="checkbox" name="cancel_orderids[]" value="{$singleresult.orderid}" class="selectedCancelOrder" {if $singleresult.pin_comments eq ''}disabled{/if}>
		</td>
	</tr>
	<tr><td colspan=7><hr/></td></tr>
	{/foreach}
	
	<tr>
		<td colspan=5></td>
		<td align="center"><input type="checkbox" class="selectAllQueue"><br/>Select All</td>
		<td align="center"><input type="checkbox" class="selectAllCancel"><br/>Select All</td>
	</tr>
	<tr><td colspan=7><hr/></td></tr>
	<tr>
		<td colspan=5></td>
		<td align="center">
			<input type="submit" value="Queue these COD orders" id="queuecodorders">
		</td>
		<td align="center">
			<input type="submit" value="Cancel these COD orders" id="cancelcodorders">
		</td>
	</tr>

</table>
</form>

{/capture}
{include file="dialog.tpl" title="Search Results" content=$smarty.capture.dialog extra='width="100%"'}

<script type="text/javascript" src="{$http_location}/skin1/myntra_js/jquery-ui-datepicker.js"></script>

{literal}
<script>	
$(document).ready(function(){
	$('#startDate').datetimepicker();
	$('#endDate').datetimepicker();
	
	$(".selectAllQueue").click(function() {
		var checked_status = this.checked;
		$(".selectedOrder").each(function() {
			if(this.disabled == false) {
				this.checked = checked_status;
			}
		});
		$(".selectAllQueue").each(function() {
			this.checked = checked_status;
		});
	});

	$(".selectAllCancel").click(function() {
		var checked_status = this.checked;
		$(".selectedCancelOrder").each(function() {
			if(this.disabled == false) {
				this.checked = checked_status;
			}
		});
		$(".selectAllCancel").each(function() {
			this.checked = checked_status;
		});
	});

	$("#searchorders").click(function() {
		$("#mode").val("searchorders");
	});
	
	$("#cancelcodorders").click(function() {
		var check = confirm('Are you sure you want to cancel these COD orders ?');
		if(check) {
			$("#mode").val("cancelcodorders");
		} else {
			return false;
		}
	});

	$("#queuecodorders").click(function() {
		var check = confirm('Are you sure you want to Queue these COD orders ?');
		if(check) {
			$("#mode").val("queuecodorders");
		} else {
			return false;
		}
	});
});
</script>
{/literal}