{include file="page_title.tpl" title='Returns Requests'}
<br><br>
{capture name=dialog}
	<form action="" method="POST">
		<input type="hidden" name="action" value="search">
		<table border=0 cellspacing=0 cellpadding=0 width=99%>
	  		<tr height=50>
	  			<td>Return Id:  <input type=text style="padding: 2px; border: 1px solid #CCCCCC" name="returnid" value="{$returnid}"></td>
	    		<td>Order Id:  <input type=text style="padding: 2px; border: 1px solid #CCCCCC" name="orderid" value="{$orderid}"></td>
	    		<td>Customer Login: <input type=text style="padding: 2px; border: 1px solid #CCCCCC" name="cust_login" value="{$cust_login}"></td>
	  		</tr>
	  		<tr height=30>
	  			<td colspan=3 align=right><input type=submit name="return_search_btn" style="padding: 3px;" value="Search"></td>
	  		</tr>
	  	</table>
	</form>
{/capture}
{include file="dialog.tpl" title='Search Panel' content=$smarty.capture.dialog extra='width="100%"'}

<br><br><br>
{capture name=dialog}
<table border=0 cellspacing=1 cellpadding=0 width=99%>
	<tr style="background-color: #DFDFDF; height: 30px;">
		<th>Return Id</th>
		<th>Order Id</th>
		<th>Item Id</th>
		<th>Product Name</th>
		<th>Style Name</th>
		<th>Quantity</th>
		<th>Total</th>
		<th>Status</th>
		<th>Customer Login</th>
		<th>Pickup Address</th>
		<th>Created On</th>
	</tr>  
	{section name=idx loop=$return_requests}
		<tr height=30>
			<td align=center><a href="return_request.php?id={$return_requests[idx].returnid}&access=cc">{$return_requests[idx].returnid}</a></td>
			<td align=center>{$return_requests[idx].orderid}</td>
			<td align=center>{$return_requests[idx].itemid}</td>
			<td align=center>{$return_requests[idx].productName}</td>
			<td align=center>{$return_requests[idx].productStyleName}</td>
			<td align=center>{$return_requests[idx].quantity}</td>
			<td align=center>{$return_requests[idx].total}</td>
			<td align=center>{$return_requests[idx].status_display}</td>
			<td align=center>{$return_requests[idx].login}</td>
			<td align=left>{$return_requests[idx].pickup_address}</td>
			<td align=center>{$return_requests[idx].createddate}</td>
		</tr>
	{/section}
</table>
{/capture}
{include file="dialog.tpl" title='Search Results' content=$smarty.capture.dialog extra='width="100%"'}