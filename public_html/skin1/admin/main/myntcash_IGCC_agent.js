Ext.onReady(function() {
	Ext.QuickTips.init();
	var myMask = new Ext.LoadMask(Ext.getBody(), {msg: "Loading..."});
	Ext.Ajax.on('beforerequest', myMask.show, myMask);
	Ext.Ajax.on('requestcomplete', myMask.hide, myMask);
	Ext.Ajax.on('requestexception', myMask.hide, myMask);
	
	
	
	var TotalGoodWillSearch = new Ext.form.FormPanel({
		renderTo : 'contentPanel',
		title : 'Search Total Goodwill',
		id : 'formPanel',
		autoHeight : true,
		collapsible : true,
		border : true,
		bodyStyle : 'padding: 5px',
		buttonAlign : 'left',
		width : 1310,
		items : [{
			layout : 'table',
			border : false,
			layoutConfig : {columns : 2},
			defaults : {width : 450, border : false, 
				layout : 'form', bodyStyle : 'padding-left : 15px'},
			items : [
			         {
			        	 items : [
			        	 {
			        		width: 300,
			        		xtype : 'textarea',
			        		grow: true,
			        		id : 'userLogins',
			        		 fieldLabel : 'Login Ids (comma seperated list of agents / customers)'
			        	 },{
			        		 width: 300,
			        		 xtype : 'datefield',
			        		 id : 'startDate',
			        		 
			        		 fieldLabel : 'Start Date'
			        	 },{
			        		 width: 300,
			        		 xtype : 'timefield',
			        		 id : 'startTime',
			        		 format : 'H:i',
			        		 fieldLabel : 'Start Time'
			        	 },{
			        		 width: 300,
			        		 xtype : 'datefield',
			        		 id : 'endDate',
			        		 fieldLabel : 'End Date'
			        	 },{
			        		 width: 300,
			        		 xtype : 'timefield',
			        		 id : 'endTime',
			        		 format : 'H:i',
			        		 fieldLabel : 'End Time'
			        	 },{
			        		 width: 300,
			        		 xtype : 'numberfield',
			        		 id : 'orderid',
			        		
			        		 fieldLabel : 'Order ID'
			        	 }
			        	 
			        	 ]
			         }
			]
		}],
		buttons : [ {
			text : 'Agent wise Daily / Weekly  / Monthly Reports',
			handler : function() {
				getAgentUsageLimits();
			}
		},{
			text : 'Business Process wise View',
			handler : function() {
				getReasonCodeWiseAmounts();
			}
		},{
			text : ' Goodwill Reason wise View',
			handler : function() {
				getGoodwillReasonCodeWiseAmounts();
			}
		},{
			text : 'Order Details Look Up',
			handler : function() {
				getOrderLookUp();
			}
		}],
	});
	

	var agentGoodwillUsageResultsStore = new Ext.data.Store({
		url: 'myntCashIgccReports.php',
		pruneModifiedRecords : true,
		reader: new Ext.data.JsonReader({
	  		root: 'results',
	  	  	totalProperty: 'count',
	  	  	fields : [ 'login','dayAmount','dayRemaining', 'dayLimit','dayCount','weekAmount','weekRemaining', 'weekLimit','weekCount','monthAmount','monthRemaining', 'monthLimit','monthCount',]
		}),
	});
	
	
	var reasonGoodwillResultsStore = new Ext.data.Store({
		url: 'myntCashIgccReports.php',
		pruneModifiedRecords : true,
		reader: new Ext.data.JsonReader({
	  		root: 'results',
	  	  	totalProperty: 'count',
	  	  	fields :[ 'business_process','credit_inflow', 'credit_outflow','no_of_credits',]
		}),
	});
	
	var goodwillReasonGoodwillResultsStore = new Ext.data.Store({
		url: 'myntCashIgccReports.php',
		pruneModifiedRecords : true,
		reader: new Ext.data.JsonReader({
	  		root: 'results',
	  	  	totalProperty: 'count',
	  	  	fields :[ 'goodwill_reason','credit_inflow', 'credit_outflow','no_of_credits',]
		}),
	});
	

	function getAgentUsageLimits(){
		var form = TotalGoodWillSearch.getForm();
		
		if(form.isValid()){
			var params = {};
			params["userLogins"] = form.findField("userLogins").getValue();
			params["startDate"] = Date.parse(form.findField("startDate").getValue())/1000;
			params["startTime"] = params["startDate"]+timeToSeconds(form.findField("startTime").getValue());			
			params["endDate"] =  Date.parse(form.findField("endDate").getValue())/1000;
			params["endTime"] = params["endDate"]+timeToSeconds(form.findField("endTime").getRawValue());
		}
		params["action"] = "agentWiseGwLimitsUsage";
		//alert(JSON.stringify(params));
		
		tgSearchResults.reconfigure( agentGoodwillUsageResultsStore,colModelGwAgentUsage);
		agentGoodwillUsageResultsStore.baseParams = params;
		agentGoodwillUsageResultsStore.load();
	}
	
	function getReasonCodeWiseAmounts(){
		var form = TotalGoodWillSearch.getForm();
		
		if(form.isValid()){
			var params = {};
			params["startDate"] = Date.parse(form.findField("startDate").getValue())/1000;
			params["startTime"] = params["startDate"]+timeToSeconds(form.findField("startTime").getValue());			
			params["endDate"] =  Date.parse(form.findField("endDate").getValue())/1000;
			params["endTime"] = params["endDate"]+timeToSeconds(form.findField("endTime").getRawValue());
		}
		params["action"] = "reasonWiseGW";
		//alert(JSON.stringify(params));
		
		tgSearchResults.reconfigure( reasonGoodwillResultsStore,colModelGwReason);
		reasonGoodwillResultsStore.baseParams = params;
		reasonGoodwillResultsStore.load();
	}

	function getGoodwillReasonCodeWiseAmounts(){
		var form = TotalGoodWillSearch.getForm();
		
		if(form.isValid()){
			var params = {};
			params["startDate"] = Date.parse(form.findField("startDate").getValue())/1000;
			params["startTime"] = params["startDate"]+timeToSeconds(form.findField("startTime").getValue());			
			params["endDate"] =  Date.parse(form.findField("endDate").getValue())/1000;
			params["endTime"] = params["endDate"]+timeToSeconds(form.findField("endTime").getRawValue());
		}
		params["action"] = "goodwillReasonWiseGW";
		//alert(JSON.stringify(params));
		
		tgSearchResults.reconfigure( goodwillReasonGoodwillResultsStore,colModelGwGoodwillReason);
		goodwillReasonGoodwillResultsStore.baseParams = params;
		goodwillReasonGoodwillResultsStore.load();
	}
	
	
	function getOrderLookUp(){
		var form = TotalGoodWillSearch.getForm();
		
		if(form.isValid()){
			var params = {};
			params["orderid"] = form.findField("orderid").getValue();
			
		}
		params["action"] = "orderLookup";
		//alert(JSON.stringify(params));
		if(params["orderid"]==null){
			Ext.Msg.minWidth = 200;
			Ext.Msg.alert(' please enter order id ');
			return false;
		}
		tgSearchResults.reconfigure( orderDetailStore,colModelOrderDetails);
		orderDetailStore.baseParams = params;
		orderDetailStore.load();
	}
	
	function timeToSeconds(time){				
		var tm = time.split(":");
		if(tm.length==2){
			return tm[0]*60*60+tm[1]*60;
		}else{
			return 0;
		}
	}
	
	var colModel = new Ext.grid.ColumnModel({
		defaults: {
	        sortable: true
	    },
	    columns: [
	        new Ext.grid.RowNumberer(),
	        {id : 'modifiedOn', header : "Date", sortable: false, width: 125, dataIndex : 'modifiedOn'},	        
	        //{id : 'coupon', header : "Coupon", sortable: false, width: 125, dataIndex : 'coupon'},
	        {id : 'login', header : "Login", sortable: false, width: 150, dataIndex : 'login'},
	        //{id : 'txn_type', header : "Transaction Type", sortable: false, width: 125, dataIndex : 'txn_type', renderer: txnTypeRenderer},
	        {id : 'creditInflow', header : "Credit Amount", sortable: false, width: 100, dataIndex : 'creditInflow'},
	        {id : 'creditOutflow', header : "Debit Amount", sortable: false, width: 100, dataIndex : 'creditOutflow'},	        
	        {id : 'balance', header : "Balance", sortable: false, width: 100, dataIndex : 'balance'},
	        {id : 'businesProcess', header : "Business Process", sortable: false, width: 300, dataIndex : 'businesProcess'},
	        {id : 'itemId', header : "Item ID", sortable: false, width: 100, dataIndex : 'itemId'},
	        {id : 'itemType', header : "Item Type", sortable: false, width: 100, dataIndex : 'itemType'},
	        {id : 'modifiedBy', header : "Transaction By", sortable: false, width: 125, dataIndex : 'modifiedBy'},
	        {id : 'description', header : "Description", sortable: false, width: 300, dataIndex : 'description'}
  	  	]
	});
	
	var colModelGwAgentUsage = new Ext.grid.ColumnModel({
		defaults: {
	        sortable: true,
	        editable:true,
	        editor : Ext.form.Field
	    },
	    columns: [
	        new Ext.grid.RowNumberer(),
	        //{id : 'modified_on', header : "Date", sortable: false, width: 125, dataIndex : 'modified_on'},	 
	        {id : 'login', header : "Agent Name", sortable: false, width: 125, dataIndex : 'login'},
	        //{id : 'coupon', header : "Coupon", sortable: false, width: 125, dataIndex : 'coupon'},
	        //{id : 'login', header : "Customer Login", sortable: false, width: 150, dataIndex : 'login'},
	        //{id : 'txn_type', header : "Transaction Type", sortable: false, width: 125, dataIndex : 'txn_type', renderer: txnTypeRenderer},
	        {id : 'dayAmount', header : "Day Usage", sortable: true, width: 100, dataIndex : 'dayAmount'},
	        {id : 'dayRemaining', header : "Day Remaining amount", sortable: true, width: 100, dataIndex : 'dayRemaining'},
	        {id : 'dayLimit', header : "Day Limit", sortable: false, width: 100, dataIndex : 'dayLimit'},
	        {id : 'dayCount', header : "Day goodwill credits", sortable: true, width: 100, dataIndex : 'dayCount'},
	        {id : 'weekAmount', header : "Week Usage", sortable: true, width: 100, dataIndex : 'weekAmount'},
	        {id : 'weekRemaining', header : "Week Remaining amount", sortable: true, width: 100, dataIndex : 'weekRemaining'},
	        {id : 'weekLimit', header : "Week Limit", sortable: false, width: 100, dataIndex : 'weekLimit'},
	        {id : 'weekCount', header : "Week goodwill credits", sortable: true, width: 100, dataIndex : 'weekCount'},
	        {id : 'monthAmount', header : "Month Usage", sortable: true, width: 100, dataIndex : 'monthAmount'},
	        {id : 'monthRemaining', header : "Month Remaining amount", sortable: true, width: 100, dataIndex : 'monthRemaining'},
	        {id : 'monthLimit', header : "Month Limit", sortable: false, width: 100, dataIndex : 'monthLimit'},
	        {id : 'monthCount', header : "Month goodwill credits", sortable: true, width: 100, dataIndex : 'monthCount'},
	       
  	  	]
	});
	
	
	var colModelGwReason = new Ext.grid.ColumnModel({
		defaults: {
	        sortable: true,
	        editable:true,
	        editor : Ext.form.Field
	    },
	    columns: [
	        new Ext.grid.RowNumberer(),
	        {id : 'business_process', header : "Business Process", sortable: false, width: 125, dataIndex : 'business_process'},
	        {id : 'credit_inflow', header : "credit Amount", sortable: true, width: 100, dataIndex : 'credit_inflow'},
	        {id : 'credit_outflow', header : "Debit Amount", sortable: true, width: 100, dataIndex : 'credit_outflow'},
	        {id : 'no_of_credits', header : "Number of Transactions", sortable: true, width: 100, dataIndex : 'no_of_credits'},	       
  	  	]
	});

	var colModelGwGoodwillReason = new Ext.grid.ColumnModel({
		defaults: {
	        sortable: true,
	        editable:true,
	        editor : Ext.form.Field
	    },
	    columns: [
	        new Ext.grid.RowNumberer(),
	        {id : 'goodwill_reason', header : "Goodwill Reason", sortable: false, width: 125, dataIndex : 'goodwill_reason'},
	        {id : 'credit_inflow', header : "credit Amount", sortable: true, width: 100, dataIndex : 'credit_inflow'},
	        {id : 'credit_outflow', header : "Debit Amount", sortable: true, width: 100, dataIndex : 'credit_outflow'},
	        {id : 'no_of_credits', header : "Number of Transactions", sortable: true, width: 100, dataIndex : 'no_of_credits'},	       
  	  	]
	});
	
	
	var orderDetailStore = new Ext.data.Store({
		url: 'myntCashIgccReports.php',
		pruneModifiedRecords : true,
		reader: new Ext.data.JsonReader({
	  		root: 'results',
	  	  	totalProperty: 'count',
	  	  	fields :[ 'orderid','group_id','product_display_name','size','item_status',
	  	  	          'global_attr_gender','order_time','cancel_time','shipped_time','delivery_time',
	  	  	          'returnfiled_date','refunded_date','quantity','paid_amount']
		}),
	});
	
	var colModelOrderDetails = new Ext.grid.ColumnModel({
		defaults: {
	        sortable: true,
	        editable:true,
	        editor : Ext.form.Field
	    },
	    columns: [
	        new Ext.grid.RowNumberer(),
	        {id : 'orderid', header : "Order Id", sortable: true, width: 105, dataIndex : 'orderid'},
	        {id : 'group_id', header : "Order Group Id", sortable: true, width: 100, dataIndex : 'group_id'},
	        {id : 'product_display_name', header : "Product Name", sortable: true, width: 100, dataIndex : 'product_display_name'},
	        {id : 'size', header : "size", sortable: true, width: 100, dataIndex : 'size'},	
	        {id : 'item_status', header : "Item Status", sortable: true, width: 100, dataIndex : 'item_status'},	
	        {id : 'quantity', header : "Quantity", sortable: true, width: 100, dataIndex : 'quantity'},	
	        {id : 'global_attr_gender', header : "Item Gender", sortable: true, width: 100, dataIndex : 'global_attr_gender'},	
	        {id : 'order_time', header : "Order Placement Time", sortable: true, width: 100, dataIndex : 'order_time'},	
	        {id : 'cancel_time', header : "Cancellation Time", sortable: true, width: 100, dataIndex : 'cancel_time'},	
	        {id : 'shipped_time', header : "Shipped Time", sortable: true, width: 100, dataIndex : 'shipped_time'},	
	        {id : 'delivery_time', header : "Delivery Time", sortable: true, width: 100, dataIndex : 'delivery_time'},	
	        {id : 'returnfiled_date', header : "Refund Filed Time", sortable: true, width: 100, dataIndex : 'returnfiled_date'},	
	        {id : 'refunded_date', header : "Refund Issued Time", sortable: true, width: 100, dataIndex : 'refunded_date'},		       
	        {id : 'paid_amount', header : "Amount Paid including CashBack", sortable: true, width: 200, dataIndex : 'paid_amount'},	
  	  	]
	});
		
	var tgSearchResults = new Ext.grid.EditorGridPanel({
	    store  : agentGoodwillUsageResultsStore,
	    cm : colModelGwAgentUsage,
	    id : 'tGsearchResults-panel',
	    title : 'Search Results',
	    frame : true,
		loadMask : false,
		autoHeight : true,
		bbar : new Ext.PagingToolbar({
			pageSize : 30,
		    store: agentGoodwillUsageResultsStore,
		    displayInfo: true
		}),
		stripeRows : true,
		autoScroll : true,
		clicksToEdit:2,
		viewConfig : {
			enableRowBody : true
		}
		
	});
	
	var mainPanel = new Ext.Panel({
		renderTo : 'contentPanel',
		border : false,
		items : [
		     TotalGoodWillSearch,tgSearchResults
		],
		listeners : {
			'afterrender' : function(comp) {
				if(get_login && get_login != "") {
					searchPanel.getForm().findField("userlogin").setValue(get_login);
				}
				/*if(get_coupon && get_coupon != "") {
					searchPanel.getForm().findField("coupon").setValue(get_coupon);
				}*/
				reloadGrid();
			}
		}
	});
});
