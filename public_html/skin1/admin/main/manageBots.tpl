{* $Id: featured_products.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
{include file="main/include_js.tpl" src="main/popup_product.js"}

{capture name=dialog}

<table cellpadding="3" cellspacing="1" width="100%">

	<tr class="TableHead">
		<td align="left">Information</td>
	</tr>
	<tr>
		<td><b>Internet bots</b>, also known as <b>web robots, WWW robots</b> or simply <b>bots</b>, 
		are software applications that run automated tasks over the Internet. <br/>
		Typically, bots perform tasks that are both simple and structurally repetitive, 
		at a much higher rate than would be possible for a human alone. <br/>
		The largest use of bots is in web spidering, in which an automated script fetches, 
		analyzes and files information from web servers at many times the speed of a human.
		</td>
	</tr>
	<tr>
		<td>	
			These bots have been a cause of concern over excess session management data being created on www.myntra.com servers. <br/>
			To avoid such session management issues, we manage a list of such bots from admin settings. <br/><br/>
			<b>Note: </b>Adding bot information here, does not impact SEO functionality.
		</td>
	</tr>

</table>
{/capture}
{include file="dialog.tpl" title="About this page" content=$smarty.capture.dialog extra='width="100%"'}

{capture name=dialog}

<form name="addNewBot" action="manageBots.php" method="post">
	<input type="hidden" name="mode" id="mode">
	<table cellpadding="3" cellspacing="1" width="60%">

		<tr class="TableHead">
			<td align="left" width="30%">Bot Type</td>
			<td align="left" width="30%">Bot Name</td>
		</tr>
		
		<tr>
			<td><input type="text" name="botType" size="50"/></td>
			<td><input type="text" name="botName" size="50"/></td>
			<td class="SubmitBox" align="left">
			   <input type="submit" value="Add New Bot" onclick="javascript: document.addNewBot.mode.value = 'add_bot'; document.addNewBot.submit();" />
			</td>
		</tr>

	</table>
		
</form>

{/capture}
{include file="dialog.tpl" title="Add New Bot" content=$smarty.capture.dialog extra='width="100%"'}

<br/><br/>

{capture name=dialog}

<form name="remove_bot" action="manageBots.php" method="post">
	<input type="hidden" name="mode" id="mode">
	<table cellpadding="3" cellspacing="1" width="50%">

		<tr class="TableHead">
			<td align="left" width="5%">No.</td>
			<td align="left" width="10%">Bot Type</td>
			<td align="left" width="10%">Bot Name</td>
			<td align="left" width="5%">Remove ?</td>
			
		</tr>
		
		{section name=bot_num loop=$bots}
		{assign var=indexnum value=$smarty.section.bot_num.index+1}
		<tr> 
        	<td>{$indexnum}</td>
        	<td>{$bots[bot_num].bot_type}</td>
        	<td>{$bots[bot_num].bot_name}</td>
        	<td><INPUT TYPE=CHECKBOX NAME="removeBots[]" value="{$bots[bot_num].id}"></td>
        	{if $indexnum is div by 20}
        		<td>
        		<input type="submit" value="Remove Bots" 
			   	onclick="javascript: document.remove_bot.mode.value = 'remove_bot'; document.remove_bot.submit();" />
			   	</td>
        	{/if}
        </tr>
		{/section}
		
		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td class="SubmitBox" align="left">
			   <input type="submit" value="Remove Bots" 
			   	onclick="javascript: document.remove_bot.mode.value = 'remove_bot'; document.remove_bot.submit();" />
			</td>
		</tr>

	</table>
		
</form>

{/capture}
{include file="dialog.tpl" title="Manage Bots" content=$smarty.capture.dialog extra='width="100%"'}
