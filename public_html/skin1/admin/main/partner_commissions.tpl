{* $Id: partner_commissions.tpl,v 1.18.2.2 2006/07/11 08:39:26 svowl Exp $ *}
{include file="page_title.tpl" title=$lng.lbl_commissions}
{$lng.txt_partner_commissions_note}<br /><br />

<!-- IN THIS SECTION -->

{include file="dialog_tools.tpl"}

<!-- IN THIS SECTION -->
<br />

<script type="text/javascript" language="JavaScript 1.2">
<!--
var txt = '';
var lbl_all_partners = "{$lng.lbl_all_partners}";

{literal}
function change_filter(obj) {
	document.getElementById('partner').disabled = !document.getElementById('use_filter').checked;
	if (document.getElementById('use_filter').checked) {
		if (txt == lbl_all_partners)
			txt = '';
		if (document.getElementById('partner').value == lbl_all_partners)
			document.getElementById('partner').value = txt;
	} else {
		txt = document.getElementById('partner').value;
		document.getElementById('partner').value = lbl_all_partners;
	}
}
{/literal}
-->
</script>

{capture name=dialog}
<form action="partner_commissions.php" method="post" name="searchform">
<input type="hidden" name="mode" value="go" />

<table cellspacing="2" cellpadding="2">
<tr> 
    <td>{$lng.lbl_plan}</td>
    <td colspan="3">
	<select name="pc">
		<option value="">{$lng.lbl_select_affiliate_plan}</option>
{foreach from=$partner_plans item=plan}
		<option value="{$plan.plan_id}">{$plan.plan_title|escape}</option>
{/foreach}
	</select>
	</td>
</tr> 
<tr>
	<td>{$lng.lbl_partner}</td>
	<td><input type="text" value="{$partner}" name="partner" id="partner" /></td>
	<td><input type="checkbox" id="use_filter" name="use_filter" value="Y"{if $use_filter eq 'Y'} checked="checked"{/if} onclick="javascript: change_filter();" /></td>
	<td><label for="use_filter">{$lng.lbl_use_filter}</label></td>
</tr>
<tr>
	<td>&nbsp;</td>
	<td colspan="3">
	<input type="button" value="{$lng.lbl_apply|strip_tags:false|escape}" onclick="javascript: if (confirm('{$lng.txt_apply_aff_plan_to_partners|strip_tags}')) submitForm(this, 'apply_global');" />
	&nbsp;
	<input type="submit" name="go" value="{$lng.lbl_show|strip_tags:false|escape}" />
	</td>
</tr>
</table>
</form>

<script type="text/javascript" language="JavaScript 1.2">
<!--
change_filter();
-->
</script>

{/capture}
{include file="dialog.tpl" content=$smarty.capture.dialog title=$lng.lbl_search extra='width="100%"'}
{if $mode eq "go"}
<br />
{capture name=dialog}
{if $partner_info eq ""}
{$lng.lbl_no_partners}
{else}
<form method="post" action="partner_commissions.php">
<input type="hidden" name="mode" value="apply" />
<input type="hidden" name="partner" value="{$partner}" />
<input type="hidden" name="page" value="{$smarty.get.page|escape:"html"}" />

<table cellpadding="2" cellspacing="2" width="100%">
<tr class="TableHead">
	<td width="10%">{$lng.lbl_login}</td>
	<td width="70%">{$lng.lbl_name}</td>
	<td width="20%">{$lng.lbl_affiliate_plan}</td>
</tr>
{foreach from=$partner_info item=p}
<tr{cycle values=', class="TableSubHead"'}>
	<td><a href="user_modify.php?user={$p.login|escape:"url"}&amp;usertype=B">{$p.login}</a></td>
	<td><a href="user_modify.php?user={$p.login|escape:"url"}&amp;usertype=B">{$p.firstname}&nbsp;{$p.lastname}</a></td>
	<td>
	<select name="plans[{$p.login}]">
		<option value="">{$lng.lbl_no_plans_assigned}</option>
{foreach from=$partner_plans item=plan}
		<option value="{$plan.plan_id}"{if $plan.plan_id eq $p.plan_id} selected="selected"{/if}>{$plan.plan_title|escape}</option>
{/foreach}
	</select>
	</td>
</tr>
{/foreach}

<tr>
	<td colspan="3" class="SubmitBox"><input type="submit" value="{$lng.lbl_apply|strip_tags:false|escape}" /></td>
</tr>

</table>
</form>

{/if}
{/capture}
{include file="dialog.tpl" content=$smarty.capture.dialog title=$lng.lbl_search_results extra='width="100%"'}
{/if}
