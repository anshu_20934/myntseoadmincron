{* $Id: featured_products.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
{include file="main/include_js.tpl" src="main/popup_product.js"}

{literal}
  <script language="Javascript">
      function confirmDelete()
      {
          var flag = confirm('Are you sure you want to delete?')
          if(!flag)
          {
             return false;
          }
          else
          {
          	 document.skinform.mode.value = 'delskin';
          	 document.skinform.submit();
          	 return true;
          }
      }
      
      function enable_name(cbox,postdata){
      	var skin = document.getElementById(postdata+'[skin_name]');
      	var active = document.getElementById(postdata+'[avail]');
      	if(cbox.checked == true){
      		skin.disabled = false;
	      	active.disabled = false;	      	
      	}else{
      		skin.disabled = true;
	      	active.disabled = true;
      	}
      }
  </script>
{/literal}
<a name="featured" />
{$lng.txt_featured_products}
<br /><br />
{capture name=dialog}

<form action="admin_product_group.php" method="post" name="productgroup" enctype="multipart/form-data">
<input type="hidden" name="mode"  />
<table cellpadding="3" cellspacing="1" width="100%">
<tr class="TableHead">
	<td width="10%">id</td>
	<td width="15%">Name</td>
	<td width="15%">Label</td>
	<td width="15%">Title</td>
	<td width="25%">Description</td>
	<td width="25%">Default Style Id</td>
	<td width="25%">Type</td>
	<td width="25%">url</td>
	<td width="25%">Image</td>
	<td width="25%">Display Order</td>	
	<td width="10%">Is Active</td>
	

</tr>
{section name=row loop=$product_group}
	<tr>
		<td><input type="checkbox" name="posted_data[{$product_group[row].id}][to_delete]" value="{$product_group[row].id}" /></td>
		<td><input type="text" name="posted_data[{$product_group[row].id}][name]" value="{$product_group[row].name}" /></td>
		<td><input type="text" name="posted_data[{$product_group[row].id}][label]" value="{$product_group[row].label}" /></td>
		<td><input type="text" name="posted_data[{$product_group[row].id}][title]" value="{$product_group[row].title}" /></td>
		<td><input type="text" name="posted_data[{$product_group[row].id}][description]" value="{$product_group[row].description}" /></td>
		<td><input type="text" name="posted_data[{$product_group[row].id}][default_style_id]" value="{$product_group[row].default_style_id}" /></td>
		<td><input type="text" name="posted_data[{$product_group[row].id}][type]" value="{$product_group[row].type}" /></td>
		<td><input type="text" name="posted_data[{$product_group[row].id}][url]" value="{$product_group[row].url}" /></td>
		<td><input type="text" name="posted_data[{$product_group[row].id}][image]" value="{$product_group[row].image}" /></td>
		<td><input type="text" name="posted_data[{$product_group[row].id}][display_order]" value="{$product_group[row].display_order}" /></td>
		<td><input type="text" name="posted_data[{$product_group[row].id}][is_active]" value="{$product_group[row].is_active}" /></td>
		
	</tr>
	
{sectionelse}
	<tr>
	<td>No product group is available.</td>
	</tr>
	
{/section}	
	
<tr>
	<td colspan="2" class="SubmitBox">
	{if $product_group}
			<input type="button" value="delete" onclick="javascript: document.productgroup.mode.value = 'delete_product_group'; document.productgroup.submit();"  />
			<input type="button" value="{$lng.lbl_update|escape}" onclick="javascript: document.productgroup.mode.value = 'update_product_group'; document.productgroup.submit();"  />          
	{/if}
	</td>
</tr>

 <tr>
	<td colspan="2"><br /><br /></td>
</tr>
</table>

<!--table for adding addons-->
<table cellpadding="3" cellspacing="1" width="100%">
	<tr>
	<td><b>Name</b></td><td><input type="text" name="name"  /></td><td><b>Label</b></td><td><input type="text" name="label"  /></td><td><b>Title</b></td><td><input type="text" name="title"  /></td><td><b>Description</b></td><td><input type="text" name="description"  /></td><td><b>URL</b></td><td><input type="text" name="url"  /></td>
	</tr>
	<tr>
	<td><b>Default Style Id</b></td><td><input type="text" name="default_style_id"  /></td><td><b>Type</b></td><td><input type="text" name="type"  /></td><td><b>Image</b></td><td><input type="text" name="image"  /></td><td><b>Display Order</b></td><td><input type="text" name="display_order"  /></td><td><b>is_active</b></td><td><input type="text" name="is_active"  /></td>
	</tr>
	
<tr>
	<td colspan="2" class="SubmitBox">
			<input type="button" value="add" onclick="javascript: document.productgroup.mode.value = 'add_product_group'; document.productgroup.submit();"  />		
	</td>
</tr>

 <tr>
	<td colspan="2"><br /><br /></td>
</tr>
</table>
<!--end add addons template-->
</form>

{/capture}
{include file="dialog.tpl" title="Promotion Templates" content=$smarty.capture.dialog extra='width="100%"'}
<br /><br />
{if $template}
{include file="admin/main/event_promotion_skin.tpl"}
{/if}