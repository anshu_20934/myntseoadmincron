{include file="main/include_js.tpl" src="main/popup_category.js"}
{include file="main/include_js.tpl" src="main/calendar2.js"}
{if $active_modules.HTML_Editor}{include file="modules/HTML_Editor/editor.tpl"}{/if}

{literal}
<script language="javascript">
function validateForm()
{
	var error = false;
	if(document.getElementById("refname").value == "")
	{
		alert("Please input shop name");
		document.getElementById("refname").focus();
		var error = true;
		return false;
	}
	if(document.getElementById("startdate").value == "")
	{
		alert("Please select start date");
		document.getElementById("startdate").focus();
		var error = true;
		return false;
	}
	if(document.getElementById("enddate").value == "")
	{
		alert("Please select end date");
		document.getElementById("enddate").focus();
		var error = true;
		return false;
	}
	if(document.getElementById("refpercent").value == "")
	{
		alert("Please input referral percentage");
		document.getElementById("refpercent").focus();
		var error = true;
		return false;
	}
	if(document.getElementById("refdescr").value == "")
	{
		alert("Please input referral description");
		document.getElementById("refdescr").focus();
		var error = true;
		return false;
	}

	if(error == false)
	{
		document.addreferral.submit();
	}
}
</script>
{/literal}





{capture name=dialog}

<form action="{$formaction}" method="post" name="addreferral" enctype="multipart/form-data" >
<input type="hidden" name="mode" id="mode"  />
{if $editdata.id != ""}
<input type="hidden" name="hideid" id="hideid" value="{$editdata.id}"  />
{/if}

<table cellpadding="3" cellspacing="1" width="100%">

<tr>
<td width="40%">Referral Name</td>
<td width="60%">
<input type="text" size="35" name="refname" id="refname"
value="{if $editdata.refferal_name == ""}{$smarty.post.refname}{else}{$editdata.refferal_name}{/if}" />
</td>
</tr>


<tr>
<td width="40%">Start date</td>
<td width="60%"><input type="text" id='startdate' name='startdate' value="{if $editdata.startdate == ""}{$smarty.post.startdate}{else}{$editdata.startdate}{/if}"/>
	<a href="javascript:cal4.popup();" ><img src="{$cdn_base}/images/cal.gif" width="16" height="16" border="0" alt="Click Here to Pick up the date"></a></td>
</tr>

<tr>
<td width="40%">End date</td>
<td width="60%"><input type="text" id='enddate' name='enddate' value="{if $editdata.enddate == ""}{$smarty.post.enddate}{else}{$editdata.enddate}{/if}"/>
	<a href="javascript:cal5.popup();" ><img src="{$cdn_base}/images/cal.gif" width="16" height="16" border="0" alt="Click Here to Pick up the date"></a></td>
</tr>


<tr>
<td width="40%">Status</td>
<td width="60%">
Active&nbsp;
{if $editdata.status == "1"}
<input name="refstatus" type="radio" value="{$editdata.status}" checked>
{else}
<input name="refstatus" type="radio" value="1" {if $smarty.post.refstatus == "1"} checked='checked'{/if} checked>
{/if}
Inactive&nbsp;
{if $editdata.status == "0"}
<input name="refstatus" type="radio" value="{$editdata.status}" checked>
{else}
<input name="refstatus" type="radio" value="0" {if $smarty.post.refstatus == "0"} checked='checked'{/if}>
{/if}
</td>
</tr>
<tr>
<td width="40%">Referral percentage</td>
<td width="60%">
<input type="text" size="5" name="refpercent" id="refpercent"
value="{if $editdata.refferal_percent == ""}{$smarty.post.refpercent}{else}{$editdata.refferal_percent}{/if}" />%
</td>
</tr>
<tr>
<td width="40%">Description</td>
<td width="60%">
{if $editdata.description eq ""}
	{assign var="data" value=$smarty.post.refdescr}
{else}
	{assign var="data" value=$editdata.description}
{/if}
{include file="main/textarea.tpl" name="refdescr" id="refdescr" cols=45 rows=8 class="InputWidth"
width="80%" btn_rows=4}
</td>
</tr>

<tr>
<td colspan="2">
{if $editdata.description == ""}
<input type="button" value="save" id="save"
onclick="javascript:document.addreferral.mode.value = 'add'; return validateForm();"/>
{else}
<input type="button" value="update" id="save"
onclick="javascript:document.addreferral.mode.value = 'update'; return validateForm();"/>
{/if}
<input type="button" value="cancel" onclick="javascript:window.location='admin_referral.php'" />
</td>

</tr>

</table>
<script language="javascript">
      var cal4 = new calendar2(document.forms['addreferral'].elements['startdate']);
      cal4.year_scroll = true;
      cal4.time_comp = false;

       var cal5 = new calendar2(document.forms['addreferral'].elements['enddate']);
      cal5.year_scroll = true;
      cal5.time_comp = false;
</script>

</form>
{/capture}
{include file="dialog.tpl" title="General" content=$smarty.capture.dialog extra='width="100%"'}