{include file="main/include_js.tpl" src="main/popup_product.js"}
{capture name=dialog}

{if $action eq "upload"}
        <form action="" method="post" name="queue_orders" enctype="multipart/form-data" >
                * Input file format : <font color='blue'>Orderid, delivery attempt count, UD reason</font>
                <br/>
                * First line of CSV file is ignored.
                <br />
                * If remark contains comma then give remark in double quotes.
		<br />
		<br />
		Upload the csv file : <input type="file" name="orderdetail" >{if $message}<font color='red'>{$message}</font>{/if}
                <input type='submit'  name='uploadxls'  value="Upload">
                <input type='hidden' name='action' value='verify'>
        </form>
{elseif $action eq verify}
	{if $dataFormatError}
                <b>{$dataFormatError}</b>
        {else}
                <table cellpadding="2" cellspacing="1" width="100%">
			<tr class="TableHead"><td>UD Orders Queued</td></tr>
			{section name=idx loop=$orders}
                        	<tr{cycle values=", class='TableSubHead'"}> <td align="center"><input type=hidden name = orderids[] value="{$orders[idx]}"><a href="old_ud_tracking.php?order_id={$orders[idx]}" target="_blank">{$orders[idx]}</a></td></tr>
			{/section}
		</table>
		
			{if failedorders }
				<table cellpadding="2" cellspacing="1" width="100%">
                                	<tr class="TableHead"> <td colspan="2">UD Orders - Failed to be queued</td></tr>
					{section name=idx1 loop=$failedorders}
						<tr{cycle values=", class='TableSubHead'"}>
                                                	<td align="right"> <input type=hidden name = failedorderids[] value="{$failedorders[idx1].orderid}-{$failedorders[idx1].reason}"/> <a href="../order.php?orderid={$failedorders[idx1].orderid}">{$failedorders[idx1].orderid}</a> </td>
                                                	<td align="left"> <b>{$failedorders[idx1].reason}</b></td>
                                        	</tr>	
					{/section}
				</table>
			{/if}			
	
	{/if}
{/if}
{/capture}
{include file="dialog.tpl" title="Upload CSV of Undelivered Orders s to be queue" content=$smarty.capture.dialog extra='width="100%"'}
