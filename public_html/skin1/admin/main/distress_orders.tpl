{* $Id: featured_products.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
{include file="main/include_js.tpl" src="main/popup_product.js"}
<link rel="stylesheet" href="{$SkinDir}/skin1_admin.css" />

{capture name=dialog}
<form name="distress_orders" action="distress_orders.php" method="post">
	<input type="hidden" name="mode" id="mode" value="distress">
    <div class="ITEM_PARENT_DIV">
	<div class="ITEM_FIRST_CHILD_DIV" style="width:450px;">
    <div class="ITEM_LABLE_DIV" style="width:200px;">Search distress type orders</div>
    <div class="ITEM_FIELD_DIV" style="width:200px;">
      <select name="distress_type" id="distress_type" onchange="javascript:document.distress_orders.submit();">
        <option value="0">Select distress type</option>
      	<option value="OH" {if $smarty.post.distress_type=="OH"} selected {/if}>On Hold</option>
      	<option value="FD" {if $smarty.post.distress_type=="FD"} selected {/if}>Failed to deliver</option>
      	<option value="RP" {if $smarty.post.distress_type=="RP"} selected {/if}>Replacement added</option>
        <option value="RPN" {if $smarty.post.distress_type=="RP"} selected {/if}>Replacement Orders (New)</option>
      	<option value="Q2" {if $smarty.post.distress_type=="Q2"} selected {/if}>Queued for more than 2 days</option>
      </select>
    </div>  
    </div>
    </div>     
</form>

{/capture}
{include file="dialog.tpl" title="Distree type order" content=$smarty.capture.dialog extra='width="100%"'}
<br/> <br/>
{capture name=dialog}

<div align="right">{include file="buttons/button.tpl" button_title=$lng.lbl_search_again href="orders.php"}</div>

{include file="customer/main/navigation.tpl"}

{include file="main/check_all_row.tpl" form="processorderform" prefix="orderids"}

<form action="process_order.php" method="post" name="processorderform">
<input type="hidden" name="mode" value="" />


<table cellpadding="2" cellspacing="1" width="100%" >

{assign var="colspan" value=6}

<tr class="TableHead">
	<td width="5">&nbsp;</td>
	<td width="5%" nowrap="nowrap">{if $search_prefilled.sort_field eq "orderid"}{include file="buttons/sort_pointer.tpl" dir=$search_prefilled.sort_direction}&nbsp;{/if}<a href="orders.php?mode=search&amp;sort=orderid">#</a></td>
	<td width="3%" nowrap="nowrap">{if $search_prefilled.sort_field eq "status"}{include file="buttons/sort_pointer.tpl" dir=$search_prefilled.sort_direction}&nbsp;{/if}<a href="orders.php?mode=search&amp;sort=status">{$lng.lbl_status}</a></td>
	<td width="5%" nowrap="nowrap">{if $search_prefilled.sort_field eq "status"}{include file="buttons/sort_pointer.tpl" dir=$search_prefilled.sort_direction}&nbsp;{/if}<a href="orders.php?mode=search&amp;sort=status">{$lng.lbl_payment_mode}</a></td>
	<td width="30%" nowrap="nowrap">{if $search_prefilled.sort_field eq "customer"}{include file="buttons/sort_pointer.tpl" dir=$search_prefilled.sort_direction}&nbsp;{/if}<a href="orders.php?mode=search&amp;sort=customer">{$lng.lbl_customer}</a></td>
{if $usertype eq "A" and $single_mode eq ""}
{assign var="colspan" value=7}
	<td width="5%" nowrap="nowrap">
	{if $search_prefilled.sort_field eq "qtyInOrder"}
		{include file="buttons/sort_pointer.tpl" dir=$search_prefilled.sort_direction}&nbsp;{/if}
			<a href="orders.php?mode=search&amp;sort=qtyInOrder">{$lng.lbl_qtyInOrder}</a></td>
  	{/if}
<td width="5%" nowrap="nowrap">{if $search_prefilled.sort_field eq "gift_status"}{include file="buttons/sort_pointer.tpl" dir=$search_prefilled.sort_direction}&nbsp;{/if}<a href="orders.php?mode=search&amp;sort=gift_status">Gift</a></td>
<td width="20%" nowrap="nowrap">{if $search_prefilled.sort_field eq "date"}{include file="buttons/sort_pointer.tpl" dir=$search_prefilled.sort_direction}&nbsp;{/if}<a href="orders.php?mode=search&amp;sort=date">{$lng.lbl_date}</a></td>
<td width="20%" nowrap="nowrap">{if $search_prefilled.sort_field eq "date"}{include file="buttons/sort_pointer.tpl" dir=$search_prefilled.sort_direction}&nbsp;{/if}<a href="orders.php?mode=search&amp;sort=date">Queued Date</a></td>
<td width="10%" align="right" nowrap="nowrap">{if $search_prefilled.sort_field eq "total"}{include file="buttons/sort_pointer.tpl" dir=$search_prefilled.sort_direction}&nbsp;{/if}<a href="orders.php?mode=search&amp;sort=total">{$lng.lbl_total}</a></td>
</tr>
{if $orders}
{section name=oid loop=$orders}

{assign var=amount value=$orders[oid].total}
{assign var=shipcost value=$orders[oid].shipping_cost}
{assign var=vatcts value=$orders[oid].tax}
{assign var=ref_discount value=$orders[oid].ref_discount}
{assign var=gift_charges value=$orders[oid].gift_charges}
{assign var=cod value=$orders[oid].cod}
{assign var=amounttobepaid value=$amount+$shipcost+$vatcts+$cod}
{assign var=amounttobepaid value=$amounttobepaid-$ref_discount}


{math equation="x + ordertotal" x=$total ordertotal=$amounttobepaid assign="total"}
{if $orders[oid].status eq "P" or $orders[oid].status eq "C"}
{math equation="x + ordertotal" x=$total_paid ordertotal=$amounttobepaid assign="total_paid"}
{/if}

<tr{cycle values=", class='TableSubHead'"}>
	<td width="5"><input type="checkbox" name="orderids[{$orders[oid].orderid}]" /></td>
	<td><a href="order.php?orderid={$orders[oid].orderid}">#{$orders[oid].orderid}</a></td>

	<td nowrap="nowrap" align="center">

{if $usertype eq "A" or ($usertype eq "P" and $active_modules.Simple_Module ne "")}

<!--<input type="hidden" name="order_status_old[{$orders[oid].orderid}]" value="{$orders[oid].status}" />-->
{include file="main/order_status.tpl" status=$orders[oid].status mode="static" }
{else}
<a href="order.php?orderid={$orders[oid].orderid}">
	<b>{include file="main/order_status.tpl" status=$orders[oid].status mode="static"}</b></a>
{/if}

{if $active_modules.Stop_List ne '' && $orders[oid].blocked  eq 'Y'}
<img src="{$cdn_base}/skin1/images/no_ip.gif" style="vertical-align: middle;" alt="" />
{/if}
	</td>
	<td align="center">
	{if $orders[oid].payment_method == 'chq'} Check
		{elseif $orders[oid].payment_method == 'cod'} Cash
		{elseif $orders[oid].payment_method == 'cash'} Cash
		{elseif $orders[oid].payment_method == 'inm'} Invoice Me
		{else} CC/Net Banking
	{/if}
	</td>
	<td>{$orders[oid].firstname} {$orders[oid].lastname} ({$orders[oid].login})</td>
{if $usertype eq "A" and $single_mode eq ""}
	<td align="center">{$orders[oid].qtyInOrder}</td>
{/if}
	<td nowrap="nowrap" align="center"><a href="order.php?orderid={$orders[oid].orderid}">{$orders[oid].gift_status}</a></td>
	<td nowrap="nowrap" align="center"><a href="order.php?orderid={$orders[oid].orderid}">{$orders[oid].date}</a></td>
	<td nowrap="nowrap" align="center"><a href="order.php?orderid={$orders[oid].orderid}">{$orders[oid].queueddate}</a></td>
	<td nowrap="nowrap" align="right">
	<a href="order.php?orderid={$orders[oid].orderid}">{include file="currency.tpl" value=$amounttobepaid}</a>
	</td>
</tr>
<input type="hidden" name="order_email[{$orders[oid].orderid}]" value="{$orders[oid].email}" />
{/section}
<tr>
	<td colspan="8"><img src="{$cdn_base}/skin1/images/spacer.gif" width="100%" height="1" alt="" /></td>
</tr>

<tr>
	<td colspan="8" align="right">{$lng.lbl_gross_total}: <b>{include file="currency.tpl" value=$total}</b></td>
</tr>

<tr>
	<td colspan="8" align="right">{$lng.lbl_total_paid}: <b>{include file="currency.tpl" value=$total_paid}</b></td>
</tr>

{else}
<tr><td colspan="8" align="center"><strong>No order found for the given criteria</strong></td></tr>
{/if}
<tr>
<td colspan="8"><br />
{include file="customer/main/navigation.tpl"}
</td>
</tr>

</table>
</form>
{/capture}
{include file="dialog.tpl" title=$lng.lbl_search_results content=$smarty.capture.dialog extra='width="100%"'}
