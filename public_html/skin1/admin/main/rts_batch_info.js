Ext.onReady(function() {
	Ext.QuickTips.init();
	var myMask = new Ext.LoadMask(Ext.getBody(), {msg: "Loading..."});
	
	Ext.Ajax.on('beforerequest', myMask.show, myMask);
	Ext.Ajax.on('requestcomplete', myMask.hide, myMask);
	Ext.Ajax.on('requestexception', myMask.hide, myMask);
	
	var searchPanel = new Ext.form.FormPanel({
		renderTo : 'contentPanel',
		title : 'Search Panel',
		id : 'formPanel',
		autoHeight : true,
		collapsible : true,
		border : true,
		bodyStyle : 'padding: 5px',
		width : 1000,
		items : [{
			layout : 'table',
			border : false,
			layoutConfig : {columns : 2},
			defaults : {width : 450, border : false, 
				layout : 'form', bodyStyle : 'padding-left : 15px'},
			items : [{
				items : [{
					xtype : 'numberfield',
		        	id : 'batch_id',
		        	fieldLabel : 'Batch No'
				}]
			}, {
				items :[{
					xtype : 'textfield',
		        	id : 'login',
		        	fieldLabel : 'Login',
		        	width : 200
				}]
			}, 
			{
				items : [{
					xtype : 'datefield',
	                name : 'from_date',
	                width : 150,
	                fieldLabel : "From Date"
				}]
			}, {
				items : [{
					xtype : 'datefield',
					name : 'to_date',
					width : 150,
					fieldLabel : "To Date"
				}]
			}]
		}],
		buttons : [ {
			text : 'Search',
			handler : function() {
				reloadGrid();
			}
		}]
	});
	
	
	
	function getSearchPanelParams() {
		var form = searchPanel.getForm();
		
		if (form.isValid()) {
			var params = {};
			params["batch_id"] = form.findField("batch_id").getValue();
			params["created_by"] = form.findField("login").getValue();
			params["from_date"] = form.findField("from_date").getRawValue();
			params["to_date"] = form.findField("to_date").getRawValue();
			
			return params;
		} else {
			return false;
		}
	}
	
	function reloadGrid() {
		
		var params = getSearchPanelParams();
		
		if (params == false) {
			Ext.MessageBox.alert('Error', 'Errors in the search form');
			return false;
		}
		params["action"] = "search";
		
		searchResultsStore.baseParams = params;
		searchResultsStore.load();
	}
	
	
	var requestIdLinkRender = function(value, p, record) {
		
		return '<a href="/admin/orders_ship_report.php?action=generate_batch_report&batchid='+record.data["id"]+ '" target="_blank"">' + record.data["id"] + '</a>';
	}
	
	var downloadLinkRender = function(value, p, record) {
		
		return '<a href="/admin/orders_ship_report.php?action=generate_batch_report&batchid='+record.data["id"]+ '" target="_blank""> Download RTS Report</a>';
	}
	
	var colModel = new Ext.grid.ColumnModel({
		defaults: {
	        sortable: true
	    },
	    columns: [
	        {id : 'batchid', header : "Batch No", sortable: true, width: 75, dataIndex : 'id',renderer : requestIdLinkRender},
	        {id : 'orderids', header : "Order Ids", sortable: true, width: 250, dataIndex : 'orderids'},
	        {id : 'created_by', header : "Created By", sortable: true, width: 150, dataIndex : 'created_by'},
	        {id : 'created_time', header : "Created Time", sortable: true, width: 150, dataIndex : 'created_time'},
	        {id : 'downloadlink', header : "RTS Download", sortable: true, width: 250, dataIndex : 'id',renderer:downloadLinkRender}
	    ]
	});
		
	var searchResultsStore = new Ext.data.Store({
		url: 'rtsbatchinfoajax.php',
		pruneModifiedRecords : true,
		reader: new Ext.data.JsonReader({
	  		root: 'results',
	  	  	totalProperty: 'count',
	  	  	fields : ['id', 'orderids', 'created_by', 'created_time']
		}),
		sortInfo:{field : 'id', direction:'DESC'},
		remoteSort: true
	});

	var pagingBar = new Ext.PagingToolbar({
		pageSize : 30,
	    store: searchResultsStore,
	    displayInfo: true
	});
	
	searchResults = new Ext.grid.EditorGridPanel({
	    store  : searchResultsStore,
	    cm : colModel,
	    id : 'searchResults-panel',
	    title : 'Search Results',
	    frame : true,
		loadMask : false,
		autoHeight : true,
		bbar : pagingBar,
		stripeRows : true,
		autoScroll : true,
		viewConfig : {
			enableRowBody : true
		},
		tbar:[]
	});
	
	var mainPanel = new Ext.Panel({
		renderTo : 'contentPanel',
		border : false,
		items : [
		     searchPanel, searchResults
		]
	});
});
