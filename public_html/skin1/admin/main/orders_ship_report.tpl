{* $Id: orders_ship_report.tpl,v 1.24.2.1 2011/07/14 10:47:40 max Exp $ *}
<script type="text/javascript">
	var couriers =  {$couriers_json};
	var colorMap = {$courier_color_map};
	var noOfCourierFilters = {$num_courier_filters};
	var addOrUpdate = "add";
	var progressKey = '{$progresskey}';
	var adminUser = '{$login}';
	var httpLocation = '{$http_location}';
	var batchid = '';
</script>
{include file="main/include_js.tpl" src="main/popup_product.js"}
<link rel="stylesheet" href="{$SkinDir}/skin1_admin_operations.css" />
{literal}
<script type="text/javascript">

    var timerId;
    function hideUploadStatusWindow() {
        $("#shipstatuspopup").css("display","none");
        $("#shipstatuspopup-content").css("display","none");
        $("#shipstatuspopup_title").css("display","none");
        clearInterval(timerId);
        $("#progress_bar").css('width', "0%");
        $("#progress_text").html('');
    }
    
    function showUploadStatusWindow() {
        $("#shipstatuspopup").css("display","block");
        $("#shipstatuspopup-content").css("display","block");
        $("#shipstatuspopup_title").css("display","block");
        
        var oThis = $("#shipstatuspopup-main");
        var topposition = ( $(window).height() - oThis.height() ) / 2;
        if ( topposition < 10 ) 
            topposition = 10;
        oThis.css("top", ( topposition + $(window).scrollTop() + "px"));
        oThis.css("left", ( $(window).width() - oThis.width() ) / 2+$(window).scrollLeft() + "px");

        timerId = setInterval(trackStatus, 3000);

        return true;
    }
    
    function trackStatus() {
        $.get("getprogressinfo.php?progresskey="+progressKey,
            function(percent) {
                if(percent != "false") {
                    $("#progress_bar").css('width', percent+"%");
                    $("#progress_text").html(percent);
                }
            }
        );
    }

	$(document).ready(function(){
		
	   $('#ship_report_table').hide();
	   courierPrefChanged();
	 
	   $("#orderid").keydown(function(event) {
	        // Allow only backspace and delete
	        if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode ==13) {
	            // let it happen, don't do anything
	        }
	        else {
	            // Ensure that it is a number and stop the keypress
	            if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
	                event.preventDefault(); 
	            }   
	        }
	    });

		$('#uploadstatuspopup').hide();	
		$('#order_table_header').hide();	
		$('#uploadstatuspopup')
			.ajaxStart(function(){
				$(this).show();
			}).ajaxStop(function(){
				$(this).hide();
			});													
	});

	function setbackground(sel)
	{
		sel.style.backgroundColor = sel.options[sel.selectedIndex].style.backgroundColor;
		courierPrefChanged();
	}

   /*
	* Eanble operations once courier preferences saved.
	*/
	function courierPrefSaved(){
	   $("#save-courier-pref-btn").attr("disabled", true);
	}

	/*
	* Disable operations not available once 
	* courier preferences are changed.
	*/
	function courierPrefChanged(){
		$("#save-courier-pref-btn").attr("disabled", false);
	}
	
	function addCourierFilter() {			
		noOfCourierFilters ++;
		var new_courier_filter_html = '<tr id="row_'+noOfCourierFilters+'">';
		new_courier_filter_html += '<td>Select '+noOfCourierFilters+' courier preference: </td>';
		new_courier_filter_html += '<td>';
		new_courier_filter_html += '<select id="filter_'+noOfCourierFilters+'" name="filter_'+noOfCourierFilters+'" onchange="setbackground(this)"><option value="0"> --Select Courier-- </option>';
		for(var i in couriers) {
			new_courier_filter_html += '<option value="'+couriers[i].code+'" style="background-color:'+ couriers[i].color_code +';">'+couriers[i].name+'</option>';
		}
		new_courier_filter_html += '</select> &nbsp;&nbsp;';
		new_courier_filter_html += '<a id="remove_link_'+noOfCourierFilters+'" href="javascript:void(0);" onclick="removeCourierFilter();">Remove Filter</a>';
		new_courier_filter_html += '</td>';
		new_courier_filter_html += '</tr>';
		$("#ship_report_table tr:last").before(new_courier_filter_html);

		new_courier_filter_html = '<tr id="row_'+noOfCourierFilters+'_space"><td colspan=2>&nbsp;</td></tr>';
		$("#ship_report_table tr:last").before(new_courier_filter_html);

		if($("#remove_link_"+(noOfCourierFilters-1))){
			$("#remove_link_"+(noOfCourierFilters-1)).css('display', 'none');
		}
		$("#noofcouriers").val(noOfCourierFilters);
	}

	function removeCourierFilter() {
		$("#row_"+noOfCourierFilters).remove();
		$("#row_"+noOfCourierFilters+"_space").remove();
		noOfCourierFilters--;
		if($("#remove_link_"+(noOfCourierFilters))){
			$("#remove_link_"+(noOfCourierFilters)).css('display', 'inline');
		}
		$("#noofcouriers").val(noOfCourierFilters);
		courierPrefChanged();
	}

	function validateForm() {
		if($("#orderids").val() == "") {
			alert("Please enter orderids to generate shipping report.");
			return false;
		} else {
			var orderids = $("#orderids").val();
			orderids = orderids.split('\n');
			for(var i in orderids) {
				var orderid = orderids[i].trim();	
				if(orderid.replace(/[\d]/g, '') != "") {
					alert("Invalid separators used for order ids");
					return false;
				}
			}
		}
		return true;
	}

	function resetRTS(){		
		removeOrders();
		getCourierPref();
	}
	function removeOrders(){
		$("#rtsOrderIds").text('');
		$("#orderids").text('');
		$("#orderid").val('');
		$("#order_table tr").remove();
		$("#markship-btn").attr('disabled', true);
		batchid = '';
	}

	function markOrdersShipped(){
	   $("#markship-btn").attr('disabled', true);
	   $("#successMsg").html('');
	   $("#errorMsg").html('');
		if($("#rtsOrderIds").val() == "") {
			alert("No order found which can be marked shipped.");
			return false;
		} 

        showUploadStatusWindow();
		$.ajax({
			   type: "POST",
			   url: "",
			   data: "action=mark_orders_shipped&orderids=" +  $("#rtsOrderIds").val()+"&progresskey="+progressKey+"&login="+adminUser,
			   success: function(response){
			        response = $.parseJSON(response);
			        if(response.successmsg != '') {
			             $("#successMsg").html(response.successmsg);
			             
			         }
			        if(response.errormsg != '')
			             $("#errorMsg").html(response.errormsg);
				        
                    hideUploadStatusWindow();
                    
                    if(response.successful_orderids && response.successful_orderids.length > 0) {
                        for(var i in response.successful_orderids) {
                            var remarks = $("#remark_"+response.successful_orderids[i]).html();
                            $("#remark_"+response.successful_orderids[i]).html("Shipped via "+remarks);
                        }
                    }
                    
                    var rtsOrderIds = "";
                    if(response.failed_orderids && response.failed_orderids.length > 0) {
                        for(var i in response.failed_orderids) {
                            $("#remark_"+response.failed_orderids[i]).html("Failed to Mark Shipped");
                            rtsOrderIds += response.failed_orderids[i]+",";
                        }
                    }
                    
                    if(response.invalid_orderids && response.invalid_orderids.length > 0) {
                        for(var i in response.invalid_orderids)
                            $("#remark_"+response.invalid_orderids[i]).html("Invalid Order");
                    }
                    
                    $("#rtsOrderIds").text(rtsOrderIds);
                    if(rtsOrderIds != "")
                        $("#markship-btn").attr('disabled', false);
			   }
		});
		return false;
	}
	
	function orderScanned(){
	   $("#errorMsg").html('');
		var orderid = $("#orderid").val();
		if(orderid == null || orderid.trim() == "" || orderid == "") {
			alert("Blank Order Id");
			return false;
		}

		if($("#orderids").text().search(new RegExp(orderid + '\n')) >= 0) {
			alert("ERROR: Order already scanned: " + orderid);
			$("#orderid").val('');
			return false;
		}else{
			$("#orderids").append( orderid + "\n");
		}
		
		var rts_orderids = $("#rtsOrderIds").val();
	    var rtsorderids_array = rts_orderids.split(",");
	    if(rtsorderids_array.length == 51) {
	       alert("50 orders are ready to be shipped. You can mark only 50 orders as shipped in one go.");
	       return false;
	    }

		$.ajax({
			   type: "POST",
			   url: "",
			   data: "action=order_check&orderids=" +  $("#orderid").val()+"&login="+adminUser+"&batchid="+batchid, 
			   success: function(msg){
				    if(msg['batchid'] != ''){
				   		batchid = msg['batchid'];
				   		$("#batchinfo").html(batchid);
				    }
				    var trackingNo = msg['tracking_no'];
				    if(!trackingNo || trackingNo == ""){
				        trackingNo = "N/A";
				    }
			        var newRow = '<tr id="orderTableRow" bgcolor="';
					if(msg['courier']){
			        	newRow 	+= colorMap[msg['courier']];
					}else{
						newRow 	+= '#F7F6F6';
					}				    	
				    newRow	+= '"><td>'; 
					newRow  += msg['orders'][0];
					newRow  += '</td>';
					newRow	+= '<td>'; 
					newRow  += trackingNo;
					newRow  += '</td>';
					newRow  += '<td id="remark_'+ msg['orders'][0] + '">' + msg['remark'] + ' </td></tr>';
					$("#order_table").prepend(newRow);
					
					$('#order_table_header').show();
			   }
		   
		});
		 
		$("#orderid").val('');
		return false;
	}

	function getCartonInfo(){
		if(validateForm()){
			document.getElementById('action').value = 'cartoninfo';
			document.forms['report-generate-form'].submit();
		}
	}
	function getItemInfo(){
		if(validateForm()){
			document.getElementById('action').value = 'iteminfo';
			document.forms['report-generate-form'].submit();
		}
	}
	function getCourierInfo(){
		if(validateForm()){
			document.getElementById('action').value = 'generate_report';
            document.forms['report-generate-form'].submit();
		}
	}

	function saveCourierPref(){
		var whId = $("#warehouse").val();
		if(whId == "0"){
			alert("Please select a warehouse.");
			return false;
		}
		
		var selectedCouriers = [];
		var courierFilter = "";
		for(i=1; i<=noOfCourierFilters; i++) {
			var c = $("#filter_"+i).val();
			if(selectedCouriers.indexOf(c) != -1){
				alert("Same courier selected across multiple preferences");
				return false;
			}
			if(c == "0"){
				alert("Courier not selected for "+i+" preference");
				return false;
			}
			
			selectedCouriers[selectedCouriers.length] = c;
			courierFilter += "&filter_"+i +"="+c;
		}

		$.ajax({
			   type: "POST",
			   url: "",
			   data: "action=save_courier_pref&wh_id=" + whId + "&wh_barcode=" + whId + "&add_or_Update=" + addOrUpdate + "&noofcouriers="+ noOfCourierFilters +courierFilter,
			   success: function(msg){				   
				   var resp = $.parseJSON(msg);
				   if(resp.status == 'failure'){
					   alert(resp.msg);
				   }else{
					   alert(resp.msg);
				   }
				   courierPrefSaved();				   
			   }
		   
		});
		
		return true;
	}

	function getCourierPref(){
		removeOrders();
		
		$('#ship_report_table').hide();
		
		len = noOfCourierFilters;
		for(i=2; i<=len; i++) {
			removeCourierFilter();			
		}
		$('#filter_1').val(0);
		$('#filter_1').trigger('change');
		   
		var whId = $("#warehouse").val();
		if(whId == "0"){
			alert("Please select a warehouse.");
			return false;
		}

		$('#preloaderText').text('Loading...');

		$.ajax({
			   type: "POST",
			   url: "",
			   data: "action=get_courier_pref&wh_id=" + whId,
			   success: function(msg){
				   var resp = $.parseJSON(msg);
				   if(resp.status == 'failure'){
					   alert("Failed to load courier preferences.");
				   }else{
					   i=0;			   
					   $.each(resp.data, function(key, value) {
						   i++;
						   if(i>1){
						      addCourierFilter();
						   }
						   $('#filter_' + i).val(value.code);
						   $('#filter_' + i).trigger('change');
					   });
				   }
				   addOrUpdate = i>0?"update":"add";
				   courierPrefSaved();				   		   
				   $('#ship_report_table').show();				   						   
			   }
		});

		
		return;
	}
</script>
{/literal}
{capture name=dialog}
<div style="clear:both;padding:10px;">
	<a href="/admin/rts_batch_info.php">Ready To Ship Batches</a>
</div>
{if ($mode == 'set_courier_pref') }
<br>
	<div id="set_courier_pref_div" style="width: 850px; text-align:left;">
		<table id="wh_table" cellspacing=0 cellpadding=0 border=0 width=500>	
			<tr id="row_{$rowcount}" bgcolor="#6D7B8D">
				<td align="center">
					<select id="warehouse" name="warehouse" onChange="return getCourierPref();">
						<option value="0"> --Select Warehouse-- </option>
						{section name=i loop=$warehouses}
							<option value="{$warehouses[i].id}">{$warehouses[i].name}</option>	
						{/section}
					</select> &nbsp;&nbsp;				
				</td>
			</tr>	
		</table>
		<table id="ship_report_table" cellspacing=0 cellpadding=0 border=0 width=500>
			<tr><td colspan=2>&nbsp;</td></tr>
			{if $courier_filters|@count > 0 }
				{assign var="rowcount" value=1}
				{section name=idx loop=$courier_filters}
					<tr id="row_{$rowcount}">
						<td>Select {$rowcount} courier preference: </td>
						<td>
							<select id="filter_{$rowcount}" name="filter_{$rowcount}" onchange="setbackground(this)">
								<option value="0" style="background-color:#0000ff"> --Select Courier-- </option>
								{section name=i loop=$couriers}
									<option value="{$couriers[i].code}" style="background-color:{$couriers[i].color_code};" {if $couriers[i].code eq $courier_filters[idx]} selected {/if}>{$couriers[i].name}</option>	
								{/section}
							</select> &nbsp;&nbsp;
							{if $rowcount neq 1 }
								{if $rowcount neq $num_courier_filters }
									<a id="remove_link_{$rowcount}" href="javascript:void(0);" onclick="removeCourierFilter();" style="display:none;">Remove Filter</a>
								{else}
									<a id="remove_link_{$rowcount}" href="javascript:void(0);" onclick="removeCourierFilter();">Remove Filter</a>	
								{/if}
							{/if}
						</td>
					</tr>
					<tr id="row_{$rowcount}_space"><td colspan=2>&nbsp;</td></tr>
					{assign var="rowcount" value=$rowcount+1}
				{/section}
			{else}
				<tr id="row_1">
					<td>Select 1 courier preference: </td>
					<td>
						<select id="filter_1" name="filter_1" onchange="setbackground(this)">
							<option value="0"> --Select Courier-- </option>
							{section name=i loop=$couriers}
								<option value="{$couriers[i].code}" style="background-color:{$couriers[i].color_code};">{$couriers[i].name}</option>	
							{/section}
						</select>
					</td>
				</tr>
				<tr id="row_1_space"><td colspan=2>&nbsp;</td></tr>
			{/if}
			<tr><td colspan=2><a href="javascript:void(0);" onclick="addCourierFilter();">Add More Courier Preferences</a></td>
		</table>
		</br>
		</br>
		<input type="button" id="save-courier-pref-btn" name="save-courier-pref-btn" value="Save" onclick="saveCourierPref();">
		<input type="button" name="reset-btn" value="Reset" onclick="getCourierPref();">
	</div>
{/if}
{if ($mode == 'gen_rts_report') }
<div id="order_scan_btn_div" style="text-align:left" align="center">
	<form id="report-generate-form" action="" method="POST">
		<input type="hidden" id="action" name="action" value="generate_report">
		<input type="hidden" id="noofcouriers" name="noofcouriers" value="{$num_courier_filters}">
		<textarea id="orderids" name="orderids" rows="1" style="width: 650px;display:none;">{$orderids}</textarea>
		<textarea id="rtsOrderIds" name="rtsOrderIds" rows="1" style="width: 650px;display:none;">{$rtsOrderIds}</textarea>
		<!--input type="button" name="carton-info" value="Get Item Info" onclick="getItemInfo();" -->		
		<input type="button" name="generate-report-btn" value="Generate Report" onclick="getCourierInfo();">
		<!--input type="button" id="markship-btn" name="markship-btn" value="Mark Orders Shipped" onclick="markOrdersShipped();" disabled -->
		<input type="button" name="reset-btn" value="Reset" onclick="removeOrders();">
	</form>
</div>
<div id="errorMsg" style="color:red; font-size: 120%; font-weight: bold; text-align:center; margin: 20px 0;"></div>
<div id="successMsg" style="color:green; font-size: 120%; font-weight: bold; text-align:center; margin: 20px 0;"></div>
<div style="clear:both;padding:10px;padding-bottom:20px;padding-left:300px;font-weight: bold;color:red;font-size:14px;">
	Current Batch No
	<span id ="batchinfo">
		{if $batchid eq ''}
			will be generated on first order scan
		{else}
			{$batchid}	
		{/if}
	</span>
</div>
<div id="orders_ship_main_div" align="center">
	<form action="" method="POST" onsubmit="return orderScanned()">
		<input type="hidden" id="action" name="action" value="order_check">
		<label style="color: #000000; font-family: Verdana; font-weight: bold; font-size: 15px;">ENTER ORDER-ID:</label>
		<input type="text" id="orderid" name="orderid" style="width: 340px;color: #FFFFFF; font-family: Verdana; font-weight: bold; font-size: 20px; background-color: #CEE3F6;"/>
	</form>
</div>
	</br>
	</br>
<div id="orders_table_div" align="center">	
	<table id="order_table_header" border=1 width=700>
	<col width="200" />
	<col width="200" />
	<col width="300" />
	<tr bgcolor="#A9A9A9"><th>Order</th><th>Tracking No.</th><th>Remark</th></tr>
	</table>
	<table id="order_table" border=1 width=700>
	<col width="200" />
	<col width="200" />
	<col width="300" />
	  </table>
	<br/>
</div>
{/if}
<div id="uploadstatuspopup" style="display:none">
	<div class="overlay" style="display:block;_position:absolute;_width:1300px;_height:1500px;">&nbsp;</div>
	<div style="width: 100%; text-align:center; position: absolute; left: 10%; top: 20%; font-size: 150%; font-weight:bold; color: green;" >
	   Processing...
	</div>
</div>
<div id="shipstatuspopup" style="display:none">
    <div class="overlay" style="display:block;_position:absolute;_width:1300px;_height:1500px;">&nbsp;</div>
    <div id="shipstatuspopup-main" style="position:absolute; left: 20%; width:450px; background:white; z-index:1006;border:2px solid #999999;">
        <div id="shipstatuspopup-content" style="display:none;">
            <div id="shipstatuspopup_title" class="popup_title">
                <div id="shipstatuspopup_ajaxWindowTitle" class="popup_ajaxWindowTitle">Upload Status</div>
                <div class="clear">&nbsp;</div>
            </div>
            <div style="padding:5px 25px; text-align:left; font-size:12px" >
                <div id="progress_container"> 
                    <div id="progress_bar">
                        <div id="progress_completed"></div> 
                    </div>
                </div>
                <span id="progress_text" style="font-size: 14px; color: green; padding: 0 0 0 5px;">0</span> % Complete
            </div>
            <div class="clear">&nbsp;</div>
        </div>
    </div>
</div>
{/capture}
{include file="dialog.tpl" title=$title content=$smarty.capture.dialog extra='width="100%"'}
