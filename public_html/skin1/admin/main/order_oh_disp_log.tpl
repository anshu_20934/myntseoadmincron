{capture name=dialog}
	<br>
	COD OH Calling Log
	<br><br><br>
	<table border=1 cellspacing=0 cellpadding=0 width=99% style="border-color: #CCCCCC; border-collapse: collapse;">
		<tr height=30 bgcolor="#DFDFDF">
			<th style="font-size:14px;">Added by</th>
			<th style="font-size:14px;">Reason</th>
			<th style="font-size:14px;">Sub Reason</th>
			<th style="font-size:14px;">Trial number</th>
			<th style="font-size:14px;">Comments</th>
			<th style="font-size:14px;">Added On</th>
		</tr>
		{section name=idx loop=$comments_log}
			<tr height=30>
				<td align=center>{$comments_log[idx].created_by}</td>
				<td align=center>{$comments_log[idx].primary_reason}</td>
				<td align=center>{$comments_log[idx].reason_display_name}</td>
				<td>{$comments_log[idx].trial_number}</td>
				<td align=center>{$comments_log[idx].comment}</td>
				<td align=center>{$comments_log[idx].created_time}</td>
			</tr>
		{/section}
	</table>
{/capture}
{include file="dialog.tpl" title='COD OH Calling Log' content=$smarty.capture.dialog extra='width="100%"'}