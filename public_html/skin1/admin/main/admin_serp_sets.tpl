{if $status_message}
  <br/><div class="message">{$status_message}</div><br/>
{/if}
<script type="text/javascript" src="{$http_location}/skin1/js_script/jquery.js"></script>
<script type="text/javascript" src="{$http_location}/skin1/js_script/thickbox.admin.js"></script>
<link rel="stylesheet" href="{$http_location}/skin1/css/thickbox.css" />
<script type="text/javascript">
  window.tb_pathToImage = "{$cdn_base}/images/loadingAnimation.gif";
{literal}
  function loadJs(url){
    var sc = $('#script-container')[0];
    var node = document.createElement("script");
    node.src = url;
    sc.appendChild(node); 
  }
  function checkall(){
    if($("#checkallbox").attr("checked"))
	  $(".abc").attr("checked","checked");
    else
	  $(".abc").removeAttr("checked");
  }
  function deleteSet(id){
    var text = $("tr#tr_"+id+" td a")[0].innerHTML;
    if(!confirm("Do you want to delete the set '"+text+"' ? ")) return;
    var u = window.location.pathname+"?mode=deleteSet&setid="+id+"&handler=setDeleted";
    loadJs(u);
  }
  function setDeleted(id){
    var row = $("tr#tr_"+id)[0];
    row.parentNode.removeChild(row);
    alert("Set Deleted");
  }
  function updateSet(id){
    var tr = $("tr#tr_"+id);
    var name = $("td a",tr)[0].innerHTML;
    var desc = $("td",tr)[1].innerHTML;
    var form = $("form#setUpdateForm")[0];
    form.setName.value = name;
    form.setDesc.value = desc;
    form.setid.value = id;

    tb_show("updating set - "+name,"#TB_inline?height=150&width=300&inlineId=updateSetForm",false);
    return false;    
  }
  function deleteRecord(id){
    var text = $("tr#tr_"+id+" td a")[0].innerHTML;
    if(!confirm("Do you want to delete the keyword '"+text+"' ? ")) return;
    var u = window.location.pathname+"?mode=delete&id="+id+"&handler=recordDeleted";
    loadJs(u);    
  }
  function recordDeleted(id){
    var row = $("tr#tr_"+id)[0];
    row.parentNode.removeChild(row);
    alert("Record Deleted");
  }
  function updateRecord(id){
    var tr = $("tr#tr_"+id);
    var name = $("td a",tr)[0].innerHTML;
    var urls = $("td div.mainurls",tr)[0].innerHTML.replace("<br/>","\n").replace("<br>","\n");
    var form = $("form#keywordUpdateForm")[0];
    form.keyword.value = name;
    form.urls.value = urls;
    form.id.value = id;

    tb_show("updating keyword - "+name,"#TB_inline?height=170&width=400&inlineId=updateKeywordForm",false);
    return false;
  }
  function testSERP(){
    var form = $("form#serpTesterForm")[0];
    $.get(
      window.location.pathname+'?mode=test&keyword='+form.keyword.value+"&urls="+form.urls.value.replace("\n",","),
      function(data){
        $('div#testerOutput').html(data);
      }
    );
    return false;
  }
  function showStats(){
    var keys = $("input.abc[@checked]");
    if(keys.length==0){
      alert("no keyword selected");
      return;
    }
    var x=[];
    keys.each(function(){x.push(this.name.match(/[0-9]/g).join(''));});
    var ids = x.join(',');
    tb_show("stats for multiple keywords",window.location.pathname+"?id="+ids+"&mode=keystats&height=470&width=850",false);
  }
  $(function init(){
    $("input.fuzzy").each(function(){
      this.checked = (this.getAttribute("isfuzzy")==1)?true:false;
    }).click(function toggleFuzzy(){
      var checked = this.checked?1:0;
      this.checked = !this.checked;
      var id = this.getAttribute("rel");
      var box = this;
      box.disabled = true;
      $.get(
        window.location.pathname+'?mode=updatefuzzy&id='+id+'&isfuzzy='+checked,
        function(data){
          box.disabled = false;
          box.checked = (data==0)?false:true;
        }
      );
      
    });
  });
{/literal}
</script>

<div id="serpTester" style="float:left;">
  <a href="#TB_inline?dummy&height=180&width=300&inlineId=testSERPForm" class="thickbox" title="compute serp" style="text-decoration:none;"><button style="font-size:10px;">SERP Computer</button></a>

  <button value="Show Stats for Selected Keywords" onclick="showStats()" style="font-size:10px;">Show Stats for Selected Keywords</button>
  
  <button value="Run Rank Computer Cron" onclick="$.get('/crons/rank-computer.php'); $('button#rcompute').attr('disabled', 'disabled');" id="rcompute" style="font-size:10px;">Run Rank Computer Cron</button>
  
  <div id="testSERPForm" style="display:none;">
  <form action="{$main}.php" method="post" name="serpTesterForm" id="serpTesterForm">
    <input type="hidden" name="mode" value="testSERP" />
    <table>
    <tr><td valign="top"><b>Keyword</b> : </td><td><input type="text" name="keyword" style="width:220px;"/><br/></td></tr>
    <tr><td valign="top"><b>URLs</b> : </td><td><textarea name="urls" style="width:220px;height:70px;"></textarea><br/></td></tr>
    <tr><td valign="bottom" align="center" colspan="2">
      <input type="submit" value="{$lng.lbl_test|escape}" onclick="return testSERP();"/>
      <input type="button" value="{$lng.lbl_close|escape}" onclick="tb_remove()" />      
    </td></tr>
    <tr><td valign="bottom" align="left" colspan="2"><br/><div id="testerOutput"></div></td></tr>
    </table>    
  </form>
  </div>
</div>

<div class="pagination" style="float:right;margin-bottom:5px;">
<!--pagination-->
{if $totalpages > 1}
  {$paginator}
{/if}	
</div>
<!--pagination-->
<div style="float:none;clear:both;"><br/></div>

{capture name=dialog}
{if $show_keywords}
<table cellpadding="3" cellspacing="1" width="100%" class="records serp">
<tr>
<td colspan="2" align="left" valign="top">
  &#171; <a href="{$main}.php" title="back">Back to set list</a>
</td>
<td colspan="1" align="left">
    {if $setArr}
    <b>Select another set </b> : 
    <select id="setSelector">
    {section name=i loop=$setArr}
    <option value="{$setArr[i][0]}" {if $setArr[i][0] == $setid}selected{/if}>{$setArr[i][1]}</option> 
    {/section}
    </select>
    <script type="text/javascript">
      $("select#setSelector").change(
        {literal}
        function(){window.location=window.location.pathname+"?setid="+this.value;}
        {/literal}
      );
    </script>
    <!--
    &nbsp;&nbsp;
    <a href="{$main}.php?setid={$setid}&mode=unused" title="Unused Keywords in {$setName}" class="thickbox">Unused keywords</a>
    -->
    {/if}
</td>
<td colspan="8" align="right">
  <form action="{$main}.php?setid={$setid}" method="POST" enctype="multipart/form-data" style="float:right;">
    <strong>upload CSV :</strong>
    <input type="hidden" name="MAX_FILE_SIZE" value="102400" />
    <input type="hidden" name="mode" value="upload" />
    <input type="hidden" name="setid" value="{$setid}" />
    <input type="file" name="csv_file" id="csv_file"/>
    <input type="submit" value="upload"/>
    <a href="{$main}.php?setid={$setid}&mode=download" id="download_bt" style="text-decoration:none;">
      <input type="button" value="download" />
    </a>
    <a href="{$main}.php?setid={$setid}&mode=downloadall" id="downloadall_bt" style="text-decoration:none;">
      <input type="button" value="download data" />
    </a>
  </form>
  <br style="float:none;clear:both;"/>
</td></tr>
{if $resultset}
<tr class="TableHead">
  <td width="10"><input type='checkbox' onchange='javascript:checkall();' id='checkallbox'/></td>
  <td align="left" width="140"><a href="{$sorturl}keyword">Keyword</a></td>
  <td align="left" width="280">URLs</td>
  <td align="left"><a href="{$sorturl}serp1">SERP</a></td>
  <td><a href="{$sorturl}visits">Visits</a></td>
  <td><a href="{$sorturl}transactions">Transactions</a></td>
  <td><a href="{$sorturl}items">Items</a></td>
  <td><a href="{$sorturl}revenue">Revenue</a></td>
  <td><a href="{$sorturl}conversion">Conversion</a></td>
  <td>Fuzzy</td>
  <td align="center" width="90">Actions</td>
</tr>
{section name=i loop=$resultset}
<tr {cycle values=", class='TableSubHead'"} id="tr_{$resultset[i][0]}">
  <td width="10" valign="top">
    <input class='abc' type="checkbox" name="posted_data[{$resultset[i][0]}][key_id_checkbox]" />
  </td>
  <td align="left" class="td1" valign="top">
    <a href="{$main}.php?id={$resultset[i][0]}&mode=keystats&height=470&width=850" class="thickbox" title="More Stats for {$resultset[i][1]}">{$resultset[i][1]}</a>
  </td>
  <td align="left" class="td2 urls" valign="top">
    <div class="mainurls">{$resultset[i][2]}</div>
    {if $resultset[i][8]}<div class="otherurl">{$resultset[i][8]}</div>{/if}
  </td>
  <td align="left" class="td2" width="60" valign="top">{$resultset[i][3]}</td>
  <td class="td2">{$resultset[i][4]}</td>
  <td class="td2">{$resultset[i][5]}</td>
  <td class="td2">{$resultset[i][6]}</td>
  <td class="td2">{$resultset[i][7]}</td>
  <td class="td2">{$resultset[i][9]} %</td>
  <td class="td2" align="center"><input type="checkbox" rel="{$resultset[i][0]}" class="fuzzy" isfuzzy="{$resultset[i][10]}" /></td>
  <td align="center" class="td2" valign="top">
    <a href="javascript:void(0);" onclick="deleteRecord({$resultset[i][0]})" title="delete this record"><span class="delete-bt"></span></a> 
    &nbsp;
    <a href="javascript:void(0);" onclick="updateRecord({$resultset[i][0]})" title="edit this record"><span class="edit-bt"></span></a>
  </td>
</tr>
{/section}
{else}
  <tr><td colspan="10" align="center" style="border:none;">No Results Found</td></tr>
{/if}
<tr><td colspan="10" style="border:none;">
  <br />
  <div id="updateKeywordForm" style="display:none;">
  <form action="{$main}.php?setid={$setid}" method="post" name="keywordUpdateForm" id="keywordUpdateForm">
    <input type="hidden" name="mode" value="update" />
    <input type="hidden" name="id" value="" />
    <table>
    <tr><td valign="top"><b>Keyword</b> : </td><td><input type="text" name="keyword" style="width:320px;"/><br/></td></tr>
    <tr><td valign="top"><b>URLs</b> : </td><td><textarea name="urls" style="width:320px;height:70px;"></textarea><br/></td></tr>
    <tr><td colspan="2"><sup>*</sup> you can put multiple URLs.. one per line<br/><br/></td></tr>  
    <tr><td valign="bottom" align="center" colspan="2">
      <input type="submit" value="{$lng.lbl_update|escape}"/>
      <input type="button" value="{$lng.lbl_cancel|escape}" onclick="tb_remove()" />      
    </td></tr>  
    </table>    
  </form>
  </div>
</td></tr>
</table>
{else}
<style>
{literal}
table.serpstats tr.avg td{padding:5px;font-size:12px;font-weight:bold;color:#FFF;background-color:#444;}
td.rline{border-right:1px solid #333;}
{/literal}
</style>
<table cellpadding="3" cellspacing="1" width="100%" class="records serp">
{if $resultset}
<tr class="TableHead">
  <td align="left" width="80">Name</td>
  <td align="left">Description</td>
  <td align="center" width="50">Updated</td>
  <td align="center" width="30">Keywords</td>
  <td align="center" width="30">Avg. SERP</td>
  <td align="center" width="30">Visits</td>
  <td align="center" width="30">Revenue</td>
  <td align="center" width="30">Conversion</td>
  <td align="center" width="110">Actions</td>
</tr>
{section name=i loop=$resultset}
<tr {cycle values=", class='TableSubHead'"} id="tr_{$resultset[i][0]}">
  <td align="left" class="td1"><a href="{$main}.php?setid={$resultset[i][0]}">{$resultset[i][1]}</a></td>
  <td align="left" class="td2">{$resultset[i][2]}</td>
  <td align="left" class="td2">{$resultset[i][3]}</td>
  <td align="left" class="td2">{$resultset[i][5]}</td>  
  <td align="left" class="td2">{$resultset[i][4]}</td>
  <td align="left" class="td2">{$resultset[i][6]}</td>
  <td align="left" class="td2">{$resultset[i][7]}</td>
  <td align="left" class="td2">{$resultset[i][8]} %</td>
  <td align="center" class="td2">
    <a href="javascript:void(0);" onclick="deleteSet({$resultset[i][0]})" title="delete this set"><span class="delete-bt"></span></a> 
    &nbsp;
    <a href="javascript:void(0);" onclick="updateSet({$resultset[i][0]})" title="edit this set"><span class="edit-bt"></span></a>
    &nbsp;
    <a class="thickbox" title="More Stats for Set - <b>{$resultset[i][1]}</b>" 
      href="{$main}.php?setid={$resultset[i][0]}&mode=keystats&height=470&width=850"><span class="info-bt"></span></a>
    &nbsp;
    <a class="thickbox" title="SERP Stats for Set - <b>{$resultset[i][1]}</b>" 
      href="{$main}.php?setid={$resultset[i][0]}&mode=serpstats&height=470&width=1020"><span class="serp-bt"></span></a>
  </td>
</tr>
{/section}
{else}
  <tr><td colspan="9" align="center" style="border:none;">No Results Found</td></tr>
{/if}
<tr><td colspan="9" style="border:none;">
  <br />
  <input type="button" value="{$lng.lbl_create|escape}" alt="#TB_inline?height=150&width=300&inlineId=createSetForm" class="thickbox" title="Create a new Set">
  <div id="createSetForm" style="display:none;">
  <form action="{$main}.php" method="post" name="serpSetForm">
    <input type="hidden" name="mode" value="create" />
    <table>
    <tr><td valign="top"><b>Set Name</b> : </td><td><input type="text" name="setName" style="width:220px;"/><br/></td></tr>
    <tr><td valign="top"><b>Description</b> : </td><td><textarea name="setDesc" style="width:220px;height:70px;"></textarea><br/><br/></td></tr>
    <tr><td valign="bottom" align="center" colspan="2">
      <input type="submit" value="{$lng.lbl_create|escape}"/>
      <input type="button" value="{$lng.lbl_cancel|escape}" onclick="tb_remove()" />      
    </td></tr>    
    </table>    
  </form>
  </div>
  <div id="updateSetForm" style="display:none;">
  <form action="{$main}.php" method="post" name="setUpdateForm" id="setUpdateForm">
    <input type="hidden" name="mode" value="updateSet" />
    <input type="hidden" name="setid" value="" />
    <table>
    <tr><td valign="top"><b>Set Name</b> : </td><td><input type="text" name="setName" style="width:220px;"/><br/></td></tr>
    <tr><td valign="top"><b>Description</b> : </td><td><textarea name="setDesc" style="width:220px;height:70px;"></textarea><br/><br/></td></tr>
    <tr><td valign="bottom" align="center" colspan="2">
      <input type="submit" value="{$lng.lbl_update|escape}"/>
      <input type="button" value="{$lng.lbl_cancel|escape}" onclick="tb_remove()" />      
    </td></tr>    
    </table>    
  </form>
  </div>
</td></tr>
</table>
{/if}
{/capture}

{include file="dialog.tpl" title=$lng.lbl_productstyle_heading content=$smarty.capture.dialog extra='width="100%"'}

<br/>
<button value="Show Stats for Selected" onclick="showStats()" style="font-size:10px;">Show Stats for Selected</button>

<div class="pagination" style="float:right;margin-bottom:5px;">
<!--pagination-->
{if $totalpages > 1}{$paginator}{/if}
</div>
<!--pagination-->
<div id="script-container"></div>
