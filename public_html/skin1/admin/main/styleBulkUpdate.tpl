<div>Change Article Group: <select name="select_aid"  id="select_aid" onChange="reloadPage()">
	{foreach from=$articleIds item=article}
		<option value="{$article.id}" {if $articleTypeId eq $article.id} selected {/if}>{$article.name}</option>
	{/foreach}
</select>
<br>
</div>
<script type="text/javascript">
	var articleTypeId = {$articleTypeId};
	var style_ids = '{$style_ids}';
	var styleIdList    = {$styleIdList};
	var noStyleColumns = styleIdList.length;
	
	var styleAll = {$styleAll};
	var styleColKeys = {$styleColKeys};
	var styleStatusData = {$styleStatus};
	var possibleStatusTrans = {$possibleStatusTrans};
	var stylePropertyName = {$stylePropertyName};
	
    var brandData         = {$brand};
    var ageGroupData      = {$ageGroup};
    var genderData        = {$gender};
    var fashionTypeData   = {$fashionType};
    var colourData        = {$colour};
    var seasonData        = {$season};
    var yearData          = {$year};
    var usageData         = {$usage};

    var classificationBrandData = {$classificationBrand};
    
    var specificAttributeExist = {$specificAttributeExist};
    
    {if isset($specificAttribute)}
    	var specificAttributeData = {$specificAttribute};
    	var specificAttributeName = {$specificAttributeName};
    {/if}
    
    var masterCategoryData = {$masterCategory};
	var subCategoryData    = {$subCategory};
	var articleTypeData    = {$articleType};
	var productTypeData    = {$productType};

    var httpLocation       = '{$http_location}';
    {literal}
    function reloadPage()
    {
    	var r = confirm("Are you sure want to change the group? Any unsaved changes will be lost.");
	    if(r == true)
	    {
	    	var aid = $("#select_aid").val();
	        var url = httpLocation+'/admin/catalog/styleBulkUpdate.php?aid='+aid+'&style_ids='+style_ids;
	    	window.open(url, '_self', false);
	    }
	    else
	    {
	    	$("#select_aid").val(articleTypeId);
	    }
    }
    {/literal}
</script>
<link rel="stylesheet" type="text/css" href="{$http_location}/admin/extjs/resources/css/ext-all.css"/>
<script type="text/javascript" src="{$http_location}/admin/extjs/adapter/ext/ext-base.js"></script>
<script type="text/javascript" src="{$http_location}/admin/extjs/ext-all.js"></script>
<script type="text/javascript" src="styleContentHtmlEditor.js"></script>
<script type="text/javascript" src="StylePickerDialog.js"></script>
<script type="text/javascript" src="styleSearchCopy.js"></script>
<script type="text/javascript" src="styleBulkUpdate.js"></script>
<div id="contentPanel"></div>