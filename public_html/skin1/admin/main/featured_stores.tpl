{* $Id: featured_products.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
{literal}
<script language="Javascript">
   function chkFields(frm){
   	  var mobj = frm.mailerimg;  
   	  if(mobj == ''){
   	  	alert("Browse image to upload.")
   	  }
   }
</script>
{/literal}
<a name="featured" />
{$lng.txt_featured_products}
<br /><br />

{capture name=dialog}

<form name='uplMailer' id='' action='featured_stores.php' method='POST' enctype="multipart/form-data" onsubmit="return chkFields(this);">
{if $mode eq 'modify'}
{section name=idx loop=$contest}
<input type='hidden' name='ids[]' value = "{$contest[idx].id}" >
<input type='hidden' name='mode' value = "modify" >
  <div>
     <p><strong>Store Name</strong> : <input type="text" name="storename[]" id="storename" value="{$contest[idx].storename}" /><strong>eg: Nike Store</strong></p>
 </div>
 <div>
     <p><strong>Store URL</strong> : <input type="text" name="storeurl[]" id="storeurl" value="{$contest[idx].storeurl}"/><strong>eg: nike-store</strong></p>
 </div>
  <div>
     <p><strong>Display Order</strong> : <input type="text" name="displayorder[]" id="displayorder" value="{$contest[idx].display_order}"/><strong>eg:1</strong></p>
 </div>
 <div>
     <span style='font-size:1.5em;font-weight:bold;'>Upload Stores Image for Horizontal</span>
     <p><strong>Browse image </strong> : <input type="file" name="storeimage1[]" id="storeimage1"  /><img src='{$cdn_base}/{$contest[idx].storeimage_h}' width='40' height='40' /></p>
     
 </div>
 <div>
     <span style='font-size:1.5em;font-weight:bold;'>Upload Stores Image for vertical </span>
     <p><strong>Browse image </strong> : <input type="file" name="storeimage2[]" id="storeimage2" /><img src='{$cdn_base}/{$contest[idx].storeimage_v}' width='40' height='40' /></p>
 </div>
<div style="clear:both;"></div>
{/section}
{else}


  <div>
     <p><strong>Store Name</strong> : <input type="text" name="storename" id="storename" /><strong>eg: Nike Store</strong></p>
 </div>
 <div>
     <p><strong>Store URL</strong> : <input type="text" name="storeurl" id="storeurl" /><strong>eg: nike-store</strong></p>
 </div>
  <div>
     <p><strong>Display Order</strong> : <input type="text" name="displayorder" id="displayorder" /><strong>eg:1</strong></p>
 </div>
 <div>
     <span style='font-size:1.5em;font-weight:bold;'>Upload Stores Image for Horizontal</span>
     <p><strong>Browse image </strong> : <input type="file" name="storeimage1" id="storeimage1" /></p>
 </div>
 <div>
     <span style='font-size:1.5em;font-weight:bold;'>Upload Stores Image for vertical </span>
     <p><strong>Browse image </strong> : <input type="file" name="storeimage2" id="storeimage2" /></p>
 </div>
<div style="clear:both;"></div>

{/if}
<div ><input type="submit" name="smtImages" value="submit" />&nbsp;&nbsp;&nbsp;</div>
</form>
 	
{/capture}
{include file="dialog.tpl" title='Featured Stores Management' content=$smarty.capture.dialog extra='width="100%"'}
<br /><br />

