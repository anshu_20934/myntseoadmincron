<br/>
<br/>
{capture name=dialog}

<table width="100%"  border="0" cellspacing="0" cellpadding="0">

<tr>
  <td valign="top">
  	<table width="98%"  border="0" align="center" cellpadding="0" cellspacing="0">
	<tr>
		 <td>
			<table width="100%" height="21" border="0" cellpadding="0" cellspacing="0" class="textBlack">
				<tr>
					<td><strong>Payment gateway details for orderid {$orderid}</strong></td>
				</tr>
			</table>
			<br>
			<table width="100%" height="25" border="1" cellpadding="0" cellspacing="0">
				<tr bgcolor="#ccffff">
					<th style="font-size:14px;">Gateway</th>
					<th style="font-size:14px;">Browser</th>
					<th style="font-size:14px;">IP Address</th>
					<th style="font-size:14px;">Pmt Option</th>
					<th style="font-size:14px;">Pmt Type</th>
					<th style="font-size:14px;">TrnsID</th>
					<th style="font-size:14px;">PmtID</th>
					<th style="font-size:14px;">InsertTime</th>
					<th style="font-size:14px;">ReturnTime</th>
					<th style="font-size:14px;">Trans Time</th>
					<th style="font-size:14px;">Flag</th>
					<th style="font-size:14px;">Tamper</th>
                    <th style="font-size:14px;">Amt Rcvd</th>
                    <th style="font-size:14px;">Rsp Code</th>
					<th style="font-size:14px;">Rsp Mssg</th>					
					<th style="font-size:14px;">Complete Rsp</th>
					<th style="font-size:14px;">Card Bank Name</th>
					<th style="font-size:14px;">Card Bin Number</th>
				</tr>
				<tr bgcolor="{cycle values="#eeddff,#d0ddff"}">					
					<td style="font-size:12px;">{$orderPaymentServiceLog.gatewayName}&nbsp;</td>
					<td><textarea name="query" cols=15 rows=3 readonly>{$orderPaymentServiceLog.userAgent}</textarea></td>
					<td style="font-size:12px;">{$orderPaymentServiceLog.ipAddress}&nbsp;</td>
					<td style="font-size:12px;">{$orderPaymentServiceLog.paymentOption}&nbsp;</td>
					<td style="font-size:12px;">{$orderPaymentServiceLog.paymentIssuer}&nbsp;</td>
	 				<td style="font-size:12px;">{$orderPaymentServiceLog.bankTransactionId}&nbsp;</td>
	 				<td style="font-size:12px;">{$orderPaymentServiceLog.gatewayPaymentId}&nbsp;</td>
	 				<td style="font-size:12px;">{$orderPaymentServiceLog.insertTime|date_format:"%d/%b/%Y, %H:%M:%S"}&nbsp;</td>
	 				<td style="font-size:12px;">{$orderPaymentServiceLog.returnTime|date_format:"%d/%b/%Y, %H:%M:%S"}&nbsp;</td>
					<td style="font-size:12px;">{$orderPaymentServiceLog.transactionTime|date_format:"%d/%b/%Y, %H:%M:%S"}&nbsp;</td>
					<td style="font-size:12px;">{if isset($orderPaymentServiceLog.isFlagged) }{if $orderPaymentServiceLog.isFlagged eq 1 } YES {else} NO{/if}{else}NA{/if}&nbsp;</td>
					<td style="font-size:12px;">{if isset($orderPaymentServiceLog.isTampered)}{if $orderPaymentServiceLog.isTampered eq 1} YES {else} NO{/if}{else}NA{/if}&nbsp;</td>
					<td style="font-size:12px;">{$orderPaymentServiceLog.amountPaid}&nbsp;</td>
					<td style="font-size:12px;">{$orderPaymentServiceLog.responseCode}&nbsp;</td>
                    <td><textarea name="query" cols=15 rows=3 readonly>{$orderPaymentServiceLog.responseMessage}</textarea></td>	
                    <td><textarea name="query" cols=15 rows=3 readonly>{$orderPaymentServiceLog.gatewayResponse}</textarea></td>
                    <td style="font-size:12px;">{$orderPaymentServiceLog.cardBankName}&nbsp;</td>
					<td style="font-size:12px;">{$orderPaymentServiceLog.binNumber}&nbsp;</td>                    
				</tr>    
			</table>
		</table>
	</td>
</tr>
</table>

{/capture}
{include file="dialog.tpl" title="Order Payment Logs from Service" content=$smarty.capture.dialog extra='width="100%"'}