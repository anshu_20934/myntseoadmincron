{* $Id: statistics.tpl,v 1.24.2.1 2006/06/16 10:47:41 max Exp $ *}
{include file="main/include_js.tpl" src="main/calendar2.js"}
{capture name=dialog}

<form action="" method="post" name="userreportform">
<input type="hidden" name="mode" value="search" />

<table cellpadding="1" cellspacing="1">


<tr>
	<th align="right">{$lng.lbl_reporttype}:</th>
	<td>
	     <select name='report_type'>
	      <option value="week"  { if $reportType == "week"} selected {/if} >Weekly</option>
              <option value="month" { if $reportType == "month"} selected {/if} >Monthly</option> 
	      
	       
           </select>
	</td>
	<th align="right">{$lng.lbl_region}:</th>
	<td>
	    <select name='region'>
	      <option value="all" {if $region eq 'all'} selected {/if} >All</option>
              {section name=idx loop=$regions}
                  <option value="{$regions[idx].code}" {if $regions[idx].code eq $region} selected {/if} >{$regions[idx].state}</option>
	       {/section}
           </select>
	</td>
</tr>

<tr>
	<th align="right">{$lng.lbl_report_from}:</th>
	<td><input type="text" name='Startdate' value="{$SDate}"/><a href="javascript:cal4.popup();" ><img src="{$cdn_base}/images/cal.gif" width="16" height="16" border="0" alt="Click Here to Pick up the date"></a></td>
	<th align="right">{$lng.lbl_report_to}:</th>
	<td><input type="text" name='Enddate'  value="{$EDate}" /><a href="javascript:cal5.popup();" class=""><img src="{$cdn_base}/images/cal.gif" width="16" height="16" border="0" alt="Click Here to Pick up the date"></a></td>
</tr>

<tr>
	<td colspan="4"><input type="submit" value="{$lng.lbl_submit|escape}" /></td>
</tr>
<script language="javascript">
      var cal4 = new calendar2(document.forms['userreportform'].elements['Startdate']);
      cal4.year_scroll = true;
      cal4.time_comp = false;

       var cal5 = new calendar2(document.forms['userreportform'].elements['Enddate']);
      cal5.year_scroll = true;
      cal5.time_comp = false;


</script>

</table>
</form>

{/capture}
{include file="dialog.tpl" title='' content=$smarty.capture.dialog extra='width="100%"'}

<br />
<br />
{if $mode}
{capture name=dialog}
<table cellpadding="1" cellspacing="1" width="100%">

<tr class="TableHead">
	<td>{$lng.lbl_duration}</td>
	<td align="center">{$lng.lbl_unq_visitors}</td>
	<td align="center">{$lng.lbl_new_users}</td>
	<td align="center">{$lng.lbl_users_bgt_products}</td>
	
	
</tr>


{if $users}

{section name=num loop=$users}

           
    {section name=idx loop=$users[num]}

<tr{cycle values=', class="TableSubHead"'}>
	
        <td  align="center">{$users[num][idx].duration1}-{$users[num][idx].duration2}</td>
	<td  align="center">{*$users[num][idx].num_of_users*}</td>
	<td  align="center">{$users[num][idx].num_of_users}</td>
	<td  align="center">{$users[num][idx].num_of_orders}</td>
	
</tr>
{/section}
{/section}
<tr{cycle values=', class="TableSubHead"'}>
	
        <td  align="center"><b>Total</b></td>
	<td  align="center">{*$users[num][idx].num_of_users*}</td>
	<td  align="center"><b>{$totalUsers}</b></td>
	<td  align="center"><b>{$totalOrders}</b></td>
	
</tr>

{else}

<tr>
	<td colspan="4" align="center">{$lng.txt_no_result}</td>
</tr>

{/if}


</table>


</form>

<br />

{/capture}
{include file="dialog.tpl" title='' content=$smarty.capture.dialog extra='width="100%"'}
{/if}
