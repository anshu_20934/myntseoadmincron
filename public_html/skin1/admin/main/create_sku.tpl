{if $accessallowed eq 1 }
	<script type="text/javascript">
	    var brandsData =  {$brands};
	    var articleTypesData = {$articleTypes};
	    var httpLocation = '{$http_location}';
	</script>
	
	<link rel="stylesheet" type="text/css" href="{$http_location}/admin/extjs/resources/css/ext-all.css"/>
	
	<script type="text/javascript" src="{$http_location}/admin/extjs/adapter/ext/ext-base.js"></script>
	
	<script type="text/javascript" src="{$http_location}/admin/extjs/ext-all.js"></script>
	<script type="text/javascript" src="create_sku.js"></script>
	
	<div id="contentPanel"></div>
{else}
	<b>Creating SKU is not allowed. Please visit WMS for the same!</b>
{/if}
