Ext.onReady(function() {
	Ext.QuickTips.init();
	var myMask = new Ext.LoadMask(Ext.getBody(), {msg: "Loading..."});
	Ext.Ajax.on('beforerequest', myMask.show, myMask);
	Ext.Ajax.on('requestcomplete', myMask.hide, myMask);
	Ext.Ajax.on('requestexception', myMask.hide, myMask);
	
	var searchPanel = new Ext.form.FormPanel({
		renderTo : 'contentPanel',
		title : 'Search Panel',
		id : 'formPanel',
		autoHeight : true,
		collapsible : true,
		border : true,
		bodyStyle : 'padding: 5px',
		width : 1000,
		items : [{
			layout : 'table',
			border : false,
			layoutConfig : {columns : 2},
			defaults : {width : 450, border : false, 
				layout : 'form', bodyStyle : 'padding-left : 15px'},
			items : [
			         {
			        	 items : [{
			        		 width: 300,
			        		 xtype : 'textfield',
			        		 id : 'userlogin',
			        		 fieldLabel : 'Customer Login'
			        	 }]
			         },
			         {
			        	 items : [{
			        		 width: 300,
			        		 xtype : 'combo',
			        		 id : 'accountType',
			        		 fieldLabel : 'Account Type',
			        		 store : new Ext.data.ArrayStore({
					        	    fields: ['id', 'name'],
					        	    data : [["activePoints","activePoints"],
					        	            ["inActivePoints","inActivePoints"]]			        	            
					        	}),
					        	mode: 'local',
					        	displayField : 'name',
					        	valueField : 'id',
					        	triggerAction: 'all'
			        	 }]
			         }
			]
		}],
		buttons : [ {
			text : 'Search',
			handler : function() {
				reloadGrid();
			}
		}],
	});
	
	/*var goodwillReasonStore = new Ext.data.JsonStore({
		fields : ['id', 'name'],
		data : goodwillReasonData,
		sortInfo : {field : 'name', direction : 'ASC'}
	});*/	

	function getSearchPanelParams() {
		var form = searchPanel.getForm();
		
		if (form.isValid()) {
			var params = {};
			//params["coupon"] = form.findField("coupon").getValue();
			params["userlogin"] = form.findField("userlogin").getValue();
			params["accountType"] = form.findField("accountType").getValue();
			return params;
		} else {
			return false;
		}
	}
	
	function createNewTransaction() {
		
		var addTxnPanel = new Ext.form.FormPanel({
			id : 'addTxnPanel',
			height: 450,
			border : false,
			bodyStyle : 'padding: 5px',
			items : [{
				layout : 'table',
				border : false,
				layoutConfig : {columns : 1},
				defaults : {border : false, layout : 'form', bodyStyle : 'padding-left : 15px'},
				items: [{
					items: [{
						width: 300,
			        	xtype : 'combo',
			        	emptyText : 'Select Txn Type',
			        	store : new Ext.data.ArrayStore({
			        	    fields: ['id', 'name'],
			        	    data : [["C", "Credit"], ["D", "Debit"]]
			        	}),
			        	mode: 'local',
			        	displayField : 'name',
			        	valueField : 'id',
			        	triggerAction: 'all',
			        	fieldLabel : 'Transaction Type',
			        	name : 'addtxn_type',
			        	id : 'addtxn_type'
			        }]
				},{
					items: [{
						width: 300,
						xtype : 'numberfield',
			        	id : 'addtxn_points',
			        	fieldLabel : 'POints'
					}]
				},{
					
					items:[{
								        		
		        		width: 300,
			        	xtype : 'combo',
			        	emptyText : 'Select Item Type',
			        	store : new Ext.data.ArrayStore({
			        	    fields: ['id', 'name'],
			        	    data : [["ORDER","ORDER"],
			        	            ["RETURN_ID","RETURN_ID"],
			        	            ["ITEM_ID","ITEM_ID"]]
			        	}),
			        	mode: 'local',
			        	displayField : 'name',
			        	valueField : 'id',
			        	triggerAction: 'all',
			        	fieldLabel : 'Type',
			        	name : 'addtxn_itemType',
			        	id : 'addtxn_itemType'		
				
					}]					
				},{
					
					items:[{
						width: 300,
						xtype : 'numberfield',
			        	id : 'addtxn_itemid',
			        	fieldLabel : 'Id'			
					
					}]					
				},{
					
					items:[{
						width: 300,
			        	xtype : 'combo',
			        	emptyText : 'Select Bussiness Process',
			        	store : new Ext.data.ArrayStore({
			        	    fields: ['id', 'name'],
			        	    data : [["POINTS_AWARDED","POINTS_AWARDED"],
			        	            ["POINTS_REDEEMED","POINTS_REDEEMED"],
			        	    		["POINTS_ACTIVATED","POINTS_ACTIVATED"],
			        	    		["GOOD_WILL","GOOD_WILL"],
							["ITEM_CANCELLATION","ITEM_CANCELLATION"]
			        	    		
			        	    ]
			        	}),
			        	mode: 'local',
			        	displayField : 'name',
			        	valueField : 'id',
			        	triggerAction: 'all',
			        	fieldLabel : 'Bussiness Process Type',
			        	name : 'addtxn_bussiness_process',
			        	id : 'addtxn_bussiness_process'			
					
					}]					
				},/*{
	                               items:[{
	                                        width: 300,
        	                                xtype : 'combo',
                	                        emptyText : 'Select Goodwill Reason',
                        	                store : goodwillReasonStore,
                                       		mode: 'local',
	                                        displayField : 'name',
        	                                valueField : 'name',
                	                        triggerAction: 'all',
                        	                fieldLabel : 'Goodwill Reason',
                                	        name : 'goodwillReason',
                                        	id : 'goodwillReason'

                                        }]

				},*/{
					items: [{
						xtype : 'textarea',
			        	id : 'addtxn_description',
			        	fieldLabel : 'Description',
			        	width: 300,
			        	height: 100
					}]
				}]
			}]
		});
		
		
		var window =  new Ext.Window({
			title: 'Add New Transaction',
			id: 'newtxnwindow',
			width: 500,
			height: 450,
			items: addTxnPanel,
			buttons: [{
				 id: 'newtxnadd',
				 text: 'Add',
				 handler: function() {
					var params = getSearchPanelParams();
					var form = addTxnPanel.getForm();
					var type = form.findField("addtxn_type").getValue();
					var points = form.findField("addtxn_points").getValue();
					var description = form.findField("addtxn_description").getValue();
					
					//var accNo = form.findField("addtxn_accno").getValue();
					var bussinessProcess = form.findField("addtxn_bussiness_process").getValue();
					var itemid = form.findField("addtxn_itemid").getValue();
					var itemType = form.findField("addtxn_itemType").getValue();
//					var goodwillReason = form.findField("goodwillReason").getValue();
					
					
					if(type == "" || points == "" || description == "" || bussinessProcess=="") {
						Ext.Msg.minWidth = 300;
						Ext.Msg.alert('Please enter all the values first');
						return false;
					}

					
					if(bussinessProcess == "TECH_ISSUES" && type=="D"){
						Ext.Msg.minWidth = 300;
                                                Ext.Msg.alert('Busines process TECH_ISSUES cannot have Transaction type as Debit');
                                                return false;
					}


					/*if(bussinessProcess=="REFUND" && accNo==""){
						Ext.Msg.minWidth = 300;
						Ext.Msg.alert('Please enter the user account number for refund transaction');
						return false;
					}*/
					/*if(((bussinessProcess=="REFUND"||bussinessProcess=="FRAUD_REVERSAL"||bussinessProcess=="CASHBACK_REVERSAL") && type=="C") ||((bussinessProcess!="REFUND" && bussinessProcess!="FRAUD_REVERSAL" && bussinessProcess!="CASHBACK_REVERSAL") && type=="D"))
					{
						Ext.Msg.minWidth = 300;
						Ext.Msg.alert('Transaction type and bussiness process do not match!');
						return false;
					}*/
					if(itemid == "" || itemType ==""){
						Ext.Msg.minWidth = 300;
						Ext.Msg.alert('Please enter item type and item id');
						return false;
					}
					params["txn_type"] = type;
					params["points"] = points;
					params["description"] = description;
					params["action"] = "addtransaction";
					params["businessProcess"] = bussinessProcess;
					params["itemID"] = itemid;
					params["itemType"] = itemType;
					//params["loggedBy"] = loginUser.toString();
					
					
					
					
					Ext.Ajax.request({
						url : 'loyaltyPointsAccountsEdit.php',
						params : params,
					    success : function(response, action) {
							var jsonObj = Ext.util.JSON.decode(response.responseText);
							if(jsonObj.success) {
								window.destroy();
								reloadGrid();
							} else {
								Ext.Msg.minWidth = 400;
								Ext.Msg.alert('Error', jsonObj.msg);
							}
						}
					});
				 }
			},
			{
				id: 'newtxncancel',
				text: 'Close',
				handler: function() {
					window.destroy();
				}
			}]
		}).show();
		window.center();
	}
	
	function reloadGrid() {
		
		var params = getSearchPanelParams();
		
		if (params == false) {
			Ext.MessageBox.alert('Error', 'Errors in the search form');
			return false;
		}
		params["action"] = "search";
		
		searchResultsStore.baseParams = params;
		searchResultsStore.load();
	}
	
	var txnTypeRenderer = function(value, p, record) {
		if(value == 'C') {
			return 'Credit';
		} else {
			return 'Debit';
		}
	}
	
	var colModel = new Ext.grid.ColumnModel({
		defaults: {
	        sortable: true,
	        editable:true,
	        editor : Ext.form.Field
	    },
	    columns: [
	        new Ext.grid.RowNumberer(),
	        {id : 'modifiedOn', header : "Date", sortable: false, width: 125, dataIndex : 'modifiedOn'},	        
	        //{id : 'coupon', header : "Coupon", sortable: false, width: 125, dataIndex : 'coupon'},
	        {id : 'login', header : "Login", sortable: false, width: 150, dataIndex : 'login'},
	        //{id : 'txn_type', header : "Transaction Type", sortable: false, width: 125, dataIndex : 'txn_type', renderer: txnTypeRenderer},
	        {id : 'creditInflow', header : "Credit Amount", sortable: false, width: 100, dataIndex : 'creditInflow'},
	        {id : 'creditOutflow', header : "Debit Amount", sortable: false, width: 100, dataIndex : 'creditOutflow'},	        
	        {id : 'balance', header : "Balance", sortable: false, width: 100, dataIndex : 'balance'},
	        //{id : 'businesProcess', header : "Business Process", sortable: false, width: 300, dataIndex : 'businesProcess'},
	        //{id : 'itemId', header : "Item ID", sortable: false, width: 100, dataIndex : 'itemId'},
	        //{id : 'itemType', header : "Item Type", sortable: false, width: 100, dataIndex : 'itemType'},
	        {id : 'modifiedBy', header : "Transaction By", sortable: false, width: 125, dataIndex : 'modifiedBy'},
	        {id : 'description', header : "Description", sortable: false, width: 300, dataIndex : 'description'}
  	  	]
	});
		
	var searchResultsStore = new Ext.data.Store({
		url: 'LoyaltyPointsAccounts.php',
		pruneModifiedRecords : true,
		reader: new Ext.data.JsonReader({
	  		root: 'results',
	  	  	totalProperty: 'count',
	  	  	fields : ['modifiedOn', 'login', 'creditInflow','creditOutflow','businesProcess', 'itemId','itemType', 'balance','modifiedBy' ,'description']
		}),
	});

	var pagingBar = new Ext.PagingToolbar({
		pageSize : 30,
	    store: searchResultsStore,
	    displayInfo: true
	});
	
	var searchResults = new Ext.grid.EditorGridPanel({
	    store  : searchResultsStore,
	    cm : colModel,
	    id : 'searchResults-panel',
	    title : 'Search Results',
	    frame : true,
		loadMask : false,
		autoHeight : true,
		bbar : pagingBar,
		stripeRows : true,
		autoScroll : true,
		viewConfig : {
			enableRowBody : true
		},
		tbar:[{
			text : 'Add New Transaction',
			tooltip : 'Add New Transaction',
	   		iconCls : 'add',
			handler : function() {
				var params = getSearchPanelParams();
				if(params['userlogin'] == "") {
					Ext.Msg.minWidth = 400;
					Ext.Msg.alert('Error', "Select user login for which you want to add transaction.");
					return false;
				}
				if(params['accountType'] == "") {
					Ext.Msg.minWidth = 400;
					Ext.Msg.alert('Error', "Select account type for which you want to add transaction.");
					return false;
				}
				createNewTransaction();
			}
		}]
	});
	var mainPanel = new Ext.Panel({
		renderTo : 'contentPanel',
		border : false,
		items : [
		     searchPanel, searchResults
		],
		listeners : {
			'afterrender' : function(comp) {
				if(get_login && get_login != "") {
					searchPanel.getForm().findField("userlogin").setValue(get_login);
				}
				/*if(get_coupon && get_coupon != "") {
					searchPanel.getForm().findField("coupon").setValue(get_coupon);
				}*/
				reloadGrid();
			}
		}
	});
});
