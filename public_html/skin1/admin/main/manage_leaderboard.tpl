<link rel="stylesheet" href="{$SkinDir}/skin1_admin_operations.css" />
<script type="text/javascript">
    var leaderBoardMessages = {$leaderboardmessages};
</script>

{capture name=dialog}
    <form name="ticker-message-form" id="ticker-message-form" width=100% onsubmit="return updateTickerMessage();" method="POST">
        {if $successMessage } <div style="text-align:center; margin: 10 0 20 0px; font-weight: bold; color: green;">{$successMessage}</div> {/if}
        <input type=hidden name="action" id="action" value="updateTickerMessage">
	    <table width=100% border=0 cellspacing=0 cellpadding=0>
	        <tr>
	            <td width=20%>Leader Board:</td>
	            <td width=80%>
				    <select id="leaderboard" name="leaderboard" onchange="getLeaderBoardMessage(this.value);" style="border: 1px solid #CCCCCC; padding: 5px; width: 300px;">
				        <option value="0">--Select Leader Board--</option>
				        {section name=idx loop=$leaderboards}
				            <option value="{$leaderboards[idx].lb_fk_id}">{$leaderboards[idx].name}</option>
				        {/section}
				    </select>
				</td>
	        </tr>
            <tr>
	            <td width=20%>Ticker Message:</td>
	            <td width=80%><textarea id="ticker-message" name="ticker-message" style="width: 400px; height: 75px; padding: 5px;"></textarea></td>
	        </tr>
            <tr>
                <td width=20%>Title:</td>
                <td width=80%><textarea id="title" name="title" style="width: 400px; height: 75px; padding: 5px;"></textarea></td>
            </tr>
            <tr>
                <td width=20%>Banner URL:</td>
                <td width=80%><textarea id="banner-url" name="banner-url" style="width: 400px; height: 75px; padding: 5px;"></textarea></td>
            </tr>
            <tr>
                <td width=20%>Banner HREF:</td>
                <td width=80%><textarea id="banner-href" name="banner-href" style="width: 400px; height: 75px; padding: 5px;"></textarea></td>
            </tr>
	        <tr>
	           <td colspan=2 align=right><input type=submit name="update-ticker-message" id="update-ticker-message" value="Update Message" disabled/></td>
	        </tr>
	    </table>
	</form>
{/capture}
{include file="dialog.tpl" title="Ticker Message" extra='width="100%"' content=$smarty.capture.dialog}
{literal}
<script type="text/javascript">
    function getLeaderBoardMessage(id) {
        if(id != 0) {
            $("#update-ticker-message").attr("disabled", false);
            $("#ticker-message").val(leaderBoardMessages[id]['message']);
            $("#title").val(leaderBoardMessages[id]['title']);
            $("#banner-url").val(leaderBoardMessages[id]['banner_url']);
            $("#banner-href").val(leaderBoardMessages[id]['banner_href']);
        } else {
            $("#update-ticker-message").attr("disabled", true);
            $("#ticker-message").val("");
            $("#title").val("");
            $("#banner-url").val("");
            $("#banner-href").val("");
        }
    }
    
    function updateTickerMessage() {
    
        var leaderBoardId = $("#leaderboard").val();
        if(leaderBoardId == 0) {
            alert("Please select a leader board");
            return false;
        }
         
        var newMessage = $("#ticker-message").val();
        if(!newMessage || newMessage.trim() == "") {
            alert("Please enter a ticker message");
            return false;
        }
        alert(newMessage);
        var overlay = new ProcessingOverlay();
        overlay.show();
        return true;
        
    }
</script>
{/literal}