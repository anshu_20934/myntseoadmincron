{* $Id: featured_products.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
{include file="main/include_js.tpl" src="main/popup_product.js"}
<script type="text/javascript" src="{$http_location}/skin1/js_script/jquery.js">
</script>
{literal}
  <script language="Javascript">
      function confirmDelete()
      {
          var flag = confirm('Are you sure you want to delete?')
          if(!flag)
          {
             return false;
          }
          else
          {
          	 document.productstyleform.mode.value = 'delete';
          	 document.productstyleform.submit();
          	 return true;
          }
      }
     
	function getHTTPObject(){
		if(window.XMLHttpRequest){
			http=new XMLHttpRequest();}
		else if(window.ActiveXObject){
			http=new ActiveXObject("Microsoft.XMLHTTP");}
		return http;
	}

	function updatesearchtable(key_id,fieldArray){		
		//to make ajax on check box selected	
		var fieldvalue = '';
		var qString = '';
		for(i=0;i<fieldArray.length;i++){
			if(typeof document.getElementById(fieldArray[i]+'_'+key_id) != "undefined" && document.getElementById(fieldArray[i]+'_'+key_id) != null){
				fieldvalue = document.getElementById(fieldArray[i]+'_'+key_id).value;
				if(i==fieldArray.length-1){
					qString += fieldArray[i]+'='+encodeURIComponent(fieldvalue);	
				} else {
					qString += fieldArray[i]+'='+encodeURIComponent(fieldvalue)+'&';
				}	
			}
		}			
		
		var http = new getHTTPObject();			
		var params = "keyid="+key_id+"&"+qString;			
		var searchURL = http_loc+"/admin/admin_update_search_seo.php";			
		
		http.open("POST", searchURL, true); 
		http.setRequestHeader('Content-Type','application/x-www-form-urlencoded'); 
		http.onreadystatechange = function(){
			if(http.readyState==4){
				if(http.status==200){
					var result=http.responseText;						
				}
			}    	
		}
		http.send(params);			
	}
	
function checkall(){

if($("#checkallbox").attr("checked"))
	$(".abc").attr("checked","checked");
else
	$(".abc").removeAttr("checked");

}
  </script>
{/literal}
<a name="featured" />

{$lng.txt_featured_products}

<br /><br />
<div class="pagination" style="float:right;margin-bottom:5px;">
<!--pagination-->
	
		{if $totalpages > 1}{$paginator}{/if}
	
</div><!--pagination-->
{capture name=dialog}

<form action="admin_seo_search.php" method="post" name="Addkeywordform" enctype="multipart/form-data">
<div style="font-size:14px;color:red">{$addkeyword_message}</div>
<input type="hidden" name="mode"  value="addnewkeyword"/>
<div>
	<div style="float:left;width:6%;font-weight:bold;"> Add new keyword</div>
	<div style="float:left;">
		<input type="text" value="" name="addNewKeyword" style='width:275px;height:20px;' />
	</div>
	<div style="float:left;margin-left:5px;">
		<input type="submit" name="AddKeyword" value="Add New Keyword" />
	</div>
	<div style="float:none;clear:both;">&nbsp;</div>
</div>
</form>
<form action="admin_seo_search.php" method="post" name="stylesearchform" enctype="multipart/form-data">
<input type="hidden" name="mode"/>
<table cellpadding="3" cellspacing="1" width="100%">
<tr>
	<td align="left" width="20"><b>Keyword</b></td>
	<td align="left" width="50"><input type="text"  style='width:275px;height:20px;' name="keyword" value="{$selectedkeyword}" /></td>
	<td align="left" width="80"><b>Search Type</b></td>
	<td align="left" width="30">
		<select name="searchtype">
			<option value="keyword" {if $searchtype == "keyword"}selected{/if} >keyword</option>
			<option value="keyid" {if $searchtype == "keyid"}selected{/if}>keyid</option>
			
		</select>
	</td>
	<td class="SubmitBox" style="padding-bottom:10px;">
		<input type="submit" value="{$lng.lbl_search|escape}" onclick="javascript: document.stylesearchform.mode.value = 'search'; document.stylesearchform.submit();"/>
	</td>
</tr>
</table>
</form>


<form action="admin_seo_search.php" method="post" name="productstyleform" enctype="multipart/form-data">
<input type="hidden" name="mode"  />
<input type="hidden" name="updatemode" value="update"/>

<table cellpadding="3" cellspacing="1" width="100%">

<tr class="TableHead">
	<td width="5%">ID<!--input type='checkbox' onchange='javascript:checkall();' id='checkallbox'/--></td>
	<td width="20%">Keyword</td>
    <td width="20%">Page Title</td>
	<td width="20%">Page H1</td>
    <td width="20%">Meta Dsrciption</td>
    <td width="20%">Meta Keyword</td>
	<td width="20%">Search URL</td>
	<td width="5%" align="center"><a href='{$sort_count_of_products}'>Count of products</a></td>
	<td width="5%" align="center"><a href='{$sort_count_of_searches}'>Count of searches</a></td>
	<td width="40%" align="center">Description</td>
	<td width="20%" align="center">Banner</td>
	<td width="30%" align="center">HTML Banner</td>
	<td width="10%" align="center">Action</td>
</tr>

{if $products}

{section name=prod_num loop=$products}
{if $products[prod_num].count_of_products < 3}
<tr {cycle values=", class='TableSubHead'"} style='background-color:red;'>
{else}
<tr {cycle values=", class='TableSubHead'"}>
{/if}
        <input type="hidden" name="posted_data[{$products[prod_num].key_id}][key_id]" value={$products[prod_num].key_id} />
	<td>{$products[prod_num].key_id}<br><!--input class='abc' type="checkbox" name="posted_data[{$products[prod_num].key_id}][key_id_checkbox]" id="{$products[prod_num].key_id}"/--></td>
	<td align="center"><b>{$products[prod_num].keyword}</b></td>
	<td align="center">
		<textarea style='width:400px;height:100px;' id="page_title_{$products[prod_num].key_id}" name="posted_data[{$products[prod_num].key_id}][page_title]" > {$products[prod_num].page_title}</textarea>
	</td>
	<td align="center">
		<textarea style='width:100px;height:100px;' id="page_h1_{$products[prod_num].key_id}" name="posted_data[{$products[prod_num].key_id}][page_h1]"> {$products[prod_num].page_h1}</textarea>
	</td>
	<td align="center">
		<textarea style='width:400px;height:100px;' id="meta_description_{$products[prod_num].key_id}" name="posted_data[{$products[prod_num].key_id}][meta_description]"> {$products[prod_num].meta_description}</textarea>
	</td>
	<td align="center">
		<textarea style='width:400px;height:100px;' id="meta_keyword_{$products[prod_num].key_id}" name="posted_data[{$products[prod_num].key_id}][meta_keyword]"> {$products[prod_num].meta_keyword}</textarea>
	</td>
	<td align="center"><b><a href='http://www.myntra.com/{$products[prod_num].keyword|replace:" ":"-"}/' target='_blank'>http://www.myntra.com/{$products[prod_num].keyword|replace:" ":"-"}/</a></b></td>
	<td align="center"><b>{$products[prod_num].count_of_products}</b></td>
	<td align="center"><b>{$products[prod_num].count_of_searches}</b></td>
	<td align="center">
		<textarea style='width:400px;height:100px;' id="description_{$products[prod_num].key_id}" name="posted_data[{$products[prod_num].key_id}][description]">{$products[prod_num].description}</textarea>
	</td>
	<td align="center">
	    <img src="../{$products[prod_num].top_banner}" alt="" height="50px" width="50px"><br>
		<input type="file" name="{$products[prod_num].key_id}_banner" id="banner_{$products[prod_num].key_id}">
	</td>
	<td align="center">
		<textarea style='width:400px;height:100px;' id="page_promotion_{$products[prod_num].key_id}" name="posted_data[{$products[prod_num].key_id}][page_promotion]">{$products[prod_num].page_promotion}</textarea>
	</td>
	<td align="center">	    
		<input type="button" name="update" value="Update" onclick="updatesearchtable({$products[prod_num].key_id},Array('page_title','page_h1','meta_description','meta_keyword','is_valid','description','page_promotion'));">
	</td>

</tr>

{/section}

<td colspan="5"><br /><br /></td>


{else}

<tr>
<td colspan="5" align="center">{$lng.txt_no_featured_products}</td>
</tr>

{/if}


</table>
</form>

{/capture}

{include file="dialog.tpl" title=$lng.lbl_productstyle_heading content=$smarty.capture.dialog extra='width="100%"'}
<div class="pagination" style="float:right;margin-bottom:5px;">
<!--pagination-->
	
		{if $totalpages > 1}{$paginator}{/if}
	
</div><!--pagination-->
<br /><br />


