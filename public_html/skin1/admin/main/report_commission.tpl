{* $Id: statistics.tpl,v 1.24.2.1 2006/06/16 10:47:41 max Exp $ *}
{include file="main/include_js.tpl" src="main/calendar2.js"}
{capture name=dialog}

<form action="" method="post" name="commreportform">
<input type="hidden" name="mode" value="search"  />

<table cellpadding="1" cellspacing="1">


<tr>
	<th align="right">{$lng.lbl_reporttype}:</th>
	<td>
	     <select name='report_type'>
	      <option value="week"  { if $reportType == "week"} selected {/if} >Weekly</option>
              <option value="month" { if $reportType == "month"} selected {/if} >Monthly</option> 
	     
	    </select>
	</td>
	<th align="right">{$lng.lbl_producttype}:</th>
	<td>
	    <select name='product_type'>
	       <option value="all"  { if $prdType == "all"} selected {/if} >All</option>
               
	       {section name=num loop=$prd_types}
		    <option value="{$prd_types[num].id}" { if $prdType == {$prd_types[num].id} } selected {/if} >{$prd_types[num].name}</option>
	       {/section}
           </select>
	</td>
</tr>


<tr>
	<th align="right">{$lng.lbl_report_from}:</th>
	<td><input type="text" name='Startdate' value="{$SDate}"/><a href="javascript:cal4.popup();" ><img src="{$cdn_base}/images/cal.gif" width="16" height="16" border="0" alt="Click Here to Pick up the date"></a></td>
	<th align="right">{$lng.lbl_report_to}:</th>
	<td><input type="text" name='Enddate'  value="{$EDate}" /><a href="javascript:cal5.popup();" class=""><img src="{$cdn_base}/images/cal.gif" width="16" height="16" border="0" alt="Click Here to Pick up the date"></a></td>
</tr>

<tr>
	<td colspan="4"><input type="submit" value="{$lng.lbl_submit|escape}" /></td>
</tr>
<script language="javascript">
      var cal4 = new calendar2(document.forms['commreportform'].elements['Startdate']);
      cal4.year_scroll = true;
      cal4.time_comp = false;

       var cal5 = new calendar2(document.forms['commreportform'].elements['Enddate']);
      cal5.year_scroll = true;
      cal5.time_comp = false;


</script>

</table>
</form>

{/capture}
{include file="dialog.tpl" title='' content=$smarty.capture.dialog extra='width="100%"'}

<br />
<br />
{if $mode neq  ""}

{capture name=dialog}
<table cellpadding="1" cellspacing="1" width="100%">

<tr class="TableHead">
	<td >{$lng.lbl_duration}</td>
	<td >{$lng.lbl_producttype}</td>
	<td >{$lng.lbl_products}</td>
	<td >{$lng.lbl_pix_designs}</td>
	<td >{$lng.lbl_seller_designs}</td>
	<td >{$lng.lbl_commission}</td>
	
	
</tr>


{if $commission}
  {section name=num loop=$commission}

          
     {section name=idx loop=$commission[num]}
    

       <tr{cycle values=', class="TableSubHead"'}>
	
        <td align="center">{$commission[num][idx].duration1}-{$commission[num][idx].duration2}</td>
	<td align="center">{$commission[num][idx].name}</td>
	<td align="center">{$commission[num][idx].num_of_products}</td>
	<td align="center">{if $commission1[num][idx].no_of_products eq 0}0{/if}</td>
	{if $commission1[num][idx].no_of_products eq 0}
		<td align="center">{$commission[num][idx].num_of_products}</td>
         {else}
	       <td align="center">{$commission[num][idx].no_of_products-$commission1[num][idx].no_of_products}</td>
         {/if} 
	  <td align="center">{$commission2[num][idx].camt}</td>
     </tr>


  {/section}
    
{/section}
  <tr>
	
        <td align="center" colspan="6" >&nbsp;</td>
	
  </tr>
  <tr>
	
        <td align="center" colspan="6" ><b><hr/></b></td>
	
  </tr>
  <tr>
	
        <td align="center" colspan="5" ><b>Total</b></td>
	<td align="center" colspan="1"><b>{$total}</b></td>
 </tr>

{else}

<tr>
	<td colspan="4" align="center">{$lng.txt_no_result}</td>
</tr>

{/if}


</table>


</form>

<br />

{/capture}
{include file="dialog.tpl" title='' content=$smarty.capture.dialog extra='width="100%"'}
{/if}