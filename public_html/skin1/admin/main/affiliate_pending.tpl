{literal}
<script Language="JavaScript" Type="text/javascript" src="../skin1/main/validation_inv.js"></script>
<script Language="JavaScript" Type="text/javascript">
function updateDesigner(did)
		{
		//alert(did);
		document.designerreportform.designid.value =did;
		document.designerreportform.mode_report.value ='updatePending'; 
		document.designerreportform.submit();
		}
	
		</script>
		{/literal}
{capture name=dialog}
<form action="adminaffiliates.php" method="post" name="designerreportform" >
<input type="hidden" name="mode_report" />
<input type="hidden" name="designid" />

<table width="100%" height="25" border="1" cellpadding="0" cellspacing="0">
				<tr bgcolor="#e9edf4">
					<th style="font-size:14px;width:150px;">Company Name</th>
					<th style="font-size:14px;">Contact Email</th>
					<th style="font-size:14px;">Total Commission</th>
					<th style="font-size:14px;">Commission Paid</th>
					<th style="font-size:14px;">Pending Commission</th>
					<th style="font-size:14px;">Mailing Address</th>
					
				</tr>
				{foreach from=$designers item=designer}
				<tr bgcolor="{cycle values="#d0d8e8,#e9edf4"}">					
					<td style="font-size:12px;width:150px;">{$designer.name}</td>
					<td style="font-size:12px;"><a href="#"  onClick="updateDesigner({$designer.id})">{$designer.email}</td>
					<td style="font-size:12px;">{$designer.total}</td>
					<td style="font-size:12px;">{$designer.paid}</td>
					<td style="font-size:12px;">{$designer.pending}</td>	
					<td style="font-size:12px;">{$designer.address}</td>
						
				</tr>				
                {/foreach}    
			
			<tr>
</tr>
</table>
</form>

{/capture}
{include file="dialog.tpl" title="Pending Commissions Report" content=$smarty.capture.dialog extra='width="100%"'}
<br/>
<br/>


