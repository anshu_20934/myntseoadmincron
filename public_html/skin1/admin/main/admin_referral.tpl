{include file="main/include_js.tpl" src="main/popup_category.js"}

{capture name=dialog}

{if $referral}
{include file="main/check_all_row.tpl" style="line-height: 170%;" form="referrallist" prefix="posted_data.+to_delete"}
{/if}

<form action="{$formaction}" method="post" name="referrallist">
<input type="hidden" name="mode" id="mode"  />
<table cellpadding="3" cellspacing="1" width="100%">

<tr class="TableHead">
	<td width="5%">&nbsp;</td>
	<td width="5%">Id</td>
	<td width="30%" align="center">Name</td>
	<td width="25%" align="center">Start date</td>
	<td width="25%" align="center">End date</td>
	<td width="25%" align="center">Percentage</td>
	<td width="10%" align="center">Status</td>
</tr>

{if $referral}

{section name=ref loop=$referral}

<tr{cycle values=", class='TableSubHead'"}>
          
	<td><input type="checkbox" name="posted_data[{$referral[ref].id}][to_delete]" /></td>
	<td><b><a href="admin_addreferral.php?id={$referral[ref].id}&edit=yes">{$referral[ref].id}</a></b></td>
	<td align="center"><a href="admin_addreferral.php?id={$referral[ref].id}&edit=yes">{$referral[ref].refferal_name}</a></td>
	<td align="center">{$referral[ref].startdate}</td>
	<td align="center">{$referral[ref].enddate}</td>
	<td align="center">{$referral[ref].refferal_percent}</td>
	<td align="center">
	<select name="posted_data[{$referral[ref].id}][status]" id="status" >
		<option value="1" {if $referral[ref].status eq "1"} selected="selected"{/if}>Yes</option>
		<option value="0" {if $referral[ref].status eq "0"} selected="selected"{/if}>No</option>
	</select>
	</td>

</tr>

{/section}

<tr>
	<td colspan="4" class="SubmitBox">

	<input type="button" value="{$lng.lbl_upd_selected|escape}"  onclick="javascript: document.referrallist.mode.value = 'update'; document.referrallist.submit();"/>
	<input type="button" value="{$lng.lbl_delete_selected|escape}" onclick="javascript: document.referrallist.mode.value = 'delete'; document.referrallist.submit();" />
	</td>
</tr>

{else}

<tr>
<td colspan="4" align="center">No record available</td>
</tr>

{/if}
<tr>
<td colspan="4"><br /><br />{include file="main/subheader.tpl" title=""}</td>
</tr>
<tr>
	<td colspan="4" class="SubmitBox">

 	<input type="button" value="{$lng.lbl_add_new_|escape}"
 	onclick="javascript: window.location='admin_addreferral.php'"/>
	</td>
</tr>



</table>
</form>


{/capture}
{include file="dialog.tpl" title="Outer shop" content=$smarty.capture.dialog extra='width="100%"'}