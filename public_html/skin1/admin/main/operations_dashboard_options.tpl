{* $Id: operations_dashboard_options.tpl, v1.00.0.0 2008/19/09 11:29 max Exp $ *}
{include file="main/include_js.tpl" src="main/popup_product.js"}

{capture name=dialog}
<!--<font size="2" color = "#000000">-->
<div class="OPS_PARENT_DIV">
		<div class="OPS_FIRST_CHILD_DIV" style="margin:2px;">
			<strong>Total Outstanding Orders :</strong>
<br/>
<br/>
			<div class="OPS_URI_DIV"><a href="{$http_location}/admin/orders_search.php?mode=search&order_status[]=Q&order_status[]=WP&order_status[]=OH">Total Outstanding: {$total_outstanding_orders}</a></div>
<br/>
			<div class="OPS_URI_DIV"><a href="{$http_location}/admin/late_order_report.php?type=lateorders">Total Outstanding Missed Promise : {$late_count}</a></a></div>
<br/>
            <div class="OPS_URI_DIV color="red">Total Outstanding Orders Within Promise : {$total_outstanding_orders-$late_count}</a></div>
<br/>
		


	</div>
<div class="OPS_FIRST_CHILD_DIV" style="margin:2px;">
			<div class="OPS_URI_DIV"><strong>Outstanding Order Breakup :</strong></div><br/>

			<div class="OPS_URI_DIV"><a href="{$http_location}/admin/orders_search.php?mode=search&order_status[]=Q">Queued : {$state_count.Q}</a></div>
<br/>
            <div class="OPS_URI_DIV"><a href="{$http_location}/admin/orders_search.php?mode=search&order_status[]=WP">Work In Progress : {$state_count.WP}</a></div>
<br/>
            <div class="OPS_URI_DIV"><a href="{$http_location}/admin/ready_to_ship_report.php">Ready To Ship : {$rts_count}</a></div>
<br/>
			<div class="OPS_URI_DIV"><a href="{$http_location}/admin/orders_search.php?mode=search&order_status[]=OH">On-Hold : {$state_count.OH}</a></div>
<br/>
</div>

		<div class="OPS_FIRST_CHILD_DIV" style="margin:2px;">
			<div class="OPS_URI_DIV"><strong>Queued Order Breakup :</strong></div>
<br/>
			<div class="OPS_URI_DIV"><a href="{$http_location}/admin/orders_search.php?mode=search&order_status[]=Q&order_min_queued_time=0&order_max_queued_time=86400">Orders-Pending &lt;24 hours : {$orders_pending_counts.lt_24}</a></div>
<br/>
			<div class="OPS_URI_DIV"><a href="{$http_location}/admin/orders_search.php?mode=search&order_status[]=Q&order_min_queued_time=86400&order_max_queued_time=172800">Orders-Pending for 24-48 hours : {$orders_pending_counts.gt_24_lt_48}</a></div>
<br/>
			<div class="OPS_URI_DIV"><a href="{$http_location}/admin/orders_search.php?mode=search&order_status[]=Q&order_min_queued_time=172800&order_max_queued_time=259200">Orders-Pending for 48-72 hours : {$orders_pending_counts.gt_48_lt_72}</a></div>
<br/>
			<div class="OPS_URI_DIV"><a href="{$http_location}/admin/orders_search.php?mode=search&order_status[]=Q&order_min_queued_time=259200">Orders-Pending &gt;72 hours : {$orders_pending_counts.gt_72}</a></div>
<br/>
<br/>
</div>

<div class="OPS_FIRST_CHILD_DIV" style="margin:2px;">
		<div class="OPS_URI_DIV"><strong>Reports :</strong></div>
<br>
            <div class="OPS_URI_DIV"><strong><a href="{$http_location}/reports/completed_order_report.php?">Shipped Order Report</a></strong></div>
<br>			
			<div class="OPS_URI_DIV"><font color="red"><strong><a href="{$http_location}/admin/late_order_report.php?type=lateorders">OUTSTANDING MISSED PROMISE</a></strong></font></a></div>
<br/>
</div>
<!--</font>-->


<div class="OPS_PARENT_DIV">
		<div class="OPS_FIRST_CHILD_DIV" style="margin:2px;">
			<strong>Items :</strong>
<br/>
<br/>
			<div class="OPS_URI_DIV"><a href="{$http_location}/admin/item_assignment.php?mode=search&item_status[]=N&item_status[]=A&item_status[]=F&item_status[]=H&item_status[]=S&item_status[]=UA">Total Outstanding Items : {$items_status_counts.PENDING}</a></div>

<br/>
			<div class="OPS_URI_DIV"><a href="{$http_location}/admin/item_assignment.php?mode=search&item_status[]=UA">Items-Status:Unassigned : {$items_status_counts.UA}</a></div>
<br/>
			<div class="OPS_URI_DIV"><a href="{$http_loaction}/admin/item_assignment.php?mode=search&item_status[]=A">Items-Status:Assigned : {$items_status_counts.A}</a></div>
<br/>
			<div class="OPS_URI_DIV"><a href="{$http_location}/admin/item_assignment.php?mode=search&item_status[]=S">Items-Status:Sent Outside : {$items_status_counts.S}</a></div>
<br/>
			<div class="OPS_URI_DIV"><a href="{$http_location}/admin/item_assignment.php?mode=search&item_status[]=H">Items-Status:On Hold : {$items_status_counts.H}</a></div>
<br/>
			<div class="OPS_URI_DIV"><a href="{$http_location}/admin/item_assignment.php?mode=search&item_status[]=OD">Items-Status:Ops Done : {$items_status_counts.OD}</a></div>
<br/>
	        <div class="OPS_URI_DIV"><a href="{$http_location}/admin/item_assignment.php?mode=search&item_status[]=QD">Items-Status:QA Done : {$items_status_counts.QD}</a></div>
<br/>
	        <div class="OPS_URI_DIV"><a href="{$http_location}/admin/item_assignment.php?mode=search&item_status[]=D">Items-Status:Done : {$items_status_counts.D}</a></div>

		</div>
		<div class="OPS_FIRST_CHILD_DIV" style="margin:2px;">
			<strong>Assignees :</strong>
<br/>
			{foreach from=$assignees item=ass_value key=ass_key}
<br/>

				<div class="OPS_URI_DIV"><a href="{$http_location}/admin/item_assignment.php?mode=search&item_status[]=A&order_status[]=WP&item_assignee[]={$ass_value.id}">Items-Assignee:{$ass_value.name} : {$ass_value.assigned_count}</a></div>
			{/foreach}
		</div>
		<div class="OPS_FIRST_CHILD_DIV" style="margin:2px;">
		<strong>	Location :</strong>
<br/>
			{foreach from=$locations item=loc_value key=ass_key}
<br/>
				<div class="OPS_URI_DIV"><a href="{$http_location}/admin/item_assignment.php?mode=search&item_status[]=S&order_status[]=WP&item_location[]={$loc_value.id}">Items-Location:{$loc_value.location_name} : {$loc_value.location_count}</a></div>
			{/foreach}
		</div>
	</div>
{/capture}
{include file="dialog.tpl" title="Operations Dashboard" content=$smarty.capture.dialog extra='width="100%"'}
<br/><br/>
