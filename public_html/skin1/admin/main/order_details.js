$(function() {

    $('#pptocod_form').submit(function() {
        var overlay = new ProcessingOverlay();
        overlay.show();
        return true;
    });

    $(".error_queue_order").click(function() {
        $(this).attr('disabled', true);
        var overlay = new ProcessingOverlay();
        overlay.show();
        var mode = $(this).attr("id");
        $.ajax({
            type: "POST",
            url: "/admin/order.php?orderid=" + orderId,
            data: "mode=" + mode + "&orderid=" + orderId,
            success: function(response) {
                response = $.parseJSON(response);
                overlay.hide();
                if (response[0] == 'Success') {
                    alert(response[1]);
                    $(this).attr('disabled', true);
                    window.location.reload();
                } else {
                    alert(response[1]);
                    $("#status").removeAttr('disabled');
                    $(this).attr('disabled', true);
                }
            }
        });
    });

    $("#cancel-items-btn").click(function() {
        //var unselecteditems = $('input:checkbox[name="item"]:not(:checked)').size();
        /*if (unselecteditems == 0) {
			alert('You have selected all items. Please cancel order itself.');
		} else {*/
        var itemids = '';
        $('input:checkbox[name="item"]:checked').each(function() {
            if (itemids != '') {
                itemids += ','
            }
            itemids += $(this).val();
        });
        if (itemids != '') {
            window.open("/admin/cancelitems.php?orderid=" + orderId + "&itemids=" + itemids, "Cancel Items", "toolbar=no,status=no,scrollbars=yes,resizable=no,menubar=no,location=no,direction=no,width=" + (screen.availWidth - 100) + ",height=" + (screen.availHeight - 100));
        } else {
            alert('Please select an item to cancel');
        }
        //}
    });

    $("#split-order-btn").click(function() {
        var itemids = '';
        $('input:checkbox[name="item"]:checked').each(function() {
            if (itemids != '') {
                itemids += ','
            }
            itemids += $(this).val();
        });
        if (itemids != '') {
            window.open("/admin/split_order.php?orderid=" + orderId + "&itemids=" + itemids, "Split Order", "toolbar=no,status=no,scrollbars=yes,resizable=no,menubar=no,location=no,direction=no,width=" + (screen.availWidth - 100) + ",height=" + (screen.availHeight - 100));
        } else {
            alert('Please select an item to split the order');
        }
    });

    $("#return-items-btn").click(function() {
        var itemid = '';
        var count = 0;
        $('input:checkbox[name="item"]:checked').each(function() {
            if (itemid != '') {
                itemid += ','
            }
            itemid += $(this).val();
            count++;
        });
        if (count > 0) {
            if (count == 1) {
                window.open("/admin/returns/return_item.php?orderid=" + orderId + "&itemid=" + itemid, "Return Item", "toolbar=no,status=no,scrollbars=yes,resizable=no,menubar=no,location=no,direction=no,width=" + (screen.availWidth - 100) + ",height=" + (screen.availHeight - 100));
            } else
                alert('Only a single item can be returned at one time');
        } else {
            alert('Please select an item to return from the order');
        }
    });

    $("#exchange-items-btn").click(function() {
        var itemid = '';
        var count = 0;
        var pickup_excluded = false;
        $('input:checkbox[name="item"]:checked').each(function() {
            if (itemid != '') {
                itemid += ','
            }
            itemid += $(this).val();
            count++;
            pickup_excluded = $(this).data('pickup-excluded') == '1';

        });
        if (count > 0) {
            if (count == 1) {
                //pickup changes
                if (!pickup_excluded) {
                    window.open("/admin/returns/exchange_item.php?orderid=" + orderId + "&itemid=" + itemid, "Exchange Item", "toolbar=no,status=no,scrollbars=yes,resizable=no,menubar=no,location=no,direction=no,width=" + (screen.availWidth - 100) + ",height=" + (screen.availHeight - 100));
                } else {
                    alert('This item is not eligible for exchange.');
                }
            } else
                alert('Only a single item can be exchanged at one time');
        } else {
            alert('Please select an item to exchange from the order');
        }
    });

    $("#assign-wh-btn").click(function() {
        var itemids = '';
        $('input:checkbox[name="item"]:checked').each(function() {
            if (itemids != '') {
                itemids += ','
            }
            itemids += $(this).val();
        });
        if (itemids != '') {
            window.open("/admin/reassign_warehouse.php?orderid=" + orderId + "&itemids=" + itemids + "&current_warehouseid=" + $("#warehouseid").val(), "Reassign Warehouse", "toolbar=no,status=no,scrollbars=yes,resizable=no,menubar=no,location=no,direction=no");
        } else {
            alert('Please select an item to move to different warehouse');
        }
    });

    $("#price-ovrrd-btn").click(function() {
        var itemid = '';
        $('input:checkbox[name="item"]:checked').each(function() {
            if (itemid != '') {
                itemid += ','
            }
            itemid += $(this).val();
        });
        if (itemid != '') {
            window.open("admin/price_override.php?orderid=" + orderId + "&itemid=" + itemid, "Price Override", "toolbar=no,status=no,scrollbars=yes,resizable=no,menubar=no,location=no,direction=no");
        } else {
            alert('Please select an item to override the price');
        }
    });

    $("#cancel-items-form").submit(function() {
        $("#cancel-items-yes").attr('disabled', 'disabled');
        $("#msgBar").html("");
        var errors = "";
        if ($("#cancellation_type").val() == "") {
            errors += "Please select Cancellation Type<br/>";
        }
        if ($("#cancellation_reason").val() == "") {
            errors += "Please select Cancellation Reason<br/>";
        }
        if ($("#cancel_comment").val() == "") {
            errors += "Please add a remark for cancelling items<br/>";
        }
        if (errors != "") {
            $("#msgBar").html(errors);
            $("#cancel-items-yes").attr('disabled', '');
            return false;
        }
        var overlay = new ProcessingOverlay();
        overlay.show();
        return true;
    });

    $("#split-order-form").submit(function() {
        $("#msgBar").html("");
        var errors = "";
        if ($("#split_reason").val() == "0") {
            errors += "Please select a reason for split<br/>";
        }
        if ($("#split_comment").val() == "") {
            errors += "Please add a remark for splitting the order<br/>";
        }
        if (errors != "") {
            $("#msgBar").html(errors);
            return false;
        }
        var overlay = new ProcessingOverlay();
        overlay.show();
        return true;
    });



    $(".priceOverRideSubmit").click(function() {
        $("#msgBar").html("");
        var errors = "";
        var itemid = $(this).attr("itemid");
        var price = $("#newPrice_" + itemid).attr('price');
        var newPrice = $("#newPrice_" + itemid).val();
        var overrideComment = $("#overrideComment_" + itemid).val();

        if (newPrice == "") {
            errors += "Please enter new price for the item<br/>";
        }
        if (newPrice < 0 || isNaN(newPrice)) {
            errors += "Invalid value for the price field<br/>";
        }
        if (overrideComment == "") {
            errors += "Please add a remark for overriding the price<br/>";
        }
        if (errors != "") {
            $("#msgBar").html(errors);
            return false;
        } else {
            if (parseFloat(newPrice) >= parseFloat(price)) {
                errors += "New price cannot be greater than or equal to the existing price tag<br/>";
                alert("New price cannot be greater than or equal to the existing price tag");
            } else if (confirm('Are you sure you want to change the price from  ' + price + ' to ' + newPrice)) {
                var overlay = new ProcessingOverlay();
                overlay.show();

                $.ajax({
                    type: "POST",
                    url: "/admin/price_override.php?orderid=" + orderId,
                    data: "action=override&itemid=" + itemid + "&newPrice=" + newPrice + "&overrideComment=" + overrideComment,
                    success: function(response) {
                        var response = $.parseJSON(response);
                        var html = '';
                        if (response['status'] == 'SUCCESS') {
                            alert(response['successMsg']);
                        } else {
                            alert(response['errorMsg']);
                        }
                        $("#newPrice_" + itemid).attr('disabled', 'true');
                        $("#overrideComment_" + itemid).attr('disabled', 'true');
                        $("#price-override_" + itemid).attr('disabled', 'true');
                        overlay.hide();
                    },
                    error: function(response) {
                        overlay.hide();
                    }
                });
            } else
                return false;
        }
    });


    $(".item_qty_select").change(function() {
        var itemidinfo = $(this).attr('id').split("_");
        var itemid = itemidinfo[2];
        var quantityOrdered = $(this).val();
        if (quantityOrdered == "0") {
            $("#item_whinfo_" + itemid).html('None');
            $("#confirm-btn-" + itemid).attr("disabled", true);
            return;
        }
        var overlay = new ProcessingOverlay();
        overlay.show();
        // for this itemid and amount.. check if this sku can be processed from one warehouse....
        $.ajax({
            type: "POST",
            url: "",
            data: "action=get_wh_info&itemid=" + itemid + "&qtySelected=" + quantityOrdered + "&current_warehouseid=" + $("#warehouseid").val(),
            success: function(response) {
                var response = $.parseJSON(response);
                var html = '';
                if (response[0] == 'SUCCESS') {
                    var valid_warehouses = response[1];
                    html = '<select id="item_assign_to_' + itemid + '">';
                    for (var id in valid_warehouses) {
                        html += '<option value="' + id + '">' + valid_warehouses[id] + '</option>';
                    }
                    html += '</select>';
                    $("#confirm-btn-" + itemid).removeAttr("disabled");
                } else {
                    if (response[1] && response[1] != "")
                        html = response[1];
                    else
                        html = 'None';
                }
                $("#item_whinfo_" + itemid).html(html);
                overlay.hide();
            },
            error: function(response) {
                overlay.hide();
            }
        });
    });
});

function reassignConfirm(itemid) {
    var itemQty = $("#item_quantity_" + itemid).val();
    var warehouseId = $("#item_assign_to_" + itemid).val();
    var overlay = new ProcessingOverlay();
    overlay.show();
    $.ajax({
        type: "POST",
        url: "",
        data: "action=confirm&itemid=" + itemid + "&qty=" + itemQty + "&warehouseid=" + warehouseId,
        success: function(response) {
            var response = $.parseJSON(response);
            if (response[0] == 'SUCCESS') {
                CommonUtils.showSuccessMsg(response[1], itemid);
                $("#item_quantity_" + itemid).attr("disabled", true);
                $("#item_assign_to_" + itemid).attr("disabled", true);
                $("#confirm-btn-" + itemid).attr("disabled", true);
            } else {
                CommonUtils.showErrorMsg(response[1], itemid);
            }
            overlay.hide();
        },
        error: function(response) {
            overlay.hide();
        }
    });
}


function OrderDetails() {}

OrderDetails.toggleSection = function(element) {
    if ($(element).attr("class") == "divClose") {
        $(element).attr("class", "divOpen");
        $('#item-details-1').slideDown('slow');
    } else {
        $(element).attr("class", "divClose");
        $('#item-details-1').slideUp('slow');
    }
}