{* $Id: featured_products.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
{include file="main/include_js.tpl" src="main/popup_product.js"}
{capture name=dialog}

{if $action eq "upload"}
           
	<form action="" method="post" name="bulkorderaction" enctype="multipart/form-data" >
		Upload the csv file : <input type="file" name="orderdetail" >{if $message}<font color='red'>{$message}</font>{/if}
		<input type='submit'  name='uploadxls'  value="Upload">
		<input type='hidden' name='action' value='verify'>

	</form>
{else}
      <form name='frmconfirm'  action=""  method="post"  >
      <input type='hidden' name='action' value='confirmorder'>
      <input type='hidden' name='filetoread' value='{$uploadorderFile}'>
         <table cellpadding="2" cellspacing="1" width="100%">
	      <tr class="TableHead">
		<td >employee_id</td>
		<td>name</td>
		<td>dept</td>
		<td>role</td>
		<td>status</td>
		<td>manager_id</td>
		<td>address</td>
		<td>city</td>
		<td>state</td>
		<td>zipcode</td>
		<td>country</td>
        <td>email</td>
        <td>mobile</td>
        <td>phone</td>
        <td>personal account</td>
        <td>handshake account</td>
        <td>prodigy account</td>
	     </tr>
              {section name=idx loop=$orderListDisplay}
		     <tr{cycle values=", class='TableSubHead'"}>
			<td align="center">{$orderListDisplay[idx][0]}</td>
			<td align="left" >{$orderListDisplay[idx][1]}</td>
			<td align="left" >{$orderListDisplay[idx][2]}</td>
			<td align="left" >{$orderListDisplay[idx][3]}</td>
			<td align="left" >{$orderListDisplay[idx][4]}</td>
			<td align="left" >{$orderListDisplay[idx][5]}</td>
			<td align="left" >{$orderListDisplay[idx][6]}</td>
			<td align="left" >{$orderListDisplay[idx][7]}</td>
			<td align="left" >{$orderListDisplay[idx][8]}</td>
			<td align="left" >{$orderListDisplay[idx][9]}</td>
			<td align="left" >{$orderListDisplay[idx][10]}</td>
			<td align="left" >{$orderListDisplay[idx][11]}</td>
			<td align="left" >{$orderListDisplay[idx][12]}</td>
			<td align="left" >{$orderListDisplay[idx][13]}</td>
            <td align="left" >{$orderListDisplay[idx][14]}</td>
            <td align="left" >{$orderListDisplay[idx][15]}</td>
            <td align="left" >{$orderListDisplay[idx][16]}</td>
		    </tr>
               {/section}
             <tr class="TableHead">
		<td >&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
        <td>&nbsp;</td>
		<td>&nbsp;</td>
        <td>&nbsp;</td>
	     </tr>
	     <tr class="TableSubHead">
		<td >&nbsp;</td>
		<td><input type='submit' name='confirm'  value='confirm' /></td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
	     </tr>

	 </table>
	 </form>
{/if}

{/capture}
{include file="dialog.tpl" title="RR Employee Create CSV Upload Form" content=$smarty.capture.dialog extra='width="100%"'}


