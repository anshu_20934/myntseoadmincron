{* $Id: partner_level_commissions.tpl,v 1.9.2.2 2006/07/11 08:39:26 svowl Exp $ *}
{include file="page_title.tpl" title=$lng.lbl_multi_tier_affiliates}
{$lng.txt_partnership_commissions_note}<br /><br />

<!-- IN THIS SECTION -->

{include file="dialog_tools.tpl"}

<!-- IN THIS SECTION -->
<br />

{capture name=dialog}
<form action="partner_level_commissions.php" method="post">
<input type="hidden" name="mode" value="edit" />

<table cellspacing="2" cellpadding="2">
<tr class="TableHead">
	<td><b>{$lng.lbl_level}</b></td>
	<td><b>{$lng.lbl_commission}</b></td>
</tr>
{foreach from=$levels item=v key=k}
<tr>
    <td>{$k}</td>
    <td><input size="6" type="text" name="level[{$k}]" value="{$v.commission|formatprice|default:$zero}" />%</td>
</tr>
{/foreach}
</table>
<br />
<input type="submit" value="{$lng.lbl_update|strip_tags:false|escape}" />

</form>
{/capture}
{include file="dialog.tpl" content=$smarty.capture.dialog title=$lng.lbl_multi_tier_affiliates extra='width="100%"'} 

