<script type="text/javascript">
    var orderId = "{$orderid}";
    var within30days = '{$within30days}';
    var warehouseid = {$warehouseid};
</script>
{if $action eq 'confirm_exchange' }
	<div class="section-heading prepend">Exchange Item for Order: #{$orderid}</div>
	<div class="blue-divider"></div>
	<div class="divider">&nbsp;</div>
	{if $exchangeable eq 0 }
		{if $exchange_orderid }
			<div class="notification p5"><img src="/skin1/mkimages/unchecked.gif"> &nbsp; &nbsp; An exchange already exists for this item. <a href="../order.php?orderid={$exchange_orderid}" target="_blank">Click here</a> to check the exchange order details.</div>
		{else}
			<div class="notification p5"><img src="/skin1/mkimages/unchecked.gif"> &nbsp; &nbsp; Exchange cannot be created for selected item - {$errorMsg}</div>
		{/if}
	{else}
		{if $singleitem }
			<div class="divider">&nbsp;</div>
			<div class="divider">&nbsp;</div>
			<form id="exchangesubmitform" action="" method="POST">
		    	<input type="hidden" name="orderid" value="{$orderid}">
	    		<input type="hidden" name="itemid" value="{$singleitem.itemid}">
	    		<input type="hidden" name="userid" value="{$login}">
	    		<input type="hidden" name="warehouseid" value="{$warehouseid}">
				<input type="hidden" name="customer_login" id="customer_login" value="{$singleitem.login}">
	    		<script type="text/javascript">
	    			var itemprice = "{$singleitem.price}";
	    			var itemdiscount = "{$singleitem.discount}";
	    			var itemcoupondiscount = "{$singleitem.coupon_discount_product}";
	    			var itempgdiscount = "{$singleitem.pg_discount}";
	    			var itemcashback = "{$singleitem.cashback}";
	    			var itemtaxamount = "{$singleitem.taxamount}";
                                var itemLoyaltyCredit = "{$singleitem.item_loyalty_points_used*$singleitem.loyalty_points_conversion_factor}";
	    			var itemqty = "{$singleitem.amount}";
	    			var pickupcost = "{$pickupcharges}";
	    			var selfDeliveryCredit = "{$selfDeliveryCredit}";
				var cartDiscountSplitOnRatio = "{$singleitem.cart_discount_split_on_ratio}";
				var discountQuantity	= "{$discount_quantity}";
					var emicharge = "{$singleitem.emi_charge}";
	    		</script>
	    		<div style="min-height: 250px;">
		    		<div class="left return-item-details">
		    			<div class="item-img">
			    			{if $singleitem.default_image}
			    				<img src="{$singleitem.default_image}">
		                    {elseif $singleitem.image_portal_t}
		                    	<img src="{$singleitem.image_portal_t}">
		                    {/if}
		                </div>
	                    <div class="divider"></div>
	                    <ul>
	                    	{if $singleitem.stylename}
	                    		<li><div><b>{$singleitem.stylename}</b></div></li> 
	                    	{/if}
	                        {* if $singleitem.product}
	                        	<li><div>{$singleitem.product}</div></li> 
	                        {/if *}
	                        {if $singleitem.price}<li><div>Rs {$singleitem.price}</div></li> {/if}
	                    </ul>
		    		</div>
	    			<div class="left return-reason-details">
		    			<table border=0 cellspacing=0 cellpadding=0 width=100%>
		    				<tr>
		    					<td width=30% align=right style="vertical-align: top;">Size and Availability:</td>
		    					<td width=70% align=left style="vertical-align: top;">
		    						<select name="size" id="size" class="return-select">
		    							{assign var=unavailable value=0}
			    						{section name=idx loop=$singleitem.sizes}
			    							{if $singleitem.sizes[idx].is_available}
	  											<option value="{$singleitem.sizes[idx].optionid}-{$singleitem.sizes[idx].sku_id}-{$singleitem.sizes[idx].sku_count}">
	  												{$singleitem.sizes[idx].size} : {if $singleitem.sizes[idx].sku_count > $singleitem.quantity}In Stock{else}
	  																				Only qty {$singleitem.sizes[idx].sku_count} available{/if}</option>
	  										{else}
	  											{assign var=unavailable value=$unavailable+1}
	  										{/if}
										{/section}
										{if $singleitem.sizes|@count eq $unavailable}
											<option value="0">No sizes available</option>
										{/if}
									</select>
									<input type=hidden name="availablecount" id="availablecount" value=$singleitem.sizes[idx].sku_count>
		    					</td>
		    				</tr>
		    				<tr>
	    						<td width=30% align=right style="vertical-align: top;">Quantity:</td>
	    						<td width=70% align=left style="vertical-align: top;">
		    						<select name="quantity" id="quantity" class="return-select">
			    						{section name=qtyVal loop=$singleitem.quantity }
	  										<option value="{$smarty.section.qtyVal.iteration}">{$smarty.section.qtyVal.iteration}</option>
										{/section}
									</select>
		    					</td>
		    				</tr>
		    				<tr>
		    					<td width=30% align=right style="vertical-align: top;">Reason<em style="color:red;">*</em>:</td>
	    						<td width=70% align=left style="vertical-align: top;">
	    							<select id="return-reason" name="return-reason" class="return-select" onchange='javascript:handleReturnReasonChange(this.value);'>
			    						<option value="0">Select Reason</option>
			    						{section name=idx loop=$reasons}
			    							<option value="{$reasons[idx].code}-{$reasons[idx].displayname}">{$reasons[idx].displayname}</option>
			    						{/section}
			    					</select>
	    						</td>
	    					</tr>
	    					<tr>
	    						<td width=40% align=right style="vertical-align: top;">Details<em style="color:red;">*</em>:</td>
	    						<td width=60% align=left style="vertical-align: top;">
				    				<textarea id="return-details" name="return-details" class="return-details-box"></textarea>
				    			</td>
				    		</tr>
				    	</table>
				    </div>
				    <div class="right return-address-details">
		               	<div class="section-heading">Exchange Shipping Address</div>
		               	<div id="">
		               	</div>
		               	<div id="">
	               		</div>
		               	<div class="divider"></div>
		               	<input type=hidden name="default-address-id" id="default-address-id" value="{$defaultAddressId}">
		               	<div id="selected-address-details">
		               		{if $selectedAddressId && $selectedAddressId neq '' }
		               			<input type=hidden name="selected-address-id" id="selected-address-id" value="{$selectedAddressId}">
		               			<input type=hidden id="pickup-available" value="{$selectedAddress.is_serviceable}">
		               			<div id="address-details" class="addressblock">
				               		<div class="left" style="width: 300px;">
										<div id="no-exchange-available" class="hide">Exchange not available for this address</div>			               				
					            		<div class="name setname">
					                       	{$selectedAddress.name}
					                   	</div>
					                   	<div class="address">{$selectedAddress.address}</div>
					                   	<div class="city">{$selectedAddress.city} - {$selectedAddress.pincode}</div>
					                   	<div class="state">{$selectedAddress.state_display}, {$selectedAddress.country_display}</div>
					                   	<div class="mobile-no"><b>Mobile</b>: {$selectedAddress.mobile}</div>
					                   	<div class="email"><b>Email</b>: {$selectedAddress.email}</div>
					                </div>
				                  	<a href="javascript:void(0);" class="editAddress" id="change-address-link" name="change-address-link" title="Change Address"></a> &nbsp;&nbsp;
				                   	<div class="divider"></div>
				                </div>
			                 	<div class="divider"></div>
		               		{ else }
	               				<div id="address-details" class="addressblock">
	               					<a href="javascript:void(0);" class="editAddress" id="change-address-link" name="change-address-link" title="Change Address"></a> &nbsp;&nbsp;
	               					No Address selected. Please select your address.
	               				</div>
		               			<div class="divider"></div>
		               		{/if}
	               		</div>
					    <div class="divider">&nbsp;</div>
						{include file="admin/main/return_item_address.tpl" address=$selectedAddress}
					</div>
				</div>
				<div class="divider">&nbsp;</div>
	    		<div class="left" style="width: 600px;">
		    		<input type=checkbox id="check1" name="check1" value="true">
		    		Please confirm that the product being returned is in unused, unwashed and has all tags/stickers/ accompanying material that the product was shipped with originally.
		    		<br/><br/>
		    		Please make sure that you ship with your returns the original order invoice and the packaging that the original order was shipped to you with.
		    		<br/><br/>
		    		Please confirm that you have read and are in agreement with all terms and conditions in Myntra’s Returns Policy.
		    	</div>
	       		<div class="refunds-section-outer right">
	           		<div class="refunds-section-inner"></div>
	            </div>
		        <div class="divider">&nbsp;</div>
		    	<div><input class="orange-bg corners p5 clearfix right" type=submit id="exchange-confirm-btn" name=exchange-confirm-btn value="Confirm Exchange" disabled></div>
		    	<div class="divider"></div>
			</form>
		{ else }
			<div class="notification p5"><img src="/skin1/mkimages/unchecked.gif"> &nbsp; &nbsp; No items are present in this order which can be exchanged.</div>
		{/if}
	{/if}
	<div class="divider"></div>
	{literal}
	<script type="text/javascript">
		function handleReturnReasonChange(val){
			if(within30days == 'false' && val != '0'){
				alert('Please ensure that you have validated the warranty expiry date / terms for the item being exchanged with the customer');
			}
		}
		var vtip = function() {
		    this.xOffset = 15; // x distance from mouse
		    this.yOffset = -20; // y distance from mouse
	
		    $(".vtip").unbind().hover(
		        function(e) {
		            this.t = this.title;
		            this.title = '';
		            this.top = (e.pageY + yOffset); this.left = (e.pageX + xOffset);
	
		            $('body').append( '<div id="vtip">' + this.t + '</div>' );
	
		            // $('div#vtip #vtipArrow').attr("src", 'images/vtip_arrow.png');
		            $('div#vtip').css("top", this.top+"px").css("left", this.left+"px").fadeIn("slow");
	
		        },
		        function() {
		            this.title = this.t;
		            $("div#vtip").fadeOut("slow").remove();
		        }
		    ).mousemove(
		        function(e) {
		            this.top = (e.pageY + yOffset);
		            this.left = (e.pageX + xOffset);
	
		            $("div#vtip").css("top", this.top+"px").css("left", this.left+"px");
		        }
		    );
	
		};
	
		var policyChecker = function() {
			if( $("#check1").is(":checked")){
			$("#exchange-confirm-btn").attr('disabled', false);
				$("#exchange-confirm-btn").removeClass('disabled-btn');
			} else {
				$("#exchange-confirm-btn").attr('disabled', true);
				$("#exchange-confirm-btn").addClass('disabled-btn');
			}
		};

		function getRefundDisplay() {
			var unitCashback = itemcashback/itemqty;
			var unitDiscount = itemdiscount/itemqty;
			var unitCouponDiscount = itemcoupondiscount/itemqty;
			var unitpgdiscount = itempgdiscount/itemqty;
			var unitCartDiscountSplitOnRatio = cartDiscountSplitOnRatio/itemqty;
			var vatamount = itemtaxamount/itemqty;
                        var unitLoyaltyCredit = Number(itemLoyaltyCredit/itemqty);
			var selectedQty = $("#quantity").val();
			var final_refund = (Number(itemprice) + Number(vatamount) - unitDiscount - unitCouponDiscount - unitpgdiscount - unitCashback - unitCartDiscountSplitOnRatio - unitLoyaltyCredit)*selectedQty ;
			final_refund -= parseFloat(pickupcost);
			if(final_refund < 0){
				final_refund = 0;
			}				
			var html = '<div class="label">Item Price:</div><div class="field">'+(itemprice*selectedQty).toFixed(2)+'</div>';
			html += '<div class="clear"></div>';
			if(unitDiscount != 0) {
				html += '<div class="label">Item Discount:</div><div class="field">-'+(unitDiscount*selectedQty).toFixed(2)+'</div>';
				html += '<div class="clear"></div>';
			}
			if(unitCartDiscountSplitOnRatio != 0 ) {
				html += '<div class="label" >Cart Discount:</div><div class="field">-'+(unitCartDiscountSplitOnRatio*selectedQty).toFixed(2)+'</div>';
				html += '<div class="clear"></div>';
			}
			if(unitCouponDiscount != 0) {
				html += '<div class="label">Coupon Discount:</div><div class="field">-'+(unitCouponDiscount*selectedQty).toFixed(2)+'</div>';
				html += '<div class="clear"></div>';
			}
			if(unitpgdiscount != 0){
				html += '<div class="label">PG Discount:</div><div class="field">-'+(unitpgdiscount*selectedQty).toFixed(2)+'</div>';
				html += '<div class="clear"></div>';
			}
			if(unitCashback != 0) {
				html += '<div class="label">*Cashback Reversal:</div><div class="field">-'+(unitCashback*selectedQty).toFixed(2)+'</div>';
				html += '<div class="clear"></div>';
			}
			if(pickupcost != 0.00) {
				html += '<div class="label">**Convenience Charge:</div><div class="field">-'+pickupcost+'</div>';
				html += '<div class="clear"></div>';
			}
			if(vatamount != 0.00) {
                                html += '<div class="label">*Vat amount:</div><div class="field">'+(vatamount*selectedQty).toFixed(2)+'</div>';
                                html += '<div class="clear"></div>';
                        }
                        if(unitLoyaltyCredit != 0.00) {
                                html += '<div class="label">*Loyalty Credit:</div><div class="field">'+-(unitLoyaltyCredit*selectedQty).toFixed(2)+'</div>';
                                html += '<div class="clear"></div>';
                        }
			html += '<div style="border-bottom: 1px dashed #CCCCCC;"></div>';
			html += '<div class="grandlabel">Amount Paid:</div><div class="grandfield">'+final_refund.toFixed(2)+'</div>';
			html += '<div class="clear"></div>';
			return html;
		}

		$(document).ready(function() {
 			$("#check1").click(policyChecker);
			policyChecker();
			$("input[name='return-method']").click(function(){
				if($("#pickup-available").val() != "1") {
					$("#no-exchange-available").css('display', 'block');
				}	
			$(".refunds-section-inner").html(getRefundDisplay());
			vtip();
			});
			if($("#pickup-available").val() != "1") {
				$("#return-method-pickup").attr('disabled', 'disabled');
			}
		$("#change-address-link").click(function(e){
			showSelectAddressWindow(e);
		});
	
		$("#newaddress-link").click(function(e){
		 	closeSelectAddressWindow();
		   	showNewAddressWindow(e);
		});
	
	    $("#new_country").change(function() {
			var country = $(this).val();
			if(country == 'IN') {
				$("#new_state_list").css('display', 'block');
				$("#new_state").css('display', 'none');
			} else {
				$("#new_state_list").css('display', 'none');
				$("#new_state").css('display', 'block');
			}
		});
					 	
		$("#exchangesubmitform").submit(function() {
			$("#exchange-confirm-btn").attr("disabled","true");
			
			if($("#size").val() == "0") {
				alert("No Item available in stock for exchange");
				$("#exchange-confirm-btn").attr("disabled","");
				return false;
			}
			
			if($("#return-reason").val() == "0") {
				alert("Please select reason for return");
				$("#exchange-confirm-btn").attr("disabled","");
				return false;
			}
			
			if($("#return-details").val() == "") {
				alert("Please describe issues/defects in the product");
				$("#exchange-confirm-btn").attr("disabled","");
				return false;
			}
			var size_id_count = $("#size").val();
			var count = size_id_count.split("-");
			count = count[2];
			if(Number($("#quantity").val()) > Number(count)) {
				alert("Quantity not available in stock for exchange");
				$("#exchange-confirm-btn").attr("disabled","");
				return false;
			}
	
			if( !$("#selected-address-id").val() || $("#selected-address-id").val() == "" ) {
				alert("You have not specified an address. Please select one.");
				$("#exchange-confirm-btn").attr("disabled","");
				return false;
			}
	
			if($("#pickup-available").val() != "1") {
				alert("Please select an address with exchange serviceablity");
				$("#exchange-confirm-btn").attr("disabled","");
				return false;
			}
			
			var overlay = new ProcessingOverlay();
            overlay.show();
			
			return true;
		});
	
		$("#quantity").change(function() {
			$(".refunds-section-inner").html(getRefundDisplay());
		});
	
		if($("#new_country").val() == "IN") {
			$("#new_state").css("display","none");	
		} else {
			$("#new_state_list").css("display","block");
		}
	
		if($("#pickup-available").val() != "1") {
	   		$("#no-exchange-available").css('display', 'block');
		}	
		$(".refunds-section-inner").html(getRefundDisplay());
		});
		
	</script>
	{/literal}
{ else }
	<div class="section-heading prepend">Exchange Created</div>
    <div class="blue-divider"></div>
    <div class="divider">&nbsp;</div>
    <div id="return-details" class="left" style="width: 580px; padding: 10px;">
		<div>Exchange order created Successfully! Please note <b>Exchange Order ID #{$exchange_orderid}</b>.</div>
		<div style="color:red;">{$errorMsg}</div>
		<div class="divider">&nbsp;</div>
	    <div>Following are the details of Exchange request</div>
	    <div class="item-img">
	    	{if $singleitem.default_image}
			    <img src="{$singleitem.default_image}" style="border:1px solid #CCCCCC;">
            {elseif $singleitem.image_portal_t}
               	<img src="{$singleitem.image_portal_t}" style="border:1px solid #CCCCCC;">
            {/if}
        </div>
        <div class="item-info" style="width: 400px;">
        	<ul class="cart-order">
            	{if $return_req.productStyleName}<li><div class="order-label"></div><div><b>{$return_req.productStyleName}</b></div></li> {/if}
                <li> <b>Product</b>: { $singleitem.stylename } </li>
                <li> <b>Quantity</b>: { $singleitem.quantity_breakup } </li>
                <li> <b>Reason</b>: { $reason } </li>
                <li> <b>Details</b>: { $details } </li>
            </ul>
        </div> 
        <div class="divider">&nbsp;</div>
        <input type=button name="close-btn" id="close-btn" value="Close" style="padding:5px 10px;" onclick="opener.window.location.reload(); self.close();"/>
    </div>
    <div class="divider">&nbsp;</div>
{/if}
