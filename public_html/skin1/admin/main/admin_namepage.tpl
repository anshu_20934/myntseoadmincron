{* $Id: featured_products.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
{include file="main/include_js.tpl" src="main/popup_product.js"}

<a name="featured" />

{$lng.txt_featured_products}

<br /><br />

{capture name=dialog}


<form action="namepage_action.php" method="post" name="admincompanyform">
<input type="hidden" name="mode" />


<table cellpadding="3" cellspacing="1" width="100%">

<tr class="TableHead">
	<td width="10%">&nbsp;</td>
	<td width="20%">Product Name</td>
	<td width="20%">description</td>
	<td width="20%">url</td>
</tr>

{if $stores}
{section name=idx loop=$stores}

<tr{cycle values=", class='TableSubHead'"}>
	<td style="left-padding:20px"><input type="checkbox" name="posted_data[{$stores[idx].id}][delete]"   /></td>
	<td align="left" style="left-padding:30px"><b>{$stores[idx].product_name}</b></td>
	<td align="left" style="left-padding:30px"><b>{$stores[idx].description}</b></td>
	<td align="left" style="left-padding:30px"><b>{$stores[idx].url}</b></td>
</tr>

{/section}

<tr>
	<td colspan="5" class="SubmitBox">
	<input type="button" value="{$lng.lbl_delete_selected|escape}" onclick="javascript: document.admincompanyform.mode.value = 'delete'; document.admincompanyform.submit();" />
	<input type="submit" value="{$lng.lbl_add|escape}"  onclick="javascript: submitForm(this, 'add');"/>
        <input type="submit" value="{$lng.lbl_modify_selected|escape}" onclick="javascript: submitForm(this, 'modify');" />
	</td>
</tr>


{else}

<tr>
<td colspan="5" align="center">{$lng.txt_no_contest_type}</td>
</tr>

<tr>
<td colspan="5" align="left"><input type="submit" value="{$lng.lbl_add|escape}"  onclick="javascript: submitForm(this, 'add');"/></td>
</tr>
{/if}



</table>
</form>

{/capture}
{include file="dialog.tpl" title=$lng.lbl_contest_heading content=$smarty.capture.dialog extra='width="100%"'}
<br /><br />

