{capture name=dialog}
<div>
	{if $message}<font color='green' size='3'>{$message}</font><br/><br/>{/if}
	{if $errorMessage}<font color='red' size='2'>{$errorMessage}</font><br/><br/>{/if}
	<form action="" method="post" name="sizeChartUploader" id="sizeChartUploader" enctype="multipart/form-data">
		<table>
			<tr>
				<td>New Line Separated Style IDs:</td>
				<td><textarea name="style_id_list" id="style_ids"></textarea></td>
				<!--  <td><input type="text" name="style_id_list" id="style_ids"></td> -->
			</tr>
			<tr>
				<td>Upload the Size Chart Image:</td>
				<td>
					<input type="hidden" name="fileName" value="sizeChartImage"/>
					<input type="file" name="sizeChartImage" id="sizeChartImage" size=100>
				</td>
			</tr>
			<tr>
				<td colspan=2 align=left><input type="button" value="Upload Images" onclick="uploadImages()"></td>
			</tr>
		</table>
		<input type='hidden' name='mode' value='upload'>
	</form>
</div>
{/capture}

{literal}
<script type="text/javascript">
function uploadImages()
{
	var style_id = $('#style_ids').val();
	if (style_id == '') 
	{ 
		alert('Please enter a Style ID.');
		return;
	}

	var images_style = $('#sizeChartImage').val();
	if (images_style == '') 
	{ 
		alert('Please select images which you want to upload.');
		return;
	}
	
	document.sizeChartUploader.mode.value = 'uploadSizeChart';
	$('#sizeChartUploader').submit();
}
</script>	
{/literal}
{include file="dialog.tpl" title='New Image Size Chart Uploader' content=$smarty.capture.dialog extra='width="100%"'}