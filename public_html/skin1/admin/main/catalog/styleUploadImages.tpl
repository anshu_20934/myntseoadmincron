{capture name=dialog}
<div>
	{if $message}<font color='green' size='3'>{$message}</font><br/><br/>{/if}
	{if $errorMessage}<font color='red' size='2'>{$errorMessage}</font><br/><br/>{/if}
	<form action="" method="post" name="styleUploadImages" id="styleUploadImages" enctype="multipart/form-data">
		<table>
			<tr>
				<td>Style ID:</td>
				<td><input type="text" name="style_id" id="style_id"></td>
			</tr>
			<tr>
				<td>Upload the images:</td>
				<td><input type="file" name="styleImagefile[]" id="styleImagefile" multiple size=100></td>
			</tr>
			<tr>
				<td colspan=2 align=left><input type="button" value="Upload Images" onclick="uploadImages()"></td>
			</tr>
			<tr>
				This page will be permanently removed from 22nd December 2014. The same functionality is provided by <a href = 'http://spoc.mynt.myntra.com/faces/catalog/images/styleImagesUpload.xhtml' target="_blank">http://spoc.mynt.myntra.com/faces/catalog/images/styleImagesUpload.xhtml </a>.
			</tr>
			<tr>
				Starting 22nd December 2014, please start using the above link to upload style images
			</tr>
		</table>
		<input type='hidden' name='mode' value='upload'>
	</form>
</div>
{/capture}

{literal}
<script type="text/javascript">
function uploadImages()
{
	var style_id = $('#style_id').val();
	if (style_id == '') 
	{ 
		alert('Please enter a Style ID.');
		return;
	}

	var images_style = $('#styleImagefile').val();
	if (images_style == '') 
	{ 
		alert('Please select images which you want to upload.');
		return;
	}
	
	document.styleUploadImages.mode.value = 'uploadImages';
	$('#styleUploadImages').submit();
}
</script>	
{/literal}
{include file="dialog.tpl" title='Upload Style Images' content=$smarty.capture.dialog extra='width="100%"'}