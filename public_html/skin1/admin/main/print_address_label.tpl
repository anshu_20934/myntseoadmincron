{* $Id: featured_products.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}


{capture name=dialog}
{literal}
     
{/literal}
{foreach key=key item=item from=$userinfo}
   {if $key eq 's_firstname'}
    {assign var='firstname' value=$item}
    {/if}
    {if $key eq 's_lastname'}
    {assign var='lastname' value=$item}
    {/if}
    {if $key eq 's_address'}
    {assign var='address' value=$item}
    {/if}
    {if $key eq 's_city'}
    {assign var='city' value=$item}
    {/if}
    {if $key eq 's_country'}
    {assign var='country' value=$item}
    {/if}
    {if $key eq 's_zipcode'}
    {assign var='zipcode' value=$item}
    {/if}

    {if $key eq 'phone'}
    {assign var='phone' value=$item}
    {/if}
    {if $key eq 'mobile'}
    {assign var='mobile' value=$item}
    {/if}
    {if $key eq 'email'}
    {assign var='email' value=$item}
    {/if}
    {if $key eq 's_state'}
    {assign var='state' value=$item}
    {/if}

{/foreach}
<table cellpadding="3" cellspacing="1" width="100%" border=0 bordercolor="#0033FF">
<tr>
     <td colspan="3">

<table width="100%" border=0>
 	<tr><td><font size="2"><strong>Shipping Address &nbsp;(#Order Id : {$orderid})</strong></font></td></tr>
 	<tr><td> <hr/></td></tr>
 	
 	
	<tr> 
		<td> 
 			<font size="2"><strong>Customer Name:&nbsp;</strong></font>{$firstname}&nbsp;{$lastname}
 		</td>
 	</tr>
 	<tr> 
		<td > 
 			<font size="2"><strong>Address:&nbsp;</strong></font>{$address}
 		</td>
 	</tr>

	<tr> 
		<td >             
 			<font size="2"><strong>City:&nbsp;</strong></font>{$city}
 		</td>
 	</tr>

 	<tr> 
		<td >
 			<font size="2"><strong>State:&nbsp;</strong></font>{$state}
 		</td>
 	</tr>

    <tr> 
		<td >        
 			<font size="2"><strong>Country:&nbsp;</strong></font>{$country}
 		</td>
 	</tr>

 	<tr> 
		<td >        
 			<font size="2"><strong>Zip/Postal Code:&nbsp;</strong></font>{$zipcode}
 		</td>
 	</tr>
 			
	<tr> 
		<td >   
 			<font size="2"><strong>Phone:&nbsp;</strong></font>{$phone}
 		</td>
 	</tr>
 			
	<tr> 
		<td >      
 			<font size="2"><strong>Mobile:&nbsp;</strong></font>{$mobile}
 		</td>
 	</tr>
	
 	<tr> 
		<td > 
 			<font size="2"><strong>Email:&nbsp;</strong></font>{$email}
 		</td>
 	</tr>
 	<tr><td> &nbsp;</td></tr>
 	
 	<tr><td><a href="#" onclick="javascript:window.open('../print_shipping_address.php?orderid={$orderid}','null','width=700,height=800,resizable=1,scrollbar=1')" >print address</a></td></tr>
 	
 	</table>

</td>
	
</tr>

</table>


{/capture}
{include file="dialog.tpl" title='' content=$smarty.capture.dialog extra='width="100%"'}
