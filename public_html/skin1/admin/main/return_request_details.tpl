{* $Id: history_order.tpl,v 1.83.2.6 2006/06/27 08:08:06 max Exp $ *}
{include file="page_title.tpl" title='Page Disabled'}
{literal}
<style >
.overlay_r{
    background-image: url("http://myntra.myntassets.com/skin1/mkimages/spacer_transparent.png");
    height: 1500px;
    left: 0;
    opacity: 0.5;
    position: absolute;
    top: 0;
    width: 100%;
    z-index: 800;
}
.box{
    position:fixed;
    top:-200px;
    left:30%;
    right:30%;
    background-color:#fff;
    color:#7F7F7F;
    padding:20px;
    border:2px solid #ccc;
    -moz-border-radius: 20px;
    -webkit-border-radius:20px;
    -khtml-border-radius:20px;
    -moz-box-shadow: 0 1px 5px #333;
    -webkit-box-shadow: 0 1px 5px #333;
    z-index:9999;
}
a.boxclose{
    float:right;
    width:26px;
    height:26px;
    background:transparent url(images/cancel.png) repeat top left;
    margin-top:-30px;
    margin-right:-30px;
    cursor:pointer;
}

</style>
{/literal}
<p align="center"><font color="red" size="30">Use REJOY for Return Details</font></p>
{*
<script type="text/javascript">
    var returnid = "{$returnid}";
    var old_status = "{$return_details.status}";
    var current_cs = "{$return_details.courier_service}";
    var returnMode = "{$return_mode}";
</script>
{include file="main/include_js.tpl" src="admin/main/returns.js"}
This section allows you to view details of Return Request.
<br><br>
{capture name=dialog}
	<div style="height: 10px;">&nbsp;</div>
	<div class="clear">
		<div class="return-left-div">
			<div style="padding: 10px 0px;"><b>Request Id</b>: #{$returnid}</div>
			<div style="padding: 10px 0px;"><b>Order Id</b>: #{$return_details.orderid}</div>
			<div style="padding: 10px 0px;"><b>Item Id</b>: #{$return_details.itemid}</div>
			{if !empty($return_details.store_return_id)}
			<div style="padding: 10px 0px;"><b>Store Return Id</b>: #{$return_details.store_return_id}</div>
			{/if}
			<div style="padding: 10px 0px;"><b>Customer Name</b>: {$return_details.name}</div>
			<div style="padding: 10px 0px;"><b>User Login</b>: {$return_details.login}</div>
			<div style="padding: 10px 0px;"><b>User Contact No.</b>: {$return_details.mobile}</div>
			<div style="padding: 10px 0px;"><b>Received at warehouse</b>: {$return_details.wh_name}</div>
			<div style="padding: 10px 0px;"><b>Received at Delivery Center</b>: {$return_details.dc_code}</div>
			{if $return_details.return_mode eq 'pickup'}
				<div style="padding: 10px 0px;"><b>Return Mode</b>: Myntra Pickup </div>
				<div style="padding: 10px 0px;"><b>Delivery Center code</b>:  {$return_details.dc_code} </div>
				<div style="padding: 10px 0px;"><b>Pickup Address</b>: { $return_details.pickup_address }</div>
			{else}
				<div style="padding: 10px 0px;"><b>Return Mode</b>: Self-Shipped</div>
			{/if}
		</div>
		<div class="return-right-div">
			<div style="padding: 10px 0px;"><b>Request Date</b>: {$return_details.createddate}</div>
			{if $return_details.return_mode eq 'pickup'}
				<div style="padding: 10px 0px;"><b>Pickup Init Date</b>: {$return_details.RPI_fdate}</div>
				<div style="padding: 10px 0px;"><b>Pickup Date</b>: {$return_details.RPU_fdate}</div>
				<div style="padding: 10px 0px;"><b>Delivered Date</b>: {$return_details.RSD_fdate}</div>
			{/if}
			<div style="padding: 10px 0px;"><b>Received Date</b>: {$return_details.RRC_fdate}</div>
			{if $return_details.status eq 'RQP'}
				<div style="padding: 10px 0px;"><b>QA Pass Date</b>: {$return_details.qapassfdate}</div>
			{/if}
			{if $return_details.RQCF_fdate}
				<div style="padding: 10px 0px;"><b>QA Fail Date</b>: {$return_details.RQCF_fdate}</div>
			{/if}
			{if $return_details.status eq 'RRS' }
				<div style="padding: 10px 0px;"><b>Reshipped Date</b>: {$return_details.RRS_fdate}</div>
			{/if}
			{if $return_details.status eq 'RSD'}
				<div style="padding: 10px 0px;"><b>Reshipped Date</b>: {$return_details.RRS_fdate}</div>
				<div style="padding: 10px 0px;"><b>ReDelivered Date</b>: {$return_details.RSD_fdate}</div>
			{/if}
			<div style="padding: 10px 0px;"><b>Item BarCode</b>: {$return_details.itemBarCode}</div>
		</div>
	</div>
	<div class="clear" style="margin-left:100px; display:block;"> 
		<div style="margin-left:20px;">
			<a href="/generatelabel.php?for=returndetails&id={$return_details.returnid}" target="_blank">Download Returns Form</a>
		</div>
	</div>
	<table>
				{if $return_details.supply_type eq 'JUST_IN_TIME' }
                                    <tr><td><span style="font-size:14px;color:red;font-weight:bold;">This is a market place return. Please forward it to this warehouse: {$order_warehouse} and return to the vendor</span></td></tr>
				{/if}
	</table>
	{if $return_details.status eq 'RRQP' or $return_details.status eq 'RPUQ2' or $return_details.status eq 'RPUQ3'}
		<div class="clear" style="margin-left:100px;"> 
			<input id ="lms_requeue_button" type="button" name="Queue to LMS" value="Queue to LMS" />
			<span> clicking this button will check if the return can be pickedup by ML, if yes it will push a pickup request to LMS.</span>
		</div>	
	{/if}
	<div class="clear"></div>
	
	{if $return_details.source_warehouseid eq 7}
        <div style="margin: 5px; padding: 10px; text-align:center; color: white; background-color:#A00000; font-size: 15px; font-weight: bold;">This item belongs to Virtual warehouse - ({$return_details.source_wh_name}). Please do a stock transfer to {$return_details.source_wh_name}</div>
    {/if}
	
	<div><hr></div>
	<form action="return_request.php?id={$returnid}" id="return_status_form" name="return_status_update" method="POST">
		<input type=hidden name=action value="return_status_change">
		<input type=hidden id="orderid" name=orderid value="{$return_details.orderid}">
		<input type=hidden id = "itemid" name=itemid value="{$return_details.itemid}">
		<input type=hidden name="shipped_items" id="shipped_items" value="">
		<input type=hidden name=skip_validation id= "skip_validation" value="no">
		<input type=hidden name=login value="{$login}">
		<input type=hidden name=dc_code value="{$return_details.dc_code}">
		<input type=hidden name=warehouse_id value="{$return_details.warehouseid}">
		<div id="errorMsgBar" style="height:30px; color:red; font-weight:bold; font-size: 12px; width: 700px; text-align:center">{$errormessage}</div>
		<table border=0 cellspacing=0 cellpadding=0 width=960>
			<tr height=30>
				<td width=150 align=right style="padding: 0 10px;"><b>Pickup Courier Service</b>: </td>
				<td width=270 align=left style="padding: 0 10px;">
					{if $return_details.courier_service }
						{ $return_details.courier_service_display }
					{else}
						<select name="courier_service" id="courier_service" style="padding: 3px; width: 150px; border: 1px solid #777777;" onchange="updateCourierServiceDisplay(this.value);">
							{section name=idx loop=$couriers}
								<option value="{$couriers[idx].code}">{$couriers[idx].display_name}</option>
							{/section}
							<option value="other">Other</option>
						</select>
						<input type="text" name="courier_service_custom" id="courier_service_custom" style="width:150px; display:none;">
					{/if}
				</td>
				<td width=150 align=right style="padding: 0 10px;">
					<b>Pickup Tracking No</b>: 
				</td>
				<td width=270 align=left style="padding: 0 10px;">
					<input type=text name="trackingno" id="trackingno" value="{$return_details.tracking_no}" style="padding: 3px; width: 200px; border: 1px solid #777777;" {if $return_details.tracking_no neq ''} disabled {/if}>
				</td>
			</tr>
			{if $return_details.status eq 'RRS' || $return_details.status eq 'RSD' || $return_details.status eq 'RRRS' }
				<tr height=30>
					<td width=150 align=right style="padding: 0 10px;"><b>Reship Courier Service</b>: </td>
					<td width=270 align=left style="padding: 0 10px;"> {$return_details.reship_courier_display} </td>
					<td width=150 align=right style="padding: 0 10px;"> <b>Reship Tracking No</b>: </td>
					<td width=270 align=left style="padding: 0 10px;">{$return_details.reship_tracking_no}</td>
				</tr>
			{/if}
			<tr height=10><td colspan=3></td></tr>
			<tr height=30>
				<td width=150 align=right style="padding: 0 10px;"><b>Return Status</b>: </td> 
				<td width=270 align=left style="padding: 0 10px;">
					<select name="status" id="return-status" style="width: 200px; padding: 3px; float:left;">
						{foreach from=$statuses key=code item=status}
							<option value="{$code}" {if $return_details.status eq $code}style="color:red;"{/if} {if $return_details.status eq $code }selected{/if}>{$status}</option>
						{/foreach} 
					</select>
					<select name="qapass-quality" id="qapass-quality" style="display:none; padding: 3px; float:left; width: 50px;">
						<option value="Q1">Q1</option>
						<option value="Q2">Q2</option>
						<option value="Q3">Q3</option>
					</select>
				</td>
				<td id="qc-reason-label" width=150 align=right style="padding: 0 10px; display:none;"><b>Quality Reason</b>: </td> 
				<td id="qc-reason-select" width=270 align=left style="padding: 0 10px; display:none;">
					<select name="qc-reason" id="qc-reason" style="width: 200px;">
						<option value="">Select Reason</option>
    					{foreach from=$quality_check_codes item=qcReason }
                        	<option value="{$qcReason.id}">{$qcReason.quality} - {$qcReason.rejectReason} - {$qcReason.rejectReasonDescription}</option>
                        {/foreach}				
					</select>
				</td>
			</tr>
			<!--
			{if $return_details.status eq 'RRC' }
				<tr height=10><td colspan=3></td></tr>
				<tr height=30>
					<td width=150 align=right style="padding: 0 10px;"><b>Return Reason Validation</b>: </td> 
					<td width=270 align=left style="padding: 0 10px;">
						<div id="return-reason-validation" style="width:200">
		               		<div>
		               			<input type=radio id="reason-validation" name="valid" checked value="yes"> Return Reason Accurate
		               		</div>
	               			<div>
	               				<input type=radio id="reason-validation" name="valid" value="no"> Return Reason In-Accurate
	               			</div>
		               	</div>
					</td>
					<td id="reason-label" width=150 align=right style="padding: 0 10px; display:none"><b>Identified Reason</b>: </td> 
					<td id="reason-select" width=270 align=left style="padding: 0 10px; display:none">
	                	<select name="accurate-reason" id="accurate-reason" style="width: 240px; padding: 3px; float:left;">
							<option value="">Select Reason</option>
    						{section name=idx loop=$reasons}
    							<option value="{$reasons[idx].code}">{$reasons[idx].displayname}</option>
    						{/section}
						</select>
					</td>
				</tr>
			{/if}
			-->
			<tr height=10><td colspan=3></td></tr>
			<tr>
				<td width=150 align=right style="padding: 0 10px;"><b>Received Item Barcode</b>: </br>Enter items in new lines &nbsp;</td>
				<td width=270 align=left style="padding: 0 10px;">	
					{if $return_details.status eq 'RRQP' || $return_details.status eq 'RRQS' || $return_details.status eq 'RPI' || $return_details.status eq 'RPU' || $return_details.status eq 'RDU' || $return_details.status eq 'RRC'}
						<textarea name="item_bar_code" id="item_bar_code" disabled> </textarea>
						<input type="hidden" name="item_barcodes_list" id="item_barcodes_list" /> <br/>
						<input type="hidden" name="skip_item_validation" id="skip_item_validation" value="no"/> 
					{else}
						{if $return_details.itembarcode ne ''}
							{$return_details.itembarcode}
						{else}
							-
						{/if}
					{/if} 
				</td>
				<td width=150 align=right style="padding: 0 10px;"><b>Comments</b> (<em style="color:red;">*</em>):</td>
				<td width=270 align=left style="padding: 0 10px;">
					<textarea rows="4" cols="40" name="comment" id="comment"></textarea>
				</td>
			</tr>
			<tr height=20><td colspan=3></td></tr>
			<tr>
                <td width=150 align=right style="padding: 0 10px;"><b>Ops Location</b>:</td>
                <td width=270 align=left style="padding: 0 10px;">  
                    <select id="warehouseid" name="warehouseid" style="width: 150px; padding: 3px; float:left;" disabled>
					    <option value="">--Select Ops Location--</option>
					    {section name=idx loop=$warehouses}
					        <option value="{$warehouses[idx].id}" {if $return_details.warehouseid eq $warehouses[idx].id }selected{/if}>{$warehouses[idx].name}</option>
					    {/section}
					</select>
                </td>
                <td width=150 align=right style="padding: 0 10px;"><b>Delivery Center Location</b>:</td>
                <td width=270 align=left style="padding: 0 10px;">
                	<select name="dc-code" id="dc-code" style="width: 150px; padding: 3px; float:left;" disabled>
						<option value="">--Select DC Location--</option>
						{foreach from=$dccodes item=name}
							<option value="{$name.code}" {if $return_details.dc_code eq $name.code}selected{/if}>{$name.name}</option>
						{/foreach} 
					</select>
				</td>
            </tr>
			<tr>
				<td align=right colspan=4>
					<input id="status_change_button" type=submit name="status_change_button" value="Apply Changes" style="padding: 3px;">
				</td>
			</tr>
		</table>
	</form>
	{if $fileuploadmessage != ''}
		<div style="text-align: center;background-color: #efefef;margin:10px;padding:10px;">
			<span style="font-size:14px;color:green">{$fileuploadmessage}</span>
		</div>
	{/if}
	<div style="clear:both;margin-top:10px;border:1px solid #999999;width:95%">
	<form action="" method="POST" enctype="multipart/form-data">
		<input type=hidden name=login value="{$login}">
		<input type="hidden" name="action" value="fileupload">
		<table width="100%">
			<tr>
				<td colspan="4" style="text-align: center;background-color: #efefef;"> <span style="font-size:14px; color:red;font-weight: bold;">File Upload Section:</span></td>
			</tr>
			<tr style="padding: 5 10px;">
				<td width=150 align=right><b>Image</b> : </td>
				<td width=100 align=left>
					<input type="file" name="attachment1">
				</td>
				<td width=50 align=right><b>Comment</b> : </td>
				<td width=270 align=left>
					<input type="text" name="comment1" size="70">
				</td>
			</tr>
			<tr style="padding: 5 10px;">
				<td width=150 align=right><b>Image</b> : </td>
				<td width=100 align=left>
					<input type="file" name="attachment2">
				</td>
				<td width=50 align=right><b>Comment</b> : </td>
				<td width=270 align=left>
					<input type="text" name="comment2" size="70">
				</td>
			</tr>
			<tr style="padding: 5 10px;">
				<td width=150 align=right><b>Image</b> : </td>
				<td width=100 align=left>
					<input type="file" name="attachment3">
				</td>
				<td width=50 align=right><b>Comment</b> : </td>
				<td width=270 align=left>
					<input type="text" name="comment3" size="70">
				</td>
			</tr>
			<tr style="padding: 5 10px;">
				<td width=150 align=right><b>Image</b> : </td>
				<td width=100 align=left>
					<input type="file" name="attachment4">
				</td>
				<td width=50 align=right><b>Comment</b> : </td>
				<td width=270 align=left>
					<input type="text" name="comment4" size="70">
				</td>
			</tr>
			<tr>
				<td colspan="4" style="text-align: right;"> <input style = "margin-right:200px;padding:5px;" type="submit" name="submit" value="Submit"></td>
			</tr>
		</table>
	</form>
			
	
	</div>
{/capture}
{include file="dialog.tpl" title='Request Details' content=$smarty.capture.dialog extra='width="100%"'}
<br><br>
{capture name=dialog}
<form action="" method="POST">
	<table width="100%"  border="0" cellspacing="0" cellpadding="0">
		<tr height="75">
			<td>&nbsp;</td>
			<td width=150>
				{if $return_details.designImagePath != ""}
					<img src="{$return_details.designImagePath}"  style="border:1px solid #CCCCCC;">
				{else}
					<img  src="{$xcart_web_dir}/image.php?type={$type}&amp;id={$id}&amp;ts={$smarty.now}" width="100"  height="70" hspace="5" vspace="5" />
				{/if}
			</td>
			<td valign=top width=250 style="padding: 0px 5px;">
				<b>Product:</b>  {$return_details.productStyleName}<br><br>
				<b>Style:</b>   {$return_details.productStyleLabel}<br><br>
				<b>Quantity: </b>  {$return_details.quantity}<br><br>
				<b>Price:</b>  {$return_details.item_price*$return_details.item_qty}<br><br>
				<b>SKU Code:</b>  {$return_details.skucode}<br><br>
			</td>
			<td valign=top width=350 style="padding: 0px 5px;">
				<b>Reason:</b> {$return_details.reason} <br><br>
				{if $return_details.validated_reason neq ''} 
					<b>Validated Reason:</b> {$return_details.validated_reason} <br><br>
				{/if}
				<b>Details:</b> {$return_details.description} <br>
			</td>
			<td valign=top style="padding: 0px 5px;">
				<b>Status:</b>  {$return_details.status_display}
				{if $return_details.couponcode }
					<br><br>
					<b>Refund Coupon:</b>  {$return_details.couponcode}
					<br><br>
					<b>Refund Amount:</b>  {$return_details.refundamount}
				{/if}
                                <b>Article number: </b> {$return_details.article_number} <br>
                                <b>Size: </b> {$return_details.sizeoption} <br>
			</td>
		</tr>
		<tr height=20><td colspan=8><hr/></td></tr>
	</table>
</form>

<div class="overlay_r" id="overlay_r" style="display:none;"></div>
<div class="box" id="box">
 <h1>Sku Id of the item scanned did not match sku id of the item ordered</h1>
 <p style="font-size:1.2em;"> 
 	If you still want to proceed with the returns process, click proceed. If not, close this by clicking cancel.
 </p> 
  <input type="button" id="boxsubmit" value="Proceed"> &nbsp;&nbsp;&nbsp;
  <input type="button" id="boxclose" value="Cancel">
  
 
</div>

{/capture}
{include file="dialog.tpl" title='Returned Items Details' content=$smarty.capture.dialog extra='width="100%"'}
<br><br>
{capture name=dialog}
	<br>
	<div style="float:left; width: 50%; margin: 10px 0;">Comments log for return request #{$returnid}</div>
	<div style="float:right; width: 40%; margin: 10px 50px 10px 0; text-align: right;"><a href="javascript:void(0);" id="newCommentLink"><b>Add New Comment</b></a></div>
	<div style="clear: both;"></div>
	<div style="display:none; margin: 10px 0;" id="newCommentSection">
		<form id="newCommentForm" name="newCommentForm" action="" method="POST" style="border: 1px solid black;">
			<input type="hidden" name="action" value="addNewComment">
			<input type="hidden" name="id" value = "{$returnid}">
			<table border=0 cellspacing=0 cellpadding=0 width=99% style="border-color: #CCCCCC; border-collapse: collapse; margin: 5px;">
				<tr>
					<td style="padding: 10px; width: 10%;">Added By:</td>
					<td style="padding: 10px;"><input type="text" name="updatedBy" value="{$login}" disabled style="width: 300px; padding: 10px;"></td>
				</tr>
				<tr>
					<td style="padding: 10px; width: 10%;">Title:</td>
					<td style="padding: 10px;"><input type="text" name="title" id="title" value="" style="width: 300px; padding: 10px;"></td>
				</tr>
				<tr>
					<td style="padding: 10px; width: 10%;">Comment Description:</td>
					<td style="padding: 10px;"><input type="text" name="description" id="description" value="" style="width: 750px; padding: 10px;"></td>
				</tr>
				<tr>
					<td style="padding: 10px; width: 10%;"><input id="cancelAddComment" type=button name="cancelAddComment" value="Cancel" style=" padding: 10px;"></td>
					<td style="padding: 10px;"><input type=submit name="submitBtn" value="Save Comment" style="padding: 10px;"><td>
				</tr>
			</table>
		</form>
	</div>
	<div style="margin: 10px 0;" id="commentSection">
		<table border=1 cellspacing=0 cellpadding=0 width=99% style="border-color: #CCCCCC; border-collapse: collapse;">
			<tr height=30 bgcolor="#DFDFDF">
				<th style="font-size:14px;">Added by</th>
				<th style="font-size:14px;">Title</th>
				<th style="font-size:14px;">Type</th>
				<th style="font-size:14px;">Description</th>
				<th style="font-size:14px;">Added On</th>
			</tr>
			{section name=idx loop=$comments_log}
				<tr height=30>
					<td align=center>{$comments_log[idx].addedby}</td>
					<td align=center>{$comments_log[idx].commenttitle}</td>
					<td align=center>{$comments_log[idx].commenttype}</td>
					<td>{$comments_log[idx].description}
					{if $comments_log[idx].image_url != ""}
						<br/>Click <a target="_blank" href = "{$http_location}/admin/data/returns/{$returnid}/{$comments_log[idx].image_url}"> here</a> to view image.
					{/if}
					</td>
					<td align=center>{$comments_log[idx].adddate}</td>
				</tr>
			{/section}
		</table>
	</div>
{/capture}
{include file="dialog.tpl" title='Comments Log' content=$smarty.capture.dialog extra='width="100%"'}
<br><br><br>
{capture name=dialog}
	<br>
	Audit Log for return #{$returnid}
	<br><br><br>
	<table border=1 cellspacing=0 cellpadding=0 width=99% style="border-color: #CCCCCC; border-collapse: collapse;">
		<tr height=30 bgcolor="#DFDFDF">
			<th style="font-size:14px;">Added by</th>
			<th style="font-size:14px;">From State</th>
			<th style="font-size:14px;">To State</th>
			<th style="font-size:14px;">Added On</th>
		</tr>
		{section name=idx loop=$audit}
			<tr height=30>
				<td align=center>{$audit[idx].created_by}</td>
				<td align=center>{$audit[idx].from_name}</td>
				<td align=center>{$audit[idx].to_name}</td>
				<td align=center>{$audit[idx].aud_time}</td>
			</tr>
		{/section}
	</table>
{/capture}
{include file="dialog.tpl" title='Audit Log' content=$smarty.capture.dialog extra='width="100%"'}
*}
