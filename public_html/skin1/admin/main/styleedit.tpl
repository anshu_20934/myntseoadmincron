
<link  rel="stylesheet" href="{$http_location}/skin1/css/uploadify.css"   />
{literal}
<style type="text/css">
 .style_table{border:1px solid #ccc;padding:5px;}

 .style_table td div.error_div{clear:both;margin:5px 0px}
 .style_table .textbox{width:150px;}
 .style_table select{width:150px;}
 .style_table .shade1{background-color:#ccc;height:80px}
 .style_table .shade2{background-color:#f0f0ee;;height:80px}
 .style_table .label{font-weight:bold;padding-top:20px;vertical-align:top;}
 .overlay {
background-image:url(http://myntra.myntassets.com/skin1/mkimages/spacer_transparent.png);
height:100%;
left:0;
opacity:0.5;
position:fixed;
top:0;
width:100%;
z-index:800;
}

form label.error
{
  background:url("../skin1/mkimages/unchecked.gif") no-repeat 0px 0px;
  padding-left: 16px;
  padding-bottom: 2px;
  font-weight: bold;
  color: #EA5200;
}

form label.checked
{
  background:url("../skin1/mkimages/check_valid.gif") no-repeat 0px 0px;
}
</style>
<script type="text/javascript">
var rowCount = 0;
var scale_size_value = '<option value="">- Select a Scale first -</option>';
var style_id='';
var size_chart_scale_id = '';
function getOptionRow(i)
{
	var optionRowHtml = '<tr class="shade2" id="optionTr'+i+'">';
	if(applySizeScaleRestrictionPageMode)
	{
		optionRowHtml = optionRowHtml
		+'<td class="label selected_scale_size_values_td">Size Value*:'
		+'<select onchange="setTextBoxSizeValue(this, '+i+')" class="selected_scale_size_values" name="attr_type_size_value['+i+']"  id="attr_type_size_value['+i+']">'+scale_size_value+'</select></td>';
	}
	else if(applySizeScaleRestriction)
	{
		optionRowHtml = optionRowHtml
		+'<td class="label selected_scale_size_values_td" style="display:none">Size Value*:'
		+'<select onchange="setTextBoxSizeValue(this, '+i+')" class="selected_scale_size_values" name="attr_type_size_value['+i+']"  id="attr_type_size_value['+i+']">'+scale_size_value+'</select></td>';
	}
	
	optionRowHtml = optionRowHtml	
	+'<td style="display:none">'
	+'<select style="display:none" name="size_option_name['+i+']" id="size_option_name['+i+']" style="width:150px;">  <option value="">None</option>  <option value="Size" selected="true">Size</option>  </select> </td>'
	+'<td style="padding-left:10px" class="label"> Size * : ';

	if(applySizeScaleRestrictionPageMode)
	{
		optionRowHtml = optionRowHtml
		+'<input type="text" class="textbox textbox_size_value" name="size_option_value['+i+']" id="size_option_value['+i+']" value="" readonly="readonly"/>';
	}
	else
	{
		optionRowHtml = optionRowHtml
		+'<input type="text" class="textbox textbox_size_value" name="size_option_value['+i+']" id="size_option_value['+i+']" value=""/>';
	}

	optionRowHtml = optionRowHtml
	+'<input type="hidden" class="textbox" name="option_id['+i+']" id="option_id['+i+']" value="" /> </td>'
	+'<td class="label"> SKU Code * : <input class="textbox" name="sku_code_value['+i+']" id="sku_code_value_'+i+'" onkeyup="list_skus(\'sku_code_value_'+i+'\')" value="" autocomplete="off" /> </td> '
	+'<td class="label" style="display:none;"> Enabled : '
	+'<input onchange="checkEnabled(this, '+i+')" type="checkbox" name="sku_enabled_cb['+i+']" id="sku_enabled_cb['+i+']" checked="true" />'
	+'<input type="hidden" name="sku_enabled['+i+']" id="sku_enabled['+i+']" value="1" />'
	+'<input type="hidden" name="sku_id_value['+i+']" id="sku_id_value['+i+']" value="" /></td>'
	+'<td colspan=3></td></tr>';
	return optionRowHtml;
}

function addOptionsRow()
{
	var tempHtml;
	tempHtml = getOptionRow(rowCount);
	$("#addTr").before(tempHtml);
	rowCount++;
}

function deleteOptionsRow()
{
	if(rowCount < 2)
	{
		alert("You can not delete this one size option row.");
		return;
	}
	rowCount--;
	$('#optionTr'+rowCount).remove();
}


function deleteOptionsSkusMapping(ele, i)
{
	if($('#optionTr'+i+' .option-name-select').val() == 'None')
	{
		ele.value = "Delete Row";
		$('#optionTr'+i+' .option-name-select').val("Size");
		$('#optionTr'+i).contents('td').css('border', '0');
	}
	else
	{
		ele.value = "Undelete Row";
		$('#optionTr'+i+' .option-name-select').val("None");
		$('#optionTr'+i).contents('td').css('border', '1px solid red');
		alert("Red Border Size option will be deleted when you update the product.");
	}
}

function checkEnabled(ele, i)
{
	if(i == 'freesize')
	{
		var eleHid = document.getElementById("sku_enabled_freesize");
	}
	else
	{
		var eleHid = document.getElementById("sku_enabled["+i+"]");
	}
	if(ele.checked)
	{
		eleHid.value = 1;
	}
	else
	{
		eleHid.value = 0;
	}
}

function deleteImage(image_type)
{
	$.ajax({ url : http_loc + '/admin/catalog/styleedit.php',
				type : 'POST',
				data : {'style_id' : style_id, 'image_type' : image_type, 'mode' : 'remove_image'}
	});
}
</script>
{/literal}
{capture name=dialog}
{if $success_msg}
<div style="color:red;font-size:14px;font-weight:bold;padding:10px">{$success_msg}</div>
{/if}
{if $error_msg}
<div style="color:red;font-size:14px;font-weight:bold;padding:10px">{$error_msg}</div>
{/if}
<script type="text/javascript">
var possibleStatusTrans = {$possibleStatusTrans};
var applySizeScaleRestriction = '{$applySizeScaleRestriction}';
var applySizeScaleRestrictionPageMode = {$applySizeScaleRestrictionPageMode};
</script>
{if $smarty.get.mode eq 'modifystyle'}
<script type="text/javascript">
style_id='{$style_details[0].id}';
size_chart_scale_id = '{$optionScaleId}';
scale_size_value  = '';
{foreach from=$sizeChartValues item=size_value}
	scale_size_value += '<option value="{$size_value.id}">{$size_value.size_value}</option>';	
{/foreach}
</script>
{include file="admin/main/styleedit_modify_action.tpl"}
{else}
<form action="{$http_location}/admin/catalog/styleedit.php" method="post" name="productstyleaction" id="productstyleaction" enctype="multipart/form-data">
<input type="hidden" name="mode" value="addstyle">
<table width="100%" cellpadding="0" cellspacing="0" class="style_table" >
    <tr class="shade2">
		<td class="label" width="33%">
            Vendor Article Number * : <input type="text" class="textbox" name="article_number" value="" maxlength="255"> <div class="error_div"></div>
        </td>
        <td class="label" width="33%">
            Vendor Article Name * : <input type="text" class="textbox" name="article_name" id="article_name" value="" maxlength="255" onblur="setProductDisplayName()"><div class="error_div"></div>
        </td>
		<td  class="label" width="33%">
         Status : <select name="styletype" id="styletype">
         {foreach from=$allStyleStatus item=status}
            <option value="{$status.id}" >{$status.name}</option>
          {/foreach}
         </select>
        </td>
	</tr>
	<tr class="shade2">
		<td class="label" colspan="3">
           Comments : <input type="text" class="textbox" name="style_comments" value="{$style_properties.comments}">
        </td>
	</tr>
</table>
<div style="font-size: 15px;font-weight: bold;padding: 10px 5px 5px;" > CATALOG CLASSIFICATION - USED FOR OPERATION AND ANALYSIS</div>
<table width="100%" cellpadding="0" cellspacing="0" class="style_table">
    <tr class="shade2">
		
		<td class="label" width="100px">
            Article type * : 
		</td>
		<td class="label" width="200px">
			<select name="global_attr_article_type" onchange="getParents(this.value); checkDiscountRule();" id="global_attr_article_type" >
				<option value="">Select</option>
				{foreach from=$article_types item=types}
				<option value="{$types.id}">{$types.typename}</option>
				{/foreach}
			</select>
            <div class="error_div"></div>
        </td>
		<td class="label" width="100px">
            Sub Category * : 
		</td>
		<td class="label" width="200px">
		<input type="text" class="textbox" name="global_attr_sub_category_name" id="global_attr_sub_category_name" value="" readonly="true">
		<input type="hidden" class="textbox" name="global_attr_sub_category" id="global_attr_sub_category_id" value="" readonly="true">
            
        </td>
		<td class="label" width="100px">
            Master Category * : 
		</td>
		<td class="label" width="200px">
			<input type="text" class="textbox" name="global_attr_master_category_name" id="global_attr_master_category_name" value="" readonly="true">
			<input type="hidden" class="textbox" name="global_attr_master_category" id="global_attr_master_category_id" value="" readonly="true">
            
        </td>
	</tr>
	<tr class="shade2">
		<td class="label">
            Brand * : 
		</td>
		<td class="label">
			<select name="global_attr_brand" onchange="setBrand(this.value); checkDiscountRule();" id="global_attr_brand">
				<option value="">NA</option>
				{foreach from=$attributes.Brand item=types}
						<option value="{$types}">{$types}</option>
				{/foreach}
			</select>
			<div class="error_div"></div>
        </td>
		<td class="label">
            Age Group : 
		</td>
		<td class="label">
			<select name="global_attr_age_group" >
				<option value="NA">NA</option>
				{foreach from=$attributes.AgeGroup item=types}
						<option value="{$types}">{$types}</option>
				{/foreach}
			</select>
            
        </td>
		<td class="label">
            Gender * :
		</td>
		<td class="label">
			<select name="global_attr_gender" id="global_attr_gender">
				<option value="">NA</option>
				{foreach from=$attributes.Gender item=types}
						<option value="{$types}">{$types}</option>
				{/foreach}
			</select>
            <div class="error_div"></div>
        </td>
	</tr>
	<tr class="shade2">
		<td class="label">
            Base Colour * :
		</td>
		<td class="label">
			<select name="global_attr_base_colour" id="global_attr_base_colour">
				<option value="">NA</option>
				{foreach from=$attributes.Colour item=types}
						<option value="{$types}">{$types}</option>
				{/foreach}
			</select>
			<div class="error_div"></div>
        </td>
		<td class="label">
            Colour 1 : 
		</td>
		<td class="label">
			<select name="global_attr_colour1" >
				<option value="NA">NA</option>
				{foreach from=$attributes.Colour item=types}
						<option value="{$types}">{$types}</option>
				{/foreach}
			</select>
            
        </td>
		<td class="label">
            Colour 2 : 
		</td>
		<td class="label">
			<select name="global_attr_colour2" >
				<option value="NA">NA</option>
				{foreach from=$attributes.Colour item=types}
						<option value="{$types}">{$types}</option>
				{/foreach}
			</select>
            
        </td>
	</tr>
	<tr class="shade2">
		<td class="label">
            Fashion Type * :
		</td>
		<td class="label">
			<select name="global_attr_fashion_type"  id="global_attr_fashion_type" onChange="checkDiscountRule()">
				<option value="">NA</option>
				{foreach from=$attributes.FashionType item=types}
						<option value="{$types}">{$types}</option>
				{/foreach}
			</select>
			<div class="error_div"></div>
        </td>
		<td class="label">
            Season * :
		</td>
		<td class="label">
			<select name="global_attr_season" id="global_attr_season" >
				<option value="">NA</option>
				{foreach from=$attributes.Season item=types}
						<option value="{$types}">{$types}</option>
				{/foreach}
			</select>
			<div class="error_div"></div>
        </td>
		<td class="label">
            Year * :
		</td>
		<td class="label">
			<select name="global_attr_year" id="global_attr_year" >
				<option value="">NA</option>
				{foreach from=$attributes.Year item=types}
						<option value="{$types}">{$types}</option>
				{/foreach}
			</select>
            <div class="error_div"></div>
        </td>
	</tr>
	<tr class="shade2">
		<td class="label">
            Usage * :
		</td>
		<td class="label">
			<select name="global_attr_usage" id="global_attr_usage" >
				<option value="">NA</option>
				{foreach from=$attributes.Usage item=types}
						<option value="{$types}">{$types}</option>
				{/foreach}
			</select>
			<div class="error_div"></div>
        </td>
		<td colspan="5"></td>
		
	</tr>
		
</table>
<div style="font-size: 15px;font-weight: bold;padding: 10px 5px 5px;display:none" id="article_type_specific_attributes_div">Specific Attributes</div>
<table width="100%" cellpadding="0" cellspacing="0" class="style_table" id="article_type_specific_attributes_table" style="display:none">
    
</table>	
<div style="font-size: 15px;font-weight: bold;padding: 10px 5px 5px;" > PRICING AND DISCOUNTS</div>
<table width="100%" cellpadding="0" cellspacing="0" class="style_table">
    <tr class="shade2">
		<td class="label">
            Price * : <input type="text" class="textbox" id="price" name="price" value="" >
            <div class="error_div"></div>
        </td>

        <!-- td class="label">
            Discount Type :
            <select name="discount_type">
            <option value="">None</option>
            <option value="absolute">Absolute</option>
            <option value="percent">Percent</option>
            </select>
        </td>

         <td class="label">
            Discount Value : <input type="text" class="textbox" name="discount_value" value="" >
        </td -->
        
        <td class="label">
            Rating: <select name="rating">
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="9">9</option>
                    <option value="10">10</option>
                    <option value="11">11</option>
                    </select>
        </td>
        
        <td class="label">
        	<div id="discountrule"></div>
        </td>

	</tr>
</table>
<div style="font-size: 15px;font-weight: bold;padding: 10px 5px 5px;" > SIZE OPTIONS</div>
<table width="100%" cellpadding="0" cellspacing="0" border="0" class="style_table" >
	<tr class="shade2"  >
		<td style="border-right:1px solid #999"> <input type="radio" name="size_option_check" value='2' id="freesize_check" checked onchange="disable_inputs('freesize')" > Freesize </td>
		<td style="padding-left:10px">
			<table width="100%" cellpadding="0" cellspacing="0" id="freesize_table">
				<tr id="addFreesizeTr" class="shade2">
				<td class="label" width="380px">
					<input type="hidden" class="textbox" name="size_option_freesize" id="size_option_value_freesize" value="Freesize" />
					<input type="hidden" class="textbox" name="option_id_freesize" id="option_id_freesize" value="" />
				</td>
				<td class="label"> SKU Code * :
					<input class="textbox" name="sku_code_freesize" id="sku_code_value_freesize" onkeyup="list_skus('sku_code_value_freesize')" value="" autocomplete="off" />
				</td>
				<td class="label" style="visibility:hidden;"> Enabled : <input onchange="checkEnabled(this, 'freesize')" type="checkbox" name="sku_enabled_cb_freesize" id="sku_enabled_cb_freesize" checked="true" />
					<input type="hidden" name="sku_enabled_freesize" id="sku_enabled_freesize" value="1" />
					<input type="hidden" name="sku_id_value_freesize" id="sku_id_value_freesize" value="" />
				</td>

				</tr>
			</table>
		</td>
	</tr>
	<tr class="shade2" >
		<td  style="border-top:1px solid #999;border-right:1px solid #999"> Size Options
			<input type="radio" id="size_option_check" name="size_option_check" value='1' onchange="disable_inputs('sizeoption')"/>
			<input type="hidden" class="textbox" name="appy_scale_mode" id="appy_scale_mode" value="{$applySizeScaleRestrictionPageMode}" />
		</td>
		<td  style="border-top:1px solid #999;">
			<table width="100%" cellpadding="0" cellspacing="0" id="sizeoption_table" >
			{if $applySizeScaleRestriction}
				<tr class="shade2" id="option_scale_tr">
					<td class="label">Size Scale*:
						<select name="attr_type_size_chart_scale"  id="attr_type_size_chart_scale" onChange="getSizeOptionValue(this.value)">
						   	<option value="">- Select Article Type -</option>
						</select>
					</td>
				</tr>
			{/if}
			<tr id="addTr" class="shade2">
				<td colspan="1" style="padding-left:10px"><input type="button" value="Add Row" onclick="addOptionsRow()"/></td>
				<td colspan="5" style="padding-left:10px"><input type="button" value="Delete Last Row" onclick="deleteOptionsRow()"/></td>
			</tr>
			</table>
		</td>
	</tr>
</table>
<div style="font-size: 15px;font-weight: bold;padding: 10px 5px 5px;" >DISPLAY CLASSIFICATION - USED ON THE MYNTRA.COM USER INTERFACE</div>
<table width="100%" cellpadding="0" cellspacing="0" class="style_table">
	<tr class="shade2">
	{assign var=counter value=0}
	{foreach from=$all_filters key=group item=filters}
        <td class="label" width="100px" align="center">
			{if $group eq 'Brands'}Classification Brand{else}{$group}{/if}
		</td>
		<td class="label" width="250px" align="center">
            <select name="{$group|replace:" ":""|replace:"/":""|lower}_filters[]" id="{$group|lower}" {if $group eq 'Brands' || $group eq 'Fit'}onchange="setProductDisplayName()" {else}multiple="multiple" size="10"{/if} style="width:160px" >
                {if $group eq 'Brands' || $group eq 'Fit'}<option value="">None</option>{/if}
            {foreach from=$filters key=k item=filter}
                <option value="{$filter.filter_name}">{$filter.filter_name}</option>
            {/foreach}
            </select>
        </td>
		{assign var=counter value=$counter+1}

		{if $counter mod 3 eq 0}
			</tr>
			<tr class="shade2">
		{/if}
    {/foreach}
	</tr>
</table>
<div style="font-size: 15px;font-weight: bold;padding: 10px 5px 5px;" > PRODUCT DETAILS</div>
<table width="100%" cellpadding="0" cellspacing="0" class="style_table">
	<tr class="shade2">
		<td colspan="2" class="label">Product Display Name : <input type="text"  name="product_display_name" value="" style="width:300px" id="product_display_name" readonly="true" maxlength="255">
			<input type="button" value="modify" onclick="$('#product_display_name').removeAttr('readonly')"/>
        </td>
		<td class="label" colspan="2">
            Tags * : <input type="text" class="textbox" name="product_tags" id="product_tags" value="" style="width:400px">
            <div class="error_div"></div>
        </td>
    </tr>
	<tr class="shade1">
		<td class="label">Description</td>
        <td colspan="3"><textarea name="product_desc" cols="50" rows="5" class="mceEditor" ></textarea></td>
        <div class="error_div"></div>
    </tr>
    <tr class="shade2"><td class="label" colspan="4"></td></tr>
    <tr class="shade1">
		<td class="label">Style Note</td>
        <td colspan="3"><textarea name="styleNote" cols="50" rows="5" class="mceEditor" ></textarea></td>
        <div class="error_div"></div>
    </tr>
    <tr class="shade2"><td class="label" colspan="4"></td></tr>
    <tr class="shade1">
		<td class="label">Materials Care Desc</td>
        <td colspan="3"><textarea name="materialsCareDesc" cols="50" rows="5" class="mceEditor" ></textarea></td>
        <div class="error_div"></div>
    </tr>
    <tr class="shade2"><td class="label" colspan="4"></td></tr>
    <tr class="shade1">
		<td class="label">Size Fit Desc</td>
        <td colspan="3"><textarea name="sizeFitDesc" cols="50" rows="5" class="mceEditor" ></textarea></td>
        <div class="error_div"></div>
    </tr>
    <tr class="shade2">
        <td class="label" colspan="2">
            Product type * :
            <select name='product_type' id="product_type" onchange="setProductDisplayName()">
            {foreach from=$producttypes key=group item=types}
                <optgroup label="{$group}">
                {foreach from=$types item=type}
                    <option value="{$type.id}" >{$type.name}</option>
                 {/foreach}
            {/foreach}

            </select>
            <div class="error_div"></div>
        </td>
        <td class="" colspan="2">
            Target URL : <input type="text" class="textbox" name="target_url" id="target_url" value="" maxlength="255" style="width:400px">
            <br>if you want to make the url like {$http_location}/mumbai-indian-jersey<br> then enter only <b>mumbai-indian-jersey</b>
        </td>
    </tr>
	<tr class="shade2">
        <td class="label">
            Default Image *
        </td>
        <td colspan="2">

              <!-- input id="default_image" name="default_image" type="file" / -->
              <div class="error_div"></div>
              <td ><span id="default_images_preview"></td>
        </td>
        <td></td>
    </tr>
	<!--other images -->
    <tr class="shade1">
        <td class="label">
            Top Image
        </td>
        <td colspan="2">
              <!-- input id="top_image" name="top_image" type="file" / -->
        </td>
        <td></td>
    </tr>
    <tr class="shade2">
        <td class="label">
            Front Image
        </td>
        <td colspan="2">
              <!-- input id="front_image" name="front_image" type="file" onchange="$('#front_image_url').val(this.value);"/ -->
        </td>
        <td></td>
    </tr>
    <tr class="shade1">
        <td class="label">
            Back Image
        </td>
        <td colspan="2">
              <!-- input id="back_image" name="back_image" type="file" / -->
        </td>
        <td></td>
    </tr>
    <tr class="shade2">
        <td class="label">
            Right Image
        </td>
        <td colspan="2">
              <!-- input id="right_image" name="right_image" type="file" / -->
        </td>
        <td></td>
    </tr>
    <tr class="shade1">
        <td class="label">
            Left Image
        </td>
        <td colspan="2">
              <!-- input id="left_image" name="left_image" type="file" / -->
        </td>
        <td></td>
    </tr>
    <tr class="shade2">
        <td class="label">
            Bottom Image
        </td>
        <td colspan="2">
              <!-- input id="bottom_image" name="bottom_image" type="file" / -->
        </td>
        <td></td>
    </tr>
    <!-- othere images ends-->
    <tr class="shade2">
        <td class="label">
            Size Chart Image
        </td>
        <td colspan="2">
              <p >Size Chart Image Name :<input type="text" name="size_chart_image_name" id="size_chart_image_name" value=""></p>
              <input id="size_chart_image" name="size_chart_image" type="file" />
              <input id="size_chart_image_url" name="size_chart_image_url" type="hidden" value="" />
        </td>
        <td class="label"><div style="float:left;padding:20px"><a style="cursor:pointer;text-decoration:underline" onclick="getImagesFromRepo()">Select From Repository</a></div><div style="float:left;" id="size_chart_image_preview"></div>
        <a style="cursor:pointer;float:left;padding:20px 0;display:none" id="size_chart_remove" onclick="$('#size_chart_image_url').val('');$('#size_chart_image_preview').html('');$(this).hide();" style="display:none">remove</a>
        </td>
    </tr>

    <tr class="shade1">
        <td class="label">Whether Customizable</td>
        <td>
         <select name="is_customizable" >
            <option value="0" selected>No</option>
         </select>
         </td>
         <td colspan="2"></td>

    </tr>
	<tr class="shade2">
		<td class="label">Style category map</td>
        <td colspan="9">
		<select name="stylecatmap[]" id="stylecatmap">
			<option></option>
			{section name="subcat" loop=$subcategories}
				{section name="selectedcat" loop=$stylemaparray}
					{if $subcategories[subcat].categoryid == $stylemaparray[selectedcat].categoryid}
						{assign var="selcat" value="selected"}
					{/if}
				{/section}
				<option value="{$subcategories[subcat].categoryid}"  {$selcat}>{$subcategories[subcat].category}</option>
				{assign var="selcat" value=""}
			{/section}
		</select>
	 	</td>
	</tr>
    </div>
    </td></tr>


    <tr class="shade1">
        <td class="label" colspan="2"><input type="submit" value="Add Product"> </td>
    </tr>

</table>
</form>
<script type="text/javascript">
	addOptionsRow();
	disable_inputs('freesize');
</script>
{/if}
<div id="size_chart_images_popup" style="display:none">
	<div class="overlay" style="_position:absolute;_width:1300px;_height:1500px;">&nbsp;</div>
	<div style="left:28%; position:fixed; _position:absolute; height:400px; width:650px; background:white; top:20%; z-index:1000;">
		<div id="" >
			<p style="text-align:right;padding:5px 0px;background-color:#CCCCCC;margin:0px" id="">
				<a onClick="closeDirectoryPopup()" style="cursor:pointer;font-size:13px;font-weight:bold;text-align:right;padding:10px">Close</a>
			</p>
		</div>
		<div class="clear">&nbsp;</div>
		<div id="size_chart_content" style="padding:10px; text-align:left; font-size:12px;overflow-y:auto;height:320px" >
		</div>
	</div>
</div>



{/capture}
<script type="text/javascript" src="{$http_location}/skin1/js_script/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript" src="{$http_location}/skin1/myntra_js/jquery.min.js"></script>
<script type="text/javascript" src="{$http_location}/skin1/js_script/jquery.validate.js"></script>
<link rel="stylesheet" type="text/css" href="{$http_location}/skin1/myntra_css/jquery.autocomplete.css" />

<script type='text/javascript' src='{$http_location}/skin1/myntra_js/jquery.bgiframe.min.js'></script>
<script type='text/javascript' src='{$http_location}/skin1/myntra_js/jquery.ajaxQueue.js'></script>
<script type='text/javascript' src='{$http_location}/skin1/myntra_js/jquery.autocomplete.pack.js'></script>

<script type="text/javascript">
var i=1;
var firstTimePageLoad = true;

</script>
{literal}
<script type="text/javascript">

function showDirectoryPopup()
{
	$("#size_chart_images_popup").show();
}

function closeDirectoryPopup()
{
	$("#size_chart_images_popup").hide();
}

function getImagesFromRepo()
{
	$.get(http_loc+'/directoryReader.php', function(data) {
	  $('#size_chart_content').html(data);
	});
	showDirectoryPopup();
}

function selectImage(url)
{
	$("#size_chart_image_url").val(url);
	var img_html="<img src='"+cdn_base+"/"+url+"' height='50px' width='50px'>";
    $("#size_chart_image_preview").html(img_html);
    $("#size_chart_remove").show();
	closeDirectoryPopup();
}
var sku_code_list="";
var size_option_list="";
function validateOptions()
{
	if(applySizeScaleRestrictionPageMode)
	{
		if($("#attr_type_size_chart_scale").val() =='')
		{
			alert("Please select a Scale Type");
			return false;
		}
	}
	sku_code_list="";
	size_option_list="";
	if($("#size_option_check").attr("checked"))
	{
		var serializearray=$("#productstyleaction").serializeArray();
		for(var i=0;i<serializearray.length;i++)
		{
			if(serializearray[i].name.search("sku_code_value")==0)
			{
				if(serializearray[i].value==null || serializearray[i].value.trim()=='')
				{
					alert("SKU code cannot be empty");
					return false;
				}
				else
				{
					sku_code_list+=" "+serializearray[i].value.trim();
				}
			}
			else if(serializearray[i].name.search("size_option_value")==0)
			{
				if(serializearray[i].value==null || serializearray[i].value.trim()=='')
				{
					alert("Option value cannot be empty");
					return false;
				}
				else
				{
					size_option_list+=", "+serializearray[i].value.trim();
				}
			}
		}
	}
	else
	{
		if($.trim($("#sku_code_value_freesize").val()) == '')
		{
			alert("SKU code cannot be empty");
					return false;
		}
		else
		{
			sku_code_list=$.trim($("#sku_code_value_freesize").val());
		}
		if($.trim($("#size_option_value_freesize").val()) == '')
		{
			alert("Option value cannot be empty");
			return false;
		}
	}

	return true;
}

function submitForm()
{
	var hasError;
	var stylePrice = $('#price').val();
	var styleStatus = $("#styletype").val();
	$.get(http_loc+"/admin/catalog/styleedit.php?mode=check_skus&query="+sku_code_list+"&style_id="+style_id+"&size_option_list="+size_option_list+"&stylePrice="+stylePrice+"&styleStatus="+styleStatus,function(data){
		if(data==null || data.trim()=='')
		{
			alert('Communication error');
			return;
		}
		if(data.trim()=='1')
		{
			document.getElementById('productstyleaction').submit();
			return;
		}
		 alert(data.trim());
		});
}

function list_skus(box )
{
	var query=document.getElementById(box).value;
	if(query.length!=4)
	{
		return;
	}
	var http;
	if (window.XMLHttpRequest)
  	{
  		http=new XMLHttpRequest();
  	}
	else
  	{
  		http=new ActiveXObject("Microsoft.XMLHTTP");
  	}
	http.onreadystatechange=function(url){
		if (http.readyState==4 && http.status==200)
		{
			if(http.responseText!=null)
			{
				var data=http.responseText.trim().split(',');
				$("#"+box).autocomplete(data);
			}


		}
	};
	var url=http_loc+"/admin/catalog/styleedit.php?";

	url+="mode=list_skus"+"&query="+query;

	http.open("GET",url,true);
	http.send(null);
}


$(document).ready(function() {

$('#global_attr_article_type').focus();

//create the Specific attributes block dynamically
getParents($('#global_attr_article_type').val());

jQuery.validator.addMethod("validateASCII", function(value, element) {
  	return /^[\x00-\x7F]*$/.test(value);
}, element+" contains non-ASCII character(s)");

var validator = $("#productstyleaction").validate({
		  rules: {
		   article_number:
			{
			   required: true ,
			   remote : http_loc+"/admin/catalog/styleedit.php?mode=checkArticleNumber&style_id="+style_id

			},
			global_attr_article_type:
			{
			   required: true
            },
            global_attr_brand:
			{
			   required: true
            },
            global_attr_gender:
			{
			   required: true
            },
            global_attr_base_colour:
			{
			   required: true
            },
            global_attr_fashion_type:
			{
			   required: true
            },
            global_attr_season:
			{
			   required: true
            },
            global_attr_year:
			{
			   required: true
            },
            global_attr_usage:
			{
			   required: true
            },
			product_type:
			{
			   required: true

			},
			article_name:
			{
			   required: true,
			   maxlength: 255
			},
			price:
			{
			   required: true,
			    number:true
			},
			default_image:{
			    required: function(){
				    return $('#default_images_preview').html() == '';
			    }
			},
			product_tags:{
			    required: true
			},
			product_desc: {
				required = true;
			},
			product_desc: {
				validateASCII = true;
			},
			styleNote: {
				validateASCII = true;
			},
			materialsCareDesc: {
				validateASCII = true;
			},
			sizeFitDesc: {
				validateASCII = true;
			}
		  },
		  messages: {
                 article_number:
                {
                   required :"This field is required",
                   remote : "Article number already exists"
                },
                global_attr_article_type:
                {
                   required :"This field is required" 
                },
                global_attr_brand:
                {
                   required :"This field is required"
                },
                global_attr_gender:
                {
                   required :"This field is required"
                },
                global_attr_base_colour:
                {
                   required :"This field is required"
                },
                global_attr_fashion_type:
                {
                   required :"This field is required"
                },
                global_attr_season:
                {
                   required :"This field is required"
                },
                global_attr_year:
                {
                   required :"This field is required"
                },
                global_attr_usage:
                {
                   required: "This field is required"
                },
                article_name:
                {
                   required: "This field is required",
                   maxlength:"Should not be greater than 50 chars"
                },
                product_type:
                {
                   required :"This field is required"
                },
                price:
                {
                   required: "This field is required",
                   number:"Please enter number "
                },
                default_image:{
                    required: "This field is required"
                },
                product_tags:{
                    required: "This field is required"
                }

			  },
	  // the errorPlacement has to take the table layout into account
		errorPlacement: function(error, element)
		{
			if(element.is(":radio"))
			{
				error.appendTo(element.parent().next());
			}
			else if(element.is(":checkbox"))
			{
				error.appendTo(element.next().next());
			}
			else
			{
				error.appendTo(element.next());
			}
			return false;

		},
		// specifying a submitHandler prevents the default submit
		submitHandler: function(form)
		{
            var r=validateOptions()&&confirm("Are you sure, you want to continue");
            if(r==true)
            {
                submitForm();
            }
		},
		// set this class to error-labels to indicate valid fields
		success: function(label)
		{
		  // set &nbsp; as text for IE
		  label.html("&nbsp;").addClass("checked");
        }
	});
});
</script>
{/literal}
{literal}

<script type="text/javascript">

	tinyMCE.init({
    	// General options
    	mode : "textareas",
    	editor_selector : "mceEditor",
    	theme : "advanced",
    	plugins : "safari,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,inlinepopups",
		// Theme options
// Theme options
    	theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull",
    	theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
    	theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
    	theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak",
	    theme_advanced_toolbar_location : "top",
    	theme_advanced_toolbar_align : "left",
       	theme_advanced_statusbar_location : "bottom",
		theme_advanced_resizing : true,
		// Example content CSS (should be your site CSS)

		content_css : "css/content.css",
		// Drop lists for link/image/media/template dialogs

		template_external_list_url : "lists/template_list.js",

		external_link_list_url : "lists/link_list.js",

		external_image_list_url : "lists/image_list.js",

		media_external_list_url : "lists/media_list.js"
		// Replace values for the template plugin
	});



function setProductDisplayName()
{
    var name=$.trim($("#brands").val())+" "+$.trim($("#fit").val())+" "+$.trim($("#article_name").val())+" "+$.trim($("#product_type option:selected").text());
    name=$.trim(name);
    $("#product_display_name").val(name);
    setProductTags();

}
var selected_brand='';
var selected_fit='';
var selected_type='';
function setProductTags()
{
    var current_brand=$.trim($("#brands").val());
    var current_type=$.trim($("#product_type option:selected").text())
    var current_fit=$.trim($("#fit").val())
    var initial_tags=$.trim($("#product_tags").val());
    if(initial_tags != '')
    {
        if(selected_brand =='')
        {
            if(current_brand !='')
                {
                	initial_tags += ","+current_brand;
                }
        }
        else if(current_brand != selected_brand)
        {
           initial_tags=initial_tags.replace(selected_brand, current_brand);
        }
        if(selected_type =='')
        {
            if(current_type !='')
            {
                initial_tags += ","+current_type;
            }
        }
        else if(current_type != selected_type)
        {
           initial_tags=initial_tags.replace(selected_type, current_type);
        }
        if(selected_fit =='')
        {
            if(current_fit !='')
            {
                initial_tags += ","+current_fit;
            }
        }
        else if(current_fit != selected_fit)
        {
           initial_tags=initial_tags.replace(selected_fit, current_fit);
        }
    }
    else{
           initial_tags= current_type;
           if(current_brand != '')
           {
            	initial_tags += ","+current_brand;
           }
           if(current_fit != '')
           {
            	initial_tags += ","+current_fit;
           }


    }
    $("#product_tags").val(initial_tags);
    selected_brand=current_brand;
    selected_fit=current_fit;
    selected_type=current_type;
}
function getParents(id)
{
	if(id=='')
	{
		return;
	}
	$.get(http_loc+'/admin/catalog/styleedit.php',
		 {mode:"getParents", article_id:id, style_id:style_id},
		 function(data)
		 {
			var obj = jQuery.parseJSON(data);
			if(obj.success == 'true')
			{
				$("#global_attr_sub_category_name").val(obj.subcategoryname);
				$("#global_attr_sub_category_id").val(obj.subcategoryid);
				$("#global_attr_master_category_name").val(obj.mastercategoryname);
				$("#global_attr_master_category_id").val(obj.mastercategoryid);
		
			}
			else
			{
				alert("Category level not defined");
		        $("#global_attr_sub_category_name").val('');
		        $("#global_attr_sub_category_id").val('');
		        $("#global_attr_master_category_name").val('');
		        $("#global_attr_master_category_id").val('');
		        $("#global_attr_article_type").val('');
		
		    }
		    
		    //populate size scales
		    if(applySizeScaleRestriction)
		    {
		    	var options = '';
			    if(!obj.size_scales)
			    {
			    	options = '<option value="">-- Not Available --</option>';
			    	scale_size_value = '<option value="">-- Not Available --</option>';
			    	setScaleRestrictionOnOff(false);
			    }
			    else
			    {
			    	setScaleRestrictionOnOff(true);
			    	$("#attr_type_size_chart_scale").show();
			    	options = '<option value="">-- Select --</option>';
			    	scale_size_value = '<option value="">- Select a Scale first -</option>';
			        for(var i = 0; i < obj.size_scales.length; i++)
			        {
			            if(obj.size_scales[i].id == size_chart_scale_id)
			            {
			        		options += '<option value="' + obj.size_scales[i].id + '" selected>' + obj.size_scales[i].scale_name + '</option>';
			            }
			            else
			            {
			            	options += '<option value="' + obj.size_scales[i].id + '">' + obj.size_scales[i].scale_name + '</option>';
			            }
			        }
			    }
			    $("select#attr_type_size_chart_scale").html(options);
			    $(".selected_scale_size_values").html(scale_size_value);
		        if(firstTimePageLoad)
			    {
		        	firstTimePageLoad = false;
			    }
		        else
		        {
				    $(".textbox_size_value").val('');
		        }
		    }
    
		    // clear or show the article type specific table and div
		    $("#article_type_specific_attributes_table").find("tr").remove();
		    if(obj.attributes.length == 0)
		    {
		    	$("#article_type_specific_attributes_div").hide();
				$("#article_type_specific_attributes_table").hide();
		    }
		    else
		    {
		    	$("#article_type_specific_attributes_div").show();
				$("#article_type_specific_attributes_table").show();
		    }
		    
		    // Populate article specific attributes
		    var counter = 1;
		    var tablehtml='';
		    
		    $.each(obj.attributes, function(intIndex,attribute){
		    	if(counter%3 == 1)
		        {
					tablehtml+='<tr class="shade2">';
				}
				
				tablehtml+='<td class="label">';
		        tablehtml+=attribute.name+' : ';
		        tablehtml+='</td>';
		        tablehtml+='<td class="label">';
		       	tablehtml+='<select name="article_type_attribute_id[]">';
		       	tablehtml+='<option value="" >None</option>';
		       	$.each(attribute.allowedValues,function(intValueIndex,valueSelected){
		       		if(valueSelected.selected=='selected')
		           	{
		       			tablehtml+='<option value="'+valueSelected.attribute_value_id+'" SELECTED >'+valueSelected.attribute_name+'</option>';
		       		}
		       		else
		           	{
		       			tablehtml+='<option value="'+valueSelected.attribute_value_id+'" >'+valueSelected.attribute_name+'</option>';
		       		}
       			});
		        tablehtml+='</select>';
		        tablehtml+='</td>';
				
				if(counter%3 == 0)
				{
					tablehtml+='</tr>';
					
					$("#article_type_specific_attributes_table").append(tablehtml);
					tablehtml='';
				}	
				counter++;
    
    		});
    		counter--;
    		if(counter%3 != 0)
		    {
		    	for(var i=3;i>counter%3;i--)
		        {
		    		tablehtml+='<td colspan=2></td>';
		    	}
				tablehtml+='</tr>';
				$("#article_type_specific_attributes_table").append(tablehtml);
		    }
        
		}
	);
}

function setScaleRestrictionOnOff(val)
{
	applySizeScaleRestrictionPageMode = val;
	if(applySizeScaleRestrictionPageMode)
	{
		$("#option_scale_tr").show();
		$(".selected_scale_size_values_td").show();
		$("#appy_scale_mode").val(applySizeScaleRestrictionPageMode);
		$(".textbox_size_value").each(function()
			{
        		$(this).attr("readonly","readonly");
			}
		);
        
        
	}
	else
	{
		$("#option_scale_tr").hide();
		$(".selected_scale_size_values_td").hide();
		$("#appy_scale_mode").val(applySizeScaleRestrictionPageMode);
		$(".textbox_size_value").each(function()
			{
        		$(this).removeAttr("readonly");
        	}
    	);
	}
}

function setBrand(value)
{
    $("#brands").val(value);
        
}

function disable_inputs(id)
{
    if(id=='freesize')
    {
        $("#freesize_table input").each(function(){
        	$(this).removeAttr("disabled");
        });
        $("#sizeoption_table input").each(function(){
        	$(this).attr("disabled","disabled");
        });
        $("#sizeoption_table select").each(function(){
        	$(this).attr("disabled","disabled");
        });
    }
    else
    {
        $("#freesize_table input").each(function(){
        	$(this).attr("disabled","disabled");
        });
        $("#sizeoption_table input").each(function(){
        	$(this).removeAttr("disabled");
        });
        $("#sizeoption_table select").each(function(){
        	$(this).removeAttr("disabled");
        });
    }
}

function checkDiscountRule()
{
	var current_brand=$.trim($("#global_attr_brand").val());
	var current_articletype=$.trim($("#global_attr_article_type").val());
	var current_fashion=$.trim($("#global_attr_fashion_type").val());

	if(current_brand!='' && current_articletype!='' && current_fashion!='') {
		$.get(http_loc+'/admin/catalog/styleedit.php',{mode:"getDiscountRule",brand:current_brand, articletypeid: current_articletype,fashion: current_fashion}, function(data) {
			$("#discountrule").html(data);
		});
	}
}

/**
 * checks the changed status and suggest if it is wrong
 */
function validateNewStatus(oldStatus, newStatus)
{
	var toStatusCode = possibleStatusTrans[oldStatus].to_code;
	if(oldStatus == newStatus)
	{
		return;
	}
	if($.inArray(newStatus, toStatusCode) == -1)
	{
		alert('Invalid status selected. Allowed status from current status are ' + possibleStatusTrans[oldStatus].to_name);
		$("#styletype").val(oldStatus);
	}
	return;
}
</script>
{/literal}
{if $applySizeScaleRestriction}
{literal}
<script>
function getSizeOptionValue(id)
{
	//this function gets the size value corresponding to a scale
	if(id=='')
	{
		scale_size_value = '<option value="">- Select a Scale first -</option>';
		$(".selected_scale_size_values").html(scale_size_value);
        $(".textbox_size_value").val('');
		return;
	}
	$.get(http_loc+'/admin/catalog/styleedit.php',
		 {mode:"getSizeOptionValue", scale_id:id},
		 function(data)
		 {
			 
			 var sizeOptionValues = jQuery.parseJSON(data);
			 if(!sizeOptionValues)
			 {
				scale_size_value = '<option>-- Not Available --</option>';
			 }
			 else
			 {
			     scale_size_value = '<option>-- Select --</option>';
		         for(var i = 0; i < sizeOptionValues.length; i++)
			     {
		        	 scale_size_value += '<option value="' + sizeOptionValues[i].id + '">' + sizeOptionValues[i].size_value + '</option>';
		         }
			 }
			$(".selected_scale_size_values").html(scale_size_value);
		    $(".textbox_size_value").val('');
		}
	);
}

function setTextBoxSizeValue(ele, i)
{
	var selectedScale = $("#attr_type_size_chart_scale option:selected").text();
	var selectedSize = $("#attr_type_size_value\\["+i+"\\] option:selected").text();
	if((selectedScale == "Text Size") || (selectedScale == "Free Size") || (selectedScale == "Number size UK"))
	{
		$("#size_option_value\\["+i+"\\]").val(selectedSize);
	}
	else
	{
		$("#size_option_value\\["+i+"\\]").val(selectedScale + selectedSize);
	}
}
</script>
{/literal}
{/if}
{if $smarty.get.mode eq 'modifystyle'}
{include file="dialog.tpl" title="Modify Product" content=$smarty.capture.dialog extra='width="100%"'}
{else}
{include file="dialog.tpl" title="Add New Product" content=$smarty.capture.dialog extra='width="100%"'}
{/if}
