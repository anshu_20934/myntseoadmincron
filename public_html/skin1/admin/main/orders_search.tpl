{* $Id:orders_search.tpl, v1.00.0.0, 2008/09/23 6:08:00 max Exp $ *}
{include file="main/include_js.tpl" src="main/popup_product.js"}
<script type="text/javascript" src="{$http_location}/skin1/js_script/jquery.js"></script>
<script type="text/javascript" src="{$http_location}/skin1/js_script/thickbox.admin.js"></script>
<link rel="stylesheet" href="{$http_location}/skin1/css/thickbox.css" />
{literal}
<script type="text/javascript">
function getHTTPObject(){
	if(window.XMLHttpRequest){
		http=new XMLHttpRequest();}
	else if(window.ActiveXObject){
		http=new ActiveXObject("Microsoft.XMLHTTP");}
	return http;
}

function load_sources(s_location,s_type){
	var http = new getHTTPObject();
	var sourceURL = http_loc+"/admin/load_sources.php?s_location="+s_location.value+"&s_type="+s_type;
	http.open("GET", sourceURL, true);
	http.onreadystatechange = function(){
    	if(http.readyState == 4){
	 		if(http.status == 200){
	 			var result = http.responseText;
	 			document.getElementById("source_span").innerHTML = "";
            	document.getElementById("source_span").innerHTML = result;            	
            }else{
				alert(http.status+"===="+http.statusText);
			}
    	}
    }
    http.send(null);
}

function printBulkInvoice(){
	var orderids = $("textarea#bulkInvoiceIDs").val();
	orderids = orderids.split('\n');
	if(orderids.length > 50){
	alert("You have exceeded max order limit of 50.");
	return false;
  }
	var validOrderIds = "";
	for(var i in orderids) {
		var orderid = orderids[i].trim();	
		if(orderid.replace(/[\d]/g, '') != "") {
			alert("Invalid separators used for order ids");
			return;
		} else {
			if(orderid != "") {
				validOrderIds += validOrderIds!=""?",":"";
				validOrderIds += orderid;
			}
		}
  	}
  	if(validOrderIds != "") {
		var w = window.open('../order_invoice.php?orderid='+validOrderIds+'&type=admin&copies=1','null','scrollbars=1,width=900,height=900,resizable=1');
  	}
}

function printBulkShippingLabel(){
        var orderids = $("textarea#bulkInvoiceIDs").val();
	orderids = orderids.split('\n');
	if(orderids.length > 50){
	alert("You have exceeded max order limit of 50.");
	return false;
  }
	var validOrderIds = "";
	for(var i in orderids) {
		var orderid = orderids[i].trim();
		if(orderid.replace(/[\d]/g, '') != "") {
			alert("Invalid separators used for order ids");
			return;
		} else {
			if(orderid != "") {
				validOrderIds += validOrderIds!=""?",":"";
				validOrderIds += orderid;
			}
		}
  	}
  	if(validOrderIds != "") {
		var w = window.open('../shipping_label.php?orderid='+validOrderIds+'&type=admin','null','scrollbars=1,width=900,height=900,resizable=1');
  	}
}

function limitLines(obj, e) {
  var keynum;
  // IE
  if(window.event) {
    keynum = e.keyCode;
  // Netscape/Firefox/Opera
  } else if(e.which) {
    keynum = e.which;
  }


  if(keynum == 13) {  
    var orderids = $("textarea#bulkInvoiceIDs").val();
	orderids = orderids.split('\n');
	if(orderids.length > obj.rows){
	alert("You have reached max order limit of 50.");
	return false;
  }
  }
}
</script>
{/literal}
<link rel="stylesheet" href="{$SkinDir}/skin1_admin.css" />
<div style="text-align:right;margin-bottom:10px;"><a href="#TB_inline?height=400&width=300&inlineId=bulkInvoiceForm" class="thickbox" title="Bulk Invoice Printing"> Bulk Invoice/Shipping Label Printing</a></div>
<div id="bulkInvoiceForm" style="display:none;">
  <div style="text-align:left;padding:3px;">
    New Line seperated Order-IDs to print Invoice<br/>
    Max 50 orders at allowed.<br/>
    
    <textarea id="bulkInvoiceIDs" rows="50" style="width:100%;border:1px solid #999;height:300px;margin:4px auto;" onkeydown="return limitLines(this, event)"></textarea><br/><br/>
    <div style="text-align:center;">
      <button onclick="printBulkInvoice();">Print Invoice(s)</button>
      <button onclick="printBulkShippingLabel();">Print Shipping Label(s)</button>
    </div>
  </div>
</div>
{include file="admin/main/order_search_criteria.tpl"}
{if $mode=='search'}
	{capture name=dialog}
		{include file="customer/main/navigation_new.tpl"}
		<form action="" method="post" name="orderlst">
			<input type="hidden" name="mode" />
			<input type="hidden" name="page" id="page" value="{$smarty.get.page}" />
			<input type="hidden" name="query_string" id="query_string" value="{$query_string}" />
			<table cellpadding="2" cellspacing="1" width="100%">
				<tr class="tablehead">
					<th width="5%" nowrap="nowrap">
						<a href="{$navigation_script}&amp;order_by=order_id{if $smarty.get.sort_order=='desc' && $smarty.get.order_by=='order_id'}&amp;sort_order=asc{else}&amp;sort_order=desc{/if}">
							Order Id
							{if $smarty.get.sort_order=='desc' && $smarty.get.order_by=='order_id'}
								<img src="{$cdn_base}/skin1/images/r_bottom.gif" />
								{elseif $smarty.get.sort_order=='asc' && $smarty.get.order_by=='order_id'}
								<img src="{$cdn_base}/skin1/images/r_top.gif"/>
							{else}
							{/if}
						</a>
					</th>
					
					<th width="5%" nowrap="nowrap">
						<a href="{$navigation_script}&amp;order_by=order_name{if $smarty.get.sort_order=='desc' && $smarty.get.order_by=='order_name'}&amp;sort_order=asc{else}&amp;sort_order=desc{/if}">
							Order Name
							{if $smarty.get.sort_order=='desc' && $smarty.get.order_by=='order_name'}
								<img src="{$cdn_base}/skin1/images/r_bottom.gif" />
								{elseif $smarty.get.sort_order=='asc' && $smarty.get.order_by=='order_name'}
								<img src="{$cdn_base}/skin1/images/r_top.gif"/>
							{else}
							{/if}
						</a>
					</th>
					
					<th width="5%" nowrap="nowrap">
						<a href="{$navigation_script}&amp;order_by=order_status{if $smarty.get.sort_order=='desc' && $smarty.get.order_by=='order_status'}&amp;sort_order=asc{else}&amp;sort_order=desc{/if}">
							Order Status
							{if $smarty.get.sort_order=='desc' && $smarty.get.order_by=='order_status'}
								<img src="{$cdn_base}/skin1/images/r_bottom.gif" />
								{elseif $smarty.get.sort_order=='asc' && $smarty.get.order_by=='order_status'}
								<img src="{$cdn_base}/skin1/images/r_top.gif"/>
							{else}
							{/if}
						</a>
					</th>
					<th width="5%" nowrap="nowrap">
						<a href="{$navigation_script}&amp;order_by=customer{if $smarty.get.sort_order=='desc' && $smarty.get.order_by=='customer'}&amp;sort_order=asc{else}&amp;sort_order=desc{/if}">
							Customer
							{if $smarty.get.sort_order=='desc' && $smarty.get.order_by=='customer'}
								<img src="{$cdn_base}/skin1/images/r_bottom.gif" />
								{elseif $smarty.get.sort_order=='asc' && $smarty.get.order_by=='customer'}
								<img src="{$cdn_base}/skin1/images/r_top.gif"/>
							{else}
							{/if}
						</a>
					</th>
					
					<th width="5%" nowrap="nowrap">
						<a href="{$navigation_script}&amp;order_by=source_name{if $smarty.get.sort_order=='desc' && $smarty.get.order_by=='source_name'}&amp;sort_order=asc{else}&amp;sort_order=desc{/if}">
							Source
							{if $smarty.get.sort_order=='desc' && $smarty.get.order_by=='source_name'}
								<img src="{$cdn_base}/skin1/images/r_bottom.gif" />
								{elseif $smarty.get.sort_order=='asc' && $smarty.get.order_by=='source_name'}
								<img src="{$cdn_base}/skin1/images/r_top.gif"/>
							{else}
							{/if}
						</a>
					</th>
										
					<th width="5%" nowrap="nowrap">
						<a href="{$navigation_script}&amp;order_by=order_date{if $smarty.get.sort_order=='desc' && $smarty.get.order_by=='order_date'}&amp;sort_order=asc{else}&amp;sort_order=desc{/if}">
							Order Date
							{if $smarty.get.sort_order=='desc' && $smarty.get.order_by=='order_date'}
								<img src="{$cdn_base}/skin1/images/r_bottom.gif" />
								{elseif $smarty.get.sort_order=='asc' && $smarty.get.order_by=='order_date'}
								<img src="{$cdn_base}/skin1/images/r_top.gif"/>
							{else}
							{/if}
						</a>
					</th>
					<th width="5%" nowrap="nowrap">
						<a href="{$navigation_script}&amp;order_by=queued_date{if $smarty.get.sort_order=='desc' && $smarty.get.order_by=='queued_date'}&amp;sort_order=asc{else}&amp;sort_order=desc{/if}">
							Queued Date
							{if $smarty.get.sort_order=='desc' && $smarty.get.order_by=='queued_date'}
								<img src="{$cdn_base}/skin1/images/r_bottom.gif" />
								{elseif $smarty.get.sort_order=='asc' && $smarty.get.order_by=='queued_date'}
								<img src="{$cdn_base}/skin1/images/r_top.gif"/>
							{else}
							{/if}
						</a>
					</th>
					<th width="5%" nowrap="nowrap" align="right">
						<a href="{$navigation_script}&amp;order_by=total_amount{if $smarty.get.sort_order=='desc' && $smarty.get.order_by=='total_amount'}&amp;sort_order=asc{else}&amp;sort_order=desc{/if}">
							Total Amount
							{if $smarty.get.sort_order=='desc' && $smarty.get.order_by=='total_amount'}
								<img src="{$cdn_base}/skin1/images/r_bottom.gif" />
								{elseif $smarty.get.sort_order=='asc' && $smarty.get.order_by=='total_amount'}
								<img src="{$cdn_base}/skin1/images/r_top.gif"/>
							{else}
							{/if}
						</a>
					</th>
				</tr>
				{if $orderresult|@count > 0 }
					{section name=order loop=$orderresult}
					
							{if $orderresult[order].payment_method == 'cod'}
							<tr class="{cycle values=",TableSubHead"}" style="background-color:#FFCC33">
							{else} 
							<tr class="{cycle values=",TableSubHead"}">
							{/if}
							<td align="center">
								{if $orderresult[order].is_customizable eq 0}<span style="background-color:red;font-weight:bold;padding-left:4px;padding-right:4px;">NP</span>{/if} 
								<a href="order.php?orderid={$orderresult[order].order_id}">#{$orderresult[order].order_id}</a>
							</td>
							<td align="center">
								<a href="order.php?orderid={$orderresult[order].order_id}">{$orderresult[order].order_name}</a>
							</td>
							<td align="center">
								{include file="main/order_status.tpl" mode="static" status=$orderresult[order].order_status}
							</td>
							<td align="center">
								{$orderresult[order].customer}
							</td>
							<td align="center">
								{$orderresult[order].source_name}
							</td>							
							<td align="center">
								{$orderresult[order].order_date}
							</td>
							<td align="center">
								{$orderresult[order].order_queueddate}
							</td>
							<td align="right">
								Rs.{$orderresult[order].order_total_amount|abs_value|formatprice}
							</td>
						</tr>
					{/section}
				{/if}
			</table>
		</form>
		{include file="customer/main/navigation_new.tpl"}

	{/capture}
	{include file="dialog.tpl" title="Order Results" content=$smarty.capture.dialog extra='width="100%"'}
{/if}
