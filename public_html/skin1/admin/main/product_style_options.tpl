{* $Id: featured_products.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
{*include file="main/include_js.tpl" src="main/popup_product.js"*}

<a name="featured" />

{$lng.txt_featured_products}

<br /><br />

{capture name=dialog}



<form action="productstyle_action.php" method="post" name="optionsform" >
<input type="hidden" name="mode"  />
<input type="hidden" name="styleid" value="{$styleid}" />

<table cellpadding="3" cellspacing="1" width="100%">
{if $options  && $mode neq 'modifyoption' }
<tr class="TableHead">
	<td width="10">&nbsp;</td>
	<td width="10%">{$lng.lbl_option_name}</td>
	<td width="10%">{$lng.lbl_option_value}</td>
	<td width="10%">{$lng.lbl_default}</td>
	<td width="45%" align="center">{$lng.lbl_pos}</td>
	<td width="15%" align="center">{$lng.lbl_active}</td>
</tr>



{section name=num loop=$options}

<tr{cycle values=", class='TableSubHead'"}>
	<td align="center"><input type="checkbox" name="posted_data[{$options[num].id}][to_delete]"  /></td>
	<td align="center"><b>{$options[num].name}</b></td>
	<td align="center"><b>{$options[num].value}</b></td>
	<td align="center"><input type="checkbox" name="posted_data[{$options[num].id}][default]"  {if $options[num].default_option eq "Y"} checked {/if} /></td>
	<td align="center">
	<span style="float:left;margin:4px">Chest <br/>
	    <input type="text" name="posted_data[{$options[num].id}][chest]" size="5" value="{$options[num].product_option_details.chest}" />
	</span>
	<span style="float:left;margin:4px">Full Length <br/>
	<input type="text" name="posted_data[{$options[num].id}][full_length]" size="5" value="{$options[num].product_option_details.full_length}" />
	</span>
	<span style="float:left;margin:4px">Sleeve Lenght <br/>
	<input type="text" name="posted_data[{$options[num].id}][sleeve_l]" size="5" value="{$options[num].product_option_details.sleeve_l}" />
	</span>
	<span style="float:left;margin:4px">Sleeve width <br/>
	<input type="text" name="posted_data[{$options[num].id}][sleeve_w]" size="5" value="{$options[num].product_option_details.sleeve_w}" />
	</span>
	<span style="float:left;margin:4px">Shoulder<br/>
	<input type="text" name="posted_data[{$options[num].id}][shoulder]" size="5" value="{$options[num].product_option_details.shoulder}" />
	</span>


	</td>
	<td align="center">
	<select  name="posted_data[{$options[num].id}][is_active]">
	           <option value="1" {if $options[num].is_active eq "1"} selected {/if}>Yes</option>
		   <option value="0" {if $options[num].is_active eq "0"} selected {/if}>No</option>
        </select> 
			   
       </td>
</tr>

{/section}

<tr>
	<td colspan="6" class="SubmitBox">
	<input type="button" value="{$lng.lbl_delete_selected|escape}" onclick="javascript: document.optionsform.mode.value = 'delopt'; document.optionsform.submit();" />
	<input type="submit" value="{$lng.lbl_update|escape}" onclick="javascript: document.optionsform.mode.value = 'updopt'; document.optionsform.submit();"/>
	<input type="button" value="{$lng.lbl_modify_selected|escape}" onclick="javascript: document.optionsform.mode.value = 'modifyoption'; document.optionsform.submit();" />
	<input type="button" value="add addons" onclick="javascript: document.optionsform.mode.value = 'addons'; document.optionsform.submit();" />
	
	</td>
	</td>
</tr>


{else}

<!--<tr>
<td colspan="6" align="center">{$lng.txt_no_featured_products}</td>
</tr> -->

{/if}



{if $mode neq 'modifyoption'}
<tr>
<td colspan="6"><br /><br />{include file="main/subheader.tpl" title=$lng.lbl_option_event}</td>
</tr>

<tr>
	<td align="right"  ><b>Option Name</b></td>
	<td align="left"><input type="text" name="option_name" size="30" /><font color="red">{$errMsg}</font></td>
	
	<td align="right"><b>Option Value</b></td>
	<td align="left"><input type="text" name="option_value" size="30" /><font color="red">{$errMsg}</font></td>
	
	<td align="right"><b>Price</b></td>
	<td align="left"><input type="text" name="price" size="30" /><font color="red">{$errMsg}</font></td>
	
</tr>

<tr>
	
	<td align="left"><b>Image</b></td>
	<td align="left"><input type="file" name="optimage" /></td>
	
		
	<td align="right"><b>Description</b></td>
	<td align="left"><textarea name="desc" cols="40" rows="2"></textarea></td>
	
	
	
</tr>
<tr>
    <td colspan="6">
            <span style="float:left;margin:4px">Chest <input type="text" name="chest" size="5" value="" />
        	</span>
        	<span style="float:left;margin:4px">Full Length <input type="text" name="full_length" size="5" value="" />
        	</span>
        	<span style="float:left;margin:4px">Sleeve Length <input type="text" name="sleeve_l" size="5" value="" />
        	</span>
        	<span style="float:left;margin:4px">Sleeve width  <input type="text" name="sleeve_w" size="5" value="" />
        	</span>
        	<span style="float:left;margin:4px">Shoulder <input type="text" name="shoulder" size="5" value="" />
        	</span>

    </td>
</tr>

<tr>
	<td colspan="6" class="SubmitBox">
	<input type="submit" value="{$lng.lbl_add_new_|escape}" onclick="javascript: document.optionsform.mode.value = 'addopt'; document.optionsform.submit();"/>
	</td>
</tr>
{/if}
{if $mode eq 'modifyoption'}
{section name=num loop=$options}
  <input type="hidden" name="optionid" value="{$options[num].id}"  />
  <tr>
	<td colspan="6"><br /><br />{include file="main/subheader.tpl" title='Edit Option'}</td>
</tr>
   <tr>
	<td align="right"  ><b>Option Name</b></td>
	<td align="left"><input type="text" name="option_name" size="30" value="{$options[num].name}" /><font color="red">{$errMsg}</font></td>
	
	<td align="right"><b>Option Value</b></td>
	<td align="left"><input type="text" name="option_value" size="30" value="{$options[num].value}" /><font color="red">{$errMsg}</font></td>
	
	<td align="right"><b>Price</b></td>
	<td align="left"><input type="text" name="price" size="30" value="{$options[num].price}" /><font color="red">{$errMsg}</font></td>
	
</tr>


<tr>
	<td colspan="6" class="SubmitBox">
	<input type="submit" value="{$lng.lbl_update|escape}" onclick="javascript: document.optionsform.mode.value = 'updateoption'; document.optionsform.submit();"/>
	</td>
</tr>
{/section}


{/if}

</table>
</form>

{/capture}
{include file="dialog.tpl" title=$lng.lbl_productstyle_options content=$smarty.capture.dialog extra='width="100%"'}