{* $Id: featured_products.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
{include file="main/include_js.tpl" src="main/popup_product.js"}

<a name="featured" />

{$lng.txt_featured_products}

<br /><br />
<script type="text/javascript" src="{$http_location}/skin1/js_script/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
{literal}

<script type="text/javascript">

	tinyMCE.init({

		// General options

		mode : "textareas",

		editor_selector : "mceEditor",
		
		theme : "advanced",

		plugins : "safari,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,inlinepopups",



		// Theme options

// Theme options

		theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect",

		theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",

		theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",

		theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak",




		theme_advanced_toolbar_location : "top",

		theme_advanced_toolbar_align : "left",

		theme_advanced_statusbar_location : "bottom",

		theme_advanced_resizing : true,



		// Example content CSS (should be your site CSS)

		content_css : "css/content.css",



		// Drop lists for link/image/media/template dialogs

		template_external_list_url : "lists/template_list.js",

		external_link_list_url : "lists/link_list.js",

		external_image_list_url : "lists/image_list.js",

		media_external_list_url : "lists/media_list.js",



		// Replace values for the template plugin



	});


</script>
{/literal}
{capture name=dialog}
<form action="productstyle_action.php" method="post" name="producttypeaction" enctype="multipart/form-data">
<input type="hidden" name="mode"  />
<input type="hidden" name="styleid" value="{$styleid}" />

<table cellpadding="3" cellspacing="1" border="0" width="100%">

{if $products}
{section name=prod_num loop=$products}
     <tr>
         <td><b>{$lng.lbl_type_name} : </b></td>
         <td><input type="text" name="type_name" size="50" value="{$products[prod_num].name}" /></td>
        
	 <td><b>{$lng.lbl_type_label} : </b></td>
         <td><input type="text" name="type_label" size="50" value="{$products[prod_num].label}" /></td>
       
    </tr>
    <tr>
         <td><b>{$lng.lbl_type_thumbimg} : </b></td>
         <td>
	     {assign var="thumbnailexist" value="false"}
	     {section name=img_num loop=$imagepath}
	   
		 {assign var="imageid" value=$imagepath[img_num].id}
		 {assign var="imagetype" value=$imagepath[img_num].type}
		{if $imagepath[img_num].type eq "T" && $imagepath[img_num].image_path neq ""  &&  $imagepath[img_num].isdefault eq "Y"} 
		 {assign var="thumbnailexist" value="true"}
			<img src="{$cdn_base}/{$imagepath[img_num].image_path}" height="50px" width="50px"  alt="thumbnail"/>
	          {/if} 
	    
	        {if $imagetype eq "T" && $imagepath[img_num].isdefault eq 'Y'}
				 &nbsp;&nbsp;<a href="javascript:uploadWindow('thstyle','{$styleid}','{$imageid}');"   valign="top">Change</a></td>
		{/if}
	       
	  {/section}
	 {if $thumbnailexist eq "false"}
	    
			<img id="{$idtag}" src="{$xcart_web_dir}/image.php?type={$type}&amp;id={$id}&amp;ts={$smarty.now}{if $already_loaded}&amp;tmp=Y{/if}"{if $image_x ne 0} width="{$image_x}"{/if}{if $image_y ne 0} height="{$image_y}"{/if} alt="{include file="main/image_property.tpl"}"/>
	
                        &nbsp;&nbsp;<a href="javascript:uploadWin('thstyle','{$styleid}');"   valign="top">Change</a></td>
	 {/if}
	 
          </td>
	 <td><b>{$lng.lbl_type_detailimage} : </b></td>
	    {assign var="detailimageexists" value="false"}
	    <td>
	   {section name=img_num loop=$imagepath}
	     {assign var="imageid" value=$imagepath[img_num].id}
	     {assign var="imagetype" value=$imagepath[img_num].type}
	    {if $imagepath[img_num].type eq "D" && $imagepath[img_num].image_path neq ""  && $imagepath[img_num].isdefault eq "Y" } 
	      {assign var="detailimageexists" value="true"} 
		<img src="../{$imagepath[img_num].image_path}" height="50px" width="50px" />
               {/if} 
	    {if $imagetype eq "D" && $imagepath[img_num].isdefault eq 'Y'}
             &nbsp;&nbsp;<a href="javascript:uploadWindow('detstyle','{$styleid}','{$imageid}');"   valign="top">Change</a>
	  {/if}
	
	  {/section}
	
	{if $detailimageexists eq "false"}
  
		<img id="{$idtag}" src="{$xcart_web_dir}/image.php?type={$type}&amp;id={$id}&amp;ts={$smarty.now}{if $already_loaded}&amp;tmp=Y{/if}"{if $image_x ne 0} width="{$image_x}"{/if}{if $image_y ne 0} height="{$image_y}"{/if} alt="{include file="main/image_property.tpl"}"/>
	
             &nbsp;&nbsp;<a href="javascript:uploadWin('detstyle','{$styleid}');"   valign="top">Change</a>
	  {/if}
	   </td>
    </tr>
    <tr>
          <td><b>Style Code: </b></td>
         <td><input type="text" name="style_code" size="10" value="{$products[prod_num].style_code}" /></td>
        <td ><b>{$lng.lbl_producttype}:</b></td>
	<td>
	    <select name='prd_type'>
                {section name=num loop=$ddprducttypes}
		   <option value="{$ddprducttypes[num].id}" {if $ddprducttypes[num].id == $products[prod_num].product_type} selected='selected' {/if}>{$ddprducttypes[num].name}</option>
		{/section}
           </select>
	</td>
    </tr>
    
    <tr>
         <td><b>{$lng.lbl_price}(Rs) : </b></td>
         <td><input type="text" name="price" size="30" value="{$products[prod_num].price}" /><font color="red" >{$errMsg}</font></td>
         <td valign="middle"><b>{$lng.lbl_type_description} : </b></td>
         <td><textarea name="desc" cols="50" rows="5" class="mceEditor" >{$products[prod_num].description}</textarea></td>
    </tr>

    <tr>
         <td><b>Markdown Percentage(Rs) : </b></td>
         <td><input type="text" name="store_markdown" size="30" value="{$products[prod_num].default_store_markdown}" /><font color="red" >{$errMsg}</font></td>
         <td><b>Store rate(Rs) : </b></td>
         <td><input type="text" name="store_rate" size="30" value="{$products[prod_num].default_store_rate}" /><font color="red" >{$errMsg}</font></td>
    </tr>

    <tr>
         
	<td><b><b>{$lng.lbl_inventory}</td>
	 <td><input type="text" name="inventory_count" size="20"  value="{$products[prod_num].iventory_count}" /></td>
	 <td><b><b>{$lng.lbl_icon_image}</td>
	 <td>
	 {assign var="iconimageexists" value="false"}
	   {section name=img_num loop=$imagepath}
	     {assign var="imageid" value=$imagepath[img_num].id}
	      {assign var="imagetype" value=$imagepath[img_num].type}
	    {if $imagepath[img_num].type eq "I" && $imagepath[img_num].image_path neq "" } 
	    {assign var="iconimageexists" value="true"}
		<img src="../{$imagepath[img_num].image_path}"  name="icon_image" height="30px" width="30px" />
             {/if} 
	         
              
	     {if $imagetype eq 'I'}
             
		&nbsp;&nbsp;<a href="javascript:uploadWindow('iconstyle','{$styleid}','{$imageid}');"   valign="top">Change</a>
	     
	      {/if}
	     {/section}

	     {if $iconimageexists eq "false" }
		<img id="{$idtag}" src="{$xcart_web_dir}/image.php?type={$type}&amp;id={$id}&amp;ts={$smarty.now}{if $already_loaded}&amp;tmp=Y{/if}"{if $image_x ne 0} width="{$image_x}"{/if}{if $image_y ne 0} height="{$image_y}"{/if} alt="{include file="main/image_property.tpl"}"/>
	   
	      &nbsp;&nbsp;<a href="javascript:uploadWin('iconstyle',{$styleid});"   valign="top">Change</a>
	      {/if}
	     </td>   
    </tr>
    <tr>

    <td><b>Material : </b></td>
    <td><textarea name="material" cols="50" rows="5">{$products[prod_num].material}</textarea></td>
    </tr>
    <tr>
         <td><b>Product Style type</b></td>
         <td>
		<select name="pstyletype" id="pstyletype">
			<option value="P" {if $products[prod_num].styletype == 'P'} selected {/if}>Public</option>
			<option value="A" {if $products[prod_num].styletype == 'A'} selected {/if}>Affiliate</option>
			<option value="POS" {if $products[prod_num].styletype == 'POS'} selected {/if}>POS</option>
			
		</select>
	 </td>

	<td>
	</td>
    </tr>

    <tr>
	<td><b>Style category map</b></td>
         <td>
		<select name="stylecatmap[]" id="stylecatmap">
			<option></option>
			{section name="subcat" loop=$subcategories}
				{section name="selectedcat" loop=$stylemaparray}
					{if $subcategories[subcat].categoryid == $stylemaparray[selectedcat].categoryid}
						{assign var="selcat" value="selected"}
					{/if}
				{/section}
				<option value="{$subcategories[subcat].categoryid}"  {$selcat}>{$subcategories[subcat].category}</option>
					{assign var="selcat" value=""}
			{/section}
		</select>
	 </td>
         <td><b>Bulk Availability</b></td>
         <td>
		<INPUT TYPE="radio" NAME="bulk" ID="bulkyes" value="Y" {if $products[prod_num].bulk =='Y'} checked {/if}>&nbsp;Yes
		<INPUT TYPE="radio" NAME="bulk" ID="bulkno" value="N" {if $products[prod_num].bulk =='N'} checked {/if}>&nbsp;No
	 </td>
   </tr>

    
   <tr>
   <td>
   {section name=img_num loop=$imagepath}
             {assign var="imageid" value=$imagepath[img_num].id}
	      
	    {if $imagepath[img_num].type eq "O" && $imagepath[img_num].image_path neq ""  && $imagepath[img_num].isdefault eq "Y"} 
	    
		<img src="../{$imagepath[img_num].image_path}" height="50px" width="50px" />
            {/if} 
	 {/section}
	 </td>
	
         <td colspan="2"><b><a href="javascript:uploadWin('otherimg',{$styleid});"   valign="top">upload more images</a></td>
	 
         
    </tr>
     <tr>
        <td>
           Is Customizable
        </td>
        <td>
            <select name="is_customizable">
            <option value="1" {if $products[prod_num].is_customizable eq '1'}selected{/if}>Yes</option>
            <option value="0" {if $products[prod_num].is_customizable eq '0'}selected{/if}>No</option>
            </select>
        </td>
     </tr>
{/section}

{else}
 

       <tr>
         <td><b>{$lng.lbl_type_name} : </b></td>
         <td><input type="text" name="type_name" size="30"  /><font color="red" >{$errMsg}</font></td>
        
	 <td><b>{$lng.lbl_type_label} : </b></td>
         <td><input type="text" name="type_label" size="30"  /><font color="red" >{$errMsg}</font>
         
         <b>Style Code: </b>
         <input type="text" name="style_code" size="10" /><font color="red" >{$errMsg}</td>
       
    </tr>
    

    <tr>
         <td><b>{$lng.lbl_type_thumbimg} : </b></td>
         <td><input type="file" name="th_img" ></td>

	 <td><b>{$lng.lbl_type_detailimage} : </b></td>
         <td><input type="file" name="det_img" > </td>
    </tr>
     
   

    <tr>
         <td><b>{$lng.lbl_price}(Rs) : </b></td>
         <td><input type="text" name="price" size="20"  /><font color="red" >{$errMsg}</font></td>


        <td ><b>{$lng.lbl_producttype}:</b></td>
	<td>
	    <select name='prd_type'>
	       
                {section name=num loop=$ddprducttypes}
		   <option value="{$ddprducttypes[num].id}" >{$ddprducttypes[num].name}</option>
		{/section}
               
           </select>
	</td>
	
    </tr>
    
    <tr>
         <td><b>Markdown Percentage(Rs) : </b></td>
         <td><input type="text" name="store_markdown" size="30" /><font color="red" >{$errMsg}</font></td>
         <td><b>Store rate(Rs) : </b></td>
         <td><input type="text" name="store_rate" size="30" /><font color="red" >{$errMsg}</font></td>
    </tr>

    <tr>
	      <td><b>{$lng.lbl_inventory}</b></td>
	      <td><input type="text" name="inventory_count" size="20"  /><font color="red" >{$errMsg}</td>

	     <td valign="middle"><b>{$lng.lbl_type_description} : </b></td>
	     <td><textarea name="desc" cols="50" rows="5"></textarea></td>
    
             
    </tr>

    <tr>
	      <td><b>{$lng.lbl_icon_image}</td>
	      <td><input type="file" name="icon_image"   /></td>

	    <td valign="middle"><b>Material : </b></td>
	     <td><textarea name="material" cols="50" rows="5"></textarea></td>
	     <td>&nbsp;</td>
    
             
    </tr>
<tr>
        <td><b>Product Style type</b></td>
        <td>
			<select name="pstyletype" id="pstyletype">
				<option value="P">Public</option>
				<option value="A">Affiliate</option>
				<option value="POS">POS</option>
			</select>
		</td>
    </tr>

    <tr>
	<td><b>Style category map</b></td>
         <td>
		<select name="stylecatmap[]" id="stylecatmap">
			<option></option>
			{section name="subcat" loop=$subcategories}
				<option value="{$subcategories[subcat].categoryid}">{$subcategories[subcat].category}</option>
			{/section}
		</select>
	 </td>
         <td><b>Bulk Availability</b></td>
         <td>
		<INPUT TYPE="radio" NAME="bulk" ID="bulkyes" value="Y">&nbsp;Yes
		<INPUT TYPE="radio" NAME="bulk" ID="bulkno" value="N" checked>&nbsp;No
	 </td>
 </tr>
<tr>
        <td>
           Is Customizable
        </td>
        <td>
            <select name="is_customizable">
            <option value="1" >Yes</option>
            <option value="0" >No</option>
            </select>
        </td>
     </tr>

  
{/if}
 <tr>
     <td>
		size chart
	 </td>
      <td>
      {if $styleid neq ''}
      {if $sizechart_img neq ''}
  
	<img src="../{$sizechart_img}" height="50px" width="50px" />
	&nbsp;&nbsp;<a href="javascript:uploadWindow('sizechart','{$styleid}','{$imageid}');"   valign="top">change sizechart</a>
	{else}
	<a href="javascript:uploadWindow('sizechart','{$styleid}','{$imageid}');"   valign="top">upload sizechart</a>
	  {/if}
	  {/if}
	 </td>
 </tr>
 <!-- adding for new sports merchandize -->
 <tr>
    <td>
       Product Sku Id
    </td>
    <td>
        <input type="text" name="product_sku_id" value="{if $style_properties.product_sku_id} {$style_properties.product_sku_id}{/if}">
    </td>
   <td>
       Product tags
    </td>
    <td>
        <input type="text" name="product_tags" value="{if $style_properties.product_tags} {$style_properties.product_tags}{/if}">
    </td>
         
 </tr>
 <tr>
     <td>
        Product Display Name
     </td>
     <td>
         <input type="text" name="product_display_name" value="{if $style_properties.product_display_name} {$style_properties.product_display_name}{/if}">
     </td>
    <td>
        Variant Name
     </td>
     <td>
         <input type="text" name="variant_name" value="{if $style_properties.variant_name} {$style_properties.variant_name}{/if}">
     </td>

  </tr>
  <tr>
      <td>
         Discount Type
      </td>
      <td>
          <select name="discount_type">
            <option value="none">None</option>
            <option value="absolute" {if $style_properties.discount_type && $style_properties.discount_type eq 'absolute'} selected{/if} >Absolute</option>
            <option value="percent" {if $style_properties.discount_type && $style_properties.discount_type eq 'percent'} selected{/if}>Percent</option>
          </select>

      </td>
     <td>
         Discount Value
      </td>
      <td>
          <input type="text" name="discount_value" value="{if $style_properties.discount_value} {$style_properties.discount_value}{/if}">
      </td>

   </tr>
   <tr>
      <td>
         Myntra Rating
      </td>
      <td>
          <select name="myntra_rating">
          <option value="none">None</option>
            {section name=foo start=0 loop=11 step=1}
                <option value="{$smarty.section.foo.index}" {if $style_properties.myntra_rating eq $smarty.section.foo.index}selected{/if} >{$smarty.section.foo.index}</option>
            {/section}
          </select>

      </td>
      <td>
         Product Information
      </td>
      <td>
          <textarea name="product_information">{if $style_properties.product_display_name} {$style_properties.product_display_name}{/if}</textarea>
      </td>

   </tr>

   <tr>
        <td>
         Default Image
        </td>
        <td>
            <img src="{if $style_properties.default_image} {$cdn_base}/{$style_properties.default_image}{/if}" alt="" height="200px" width="200px">
        </td>
        <td>
            Change Default Image 
        </td>
        <td>
          <input type="file" name="default_image" value="">
		(Please upload an image of size more than  1170 x 900)
		<br>{if $style_properties.default_image} Delete : <input type="checkbox" value="1" name="delete_default_image" >{/if}
        </td>

   </tr>
   <tr>
           <td>
            Front Image
           </td>
           <td>
               <img src="{if $style_properties.front_image} {$cdn_base}/{$style_properties.front_image}{/if}" alt="" height="200px" width="200px">
           </td>
           <td>
               Change Front Image
           </td>
           <td>
             <input type="file" name="front_image" value="">
		    (Please upload an image of size more than  1170 x 900)
		    <br>{if $style_properties.front_image} Delete : <input type="checkbox" value="1" name="delete_front_image" >{/if}
           </td>

      </tr>
<tr>
        <td>
         Left Image
        </td>
        <td>
            <img src="{if $style_properties.left_image} {$cdn_base}/{$style_properties.left_image}{/if}" alt="" height="200px" width="200px">
        </td>
        <td>
            Change Left Image
        </td>
        <td>
          <input type="file" name="left_image" value="">
		(Please upload an image of size more than  1170 x 900)
		<br>{if $style_properties.left_image} Delete : <input type="checkbox" value="1" name="delete_left_image" >{/if}
        </td>

   </tr>
<tr>
        <td>
         Right Image
        </td>
        <td>
            <img src="{if $style_properties.right_image} {$cdn_base}/{$style_properties.right_image}{/if}" alt="" height="200px" width="200px">
        </td>
        <td>
            Change Right Image
        </td>
        <td>
          <input type="file" name="right_image" value="">
		(Please upload an image of size more than  1170 x 900)
		<br>{if $style_properties.right_image} Delete : <input type="checkbox" value="1" name="delete_right_image" >{/if}
        </td>

   </tr>
<tr>
        <td>
         Back Image
        </td>
        <td>
            <img src="{if $style_properties.back_image} {$cdn_base}/{$style_properties.back_image}{/if}" alt="" height="200px" width="200px">
        </td>
        <td>
            Change Back Image
        </td>
        <td>
          <input type="file" name="back_image" value="">
		(Please upload an image of size more than  1170 x 900)
		<br>{if $style_properties.back_image} Delete : <input type="checkbox" value="1" name="delete_back_image" >{/if}
        </td>

   </tr>
<tr>
        <td>
         Top Image
        </td>
        <td>
            <img src="{if $style_properties.top_image} {$cdn_base}/{$style_properties.top_image}{/if}" alt="" height="200px" width="200px">
        </td>
        <td>
            Change Top Image
        </td>
        <td>
          <input type="file" name="top_image" value="">
		(Please upload an image of size more than  1170 x 900)
		<br>{if $style_properties.top_image} Delete : <input type="checkbox" value="1" name="delete_top_image" >{/if}
        </td>

   </tr>
<tr>
        <td>
         Search Image
        </td>
        <td>
            <img src="{if $style_properties.search_image} {$cdn_base}/{$style_properties.search_image}{/if}" alt="" height="200px" width="200px">
        </td>
        <td>
            Change Search Image 
        </td>
        <td>
          <input type="file" name="search_image" value="">
		(Please upload an image of size 500 x 500)
        </td>

   </tr>
   <tr>
        <td>Apply Target Url</td>
        <td></td>
        <td>Target Url</td>
        <td> <input style="width:500px" type="text" name="target_url" value="{if $style_properties.target_url} {$style_properties.target_url}{/if}"><br/>
        
        </td>
   </tr>

	<tr><td colspan=4>This field is basically for jerseys ,if url is like {$http_location}/manchester-united-jersey then enter here only <b>manchester-united-jersey</b></td></tr>
	<tr><td>&nbsp;</td></tr>
	<tr><td>&nbsp;</td></tr>
	<!-- new filters -->
	{foreach from=$allfilters key=group item=filters}
    {assign var=counter value=0}
    <tr class="shade2">
        <td class="label">
            {$group}
        </td>

        <td colspan="3">
            <select name="{$group|replace:" ":""|replace:"/":""|lower}_filters[]" id="{$group|lower}" {if $group eq 'Brands' || $group eq 'Fit'}{else}multiple="multiple" size="10"{/if} style="width:160px">

                <option value="">None</option>
            {foreach from=$filters key=k item=filter}
                {assign var=is_selected value="notselected"}
                {if $selected_filters}
                    {section name=inner loop=$selected_filters}
                        {if $selected_filters[inner] eq $filter.filter_name}
                            {assign var=is_selected value="selected"}
                        {/if}
                    {/section}
                {/if}

                <option {if $is_selected eq 'selected'}selected{/if} value="{$filter.filter_name}">{$filter.filter_name}</option>
            {/foreach}
            </select>
        </td>

    </tr>
    {/foreach}
	<!-- ends -->
	

 

<tr>
	<td colspan="2" class="SubmitBox">
	{if $products}
            <input type="button" value="{$lng.lbl_update|escape}" onclick="javascript: submitForm(this, 'updatetype');" />
	{else}
             <input type="button" value="{$lng.lbl_save|escape}" onclick="javascript: submitForm(this, 'addtype');" />
	{/if}
	

	<input type="reset" value="{$lng.lbl_cancel|escape}"   />
	</td>
	
</tr>



 <tr>
<td colspan="2"><br /><br /></td>
</tr>



</table>
</form>

{/capture}
{include file="dialog.tpl" title=$lng.lbl_productstyle_heading content=$smarty.capture.dialog extra='width="100%"'}
<br /><br />
{include file="admin/main/product_style_options.tpl"}
<br /><br />
{include file="admin/main/product_style_custarea.tpl"}

{literal}
<script type="text/javascript">
function showFilterNames(filternames) {
	var names = '';
	for(var i=0; i<filternames.length; i++) {
		names = names + filternames[i] + ' . ';
	}
	document.getElementById('selectedFilters').innerHTML = 	names;
}
</script>
{/literal}
<script type="text/javascript" src="{$http_location}/skin1/js_script/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="{$http_location}/skin1/js_script/multipleselect.js"></script>
