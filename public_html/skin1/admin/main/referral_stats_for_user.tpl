<html>
<title>
</title>
<head>
{literal}
<style>
body{font-family: inherit;}
.title{font-size: 14px;
    font-weight: bold;
    padding: 10px;}
.line{ background: url("http://myntra.myntassets.com/skin1/myntra_images/line.png") repeat-x scroll 0 30px transparent;}
.user-details{margin:15px}
.coupon-details{margin:15px}
.mobile-verification{margin:15px}
.user-details .detail{padding:5px}
.user-details label{font-weight:bold;padding:0 5px}
table.my-c-table {
    border: 1px solid #CCCCCC;
    padding: 1px;
}
table.my-c-table th {
    background: none repeat scroll 0 0 #CCCCCC;
    text-align: center;
	padding:5px;
}
table.my-c-table td {
    border-right: 1px solid #999999;
	padding:5px;
}
table.my-c-table td.td-rs {
    text-align: right;
}
table.my-c-table td.td-last {
    border-right: 0 none;
}
table.my-c-table td.td-cr {
    text-align: center;
}
table.my-c-table tbody tr.even td {
    background: none repeat scroll 0 0 #EBEBEB;
}

</style>
{/literal}
</head>
<body>
<div style="">
	<div style="margin:0 auto">
		<div class="user-details">
			<div class="title line">User Details</div>
			{if $userProfileData.login}
			<div class="detail"> <label>UserId:</label>{$userProfileData.login}</div>
			<div class="detail"> <label>Date of Joining:</label>{$userProfileData.first_login|date_format:"%e %B, %Y"}</div>
			<div class="detail"> <label>Referred By:</label>
				{if $userProfileData.referer}
					{if $userProfileData.rule_applied eq 'first_login'}
					{$userProfileData.referer}(Referrerl Signin)
					{else}
					{$userProfileData.referer}(Rererral Facebook)
					{/if}
				{else}
					{if $userProfileData.rule_applied eq 'first_fb_login'}
						Direct Facebook
					{else}
						Direct Signin
					{/if}
				{/if}
			</div>
			{else}
				<div class="detail"> <label>No records found for this user</label></div>
			{/if}
		</div>
		<div class="coupon-details">
			<div class="title line">Coupon Details</div>
			<table cellpadding="0" cellspacing="0" width="100%" class=" my-c-table">
		        <tbody>
		                <tr>
			                <th>Coupon name</th>
			                <th>Amount</th>
			                <th>Type</th>
					<th>Created Date</th>
					<th>Details</th>
					<th>Order</th>
        		        </tr>
		                {section name=prod_num loop=$my_coupons}
		                <tr class="{cycle values="odd,even"}">
	                                <td class="td-cr" width="150px">{$my_coupons[prod_num].code|upper}</td>
					<td class="td-cr" width="300px">
					{if $my_coupons[prod_num].couponType eq "absolute"}
                                        <b>Rs. {$my_coupons[prod_num].mrpAmount|round}</b> off
					{elseif $my_coupons[prod_num].couponType eq "percentage"}
					<b>{$my_coupons[prod_num].mrpPercentage}%</b> off
					{elseif $my_coupons[prod_num].couponType eq "dual"}
					<b>{$my_coupons[prod_num].mrpPercentage}%</b> off upto <b>Rs. {$my_coupons[prod_num].mrpAmount|round}</b>
					{/if}
					{if $my_coupons[prod_num].minimum neq '' && $my_coupons[prod_num].minimum gt 0}
					 on a minimum purchase for Rs. {$my_coupons[prod_num].minimum|round}
					{/if}
					</td>
					<td class="td-cr" width="150px">
						{if $my_coupons[prod_num].groupName eq 'FirstLogin'}
							Registration
						{elseif $my_coupons[prod_num].groupName eq 'MrpReferrals' and $my_coupons[prod_num].mrpAmount eq 250}
							Referral Registration
						{elseif $my_coupons[prod_num].groupName eq 'MrpReferrals' and $my_coupons[prod_num].mrpAmount eq 500}
							Referral First Purchase
						{/if}
					</td>
					<td class="td-cr" width="150px">{$my_coupons[prod_num].timeCreated|date_format:"%e %B, %Y"}</td>
		                        <td class="td-cr" width="350px">
						{$my_coupons[prod_num].description}
			                </td>
					<td class="td-last" width="150px">{if $my_coupons[prod_num].orderid}{$my_coupons[prod_num].orderid}{/if}</td>
				</tr>
		                {/section}
			        </tbody>
			</table>

		</div>
		<div class="mobile-verification">
			 <div class="title line">Mobile Verification</div>
			{if $mobileVerificationSuccess}
			<div style="background: none repeat scroll 0 0 #E6EFC2;border-color: #C6D880;color: #264409;padding: 0;">{$mobileVerificationSuccess}</div>
			{/if}
			<form name="verify_mobile_form" id="verify_mobile_form" action="" method="post">
			<input type="hidden" name="mode" value="verify_mobile" >
			<input type="hidden" name="login" value="{$userProfileData.login}" >
				<table cellpadding="0" cellspacing="0" width="100%" class=" my-c-table">
				<tbody>
					<tr>
						<th>Mobile Number</th>
						<th>Date</th>
						<th>Code</th>
						<th>Status</th>
						<th>Verify</th>
				</tr>
				{assign var=is_unverified value=0}
				{assign var=code_empty value=0}
				{section name=prod_num loop=$mobile_mapping}
				
				<tr class="{cycle values="odd,even"}">
						<td class="td-cr" width="150px">{$mobile_mapping[prod_num].mobile}</td>
						<td class="td-cr" width="150px">{if $mobile_mapping[prod_num].added_date}$mobile_mapping[prod_num].added_date|date_format:"%e %B, %Y"}{else}-{/if}</td>
						<td class="td-cr" width="150px">{if $mobile_mapping[prod_num].code}{$mobile_mapping[prod_num].code}{else}-{/if}</td>
						<td class="td-cr" width="150px">
								{if $mobile_mapping[prod_num].status eq 'U'}
										Unverified
								{elseif $mobile_mapping[prod_num].status eq 'V'}
										Verified
								{elseif $mobile_mapping[prod_num].status eq 'O'}
										Deactivated
								{/if}
						</td>
						<td class="td-cr" width="150px">
							{if $mobile_mapping[prod_num].status eq 'U' && $mobile_mapping[prod_num].code neq ''}
								{assign var=code_empty value=1}
								<input type="radio" name="mobile" value="{$mobile_mapping[prod_num].mobile}" {if $is_unverified eq '0'} checked="checked" {/if}>
							{/if}
						</td>
						
				</tr>
				{if $mobile_mapping[prod_num].status eq 'U'}
					{assign var=is_unverified value=1}
				{/if}
				{/section}
				{if $is_unverified eq 1 && $code_empty eq 1}
				<tr class="even" >
					<td class="td-cr" colspan="5"> <input type="submit" name="sub-mobile" value="verify" > </td>
				</tr>
				{/if}
				</tbody>
				</table>
			</form>
			</div>
		</div>	
</div>
</body>
</html>
