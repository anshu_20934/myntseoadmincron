<script type="text/javascript">
    var orderId = "{$orderid}";
    var within30days = '{$within30days}';
    var warehouseid = {$warehouseid};
</script>
{if $action eq 'confirm_return' }
	<div class="section-heading prepend">Return Item from Order: #{$orderid}</div>
	<div class="blue-divider"></div>
	<div class="divider">&nbsp;</div>
	{if $returnable eq 0}
		{if $return_id }
			<div class="notification p5"><img src="/skin1/mkimages/unchecked.gif"> &nbsp; &nbsp; This item has already been returned. <a href="/admin/returns/return_request.php?id={$return_id}">Click here</a> to check your return details.</div>
		{else}
			<div class="notification p5"><img src="/skin1/mkimages/unchecked.gif"> &nbsp; &nbsp; This item cannot be returned -  {$nonReturnableErrorMsg} </div>
		{/if}
	{else}
		{if $singleitem }
			<div class="divider">&nbsp;</div>
			<div class="divider">&nbsp;</div>
			<form id="returnsubmitform" action="" method="POST">
		    	<input type="hidden" name="orderid" value="{$orderid}">
	    		<input type="hidden" name="itemid" value="{$singleitem.itemid}">
	    		<input type="hidden" name="item_option" value="{$singleitem.option}">
	    		<input type="hidden" name="userid" value="{$login}">
	    		<input type="hidden" name="warehouseid" value="{$warehouseid}">
				<input type="hidden" name="customer_login" id="customer_login" value="{$singleitem.login}">
	    		<script type="text/javascript">
	    			var itemprice = "{$singleitem.price}";
	    			var itemdiscount = "{$singleitem.discount}";
	    			var itemcoupondiscount = "{$singleitem.coupon_discount_product}";
	    			var itempgdiscount = "{$singleitem.pg_discount}";
	    			var itemcashback = "{$singleitem.cashback}";
                                var itemtaxamount = "{$singleitem.taxamount}";
                                var itemLoyaltyCredit = "{$singleitem.item_loyalty_points_used*$singleitem.loyalty_points_conversion_factor}";
	    			var itemqty = "{$singleitem.amount}";
	    			var pickupcost = "{$pickupcharges}";
	    			var selfDeliveryCredit = "{$selfDeliveryCredit}";
				var cartDiscountSplitOnRatio = "{$singleitem.cart_discount_split_on_ratio}";
				var discountQuantity	= "{$discount_quantity}";
					var emicharge = "{$singleitem.emi_charge}";
	    		</script>
	    		<div style="min-height: 250px;">
		    		<div class="left return-item-details">
		    			<div class="item-img">
			    			{if $singleitem.default_image}
			    				<img src="{$singleitem.default_image}">
		                    {elseif $singleitem.image_portal_t}
		                    	<img src="{$singleitem.image_portal_t}">
		                    {/if}
		                </div>
	                    <div class="divider"></div>
	                    <ul>
	                    	{if $singleitem.stylename}
	                    		<li><div><b>{$singleitem.stylename}</b></div></li> 
	                    	{/if}
	                        {* if $singleitem.product}
	                        	<li><div>{$singleitem.product}</div></li> 
	                        {/if *}
	                        {if $singleitem.price}<li><div>Rs {$singleitem.price}</div></li> {/if}
	                    </ul>
		    		</div>
	    			<div class="left return-reason-details">
		    			<table border=0 cellspacing=0 cellpadding=0 width=100%>
		    				<tr>
		    					<td width=30% align=right style="vertical-align: top;">Size:</td>
	    						<td width=70% align=left style="vertical-align: top;">{$singleitem.size}</td>
		    				</tr>
		    				<tr>
	    						<td width=30% align=right style="vertical-align: top;">Quantity:</td>
	    						<td width=70% align=left style="vertical-align: top;">
		    						<select name="quantity" id="quantity" class="return-select">
			    						{section name=qtyVal loop=$singleitem.quantity }
	  										<option value="{$smarty.section.qtyVal.iteration}">{$smarty.section.qtyVal.iteration}</option>
										{/section}
									</select>
		    					</td>
		    				</tr>
		    				<tr>
		    					<td width=30% align=right style="vertical-align: top;">Reason<em style="color:red;">*</em>:</td>
	    						<td width=70% align=left style="vertical-align: top;">
	    							<select id="return-reason" name="return-reason" class="return-select" onchange='javascript:handleReturnReasonChange(this.value);'>
			    						<option value="0">Select Reason</option>
			    						{section name=idx loop=$reasons}
			    							<option value="{$reasons[idx].code}">{$reasons[idx].displayname}</option>
			    						{/section}
			    					</select>
	    						</td>
	    					</tr>
	    					<tr>
	    						<td width=40% align=right style="vertical-align: top;">Details<em style="color:red;">*</em>:</td>
	    						<td width=60% align=left style="vertical-align: top;">
				    				<textarea id="return-details" name="return-details" class="return-details-box"></textarea>
				    			</td>
				    		</tr>
				    	</table>
				    </div>
				    <div class="right return-address-details">
		               	<div class="section-heading">Return Shipment Options</div>
		               	<div id="return-method-choice">
		               		<div>
		               			<input type=radio name="return-method" value="self" {if $warehouseid eq 4 or $warehouseid eq 5} disabled {/if} {if $returnMode eq 'self'} checked {/if} onchange="showAlertForPickup();"> Self-Shipped 
		               			<a href="javascript:void(0);">
		               			<img src="{$cdn_base}/skin1/images/custom/question_button.gif"  title="To be provided only if pickup is not available or has exceeded the daily limit of pickup requests." class="vtip"> </a>
		               		</div>
	               			<div>
	               				<!-- pickup changes -->
	               					{if !$pickup_excluded} <input type=radio name="return-method" id="return-method-pickup" value="pickup"  {if $returnMode eq 'pickup'} checked {/if} {if $warehouseid eq 4 or $warehouseid eq 5} onclick="showPickupCouriers()" {/if} onchange="alert('Please confirm selection of pickup option for this request.');"> Pick Up &nbsp; &nbsp;
	               				{if $pickupcharges != 0.00}
	               					<span class="caption">(Convenience charge of Rs {$pickupcharges} applicable)</span>
	               				{/if}
	               				{/if}
	               			</div>
		               	</div>
		               	<div class="addressblock" id="pickup-courier-choice" style="display:none"> Select Pickup Courier for Customer's pincode
		               		<div>
		               			<input type=radio name="pickup-courier" value="BJ"> BJ
		               		</div>
	               			<div>
	               				<input type=radio name="pickup-courier" value="SQ"> SQ
	               			</div>
	               		</div>
		               	<div class="divider"></div>
		               	<input type=hidden name="default-address-id" id="default-address-id" value="{$defaultAddressId}">
		               	<div id="selected-address-details">
		               		{if $selectedAddressId && $selectedAddressId neq '' }
		               			<input type=hidden name="selected-address-id" id="selected-address-id" value="{$selectedAddressId}">
		               			<input type=hidden id="pickup-available" value="{$selectedAddress.is_serviceable}">
		               			<input type="hidden" name="pickup-by" value="{$selectedAddress.courier}">
		               			<div id="address-details" class="addressblock">
				               		<div class="left" style="width: 300px;">
										<div id="no-pickup-available" class="hide">Pickup not available for this address</div>			               				
					            		<div class="name setname">
					                       	{$selectedAddress.name}
					                   	</div>
					                   	<div class="address">{$selectedAddress.address}</div>
					                   	<div class="city">{$selectedAddress.city} - {$selectedAddress.pincode}</div>
					                   	<div class="state">{$selectedAddress.state_display}, {$selectedAddress.country_display}</div>
					                   	<div class="mobile-no"><b>Mobile</b>: {$selectedAddress.mobile}</div>
					                   	<div class="email"><b>Email</b>: {$selectedAddress.email}</div>
					                </div>
				                  	<a href="javascript:void(0);" class="editAddress" id="change-address-link" name="change-address-link" title="Change Address"></a> &nbsp;&nbsp;
				                   	<div class="divider"></div>
				                </div>
			                 	<div class="divider"></div>
		               		{ else }
	               				<div id="address-details" class="addressblock">
	               					<a href="javascript:void(0);" class="editAddress" id="change-address-link" name="change-address-link" title="Change Address"></a> &nbsp;&nbsp;
	               					No Address selected. Please select your address.
	               				</div>
		               			<div class="divider"></div>
		               		{/if}
	               		</div>
					    <div class="divider">&nbsp;</div>
						{include file="admin/main/return_item_address.tpl" address=$selectedAddress}
					</div>
				</div>
				<div class="divider">&nbsp;</div>
	    		<div class="left" style="width: 600px;">
		    		<input type=checkbox id="check1" name="check1" value="true">
		    		Please confirm that the product being returned is in unused, unwashed and has all tags/stickers/ accompanying material that the product was shipped with originally.
		    		<br/><br/>
		    		Please make sure that you ship with your returns the original order invoice and the packaging that the original order was shipped to you with.
		    		<br/><br/>
		    		Please confirm that you have read and are in agreement with all terms and conditions in Myntra’s Returns Policy.
		    	</div>
	       		<div class="refunds-section-outer right">
	           		<div class="refunds-section-inner"></div>
	            </div>
		        <div class="divider">&nbsp;</div>
		    	<div><input class="orange-bg corners p5 clearfix right" type=submit id="return-confirm-btn" name=return-confirm-btn value="Confirm Return" disabled></div>
		    	<div class="divider"></div>
			</form>
		{ else }
			<div class="notification p5"><img src="/skin1/mkimages/unchecked.gif"> &nbsp; &nbsp; No items are present in this order which can be returned.</div>
		{/if}
	{/if}
	<div class="divider"></div>
	{literal}
	<script type="text/javascript">
		function handleReturnReasonChange(val){
			if(within30days == 'false' && val != '0'){
				alert('Please ensure that you have validated the warranty expiry date / terms for the item being returned with the customer');
			}
		}
		function showAlertForPickup(){
			if($("#pickup-available").val() == 1) {
				alert("Are you sure customer does not want to have this picked up?");
			}else{
				alert("Please confirm selection of self-ship option.");
			}
		}
		function showPickupCouriers(){
			$("#pickup-courier-choice").css('display', 'block');
		}
	
		var vtip = function() {
		    this.xOffset = 15; // x distance from mouse
		    this.yOffset = -20; // y distance from mouse
	
		    $(".vtip").unbind().hover(
		        function(e) {
		            this.t = this.title;
		            this.title = '';
		            this.top = (e.pageY + yOffset); this.left = (e.pageX + xOffset);
	
		            $('body').append( '<div id="vtip">' + this.t + '</div>' );
	
		            // $('div#vtip #vtipArrow').attr("src", 'images/vtip_arrow.png');
		            $('div#vtip').css("top", this.top+"px").css("left", this.left+"px").fadeIn("slow");
	
		        },
		        function() {
		            this.title = this.t;
		            $("div#vtip").fadeOut("slow").remove();
		        }
		    ).mousemove(
		        function(e) {
		            this.top = (e.pageY + yOffset);
		            this.left = (e.pageX + xOffset);
	
		            $("div#vtip").css("top", this.top+"px").css("left", this.left+"px");
		        }
		    );
	
		};
	
		var policyChecker = function() {
			if( $("#check1").is(":checked")){
			$("#return-confirm-btn").attr('disabled', false);
				$("#return-confirm-btn").removeClass('disabled-btn');
			} else {
				$("#return-confirm-btn").attr('disabled', true);
				$("#return-confirm-btn").addClass('disabled-btn');
			}
		};

		function getRefundDisplay() {
			var unitCashback = itemcashback/itemqty;
			var unitDiscount = itemdiscount/itemqty;
			var unitCouponDiscount = itemcoupondiscount/itemqty;
			var unitpgdiscount = itempgdiscount/itemqty;
			var unitCartDiscountSplitOnRatio = cartDiscountSplitOnRatio/itemqty;
                        var vatamount = itemtaxamount/itemqty;
                        var unitLoyaltyCredit = Number(itemLoyaltyCredit/itemqty);
			var selectedQty = $("#quantity").val();
			var final_refund = (Number(itemprice) + Number(vatamount) - unitDiscount - unitCouponDiscount - unitpgdiscount - unitCashback - unitCartDiscountSplitOnRatio - unitLoyaltyCredit)*selectedQty ;
			var returnMode = $("input[name='return-method']:checked").val();
			if(returnMode == 'pickup') {
				final_refund -= parseFloat(pickupcost);
			} else if(returnMode == 'self'){
				final_refund += parseFloat(selfDeliveryCredit);
			}
			if(final_refund < 0){
				final_refund = 0;
			}				
			var html = '<div class="label">Item Price:</div><div class="field">'+(itemprice*selectedQty).toFixed(2)+'</div>';
			html += '<div class="clear"></div>';
			if(unitDiscount != 0) {
				html += '<div class="label">Item Discount:</div><div class="field">-'+(unitDiscount*selectedQty).toFixed(2)+'</div>';
				html += '<div class="clear"></div>';
			}
			if(unitCartDiscountSplitOnRatio != 0 ) {
				html += '<div class="label" >Cart Discount:</div><div class="field">-'+(unitCartDiscountSplitOnRatio*selectedQty).toFixed(2)+'</div>';
				html += '<div class="clear"></div>';
			}
			if(unitCouponDiscount != 0) {
				html += '<div class="label">Coupon Discount:</div><div class="field">-'+(unitCouponDiscount*selectedQty).toFixed(2)+'</div>';
				html += '<div class="clear"></div>';
			}
			if(unitpgdiscount != 0){
				html += '<div class="label">PG Discount:</div><div class="field">-'+(unitpgdiscount*selectedQty).toFixed(2)+'</div>';
				html += '<div class="clear"></div>';
			}
			if(unitCashback != 0) {
				html += '<div class="label">*Cashback Reversal:</div><div class="field">-'+(unitCashback*selectedQty).toFixed(2)+'</div>';
				html += '<div class="clear"></div>';
			}
			if(returnMode == 'pickup' && pickupcost != 0.00) {
				html += '<div class="label">**Convenience Charge:</div><div class="field">-'+pickupcost+'</div>';
				html += '<div class="clear"></div>';
			}
			if(returnMode == 'self' && selfDeliveryCredit != 0.00) {
				html += '<div class="label">Self-Shipping Credit:</div><div class="field">'+selfDeliveryCredit+'</div>';
				html += '<div class="clear"></div>';
			}
                        if(vatamount != 0.00) {
                                html += '<div class="label">*Vat amount:</div><div class="field">'+(vatamount*selectedQty).toFixed(2)+'</div>';
                                html += '<div class="clear"></div>';
                        }
                        if(unitLoyaltyCredit != 0.00) {
                                html += '<div class="label">*Loyalty Credit:</div><div class="field">'+-(unitLoyaltyCredit*selectedQty).toFixed(2)+'</div>';
                                html += '<div class="clear"></div>';
                        }

			html += '<div style="border-bottom: 1px dashed #CCCCCC;"></div>';
			html += '<div class="grandlabel">Refund Amount:</div><div class="grandfield">'+final_refund.toFixed(2)+'</div>';
			html += '<div class="clear"></div>';
			return html;
		}

		$(document).ready(function() {
 			$("#check1").click(policyChecker);
			policyChecker();
			$("input[name='return-method']").click(function(){
				if( $(this).val() == "self" ) {
					$("#no-pickup-available").css('display', 'none');
					//$("#address-details").css('display', 'block');
				} else {
					if($("#pickup-available").val() != "1") {
					
					//$("#address-details").css('display', 'none');
					$("#no-pickup-available").css('display', 'block');
					
				}	
			}

			$(".refunds-section-inner").html(getRefundDisplay());
			vtip();
		});
			if($("#pickup-available").val() != "1") {
				$("#return-method-pickup").attr('disabled', 'disabled');
			}
		$("#change-address-link").click(function(e){
			showSelectAddressWindow(e);
		});
	
		$("#newaddress-link").click(function(e){
		 	closeSelectAddressWindow();
		   	showNewAddressWindow(e);
		});
	
	    $("#new_country").change(function() {
			var country = $(this).val();
			if(country == 'IN') {
				$("#new_state_list").css('display', 'block');
				$("#new_state").css('display', 'none');
			} else {
				$("#new_state_list").css('display', 'none');
				$("#new_state").css('display', 'block');
			}
		});
					 	
		$("#returnsubmitform").submit(function() {
			$("#return-confirm-btn").attr("disabled","true");
			
			if($("#return-reason").val() == "0") {
				alert("Please select reason for return");
				$("#return-confirm-btn").attr("disabled","");
				return false;
			}

			var value = $(":radio[name=return-method]:checked").attr('value');
			if(value == null || value == ''){
				alert("Please select return method");
				$("#return-confirm-btn").attr("disabled","");
				return false;
			}
			
			if($("#return-details").val() == "") {
				alert("Please describe issues/defects in the product");
				$("#return-confirm-btn").attr("disabled","");
				return false;
			}
	
			if( !$("#selected-address-id").val() || $("#selected-address-id").val() == "" ) {
				alert("You have not specified an address. Please select one.");
				$("#return-confirm-btn").attr("disabled","");
				return false;
			}
	
			var returnMode = $("input[name='return-method']:checked").val();
				if(returnMode == 'pickup' && $("#pickup-available").val() != "1") {
					alert("Please select an address with pickup availability");
					$("#return-confirm-btn").attr("disabled","");
					return false;
			}
			
			if((warehouseid == 4 || warehouseid == 5) && !$("input[name='pickup-courier']:checked").val()){
				alert("You have not specified a pickup-courier. Please select one.");
				$("#return-confirm-btn").attr("disabled","");
				return false;
			}
			
			var overlay = new ProcessingOverlay();
            overlay.show();
			
			return true;
		});
	
		$("#quantity").change(function() {
			$(".refunds-section-inner").html(getRefundDisplay());
		});
	
		if($("#new_country").val() == "IN") {
			$("#new_state").css("display","none");	
		} else {
			$("#new_state_list").css("display","block");
		}
	
		var returnMode = $("input[name='return-method']:checked").val();
			if( returnMode == "self" ) {
				$("#no-pickup-available").css('display', 'none');
			} else {
				if($("#pickup-available").val() != "1") {
			   		$("#no-pickup-available").css('display', 'block');
				}	
			}
			$(".refunds-section-inner").html(getRefundDisplay());
		});
		
	</script>
	{/literal}
{ else }
	<div class="section-heading prepend">Return Submitted</div>
    <div class="blue-divider"></div>
    <div class="divider">&nbsp;</div>
    <div id="return-details" class="left" style="width: 580px; padding: 10px;">
		<div>Return request created Successfully! Please note <b>Return Number #{$returnid}</b>.</div>
		<div style="color:red;">{$errorMsg}</div>
		<div class="divider">&nbsp;</div>
	    <div>Following are the details of Return request</div>
	    <div class="item-img">
           	<img src="{$return_req.designImagePath}"  style="border:1px solid #CCCCCC;">
        </div>
        <div class="item-info" style="width: 400px;">
        	<ul class="cart-order">
            	{if $return_req.productStyleName}<li><div class="order-label"></div><div><b>{$return_req.productStyleName}</b></div></li> {/if}
                <li> <b>Size</b>: { $return_req.sizeoption } </li>
                <li> <b>Quantity</b>: { $return_req.quantity } </li>
                <li> <b>Reason</b>: { $return_req.reason } </li>
                <li> <b>Details</b>: { $return_req.description } </li>
            </ul>
        </div> 
        <div class="divider">&nbsp;</div>
        <input type=button name="close-btn" id="close-btn" value="Close" style="padding:5px 10px;" onclick="opener.window.location.reload(); self.close();"/>
    </div>
    <div class="divider">&nbsp;</div>
{/if}
