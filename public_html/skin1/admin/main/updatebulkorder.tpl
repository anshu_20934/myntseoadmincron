{* $Id: updatebulkorders.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
{include file="main/include_js.tpl" src="main/popup_product.js"}
<script type="text/javascript">
	var progressKey = '{$progresskey}';
</script>
<link rel="stylesheet" href="{$SkinDir}/skin1_admin_operations.css" />
{literal}
<script type="text/javascript">
	function showUploadStatusWindow() {
		$("#uploadstatuspopup").css("display","block");
		$("#uploadstatuspopup-content").css("display","block");
		$("#uploadstatuspopup_title").css("display","block");
		
		var oThis = $("#uploadstatuspopup-main");
		var topposition = ( $(window).height() - oThis.height() ) / 2;
		if ( topposition < 10 ) 
			topposition = 10;
		oThis.css("top", ( topposition + $(window).scrollTop() + "px"));
		oThis.css("left", ( $(window).width() - oThis.width() ) / 2+$(window).scrollLeft() + "px");

		setInterval(trackStatus, 1000);

		return true;
	}
	
	function trackStatus() {
		$.get("getprogressinfo.php?progresskey="+progressKey,
			function(percent) {
				if(percent != "false") {
					$("#progress_bar").css('width', percent+"%");
					$("#progress_text").html(percent);
				}
			}
		);
	}
</script>

{/literal}
{capture name=dialog}
	{if $action eq "upload"}
        <div style="font-weight: bold; font-size: 13px; margin-bottom: 30px; color:#9F6000; border: 1px solid; padding: 5px; text-align: center"><b>IMPORTANT: Max 50 Orders can be uploaded in one run.</b></div>
        {if $errorMsg } <div style="font-weight: bold; color:red; padding: 5px;">{$errorMsg}</div> {/if}
		{if $msg } <div style="font-weight: bold; color: green; padding: 0 5px;">{$msg}</div> {/if}
		<form action="" method="post" name="bulkorderaction" enctype="multipart/form-data">
			<input type='hidden' name='action' value='verify'>
			<table cellpadding="1" cellspacing="1" width="750">
				<tr>
					<td>
					   Upload the csv file <br> (Orderid, TrackingNo): <br>
					</td> 
					<td><input type="file" name="orderdetail" size=50>{if $message}<font color='red'>{$message}</font>{/if}</td>
				</tr>
				<tr>
					<td>Courier Service</td>
					<td>
					<select id="courier_service" name="courier_service">
						<option value="0">--Select Courier Service--</option>
						{section name=idx loop=$couriers}
							<option value="{$couriers[idx].code}" {if $couriers[idx].code eq $courier_service}selected{/if}>{$couriers[idx].display_name}</option>
						{/section}
					</select>
					</td> 
				<tr>
				<tr>
					<td>Shipping Option</td>
					<td>
						<input type="radio" name="update_to" value="ship" {if $update_to eq 'ship'}checked{/if}> Ship Orders
						<input type="radio" name="update_to" value="reship" {if $update_to eq 'reship'}checked{/if}> Re-Ship Orders
					</td>
				</tr>
				<tr>
					<td colspan=2 align=right><input type='submit'  name='uploadxls'  value="Upload"></td>
				</tr>
			</table>
		</form>
	{else}
		<form name='frmconfirm'  action=""  method="post" onsubmit="return showUploadStatusWindow();">
	      	<input type='hidden' name='action' value='confirmorder'>
	     	<input type='hidden' name='filetoread' value='{$uploadorderFile}'>
	     	<input type='hidden' name='progresskey' value='{$progresskey}'>
	     	<input type='hidden' name='login' value='{$login}'>
	     	<input type='hidden' name='courier_service' value='{$courier_service}'>
	     	<input type='hidden' name='update_to' value='{$update_to}'>
	        <table cellpadding="2" cellspacing="1" width="100%">
		    	<tr class="TableHead">
					<td >Order ID</td>
					<td>Tracking Number</td>
					<td>Courier Service</td>
		     	</tr>
	            {section name=idx loop=$orderListDisplay}
			    	<tr{cycle values=", class='TableSubHead'"}>
						<td align="center">{$orderListDisplay[idx][0]}</td>
						<td align="left" >{$orderListDisplay[idx][1]}</td>
						<td align="left" >{$orderListDisplay[idx][2]}</td>
			    	</tr>
	           	{/section}
	            <tr class="TableHead">
					<td >&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
		     	</tr>
		     	<tr class="TableSubHead">
					<td >&nbsp;</td>
					<td><input type='submit' name='confirm'  value='confirm' /></td>
					<td>&nbsp;</td>
		     	</tr>
		 	</table>
		</form>
		<div id="uploadstatuspopup" style="display:none">
			<div class="overlay" style="display:block;_position:absolute;_width:1300px;_height:1500px;">&nbsp;</div>
			<div id="uploadstatuspopup-main" style="position:absolute; left: 20%; width:450px; background:white; z-index:1006;border:2px solid #999999;">
				<div id="uploadstatuspopup-content" style="display:none;">
				    <div id="uploadstatuspopup_title" class="popup_title">
				    	<div id="uploadstatuspopup_ajaxWindowTitle" class="popup_ajaxWindowTitle">Upload Status</div>
						<div class="clear">&nbsp;</div>
					</div>
					<div style="padding:5px 25px; text-align:left; font-size:12px" >
						<div id="progress_container"> 
						    <div id="progress_bar">
						    	<div id="progress_completed"></div> 
						    </div>
						</div>
						<span id="progress_text" style="font-size: 14px; color: green; padding: 0 0 0 5px;">0</span> % Complete
					</div>
					<div class="clear">&nbsp;</div>
				</div>
			</div>
		</div>
	{/if}
{/capture}
{include file="dialog.tpl" title='Upload Orders in Bulk' content=$smarty.capture.dialog extra='width="100%"'}
