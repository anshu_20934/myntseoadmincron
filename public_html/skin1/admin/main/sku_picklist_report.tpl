{if $accessallowed eq 1 }
<link rel="stylesheet" type="text/css" href="{$http_location}/admin/extjs/resources/css/ext-all.css"/>

<script type="text/javascript">
var httpLocation = '{$http_location}';
</script>

<script type="text/javascript" src="{$http_location}/admin/extjs/adapter/ext/ext-base.js"></script>

<script type="text/javascript" src="{$http_location}/admin/extjs/ext-all.js"></script>

<script type="text/javascript" src="sku_picklist_report.js"></script>

<div id="contentPanel"></div>
{else}
	<b>This report has been disabled on performance grounds. Please get in touch with technology team for further details!</b>
{/if}