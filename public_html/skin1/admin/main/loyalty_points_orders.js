Ext.onReady(function() {
	Ext.QuickTips.init();
	var myMask = new Ext.LoadMask(Ext.getBody(), {msg: "Loading..."});
	Ext.Ajax.on('beforerequest', myMask.show, myMask);
	Ext.Ajax.on('requestcomplete', myMask.hide, myMask);
	Ext.Ajax.on('requestexception', myMask.hide, myMask);
	
	var searchPanel = new Ext.form.FormPanel({
		renderTo : 'contentPanel',
		title : 'Search Panel',
		id : 'formPanel',
		autoHeight : true,
		collapsible : true,
		border : true,
		bodyStyle : 'padding: 5px',
		width : 1000,
		items : [{
			layout : 'table',
			border : false,
			layoutConfig : {columns : 2},
			defaults : {width : 450, border : false, 
				layout : 'form', bodyStyle : 'padding-left : 15px'},
			items : [
			         {
			        	 items : [{
			        		 width: 300,
			        		 xtype : 'textfield',
			        		 id : 'userlogin',
			        		 fieldLabel : 'Customer Login'
			        	 }]
			         },
			         {
			        	 items : [{
			        		 width: 300,
			        		 xtype : 'textfield',
			        		 id : 'orderid',
			        		 fieldLabel : 'Order Id'
			        	 }]
			         }
			]
		}],
		buttons : [ {
			text : 'Search',
			handler : function() {
				reloadGrid();
			}
		}],
	});
	
	/*var goodwillReasonStore = new Ext.data.JsonStore({
		fields : ['id', 'name'],
		data : goodwillReasonData,
		sortInfo : {field : 'name', direction : 'ASC'}
	});*/	

	function getSearchPanelParams() {
		var form = searchPanel.getForm();
		
		if (form.isValid()) {
			var params = {};
			//params["coupon"] = form.findField("coupon").getValue();
			params["userlogin"] = form.findField("userlogin").getValue();
			params["orderid"] = form.findField("orderid").getValue();
			
			return params;
		} else {
			return false;
		}
	}
	
	function reloadGrid() {
		
		var params = getSearchPanelParams();
		
		if (params == false) {
			Ext.MessageBox.alert('Error', 'Errors in the search form');
			return false;
		}
		params["action"] = "search";
		
		searchResultsStore.baseParams = params;
		searchResultsStore.load();
	}
	
	var txnTypeRenderer = function(value, p, record) {
		if(value == 'C') {
			return 'Credit';
		} else {
			return 'Debit';
		}
	}
	
	var colModel = new Ext.grid.ColumnModel({
		defaults: {
	        sortable: true,
	        editable:true,
	        editor : Ext.form.Field
	    },
	    columns: [
	        new Ext.grid.RowNumberer(),
	        {id : 'modifiedOn', header : "Date", sortable: false, width: 125, dataIndex : 'modifiedOn'},	        
	        //{id : 'coupon', header : "Coupon", sortable: false, width: 125, dataIndex : 'coupon'},
	        {id : 'login', header : "Login", sortable: false, width: 150, dataIndex : 'login'},
	        {id : 'accountType', header : "Login", sortable: false, width: 150, dataIndex : 'accountType'},
	        //{id : 'txn_type', header : "Transaction Type", sortable: false, width: 125, dataIndex : 'txn_type', renderer: txnTypeRenderer},
	        {id : 'creditInflow', header : "Credit Amount", sortable: false, width: 100, dataIndex : 'creditInflow'},
	        {id : 'creditOutflow', header : "Debit Amount", sortable: false, width: 100, dataIndex : 'creditOutflow'},	        
	        {id : 'balance', header : "Balance", sortable: false, width: 100, dataIndex : 'balance'},
	        //{id : 'businesProcess', header : "Business Process", sortable: false, width: 300, dataIndex : 'businesProcess'},
	        //{id : 'itemId', header : "Item ID", sortable: false, width: 100, dataIndex : 'itemId'},
	        //{id : 'itemType', header : "Item Type", sortable: false, width: 100, dataIndex : 'itemType'},
	        {id : 'modifiedBy', header : "Transaction By", sortable: false, width: 125, dataIndex : 'modifiedBy'},
	        {id : 'description', header : "Description", sortable: false, width: 300, dataIndex : 'description'}
  	  	]
	});
		
	var searchResultsStore = new Ext.data.Store({
		url: 'LoyaltyPointsOrders.php',
		pruneModifiedRecords : true,
		reader: new Ext.data.JsonReader({
	  		root: 'results',
	  	  	totalProperty: 'count',
	  	  	fields : ['modifiedOn', 'login', 'accountType','creditInflow','creditOutflow','businesProcess', 'itemId','itemType', 'balance','modifiedBy' ,'description']
		}),
	});

	var pagingBar = new Ext.PagingToolbar({
		pageSize : 30,
	    store: searchResultsStore,
	    displayInfo: true
	});
	
	var searchResults = new Ext.grid.EditorGridPanel({
	    store  : searchResultsStore,
	    cm : colModel,
	    id : 'searchResults-panel',
	    title : 'Search Results',
	    frame : true,
		loadMask : false,
		autoHeight : true,
		bbar : pagingBar,
		stripeRows : true,
		autoScroll : true,
		viewConfig : {
			enableRowBody : true
		}
	});
	var mainPanel = new Ext.Panel({
		renderTo : 'contentPanel',
		border : false,
		items : [
		     searchPanel, searchResults
		],
		listeners : {
			'afterrender' : function(comp) {
				if(get_login && get_login != "") {
					searchPanel.getForm().findField("userlogin").setValue(get_login);
				}
				/*if(get_coupon && get_coupon != "") {
					searchPanel.getForm().findField("coupon").setValue(get_coupon);
				}*/
				reloadGrid();
			}
		}
	});
});
