{* $Id: featured_products.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
<script type="text/javascript" src="{$http_location}/modules/uploadify/jquery.uploadify.v2.1.4.min.js"></script>
<script type="text/javascript" src="{$http_location}/modules/uploadify/swfobject.js"></script>
<link href="{$http_location}/modules/uploadify/uploadify.css" rel="stylesheet" type="text/css">

<a name="featured" />
{$lng.txt_featured_products}
<br /><br />

{capture name=dialog}
    <form enctype="multipart/form-data" action="upload_banner.php" method="POST">
        <div>
            <div class="lblleft">Image field</div><input name="image" type="file" id="image"/>
            </div>
            <div>
                <input type="submit" value="Add"/>
            </div>
        </div>
    </form>

{/capture}

{capture name=S3list}
    <div style="margin-top:10px;">
        {$html}
    </div>
{/capture}

{include file="dialog.tpl" title='Mailer management' content=$smarty.capture.dialog extra='width="100%"'}
<br/>
{include file="dialog.tpl" title='Banner Images in S3' content=$smarty.capture.S3list extra='width="100%"'}
