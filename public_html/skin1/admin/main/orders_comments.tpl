{* $Id: cancelorders.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
{include file="main/include_js.tpl" src="main/popup_product.js"}
<script type="text/javascript">
	var progressKey = '{$progresskey}';
</script>
<link rel="stylesheet" href="{$SkinDir}/skin1_admin_operations.css" />
{literal}
<script type="text/javascript">
	function showUploadStatusWindow() {

		$("#uploadstatuspopup").css("display","block");
		$("#uploadstatuspopup-content").css("display","block");
		$("#uploadstatuspopup_title").css("display","block");
		
		var oThis = $("#uploadstatuspopup-main");
		var topposition = ( $(window).height() - oThis.height() ) / 2;
		if ( topposition < 10 ) 
			topposition = 10;
		oThis.css("top", ( topposition + $(window).scrollTop() + "px"));
		oThis.css("left", ( $(window).width() - oThis.width() ) / 2+$(window).scrollLeft() + "px");

		setInterval(trackStatus, 500);

		return true;
	}
	
	function trackStatus() {
		$.get("getprogressinfo.php?progresskey="+progressKey,
			function(percent) {
				if(percent != "false") {
					$("#progress_bar").css('width', percent+"%");
					$("#progress_text").html(percent);
				}
			}
		);
	}
</script>
{/literal}

{capture name=dialog}

	{if $message}<font color='green' size='2'>{$message}</font><br/><br/>{/if}
	{if $errormessage}<font color='red' size='2'>{$errormessage}</font><br/><br/>{/if}
	<form action="" method="post" name="orderscommentsform" enctype="multipart/form-data" onsubmit="return showUploadStatusWindow();">
		<table border=0 cellspacing=0 cellpadding=0 width=100%>
			<tr>
				<td>Upload the file:<br>(Excel file with OrderIds in first column. <br>First row will be ignored as header)</td>
				<td><input type="file" name="orderidsfile" size=100></td>
			</tr>
			<tr><td>Comment Title:</td><td><input type="text" id="comment_title" name="comment_title" size=30></td></tr>
			<tr><td>Comment Text:</td><td><textarea id="comment_text" name="comment_text" rows=5 cols=60></textarea></td></tr>
			<tr><td colspan=2 align=right><input type='submit' name='uploadxls' value="Upload"></td></tr>
		</table>
		<input type='hidden' name='action' value='verify'>
		<input type='hidden' name='progresskey' value='{$progresskey}'>
		<input type='hidden' name='login' value='{$login}'>
	</form>
	
	<div id="uploadstatuspopup" style="display:none">
		<div class="overlay" style="display:block;_position:absolute;_width:1300px;_height:1500px;">&nbsp;</div>
		<div id="uploadstatuspopup-main" style="position:absolute; left: 20%; width:450px; background:white; z-index:1006;border:2px solid #999999;">
			<div id="uploadstatuspopup-content" style="display:none;">
			    <div id="uploadstatuspopup_title" class="popup_title">
			    	<div id="uploadstatuspopup_ajaxWindowTitle" class="popup_ajaxWindowTitle">Upload Status</div>
					<div class="clear">&nbsp;</div>
				</div>
				<div style="padding:5px 25px; text-align:left; font-size:12px" >
					<div id="progress_container"> 
					    <div id="progress_bar">
					    	<div id="progress_completed"></div> 
					    </div>
					</div>
					<span id="progress_text" style="font-size: 14px; color: green; padding: 0 0 0 5px;">0</span> % Complete
				</div>
				<div class="clear">&nbsp;</div>
			</div>
		</div>
	</div>
{/capture}
{include file="dialog.tpl" title='Cancel Orders' content=$smarty.capture.dialog extra='width="100%"'}