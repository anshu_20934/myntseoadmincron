{* $Id: featured_products.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
{include file="main/include_js.tpl" src="main/popup_product.js"}
{literal}
  <script language="Javascript">
      function confirmDelete()
      {
          var flag = confirm('Are you sure you want to delete?')
          if(!flag)
          {
             return false;
          }
          else
          {
          	 document.groupfrm.mode.value = 'delete';
          	 document.groupfrm.submit();
          	 return true;
          }
      }
	function addPriority()
	{
		var w = document.frmG.name.selectedIndex;
		var selected_text = document.frmG.name.options[w].text;
		 document.frmG.label.value = selected_text;
		document.frmG.submit();
	}
  </script>
{/literal}
<a name="featured" />

{$lng.txt_featured_products}

<br /><br />

{capture name=dialog}

<form action="{$form_action}" method="post" name="groupfrm" enctype="multipart/form-data">
<input type="hidden" name="mode"  />

<table cellpadding="3" cellspacing="1" width="100%">

<tr class="TableHead">
	<td width="10%">&nbsp;</td>
	<td width="20%">Name</td>
	<td width="20%">Priority</td>
	<td width="20%" align="center">Active</td>

</tr>

{if $priority}

{section name=grid loop=$priority}

<tr{cycle values=", class='TableSubHead'"}>
    <input type="hidden" name="posted_data[{$priority[grid].id}][gid]" value={$products[grid].id} />
	<td><input type="checkbox" name="posted_data[{$priority[grid].id}][to_delete]" /></td>
	<td align="center">{$priority[grid].label}</td>
    <td align="center"><input type="text" name="posted_data[{$priority[grid].id}][priority]" value="{$priority[grid].priority}"  size="2" /></td>
	<td align="center">
	<select name="posted_data[{$priority[grid].id}][is_active]">
		<option value="1"{if $priority[grid].is_active eq "1"} selected="selected"{/if}>{$lng.lbl_yes}</option>
		<option value="0"{if $priority[grid].is_active eq "0"} selected="selected"{/if}>{$lng.lbl_no}</option>
	</select>
	</td>
</tr>

{/section}
<td colspan="5"><br /><br /></td>
{else}

<tr>
 <td colspan="4" align="center">No Priority available.</td>
</tr>
{/if}

<!--<tr>
<td colspan="5"><br /><br />{include file="main/subheader.tpl" title=$lng.lbl_add_product}</td>
</tr> -->

<tr>
	<td colspan="5" class="SubmitBox">
	<input type="button" value="{$lng.lbl_delete_selected|escape}" onclick="return confirmDelete();" />
	<input type="submit" value="{$lng.lbl_update|escape}" onclick="javascript: document.groupfrm.mode.value = 'update'; document.groupfrm.submit();" />

	</td>
</tr>


</table>
</form>
<form name="frmG" action="{$form_action}" method="post" enctype="multipart/form-data" >
<input type="hidden" name="mode" value="addgroup" />
<table cellpadding="3" cellspacing="1" width="100%">
   <tr>
        <td><hr/></td>
        <td><hr /></td>
   </tr>
   <tr>
        <td><h2>Add Priority Field</h2></td>
        <td>&nbsp;</td>
   </tr>
   <tr>
        <td colspan="2">Name : 
        <select name="name" style="width:200px">
        <option value="">None</option>
            {section name=num loop=$groups}
                <option value="{$groups[num].group_name|replace:" ":""|replace:"/":""|lower}_filter" style="width:200px" >{$groups[num].group_name}</option>
            {/section}
            <option value="product_type_group" style="width:200px" >Product Type Group</option>
            <option value="style_group" style="width:200px" >Style Group</option>
            <option value="stylename" style="width:200px" >Style Name</option>
            <option value="product" style="width:200px" >Product</option>
            <option value="descr" style="width:200px" >Description</option>
            <option value="keywords" style="width:200px" >Keywords</option>
            
        </select>
	<input type="hidden" name="label" value="" id="add_label">
        </td>
        <td>Priority :  <input type="text" name="priority" /></td>


        <td>Active : 
            <select name="is_active">
                <option value="1" >{$lng.lbl_yes}</option>
                <option value="0">{$lng.lbl_no}</option>
            </select>
        </td>

   </tr>
   <tr>
   <td colspan="2" align="center">
   <input type="button" value="{$lng.lbl_add|escape}" name="sbtFrm" onclick="addPriority()" />
   </td>
   </tr>

</table>
</form>
{/capture}
{include file="dialog.tpl" title="Solr Fields Priority List" content=$smarty.capture.dialog extra='width="100%"'}
<br /><br />


