{* $Id: featured_products.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}

<a name="featured" />

{$lng.txt_featured_products}

<br /><br />

{capture name=dialog}



<form action="style_custumization.php" method="post" name="orientationform" >
<input type="hidden" name="mode"  />
<input type="hidden" name="custom_id" value="{$custom_id}" />

<table cellpadding="3" cellspacing="1" width="100%">

<tr class="TableHead">
	<td width="5%">&nbsp;</td>
	<td width="15%">{$lng.lbl_orientation_name}</td>
	<td width="10%">{$lng.lbl_startx}</td>
	<td width="10%">{$lng.lbl_starty}</td>
	<td width="10%">{$lng.lbl_sizex}</td>
	<td width="10%">{$lng.lbl_sizey}</td>
	<td width="5%">Height</td>
	<td width="5%">Width</td>
	<td width="10%">{$lng.lbl_default}</td>
	<td width="10%" align="center">{$lng.lbl_pos}</td>
	<!--<td width="10%" align="center">{$lng.lbl_active}</td> -->
</tr>

{if $orientation}

{section name=num loop=$orientation}

<tr{cycle values=", class='TableSubHead'"}>
	<td align="center"><input type="checkbox" name="posted_data[{$orientation[num].id}][to_delete]"  /></td>
	<td align="center"><b>{$orientation[num].location_name}</b></td>
	<td align="center"><b>{$orientation[num].start_x}</b></td>
	<td align="center"><b>{$orientation[num].start_y}</b></td>
	<td align="center"><b>{$orientation[num].size_x}</b></td>
	<td align="center"><b>{$orientation[num].size_y}</b></td>
	<td align="center"><b>{$orientation[num].height_in}</b></td>
	<td align="center"><b>{$orientation[num].width_in}</b></td>
	<td align="center"><input type="checkbox" name="posted_data[{$orientation[num].id}][default]"  {if $orientation[num].ort_default eq "Y"} checked {/if}/></td>
	<td align="center"><input type="text" name="posted_data[{$orientation[num].id}][display_order]" size="5" value="{$orientation[num].orientation_order}" /></td>
	<!-- <td align="center">
	<select  name="posted_data[{$orientation[num].id}][is_active]">
	           <option value="1" {if $orientation[num].is_active eq "1"} selected {/if}>Yes</option>
		   <option value="0" {if $orientation[num].is_active eq "0"} selected {/if}>No</option>
        </select> 
			   
       </td> -->
</tr>

{/section}

<tr>
	<td colspan="8" class="SubmitBox">
	<input type="button" value="{$lng.lbl_delete_selected|escape}" onclick="javascript: document.orientationform.mode.value = 'delort'; document.orientationform.submit();" />
	<input type="submit" value="{$lng.lbl_update|escape}" onclick="javascript: document.orientationform.mode.value = 'updort'; document.orientationform.submit();"/>
	<input type="submit" value="{$lng.lbl_modify_selected|escape}" onclick="javascript: document.orientationform.mode.value = 'modifyort'; document.orientationform.submit();"/>
	</td>
</tr>


{else}

<tr>
<td colspan="10" align="center">{$lng.txt_no_featured_products}</td>
</tr>

{/if}

<tr>
<td colspan="10"><br /><br />{include file="main/subheader.tpl" title=$lng.lbl_option_event}</td>
</tr>

<tr>
	<td  ><b>Name</b></td>
	<td  ><input type="text" name="ort_name" size="30" /></td>
	
	<td ><b>Start X</b></td>
	<td ><input type="text" name="startx" size="20" /></td>
	
	<td><b>Start Y</b></td>
	<td  ><input type="text" name="starty" size="20" /></td>
	
	
</tr>

<tr>
	
	<td ><b>X Size</b></td>
	<td ><input type="text" name="sizex" size="20" /></td>
	
	<td  ><b>Y Size</b></td>
	<td  ><input type="text" name="sizey" size="20" /></td>

	<td align="left" width="100"><b>Height</b>(in inch) </td>
	<td align="left"> <input type="text" name="height_in"  size="20" /></td>
	
	
</tr>

<tr>
    <td align="left" width="100"><b>Width</b>(in inch)</td>
	<td align="left"> <input type="text" name="width_in"  size="20"/></td>
	<td align="right"><b>Default</b></td>
	<td align="left"><select name="default"><option value="Y">Y</option><option value="N">N</option></select></td>
        <td align="right"><b>Description</b></td>
	<td align="left"><textarea name="desc" cols="25" rows="2"></textarea></td>
</tr>

<tr>
	
	
	<td ><b>Bg Start X</b></td>
	<td ><input type="text" name="bgstartx" size="20" /></td>
	
	<td><b>Bg Start Y</b></td>
	<td  ><input type="text" name="bgstarty" size="20" /></td>
	<td ><b>Bg X Size</b></td>
	<td ><input type="text" name="bgsizex" size="20" /></td>
	
	
</tr>

<tr>
	
	<td  ><b>Bg Y Size</b></td>
	<td  ><input type="text" name="bgsizey" size="20" /></td>
	<td align="left" width="100"><b>Bg Height</b>(in inch) </td>
	<td align="left"> <input type="text" name="bgheight_in"  size="20" /></td>
	 <td align="left" width="100"><b>Bg Width</b>(in inch)</td>
	<td align="left"> <input type="text" name="bgwidth_in"  size="20"/></td>
	
	
</tr>


<tr>
	<td><b>Action: </b></td>
	<td><input type="text" name="action" value=""></td>
	<td><b>Customization Type: </b></td>
	<td><select name="typeid"> {html_options options=$customization_types} </select></td>
	<td><b>Image Allowed: </b></td>
	<td><input type="checkbox" name="imageAllowed" value="1"></td>
</tr>

<tr>
	<td><b>Image Cust Allowed: </b></td>
	<td><input type="checkbox" name="imageCustAllowed" value="1"></td>
	<td><b>Text Allowed: </b></td>
	<td><input type="checkbox" name="textAllowed" value="1"></td>
	<td><b>Text Cust Allowed: </b></td>
	<td><input type="checkbox" name="textCustAllowed" value="1"></td>
	<td><b>Text Limitation: </b></td>
	<td><SELECT NAME="textLimitation" >
		<OPTION VALUE="1">Y</option>
		<OPTION VALUE="0">N</option>
		</SELECT></td>
</tr>
<tr>
		<td><b>Text Limitation TPL Name: </b></td>
		<td><input type="text" name="textLimitationTpl" ></td>
		<td><b>Text Font Color: </b></td>
		<td><div style="width:410px;border:1px solid #000;height:240px;">
		{section name=idx loop=$font_results}
			<div style="float:left">
				<input type="checkbox" name="textFontColors[]" value="{$font_results[idx].id}" style="float:left;" /><span style="background-color:{$font_results[idx].color};height:13px;_height:14px;font-size:0px;width:13px;display:inline;float:left;margin-top:3px;">&nbsp;</span>
			</div>
		{/section}
		</div></td>
		<td>Text color default</td><td><div style="width:410px;border:1px solid #000;height:200px;">
		{section name=idx loop=$font_results}
			<div style="float:left">
				<input type="radio" name="textColorDefault" value="{$font_results[idx].id}" style="float:left" checked="checked"/><span style="background-color:{$font_results[idx].color};height:10px;_height:12px;font-size:0px;width:10px;display:block;float:left;margin-top:3px"></span>
			</div>
		{/section}
		</div></td>
	
</tr>


<tr>
	<td colspan="6" class="SubmitBox">
	<input type="submit" value="{$lng.lbl_add_new_|escape}" onclick="javascript: document.orientationform.mode.value = 'addorient'; document.orientationform.submit();"/>
	</td>
</tr>

</table>
</form>

{/capture}
{include file="dialog.tpl" title=$lng.lbl_orientation_heading content=$smarty.capture.dialog extra='width="100%"'}
