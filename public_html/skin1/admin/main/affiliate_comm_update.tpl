{* $Id: featured_products.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
{include file="main/include_js.tpl" src="main/popup_product.js"}
{literal}
<script language="Javascript">
 function validateComm(modeVal)
 {
 	 frmObj = document.commform;
 	 if(frmObj.payamt.value == '' )
 	 {
 	 	alert("Please enter valid digit(s).");
 	 	frmObj.payamt.focus();
 	 	return false
 	 }
 	 frmObj.mode.value=modeVal;
 	 return true;
 }
 function IsNumeric(sText)
 {
   var ValidChars = "0123456789";
   var IsNumber=true;
   var Char;

 
   for (i = 0; i < sText.length && IsNumber == true; i++) 
      { 
      Char = sText.charAt(i); 
      if (ValidChars.indexOf(Char) == -1) 
         {
           IsNumber = false;
         }
      }
   return IsNumber;
   
 }
</script>
{/literal}
<a name="featured" />

{$lng.txt_featured_products}

<br /><br />


{capture name=dialog}


{if $affiliates}

<table cellpadding="3" cellspacing="1" width="100%">

{section name=num loop=$affiliates}
  
     <tr>
         <td><b>{$lng.lbl_affiliate}</b></td>
         <td>{$affiliates[num].company_name}</td>
         <td align="left"><b>Contact Name</b></td>
         <td>{$affiliates[num].contact_name}</td>
         <td>&nbsp;</td>
         <td>&nbsp;</td>
    </tr>
    
   

   
{/section}

</table>
<br/>
<br/>

<form action="adminaffiliates.php" method="post" name="commform" >
<input type="hidden" name="mode"  />
<input type="hidden" name="affiliateid" value="{$affiliateid}" />

<table cellpadding="3" cellspacing="1" width="100%">
 <tr>
   <td colspan="6">{include file="main/subheader.tpl" title='Commission Details'}</td>
</tr>

   <tr>
         <td><b>Commission Earned</b></td>
         <td><select name="commStatus" id="commStatus" >
              <option value="1011" {if $status eq '1011' }selected{/if}>Pending</option>
              <option value="1006" {if $status eq '1006' }selected{/if}>Paid</option>
              <option value="All" {if $status eq 'All' }selected{/if}>All</option>
            </select>
          </td>
   </tr>
   <tr>
	<td colspan="2" class="SubmitBox">
	      <input type="submit" value="Submit" onclick="javascript: submitForm(this, 'show');" />
	</td>
   </tr>	    
    
   
</table>

{if $mode eq 'show' ||  $mode eq 'payment' || $mode eq 'update'}
<br/>
<table cellpadding="3" cellspacing="1" width="100%">
	{if $comm_details}
	
	    <tr class="TableHead">
			<td width="10%">&nbsp;</td>
			<td width="15%">#Order ID</td>
			<td width="25%">Content From</td>
			<td width="15%" align="center">Commission(Rs)</td>
			<td width="15%" align="center">Status</td>
		</tr>
		{assign var=total value=0}
		{section name=idx loop=$comm_details}

		<tr{cycle values=", class='TableSubHead'"}>
		     <input type="hidden" name="posted_data[{$comm_details[idx].id}][to_delete]" />
		     <input type="hidden" name="posted_data[{$comm_details[idx].id}][comm]" value="{$comm_details[idx].commission}" />
			<td style="left-padding:10px">
			{if $comm_details[idx].status eq '1006'}
			  &nbsp;
			{else}  
				<input type="checkbox" name="posted_data[{$comm_details[idx].id}][delete]" {if $chkArr[idx] eq $comm_details[idx].id} checked{/if} />
			{/if}	
			</td>
			<td align="center"><b>{$comm_details[idx].order_id}</b></td>
			<td align="center"><b>{$comm_details[idx].content_from}</b></td>
			<td align="center"><b>{$comm_details[idx].commission}</b></td>
			<td align="center"><b>
			{if $comm_details[idx].status eq '1011'}
			    Pending
			{elseif $comm_details[idx].status eq '1006'}
			    Paid
			{else}
			    NA     
			{/if}   
			 </b></td>
						
		</tr>
		{assign var=total value=$total+$comm_details[idx].commission}
		{/section}
		<tr>
   			<td colspan="6" align="right" ><hr/></td>
		</tr>
		<tr>
   			<td colspan="4" align="right" style="padding-right:220px""><b>Total:&nbsp;{$total|string_format:"%.2f"}</b></td>
		</tr>
		{if $status ne '1006'}
		 <tr>
		
			<td colspan="2" class="SubmitBox">
	     		 <input type="submit" value="Make Payment" onclick="javascript: submitForm(this, 'payment');" />
			</td>
  	 	 </tr>
  	 	 {/if}
  	{else}
  	   <tr><td>No order is placed for you product.</td></tr>
	{/if}
	</table>
{/if}  

{if $mode eq 'payment'}
<br/>
<br/>


<table cellpadding="3" cellspacing="1" width="100%">
<tr>
   <td colspan="6">{include file="main/subheader.tpl" title='Pay Commission'}</td>
</tr>

    <tr>
         <td><b>Payment Amount</b></td>
         <td>
	 		<input type="text" name="payamt" id="payamt" value="{$totalcomm}" >
	 </td>
    </tr>   

    <tr>
         <td><b>{$lng.lbl_payment_methods}</b></td>
         <td>
	 <select  name='pay_methods'>
	   {section name=idx loop=$pay_methods}
	      <option value='{$pay_methods[idx].paymentid}' >{$pay_methods[idx].payment_method}</option>
	   {/section} 
	  </select>
	 </td>
    </tr>    

    <tr>
         <td><b>{$lng.lbl_comments}</b></td>
         <td>
	 <textarea name='comments' cols='40' rows='5'></textarea>
	 </td>
    </tr>   

   <tr>
		 <td colspan="2">&nbsp;</td>
		 
   </tr>



<tr>
	<td colspan="2" class="SubmitBox">
	
            <input type="submit" value="{$lng.lbl_save|escape}" onclick="return validateComm('save');" />
	
	

	<input type="reset" value="{$lng.lbl_cancel|escape}"   />
	</td>
</tr>



 <tr>
  <td colspan="2"><br /><br /></td>
</tr>
</table>

<!-- Display the commission made -->
{/if}
{if $comm_history}
<table cellpadding="3" cellspacing="1" width="100%">

 <tr>
   <td colspan="4"><br /><br />{include file="main/subheader.tpl" title=$lng.lbl_comm_payment_header}</td>
</tr>
		<tr class="TableHead">
			<td width="15%">Payment Amount(Rs)</td>
			<td width="25%">Payment Date</td>
			<td width="15%">Payment method</td>
			<td width="15%" align="center">Status</td>
			<td width="15%" align="center">Comments</td>
		</tr>
{*if $comm_history*}
{assign var=total value=0}
{section name=num loop=$comm_history}

<tr{cycle values=", class='TableSubHead'"}>
	<td align="center"><b>{$comm_history[num].payment_amt}</b></td>
	<td align="center"><b>{$comm_history[num].payment_date}</b></td>
	<td align="center"><b>{$comm_history[num].payment_method}</b></td>
	<td align="center"><b>{$comm_history[num].status}</b></td>
	<td align="center"><b>{$comm_history[num].comments}</b></td>
	{assign var=total value=$total+$comm_history[num].payment_amt}
</tr>

{/section}
<tr>
   <td colspan="5" align="left" style="padding-left:10px""><hr/><br/><b>Total:&nbsp;{$total|string_format:"%.2f"}</b></td>
</tr>
{*/if*}

</table>
{/if}
<!-- Display the commission made -->

</form>
{/if}
{/capture}
{include file="dialog.tpl" title='Affiliate Commission Payment' content=$smarty.capture.dialog extra='width="100%"'}
<br /><br />

