Ext.onReady(function() {
	Ext.QuickTips.init();
	var myMask = new Ext.LoadMask(Ext.getBody(), {msg: "Loading..."});
	Ext.Ajax.on('beforerequest', myMask.show, myMask);
	Ext.Ajax.on('requestcomplete', myMask.hide, myMask);
	Ext.Ajax.on('requestexception', myMask.hide, myMask);
	
	function renderOverviewHtmlToPanel(responseData) {
		var results = responseData['results'];
		var rows=[];
		
		var data_24_hours = results[0];
		var data_7_days = results[1];
		var data_30_days = results[2];
		
		for(var i in data_24_hours) {
			rows.push({'col1':i, 'col2': data_24_hours[i], 'col3': data_7_days[i], 'col4': data_30_days[i]});
		}
		
		var dataStore = new Ext.data.JsonStore({
	        fields: [
	           {name: 'col1'},
	           {name: 'col2'},
	           {name: 'col3'},
	           {name: 'col4'}
	        ]
	    });
		dataStore.loadData(rows);
		
		var grid = new Ext.grid.GridPanel({
			layout: 'table',
			renderTo: 'returns-overview-panel',
	        store: dataStore,
	        columns: [
	            {dataIndex: 'col1', 'header': "", width: 240},
	            {dataIndex: 'col2', 'header': "Last 48 Hours", width: 240},
	            {dataIndex: 'col3', 'header': "Last 7 Days", width: 240},
	            {dataIndex: 'col4', 'header': "Last 30 Days", width: 240}
	        ],
	        title: "Returns Overview",
	        stripeRows: true,
	        width: 960,
	        autoHeight : true,
			autoScroll : true,
			collapsible: true,
	    });
	}
	
	function loadOverviewGrid() {
		Ext.Ajax.request({
			url : 'sladashboard.php',
			method:'POST',
			params: {'action': 'overview'},
			success: function(response){
				var responseData = Ext.decode(response.responseText);
				renderOverviewHtmlToPanel(responseData);
			}
		});
	}
	
	function renderPendingReturnsHtmlToPanel(responseData) {
		var results = responseData['results'];
		var rows=[];
		
		var data_48_hours = results[0];
		var data_7_days = results[1];
		var data_30_days = results[2];
		
		for(var i in data_48_hours) {
			rows.push({'col1':i, 'col2': data_48_hours[i], 'col3': data_7_days[i], 'col4': data_30_days[i]});
		}
		
		var dataStore = new Ext.data.JsonStore({
	        fields: [
	           {name: 'col1'},
	           {name: 'col2'},
	           {name: 'col3'},
	           {name: 'col4'}
	        ]
	    });
		dataStore.loadData(rows);
		
		var grid = new Ext.grid.GridPanel({
			layout: 'table',
			renderTo: 'pending-returns-panel',
	        store: dataStore,
	        columns: [
	            {dataIndex: 'col1', 'header': "", width: 240},
	            {dataIndex: 'col2', 'header': "More Than 48 Hours", width: 240},
	            {dataIndex: 'col3', 'header': "More Than 7 Days", width: 240},
	            {dataIndex: 'col4', 'header': "More Than 30 Days", width: 240}
	        ],
	        title: "Pending Returns Overview",
	        stripeRows: true,
	        width: 960,
	        autoHeight : true,
			autoScroll : true,
			collapsible: true,
	    });
	}
	
	function loadPendingReturnsGrid() {
		Ext.Ajax.request({
			url : 'sladashboard.php',
			method:'POST',
			params: {'action': 'pendingReturns'},
			success: function(response){
				var responseData = Ext.decode(response.responseText);
				renderPendingReturnsHtmlToPanel(responseData);
			}
		});
	}
	
	loadOverviewGrid();
	loadPendingReturnsGrid();
	
	
	// SLA Complicance REport SEction
	
	var searchPanel = new Ext.form.FormPanel({
		title : 'Returns SLA Compliance Report',
		id : 'formPanel',
		autoHeight : true,
		collapsible : true,
		border : true,
		bodyStyle : 'padding: 5px',
		width : 1000,
		items : [{
			layout : 'table',
			border : false,
			layoutConfig : {columns : 2},
			defaults : {width : 450, border : false, 
				layout : 'form', bodyStyle : 'padding-left : 15px'},
			items : [
			         {
			        	 items : [{
			        		 xtype : 'datefield',
			        		 name : 'from_date',
			        		 width : 150,
			        		 fieldLabel : "Return Queued Date From",
			        		 maxValue : new Date()
			        	 }]
			         },{
			        	 items : [{
			        		 xtype : 'datefield',
			        		 name : 'to_date',
			        		 width : 150,
			        		 fieldLabel : "Return Queued Date To",
			        		 maxValue : new Date()
			        	 }]
			         }
			]
		}],
		buttons : [ {
			text : 'Search',
			handler : function() {
				reloadSlaComplianceData();
			}
		}],
	});
	
	function reloadSlaComplianceData() {
		var form = searchPanel.getForm();
		if(form.findField("from_date").getRawValue() == "" || 
				form.findField("to_date").getRawValue() == "" ) {
			Ext.MessageBox.alert('Error', 'Date range can not be empty');
			return false;
		}
		var params = {};
		params["from_date"] = form.findField("from_date").getRawValue();
		params["to_date"] = form.findField("to_date").getRawValue();
		params["action"] = "slaDetails";
		
		searchResultsStore.baseParams = params;
		searchResultsStore.load();
	}
	
	var colModel = new Ext.grid.ColumnModel({
		defaults: {
	        sortable: false
	    },
	    columns: [
	        new Ext.grid.RowNumberer(),
	        {id : 'action', header : "Action", dataIndex : 'action', width: 300},
	        {id : 'sla', header : "Expected SLA (In Hrs)", dataIndex : 'sla', width: 150},
	        {id : 'total', header : "Total Transitions Count", dataIndex : 'total', width: 150},
	        {id : 'tat', header : "Average TAT (In mins)", dataIndex: 'tat', width: 150},
	        {id : 'compliance', header : "% Compliance", dataIndex : 'compliance', width: 150}
  	  	]
	});
	
	var searchResultsStore = new Ext.data.Store({
		url: 'sladashboard.php',
		reader: new Ext.data.JsonReader({
	  		root: 'results',
	  	  	totalProperty: 'count',
	  	  	fields : ['action','sla','total','compliance','tat']
		}),
	});
	
	var searchResults = new Ext.grid.EditorGridPanel({
	    store  : searchResultsStore,
	    cm : colModel,
	    id : 'searchResults-panel',
	    title : 'Returns SLA Info',
	    frame : true,
		loadMask : false,
		autoHeight : true,
		stripeRows : true,
		autoScroll : true,
		viewConfig : {
			enableRowBody : true
		}
	});
	
	var mainPanel = new Ext.Panel({
		renderTo : 'returns-sla-panel',
		border : true,
		items : [
		     searchPanel, searchResults
		]
	});
	
});