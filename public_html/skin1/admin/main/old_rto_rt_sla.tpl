<script type="text/javascript">
	var overDate = '{$dt}';
	var sladt1 = '{$dt1}';
	var sladt2 = '{$dt2}';
</script>
<script type="text/javascript" src="{$http_location}/skin1/js_script/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="{$http_location}/skin1/js_script/calender.js"></script>
<link rel="stylesheet" type="text/css" href="{$http_location}/admin/extjs/resources/css/ext-all.css"/>
<script type="text/javascript" src="{$http_location}/admin/extjs/adapter/ext/ext-base.js"></script>
<script type="text/javascript" src="{$http_location}/admin/extjs/ext-all.js"></script>
<script type="text/javascript" src="sla_rtos.js"></script>

</br>
</br>
<div style="text-align:center;font-size:1.5em;">{$returntype} SLA Dashboard</div>
</br>
</br>
<div id='contentPanel1'>
<div id='beforedate' align="center"></div>
<table align="center" width="600">
	<tr>
		<td>&nbsp;</td>
		<th style="font-size:1.05em;align:center">Yesterday</th>
		<th style="font-size:1.05em;align:center">Last 7 Days</th>
		<th style="font-size:1.05em;align:center">Last 30 Days</th>
	</tr>
	{foreach from=$overview key=k item=v}
		<tr>
			<td style="font-size:1.05em;">{$k}</td>
			{foreach from=$v item=v2}
				<td style="text-align:center">{$v2}</td>
			{/foreach}
		</tr>	
	{/foreach}
</table>
</div>

<br />
<br />
<br />

<div id="controlpanel2">
	<div id='sladate' align="center"></div>
	<table  align="center"  width="600">
		<tr>
			<td>&nbsp;</td>
			<th style="font-size:1em;align:center;text-align:center">SLA</th>
			<th style="font-size:1em;align:center;text-align:center">Avg. TAT</th>
			<th style="font-size:1em;align:center;text-align:center">% Compliance</th>
			<th style="font-size:1em;align:center;text-align:center"># Current Orders</th>
		</tr>
		<tr></tr>
		{foreach from=$stateorder item=data}
			<tr>
				<td style="font-size:1.05em;width:220">{if $statedescr[$data]} {$statedescr[$data]} {else} {$data} {/if}</td>
				<td style="text-align:center">{if $complsla[$data]} {$complsla[$data]} {else} 0d 00h:00m {/if}</td>
				<td style="text-align:center">{if $compltat[$data]} {$compltat[$data]} {else} 0d 00h:00m {/if}</td>
				<td style="text-align:center">{if $complcompl[$data]} {$complcompl[$data]} {else} 0 {/if}</td>
				<td style="text-align:center">{if $complcount[$data]} {$complcount[$data]} {else} 0 {/if}</td>
			</tr>
		{/foreach}
	</table>
</div>


