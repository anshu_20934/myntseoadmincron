{* $Id: featured_products.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
{include file="main/include_js.tpl" src="main/popup_product.js"}
{literal}
  <script language="Javascript">
      function confirmDelete()
      {
          var flag = confirm('Are you sure you want to delete?')
          if(!flag)
          {
             return false;
          }
          else
          {
             document.producttypeform.mode.value = 'deltype';
             document.producttypeform.submit();
          	 return true;
          }
      }
  </script>
{/literal}

<a name="featured" />

{$lng.txt_featured_products}

<br /><br />

{capture name=dialog}

{if $producttype ne ""}
{*include file="main/check_all_row.tpl" style="line-height: 170%;" form="producttypeform" prefix="posted_data.+to_delete"*}
{/if}

<form action="product_type_action.php" method="post" name="producttypeform">
<input type="hidden" name="mode" />


<table cellpadding="3" cellspacing="1" width="100%">

<tr class="TableHead">
	<td width="10">&nbsp;</td>
	<td width="40%">{$lng.lbl_product_type}</td>
	<td width="20%">#{$lng.lbl_product_styles}</td>
	<td width="15%" align="center">#{$lng.lbl_product_num}</td>
	<td width="15%" align="center">{$lng.lbl_active}</td>
	<td width="10%" align="center">{$lng.lbl_pos}</td>
</tr>

{if $producttype}

{section name=prod_num loop=$producttype}

<tr{cycle values=", class='TableSubHead'"}>
	<td style="left-padding:10px"><input type="checkbox" name="posted_data[{$producttype[prod_num].id}][delete]"   /></td>
	<td align="left" style="left-padding:30px"><b>{$producttype[prod_num].name}</b></td>
	<td align="center"><b>{if $producttype[prod_num].styles eq 0 }0 {else}{$producttype[prod_num].styles}{/if}</b></td>
	<td align="center"><b>{if $no_of_products[prod_num] eq 0 }0 {else}{$no_of_products[prod_num]}{/if}</b></td>
	<td align="center">
	<select name="posted_data[{$producttype[prod_num].id}][avail]">
		<option value="1"{if $producttype[prod_num].is_active eq "1"} selected="selected"{/if}>{$lng.lbl_yes}</option>
		<option value="0"{if $producttype[prod_num].is_active eq "0"} selected="selected"{/if}>{$lng.lbl_no}</option>
	</select>
	</td>
	<td align="center"><input type="text" name="posted_data[{$producttype[prod_num].id}][order_by]" size="5" value="{$producttype[prod_num].display_order}" /></td>

</tr>

{/section}



{else}

<tr>
<td colspan="5" align="center">{$lng.txt_no_product_types}</td>
</tr>

{/if}

<!--<tr>
<td colspan="5"><br /><br />{include file="main/subheader.tpl" title=$lng.lbl_add_product}</td>
</tr> -->


<tr>
	<td colspan="5" class="SubmitBox">
	<!--<input type="button" value="{$lng.lbl_delete_selected|escape}" onclick="return confirmDelete();" />-->
	<input type="submit" value="{$lng.lbl_add|escape}"  onclick="javascript: submitForm(this, 'add');"/>
        <input type="submit" value="{$lng.lbl_modify_selected|escape}" onclick="javascript: submitForm(this, 'modify');" />
	<input type="submit" value="{$lng.lbl_upd_selected|escape}" onclick="javascript: submitForm(this, 'update');" />
	</td>
</tr>

</table>
</form>

{/capture}
{include file="dialog.tpl" title=$lng.lbl_producttype_heading content=$smarty.capture.dialog extra='width="100%"'}
<br /><br />

