{* $Id: history_order.tpl,v 1.83.2.6 2006/06/27 08:08:06 max Exp $ *}
{include file="page_title.tpl" title="Reassign Warehouse For Items"}

<link rel="stylesheet" href="{$SkinDir}/skin1_admin_operations.css" />

<script type="text/javascript">
    var orderId = "{$orderid}";
</script>
{include file="main/include_js.tpl" src="/admin/main/order_details.js"}

{capture name=dialog}
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        {section name=idx loop=$productsInCart}
            <tr {if $productsInCart[idx].is_customizable eq '0'}style="background-color:#ffbb00"{/if}>
                <div id="msgBar_{$productsInCart[idx].itemid}" style="text-align:center; font-weight: bold; margin: 10px;"></div>
            </tr>
            <tr {if $productsInCart[idx].is_customizable eq '0'}style="background-color:#ffbb00"{/if}>
                <td width="130" style="padding-left: 5px;">
                    {if $productsInCart[idx].designImagePath != ""}
                        <img src="{$productsInCart[idx].designImagePath}"  style="border:1px solid #CCCCCC;">
                    {else}
                        <img src="{$xcart_web_dir}/image.php?type={$type}&amp;id={$id}&amp;ts={$smarty.now}" width="100"  height="70" hspace="5" vspace="5" />
                    {/if}
                </td>
                <td width=350>
                    <div style="width: 30%; float:left;">Style:</div>
                    <div style="width: 60%; float:left;">{$productsInCart[idx].productStyleLabel}</div>
                    <div class="clear"></div>
                    <br>
                    <div style="width: 30%; float:left;">Article Id:</div>
                    <div style="width: 60%; float:left;">{$productsInCart[idx].article_number}</div>
                    <div class="clear"></div>
                    <br>            
                    <div style="width: 30%; float:left;">Size:Quantity</div>
                    <div style="width: 60%; float:left;">{$productsInCart[idx].size}:{$productsInCart[idx].amount}</div>
                    <div class="clear"></div>
                </td>     
                {if $productsInCart[idx].item_status == 'A' || $productsInCart[idx].item_status == 'RP'}
	                <td width=30%>
	                    {$productsInCart[idx].size}:
	                    <select class="item_qty_select" id="item_quantity_{$productsInCart[idx].itemid}" name="item_quantity_{$productsInCart[idx].itemid}" style="width:150px;">
	                        <option value="0">--Select no of pieces--</option>
	                        {section name=quantity loop=$productsInCart[idx].amount }
	                            <option value="{$smarty.section.quantity.iteration}">{$smarty.section.quantity.iteration}</option>
	                        {/section}
	                    </select>
	                </td>
	                <td> 
	                    <div style="float:left; width: 150px;">Applicable Warehouses:</div>
	                    <div style="float:left; width: 100px;" id="item_whinfo_{$productsInCart[idx].itemid}">None</div>
	                    <div class="clear"></div>
	                </td> 
                {else}
                     <td colspan="2">Item warehouse cannot be changed now. Item Status - {$productsInCart[idx].item_status}</td>               
                {/if}                     
            </tr>
            <tr {if $productsInCart[idx].is_customizable eq '0'}style="background-color:#ffbb00"{/if}>
                <td colspan="4" align=right>
                    <input type=button name="confirm-btn" id="confirm-btn-{$productsInCart[idx].itemid}" value="Reassign" style="padding:5px;" disabled onclick="reassignConfirm({$productsInCart[idx].itemid});" />
                </td>
            </tr>
            <tr height=20><td colspan=5><hr/></td></tr>
        {/section}
    </table>
    <div style="text-align:right;">
        <input type=button name="return-btn" id="return-btn" value="Close Window" style="padding:5px 10px;" onclick="opener.window.location.reload(); self.close();"/>
    </div>
    <input type="hidden" name="warehouseid" id="warehouseid" value="{$current_warehouseid}">
{/capture}
{include file="dialog.tpl" title="Reassign Warehouses" content=$smarty.capture.dialog extra='width="100%"'}