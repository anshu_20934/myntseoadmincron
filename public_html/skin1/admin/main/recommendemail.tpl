<div style="border: 1px solid gray; border-top: none;">
	<div style="font-family: arial, sans-serif;  padding: 10px">
		<P style="text-transform: capitalize">Dear {$customerName},</P>
		<P>Thank you for being a privileged customer of Myntra.com. Hope your shopping experience with us has been delightful.</P>
		<P>We are happy to offer you <b>additional 10% off</b> on your next purchase on Myntra.</P> 
		<!-- This would be in addition to any discounts that are running already (We have discounts running on most major brands, if you haven't checked the site recently). -->
		<span style="color: #500050">To avail this offer, use the coupon code: <B style="text-transform: uppercase">{$couponCode}</B> during checkout. This offer is <b>valid upto {$couponValidUpto}</b> only.</span> 
		<P>To help you choose, we are recommending some products based on your interests.</P>
		
		<div style="margin:0 auto;width:95%;font-family:arial,serif;font-size:12px;padding:5px;overflow:hidden;">
			{foreach key=index item=product from=$recommendations}				
			<div style="float:left;width:250px;height:132px;padding-right:4px;margin-right: 12px;margin-bottom:10px;border: 1px dotted #999;">
				<a href="{$product.landingpageurl}">
					<img style="padding:1px;margin-right:6px; border:1px solid #eee;float:left" src="{$product.image}" alt="{$product.name}"/>
				</a>
				<br>
				<a href="{$product.landingpageurl}" style="color:#006699;margin-bottom:10px;display:block;margin-right:4px">
					{$product.name}
				</a>
				{if $product.discountedprice neq $product.mrp}
					<span style="text-decoration: line-through">Rs {$product.mrp}</span> ({$product.discounttext} OFF)
					<b style="color:#990000;margin-top:3px;margin-bottom:3px;display:block;">Now Rs {$product.discountedprice}</b>
					<span style="font-size:11px;color:#666;">{$product.yousave}</span>
				{else}
					<span style="color:#990000;margin-top:3px;margin-bottom:3px;font-weight:bold">Rs {$product.mrp}</span> <BR>
				{/if}
			</div>
			{/foreach}
		</div>
		<BR/>
		At any point if you need assistance, our 24X7 customer care will be glad to help you complete your order or answer any other queries that you may have. 
		<BR/><BR/>
		Regards,<BR/>
		Team Myntra
		<BR/>
		<div style="font-size: 90%; height: 35px; background-color: white; margin: 0pt 0pt 0px;">
		   <a href="mailto:support@myntra.com">support@myntra.com</a> | Call: +91-80-43541999 - 24x7 | <a href="http://www.myntra.com?utm_source=mlr&utm_medium=per_mail&utm_campaign={$campaignDate}">www.myntra.com</a>
  		</div> 
	</div>
</div>