{* $Id: featured_products.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
{include file="main/include_js.tpl" src="main/popup_product.js"}
<link rel="stylesheet" href="{$SkinDir}/skin1_admin_operations.css" />

<h1><center> <B> LATE ORDER REPORT </B></center></h1>
{capture name=dialog}
{include file="customer/main/navigation.tpl"}
{if $mode == "display"}

    {if $lateorders}
	<table><tr>
    <td><a href="../admin/late_order_report.php?type=times">Click Here for Processing Time for different products.</a></td>
	<td></td>
	<td></td>
	<td></td>
	<td></td>
    <td><a href="../admin/late_order_report.php?type=lateproducts">Click Here for LATE ORDER COUNT BY PRODUCT TYPE</a></td>
	</tr></table><BR><BR>
    <table cellpadding="2" cellspacing="1" width="40%" >
        <tr class="TableHead">
            <td width="20%" nowrap="nowrap"> Status</td>
            <td width="20%" nowrap="nowrap"> Outstanding LATE Orders</td>
        </tr>
            {section name=ind loop=$lateorderstatus}

            <tr class='TableSubHead'>
                <td>{$lateorderstatus[ind].status} </td>
                <td>{$lateorderstatus[ind].count} </td>
            </tr>
            {/section}
            <tr class='TableSubHead'>
                <td><B>Total</B> </td>
                <td><B>{$totallatecount} </B></td>
            </tr>

    </table><br/><p><B>LATE ORDER LIST</B></p><br>
    <table cellpadding="2" cellspacing="1" width="100%" >
        <tr class="TableHead">
            <td width="10%" nowrap="nowrap"> Ops Location</td>
            <td width="10%" nowrap="nowrap"> Order Name</td>
            <td width="10%" nowrap="nowrap"> orderid</td>
            <td width="15%" nowrap="nowrap"> Item Name</td>
            <td width="10%" nowrap="nowrap"> Item Status</td>
            <td width="10%" nowrap="nowrap"> Order Status</td>
            <td width="4%" nowrap=""> Days Queued</td>
            <td width="13%" nowrap="nowrap"> Order Date</td>
            <td width="13%" nowrap="nowrap"> Estimated proc Date</td>
            <td width="5%" nowrap="nowrap"> Days Late</td>
        </tr>
            {section name=oid loop=$lateorders}

                {if $prevorderid != $lateorders[oid].orderid}
                <tr class='TableSubHead'>
                <td>{$lateorders[oid].location_name} </td>
                <td><a href="order.php?orderid={$lateorders[oid].orderid}">#{$lateorders[oid].order_name}</a> </td>
                <td><a href="order.php?orderid={$lateorders[oid].orderid}">#{$lateorders[oid].orderid}</a> </td>
                {else}
                <tr>
                <td></td>
                <td></td>
                <td></td>
                {/if}
                <td>{$lateorders[oid].style_name} </td>
                <td>{$lateorders[oid].item_status} </td>
                <td>{$lateorders[oid].order_status} </td>
                <td>{$lateorders[oid].queued_age} </td>
                <td>{$lateorders[oid].order_date} </td>
                <td>{$lateorders[oid].estimated_time} </td>
                <td>{$lateorders[oid].late_age} </td>
                </tr>
            {assign var=prevorderid value=$lateorders[oid].orderid}
            {/section}

    </table>
    {/if}
    {if $processingtimes}
            <table cellpadding="2" cellspacing="1" width="100%" >
            <tr class="TableHead">
                <td width="50%" nowrap="nowrap"> Product Type</td>
                <td width="50%" nowrap="nowrap"> Processing Time in Hours</td>
            </tr>
                {section name=oid loop=$processingtimes}

                <tr class='TableSubHead'>
                    <td>{$processingtimes[oid].name} </td>
                    <td>{$processingtimes[oid].time_in_hours} </td>

                </tr>
                {/section}

        </table>
    {/if}
    {if $lateproducts}
            <table cellpadding="2" cellspacing="1" width="100%" >
            <tr class="TableHead">
                <td width="50%" nowrap="nowrap"> Product Name</td>
                <td width="25%" nowrap="nowrap"> Total Quantity Late</td>
                <td width="25%" nowrap="nowrap"> Orders effected</td>
            </tr>
                {section name=oid loop=$lateproducts}

                <tr class='TableSubHead'>
                    <td>{$lateproducts[oid].product_name} </td>
                    <td>{$lateproducts[oid].count} </td>
                    <td>{$lateproducts[oid].ordercount} </td>

                </tr>
                {/section}

        </table>
    {/if}

{/if}
{/capture}
{include file="dialog.tpl" title="$title" content=$smarty.capture.dialog extra='width="100%"'}
