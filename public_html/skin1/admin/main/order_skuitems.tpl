{* $Id: order_skuitems.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
{if $order_skuItems }
		<div style="padding: 20px;">
			<table cellspacing=0 cellpadding=0 border=0 width=100% style="border-collapse: collapse;">
				<tr height=30 style="background-color: #ccffff">
					<th style="font-size:14px; padding: 2px;">&nbsp;</th>
					<th style="font-size:14px; padding: 2px;">Item Barcode</th>
					<th style="font-size:14px; padding: 2px;">Sku Code</th>
					<th style="font-size:14px; padding: 2px;">Vendor Article Number</th>
					<th style="font-size:14px; padding: 2px;">Vendor Article Name</th>
					<th style="font-size:14px; padding: 2px;">Size</th>
					<th style="font-size:14px; padding: 2px;">Bin Barcode</th>
					<th style="font-size:14px; padding: 2px;">Item Status</th>
					<th style="font-size:14px; padding: 2px;">Action</th>
				</tr>
				{section name=idx loop=$order_skuItems}
					<tr height=30 bgcolor="{cycle values="#eeddff,#d0ddff"}">
						<td></td>
						<td>{$order_skuItems[idx].itemBarCode}</td>
						<td>{$order_skuItems[idx].skuCode}</td>
						<td>{$order_skuItems[idx].vendorArticleNo}</td>
						<td>{$order_skuItems[idx].vendorArticleName}</td>
						<td>{$order_skuItems[idx].size}</td>
						<td>{$order_skuItems[idx].binbarcode}</td>
						<td>{$order_skuItems[idx].itemStatus}</td>
						<td>
							<form action="" method="post" onsubmit="return confirmItemRemoveAction({$order_skuItems[idx].itemBarCode});">
								<input type="hidden" name="mode" value="removeskuitem">
								<input type="hidden" name="userid" value="{$login}">
								<input type="hidden" name="skuItemCode" value="{$order_skuItems[idx].itemBarCode}">
								<input type="hidden" name="skuId" value="{$order_skuItems[idx].skuId}">
								{if $order_skuItems[idx].itemStatus eq 'ISSUED'}
									<input type="submit" value="Remove Item From Order">
								{else}
									<input type="submit" value="Remove Item From Order" disabled>
								{/if}
							</form>
						</td>
					</tr>
				{/section}
			</table>
		</div>
{else}
	There are no items issued for this order	
{/if}
