{* $Id: featured_products.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
{include file="main/include_js.tpl" src="main/popup_product.js"}

{capture name=dialog}

<form name="add_assignee" action="item_assignee.php" method="post">
	<input type="hidden" name="mode" id="mode">
		<div class="ITEM_PARENT_DIV" style="width:650px;">
		<div class="ITEM_FIRST_CHILD_DIV" style="width:auto;">
        <div class="ITEM_LABLE_DIV">Assignee Name</div>
        <div class="ITEM_FIELD_DIV" style="width:auto;">
            <input type="text" name="ass_name" id="ass_name" maxlength="128">
            <input type="submit" value="{$lng.lbl_add|escape}" onclick="javascript: document.add_assignee.mode.value = 'add_ass'; document.add_assignee.submit();" />
        </div> 
        </div>
        </div>
</form>


{/capture}
{include file="dialog.tpl" title="Add assignee" content=$smarty.capture.dialog extra='width="100%"'}
<br /><br />


{capture name=dialog}
<form action="item_assignee.php" method="post" name="assigneeform">
<input type="hidden" name="mode" />
<table cellpadding="3" cellspacing="1" width="60%">
<tr class="TableHead">
	<td width="40%">Assignee name</td>
	<td width="20%">{$lng.lbl_active}</td>
</tr>

{if $assignee}

{section name=prod_num loop=$assignee}
<tr{cycle values=", class='TableSubHead'"}>
	
	<td align="left" style="left-padding:30px"><b>{$assignee[prod_num].name}</b></td>
	<td align="center">
	<select name="ass_status[{$assignee[prod_num].id}][avail]">
		<option value="1"{if $assignee[prod_num].status eq "1"} selected="selected"{/if}>{$lng.lbl_yes}</option>
		<option value="0"{if $assignee[prod_num].status eq "0"} selected="selected"{/if}>{$lng.lbl_no}</option>
	</select>
	</td>
</tr>
{/section}
{else}
<tr>
<td colspan="2" align="center">no assignee added as yet</td>
</tr>
{/if}
<tr>
	<td colspan="5" class="SubmitBox" align="right">
	   <input type="submit" value="{$lng.lbl_upd_selected|escape}" onclick="javascript: document.assigneeform.mode.value = 'update_ass'; document.assigneeform.submit();" />
	</td>
</tr>

</table>
</form>

{/capture}
{include file="dialog.tpl" title="Assignee List" content=$smarty.capture.dialog extra='width="100%"'}
