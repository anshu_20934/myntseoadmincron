<script type="text/javascript">
	var loginUser = "{$login}";
	var couriers =  {$couriers};
	var warehouseData =  {$warehouses};
	var paymentMethods =  {$payment_methods};
	var httpLocation = "{$http_location}";
	var progressKey = "{$progresskey}";
</script>

<link rel="stylesheet" type="text/css" href="{$http_location}/admin/extjs/resources/css/ext-all.css"/>
<script type="text/javascript" src="{$http_location}/admin/extjs/adapter/ext/ext-base.js"></script>
<script type="text/javascript" src="{$http_location}/admin/extjs/ext-all.js"></script>
<script type="text/javascript" src="{$http_location}/admin/extjs/plugins/superselectbox/SuperBoxSelect.js"></script>
<link rel="stylesheet" type="text/css" href="{$http_location}/admin/extjs/plugins/superselectbox/superboxselect.css"/>
<script type="text/javascript" src="/skin1/admin/main/update_delivered_orders.js"></script>
<br>
<div id="contentPanel"></div>
<div id="uploadstatuspopup" style="display:none">
	<div class="overlay" style="display:block;_position:absolute;_width:1300px;_height:1500px;">&nbsp;</div>
	<div id="uploadstatuspopup-main" style="position:absolute; left: 20%; width:450px; background:white; z-index:1006;border:2px solid #999999;">
		<div id="uploadstatuspopup-content" style="display:none;">
		    <div id="uploadstatuspopup_title" class="popup_title">
		    	<div id="uploadstatuspopup_ajaxWindowTitle" class="popup_ajaxWindowTitle">Upload Status</div>
				<div class="clear">&nbsp;</div>
			</div>
			<div style="padding:5px 25px; text-align:left; font-size:12px" >
				<div id="progress_container"> 
				    <div id="progress_bar">
				    	<div id="progress_completed"></div> 
				    </div>
				</div>
				<span id="progress_text" style="font-size: 14px; color: green; padding: 0 0 0 5px;">0</span> % Complete
			</div>
			<div class="clear">&nbsp;</div>
		</div>
	</div>
</div>
