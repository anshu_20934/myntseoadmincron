<script type="text/javascript" src="{$http_location}/skin1/js_script/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="{$http_location}/skin1/js_script/calender.js"></script>
{literal}
<script type="text/javascript">

	$(document).ready(function(){
		enableDisableFormControls();
		
		$('#resolution_lbl').hide();
		$('#resolution').attr('disabled', 'disabled');
		$('#resolution_input').hide();	

		$('#cc_feed_back').hide();
		
		$('#ccdisposition').change(function(){
			if ($(this).val() == 'CM') {
				$('#resolution_lbl').show();
				$('#resolution').removeAttr('disabled');
				$('#resolution_input').show();			
			} else {
				$('#resolution_lbl').hide();
				$('#resolution').attr('disabled', 'disabled');
				$('#resolution_input').hide();			
			}
		});			

		$('#rto_action').change(function(){
			var action = $(this).val();
			var iscod  = $('#iscod').val();
			
			if(action == 'RTOCAL1' ||action == 'RTOCAL2'||action == 'RTOCAL3'){
				$('#cc_feed_back').show();
			} else{
				$('#cc_feed_back').hide();
			}
            if (action == 'RTOC') {
                       		$('tr#cancel_row').show();
				$('#rto_cancel_reason').removeAttr('disabled');				 
			} else {
                        	$('#rto_cancel_reason').attr('disabled', 'disabled');
				$('tr#cancel_row').hide();
				if (action == 'RTORF') {
					if (iscod == '0') {
						$('#coupon_row').show();
                                		$('#coupon_code').removeAttr('disabled');
                                		$('#coupon_value').removeAttr('disabled');                              
                                		$('#items_rej_val').attr('disabled', 'disabled');
                                		$('#items_rstk_val').attr('disabled', 'disabled');
                                		$('#restock_row').hide();		
					}	
				} else if (action == 'RTOAI') {
					$('#coupon_row').hide();
                                	$('#coupon_code').attr('disabled', 'disabled');
                                	$('#coupon_value').attr('disabled', 'disabled');                                
                                	$('#items_rej_val').removeAttr('disabled');
                               	 	$('#items_rstk_val').removeAttr('disabled');
                                	$('#restock_row').show();
				} else {
					$('#coupon_row').hide();
                                	$('#coupon_code').attr('disabled', 'disabled');
                                	$('#coupon_value').attr('disabled', 'disabled');                                
                               	 	$('#items_rej_val').attr('disabled', 'disabled');
                                	$('#items_rstk_val').attr('disabled', 'disabled');
                                	$('#restock_row').hide();
				}	
			}      		 
                });
	});

	function enableDisableFormControls(){
		var currst = $('#currst').val();
		$('#order_id').attr('readonly', 'readonly');
		$('#curr_st').attr('readonly', 'readonly');
		$('#cust_name').attr('readonly', 'readonly');
		$('#cust_addr').attr('readonly', 'readonly');
		$('#cust_city').attr('readonly', 'readonly');
		$('#cust_state').attr('readonly', 'readonly');
		$('#cust_country').attr('readonly', 'readonly');
		$('#cust_zipcode').attr('readonly', 'readonly');	
		$('#courier').attr('disabled', 'disabled');
		$('#tracking_num').attr('readonly', 'readonly');
		$('#returnreason').attr('readonly', 'readonly');
					
		$('#cancel_row').hide();	
		$('#comment_row').show();
		$('#update_btn').hide();	
		$('#add_btn').show();	

		$('#coupon_row').hide();
                $('#restock_row').hide();

		if (currst == '') {
			$('#cust_addr').removeAttr('readonly');
			$('#cust_city').removeAttr('readonly');
			$('#cust_state').removeAttr('readonly');
			$('#cust_country').removeAttr('readonly');
			$('#cust_zipcode').removeAttr('readonly');
			$('#courier').removeAttr('disabled');
			$('#tracking_num').removeAttr('readonly');
			$('#returnreason').removeAttr('readonly');
			
		} else if (currst == 'RTORSC') {
			$('#add_btn').hide();
			$('#update_btn').hide();
			$('#comment_row').hide();	
		} else if (currst == 'RTORS') {
			$('#courier').removeAttr('disabled');
			$('#tracking_num').removeAttr('readonly');
			$('#update_btn').show();
			$('#add_btn').hide();
		} else if (currst == 'END') {
			$('#add_btn').hide();
			$('update_btm').hide();
			$('#courier_row').remove();	
			$('#comment_row').remove();	
		} else {
			$('#update_btn').show();
			$('#add_btn').hide();
		} 

	}

	function updateRTOOrder(){	
		var prevst = $('#currst').val();
		var nextst = $('#rto_action').val();
		var comment = $('#comment').val();
		
		if (nextst == '-') {
			alert("Please select an RTO action");
			return false;
		}	
	
		if (isEmptyString(comment)) {
                                alert('Please add a comment');
                                return false;
         	}
		$('#action').val('update');

		if (nextst == 'RTOC') { 
			if ($('#rto_cancel_reason').val() == '-') { 
				alert("Please select a reason for cancellation"); 
				return false;   
			} 
		}
		
		if (nextst == 'RTORSC') {
			var trackingNo = $('#tracking_num').val();
                        if (isEmptyString(trackingNo)) {
                                alert('Please enter a valid tracking num');
                                return false;
                        }
                        var courier = $('#courier').val();      
                        if(courier == '-') {
                                alert('Please select a courier operator');
                                return false;
                        }
		}

		if (nextst == 'RTOAI') {
			if(!isAmount($('#items_rej_val').val())) {
                                alert("Please enter a valid value for items rejected.");        
                                return false;
                        }               
                        if (!isAmount($('#items_rstk_val').val())) {
                                alert("Please enter a valid value for items restocked.");
                                return false;
                        }	
		}

		if (nextst == 'RTORF') {
			var iscod = $('#iscod').val();
			if(iscod == '0') {
				if (!isAmount($('#coupon_value').val())) {
                                	alert('Please enter a valid amount for refund');
                                	return false;
                        	}
                        
                        	if (isEmptyString($('#coupon_code').val())) {
                                	alert('Please enter a valid coupon code');
                                	return false;
                        	}	
			}
		}
		
		if (nextst == 'RTOCAL1' || nextst == 'RTOCAL2' || nextst == 'RTOCAL3'){
			var ccdisposition = $('#ccdisposition').val();
			if (ccdisposition == '-') {
				alert('Select a customer disposition');
				return false;
			}
			if (ccdisposition == 'CM') {
				var ccresolution = $('#resolution').val();
				if (ccresolution == '-') {
					alert('Select a customer resolution');
					return false;
				}
			}
		}
		
		var overlay = new ProcessingOverlay();
        overlay.show();
		return true;
	}

	function createRTOOrder(){
		var rtoaction	= $('#rto_action').val();
		if (rtoaction != 'RTOQ') {
			alert('RTO Action must be "RTO Queued" for adding an RTO Order');
			return false;
		}	
		var custname	= $('#cust_name').val();
		if (isEmptyString(custname)) {
			alert("Customer Name Should not be empty");
			return false;
		}
		var custaddr	= $('#cust_addr').val();
		if (isEmptyString(custaddr)) {
			alert("Address should not be empty");
			return false;	
		}
		var custcity 	= $('#cust_city').val();
		if(isEmptyString(custcity)) {
			alert("City Should not be empty");
			return false;
		}
		var custstate 	= $('#cust_state').val();
		if(isEmptyString(custstate)) {
			alert("State Should not be empty");
			return false;
		}
		var custcountry = $('#cust_country').val();
		if(isEmptyString(custcountry)) {
			alert("Country Should not be empty");
			return false;
		}
		var custzip 	= $('#cust_zipcode').val();
		if(isEmptyString(custzip)) {
			alert("ZIP Code Should not be empty");
			return false;
		}
		var courier = $('#courier').val();	
		if (courier == '-') {
			alert("Please select a courier operator");
			return false;
		}
		var tracking = $('#tracking_num').val();
		if (isEmptyString(tracking)) {
			alert('Tracking number should not be empty');
			return false;
		}
		var comment = $('#comment').val();
		if (isEmptyString(comment)) {
			alert("Please add a comment");
			return false;
		}
		
		$('#action').val('add');	
		return true;
	}

	function isEmptyString(s){
		return (s.replace(/\s/g,'') == '');	
	}

	function isAmount(amount) {
                var regex = /(^\+?\d+.\d+$)|(^\+?\d+$)/;        
                return regex.test(amount);
        }
		
</script>
{/literal}
<h2 style="text-align:center;">RTO Details</h2>
{if $errormsg }<h2 style="color:red;">{$errormsg}</h2>{/if}
<h3>Order # <a href="{$http_location}/admin/order.php?orderid={$orderData.orderid}">{$orderData.orderid}</a></h3>

<form name="statusForm" action="old_rto_tracking.php" method="post">
	<input type="hidden" id="currst" name="currst" value="{$orderData.currstate}">
	<input type="hidden" id="action" name="action">
	<input type="hidden" id="iscod" name="iscod" value="{$orderData.iscod}">
<table cellspacing="0" cellpadding="0" width="100%">
	<tr>
		<td align="right"><label>Order Id : </label></td>
		<td align="left">
			<input type="text" id="order_id" name="order_id" value="{$orderData.orderid}">
		</td>
	</tr>
	<tr>
        <td align="right"><label>Current State : </label></td>
        <td align="left"><input id="curr_st" type="text" value="{$allstates[$orderData.currstate]}"></td>
		<td align="right"><label>RTO Action : </label></td>
        <td align="left">
          <select id="rto_action" name="rto_action">
		      <option value="-">--- Select an RTO action ---</option>
              {foreach from=$rtoactions key=k item=v}
                  <option value="{$k}">{$v}</option>
              {/foreach}
          </select>
        </td>
	</tr>
	
	<tr id="rto_reason">
		<td align="right"> warehouseid :  </td>
		<td align="left"> &nbsp; {$orderData.wh_name} </td>
		<td align="right"><label>Return Reason:</label></td>
		<td align="left"><input type="text" name="returnreason" id="returnreason" style="width:180" value="{$orderData.returnreason}"></td>
	</tr>
	
    <tr><td colspan="2">&nbsp;</td></tr>
	<tr id="cc_feed_back">
		<td align="right"><label>CC Dispositions</label></td>
		<td align="left">
			<select name="ccdisposition" id="ccdisposition">
				<option value="-">--- Select a CC Disposition ---</option>
				{foreach from=$ccdispositions key=k item=d }
                                        <option value="{$k}">{$d}</option>
                                {/foreach}	
			</select>
		</td>
		<td align="right" id="resolution_lbl"><label>Resolution : </label></td>
		<td align="left" id="resolution_input">
			<select name="resolution" id="resolution">
				<option value="-">--- Select an option ---</option>
				{foreach from=$ccresolution key=k item=d}
					<option value="{$k}">{$d}</option>
				{/foreach}
			</select>
		</td>
	</tr>
	
	<tr id="cancel_row">
		<td colspan="2">&nbsp;</td>
		<td align="right"><label>Cancel Reason</label></td>
		<td align="left">
			<select id="rto_cancel_reason" name="rto_cancel_reason" disabled="disabled">
				<option value="-">--- Select a reson for cancelling ---</option>
				{foreach from=$rtocancelreasons key=k item=v}
					<option value="{$k}">{$v}</option>
				{/foreach}
			</select>
		</td>
	</tr>
	<tr><td colspan="4">&nbsp;</td></tr>
	<tr><td colspan="4">&nbsp;</td></tr>
	
	<tr>
		<td align="right"><label>Customer Name : </label></td>
		<td align="left">
			<input type="text" name="cust_name" id="cust_name" value="{$orderData.custname}">
		</td>
		<td align="right"><label>Customer Address : </label></td>
		<td align="left">
			<textarea id="cust_addr" name="cust_addr" cols="40" rows="2">{$orderData.pickupaddress}</textarea>
		</td>
	</tr>

	<tr>
		<td align="right"><label>City : </label></td>
		<td align="left"><input type="text" name="cust_city" id="cust_city" value="{$orderData.pickupcity}"></td>
		<td align="right"><label>State : </label></td>
		<td align="left"><input type="text" name="cust_state" id="cust_state" value="{$orderData.pickupstate}"></td>
	</tr>

	<tr>
		<td align="right"><label>Country : </label></td>
		<td align="left"><input type="text" name="cust_country" id="cust_country" value="{$orderData.pickupcountry}"></td>
		<td align="right"><label>ZIP Code : </label></td>
		<td align="left"><input type="text" name="cust_zipcode" id="cust_zipcode" value="{$orderData.pickupzipcode}"></td>
	</tr>

	<tr id="address_row4">
                <td align="right"><label>Contact # : </label></td>
                <td align="left"><input type="text" readonly="readonly" name="contact_no" id="contact_no" value="{$orderData.phonenumber}"></td>
                <td colspan="2">&nbsp;</td>
        </tr>

	<tr><td colspan="4">&nbsp;</td></tr>
	<tr><td colspan="4">&nbsp;</td></tr>
	
	<tr id="courier_row">
		<td align="right"><label>Courier : </label></td>
                <td align="left">
                        <select name="courier" id="courier">
				<option value="-">--- Select a Courier Operator ---</option>
                                {foreach from=$couriers key=k item=v}
                                        <option value="{$v.code}"
                                        {if ($v.code == $orderData.courier)}
                                         	selected="true"
                                        {/if}
                                        >{$v.display_name}</option>
                                {/foreach}
                        </select>
                </td>
		<td align="right"><label>Tracking # : </label></td>
		<td align="left"><input type="text" id="tracking_num" name="tracking_num" value={if !is_null($orderData.trackingno)}{$orderData.trackingno}{else}{$orderData.trackingnum}{/if}></td>
	</tr>	

	<tr id="coupon_row">
                <td align="right"><label>Coupon Code : </label></td>
                <td align="left"><input type="text" name="coupon_code" id="coupon_code" disabled="disabled"></td>
                <td align="right"><label>Coupon Value : </label></td>
                <td align="left"><input type="text" name="coupon_value" id="coupon_value" disabled="disabled"></td>
        </tr>

        <tr id="restock_row">
                <td align="right"><label>Rejected Items Value : </label></td>
                <td align="left"><input type="text" name="items_rej_val" id="items_rej_val" disabled="disabled"></td>
                <td align="right"><label>Restocked Items Value : </label></td>
                <td align="left"><input type="text" name="items_rstk_val" id="items_rstk_val" disabled="disabled"></td>
        </tr>

	<tr><td colspan="4">&nbsp;</td></tr>
	
	<tr><td colspan="4">&nbsp;</td></tr>
	<tr id="comment_row">
		<td align="right"><label>Comment : </label></td>
		<td align="left" colspan="3"><textarea wrap="ON" cols="70" rows="6" id="comment" name="comment" value="comment"></textarea></td>
	</tr>	

	<tr><td colspan="4">&nbsp;</td></tr>
	<tr>
		<td>&nbsp;</td>
		<td align="right">
			<input type="submit" id="update_btn" value="Update" onclick="javascript:return updateRTOOrder();" {if not $orderData.canupdate}disabled="disabled"{/if}>
		</td>
		<td align="left"><input type="submit" id="add_btn" value="Create" onclick="javascript:return createRTOOrder();" {if not $orderData.cancreate}disabled="disabled"{/if}></td>
		<td>&nbsp;</td>
	</tr>
</table>
</form>
<br/>

<table border="1">
	<thead><b>Edit History</b></thead>
	
	<tr>
		<th>Activity</th>
		<th>Done by</th>
		<th>Recorded on</th>
		<th>Elapsed Time</th>
		<th>SLA</th>
		<th>Remark</th>	
	</tr>

	{foreach from=$edithistory key=k item=data}
		<tr {if $data.time_since_last_activity > $data.sla_time }style="background:red;"{/if}>
			<td>{$data.activity}</td>
			<td>{$data.adminuser}</td>
			<td>{$data.recorddate}</td>
			<td style="text-align:center;">{$data.time_since_last_activity_descr}</td>
			<td style="text-align:center;">{$data.sla_time_descr}</td>
			<td>{$data.remark}</td>
		</tr>
	{/foreach}		
</table>
