{* $Id: order_replacements_list.tpl,v 1.24.2.1 2008/08/21 11:02:40 max Exp $ *}
{include file="main/include_js.tpl" src="main/popup_product.js" src="main/overlib.js"}
{popup_init src="../skin1/main/overlib.js"}
<a name="featured" />

{$lng.txt_featured_products}

<br /><br />

{capture name=dialog}

<table width="100%"  border="0" cellspacing="0" cellpadding="0">

<tr>
  <td valign="top">
  		<table width="98%"  border="0" align="center" cellpadding="0" cellspacing="0">
		<tr>
		 <td>
			<table width="100%" height="21" border="0" cellpadding="0" cellspacing="0" class="textBlack">

				<tr>
					<td><strong>Replacements sent Log for orderid {$orderid}</strong></td>
				</tr>
			</table>
			<br>
			<table width="100%" height="25" border="1" cellpadding="0" cellspacing="0">
				<tr bgcolor="#ccffff">
					
					<th style="font-size:14px;">Replacement Date</th>
					<th style="font-size:14px;">Item ID</th>
					<th style="font-size:14px;">Quantity</th>
					<th style="font-size:14px;">Reason for Replacement</th>
					<th style="font-size:14px;">Type</th>
					<th style="font-size:14px;">Address</th>
					<th style="font-size:14px;">Amount</th>
					
				</tr>
				{foreach from=$replacementslist item=replacement}
				<tr bgcolor="{cycle values="#eeddff,#d0ddff"}">					
					<td style="font-size:12px;">{$replacement.replacement_date}</td>
					<td style="font-size:12px;">{$replacement.replacement_itemid}</td>
					<td style="font-size:12px;">{$replacement.replacement_quantity}</td>
			        <td><textarea name="query" cols=35 rows=5 readonly>{$replacement.replacement_reason}</textarea></td>
					<td style="font-size:12px;">{$replacement.replacement_type}</td>
			        <td><textarea name="query" cols=35 rows=5 readonly>{$replacement.replacement_address},
						{$replacement.replacement_city},{$replacement.replacement_country}-{$replacement.replacement_zipcode}</textarea></td>
					<td style="font-size:12px;">{$replacement.replacement_amount}</td>
				</tr>				
                {/foreach}    
			</table>
		</table>
</table>
{/capture}
{include file="dialog.tpl" title="Order Replacement Details" content=$smarty.capture.dialog extra='width="100%"'}



