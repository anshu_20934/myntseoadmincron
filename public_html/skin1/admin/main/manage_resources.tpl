{include file="main/include_js.tpl" src="main/popup_product.js}

<link rel="stylesheet" href="{$SkinDir}/skin1_admin.css"/>

{capture name=dialog}
	<table cellpadding="2" cellspacing="1" width="100%">
		<tr class="tablehead">
			<th width="20%" nowrap="nowrap">
				Resource Id
			</th>
			<th width="20%" nowrap="nowrap">
				Resource Name
			</th>
            <th width="20%" nowrap="nowrap">
				Threshold Value
			</th>
            <th width="20%" nowrap="nowrap">
				Operations Location
			</th>
			<th width="20%" nowrap="nowrap">
				Enabled
			</th>
			<th width="20%" nowrap="nowrap">
			</th>
		</tr>
		{if $assignees>0}
			{section name=assignee loop=$assignees}
				<form action="" method="get" name="modify_{$assignees[assignee].id}">
					<tr class="{cycle values=",TableSubHead"}">
						<td align="center">
							{$assignees[assignee].id}
							<input type="hidden" name="id" value="{$assignees[assignee].id}"/>
						</td>
						<td align="center">
							<input type="text" value="{$assignees[assignee].name}" name="name"/>
						</td>
                        <td align="center">
							<input type="text" value="{$assignees[assignee].threshold}" name="threshold"/>
						</td>
                        <td align="center">
							<select name="operations_location">
								{html_options options=$operations_locations selected=$assignees[assignee].operations_location}
							</select>
						</td>
						<td align="center">
							<input type="checkbox" name="status" {if $assignees[assignee].status}checked{/if}/>
						</td>
						<td align="center">
							<input type="submit" name="modify_assignee" value="modify"/>
						</td>
					</tr>
				</form>
			{/section}
		{/if}
		<form action="" method="get" name="add_assignee">
			<tr class="tablehead">
				<td nowrap="nowrap">
					Add Resource : 
				</td>
				<td nowrap="nowrap">
					<input type="text" name="name"/>
				</td>
				
				<td nowrap="nowrap">
					<input type="text" name="threshold" />
				</td>
				<td nowrap="nowrap">
					<select name="operations_location">
						{html_options options=$operations_locations}
					</select>
				<td>
				
				<td nowrap="nowrap" colspan="2" align="center">
					<input type="submit" name="add_assignee" value="add"/>
				</td>
			</tr>
		</form>
	</table>
{/capture}
{include file="dialog.tpl" title="Resources List" content=$smarty.capture.dialog extra='width="40%"'}
