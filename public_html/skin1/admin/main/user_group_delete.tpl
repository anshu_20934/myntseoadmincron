<div id='deleteUserGroup'>
	<h1 class='ntfSuccess'>Group Deleted Successfully<br /><br /></h1>	
	{if $resultCount gt 0}		
	<h1 class='pageHeader'>Delete User Group</h1>
	<table id='delUGTable' cellspacing='0' cellpadding='0' border='0'>
		<thead>
			<th class='tdNo'>Sl. No.</td>
			<th class='tdTitle'>Title</td>
			<th class='tdMessage'>Description</td>
			<th class='tdId'>Group Id</td>
			<th class='tdDel'>&nbsp;</td>
		</thead>
		{assign var="i" value=1}
		{foreach item=userGroup from=$results}
			<tr>
				<td class='tdNo'>{$i++}</td>
				<td class='tdTitle'>{$userGroup.title}</td>
				<td class='tdMessage'>{$userGroup.description}&nbsp;</td>
				<td class='tdId'>{$userGroup.id}</td>
				<td class='tdDel genDel'><a data-group-id='{$userGroup.id}' href='javascript:void(0);' title='Delete User Group'>Delete</a></td>
			</tr>
		{/foreach}
	</table>
	{else}
	<h1>No User Groups Exist</h1>
	{/if}
	<form action='/admin/user_group_delete_rest.php' method='post' enctype='multipart/form-data'><input name='groupId' type='hidden' /></form>
</div>
<script language="javascript" src="/skin1/myntra_js/ns.js"> </script>
<script language="javascript" src="/skin1/myntra_js/mk-base.js"> </script>
<script src='/skin1/admin/notifications/usergroup.js'></script>
<script>Myntra.ntfFlag = "{$deleteGroupFlag}";</script>
