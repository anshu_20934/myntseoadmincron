<div id='deleteNotifications'>
	<h1 class='ntfSuccess'>Notification Deleted Successfully<br /><br /></h1>	
	{if $resultCount gt 0}		
	<h1 class='pageHeader'>Delete Notifications</h1>
	<table id='delNtfTable' cellspacing='0' cellpadding='0' border='0'>
		<thead>
			<th class='tdNo'>Sl. No.</th>
			<th class='tdTitle'>Title</th>
			<th class='tdMessage'>Message</th>
			<th class='tdId'>Notification Id</th>
			<th class='tdImage'>Image</th>
			<th class='tdStartDate'>Start Date<br/><span>(yyyy-mm-dd)</span></th>
			<th class='tdEndDate'>End Date<br/><span>(yyyy-mm-dd)</span></th>
			<th class='tdDel'>&nbsp;</th>
		</thead>
		{assign var="i" value=1}
		{foreach item=notification from=$results}
			<tr>
				<td class='tdNo'>{$i++}</td>
				<td class='tdTitle'>{$notification.title}</td>
				<td class='tdMessage'>{$notification.message|@urldecode}</td>
				<td class='tdId'>{$notification.id}</td>
				<td class='tdImage'><img src='{$notification.image}' /></td>
				<td class='tdStartDate'>{$notification.startDate|@substr:0:10}</td>
				<td class='tdEndDate'>{$notification.endDate|@substr:0:10}</td>
				<td class='tdDel genDel'><a data-notification-id='{$notification.id}' href='javascript:void(0);' title='Delete Notification'>Delete</a></td>
			</tr>
		{/foreach}
	</table>
	{else}
	<h1>No Notifications Exist</h1>
	{/if}
	<form action='/admin/notifications_delete_rest.php' method='post' enctype='multipart/form-data'><input name='ntfId' type='hidden' /></form>
</div>
<script language="javascript" src="/skin1/myntra_js/ns.js"> </script>
<script language="javascript" src="/skin1/myntra_js/mk-base.js"> </script>
<script src='/skin1/admin/notifications/notifications-delete.js'></script>
<script>Myntra.ntfFlag = "{$ntfDeleteFlag}";</script>
