<script type="text/javascript">
	var loginUser = "{$login}";
	var couriers =  {$couriers};
	var statusArr = {$statuses};
    var httpLocation = "{$http_location}";
    var get_returnid = "{$returnid}";
    var get_status = "{$status}";
    var get_orderid = "{$orderid}";
    var get_login = "{$searchlogin}";
    var progresskey = '{$progresskey}';
    var warehouses = {$warehouses};
    var timefilters = {$timefilters};
</script>

<link rel="stylesheet" type="text/css" href="{$http_location}/admin/extjs/resources/css/ext-all.css"/>

<script type="text/javascript" src="{$http_location}/admin/extjs/adapter/ext/ext-base.js"></script>

<script type="text/javascript" src="{$http_location}/admin/extjs/ext-all.js"></script>
<script type="text/javascript" src="{$http_location}/admin/extjs/plugins/superselectbox/SuperBoxSelect.js"></script>
<link rel="stylesheet" type="text/css" href="{$http_location}/admin/extjs/plugins/superselectbox/superboxselect.css"/>
<script type="text/javascript" src="return_requests.js"></script>
<br>
<br>
<div id="contentPanel"></div>