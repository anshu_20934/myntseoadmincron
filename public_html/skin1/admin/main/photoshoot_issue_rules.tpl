{literal}
<style type="text/css">
.style_table{border:1px solid #ccc;padding:5px;}

.style_table td div.error_div{clear:both;margin:5px 0px}
.style_table .textbox{width:150px;}
.style_table select{width:150px;}
.style_table .shade1{background-color:#ccc;height:80px}
.style_table .shade2{background-color:#f0f0ee;;height:80px}
.style_table .label{font-weight:bold;padding-top:20px;vertical-align:top;}
</style>
<script type="text/javascript">	
//main create new rule
function createNewRule()
{
	article_id = $("#article_type").val();
	brand_id = $("#brand").val();

	if((article_id == '') || (brand_id == ''))
	{
		alert("Please make sure that brand and article type is selected");
		return;
	}
	$("#create_new_rule_list").slideDown();
	$("#update_delete_list").slideUp();
	$("#add_brand").val($("#brand").val());
	$("#add_article_type").val($("#article_type").val());
	$("#add_gender").val($("#gender").val());
	$("#add_age_group").val($("#age_group").val());
	$("#add_usage").val($("#usage").val());
	getSizeOptions(article_id, brand_id);
}

//from add new rule brand and article type dropdown
function addCreateNewRule()
{
	article_id = $("#add_article_type").val();

	brand_id = $("#brand").val();
	getSizeOptions(article_id, brand_id);
}
//this is for create new rule
function getSizeOptions(article_id, brand_id)
{
	//this function gets the size value corresponding to a scale id
	if((article_id == '') || (brand_id == ''))
	{
		$(".add_size_option").html('<option value="">-- Not Available --</option>');
		return;
	}
	$.get(http_loc+'/admin/catalog/photoshoot_issue_rules_ajax_handler.php',
		{mode:"getSizeOptions",  article_id:article_id, brand_id:brand_id},
		function(data)
		{
			var obj = jQuery.parseJSON(data);
			var size_option_value = '';
			//populate size values
		    if(!obj)
			{
		    	size_option_value  = '<option value="">-- Not Available --</option>';
		    }
		    else
			{
		    	size_option_value  = '<option value="">-- Select --</option>';
		        for(var i = 0; i < obj.length; i++)
			    {
		        	size_option_value += '<option value="' + obj[i].value + '">' + obj[i].value + '</option>';
		        }
		    }
			$(".add_size_option").html(size_option_value);
		}
	);
}

//this is for updating rules
function getSizeOptionsUpdate(id)
{
	var article_id = $("#update_article_type\\["+id+"\\]").val();
	var brand_id = $("#update_brand\\["+id+"\\]").val();
	debugger;
	if(article_id=='' || brand_id=='')
	{
		return;
	}
	$.get(http_loc+'/admin/catalog/photoshoot_issue_rules_ajax_handler.php',
		{mode:"getSizeOptions", article_id:article_id, brand_id:brand_id},
		function(data)
		{
			var obj = jQuery.parseJSON(data);
			var size_option_value = '';
			//populate size values
		     if(!obj)
			{
		    	size_option_value  = '<option value="">-- Not Available --</option>';
		    }
		    else
			{
		    	size_option_value  = '<option value="">-- Select --</option>';
		        for(var i = 0; i < obj.length; i++)
			    {
		        	size_option_value += '<option value="' + obj[i].value + '">' + obj[i].value + '</option>';
		        }
		    }
		    $(".update_size_value\\["+id+"\\]").html(size_option_value);
		}
	);
}

function addPhotoshootIssueRule()
{
	if(($("#add_brand").val() == '') || ($("#add_article_type").val() == '') || ($("#add_size_option_1").val() == ''))
	{
		alert("Please make sure that brand, article type and size option I are selected");
		return;
	}
	if(($("#add_size_option_2").val() != '') && ($("#add_size_option_3").val() != ''))
	{
		if(($("#add_size_option_1").val() == $("#add_size_option_2").val()) || ($("#add_size_option_2").val() == $("#add_size_option_3").val()) || ($("#add_size_option_3").val() == $("#add_size_option_1").val()))
		{
			alert("Please make sure that differenct size options are selected.");
			return;
		}
	}
	document.addrules.mode.value = 'add';
    document.addrules.submit();
    
}

function searchRules()
{
	if(($("#brand").val() == '') || ($("#article_type").val() == ''))
	{
		alert("Please make sure that brand and article type is selected");
		return;
	}
	$("#create_new_rule_list").slideUp();
	document.searchrules.mode.value = 'search';
    document.searchrules.submit();
}

function deletePhotoshootIssueRule()
{
	var fields = $("input[class='update_checkbox']").serializeArray(); 
	if (fields.length == 0) 
	{ 
		alert('Please select the rule which you want to update');
		return;
	}
    var r=confirm("Are you sure you want to delete the selected issue rules");
    if(r == true)
    {
        document.updaterules.mode.value = 'delete';
        document.updaterules.submit();
    }
}

function updatePhotoshootIssueRule()
{
	var fields = $("input[class='update_checkbox']").serializeArray(); 
	if (fields.length == 0) 
	{ 
		alert('Please select the rule which you want to update');
		return;
	}
	var r=confirm("Make sure that mandatory field are selected then only it will be updated else it will be neglected");		
	if(r == true)
    {
	    document.updaterules.mode.value = 'update';
		document.updaterules.submit();
    }
}

</script>
{/literal}
{capture name=dialog}
<div style="font-size: 15px;font-weight: bold;padding: 10px 5px 5px;" >Search/Create New Rule</div>
<form action="photoshoot_issue_rules.php" method="post" name="searchrules" class="style_table">
	<input type="hidden" name="mode" />
	<table cellpadding="3" cellspacing="1" width="100%">
		<tbody>
			<tr class="shade2">
				<td>Brand*:
        			<select name="brand"  id="brand">
        				<option value="">-- Select --</option>
        				{foreach from=$brands item=brand}
							<option value="{$brand.id}">{$brand.name}</option>
						{/foreach}
        			</select>
        		</td>
        		<td>Article Type*:
        			<select name="article_type"  id="article_type">
        				<option value="">-- Select --</option>
        				{foreach from=$articleTypes item=articleType}
							<option value="{$articleType.id}">{$articleType.name}</option>
						{/foreach}
        			</select>
        		</td>
        		<td>Gender:
        			<select name="gender"  id="gender">
        				<option value="">-- Select --</option>
        				{foreach from=$genders item=gender}
							<option value="{$gender.id}">{$gender.name}</option>
						{/foreach}
        			</select>
        		</td>
        		<td>Age Group:
        			<select name="age_group"  id="age_group">
        				<option value="">-- Select --</option>
        				{foreach from=$ageGroups item=ageGroup}
							<option value="{$ageGroup.id}">{$ageGroup.name}</option>
						{/foreach}
        			</select>
        		</td>
        		<td>Usage:
        			<select name="usage"  id="usage">
        				<option value="">-- Select --</option>
        				{foreach from=$usages item=usage}
							<option value="{$usage.id}">{$usage.name}</option>
						{/foreach}
        			</select>
        		</td>
        	</tr>
		</tbody>
	</table>
	<div>
		<input type="button" value="Search Rules" onclick="searchRules()">
		<input type="button" value="Create New Rule" onclick="createNewRule()">
	 </div>
</form>

<div name="create_new_rule_list" id="create_new_rule_list" style="display:none">
<div style="font-size: 15px;font-weight: bold;padding: 10px 5px 5px;" >Create New Rule</div>
<form action="photoshoot_issue_rules.php" method="post" name="addrules">
	<input type="hidden" name="mode" />
	<table cellpadding="3" cellspacing="1" width="100%" class="style_table">
		<tbody>
			<tr class="shade2">
				<td>Brand:
        			<select name="add_brand"  id="add_brand" onchange="addCreateNewRule()">
        				{foreach from=$brands item=brand}
							<option value="{$brand.id}">{$brand.name}</option>
						{/foreach}
        			</select>
        		</td>
        		<td>Article Type:
        			<select name="add_article_type"  id="add_article_type" onchange="addCreateNewRule()">
        				{foreach from=$articleTypes item=articleType}
							<option value="{$articleType.id}">{$articleType.name}</option>
						{/foreach}
        			</select>
        		</td>
        		<td>Gender:
        			<select name="add_gender"  id="add_gender">
        				<option value="">-- Select --</option>
        				{foreach from=$genders item=gender}
							<option value="{$gender.id}">{$gender.name}</option>
						{/foreach}
        			</select>
        		</td>
        		<td>Age Group:
        			<select name="add_age_group"  id="add_age_group">
        				<option value="">-- Select --</option>
        				{foreach from=$ageGroups item=ageGroup}
							<option value="{$ageGroup.id}">{$ageGroup.name}</option>
						{/foreach}
        			</select>
        		</td>
        		<td>Usage:
        			<select name="add_usage"  id="add_usage">
        				<option value="">-- Select --</option>
        				{foreach from=$usages item=usage}
							<option value="{$usage.id}">{$usage.name}</option>
						{/foreach}
        			</select>
        		</td>
        	</tr>
        	<tr class="shade2">
        		<td>Size Option 1:
        			<select class="add_size_option" name="add_size_option_1"  id="add_size_option_1">
        				<option value="">-- Select --</option>
        			</select>
        		</td>
        		<td>Size Option 2:
        			<select class="add_size_option" name="add_size_option_2"  id="add_size_option_2">
        				<option value="">-- Select --</option>
        			</select>
        		</td>
        		<td>Size Option 3:
        			<select class="add_size_option" name="add_size_option_3"  id="add_size_option_3">
        				<option value="">-- Select --</option>
        			</select>
        		</td>
        	</tr>
		</tbody>
	</table>
	<div>
		<input type="button" value="Add New Rule" onclick="addPhotoshootIssueRule()">
	 </div>
</form>
</div>
{if $postmode eq 'searchmode'}
<div name="update_delete_list" id="update_delete_list">
<div style="font-size: 15px;font-weight: bold;padding: 10px 5px 5px;">Search Results Rule</div>
<form action="photoshoot_issue_rules.php" method="post" name="updaterules" class="style_table">
	<input type="hidden" name="mode" />
	<input type="hidden" name="searchParam" value="{$searchParam}" />
	<table cellpadding="3" cellspacing="1" width="100%">
		<thead>
			<tr class="TableHead">
				<td>Update/Delete</td>
				<td>Brand</td>
				<td>Article Type</td>
				<td>Gender</td>
				<td>Age Group</td>
				<td>Usage</td>
				<td>Size Option 1</td>
				<td>Size Option 2</td>
				<td>Size Option 2</td>
			</tr>
		</thead>
		<tbody id="search_rules_list">
			{foreach from=$searchedRules item=rule}
				<tr class="shade2" {if $rule.error_update eq '1'} style="background-color:red" {/if}>
				<td style="text-align: center; vertical-align: middle;"> <input class="update_checkbox" type="checkbox" name="update_id[]" value="{$rule.id}"></td>
				<td><select id="update_brand[{$rule.id}]" name="update_brand[{$rule.id}]"  onchange="getSizeOptionsUpdate({$rule.id})">
				     	{foreach from=$brands item=brand}
							<option value="{$brand.id}" {if $rule.brand_id eq $brand.id}selected{/if}>{$brand.name}</option>
						{/foreach}
        			</select></td>
        		<td><select id="update_article_type[{$rule.id}]" name="update_article_type[{$rule.id}]"  onchange="getSizeOptionsUpdate({$rule.id})">
        				{foreach from=$articleTypes item=articleType}
							<option value="{$articleType.id}" {if $rule.article_type_id eq $articleType.id}selected{/if}>{$articleType.name} </option>
						{/foreach}
        			</select></td>
        		<td><select name="update_gender[{$rule.id}]">
        			<option value="">-- Select --</option>
        				{foreach from=$genders item=gender}
							<option value="{$gender.id}" {if $rule.gender_id eq $gender.id}selected{/if}>{$gender.name}</option>
						{/foreach}
        			</select></td>
        		<td><select name="update_age_group[{$rule.id}]">
        			<option value="">-- Select --</option>
        				{foreach from=$ageGroups item=ageGroup}
							<option value="{$ageGroup.id}" {if $rule.age_group_id eq $ageGroup.id}selected{/if}>{$ageGroup.name}</option>
						{/foreach}
        			</select></td>
        		<td><select name="update_usage[{$rule.id}]">
        				<option value="">-- Select --</option>
        				{foreach from=$usages item=usage}
							<option value="{$usage.id}" {if $rule.usage_id eq $usage.id}selected{/if}>{$usage.name}</option>
						{/foreach}
        			</select></td>
        		<td><select class="update_size_value[{$rule.id}]" name="update_size_option_1[{$rule.id}]">
        				<option value="">-- Select --</option>
        				{foreach from=$sizeOptions item=size}
        					<option value="{$size.value}" {if $rule.size_option_1 eq $size.value}selected{/if}>{$size.value}</option>
        				{/foreach}
        			</select></td>
        		<td><select class="update_size_value[{$rule.id}]" name="update_size_option_2[{$rule.id}]">
        				<option value="">-- Select --</option>
        				{foreach from=$sizeOptions item=size}
        					<option value="{$size.value}" {if $rule.size_option_2 eq $size.value}selected{/if}>{$size.value}</option>
        				{/foreach}
        			</select></td>
        		<td><select class="update_size_value[{$rule.id}]" name="update_size_option_3[{$rule.id}]">
        				<option value="">-- Select --</option>
        				{foreach from=$sizeOptions item=size}
        					<option value="{$size.value}" {if $rule.size_option_3 eq $size.value}selected{/if}>{$size.value}</option>
        				{/foreach}
        			</select></td>
        		</tr>
        	{/foreach}
		</tbody>
	</table>
	<div>
		<input id="update_rules_button" type="button" value="Update Rules" onclick="updatePhotoshootIssueRule()">
		<input id="delete_rules_button" type="button" value="Delete Rules" onclick="deletePhotoshootIssueRule()">
	 </div>
</form>
</div>

{/if}
{/capture}
{include file="dialog.tpl" title="Photoshoot issue rules" content=$smarty.capture.dialog extra='width="100%"'}