{* $Id: featured_products.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
{include file="main/include_js.tpl" src="main/popup_product.js"}
{capture name=dialog}
<div style="text-align:center;"> 
	<input type="button" name="codAdminSearchBtn" id="codAdminSearchBtn" value="COD ORDER UPDATE">
	<input type="button" id="codAdminUploadBtn" value="COD ORDER UPDATE BY UPLOADING CSV FILE">
	<input type="button" id="codAdminLostBtn" value="COD  LOST ITEM  UPDATE">
	{if $message != ''}
	<br/> <span style="color:red;font-size: 16px;margin:10px;">{$message}</span>
	{/if}
</div>
{include file="admin/main/cod-admin/cod_admin_search.tpl"}

{include file="admin/main/cod-admin/cod_upload_csv.tpl"}
{include file="admin/main/cod-admin/cod_lost_item.tpl"}
{/capture}
{include file="dialog.tpl" title=" Cach-on-Delivery Admin" content=$smarty.capture.dialog extra='width="100%"'}
{literal}
<script type="text/javascript">
$(document).ready(function()
{
	
	$("#codAdminSearchBtn").click(function() {
		$("#codSearchDiv").css("display","block");
		$("#codUploadDiv").css("display","none");
		$("#codLostDiv").css("display","none");		
		
	});
	$("#codAdminUploadBtn").click(function() {
		$("#codSearchDiv").css("display","none");
		$("#codUploadDiv").css("display","block");
		$("#codLostDiv").css("display","none");		
		
	});
	$("#codAdminLostBtn").click(function() {
		$("#codSearchDiv").css("display","none");
		$("#codUploadDiv").css("display","none");
		$("#codLostDiv").css("display","block");		
		
	});
	$("#codSelectAll").click(function()	{
		var checked_status = this.checked;
		$("input[name=codSelect[]]").each(function()
		{
			this.checked = checked_status;
		});
	});	
	$("#lostItemSelectAll").click(function() {
				var checked_status1 = this.checked;
				$("input[name=lostItemSelect[]]").each(function()
				{
					this.checked = checked_status1;
				});
	});		
					
});
function submitCodUpdate() {

	var answer = confirm ("Are you sure to update cod orders data")
	if (answer) {
		$('#codUpdateForm').submit();
	} else {
		return false;
	} 
}
function submitCodCsvUpdate() {

	var answer = confirm ("Are you sure to update cod orders data")
	if (answer) {
		$('#codUploadCsv').submit();
	} else {
		return false;
	} 
}
function submitCodItemUpdate() {

	var answer = confirm ("Are you sure to update cod orders data")
	if (answer) {
		$('#lostItemUpdateForm').submit();
	} else {
		return false;
	} 
}
</script>
{/literal}
