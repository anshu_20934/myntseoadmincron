<script type="text/javascript">
	var loginUser = "{$login}";
	var couriers =  {$couriers};
	var warehouses = {$warehouses};
	var deliveryCenters = {$deliveryCenters};
</script>

<link rel="stylesheet" type="text/css" href="{$http_location}/admin/extjs/resources/css/ext-all.css"/>

<script type="text/javascript" src="{$http_location}/admin/extjs/adapter/ext/ext-base.js"></script>

<script type="text/javascript" src="{$http_location}/admin/extjs/ext-all.js"></script>
<script type="text/javascript" src="{$http_location}/admin/extjs/plugins/superselectbox/SuperBoxSelect.js"></script>
<link rel="stylesheet" type="text/css" href="{$http_location}/admin/extjs/plugins/superselectbox/superboxselect.css"/>
<script type="text/javascript" src="receive_returns.js"></script>
<br>
<br>
<div id="contentPanel"></div>