var today = new Date();
var today_date_str = (today.getMonth()+1)+"/"+today.getDate()+"/"+today.getFullYear();

var orderStatusArr = [["SH", "Shipped"], ["DL", "Delivered"], ["C", "Complete"], ["F", "Rejected"]];

Ext.onReady(function() {
	Ext.QuickTips.init();
	var myMask = new Ext.LoadMask(Ext.getBody(), {msg: "Loading..."});
	Ext.Ajax.on('beforerequest', myMask.show, myMask);
	Ext.Ajax.on('requestcomplete', myMask.hide, myMask);
	Ext.Ajax.on('requestexception', myMask.hide, myMask);
	
	var searchPanel = new Ext.form.FormPanel({
		renderTo : 'contentPanel',
		title : 'Search Shipped Orders',
		id : 'formPanel',
		autoHeight : true,
		collapsible : true,
		border : true,
		bodyStyle : 'padding: 5px',
		width : 1000,
		items : [{
			layout : 'table',
			border : false,
			layoutConfig : {columns : 2},
			defaults : {width : 450, border : false, 
				layout : 'form', bodyStyle : 'padding-left : 15px'},
			items : [
			         {
			        	 items : [{
			        		 xtype : 'datefield',
			        		 name : 'from_date',
			        		 width : 150,
			        		 fieldLabel : "Shipping Date From",
			        		 value: today_date_str,
			        		 maxValue : new Date()
			        	 }]
			         },{
			        	 items : [{
			        		 xtype : 'datefield',
			        		 name : 'to_date',
			        		 width : 150,
			        		 fieldLabel : "Shipping Date To",
			        		 value: today_date_str,
			        		 maxValue : new Date()
			        	 }]
			         }, {
							items : [{
					        	xtype : 'combo',
					        	store : new Ext.data.JsonStore({
					        		fields : [ 'id', 'display_name'],
					        		data : warehouseData,
					        		sortInfo : {
					        			field : 'display_name',
					        			direction : 'ASC'
					        		}
					        	}),
					        	emptyText : 'Select Ops Location',
					        	displayField : 'display_name',
					        	valueField : 'id',
					        	fieldLabel : 'Ops Location',
					        	name : 'warehouse_id',
					        	mode: 'local',
					        	triggerAction: 'all'
							}]
						}
			]
		}],
		buttons : [ {
			text : 'Search',
			handler : function() {
				reloadGrid();
			}
		}],
	});
	
	function getSearchPanelParams() {
		var form = searchPanel.getForm();
		
		if (form.isValid()) {
			if(form.findField("from_date").getRawValue() == "" || 
					form.findField("to_date").getRawValue() == "" ) {
				Ext.MessageBox.alert('Error', 'Date range can not be empty');
				return false;
			}
			
			var params = {};
			params["from_date"] = form.findField("from_date").getRawValue();
			params["to_date"] = form.findField("to_date").getRawValue();
			params["warehouse_id"] = form.findField("warehouse_id").getValue();
			return params;
		} else {
			return false;
		}
	}
	
	function reloadGrid() {
		
		var params = getSearchPanelParams();
		
		if (params == false) {
			Ext.MessageBox.alert('Error', 'Errors in the search form');
			return false;
		}
		params["action"] = "search";
		
		searchResultsStore.baseParams = params;
		searchResultsStore.load();
	}
	
	var orderNameLinkRender = function(value, p, record) {
		var retLink = '<a href="' + httpLocation + '/admin/order.php?orderid=' + record.data["orderid"] + '" target="_blank">' + record.data["order_name"] + '</a>';
		if (record.data['customizable'] == '0') {
			retLink = retLink + "<span style='color:red'> (NP) <span>";
		}
		return retLink;
	}
	
	var orderStatusRenderer = function(value, p, record) {
		var status = record.data['status'];
		for (var i = 0; i < orderStatusArr.length; i++) {
			if (orderStatusArr[i][0] == status) {
				return orderStatusArr[i][1];
			}
		}
		return "NOT SPECIFIED";
	}
	
	var warehouseNameRenderer = function(value, p, record) {
		for (var i in warehouseData) {
			if (warehouseData[i]['id'] == record.data['warehouseid']) {
				return warehouseData[i]['display_name'];
			}
		}
	}
	
	var colModel = new Ext.grid.ColumnModel({
		defaults: {
	        sortable: true
	    },
	    columns: [
	        new Ext.grid.RowNumberer(),
	        {id : 'order_name', header : "Order Name", sortable: true, dataIndex : 'order_name', renderer : orderNameLinkRender},
	        {id : 'payment_method', header : "Payment Method", sortable: false, dataIndex : 'payment_method'},
	        {id : 'orderid', header : "Order ID", sortable: true, dataIndex : 'orderid', sortable: false},
	        {id : 'login', header : "Customer Login", sortable: true, dataIndex : 'login'},
	        {id : 'status', header : "Status", sortable: true, dataIndex: 'status', renderer : orderStatusRenderer},
	        {id : 'warehouseid', header : "Ops Location", sortable: false, dataIndex: 'warehouseid', renderer : warehouseNameRenderer},
	        {id : 'queueddate', header : "Queued Date", sortable: true, dataIndex : 'queueddate'},
	        {id : 'shippeddate', header : "Shipped Date", sortable: true, dataIndex : 'shippeddate'},
	        {id : 'delivereddate', header : "Delivered Date", sortable: true, dataIndex : 'delivereddate'},
	        {id : 'completeddate', header : "Completed Date", sortable: true, dataIndex : 'completeddate'}
  	  	]
	});
		
	var searchResultsStore = new Ext.data.Store({
		url: 'shipped_orders.php',
		pruneModifiedRecords : true,
		reader: new Ext.data.JsonReader({
	  		root: 'results',
	  	  	totalProperty: 'count',
	  	  	fields : ['order_name','payment_method','orderid','login','status', 'warehouseid', 'queueddate','shippeddate','delivereddate','completeddate']
		}),
	});

	var pagingBar = new Ext.PagingToolbar({
		pageSize : 30,
	    store: searchResultsStore,
	    displayInfo: true
	});
	
	var searchResults = new Ext.grid.EditorGridPanel({
	    store  : searchResultsStore,
	    cm : colModel,
	    id : 'searchResults-panel',
	    title : 'Search Results',
	    frame : true,
		loadMask : false,
		autoHeight : true,
		bbar : pagingBar,
		stripeRows : true,
		autoScroll : true,
		viewConfig : {
			enableRowBody : true
		}
	});
	
	var mainPanel = new Ext.Panel({
		renderTo : 'contentPanel',
		border : false,
		items : [
		     searchPanel, searchResults
		],
		listeners : {
			'afterrender' : function(comp) {
				reloadGrid();
			}
		}
	});
});