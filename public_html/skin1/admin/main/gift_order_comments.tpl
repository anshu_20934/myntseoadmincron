{* $Id: order_comments.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
{include file="main/include_js.tpl" src="main/popup_product.js" src="main/overlib.js"}
{popup_init src="../skin1/main/overlib.js"}
<table width="100%"  border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td valign="top">
			<table width="98%"  border="0" align="center" cellpadding="0" cellspacing="0">
				<tr>
					<td>
						<table width="100%" height="21" border="0" cellpadding="0" cellspacing="0" class="textBlack">
							{if $changerequest_status=='Y'}
								<tr>
									<td><p  style="font-size:1.5em;color:#FF0000;background-color:#FFFFCC;"><strong>This order has change requests logged against it. Please see the Comments Log below.</strong></p></td>
								</tr>
							{/if}
							<tr>
								<td><strong>Gift Comments Log for orderid {$orderid}</strong></td>
							</tr>
						</table>
						<br/>
						<table width="100%" height="25" border="1" cellpadding="0" cellspacing="0">
							<tr bgcolor="#ccffff">								
								<th style="font-size:14px;">Gift Card Id</th>
								<th style="font-size:14px;">Activated by</th>
								<th style="font-size:14px;">Activated on</th>
								<th style="font-size:14px;">Created on</th>
								<th style="font-size:14px;">Updated on</th>
								<th style="font-size:14px;">Updated by</th>								
							</tr>
							{foreach from=$comments item=comment}
							<tr bgcolor="{cycle values="#eeddff,#d0ddff"}">					
								<td style="font-size:12px;">{$comment.gift_card_id}</td>
								<td style="font-size:12px;">{$comment.activated_by}</td>
								<td style="font-size:12px;">{$comment.activated_on}</td>
								<td style="font-size:12px;">{$comment.created_on|date_format:$config.Appearance.datetime_format}</td>
								<td style="font-size:12px;">{$comment.updated_on|date_format:$config.Appearance.datetime_format}</td>
								<td style="font-size:12px;">{$comment.updated_by}</td>	
							</tr>				
							{/foreach}    
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<table width="100%" height="21" border="0" cellpadding="0" cellspacing="0" class="textBlack">
							<tr>
								<td><strong>Order Comments Log for orderid {$orderid}</strong></td>
							</tr>
						</table>
						<br/>
						<table width="100%" height="25" border="1" cellpadding="0" cellspacing="0">
							<tr bgcolor="#ccffff">								
								<th style="font-size:14px;">Added by</th>
								<th style="font-size:14px;">Title</th>
								<th style="font-size:14px;">Type</th>
								<th style="font-size:14px;">Description</th>
								<th style="font-size:14px;">Added on</th>								
							</tr>
							{foreach from=$ordercomments item=comment}
							<tr bgcolor="{cycle values="#eeddff,#d0ddff"}">					
								<td style="font-size:12px;">{$comment.commentaddedby}</td>
								<td style="font-size:12px;">{$comment.commenttitle}</td>
								<td style="font-size:12px;">{$comment.commenttype}</td>
								<td style="font-size:12px;">{$comment.description}</td>
								<td style="font-size:12px;">{$comment.commentdate|date_format:$config.Appearance.datetime_format}</td>
							</tr>				
							{/foreach}    
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>