{* $Id: featured_products.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
{include file="main/include_js.tpl" src="main/popup_product.js"}
{literal}
  <style type="text/css">
    .modify input , select{width:200px}
    .modify textarea{width:700px}
    
  </style>
  <script language="Javascript">
      function confirmDelete()
      {
          var flag = confirm('Are you sure you want to delete?')
          if(!flag)
          {
             return false;
          }
          else
          {
          	 document.groupfrm.mode.value = 'delete';
          	 document.groupfrm.submit();
          	 return true;
          }
      }
      var i=1;
      function addfilter(type,filter_group_id){
          var add_html='<tr id="new_'+type+'_tr_'+i+'" style="background-color:pink;"><td colspan="5"><form action="" id="new_'+type+'_form_'+i+'" method="post"><input type="hidden" name="mode" value="addNewFilter" ><input type="hidden" name="filter_group_id" value="'+filter_group_id+'" ><table width="100%"><tr><td width="20%" align="center"><input type="text" name="filter_name"></td><td width="20%" align="center"><select name="filter_map_id" id="'+type+'_mapsto_'+i+'"></select></td><td width="10%" align="center">Yes</td><td width="10%" align="center"><input type="text" name="display_order"></td><td width="20%" align="center"><a style="cursor:pointer;padding-right:10px" onclick="savefilter(\''+type+'\',\''+i+'\')">Save</a><a style="cursor:pointer" onclick="cancelfilter(\''+type+'\',\''+i+'\')">Cancel</a></td></tr></table></form></td></tr>';
          $('#'+type+'_filter').append(add_html);

          $.ajax({
               type: "POST",
               url: http_loc+"/admin/addfilters.php",
               data: "mode=getAllFilters",
               success: function(msg){
                 var all_filter_html=$.trim(msg);
                 $("#"+type+"_mapsto_"+i).html(all_filter_html);
                  i++;
               }
             });


      }
      function savefilter(type_i,value)
      {
            var params=$("#new_"+type_i+"_form_"+value).serializeArray();
            $.ajax({

               type: "POST",
               url: http_loc+"/admin/addfilters.php",
               data: params,
               success: function(msg){
                 var filter_saved_html=$.trim(msg);
                 
                 $('#new_'+type_i+'_tr_'+value).remove();
                 $('#'+type_i+'_filter').append(filter_saved_html);

               }
             });
      }
      function cancelfilter(type_i,value)
      {
        $("#new_"+type_i+'_tr_'+value).remove();
      }
      function changeOrder(id,value)
      {
        $.get(http_loc+"/admin/addfilters.php", { mode: "changeorder", id: id,order:value },
           function(data){
                
           });
      }
  </script>
{/literal}
<a name="featured" />

{$lng.txt_featured_products}

<br /><br />

{capture name=dialog}
{if $method eq 'modifyfilter'}
    <form action="{$form_action}" method="post" name="filter_form" enctype="multipart/form-data">
        <input type="hidden" name="mode" value="savefilter">
        <input type="hidden" name="filter_id" value="{$filter_id}">
        <table border="0" cellspacing="5" cellpadding="5"  width="100%" class="modify">
            <tr>
                <td>Filter Name</td>
                <td><input type="text" name="filter_name" value="{ if $filter_details.filter_name }{$filter_details.filter_name} {/if}"> </td>
            </tr>
            <tr>
                <td>Display Order</td>
                <td><input type="text" name="display_order" value="{ if $filter_details.display_order }{$filter_details.display_order} {/if}"> </td>
            </tr>

            <tr>
                <td>Filter Url</td>
                <td><input type="text" name="filter_url" value="{ if $filter_details.filter_url }{$filter_details.filter_url} {/if}">
                For example if the url is {$http_location}/sportswear/sporting-goods , then enter only <b>sporting-goods</b> here 
                </td>
            </tr>
            <tr>
                <td>Filter Type</td>
                <td>
                    <select name="filter_group_id" class="">
                       {section name=num loop=$groups}
                            <option value="{$groups[num].id}" {if $filter_details.filter_group_id eq $groups[num].id}selected {/if} >{$groups[num].group_name}</option>
                       {/section}
                    </select>
                </td>
            </tr>
            <tr>
                <td>Maps To</td>
                <td>
                    <select name="filter_map_id" class="">
                       <option value="">None</option>
                       {section name=num loop=$filters}
                            <option value="{$filters[num].id}" {if $mapping_details[0].filter_map_id eq $filters[num].id}selected {/if} >{$filters[num].filter_name}</option>
                       {/section}
                    </select>
                </td>
            </tr>
            <tr>
                <td>Narrow By Section</td>
                <td>
                    <select name="narrow_by[]" class="" multiple="multiple">

                       <option value="">None</option>
                       {section name=n loop=$groups}
                            {assign var=is_selected value="notselected"}
                            {if $selected_narrow_by}
                                {section name=inner loop=$selected_narrow_by}
                                    {if $selected_narrow_by[inner] eq $groups[n].id}
                                        {assign var=is_selected value="selected"}
                                    {/if}
                                {/section}
                            {/if}
                            <option value="{$groups[n].id}" {if $is_selected eq 'selected'}selected{/if} >{$groups[n].group_name}</option>
                       {/section}
                    </select>
                </td>
            </tr>
            <tr>
                <td>Icon/Logo</td>
                <td>
                    <img src="{ if $filter_details.logo }{$filter_details.logo} {/if}" alt="" height="50px" width="50px">
                    <br>
                    <input type="file" name="logo" value="">
                    <br>
                    {if $filter_details.logo} Remove Logo :<input type="checkbox" value="1" name="remove_logo"> {/if}
                </td>
            </tr>
            <tr>
                <td>Icon / Logo Url</td>
                <td><input type="text" name="logo_url" value="{ if $filter_details.logo_url }{$filter_details.logo_url} {/if}"> </td>
            </tr>
            <tr>
                <td>Icon / Logo Alt tag</td>
                <td><input type="text" name="logo_alt_tag" value="{ if $filter_details.logo_alt_tag }{$filter_details.logo_alt_tag} {/if}"> </td>
            </tr>
            <tr>
                <td>Top Banner</td>
                <td>
                    <img src="{ if $filter_details.top_banner }{$filter_details.top_banner} {/if}" alt="" height="50px" width="700px">
                    <br>
                    <input type="file" name="top_banner" value="">
                    <br>
                    {if $filter_details.top_banner} Remove Banner : <input type="checkbox" value="1" name="remove_top_banner"> {/if}
                </td>
            </tr>
            <tr>
                <td>Top Banner Url</td>
                <td><input type="text" name="top_banner_url" value="{ if $filter_details.top_banner_url }{$filter_details.top_banner_url} {/if}"> </td>
            </tr>
            <tr>
                <td>Top Banner Alt Tag</td>
                <td><input type="text" name="top_banner_alt_tag" value="{ if $filter_details.top_banner_alt_tag }{$filter_details.top_banner_alt_tag} {/if}"> </td>
            </tr>
            <tr>
                <td>About Filter</td>
                <td><textarea name="about_filter" style="height:100px;">{ if $filter_details.about_filter }{$filter_details.about_filter} {/if}</textarea> </td>
            </tr>
			
			<tr>
                <td>Page Title</td>
                <td><textarea name="page_title" >{ if $filter_details.page_title }{$filter_details.page_title} {/if}</textarea> </td>
            </tr>
			<tr>
                <td>H1 Heading</td>
                <td><textarea name="h1_title" >{ if $filter_details.h1_title }{$filter_details.h1_title} {/if}</textarea> </td>
            </tr>
			<tr>
                <td>Meta Keywords</td>
                <td><textarea name="meta_keywords" >{ if $filter_details.meta_keywords }{$filter_details.meta_keywords} {/if}</textarea> </td>
            </tr>
			<tr>
                <td>Meta Description</td>
                <td><textarea name="meta_description" >{ if $filter_details.meta_description }{$filter_details.meta_description} {/if}</textarea> </td>
            </tr>
			
            <tr>
                <td>Active</td>
                <td>
                     <select name="is_active" class="">
                       <option value="1" {if $filter_details.is_active eq 1 }selected {/if} >Yes</option>
                       <option value="0" {if $filter_details.is_active eq 0 }selected {/if} >No</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td><input type="submit" value="Update" style="width:100px"> </td>
                <td><a href="{$http_location}/admin/addfilters.php">Cancel</a> </td>
             </tr>

        </table>
    </form>
{else}
    <div style="width:100%;padding:5px" align="right"><a href="{$http_location}/admin/filtergroups.php">Create New Filter Group</a></div>
    <table border="1" cellspacing="0" cellpadding="0"  width="100%">
        <tr class="TableHead">
            <td width="20%">Filter Type</td>
            <td width="80%" align="center" colspan="4">
                <table cellpadding="5" cellspacing="1" width="100%">
                    <tr class="TableHead" >
                        <td width="20%" align="center">Filter</td>
                        <td width="20%">Maps To</td>
                        <td width="10%" align="center">Active</td>
                        <td width="10%" align="center">Display Order</td>
                        <td width="20%" align="center">Action</td>
                    </tr>
                 </table>
            </td>
        </tr>
        {if $groups}
        {foreach from=$filters key=key item=filter}
        <tr {cycle values=", class='TableSubHead'"}>
            <td align="center">{$key}
                <br><br>
                <a style="cursor:pointer;text-decoration:underline;" onclick="addfilter('{$key|replace:" ":"_"|replace:"/":"_"}','{$groups[$key].id}')">Add New {$key}</a>
            </td>
            <td align="center" colspan="4">
                <table cellpadding="3" cellspacing="1" width="100%" id="{$key|replace:" ":"_"|replace:"/":"_"}_filter">
                    {foreach from=$filter key=key item=f}
                            <tr style="background-color:{cycle values=", #ccc"}">
                                <td width="20%" align="center">{$f.filter_name}</td>
                                <td width="20%" align="center">{$f.mapsto}</td>
                                <td width="10%" align="center">{if $f.is_active}Yes{else}No{/if}</td>
                                <td width="10%" align="center"><input type="text" value="{$f.display_order}" style="width:30px" onblur="changeOrder('{$f.id}',this.value)" ></td>
                                <td width="20%" align="center"><a href="{$http_location}/admin/addfilters.php?mode=modifyfilter&filterid={$f.id}">Modify</a></td>
                            </tr>
                    {/foreach}
                 </table>
            </td>
        </tr>

    {/foreach}
    {else}

        <tr>
         <td colspan="4" align="center">No groups available.</td>
        </tr>
    {/if}

    </table>
{/if}
{/capture}
{include file="dialog.tpl" title="Add/Modify Filter" content=$smarty.capture.dialog extra='width="100%"'}
<br /><br />
