{* $Id: featured_products.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
{include file="main/include_js.tpl" src="main/popup_product.js"}
<link rel="stylesheet" href="{$SkinDir}/skin1_admin_operations.css" />

{if $mode eq "display"}
{capture name=dialog}

{include file="customer/main/navigation.tpl"}


<form action="" method="post" name="ready_to_ship"  enctype="multipart/form-data" >
	<input type="hidden" name="mode" id="mode" >
	{if $smarty.post.operations_location}
		{assign var=default_operations_location value=$smarty.post.operations_location}
	{elseif $smarty.get.operations_location}
		{assign var=default_operations_location value=$smarty.get.operations_location}
	{/if}
	
	<select name="operations_location">
		{html_options options=$operations_locations selected=$default_operations_location}
	</select>
	
	{if $smarty.post.source_types}
		{assign var=source_types value=$smarty.post.source_types}
	{elseif $smarty.get.source_types}
		{assign var=source_types value=$smarty.get.source_types}
	{/if}
	
	<select name="source_type" style="width:140px;">
		{section name=ind loop=$sources}
			<option value="{$sources[ind].type_id}" {if $sources[ind].type_id == $source_type} selected {/if} >{$sources[ind].type_name} Source </option>
		{/section}
	</select>
	
	{if $smarty.post.format}
		{assign var=default_format value=$smarty.post.format}
	{elseif $smarty.get.format}
		{assign var=default_format value=$smarty.get.format}
	{/if}
	
	<select name="format" id="format"  >
		{section name=ind loop=$courier_types}
		<option value="{$courier_types[ind].code}" {if $courier eq $courier_types[ind].code}selected{/if}>{$courier_types[ind].courier_service}</option>
		{/section}
	</select>
	
	<select name="payment_method" style="width:140px;">
	{if $payment_method == "cod" } 
		<option value="none" >Other than COD</option>
		<option value="cod" selected>COD</option>
	{else}
		<option value="none" selected>Other than COD</option>
		<option value="cod" >COD</option>
	{/if}
	</select>
	
	<select name="country_location" style="width:140px">
		<option value="ALL" >ALL</option>
		<option value="IN" {if $country_location == "IN" }selected{/if}>India</option>
		<option value="OC" {if $country_location == "OC" }selected{/if}>Other Countries</option>
	</select>
	
	<select name="type" style="width:140px">
		<option value="QADone" {if $type == "QADone" }selected{/if}>All QA Done Items</option>
		<option value="OpsDone" {if $type == "OpsDone" }selected{/if}>Ops Done Items</option>
	</select>
	
	<br><br>
	<input type='submit' name='filter' value="View Result" onclick="javascript:document.ready_to_ship.mode.value='compute'; document.ready_to_ship.submit();">
	<input type='submit'  name='DownloadCSV'  value="DownloadSpreadsheet" onclick="javascript: document.ready_to_ship.mode.value = 'getCSV'; document.ready_to_ship.submit();">
	<!-- input type='submit'  name='DownloadLabels'  value="PrintLabels" onclick="javascript: document.ready_to_ship.mode.value = 'getLabels'; document.ready_to_ship.submit();">  -->
</form>
<br><br><br>
{if $error_message!=""}
	<script language="javascript" type="text/javascript">
		alert("{$error_message}")
	</script>
{/if}

<P> <font color="7D0552"><B> PLEASE CHECK AGAIN IF ALL ITEMS IN THE ORDER ARE PRESENT IN THE RIGHT QUANTITY BEFORE PACKING THE ORDER</B></font></p><BR>
<table cellpadding="2" cellspacing="1" width="100%" >
	{assign var="colspan" value=10}

	<tr class="TableHead">
		<td width="5%" nowrap="nowrap"><a href="{$navigation_script}&amp;sort_by=orderid{if $smarty.get.sort_order=='desc' && $smarty.get.sort_by=='orderid'}&amp;sort_order=asc{elseif $smarty.get.sort_order=='asc' && $smarty.get.sort_by_by=='orderid'}&amp;sort_order=desc{else}&amp;sort_order=desc{/if}">OrderId{if $smarty.get.sort_order=='desc' && $smarty.get.sort_by=='orderid'}&uArr;{elseif $smarty.get.sort_order=='asc' && $smarty.get.sort_by=='orderid'}&dArr;{else}{/if}</a></td>
		<td width="5%" nowrap="nowrap">Method</td>
		<td width="5%" nowrap="nowrap"><a href="{$navigation_script}&amp;sort_by=qtyInOrder{if $smarty.get.sort_order=='desc' && $smarty.get.sort_by=='qtyInOrder'}&amp;sort_order=asc{elseif $smarty.get.sort_order=='asc' && $smarty.get.sort_by_by=='qtyInOrder'}&amp;sort_order=desc{else}&amp;sort_order=desc{/if}">Total Qty{if $smarty.get.sort_order=='desc' && $smarty.get.sort_by=='qtyInOrder'}&uArr;{elseif $smarty.get.sort_order=='asc' && $smarty.get.sort_by=='qtyInOrder'}&dArr;{else}{/if}</a></td>
	    <td width="5%" nowrap="nowrap"><a href="{$navigation_script}&amp;sort_by=gift_status{if $smarty.get.sort_order=='desc' && $smarty.get.sort_by=='gift_status'}&amp;sort_order=asc{elseif $smarty.get.sort_order=='asc' && $smarty.get.sort_by_by=='gift_status'}&amp;sort_order=desc{else}&amp;sort_order=desc{/if}">Gift Status{if $smarty.get.sort_order=='desc' && $smarty.get.sort_by=='gift_status'}&uArr;{elseif $smarty.get.sort_order=='asc' && $smarty.get.sort_by=='gift_status'}&dArr;{else}{/if}</a></td>
		<td width="5%" nowrap="nowrap">Item ID</td>	
	    <td width="5%" nowrap="nowrap">Item Qty</td>	
	    <td width="5%" nowrap="nowrap">Product name</td>	
	    <td width="5%" nowrap="nowrap">Style Name</td>
		<td width="30%" nowrap="nowrap"><a href="{$navigation_script}&amp;sort_by=customer{if $smarty.get.sort_order=='desc' && $smarty.get.sort_by=='customer'}&amp;sort_order=asc{elseif $smarty.get.sort_order=='asc' && $smarty.get.sort_by_by=='customer'}&amp;sort_order=desc{else}&amp;sort_order=desc{/if}">Customer{if $smarty.get.sort_order=='desc' && $smarty.get.sort_by=='customer'}&uArr;{elseif $smarty.get.sort_order=='asc' && $smarty.get.sort_by=='customer'}&dArr;{else}{/if}</a></td>
	    <td width="20%" nowrap="nowrap"><a href="{$navigation_script}&amp;sort_by=date{if $smarty.get.sort_order=='desc' && $smarty.get.sort_by=='date'}&amp;sort_order=asc{elseif $smarty.get.sort_order=='asc' && $smarty.get.sort_by_by=='date'}&amp;sort_order=desc{else}&amp;sort_order=desc{/if}">Date{if $smarty.get.sort_order=='desc' && $smarty.get.sort_by=='date'}&uArr;{elseif $smarty.get.sort_order=='asc' && $smarty.get.sort_by=='date'}&dArr;{else}{/if}</a></td>
	    <td width="10%" align="right" nowrap="nowrap"><a href="{$navigation_script}&amp;sort_by=total{if $smarty.get.sort_order=='desc' && $smarty.get.sort_by=='total'}&amp;sort_order=asc{elseif $smarty.get.sort_order=='asc' && $smarty.get.sort_by_by=='total'}&amp;sort_order=desc{else}&amp;sort_order=desc{/if}">Total{if $smarty.get.sort_order=='desc' && $smarty.get.sort_by=='total'}&uArr;{elseif $smarty.get.sort_order=='asc' && $smarty.get.sort_by=='total'}&dArr;{else}{/if}</a></td>
	</tr>
	
	{if $orders}
	
		{section name=oid loop=$orders}
			{assign var=amount value=$orders[oid].total}
			{assign var=shipcost value=$orders[oid].shipping_cost}
			{assign var=vatcts value=$orders[oid].tax}
			{assign var=ref_discount value=$orders[oid].ref_discount}
			{assign var=gift_charges value=$orders[oid].gift_charges}
			{assign var=cod value=$orders[oid].cod}
			{assign var=amounttobepaid value=$amount+$shipcost+$vatcts+$cod}
			{assign var=amounttobepaid value=$amounttobepaid-$ref_discount}

			{math equation="x + ordertotal" x=$total ordertotal=$amounttobepaid assign="total"}
			{if $orders[oid].status eq "P" or $orders[oid].status eq "C"}
				{math equation="x + ordertotal" x=$total_paid ordertotal=$amounttobepaid assign="total_paid"}
			{/if}

			{if $prevorderid != $orders[oid].orderid }
			<tr class='TableSubHead'>
			{else}
			<tr>
			{/if}
			
			{if $prevorderid != $orders[oid].orderid }
				<td><a href="order.php?orderid={$orders[oid].orderid}">#{$orders[oid].orderid}</a> </td>
				<td align="center">
					{if $orders[oid].paymentmethod eq 'cod'}
						cod
					{else}
						non-cod
					{/if}
				</td>
				<td align="center">{$orders[oid].qtyInOrder}</td>
				<td nowrap="nowrap" align="center"><a href="order.php?orderid={$orders[oid].orderid}">{$orders[oid].gift_status}</a></td>
			{else}
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			{/if}
			
			<td nowrap="nowrap"> {$orders[oid].itemid} </td>
			<td nowrap="nowrap" align="center"> {$orders[oid].itemqty} </td>
			<td nowrap="nowrap"> {$orders[oid].typename} </td>
			<td nowrap="nowrap"> {$orders[oid].stylename} </td>
			
			{if $prevorderid != $orders[oid].orderid }
				<td>{$orders[oid].firstname} {$orders[oid].lastname} ({$orders[oid].login})</td>	
				<td nowrap="nowrap" align="center"><a href="order.php?orderid={$orders[oid].orderid}">{$orders[oid].date}</a></td>	
				<td nowrap="nowrap" align="right">
					<a href="order.php?orderid={$orders[oid].orderid}">{include file="currency.tpl" value=$amounttobepaid}</a>
				</td>
			{else}
				<td></td>
				<td></td>
				<td></td>
			{/if}
			
			</tr>
		
			{assign var=prevorderid value=$orders[oid].orderid}
			<input type="hidden" name="order_email[{$orders[oid].orderid}]" value="{$orders[oid].email}" />
		{/section}
		
		<tr>
			<td colspan="8"><img src="{$cdn_base}/skin1/images/spacer.gif" width="100%" height="1" alt="" /></td>
		</tr>
		
	{else}
		<tr>
			<td colspan="8" align="center"><strong>No order found for the given criteria</strong></td>
		</tr>
	{/if}

	<tr>
		<td colspan="8"><br />
			{include file="customer/main/navigation.tpl"}
		</td>
	</tr>
	
</table>

{/capture}
{/if}

{include file="dialog.tpl" title="$title" content=$smarty.capture.dialog extra='width="100%"'}