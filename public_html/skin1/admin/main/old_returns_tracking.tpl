<script type="text/javascript">
	var couriers =  {$couriers};
	var statusArr = {$statuses};
	var httpLocation = "{$http_location}";
	var dateNames = {$alldates};
</script>

<link rel="stylesheet" type="text/css" href="{$http_location}/admin/extjs/resources/css/ext-all.css"/>

<script type="text/javascript" src="{$http_location}/admin/extjs/adapter/ext/ext-base.js"></script>

<script type="text/javascript" src="{$http_location}/admin/extjs/ext-all.js"></script>
<script type="text/javascript" src="{$http_location}/admin/extjs/plugins/superselectbox/SuperBoxSelect.js"></script>
<link rel="stylesheet" type="text/css" href="{$http_location}/admin/extjs/plugins/superselectbox/superboxselect.css"/>
<script type="text/javascript" src="old_returns_tracking.js"></script>
<br>
<br>
<div style="text-align:center;font-size:1.5em;">Returns Tracker</div>
<br>
<div id="contentPanel"></div>
