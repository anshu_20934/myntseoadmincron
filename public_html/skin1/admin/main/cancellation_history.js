var today = new Date();
var today_date_str = (today.getMonth()+1)+"/"+today.getDate()+"/"+today.getFullYear();

Ext.onReady(function() {
	Ext.QuickTips.init();
	var myMask = new Ext.LoadMask(Ext.getBody(), {msg: "Loading..."});
	Ext.Ajax.on('beforerequest', myMask.show, myMask);
	Ext.Ajax.on('requestcomplete', myMask.hide, myMask);
	Ext.Ajax.on('requestexception', myMask.hide, myMask);
	
	
	var refundProcessingPanel = new Ext.form.FormPanel({
		id : 'addTxnPanel',
		height: 250,
		border : false,
		bodyStyle : 'padding: 5px',
		items : [{
			layout : 'table',
			border : false,
			layoutConfig : {columns : 1},
			defaults : {border : false, layout : 'form', bodyStyle : 'padding-left : 15px'},
			items: [{
				items: [{
					width: 300,
		        	xtype : 'combo',
		        	emptyText : 'Select Txn Type',
		        	store : new Ext.data.ArrayStore({
		        	    fields: ['id', 'name'],
		        	    data : [["C", "Credit"], ["D", "Debit"]]
		        	}),
		        	mode: 'local',
		        	displayField : 'name',
		        	valueField : 'id',
		        	triggerAction: 'all',
		        	fieldLabel : 'Transaction Type',
		        	name : 'addtxn_type',
		        	id : 'addtxn_type'
		        }]
			},{
				items: [{
					width: 300,
					xtype : 'numberfield',
		        	id : 'addtxn_amount',
		        	fieldLabel : 'Amount'
				}]
			},{
				items: [{
					xtype : 'textarea',
		        	id : 'addtxn_description',
		        	fieldLabel : 'Description',
		        	width: 300,
		        	height: 100
				}]
			}]
		}]
	});
	
	
	
	var searchPanel = new Ext.form.FormPanel({
		renderTo : 'contentPanel',
		title : 'Search Cancelled Orders/Items',
		id : 'formPanel',
		autoHeight : true,
		collapsible : true,
		border : true,
		bodyStyle : 'padding: 5px',
		width : 1000,
		items : [{
			layout : 'table',
			border : false,
			layoutConfig : {columns : 2},
			defaults : {width : 450, border : false, 
				layout : 'form', bodyStyle : 'padding-left : 15px'},
			items : [
			         {
			        	 items : [{
			        		xtype : 'combo',
					        store : new Ext.data.ArrayStore({
					        	fields: ['id', 'name'],
					            data : [["F", "Full"], ["P", "Part"]]
					        }),
					        mode: 'local',
					        displayField : 'name',
					        valueField : 'id',
					        triggerAction: 'all',
					        fieldLabel : 'Cancellation Type',
					        name : 'cancellation_type',
					        value: 'F'
			        	 }]
			         },
			         {
			        	 items : [{
				        	xtype : 'combo',
				        	store : new Ext.data.ArrayStore({
					        	fields: ['id', 'name'],
					            data : [["On", "Online"], ["cod", "COD"]]
					        }),
					        mode: 'local',
					        displayField : 'name',
					        valueField : 'id',
					        triggerAction: 'all',
					        fieldLabel : 'Payment Method',
					        name : 'payment_method'
			        	 }]
			         },
			         {
						items : [{
							xtype : 'numberfield',
					       	id : 'orderid',
					       	fieldLabel : 'Order Id'
						}]
					 },
			         {
						items :[{
							xtype : 'textfield',
				        	id : 'customerlogin',
				        	fieldLabel : 'Customer Login',
				        	width : 300
						}]
					 },
			         {
			        	 items : [{
			        		 xtype : 'datefield',
			        		 name : 'from_date',
			        		 width : 150,
			        		 fieldLabel : "From Date",
			        		 value: today_date_str,
			        		 maxValue : new Date(),
			        		 listeners: {
				                    select: function (datefield) {
				                    	var enddatefield = searchPanel.getForm().findField("to_date");
				                    	var enddate = enddatefield.getValue();
				                    	var fromdate = new Date(datefield.getValue());
				                    	if(!enddate || enddate==""){
				                    		enddate = new Date();
				                    	} else {
				                    		enddate = new Date(enddate);
				                    	}
				                    	
				                    	if((fromdate.getTime() - enddate.getTime())/(24*60*60*1000) >= 0) {
				                    		enddate.setTime(fromdate.getTime() + 1*24*60*60*1000);
				                    		if((enddate.getTime() - today.getTime())/(24*60*60*1000) > 0){
				                    			enddate = new Date();
				                    		}
				                    	}
				                    	enddatefield.setValue(enddate);
				                  	}
				                }
			        	 }]
			         },
			         {
			        	 items : [{
			        		 xtype : 'datefield',
			        		 name : 'to_date',
			        		 width : 150,
			        		 fieldLabel : "To Date",
			        		 value: today_date_str,
			        		 maxValue : new Date(),
			        		 listeners: {
				                    select: function (datefield) {
				                    	var fromdatefield = searchPanel.getForm().findField("from_date");
				                    	var fromdate = fromdatefield.getValue();
				                    	if(!fromdate || fromdate==""){
				                    		fromdate = new Date();
				                    	} else {
				                    		fromdate = new Date(fromdate);
				                    	}
				                    	var enddate = new Date(datefield.getValue());
				                    	// if end date is selected out of range..
				                    	if( (enddate.getTime() - fromdate.getTime())/(24*60*60*1000) < 0 ){
				                    		fromdate.setTime(enddate.getTime() - 1*24*60*60*1000);
				                    		fromdatefield.setValue(fromdate);
				                    	}
				                    }
				                }
			        	 }]
			         }
			]
		}],
		buttons : [ {
			text : 'Search',
			handler : function() {
				reloadGrid();
			}
		},{
			text : 'Export Results',
			handler : function() {
				var params = getSearchPanelParams();
				
				if (params == false) {
					Ext.MessageBox.alert('Error', 'Errors in the search form');
					return false;
				}
				params["action"] = "download";
				
				Ext.Ajax.request({
					url : 'cancellation_history.php',
					params : params,
				    success : function(response, action) {
						var jsonObj = Ext.util.JSON.decode(response.responseText);
						window.open(httpLocation + "/bulkorderupdates/" + jsonObj);
					}
				});
			}
		},{
			text : 'Email Contents',
			handler : function() {
				var params = getSearchPanelParams();
				
				if (params == false) {
					Ext.MessageBox.alert('Error', 'Errors in the search form');
					return false;
				}
				params["action"] = "email_report";
				
				Ext.Ajax.request({
					url : 'cancellation_history.php',
					params : params,
				    success : function(response, action) {
						var jsonObj = Ext.util.JSON.decode(response.responseText);
						if(jsonObj[0] != "SUCCESS") {
							Ext.MessageBox.alert('Error', jsonObj[1]);
						}
					}
				});
			}
		}],
	});
	
	function getSearchPanelParams() {
		var form = searchPanel.getForm();
		
		if (form.isValid()) {
			if(form.findField("from_date").getRawValue() == "" || 
					form.findField("to_date").getRawValue() == "" ) {
				Ext.MessageBox.alert('Error', 'Date range can not be empty');
				return false;
			}
			
			if(form.findField("cancellation_type").getValue() == "") {
				Ext.MessageBox.alert('Error', 'Please select a cancellation type');
				return false;
			}
			
			var params = {};
			params["cancellation_type"] = form.findField("cancellation_type").getValue();
			params["payment_method"] = form.findField("payment_method").getValue();
			params["orderid"] = form.findField("orderid").getValue();
			params["customerlogin"] = form.findField("customerlogin").getValue();
			params["from_date"] = form.findField("from_date").getRawValue();
			params["to_date"] = form.findField("to_date").getRawValue();
			return params;
		} else {
			return false;
		}
	}
	
	function reloadGrid() {
		
		var params = getSearchPanelParams();
		
		if (params == false) {
			Ext.MessageBox.alert('Error', 'Errors in the search form');
			return false;
		}
		params["action"] = "search";
		
		searchResultsStore.baseParams = params;
		searchResultsStore.load();
	}
	
	var orderNameLinkRender = function(value, p, record) {
		var retLink = '<a href="' + httpLocation + '/admin/order.php?orderid=' + record.data["orderid"] + '" target="_blank">' + record.data["order_name"] + '</a>';
		if (record.data['customizable'] == '0') {
			retLink = retLink + "<span style='color:red'> (NP) <span>";
		}
		return retLink;
	}
	
	var colModel = new Ext.grid.ColumnModel({
		defaults: {
	        sortable: true
	    },
	    columns: [
	        new Ext.grid.RowNumberer(),
	        {id : 'order_name', header : "Order Name", sortable: true, dataIndex : 'order_name', renderer : orderNameLinkRender},
	        {id : 'payment_method', header : "Payment Method", sortable: true, dataIndex : 'payment_method'},
	        {id : 'orderid', header : "Order ID", sortable: true, dataIndex : 'orderid', sortable: false},
	        {id : 'itemid', header : "Item ID", sortable: true, dataIndex : 'itemid'},
	        {id : 'total', header : "Total Amount", sortable: false, dataIndex : 'total'},
	        {id : 'discount', header : "System Discount", sortable: false, dataIndex : 'discount'},
	        {id : 'coupon_discount', header : "Coupon Discount", sortable: false, dataIndex : 'coupon_discount'},
	        {id : 'final_amount', header : "Amount Paid", sortable: false, dataIndex : 'final_amount'},
	        {id : 'refund', header : "Refund Given", sortable: false, dataIndex : 'refund'},
	        {id : 'refund_mode', header : "Refund Mode", sortable: false, dataIndex : 'refund_mode'},
	        {id : 'couponcode', header : "Coupon Code", sortable: false, dataIndex : 'couponcode'},
	        {id : 'pgname', header : "PG Name", sortable: false, dataIndex : 'pgname'},
	        {id : 'login', header : "Customer Login", sortable: true, dataIndex : 'login'},
	        {id : 'date', header : "Order Date", sortable: true, dataIndex : 'date'},
	        {id : 'canceltime', header : "Cancelled Date", sortable: true, dataIndex : 'canceltime'},
	        {id : 'cancel_reason', header : "Cancellation Reason", sortable: false, dataIndex : 'cancel_reason'}
  	  	]
	});
	
	var searchResultsStore = new Ext.data.Store({
		url: 'cancellation_history.php',
		pruneModifiedRecords : true,
		reader: new Ext.data.JsonReader({
	  		root: 'results',
	  	  	totalProperty: 'count',
	  	  	fields : ['order_name','payment_method','orderid','itemid','total','discount','coupon_discount',
	  	  	          'final_amount', 'refund','refund_mode','pgname','couponcode','login','date','canceltime', 'cancel_reason']
		}),
	});

	var pagingBar = new Ext.PagingToolbar({
		pageSize : 30,
	    store: searchResultsStore,
	    displayInfo: true
	});
	
	var actionsMenu = new Ext.menu.Menu({ 
		items: [ 
	        {
	        	text : 'Mark Refund Processed',
        		tooltip : 'Mark Refund Processed',
        		iconCls : 'add',
        		handler : function() {
        			var selected = searchResults.getSelectionModel().getSelected();
        			var data = {"action" : "mark_refund_process", "login": loginUser};
        			var titleString = "";
        			if(selected.data.itemid != "-") {
        				data["itemid"] = selected.data.itemid;
        				data["orderid"] = selected.data.orderid;
        				titleString = 'Enter Refund Details for Item - '+data["itemid"]+' in Order '+data["orderid"];
        			} else {
        				data["orderid"] = selected.data.orderid;
        				titleString = 'Enter Refund Details for Order - '+data["orderid"]; 
        			}
        			
        			var window =  new Ext.Window({
        				title: titleString,
        				id: 'refunddetailswindow',
        				width: 500,
        				items : [{
        					layout : 'table',
        					border : false,
        					layoutConfig : {columns : 1},
        					defaults : {border : false, layout : 'form', bodyStyle : 'padding-left : 15px; margin-top : 15px'},
        					items: [{
        						items: [{
        							width: 300,
        				        	xtype : 'combo',
        				        	emptyText : 'Select Refund Mode',
        				        	store : new Ext.data.ArrayStore({
        				        	    fields: ['id', 'name'],
        				        	    data : [["PG", "Via Payment Gateway"], ["C", "Coupon"]]
        				        	}),
        				        	mode: 'local',
        				        	displayField : 'name',
        				        	valueField : 'id',
        				        	triggerAction: 'all',
        				        	fieldLabel : 'Refund Mode',
        				        	name : 'refund_mode',
        				        	id : 'refund_mode',
        				        	value: 'PG',
        				        	listeners: {
        				        		'select': function() {
        				        			if(this.getValue() == 'C') {
        				        				Ext.getCmp('refund_coupon').enable();
        				        				Ext.getCmp('pg_name').disable();
        				        			} else {
        				        				Ext.getCmp('refund_coupon').disable();
        				        				Ext.getCmp('pg_name').enable();
        				        			}
        				        		}
        				        	}
        				        }]
        					},{
        						items: [{
        							width: 300,
        							xtype : 'textfield',
        				        	id : 'pg_name',
        				        	fieldLabel : 'Payment Gateway',
        						}]
        					},{
        						items: [{
        							width: 300,
        							xtype : 'textfield',
        				        	id : 'refund_coupon',
        				        	fieldLabel : 'Coupon Code',
        				        	disabled: true
        						}]
        					},{
        						items: [{
        							width: 300,
        							xtype : 'textfield',
        				        	id : 'refund_amount',
        				        	fieldLabel : 'Refund Amount'
        						}]
        					},{
        						items: [{
        							xtype : 'textarea',
        				        	id : 'refund_comment',
        				        	fieldLabel : 'Comments',
        				        	width: 300,
        				        	height: 100
        						}]
        					}]
        				}],
        				buttons: [{
        					 id: 'confirm',
        					 text: 'Mark Processed',
        					 handler: function() {
        						 var errorMsg = "";
        						 data['refund_mode'] = Ext.getCmp('refund_mode').getValue();
        						 if(data['refund_mode'] == "") {
        							 errorMsg += 'Please select Refund Mode<br>';
        						 }
        						 if(data['refund_mode'] == 'C') {
        							 data['coupon'] = Ext.getCmp('refund_coupon').getValue();
        							 if(data['coupon'] == "") {
        								 errorMsg += 'Refund Coupon code is missing<br>';
        							 }
        						 } else {
        							 data['pg_name'] = Ext.getCmp('pg_name').getValue();
        							 if(data['pg_name'] == "") {
        								 errorMsg += 'Payment Gateway Name is missing<br>';
        							 }
        						 }
        						 data['refund_amount'] = Ext.getCmp('refund_amount').getValue();
        						 if(data['refund_amount'] == "") {
    								 errorMsg += 'Refund Amount is missing<br>';
    							 }
        						 if(errorMsg != "") {
        							 Ext.MessageBox.alert('Error', errorMsg);
        							 return;
        						 }
        						 data['refund_comment'] = Ext.getCmp('refund_comment').getValue();
        						 
        						 Ext.Ajax.request({
    		        				url : 'cancellation_history.php',
    		        				params : data,
    		        			    success : function(response, action) {
    		        			    	window.destroy();
    		        					reloadGrid();
    		        				}
    		        			});
        					 }
        				},
        				{
        					id: 'cancel',
        					text: 'Cancel',
        					handler: function() {
        						window.destroy();
        					}
        				}]
        			}).show();
        			window.center();
        		}
	        }
		] 
	});
	
	var searchResults = new Ext.grid.EditorGridPanel({
	    store  : searchResultsStore,
	    cm : colModel,
	    id : 'searchResults-panel',
	    title : 'Search Results',
	    sm: new Ext.grid.RowSelectionModel(),
	    frame : true,
		loadMask : false,
		autoHeight : true,
		bbar : pagingBar,
		stripeRows : true,
		autoScroll : true,
		viewConfig : {
			enableRowBody : true
		},
		listeners : {
			"rowcontextmenu" : function(grid, row, event) {
				grid.getSelectionModel().selectRow(row);
				var selected = grid.getSelectionModel().getSelected();
				if(selected.data.payment_method != 'cod' && selected.data.refund == "-") {
					event.preventDefault();
	    	    	event.stopEvent();
	    	    	actionsMenu.showAt(event.getXY());
				}
			}
		}
	});
	
	var mainPanel = new Ext.Panel({
		renderTo : 'contentPanel',
		border : false,
		items : [
		     searchPanel, searchResults
		],
		listeners : {
			'afterrender' : function(comp) {
				reloadGrid();
			}
		}
	});
});