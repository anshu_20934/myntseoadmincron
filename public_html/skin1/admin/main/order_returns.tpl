{* $Id: order_returns.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
<br /><br />
{capture name=dialog}
	<div style="padding: 20px;">
		<div style="font-size:12px; padding: 5px;">List of Items returned from this order:</div>
		<table cellspacing=0 cellpadding=0 border=1 width=100% style="border: 1px solid black; border-collapse: collapse;">
			<tr height=30 style="background-color: #ccffff">
				<th style="font-size:14px; padding: 5px;">Return No</th>
				<th style="font-size:14px; padding: 5px;">Product Name</th>
				<th style="font-size:14px; padding: 5px;">Sku Code</th>
				<th style="font-size:14px; padding: 5px;">Article Number</th>
				<th style="font-size:14px; padding: 5px;">Status</th>
				<th style="font-size:14px; padding: 5px;">Quantity</th>
				<th style="font-size:14px; padding: 5px;">Size</th>
				<th style="font-size:14px; padding: 5px;">Total Amount</th>
				<th style="font-size:14px; padding: 5px;">Request Date</th>
			</tr>
			{section name=index loop=$order_returns}
			<tr bgcolor="{cycle values="#eeddff,#d0ddff"}">
				<td style="font-size:12px; padding: 5px;"><a href="/admin/returns/return_request.php?id={$order_returns[index].returnid}" target="_blank">{$order_returns[index].returnid}</a></td>
				<td style="font-size:12px; padding: 5px;">{$order_returns[index].productStyleName}</td>
				<td style="font-size:12px; padding: 5px;">{$order_returns[index].skucode}</td>
				<td style="font-size:12px; padding: 5px;">{$order_returns[index].article_number}</td>
				<td style="font-size:12px; padding: 5px;">{$order_returns[index].status_display}</td>
				<td style="font-size:12px; padding: 5px;">{$order_returns[index].quantity}</td>
				<td style="font-size:12px; padding: 5px;">{$order_returns[index].sizeoption}</td>
				<td style="font-size:12px; padding: 5px;">{$order_returns[index].item_total_amount}</td>
				<td style="font-size:12px; padding: 5px;">{$order_returns[index].createddate_display}</td>
			</tr>
			{/section}
		</table>
	</div>
{/capture}
{include file="dialog.tpl" title='Returned Items Details' content=$smarty.capture.dialog extra='width="99%"'}
