{* $Id: featured_products.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
<base href="{$http_location}/">
{include file="main/include_js.tpl" src="main/popup_product.js"}
<a name="featured" />
{literal}
<style type="text/css">
.article_table {font-size:14px;}
.article_table tr td {padding:10px}
.article_table tr td input,select{font-size:11px;line-height:12px}
.error_block{
 	background:red;
 }
</style>
 <script language="Javascript">
	function checkForDuplicateFilterOrder(){
		var arr_order=[];
		var no_error=true;
		$('.data_row').each(function(index) {
				var dataRow = $(this);
				if($(this).find(":checkbox").attr('checked')){
					if(jQuery.inArray($(this).find('.filter_order').val(),arr_order) != -1){
						alert('filter order:'+$(this).find('.filter_order').val()+' already exist. Please specify different orders');
						no_error=false;
						jQuery(dataRow).addClass('error_block');
						return false;
					}
					arr_order.push($(this).find('.filter_order').val());
				}
			});
		return no_error;
		}
</script>
{/literal}

{$lng.txt_featured_products}

<br /><br />

{capture name=dialog}

<form action="{$form_action}" method="post" name="groupfrm" enctype="multipart/form-data" onsubmit="return checkForDuplicateFilterOrder();">
<input type="hidden" name="mode"  />
<table width="100%">
<tr>
<td colspan="5"><br /><br />{include file="main/subheader.tpl" title="Article Types"}</td>
</tr>
</table>

<table cellpadding="3" cellspacing="1" width="100%" class="article_table">

<tr class="TableHead">
	<td width="10%">&nbsp;</td>
	<td width="10%">Type Name</td>
	<td width="10%">Type Code</td>
	<td width="15%" align="center">Sub Category</td>
	<td width="15%">Master Category</td>
	<td width="10%">filter order</td>
	<td width="10%" align="center">Active</td>
	<td width="10%" align="center">Enable Social Sharing</td>
</tr>

{if $article_types}

{section name=grid loop=$article_types}

<tr class="{cycle values="data_row ,data_row TableSubHead"}">
    <input type="hidden" name="posted_data[{$article_types[grid].id}][id]" value={$article_types[grid].id} />
	<td align="center"><input type="checkbox" name="posted_data[{$article_types[grid].id}][to_delete]" /></td>
	<td align="center"><input type="text" name="posted_data[{$article_types[grid].id}][typename]" value="{$article_types[grid].typename}" /></td>
	<td align="center"><input type="text" name="posted_data[{$article_types[grid].id}][typecode]" value="{$article_types[grid].typecode}" /></td>
	<td align="center">
	<select name="posted_data[{$article_types[grid].id}][parent1]" onchange="selectMasterCat(this.value,'article_sec_master_cat')">
		<option value="-1">NA</option>
		{foreach from=$sub_categories item=types}
		    <option value="{$types.id}" {if $article_types[grid].parent1 eq $types.id} selected="selected"{/if} >{$types.typename}</option>
		{/foreach}

	</select>
	</td>
	<td align="center">
	<select name="posted_data[{$article_types[grid].id}][parent2]" id="article_sec_master_cat">
		<option value="-1">NA</option>
		{foreach from=$master_categories item=types}
		    <option value="{$types.id}" {if $article_types[grid].parent2 eq $types.id} selected="selected"{/if} >{$types.typename}</option>
		{/foreach}

	</select>
	</td>
	<td align="center"><input type="text" class="filter_order" name="posted_data[{$article_types[grid].id}][filter_order]" value="{$article_types[grid].filter_order}" /></td>
	<td align="center">
	<select name="posted_data[{$article_types[grid].id}][is_active]">
		<option value="1"{if $article_types[grid].is_active eq "1"} selected="selected"{/if}>{$lng.lbl_yes}</option>
		<option value="0"{if $article_types[grid].is_active eq "0"} selected="selected"{/if}>{$lng.lbl_no}</option>
	</select>
	</td>
	<td align="center">
	<select name="posted_data[{$article_types[grid].id}][enable_social_sharing]">
		<option value="1"{if $article_types[grid].enable_social_sharing eq "1"} selected="selected"{/if}>{$lng.lbl_yes}</option>
		<option value="0"{if $article_types[grid].enable_social_sharing eq "0"} selected="selected"{/if}>{$lng.lbl_no}</option>
	</select>
	</td>
</tr>

{/section}

{else}

<tr>
 <td colspan="4" align="center">No Article types available.</td>
</tr>
{/if}



<tr>
	<td colspan="6" class="SubmitBox" align="center">
	<input type="button" value="Delete Selected" onclick="return confirmDelete();" />
	<input type="submit" value="Update Selected" onclick="javascript: document.groupfrm.mode.value = 'update'; " />
    <input type="button" value="Add Article Type" onclick="showDialog('addArticle')" />
	</td>
</tr>


</table>
<table width="100%">
<tr>
<td colspan="5"><br /><br />{include file="main/subheader.tpl" title="Sub Categories"}</td>
</tr>
</table>
<!-- subcategory starts here -->
<table cellpadding="3" cellspacing="1" width="100%" class="article_table">

<tr class="TableHead">
	<td width="10%">&nbsp;</td>
	<td width="20%">Sub Category Name</td>
	<td width="20%">Master Category</td>
	<td width="10%" align="center">Active</td>
</tr>

{if $sub_categories}

{section name=grid loop=$sub_categories}

<tr{cycle values=", class='TableSubHead'"}>
    <input type="hidden" name="posted_data[{$sub_categories[grid].id}][id]" value={$sub_categories[grid].id} />
	<td align="center"><input type="checkbox" name="posted_data[{$sub_categories[grid].id}][to_delete]" /></td>
	<td align="center"><input type="text" name="posted_data[{$sub_categories[grid].id}][typename]" value="{$sub_categories[grid].typename}" /></td>
	<td align="center">
    <select name="disabled_master_category" disabled="disabled" >
    		{foreach from=$master_categories item=types}
    		    <option value="{$types.id}" {if $sub_categories[grid].parent1 eq $types.id} selected="selected"{/if} >{$types.typename}</option>
    		{/foreach}

    	</select>

	<select name="posted_data[{$sub_categories[grid].id}][parent1]" style="display:none;">
		{foreach from=$master_categories item=types}
		    <option value="{$types.id}" {if $sub_categories[grid].parent1 eq $types.id} selected="selected"{/if} >{$types.typename}</option>
		{/foreach}

	</select>

	</td>
	<td align="center">
	<select name="posted_data[{$sub_categories[grid].id}][is_active]" >
		<option value="1"{if $sub_categories[grid].is_active eq "1"} selected="selected"{/if}>{$lng.lbl_yes}</option>
		<option value="0"{if $sub_categories[grid].is_active eq "0"} selected="selected"{/if}>{$lng.lbl_no}</option>
	</select>
	</td>
</tr>

{/section}

{else}

<tr>
 <td colspan="4" align="center">No Sub Categories available.</td>
</tr>
{/if}



<tr>
	<td colspan="5" class="SubmitBox" align="center">
	<input type="button" value="Delete Selected" onclick="return confirmDelete();" />
	<input type="submit" value="Update Selected" onclick="javascript: document.groupfrm.mode.value = 'update'; document.groupfrm.submit();" />
    <input type="button" value="Add Sub Category" onclick="showDialog('addSubCategory')" />
	</td>
</tr>


</table>
<table width="100%">
<tr>
<td colspan="5"><br /><br />{include file="main/subheader.tpl" title="Master Categories"}</td>
</tr>
</table>
<!-- subcategory starts here -->
<table cellpadding="3" cellspacing="1" width="100%" class="article_table">

<tr class="TableHead">
	<td width="10%">&nbsp;</td>
	<td width="20%">Master Category</td>
	<td width="10%" align="center">Active</td>
</tr>

{if $master_categories}

{section name=grid loop=$master_categories}

<tr{cycle values=", class='TableSubHead'"}>
    <input type="hidden" name="posted_data[{$master_categories[grid].id}][id]" value={$master_categories[grid].id} />
	<td align="center"><input type="checkbox" name="posted_data[{$master_categories[grid].id}][to_delete]" /></td>
	<td align="center"><input type="text" name="posted_data[{$master_categories[grid].id}][typename]" value="{$master_categories[grid].typename}" /></td>
	<td align="center">
	<select name="posted_data[{$master_categories[grid].id}][is_active]">
		<option value="1"{if $master_categories[grid].is_active eq "1"} selected="selected"{/if}>{$lng.lbl_yes}</option>
		<option value="0"{if $master_categories[grid].is_active eq "0"} selected="selected"{/if}>{$lng.lbl_no}</option>
	</select>
	</td>
</tr>

{/section}

{else}

<tr>
 <td colspan="4" align="center">No Master Categories Available.</td>
</tr>
{/if}



<tr>
	<td colspan="5" class="SubmitBox" align="center">
	<input type="button" value="Delete Selected" onclick="return confirmDelete();" />
	<input type="submit" value="Update Selected" onclick="javascript: document.groupfrm.mode.value = 'update'; document.groupfrm.submit();" />
    <input type="button" value="Add Master Category" onclick="showDialog('addMasterCategory')" />
	</td>
</tr>


</table>
</form>
<div id="addMasterCategory" style="display:none">
<form name="frmG" action="{$form_action}" method="post" enctype="multipart/form-data" id="master_form">
<input type="hidden" name="mode" value="addgroup" />
<input type="hidden" name="addtype" value="master" />
<table cellpadding="3" cellspacing="1" width="100%" class="article_table">

   <tr>
        <td><h2>Add Master Category</h2></td>
        <td>&nbsp;</td>
   </tr>
   <tr>
        <td>Type Name </td>
        <td><input type="text" name="typename" value="" style="width: 150px;" id="master_typename"/></td>
   </tr>
   <tr>
          <td>Active</td>
             <td>
                <select name="is_active" style="width: 150px;">
                    <option value="1" >{$lng.lbl_yes}</option>
                    <option value="0">{$lng.lbl_no}</option>
                </select>
        	</td>
   </tr>
   <tr>
   <td colspan="2" align="center">
   <input type="button" value="{$lng.lbl_add|escape}" name="sbtFrm" style="padding:5px" onclick="addArticle('master')"/>
   &nbsp;&nbsp;&nbsp;<input type="button" onclick="cancel_dialog('addMasterCategory')" style="padding:5px" value="Cancel"/>
   </td>
   </tr>

</table>
</form>
</div>

<div id="addSubCategory" style="display:none">
<form name="frmG" action="{$form_action}" method="post" enctype="multipart/form-data" id="sub_form">
<input type="hidden" name="mode" value="addgroup" />
<input type="hidden" name="addtype" value="sub" />
<table cellpadding="3" cellspacing="1" width="100%" class="article_table">

   <tr>
        <td><h2>Add Sub Category</h2></td>
        <td>&nbsp;</td>
   </tr>
   <tr>
        <td>Type Name </td>
        <td><input type="text" name="typename" value=""  id="sub_typename" style="width: 150px;"/></td>
   </tr>
   <tr>
        <td>Master Category</td>
        <td >
        <select name="parent1" id="sub_parent1" style="width: 150px;">
            <option value="-1">NA</option>
            {foreach from=$master_categories item=types}
                <option value="{$types.id}">{$types.typename}</option>
		    {/foreach}
        </select>
        </td>
   </tr>
   <tr>
          <td>Active</td>
             <td>
                <select name="is_active" style="width: 150px;">
                    <option value="1" >{$lng.lbl_yes}</option>
                    <option value="0">{$lng.lbl_no}</option>
                </select>
        	</td>
   </tr>
   <tr>
   <td colspan="2" align="center">
   <input type="button" value="{$lng.lbl_add|escape}" name="sbtFrm" style="padding:5px" onclick="addArticle('sub')" />
   &nbsp;&nbsp;&nbsp;<input type="button" onclick="cancel_dialog('addSubCategory')" style="padding:5px" value="Cancel"/>
   </td>
   </tr>

</table>
</form>
</div>
<div id="addArticle" style="display:none">
<form name="frmG" action="{$form_action}" method="post" enctype="multipart/form-data" id="article_form">
<input type="hidden" name="mode" value="addgroup" />
<input type="hidden" name="addtype" value="article" />
<table cellpadding="3" cellspacing="1" width="100%" class="article_table">
   
   <tr>
        <td><h2>Add Article</h2></td>
        <td>&nbsp;</td>
   </tr>
   <tr>
        <td>Type Name </td>
        <td><input type="text" name="typename" value="" id="article_typename" style="width: 150px;" /></td>
   </tr>
   <tr>
        <td>Type Code </td>
        <td><input type="text" name="typecode" value="" style="width: 150px;" /></td>
   </tr>
   <tr>
        <td>Sub Category</td>
        <td >
        <select name="parent1" id="article_parent1" style="width: 150px;" onchange="selectMasterCat(this.value,'article_parent2')">
            <option value="-1">NA</option>
            {foreach from=$sub_categories item=types}
                <option value="{$types.id}">{$types.typename}</option>
		    {/foreach}
        </select>
        </td>
   </tr>
   <tr>
        <td>Master Category</td>
             <td>
             <select name="parent2" id="article_parent2" style="width: 150px;">
        		<option value="-1">NA</option>
        		{foreach from=$master_categories item=types}
        		    <option value="{$types.id}">{$types.typename}</option>
        		{/foreach}

        	</select>
             </td>
   </tr>
   <tr>
        <td>Filter Order </td>
        <td><input type="text" name="filter_order" value="" style="width: 150px;" /></td>
   </tr>
   <tr>
          <td>Active</td>
             <td>
                <select name="is_active" style="width: 150px;">
                    <option value="1" >{$lng.lbl_yes}</option>
                    <option value="0">{$lng.lbl_no}</option>
                </select>
        	</td>
   </tr>
   <tr>
          <td>Enable Social Sharing</td>
             <td>
                <select name="enable_social_sharing" style="width: 150px;">
                    <option value="1" >{$lng.lbl_yes}</option>
                    <option value="0">{$lng.lbl_no}</option>
                </select>
        	</td>
   </tr>
   <tr>
   <td colspan="2" align="center">
   <input type="button" value="{$lng.lbl_add|escape}" style="padding:5px" name="sbtFrm" onclick="addArticle('article')" />
   &nbsp;&nbsp;&nbsp;<input type="button" onclick="cancel_dialog('addArticle')" style="padding:5px"  value="Cancel"/>
   </td>
   </tr>

</table>
</form>
</div>
{/capture}
<script
	type="text/javascript"
	src="{$http_location}/skin1/myntra_js/jquery.min.js"></script>
<link
	rel="stylesheet" type="text/css"
	href="{$http_location}/skin1/myntra_css/thickbox3.css" />
<script
	type='text/javascript'
	src='{$http_location}/skin1/myntra_js/thickbox3.js'></script>
{literal}
  <script language="Javascript">
      function confirmDelete()
      {
          var flag = confirm('Are you sure you want to delete?')
          if(!flag)
          {
             return false;
          }
          else
          {
          	 document.groupfrm.mode.value = 'delete';
          	 document.groupfrm.submit();
          	 return true;
          }
      }
    function showDialog(dg){
	$("#"+dg).css("display","block");
	tb_show("","#TB_inline?height=350&amp;width=500&amp;inlineId="+dg+";modal=true","");
	}
	function cancel_dialog(dg){
	$("#"+dg).css("display","none");
	tb_remove();
}
function addArticle(id)
{
    if(id=='article')
    {
        if($.trim($("#article_typename").val()) =='')
        {
            alert("Please enter Type Name ");
            return false;
        }
        else if($.trim($("#article_parent1").val()) =='-1')
        {
            alert("Please Select Sub Category");
            return false;
        }
        else if($.trim($("#article_parent2").val()) =='-1')
        {
            alert("Please Select Master Category");
            return false;
        }
        else
            $("#article_form").trigger("submit");
    }
    else if(id == 'sub')
    {
        if($.trim($("#sub_typename").val()) =='')
        {
            alert("Please enter Type Name ");
            return false;
        }
        else if($.trim($("#sub_parent1").val()) =='-1')
        {
            alert("Please Select Master Category");
            return false;
        }
        else
            $("#sub_form").trigger("submit");
    }
    else if(id =='master')
    {
        if($.trim($("#master_typename").val()) =='')
        {
            alert("Please enter Type Name ");
            return false;
        }
        else
            $("#master_form").trigger("submit");
    }
}
function selectMasterCat(val,id)
{
    $("#"+id).val(sub_category[val]);
}
var sub_category = new Array();
  </script>
{/literal}
<script type="text/javascript">
{foreach from=$sub_categories item=types}
    sub_category['{$types.id}']= '{$types.parent1}';
{/foreach}
</script>
{include file="dialog.tpl" title="Catalog Classification" content=$smarty.capture.dialog extra='width="100%"'}
<br /><br />


