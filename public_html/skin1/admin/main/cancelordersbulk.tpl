{* $Id: cancelorders.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
{include file="main/include_js.tpl" src="main/popup_product.js"}
<script type="text/javascript">
	var progressKey = '{$progresskey}';
</script>
<link rel="stylesheet" href="{$SkinDir}/skin1_admin_operations.css" />
{literal}
<script type="text/javascript">
	function showUploadStatusWindow() {
		if($("#cancellation_type").val() == "0"){
			alert("Please select a cancellation type");
			return false;
		}

		if($("#cancellation_reason").val() == "0"){
			alert("Please select a cancellation reason");
			return false;
		}

		if($("textarea#cancellation_comment").val() == ""){
			alert("Please enter cancellation comment");
			return false;
		}
		
		$("#uploadstatuspopup").css("display","block");
		$("#uploadstatuspopup-content").css("display","block");
		$("#uploadstatuspopup_title").css("display","block");
		
		var oThis = $("#uploadstatuspopup-main");
		var topposition = ( $(window).height() - oThis.height() ) / 2;
		if ( topposition < 10 ) 
			topposition = 10;
		oThis.css("top", ( topposition + $(window).scrollTop() + "px"));
		oThis.css("left", ( $(window).width() - oThis.width() ) / 2+$(window).scrollLeft() + "px");

		setInterval(trackStatus, 1000);

		return true;
	}
	
	function trackStatus() {
		$.get("getprogressinfo.php?progresskey="+progressKey,
			function(percent) {
				if(percent != "false") {
					$("#progress_bar").css('width', percent+"%");
					$("#progress_text").html(percent+" % Complete");
				}
			}
		);
	}

	$(function(){
		$("#cancellation_type").change(function(){
			$("textarea#cancellation_comment").val($("#cancellation_type option:selected").text()+"\n");
			
			var reasonsEle = $("#cancellation_reason");
			reasonsEle.children('option:not(:first)').remove();

			var reason = document.getElementById("cancellation_type").value;
			$(".cancel_reason").css("display","none");
			$(".cancel_reason").attr('disabled', 'disabled');
			
			$("#cancellation_reason_"+reason).css("display","inline");
			$("#cancellation_reason_"+reason).removeAttr('disabled');
			if($(this).val() == 'CCC') {
/*				reasonsEle.append($("<option></option>").attr("value", 'OOSC').text('OOS Cancellation'));
				reasonsEle.append($("<option></option>").attr("value", 'OPIS').text('Online Payment Issue'));
				reasonsEle.append($("<option></option>").attr("value", 'NCNV').text('COD NC NV'));
				reasonsEle.append($("<option></option>").attr("value", 'CODNR').text('COD Verified NR'));
				reasonsEle.append($("<option></option>").attr("value", 'RTO').text('Returned To Origin')); */

				$("#send_goodwill_coupons").attr('disabled', false);
				$("#send_goodwill_coupons").attr('checked', false);
			} else {
			/*	reasonsEle.append($("<option></option>").attr("value", 'DDC').text('Delayed Delivery Cancellation'));
				reasonsEle.append($("<option></option>").attr("value", 'ISOC').text('Incorrect size ordered'));
				reasonsEle.append($("<option></option>").attr("value", 'DOC').text('Duplicate order cancelled'));
				reasonsEle.append($("<option></option>").attr("value", 'PNRA').text('Product not required anymore / Not interested'));
				reasonsEle.append($("<option></option>").attr("value", 'CIOC').text('Cash Issue- Cancel Order'));
				reasonsEle.append($("<option></option>").attr("value", 'OBM').text('Ordered by mistake'));
				reasonsEle.append($("<option></option>").attr("value", 'FO').text('Fraud Order'));
				reasonsEle.append($("<option></option>").attr("value", 'WTCS').text('Wants to change style/color')); */

				$("#send_goodwill_coupons").attr('disabled', true);
			}
		}); 

		$(".cancel_reason").change(function(){
			var reason = document.getElementById("cancellation_type").value;
			var value = $("#cancellation_type option:selected").text();
			value += "\n"+$("#cancellation_reason_"+reason+" option:selected").text()+"\n";
			$("textarea#cancellation_comment").val(value);
		});
	});
</script>
{/literal}

{capture name=dialog}

	{if $message}<font color='green' size='2'>{$message}</font><br/><br/>{/if}
	{if $errormessage}<font color='red' size='2'>{$errormessage}</font><br/><br/>{/if}
	<form action="" method="post" name="cancelordersform" enctype="multipart/form-data" onsubmit="return showUploadStatusWindow();">
		<table cellspacing=0 cellpadding=0 border=0 width=100%>
			<tr height=30>
				<td width=30%>Cancellation Type:</td>
				<td width=70%>
				<select name="cancellation_type" id="cancellation_type">
					{foreach from=$reject_reason_type key=reasonKey item=reasonValue}
						<option value="{$reasonKey}">{$reasonValue}</option>
					{/foreach}
				</select>
				</td>
			</tr>
			<tr height=30>
				<td width=30%>Cancellation Reason:</td>
				<td width=70%>
					{foreach from=$reject_reason_type key=reasonKey item=reasonValue}
		            	<select name="cancellation_reason" id="cancellation_reason_{$reasonKey}" class="cancel_reason">
							{foreach from=$reject_reasons.$reasonKey key=reasonKey item=reasonValue}
							<option value="{$reasonKey}">{$reasonValue}</option>
							{/foreach}
						</select>
            		{/foreach}
				</td>
			</tr>
			<tr height=30>
				<td width=30%>Cancellation Reason:</td>
				<td width=70%><textarea id="cancellation_comment" name="cancellation_comment" style="width: 300px; height: 150px;"></textarea></td>
			</tr>
			<tr height=30>
				<td width=30%>Upload the file :<br>(Excel file with OrderId, Coupon Amount, Min Purchase, Expiry Period in days, No of coupons)</td>
				<td width=70%><input type="file" name="orderdetailsfile" size=70></td>
			</tr>
			<tr>
				<td colspan=2 align=right>
					<input type=checkbox name="send_goodwill_coupons" id="send_goodwill_coupons" value="true"> Generate Goodwill coupons for cancellation
				</td>
			</tr>
			<tr height=30>
				<td width=30%></td>
				<td width=70% align=right><input type='submit' name='uploadxls' value="Upload"></td>
			</tr>
		</table>
		<input type='hidden' name='action' value='verify'>
		<input type='hidden' name='progresskey' value='{$progresskey}'>
		<input type='hidden' name='login' value='{$login}'>
	</form>
	
	<div id="uploadstatuspopup" style="display:none">
		<div class="overlay" style="display:block;_position:absolute;_width:1300px;_height:1500px;">&nbsp;</div>
		<div id="uploadstatuspopup-main" style="position:absolute; left: 20%; width:450px; background:white; z-index:1006;border:2px solid #999999;">
			<div id="uploadstatuspopup-content" style="display:none;">
			    <div id="uploadstatuspopup_title" class="popup_title">
			    	<div id="uploadstatuspopup_ajaxWindowTitle" class="popup_ajaxWindowTitle">Upload Status</div>
					<div class="clear">&nbsp;</div>
				</div>
				<div style="padding:5px 25px; text-align:left; font-size:12px" >
					<div id="progress_container"> 
					    <div id="progress_bar">
					    	<div id="progress_completed"></div> 
					    </div>
					</div>
					<div id="progress_text" style="font-size: 14px; color: green; padding: 0 0 0 5px;">0 % Complete</div>
				</div>
				<div class="clear">&nbsp;</div>
			</div>
		</div>
	</div>
{/capture}
{include file="dialog.tpl" title='Cancel Orders' content=$smarty.capture.dialog extra='width="100%"'}
{literal}
<script type="text/javascript">
	$(document).ready(function() {
		var reason = document.getElementById("cancellation_type").value;
		$(".cancel_reason").css("display","none");
		$("#cancellation_reason_"+reason).css("display","inline");
	});
</script>
{/literal}