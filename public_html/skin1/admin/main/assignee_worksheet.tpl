{* $Id: featured_products.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
{include file="main/include_js.tpl" src="main/popup_product.js"}

<link rel="stylesheet" href="{$SkinDir}/skin1_admin_operations.css" />
<script language="javascript" src="../skin1/js_script/item_assignment.js"></script> 

{capture name=dialog}

<form name="assignee_task" action="assignee_worksheet.php" method="post">
	<input type="hidden" name="mode" id="mode" value="search">
		<div class="ITEM_PARENT_DIV" style="width:650px;">
		<div class="ITEM_FIRST_CHILD_DIV" style="width:auto;">
        <div class="ITEM_LABLE_DIV">Assignee Name</div>
        <div class="ITEM_FIELD_DIV" style="width:auto;">
            <select name="item_assignee" style="width:250px;" onchange="javascript:document.assignee_task.submit();">
                    <option value="0">---select assignee---</option>
				{section name=ass loop=$assignee}
				    <option value="{$assignee[ass].id}" {if $smarty.post.item_assignee == $assignee[ass].id  || $smarty.get.item_assignee == $assignee[ass].id} selected="selected" {/if}>{$assignee[ass].name}</option>
				{/section}
			</select>
        </div> 
        </div>
        </div>
</form>
{/capture}
{include file="dialog.tpl" title="Select Assignee" content=$smarty.capture.dialog extra='width="100%"'}

<br/><br/>

{if $assigneeid != ""}
{capture name=dialog}
<div style="float:left;width:auto;height:auto;border : 0px solid #000000;">
{include file="customer/main/navigation.tpl"}
</div>


    <form action="" method="post" name="assignee_worksheets">
        <input type="hidden" name="mode"  />
        <input type="hidden" name="page" id="page" value="{$smarty.get.page}"  />
         <input type="hidden" name="query_string" id="query_string" value="{$query_string}"  />

        <input type="button" name="download_all" value="Download All Processing Sheets" onclick="javascript: window.open('{$xcart_dir}/orderprocess_sheet.php?assigneeid={$assigneeid}');">
         
        <div class="DIV_TABLE" style="width:1500px;">
        <div class="DIV_TH" style="width:1500px;"><div class="DIV_TD" style="width:900px;color:#FF0000;font-size:15px;">{$file_can_not_download}</div></div>
        <div class="DIV_TH" style="width:1500px;">
            <div class="DIV_TD" style="width:130px;"><a href="{$navigation_script}&amp;order_by=order_name{if $smarty.get.sort_order=='desc' && $smarty.get.order_by=='order_name'}&amp;sort_order=asc{else}&amp;sort_order=desc{/if}">Order Name{if $smarty.get.sort_order=='desc' && $smarty.get.order_by=='order_name'}&uArr;{elseif $smarty.get.sort_order=='asc' && $smarty.get.order_by=='order_name'}&dArr;{else}{/if}</a></div>
            <div class="DIV_TD" style="width:65px;"><a href="{$navigation_script}&amp;order_by=orderid{if $smarty.get.sort_order=='desc' && $smarty.get.order_by=='orderid'}&amp;sort_order=asc{else}&amp;sort_order=desc{/if}">order id{if $smarty.get.sort_order=='desc' && $smarty.get.order_by=='orderid'}&uArr;{elseif $smarty.get.sort_order=='asc' && $smarty.get.order_by=='orderid'}&dArr;{else}{/if}</a></div>
            
            <div class="DIV_TD" style="width:55px;">Item Id</div>
            <div class="DIV_TD" style="width:200px;"><a href="{$navigation_script}&amp;order_by=login{if $smarty.get.sort_order=='desc' && $smarty.get.order_by=='login'}&amp;sort_order=asc{else}&amp;sort_order=desc{/if}">Login{if $smarty.get.sort_order=='desc' && $smarty.get.order_by=='login'}&uArr;{elseif $smarty.get.sort_order=='asc' && $smarty.get.order_by=='login'}&dArr;{else}{/if}</a></div>
            <div class="DIV_TD" style="width:85px;"><a href="{$navigation_script}&amp;order_by=item_status{if $smarty.get.sort_order=='desc' && $smarty.get.order_by=='item_status'}&amp;sort_order=asc{else}&amp;sort_order=desc{/if}">Status{if $smarty.get.sort_order=='desc' && $smarty.get.order_by=='item_status'}&uArr;{elseif $smarty.get.sort_order=='asc' && $smarty.get.order_by=='item_status'}&dArr;{else}{/if}</a></div>
            <div class="DIV_TD" style="width:125px;"><a href="{$navigation_script}&amp;order_by=stylename{if $smarty.get.sort_order=='desc' && $smarty.get.order_by=='stylename'}&amp;sort_order=asc{else}&amp;sort_order=desc{/if}">Item Name{if $smarty.get.sort_order=='desc' && $smarty.get.order_by=='stylename'}&uArr;{elseif $smarty.get.sort_order=='asc' && $smarty.get.order_by=='stylename'}&dArr;{else}{/if}</a></div>
            
            <div class="DIV_TD" style="width:70px;">quantity</div>
            <div class="DIV_TD" style="width:100px;"><a href="{$navigation_script}&amp;order_by=location_name{if $smarty.get.sort_order=='desc' && $smarty.get.order_by=='location_name'}&amp;sort_order=asc{else}&amp;sort_order=desc{/if}">Location{if $smarty.get.sort_order=='desc' && $smarty.get.order_by=='location_name'}&uArr;{elseif $smarty.get.sort_order=='asc' && $smarty.get.order_by=='location_name'}&dArr;{else}{/if}</a></div>
            <div class="DIV_TD" style="width:190px;">assignment date</div>
            <div class="DIV_TD" style="width:150px;">Estimated completion</div>
            <div class="DIV_TD" style="width:75px;">priview image</div>
            <div class="DIV_TD" style="width:210px;">download processing sheet</div>
        </div>
        
        {if $total_items > 0 || $mode == "dow"}
        {assign var=oldorderid value="-1"}
        {assign var=i value=0}
        {section name=item loop=$itemResult }
         <div class="DIV_TR" style="width:1500px;{if $itemResult[item].ccode neq ""}background-color:{$itemResult[item].ccode};{/if}" >
            
            
                <div class="DIV_TR_TD" style="width:130px;">
	{if $itemResult[item].is_customizable eq '0'}<span style="background-color:red;font-weight:bold;padding-left:4px;padding-right:4px;">NP</span>{/if}	
                    {if $itemResult[item].orderid != $oldorderid}
                        <a href="javascript:void(0);" onclick="javascript:window.open('order.php?orderid={$itemResult[item].orderid}');" onmouseover="javascript:setBorder('17');writetxt('These HDF coasters come in sets of three and are of the size 3&quot;X3&quot;. They are printable on one side with the same print for all the three coasters. ');" onmouseout="javascript:resetBorder('17');writetxt(0);"><p>{$itemResult[item].order_name}</p></a>
                    {else}
                        <a href="javascript:void(0);" style="text-decoration:none" onclick="javascript:window.open('order.php?orderid={$itemResult[item].orderid}');"><p><font style="text-transform: uppercase;">L</font></p></a>
                    {/if}
                </div>
                <div class="DIV_TR_TD" style="width:65px;">
                    {if $itemResult[item].orderid != $oldorderid}
                        <input type="hidden" name="orderids[]" value="{$itemResult[item].orderid}">
                        <a href="javascript:void(0);" onclick="javascript:window.open('order.php?orderid={$itemResult[item].orderid}');" onmouseover="javascript:setBorder('17');writetxt('These HDF coasters come in sets of three and are of the size 3&quot;X3&quot;. They are printable on one side with the same print for all the three coasters. ');" onmouseout="javascript:resetBorder('17');writetxt(0);"><p>{$itemResult[item].orderid}</p></a>
                        {assign var=i value=$i+1}
                    {else}
                        <a href="javascript:void(0);" style="text-decoration:none" onclick="javascript:window.open('order.php?orderid={$itemResult[item].orderid}');"><p><font style="text-transform: uppercase;">L</font></p></a>
                    {/if}
                </div>
            <div class="DIV_TR_TD" style="width:55px;"><p>{if $itemResult[item].failreason != ""}<a href=""  title="{$itemResult[item].failreason}" onclick="return false">?#</a>{/if}{if $itemResult[item].QAFailure == "true"}<FONT color="#800080"> <B>{/if} {$itemResult[item].itemid}{if $itemResult[item].QAFailure == "true"}</FONT> </B>{/if}</p></div>
            <div class="DIV_TR_TD" style="width:200px;">{if $itemResult[item].login}{$itemResult[item].login}{/if}</div>
            <div class="DIV_TR_TD" style="width:85px;"><p>
            {assign var=i_status value=$itemResult[item].item_status}
            {if $i_status == 'UA'} {assign var=status_color value="#EE0F14"} {/if}
            {if $i_status == 'A'} {assign var=status_color value="#77AB3D"} {/if}
            {if $i_status == 'D'} {assign var=status_color value="#80FFFF"} {/if}
            {if $i_status == 'F'} {assign var=status_color value="#EF940A"} {/if}
            {if $i_status == 'H'} {assign var=status_color value="#F5FF42"} {/if}
            {if $i_status == 'S'} {assign var=status_color value="#04FF82"} {/if}
            <select name="item_status" id="item_st_{$itemResult[item].itemid}" onchange="javascript:itemDoneAssignee('status','{$itemResult[item].itemid}','{$itemResult[item].item_status}');" 
            {if $i_status != 'A' && $i_status != 'S'} disabled="disabled" {/if} style="background-color:{$status_color};width:80px;">
                 {foreach from=$status item=st_value key=st_key}
                    <option value="{$st_key}" {if $i_status == $st_key} selected {/if} style="color:#000000;background-color:{$st_value.color};">{$st_value.status}</option>
                {/foreach}
			</select></p>
            </div>
           <input type="hidden" name="orderid_{$itemResult[item].itemid}" id="orderid_{$itemResult[item].itemid}" value="{$itemResult[item].orderid}">
            <div class="DIV_TR_TD" style="width:125px;"><p>{$itemResult[item].stylename}</p></div>
            <div class="DIV_TR_TD" style="width:70px;"><p>{$itemResult[item].amount}</p>
                <input type="hidden" name="qty_{$itemResult[item].itemid}" id="qty_{$itemResult[item].itemid}" value="{$itemResult[item].amount}">
            </div>
	    	<div class="DIV_TR_TD" style="width:100px;">{if $itemResult[item].location_name}{$itemResult[item].location_name}{else}Not Assigned{/if}</div>
            <div class="DIV_TR_TD" style="width:190px;">{$itemResult[item].ass_date}</div>
            <div class="DIV_TR_TD" style="width:150px;">{if $itemResult[item].estimated_date}{$itemResult[item].estimated_date}{/if}</div>
            <div class="DIV_TR_TD" style="width:75px;">
		{if $itemResult[item].is_customizable eq '0'}
		<a href="#" onclick="window.open('imagePreview.php?style_id={$itemResult[item].product_style}', null, 'height=500,width=700,status=yes,toolbar=no,menubar=no,location=no,resizable=1,scrollbars=1');">Preview Image</a>
		{else}
		<a href="javascript:void(0);" onclick="javascript:window.open('view_print_design.php?productId={$itemResult[item].productid}&orderid={$itemResult[item].orderid}');"><p>preview image</p></a>
		{/if}

		</div>
            <div class="DIV_TR_TD" style="width:210px;">
                {if $itemResult[item].orderid != $oldorderid}
                    <input type="button" name="item_assignment_done" value="Download Processing Sheet" onclick="javascript: window.open('{$http_location}/orderprocess_sheet.php?orderid={$itemResult[item].orderid}');">
                {else}
                     L
                {/if}
            </div>
            {assign var=oldorderid value=$itemResult[item].orderid}
         </div>
         
        {/section}
        <div class="ITEM_SEARCH_BUTTON_DIV" style="width:1500px;">
            <input type="button" name="download_csv" value="Download CSV" onclick="javascript: window.open('assignee_worksheet.php?mode=download_csv&item_assignee={$assigneeid}&order_by={$smarty.get.order_by}&sort_order={$smarty.get.sort_order}');">
            <input type="button" name="download_csv" value="Download XLS" onclick="javascript: window.open('assignee_worksheet.php?mode=download_xls&item_assignee={$assigneeid}&order_by={$smarty.get.order_by}&sort_order={$smarty.get.sort_order}');">
            <input type="button" value="print task list" style="margin-left : 300px;"onclick="javascript:window.open ('printAssigneeTaskList.php?assigneeid={$assigneeid}&order_by={$smarty.get.order_by}&sort_order={$smarty.get.sort_order}', 'corporatequote', 'width=870,height=500,toolbar=no,status=no,scrollbars=yes,resizable=yes,menubar=no,location=no,direction=no');" />
        </div>
        {else}
        <div class="DIV_TR" style="width:1500px;">
            <div class="DIV_TR_TD" style="width:700px;"><strong>task has not been assigned</strong></div>
        </div>
        {/if}
        
        </div>
    </form>
    
 <div style="float:left;width:auto;height:auto;border : 0px solid #000000;">   
{include file="customer/main/navigation.tpl"}
</div>
{/capture}
{include file="dialog.tpl" title="Assigned task list" content=$smarty.capture.dialog extra='width="100%"'}
{/if}
