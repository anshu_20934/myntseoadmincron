{* $Id: featured_products.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
{include file="main/include_js.tpl" src="main/popup_product.js"}
{literal}
  <script language="Javascript">
      function confirmDelete()
      {
          var flag = confirm('Are you sure you want to delete?')
          if(!flag)
          {
             return false;
          }
          else
          {
          	 document.groupfrm.mode.value = 'delete';
          	 document.groupfrm.submit();
          	 return true;
          }
      }
	function addPriority()
	{
		var w = document.frmG.name.selectedIndex;
		var selected_text = document.frmG.name.options[w].text;
		 document.frmG.label.value = selected_text;
		document.frmG.submit();
	}
  </script>
{/literal}
<a name="featured" />

{$lng.txt_featured_products}

<br /><br />

{capture name=dialog}

<form action="{$form_action}" method="post" name="groupfrm" enctype="multipart/form-data">
<input type="hidden" name="mode"  />

<table cellpadding="3" cellspacing="1" width="100%">

<tr class="TableHead">
	<td width="20%">Name</td>
	<td width="20%">Weightage</td>
	<td width="20%" align="center">Cutoff days</td>

</tr>

{if $transients}

{section name=grid loop=$transients}

<tr{cycle values=", class='TableSubHead'"}>
    <input type="hidden" name="posted_data[{$transients[grid].action}][gid]" value={$products[grid].action} />
	<td align="center">{$transients[grid].action}</td>
    <td align="center"><input type="text" name="posted_data[{$transients[grid].action}][weightage]" value="{$transients[grid].weightage}"  size="2" /></td>
    <td align="center"><input type="text" name="posted_data[{$transients[grid].action}][cutoff_days]" value="{$transients[grid].cutoff_days}"  size="2" /></td>
</tr>

{/section}
<td colspan="5"><br /><br /></td>
{else}

<tr>
 <td colspan="4" align="center">No Priority available.</td>
</tr>
{/if}

<!--<tr>
<td colspan="5"><br /><br />{include file="main/subheader.tpl" title=$lng.lbl_add_product}</td>
</tr> -->

<tr>
	<td colspan="5" class="SubmitBox">
	<input type="submit" value="{$lng.lbl_update|escape}" onclick="javascript: document.groupfrm.mode.value = 'update'; document.groupfrm.submit();" />

	</td>
</tr>


</table>
</form>
{/capture}
{include file="dialog.tpl" title="Solr Fields Priority List" content=$smarty.capture.dialog extra='width="100%"'}
<br /><br />


