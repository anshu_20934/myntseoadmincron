var today = new Date();
var today_date_str = (today.getMonth()+1)+"/"+today.getDate()+"/"+today.getFullYear();

var orderStatusArr = [["Q", "Queued"], ["WP", "WIP"], ["OH", "On Hold"]];

Ext.onReady(function() {
	Ext.QuickTips.init();
	var myMask = new Ext.LoadMask(Ext.getBody(), {msg: "Loading..."});
	Ext.Ajax.on('beforerequest', myMask.show, myMask);
	Ext.Ajax.on('requestcomplete', myMask.hide, myMask);
	Ext.Ajax.on('requestexception', myMask.hide, myMask);
	
	var searchPanel = new Ext.form.FormPanel({
		renderTo : 'contentPanel',
		title : 'Search OOS Orders',
		id : 'formPanel',
		autoHeight : true,
		collapsible : true,
		border : true,
		bodyStyle : 'padding: 5px',
		width : 1000,
		items : [{
			layout : 'table',
			border : false,
			layoutConfig : {columns : 2},
			defaults : {width : 450, border : false, 
				layout : 'form', bodyStyle : 'padding-left : 15px'},
			items : [
			         {
			        	 items : [{
			        		 xtype : 'datefield',
			        		 name : 'from_date',
			        		 width : 150,
			        		 fieldLabel : "From Date",
			        		 value: today_date_str,
			        		 maxValue : new Date()
			        	 }]
			         },{
			        	 items : [{
			        		 xtype : 'datefield',
			        		 name : 'to_date',
			        		 width : 150,
			        		 fieldLabel : "To Date",
			        		 value: today_date_str,
			        		 maxValue : new Date()
			        	 }]
			         }
			]
		}],
		buttons : [ {
			text : 'Search',
			handler : function() {
				reloadGrid();
			}
		}, {
			text : 'Download Report',
			handler : function() {
				var params = getSearchPanelParams();
				if (params != false) {
					params["action"] = "download_report";
					Ext.Ajax.request({
						url : 'oosordersreport.php',
						params : params,
					    success : function(response, action) {
							var jsonObj = Ext.util.JSON.decode(response.responseText);
							window.open(httpLocation + "/bulkorderupdates/" + jsonObj);
						}
					});
				}
			}
		}],
	});
	
	function getSearchPanelParams() {
		var form = searchPanel.getForm();
		
		if (form.isValid()) {
			if(form.findField("from_date").getRawValue() == "" || 
					form.findField("to_date").getRawValue() == "" ) {
				Ext.MessageBox.alert('Error', 'Date range can not be empty');
				return false;
			}
			
			var params = {};
			params["from_date"] = form.findField("from_date").getRawValue();
			params["to_date"] = form.findField("to_date").getRawValue();
			return params;
		} else {
			return false;
		}
	}
	
	function reloadGrid() {
		
		var params = getSearchPanelParams();
		
		if (params == false) {
			Ext.MessageBox.alert('Error', 'Errors in the search form');
			return false;
		}
		params["action"] = "search";
		
		searchResultsStore.baseParams = params;
		searchResultsStore.load();
	}
	
	var orderNameLinkRender = function(value, p, record) {
		var retLink = '<a href="' + httpLocation + '/admin/order.php?orderid=' + record.data["orderid"] + '" target="_blank">' + record.data["order_name"] + '</a>';
		if (record.data['customizable'] == '0') {
			retLink = retLink + "<span style='color:red'> (NP) <span>";
		}
		return retLink;
	}
	
	var orderStatusRenderer = function(value, p, record) {
		var status = record.data['status'];
		for (var i = 0; i < orderStatusArr.length; i++) {
			if (orderStatusArr[i][0] == status) {
				return orderStatusArr[i][1];
			}
		}
		return "NOT SPECIFIED";
	}

	var colModel = new Ext.grid.ColumnModel({
		defaults: {
	        sortable: true
	    },
	    columns: [
	        new Ext.grid.RowNumberer(),
	        {id : 'order_name', header : "Order Name", sortable: true, dataIndex : 'order_name', renderer : orderNameLinkRender},
	        {id : 'payment_method', header : "Payment Method", sortable: false, dataIndex : 'payment_method'},
	        {id : 'orderid', header : "Order ID", sortable: true, dataIndex : 'orderid', sortable: false},
	        {id : 'qtyInOrder', header : "Total Items In Order", sortable: true, dataIndex : 'qtyInOrder', sortable: false},
	        {id : 'itemid', header : "Item ID", sortable: true, dataIndex : 'itemid'},
	        {id : 'amount_paid', header : "Amount Paid", sortable: false, dataIndex : 'amount_paid'},
	        {id : 'price', header : "Item Price", sortable: false, dataIndex : 'price'},
	        {id : 'qty', header : "Item Qty", sortable: false, dataIndex : 'qty'},
	        {id : 'discount', header : "Discount", sortable: false, dataIndex : 'discount'},
	        {id : 'coupon_discount_product', header : "Coupon Discount", sortable: false, dataIndex : 'coupon_discount_product'},
	        {id : 'cash_redeemed', header : "Cash Redeemed", sortable: false, dataIndex : 'cash_redeemed'},
	        {id : 'pg_discount', header : "PG Discount", sortable: false, dataIndex : 'pg_discount'},
	        {id : 'login', header : "Customer Login", sortable: true, dataIndex : 'login'},
	        {id : 'code', header : "Sku Code", sortable: false, dataIndex: 'code'},
	        {id : 'status', header : "Status", sortable: false, dataIndex: 'status', renderer : orderStatusRenderer},
	        {id : 'date', header : "Order Date", sortable: true, dataIndex : 'date'},
	        {id : 'queueddate', header : "Queued Date", sortable: true, dataIndex : 'queueddate'}
  	  	]
	});
		
	var searchResultsStore = new Ext.data.Store({
		url: 'oosordersreport.php',
		pruneModifiedRecords : true,
		reader: new Ext.data.JsonReader({
	  		root: 'results',
	  	  	totalProperty: 'count',
	  	  	fields : ['order_name','payment_method','orderid','itemid','login','status','date','queueddate','code',
	  	  	          'qtyInOrder', 'price', 'qty', 'discount', 'coupon_discount_product', 'cash_redeemed', 'pg_discount', 'amount_paid']
		}),
	});

	var pagingBar = new Ext.PagingToolbar({
		pageSize : 30,
	    store: searchResultsStore,
	    displayInfo: true
	});
	
	var searchResults = new Ext.grid.EditorGridPanel({
	    store  : searchResultsStore,
	    cm : colModel,
	    id : 'searchResults-panel',
	    title : 'Search Results',
	    frame : true,
		loadMask : false,
		autoHeight : true,
		bbar : pagingBar,
		stripeRows : true,
		autoScroll : true,
		viewConfig : {
			enableRowBody : true
		}
	});
	
	var mainPanel = new Ext.Panel({
		renderTo : 'contentPanel',
		border : false,
		items : [
		     searchPanel, searchResults
		],
		listeners : {
			'afterrender' : function(comp) {
				reloadGrid();
			}
		}
	});
});