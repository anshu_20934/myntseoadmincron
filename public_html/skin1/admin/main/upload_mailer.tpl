{* $Id: featured_products.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
<script type="text/javascript" src="{$http_location}/modules/uploadify/jquery.uploadify.v2.1.4.min.js"></script>
<script type="text/javascript" src="{$http_location}/modules/uploadify/swfobject.js"></script>
<link href="{$http_location}/modules/uploadify/uploadify.css" rel="stylesheet" type="text/css">

<a name="featured" />
{$lng.txt_featured_products}
<br /><br />

{capture name=dialog}
    <form name='uplMailer' method='POST'></form>

    <div><b>10 files</b> can max be uploaded at a time, and each file size should not exceed <b>200KB</b></div>
    <br/>

    <script type="text/javascript">
    {literal}
    $(document).ready(function() {
        $('#file_upload').uploadify({
            'uploader'      : '../modules/uploadify/uploadify.swf',
            'script'        : '../modules/uploadify/uploadify.php',//script which uploads files
            'cancelImg'     : '../modules/uploadify/cancel.png',//cancel button imahe path
            'folder'        : '{/literal}{$upload_location}{literal}',//to set in uploadify.php and check.php to secure -- to store uploaded files
            'auto'          : false,//uploads automatically
            'multi'         : true,//multi-upload
            'expressInstall': '../modules/uploadify/expressInstall.swf',//tells to install flash if not installed
            'checkScript'   : null,// '../modules/uploadify/check.php' --- ,//checks already uploaded file on name
            'buttonText'    : 'Select Images',
            'height'        : 30,//height of button
            'width'         : 120,//width of button
            'queueSizeLimit': 10,//max number files can be uploaded
            'removeCompleted' : false,//removes files from the progress lists once uploaded
            'sizeLimit'     : 204800,//each file size can be at max 200KB

            'onSelectOnce'  : function(event,data) {
                                $('#upload_status').text(data.fileCount + ' files to upload').css({color:"#000000",fontWeight:'bold'});
                              },

            'onAllComplete' : function(event,data) {
                                $('#upload_status').text(data.filesUploaded + ' files uploaded').css({color:"green",fontWeight:'bold'});

                                if(data.errors){
                                    $('#upload_error_status').text(data.errors + ' files are not uploaded').css({color:"red",fontWeight:'bold'});
                                }
                              },

            'onClearQueue'  : function(event,data) {
                                $('#upload_status').text('');
                                $('#upload_error_status').text('');
                              },

            'onCancel'      : function(event,ID,fileObj,data) {
                                $('#upload_status').text(data.fileCount + ' files to upload').css({color:"#000000",fontWeight:'bold'});
                              }

        });
    });
    {/literal}
    </script>

    <input id="file_upload" name="file_upload" type="file" />
    <br/>
    <div id="upload_status"></div><div id="upload_error_status"></div><div id="s3load" style="display:none"><img src="{$cdn_base}/skin1/myntra_images/loading_240_320.gif" alt="uploading..."></div>
    <br/>
    <a href="javascript:$('#file_upload').uploadifyUpload();">Upload Images</a>  |  <a href="javascript:$('#file_upload').uploadifyClearQueue();">Cancel All Upload</a>  |  <a href="javascript:$('#s3load').show();$('form[name=&quot;uplMailer&quot;]').submit();">Move All Images to S3</a>
{/capture}

{capture name=S3list}
    <div style="margin-top:10px;">
        {$html}
    </div>
{/capture}

{include file="dialog.tpl" title='Mailer management' content=$smarty.capture.dialog extra='width="100%"'}
<br/>
{include file="dialog.tpl" title='Mailer Images in S3' content=$smarty.capture.S3list extra='width="100%"'}