Ext.onReady(function() {
	Ext.QuickTips.init();
	var myMask = new Ext.LoadMask(Ext.getBody(), {msg: "Loading..."});
	
	Ext.Ajax.on('requestcomplete', myMask.hide, myMask);
	Ext.Ajax.on('requestexception', myMask.hide, myMask);
	
	var downloadMask;
	var timerid;
	var trialStore = new Ext.data.JsonStore({
        fields : [ 'trialNo', 'display_name' ],
        data : trialArr,
        sortInfo : {
                field : 'trialNo',
                direction : 'ASC'
        }
	});
	
	var trials = new Ext.ux.form.SuperBoxSelect({
	        xtype : 'superboxselect',
	        fieldLabel : 'Number of Trials',
	        emptyText : 'Select Number of Trials',
	        name : 'trial_number',
			id : 'trial_numbers',
	        anchor : '100%',
	        store : trialStore,
	        mode : 'local',
	        displayField : 'display_name',
	        valueField : 'trialNo',
	        forceSelection : true
	});

	var searchPanel = new Ext.form.FormPanel({
		renderTo : 'contentPanel',
		title : 'Search Panel',
		id : 'formPanel',
		autoHeight : true,
		collapsible : true,
		border : true,
		bodyStyle : 'padding: 5px',
		width : 1000,
		items : [{
			layout : 'table',
			border : false,
			layoutConfig : {columns : 2},
			defaults : {width : 450, border : false, 
				layout : 'form', bodyStyle : 'padding-left : 15px'},
			items : [{
				items : [{
					xtype : 'numberfield',
		        	id : 'order_id',
		        	fieldLabel : 'Order Id'
				}]
			}, {
				items :[{
					xtype : 'textfield',
		        	id : 'login',
		        	fieldLabel : 'Login',
		        	width : 200
				}]
			}, {
				items : [trials]
			},{
				items : []
			},
			{
				items : [{
					xtype : 'datefield',
	                name : 'from_date',
	                width : 150,
	                fieldLabel : "From Date",
	                "format": "d/m/Y H:i:s"
				}]
			}, {
				items : [{
					xtype : 'datefield',
					name : 'to_date',
					width : 150,
					fieldLabel : "To Date",
					"format": "d/m/Y H:i:s"
				}]
			}]
		}],
		buttons : [ {
			text : 'Download',
			handler : function() {
				downloadSearchResults();
			}
		},{
			text : 'Minacs Download',
			handler : function() {
				downloadMinacsResults();
			}
		},{
			text : 'Search',
			handler : function() {
				reloadGrid();
			}
		}]
	});
	
	
	
	function getSearchPanelParams() {
		var form = searchPanel.getForm();
		
		if (form.isValid()) {
			var params = {};
			params["order_id"] = form.findField("order_id").getValue();
			params["login"] = form.findField("login").getValue();
			params["from_date"] = form.findField("from_date").getRawValue();
			params["to_date"] = form.findField("to_date").getRawValue();
			params["trial_number"] = form.findField("trial_number").getValue();
			
			return params;
		} else {
			return false;
		}
	}
	
	function reloadGrid() {
		
		var params = getSearchPanelParams();
		
		if (params == false) {
			Ext.MessageBox.alert('Error', 'Errors in the search form');
			return false;
		}
		params["action"] = "search";
		
		searchResultsStore.baseParams = params;
		myMask.show();
		searchResultsStore.load();
	}
	
	function downloadSearchResults(){
		var params = getSearchPanelParams();
		if (params == false) {
			Ext.MessageBox.alert('Error', 'Errors in the search form');
			return false;
		}
		params["action"] = "downloadreport";
		params["progresskey"] = progresskey;
		downloadMask = new Ext.LoadMask(Ext.get('formPanel'), {msg: "Generating File"});
		downloadMask.show();
		Ext.Ajax.request({
			url : 'codohordersajax.php',
			params : params,
		    success : function(response, action) {
		    	//downloadMask.show();
			}
		});
		timerid = setInterval(trackStatus, 3000);
	}
	
	function downloadMinacsResults() {
		var params = getSearchPanelParams();
		if (params == false) {
			Ext.MessageBox.alert('Error', 'Errors in the search form');
			return false;
		}
		params["action"] = "downloadminacsreport";
		params["progresskey"] = progresskey;
		downloadMask = new Ext.LoadMask(Ext.get('formPanel'), {msg: "Generating File"});
		downloadMask.show();
		Ext.Ajax.request({
			url : 'codohordersajax.php',
			params : params,
		    success : function(response, action) {
		    	//downloadMask.show();
			}
		});
		timerid = setInterval(trackStatus, 3000);
	}
	
	function trackStatus() {
		Ext.Ajax.request({
			url : "/admin/getprogressinfo.php?type=status&progresskey="+progresskey,
			method : 'GET',
			mask: false,
		    success : function(response, action) {
		    	var jsonObj = Ext.util.JSON.decode(response.responseText);
		    	if(jsonObj) {
			    	if(jsonObj.status == 'DONE' && jsonObj.success == true){
			    		clearInterval(timerid);
			    		downloadMask.hide();
			    		var html = "Click <a href = '"+httpLocation + jsonObj.file_location+"' target='_blank'> here </a> to download file";
			    		Ext.Msg.show({
			    		    title:      "Download Link",
			    		    msg:        html,
			    		    icon:       Ext.MessageBox.INFO
			    		});
			    	} else {
			    		clearInterval(timerid);
			    		downloadMask.hide();
			    		alert(jsonObj.msg);
			    	}
		    	}
			}
		});
	}
	
	var sm = new Ext.grid.CheckboxSelectionModel();
	
	var requestIdLinkRender = function(value, p, record) {
		return '<a href="' + httpLocation + '/admin/order.php?orderid=' + record.data["orderid"] + '" target="_blank">' + record.data["orderid"] + '</a>';
	}
	
	
	var colModel = new Ext.grid.ColumnModel({
		defaults: {
	        sortable: true
	    },
	    columns: [
	        sm,
	        {id : 'orderid', header : "Order ID", sortable: true, width: 75, dataIndex : 'orderid',renderer : requestIdLinkRender},
	        {id : 'login', header : "Customer Login", sortable: true, width: 150, dataIndex : 'login'},
	        {id : 'name', header : "Customer Name", sortable: true, width: 150, dataIndex : 'name'},
	        {id : 'orderdate', header : "Order date", sortable: false, width: 150, dataIndex : 'orderdate'},
	        {id : 'total', header : "Order total", sortable: false, width: 120, dataIndex : 'total'},
	        {id : 'reason', header : "Reason for OH", sortable: false, width: 300, dataIndex : 'reason'},
	        {id : 'address', header : "Address", sortable: false, width: 400, dataIndex : 'address'}
	    ]
	});
		
	var searchResultsStore = new Ext.data.Store({
		url: 'codohordersajax.php',
		pruneModifiedRecords : true,
		reader: new Ext.data.JsonReader({
	  		root: 'results',
	  	  	totalProperty: 'count',
	  	  	fields : ['orderid', 'login', 'name', 'address', 'orderdate', 'total', 'reason']
		}),
		sortInfo:{field : 'orderid', direction:'ASC'},
		remoteSort: true
	});

	var pagingBar = new Ext.PagingToolbar({
		pageSize : 30,
	    store: searchResultsStore,
	    displayInfo: true
	});
	
	searchResults = new Ext.grid.EditorGridPanel({
	    store  : searchResultsStore,
	    cm : colModel,
	    id : 'searchResults-panel',
	    title : 'Search Results',
	    frame : true,
		loadMask : false,
		autoHeight : true,
		sm: sm,
		bbar : pagingBar,
		stripeRows : true,
		autoScroll : true,
		viewConfig : {
			enableRowBody : true
		},
		tbar:[]
	});
	
	var mainPanel = new Ext.Panel({
		renderTo : 'contentPanel',
		border : false,
		items : [
		     searchPanel, searchResults
		]
	});
});
