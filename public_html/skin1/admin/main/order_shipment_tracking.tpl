<div style="padding: 20px;">
	{if $shipment_tracking_details}
		<div style="font-size:12px; padding: 5px;">Shipment Tracking Details:</div>
		<table cellspacing=0 cellpadding=0 border=0 width=100% style="border-collapse: collapse;">
			<tr class="tablehead">
	            <th width="5%" nowrap="nowrap">Scan Date</th>
	            <th width="5%" nowrap="nowrap">Scanned Location</th>
	            <th width="15%" nowrap="nowrap">Remarks</th>
	        </tr>
			{section name=idx1 loop=$shipment_tracking_details} 
				<tr{cycle values=", class='TableSubHead'"}> 
					<td align='center'>{$shipment_tracking_details[idx1].scanDate|date_format:"%d %B, %Y %I:%M %p"}</td>
					<td align='center'>{$shipment_tracking_details[idx1].scannedlocation}</td>
					<td align='center'>{$shipment_tracking_details[idx1].remarks}</td>
				</tr> 
			{/section} 
		</table>
	{else}
	   No Details Available
	{/if}
</div>
