Ext.onReady(function() {
	Ext.QuickTips.init();
	var myMask = new Ext.LoadMask(Ext.getBody(), {msg: "Loading..."});
	Ext.Ajax.on('beforerequest', myMask.show, myMask);
	Ext.Ajax.on('requestcomplete', myMask.hide, myMask);
	Ext.Ajax.on('requestexception', myMask.hide, myMask);
	
	var searchPanel = new Ext.form.FormPanel({
		renderTo : 'contentPanel',
		title : 'Tier Info Panel',
		id : 'formPanel',
		autoHeight : true,
		collapsible : true,
		border : true,
		bodyStyle : 'padding: 5px',
		width : 1000,
		items : [{
			layout : 'table',
			border : false,
			layoutConfig : {columns : 2},
			defaults : {width : 450, border : false, 
				layout : 'form', bodyStyle : 'padding-left : 15px'},
			items : [
			         {
			        	 items : [{
			        		 width: 300,
			        		 xtype : 'textfield',
			        		 id : 'userlogin',
			        		 fieldLabel : 'Customer Login'
			        	 }]
			         }
			]
		}],
		buttons : [ {
			text : 'Get Tier Info',
			handler : function() {
				reloadGrid();
			}
		},{
			text : 'ReCompute Tier Info',
			handler : function() {
				reCompute();
			}
		}],
	});

	function getSearchPanelParams() {
		var form = searchPanel.getForm();
		
		if (form.isValid()) {
			var params = {};
			//params["coupon"] = form.findField("coupon").getValue();
			params["userlogin"] = form.findField("userlogin").getValue();
			return params;
		} else {
			return false;
		}
	}
	
	function reloadGrid() {
		
		var params = getSearchPanelParams();
		
		if (params == false) {
			Ext.MessageBox.alert('Error', 'Errors in the search form');
			return false;
		}
		params["action"] = "getTierLogs";
		
		searchResultsStore.baseParams = params;
		searchResultsStore.load();
	}

	function reCompute() {
		
		var params = getSearchPanelParams();
		
		if (params == false) {
			Ext.MessageBox.alert('Error', 'Errors in the search form');
			return false;
		}
		params["action"] = "computeTierInfo";

		
		searchResultsStore.baseParams = params;
		searchResultsStore.load();
	}

	var colModel = new Ext.grid.ColumnModel({
		defaults: {
	        sortable: true,
	        editable:true,
	        editor : Ext.form.Field
	    },
	    columns: [
	        new Ext.grid.RowNumberer(),
	        {id : 'modifiedOn', header : "Date", sortable: false, width: 125, dataIndex : 'modifiedOn'},	        
	        {id : 'login', header : "Login", sortable: false, width: 150, dataIndex : 'login'},
	        {id : 'tier', header : "Tier", sortable: false, width: 100, dataIndex : 'tier'},
	        {id : 'returnPeriodOrderActivity', header : "Return Period Activity", sortable: false, width: 100, dataIndex : 'returnPeriodOrderActivity'},	        
	        {id : 'sixMonthOrderActivity', header : "Six Month Order Activity", sortable: false, width: 100, dataIndex : 'sixMonthOrderActivity'},
	        {id : 'twelveMonthOrderActivity', header : "Twelve Month Order Activity", sortable: false, width: 125, dataIndex : 'twelveMonthOrderActivity'},
	        {id : 'validUpto', header : "Valid upto", sortable: false, width: 125, dataIndex : 'validUpto'},
	        {id : 'tNcAcceptedOn', header : "T&C Accepted On", sortable: false, width: 300, dataIndex : 'tNcAcceptedOn'}
  	  	]
	});
		
	var searchResultsStore = new Ext.data.Store({
		url: 'LoyaltyPointsTier.php',
		pruneModifiedRecords : true,
		reader: new Ext.data.JsonReader({
	  		root: 'results',
	  	  	totalProperty: 'count',
	  	  	fields : ['modifiedOn', 'login','tier', 'returnPeriodOrderActivity','sixMonthOrderActivity','twelveMonthOrderActivity', 'validUpto','tNcAcceptedOn']
		}),
	});

	var pagingBar = new Ext.PagingToolbar({
		pageSize : 30,
	    store: searchResultsStore,
	    displayInfo: true
	});
	
	var searchResults = new Ext.grid.EditorGridPanel({
	    store  : searchResultsStore,
	    cm : colModel,
		id : 'searchResults-panel',
	    title : 'Tier Info Results',
	    frame : true,
		loadMask : false,
		autoHeight : true,
		bbar : pagingBar,
		stripeRows : true,
		autoScroll : true,
		viewConfig : {
			enableRowBody : true
		}
	});	
	var mainPanel = new Ext.Panel({
		renderTo : 'contentPanel',
		border : false,
		items : [
		     searchPanel, searchResults
		],
		listeners : {
			'afterrender' : function(comp) {
				if(get_login && get_login != "") {
					searchPanel.getForm().findField("userlogin").setValue(get_login);
				}
				/*if(get_coupon && get_coupon != "") {
					searchPanel.getForm().findField("coupon").setValue(get_coupon);
				}*/
				reloadGrid();
			}
		}
	});
});
