<script type="text/javascript">
    var assigneeData = {$assignee};
    var httpLocation = '{$http_location}';
</script>

<link rel="stylesheet" type="text/css" href="{$http_location}/admin/extjs/resources/css/ext-all.css"/>
<link rel="stylesheet" type="text/css" href="{$http_location}/admin/extjs/plugins/itemselector/MultiSelect.css"/>

<script type="text/javascript" src="{$http_location}/admin/extjs/adapter/ext/ext-base.js"></script>
<script type="text/javascript" src="{$http_location}/admin/extjs/ext-all.js"></script>

<script type="text/javascript" src="{$http_location}/admin/extjs/plugins/itemselector/MultiSelect.js"></script>
<script type="text/javascript" src="{$http_location}/admin/extjs/plugins/itemselector/ItemSelector.js"></script>

<script type="text/javascript" src="editors.js"></script>

<div id="contentPanel"></div>
