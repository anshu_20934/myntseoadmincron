{* $Id: featured_products.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
{include file="main/include_js.tpl" src="main/popup_product.js"}

<a name="featured" />

{$lng.txt_featured_products}

<br /><br />

{capture name=dialog}

{*{if $products ne ""}
{include file="main/check_all_row.tpl" style="line-height: 170%;" form="featuredproductsform" prefix="posted_data.+to_delete"}
{/if}*}

<form action="categories.php" method="post" name="featuredproductsform">
<input type="hidden" name="mode" value="update" />
<input type="hidden" name="cat" value="{$f_cat}" />

<table cellpadding="3" cellspacing="1" width="100%">

<tr class="TableHead">
	<td width="10">&nbsp;</td>
	<td width="20%">{$lng.lbl_image_name}</td>
	<td width="40%">{$lng.lbl_event_name}</td>
	<td width="10%">{$lng.lbl_date_name}</td>
	<td width="15%" align="center">{$lng.lbl_pos}</td>
	<td width="15%" align="center">{$lng.lbl_active}</td>
</tr>

{if $products}

{section name=prod_num loop=$products}

<tr{cycle values=", class='TableSubHead'"}>
	<td><input type="checkbox" name="posted_data[{$products[prod_num].productid}][to_delete]" /></td>
	<td><b><a href="product.php?productid={$products[prod_num].productid}" target="_blank">{$products[prod_num].product}</a></b></td>
	<td><b></b></td>
	<td><b></b></td>
	<td align="center"><input type="text" name="posted_data[{$products[prod_num].productid}][product_order]" size="5" value="{$products[prod_num].product_order}" /></td>
	{*<td align="center"><input type="checkbox" name="posted_data[{$products[prod_num].productid}][avail]"{if $products[prod_num].avail eq "Y"} checked="checked"{/if} /></td>*}
	<td align="center">
	<select  name="posted_data[{$products[prod_num].productid}][avail]">
	           <option value="Y" {if $products[prod_num].avail eq "Y"} selected {/if}>Yes</option>
		   <option value="N" {if $products[prod_num].avail eq "N"} selected {/if}>No</option>
        </select> 
			   
       </td>
</tr>

{/section}

<tr>
	<td colspan="6" class="SubmitBox">
	<input type="button" value="{$lng.lbl_delete_selected|escape}" onclick="javascript: document.featuredproductsform.mode.value = 'delete'; document.featuredproductsform.submit();" />
	<input type="submit" value="{$lng.lbl_update|escape}" />
	</td>
</tr>


{else}

<tr>
<td colspan="6" align="center">{$lng.txt_no_featured_products}</td>
</tr>

{/if}

<tr>
<td colspan="6"><br /><br />{include file="main/subheader.tpl" title=$lng.lbl_add_event}</td>
</tr>

<tr>
	<td align="left"><b>Image</b></td>
	<td align="left"><input type="file"  name="newproduct" /></td>
	
	<td align="right"><b>Date</b></td>
	<td align="left"><input type="text" name="date" size="20" /></td>
	
</tr>

<tr>
	
	
	<td align="left" ><b>Event</b></td>
	<td align="left"><input type="text" name="event" size="30" /></td>
	
	<td align="right"><b>Description</b></td>
	<td align="left"><textarea name="edesc" cols="25" rows="2"></textarea></td>
	
</tr>






<tr>
	<td colspan="6" class="SubmitBox">
	<input type="submit" value="{$lng.lbl_add_new|escape}" onclick="javascript: document.featuredproductsform.mode.value = 'add'; document.featuredproductsform.submit();"/>
	</td>
</tr>

</table>
</form>

{/capture}
{include file="dialog.tpl" title=$lng.lbl_featured_products content=$smarty.capture.dialog extra='width="100%"'}
