{* $Id: statistics.tpl,v 1.24.2.1 2006/06/16 10:47:41 max Exp $ *}
{include file="main/include_js.tpl" src="main/calendar2.js"}
{capture name=dialog}

<form action="" method="post" name="disreportform">
<input type="hidden" name="mode" value="search"  />

<table cellpadding="1" cellspacing="1">


<tr>
	<th align="right">{$lng.lbl_reporttype}:</th>
	<td>
	     <select name='report_type'>
	     <option value="week"  { if $reportType == "week"} selected {/if} >Weekly</option>
              <option value="month" { if $reportType == "month"} selected {/if} >Monthly</option> 
	      <option value="daily" { if $reportType == "daily"} selected {/if} >Daily</option>
	       
           </select>
	</td>
	<th align="right">{$lng.lbl_region}:</th>
	<td>
	    <select name='region'>
	      <option value="all" >All</option>
               {section name=idx loop=$regions}
                  <option value="{$regions[idx].region}" { if $regions[idx].region eq $rgn } selected {/if} >{$regions[idx].region}</option>
	       {/section}
           </select>
	</td>
</tr>

<tr>
	<th align="right">{$lng.lbl_report_from}:</th>
	<td><input type="text" name='Startdate' value="{$SDate}"/><a href="javascript:cal4.popup();" ><img src="{$cdn_base}/images/cal.gif" width="16" height="16" border="0" alt="Click Here to Pick up the date"></a></td>
	<th align="right">{$lng.lbl_report_to}:</th>
	<td><input type="text" name='Enddate'  value="{$EDate}" /><a href="javascript:cal5.popup();" class=""><img src="{$cdn_base}/images/cal.gif" width="16" height="16" border="0" alt="Click Here to Pick up the date"></a></td>
</tr>
<tr>
	<td colspan="4"><input type="submit" value="{$lng.lbl_submit|escape}" /></td>
</tr>

<script language="javascript">
      var cal4 = new calendar2(document.forms['disreportform'].elements['Startdate']);
      cal4.year_scroll = true;
      cal4.time_comp = false;

       var cal5 = new calendar2(document.forms['disreportform'].elements['Enddate']);
      cal5.year_scroll = true;
      cal5.time_comp = false;


</script>

</table>
</form>

{/capture}
{include file="dialog.tpl" title='' content=$smarty.capture.dialog extra='width="100%"'}

<br />
<br />
{if $mode}
{capture name=dialog}
<table cellpadding="1" cellspacing="1" width="100%">

<tr class="TableHead">
	
	<td >{$lng.lbl_discount}</td>
	<td>{$lng.lbl_pct}(%)</td>
	<td >{$lng.lbl_from}</td>
	<td >{$lng.lbl_to}</td>
	<td >{$lng.lbl_product_sold}</td>
	<td >{$lng.lbl_sales}(Rs)</td>
	<td >{$lng.lbl_discount}</td>
</tr>


{if $discounts}

{section name=num loop=$discounts}

<tr{cycle values=', class="TableSubHead"'}>
	<td style="left-padding:20px">{$discounts[num].name}</td>
        <td style="left-padding:20px">{$discounts[num].pct}</td>
	<td style="left-padding:20px">{$discounts[num].from}</td>
	<td style="left-padding:20px">{$discounts[num].to}</td>
	<td style="left-padding:20px">{$discounts[num].sold}</td>
	<td style="left-padding:20px">{$discounts[num].sales}</td>
	<td align="center">{$discounts[num].discount}</td>
	
</tr>
{/section}

{else}

<tr>
	<td colspan="4" align="center">{$lng.txt_no_result}</td>
</tr>

{/if}


</table>


</form>

<br />

{/capture}
{include file="dialog.tpl" title='' content=$smarty.capture.dialog extra='width="100%"'}
{/if}