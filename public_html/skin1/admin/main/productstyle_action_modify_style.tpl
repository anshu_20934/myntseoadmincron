<base href="{$http_location}/" >
<form action="{$http_location}/admin/productstyle_action_new.php" method="post" name="productstyleaction" id="productstyleaction" enctype="multipart/form-data">
<input type="hidden" name="mode" value="updatestyle">
<input type="hidden" name="style_id" value="{$style_details[0].id}">
<div style="font-size: 15px;font-weight: bold;padding: 10px 5px 5px;" > {$style_properties.product_display_name}</div>
<table width="100%" cellpadding="0" cellspacing="0" class="style_table" >

 <tr>
<td><b>This page will be permanently removed from 22nd December 2014. The same functionality is provided by <a href = 'http://spoc.mynt.myntra.com/faces/catalog/product/searchStyles.xhtml' target="_blank">http://spoc.mynt.myntra.com/faces/catalog/product/searchStyles.xhtml</a></b>
</td>
</tr>

    <tr class="shade2">
		<td class="label" width="33%">
           Vendor Article Number * : <input type="text" class="textbox" name="article_number" value="{$style_properties.article_number}" maxlength="255"> <div class="error_div"></div>
        </td>
        <td class="label" width="33%">
            Vendor Article Name * : <input type="text" class="textbox" name="article_name" id="article_name" maxlength="255" value="{$style_properties.variant_name}" ><div class="error_div"></div>
        </td>
		<td  class="label" width="33%">
         Status : <select name="styletype" id="styletype"  onchange="validateNewStatus('{$style_details[0].styletype}', this.value);">
         {foreach from=$allStyleStatus item=status}
            <option value="{$status.id}" {if $style_details[0].styletype eq $status.id} selected{/if}>{$status.name}</option>
          {/foreach}
         </select>
        </td>
	</tr>
	<tr class="shade2">
		<td class="label" colspan="3">
           Comments : <input type="text" class="textbox" name="style_comments" value="{$style_properties.comments}">
        </td>
	</tr>
</table>
<script type="text/javascript">
var singleStyleMode=true;
var ownerStyleId={$style_details[0].id};
var resultPanelHeight=200;
var panelWidth='100%';
var possibleStatusTrans = {$possibleStatusTrans};
</script>
<div style="font-size: 15px;font-weight: bold;padding: 10px 5px 5px;" > ATTRIBUTE GROUPINGS</div>
<link rel="stylesheet" type="text/css" href="{$http_location}/admin/extjs/resources/css/ext-all.css"/>
	
<script type="text/javascript" src="{$http_location}/admin/extjs/adapter/ext/ext-base.js"></script>
	
<script type="text/javascript" src="{$http_location}/admin/extjs/ext-all.js"></script>
<script type="text/javascript" src="{$http_location}/skin1/admin/style/attr_grouping.js"></script>
<div id="contentPanel"></div>

<div style="font-size: 15px;font-weight: bold;padding: 10px 5px 5px;" > CATALOG CLASSIFICATION - USED FOR OPERATION AND ANALYSIS</div>
<table width="100%" cellpadding="0" cellspacing="0" class="style_table">
    <tr class="shade2">

		<td class="label" width="100px">
            Article type * :
		</td>
		<td class="label" width="200px">
			<select name="global_attr_article_type" id="global_attr_article_type"  onchange="getParents(this.value); checkDiscountRule();">
				<option value="">Select</option>
				{foreach from=$article_types item=types}
				<option value="{$types.id}" {if $style_properties.global_attr_article_type eq $types.id} selected{/if}>{$types.typename}</option>
				{/foreach}
			</select>
            <div class="error_div"></div>
        </td>
		<td class="label" width="100px">
            Sub Category * :
		</td>
		<td class="label" width="200px">
		<input type="text" class="textbox" name="global_attr_sub_category_name" id="global_attr_sub_category_name" value="{$style_properties.global_attr_sub_category_name}" readonly="true">
        <input type="hidden" class="textbox" name="global_attr_sub_category" id="global_attr_sub_category_id" value="{$style_properties.global_attr_sub_category}" readonly="true">
        </td>
		<td class="label" width="100px">
            Master Category * :
		</td>
		<td class="label" width="200px">
			<input type="text" class="textbox" name="global_attr_master_category_name" id="global_attr_master_category_name" value="{$style_properties.global_attr_master_category_name}" readonly="true">
			<input type="hidden" class="textbox" name="global_attr_master_category" id="global_attr_master_category_id" value="{$style_properties.global_attr_master_category}" readonly="true">

        </td>
	</tr>
	<tr class="shade2">
		<td class="label">
            Brand * :
		</td>
		<td class="label">
			<select name="global_attr_brand" id="global_attr_brand" onChange="checkDiscountRule();">
				<option value="">NA</option>
				{foreach from=$attributes.Brand item=types}
						<option value="{$types}" {if $style_properties.global_attr_brand eq $types} selected{/if} >{$types}</option>
				{/foreach}
			</select>
			<div class="error_div"></div>
        </td>
		<td class="label">
            Age Group :
		</td>
		<td class="label">
			<select name="global_attr_age_group" >
				<option value="NA">NA</option>
				{foreach from=$attributes.AgeGroup item=types}
						<option value="{$types}" {if $style_properties.global_attr_age_group eq $types} selected{/if} >{$types}</option>
				{/foreach}
			</select>

        </td>
		<td class="label">
            Gender * :
		</td>
		<td class="label">
			<select name="global_attr_gender" id="global_attr_gender" >
				<option value="">NA</option>
				{foreach from=$attributes.Gender item=types}
						<option value="{$types}" {if $style_properties.global_attr_gender eq $types} selected{/if}>{$types}</option>
				{/foreach}
			</select>
            <div class="error_div"></div>
        </td>
	</tr>
	<tr class="shade2">
		<td class="label">
            Base Colour * :
		</td>
		<td class="label">
			<select name="global_attr_base_colour" id="global_attr_base_colour" >
				<option value="">NA</option>
				{foreach from=$attributes.Colour item=types}
						<option value="{$types}" {if $style_properties.global_attr_base_colour eq $types} selected{/if}>{$types}</option>
				{/foreach}
			</select>
			<div class="error_div"></div>
        </td>
		<td class="label">
            Colour 1 :
		</td>
		<td class="label">
			<select name="global_attr_colour1" >
				<option value="NA">NA</option>
				{foreach from=$attributes.Colour item=types}
						<option value="{$types}" {if $style_properties.global_attr_colour1 eq $types} selected{/if}>{$types}</option>
				{/foreach}
			</select>

        </td>
		<td class="label">
            Colour 2 :
		</td>
		<td class="label">
			<select name="global_attr_colour2" >
				<option value="NA">NA</option>
				{foreach from=$attributes.Colour item=types}
						<option value="{$types}" {if $style_properties.global_attr_colour2 eq $types} selected{/if} >{$types}</option>
				{/foreach}
			</select>

        </td>
	</tr>
	<tr class="shade2">
		<td class="label">
            Fashion Type * :
		</td>
		<td class="label">
			<select name="global_attr_fashion_type" id="global_attr_fashion_type" onChange="checkDiscountRule();">
				<option value="">NA</option>
				{foreach from=$attributes.FashionType item=types}
						<option value="{$types}" {if $style_properties.global_attr_fashion_type eq $types} selected{/if} >{$types}</option>
				{/foreach}
			</select>
			<div class="error_div"></div>
        </td>
		<td class="label">
            Season * :
		</td>
		<td class="label">
			<select name="global_attr_season" id="global_attr_season" >
				<option value="">NA</option>
				{foreach from=$attributes.Season item=types}
						<option value="{$types}" {if $style_properties.global_attr_season eq $types} selected{/if}>{$types}</option>
				{/foreach}
			</select>
            <div class="error_div"></div>
        </td>
		<td class="label">
            Year * :
		</td>
		<td class="label">
			<select name="global_attr_year" id="global_attr_year" >
				<option value="">NA</option>
				{foreach from=$attributes.Year item=types}
						<option value="{$types}" {if $style_properties.global_attr_year eq $types} selected{/if}>{$types}</option>
				{/foreach}
			</select>
            <div class="error_div"></div>
        </td>
	</tr>
	<tr class="shade2">
		<td class="label">
            Usage * :
		</td>
		<td class="label">
			<select name="global_attr_usage" id="global_attr_usage" >
				<option value="">NA</option>
				{foreach from=$attributes.Usage item=types}
						<option value="{$types}" {if $style_properties.global_attr_usage eq $types} selected{/if}>{$types}</option>
				{/foreach}
			</select>
			<div class="error_div"></div>
        </td>
		<td colspan="5"></td>

	</tr>

</table>
<div style="font-size: 15px;font-weight: bold;padding: 10px 5px 5px;display:none;" id="article_type_specific_attributes_div">Specific Attributes</div>
<table width="100%" cellpadding="0" cellspacing="0" class="style_table" id="article_type_specific_attributes_table" style="display:none;">

</table>	
<div style="font-size: 15px;font-weight: bold;padding: 10px 5px 5px;" >PRICING AND DISCOUNTS </div>
<table width="100%" cellpadding="0" cellspacing="0" class="style_table">
    <tr class="shade2">
		<td class="label">
            Price * : <input type="text" class="textbox" id="price" name="price" value="{$style_details[0].price}" >
            <div class="error_div"></div>
        </td>


        <!-- td class="label">
            Discount Type :
             <select name="discount_type">
            <option value="" {if $style_properties.discount_type eq ''}SELECTED{/if}>None</option>
            <option value="absolute" {if $style_properties.discount_type eq 'absolute'}SELECTED{/if}>Absolute</option>
            <option value="percent" {if $style_properties.discount_type eq 'percent'}SELECTED{/if}>Percent</option>
            </select>
        </td>

         <td class="label">
            Discount Value : <input type="text" class="textbox" name="discount_value" value="{$style_properties.discount_value}" >
        </td -->
        
        <td class="label">
           Rating : <select name="rating">
			<option value="1" {if $style_properties.myntra_rating eq '1'} selected="selected"{/if}>1</option>
			<option value="2" {if $style_properties.myntra_rating eq '2'} selected="selected"{/if}>2</option>
			<option value="3" {if $style_properties.myntra_rating eq '3'} selected="selected"{/if}>3</option>
			<option value="4" {if $style_properties.myntra_rating eq '4'} selected="selected"{/if}>4</option>
			<option value="5" {if $style_properties.myntra_rating eq '5'} selected="selected"{/if}>5</option>
			<option value="6" {if $style_properties.myntra_rating eq '6'} selected="selected"{/if}>6</option>
			<option value="7" {if $style_properties.myntra_rating eq '7'} selected="selected"{/if}>7</option>
			<option value="8" {if $style_properties.myntra_rating eq '8'} selected="selected"{/if}>8</option>
			<option value="9" {if $style_properties.myntra_rating eq '9'} selected="selected"{/if}>9</option>
			<option value="10" {if $style_properties.myntra_rating eq '10'} selected="selected"{/if}>10</option>
			<option value="11" {if $style_properties.myntra_rating eq '11'} selected="selected"{/if}>11</option>
		</select>
        </td>
        
        <td class="label">
        	<div id="discountrule">
	        {if $discountdata}
            Discount Rule #{$discountdata.ruleid} - {$discountdata.display_name} - {if $discountwillbeapplied} Will be applied in some time. {else} is Applied. {/if} <br/>
            This discount rule is currently {if $discountdata.enabled eq '1'} Enabled {else} Disabled {/if} <br/> 
            Discount Details : {$discountdata.algoid} - {$discountenginedata.labelText}
	        {/if}
            </div>
        </td>
	</tr>
</table>
<div style="font-size: 15px;font-weight: bold;padding: 10px 5px 5px;" > SIZE OPTIONS</div>
<span style="display:none"> <input type="radio" id="size_option_check" name="size_option_check" value='1' checked></span>
<table width="100%" cellpadding="0" cellspacing="0" border="0" class="style_table">
<tr class="shade2">
	<td class="label" colspan=5>
		<input type="hidden" class="textbox" name="appy_scale_mode" id="appy_scale_mode" value="{$applySizeScaleRestrictionPageMode}" />
	</td>
</tr>
{if $applySizeScaleRestriction}
	<tr class="shade2" id="option_scale_tr">
		<td class="label">Size Scale*:
			<select name="attr_type_size_chart_scale"  id="attr_type_size_chart_scale" onChange="getSizeOptionValue(this.value)">
				<option value="">- Select -</option>
				{foreach from=$sizeScales item=scales}
					<option value="{$scales.id}" {if $scales.id eq $optionScaleId} selected = "true" {/if}>{$scales.scale_name}</option>
				{/foreach}
			</select>
		</td>
		<td colspan=3></td>
	</tr>
{/if}
	{foreach from=$skus item=opt name=itr}
    <tr class="shade2" id="optionTr{$smarty.foreach.itr.index}">
		<td class="label" style="display:none;">Option Name * :
            <select name="size_option_name[{$smarty.foreach.itr.index}]" id="size_option_name[{$smarty.foreach.itr.index}]" style="width:150px;float:right;" class="option-name-select">
            <option value="None">None</option>
            <option value="Size" {if $opt.option_id } selected="true"{/if}>Size</option>
            <!-- <option value="Color">Color</option> -->
            </select>
            <input type="hidden" class="textbox" name="option_mapping_id[{$smarty.foreach.itr.index}]" id="option_mapping_id[{$smarty.foreach.itr.index}]" value="{$opt.id}" />
        </td>
        {if $applySizeScaleRestriction}
        <td class="label selected_scale_size_values_td">Size Value * :
        	<select class="selected_scale_size_values" name="attr_type_size_value[{$smarty.foreach.itr.index}]"  id="attr_type_size_value[{$smarty.foreach.itr.index}]" onchange="setTextBoxSizeValue(this, {$smarty.foreach.itr.index})">
        		{if $sizeChartValues}
	        		{foreach from=$sizeChartValues item=size_value}
						<option value="{$size_value.id}" {if $size_value.id eq $opt.size_id} selected = "true" {/if}>{$size_value.size_value}</option>
					{/foreach}
        		{else}
        			<option value="">- Select a Scale -</option>
        		{/if}
        		
        	</select>
        </td>
        {/if}
		<td class="label" style="padding-left:10px">Size * : 
			<input type="text" class="textbox textbox_size_value" name="size_option_value[{$smarty.foreach.itr.index}]" id="size_option_value[{$smarty.foreach.itr.index}]" value="{$opt.option_value}" {if $applySizeScaleRestriction} readonly="readonly;"{/if}/>
			<input type="hidden" class="textbox" name="option_id[{$smarty.foreach.itr.index}]" id="option_id[{$smarty.foreach.itr.index}]" value="{$opt.option_id}" />
        </td>
		<td class="label">SKU Code * :
			<input type="text" class="textbox" name="sku_code_value[{$smarty.foreach.itr.index}]" id="sku_code_value_{$smarty.foreach.itr.index}" onkeyup="list_skus('sku_code_value_{$smarty.foreach.itr.index}')" value="{$opt.sku_code}" /> <input type="hidden" class="textbox" name="sku_code_id[{$smarty.foreach.itr.index}]" id="sku_code_id[{$smarty.foreach.itr.index}]" value="{$opt.sku_id}" />
        </td>
		<td class="label" style="display:none;">
			Enabled : <input type="checkbox" name="sku_enabled_cb[{$smarty.foreach.itr.index}]" id="sku_enabled_cb[{$smarty.foreach.itr.index}]" onchange="checkEnabled(this, {$smarty.foreach.itr.index})" {if $opt.sku_enabled eq '1'} checked='true'{/if} />
			<input type="hidden" name="sku_enabled[{$smarty.foreach.itr.index}]" id="sku_enabled[{$smarty.foreach.itr.index}]" value="{$opt.sku_enabled}" />
		</td>
		<td style="padding-left:10px"><input type="button" value="Delete Row" onclick="deleteOptionsSkusMapping(this, {$smarty.foreach.itr.index})"/></td>
    </tr>
	<script type="text/javascript">
		rowCount++;
	</script>
	{/foreach}
	<tr class="shade2" id="addTr">
		<td style="padding-left:10px"><input type="button" value="Add Row" onclick="addOptionsRow()"/></td>
		<td colspan="3"></td>
	</tr>

</table>
<div style="font-size: 15px;font-weight: bold;padding: 10px 5px 5px;" >DISPLAY CLASSIFICATION - USED ON THE MYNTRA.COM USER INTERFACE</div>
<table width="100%" cellpadding="0" cellspacing="0" class="style_table">
	<tr class="shade2">
	{assign var=counter value=0}
	{foreach from=$all_filters key=group item=filters}
        <td class="label" width="100px" align="center">
            {if $group eq 'Brands'}Classification Brand{else}{$group}{/if}
        </td>

        <td class="label" width="250px" align="center">
            <select name="{$group|replace:" ":""|replace:"/":""|lower}_filters[]" id="{$group|lower}" {if $group eq 'Brands' || $group eq 'Fit'}{else}multiple="multiple" size="10"{/if} style="width:160px">

                <option value="">None</option>
            {foreach from=$filters key=k item=filter}
                {assign var=is_selected value="notselected"}
                {if $selected_filters}
                    {section name=inner loop=$selected_filters}
                        {if $selected_filters[inner] eq $filter.filter_name}
                            {assign var=is_selected value="selected"}
                        {/if}
                    {/section}
                {/if}

                <option {if $is_selected eq 'selected'}selected{/if} value="{$filter.filter_name}">{$filter.filter_name}</option>
            {/foreach}
            </select>
        </td>
	{assign var=counter value=$counter+1}
	{if $counter mod 3 eq 0}
			</tr>
			<tr class="shade2">
	{/if}
    {/foreach}
	</tr>
</table>
<div style="font-size: 15px;font-weight: bold;padding: 10px 5px 5px;" >PRODUCT DETAILS</div>
<table width="100%" cellpadding="0" cellspacing="0" class="style_table">
	<tr class="shade2">
		<td colspan="2" class="label">Product Display Name : <input type="text"  name="product_display_name" value="{$style_properties.product_display_name}" style="width:300px" id="product_display_name" readonly="true" maxlength="255">
			<input type="button" value="modify" onclick="$('#product_display_name').removeAttr('readonly')"/>
        </td>
		<td class="label" colspan="2">
            Tags * : <input type="text" class="textbox" name="product_tags" id="product_tags" value="{$style_properties.product_tags}" style="width:400px">
            <div class="error_div"></div>
        </td>
    </tr>
	<tr class="shade1">
		<td class="label">Description</td>
        <td colspan="3"><textarea name="product_desc" cols="50" rows="5" class="mceEditor" >{$style_details[0].description}</textarea></td>
        <div class="error_div"></div>
    </tr>
    <tr class="shade2"><td class="label" colspan="4"></td></tr>
    <tr class="shade1">
		<td class="label">Style Note</td>
        <td colspan="3"><textarea name="styleNote" cols="50" rows="5" class="mceEditor" >{$style_properties.style_note}</textarea></td>
        <div class="error_div"></div>
    </tr>
    <tr class="shade2"><td class="label" colspan="4"></td></tr>
    <tr class="shade1">
		<td class="label">Materials Care Desc</td>
        <td colspan="3"><textarea name="materialsCareDesc" cols="50" rows="5" class="mceEditor" >{$style_properties.materials_care_desc}</textarea></td>
        <div class="error_div"></div>
    </tr>
    <tr class="shade2"><td class="label" colspan="4"></td></tr>
    <tr class="shade1">
		<td class="label">Size Fit Desc</td>
        <td colspan="3"><textarea name="sizeFitDesc" cols="50" rows="5" class="mceEditor" >{$style_properties.size_fit_desc}</textarea></td>
        <div class="error_div"></div>
    </tr>
    <tr class="shade2">
        <td class="label" colspan="2">
            Product type * :
             <select name='product_type' id='product_type'>
                {foreach from=$producttypes key=group item=types}
                <optgroup label="{$group}">
                {foreach from=$types item=type}
                    <option value="{$type.id}" {if $style_details[0].product_type eq $type.id} selected{/if} >{$type.name}</option>
                 {/foreach}
            {/foreach}
            </select>
            <div class="error_div"></div>
        </td>
         <td class="" colspan="2">
            Target URL : <input type="text" class="textbox" name="target_url" id="target_url" value="{$style_properties.target_url}" maxlength="255" style="width:400px">
            <br>if you want to make the url like {$http_location}/mumbai-indian-jersey<br> then enter only <b>mumbai-indian-jersey</b>
        </td>
    </tr>
	<tr class="shade2">
        <td class="label">
            Default Image  *
        </td>
        <td colspan="2">
              <!-- input id="default_image" name="default_image" type="file" / -->
              <div class="error_div"></div>
        </td>
        <td ><span id="default_images_preview"> <img src="{$style_properties.default_image|replace:'_images.':'_images_48_64.'}" ></span>
        </td>
    </tr>
	<!--other images -->
    <tr class="shade1">
        <td class="label">
            Top Image
        </td>
        <td colspan="2">
              <!-- input id="top_image" name="top_image" type="file" / -->
        </td>
        <td ><span style="float:left;padding:0px 5px" id="top_images_preview"><img src="{$style_properties.top_image|replace:'_images.':'_images_48_64.'}" ></span>
        <a id="top_images_remove" onclick="deleteImage('top_image');$('#top_images_preview').html('');$(this).hide();" style="cursor:pointer;float:left;padding:20px 0;{if $style_properties.top_image}{else}display:none{/if}">remove</a>
        </td>
    </tr>
    <tr class="shade2">
        <td class="label">
            Front Image
        </td>
        <td colspan="2">
              <!-- input id="front_image" name="front_image" type="file" / -->
        </td>
        <td><span style="float:left;padding:0px 5px" id="front_images_preview"><img src="{$style_properties.front_image|replace:'_images.':'_images_48_64.'}" ></span>
        <a id="front_images_remove" onclick="deleteImage('front_image');$('#front_images_preview').html('');$(this).hide();" style="cursor:pointer;float:left;padding:20px 0;{if $style_properties.front_image}{else}display:none{/if}">remove</a>
        </td>
    </tr>
    <tr class="shade1">
        <td class="label">
            Back Image
        </td>
        <td colspan="2">
              <!-- input id="back_image" name="back_image" type="file" / -->
        </td>
        <td><span style="float:left;padding:0px 5px" id="back_images_preview"><img src="{$style_properties.back_image|replace:'_images.':'_images_48_64.'}" ></span>
        <a id="back_images_remove" onclick="deleteImage('back_image');$('#back_images_preview').html('');$(this).hide();" style="cursor:pointer;float:left;padding:20px 0;{if $style_properties.back_image}{else}display:none{/if}">remove</a>
        </td>
    </tr>
    <tr class="shade2">
        <td class="label">
            Right Image
        </td>
        <td colspan="2">
              <!-- input id="right_image" name="right_image" type="file" / -->
        </td>
        <td><span style="float:left;padding:0px 5px" id="right_images_preview"><img src="{$style_properties.right_image|replace:'_images.':'_images_48_64.'}" ></span>
        <a id="right_images_remove" onclick="deleteImage('right_image');$('#right_images_preview').html('');$(this).hide();" style="cursor:pointer;float:left;padding:20px 0;{if $style_properties.right_image}{else}display:none{/if}">remove</a>
        </td>
    </tr>
    <tr class="shade1">
        <td class="label">
            Left Image
        </td>
        <td colspan="2">
              <!-- input id="left_image" name="left_image" type="file" / -->
        </td>
        <td><span style="float:left;padding:0px 5px" id="left_images_preview"><img src="{$style_properties.left_image|replace:'_images.':'_images_48_64.'}" ></span>
        <a id="left_images_remove" onclick="deleteImage('left_image');$('#left_images_preview').html('');$(this).hide();" style="cursor:pointer;float:left;padding:20px 0;{if $style_properties.left_image}{else}display:none{/if}">remove</a>
        </td>
    </tr>
    <tr class="shade2">
        <td class="label">
            Bottom Image
        </td>
        <td colspan="2">
              <!-- input id="bottom_image" name="bottom_image" type="file" / -->
        </td>
        <td ><span style="float:left;padding:0px 5px" id="bottom_images_preview"><img src="{$style_properties.bottom_image|replace:'_images.':'_images_48_64.'}" ></span>
        <a id="bottom_images_remove" onclick="deleteImage('bottom_image');$('#bottom_images_preview').html('');$(this).hide();" style="cursor:pointer;float:left;padding:20px 0;{if $style_properties.bottom_image}{else}display:none{/if}">remove</a>
        </td>
    </tr>
    <!-- othere images ends-->
    <tr class="shade1">
        <td class="label">
            Size Chart Image
        </td>
        <td colspan="2">
              <p >Size Chart Image Name :<input type="text" name="size_chart_image_name" id="size_chart_image_name" value=""></p>
              <input id="size_chart_image" name="size_chart_image" type="file" />
              <input type="hidden" name="size_chart_image_url" id="size_chart_image_url" value="{$style_details[0].size_chart_image}">
              <p><a href="javascript:uploadSizeChartImage()">Upload File</a></p>
        </td>
        <td class="label"><div style="float:left;padding:20px"><a style="cursor:pointer;text-decoration:underline" onclick="getImagesFromRepo()">Select From Repository</a></div>
        <div style="float:left;" id="size_chart_image_preview"><img src="{$style_details[0].size_chart_image}" alt="" height="50px" width="50px"></div>
        <a id="size_chart_remove" onclick="$('#size_chart_image_url').val('');$('#size_chart_image_preview').html('');$(this).hide();" style="cursor:pointer;float:left;padding:20px 0;{if $style_details[0].size_chart_image}{else}display:none{/if}">remove</a>
        </td>
    </tr>

    <tr class="shade1">
        <td class="label">Whether Customizable</td>
        <td  >
         <select name="is_customizable" onchange="$('#customization_area').toggle()">
            <option value="0" {if $style_details[0].is_customizable eq '0'} selected{/if}>No</option>
            <option value="1" {if $style_details[0].is_customizable eq '1'} selected{/if}>Yes</option>
         </select>
         </td>
         <td colspan="2"></td>

    </tr>
	<tr class="shade2">
		<td class="label">Style category map</td>
        <td colspan="9">
		<select name="stylecatmap[]" id="stylecatmap">
			<option></option>
			{section name="subcat" loop=$subcategories}
				{section name="selectedcat" loop=$stylemaparray}
					{if $subcategories[subcat].categoryid == $stylemaparray[selectedcat].categoryid}
						{assign var="selcat" value="selected"}
					{/if}
				{/section}
				<option value="{$subcategories[subcat].categoryid}"  {$selcat}>{$subcategories[subcat].category}</option>
					{assign var="selcat" value=""}
			{/section}
		</select>
	 </td>
	</tr>
    <tr class="shade1">
        <td class="label" colspan="2"><input type="submit" value="Update Product"> </td><td class="label" colspan="1">Update recency : <input type="checkbox" name="update_recency" ></td>
    </tr>

</table>
</form>
