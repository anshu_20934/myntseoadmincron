{literal}
<script type="text/javascript">
function func_delete_image_key(id){
    var r=confirm("Are you sure you want to delete key : "+id);
    if(r == true){
        document.update.mode.value = 'delete';
        document.update.delete_key_id.value = id;
        document.update.submit();
    }
}

function func_add_image_key(){
    if ( !document.update.add_image_key || document.update.add_image_key.value == '' ||
         !document.update.add_enabled || document.update.add_enabled.value == '' ){
        alert('Key is empty');
        return;
    } 
    document.update.mode.value = 'add';
    document.update.submit();
}

function func_disable_all(){
    $(".enabled").each(
        function(index){
            $(this).removeAttr("checked");
        }
    );
    $(".disabled").each(
        function(index){
            $(this).attr("checked", true);
            $(this).val("0");
        }
    );
    document.update.mode.value = 'save';
    document.update.submit();
}

function func_enable_all(){
    $(".disabled").each(
        function(index){
            $(this).removeAttr("checked");
        }
    );
    $(".enabled").each(
        function(index){
            $(this).attr("checked", true);
            $(this).val("1");
        }
    );
    document.update.mode.value = 'save';
    document.update.submit();
}

</script>
{/literal}

{capture name=dialog}
<form action="jpegminiConfig.php" method="post" name="update">
<input type="hidden" name="mode" />
<input type="hidden" name="delete_key_id" value=""/>
<table cellpadding="3" cellspacing="1" width="100%">
<tr class="TableHead">
	<td width="25%">Image Key</td>
	<td width="25%">Enabled</td>
	<td width="25%">Disabled</td>
	<td width="25%">Action</td>
</tr>

{if $data}

{foreach key=image item=enabled from=$data}
<tr{cycle values=", class='TableSubHead'"}>
		
	<td align="center" style="left-padding:30px"><input name="image_key[{$image}]" type="text" value="{$image}"  maxlength=50/></td>
	<td align="center" style="left-padding:30px"><input name="enabled[{$image}]" class="enabled" type="radio" value="1" {if $enabled}checked="checked"{/if} /></td>
	<td align="center" style="left-padding:30px"><input name="enabled[{$image}]" class="disabled" type="radio" value="0" {if !$enabled}checked="checked"{/if} /></td>
	<td align="center">
		<input type="button" value="Delete" onclick="javascript: func_delete_image_key('{$image}')" />
	</td>	
</tr>
{/foreach}
{else}
<tr>
<td colspan="2" align="center">No Image Keys added as yet</td>
</tr>
{/if}
<tr class='TableSubHead' style="background-color:blue">
	<td align="center" style="left-padding:30px"><input name="add_image_key" type="text" value=""  maxlength=40/></td>
	<td align="center" style="left-padding:30px"><input name="add_enabled" type="radio" value="1" /></td>
	<td align="center" style="left-padding:30px"><input name="add_enabled" type="radio" value="0" /></td>
	<td align="center">
		<input type="button" value="Add" onclick="javascript: func_add_image_key()" />
	</td>
</tr>
<tr>
	<td></td>
	<td></td>
	<td></td>
	<td class="SubmitBox" align="right">
	   <input type="button" value="Enable All" onclick="javascript: func_enable_all();" />
	   <input type="button" value="Disable All" onclick="javascript: func_disable_all();" />
	   <input type="button" value="Update" onclick="javascript: document.update.mode.value = 'save'; document.update.submit();" />
	</td>
	<td></td>
</tr>
</table>
</form>

{/capture}
{include file="dialog.tpl" title="Jpegmini Config" content=$smarty.capture.dialog extra='width="100%"'}
