{* $Id: featured_products.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
{include file="main/include_js.tpl" src="main/popup_product.js"}
{literal}
  <script language="Javascript">
      function confirmDelete()
      {
          var flag = confirm('Are you sure you want to delete?')
          if(!flag)
          {
             return false;
          }
          else
          {
          	 document.groupfrm.mode.value = 'delete';
          	 document.groupfrm.submit();
          	 return true;
          }
      }
  </script>
{/literal}
<a name="featured" />

{$lng.txt_featured_products}

<br /><br />

{capture name=dialog}

<form action="{$form_action}" method="post" name="groupfrm" enctype="multipart/form-data">
<input type="hidden" name="mode"  />

<table cellpadding="3" cellspacing="1" width="100%">

<tr class="TableHead">
	<td width="10%">&nbsp;</td>
	<td width="20%">Name</td>
	<td width="20%">Label</td>
	<td width="20%" align="center">Description</td>
	<td width="20%">Config</td>
	<td width="10%" align="center">Display Order</td>
	<td width="10%" align="center">Active</td>
</tr>

{if $groups}

{section name=grid loop=$groups}

<tr{cycle values=", class='TableSubHead'"}>
    <input type="hidden" name="posted_data[{$groups[grid].id}][gid]" value={$products[grid].id} />
	<td><input type="checkbox" name="posted_data[{$groups[grid].id}][to_delete]" /></td>
	<td align="center"><input type="text" name="posted_data[{$groups[grid].id}][group_name]" value="{$groups[grid].group_name}" /></td>
	<td align="center"><input type="text" name="posted_data[{$groups[grid].id}][group_label]" value="{$groups[grid].group_label}" /></td>
	<td align="center"><textarea name="posted_data[{$groups[grid].id}][description]" >{$groups[grid].description}</textarea></td>
	<td align="center"><input type="text" name="posted_data[{$groups[grid].id}][config]" value="{$groups[grid].config}" /></td>
	<td align="center"><input type="text" name="posted_data[{$groups[grid].id}][display_order]" value="{$groups[grid].display_order}"  size="1" /></td>
	<td align="center">
	<select name="posted_data[{$groups[grid].id}][is_active]">
		<option value="1"{if $groups[grid].is_active eq "1"} selected="selected"{/if}>{$lng.lbl_yes}</option>
		<option value="0"{if $groups[grid].is_active eq "0"} selected="selected"{/if}>{$lng.lbl_no}</option>
	</select>
	</td>
</tr>

{/section}
<td colspan="5"><br /><br /></td>
{else}

<tr>
 <td colspan="4" align="center">No groups available.</td>
</tr>
{/if}

<!--<tr>
<td colspan="5"><br /><br />{include file="main/subheader.tpl" title=$lng.lbl_add_product}</td>
</tr> -->

<tr>
	<td colspan="5" class="SubmitBox">
	<input type="button" value="{$lng.lbl_delete_selected|escape}" onclick="return confirmDelete();" />
	<input type="submit" value="{$lng.lbl_update|escape}" onclick="javascript: document.groupfrm.mode.value = 'update'; document.groupfrm.submit();" />

	</td>
</tr>


</table>
</form>
<form name="frmG" action="{$form_action}" method="post" enctype="multipart/form-data" >
<input type="hidden" name="mode" value="addgroup" />
<table cellpadding="3" cellspacing="1" width="100%">
   <tr>
        <td><hr/></td>
        <td><hr /></td>
   </tr>
   <tr>
        <td><h2>Add Group</h2></td>
        <td>&nbsp;</td>
   </tr>
   <tr>
        <td>Group Name </td>
        <td><input type="text" name="group_name" value=""  /></td>
        <td>Group Label </td>
        <td><input type="text" name="group_label" value=""  /></td>

   </tr>
   <tr>
    <td>Description </td>
        <td colspan="2"><textarea name="description" cols="25" rows="5" ></textarea></td>
   </tr>
   <tr>
     <td>Display Order </td>
     <td><input type="text" name="display_order" /></td>
     <td>Config </td>
     <td><input type="text" name="config" /></td>

     <td>Active</td>
     <td align="center">
        <select name="is_active">
            <option value="1" >{$lng.lbl_yes}</option>
            <option value="0">{$lng.lbl_no}</option>
        </select>
	</td>
   </tr>
   <tr>
   <td colspan="2" align="center">
   <input type="submit" value="{$lng.lbl_add|escape}" name="sbtFrm" />
   </td>
   </tr>

</table>
</form>
{/capture}
{include file="dialog.tpl" title="Filter Groups" content=$smarty.capture.dialog extra='width="100%"'}
<br /><br />


