<div style="border: 1px solid gray; border-top: none; ">
	<div style="font-family: arial, sans-serif;  padding: 10px">
		<P style="text-transform: capitalize">Dear {$customerName},</P>
		We noticed that there are few items waiting for your attention in your shopping cart. <BR/>
		{if $discountUpto gt 0}
		We are running <i>upto {$discountUpto}%</i> discount on select products, until stocks last. This may be the best time to buy those items.<BR/> 
		So hurry up and grab your products on Myntra!<BR/><BR/>
		{/if}		
		You can <a href="http://www.myntra.com/mkmycart.php?utm_source=rem&utm_medium=abn_cart&utm_campaign={$campaignDate}">Click on this link</a> to visit your cart and complete your transaction.<BR/><BR/>
		
		<table cellspacing="0" cellpadding="0" width="70%">
			<thead style="background-color: rgb(224, 224, 224);font-size: 110%; font-weight: bold;">
				<tr>
					<td colspan="2" style="padding: 5px">Shopping Cart Items</td>
					<!-- 
					<td style="padding: 5px; text-align: center">Size</td>
					<td style="padding: 5px; text-align: center">Quantity</td>
					<td style="padding: 5px; text-align: center">Price</td>
					 -->
				</tr>
			</thead>
			<tbody>
				{if $productsInCart|@count > 0}
				{foreach name=outer item=product key=itemid from=$productsInCart}
				<tr style="">
					<!-- This TD Contains Product Image only -->
					<td style="border: 1px solid lightgray; width: 60px;border-right: 0px none;">
						<div>
							{assign var='productStyleId' value=$product.productStyleId}
							{assign var='item' value=$product.cartImagePath}
							{include file="affiliatetemplates/reebok_ipl/cricket_jersey_cart_images.tpl" assign='cartImagePath'}
							
							{if $product.is_customizable eq 0 and $product.totalCustomizedAreas eq 0 and $product.landingpageurl neq ''}
							<a href="http://www.myntra.com/{$product.landingpageurl}?utm_source=rem&utm_medium=abn_cart&utm_campaign={$campaignDate}">
								<img style="border: 0px none" src="{if $cartImagePath|trim neq ' '}{$cartImagePath}{else}{$product.cartImagePath}{/if}" alt="{$product.productStyleName}">
							</a>
							{else}
								<img style="border: 0px none" src="{if $cartImagePath|trim neq ' '}{$cartImagePath}{else}{$product.cartImagePath}{/if}" alt="{$product.productStyleName}">
							{/if}	
						</div>
					</td>
					<!-- This TD Contains Product Name -->
					<td style="border: 1px solid lightgray;padding-left:10px">
						<b>
						{if $product.is_customizable eq 0 and $product.totalCustomizedAreas eq 0 and $product.landingpageurl neq ''}
							<a href="http://www.myntra.com/{$product.landingpageurl}?utm_source=rem&utm_medium=abn_cart&utm_campaign={$campaignDate}">{$product.productStyleName}</a>
						{else}
							<p>{$product.productStyleName}</p>
						{/if}
						</b>
					</td>    
				</tr>
				{/foreach}
				{/if}
				
				<tr style="height: 50px">
					<td colspan="2">
						<div align="right" style="margin-top:10px">
							<a href="http://www.myntra.com/mkmycart.php?utm_source=rem&utm_medium=abn_cart&utm_campaign={$campaignDate}"
							 style="text-decoration: none; padding: 3px 30px; background-color: orange; font-size: 20px; font-family: arial; color: white; -moz-border-radius: 30px; -webkit-border-radius: 30px;-moz-linear-gradient(center bottom , #FF9009 22%, #FFAA44 81%);-moz-box-shadow: 1px 1px 3px rgba(0, 0, 0, 0.2);">
							<b>Buy Now</b></a>
						</div>			
					</td>
				</tr>
				 
				<!--   
				<tr style="height: 50px">
					<td colspan="2">
						<div align="right" style="margin-top:10px">
							<a href="http://www.myntra.com/mkmycart.php?utm_source=rem&utm_medium=abn_cart&utm_campaign={$campaignDate}"
							 style="text-decoration: none; padding: 3px 30px; background-image: url(http://localhost/images/mailer-buy-btn.png); font-size: 20px; font-family: arial; color: white; -moz-border-radius: 30px; -webkit-border-radius: 30px;-moz-linear-gradient(center bottom , #FF9009 22%, #FFAA44 81%);-moz-box-shadow: 1px 1px 3px rgba(0, 0, 0, 0.2);">
							<b>Buy Now</b></a>
						</div>			
					</td>
				</tr>
				-->
			</tbody>
		</table>
		<br/>
		
		Our Customer Champions are available 24 hours all 7 days of the week and will be glad to help you with completing this order or answer any other questions that you may have. 
		<BR/><BR/>
		Regards,<BR/>
		Team Myntra
		<BR/>
		<div style="height: 35px; background-color: white; margin: 6px 0px;">
		   <a href="mailto:support@myntra.com">support@myntra.com</a> | Call: +91-80-43541999 - 24x7 | <a href="http://www.myntra.com?utm_source=rem&utm_medium=abn_cart&utm_campaign={$campaignDate}">www.myntra.com</a>
  		</div> 
	</div>
</div>