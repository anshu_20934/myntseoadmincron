{* $Id: featured_products.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
{include file="main/include_js.tpl" src="main/popup_product.js"}

<a name="featured" />

{$lng.txt_featured_products}

<br /><br />

{capture name=dialog}


<form action="commissionrate_action.php" method="post" name="adminform">
<input type="hidden" name="mode" />


<table cellpadding="3" cellspacing="1" width="100%">

<tr class="TableHead">
	<td width="10%">&nbsp;</td>
	<td width="30%">Commission on</td>
	<td width="20%">Commission rate(in %)</td>
	<td width="20%" align="center">Commission For</td>
	<td width="20%" align="center">{$lng.lbl_status}</td>
</tr>

{if $commissionRatesForStyles}

{section name=idx loop=$commissionRatesForStyles}

<tr{cycle values=", class='TableSubHead'"}>
	<td style="left-padding:20px"><input type="checkbox" name="posted_data[{$commissionRatesForStyles[idx].id}][delete]"   /></td>
	<td align="left" style="left-padding:30px"><b>{$commissionRatesForStyles[idx].name}</a></b></td>
	<td align="center" ><b>{$commissionRatesForStyles[idx].commission_rate}</b></td>
	<td align="center"><b>{$commissionRatesForStyles[idx].commission_for}</b></td>
	<td align="center"><b>{if $commissionRatesForStyles[idx].is_active eq '1'}ACTIVE{else}INACTIVE{/if}</b> </td>
		
</tr>

{/section}

{section name=idx loop=$commissionRatesForAffiliates}

<tr{cycle values=", class='TableSubHead'"}>
	<td style="left-padding:20px"><input type="checkbox" name="posted_data[{$commissionRatesForAffiliates[idx].id}][delete]"   /></td>
	<td align="left" style="left-padding:30px"><b>{$commissionRatesForAffiliates[idx].company_name}</a></b></td>
	<td align="center" ><b>{$commissionRatesForAffiliates[idx].commission_rate}</b></td>
	<td align="center"><b>{$commissionRatesForAffiliates[idx].commission_for}</b></td>
	<td align="center"><b>{if $commissionRatesForAffiliates[idx].is_active eq '1'}ACTIVE{else}INACTIVE{/if}</b> </td>
		
</tr>

{/section}


<tr>
	<td colspan="5" class="SubmitBox">
	<input type="button" value="{$lng.lbl_delete_selected|escape}" onclick="javascript: document.adminform.mode.value = 'delete'; document.adminform.submit();" />
	<input type="submit" value="{$lng.lbl_add|escape}"  onclick="javascript: submitForm(this, 'add');"/>
        <input type="submit" value="{$lng.lbl_modify_selected|escape}" onclick="javascript: submitForm(this, 'modify');" />
	</td>
</tr>


{else}

<tr>
<td colspan="5" align="center">No commission set.</td>
</tr>

<tr>
<td colspan="5" align="left"><input type="submit" value="{$lng.lbl_add|escape}"  onclick="javascript: submitForm(this, 'add');"/></td>
</tr>



{/if}



</table>
</form>

{/capture}
{include file="dialog.tpl" title='List of commission rate' content=$smarty.capture.dialog extra='width="100%"'}
<br /><br />

