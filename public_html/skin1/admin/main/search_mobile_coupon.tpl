<link rel="stylesheet" href="{$SkinDir}/skin1_admin.css" />
<form action="search_mobile_coupon.php" method="post" id="searchMobileCoupon">
	{if $queryResult eq 'default'}
		<div>
			Mobile Number: <input type="text" name="mobileNoToSearch" value="+91"/>
			<input type="submit" name="searhMobile" style="margin-left:10px" onclick="document.getElementById('searchButtonValue').value='searchMobile';$('#searchMobileCoupon').trigger('submit');" value="Search">
		</div>
		<div>
			Email Id: <input type="text" name="emailIdToSearch" value=""/>
			<input type="submit" name="searhEmail" style="margin-left:10px" onclick="document.getElementById('searchButtonValue').value='searchEmail';$('#searchMobileCoupon').trigger('submit');" value="Search">
		</div>
		<input type="hidden" name="method" id="searchButtonValue" value="default"/>
	{elseif $queryResult eq 'couponNotPresentForThisNumber'}
		<div>
			No coupon found for the given mobile number.
		</div>
	{elseif $queryResult eq 'couponExpired'}
		<div>
			Coupon number is {$couponCode}. It is expired.
		</div>
	{elseif $queryResult eq 'couponAlreadyUsed'}
		<div>
			Coupon number is {$couponCode}. It has already been used.
		</div>
	{elseif $queryResult eq 'couponResent'}
		<div>
			Coupon resent.
		</div>
	{elseif $queryResult eq 'couponNotGenerated'}
		<div>
			Coupon hasn't been generated for this user. Please <a href={$genearteCouponLink}>click here</a> to genreate the coupon.
		</div>
	{elseif $queryResult eq 'unregisterdUser'}
		<div>
			This is either an unregistered user or a user registered using old process.
		</div>
	{elseif $queryResult eq 'couponPresentAndUsable'}
		<div>
			Coupon number is {$couponCode}
		</div>		
		<div>
			<input type="submit" name="resendCoupon" onclick="$('#searchMobileCoupon').trigger('submit');" value="Resend"/>
			<input type="hidden" name="method" value="resendCoupon"/>
		</div>
	{/if}
</form>