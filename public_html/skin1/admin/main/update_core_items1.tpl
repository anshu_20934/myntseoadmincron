{* $Id: process_item_exit.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}

<link rel="stylesheet" href="{$SkinDir}/skin1_admin_operations.css" />
{literal}
<script Language="JavaScript" Type="text/javascript">
	$(function() {
		$("input[name='property']").click(function(){
			if( $(this).val() == "status" ) {
				$("#status_change_row").css("display", "table-row");
				$("#quality_change_row").css("display", "none");
			} else {
				$("#quality_change_row").css("display", "table-row");
				$("#status_change_row").css("display", "none");
			}
		});

		$('#update-items-form').submit(function(){
			var property = $("input[name='property']:checked").val();
			if(!property || property == "") {
				alert("Please select a property to update");
				return false;
			}

			var actionMsg = "Are you sure you want to ";
			
			if(property == "status") {
				if($("#status_change").val() == ""){
					alert("Please select a status transition to perform");	
					return false;
				}
				actionMsg += "change status from "+$("#status_change").val();
			} else {
				if($("#quality_level").val() == ""){
					alert("Please select a quality level to change");	
					return false;
				}
				actionMsg += "change Quality Level to "+$("#quality_level").val();
			}

			var itembarcodes = $("#itembarcodes").val();
			itembarcodes = itembarcodes.split('\n');
			var validBarcodes = "";
			var validItemsCount = 0;
			for(var i in itembarcodes) {
				var itembarcode = itembarcodes[i].trim();	
				if(itembarcode.replace(/[\d]/g, '') != "") {
					alert("Invalid separators used for item barcodes");
					return false;
				} else {
					if(itembarcode.trim() != "") {
						validBarcodes += validBarcodes==""?"":",";
						validBarcodes += itembarcode.trim();
						validItemsCount++;
                        if(validItemsCount > 50) {
                          alert("Only 50 items can be scanned in one operation");
                          return false;
                        }
					}
				}
			}
			if(validBarcodes == "") {
				alert("Please enter item barcodes to update");
				return false;
			}
			$("#item_barcodes_list").val(validBarcodes);

			if(confirm(actionMsg)) {
				return true;
			}
			return false;
		});
	});
</script>
{/literal}	

{capture name=dialog}
	{if $errorMsg and $errorMsg ne '' }
		<div style="color: red; font-size: 12px; font-weight: bold; text-align:center;">{$errorMsg}</div>
	{/if}
	{if $successMsg and $successMsg ne '' }
		<div style="font-size: 12px; font-weight: bold; text-align:center;">{$successMsg}</div>
	{/if}
	<form action="updatecoreitemsbulk.php" id="update-items-form" method="POST">
		<input type="hidden" name="action" value="updateitems">
		<input type="hidden" name="user" value="{$login}">
		<input type="hidden" name="item_barcodes_list" id="item_barcodes_list" value="">
		<table border=0 cellspacing=0 cellpadding=0 width=100%>
			<tr>
				<td width=300 valign=top>
					Please Scan the item barcodes below (newline separated):
				</td>
				<td width=520>
					<textarea id="itembarcodes" name="itembarcodes" style="border: 1px solid #cdcdcd; padding: 2px; width: 500px; height: 300px;"></textarea>
				</td>
			</tr>	
			<tr>
				<td width=300 valign=top>
					Select property to be update:
				</td>
				<td>
					<input type="radio" name="property" value="status"> Item Status
					<input type="radio" name="property" value="quality"> Quality Level
				</td>
			</tr>	
			<tr id="status_change_row" style="display:none;">
				<td width=300 valign=top>
					Select Status transition:
				</td>
				<td>
					<select name="status_change" id="status_change">
						<option value="">--Select Transition--</option>
						<option value="NOT_FOUND-SHRINKAGE">NOT FOUND TO SHRINKAGE</option>
						<option value="FOUND-STORED">FOUND TO STORED</option>
						<option value="SHIPPED-CUSTOMER_RETURNED">SHIPPED TO CUSTOMER_RETURNED</option>
						<option value="ISSUED-SHIPPED">ISSUED TO SHIPPED</option>
                        <option value="RETURN_FROM_OPS-STORED">RETURN_FROM_OPS TO STORED</option>
                        <option value="CUSTOMER_RETURNED-STORED">CUSTOMER_RETURNED TO STORED</option>
                        <option value="ISSUED-RETURN_FROM_OPS">ISSEUD TO RETURN_FROM_OPS</option>
                        <option value="STORED-RETURNED">STORED TO RETURNED</option>
                        <option value="STORED-PROCESSING">STORED TO PROCESSING</option>
                        <option value="NOT_FOUND-FOUND">STORED TO PROCESSING</option>
					</select>
				</td>
			</tr>	
			<tr id="quality_change_row" style="display:none;">
				<td width=300 valign=top>
					Select Quality Level:
				</td>
				<td>
					<select name="quality_level" id="quality_level">
						<option value="">--Select Quality Level--</option>
						<option value="Q1">Q1</option>
						<option value="Q2">Q2</option>
						<option value="Q3">Q3</option>
					</select>
				</td>
			</tr>	
			<tr height=5><td colspan=2>&nbsp;</td></tr>
			<tr><td colspan=2 align=right><input type=submit name="update-item-btn" value="Update"></td></tr>
		</table>
	</form>
{/capture}
{include file="dialog.tpl" title="Bulk Items Editing Brahmastra" content=$smarty.capture.dialog extra='width="100%"'}
