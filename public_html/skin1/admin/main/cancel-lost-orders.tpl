<script type="text/javascript">
    var cancellation_codes = {$cancellation_codes};
    var progresskey = '{$progresskey}';
</script>

<link rel="stylesheet" type="text/css" href="{$http_location}/admin/extjs/resources/css/ext-all.css"/>
<script type="text/javascript" src="{$http_location}/admin/extjs/adapter/ext/ext-base.js"></script>
<script type="text/javascript" src="{$http_location}/admin/extjs/ext-all.js"></script>
<script type="text/javascript" src="{$http_location}/admin/extjs/plugins/superselectbox/SuperBoxSelect.js"></script>
<script type="text/javascript" src="{$http_location}/admin/extjs/plugins/fileuploadfield/FileUploadField.js"></script>
<link rel="stylesheet" type="text/css" href="{$http_location}/admin/extjs/plugins/superselectbox/superboxselect.css"/>
<link rel="stylesheet" type="text/css" href="{$http_location}/admin/extjs/plugins/fileuploadfield/css/fileuploadfield.css"/>
<script type="text/javascript" src="/skin1/admin/main/cancel-lost-orders.js"></script>
<br>
<br>
<div id="contentPanel"></div>