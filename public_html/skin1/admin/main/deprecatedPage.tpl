{* $Id: cancelorders.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
{include file="main/include_js.tpl" src="main/popup_product.js"}

<link rel="stylesheet" href="{$SkinDir}/skin1_admin_operations.css" />
{literal}
<style>
.list-form tr{
	height:5px;
}
</style>
{/literal}
<script type="text/javascript">
	var progressKey = '{$progresskey}';
</script>
{capture name=dialog}
<div>
	<table>
			<tr>
				<td>This page has been permanently removed. Please start using 
					<a href = 'http://spoc.mynt.myntra.com/faces/catalog/images/styleImagesUpload.xhtml' target="_blank">http://spoc.mynt.myntra.com/faces/catalog/images/styleImagesUpload.xhtml </a> page which provides same functionality.
				</td>
			</tr>
			<tr>
				The new page is more fast and easy to use.
			</tr>
	</table>
</div>
{/capture}
<script type="text/javascript" src="{$http_location}/skin1/js_script/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript" src="{$http_location}/skin1/myntra_js/jquery.min.js"></script>
{include file="dialog.tpl" title='This page is deprecated' content=$smarty.capture.dialog extra='width="100%"'}