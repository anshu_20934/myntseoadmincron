var today = new Date();
var dateOffset = (24*60*60*1000) * 7;
var lastweek = new Date();
lastweek.setTime(today.getTime() - dateOffset);
var from_date_str = (lastweek.getMonth()+1)+"/"+lastweek.getDate()+"/"+lastweek.getFullYear();
var to_date_str = (today.getMonth()+1)+"/"+today.getDate()+"/"+today.getFullYear();
Ext.onReady(function() {
	Ext.QuickTips.init();
	var myMask = new Ext.LoadMask(Ext.getBody(), {msg: "Loading..."});
	Ext.Ajax.on('beforerequest', myMask.show, myMask);
	Ext.Ajax.on('requestcomplete', myMask.hide, myMask);
	Ext.Ajax.on('requestexception', myMask.hide, myMask);
	
	var couriersStore = new Ext.data.JsonStore( {
		fields : [ 'code', 'display_name' ],
		data : couriers,
		sortInfo : {
			field : 'display_name',
			direction : 'ASC'
		}
	});
	
	var couriersField = {
    	xtype : 'combo',
    	emptyText : 'Select Courier Service',
    	store : couriersStore,
    	mode: 'local',
    	displayField : 'display_name',
    	valueField : 'code',
    	triggerAction: 'all',
    	fieldLabel : 'Shipped Via',
    	name : 'courier_service'
	};
	
	var paymentMethodsStore = new Ext.data.JsonStore( {
		fields : [ 'id', 'name' ],
		data : paymentMethods,
		sortInfo : {
			field : 'name',
			direction : 'ASC'
		}
	});
	
	var paymentMethodsField = {
    	xtype : 'combo',
    	emptyText : 'Payment Method',
    	store : paymentMethodsStore,
    	mode: 'local',
    	displayField : 'name',
    	valueField : 'id',
    	triggerAction: 'all',
    	fieldLabel : 'Payment Method',
    	name : 'payment_method',
    	value: 'all'
	};
	
	var searchPanel = new Ext.form.FormPanel({
		renderTo : 'contentPanel',
		title : 'Search Panel',
		id : 'formPanel',
		autoHeight : true,
		collapsible : true,
		border : true,
		bodyStyle : 'padding: 5px',
		width : 1000,
		items : [{
			layout : 'table',
			border : false,
			layoutConfig : {columns : 2},
			defaults : {width : 450, border : false, 
				layout : 'form', bodyStyle : 'padding-left : 15px'},
			items : [{
					items: [couriersField]
				}, {
					items: [paymentMethodsField]
				},{
					items: [{
						xtype : 'datefield',
		                name : 'from_date',
		                width : 150,
		                fieldLabel : "Shipped Date From",
		                value: from_date_str,
		                maxValue : new Date(),
		                listeners: {
		                    select: function (datefield) {
		                    	var enddatefield = searchPanel.getForm().findField("to_date");
		                    	var enddate = enddatefield.getValue();
		                    	var startdate = new Date(datefield.getValue());
		                    	if(!enddate || enddate==""){
		                    		enddate = new Date();
		                    	} else {
		                    		enddate = new Date(enddate);
		                    	}
		                    	
		                    	if( (enddate.getTime() - startdate.getTime())/(24*60*60*1000) > 31) {
		                    		enddate.setTime(startdate.getTime() + 31*24*60*60*1000);
		                    		if((enddate.getTime() - today.getTime())/(24*60*60*1000) > 0){
		                    			enddate = new Date();
		                    		}
		                    	} else if((startdate.getTime() - enddate.getTime())/(24*60*60*1000) >= 0) {
		                    		enddate.setTime(startdate.getTime() + 31*24*60*60*1000);
		                    		if((enddate.getTime() - today.getTime())/(24*60*60*1000) > 0){
		                    			enddate = new Date();
		                    		}
		                    	}
		                    	enddatefield.setValue(enddate);
		                  	}
		                }
					}]
				}, {
					items : [{
						xtype : 'datefield',
						name : 'to_date',
						width : 150,
						fieldLabel : "Shipped Date To",
						value: to_date_str,
						maxValue : new Date(),
						listeners: {
		                    select: function (datefield) {
		                    	var startdatefield = searchPanel.getForm().findField("from_date");
		                    	var startdate = startdatefield.getValue();
		                    	if(!startdate || startdate==""){
		                    		startdate = new Date();
		                    	} else {
		                    		startdate = new Date(startdate);
		                    	}
		                    	var enddate = new Date(datefield.getValue());
		                    	// if end date is selected out of range..
		                    	if( (enddate.getTime() - startdate.getTime())/(24*60*60*1000) > 31 ){
		                    		startdate.setTime(enddate.getTime() - 31*24*60*60*1000);
			                    	startdatefield.setValue(startdate);
		                    	}
		                    	if( (enddate.getTime() - startdate.getTime())/(24*60*60*1000) < 0 ){
		                    		startdate.setTime(enddate.getTime() - 31*24*60*60*1000);
			                    	startdatefield.setValue(startdate);
		                    	}
		                    }
		                }
					}]
				}, {
					items :[{
						xtype : 'textarea',
			        	id : 'orderids',
			        	fieldLabel : 'Order Ids (newline separated)',
			        	width: "100%",
			        	height: 100
					}]
				},{
					items : [{
			        	xtype : 'combo',
			        	store : new Ext.data.JsonStore({
			        		fields : [ 'id', 'display_name'],
			        		data : warehouseData,
			        		sortInfo : {
			        			field : 'display_name',
			        			direction : 'ASC'
			        		}
			        	}),
			        	emptyText : 'Select Ops Location',
			        	displayField : 'display_name',
			        	valueField : 'id',
			        	fieldLabel : 'Ops Location',
			        	name : 'warehouse_id',
			        	mode: 'local',
			        	triggerAction: 'all'
					}]
				}
			]
		}],
		buttons : [ {
			text : 'Search',
			handler : function() {
				reloadGrid();
			}
		}]
	});
	
	
	
	function getSearchPanelParams() {
		var form = searchPanel.getForm();
		
		if (form.isValid()) {
			var params = {};
			params["from_date"] = form.findField("from_date").getRawValue();
			params["to_date"] = form.findField("to_date").getRawValue();
			if(params["from_date"] == "" || params["to_date"] == "") {
				Ext.MessageBox.alert('Error', 'Date fields cannot be empty.');
				return false;
			}
			params["courier_service"] = form.findField("courier_service").getValue();
			params["payment_method"] = form.findField("payment_method").getValue();
			var orderids = form.findField("orderids").getRawValue();
			var orderIds = "";
			if(orderids != "") {
				orderids = orderids.split('\n');
				for(var i=0; i < orderids.length; i++) {
					var orderid = orderids[i].trim();	
					if(orderid != "") {
						if(orderid.replace(/[\d]/g, '') != "") {
							Ext.MessageBox.alert('Error', 'Invalid separator used between orderids.');
							return false;
						}
						orderIds += orderIds==""?"":",";
						orderIds += orderid;
					}
				}
			}
			params["orderids"] = orderIds;
			params["warehouse_id"] = form.findField("warehouse_id").getValue();
			return params;
		} else {
			Ext.MessageBox.alert('Error', 'Errors in the search form.');
			return false;
		}
	}
	
	function reloadGrid() {
		
		var params = getSearchPanelParams();
		
		if (params == false) {
			return false;
		}
		
		params["action"] = "search";
		searchResultsStore.baseParams = params;
		searchResultsStore.load();
	}
	
	var sm = new Ext.grid.CheckboxSelectionModel();
	
	var orderIdLinkRender = function(value, p, record) {
		return '<a href="' + httpLocation + '/admin/order.php?orderid=' + record.data["orderid"] + '" target="_blank">' + record.data["orderid"] + '</a>';
	}
	
	var courierRenderer = function(value, p, record) {
		var courier_code = record.data['courier_service'];
		for (var i=0; i< couriers.length; i++) {
			if (couriers[i].code == courier_code) {
				return couriers[i].display_name;
			}
		}
		return courier_code;
	}
	
	var warehouseNameRenderer = function (value, p, record) {
		for(var i in warehouseData){
			var warehouse = warehouseData[i];
			if(warehouse.id == record.data['warehouseid'])
				return warehouse.display_name;
		}
	}
	
	var colModel = new Ext.grid.ColumnModel({
		defaults: {
	        sortable: true
	    },
	    columns: [
	        sm,
	        {id : 'orderid', header : "Order ID", sortable: true, width: 75, dataIndex : 'orderid', renderer: orderIdLinkRender},
	        {id : 'login', header : "Login", sortable: true, width: 150, dataIndex : 'login'},
	        {id : 'name', header : "Name", sortable: false, width: 150, dataIndex : 'name'},
	        {id : 'shipping_address', header : "Address", sortable: false, width: 300, dataIndex : 'shipping_address'},
	        {id : 'payment_method', header : "Payment Method", sortable: false, width: 150, dataIndex : 'payment_method'},
	        {id : 'total', header : "Total", sortable: false, width: 75, dataIndex : 'total'},
	        {id : 'warehouseid', header : "Ops Location", sortable: true, width: 125, dataIndex : 'warehouseid', renderer: warehouseNameRenderer},
	        {id : 'courier_service', header : "Courier Service", sortable: false, width: 100, dataIndex : 'courier_service'},
	        {id : 'date', header : "Order Date", sortable: true, width: 125, dataIndex : 'date'},
	        {id : 'shippeddate', header : "Shipping Date", sortable: true, width: 125, dataIndex : 'shippeddate'}
	    ]
	});
		
	var searchResultsStore = new Ext.data.Store({
		url: 'update_delivered_orders.php',
		pruneModifiedRecords : true,
		reader: new Ext.data.JsonReader({
	  		root: 'results',
	  	  	totalProperty: 'count',
	  	  	fields : ['orderid', 'courier_service', 'login', 'name', 'shipping_address', 'payment_method', 'warehouseid', 'total','shippeddate','date']
		}),
		sortInfo:{field : 'orderid', direction:'ASC'},
		remoteSort: true
	});

	var pagingBar = new Ext.PagingToolbar({
		pageSize : 30,
	    store: searchResultsStore,
	    displayInfo: true
	});
	
	searchResults = new Ext.grid.EditorGridPanel({
	    store  : searchResultsStore,
	    cm : colModel,
	    id : 'searchResults-panel',
	    title : 'Search Results',
	    frame : true,
		loadMask : false,
		autoHeight : true,
		sm: sm,
		bbar : pagingBar,
		stripeRows : true,
		autoScroll : true,
		viewConfig : {
			enableRowBody : true
		},
		tbar:[{
			text : '<b>Mark Delivered</b>',
			iconCls : 'add',
			handler : function() {
        		var selections = searchResults.initialConfig.sm.selections.items;
        		
        		if (selections.length == 0) {
        			Ext.MessageBox.alert('Error', 'No orders selected to assign');
        			return false;
        		}
        		
        		var orderIds = "";
        		for (var i = 0; i < selections.length; i++) {
        			orderIds += orderIds==""?"":",";
        			orderIds += selections[i].data.orderid;
        		}
        		
        		var params = getSearchPanelParams();
        		Ext.Ajax.request({
					url : 'update_delivered_orders.php',
					params : {"orderids" : orderIds, "action" : "markdelivered", "user": loginUser},
				    success : function(response, action) {
						reloadGrid();
					}
				});
        	}
		}]
	});
	
	var mainPanel = new Ext.Panel({
		renderTo : 'contentPanel',
		border : false,
		items : [
		     searchPanel, searchResults
		]
	});
});


function showUploadStatusWindow() {
	$("#uploadstatuspopup").css("display","block");
	$("#uploadstatuspopup-content").css("display","block");
	$("#uploadstatuspopup_title").css("display","block");
	
	var oThis = $("#uploadstatuspopup-main");
	var topposition = ( $(window).height() - oThis.height() ) / 2;
	if ( topposition < 10 ) 
		topposition = 10;
	oThis.css("top", ( topposition + $(window).scrollTop() + "px"));
	oThis.css("left", ( $(window).width() - oThis.width() ) / 2+$(window).scrollLeft() + "px");

	setInterval(trackStatus, 1000);

	return true;
}

function trackStatus() {
	$.get("getprogressinfo.php?progresskey="+progressKey,
		function(percent) {
			if(percent != "false") {
				$("#progress_bar").css('width', percent+"%");
				$("#progress_text").html(percent);
			}
		}
	);
}

