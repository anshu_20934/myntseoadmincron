{* $Id: order_delay_notifications.tpl,v 1.24.2.1 2008/08/21 11:02:40 max Exp $ *}
{include file="main/include_js.tpl" src="main/popup_product.js" src="main/overlib.js"}
{popup_init src="../skin1/main/overlib.js"}
<a name="featured" />

{$lng.txt_featured_products}

<br /><br />

{capture name=dialog}

<table width="100%"  border="0" cellspacing="0" cellpadding="0">

<tr>
  <td valign="top">
  		<table width="98%"  border="0" align="center" cellpadding="0" cellspacing="0">
		<tr>
		 <td>
			<table width="100%" height="21" border="0" cellpadding="0" cellspacing="0" class="textBlack">

				<tr>
					<td><strong>Delay Notifications Log for orderid {$orderid}</strong></td>
				</tr>
			</table>
			<br>
			<table width="100%" height="25" border="1" cellpadding="0" cellspacing="0">
				<tr bgcolor="#ccffff">
					
					<th style="font-size:14px;">Notification ID</th>
					<th style="font-size:14px;">Delay Reason</th>
					<th style="font-size:14px;">Expected Delivery Date</th>
					<th style="font-size:14px;">Action Needed</th>
					
				</tr>
				{foreach from=$delaynotifications item=delaynotification}
				<tr bgcolor="{cycle values="#eeddff,#d0ddff"}">					
					<td style="font-size:12px;">{$delaynotification.notificationid}</td>
					<td><textarea name="query" cols=45 rows=5 readonly>{$delaynotification.delay_reason}</textarea></td>
					<td style="font-size:12px;">{$delaynotification.expected_date}</td>
			        <td><textarea name="query" cols=45 rows=5 readonly>{$delaynotification.action_needed}</textarea></td>

				</tr>				
                {/foreach}    
			</table>
		</table>
</table>
{/capture}
{include file="dialog.tpl" title="Order Delay Notification" content=$smarty.capture.dialog extra='width="100%"'}



