{* $Id: featured_products.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
{include file="main/include_js.tpl" src="main/popup_product.js"}
{capture name=dialog}
<form name="ordersearch" action="orders_search.php" method="GET">
	<input type="hidden" name="mode" id="mode" value="search"/>
	<input type="hidden" name="type" id="type" value="{$smarty.get.type}"/>

		<div class="ITEM_PARENT_DIV">
			<div class="ITEM_FIRST_CHILD_DIV">
				<div class="ITEM_LABLE_DIV">Order Status</div>
				<div class="ITEM_FIELD_DIV" >
					{if $smarty.post.order_status}
		    	        {assign var=ordstat value=$smarty.post.order_status.0}
	    	    	{elseif $smarty.get.order_status}
	    	    	    {assign var=ordstat value=$smarty.get.order_status.0}
			        {/if}
					{include file='main/order_status.tpl' name="order_status" mode="select" extended='Y' status=$ordstat}
				</div>
			</div>
		
		<div class="ITEM_FIRST_CHILD_DIV">
	        <div class="ITEM_LABLE_DIV">Operations Location</div>
	        <div class="ITEM_FIELD_DIV">
	            {if $ops_location}
	                {assign var=warehouse_id value=$smarty.get.ops_location}
	            {elseif $smarty.get.ops_location}
	                {assign var=warehouse_id value=$smarty.get.ops_location}
	            {/if}
	            <select name="ops_location" style="width:140px;">
	            	<option value="">--select ops location--</option>
	                {foreach from=$ops_locations item=item key=key}
	                	<option value="{$item.id}" {if $warehouse_id eq $item.id} selected {/if}>{$item.display_name}</option>                    
	               	{/foreach}
				</select>
	        </div>  
        </div>        
		  
		<div class="ITEM_FIRST_CHILD_DIV">
        <div class="ITEM_LABLE_DIV">Source Name</div>
        <div class="ITEM_FIELD_DIV">
        	{if $smarty.post.sourceid}
                {assign var=s_id_array value=$smarty.post.sourceid}
            {elseif $smarty.get.sourceid}
                {assign var=s_id_array value=$smarty.get.sourceid}
            {/if}
            <span id="source_span">
	            <select name="sourceid[]" size="4" style="width:200px;" multiple>
	            	<option value="">--select source name/s--</option>
		           	{foreach from=$order_sources item=item key=key}
		            	{foreach from=$s_id_array item=s_id}	            		
		               		{if $s_id == $item.source_id}
		            			{assign var=source_selected value="Y"}
		            		{/if}     
		            	{/foreach}
		                <option value="{$item.source_id}" {if $source_selected == "Y"} selected {/if}>{$item.source_name} ({$item.source_code})</option>
		                {assign var=source_selected value="" }
		           	{/foreach}
				</select>
			</span>
        </div>  
        </div>        
        </div>
        
{* Row 2 of Inputs *}	        

        <div class="ITEM_PARENT_DIV">
        
        <div class="ITEM_FIRST_CHILD_DIV">
        <div class="ITEM_LABLE_DIV">Order Id</div>
        <div class="ITEM_FIELD_DIV">
            <input type="text" name="orderid" id="orderid" {if $smarty.post.orderid !=""} value = {$smarty.post.orderid} {elseif  $smarty.get.orderid !=""} value = {$smarty.get.orderid} {/if}>
        </div>  
        </div>	
        
        <div class="ITEM_FIRST_CHILD_DIV">
        <div class="ITEM_LABLE_DIV">Email ID</div>
        <div class="ITEM_FIELD_DIV">
                {if $smarty.post.order_login}
                    {assign var=o_login value=$smarty.post.order_login}
                {elseif $smarty.get.order_login}
                    {assign var=o_login value=$smarty.get.order_login}
                {/if}
        		<input type="text" name="order_login" value="{$o_login}"/>
        </div>  
        </div>
        
        <div class="ITEM_FIRST_CHILD_DIV">
        <div class="ITEM_LABLE_DIV">User Name</div>
        <div class="ITEM_FIELD_DIV" >
            {if $smarty.post.ship_name}
                {assign var=s_name value=$smarty.post.ship_name}
            {elseif $smarty.get.ship_name}
                {assign var=s_name value=$smarty.get.ship_name}
            {/if}
            <input type="text" name="ship_name" value="{$s_name}"/>
        </div>
        </div>
        </div>
        <div class="ITEM_SEARCH_BUTTON_DIV">
        <input type="submit" value="{$lng.lbl_search|escape}" style="margin-left:400px;"/>
        </div>          
</form>


{/capture}
{include file="dialog.tpl" title="Order Search" content=$smarty.capture.dialog extra='width="100%"'}
<br /><br />
