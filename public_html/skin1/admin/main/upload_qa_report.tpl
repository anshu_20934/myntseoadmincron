{* $Id: featured_products.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
{include file="main/include_js.tpl" src="main/popup_product.js"}
<base href="{$http_location}"> 
<link rel="stylesheet" href="{$SkinDir}/skin1_admin_operations.css" />

{literal}
<script Language="JavaScript" Type="text/javascript">

		function QAFormValidator(theform)
		{
         var qaResult=theform.qaResult;         
          var reject_comment;
		  if (qaResult.value!="pass")
		  {
            var reject_reason = prompt("Enter reason for Rejecting item","");
            if (reject_reason == null ){ // Cancel pressed
               return false;
            }
            else if (reject_reason == "" || reject_reason == " "){ // OK pressed but no text
                 alert("Valid reason not entered. Please try again.");
                 return false;
            }
            else {
                var reject_text = document.getElementById("reject_reason");
                reject_text.value=reject_reason;
            }
		  }
          return true;

		}
		</script>
{/literal}

{capture name=dialog}
	<div id="errorMsg" style="color:red; font-weight: bold; font-size: 12px;">{$errorMsg}</div>
	<br>
	<form action="" method="post" name="upload_qa_report_search" enctype="multipart/form-data" >
		<input type='hidden' name='action' value='getData'>
	    Order ID: <input type="text" name="orderid" id="orderid" value="{$orderid}">
	    Item ID: <input type="text" name="itemid" id="itemid" value="{$itemid}">
	    <input type='submit' name='search' value="Search">
	</form>
{/capture}
{include file="dialog.tpl" title="Ops Done Item Search" content=$smarty.capture.dialog extra='width="100%"'}
<br><br>

{if $action eq "showData"}
	{if $rts_info neq ""}
    	{capture name=dialog}
	    	{assign var=lastitem value="0"}
	   		<p><font color="7D0552"><b>ORDER READY TO SHIP. PLEASE PUT ORDER IN PACKING AREA.</b></font></p>
	    	<table>
	      		{section name=index loop=$rts_info}
	      		{if $rts_info[index].item_id != $lastitem}
		      		<tr>
			        	<td width=100>Order # {$rts_info[index].orderid}</td>
			        	<td width=100>Item # {$rts_info[index].item_id}</td>
			        	<td width=200>{$rts_info[index].item_name}</td>
			        	<td width=50>{$rts_info[index].item_count}</td>
			        	<td><img src="{$rts_info[index].image}" width='100' /> </td>
		      		</tr>
	      			{assign var=lastitem  value=$rts_info[index].item_id}
	      		{/if}
	     		{/section}
	    	</table>
    	{/capture}
    	{include file="dialog.tpl" title="Ready To Ship Order!" content=$smarty.capture.dialog extra='width="100%"'}
    	<br><br>
    {/if}
    {capture name=dialog}
    	<p></p>
        <p></P>
        <table cellpadding="2" cellspacing="1" width="100%" >
        	{assign var="colspan" value=6}
        	<tr class="TableHead">
	            <td width="2%">Select QA Pass/Fail</td>
				<td width="5%">Submit </td>
	            <!--  
	            <td width="5%"><a href="upload_qa_report.php?order_by=itemid{if $smarty.get.sort_order=='desc' && $smarty.get.order_by=='itemid'}&amp;sort_order=asc{else}&amp;sort_order=desc{/if}">Item Details{if $smarty.get.sort_order=='desc' && $smarty.get.order_by=='itemid'}&uArr;{elseif $smarty.get.sort_order=='asc' && $smarty.get.order_by=='itemid'}&dArr;{else}{/if}</a></td>
	            <td width="5%"> <a href="upload_qa_report.php?order_by=orderid{if $smarty.get.sort_order=='desc' && $smarty.get.order_by=='orderid'}&amp;sort_order=asc{else}&amp;sort_order=desc{/if}">order id{if $smarty.get.sort_order=='desc' && $smarty.get.order_by=='orderid'}&uArr;{elseif $smarty.get.sort_order=='asc' && $smarty.get.order_by=='orderid'}&dArr;{else}{/if}</a></td> 
	            -->
	            <td width="5%">Item Details</td>
	            <td width="5%">Order Id</td> 
				<td width="15%">Item Style Name</td>
				<td width="15%">Article Id</td>
	            <!-- 
	            <td width="2%"> <a href="upload_qa_report.php?order_by=amount{if $smarty.get.sort_order=='desc' && $smarty.get.order_by=='amount'}&amp;sort_order=asc{else}&amp;sort_order=desc{/if}">Quantity{if $smarty.get.sort_order=='desc' && $smarty.get.order_by=='amount'}&uArr;{elseif $smarty.get.sort_order=='asc' && $smarty.get.order_by=='amount'}&dArr;{else}{/if}</a></td>
	            <td width="5%"> <a href="upload_qa_report.php?order_by=assignee{if $smarty.get.sort_order=='desc' && $smarty.get.order_by=='assignee'}&amp;sort_order=asc{else}&amp;sort_order=desc{/if}">Assignee{if $smarty.get.sort_order=='desc' && $smarty.get.order_by=='assignee'}&uArr;{elseif $smarty.get.sort_order=='asc' && $smarty.get.order_by=='assignee'}&dArr;{else}{/if}</a></td>
				-->
				<td width="2%">Quantity</td>
				<td width="5%">Assignee</td>
				<td width="10%">Quantity Breakup</td>
				<td width="5%">ExtraData </td>
        	</tr>
        	{if $opsdoneItems}
		        {section name=index loop=$opsdoneItems}
		
		        {assign var=amount value=$opsdoneItems[index].amount}
		        {assign var=itemid value=$opsdoneItems[index].itemid}
				{assign var=productid value=$opsdoneItems[index].productid}
				{assign var=name value=$opsdoneItems[index].name}
				{assign var=article_number value=$opsdoneItems[index].article_number}
				{assign var=breakup value=$opsdoneItems[index].quantity_breakup}
				{assign var=extra_data value=$opsdoneItems[index].extra_data}
				{assign var=orderid value=$opsdoneItems[index].orderid}
				{assign var=group_id value=$opsdoneItems[index].group_id}        
				{assign var=assigneeName value=$opsdoneItems[index].assigneeName}


    			<form action="" method="post" name="upload_qa_report" onSubmit='return QAFormValidator(this)' enctype="multipart/form-data" >
			        <input type='hidden' name='action' value='qaUpdate'>
			        <input type='hidden' name='orderid' value={$orderid}>
			        <input type='hidden' name='reject_reason' id="reject_reason" value="">
			        <input type='hidden' name='is_customizable' id="is_customizable" value="{$opsdoneItems[index].is_customizable}">

        			<tr{cycle values=", class='TableSubHead'"}>
          				<input type='hidden' name='itemid' value={$itemid}>
          					<td>
					        	<select name="qaResult" id="qaResult_{$itemid}"  >
					               <option value="pass">QA Pass</option>
					               <option value="QAFail: Apparel : T-shirt Size">QAFail: Apparel : T-shirt Size</option>
					               <option value="QAFail: Apparel : T-shirt Print not matching">QAFail: Apparel : T-shirt Print not matching</option>
					               <option value="QAFail: Apparel : T-shirt Stained">QAFail: Apparel : T-shirt Stained</option>
					               <option value="QAFail: Apparel : T-shirt Torn">QAFail: Apparel : T-shirt Torn</option>
					               <option value="QAFail: Apparel : T-shirt Brandname">QAFail: Apparel : T-shirt Brandname</option>
					               <option value="QAFail: Non-Apparel : Print not matching">QAFail: Non-Apparel : Print not matching</option>
					               <option value="QAFail: Non-Apparel : Material damaged">QAFail: Non-Apparel : Material damaged</option>
					               <option value="QAFail: OTHER">QAFail: OTHER</option>
					           	</select>
          					</td>

							<td><input type='submit' name='update'  value='update' /></td>
							<td>
            					{if $doneItems[index].is_customizable eq '0'}{$itemid}
            					{else}
            						<a href="#" onclick="window.open('view_print_design.php?productId={$productid}&orderid={$orderid}', null, 'height=500,width=700,status=yes,toolbar=no,menubar=no,location=no,resizable=1,scrollbars=1');">{$itemid}</a>
            					{/if}
          					</td>		  
          					<td><a href="order.php?orderid={$orderid}">#{$orderid}</a></td>
		  					<td>{$name}</td>
		  					<td>{$article_number}</td>
          					<td>{$amount}</td>
				          	<td>{$assigneeName}</td>
						  	<td>{$breakup}</td>
		  					<td>
		  						{if $doneItems[index].is_customizable eq '0'}
          		  					<span style="background-color: red; font-weight: bold; padding-left: 4px; padding-right: 4px;">NP</span>
          		  					<br>
          		  					<a href="#" onclick="window.open('imagePreview.php?style_id={$doneItems[index].product_style}', null, 'height=500,width=700,status=yes,toolbar=no,menubar=no,location=no,resizable=1,scrollbars=1');">Preview Image</a>

		  						{else}
		  							{$extra_data}
		  						{/if}
		  					</td>
        				</tr>
    				</form>
        		{/section}
        	{/if}
			<tr></tr>
		</table>
    {/capture}
    {include file="dialog.tpl" title="QA Check Form" content=$smarty.capture.dialog extra='width="100%"'}
    <br><br>
    {capture name=dialog}
	    <form action="" method="post" name="upload_qa_report" enctype="multipart/form-data" >
	    	{if $message}<font color='red'>{$message}</font>{/if}
	    	<label>Upload the QA csv file:</label> <input type="file" name="qareport" >
	       	<input type='hidden' name='action' value='csvupload'>
	        <input type='submit'  name='uploadxls'  value="Upload">
	    </form>
    {/capture}
    {include file="dialog.tpl" title="Upload QA csv" content=$smarty.capture.dialog extra='width="100%"'}
    
{else if $action eq "confirmUpload"}
	{capture name=dialog}
		<B><P> Upload Status<P></B>
		<p></p>
		<P>{$message}</P>
	{/capture}
    {include file="dialog.tpl" title="QA Check Report" content=$smarty.capture.dialog extra='width="100%"'}
{/if}

<script type="text/javascript" language="JavaScript">
document.forms['upload_qa_report_search'].elements['orderid'].focus();
</script>