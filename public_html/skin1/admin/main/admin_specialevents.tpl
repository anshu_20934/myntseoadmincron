{* $Id: featured_products.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
{include file="main/include_js.tpl" src="main/popup_product.js"}

<a name="featured" />

{$lng.txt_featured_products}

<br /><br />

{capture name=dialog}


<form action="special_events_action.php" method="post" name="specialeventsform">
<input type="hidden" name="mode" />


<table cellpadding="3" cellspacing="1" width="100%">

<tr class="TableHead">
	<td width="5%">&nbsp;</td>
	<td width="5%">EVENT ID</td>
	<td width="30%" align="center">EVENT NAME</td>
	<td width="25%" align="center">EVENT PRODUCT HEADING</td>
	<td width="25%" align="center">EVENT TEMPLATE HEADING</td>
</tr>

{if $specialevents }

{section name=idx loop=$specialevents}

<tr{cycle values=", class='TableSubHead'"}>
	<td style="left-padding:10px"><input type="checkbox" name="posted_data[{$specialevents[idx].event_id}][delete]"   /></td>
	<td align="center" style="left-padding:30px"><b>{$specialevents[idx].event_id}</b></td>
	<td align="center"><b>{$specialevents[idx].event_name}</b></td>
	<td align="center"><b>{$specialevents[idx].event_products_heading}</b></td>
	<td align="center"><b>{$specialevents[idx].event_template_heading}</b></td>
	 
</tr>

{/section}

<tr>
	<td colspan="5" class="SubmitBox">
	<input type="button" value="{$lng.lbl_delete_selected|escape}" onclick="javascript: submitForm(this,'delete');" />
	<input type="submit" value="{$lng.lbl_add|escape}"  onclick="javascript: submitForm(this, 'add');"/>
    <input type="submit" value="{$lng.lbl_modify_selected|escape}" onclick="javascript: submitForm(this, 'modify');" />
	</td>
</tr>


{else}

<tr>
<td colspan="5" align="center">No Special Events Found</td>
</tr>

<tr>
<td colspan="5" align="left"><input type="submit" value="{$lng.lbl_add|escape}"  onclick="javascript: submitForm(this, 'add');"/></td>
</tr>



{/if}



</table>
</form>

{/capture}
{include file="dialog.tpl" title='Special Events List' content=$smarty.capture.dialog extra='width="100%"'}
<br /><br />

