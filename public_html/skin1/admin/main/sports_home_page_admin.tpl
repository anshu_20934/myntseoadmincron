{* $Id: featured_products.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
{include file="main/include_js.tpl" src="main/popup_product.js"}
<a name="featured" />
{literal}
<style type="text/css">
  .homepage input, select {width: 400px}
</style>
{/literal}
{$lng.txt_featured_products}

<br /><br />
<br /><br />
<script type="text/javascript" src="{$http_location}/skin1/js_script/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
{literal}

<script type="text/javascript">

	tinyMCE.init({

		// General options

		mode : "textareas",

		editor_selector : "mceEditor",

		theme : "advanced",

		plugins : "safari,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,inlinepopups",



		// Theme options

// Theme options

		theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect",

		theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",

		theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",

		theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak",




		theme_advanced_toolbar_location : "top",

		theme_advanced_toolbar_align : "left",

		theme_advanced_statusbar_location : "bottom",

		theme_advanced_resizing : true,



		// Example content CSS (should be your site CSS)

		content_css : "css/content.css",



		// Drop lists for link/image/media/template dialogs

		template_external_list_url : "lists/template_list.js",

		external_link_list_url : "lists/link_list.js",

		external_image_list_url : "lists/image_list.js",

		media_external_list_url : "lists/media_list.js",



		// Replace values for the template plugin



	});


</script>
{/literal}
{capture name=dialog}

<form action="{$form_action}" method="post" name="home_page_form" enctype="multipart/form-data">
<input type="hidden" name="mode" value="save" />

<table cellpadding="3" cellspacing="1" width="100%" class="homepage">
<tr>
    <td>Page Name</td>
	<td><input type="text" name="page_name"  value="{if $page_name} {$page_name}{/if}"/></td>
</tr>
<tr>
    <input type="hidden" name="id" value="{if $details[0].id} {$details[0].id}{/if}" />
    <td>Page Title</td>
	<td><input type="text" name="page_title"  value="{if $details[0].page_title} {$details[0].page_title}{/if}"/></td>
</tr>
<tr>
    <td>H1 Title</td>
	<td><input type="text" name="h1_title"  value="{if $details[0].h1_title} {$details[0].h1_title}{/if}"/></td>
</tr>
<tr>
<tr>
    <td>Top Banner</td>
	<td>
	    <img src="{if $details[0].top_banner} {$details[0].top_banner}{/if}" />
	    <input type="file" name="top_banner" />
	</td>
</tr>

<tr>
    <td>Top Banner Url</td>
	<td><input type="text" name="top_banner_url"  value="{if $details[0].top_banner_url} {$details[0].top_banner_url}{/if}"/></td>
</tr>

<tr>
    <td>Meta Keywords</td>
	<td><input type="text" name="meta_keywords"  value="{if $details[0].meta_keywords} {$details[0].meta_keywords}{/if}"/></td>
</tr>

<tr>
    <td>Meta Description</td>
	<td><input type="text" name="meta_description"  value="{if $details[0].meta_description} {$details[0].meta_description}{/if}"/></td>
</tr>

<!--tr>
    <td>Filter Groups to show in Left Panel </td>
	<td>
        <select name="left_panel_filter_groups[]" class="" multiple="multiple">

           <option value="">None</option>
           {section name=n loop=$groups}
                {assign var=is_selected value="notselected"}
                {if $selected_left_panel_filter_groups}
                    {section name=inner loop=$selected_left_panel_filter_groups}
                        {if $selected_left_panel_filter_groups[inner] eq $groups[n].id}
                            {assign var=is_selected value="selected"}
                        {/if}
                    {/section}
                {/if}
                <option value="{$groups[n].id}" {if $is_selected eq 'selected'}selected{/if} >{$groups[n].group_name}</option>
           {/section}
        </select>
	</td>
</tr-->
<tr>
    <td>Bottom Text Heading</td>
	<td><input type="text" name="bottom_text_heading"  value="{if $details[0].bottom_text_heading} {$details[0].bottom_text_heading }{/if}"/></td>
</tr>

<tr>
    <td>Bottom text</td>
	
	<td><textarea name="bottom_text" cols="50" rows="5" class="mceEditor" >{if $details[0].bottom_text} {$details[0].bottom_text}{/if}</textarea></td>
</tr>


<tr>

	<td colspan="5" class="SubmitBox">
	<input type="submit" value="{$lng.lbl_update|escape}" onclick="document.home_page_form.submit();" />

	</td>
</tr>


</table>
</form>
{/capture}
{include file="dialog.tpl" title="Sports Home Page Details" content=$smarty.capture.dialog extra='width="100%"'}
<br /><br />


