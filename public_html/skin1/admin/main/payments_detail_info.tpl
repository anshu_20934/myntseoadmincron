<br/>
<br/>
{capture name=dialog}

<table width="100%"  border="0" cellspacing="0" cellpadding="0">

<tr>
  <td valign="top">
  	<table width="98%"  border="0" align="center" cellpadding="0" cellspacing="0">
	<tr>
		 <td>
			<table width="100%" height="21" border="0" cellpadding="0" cellspacing="0" class="textBlack">
				<tr>
					<td><strong>Payment gateway details for orderid {$orderid}</strong></td>
				</tr>
			</table>
			<br>
			<table width="100%" height="25" border="1" cellpadding="0" cellspacing="0">
				<tr bgcolor="#ccffff">
					<th style="font-size:14px;">Gateway</th>
					<th style="font-size:14px;">Browser</th>
					<th style="font-size:14px;">IP Address</th>
					<th style="font-size:14px;">Pmt Option</th>
					<th style="font-size:14px;">Pmt Type</th>
					<th style="font-size:14px;">TrnsID</th>
					<th style="font-size:14px;">PmtID</th>
					<th style="font-size:14px;">InsertTime</th>
					<th style="font-size:14px;">ReturnTime</th>
					<th style="font-size:14px;">Trans Time</th>
					<th style="font-size:14px;">Flag</th>
					<th style="font-size:14px;">Tamper</th>
                    <th style="font-size:14px;">Amt Rcvd</th>
                    <th style="font-size:14px;">Rsp Code</th>
					<th style="font-size:14px;">Rsp Mssg</th>					
					<th style="font-size:14px;">Complete Rsp</th>
					<th style="font-size:14px;">Card Bank Name</th>
					<th style="font-size:14px;">Card Bin Number</th>
				</tr>
				<tr bgcolor="{cycle values="#eeddff,#d0ddff"}">					
					<td style="font-size:12px;">{$orderPaymentLog.payment_gateway_name}&nbsp;</td>
					<td><textarea name="query" cols=15 rows=3 readonly>{$orderPaymentLog.user_agent}</textarea></td>
					<td style="font-size:12px;">{$orderPaymentLog.ip_address}&nbsp;</td>
					<td style="font-size:12px;">{$orderPaymentLog.payment_option}&nbsp;</td>
					<td style="font-size:12px;">{$orderPaymentLog.payment_issuer}&nbsp;</td>
	 				<td style="font-size:12px;">{$orderPaymentLog.bank_transaction_id}&nbsp;</td>
	 				<td style="font-size:12px;">{$orderPaymentLog.gateway_payment_id}&nbsp;</td>
	 				<td style="font-size:12px;">{$orderPaymentLog.time_insert|date_format:"%d/%b/%Y, %H:%M:%S"}&nbsp;</td>
	 				<td style="font-size:12px;">{$orderPaymentLog.time_return|date_format:"%d/%b/%Y, %H:%M:%S"}&nbsp;</td>
					<td style="font-size:12px;">{$orderPaymentLog.time_transaction|date_format:"%d/%b/%Y, %H:%M:%S"}&nbsp;</td>
					<td style="font-size:12px;">{if isset($orderPaymentLog.is_flagged) }{if $orderPaymentLog.is_flagged eq 1 } YES {else} NO{/if}{else}NA{/if}&nbsp;</td>
					<td style="font-size:12px;">{if isset($orderPaymentLog.is_tampered)}{if $orderPaymentLog.is_tampered eq 1} YES {else} NO{/if}{else}NA{/if}&nbsp;</td>
					<td style="font-size:12px;">{$orderPaymentLog.amountPaid}&nbsp;</td>
					<td style="font-size:12px;">{$orderPaymentLog.response_code}&nbsp;</td>
                    <td><textarea name="query" cols=15 rows=3 readonly>{$orderPaymentLog.response_message}</textarea></td>	
                    <td><textarea name="query" cols=15 rows=3 readonly>{$orderPaymentLog.gatewayResponse}</textarea></td>
                    <td style="font-size:12px;">{$orderPaymentLog.card_bank_name}&nbsp;</td>
					<td style="font-size:12px;">{$orderPaymentLog.bin_number}&nbsp;</td>                    
				</tr>    
			</table>
		</table>
	</td>
</tr>
</table>

{/capture}
{include file="dialog.tpl" title="Order Payment Logs" content=$smarty.capture.dialog extra='width="100%"'}