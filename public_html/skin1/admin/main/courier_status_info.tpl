{* $Id: courier_serviceability.tpl,v 1.83.2.6 2006/06/27 08:08:06 max Exp $ *}
{include file="page_title.tpl" title='Courier Status Info'}
<br><br>
{capture name=dialog}
	{if $courier_msg } <div style="font-size: 12px; font-weight: bold; color: green;">{$courier_msg}</div>{/if}
	<form action="" method="post" name="courier_info" id="courier_info" enctype="multipart/form-data">
		<input type=hidden name="action" value="update_courier_info">
		<table border=0 cellspacing=0 cellpadding=0 width=100%>
			<tr class="tablehead">
				<td><b>Courier Service</b></td>
				<td><b>Delivery Mode</b></td>
				<td><b>Pickup Mode</b></td>
			</tr>
			{section name=idx loop=$all_couriers}
				<tr>
					<td align=center>{$all_couriers[idx].display_name}</td>
					<td align=center>
						<select name="enable_status_{$all_couriers[idx].code}" style="padding: 2px; border: #CCCCCC 1px solid; width: 150px;">
							<option value="1" {if $all_couriers[idx].enable_status eq 1}selected{/if}>Enabled</option>
							<option value="0" {if $all_couriers[idx].enable_status eq 0}selected{/if}>Disabled</option>
						</select>
					</td>
					<td align=center>
						<select name="pickup_status_{$all_couriers[idx].code}" style="padding: 2px; border: #CCCCCC 1px solid; width: 150px;">
							<option value="1" {if $all_couriers[idx].return_supported eq 1}selected{/if}>Enabled</option>
							<option value="0" {if $all_couriers[idx].return_supported eq 0}selected{/if}>Disabled</option>
						</select>
					</td>
				</tr>
	  		{/section}
			<tr><td colspan=3 align=right><input type="submit" name="update_courier_info-btn" value="Update Changes" style="padding:5px;"></td></tr>
		</table>
	</form>
{/capture}
{include file="dialog.tpl" title='Courier Status Info' content=$smarty.capture.dialog extra='width="100%"'}