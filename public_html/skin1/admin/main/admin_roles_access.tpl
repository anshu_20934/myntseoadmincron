{*include file="main/include_js.tpl" src="main/popup_product.js"*}

{literal}
<script type="text/javascript">
function delete_roleaccesspair(id){
    var r=confirm("Are you sure you want to delete this role access pair");
    if(r == true){
        document.updateroleaccesspairs.mode.value = 'delete';
        document.updateroleaccesspairs.roleaccess_id.value = id;
        document.updateroleaccesspairs.submit();
    }
}

function add_roleaccesspair(){
	if($('.access_new').val().trim() ==''){
        alert('Page address cannot be empty');
        return;
    }
    document.updateroleaccesspairs.mode.value = 'add';
    document.updateroleaccesspairs.submit();
}

function update_roleaccesspair(){
	var updatetype = true;
	$('textarea').each(function() {
		if(!($(this).hasClass("access_new") || $(this).hasClass("extra_logins_allowed_tarea"))){
			if($(this).val().trim() == ''){
				updatetype = false;
				return;
			}
		}
		
	});
	if(updatetype){		
		document.updateroleaccesspairs.mode.value = 'update';
		document.updateroleaccesspairs.submit();
	}
	else {
		alert('Page address cannot be empty');
		return
	}
}
</script>
{/literal}
{capture name=dialog}
{if $roletypes}
<form action="admin_roles_access.php" method="post" name="updateroleaccesspairs">
<input type="hidden" name="mode" />
<input type="hidden" name="roleaccess_id" />
<table cellpadding="3" cellspacing="1" width="100%">
<thead>
<tr class="TableHead">
	<td>Update</td>
	<td>Page Address</td>
	{section name=role_num loop=$roletypes}
	<td>{$roletypes[role_num].name}</td>
	{/section}
	<td>Extra Logins Allowed</td>
</tr>
</thead>
<tbody>
{if $roleaccesspairs}
{section name=key_num loop=$roleaccesspairs}
<tr>
	<td style="text-align: center; vertical-align: middle;"> <input type="checkbox" name="update_id[]" value="{$roleaccesspairs[key_num].id}"></td>
	<td align="center" style="left-padding:30px"><textarea name="update_access_field[{$roleaccesspairs[key_num].id}]">{$roleaccesspairs[key_num].pagename}</textarea></td>
	{*Adds the checkbox for setting the role types*}
	{section name=role_num loop=$roletypes}
	{if $pageroleaccess[key_num][role_num] eq 1}
	
	<td style="text-align: center; vertical-align: middle;"> <input type="checkbox" name="roletypes[{$roleaccesspairs[key_num].id}][]" value="{$roletypes[role_num].type}" checked="yes"></td>
	
	{else}
	<td style="text-align: center; vertical-align: middle;"> <input type="checkbox" name="roletypes[{$roleaccesspairs[key_num].id}][]" value="{$roletypes[role_num].type}" ></td>
	{/if}
	{/section}
	<td align="center" style="left-padding:30px"><textarea name="update_extra_logins_allowed[{$roleaccesspairs[key_num].id}]" class="extra_logins_allowed_tarea">{$roleaccesspairs[key_num].extra_logins_allowed}</textarea></td>
	<td align="center"> <input type="button" value="Delete" onclick="javascript: delete_roleaccesspair('{$roleaccesspairs[key_num].id}')" /> </td>	
</tr>
{/section}
{else}
<tr>
	<td colspan="2" align="center">No Page access pairs added as yet</td>
</tr>
{/if}
<tr>
	<td></td>
	<td></td>
	{section name=role_num loop=$roletypes}
	<td></td>
	{/section}
	<td class="SubmitBox" align="right">
	   <input type="button" value="{$lng.lbl_upd_selected|escape}" onclick="javascript: update_roleaccesspair()" />
	</td>
	<td></td>
</tr>
</tbody>
</table>


<h2>Add new page and define its role access: </h2>
<table>
<tr class="TableHead">
	<td>Page Address</td>
	{section name=role_num loop=$roletypes}
	<td>{$roletypes[role_num].name}</td>
	{/section}
	<td>Extra Logins Allowed</td> 
</tr>

<tr>
	<td align="center" style="left-padding:30px"><textarea name="access_field_new" class="access_new"></textarea></td>
	<!-- td align="center" style="left-padding:30px"><input name="role_field_new" type="text" value="" /></td> -->
	{*Adds the checkbox for setting the role types*}
	{section name=role_num loop=$roletypes}
	<td style="text-align: center; vertical-align: middle;"> <input type="checkbox" name="access_roletype_new[]" value="{$roletypes[role_num].type}" ></td>
	{/section}
	<td align="center" style="left-padding:30px"><textarea name="extra_logins_allowed_new" class="extra_logins_allowed_tarea"></textarea></td>
	<td align="center">
		<input type="button" value="Add" onclick="javascript: add_roleaccesspair()" />
	</td>
</tr>
</table>
</form>
{else}
There are no role types.
{/if}
{/capture}
{include file="dialog.tpl" title="Page access-Role type Pairs" content=$smarty.capture.dialog extra='width="100%"'}