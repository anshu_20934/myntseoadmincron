{* $Id: featured_products.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
{include file="main/include_js.tpl" src="main/popup_product.js"}
<link rel="stylesheet" href="{$SkinDir}/skin1_admin_operations.css" />
<script type="text/javascript" src="{$http_location}/skin1/js_script/jquery.js"></script>

{literal}
<script Language="JavaScript" Type="text/javascript">
function checkAction(){

   if(document.getElementById("action")!==null && document.getElementById("action").value=="queue"){
     if(!confirm("Please confirm if you have verified the address and the phone number of the Customer. \nCOD Orders can be queued only after verification"))
         return false;
   }
   if(document.getElementById("resendaction")!=null &&document.getElementById("resendaction").value=="resend"){
     if(!confirm("Please confirm if you have verified the address and the phone number of the Customer again. \n(This order has failed delivery once)"))
         return false;
   }
}
</script>
{/literal}
{capture name=dialog}
{capture name=dialog}

<script type="text/javascript" src="{$SkinDir}/js_script/calender.js" ></script>
<link rel="stylesheet" href="{$SkinDir}/css/calender.css" type="text/css" media="screen" />
<form name="codReportForm" action="cod_order_report.php" method="POST">
	<input type="hidden" name="mode" id="mode" value="search"/>
	<table width="100%" cellspacing="5" cellpadding="1">

<tbody>
<tr>
	<td nowrap="nowrap" class="FormButton">Order Date Period:</td>
	<td width="10">&nbsp;</td>
	<td class="FormButton">
	Start Date::<input type="text" name="orderStartDate" id="orderStartDate" value="{ $post_data.orderStartDate}"/>&nbsp;
	<img border="0" onclick="displayDatePicker('orderStartDate');" alt="Click Here to Pick up the date" src="{$cdn_base}/images/cal.gif">&nbsp;
	End Date::<input type="text" name="orderEndDate"id="orderEndDate" value="{ $post_data.orderEndDate}"/>&nbsp;
	<img border="0" onclick="displayDatePicker('orderEndDate');" alt="Click Here to Pick up the date" src="{$cdn_base}/images/cal.gif">
	</td> 
</tr>
<tr>
	<td nowrap="nowrap" class="FormButton">Shipping Date Period:</td>
	<td width="10">&nbsp;</td>
	<td class="FormButton">
		Start Date::<input type="text" name="shippingStartDate" id="shippingStartDate" value="{ $post_data.shippingStartDate}" />&nbsp;
		<img border="0" onclick="displayDatePicker('shippingStartDate');" alt="Click Here to Pick up the date" src="{$cdn_base}/images/cal.gif">&nbsp;
		End Date::<input type="text" name="shippingEndDate" id="shippingEndDate" value="{ $post_data.shippingEndDate}"/>&nbsp;
		<img border="0" onclick="displayDatePicker('shippingEndDate');" alt="Click Here to Pick up the date" src="{$cdn_base}/images/cal.gif">
	</td>	
</tr>
<tr>
	<td nowrap="nowrap" class="FormButton">Order Value:</td>
	<td width="10">&nbsp;</td>
	<td class="FormButton">Start Value::<input type="text" name="orderStartValue" value="{ $post_data.orderStartValue}"/>&nbsp;End Value::<input type="text" name="orderEndValue" value="{ $post_data.orderEndValue}" /></td>	
</tr>
<tr><td class="FormButton"> Pay Status::</td> <td width="10">&nbsp;</td><td> <input type="text" name="payStatus"  value="{ $post_data.payStatus}"/></td></tr>
<tr><td class="FormButton"> Order Id::</td> <td width="10">&nbsp;</td><td> <input type="text" name="orderId" value="{ $post_data.orderId}"/></td></tr>
<tr><td class="FormButton"> AWB No::</td> <td width="10">&nbsp;</td><td> <input type="text" name="awbno" value="{ $post_data.awbno}"/></td></tr>

<!--<tr>
	<td nowrap="nowrap" class="FormButton">Payment Date Period:</td>
	<td width="10">&nbsp;</td>
	<td class="FormButton">
	Start Date::<input type="text" name="paymentStartDate" id="paymentStartDate" value="{ $post_data.paymentStartDate}"/>&nbsp;
	<img border="0" onclick="displayDatePicker('paymentStartDate');" alt="Click Here to Pick up the date" src="{$cdn_base}/images/cal.gif">&nbsp;
	End Date::<input type="text" name="paymentEndDate" id="paymentEndDate" value="{ $post_data.paymentEndDate}"/>&nbsp;
	<img border="0" onclick="displayDatePicker('paymentEndDate');" alt="Click Here to Pick up the date" src="{$cdn_base}/images/cal.gif">
	</td>	
</tr> -->
</tbody></table>
<center>
	<input type="submit" value="Search" name="codSearchBtn">
	<input type="submit" value="DownloadAsCsv" name="codSearchBtn">
	
</center>
</form>
<p><b> 1.Please select some criteria to see any report.<br/> 2.Start and end date(for any date criteria) is mandatory for any report.<br/> 3.Order start value should be greater than 0.</b></p>
{/capture}
{include file="dialog.tpl" title="Cod Data Search" content=$smarty.capture.dialog extra='width="100%"'}
{capture name=dialog}
<br/><br/><br/>
<table width="100%" cellspacing="5" cellpadding="1" border="1px">
	<tbody>
	    <tr> <td width="35%">
			<table cellspacing="5" cellpadding="1" border="1px">
				<p style="font-size:15px;font-weight:bold;text-align:center;">Transactions Info</p> 
				<tr class="tablehead">
					<th nowrap="nowrap">Pay Status</th>
					<th nowrap="nowrap">Total Transactions</th>
					<th nowrap="nowrap">Total Value</th>
				</tr>
				{foreach from=$pay_status_total item=result_row}
					<tr class="tablehead">
						{foreach from=$result_row[0] item=result_col}				
							<td width="5%" nowrap="nowrap">{$result_col}</td>	
						{/foreach}
					</tr>
			 {/foreach}
			</table>
		 </td>
	 	<td width="15%">
		<table cellspacing="5" cellpadding="1" border="1px" style="text-align:center;">
			<p style="font-size:15px;font-weight:bold;text-align:center;">Avg. Settlement Cycle</p>
			<tr class="tablehead"><th nowrap="nowrap">{$avgPaymentCycle} Days</th></tr>
		</table>
		</td> 
		<td width="25%">
		<table width="25%" cellspacing="5" cellpadding="1" border="1px">
		    <p style="font-size:15px;font-weight:bold;text-align:center;">Pending Payments</p> 
			<tr class="tablehead">
				<th nowrap="nowrap">No. of Days</th>
				<th nowrap="nowrap">Total Value</th>
			</tr>
			{foreach from=$pending_payment_data item=result_row}
					<tr class="tablehead">
						{foreach from=$result_row[0] item=result_col}				
							<td width="5%" nowrap="nowrap">{$result_col}</td>	
						{/foreach}
					</tr>
			 {/foreach}
		</table>
		</td>
		<td width="25%">
		<table width="25%" cellspacing="5" cellpadding="1" border="1px" >
			<p style="font-size:15px;font-weight:bold;text-align:center;">Items Delivery Status</p>
				<tr class="tablehead">
				<th nowrap="nowrap">Item Status </th>
				<th nowrap="nowrap">Item Count</th>
			</tr>	
			{foreach from=$item_status_data item=result_row}
					<tr class="tablehead">
						{foreach from=$result_row[0] item=result_col}				
							<td width="5%" nowrap="nowrap">{$result_col}</td>	
						{/foreach}
					</tr>
			 {/foreach}			
		</table>
		</td>
		</tr>
	</tbody>
</table>
{/capture}
{include file="dialog.tpl" title="Cod Analysis Data " content=$smarty.capture.dialog extra='width="100%"'}
{capture name=dialog}
<p align="center"><font color="red">{$message}</font></p>
<table width="100%" cellspacing="1" cellpadding="2">
				<tbody>
					<tr class="tablehead">
					<th style="border-right: 1px dotted #000000 ; text-align:center ;" width="5%" nowrap="nowrap">Order Id</th>
					<th style="border-right: 1px dotted #000000 ; text-align:center ;" width="5%" nowrap="nowrap">Shipping Date</th>
					<th style="border-right: 1px dotted #000000 ; text-align:center ;" width="5%" nowrap="nowrap">Payment Date</th>
					<th style="border-right: 1px dotted #000000 ; text-align:center ;" width="5%" nowrap="nowrap">Order Date</th>
					<th style="border-right: 1px dotted #000000 ; text-align:center ;" width="5%" nowrap="nowrap">Customer Login</th>
					<th style="border-right: 1px dotted #000000 ; text-align:center ;" width="5%" nowrap="nowrap">AWB No</th>
					<th style="border-right: 1px dotted #000000 ; text-align:center ;" width="5%" nowrap="nowrap">Value(Rs)</th>
					<th style="border-right: 1px dotted #000000 ; text-align:center ;" width="5%" nowrap="nowrap">Pay Status</th>
					<th style="border-right: 1px dotted #000000 ; text-align:center ;" width="5%" nowrap="nowrap">More Info</th>
				</tr> 
				{if $cod_search_data } 
				    {foreach from=$cod_search_data item=result_row}
					<tr>
						{foreach from=$result_row item=result_col}				
						<td style="border-right: 1px dotted #000000 ; text-align:center ;" width="5%" nowrap="nowrap">{$result_col}</td>	
						{/foreach}
							<td style="border-right: 1px dotted #000000 ; text-align:center ;" width="5%" nowrap="nowrap"><a href="order.php?orderid={$result_row.orderid}">View</a></td>
						</tr>
						<tr><td colspan="11"><hr/></td></tr>
					{/foreach}
				{/if}
				</tbody>
</table>
{/capture} {include file="dialog.tpl" title="Cod Search Result "
content=$smarty.capture.dialog extra='width="100%"'} {/capture} {include
file="dialog.tpl" title="Cod Data Report" content=$smarty.capture.dialog
extra='width="100%"'} {literal}
<script type="text/javascript">
$(document).ready(function()
{
	$("#codreportSelectAll").click(function()				
	{
		//alert( " I am here");
		var checked_status = this.checked;
		$("input[name=codreportSelect[]]").each(function()
		{
			//alert("coming here");
			this.checked = checked_status;
		});
	});		
					
});

</script>
{/literal}
