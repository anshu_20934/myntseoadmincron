<script type="text/javascript">
    var httpLocation = '{$http_location}';
</script>

<link rel="stylesheet" type="text/css" href="{$http_location}/admin/extjs/resources/css/ext-all.css"/>

<script type="text/javascript" src="{$http_location}/admin/extjs/adapter/ext/ext-base.js"></script>

<script type="text/javascript" src="{$http_location}/admin/extjs/ext-all.js"></script>
<script type="text/javascript" src="{$http_location}/admin/extjs/plugins/superselectbox/SuperBoxSelect.js"></script>
<link rel="stylesheet" type="text/css" href="{$http_location}/admin/extjs/plugins/superselectbox/superboxselect.css"/>
<script type="text/javascript" src="order_items.js"></script>

<div id="contentPanel"></div>
