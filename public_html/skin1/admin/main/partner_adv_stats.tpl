{* $Id: partner_adv_stats.tpl,v 1.11.2.2 2006/07/11 08:39:26 svowl Exp $ *}
{include file="page_title.tpl" title=$lng.lbl_adv_statistics}
{$lng.txt_advertising_stats_note}<br /><br />

<!-- IN THIS SECTION -->

{include file="dialog_tools.tpl"}

<!-- IN THIS SECTION -->
<br />
 
{capture name=dialog}
<form action="partner_adv_stats.php" method="post">

<table>
<tr>
	<td>{$lng.lbl_period_from}:</td>
	<td>{html_select_date prefix="Start" time=$search.start_date|default:$month_begin start_year=$config.Company.start_year end_year=$config.Company.end_year}</td>
</tr>
<tr>
    <td>{$lng.lbl_period_to}:</td>
    <td>{html_select_date prefix="End" time=$search.end_date start_year=$config.Company.start_year end_year=$config.Company.end_year}</td>
</tr>
<tr>
    <td>{$lng.lbl_campaigns}:</td>
    <td><select name="search[campaignid]">
	<option value=''{if $search.campaignid eq ''} selected="selected"{/if}>{$lng.lbl_all}</option>
	{if $campaigns ne ''}
	{foreach from=$campaigns item=v}
	<option value='{$v.campaignid}'{if $search.campaignid eq $v.campaignid} selected="selected"{/if}>{$v.campaign}</option>
	{/foreach}
	{/if}
	</select></td>
</tr>

<tr>
	<td>&nbsp;</td>
	<td class="SubmitBox"><input type="submit" value="{$lng.lbl_search|strip_tags:false|escape}" /></td>
</tr>

</table>
</form>

{/capture}
{include file="dialog.tpl" content=$smarty.capture.dialog title=$lng.lbl_search extra='width="100%"'}

<br />

{if $result ne ''}
{capture name=dialog}
<table cellspacing="1" cellpadding="2">
<tr class="TableHead">
	<td>{$lng.lbl_campaign}</td>
    <td>{$lng.lbl_clicks}</td>
    <td>{$lng.lbl_estimated_expences}</td>
    <td>{$lng.lbl_acquisition_cost}</td>
	<td>{$lng.lbl_sales}</td>
	<td>{$lng.lbl_roi}</td>
</tr>
{foreach from=$result item=v}
<tr>
	<td><a href="partner_adv_campaigns.php?campaignid={$v.campaignid}">{$v.campaign}</a></td>
	<td>{$v.clicks}</td>
    <td align="right" nowrap="nowrap">{include file="currency.tpl" value=$v.ee|default:"0"}</td>
	<td align="right" nowrap="nowrap">{include file="currency.tpl" value=$v.acost|default:"0"}</td>
    <td align="right" nowrap="nowrap">{include file="currency.tpl" value=$v.total|default:"0"}</td>
    <td>{$v.roi|default:"0"}%</td>
</tr>
{/foreach}
<tr>
    <td colspan="6" height="1"><hr size="1" /></td>
</tr>

<tr>
	<td><b>{$lng.lbl_total}:</b></td>
    <td>{$total.clicks}</td>
    <td align="right" nowrap="nowrap">{include file="currency.tpl" value=$total.ee|default:"0"}</td>
    <td align="right" nowrap="nowrap">{include file="currency.tpl" value=$total.acost|default:"0"}</td>
    <td align="right" nowrap="nowrap">{include file="currency.tpl" value=$total.total|default:"0"}</td>
    <td>{$total.roi|default:"0"}%</td>
</tr>
</table>

{/capture}
{include file="dialog.tpl" content=$smarty.capture.dialog title=$lng.lbl_advertising_campaigns extra='width="100%"'} 
{/if}
