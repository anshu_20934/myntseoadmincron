{* $Id: featured_products.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
{include file="main/include_js.tpl" src="main/popup_product.js"}

<link href="{$http_location}/skin1/myntra_css/jquery-ui-datepicker.css" rel="stylesheet" type="text/css">
{literal}
<style type="text/css">
/* This is used for date picker for start and end dates of a discount rule. */
.ui-timepicker-div .ui-widget-header{ margin-bottom: 8px; }
.ui-timepicker-div dl{ text-align: left; }
.ui-timepicker-div dl dt{ height: 25px; }
.ui-timepicker-div dl dd{ margin: -25px 10px 10px 65px; }
.ui-timepicker-div td { font-size: 90%; }
</style>
{/literal}

{capture name=dialog}
<div style="text-align:center;">
	<input type="button" id="eossViewDiscountRuleBtn" value="View Discount Rules"> 
	<input type="button" id="eossViewExceptionsRuleBtn" value="Add Static Discount Rule">
</div>

{if $mode=='adddiscountrule' || $mode=='searchstyles'}
{include file="admin/main/eoss-admin/eoss_add_discount_rule.tpl"}
{elseif $mode=='viewdiscountrules' || $mode=='enablediscountrules' || $mode=='filterdiscountrules' || $mode=='multiplediscountrulechange'}
{include file="admin/main/eoss-admin/eoss_view_discount_rule.tpl"}
{elseif $mode=='managediscountrule'}
{include file="admin/main/eoss-admin/eoss_modify_discount_rule.tpl"}
{elseif $mode=='addexceptiondiscountrule' || $mode=='viewexceptiondiscountrule'}
{include file="admin/main/eoss-admin/eoss_exception_discount_rule.tpl"}
{/if}

{/capture}

{include file="dialog.tpl" title="End of Season Sales (EOSS) Admin" content=$smarty.capture.dialog extra='width="100%"'}

{literal}
<script type="text/javascript">

function countCheckBoxes(checkboxName) {
	var checkboxElements = document.getElementsByName(checkboxName);
	var totalcheckboxes = checkboxElements.length;

	var count = 0;
	for(x=0; x<totalcheckboxes; x++){
		if(checkboxElements[x].checked) {
			count++;
		}
	}

	return count;
}

$(document).ready(function()
{
	$("#eossAddDiscountRuleBtn").click(function() {
		$("#mode").val("searchstyles");
		$(".eossDiscountRuleForm").trigger("submit");
	});
	
	$("#eossViewDiscountRuleBtn").click(function() {
		$("#mode").val("viewdiscountrules");
		$(".selectbrands").val("");
		$(".selectarticletypes").val("");
		$(".selectfashion").val("");
		$(".eossDiscountRuleForm").trigger("submit");
	});

	$("#eossViewExceptionsRuleBtn").click(function() {
		$("#mode").val("viewexceptiondiscountrule");
		$(".eossDiscountRuleForm").trigger("submit");
	});

	$(".selectAll").click(function() {
		var checked_status = this.checked;
		$(".selectedStyle").each(function() {
			this.checked = checked_status;
		});
	});

	$(".selectAll2").click(function() {
		var checked_status = this.checked;
		$(".selectedStyle2").each(function() {
			this.checked = checked_status;
		});
	});

	$(".selectAll3").click(function() {
		var checked_status = this.checked;
		$(".selectedStyle3").each(function() {
			this.checked = checked_status;
		});
	});
});

</script>
{/literal}