<div id='createTemplate' class='userGroupCreate'>
<h1 class='ntfSuccess'>User Group Created Successfully, Group Id: <span></span></h1>	
<h1>Create User Group</h1>
<form action='usg_create_submit.php' method='POST' enctype='multipart/form-data' name='user_group_create' id='ugCreateForm'>
	<p class='mandatoryText'>* - Mandatory Field</p>
	<div class='formElement clear groupName'>
		<label>Group Name:<sup>*</sup><span class='error'>Enter proper name</span></label>
		<input type='text' name='group_name' class='textInput' />
	</div>
	<div class='formElement clear'>
		<label>Group Description:</label>
		<textarea name='group_desc' class='textArea'></textarea>
	</div>
	<!--<div class='formElement clear groupType'>
		<label>Group Type:<sup>*</sup><span class='error'>Select proper group type:</span></label>
		<select name='group_type' class='selectBox'>
			<option selected="selected" value='-1'>Select Group Type</option>
			<option value='banner'>Banner Group</option>
			<option value='notification'>Notification Group</option>
		</select>
	</div>-->
	<div class='formElement clear csvFile'>
		<label>Group Email Ids:<sup>*</sup><span class='guide'>(upload email ids using csv file)</span>
				<span class='error'>Enter proper csv file</span></label>
		<input type='file' name='group_csv' class='groupCsv' size='60' />
	</div>
	<div class='singleElement'>
		<div class='leftElement'><input type='submit' class='submitBtn' value='Create' /></div>
		<div class='leftElement'><input type='reset' class='resetBtn' value='Clear' /></div>
	</div>
</form>
</div>
<script language="javascript" src="/skin1/myntra_js/ns.js"> </script>
<script language="javascript" src="/skin1/myntra_js/mk-base.js"></script>
<script src='/skin1/admin/notifications/usergroup.js'></script>
<script>
Myntra.ntfFlag = "{$ntfFlag}";
Myntra.groupId = "{$groupId}";
</script>