{* $Id: statistics.tpl,v 1.24.2.1 2006/06/16 10:47:41 max Exp $ *}
{include file="main/include_js.tpl" src="main/calendar2.js"}

{capture name=dialog}

<form action="" method="post" name="salesreportform">
<input type="hidden" name="mode" value="search" />

<table cellpadding="1" cellspacing="1">


<tr>
	<th align="right">{$lng.lbl_reporttype}:</th>
	<td>
	     <select name='report_type'>
	      <option value="week"  { if $reportType == "week"} selected {/if} >Weekly</option>
              <option value="month" { if $reportType == "month"} selected {/if} >Monthly</option> 
	     
	   </select>
	</td>
	<th align="right">{$lng.lbl_productstyle}:</th>
	<td>
	    <select name='prd_style'>
	      <option value="All" { if $style == "All"} selected {/if} >All</option>
               
                {section name=num loop=$prd_styles}
		   <option value="{$prd_styles[num].id}" { if $prd_styles[num].id eq $style} selected {/if}>{$prd_styles[num].name}</option>
		{/section}
               
           </select>
	</td>
</tr>

<tr>
	<th align="right">{$lng.lbl_report_from}:</th>
	<td><input type="text" name='Startdate' value="{$SDate}"/><a href="javascript:cal4.popup();" ><img src="{$cdn_base}/images/cal.gif" width="16" height="16" border="0" alt="Click Here to Pick up the date"></a></td>
	<th align="right">{$lng.lbl_report_to}:</th>
	<td><input type="text" name='Enddate'  value="{$EDate}" /><a href="javascript:cal5.popup();" ><img src="{$cdn_base}/images/cal.gif" width="16" height="16" border="0" alt="Click Here to Pick up the date"></a></td>
</tr>

<tr>
	<td colspan="4"><input type="submit" value="{$lng.lbl_submit|escape}" /></td>
</tr>
<script language="javascript">
      var cal4 = new calendar2(document.forms['salesreportform'].elements['Startdate']);
      cal4.year_scroll = true;
      cal4.time_comp = false;

      var cal5 = new calendar2(document.forms['salesreportform'].elements['Enddate']);
      cal5.year_scroll = true;
      cal5.time_comp = false;


</script>

</table>
</form>

{/capture}
{include file="dialog.tpl" title='' content=$smarty.capture.dialog extra='width="100%"'}

<br />
<br />
{if $mode}

{capture name=dialog}
<table cellpadding="1" cellspacing="1" width="100%">

<tr class="TableHead">
	<td >{$lng.lbl_duration}</td>
	<td >{$lng.lbl_productstyle}</td>
	<td >{$lng.lbl_products}</td>
	<td >{$lng.lbl_sales_amt}(Rs)</td>
</tr>


{if $sales }

{section name=num loop=$sales}

     
 {section name=idx loop=$sales[num]}
              


<tr{cycle values=', class="TableSubHead"'}>
	<td align="center">{$sales[num][idx].duration1}-{$sales[num][idx].duration2}</td>
	<td align="center">{$sales[num][idx].name}</td>
	<td align="center">{$sales[num][idx].num_of_products}</td>
	<td align="center">{$sales[num][idx].price*$sales[num][idx].num_of_products}</td>
</tr>

{/section}
  
{/section}
 <tr{cycle values=', class="TableSubHead"'}>
	<td align="center"><b>Total</b></td>
	<td align="center">&nbsp;</td>
	<td align="center">&nbsp;</td>
	<td align="center"><b>{$total}</b></td>
</tr>




{else}

<tr>
	<td colspan="4" align="center">{$lng.txt_no_record}</td>
</tr>

{/if}


</table>


</form>

<br />

{/capture}
{include file="dialog.tpl" title='' content=$smarty.capture.dialog extra='width="100%"'}

{/if}