{* $Id: featured_products.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
{*include file="main/include_js.tpl" src="main/popup_product.js"*}
{*include file="main/include_js.tpl" src="main/calendar2.js"*}


<a name="featured" />

{$lng.txt_featured_products}



{capture name=dialog}


<form action="adminaffiliates.php" method="post" name="searchform">
<input type="hidden" name="mode"  />


<table cellpadding="3" cellspacing="1" width="100%">




<tr>
	<td align="left"><b>Affiliate Name</b></td>
	<td align="left"><input type="text" size ="40" name="affiliatecontact" value="{$searchText}" /></td>
	
	<td align="left"><b>Status</b></td>
	<td align="left">
	               <select name="aff_status" id="aff_status">
                     <option value="N" {if $selectedStatus eq 'N'} selected {/if}>In Active</option>  
	                 <option value="Y" {if $selectedStatus eq 'Y'} selected {/if} >Active</option>   
	                 <option value="C" {if $selectedStatus eq 'C'} selected {/if}>Rejected</option>    
	               </select>
	
	</td>

</tr>


<tr>
	<td colspan="4" class="SubmitBox">
	<input type="submit" value="{$lng.lbl_search|escape}" onclick="javascript: document.searchform.mode.value = 'search'; document.searchform.submit();"/>
	</td>
</tr>

</table>
</form>

{/capture}
{include file="dialog.tpl" title="Search Affiliate" content=$smarty.capture.dialog extra='width="100%"'}
<br/>
<!-- display only user search for affiliates -->
{*if $affiliates*}

	{capture name=dialog}
	<form action="adminaffiliates.php" method="post" name="affiliatefrm" >
	<input type="hidden" name="mode" />
	
	<table cellpadding="2" cellspacing="1" width="100%">
	
{if $affiliates}
	
	<tr class="TableHead">
		<td >&nbsp;</td>
		<td>ID</td>
		<td >Company</td>
		<td >Contact Person</td>
		<td  align="center">Contact Email</td>
		<td  align="center">Joined Date</td>
		<td  align="center">Content Provider</td>
		<td align="center">Earn Comm(Rs)</td>
		<td align="center">Paid(Rs)</td>
		<td  align="center">Active</td>
	</tr>
		
	{assign var="totalcommEarn" value=0.00}
	{assign var="totalcommPaid" value=0.00}
		
	{section name=prod_num loop=$affiliates}
	
	<tr{cycle values=", class='TableSubHead'"}>
	
		<td ><input type="checkbox" name="posted_data[{$affiliates[prod_num].affiliate_id}][delete]"   /></td>
		<td align="left" ><a href="affiliate_modify.php?affId={$affiliates[prod_num].affiliate_id}&mode=modify">{$affiliates[prod_num].affiliate_id}</a></td>
		<td >{$affiliates[prod_num].company_name}</td>
		<td >{$affiliates[prod_num].contact_name}</td>
		<td >{$affiliates[prod_num].contact_email}</td>
		<td align="center">{$affiliates[prod_num].joined_date|date_format}</td>
		<td align="center">{$affiliates[prod_num].contentprovider}</td>
		<td align="center"><b>{$affiliates[prod_num].commEarned}</b></td>
		<td align="center"><b>{$affiliates[prod_num].paidComm}</b></td>
		<td align="center">{$affiliates[prod_num].active}
		      <!-- <select name="posted_data[{$affiliates[prod_num].affiliate_id}][status]"> 
			 <option value='Y' {if $affiliates[prod_num].active eq 'Y'}selected {/if} >Yes</option>
			 <option value='N'  {if $affiliates[prod_num].active eq 'N'} selected {/if}>No</option>
		      </select>-->
	       </td>
		{math equation="x + earnComm" x=$totalcommEarn earnComm=$affiliates[prod_num].commEarned assign="totalcommEarn"}
		{math equation="x + paidComm" x=$totalcommEarn paidComm=$affiliates[prod_num].paidComm assign="totalcommPaid"}
			
	
	</tr>
	
	{/section}
	
	<tr>
		<td colspan="9" >
		<hr>
		</td>
	</tr>
	<tr>
		<td colspan="5" >&nbsp;
		</td>
		<td align="center"><strong>&nbsp;</strong></td>
		<td align="center"><strong>&nbsp;</strong></td>
		<td align="center"><strong>{include file="currency.tpl" value=$totalcommEarn}</strong></td>
		<td align="center"><strong>{include file="currency.tpl" value=$totalcommPaid}</strong></td>
	</tr>
	<tr>
		<td colspan="5" class="SubmitBox">
		<input type="submit" value="Commission payment" onclick="document.affiliatefrm.mode.value = 'update'; document.affiliatefrm.submit();" />
		
		<input type="button" value="{$lng.lbl_modify_selected|escape}" onclick="javascript: submitForm(this, 'updatestatus');" /></td>
	</tr>

	{/if}
	{if $mode eq 'search' && $affiliates eq ''}
		 <tr>
		     <td colspan="5" align="center">{$lng.txt_no_designer_search}</td>
		</tr>
	{/if}
	
		
	
	</table>
	</form>
	
	{/capture}
	{include file="dialog.tpl" title='Search Result for Affiliates' content=$smarty.capture.dialog extra='width="100%"'}
{*/if*}	

{include file="admin/main/affiliate_pending.tpl"}

