{* $Id: process_item_exit.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
{include file="main/include_js.tpl" src="main/popup_product.js"}
<base href="{$http_location}"> 
<link rel="stylesheet" href="{$SkinDir}/skin1_admin_operations.css" />
{literal}
<script Language="JavaScript" Type="text/javascript">
	$(function() {

		$('#reset-btn').click(function(e) {
			$("textarea#carton_barcodes").val("");
			$("textarea#carton_barcodes").attr("disabled", false);
			$("#logs_section").html("");
		});
		
		$('#validate-btn').click(function(e) { 
			var cartonBarcodes = $("textarea#carton_barcodes").val();
			if(cartonBarcodes == "") {
				alert("Please input carton barcodes");
				return;
			}
			$("textarea#carton_barcodes").attr("disabled", true);
			$("#logs_section").html("Loading...");
			$("#orderids").val("");
			
			$.ajax({
				url: "/admin/operations/processitemexit.php",
				type: 'POST',
				data: {'action': 'validateCartonOrders', 'cartonBarCodes': cartonBarcodes},
				context: document.body,
				dataType: "json",
				success: function(response){
					$("#logs_section").html("");
					if(response[0] == 'Success') {
						var moveableCartonBarcodes = response[1];
						var unmovableCartonBarcodes = response[2];
						var invalidCartonBarcodes = response[3];
						var cartonbarcode2orderIdMap = response[4];
						var orderid2StatusMap = response[5];

						if(invalidCartonBarcodes.length > 0) {
							var html = '<span style="color:red;">Following Cartons were not located: <br><br>';
							for(var index in invalidCartonBarcodes) {
								if(invalidCartonBarcodes[index].trim() != "") {
									html += invalidCartonBarcodes[index]+"<br>";
								}
							}
							html += '</span>';
							$("#logs_section").append(html);
							$("#logs_section").append("<br><hr>");
						}

						if(unmovableCartonBarcodes.length > 0) {
							var html = '<span style="color:red;">Following Cartons cannot be moved to Exit Bin because of exit movement restrictions: <br><br>';
							for(var index in unmovableCartonBarcodes) {
								if(unmovableCartonBarcodes[index].trim() != "") {
									html += unmovableCartonBarcodes[index]+"<br>";
								}
							}
							html += '</span>';
							$("#logs_section").append(html);
							$("#logs_section").append("<br><hr>");
						}
						
						var validOrdersHtml = "";
						var cancelledOrdersHtml = "";
						var noOrdersCartons = "";
						var invalidOrdersCartons = "";

						for(var cartonbarcode in cartonbarcode2orderIdMap) {
							var orderid = ""+cartonbarcode2orderIdMap[cartonbarcode];
							if(orderid.trim() == 'Invalid') {
								invalidOrdersCartons += cartonbarcode+"<br>";
							} else if (orderid == 'Empty') {
								noOrdersCartons += cartonbarcode+"<br>";
							} else {
								var status = orderid2StatusMap[orderid];
								if(status != 'SH' && status != 'WP') {
									cancelledOrdersHtml += "Order "+orderid+" in carton "+cartonbarcode+" is in "+status+" state.<br>";
								} else {
									validOrdersHtml += "Order "+orderid+" in carton "+cartonbarcode+" is in state "+status+"<br>";
								}
							}
						}
						
						if(cancelledOrdersHtml != "") {
							$("#logs_section").append('<span style="color:red;">Following orders were cancelled: <br><br>'+cancelledOrdersHtml+'</span><br><hr>');
						}

						if(validOrdersHtml != "") {
							$("#logs_section").append('<span style="color:green;">Following orders can be moved out: <br><br>'+validOrdersHtml+'</span><br><hr>');
						}
						if(noOrdersCartons != "") {
							$("#logs_section").append('<span style="color:red;">Following cartons do not contain any order: <br><br>'+noOrdersCartons+'</span><br><hr>');
						}
						if(invalidOrdersCartons != "") {
							$("#logs_section").append('<span style="color:red;">Following cartons contain invalid items: <br><br>'+invalidOrdersCartons+'</span><br><hr>');
						}
					} else {
						 $("#logs_section").append('<span style="color:red;">'+response[1]+'</span><br>');
					}

					var validCartons = "";
					for (var i in moveableCartonBarcodes) {
						validCartons += validCartons==""?"":",";
						validCartons += moveableCartonBarcodes[i];
					}
					
					if(validCartons && validCartons != "") {
						$("#cartonBarcodes").val(validCartons);
						$("#exit-btn").attr("disabled", false);
					}
				}
			});
		}); 
	});
</script>
{/literal}

{capture name=dialog}
	{if $errorMsg and $errorMsg ne '' }
		<div style="color: red; font-size: 12px; font-weight: bold; text-align:center;">{$errorMsg}</div>
	{/if}
	{if $successMsg and $successMsg ne '' }
		<div style="font-size: 12px; font-weight: bold; text-align:center;">{$successMsg}</div>
	{/if}
	
	<table border=0 cellspacing=0 cellpadding=0 width=100%>
		<tr>
			<td width=150>Enter Carton Barcodes <br> (containing items to send outside, separated by newline)</td>
			<td>
				<textarea id="carton_barcodes" name="carton_barcodes" style="width: 250px; height: 300px;"></textarea>
			</td>
			<td valign=middle>
				<input type="button" name="validate-btn" id="validate-btn" value="Validate Orders">
				<br><br>
				<input type="button" name="reset-btn" id="reset-btn" value="Reset">
			</td>
			<td>
				<div style="width: 300px; height: 300px; overflow: auto; background-color: #EEEEEF; border: 1px solid #555555;" id="logs_section"></div>
			</td>
			<td>
				<form action="" method="post">
					<input type="hidden" name="cartonBarcodes" id="cartonBarcodes" value=""> 
					<input type="hidden" name="userid" id="userid" value="{$login}"> 
					<input type="hidden" name="action" id="action" value="markItemShipped"> 
					<input type="submit" name="exit-btn" id="exit-btn" value="Move To Exit" disabled>
				</form>
			</td>
		</tr>			
	</table>
{/capture}
{include file="dialog.tpl" title="Material Movement" content=$smarty.capture.dialog extra='width="100%"'}	
