{literal}<style>
.coupon-slab-cont {border:solid 1px #68C1FF;
	padding:10px 30px;
	background:transparent url(http://localhost.local/admin/extjs/resources/images/default/panel/top-bottom.gif) repeat 0 0;
}
.coupon-slab-cont .small-box input{
	width:50px;
}
.coupon-slab-cont .TableHead td {
	background:transparent url(http://localhost.local/admin/extjs/resources/images/default/grid/grid3-hrow.gif) repeat 0 0;
	border-right:
}
</style>
<script>
$(document).ready(function(){
	InitCouponConfig();
});

var InitCouponConfig = function (){

	var mod=$('input[name="mode"]'),
		id=$('input[name="id"]'),
		lowerBound = $('input[name="lowerBound"]'),
		noOfCoupons=$('input[name="noOfCoupons"]'),
		couponValue=$('input[name="CouponValue"]'),
		minValue=$('input[name="CouponMinPurchaseValue"]'),
		formToSubmit=$('#updatecoupon_slabs'),
		nLowerBound=$('input[name="lowerbound_field_new"]'),
		nNoOfCoupons=$('input[name="count_field_new"]'),
		nCouponValue=$('input[name="value_field_new"]'),
		nMinValue=$('input[name="min_value_field_new"]');
		action='coupon_config.php';
		formToSubmit.attr('action',action);
		
	$('.add-slab').click(function(){
		lowerBound.val(nLowerBound.val());
		noOfCoupons.val(nNoOfCoupons.val());
		couponValue.val(nCouponValue.val());
		minValue.val(nMinValue.val());
		if(validate()){
			mod.val(1);
			formToSubmit.submit();
		}
		else {
			alert('All fields are mandatory and values has to be Numbers');
		}
	});

	$('.edit-slab').click(function(){
		//Enable text boxes
		$(this).closest('table').find('tr.existing-block .edit-slab').show();
		$(this).closest('table').find('tr.existing-block .save-slab').hide();
		$(this).closest('table').find('tr.existing-block input[type="text"]').attr('disabled',true).css('border-color','#eee');
		$(this).closest('tr.existing-block').find('input[type="text"]').attr('disabled',false).css('border-color','#d14747');
		$(this).hide();
		$(this).siblings().show();
		
	});

	$('.save-slab').click(function(){
		//save slab
		console.log('save now');
		lowerBound.val($(this).closest('tr').find('.row-lowerbound').val());
		noOfCoupons.val($(this).closest('tr').find('.row-count').val());
		couponValue.val($(this).closest('tr').find('.row-value').val());
		minValue.val($(this).closest('tr').find('.row-minvalue').val());
		id.val($(this).closest('tr').find('.row-id').val());
		console.log(lowerBound.val(),'--',noOfCoupons.val());
		if(validate()){
			mod.val(2);
			formToSubmit.submit();
		}
		else {
			alert('All fields are mandatory and values has to be Numbers');
		}
	});

	$('.delete-slab').click(function(){
			mod.val(3);
			id.val($(this).closest('tr').find('.row-id').val());
			formToSubmit.submit();
	});
	var validate = function(){
		if(!$.trim(lowerBound.val()) ||  !$.trim(noOfCoupons.val()) || !$.trim(couponValue.val()) || !$.trim(minValue.val()) || isNaN(lowerBound.val()) || isNaN(noOfCoupons.val()) || isNaN(couponValue.val()) ||  isNaN(minValue.val())){
			return false;
		}else {
			return true;
		}
			
	}
	
	
	$('.save-key-value').click(function(){
		var widgetVal=$(this).closest('.widget-block').find('.val').val(),
			widgetKey=$(this).closest('.widget-block').find('.key').val(),
			formS =  $('#widget-key-form');
		if(!$.trim(widgetVal)){
			alert('value cannot be empty');
			return false;
			
		}
		else {
			formS.attr('action','coupon_config.php');
			formS.find('input[name="widget-key"]').val(widgetKey);
			formS.find('input[name="widget-val"]').val(widgetVal);
			formS.submit();
		}
		
	});

}
</script>
{/literal}
<div class="coupon-slab-cont">
<form id="widget-key-form" method="post">
	<input type="hidden" value="" name="widget-key"/>
	<input type="hidden" value="" name="widget-val"/>
	<input type="hidden" value="save" name="key-update"/>
	
	
	<h2>Widget Key Value Pairs</h2>
	<table cellpadding="3" cellspacing="1" width="90%" style="margin:0 auto">
		<tr class="TableHead">
			<td width="20%">Widget Key Name</td>
			<td width="40%">Value</td>
			<td width="40%">Description</td> 
			<td style="display:none"></td>
		</tr>
		{if $couponPrefix}
		<tr class="widget-block">
			<td align="center">Coupon Prefix</td>
			<td align="center">
				<input type="text"  style="width:300px" value="{$couponPrefix}" class="val"/>
				<input type="hidden"  value="{$couponPrefixKey}" class="key"/>
			</td>
			<td align="center">Whats is coupon prefix</td>
			<td align="center"><input type="button" value="save" class="save-key-value"/></td>
		</tr>
		{/if}
		{if $couponGroupName}
		<tr class="widget-block">
			<td align="center">Coupon Group Name</td>
			<td align="center">
				<input disabled type="text" style="width:300px" value="{$couponGroupName}" class="val"/>
			</td>
			<td align="center">Whats is coupon Group Name</td>

		</tr>
		{/if}
		{if $orderConfirmationMsg}
		<tr class="widget-block">
			<td align="center">Order Confirmation message</td>
			<td align="center">
				<textarea rows="5" style="width:300px" class="val">{$orderConfirmationMsg}</textarea>
				<input type="hidden"  value="{$orderConfirmationMsgKey}" class="key"/>	
			</td>
			<td align="center">Whats is Order Confirmation Msg</td>
			<td align="center"><input type="button" value="save" class="save-key-value"/></td>
		</tr>
		{/if}

	</table>
	</form>
</div>
<br/>
<div class="coupon-slab-cont">
<h2>Slab Based Coupons</h2>
{if $error_msg}
	<div style="font-size:12px;color:#d14747;text-align:center;padding:0 0 10px">{$error_msg}</div>
{/if}
{if !$coupon_slabs}
	<div style="font-size:12px;color:#d14747;text-align:center;padding:0 0 10px">No Slabs Added</div>
{/if}

<form action="" method="post" name="updatecoupon_slabs" id="updatecoupon_slabs">
<input type="hidden" name="mode" value=""/>
<input type="hidden" name="id" />
<input type="hidden" name="lowerBound" />
<input type="hidden" name="noOfCoupons" />
<input type="hidden" name="CouponValue" />
<input type="hidden" name="CouponMinPurchaseValue" />


<table cellpadding="3" cellspacing="1" width="90%" style="margin:0 auto">
<tr class="TableHead">
	<td width="5%">S.No</td>
	<td width="25%">Lower Bound</td>
	<td width="10%">No of Coupons</td> 
	<td width="25%">Coupon Value</td>
	<td width="25%">Coupon Min Purchase Value</td>
	<td width="5%" style="display:none"></td>
	<td width="5%" style="display:none"> </td>
</tr>

{if $coupon_slabs}
{foreach name=coupon_slab key=key item=coupon_slab from=$coupon_slabs}
<tr class="existing-block">
	<td align="center">{$smarty.foreach.coupon_slab.iteration}
	<input type="hidden" class="row-id" name="update_id_field[{$coupon_slab.id}]" value="{$coupon_slab.id}">
	</td>
	<td align="center" ><input disabled class="row-lowerbound" name="update_lower_bound_field[{$coupon_slab.id}]" type="text" value="{$coupon_slab.lowerBound}" ></td>
	<td align="center" class="small-box"><input disabled  class="row-count" name="update_count_field[{$coupon_slab.id}]" type="text" value="{$coupon_slab.noOfCoupons|escape}" /></td>
	<td align="center" ><input disabled class="row-value" name="update_value_field[{$coupon_slab.id}]" type="text" value="{$coupon_slab.CouponValue|escape}" /></td>
	<td align="center" ><input disabled  class="row-minvalue" name="update_min_value_field[{$coupon_slab.id}]" type="text" value="{$coupon_slab.CouponMinPurchaseValue|escape}" /></td>
	<td align="center"><input type="button" value="edit" class="edit-slab"/><input style="display:none" type="button" value="save"  class="save-slab"/></td>
	<td align="center"><input type="button" value="delete" class="delete-slab"/></td>
</tr>
{/foreach}
{/if}
<tr>
	<td></td>
	<td align="center" ><input name="lowerbound_field_new" type="text" value=""></td>
	<td align="center" class="small-box"><input name="count_field_new" type="text" value="" /></td>
	<td align="center" ><input name="value_field_new" type="text" value="" /></td>
	<td align="center" ><input name="min_value_field_new" type="text" value="" /></td>
	<td align="center">
		<input type="button" value="add" class="add-slab"/>
	</td>
</tr>
</table>
</form>
</div>