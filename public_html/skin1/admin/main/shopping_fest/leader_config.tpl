{literal}<style>
.leader-slab-cont {
	border:solid 1px #68C1FF;
	padding:10px 30px;
	background:#DFE8F6 url(http://localhost.local/admin/extjs/resources/images/default/panel/top-bottom.gif) repeat-x 0 -20px;
	
}
.leader-slab-cont .small-box input{
	width:50px;
}
.leader-slab-cont .TableHead td {
	background:transparent url(http://localhost.local/admin/extjs/resources/images/default/grid/grid3-hrow.gif) repeat 0 0;

}

.leader-slab-cont .widget-block td{
	border-bottom:solid 2px #F2F2F2;
}  
.leader-slab-cont .widget-block td.left-align input.val,
.leader-slab-cont .widget-block td.left-align input.new-val{
	margin-left:20px;
}
</style>
<script>
$(document).ready(function(){
	InitLeaderConfig();
});

var InitLeaderConfig = function (){
	var formS =  $('#widget-key-form');
		formS.attr('action','leader_config.php');
	$('.save-key-value').click(function(){
		var widgetVal=$(this).closest('.widget-block').find('.val').val(),
			widgetKey=$(this).closest('.widget-block').find('.key').val();
			
		if(!$.trim(widgetVal)){
			alert('value cannot be empty');
			return false;
			
		}
		else {
			formS.find('input[name="widget-key"]').val(widgetKey);
			formS.find('input[name="widget-val"]').val(widgetVal);
			formS.submit();
		}
		
	});
	//ARRAY SAVE
	$('.save-key-value-arr').click(function(){
		console.log('clciked array save');
		//get all the values and check for null 
		var widgetKey=$(this).closest('.widget-block').find('.key').val(),
			widgetValues = $(this).closest('.widget-block').find('.val'),
			valString = '',
			valArr =[];
		
		widgetValues.each(function(index){
			if(!$.trim($(this).val())){
				alert('value cannot be empty');
				return false
			}
			else {
				valArr.push($(this).val());
			}
		});
		var valString=valArr.join(';')
		formS.find('input[name="widget-key"]').val(widgetKey);
		formS.find('input[name="widget-val"]').val(valString);
		formS.submit();
			
		
	});
	//ADD  A NEW VAl 
	$('.add-val').click(function(){
		console.log('clciked array add');
		
		var widgetKey=$(this).closest('.widget-block').find('.key').val(),
		newVal=$(this).closest('.widget-block').find('.new-val').val();
		
		if(!$.trim(newVal)){
			alert('value cannot be empty');
			return false;
			
		}
		else 
		{	
			var oldValues=$(this).closest('.widget-block').find('.input-block .val');
			var oldValuesJ=JSON.parse($(this).closest('.widget-block').find('.old-val').val());
			var valArr = [];
			for(key in oldValuesJ){
				valArr.push(oldValuesJ[key]);
			}
			var valString=valArr.join(';')+';'+newVal;
			
			formS.find('input[name="widget-key"]').val(widgetKey);
			formS.find('input[name="widget-val"]').val(valString);
			formS.submit();
		}
		
	});
	//DELETE A NEW VAl
	$('.del-val').click(function(index){
		console.log('clicked delete');
		var widgetKey=$(this).closest('.widget-block').find('.key').val();
		var oldValuesJ=JSON.parse($(this).closest('.widget-block').find('.old-val').val());
		var valArr = [];
		for(key in oldValuesJ){
			if(oldValuesJ[key]!=$(this).siblings('input.val').val()){
			valArr.push(oldValuesJ[key]);
			}
		}
		
		var valString=valArr.join(';')
		formS.find('input[name="widget-key"]').val(widgetKey);
		formS.find('input[name="widget-val"]').val(valString);
		formS.submit();
	});
	
}
</script>
{/literal}
<div class="leader-slab-cont">
<form id="widget-key-form" method="post">
	<input type="hidden" value="" name="widget-key"/>
	<input type="hidden" value="" name="widget-val"/>
	<input type="hidden" value="save" name="key-update"/>
	<input type="hidden" value="" name="old-val"/>
	
	
	<h2>Widget Key Value Pairs</h2>
	<table cellpadding="3" cellspacing="1" width="90%" style="margin:0 auto">
		<tr class="TableHead">
			<td width="20%">Widget Key Name</td>
			<td width="40%">Value</td>
			<td width="30%">Description</td> 
			<td style="display:none"></td>
		</tr>
		{if $fest_month_start_date}
		<tr class="widget-block">
			<td align="center">Fest month start date</td>
			<td align="left" class="left-align">
				<input type="text"  style="width:300px" value="{$fest_month_start_date}" class="val"/>
				<input type="hidden"  value="{$k_fest_month_start_date}" class="key"/>
			</td>
			<td align="center">given date in YYYY-MM-DD format</td>
			<td align="center"><input type="button" value="save" class="save-key-value"/></td>
		</tr>
		{/if}
		{if $fest_month_end_date}
		<tr class="widget-block">
			<td align="center">Fest month end date</td>
			<td align="left" class="left-align">
				<input type="text"  style="width:300px" value="{$fest_month_end_date}" class="val"/>
				<input type="hidden"  value="{$k_fest_month_end_date}" class="key"/>
			</td>
			<td align="center">given date in YYYY-MM-DD format</td>
			<td align="center"><input type="button" value="save" class="save-key-value"/></td>
		</tr>
		{/if}
		{if $fest_week_start_day}
		<tr class="widget-block">
			<td align="center">Fest week start day</td>
			<td align="left" class="left-align">
				<input type="text"  style="width:300px" value="{$fest_week_start_day}" class="val"/>
				<input type="hidden"  value="{$k_fest_week_start_day}" class="key"/>
			</td>
			<td align="center">put 0 for sunday,1 for monday,2 for tues.....6 for saturday</td>
			<td align="center"><input type="button" value="save" class="save-key-value"/></td>
		</tr>
		{/if}
		{if $fest_week_end_day}
		<tr class="widget-block">
			<td align="center">Fest week end day</td>
			<td align="left" class="left-align">
				<input type="text"  style="width:300px" value="{$fest_week_end_day}" class="val"/>
				<input type="hidden"  value="{$k_fest_week_end_day}" class="key"/>
			</td>
			<td align="center">put 0 for sunday,1 for monday,2 for tues.....6 for saturday</td>
			<td align="center"><input type="button" value="save" class="save-key-value"/></td>
		</tr>
		{/if}
		{if $fest_leader_board_id}
		<tr class="widget-block">
			<td align="center">Fest leader board id</td>
			<td align="left" class="left-align">
				<input type="text"  style="width:300px" value="{$fest_leader_board_id}" class="val"/>
				<input type="hidden"  value="{$k_fest_leader_board_id}" class="key"/>
			</td>
			<td align="center">leader board id reflects id in leader_board table corresponding to leader board being used</td>
			<td align="center"><input type="button" value="save" class="save-key-value"/></td>
		</tr>
		{/if}
		{if $fest_cache_invalidate_time}
		<tr class="widget-block">
			<td align="center">Fest cache invalidate time</td>
			<td align="left" class="left-align">
				<input type="text"  style="width:300px" value="{$fest_cache_invalidate_time}" class="val"/>
				<input type="hidden"  value="{$k_fest_cache_invalidate_time}" class="key"/>
			</td>
			<td align="center">set the time afte which cache should become invalid</td>
			<td align="center"><input type="button" value="save" class="save-key-value"/></td>
		</tr>
		{/if}
		{if $fest_topNbuyers}
		<tr class="widget-block">
			<td align="center">Fest top N buyers</td>
			<td align="left" class="left-align">
				<input type="text"  style="width:300px" value="{$fest_topNbuyers}" class="val"/>
				<input type="hidden"  value="{$k_fest_topNbuyers}" class="key"/>
			</td>
			<td align="center">gives value of N in top N buyers</td>
			<td align="center"><input type="button" value="save" class="save-key-value"/></td>
		</tr>
		{/if}
		{if $fest_topNCategoryBuyers}
		<tr class="widget-block">
			<td align="center">Fest top N category buyers</td>
			<td align="left" class="left-align">
				<input type="text"  style="width:300px" value="{$fest_topNCategoryBuyers}" class="val"/>
				<input type="hidden"  value="{$k_fest_topNCategoryBuyers}" class="key"/>
			</td>
			<td align="center">gives value of N in top N category buyers</td>
			<td align="center"><input type="button" value="save" class="save-key-value"/></td>
		</tr>
		{/if}
		{if $fest_lucky_draw_increment_amount}
		<tr class="widget-block">
			<td align="center">Fest lucky draw increment amount</td>
			<td align="left" class="left-align">
				<input type="text"  style="width:300px" value="{$fest_lucky_draw_increment_amount}" class="val"/>
				<input type="hidden"  value="{$k_fest_lucky_draw_increment_amount}" class="key"/>
			</td>
			<td align="center">incremental amount on which extra each ticket will be added</td>
			<td align="center"><input type="button" value="save" class="save-key-value"/></td>
		</tr>
		{/if}
		{if $fest_lucky_draw_min_amount}
		<tr class="widget-block">
			<td align="center">Fest lucky draw minimum amount</td>
			<td align="left" class="left-align">
				<input type="text"  style="width:300px" value="{$fest_lucky_draw_min_amount}" class="val"/>
				<input type="hidden"  value="{$k_fest_lucky_draw_min_amount}" class="key"/>
			</td>
			<td align="center">'minimum amount to qualify for the first lucky draw ticket</td>
			<td align="center"><input type="button" value="save" class="save-key-value"/></td>
		</tr>
		{/if}
		{if $fest_shop_more_to_win_msg}
		<tr class="widget-block">
			<td align="center">Fest shop more to win message</td>
			<td align="left" class="left-align">
				<input type="text"  style="width:300px" value="{$fest_shop_more_to_win_msg}" class="val"/>
				<input type="hidden"  value="{$k_fest_shop_more_to_win_msg}" class="key"/>
			</td>
			<td align="center">Message to be displayed when I need to shop more and win some prize</td>
			<td align="center"><input type="button" value="save" class="save-key-value"/></td>
		</tr>
		{/if}
		{if $fest_top_in_all_cat_msg}
		<tr class="widget-block">
			<td align="center">Fest top in all category message</td>
			<td align="left" class="left-align">
				<input type="text"  style="width:300px" value="{$fest_top_in_all_cat_msg}" class="val"/>
				<input type="hidden"  value="{$k_fest_top_in_all_cat_msg}" class="key"/>
			</td>
			<td align="center">message to be displayed when the user is the topper in all categories</td>
			<td align="center"><input type="button" value="save" class="save-key-value"/></td>
		</tr>
		{/if}
		{if $myntrashoppingfestleaderboard}
		<tr class="widget-block">
			<td align="center">Fest Leader Board</td>
			<td align="left" class="left-align">
				<input type="text"  style="width:300px" value="{$myntrashoppingfestleaderboard}" class="val"/>
				<input type="hidden"  value="{$k_myntrashoppingfestleaderboard}" class="key"/>
			</td>
			<td align="center">Enabling/Disabling the leader board functionality for Myntra Shopping Festival</td>
			<td align="center"><input type="button" value="save" class="save-key-value"/></td>
		</tr>
		{/if}
		{if $fest_prizes_semicolon_separated}
		<tr class="widget-block">
			<td align="center">Fest Prizes</td>
			<td align="left" class="left-align">
				{foreach name=item key=key item=item from=$fest_prizes_semicolon_separated}
				{if $item ne ''}			
				<div class="input-block">
				<input type="text"  style="width:300px" value="{$item}" class="val" name="val[{$smarty.foreach.item.index}]"/>
				<input type="button" value="delete" class="del-val" name="del[{$smarty.foreach.item.index}]"/>
				</div>
				{/if}
				{/foreach}
				<input type="text"  style="width:300px" value="" class="new-val" name="new-val"/>
				<input type="button" value="add" class="add-val"/>
				<input type="hidden"  value="{$k_fest_prizes_semicolon_separated}" class="key"/>
				<input type="hidden"  value='{$j_fest_prizes_semicolon_separated}' class="old-val"/>
			</td>
			<td align="center">prizes for the respective positions</td>
			
			<td align="center"><input type="button" value="save" class="save-key-value-arr"/></td>
		</tr>
		{/if}
		{if $fest_prizes_men_semicolon_separated}
		<tr class="widget-block">
			<td align="center">Fest Prizes men</td>
			<td align="left" class="left-align">
				{foreach name=item key=key item=item from=$fest_prizes_men_semicolon_separated}
				{if $item ne ''}			
				<div class="input-block">
				<input type="text"  style="width:300px" value="{$item}" class="val" name="val[{$smarty.foreach.item.index}]"/>
				<input type="button" value="delete" class="del-val" name="del[{$smarty.foreach.item.index}]"/>
				</div>
				{/if}
				{/foreach}
				<input type="text"  style="width:300px" value="" class="new-val" name="new-val"/>
				<input type="button" value="add" class="add-val"/>
				<input type="hidden"  value="{$k_fest_prizes_men_semicolon_separated}" class="key"/>
				<input type="hidden"  value='{$j_fest_prizes_men_semicolon_separated}' class="old-val"/>
			</td>
			<td align="center">prizes for the respective positions</td>
			<td align="center"><input type="button" value="save" class="save-key-value-arr"/></td>
		</tr>
		{/if}
		{if $fest_prizes_women_semicolon_separated}
		<tr class="widget-block">
			<td align="center">Fest Prizes Women</td>
			<td align="left" class="left-align">
				{foreach name=item key=key item=item from=$fest_prizes_women_semicolon_separated}
				{if $item ne ''}			
				<div class="input-block">
				<input type="text"  style="width:300px" value="{$item}" class="val" name="val[{$smarty.foreach.item.index}]"/>
				<input type="button" value="delete" class="del-val" name="del[{$smarty.foreach.item.index}]"/>
				</div>
				{/if}
				{/foreach}
				<input type="text"  style="width:300px" value="" class="new-val" name="new-val"/>
				<input type="button" value="add" class="add-val"/>
				<input type="hidden"  value="{$k_fest_prizes_women_semicolon_separated}" class="key"/>
				<input type="hidden"  value='{$j_fest_prizes_women_semicolon_separated}' class="old-val"/>
			</td>
			<td align="center">prizes for the respective positions</td>
			<td align="center"><input type="button" value="save" class="save-key-value-arr"/></td>
		</tr>
		{/if}
	</table>
	</form>
</div>
