{* $Id: order_comments.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
{include file="main/include_js.tpl" src="main/popup_product.js" src="main/overlib.js"}
{popup_init src="../skin1/main/overlib.js"}
<table width="100%"  border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td valign="top">
			<table width="98%"  border="0" align="center" cellpadding="0" cellspacing="0">
				<tr>
					<td>
						<table width="100%" height="21" border="0" cellpadding="0" cellspacing="0" class="textBlack">
							{if $changerequest_status=='Y'}
								<tr>
									<td><p  style="font-size:1.5em;color:#FF0000;background-color:#FFFFCC;"><strong>This order has change requests logged against it. Please see the Comments Log below.</strong></p></td>
								</tr>
							{/if}
							<tr>
								<td><strong>Comments Log for orderid {$orderid}</strong></td>
							</tr>
						</table>
						
						<br/>
						
						<table width="100%" height="25" border="1" cellpadding="0" cellspacing="0">
							<tr bgcolor="#ccffff">								
								<th style="font-size:14px;">Added by</th>
								<th style="font-size:14px;">Title</th>
								<th style="font-size:14px;">Type</th>
								<th style="font-size:14px;">description</th>
								<th style="font-size:14px;">New Address</th>
								<th style="font-size:14px;">Gift Wrap Message</th>
								<th style="font-size:14px;">View attachment</th>								
							</tr>
							{foreach from=$comments item=comment}
							<tr bgcolor="{cycle values="#eeddff,#d0ddff"}">					
								<td style="font-size:12px;">{$comment.commentaddedby}{" ("}{$comment.date|date_format:$config.Appearance.datetime_format}{")"}</td>
								<td style="font-size:12px;">{$comment.commenttitle}</td>
								<td style="font-size:12px;">{$comment.type}</td>
								<!--  <td style="font-size:12px;" {popup caption=$comment.date|date_format:$config.Appearance.datetime_format text=$comment.description}>{$comment.desc_part}{"  ...."}</td>-->
								<td><textarea name="query" cols=35 rows=5 readonly>{$comment.description}</textarea></td>
								<td><textarea name="query" cols=35 rows=5 readonly>{$comment.newaddress}</textarea></td>
								<td style="font-size:12px;">{$comment.giftwrapmessage}</td>
								{if $comment.attachmentname}
								<td style="font-size:12px;"><a href="{$http_location}/admin/data/{$comment.attachmentname}" TARGET = "_blank">{$comment.attachmentname}</a></td>
								{else}	
								<td style="font-size:12px;">No attachment</td>
								{/if}	
							</tr>				
							{/foreach}    
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	
	<tr>
		<td>
			<table height="25" width="98%" border="0" cellpadding="0" cellspacing="0"  valign="bottom" align=center>
				<tr >
					<td>
						<a href="javascript:void(0);"  onClick="javascript:window.open('/admin/addOrder_comment.php?orderid={$orderid}','null', 'menubar=0,resizable=1,toolbar=0,width=800,height=500')"><b>Add New Comment</b></a>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<br><br>
<div style="font-weight: bold;color:red; width: 98%; margin: 0 auto;">COD OH Calling Log</div>
<br>
<table border=1 cellspacing=0 cellpadding=0 width=98% style="border-color: #CCCCCC; border-collapse: collapse;" align="center">
	<tr height=30 bgcolor="#DFDFDF">
		<th style="font-size:14px;">Added by</th>
		<th style="font-size:14px;">Reason</th>
		<th style="font-size:14px;">Sub Reason</th>
		<th style="font-size:14px;">Trial number</th>
		<th style="font-size:14px;">Comments</th>
		<th style="font-size:14px;">Added On</th>
	</tr>
	{section name=idx loop=$comments_log}
		<tr height=30>
			<td align=center>{$comments_log[idx].created_by}</td>
			<td align=center>{$comments_log[idx].primary_reason}</td>
			<td align=center>{$comments_log[idx].reason_display_name}</td>
			<td>{$comments_log[idx].trial_number}</td>
			<td align=center>{$comments_log[idx].comment}</td>
			<td align=center>{$comments_log[idx].created_time}</td>
		</tr>
	{/section}
</table>
