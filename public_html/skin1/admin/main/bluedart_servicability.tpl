{* $Id: featured_products.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
{include file="main/include_js.tpl" src="main/popup_product.js"}
{capture name=dialog}
<div style="text-align:center;"> 
	<input type="button" id="bluedartAdminUploadBtn" value="Upload ZipCodes provided by BLUEDART">
	<!-- input type="button" id="bluedartAdminView" value="View Zip Codes servicable by BLUEDART" -->
</div>
{include file="admin/main/bluedart_upload_zipcode.tpl"}
{/capture}
{include file="dialog.tpl" title="BLUEDART Zipcode" content=$smarty.capture.dialog extra='width="100%"'}
{literal}
<script type="text/javascript">
$(document).ready(function()
{
	$("#bluedartAdminUploadBtn").click(function() {
		$("#bluedartUploadZipCsv").css("display","block");
		
	});
	$("#bluedartAdminView").click(function() {
		$("#bluedartUploadZipCsv").css("display","none");
	});
});

function submitBlueDartZipCodeUpdate() {
	
	var answer = confirm ("Update Bluedart ZIP Codes ? ");
	if (answer) {
		// Uploading zipcodes again ? then hide earlier zipcode uploaded message.
		$("#bluedartconfirmupload").css("display","none");

		// Submit form. This will cause zipcodes to be reloaded
		$('#bluedartUploadZipCsvForm').submit();
	} else {
		return false;
	} 
}
</script>
{/literal}
