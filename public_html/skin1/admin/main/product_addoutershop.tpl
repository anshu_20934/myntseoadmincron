{* $Id: product_addoutershop.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
{include file="main/include_js.tpl" src="main/popup_product.js"}

{capture name=dialog}

{if $products ne ""}
{include file="main/check_all_row.tpl" style="line-height: 170%;" form="productshopform" prefix="posted_data.+to_delete"}
{/if}

<form action="{$formaction}" method="post" name="productshopform">
<input type="hidden" name="mode" id="mode" >
<input type="hidden" name="hideshopid" id="hideshopid" value="{$editdata.shopid}"  />
<table cellpadding="3" cellspacing="1" width="60%">

<tr class="TableHead">
	<td width="5%">&nbsp;</td>
	<td width="20%">Product Id</td>
	<td width="35%" align="center">Product Name</td>
</tr>

{if $products}

{section name=prod_num loop=$products}

<tr{cycle values=", class='TableSubHead'"}>
      <input type="hidden" name="posted_data[{$products[prod_num].productid}][shopid]" value="{$products[prod_num].shopid}" />
	<td><input type="checkbox" name="posted_data[{$products[prod_num].productid}][to_delete]" /></td>
	<td align="center"><b>{$products[prod_num].productid}</b></td>
	<td align="center">{$products[prod_num].product}</td>
</tr>

{/section}

<tr>
	<td colspan="4" class="SubmitBox">

		<input type="button" value="{$lng.lbl_delete_selected|escape}" onclick="javascript: document.productshopform.mode.value = 'deleteproduct'; document.productshopform.submit();" />
	</td>
</tr>

{else}

<tr>
<td colspan="4" align="center">Record is not available</td>
</tr>

{/if}

<tr>
<td colspan="4"><br /><br />{include file="main/subheader.tpl" title="Add Product"}</td>
</tr>
<tr>

	<td colspan=3 align="left">
		<input type="hidden" name="newproductid" />

		<input type="text"  size="35" name="newproduct" />

		<input type="button" value="{$lng.lbl_browse_|escape}"
		onclick="javascript: popup_outer_shop('productshopform.newproductid', 'productshopform.newproduct');" />
	</td>
</tr>

<tr>
    <td colspan="4">Enter Product ID <input type="text"  size="20" name="p_productid" /></td>
</tr>

<tr>
	<td colspan="4" class="SubmitBox">
	<input type="submit" value="{$lng.lbl_add_new_|escape}" onclick="javascript: document.productshopform.mode.value = 'addproduct'; document.productshopform.submit();"/>
	</td>
</tr>

</table>
</form>

{/capture}
{include file="dialog.tpl" title="Product List" content=$smarty.capture.dialog extra='width="100%"'}