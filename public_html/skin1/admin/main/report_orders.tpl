{* $Id: statistics.tpl,v 1.24.2.1 2006/06/16 10:47:41 max Exp $ *}
{include file="main/include_js.tpl" src="main/calendar2.js"}
{capture name=dialog}

<form action="" method="post" name="orderreportform">
<input type="hidden" name="mode"  value="search" />

<table cellpadding="1" cellspacing="1" border="0">


<tr>
	<th align="right">{$lng.lbl_reporttype}:</th>
	<td align="right">
	     <select name='report_type'>
	      <option value="week" { if $reportType == "week"} selected {/if} >Weekly</option>
              <option value="month"    { if $reportType == "month"} selected {/if}>Monthly</option> 
	      
	       
           </select>
	</td>
	<th align="right">{$lng.lbl_status}:</th>
	<td align="right">
	    <select name='status'>
	      <option value="All" { if $stat eq 'All'} selected {/if} >All</option> 
	      <option value="A" { if $stat eq 'A'} selected {/if} >Approved</option>
	      <option value="NA" { if $stat eq 'NA'} selected {/if} >Not Approved</option>
              <option value="P" { if $stat eq 'P'} selected {/if} >In Progress</option>
	      <option value="R" { if $stat eq 'R'} selected {/if} >Recieved</option>
	      <option value="C" { if $stat eq 'C'} selected {/if} >Cancelled</option>
               
           </select>
	</td>
</tr>

<tr>
	<th align="right">{$lng.lbl_report_from}:</th>
	<td><input type="text" name='Startdate' value="{$SDate}"/><a href="javascript:cal4.popup();" ><img src="{$cdn_base}/images/cal.gif" width="16" height="16" border="0" alt="Click Here to Pick up the date"></a></td>
	<th align="right">{$lng.lbl_report_to}:</th>
	<td><input type="text" name='Enddate'  value="{$EDate}" /><a href="javascript:cal5.popup();" class=""><img src="{$cdn_base}/images/cal.gif" width="16" height="16" border="0" alt="Click Here to Pick up the date"></a></td>
</tr>

<tr>
	<td colspan="4"><input type="submit" value="{$lng.lbl_submit|escape}" /></td>
</tr>

<script language="javascript">
      var cal4 = new calendar2(document.forms['orderreportform'].elements['Startdate']);
      cal4.year_scroll = true;
      cal4.time_comp = false;

       var cal5 = new calendar2(document.forms['orderreportform'].elements['Enddate']);
      cal5.year_scroll = true;
      cal5.time_comp = false;


</script>




</table>
</form>

{/capture}
{include file="dialog.tpl" title='' content=$smarty.capture.dialog extra='width="100%"'}

<br />
<br />
{if $mode}

{capture name=dialog}
<table cellpadding="1" cellspacing="1" width="100%">

<tr class="TableHead">
	<td >{$lng.lbl_duration}</td>
	<td >{$lng.lbl_order_tracking}</td>
	<td >{$lng.lbl_status}</td>
	<td >{$lng.lbl_products}</td>
	<td >{$lng.lbl_sales_amt}</td>
</tr>


{if $orders}

{section name=num loop=$orders}

           
    {section name=idx loop=$orders[num]}

      

<tr{cycle values=', class="TableSubHead"'}>
	
        <td align="center">{$orders[num][idx].duration1}-{$orders[num][idx].duration2}</td>
	<td align="center">{$orders[num][idx].tracking}</td>
	
	{if $orders[num][idx].status eq "A" }
	      <td align="center">Approved</td>
	{elseif $orders[num][idx].status eq "NA" }
               <td align="center">Not Approved</td>
        {elseif $orders[num][idx].status eq "P"}
              <td align="center">In Progress</td>
	{elseif $orders[num][idx].status eq "R"}
		<td align="center">Recieved</td>
        {else $orders[num][idx].status eq "C"} 
	        <td align="center">Cancelled</td>
        {/if}
	<td align="center">{$orders[num][idx].no_of_products}</td>
	<td align="center">{$orders[num][idx].price*$orders[num][idx].no_of_products}</td>
</tr>
{/section}
{/section}
 <tr{cycle values=', class="TableSubHead"'}>
	<td align="center"><b>Total</b></td>
	<td align="center">&nbsp;</td>
	<td align="center">&nbsp;</td>
	<td align="center">&nbsp;</td>
	<td align="center"><b>{$total}</b></td>
</tr>



{else}

<tr>
	<td colspan="4" align="center">{$lng.txt_no_result}</td>
</tr>

{/if}


</table>


</form>

<br />

{/capture}
{include file="dialog.tpl" title='' content=$smarty.capture.dialog extra='width="100%"'}
{/if}