{* $Id: users.tpl,v 1.76.2.2 2006/06/16 10:47:41 max Exp $ *}
{include file="page_title.tpl" title='Affiliate Management'}

{$lng.txt_users_management_top_text}

<br /><br />

<!-- IN THIS SECTION -->

{include file="dialog_tools.tpl"}

<!-- IN THIS SECTION -->

<br />



<!-- SEARCH RESULTS SUMMARY -->

<a name="results" />

{if $mode eq "search"}
{if $total_items gt "0"}
{$lng.txt_N_results_found|substitute:"items":$total_items}<br />
{$lng.txt_displaying_X_Y_results|substitute:"first_item":$first_item:"last_item":$last_item}
{else}
{$lng.txt_N_results_found|substitute:"items":0}
{/if}
{/if}


<!-- SEARCH RESULTS START -->

<br /><br />

{capture name=dialog}

<div align="right">{*include file="buttons/button.tpl" button_title=$lng.lbl_search_again href="users.php"*}</div>

{if $total_pages lt 3}
<br />
{else}
{assign var="pagestr" value="&page=`$navigation_page`"}
{/if}

{include file="customer/main/navigation.tpl"}

{*include file="main/check_all_row.tpl" style="line-height: 170%;" form="processuserform" prefix="user"*}

<form action="" method="post" name="processuserform">
<input type="hidden" name="mode" value="" />
<input type="hidden" name="pagestr" value="{$pagestr|amp}" />

<table cellpadding="2" cellspacing="1" width="100%">

<tr class="TableHead">
	<td>&nbsp;</td>
	<td>Company</td>
	<td>Contact Person</td>
	<td>Contact Email</td>
	<td>URL</td>
	<td>Joined Date</td>
	<td>Content Provider</td>
	<td>ACTIVE</td>
</tr>

{section name=cat_num loop=$users}



<tr{cycle values=', class="TableSubHead"'}>
	<td width="5"><input type="checkbox" name="posted_data[{$users[cat_num].affiliate_id}][delete]" /></td>
	<td><a href="affiliate_modify.php?affId={$users[cat_num].affiliate_id}">{$users[cat_num].company_name}</a></td>
	<td>{$users[cat_num].contact_name}</td>
	<td>{$users[cat_num].contact_email}</td>
	<td>{$users[cat_num].affiliate_return_url}</td>
	<td>{$users[cat_num].joined_date|date_format}</td>
	<td nowrap="nowrap" align="center">{$users[cat_num].contentprovider}</td>
	<td align="center">
	     <select name="posted_data[{$users[cat_num].affiliate_id}][status]">
	         <option value='Y' {if $users[cat_num].active eq 'Y'}selected {/if} >Yes</option>
	         <option value='N'  {if $users[cat_num].active eq 'N'} selected {/if}>No</option>
	     </select>
	</td>
</tr>

{/section}

<tr>
	<td colspan="8" class="SubmitBox">
	<input type="button" value="{$lng.lbl_delete_selected|escape}" onclick="javascript: submitForm(this, 'delete');" />
	<input type="button" value="{$lng.lbl_update|escape}" onclick="javascript: submitForm(this, 'updatestatus');" />
	
	</td>
</tr>



</table>
</form>

<br />

{include file="customer/main/navigation.tpl"}

{/capture}
{include file="dialog.tpl" title=$lng.lbl_search_results content=$smarty.capture.dialog extra='width="100%"'}

<!-- SEARCH RESULTS START -->


<br />

