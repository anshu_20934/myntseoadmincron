{include file="page_title.tpl" title='Return SLA Editor'}
{capture name=dialog}
	<div id="success" class="notification" style="display:none; color: green;">Updated SLAs Successfully!</div>
	<div id="failure" class="notification" style="display:none; color: red;">Failed to update SLAs. Please try again later!</div>
	
	<div id="msgdiv" class="notification" style="display:none;margin-bottom:5px;"></div>
	
	<form action="" name="transition_sla_data" method="POST">
		<input type="hidden" name="action" value="update"/>
		<table border=0 cellspacing=0 cellpadding=0 width=100%>
			<tr style="background-color: #ccffff">
				<td width=5%>&nbsp;</td>
				<td width=25%>From State</td>
				<td width=25%>To State</td>
				<td width=20%>Allowed SLA</td>
				<td width=20%>Actions</td>
				<td width=5%>&nbsp;</td>
			</tr>
			{section name=idx loop=$transitions}
				<tr height=30 id="slabox_{$transitions[idx].id}">
					<td width=5%>&nbsp;</td>
					<td width=25%>{$transitions[idx].from_state}</td>
					<td width=25%>{$transitions[idx].to_state}</td>
					<td width=20%><input type=text name="sla_{$transitions[idx].id}" value="{$transitions[idx].sla}" id="sla_{$transitions[idx].id}"> Hrs</td>
					<td width=20%><a href="javascript:editTransition({$transitions[idx].id});">Edit</a>&nbsp;&nbsp;&nbsp;<a href="javascript:deleteTransition({$transitions[idx].id});">Delete</a></td>
					<td width=5%>&nbsp;</td>
				</tr>
			{/section}
			<tr height=10><td colspan=5></td></tr>
		</table>
	</form>
{/capture}
{include file="dialog.tpl" title='Returns SLA Editor' content=$smarty.capture.dialog extra='width="100%"'}
<div id="addTransitionMsgDiv" style="display:none;"></div>
<div id="addTransitionDiv" style="border:1px solid black;width:400px;padding:10px;float:left;margin-top:20px;">
	<form action="" method="post" name="cancelordersform" onsubmit="return addTransition();">
			<div style="width:98%;margin-top:10px;float:left;">
				<span style="font-weight:bold;font-size:14px;">Add new transition</span>
			</div>
			<div style="width:98%;margin-top:20px;float:left;">
				<div style="width:25%;float:left;padding-top:5px;"><span>From State : </span></div>
				<div style="width:70%;float:left">
						<select name="fromState" id="fromState">
							{foreach from=$fromStates key=reasonKey item=reasonValue}
								<option value="{$reasonKey}">{$reasonValue}</option>
							{/foreach}
						</select>
				</div>
			</div>
			<div style="width:98%;margin-top:20px;float:left;">
				<div style="width:25%;float:left;padding-top:5px;"><span>To State : </span></div>
				<div style="width:70%;float:left">
						{foreach from=$toStates key=reasonKey item=reasonValue}
			            	<select name="toState" id="toState_{$reasonKey}" class="to_state" style="display:none">
								{foreach from=$toStates.$reasonKey key=reasonKey item=reasonValue}
								<option value="{$reasonKey}">{$reasonValue}</option>
								{/foreach}
							</select>
	            		{/foreach}
				</div>
			</div>
			<div style="width:98%;margin-top:20px;float:left;">
				<div style="width:25%;float:left;padding-top:5px;"><span>Description : </span></div>
				<div style="width:70%;float:left">
						<input name="description" id="description" style="width:200px;">
				</div>
			</div>
			<div style="width:98%;margin-top:20px;float:left;">
				<div style="width:25%;float:left;padding-top:5px;"><span>Sla : </span></div>
				<div style="width:70%;float:left">
						<input name="sla" id="sla" size="5" maxlength="3">
				</div>
			</div>
			<div style="width:80%;padding-left:100px;margin-top:10px;float:left;">
				<input type='submit' name='add' value="Add">
			</div>
		<input type='hidden' name='action' value='add'>
		<input type='hidden' name='login' value='{$login}'>
	</form>
</div>

{literal}
	<script type="text/javascript">
		function editTransition(id){
			var sla = document.getElementById('sla_'+id).value;
			$.ajax({
				url:"/admin/returns/slaeditor.php",
				data:  "id="+id+"&action=update&ajaxRequest=true&sla="+sla,
				type:"POST",
				success : function(data){
					var response = eval('('+data+')');
					if(response.result == "success"){
						$("#msgdiv").css("display","block");
						$("#msgdiv").html("Transition edited successfully!");
					}else{
						$("#msgdiv").css("display","block");
						$("#msgdiv").html("Request could not be completed. please contact tech team.");
					}
				}	
			});
		}
		function deleteTransition(id){
			$.ajax({
				url:"/admin/returns/slaeditor.php",
				data:  "id="+id+"&action=delete&ajaxRequest=true",
				type:"POST",
				success : function(data){
					var response = eval('('+data+')');
					if(response.result == "success"){
						$("#msgdiv").css("display","block");
						$("#msgdiv").html("Transition deleted successfully!");
						$("#slabox_"+id).html("");
					}else{
						$("#msgdiv").css("display","block");
						$("#msgdiv").html("Request could not be completed. please contact tech team.");
					}
				}	
			});
		}
		function addTransition(){
			var fromState = document.getElementById('fromState').value;
			var toState = document.getElementById('toState').value;
			var description = document.getElementById('description').value;
			var sla = document.getElementById('sla').value;
			$.ajax({
				url:"/admin/returns/slaeditor.php",
				data:  "action=add&ajaxRequest=true&description="+description+"&sla="+sla+"&fromState"+fromState+"&toState="+toState,
				type:"POST",
				success : function(data){
					var response = eval('('+data+')');
					if(response.result == "success"){
						document.getElementById('fromState').value = "";
						document.getElementById('toState').value = "";
						document.getElementById('description').value = "";
						document.getElementById('sla').value = "";
						$("#addTransitionMsgDiv").css("display","block");
						$("#addTransitionMsgDiv").html("Transition deleted successfully!");
					}else{
						$("#addTransitionMsgDiv").css("display","block");
						$("#addTransitionMsgDiv").html("Request could not be completed."+response.message);
					}
				}	
			});
		}	
		$(document).ready(function() {
		var fromState = document.getElementById("fromState").value;
		$(".to_state").css("display","none");
		$(".to_state").attr('disabled', 'disabled');
		$("#toState_"+fromState).css("display","inline");
		$("#toState_"+fromState).removeAttr('disabled');

		$("#fromState").change(function(){
			var fromState = document.getElementById("fromState").value;
			$(".to_state").css("display","none");
			$(".to_state").attr('disabled', 'disabled');
			
			$("#toState_"+fromState).css("display","inline");
			$("#toState_"+fromState).removeAttr('disabled');
		});
		
		$("#sla").keydown(function(event) {
	        if ( event.keyCode == 46 || event.keyCode == 8 ) {

	        }
	        else {
	            if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
	                event.preventDefault(); 
	            }   
	        }
	    });
	});
	
</script>
{/literal}