Ext.onReady(function() {
        Ext.QuickTips.init();

        var cancelIndividualOrdersPanel = new Ext.form.FormPanel({
    		title : 'Cancel Individual Lost Orders',
    		style: '{text-align:left; padding:25px;}',
    		id : 'formPanel1',
    		autoHeight : true,
    		collapsible : true,
    		border : true,
    		bodyStyle : 'padding: 25px; background-color: #ccddff;',
    		width : 1000,
    		items : [{
    			layout : 'table',
    			border : false,
    			layoutConfig : {columns : 4},
    			defaults : {width : 500, border : false, height : 40,
    				layout : 'form', bodyStyle : 'padding-left : 15px; background-color: #ccddff;'},
    			items : [{
    				items : [{
    					xtype : 'numberfield',
    		        	id : 'order_id',
    		        	fieldLabel : 'OrderId',
    		        	emptyText : 'Enter OrderId',
    		        	allowBlank : false
    				}]
    			}, {
    				items :[{
    					xtype : 'combo',
    		        	store : new Ext.data.JsonStore({
    		        		fields : ['cancel_reason', 'cancel_reason_desc'],
    		        		data : cancellation_codes,
    		        	}),
    		        	emptyText : 'Select Lost Reason',
    		        	typeAhead : true,
    		        	displayField : 'cancel_reason_desc',
    		        	valueField : 'cancel_reason',
    		        	fieldLabel : 'Lost Reason',
    		        	name : 'lost_reason',
    		        	mode: 'local',
    		        	triggerAction: 'all',
    		        	allowBlank : false
    				}]
    			}]
    		}],
    		buttons : [{
    			text : 'Cancel Lost Order',
    			handler : function() {
    				var form = cancelIndividualOrdersPanel.getForm();
    				if (form.isValid()) {
    					cancelorders();
    				}
    			}
    		}]
    	});

        function cancelorders() {
        	var form = cancelIndividualOrdersPanel.getForm();
    		var params = {};
    		params["order_id"] = form.findField("order_id").getValue();
    		params["lost_reason"] = form.findField("lost_reason").getValue();
        	params["action"] = "cancelSingleOrder";
        	downloadMask = new Ext.LoadMask(Ext.get('workflowPanel'), {msg: "Cancelling the Lost Order"});
        	downloadMask.show();
        	Ext.Ajax.request({
        		url : 'cancel-lost-orders.php',
        		params : params,
        		success : function(response, action) {
        			var jsonObj = Ext.util.JSON.decode(response.responseText);
        			downloadMask.hide();
        			if(jsonObj[0] == "success") {
        				Ext.MessageBox.alert('success', jsonObj[1]);
        				form.findField("order_id").reset();
        				form.findField("lost_reason").reset();
        			} else {
        				Ext.Msg.show({
				    		    title:      'Error!',
				    		    msg:        jsonObj[1],
				    		    icon:        Ext.MessageBox.INFO
							});
        			}
        		}
        	});
        }
        
        var cancelBulkOrdersPanel = new Ext.form.FormPanel({
        	fileUpload: true,
        	title : 'Cancel Lost Orders in bulk',
    		style: '{text-align:left; padding:25px;}',
    		id : 'formPanel',
    		autoHeight : true,
    		collapsible : true,
    		border : false,
    		bodyStyle : 'background-color: #ccddff;',
    		width : 1000,
    		border : false,
    		items : [{
    			layout : 'table',
    			defaults : {width : 1000, border : false, height : 120,
    				layout : 'form', bodyStyle : 'padding:25px 20px; background-color: #ccddff;'},
    			items : [{
    				items : [{
			        	title: 'Upload the file containing OrderIds and Lost-reasons in .csv format',
			     		frame: true,
			     		width: 700,
			            layout: 'table',
			            fieldLabel : 'Lost Orders',
			     		items: [{
			     			width : 250,
			     			items: [{
			     				xtype : 'fileuploadfield',
					     		name : 'lostorders',
			     				allowBlank: false,
			     		        bodyPadding: '50 10 0',
			     				emptyText : 'Lost Orders and Reasons',
			     				buttonText : ' Select a File ',
			     			}]
			     		}, {
			     			buttons: [{
			     				text: 'Upload',
			     				handler: function() {
			     					var form = cancelBulkOrdersPanel.getForm();
			     					if(form.isValid()){
			                             form.submit({
			                             	url : 'cancel-lost-orders.php',
			                                waitMsg: 'Uploading file...',
			                                success: function(form, action){

			         						}
			                             });
			                             processMask = new Ext.LoadMask(Ext.get('workflowPanel'), {msg: "Processing Orders ..."});
					     				 processMask.show();
			                             timerid = setInterval(trackStatus, 2000);
			     					}
			     				}
			     			}, {
			     			    text: 'Reset',
			     				handler: function() {
			     					cancelBulkOrdersPanel.getForm().reset();
			     				}
			     		    }]
			     		}]
    				}]
    			}, {
    				items : [{
     					xtype : 'textfield',
    					name : 'action',
    					value : 'upload',
    					hidden : true
    				}]
    			}, {
    				items : [{
     					xtype : 'textfield',
    					name : 'progresskey',
    					value : progresskey,
    					hidden : true
    				}]
    			}]
    		}]
    	});
        
    	function trackStatus() {
    		Ext.Ajax.request({
    			url : "/admin/getprogressinfo.php?type=status&progresskey="+progresskey,
    			method : 'GET',
    			mask: false,
    		    success : function(response, action) {
    		    	var jsonObj = Ext.util.JSON.decode(response.responseText);
    		    	if(jsonObj.status == 'DONE'){
    		    		clearInterval(timerid);
    		    		processMask.hide();
    		    		var jsonObj = Ext.util.JSON.decode(response.responseText);
    					if(jsonObj.success) {
    						Ext.Msg.show({
 				    		    title:      'Success!',
 				    		    msg:        'Data posted successfully' + jsonObj.message,
 				    		    icon:        Ext.MessageBox.INFO
 							});
    					} else {
    						Ext.MessageBox.alert('Error', jsonObj.message);
    					}
    		    		
    		    	}
    			}
    		});
    	}
        

        var WorkflowPanel = new Ext.Panel({
        	id: 'workflowPanel',
        	title : 'Lost Orders Workflow',
            renderTo : 'contentPanel',
            border : true,
            style: '{text-align:center;}',
            items: [cancelIndividualOrdersPanel, cancelBulkOrdersPanel],
        });
        
        var mainPanel = new Ext.Panel({
                renderTo : 'contentPanel',
                border : false,
        });

});