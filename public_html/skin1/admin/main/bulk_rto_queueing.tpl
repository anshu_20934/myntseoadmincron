{include file="main/include_js.tpl" src="main/popup_product.js"}
<link rel="stylesheet" href="{$SkinDir}/skin1_admin_operations.css" />

{literal}
<script type="text/javascript">

    function resetScanning(type) {
        $("#return_"+type+"_div").hide();  
        $("#"+type+"_table_div").hide();    
        $("#"+type+"_table_contents").html('');
        $("#scanned_"+type).val('');
        
        $("#results_table_div").hide();
	    $("#results_table_contents").html(''); 
	    $("#queue_rtos").attr('disabled', true);
	    $("#valid_rto_orderids").val('');
    }
    
    $(document).ready(function(){
        $("#orderid").focus();
        $('#preloader').hide(); 
         
        resetScanning('orders');
        resetScanning('items');
        
        $('#preloader')
            .ajaxStart(function(){
                $(this).show();
            }).ajaxStop(function(){
                $(this).hide();
            }); 
            
        $("input[name='scan_by']").change(function(){
            $("#items_table").html('');
            if( $(this).val() == "scan_by_orderid" ) {
                $('#return_orders_div').show();
                resetScanning('items');
            } else {
                $('#return_items_div').show();   
                resetScanning('orders');
            }   
        });
        
        $("#orderid").keydown(function(event) {
            // Allow only backspace and delete
            if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode ==13) {
                // let it happen, don't do anything
            }
            else {
                // Ensure that it is a number and stop the keypress
                if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                    event.preventDefault(); 
                }   
            }
        });
        
        $("#itembarcode").keydown(function(event) {
            // Allow only backspace and delete
            if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode ==13) {
                // let it happen, don't do anything
            }
            else {
                // Ensure that it is a number and stop the keypress
                if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                    event.preventDefault(); 
                }   
            }
        });
    });
    
    function createOrderIdsHtml(orderIds) {
        var orderIds_html = "";
        for(var i in orderIds){
            orderIds_html +=  orderIds==""?"":"<br>";
            orderIds_html +=  orderIds[i];
        }
        
        return (orderIds_html==""?"-":orderIds_html);
    }
    
    function pushOrdersToRTOTracker() {
        $('#preloaderText').text('Pushing...');
        $("#return_orders_div").hide();
        $("#orders_table_div").hide();
        var valid_rto_orderids = $("#valid_rto_orderids").val();
        var adminLogin = $("#admin_login").val();
        $.ajax({
            type: "POST",
            url: "",
            data: "action=queue_rto_orders&orderids="+valid_rto_orderids+"&login="+adminLogin+"&warehouseid="+$("#warehouse_id").val(),
            success: function(response){
                
                var queuedOrdersCOD = response['queued']['cod'];
                var queuedOrdersNONCOD = response['queued']['on'];
                
                var failedOrdersCOD = response['failed']['cod'];
                var failedOrdersNONCOD = response['failed']['on'];
                
                var countsRow = '<tr><td align=center>Total</td>';
                countsRow += '<td align=center>'+ queuedOrdersCOD.length+'</td>';
                countsRow += '<td align=center>'+queuedOrdersNONCOD.length+'</td>';
                countsRow += '<td align=center>'+failedOrdersCOD.length+'</td>';
                countsRow += '<td align=center>'+failedOrdersNONCOD.length+'</td></tr>';
                
                $("#results_table_contents").append(countsRow);
                
                var orderidsRow = '<tr><td align=center>Order Ids</td>';
                orderidsRow += '<td align=center>'+createOrderIdsHtml(queuedOrdersCOD)+'</td>';
                orderidsRow += '<td align=center>'+createOrderIdsHtml(queuedOrdersNONCOD)+'</td>';
                orderidsRow += '<td align=center>'+createOrderIdsHtml(failedOrdersCOD)+'</td>';
                orderidsRow += '<td align=center>'+createOrderIdsHtml(failedOrdersNONCOD)+'</td>';
                orderidsRow += '</tr>';
                
                $("#results_table_contents").append(orderidsRow);
                $('#results_table_div').show();
            }
        });
    }
    
    function clearScanning() {
        this.window.location.reload();
    }
    
    function orderScanned(){
        var orderid = $("#orderid").val();
        if(orderid == null || orderid.trim() == "" || orderid == "") {
            alert("Blank Order Id");
            return false;
        }

        if($("#warehouse_id").val() == "") {
            alert("Select an Ops Location");
            return false;
        }

        if($("#scanned_orders").val().indexOf(orderid) != -1){
            alert("ERROR: Order already scanned: " + orderid);
            $("#orderid").val('');
            return false;
        }
        
        $('#preloaderText').text('Receiving...');
        $.ajax({
            type: "POST",
            url: "",
            data: "action=order_check&orderid="+orderid+"&warehouseid="+$("#warehouse_id").val(),
            success: function(response){
                var bgcolorcode = response['color'];
                if(response['status'] == "Success") {
                    var valid_rto_orderids = $("#valid_rto_orderids").val();
	                valid_rto_orderids += valid_rto_orderids==""?"":",";
	                valid_rto_orderids += response['orderid'];
	                $("#valid_rto_orderids").val(valid_rto_orderids);
	                $("#queue_rtos").attr('disabled', false);
                }
                
                var newRow = '<tr bgcolor="'+bgcolorcode+'"><td align=center>'+response['orderid']+'</td>';
                newRow += '<td align=center>'+response['type']+'</td>';
                newRow += '<td align=center>'+response['shippingdate']+'</td>';
                newRow += '<td align=center>'+response['remarks']+'</td></tr>';
                $("#orders_table_contents").prepend(newRow);
                $('#orders_table_div').show();
                var scanned_orders = $("#scanned_orders").val();
                scanned_orders += scanned_orders==""?"":",";
                scanned_orders += response['orderid'];
                $("#scanned_orders").val(scanned_orders);
                if(scanned_orders != "")
                    $("#warehouse_id").attr("disabled", true);
            }
        });
         
        $("#orderid").val('');
        $("#orderid").focus();
        return false;
    }
    
    function itemScanned(){
        var itembarcode = $("#itembarcode").val();
        if(itembarcode == null || itembarcode.trim() == "" || itembarcode == "") {
            alert("Blank Item Barcode");
            return false;
        }

        if($("#warehouse_id").val() == "") {
            alert("Select an Ops Location");
            return false;
        }

        if($("#scanned_items").val().indexOf(itembarcode) != -1){
            alert("ERROR: Item already scanned: " + itembarcode);
            $("#itembarcode").val('');
            return false;
        }
        
        $('#preloaderText').text('Receiving...');
        $.ajax({
            type: "POST",
            url: "",
            data: "action=item_check&itembarcode="+itembarcode+"&warehouseid="+$("#warehouse_id").val(),
            success: function(response){
                var bgcolorcode = "";
                if(response['status'] == "Failure") {
                    bgcolorcode = "#F21633";
                } else {
                    bgcolorcode = "#22B804";
                }
                
                var newRow = '<tr bgcolor="'+bgcolorcode+'"><td align=center>'+response['itembarcode']+'</td>';
                newRow += '<td align=center>'+response['remarks']+'</td></tr>';
                $("#items_table_contents").prepend(newRow);
                $('#items_table_div').show();
                var scanned_items = $("#scanned_items").val();
                scanned_items += scanned_items==""?"":",";
                scanned_items += response['itembarcode'];
                $("#scanned_items").val(scanned_items);
                if(scanned_items != "")
                    $("#warehouse_id").attr("disabled", true);
            }
        });
         
        $("#itembarcode").val('');
        $("#itembarcode").focus();
        return false;
    }
    
</script>
{/literal}
<p align="center"><font color="red" size="30">Use REJOY for RTO processing</font></p>
{*
<input type=hidden name="admin_login" id="admin_login" value="{$login}">

<div id="preloader">
    <div class="overlay" style="display:block;_position:absolute;_width:1300px;_height:1500px;">&nbsp;</div>
    <div style="width: 100%; position: absolute; left: 40%; top: 40%; font-size: 150%; font-weight:bold; color: green;" >
        <label id="preloaderText" style="font-family: Verdana; font-weight: bold; font-size: 20px;">Receiving...</label>    
    </div>
</div>

<div style="float:left; font-weight: bold; font-size: 14px; width: 200px;"><b>Select Operations Location:</b></div>
<select id="warehouse_id" name="warehouse_id" style="width: 150px; padding: 3px; float:left;">
    <option value="">--Select Ops Location--</option>
    {section name=idx loop=$warehouses}
        <option value="{$warehouses[idx].id}">{$warehouses[idx].name}</option>
    {/section}
</select>
<div class="clear"></div>
<br><br>
<div style="float:left; font-weight: bold; font-size: 14px; width: 200px;"><b>Scanning received units by:</b></div>
<div style="float:left;"><input type="radio" value="scan_by_orderid" name="scan_by"> Order Id &nbsp; &nbsp; 
<input type="radio" value="scan_by_itembarcode" name="scan_by"> Item Barcode </div>
<div class="clear"></div>
<br><br>

<div id="return_orders_div" style="margin: 10px 0px;">
    <input type=hidden name="scanned_orders" id="scanned_orders" value="">
    <input type=hidden name="valid_rto_orderids" id="valid_rto_orderids" value="">
    <form action="" method="POST" onsubmit="return orderScanned();">
        <input type="hidden" id="action" name="action" value="order_check"> 
        <div style="float:left; font-weight: bold; font-size: 14px; width: 200px;">Enter Order-Id:</div>
        <input type="text" id="orderid" name="orderid" style="float:left; width: 340px; color: #000000; font-weight: bold; font-size: 14px; background-color: #CEE3F6;"/>
        <input type="submit" name="order_scan_btn" value="Receive Order" style="float:left; font-size: 12px;">
        <input type="button" name="reset_btn" value="Reset" style="float:left; font-size: 12px;" onclick="clearScanning();">
        <div class="clear"></div>
    </form>
</div>
<div id="orders_table_div" style="margin-top: 20px;">  
    <input type="button" id="queue_rtos" name="queue_rtos" value="Push Orders To RTO Tracker" onclick="pushOrdersToRTOTracker();" disabled> <br/><br/>
    <table id="orders_table_header" border=1 width=800>
        <thead bgcolor="#d0d0d0">
            <th width="150">Order</th><th width="150">Order Type</th>
            <th width="150">Shipping Date</th><th width="300">Remarks</th>
        </thead>
        <tbody id="orders_table_contents"></tbody>    
    </table>
</div>

<div id="return_items_div" style="margin: 10px 0px;">
    <form action="" method="POST" onsubmit="return itemScanned();">
	    <input type=hidden name="scanned_items" id="scanned_items" value="">
	    <div style="float:left; font-weight: bold; font-size: 14px; width: 200px;">Enter Item Barcode:</div>
	    <input type="text" id="itembarcode" name="itembarcode" style="float:left; width: 340px; color: #000000; font-weight: bold; font-size: 14px; background-color: #CEE3F6;"/>
	    <input type="submit" name="item_scan_btn" value="Receive Item" style="float:left; font-size: 12px;">
	    <input type="button" name="reset_btn" value="Reset" style="float:left; font-size: 12px;" onclick="clearScanning();">
	    <div class="clear"></div>
	</form>
</div>
<div id="items_table_div" style="margin-top: 20px;">  
    <table id="items_table_header" border=1 width=450>
        <thead bgcolor="#d0d0d0"><th width="200">Item Barcode</th><th width="300">Remarks</th></thead>
        <tbody id="items_table_contents"></tbody>
    </table>
</div>

<div id="results_table_div" style="margin-top: 20px;">
    <table id="results_table_header" border=1 width=900>
        <thead bgcolor="#d0d0d0">
            <th width="100">&nbsp;</th><th width="200">RTO Queued Successfully(COD)</th>
            <th width="200">RTO Queued Successfully (NON COD)</th>
            <th width="200">RTO Queueing Failed (COD)</th><th width="200">RTO Queueing Failed (NON COD)</th>
        </thead>
        <tbody id="results_table_contents"></tbody>
    </table>
    <div  style="margin-top: 30px;"><a href="">&laquo; Go Back to Scan Returns</a></div>
</div>
*}