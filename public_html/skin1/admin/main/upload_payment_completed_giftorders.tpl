{include file="main/include_js.tpl" src="main/popup_product.js"}

{literal}
    <script type="text/javascript">
	    $(function() {
	       $("#uploadxls").click( function() {
                var overlay = new ProcessingOverlay();
                overlay.show();
	       });
	       $("#confirm_queue").click( function() {
                var overlay = new ProcessingOverlay();
                overlay.show();
           });
	    });
    </script>
{/literal}

{capture name=dialog}
{if $action eq "upload"}
    {if $errorMsg }
        <div style="color: red; font-weight: bold;">{$errorMsg}</div>
    {/if}
	<form action="" method="post" name="queue_orders" enctype="multipart/form-data" >
		Upload the csv file : <input type="file" name="orderdetail" >{if $message}<font color='red'>{$message}</font>{/if}
		<input type='submit'  name='uploadxls' id='uploadxls' value="Upload">
		<input type='hidden' name='action' value='verify'>
	</form>
	
	{if $summary}
	   <div id="summary" style="margin-top: 20px;"> 
	       {$summary}
	   </div>
	{/if}

	{if $googleEcommerceTracking }	
		<script type="text/javascript">
    		var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
    		document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
    	</script>

		<script type="text/javascript">
			var pageTracker = _gat._getTracker("{$ga_acc}");
            pageTracker._trackPageview();
			{section name=idx loop=$ecomtrackingdata}
                pageTracker._addTrans(
            		"{$ecomtrackingdata[idx].orderid}",
            		"{$ecomtrackingdata[idx].userinfo.b_firstname} {$ecomtrackingdata[idx].userinfo.b_lastname}", 
            		"{$ecomtrackingdata[idx].amountafterdiscount|string_format:'%.2f'}",  
        			"{$ecomtrackingdata[idx].vat}",        
        			"{$ecomtrackingdata[idx].shippingRate}",    
        			"{$ecomtrackingdata[idx].userinfo.s_city}",           
        			"{$ecomtrackingdata[idx].userinfo.s_state}",      
        			"{$ecomtrackingdata[idx].userinfo.s_country}" 
        		);
                {assign var='netTotalPrice' value=0}
            	{assign var='productidDelimString' value=''}
            	{assign var='productTypeDelimString' value=''}
                
            	{foreach name=outer k=key item=product from=$ecomtrackingdata[idx].productsInCart}
                    {foreach key=key item=item from=$product}
                    	{if $key eq 'productId'}
                    		{assign var='productId' value=$item}
                    		{assign var='productidDelimString' value=$productidDelimString|cat:$productId|cat:"|"}
                    	{/if}

                    	{if $key eq 'productStyleName'}
                    		{assign var='productStyleName' value=$item}
                    	{/if}

                    	{if $key eq 'quantity'}
                    		{assign var='quantity' value=$item}
                    	{/if}

                    	{if $key eq 'productTypeLabel'}
                    		{assign var='productTypeLabel' value=$item}
                    		{assign var='productTypeDelimString' value=$productTypeDelimString|cat:$productTypeLabel|cat:"|"}
                    	{/if}

                    	{if $key eq 'totalPrice'}
                    		{assign var='totalPrice' value=$item}
                    		{assign var='netTotalPrice' value=$netTotalPrice+$totalPrice}
                    	{/if}
                    {/foreach}	
                    pageTracker._addItem(
            			"{$ecomtrackingdata[idx].orderid}",   
            			"{$productId}",    
            			"{$productStyleName}",
            			"{$productTypeLabel}",
            			"{$totalPrice}",   
            			"{$quantity}"   
            		);
                {/foreach}
                pageTracker._trackTrans();
			{/section}
		</script>
	{/if}
{elseif $action eq verify}
	
	{if $dataFormatError}
		<b>{$dataFormatError}</b>
	{else}	
		<form action="" method="post" name="queue_orders" enctype="multipart/form-data" >
			<table cellpadding="2" cellspacing="1" width="100%">
	            <input type='hidden' name='action' value='confirm'>
	            <tr class="TableHead">
	                <td>Orderids to queue</td>
				</tr>
				{if $orderListDisplay|@count > 0}
		            {section name=idx loop=$orderListDisplay}
				     	<tr{cycle values=", class='TableSubHead'"}>
		                    <td align="center"><input type=hidden name = orderids[] value="{$orderListDisplay[idx]}"><a href="order.php?orderid={$orderListDisplay[idx]}&mode=gift_order" target="_blank">{$orderListDisplay[idx]}</a></td>
						</tr>
		            {/section}
	                <tr><td align=right><input type='submit' name='confirm' id="confirm_queue" value='Confirm' /></td></tr>
	            {else}
	               <tr{cycle values=", class='TableSubHead'"}><td align="center">No Orders to Queue.</td></tr>
	            {/if}
			</table>
		</form>
		
		<div style="height: 30px;"><hr/></div>	
		
		{if $failedOrderListDisplay}
			<table cellpadding="2" cellspacing="1" width="100%">
				<tr class="TableHead">
					<td>Failed Orderids</td><td>Reason</td>
				</tr>
				
				{section name=idx1 loop=$failedOrderListDisplay}
					<tr{cycle values=", class='TableSubHead'"}>
						<td align="center">
	                        <input type=hidden name=failedorderids[] value="{$failedOrderListDisplay[idx1].orderid}"/>
	                        <a href="order.php?orderid={$failedOrderListDisplay[idx1].orderid}&mode=gift_order">{$failedOrderListDisplay[idx1].orderid}</a>
						</td>
						<td align="center">
							<b>{$failedOrderListDisplay[idx1].msg}</b>
						</td>
					</tr>
				{/section}
			</table>
		{/if}
	{/if}
{/if}
{/capture}
{include file="dialog.tpl" title="Upload Payment Recieved Orders csv to queue" content=$smarty.capture.dialog extra='width="100%"'}
