{include file="main/include_js.tpl" src="main/popup_category.js"}
{include file="main/include_js.tpl" src="main/calendar2.js"}

{capture name=dialog}

<form action="offline_orders.php" method="post" name="modifyform">

<input type="hidden" name="mode"  id = "mode" />

<table cellpadding="4" cellspacing="0" width="100%" >

<tr>
	<td colspan="2"><br />{include file="main/subheader.tpl" title='Customer details'}</td>
</tr>

<tr> 
	
	<td class="FormButton" nowrap="nowrap">{$lng.lbl_exist_cust}:<span class="Star">*</span></td>
	<td class="ProductDetails">
		<select name="ecust" id="ecust" onChange="javascript:loadCustomer();">
			<option value="0">--select customer--</option>
			{if isset($smarty.post.ecust) && !isset($offlinecustomer.login)}
				{html_options options=$custarr selected=$smarty.post.ecust}
			{elseif isset($offlinecustomer.login)}
				{html_options options=$custarr selected=$offlinecustomer.login}
			{else}
				{html_options options=$custarr } 
			{/if} 
		</select>
	</td>
</tr>

<tr> 
	
	<td class="FormButton" nowrap="nowrap">{$lng.lbl_first_name}:<span class="Star">*</span></td>
	<td class="ProductDetails"> 
	<input type="text" name="firstname" id="firstname" 
        value="{if $offlinecustomer.firstname == ""}{$smarty.post.firstname}{else}{$offlinecustomer.firstname}{/if}" />
	</td>
</tr>


<tr> 
	
	<td class="FormButton" nowrap="nowrap">{$lng.lbl_last_name}:<span class="Star">*</span></td>
	<td class="ProductDetails"> 
	<input type="text" name="lastname" id="lastname" 
        value="{if $offlinecustomer.lastname == ""}{$smarty.post.lastname}{else}{$offlinecustomer.lastname}{/if}" />
	</td>
</tr>


<tr> 
	
	<td class="FormButton" nowrap="nowrap">{$lng.lbl_email}:<span class="Star">*</span></td>
	<td class="ProductDetails"> 
	{if $offlinecustomer.login != ""}
		<input type="hidden" name="email" id="email" value={$offlinecustomer.login}>
	<b>{$offlinecustomer.login}</b>
	{else}
		<input type="text" name="email" id="email" value="{$smarty.post.email}"  />
	{/if}

	</td>
</tr>
<tr> 
	
	<td class="FormButton" nowrap="nowrap">Company name:<span class="Star">*</span></td>
	<td class="ProductDetails"> 
	<input type="text" name="company" id="company" 
        value="{if $offlinecustomer.company == ""}{$smarty.post.company}{else}{$offlinecustomer.company}{/if}"  />
	</td>
</tr>
<tr> 
	<td class="FormButton" nowrap="nowrap">{$lng.lbl_order_date}:</td>
	<td> 
	<input type="text" id='orderdate' name='orderdate' value="{if $offlinecustomer.first_login == ""}{$smarty.post.orderdate}{else}{$offlinecustomer.first_login}{/if}"/>
	<a href="javascript:cal4.popup();" ><img src="{$cdn_base}/images/cal.gif" width="16" height="16" border="0" alt="Click Here to Pick up the date"></a>
	</td>
</tr>



<tr>
<!-- shipping details -->
<td width="50%">
<table  cellspacing="0" width="100%" >

<tr> 
	<td colspan="2"><b>Shipping details</b></td>
</tr>
<tr>
	<td class="FormButton" >{$lng.lbl_address}:</td>
	<td class="ProductDetails">
        <textarea name="s_address" id="s_address" cols=45 rows=5 >{if $offlinecustomer.s_address == ""}{$smarty.post.s_address}{else}{$offlinecustomer.s_address}{/if}</textarea>
        </td>
</tr>
<tr>
	<td class="FormButton" >{$lng.lbl_city}:</td>
	<td class="ProductDetails">
        <input type="text" name="s_city" id="s_city"
        value="{if $offlinecustomer.s_city == ""}{$smarty.post.s_city}{else}{$offlinecustomer.s_city}{/if}"  /></td>
</tr>
<tr> 
	<td class="FormButton" >{$lng.lbl_state} :</td>
	<td class="ProductDetails">
            
	<select name="s_state" id="s_state">
            {if isset($smarty.post.s_state) && !isset($offlinecustomer.s_state)}
                {html_options options=$statesarr selected=$s_state}
            {elseif isset($offlinecustomer.s_state)}
                {html_options options=$statesarr selected=$s_state}
            {else}
                {html_options options=$statesarr } 
            {/if} 
	    
    </select>
	</td>
 </tr> 
 <tr> 
	<td class="FormButton" >{$lng.lbl_country} :</td>
	<td class="ProductDetails">
	 <input type="text" name="s_country" id="s_country" value="India" />
	</td>
 </tr>
 <tr>
	<td class="FormButton" >Zip Code:</td>
	<td class="ProductDetails">
        <input type="text" name="s_zip" id="s_zip"
        value="{if $offlinecustomer.s_zipcode == ""}{$smarty.post.s_zip}{else}{$offlinecustomer.s_zipcode}{/if}" /></td>
</tr>
<tr>
	<td class="FormButton" >{$lng.lbl_mobile}:</td>
	<td class="ProductDetails"><input type="text" name="s_mobile" id="s_mobile"
        value="{if $offlinecustomer.mobile == ""}{$smarty.post.s_mobile}{else}{$offlinecustomer.mobile}{/if}" /></td>
</tr>
<tr>
	<td class="FormButton" >{$lng.lbl_phone}:</td>
	<td class="ProductDetails">
        <input type="text" name="s_phone" id="s_phone" 
        value="{if $offlinecustomer.phone == ""}{$smarty.post.s_phone}{else}{$offlinecustomer.phone}{/if}" /></td>
</tr>
<tr> 
	<td colspan="2">
        <input type="checkbox" value="Y" name="sameAsShip" id="sameAsShip" onClick="javascript:sameAsShipping();"
        {if $offlinecustomer.sameAsShip == "Y"} checked='checked' {elseif $smarty.post.sameAsShip == 'Y'} checked='checked' {/if} />
        <strong>Same as shipping address</strong>
	</td>
</tr>
</table>
</td>
<!-- end shipping details-->

<!-- begin billing details-->
<td width="50%" valign="top">
<table cellpadding="4" cellspacing="0" width="100%">


<tr> 
	<td colspan="2"><b>Billing details</b></td>
</tr>

<tr>
	<td class="FormButton" nowrap="nowrap">{$lng.lbl_address}:</td>
	<td class="ProductDetails">
        <textarea name="b_address" id="b_address" cols=45 rows=5  >{if $offlinecustomer.b_address == ""}{$smarty.post.b_address}{else}{$offlinecustomer.b_address}{/if}</textarea> </td>
</tr>
<!-- end  -->

<tr>
	<td class="FormButton" nowrap="nowrap">{$lng.lbl_city}:</td>
	<td class="ProductDetails">
        <input type="text" name="b_city" id="b_city" 
        value="{if $offlinecustomer.b_city == ""}{$smarty.post.b_city}{else}{$offlinecustomer.b_city}{/if}" /></td>
</tr>
 <tr> 
	<td class="FormButton" nowrap="nowrap">{$lng.lbl_state} :</td>
	<td class="ProductDetails">
	<select name="b_state" id="b_state">
            {if isset($smarty.post.b_state) && !isset($offlinecustomer.b_state)}
                {html_options options=$statesarr selected=$b_state}
            {elseif isset($offlinecustomer.b_state)}
                {html_options options=$statesarr selected=$b_state}
            {else}
                {html_options options=$statesarr } 
            {/if}
        </select>
	</td>
 </tr> 
 
<tr> 
	<td class="FormButton" nowrap="nowrap">{$lng.lbl_country} :</td>
	<td class="ProductDetails">
	 <input type="text" name="b_country" id="b_country" value="India" />
	</td>
 </tr> 
 
 <tr>
	<td class="FormButton" nowrap="nowrap">Zip Code:</td>
	<td class="ProductDetails"><input type="text" name="b_zip" id="b_zip" 
        value="{if $offlinecustomer.b_zipcode == ""}{$smarty.post.b_zip}{else}{$offlinecustomer.b_zipcode}{/if}" /></td>
</tr>

</table>  


</td>
</tr>
<!-- end billing details-->

<tr>
	{if $custemail}
		<td align="center"><br /><input type="submit" value=" {$lng.lbl_update} "  
	onClick="return formValidation('updatecustomer');"/></td>
	{else}
		<td align="center"><br /><input type="submit" value=" {$lng.lbl_save} "  
	onClick="return formValidation('savecustomer');"/></td>
	{/if}
</tr>

</table>
<script language="javascript">
      var cal4 = new calendar2(document.forms['modifyform'].elements['orderdate']);
      cal4.year_scroll = true;
      cal4.time_comp = false;
</script>
</form>

{/capture}
{include file="dialog.tpl" title="Offline Customer" content=$smarty.capture.dialog extra='width="100%"'}