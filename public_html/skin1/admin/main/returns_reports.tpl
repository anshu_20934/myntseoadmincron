{literal}
	<style>
		.mymyntra-ajax-loader-icon {
		    width: 100%;
		    height: 100%;
		    background: #fff url(http://myntra.myntassets.com/skin1/myntra_images/loading_240_320.gif);
		}
	</style>
{/literal}
<div style="float:left;">
	<ul>
		<li><a class="button" href="javascript:void(1);" onclick="javascript:getReport('pin_codes');"> Pin Codes</a></li>
		<li><a class="button" href="javascript:void(1);" onclick="javascript:getReport('conversion_rate');"> Returns delivery conversion rate</a></li>
		<li><a class="button" href="javascript:void(1);" onclick="javascript:getReport('contribution_ratio');"> Contribution of returns requests across the different returns reasons</a></li>
		<li><a class="button" href="javascript:void(1);" onclick="javascript:getReport('avg_received_time');"> Avg Time to recieve return at WH</a></li>
		<li><a class="button" href="javascript:void(1);" onclick="javascript:getReport('avg_processing_time');"> Avg time to process once recieved at WH</a></li>
		<li><a class="button" href="javascript:void(1);" onclick="javascript:getReport('qa_pass_rate');"> Qa pass rate</a></li>
		<li><a class="button" href="javascript:void(1);" onclick="javascript:getReport('brand_wise_breakup');"> Brand wise distribution</a></li>
		<li><a class="button" href="javascript:void(1);" onclick="javascript:getReport('category_wise_breakup');"> Category wise distribution</a></li>
		
	</ul>
</div>
<div class="mymyntra-ajax-loader" style="display:none;float:left;clear:both;height:300px;width: 150px;">
	<div class="mymyntra-ajax-loader-icon">&nbsp;</div>
</div>
<div id = "reportsPanel" style="margin-left:20px;margin-top:20px;float:left;clear:both;">


</div>
{literal}
	<script type="text/javascript">
		function getReport(reportName){
			$(".mymyntra-ajax-loader").show();
			$.ajax({ 
				type : 'GET', 
				url : '/admin/returns/returns_reports.php?report='+reportName, 
				success: function(response){
					$(".mymyntra-ajax-loader").hide();
					$("#reportsPanel").html(response);
				},
				error: function(xmlHttpRequest, textStatus, errorThrown){
					$(".mymyntra-ajax-loader").hide();
					$("#reportsPanel").html("there seems to be some problem! pls contact tech team");
				}
			});
		}
	</script>
{/literal}
