{if $graph}
<div class="graph">{$graph}</div>
<table class="stats">
  <tr>
    <td width="50" class="none"></td>
    <td class="head">Visits</td>
    <td class="head">Transactions</td>
    <td class="head">Revenues</td>
    <td class="head">Avg. SERP</td>
  </tr>
  <tr>
    <td class="head">Min</td>
    <td>{$min_visits}</td>
    <td>{$min_transactions}</td>
    <td>{$min_revenues}</td>
    <td>{$min_serp}</td>
  </tr>
  <tr>
    <td class="head">Max</td>
    <td>{$max_visits}</td>
    <td>{$max_transactions}</td>
    <td>{$max_revenues}</td>
    <td>{$max_serp}</td>
  </tr>
  <tr class="total">
    <td class="head">Total</td>
    <td>{$total_visits}</td>
    <td>{$total_transactions}</td>
    <td>{$total_revenues}</td>
    <td>{$total_serp}</td>
  </tr>
</table>
{/if}
{if $keywords}
<br/>
<div class="keywords">
  <b>Keyword(s)</b> : {$keywords}
</div>
{/if}
{if $data}
{section name=i loop=$data}
{$data[i]}<br/>
{/section}
{/if}