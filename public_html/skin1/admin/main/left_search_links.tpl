{* $Id: featured_products.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
{include file="main/include_js.tpl" src="main/popup_product.js"}

{capture name=dialog}

<form name="add_left_link" action="left_search_links.php" method="post">
	<input type="hidden" name="mode" id="mode" value="">
	
	<div class="ITEM_PARENT_DIV" style="width:650px;">
		<div class="ITEM_FIRST_CHILD_DIV" style="width:auto;">
			<div class="ITEM_LABLE_DIV" style="text-align:left;">Link Name</div>
			<div class="ITEM_FIELD_DIV" style="width:auto;">
				<input type="text" name="link_name" id="link_name" maxlength="128">
			</div> 
		</div>
	</div>
	
	<div class="ITEM_PARENT_DIV" style="width:650px;">
		<div class="ITEM_FIRST_CHILD_DIV" style="width:auto;">
			<div class="ITEM_LABLE_DIV" style="text-align:left;">Link</div>
			<div class="ITEM_FIELD_DIV" style="width:auto;">
				<input type="text" name="link_actual" id="link_actual" style="width:550px">
			</div> 
		</div>
	</div>
	
	<div class="ITEM_PARENT_DIV" style="width:650px;">
		<div class="ITEM_FIRST_CHILD_DIV" style="width:auto;">
			<div class="ITEM_LABLE_DIV" style="text-align:left;">Link Description</div>
			<div class="ITEM_FIELD_DIV" style="width:auto;">
				<input type="text" name="link_description" id="link_description" style="width:550px">
			</div> 
		</div>
	</div>
	
	<div class="ITEM_PARENT_DIV" style="width:650px;">
		<div class="ITEM_FIRST_CHILD_DIV" style="width:auto;">
			<div class="ITEM_LABLE_DIV" style="text-align:left;">{$lng.lbl_active}</div>
			<div class="ITEM_FIELD_DIV" style="width:auto;">
			<select name="link_enabled">
				<option value="1" selected="selected">{$lng.lbl_yes}</option>
				<option value="0">{$lng.lbl_no}</option>
			</select>
			</div> 
		</div>
	</div>
	
	<div class="ITEM_PARENT_DIV" style="width:650px;">
		<div class="ITEM_LABLE_DIV"></div>
		<div class="ITEM_FIELD_DIV" style="width:auto;">
			<input type="submit" value="{$lng.lbl_add|escape}" onclick="javascript: document.add_left_link.mode.value = 'add_link'; document.add_left_link.submit();" style="width:300px;" />
		</div> 
	</div>
</form>


{/capture}
{include file="dialog.tpl" title="Add Links for Left Search Panel" content=$smarty.capture.dialog extra='width="100%"'}
<br /><br />


{capture name=dialog}
<form action="left_search_links.php" method="post" name="updateleftlinks">
<input type="hidden" name="mode" />
<input type="hidden" name="move_link_id" />
<table cellpadding="3" cellspacing="1" width="100%">
<tr class="TableHead">
	<td width="15%">Link name</td>
	<td width="30%">Link</td>
	<td width="25%">Link Description</td>
	<td width="10%">{$lng.lbl_active}</td>
	<td width="20%">Action</td>
</tr>

{if $leftlinks}

{section name=link_num loop=$leftlinks}
<tr{cycle values=", class='TableSubHead'"}>
		
	<td align="center" style="left-padding:30px"><b>{$leftlinks[link_num].link_name}</b></td>
	<td align="center" style="left-padding:30px"><b><a target="_blank" href="{$leftlinks[link_num].link}" title="{$leftlinks[link_num].link_description}">{$leftlinks[link_num].link}</a></b></td>
	<td align="center" style="left-padding:30px"><b>{$leftlinks[link_num].link_description}</b></td>
	<td align="center">
	<select name="link_status[{$leftlinks[link_num].link_id}][is_active]">
		<option value="1"{if $leftlinks[link_num].is_active eq "1"} selected="selected"{/if}>{$lng.lbl_yes}</option>
		<option value="0"{if $leftlinks[link_num].is_active eq "0"} selected="selected"{/if}>{$lng.lbl_no}</option>
	</select>
	</td>
	<td align="center">

	<input type="submit" value="Up" onclick="javascript: document.updateleftlinks.mode.value = 'move_link_up';  document.updateleftlinks.move_link_id.value = '{$leftlinks[link_num].link_id}'; document.updateleftlinks.submit();" />
	<input type="submit" value="Down" onclick="javascript: document.updateleftlinks.mode.value = 'move_link_down';  document.updateleftlinks.move_link_id.value = '{$leftlinks[link_num].link_id}'; document.updateleftlinks.submit();" />
	<input type="submit" value="Delete" onclick="javascript: document.updateleftlinks.mode.value = 'link_delete';  document.updateleftlinks.move_link_id.value = '{$leftlinks[link_num].link_id}'; document.updateleftlinks.submit();" />

	</td>	
</tr>
{/section}
{else}
<tr>
<td colspan="2" align="center">No Links added as yet</td>
</tr>
{/if}
<tr>
	<td></td>
	<td></td>
	<td></td>
	<td class="SubmitBox" align="right">
	   <input type="submit" value="{$lng.lbl_upd_selected|escape}" onclick="javascript: document.updateleftlinks.mode.value = 'update_link'; document.updateleftlinks.submit();" />
	</td>
	<td></td>
</tr>

</table>
</form>

{/capture}
{include file="dialog.tpl" title="Left Search Panel Links" content=$smarty.capture.dialog extra='width="100%"'}
