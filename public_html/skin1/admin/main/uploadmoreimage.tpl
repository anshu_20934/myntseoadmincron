{* $Id: featured_products.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
<br/></br/>
{capture name=dialog}
<html>
<head>
	<title>{$lng.lbl_select_product|strip_tags}</title>
	<link rel="stylesheet" href="{$SkinDir}/skin1_admin.css" />
</head>
<body>
<form action="uploadmoreimage.php" method="post" name="uploadform" enctype="multipart/form-data">
<input type="hidden" name="mode" value="upload" />
<input type="hidden" name="imgtype" value="{$smarty.get.imgType}" />
<input type="hidden" name="tid" value="{$typeid}" />
<input type="hidden" name="imageid" value="{$imageid}" />

<table cellpadding="3" cellspacing="1" width="100%">
    {if $message}
	<tr>
            <td colspan="2">{$message}</td>
        </tr>
    {/if}
<tr>
	<td colspan="2"></td>
</tr>
<tr>
	<td><b>Image name</b></td>
	<td><input type="text" name="imgname" id="imgname"/></td>
</tr>
<tr>
	<td><b>Upload New Image </b></td>
	<td><input type="file" name="img" /></td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2" align="right"><a href="javascript:window.close();" >Close Window</a></td>
</tr>
<tr>
	<td colspan="2" class="SubmitBox">
	<input type="submit" value="{$lng.lbl_update|escape}" onclick="javascript:document.uploadform.mode.value='upload';document.uploadform.submit()"  
	</td>
</tr>
</table>
</form>
</body>
</html>
{/capture}
{include file="dialog.tpl" title=$lng.lbl_change_image content=$smarty.capture.dialog extra='width="100%"'}
