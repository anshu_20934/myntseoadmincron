{* $Id: featured_products.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
{include file="main/include_js.tpl" src="main/popup_product.js"}

{if $mode=='managediscountrule'}
<div id="eossModifyDiscountRuleDiv" style="display: block;">
{else}
<div id="eossModifyDiscountRuleDiv" style="display: none;">
{/if}

{capture name=dialog}

<form name="eossDiscountRuleForm" class="eossDiscountRuleForm" action="eoss_discount_rules.php" method="post">
<input type="hidden" name="mode" id="mode" value="managediscountrule" />
<input type="hidden" name="movestyles" id="movestyles" value="0" />
<input type="hidden" name="modifydiscountruleid" value="{$singleDiscountRule.ruleid}" />

{if $newrulecreated}
<table width="100%" cellspacing="5" cellpadding="1" border="0">
<tr>
	<td valign="middle" align="center" style="background-color:yellow">
		<br/><font size=2 color=green>New Static Rule #{$singleDiscountRule.ruleid} : "<b>{$singleDiscountRule.display_name}</b>" has been successfully created. </font><br/><br/>
	</td>
</tr>
</table>
{elseif $rulesuccessfullymodified}
<table width="100%" cellspacing="5" cellpadding="1" border="0">
<tr>
	<td valign="middle" align="center" style="background-color:yellow">
		<br/><font size=2 color=green><b>This Rule has been Successfully Modified.</b></font><br/><br/>
	</td>
</tr>
</table>
{/if}

<div style="overflow:hidden">
<fieldset style="width:48%; float:left">
<legend>Discount Rule #{$singleDiscountRule.ruleid} Properties </legend><br/>
<table width="100%" cellspacing="5" cellpadding="1" border="0">
	<tr>
		<td valign="top">
			<div>
				{if $singleDiscountRule.ruletype == 1}<b>STATIC RULE</b>{else}<b>DYNAMIC RULE</b>{/if}<br/><br/>
				Rule Type : {$singleDiscountRule.algoid}<br/><br/>
				Rule Name * : <input type="text" name="discountrulename" id="discountrulename" value="{$singleDiscountRule.display_name}"><br/><br/>
				Brand : <b>{$singleDiscountRule.brand}</b><br/>
				Article Type : <b>{$singleDiscountRule.articletype}</b><br/>
				Fashion : <b>{$singleDiscountRule.fashion}</b><br/><br/>
				
				{if $singleDiscountRule.algoid eq 'FLAT'}
				<ul class="unlist">
					<li><input type="radio" id="flatdiscounttype_percentage" name="flatdiscounttype" value="percentage" {if $singleDiscountRuleType == '1'}checked{/if}>Percentage</li>
					<li><input type="radio" id="flatdiscounttype_absolute" name="flatdiscounttype" value="value" {if $singleDiscountRuleType == '0'}checked{/if}>Value</li>
					<br />
					<li>Discount Value * : <input type="text" name="discountvalue" id="discountvalue" value="{$singleDiscountRuleValue}"></li>
				</ul>
				{/if}
				
				<br/><br/><input type="button" value="Modify Discount Rule" onClick="validateflatrulecreation();">
			</div>
		</td>
		
		<td valign="top">
			<div>
				Start Date * <br/>
				<input id="discountrulestartdate" type="text" value="{$singleDiscountRule.start_date}" name="discountrulestartdate">
				
				<br/><br/>End Date *<br/>
				<input id="discountruleenddate" type="text" value="{$singleDiscountRule.end_date}" name="discountruleenddate">
				
				<br/><br/>Display Text<br/>
				<textarea name="discountruledisplaytext" style="width: 100%">{$singleDiscountRule.display_text}</textarea>
				
				<br/><br/>Terms and Conditions<br/>
				<textarea name="discountruletermsandconditions"  style="width: 100%">{$singleDiscountRule.terms}</textarea>
			</div>
		</td>
	</tr>
</table>
</fieldset>

{if $singleDiscountRule.ruletype == 1}
<fieldset style="width:47%; float:right; margin-bottom:30px" >
<legend>Add Styles to Static Rule #{$singleDiscountRule.ruleid} </legend><br/>
<table width="100%" cellspacing="5" cellpadding="1" border="0">
	<tr>
		<td width="5%"><input type="checkbox" class="selectAll"></td>
		<td width="10%">ALL</td>
		<td width="5%"></td>
		<td></td>
	<tr>
	<tr>
		<td colspan="4"><hr/></td>
	<tr>

	{foreach from=$stylesExceptions key=k item=v}
	<tr>
		<td><input type="checkbox" name="selectedStyle[]" class="selectedStyle" value="{$v.style_id}"></td>
		<td>{$v.style_id}</td>
		<td></td>
		<td>{$v.product_display_name}</td>
	</tr>
	{/foreach}
</table>
<br/><br/><br/>
<legend>You can also provide Comma Separated list of StyleIDs for Static rule</legend>
<table width="100%" cellspacing="5" cellpadding="1" border="0">
	<textarea name="commaSeparatedExceptionStyles" id="commaSeparatedExceptionStyles" rows="6" style="width:100%" ></textarea>
</table>
</fieldset>
{/if}

</div>
<br/>

<fieldset>
<legend>Applicable Styles for Rule#{$singleDiscountRule.ruleid}</legend><br />
<table width="100%" cellspacing="5" cellpadding="1" border="0">

	{if $singleDiscountRule.ruletype == 1 && $stylesRuleApplied|@count == 0}
		<tr><td colspan=5>No Styles belong to this Static Rule. You could safely delete this discount rule by clicking the button below.</td></tr>
		<tr><td colspan=5><br/><input type="button" value="Delete this Discount Rule" onClick="confirmDeleteDiscountRule();"></td></tr>
	{else}
		<tr>
			<td width="5%"><b>Style ID</b></td>
			<td width="15%"><b>Style Name</b></td>
			<td width="10%"><b>Article Number</b></td>
			<td width="10%"><b>Status</b></td>
			<td width="10%"><b>Mark Exception</b></td>
		</tr>
	{/if}
	
	{if $stylesRuleApplied || $stylesRuleWillBeApplied}
	<tr>
		<td colspan="4"></td>
		<td><input type="checkbox" class="selectAll2">Select All</td>
	</tr>
	<tr><td colspan="5">&nbsp;</td></tr>
	{/if}
	
	
	{if $stylesRuleApplied}
		{foreach from=$stylesRuleApplied key=k item=v}
		<tr>
			<td>{$v.style_id}</td>
			<td>{$v.product_display_name}</td>
			<td>{$v.article_number}</td>
			<td>Applied</td>
			<td><input type="checkbox" name="moveToException[]" value="{$v.style_id}" class="selectedStyle2"></td>
		</tr>
		{/foreach}
	{/if}
		
	{if $stylesRuleWillBeApplied}
		{if $stylesRuleApplied}<tr><td colspan="5"><hr/></td>{/if}
		{foreach from=$stylesRuleWillBeApplied key=k item=v}
		<tr>
			<td>{$v.style_id}</td>
			<td>{$v.product_display_name}</td>
			<td>{$v.article_number}</td>
			<td>Will be Applied</td>
			<td><input type="checkbox" name="moveToException[]" value="{$v.style_id}" class="selectedStyle2"></td>
		</tr>
		{/foreach}
	{/if}
	
	{if $stylesRuleApplied || $stylesRuleWillBeApplied}
	<tr>
		<td colspan="4"></td>
		<td><input type="submit"
				{if $singleDiscountRule.ruletype == 0}
					value="Move to Exception"
					onClick="return confirmMoveStyleToException();"
				{else if $singleDiscountRule.ruletype == 1}
					value="Remove from Static Rule"
					onClick="return confirmRemoveStyleFromStaticRule();"
				{/if} 
			>
		</td>
	</tr>
	{/if}
</table>
</fieldset>

{if $singleDiscountRule.ruletype == 0}
<br/><br/>
<fieldset>
<legend>Exception Set for Rule#{$singleDiscountRule.ruleid}</legend><br />
<table width="100%" cellspacing="5" cellpadding="1" border="0">
	<tr>
		<td width="5%"><b>Style ID</b></td>
		<td width="15%"><b>Style Name</b></td>
		<td width="10%"><b>Article Number</b></td>
		<td width="10%"><b>Status</b></td>
		<td width="10%"><b>Action</b></td>
	</tr>
	
	{if $stylesRemovedFromRule || $stylesWithSpecialRule}
	<tr>
		<td colspan="4"></td>
		<td><input type="checkbox" class="selectAll3">Select All</td>
	</tr>
	<tr><td colspan="5">&nbsp;</td></tr>
	{/if}
	
	{if $stylesRemovedFromRule}
		{foreach from=$stylesRemovedFromRule key=k item=v}
		<tr>
			<td>{$v.style_id}</td>
			<td>{$v.product_display_name}</td>
			<td>{$v.article_number}</td>
			<td>No Static Rule Applied</td>
			<td><input type="checkbox" name="moveToRule[]" value="{$v.style_id}" class="selectedStyle3"></td>
		</tr>
		{/foreach}
	{/if}
	
	{if $stylesWithSpecialRule}
		{if $stylesRemovedFromRule}<tr><td colspan="5"><hr/></td></tr>{/if}
		{foreach from=$stylesWithSpecialRule key=k item=v}
		<tr>
			<td>{$v.style_id}</td>
			<td>{$v.product_display_name}</td>
			<td>{$v.article_number}</td>
			<td>Applied to Static Rule #{$v.ruleid}<br/>
			Rule: <a href="?managediscountruleid={$v.ruleid}">{$v.display_name}</a>
			</td>
			<td><input type="checkbox" name="moveToRule[]" value="{$v.style_id}" class="selectedStyle3"></td>
		</tr>
		{/foreach}
	{/if}
	
	{if $stylesRemovedFromRule || $stylesWithSpecialRule}
	<tr>
		<td colspan="4"></td>
		<td><input type="submit" value="Move to Rule" onClick="return confirmMoveStyleToRule();"></td>
	</tr>
	{/if}
</table>
</fieldset>

{/if}
	
</form>
{/capture} {include file="dialog.tpl" title="Modify Discount Rule" content=$smarty.capture.dialog extra='width="100%"'}</div>

<script type="text/javascript" src="{$http_location}/skin1/myntra_js/jquery-ui-datepicker.js"></script>

<script type="text/javascript">
var ruletype = {$singleDiscountRule.ruletype};
</script>

{literal}
<script type="text/javascript">

function confirmMoveStyleToException() {
	var count = countCheckBoxes('moveToException[]');
	if(count==0) {
		alert('No styles marked for Exception.');
		return false;
	}

	var confirmmove = confirm('Are you sure you want to mark selected Styles as exception');
	if(confirmmove) {
		$('#movestyles').val("1");
		return true;
	} else {
		return false;
	}
}

function confirmMoveStyleToRule() {
	var count = countCheckBoxes('moveToRule[]');
	if(count==0) {
		alert('No styles selected to be moved back to Dynamic Rule');
		return false;
	}
	
	var confirmmove = confirm('Are you sure you want move selected Styles back to original dynamic rule ?');
	if(confirmmove) {
		$('#movestyles').val("2");
		return true;
	} else {
		return false;
	}
}

function confirmRemoveStyleFromStaticRule() {
	var count = countCheckBoxes('moveToException[]');
	if(count==0) {
		alert('No styles selected to be removed from this Special Rule.');
		return false;
	}
	
	var confirmmove = confirm('Are you sure you remove selected Styles from this Special Rule ?');
	if(confirmmove) {
		$('#movestyles').val("1");
		return true;
	} else {
		return false;
	}
}

$(document).ready(function(){
	$('#discountrulestartdate').datetimepicker();
	$('#discountruleenddate').datetimepicker();
});

function validateflatrulecreation() {
	if ($('#discountvalue').val()=="") {
		alert('Please enter appropriate discount value');
		$('#discountvalue').focus();
		return; 
	}
	var regex = new RegExp("[^0-9\.]","g");
	var discountvalue = $('#discountvalue').val();
	if(discountvalue != "" && regex.test(discountvalue)){
		alert('Please enter only numbers in discount value');
		$('#discountvalue').focus();
		return;
	}
	if(document.getElementById('flatdiscounttype_percentage').checked){
		if(discountvalue > 100){
			alert('Discount cannot be greater than 100');
			$('#discountvalue').focus();
			return;
		}
	}
	if ($('#discountrulename').val()=="") {
		alert('Please Enter a Rule Name');
		$('#discountrulename').focus();
		return; 
	} else if ($('#discountrulestartdate').val()=="") {
		alert('Please set the Start Date');
		$('#discountrulestartdate').focus();
		return; 
	} else if ($('#discountruleenddate').val()=="") {
		alert('Please set the End Date');
		$('#discountruleenddate').focus();
		return; 
	}

	if(ruletype == 1) {
		$.ajax({
	        type: "POST",
	        url: "eoss_exception_rule_check.php",
	        async: false,
	        data: "&styleids=" + $('#commaSeparatedExceptionStyles').val(),
	        success: function(data){
	        	var result = $.parseJSON(data);
	        	var status = result.status;
	            if(status==1) {
	            	var message=result.message;
	            	alert(message);
	            	return false;
	            } else if(status==2) {
	            	if(confirm(result.message)) {
	            		$('.eossDiscountRuleForm').trigger("submit");
	            	}
	            } else {
	            	if(confirm("Do you want to update this static rule ?")) {
	                	$('.eossDiscountRuleForm').trigger("submit");
	            	}
	            }
			}
		});
	} else {
		if(confirm("Do you want to update this dynamic rule ?")) {
			$('.eossDiscountRuleForm').trigger("submit");
		}
	}		
}

function confirmDeleteDiscountRule() {
	if(confirm("Are you sure that you want to delete this Static Rule ?")) {
		$('#mode').val("deletediscountrule");
    	$('.eossDiscountRuleForm').trigger("submit");
	}
}

</script>

{/literal}