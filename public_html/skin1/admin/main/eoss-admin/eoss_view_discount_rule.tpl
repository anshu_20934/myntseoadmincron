{* $Id: featured_products.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
{include file="main/include_js.tpl" src="main/popup_product.js"}

{if $mode=='viewdiscountrules' || $mode=='enablediscountrules' || $mode=='filterdiscountrules' || $mode=='multiplediscountrulechange'}
<div id="eossViewDisountRulesDiv" style="display:block;">
{else}
<div id="eossViewDisountRulesDiv" style="display:none;">
{/if}

{literal}
<style type="text/css">
.enabled{background-color:#FFFD93;border: 1px solid #DBCF44;}
</style>
{/literal}

{capture name=dialog}
<form name="eossDiscountRuleForm" class="eossDiscountRuleForm" action="eoss_discount_rules.php" method="post">
<input type="hidden" name="mode" id="mode" value="enablediscountrules" />
<input type="hidden" name="actionOnSelectedRule" id="actionOnSelectedRule" value="0" />
{if isset($searchSelectedBrand)}
<input type="hidden" name="filterapplied" value="1" />
{/if}

<table width="80%" cellspacing="5" cellpadding="1">
	<tbody>
		<tr>
			<td width="25%">Brand
				<select name="brand" class="selectbrands">
					<option value=""></option>
					{foreach from=$allBrands item=v}
					<option value="{$v}"{if $searchSelectedBrand eq $v}selected{/if}>{$v}</option>
					{/foreach}
				</select></td>
			<td width="25%">Article Type
				<select name="articletype" class="selectarticletypes">
					<option value=""></option>
					{foreach from=$allArticleTypes item=v}
					<option value="{$v}"{if $searchSelectedArticleType eq $v}selected{/if}>{$v}</option>
					{/foreach}
				</select>
			</td>
			<td width="25%">Fashion
				<select name="fashion" class="selectfashion">
					<option value=""></option>
					{foreach from=$allFashions item=v}
					<option value="{$v}"{if $searchSelectedFashion eq $v}selected{/if}>{$v}</option>
					{/foreach}
				</select>
			</td>
			<td width="20%"><input type="submit" value="Filter Matching Rules"
				onClick="document.eossDiscountRuleForm.mode.value='filterdiscountrules'" id="searchButton">
			</td>
		</tr>
	</tbody>
</table>

<table width="100%" cellspacing="1" cellpadding="2">
	<tbody>
		{if $allDynamicDiscountRules}
		<tr class="tablehead" style="background-color: lightBlue">
		{if isset($searchSelectedBrand)}
			<td align="center"><input type="checkbox" class="selectAll" checked></td>
			<td align="center"><b>ALL</b></td>
		{/if}
			<td colspan="{if isset($searchSelectedBrand)}9{else}10{/if}" align="center"><b>DYNAMIC RULES</b></td>
		</tr>
	
		<tr class="tablehead">
			{if isset($searchSelectedBrand)}
			<th width="5%" nowrap="nowrap">Select</th>
			{/if}
			<th width="5%" nowrap="nowrap">Rule ID</th>
			<th width="10%" nowrap="nowrap">Name</th>
			<th width="5%" nowrap="nowrap">Algorithm</th>
			<th width="15%" nowrap="nowrap">Filters</th>
			<th width="5%" nowrap="nowrap">Eligible Styles</th>
			<th width="5%" nowrap="nowrap">Active Styles</th>
			<th width="10%" nowrap="nowrap">Start Date</th>
			<th width="10%" nowrap="nowrap">End Date</th>
			<th width="5%" nowrap="nowrap">Enabled</th>
			<th width="5%" nowrap="nowrap">Action</th>
		</tr>
		
		{foreach from=$allDynamicDiscountRules item=singlerule}
		<tr {if $singlerule.enabled eq '1'}class="enabled"{/if}>
			{if isset($searchSelectedBrand)}
				<td align="center"><input type="checkbox" name="selectedRules[]" value="{$singlerule.ruleid}" class="selectedStyle" checked></td>
			{/if}
			<td align="center">{$singlerule.ruleid}</td>
			<td align="center">{$singlerule.display_name}</td>
			<td align="center">{$singlerule.algoid} - {$singlerule.discountdata}</td>
			<td>{$singlerule.brand} | {$singlerule.articletype} | {$singlerule.fashion}</td>
			<td align="center">{$singlerule.totalcount}</td>
			<td align="center">{$singlerule.count}</td>
			<td align="center">{$singlerule.start_date}</td>
			<td align="center">{$singlerule.end_date}</td>
			<td align="center">
				<select name="discountenabledrules[{$singlerule.ruleid}]">
				<option value="1" {if $singlerule.enabled=='1'} selected{/if}>Yes</option>
				<option value="0" {if $singlerule.enabled=='0'} selected{/if}>No</option>
				</select>
			</td>
			<td align="center"><a href="?managediscountruleid={$singlerule.ruleid}">Modify</a></td>
		</tr>
		{/foreach}
		
		<tr>
			<td colspan="3"><input type="submit" value="Disable Discount Rules" style="width:100%"  onClick="return confirmDisablingSelectedRules();"> </td>
			<td colspan="5"></td>
			{if isset($searchSelectedBrand)}<td></td>{/if}
			<td colspan="2"><input type="submit" value="Update" style="width:100%" onClick="return confirmEnablingSelectedRules();">
		</tr>
		
		{/if}
		
		
		{if $allSpecialDiscountRules}
		<tr><td>&nbsp;</td></tr>
		<tr><td>&nbsp;</td></tr>
			
		<tr class="tablehead" style="background-color: orange">
			<td colspan="10" align="center"><b>STATIC RULES</b></td>
		</tr>
		<tr class="tablehead">
			<th width="5%" nowrap="nowrap">Rule ID</th>
			<th width="10%" nowrap="nowrap">Name</th>
			<th width="5%" nowrap="nowrap">Algorithm</th>
			<th width="15%" nowrap="nowrap">Filters</th>
			<th width="5%" nowrap="nowrap"></th>
			<th width="5%" nowrap="nowrap">Active Styles</th>
			<th width="10%" nowrap="nowrap">Start Date</th>
			<th width="10%" nowrap="nowrap">End Date</th>
			<th width="5%" nowrap="nowrap">Enabled</th>
			<th width="5%" nowrap="nowrap">Action</th>
		</tr>
		
		{foreach from=$allSpecialDiscountRules item=singlerule}
		<tr {if $singlerule.enabled eq '1'}class="enabled"{/if}>
			{if isset($searchSelectedBrand)}
				<td align="center"><input type="checkbox" name="selectedRules[]" value="{$singlerule.ruleid}" class="selectedStyle" checked></td>
			{/if}
			<td align="center">{$singlerule.ruleid}</td>
			<td align="center">{$singlerule.display_name}</td>
			<td align="center">{$singlerule.algoid} - {$singlerule.discountdata}</td>
			<td align="center">--</td>
			<td align="center"></td>
			<td align="center">{$singlerule.count}</td>
			<td align="center">{$singlerule.start_date}</td>
			<td align="center">{$singlerule.end_date}</td>
			<td align="center">
				<select name="discountenabledrules[{$singlerule.ruleid}]">
				<option value="1" {if $singlerule.enabled=='1'} selected{/if}>Yes</option>
				<option value="0" {if $singlerule.enabled=='0'} selected{/if}>No</option>
				</select>
			</td>
			<td align="center"><a href="?managediscountruleid={$singlerule.ruleid}">Modify</a></td>
		</tr>
		{/foreach}
		
		
		<tr>
			<td colspan="3"><input type="submit" value="Disable Discount Rules" style="width:100%"  onClick="return confirmDisablingSelectedRules();"> </td>
			<td colspan="5"></td>
			<td colspan="2"><input type="submit" value="Update" style="width:100%" onClick="return confirmEnablingSelectedRules();">
		</tr>
		{/if}
		
		
		{if isset($searchSelectedBrand) && ($allDynamicDiscountRules|@count gt 0)}
		<tr><td><br/><br/><br/></td></tr>
		
		<tr>
			<td colspan="5" valign="top">
				<fieldset><legend>Change settings to change for all selected Rules</legend> <br />
				<ul class="unlist">
					<li><input type="radio" name="alldiscountruletype" value="FlatDiscount" checked>Flat Discount</li>
					<li><input type="radio" name="alldiscountruletype" value="AddedProductSet" disabled>Added Product Set</li>
					<li><input type="radio" name="alldiscountruletype" value="CeilingBasedPromotion" disabled>Ceiling Based Promotion</li>
					<li><input type="radio" name="alldiscountruletype" value="VariablePriceSlabs" disabled>Variable Price Slabs</li>
				</ul>
				</fieldset>
			</td>
			<td colspan="5" valign="top">
				<fieldset>
				<legend>Flat Discount Settings</legend> <br />
				<div id="flatdiscountdiv">
					<ul class="unlist">
						<li><input type="radio" name="allflatdiscounttype" value="percentage" checked>Percentage</li>
						<li><input type="radio" name="allflatdiscounttype" value="value">Value</li>
						<br />
						<li>Enter value of discount * : <input type="text" name="alldiscountvalue" id="alldiscountvalue"></li>
					</ul>
					
					<br/><br/>Start Date * <br/>
					<input id="alldiscountrulestartdate" type="text" value="" name="alldiscountrulestartdate">
					
					<br/><br/>End Date * <br/>
					<input id="alldiscountruleenddate" type="text" value="" name="alldiscountruleenddate">
					
					<br/><br/><br/>Display Text<br/>
					<textarea name="alldiscountruledisplaytext" style="width: 100%"></textarea>
			
					<br/><br/><br/>Terms and Conditions<br/>
					<textarea name="alldiscountruletermsandconditions" style="width: 100%"></textarea>
					
					<input type="button" value="Change All Rules" onClick="validatemultipleflatrulecreation();">
				</div>
				</fieldset>
			</td>
		</tr>
		
		{/if}
	</tbody>
</table> 
</form>
{/capture}
{include file="dialog.tpl" title="List of Discount Rules" content=$smarty.capture.dialog extra='width="100%"'}
</div>
</br>

<script type="text/javascript" src="{$http_location}/skin1/myntra_js/jquery-ui-datepicker.js"></script>

<script type="text/javascript">
	var ruleoptions = $.parseJSON('{$ruleoptionsjson}');
	var searchSelectedBrand = '{$searchSelectedBrand}';
	var searchSelectedArticleType = '{$searchSelectedArticleType}';
	var searchSelectedFashion = '{$searchSelectedFashion}';
	{if isset($searchSelectedBrand)}
		var viewingAllRules = 0;
	{else}
		var viewingAllRules = 1;
	{/if}
	var totalcount = {$totalcount};
</script>

{literal}

<script type="text/javascript">

function confirmDisablingSelectedRules() {
	var count = totalcount;
	if(viewingAllRules == 0) {
		count = countCheckBoxes('selectedRules[]');
	}
	
	if(count == 0) {
		alert('No Rule selected to disable');
		return false;
	}

	var rulemessage = "Are you sure you want to disable " + count + " selected discount rule";
	if(count>1)
		rulemessage+="s";
	
	rulemessage+=" ?";
	
	var confirmDisable = confirm(rulemessage);
	if(confirmDisable) {
		document.getElementById("actionOnSelectedRule").value="1";
		return true;
	}
	
	return false;
}

function confirmEnablingSelectedRules() {

	var count = totalcount;
	if(viewingAllRules == 0) {
		count = countCheckBoxes('selectedRules[]');
	}

	if(count == 0) {
		alert('No Rule selected to update');
		return false;
	}

	var rulemessage = "Are you sure you want to update " + count + " selected discount rule";
	if(count>1)
		rulemessage+="s";
	
	rulemessage+=" ?";

	var confirmDisable = confirm(rulemessage);
	if(confirmDisable) {
		document.getElementById("actionOnSelectedRule").value="2";
		return true;
	}
	
	return false;
}

$(document).ready(function(){

	$('#alldiscountrulestartdate').datetimepicker();
	$('#alldiscountruleenddate').datetimepicker();

	$('.selectarticletypes').attr("disabled","disabled");
	$('.selectfashion').attr("disabled","disabled");

	if(searchSelectedBrand!='') {
		$('.searchSelectedBrand').removeAttr("disabled");
		$('.selectarticletypes').removeAttr("disabled");
	}

	if(searchSelectedArticleType!='') {
		$('.selectarticletypes').removeAttr("disabled");
		$('.selectfashion').removeAttr("disabled");
	}

	$('.selectbrands').change(function(){
		var selectedbrand = $('.selectbrands').val();

		var articletypehtmloptions= "";
		
		for (var key in ruleoptions) {
			if(selectedbrand == key) {
				var articletypeobj = ruleoptions[key];
				articletypehtmloptions = "<option value=\"\"></option>";
				
				for (var key2 in articletypeobj){
					articletypehtmloptions+= "<option value=\"" + key2 + "\">" + key2 + "</option>"; 
				}
				break;
			}
		}

		$('.selectarticletypes').html(articletypehtmloptions);

		// We need to clear out selection of fashion values.
		$('.selectfashion').html('');

		$('.selectarticletypes').removeAttr("disabled");
		$('.selectfashion').attr("disabled","disabled");

	});


	$('.selectarticletypes').change(function(){
		var selectedbrand = $('.selectbrands').val();
		var selectedarticletype = $('.selectarticletypes').val();

		var fashionhtmloptions= "";
		
		for (var key in ruleoptions) {
			if(key == selectedbrand) {
				var articletypeobj = ruleoptions[key];

				for (var key2 in articletypeobj) {

					if(key2 == selectedarticletype) {

						fashionhtmloptions = "<option value=\"\"></option>";
						var fashionobj = articletypeobj[key2];

						for (var key3 in fashionobj) {
							fashionhtmloptions+= "<option value=\"" + key3 + "\">" + key3 + "</option>";
						}
						break; // Break loop for key2
					}
				}
				break; // Break loop for key
			}
		}

		$('.selectfashion').html(fashionhtmloptions);
		$('.selectfashion').removeAttr("disabled");
	});
});


function validatemultipleflatrulecreation() {

	if ($('#alldiscountvalue').val()=="") {
		alert('Please enter appropriate discount value');
		$('#alldiscountvalue').focus();
		return false; 
	} else if ($('#alldiscountrulestartdate').val()=="") {
		alert('Please set the Start Date');
		$('#alldiscountrulestartdate').focus();
		return false; 
	} else if ($('#alldiscountruleenddate').val()=="") {
		alert('Please set the End Date');
		$('#alldiscountruleenddate').focus();
		return false; 
	}

	var confirmEnabled = confirm('Are you sure you want to modify these selected Discount Rules ?');
	
	if(confirmEnabled) {
		$('#mode').val("multiplediscountrulechange"); 
		$('.eossDiscountRuleForm').trigger("submit");
	}
	
	return false;
}

</script>
{/literal}
