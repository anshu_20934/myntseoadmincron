{* $Id: featured_products.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
{include file="main/include_js.tpl" src="main/popup_product.js"}

{if $mode=='adddiscountrule' || $mode=='searchstyles'}
<div id="eossAddDiscountRuleDiv" style="display: block;">
{else}
<div id="eossAddDiscountRuleDiv" style="display: none;">
{/if}

{capture name=dialog}

<form name="eossDiscountRuleForm" class="eossDiscountRuleForm" action="eoss_discount_rules.php" method="post">
<input type="hidden" name="mode" id="mode" value="adddiscountrule" />

{if $newruleid}
<table width="80%" cellspacing="5" cellpadding="1">
	<fieldset>
	<legend><b>New Rule has been Created</b></legend><br />
	Rule ID : {$newruleid}<br/>
	{$newrulemessage}
	</fieldset>
</table>
<br/><br/><br/>
{/if}

<table width="80%" cellspacing="5" cellpadding="1">
	<tbody>
		<tr>
			<td width="25%">Brand
				<select name="brand" class="selectbrands">
					<option value=""></option>
					{foreach from=$allBrands item=v}
					<option value="{$v}"{if $searchSelectedBrand eq $v}selected{/if}>{$v}</option>
					{/foreach}
				</select></td>
			<td width="25%">Article Type
				<select name="articletype" class="selectarticletypes">
					<option value=""></option>
					{foreach from=$allArticleTypes item=v}
					<option value="{$v}"{if $searchSelectedArticleType eq $v}selected{/if}>{$v}</option>
					{/foreach}
				</select>
			</td>
			<td width="25%">Fashion
				<select name="fashion" class="selectfashion">
					<option value=""></option>
					{foreach from=$allFashions item=v}
					<option value="{$v}"{if $searchSelectedFashion eq $v}selected{/if}>{$v}</option>
					{/foreach}
				</select>
			</td>
			<td width="20%"><input type="submit" value="Display Matching Styles"
				onClick="document.eossDiscountRuleForm.mode.value='searchstyles'; return validatefilters(); " id="searchButton">
			</td>
		</tr>
	</tbody>
</table>
<br />
<br />
<table width="80%" cellspacing="5" cellpadding="1" border="0">
	<tr>
		<td valign="top" width="40%">
			<div>
			<fieldset><legend>Select Discount Rule Type</legend> <br />
			<ul class="unlist">
				<li><input type="radio" name="discountruletype" value="FlatDiscount" checked>Flat Discount</li>
				<li><input type="radio" name="discountruletype" value="AddedProductSet" disabled>Added Product Set</li>
				<li><input type="radio" name="discountruletype" value="CeilingBasedPromotion" disabled>Ceiling Based Promotion</li>
				<li><input type="radio" name="discountruletype" value="VariablePriceSlabs" disabled>Variable Price Slabs</li>
			</ul>
			<br />
			
			</fieldset>
			</div>
			<br/><br/><br/>
			<div id="flatdiscountdiv">
				<fieldset><legend>Flat Discount Rule</legend> <br />
				<ul class="unlist">
					<li>Suggest a Rule Name * : <input type="text" name="discountrulename" id="discountrulename"></li><br/>
					<li><input type="radio" name="flatdiscounttype" value="percentage" checked>Percentage</li>
					<li><input type="radio" name="flatdiscounttype" value="value">Value</li>
					<br />
					<li>Enter value of discount * : <input type="text" name="discountvalue" id="discountvalue"></li>
				</ul>
				
				<br/><br/>Start Date * <br/>
				<input id="discountrulestartdate" type="text" value="" name="discountrulestartdate">
				
				<br/><br/>End Date * <br/>
				<input id="discountruleenddate" type="text" value="" name="discountruleenddate">
				
				<br/><br/><br/>Display Text<br/>
				<textarea name="discountruledisplaytext" style="width: 100%"></textarea>
		
				<br/><br/><br/>Terms and Conditions<br/>
				<textarea name="discountruletermsandconditions" style="width: 100%"></textarea>

				<br/><br/><input type="button" value="Create Flat Discount Rule" onClick="validateflatrulecreation();">
				<br/><br/>
				</fieldset>
			</div>
		</td>
		
		<td valign="top" width="40%">
		{if $matchingStyles}
		<fieldset>
		<legend>Results - {$searchSelectedBrand} / {$searchSelectedArticleType} / {$searchSelectedFashion}</legend>
		<br />
		<ul class="unlist">
			<!-- li><input type="checkbox" class="selectAll">ALL<hr / -->
			<table>
				<tr>
					<td width="25%"><b>Style ID</b></td>
					<td width="5%"></td>
					<td><b>Product Name</b></td>
				<tr>
			{foreach from=$matchingStyles key=k item=v}
			<!-- li><input type="checkbox" name="selectedStyle[]" class="selectedStyle"
				value="{$v.style_id}">{$v.product_display_name}
				{if $v.discount_rule}<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(Rule #{$v.discount_rule} already applied){/if}
			</li -->
				<tr>
					<td>{$v.style_id}</td>
					<td></td>
					<td>{$v.product_display_name}</td>
				<tr>
			{/foreach}
			</table>
		</ul>
		</fieldset>
		{/if}
		</td>		
	</tr>
</table>

</form>
{/capture} {include file="dialog.tpl" title="Add New Dynamic Discount Rule" content=$smarty.capture.dialog extra='width="100%"'}</div>

<script type="text/javascript" src="{$http_location}/skin1/myntra_js/jquery-ui-datepicker.js"></script>

<script type="text/javascript">
	var ruleoptions = $.parseJSON('{$ruleoptionsjson}');
	var searchSelectedBrand = '{$searchSelectedBrand}';
	var searchSelectedArticleType = '{$searchSelectedArticleType}';
	var searchSelectedFashion = '{$searchSelectedFashion}';

</script>

{literal} 
<script type="text/javascript">
$(document).ready(function(){

	$('#discountrulestartdate').datetimepicker();
	$('#discountruleenddate').datetimepicker();
	
	$('.selectarticletypes').attr("disabled","disabled");
	$('.selectfashion').attr("disabled","disabled");

	if(searchSelectedBrand!='') {
		$('.searchSelectedBrand').removeAttr("disabled");
		$('.selectarticletypes').removeAttr("disabled");
	}

	if(searchSelectedArticleType!='') {
		$('.selectarticletypes').removeAttr("disabled");
		$('.selectfashion').removeAttr("disabled");
	}

	$('.selectbrands').change(function(){
		var selectedbrand = $('.selectbrands').val();

		var articletypehtmloptions= "";
		
		for (var key in ruleoptions) {
			if(selectedbrand == key) {
				var articletypeobj = ruleoptions[key];
				articletypehtmloptions = "<option value=\"\"></option>";
				
				for (var key2 in articletypeobj){
					articletypehtmloptions+= "<option value=\"" + key2 + "\">" + key2 + "</option>"; 
				}
				break;
			}
		}

		$('.selectarticletypes').html(articletypehtmloptions);

		// We need to clear out selection of fashion values.
		$('.selectfashion').html('');

		$('.selectarticletypes').removeAttr("disabled");
		$('.selectfashion').attr("disabled","disabled");

	});


	$('.selectarticletypes').change(function(){
		var selectedbrand = $('.selectbrands').val();
		var selectedarticletype = $('.selectarticletypes').val();

		var fashionhtmloptions= "";
		
		for (var key in ruleoptions) {
			if(key == selectedbrand) {
				var articletypeobj = ruleoptions[key];

				for (var key2 in articletypeobj) {

					if(key2 == selectedarticletype) {

						fashionhtmloptions = "<option value=\"\"></option>";
						var fashionobj = articletypeobj[key2];

						for (var key3 in fashionobj) {
							fashionhtmloptions+= "<option value=\"" + key3 + "\">" + key3 + "</option>";
						}
						break; // Break loop for key2
					}
				}
				break; // Break loop for key
			}
		}

		$('.selectfashion').html(fashionhtmloptions);
		$('.selectfashion').removeAttr("disabled");
	});

});

function validatefilters() {
	if($('.selectbrands').val()=="") {
		alert('Please select a Brand');
		return false;
	} else if($('.selectarticletypes').val()=="") {
		alert('Please select an Article Type');
		return false;
	} else if ($('.selectfashion').val()=="") {
		alert('Please select fashion');
		return false; 
	}

	return true; 
}

function validateflatrulecreation() {

	var filters = validatefilters();
	if(filters == false) {
		return false;
	}
	
	if ($('#discountrulename').val()=="") {
		alert('Please Enter a Rule Name');
		$('#discountrulename').focus();
		return false; 
	} else if ($('#discountvalue').val()=="") {
		alert('Please enter appropriate discount value');
		$('#discountvalue').focus();
		return false; 
	} else if ($('#discountrulestartdate').val()=="") {
		alert('Please set the Start Date');
		$('#discountrulestartdate').focus();
		return false; 
	} else if ($('#discountruleenddate').val()=="") {
		alert('Please set the End Date');
		$('#discountruleenddate').focus();
		return false; 
	}

	$.ajax({
        type: "POST",
        url: "eoss_exception_rule_check.php",
        async: false,
        data: "&mode=checknewrulefilters&brand=" + $('.selectbrands').val() + "&articletype=" + $('.selectarticletypes').val() + "&fashion=" + $('.selectfashion').val(),
        success: function(data){
        	var result = $.parseJSON(data);
        	var status = result.status;
            if(status==1) {
            	var message=result.message;
            	alert(message);
            	return false;
            } else {
                $('.eossDiscountRuleForm').trigger("submit");
            }
		}
	});

	return true;
}
</script>

{/literal}