{* $Id: featured_products.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
{include file="main/include_js.tpl" src="main/popup_product.js"}

{if $mode=='addexceptiondiscountrule' || $mode=='viewexceptiondiscountrule'}
<div id="eossAddExceptionDiscountRuleDiv" style="display: block;">
{else}
<div id="eossAddExceptionDiscountRuleDiv" style="display: none;">
{/if}

{capture name=dialog}

<form name="eossDiscountRuleForm" class="eossDiscountRuleForm" action="eoss_discount_rules.php" method="post">
<input type="hidden" name="mode" id="mode" value="addexceptiondiscountrule" />

{if $newruleid}
<table width="80%" cellspacing="5" cellpadding="1">
	<fieldset>
	<legend><b>New Rule has been Created</b></legend><br />
	Rule ID : {$newruleid}<br/>
	{$newrulemessage}
	</fieldset>
</table>
<br/><br/><br/>
{/if}

<table width="80%" cellspacing="5" cellpadding="1" border="0">
	<tr>
		<td valign="top" width="40%">
			<div>
			<fieldset><legend>Select Discount Rule Type</legend> <br />
			<ul class="unlist">
				<li><input type="radio" name="discountruletype" value="FlatDiscount" checked>Flat Discount</li>
				<li><input type="radio" name="discountruletype" value="AddedProductSet" disabled>Added Product Set</li>
				<li><input type="radio" name="discountruletype" value="CeilingBasedPromotion" disabled>Ceiling Based Promotion</li>
				<li><input type="radio" name="discountruletype" value="VariablePriceSlabs" disabled>Variable Price Slabs</li>
			</ul>
			<br />
			
			</fieldset>
			</div>
			<br/><br/><br/>
			<div id="flatdiscountdiv">
				<fieldset><legend>Flat Discount Rule</legend> <br />
				<ul class="unlist">
					<li>Suggest a Rule Name * : <input type="text" name="discountrulename" id="discountrulename"></li><br/>
					<li><input type="radio" name="flatdiscounttype" value="percentage" checked>Percentage</li>
					<li><input type="radio" name="flatdiscounttype" value="value">Value</li>
					<br />
					<li>Enter value of discount * : <input type="text" name="discountvalue" id="discountvalue"></li>
					
					<br/><br/>Start Date * <br/>
					<li><input id="discountrulestartdate" type="text" value="{$singleDiscountRule.start_date}" name="discountrulestartdate"></li>
				
					<br/><br/>End Date * <br/>
					<li><input id="discountruleenddate" type="text" value="{$singleDiscountRule.end_date}" name="discountruleenddate"></li>
					
					<br/>Display Text<br/>
					<li><textarea name="discountruledisplaytext" style="width: 100%"></textarea></li>
					
					<br/>Terms and Conditions<br/>
					<textarea name="discountruletermsandconditions"  style="width: 100%"></textarea>
				</ul>
				<br/><br/>
				
				<input type="button" value="Create Flat Discount Rule" onClick="validateflatrulecreation();">
				
				</fieldset>
			</div>
		</td>
		
		<td valign="top" width="40%">
		<div>
			<fieldset>
			<legend>List of Styles removed from Dynamic Rules</legend>
			<br />
			
			<table>
			
				<tr>
					<td width="5%"><input type="checkbox" class="selectAll"></td>
					<td width="10%">ALL</td>
					<td width="5%"></td>
					<td></td>
				<tr>
				</hr>
				<tr>
					<td width="5%"></td>
					<td width="20%"><b>Style ID</b></td>
					<td width="10%"></td>
					<td><b>Product Name</b></td>
				<tr>
				{foreach from=$stylesExceptions key=k item=v}
				<tr>
					<td><input type="checkbox" name="selectedStyle[]" class="selectedStyle" value="{$v.style_id}"></td>
					<td>{$v.style_id}</td>
					<td></td>
					<td>{$v.product_display_name}</td>
				</tr>
				{/foreach}
			</table>
			</fieldset>
		</div>
		<br/><br/>
		<div>
			<fieldset>
			<legend>Add Comma Separated list of StyleIDs for Static rule</legend>
			<textarea name="commaSeparatedExceptionStyles" rows="6" style="width:100%" id="commaSeparatedExceptionStyles"></textarea>
			<br />
			</fieldset>
			
		</div>
		</td>
	</tr>
</table>

</form>
{/capture}
</div>

{include file="dialog.tpl" title="Add Static Rule" content=$smarty.capture.dialog extra='width="100%"'}

<script type="text/javascript" src="{$http_location}/skin1/myntra_js/jquery-ui-datepicker.js"></script>

{literal} 
<script type="text/javascript">

$(document).ready(function(){
	$('#discountrulestartdate').datetimepicker();
	$('#discountruleenddate').datetimepicker();
});

function validateflatrulecreation() {

	if ($('#discountrulename').val()=="") {
		alert('Please Enter a Rule Name');
		$('#discountrulename').focus();
		return; 
	} else if ($('#discountvalue').val()=="") {
		alert('Please enter appropriate discount value');
		$('#discountvalue').focus();
		return; 
	} else if ($('#discountrulestartdate').val()=="") {
		alert('Please set the Start Date');
		$('#discountrulestartdate').focus();
		return; 
	} else if ($('#discountruleenddate').val()=="") {
		alert('Please set the End Date');
		$('#discountruleenddate').focus();
		return; 
	}
	
	$.ajax({
        type: "POST",
        url: "eoss_exception_rule_check.php",
        async: false,
        data: "&styleids=" + $('#commaSeparatedExceptionStyles').val(),
        success: function(data){
        	var result = $.parseJSON(data);
        	var status = result.status;
            if(status==1) {
            	var message=result.message;
            	alert(message);
            	return false;
            } else if(status==2) {
            	if(confirm(result.message)) {
            		$('.eossDiscountRuleForm').trigger("submit");
            	}
            } else {
            	if(confirm("Do you want to create this Static Rule ?")) {
                	$('.eossDiscountRuleForm').trigger("submit");
            	}
            }
		}
	});
}
</script>

{/literal}