{include file="main/include_js.tpl" src="main/popup_product.js}

<link rel="stylesheet" href="{$SkinDir}/skin1_admin.css"/>

{capture name=dialog}
	<table cellpadding="2" cellspacing="1" width="100%">
		<tr class="tablehead">
			<th width="20%" nowrap="nowrap">
				Location Id
			</th>
			<th width="20%" nowrap="nowrap">
				Location Name
			</th>
            <th width="20%" nowrap="nowrap">
				Operations Location
			</th>
			<th width="20%" nowrap="nowrap">
				Enabled
			</th>
			<th width="20%" nowrap="nowrap">
			</th>
		</tr>
		{if $locations>0}
			{section name=location loop=$locations}
				<form action="" method="get" name="modify_{$locations[location].id}">
					<tr class="{cycle values=",TableSubHead"}">
						<td align="center">
							{$locations[location].id}
							<input type="hidden" name="id" value="{$locations[location].id}"/>
						</td>
						<td align="center">
							<input type="text" value="{$locations[location].location_name}" name="location_name"/>
						</td>
                        <td align="center">
							<select name="operations_location">
								{html_options options=$operations_locations selected=$locations[location].operations_location}
							</select>
						</td>
						<td align="center">
							<input type="checkbox" name="status" {if $locations[location].status}checked{/if}/>
						</td>
						<td align="center">
							<input type="submit" name="modify_location" value="modify"/>
						</td>
					</tr>
				</form>
			{/section}
		{/if}
		<form action="" method="get" name="add_location">
			<tr class="tablehead">
				<td nowrap="nowrap">
					Add Location : 
				</td>
				<td nowrap="nowrap">
					<input type="text" name="location_name"/>
				</td>
				<td nowrap="nowrap">
					<select name="operations_location">
						{html_options options=$operations_locations}
					</select>
				</td>
				<td>
				</td>
				<td nowrap="nowrap">
					<input type="submit" name="add_location" value="add"/>
				</td>
			</tr>
		</form>
	</table>
{/capture}
{include file="dialog.tpl" title="Locations List" content=$smarty.capture.dialog extra='width="40%"'}
