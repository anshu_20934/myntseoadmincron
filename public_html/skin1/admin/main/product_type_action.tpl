{* $Id: featured_products.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
{include file="main/include_js.tpl" src="main/popup_product.js"}

<a name="featured" />

{$lng.txt_featured_products}

<br /><br />
<script type="text/javascript" src="{$http_location}/skin1/js_script/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
{literal}

<script type="text/javascript">

	tinyMCE.init({

		// General options

		mode : "textareas",

		editor_selector : "mceEditor",
		
		theme : "advanced",


		plugins : "safari,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,inlinepopups",


		// Theme options

		theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect",

		theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",

		theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",

		theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak",





		theme_advanced_toolbar_location : "top",

		theme_advanced_toolbar_align : "left",

		theme_advanced_statusbar_location : "bottom",

		theme_advanced_resizing : true,



		// Example content CSS (should be your site CSS)

		content_css : "css/content.css",



		// Drop lists for link/image/media/template dialogs

		template_external_list_url : "lists/template_list.js",

		external_link_list_url : "lists/link_list.js",

		external_image_list_url : "lists/image_list.js",

		media_external_list_url : "lists/media_list.js",



		// Replace values for the template plugin



	});


</script>
{/literal}


{capture name=dialog}


<form action="" method="post" name="producttypeaction" enctype="multipart/form-data" >
<input type="hidden" name="mode" value="update" />
<input type="hidden" name="typeid" value="{$typeid}" />

<table cellpadding="3" cellspacing="1" width="100%">

{if $products}

{section name=prod_num loop=$products}
  
   
     <tr>
         <td><b>{$lng.lbl_type_name}</b></td>
         <td><input type="text" name="type_name" size="50" value="{$products[prod_num].name}" />&nbsp;&nbsp;{if $errMsg }{$errMsg}{/if}</td>
    </tr>
    <tr>
         <td colspan="2">&nbsp;</td>
         
    </tr>

    <tr>
         <td><b>{$lng.lbl_type_label}</b></td>
         <td><input type="text" name="type_label" size="50" value="{$products[prod_num].label}" />&nbsp;&nbsp;{if $errMsg }{$errMsg}{/if}</td>
    </tr>
     <tr>
         <td colspan="2">&nbsp;</td>
         
    </tr>
<tr>
         <td><b>Product Code(2 chars eg TS for T-shirt)</b></td>
         <td><input type="text" name="type_code" size="10" value="{$products[prod_num].product_code}" />&nbsp;&nbsp;{if $errMsg }{$errMsg}{/if}</td>
    </tr>
    <tr>
         <td colspan="2">&nbsp;</td>
         
    </tr>
    <tr>
         <td><b>{$lng.lbl_type_thumbimg}</b></td>
	<td>
	  {if $products[prod_num].image_t neq ""} 
	          <img src="../{$products[prod_num].image_t}" height="50px" width="50px" ALT="No Image"/>
	   {else}
                      <img id="{$idtag}" src="{$xcart_web_dir}/image.php?type={$type}&amp;id={$id}&amp;ts={$smarty.now}{if $already_loaded}&amp;tmp=Y{/if}"{if $image_x ne 0} width="{$image_x}"{/if}{if $image_y ne 0} height="{$image_y}"{/if} alt="{include file="main/image_property.tpl"}"/>
	    {/if}
	    &nbsp;&nbsp;<a href="javascript:uploadWin('thumb','{$typeid}');"   valign="top" id="hrefload" style="visibility:visible;">Change</a>
	    <div name="reload" id="reload" style="visibility:hidden;display:none;">
		<input type="button"  onclick="javascript:window.location.reload(true);" value="upload new"> 
	    </div>

	</td>
    </tr>
     <tr>
         <td colspan="2">&nbsp;</td>
         
    </tr>

    <tr>
         <td><b>{$lng.lbl_type_detailimage}</b></td>
         <td>
	  {if $products[prod_num].image_l  neq "" }
	      <img src="../{$products[prod_num].image_l}" height="50px" width="50px" alt="No Image" />
           {else}
	        <img id="{$idtag}" src="{$xcart_web_dir}/image.php?type={$type}&amp;id={$id}&amp;ts={$smarty.now}{if $already_loaded}&amp;tmp=Y{/if}"{if $image_x ne 0} width="{$image_x}"{/if}{if $image_y ne 0} height="{$image_y}"{/if} alt="{include file="main/image_property.tpl"}"/>
	   {/if}
	 &nbsp;&nbsp;<a href="javascript:uploadWin('detail',{$typeid});"   valign="top" id="hrefload1" style="visibility:visible;">Change</a>
	 <div name="reload1" id="detailreload" style="visibility:hidden;display:none;">
		<input type="button"  onclick="javascript:window.location.reload(true);" value="upload new"> 
	    </div>
	    </td>
    </tr>
     <tr>
         <td colspan="2">&nbsp;</td>
         
    </tr>
        <tr>
         <td><b>Icon Image</b></td>
         <td>
	  {if $products[prod_num].image_i  neq "" }
	      <img src="../{$products[prod_num].image_i}" height="50px" width="50px" alt="No Image" />
       {else}
	        <img id="{$idtag}" src="{$xcart_web_dir}/image.php?type={$type}&amp;id={$id}&amp;ts={$smarty.now}{if $already_loaded}&amp;tmp=Y{/if}"{if $image_x ne 0} width="{$image_x}"{/if}{if $image_y ne 0} height="{$image_y}"{/if} alt="{include file="main/image_property.tpl"}"/>
	   {/if}
	   &nbsp;&nbsp;<a href="javascript:uploadWin('iconimg',{$typeid});"   valign="top" id="hrefload1" style="visibility:visible;">Change</a>
	 <div name="reload1" id="detailreload" style="visibility:hidden;display:none;">
		<input type="button"  onclick="javascript:window.location.reload(true);" value="upload new"> 
	    </div>
	    </td>
    </tr>
    <tr>
         <td colspan="2">&nbsp;</td>
    </tr>

	<tr>
         <td><b>{$lng.lbl_type_sizeimage}</b></td>
         <td>
	  {if $products[prod_num].image_s  neq "" }
	      <img src="../{$products[prod_num].image_s}" height="50px" width="50px" alt="No Image" />
           {else}
	        <img id="{$idtag}" src="{$xcart_web_dir}/image.php?type={$type}&amp;id={$id}&amp;ts={$smarty.now}{if $already_loaded}&amp;tmp=Y{/if}"{if $image_x ne 0} width="{$image_x}"{/if}{if $image_y ne 0} height="{$image_y}"{/if} alt="{include file="main/image_property.tpl"}"/>
	   {/if}
	 &nbsp;&nbsp;<a href="javascript:uploadWin('size',{$typeid});"   valign="top" id="hrefload2" style="visibility:visible;">Change</a>
	 <div name="reload1" id="sizereload" style="visibility:hidden;display:none;">
		<input type="button"  onclick="javascript:window.location.reload(true);" value="upload new"> 
	    </div>
	    </td>
    </tr>
     <tr>
         <td colspan="2">&nbsp;</td>     
    </tr>
	
	<tr>
         <td><b>Thumb Image small</b></td>
         <td>
	  {if $products[prod_num].thumb_image_small_path  neq "" }
	      <img src="../{$products[prod_num].thumb_image_small_path}" height="50px" width="50px" alt="No Image" />
           {else}
	        <img id="{$idtag}" src="{$xcart_web_dir}/image.php?type={$type}&amp;id={$id}&amp;ts={$smarty.now}{if $already_loaded}&amp;tmp=Y{/if}"{if $image_x ne 0} width="{$image_x}"{/if}{if $image_y ne 0} height="{$image_y}"{/if} alt="{include file="main/image_property.tpl"}"/>
	   {/if}
	 &nbsp;&nbsp;<a href="javascript:uploadWin('thumbsmall',{$typeid});"   valign="top" id="hrefload2" style="visibility:visible;">Change</a>
	 <div name="reload1" id="sizereload" style="visibility:hidden;display:none;">
		<input type="button"  onclick="javascript:window.location.reload(true);" value="upload new"> 
	    </div>
	    </td>
    </tr>
     <tr>
         <td colspan="2">&nbsp;</td>     
    </tr>

    <tr>
         <td><b>Thumb Image Big</b></td>
         <td>
	  {if $products[prod_num].thumb_image_big_path  neq "" }
	      <img src="../{$products[prod_num].thumb_image_big_path}" height="50px" width="50px" alt="No Image" />
           {else}
	        <img id="{$idtag}" src="{$xcart_web_dir}/image.php?type={$type}&amp;id={$id}&amp;ts={$smarty.now}{if $already_loaded}&amp;tmp=Y{/if}"{if $image_x ne 0} width="{$image_x}"{/if}{if $image_y ne 0} height="{$image_y}"{/if} alt="{include file="main/image_property.tpl"}"/>
	   {/if}
	 &nbsp;&nbsp;<a href="javascript:uploadWin('thumbbig',{$typeid});"   valign="top" id="hrefload2" style="visibility:visible;">Change</a>
	 <div name="reload1" id="sizereload" style="visibility:hidden;display:none;">
		<input type="button"  onclick="javascript:window.location.reload(true);" value="upload new"> 
	    </div>
	    </td>
    </tr>
     <tr>
         <td colspan="2">&nbsp;</td>     
    </tr>
    

    <tr>
         <td><b>{$lng.lbl_type_description}</b></td>
         <td><textarea name="desc" cols="50" rows="5" class="mceEditor" >{$products[prod_num].description}</textarea></td>
    </tr>

    <tr>
         <td><b>Product type</b></td>
         <td>
		<select name="ptypetype" id="ptypetype">
			<option value="P" {if $products[prod_num].type =='P'}selected='selected'{/if}>Public</option>
			<option value="A" {if $products[prod_num].type =='A'}selected='selected'{/if}>Affiliate</option>
			<option value="POS" {if $products[prod_num].type =='POS'}selected='selected'{/if}>POS</option>
		</select>
	 </td>
    </tr>

	<tr>
         <td><b>{$lng.lbl_type_group}</b></td>
         <td>
		<select name="ptypegroup" id="ptypegroup">
		    <option value="">Select</option>
			{foreach from=$ptgroups item=item}
				<option value="{$item[0]}" {if $products[prod_num].product_type_groupid ==$item[0]}selected='selected'{/if}>{$item[1]}</option>
			{/foreach}
		</select>
	 </td>
    </tr>
	
	<tr>
         <td><b>Product operation location</b></td>
         <td>
		<select name="operationlocation" id="operationlocation">
		      <option value="0">---select location---</option>
    		{section name=loc loop=$locations}
    		   <option value="{$locations[loc].id}" {if $products[prod_num].location == $locations[loc].id} selected {/if}>{$locations[loc].location_name}</option>
    	    {/section}
		</select>
	 </td>
    </tr>
	
	<tr>
         <td><b>Price Start From</b></td>
         <td><input type="text" name="start_from" size="50" value="{$products[prod_num].price_start}" /></td>
    </tr>

	<tr>
         <td><b>VAT Rate</b></td>
         <td><input type="text" name="vatrate" size="50" value="{$products[prod_num].vat}" /></td>
    </tr>

	<tr>
         <td><b>Processing Time</b></td>
         <td><input type="text" name="processingtime" size="50" value="{$products[prod_num].processing_time}" /></td>
    </tr>

	<tr>
         <td><b>Processing Quantity</b></td>
         <td><input type="text" name="processingquantity" size="50" value="{$products[prod_num].max_processing_quantity}" /></td>
    </tr>

     <tr>
         <td><b>Consignment Capacity</b></td>
         <td><input type="text" name="consignmentcapacity" size="50" value="{$consignmentcapacity}" /></td>
    </tr>
    <tr>
         <td><b>Display Order</b></td>
         <td><input type="text" name="display_order" size="50" value="{$products[prod_num].display_order}" /></td>
    </tr>


    <tr>
         <td><b>Bulk Availability</b></td>
         <td>
		<INPUT TYPE="radio" NAME="bulk" ID="bulkyes" value="Y" {if $products[prod_num].bulk =='Y'} checked {/if}>&nbsp;Yes
		<INPUT TYPE="radio" NAME="bulk" ID="bulkno" value="N" {if $products[prod_num].bulk =='N'} checked {/if}>&nbsp;No
	 </td>
    </tr>

   <tr>
         <td><b>Cash On Delivery</b></td>
         <td>
		<INPUT TYPE="radio" NAME="cod_enabled" ID="cod_enabled_yes" value="Y" {if $products[prod_num].cod_enabled =='Y'} checked {/if}>&nbsp;Yes
		<INPUT TYPE="radio" NAME="cod_enabled" ID="cod_enabled_no" value="N" {if $products[prod_num].cod_enabled != 'Y'} checked {/if}>&nbsp;No
	 </td>
    </tr>
    <tr>
         <td><b>Featured Product</b></td>
         <td>
		<INPUT TYPE="radio" NAME="is_featured" ID="is_featured_yes" value="Y" {if $products[prod_num].is_featured =='Y'} checked {/if} >&nbsp;Yes
		<INPUT TYPE="radio" NAME="is_featured" ID="is_featured_no"  value="N" {if $products[prod_num].is_featured !=='Y'} checked {/if}>&nbsp;No
	 </td>
    </tr>
    <tr>
         <td><b>Default Style Id</b></td>
         <td>
		<select name="default_style_id" id="default_style_id" >
		<option value="">Select</option>
		{section name=num loop=$productstyles}
		    <option value="{$productstyles[num].id}" {if $products[prod_num].default_style_id eq $productstyles[num].id} selected {/if}>{$productstyles[num].name}</option>
		{/section}
		</select>
	 </td>
    </tr>

   
{/section}

{else}
 
    <tr>
         <td><b>{$lng.lbl_type_name}<font color="red">*</font></b></td>
         <td><input type="text" name="type_name" size="50"  />&nbsp;&nbsp;{if $errMsg  }(<font color="red">{$errMsg}</font>){/if}</td>
    </tr>
    <tr>
         <td colspan="2">&nbsp;</td>
         
    </tr>

    <tr>
         <td><b>{$lng.lbl_type_label}<font color="red">*</font></b></td>
         <td><input type="text" name="type_label" size="50"  />&nbsp;&nbsp;{if $errMsg neq '' }(<font color="red">{$errMsg}</font>){/if}</td>
    </tr>
     <tr>
         <td colspan="2">&nbsp;</td>
         
    </tr>
    <tr>
         <td><b>Product Code (2 chars eg TS for T-shirt)<font color="red">*</font></b></td>
         <td><input type="text" name="type_code" size="10"  />&nbsp;&nbsp;{if $errMsg  }(<font color="red">{$errMsg}</font>){/if}</td>
    </tr>
    <tr>
         <td colspan="2">&nbsp;</td>
         
    </tr>

    <tr>
         <td><b>{$lng.lbl_type_thumbimg}</b></td>
         <td><input type="file" name="thumb_img" ></td>
    </tr>
     <tr>
         <td colspan="2">&nbsp;</td>
         
    </tr>

    <tr>
         <td><b>{$lng.lbl_type_detailimage}</b></td>
         <td><input type="file" name="detail_img" /></td>
    </tr>
     <tr>
         <td colspan="2">&nbsp;</td>
         
    </tr>
    
    <tr>
         <td><b>Icon Image</b></td>
         <td><input type="file" name="icon_img" /></td>
    </tr>
     <tr>
         <td colspan="2">&nbsp;</td>
         
    </tr>


     <tr>
         <td><b>size chart image</b></td>
         <td><input type="file" name="size_img" /></td>
    </tr>
     <tr>
         <td colspan="2">&nbsp;</td>   
    </tr>
      <tr>
         <td><b>Thumb small image</b></td>
         <td><input type="file" name="thumb_small" /></td>
    </tr>
     <tr>
         <td colspan="2">&nbsp;</td>   
    </tr>
      <tr>
         <td><b>Thumb big image</b></td>
         <td><input type="file" name="thumb_big" /></td>
    </tr>
     <tr>
         <td colspan="2">&nbsp;</td>   
    </tr>

    <tr>
         <td><b>{$lng.lbl_type_description}</b></td>
         <td><textarea name="desc" cols="50" rows="5"></textarea></td>
    </tr>
    <tr>
         <td><b>Product type</b></td>
         <td>
		<select name="ptypetype" id="ptypetype">
			<option value="P">Public</option>
			<option value="A">Affiliate</option>
			<option value="POS">POS</option>
		</select>
	 </td>
    </tr>
	
	<tr>
         <td><b>{$lng.lbl_type_group}</b></td>
         <td>
		<select name="ptypegroup" id="ptypegroup">
		{foreach from=$ptgroups item=item}
			<option value="{$item[0]}" >{$item[1]}</option>
		{/foreach}
		</select>
	 </td>
    </tr>
    
    <tr>
         <td><b>Product operation location</b></td>
         <td>
		<select name="operationlocation" id="operationlocation">
                <option value="0">---select location---</option>
		{section name=loc loop=$locations}
                <option value="{$locations[loc].id}" >{$locations[loc].location_name}</option>
	    {/section}
		</select>
	 </td>
    </tr>

	 <tr>
         <td><b>Price Start From</b></td>
         <td><input type="text" name="start_from" size="50" value="" /></td>
    </tr>

	<tr>
         <td><b>VAT Rate</b></td>
         <td><input type="text" name="vatrate" size="50" value="" /></td>
    </tr>

	<tr>
         <td><b>Processing Time</b></td>
         <td><input type="text" name="processingtime" size="50" value="" /></td>
    </tr>

	<tr>
         <td><b>Processing Quantity</b></td>
         <td><input type="text" name="processingquantity" size="50" value="" /></td>
    </tr>

     <tr>
         <td><b>Consignment Capacity</b><font color="red">*</font></td>
         <td><input type="text" name="consignmentcapacity" size="50" value="{$consignmentcapacity}" />&nbsp;&nbsp;{if $errMsg  }(<font color="red">{$errMsg}</font>){/if}</td>
    </tr>
    <tr>
         <td><b>Display Order</b></td>
         <td><input type="text" name="display_order" size="50"  /></td>
    </tr>
     <tr>
         <td><b>Bulk Availability</b></td>
         <td>
		<INPUT TYPE="radio" NAME="bulk" ID="bulkyes" value="Y">&nbsp;Yes
		<INPUT TYPE="radio" NAME="bulk" ID="bulkno" value="N" checked>&nbsp;No
	 </td>
    </tr>
    <tr>
         <td><b>Cash On Delivery</b></td>
         <td>
		<INPUT TYPE="radio" NAME="cod_enabled" ID="cod_enabled_yes" value="Y" checked>&nbsp;Yes
		<INPUT TYPE="radio" NAME="cod_enabled" ID="cod_enabled_no" value="N">&nbsp;No
	 </td>
    </tr>
      <tr>
         <td><b>Featured Product</b></td>
         <td>
		<INPUT TYPE="radio" NAME="is_featured" ID="is_featured_yes" value="Y"  >&nbsp;Yes
		<INPUT TYPE="radio" NAME="is_featured" ID="is_featured_no"  value="N" checked>&nbsp;No
	 </td>
    </tr>
    <tr>
         <td><b>Default Style Id</b></td>
         <td>
		<select name="default_style_id" id="default_style_id" >
		{section name=num loop=$productstyles}
		    <option value="{$productstyles[num].id}">{$productstyles[num].name}</option>
		{/section}
		</select>
	 </td>
    </tr>


{/if}

 <tr>
         <td colspan="2">&nbsp;</td>
         
 </tr>

<tr>
	<td colspan="2" class="SubmitBox">
	{if $products}
            <input type="submit" value="{$lng.lbl_update|escape}" onclick="javascript: submitForm(this, 'updatetype');" />
	{else}
             <input type="submit" value="{$lng.lbl_save|escape}" onclick="javascript:  submitForm(this, 'addtype');" />
	{/if}
	

	<!--<input type="reset" value="{$lng.lbl_cancel|escape}"   /> -->
	</td>
</tr>



 <tr>
<td colspan="2"><br /><br /></td>
</tr>



</table>
</form>

{/capture}
{include file="dialog.tpl" title=$lng.lbl_producttype_heading content=$smarty.capture.dialog extra='width="100%"'}
<br /><br />
