{* $Id: featured_products.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
{include file="main/include_js.tpl" src="main/popup_product.js"}

{capture name=dialog}

<form name="operation_location" action="operation_location.php" method="post">
	<input type="hidden" name="mode" id="mode">
		<div class="ITEM_PARENT_DIV" style="width:650px;">
		<div class="ITEM_FIRST_CHILD_DIV" style="width:auto;">
        <div class="ITEM_LABLE_DIV">Location</div>
        <div class="ITEM_FIELD_DIV" style="width:auto;">
            <input type="text" name="loc_name" id="loc_name" maxlength="128">
            <input type="submit" value="{$lng.lbl_add|escape}" onclick="javascript: document.operation_location.mode.value = 'add_loc'; document.operation_location.submit();" />
        </div> 
        </div>
        </div>
</form>


{/capture}
{include file="dialog.tpl" title="Add location name" content=$smarty.capture.dialog extra='width="100%"'}
<br /><br />


{capture name=dialog}
{if $producttype ne ""}
{*include file="main/check_all_row.tpl" style="line-height: 170%;" form="locationform" prefix="posted_data.+to_delete"*}
{/if}

<form action="operation_location.php" method="post" name="locationform">
<input type="hidden" name="mode" />


<table cellpadding="3" cellspacing="1" width="60%">

<tr class="TableHead">
	
	<td width="40%">Location name</td>
	<td width="20%">{$lng.lbl_active}</td>
	
</tr>

{if $locations}

{section name=prod_num loop=$locations}
<tr{cycle values=", class='TableSubHead'"}>
	
	<td align="left" style="left-padding:30px"><b>{$locations[prod_num].location_name}</b></td>
	<td align="center">
	<select name="loc_status[{$locations[prod_num].id}][avail]">
		<option value="1"{if $locations[prod_num].status eq "1"} selected="selected"{/if}>{$lng.lbl_yes}</option>
		<option value="0"{if $locations[prod_num].status eq "0"} selected="selected"{/if}>{$lng.lbl_no}</option>
	</select>
	</td>
</tr>
{/section}
{else}
<tr>
<td colspan="2" align="center">no locations added as yet</td>
</tr>
{/if}
<tr>
	<td colspan="5" class="SubmitBox" align="right">
	   <input type="submit" value="{$lng.lbl_upd_selected|escape}" onclick="javascript: document.locationform.mode.value = 'update_loc'; document.locationform.submit();" />
	</td>
</tr>

</table>
</form>

{/capture}
{include file="dialog.tpl" title="Location List" content=$smarty.capture.dialog extra='width="100%"'}
