{* $Id: featured_products.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
{include file="main/include_js.tpl" src="main/popup_product.js"}

<link rel="stylesheet" href="{$SkinDir}/skin1_admin.css" />
<script language="javascript" src="../skin1/js_script/item_assignment.js"></script>

{capture name=dialog}
<div style="float:left;width:auto;height:auto;border : 0px solid #000000;">
{include file="customer/main/navigation.tpl"}
</div>
<div style="float:right;width:auto;height:auto;border : 0px solid #000000;">
					{if $smarty.post.operations_location}
    	    	        {assign var=default_operations_location value=$smarty.post.operations_location}
        	    	{elseif $smarty.get.operations_location}
        	    	    {assign var=default_operations_location value=$smarty.get.operations_location}
			           {/if}
					<select name="operations_location" onchange="window.location='all_queued_orders.php?operations_location='+this.value">
						{html_options options=$operations_locations selected=$default_operations_location}
					</select>
<input type="button" name="download_report" value="    Download Report    "  onclick="javascript:window.location='all_queued_orders.php?action=ex'"/>
</div>
    <form action="" method="post" name="">
        <input type="hidden" name="mode"  />
        <input type="hidden" name="page" id="page" value="{$smarty.get.page}"  />
         <input type="hidden" name="query_string" id="query_string" value="{$query_string}"  />
        <div class="DIV_TABLE" style="width:900px;float:left;">

        <div class="DIV_TH"  style="width:900px;float:left;">
            <div class="DIV_TD" style="width:65px;">Order Id</div>
            <div class="DIV_TD" style="width:55px;">Item Id</div>
            <div class="DIV_TD" style="width:80px;">PRODUCT TYPE</div>
            <div class="DIV_TD" style="width:150px;">Product Style</div>
            <div class="DIV_TD" style="width:195px;">Quantity Breakup</div>
            <div class="DIV_TD" style="width:195px;">Queued Date</div>
            <div class="DIV_TD" style="width:80px;">Total Quantity</div>
        </div>

        {if $total_items > 0 }
        {assign var=oldorderid value="-1"}
            {section name=item loop=$itemResult max=$sizeofitemresult}
                 <div class="DIV_TR"  style="width:900px;float:left;">
                        {if $itemResult[item].orderid != $oldorderid}
                            <div class="DIV_TR_TD" style="width:65px;"><a href="javascript:void(0);" onclick="javascript:window.open('order.php?orderid={$itemResult[item].orderid}');"><p>{$itemResult[item].orderid}</p></a></div>
                        {else}
                            <div class="DIV_TR_TD" style="width:65px;"><a href="javascript:void(0);" style="text-decoration:none" onclick="javascript:window.open('order.php?orderid={$itemResult[item].orderid}');"><p><font style="text-transform: uppercase;">L</font></p></a></div>
                        {/if}
                        {assign var=oldorderid value=$itemResult[item].orderid}
                        <div class="DIV_TR_TD" style="width:55px;"><p>{$itemResult[item].itemid}</p></div>
                        <div class="DIV_TR_TD" style="width:80px;"><p>{$itemResult[item].p_type}</p></div>
                        <div class="DIV_TR_TD" style="width:150px;"><p>{$itemResult[item].p_style}</p></div>
                        <div class="DIV_TR_TD" style="width:195px;"><p>{$itemResult[item].quantity_breakup}</p></div>
                        <div class="DIV_TR_TD" style="width:195px;"><p>{$itemResult[item].queueddate}</p></div>
                        <div class="DIV_TR_TD" style="width:80px;"><p>{$itemResult[item].total_quantity}</p></div>
                 </div>
            {/section}
        {else}
            <div class="DIV_TR">
                <div class="DIV_TR_TD" style="width:900px;"><strong>no matching request found</strong></div>
            </div>
        {/if}
        </div>
    </form>

 <div style="float:left;width:auto;height:auto;border : 0px solid #000000;">
{include file="customer/main/navigation.tpl"}
</div>
{/capture}
{include file="dialog.tpl" title="Location task list" content=$smarty.capture.dialog extra='width="100%"'}

