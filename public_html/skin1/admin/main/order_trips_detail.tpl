{capture name=dialog}
	{if $order_trip_info.status eq "failure"}
	<font color="red">{$order_trip_info.msg}</font>
	{else}
	<div style="padding: 20px;" align="center">
		<table align="center" cellspacing=0 cellpadding=0 border=0 width=100% style="border-collapse: collapse;">
			<tr bgcolor="#A9A9A9" align="center">
				<th>DC Name</th>
				<th>DC Manager</th>
				<th>DC Manager Contact #</th>
				<th>Delivery Assignee</th> 
				<th>Assignee Phone #</th> 
				<th>Trip Number</th> 
				<th>Trip Start Date</th> 
				<th>Trip Order Status</th>
			</tr>
			<tr align="center">
			<td>{$order_trip_info.dc.name}</td>
			<td>{$order_trip_info.dc.manager}</td>
			<td>{$order_trip_info.dc.contactNumber}</td>
			<td>{$order_trip_info.ds.firstName} {$order_trip_info.ds.lastName}</td>
			<td>{$order_trip_info.ds.mobile}</td>
			<td>{$order_trip_info.trip.tripNumber}</td>
			<td>{$order_trip_info.trip.tripStartTime|date_format:"%B %e, %Y %H:%M:%S"}</td>
			<td>{$order_trip_info.trip.tripStatus}</td>
			</tr>
		</table>
	</div>
	{/if}
{/capture}
{include file="dialog.tpl" title='Trips Detail' content=$smarty.capture.dialog extra='width="99%"'}
