{if $smarty.get.action eq 'edit transaction'}
{literal}
<script LANGUAGE="JavaScript">
function confirmtran(){
var agree=confirm("ARE YOU SURE");
if (agree)
return true ;
else
return false ;
}
</script>
{/literal}
	<form action='' method=get>
	<input type=hidden name=action value='process transaction'/>
	<input type=hidden name=transactionid value='{$smarty.get.transactionid}'/>
	<input type=hidden name=paymenttype value='{$transactiondetails.type}'/>
	<input type=hidden name=amount value='{$transactiondetails.amount}'/>
	<input type=hidden name=account_id value='{$transactiondetails.account_id}'/>
	{if $transactiondetails.type eq 'cash'}
	<fieldset id=cashpayment name="cash deposit">
	<p style="font-size:12px;">

	<strong>Store Payments : Request Detail</strong><BR/><BR/>
	Following Payments Have Been Recieved<Br/><BR/>
	<table>
	<tr>
	<td>Payment Type : </td>
	<td><input type=text value=Cash disabled></td>
	<td>Shop Master : </td>
	<td><a href="shop_master_management.php?action=edit+shop+master&shop_master_id={$transactiondetails.shop_master_id}">{$transactiondetails.name}</a></td>
	</tr>
	<tr>
	<td>Bank Name : </td>
	<td><input type=text value="{$transactiondetails.Bank_Name}" disabled></td>
	<td>Store : </td>
	<td><a href="shop_master_management.php?action=edit+store&emailid={$transactiondetails.source_manager_email}">{$transactiondetails.source_name}</a></td>
	</tr>
	<tr>
	<td>Branch Name : </td>
	<td><input type=text value="{$transactiondetails.Bank_Branch}" disabled></td>
	<td></td>
	<td></td>
	</tr>
	<tr>
	<td>Myntra Account # :  </td>
	<td><input type=text value="{$transactiondetails.account_id}" disabled></td>
	<td></td>
	<td></td>
	</tr>
	<tr>
	<td>Amount # : Rs. </td>
	<td><input type=text value="{$transactiondetails.amount|number_format:2}" disabled></td>
	<td></td>
	<td></td>
	</tr>
	<tr>
	<tr>
	<td>Address :  </td>
	<td><textarea rows=5 style="width:240px;" disabled>{$transactiondetails.Address} </textarea></td>
	<td></td>
	<td></td>
	</tr>
	<tr>
	<td>Memo :  </td>
	<td><textarea rows=5 style="width:240px;" disabled>{$transactiondetails.memo}" </textarea></td>
	<td></td>
	<td></td>
	</tr>
	{if $transactiondetails.status eq 0}
	<tr>
	<td><input type=submit name=submit value=confirm onClick="return confirmtran()">  </td>
	<td><input type=submit name=submit value=reject onClick="return confirmtran()"></td>
	<td></td>
	<td></td>
	</tr>
	{/if}
	</table>
	</p>
	</fieldset>
	{/if}
	{if $transactiondetails.type eq 'cheque'}
	<fieldset id=cashpayment name="cash deposit">
	<p style="font-size:12px;">

	<strong>Store Payments : Request Detail</strong><BR/><BR/>
	Following Payments Have Been Recieved<Br/><BR/>
	<table>
	<tr>
	<td>Payment Type : </td>
	<td><input type=text value=Cheque disabled></td>
	<td>Shop Master : </td>
	<td><a href="shop_master_management.php?action=edit+shop+master&shop_master_id={$transactiondetails.shop_master_id}">{$transactiondetails.name}</a></td>
	</tr>
	<tr>
	<td>Bank Name : </td>
	<td><input type=text value="{$transactiondetails.Bank_Name}" disabled></td>
	<td>Store : </td>
	<td><a href="shop_master_management.php?action=edit+store&emailid={$transactiondetails.source_manager_email}">{$transactiondetails.source_name}</a></td>
	</tr>
	<tr>
	<td>Branch Name : </td>
	<td><input type=text value="{$transactiondetails.Bank_Branch}" disabled></td>
	<td></td>
	<td></td>
	</tr>
	<tr>
	<td>Myntra Account # :  </td>
	<td><input type=text value="{$transactiondetails.account_id}" disabled></td>
	<td></td>
	<td></td>
	</tr>
	<tr>
	<td>Amount # : Rs. </td>
	<td><input type=text value="{$transactiondetails.amount|number_format:2}" disabled></td>
	<td></td>
	<td></td>
	</tr>
	<tr>
	<td>Cheque/Draft # </td>
	<td><input type=text value="{$transactiondetails.Cheque_Number}" disabled></td>
	<td></td>
	<td></td>
	</tr>
	<tr>
	<td>Address :  </td>
	<td><textarea rows=5 style="width:240px;" disabled>{$transactiondetails.Address}" </textarea></td>
	<td></td>
	<td></td>
	</tr>
	<tr>
	<td>Memo :  </td>
	<td><textarea rows=5 style="width:240px;" disabled>{$transactiondetails.description}" </textarea></td>
	<td></td>
	<td></td>
	</tr>
	{if $transactiondetails.status eq 0}
	<tr>
	<td><input type=submit name=submit value=confirm>  </td>
	<td><input type=submit name=submit value=reject></td>
	<td></td>
	<td></td>
	</tr>
	{/if}
	</table>
	</p>
	</fieldset>

	{/if}
	</form>
{else}
<form action="" method=get>
<p align=right>	Filter : <select name=filter  onChange="this.form.submit();" ><option value=3 >Pending</option><option value=1 {if $smarty.get.filter eq 1}selected{/if}>Approved</option><option value=2 {if $smarty.get.filter eq 2}selected{/if}>Declined</option><option value=all  {if $smarty.get.filter eq 'all'}selected{/if}>All</option></select></p> 
</form>
<table style="" width="100%">
	<tr style="background-color:#000; cellpadding:5px;font-weight:bold;;color:#fff;border: solid #333;">
		<td style="border: solid #333 1px;color:#fff;font-weight:bold;padding:5px;font-size:14px;" width=14%>
		Transaction Id 
		</td>
		<td style="border: solid #333 1px;color:#fff;font-weight:bold;padding:5px;font-size:14px;">
		Shop Master	
		</td>
		<td style="border: solid #333 1px;color:#fff;font-weight:bold;padding:5px;font-size:14px;">
		Store	
		</td>
		<td style="border: solid #333 1px;color:#fff;font-weight:bold;padding:5px;font-size:14px;">
		Payment Type	
		</td>
		<td style="border: solid #333 1px;color:#fff;font-weight:bold;padding:5px;font-size:14px;">
		Amount	
		</td>
		<td style="border: solid #333 1px;color:#fff;font-weight:bold;padding:5px;font-size:14px;">
		Action	
		</td>
	</tr>
	{section name=idx loop=$transactions}
	<tr class="{cycle values=",TableSubHead"}">
		<form method=get action=''>
		<input type=hidden name=action value=changestatus />
		<td >
		<a href="?action=edit transaction&transactionid={$transactions[idx].id}">{$transactions[idx].id}</a>
		</td>
		<td >
		{$transactions[idx].name}
		</td>
		<td >
		{$transactions[idx].source_name}
		</td>
		<td >
		{$transactions[idx].type}
		</td>
		<td>
		{$transactions[idx].amount|number_format:2}
		</td>
		<td >
			{if $transactions[idx].status eq 0}Pending{/if}
		 	{if $transactions[idx].status eq 1}approved{/if}
			{if $transactions[idx].status eq 2}declined{/if}
		</td>
		</form>
	</tr>
	{/section}
</table>
{/if}
