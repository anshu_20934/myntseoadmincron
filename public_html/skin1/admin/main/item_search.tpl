{* $Id: featured_products.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
{include file="main/include_js.tpl" src="main/popup_product.js"}
{capture name=dialog}
<form name="itemsearch" action="item_assignment.php" method="get">
	<input type="hidden" name="mode" id="mode" >
		<div class="ITEM_PARENT_DIV">
		<div class="ITEM_FIRST_CHILD_DIV">
        <div class="ITEM_LABLE_DIV">Location</div>
        <div class="ITEM_FIELD_DIV">
                {if $smarty.post.item_location}
                    {assign var=loc_arr value=$smarty.post.item_location}
                {elseif $smarty.get.item_location}
                    {assign var=loc_arr value=$smarty.get.item_location}
                {/if}
            <select name="item_location[]" multiple="multiple" size="4" style="width:140px;">
				{section name=loc loop=$locations}
				    {section name=post_get_loc loop=$loc_arr}
				        {if $loc_arr[post_get_loc] == $locations[loc].id}
				            {assign var=loc_selected value="Y" }
                        {/if}
				    {/section}
				   <option value="{$locations[loc].id}" {if $loc_selected == 'Y'} selected {/if}>{$locations[loc].location_name}</option>
				   {assign var=loc_selected value=""}
				{/section}
		    </select>  
        </div>  
        </div>
        
        <div class="ITEM_FIRST_CHILD_DIV">
        <div class="ITEM_LABLE_DIV">Item Status</div>
        <div class="ITEM_FIELD_DIV">
            {if $smarty.post.item_status}
                {assign var=st_arr value=$smarty.post.item_status}
            {elseif $smarty.get.item_status}
                {assign var=st_arr value=$smarty.get.item_status}
            {/if}
            <select name="item_status[]" multiple="multiple" size="4" style="width:140px;">
                {foreach from=$status item=st_value key=st_key}
                    {foreach from=$st_arr item=get_value}
                        {if $get_value == $st_key}
				            {assign var=status_selected value="Y" }
                        {/if}
                    {/foreach}
                    <option value="{$st_key}" {if $status_selected == 'Y'} selected {/if}>{$st_value.status}</option>
                    {assign var=status_selected value="" }
                {/foreach}
			</select>
        </div>  
        </div>
        
        <div class="ITEM_FIRST_CHILD_DIV">
        <div class="ITEM_LABLE_DIV">Assignee</div>
        <div class="ITEM_FIELD_DIV" >
            {if $smarty.post.item_assignee}
                {assign var=ass_arr value=$smarty.post.item_assignee}
            {elseif $smarty.get.item_assignee}
                {assign var=ass_arr value=$smarty.get.item_assignee}
            {/if}
            <select name="item_assignee[]" multiple="multiple" size="4" style="width:140px;">
				{section name=ass loop=$assignee}
				    {section name=post_get_ass loop=$ass_arr}
				        {if $ass_arr[post_get_ass] == $assignee[ass].id}
				            {assign var=ass_selected value="Y" }
                        {/if}
				    {/section}
				   <option value="{$assignee[ass].id}" {if $ass_selected == 'Y'} selected {/if}>{$assignee[ass].name}</option>
				   {assign var=ass_selected value=""}
				{/section}
			</select>
        </div>
        </div>
        </div>


{* Row 2 of Inputs *}
		<div class="ITEM_PARENT_DIV">
			<div class="ITEM_FIRST_CHILD_DIV">
				<div class="ITEM_LABLE_DIV">Order Status</div>
				<div class="ITEM_FIELD_DIV" >
					{if $smarty.post.order_status}
    	    	        {assign var=ordstat value=$smarty.post.order_status}
        	    	{elseif $smarty.get.order_status}
        	    	    {assign var=ordstat value=$smarty.get.order_status}
			        {/if}
                    <select name="order_status[]" multiple="multiple" size="4" style="width:140px;">
                        {foreach key=code item=value from=$order_status}
                            {section name=post_get_ostatus loop=$ordstat}
                                {if $ordstat[post_get_ostatus] == $code}
                                    {assign var=ostatus_selected value="Y" }
                                {/if}
                            {/section}
                           <option value="{$code}" {if $ostatus_selected == 'Y'} selected {/if}>{$value}</option>
                           {assign var=ostatus_selected value=""}
                        {/foreach}
                   </select>
				</div>
			</div>
			<div class="ITEM_FIRST_CHILD_DIV">
				<div class="ITEM_LABLE_DIV">Customer Billing Name</div>
				<div class="ITEM_FIELD_DIV" >
                    <input type="text" name="customer_name" id="customer_name" {if $smarty.post.customer_name !=""} value = {$smarty.post.customer_name} {elseif  $smarty.get.customer_name !=""} value = {$smarty.get.customer_name} {/if}>
				</div>
			</div>
			<div class="ITEM_FIRST_CHILD_DIV">
				<div class="ITEM_LABLE_DIV">Product Type</div>
				<div class="ITEM_FIELD_DIV" >			
					{if $smarty.post.product_type}
    	    	        {assign var=default_product_type value=$smarty.post.product_type}
        	    	{elseif $smarty.get.product_type}
        	    	    {assign var=default_product_type value=$smarty.get.product_type}
			        {/if}
					<select name="product_type" >
						<option value="">--Select product type--</option>
						{html_options options=$product_types selected=$default_product_type}
					</select>
				</div>
			</div>

           <div class="ITEM_FIRST_CHILD_DIV">
				<div class="ITEM_LABLE_DIV">Product Style</div>
				<div class="ITEM_FIELD_DIV" >
					{if $smarty.post.product_style}
    	    	        {assign var=default_product_style value=$smarty.post.product_style}
        	    	{elseif $smarty.get.product_style}
        	    	    {assign var=default_product_style value=$smarty.get.product_style}
			        {/if}
					<select name="product_style" >
						<option value="">--Select product style--</option>
						{html_options options=$product_styles selected=$default_product_style}
					</select>
				</div>
			</div>
		</div>

		<div class="ITEM_PARENT_DIV">
		<div class="ITEM_FIRST_CHILD_DIV">
        <div class="ITEM_LABLE_DIV">Item Id</div>
        <div class="ITEM_FIELD_DIV">
              <input type="text" name="item_id" id="item_id" {if $smarty.post.item_id !=""} value = {$smarty.post.item_id} {elseif  $smarty.get.item_id !=""} value = {$smarty.get.item_id} {/if}>
        </div>  
        </div>
        
        <div class="ITEM_FIRST_CHILD_DIV">
        <div class="ITEM_LABLE_DIV">Order Id</div>
        <div class="ITEM_FIELD_DIV">
            <input type="text" name="order_id" id="order_id" {if $smarty.post.order_id !=""} value = {$smarty.post.order_id} {elseif  $smarty.get.order_id !=""} value = {$smarty.get.order_id} {/if}>
        </div>  
        </div>
          
        <div class="ITEM_FIRST_CHILD_DIV">
        <div class="ITEM_LABLE_DIV">Ops location</div>
        <div class="ITEM_FIELD_DIV">
					{if $smarty.post.operations_location}
    	    	        {assign var=default_operations_location value=$smarty.post.operations_location}
        	    	{elseif $smarty.get.operations_location}
        	    	    {assign var=default_operations_location value=$smarty.get.operations_location}
			           {/if}
					<select name="operations_location" onchange="window.location='item_assignment.php?mode=search&operations_location='+this.value">
						{html_options options=$operations_locations selected=$default_operations_location}
					</select>
        </div>  
        </div>
        </div>
        
        <div class="ITEM_PARENT_DIV">
		<div class="ITEM_FIRST_CHILD_DIV">
        <div class="ITEM_LABLE_DIV">Queued From date</div>
        <div class="ITEM_FIELD_DIV">
              <table width="100%">
                    <tr>
                        <td width="60%"><input type="text" id="from_date" name="from_date" size="13" MaxLength="12" {if $smarty.post.from_date !=""} value = {$smarty.post.from_date} {elseif  $smarty.get.from_date !=""} value = {$smarty.get.from_date} {/if} ></td>
                        <td width="40%"><img src="{$cdn_base}/images/cal.gif"  border="0" alt="Click Here to Pick up the date" onclick="displayDatePicker('from_date');"></td>
                    </tr>
                </table>
        </div>
        </div>

        <div class="ITEM_FIRST_CHILD_DIV">
        <div class="ITEM_LABLE_DIV">Queued To date</div>
        <div class="ITEM_FIELD_DIV">
            <table width="100%">
                <tr>
                    <td width="60%"><input type="text" id="to_date" name="to_date" size="13" MaxLength="12" {if $smarty.post.to_date !=""} value = {$smarty.post.to_date} {elseif  $smarty.get.to_date !=""} value = {$smarty.get.to_date} {/if} ></td>
                    <td width="40%"><img src="{$cdn_base}/images/cal.gif"  border="0" alt="Click Here to Pick up the date" onclick="displayDatePicker('to_date');"></td>
                </tr>
            </table>
        </div>
        </div>
        </div>

		<div class="ITEM_PARENT_DIV">
		<div class="ITEM_FIRST_CHILD_DIV">
        <div class="ITEM_LABLE_DIV">Payment Method</div>
        <div class="ITEM_FIELD_DIV">
		<select name="payment_method" style="width:140px;">
                    {if $payment_method == "" } 
						<option value="none" selected></option>
					{else}
						<option value="none"></option>
					{/if}
					
					{if $payment_method == "noncod" } 
						<option value="noncod" selected>Other than COD</option>
					{else}
						<option value="noncod">Other than COD</option>
					{/if}

					{if $payment_method == "cod" }
						<option value="cod" selected >COD</option>
					{else}
						<option value="cod" >COD</option>
					{/if}
		</select>
		</div></div>
		<div class="ITEM_FIRST_CHILD_DIV">
        <div class="ITEM_LABLE_DIV">Non Personalized Products</div>
        <div class="ITEM_FIELD_DIV">
		<select name="is_customizable" style="width:140px;">
                    	<option value="" {if $smarty.get.is_customizable eq ""} selected {/if}>None</option>
						<option value="0" {if $smarty.get.is_customizable eq '0'} selected {/if}>Non Personalized</option>
						<option value="1" {if $smarty.get.is_customizable eq '1'} selected {/if} >Others</option>
					
		</select>
		</div></div>
		<div class="ITEM_FIRST_CHILD_DIV">
	        <div class="ITEM_LABLE_DIV">Source Name</div>
	        <div class="ITEM_FIELD_DIV">
	        	{if $smarty.post.sourceid}
	                {assign var=s_id_array value=$smarty.post.sourceid}
	            {elseif $smarty.get.sourceid}
	                {assign var=s_id_array value=$smarty.get.sourceid}
	            {/if}
	            <span id="source_span">
		            <select name="sourceid[]" size="4" style="width:300px;" multiple>
		            	<option value="">--select source name/s--</option>
			           	{foreach from=$order_sources item=item key=key}
			            	{foreach from=$s_id_array item=s_id}	            		
			               		{if $s_id == $item.source_id}
			            			{assign var=source_selected value="Y"}
			            		{/if}     
			            	{/foreach}
			                <option value="{$item.source_id}" {if $source_selected == "Y"} selected {/if}>{$item.source_name} ({$item.source_manager_email})</option>
			                {assign var=source_selected value="" }
			           	{/foreach}
					</select>
				</span>
	        </div>  
        </div>
		
		</div></div>
        <div class="ITEM_SEARCH_BUTTON_DIV">
        <div style="float:left;margin-left:100px;"><input type="submit" value="{$lng.lbl_search|escape}" onclick="javascript: document.itemsearch.mode.value = 'search'; document.itemsearch.submit();" /></div>

        <div style="float:right;margin-right:100px;"><input type="submit" name="item_assignment_done" value="    Download Report    " onclick="javascript: document.itemsearch.mode.value = 'download_report'; document.itemsearch.submit();"></div>
        </div>
        
</form>


{/capture}
{include file="dialog.tpl" title="Item Assignment" content=$smarty.capture.dialog extra='width="760px"'}
<br /><br />
