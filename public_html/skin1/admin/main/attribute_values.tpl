{* $Id: featured_products.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
<base href="{$http_location}/">
<script type="text/javascript" src="{$http_location}/skin1/myntra_js/jquery.min.js"></script>
<script type="text/javascript" src="{$http_location}/skin1/js_script/jquery.validate.js"></script>
{include file="main/include_js.tpl" src="main/popup_product.js"}
{literal}
<style>
 .error_block{
 	background:red;
 }
</style>
  <script language="Javascript">
	function checkForDuplicateFilterOrder(){
		var arr_order=[];
		var no_error=true;
		$('.data_row').each(function(index) {
				var dataRow = $(this);
				if($(this).find(":checkbox").attr('checked')){
					if(jQuery.inArray($(this).find('.filter_order').val(),arr_order) != -1){
						alert('filter order:'+$(this).find('.filter_order').val()+' already exist. Please specify different orders');
						no_error=false;
						jQuery(dataRow).addClass('error_block');
						return false;
					}
					arr_order.push($(this).find('.filter_order').val());
				}
			});
		return no_error;
		}
  	function confirmDelete()
      {
          var flag = confirm('Are you sure you want to delete?')
          if(!flag)
          {
             return false;
          }
          else
          {
          	 document.groupfrm.mode.value = 'delete';
          	 document.groupfrm.submit();
          	 return true;
          }
      }
      
      function onChangeArticleType(obj,issearch)
      {
      	  	
      	  $.get(http_loc+'/admin/catalog/attribute_values.php',{mode:"getArticleAttributes",article_id:obj.value}, function(data) {
	   
	   		var jsonData = jQuery.parseJSON(data);
	       	jQuery(obj).parent().parent().find("select:eq(1)").find("option").remove();
			var optionshtml='';
			if(issearch=='true'){
      	  		optionshtml+='<option value="" >All</option>';
      	  	}
			$.each(jsonData.attributes, function(intIndex,attribute){
				optionshtml+='<option value="'+attribute.id+'" >'+attribute.name+'</option>';
			});
			jQuery(obj).parent().parent().find("select:eq(1)").append(optionshtml);
	   		
      	  });
      }

      function checkBrandCode4Char()
      {
    	  var article = document.frmG.article_type_id;
    	  var article_selected_index = article.selectedIndex;
    	  var article_text = article.options[article_selected_index].text;

    	  var attribute = document.frmG.attribute_type_id;
    	  var attribute_selected_index = attribute.selectedIndex;
    	  var attribute_text = attribute.options[attribute_selected_index].text;

    	  if(article_text=='Global' && attribute_text=='Brand'){
    		  var code = document.frmG.attribute_code.value;
    		  if(code.length !=4){
        		  alert("Brand Code must be 4 characters long");
        		  return false;
    		  }
    	  }
    	  document.frmG.submit();
          return true;
      }
  </script>
{/literal}
<a name="featured" />

{$lng.txt_featured_products}

<br /><br />

{capture name=dialog}
<form  action="{$form_action}" method="post" name="groupfrm1" enctype="multipart/form-data">
<input type="hidden" name="mode" value="search" >
&nbsp;&nbsp;Search By Article Type :&nbsp;&nbsp;<select name="search_article_type_id" onchange="onChangeArticleType(this,'true');">
        <option value="">Global</option>
        {foreach from=$article_types item=types}
		    <option value="{$types.id}" {if $search_article_type_id eq $types.id}selected="selected"{/if} >{$types.typename}</option>
		{/foreach}
	</select>
    &nbsp;&nbsp;Search By Type :&nbsp;&nbsp;<select name="search_type">
        <option value="">All</option>
		{foreach from=$article_type_attributes key=attr_type_id item=attr_types}
			{if ($attr_type_id eq $search_article_type_id or ($search_article_type_id eq "" and $attr_type_id eq "global"))}
				{foreach from=$attr_types  item=types}
		    		<option value="{$types.id}" {if $search_type eq $types.id} selected{/if} >{$types.attribute_type}</option>
		    	{/foreach}
		    {/if}
		{/foreach}
	</select>
	&nbsp;&nbsp;&nbsp;<input type="submit" value="search" name="sub"> 


</form>
<br>
<form action="{$form_action}" method="post" name="groupfrm" enctype="multipart/form-data" onsubmit="return checkForDuplicateFilterOrder();">
<input type="hidden" name="mode"  />
<input type="hidden" name="search_type" value="{$smarty.post.search_type}" />
<table cellpadding="3" cellspacing="1" width="100%">

<tr class="TableHead">
	<td width="5%">&nbsp;</td>
	<td width="10%">Article Type</td>
	<td width="15%">Attribute Type</td>
	<td width="20%">Attribute Value</td>
	<td width="20%">Attribute Code</td>
	<td width="10%">filter order</td>
	<td width="20%" align="center"  style="display:none;">Applicable Set</td>
	<td width="3%" align="center">Active</td>
	<td width="3%" align="center">Featured</td>
	<td width="4%" align="center">Latest</td>
</tr>

{if $attributes}

{section name=grid loop=$attributes}

<tr class="{cycle values="data_row ,data_row TableSubHead"}">
    <input type="hidden" name="posted_data[{$attributes[grid].id}][id]" value={$attributes[grid].id} />
	<td><input type="checkbox" name="posted_data[{$attributes[grid].id}][to_delete]" /></td>
	<td align="center">
	<select name="posted_data[{$article_type[grid].id}][article_type_id]" onchange="onChangeArticleType(this);">
				<option value="">Global</option>
				{foreach from=$article_types item=types}
				<option value="{$types.id}" {if $attributes[grid].catalogue_classification_id eq $types.id} selected{/if}>{$types.typename}</option>
				{/foreach}
	</select>
	</td>	
	<td align="center">
	<select name="posted_data[{$attributes[grid].id}][attribute_type_id]">

		{foreach from=$article_type_attributes key=attr_type_id item=attr_types}
			{if ($attr_type_id eq $attributes[grid].catalogue_classification_id) or ($attributes[grid].catalogue_classification_id eq "" and $attr_type_id eq "global")}
				{foreach from=$attr_types  item=types}
		    		<option value="{$types.id}" {if $attributes[grid].attribute_type_id eq $types.id} selected{/if} >{$types.attribute_type}</option>
		    	{/foreach}
		    {/if}
		{/foreach}

	</select>
	</td>
	<td align="center"><input type="text" name="posted_data[{$attributes[grid].id}][attribute_value]" value="{$attributes[grid].attribute_value}" /></td>
	<td align="center">
		<label>{$attributes[grid].attribute_code}</label>
	</td>
	<td align="center"  style="display:none;">
	<input type="text" value="{$attributes[grid].applicable_set}" name="posted_data[{$attributes[grid].id}][applicable_set]">
	</td>
	
	</td>
	<td align="center"  >
	<input type="text" class="filter_order" value="{$attributes[grid].filter_order}" name="posted_data[{$attributes[grid].id}][filter_order]">
	</td>
	
	<td align="center">
	<select name="posted_data[{$attributes[grid].id}][is_active]">
		<option value="1"{if $attributes[grid].is_active eq "1"} selected="selected"{/if}>{$lng.lbl_yes}</option>
		<option value="0"{if $attributes[grid].is_active eq "0"} selected="selected"{/if}>{$lng.lbl_no}</option>
	</select>
	</td>
	<td align="center">
	<select name="posted_data[{$attributes[grid].id}][is_featured]">
		<option value="1"{if $attributes[grid].is_featured eq "1"} selected="selected"{/if}>{$lng.lbl_yes}</option>
		<option value="0"{if $attributes[grid].is_featured eq "0"} selected="selected"{/if}>{$lng.lbl_no}</option>
	</select>
	</td>
	<td align="center">
	<select name="posted_data[{$attributes[grid].id}][is_latest]">
		<option value="1"{if $attributes[grid].is_latest eq "1"} selected="selected"{/if}>{$lng.lbl_yes}</option>
		<option value="0"{if $attributes[grid].is_latest eq "0"} selected="selected"{/if}>{$lng.lbl_no}</option>
	</select>
	</td>
</tr>

{/section}
<td colspan="5"><br /><br /></td>
{else}

<tr>
 <td colspan="4" align="center">No Article types available.</td>
</tr>
{/if}

<!--<tr>
<td colspan="5"><br /><br />{include file="main/subheader.tpl" title=$lng.lbl_add_product}</td>
</tr> -->

<tr>
	<td colspan="5" class="SubmitBox">
	<input type="button" value="Delete Selected" onclick="return confirmDelete();" />
	<input type="submit" value="Update Selected" onclick="javascript:document.groupfrm.mode.value = 'update';" />

	</td>
</tr>


</table>
</form>
<form name="frmG" action="{$form_action}" method="post" enctype="multipart/form-data" >
<input type="hidden" name="mode" value="addgroup" />
<input type="hidden" name="search_type" value="{$smarty.post.search_type}" />
<table cellpadding="3" cellspacing="1" width="100%">
   <tr>
        <td><hr/></td>
        <td><hr /></td>
   </tr>
   <tr>
        <td><h2>Add Attribute</h2></td>
        <td>&nbsp;</td>
   </tr>
   <tr>
   		<td>Article Type </td>
		<td>
		<select name="article_type_id" onchange="onChangeArticleType(this);">
				<option value="">Global</option>
				{foreach from=$article_types item=types}
				<option value="{$types.id}" {if $search_article_type_id eq $types.id}selected="selected"{/if}>{$types.typename}</option>
				{/foreach}
		</select>        
		</td>
		   			
        <td>Attribute Type </td>
        <td>
        <select name="attribute_type_id">
        {foreach from=$attribute_types item=types}
		    <option value="{$types.id}" {if $search_type eq $types.id} selected{/if}>{$types.attribute_type}</option>
		{/foreach}
		</select>
        </td>
        </tr>
        <tr>
        <td>Attribute Value</td>
        <td><input type="text" name="attribute_value" value=""  /></td>
        <td>Attribute Code</td>
        <td><input type="text" name="attribute_code" value=""  /></td>
        <td>Filter Order</td>
        <td><input type="text" name="filter_order" value="0"  /></td>
        <td><input type="button" value="Add Attribute" name="sbtFrm" onClick="checkBrandCode4Char()" /></td>
        <td  style="visibility:hidden;">

        <select name="applicable_set">
            <option value="Default">Default</option>

        </select>
        </td>
        <td style="visibility:hidden;">Active</td>
         <td align="center" style="visibility:hidden;">
         
            <select name="is_active">
                <option value="1" >{$lng.lbl_yes}</option>
                <option value="0">{$lng.lbl_no}</option>
            </select>
        </td>
   </tr>
   <tr><td colspan="5"></td> </tr>
   <tr><td colspan="5"></td> </tr>
   <tr>
   <td colspan="4" align="center">
   
   </td>
   </tr>

</table>
</form>
{/capture}
{include file="dialog.tpl" title="Attribute Type Values" content=$smarty.capture.dialog extra='width="100%"'}
<br /><br />



