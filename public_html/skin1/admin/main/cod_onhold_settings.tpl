<script type="text/javascript">
    var states = {$states};
    var range = {$range};
    var displayRange = {$displayRange};
    var zipcodesChosen = {$zipcodesChosen};
    var selectedStates = {$selectedStates};
    var selectedPaymentMethods = {$selectedPaymentMethods};
</script>

<link rel="stylesheet" type="text/css" href="{$http_location}/admin/extjs/resources/css/ext-all.css"/>
<script type="text/javascript" src="{$http_location}/admin/extjs/adapter/ext/ext-base.js"></script>
<script type="text/javascript" src="{$http_location}/admin/extjs/ext-all.js"></script>
<script type="text/javascript" src="{$http_location}/admin/extjs/plugins/superselectbox/SuperBoxSelect.js"></script>
<script type="text/javascript" src="{$http_location}/admin/extjs/plugins/fileuploadfield/FileUploadField.js"></script>
<link rel="stylesheet" type="text/css" href="{$http_location}/admin/extjs/plugins/superselectbox/superboxselect.css"/>
<link rel="stylesheet" type="text/css" href="{$http_location}/admin/extjs/plugins/fileuploadfield/css/fileuploadfield.css"/>
<script type="text/javascript" src="/skin1/admin/main/cod_onhold_settings.js"></script>
<br>
<br>
<div id="contentPanel"></div>