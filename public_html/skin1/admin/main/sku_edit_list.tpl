{* $Id: featured_products.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $
*} {include file="main/include_js.tpl" src="main/popup_product.js"}

{if $access eq 1}

<script type="text/javascript" src="{$http_location}/skin1/myntra_js/jquery.min.js"></script>
<link
	rel="stylesheet" type="text/css"
	href="{$http_location}/skin1/myntra_css/jquery.autocomplete.css" />
<link
	rel="stylesheet" type="text/css"
	href="{$http_location}/skin1/myntra_css/thickbox3.css" />
<script
	type='text/javascript'
	src='{$http_location}/skin1/myntra_js/jquery.bgiframe.min.js'></script>
<script
	type='text/javascript'
	src='{$http_location}/skin1/myntra_js/jquery.ajaxQueue.js'></script>
<script
	type='text/javascript'
	src='{$http_location}/skin1/myntra_js/jquery.autocomplete.pack.js'></script>
<script
	type='text/javascript'
	src='{$http_location}/skin1/myntra_js/thickbox3.js'></script>
{literal}
<script type="text/javascript">
var count=0;
var newCount=0;
var bubbleUp=false;
var toggle_text_elements = {};
$(document).ready(function(){
/* Toggle Text */
toggle_text_all(".toggle-text");
});

/*
 * This function finds all HTML elements with "toggle-text" class and binds the "focus"/"blur" events for
 * toggling the default text in those elements.
 */
function toggle_text_all(selector) {
    $(selector).each(function(e) {
        toggle_text_elements[this.id] = $(this).val();
        addToggleText(this);
    });
}
function addToggleText(elem) {
    $(elem).addClass("blurred");
    $(elem).focus(function() {
        if ($(this).val() == toggle_text_elements[this.id]) {
            $(this).val("");
            $(this).removeClass("blurred");
        }
    }).blur(function() {
        if ($(this).val() == "") {
            $(this).addClass("blurred");
            $(this).val(toggle_text_elements[this.id]);
        }
    });
}
function addNewRow(){
	var theTable=document.getElementById("SKU_TABLE");
	var newRow = theTable.insertRow(2);
	newRow.id = "NEWSKU"+newCount;
	var id="NEWSKU"+newCount;
	newRow.innerHTML="<td align=center><input type='text' id='"+id+":sku_id' value='' ></td>"
		+"<td id='"+id+":style_name'> </td>"
		+"<td align='center'><input type='text' id='"+id+":sku_inv_count' value=0 disabled='disabled'></td><td align='center'>"
	+"<td align=center ><input id='"+id+":sku_wh_count' value=0 disabled='disabled' /></td><td align='center'>"
	+"<td align='center'><label id='"+id+":enabled'>Unassigned</label><input type='button' onclick=\"saveNewRow('"+id+"')\" value='Save' /></td>"
	+"<td align='center'></td>";
	newRow.bgColor="#00FFFF";	
	newCount++;
}
function saveNewRow(skuid){
	var sku_code=document.getElementById(skuid+':sku_id').value;
	var sku_inv_count=document.getElementById(skuid+':sku_inv_count').value;
	var sku_wh_count=document.getElementById(skuid+':sku_wh_count').value;
	var http;
	if (window.XMLHttpRequest)
  	{
  		http=new XMLHttpRequest();
  	}
	else
  	{
  		http=new ActiveXObject("Microsoft.XMLHTTP");
  	}
	http.onreadystatechange=function(){
		if (http.readyState==4 && http.status==200)
		{
			if(http.responseText!=null && http.responseText.search('Success')>=1){
				var splits=http.responseText.split('[[//]]');
				var targetRow=document.getElementById(skuid);
				document.getElementById("messagebox").innerHTML="<font color='green' size=2>"+splits[0]+"</font>";
				var sku_id=splits[1];
				var sku_code=splits[2];
				var sku_inv_count=splits[4];
				var sku_wh_count=splits[5];
				var sku_style_name=splits[6];
				var sku_option_value=splits[7];
				var sku_enabled;
				if(splits[3]!=null && !splits[3].trim()=="")
					sku_enabled=(splits[3]==0?false:true);
				targetRow.id="SKU"+sku_id;
				var id="SKU"+sku_id;
				targetRow.innerHTML="<td align='center'>"+sku_code+"<input type='hidden' id='"+id+":sku_id' value='"+sku_id+"'></td>"
					+"<td align='center' id='"+id+":style_name' >"+sku_style_name+":"+sku_option_value+"</td>"
					+"<td align='center' ><input id='"+id+":sku_inv_count' disabled='disabled' value='"+sku_inv_count+"'/></td>"
					+"<td align='center'>"
						+"<input type='button' value='+' onclick=\"showDialogWithCode('confirmInventoryIncrease','"+sku_code+"')\" />"
						+"<input type='button' value='-' onclick=\"showDialogWithCode('confirmInventoryDecrease','"+sku_code+"')\" />"
					+"</td>"
					+"<td align='center'><input id='"+id+":sku_wh_count' disabled='disabled' value='"+sku_wh_count+"'/></td>"
					+"<td align='center'></td>"
					+"<td align='center'>"+(splits[3].trim()==""?"<label id='"+id+":enabled'>Unassigned</label>":("<input type='checkbox' id='"+id+":enabled' align='middle'  checked='"+sku_enabled+"'  onchange=\"updateSKU('"+id+"','ENABLED','ENABLED')\"  {/literal}{if $checkInStock eq false}disabled=\"disabled\"{/if}{literal} />"))+"</td>"
					+"<td align='center'><input type='button' value='view logs' onclick=\"javascript:window.open('sku_edit_list.php?page=1&mode=viewlog&code="+sku_code+"');\"></td>";
				targetRow.bgColor=null;	
				}else{
				alert(http.responseText);
			}
			var t=setTimeout("clearMessage()",3000);
		}
	};
	var url="sku_edit_list.php?";
	url+="sku_code="+sku_code+"&mode=addnew"+"&sku_inv_count="+sku_inv_count+"&sku_wh_count="+sku_wh_count;
	
	http.open("GET",url,true);
	http.send(null);
}
function updateSKU(skuid,operation,target)
{
	var targetSku=document.getElementById(skuid+':sku_id').value;
	var sku_inv_count=document.getElementById(skuid+':sku_inv_count').value;
	var sku_wh_count=document.getElementById(skuid+':sku_wh_count').value;
	var sku_enabled=document.getElementById(skuid+':enabled').checked;
	var sku_jit=document.getElementById(skuid+':jit').checked;
	var input_inv_value=0;
	
	var http;
	if (window.XMLHttpRequest)
  	{
  		http=new XMLHttpRequest();
  	}
	else
  	{
  		http=new ActiveXObject("Microsoft.XMLHTTP");
  	}
	http.onreadystatechange=function(){
		if (http.readyState==4 && http.status==200)
		{
			if(http.responseText!=null && http.responseText.search('Success')>=1){
				updateSkuRowHtml(http.responseText);
			}else{
				if(operation=='ENABLED'){
					var sku_enabled=document.getElementById(skuid+':enabled');
					sku_enabled.checked=(!sku_enabled.checked);
				} else if (operation=='JIT_SOURCED'){
					var sku_jit=document.getElementById(skuid+':jit');
					sku_jit.checked=(!sku_jit.checked);
				}
				alert(http.responseText);
			}
			
		}
	}; 
	var url="sku_edit_list.php?";
	url+="sku_id="+targetSku+"&mode=update";
	if(target=='INV'){
		url+="&property=INV&value="+((operation =='ADD'?Number(input_inv_value):-Number(input_inv_value)));
	}else if(target=='ENABLED'){
		url+="&property=ENABLED&value="+(sku_enabled);
	}else if(target=='JIT_SOURCED'){
		url+="&property=JIT_SOURCED&value="+(sku_jit);
	}else{
		alert("Wrong property access!!");
		return;
	}
	http.open("GET",url,true);
	http.send(null);
}

function updateSkuRowHtml(text){
	
		var splits=text.split('[[//]]');
		var skuid="SKU"+splits[1];
		var targetSku=document.getElementById(skuid+':sku_id');
		
		var theTable=document.getElementById("SKU_TABLE");
		if(targetSku!=null && bubbleUp){
			var targetRow=document.getElementById(skuid);
			var htmlS=targetRow.innerHTML;
			theTable.deleteRow(targetRow.rowIndex);
			targetRow = theTable.insertRow(2);
			targetRow.id=skuid;
			targetRow.innerHTML=htmlS;
		}
		if(targetSku==null){
			var targetRow = theTable.insertRow(2);
			var sku_id=splits[1];
			var sku_code=splits[2];
			var sku_inv_count=splits[4];
			var sku_wh_count=splits[5];
			var sku_style_name=splits[6];
			var sku_option_value=splits[7];
			var sku_enabled;
			if(splits[3]!=null && !splits[3].trim()=="")
				sku_enabled=(splits[3]==0?false:true);
			targetRow.id="SKU"+sku_id;
			var id="SKU"+sku_id;
			targetRow.innerHTML="<td align='center'>"+sku_code+"<input type='hidden' id='"+id+":sku_id' value='"+sku_id+"'></td>"
				+"<td align='center' id='"+id+":style_name' >"+sku_style_name+":"+sku_option_value+"</td>"
				+"<td align='center' ><input id='"+id+":sku_inv_count' disabled='disabled' value='"+sku_inv_count+"'/></td>"
				+"<td align='center'>"
					+"<input type='button' value='+' onclick=\"showDialogWithCode('confirmInventoryIncrease','"+sku_code+"')\" />"
					+"<input type='button' value='-' onclick=\"showDialogWithCode('confirmInventoryDecrease','"+sku_code+"')\" />"
				+"</td>"
				+"<td align='center'><input id='"+id+":sku_wh_count' disabled='disabled' value='"+sku_wh_count+"'/></td>"
				+"<td align='center'></td>"
				+"<td align='center'>"+(splits[3].trim()==""?"<label id='"+id+":enabled'>Unassigned</label>":("<input type='checkbox' id='"+id+":enabled' align='middle'  checked='"+sku_enabled+"'  onchange=\"updateSKU('"+id+"','ENABLED','ENABLED')\" {/literal}{if $checkInStock eq false}disabled=\"disabled\"{/if}{literal} />"))+"</td>"
				+"<td align='center'><input type='button' value='view logs' onclick=\"javascript:window.open('sku_edit_list.php?page=1&mode=viewlog&code="+sku_code+"');\"></td>";
			targetRow.bgColor=null;	
		}else{
			var sku_inv_count=document.getElementById(skuid+':sku_inv_count');
			var sku_wh_count=document.getElementById(skuid+':sku_wh_count');
			var sku_enabled=document.getElementById(skuid+':enabled');
			var sku_jit=document.getElementById(skuid+':jit');
			
			sku_inv_count.value=splits[4];
			sku_wh_count.value=splits[5];
			var sku_style_name=splits[6];
			var sku_option_value=splits[7];
			
			document.getElementById(skuid+':style_name').innerHTML=sku_style_name+":"+sku_option_value; 
			if(splits[3]!=null && !splits[3].trim()=="")
				sku_enabled.checked=(splits[3]==0?false:true);

			if(splits[8]!=null && splits[8].trim()!="")
				sku_jit.checked=(splits[8]==0?false:true);
		}
	
		document.getElementById("messagebox").innerHTML="<font color='green' size=2>"+splits[0]+"</font>";
		
	
		var t=setTimeout("clearMessage()",3000);
}

function clearMessage(){
	document.getElementById('messagebox').innerHTML='';
}

function generateNewLotID(){
	var date=new Date();
	var year=date.getFullYear()+"";
	var month=date.getMonth()+1+"";
	var day=date.getDate()+"";
	var day_part;
	if(date.getHours()<6)
		day_part="1";
	else if(date.getHours()<12)
		day_part="2";
	else if(date.getHours()<18)
		day_part="3";
	else 
		day_part="4";

	//day divided into 4 parts
	var str=year;
	
	str+=(month.length==1?"0"+month:month);
	str+=(day.length==1?"0"+day:day);
	if($("input[name='inward_type_I']:checked").val() == "vendor") {
		str+="/MTR/";
	} else {
		str+="/MTRJIT/";
	}
	str+=(day_part.length==1?"0"+day_part:day_part);
	$("#lot_id_I").val(str);
}

function changeReasonI(){
	var value=$('#reasons_I').val();
	var element=$('#other_reason_I');
	$("#lot_id_I").attr("disabled","disabled");
	if(value=='{/literal}{$new_purchase_id_inc}{literal}'){
		generateNewLotID();
		$("#price_row").show();
	}else{
		$("#lot_id_I").val("");
		$("#price_row").hide();
	}
	if(value=='{/literal}{$others_id_inc}{literal}'){
		
		element.attr("disabled","");
	}else{
		element.attr("disabled","disabled");
		element.val("If Any Other Reason.");
		element.addClass("blurred");
	}
}
function changeReasonD(){
	var value=$('#reasons_D').val();
	var element=$('#other_reason_D');
	if(value=='{/literal}{$others_id_dec}{literal}'){
		
		element.attr("disabled","");
	}else{
		element.attr("disabled","disabled");
		element.val("If Any Other Reason.");
		element.addClass("blurred");
	}
}

function showDialog(dg){
	$("#"+dg).css("display","block");
	tb_show("","#TB_inline?height=350&amp;width=500&amp;inlineId="+dg+";modal=true","");
	if(dg=="confirmInventoryDecrease"){
		$("#sku_code_value_D").val("");
		$("#article_name_D").html("");
		$("#article_id_D").html("");
		$("#quantity_D").val("");
		$("#other_reason_D").val("If Any Other Reason.");
		$("#inv_D").html("");
		$("#wh_D").html("");
		$("#sku_code_value_D").focus();
	}
	else if(dg=="setInventory"){
		$("#sku_code_value_S").val("");
		$("#article_name_S").html("");
		$("#article_id_S").html("");
		$("#quantity_S").val("");
		$("#inv_S").html("");
		$("#wh_S").html("");
		$("#lastmodified_S").html("");
		$("#sku_code_value_S").focus();
	}
	else{
		$("#sku_code_value_I").val("");
		$("#article_name_I").html("");
		$("#article_id_I").html("");
		$("#quantity_I").val("");
		$("#price_I").val("");
		$("#vendor_name_I").html("");
		$("#other_reason_I").val("If Any Other Reason.");
		$("#lot_id_I").val("");
		$("#inv_I").html("");
		$("#wh_I").html("");
		$("#inward_type_I_vendor").attr('checked', true);
		$("#sku_code_value_I").focus();

		$("input[name='inward_type_I']").click(function(){
			if ($("#reasons_I").val()=="{/literal}{$new_purchase_id_inc}{literal}") {
				generateNewLotID();
			}
	    });
	}
	bubbleUp=true;
	generateNewLotID();
}

function showDialogWithCode(dg,code){
	showDialog(dg);
	if(dg=="confirmInventoryDecrease")
		$("#sku_code_value_D").val(code);
	else
		$("#sku_code_value_I").val(code);
	bubbleUp=false;
}

function cancel_dialog(dg){
	$("#"+dg).css("display","none");
	tb_remove();
	bubbleUp=false;
}

function list_skus(box ){
	var query=document.getElementById(box).value;
	if(query.length!=4)
		return;
	var http;
	if (window.XMLHttpRequest)
  	{
  		http=new XMLHttpRequest();
  	}
	else
  	{
  		http=new ActiveXObject("Microsoft.XMLHTTP");
  	}
	http.onreadystatechange=function(url){
		if (http.readyState==4 && http.status==200)
		{
			if(http.responseText!=null){
				var data=http.responseText.trim().split(',');
				$("#"+box).autocomplete(data);
			}
			
						
		}
	};
	var url="sku_edit_list.php?";
	
	url+="mode=list_skus"+"&query="+query;
	
	http.open("GET",url,true);
	http.send(null);
}

function update_inventory(tp){
	if(!validateBeforeSubmit(tp))
		return;
	var property = "INV";
	if (tp  == 'S') property = "SET";
	var url="sku_edit_list.php?mode=update&property=" + property + "&tp="+tp+"&code="+$("#sku_code_value_"+tp).val();
	if(tp=='D'){
		url+="&value="+(-Number($("#quantity_"+tp).val()));
	}else{
		url+="&value="+$("#quantity_"+tp).val();
	}
	url+="&reasonid="+$("#reasons_"+tp).val();
	url+="&extra_reason="+($("#other_reason_"+tp).val()=='If Any Other Reason.'?"":$("#other_reason_"+tp).val());
	if(tp=='I'){
		url+="&lot_id="+$("#lot_id_I").val();
		url+="&price="+$("#price_I").val();
		url+="&inwardtype="+$("input[name='inward_type_I']:checked").val();
	}
	
	$.get(url,function(data){
		if(data!=null && data.search('Success')>=1){
			if(tp=='S'){
                                $("#sku_code_value_S").val("");
                                $("#article_name_S").html("");
                                $("#article_id_S").html("");
                                $("#quantity_S").val("");
                                $("#inv_S").html("");
                                $("#wh_S").html("");
                                $("#wh_S").html("");
                                $("#lastmodified_S").html("");
                                $("#sku_code_value_S").focus();
				document.getElementById("messagebox_S").innerHTML="<font color='green' size=2>Successfully updated count</font>";
				var t = setTimeout("clearDialogMessage()", 2000);
                        } else {

				updateSkuRowHtml(data);
				if(tp=='I'){
				cancel_dialog('confirmInventoryIncrease');
				} else {
					cancel_dialog('confirmInventoryDecrease');
				}
			}
		}else{	
			alert(data);
		}
	});

}

function clearDialogMessage(){
	document.getElementById('messagebox_S').innerHTML='';
}

var sku_code_invalid=true;
var vendor_invalid=true;
function setskucode(id,tp){
	if($("#"+id).val().trim()==''){
		sku_code_invalid=true;
		return;
	}
	var url="sku_edit_list.php?mode=getarticle&query="+$("#"+id).val();
	$.get(url,function(data){
		if(data.trim()!="INVALID"){
			var splits=data.split('[[//]]');
			$("#article_id_"+tp).html(splits[1]);
			$("#article_name_"+tp).html(splits[2]);
			$("#inv_"+tp).html(splits[3]);
			$("#wh_"+tp).html(splits[4]);
			sku_code_invalid=false;
			if (tp=="S")  {
				$("#lastmodified_"+tp).html(splits[5]);
			}
		}else{
			//alert("Invalid SKU code");
			$("#article_id_"+tp).html("");
			$("#article_name_"+tp).html("");
			$("#inv_"+tp).html("");
			$("#wh_"+tp).html("");
			sku_code_invalid=true;
			if (tp=="S") $("#lastmodified_"+tp).html("");
		}
		});
}

function setvendor(){

	var splits=$("#lot_id_I").val().split('/');
	if(splits.length <2)
		return;
	if('MTR'==splits[1] || 'MTRJIT'==splits[1]){
		vendor_invalid=false;
		return;
	}
	var url="sku_edit_list.php?mode=getvendor&query="+splits[1];
	$.get(url,function(data){
		if(data.trim()!="INVALID"){
			$("#vendor_name_I").html(data);
			vendor_invalid=false;
		}else{
			$("#vendor_name_I").html('');
			vendor_invalid=true;
		}
	});
}

function validateBeforeSubmit(tp){
	if($("#quantity_"+tp).val().trim()=="" || isNaN($("#quantity_"+tp).val())){
		alert("Invalid quantity");
		return false;
	}
	if(sku_code_invalid){
		alert("SKU code is invalid");
		return false;
	}
	if(vendor_invalid && tp=='I' && $("#lot_id_I").val().trim()!=""){
		alert("Invalid vendor code in LOT ID");
		return false;
	}
	
	if(tp=="I" && $("#reasons_I").val()=="{/literal}{$new_purchase_id_inc}{literal}" && ($("#price_I").val().trim()==""|| isNaN($("#price_I").val()))){
		alert("Price value is invalid");
		return false;
	}
		
	if (tp == "S" && !$("#lastmodified_S").text().trim()=="") {
		// need to validate the datetime as well
		//var d = dateFormat($("#lastmodified_S").val().trim(), "yyyy-mm-dd HH:MM:ss");
		var tmp = $("#lastmodified_S").text().trim().split(" ");

		var tmp1 = tmp[0].trim().split("-");
		var tmp2 = tmp[1].trim().split(":");
		
		// Javascript Date object month is '0' based
		var d = new Date(parseInt(tmp1[0],10), parseInt(tmp1[1],10) - 1, parseInt(tmp1[2],10), 
				parseInt(tmp2[0],10), parseInt(tmp2[1],10), parseInt(tmp2[2],10));
         	
		var currentDate = new Date();
                var diff = ((currentDate - d)/(60*60*1000));

                if (diff < 72) {
			alert("last modified is less than 72 hours");
			return flase;
		}
	}
	return true;
}

</script>
{/literal}

<div id="confirmInventoryDecrease" style="display:none">
<table width="100%"  cellspacing="5" >
	<tr class="TableHead">
	<td colspan="2" align="center">Confirm Inventory Decrease</td>
	</tr>
	<tr > <td colspan="2"/></tr>
	<tr>
		<td>SKU Code * :</td>
		<td><input class="textbox" name="sku_code_value_D" style="font-size:20px"
			id="sku_code_value_D" onkeyup="list_skus('sku_code_value_D')"
			value="" autocomplete="off" onblur="setskucode('sku_code_value_D','D')"/></td>
	</tr>
	<tr>
		<td>Article Name :</td>
		<td><label id="article_name_D"></label></td>
	</tr>
	<tr>
		<td>Article ID :</td>
		<td><label id="article_id_D"></label></td>
	</tr>
	<tr>
		<td>Inventory Count :</td>
		<td><label id="inv_D"></label></td>
	</tr>
	<tr>
		<td>Warehouse Count :</td>
		<td><label id="wh_D"></label></td>
	</tr>
	<tr>
		<td>Quantity to Remove:</td>
		<td><input class="textbox" type="text" name="quantity_D"
			id="quantity_D" value="" style="font-size:20px" /></td>
	</tr>
	<tr>
		<td>Reason For :</td>
		<td><select id="reasons_D" onchange="changeReasonD()"  style="font-size:20px">
			{foreach from=$inv_reason.D key=code item=reason}
			<option value="{$code}">{$reason}</option>
			{/foreach}
		</select></td>
	</tr>
	<tr>
		<td></td>
		<td><input class="toggle-text blurred" name="other_reason_D"
			id="other_reason_D" style="font-size:20px"
			value="If Any Other Reason."
			disabled="disabled" /></td>
	</tr>
	<tr > <td/><td/></tr>
	<tr > <td/><td/></tr>
	
	<tr>
		<td align="right"></td>
		<td align="center">
		<input type="button" onclick="update_inventory('D')"
			value="Confirm" style="height:40">&nbsp;&nbsp;&nbsp;&nbsp;<input type="button" onclick="cancel_dialog('confirmInventoryDecrease')"
			value="Cancel" style="height:40"></td>
	</tr>
</table>
</div>

<div id="setInventory" style="display:none">
<div><span id="messagebox_S"> </span></div>
<table width="100%"  cellspacing="5" >
	<tr class="TableHead">
	<td colspan="2" align="center">Confirm Set Inventory</td>
	</tr>
	<tr > <td colspan="2"/></tr>
	<tr>
		<td>SKU Code * :</td>
		<td><input class="textbox" name="sku_code_value_S" style="font-size:20px"
			id="sku_code_value_S" onkeyup="list_skus('sku_code_value_S')"
			value="" autocomplete="off" onblur="setskucode('sku_code_value_S','S')"/></td>
	</tr>
	<tr>
		<td>Article Name :</td>
		<td><label id="article_name_S"></label></td>
	</tr>
	<tr>
		<td>Article ID :</td>
		<td><label id="article_id_S"></label></td>
	</tr>
	<tr>
		<td>Inventory Count :</td>
		<td><label id="inv_S"></label></td>
	</tr>
	<tr>
		<td>Warehouse Count :</td>
		<td><label id="wh_S"></label></td>

	</tr>
	<tr>
                <td>Last Updated :</td>
                <td><label id="lastmodified_S"></label></td>
        </tr>

	<tr>
		<td>Quantity to Set:</td>
		<td><input class="textbox" type="text" name="quantity_S"
			id="quantity_S" value="" style="font-size:20px" /></td>
	</tr>
	
	<tr>
		<td align="right"></td>
		<td align="center">
		<input type="button" onclick="update_inventory('S')"
			value="Confirm" style="height:40">&nbsp;&nbsp;&nbsp;&nbsp;<input type="button" onclick="cancel_dialog('setInventory')"
			value="Cancel" style="height:40"></td>
	</tr>
</table>
</div>

<div id="confirmInventoryIncrease" style="display:none">
<table width="100%"  cellspacing="5" style="font-wiight=bolder">
	<tr class="TableHead">
	<td colspan="2" align="center">Confirm Inventory Increase</td>
	</tr>
	<tr > <td colspan="2"/></tr>
	<tr>
		<td>SKU Code * :</td>
		<td><input class="textbox" name="sku_code_value_I" style="font-size:20px"
			id="sku_code_value_I" onkeyup="list_skus('sku_code_value_I')"
			value="" autocomplete="off" onblur="setskucode('sku_code_value_I','I')"/></td>
	</tr>
	<tr>
		<td>Article Name :</td>
		<td><label id="article_name_I"></label></td>
	</tr>
	<tr>
		<td>Article ID :</td>
		<td><label id="article_id_I"></label></td>
	</tr>
		<tr>
		<td>Inventory Count :</td>
		<td><label id="inv_I"></label></td>
	</tr>
	<tr>
		<td>Warehouse Count :</td>
		<td><label id="wh_I"></label></td>
	</tr>
	<tr id="price_row">
		<td>Per Item Price * :</td>
		<td><input class="textbox" type="text" name="price_I" id="price_I" value=""  style="font-size:20px"/></td>
	</tr>
	<tr>
		<td>Quantity to Add:</td>
		<td><input class="textbox" type="text" name="quantity_I"
			id="quantity_I" value=""  style="font-size:20px"/></td>
	</tr>
	<tr>
		<td>Reason For :</td>
		<td><select id="reasons_I" onchange="changeReasonI()"  style="font-size:20px">
			{foreach from=$inv_reason.I key=code item=reason}
			<option value="{$code}">{$reason}</option>
			{/foreach}
		</select></td>
	</tr>
	<tr>
		<td></td>
		<td><input class="toggle-text blurred" name="other_reason_I"
			id="other_reason_I" style="font-size:20px" 
			value="If Any Other Reason."
			disabled="disabled" /></td>
	</tr>
	<tr>
		<td>Lot Id :</td>
		<td><input class="textbox" name="lot_id_I" id="lot_id_I" value=""
			autocomplete="off" onkeypress="setvendor()" style="font-size:20px" disabled="disabled"/></td>
	</tr>
	<tr>
		<td>Vendor Name :</td>
		<td><label id="vendor_name_I"></label></td>
	</tr>
	<tr>
		<td>Inward Type :</td>
		<td>
			<input type="radio" name="inward_type_I" id="inward_type_I_vendor" value="vendor"> Vendor Purchase
			<input type="radio" name="inward_type_I" id="inward_type_I_jit" value="jit"> JIT Purchase
		</td>
	</tr>
	<tr > <td/><td/></tr>
	<tr > <td/><td/></tr>
	<tr align="left">
		<td align="right"></td>
		<td align="center">
		<input type="button" onclick="setvendor();update_inventory('I');"  style="height:40"
			value="Confirm">&nbsp;&nbsp;&nbsp;&nbsp;<input type="button" onclick="cancel_dialog('confirmInventoryIncrease')"  style="height:40" 
			value="Cancel"></td>
	</tr>
</table>
</div>
<div><span id=messagebox> </span></div>
<input
	type='button' onclick="showDialog('confirmInventoryIncrease')" value="Increase Inventory" />
<input
	type='button' onclick="showDialog('confirmInventoryDecrease')" value="Decrease Inventory" />
	
<input type='button' onclick="showDialog('setInventory')" value="Set Inventory" {if $checkInStock eq false}disabled="disabled"{/if}/>

<table id="SKU_TABLE" cellpadding="3" cellspacing="1" width="100%">
	<tr class="TableHead">
		<td colspan="8">{foreach from=$alphabets item=alphabet} {if $filter eq
		$alphabet} <a href="./sku_edit_list.php?filter={$alphabet}"><font
			size="4" color="green">{$alphabet}</font></a>&nbsp;&nbsp; {else} <a
			href="./sku_edit_list.php?filter={$alphabet}">{$alphabet}</a>&nbsp;&nbsp;
		{/if} {/foreach}</td>
	</tr>
	<tr class="TableHead">
		<td width="10%">#Sku Code</td>
		<td width="25%">Style</td>
		<td colspan="2" width="20%">System Inventory Count</td>
		<td width="15%">Warehouse Count</td>
		<td width="10%">Enable</td>
		<td width="10%">Jit Sourced</td>
		<td width="10%"></td>
	</tr>
	{foreach from=$skus item=sku}
	<tr id="SKU{$sku.sku_id}" bgcolor="">
		<td align="center">{$sku.sku_code}<input type="hidden"
			id="SKU{$sku.sku_id}:sku_id" value="{$sku.sku_id}"></td>
		<td align="center" id='SKU{$sku.sku_id}:style_name'>{$sku.style_name}:{$sku.option_value}</td>
		<td align="center"><input id="SKU{$sku.sku_id}:sku_inv_count"
			disabled="disabled" value="{$sku.sku_inv_count}" /></td>
		<td align="center"><input
			type="button" value="+"
			onclick="showDialogWithCode('confirmInventoryIncrease','{$sku.sku_code}')" /> <input
			type="button" value="-"
			onclick="showDialogWithCode('confirmInventoryDecrease','{$sku.sku_code}')" /></td>
		<td align="center"><input id="SKU{$sku.sku_id}:sku_wh_count"
			disabled="disabled" value="{$sku.sku_wh_count}" /></td>
		<td align="center">{if isset($sku.sku_enabled)}<input type="checkbox" 
			id="SKU{$sku.sku_id}:enabled" align="middle" {if $sku.sku_enabled
			eq '1'} checked {/if}"  onchange="updateSKU('SKU{$sku.sku_id}','ENABLED','ENABLED')" {if $checkInStock eq false}disabled="disabled"{/if}/>{else}<label
			id="SKU{$sku.sku_id}:enabled">Unassigned</label>{/if}</td>
		<td align="center">
			{if isset($sku.jit_sourced)}
				<input type="checkbox" id="SKU{$sku.sku_id}:jit" align="middle" {if $sku.jit_sourced eq '1'} checked {/if}" onchange="updateSKU('SKU{$sku.sku_id}','JIT_SOURCED','JIT_SOURCED')" />
			{else}
				<label id="SKU{$sku.sku_id}:jit">Unassigned</label>
			{/if}
		</td>
		<td align='center'><input type='button' value='view logs' onclick="javascript:window.open('sku_edit_list.php?page=1&mode=viewlog&code={$sku.sku_code}');"></td>
	</tr>
	{/foreach}
</table>
{else}
    <p>This page is blocked!!</p>
{/if}
