{* $Id: statistics.tpl,v 1.24.2.1 2006/06/16 10:47:41 max Exp $ *}
{include file="main/include_js.tpl" src="main/calendar2.js"}
{capture name=dialog}

<form action="" method="post" name="proreportform">
<input type="hidden" name="mode"  />

<table cellpadding="1" cellspacing="1">


<tr>
	<th align="right">{$lng.lbl_reporttype}:</th>
	<td>
	     <select name='report_type'>
	      <option value="week"  { if $reportType == "week"} selected {/if} >Weekly</option>
              <option value="month" { if $reportType == "month"} selected {/if} >Monthly</option> 
	      <option value="daily" { if $reportType == "daily"} selected {/if} >Daily</option>
	      
           </select>
	</td>
	<th align="right">{$lng.lbl_region}:</th>
	<td>
	    <select name='region'>
	      <option value="all" >All</option>
	       {section name=idx loop=$regions}
                  <option value="{$regions[idx].region}" >{$regions[idx].region}</option>
	       {/section}
              
	    </select>
	</td>
</tr>

<tr>
	<th align="right">{$lng.lbl_report_from}:</th>
	<td><input type="text" name='Startdate' value="{$SDate}"/><a href="javascript:cal4.popup();" ><img src="{$cdn_base}/images/cal.gif" width="16" height="16" border="0" alt="Click Here to Pick up the date"></a></td>
	<th align="right">{$lng.lbl_report_to}:</th>
	<td><input type="text" name='Enddate'  value="{$EDate}" /><a href="javascript:cal5.popup();" class=""><img src="{$cdn_base}/images/cal.gif" width="16" height="16" border="0" alt="Click Here to Pick up the date"></a></td>
</tr>

<tr>
	<td colspan="4"><input type="submit" value="{$lng.lbl_submit|escape}" /></td>
</tr>
<script language="javascript">
      var cal4 = new calendar2(document.forms['proreportform'].elements['Startdate']);
      cal4.year_scroll = true;
      cal4.time_comp = false;

       var cal5 = new calendar2(document.forms['proreportform'].elements['Enddate']);
      cal5.year_scroll = true;
      cal5.time_comp = false;


</script>


</table>
</form>

{/capture}
{include file="dialog.tpl" title='' content=$smarty.capture.dialog extra='width="100%"'}

<br />
<br />
{if $mode}
{capture name=dialog}
<table cellpadding="1" cellspacing="1" width="100%">

<tr class="TableHead">
	<td>&nbsp;</td>
	<td >{$lng.lbl_promotion_code}</td>
	<td>{$lng.lbl_pct}(%)</td>
	<td >{$lng.lbl_from}</td>
	<td >{$lng.lbl_to}</td>
	<td >{$lng.lbl_product_sold}</td>
	<td >{$lng.lbl_sales}(Rs)</td>
	<td >{$lng.lbl_discount}</td>
</tr>


{if $promotions}

{section name=num loop=$promotions}

<tr{cycle values=', class="TableSubHead"'}>
	<td><a href="categories.php?cat={$promotions[num].id}">{$promotions[num].id}</a></td>
        <td>{$promotions[num].pct}</td>
	<td>{$promotions[num].from}</td>
	<td  >{$promotions[num].to}</td>
	<td >{$promotions[num].sold}</td>
	<td >{$promotions[num].sales}</td>
	<td >{$promotions[num].discount}</td>
	</td>
</tr>
{/section}

{else}

<tr>
	<td colspan="4" align="center">{$lng.txt_no_result}</td></tr>

{/if}


</table>


</form>

<br />

{/capture}
{include file="dialog.tpl" title=$lng.lbl_promotion_report_heading content=$smarty.capture.dialog extra='width="100%"'}
{/if}