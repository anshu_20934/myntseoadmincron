{* $Id: featured_products.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
{include file="main/include_js.tpl" src="main/popup_product.js"}
{literal}
<style>
	.lblleft {float:left; width:100px;}
    .show { display:block;}
    .hide { display:none;}
    .sub-section {font-size:16px; font-weight:bold; margin-top:10px;}
    .overlay{position:fixed; top:0; left:0; height:100%; width:100%;background: #000000; opacity:0.5; filter:alpha(opacity=50);z-index:1000;}
    .edit-mapping-div{background: none repeat scroll 0 0 #FFFFFF;
    height: 200px;
    left: 600px;
    padding: 10px;
    position: fixed;
    top: 100px;
    width: 300px;
    z-index: 1001;
    }
    .error-input{border:1px solid #ff0000;}
    .error-msgs,.edit-error-msgs{color:#ff0000;}
    .edit-mapping-div label{float:left; width:100px; display:block;}
    .edit-mapping-div div{margin-bottom:10px;}
    .notification-message{color:#41A317;}
</style>
<script type="text/javascript">

$(document).ready(function(){
	//Validation before submitting the edit mapping form
	$(".submit-btn").click(function(){
		$(".mapping-search-form").submit();
	});
});

</script>
{/literal}
{capture name=dialog}
<div class="notification-message">{$message}</div>
<form  actionName="{$form_action}" method="post" name="mapping-search-form" class="mapping-search-form">
<input type="hidden" name="mode" value="search" >
<table>
<tr>
<tr class="TableHead">
	<td >Search Display Category</td>
</tr>
<tr>
	<td  class="error-msgs"></td>
</tr>
<tr>
	<td >
		<label>Name: *</label>
		<input type="text" name="display_category_search_name" class="display_category_search_name" value="">
	</td>
	<td>
		<input type="button" name="submit-btn" class="submit-btn" value="Search Display Category">
	</td>
</tr>
</form>
<table cellpadding="3" cellspacing="1" width="100%" >
	<tr class="TableHead">
		<td>Name</td>
		<td>Active</td>
		<td>Position</td>
		<td colspan="3">Actions</td>
	</tr>
	{if $display_categories_search_results}
		{foreach item=search_result from=$display_categories_search_results}
			<tr class="TableSubHead">
				<td>{$search_result.name}</td>
				<td>{if $search_result.is_active eq 1}yes{else}no{/if}</td>
				<td>{$search_result.filter_order}</td>
				<td>
				<form method="post">
					<input type="hidden" name="mode" value="run" >
					<input type="hidden" name="id" value="{$search_result.id}" >
					<button type="submit">Run</button>
				</form>
				</td>
				<td>
				<form action="add_display_category.php">
					<input type="hidden" name="mode" value="edit" >
					<input type="hidden" name="id" value="{$search_result.id}" >
					<button type="submit">Edit</button>
				</form>
				</td>
				<td>
				<form method="post" onsubmit="return confirm('It will delete this Display Rule. Do You want to delete it?')">
					<input type="hidden" name="mode" value="delete" >
					<input type="hidden" name="id" value="{$search_result.id}" >
					<button type="submit">Delete</button>
				</form>
				</td>
			</tr>
		{/foreach}
	{/if}
</table>	
<form  action="add_display_category.php">
<input type="hidden" name="mode" value="add">
<input type="submit" value="Add New Display Category" />
</form>
{/capture}	
{include file="dialog.tpl" title="Display Categories" content=$smarty.capture.dialog extra='width="100%"'}
<br /><br />