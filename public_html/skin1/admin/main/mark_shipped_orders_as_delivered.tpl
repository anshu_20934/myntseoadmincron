{* $Id: featured_products.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
{include file="main/include_js.tpl" src="main/popup_product.js"}

<link href="{$http_location}/skin1/myntra_css/jquery-ui-datepicker.css" rel="stylesheet" type="text/css">
{literal}
<style type="text/css">
/* This is used for date picker for start and end dates of a discount rule. */
.ui-timepicker-div .ui-widget-header{ margin-bottom: 8px; }
.ui-timepicker-div dl{ text-align: left; }
.ui-timepicker-div dl dt{ height: 25px; }
.ui-timepicker-div dl dd{ margin: -25px 10px 10px 65px; }
.ui-timepicker-div td { font-size: 90%; }

ul{border:0; margin:0; padding:0;}

#pagination-flickr li{
	border:0; margin:0; padding:0;
	font-size:11px;
	list-style:none;
}
#pagination-flickr a{
	border:solid 1px #DDDDDD;
	margin-right:2px;
}
#pagination-flickr .previous-off,
#pagination-flickr .next-off {
	color:#666666;
	display:block;
	float:left;
	font-weight:bold;
	padding:3px 4px;
}
#pagination-flickr .next a,
#pagination-flickr .previous a {
	font-weight:bold;
	border:solid 1px #FFFFFF;
}
#pagination-flickr .active{
	color:#ff0084;
	font-weight:bold;
	display:block;
	float:left;
	padding:4px 6px;
}
#pagination-flickr a:link,
#pagination-flickr a:visited {
	color:#0063e3;
	display:block;
	float:left;
	padding:3px 6px;
	text-decoration:none;
}
#pagination-flickr a:hover {
	border:solid 1px #666666;
}

</style>
{/literal}

{capture name=dialog}
<div> 
	This page displays those orders which are marked Shipped (SH Order status). The only status changes allowed on this page are from SH to DL (Delivered State)
</div>
{/capture}
{include file="dialog.tpl" title="A note about this page" content=$smarty.capture.dialog extra='width="100%"'}

<br/><br/>
<form action="mark_shipped_orders_as_delivered.php" method="get" id="markdeliveredform">

{capture name=dialog}
<input type="hidden" name="mode" value="search" id="mode">
<input type="hidden" name="sortorder" value="" id="sortorder">
<input type="hidden" name="sortvendor" value="" id="sortvendor">
<input type="hidden" name="sortshippeddate" value="" id="sortshippeddate">

<div>
	Start Date : <input type="text" name="startDate" id="startDate" value="{$startDate}">
	End Date : <input type="text" name="endDate" id="endDate" value="{$endDate}">
	
	Payment Method
	<select name="paymentmethod">
		<option value=""></option>
		{foreach from=$uniquepaymentmethods item=singleresult}
		<option value="{$singleresult.payment_method}" {if $singleresult.payment_method eq $paymentmethod} selected {/if}>{$singleresult.payment_method}</option>
		{/foreach}
	</select>
	
	Shipping Method
	<select name="shippingmethod">
		<option value=""></option>
		{foreach from=$uniqueshippingmethods item=singleresult}
		<option value="{$singleresult.courier_service}" {if $singleresult.courier_service eq $shippingmethod} selected {/if}>{$singleresult.courier_service}</option>
		{/foreach}
	</select>
	
	<input type="button" value="Search Shipped orders" id="searchorders">
</div>
{/capture}
{include file="dialog.tpl" title="Data Range to find Shipped orders" content=$smarty.capture.dialog extra='width="100%"'}

<br/><br/>
{capture name=dialog}
<table cellpadding="3" cellspacing="1" width="100%">
	<thead>
		<tr>
			<td colspan=4>
			<ul id="pagination-flickr">
				{assign var="previouspagenum" value=$pagenum-1}
				{if $pagenum>1}
					<li class="previous"><a href="?page=1{$searchFilters}" title="Orders: {$orderList.1}">� First</a></li>
					<li class="previous"><a href="?page={$pagenum-1}{$searchFilters}" title="Orders: {$orderList.$previouspagenum}">� Previous</a></li>
				{elseif $totalPages>1}
					<li class="previous-off">� First</li>
					<li class="previous-off">� Previous</li>
				{/if}
				{section name=foo loop=$totalPages}
					{if $smarty.section.foo.iteration eq $pagenum}
						<li class="active">{$smarty.section.foo.iteration}</li>
					{else}
    					<li><a href="?page={$smarty.section.foo.iteration}{$searchFilters}" 
    						title="Orders: {$orderList[$smarty.section.foo.iteration]}">{$smarty.section.foo.iteration}</a></li>
    				{/if}
				{/section}
				
				{assign var="nextpagenum" value=$pagenum+1}
				{if $pagenum<$totalPages}
					<li class="next"><a href="?page={$pagenum+1}{$searchFilters}" title="Orders: {$orderList.$nextpagenum}">Next �</a></li>
					<li class="next"><a href="?page={$totalPages}{$searchFilters}" title="Orders: {$orderList.$totalPages}">Last �</a></li>
				{elseif $totalPages>1}
					<li class="next-off">Next �</li>
					<li class="next-off">Last �</li>
				{/if}
			</ul>
			</td>
		</tr>
	</thead>
	
	<tfoot>
		<tr>
			<td colspan=4>
			<ul id="pagination-flickr">
				{assign var="previouspagenum" value=$pagenum-1}
				{if $pagenum>1}
					<li class="previous"><a href="?page=1{$searchFilters}" title="Orders: {$orderList.1}">� First</a></li>
					<li class="previous"><a href="?page={$pagenum-1}{$searchFilters}" title="Orders: {$orderList.$previouspagenum}">� Previous</a></li>
				{elseif $totalPages>1}
					<li class="previous-off">� First</li>
					<li class="previous-off">� Previous</li>
				{/if}
				{section name=foo loop=$totalPages}
					{if $smarty.section.foo.iteration eq $pagenum}
						<li class="active">{$smarty.section.foo.iteration}</li>
					{else}
    					<li><a href="?page={$smarty.section.foo.iteration}{$searchFilters}" 
    						title="Orders: {$orderList[$smarty.section.foo.iteration]}">{$smarty.section.foo.iteration}</a></li>
    				{/if}
				{/section}
				
				{assign var="nextpagenum" value=$pagenum+1}
				{if $pagenum<$totalPages}
					<li class="next"><a href="?page={$pagenum+1}{$searchFilters}" title="Orders: {$orderList.$nextpagenum}">Next �</a></li>
					<li class="next"><a href="?page={$totalPages}{$searchFilters}" title="Orders: {$orderList.$totalPages}">Last �</a></li>
				{elseif $totalPages>1}
					<li class="next-off">Next �</li>
					<li class="next-off">Last �</li>
				{/if}
			</ul>
			</td>
		</tr>
	</tfoot>
	
	<tr class="TableHead">
		<td width="10%"><a href="javascript:void(0)"><img src="img/arrow-up.png" id="orderasc"></a> Order id <a href="javascript:void(0)"><img src="img/arrow-down.png" id="orderdesc"></a></td>
		<td width="12%">Order Total</td>
		<td width="10%" colspan=2>User Details</td>
		<td width="5%">Payment</td>
		<td width="10%"><a href="javascript:void(0)"><img src="img/arrow-up.png" id="vendorasc"></a> Shipped by <a href="javascript:void(0)"><img src="img/arrow-down.png" id="vendordesc"></a></td>
		<td width="10%"><a href="javascript:void(0)"><img src="img/arrow-up.png" id="shippeddateasc"></a> Shipped on <a href="javascript:void(0)"><img src="img/arrow-down.png" id="shippeddatedesc"></a></td>
		<td width="10%">Mark Delivered ?</td>
	</tr>
	
	{assign var="totalColumns" value=8}
	
	<tr>
		<td align="center">{if $sortordermode eq 'asc'}<img src="img/arrow-up.png">{elseif $sortordermode eq 'desc'}<img src="img/arrow-down.png"{/if}</td>
		<td></td>
		<td colspan=2></td>
		<td></td>
		<td align="center">{if $sortvendormode eq 'asc'}<img src="img/arrow-up.png">{elseif $sortvendormode eq 'desc'}<img src="img/arrow-down.png"{/if}</td>
		<td align="center">{if $sortshippedmode eq 'asc'}<img src="img/arrow-up.png">{elseif $sortshippedmode eq 'desc'}<img src="img/arrow-down.png"{/if}</td>
		<td align="center"><input type="checkbox" class="selectAll"><br/>Select All</td>
	</tr>
	
	<tr><td colspan={$totalColumns}><hr/></td></tr>
	
	{foreach from=$shippedorders item=singleresult}
	<tr align="center">
		<td>
			<a href="order.php?orderid={$singleresult.orderid}" target="_blank">{$singleresult.orderid}</a><br/><br/>
			Placed on<br/> {$singleresult.placeddate}<br/>
		</td>
		
		<td align="left">
			{assign var="customerPays" value=$singleresult.total+$singleresult.cod}
			<b>Customer Pays : {$customerPays} {if $singleresult.cod>0} </b><br/>(includes COD charge {$singleresult.cod}) {/if}
			
			{if $singleresult.eossdiscount>0 || $singleresult.coupon_discount>0 || $singleresult.cash_redeemed>0}
			<br/><br/> Actual Amount : {$singleresult.actualamount}<br/><br/> 
				<i>Discounts Applicable :</i><br/>
				{if $singleresult.eossdiscount>0}
				Auto-Discount :  {$singleresult.eossdiscount}<br/>
				{/if}
				{if $singleresult.coupon_discount>0}
				Coupon:  {$singleresult.coupon_discount} ({$singleresult.coupon})<br/>
				{/if}
				{if $singleresult.cash_redeemed>0}
				CashBack:  {$singleresult.cash_redeemed} ({$singleresult.cash_coupon_code})<br/>
				{/if}
				{if $singleresult.totalcoupondiscount>0}
				<b>Total Discount Redeemed : {$singleresult.totalcoupondiscount}</b>
				{/if}
			{/if}
		</td>
		
		<td align="left" valign="top">
			Login : <br/>
			Name  : <br/>
			Call  : <br/>
			Address: 
		</td>
		<td align="left" valign="top">
			{$singleresult.login}<br/>
			{$singleresult.firstname}<br/>
			{$singleresult.mobile}<br/>
			{$singleresult.s_address}<br/>
			{$singleresult.s_city} - {$singleresult.s_state} - {$singleresult.s_country}
			{if $singleresult.s_zipcode} - {$singleresult.s_zipcode} {/if}
		</td>
		<td> {$singleresult.payment_method} </td>
		<td> {$singleresult.courier_service} <br/> {$singleresult.tracking}</td>
		<td> {$singleresult.shippeddate}</td>
		<td><input type="checkbox" name="orderids[]" value="{$singleresult.orderid}" class="selectedOrder"></td>
	</tr>
	<tr><td colspan={$totalColumns}><hr/></td></tr>
	{/foreach}
	
	<tr>
		<td colspan={$totalColumns-1}</td>
		<td align="center"><input type="checkbox" class="selectAll"><br/>Select All</td>
	</tr>
	
	<tr>
		<td colspan={$totalColumns} align="right"> 
			<hr/><input type="button" id="markdelivered" value="Mark these Orders as Delivered">
		</td> 
	</tr>

</table>
</form>

{/capture}
{include file="dialog.tpl" title="Search Results" content=$smarty.capture.dialog extra='width="100%"'}

<script type="text/javascript" src="{$http_location}/skin1/myntra_js/jquery-ui-datepicker.js"></script>

{literal}
<script>
$(document).ready(function(){
	$('#startDate').datetimepicker();
	$('#endDate').datetimepicker();
	
	$(".selectAll").click(function() {
		var checked_status = this.checked;
		$(".selectedOrder").each(function() {
			this.checked = checked_status;
		});
	});

	$("#markdelivered").click(function() {
		$("#mode").val("markdelivered");
		$("#markdeliveredform").submit();
	});

	$("#searchorders").click(function() {
		$("#mode").val("search");
		$("#markdeliveredform").submit();
	});

	$("#orderasc").click(function() {
		$("#sortorder").val("asc");
		$("#markdeliveredform").submit();
	});

	$("#orderdesc").click(function() {
		$("#sortorder").val("desc");
		$("#markdeliveredform").submit();
	});

	$("#vendorasc").click(function() {
		$("#sortvendor").val("asc");
		$("#markdeliveredform").submit();
	});

	$("#vendordesc").click(function() {
		$("#sortvendor").val("desc");
		$("#markdeliveredform").submit();
	});

	$("#shippeddateasc").click(function() {
		$("#sortshippeddate").val("asc");
		$("#markdeliveredform").submit();
	});

	$("#shippeddatedesc").click(function() {
		$("#sortshippeddate").val("desc");
		$("#markdeliveredform").submit();
	});
});

</script>
{/literal}