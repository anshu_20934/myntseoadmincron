<script type="text/javascript" src="{$http_location}/skin1/js_script/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="{$http_location}/skin1/js_script/calender.js"></script>
{literal}
<script type="text/javascript">

	$(document).ready(function(){
		enableDisableFormControls();
	
		$('#shipping_type').change(function(){
			if ($(this).val() == 'MS') {
				$('#pickup_dt').removeAttr('disabled');
				$('#pickup_time').removeAttr('disabled');	
				$('#pickup_dt_pref_row').show();		
			} else {
				$('#pickup_dt').attr('disabled', 'disabled');
				$('#pickup_time').attr('disabled', 'disabled');	
				$('#pickup_dt_pref_row').hide();		
			}	
		});

		$('#rt_action').change(function(){
			var action = $(this).val();
			if (action == 'RTQP') {
				$("#qapass-quality").attr("disabled", false);
			} else {
				$("#qapass-quality").attr("disabled", true);
			}
			
			if (action == 'RTRF') {
				$('#coupon_row').show();
				$('#coupon_code').removeAttr('disabled');
				$('#coupon_value').removeAttr('disabled');				
				$('#items_rej_val').attr('disabled', 'disabled');
				$('#items_rstk_val').attr('disabled', 'disabled');
				$('#restock_row').hide();		
			} else if (action == 'RTAI') {
				$('#coupon_row').hide();
				$('#coupon_code').attr('disabled', 'disabled');
				$('#coupon_value').attr('disabled', 'disabled');				
				$('#items_rej_val').removeAttr('disabled');
				$('#items_rstk_val').removeAttr('disabled');
				$('#restock_row').show();		
			} else {
				$('#coupon_row').hide();
				$('#coupon_code').attr('disabled', 'disabled');
				$('#coupon_value').attr('disabled', 'disabled');				
				$('#items_rej_val').attr('disabled', 'disabled');
				$('#items_rstk_val').attr('disabled', 'disabled');
				$('#restock_row').hide();		
			}
		});	
	});

	function enableDisableFormControls(){
		var currst = $('#currst').val();

		$('#coupon_row').hide();
		$('#restock_row').hide();
	
		if (currst == '') {
			$("#info_tbl").hide();
			$('#pickup_dt_pref_row').hide();
			$('#courier_row').hide();
			$('#update_btn').hide();        
                	$('#add_btn').show();	
		} else {
			$('#update_btn').show();        
                        $('#add_btn').hide();
	
			$("#info_tbl").show();
			$("#reason_row").remove();
			$("#address_row1").remove();
			$("#address_row2").remove();
			$("#address_row3").remove();
			$("#address_row4").remove();
			$("#pickuptype_row").remove();
			$(".blank_row").remove();
			$('#pickup_dt_pref_row').remove();
			if (currst == 'RTQ') {
				var st = $('#st').val();
				if (st == 'SS') {
					$('#courier').replaceWith("<input type='text' id='courier' name='courier'>");
					$('#tracking_num').removeAttr('disabled');
				} else if (st == 'MS') {
					$('#courier').removeAttr('disabled');
					$('#tracking_num').removeAttr('disabled');
					$('#label_trackingnum').html('Reference # :');								
				}				
			} else if (currst == 'RTPI') {
				$('#label_courier').html('&nbsp;');		
				$('#courier').replaceWith('<td>&nbsp;</td>');
				$('#tracking_num').removeAttr('disabled');				
			} else if (currst == 'RTRS') {
				$('#courier').removeAttr('disabled');
                                $('#tracking_num').removeAttr('disabled');		
			} else {
				$('#courier_row').remove();	
				if (currst == 'END') {
					$('#comment_row').remove();
					$('#update_btn').hide();	
					$('#activity_row').hide();	
				}	
			}
		}	

		return;
	}

	function updateRTOOrder(){		
		var prevst = $('#currst').val();
                var nextst = $('#rt_action').val();
                var comment = $('#comment').val();
		
		if (isEmptyString(nextst) || nextst == '-') {
			alert("Please select a Return Action");
			return false;	
		}
                if (isEmptyString(comment)) {
                                alert('Please add a comment');
                                return false;
                }
                $('#action').val('update');
	
		if (nextst == 'RTCSD') {
			if (isEmptyString($('#courier').val()) ) {
                                alert("Please provide courier operator details");
                                return false;
                        }	
			if (isEmptyString($('#tracking_num').val())) {
                                alert("Please enter a valid tracking number");
                                return false;
                        }
		} else if ((nextst == 'RTPI') || (nextst == 'RTRSC')) {
			if ($('#courier').val() == '-') {
				alert("Please select a courier operator");
				return false;
			}
			if (isEmptyString($('#tracking_num').val())) {
				alert("Please enter a valid tracking/referene number");
				return false;
			}
		} else if (nextst == 'RTPTU') {
			if (isEmptyString($('#tracking_num').val())) {
                                alert("Please enter a valid tracking number");
                                return false;
                        }
		} else if (nextst == 'RTAI') {
			if(!isAmount($('#items_rej_val').val())) {
				alert("Please enter a valid value for items rejected.");	
				return false;
			}		
			if (!isAmount($('#items_rstk_val').val())) {
				alert("Please enter a valid value for items restocked.");
				return false;
			}
		} else if (nextst == 'RTRF') {
			if (!isAmount($('#coupon_value').val())) {
				alert('Please enter a valid amount for refund');
				return false;
			}
			
			if (isEmptyString($('#coupon_code').val())) {
				alert('Please enter a valid coupon code');
				return false;
			}		
		}
				
		return true;
	}

	function createRTOOrder(){
		var returnreason = $('#return_reason').val();
		if (returnreason == '-') {
			alert("Please choose a reason for return");
			return false;
		}
		var returnaction = $('#rt_action').val();
		if (returnaction == '-') {
			alert('Please choose a return action');
			return false;
		}
		if (returnaction != 'RTQ') {
			alert('Return Action must be "Return Queued" for adding a Return Order');
                        return false;
		}
                var custname    = $('#cust_name').val();
                if (isEmptyString(custname)) {
                        alert("Customer Name Should not be empty");
                        return false;
                }
		var custaddr    = $('#cust_addr').val();
                if (isEmptyString(custaddr)) {
                        alert("Address should not be empty");
                        return false;   
                }			
		var custcity    = $('#cust_city').val();
                if (isEmptyString(custcity)) {
                        alert("City should not be empty");
                        return false;   
                }			
		var custstate    = $('#cust_state').val();
                if (isEmptyString(custstate)) {
                        alert("State should not be empty");
                        return false;   
                }			
		var custcountry    = $('#cust_country').val();
                if (isEmptyString(custcountry)) {
                        alert("Country should not be empty");
                        return false;   
                }			
		var custzipcode    = $('#cust_zipcode').val();
                if (isEmptyString(custzipcode)) {
                        alert("ZIP Code should not be empty");
                        return false;   
                }			
		var shipmenttype = $('#shipping_type').val();	
		if (shipmenttype == '-') {
			alert("Please choose an option for shipment type");
			return false;
		}
		if (shipmenttype == 'MS') {
			var pickupdt = $('#pickup_dt').val();	
			if(isEmptyString(pickupdt)) {
				alert("Pickup Date Should not be empty");
				return false;
			}		
			var pickuptm = $('#pickup_time').val();
			if(pickuptm == '-') {
				alert("Pickup Time should not be empty");
				return false;
			}
		}
		var comment = $('#comment').val();
		if (isEmptyString(comment)) {
			alert("Please add more details about the return in the comment field");
			return false;
		}		
		$('#action').val('add');	
		return true;
	}

	function isEmptyString(s){
		return (s.replace(/\s/g,'') == '');	
	}

	function isAmount(amount) {
		var regex = /(^\+?\d+.\d+$)|(^\+?\d+$)/;	
		return regex.test(amount);
	}
		
</script>
{/literal}
<h1 style="text-align:center;">Return Order Details</h1>
{if $errormsg }<h2 style="color:red; text-align:center">{$errormsg}</h2>{/if}
<h3>Order # <a href="{$http_location}/admin/order.php?orderid={$orderData.orderid}">{$orderData.orderid}</a></h3>

<form name="statusForm" action="" method="post">
	<input type="hidden" id="currst" name="currst" value="{$orderData.currstate}">
	<input type="hidden" id="action" name="action">
	<input type="hidden" id="st" name="st" value="{$orderData.shipmenttype}">
	<input type="hidden" id="iscod" name="iscod" value="{$orderData.iscod}">

<table cellspacing="0" cellpadding="0" width="100%" id="info_tbl">
	<tr>
		<td align="right"><label>Order Id : </label></td>
		<td align="left"><input type="text" readonly="readonly"  id="order_id" name="order_id" value="{$orderData.orderid}"></td>
		<td align="right"><label>Return Reason : </label></td>
		<td align="left"><input type="text" disabled="disabled" value="{$returnreason[$orderData.returnreasoncode]}"></td>
	</tr>
	<tr><td colspan="4">&nbsp;</td></tr>
	<tr>
		<td align="right"><label>Customer Name : </label></td>
                <td align="left"><input type="text" disabled="disabled" value="{$orderData.custname}"></td>
                <td align="right"><label>Customer Address : </label></td>
                <td align="left"><textarea cols="40" rows="2" disabled="disabled">{$orderData.pickupaddress}</textarea></td>	
	</tr>
	<tr>
                <td align="right"><label>City : </label></td>
                <td align="left"><input type="text" disabled="disabled" value="{$orderData.pickupcity}"></td>
                <td align="right"><label>State : </label></td>
                <td align="left"><input type="text" disabled="disabled"  value="{$orderData.pickupstate}"></td>
	</tr>
	<tr>
                <td align="right"><label>Country : </label></td>
                <td align="left"><input type="text" disabled="disabled" value="{$orderData.pickupcountry}"></td>
                <td align="right"><label>ZIP Code : </label></td>
                <td align="left"><input type="text" disabled="disabled" value="{$orderData.pickupzipcode}"></td>
        </tr>

        <tr>
                <td align="right"><label>Contact # : </label></td>
                <td align="left"><input type="text" disabled="disabled" value="{$orderData.phonenumber}"></td>
                <td colspan="2">&nbsp;</td>
        </tr>
	<tr><td colspan="4">&nbsp;</td></tr>

        <tr>
                <td align="right"><label>Pickup Type : </label></td>
                <td align="left"><input type="text" disabled="disabled" 
				{if $orderData.shipmenttype}
					{if $orderData.shipmenttype eq 'MS'}
						value="Pickup by Myntra"
					{else}
						value="Shipping by Customer"
					{/if}
				{/if} >
                </td>
                <td colspan="2">&nbsp;</td>
        </tr>
	
	<tr>
		<td align="right"><label>Pickup Courier : </label></td>
		<td align="left"><input type="text" disabled="disabled" value="{$orderData.courier}"></td>
		<td align="right"><label>Tracking # : </label></td>		
		<td align="left"><input type="text" disabled="disabled" value="{$orderData.trackingno}"></td>
	</tr>
	
	<tr>
		<td align="right"><label>Reshipping Courier : </label></td>
		<td align="left"><input type="text" disabled="disabled" value="{$orderData.revcourier}"></td>
		<td align="right"><label>Tracking # : </label></td>		
		<td align="left"><input type="text" disabled="disabled" value="{$orderData.revtrackingno}"></td>
	</tr>
	
</table>
<br />
<br />

<table cellspacing="0" cellpadding="0" width="100%">
	<tr id="reason_row">
		<td align="right"><label>Order Id : </label></td>
		<td align="left">
			<input type="text" id="order_id" name="order_id" value="{$orderData.orderid}">
		</td>
		<td align="right"><label>Return Reason : </label></td>
		<td align="left">
			<select name="return_reason" id="return_reason">
				<option value="-">--- Select a return reason ---</option>
			{foreach from=$returnreason key=k item=v}
				{if $k eq $orderData.returnreasoncode }
					<option value="{$k}" selected="selected">{$v}</option>
				{else}
					<option value="{$k}">{$v}</option>
				{/if}
			{/foreach}
                        </select>
		</td>
	</tr>
	
	<tr class="blank_row"><td colspan="4">&nbsp;</td></tr>
	<tr id="activity_row">
    	<td align="right"><label>Current State : </label></td>
        <td align="left"><input id="curr_st" type="text" disabled="disabled" value="{$allstates[$orderData.currstate]}"></td>
        <td align="right">
        	<label>Return Action :</label>
        </td>
        <td align="left">
        	<select id="rt_action" name="rt_action">
            	<option value="-">--- Select a return action ---</option>
                {foreach from=$rtactions key=k item=v}
                <option value="{$k}">{$v}</option>
                {/foreach}
            </select>
        </td>
    </tr>
    <tr class="blank_row"><td colspan="4">&nbsp;</td></tr>
    <tr class="blank_row"><td colspan="4">&nbsp;</td></tr>
    <tr>
    	<td align="right">&nbsp;</td>
    	<td align="left">&nbsp;</td>
    	<td align="right"><label>Returned Item(s) Barcode : </label></td>                    
    	<td align="left">
    		<textarea name="itembarcodes" id="itembarcodes" {if $orderData.currstate ne 'RTCSD' && $orderData.currstate ne 'RTPTU'} readOnly {/if}>{$orderData.itembarcodes}</textarea>
    	</td> 
    </tr>

	<tr class="blank_row"><td colspan="4">&nbsp;</td></tr>
	<tr class="blank_row"><td colspan="4">&nbsp;</td></tr>
	
	{if $orderData.itembarcodes ne '' && $orderData.currstate eq 'RTR' }
		<tr id="qapass-quality-row">
	    	<td align="right">&nbsp;</td>
	    	<td align="left">&nbsp;</td>
	    	<td align="right"><label>Returned Item(s) Quality : </label></td>                    
	    	<td align="left">
	    		<select name="qapass-quality" id="qapass-quality" style="padding: 0px 2px; width: 50px; height: 20px; border: 1px solid #777777;" disabled>
					<option value="Q1">Q1</option>
					<option value="Q2">Q2</option>
					<option value="Q3">Q3</option>
				</select>
	    	</td> 
	    </tr>
    {/if}
    <tr class="blank_row"><td colspan="4">&nbsp;</td></tr>
	<tr class="blank_row"><td colspan="4">&nbsp;</td></tr>
	
	<tr id="address_row1">
		<td align="right"><label>Customer Name : </label></td>
                <td align="left">
                	<input type="text" name="cust_name" id="cust_name" value="{$orderData.custname}">
                </td>
		<td align="right"><label>Customer Address : </label></td>
                <td align="left">
                        <textarea id="cust_addr" name="cust_addr" cols="40" rows="2">{$orderData.pickupaddress}</textarea>
                </td>
	</tr>
	
	<tr id="address_row2">
		<td align="right"><label>City : </label></td>
		<td align="left"><input type="text" id="cust_city" name="cust_city" value="{$orderData.pickupcity}"></td>
		<td align="right"><label>State : </label></td>
                <td align="left"><input type="text" id="cust_state" name="cust_state" value="{$orderData.pickupstate}"></td>
	</tr>
	
	<tr id="address_row3">
		<td align="right"><label>Country : </label></td> 
		<td align="left"><input type="text" id="cust_country" name="cust_country" value="{$orderData.pickupcountry}"></td>
		<td align="right"><label>ZIP Code : </label></td>
		<td align="left"><input type="text" id="cust_zipcode" name="cust_zipcode" value="{$orderData.pickupzipcode}"></td>
	</tr>
	
	<tr id="address_row4">
		<td align="right"><label>Contact # : </label></td>
		<td align="left"><input type="text" readonly="readonly" name="contact_no" id="contact_no" value="{$orderData.phonenumber}"></td>
		<td colspan="2">&nbsp;</td>
	</tr>
		
	<tr class="blank_row"><td colspan="4">&nbsp;</td></tr>
	<tr class="blank_row"><td colspan="4">&nbsp;</td></tr>

	<tr id="pickuptype_row">
		<td align="right"><label>Pickup Type : </label></td>
		<td align="left">
			<select name="shipping_type" id="shipping_type">
				<option value="-">--- Select shipping type ---</option>
				<option value="MS">Pickup by Myntra</option>
				<option value="SS">Shipping by Customer</option>
			</select>
		</td>
		<td colspan="2">&nbsp;</td>
	</tr>
	
	<tr id="pickup_dt_pref_row">
		<td align="right"><label>Prefered Pickup Date : </label></td>
                <td align="left"><input type="text" disabled="disabled" id="pickup_dt" name="pickup_dt"><input type=button value="select" onclick="displayDatePicker('pickup_dt');"></td>
		<td align="right"><label>Time : </label></td>
		<td>
			<select name="pickup_time" id="pickup_time" disabled="disabled">
				<option value="-">--- Select pickup time ---</option>
				<option value="2">1 P.M. - 3 P.M.</option>
				<option value="4">3 P.M. - 5 p.M.</option>
				<option value="5">5 P.M. - 7 P.M.</option>
			</select>
		</td>
	</tr>
	
	<tr id="courier_row">
		<td align="right"><label id="label_courier">Courier : </label></td>
                <td align="left">
                        <select name="courier" id="courier" disabled="disabled">
                                        <option value="-">--- Select a courier operator ---</option>
                                {foreach from=$couriers key=k item=v}
                                        <option value="{$v.code}">{$v.display_name}</option>
                                {/foreach}
                        </select>
                </td>
		<td align="right"><label id="label_trackingnum">Tracking # : </label></td>
		<td align="left"><input type="text" id="tracking_num" name="tracking_num" disabled="disabled"></td>
	</tr>	

	<tr id="coupon_row">
		<td align="right"><label>Coupon Code : </label></td>
		<td align="left"><input type="text" name="coupon_code" id="coupon_code" disabled="disabled"></td>
		<td align="right"><label>Coupon Value : </label></td>			
		<td align="left"><input type="text" name="coupon_value" id="coupon_value" disabled="disabled"></td>
	</tr>
	
	<tr id="restock_row">
		<td align="right"><label>Rejected Items Value : </label></td>
		<td align="left"><input type="text" name="items_rej_val" id="items_rej_val" disabled="disabled"></td>
		<td align="right"><label>Restocked Items Value : </label></td>
		<td align="left"><input type="text" name="items_rstk_val" id="items_rstk_val" disabled="disabled"></td>
	</tr>

	<tr><td colspan="4">&nbsp;</td></tr>
	
	<tr id="comment_row">
		<td align="right"><label>Comment : </label></td>
		<td align="left" colspan="3"><textarea wrap="ON" cols="70" rows="6" id="comment" name="comment" value="comment"></textarea></td>
	</tr>	

	<tr><td colspan="4">&nbsp;</td></tr>
	<tr>
		<td>&nbsp;</td>
		<td align="right">
			<input type="submit" id="update_btn" value="Update" onclick="javascript:return updateRTOOrder();" {if not $orderData.canupdate}disabled="disabled"{/if}>
		</td>
		<td align="left"><input type="submit" id="add_btn" value="Create" onclick="javascript:return createRTOOrder();" {if not $orderData.cancreate}disabled="disabled"{/if}></td>
		<td>&nbsp;</td>
	</tr>
</table>
</form>
<br/>

<table border="1">
	<thead><b>Edit History</b></thead>
	
	<tr>
		<th>Activity</th>
		<th>Done by</th>
		<th>Recorded on</th>
		<th>Elapsed Time</th>
		<th>SLA </th>
		<th>Remark</th>	
	</tr>

	{foreach from=$edithistory key=k item=data}
		<tr {if $data.time_since_last_activity > $data.sla_time }style="background:red;"{/if}>
			<td>{$data.activity}</td>
			<td>{$data.adminuser}</td>
			<td>{$data.recorddate}</td>
			<td style="text-align:center;">{$data.time_since_last_activity_descr}</td>
			<td style="text-align:center;">{$data.sla_time_descr}</td>
			<td>{$data.remark}</td>
		</tr>
	{/foreach}		
</table>
