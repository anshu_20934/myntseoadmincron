{include file="main/include_js.tpl" src="main/popup_product.js}
<script type="text/javascript" src="{$SkinDir}/js_script/tabber.js"></script>
<link rel="stylesheet" href="{$SkinDir}/skin1_admin.css"/>
<link rel="stylesheet" href="{$SkinDir}/css/tabber.css"/>
{literal}
<!--<script type="text/javascript" src="{$SkinDir}/js_script/ajaxObject.js"></script>-->
<script type="text/javascript">
function checkpass( str ) {
	var reg = new RegExp('["`\']');
	if(reg.test(str)){
		alert("Strings ('),(\"),(`) are not allowed");
	}
}
</script>
<script type="text/javascript">
/* Optional: Temporarily hide the "tabber" class so it does not "flash"
   on the page as plain HTML. After tabber runs, the class is changed
   to "tabberlive" and it will appear. */

	document.write('<style type="text/css">.tabber{display:none;}<\/style>');
</script>
{/literal}
{capture name=dialog}
<div class="tabber">
	{section name=options loop=$types}
	{if $types[options].active eq 1}
	<div class="tabbertab">
		<h2>{$types[options].type_name}</h2>
		<p>
			{if $errorcode}
			<table cellpadding="2" cellspacing="1" width="100%">
				<tr class="tablehead">
					<th width=100%" nowrap="nowrap">
						<font color="#ff0000">{$errorcode}</font>
					</th>
				</tr>
			</table>
			{/if}
			<table cellpadding="2" cellspacing="1" width="100%">
				<tr class="tablehead">
					<th width="10%" nowrap="nowrap">
						Id
					</th>
					<th width="10%" nowrap="nowrap">
						Code
					</th>
					<th width="10%" nowrap="nowrap">
						Source Name 
					</th>
					<th width="10%" nowrap="nowrap">
						Source type
					</th>
					<th width="10%" nowrap="nowrap">
						Location
					</th>
					<th width="10%" nowrap="nowrap">
						Source Address
					</th>
					<th width="10%" nowrap="nowrap">
						Sync Username
					</th>
					<th width="10%" nowrap="nowrap">
						Sync Password
					</th>
					<th width="10%" nowrap="nowrap">
						Manager Email
					</th>
					<th width="10%" nowrap="nowrap">
						Date
					</th>
					<th width="10%" nowrap="nowrap">
						Active	
					</th>
					<th width="10%" nowrap="nowrap">
						Remote
					</th>
					<th width="10%" nowrap="nowrap">
					</th>
				</tr>
				<form action="" method="get" name="add_source">
					<tr class="tablehead">
						<td nowrap="nowrap">
						</td>
						<td nowrap="nowrap">
							<input type="text" name="source_code" value="{$smarty.get.source_code}" size="5"/>
						</td>
						<td nowrap="nowrap">
							<input type="text" name="source_name" value="{$smarty.get.source_name}" />
						</td>
						<td nowrap="nowrap">
							<select name="type_id"/>
								<option value="">--select source type--</option>
								{section name=typeoptions loop=$types}
									{if $types[typeoptions].active ne 0}
										<option value="{$types[typeoptions].type_id}" {if $types[options].type_id eq $types[typeoptions].type_id} selected {/if}>{$types[typeoptions].type_name}</option>
									{/if}
								{/section}
							</select>
						</td>
						<td nowrap="nowrap">
							<select name="location_id"/>
								<option value="">--select location--</option>							
								{section name=locoptions loop=$locations}
									<option value="{$locations[locoptions].location_id}" >{$locations[locoptions].location_name}</option>							
								{/section}
							</select>
						</td>
						<td nowrap="nowrap">
							<TEXTAREA name="source_address">{$smarty.get.source_address}</TEXTAREA> 
						</td>
						<td nowrap="nowrap">
							<input type="text" name="source_sync_username" value="{$smarty.get.source_sync_username}" />
						</td>
						<td nowrap="nowrap">
							<div><div>Password :</div><div><input type="password" name="source_sync_password" value='' onBlur="javascript:return checkpass(this.value);"/></div>
							<div>Confirm :</div><div><input type="password" name="source_sync_password2" value='' onBlur="javascript:return checkpass(this.value);"/><div></div>
						</td>
						<td nowrap="nowrap">
							<input type="text" name="source_manager_email" value="{$smarty.get.source_manager_email}" />
						</td>
						<td nowrap="nowrap">
						</td>
						<td nowrap="nowrap">
							<input type="checkbox" name="active" checked/>
						</td>
						<td nowrap="nowrap">
						</td>
						<td nowrap="nowrap">
							<input type="submit" value = "add" name="add_source"/>
						</td>
					</tr>
				</form>
				{section name=source loop=$sources}
					{if $sources[source].source_type_id eq $types[options].type_id }
					{assign var=disable value=''}
					{if $sources[source].type_active ne 1}
						{assign var=disable value='disabled'}
					{/if}
					<form action="" method="get" name="modify_{$sources[source].id}">
						<tr class="tablehead">
							<th width="10%" nowrap="nowrap">
								{$sources[source].source_id}
								<input type=hidden name="source_id" value="{$sources[source].source_id}"/>
							</th>
							<th width="10%" nowrap="nowrap">
								{$sources[source].source_code}
							</th>
							<th width="10%" nowrap="nowrap">
								<input type="text" value="{$sources[source].source_name}" name="source_name" {$disable}>
							</th>
							<th width="10%" nowrap="nowrap">
								<select name="type_id" {$disable}/>
									<option value="">--select source type--</option>							
									{section name=typeoptions loop=$types}
										<option value="{$types[typeoptions].type_id}" {if $sources[source].source_type_id eq $types[typeoptions].type_id} selected {/if}>{$types[typeoptions].type_name}{if $types[typeoptions].active eq 0} (inactive){/if}</option>
									{/section}
								</select>
							</th>
							<th width="10%" nowrap="nowrap">
								<select name="location_id" {$disable}/>
									<option value="">--select location--</option>							
									{section name=locoptions loop=$locations}
										<option value="{$locations[locoptions].location_id}" {if $sources[source].location_id eq $locations[locoptions].location_id} selected {/if}>{$locations[locoptions].location_name}</option>									
									{/section}
								</select>
							</th>
							<th width="10%" nowrap="nowrap">
								<TEXTAREA name="source_address" {$disable}>{$sources[source].source_address}</TEXTAREA> 
							</th>
							<th width="10%" nowrap="nowrap">
								{if $sources[source].synchronizable}
									<input type="text" value="{$sources[source].source_sync_username}" name="source_sync_username" {$disable}/>
								{else}
									<font color = "#00ff00">LOCAL</font>
								{/if}
							</th>
							<th width="10%" nowrap="nowrap">
								{if $sources[source].synchronizable}
									<div><div>Password :</div><div><input type="password" name="source_sync_password" value='' onBlur="javascript:return checkpass(this.value);" {$disable}/></div>
									<div>Confirm :</div><div><input type="password" name="source_sync_password2" value='' onBlur="javascript:return checkpass(this.value);" {$disable}/><div></div>
								{else}
									<font color = "#00ff00">LOCAL</font>	
								{/if}
							</th>
							<th width="10%" nowrap="nowrap">
								<input type="text" value="{$sources[source].source_manager_email}" name="source_manager_email" {$disable}/>
							</th>
							<th width="10%" nowrap="nowrap">
								{$sources[source].source_start_date}
							</th>
							<th width="10%" nowrap="nowrap">
								{if $sources[source].synchronizable}
									<input type="checkbox" name="status" {if $sources[source].active}checked{/if} {$disable}/>
								{else}
									<font color = "#00ff00">LOCAL</font>	
								{/if}
							</th>
							<th width="10%" nowrap="nowrap">
								{if $sources[source].synchronizable}remote{else}<font color = "#00ff00">LOCAL</font>{/if}
							</th>
							<td nowrap="nowrap">
								<input type="submit" value = "modify" name="modify_source" {$disable}/>
							</td>
						</tr>					
					</form>
					{/if}
				{/section}
			</table>
		</p>
	</div>
	{/if}
	{/section}
</div>
{/capture}
{include file="dialog.tpl" title="Sources List" content=$smarty.capture.dialog extra='width="100%"'}<br/><br/>

<!--managing source types-->
{capture name=dialog}
<table cellpadding="3" cellspacing="1" width="40%">
<tr>
	<td colspan="3" class="SubmitBox"><span style="color:red;">{$admin_info}</span>
	</td>
</tr>
<tr class="TableHead">
	<td width="30%">source type name</td>
	<td>Active</td> 
	<td colspan="1">&nbsp;</td> 
</tr>

{section name=num loop=$types}
	<form action="manage_order_sources.php" method="post" name="modify_{$types[num].type_id}" >
	<input type="hidden"  name="type_id"  value="{$types[num].type_id}"/>
	<input type="hidden"  name="mode"/>
	<tr {cycle values=", class='TableSubHead'"}>
	    <td  align="center"><input type="text"  name="type_name"  value="{$types[num].type_name}" size="30"/></td>
		<td align="center">			
			<input type="checkbox" name="active" {if $types[num].active eq 1}checked{/if}/>
		</td>
		<td  align="center"><input type="submit"  name="modify_type"  value="Modify"/></td>
		<!--<td  align="center"><input type="button"  name="delete_type"  value="Delete" onclick='document.modify_{$types[num].type_id}.mode.value="delete_type";(confirm("Do you really want to delete \"{$types[num].type_name}\"?")) ? document.modify_{$types[num].type_id}.submit() : false;'/></td>	-->
	</tr>
	</form>
{sectionelse}
	<tr>
		<td colspan="3" align="center">No source types available!Add new.</td>
	</tr>
{/section}
	<form action="manage_order_sources.php" method="post" name="source_type_add_form" enctype="multipart/form-data">
	<tr {cycle values=", class='TableSubHead'"}>
	    <td  align="center"><input type="text" name="type_name"size="30"/></td>
		<td align="center">			
			<input type="checkbox" name="active"/>
		</td>
		<td align="center"><input type="submit"  name="add_type"  value="Add"/></td>		
		<td align="center">&nbsp;</td>		
	</tr>
	</form>
</table>
{/capture}
{include file="dialog.tpl" title="Source Types" content=$smarty.capture.dialog extra='width="100%"'}
<!--managing source types-->

<!--managing locations-->
{capture name=dialog}
<table cellpadding="3" cellspacing="1" width="60%">
<tr>
	<td colspan="3" class="SubmitBox"><span style="color:red;">{$location_info}</span>
	</td>
</tr>
<tr class="TableHead">
	<td>Location</td>
	<td>Country</td> 
	<td>State</td>
	<td>City</td>
	<td colspan="1">&nbsp;</td> 
</tr>

{section name=num loop=$locations}
	<form action="manage_order_sources.php" method="post" name="modify_{$locations[num].location_id}">
	<input type="hidden"  name="location_id"  value="{$locations[num].location_id}"/>
	<input type="hidden"  name="mode"/>
	<tr {cycle values=", class='TableSubHead'"}>
	    <td  align="center"><input type="text"  name="location_name"  value="{$locations[num].location_name}" size="30"/></td>
	    <td  align="center">
		    <select name="country_code" ><!--onchange="loadstates(this,'{$locations[num].state}','{$locations[num].location_id}');"-->
		    	<option value="">--select country--</option>
				{section name=options loop=$country}
					<option value="{$country[options].code}" {if $locations[num].country eq $country[options].code} selected {/if}>{$country[options].name}</option>				{/section}
		    </select>
	    </td>
	    <td  align="center">
			<span id="statespan_{$locations[num].location_id}">
				<select name="state_code" id="state_code">
					<option value="">---select state---</option>
					{section name=options loop=$states}
						<option value="{$states[options].code}" {if $locations[num].state eq $states[options].code} selected {/if}>{$states[options].state}</option>					{/section}
				</select>
			</span>
		</td>
	    <td  align="center"><input type="text"  name="city"  value="{$locations[num].city}" size="30"/></td>
		<td  align="center"><input type="submit"  name="modify_location"  value="Modify"/></td>
		<!--<td  align="center"><input type="button"  name="delete_location"  value="Delete" onclick='document.modify_{$locations[num].location_id}.mode.value="delete_location";(confirm("Do you really want to delete \"{$locations[num].location_name}\"?")) ? document.modify_{$locations[num].location_id}.submit() : false;'/></td>	-->
	</tr>
	</form>
{sectionelse}
	<tr>
		<td colspan="5" align="center" style="color:red;">No source locations available!Add new.</td>
	</tr>
{/section}
	<form action="manage_order_sources.php" method="post" name="source_location_add_form">
	<tr {cycle values=", class='TableSubHead'"}>
	    <td  align="center"><input type="text"  name="location_name" size="30"/></td>
	    <td  align="center">
		    <select name="country_code"><!-- onchange="loadstates(this,'','add');"-->
		    	<option value="">--select country--</option>
				{foreach item=options from=$country}
					<option value="{$options.code}">{$options.name}</option>					
				{/foreach}
		    </select>
	    </td>
	    <td  align="center">
			<span id="statespan">
				<select name="state_code" >
					<option value="">--select state</option>
					{section name=options loop=$states}
						<option value="{$states[options].code}">{$states[options].state}</option>
					{/section}
				</select>
			</span>
		</td>
	    <td  align="center"><input type="text"  name="city"  size="30"/></td>
		<td align="center"><input type="submit"  name="add_location"  value="Add"/></td>		
		<td align="center">&nbsp;</td>		
	</tr>
	</form>
</table>
{/capture}
{include file="dialog.tpl" title="Locations" content=$smarty.capture.dialog extra='width="100%"'}
<!--managing locations-->
