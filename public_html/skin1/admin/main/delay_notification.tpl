<html>
<head>
	<title>Delay Notification</title>
	<link rel="stylesheet" href="{$SkinDir}/skin1_admin.css" />
	<script type="text/javascript" src="{$SkinDir}/js_script/calender.js" ></script>
	<link rel="stylesheet" href="{$SkinDir}/css/calender.css" type="text/css" media="screen" />
</head>
<body align="center" style="margin-left:60px;">
<br><br>
{capture name=dialog}
<form name="delay_notification" action="delay_notification.php" method="post">
<input type="hidden" name="mode" id="mode" value="delay"  />
<input type="hidden" name="userid" id="userid" value="{$smarty.get.userid}" />
<input type="hidden" name="orderid" id="orderid" value="{$smarty.get.orderid}" />

	<br/>
	<table width='95%' class='formtable' cellpadding='2' cellspacing='2' style='border: 1px solid #999999;'>
		{if $smarty.get.d == 'N'}
			<div style="float:left;width:300px;height:20px;font-size:1.0em;color:#D2061B;">
				<strong>Notification can not be sent.</strong>
			</div>
		{elseif $smarty.get.d == 'I'}
			<div style="float:left;width:300px;height:20px;font-size:1.0em;color:#04BB2E;">
				<strong>Notification has been sent.</strong>
			</div>
		{else}
		<tr style="background-color:#91DDE6;">
			<td width='95%' align="left" colspan='2'><p><strong>Send delay notification</strong></p></td>
		</tr>
		<tr class='TableSubHead'>
			<td width='20%' class="SubmitBox" align="right" valign="top"><p><strong>Delay Reason</strong></p></td>
			<td width='75%' class="SubmitBox" align="left"><p><TEXTAREA NAME="delayreason"  ID="delayreason" ROWS="15" COLS="100"></TEXTAREA></p></td>
		</tr>
		<tr class='TableSubHead'>
			<td width='20%' class="SubmitBox" align="right" valign="top"><p><strong>Expected shipment date</strong></p></td>
			<td width='75%' class="SubmitBox" align="left"><p><input type="text" name="shipmentdate" id="shipmentdate" ><img src="{$cdn_base}/images/cal.gif" width="16" height="16" border="0" alt="Click Here to Pick up the date" onclick="displayDatePicker('shipmentdate');"></p></td>
		</tr>
		<tr class='TableSubHead'>
			<td width='20%' class="SubmitBox" align="right" valign="top"><p><strong>Action required from customer</strong></p></td>
			<td width='75%' class="SubmitBox" align="left"><p><TEXTAREA NAME="customeraction"  ID="customeraction" ROWS="15" COLS="100"></TEXTAREA></p></td>
		</tr>
		<tr class='TableSubHead'>
			<td width='95%' class="SubmitBox" align="center" colspan='2'><p>
				<input type="submit" value="Send Notification" name="sendnotification" class="submit" /></p>
			</td>
		</tr>
		{/if}
	 </table>
  </form>
{/capture}
{include file="dialog.tpl" title="Send Delay Notification" content=$smarty.capture.dialog extra='width="90%"'}
</body>
</html>
