{* $Id: featured_products.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}


<a name="featured" />
{literal}
  <script type="text/javascript">

	function popupWindow(URL) {

		window.open(URL, null, "height=500,width=700,status=yes,toolbar=no,menubar=no,location=no,resizable=1,scrollbars=1");

	}
	</script>
{/literal}

{$lng.txt_featured_products}

<br /><br />

{capture name=dialog}

<table cellpadding="3" cellspacing="1" width="100%" border=0>
<tr>
     <td colspan="3">

     {section name=idx loop=$products_design_preview}

      <table>
		<tr>
		    <td >
		    <b>{$lng.lbl_product_type}:</b>&nbsp;{$products_design_preview[idx].type_name}
		    &nbsp;&nbsp;<b>{$lng.lbl_productstyle}:</b>&nbsp;{$products_design_preview[idx].style_name}
		    </td>
		    <td><strong>Options[&nbsp;</strong>
		     {section name=options loop=$optionNames}
			{section name=options loop=$optionValues}
				<strong>{$optionNames[options]}:&nbsp;</strong>{$optionValues[options]}

			{/section}
		     {/section}
		     <strong>{$noOption}</strong>
		   <strong>&nbsp;]</strong> </td>
		</tr>
      </table>
      {/section}
    </td>
</tr>
<tr >

	<td  height="30" >&nbsp;</td>

</tr>

<tr class="TableHead">

	<td width="10%">{$lng.lbl_custmization_area}</td>
	<td width="40%" align="center">{$lng.lbl_preview}</td>
	<td width="30%" align="center">Preview Details</td>
	<td width="20%" align="center">{$lng.lbl_action}</td>
</tr>

<tr>
	<td colspan=4>
		<span style='font-size:16px;font-weight:bold;color:red;'>{$instruction}</span>
	</td>
</tr>

{section name=cust loop=$custAreaId}

<tr>

			<td>{$custLabel[cust]}</td>
			<td align=center>
			<img src={$custImage[cust]} >
			<br/>
			{if $templates[cust]}
			   <span><b>Template : {$templates[cust]}</b></span>
			{/if}
			{if $addons[cust]}
			   <br/> <span><b>Addon : {$addons[cust]}</b></span>
			{/if}	
			</td>
			
			{if $moreOrientation eq 'Y'}
			<td align="left"  colspan="2">
			<table align="left" style="border:1px solid #CCCCCC;" width="100%">
			 {foreach name=outer item=detail from=$custAreaDetails}
			   {foreach key=key item=item from=$detail}
			    {if $custPageNum[cust] eq $item.page_no}    
					<tr><td align="left">
					 <p>
					  {if $item.orientation_final_image_display ne ''}
			                 <b>Image Attributes</b><br/>
							 ImageX: {$item.image_x}px<br>
							 ImageY: {$item.image_y}px<br>
							 Adjusted Image Width: {$item.image_width}px<br>
							 Adjusted Image Height: {$item.image_height}px<br>
			         {/if}
					 {if $item.text ne ''}
					         <b>Text Attributes</b><br/>
							 Text Content: {$item.text}<br>
							 Text X: {$item.text_x}px<br>
							 Text Y: {$item.text_y}px<br>
							 Text Format:  <br/>
							 {$item.text_format}<br>
		            {/if}
					{if $item.bgcolor ne ''}
						    BG Color : {$item.bgcolor}
					{/if}
					</p>
					</td>
			
				  <td>
                  {if $item.orientation_final_image_display  ne ''}
					<a href="javascript:popupWindow('view_full_image.php?mode=orignal&orderid={$orderId}&productid={$productId}&areaid={$custAreaId[cust]}&page={$item.page_no}&oid={$item.orientation_id}')">View original image</a>&nbsp;<br><a href="download_image.php?mode=orignal&orderid={$orderId}&productid={$productId}&areaid={$custAreaId[cust]}&page={$item.page_no}&oid={$item.orientation_id}">[ Download ]</a><br /><br>
					{/if}

                 <a href="javascript:popupWindow('view_full_image.php?mode=large&orderid={$orderId}&productid={$productId}&areaid={$custAreaId[cust]}&page={$item.page_no}&oid={$item.orientation_id}')">View guiding image</a>&nbsp;<br><a href="download_image.php?mode=large&orderid={$orderId}&productid={$productId}&areaid={$custAreaId[cust]}&page={$item.page_no}&oid={$item.orientation_id}">[ Download ]</a><br /><br>

                  {if $image_magick_installed}<!Once image magick is installed this can be shown>
				  <a href="javascript:popupWindow('view_full_image.php?mode=large&orderid={$orderId}&productid={$productId}&areaid={$custAreaId[cust]}&page={$item.page_no}&oid={$item.orientation_id}&func=bgremove&br=255&bg=255&bb=255')">View Edited Image</a>&nbsp;<br><a href="download_image.php?mode=large&orderid={$orderId}&productid={$productId}&areaid={$custAreaId[cust]}&page={$item.page_no}&oid={$item.orientation_id}&func=bgremove&br=255&bg=255&bb=255">[ Download ]</a><br /><br>
                  {/if}
                   <a href="javascript:popupWindow('view_full_image.php?mode=bindurao&orderid={$orderId}&productid={$productId}&areaid={$custAreaId[cust]}&oid={$item.orientation_id}')">View Operations Template Image</a><br><a href="download_image.php?mode=bindurao&orderid={$orderId}&productid={$productId}&areaid={$custAreaId[cust]}&page={$item.page_no}&oid={$item.orientation_id}">[ Download ]</a></br>

				     
					{if $item.text ne ''}
					<a href="javascript:popupWindow('view_full_image.php?mode=text&orderid={$orderId}&productid={$productId}&areaid={$custAreaId[cust]}&page={$item.page_no}&oid={$item.orientation_id}')">View text image</a>&nbsp;<br><a href="download_image.php?mode=text&orderid={$orderId}&productid={$productId}&areaid={$custAreaId[cust]}&page={$item.page_no}&oid={$item.orientation_id}">[ Download ]</a>
						<br/>
		            {/if}
				</td></tr>
				{/if}
			  {/foreach}
			{/foreach}
			
			</table>
			</td>
		  {else}
		     <td align="left"  colspan="2">
			<table align="left" style="border:1px solid #CCCCCC;" width="100%">
			 {foreach name=outer key=k item=detail from=$custAreaDetails}
			   {foreach key=key item=item from=$detail}
			   
			    {if $custAreaId[cust] eq $k}
					<tr><td align="left">
					 <h3>{$item.orientation_name}</h3>
					 <p>
					  {if $item.orientation_final_image_display ne ''}
			                 <b>Image Attributes</b><br/>
							 ImageX: {$item.image_x}px<br>
							 ImageY: {$item.image_y}px<br>
							 Adjusted Image Width: {$item.image_width}px<br>
							 Adjusted Image Height: {$item.image_height}px<br>
			         {/if}
					 {if $item.text ne ''}
					         <b>Text Attributes</b><br/>
							 Text Content: {$item.text}<br>
							 Text X: {$item.text_x}px<br>
							 Text Y: {$item.text_y}px<br>
							 Text Format:  <br/>
							 {$item.text_format}<br>
		            {/if}
					{if $item.bgcolor ne ''}
						    BG Color : {$item.bgcolor}
					{/if}
					</p>
					</td>
			
				  <td>
				  <h3>{$item.orientation_name}</h3>
                    {if $item.orientation_final_image_display  ne ''}
					<a href="javascript:popupWindow('view_full_image.php?mode=orignal&orderid={$orderId}&productid={$productId}&areaid={$custAreaId[cust]}&oid={$item.orientation_id}')">View original image</a>&nbsp;<br><a href="download_image.php?mode=orignal&orderid={$orderId}&productid={$productId}&areaid={$custAreaId[cust]}&oid={$item.orientation_id}">[ Download ]</a><br /><br>
					{/if}

                 <a href="javascript:popupWindow('view_full_image.php?mode=large&orderid={$orderId}&productid={$productId}&areaid={$custAreaId[cust]}&page={$item.page_no}&oid={$item.orientation_id}')">View guiding image</a>&nbsp;<br><a href="download_image.php?mode=large&orderid={$orderId}&productid={$productId}&areaid={$custAreaId[cust]}&page={$item.page_no}&oid={$item.orientation_id}">[ Download ]</a><br /><br>

                  {if $if_image_magick_installed}<!Once image magick is installed this can be shown>
				  <a href="javascript:popupWindow('view_full_image.php?mode=large&orderid={$orderId}&productid={$productId}&areaid={$custAreaId[cust]}&page={$item.page_no}&oid={$item.orientation_id}&func=bgremove&br=255&bg=255&bb=255')">View Edited Image</a>&nbsp;<br><a href="download_image.php?mode=large&orderid={$orderId}&productid={$productId}&areaid={$custAreaId[cust]}&page={$item.page_no}&oid={$item.orientation_id}&func=bgremove&br=255&bg=255&bb=255">[ Download ]</a><br /><br>
                  {/if}
                  <a href="javascript:popupWindow('view_full_image.php?mode=bindurao&orderid={$orderId}&productid={$productId}&areaid={$custAreaId[cust]}&oid={$item.orientation_id}')">View Operations Template Image</a><br><a href="download_image.php?mode=bindurao&orderid={$orderId}&productid={$productId}&areaid={$custAreaId[cust]}&page={$item.page_no}&oid={$item.orientation_id}">[ Download ]</a></br>
                  
				    
					{if $item.text ne ''}
					<a href="javascript:popupWindow('view_full_image.php?mode=text&orderid={$orderId}&productid={$productId}&areaid={$custAreaId[cust]}&oid={$item.orientation_id}')">View text image</a>&nbsp;<br><a href="download_image.php?mode=text&orderid={$orderId}&productid={$productId}&areaid={$custAreaId[cust]}&oid={$item.orientation_id}">[ Download ]</a>
						<br/>
		            {/if}
				</td></tr>
				{/if}
			  {/foreach}
			{/foreach}
			
			</table>
			</td>

		  {/if}	


</tr>

{/section}

</table>


{/capture}
{include file="dialog.tpl" title='' content=$smarty.capture.dialog extra='width="100%"'}

