{* $Id: qa_actions.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
{include file="main/include_js.tpl" src="main/popup_product.js"}
<base href="{$http_location}"> 
<link rel="stylesheet" href="{$SkinDir}/skin1_admin_operations.css" />

{literal}
<script Language="JavaScript" Type="text/javascript">
    function beforeSubmit() {
        var overlay = new ProcessingOverlay();
        overlay.show();
        return true;
    }
        
    function beforeWeightSubmit(){
		var weight = $('#weight').val();
		if(!weight || weight == ""){
			alert("Please enter weight");
			return false;
		}     
		var regex = new RegExp("[^0-9\.]","g");
		if(regex.test(weight)){
			alert("weight should contain only numbers and decimal point");
			return false;
		}
		return true;
    }
    
    function validateSkuBarCode() {
        var itemsToProcessString = $("textarea#skuitemcodes").val();
        // Check for empty string
        if(!itemsToProcessString || itemsToProcessString == "") {
            alert("Please input elements before processing");
            return false;
        }
        //check for unwanted separators used between item barcodes.
        // check if items are already processed or item is not for this order.
        var invalidItems = []; var duplicateItems = [];        
        
        var processed_itemcodes = $("#processed_items").val().split(",");
        var valid_itemcodes = $("#valid_items").val().split(",");
        var itemsToProcess = itemsToProcessString.split('\n');
        for(var i in itemsToProcess) {
            var itembarcode = itemsToProcess[i].trim();
            if(itembarcode != "") { 
                if(itembarcode.replace(/[\da-zA-Z]/g, "") != "") {
                    alert("Invalid separator used between item barcodes");
                    return false;
                }
                
                if(valid_itemcodes.indexOf(itembarcode) == -1) {
                    invalidItems.push(itembarcode);
                } else if(processed_itemcodes.indexOf(itembarcode) != -1) {
                    duplicateItems.push(itembarcode);
                }
            }
        }

        if(duplicateItems.length > 0) {
            alert("Following Items are already processed - "+duplicateItems.join(", "));
            return false;
        }
        if(invalidItems.length > 0) {
            alert("Following Items not issued for this order - "+invalidItems.join(", "));
            return false;
        }
        
        return true;
    }
    
    function processItemsQACheck(orderid, itemid, valid_item_codes, qapass_item_codes) {
        $('#skuitemcodes').unbind();
        showProcessItemsWindow();

        $("#orderid").val(orderid);
        $("#itemid").val(itemid);
        $("#orderid-display").val(orderid);
        $("#itemid-display").val(itemid);
        $("#processed_items").val(qapass_item_codes);
        $("#valid_items").val(valid_item_codes);
        $("textarea#skuitemcodes").val("");
        $("#qaresult").val("");
        $("#reject_comment_capture").css("display", "none");
        $("#reject_comment").val("");
    }
    
    function showProcessItemsWindow() {
        $("#processitemspopup").css("display","block");
        $("#processitemspopup-content").css("display","block");
        $("#processitemspopup_title").css("display","block");
        
        var oThis = $("#processitemspopup-main");
        var topposition = ( $(window).height() - oThis.height() ) / 2;
        if ( topposition < 10 ) 
            topposition = 10;

        oThis.css("top", ( topposition + $(window).scrollTop() + "px"));
        oThis.css("left", ( $(window).width() - oThis.width() ) / 2+$(window).scrollLeft() + "px");

    }

    function closeProcessItemsWindow() {
        $("#processitemspopup").css("display","none");
        $("#processitemspopup-content").css("display","none");
    }

    /*function reasonChange() {
        if($("#qaresult").val() && $("#qaresult").val() != "" && $("#qaresult").val() != "1") {
            $("#reject_comment_capture").css("display", "block");
        } else {
            $("#reject_comment_capture").css("display", "none");
        }
    }*/
    
    function validateForm() {
        if(validateSkuBarCode() == false)
            return false;
                
        var qaResult=$("#qaresult").val();   
        if(!qaResult || qaResult == ""){
            alert("Please select a status to update for the item");
            return false;
        }   
                
        if (qaResult != "61" ) {
           var proceed = confirm("Are you sure you want to QA Fail this item");
           if(!proceed)
               return false; 
        }
 
        var overlay = new ProcessingOverlay();
        overlay.show();
        return true;
    }
    
</script>
{/literal}


{if $action eq 'showData'}
	{capture name=dialog}
	    <div style="clear:both;margin:10px 0px;">
	          <form action="" method="post" style="display:inline;" onsubmit="return beforeWeightSubmit();">
	                <input type="hidden" name="action" value="update_order_weight">
	                <input type="hidden" name="orderid" value="{$orderid}">
	                <input type="hidden" name="userid" value="{$login}">
	                <table>
	                	<tr> 
	                		<td width="20%">Weight(in kgs) </td>
	                		<td width="20%"><input type="text" id="weight" name="weight" value=""> </td>
	                		<td width="50%"><input type=submit id="update-weight" name="update-weight" value="Submit"></td> 
	                	</tr>
	                </table>
	            </form> 
	    </div>
	{/capture}
	{include file="dialog.tpl" title="Order Weight form" content=$smarty.capture.dialog extra='width="100%"'}
{/if}   

{capture name=dialog}
    <div id="errorMsg" style="padding: 10px 0px; color:red; font-weight: bold; font-size: 12px;">{$errorMsg}</div>
    <br>
    <form action="" method="post" name="qa_actions_search" enctype="multipart/form-data" >
        <input type='hidden' name='action' value='getData'>        
        <table border=0 cellspacing=0 cellpadding=0 width=60%>
            <tr>
                <td> Select Your location:</td>
                <td>
                    <select id="warehouseId" name="warehouseId">
                        <option value="">--Select Location--</option>
                        {section name=idx loop=$warehouses}
                            <option value="{$warehouses[idx].id}" {if $warehouses[idx].id eq $warehouseId}selected{/if}>{$warehouses[idx].display_name}</option>
                        {/section}            
                    </select>
                </td>   
            </tr>
            <tr>
                <td>Choose scan type:</td>
                <td><input type=radio name='searchParam' value='itembarcode' {if $searchParam eq 'itembarcode' || $searchParam eq ''}checked{/if}> ItemBarCode &nbsp; &nbsp; &nbsp; <input type=radio name='searchParam' value='orderid' {if $searchParam eq 'orderid'}checked{/if}> OrderId</td>
            </tr>
            <tr>
                <td>Scan value:</td>
                <td><input type="text" name="inputVal" value="{$inputVal}"></td>
            </tr>
            <tr><td></td><td><input type='submit' name='search' value="Search"></td><tr>
        </table>
    </form>
{/capture}
{include file="dialog.tpl" title="Assigned Items Search" content=$smarty.capture.dialog extra='width="100%"'}
<br><br>

{if $action eq 'showData'}
    {if $updateErrorMsg }
        <div id="errorMsg" style="padding: 10px 0px; text-align:center; color:red; font-weight: bold; font-size: 12px;">{$updateErrorMsg}</div>
        <br><br>
    {/if}
    {if $successMsg }
        <div id="successMsg" style="padding: 10px 0px; text-align:center; color:green; font-weight: bold; font-size: 12px;">{$successMsg}</div>
        <br><br>
    {/if}
    {if $premium_packaging eq 'Y'}
        <div style="font-size:1.5em;color:white;background-color:purple; text-align:center">
            <strong>This order has one or more items to be Premium Packaged.</strong>
            <br><br>
        </div>
    {/if}
    {if $gift_status eq 'Y'}    
        <div style="font-size:1.5em;color:#FF0000;background-color:#FFFFCC; text-align:center">
            <strong>This order has to be shipped as a gift. Please click on the button below to print the gift message.</strong>
            <br><br>
        </div>
        <p style="text-align:left"><b> Gift Message:</b> "{$gift_message}" </p>
        <input type=button onClick="javascript:window.open(http_loc+'/admin/qa_actions.php?orderid={$orderid}&print=true','null', 'menubar=0,resizable=1,toolbar=0,width=600,height=300')" name = printgiftmessage value="Print Gift Message">
        <br><br>
    {/if}
    {if $first_order}
    	<div style="font-size:1.5em;color:red;border:1px solid #81BEF7;text-align:center;padding-top:10px;">
            <strong>{$first_order}</strong>
            <br><br>
        </div>
    {/if}
    {if $shipping_method eq 'XPRESS'}
	<div style="font-size:1.7em;color:red;border:1px solid #81BEF7;text-align:center;padding-top:10px;">
            <strong>Next Day Delivery: </strong>
            <br>
            {if $packing_cutoff neq 'false'}
            Packing Cutoff: {$packing_cutoff}
            {/if}
            <br>
            {if $qc_cutoff neq 'false'}
            QC Cutoff: {$qc_cutoff}
            {/if}
        </div>
    {/if}
        {if $shipping_method eq 'SDD'}
	<div style="font-size:1.7em;color:red;border:1px solid #81BEF7;text-align:center;padding-top:10px;">
            <strong>Same Day Delivery: </strong>
            <br>
            {if $packing_cutoff neq 'false'}
            Packing Cutoff: {$packing_cutoff}
            {/if}
            <br>
            {if $qc_cutoff neq 'false'}
            QC Cutoff: {$qc_cutoff}
            {/if}
        </div>
    {/if}

    {if $special_pincode neq 'true'}
        <div>
                                {if $extraCopies > 1 }
                                <table style="background-color:green; margin-top:10px; width:420px">
                                        <tr>
                                                <td style="color:black;">
                                               <b>Notification: Extra Invoice will be generated for this Order.</b>
                                        </td>
                                        <tr>
                                </table>
                                {/if}

            <input type="button" onClick="javascript:window.open(http_loc+'/order_invoice_new.php?orderids={$orderid}&type=admin&print=true&copies={$extraCopies}','null', 'menubar=0,resizable=1,toolbar=0,width=800,height=500')" accesskey="o" value="Print Invoice" name="Print Invoice"/>
            Shortcut (Firefox: Alt + Shift + O, Chrome: Alt + O)
        </div>
    {else}
        <div>
           <input type="button" 
                {if $skipSpecialPincodeRestriction neq 'true' and ($order_status eq "PK" or $order_status eq "WP") and $courier eq "ML"}
                    onClick="javascript:window.alert('This order belongs to a special zipcode. Invoice printing is possible only at the DC. Please print shipping label for this order.')" 
                {else} 
                    onClick="javascript:window.open(http_loc+'/order_invoice_new.php?orderids={$orderid}&type=admin&invoiceType=special&copies={$extraCopies}','null', 'menubar=0,resizable=1,toolbar=0,width=800,height=500')" 
                {/if} 
                {if $order_status eq "F" } disabled {/if} accesskey="o" value="Print Invoice" name="Print Invoice"/>
           Shortcut (Firefox: Alt + Shift + O, Chrome: Alt + O)
        </div>
    {/if}
    {if $special_pincode eq 'true' and $skipSpecialPincodeRestriction neq 'true' }
        <div>
            <input type="button" onClick="javascript:window.open(http_loc+'/shipping_label.php?orderid={$orderid}&type=special','null','scrollbars=1,width=900,height=900,resizable=1')" {if $order_status eq "F" } disabled {/if} value="Shipping Label" name="Shipping Label"/>
        </div>
    {/if}

    {capture name=dialog}    
        {if $qadoneItems|@count ne 0}
            <table cellspacing=0 cellpadding=0 border=0 width=100%>
                <tr class="TableHead">
                    <td width="15%">&nbsp;</td>
                    <td width="15%">Order Id - Status</td>
                    <td width="20%">Item Details</td> 
                    <td width="10%">Ordered Quantity</td>
                    <td width="10%">Issued Quantity</td>
                    <td width="25%">QA Pass Item Barcodes</td>
                    <td width="20%">Action</td>
                </tr>
                {section name=idx loop=$qadoneItems}
                    {if $qadoneItems[idx].customized_message neq ''}
                        <tr height="20">
                                <td colspan="7"><p  style="font-size:1.5em;color:#FF0000;background-color:#FFFFCC; text-align: center"><strong>This order has to be customized as per details.</strong></p></td>
                        </tr>
                    {/if}
                    <tr>
                        <td align=center>
                            <div class="item-img">
                                {if $qadoneItems[idx].default_image}
                                    <img src="{$qadoneItems[idx].display_image}">
                                    <div> {$qadoneItems[idx].product_display_name} </div>
                                {elseif $qadoneItems[idx].image_portal_t}
                                    <img src="{$qadoneItems[idx].image_portal_t}">
                                    <div> {$qadoneItems[idx].product_display_name} </div>
                                {/if}
                            </div>
                        </td>
                        <td align=center>{$qadoneItems[idx].orderid} - {$qadoneItems[idx].status}</td>
                        <td align=center>
                        	<table>
                        		<tr>
                        			<td width="25%">Itemid:</td>
                        			<td width="25%">{$qadoneItems[idx].itemid}</td>
                        		</tr>
                        		<tr>
                        			<td width="25%">SKU Code:</td>
                        			<td width="25%">{$qadoneItems[idx].sku_code}</td>
                        		</tr>
                        		<tr>
                        			<td width="25%">Article id:</td>
                        			<td width="25%">{$qadoneItems[idx].article_number}</td>
                        		</tr>
                        		<tr>
                        			<td width="25%">Size-Quantity:</td>
                        			<td width="25%">{$qadoneItems[idx].size} - {$qadoneItems[idx].totalQty}</td>
                        		</tr>
                        		{if $qadoneItems[idx].status eq 'WP' && $qadoneItems[idx].item_status neq 'A' }
                                    <tr>
                                    	<td width="25%">QA Item Status:</td>
                                    	<td width="25%">{$qadoneItems[idx].item_status}</td>
                                    </tr>
                                {/if}
                        		<tr>
                        			<table>
		                        		<tr>
		                        			<td width="25%"><b>MRP:</b></td>
		                        			<td width="25%" align="right"><b>Rs.{$qadoneItems[idx].price}</b></td>
		                        		</tr>
		                        		<!--div>
		                        		{if $qadoneItems[idx].discount != 0.00 }
		                                <tr>
		                                        <td width="25%">Item Discount:</td>
		                                        <td width="25%" align="right">Rs.{$qadoneItems[idx].discount}</td>
		                                    </tr>
		                                {/if}
		                                {if $qadoneItems[idx].cartDiscount != 0.00 }
		                                    <tr>
		                                        <td width="25%">Cart Discount:</td>
		                                        <td width="25%" align="right">Rs.{$qadoneItems[idx].cartDiscount}</td>
		                                    </tr>
		                                {/if}
		                                {if $qadoneItems[idx].item_coupon_discount != 0.00 }
		                                    <tr>
		                                        <td width="25%">Coupon Discount:</td>
		                                        <td width="25%" align="right">Rs.{$qadoneItems[idx].item_coupon_discount}</td>
		                                    </tr>
		                                {/if}
		                                {if $qadoneItems[idx].pg_discount != 0.00 }
		                                    <tr>
		                                        <td width="25%">PG Discount:</td>
		                                        <td width="25%" align="right">Rs.{$qadoneItems[idx].pg_discount}</td>
		                                    </tr>
		                                {/if}
		                                {if $qadoneItems[idx].item_cashdiscount != 0.00 }
		                                    <tr>
		                                        <td width="25%">Cash Redeemed:</td>
		                                        <td width="25%" align="right">Rs.{$qadoneItems[idx].item_cashdiscount}</td>
		                                    </tr>
		                                {/if}
		                                </div-->
		                                <tr>
		                                    <td width="25%"><b>Amount Paid:</b></td>
		                                    <td width="25%" align="right"><b>Rs.{$qadoneItems[idx].total_amount + $qadoneItems[idx].taxamount}</b></td>
		                                </tr>
		                                {if $qadoneItems[idx].difference_refund != 0.00 }
		                                    <tr>
		                                    	<td width="25%"><b>Price Mismatch Refund</b></td>
		                                    	<td width="25%" align="right"><b>Rs.{$qadoneItems[idx].difference_refund}</b></td>
		                                    </tr>
		                                {/if}
                                </tr>
                        	</table>
                        	{if $qadoneItems[idx].special_packaging eq 'Y'}
                        	<table style="background-color:green; margin-top:10px; width:280px">
                        		<tr>
                        			<td style="color:black;">
                                		<b>Notification: Special packaging required.</b>
                                	</td>
                        		<tr>
                        	</table>
                        	{/if}
                                {if $qadoneItems[idx].packaging_type eq 'PREMIUM'}
                                <table style="background-color:purple; margin-top:10px; width:280px">
                                <tr>
                                    <td style="color:white;">
                                        <b>Notification: Premium packaging required.</b>
                                    </td>
                                <tr>
                                </table>
                                {/if}
                        	{if $qadoneItems[idx].deos_to_BD eq 'Y'}
                        	<table style="background-color:red; margin-top:10px; width:280px">
                        		<tr>
                        			<td style="color:white;">
                                		<b>Warning: Deos assigned to BD.</b>
                                	</td>
                        		<tr>
                        	</table>
                        	{/if}
                            {if $qadoneItems[idx].customized_message neq ''}
                                {assign var=item_data value=$assignedItems[idx]}
                                {assign var=fifa_json value=$item_data.customized_message}
                                <table style="background-color:yellow; margin-top:10px; width:280px">
                                    <tr>
                                        <td><b> Position </b></td>
                                        <td><b> Type </b></td>
                                        <td><b> Color </b></td>
                                        <td><b> Value </b></td>
                                    </tr>
                                       {foreach from=$fifa_json key=name item=nvalue}
                                            {assign var=position value=$name}
                                            {if $name eq 'le'}
                                                {assign var=position value='left exterior'}
                                            {/if}
                                            {if $name eq 'li'}
                                                {assign var=position value='left interior'}
                                            {/if}
                                            {if $name eq 're'}
                                                {assign var=position value='right exterior'}
                                            {/if}
                                            {if $name eq 'ri'}
                                                {assign var=position value='right interior'}
                                            {/if}
                                            {if $name eq 'ba'}
                                                {assign var=position value='back name'}
                                            {/if}
                                            {if $name eq 'bu'}
                                                {assign var=position value='back number'}
                                            {/if}

                                            {assign var=itemType value='-'}
                                            {assign var=jsnItem value=$nvalue.t}
                                            {assign var=jsnColor value=$nvalue.c}
                                            {assign var=jsnValue value=$nvalue.v}
                                             {if $jsnItem eq 'f'}
                                                 {assign var=itemType value='flag'}
                                            {/if}
                                            {if $jsnItem eq 'a'}
                                                {assign var=itemType value='name'}
                                            {/if}
                                            {if $jsnItem eq 'u'}
                                                {assign var=itemType value='number'}
                                            {/if}
                                            <tr>
                                                <td>{$position}</td>
                                                <td>{$itemType}</td>
                                                <td bgcolor="{$jsnColor}">{$jsnColor}</td>
                                                <td>{$jsnValue}</td>
                                            </tr>

                                        {/foreach}
                                </table>
                            {/if}
                        </td>
                        <td align=center>{$qadoneItems[idx].totalQty}</td>
                        <td align=center>{$qadoneItems[idx].issuedQty}</td>
                        <td align=center>
                            {if $qadoneItems[idx].qapass_items}
                                {$qadoneItems[idx].qapass_items}
                            {else} - {/if}
                        </td>
                         <td align=center>
                        {if $qadoneItems[idx].packaging_type eq 'PREMIUM' && $qadoneItems[idx].is_packaged eq 1}
                                Premium Packaging Done
                               
                         {elseif  $qadoneItems[idx].packaging_type eq 'PREMIUM' && $qadoneItems[idx].is_packaged eq 0}
                          <form action="" method="post" style="display:inline;" onsubmit="return beforeSubmit();">
                                        <input type="hidden" name="action" value="update_packaging_status">
                                        <input type="hidden" name="orderid" value="{$qadoneItems[idx].orderid}">
                                        <input type="hidden" name="itemid" value="{$qadoneItems[idx].itemid}">
                                        <input type="hidden" name="userid" value="{$login}">
                                    <input type=submit id="premium-done" name="premium-done" value="Mark Premium Packaged">
                                </form> 
                             {/if}
                         </td>          
                    </tr>
                    <tr>
                    	<td colspan="10">
                    		<div style="font-size:2pt; width:100%; height:1px; background-color:#000000; color:inherit;"></div>
                    	</td>
                    </tr>
                {/section}
            </table>
        {else}
            <div style="padding-left: 30px;">No Items found in QA Done state.</div>
        {/if}
    {/capture}
    {include file="dialog.tpl" title="QA Done Items" content=$smarty.capture.dialog extra='width="100%"'}
    <br><br>  
    
    {capture name=dialog}
        {if $assignedItems|@count ne 0}
            <table cellspacing=0 cellpadding=0 border=0 width=100%>
                <tr class="TableHead">
                    <td width="10%">&nbsp;</td>
                    <td width="15%">Order Id - Status</td>
                    <td width="20%">Item Details</td> 
                    <td width="10%">Ordered Quantity</td>
                    <td width="10%">Issued Quantity</td>
                    <td width="25%">QA Pass Item Barcodes</td>
                    <td width="20%">Action</td>
                </tr>
                {section name=idx loop=$assignedItems}
                    {if $assignedItems[idx].customized_message neq ''}
                        <tr height="20">
                                <td colspan="7"><p  style="font-size:1.5em;color:#FF0000;background-color:#FFFFCC; text-align: center"><strong>This order has to be customized as per details.</strong></p></td>
                        </tr>
                    {/if}
                    <tr>
                        <td align=center>
                            <div class="item-img">
                                {if $assignedItems[idx].default_image}
                                    <a href="{$assignedItems[idx].product_style}?userQuery=true" target="_blank"><img src="{$assignedItems[idx].display_image}">
                                    <div> {$assignedItems[idx].product_display_name} </div>
                                {elseif $assignedItems[idx].image_portal_t}
                                    <img src="{$assignedItems[idx].image_portal_t}">
                                    <div> {$assignedItems[idx].product_display_name} </div>
                                {/if}
                            </div>
                        </td>
                        <td align=center>{$assignedItems[idx].orderid} - {$assignedItems[idx].status}</td>
                        <td align=center>
                        	<table>
                        		<tr>
                        			<td width="25%">Itemid:</td>
                        			<td width="25%">{$assignedItems[idx].itemid}</td>
                        		</tr>
                        		<tr>
                        			<td width="25%">SKU Code:</td>
                        			<td width="25%">{$assignedItems[idx].sku_code}</td>
                        		</tr>
                        		<tr>
                        			<td width="25%">Article id:</td>
                        			<td width="25%">{$assignedItems[idx].article_number}</td>
                        		</tr>
                        		<tr>
                        			<td width="25%">Size-Quantity:</td>
                        			<td width="25%">{$assignedItems[idx].size} - {$assignedItems[idx].totalQty}</td>
                        		</tr>
                        		{if $assignedItems[idx].status eq 'WP' && $assignedItems[idx].item_status neq 'A' }
                                    <tr>
                                    	<td width="25%">QA Item Status:</td>
                                    	<td width="25%">{$assignedItems[idx].item_status}</td>
                                    </tr>
                                {/if}
                        		<tr>
                        			<table>
		                        		<tr>
		                        			<td width="25%"><b>MRP:</b></td>
		                        			<td width="25%" align="right"><b>Rs.{$assignedItems[idx].price}</b></td>
		                        		</tr>
		                        		<!--div>
		                        		{if $assignedItems[idx].discount != 0.00 }
		                                <tr>
		                                        <td width="25%">Item Discount:</td>
		                                        <td width="25%" align="right">Rs.{$assignedItems[idx].discount}</td>
		                                    </tr>
		                                {/if}
		                                {if $assignedItems[idx].cartDiscount != 0.00 }
		                                    <tr>
		                                        <td width="25%">Cart Discount:</td>
		                                        <td width="25%" align="right">Rs.{$assignedItems[idx].cartDiscount}</td>
		                                    </tr>
		                                {/if}
		                                {if $assignedItems[idx].item_coupon_discount != 0.00 }
		                                    <tr>
		                                        <td width="25%">Coupon Discount:</td>
		                                        <td width="25%" align="right">Rs.{$assignedItems[idx].item_coupon_discount}</td>
		                                    </tr>
		                                {/if}
		                                {if $assignedItems[idx].pg_discount != 0.00 }
		                                    <tr>
		                                        <td width="25%">PG Discount:</td>
		                                        <td width="25%" align="right">Rs.{$assignedItems[idx].pg_discount}</td>
		                                    </tr>
		                                {/if}
		                                {if $assignedItems[idx].item_cashdiscount != 0.00 }
		                                    <tr>
		                                        <td width="25%">Cash Redeemed:</td>
		                                        <td width="25%" align="right">Rs.{$assignedItems[idx].item_cashdiscount}</td>
		                                    </tr>
		                                {/if}
		                                </div-->
		                                <tr>
		                                    <td width="25%"><b>Amount Paid:</b></td>
		                                    <td width="25%" align="right"><b>Rs.{$assignedItems[idx].total_amount + $assignedItems[idx].taxamount}</b></td>
		                                </tr>
		                                {if $assignedItems[idx].difference_refund != 0.00 }
		                                    <tr>
		                                    	<td width="25%"><b>Price Mismatch Refund</b></td>
		                                    	<td width="25%" align="right"><b>Rs.{$assignedItems[idx].difference_refund}</b></td>
		                                    </tr>
		                                {/if}
                                </tr>
                        	</table>
                        	{if $assignedItems[idx].special_packaging eq 'Y'}
                        	<table style="background-color:green; margin-top:10px; width:280px">
                        		<tr>
                        			<td style="color:black;">
                                		<b>Notification: Special packaging required.</b>
                                	</td>
                        		<tr>
                        	</table>
                        	{/if}
                                {if $assignedItems[idx].packaging_type eq 'PREMIUM'}
                                <table style="background-color:purple; margin-top:10px; width:280px">
                                <tr>
                                    <td style="color:white;">
                                        <b>Notification: Premium packaging required.</b>
                                    </td>
                                <tr>
                                </table>
                                {/if}
                        	{if $assignedItems[idx].deos_to_BD eq 'Y'}
                        	<table style="background-color:red; margin-top:10px; width:280px">
                        		<tr>
                        			<td style="color:white;">
                                		<b>Warning: Deos assigned to BD.</b>
                                	</td>
                        		<tr>
                        	</table>
                        	{/if}
                            {if $assignedItems[idx].customized_message neq ''}
                                {assign var=item_data value=$assignedItems[idx]}
                                {assign var=fifa_json value=$item_data.customized_message}
                                <table style="background-color:yellow; margin-top:10px; width:280px">
                                    <tr>
                                        <td><b> Position </b></td>
                                        <td><b> Type </b></td>
                                        <td><b> Color </b></td>
                                        <td><b> Value </b></td>
                                    </tr>
                                   {foreach from=$fifa_json key=name item=nvalue}
                                        {assign var=position value=$name}
                                        {if $name eq 'le'}
                                            {assign var=position value='left exterior'}
                                        {/if}
                                        {if $name eq 'li'}
                                            {assign var=position value='left interior'}
                                        {/if}
                                        {if $name eq 're'}
                                            {assign var=position value='right exterior'}
                                        {/if}
                                        {if $name eq 'ri'}
                                            {assign var=position value='right interior'}
                                        {/if}
                                        {if $name eq 'ba'}
                                            {assign var=position value='back name'}
                                        {/if}
                                        {if $name eq 'bu'}
                                            {assign var=position value='back number'}
                                        {/if}

                                        {assign var=itemType value='-'}
                                        {assign var=jsnItem value=$nvalue.t}
                                        {assign var=jsnColor value=$nvalue.c}
                                        {assign var=jsnValue value=$nvalue.v}
                                         {if $jsnItem eq 'f'}
                                             {assign var=itemType value='flag'}
                                        {/if}
                                        {if $jsnItem eq 'a'}
                                            {assign var=itemType value='name'}
                                        {/if}
                                        {if $jsnItem eq 'u'}
                                            {assign var=itemType value='number'}
                                        {/if}
                                        <tr>
                                            <td>{$position}</td>
                                            <td>{$itemType}</td>
                                            <td bgcolor="{$jsnColor}">{$jsnColor}</td>
                                            <td>{$jsnValue}</td>
                                        </tr>

                                    {/foreach}
                              </table>
                            {/if}
                        </td>
                        <td align=center>{$assignedItems[idx].totalQty}</td>
                        <td align=center>{$assignedItems[idx].issuedQty}</td>
                        <td align=center>
                            {if $assignedItems[idx].qapass_items}
                                {$assignedItems[idx].qapass_items}
                            {else} - {/if}
                        </td>
                        <td align=center>
                            {if $assignedItems[idx].totalQty eq $assignedItems[idx].issuedQty and $assignedItems[idx].totalQty eq $assignedItems[idx].qapass_items_count}
                                <form action="" method="post" style="display:inline;" onsubmit="return beforeSubmit();">
                                    <input type="hidden" name="action" value="update_item_status">
                                    <input type="hidden" name="orderid" value="{$assignedItems[idx].orderid}">
                                    <input type="hidden" name="itemid" value="{$assignedItems[idx].itemid}">
                                    <input type="hidden" name="itembarcodes" value="{$assignedItems[idx].qapass_items}">
                                    <input type="hidden" name="warehouseid" value="{$assignedItems[idx].warehouseid}">
                                    <input type="hidden" name="inputVal" value="{$inputVal}">
                                    <input type="hidden" name="warehouseId" value="{$warehouseId}">
                                    <input type="hidden" name="searchParam" value="{$searchParam}">
                                    <input type="hidden" name="userid" value="{$login}">
                                    <input type=submit id="markitem-qadone" name="markitem-qadone" value="Mark QA Done">
                                </form> 
                            {else}
                                <input type=button name="markitem-processed" value="Process Items" onclick="processItemsQACheck({$assignedItems[idx].orderid}, {$assignedItems[idx].itemid}, '{$assignedItems[idx].issued_item_barcodes}', '{$assignedItems[idx].qapass_items}');">
                            {/if}
                        </td>
                    </tr>
                    <tr>
                    	<td colspan="10">
                    		<div style="font-size:2pt; width:100%; height:1px; background-color:#000000; color:inherit;"></div>
                    	</td>
                    </tr>
                {/section}
            </table>
            
            <div id="processitemspopup" style="display:none">
                <div class="overlay" style="display:block;_position:absolute;_width:1300px;_height:1500px;">&nbsp;</div>
                <div id="processitemspopup-main" style="position:absolute; left: 20%; background:white; z-index:900;border:2px solid #999999;">
                    <div id="processitemspopup-content" style="display:none;">
                        <div id="processitemspopup_title" class="popup_title">
                            <div id="processitemspopup_ajaxWindowTitle" class="popup_ajaxWindowTitle">Process Item QA Status</div>
                            <div id="processitemspopup_closeAjaxWindow" class="popup_closeAjaxWindow"><a class="no-decoration-link" href="javascript:closeProcessItemsWindow();">Close</a></div>
                        </div>
                        <div class="clear">&nbsp;</div>
                        <div style="padding:5px 25px; text-align:left; font-size:12px" >    
                            <form action="" method="post" onsubmit="return validateForm();">
                                <input type="hidden" name="action" value="skuitem_status_update">
                                <input type="hidden" id="processed_items" name="processed_items" value="">
                                <input type="hidden" id="valid_items" name="valid_items" value="">
                                <input type="hidden" name="userid" value="{$login}">
                                <input type="hidden" name="orderid" value="{$orderid}">
                                <input type="hidden" name="inputVal" value="{$inputVal}">
                                <input type="hidden" name="warehouseId" value="{$warehouseId}">
                                <input type="hidden" name="searchParam" value="{$searchParam}">
                                <input type="hidden" name="itemid" id="itemid" value="{$itemid}">
                                <table cellspacing=0 cellpadding=0 border=0 width=100%>
                                    <tr class="TableHead">
                                        <td width="15%">Order Id</td>
                                        <td width="15%">Item Id</td> 
                                        <td width="20%">Item Bar Code</td>
                                        <td width="40%">Status</td>
                                        <td width="10%">&nbsp;</td>
                                    </tr>
                                    <tr height=120>
                                        <td align=center style="padding:0 5px;"><input type="text" id="orderid-display" disabled size=11></td>
                                        <td align=center style="padding:0 5px;"><input type="text" id="itemid-display" disabled size=11></td>
                                        <td align=center style="padding:0 5px;"><textarea name="skuitemcodes" id="skuitemcodes" onCopy="return false;" onDrag="return false;" onDrop="return false;" onPaste="return false;"></textarea></td>
                                        <td align=center style="padding:0 5px;">
                                            <select name="qaresult" id="qaresult">
                                                <option value="">-- Select Status To update --</option>
                                                {foreach from=$quality_check_codes item=qcReason }
                                                    <option value="{$qcReason.id}">{$qcReason.rejectReason} - {$qcReason.rejectReasonDescription}</option>
                                                {/foreach}
                                            </select>
                                            <!--
                                            <div style="display:none; padding: 10px 0 0 0px;" id="reject_comment_capture">
                                                Select Quality: 
                                                <select id="reject_quality" name="reject_quality">
                                                    <option value="Q1">Q1</option>
                                                    <option value="Q2">Q2</option>
                                                    <option value="Q3">Q3</option>
                                                </select><br>
                                                Input comment for rejecting:
                                                <textarea name="reject_comment" id="reject_comment" rows=2 cols=30></textarea> 
                                            </div>
                                            -->
                                        </td>
                                        <td align=center style="padding:0 5px;">
                                            <input type=submit name="update-status" id="update-status" value="Update">
                                        </td>
                                    </tr>
                                </table>
                            </form>
                        </div>
                    </div>
                </div>
            </div>  
        {else}
            <div style="padding-left: 30px;">No Items found in Assigned state.</div>
        {/if}
    {/capture}
    {include file="dialog.tpl" title="Assigned Items" content=$smarty.capture.dialog extra='width="100%"'}  
{/if}
