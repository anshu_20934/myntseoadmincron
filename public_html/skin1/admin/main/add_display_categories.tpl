{* $Id: featured_products.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $
*} {include file="main/include_js.tpl" src="main/popup_product.js"}
{literal}
<style>
ul{
	height:300px;
	overflow:auto;
	background:#eeeeee;
}
ul li {
	list-style: none;
	float: left;
	width: 150px;
}
.notification-message{color:#ff0000;}
.message{color:#41A317;}
</style>
<script type="text/javascript">
function addAnotherRule(){
	var rule_block = $("#rule_block");
	var rule_block_clone = $(rule_block).clone();
	var counter = $(rule_block).find('input[name="counter"]');
	var i = parseInt($(counter).val());
	// change names
	$(rule_block_clone).find('.attribute_function_tmp').attr('name','row['+i+'][attribute_function]').removeClass('attribute_function_tmp');
	$(rule_block_clone).find('.attribute_type_id_tmp').attr('name','row['+i+'][attribute_type_value][]').removeClass('attribute_type_id_tmp');
	$(rule_block_clone).find('.attribute_id_tmp').attr('name','row['+i+'][attribute_type]').removeClass('attribute_id_tmp');
	$(rule_block_clone).removeAttr('id').insertBefore("#add_rule_button").show();

	$(counter).attr('value',i+1);
}
function removeRule(ruleBlock){
	$(ruleBlock).parent().parent().remove();	
}
function onChangeAttribure(attrBlock){
	$.get(http_loc+'/admin/catalog/add_display_category.php',{mode:"getAttributeValues",attribute_type:$(attrBlock).val()}, function(data) {
		var obj = jQuery.parseJSON(data);
		var html='<ul>';
		var i = $(attrBlock).parent().parent().find('input[name="counter"]').val();
		$.each(obj, function(intIndex,attribute){
			html+='<li>';
			html+='<input type="checkbox" name="row['+i+'][attribute_type_value][]" value="'+attribute+'">'+attribute+'</li>';
			html+='</li>';
		});
		html+='</ul>';
		$(attrBlock).parent().next().empty();
		$(attrBlock).parent().next().append(html);
	});
}
function validateForm(){
	if($('.display_category_name').val() ==""){
		$('.notification-message').html("Name can not be null");
		return false;
	}else if($('.filter_order').val() =="" || isNaN($('.filter_order').val())){
		$('.notification-message').html("Position can not be null and should be a number");
		return false;
	}else {
		if($('.mode').val()=='update'){
			return true;
			}else{
		$display_cat_name=$('.display_category_name').val();
		var out=true;
		$.ajax({
			url:	http_loc+'/admin/catalog/add_display_category.php?mode=checkForDisplayCategory&name='+$display_cat_name, 
			success:function(data) {
						if($.trim(data) == 'yes'){
							$('.notification-message').html("Display Category with this name alreay exist");
							out=false;
						}
					},
					
			async: false
		});
		return out;
		}
	}
}
</script>
{/literal} {capture name=dialog}
<div class="message">{$message}</div>
<div class="notification-message"></div>
<form name="frmG" action="{$form_action}" method="post" onsubmit="return validateForm()">
<input type="hidden" name="mode" value="{$mode}" class="mode"/>
<input type="hidden" name="display_category_id" value="{$display_category_id}" />
<div style="padding-top: 20px;">Add New display Category <input
	type="text" name="display_category_name" value="{$display_category_name}" class="display_category_name"/> Active <select
	name="is_active">
	<option value="0" {if $is_active == 0}selected{/if}>No</option>
	<option value="1" {if $is_active == 1}selected{/if}>Yes</option>
</select>
Position* 
	<input type="text" name="filter_order" value="{$filter_order}" class="filter_order">
</div>

<div>
<h2>Rules</h2>
<h2>Article Type</h2>
<ul>
	{foreach from=$article_types item=article_type}
	<li><input type="checkbox" name="article_type_id[]" 
		value="{$article_type.id}" {if in_array($article_type.id,$article_type_id_list)}checked{/if}>{$article_type.typename}</input></li>
	{/foreach}
</ul>
</div>
{if $rules}
	{foreach from=$rules item=rule key=rule_number}
<div
	style="clear: both; border-bottom: 1px dashed; padding-bottom: 10px">
<div style="padding: 20px 40px">
<select name="row[{$rule_number}][attribute_function]">
	<option {if $rule->attribute_function == 'OR'}selected{/if}>OR</option>
	<option {if $rule->attribute_function == 'AND'}selected{/if}>AND</option>
</select>
<select name="row[{$rule_number}][attribute_type]"
	onchange="onChangeAttribure(this);">
	{foreach from=$global_attributes item=global_attribute}
	<option value="{$global_attribute.attribute_type}" {if $rule->attribute_type == $global_attribute.attribute_type}selected{/if}>{$global_attribute.attribute_type}</option>
	{/foreach}
</select> equals</div>
<div>
<ul>
{foreach from=$attributes item=attribute key=k}
	{if $k == $rule->attribute_type}
	{foreach from=$attribute
	item=default_global_attribute_value}
	<li><input name="row[{$rule_number}][attribute_type_value][]" type="checkbox"
		value="{$default_global_attribute_value}" {if in_array($default_global_attribute_value,$rule->attribute_type_value)}checked{/if}>{$default_global_attribute_value}</li>
	{/foreach}
	{/if}
{/foreach}
	</ul>

</div>
<div style="clear: both;" align="center"></div>
</div>
	{/foreach}
{/if}	
<div style="padding: 20px 30px" id='add_rule_button'><input
	type="button" onclick="addAnotherRule()" value="Add Another Rule"></div>
<div
	style="display: none;clear: both; border-bottom: 1px dashed; padding: 10px 0;"
	id="rule_block" class="rule_block">
	<input type="hidden" name="counter" value="{$start_counter}">
<div style="clear: both; padding: 20px 30px">
<select class="attribute_function_tmp">
	<option>AND</option>
	<option>OR</option>
</select>
<select
	onchange="onChangeAttribure(this);" class="attribute_id_tmp">
	{foreach from=$global_attributes item=global_attribute}
	<option value="{$global_attribute.attribute_type}">{$global_attribute.attribute_type}</option>
	{/foreach}
</select> 
<input type="button" class="remove-rule" value="Remove this rule" style="float:right" onclick="removeRule(this)">
</div>
<div>
<ul>
	{foreach from=$default_global_attribute_values
	item=default_global_attribute_value}
	<li><input type="checkbox" class="attribute_type_id_tmp"
		value="{$default_global_attribute_value.attribute_value}">{$default_global_attribute_value.attribute_value}</li>
	{/foreach}
	<ul>

</div>
<div style="clear: both;" align="center"></div>
</div>
<div style="clear: both; padding: 20px 30px">
<button type="submit">{$submit_button_text}</button>
</div>
</form>
<a href="{$http_location}/admin/catalog/display_categories.php">Back to Display Categories</a>
{/capture} {include file="dialog.tpl" title="$submit_button_text"
content=$smarty.capture.dialog extra='width="100%"'}
<br />
<br />
