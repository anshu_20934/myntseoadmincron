<script type="text/javascript">
	var httpLocation = '{$http_location}';
	var style_ids = '{$style_ids}';
	var styleIdData = {$styleId};
	var updateContentStyleId = '';
	var styleStatusData = {$styleStatus};
	var possibleStatusTrans = {$possibleStatusTrans};
	
</script>
<link rel="stylesheet" type="text/css" href="{$http_location}/admin/extjs/resources/css/ext-all.css"/>
<script type="text/javascript" src="{$http_location}/admin/extjs/adapter/ext/ext-base.js"></script>
<script type="text/javascript" src="{$http_location}/admin/extjs/ext-all.js"></script>
<script type="text/javascript" src="styleContentHtmlEditor.js"></script>
<script type="text/javascript" src="StylePickerDialog.js"></script>
<script type="text/javascript" src="styleSearchCopy.js"></script>
<script type="text/javascript" src="styleContentUpdate.js"></script>
<div id="formPanel"></div>
<div id="contentPanel"></div>