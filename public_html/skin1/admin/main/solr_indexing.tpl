{* $Id: cancelorders.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
{include file="main/include_js.tpl" src="main/popup_product.js"}
{literal}
  <script language="Javascript">
      
  </script>
{/literal}
{capture name=dialog}
<div class="notification-message">{$message}</div>
<div style="clear:both">
<form  actionName="" method="post" name="mapping-search-form" class="mapping-search-form">
<input type="hidden" name="mode" value="index" >
<div>Servers to publish	:
<ul style="list-style-type: none">
{foreach from=$solr_servers item=solr_server}
	<li>{$solr_server} 
		<input type="checkbox" name="index_server[]" value="{$solr_server}">
	</li>
{/foreach}
<ul>
</div>
<table>
<tr>
	<td >
		<label>Index On Local Servers</label>
	</td>
	<td>	
		<input type="checkbox" name="only_local_server" >
	</td>
</tr>
<tr>
	<td >
		<label>Index Type</label>
	</td>
	<td>	
		<select name="styletype" >
            <option value="1" >Non Customized</option>
            <option value="2" >Customized</option>
            <option value="0" >All</option>
         </select>
	</td>
</tr>
<tr>
	<td >
		<label>Threads Per Server</label>
	</td>
	<td>	
		<select name="threads" >
            <option value="1" >1 Thread</option>
            <option value="2" >2 Thread</option>
            <option value="3" >3 Thread</option>
            <option value="4" >4 Thread</option>
         </select>
	</td>
</tr>
<tr>	
	<td>
		<input type="submit" name="submit-btn" class="submit-btn" value="Start Indexing">
	</td>
</tr>
</table>
</form>
</div>
{/capture}
{include file="dialog.tpl" title='Solr Indexing' content=$smarty.capture.dialog extra='width="100%"'}