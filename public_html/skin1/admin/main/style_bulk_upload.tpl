{* $Id: cancelorders.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
{include file="main/include_js.tpl" src="main/popup_product.js"}

<link rel="stylesheet" href="{$SkinDir}/skin1_admin_operations.css" />
{literal}
<style>
.list-form tr{
	height:5px;
}
</style>
{/literal}
<script type="text/javascript">
	var progressKey = '{$progresskey}';
</script>
{capture name=dialog}
<div>
	{if $message}<font color='green' size='3'>{$message}</font><br/><br/>{/if}
	{if $errormessage}<font color='red' size='2'>{$errormessage}</font><br/><br/>{/if}
	<form action="" method="post" name="skuexcelfileform" enctype="multipart/form-data">
		<table>
			<tr>
				<td>Upload the file:<br>(Excel file. <br>First row will be ignored as header)</td>
				<td><input type="file" name="skusfile" size=100></td>
			</tr>
			<tr><td colspan=2 align=left><input type='submit' name='uploadxls' value="Upload"></td></tr>
		</table>
		<input type='hidden' name='mode' value='upload'>
		<input type='hidden' name='login' value='{$login}'>
	</form>
</div>
<div>
<form action="style_bulk_upload.php" method="post" id="mappingaddform" name="mappingaddform" class="list-form" onsubmit="return showUploadStatusWindow();">
	<input type="hidden" name="mode"  value="save"/>
	<input type="hidden" name="progresskey"  value="{$progresskey}"/>
	<table cellpadding="3" cellspacing="1" width="100%">
	<thead>
	<tr class="TableHead">
		<th>Update</th>
		<th>Error</th>
		<th>Style Id</th>
		<th>Vendor Article Number</th>
		<th>Vendor Article Name</th>
		<th>Status</th>
		<th>Article Type</th>
		<th>Brand</th>
		<th>Style Description</th>
		<th>Style Note</th>
		<th>Materials and Care Description</th>
		<th>Size and Fit Description</th>
		<th>Age Group</th>
		<th>Gender</th>
		<th>Base Colour</th>
		<th>Colour1</th>
		<th>Colour2</th>
		<th>Season</th>
		<th>Fashion Type</th>
		<th>Usage</th>
		<th>Year</th>
		{if $applySizeScaleRestriction}
			<th>Size Chart Scale</th>
		{/if}
		<th>Size - SKU code pair</th>
		<th>MRP</th>
		<th>Size Chart Path</th>
		<th>Product Display Name</th>
		<th>Tags</th>
		<th>Product Type</th>
		<th>Classification Brand</th>
		<th>Comments</th>
	</tr>
	</thead>
	<tbody>
{if $upload_styles}
	{foreach name=dataloop key=k item=row from=$upload_styles}
	<tr class="style_row">
		<input type="hidden" name="posted_data[{$k}][validated_style]" value="{$row.validated}">
		<input type="hidden" name="posted_data[{$k}][upload_image]" value="{$row.upload_image}">
		<td>
			<input type="checkbox" name="posted_data[{$k}][validated_style_chk]" {if $row.validated eq 'Y'}checked{/if}  readonly="readonly" class='style_checkbox' style="display:none">
			<input type="checkbox" name="posted_data[{$k}][validated_style_chk]" {if $row.validated eq 'Y'}checked{/if}  disabled>
		</td>
		<td style="color:red">{$row.validated_error}</td>
		<td><input type="text" name="posted_data[{$k}][style_id]" value="{$row.style_id}" readonly="readonly"></td>
		<td><input type="text" name="posted_data[{$k}][article_number]" value="{$row.vendor_article_number}" readonly="readonly"></td>
		<td><input type="text" name="posted_data[{$k}][article_name]" value="{$row.vendor_article_name}" readonly="readonly"></td>
		<td><input type="text" name="posted_data[{$k}][status]" value="{$row.status}" readonly="readonly"></td>
		<td><input type="text" name="posted_data[{$k}][global_attr_article_type]" value="{$row.article_type}" readonly="readonly"></td>
		<td><input type="text" name="posted_data[{$k}][global_attr_brand]" value="{$row.brand}" readonly="readonly"></td>
		<td><textarea name="posted_data[{$k}][description]" cols="50" rows="5" class="mceEditor" readonly="readonly">{$row.description}</textarea></td>
		<td><textarea name="posted_data[{$k}][styleNote]" cols="50" rows="5" class="mceEditor" readonly="readonly">{$row.styleNote}</textarea></td>
		<td><textarea name="posted_data[{$k}][materialsCareDesc]" cols="50" rows="5" class="mceEditor" readonly="readonly">{$row.materialsCareDesc}</textarea></td>
		<td><textarea name="posted_data[{$k}][sizeFitDesc]" cols="50" rows="5" class="mceEditor" readonly="readonly">{$row.sizeFitDesc}</textarea></td>
		<td><input type="text" name="posted_data[{$k}][age_group]" value="{$row.age_group}" readonly="readonly"></td>
		<td><input type="text" name="posted_data[{$k}][global_attr_gender]" value="{$row.gender}" readonly="readonly"></td>
		<td><input type="text" name="posted_data[{$k}][global_attr_base_colour]" value="{$row.base_color}" readonly="readonly"></td>
		<td><input type="text" name="posted_data[{$k}][colour1]" value="{$row.color1}" readonly="readonly"></td>
		<td><input type="text" name="posted_data[{$k}][colour2]" value="{$row.color2}" readonly="readonly"></td>
		<td><input type="text" name="posted_data[{$k}][global_attr_season]" value="{$row.global_attr_season}" readonly="readonly"></td>
		<td><input type="text" name="posted_data[{$k}][global_attr_fashion_type]" value="{$row.fashion_type}" readonly="readonly"></td>
		<td><input type="text" name="posted_data[{$k}][global_attr_usage]" value="{$row.usage}" readonly="readonly"></td>
		<td><input type="text" name="posted_data[{$k}][global_attr_year]" value="{$row.year}" readonly="readonly"></td>
		{if $applySizeScaleRestriction}
			<td><input type="text" name="posted_data[{$k}][size_chart_scale]" value="{$row.size_chart_scale}" readonly="readonly"></td>
		{/if}
		<td><input type="text" name="posted_data[{$k}][size_skus]" value="{$row.size_skus}" readonly="readonly"></td>
		<td><input type="text" name="posted_data[{$k}][price]" value="{$row.mrp}" readonly="readonly"></td>
		<td><input type="text" name="posted_data[{$k}][size_chart_path]" value="{$row.size_chart_path}" readonly="readonly"></td>
		<td><input type="text" name="posted_data[{$k}][product_display_name]" value="{$row.product_display_name}" readonly="readonly"></td>
		<td><input type="text" name="posted_data[{$k}][product_tags]" value="{$row.tags}" readonly="readonly"></td>
		<td><input type="text" name="posted_data[{$k}][product_type]" value="{$row.product_type}" readonly="readonly"></td>
		<td><input type="text" name="posted_data[{$k}][classification_brand]" value="{$row.classification_brand}" readonly="readonly"></td>
		<td><input type="text" name="posted_data[{$k}][comments]" value="{$row.comments}" readonly="readonly"></td>
	</tr>
	{/foreach}
{/if}	
	</tbody>
	</table>
<input type="button" value="Upload Styles" onclick="saveStyles()">
</form>
<div id="uploadstatuspopup" style="display:none">
			<div class="overlay" style="display:block;_position:absolute;_width:1300px;_height:1500px;">&nbsp;</div>
			<div id="uploadstatuspopup-main" style="position:absolute; left: 20%; width:450px; background:white; z-index:1006;border:2px solid #999999;">
				<div id="uploadstatuspopup-content" style="display:none;">
				    <div id="uploadstatuspopup_title" class="popup_title">
				    	<div id="uploadstatuspopup_ajaxWindowTitle" class="popup_ajaxWindowTitle">Upload Status</div>
						<div class="clear">&nbsp;</div>
					</div>
					<div style="padding:5px 25px; text-align:left; font-size:12px" >
						<div id="progress_container"> 
						    <div id="progress_bar">
						    	<div id="progress_completed"></div> 
						    </div>
						</div>
						<span id="progress_text" style="font-size: 14px; color: green; padding: 0 0 0 5px;">0</span> % Complete
					</div>
					<div class="clear">&nbsp;</div>
				</div>
			</div>
		</div>
</div>
{/capture}
<script type="text/javascript" src="{$http_location}/skin1/js_script/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript" src="{$http_location}/skin1/myntra_js/jquery.min.js"></script>
{literal}
<script type="text/javascript">

	tinyMCE.init({
    	// General options
    	mode : "textareas",
    	editor_selector : "mceEditor",
    	theme : "advanced",
    	plugins : "safari,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,inlinepopups",
		// Theme options
// Theme options
    	theme_advanced_buttons1 : "newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull",
    	theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
    	theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
    	theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak",
	    theme_advanced_toolbar_location : "none",
    	theme_advanced_toolbar_align : "none",
       	theme_advanced_statusbar_location : "none",
		theme_advanced_resizing : true,
		// Example content CSS (should be your site CSS)

		content_css : "css/content.css",
		// Drop lists for link/image/media/template dialogs

		template_external_list_url : "lists/template_list.js",

		external_link_list_url : "lists/link_list.js",

		external_image_list_url : "lists/image_list.js",

		media_external_list_url : "lists/media_list.js"
		// Replace values for the template plugin
	});

	function saveStyles()
	{
		var fields = $("input[class='style_checkbox']").serializeArray();
		if (fields.length == 0) 
		{ 
			alert('No Style is completely correct. Please correct it before uploading.');
			return;
		}
		else
		{
			var r = confirm("Correct " + fields.length + " styles will be updated or added");
		    if(r == true)
		    {
		    	document.mappingaddform.mode.value = 'save';
			   // document.mappingaddform.submit();
			    $('#mappingaddform').submit();
		    }
		}
	}
	
	function showUploadStatusWindow() {
		$("#uploadstatuspopup").css("display","block");
		$("#uploadstatuspopup-content").css("display","block");
		$("#uploadstatuspopup_title").css("display","block");
		
		var oThis = $("#uploadstatuspopup-main");
		var topposition = ( $(window).height() - oThis.height() ) / 2;
		if ( topposition < 10 ) 
			topposition = 10;
		oThis.css("top", ( topposition + $(window).scrollTop() + "px"));
		oThis.css("left", ( $(window).width() - oThis.width() ) / 2+$(window).scrollLeft() + "px");

		setInterval(trackStatus, 1000);

		return true;
	}
	
	function trackStatus() {
		$.get("getprogressinfo.php?progresskey="+progressKey,
			function(percent) {
				if(percent != "false") {
					$("#progress_bar").css('width', percent+"%");
					$("#progress_text").html(percent);
				}
			}
		);
	}
</script>	
{/literal}
{include file="dialog.tpl" title='Upload Products' content=$smarty.capture.dialog extra='width="100%"'}