<link rel="stylesheet" type="text/css"
	href="{$http_location}/admin/extjs/resources/css/ext-all.css" />
<script
	type="text/javascript"
	src="{$http_location}/admin/extjs/adapter/ext/ext-base.js"></script>
<script
	type="text/javascript" src="{$http_location}/admin/extjs/ext-all.js"></script>

{if ($type == 'dashboard') }
	<script
		type="text/javascript" src="shipment_tracking_dashboard.js"></script>
	</br>
	</br>
	<div style="text-align: center; font-size: 1.5em;">Delivery tracking report</div>
	</br>
	</br>
	<div id='filterPanel' align="center"></div>
	<div id='dashboardPanel' align="center"></div>
{/if}

{if ($type == 'sla') }
	<script
		type="text/javascript" src="shipment_tracking_sla.js"></script>
	<div style="text-align: center; font-size: 1.5em;">Shipping vendor SLAs</div>
	</br>
	</br>
	<div id='slaFilterPanel' align="center"></div>
	<div id='deliveryTATPanel' align="center"></div>
	<div id='firstScanTATPanel' align="center"></div>
	<div id='rtoTATPanel' align="center"></div>
{/if}