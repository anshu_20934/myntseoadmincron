<script type="text/javascript">
	var articleTypeData = {$articleType};
	var spset = 'false';
	var articleTypeId = {$articleTypeId}
	{if isset($specificAttribute)}
		var specificAttribute = {$specificAttribute};
		var specificAttributeId = '{$specificAttributeId}';
		spset = 'true';
	{/if}
    var httpLocation = '{$http_location}';
</script>
<link rel="stylesheet" type="text/css" href="{$http_location}/admin/extjs/resources/css/ext-all.css"/>
<script type="text/javascript" src="{$http_location}/admin/extjs/adapter/ext/ext-base.js"></script>
<script type="text/javascript" src="{$http_location}/admin/extjs/ext-all.js"></script>
<script type="text/javascript" src="styleSpecificAttribute.js"></script>
<div id="wholeExt">
<div id="formPanel"></div>
<div id="contentPanel"></div>
</div>