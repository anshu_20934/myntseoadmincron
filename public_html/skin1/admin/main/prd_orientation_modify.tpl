{* $Id: featured_products.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
{include file="main/include_js.tpl" src="main/popup_product.js"}

<a name="featured" />

{$lng.txt_featured_products}

<br /><br />


{capture name=dialog}


<form action="style_custumization.php" method="post" name="ortmodifyform"  >
<input type="hidden" name="mode"  />
<input type="hidden" name="styleid" value="{$styleid}" />
<input type="hidden" name="custom_id" value="{$custom_id}" />
<input type="hidden" name="delete_row_id" value="0" />


<table cellpadding="3" cellspacing="1" width="100%">

{if $orient}

{section name=prod_num loop=$orient}
<input type="hidden" name="ortid" value="{$orient[prod_num].id}" />
  
   
     <tr>
         <td><b>{$lng.lbl_area_name} : </b></td>
         <td><input type="text" name="loc_name" size="50" value="{$orient[prod_num].location_name}" /></td>
        
	 <td><b>Start X : </b></td>
         <td><input type="text" name="startx" size="20" value="{$orient[prod_num].start_x}" /></td>
       
    </tr>
    

    <tr>
         <td><b>Start Y : </b></td>
         <td><input type="text" name="starty" size="20" value="{$orient[prod_num].start_y}" /></td>

	 <td><b>Size X : </b></td>
         <td><input type="text" name="sizex" size="20" value="{$orient[prod_num].size_x}" /></td>
    </tr>
     
   

    <tr>
         <td><b>Size Y : </b></td>
         <td><input type="text" name="sizey" size="20" value="{$orient[prod_num].size_y}" /></td>
         <td valign="middle"><b><b>Height</b>(in inch):</b></td>
         <td><input type="text" name="height_in" size="20" value="{$orient[prod_num].height_in}" /></td>
         
    </tr>
    
     <tr>
         <td><b>Width</b>(in inch): </td>
         <td><input type="text" name="width_in" size="20" value="{$orient[prod_num].width_in}" /></td>
         <td valign="middle"><b>{$lng.lbl_type_description} : </b></td>
         <td><textarea name="desc" cols="50" rows="5">{$orient[prod_num].description}</textarea></td>
         
    </tr>
    
    
	<tr>
		<td ><b>Bg Start X</b></td>
		<td ><input type="text" name="bgstartx" size="20" value="{$orient[prod_num].bgstart_x}"/></td>
		
		<td><b>Bg Start Y</b></td>
		<td  ><input type="text" name="bgstarty" size="20" value="{$orient[prod_num].bgstart_y}"/></td>
		
	</tr>
	<tr>
	   <td ><b>Bg X Size</b></td>
		<td ><input type="text" name="bgsizex" size="20" value="{$orient[prod_num].bgsize_x}"/></td>
		<td  ><b>Bg Y Size</b></td>
		<td><input type="text" name="bgsizey" size="20" value="{$orient[prod_num].bgsize_y}" /></td>
	</tr>

	<tr>
	  <td align="left" width="100"><b>Bg Height</b>(in inch) </td>
	  <td align="left"> <input type="text" name="bgheight_in"  size="20" value="{$orient[prod_num].bgheight_in}" /></td>
	  <td align="left" width="100"><b>Bg Width</b>(in inch)</td>
	  <td align="left"> <input type="text" name="bgwidth_in"  size="20" value="{$orient[prod_num].bgwidth_in}"/></td>
	</tr>

	<tr>
	  <td align="left" width="100"><b>Action: </b></td>
	  <td align="left"> <input type="text" name="action"  value="{$orient[prod_num].action}" /></td>
	  <td align="left" width="100"><b>Customization Type: </b></td>
	  <td align="left"><select name="typeid"> {html_options options=$customization_types selected=$orient[prod_num].typeid} </select></td>
	</tr>

    <tr>
    	  <td align="left" width="100"><b>Image Allowed: </b></td>
    	  <td align="left"> <input type="checkbox" name="imageAllowed"  {if $orient[prod_num].imageAllowed eq 1 } checked{/if} value="1" /></td>
    	  <td align="left" width="100"><b>Image Customisation Allowed: </b></td>
    	  <td align="left"><input type="checkbox" name="imageCustAllowed"  {if $orient[prod_num].imageCustAllowed eq 1 } checked{/if} value="1" /></td>
   	</tr>

   <tr>
    	  <td align="left" width="100"><b>Text Allowed: </b></td>
    	  <td align="left"> <input type="checkbox" name="textAllowed"  {if $orient[prod_num].textAllowed eq 1 } checked{/if} value="1" /></td>
    	  <td align="left" width="100"><b>Text Customisation Allowed: </b></td>
    	  <td align="left"><input type="checkbox" name="textCustAllowed"  {if $orient[prod_num].textCustAllowed eq 1 } checked{/if} value="1" /></td>
   	</tr>
   	
	<tr>
			<td><b>Text Limitation: </b></td>
			<td><SELECT NAME="textLimitation" >
				<OPTION {if $orient[prod_num].textLimitation eq 1 } selected {/if} VALUE="1">Y</option>
				<OPTION {if $orient[prod_num].textLimitation eq 0 } selected {/if} VALUE="0">N</option>
				</SELECT></td>
				
			<td><b>Text Limitation TPL Name: </b></td>
			<td><input type="text" name="textLimitationTpl" value={$orient[prod_num].textLimitationTpl}></td>
   	</tr>
   	
   	<tr><td colspan="4"><hr></td></tr>
	<tr>
		<td><b>Text Image Path: (For Jerseys)</b></td>
		<td colspan=2><input type="text" name="pathForImagesToReplaceText" value="{$orient[prod_num].pathForImagesToReplaceText}" size="100"></td>
   	</tr>
	<tr>
		<td>Example for path location:</td>
		<td>skin1/affiliatetemplates/images/nike/number/</td>
   	</tr>
   	<tr><td colspan="4"><hr></td></tr>
   	
	<tr>
		<td colspan="2">Text Font Colors</td>
		<td colspan="2">Text Default Color</td>
	</tr>
   	
	<tr>
		<td colspan="2">
			<div style="width:410px;border:1px solid #000;height:215px;">
				{assign var="selectedFonts" value=$orient[prod_num].textFontColors}
				{section name=idx loop=$font_results}
				<div style="float:left">
					<input type="checkbox" name="textFontColors[]" value="{$font_results[idx].id}" style="float:left;margin-bottom:0px;" 
						{section name=idx2 loop=$selectedFonts}
							{if $selectedFonts[idx2] eq $font_results[idx].id}
								checked
							{/if}
						{/section}
					/>
					<span style="background-color:{$font_results[idx].color};height:13px;_height:14px;font-size:0px;width:13px;display:inline;float:left;margin-top:3px;">&nbsp;</span>
				</div>
				{/section}
			</div>
		</td>
		
		<td colspan="2">
			<div style="width:410px;border:1px solid #000;height:215px;">
				{section name=idx loop=$font_results}
				<div style="float:left">
					<input type="radio" name="textColorDefault" value="{$font_results[idx].id}" style="float:left;margin-bottom:0px;"  
						{if $orient[prod_num].textColorDefault eq $font_results[idx].id } checked {/if}  
					/>
					<span style="background-color:{$font_results[idx].color};height:13px;_height:14px;font-size:0px;width:13px;display:inline;float:left;margin-top:3px">&nbsp;</span>
				</div>
				{/section}
		</div>
		</td>
   	</tr>
   	
   	
   	
   	<tr>
   		<td colspan=4><hr>
   			<table cellpadding="3" cellspacing="1" width="100%">
			   	<tr>
			   		<td>Specific Text limitations</td>
			   	</tr>
			   	<tr>
			   		<th align="left">Orientation ID</th>
			   		<th align="left">Default</th>
			   		<th align="left">Font</th>
			   		<th align="left">Maxchars</th>
			   		<th align="left">Font Size</th>
			   		<th align="left">Default Font Size</th>
			   		<th align="left">Char Type</th>
			   		<th align="left">Char Case</th>
			   		<th align="left">Action</th>
			   	</tr>
			   	
   				{if $limitedTextFontDetails}
			   	{section name=limitorientnum loop=$limitedTextFontDetails}
			   	<tr>
			   		<td align="left">{$limitedTextFontDetails[limitorientnum].orientation_id}
			   		{if $limitedTextFontDetails[limitorientnum].font_default eq 'Y'}
			   			- Default
			   		{/if}
			   		
			   		</td>
			   		
			   		<td align="left">
			   			<input type="radio" name="defaultfont" 
			   				{if $limitedTextFontDetails[limitorientnum].font_default eq 'Y'} checked{/if}
			   				value="{$limitedTextFontDetails[limitorientnum].id}" 
			   			/>
			   		</td>
			   		
			   		<td align="left">
				   		<select name="textlimitationfont[{$limitedTextFontDetails[limitorientnum].id}]">
				   		{section name=idx loop=$fonts}
				   			<option value="{$fonts[idx].id}" {if $limitedTextFontDetails[limitorientnum].font_id eq $fonts[idx].id } selected {/if}>
				   				{$fonts[idx].name}
				   			</option>
				   		{/section}
				   		</select>
			   		</td>
			   		
			   		<td align="left">
			   			<input type="text" value="{$limitedTextFontDetails[limitorientnum].maxchars}" 
			   				name="textlimitationmaxchar[{$limitedTextFontDetails[limitorientnum].id}]"/>
			   		</td>
			   		<td align="left">
			   			<input type="text" value="{$limitedTextFontDetails[limitorientnum].font_size}" 
			   				name="textlimitationfontsize[{$limitedTextFontDetails[limitorientnum].id}]" />
			   		</td>
					<td align="left">
						<input type="radio" name="textlimitationdefaultfontsize[{$limitedTextFontDetails[limitorientnum].font_id}]"
							value="{$limitedTextFontDetails[limitorientnum].id}"
							{if $limitedTextFontDetails[limitorientnum].size_default eq 'Y'} checked {/if}
						/>
			   		</td>

					<td align="left">
						<select name="textlimitchartype[{$limitedTextFontDetails[limitorientnum].id}]">
							<option value="all" {if $limitedTextFontDetails[limitorientnum].chartype eq 'all'} selected {/if}> All </option>
							<option value="alphaonly" {if $limitedTextFontDetails[limitorientnum].chartype eq 'alphaonly'} selected {/if}> AlphaOnly </option>
							<option value="numericonly" {if $limitedTextFontDetails[limitorientnum].chartype eq 'numericonly'} selected {/if}> NumericOnly </option>
							<option value="alphanumeric" {if $limitedTextFontDetails[limitorientnum].chartype eq 'alphanumeric'} selected {/if}> AlphaNumber </option>
			   			</select>
			   		</td>
			   		
					<td align="left">
						<select name="textlimitcharcase[{$limitedTextFontDetails[limitorientnum].id}]">
							<option value="all" {if $limitedTextFontDetails[limitorientnum].charcase eq 'all'} selected{/if}> All </option>
							<option value="upper" {if $limitedTextFontDetails[limitorientnum].charcase eq 'upper'} selected{/if}> Upper </option>
							<option value="lower" {if $limitedTextFontDetails[limitorientnum].charcase eq 'lower'} selected{/if}> Lower </option>
			   			</select>
			   		</td>

			   		<td align="left">
			   			{if $limitedTextFontDetails[limitorientnum].font_default neq 'Y'}
			   		    <input type="submit" value="{$lng.lbl_delete|escape}"  
			   		    	onclick="document.ortmodifyform.delete_row_id.value={$limitedTextFontDetails[limitorientnum].id}; javascript: submitForm(this, 'deletetextlimitation');" />
			   		    {else}
			   		    	Cannot delete default
			   		    {/if}
			   		</td>
			   	</tr>
			   	{/section}
			   	{/if}
			   	<tr>
			   		<td align="left">Add New Text Detail</td>
			   		<td align="left"></td>
			   		<td align="left">
			   			<select name="textlimitationfont_new">
			   			<option value="0"></option>
				   		{section name=idx loop=$fonts}
				   			<option value="{$fonts[idx].id}"> {$fonts[idx].name} </option>
				   		{/section}
				   		</select>
				   	</td>
				   	<td align="left"><input type="text" name="textlimitationmaxchar_new"></td>
				   	<td align="left"><input type="text" name="textlimitationfontsize_new"></td>
				   	<td align="left"></td>
				   	<td align="left"></td>
				   	<td align="left"></td>
				   	<td align="left">
			   		    <input type="submit" value="Add New" onclick="javascript: submitForm(this, 'addtextlimitation');" />
			   		</td>
			   	</tr>
	   		</table>
   		
   		</td>
   	</tr>
 
	
{/section}

{/if}

 <tr>
         <td colspan="2">&nbsp;</td>
         
 </tr>

<tr>
	<td colspan="2" class="SubmitBox">
    <input type="submit" value="{$lng.lbl_update|escape}" onclick="javascript: submitForm(this, 'updateort');" />
	<input type="reset" value="{$lng.lbl_cancel|escape}"   />
	</td>
</tr>



 <tr>
<td colspan="2"><br /><br /></td>
</tr>



</table>
</form>

{/capture}
{include file="dialog.tpl" title=$lng.lbl_orientation_heading content=$smarty.capture.dialog extra='width="100%"'}

