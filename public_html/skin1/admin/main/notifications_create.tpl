<link rel="stylesheet" type="text/css" href="/skin1/myntra_css/lightbox_admin.css">
<link rel="stylesheet" type="text/css" href="/skin1/myntra_css/jquery-ui-datepicker.css">
<h1 class='ntfSuccess'>Notification Created Successfully</h1>
<a href='javascript:void(0);' class='createNewNotification'><h2>Create New Notification</h2></a>
<div id = 'createTemplate' class='lightbox'>
	<div class='mod'>
		<h2>Create New Notification</h2>
		<form class='clear' id='notificationForm' action='/admin/notifications_submit.php' method='post' enctype='multipart/form-data'>
			<p class='mandatoryText'>* - Mandatory Field</p>
			<div class='clear formElement ntfTitle'>
				<label>Notification Title<sup>*</sup><span class='error'>Enter proper title</span></label>
				<div class='rightElement'><input type='text' name='ntfTitle' /></div> 
			</div>
			<div class='clear formElement ntfMessage'>
				<label>Notification Message<sup>*</sup>
					<span class='guide'>(max 500 characters)</span>
					<span class='error'>Enter proper Message</span>
				</label>
				<div class='rightElement'><textarea class='editor'></textarea></div>
				<div class='hidden' id='ntfMock'></div>
				<textarea name='ntfMessage' class='filteredMessage hidden'></textarea> 
			</div>
			<div class='clear formElement ntfImageUrl'>
				<label>CDN URL For Image<sup>*</sup><span class='guide'>(supported image types: bmp, jpg, gif, png)</span><span class='guide'>Image Size:48px X 64px</span><span class='error'>Enter proper image url</span></label>
				<div class='rightElement'><input type='text' name='ntfImageUrl' /></div>
			</div>
			<div class='clear singleElement'>
				<div class='leftElement'><label>Start Date:<sup>*</sup></label></label><input readonly='readonly' type='text' class='sdate' name='startDate'/></div> 
				<div class='leftElement'><label>End Date:<sup>*</sup></label><input readonly='readonly' type='text' class='edate' name='endDate'/></div>
				<span class='error e1'>End Date cannot be less than or equal to start date</span> 
				<span class='error e2'>Please provide an end date</span> 
			</div>
			{if count($userGroup)}
			<div class='clear formElement ntfGroups'>
				<label>Group</label>
				<div class='rightElement'>
					<select name='userGroup'>
						<option selected="selected" value='-1'>Select Group</option>
						{foreach item=usg from=$userGroup}
							<option value='{$usg.id}'>{$usg.title}</option>								
						{/foreach}
					</select>
				</div>
			</div>
			{/if}
			<div class='clear formElement ntfIds'>
				<label>Additional User Ids:<span class='guide'>(Please use Comma Separated Values without Spaces in between)</span><span class='error'>Enter email ids in proper format</span></label>
				<div class='rightElement'><textarea class='userIds' name='userIds'></textarea></div>
			</div>
			<div class='clear formElement' id='mockView'>
				<label>Mock View</label>
				<input type='button' value='Refresh' class='refresh' />
				<div class='rightElement'>
					<img alt="Image" src="" />
					<h3>Title</h3>
					<p class='msg'>Message</p>
				</div>
				<div class='mockNotificationMsg'></div>
			</div>
			<div class='singleElement'>
				<div class='leftElement'><input type='submit' class='submitBtn' value='Save' /></div>
				<div class='leftElement'><input type='reset' class='resetBtn' value='Clear' /></div>
			</div>			
		</form>
	</div>
</div>
<script language="javascript" src="/skin1/myntra_js/ns.js"> </script>
<script language="javascript" src="/skin1/myntra_js/mk-base.js"> </script>
<script language="javascript" src="/skin1/myntra_js/lightbox.js"> </script>
<script type="text/javascript" src="/skin1/admin/js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="/skin1/admin/js/ckeditor/adapters/jquery.js"></script>
<script language="javascript" src="/skin1/myntra_js/jquery-ui-datepicker.js"> </script>
<script src='/skin1/admin/notifications/notifications.js'></script>
<script>Myntra.ntfFlag = "{$ntfFlag}";</script>