Ext.onReady(function() {	
	Ext.QuickTips.init();
	var myMask = new Ext.LoadMask(Ext.getBody(), {msg: "Loading..."});
	Ext.Ajax.on('beforerequest', myMask.show, myMask);
	Ext.Ajax.on('requestcomplete', myMask.hide, myMask);
	Ext.Ajax.on('requestexception', myMask.hide, myMask);
		
	var tabs = new Ext.TabPanel({
		renderTo: 'contentPanel',
		autoHeight : true,
		border : true,
		bodyStyle : 'padding: 5px',
		width : 1000,
		activeTab : 0,
		deferredRender: false,
		autoTabs : true,
		items : [
		{
			title: 'COH On-Hold Zip Codes Settings',
			id: 'zipCodesSettings',
			html : 'Upload the zip-codes and select price range to put COD orders on hold',
			bodyStyle : 'padding: 5px'
		},{
			title: 'First Purchase On-Hold Settings',
			id: 'firstPurchaseSettings',
			bodyStyle : 'padding: 5px'
		}]	 
	});
	
	var statesStore = new Ext.data.JsonStore({
		fields : [ 'code', 'state'],
		data : states
	});

	var statesElement = new Ext.ux.form.SuperBoxSelect({
		xtype : 'superboxselect',
		fieldLabel : 'Select States',
		emptyText : 'Select States',
		name : 'states',
		anchor : '100%',
		store : statesStore,
		mode : 'local',
		displayField : 'state',
		valueField : 'code',
		value: selectedStates,
		forceSelection : true
	});
	
	var zppmPanel = new Ext.form.FormPanel({
		renderTo : 'zipCodesSettings',
		id : 'zppmPanel'
	});
	
	var priceRangeStore = new Ext.data.JsonStore({
		fields : [ 'id', 'range' ],
		data : range
	});
	
	var zipCodesPricePointsPanel = new Ext.form.FormPanel({
		fileUpload: true,
		renderTo : 'zppmPanel',
		title : 'Zip Codes and Price Points Settings',
		id : 'pricePanel',
		autoHeight : true,
		collapsible : true,
		border : true,
		bodyStyle : 'padding: 5px',
		width : 1000,
		items : [{
			layout : 'table',
			border : false,
			layoutConfig : {columns : 1},
			defaults : {width : 800, border : false, 
				layout : 'form', bodyStyle : 'padding-left : 15px'},
			items : [{ 
				items: [{
		        	xtype : 'superboxselect',
		      		fieldLabel : 'Price Range',
		      		emptyText : 'Select Price Range',
		      		name : 'rangevalue',
		      		anchor : '100%',
		      		store : priceRangeStore,
		      		mode : 'local',
		      		displayField : 'range',
		      		valueField : 'range',
		      		listWidth : 670,
		      		forceSelection : true,
		      		allowBlank : false
	          	}]
			},{ 
				items: [{
		        	title: 'Upload the file containing Zipcodes in .csv format',
		     		frame: true,
		     		width: 700,
		            layout: 'table',
		            fieldLabel : 'Zipcodes',
		     		items: [{
		     			width : 250,
		     			items: [{
		     				xtype : 'fileuploadfield',
				     		name : 'zipcodes',
		     				allowBlank: false,
		     		        bodyPadding: '50 10 0',
		     				emptyText : 'Zipcodes',
		     				buttonText : ' Select a File '
		     			}]
		     		}, {
		     			buttons: [{
		     				text: 'Upload',
		     				handler: function() {
		     					var form = zipCodesPricePointsPanel.getForm();
		     					if(form.isValid()){
		     						form.findField('rvalue').setValue('');
		     						Ext.each(form.findField('rangevalue').getValue(),function(range,index){
		     							form.findField('rvalue').setValue(form.findField('rvalue').getValue()+range);
		     						});
		                             form.submit({
		                             	url : 'cod_onhold_settings.php',
		                                waitMsg: 'Uploading file...',
		                                success: function(form, action){
		                                	Ext.Msg.alert('Success', 'Data posted successfully', showResult);
		         						},
		         						failure: function(form, action){
		         							Ext.Msg.alert('Warning', 'Data not posted! '+action.result.message);
		         						}
		                             });
		                          	function showResult(btn){
		                           		window.location.reload();
                                    };
		     					}
		     				}
		     			}, {
		     			    text: 'Reset',
		     				handler: function() {
		     					zipCodesPricePointsPanel.getForm().reset();
		     				}
		     		    }]
		     		}]
	         	}] 
	        },{ 
	        	 items : [{
 					xtype : 'textfield',
					name : 'action',
					value : 'upload',
					hidden : true
				}]
			},{ 
	        	 items : [{
 					xtype : 'textfield',
					name : 'rvalue',
					hidden : true
				}]
			}]
		}]		
	});
	
	var displayRangeStore = new Ext.data.JsonStore({
		fields : ['displayRange'],
		data: displayRange
	});
	
	var zipcodeStore = new Ext.data.JsonStore({
		fields : ['zipcodesChosen'],
		data: zipcodesChosen
	});

	
	var codOHPointsPanel1 = new Ext.grid.GridPanel({
		renderTo : 'zppmPanel',
		title : 'All orders will be put on hold which belong to the following zipcodes and the price-range',
		id : 'displayPanel1',
		autoHeight : true,
		border : true,
		bodyStyle : 'padding: 5px',
		width : 1000,
		store : displayRangeStore,
		colModel : new Ext.grid.ColumnModel({
			columns : [{
				header: "Price Range Chosen", width : 900
			}]
		})
	});
	
	var codOHPointsPanel2 = new Ext.grid.GridPanel({
		renderTo : 'zppmPanel',
		id : 'displayPanel2',
		autoHeight : true,
		border : true,
		bodyStyle : 'padding: 5px',
		width : 1000,
		store : zipcodeStore,
		colModel : new Ext.grid.ColumnModel({
			columns : [{
				header: "Zipcodes Chosen", width : 900
			}]
		})
	});
	
	var searchPanel = new Ext.form.FormPanel({
		renderTo : 'firstPurchaseSettings',
		title : 'Search Panel',
		id : 'formPanel',
		autoHeight : true,
		collapsible : true,
		border : true,
		bodyStyle : 'padding: 5px',
		width : 1000,
		items : [{
			layout : 'table',
			border : false,
			layoutConfig : {columns : 1},
			defaults : {width : 800, border : false, 
				layout : 'form', bodyStyle : 'padding-left : 15px'},
			items : [
			          { items: [statesElement] }
			         // { items: [paymentMethodElement] }
			       ]
		}],
		
		buttons : [{
			text : 'Save Selection',
			handler : function() {
				submitSelection();
			}
		}]
	});
	
	function submitSelection() {
		var form = searchPanel.getForm();
		var params = {};
		params["action"] = "save";
		params["states"] = form.findField("states").getValue();
		//params["paymentMethods"] = form.findField("paymentMethods").getValue();
		
		Ext.Ajax.request({
			url : 'cod_onhold_settings.php',
			params : params,
		    success : function(response, action) {
				var jsonObj = Ext.util.JSON.decode(response.responseText);
				if(jsonObj[0] == "SUCCESS") {
					Ext.MessageBox.alert('success', 'Successfully Saved On Hold States selection');
				} else {
					Ext.MessageBox.alert('Error', 'Unable to save your selection. Please contact admin');
				}
			}
		});
	}
	
	var mainPanel = new Ext.Panel({
		renderTo : 'contentPanel',
		border : false,
	});   
	
});
