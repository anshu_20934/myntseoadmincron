<div id="newaddresspopup" style="display:none">
	<div class="overlay" style="display:block;_position:absolute;_width:1300px;_height:1500px;" onClick="javascript:closeNewAddressWindow();">&nbsp;</div>
	<div id="newaddresspopup-main" style="position:absolute; left: 20%; width:325px; background:white; z-index:1006;border:2px solid #999999;">
		<div id="newaddresspopup-content" style="display:none;">
		    <div id="newaddresspopup_title" class="popup_title">
		    	<div id="newaddresspopup_ajaxWindowTitle" class="popup_ajaxWindowTitle">Enter New Address Details</div>
		    	<div id="newaddresspopup_closeAjaxWindow" class="popup_closeAjaxWindow"><a class="no-decoration-link" href="javascript:closeNewAddressWindow();">Close</a></div>
		    </div>
			
			<div class="clear">&nbsp;</div>
			<div style="padding:5px 25px; text-align:left; font-size:12px" >
				<div id="newaddress-errorMsg" class="notification hide">We were not able to add the address. Please try later.</div>
				<div id="return-new-pickup-address" class="new-address-block">
					<div>
						<div class="label">Contact Person <span>*</span></div>
						<div class="field">
							<input type="text" name="new_name" id="new_name" value="{$address.name}">
						</div>
						<div class="clear"></div>
					</div>
					<div>
						<div class="label">Address <span>*</span></div>
						<div class="field">
							<textarea name="new_address" id="new_address"></textarea>
						</div>
						<div class="clear"></div>
					</div>
					<div>
						<div class="label">City <span>*</span></div>
						<div class="field">
							<input type="text" name="new_city" id="new_city" value="{$address.city}">
						</div>
						<div class="clear"></div>
					</div>
					<div>
						<div class="label">Country <span>*</span></div>
						<div class="field">
							<select name="new_country" id="new_country">
								{foreach key=countrycode item=countryname from=$countries}
									<option value="{$countrycode}" {if $address.country eq $countrycode}selected="selected"{/if}>{$countryname}</option>
								{/foreach}
							</select>
						</div>
						<div class="clear"></div>
					</div>
					<div>
						<div class="label">State <span>*</span></div>
						<div class="field">
							<div id="state">
								<select name="new_state_list" id="new_state_list">
									{section name="statename" loop=$states}
										<option value="{$states[statename].code}" {if $address.state eq $states[statename].code}selected="selected"{/if}>{$states[statename].state}</option>
									{/section}
								</select>
								<input type="text" value="" id="new_state" name="new_state" value="{$address.state}">
							</div>
						</div>
						<div class="clear"></div>
					</div>
					<div>
						<div class="label">Pin Code <span>*</span></div>
						<div class="field"><input type="text" onkeydown="return ( event.ctrlKey || event.altKey 
                                                    || (47<event.keyCode && event.keyCode<58 && event.shiftKey==false) 
                                                    || (95<event.keyCode && event.keyCode<106)
                                                    || (event.keyCode==8) || (event.keyCode==9) 
                                                    || (event.keyCode>34 && event.keyCode<40) 
                                                    || (event.keyCode==46) )" name="new_zipcode" id="new_zipcode" maxlength="6"></div>
						<div class="clear"></div>
					</div>
					<div>
						<div class="label">Email <span>*</span></div>
						<div class="field"><input type="text" name="new_email" id="new_email" value="{$address.email}"></div>
						<div class="clear"></div>
					</div>
					<div>
						<div class="label">Mobile <span>*</span></div>
						<div class="field"><input type="text" name="new_mobile" id="new_mobile" value="{$address.mobile}"></div>
					</div>
					<div>
						<div class="label">Telephone</div>
						<div class="field"><input type="text" name="new_phone" value="{$address.phone}"></div>
						<div class="clear"></div>
					</div>
					<div>
						<div class="button" style="text-align: right"><input type="button" name="save-address" id="save-address" value="Save and Use this Address" onclick="saveAddress();"></div>
					</div>
				</div>
			</div>
			<div class="clear">&nbsp;</div>
		</div>
	</div>
</div>

<div id="selectaddresspopup" style="display:none">
    <div class="overlay" style="display:block;_position:absolute;_width:1300px;_height:1500px;" onClick="javascript:closeSelectAddressWindow();">&nbsp;</div>
    <div id="selectaddresspopup-main" style="position:absolute; left: 20%; width:450px; background:white; z-index:1006;border:2px solid #999999;">
        <div id="selectaddresspopup-content" style="display:none;">
		    <div id="selectaddresspopup_title" class="popup_title">
		        <div id="selectaddresspopup_ajaxWindowTitle" class="popup_ajaxWindowTitle">Select Different Address</div>
		        <div id="selectaddresspopup_closeAjaxWindow" class="popup_closeAjaxWindow"><a class="no-decoration-link" href="javascript:closeSelectAddressWindow();">Close</a></div>
		    </div>
		
    		<div class="clear">&nbsp;</div>
	        <div style="padding:5px 25px; text-align:left; font-size:12px" >
	        	<div id="selectaddress-errorMsg" class="hide">We were not able to update your selection. Please try later.</div>
			    <div class="old-address-section">
			    	{if $allAddresses|@count != 0}
				        {section name=idx loop=$allAddresses}
				        	{if $allAddresses[idx]}
					        	<input type=hidden name="select-address-pickup" id="select-address-pickup-{$allAddresses[idx].id}" value="{$allAddresses[idx].pickup}">
							    <input type=radio name="selected-address" id="select-address-{$allAddresses[idx].id}" value="{$allAddresses[idx].id}" {if $allAddresses[idx].is_serviceable neq 1} disabled {/if}> Use this address
							    <div class="addressblock">
							        <div class="name setname">
								    	{$allAddresses[idx].name}
								    </div>
								    <div class="address">{$allAddresses[idx].address}</div>
								    <div class="city">{$allAddresses[idx].city} - {$allAddresses[idx].pincode}</div>
								    <div class="state">{$allAddresses[idx].state_display}, {$allAddresses[idx].country_display}</div>
								    <div class="mobile-no"><b>Mobile</b>: {$allAddresses[idx].mobile}</div>
								    <div class="email"><b>Email</b>: {$allAddresses[idx].email}</div>
								</div>
							    <div class="divider"></div>
						    {/if}
					    {/section}
					{ else }
						No other address available.
					{/if}
			    </div>
			    <div class="divider"></div>
				<div class="button">
					<a href="javascript:void(0);" id="newaddress-link" name="newaddress-link">Add New Address</a>
					<input type="button" class="orange-bg corners p5 right" name="use-address" id="use-address" value="Use Selected Address" onclick="selectAddress();">
				</div>
				 <div class="divider"></div>
			</div>
		</div>
	</div>
</div>
{literal}
<script type="text/javascript">
	function showNewAddressWindow(e) {
		$("#newaddresspopup").css("display","block");
		$("#newaddresspopup-content").css("display","block");
		$("#newaddresspopup_title").css("display","block");
		//$("#newaddresspopup-main").css("top", window.pageYOffset+50);
		/*$(window).scroll(function() {
			$("#newaddresspopup-main").css("top", window.pageYOffset+50);
		});*/
		var oThis = $("#newaddresspopup-main");
		var topposition = ( $(window).height() - oThis.height() ) / 2;
		if ( topposition < 10 ) 
			topposition = 10;
		oThis.css("top", ( topposition + $(window).scrollTop() + "px"));
		oThis.css("left", ( $(window).width() - oThis.width() ) / 2+$(window).scrollLeft() + "px");
	}

	function closeNewAddressWindow() {
		$("#newaddresspopup").css("display","none");
		$("#newaddresspopup-content").css("display","none");
		//$(window).unbind('scroll');
	}

	function saveAddress() {
		var name = $("#new_name").val();
		var address = $("#new_address").val();
		var city = $("#new_city").val();
		var country = $("#new_country").val();
		var state = "";
		if(country == 'IN') {
			state = $("#new_state_list").val();
		} else {
			state = $("#new_state").val();
		}
		var zipcode = $("#new_zipcode").val();
		var mobile = $("#new_mobile").val();
		var email = $("#new_email").val();
		var phone = $("#new_phone").val();

		if(name == "" || address=="" || city == "" || country == ""
			|| state == "" || zipcode == "" || mobile == "" || email == "") {
			alert("Please enter all address fields");
			return;
		} 

		var defaultAddressId =  $("#default-address-id").val();
		var customerLogin = $("#customer_login").val();
		
		$.ajax({ 
			type : 'POST', 
			url : 'return_item.php', 
			dataType : 'json', 
			data: { 'name':name, 'address': address, 'city':city, 'country':country, 'state':state,
					'zipcode' : zipcode, 'mobile':mobile,'email':email, 'phone': phone, 
					'defaultAddId': defaultAddressId, 'action': 'newaddress', 'customer_login': customerLogin },
			success: function(response){
				if(response[0] == 'SUCCESS') {
					closeNewAddressWindow();

					var baseUrl = window.location.href;
					var index = baseUrl.indexOf('&address');
					if(index != -1) {
						baseUrl = window.location.href.slice(0, index);
					}
					window.location.href = baseUrl+'&address='+response[1];
				} else {
					$("#newaddress-errorMsg").css("display", "block");
				}
			},
			error: function(xmlHttpRequest, textStatus, errorThrown){
				alert("Sorry internal error. "+textStatus);
			}
		});
	}

	function showSelectAddressWindow(e) {
		$("#selectaddresspopup").css("display","block");
		$("#selectaddresspopup-content").css("display","block");
		$("#selectaddresspopup_title").css("display","block");
		var oThis = $("#selectaddresspopup-main");
		var topposition = ( $(window).height() - oThis.height() ) / 2;
		if ( topposition < 10 ) 
			topposition = 10;
		oThis.css("top", ( topposition + $(window).scrollTop() + "px"));
		oThis.css("left", ( $(window).width() - oThis.width() ) / 2+$(window).scrollLeft() + "px");

		var selectedAddressId = $("#selected-address-id").val();
		var returnMode = $("input[name='return-method']:checked").val();
    	if(returnMode == "pickup") {
			if($("#select-address-pickup-"+selectedAddressId).val() == '1' ) {
				$("#select-address-"+selectedAddressId).attr("checked", true);
			}
		} else {
			$("#select-address-"+selectedAddressId).attr("checked", true);
		}
	}

	function closeSelectAddressWindow() {
		$("#selectaddresspopup").css("display","none");
		$("#selectaddresspopup-content").css("display","none");
		//$(window).unbind('scroll');
	}

	function selectAddress() {
		var addId = $("input:radio[name='selected-address']:checked").val();
		var selectedAddressId = $("#selected-address-id").val();

		if(addId && addId != "") {
			if(addId == selectedAddressId) {
				closeSelectAddressWindow();
				return;	
			}

			var baseUrl = window.location.href;
			var index = baseUrl.indexOf('&address');
			if(index != -1) {
				baseUrl = window.location.href.slice(0, index);
			}
			window.location.href = baseUrl+'&address='+addId;
		}else {
			alert("Please select an address to use");
			return;
		}
	}
	
</script>
{/literal}