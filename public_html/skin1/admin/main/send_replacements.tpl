<html>
<head>

   {literal}
	<script>
        function validateForm(){
            var added_by=document.getElementById("added_by");
            var reason=document.getElementById("replacement_reason");

            if(added_by.value == ''){
                alert("Please enter your name");
                return false;
            }
            if(reason.value == ''){
                alert("Please enter Replacement Reason");
                return false;
             }
            return true;
        }
		function validateQuantity(quantity,maxQuantity){
          if(quantity.value > maxQuantity){

		    alert("You cannot ask for a replacement of more than "+maxQuantity.toString()+" items");
		    quantity.focus(); // set the focus to this input
            quantity.value=maxQuantity;
		    return false;
	        }
	      return true;

        }
        function validateQuantitySplit(currQuantity,maxQuantity,itemid){
          var sizeArray=document.getElementsByName("quantity"+"_"+itemid+"[]");
          var sizeOptionArray=document.getElementsByName("quantity_option"+"_"+itemid+"[]");
          var total=0;
          for(var i=0;i<sizeArray.length;i++){
              total=total+parseInt(sizeArray[i].value);
          }
          if(total > maxQuantity){

		    alert("You cannot ask for a total replacement of more than "+maxQuantity.toString()+" items");
		    currQuantity.focus(); // set the focus to this input
            currQuantity.value=0;
		    return false;
	  	}

          var breakup = document.getElementById("breakup_"+itemid);
          breakup.value="";
          for(var i in sizeArray){
             if(sizeOptionArray[i].value !== undefined) {
             breakup.value=breakup.value+sizeOptionArray[i].value+":"+sizeArray[i].value;
             if(i < sizeArray.length-1) 
            	 breakup.value=breakup.value+",";
             }
             
          }
          var totQuantity = document.getElementById("text_"+itemid);
          totQuantity.value=parseInt(total);
          return true;

        }
	</script>
	{/literal}

    <base href="{$http_location}/">
	<title>Send Replacements</title>
	<link rel="stylesheet" href="{$SkinDir}/skin1_admin.css" />
	<script type="text/javascript" src="{$SkinDir}/js_script/calender.js" ></script>
	<link rel="stylesheet" href="{$SkinDir}/css/calender.css" type="text/css" media="screen" />
</head>
<body align="center" style="margin-left:60px;">
<br><br>
{capture name=dialog}

{if $mode eq "success" }
          <div style="float:left;width:300px;height:20px;font-size:1.0em;color:#04BB2E;">
				<strong>Replacemente data added. New order {$neworderid} created </strong>
			</div>
{elseif $mode eq "failure" }
          <div style="float:left;width:300px;height:20px;font-size:1.0em;color:#04BB2E;">
				<strong>Replacement could not be created. This order is probably not yet shipped.. </strong>
			</div>

{elseif $mode eq "getData"}

{if $payment_method == 'cod'} 
	<div style="color:green;text-weight:bold;font-size:12px;">
		<b> IT is a Cash-On-Delivery Order</b><br/> 
		Order Tracking No :: <b>{$awbno}</b>
		Order Deliver Status ::<b>{if $delivery_status == 'DL'} Delivered {else} Not Delivered {/if}</b>
		{if $delivery_status != 'DL'}
		<div style="color:red;text-weight:bold;font-size:12px;"> 
			WARNING:: Are you sure you want to send replacement without cash collection?
		</div> 
		{/if}
 	</div>
{/if}
<form name="send_replacements" action="{$http_location}/admin/send_replacements.php" onsubmit="return validateForm();" method="post">
<input type="hidden" name="mode" id="mode" value="replace"  />
<input type="hidden" name="userid" id="userid" value="{$smarty.get.userid}" />
<input type="hidden" name="orderid" id="orderid" value="{$smarty.get.orderid}" />

	<br/>
	<table width='95%' class='formtable' cellpadding='2' cellspacing='2' style='border: 1px solid #999999;'>
		{if $smarty.get.d == 'N'}
			<div style="float:left;width:300px;height:20px;font-size:1.0em;color:#D2061B;">
				<strong>Replacement could not be added.Please check if the quanity is appropriate for the given item</strong>
			</div>
		{elseif $smarty.get.d == 'I'}
			<div style="float:left;width:300px;height:20px;font-size:1.0em;color:#04BB2E;">
				<strong>Replacement data added. New order <a href="http://localhost/myntra/admin/order.php?orderid={$smarty.get.neworderid}"></a> created </strong>
			</div>
		{else}
		<tr style="background-color:#91DDE6;">
			<td width='95%' align="left" colspan='2'><p><strong>Send Replacements</strong></p></td>
		</tr>
		<!--tr class='TableSubHead'>
			<td width='20%' class="SubmitBox" align="right" valign="top"><p><strong>Replacement Date</strong></p></td>
			<td width='75%' class="SubmitBox" align="left"><p><input type="text" name="replacement_date" id="replacement_date" value="{$replacement_date}"><img src="{$cdn_base}/images/cal.gif" width="16" height="16" border="0" alt="Click Here to Pick up the date" onclick="displayDatePicker('replacement_date');"></p></td>
		</tr-->
		<tr class='TableSubHead'>
			<td width='20%' class="SubmitBox" align="right" valign="top"><p><strong>Name</strong></p></td>
			<td width='75%' class="SubmitBox" align="left"><p><input type="text" name="added_by" id="added_by" value=""> </p></td>
		</tr>

		<tr class='TableSubHead'>
			<td width='20%' class="SubmitBox" align="right" valign="top"><b>Replacement Items : </b></td>
			<td>


                  <TABLE width='95%' class='formtable' cellpadding='2' cellspacing='2' style='border: 1px solid #999999;'>
                  {section name=item loop=$item_hash}
                  <tr>
                      <td> <img src="{$item_hash[item].image}" width='100' /> </td>
                      <td> {$item_hash[item].item_id} </td>
                      <td> {$item_hash[item].item_name} </td>
                      <input type="hidden" name="quantity_breakup[]" id="breakup_{$item_hash[item].item_id}" value="" >
                      <input type="hidden" name="itemid[]"  value={$item_hash[item].item_id}>
                      <td></td>
		</tr>
		<tr>
                      <td colspan=3> Quantity  <input type="text" size=4 name="quantity[]" id="text_{$item_hash[item].item_id}" value=0  {if $item_hash[item].quantity_breakup eq ""}  {else} readonly="true" {/if} onBlur="validateQuantity(this,{$item_hash[item].item_count}) ">
                      
                      {if $item_hash[item].quantity_breakup eq ""}
                      {else}                      
    	    	          {assign var=itemid value=$item_hash[item].item_id}
    	    	          {assign var=options value=$size_options[$itemid]}
                          {section name=optid loop=$options}
                            {$options[optid].value} <input type="text" size=4 name="quantity_{$item_hash[item].item_id}[]" id="text_option_{$item_hash[item].item_id}[]" value=0 onBlur="validateQuantitySplit(this,{$item_hash[item].item_count},{$item_hash[item].item_id})">
                            <input type="hidden" size=4 name="quantity_option_{$item_hash[item].item_id}[]" id="text_option_{$item_hash[item].item_id}[]" value="{$options[optid].value}" >
                            
                          {/section}
                      {/if}
                      </td>
                  </tr>
                   {/section}
                  </TABLE>

           </td>
		</tr>

		<tr class='TableSubHead'>
			<td width='20%' class="SubmitBox" align="right" valign="top"><p><strong>Replacement Reason</strong></p></td>
			<td width='75%' class="SubmitBox" align="left"><p><TEXTAREA NAME="replacement_reason"  ID="replacement_reason" ROWS="2" COLS="50"></TEXTAREA></p></td>
		</tr>

		<tr class='TableSubHead'>
			<td width='95%' class="SubmitBox" align="center" colspan='2'><p>
				<input type="submit" value="Send Replacement" name="send_replacement" class="submit"  /></p>
			</td>
		</tr>

		{/if}
	 </table>
  </form>
{else}
			<div style="float:left;width:300px;height:20px;font-size:1.0em;color:#D2061B;">
				<strong>Replacement could not be added.Please check if the quanity is appropriate for the given orderid</strong>
			</div>
{/if}
{/capture}
{include file="dialog.tpl" title="Replacement confirmation" content=$smarty.capture.dialog extra='width="90%"'}
</body>
</html>
