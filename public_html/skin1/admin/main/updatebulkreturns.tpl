{* $Id: featured_products.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
{include file="page_title.tpl" title='Update Returns'}

{capture name=dialog}
{if $action eq "upload"}
	<form action="" method="post" name="bulkreturnsaction" id="bulkreturnsaction" enctype="multipart/form-data">
		{if $message}<div style="font-weight: bold; font-size: 13px; color: green;"><br>{$message}<br><br></div>{/if}
		{if $errormessage}<div style="font-weight: bold; font-size: 13px; color: red;"><br>{$errormessage}<br><br></div>{/if}
		<table>
			<tr>
				<td>Courier Service:</td>
				<td>
					<select name="courier">
						<option value="0">Select Courier</option>
						{section name=idx loop=$couriers}
							<option value="{$couriers[idx].code}">{$couriers[idx].display_name}</option>
						{/section}
					</select>
				</td>
			</tr>
			<tr>
				<td>Update Status To:</td>
				<td>
					<select name="status" id="status" style="width: 150px;float:left" onchange="javascript:showFormat(this.value);">
						<option value="">Select Status</option>
						<option value="RPI">Initiate Pickup</option>
						<option value="RPI2">Re Initiate Pickup</option>
                                                <option value="RPI3">Re Initiate Pickup 3rd Time</option>
						<option value="RPU">Return Picked-Up</option>
						<option value="RPF">Return Pickup Failed</option>
						<option value="RPF2">Return Pickup Failed Again</option>
                                                <option value="RPF3">Return Pickup Failed 3rd Time</option>
					</select>
					<div id = "formatDiv" style="margin-left:20px;float:left;color:red"> </div>
				</td>
			</tr>
			<tr>
				<td>Upload the xls file:</td>
				<td><input type="file" name="returnsdetail" size=50></td>
			</tr>
			<tr>
				<td colspan=2 align=right><input type='submit' name='uploadxls' value="Upload"></td>
			</tr>
		</table>
		<input type='hidden' name='action' value='verify'>
		<input type='hidden' name='login' value='{$login}'>
	</form>
{else}
      <form name='frmconfirm'  action=""  method="post"  >
      	<input type='hidden' name='action' value='confirmupload'>
      	<input type='hidden' name='login' value='{$login}'>
      	<input type='hidden' name='courier' value='{$courier}'>
      	<input type='hidden' name='filetoread' value='{$uploadreturnsFile}'>
      	<input type='hidden' name='status' value='{$status}'>
      	<table cellpadding="2" cellspacing="1" width="100%">
	    	<tr class="TableHead">
				<td>Return No</td>
				<td>Tracking No</td>
				{if $status == 'RPI' or $status == 'RPI2' or $status == 'RPI3'}
					<td>Token No</td>
				{/if}
				<td>Remarks</td>
				<td>Courier Service</td>
	     	</tr>
         	{section name=idx loop=$returnListDisplay}
		  		<tr{cycle values=", class='TableSubHead'"}>
					<td align="center">{$returnListDisplay[idx].returnid}</td>
					<td align="left" >{$returnListDisplay[idx].trackingNo}</td>
					{if $status == 'RPI' or $status == 'RPI2' or $status == 'RPI3' }
						<td align="left" >{$returnListDisplay[idx].tokenNo}</td>
					{/if}
					<td align="left" >{$returnListDisplay[idx].remarks}</td>
					<td align="left" >{$returnListDisplay[idx].courier}</td>
		    	</tr>
         	{/section}
	     	<tr class="TableSubHead">
				<td >&nbsp;</td>
				<td >&nbsp;</td>
				{if $status == 'RPI' or $status == 'RPI2' or $status == 'RPI3' }
					<td >&nbsp;</td>
				{/if}
				<td >&nbsp;</td>
				<td><input type='submit' name='confirm'  value='confirm' /></td>
	     	</tr>
	 	</table>
	 </form>
{/if}

{/capture}
{include file="dialog.tpl" title='Update Returns' content=$smarty.capture.dialog extra='width="100%"'}

{literal}
	<script type="text/javascript">
		function showFormat(status){
			if(status == 'RPI' || status == 'RPI2' || status == 'RPI3'){
				$("#formatDiv").html("xls must have 2 columns.first column must have header \"Return No\" and second column must have header \"Token No\".");
			}
			if(status == 'RPU'){
				$("#formatDiv").html("xls must have 2 columns.first column must have header \"Return No\" and second column must have header \"Tracking No\".");
			}
			if(status == 'RPF' || status == 'RPF2' || status == 'RPF3'){
				$("#formatDiv").html("xls must have 2 columns.first column must have header \"Return No\" and second column must have header \"Remarks\".");
			}
			if(status == ''){
				$("#formatDiv").html("");
			}
		}
	</script>
{/literal}

