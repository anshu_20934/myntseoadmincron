{* $Id: featured_products.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
{include file="main/include_js.tpl" src="main/popup_product.js"}

<link rel="stylesheet" href="{$SkinDir}/skin1_admin.css" />
<script language="javascript" src="../skin1/js_script/item_assignment.js"></script>

{capture name=dialog}

<form name="location_task" action="assigned_items_report.php" method="post">
	<input type="hidden" name="mode" id="mode" value="search">
		<div class="ITEM_PARENT_DIV" style="width:650px;">
		<div class="ITEM_FIRST_CHILD_DIV" style="width:auto;">
        <div class="ITEM_LABLE_DIV">Location</div>
        <div class="ITEM_FIELD_DIV" style="width:auto;">
            <select name="item_location" style="width:140px;" onchange="javascript:document.location_task.submit();">
                <option value="0">---select location---</option>
				{section name=loc loop=$locations}
				   <option value="{$locations[loc].id}" {if $smarty.post.item_location == $locations[loc].id || $smarty.get.item_location == $locations[loc].id} selected {/if}>{$locations[loc].location_name}</option>
				   {assign var=loc_selected value=""}
				{/section}
		    </select>
        </div>
        </div>
        </div>
</form>

{/capture}
{include file="dialog.tpl" title="Select Location" content=$smarty.capture.dialog extra='width="100%"'}

<br/><br/>

{if $locationid != ""}
{capture name=dialog}
<div style="float:left;width:auto;height:auto;border : 0px solid #000000;">
{include file="customer/main/navigation.tpl"}
</div>
    <form action="" method="post" name="">
        <input type="hidden" name="mode"  />
        <input type="hidden" name="page" id="page" value="{$smarty.get.page}"  />
         <input type="hidden" name="query_string" id="query_string" value="{$query_string}"  />
        <div class="DIV_TABLE" style="width:600px;float:left;">
	
        <div class="DIV_TH" style="width:600px;float:left;">
            <div class="DIV_TD" style="width:165px;">Product Type</div>
            <div class="DIV_TD" style="width:170px;">Product Style</div>
            <div class="DIV_TD" style="width:85px;">Quantity Ordered</div>
        </div>

        {if $total_items > 0 }

        {section name=item loop=$itemResult max=$sizeofitemresult}
         <div class="DIV_TR" style="width:600px;float:left;">
            <div class="DIV_TR_TD" style="width:165px;"><p>{$itemResult[item].p_type}</p></div>
            <div class="DIV_TR_TD" style="width:170px;"><p>{$itemResult[item].p_style}</p></div>
            <div class="DIV_TR_TD" style="width:85px;"><p>{$itemResult[item].total_quantity}</p></div>
            
        {/section}
        
        {else}
        <div class="DIV_TR" style="width:600px;float:left;">
            <div class="DIV_TR_TD" style="width:750px;"><strong>task has not been assigned</strong></div>
        </div>
        {/if}

        </div>
    </form>

 <div style="float:left;width:auto;height:auto;border : 0px solid #000000;">
{include file="customer/main/navigation.tpl"}
</div>
{/capture}
{include file="dialog.tpl" title="Location task list" content=$smarty.capture.dialog extra='width="100%"'}
{/if}
