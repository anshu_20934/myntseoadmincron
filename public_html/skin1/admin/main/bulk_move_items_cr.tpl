<script type="text/javascript">
    var progressKey = '{$progresskey}';
</script>
<link rel="stylesheet" href="{$SkinDir}/skin1_admin_operations.css" />
{literal}
<script type="text/javascript">
    function showUploadStatusWindow() {
        $("#uploadstatuspopup").css("display","block");
        $("#uploadstatuspopup-content").css("display","block");
        $("#uploadstatuspopup_title").css("display","block");

        var oThis = $("#uploadstatuspopup-main");
        var topposition = ( $(window).height() - oThis.height() ) / 2;
        if ( topposition < 10 )
            topposition = 10;
        oThis.css("top", ( topposition + $(window).scrollTop() + "px"));
        oThis.css("left", ( $(window).width() - oThis.width() ) / 2+$(window).scrollLeft() + "px");

        setInterval(trackStatus, 1000);

        return true;
    }

    function trackStatus() {
        $.get("getprogressinfo.php?progresskey="+progressKey,
            function(percent) {
                if(percent != "false") {
                    $("#progress_bar").css('width', percent+"%");
                    $("#progress_text").html(percent);
                }
            }
        );
    }
</script>
{/literal}

{if $action eq "upload"}
    <div style="font-weight: bold; font-size: 13px; margin-bottom: 30px; color:#9F6000; border: 1px solid; padding: 5px; text-align: center"><b>IMPORTANT: Make sure Items are in proper status.</b></div>
    {if $errorMsg } <div style="font-weight: bold; color:red; padding: 5px;">{$errorMsg}</div><br><br><br> {/if}
    <form action="" method="post" name="bulkitemaction" enctype="multipart/form-data">
        <input type='hidden' name='action' value='verify'>
  <b>Present Item Status:</b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="radio" name="currentstatus" value="A_R" checked>Accepted Returns &nbsp;&nbsp;&nbsp;&nbsp;
        <input type="radio" name="currentstatus" value="S">Shipped<br>
      <b>Items to be moved to Warehouse ID:</b> <input type="number" name="warehouseid" value="8"><br>
        <table cellpadding="1" cellspacing="1" width="750">
            <tr>
                <td>
                   Upload the csv file <br>(with item barcodes in first column):<br>
                </td>
                <td><input type="file" name="move_items_to_cr" size=50>{if $message}<font color='red'>{$message}</font>{/if}</td>
            </tr>
            <tr>
                <td colspan=2 align=right><input type='submit'  name='uploadxls'  value="Upload"></td>
            </tr>
        </table>
    </form>
    {if $success_info } Detail Info: <div style="font-weight: bold; color: green; padding: 0 5px;">{$success_info}</div> {/if}
    {if $error_info } Detail Info: <div style="font-weight: bold; color: red; padding: 0 5px;">{$error_info}</div> {/if}
{else}
    <form name='frmconfirm'  action=""  method="post" onsubmit="return showUploadStatusWindow();">
        <input type='hidden' name='action' value='confirm'>
        <input type="hidden" name="currentstatus" value="{$currentstatus}">
        <input type="hidden" name="warehouseid" value="{$warehouseid}">
        <input type='hidden' name='filetoread' value='{$uploadorderFile}'>
        <input type='hidden' name='progresskey' value='{$progresskey}'>

        <div style="font-weight: bold; color: green; padding: 0 5px;">Status: {if $currentstatus == 'A_R'}Accepted Returns{/if}{if $currentstatus == 'S'}Shipped{/if}</div>
        <div style="font-weight: bold; color: green; padding: 0 5px;">Warehouse ID: {$warehouseid}</div>

        <table cellpadding="2" cellspacing="1" width="100%">
            <tr{cycle values=", class='TableSubHead'"}>
                <td align="center">Following Items will be moved - {$itemsTobeMoved}</td>
            </tr>
            <tr class="TableSubHead">
                <td >&nbsp;</td>
                <td><input type='submit' name='confirm'  value='confirm' /></td>
                <td>&nbsp;</td>
            </tr>
        </table>
    </form>
    <div id="uploadstatuspopup" style="display:none">
        <div class="overlay" style="display:block;_position:absolute;_width:1300px;_height:1500px;">&nbsp;</div>
        <div id="uploadstatuspopup-main" style="position:absolute; left: 20%; width:450px; background:white; z-index:1006;border:2px solid #999999;">
            <div id="uploadstatuspopup-content" style="display:none;">
                <div id="uploadstatuspopup_title" class="popup_title">
                    <div id="uploadstatuspopup_ajaxWindowTitle" class="popup_ajaxWindowTitle">Upload Status</div>
                    <div class="clear">&nbsp;</div>
                </div>
                <div style="padding:5px 25px; text-align:left; font-size:12px" >
                    <div id="progress_container">
                        <div id="progress_bar">
                            <div id="progress_completed"></div>
                        </div>
                    </div>
                    <span id="progress_text" style="font-size: 14px; color: green; padding: 0 0 0 5px;">0</span> % Complete
                </div>
                <div class="clear">&nbsp;</div>
            </div>
        </div>
    </div>
{/if}