{literal}
<script type="text/javascript">
function func_delete_rule(elem){
    var row = $(elem).closest('tr');
    var id = row.find('input').first();
    var r=confirm("Are you sure you want to delete : "+ id.val());
    if(r == true){
        row.remove();
        document.update.mode.value = 'save';
        document.update.submit();
    }
}

function func_add_rule(){
    if ( !document.update.add_device || document.update.add_device.value == '' ||
         !document.update.add_browser || document.update.add_browser.value == '' ){
        alert('Key is empty');
        return;
    } 
    document.update.mode.value = 'add';
    document.update.submit();
}

function func_remove_all(){
    var r=confirm("Are you sure you want to remove all rules");
    if(r == true){
        $(document.update).find('table').remove();
        document.update.mode.value = 'save';
        document.update.submit();
    }
}

</script>
{/literal}

{capture name=dialog}
<form action="mobileUAWhitelist.php" method="post" name="update">
<input type="hidden" name="mode" />
<hr>
<label>The current mobile detection plugin is based on <a href="http://mobiledetect.net/">PHP Mobile Detect</a>. Please refer to the <a href="http://demo.mobiledetect.net/">link</a> and find out the exact string to the put in the input below. Also refer to this <a href="http://www.useragentstring.com/pages/Mobile%20Browserlist/">link</a> for a complete list of mobile user agent strings</label>
<br>
<label>No rules present means that mobile view is enabled for All "mobile" devices</label>
<hr>
<table cellpadding="3" cellspacing="1" width="100%">
<tr class="TableHead">
    <td width="30%">OS/Device Name(s) (Comma separated)<br><hr> Case insensitive strings e.g. ios,androidos,windowsmobileos,iphone,dell etc. <br> Put "all" if you want to match all devices/os <br>NOTE: No spaces between two entries</td>
    <td width="30%">Browsers (Comma separated)<br><hr> Case insensitive strings e.g. opera,chrome,safari,ie,dolfin etc. <br> Put "all" if you want to match all browsers <br> NOTE: No spaces between two entries</td>
    <td width="20%">Action</td>
</tr>

{if $data}

{foreach key=device item=browser from=$data name=data}
<tr{cycle values=", class='TableSubHead'"}>
        
    <td align="center" style="left-padding:30px"><input name="device[{$smarty.foreach.data.index}]" type="text" value="{$device}"  maxlength=50/></td>
    <td align="center" style="left-padding:30px"><input name="browser[{$smarty.foreach.data.index}]" class="" type="text" value="{$browser}" {if $enabled}checked="checked"{/if} /></td>
    <td align="center">
        <input type="button" value="Delete" onclick="javascript: func_delete_rule(this)" />
    </td>    
</tr>
{/foreach}
{else}
<tr>
<td colspan="2" align="center">No Whitelist defined yet</td>
</tr>
{/if}
<tr class='TableSubHead' style="background-color:blue">
    <td align="center" style="left-padding:30px"><input name="add_device" type="text" value=""  maxlength=255/></td>
    <td align="center" style="left-padding:30px"><input name="add_browser" type="text" value="" maxlength=255/></td>
    <td align="center">
        <input type="button" value="Add" onclick="javascript: func_add_rule()" />
    </td>
</tr>
<tr>
    <td></td>
    <td></td>
    <td></td>
    <td class="SubmitBox" align="right">
       <input type="button" value="Remove All" onclick="javascript: func_remove_all();" />
       <input type="button" value="Update" onclick="javascript: document.update.mode.value = 'save'; document.update.submit();" />
    </td>
    <td></td>
</tr>
</table>
</form>

{/capture}
{include file="dialog.tpl" title="Mobile UA Whitelist" content=$smarty.capture.dialog extra='width="100%"'}
