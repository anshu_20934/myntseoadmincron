{* $Id: courier_serviceability.tpl,v 1.83.2.6 2006/06/27 08:08:06 max Exp $ *}
{include file="page_title.tpl" title='Courier Serviceability'}
<br><br>
{capture name=dialog}
	<form action="" method="post" name="zipcodelookup" id="zipcodelookup" enctype="multipart/form-data">
		<input type=hidden name="action" value="zipcodelookup">
		Enter Zipcode:  <input type="text" name="zipcode" value="{$zipcode}" id="zipcode" style="width: 100px; border: 1px solid #CCCCCC; padding: 5px;"> &nbsp; &nbsp;
		<b style="color: gray">OR</b> &nbsp; &nbsp;
		Enter Order ID:  <input type="text" name="orderId" value="{$orderId}" id="orderId" style="width: 100px; border: 1px solid #CCCCCC; padding: 5px;"> &nbsp; &nbsp;
		<b style="color: gray">AND</b> &nbsp; &nbsp;
		Enter Style ID:  <input type="text" name="styleId" value="{$styleId}" id="styleId" style="width: 100px; border: 1px solid #CCCCCC; padding: 5px;"> &nbsp; &nbsp;
		
		<input type="submit" name="zipcodelookup-btn" value="Search Serviceability" style="padding:5px;">		
	</form>
	
	<br><br>	
	{if $whId2SupportedCouriers }
		<table width=100% cellspacing=0 cellpadding=0 border=0>
			<tr height=30 style="background-color: #ccffff">
				<th style="font-size:14px; padding: 5px;">&nbsp;</th>
				<th style="font-size:14px; padding: 5px;">Courier Name</th>
				<th style="font-size:14px; padding: 5px;">Prepaid</th>
				<th style="font-size:14px; padding: 5px;">CoD</th>
				<th style="font-size:14px; padding: 5px;">Pickup</th>
				<th style="font-size:14px; padding: 5px;">Exchange</th>
				<th style="font-size:14px; padding: 5px;">Fragile</th>
				<th style="font-size:14px; padding: 5px;">Hazmat</th>
				<th style="font-size:14px; padding: 5px;">Jewellery</th>
                                <th style="font-size:14px; padding: 5px;">Next day delivery</th>
				<th style="font-size:14px; padding: 5px;">Cod Limit</th>
			</tr>
			{foreach from=$whId2SupportedCouriers key=whId item=supported_couriers}
				<tr>
					<td colspan="9">{$whId}</td>
				</tr>
				{section name=idx loop=$supported_couriers }
					<tr bgcolor="{cycle values="#eeddff,#d0ddff"}">
						<td style="font-size:12px; padding: 5px;" align=center>&nbsp;</td>
						<td style="font-size:12px; padding: 5px;" align=center>{$supported_couriers[idx].courierName}</td>
						<td style="font-size:12px; padding: 5px;" align=center>
							{if $supported_couriers[idx].nonCodSupported eq 1 }
								<img src="/skin1/mkimages/check_valid.gif">
							{else}
								<img src="/skin1/mkimages/unchecked.gif">
							{/if}
						</td>
						<td style="font-size:12px; padding: 5px;" align=center>
							{if $supported_couriers[idx].codSupported eq 1 }
								<img src="/skin1/mkimages/check_valid.gif">
							{else}
								<img src="/skin1/mkimages/unchecked.gif">
							{/if}
						</td>
						<td style="font-size:12px; padding: 5px;" align=center>
							{if $supported_couriers[idx].pickupSupported eq 1 }
								<img src="/skin1/mkimages/check_valid.gif">
							{else}
								<img src="/skin1/mkimages/unchecked.gif">
							{/if}
						</td>
						<td style="font-size:12px; padding: 5px;" align=center>
							{if $supported_couriers[idx].exchangeSupported eq 1 }
								<img src="/skin1/mkimages/check_valid.gif">
							{else}
								<img src="/skin1/mkimages/unchecked.gif">
							{/if}
						</td>
						<td style="font-size:12px; padding: 5px;" align=center>
							{if $supported_couriers[idx].fragileSupported eq 1 }
								<img src="/skin1/mkimages/check_valid.gif">
							{else}
								<img src="/skin1/mkimages/unchecked.gif">
							{/if}
						</td>
						<td style="font-size:12px; padding: 5px;" align=center>
							{if $supported_couriers[idx].hazmatSupported eq 1 }
								<img src="/skin1/mkimages/check_valid.gif">
							{else}
								<img src="/skin1/mkimages/unchecked.gif">
							{/if}
						</td>
						<td style="font-size:12px; padding: 5px;" align=center>
							{if $supported_couriers[idx].jewellerySupported eq 1 }
								<img src="/skin1/mkimages/check_valid.gif">
							{else}
								<img src="/skin1/mkimages/unchecked.gif">
							{/if}
						</td>
                                                <td style="font-size:12px; padding: 5px;" align=center>
							{if $supported_couriers[idx].expressSupported eq 1 }
								<img src="/skin1/mkimages/check_valid.gif">
							{else}
								<img src="/skin1/mkimages/unchecked.gif">
							{/if}
						</td>
						<td style="font-size:12px; padding: 5px;" align=center>
							{$supported_couriers[idx].codLimit}
						</td>
					</tr>
				{/section}
				<tr>
					<td colspan="9">&nbsp;</td>
				</tr>			
			{/foreach}
		</table>
	{else}
		No serviceable couriers found
	{/if}
{/capture}
{include file="dialog.tpl" title='Zipcode Serviceability Lookup' content=$smarty.capture.dialog extra='width="100%"'}