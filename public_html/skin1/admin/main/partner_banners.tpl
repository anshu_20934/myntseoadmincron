{* $Id: partner_banners.tpl,v 1.18.2.2 2006/07/11 08:39:26 svowl Exp $ *}
{include file="page_title.tpl" title=$lng.lbl_banners_management}
{if $banner_type eq ''}
{$lng.txt_banners_note}
{elseif $banner_type eq 'T'}
{$lng.txt_banners_text_link_note}
{elseif $banner_type eq 'G'}
{$lng.txt_banners_graphic_banner_note}
{elseif $banner_type eq 'M'}
{$lng.txt_banners_media_rich_banner_note}
{elseif $banner_type eq 'P'}
{$lng.txt_banners_product_link_note}
{/if}
<br /><br />

<!-- IN THIS SECTION -->

{include file="dialog_tools.tpl"}

<!-- IN THIS SECTION -->

<br />
{if $config.XAffiliate.display_as_iframe eq 'Y'}{assign var="local_type" value="iframe"}{else}{assign var="local_type" value="js"}{/if}

{if $banners ne "" && $banner_type eq ''}
{capture name=dialog}
<table cellspacing="1" cellpadding="2">
{foreach from=$banners item=v}
<tr>
	<td><b>{$lng.lbl_banner}:</b>&nbsp;{$v.banner}&nbsp;<i>({if $v.banner_type eq 'T'}{$lng.lbl_text_link}{elseif $v.banner_type eq 'G'}{$lng.lbl_graphic_banner}{elseif $v.banner_type eq 'M'}{$lng.lbl_media_rich_banner}{else}{$lng.lbl_product_banner}{/if})</i><br /><br />
	<table cellspacing="1" cellpadding="0" border="0" bgcolor="#000000">
	<tr bgcolor="#ffffff">
		<td>{include file="main/display_banner.tpl" assign="ban" banner=$v type=$local_type partner=''}{$ban|amp}</td>
	</tr>
	</table>
	</td>
</tr>
<tr>
	<td>
	<input type="button" onclick="javascript: self.location='partner_banners.php?bannerid={$v.bannerid}&amp;mode=delete';" value="{$lng.lbl_delete|strip_tags:false|escape}" />&nbsp;
	<input type="button" onclick="javascript: self.location='partner_banners.php?bannerid={$v.bannerid}';" value="{$lng.lbl_modify|strip_tags:false|escape}" />
	<br /><br />&nbsp;
	</td>
</tr>
{/foreach}
</table>
{/capture}
{include file="dialog.tpl" content=$smarty.capture.dialog title=$lng.lbl_available_banners extra='width="100%"'}

{elseif $banner_type ne ''}
{if $banner ne ''}
<br />

{capture name=dialog}
<table cellspacing="1" cellpadding="0" bgcolor="#000000">
<tr bgcolor="#ffffff">
	<td>{include file="main/display_banner.tpl" assign="ban" banner=$banner type=$local_type partner=''}{$ban|amp}</td>
</tr>
</table>
{/capture}
{include file="dialog.tpl" content=$smarty.capture.dialog title=$lng.lbl_edited_banner extra='width="100%"'}
{/if}

<br />

{capture name=dialog}
<form action="partner_banners.php" method="post" enctype="multipart/form-data">
<input type="hidden" name="mode" value="add" />
<input type="hidden" name="banner_type" value="{$banner_type}" />
<input type="hidden" name="bannerid" value="{$banner.bannerid}" />

<table cellpadding="0" cellspacing="3" width="100%">
<tr>
    <td>{$lng.lbl_banner_name}:</td>
    <td><input type="text" maxlength="128" size="40" name="add[banner]" value="{$banner.banner}" /></td>
</tr>
<tr>
    <td>{$lng.lbl_banner_width}:</td>
    <td><input type="text" size="5" value="{$banner.banner_x}" name="add[banner_x]" /></td>
</tr>
<tr>
    <td>{$lng.lbl_banner_height}:</td>
    <td><input type="text" size="5" value="{$banner.banner_y}" name="add[banner_y]" /></td>
</tr>
<tr>
    <td>{$lng.lbl_availability}:</td>
    <td><input type="checkbox" value="Y" name="add[avail]"{if $banner.avail eq 'Y' || $banner.bannerid eq ''} checked="checked"{/if} /></td>
</tr>
<tr>
    <td>{$lng.lbl_open_in_new_window}:</td>
    <td><input type="checkbox" value="Y" name="add[open_blank]"{if $banner.open_blank eq 'Y' || $banner.bannerid eq ''} checked="checked"{/if} /></td>
</tr>
{if $banner_type eq 'T'}
<tr>
    <td>{$lng.lbl_text}:</td>
    <td><textarea cols="50" rows="3" name="add[body]">{$banner.body}</textarea></td>
</tr>
{elseif $banner_type eq 'G'}
<tr>
    <td>{$lng.lbl_text} ({$lng.lbl_optional}):</td>
    <td><textarea cols="50" rows="3" name="add[legend]">{$banner.legend}</textarea></td>
</tr> 
<tr>
    <td>{$lng.lbl_alt_tag} ({$lng.lbl_optional}):</td> 
    <td><textarea cols="50" rows="3" name="add[alt]">{$banner.alt}</textarea></td>
</tr>  
<tr>
    <td>{$lng.lbl_text_location}:</td>
    <td><select name="add[direction]">
    <option value="U"{if $banner.direction eq 'U' || $banner.direction eq ''} selected="selected"{/if}>{$lng.lbl_above}</option>
    <option value="L"{if $banner.direction eq 'L'} selected="selected"{/if}>{$lng.lbl_left}</option>
    <option value="R"{if $banner.direction eq 'R'} selected="selected"{/if}>{$lng.lbl_right}</option>
	<option value="D"{if $banner.direction eq 'D'} selected="selected"{/if}>{$lng.lbl_below}</option>
	</select></td>
</tr>  
<tr>
    <td>{$lng.lbl_image}:</td> 
    <td><input type="file" name="userfile" /></td>
</tr>  
{elseif $banner_type eq 'P'}
<tr>
    <td>{$lng.lbl_picture}:</td>
    <td><input type="checkbox" name="add[is_image]" value='Y'{if $banner.is_image eq 'Y'} checked="checked"{/if} /></td>
</tr>
<tr>
    <td>{$lng.lbl_full_name}:</td>
    <td><input type="checkbox" name="add[is_name]" value='Y'{if $banner.is_name eq 'Y'} checked="checked"{/if} /></td>
</tr>  
<tr> 
    <td>{$lng.lbl_description}:</td>
    <td><input type="checkbox" name="add[is_descr]" value='Y'{if $banner.is_descr eq 'Y'} checked="checked"{/if} /></td>
</tr>
<tr>
    <td>{$lng.lbl_add_to_cart_link}:</td>
    <td><input type="checkbox" name="add[is_add]" value='Y'{if $banner.is_add eq 'Y'} checked="checked"{/if} /></td>
</tr>
{elseif $banner_type eq 'M'}
<tr> 
    <td>
<script type="text/javascript" language="JavaScript 1.2">
<!--
{literal}
function preview_body() {
	win = window.open('preview_banner.php','PREVIEW_POPUP','width=600,height=460,toolbar=no,status=no,scrollbars=yes,resizable=yes,menubar=no,location=no,direction=no');
}
{/literal}
-->
</script>
	{$lng.lbl_body}:</td>
    <td><textarea cols="60" rows="10" name="add[body]" id="banner_body">{$banner.body}</textarea></td>
</tr>
<tr>
	<td>&nbsp;</td>
	<td>
	<table cellspacing="0" cellpadding="2">
	<tr>
		<td><a href="javascript: void(0);" onclick="javascript: document.getElementById('banner_body').value += '<#A#>';"><img src="{$cdn_base}/skin1/images/open_a.gif" alt="" /></a></td>
		<td><a href="javascript: void(0);" onclick="javascript: document.getElementById('banner_body').value += '<#A#>';">{$lng.lbl_add_link_opening_tag}</a></td>
	</tr>
	</table>
	<table cellspacing="0" cellpadding="2">
	<tr>
		<td><a href="javascript: void(0);" onclick="javascript: document.getElementById('banner_body').value += '<#/A#>';"><img src="{$cdn_base}/skin1/images/close_a.gif" alt="" /></a></td>
		<td><a href="javascript: void(0);" onclick="javascript: document.getElementById('banner_body').value += '<#/A#>';">{$lng.lbl_add_link_closing_tag}</a></td>
	</tr>
	</table>
	<table cellspacing="0" cellpadding="2">
	<tr>
		<td><a href="javascript: void(0);" onclick="javascript: preview_body();"><img src="{$cdn_base}/skin1/images/preview_img.gif" alt="{$lng.lbl_preview|escape}" /></a></td>
		<td><a href="javascript: void(0);" onclick="javascript: preview_body();">{$lng.lbl_preview}</a></td>
	</tr>
	</table>
	</td>
</tr>
{if $elements ne ''}
<tr>
    <td>&nbsp;</td>
    <td><br /><b>{$lng.lbl_media_library}:</b><br />
	<iframe width="100%" height="300" src="{$catalogs.admin}/partner_element_list.php"></iframe>
    </td>
</tr>
{/if}
{/if}
<tr>
	<td>&nbsp;</td>
	<td class="SubmitBox">
	<input type="submit" value="{$lng.lbl_save_banner|strip_tags:false|escape}" />
	{if $banner.bannerid > 0}&nbsp;<input type="submit" name="close" value="{$lng.lbl_close|strip_tags:false|escape}" />{/if}
	</td>
</tr>
</table>
</form>

{if $banner_type eq 'M'}
<br />

<b>{$lng.lbl_add_media_object}</b><br />
<form action="partner_banners.php" method="post" enctype="multipart/form-data">
<input type="hidden" name="mode" value="upload" />
<input type="hidden" name="banner_type" value="{$banner_type}" />
<input type="hidden" name="bannerid" value="{$banner.bannerid}" />

<table cellpadding="0" cellspacing="3">
<tr>
	<td>{$lng.lbl_media_object}:</td>
	<td><input type="file" name="userfile" /></td>
</tr>
<tr> 
    <td colspan="2"><br /><b>{$lng.txt_flash_note}</b></td>
</tr>
<tr> 
    <td>{$lng.lbl_width}:</td>
    <td><input type="text" size="5" name="width" /></td>
</tr>
<tr>  
    <td>{$lng.lbl_height}:</td>
    <td><input type="text" size="5" name="height" /></td>
</tr>
<tr>
	<td>&nbsp;</td>
	<td><input type="submit" value="{$lng.lbl_add|strip_tags:false|escape}" /></td>
</tr>
</table>
</form>

{/if}
{/capture}
{if $banner ne ''}{assign var="title" value=$lng.lbl_modify_banner}{else}{assign var="title" value=$lng.lbl_add_banner}{/if}
{include file="dialog.tpl" content=$smarty.capture.dialog title=$title extra='width="100%"'}
{/if}
