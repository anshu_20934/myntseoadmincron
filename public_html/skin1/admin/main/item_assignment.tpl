{* $Id: featured_products.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
{include file="main/include_js.tpl" src="main/popup_product.js"}
<link rel="stylesheet" href="{$SkinDir}/skin1_admin_operations.css" />
<script language="javascript" src="../skin1/js_script/item_assignment.js"></script>
<script type="text/javascript" src="{$SkinDir}/js_script/calender.js" ></script>
<link rel="stylesheet" href="{$SkinDir}/css/calender.css" type="text/css" media="screen" />
{include file="admin/main/item_search.tpl"}
{if $smarty.get.action == "a_failed"}
<p><b><FONT COLOR="#ff0000">  Some of the orders you have tried to assign are not in Queued/Work in Progress . These orders have not been assigned. </FONT></b></p>
{/if}

{if $mode == "search"}
{capture name=dialog}
<div style="float:left;width:auto;height:auto;border : 0px solid #000000;">
{include file="customer/main/navigation.tpl"}
</div>
    <form action="" method="post" name="item_assignment">
        <input type="hidden" name="mode" />
        <input type="hidden" name="page" id="page" value="{$smarty.get.page}"  />
         <input type="hidden" name="query_string" id="query_string" value="{$query_string}"  />

         <div style="width:995px;height:40px;">
            
            <div style="float:right;margin:0 0 0 10px;">
                <p>
                    <input type="button" name="item_assignment_done" value="    Item Assignment Done    " onclick="javascript:is_items_not_assigned('{$total_items}');">
                </p>
            </div>
            <div style="float:right">
                <p><label style="float:left;font-size:100%;margin:0 0 0 10px;padding:2px;width:80px;font-weight:bold;" for="item_location">Location:<span class="required">*</span></label>
                    <select name="item_location" id="item_loc"  >
                        <option value="0">-select location-</option>
                        {section name=loc loop=$locations}
                           <option value="{$locations[loc].id}">{$locations[loc].location_name}</option>
                        {/section}
                    </select>
                </p>
            </div>
            <div style="float:right">
                <p><label style="float:left;font-size:100%;margin:0 0 0 10px;padding:2px;width:80px;font-weight:bold;" for="item_location">Assignee:<span class="required">*</span></label>
                {section name=ass loop=$assignee}
                    <input type="hidden" id="threshhold_{$assignee[ass].id}" value="{$assignee[ass].threshold}">
                    {section name=assigned loop=$assigned_item}
                       {if $assigned_item[assigned].aid == $assignee[ass].id} {assign var=total_item value=$assigned_item[assigned].assigned_item} {/if}
                    {/section}
                    <input type="hidden" id="totalassigneditem_{$assignee[ass].id}" value="{$total_item}">
                    {assign var=total_item value="0"}
                {/section}
                
                  
                    <select name="item_assignee" id="item_ass" onchange="javascript:changeAssignee();" style="width:125px;">
                        <option value="0">-select assignee-</option>
                        {section name=ass loop=$assignee}
                            {section name=assigned loop=$assigned_item}
                               {if $assigned_item[assigned].aid == $assignee[ass].id} {assign var=total_item value=$assigned_item[assigned].assigned_item} {/if}
                            {/section}
                            <option value="{$assignee[ass].id}">{$assignee[ass].name}&nbsp;({$total_item})</option>
                            {assign var=total_item value="0"}
                        {/section}
                    </select>
                </p>
            </div>
         </div>

        <div class="DIV_TABLE" style="width:2390px">
        <div class="DIV_TH" style="width:2390px">
            <div class="DIV_TD" style="width:180px;"><a href="{$navigation_script}&amp;order_by=order_name{if $smarty.get.sort_order=='desc' && $smarty.get.order_by=='order_name'}&amp;sort_order=asc{else}&amp;sort_order=desc{/if}">Order Name{if $smarty.get.sort_order=='desc' && $smarty.get.order_by=='order_name'}&uArr;{elseif $smarty.get.sort_order=='asc' && $smarty.get.order_by=='order_name'}&dArr;{else}{/if}</a></div>
            <div class="DIV_TD" style="width:65px;"><a href="{$navigation_script}&amp;order_by=order_id{if $smarty.get.sort_order=='desc' && $smarty.get.order_by=='order_id'}&amp;sort_order=asc{else}&amp;sort_order=desc{/if}">order id{if $smarty.get.sort_order=='desc' && $smarty.get.order_by=='order_id'}&uArr;{elseif $smarty.get.sort_order=='asc' && $smarty.get.order_by=='order_id'}&dArr;{else}{/if}</a></div>
            <div class="DIV_TD" style="width:55px;">PM</div>
            <div class="DIV_TD" style="width:55px;">Item Id</div>
            <div class="DIV_TD" style="width:90px;_width:95px;"><a href="{$navigation_script}&amp;order_by=item_status{if $smarty.get.sort_order=='desc' && $smarty.get.order_by=='item_status'}&amp;sort_order=asc{else}&amp;sort_order=desc{/if}">status{if $smarty.get.sort_order=='desc' && $smarty.get.order_by=='item_status'}&uArr;{elseif $smarty.get.sort_order=='asc' && $smarty.get.order_by=='item_status'}&dArr;{else}{/if}</a></div>
            <div class="DIV_TD" style="width:80px;"><a href="{$navigation_script}&amp;order_by=product_type{if $smarty.get.sort_order=='desc' && $smarty.get.order_by=='product_type'}&amp;sort_order=asc{else}&amp;sort_order=desc{/if}">PRODUCT TYPE{if $smarty.get.sort_order=='desc' && $smarty.get.order_by=='product_type'}&uArr;{elseif $smarty.get.sort_order=='asc' && $smarty.get.order_by=='product_type'}&dArr;{else}{/if}</a></div>
            <div class="DIV_TD" style="width:125px;"><a href="{$navigation_script}&amp;order_by=product_style{if $smarty.get.sort_order=='desc' && $smarty.get.order_by=='product_style'}&amp;sort_order=asc{else}&amp;sort_order=desc{/if}">item name{if $smarty.get.sort_order=='desc' && $smarty.get.order_by=='product_style'}&uArr;{elseif $smarty.get.sort_order=='asc' && $smarty.get.order_by=='product_style'}&dArr;{else}{/if}</a></div>
            <div class="DIV_TD" style="width:200px;">quantity breakup</div>
            <div class="DIV_TD" style="width:200px;">Design Name</div>
            <div class="DIV_TD" style="width:50px;">qty</div>
            <div class="DIV_TD" style="width:100px;"><a href="{$navigation_script}&amp;order_by=location{if $smarty.get.sort_order=='desc' && $smarty.get.order_by=='location'}&amp;sort_order=asc{else}&amp;sort_order=desc{/if}">location{if $smarty.get.sort_order=='desc' && $smarty.get.order_by=='location'}&uArr;{elseif $smarty.get.sort_order=='asc' && $smarty.get.order_by=='location'}&dArr;{else}{/if}</a></div>
            <div class="DIV_TD" style="width:100px;"><a href="{$navigation_script}&amp;order_by=assignee{if $smarty.get.sort_order=='desc' && $smarty.get.order_by=='assignee'}&amp;sort_order=asc{else}&amp;sort_order=desc{/if}">assignee{if $smarty.get.sort_order=='desc' && $smarty.get.order_by=='assignee'}&uArr;{elseif $smarty.get.sort_order=='asc' && $smarty.get.order_by=='assignee'}&dArr;{else}{/if}</a></div>
            <div class="DIV_TD" style="width:50px;">select</div>
            <div class="DIV_TD" style="width:180px;">queued date</div>
            <div class="DIV_TD" style="width:180px;">source</div>
            <div class="DIV_TD" style="width:180px;">assigned date</div>
            <div class="DIV_TD" style="width:180px;">completed date</div>
            <div class="DIV_TD" style="width:180px;">Customer login</div>
            <div class="DIV_TD" style="width:100px;">priview image</div>
        </div>
       
        {if $total_items > 0 }
        {assign var=oldorderid value="-1"}
        {section name=item loop=$itemResult max=$sizeofitemresult}
             {section name=assigned loop=$assigned_item}
                {if $assigned_item[assigned].aid == $itemResult[item].assignee} {assign var=total_item value=$assigned_item[assigned].assigned_item} {/if}
             {/section}
             <div class="DIV_TR" style="width:2390px">
                    {if $itemResult[item].orderid != $oldorderid}
                        <div class="DIV_TR_TD" style="width:180px;">{if $itemResult[item].is_customizable eq 0}<span style="background-color:red;font-weight:bold;padding-left:4px;padding-right:4px;">NP</span>{/if} <a href="javascript:void(0);" onclick="javascript:window.open('order.php?orderid={$itemResult[item].orderid}');"><p>{$itemResult[item].order_name}</p></a></div>
                        <div class="DIV_TR_TD" style="width:65px;"><a href="javascript:void(0);" onclick="javascript:window.open('order.php?orderid={$itemResult[item].orderid}');"><p>{$itemResult[item].orderid}</p></a></div>
                    {else}
                        <div class="DIV_TR_TD" style="width:180px;">{if $itemResult[item].is_customizable eq 0}<span style="background-color:red;font-weight:bold;padding-left:4px;padding-right:4px;">NP</span>{/if}<a href="javascript:void(0);" style="text-decoration:none" onclick="javascript:window.open('order.php?orderid={$itemResult[item].orderid}');"><p><font style="text-transform: uppercase;">L</font></p></a></div>
                        <div class="DIV_TR_TD" style="width:65px;"><a href="javascript:void(0);" style="text-decoration:none" onclick="javascript:window.open('order.php?orderid={$itemResult[item].orderid}');"><p><font style="text-transform: uppercase;">L</font></p></a></div>
                    {/if}
                    {assign var=oldorderid value=$itemResult[item].orderid}
                    <div class="DIV_TR_TD" style="width:55px;"><p>{$itemResult[item].paymentmethod}</p></div>
                    <div class="DIV_TR_TD" style="width:55px;"><p>{$itemResult[item].itemid}</p></div>
                    <div class="DIV_TR_TD" style="width:90px;_width:95px;"><p>
                    {assign var=i_status value=$itemResult[item].item_status}
                    {if $i_status == 'UA'} {assign var=status_color value="#EE0F14"} {/if}
                    {if $i_status == 'A'} {assign var=status_color value="#77AB3D"} {/if}
                    {if $i_status == 'D'} {assign var=status_color value="#80FFFF"} {/if}
                    {if $i_status == 'F'} {assign var=status_color value="#EF940A"} {/if}
                    {if $i_status == 'H'} {assign var=status_color value="#F5FF42"} {/if}
                    {if $i_status == 'S'} {assign var=status_color value="#04FF82"} {/if}
                    <select name="item_status" id="item_st_{$itemResult[item].itemid}" onchange="javascript:assignItemAssignment('status', '{$itemResult[item].itemid}','{$itemResult[item].item_status}');"
                    {if $i_status != 'A' && $i_status != 'S' && $i_status != 'I'} disabled="disabled" {/if} style="background-color:{$status_color};width:90px;">
                         {foreach from=$status item=st_value key=st_key}
                            <option value="{$st_key}" {if $i_status == $st_key} selected {/if} style="color:#000000;background-color:{$st_value.color};">{$st_value.status}</option>
                        {/foreach}
                    </select></p>
                    </div>
                    <div class="DIV_TR_TD" style="width:80px;"><p>{$itemResult[item].product_type_name}</p></div>
                    <div class="DIV_TR_TD" style="width:125px;"><p>{$itemResult[item].stylename}</p></div>
                    <div class="DIV_TR_TD" style="width:200px;"><p>{$itemResult[item].quantity_breakup}</p></div>
                    <div class="DIV_TR_TD" style="width:200px;"><p>{$itemResult[item].designName}</p></div>
                    <div class="DIV_TR_TD" style="width:50px;"><p>{$itemResult[item].amount}</p>
                        <input type="hidden" name="qty[]" id="qty_{$itemResult[item].itemid}" value="{$itemResult[item].itemid}">
                    </div>
                    <div class="DIV_TR_TD" style="width:100px;"><p>{$itemResult[item].location_name}</p></div>
                    <div class="DIV_TR_TD" style="width:100px;"><p>{if $itemResult[item].assignee_name !="Not Assigned"}{$itemResult[item].assignee_name}&nbsp;({$total_item}){else}{$itemResult[item].assignee_name}&nbsp;{/if}</p></div>
                    <div class="DIV_TR_TD" style="width:50px;">
                        <input type="checkbox" id="check_{$itemResult[item].itemid}" name="check[{$itemResult[item].itemid}]" value="{$itemResult[item].amount}">
                    </div>
                    
                    <div class="DIV_TR_TD" style="width:180px;">{$itemResult[item].queueddate|date_format:$config.Appearance.datetime_format}</div>
                    <div class="DIV_TR_TD" style="width:180px;">{$itemResult[item].source_name}</div>
                    <div class="DIV_TR_TD" style="width:180px;">{if $itemResult[item].assignment_time} {$itemResult[item].assignment_time|date_format:$config.Appearance.datetime_format} {else} -- {/if}</div>
                    <div class="DIV_TR_TD" style="width:180px;">{if $i_status == 'D'} {$itemResult[item].completion_time|date_format:$config.Appearance.datetime_format} {else} -- {/if}</div>
					<div class="DIV_TR_TD" style="width:180px;">{$itemResult[item].login}</div>
                    <div class="DIV_TR_TD" style="width:100px;"><a href="javascript:void(0);" onclick="javascript:window.open('view_print_design.php?productId={$itemResult[item].productid}&orderid={$itemResult[item].orderid}');"><p>preview image</p></a></div>
             </div>
        {/section}
        {else}
            <div class="DIV_TR">
                <div class="DIV_TR_TD" style="width:1000px;"><strong>no matching request found</strong></div>
            </div>
        {/if}
        
        </div>
        
        
    </form>
    
 <div style="float:left;width:auto;height:auto;border : 0px solid #000000;">   
{include file="customer/main/navigation.tpl"}
</div>
{/capture}
{include file="dialog.tpl" title="Item Assignment List" content=$smarty.capture.dialog extra='width="100%"'}
{/if}
