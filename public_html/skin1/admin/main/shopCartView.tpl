{* $Id: featured_products.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
{include file="main/include_js.tpl" src="main/popup_product.js"}

<a name="featured" />

{$lng.txt_featured_products}

<br /><br />

{capture name=dialog}

<table cellpadding="3" cellspacing="1" width="100%">

<tr class="TableHead">
	<td width="10%" align="center">No.</td>
	<td width="20%" align="center">Login</td>
	<td width="30%" align="center">No. Of Products In Carts<br>(Type--Style--TotalAmount)</td>
	<td width="30%" align="center">Cart Product Ids</td>
</tr>

{if $cartdetails }
{assign var=loop value=1}
{foreach from=$cartdetails item=item}

<tr{cycle values=", class='TableSubHead'"}>
	<td align="center">{$loop}</td>
	<td align="center">{$item[0]}</td>
	<td align="center">{foreach from=$item[1] item=a}{$a.productTypeLabel}--{$a.productStyleName}--Rs.{$a.totalPrice}<br>{/foreach}</td>
	<td align="center">{foreach from=$item[2] item=b}{$b}<br>{/foreach}</td>
{assign var=loop value=$loop+1}	 
</tr>

{/foreach}

</table>
{/if}

{/capture}
{include file="dialog.tpl" title='User Shopping Carts' content=$smarty.capture.dialog extra='width="100%"'}
<br /><br />

