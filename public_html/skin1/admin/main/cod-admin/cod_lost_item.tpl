{* $Id: featured_products.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
{include file="main/include_js.tpl" src="main/popup_product.js"}
{ if $mode == 'lost' || $mode == 'lostItemUpdate'}
<div id="codLostDiv" style="display:block;">
{else }
<div id="codLostDiv" style="display:none;">
{/if}
{capture name=dialog}
<form name="codLostItemForm" action="cod_reconcile.php" method="POST">
	<input type="hidden" name="mode" id="mode" value="lost"/>
	<table width="100%" cellspacing="5" cellpadding="1">
		<tbody>
		<tr>
			<td nowrap="nowrap" class="FormButton">OrderId::</td>
			<td width="10">&nbsp;</td>
			<td class="FormButton"><input type="text" name="lostOrderId" value="{ $post_data.lostOrderId}"/></td>	
		</tr>
		<tr>
			<td nowrap="nowrap" class="FormButton">AWB Id::</td>
			<td width="10">&nbsp;</td>
			<td class="FormButton"><input type="text" name="lostAwbNo" value="{ $post_data.lostAwbNo}"/></td>	
		</tr>
		
</tbody>
</table>
<center>
	<input type="submit" value="Search" id="lostItemSearchBtn" name="lostItemSearchBtn">
	<input type="submit" document.codLostItemForm.submit();" value="DownloadAsCsv" name="codSearchBtn">
</center>

</form>

<form name="lostItemUpdateForm" id="lostItemUpdateForm" action="cod_reconcile.php" method="POST">
<input type="hidden" name="mode" id="mode" value="lostItemUpdate" />
<div style="text-align:right">
<select style="width: 15%;" name="lostItemStatus" value="{ $post_data.lostItemStatus}">
<option value="">--Select Item Status--</option>
<option value="L">Lost</option>
<option value="R">Returned</option>
<option value="D">Delivered</option>
</select>
<input type="button" onclick="submitCodItemUpdate()" value="modify" name="lostItemBtn">
<!-- <input type="submit" value="modify" name="lostItemBtn"> -->

</div>
<table width="100%" cellspacing="1" cellpadding="2">
				<tbody>
				<tr class="tablehead">
					<th width="5%" nowrap="nowrap">Order Id</th>
					<th width="5%" nowrap="nowrap">Shipping Date</th>
					<th width="5%" nowrap="nowrap">Item Id</th>
					<th width="5%" nowrap="nowrap">Product Type</th>
					<th width="5%" nowrap="nowrap">Product Style</th>
					<th width="5%" nowrap="nowrap">Awb No</th>
					<th width="5%" nowrap="nowrap">Value(Rs)</th>
					<th width="5%" nowrap="nowrap">Item Status</th>
					<th width="5%" nowrap="nowrap">	<input type="checkbox" name="lostItemSelectAll" id="lostItemSelectAll"/>
					Select All</th>
					<th width="5%" nowrap="nowrap">More Info</th>
				</tr> 
				{if $lost_item_data } 
				{foreach from=$lost_item_data item=result_row}
					<tr>
						{foreach from=$result_row item=result_col}				
						<td width="5%" nowrap="nowrap">{$result_col}</td>	
						{/foreach}
						{if $result_row.item_status == "S" }
						<td width="5%" nowrap="nowrap"><input type="checkbox" name="lostItemSelect[]" value="{$result_row.orderid}"/>Select </td>
						{else}
						<td width="5%" nowrap="nowrap">
						{/if}
						<td width="5%" nowrap="nowrap"><a href="order.php?orderid={$result_row.orderid}">View</a></th>
						</tr>
						<tr><td colspan="10"><hr/></td></tr>
					{/foreach}
				{/if}
				</tbody>
</table>
</form>
{/capture}
{include file="dialog.tpl" title="Account for lost items" content=$smarty.capture.dialog extra='width="100%"'}
</div>
</br> </br>
