{* $Id: featured_products.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
{include file="main/include_js.tpl" src="main/popup_product.js"}
{ if $mode == 'search' || $mode == 'updateOrderStatus'}
<div id="codSearchDiv" style="display:block;">
{else }
<div id="codSearchDiv" style="display:none;">
{/if}
{capture name=dialog}
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
<script type="text/javascript" src="{$SkinDir}/js_script/calender.js" ></script>
<link rel="stylesheet" href="{$SkinDir}/css/calender.css" type="text/css" media="screen" />
<form name="codSearchForm" action="cod_reconcile.php" method="POST">
	<input type="hidden" name="mode" id="mode" value="search"/>
	<table width="100%" cellspacing="5" cellpadding="1">

<tbody>
<tr>
	<td nowrap="nowrap" class="FormButton">Order Date Period:</td>
	<td width="10">&nbsp;</td>
	<td class="FormButton">
	Start Date::<input type="text" name="orderStartDate" value="{ $post_data.orderStartDate}"  id="orderStartDate"/>&nbsp;
	<img border="0" onclick="displayDatePicker('orderStartDate');" alt="Click Here to Pick up the date" src="{$cdn_base}/images/cal.gif">&nbsp;
	End Date::<input type="text" name="orderEndDate" value="{ $post_data.orderEndDate}"  id="orderEndDate"/>&nbsp;
	<img border="0" onclick="displayDatePicker('orderEndDate');" alt="Click Here to Pick up the date" src="{$cdn_base}/images/cal.gif">
	</td>	
</tr>
<tr>
	<td nowrap="nowrap" class="FormButton">Shipping Date Period:</td>
	<td width="10">&nbsp;</td>
	<td class="FormButton">
		Start Date::<input type="text" name="shippingStartDate" value="{ $post_data.shippingStartDate}" id="shippingStartDate"/>&nbsp;
		<img border="0" onclick="displayDatePicker('shippingStartDate');" alt="Click Here to Pick up the date" src="{$cdn_base}/images/cal.gif">&nbsp;
		End Date::<input type="text" name="shippingEndDate" value="{ $post_data.shippingEndDate}" id="shippingEndDate"/>&nbsp;
		<img border="0" onclick="displayDatePicker('shippingEndDate');" alt="Click Here to Pick up the date" src="{$cdn_base}/images/cal.gif">
	</td>	
</tr>
<tr>
	<td nowrap="nowrap" class="FormButton">Payment Date Period:</td>
	<td width="10">&nbsp;</td>
	<td class="FormButton">
	Start Date::<input type="text" name="paymentStartDate" value="{ $post_data.paymentStartDate}"  id="paymentStartDate"/>&nbsp;
	<img border="0" onclick="displayDatePicker('paymentStartDate');" alt="Click Here to Pick up the date" src="{$cdn_base}/images/cal.gif">&nbsp;
	End Date::<input type="text" name="paymentEndDate" value="{ $post_data.paymentEndDate}"  id="paymentEndDate"/>&nbsp;
	<img border="0" onclick="displayDatePicker('paymentEndDate');" alt="Click Here to Pick up the date" src="{$cdn_base}/images/cal.gif">
	</td>	
</tr>
<tr>
	<td nowrap="nowrap" class="FormButton">Order Value:</td>
	<td width="10">&nbsp;</td>
	<td class="FormButton">Start Value::<input type="text" name="orderStartValue" value="{ $post_data.orderStartValue}" />&nbsp;End Value::<input type="text" name="orderEndValue"  value="{ $post_data.orderEndValue}"/></td>	
</tr>
<tr><td class="FormButton"> Pay Status::</td> <td width="10">&nbsp;</td><td> <input type="text" name="payStatus"  value="{ $post_data.payStatus}"/></td></tr>
<tr><td class="FormButton"> Order Id::</td> <td width="10">&nbsp;</td><td> <input type="text" name="orderId" value="{ $post_data.orderId}"/></td></tr>
<tr><td class="FormButton"> Customer Login::</td> <td width="10">&nbsp;</td><td> <input type="text" name="customerName"value="{ $post_data.customerName}"/></td></tr>
<tr><td class="FormButton"> AWB No::</td> <td width="10">&nbsp;</td><td> <input type="text" name="awbno" value="{ $post_data.awbno}"/></td></tr>

</tbody></table>
<center>
		<input type="submit" value="search" name="codSearchBtn"/>
        <input type="submit" document.codSearchForm.submit();" value="DownloadAsCsv" name="codSearchBtn">
</center>
</form>
</br></br>
<form name="codUpdateForm" id="codUpdateForm" action="cod_reconcile.php" method="POST">
<input type="hidden" name="mode" id="mode" value="updateOrderStatus"/>
<div style="text-align:right">
<select style="width: 15%;" name="orderStatus">
<option value="">Select Status</option>
<option value="paid">Paid</option>
</select>
<input type="button" onclick="submitCodUpdate()" value="modify" name="codUpdateBtn">
</div>
<table width="100%" cellspacing="1" cellpadding="2">
				<tbody>
				<tr class="tablehead">
					<th width="5%" nowrap="nowrap">Order Id</th>
					<th width="5%" nowrap="nowrap">Shipping Date</th>
					<th width="5%" nowrap="nowrap">Payment Date</th>
					<th width="5%" nowrap="nowrap">Order Date</th>
					<th width="5%" nowrap="nowrap">Customer Login</th>
					<th width="5%" nowrap="nowrap">AWB No</th>
					<th width="5%" nowrap="nowrap">Value(Rs)</th>
					<th width="5%" nowrap="nowrap">Pay Status</th>
					<th width="5%" nowrap="nowrap"><input type="checkbox" name="codSelectAll" id="codSelectAll"/>Select All</th>
					<th width="5%" nowrap="nowrap">Info</th>
				</tr>
				{if $cod_search_data } 
				    {foreach from=$cod_search_data item=result_row}
					<tr >
						{foreach from=$result_row item=result_col}				
						<td nowrap="nowrap"  align="center">{$result_col}</td>	
						{/foreach}
						{if $result_row.cod_pay_status == "pending" || $result_row.cod_pay_status == "delivered" || $result_row.cod_pay_status == "paidtobluedart" }
						<td nowrap="nowrap" align="center"><input type="checkbox" name="codSelect[]" value="{$result_row.orderid}"/></td>
						{else}
						<td nowrap="nowrap"/>
						{/if}
						<td nowrap="nowrap" align="center"><a href="order.php?orderid={$result_row.orderid}">View</a></td>
						</tr>
							<tr><td colspan="11"><hr/></td>
						</tr>
					{/foreach}
				{/if}
				</tbody>
</table>
</form>
{/capture}
{include file="dialog.tpl" title="Search COD Orders" content=$smarty.capture.dialog extra='width="100%"'}
</div>
<br /><br />