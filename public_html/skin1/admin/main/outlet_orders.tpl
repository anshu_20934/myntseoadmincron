<link rel="stylesheet" href="{$SkinDir}/skin1_admin.css" />
{capture name=dialog}
<form name="outlet_orders" action="outlet_order_search.php" method="post">
    <div class="ITEM_PARENT_DIV">
		<div class="ITEM_FIRST_CHILD_DIV" style="width:450px;">
		    <div class="ITEM_LABLE_DIV" style="width:200px;">Search CCD outlet orders</div>
		    <div class="ITEM_FIELD_DIV" style="width:200px;">
		      <input type="text" name="refid" id="refid" value="{$refid}"/>
		      <input type="hidden" name="mode" id="mode" value="search"/>
		      <input type="submit" value="search"/>
		    </div>  
	    </div>
    </div>     
</form>
{/capture}
{include file="dialog.tpl" title="CCD outlet orders" content=$smarty.capture.dialog extra='width="100%"'}

<br/> <br/>
{capture name=dialog}
<table cellpadding="2" cellspacing="1" width="100%" >
<tr class="TableHead">
	<td width="5">&nbsp;</td>
	<td width="3%" nowrap="nowrap">#</td>
	<td width="5%" nowrap="nowrap">Ref Id</a></td>
	<td width="15%" nowrap="nowrap">Status</a></td>
	<td width="25%" nowrap="nowrap">Login</td>
	<td width="5%" nowrap="nowrap">Create Date</td>
	<td width="15%" nowrap="nowrap">Outlet Name</td>
	<td width="5%" nowrap="nowrap">City</td>
	<td width="10%" nowrap="nowrap">Area Manager</td>
	<td width="10%" align="right" nowrap="nowrap">Total</td>
</tr>
{if $orders}
{foreach key=key item=item from=$orders}
<tr class='TableSubHead'>
	<td width="5"><input type="checkbox" name="orderids[{$item.orderid}]" /></td>
	<td><a href="order.php?orderid={$item.orderid}">{$item.orderid}</a></td>
	<td><a href="order.php?orderid={$item.orderid}">{$item.ref_id}</a></td>
	<td><a href="order.php?orderid={$item.orderid}">{include file="main/order_status.tpl" status=$item.status mode="static" }</a></td>
	<td>{$item.firstname} {$item.lastname} ({$item.login})</td>
	<td>{$item.date}</td>
	<td>{$item.outlet_name}</td>
	<td>{$item.outlet_city}</td>
	<td>{$item.area_mgr}</td>
	<td>{$item.total}</td>
</tr>
{/foreach}
{/if}
</table>
{/capture}
{include file="dialog.tpl" title="Search Results" content=$smarty.capture.dialog extra='width="100%"'}
