{* $Id: manufacturers.tpl,v 1.32.2.1 2006/06/16 10:47:46 max Exp $ *}
{literal}
<script language="Javascript">
 function validateInput(mode)
 {
 	frmObj = document.tagform;
 	if(frmObj.pagename.value == "")
 	{
 		alert("Please enter page url")
 		frmObj.pagename.focus()
 		return false;
 	}
 	
 	frmObj.mode.value=mode
 	return true;
 } 
 
</script>
{/literal}


{include file="page_title.tpl" title='Meta Tags Management'}
<p>There are some reserved keywords you can use to dynamically change the meta information.
  You can use <strong>#product.name# , #ptype.name# ,#pstyle.name#,#designer.name#,#tag.name#,
  #category.name#,#shop.name#</strong> to replace name of the <strong>product,product type,product style,designer,tag name,
  category name,shop name </strong>respectively.
  You can use <strong>#ptype.keywords# ,#category.keywords#, #product.keywords#</strong>  to replace tags/keywords of <strong>product type,category and product</strong> respectively.
  <br/>
  Also mention the query identifier ie id for product page, product style page, category page , product type page,
  designer page, shop page .
 
  </p>

{if $pagelist ne "" && $pageid eq ""}

{capture name=dialog}

{*include file="customer/main/navigation.tpl"*}

<form action="metatags_management.php" method="post" name="tagform">
<input type="hidden" name="mode" value="update" />
<input type="hidden" name="page" value="{$page}" />

<table cellpadding="3" cellspacing="1" width="100%">

<tr class="TableHead">
	<td width="30"  align="right" colspan="5">{$previous}|{$next}</td>
</tr>


<tr class="TableHead">
	<td width="10">&nbsp;</td>
	<td width="20%">Page Name</td>
	<td width="30%">Meta Keywords</td>
	<td width="20%" align="center">Page Title</td>
	<td width="30%" align="center">Meta Description</td>
	<!--<td width="30" align="center">{$lng.lbl_active}</td> -->
</tr>

{if $pagelist ne ""}

{section name=idx loop=$pagelist}

	<tr{cycle values=", class='TableSubHead'"}>
		<td align="center"><input type="checkbox" name="posted_data[{$pagelist[idx].id}][to_delete]" /></td>
		<td>{$pagelist[idx].pageurl}</td>
		<td>{$pagelist[idx].keywords}</td>
		<td align="center">{$pagelist[idx].title}</td>
		<td align="center">{$pagelist[idx].description}</td>
		<!--<td align="center">{$pagelist[idx].id}</td> -->
	</tr>

{/section}



<tr>
	<td colspan="6" class="SubmitBox">
	<input type="button" value="{$lng.lbl_delete_selected|escape}" onclick="javascript: if (confirm('Are you sure you want to delete?')) {ldelim} document.tagform.mode.value='delete'; document.tagform.submit(); {rdelim}" />
	<input type="submit" value="{$lng.lbl_modify_selected|escape}" onclick="javascript:document.tagform.mode.value='modify'; document.tagform.submit();"/>
	</td>
</tr>

{else}

	<tr>
		<td colspan="6" align="center"><br />{$lng.txt_no_manufacturers}</td>
	</tr>

{/if}

<tr>
<td colspan="6"><br /><input type="button" value="{$lng.lbl_add_new_|escape}" 
onclick="javascript: self.location = 'metatags_management.php?mode=add';" /></td>
</tr>

</table>

</form>

{*include file="customer/main/navigation.tpl"*}

{/capture}
{include file="dialog.tpl" title='Meta Tags Management' content=$smarty.capture.dialog extra='width="100%"'}

{else}



{capture name=dialog}


<form action="metatags_management.php" method="post" name="tagform">
<input type="hidden" name="mode"  />
<input type="hidden" name="pageid" value="{$pagelist.id}" />
<input type="hidden" name="hidAppendTag[]"  value="{$seoParams.Keywords}" />
<input type="hidden" name="hidAppendTag[]"  value="{$seoParams.Title}" />
<input type="hidden" name="hidAppendTag[]"  value="{$seoParams.Description}" />




<table cellpadding="3" cellspacing="1" width="100%">

<tr>
	<td width="20%" class="FormButton">Page Name:</td>
	<td><font class="Star">*</font></td>
	<td width="80%"><input type="text" name="pagename" size="50" value="{$pagelist.pageurl}"  /></td>
</tr>



<tr>
	<td class="FormButton">Keywords:</td>
	<td>&nbsp;</td>
	<td>
		<input type="text" name="keywords" size="90" value="{$pagelist.keywords}"  />&nbsp;
		<!--<b>override:</b><input type="checkbox" name="chkAppendTag[]"  value="{$seoParams.Keywords}" {if $seoParams.Keywords eq $overrideParams.Keywords} checked {/if}/>-->
	</td>
</tr>

<tr>
	<td class="FormButton">Title:</td>
	<td>&nbsp;</td>
	<td><input type="text" size="50" name="title" value="{$pagelist.title}"  />&nbsp;
	<!--<b>override</b><input type="checkbox" name="chkAppendTag[]"  value="{$seoParams.Title}" {if $seoParams.Title eq $overrideParams.Title} checked {/if} />-->
	
	</td>
</tr>


<tr>
	<td class="FormButton">Description:</td>
	<td>&nbsp;</td>
	<td><input type="text" name="metadesc" size="90" value="{$pagelist.description}" />&nbsp;
	<!--<b>override</b> <input type="checkbox" name="chkAppendTag[]"  value="{$seoParams.Description}" {if $seoParams.Description eq $overrideParams.Description} checked {/if}/>-->
	</td>
</tr>

<!-- <tr>
	<td class="FormButton">Qualifier:</td>
	<td>&nbsp;</td>
	<td><input type="text" name="qualifier" size="50" value="{$pagelist.qualifier}" /></td>
</tr>
<tr>
	<td class="FormButton">Qualifier Postion:</td>
	<td>&nbsp;</td>
	<td><select name="qualifier_pos">
	      <option value="0">--Select--</option> 
	      <option value="P" {if $pagelist.qualifier_pos eq 'P' }selected{/if}>Prefix</option> 
	      <option value="S" {if $pagelist.qualifier_pos eq 'S' }selected{/if}>Suffix</option> 
	     
	   </select></td>
</tr>

<tr>
	<td class="FormButton">Apply Tag For:</td>
	<td>&nbsp;</td>
	<td>
	   <select name="tagtype">
	      <option value="0">--Select--</option> 
	      <option value="P" {if $pagelist.tagtype eq 'P' }selected{/if}>Product</option> 
	      <option value="T" {if $pagelist.tagtype eq 'T' }selected{/if}>Product Type</option> 
	      <option value="C" {if $pagelist.tagtype eq 'C' }selected{/if}>Category</option> 
	      <option value="S" {if $pagelist.tagtype eq 'S' }selected{/if}>Product Style</option>
	      <option value="K" {if $pagelist.tagtype eq 'K' }selected{/if}>Product Keyword</option>
	      <option value="A" {if $pagelist.tagtype eq 'A' }selected{/if}>Affiliate Shop</option>
	   </select>
	</td>
</tr> -->
<tr>
	<td class="FormButton">Query String Identifier:</td>
	<td>&nbsp;</td>
	<td><input type="text" name="query_identifier" size="30" value="{$pagelist.query_identifier}" /></td>
</tr>
<tr>
	<td class="FormButton">External Content</td>
	<td>&nbsp;</td>
	<td><textarea name="extContent" id="extContent" style="width:400px;height:100px;">{$pagelist.external_content|escape:"quotes"}</textarea></td>
</tr>
<tr>
	<td class="FormButton">comments</td>
	<td>&nbsp;</td>
	 <td>
	      <select name="comments">
	        <option value="1" {if $pagelist.comments eq 1} selected {/if}>Y</option>
	        <option value="0" {if $pagelist.comments eq 0} selected {/if} >N</option>
	      </select>
	</td>
</tr>
<tr>
	<td class="FormButton">Rss Feeds:</td>
	<td>&nbsp;</td>
	<td><textarea name="rssfeed" id="rssfeed" style="width:400px;height:100px;">{$pagelist.rssfeeds|stripslashes}</textarea></td>
</tr>
<tr>
	<td class="FormButton">H1 Tag</td>
	<td>&nbsp;</td>
	<td><textarea name="h1tag" id="h1tag" style="width:400px;height:100px;">{$pagelist.h1tag|stripslashes}</textarea></td>
</tr>

<tr>
	<td class="FormButton">H2 Tag</td>
	<td>&nbsp;</td>
	<td><textarea name="h2tag" id="h2tag" style="width:400px;height:100px;">{$pagelist.h2tag|stripslashes}</textarea></td>
</tr>
<tr>
	<td class="FormButton">Related keywords</td>
	<td>&nbsp;</td>
	<td><textarea name="relatedkeywords" id="relatedkeywords" style="width:400px;height:100px;">{$pagelist.relatedkeywords|stripslashes}</textarea></td>
</tr>
<tr>
	<td class="FormButton" >Image Source</td>
	<td>&nbsp;</td>
	<td><textarea name="imagesource" id="imagesource" style="width:400px;height:100px;">{$pagelist.imagesource|stripslashes}</textarea></td>
</tr>





<!--<tr>
	<td class="FormButton">{$lng.lbl_availability}:</td>
	<td>&nbsp;</td>
	<td><input type="checkbox" name="avail" value="Y"{if $manufacturer.avail eq 'Y' || $manufacturer.manufacturerid eq ''} checked="checked"{/if} /></td>
</tr> -->


<tr>
	<td colspan="2">&nbsp;</td>
	<td>
	{if $pageid eq ''}
		<input type="submit" value=" {$lng.lbl_save} "  onclick="return validateInput('save')" />
	{else}
		<input type="submit" value=" {$lng.lbl_update} "  onclick="return validateInput('update')" />
	{/if}
	</td>
</tr>

</table>
</form>

{/capture}
{include file="dialog.tpl" title='Meta Details' content=$smarty.capture.dialog extra='width="100%"'}

{/if}

