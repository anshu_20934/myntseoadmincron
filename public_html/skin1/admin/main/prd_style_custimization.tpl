{* $Id: featured_products.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
{include file="main/include_js.tpl" src="main/popup_product.js"}

<a name="featured" />

{$lng.txt_featured_products}

<br /><br />


{capture name=dialog}


<form action="style_custumization.php" method="post" name="custumareaform"  enctype="multipart/form-data">
<input type="hidden" name="mode"  />
<input type="hidden" name="styleid" value="{$styleid}" />
<input type="hidden" name="custom_id" value="{$custom_id}" />



<table cellpadding="3" cellspacing="1" width="100%">

{if $custmization}

{section name=prod_num loop=$custmization}
  
   
     <tr>
         <td><b>{$lng.lbl_area_name} : </b></td>
         <td><input type="text" name="area_name" size="50" value="{$custmization[prod_num].name}" /></td>
        
	 <td><b>{$lng.lbl_type_label} : </b></td>
         <td><input type="text" name="area_label" size="50" value="{$custmization[prod_num].label}" /></td>
       
    </tr>
    

    <tr>
         <td><b>{$lng.lbl_type_thumbimg} : </b></td>
         <td><img src="../{$custmization[prod_num].image_t}" height="50" width="50" />
	 &nbsp;&nbsp;<a href="javascript:uploadWin('thcust',{$custom_id});"  valign="top">Change</a></td>

	 <td><b>{$lng.lbl_type_detailimage} : </b></td>
         <td><img src="../{$custmization[prod_num].image_l}" height="50" width="50" />
	 &nbsp;&nbsp;<a href="javascript:uploadWin('detcust',{$custom_id});"   valign="top">Change</a>
	 </td>
    </tr>
     
   
    <tr>
         <td><b>{$lng.lbl_primary} : </b></td>
         <td><input type="checkbox" name="primary" {if $custmization[prod_num].isprimary eq 1 } checked {/if} /></td>
         <td valign="middle"><b>{$lng.lbl_type_description} : </b></td>
         <td><textarea name="desc" cols="50" rows="5">{$custmization[prod_num].description}</textarea></td>
    </tr>
     <tr>
	      <td><b>Background preview image : </b></td>
          <td><img src="../{$custmization[prod_num].bkgroundimage}" height="50" width="50" />
          &nbsp;&nbsp;<a href="javascript:uploadWin('previewcust',{$custom_id});"   valign="top">Change</a></td>
          <td><b>Zoom Image 500 * 500: </b></td>
          <td><img src="../{$custmization[prod_num].image_z}" height="50" width="50" />
          &nbsp;&nbsp;<a href="javascript:uploadWin('zoomimage',{$custom_id});"   valign="top">Change</a></td>

    </tr>
    <tr><td></td></tr>
    <tr>
    	<td><b>Customization Price</b></td>
		<td><input type="text" name="customizationareaprice" size="10" value="{$custmization[prod_num].customizationcharge}" /> Rupees</td>
    </tr>
    
{/section}

{else}
 

       <tr>
         <td><b>{$lng.lbl_area_name} : </b></td>
         <td><input type="text" name="area_name" size="50"  /></td>
        
	 <td><b>{$lng.lbl_type_label} : </b></td>
         <td><input type="text" name="area_label" size="50"  /></td>
       
    </tr>
    
    <tr>
         <td><b>{$lng.lbl_type_thumbimg} : </b></td>
         <td><input type="file" name="img1" ></td>

	 <td><b>{$lng.lbl_type_detailimage} : </b></td>
         <td><input type="file" name="det_img" > </td>
    </tr>

     
   

    <tr>
         <td><b>{$lng.lbl_primary} : </b></td>
         <td><input type="checkbox" name="primary"  value="1" /></td>
         
	 <td valign="middle"><b>{$lng.lbl_type_description} : </b></td>
         <td><textarea name="desc" cols="50" rows="5"></textarea></td>
    </tr>
    <tr>
	     <td><b>Background preview image : </b></td>
         <td><input type="file" name="preview_img" id="preview_img"> </td>
         <td><b>&nbsp;</b></td>
         <td>&nbsp;</td>

    </tr>
    <tr>
	     <td><b>Zoom image 500 * 500 : </b></td>
         <td><input type="file" name="zoom_img" id="zoom_img"> </td>
         <td><b>&nbsp;</b></td>
         <td>&nbsp;</td>

    </tr>
    
      



{/if}

 <tr>
         <td colspan="2">&nbsp;</td>
         
 </tr>

<tr>
	<td colspan="2" class="SubmitBox">
	{if $custmization}
            <input type="submit" value="{$lng.lbl_update|escape}" onclick="javascript: submitForm(this, 'updcust');" />
	{else}
            <!-- <input type="submit" value="{$lng.lbl_save|escape}" onclick="javascript: submitForm(this, 'savecust');" /> -->
	    <input type="submit" value="{$lng.lbl_save|escape}" onclick="javascript: return  validateCustumization(document.custumareaform, 'savecust');" />
	     <input type="reset" value="{$lng.lbl_cancel|escape}"   />
	{/if}
	

	
	</td>
</tr>



 <tr>
<td colspan="2"><br /><br /></td>
</tr>



</table>
</form>

{/capture}
{include file="dialog.tpl" title=$lng.lbl_custmization_area_detail content=$smarty.capture.dialog extra='width="100%"'}
<br /><br />

{if $modify_ort eq  "yes"}

 {include file="admin/main/prd_orientation_modify.tpl"}
 
{else}

{include file="admin/main/prd_style_orientation.tpl"}

{/if}