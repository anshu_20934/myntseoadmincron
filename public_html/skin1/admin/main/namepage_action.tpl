{* $Id: featured_products.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
{literal}
<script language="Javascript">
   function chkFields(frm){
   	  var mobj = frm.mailerimg;  
   	  if(mobj == ''){
   	  	alert("Browse image to upload.")
   	  }
   }
</script>
{/literal}
<a name="featured" />
{$lng.txt_featured_products}
<br /><br />

{capture name=dialog}
<table>
<form name='uplMailer' id='' action='name_page.php' method='POST' enctype="multipart/form-data" onsubmit="return chkFields(this);">
{if $mode eq 'modify'}
{section name=idx loop=$contest}
<input type='hidden' name='ids' value = "{$contest[idx].id}" >
<input type='hidden' name='mode' value = "modify" >
  <tr>
     <td><strong>Style Id</strong></td><td> : <input type="text" name="style_id" id="style_id" value="{$contest[idx].style_id}"/></td><td><strong>&nbsp;Eg: 2</strong></td>
 </tr>
 <tr>
     <td><strong>Product Type Id</strong></td><td> : <input type="text" name="product_type_id" id="product_type_id" value="{$contest[idx].product_type_id}" /></td><td><strong>eg: 2 for t-shirt</strong></td>
 </tr>
 <tr>
     <td><strong>Descriprion</strong></td><td> : <input type="text" name="description" id="description" value="{$contest[idx].description}" /></td><td><strong>eg: You can customize official indian team jersey </strong></td>
 </tr>
 <tr>
     <td><strong>Row Sequence</strong></td><td> : <input type="text" name="row_sequence" id="row_sequence" value="{$contest[idx].row_sequence}"/></td><td><strong>eg:1 </strong></td>
 </tr>
 <tr>
     <td><strong>Width small font</strong></td><td>  : <input type="text" name="width_small_font" id="width_small_font" value="{$contest[idx].width_small_font}"/></td><td><strong>eg:10 </strong></td>
 </tr>
 <tr>
     <td><strong>Width large font</strong></td><td> : <input type="text" name="width_large_font" id="width_large_font" value="{$contest[idx].width_large_font}"/></td><td><strong>eg:10 </strong></td>
 </tr>
  <tr>
     <td><strong>Height small font</strong></td><td> : <input type="text" name="height_small_font" id="height_small_font" value="{$contest[idx].height_small_font}"/></td><td><strong>eg:10 </strong></td>
 </tr>
 <tr>
     <td><strong>Height large font</strong></td><td> : <input type="text" name="height_large_font" id="height_large_font" value="{$contest[idx].height_large_font}"/></td><td><strong>eg:10 </strong></td>
 </tr> 
  <tr>
     <td><strong>price</strong></td><td> : <input type="text" name="price" id="price" value="{$contest[idx].price}"/></td><td><strong>eg:300 </strong></td>
 </tr>
 <tr>
     <td><strong>Product name</strong></td><td> : <input type="text" name="product_name" id="product_name" value="{$contest[idx].product_name}"/></td><td><strong>eg:indian team jersey </strong></td>
 </tr>
 <tr>
     <td><strong>Landing Page URL</strong></td><td> : <input type="text" name="url" id="url" value="{$contest[idx].url}"/></td><td><strong>eg:http://www.myntra.com/mumbai-indians-jersey </strong></td>
 </tr>
 <tr>
     <td><strong>coordinates of small image</strong></td><td> : <input type="text" name="coordinates_of_small_image" id="coordinates_of_small_image" value="{$contest[idx].coordinates_of_small_image}" /></td><td><strong>eg:0,10[margin-left,margin-top]</strong></td>
 </tr>
 <tr>
     <td><strong>coordinates of large image</strong></td><td> : <input type="text" name="coordinates_of_large_image" id="coordinates_of_large_image" value="{$contest[idx].coordinates_of_large_image}" /></td><td><strong>eg:0,10[margin-left,margin-top] </strong></td>
 </tr>
 <tr>
     <td><strong>Alt</strong></td><td> : <input type="text" name="alt" id="alt" value="{$contest[idx].alt}" /></td><td></td>
 </tr>
 <tr>
     <td><strong>Title</strong></td><td> : <input type="text" name="title" id="title" value="{$contest[idx].title}" /></td><td></td>
 </tr>
 <tr>
     <td><strong>Browse image </strong></td><td> : <input type="file" name="nameimage[]" id="nameimage1"  /><img src='{$cdn_base}/{$contest[idx].small_font_image}' width='40' height='40' /></td><td> small font image</td> 
 </tr>
 <tr>
     <td><strong>Browse image </strong></td><td> : <input type="file" name="nameimage[]" id="nameimage2" /><img src='{$cdn_base}/{$contest[idx].large_font_image}' width='40' height='40' /></td><td>large font image</td>
 </tr>
 <tr>
     <td><strong>Browse image </strong></td><td> : <input type="file" name="nameimage[]" id="nameimage3"  /><img src='{$cdn_base}/{$contest[idx].icon_style_image}' width='40' height='40' /></td><td>icon  style image</td>
      </tr>
 <tr>
     <td><strong>Browse image </strong></td><td> : <input type="file" name="nameimage[]" id="nameimage4"  /><img src='{$cdn_base}/{$contest[idx].small_style_image}' width='40' height='40' /></td><td>small style image</td>
 </tr>
 <tr>
     <td><strong>Browse image </strong></td><td> : <input type="file" name="nameimage[]" id="nameimage5" /><img src='{$cdn_base}/{$contest[idx].large_style_image}' width='40' height='40' /></td><td>large style image</td>
 </tr>
<tr>
     <td><strong>Browse image </strong></td><td> : <input type="file" name="nameimage[]" id="nameimage6"  /><img src='{$cdn_base}/{$contest[idx].small_back_image}' width='40' height='40' /></td><td>small back image</td>
 </tr>
 <tr>
     <td><strong>Browse image </strong></td><td> : <input type="file" name="nameimage[]" id="nameimage7" /><img src='{$cdn_base}/{$contest[idx].large_back_image}' width='40' height='40' /></td><td>large back image</td>
 </tr>
<div style="clear:both;"></div>
{/section}
{else}

  <tr>
     <td><strong>Style Id</strong></td><td> : <input type="text" name="style_id" id="style_id" /></td><td><strong>&nbsp;Eg: 2</strong></td>
 </tr>
 <tr>
     <td><strong>Product Type Id</strong></td><td> : <input type="text" name="product_type_id" id="product_type_id" /></td><td><strong>eg: 2 for t-shirt</strong></td>
 </tr>
 <tr>
     <td><strong>Descriprion</strong></td><td> : <input type="text" name="description" id="description" /></td><td><strong>eg: You can customize official indian team jersey </strong></td>
 </tr>
 <tr>
     <td><strong>Row Sequence</strong></td><td> : <input type="text" name="row_sequence" id="row_sequence" /></td><td><strong>eg:1 </strong></td>
 </tr>
 <tr>
     <td><strong>Width small font</strong></td><td>  : <input type="text" name="width_small_font" id="width_small_font" /></td><td><strong>eg:10 </strong></td>
 </tr>
 <tr>
     <td><strong>Width large font</strong></td><td> : <input type="text" name="width_large_font" id="width_large_font" /></td><td><strong>eg:10 </strong></td>
 </tr>
  <tr>
     <td><strong>Height small font</strong></td><td> : <input type="text" name="height_small_font" id="height_small_font" /></td><td><strong>eg:10 </strong></td>
 </tr>
 <tr>
     <td><strong>Height large font</strong></td><td> : <input type="text" name="height_large_font" id="height_large_font" /></td><td><strong>eg:10 </strong></td>
 </tr> 
  <tr>
     <td><strong>price</strong></td><td> : <input type="text" name="price" id="price" /></td><td><strong>eg:300 </strong></td>
 </tr>
 <tr>
     <td><strong>Product name</strong></td><td> : <input type="text" name="product_name" id="product_name" /></td><td><strong>eg:indian team jersey </strong></td>
 </tr>
 <tr>
     <td><strong>Landing Page URL</strong></td><td> : <input type="text" name="url" id="url" /></td><td><strong>eg:http://www.myntra.com/mumbai-indians-jersey </strong></td>
 </tr>
 <tr>
     <td><strong>coordinates of small image</strong></td><td> : <input type="text" name="coordinates_of_small_image" id="coordinates_of_small_image" /></td><td><strong>eg:0,10[margin-left,margin-top]</strong></td>
 </tr>
 <tr>
     <td><strong>coordinates of large image</strong></td><td> : <input type="text" name="coordinates_of_large_image" id="coordinates_of_large_image" /></td><td><strong>eg:0,10[margin-left,margin-top] </strong></td>
 </tr>
<tr>
     <td><strong>Browse image </strong></td><td> : <input type="file" name="nameimage[]" id="small_font_image" /></td><td>upload small font image</td>
 </tr>
 <tr>
     <td><strong>Browse image </strong></td><td> : <input type="file" name="nameimage[]" id="large_font_image" /></td><td>upload large font image</td>
 </tr>
 <tr>
     <td><strong>Browse image </strong></td><td> : <input type="file" name="nameimage[]" id="small_style_image" /></td><td>upload small style image</td>
 </tr>
 <tr>
     <td><strong>Browse image </strong></td><td> : <input type="file" name="nameimage[]" id="large_style_image" /></td><td>upload large style image</td>
 </tr>
 <tr>
     <td><strong>Browse image </strong></td><td> : <input type="file" name="nameimage[]" id="small_back_image" /></td><td>upload small back image</td>
 </tr>
 <tr>
     <td><strong>Browse image </strong></td><td> : <input type="file" name="nameimage[]" id="large_back_image" /></td><td>upload large back image</td>
 </tr>
{/if}
<tr><td></td><td><input type="submit" name="smtImages" value="submit" />&nbsp;&nbsp;&nbsp;</td></tr>

</form>
 </table>	
{/capture}
{include file="dialog.tpl" title='Featured Stores Management' content=$smarty.capture.dialog extra='width="100%"'}
<br /><br />

