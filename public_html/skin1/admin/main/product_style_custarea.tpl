{* $Id: featured_products.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
{*include file="main/include_js.tpl" src="main/popup_product.js"*}

<a name="featured" />

{$lng.txt_featured_products}

<br /><br />

{capture name=dialog}


<form action="productstyle_action.php" method="post" name="custmizationform" >
<input type="hidden" name="mode"  />
<input type="hidden" name="styleid" value="{$styleid}" />

<table cellpadding="3" cellspacing="1" width="100%">

<tr class="TableHead">
	<td width="10%">&nbsp;</td>
	<td width="60%">{$lng.lbl_custmization_area}</td>
	<!--<td width="40%">{$lng.lbl_image}</td> -->
	<td width="15%" align="center">{$lng.lbl_pos}</td>
	<td width="15%" align="center">{$lng.lbl_active}</td>
</tr>

{if $custarea}

{section name=num loop=$custarea}

<tr{cycle values=", class='TableSubHead'"}>
	<td align="center"><input type="checkbox" name="posted_data[{$custarea[num].id}][to_delete]"  /></td>
	<td align="center"><b>{$custarea[num].name}</b></td>
	<!--<td align="center"><b>{$custarea[num].default_image}</b></td> -->
	<td align="center"><input type="text" name="posted_data[{$custarea[num].id}][display_order]" size="5" value="{$custarea[num].display_order}" /></td>
	<td align="center">
	<select  name="posted_data[{$custarea[num].id}][is_active]">
	           <option value="1" {if $custarea[num].is_active eq "1"} selected {/if}>Yes</option>
		   <option value="0" {if $custarea[num].is_active eq "0"} selected {/if}>No</option>
        </select> 
			   
       </td>
</tr>

{/section}




{else}

<tr>
<td colspan="6" align="center">{$lng.txt_no_featured_products}</td>
</tr>

{/if}

<tr>
	<td colspan="6" class="SubmitBox">
	<input type="button" value="{$lng.lbl_delete_selected|escape}" onclick="javascript: document.custmizationform.mode.value = 'delcustarea'; document.custmizationform.submit();" />
	<input type="submit" value="{$lng.lbl_add_new_|escape}" onclick="javascript: document.custmizationform.mode.value = 'addcustarea'; document.custmizationform.submit();"/>
	<input type="button" value="{$lng.lbl_update|escape}" onclick="javascript: document.custmizationform.mode.value = 'updcustarea'; document.custmizationform.submit();" />
	<input type="button" value="{$lng.lbl_modify_selected|escape}" onclick="javascript: document.custmizationform.mode.value = 'modifyarea'; document.custmizationform.submit();" />
	</td>
</tr>


</table>
</form>

{/capture}
{include file="dialog.tpl" title=$lng.lbl_productstyle_custarea content=$smarty.capture.dialog extra='width="100%"'}