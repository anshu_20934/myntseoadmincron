{* $Id: statistics.tpl,v 1.24.2.1 2006/06/16 10:47:41 max Exp $ *}
{include file="main/include_js.tpl" src="main/calendar2.js"}
{capture name=dialog}

<form action="" method="post" name="reportbymktfrom">
<input type="hidden" name="mode"  value="search" />

<table cellpadding="1" cellspacing="1">


<tr>
	<th align="right">{$lng.lbl_reporttype}:</th>
	<td>
	     <select name='report_type'>
	      <option value="week"  { if $reportType == "week"} selected {/if} >Weekly</option>
              <option value="month" { if $reportType == "month"} selected {/if} >Monthly</option> 
	           
           </select>
	</td>
	<th align="right">&nbsp;</th>
	<td>
	   &nbsp;
	</td>
</tr>

<tr>
	<th align="right">{$lng.lbl_report_from}:</th>
	<td><input type="text" name='Startdate' value="{$SDate}"/><a href="javascript:cal4.popup();" ><img src="{$cdn_base}/images/cal.gif" width="16" height="16" border="0" alt="Click Here to Pick up the date"></a></td>
	<th align="right">&nbsp;&nbsp;&nbsp;&nbsp;{$lng.lbl_report_to}:</th>
	<td><input type="text" name='Enddate'  value="{$EDate}" /><a href="javascript:cal5.popup();" class=""><img src="{$cdn_base}/images/cal.gif" width="16" height="16" border="0" alt="Click Here to Pick up the date"></a></td>
</tr>

<tr>
	<td colspan="4"><input type="submit" value="{$lng.lbl_submit|escape}" /></td>
</tr>
<script language="javascript">
      var cal4 = new calendar2(document.forms['reportbymktfrom'].elements['Startdate']);
      cal4.year_scroll = true;
      cal4.time_comp = false;

       var cal5 = new calendar2(document.forms['reportbymktfrom'].elements['Enddate']);
      cal5.year_scroll = true;
      cal5.time_comp = false;


</script>

</table>
</form>

{/capture}
{include file="dialog.tpl" title='' content=$smarty.capture.dialog extra='width="100%"'}

<br />
<br />
{if $mode}
{capture name=dialog}
<table cellpadding="1" cellspacing="1" width="100%">

<tr class="TableHead">
	<td >{$lng.lbl_duration}</td>
	<td >{$lng.lbl_agegroup}</td>
	<td >{$lng.lbl_sex}</td>
	<td >{$lng.lbl_orders}</td>
	<td >{$lng.lbl_sales}(Rs)</td>
	
</tr>


{if $segmentby_profile}

{section name=num loop=$segmentby_profile}

           
    {section name=idx loop=$segmentby_profile[num]}

<tr{cycle values=', class="TableSubHead"'}>
	
        <td align="center" >{$segmentby_profile[num][idx].duration1}-{$segmentby_profile[num][idx].duration2}</td>
	<td align="center">
	{ if $segmentby_profile[num][idx].agegroup  < 18 }   
	    0-18
	{elseif $segmentby_profile[num][idx].agegroup  > 18 && $segmentby_profile[num][idx].agegroup  <= 30}    
	    19-30
        {elseif $segmentby_profile[num][idx].agegroup  > 30 && $segmentby_profile[num][idx].agegroup  <= 50} 
	    30-50 
        {else}
	     50+
        {/if}  
	 
	</td>
	<td align="center">{$segmentby_profile[num][idx].sex}</td>
	<td align="center">{$segmentby_profile[num][idx].num_of_orders}</td>
	<td align="center">{$segmentby_profile[num][idx].price * $segmentby_profile[num][idx].num_of orders}</td>
</tr>
{/section}
{/section}
<tr{cycle values=', class="TableSubHead"'}>
	
        <td align="center" ><b>Total</b></td>
	<td align="center">&nbsp;</td>
	<td align="center">&nbsp;</td>
	<td align="center"><b>{$totalOrders}</b></td>
	<td align="center"><b>{$totalSales}</b></td>
</tr>

{else}

<tr>
	<td colspan="4" align="center">{$lng.txt_no_categories}</td>
</tr>

{/if}


</table>


</form>

<br />

{/capture}
{include file="dialog.tpl" title='' content=$smarty.capture.dialog extra='width="100%"'}
{/if}