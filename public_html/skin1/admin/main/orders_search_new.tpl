
{* $Id:orders_search.tpl, v1.00.0.0, 2008/09/23 6:08:00 max Exp $ *}

{include file="main/include_js.tpl" src="main/popup_product.js"}
<link rel="stylesheet" href="{$http_location}/skin1/css/thickbox.css" />
<link rel="stylesheet" href="/skin1/myntra_css/jquery-ui-datepicker.css" />

<script type="text/javascript" src="/skin1/js_script/jquery-1.4.2.min.js" > </script>
<script type="text/javascript" src="/skin1/myntra_js/jquery-ui-1.8.13.custom.min.js" > </script>
<script type="text/javascript" src="/skin1/myntra_js/jquery-ui-datepicker.js" > </script>
<script type="text/javascript" src="{$http_location}/skin1/js_script/thickbox.admin.js"></script>
{literal}
<script Language="JavaScript" Type="text/javascript">

    var orderStatuses = { "WP":"Work In Progress", "OH":"On Hold", "Q":"Queued", "F":"Rejected", "D":"Declined", "SH":"Shipped", "RFR" : "Ready For Release",
                          "DL":"Delivered", "C":"Complete", "PV":"Pending Verification", "PP":"Pre Processed", "PK":"Packed", "L":"Lost"};
    
    function limitLines(obj, e) {
        var keynum;
        // IE
        if(window.event) {
            keynum = e.keyCode;
            // Netscape/Firefox/Opera
        } else if(e.which) {
            keynum = e.which;
        }

        if(keynum == 13) {  
	        var orderids = $("textarea#bulkInvoiceIDs").val();
	        orderids = orderids.split('\n');
	        if(orderids.length > obj.rows){
	           alert("You have reached max order limit of 50.");
	           return false;
            }
        }
    }
    
    function getResults(e) {
        var keynum;
        // IE
        if(window.event) {
            keynum = e.keyCode;
            // Netscape/Firefox/Opera
        } else if(e.which) {
            keynum = e.which;
        }

        if(keynum == 13) {  
	        searchOrders();
	        return false;
        }
    }
    
    function printBulkInvoice(printFor){
	    var orderids = $("textarea#bulkInvoiceIDs").val();
	    orderids = orderids.split('\n');
	    if(orderids.length > 50){
	       alert("You have exceeded max order limit of 50.");
	       return false;
        }
	    var validOrderIds = "";
	    for(var i in orderids) {
	        var orderid = orderids[i].trim();   
	        if(orderid.replace(/[\d]/g, '') != "") {
	            alert("Invalid separators used for order ids");
	            return;
	        } else {
	            if(orderid != "") {
	                validOrderIds += validOrderIds!=""?",":"";
	                validOrderIds += orderid;
	            }
	        }
	    }
	    if(validOrderIds != "") {
	    	if(printFor == 'finance'){
	    		var w = window.open('../order_invoice_new.php?orderids='+validOrderIds+'&type=admin&invoiceType=finance&copies=1','null','scrollbars=1,width=900,height=900,resizable=1');
	    	} else if(printFor == 'special'){
	    		var w = window.open('../order_invoice_new.php?orderids='+validOrderIds+'&type=admin&invoiceType=special&copies=1','null','scrollbars=1,width=900,height=900,resizable=1');
	    	}else {
	        	var w = window.open('../order_invoice_new.php?orderids='+validOrderIds+'&type=admin&copies=1','null','scrollbars=1,width=900,height=900,resizable=1');
	        }
	    }
	}
	
    function printBulkShippingLabel(){
        var orderids = $("textarea#bulkInvoiceIDs").val();
        orderids = orderids.split('\n');
        if(orderids.length > 50){
	       alert("You have exceeded max order limit of 50.");
	       return false;
        }
	    
	    var validOrderIds = "";
	    for(var i in orderids) {
	        var orderid = orderids[i].trim();
	        if(orderid.replace(/[\d]/g, '') != "") {
	            alert("Invalid separators used for order ids");
	            return;
	        } else {
	            if(orderid != "") {
	                validOrderIds += validOrderIds!=""?",":"";
	                validOrderIds += orderid;
	            }
	        }
	    }
	    if(validOrderIds != "") {
	        var w = window.open('../shipping_label.php?orderid='+validOrderIds+'&type=admin','null','scrollbars=1,width=900,height=900,resizable=1');
	    }
	}
    
    function searchOrders() {
        var postData = { orderid:$("#orderid").val(), order_status:$("#order_status").val(),
                         warehouse_id: $("#ops_location").val(), customer_login: $("#customer_login").val(),
                         customer_name: $("#customer_name").val(), customer_phonenum: $("#customer_phonenum").val(), action: "search", current_page: $("#current_page").val(),
                         from_order_date: $("#from_orderdate").val(), to_order_date: $("#to_orderdate").val(),
                         payment_method: $("#payment_method").val(), ordertype: $("#ordertype").val()
                         };
                         
        $.ajax({
            url: "",
            type: "POST",
            data: postData,
            success: function(response) {
                response = $.parseJSON(response);
                if(response.status) {
                    $(".current_page_display").html($("#current_page").val());
                    if($("#current_page").val() > 1) {
                        $(".prev_page_link").css("display", "inline");
                    } else {
                        $(".prev_page_link").css("display", "none");
                    }
                    
                    var orders = response.results;
                    $("#giftorders_search_results").css("display", "none");
                    $("#orders_search_results").css("display", "block");
                    $("#orders_search_results_body").html('');
                    if(orders == false) {
	                	$("#search_gift_orders_div").css("display", "inline");
	                } else {
	                	$("#search_gift_orders_div").css("display", "none");
	                }
                    for(var i in orders) {
                        var order = orders[i];
                        var newRow = '';
                        var className = "";
                        if(i%2 == 0)
                            className = "TableSubHead";
                        
                        if(order.payment_method == 'cod')
                            newRow = '<tr class="'+className+'" style="background-color:#FFCC33">';
                        else 
                            newRow = '<tr class="'+className+'">';
                        
                        var orderLink = "/admin/order.php?orderid="+order.orderid;
                        newRow += '<td align="center"><a href="'+orderLink+'">'+order.orderid+'</a></td>';
                        newRow += '<td align="center"><a href="'+orderLink+'">'+order.order_name+'</a></td>';
                        newRow += '<td align="center">'+orderStatuses[order.status]+'</td>';
                        newRow += '<td align="center">'+order.payment_method+'</td>';
                        newRow += '<td align="center">'+order.customer+'</td>';
                        newRow += '<td align="center">'+order.issues_contact_number+'</td>';
                        newRow += '<td align="center">'+order.date+'</td>';
                        newRow += '<td align="center">'+order.queueddate+'</td>';
                        newRow += '<td align="center">'+order.order_total_amount+'</td>';
                        newRow += '</tr>';
                        
                        $("#orders_search_results_body").append(newRow);
                    }    
                }                  
            }
        });
    }
    
    function searchGiftOrders() {
        var postData = { orderid:$("#orderid").val(), order_status:$("#order_status").val(),
                         warehouse_id: $("#ops_location").val(), customer_login: $("#customer_login").val(),
                         customer_name: $("#customer_name").val(), customer_phonenum: $("#customer_phonenum").val(), action: "giftsearch", current_page: $("#current_page").val(),
                         from_order_date: $("#from_orderdate").val(), to_order_date: $("#to_orderdate").val(),
                         payment_method: $("#payment_method").val(), ordertype: $("#ordertype").val()
                         };
                         
        $.ajax({
            url: "",
            type: "POST",
            data: postData,
            success: function(response) {
                response = $.parseJSON(response);
                if(response.status) {
                    $(".current_page_display").html($("#current_page").val());
                    if($("#current_page").val() > 1) {
                        $(".prev_giftpage_link").css("display", "inline");
                    } else {
                        $(".prev_giftpage_link").css("display", "none");
                    }
                    
                    var orders = response.results;
                    $("#orders_search_results").css("display", "none");
                    $("#giftorders_search_results").css("display", "block");
                    $("#giftorders_search_results_body").html('');
                    if(orders == false) {
	                	$("#search_orders_div").css("display", "inline");
	                } else {
	                	$("#search_orders_div").css("display", "none");
	                }
                    for(var i in orders) {
                        var order = orders[i];
                        var newRow = '';
                        var className = "";
                        if(i%2 == 0)
                            className = "TableSubHead";
                        
                        if(order.payment_method == 'cod')
                            newRow = '<tr class="'+className+'" style="background-color:#FFCC33">';
                        else 
                            newRow = '<tr class="'+className+'">';
                        
                        var orderLink = "/admin/order.php?orderid="+order.orderid+"&mode=gift_order";
                        newRow += '<td align="center"><a href="'+orderLink+'">'+order.orderid+'</a></td>';
                        newRow += '<td align="center">'+order.status+'</td>';
                        newRow += '<td align="center">'+order.payment_option+'</td>';
                        newRow += '<td align="center">'+order.login+'</td>';
                        newRow += '<td align="center">'+order.date+'</td>';
                        newRow += '<td align="center">'+order.order_total_amount+'</td>';
                        newRow += '</tr>';
                        
                        $("#giftorders_search_results_body").append(newRow);
                    }    
                }                    
            }
        });
    }
    
    $(function() {
    
        $('#statuspopup').ajaxStart(function(){
            $(this).show();
        }).ajaxStop(function(){
            $(this).hide();
        }); 
    
        $('.date-picker').datepicker({ dateFormat:'yy-mm-dd', maxDate: new Date() });
    
        $("#orders_search_btn").click(function() {
            $("#current_page").val('1');
            searchOrders();
        });
        $("#search_orders_btn").click(function() {
            $("#current_page").val('1');
            searchOrders();
        });
        $("#giftorders_search_btn").click(function() {
            $("#current_page").val('1');
            searchGiftOrders();
        });
        $("#search_gift_orders_btn").click(function() {
            $("#current_page").val('1');
            searchGiftOrders();
        });
        $(".prev_page_link").click(function() {
            var currentPage = parseInt($("#current_page").val());
            $("#current_page").val(currentPage - 1);
            searchOrders();
        });
        $(".next_page_link").click(function() {
            var currentPage = parseInt($("#current_page").val());
            $("#current_page").val(currentPage + 1);
            searchOrders();
        });
        $(".prev_giftpage_link").click(function() {
            var currentPage = parseInt($("#current_page").val());
            $("#current_page").val(currentPage - 1);
            searchGiftOrders();
        });
        $(".next_giftpage_link").click(function() {
            var currentPage = parseInt($("#current_page").val());
            $("#current_page").val(currentPage + 1);
            searchGiftOrders();
        });
    });
</script>
{/literal}

<p align="center"><font color="red" size="30">Page disabled! Please contact CRM team. Mail to crmoncall@myntra.com</font></p>

{*{capture name=dialog}
<div style="text-align:right;margin-bottom:10px;"><a href="#TB_inline?height=400&width=300&inlineId=bulkInvoiceForm" class="thickbox" title="Bulk Invoice Printing"> Bulk Invoice Printing</a></div>
<div id="bulkInvoiceForm" style="display:none;">
  <div style="text-align:left;padding:3px;">
    New Line seperated Order-IDs to print Invoice<br/>
    Max 50 orders at allowed.<br/>
    
    <textarea id="bulkInvoiceIDs" rows="50" style="width:100%;border:1px solid #999;height:300px;margin:4px auto;" onkeydown="return limitLines(this, event)"></textarea><br/><br/>
    <div style="text-align:center;">
      <button onclick="printBulkInvoice('');">Print Invoice(s)</button>
      <button onclick="printBulkInvoice('finance');">Print Finance Invoice(s)</button>
      <button onclick="printBulkInvoice('special');">Print Special Invoice(s)</button>
      <button onclick="printBulkShippingLabel();">Print Shipping Label(s)</button>
    </div>
  </div>
</div>
{/capture}    
{include file="dialog.tpl" title="Order Bulk Operations" content=$smarty.capture.dialog extra='width="100%"'}
<br>

{capture name=dialog}
    <input type="hidden" name="current_page" id="current_page" value="1">
    <div class="item_parent_div" style="width:90%;" onkeypress="return getResults(event)">
        <div class="item_first_child_div" style="width:45%;">
            <div class="item_lable_div" style="width:35%;">Order Status</div>
            <div class="item_field_div" style="width:65%;">
                {include file='main/order_status.tpl' name="order_status" mode="select" extended='Y'}
            </div>
        </div>
        <div class="item_first_child_div" style="width:45%;">
            <div class="item_lable_div" style="width:35%;">Warehouse Location</div>
            <div class="item_field_div" style="width:65%;">
                <select name="ops_location" id="ops_location" style="width: 150px;">
                    <option value="">--Select Warehouse--</option>
                    {foreach from=$ops_locations item=item key=key}
                        <option value="{$item.id}">{$item.name}</option>
                    {/foreach}
                </select>
            </div>
        </div>
        <div class="clear"></div>
        <div class="item_first_child_div" style="width:45%;">
            <div class="item_lable_div" style="width:35%;">Order Id</div>
            <div class="item_field_div" style="width:65%;">
               <input type="text" name="orderid" id="orderid">
            </div>
        </div>
        <div class="item_first_child_div" style="width:45%;">
            <div class="item_lable_div" style="width:35%;">Customer Login</div>
            <div class="item_field_div" style="width:65%;">
                <input type="text" name="customer_login" id="customer_login">
            </div>
        </div>
        <div class="clear"></div>
        <div class="item_first_child_div" style="width:45%;">
            <div class="item_lable_div" style="width:35%;">Customer Name</div>
            <div class="item_field_div" style="width:65%;">
                <input type="text" name="customer_name" id="customer_name">
            </div>
        </div>
        <div class="item_first_child_div" style="width:45%;">
            <div class="item_lable_div" style="width:35%;">Customer Phone No.</div>
            <div class="item_field_div" style="width:65%;">
                <input type="text" name="customer_phonenum" id="customer_phonenum">
            </div>
        </div>
        <div class="item_first_child_div" style="width:45%;">
            <div class="item_lable_div" style="width:35%;">Payment Method</div>
            <div class="item_field_div" style="width:65%;">
                <select name="payment_method" id="payment_method" style="width: 150px;">
                    <option value="">--Select Payment Method--</option>
                    <option value="on">ON</option>
                    <option value="cod">COD</option>
                </select>
            </div>
        </div>
        <div class="item_first_child_div" style="width:45%;">
            <div class="item_lable_div" style="width:35%;">Order Type</div>
            <div class="item_field_div" style="width:65%;">
                <select name="ordertype" id="ordertype" style="width: 150px;">
                    <option value="-1">--Select Order Type--</option>
                    <option value="on">Online</option>
                    <option value="off">Offline</option>
                    <option value="ex">Exchange</option>
                </select>
            </div>
        </div>
        <div class="item_first_child_div" style="width:45%;">
            <div class="item_lable_div" style="width:35%;">Order placed after</div>
            <div class="item_field_div" style="width:65%;">
               <input type="text" name="from_orderdate" id="from_orderdate" class="date-picker"/>
            </div>
        </div>
        <div class="item_first_child_div" style="width:45%;">
            <div class="item_lable_div" style="width:35%;">Order placed before</div>
            <div class="item_field_div" style="width:65%;">
               <input type="text" name="to_orderdate" id="to_orderdate" class="date-picker"/>
            </div>
        </div>
        <div class="clear"></div>
        <div style="margin: 5px; width: 90%; text-align:right;">
        	<input type="button" name="giftorders_search_btn" id="giftorders_search_btn" value="Gift Search">
            <input type="button" name="orders_search_btn" id="orders_search_btn" value="Search">          
        </div>
    </div>
{/capture}    
{include file="dialog.tpl" title="Order Search" content=$smarty.capture.dialog extra='width="100%"'}
<br>

<div id="orders_search_results" style="display:none;">
    {capture name=dialog}
    	<div name="search_gift_orders_div" id="search_gift_orders_div" style="margin: 5px; width: 90%; text-align:left; display:none">
        	<b> No results found. Try the Gift Search!</b>
        	<input type="button" style="margin: 15px;" name="search_gift_orders_btn" id="search_gift_orders_btn" value="Gift Search">          
        </div>
        <div style="text-align: left; padding: 10px 0px;">
            <a href="javascript:void(0);" class="prev_page_link"><img title="Previous Page" src="{$cdn_base}/skin1/images/larrow.gif" class="NavigationArrow" alt="{$lng.lbl_prev_page|escape}"/></a>
		    &nbsp; &nbsp;
		    Showing Page <span class="current_page_display"></span>
		    &nbsp;&nbsp;
		    <a href="javascript:void(0);" class="next_page_link"><img title="Next Page" src="{$cdn_base}/skin1/images/rarrow.gif" class="NavigationArrow" alt="{$lng.lbl_next_group_pages|escape}"/></a>    
		</div>
    
        <table cellpadding="2" cellspacing="1" width="100%">
	        <tr class="tablehead">
	            <th width="5%" nowrap="nowrap">Order Id</th>
	            <th width="5%" nowrap="nowrap">Order Name</th>
	            <th width="5%" nowrap="nowrap">Order Status</th>
	            <th width="5%" nowrap="nowrap">Payment Method</th>
	            <th width="5%" nowrap="nowrap">Customer</th>
	            <th width="5%" nowrap="nowrap">Phone No.</th>
	            <th width="5%" nowrap="nowrap">Order Date</th>
	            <th width="5%" nowrap="nowrap">Queued Date</th>
	            <th width="5%" nowrap="nowrap">Total Amount</th>
	        </tr>
    	    <tbody id="orders_search_results_body"></tbody>
    	</table>
    	
    	<div style="text-align: left; padding: 10px 0px;">
            <a href="javascript:void(0);" class="prev_page_link"><img title="Previous Page" src="{$cdn_base}/skin1/images/larrow.gif" class="NavigationArrow" alt="{$lng.lbl_prev_page|escape}"/></a>
            &nbsp; &nbsp;
            Showing Page <span class="current_page_display"></span>
            &nbsp;&nbsp;
            <a href="javascript:void(0);" class="next_page_link"><img title="Next Page" src="{$cdn_base}/skin1/images/rarrow.gif" class="NavigationArrow" alt="{$lng.lbl_next_group_pages|escape}"/></a>    
        </div>
    	
    {/capture}
    {include file="dialog.tpl" title="Search Results" content=$smarty.capture.dialog extra='width="100%"'}
</div>

<div id="giftorders_search_results" style="display:none;">
    {capture name=dialog}
    	<div name="search_orders_div" id="search_orders_div" style="margin: 5px; width: 90%; text-align:left; display:none"">
    		<b> No results found. Try the regular search!</b>
        	<input type="button" style="margin: 15px;" name="search_orders_btn" id="search_orders_btn" value="Search">          
        </div>
        <div style="text-align: left; padding: 10px 0px;">
            <a href="javascript:void(0);" class="prev_giftpage_link"><img title="Previous Page" src="{$cdn_base}/skin1/images/larrow.gif" class="NavigationArrow" alt="{$lng.lbl_prev_page|escape}"/></a>
		    &nbsp; &nbsp;
		    Showing Page <span class="current_page_display"></span>
		    &nbsp;&nbsp;
		    <a href="javascript:void(0);" class="next_giftpage_link"><img title="Next Page" src="{$cdn_base}/skin1/images/rarrow.gif" class="NavigationArrow" alt="{$lng.lbl_next_group_pages|escape}"/></a>    
		</div>
    
        <table cellpadding="2" cellspacing="1" width="100%">
	        <tr class="tablehead">
	            <th width="5%" nowrap="nowrap">Order Id</th>
	            <th width="5%" nowrap="nowrap">Order Status</th>
	            <th width="5%" nowrap="nowrap">Payment Method</th>
	            <th width="5%" nowrap="nowrap">Customer</th>
	            <th width="5%" nowrap="nowrap">Order Date</th>
	            <th width="5%" nowrap="nowrap">Total Amount</th>
	        </tr>
    	    <tbody id="giftorders_search_results_body"></tbody>
    	</table>
    	
    	<div style="text-align: left; padding: 10px 0px;">
            <a href="javascript:void(0);" class="prev_giftpage_link"><img title="Previous Page" src="{$cdn_base}/skin1/images/larrow.gif" class="NavigationArrow" alt="{$lng.lbl_prev_page|escape}"/></a>
            &nbsp; &nbsp;
            Showing Page <span class="current_page_display"></span>
            &nbsp;&nbsp;
            <a href="javascript:void(0);" class="next_giftpage_link"><img title="Next Page" src="{$cdn_base}/skin1/images/rarrow.gif" class="NavigationArrow" alt="{$lng.lbl_next_group_pages|escape}"/></a>    
        </div>
    	
    {/capture}
    {include file="dialog.tpl" title="Gifts Search Results" content=$smarty.capture.dialog extra='width="100%"'}
</div>

<div id="statuspopup" style="display:none">
    <div class="overlay" style="display:block;_position:absolute;_width:1300px;_height:1500px;">&nbsp;</div>
    <div style="position: absolute; left: 40%; top: 25%;"><img src="{$cdn_base}/skin1/icons/indicator.gif"></div>
</div>
*}