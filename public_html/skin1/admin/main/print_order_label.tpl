{* $Id: featured_products.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}


<a name="featured" />

{$lng.txt_featured_products}

<br /><br />

{capture name=dialog}

<table cellpadding="3" cellspacing="1" width="100%" border=0 bordercolor="#0033FF">
<tr>
     <td colspan="3">



      <table width="100%" border=0>
		<tr>
		    <td width=20%><b>#Order Id:</b></td>
		    <td width=30% align=left>{$orderid}</td>
		    <td width=20%><b>Order Date:</b></td>
		    <td width=30% align=left>{$date}</td>
		  </tr>
		  <tr>
		    <td><b>{$lng.lbl_delivery}:</b></td>
		    <td>{$shipping_method}</td>
		    <td><b>Payment Status:</b></td>
		    <td>{include file="main/order_status.tpl" status=$payment_status mode="static" }</td>
		  </tr>
		  <tr>
		    <td><b># Items:</b></td>
		    <td>{$items}</td>
		    <td><b>Estimated Ship Date:</b></td>
		    <td></td>
		  </tr>
      </table>

    </td>
</tr>
<tr >

	<td  height="30" >&nbsp;</td>

</tr>

<tr >

	<td colspan="3" >
	<table width="100%" border=0>
		{foreach name=outer item=product from=$productsInCart}
			{foreach key=key item=item from=$product}
				{if $key eq 'product'}
					{assign var='productName' value=$item}
				{/if}
				{if $key eq 'productid'}
					{assign var='productId' value=$item}
				{/if}
				{if $key eq 'product_type'}
					{assign var='productType' value=$item}
				{/if}
				{if $key eq 'OptionNames'}
					{assign var='OptionNames' value=$item}
				{/if}
				{if $key eq 'OptionValues'}
					{assign var='OptionValues' value=$item}
				{/if}
				{if $key eq 'amount'}
					{assign var='quantity' value=$item}
				{/if}
				{if $key eq 'Designlabel'}
					{assign var='Designlabel' value=$item}
				{/if}
				{if $key eq 'DesignImage'}
					{assign var='DesignImage' value=$item}
				{/if}
				{if $key eq 'OriName'}
					{assign var='OriName' value=$item}
				{/if}
				{if $key eq 'productStyleName'}
					{assign var='productStyleName' value=$item}
				{/if}
			{/foreach}
		<tr>
			<td>
				<table width="100%" border=1 bordercolor="#CC0033">
					<tr>
						<td width="40%">
							{$productName}&nbsp;[Product Id: {$productId}]<br />
							{$productType}<br />
							{section name=options loop=$OptionNames}
							{section name=options loop=$OptionValues}
								{$OptionNames[options]}: {$OptionValues[options]}<br />
							{/section}
							{/section}
							Quantity:&nbsp;{$quantity}<br />
							Style Name :{$productStyleName}
							{section name=designs loop=$Designlabel}
								{$Designlabel[designs]},

							{/section}
						</td>
						{section name=image loop=$DesignImage}
						  {section name=image loop=$Designlabel}
						    {section name=image loop=$OriName}

						<td width="30%" align=center><img src=".{$DesignImage[image]}" ><br/ >
						Area: {$Designlabel[image]}<br />
						Orientation: {$OriName[image]}
						</td>
						     {/section}
						  {/section}
						{/section}

					</tr>
				</table>
			</td>
		</tr>
		<tr >

	<td  height="30" >&nbsp;</td>

</tr>
		{/foreach}
	</table>


	</td>

</tr>








</table>


{/capture}
{include file="dialog.tpl" title='' content=$smarty.capture.dialog extra='width="100%"'}

