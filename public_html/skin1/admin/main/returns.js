$(function() {
	$('#status_change_button').click(function(){
		
		if(old_status == $("#return-status").val()) {
			$("#errorMsgBar").html("Please change the status to update.");
			return false;
		}

		if( ($("#return-status").val() == 'RIS' || $("#return-status").val() == 'RJS') && $("#qc-reason").val() == "") {
			$("#errorMsgBar").html("Please select a reason for quality Check");
			return false;
		}
		
		if(($("#return-status").val() == 'RPU' || $("#return-status").val() == 'RRC' || $("#return-status").val() == 'RDU') && returnMode == "self" && $("#trackingno").val() == "") {
			$("#errorMsgBar").html("Please input Tracking No for the received return item");
			return false;
		}
		
		if(($("#return-status").val() == 'RPU' || $("#return-status").val() == 'RRC' || $("#return-status").val() == 'RDU') && ($("#courier_service").val() == "" || 
				($("#courier_service").val() == "other" && $("#courier_service_custom").val() == "") ) ) {
			$("#errorMsgBar").html("Status cannot change to received without courier pickup");
			return false;
		}
		
		if(($("#return-status").val() == 'RPU' || $("#return-status").val() == 'RRC' || $("#return-status").val() == 'RDU')&& $("#trackingno").val() == "") {
			$("#errorMsgBar").html("Status cannot change to received without entering tracking no.");
			return false;
		}
		
		if(($("#return-status").val() == "RQP" || $("#return-status").val() == "RQF") && $("#item_bar_code").val() == "") {
			$("#errorMsgBar").html("Status cannot change without entering item bar code.");
			return false;
		}
		
		/*if(($("#return-status").val() == "RQP" || $("#return-status").val() == "RQF") && $("input:radio[name=valid]:checked").val() == "no" && $("#accurate-reason").val() == "") {
			$("#errorMsgBar").html("Status cannot change without selecting the identified reason for return.");
			return false;
		}*/
		
		if(($("#return-status").val() == "RQP" || $("#return-status").val() == "RQF" || $("#return-status").val() == 'RRC') && $("#warehouseid").val() == "") {
			$("#errorMsgBar").html("Please select Ops Location where items are received");
			return false;
		}
		
		if(($("#return-status").val() == 'RADC' || $("#return-status").val() == 'RJDC') && $("#dc-code").val() == "") {
			$("#errorMsgBar").html("Please select a DC Location where items are received");
			return false;
		}
		
		if(($("#return-status").val() == 'RADC' || $("#return-status").val() == 'RJDC' || $("#return-status").val() == 'CPDC' || $("#return-status").val() == 'CFDC') && $("#comment").val().trim() == "") {
			$("#errorMsgBar").html("Please enter a comment for return status change");
			return false;
		}
		
		if($("#return-status").val() == "RQP" || $("#return-status").val() == "RQF"){
			var itembarcodes = $("#item_bar_code").val();
			itembarcodes = itembarcodes.split('\n');
			var validBarcodes = "";
			for(var i in itembarcodes) {
				var itembarcode = itembarcodes[i].trim();
				if(itembarcode != "") {
					if(itembarcode.replace(/[\da-zA-Z]/g, '') != "") {
						alert("Invalid item barcode");
						return false;
					} else {
						validBarcodes += validBarcodes==""?"":",";
						validBarcodes += itembarcode;
					}
				}
			}
			
			if(validBarcodes == "") {
				alert("Please enter item barcodes to update");
				return false;
			}
			
			if($("#comment").val().trim() == "") {
				$("#errorMsgBar").html("Please enter a comment for return status change");
				return false;
			}
			
			$("#item_barcodes_list").val(validBarcodes);
			var orderid = $("#orderid").val();
			var itemid = $("#itemid").val();
			
			var overlay = new ProcessingOverlay();
			overlay.show();
			
			$.ajax({
				type: "POST",
				url: "",
				data: "action=validateitembarcodes&barcodes="+validBarcodes+"&orderid="+orderid+"&itemid="+itemid,
				success: function(msg) {
					var response = jQuery.parseJSON(msg);
					if(response.status == true){
						$("#shipped_items").val(response.items);
						$("#return_status_form").submit();
					}else{
						overlay.hide();
						if(response.reason == 'sku_id_not_matched'){
							$("#shipped_items").val(response.items);
							$('#overlay_r').fadeIn('fast',function(){
					            $('#box').animate({'top':'160px'},500);
					        });
						} else if(response.reason == 'warehouse_not_matched') { 
							$("#errorMsgBar").html("Scanned Items do not belong to the warehouse from where order was Shipped.");
							return false;
						} else {
							$("#errorMsgBar").html("Scanned items are not in shipped or accepted_returns state or item do not belong to this return.");
							return false;
						}
					}
				}
			});
			return false;
		}else{
			var overlay = new ProcessingOverlay();
			overlay.show();
		}
		
		return true;
	});
	
	$('#boxclose').click(function(){
        $('#box').animate({'top':'-200px'},500,function(){
            $('#overlay_r').fadeOut('fast');
        });
    });
	
	$('#boxsubmit').click(function(){
		$('#box').html('<p style ="font-size:1.5em;font-weight:bold;color:green;">Processing ... </p>');
		$("#skip_validation").val("yes");
		$("#return_status_form").submit();
    });	 
	
	/*$("input:radio[name=valid]").change(function() {
		if($("input:radio[name=valid]:checked").val() == "no"){
			$("#reason-label").css("display", "");
			$("#reason-select").css("display", "");
		} else {
			$("#accurate-reason").val('');
			$("#reason-label").css("display", "none");
			$("#reason-select").css("display", "none");
		}
	});*/
	
	$("#return-status").change(function() {
		if($(this).val() == "RJS" || $(this).val() == "RIS") {
			//$("#qapass-quality").css("display", "inline");
			$("#qc-reason-label").css("display", "inline");
			$("#qc-reason-select").css("display", "inline");
		} else {
			//$("#qapass-quality").css("display", "none");
			$("#qc-reason-label").css("display", "none");
			$("#qc-reason-select").css("display", "none");
		}
		
		if($(this).val() == "RADC" || $(this).val() == "RJDC") {
			$("#dc-code").removeAttr('disabled');
		} else {
			$("#dc-code").attr('disabled', true);
		}
		if(($(this).val() == "RQP" || $(this).val() == "RQF") && old_status == "RRC") {
			$("#item_bar_code").removeAttr('disabled');
		} else {
			$("#item_bar_code").attr('disabled', true);
		}
		if($(this).val() == "RRC" && old_status != "RRC") {
			$("#warehouseid").removeAttr('disabled');
		} else {
			$("#warehouseid").attr('disabled', true);
		}
		
	});
	
	$('#item_status_form').submit(function(){
		if($("#comment").val().trim() == "") {
			$("#errorMsgBar").html("Please enter a comment for item's approval or rejection");
			return false;
		}
		var overlay = new ProcessingOverlay();
		overlay.show();
		return true;
	});
	
	$("#lms_requeue_button").click(function(){
		var overlay = new ProcessingOverlay();
		overlay.show();
		$.ajax({
			type: "GET",
			url: "",
			data: "action=queue_to_lms&id="+returnid,
			success: function(msg) {
				overlay.hide();
				var response = jQuery.parseJSON(msg);
				if(response.status == true){
					alert('Successfully pushed to LMS');
					window.location.reload();
				}else{
					alert('Failed to push return to LMS'); 
					$("#errorMsgBar").html(response.errorMsg);
				}
			},
			error: function() {
				overlay.hide();
			}
		});
	});
	
	$('#newCommentLink').click(function(){
		$("#newCommentSection").css('display', "block");
		$("#commentSection").css('display', "none");
	});
	
	$('#cancelAddComment').click(function(){
		$("#newCommentSection").css('display', "none");
		$("#commentSection").css('display', "block");
	});
	
	$('#newCommentForm').submit(function(){
		
		if($("#title").val().trim() == "") {
			alert("Please enter title for the comment");
			return false;
		}
		
		if($("#description").val().trim() == "") {
			alert("Please enter description for the comment");
			return false;
		}
		
		var overlay = new ProcessingOverlay();
		overlay.show();
		return true;
	});
	
});

function updateCourierServiceDisplay(courier) {
	if(courier == "other") {
		$("#courier_service_custom").css('display', "block");
	} else {
		$("#courier_service_custom").css('display', "none");
	}
}
