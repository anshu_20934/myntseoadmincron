<script type="text/javascript" src="{$http_location}/skin1/js_script/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="{$http_location}/skin1/js_script/calender.js"></script>
{literal}
<script type="text/javascript">

	$(document).ready(function(){
		enableDisableFormControls();
		
		$('#ccdisposition').change(function(){
			if ($(this).val() == 'CM' || $(this).val() == 'WRN' || $(this).val() == 'INV') {
				$('#resolution_lbl').show();
				$('#resolution').removeAttr('disabled');
				$('#resolution_input').show();
				$(".res_option").show();			
			} else {
				$('#resolution_lbl').hide();
				$('#resolution').attr('disabled', 'disabled');
				$('#resolution_input').hide();			
			}
			if($(this).val() == 'WRN' || $(this).val() == 'INV'){
				$(".res_option").hide();
				$("#RJ").show();
			}
		});			
	});

	function enableDisableFormControls(){
		var currst = $('#currst').val();
		$('#update_btn').hide();
		$('#add_btn').hide();
	
		if (currst == '') {
			$('#courier').removeAttr('disabled');
			$('#comment_row').remove();
			$('#cc_feed_back').remove();
			$('#add_btn').show();
			$('#attempt_count').remove();	
		} else {
			$('#update_btn').show();
			$('#courier_row').remove();
			$('#attempt_count').attr('readonly', 'readonly');
			$('#reason').attr('readonly', 'readonly');
			$('#delivery_attempts').remove();
			if (currst == 'UDCC') {
				$('#cc_feed_back').remove();
			} else if (currst == 'END') {
				$('#cc_feed_back').remove();
				$('#activity_row').remove();
				$('#comment_row').remove();				
				$('#update_btn').hide();	
			}					
		}		
		if ($('#ccdisposition').val() == 'CM') {
			$('#resolution_lbl').show();
			$('#resolution').removeAttr('disabled');
			$('#resolution_input').show();			
		} else {
			$('#resolution_lbl').hide();
			$('#resolution').attr('disabled', 'disabled');
			$('#resolution_input').hide();			
		}
	}

	function updateUDOrder(){		
		var nextstate = $('#rt_action').val();
		
		if (nextstate == '-') {
			alert('Select an UD action');
			return false;
		}

		if ((nextstate == 'UDC1') || (nextstate == 'UDC2') || (nextstate == 'UDC3')) {
			var ccdisposition = $('#ccdisposition').val();
			if (ccdisposition == '-') {
				alert('Select a customer disposition');
				return false;
			}
			if (ccdisposition == 'CM' || ccdisposition == 'WRN' || ccdisposition == 'INV' ) {
				var ccresolution = $('#resolution').val();
				if (ccresolution == '-') {
					alert('Select a customer resolution');
					return false;
				}
			} 		
		} else {
				
		}

		$('#action').val('update');
		return true;	
	}
	
	function createUDOrder(){
		if ($('#rt_action').val() == '-') {
			alert('Select a UD Action');
			return false;
		}			
		if ($('#delivery_attempts').val() == '-') {
			alert("Select a failed delivery count");
			return false;
		}
		if (isEmptyString($('#reason').val())) {
			alert('Enter a reason for undelivery');
			return false;
		}
		if (isEmptyString($('#tracking_num').val())) {
			alert('Enter tracking number');
			return false;	
		}	

		$('#action').val('add');
		return true;	
	}

	function isEmptyString(s){
		return (s.replace(/\s/g,'') == '');	
	}
		
</script>
{/literal}

<h1 style="text-align:center;">Undelivered Order Details</h1>
{if $errormsg }<h2 style="color:red;">{$errormsg}</h2>{/if}
<h3>Order # <a href="{$http_location}/admin/order.php?orderid={$orderData.orderid}">{$orderData.orderid}</a></h3>

<form name="statusForm" action="" method="post">
        <input type="hidden" id="currst" name="currst" value="{$orderData.currstate}">
        <input type="hidden" id="action" name="action">
	<input type="hidden" id="iscod" name="iscod" value="{$orderData.iscod}">	

<table cellspacing="0" cellpadding="0" width="100%">
	<tr id="reason_row">
		<td align="right"><label>Order Id : </label></td>
                <td align="left">
                        <input type="text" id="order_id" name="order_id" readonly="readonly" value="{$orderData.orderid}">
                </td>
                <td colspan="2">&nbsp;</td>
        </tr>
	
        <tr class="blank_row"><td colspan="4">&nbsp;</td></tr>
        <tr id="address_row1">
                <td align="right"><label>Customer Name : </label></td>
                <td align="left">
                        <input type="text" name="cust_name" id="cust_name" readonly="readonly" value="{$orderData.custname}">
                </td>
                <td align="right"><label>Customer Address : </label></td>
                <td align="left">
                        <textarea id="cust_addr" name="cust_addr" cols="40" rows="2" readonly="readonly">{$orderData.pickupaddress}</textarea>
                </td>
        </tr>

	<tr id="address_row2">
                <td align="right"><label>City : </label></td>
                <td align="left"><input type="text" id="cust_city" name="cust_city" readonly="readonly" value="{$orderData.pickupcity}"></td>
                <td align="right"><label>State : </label></td>
                <td align="left"><input type="text" id="cust_state" name="cust_state" readonly="readonly" value="{$orderData.pickupstate}"></td>
        </tr>

        <tr id="address_row3">
                <td align="right"><label>Country : </label></td>
                <td align="left"><input type="text" id="cust_country" name="cust_country" readonly="readonly" value="{$orderData.pickupcountry}"></td>
                <td align="right"><label>ZIP Code : </label></td>
                <td align="left"><input type="text" id="cust_zipcode" name="cust_zipcode" readonly="readonly" value="{$orderData.pickupzipcode}"></td>
        </tr>

        <tr id="address_row4">
                <td align="right"><label>Contact # : </label></td>
                <td align="left"><input type="text" readonly="readonly" name="contact_no" id="contact_no" readonly="readonly" value="{$orderData.phonenumber}"></td>
                <td colspan="2">&nbsp;</td>
        </tr>
		<tr id="address_row3">
                <td align="right"><label>Courier : </label></td>
                <td align="left"> &nbsp;{$orderData.courier}</td>
                <td align="right"><label>Tracking Number: </label></td>
                <td align="left"> &nbsp;{$orderData.trackingno}</td>
        </tr>
        <tr id="address_row3">
                <td align="right"><label>Queued Date : </label></td>
                <td align="left"> &nbsp;{$orderData.queued_date}</td>
                <td align="right"><label>Shipped Date: </label></td>
                <td align="left"> &nbsp;{$orderData.shippeddate}</td>
        </tr>
        <tr id="address_row3">
                <td align="right"><label>Age in hrs : </label></td>
                <td align="left"> &nbsp;{$orderData.age}</td>
                <td align="right"><label>Payment: </label></td>
                <td align="left"> &nbsp;
        			{if $orderData.iscod == 1}
        				COD
        			{else}
        				Online Payment
        			{/if}        
                </td>
        </tr>
        
	<tr class="blank_row"><td colspan="4">&nbsp;</td></tr>
	<tr id="activity_row">
                <td align="right"><label>Current State : </label></td>
                <td align="left"><input id="curr_st" type="text" disabled="disabled" value="{$allstates[$orderData.currstate]}"></td>
                <td align="right"><label>UD Action : </label></td>
                <td align="left">
                        <select id="rt_action" name="rt_action">
                                <option value="-">--- Select a UD  action ---</option>
                                {foreach from=$rtactions key=k item=v}
                                        <option value="{$k}">{$v}</option>
                                {/foreach}
                        </select>
                </td>
        </tr>

	<tr id="reasons_row">
		<td align="right"><label># Failed Delivery Attempts : </label></td>
		<td align="left">
			<select name="delivery_attempts" id="delivery_attempts">
				<option value="-">--- Select a Value ---</option>
				<option value="1">1</option>
				<option value="2">2</option>
				<option value="3">3</option>
				<option value="4">4</option>		
			</select>
			<input id="attempt_count" type="text" name="attempt_count"  value="{$orderData.attemptcount}">
		</td>
		<td align="right"><label>Undelivery Reason : </label></td>	
		<td align="left"><input type="text" name="reason" id="reason" value="{$orderData.reason}"></td>
	</tr>

	<tr id="courier_row">
                <td align="right"><label id="label_courier">Courier : </label></td>
                <td align="left"><input type="text" name="courier" id="courier" readonly="readonly" value="{$orderData.courier}"></td>
		<td align="right"><label id="label_trackingnum">Tracking # : </label></td>
                <td align="left"><input type="text" id="tracking_num" name="tracking_num" value="{$orderData.trackingnum}"></td>			
        </tr>

	<tr><td colspan="2">&nbsp;</td></tr>
	<tr id="cc_feed_back">
		<td align="right"><label>CC Dispositions</label></td>
		<td align="left">
			<select name="ccdisposition" id="ccdisposition">
				<option value="-">--- Select a CC Disposition ---</option>
				{foreach from=$ccdispositions key=k item=d }
                                        <option value="{$k}">{$d}</option>
                                {/foreach}	
			</select>
		</td>
		<td align="right" id="resolution_lbl"><label>Resolution : </label></td>
		<td align="left" id="resolution_input">
			<select name="resolution" id="resolution" disabled="disabled">
				<option value="-">--- Select an option ---</option>
				{foreach from=$ccresolution key=k item=d}
					<option value="{$k}" class= "res_option" id="{$k}">{$d}</option>
				{/foreach}
			</select>
		</td>
	</tr>

	<tr><td colspan="4">&nbsp;</td></tr>
	<tr id="comment_row">
                <td align="right"><label>Comment : </label></td>
                <td align="left" colspan="3"><textarea wrap="ON" cols="70" rows="6" id="comment" name="comment" value="comment"></textarea></td>
        </tr>
	
	<tr><td colspan="4">&nbsp;</td></tr>
        <tr>
                <td>&nbsp;</td>
                <td align="right"> <input type="submit" id="update_btn" value="Update" onclick="javascript:return updateUDOrder();"></td>
                <td align="left"><input type="submit" id="add_btn" value="Create" onclick="javascript:return createUDOrder();"></td>
                <td>&nbsp;</td>
        </tr>
				
</table>

</form>

<br />
<br />

<table border="1">
        <thead><b>Edit History</b></thead>

        <tr>   
                <th>Activity</th>
                <th>Done by</th>
                <th>Recorded on</th>
                <th>Elapsed Time</th>
                <th>SLA </th>
                <th>Remark</th>
        </tr>

        {foreach from=$edithistory key=k item=data}
                <tr {if $data.time_since_last_activity > $data.sla_time }style="background:red;"{/if}>
                        <td>{$data.activity}</td>
                        <td>{$data.adminuser}</td>
                        <td>{$data.recorddate}</td>
                        <td style="text-align:center;">{$data.time_since_last_activity_descr}</td>
                        <td style="text-align:center;">{$data.sla_time_descr}</td>
                        <td>{$data.remark}</td>
                </tr>
        {/foreach}
</table>
