{* $Id: cancelorders.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
{include file="main/include_js.tpl" src="main/popup_product.js"}
{literal}
  <script language="Javascript">
      function confirmDelete()
      {
          var flag = confirm('Are you sure you want to delete?')
          if(!flag)
          {
             return false;
          }
          else
          {
          	 document.groupfrm.mode.value = 'delete';
          	 document.groupfrm.submit();
          	 return true;
          }
      }
  </script>
{/literal}
{capture name=dialog}
<div>{$top_message}</div>
<div>
<form action="" method="post" name="addfrm" enctype="multipart/form-data">
<input type="hidden" name="mode"  />
<table cellpadding="3" cellspacing="1" width="100%">

<tr class="TableHead">
	<td width="40%">Page URL</td>
	<td width="40%">Parametrized Page key</td>
	<td width="20%"></td>

</tr>

<tr{cycle values=", class='TableSubHead'"}>
    <td align="center"><input type="text" name="page_url"  style="width:80%"/></td>
	<td align="center"><input type="text" name="page_key"  style="width:80%"/></td>
	<td colspan="5" class="SubmitBox">
	<input type="button" value="Add" onclick="javascript: document.addfrm.mode.value = 'add'; document.addfrm.submit();"/>
	</td>
</tr>


<tr>
</tr>
</table>
</form>
</div>

<div>
<form action="" method="post" name="searchfrm" enctype="multipart/form-data">
<input type="hidden" name="mode"  />
<input type="hidden" name="page" value="0"/>
<table cellpadding="3" cellspacing="1" width="100%">

<tr class="TableHead">
	<td width="40%">Search</td>
	<td width="10%">Page Size</td>
	<td width="20%"></td>
	<td width="35%"></td>
</tr>
<tr>
    <td align="center"><input type="text" name="search_keyword" value="{$search_keyword}" style="width:80%"/></td>
    <td align="center"><input type="text" name="page_size" value="{$page_size}" maxlength="2" style="width:80%"/></td>
	<td class="SubmitBox">
	<input type="button" value="Search" onclick="javascript: document.searchfrm.mode.value = 'search'; document.searchfrm.submit();"/>
	</td>
    <td>
{if $prev_page gt -1}
	<input  type="button" value="Previous" 
            onclick="javascript: 
            document.searchfrm.mode.value = 'search';
            document.searchfrm.page.value = {$prev_page}; 
            document.searchfrm.submit();"/>
{/if}
{if $next_page}
	<input  type="button" value="Next" 
            onclick="javascript: 
            document.searchfrm.mode.value = 'search';
            document.searchfrm.page.value = {$next_page}; 
            document.searchfrm.submit();"/>
{/if}
</td>
</tr>

</table>
</form>
</div>
<div>
<form action="" method="post" name="groupfrm" enctype="multipart/form-data">
<input type="hidden" name="mode"  />
<table cellpadding="3" cellspacing="1" width="100%">

<tr class="TableHead">
	<td width="5%">&nbsp;</td>
	<td width="32%">Page URL</td>
	<td width="26%">Parametrized Page key</td>
	<td width="5%">Auto Suggest</td>
	<td width="32%">Alt Text</td>

</tr>

{if $landing_pages}

{section name=grid loop=$landing_pages}

<tr{cycle values=", class='TableSubHead'"}>
    <input type="hidden" name="posted_data[{$landing_pages[grid].id}][gid]" value={$landing_pages[grid].id} />
	<td><input type="checkbox" name="posted_data[{$landing_pages[grid].id}][to_delete]" /></td>
    <td align="center"><input type="text" name="posted_data[{$landing_pages[grid].id}][page_url]" value="{$landing_pages[grid].page_url}" style="width:80%"/></td>
	<td align="center"><input type="text" name="posted_data[{$landing_pages[grid].id}][page_key]" value="{$landing_pages[grid].page_key}" style="width:80%"/></td>
	<td><input type="checkbox" name="posted_data[{$landing_pages[grid].id}][is_autosuggest]" {if $landing_pages[grid].is_autosuggest}checked{/if}/></td>
    <td align="center"><input type="text" name="posted_data[{$landing_pages[grid].id}][alt_text]" value="{$landing_pages[grid].alt_text}" style="width:80%"/></td>
</tr>

{/section}
<td colspan="5"><br /><br /></td>
{else}

<tr>
 <td colspan="4" align="center">No Landing Pages available.</td>
</tr>
{/if}

<!--<tr>
<td colspan="5"><br /><br />{include file="main/subheader.tpl" title=$lng.lbl_add_product}</td>
</tr> -->

<tr>
	<td colspan="5" class="SubmitBox">
	<input type="button" value="{$lng.lbl_delete_selected|escape}" onclick="return confirmDelete();" />
	<input type="submit" value="{$lng.lbl_update|escape}" onclick="javascript: document.groupfrm.mode.value = 'update'; document.groupfrm.submit();" />
	</td>
</tr>


</table>
</form>
</div>
{/capture}
{include file="dialog.tpl" title='Landing pages' content=$smarty.capture.dialog extra='width="100%"'}
