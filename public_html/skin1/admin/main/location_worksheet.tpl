{* $Id: featured_products.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
{include file="main/include_js.tpl" src="main/popup_product.js"}

<link rel="stylesheet" href="{$SkinDir}/skin1_admin.css" />
<script language="javascript" src="../skin1/js_script/item_assignment.js"></script> 

{capture name=dialog}

<form name="location_task" action="location_worksheet.php" method="post">
	<input type="hidden" name="mode" id="mode" value="search">
		<div class="ITEM_PARENT_DIV" style="width:650px;">
		<div class="ITEM_FIRST_CHILD_DIV" style="width:auto;">
        <div class="ITEM_LABLE_DIV">Location</div>
        <div class="ITEM_FIELD_DIV" style="width:auto;">
            <select name="item_location" style="width:140px;" onchange="javascript:document.location_task.submit();">
                <option value="0">---select location---</option>
				{section name=loc loop=$locations}
				   <option value="{$locations[loc].id}" {if $smarty.post.item_location == $locations[loc].id || $smarty.get.item_location == $locations[loc].id} selected {/if}>{$locations[loc].location_name}</option>
				   {assign var=loc_selected value=""}
				{/section}
		    </select>  
        </div> 
        </div>
        </div>
</form>

{/capture}
{include file="dialog.tpl" title="Select Location" content=$smarty.capture.dialog extra='width="100%"'}

<br/><br/>

{if $locationid != ""}
{capture name=dialog}
<div style="float:left;width:auto;height:auto;border : 0px solid #000000;">
{include file="customer/main/navigation.tpl"}
</div>
    <form action="" method="post" name="">
        <input type="hidden" name="mode"  />
        <input type="hidden" name="page" id="page" value="{$smarty.get.page}"  />
         <input type="hidden" name="query_string" id="query_string" value="{$query_string}"  />
        <div class="DIV_TABLE" style="width:1040px;">
	<div class="ITEM_SEARCH_BUTTON_DIV" style="width:1035px;">
            <input type="button" value="print task list" style="margin-left : 350px;" onclick="javascript:printLocationTaskList('{$locationid}');" />
        </div>
        <div class="DIV_TH" style="width:1040px;">
            <div class="DIV_TD" style="width:55px;">Item Id</div>
            <div class="DIV_TD" style="width:65px;">order id</div>
            <div class="DIV_TD" style="width:85px;">status</div>
            <div class="DIV_TD" style="width:125px;">item name</div>
            <div class="DIV_TD" style="width:70px;">quantity</div>
	    	<div class="DIV_TD" style="width:100px;">Assignee</div>
            <div class="DIV_TD" style="width:190px;">assignment date</div>
            <div class="DIV_TD" style="width:190px;">sent date</div>
            <div class="DIV_TD" style="width:75px;">priview image</div>
        </div>
        
        {if $total_items > 0 }
        
        {section name=item loop=$itemResult max=$sizeofitemresult}
         <div class="DIV_TR" style="width:1040px;">
            <div class="DIV_TR_TD" style="width:55px;"><p>{$itemResult[item].itemid}</p></div>
            <div class="DIV_TR_TD" style="width:65px;"><a href="javascript:void(0);" onclick="javascript:window.open('order.php?orderid={$itemResult[item].orderid}');"><p>{$itemResult[item].orderid}</p></a></div>
            <div class="DIV_TR_TD" style="width:85px;"><p>
            {assign var=i_status value=$itemResult[item].item_status}
            {if $i_status == 'N'} {assign var=status_color value="#EE0F14"} {/if}
            {if $i_status == 'A'} {assign var=status_color value="#77AB3D"} {/if}
            {if $i_status == 'D'} {assign var=status_color value="#80FFFF"} {/if}
            {if $i_status == 'F'} {assign var=status_color value="#EF940A"} {/if}
            {if $i_status == 'H'} {assign var=status_color value="#F5FF42"} {/if}
            {if $i_status == 'S'} {assign var=status_color value="#04FF82"} {/if}
            <select name="item_status" id="item_st_{$itemResult[item].itemid}" onchange="javascript:itemDoneLocation('status','{$itemResult[item].itemid}','{$itemResult[item].item_status}');" 
            {if $i_status != 'A' && $i_status != 'S'} disabled="disabled" {/if} style="background-color:{$status_color};width:80px;">
                 {foreach from=$status item=st_value key=st_key}
                    <option value="{$st_key}" {if $i_status == $st_key} selected {/if} style="color:#000000;background-color:{$st_value.color};">{$st_value.status}</option>
                {/foreach}
			</select></p>
            </div>
            <div class="DIV_TR_TD" style="width:125px;"><p>{$itemResult[item].stylename}</p></div>
            <div class="DIV_TR_TD" style="width:70px;"><p>{$itemResult[item].amount}</p>
                <input type="hidden" name="qty_{$itemResult[item].itemid}" id="qty_{$itemResult[item].itemid}" value="{$itemResult[item].amount}">
            </div>
	    <div class="DIV_TR_TD" style="width:100px;">{$itemResult[item].name}</div>
            <div class="DIV_TR_TD" style="width:190px;">{$itemResult[item].ass_date}</div>
            <div class="DIV_TR_TD" style="width:190px;">{if $i_status == 'S'}{$itemResult[item].sent_date}{else}--{/if}</div>
            <div class="DIV_TR_TD" style="width:75px;"><a href="javascript:void(0);" onclick="javascript:window.open('view_print_design.php?productId={$itemResult[item].productid}&orderid={$itemResult[item].orderid}');"><p>preview image</p></a></div>
         </div>
        {/section}
        <div class="ITEM_SEARCH_BUTTON_DIV" style="width:1035px;">
            <input type="button" value="print task list" style="margin-left : 350px;" onclick="javascript:printLocationTaskList('{$locationid}');" />
        </div>
        {else}
        <div class="DIV_TR" style="width:750px;">
            <div class="DIV_TR_TD" style="width:750px;"><strong>task has not been assigned</strong></div>
        </div>
        {/if}
        
        </div>
    </form>
    
 <div style="float:left;width:auto;height:auto;border : 0px solid #000000;">   
{include file="customer/main/navigation.tpl"}
</div>
{/capture}
{include file="dialog.tpl" title="Location task list" content=$smarty.capture.dialog extra='width="100%"'}
{/if}
