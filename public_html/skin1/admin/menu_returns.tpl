{* $Id: menu.tpl,v 1.68.2.2 2006/06/20 05:52:05 svowl Exp $ *}

{capture name=menu}
<br>
<li><a href="{$catalogs.admin}/old_returns_tracking/bulk_rto_queueing.php" class="VertMenuItems">Receive Items at Warehouse</a></li>
<li><a href="{$catalogs.admin}/old_returns_tracking/receive_returns.php" class="VertMenuItems">Receive Returns</a></li>
<br><br>
<li><a href="{$catalogs.admin}/old_returns_tracking/old_undelivered_order_tracking.php" class="VertMenuItems">Undelivered Order Tracking</a></li>
<li><a href="{$catalogs.admin}/old_returns_tracking/old_returnedorders_tracking.php" class="VertMenuItems">RTO Tracking</a></li>
<!-- <li><a href="{$catalogs.admin}/old_returns_tracking/old_returns_tracking.php" class="VertMenuItems">Returns Tracking</a></li> -->
<br/>
<li><a href="{$catalogs.admin}/returns/bulk_returns_decline.php" class="VertMenuItems">Bulk Uploader - Returns Decline</a></li>
<li><a href="{$catalogs.admin}/old_returns_tracking/bulk_uploader_for_ud.php" class="VertMenuItems">Bulk Uploader - UDQ</a></li>
<li><a href="{$catalogs.admin}/old_returns_tracking/bulk_uploader_for_udls.php" class="VertMenuItems">Bulk Uploader - UDLS</a></li>
<li><a href="{$catalogs.admin}/old_returns_tracking/bulk_uploader.php" class="VertMenuItems">Bulk Uploader - RTO Reasons</a></li>
<li><a href="{$catalogs.admin}/old_returns_tracking/bulk_rto_preq.php" class="VertMenuItems">Bulk Uploader - RTO PREQ</a></li>
<li><a href="{$catalogs.admin}/old_returns_tracking/bulk_rto_cancellations.php" class="VertMenuItems">Bulk Uploader - RTO Cancellations</a></li>
<br/>
<br/>
<li><a href="{$catalogs.admin}/old_returns_tracking/sla_ud.php" class="VertMenuItems">SLA - UDs</a></li>
<li><a href="{$catalogs.admin}/old_returns_tracking/sla_rtos.php" class="VertMenuItems">SLA - RTOs</a></li>
<!-- <li><a href="{$catalogs.admin}/old_returns_tracking/sla_returns.php" class="VertMenuItems">SLA - Returns</a></li> -->
<br/>
<li><a href="{$catalogs.admin}/returns/return_requests.php" class="VertMenuItems">Manage Returns</a></li>
    <li><a href="{$catalogs.admin}/returns/bulk_upload_store_returns.php" class="VertMenuItems">Flipkart Returns Uploader</a></li>
<li><a href="{$catalogs.admin}/returns/updatereturnsinbulk.php" class="VertMenuItems">Returns Bulk Uploader</a></li>
<li><a href="{$catalogs.admin}/returns/sladashboard.php" class="VertMenuItems">Returns SLA Dashboard</a></li>
<li><a href="{$catalogs.admin}/returns/slaeditor.php" class="VertMenuItems">Returns SLA Editor</a></li>
<li><a href="{$catalogs.admin}/returns/returns_reports.php" class="VertMenuItems">Returns Reports</a></li>
{/capture}
{include file="menu.tpl" dingbats="dingbats_categorie.gif" menu_title=Returns menu_content=$smarty.capture.menu }