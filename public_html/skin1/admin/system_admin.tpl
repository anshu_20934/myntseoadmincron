{* $Id: menu_admin.tpl,v 1.34 2005/11/17 06:55:36 max Exp $ *}
{capture name=menu}
<li><a href="{$catalogs.admin}/general.php" class="VertMenuItems">{$lng.lbl_summary}</a></li>
<li><a href="{$catalogs.admin}/db_backup.php" class="VertMenuItems">{$lng.lbl_db_backup_restore}</a></li>
<li><a href="{$catalogs.admin}/import.php" class="VertMenuItems">{$lng.lbl_import_export}</a></li>
<li><a href="{$catalogs.admin}/memberships.php" class="VertMenuItems">{$lng.lbl_membership_levels}</a></li>
<li><a href="{$catalogs.admin}/card_types.php" class="VertMenuItems">{$lng.lbl_credit_card_types}</a></li>
<li><a href="{$catalogs.admin}/titles.php" class="VertMenuItems">{$lng.lbl_titles}</a></li>
<li><a href="{$catalogs.admin}/file_edit.php" class="VertMenuItems">{$lng.lbl_edit_templates}</a></li>
<li><a href="{$catalogs.admin}/file_manage.php" class="VertMenuItems">{$lng.lbl_files}</a></li>
<li><a href="{$catalogs.admin}/configuration.php" class="VertMenuItems">{$lng.lbl_general_settings}</a></li>
<li><a href="{$catalogs.admin}/images_location.php" class="VertMenuItems">{$lng.lbl_images_location}</a></li>
<li><a href="{$catalogs.admin}/languages.php" class="VertMenuItems">{$lng.lbl_languages}</a></li>
<li><a href="{$catalogs.admin}/editor_mode.php" class="VertMenuItems">{$lng.lbl_webmaster_mode}</a></li>
<li><a href="{$catalogs.admin}/modules.php" class="VertMenuItems">{$lng.lbl_modules}</a></li>
<li><a href="{$catalogs.admin}/payment_methods.php" class="VertMenuItems">{$lng.lbl_payment_methods}</a></li>
<li><a href="{$catalogs.admin}/patch.php" class="VertMenuItems">{$lng.lbl_patch_upgrade}</a></li>
<li><a href="{$catalogs.admin}/speed_bar.php" class="VertMenuItems">{$lng.lbl_speed_bar}</a></li>
<li><a href="{$catalogs.admin}/admin_roles_access.php" class="VertMenuItems">Manage Pages Roles Access</a></li>
{/capture}
{ include file="menu.tpl" dingbats="dingbats_categorie.gif" menu_title='System Admin' menu_content=$smarty.capture.menu }