{capture name=pl_report}
    <div>
        <div style="float:left;background-color:#f0f0f0;">
            <form name="retrieve_pl"  action="{$http_location}/admin/payment_logs.php" method="post">
            <table cellpadding="3" cellspacing="1" width="100%">
                <tr>
                    <td>
                        <table>
                            <tr>    
                                <div>
                                    Retrieve for these Order Ids (Comma Seperated Values) <textarea cols="52" rows="2" name="csv-file-list" id="csv-file-list"></textarea>
                                </div>
                            </tr>    

                            <tr>
                               <td colspan="4">&nbsp;</td>                                
                            </tr>

                            <tr>
                               <td colspan="2">&nbsp;</td>
                                <td colspan="2"><input type="submit" value="Retrieve Payment Logs" class="button"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            </form>
        </div>
        <div style="clear:both;"></div>
    </div>

    <br/><hr>
   
    <div style="width:1125px;">
        <table cellpadding="3" cellspacing="1" width="90%">
            <tr class="TableHead">
                <th rowspan="1">Order Id</th>
                <th rowspan="1">Payment Gateway Name</th>
                <th rowspan="1">Payment Option</th>
                <th rowspan="1">Amount Paid</th>
                <th rowspan="1">Is Complete</th>
            </tr>

            {foreach from=$plogs item=orderlog key=orderid} 
                <tr style="background-color:#f0f0f0;" align="center">
                    <td>{$orderlog.orderId}</td>
                    <td>{$orderlog.gatewayName}</td>
                    <td>{$orderlog.paymentOption}</td>
                    <td>{$orderlog.amount}</td>
                    <td>{$orderlog.isComplete}</td>
                </tr>
            {/foreach}

        </table>

        {*<table>
             <tr>
                <td align="left"><br/>
                    <a href="{$http_location}/admin/payment_logs.php?mode=exportPLReport">Export to XLS</a>
                </td>
            </tr>
        </table>
        <form action="{$http_location}/admin/payment_logs.php?mode=exportPLReport">
            <input type="hidden" name="order_csv" value="" />
            <button>Export to XLS</button>
        </form>*}
    </div>
{/capture}
{include file="dialog.tpl" title="Payment Service Logs" content=$smarty.capture.pl_report extra='width="100%"'}