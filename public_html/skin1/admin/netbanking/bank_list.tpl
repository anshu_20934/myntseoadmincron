{include file="main/include_js.tpl" src="main/popup_product.js"}
<div style="padding-left: 320px;padding-bottom: 20px;font-size: 14px;">
	<a style="padding-left: 10px;" href="{$http_location}/admin/netbanking/activate_gateway.php"> ADD/EDIT GATEWAY MAPPING</a>
	<a style="padding-left: 10px;" href="{$http_location}/admin/netbanking/add_gateway.php"> ADD/EDIT GATEWAYS </a>
</div>
{literal}
<script type="text/javascript">
function delete_bank_name(id){
    var r=confirm("Are you sure you want to delete this bank");
    if(r == true){
        document.updatebankname.mode.value = 'delete';
        document.updatebankname.bank_id.value = id;
        document.updatebankname.submit();
    }
}

function add_BankName(){
    if (jQuery.trim(document.updatebankname.bank_name_new.value) == '') {
        alert('Bank Name cannot be empty');
        return;
    } 
    document.updatebankname.mode.value = 'add';
    document.updatebankname.submit();
}
function edit_bank_gateway(id){
	window.location = "add_bank_gateway.php?id="+id;
}

</script>
{/literal}

{capture name=dialog}
<form action="{$action_php}" method="post" name="updatebankname">
<input type="hidden" name="mode" />
<input type="hidden" name="bank_id" />
<table cellpadding="3" cellspacing="1" width="100%">
<tr class="TableHead">
	<td width="20%">ID</td>
	<td width="40%">Name</td>
	<td width="20%">Popular</td>
	<td width="20%">Action</td>
</tr>

{if $bank_list}
{section name=key_num loop=$bank_list}
<tr>
	<td align="center" style="left-padding:30px">{$bank_list[key_num].id}.</td>
	<td align="center" style="left-padding:30px"><input name="update_name_field[{$bank_list[key_num].id}]" type="text" value="{$bank_list[key_num].name}" /></td>
	<td align="center" style="left-padding:30px"><input name="update_popular_field[{$bank_list[key_num].id}]" type="checkbox" {if $bank_list[key_num].is_popular neq 0}checked="true"{/if} /></td>
	<td align="center"><input type="button" value="Add/Edit Gateways" onclick="javascript: edit_bank_gateway('{$bank_list[key_num].id}')" /><input type="button" value="Delete" onclick="javascript: delete_bank_name('{$bank_list[key_num].id}')" /></td>	
</tr>
{/section}
{else}
<tr>
<td colspan="4" align="center">No Banks added as yet</td>
</tr>
{/if}

<tr>
	<td></td>
	<td align="center" style="left-padding:30px"><input name="bank_name_new" type="text" value="" /></td>
	<td align="center" style="left-padding:30px"><input name="bank_popular_new" type="checkbox" /></td>
	<td align="center">
		<input type="button" value="Add" onclick="javascript: add_BankName()" />
	</td>
</tr>
<tr>
	<td></td>
	<td></td>
	<td></td>
	<td class="SubmitBox" align="right">
	   <input type="button" value="Update" onclick="javascript: document.updatebankname.mode.value = 'update'; document.updatebankname.submit();" />
	</td>
</tr>
<tr>
	<td></td>
	<td></td>
	<td></td>
	<td class="SubmitBox" align="right">
	   <input type="button" value="Clear XCache" onclick="javascript: document.updatebankname.mode.value = 'clearCache'; document.updatebankname.submit();" />
	</td>
</tr>
</table>
</form>

{/capture}
{include file="dialog.tpl" title="Bank Name List" content=$smarty.capture.dialog extra='width="100%"'}