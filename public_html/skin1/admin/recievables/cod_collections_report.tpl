{literal}
	<style type="text/css">
		.ui-datepicker-calendar{
			display:none;
		}
	</style>
{/literal}
<link rel="stylesheet" href="/skin1/myntra_css/jquery-ui-datepicker.css" />
<div style="width:40%;float:left;padding-left:30px;margin-top:30px;margin-bottom:20px;">
	<span style="font-weight: bold;"> COD Collections</span>
</div>
<div style="width:70%;float:left;">
	<form action = "/admin/recievables/cod_collections_report.php" method="POST">
		<div style="float:left;padding-left:30px;padding-right:20px;">
			{html_options name=carrierid options=$couriers selected="DEFAULT"}
		</div>
		<div style="float:left;padding-left:30px;padding-right:20px;">
			<span>From</span>
			<input name="fromDate" id="fromDate" class="date-picker" />
		</div>
		<div style="float:left;padding-left:30px;padding-right:20px;">
			<span>To</span>
			<input name="toDate" id="toDate" class="date-picker" />
		</div>	
		<input type="submit" name="submit" value="submit" />
	</form>
</div>
{if isset($data)}
	<div style="width:75%;float:left;margin-top:20px;margin-left:30px;">
		<div style="width:99%;float:left;">
			<div style="width:10%;float:left;text-align: center;padding:3px 0px;border:1px solid black;background: gray;">
				<span style="font-weight: bold;">Month</span>
			</div>
			<div style="width:25%;float:left;text-align: center;padding:3px 0px;border-top:1px solid black;border-right:1px solid black;border-bottom:1px solid black;background: gray;">
				<span style="font-weight: bold;">No. of orders</span>
			</div>
			<div style="width:15%;float:left;text-align: center;padding:3px 0px;border-top:1px solid black;border-right:1px solid black;border-bottom:1px solid black;background: gray;">
				<span style="font-weight: bold;">TTL</span>
			</div>
		</div>
		{foreach from=$data item=row}
			<div style="width:99%;float:left" id="entry_{$row.id}">
				<div style="width:10%;float:left;text-align: center;padding:3px 0px;border-right:1px solid black;border-bottom:1px solid black;border-left:1px solid black;">
					<span>{$row.monthName}</span>
				</div>
				<div style="width:25%;float:left;text-align: center;padding:3px 0px;border-right:1px solid black;border-bottom:1px solid black;">
					{$row.orders} 
				</div>
				<div style="width:15%;float:left;text-align: center;padding:3px 0px;border-right:1px solid black;border-bottom:1px solid black;">
					<span>{$row.amount}</span>
				</div>
				<div style="width:10%;float:left;padding:3px 5px;">
					<a href="/admin/recievables/cod_collections_report.php?fromDate={$row.mon}-{$row.yr}&carrierid={$carrierid}&mode=download" target="_blank">Download</a>
				</div>
			</div>
		{/foreach}
</div>
{/if}

<script type="text/javascript" src="/skin1/js_script/jquery-1.4.2.min.js" > </script>
<script type="text/javascript" src="/skin1/myntra_js/jquery-ui-1.8.13.custom.min.js" > </script>
<script type="text/javascript" src="/skin1/myntra_js/jquery-ui-datepicker.js" > </script>
{literal}
	<script type="text/javascript">
		$(function() {
		    $('.date-picker').datepicker( {
		        changeMonth: true,
		        changeYear: true,
		        showButtonPanel: true,
		        maxDate:new Date(),
		        dateFormat: 'mm-yy',
		        defaultDate: +0,
		        onClose: function(dateText, inst) { 
		            var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
		            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
		            $(this).datepicker('setDate', new Date(year, month, 1));
		        }
		    });
		});
	</script>
{/literal}	