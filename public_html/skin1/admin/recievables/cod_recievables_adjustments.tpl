{literal}
	<style type="text/css">
		.ui-datepicker-calendar{
			display:none;
		}
	</style>
{/literal}
<link rel="stylesheet" href="/skin1/myntra_css/jquery-ui-datepicker.css" />
<div style="width: 50%;float:left">
	<form id="cod_recievables_adjustments" action="/admin/recievables/cod_recievables_adjustments.php" method="post" >
		<div style="width:40%;float:left">
				<span style="padding-right:20px;">From: </span>
				<input name="month" id="month" class="date-picker" value="{$month}"/>
		</div>
		<div style="width:10%;float:left;">
				<input type="submit" name="Submit" value="Submit" />
		</div>
		{if isset($month)}
		<div style="width:10%;float:left;padding:5px 0px 0px 25px;">
			<a href="javascript:displayAddingDiv();">Add entry</a>
		</div>
		{/if}
	</form>
</div>
{if isset($month)}
	{if !isset($adj_data)}
	<div style="width:90%;float:left;padding-top:10px;">
		No data available.
	</div>	
	{/if}
{/if}
<div style="width:90%;float:left;padding-top:10px;">
	<span  id="errorMessagesDiv" style="color: red;">{$message}</span>
</div>
{if isset($adj_data)}
<div style="width:75%;float:left;margin-top:10px;">
	<div style="width:99%;float:left;">
		<div style="width:10%;float:left;text-align: center;padding:3px 0px;border:1px solid black;background: gray;">
			<span style="font-weight: bold;">Carrier</span>
		</div>
		<div style="width:25%;float:left;text-align: center;padding:3px 0px;border-top:1px solid black;border-right:1px solid black;border-bottom:1px solid black;background: gray;">
			<span style="font-weight: bold;">Description</span>
		</div>
		<div style="width:15%;float:left;text-align: center;padding:3px 0px;border-top:1px solid black;border-right:1px solid black;border-bottom:1px solid black;background: gray;">
			<span style="font-weight: bold;">Amount to be adjusted</span>
		</div>
		<div style="width:10%;float:left;text-align: center;padding:3px 0px;border-top:1px solid black;border-right:1px solid black;border-bottom:1px solid black;background: gray;">
			<span style="font-weight: bold;">Actions</span>
		</div>
	</div>
	{foreach from=$adj_data item=row}
		<div style="width:99%;float:left" id="entry_{$row.id}">
			<div style="width:10%;float:left;text-align: center;padding:3px 0px;border-right:1px solid black;border-bottom:1px solid black;border-left:1px solid black;">
				<span>{$row.courierName}</span>
			</div>
			<div style="width:25%;float:left;text-align: center;padding:3px 0px;border-right:1px solid black;border-bottom:1px solid black;">
				<span>{$row.description}</span>
			</div>
			<div style="width:15%;float:left;text-align: center;padding:3px 0px;border-right:1px solid black;border-bottom:1px solid black;">
				<span>{$row.amount}</span>
			</div>
			<div style="width:10%;float:left;text-align: center;padding:3px 0px;border-right:1px solid black;border-bottom:1px solid black;">
				<span style="cursor: pointer;" onclick="javascript:deleteThis('{$row.id}');">X</span>
			</div>	
		</div>
	{/foreach}
</div>
{/if}
<div style="width:40%;float:left;margin-top:20px;display:none;border:1px solid black;" id="add_entry">
	<div style="width:96%;float:left;padding-left:20px;padding-top:5px;padding-bottom:5px;background: gray;">
		<span style="font-weight: bold;">Create adjustment entry for {$monthName}</span>
	</div>
	<div style="width:95%;float:left;padding:10px 20px;">
		<form id="entry_form" action="/admin/recievables/cod_recievables_adjustments.php" method="post" onsubmit="return false">
			<input type="hidden" name="month" value="{$month}">
			<input type="hidden" name="action" value="add_entry">
			<div style="width:95%;float;left;padding-top:5px;">
				<div style="width:30%;float:left;"><span> Shipping vendor</span></div>
				{html_options name=carrierid options=$couriers selected="DEFAULT"}
			</div>
			<div style="width:95%;float;left;padding-top:5px;">
				<div style="width:30%;float:left;"><span > Description</span></div>
				<input id="description" name="description" type="text" />
			</div>
			<div style="width:95%;float;left;padding-top:5px;">
				<div style="width:30%;float:left;"><span> Amount </span></div>
				<input id="amount" name="amount" type="text" />
			</div>
			<div style="width:95%;float:left;padding-top:10px;padding-left:200px;">
				<input type="button" value="submit" onclick="javascript:submitEntryForm();" />
			</div>
		</form>
	</div>
</div>
<script type="text/javascript" src="/skin1/js_script/jquery-1.4.2.min.js" > </script>
<script type="text/javascript" src="/skin1/myntra_js/jquery-ui-1.8.13.custom.min.js" > </script>
<script type="text/javascript" src="/skin1/myntra_js/jquery-ui-datepicker.js" > </script>
{literal}
	<script type="text/javascript">
		function displayAddingDiv(){
			document.getElementById('add_entry').style.display = "block";
		}
		$(function() {
		    $('.date-picker').datepicker( {
		        changeMonth: true,
		        changeYear: true,
		        showButtonPanel: true,
		        maxDate:new Date(),
		        dateFormat: 'mm-yy',
		        defaultDate: +0,
		        onClose: function(dateText, inst) { 
		            var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
		            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
		            $(this).datepicker('setDate', new Date(year, month, 1));
		        }
		    });
		});
		function submitEntryForm(){
			var description = document.getElementById('description').value;
			var amount = document.getElementById('amount').value;
			if(amount ==null || amount.trim() == ''){
				document.getElementById('errorMessagesDiv').innerHTML = "Please enter Amount";
				return;
			}
			if(description ==null || description.trim() == ''){
				document.getElementById('errorMessagesDiv').innerHTML = "Please enter Description";
				return;
			}
			if(description.length >100){
				document.getElementById('errorMessagesDiv').innerHTML = "Description cannot be more than 100 letters";
				return;
			}
			$("#entry_form").submit();
		}
		function deleteThis(entryId){
			$.ajax({
				url:"/admin/recievables/cod_ajax_adjustments.php",
				data:  "row_id="+entryId+"&action=delete",
				success : function(data){
					if(data == "success"){
						$("#entryMessagesDiv").html("Entry deleted successfully!");
						$("#entry_"+entryId).html("");
					}else{
						$("#entryMessagesDiv").html("Request could not be completed. please contact tech team.");
					}
				}	
			});
		}		
	</script>
{/literal}