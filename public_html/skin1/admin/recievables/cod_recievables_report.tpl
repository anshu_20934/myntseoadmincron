{literal}
	<style type="text/css">
		.col1{
			width:170px;
			float:left;
			height:20px;
			text-align:center;
			border-right:1px solid black;
			border-bottom:1px solid black;
			border-left:1px solid black;
		}
		.othercols{
			width:74px;
			float:left;
			border-right:1px solid black;
			border-bottom:1px solid black;
			height:20px;
			text-align:center;
		}
		.ui-datepicker-calendar{
			display:none;
		}
	</style>
{/literal}	
<link rel="stylesheet" href="/skin1/myntra_css/jquery-ui-datepicker.css" />
<div style="width:40%;float:left;padding-left:20px;margin-top:30px;margin-bottom:20px;">
	<span style="font-weight: bold;"> Recievables Report</span>
</div>
<div style="width:95%;margin-left:20px;margin-bottom:20px;float:left;">
	<form id="cod_recievables_report" action="/admin/recievables/cod_recievables_report.php" method="post" >
		<div style="width:25%;float:left">
				<span style="padding-right:20px;">From: </span>
				<input name="startDate" id="startDate" class="date-picker" value="{$startDate}" />
		</div>
		<div style="width:25%;float:left">
				<span style="padding-right:20px;">To: </span>
				<input name="endDate" id="endDate" class="date-picker" value="{$endDate}" />
		</div>
		<div style="width:10%;float:left;">
				<input type="button" name="Submit" value="Submit" onclick="submitThisForm();"/>
		</div>
		<input name="action" id="action" value="" type="hidden" />
		<div style="width:10%;float:left;">
				<input type="button" name="Recalculate" value="Recalculate" onclick="submitThisForm('recalc');"/>
		</div>
	</form>
</div>
{assign var=smarty_variable value=0} 
{if isset($data)}
<div style="padding-left:20px;float:left">
	{foreach from=$data item=row}
	{if $smarty_variable == 0}
		{assign var = prevClosing  value= $row.starting_value}	
	{/if}
		{ if $smarty_variable % 6 == 0} 
			<div style="width:170px;float:left;">
				<div class="col1" style="height:31px;text-align:center;padding-top:10px;border-top:1px solid black;">
					<span style="font-weight:bold;">Analysis</style>
				</div>
				<div class="col1" style="height:20px;text-align:center;">
					<span style="font-weight:bold;">Opening Recievables</style>
				</div>
				<div class="col1" style="height:20px;text-align:center;">
					<span style="font-weight:bold;">Shipped</style>
				</div>
				<div class="col1" style="height:20px;text-align:center;">
					<span style="font-weight:bold;">Sales Returns</style>
				</div>
				<div class="col1" style="height:20px;text-align:center;">
					<span style="font-weight:bold;">Auto Discounts</style>
				</div>
				<div class="col1" style="height:20px;text-align:center;">
					<span style="font-weight:bold;">Coupon Discounts</style>
				</div>
				<div class="col1" style="height:20px;text-align:center;">
					<span style="font-weight:bold;">PG Discounts</style>
				</div>
				<div class="col1" style="height:20px;text-align:center;">
					<span style="font-weight:bold;">Cashback</style>
				</div>
				<div class="col1" style="height:20px;text-align:center;">
					<span style="font-weight:bold;">Collections</style>
				</div>
				<div class="col1" style="height:20px;text-align:center;">
					<span style="font-weight:bold;">Adjustments</style>
				</div>
				<div class="col1" style="height:20px;text-align:center;">
					<span style="font-weight:bold;">Closing Recievables</style>
				</div>
				<div class="col1" style="height:20px;text-align:center;border-left:none;border-right:none;background: gray;">
					<span style="font-weight:bold;">&nbsp;</style>
				</div>
				<div class="col1" style="height:20px;text-align:center;">
					<span style="font-weight:bold;">Customer Returns</style>
				</div>
				<div class="col1" style="height:20px;text-align:center;">
					<span style="font-weight:bold;">RTO</style>
				</div>
				<div class="col1" style="height:20px;text-align:center;">
					<span style="font-weight:bold;">Customer Returns %</style>
				</div>
				<div class="col1" style="height:20px;text-align:center;">
					<span style="font-weight:bold;">RTO %</style>
				</div>
				<div class="col1" style="height:20px;text-align:center;">
					<span style="font-weight:bold;">Opening recievables updated on</style>
				</div>
			</div>
		{/if}
		{assign var=smarty_variable value=$smarty_variable+1} 
		<div style="width:150px;float:left;margin-bottom:20px;">
			<div style="width:149px;text-align:center;float:left;border-top:1px solid black;border-right:1px solid black;border-bottom:1px solid black;height:20px;font-weight: bold;">
				{$row.display_name}  &nbsp;
			</div>
			<div style="float:left">
					<div class="othercols"> <span style="font-weight:bold;"> No Orders</span> </div>
					<div class="othercols"> <span style="font-weight:bold;"> Value</span> </div>
			</div>
			<div style="float:left">
				<div class="othercols"> <span> </span> </div>
				{if $prevClosing|string_format:"%.2f" eq $row.starting_value|string_format:"%.2f"}
					<div class="othercols"> <span> {$row.starting_value} &nbsp;</span> </div>
				{else}
					<div class="othercols" style="background-color: red;"> <span style="">{$row.starting_value} &nbsp;</span> </div>
				{/if}
			</div>		
			<div style="float:left">
				<div class="othercols"> <span style="">{$row.tot_orders}</span> </div>
				<div class="othercols"> <span style="">{$row.tot_amount} &nbsp;</span> </div>
			</div>
			<div style="float:left">
				<div class="othercols"> <span style="">{$row.rto+$row.rtoq} &nbsp;</span> </div>
				<div class="othercols"> <span style="">{$row.rto_value+$row.rtoq_value} &nbsp;</span> </div>
			</div>
			<div style="float:left">
				<div class="othercols"> <span style="">{$row.no_of_discounts}</span> </div>
				<div class="othercols"> <span style="">{$row.discounts} &nbsp;</span> </div>
			</div>
			<div style="float:left">
				<div class="othercols"> <span style="">{$row.noof_coupon_disc}</span> </div>
				<div class="othercols"> <span style="">{$row.tot_coupon_disc} &nbsp;</span> </div>
			</div>
			<div style="float:left">
				<div class="othercols"> <span style="">{$row.no_of_pg_disc} &nbsp;</span> </div>
				<div class="othercols"> <span style="">{$row.tot_pg_disc} &nbsp;</span> </div>
			</div>
			<div style="float:left">
				<div class="othercols"> <span style="">{$row.no_of_cashbacks} &nbsp;</span> </div>
				<div class="othercols"> <span style="">{$row.cashback} &nbsp;</span> </div>
			</div>
			<div style="float:left">
				<div class="othercols"> <span style="">{$row.collections}</span> </div>
				<div class="othercols"> <span style="">{$row.collected_amount} &nbsp;</span> </div>
			</div>
			<div style="float:left">
				<div class="othercols"> <span style="">&nbsp;</span> </div>
				<div class="othercols"> <span style="">{$row.adjustments} &nbsp;</span> </div>
			</div>
			<div style="float:left">
				<div class="othercols"> <span style="">{$row.closing_no}</span> </div>
				<div class="othercols"> <span style="">{$row.closing_val}</span> </div>
			</div>
			<div style="float:left;background: gray;">
				<div class="othercols" style=""> <span style="">&nbsp;</span> </div>
				<div class="othercols" style=""> <span style=""> &nbsp;</span> </div>
			</div>
			<div style="float:left">
				<div class="othercols"> <span style="">{$row.rtoq} &nbsp;</span> </div>
				<div class="othercols"> <span style="">{$row.rtoq_value} &nbsp;</span> </div>
			</div>
			<div style="float:left">
				<div class="othercols"> <span style="">{$row.rto} &nbsp;</span> </div>
				<div class="othercols"> <span style="">{$row.rto_value} &nbsp;</span> </div>
			</div>
			<div style="float:left">
				<div class="othercols"> <span style=""> {math equation=" (x*100) / y" x=$row.rtoq y=$row.tot_orders format="%.2f"}% &nbsp;</span> </div>
				<div class="othercols"> <span style=""> {math equation=" (x*100) / y" x=$row.rtoq_value y=$row.tot_amount format="%.2f"}% &nbsp;</span> </div>
			</div>
			<div style="float:left">
				<div class="othercols"> <span style=""> {math equation=" (x*100) / y" x=$row.rto y=$row.tot_orders format="%.2f"}% &nbsp;</span> </div>
				<div class="othercols"> <span style=""> {math equation=" (x*100) / y" x=$row.rto_value y=$row.tot_amount format="%.2f"}% &nbsp;</span> </div>
			</div>
			<div style="width:149px;text-align:center;float:left;border-right:1px solid black;border-bottom:1px solid black;height:20px;">
				<span style="">{$row.startupdate_date} &nbsp;</span>
			</div>
			{assign var = prevClosing value = $row.closing_val}
		</div>
	{/foreach}
</div>
{/if}
<script type="text/javascript" src="/skin1/js_script/jquery-1.4.2.min.js" > </script>
<script type="text/javascript" src="/skin1/myntra_js/jquery-ui-1.8.13.custom.min.js" > </script>
<script type="text/javascript" src="/skin1/myntra_js/jquery-ui-datepicker.js" > </script>
{literal}
<script type="text/javascript">
	$(function() {
	    $('.date-picker').datepicker( {
	        changeMonth: true,
	        changeYear: true,
	        showButtonPanel: true,
	        maxDate:new Date(),
	        dateFormat: 'mm-yy',
	        defaultDate: +0,
	        onClose: function(dateText, inst) { 
	            var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
	            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
	            $(this).datepicker('setDate', new Date(year, month, 1));
	        }
	    });
	});
	function submitThisForm(recalculate){
		verifyRange();
		if(recalculate){
			document.getElementById('action').value = 'recalculate';
		}
		document.forms["cod_recievables_report"].submit();
	}	
	function verifyRange(){
		var startDate = document.getElementById('startDate').value;
		var endDate = document.getElementById('endDate').value;
		if(startDate !=null && startDate.trim != ''){
			if(endDate != null && endDate.trim != ''){
				var startParts = startDate.split('-');
				var endParts = endDate.split('-');
				if((parseFloat(endParts[1]) == parseFloat(startParts[1])) && (parseFloat(endParts[0]) > parseFloat(startParts[0]) +6)){
					assignEndDate(startParts);
					return;
				}
				if(parseFloat(endParts[1]) == parseFloat(startParts[1]) + 1 && parseFloat(endParts[0]) + 6 > parseFloat(startParts[0])){
					assignEndDate(startParts);
					return;
				}
				if(parseFloat(endParts[1]) > parseFloat(startParts[1]) +1){
					assignEndDate(startParts);
					return;
				}
			}
		}
	}
	function assignEndDate(startParts){
		alert("End date has been changed to with in 6 months of start date");
		if(startParts[0] > 6){
			document.getElementById('endDate').value = '0'+(6+parseFloat(startParts[0])-12)+'-'+(parseFloat(startParts[1])+1);
		}else{
			if(6+parseFloat(startParts[0]) <10){
				document.getElementById('endDate').value =  '0'+(6+parseFloat(startParts[0]))+'-'+parseFloat(startParts[1]);
			}else{
				document.getElementById('endDate').value =  (6+parseFloat(startParts[0]))+'-'+parseFloat(startParts[1]);
			}	
		}	
	}
</script>
{/literal}
		