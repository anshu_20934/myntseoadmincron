{include file="main/include_js.tpl" src="main/popup_product.js"}
{capture name=dialog}

<form action="new_signup_control.php" method="post">

<table cellpadding="14" cellspacing="15" >

	<tr>
		<td><label>Display Name</label></td>
		<td>
			<select name="display_name">
			{if $display_name}
			<option value="true">YES</option>
			<option value="false">NO</option>
			{else}
			<option value="false">NO</option>
			<option value="true">YES</option>
			{/if}
			</select>
		</td>
	</tr>
   
    <tr>
    	<td><label>Make Name Optional</label></td>
		<td>
			<select name="is_name_optional">
			{if $is_name_optional }
			<option value="true">YES</option>
			<option value="false">NO</option>
			{else}
			<option value="false">NO</option>
			<option value="true">YES</option>			
			{/if}
			</select>
		</td>  
    </tr>
    
    <tr>
    	<td><label>Display Birth Year</label></td>
		<td>
			<select name="display_birth_year">
			{if $display_birth_year}
			<option value="true">YES</option>
			<option value="false">NO</option>
			{else}
			<option value="false">NO</option>
			<option value="true">YES</option>
			{/if}
			</select>
		</td>  
    </tr>
   
    <tr>
    	<td><label>Make Birth Year Optional</label></td>
		<td>
			<select name="is_birth_year_optional">
			{if $is_birth_year_optional}
			<option value="true">YES</option>
			<option value="false">NO</option>
			{else}
			<option value="false">NO</option>
			<option value="true">YES</option>
			{/if}
			</select>
		</td> 
    </tr>

    <tr>
    	<td><label>Birth Year Range Low</label></td>
		<td>
		<input type="text" name="birth_year_range_low" value = "{$birth_year_range_low}" } ></input>
		</td>
    </tr>

    <tr>
    	<td><label>Birth Year Range High</label></td>
    	<td>
    	<input type="text" name="birth_year_range_high" value = "{$birth_year_range_high}" } ></input>
    	</td>
    </tr>

	</table>

<button type="submit" method="post">Submit</button>
</form>

{/capture}
{include file="dialog.tpl" title="New Signup Control" content=$smarty.capture.dialog extra='width="100%"'}