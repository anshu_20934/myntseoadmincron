{* $Id: menu.tpl,v 1.68.2.2 2006/06/20 05:52:05 svowl Exp $ *}
{capture name=menu}
<li><a href="{$catalogs.admin}/users.php" class="VertMenuItems">{$lng.lbl_users}</a></li>
<li><a href="{$catalogs.admin}/cod_order_report.php" class="VertMenuItems">COD Worksheet</a></li>
<li><a href="{$catalogs.admin}/myntCashIgccReports.php" class="VertMenuItems">Cashback IGCC Reports</a></li>
<li><a href="{$catalogs.admin}/myntCashIgccReportsAgent.php" class="VertMenuItems">Agent Cashback IGCC Reports</a></li>
<li><a href="{$catalogs.admin}/cashbackaccountsnew.php" class="VertMenuItems">User Cashback Accounts</a></li>
<li><a href="{$catalogs.admin}/LoyaltyPointsAccounts.php" class="VertMenuItems">Loyalty Points Accounts</a></li>
<li><a href="{$catalogs.admin}/LoyaltyPointsOrders.php" class="VertMenuItems">Loyalty Points Orders</a></li>
<li><a href="{$catalogs.admin}/LoyaltyPointsTier.php" class="VertMenuItems">Loyalty Points Tier Info</a></li>
<li><a href="{$catalogs.admin}/cancelordersinbulk.php" class="VertMenuItems">Cancel Orders in bulk</a></li>
<li><a href="{$catalogs.admin}/uploadBulkCommentsForOrders-CC.php" class="VertMenuItems">Update Order Comments in bulk</a></li>
<li><a href="{$catalogs.admin}/cancellation_history.php" class="VertMenuItems">Cancellation History</a></li>
<li><a href="{$catalogs.admin}/cod_mobile_verification_code.php" class="VertMenuItems">MRP Mobile Verification Code </a></li>
<li><a href="{$catalogs.admin}/ServiceRequest/manageSRComponent.php" class="VertMenuItems">Manage SR Components</a></li>
<li><a href="{$catalogs.admin}/ServiceRequest/serviceRequestReport.php" class="VertMenuItems">Service Request Report</a></li>
<li><a href="{$catalogs.admin}/ServiceRequest/manageServiceRequest.php" class="VertMenuItems">Manage Service Request</a></li>
<li><a href="{$catalogs.admin}/ServiceRequest/bulkSRUpdate.php" class="VertMenuItems">Bulk SR Update</a></li>
<li><a href="{$catalogs.admin}/telesales.php" class="VertMenuItems">Telesales</a></li>
<li><a href="{$catalogs.admin}/telesales-new.php" class="VertMenuItems">New Telesales </a></li>
<li><a href="{$catalogs.admin}/contactte.php" class="VertMenuItems">Messages</a></li>
{/capture}ledsl
{include file="menu.tpl" dingbats="dingbats_categorie.gif" menu_title='Customer Care' menu_content=$smarty.capture.menu }
