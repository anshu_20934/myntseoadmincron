<div class="tele-loading-overlay " style="display: none">
    <img src="{$cdn_base}/skin1/myntra_images/loading-graphic.gif" alt="loading graphic">
</div>

<div id="cartItemsTable" style="border: 2px solid black;float: left;width: 100%;height:60%;overflow: auto;">
	<table >
        <tbody>
            {foreach name=outer item=product key=itemid from=$productsInCart}
            {assign var=numrows value=$numrows+1}
            <tr id="product_view_{$itemid}">
                <td >
                    <div >
                        {assign var='item' value=$product.cartImagePath}
                        <img src="{$product.cartImagePath}" alt="Image Not Available">
                    </div>
                </td>
                <td width="220px">
                	<div>Product Code:{$product.productStyleId}</div>
                    <div><b><a target="_blank" href="{$http_location}/{$product.landingpageurl}">{$product.productStyleName}</a><br>
                    </b>{if $product.pricePerItemAfterDiscount neq ''}<span class="discounted-mrp"><b>Rs {$product.productPrice|string_format:"%.2f"}/-</b></span>
                    <span class="final-price">
                             <b>Rs {$product.pricePerItemAfterDiscount}/-</b>
                     </span>
                     {else}
                     <span class="final-price"><b>Rs {$product.productPrice|string_format:"%.2f"}/-</b></span>
                     
                     {/if}
                       {if $product.discountDisplayText neq ''}
                       <div ><b>{eval var=$product.discountDisplayText}</b></div>
					   {elseif  $cashback_gateway_status eq 'on' && $enableCBOndiscountedProducts eq 'true'}
					   <div ><b>You will receive a cashback upto Rs.{math equation="(w-x) * z" w=$product.productPrice x=$product.discountAmount y=$product.quantity z=0.10 assign="cashback"}{$cashback|round|string_format:"%d"} per item</b></div>
					   {elseif $cashback_gateway_status eq 'on' && $enableCBOndiscountedProducts eq 'false' && $product.discountAmount eq 0}
					   <div ><b>You will receive a cashback upto Rs.{math equation="(w-x) * z" w=$product.productPrice x=$product.discountAmount y=$product.quantity z=0.10 assign="cashback"}{$cashback|round|string_format:"%d"} per item</b></div>
					   {/if}
                      
					</div>
                </td>

                <td style="text-align: center;width: 100px;">
                        <b><label>Size: {if $product.sizename neq $product.sizenameunified and $product.sizenameunified }{$product.sizenameunified} ({$product.sizename}){else}{$product.sizename}{/if}</label></b>
                </td>
                 <td style="text-align: center;width: 100px">
                        <b><label>Quantity: {$product.sizequantity}</label></b>
                        {if $product_availability[$itemid].stock =='0'}
                        <b style="color: red;font-size: 10px">Sold Out</b>
                        {else}
                        	{if $product_availability[$itemid].stock < $product.sizequantity}
                        		<b style="color: orange;font-size: 10px">Only {$product_availability[$itemid].stock} in Stock</b>
                        	{/if}
                        {/if}
                  </td>
				 <td style="text-align: center;width: 100px">
                       {if $orderid}
                       Placed
                       {else}
                        <b><a href="javascript:void(0);" onclick="removeFromCart('{$product.productId}')">Remove</a></b>
                       {/if}
                 </td>
                <td style="text-align: center;width: 100px">
                     <span >
                             <b>Rs {$product.productPrice*$product.quantity-$product.discountAmount|round}</b>
                     </span>
                </td>
            </tr>
            {/foreach}
        </tbody>
    </table>
</div>
<div style="clear:both;width: 100%">
	<table width="100%">
		<tr>
			<td width="175px" style="text-align:center">
				<div id="selectedCartShippingAddress" style="display: none;padding: 10px; text-align: center; width: 175px; border: 2px solid black;"></div>
				{if !$orderid}
				<input type="button" id="addshippingaddresscartbutton" value="Add Shipping address" style="padding-left: 15px ;background-color: orange;color: white;" onclick="showShippingAddress();">
				{/if}
			</td>
			<td width="40%">
				<div style="overflow-x: scroll; width: 255px;border: 1px solid #dddddd;">

                    {if !$orderid}
                        <div style="margin: 20px 0px; border-bottom: 1px solid #dddddd; padding: 5px;">
                            
                            {if $coupons}
                                <div>
                                    <select onchange="{literal}if(!$('#couponcodeinput').is(':disabled')){$('#couponcodeinput').val($(this).val());}{/literal}">
                                        <option value="" selected="selected">Select a Coupon</option>                                        
                                        {section name=prod_num loop=$coupons}
                                            {if $coupons[prod_num].coupon neq ""}
                                                {if $coupons[prod_num].couponType eq 'percentage'}
                                                    <option value="{$coupons[prod_num].coupon}">{$coupons[prod_num].coupon} : {$coupons[prod_num].couponType} : {$coupons[prod_num].MRPpercentage} : {$coupons[prod_num].expire|date_format:"%e-%m-%Y"}</option>
                                                {/if}
                                                {if $coupons[prod_num].couponType eq 'absolute'}
                                                    <option value="{$coupons[prod_num].coupon}">{$coupons[prod_num].coupon} : {$coupons[prod_num].couponType} : {$coupons[prod_num].MRPAmount} : {$coupons[prod_num].expire|date_format:"%e-%m-%Y"}</option>
                                                {/if}
                                                {if $coupons[prod_num].couponType eq 'dual'}
                                                    <option value="{$coupons[prod_num].coupon}">{$coupons[prod_num].coupon} : {$coupons[prod_num].couponType} : {$coupons[prod_num].MRPpercentage}% Off Upto {$coupons[prod_num].MRPAmount} : {$coupons[prod_num].expire|date_format:"%e-%m-%Y"}</option>
                                                {/if}
                                            {/if}
                                        {/section}
                                    </select>
                                </div>
                                <div style="margin:2px 0px;">OR</div>
                            {/if}


                            {if $couponCode}
                                <div style="float:left"><input type="text" value="{$couponCode}" id="couponcodeinput" disabled="disabled" ></div>
                                <div style="float:right;"><input type="button" value="Remove" style="margin-left: 15px;background-color: orange;color: white;"  onclick="updateCart('removecoupon','',true);"></div>
                            {else}
                                <div style="float:left"><input type="text" value="{if $couponCode}{$couponCode}{else}Enter Coupon Manually{/if}" id="couponcodeinput" onkeydown="{literal}if (event.keyCode == '13'){if($('#couponcodeinput').val()=='Enter Coupon Manually' || $.trim($('#couponcodeinput').val())==''){alert('Add a valid coupon code!');return false;}updateCart('addcoupon','couponcode='+$('#couponcodeinput').val(),true);;return false;	}{/literal}" style="padding-left: 5px;" onfocus="{literal}if(this.value=='Enter Coupon Manually'){this.value='';}{/literal}" onblur="{literal}if(this.value==''){this.value='Enter Coupon Manually';}{/literal}" {if $orderid}disabled="disabled"{/if} ></div>
                                <div style="float:right;"><input type="button" value="Redeem" style="margin-left: 15px;background-color: orange;color: white;"  onclick="{literal}if($('#couponcodeinput').val()=='Enter Coupon Manually' || $.trim($('#couponcodeinput').val())==''){alert('Add a valid coupon code!');return false;}{/literal}updateCart('addcoupon','couponcode='+$('#couponcodeinput').val(),true);"></div>
                            {/if}
                            <div style="clear:both;"></div>
                            {if $couponMessage}<div style="margin-top:2px;">{$couponMessage}</div>{/if}

                        </div>

                        {if $myntCashUsageMessage}
                            <div style="margin: 0px 0px 20px; padding: 5px; border-bottom: 1px solid #dddddd;height:55px;">
                                {if $isMyntCashUsed}
                                    <div style="float:left;">
                                        {$myntCashUsageMessage}
                                    </div>
                                    <div style="float:right;">
                                        <input type="button" value="Remove" style="margin-left: 15px;background-color: orange;color: white;"  onclick="updateCart('removeMyntCash','',true);"></div>
                                    </div>
                                {else}
                                    {if $myntCashUsageMessage}
                                        <div style="float:left;">
                                           {$myntCashUsageMessage} 
                                        </div>
                                        <div style="float:right;">
                                            <input type="button" value="Redeem" style="margin-left: 15px;background-color: orange;color: white;"  onclick="updateCart('uesMyntCash','userAmount={$myntCashDetails.balance}',true);">
                                        </div>
                                    {/if}
                                {/if}
                                <div style="clear:both;"></div>
                            </div>
                        {/if}
                    {/if}

				{if $orderid}
					<div>
					{if $order_message eq 'placed'}
						{if $payment_method eq 'cod'}
						<div style="font-size: 20px;color: red">Order Placed via COD</div>
						<div style="font-size: 20px;color: red">Order Id : {$orderid}</div>
						{else}
						<div style="font-size: 20px;color: red">Order Placed</div>
						<div style="font-size: 20px;color: red">Order Id : {$orderid}</div>
						{/if}
					{else}
						<div style="font-size: 20px;color: red">Order Placed</div>
						<div style="font-size: 20px;color: red">Order Id : {$orderid}</div>
					{/if}
					</div>
				{/if}
			</td>
			<td>
				
            	<div>
					<table width="100%">
						<tr>
							<td><span>Item Total</span></td>
							<td><span>Rs</span></td> <td style="text-align: right"><span>{$amount} </span></td>
						</tr>
						<tr>
							<td><span>Special Discount</span></td>
							<td><span>Rs</span></td> <td style="text-align: right"><span>{if $eossSavings}{$eossSavings}{else}0{/if} </span></td>
						</tr>
						<tr>
							<td><span>Coupon Discount</span></td>
							<td><span>Rs</span></td> <td style="text-align: right"><span>{if $couponDiscount}{$couponDiscount}{else}0{/if} </span></td>
						</tr>
						<tr>
							<td><span>Cash Discount</span></td>
							<td><span>Rs</span></td> <td style="text-align: right"><span>{if $isMyntCashUsed}{$myntCashUsage}{else}0.00{/if} </span></td>
						</tr>
						<tr>
							<td><span>Shipping Charges</span></td>
							<td><span>Rs</span></td> <td style="text-align: right"><span>{if $shippingCharges}{$shippingCharges}{else}0{/if}</span></td>
						</tr>
						<tr><td colspan="3"><hr /></td></tr>
						<tr style="font-weight: bold;">
							<td><span>Total</span></td>
							<td><span>Rs</span></td> <td style="text-align: right"><span>{$totalAmount}</span></td>							
						</tr>
					</table>
				</div>

				{if $orderid}
                    {if $payment_method neq 'cod' and $can_be_converted_COD eq 1}
                        <div style="text-align: right;"><input type="button" id="converttocodbutton" value="Convert to COD" style="padding-left: 15px ; margin-left: 15px;background-color: orange;color: white;" onclick="$('#converttocodbutton').hide();convertToCod('{$orderid}','telesalesconverttocod');"></div>
                    {/if}
				{else}

                    {* commenting ivr option for placing telesales order
                        <div style="text-align: right;"><input type="button" value="Create Order" style="padding-left: 15px ; margin-left: 15px;{if $readyForCheckout}background-color: orange;color: white;{/if}" onclick="placeOrder('ivr'); " {if !$readyForCheckout}disabled="disabled"{/if}></div>
                    *}
                    {if $can_be_converted_COD eq 1}
                        <!--amount if 0, place order using payment method as online(creditcards) to avoid COD verification for min ad max COD order value-->
                            <div style="text-align: right;"><input type="button" value="Create COD Order" style="padding-left: 15px ; margin-left: 15px;{if $readyForCheckout}background-color: orange;color: white;{/if}" onclick="placeOrder('cod'); " {if !$readyForCheckout}disabled="disabled"{/if}></div>
                    {else}
                        <div style="text-align: center; display: block; font-weight: bold; height: 20px; background-color: red; color: #ffffff;">Can not place a COD order</div>
                    {/if}


				{/if}
			</td>
		</tr>
	</table>
</div>
