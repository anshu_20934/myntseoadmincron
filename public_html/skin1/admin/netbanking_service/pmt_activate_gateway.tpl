{include file="main/include_js.tpl" src="main/popup_product.js"}
<div style="padding-left: 320px;padding-bottom: 20px;font-size: 14px;">
	<a style="padding-left: 10px;" href="{$http_location}/admin/netbanking_service/pmt_add_gateway.php"> ADD/EDIT GATEWAY </a>
	<a style="padding-left: 10px;" href="{$http_location}/admin/netbanking_service/pmt_bank_list.php"> ADD/EDIT BANKS </a>
</div>
{literal}
<script type="text/javascript">
function delete_gateway(mappingId){
	document.addgateway.mode.value = 'delete';
	document.addgateway.mappingOrBankId.value = mappingId;
	document.addgateway.submit();
}

function add_gateway(mappingId){
    document.addgateway.mode.value = 'add';
	document.addgateway.mappingOrBankId.value = mappingId;
    document.addgateway.submit();
}

function activateAll(bankId) {
	document.addgateway.mode.value = 'activate';
	document.addgateway.mappingOrBankId.value = bankId;
    document.addgateway.submit();
}

function addnew_gateway(id, name) {
	document.addgateway.mode.value = 'add_gateway';
	document.addgateway.mappingOrBankId.value = id;
	document.addgateway.bankName.value = name;
    document.addgateway.submit();
}

</script>
{/literal}

{capture name=dialog}
<form action="{$action_php}" method="post" name="addgateway">
<input type="hidden" name="mode" />
<input type="hidden" name="mappingOrBankId" value=""/>
<input type="hidden" name="bankName" value=""/>
<table cellpadding="3" cellspacing="5" width="100%">
<tr class="TableHead">
	<td width="40%">Bank Name</td>
	<td width="40%">Gateways</td>
	<td width="20%">Action</td>
</tr>

{if $bank_gateway_mapping}
{foreach from=$bank_gateway_mapping item=entry key=name} 
<tr style="margin-top:5px;">
	<td align="center" style="left-padding:30px">{$name}</td>
	<td align="center" style="left-padding:30px">
	{section name=key_num loop=$entry}
		<span style="background: #FD6;padding: 5px;margin:5px;border: 1px solid;-moz-border-radius: 5px !important;-webkit-border-radius: 5px !important;-khtml-border-radius: 5px !important;border-radius: 5px;">
		{if $entry[key_num].is_active}
			<input type='hidden' name="update_name_field[{$entry[key_num].id}]">
				{$entry[key_num].gateway_name}
			</input>
			<a href="javascript:void(0);" onclick="delete_gateway({$entry[key_num].id})"><img src="{$cdn_base}/skin1/images/clear.gif" /></a>
		{else}
			<input type='hidden' name="update_name_field[{$entry[key_num].id}]">
				{$entry[key_num].gateway_name}
			</input>
			<a href="javascript:void(0);" onclick="add_gateway({$entry[key_num].id})"><img src="{$cdn_base}/skin1/images/plus.gif" /></a>
		{/if}
		</span>
	{/section}
	</td>
	<td align="center">
		<input type="button" value="Activate All" onclick="javascript: activateAll('{$entry[0].bank_id}');" />
		<input type="button" value="Assign a new gateway for this bank" onclick="javascript: addnew_gateway('{$entry[0].bank_id}','{$name}');" />		
	</td>
	
</tr>
{/foreach} 
{else}
<tr>
	<td align="center" colspan="2">No Bank Gateways mapping exists</td>
</tr>
{/if}
<tr>
	<td></td>
	<td></td>
	<td class="SubmitBox" align="right">
	   <input type="button" value="Reset All to defaults" onclick="javascript: document.addgateway.mode.value = 'reset'; document.addgateway.mappingOrBankId.value = '1';document.addgateway.submit();" />
	</td>
</tr>
<tr>
	<td></td>
	<td></td>
	<td class="SubmitBox" align="right">
	   <input type="button" value="Clear XCache" onclick="javascript: document.addgateway.mode.value = 'clearCache'; document.addgateway.mappingOrBankId.value = '1';document.addgateway.submit();" />
	</td>
</tr>

</table>
</form>

{/capture}
{include file="dialog.tpl" title="Add New Netbanking Payment Gateways" content=$smarty.capture.dialog extra='width="100%"'}