{include file="main/include_js.tpl" src="main/popup_product.js"}
<div style="padding-left: 320px;padding-bottom: 20px;font-size: 14px;">
	<a style="padding-left: 10px;" href="{$http_location}/admin/netbanking_service/pmt_activate_gateway.php"> ADD/EDIT GATEWAY MAPPING</a>
	<a style="padding-left: 10px;" href="{$http_location}/admin/netbanking_service/pmt_bank_list.php"> ADD/EDIT BANKS </a>
</div>
{literal}
<script type="text/javascript">
function delete_gateway(id){
    var r=confirm("Are you sure you want to delete this Gateway");
    if(r == true){
        document.addgateway.mode.value = 'delete';
        document.addgateway.gateway_id.value = id;
        document.addgateway.submit();
    }
}

function add_Gateway(){
    if (jQuery.trim(document.addgateway.gateway_name_new.value) == '' || jQuery.trim(document.addgateway.gateway_weight_new.value == '')) {
        alert('Gateway Name or weight cannot be empty');
        return;
    } 
    document.addgateway.mode.value = 'add';
    document.addgateway.submit();
}

function edit_gateway_bank(id, name){
	window.location = "pmt_add_gateway_bank.php?id="+id+"&name="+name;
}

</script>
{/literal}

{capture name=dialog}
<form action="{$action_php}" method="post" name="addgateway">
<input type="hidden" name="mode" />
<input type="hidden" name="gateway_id" value=""/>
<table cellpadding="3" cellspacing="1" width="100%">
<tr class="TableHead">
	<td width="40%">Gateway Name</td>
	<td width="40%">Weight</td>
	<td width="20%">Action</td>
</tr>

{if $gateway_list}
{section name=key_num loop=$gateway_list}
<tr>
	<td align="center" style="left-padding:30px"><input name="update_name_field[{$gateway_list[key_num].id}]" type="text" value="{$gateway_list[key_num].name}" /></td>
	<td align="center" style="left-padding:30px"><input name="update_weight_field[{$gateway_list[key_num].id}]" type="text" value="{$gateway_list[key_num].weight}" /></td>
	<td align="center"><input type="button" value="Add/Edit Banks" onclick="javascript: edit_gateway_bank('{$gateway_list[key_num].id}', '{$gateway_list[key_num].name}')" /><input type="button" value="Delete" onclick="javascript: delete_gateway('{$gateway_list[key_num].id}')" /></td>	
</tr>
{/section}
{else}
<tr>
	<td align="center" colspan="2">No gateways addded</td>
</tr>
{/if}

<tr>
	<td align="center" style="left-padding:30px"><input name="gateway_name_new" type="text" value="" /></td>
	<td align="center" style="left-padding:30px"><input name="gateway_weight_new" type="text" value="" /></td>
	<td align="center">
		<input type="button" value="Add" onclick="javascript: add_Gateway()" />
	</td>
</tr>
<tr>
	<td></td>
	<td></td>
	<td></td>
	<td class="SubmitBox" align="right">
	   <input type="button" value="Update" onclick="javascript: document.addgateway.mode.value = 'update'; document.addgateway.submit();" />
	</td>
</tr>
<tr>
	<td></td>
	<td></td>
	<td></td>
	<td class="SubmitBox" align="right">
	   <input type="button" value="Clear XCache" onclick="javascript: document.addgateway.mode.value = 'clearCache'; document.addgateway.submit();" />
	</td>
</tr>
</table>
</form>

{/capture}
{include file="dialog.tpl" title="Add New Netbanking Services Payment Gateways" content=$smarty.capture.dialog extra='width="100%"'}