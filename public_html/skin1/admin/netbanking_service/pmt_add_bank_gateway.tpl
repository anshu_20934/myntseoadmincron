{include file="main/include_js.tpl" src="main/popup_product.js"}
<div style="padding-left: 320px;padding-bottom: 20px;font-size: 14px;">
	<a style="padding-left: 10px;" href="{$http_location}/admin/netbanking_service/pmt_activate_gateway.php"> ADD/EDIT GATEWAY MAPPING</a>
	<a style="padding-left: 10px;" href="{$http_location}/admin/netbanking_service/pmt_add_gateway.php"> ADD/EDIT GATEWAYS </a>
	<a style="padding-left: 10px;" href="{$http_location}/admin/netbanking_service/pmt_bank_list.php"> ADD/EDIT BANKS </a>
</div>
{literal}
<script type="text/javascript">
function delete_bank_gateway(id){
    var r=confirm("Are you sure you want to delete this Gateway mapping for {/literal}{$bank_name}{literal}");
    if(r == true){
        document.updatebankgateway.mode.value = 'delete';
        document.updatebankgateway.mapping_id.value = id;
        document.updatebankgateway.submit();
    }
}

function add_BankGateway(){
    if (jQuery.trim(document.updatebankgateway.gateway_value_new.value) == '' || jQuery.trim(document.updatebankgateway.gateway_bank_code_new.value) == '') {
        alert('Gateway selected or gateway code cannot be empty');
        return;
    } 
    document.updatebankgateway.mode.value = 'add';
    document.updatebankgateway.submit();
}

</script>
{/literal}

{capture name=dialog}
<form action="{$action_php}" method="post" name="updatebankgateway">
<input type="hidden" name="mode" />
<input type="hidden" name="bank_id" value="{$bank_id}"/>
<input type="hidden" name="bank_name" value="{$bank_name}"/>
<input type="hidden" name="mapping_id" value=""/>
<table cellpadding="3" cellspacing="1" width="100%">
<tr class="TableHead">
	<td width="40%">Gateway Name</td>
	<td width="40%">Bank Code</td>
	<td width="20%">Action</td>
</tr>

{if $bank_gateway_list}
{section name=key_num loop=$bank_gateway_list}
<tr>
	<td align="center" style="left-padding:30px">{$bank_gateway_list[key_num].gateway_name}</td>
	<td align="center" style="left-padding:30px"><input name="update_name_field[{$bank_gateway_list[key_num].id}]" type="text" value="{$bank_gateway_list[key_num].gateway_code}" /></td>
	<td align="center"><input type="button" value="Delete" onclick="javascript: delete_bank_gateway('{$bank_gateway_list[key_num].id}')" /></td>	
</tr>
{/section}
{/if}

{if $bank_unused_gateways}
<tr>
	<td align="center" style="left-padding:30px">
	<select name="gateway_value_new">
	{section name=key_num loop=$bank_unused_gateways}
		<option value="{$bank_unused_gateways[key_num].gateway_id}">{$bank_unused_gateways[key_num].gateway_name}</option>
	{/section}
	</select>
	</td>
	<td align="center" style="left-padding:30px"><input name="gateway_bank_code_new" type="text" value="" /></td>
	<td align="center">
		<input type="button" value="Add" onclick="javascript: add_BankGateway()" />
	</td>
</tr>
{/if}
<tr>
	<td></td>
	<td></td>
	<td></td>
	<td class="SubmitBox" align="right">
	   <input type="button" value="Update" onclick="javascript: document.updatebankgateway.mode.value = 'update'; document.updatebankgateway.submit();" />
	</td>
</tr>
<tr>
	<td></td>
	<td></td>
	<td></td>
	<td class="SubmitBox" align="right">
	   <input type="button" value="Clear XCache" onclick="javascript: document.updatebankgateway.mode.value = 'clearCache';document.updatebankgateway.submit();" />
	</td>
</tr>
</table>
</form>

{/capture}
{include file="dialog.tpl" title="Service: Add Gateways for $bank_name" content=$smarty.capture.dialog extra='width="100%"'}