<html>
<head>
<title>{$lng.txt_site_title}</title>
{ include file="meta.tpl" }
<link rel="stylesheet" href="{$SkinDir}/skin1_admin.css" />
<script type="text/javascript">
    var http_loc = '{$http_location}'
    var cdn_base = '{$cdn_base}'
 </script>
</head>
<body>
<table cellpadding="3" cellspacing="1" width="100%">
	<tr align="center">
		<td colspan="8"><b><font
			size="2" color="blue" >Sku Logs for {$skucode} till {$time} </font></b></td>
	</tr>
	<tr class="TableHead">
		<td >Date</td> 
		<td >Changed By</td>
		<td >Value</td>
		<td >Reason</td>
		<td>Previous Inventory Count</td>
		<td >Previous Warehouse Count</td>
		<td >Order ID</td>
		<td >Extra Comments</td>
	</tr>
	{if $total_pages eq 0}
		<tr align="center">
		<td colspan="8"><b>No Logs Found</b></td>
	</tr>
	{else}
	{foreach from=$sku_logs item=log}
	<tr bgcolor="">
		<td align="center" >{$log.logdate}</td>
		<td align="center" >{$log.changeby}</td>
		<td align="center" >{$log.value}</td>
		<td align="center" >{$log.rtext}</td>
		<td align="center" >{$log.pinvcount}</td>
		<td align="center" >{$log.pwhcount}</td>
		<td align="center" >{$log.orderid}</td>
		<td align="center" >{$log.extrainfo}</td>
	</tr>
	{/foreach}
	{/if}
	<tr class="TableHead">
		<td colspan="8">
		{if $page gt 1}
		 <a href="./sku_edit_list.php?page={$page-1}&mode=viewlog&code={$skucode}">Previous</a>&nbsp;&nbsp;&nbsp;&nbsp; 
		{/if}
		{section name=times start=$start_page loop=$end_page+1 step=1}
			{if $page neq $smarty.section.times.index}
			<a href="./sku_edit_list.php?page={$smarty.section.times.index}&mode=viewlog&code={$skucode}">{$smarty.section.times.index}</a>&nbsp;&nbsp; 
			{else} 
				<font
				size="4" color="green">{$smarty.section.times.index}</font>&nbsp;&nbsp;
			{/if}
		{/section}
		{if $page lt $total_pages}
		 <a href="./sku_edit_list.php?page={$page+1}&mode=viewlog&code={$skucode}">Next</a>&nbsp;&nbsp;&nbsp;&nbsp; 
		{/if}
		</td>
		
	</tr>
</table>
</body>

