{* $Id: featured_products.tpl,v 1.24.2.1 2006/06/16 10:47:40 max Exp $ *}
{capture name=dialog}
<div class="notification-message">{$message}</div>
<form action="vtr_enable_disable_styles.php" method="post" class="unif">
	<input type="hidden" name="mode" value="vtr-disable"/>
	Style Ids(Comma Separated) : <input type="text" name="styleId" class="long-input"/>
	<input type="submit" value="Disable Styles" class="long-input">
</form>
<form action="vtr_enable_disable_styles.php" method="post" class="unif">
	<input type="hidden" name="mode" value="vtr-enable"/>
	Style Ids(Comma Separated) : <input type="text" name="styleId" class="long-input"/>
	<input type="submit" value="Enable Styles"  class="long-input">
</form>
{/capture}
{include file="dialog.tpl" title="Enable/Disable Styles in VTR" content=$smarty.capture.dialog extra='width="100%"'}
