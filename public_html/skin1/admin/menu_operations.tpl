{* $Id: menu.tpl,v 1.68.2.2 2006/06/20 05:52:05 svowl Exp $ *}
{capture name=menu}
<li><a href="{$catalogs.admin}/OperationSLAReport.php" class="VertMenuItems">Operations SLA Dashboard</a></li>
<li><a href="{$catalogs.admin}/orders.php" class="VertMenuItems">Order Search</a></li>
<BR><BR>
<li><a href="{$catalogs.admin}/operations/item_assignment.php" class="VertMenuItems">Item Assignment</a></li>
<li><a href="{$catalogs.admin}/upload_qa_report.php" class="VertMenuItems">QA Worksheet</a></li>
<li><a href="{$catalogs.admin}/cancel-lost-orders.php" class="VertMenuItems">Lost Orders</a></li>
<!--
    <li><a href="{$catalogs.admin}/operations/downloader_dashboard.php" class="VertMenuItems">Downloader Dashboard</a></li>
    <li><a href="{$catalogs.admin}/assignee_worksheet.php" class="VertMenuItems">Assignee Worksheet</a></li>
    <li><a href="{$catalogs.admin}/location_worksheet.php" class="VertMenuItems">Location Worksheet</a></li>
    <li><a href="{$catalogs.admin}/assigned_items_report.php" class="VertMenuItems">Assigned item report</a></li>
    <li><a href="{$catalogs.admin}/all_queued_orders.php" class="VertMenuItems">All queued orders</a></li>
    <BR>
    <li><a href="{$catalogs.admin}/operations/sku_picklist_report.php" class="VertMenuItems">SKU Picklist report</a></li>
    <BR>
    <li><a href="{$catalogs.admin}/operations/processitemexit.php" class="VertMenuItems">Material Exit Movement</a></li>
    <BR>
    <li><a href="{$catalogs.admin}/oosordersreport.php" class="VertMenuItems">OOS Orders Report</a></li>     
-->
<BR>
<BR>
<li><a href="{$catalogs.admin}/courier_serviceability.php" class="VertMenuItems">Courier Serviceability</a></li>
<li><a href="{$catalogs.admin}/courier_status_info.php" class="VertMenuItems">Courier Status Info</a></li>
<li><a href="{$catalogs.admin}/orders_ship_report.php?mode=set_courier_pref" class="VertMenuItems">Courier Preference setup</a></li>
<li><a href="{$catalogs.admin}/ready_to_ship_report.php?mode=gen_rts_report" class="VertMenuItems">Ready To Ship Report</a></li>
<li><a href="{$catalogs.admin}/updateordersinbulk.php" class="VertMenuItems">Bulk Uploader(Ship Orders)</a></li>
<BR>
<BR>
<li><a href="{$catalogs.admin}/shipped_orders.php" class="VertMenuItems">Shipped Orders Report</a></li>
<li><a href="{$catalogs.admin}/update_delivered_orders.php" class="VertMenuItems">Mark Shipped Orders as Delivered</a></li>
<BR>
<BR>
<li><a href="{$catalogs.admin}/upload_payment_completed_orders.php" class="VertMenuItems">Upload paid orders</a></li>
<li><a href="{$catalogs.admin}/upload_payment_completed_giftorders.php" class="VertMenuItems">Upload paid Gift Orders</a></li>
<BR>
<li><a href="{$catalogs.admin}/operations/addordersbulkcomment.php" class="VertMenuItems">Add Orders Comment in bulk</a></li>
<BR>
<li><a href="{$catalogs.admin}/operations/updatecoreitemsbulk.php" class="VertMenuItems">Warehouse Items Bulk Changes</a></li>
<BR>
<BR>
<li><a href="{$catalogs.admin}/manage_resources.php" class="VertMenuItems">Manage Resources</a></li>
<li><a href="{$catalogs.admin}/item_assignee.php" class="VertMenuItems">Add Assignee</a></li>
{/capture}
{include file="menu.tpl" dingbats="dingbats_categorie.gif" menu_title=Operations menu_content=$smarty.capture.menu }