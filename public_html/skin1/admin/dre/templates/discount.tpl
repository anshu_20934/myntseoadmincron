{literal}
<script type="text/template" id="createrule-stylefilter-template">

<div>
    <div class="DreDialogTitleWrapper">
	<div class="DreDialogTitle">Create New <%= type == 'dynamic'  ?' Dynamic ':'Static' %>Rule</div></div>
	<div class="DreDialogBorder">
		<div style="margin-left:10px;margin-right:10px;">
			<br/>
			<div class="fl wp28" >
				<a href="#">Home</a>
				<br/>
				<a href="#discount/type/<%= type=='dynamic'  ?'static':'dynamic' %>">Create new <%= type=='dynamic'  ?' Static ':' Dynamic ' %> rule </a>
			</div>
			<div class="cl"> &nbsp; </div>
		</div>
		<div class="loading-msg" style="color:blue;display:none;">Loading ....</div>
        <div class="fl wp15" > Brand :
            <select name="brand_filter" class="brand_filter">
                <%= discount.get('isDynamic') ? '':'<option value="all">All</option>' %>
                <%= brand_filter_options %>
            </select>
        </div>
        <div class="fl wp15" > Article Type :
            <select name="article_type_filter" class="article_type_filter">
                <option value="all">All</option>
                <%= article_type_filter_options %>
            </select>
        </div>
        <div class="fl wp15" > Fashion :
            <select name="fashion_filter" class="fashion_filter">
                <option value="all">All</option>
                <%= fashion_type_filter_options %>
            </select>
        </div>
        <div class="fl wp15" > Season :
            <select name="season_filter" class="season_filter">
                <option value="all">All</option>
                <%= season_filter_options %>
            </select>
        </div>
        <div class="fl wp15" > Year :
            <select name="year_filter" class="year_filter">
                <option value="all">All</option>
                <%= year_filter_options %>
            </select>
        </div>
        <div class="cl"> &nbsp; </div>

        <% if(type !='dynamic' ) { %>
        <div>
            Check for these styles (Comma Seperated Values) <textarea cols="52" rows="2" name="csv-file-list" id="csv-file-list"></textarea>
        </div>
        <% } %>
        <br/>
        <div ><input type="button" class="filter_style" name="filter_style" value="<% if(discount.isNew()) { %> Filter <% } else { %> Fetch Style list <% } %>  "></div>

        <div class="stylelist" ></div>
    </div>
</div>
</script>

<script type="text/template" id="discount-template">
    <div class="DreDialogTitleWrapper">
<div class="DreDialogTitle"><% if(formatted.isNew) { %> Create <% } else { %> Edit <% } %>Rule</div></div>
<div class="DreDialogBorder">
    <div style="margin-left:10px;margin-right:10px;">
            <div><a href="#" >Go To Rules List</a></div>
            <div id="discount-content"></div>
    </div>
</div>
</script>

<script type="text/template" id="discount-content-template">

<div id="discount" style="padding:10px;" >
    <div class="fr" >Rule is <span style="font-weight:bold;font-size:13px;"
            <% if(isEnabled) { %> class="colg">Enabled <% } else { %>  class="colr">Disabled <% } %>
        </span></div>
    
    <form name="discount" >
        
        <div>
            <div class="fl"  style="width:50%">
                <div >
        <% if( isDynamic ) {  %>
            Product Set Name : <span style="color:blue"><input type="text" size="50"  name="name" value="<%= name %>" readonly="readonly" /></span>
        <% } else { %>
            Product Set Name : <input type="text" size="50"  name="name" value="<%= name %>" />
        <% } %>
        </div>
        <br/>
                Description&nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; : <input type="text" size="30"  name="description" value="<%= description %>" />
            </div>
            <div class="fl"  style="width:50%">
                <div >
                    Start Date : <input id="discountrulestartdate" type="text" value="<%= formatted.startedOn %>" name="discountrulestartdate">
                </div>
                <br/>
                <div >
                    End date &nbsp;: <input id="discountruleenddate" type="text" value="<%= formatted.expiredOn %>" name="discountruleenddate">
                </div>
            </div>
            <div class="cl" > &nbsp; </div>
        </div>
        
        
        <br/>
        <hr>
		<br/>

	<div id="funding_info">
	
		<div class="fl" style="width:33%">
			<div> Discount Funding &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;  &nbsp; &nbsp;:
				<select name="discount_funding" class="discount_funding">
					<option value="select">Select Value</option>
					<option value="category" <% if(discountFunding == "category") {%> selected <%}%> >Category</option>
					<option value="marketing" <% if(discountFunding == "marketing") {%> selected <%}%> >Marketing</option>
					<option value="vendor-Inseason" <% if(discountFunding == "vendor-Inseason") {%> selected <%}%> >Vendor-Inseason</option>
					<option value="vendor-EOSS" <% if(discountFunding == "vendor-EOSS") {%> selected <%}%> >Vendor-EOSS</option>
					<option value="others" <% if(discountFunding == "others") {%> selected <%}%> >Others</option>
				</select>
			</div>
		
			<br/>
	
			<div>
				Vendor Funding (Percentage) : <span style="color:blue"><input type="text" size="15" name="percentage_funding" value="<%= fundingPercentage %>"></span>
			</div>
			
			<br/>		
			<br/>
			
		</div>
	
		<div class="fl" style="width:33%">	
			<div> Basis of Vendor Funding &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp;:
				<select name="basis_funding" class="basis_funding" value="<%= fundingBasis %>">
					<option value="select" >Select Value</option>
					<option value="mrp" <% if(fundingBasis == "mrp") {%> selected <%}%> >MRP</option>
					<option value="discount" <% if(fundingBasis == "discount") {%> selected <%}%> >Discount</option>
				</select>
			</div>
			
			</br>
			
			<div>
				Funding Discount Limit (Percentage) : <span style="color:blue"><input type="text" size="15"  name="discount_limit" value="<%= discountLimit %>"/></span>
			</div>
			
			<br/>
			<br/>
				
		</div>
		
		<div class="fl" style="width:33%">	
			<div> Tax: 
				<input type="radio" name="funding-tax" value="1" <%= fundingTax == 1 ? 'checked' : '' %>> Yes
				<input type="radio" name="funding-tax" value="0" <%= fundingTax == 0 ? 'checked' : '' %>> No
			</div>
			
			<br/>
			<br/>
			<br/>
			<br/>
							
		</div>        
		        
	</div>        
	
<br/><br/><br/><br/><br/><br/>

        <div id="discount-rule-list" style="border-bottom:1px solid black;border-top:1px solid black;">
        </div>
        <% if(formatted.onCart){%>
        <br/>
        <br/>
        <h3>Exclude the following brands:</h3>
        <div>
            <select name="brand_filter" class="brand_filter" multiple size=10>
                <%= formatted.brand_filter_options %>
            </select>
        </div>

        <div class="stylelist" ></div>
        <% } %>

        <br/><br/>
        <div id="discount-style-list">
        </div>
        <br/>
        <div style="text-align:center"><input type="button" style="width:100px;" name="saveDiscount" id="saveDiscount" value="Save" /></div>
    </form>
</div>
</script>





<!---Styles--->
<script type="text/template" id="discount-style-list-template">
<div >
	<div class="fl">
		<span style="font-size:13px;" class="backbone-link"onclick="$('#discount-style-list-container').toggle();">Show/Hide Style List</span>
	</div>
	<div class="fr" style="text-align:center">
		Enter Comma Seperated StyleIds to Filter styles <br/>
        <textarea id="csv-file-list" cols="54"></textarea><br/>
		<input type="button" value="Filter" class="csv-filter-button"/>
        <br/>
        <span class="backbone-link style-csv-pop">[ Click here to Populate texbox with styleid associated with this discount ]</span>
	</div>
	<div class="cl"> &nbsp; </div>
	<div id="discount-style-list-container">
		<div id="discount-style-list-discount-attached-container"></div>
		<div id="discount-style-list-not-discount-container"></div>
		<div id="discount-style-list-discount-container"></div>
	</div>
</div>
</script>

<script type="text/template" id="discount-style-list-discount-template">
<div >
    <h3><%= formatted.title %></h3>
    <div class="dre-table">
	<div class="dre-table-head">
		<div class="fl" style="width:60px">StyleID</div>
	    <div class="fl wp28">Style Name</div>
		<div class="fl" style="width:60px">Season</div>
		<div class="fl" style="width:60px">Year</div>
		<div class="fl" style="width:60px">Fashion</div>
        <div class="fl" style="width:60px">Price</div>
		<div class="fl wp12"><% if(formatted.showDiscountName){ %>Discount Name<% } %>&nbsp;</div>
	    <div class="fl wp12"><input type="button" class="<%= formatted.checkedAll ? '' : 'allDiscountStylesIdchecked'  %>" name="allDiscountStylesId" value="Check/ Uncheck All" /></div>
		<div class="cl"> &nbsp; </div>
	</div>
	<div class="discount-style-item-discount-attached-container"></div>
    </div>
</div>
</script>

<script type="text/template" id="discount-rule-style-item-template">
<div>
    <div class="fl" style="width:60px"><%= style_id %></div>
    <div class="fl wp28"><%= product_display_name %> &nbsp; </div>
    <div class="fl" style="width:60px"><%= global_attr_season %> &nbsp; </div>
    <div class="fl" style="width:60px"><%= global_attr_year %> &nbsp; </div>
    <div class="fl" style="width:60px"><%= global_attr_fashion_type %></div>
    <div class="fl" style="width:60px" ><%= price %></div>
    <div class="fl wp12" ><% if(_discountAttachedToName != ''){ %>
	<%= _discountAttachedToName %>
    <% } %>&nbsp;</div>
    <div class="fl wp12"> <input type="checkbox" <%= _isSelectedForDiscount ? 'checked="yes"' : '' %>" name="discountStylesId"  /></div>
    <div class="cl"> &nbsp; </div>
</div>
</script>
{/literal}
