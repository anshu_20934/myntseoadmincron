{literal}
<script type="text/template" id="discount-rule-list-template">
<div >
	<h3>Rules on this discount</h3>
	
    <div id="discount-rule-group-container"></div>
</div>
</script>

<script type="text/template" id="discount-rule-group-template">
<div>
<div id="discount-rule-item-container"></div>
</div>
</script>

<script type="text/template" id="discount-rule-item-template">
<div>
    <br/>
    <input type="radio" name="add-new-rule-value" class="add-new-rule-value" value="1" <%= type == 1 || type == 8 ? 'checked' : '' %>/>Flat Discount
    <br/>
    <input type="radio" name="add-new-rule-value" class="add-new-rule-value" value="2" <%= type == 2 ? 'checked' : '' %>/>BuyXGetY
    <br/>
<!--    <input type="radio" name="add-new-rule-value" class="add-new-rule-value" value="16" <%= type == 16  ? 'checked' : '' %>/>Voucher
	<br/>
-->
	<input type="radio" name="add-new-rule-value" class="add-new-rule-value" value="4" <%= type == 4 ? 'checked' : '' %>/>Free Items
</div>
</script>

<script type="text/template" id="oncart-discount-rule-item-template">
<div>
    <br/>
    <input type="radio" name="add-new-rule-value" class="add-new-rule-value" value="1" <%= type == 1 || type == 8 ? 'checked' : '' %>/>Flat Discount
<!--    <br/>
    <input type="radio" name="add-new-rule-value" class="add-new-rule-value" value="2" <%= type == 2 ? 'checked' : '' %>/>BuyXGetY
 -->   <br/>
    <input type="radio" name="add-new-rule-value" class="add-new-rule-value" value="16" <%= type == 16  ? 'checked' : '' %>/>Voucher
	<br/>
	<input type="radio" name="add-new-rule-value" class="add-new-rule-value" value="4" <%= type == 4 ? 'checked' : '' %>/>Free Items
</div>
</script>

<script type="text/template" id="discount-rule-item-condition-template">
<div>

        <div <% if(type == 16 ) { %>style="display:none;"<% } %>>
            <input type="checkbox" value="toggleConditions" name="toggleConditions"
            <% if(buyCount || buyAmount || type == 16) { %>
                checked="true"
            <% } %>
			/>Choose Conditions
        </div>
	<br/>
	<div class="discount-rule-item-condition-container"
        <% if(type == 16) {%>
        style="display:block;"
        <% } else if(!buyCount && !buyAmount) { %>
		style="display:none;"
        <% } %>
		> 
		<div class="discount-rule-condition"><h3>Condition</h3>
		<div class="placeholdertextContainer">
			Placeholder (For conditional discounts) <input type="text" size="20" name="placeHolderText" value="<%= placeHolderText %>">
			<span style="color:red" > Warning : Will be displayed in Portal</span>
		</div>
			<div class="fl wp28">
			<input type="radio" name="condition" class="condition" value="on_buy_count" <% if ( buyCount)  { %> checked <% } %>    >
				Minimum number of Products </input>

			<br/>
			<input type="radio" name="condition" class="condition" value="on_buy_amount" <% if ( buyAmount)  { %> checked <% } %>  >
				Minimum Price </input>
			</div>
			<div class="fl wp28 pad5">
				<span style=" <% if ( !buyCount )  { %> display:none; <% } %>" >
					<input type="text" name="on_buy_count" class="condition-count" value="<%= buyCount %>"   /></span>
				<span style=" <% if ( !buyAmount )  { %> display:none; <% } %>" >
					Rs. <input type="text" name="on_buy_amount" value="<%= buyAmount %>" class="condition-amount" /></span>
			</div>
			<div class="cl"> &nbsp; </div>
		</div>
	</div>
</div>
</script>


<script type="text/template" id="discount-rt-1-template">
<div  >
    <div class="discount-rule-benefit"> <h3>Benefit</h3>
        What benefit does the customer receive ? <br/>
        <div class="fl wp40">
			<input type="radio" name="benefit" class="benefit" value="percentage"
                <% if ( percent > 0 )  { %> checked <% } %>    >
                Percentage Discount </input>

			<br/>
			<input type="radio" name="benefit" class="benefit" value="amount"
                <% if ( amount > 0 )  { %> checked <% } %>  >
                Value Discount </input>
        </div>
        <div class="fl wp40 pad5">
			<span style=" <% if ( !percent  )  { %> display:none; <% } %>" >
				<input type="text" name="percent" class="benefit-percentage" value="<%= percent %>"   /> %
			</span>
			<span style=" <% if ( !amount  )  { %> display:none; <% } %>" >
				Rs. <input type="text" name="amount" value="<%= amount %>" class="benefit-amount" />
			</span>
        </div>
        <div class="cl"> &nbsp; </div>
    </div>
</div>
</script>
<script type="text/template" id="discount-rt-4-template">
<div>
	<div class="discount-rule-benefit">
        <h3>Benefit</h3>
        <div class="fl wp28">Enter ItemId :-</div>
        <div class="fl wp40">
            <input type="text" name="item_id" value="<%= formatted.itemIdCsv %>" />
            <span class="item_name"></span>
            <br/>
            <span class="backbone-link item-id-list-link">Click here to see list of itemids</span>
        </div>
        <div class="cl"> &nbsp; </div>
        
	</div>
</div>
</script>

<script type="text/template" id="discount-rt-16-template">
    <div class="discount-rule-benefit">
        <h3>Benefit</h3>
        <!--<div class="fl wp28">Number of vouchers</div>
        <div class="fl wp40">
            <input type="text" name="count" value="<%= count %>" />
        </div>-->
        <div class="cl"> &nbsp; </div>
        <div class="fl wp28">Select Voucher Id</div>
        <div class="fl wp40">
            <select name="voucher_id">
                <option value="">Select</option>
                <% for (i in vouchers_json) { %>
                <option value="<%=vouchers_json[i].id%>"
			<%if( discountFreeItems[0].itemId == vouchers_json[i].id) { %> selected="selected" <% } %>>
			<%=vouchers_json[i].name%></option>
                <% } %>
            </select>
        </div>
        <div class="cl"> &nbsp; </div>

	</div>
</script>


<script type="text/template" id="discount-rt-2-template">
<div>

        <div style="display:none;">
            <input type="checkbox" value="toggleConditions" name="toggleConditions"
                checked="true"
			/>Choose Conditions
        </div>
	<br/>
	<div class="discount-rule-item-condition-container" style="display:block;"		> 
		<div class="discount-rule-condition"><h3>Condition</h3>
		<div class="placeholdertextContainer">
			Placeholder (For conditional discounts) <input type="text" size="20" name="placeHolderText" value="<%= placeHolderText %>">
			<span style="color:red" > Warning : Will be displayed in Portal</span>
		</div>
			<div class="fl wp28">
				<input type="radio" name="condition" class="condition" value="on_buy_count"  checked  >
					Minimum number of Products </input>
			</div>
			<div class="fl wp28 pad5">
				<span  >
					<input type="text" name="on_buy_count" class="condition-count" value="<%= buyCount %>"   /></span>
			</div>
			<div class="cl"> &nbsp; </div>
		</div>
	</div>
</div>

    <div class="discount-rule-benefit">
        <h3>Benefit</h3>
        <div class="cl"> &nbsp; </div>
        <div class="fl wp28">Number of Free products</div>
        <div class="fl wp40">
            <span  >
					<input type="text" name="count" class="benefit-count" value="<%= count %>"   /></span>
        </div>
        <div class="cl"> &nbsp; </div>

	</div>
</script>


{/literal}
