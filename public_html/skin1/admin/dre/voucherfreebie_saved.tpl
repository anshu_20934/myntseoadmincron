{if $error }
<ul>
    {foreach from=$error.message item=e}
    <li>{$e}</li>
    {/foreach}
</ul>
{else}
<ul>
    {foreach from=$limessages item=m}
    <li style="color:
        {if $m.status=='error'}red{else if $m.status=='success'}green{/if}"
        >{$m.content}</li>
    {/foreach}
</ul>
{/if}
<br/>
<a href="index.php?c=voucherfreebie&a=index" >Go Back</a>

