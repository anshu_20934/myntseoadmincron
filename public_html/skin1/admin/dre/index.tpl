<style>
{literal}
BODY,DIV {
	font-size:12px;
}
.fl {
	float:left;
}
.fr {
	float:right;
}
.wp5 {
	width: 5%;
}

.wp10 {
	width: 10%;
}
.wp12 {
	width: 12%;
}
.wp20{
	width:20%;
}
.wp28 {
	width: 28%;
}
.wp40 {
	width: 40%;
}
.cl {
	clear:both;
	float:none;
    height: 0px;
}
.border1p {
	border: 1px solid black;
}
.backbone-link {
	font-family:sans-serif; 
	color:blue; 
	cursor:pointer;
}
.backbone-link-disabled {
    font-family:sans-serif;
}
.dre-table {
	margin-top:10px;
	margin-bottom:10px;
	border-top: 1px solid #99BBE8;
	border-left: 1px solid #99BBE8;
	border-right: 1px solid #99BBE8;
}
.dre-table-head {
    background-color: #F9F9F9;
    background-image: url("/admin/extjs/resources/images/default/grid/grid3-hrow.gif");
	height: 15px;
	padding:5px;
}
.pad5 {
	padding: 5px;
}
.pad1 {
	padding: 1px;
}
.colr{
	color:red;
}
.colg{
	color:green;
}
.txal {
	text-align : left;
}
.txac {
	text-align : center;
}
DIV.DreDialogTitle {
    background-image: url("/admin/extjs/resources/images/default/panel/top-bottom.gif");
    /*background-image: url("/skin1/images/dialog_bg_n.gif");*/
    border-bottom-color: #99BBE8;
    border: 0 none;
    color: #15428B;
    font: bold 14px tahoma,arial,verdana,sans-serif;
    height: 22px;
    padding: 5 0 0 10px;
    text-align: left;
    margin:0px 5px;
}
DIV.DreDialogBorder {
    /*background-image: url("/admin/extjs/resources/images/default/panel/left-right.gif");*/
    background-color:#DFE8F6;
	border:1px solid #68C1FF;
	border-top:0px;
}
.DreDialogTitleWrapper{
    color:#15428B;
    background-image: url("/admin/extjs/resources/images/default/panel/corners-sprite.gif");
    border-bottom-color: #99BBE8;
 }
.sort-asc-image{
    background: url("/admin/extjs/resources/images/default/panel/tool-sprites.gif")  no-repeat scroll -1px -76px;
	font-size:10px;
}
.sort-desc-image{
    background: url("/admin/extjs/resources/images/default/panel/tool-sprites.gif")  no-repeat scroll -1px -62px;
	font-size:10px;
}
.x-tbar-links {
    
    background: none repeat scroll 0 0 transparent;
    background-position: center center;
    background-repeat: no-repeat;
    border: 0 none;
    cursor: pointer;
    font: 11px arial,tahoma,verdana,helvetica;
    height: 16px;
    margin: 0;
    outline: 0 none;
    padding-left: 0;
    padding-right: 0;
    white-space: nowrap;
    width: 16px;
}
.x-tbar-links-last {
    background-image: url("/admin/extjs/resources/images/default/grid/page-last.gif") !important;
}
.x-tbar-links-next {
    background-image: url("/admin/extjs/resources/images/default/grid/page-next.gif") !important;
}
.x-tbar-links-prev {
    background-image: url("/admin/extjs/resources/images/default/grid/page-prev.gif") !important;
}
.x-tbar-links-first {
    background-image: url("/admin/extjs/resources/images/default/grid/page-first.gif") !important;
}
.x-tbar-links-load {
    background-image: url("/admin/extjs/resources/images/default/grid/refresh.gif") !important;
}

{/literal}
</style>

<link href="{$http_location}/skin1/myntra_css/jquery-ui-datepicker.css" rel="stylesheet" type="text/css">
{include file="main/include_js.tpl" src="js_script/LAB.min.js"}
{include file="main/include_js.tpl" src="js_script/jquery-1.4.2.min.js"}
{include file="main/include_js.tpl" src="myntra_js/jquery-ui-datepicker.js"}
{include file="main/include_js.tpl" src="js_script/underscore.js"}
{include file="main/include_js.tpl" src="js_script/backbone.js"}

<script>
{literal}
$(document).ready(function() {
    $LAB
        .script("/skin1/js_script/dre/app.js").wait()
        .script("/skin1/js_script/dre/models.js").wait()
        .script("/skin1/js_script/dre/collections.js").wait()
        .script("/skin1/js_script/dre/viewscl.js").wait()
        .script("/skin1/js_script/dre/views/rule-list.js").wait()
        .script("/skin1/js_script/dre/views/rule-detail-list.js").wait()
        .script("/skin1/js_script/dre/views/discount.js").wait()
        .script("/skin1/js_script/dre/views/discount-rules.js").wait()
        .script("/skin1/js_script/dre/views/discount-styles.js").wait()
        .script("/skin1/js_script/dre/router.js").wait(function() {

        Dre.init();
        });
});
var discount_json = {/literal}{$discount_json}{literal};
var styles_json = {/literal}{$styles_json}{literal};
var items_json = {/literal}{$items_json}{literal};
var discount_style_map = {/literal}{$discount_style_map_json}{literal};
var vouchers_json = {/literal}{$vouchers_json}{literal};
var enabled_set_rules = {/literal}{$enabledSetRules}{literal};
var discounted_unlisted_items = {/literal}{$discounted_items_json}{literal};
var dreLoggedInUser = {/literal}'{$loggedInUser}'{literal};
{/literal}
</script>

<div id="phpError" >
	{foreach from=$phpErrors item=e}
	<div  style="color:red;font-size:14px;">{$e}</div>
	{/foreach}
</div>
<div id="message" style="background-image:url('/images/image_loading.gif');background-repeat:no-repeat;padding-left:30px;color: blue;font-size: 16px;position: fixed;top: 0;">
    <span class="message-content"></span>
</div>
<div id="main" style="font-size:14px;">
	<div>
        <div class="DreDialogTitleWrapper">
		<div class="DreDialogTitle">Discounts and Promotions Engine</div>
        </div>
		<div class="DreDialogBorder">
			<br/>
			<div style="font-size:16px;color:red;">{$dreError}</div>
			<div id="content_new" style="margin:10px 0px" ></div>
		</div>
	</div>
</div>

<!-- Underscore templates -->
<!-- discount templates -->
{include file="admin/dre/templates/discount.tpl" }
{include file="admin/dre/templates/discount-rules.tpl" }
<!-- discount templates end-->

{literal}
<script type="text/template" id="paginator-template">
</script>
<script type="text/template" id="home-content-template">
    <div class="DreDialogTitleWrapper">
<div class="DreDialogTitle">Rules and Promotions List</div></div>
<div class="DreDialogBorder">
    <div style="margin-left:0px;margin-right:10px;">
            <br/>
            <div class="fl wp28" >
                Create new
                <ul>
                    <li><a href="#discount/type/static">Static rule </a></li>
                    <li><a href="#discount/type/dynamic">Dynamic rule </a></li>
                    <li><a href="#discount/cart/create">Cart Level rule </a></li>
                </ul>
                    
            </div>
            <!--<div class="fl wp28" >
                    <input type="button" name="refresh-discount-style" value="Refresh List" />
            </div>-->
            <div class="cl"> &nbsp; </div>
            <div id="home-rule-list"></div>
    </div>
 </div>
</script>

<script type="text/template" id="rulelist-template">
<div>
    <div>
        <div>
            <div class="fl">
                <span style="color:#DD4B39">*Dynamic rules</span> &nbsp; &nbsp;	<span style="color:black">*Static rules</span>
            </div>
            <div class="fr">
                    <input type="text" style="color:grey" name="rulelist-styleid-search" value="Styleid" />
                    <select class="filter-rule-list-dynamic">
                        <option value="">Category</option>
                        <option value="dynamic">Dynamic</option>
                        <option value="static">Static</option>
                        <option value="cart">Cart Level</option>
                    </select>
                    <select class="filter-rule-list-enabled">
                        <option value="">Status</option>
                        <option value="enabled">Enabled</option>
                        <option value="disabled">Disabled</option>
                    </select>
                    <input type="button" value="Filter" class="filter-rule-list"/>
                <!--<a class="backbone-link" href="#cl/all/all">All</a> &nbsp;
                <a class="backbone-link" href="#cl/static/all">Static</a> &nbsp;
                <a class="backbone-link" href="#cl/dynamic/all">Dynamic</a> &nbsp;
                -->
            </div>
            <div class="cl"> &nbsp; </div>
         </div>
            <div class="dre-pagination-links">
            </div>
	<div class="dre-table">
            <div class="dre-table-head"  style="text-align: center;">
                    <div class="fl wp5 pad1 txal backbone-link id-sort">Id</div>
                    <div class="fl wp20 pad1 txal backbone-link name-sort">Product Set Name</div>
                    <div class="fl wp12 pad1 backbone-link styles-count-sort">No. of Styles</div>
                    <div class="fl wp12 pad1 txal backbone-link description-sort" style='
                         '>Description</div>
                    <div class="fl wp12 pad1 backbone-link start-date-sort">Start Date</div>
                    <div class="fl wp12 pad1 backbone-link end-date-sort">End Date</div>
                    <div class="fl wp12 pad1">Enabled/Disabled</div>
                    <div class="fl wp12 pad1">Actions</div>
                    <!--<div class="fl wp12 pad1">Details</div>-->
                    <div class="cl"> &nbsp; </div>
            </div>
            <div class="rulelist-display-container"></div>
	</div>
    </div>
</div>
</script>

<script type="text/template" id="rulelist-display-template">
    
    <div id="ruleitemlist-container"  style="text-align: center;"></div>

</script>

<script type="text/template" id="rulelistitem-template">
	<div style="padding:5px;border-bottom:1px solid #99BBE8;min-height:35px; <%= this.cid.replace('view','')%2 ? 'background-color:#FFFFFF;' : 'background-color:#FFF;'  %>">
		<div class="fl wp5 pad1 txal"><%= id %></div>
		<div class="fl wp20 pad1 txal"  style="color: <%= isDynamic == true ?  '#DD4B39' : 'black'  %>">
			<%= isDynamic == true ?  'Dynamic:' : ''  %><%= name %> </div>
		<div class="fl wp12 pad1 txac"> <%= formatted.stylesCount %> &nbsp; </div>
		<div class="fl wp12 pad1 txal" > <%= formatted.description %> &nbsp; </div>
		<div class="fl wp12 pad1 txac"> <%= formatted.startedOn %> &nbsp; </div>
		<div class="fl wp12 pad1 txac"> <%= formatted.expiredOn %> &nbsp; </div>
		<div class="fl wp12 pad1 txac">
			<span class="disable-enable-message-span" style="display:none;">updating..</span>
			<span class="disable-enable-select-span"><select name="disableEnableRule" class="<%= isEnabled == false ? 'colr' :'colg' %>">
				<option class="colr" value="Disable" <%= isEnabled == false ? 'selected' :'' %>>Disabled</option>
				<option class="colg" value="Enable" <%= isEnabled == true ? 'selected' :'' %>>Enabled</option>
			</select></span>
		</div>
		<div class="fl wp12 pad1 txac"> 
            		<span ><a href="#cl/discount/edit/<%= id %>" target="_blank">Edit</a></span><br/>
                    <span class="delete-rule backbone-link">
                        Delete
                    </span>
	        </div>
                <!--<div class="fl wp12 pad1"> <span class="backbone-link get-discount-details"  >[Show Details]</span> </div>-->
		<div class="cl"> &nbsp; </div>
	</div>
        <div class="home-rule-list-detail">
            <div class="home-rule-list-info-list"></div>
            <div class="home-rule-list-style-list"></div>
        </div>
</script>

<script type="text/template" id="rulelist-infolist-template">
Rules List
</script>

<script type="text/template" id="rulelist-infoitem-template">
    discount rule id <%= id %>
</script>

<script type="text/template" id="rulelist-stylelist-template">
Style List
</script>

<script type="text/template" id="rulelist-styleitem-template">
    discount rule id 
</script>





{/literal}
