{capture name=dialog}

{literal}
    <style>
    .timer-body {
        width: 100%;
    }
    .description {
        text-align: center;
    }
    .description-text {
        border: 1px solid black;
        text-align: left;
        padding: 10px;
        background: #dfdfdf;
    }
    .styleid-input {
        text-align: center;
        font-size: 14px;
        padding: 10px;
        font-weight: bold;
        border: 1px solid white;
    }
    .choose-role {
        padding: 10px;
        font-weight: bold;
        font-size: 14px;
    }
    .choose-role .change-role {
        color: blue;
        padding-left: 15px;
        text-decoration: underline;
        float:right;
    }
    .role-chosen {
        padding: 0 5px;
    }
    .role-select {
        padding: 10px;
        font-size: 14px;
    }
    .role-select select, .role-select input {
        font-size: 14px;
    }
    .timer {
        width: 100%;
        height: 200px;
        color: white;
        font-size: 30px;
        background-color: green;
        text-align: center;
        vertical-align: center;
        border-radius: 100px;
        cursor: pointer;
        float:left;
    }
    .timer .text {
        top: 50%;
        position: relative;
    }
    .start-timer {
        background-color: green;
    }
    .stop-timer {
        background-color: red;
    }
    .reset-timer {
        background-color: blue;
    }
    .timer-shim {
        background: none repeat scroll 0 0 #FFFFFF;
        height: 100%;
        width: 100%;
        position: absolute;
        left: 0;
        top: 0;
        opacity: 0.9;
        z-index: 10;
    }
    #show_timer_div {
        text-align: center;
        font-size: 14px;
        font-weight: bold;
        color: red;
    }
        
    </style>
{/literal}
<div class="timer-body">
    <form action="timer.php" method="post" name="submitRole" id="submitRole">
        <input type="hidden" name="mode" value="saveRole" />
        <div class="description">
            {if $catalogRole}
                <div class="choose-role">
                    <label>Role Chosen :</label><span class="role-chosen">{$catalogRole}</span><span class="change-role">Change role</span>
                </div>
            {/if}
                <div class="role-select {if $catalogRole}hide{/if}">
                    {if $catalogRoles}
                        <select name="catalogRole">
                            <option value="">Please Select a role</option>
                            {foreach item=catalogRoleName from=$catalogRoles name=catalogRoles}
                                {if $catalogRoleName != $catalogRole}
                                    <option value="{$catalogRoleName}">{$catalogRoleName}</option>
                                {/if}
                            {/foreach}
                        </select>
                        {if $catalogRole}
                            <input class="role-select-cancel" type="button" value="Cancel"/>
                        {/if}
                    {/if}
                </div>
                <hr/>
            {if $catalogRole}
                <div class="description-text">
                {$description}
                </div>
            {/if}
        </div>
    </form>
    {if $catalogRole}
    <hr/>
    <form action="timer.php" method="post" name="timerUpdate" id="timerUpdate">
        <div class="styleid-input">
            <label>Enter Style ID : </label><input name="styleId" type="text" pattern="[0-9]*"/>
        </div>
        <div id="show_timer_div"></div>
        <button class="timer start-timer"><span class="text">Start</span></button>
        <button class="timer stop-timer hide"><span class="text">Stop</span></button>
        <!--button class="timer small-timer reset-timer hide"><span class="text">Reset</span></button-->
        <div class="timer-shim hide"></div>
    </form>
    {/if}
</div>

{literal}
    <script type="text/javascript">
        $('#submitRole .role-select select').change(function(){
            $('#submitRole').submit();
        });
        $('.change-role').click(function(){
            $('.choose-role').hide();
            $('.role-select').removeClass('hide');
        });
        $('.role-select-cancel').click(function(){
            $('.choose-role').show();
            $('.role-select').addClass('hide');
        });
        $('.styleid-input input').focus(function(){
            $('.styleid-input').css('border-color', 'white');
        });
        $('.start-timer').click(function(e){
            e.preventDefault();
            e.stopPropagation();
            var styleId = $('.styleid-input input[name="styleId"]').val();
            if(styleId == ''){
                alert('Please enter style id');
                $('.styleid-input').css('border', '1px solid red');
                return;
            }
            $('.timer-shim').show();
            $.post('timer.php', 
                    {'mode' : 'updateTimer', 'action' : 'start', 'styleId' : styleId}, 
                    function(data){
                        data = $.parseJSON(data);
                        console.log(data);
                        if(data.status == 'success'){
                            //alert('SUCCESS : ' + data.message);
                            $('.styleid-input input[name="styleId"]').attr('disabled', 'disabled');
                            $('.start-timer').hide();
                            $('.stop-timer, .reset-timer').show();
                            startTimer();
                        }else{
                            alert('ERROR : ' + data.message);
                        }
                        $('.timer-shim').hide();
                    }
            );
        });
        $('.stop-timer').click(function(e){                                         
            e.preventDefault();                                                      
            e.stopPropagation();
            var styleId = $('.styleid-input input[name="styleId"]').val();
            $('.timer-shim').show();
            $.post('timer.php',
                    {'mode' : 'updateTimer', 'action' : 'stop', 'styleId' : styleId},
                    function(data){
                        data = $.parseJSON(data);
                        if(data.status == 'success'){
                            alert('SUCCESS : ' + data.message);
                            $('.styleid-input input[name="styleId"]').removeAttr('disabled');
                            $('.stop-timer, .reset-timer').hide();
                            $('.start-timer').show();
                            resetTimer();
                        }else{                                                       
                            alert('ERROR : ' + data.message);                        
                        } 
                        $('.timer-shim').hide();
                    }
            );
        });
        /*
        $('.reset-timer').click(function(e){ 
            e.preventDefault(); 
            e.stopPropagation();
            var r=confirm("Are you sure you want to reset the timer");
            if(r != true){
                return;
            }
            $('.styleid-input input[name="styleId"]').removeAttr('disabled');
            $('.stop-timer, .reset-timer').hide();
            $('.start-timer').show(); 
        });
        */
        // Timer implementation
        function buildTime(t) {
            minutes = t.getMinutes();
            seconds = t.getSeconds();
            hours = t.getHours();
            if (minutes < 10) {
            minutes = "0"+minutes;
            }
            if (seconds < 10) {
            seconds = "0"+seconds;
            }
            if (hours > 0) {
            return hours+":"+minutes+":"+seconds;
            } else {
            return minutes+":"+seconds;
            }
        }
         
        // Call this to start the timer
        function startTimer() {
            // If time isn't an object, create new Date and set seconds/minutes to 0
            if (typeof(time) != "object") {
            time = new Date();
            time.setSeconds(0); // Sets seconds to 0
            time.setMinutes(0); // Sets minutes to 0
            time.setHours(0);
            document.getElementById("show_timer_div").innerHTML = buildTime(time); // buildTime(time) returns 00:00
            }
            // Update seconds, to be executed every second or 1000 miliseconds
            function changeTimer() {
            time.setSeconds(time.getSeconds()+1);
            document.getElementById("show_timer_div").innerHTML = buildTime(time);
            }
            // Set Interval to every second
            interval = setInterval(changeTimer, 1000);
        }
         
        // Pauses timer, seconds/minute count will be the same when started again
        function pauseTimer() {
            clearInterval(interval);
        }
         
        // Reset timer to 00:00
        function resetTimer() {
            time = ""; // Turn time into a string
            clearInterval(interval); // Clear interval
            document.getElementById("show_timer_div").innerHTML = "00:00"; // Put timer to 0's
        }
        // End of timer implementation

    </script>
{/literal}

{/capture}

{include file="dialog.tpl" title="Mobile UA Whitelist" content=$smarty.capture.dialog extra='width="100%"'}
