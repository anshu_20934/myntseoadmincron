{literal}
<script type="text/javascript">
function func_delete_rule(elem){
    var row = $(elem).closest('tr');
    var id = row.find('input').first();
    var r=confirm("Are you sure you want to delete : "+ id.val());
    if(r == true){
        row.remove();
        document.update.mode.value = 'save';
        document.update.submit();
    }
}

function func_add_rule(){
    if ( !document.update.add_role || document.update.add_role.value == '' ||
         !document.update.add_desc || document.update.add_desc.value == '' ){
        alert('Key is empty');
        return;
    } 
    document.update.mode.value = 'add';
    document.update.submit();
}

function func_remove_all(){
    var r=confirm("Are you sure you want to remove all rules");
    if(r == true){
        $(document.update).find('table').remove();
        document.update.mode.value = 'save';
        document.update.submit();
    }
}

</script>
{/literal}

{capture name=dialog}
<form action="timerMeta.php" method="post" name="update">
<input type="hidden" name="mode" />
<table cellpadding="3" cellspacing="1" width="100%">
<tr class="TableHead">
    <td width="30%">Role</td>
    <td width="30%">Description Text</td>
    <td width="20%">Action</td>
</tr>

{if $data}

{foreach key=role item=desc from=$data name=data}
<tr{cycle values=", class='TableSubHead'"}>
        
    <td align="center" style="left-padding:30px"><input name="role[{$smarty.foreach.data.index}]" type="text" value="{$role}"  maxlength=50/></td>
    <td align="center" style="left-padding:30px"><textarea name="desc[{$smarty.foreach.data.index}]" class="" rows="4" cols="100">{$desc}</textarea></td>
    <td align="center">
        <input type="button" value="Delete" onclick="javascript: func_delete_rule(this)" />
    </td>    
</tr>
{/foreach}
{else}
<tr>
<td colspan="2" align="center">No Whitelist defined yet</td>
</tr>
{/if}
<tr class='TableSubHead' style="background-color:blue">
    <td align="center" style="left-padding:30px"><input name="add_role" type="text" value=""  maxlength=255/></td>
    <td align="center" style="left-padding:30px"><input name="add_desc" type="text" value="" maxlength=255/></td>
    <td align="center">
        <input type="button" value="Add" onclick="javascript: func_add_rule()" />
    </td>
</tr>
<tr>
    <td></td>
    <td></td>
    <td></td>
    <td class="SubmitBox" align="right">
       <input type="button" value="Remove All" onclick="javascript: func_remove_all();" />
       <input type="button" value="Update" onclick="javascript: document.update.mode.value = 'save'; document.update.submit();" />
    </td>
    <td></td>
</tr>
</table>
</form>

{/capture}
{include file="dialog.tpl" title="Timer Meta Data" content=$smarty.capture.dialog extra='width="100%"'}
