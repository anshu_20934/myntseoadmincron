<script type="text/javascript">
    var brandsData         = {$brands};
    var ageGroupsData      = {$ageGroups};
	var articleTypeData    = {$articleType};
	var progresskey        = '{$progresskey}';
    var httpLocation       = '{$http_location}';

    var selectedBrandId	   				= '{$selectedBrandId}';
    var selectedArticleTypeId 			= '{$selectedArticleTypeId}';
    var selectedAgeGroup   				= '{$selectedAgeGroup}';  
    var selectedStyleId   				= '{$selectedStyleId}';
    var selectedVendorArticleNumber   	= '{$selectedVendorArticleNumber}';
        
</script>
<link rel="stylesheet" type="text/css" href="{$http_location}/admin/extjs/resources/css/RowEditor.css"/>
<link rel="stylesheet" type="text/css" href="{$http_location}/admin/extjs/resources/css/ext-all.css"/>
<script type="text/javascript" src="{$http_location}/admin/extjs/adapter/ext/ext-base-debug.js"></script>
<script type="text/javascript" src="{$http_location}/admin/extjs/ext-all-debug.js"></script>
<script language="javascript" src="{$http_location}/skin1/myntra_js/jquery.1.4.2.min.js"> </script>
<script type="text/javascript" src="{$http_location}/admin/extjs/plugins/RowEditor.js"></script>
{** Note that I am including sizechart-position.js from skin2 assuming this is the set to be validated against. 
	In case the default skin is changed, this will need to change too**}
<script type="text/javascript" src="{$http_location}/skin2/js/sizechart-position.js"></script>
<script type="text/javascript" src="size_measurement_unif.js"></script>

<div id="contentPanel"></div>
