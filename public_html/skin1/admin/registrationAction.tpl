<html>
<head>
	<title>Registation Process</title>
	<script src="/skin1/jquery-1.8.8/jquery-1.4.4.min.js"></script>
	<script src="/skin1/jquery-1.8.8/jquery-ui-1.8.8.custom.min.js"></script>
	<link rel="stylesheet" href="/skin1/jquery-1.8.8/css/jquery-ui-1.8.8.custom.css">
	{literal}
	<script>	
		$(document).ready(function(){
			//Hide (Collapse) the toggle containers on load
			$(".toggle_container").hide(); 
	
			//Switch the "Open" and "Close" state per click then slide up/down (depending on open/close state)
			$("h2.trigger").click(function(){
				$(this).toggleClass("active").next().slideToggle("slow");
				return false; //Prevent the browser jump to the link anchor
			});
		});
	$(function() {
		var dates = $( ".from, .to" ).datepicker({
			defaultDate: "+1w",
			changeMonth: true,
			changeYear: true,
			numberOfMonths: 1,
			onSelect: function( selectedDate ) {
				//var option = this.id == "from" ? "minDate" : "maxDate",
					instance = $( this ).data( "datepicker" );
					date = $.datepicker.parseDate(
						instance.settings.dateFormat ||
						$.datepicker._defaults.dateFormat,
						selectedDate, instance.settings );
				//dates.not( this ).datepicker( "option", option, date );
			}
		});		
	});
	</script>
	{/literal}
</head>
<body>

{if $message}
	<h2>{$message}</h2>
{/if}

<h1>Available Classes:</h1>

{foreach from=$reg_classes key=k item=v}
	<h2 class="trigger"><a href="#">{$v}</a></h2>
	<div class="toggle_container">
		<div class="block">
			<form method="post" id="{$v}" name="{$v}" action="{$catalogs.admin}/registrationAction.php">
				<input type="hidden" name="class_name" value="{$v}" />
				<input type="hidden" name="current_time" value="{$current_time}" />
				<table>
					<tr>
						<td><label for="channel_code">Channel Code : </label> </td>
						<td><input type="text" size ="10" maxlength="20" name="channel_code" value="{$class_channel.$v}" /></td>
					</tr>
					{foreach from=$mail_template[$v] key=index item=name}
						{if $name.registration_email_template}
							<tr>
								<td><label for="name">Email Template : </label></td>
								<td><input type="text" size ="30" name="email_tempelate-{$index}" value="{$name.registration_email_template}" /></td>
								<td>
								<div class="demo">
									<label for="from">From</label>
									<input size ="10" type="text" class="from" name="from-{$index}" value="{$name.start_date}"/>
									<label for="to">to</label>
									<input size ="10" type="text" class="to" name="to-{$index}" value="{$name.end_date}" />
								</div><!-- End demo -->
								 </td>
							</tr>
						{/if}
					{/foreach}
					<tr>
							<td>
								<label for="name">Email Template : </label>
							</td>
							<td>
								<select name="email_tempelate-{$v}">
									{html_options values=$emailTemplates output=$emailTemplates selected="1"}
								</select>
							</td>
							
							<td>
								<div class="demo">
									<label for="from">From</label>
									<input size ="10" type="text" class="from" name="from-{$v}"/>
									<label for="to">to</label>
									<input size ="10" type="text" class="to" name="to-{$v}"/>
								</div><!-- End demo -->
							 </td>
						</tr>
				</table>
				<table>
					<tr>
						<th>Function To Execute</th>
						<th>Exceution Order</th>
					</tr>
					
					{foreach from=$class_methods[$v] key=index item=name}
						{if $name neq "__construct" && $name neq "executeActions" && $name neq "checkUser"}
						<tr>					
							<td>{$name}</td>
							<td><input type="text" size ="10" maxlength="2" name="{$name}" value="{$action_array.$v.$name}" /></td>
						</tr>
						{/if}
					{/foreach}
					<tr>
						<td><input type="submit" value="Save" /></td>
					</tr>
				</table>
			</form>
		</div>
	</div>	
{/foreach}



</body>
</html>