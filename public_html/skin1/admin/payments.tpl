{* $Id: menu.tpl,v 1.68.2.2 2006/06/20 05:52:05 svowl Exp $ *}

{capture name=menu}
<li><a href="{$catalogs.admin}/netbanking/activate_gateway.php" class="VertMenuItems">Edit NetBanking Options</a></li>
<li><a href="{$catalogs.admin}/netbanking/bank_list.php" class="VertMenuItems">Add/Edit NetBanking Bank</a></li>
<li><a href="{$catalogs.admin}/netbanking/add_gateway.php" class="VertMenuItems">Add/Edit NetBanking Gateway</a></li>
<li><a href="{$catalogs.admin}/payment_reconciliation.php" class="VertMenuItems">Payment Reconciliation</a></li>
{/capture}
{include file="menu.tpl" dingbats="dingbats_categorie.gif" menu_title='Payments' menu_content=$smarty.capture.menu }
