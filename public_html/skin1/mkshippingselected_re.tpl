{foreach key=key item=item from=$userinfo}
   {if $key eq 's_firstname'}
    {assign var='firstname' value=$item}
    {/if}
    {if $key eq 's_lastname'}
    {assign var='lastname' value=$item}
    {/if}
    {if $key eq 's_address'}
    {assign var='address' value=$item}
    {/if}
    {if $key eq 's_city'}
    {assign var='city' value=$item}
    {/if}
    {if $key eq 's_country'}
    {assign var='country' value=$item}
    {/if}
     {if $key eq 's_state'}
    {assign var='state' value=$item}
    {/if}
    {if $key eq 's_zipcode'}
    {assign var='zipcode' value=$item}
    {/if}

    {if $key eq 'phone'}
    {assign var='phone' value=$item}
    {/if}
    {if $key eq 'mobile'}
    {assign var='mobile' value=$item}
    {/if}
    {if $key eq 'email'}
    {assign var='email' value=$item}
    {/if}
    {if $key eq 's_state'}
    {assign var='state' value=$item}
    {/if}

{/foreach}
{assign var="ship" value="shipping"}
{assign var="fill" value="filling"}
{assign var="flush" value="flushing"}

<table>


	<tr>
		<td>
 			<font style="font-size:0.8em">first name</font><span class="mandatory">*</span>
 		</td>
 		<td >
 			<input type="text" name="s_firstname" id="s_firstname" value="{$firstname}" >
 		</td>
 	</tr>

 	<tr>
		<td >
 			<font style="font-size:0.8em">last name</font><span class="mandatory">*</span>
 		</td>
 		<td >
 			<input type="text" name="s_lastname" id="s_lastname" value="{$lastname}" >
 		</td>
 	</tr>

 	<tr>
		<td valign="top">
 			<font style="font-size:0.8em">address </font><span class="mandatory">*</span>
 		</td>
 		<td >
 			<textarea name="s_address" id="s_address" class="address">{$address}</textarea><br>
            <input type=hidden size=2 value=255 name="text_num" id="text_num">
 			<!-- <input type="text" name="s_address1" id="s_address1" value="{$address}" > -->
 		</td>
 	</tr>

	
 	<tr>
		<td >
 			<font style="font-size:0.8em">country</font><span class="mandatory">*</span>
 		</td>
 		<td >
 		
 			<select name="s_country" id="s_country" class="select" onchange="javascript:onchange_country('s_country');updateShipping_re();" >

 			    
 			{foreach key=countrycode item=countryname from=$returnCountry}
		
  {if $countrycode == $shipping_country }
 	 <option value="{$countrycode}" selected>{$countryname}</option>
 	{else}
   <option value="{$countrycode}" >{$countryname}</option>
 	{/if}
 {/foreach} 			
</select>
 		</td>
 	</tr>

 	
 	
 	<tr>
		<td >
 			<font style="font-size:0.8em">state</font><span class="mandatory">*</span>
 		</td>
 		<td >
 		
 		<div id="s_state_list" >
 		{if $returnState != '' }	
 		<select name="s_state" id="s_state" class="select" onchange="javascript:updateShipping_re();">
				<option value="" selected>--Select State--</option>
 			{section name="statename" loop=$returnState}
 				{if $returnState[statename].code == $s_state}
 				<option value={$returnState[statename].code} selected>{$returnState[statename].state}</option>
 				{else}
 				<option value={$returnState[statename].code}>{$returnState[statename].state}</option>
 				{/if}
 			{/section}
 			</select>
 			
 			{else}
 			<input type="text" id="s_state" value="{$s_state_name}" onblur="javascript:updateShipping_re();">
 			{/if}
 			</div>
 			
 		</td>
 	</tr>

 	
    <tr>
		<td >
 			<font style="font-size:0.8em">city</font><span class="mandatory">*</span>
 		</td>
 		<td >
 		<div id= "s_city_IN">
 			
 			<input type="text" name="s_city" id="s_city" value="{$city}" onblur="updateShipping_re();" >
 			
 		</div>
 		
 		<!--<div id= "s_city_NotIN" style="display:none;">
 			
 			<input type="text" name="s_city" id="s_city" value="{$city}">
 			
 		</div>-->
 		    <input type="hidden" name="totalamount1" id="totalamount1" value="{$totalAmountAfterDiscount}"><!--made 'totalamount' to 'totalamount1' by arun since there was another id same as this 'totalAmount', which leads to prolem in IE for gift calculation-->
 			<input type="hidden" name="totalquantity" id="totalquantity" value="{$totalQuantity}">
 			<input type="hidden" name="shippingrate" id="shippingrate" value="{$shippingRate}">
 	    </td>
 	</tr>

 	
 	
 	<tr>
		<td >
 			<font style="font-size:0.8em">zip/postal code</font><span class="mandatory">*</span>
 		</td>
 		<td >
 			<input type="text" name="s_zipcode" id="s_zipcode" value="{$zipcode}" onchange="javascript:updateShipping_re();" maxlength="6" size="7"></input>
 		</td>
 	</tr>
	<tr>
		<td/>
		<td>
			<span style="color:red;font-size:0.6em;">Please enter correct pin code to ensure delivery</span>
		</td>
	</tr>
	<tr>
		<td >
 			<font style="font-size:0.8em">phone</font>
 		</td>
 		<td >
 			<input type="text" name="s_phone" id="s_phone" value="{$phone}" >
 		</td>
 	</tr>

	<tr>
		<td >
 			<font style="font-size:0.8em">mobile</font><span class="mandatory">*</span>
 		</td>
 		<td >
 			<input type="text" name="s_mobile" id="s_mobile" value="{$mobile}"  >
 		</td>
 	</tr>

 	<tr>
		<td >
 			<font style="font-size:0.8em">email</font><span class="mandatory">*</span>
 		</td>
 		<td >
 			<input type="text" name="s_email" id="s_email" value="{$email}" >
 		</td>
 	</tr>

 	<tr>
    <td colspan="2">

        <input type="checkbox" name="updateship" id="updateship" value="upship" class="checkbox">
        &nbsp;<font style="font-size:0.8em">update shipping address in my account</font>

    </td>
  </tr>
 	</table>
