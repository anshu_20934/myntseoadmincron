 <title>
      {if $pageTitle ne ''}{$pageTitle}{elseif $pageHeader ne ''}{$pageHeader}{else}Myntra{/if}
  </title>
  <meta name="description" content="{$metaDescription|escape}" />
  <meta name="keywords" content="{$metaKeywords|escape}" />
  {if $noindex eq 2}
	<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
  {/if}
  <link rel="shortcut icon" href="{$SkinDir}/favicon.ico">
  <link href="{$http_location}/min/g=css" rel="stylesheet" type="text/css" />
  {if $affiliateId neq '' && $themeid eq 'COBRANDED_UI'}
	<link rel="stylesheet" href="{$SkinDir}/affiliatetemplates/affiliate.css" />
  {/if}
  {if $thickbox }
  	{$thickbox}
  {/if}
  {if $tooltip }
  	{$tooltip}
  {/if} 
  <script type="text/javascript">
    var http_loc = '{$http_location}'
    var cdn_base = '{$cdn_base}'
  </script>
  <base href="{$baseurl}">
