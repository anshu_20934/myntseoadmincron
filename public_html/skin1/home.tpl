<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
    <html xml:lang="en" xmlns:fb="http://www.facebook.com/2008/fbml">
    <head>
    {include file="site/header.tpl" }
    <link href="{$combine_css_path}&f=jersey_widget.css" rel="stylesheet" type="text/css"/>
    </head>
    <body id="body">
    <div id="wrapper_no_border">
    {include file="site/top.tpl" }
    <div id="container">
      <!--Header ends here-->
      <div class="clear"></div>
      <!--Banner starts here-->
      <div id="midcontent">
    {include file="home/jersey_widget.tpl" }

<div class="clear empty-block">&nbsp;</div>

<div class="slidshow">
	 <div class="images">
    	{section name=idx loop=$images.front_banner}
    	<div>
    		 <a href="{$images.front_banner[idx].url}"><img src="{$cdn_base}/skin1/images/spacer.gif" width="633" height="264" id="fb_{$images.front_banner[idx].id}" lazy-src="{$images.front_banner[idx].image}" class="lazy" alt="{$images.front_banner[idx].alt}" title="{$images.front_banner[idx].title}"></a>
    	</div>
        {/section}

    </div>

    <div class="slidetabs" style="display:none">
        {section name=idx loop=$images.front_banner}
    	
    	<div class="outer">
                <div class="inner">
                 <a href="#">&nbsp;</a>
                </div>
        </div>
    
        {/section}
    </div>	 

	</div>

<div class="youcan right_images">
 {section name=idx loop=$images.right_banner}
 <div>
          {if $images.right_banner[idx].url} <a href="{$images.right_banner[idx].url}"> {/if}
          <img src="{$cdn_base}/skin1/images/spacer.gif" width="331" height="264" id="yc_{$images.right_banner[idx].id}" lazy-src="{$cdn_base}/{$images.right_banner[idx].image}" class="lazy" alt="{$images.right_banner[idx].alt}" title="{$images.right_banner[idx].title}">
          {if $images.right_banner[idx].url} </a> {/if}

        </div>
        {/section}
        </div>

<div class="right_slidetabs">
{section name=idx loop=$images.right_banner}
    <a href="#"></a>
 {/section}   
</div>

<div class="clear empty-block">&nbsp;</div>


{$sections[1]}



          <div id="blog">
        <div class="header">
          <h1 align="center">MOST POPULAR</h1></div>
                         <div class="tags">

                        					 		{section name=idx loop=$arr_seo}
			{if $arr_seo[idx][1]  eq '' && $arr_seo[idx][3]  neq ''}
				<a href='{$http_location}{$arr_seo[idx][3]}'>{$arr_seo[idx][2]}</a>&nbsp;&nbsp;
			{/if}
		 {/section}
		 </div>
         </div>
<div class="clear empty-block">&nbsp;</div>


{$sections[2]}


       <div id="sell" >
        <div class="right_bottom_images" style="height:242px;overflow:hidden;width:335px;">
{section name=idx loop=$images.right_bottom_banner}
<div >
          <a href="{$images.right_bottom_banner[idx].url}">
          <img src="{$cdn_base}/skin1/images/spacer.gif" width="333" height="240"  id="sell_{$images.right_bottom_banner[idx].id}" lazy-src="{$cdn_base}/{$images.right_bottom_banner[idx].image}" class="lazy" alt="{$images.right_bottom_banner[idx].alt}" title="{$images.right_bottom_banner[idx].title}">
        </a> </div>
        {/section}
</div>
<div class="right_bottom_slidetabs" style="display:none">
{section name=idx loop=$images.right_bottom_banner}
    <div class="outer">
                <div class="inner">
                 <a href="#">&nbsp;</a>
                </div>
    </div>
{/section}   
</div>
</div>

<div class="clear empty-block">&nbsp;</div>



        <div id="midleftcnt">

        {$sections[3]}
 <div class="clear empty-block">&nbsp;</div>

{$sections[4]}

               {* second tabs box end *}
        <div id="abtmyn">
			<h2>About <font color="#93b429">Myntra</font></h2>
			<p>{$properties.home.aboutus.value}</p>

        </div>
        <div class="clear"></div>
      </div>

      <div id="midrightcnt" >
	{include file='facebookLike.tpl' widget_width='333' widget_height='170' widget_height_in='160' connection_cnt='6'}

        <div id="testi"><span class="testi"><b>Testimonials</b></span>
         {section name=idx loop=$testimonials}
          <p><span><br>
            {$testimonials[idx].content}
            </span>
            </p> <div class="ico_testi_text"><b>{$testimonials[idx].name}</b></div>
          <span>&nbsp;</span>
          {/section}
	
            <div style="clear:both; float:right; width: 330px; margin-top:10px">
            <h2 style="float:right;"><a href="http://twitter.com/myntradotcom"><span class="twitter"></span></a><a href="http://www.facebook.com/myntra"><span class="facebook"></span></a></h2>
			  <h2 style="margin: 10px 0 0 0 ;float:right;">
			  FOLLOW
			  <span style="color:#93b429;margin:0 0 0 5px;">Myntra</span>
			  </h2>
	</div>
        </div>
      </div>
 </div>
	<div class="clear"></div>
    {include file="site/footer.tpl" }
{literal}
<script type="text/javascript">
   $(window).load(function() {
    if($('div.tabs').length > 0) {
                $("div.tabs").tabs("div.pane > div.panes");
                $("div.scrollable").scrollable();
                if($('.slidetabs').length > 0){
                    $(".slidetabs").tabs(".images > div", {
                        // enable "cross-fading" effect
                        effect: 'fade',
                        fadeOutSpeed: "slow",
                        // start from the beginning after the last tab
                        rotate: true
                        // use the slideshow plugin. It accepts its own configuration
                    }).slideshow({interval : 5000});
                    $(".slidetabs").data("slideshow").play();
                }
                if($('.right_slidetabs').length > 0){
                    $(".right_slidetabs").tabs(".right_images > div", {
                        // enable "cross-fading" effect
                        effect: 'fade',
                        fadeOutSpeed: "slow",
                        // start from the beginning after the last tab
                        rotate: true
                        // use the slideshow plugin. It accepts its own configuration
                    }).slideshow({interval : 5000});
                    $(".right_slidetabs").data("slideshow").play();
                }
				if($('.right_bottom_slidetabs').length > 0){
                    $(".right_bottom_slidetabs").tabs(".right_bottom_images > div", {
                        // enable "cross-fading" effect
                        effect: 'fade',
                        fadeOutSpeed: "slow",
                        // start from the beginning after the last tab
                        rotate: true
                        // use the slideshow plugin. It accepts its own configuration
                    }).slideshow({interval : 5000});
                    $(".right_bottom_slidetabs").data("slideshow").play();
                }
				
            }
    });
</script>
{/literal}
    <!--Banner ends here-->
    <!--footer starts here-->
	</div>
    </body>
    </html>
