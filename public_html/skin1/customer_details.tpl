{* $Id: mykritihome.tpl,v 1.88.2.3 2006/07/19 10:19:35 max Exp $ *}
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<link rel="stylesheet" href="{$http_location}/skin1/mykriti.css" />
{include file="mykritimeta.tpl" }
<body id="body">
<div id="scroll_wrapper">
	<div id="container"> 
		<div id="wrapper"> 
			<div id="display"> 
				<div class="center_column" style="float:none"> 
			        <div class="print"> 
				        <div class="super"> 
						</div>
					</div>
				</div>
			</div>
			<div class="links"> 
            	<table cellpadding="2" class="printtable">
	              	<tr> 
		                <td class="align_left" width="25%"><b>Store name:</b></td>
            		    <td class="align_left" width="25%">	<b> {$store.source_name}</b></td>
			            <td class="align_left" width="25%"><b>Sales person:</b></td>
            		    <td class="align_left" width="25%">	<b> {$customer.personel}</b></td>
					</tr>
	              	<tr> 
		                <td class="align_left"><b>Order Id:</b></td>
            		    <td class="align_left">	<b> {$customer.orderid}</b></td>
			            <td class="align_left"></td>
    		            <td class="align_left"></td>
					</tr>
	              	<tr> 
			            <td class="align_left" width="25%">&nbsp;</td>
			            <td class="align_left" width="25%">&nbsp;</td>
			            <td class="align_left" width="25%">&nbsp;</td>
    		            <td class="align_left" width="25%">&nbsp;</td>
					</tr>
	              	<tr> 
		                <td class="align_left">First Name</td>
            		    <td class="align_left">{$customer.firstname}</td>
			            <td class="align_left">Last Name</td>
    		            <td class="align_left">{$customer.lastname}</td>
					</tr>
	              	<tr> 
		                <td class="align_left">Email</td>
            		    <td class="align_left">{$customer.email}</td>
			            <td class="align_left">Mobile</td>
    		            <td class="align_left">{$customer.mobile}</td>
					</tr>
	              	<tr> 
			            <td class="align_left"></td>
    		            <td class="align_left"></td>
					</tr>
	              	<tr> 
		                <td class="align_left">Address</td>
            		    <td class="align_left">{$customer.address1}</td>
		                <td class="align_left">City</td>
            		    <td class="align_left">{$customer.city}</td>
					</tr>
	              	<tr> 
			            <td class="align_left">Zip</td>
    		            <td class="align_left">{$customer.zip}</td>
		                <td class="align_left">State</td>
            		    <td class="align_left">{$customer.state}</td>
					</tr>
	              	<tr> 
			            <td class="align_left">Country</td>
    		            <td class="align_left">{$customer.country}</td>
		                <td class="align_left">Alternate Number</td>
            		    <td class="align_left">{$customer.alt_number}</td>
					</tr>
				</table>
			</div>
		</div>
  		<div id="footer"> 
    		<div class="footertext">&nbsp; 
		    </div>
		    <div class="footercopy">Copyright&copy; Myntra. All rights reserved.&nbsp;&nbsp;&nbsp;</div>
		</div>
	</div>
</div>
</body>
</html>

