<html>
<head>
{literal}
<script Language="JavaScript" Type="text/javascript" src="../skin1/main/validation_inv.js"></script>
<script Language="JavaScript" Type="text/javascript">

		function FrontPage_Form1_Validator(theform)
		{
         //alert("Please Generate a valid sku first");
         var skuname=theform.sku;
         var skustatus=theform.skustatus;
         var check=theform.skuchecked;
         var vendor=theform.preferred_vendor;
         var count=theform.current_value;
         var unitprice=theform.standard_unit_price;
         var thresholdemail=theform.threshold_email;
         var thresholdcount=theform.threshold_count;
         if(check.value=="false"){
         alert("Please Generate a valid sku first");
         return false;
         }
         
          if (skuname.value==null||skuname.value == ""||skustatus.value=="N")
		  {
			alert("Please Generate a valid sku first");
			skuname.focus();
			return false;
		  }
		  if (vendor.value==null||vendor.value == "")
		  {
			alert("Please enter the vendor name");
			vendor.focus();
			return false;
		  }
		  if (thresholdcount.value==null||thresholdcount.value == ""||!(isPositiveInteger(thresholdcount.value)))
		  {
			alert("Please enter a valid threshold count >=0");
			thresholdcount.focus();
			return false;
		  }
		  if (thresholdemail.value==null||thresholdemail.value == "")
		  {
			alert("Please enter the threshold email");
			thresholdemail.focus();
			return false;
		  }
	     if (echeck(thresholdemail.value)==false){
		    thresholdemail.value="";
		    thresholdemail.focus();
		    return false;
	     }
		  if (unitprice.value==null||unitprice.value == ""||!(isValidNumber(unitprice.value)))
		  {
			alert("Please enter a valid unit Price");
			unitprice.focus();
			return false;
		  }
		  if (count.value==null||count.value == ""||!(isPositiveInteger(count.value)))
		  {
			alert("Please enter initial count >=0");
			count.focus();
			return false;
		  }
		  
         
		  return true;
		  
		}
	
		</script>
		{/literal}

<link rel="stylesheet" href="../skin1/mykriti.css"/>

</head>
<body id="body">
<div id="scroll_wrapper">
	<div id="container"> 
	  <div id="wrapper"> 
	    <div id="display"> 
	      <div class="center_column"> 
	        <div class="print"> 
		        <div class="super" > 
	           		 <p>&nbsp;</p>
	          	</div>
	          	{if $commentSuccess ne "Y"}
	          	<div class="head" style="padding-left:15px;"> 
		            <p>&nbsp;&nbsp;Register New SKU</p>
          		</div>
          		<div class="links"><p></p></div>
          		<div class="foot"></div>
          		
          		
<form name="registerForm" method="post" onsubmit="return FrontPage_Form1_Validator(this)" language="JavaScript" action="register_sku.php">
<input type="hidden" name="register" value="true" />
<input type="hidden" name="skuchecked" value="false" />
         <div id="skutest">
				</div>
		<table style="border:1px black solid; width:98%;">
			
			<tr>
				<td style="text-align:left;">Date Added</td>
				<td style="text-align:left;"><input type="text" name="date_added" style="width:200px;" value="{$date}" readonly ></td>
			</tr>
			<tr>
				<td style="text-align:left;">Product Type</td>
				<td style="text-align:left;">
					<select name="ProductType" style="width:200px;" onchange="getproductstyles();">
					<option value="" selected>select</option>
					{foreach from=$types key=key item=item}
						{if $key eq $prselect}
						<option name="{$key}" value="{$key}" selected>{$item}</option>
						{else}
						<option name="{$key}" value="{$key}">{$item}</option>
						{/if}
					{/foreach}
					</select>
				</td>	
				<td style="text-align:left;">Product Style</td>	
						
				<td style="text-align:left;" id="styleid">
					
				<input type="text" name="styleComment" value ="Please select product type first" style="width:200px;color:red;" readonly>	
				</td>
			</tr>
			<tr><td colspan="4">
			<div id="optionid">
			</div>
			</td>
			<tr>
		</table>
		<br><br>
		<table style="width:98%;">
			<tr>
				<td style="text-align:left;">Preferred Vendor</td>
				<td style="text-align:left;"><input type="text" name="preferred_vendor" style="width:200px;"></td>
				<td style="text-align:left;">&nbsp;</td>
				<td style="text-align:left;">&nbsp;</td>
			</tr>
			<tr>
				<td style="text-align:left;">Threshold Count</td>
				<td style="text-align:left;"><input type="text" name="threshold_count" style="width:200px;"></td>
				<td style="text-align:left;">&nbsp;</td>
				<td style="text-align:left;">&nbsp;</td>
			</tr>
			<tr>
				<td style="text-align:left;">Threshold Email</td>
				<td style="text-align:left;"><input type="text" name="threshold_email" style="width:200px;"></td>
				<td style="text-align:left;">&nbsp;</td>
				<td style="text-align:left;">&nbsp;</td>
			</tr>
			<tr>
				<td style="text-align:left;">Standard Unit Price</td>
				<td style="text-align:left;"><input type="text" name="standard_unit_price" style="width:200px;"></td>
				<td style="text-align:left;">Current Count</td>
				<td style="text-align:left;"><input type="text" name="current_value" style="width:200px;"></td>
			</tr>
			<tr>
				<td style="text-align:left;">Notes</td>
				<td style="text-align:left;" colspan=3><textarea name="notes" cols='50' rows='5'></textarea></td>
			</tr>
			
			<tr>
				<td style="text-align:left;" colspan=3>&nbsp;</td>
				<td style="text-align:right;"><input type="submit"  value="Register" style="width:80px; background-color:#000000; color:#ffffff;"></td>
			</tr>
		</table>
	</form>
		{elseif $commentSuccess eq "Y"}
		<table>
				<tr>
					<td>SKU registered Succesfully!!!!!</td>
					<td>&nbsp;&nbsp;</td>
					<td>&nbsp;&nbsp;</td>
				</tr>
				<tr>
					<td style="font-size:.8em;color:red;"><b>Please Click the close button below to refresh data on back screen</b></td>
				</tr>
				<tr>
					<td align="center"><input type="button" style="height:20px;width:50px; background-color:black; color:white;" value="Close" onclick="opener.window.location.reload();self.close()"></td>
				</tr>
		</table>
		{/if}
		</div> 
	      </div>   	
	     </div> 
	   </div> 
	 </div>  
	</div>
	{literal}
		<script type="text/javascript">
		var request = createRequest();
		function getproductstyles(){
		     request = createRequest();
			 var url = "register_sku.php";
			 var pr_id = document.registerForm.ProductType.value;
			 var poststr = "pr_id=" + encodeURI(pr_id);
			 sendRequest(request,url,poststr,show_styles);
		}
		function getstyleoptionsmap(){
		     request = createRequest();
			 var url = "register_sku.php";
			 var st_id = document.registerForm.ProductStyle.value;
			 var poststr = "st_id=" + encodeURI(st_id);
			 sendRequest(request,url,poststr,show_options);
		}
		function sendRequest(request,url,parameters,func_name) {
		  request.onreadystatechange = func_name;
		  request.open("POST", url, true);
		  request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		  request.setRequestHeader("Content-length", parameters.length);
		  request.setRequestHeader("Connection", "close");
		  request.send(parameters);
		}
		
		function validateSKU() {
		request = createRequest();
		var url = "register_sku.php";
		var skustr="";
		if(document.registerForm.skuchecked.value=="true"){
		skustr="@SKU"+document.registerForm.sku.value;
		}
		document.registerForm.skuchecked.value="true";
		//var sku=document.registerForm.sku.value;
		var optionstr=selOption();
		optionstr+=skustr;
		//alert(optionstr);
			// var pr_id = document.registerForm.ProductType.value;
			if(optionstr!="false"){
			 var poststr = "validate=" + encodeURI(optionstr);
			 sendRequest(request,url,poststr,show_skutest);
			}
		}
		function selOption() {
			var elements = document.getElementsByTagName("select");
			var optionstr = "";
			for(var i=0; i<elements.length; i++){
					var elementname = elements.item(i).name;
  					var elementvalue = elements.item(i).value;
  					if(elementvalue==""){
  					alert("Please select value for : "+elementname);
  					return "false";
  					}
  				
  					if(i!=(elements.length-1)){
  					optionstr = optionstr+elementname+":"+elementvalue+"#";
  					}
  					else{
  					optionstr = optionstr+elementname+":"+elementvalue;
  					}
				
			}
			//alert(optionstr);
			return optionstr;
		}
		function show_styles() {
			 if (request.readyState == 4) {
				if (request.status == 200) {
					//window.document.write(request.responseText);
					var response = request.responseText;
					document.getElementById("styleid").innerHTML = response;
				}
				else {
					alert("Error! Request status is " + request.status);
				}
			}
		}
		function show_skutest() {
			 if (request.readyState == 4) {
				if (request.status == 200) {
					//window.document.write(request.responseText);
					var response = request.responseText;
					//alert(response);
					
					document.getElementById("skutest").innerHTML = response;
					
					//alert(document.getElementById("skutest").innerHTML);
				}
				else {
					alert("Error! Request status is " + request.status);
				}
			}
		}
		
		function show_options() {
		
			 if (request.readyState == 4) {
				if (request.status == 200) {
					//window.document.write(request.responseText);
					var response = request.responseText;
					//alert("Error! Request status is " + response);				
					document.getElementById("optionid").innerHTML = response;
					//alert(document.getElementById("optionid").innerHTML);
				}
				else {
					alert("Error! Request status is " + request.status);
				}
			}
		}
		function createRequest() {
		  var request = null;
		  try {
			request = new XMLHttpRequest();
		  } catch (trymicrosoft) {
			try {
			  request = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (othermicrosoft) {
			  try {
				request = new ActiveXObject("Microsoft.XMLHTTP");
			  } catch (failed) {
				request = null;
			  }
			}
		  }

		  if (request == null) {
			alert("Error creating request object!");
		  } else {
			return request;
		  }
		}

		
		</script>
	{/literal}
	</body> 
	</html>
    	