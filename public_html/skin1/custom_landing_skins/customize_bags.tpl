<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
{config_load file="$skin_config"}
<html xml:lang="en" xmlns:fb="http://www.facebook.com/2008/fbml">
<!--<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html xml:lang="en" xmlns:fb="http://www.facebook.com/2008/fbml">-->
<head>
{include file="site/header.tpl" }
	<!--<title>own personalized products page</title>-->
	<link href="{$SkinDir}/css/customize_bags.css" rel="stylesheet" type="text/css">
	<!--<link href="{$SkinDir}/css/index.css" rel="stylesheet" type="text/css">-->
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">	
	<script src="{$SkinDir}/js_script/jquery.js" type="text/javascript" language="javascript"></script>
	<script type="text/javascript" src="{$SkinDir}/js_script/ajaxObject.js"></script>
	<script src="{$SkinDir}/js_script/customize_bags.js" type="text/javascript" language="javascript"></script>	
</head>

<body id="body" style="cursor:auto;">
	<div id="scroll_wrapper">
		<div id="container">
			{include file="site/top.tpl"}
			<div id="wrapper"  class="bags_container">
				{include file="breadcrumb.tpl" }		
				<div  class="bags-text-int">
					<h1>{$selected_type_h1}</h1>
					{$selected_type_desc}
				</div>
				<div class="bags-banner"><img src="{$cdn_base}/skin1/mkimages/bags/bags-banner.jpg" alt="banner"></div>
				<div class="clear"></div>
				<!-- Tabs -->
				<div id="bags-pro-items-list">
					<div class="bags-tabs">
						<ul class="bags-tab-list">
							{section name=idx loop=$product_types}
								{if $product_types[idx].type_id eq $selected_type_id}
									<li class="active"><span><a href="{$http_location}/{$product_types[idx].type_name}">{$product_types[idx].type_name|replace:"-":" "}</a></span></li>
								{else}
									<li><span><a href="{$http_location}/{$product_types[idx].type_name}">{$product_types[idx].type_name|replace:"-":" "}</a></span></li>
								{/if}
							{/section}								
						</ul>
					</div>
				</div>
				<div class="clear"></div>
				
				<!-- Product Items list -->
				<div class="items-block">
					<div class="bags-list-items">
						{foreach key=stylegroup item=styleinfo from=$style_group_info}	
							<h2>{$stylegroup}</h2>
							<span>----------------------------------------------------------------------------------------------------------------------------------------</span>
							<ul class="pro-list-items">
								{foreach item=style from=$styleinfo}
									{if $style.style_id eq $default_style_id}
										{assign var=default_style_detail value=$style.description}
										{assign var=default_style_name value=$style.style_name}										
										{assign var=default_style_price value=$style.style_price|string_format:"%d"}										
										<li class="selected" id="{$style.style_id}">
											<span class="img"><a href="javascript:void(0);"><img src="{$cdn_base}/{$style.style_image}" alt="{$style.style_name}"></a></span>
											<p class="item-name-price">
												<strong class="name">{$style.style_name}</strong> 
												<span>Price: Rs.<strong class="price">{$style.style_price|string_format:"%d"}/-</strong></span>
											</p>	
										</li>
									{else}
										<li id="{$style.style_id}">
											<span class="img"><a href="javascript:void(0);"><img src="{$cdn_base}/{$style.style_image}" alt="{$style.style_name}"></a></span>
											<p class="item-name-price">
												<strong class="name">{$style.style_name}</strong> 
												<span>Price: Rs.<strong class="price">{$style.style_price|string_format:"%d"}/-</strong></span>
											</p>	
										</li>
									{/if}
								{/foreach}								
							</ul>
							<div class="clear"></div>								
						{/foreach}								
					</div>
					
					<div class="bags-items-previews">
						<div class="item-pname" id="style_name">{$default_style_name}</div>
						<div class="item-preview-img">
							<div class="img-pre"><img src='{$cdn_base}/{$default_area_image}' border="0"></div>
							<div class="thumb-block">
								<ul class="preview-thumb">
									{section name=idx loop=$style_cust_info}
										{if $style_cust_info[idx].isprimary eq 1}
											<li class="selected-thumb"><a href="{$http_location}/{$style_cust_info[idx].image_l}"><img src="{$cdn_base}/{$style_cust_info[idx].image_t}" width="32" alt="bag {$style_cust_info[idx].custarea_name} customization"></a></li>
										{else}
											<li><a href="{$http_location}/{$style_cust_info[idx].image_l}"><img src="{$cdn_base}/{$style_cust_info[idx].image_t}" width="32" alt="bag {$style_cust_info[idx].custarea_name} customization"></a></li>
										{/if}
									{/section}
								</ul>
							</div>
						</div>
						<div align="right">
                                  <div class="details">
                                  <div class="item-price">
                                                                                  Rs {$default_style_price}/-
                                                                                  </div>
                                  <div class="shipping-details">
                                  {if ($default_style_price + $cart_amount) gt $free_shipping_amount}
                                      <b>FREE Shipping in India!</b>
                                  {/if}
                                  </div>
                                  </div>     						
						<img style="cursor:pointer;" src="{$cdn_base}/skin1/mkimages/bags/buy-btn.jpg" id="go_to_customize" alt="buy customized {$default_style_name},buy embroidered {$default_style_name}"></div>
						<div id="style_detail"><p>&nbsp;</p>{$default_style_detail}</div>						
					</div>
				</div>
				<!--items block-->
				<br/><br/>
			</div><!--wrapper-->
		</div><!--container-->
	</div><!--scroll wrapper-->
    {include file="site/footer.tpl"}
</body>
</html>
