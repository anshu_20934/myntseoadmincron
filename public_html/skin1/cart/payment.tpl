<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

{include file="common_meta.tpl" }

    <head>
    {literal}
        <style>
		input,img{border:0px;vertical-align:middle;}
			.process-steps{}
			.process-block{ width:375px; height:35px;border:0px solid red;}
			.process-block div{ float:left; overflow:hidden;}
			.stone p{text-align:right;width:65px; font-size:11px; font-weight:bold; color:#999999;}
			.sttwo p{color:#999999;font-size:11px;font-weight:bold;text-align:center;width:187px;}
			.stthree p{text-align:right;width:75px; font-size:11px; font-weight:bold; color:#333333;}
			
			.order-details-block{overflow:hidden;}
			.order-left-block h1{display:block;float:left;font-size:18px;padding:0;margin-right:10px;}
			.order-details-block .h1-title{font-size:14px;padding:0;margin-right:10px;}
			.order-left-block .header-link a{color:#006699;font-size:11px;}
			.order-left-block{width:320px; border:0px solid red;float:left;}
			.order-right-block{width:660px; border:0px solid red;float:left;padding-top: 1px}
			.adds-box{float:left}
			.addblock{overflow:hidden;position:relative;margin:5px 0 10px 0;background:none repeat scroll 0 0 #EEEEEE; border:1px solid #CCCCCC;float:left;*float:none; -moz-border-radius: 5px; -webkit-border-radius: 5px;}
			.addblock p{padding:10px 0 10px 20px;}
			.addblock p strong{ display:block; }
			.addblock p span{color:#000000;display:block;padding:2px 0;width:280px; }
			
			.cart-opt-block{margin:7px 0 0 0;background:#eeeeee; border:1px solid #a5a5a5;-moz-border-radius: 5px;-webkit-border-radius: 5px}
			.cart-opt-block .choies-pay-opt{margin-bottom:10px;display:block;font-size:13px; font-weight:bold;}
			.cart-opt-block p{padding:10px 0 0px 10px;}
			
			table.cart-final-items td.carttotal{text-align:right;padding-right:10px;}
			table.cart-final-items{width:470px; font-size:11px;}
			table.cart-final-items td{font-size:12px;border-bottom:1px dotted;margin:0 0 10px;padding:15px 0 10px;text-align:center;}
			table.cart-final-items td.prodec{padding-left:20px;font-weight:bold;text-align:left;}
			table.cart-final-items td.pricelist{padding-left:25px; line-height:20px; font-weight:normal;text-align:left;}
			table.cart-final-items td.pricelisttotal{padding-right:12px;line-height:20px; font-weight:normal;text-align:right;}
			table.cart-final-items td.pricelisttotal span{border:0px solid red;display:inline-block;width:auto!important;min-width:50px;width:50px; }
			table.cart-final-items td.carttotal span{border:0px solid red;display:inline-block;width:auto!important;min-width:35px;width:35px; }
			table.cart-final-items td.cartsize ul, table.cart-final-items td.cartqty ul{list-style:none outside none;margin:0;}
			table.price_details td{padding:0;margin:0;border:0px}
			table.cart-final-items th{
                background:url("http://myntra.myntassets.com/skin1/images/result-bg.gif") repeat-x scroll left top #CCCCCC;
                border-top:1px solid #E0E0E0;
                padding:8px;
            }
			table.cart-final-items tr.tableheader th.leftb{border-left:1px solid #E0E0E0;}
			table.cart-final-items tr.tableheader th.rightb{border-right:1px solid #E0E0E0;}
			
			.total-block{margin-top:-1px;overflow:hidden;border:1px solid #a5a5a5;}
			
			.cart-total-text{width:200px; border:0px solid red; float:left; }
			.cart-total-text p{text-align:center;padding:5px;color:#FFFFFF; background-color:#575757;font-size:18px; font-weight:normal; width:120px; }
			.cart-total-text1{width:170px; border:0px solid red; float:left; }
			.cart-total-text1 p{font-weight:bold;line-height:28px;padding-left:20px;}
			.cart-total-price {
              background: none repeat scroll 0 0 #FFFFFF;
              border: 1px solid #CCCCCC;
              font-weight: bold;
              padding: 10px;
              color: #333;
            -moz-border-radius:5px;
            -webkit-border-radius:5px;
            }
			.cart-total-price p {
              margin: 0;
              padding: 0;
            }
            .cart-total-price #total_charges{
                color: #CC0000;
                font-size: 18px;
                font-weight:bold;
            }
			
			.cartgifts{clear:both;}
			.cartsms input{color:#CC0000; font-weight:bold;width:134px}
			.cartgifts strong{color:#8e0800;}
			.cartgifts i{color:#333;font-size:11px}
			.cartgifts-select{font-size:12px;line-height:18px; display:none;}
			.cartgifts-select textarea{margin:5px 0;width:285px; height:80px;}
			#gifting-section{display:none;}
			.llabel{margin-right: 10px;text-align: left;width: 140px;font-weight:normal;}
			p .totallabel{margin:6px 10px 0px 0px;text-align: left;width: 110px; font-weight:bold;font-size:14px;}
			
			.cart-total-price div{font-weight:normal;}
			
			.cartnumber {
				background:url("http://myntra.myntassets.com/skin1/site-horizontal/sprite.png") no-repeat scroll -982px top transparent;
				border-color:#D3D3D3;
				border-left:0 solid #D3D3D3;
				border-style:solid;
				border-width:0 1px 1px 0;
				color:black;
				font-size:12px;
				height:25px;
				padding:3px 0 0 5px;
			}
            span.opt-cr-block {
              -moz-border-radius:5px;
              -webkit-border-radius:5px;
              border-radius: 5px;

              background-color: #FFFFFF;
              border: 1px solid #C9C7C7;
              color: #000000;
              display: block;
              font-weight: bold;
              margin-bottom: 8px;
              margin-left: 0;
              padding: 12px 5px;
              width: 328px;

            }

            span.opt-cr-block select {
              -moz-border-radius: 5px 5px 5px 5px;
              -webkit-border-radius:5px;
              border-radius:5px;
              -moz-box-shadow: 1px 1px 5px #999999;
              -webkit-box-shadow: 1px 1px 5px #999999;


              background: none repeat scroll 0 0 #FFFFFF;
              border: 1px solid #999999;
              margin-left: 5px;
              padding: 4px;
              width: 200px;
            }
            .cc-img-block{
              border: 1px solid #CCCCCC;
              display: block;
              margin-left: 30px;
              margin-top: 10px;
              overflow: hidden;
              width: 240px;
            }

			span.opt-cr-block input{margin:0 7px;}			
		    span.opt-cr-block .learnlink{color:blue;font-size:11px;padding-left:10px;}
		    
			span.opt-cr-block .opt-cr-cod-error{color:red;margin:0 7px;display:block;padding-left:23px}
			span.opt-cr-block .opt-cod-limit{color:black;margin:0 7px;display:block;padding-left:23px; font-weight:normal}

			
			.cart-links {
              display: block;
              margin: 15px 0 0;
              overflow: hidden;
            }
			.cart-links a, .cart-links a:visited{color:#006699;padding-bottom:15px; display:inline-block; text-decoration:underline}
			.cart-links a:hover{text-decoration:none;color:#333333;text-decoration:none}
			.cart-links img{padding-top:4px}

			
				.cart-button-img{
			background-image:url("http://myntra.myntassets.com/skin1/images/cart-button-bg.png");
			background-position:left top;
			background-repeat:no-repeat;
			height:40px;
            float:left;
		}
		
		.cart-button-img span{
			background-image:url("http://myntra.myntassets.com/skin1/images/cart-button-bg.png");
			background-position:right -80px;
			background-repeat:no-repeat;
			margin-left:2px;
			text-align:center;
			height:40px;
			display:block;
		}
		.cart-button-img span a, .cart-button-img span a:visited{
			background-image:url("http://myntra.myntassets.com/skin1/images/cart-button-bg.png");
			background-position:right -40px;
			background-repeat:repeat-x;
			color:#FFFFFF;
			display:block;
			font-family:Arial,Helvetica,sans-serif;
			font-size:18px;
			font-weight:bold;
			height:40px;
			text-decoration:none;
			
			padding: 0 10px;
			line-height:40px;
			margin-right:2px;
		}
		.cart-button-img span a:hover{
			text-decoration:none;
			color:#F3F000;
		}
		.access-block{border:0px solid;margin:0 auto;width:580px;_margin-left:200px;margin-bottom:80px;}
		.access-error{color:#be0709;font-size:18px;font-weight:bold;text-align:center; line-height:22px;margin-bottom:20px;}
		
		.boldtext{font-weight:bold;}
		
		.cod-block{
			float:right;
			padding-top:39px;
		}
		.cod-btn{
			background:none repeat scroll 0 0 #86A313;
			border:1px solid #708B04;
			color:white;
			display:block;
			width:185px;
		}
		.cod-btn a, .cod-btn a:visited{
			border-left:1px solid #A1BE2F;
			border-right:1px solid #93AF25;
			border-top:1px solid #A1BE2F;
			color:white;
			display:block;
			font-size:13px;
			font-weight:bold;
			padding:3px 12px;
			text-align:center;
			text-decoration:none;
			padding:5px;
			text-transform:uppercase;
		}
		.cod-btn a:hover{color:#000;}
		
		a.learnmore{
		    display:inline;
			font-size:11px;
			color:#6666CC;
			padding-top:4px;
		}
		.gtotal {
          float: right;
          margin: 40px 12px 10px 10px;
          overflow: hidden;
          width: 285px;
        
        }
        .gtotal span b{font-weight:normal;}
        .pblock{
            padding-left:15px;width: 235px;
        }
        .pblock div{line-height:22px}
        .pblock span b{
            display: inline-block;
            text-align: right;
            width: 60px;
        }

        a.cart-btn{
            display: inline-block;
            font-size: 14px;
            line-height: 35px;
            text-align: center;
            text-transform: uppercase;
            background-color:  #ffaa44;
            background: -webkit-gradient(linear,left bottom, left top, color-stop(0.22, rgb(255,144,9)), color-stop(0.81, rgb(255,170,68)));
            background: -moz-linear-gradient(center bottom, rgb(255,144,9) 22%, rgb(255,170,68) 81%);
        	filter: progid:DXImageTransform.Microsoft.gradient(startColorStr=#ffaa44, endColorStr=#ff9009);
        	-ms-filter: "progid:DXImageTransform.Microsoft.gradient(startColorstr=#ffaa44, endColorstr=#ff9009)";

            -moz-box-shadow:2px 2px 5px rgba(0, 0, 0, 0.2);
            -webkit-box-shadow:2px 2px 5px rgba(0, 0, 0, 0.2);
            box-shadow:2px 2px 5px rgba(0, 0, 0, 0.2);
        }

        a.cart-btn:hover{display: inline-block;font-size: 14px;line-height: 35px;text-align: center;text-transform: uppercase;
            background: -moz-linear-gradient(center bottom , #ffaa44 22%, #ff9009 81%);
            background: -webkit-gradient(linear,left bottom, left top, color-stop(0.22, #ffaa44), color-stop(0.81, #ff9009));
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=#ff9009, endColorstr=#ffaa44);
            -ms-filter:"progid:DXImageTransform.Microsoft.gradient(startColorstr=#ff9009, endColorstr=#ffaa44)";

            -moz-box-shadow:2px 2px 5px rgba(0, 0, 0, 0.2);
            -webkit-box-shadow:2px 2px 5px rgba(0, 0, 0, 0.2);
            box-shadow:2px 2px 5px rgba(0, 0, 0, 0.2);
        }
        .fbtn{margin-top:10px;padding:10px 34px;cursor:pointer; width:195px}

        .dis-block {
          background: none repeat scroll 0 0 #E5E5E5 !important;
          border: 1px solid #DDDDDD !important;
          color: #BBBBBB !important;
          cursor: not-allowed;
        }
        .dis-block input{
          cursor: not-allowed;

        }
        .grand-total, .lines-tb {
          border-bottom: 1px solid #CCCCCC;
          border-top: 1px solid #CCCCCC;
          margin: 10px 0 !important;
          padding: 5px 0 !important;
          text-align:right;
        }
        .int-shipping {
          -moz-border-radius: 0 0 5px 5px;
          -webkit-border-radius: 0 0 5px 5px;
          background: none repeat scroll 0 0 #FFFEEE;
          border-top: 2px dotted #CCCCCC;
          padding: 10px;

          width: 280px;
        }
        .int-shipping strong{display:block;}
        .int-shipping span {
          display: inline-block;
          padding-right: 8px;
          text-align: justify;
        }
        #gift_charges{
            display:inline-block;
            color:#8b0000;
            

        #gtotal{text-align:right;}

</style>
		
    {/literal}
    </head>
    <body id="body">


        <div id="scroll_wrapper">
            <div id="container">

            {include file="mykrititop.tpl" }

                <div id="wrapper" >
                {include file="breadcrumb.tpl"}

                    <div class="order-details-block">
						<div class="process-steps" align="center"> 
							<div class="process-block">
								<div class="step-one"><img src="{$cdn_base}/skin1/images/step-one.jpg" style="border:0px";></div>
								<div class="step-two"><img src="{$cdn_base}/skin1/images/step-two-v2.jpg" style="border:0px";></div>
								<div class="step-three"><img src="{$cdn_base}/skin1/images/step-three-green.jpg" style="border:0px";></div>
							
								<div class="pblock-text">
									<div class="stone"><p>Login</p></div>
									<div class="sttwo"><p>Select Shipping Address</p></div>
									<div class="stthree"><p>Place Order</p></div>
								</div>
							</div>
						</div>
							<a name="shipping-section"></a>
							{if $transaction_status}
							<div class="clear">&nbsp;</div>
							<div class="access-block" >
								<div class="access-error"> <img src="{$cdn_base}/skin1/images/no-access.jpg" style="border:0px";> YOUR TRANSACTION HAS FAILED</div>
								<div style="line-height:18px">
									<div>Dear Customer,</div>
								
								Your transaction has failed. Please relax and try to place the order again ensuring that: <br>
								1. Information passed on to payment gateway is accurate i.e. account details, billing address, password<br>
								2. Your internet connection is not disrupted during the payment process<br>
								
								<p style="padding:0px;margin:5 0px;width:790px;">
								In case your account has been debited, the same amount will be automatically rolled back within 24 hrs. <br>
Have a query ? 	{if $customerSupportCall}Call: {$customerSupportCall} {if $customerSupportTime} ({$customerSupportTime}) {/if} or {/if}Send email to <a href="mailto:support@myntra.com">support@myntra.com.</a>
								</p>								
									We regret the inconvenience.<br>Please retry placing the order below. 
								</div>
							</div>
							{/if}

							<div class="clear">&nbsp;</div>

							<div class="order-details-block">
								<div class="order-left-block">
								<div class="adds-box">
                                    <div><span class="h1-title">SHIPPING ADDRESS</span><span class="header-link"><a href="{$http_location}/mkcustomeraddress.php?change=true" onclick="_gaq.push(['_trackEvent', 'payment_page', 'change_shipping_address'])">(Change Shipping Address)</a></span></div>
                                    <div class="addblock">
                                        <p>
                                            <span><strong>{$address.name}</strong></span>
                                            <span>{$address.address}</span>
                                            <span> {$address.city}, {$address.statename}</span>
                                            <span> {$address.countryname}, {$address.pincode}</span>
                                            <br>
                                            <span><b>Mobile:</b> {$address.mobile}</span>

                                        </p>
                                         {if $address.countryname neq 'India'}
                                        <div class="int-shipping">
                                            <strong>Disclosures for International Shipping.</strong>
                                            <span>All shipments sent to countries outside India are international shipments and may be subject to import duties or fees levied by the country of import. All these duties and fees are the liability of the importer or recipient and Myntra is not liable to pay any such charge. Please contact the local customs office of the importing country for more information.</span>
                                         </div>   
                                         {/if}
                                    </div>
								</div>

                                <div class="clearfix">&nbsp;</div>
					            <form name="paymentform" id="paymentform" action="{$http_location}/mkorderinsert.php" method="POST">
								    <input name='s_option' id ='s_option' type="hidden" value ='{$shippingoptions[0].code}'/>

								    <!--delivery preference-->
            						<input type="hidden" name="shipment_preferences" id="shipment_preferences" value="shipped_together"/>
            						<!--delivery preference-->

									<!-- Gift -->
									<div class="cartgifts"><input type="checkbox" name="asgift" id="asgift" value="Y"> Send as a <strong>GIFT</strong> for just Rs 20/- {*<img src="{$cdn_base}/skin1/images/gif-box.png" style="border:0px; padding-left:5px;vertical-align:middle;">*}  <br>  ( <i>All items are gift wrapped and sent without a bill/invoice.</i>)</div>

                                
									
									<div class="cartgifts-select"> Enter your message here<br><textarea rows="3" name="gmessage"></textarea>
									                                    <div class="cartsms">SMS Updates will be sent to <input type="text" name="issues_contact_number"  id="cartsmsmobileno" class="cartnumber"  value='{$address.mobile}'><span style="color:red; display:none;" id="cartmobileerr">Please enter valid mobile number.</label></span></div>


									</div>

									<!-- @Gift -->

								

                                    {*{include file="cart/details.tpl"}*}


</div>
								<div class="order-right-block">



								<div>PAYMENT OPTIONS</div>
							<input type="hidden" name="is_shipping_cost_set" id="is_shipping_cost_set" {if $s_city != null}value="1" {else}value="0"{/if}>
							<input type="hidden" name="checksumkey" id="checksumkey" value="{$checksumkey}">
							<input type="hidden" name="address" value="{$address.id}">
							{include file="codlearnmore.tpl" }
									<div class="cart-opt-block" >
                                    {*Grand Total*}
								<div class="gtotal">
									 <div class="cart-total-price clearfix">
									 <div class="pblock">
									 	{assign var=costMinusShippingRate value=$totalAmount-$shippingRate}
									 	{if $shippingRate neq 0}
										<div class="item-cost" style="white-space:normal;">
										    <span class="left llabel">Item Cost</span><span>Rs <b>{$costMinusShippingRate|string_format:"%.2f"}</b></span>
										</div>

										<div class="shipping-cost">
										    <span class="left llabel">Shipping Charges </span>
										    <span class="rs-block">Rs <b>{$shippingRate|string_format:"%.2f"}</b></span>
										</div>
									 	{/if}
									 	<div id="gifting-section">
									 		{if $shippingRate eq 0}
									 			<div class="item-cost">
									 			    <span class="left llabel">Item Cost </span>
									 			    <span class="rs-block">Rs <b>{$totalAmount|string_format:"%.2f"}</b></span>
									 			</div>
									 		{/if}
									 		<div class="cartgifts-select">
									 		    <span class="left llabel">Gifting Charges </span> <span class="rs-block">Rs <b id="gift_charges"></b></span></div>
									 	</div>

									 	<p {if $shippingRate neq 0}class="grand-total"{/if} id="gtotal" style="text-align:right"><span class="left totallabel">Grand Total </span> Rs <span id="total_charges">{$totalAmount+$giftamount|string_format:"%.2f"}</span></p>
									 </div>

                                    <div>
                                    <a class="orange-bg corners cart-btn fbtn"   id="submitButton" onClick="var value = $('input[@name=pm]:checked').val();  _gaq.push(['_trackEvent', 'payment_page', 'proceed_to_pay', value]); return false;">
                                        <b>Proceed to Payment</b> <img src="{$cdn_base}/skin1/images/cart-checkout-arrow.png" alt="checkout" style="padding-left:10px;_position:absolute;"></a>
                                       
                                    </div>

                                    <div class="cart-links">
                                        <a href="{$http_location}/mkmycart.php?at=c&pagetype=productdetail" onClick="_gaq.push(['_trackEvent', 'payment_page', 'back_to_cart']); return true;" style="padding-right: 30px;"> <img src="{$cdn_base}/skin1/images/back-arrow.jpg" alt="" style="float:left;">Back to Cart</a>
                                        <a href="{$http_location}/" onClick="_gaq.push(['_trackEvent', 'payment_page', 'continue_shopping']); return true;">Continue Shopping <img src="{$cdn_base}/skin1/images/more-cart.jpg" alt=""></a>
                                    </div>
                                    
                                    </div>

                                </div>
										<p>
											{if $onlinepaymentevoucherflag}
												<span class="choies-pay-opt choices-highlight">Make your payment <span style="color:#EA431E;">online</span> and receive a Rs1000 voucher right away!</span>
											{else}
												<span class="choies-pay-opt">Choose any one of the following payment options</span>
											{/if}											
											
                                            <span class="opt-cr-block">
                                            <input type="radio" name="pm" value="creditcards" checked="checked" id="ccards"><label for="ccards"> CREDIT CARD </label>
                                                <select name="creditcardtype" onclick="javascript:document.getElementById('ccards').checked=true;" id="creditCards">
                                                    <option value="" selected="selected">Select Credit Card</option>
                                                    <option value="visa">Visa</option>
                                                    <option value="masterCard">MasterCard</option>
                                                    <option value="dinersClub">Diners Club Card</option>
                                                    <option value="americanExpress">American Express</option>
                                                    <option value="JCBCard">JCB card</option>
                                                </select>
                                                <span class="cc-img-block"> <img src="{$cdn_base}/skin1/images/credit-cards.jpg"> <img src="{$cdn_base}/skin1/images/credit-cards-v2.jpg"></span>


                                           </span>
											<span class="opt-cr-block"><input type="radio" name="pm" id="nbanking" value="netbanking"><label for="nbanking">NET BANKING</label>
											<select name="netBankingCards" style="margin-left:8px;" onclick="javascript:document.getElementById('nbanking').checked=true;" id="netBanking">
                                               <option value="" selected="selected">Select Bank</option>                                        
											   <option value='AND_N'>Andhra Bank</option>
                                               <option value='UTI_N'>AXIS Bank</option>
											   <option value="BBK_N">Bank of Bahrain & Kuwait</option>
                                               <option value='BOBCO_N'>Bank of Baroda Corporate Accounts</option>
                                               <option value='BOB_N'>Bank of Baroda Retail Accounts</option>
                                               <option value='BOI_N'>Bank of India</option>
                                               <option value='BOM_N'>Bank of Maharashtra</option>
                                               <option value='BOR_N'>Bank of Rajasthan</option>
                                               <option value='CBIBAN_N'>Citibank Bank Account Online</option>
                                               <option value='CITIUB_N'>City Union Bank</option>
                                               <option value='COP_N'>Corporation Bank</option>
                                               <option value='DEUNB_N'>Deutsche Bank</option>
                                               <option value='FDEB_N'>Federal Bank</option>
                                               <option value='HDEB_N'>HDFC Bank</option>
                                               <option value='IDEB_N'>ICICI Bank</option>
                                               <option value='IDBI_N'>IDBI Bank</option>
											   <option value="IOB_N">Indian Overseas Bank</option>
                                               <option value='NIIB_N'>IndusInd Bank</option>
                                               <option value='ING_N' >ING Vysya Bank</option>
                                               <option value='JKB_N'>Jammu & Kashmir Bank</option>
                                               <option value='KTKB_N'>Karnataka Bank</option>
                                               <option value='KVB_N'>Karur Vysya Bank</option>
                                               <option value='NKMB_N'>Kotak Mahindra Bank</option>
                                               <option value='LVB_N'>Lakshmi Vilas Bank NetBanking</option>
                                               <option value='OBC_N'>Oriental Bank of Commerce</option>
                                               <option value='PNBCO_N'>Punjab National Bank Corporate Accounts</option>
											   <option value='NPNB_N'>Punjab National Bank Retail Accounts</option>
                                               <option value='SIB_N'>South Indian Bank</option>                                               
                                               <option value='SCB_N' >Standard Chartered Bank</option>
                                               <option value='SBI_N'>State Bank of India</option>										
											   <option value='SBH_N'>State Bank of Hyderabad</option>											   
											   <option value='SBM_N'>State Bank of Mysore</option>											   
                                               <option value='SBT_N'>State Bank of Travancore</option>
                                               <option value='SYNBK_N'>Syndicate Bank</option>
											   <option value="TNMB_N">Tamilnad Mercantile Bank</option>
                                               <option value='UNI_N'>Union Bank of India</option>
                                               <option value='VJYA_N'>Vijaya Bank</option>
                                               <option value='YES_N'>YES Bank</option>
								            </select>
											</span>
											<span class="opt-cr-block"><input type="radio" name="pm" value="debitcard" id="debitcard"><label for="debitcard">DEBIT CARD</label></span>

											{if $onlinepaymentevoucherflag}</p></div><div class="cart-opt-block" >
											<p>
											<span class="choies-pay-opt">Other payment options</span>{/if}





											 {*<span class="opt-cr-block"><input type="radio" name="pm" value="otherpayments" id="op"><label for="op">OTHER PAYMENTS</label>
											 <select name="otherpaymentstype" onclick="javascript:document.getElementById('op').checked=true;" id="otherpayments">
											    <option value="" selected="selected">Select Payment type</option>
											   {if $totalAmount > $check_option_enable}
											        <option value="checkdd"> CHECK/DEMAND  DRAFT </option>
											    {/if}
											    <option value="cashcard"> CASH CARD </option>
											    <option value="mobilepayment"> MOBILE PAYMENT </option>
											 </select>

											</span>*}
											<span class="opt-cr-block"><input type="radio" name="pm" value="cashcard" id="cashcard"><label for="cashcard">CASH CARD</label></span>

                                            <span class="opt-cr-block {if ($codErrorCode!=0)}dis-block{/if}">

                                                <input type="radio" name="pm" value="cod" id="cod" {if ($codErrorCode>0)}disabled="disabled"{/if}>


                                                <label for="cod"> CASH ON DELIVERY </label><img style="width:40px; padding-left:5px" src="{$cdn_base}/skin1/images/pay-cod-cash.jpg"><br/>
                                                {if ($codErrorCode==6)}
                                                    <span class="opt-cod-limit">
                                                        Available COD Credit: Rs. {$codCreditBalance}
                                                    </span>
                                                {/if}
                                                {if ($codErrorCode>0)}
                                                    <span class="opt-cr-cod-error">{$codErrorMessage}</span>

                                                {/if}

                                                {if ($codErrorCode>3)}
                                                    <a style="padding-left:30px" href="JavaScript:showLearnMore()" class="learnmore" >Why?</a>
                                                {else}
                                                    <a style="padding-left:30px" href="JavaScript:showLearnMore()" class="learnmore" >Learn More</a>
                                                {/if}
                                            </span>
											

										</p>

									</div>




								</div>
								</form>
							</div>
							
					</div>

                        <div class="clearall" style="_height:50px">&nbsp;</div>
						
                    </div>
                {include file="footer.tpl" }
                <script type="text/javascript">
                var total_amount = {$totalAmount};
                </script>
                 {literal}
                  <script type="text/javascript">
                    $(document).ready(function(){
                        $('#asgift').click(function(){
                            var final_amount = total_amount;
                            if ($("#asgift").is(":checked")) {
                                $("#gtotal").addClass("lines-tb");
                                $("#gift_charges").text("20.00");
                                var final_amount = final_amount + 20;
                                $("#total_charges").text(final_amount.toFixed(2));
                                $(".cartgifts-select").show();
                                $("#gifting-section").show();
				$("#gift_wrap_row td").css("font-weight","bold");	
                            } else {
                                $("#gtotal").removeClass("lines-tb");
                                $("#gift_charges").text("0.00");
                                $("#total_charges").text(final_amount.toFixed(2));
                                $(".cartgifts-select").hide();
                                $("#gifting-section").hide();
				$("#gift_wrap_row td").css("font-weight","normal");	
                            }
                            $.get(http_loc + "/mkupdatechecksum.php?amt=" + final_amount + "&chksumkey=" + $("#checksumkey").val());
                        });
			$('#cartsmsmobilecheck').click(function() {
				if ($("#cartsmsmobilecheck").is(":checked")) {
					$("#cartsmsmobileno").attr('readonly',"");
		
				} else {
					
					$("#cartsmsmobileno").attr('readonly',"true");
				}
				
				});
                    });

		
                    $('#submitButton').click(function(){
			if ( !(!isNaN($("#cartsmsmobileno").val()) && ($("#cartsmsmobileno").val()).length > 9)) {
				$("#cartmobileerr").css("display","block");
				$("#cartsmsmobileno").focus();
				return false;				
			} else {
				$("#cartmobileerr").css("display","none");
			}
                        if($('#checkdd').is(':checked') ){
                            if(confirm('It takes 3-4 days for us to receive the check. This will add minimum 5 days to the delivery time. Are you sure that you want to make the payment by check? Click on OK to place the order by check. Click on Cancel to change the payment method to credit card or net banking')) {
                                $('#paymentform').submit();
                            } else {
                                return false;
                            }
                        } else {

                            // Other payments select box validation
                           if($("#op").attr("checked")){
                                if($("#otherpayments").val() == ''){
                                    alert("Please select a valid mode of payment.");
                                    return false;
                                }
                           }
                            // CREDIT CARD select box validation 
                           if($("#ccards").attr("checked")){
                                if($("#creditCards").val() == ''){
                                    alert("Please select a valid Credit Card type");
                                    return false;
                                }
                           }
                           
                            // NET BANKING select box validation
                           if($("#nbanking").attr("checked")){
                                if($("#netBanking").val() == ''){
                                    alert("Please select a valid Netbanking option");
                                    return false;
                                }
                           }

                            $('#paymentform').submit();
                        }
                    }); 

                    if($("input[name='pm']:checked").val()=='cod' || $("input[name='pm']:checked").val()=='checkdd') {
                    	$("#submitButton b").html("Place the Order");
                    }
                   	$("input[name='pm']").click(function(){
                       	var option = $("input[name='pm']:checked").val();
                       	if(option==='cod' || option==='checkdd') {
                       		$("#submitButton b").html("Place the Order");
                       	} else {
                       		$("#submitButton b").html("Proceed to Payment");
                       	}
                   		   // your code here 
              		});

                    		 
                    	 
                   </script>
                 {/literal}
                </div>
            </div>
