<form action="{$https_location}/mkorderinsert.php" id="debit_card" method="post" target="MyntraPayment"
	{if $totalAmount eq 0} 
		style="display:none" 
	{elseif $defaultPaymentType eq "debit_card"} 
		style="display: block;"
	{else}
		style="display:none"	 
	{/if}
>
	<input type="hidden" name='s_option' value ='{$logisticId}'/>
	<!--delivery preference-->
	<input type="hidden" name="shipment_preferences" value="shipped_together" >
	<!--delivery preference-->								
	<input type="hidden" name="is_shipping_cost_set" value="{$s_city}">
	<input type="hidden" name="checksumkey" value="{$checksumkey}">
	<input type="hidden" name="address" value="{$address.id}">					
	<input type="hidden" value="debitcard" name="pm" />
	{if $is_icici_gateway_up}
	{if $creditDebitDownBankListStr}
		<div class="warning-text-border clearfix">
				<div class="warning-image left"></div>
				<div class="left" style="width:470px">{$creditDebitDownBankListStr}</div>
		</div>
	{/if}
	<table cellpadding="1" cellspacing="1" width="85%" class="my-cc-table" border="1">
		<tr>
			<td style="width:155px;"><label>We accept :</label></td>
			<td>
				<ul class="acceptedCards clearfix">
					<li class="cardType visa">Visa</li>
					<li class="cardType master">MasterCard</li>
					<li class="cardType maestro">Maestro</li>
				</ul>
			</td>
		</tr>
		<tr>
			<td><label class="required">Card number:<span class="cstar">*</span></label></td>
			<td><input type="text" size="19" maxlength="19" name="card_number" autocomplete="off" onkeyup="showUserCardType(document.getElementById('debit_card').card_number.value);" onfocus="showUserCardType(document.getElementById('debit_card').card_number.value);" onchange="showUserCardType(document.getElementById('debit_card').card_number.value);">
			<br><span class="weak">Your card number without spaces</span></td>
		</tr>
		<tr class="maestro-msg" style="display:none;">
			<td></td>
			<td>Valid Thru &amp; CVV are optional if not present on card.</td>
		</tr>	
		<tr>
			<td><label class="required">Valid Thru:<span class="ostar">*</span></label></td>
			<td>
				<select name="card_month">
					<option value="">Month</option>
					<option value="01"> 1 </option>
					<option value="02"> 2 </option>
					<option value="03"> 3 </option>
					<option value="04"> 4 </option>
					<option value="05"> 5 </option>
					<option value="06"> 6 </option>
					<option value="07"> 7 </option>
					<option value="08"> 8 </option>
					<option value="09"> 9 </option>
					<option value="10"> 10 </option>
					<option value="11"> 11 </option>
					<option value="12"> 12 </option>
				</select>
				<select name="card_year">
					<option value="">Year</option>
					<option value="12">2012</option>
					<option value="13">2013</option>
					<option value="14">2014</option>
					<option value="15">2015</option>
					<option value="16">2016</option>
					<option value="17">2017</option>
					<option value="18">2018</option>
					<option value="19">2019</option>
					<option value="20">2020</option>
					<option value="21">2021</option>
					<option value="22">2022</option>
					<option value="23">2023</option>
					<option value="24">2024</option>
					<option value="25">2025</option>
					<option value="26">2026</option>
					<option value="27">2027</option>
					<option value="28">2028</option>
					<option value="29">2029</option>
					<option value="30">2030</option>
					<option value="31">2031</option>
					<option value="32">2032</option>
					<option value="33">2033</option>
					<option value="34">2034</option>
					<option value="35">2035</option>
					<option value="36">2036</option>
					<option value="37">2037</option>
					<option value="38">2038</option>
					<option value="39">2039</option>
					<option value="40">2040</option>
				</select>
			</td>
		</tr>
		<tr>
			<td><label class="required">Card security code (CVV):<span class="ostar">*</span></label></td>
			<td><input type="password"  maxlength="4" size="4" name="cvv_code" autocomplete="off"></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td><img src="{$secure_cdn_base}/skin1/images/cvv_help.gif" alt="Card Verification Value Code Help"></td>
		</tr>
		<tr>
			<td><label class="required">Name on debit card<span class="cstar">*</span></label></td>
			<td><input type="text" name="bill_name"></td>
		</tr>
	</table>
	{else}
		<span>Please click on 'Proceed to Payment' to proceed with your order.<br/><br/>We will redirect you to our payment gateway partner to complete the payment process.</span>
	{/if}
</form>
