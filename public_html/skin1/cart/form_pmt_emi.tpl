<form action="{$https_location}/mkorderinsert.php" method="post" id="emi" target="MyntraPayment"
	{if $totalAmount eq 0} 
		style="display:none" 
	{elseif $defaultPaymentType eq "emi"} 
		style="display: block;"
	{else}
		style="display:none"	 
	{/if}
>
	<input type="hidden" name='s_option' value ='{$logisticId}'/>
	<!--delivery preference-->
	<input type="hidden" name="shipment_preferences" value="shipped_together" >
	<!--delivery preference-->								
	<input type="hidden" name="is_shipping_cost_set" value="{$s_city}">
	<input type="hidden" name="checksumkey" value="{$checksumkey}">
	<input type="hidden" name="address" value="{$address.id}">					
	<input type="hidden" value="emi" name="pm" />
	{if $totalAmount >= $icici3MONTHMinEMITotalAmount || $totalAmount >= $icici6MONTHMinEMITotalAmount}
		{if $is_icici_emi_gateway_up}
			{if $creditDebitDownBankListStr}
				<div class="warning-text-border clearfix">
						<div class="warning-image left"></div>
						<div class="left" style="width:470px">{$creditDebitDownBankListStr}</div>
				</div>
			{/if}
		    <div class="emi-options">
			   	{if $emiBanks|@count eq 1}
			       	<div class="emi-bank-msg">
			       		Currently, only <span class="red">{$emiBanksText}</span> credit cards are eligible for EMI payments.<br />
			       		We'll be adding more banks soon!
			       	</div>
				{else}        	
			        <div class="row emi-bank">
			            <label>Which Bank is your Credit Card from? *</label><br>
			            <select id="emi-bank" name="emi-bank">
				            {foreach from=$emiBanks item=bank} 
								<option value="{$bank}">{$bank|upper}</option> 
							{/foreach}                    
				        </select>
				    </div>
			    {/if}
		
		        <h3>EMI Options</h3>
		        <div class="emi-duration">
		        	<p>Select an EMI option that is most convenient for you</p>
		        	<table border="0" cellspacing="0">
		       			<colgroup>
		        			<col width="30" />
		        			<col width="100" />
		        			<col width="100" />
		        			<col width="120" />
		        			<col width="100" />
		       			</colgroup>
		       			<tbody>
		        			<tr>
		        				<th></th>
		        				<th>Duration</th>
		        				<th>Installment</th>
		        				<th>Processing Fee</th>
		        				<th>Total</th>
		        			</tr>
		        			<tr class="odd">
		        				<td><input type="radio" name="emi_bank" value="icici3emi" checked="checked"></td>
		        				<td>3 Months</td>
		        				<td>{$rupeesymbol} {$iciciPerMonthEMIFor3Month|round|number_format:0:".":","}</td>
		        				<td class="fee">{if $icici3MonthEMICharge gt 0}{$rupeesymbol} {$icici3MonthEMICharge|round|number_format:0:".":","}{else}FREE{/if}</td>
		        				<td class="total">{$rupeesymbol} {$iciciTotal3MonthEMI|round|number_format:0:".":","}</td>
		        			</tr>
		        			<tr class="even">
		        				<td><input type="radio" name="emi_bank" value="icici6emi"></td>
		        				<td>6 Months</td>
		        				<td>{$rupeesymbol} {$iciciPerMonthEMIFor6Month|round|number_format:0:".":","}</td>
		        				<td class="fee">{if $icici6MonthEMICharge gt 0}{$rupeesymbol} {$icici6MonthEMICharge|round|number_format:0:".":","}{else}FREE{/if}</td>
		        				<td class="total">{$rupeesymbol} {$iciciTotal6MonthEMI|round|number_format:0:".":","}</td>
		        			</tr>
		       			</tbody>
		       		</table>
		       	</div>
			</div>        
		    
		    <h3>Card Details</h3>
		
			<table cellpadding="1" cellspacing="1" width="95%" class="my-cc-table" border="0">
				<tr>
					<td style="width:155px;"><label>We accept :</label></td>
					<td>
						<ul class="acceptedCards clearfix">
							<li class="cardType visa">Visa</li>
							<li class="cardType master">Mastercard</li>												
						</ul>
					</td>
				</tr>
				<tr>
				</tr>
				<tr>
					<td><label for="creditCardNumber" class="required">Card number:<span class="cstar">*</span></label></td>
					<td><input type="text" size="19" maxlength="19" name="card_number" autocomplete="off" onkeyup="showUserCardType(document.getElementById('credit_card').card_number.value);" onfocus="showUserCardType(document.getElementById('credit_card').card_number.value);" onchange="showUserCardType(document.getElementById('credit_card').card_number.value);">
					<br><span class="weak">Your card number without spaces</span></td>
				</tr>
				<tr>
					<td><label class="required">Valid Thru:<span class="cstar">*</span></label></td>
					<td>
						<select name="card_month">
							<option value="">Month</option>
							<option value="01"> 1 </option>
							<option value="02"> 2 </option>
							<option value="03"> 3 </option>
							<option value="04"> 4 </option>
							<option value="05"> 5 </option>
							<option value="06"> 6 </option>
							<option value="07"> 7 </option>
							<option value="08"> 8 </option>
							<option value="09"> 9 </option>
							<option value="10"> 10 </option>
							<option value="11"> 11 </option>
							<option value="12"> 12 </option>
						</select>
						<select name="card_year">
							<option value="">Year</option>
							<option value="12">2012</option>
							<option value="13">2013</option>
							<option value="14">2014</option>
							<option value="15">2015</option>
							<option value="16">2016</option>
							<option value="17">2017</option>
							<option value="18">2018</option>
							<option value="19">2019</option>
							<option value="20">2020</option>
							<option value="21">2021</option>
							<option value="22">2022</option>
							<option value="23">2023</option>
							<option value="24">2024</option>
							<option value="25">2025</option>
							<option value="26">2026</option>
							<option value="27">2027</option>
							<option value="28">2028</option>
							<option value="29">2029</option>
							<option value="30">2030</option>
							<option value="31">2031</option>
							<option value="32">2032</option>
							<option value="33">2033</option>
							<option value="34">2034</option>
							<option value="35">2035</option>
							<option value="36">2036</option>
							<option value="37">2037</option>
							<option value="38">2038</option>
							<option value="39">2039</option>
							<option value="40">2040</option>
						</select>
					</td>
				</tr>
				<tr>
					<td><label class="required">Card security code (CVV):<span class="cstar">*</span></label></td>
					<td><input type="password" cardtypefieldname="card_type" class="" title="Card Verification Value Code" maxlength="4" size="4" value="" name="cvv_code" autocomplete="off"></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td><img src="{$secure_cdn_base}/skin1/images/cvv_help.gif" alt="Card Verification Value Code Help"></td>
				</tr>
				<tr>
					<td><label class="required">Name on credit card<span class="cstar">*</span></label></td>
					<td><input type="text" title="Name on credit card" class="required" value="" name="bill_name"></td>
				</tr>
			</table>
			<table cellpadding="0" cellspacing="0" width="100%" class="add-copy-block">
				<tbody>
					<tr>
						<td colspan="2">
							<h3>Credit card billing address</h3>
							<p style="padding-bottom:5px;">Your billing address is used to prevent fraudulent use of your card. Enter the address exactly as it appears on your card statement.</p>
							<p style="text-align:right;margin-right:30px;"><input type="button" value="Copy my shipping address" onclick="copycontactaddress(true);" href="javascript:void(0)"></p>
						</td>
					</tr>
					<tr>
						<td width="165"><label>Billing Name<span class="cstar">*</span></label></td>
						<td><input type="text" name="b_firstname" id="cc_b_firstname" value="Name" class="inputText boxw blurred toggle-text"> 
						<!-- input type="text" name="b_lastname" value="Last Name" class="blurred toggle-text" -->
						</td>
					</tr>
					<tr>
						<td><label>Billing Address<span class="cstar">*</span></label></td>
						<td>
							<textarea name="b_address" id="cc_b_address" class="blurred toggle-text">Enter your billing address here</textarea><br>
							<input type="text" name="b_city" id="cc_b_city" value="City" class="inputText boxw blurred toggle-text"> <br>
							<input type="text" name="b_state" id="cc_b_state" value="State" class="blurred toggle-text" style="display:block;"> 
							<input type="text" name="b_zipcode" id="cc_b_zipcode" value="PinCode" class="blurred toggle-text" style="display:block;">	
						</td>
					</tr>	
					<tr>
						<td><label style="padding-top: 8px;">Billing Country<span class="cstar">*</span></label></td>
						<td>												
							<select id="cc_b_country" name="b_country">
								{foreach key=countrycode item=countryname from=$countries}
			   							<option value="{$countrycode}" {if $countrycode eq 'IN'}selected="true"{/if}>{$countryname}</option>
								{/foreach}
							</select>
						</td>
					</tr>
				</tbody>
			</table>
		{else}
			<span>
				{if !$is_icici_emi_gateway_up}
	            ICICI Payment Gateway is currently facing technical difficulties.
	            <br/><br/>
	            Please try after some time.
	            {/if}
	        </span>
		{/if}
	{else}
		Credit card EMI options are available only for orders above value of Rs. {$iciciMinEMITotalAmount}.
		<br/>Please select another payment option. 
	{/if}
</form>
