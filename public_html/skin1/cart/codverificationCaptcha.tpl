<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
	<head>
	{include file="site/secure_header.tpl" }
	{literal}
		<style>
			input,img{border:0px;vertical-align:middle;}
			.process-steps{border:0px solid red;padding:10px;}
			.process-block{ width:375px; height:35px;border:0px solid red;}
			.process-block div{ float:left; overflow:hidden;}
			.stone p{text-align:right;width:65px; font-size:11px; font-weight:bold; color:#999999;}
			.sttwo p{color:#999999;font-size:11px;font-weight:bold;text-align:center;width:187px;}
			.stthree p{text-align:right;width:77px; font-size:11px; font-weight:bold; color:#333333;}
			.pblock-text{width:450px;}
			.order-details-block{overflow:hidden;}
			.order-left-block h1{display:block;float:left;font-size:18px;padding:0;margin-right:10px;}
			.order-details-block h1{display:block;font-size:18px;padding:0;margin-right:10px;}
			.order-left-block .header-link{float:left;line-height:22px;}
			.order-left-block .header-link a{color:#394b95;font-weight:bold;}
			.order-left-block{width:580px; border:0px solid red;}
			.order-right-block{width:310px; border:0px solid red;float:right;}
			
			.cart-opt-block{margin:5px 0 0 0;background:url("{/literal}{$secure_cdn_base}{literal}/skin1/images/left-block-bg.gif") repeat-x top left scroll; border:1px solid #a5a5a5;}
			.cart-opt-block .choies-pay-opt{margin:10px 0;display:block;font-size:13px; font-weight:bold;}
			.cart-opt-block p{border:1px solid #FFF; padding:10px 0 10px 20px;}
			
			table.cart-final-items td.carttotal{text-align:right;padding-right:10px;}
			table.cart-final-items{width:470px; font-size:11px;}
			table.cart-final-items td{font-size:12px;border-bottom:1px dotted;margin:0 0 10px;padding:15px 0 10px;text-align:center;}
			table.cart-final-items td.prodec{padding-left:20px;font-weight:bold;text-align:left;}
			table.cart-final-items td.pricelist{padding-left:25px; line-height:20px; font-weight:normal;text-align:left;}
			table.cart-final-items td.pricelisttotal{padding-right:12px;line-height:20px; font-weight:normal;text-align:right;}
			table.cart-final-items td.pricelisttotal span{border:0px solid red;display:inline-block;width:auto!important;min-width:50px;width:50px; }
			table.cart-final-items td.carttotal span{border:0px solid red;display:inline-block;width:auto!important;min-width:35px;width:35px; }
			table.cart-final-items td.cartsize ul, table.cart-final-items td.cartqty ul{list-style:none outside none;margin:0;}
			table.price_details td{padding:0;margin:0;border:0px}
			table.cart-final-items th{
				background:url("{/literal}{$secure_cdn_base}{literal}/skin1/images/result-bg.gif") repeat-x scroll left top #CCCCCC;
				border-top:1px solid #E0E0E0;
				padding:8px;
			}
			table.cart-final-items tr.tableheader th.leftb{border-left:1px solid #E0E0E0;}
			table.cart-final-items tr.tableheader th.rightb{border-right:1px solid #E0E0E0;}
			
			.total-block{margin-top:-1px;overflow:hidden;border:1px solid #a5a5a5;}
			
			.cart-total-text{width:177px; border:0px solid red; float:left; }
			.cart-total-text p{text-align:center;padding:5px;color:#FFFFFF; background-color:#575757;font-size:18px; font-weight:normal;margin:0px;}
			.cart-total-text1{width:170px; border:0px solid red; float:left; }
			.cart-total-text1 p{font-weight:bold;line-height:28px;padding-left:20px;margin:0px;}
			.cart-total-price{width:90px; border:0px solid red; float:right; }
			.cart-total-price p{line-height:20px;padding:5px 10px 5px 0px;text-align:right;font-weight:bold;margin:0px;}
			
			.cartgifts{height:40px;background-color:#F2F2F2;line-height:14px;margin:0px;padding:5px 25px; }
			.cartsms{line-height:34px;padding:5px 25px; }
			.cartgifts strong{color:#8e0800;}
			.cartgifts-select{font-size:12px;background-color:#F2F2F2;line-height:18px;padding:0px 25px 10px 25px; display:none;}
			.cartgifts-select textarea{margin:5px 0;width:215px;}
			
			.cartnumber {
				background:url("{/literal}{$secure_cdn_base}{literal}/skin1/site-horizontal/sprite.png") no-repeat scroll -982px top transparent;
				border-color:#D3D3D3;
				border-left:0 solid #D3D3D3;
				border-style:solid;
				border-width:0 1px 1px 0;
				color:#666666;
				font-size:12px;
				height:25px;
				padding:3px 0 0 5px;
			}
			span.opt-cr-block{
				margin-bottom:8px;
				margin-left:0px;
				background-color:#FFF;
				color:#666666;
				display:block;
				font-weight:bold;
				padding:12px 5px;
				width:255px;
				border:1px solid #c9c7c7;
			}
			span.opt-cr-block input{margin:0 7px;}
			
			.cart-links{margin:20px 0 0 0; text-align:center;}
			.cart-links a, .cart-links a:visited{color:#1951a6;font-weight:bold; padding-right:15px;}
			.cart-links a:hover{text-decoration:none;color:#333333;}
			
			.cart-button-img{
				background-image:url("{/literal}{$secure_cdn_base}{literal}/skin1/images/cart-button-bg.png");
				background-position:left top;
				background-repeat:no-repeat;
				height:40px;
	            float:left;
			}
		
			.cart-button-img span{
				background-image:url("{/literal}{$secure_cdn_base}{literal}/skin1/images/cart-button-bg.png");
				background-position:right -80px;
				background-repeat:no-repeat;
				margin-left:2px;
				text-align:center;
				height:40px;
				display:block;
			}
			.cart-button-img span a, .cart-button-img span a:visited{
				background-image:url("{/literal}{$secure_cdn_base}{literal}/skin1/images/cart-button-bg.png");
				background-position:right -40px;
				background-repeat:repeat-x;
				color:#FFFFFF;
				display:block;
				font-family:Arial,Helvetica,sans-serif;
				font-size:18px;
				font-weight:bold;
				height:40px;
				text-decoration:none;
				
				padding: 0 10px;
				line-height:40px;
				margin-right:2px;
			}
			
			.cart-button-img span a:hover{
				text-decoration:none;
				color:#F3F000;
			}
			
			.access-block{border:0px solid;margin:0 auto;width:580px;_margin-left:200px;margin-bottom:80px;}
			.access-error{color:#be0709;font-size:18px;font-weight:bold;text-align:center; line-height:22px;margin-bottom:20px;}
			
			.boldtext{font-weight:bold;}
			#change-image{margin-top:25px;}		
			#captcha-form{font-size: 16px;height: 30px;line-height: 30px;width: 150px;float:left;margin-right:10px;}
			.verify-captcha{padding: 5px 15px;margin-top:10px;background:#FD9B06;}
			.captcha-block{margin-top:40px;}
			.captcha-loading{background: url("{/literal}{$secure_cdn_base}{literal}/skin1/myntra_images/captcha-loading.gif") no-repeat;width:16px;height:16px;margin-left:5px;margin-top:15px;}
			.captcha-entry{clear:both;}
			.captcha-entry span {display:block;}
			.cod-page-notifications{text-align:center;}
			.captcha-message{padding:2px;width:145px;}
			.verify-captcha.btn-disabled{background:#a8a8a8; cursor:not-allowed;}
			
			.order-totals{width:334px;border:1px solid #ccc;padding-bottom:5px; padding-left:5px; padding-right:5px}
		</style>
	{/literal}
	</head>
	
	<body>
	{if $cashback_gateway_status eq 'on'}
	{include file="cashback_learnmore.tpl"}
	{/if}
		<div class="container clearfix">
		    {include file="site/secure_menu.tpl"}
		    
				<div class="order-details">
					<div class="span-33 cod-page-notifications error hide"></div>
					<div class="span-33 clearfix" style="background:#fff;">
						<div class="order-details-block left span-20">
							<div class="order-left-block">
								<div class="clearall" style="font-size:0px;">&nbsp;</div>
								<div class="smsblock"></div>
								<div class="clearall" style="font-size:0px;height:20px;">&nbsp;</div>
								{include file="cart/details.tpl"}
								<div class="clearall">&nbsp;</div>
							</div>
						</div>
						
						<div class="span-12 right captcha-block">
							<div class="notification-success"></div>
							<div class="notification-error"></div>
							<form id="captchaform" method="post" action="{$https_location}/mkorderBook.php?type=cod&orderid={$orderid}&xid={$XCARTSESSID}">
								<div class="clearfix captcha-details">
								<input type="hidden" name="mobile" value="{$codInitialMobileNumber}" id="mobile"/>
								<div class="p5 corners black-bg4 span-7 left">
									<img src="{$https_location}/captcha/captcha.php?id=codVerificationPage" id="captcha" />
								</div>
								<a href="javascript:void(0)" onclick="document.getElementById('captcha').src='{$https_location}/captcha/captcha.php?id=codVerificationPage&rand='+Math.random();document.getElementById('captcha-form').focus();_gaq.push(['_trackEvent', 'cod_verification', 'captcha_Refresh']);"
									id="change-image" class="no-decoration-link left">Change text</a>
								</div>	
								<div class="clearfix mt10 captcha-entry">								
									<span>Type the text you see in the image above and confirm the order.</span>
									<input type="text" name="userinputcaptcha" id="captcha-form" />
									<a href="javascript:void(0)" class="next-button orange-bg corners p5 verify-captcha left" onClick="verifyCaptcha()">Confirm the order</a>
									<div class="captcha-loading left" style="display:none;"></div>
								</div>
								<div class="captcha-message hide"></div>
							</form>
						</div>
						<div class="cashbackblock" style={if $cashback_gateway_status eq 'on'}"display:block;"{else}"display:none;"{/if}>
						<div class="order-totals" style="margin-top:88px;float:left;">
						{assign var=total_amout_to_be_paid value=$totalAmount+$giftamount+$cashdiscount}
							You are eligible for a cashback credit of <b>Rs
							{math equation="x * y" x=$total_amout_to_be_paid y=0.10  assign="cashback"}{$cashback|round|string_format:"%d"}
							</b> on successful completion of this order <a class="no-decoration-link right"  href="javascript:showCashbackLearnMore()" class="right" onclick="_gaq.push(['_trackEvent', 'cod_ver_cashback_msg', 'cashback_learn_more', 'click']);">Learn more</a>
						</div>
						</div>
					</div>
			</div>
	</body>
</html>
<script type="text/javascript">
	var orderid = "{$orderid}";
	var loginid = "{$loginid}";
</script>

{include file="site/secure_footer.tpl" }

{literal}
<script type="text/javascript">
	$(document).ready(function(){
		$('#captcha-form').keydown(function(event) {
			if (event.keyCode == '13') {
			event.preventDefault();
			$(".verify-captcha").trigger("click");
			}
		});
	});
	function verifyCaptcha() {
		if($(".verify-captcha").hasClass("btn-disabled")){
			return;
		}
		if($('#captcha-form').val() == ""){
			$(".captcha-message").html("Please enter the text.");
			$(".captcha-message").addClass("error");
	        $(".captcha-message").slideDown();
	        return;
		}
		$(".captcha-loading").show();
		$(".verify-captcha").addClass("btn-disabled")
		$(".captcha-message").html("").removeClass("error");
        $(".captcha-message").slideUp();
		$.ajax({
            type: "POST",
            url: "{/literal}{$https_location}{literal}/cod_verification.php",
            data: "codorderid="+orderid+"&codloginid="+ loginid + "&condition=clickverifycaptcha&confirmmode=customerpvqueued&mobile=" + $('#mobile').val() + "&userinputcaptcha=" + $('#captcha-form').val() ,
            success: function(msg){
        	        
                var resp=$.trim(msg);
                $(".captcha-loading").hide();
                $(".verify-captcha").removeClass("btn-disabled");
                if(resp=='CaptchaConfirmed') {
                	$(".verify-captcha").hide();//addClass("btn-disabled");
                	$(".captcha-loading").show();
                	$(".captcha-message").html('Code verified. Please wait while your order is being processed.');
                	$(".captcha-message").addClass('success');
                	$(".captcha-message").slideDown();
                	$("#captchaform").trigger("submit");
                	_gaq.push(['_trackEvent', 'cod_verification', 'captchaSubmit', 'Captcha_Confirmed']);
                } else if(resp=='WrongCaptcha') {
                	$(".captcha-message").html('Wrong Code entered');
                	$(".captcha-message").addClass('error');
					$(".captcha-message").slideDown();
					_gaq.push(['_trackEvent', 'cod_verification', 'captchaSubmit', 'Wrong_Captcha']);
                } else if(resp=='WrongOrder') {
                    $(".captcha-block").hide();
                    $(".cod-page-notifications").html('This order cannot be processed');
                    $(".cod-page-notifications").show();
                    _gaq.push(['_trackEvent', 'cod_verification', 'captchaSubmit', 'Wrong_Order']);
                } else if(resp=='OrderAlreadyConfirmed') {
                	$(".captcha-block").hide();
                	$(".cod-page-notifications").html('This order has already been confirmed');
                	$(".cod-page-notifications").show();
                	_gaq.push(['_trackEvent', 'cod_verification', 'captchaSubmit', 'Order_Already_Confirmed']);
                }
			}
		});
	}
</script>
{/literal}
