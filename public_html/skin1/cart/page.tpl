{include file="site/doctype.tpl"}
<head>
	{include file="site/header.tpl"}
</head>

<body class="{$_MAB_css_classes}">
<script>
Myntra.Data.pageName="cart";
</script>
{include file="site/menu.tpl" }
<div class="container clearfix">
    
<!-- shopping cart html starts-->
<h1 class="mybag-title">SHOPPING BAG</h1>
<div class="content clearfix">
<div class="content-left left last span-24 cart-summary-container">
    <table class="cart-summary">
    {if $productsInCart|@count > 0}
        <thead >
            <tr>
                <th colspan="2" style="text-align:left;width:373px; padding-left:20px; padding-right:20px;">Items</th>
                <th class="size-details">Size</th>
                <th width="112px">Quantity</th>
                <th style="text-align:right;" class="pricing-details">Subtotal</th>
            </tr>            
        </thead>
    {/if}
        <tbody>
        {if $redirectSource eq 'mi'}
        	<tr >
                <td colspan="5" style="padding:0px;">
                <div class='error'>
                    It seems your cart details have been modified during the checkout process.<br>
					Please confirm the details below and proceed.
                </div>
                </td>
            </tr>
            {/if}
            {if ($checkInStock) and ($items_out_of_stock or $items_zero_quantity or  $product_intotal_out_of_stock)}
            {if $items_zero_quantity}
            <tr >
                <td colspan="5" style="padding:0px;">
                <div class='error'>
                    Zero units have been ordered for some items. Please enter a larger number or click 'Remove'.
                </div>
                </td>
            </tr>
            {elseif $product_intotal_out_of_stock}
            <tr >
                <td colspan="5" style="padding:0px;">
                <div class='error'>
                    We're sorry, we don't have enough stock to process your entire order.
                </div>
                </td>
            </tr>
            {else}
            <tr >
                <td colspan="5" style="padding:0px;border:0px;">
                <div class='error' style="margin:0px;">
                    Some items in your order are out of stock.
                </div>
                </td>
            </tr>
            {/if}
            {/if}
            {assign var=numrows value=0}
            {if $productsInCart|@count > 0}
            {foreach name=combo item=productsSet key=discountId from=$productsInCart}
                {assign var=isCombo value=false}
                {if $discountId > 0 } 
                    {assign var=isCombo value=true}
                    {if $productsSet.dre_minMore > 0}
                        {assign var=isConditionMet value=false}
                    {else}
                        {assign var=isConditionMet value=true}
                    {/if}
                    <tr>
                        <td colspan="5" class="cart-combo noneborder" style="border-bottom: 0px none;">
                            <table class="light-grey-bg" style="margin-bottom:0px;">
                                <tr >
                                    <td colspan="5" style="padding: 8px;">
                                        <div class="combo-header-main mt10 tool-text">
                                            <span class="left">{$productsSet.icon}&nbsp;{eval var=$productsSet.label_text} </span>
                                            <a class="combo-completion-btn action-btn right" isConditionMet="{$isConditionMet}"  ctype="{if $productsSet.dre_buyCount>0}c{else}a{/if}"     min="{if $productsSet.dre_buyCount>0}{$productsSet.dre_buyCount}{else}{$productsSet.dre_buyAmount}{/if}" discountId="{$discountId}" href="javascript:void(0)">{if $isConditionMet}<span class="tick-small-icon">&nbsp;</span>&nbsp;Combo complete! {*<i>Modify Combo</i>*} {else}<span class="warning-small-icon">&nbsp;</span>&nbsp;Combo incomplete. <em>{if $productsSet.dre_buyAmount>0}Rs. {/if}{$productsSet.dre_minMore}{if $productsSet.dre_buyAmount>0}{/if} more to go!</em> {/if}</a>
                                        </div>
                                    </td>
                                </tr>
                {/if}
                
                {*Combo Loop starts here*}
                {foreach name=outer item=product key=itemid from=$productsSet.productSet}
                {assign var=numrows value=$numrows+1}
                    <tr id="product_view_{$itemid}">
                        <td class="product-details prepend">
                            <div class="product-image left">
                                {assign var='productStyleId' value=$product.productStyleId}
                                <img class="left" src="{$product.cartImagePath}" alt="Image Not Available">
                            </div>
                        </td>
                        <td class="product-name">
                            <div>
                            {if $product.productPrice gt 0}
                                <b>
                                {if $product.is_customizable eq 0 and $product.totalCustomizedAreas eq 0 and $product.landingpageurl neq '' 
					and !$product.freeItem}
                                  <a href="{$http_location}/{$product.landingpageurl}" onclick="_gaq.push(['_trackEvent', 'pdp_click', Myntra.Data.pageName, 'cart_title']);">{$product.productStyleName}</a>
				{else}
				  <span style="color: #0084BA;">{$product.productStyleName}</span>
                                {/if}<br></b>
                                
			    			{if $product.pricePerItemAfterDiscount neq '' || $product.freeItem eq 1}
                                <span class="discount-price cost">
			            {if $product.pricePerItemAfterDiscount eq 0}
					<strong> FREE </strong>
				    {else}
                                        Rs. <strong>{$product.pricePerItemAfterDiscount|round|number_format:0:".":","}</strong>
                            	    {/if}
                                </span>
                                <span class="strike"><b>{$product.productPrice|round|number_format:0:".":","}</b></span>
                                {else}
                            <span style="color:#d20000;">Rs. <strong>{$product.productPrice|round|number_format:0:".":","}</strong></span>
                                {/if}
                            {else if $product.productStyleName == 'Free Vouchers'}
                                <span style="color: #0084BA;font-weight:bold;">{$product.productStyleName}</span>
                            {/if}
                            
                            {*if $isCombo neq 0*}
                                {if $product.discountDisplayText neq ''}
                                <div class="mt10 tool-text">{$product.discountDisplayIcon}&nbsp;{eval var=$product.discountDisplayText}</div>
                                {elseif  $cashback_gateway_status eq 'on' && $enableCBOndiscountedProducts eq 'true'}
                                                    <div class="mt10 cart-info {if $cashback_displayOnCartPage neq '1'}hide{/if}">You will receive a cashback upto Rs.{math equation="(w-x) * z" w=$product.productPrice x=$product.discountAmount y=$product.quantity z=0.10 assign="cashback"}{$cashback|round|number_format:0:".":","} per item</div>
                                {elseif $cashback_gateway_status eq 'on' && $enableCBOndiscountedProducts eq 'false' && $product.discountAmount eq 0}
                                                    <div class="mt10 cart-info {if $cashback_displayOnCartPage neq '1'}hide{/if}">You will receive a cashback upto Rs.{math equation="(w-x) * z" w=$product.productPrice x=$product.discountAmount y=$product.quantity z=0.10 assign="cashback"}{$cashback|round|number_format:0:".":","} per item</div>
                                {/if}
                            {*else*}
                                {*PP: {$product.productPrice}; &nbsp;&nbsp; CS:{$comboSavings}; &nbsp; &nbsp; PQ: {$product.quantity}; &nbsp;&nbsp; PDA: {$product.discountAmount}; &nbsp; &nbsp; CT: {$comboTotal}*}
                            {*/if*}
                            
                           </div>
                        </td>
                        <form id="cartform_{$itemid}" method="post" action="modifycart.php">
                            <input type="hidden" name="_token" value="{$USER_TOKEN}" />
                            <input type="hidden" name="cartitem" id="cartitem" value="{$itemid}" >
                            <input type="hidden" name="update" value="updatesize"  >

                            <td class="size-details" align="center">
                            {assign var=breakloop value=false}
                            {if $product.sizenames && $product.sizequantities}
                                {if $product.sizenames|@count>1}
                                    {assign var=i value=0}
                                    {if $product.flattenSizeOptions}
                                        {assign var=productsizenames value=$product.sizenamesunified}
                                    {else}
                                        {assign var=productsizenames value=$product.sizenames}
                                    {/if}
                                    {foreach from=$productsizenames item=sizename}
                                        {if $product.sizequantities[$i] neq NULL and $breakloop eq false}
                                            {assign var=breakloop value=true}
                                            {assign var=selectedSize value=$sizename}
                                            {$sizename}
                                            <input name="{$itemid}sizename" class="itemsize" type="hidden" value="{$i}">
                                        {/if}
                                        {assign var=i value=$i+1}
                                    {/foreach}
                                {else}
                                    {if $product.flattenSizeOptions}
                                        {$product.sizenamesunified[0]}
                                    {else}
                                        {$product.sizenames[0]}
                                    {/if}
                                    <input name="{$itemid}sizename" class="itemsize" type="hidden" value="0">
                                {/if}
                            {else}
                                NA
                            {/if}
                            </td>
                            <td width='112px' style="text-align:center;">
                                {* -- Old Code To Discard. This was maintaining multiple rows for each Size Name -- *}
                                {* if $product.sizenames && $product.sizequantities *}


                                {* else *}
                                {assign var=sku value=$product.productStyleId|cat:'-'|cat:$selectedSize}
                                <input id="{$itemid}qty"  name="{$itemid}qty" {if $product.productPrice eq 0 || $product.freeItem }disabled="true"{/if}
                                        class="quantity filter-select cart-options quantity-box" 
                                        onChange="disableMakePaymentBlock();updateTotalQuantityNoDialog({$itemid}, this); " 
                                        value="{$product.quantity}" maxlength="3" 
                                        {if ($checkInStock)  and ($product_availability[$itemid] lt $product.quantity or $product.quantity eq 0)}
                                            style="border:2px solid #ff0000"
                                        {/if}>
                                        <br>

                                        {if ($checkInStock) and $product.quantity eq 0 }
                                        <span style="color:#ff0000;">Zero unit(s) ordered</span>
                                        {elseif ($checkInStock) and $product_availability[$itemid] lte 0 }
                                            <span style="color:#ff0000;">Sold Out</span>
                                        {elseif ($checkInStock) and $product_availability[$itemid] lt $product.quantity }
                                        <span style="color:#FF8040;">Only {$product_availability[$itemid]} units in stock</span>
                                        {/if}
                                        <br>
                                        {if $product.productPrice gt 0 && !$product.freeItem}
                                        <a href="{$http_location}/modifycart.php?remove=true&itemid={$itemid}&_token={$USER_TOKEN}"
                                class="no-decoration-link remove-link"
                                onClick="if(confirm('Remove {if $product.productStyleName} {$product.productStyleName|escape:'quotes'} {else} this {/if} from cart ?')){literal}{_gaq.push(['_trackEvent', 'cart', 'remove_item']);return true;}else{return false;}{/literal}">
                                Remove
                                </a>
                                {/if}
                            {* {/if} *}
                            </td>
                        </form>

                        <td class="pricing-details noneborder">
                            <span class="discount-price">
                                {if $product.productPrice eq 0 || $product.freeItem eq 1}
                                    FREE
                                {else}
                                    Rs. <strong>
					{assign var=amount value=0}
					{if $product.pricePerItemAfterDiscount neq ''}
					{math equation="a*b" a=$product.pricePerItemAfterDiscount b=$product.quantity assign="amount"}{$amount|round|number_format:0:".":","}
					{else}
					{math equation="(a*b)-c" c=$product.discountAmount a=$product.productPrice b=$product.quantity assign="amount"}{$amount|round|number_format:0:".":","}
					{/if}</strong>
				{/if}
                            </span>
                        </td>
                    </tr>
                {/foreach}
                {if $isCombo}
                        <tr>
                            <td colspan="5" width="100%" class="noneborder">
                                {if isset($productsSet.comboSavings) && $productsSet.comboSavings>0}
                                    <div class="cart-discount">COMBO SAVINGS <span style="color:#d20000"> Rs. <strong>{$productsSet.comboSavings|round|number_format:0:".":","}</strong></span></div>
                                {/if}
                                <div class="cart-discount">COMBO TOTAL <span class="cost">Rs. <strong>{math equation="a-b" a=$productsSet.comboTotal b=$productsSet.comboSavings assign="comboActualTotal"}{$comboActualTotal|round|number_format:0:".":","}</strong>&nbsp;{if isset($productsSet.comboSavings) && $productsSet.comboSavings>0}<span class="strike"><b>{$productsSet.comboTotal|round|number_format:0:".":","}</b></span>{/if}</span></div>
                            </td>
                        </tr>
                    </table>
                </td>
                </tr>
                {/if}
            {/foreach}
            {if $totalAmount neq $amount}
                {if (isset($eossSavings) && $eossSavings>0) || (isset($cartLevelDiscount) && $cartLevelDiscount>0 )}
                <tr>
                    <td colspan="5" width="100%" class="noneborder">
                        <div class="cart-discount">SAVINGS <span style="color:#d20000"> Rs. <strong>{math equation="a+b" a=$eossSavings b=$cartLevelDiscount assign="totalDiscount"}{$totalDiscount|round|number_format:0:".":","}</strong></span></div></td>
		 </tr>
                {/if}
            {/if}
            {if $shippingCharge && $shippingCharge > 0}
                <tr>
                    <td colspan="5" width="100%" class="noneborder"><div class="cart-discount">Shipping <span style="color:#d20000">(+) Rs. <strong>{$shippingCharge}</strong></span></div></td>
                </tr>
            {/if}
            <tr>
                <td colspan="5" width="100%" class="noneborder">
                    <div class="cart-discount-total left">{eval var=$cartDiscountMessage}
						{if $cartDiscountExclusionTT}
							&nbsp; <span class="cart-discount-exclusion-tt" style="color:#808080;border-bottom: 1px dotted #006699;" title='{eval var=$cartDiscountExclusionTT}'>
                		Conditions Apply</span>
            			{/if}
					</div>
                    <div class="cart-discount-total left">{eval var=$cartMessage}</div>
                    <div class="total-amount">TOTAL <span class="cost">Rs. <strong>{$totalAmount|round|number_format:0:".":","}</strong></span></div>
                </td>
            </tr>
                {if ($numrows>1)}
                <tr>
                    <td colspan="2" class="product-details prepend" style="border:0px none;padding:10px 0 0 0px;">
                        <div>
                            <a href="{$http_location}/modifycart.php?removeall=true&_token={$USER_TOKEN}"
                               class="no-decoration-link remove-all-link left" style="margin-bottom:40px"
                               onClick="if(confirm('Do you want to remove all products from Cart ?')){literal}{_gaq.push(['_trackEvent', 'cart', 'remove_all_items']);return true}else{return false;}{/literal}">
                               Clear Shopping Bag
                            </a>
                        </div>
                    </td>
                </tr>
                {/if}

            {else}
            <tr>
                <td class="empty-cart" style="padding:0px;border:0px none;">
                    <div>
                        Your Shopping Bag is currently empty.<br>
                         {if $login eq ""}Please <a href="javascript:void(0);" class="btn-login">Login</a> in to link items to your account, or view items already in your account{/if}
                    </div>
                </td>
            </tr>

            {/if}
        </tbody>
    </table>
    <div class="right span-8 prepend-1 last">

    </div>
</div>

{*Right side cart details*}    	
{if $productsInCart}
<div class="content-right right span-9 black-bg5 last"  id="cart_coupon_options">
    <div class="pay-now span-9 need-help black-bg5 corners last clearfix">
        <div class="title">Checkout</div>
	    

    </div>

        <div class="order-summary">
            <div class="total clearfix">TOTAL <span class="cost">Rs. <strong>{$totalAmount|round|number_format:0:".":","}</strong></span></div>
            {if $savings && $savings > 0}
            <div class="savings">
                 SAVINGS <span class="cost">Rs. <strong>{$savings|round|number_format:0:".":","}</strong></span>
            </div>
            {/if}
        </div>
       {if $cartDiscountMessage}
        <div class="cart-level-discount-message">
            <div class="inner">
                {eval var=$cartDiscountMessage}
				{if $cartDiscountExclusionTT}
                         &nbsp; <span class="cart-discount-exclusion-tt" style="color:#808080;border-bottom: 1px dotted #006699;" title='{eval var=$cartDiscountExclusionTT}'>
                        Conditions Apply</span>
				{/if}
            </div>
        </div>
        {/if} 
        {if $cartMessage}
	        <div class="cart-level-discount-message">
	            <div class="inner">
	                {eval var=$cartMessage}
	            </div>
	        </div>
        {/if}
        
        <div class="make-payment">

            <div id="pp-block" {if !$login}style="display:none;"{/if} class="clear mt20" >
                {if ($checkInStock) and ($items_out_of_stock or $items_zero_quantity or $product_intotal_out_of_stock)}
                    <span class="make-payment-link disable-btn corners">CHECKOUT NOW &raquo; </span>
                {else}
                    <a href="{$https_location}/mkcustomeraddress.php" class="make-payment-link orange-bg corners" onClick="_gaq.push(['_trackEvent', 'cart', 'make_payment']); s_analytics.linkTrackVars='events';s_analytics.linkTrackEvents=s_analytics.events='scCheckout';s_analytics.tl(this,'o','Checkout Initiated'); return true;">CHECKOUT NOW &raquo; </a>
                    <span class="tiplink">Where do I use my coupons ?</span>
                    <div class='myntra-tooltip tooltip-coupons'><div class='cre-val'>You'll have a chance to redeem your coupons on the payment page, as you checkout.</div></div>
                    {*<p class="linktip hide"><img class="tipclose" alt="close" src="{$cdn_base}/skin1/images/cart-removebtn.png"> </p>*}
                {/if}
            </div>

            {if $login eq ''}
            <div class="mt20 clearfix mb20 ">
                 <a href="javascript:void(0);" class="login-make-payment-link orange-bg corners" id="loginFromCart" onClick="_gaq.push(['_trackEvent', 'cart', 'make_payment']); s_analytics.linkTrackVars='events';s_analytics.linkTrackEvents=s_analytics.events='scCheckout';s_analytics.tl(this,'o','Checkout Initiated'); return true;">CHECKOUT NOW &raquo; </a>
            </div>
            {/if}
            {*<span class="note" style="clear:both;">Click the button above to make the payment & complete your order.</span>*}
        </div>
		    <div class="cart-cb-block clear" style={if $cashback_gateway_status eq 'on' && $cashback_displayOnCartPage eq '1'}"display:block;"{else}"display:none;"{/if}>
                    <div class="clear">
                        <span class="ttle pt10 mb10">After payment</span>
                    <span style="text-align:center;display:block;"><b>Cashback Credit: Rs. {math equation="(x + y) * z" x=$cashBackAmountDisplayOnCart y=$cashdiscount z=0.10 assign="cashback"}{$cashback|round|number_format:0:".":","}*</b></span>
                    <span class="note mt10">*Coupon discounts are not included</span>
                    </div>
            </div>
       
        <!-- PORTAL-314: Secure Image added-->
        <div align="center">
            <img src="{$cdn_base}/skin1/images/secure-shopping2.png" width="97%">
        </div>
    </div>
</div>
{/if}
	
{*AVAIL*}
<div class="related-products left clearfix last span-33 ">
    <div id="avail-container"></div>
</div>
{*END AVAIL*}

</div>
</div>

<div style="clear:both; height:10px; overflow:hidden"></div>

<div id="cart-combo-overlay" class="lightbox">
    <div class="mod">
        <div class="hd">
        </div>
        <div class="bd clearfix">
        </div>
        <div class="ft">
            <span class="right cps"></span>
        </div>
    </div>
</div>

{include file="site/footer.tpl"}

{if $windowid ne ''}
    <script language="JavaScript" type="text/JavaScript">
        $.get('generatecartimages.php?windowid={$windowid}');
    </script>
{/if}

<script>
    pageName="cart";
    var avail_cart_abtest='{$avail_cart_abtest}';
    av_id_added='';av_id_removed='';uqs='';uq="false",t_code='';
    {if $av_id_added != ''}
  		av_id_added="{$av_id_added}";
  	{/if}
   	{if $av_id_removed != ''}
        av_id_removed="{$av_id_removed}";
    {/if}
    {if $uqs != ''}
    	uqs="{$uqs}";
    {/if} 
    {if $uq != ''}
      	uq="{$uq}";
    {/if}     
   	{if $t_code != ''}
         	t_code="{$t_code}";
    {/if}  
</script>


{if $productsInCart}
<!-- Google Code for Shopping Cart - with product(s) Remarketing List -->
<script type="text/javascript">
var google_conversion_id = 1068346998;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "666666";
var google_conversion_label = "F9QICNb0swIQ9ty2_QM";
var google_conversion_value = 0;
</script>
<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
    <img height="1" width="1" alt="" 
        src="http://www.googleadservices.com/pagead/conversion/1068346998/?label=F9QICNb0swIQ9ty2_QM&amp;guid=ON&amp;script=0">
</noscript>
{/if}

{if !$productsInCart and $useractionclearcart eq true}
<!-- Google Code for Shopping Cart - Empty Remarketing List -->
<script type="text/javascript">
var google_conversion_id = 1068346998;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "666666";
var google_conversion_label = "hxAyCObyswIQ9ty2_QM";
var google_conversion_value = 0;
</script>
<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js"></script>
<noscript>
    <img height="1" width="1" alt=""
        src="http://www.googleadservices.com/pagead/conversion/1068346998/?label=hxAyCObyswIQ9ty2_QM&amp;guid=ON&amp;script=0">
</noscript> 
{/if}

</body>
</html>
