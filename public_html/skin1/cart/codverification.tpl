<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
{include file="common_meta.tpl" }
    <head>
    {literal}
        <style>
		input,img{border:0px;vertical-align:middle;}
			.process-steps{border:0px solid red;padding:10px;margin-bottom:50px;}
			.process-block{ width:375px; height:35px;border:0px solid red;}
			.process-block div{ float:left; overflow:hidden;}
			.stone p{text-align:right;width:65px; font-size:11px; font-weight:bold; color:#999999;}
			.sttwo p{color:#999999;font-size:11px;font-weight:bold;text-align:center;width:187px;}
			.stthree p{text-align:right;width:77px; font-size:11px; font-weight:bold; color:#333333;}
			.pblock-text{width:450px;}
			.order-details-block{overflow:hidden;}
			.order-left-block h1{display:block;float:left;font-size:18px;padding:0;margin-right:10px;}
			.order-details-block h1{display:block;font-size:18px;padding:0;margin-right:10px;}
			.order-left-block .header-link{float:left;line-height:22px;}
			.order-left-block .header-link a{color:#394b95;font-weight:bold;}
			.order-left-block{width:470px; border:0px solid red;}
			.order-right-block{width:310px; border:0px solid red;float:right;}
			
			/*.smsblock{width:300px;text-align:left;overflow:hidden;position:relative;margin:5px 0 0 0; background-color:lightgoldenRodYellow; border:1px  dashed #a5a5a5;}
			.smsblock p{border:0 solid #FFF; padding:10px 0 10px 20px;}
			.smsblock p strong{ display:block; }
			.smsblock p span{color:#666666;display:block;font-weight:bold;padding:11px 0;text-align:center; }
			.smsblock p span b{color:red; font-weight:normal; display:none; text-align:center;padding-bottom:5px;}
			.smsblock .smscode a{color:blue;padding-left:65px;text-decoration:underline;}
			
			.sendsmsbox a{color:blue;padding-left:45px;text-decoration:underline;}
			
			.smsbtn {
				background:url("{$cdn_base}/skin1/images/button-style1.png") repeat-x scroll left top transparent;
				border-bottom:1px solid #BC8318;
				border-right:1px solid #BC8318;
				color:white;
				cursor:pointer;
				font-weight:bold;
				height:25px;
				line-height:30px;
				margin-right:10px;
				text-align:center;
				text-decoration:none;
				width:87px;
			}*/
			
			
			
			.cart-opt-block{margin:5px 0 0 0;background:url("{$cdn_base}/skin1/images/left-block-bg.gif") repeat-x top left scroll; border:1px solid #a5a5a5;}
			.cart-opt-block .choies-pay-opt{margin:10px 0;display:block;font-size:13px; font-weight:bold;}
			.cart-opt-block p{border:1px solid #FFF; padding:10px 0 10px 20px;}
			
			table.cart-final-items td.carttotal{text-align:right;padding-right:10px;}
			table.cart-final-items{width:470px; font-size:11px;}
			table.cart-final-items td{font-size:12px;border-bottom:1px dotted;margin:0 0 10px;padding:15px 0 10px;text-align:center;}
			table.cart-final-items td.prodec{padding-left:20px;font-weight:bold;text-align:left;}
			table.cart-final-items td.pricelist{padding-left:25px; line-height:20px; font-weight:normal;text-align:left;}
			table.cart-final-items td.pricelisttotal{padding-right:12px;line-height:20px; font-weight:normal;text-align:right;}
			table.cart-final-items td.pricelisttotal span{border:0px solid red;display:inline-block;width:auto!important;min-width:50px;width:50px; }
			table.cart-final-items td.carttotal span{border:0px solid red;display:inline-block;width:auto!important;min-width:35px;width:35px; }
			table.cart-final-items td.cartsize ul, table.cart-final-items td.cartqty ul{list-style:none outside none;margin:0;}
			table.price_details td{padding:0;margin:0;border:0px}
			table.cart-final-items th{
                background:url("{$cdn_base}/skin1/images/result-bg.gif") repeat-x scroll left top #CCCCCC;
                border-top:1px solid #E0E0E0;
                padding:8px;
            }
			table.cart-final-items tr.tableheader th.leftb{border-left:1px solid #E0E0E0;}
			table.cart-final-items tr.tableheader th.rightb{border-right:1px solid #E0E0E0;}
			
			.total-block{margin-top:-1px;overflow:hidden;border:1px solid #a5a5a5;}
			
			.cart-total-text{width:177px; border:0px solid red; float:left; }
			.cart-total-text p{text-align:center;padding:5px;color:#FFFFFF; background-color:#575757;font-size:18px; font-weight:normal;}
			.cart-total-text1{width:170px; border:0px solid red; float:left; }
			.cart-total-text1 p{font-weight:bold;line-height:28px;padding-left:20px;}
			.cart-total-price{width:90px; border:0px solid red; float:right; }
			.cart-total-price p{line-height:20px;padding:5px 10px 5px 0px;text-align:right;font-weight:bold;}
			
			.cartgifts{height:40px;background-color:#F2F2F2;line-height:14px;margin:0px;padding:5px 25px; }
			.cartsms{line-height:34px;padding:5px 25px; }
			.cartgifts strong{color:#8e0800;}
			.cartgifts-select{font-size:12px;background-color:#F2F2F2;line-height:18px;padding:0px 25px 10px 25px; display:none;}
			.cartgifts-select textarea{margin:5px 0;width:215px;}
			
			.cartnumber {
				background:url("http://myntra.myntassets.com/skin1/site-horizontal/sprite.png") no-repeat scroll -982px top transparent;
				border-color:#D3D3D3;
				border-left:0 solid #D3D3D3;
				border-style:solid;
				border-width:0 1px 1px 0;
				color:#666666;
				font-size:12px;
				height:25px;
				padding:3px 0 0 5px;
			}
			span.opt-cr-block{
				margin-bottom:8px;
				margin-left:0px;
				background-color:#FFF;
				color:#666666;
				display:block;
				font-weight:bold;
				padding:12px 5px;
				width:255px;
				border:1px solid #c9c7c7;
			}
			span.opt-cr-block input{margin:0 7px;}
			
			.cart-links{margin:20px 0 0 0; text-align:center;}
			.cart-links a, .cart-links a:visited{color:#1951a6;font-weight:bold; padding-right:15px;}
			.cart-links a:hover{text-decoration:none;color:#333333;}
			
				.cart-button-img{
			background-image:url("{$cdn_base}/skin1/images/cart-button-bg.png");
			background-position:left top;
			background-repeat:no-repeat;
			height:40px;
            float:left;
		}
		
		.cart-button-img span{
			background-image:url("{$cdn_base}/skin1/images/cart-button-bg.png");
			background-position:right -80px;
			background-repeat:no-repeat;
			margin-left:2px;
			text-align:center;
			height:40px;
			display:block;
		}
		.cart-button-img span a, .cart-button-img span a:visited{
			background-image:url("{$cdn_base}/skin1/images/cart-button-bg.png");
			background-position:right -40px;
			background-repeat:repeat-x;
			color:#FFFFFF;
			display:block;
			font-family:Arial,Helvetica,sans-serif;
			font-size:18px;
			font-weight:bold;
			height:40px;
			text-decoration:none;
			
			padding: 0 10px;
			line-height:40px;
			margin-right:2px;
		}
		.cart-button-img span a:hover{
			text-decoration:none;
			color:#F3F000;
		}
		.access-block{border:0px solid;margin:0 auto;width:580px;_margin-left:200px;margin-bottom:80px;}
		.access-error{color:#be0709;font-size:18px;font-weight:bold;text-align:center; line-height:22px;margin-bottom:20px;}
		
		.boldtext{font-weight:bold;}
		
		</style>
    {/literal}
    </head>
    <body id="body">


        <div id="scroll_wrapper">
            <div id="container">

            {include file="mykrititop.tpl" }

                <div id="wrapper" >
                {include file="breadcrumb.tpl"}

                    <div class="order-details">
						<div class="process-steps" align="center"> 
							<div class="process-block">
								<div class="step-one"><img src="{$cdn_base}/skin1/images/step-one.jpg" style="border:0px";></div>
								<div class="step-two"><img src="{$cdn_base}/skin1/images/step-two-v2.jpg" style="border:0px";></div>
								<div class="step-three"><img src="{$cdn_base}/skin1/images/step-three-green.jpg" style="border:0px";></div>
							
								<div class="pblock-text">
									<div class="stone"><p>Login</p></div>
									<div class="sttwo"><p>Select Shipping Address</p></div>
									<div class="stthree"><p>Place Order</p></div>
								</div>
							</div>
						</div>
							<a name="shipping-section"></a>
							<div class="clear">&nbsp;</div>
							<div class="clear">&nbsp;</div>
							<div class="order-details-block" align="center">
								<div class="order-left-block">
								<!--div class="header-link"><a href="{$http_location}/mkcustomeraddress.php?change=true">(Change Shipping Address)</a></div-->
								<div class="clearall" style="font-size:0px;">&nbsp;</div>
									<div class="smsblock">
									
									<form id="smsform" method="POST" action="{$http_location}/mkorderBook.php?type=cod&orderid={$orderid}&xid={$XCARTSESSID}" class="corners clearfix">
										<div class="zd-notification-error"></div>
										<div class="zipdial-box corners">
											<div class="zd-logo">Mobile Number Verification</div>
											<div class="zd-stepone zd-step">
												<div class="zd-content">
													<div class="zd-notification">Enter your mobile number to verify</div>
													<span class="mobile-no-prefix">+91 - </span><input type="text" value="{$codInitialMobileNumber}" name="user-mobile-no" id="user-mobile-no">
													<a href="javascript:void(0)" class="next-button orange-bg corners" onClick="moveToStepTwo()">Verify</a>
												</div>
<!--												<div class="zd-progress"></div>-->
											</div>
											<div class="zd-steptwo zd-step" style="display:none;">
												<div class="zd-content">
													<div class="zd-notification">Dial this number from your mobile.</div>
													<div class="zd-number"></div>
													<div>(You will not be charged.)</div>
												</div>
<!--												<div class="zd-progress"></div>-->
												<a href="javascript:void(0)" class="zd-back-btn corners orange-bg" onClick="moveToStepOne('')">Back</a>
											</div>
											<div class="zd-verified zd-step" style="display:none;">
												<div class="zd-content">
													<div class="zd-notification"><span class="zd-notification-success">Mobile number successfully verified!<br/>Placing your order...</span></div>
												</div>
<!--												<div class="zd-progress"></div>-->
											</div>
											<div class="zd-loading zd-step" style="display:none;">
												<img src="{$cdn_base}/skin1/myntra_images/loading-graphic.gif" alt="loading graphic">
											</div>
										</div>
										<div class="zd-instructions">
												<b>How it works :</b><br/>
												1. Enter your mobile number and press verify button to proceed.<br/>
												2. From your mobile dial the number that will flash on the screen.<br/>&nbsp;&nbsp;&nbsp;Call is toll-free and auto disconnects after 1 ring.<br/> 
												3. Your number is verified and order is placed.<br/>
										</div>
									</form>
								</div>
								<div class="clearall" style="font-size:0px;height:20px;">&nbsp;</div>
                                    {include file="cart/details.tpl"}
									<div class="clearall">&nbsp;</div>
							</div>
							
					</div>

                        <div class="clearall" style="_height:50px">&nbsp;</div>
						
                    </div>
                {include file="footer.tpl" }
                <script type="text/javascript">
                var total_amount = {$totalAmount};
                var orderid = {$orderid};
                </script>
                 {literal}    
<script type="text/javascript">
	var numericReg  =  /(^-?\d\d*\.\d*$)|(^-?\d\d*$)|(^-?\.\d\d*$)/;
	var max_rand_limit = 99999;
	
	function moveToStepOne(msg){
		$(".zd-notification-error").html(msg);
		$(".zd-steptwo").stopTime("pollTimer");
		$(".zd-steptwo").fadeOut();
		$(".zd-stepone").fadeIn();
	}

	$(document).ready(function(){
		$("#user-mobile-no").keydown(function(event){
			if(event.keyCode == '13'){
				event.preventDefault();
				$(".next-button").trigger('click');
			}
		});
					
    });

	function validateUserMobileNumber(){
		var userMobileNumber=$("#user-mobile-no").val();
		var error=false;
		if(!numericReg.test(userMobileNumber)){
			error=true;
			$(".zd-notification-error").html("Please enter a valid mobile number");
		}
		else if(userMobileNumber.length!=10){
			error=true;
			$(".zd-notification-error").html("Mobile number should be 10 digits.");
		}
		return error;
	}
	
	function moveToStepTwo(){
		//make request to get the Zipdial number to call
		$(".zd-loading").fadeIn();
		var hasError=validateUserMobileNumber();
		if(!hasError){
			var random_num=Math.floor(Math.random()*max_rand_limit+1);
			$.get("zipDialGetDisplayNum.php?mobile="+$("#user-mobile-no").val()+"&v="+random_num+"&orderid=", function(data) {
				var responseObj=$.parseJSON(data);
				var status=responseObj.status;
				
				if(status){
					if(responseObj.codfailed) {
						wrongOrderNotification();
						$(".zd-loading").fadeOut();
					} else {
						updateZipDialNumber(responseObj.display_num);
						showStepTwo(responseObj.transaction_token);
					}
					
				//make entry in db with N status
				}
				else{
					$(".zd-loading").fadeOut();
					zipdialVerificationFailed("Sorry, verification service has failed. Please try again after sometime.");
				}
			});
		}
	}

	function showStepTwo(transaction_token){
		$(".zd-notification-error").html("");
		addEntryToCodInfoTable(transaction_token);
		$(".zd-stepone").fadeOut();
		$(".zd-loading").fadeOut();
		$(".zd-steptwo").fadeIn();
		var count=0;
		$(".zd-steptwo").everyTime(3000, "pollTimer", function(){
			count+=3000;
			if(count < 180000){
				var random_num=Math.floor(Math.random()*max_rand_limit+1);
				$.get("zipDialPollToVerify.php?transaction_token="+transaction_token+"&v="+random_num+"&orderid="+orderid, function(data) {
					var responseObj=$.parseJSON(data);
					var status=responseObj.status;
					if(status){
						if(responseObj.codadminverified) {
							codAdminVerified();
							$(".zd-steptwo").stopTime("pollTimer");
						} else if(responseObj.codfailed) {
							wrongOrderNotification();
							$(".zd-steptwo").stopTime("pollTimer");
						} else {
							zipdialVerified(transaction_token);
							$(".zd-steptwo").stopTime("pollTimer");
						}
					}
				});
			}
			else {
				moveToStepOne("Mobile Number Verification Failed. Please Retry.");
			}
		});
	}

	function updateZipDialNumber(display_num){
		$(".zd-steptwo .zd-number").html(display_num);
	}
	
	function zipdialVerificationFailed(msg){
		$(".zd-verificationfailed .zd-notification").html(msg);
		$(".zd-stepone, .zd-steptwo").fadeOut();
		$(".zd-verificationfailed").fadeIn();
	}
	
	function zipdialVerified(transaction_token){
		$(".zd-steptwo, .zd-steptwo").fadeOut();
		$(".zd-verified").fadeIn();
		
		$.ajax({
            type: "POST",
            url: "cod_verification.php",
            data: "&condition=verifyzipdial&ziptoken=" + transaction_token,
            success: function(msg){
                var resp=$.trim(msg);
                if(resp=='ZipConfirmed') {
                	$("#smsform").trigger("submit");
                } else if(resp=='WrongOrder') { 
                	wrongOrderNotification();
                } else {
                    moveToStepOne("Mobile Number Verification Failed. Please Retry.");
                }
			}
		});
	}

	function codAdminVerified() {
		$(".zd-verified .zd-notification-success").html("This order has been confirmed by Customer Support. <br/>You will receive an email and SMS shortly.<br/>Go to Mymyntra Page to check order status");
		$(".zd-stepone, .zd-steptwo").fadeOut();
		$(".zd-verified").fadeIn();
	}

	function wrongOrderNotification() {
		$(".zd-notification-success").html("This order cannot be processed.");
		$(".zd-stepone, .zd-steptwo").fadeOut();
		$(".zd-verified").fadeIn();
	}

	function addEntryToCodInfoTable(transaction_token){
		
  			$.ajax({
                type: "POST",
                url: "cod_verification.php",
                data: "mobile=" + $("#user-mobile-no").val() + "&condition=clickverifyzipdial&ziptoken=" +transaction_token,
                success: function(msg){
                    var resp=$.trim(msg);
				}
			});
	}

	
</script>
                   
                   
                 {/literal}
                </div>
            </div>
