
<script type="text/javascript">
	lists_image_srcs = new Object();
</script>
<div class="category  clearfix">
    <!-- scrollable-box -->
    <div class="cart-widget-scrollable-box scrollable-box clearfix last">
        <!-- "previous page" action -->
        <a class="prevPage browse left"></a>
        <!-- root element for scrollable -->
        <div class="cart-widget-scrollable scrollable">
            <!-- root element for the items -->
            <ul class="cart-widget-items items">
                {assign var=i value=1}
                {foreach name=widgetdata_outerloop item=widgetdata from=$data}
                {if $selectedSKUs[$widgetdata.styleid]}
                    {foreach name=widgetdataloop item=widgetskuid from=$selectedSKUs[$widgetdata.styleid]}
                    {if $widgetdata.product neq '' && $widgetdata.product neq ' '}
                        <li class="cart-widget-item item itemsizes item_{$widgetdata.styleid}" >
                        <div class="cart-widget-item-container">
                            <form class="cart-widget-checkbox">
                                <input type="checkbox" name="selStyle" value="{$widgetdata.styleid}" disabled="true"
                                       data-skuid="{$widgetskuid}" data-fromcart="1" data-productid="{$products[$widgetdata.styleid][$widgetskuid].productid}" />
                            </form>
                            <table class="cart-widget-item-box item-box cow-item-box-{$widgetdata.styleid}">
                                <tr style="height:130px">
                                    <td colspan="2" class="cart-combo-widget-image cart-widget-item-box-selected cart-widget-item-box-pre-added">
                                        <img id="im_product_img_{$widgetdata.styleid}_{$widgetskuid}" src="{$widgetdata.search_image}" height="128" width="96" alt="{$widgetdata.product}">
                                            <a class="cart-quick-look action-btn" data-widget="combo_overlay" data-styleid="{$widgetdata.styleid}" data-fromcart="true" href="javascript:void(0)"><span></span>Quick Look</a>
                                        </img>
                                        <script type="text/javascript">
                                            lists_image_srcs['im_product_img_{$widgetdata.styleid}'] = '{$widgetdata.search_image}';
                                        </script>
                                    </td>
                                </tr>
                                <tr style="height:22px">
                                    <td colspan="2" class="cart-widget-item-desc item-desc">
                                        <div class="cow-price dp" id="cow-price-{$widgetdata.styleid}"><span style="font-weight:normal; text-align: center;">{$rupeesymbol}</span>{$widgetdata.price|number_format:0:".":","}</div>
                                        <div class="cow-tooltip myntra-tooltip tool-text" id="cow-tooltip-{$widgetdata.styleid}">
                                            {$widgetdata.stylename|strip}
                                        </div>
                                    </td>
                                </tr>
                                <tr class="size-selection-row-{$widgetdata.styleid}" style="height:30px;">
                                    <td style="padding-left:2px">
                                        <span style="font-weight:normal; text-align: center">Size</span>
                                    </td>
                                    <td style="padding-right:2px" class="cow-second-col">
                                        <select style="width:50px;" disabled="true" class="cow-size-loader cow-size-select-disabled" id="cow-size-select-{$widgetdata.styleid}" data-skuid="{$widgetskuid}" disabled="true">
                                            <option value="-">-</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr class="quantity-selection-row-{$widgetdata.styleid}" style="height:30px;">
                                    <td style="padding-left:2px">
                                        <span style="font-weight:normal; text-align: center">Quantity</span>
                                    </td>
                                    <td style="padding-right:2px" class="cow-second-col">
                                        <input type="text" disabled="true" id="{$widgetdata.styleid}_cow_qty"  name="{$widgetdata.styleid}_cow_qty" data-skuid="{$widgetskuid}"
                                            value="{$products[$widgetdata.styleid][$widgetskuid].quantity}" data-styleid="{$widgetdata.styleid}" maxlength="3" style="width:40px;text-align: center;"
                                            class="quantity filter-select cart-options quantity-box"/>
                                    </td>
                                </tr>                            
                            </table>
                        </div>
                    </li>
                    {/if}
                    {/foreach}
                {else}
                    {if $widgetdata.product neq '' && $widgetdata.product neq ' '}
                        <li class="cart-widget-item item itemsizes item_{$widgetdata.styleid}">
                        <div class="cart-widget-item-container">
                            <form class="cart-widget-checkbox">
                                <input type="checkbox" name="selStyle" value="{$widgetdata.styleid}" data-skuid="0"
                                       data-productid="0" />
                            </form>
                            <table class="cart-widget-item-box item-box cow-item-box-{$widgetdata.styleid}">
                                <tr style="height:130px">
                                    <td colspan="2" class="cart-combo-widget-image cart-widget-item-box-selected">
                                        <img id="im_product_img_{$widgetdata.styleid}" {if $smarty.foreach.widgetdata_outerloop.index gt 5} src="http://myntra.myntassets.com/skin2/images/spacer.gif" data-lazy-src="{$widgetdata.search_image}" class="lazy"{else}src="{$widgetdata.search_image}" {/if} height="128" width="96" alt="{$widgetdata.product}">
                                            <a class="cart-quick-look action-btn" data-widget="combo_overlay" data-styleid="{$widgetdata.styleid}" href="javascript:void(0)"><span/>Quick Look</a>
                                        </img>
                                        <script type="text/javascript">
                                            lists_image_srcs['im_product_img_{$widgetdata.styleid}'] = '{$widgetdata.search_image}';
                                        </script>
                                    </td>
                                </tr>
                                <tr style="height:22px">
                                    <td colspan="2" class="cart-widget-item-desc item-desc">
                                        <div class="cow-price dp" id="cow-price-{$widgetdata.styleid}"><span style="font-weight:normal; text-align: center;">{$rupeesymbol}</span>{$widgetdata.price|number_format:0:".":","}</div>
                                        <div class="cow-tooltip myntra-tooltip tool-text" id="cow-tooltip-{$widgetdata.styleid}">
                                            {$widgetdata.stylename|strip}
                                        </div>
                                    </td>
                                </tr>
                                <tr class="size-selection-row-{$widgetdata.styleid}" style="height:30px; visibility: hidden;">
                                    <td style="padding-left:2px">
                                        <span style="font-weight:normal; text-align: center">Size</span>
                                    </td>
                                    <td style="padding-right:2px" class="cow-second-col">
                                        <select style="width:50px;" class="cow-size-loader" id="cow-size-select-{$widgetdata.styleid}" data-skuid="0">
                                            <option value="-">-</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr class="quantity-selection-row-{$widgetdata.styleid}" style="height:30px; visibility: hidden;">
                                    <td style="padding-left:2px">
                                        <span style="font-weight:normal; text-align: center">Quantity</span>
                                    </td>
                                    <td style="padding-right:2px" class="cow-second-col">
                                        <input type="text" id="{$widgetdata.styleid}_cow_qty"  name="{$widgetdata.styleid}_cow_qty" data-skuid="0"
                                            value="{$products[$widgetdata.styleid].quantity}" data-styleid="{$widgetdata.styleid}" maxlength="3" style="width:40px;text-align: center;"
                                            class="quantity filter-select cart-options quantity-box"/>
                                    </td>
                                </tr>                            
                            </table>
                        </div>
                    </li>
                    {/if}                    
                {/if}
                {assign var=i value=$i+1}
                {/foreach}
            </ul>
        </div>
        <!-- "next page" action -->
        <a class="nextPage browse right"></a>
    </div>
</div>

