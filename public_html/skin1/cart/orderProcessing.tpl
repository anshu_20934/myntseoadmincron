<html>
<head>
<title>Processing Myntra Order</title>
</head>
<body>
	<center><img src="{$secure_cdn_base}/images/image_loading.gif"></center>
	<br>
	<center><b>Processing Payments.... </b></center>
	{literal}
	<script type="text/javascript">
		var parentWindow = window.opener;
		var winHTTPLocation = {/literal}'{$http_location}';{literal}
		var winHTTPSLocation = {/literal}'{$https_location}';{literal}
		var redirectLocation = {/literal}'{$redirect_url}';{literal}
		var message = {/literal}'{$redirect_message}';{literal}
		var index =-1;
			
		if(parentWindow!=null) {
			//check if parent window is myntra window if yes close this window and open confirmation page in parent window else open it in this window itself
			try {
				var parentDomain = parentWindow.document.domain.toLowerCase();
				index = winHTTPLocation.toLowerCase().indexOf(parentDomain);
			} catch (err) {
				
			}

			if(index==-1) {
				index = winHTTPSLocation.toLowerCase().indexOf(parentDomain);
			}
	
			if(index==-1) {
				//parent window has some other domain open confirmation page in this window it self
				window.location = redirectLocation;
			} else {
				//close this window and open the confirmation page in parent window.
				var parentDialogHeader = parentWindow.document.getElementById('splash-payments-processing-hd');
				if(parentDialogHeader!=null) {
					parentDialogHeader.innerHTML = "<h2>"+message+"<h2>";
				}
				
				var parentDialogBody = parentWindow.document.getElementById('splash-payments-processing-bd-pp');
				if(parentDialogBody!=null) {
					parentDialogBody.innerHTML = "<center>Please wait while redirecting</center>";
				}
	
				var parentCancelButton = parentWindow.document.getElementById('cancel-payment-btn');
				if(parentCancelButton!=null) {
					parentCancelButton.innerHTML="";
				}
	
				parentWindow.location = redirectLocation;
				parentWindow.focus();
				window.close();
			}
		} else {
			window.location = redirectLocation;
		}
	</script>
	{/literal}
</body>
</html>