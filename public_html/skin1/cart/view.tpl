   <div id="pageCart">
                        <div class="pageTitle">
                        MY CART <img src="{$cdn_base}/skin1/images/title-cart-img.jpg" alt="" style="border:0px;">
                        </div>
                        {if $productsInCart}
                        <div class="cart-block">
                            <div class="cart-blockHead">
                                <div class="cart-pro-dec">Product description</div>
                                <div class="cart-pro-price">Unit Price</div>
                                <div class="cart-pro-size">Options</div>
                                <div class="cart-pro-qty">Quantity </div>
                                <div class="cart-pro-total">Total</div>

                            </div>
			    <table class="cart_list">
                            {assign var=tabindex value=1}
                    		{foreach name=outer item=product key=itemid from=$productsInCart}
                                {include file="cart/item.tpl"}
			                {/foreach}

				</table>
								<div class="clearall"></div>
						<div class="checkout-amount">
							<table cellpadding="0" cellspacing="0" border="0" class="checkout-block">
								<tr>
									<td style="width:255px;padding:5px;">Amount : </td>
									<td style="width:10px;text-transform:capitalize;">Rs.</td>
									<td><p style="text-align:right;width:35px;padding:0px;">{$amount|string_format:"%.2f"}</p></td>
								</tr>
								<tr style="background-color:#84ac0e;color:#fff;">
										<td style="padding:5px;">Discount:<br/> {if $coupondiscount != 0 } (coupon code: {$couponcode}({$couponpercent})) {/if} </td>
									<td>Rs.</td>
									<td><p style="text-align:right;width:30px;">{$coupondiscount|string_format:"%.2f"}</p></td>
								</tr>
								<tr>
									<td style="padding:5px;">Vat/CST : </td>
									<td>Rs.</td>
									<td><p style="text-align:right;width:30px;">{$vat|string_format:"%.2f"} </p></td>
								</tr>
							   {*<tr>
									<td style="padding:5px;">Shipping Cost : </td>
									<td>Rs.</td>
									<td><p style="text-align:right;width:30px;">0</p></td>
								</tr>*}
								<tr>
									<td colspan="3">&nbsp;</td>
								</tr>
								<tr class="cart-final-total">
									<td style="padding:5px;">TOTAL : </td>
									<td>Rs</td>
									<td><p style="text-align:right;width:30px;">{$totalAmount|string_format:"%.2f"}</p></td>
								</tr>
								<tr>
									<td colspan="3"><p style="color:#9D2628; padding:5px 106px 5px 5px; text-align:right;" >
									{if ($cart_amount) gt $free_shipping_amount}
									    (Free Shipping in India)
									{/if}
									</p></td>
								</tr>
							</table>
						</div>
						<div class="clearall"></div>
						    {if $divid}
                             <div class="{$divid}" style="padding-left:355px;text-align:left;">
                                {$couponMessage}
                             </div>
                            {/if}
						<div class="discount-coupon-block">
							{assign var=action value="mkmycartmultiple_re.php?c=1&pagetype=$pagetype"}

                            <form name="couponform"  action="{$action}" method="post" id="couponform">
                            <p>

								<span class="coupon-text"><strong>Please enter a valid coupon code </strong></span> <img src="{$cdn_base}/skin1/images/coupon-arrow.jpg" alt=""class="cart-arrow">
								<input name="couponcode" id="couponcode" type="text" value="{$couponcode}">
								 <span class="couponbnt"><a style="cursor:pointer" onclick="return checkCoupon('{$login}', 'couponform');">Redeem</a></span>
							                                </p>
								</form>
						</div>
						<div class="clearall"></div>
						<div class="cart-checkout-buttons">
							<span style="margin-top:8px;"><a href="javascript:confirmCartDelete();">Clear My Cart<img src="{$cdn_base}/skin1/images/clear-cart.jpg" alt=""style="border:0px;padding-left:5px;vertical-align:middle;"></a></span>
							<span style="margin-top:8px;"><a href="{$http_location}/">Add More to Cart<img src="{$cdn_base}/skin1/images/clear-cart.jpg" alt=""style="border:0px;padding-left:5px;vertical-align:middle;"></a></span>
							<span >
							    {if $login eq ''}
								        <a href="{$http_location}/register.php"  class="cart-checkout-button" >CONTINUE</a>
								{else}
							            <a href="{$http_location}/mkcustomeraddress.php" class="cart-checkout-button" >CONTINUE</a>
	                            {/if}
							</span>

						</div>
								<div class="clearall"></div>

                            </div>
							<div class="cart-checkout">
								<div class="cart-checkout-total">
								<span>Total</span> : <span>RS. {$totalAmount|string_format:"%.2f"}</span>
								<p>
								{if ($cart_amount) gt $free_shipping_amount}
								(Free Shipping in India)
								{/if}
								</p>
								</div>
								<div align="right" style="margin:10px;overflow:hidden;">
								{if $login eq ''}
								        <a href="{$http_location}/register.php" class="cart-checkout-button" >CONTINUE</a>
								{else}
							            <a href="{$http_location}/mkcustomeraddress.php" class="cart-checkout-button" >CONTINUE</a>
	                            {/if}
								</div>
							</div>
                        {else}
                         <div class="cart-block">
                            <div class="cart-blockHead">
                                <div class="cart-pro-dec">Product description</div>
                                <div class="cart-pro-price">Unit Price</div>
                                <div class="cart-pro-size">Options</div>
                                <div class="cart-pro-qty">Quantity </div>
                                <div class="cart-pro-total">Total</div>

                            </div>
                        <div class="emptyCart">
                        Your shopping cart contains no items
                        </div>
                        </div>
                        <div class="cart-checkout">
								<div class="cart-checkout-total">
								<span>Total</span> : <span>RS. {$totalAmount|string_format:"%.2f"}</span> 
								</div>
								<div align="right" style="margin:10px;overflow:hidden;">

							            <a href="{$http_location}/" class="cart-checkout-button" >Add Items</a>

								</div>
						 
                        </div>
                        {/if}
				        <div class="clearall"></div>
				        {if $max_sla}
                        <div class="sla"><strong>Shipping Information:</strong> Your product(s) will be shipped within {$max_sla}  days and delivered within {$max_sla+1} to {$max_sla+2} days in India.</div>
                        {/if}
					</div>
