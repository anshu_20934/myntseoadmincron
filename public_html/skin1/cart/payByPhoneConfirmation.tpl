<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
	<head>
		{include file="site/secure_header.tpl" }
		{literal}
		<style>
			.order-details-block{overflow:hidden;margin-left: 140px;margin-bottom: 20px;}
			.order-left-block h1{display:block;float:left;font-size:18px;padding:0;margin-right:10px;}
			.order-details-block h1{display:block;font-size:18px;padding:0;margin-right:10px;}
			.order-left-block .header-link{float:left;line-height:22px;}
			.order-left-block .header-link a{color:#394b95;font-weight:bold;}
			.order-left-block{width:580px; border:0px solid red;}
			.order-right-block{width:310px; border:0px solid red;float:right;}
			table.cart-final-items td.carttotal{text-align:right;padding-right:10px;}
			table.cart-final-items{width:470px; font-size:11px;}
			table.cart-final-items td{font-size:12px;border-bottom:1px dotted;margin:0 0 10px;padding:15px 0 10px;text-align:center;}
			table.cart-final-items td.prodec{padding-left:20px;font-weight:bold;text-align:left;}
			table.cart-final-items td.pricelist{padding-left:25px; line-height:20px; font-weight:normal;text-align:left;}
			table.cart-final-items td.pricelisttotal{padding-right:12px;line-height:20px; font-weight:normal;text-align:right;}
			table.cart-final-items td.pricelisttotal span{border:0px solid red;display:inline-block;width:auto!important;min-width:50px;width:50px; }
			table.cart-final-items td.carttotal span{border:0px solid red;display:inline-block;width:auto!important;min-width:35px;width:35px; }
			table.cart-final-items td.cartsize ul, table.cart-final-items td.cartqty ul{list-style:none outside none;margin:0;}
			table.price_details td{padding:0;margin:0;border:0px}
			table.cart-final-items th{
				background:url("{/literal}{$secure_cdn_base}{literal}/skin1/images/result-bg.gif") repeat-x scroll left top #CCCCCC;
				border-top:1px solid #E0E0E0;
				padding:8px;
			}
			table.cart-final-items tr.tableheader th.leftb{border-left:1px solid #E0E0E0;}
			table.cart-final-items tr.tableheader th.rightb{border-right:1px solid #E0E0E0;}
			
			.total-block{margin-top:-1px;overflow:hidden;border:1px solid #a5a5a5;}
			
			.cart-total-text{width:177px; border:0px solid red; float:left; }
			.cart-total-text p{text-align:center;padding:5px;color:#FFFFFF; background-color:#575757;font-size:18px; font-weight:normal;margin:0px;}
			.cart-total-text1{width:170px; border:0px solid red; float:left; }
			.cart-total-text1 p{font-weight:bold;line-height:28px;padding-left:20px;margin:0px;}
			.cart-total-price{width:90px; border:0px solid red; float:right; }
			.cart-total-price p{line-height:20px;padding:5px 10px 5px 0px;text-align:right;font-weight:bold;margin:0px;}
		</style>
		{/literal}	
	</head>	
	<body>
	<div class="container clearfix">
	{include file="site/secure_menu.tpl" }
		<div class="content clearfix">
			<div class="span-20" style="font-weight: bold; margin: 50px 0 50px 140px;">
				<div style="font-size: 20px;  margin-bottom: 10px;">Just one step away from confirming your order</div>
				<div style="font-size: 12px;background: none repeat scroll 0 0 #E6E6E6; border: 2px solid #000000;margin-top: 6px; padding: 4px 10px;">Call up <span style="color:#990000">{$pay_by_phone_number}</span> to confirm the order. Your order id is <span style="color:#990000">{$orderid}</span></div>
			</div>
			<div class="order-details-block span-20">
				<div class="order-left-block">
					{include file="cart/details.tpl"}
				</div>
			</div>
		</div>
		{include file="site/secure_footer.tpl" }
	</div>
	</body>
</html>