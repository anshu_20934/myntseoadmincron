{include file="site/doctype.tpl"}
<head>
    {include file="site/secure_header.tpl"}
</head>
<body id="body" class="{$_MAB_css_classes}">

        <div id="scroll_wrapper">
            <div id="container">

            {include file="site/secure_menu.tpl" }

                <div id="wrapper" >
                {include file="breadcrumb.tpl"}

                    <div id="billadd">
							<div class="content-block" align="center">
                                {if $addresses }
								<div class="shipping-left-block">
										<p>Choose a <strong>Previously Used Shipping Address</strong></p>
										 <form id="previous_address" action="{$https_location}/mkcustomeraddress.php" method="post">
										 <input type="hidden" name="_token" value="{$USER_TOKEN}" />
										 <input type="hidden" id="previous_address_id" name="address" value="">
										{section name="index" loop=$addresses}
                                          <div class="address-block">
											<p><input type="radio" name="default_address" value="{$addresses[index].id}" {if $addresses[index].default_address eq 1 } checked {/if}>Use this as default address <span class="removelink"><a href="{$https_location}/mkcustomeraddress.php?remove=true&id={$addresses[index].id}&_token={$USER_TOKEN}" onclick="removeThisAddress()">Remove <img src="{$secure_cdn_base}/skin1/images/cart-removebtn.png" alt="remove"></a></span></p>
                                            {if $addresses[index].default_address eq 1 }
                                            {assign var="is_default" value="1"}
                                            <div style="display:none;">
                                                <span id="d_name">{$addresses[index].name}</span>
                                                <span id="d_address">{$addresses[index].address}</span>
                                                <span id="d_city"> {$addresses[index].city}</span><span id="d_state">{$addresses[index].state_code}</span>
                                                <span id="d_country">{$addresses[index].country_code}</span><span id="d_pincode">{$addresses[index].pincode}</span>
                                                <span id="d_mobile">{$addresses[index].mobile}</span>
                                                <span id="d_email">{$login}</span>
                                            </div>
                                            {/if}
											<div class="addblock">
												<span><strong>{$addresses[index].name}</strong></span>
												<span>{$addresses[index].address}</span>
												<span> {$addresses[index].city}, {$addresses[index].state}</span>
												<span> {$addresses[index].country}, {$addresses[index].pincode}</span>
												<span>Mobile: {$addresses[index].mobile}</span>
												<input type="button" value="Use this address" class="orange-bg p5 mt10 mb10 corners" style="cursor:pointer" onclick="useThisAddress('{$addresses[index].id}')">
											</div>
										</div>
										{/section}
										</form>

								</div>
								<div class="shipping-right-block">
								{else}
									<div class="shipping-right-block shipping-right-block-center">
								{/if}
								  <form id="shipping_address" action="{$https_location}/mkcustomeraddress.php" method="post">
								  	<input type="hidden" name="_token" value="{$USER_TOKEN}" />
									<div>
									  {if $addresses }
									<strong>Or</strong>
									  {/if}
									Enter a <strong>New Shipping address</strong></div>
									{*{if $is_default eq 1}<div><input type="checkbox" value="1" id="copy_default_address"> Copy <strong>Default Address </strong></div>{/if}*}
										<div class="new-address-block">
											  <div>
												<span>Pin Code <b>*</b></span>
                                                <input type="text" name="pincode" id="pincode" maxlength="6" value="{$smarty.post.pincode}">
                                                <span class="status">{$errors.pincode}</span>
                                                <p class="note">Please enter correct pin code to ensure delivery.</p>
												<input type="hidden" name="pinerr" value="0"/>
											  </div>
											  <div>

												<span>Recipient's Name <b>*</b></span>
												<input type="text" name="name" id="name" value="{$name}">
                                                <span class="status">{$errors.name}</span>
											  </div>
											{if $promptForName eq 1}
											  <div>
												<span><input class="copy-as-acc-name" name="copy-as-acc-name" type="checkbox" checked="true"/></span>
												<span>Save as your name</span>
											  </div>
											{/if}
											{if $promptForName eq 1}
											  <div class="your-name-div hide">
												<span>Your Name  <b>*</b></span>
												<input type="text" name="yourname" value="{$yourname}">
                                                <span class="status">{$errors.yourname}</span>
											  </div>
											{/if}
											  <div>
												<span>Address  <b>*</b></span>
                                                <textarea name="address" id="address">{$smarty.post.address}</textarea>
                                                <span class="status">{$errors.address}</span>
											  </div>
											  <div>
                                                  <span>Locality  {if $abLocality eq 'yes'}<b>*</b>{/if}</span>
                                                <input type="text" name="locality" id="locality" value="{$smarty.post.locality}">
                                                <span class="status">{$errors.locality}</span>
                                                <input type="hidden" name="selected-locality" id="selected-locality" value="">
											  </div>
											  <div>
												<span>City / District  <b>*</b></span>
                                                <input type="text" name="city" id="city" value="{$smarty.post.city}">
                                                <span class="status">{$errors.city}</span>
											  </div>
											  <div>
												<span>Country <b>*</b></span>
												<select name="country" id="country">
								   					<option value="IN" selected="true">India</option>
												</select>
                                                <span class="status">{$errors.country}</span>
											  </div>
											  <div>
												<span>State <b>*</b></span>
												<span id="state">
												<select name="state" id="state_select" class="required">
													<option value="">--Select State--</option>
													{section name="statename" loop=$states}
													<option value="{$states[statename].code}" {if $smarty.post.state eq $states[statename].code}selected="true"{/if}>{$states[statename].state}</option>
													{/section}
												</select>
												</span>
                                                <span class="status">{$errors.state}</span>
											  </div>
											  <div>
												<span>Email <b>*</b></span>
                                                <input type="text" name="email" id="email" value="{$smarty.post.email}">
                                                <span class="status">{$errors.email}</span>
											  </div>
											  <div>
												<span>Mobile (+91)<b>*</b></span>
                                                <input type="text" name="mobile" id="mobile" maxlength="10" value="{if $smarty.post.mobile}{$smarty.post.mobile}{else}{$userData.mobile}{/if}">
                                                <span class="status">{$errors.mobile}</span>
                                                <p class="note">Please do not prefix your mobile number with country code or '0'</p>
											  </div>
											  <!-- Kundan: Zip validation with state--> 
											  <div class="zip-err-warning-ship-addrs hide">
												<div class="warning-image"></div>
												<div class="zip-err-text-ship-addrs"></div>
											  </div>
											  <div class="cart-button-img">
                                                  <input type="hidden" name="add-address" value="1">
											     <button type="submit" class="orange-bg p10 corners mt10 save-btn" onclick="return saveAndUseThisAddress();">Save &amp; Use this address</button>
											  </div>
											</form>
										</div>
								</div>
							</div>
					</div>

                        <div class="clearall" style="_height:50px">&nbsp;</div>

                    </div>
                {include file="site/secure_footer_content.tpl"}
                </div>
            </div>

<script language="javascript">
var stateZipsJson = {$stateZipsJson};
</script>
{include file="site/secure_footer_javascript.tpl"}

    </body>
</html>
