<form action="{$https_location}/mkorderinsert.php" id="net_banking" method="post" target="MyntraPayment"
	{if $totalAmount eq 0} 
		style="display:none" 
	{elseif $defaultPaymentType eq "net_banking"} 
		style="display: block;"
	{else}
		style="display:none"	 
	{/if}
>
	<input type="hidden" name='s_option' value ='{$logisticId}'/>
	<!--delivery preference-->
	<input type="hidden" name="shipment_preferences" value="shipped_together" >
	<!--delivery preference-->								
	<input type="hidden" name="is_shipping_cost_set" value="{$s_city}">
	<input type="hidden" name="checksumkey" id="checksumkey" value="{$checksumkey}">
	<input type="hidden" name="address" value="{$address.id}">					

	<input type="hidden" value="netbanking" name="pm" />
	{if $netbankingDownBankListStr}
		<div class="warning-text-border clearfix">
				<div class="warning-image left"></div>
				<div class="left" style="width:470px">{$netbankingDownBankListStr}</div>
		</div>
	{/if}

	<div class="clearfix">
		<dl class="horizontal">
			<dt><label class="required">Transfer funds from :</label></dt>
			<dd>
				<select class="required" name="netBankingCards" id="netBanking" onChange="netBankingOptionChange();">
					{$bank_list_text}
				</select>
			</dd>
			<dd class="note"><strong>Please note:</strong> We will redirect you to the bank you have chosen above. Once the bank verifies your net banking credentials, we will proceed with your order.</dd>

			<dd><span class="cancelEditLinkPlaceholder" style="display: none;"><a class="control" href="#">Cancel my changes</a> or </span></dd>
		</dl>
	</div>
</form>
