<div class="tab_container">
	{if $cashback_gateway_status eq 'on'}
	<div style="">
	<label>You can redeem your Coupons and your Cashback for this purchase!</label><BR>
        {if $cartDiscountMessage}
        <label>Items in your Cart (including Gift Vouchers) will not be affected by usage of coupons or cashback.</label>
        {/if}
	</div>
	{/if}
    <div class="coupons-widget clear clearfix tab_content coupons-widget-border" id="cart_coupon_options">
    <label>COUPON</label>
{if $divid eq 'success'}
<a onclick="javascript:removeCoupon();" style="padding-left:10px;cursor:pointer;">(Remove)</a>
{elseif $divid eq 'error'}
{if $couponMessage}
<span class="{$divid} mt10" id="discount_coupon_message">
{$couponMessage}
</span>
{/if}

{/if}

{assign var=action value="mkpaymentoptions.php"}
		{if $couponMessage && $divid eq 'success'}
			<div class="{$divid} message mt10 clearfix" id="discount_coupon_message">
					{$couponMessage}
			</div>
		{/if}
			<form action="{$action}{if $addressID&&$addressID>0 }?address={$addressID}{/if}" method="post" id="couponform" name="couponform" class="clearfix {if $couponMessage && $divid eq 'success' } hide {/if}" >
        <input type="hidden" name="removecouponcode" id="removecouponcode" class="coupon-code left">
        <input type="hidden" value="" id="removecashcoupon" class="cb-input" name="cashcoupon">
        <input type="hidden" value="{$appliedCashbackCode}" id="code" class="cb-input" name="code">
        <input type="hidden" value="{$cashcoupons.MRPAmount}" id="userAmount" class="cb-input" name="userAmount">

		{if $rest_coupons}
        <div class="special-coupons left">

                {if $showVerifyMobileMsg && $mobile_status neq 'V'}
                    {assign var=mobileverificationrequired value=1}
                    <div id="discount_coupon_message" class="message vmessage mt10 clearfix" style="width: 300px; height: 34px; padding-top:1px; padding-bottom:1px;margin-top:4px;padding-left:0px;">
                        Please verify your mobile number to enable usage of your coupons. <a href="javascript:void(0)" onclick="$('#smsVerification').slideDown();_gaq.push(['_trackEvent', 'coupon_widget', 'verify_now', 'click']);">[Verify now]</a>
                    </div>
                {/if}
                <div class="coupon-block">
                        {if $mobileverificationrequired neq 1}
                    <div class="your-coupons">
                        <h2>Select a Coupon</h2>
                            <ul class="select-coupons" style="display:none;">
                                {section name=prod_num loop=$rest_coupons}
                                {if $rest_coupons[prod_num].coupon neq ""}
                                    <li >
                                        <div class="coupontext clearfix"><b>Coupon Type</b>: {$rest_coupons[prod_num].couponTypeDescription}</div>
                                        <div class="coupon-val clearfix"><span class="left"><b>Valid till</b> : {$rest_coupons[prod_num].expire|date_format:"%e-%m-%Y"}</span>
                                            <span class="select_this_coupon right" style="visibility: hidden;"><a href="javascript:redeemCoupon2('{$rest_coupons[prod_num].coupon}')" class="bold" style="color:#eeeeee" >Apply</a></span>
                                        </div>

                                    </li>
                                {/if}
                                {/section}
                            </ul>
                    </div>
                        {/if}
                </div>
        </div>
        {/if}
        {*Other discount coupons*}
        <div class="coupon-redeem {if $mobileverificationrequired eq 1}right{/if}">
        
            <div class="code-enter">
            {if $rest_coupons}<span style="display:block;margin-left:10px;margin-top:7px;float:left;margin-right:10px;">OR</span>{/if}
                <input type="hidden" name="couponcode" id="couponcode" class="coupon-code left">
                <input type="text" name="othercouponcode" id="othercouponcode" autocomplete="off" class="coupon-code left" value="Enter a Coupon" {if !$rest_coupons}style="margin-bottom: 5px!important;"{/if} onclick="javascript:clearTextArea();";>
		   		<a onclick="javascript:redeemCoupon();" style="display:block;margin-left:10px;margin-top:7px;float:left;margin-right:10px;cursor:pointer;">Redeem</a>            
	 	</div>
        </div>
        {*Other discount coupons@End*}
        
    </form>
	<div id="smsVerification" class="hide">

		<form action="mkpaymentoptions.php{if $addressID&&$addressID>0 }?address={$addressID}{/if}" method="post" id="change-profile" name="change-profile">
            <input type="hidden" name="mode" value="update-profile-data" />
            <input type="hidden" name="login" id="login" value="{$userProfileData.login}" />
			<input type="hidden" id="_token" name="_token" value="{$USER_TOKEN}" />
            <div class="v-code">
                <label>Name :</label> <span>{$userProfileData.firstname} {$userProfileData.lastname}</span><br>
                <label>E-mail :</label> <span>{$userProfileData.login}</span><br>
                <label>My phone  :</label> <span>(+91)<input type="text" id="mobile" name="mobile" value="{$userProfileData.mobile}" class="profile-mobile" maxlength="10" > <div class="verify-loading" id="mobile-verify-loading" style="display:none;*position:relative;*top:-25px"></div> 
                {if $mrp_skipMobileVerification neq 1}<input type="button" value="Verify" id="verify-mobile" class="verify-mobile-cart-page orange-bg gr corners">
                {else}{if $verified_for_another eq 1}<input type="button" value="Edit" id="verify-mobile" class="verify-mobile-cart-page orange-bg gr corners">{/if}{/if}</span>
                <div id="mobile-error" class="captcha-message error hide"></div>

            </div>
            <div class="v-code-note">
            	<p id="v-code-note-message" {if $verified_for_another eq 1 || $mobile_status eq 'E'} class="error" {/if} >
            		{if $verified_for_another eq 1}
            				Your mobile number is unverified because it has been registered against another user. Please enter a valid mobile number to use your coupons.
				    {elseif $userProfileData.mobile eq ''}
	                    Note: Please provide a contact
	                    number on which we can reach you for any issues related to your
	                    transactions. This number is also required to be verified for
	                    usage of any use-specific coupons that you may have been issued
	                    through Myntra.
                    {elseif $userProfileData.mobile neq "" && $mobile_status neq "V" && $mobile_status neq "E" && $verified_for_another eq 0}
	                    Note: Please verify your
	                    specified contact number to be able to use any user-specific
	                    coupons that you may have been issued through Myntra. We may use
	                    this number to contact you for issues related to any of your
	                    transactions on Myntra.
                	{elseif $mobile_status eq 'E'}
                    	You have exceeded the maximum no. of attempts to verify this mobile number. If you have still not received the verification code or are facing any other difficulties, please provide an alternate number and retry. Or, try again after 6 hours.
                    {/if}
					{if $mrp_skipMobileVerification neq 1 && $mobile_status neq 'V'}
                    <div style="padding-top:20px;">
                        In case you do not receive your verification code from Myntra, please call us at our Customer Support number at <b>080-43541999</b> anytime and get your number verified instantly.
                    </div>
                    {/if}
                </p>
            </div>

		</form>

		<div class="captcha-block hide" id="mobile-captcha-block">
		<fieldset>
		    <legend>Type the characters you see in the picture</legend>
            <form id="mobile-captchaform" method="post" action="">
                <div class="clearfix captcha-details">
                    <div class="p5 corners black-bg4 span-7 left">
                        <img src="{$secure_cdn_base}/skin1/images/spacer.gif" id="mobile-captcha" />
                    </div>
                    <a href="javascript:void(0)" onclick="document.getElementById('mobile-captcha').src='{$http_location}/captcha/captcha.php?id=paymentpage&rand='+Math.random();document.getElementById('mobile-captcha-form').focus();" id="change-image" class="no-decoration-link left">Change text</a>
                </div>

                <div class="clearfix mt10 captcha-entry">

                    <input type="text" name="mobile-userinputcaptcha" id="mobile-captcha-form" />
                    <input type="hidden" name="mobile-login" class="btn-captcha" id="mobile-login" value="{$userProfileData.login}" >
                    <a href="javascript:void(0)" class="next-button orange-bg gr corners p5 verify-captcha" id="mobile-verify-captcha" style="color:#fff;*position:relative;*top:-15px" onClick="verifyMobileCaptcha()">Confirm</a>
                    <div id="mobile-captcha-loading" class="captcha-loading" style="display:none;"></div>
                </div>
                <div class="captcha-message hide" id="mobile-captcha-message"></div>
            </form>
        </fieldset>
        </div>


    <form action="mkpaymentoptions.php{if $addressID&&$addressID>0 }?address={$addressID}{/if}" method="post" id="verify-mobile-code" name="verify-mobile-code" class="mt20 mb10 hide">
        <input type="hidden" name="mode" value="verify-mobile-code">
        <div class="four-dig-code">
        <p id="mobile-message"></p>
            <label style="*position:relative;*top:-4px">Verification Code:</label>
            <span style="*display:inline-block"><input id="mobile-code" name="mobile-code" size="4" style="*position:relative;*top:4px"></span>
            <div class="verify-loading" id="code-verify-loading" style="display:none; margin-right: 100px;"></div>
            <input type="button" value="Submit" id="submit-code" class="submit-code gr sbt orange-bg corners" style="*display: inline-block; *margin-top: 5px;">

        </div>

        <div class="four-dig-code-note">
           <p id="mobile-verification-error">If you have not received your verification code, please wait for atleast 2 minutes before pressing submit button</p>
        </div>
    </form>
</div>
    
	</div>
	{if $cashback_gateway_status eq 'on'}
    <div class="coupons-widget clear clearfix tab_content" id="cart_cash_coupon_options">
    <label>CASHBACK</label>
					{if $cashdivid eq 'success'}
<a onclick="javascript:removeCashCoupon();" style="padding-left:10px;cursor:pointer;">(Remove)</a>
{elseif $cashdivid eq 'error'}
{if $cashCouponMessage}
<span class="{$divid} mt10" id="discount_coupon_message">
{$cashCouponMessage}
</span>
{/if}
{/if}
    {assign var=total_amout_to_be_paid value=$totalAmount+$giftamount+$cashdiscount}
		{if $cashCouponMessage && $cashdivid eq 'success'}
			<div class="{$cashdivid} message mt10 clearfix" id="discount_cashcoupon_message">
					{$cashCouponMessage}
			</div>
		{/if}
			<form action="mkpaymentoptions.php{if $addressID&&$addressID>0 }?address={$addressID}{/if}" method="post" id="cashcouponform" name="cashcouponform" class="clearfix {if $cashCouponMessage && $cashdivid eq 'success' } hide {/if}" onsubmit="return redeemCashCoupon();">
		     <div class="coupon-block" style="margin-top: 10px;">
		                <div>
		                Your current cashback balance <b style="margin-left:10px">Rs. {if $cashcoupons.MRPAmount gt 0}{$cashcoupons.MRPAmount}{else}0{/if}</b>
				<!--button class="link-btn" style="display: inline; padding:0!important;margin-left:10px;" id="redeemCashCouponButton" type="submit" >Redeem</button-->
<a style="padding-left:10px;cursor:pointer;" id="redeemCashCouponButton" onclick="javascript:redeemCashCoupon();">Redeem</a>
		                </div>
		        		<input type="hidden" value="{$cashcoupons.MRPAmount}" id="userCashAmount" class="cb-input" name="userAmount">
						
   						{*<div class="left" style="margin-top: -4px;padding-left: 10px;padding-bottom: 5px;width:200px"> <input {if $cashcoupons}type="submit"{/if} class="redeem-btn corners right" id="redeemCashCouponButton" value="Redeem" ></div>*}

		        		<input type="hidden" value="{$cashcoupons.coupon}" id="cashcoupon" class="cb-input" name="cashcoupon">
		        		<input type="hidden" value="{$appliedCouponCode}" id="c-code" class="cb-input" name="c-code">
		     </div>
     	</form>

    	
	</div>
	{/if}
	

	
</div>
<div class="clear"></div>


