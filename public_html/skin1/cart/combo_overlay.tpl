
<script type="text/javascript">
    Myntra.Combo.isConditionMet = {$isConditionMet|@json_encode};
    Myntra.Combo.selectedStyles = {$selectedStyles|@json_encode};
    Myntra.Combo.min = parseInt({$min}+0.5);
    Myntra.Combo.ctype = "{$ctype}";
    Myntra.Combo.cartSavings = parseInt({$comboSavings}+0.5);
    Myntra.Combo.cartTotal = parseInt({$comboTotal}+0.5);
    Myntra.Combo.products = {$products|@json_encode};
    Myntra.Combo.solrProducts = {$dataMap|@json_encode};
    Myntra.Combo.isAmount = {if $productsSet.dre_buyAmount>0} true; {else} false; {/if}
    Myntra.Combo.ComboId = {$comboId};
</script>
{*$isConditionMet|@json_encode}<BR>
{$selectedStyles|@json_encode}<BR>
{$min}<BR>
"{$ctype}"<BR>
{$comboSavings}<BR>
{$comboTotal}<BR>
{{$products|@json_encode}<BR>
{$dataMap|@json_encode}<BR>}
{if $productsSet.dre_buyAmount>0} true {else} false {/if} <BR>
*}

<table class="cart-combo-overlay" style="table-layout:fixed;">
    <tr>
        <td width="30px">{$discountIcon}</td>
        <td width="380px" class="header" >&nbsp;SPECIAL COMBO OFFER! </td>
	<td width="200px">&nbsp;</td>
        <td width="90px">&nbsp;</td>
    </tr>
    <tr style="height:24px;">
        <td colspan="4"><div class="combo-header-main mt10 tool-text">{eval var=$displayText}</div></td>
    </tr>
    <tr style="height:24px;">
        <td colspan="4" class="completion-message">
    {if $isConditionMet}<span class="tick-small-icon">&nbsp;</span> Combo complete! <i>Modify Combo</i> {else}<span class="warning-small-icon">&nbsp;</span> Combo incomplete. <em>{if $productsSet.dre_buyAmount}Rs. {/if}{$min}{if $productsSet.dre_buyAmount}/-{/if} more to go!</em> {/if}</td>
    </tr>
    <tr style="height:18px;">
        <td colspan="4">&nbsp;</td>
    </tr>
    <tr style="height:14px;">
        <td colspan="4" class="pagination-count">Showing <span class="pagination-start">1</span>-<span class="pagination-end">{if $data|@count gt 5}5{else}{$data|@count}{/if}</span> of {$data|@count} products</td>
    </tr>
    <tr>
        <td colspan="4">
            <div class="content clearfix">
                {include file="cart/combo_overlay_widget.tpl"}
            </div>
        </td>
    </tr>
    <tr style="height:20px;">
        <td colspan="4">&nbsp;
        </td>
    </tr>
    <tr style="height:60px;">
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td class="combo-overlay-total">
            <div class="cart-discount left">TOTAL <span class="cost" style="color: #D20000;">Rs. <strong>{math equation="a-b" a=$comboTotal b=$comboSavings assign="comboActualTotal"}{$comboActualTotal|round|number_format:0:".":","}</strong>{if isset($comboSavings) && $comboSavings>0}<span class="strike"><b>{$comboTotal|round|number_format:0:".":","}</b></span>{/if}</span></div><BR>
            <div class="cart-discount left">SAVINGS <span style="color:#d20000"> Rs. <strong>
            {if isset($comboSavings) && $comboSavings>0}
            {$comboSavings|round|number_format:0:".":","}
            {else}
            0
            {/if}
            </strong></span></div><BR>
            {if $isConditionMet neq true}
                <div class="cart-discount left">Complete the combo to save!</div>
            {/if}
        </td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td colspan="2" class="cow-replace-item-msg">&nbsp;</td>
        <td>
            <form method="post"  action="mkretrievedataforcart.php?pagetype=productdetail&lead=recently_viewed" id="form-{$widgetdata.styleid}" class="re-form-block">
                <input type="hidden" name="_token" value="{$USER_TOKEN}" />
                <button class="add-to-cart orange-bg myntrabtn corners span-4 last combo-add-to-cart-button" style="clear:both;"> ADD COMBO TO BAG <span style="font-size:15px;">&raquo;</span></button>
            </form>
            {*<a class="add-to-cart orange-bg corners action-btn-red" href="javascript:void(0)">ADD COMBO TO BAG</a>*}
        </td>
        <td>
            <a class="combo-overlay-cancel" href="javascript:void(0)">Cancel</a>
        </td>
    </tr>
    <tr>
        <td colspan="4">
        </td>
    </tr>
</table>
