<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head>
    {include file="site/secure_header.tpl" }
    {include file="cart/conversion_trackings.tpl"}
        <script type="text/javascript">
            var orderid='{$orderid}';
        </script>
        {literal}
        <style type="text/css">
          iframe {
           display:none;
          }
        </style>
        <style>
        .order-details-new{width:550px;}
		input,img{border:0px;vertical-align:middle;}

			.ardblock{border:3px solid #f3f3f3; margin:0 0 25px 0;}
			.ardblock p{text-align:center;}
			.ardblock p span{font-size:11px; color:#ad3407; margin:10px 0px; display:block;}
			.ardblock p.fs1{background-color:#f3f3f3;color:#787878;padding:15px; margin:0px; text-align:center; font-size:12px;line-height:22px;font-weight:bold;}
			.ardblock p.fs1 strong{ font-size:20px; display:block; color:#90a628;}
			.ardblock p.fs2{background-color:#FFFFFF;color:#333333;padding:15px 0 0; margin:0px; text-align:center; font-size:11px;line-height:22px;font-weight:bold;}
			.ardblock p.fs2 strong{ font-size:18px; display:block; color:#333333;}
			.ardblock p.fs2 span{border-bottom:1px solid #9dbd2c;display:block; font-size:0px; line-height:0px;}

			.well{border:3px solid #f3f3f3; margin:0 0 25px 0;}
			.well p.welltxt{background-color:#f3f3f3;text-align:center;font-size:18px; font-weight:bold; padding:8px 0px; color:#6a6a6a}
			.well p{text-align:center;font-size:12px;padding:5px;}

			.order-details-block{overflow:hidden;}
			.order-left-block h1{display:block;float:left;font-size:18px;padding:0;margin-right:10px;}
			.order-details-block h1{display:block;font-size:18px;padding:0;margin-right:10px;}
			.order-left-block .header-link{float:left;line-height:22px;}
			.order-left-block .header-link a{color:#394b95;font-weight:bold;}
			.order-left-block{border:0px solid red;float:left;}
			.order-right-block{width:550px; border:0px solid red;float:right;}
			.order-left-block{width:420px;}

			.addblock{overflow:hidden;position:relative;margin:5px 0 0 0;background:url("{/literal}{$secure_cdn_base}{literal}/skin1/images/left-block-bg.gif") repeat-x top left scroll; border:1px solid #a5a5a5;}
			.addblock p{border:1px solid #FFF; padding:10px 0 10px 20px;}
			.addblock p strong{ display:block; }
			.addblock p span{color:#666666;display:block;font-weight:bold;padding:5px 0;width:145px; }

			.cart-opt-block{margin:5px 0 0 0;background:url("{/literal}{$secure_cdn_base}{literal}/skin1/images/left-block-bg.gif") repeat-x top left scroll; border:1px solid #a5a5a5;}
			.cart-opt-block p{border:1px solid #FFF; padding:10px 0 10px 20px;}

			table.cart-final-items tr.tableheader {background:transparent url("{/literal}{$secure_cdn_base}{literal}/skin1/myntra_images/line.png") repeat-x bottom left scroll;}
			table.cart-final-items td.carttotal{text-align:right;padding-right:5px;}
			table.cart-final-items{width:470px; font-size:11px;}
			table.cart-final-items td{font-size:12px;border-bottom:1px dotted;margin:0 0 10px;padding:5px 0 5px;text-align:center;}
			table.cart-final-items td.prodec{padding-left:5px;font-weight:bold;text-align:left;}
			table.cart-final-items td.pricelist{padding-left:25px; line-height:20px; font-weight:normal;text-align:left;}
			table.cart-final-items td.pricelisttotal{padding-right:12px;line-height:20px; font-weight:normal;text-align:right;}
			table.cart-final-items td.pricelisttotal span{border:0px solid red;display:inline-block;width:auto!important;min-width:35px;width:35px; }
			table.cart-final-items td.carttotal span{border:0px solid red;display:inline-block;width:auto!important;min-width:35px;width:35px; }
			table.cart-final-items td.cartsize ul, table.cart-final-items td.cartqty ul{list-style:none outside none;padding:0px;}

			table.cart-final-items th{
                padding:8px;
            }

			.total-block{overflow:hidden;border-bottom:dotted 1px #000;}

			.cart-total-text{width:196px; float:left; }
			.cart-total-text p{text-align:center;width:120px;}
			.cart-total-text1{width:170px; border:0px solid red; float:left; }
			.cart-total-text1 p{font-weight:bold;line-height:35px;padding-left:20px;margin:0px;}
			.cart-total-price{width:90px; border:0px solid red; float:right; }
			.cart-total-price p{line-height:25px;padding:5px 10px 5px 0px;text-align:right;font-weight:bold;margin:0px;}


			span.opt-cr-block{
				margin-bottom:8px;
				margin-left:27px;
				background-color:#FFF;
				color:#666666;
				display:block;
				font-weight:bold;
				padding:12px 5px;
				width:200px;
				border:1px solid #c9c7c7;
			}
			span.opt-cr-block input{margin:0 7px;}

			.cart-links{margin:20px 0 0 0; text-align:center;}
			.cart-links a{color:#1951a6;font-weight:bold; padding-right:15px;}
			.cart-links a:hover{text-decoration:none;color:#333333;}

			.cart-button-img{
			background-image:url("{/literal}{$secure_cdn_base}{literal}/skin1/images/cart-button-bg.png");
			background-position:left top;
			background-repeat:no-repeat;
			height:40px;
            float:left;
		}

		.cart-button-img span{
			background-image:url("{/literal}{$secure_cdn_base}{literal}/skin1/images/cart-button-bg.png");
			background-position:right -80px;
			background-repeat:no-repeat;
			margin-left:2px;
			text-align:center;
			height:40px;
			display:block;
		}
		.cart-button-img span a{
			background-image:url("{/literal}{$secure_cdn_base}{literal}/skin1/images/cart-button-bg.png");
			background-position:right -40px;
			background-repeat:repeat-x;
			color:#FFFFFF;
			display:block;
			font-family:Arial,Helvetica,sans-serif;
			font-size:18px;
			font-weight:bold;
			height:40px;
			text-decoration:none;

			padding: 0 10px;
			line-height:40px;
			margin-right:2px;
		}
		.cart-button-img span a:hover{
			text-decoration:none;
			color:#F3F000;
		}

		.linkbuttons{}
		.linkbuttons ul{list-style:none outside none;overflow:hidden; margin:5px 55px; }
		.linkbuttons ul li{display:inline; float:left;}
		.linkbuttons a.printbtn{
			background-image:url("{/literal}{$secure_cdn_base}{literal}/skin1/images/button-style2-bg.png");
			background-position:left top;
			background-repeat:repeat-x;
			border:1px solid #BC8318;
			color:#FFFFFF;
			display:block;
			font-size:12px;
			font-weight:bold;
			text-align:center;
			text-decoration:none;
			width:200px;
			margin:5px;

		}
		.linkbuttons a.printbtn:hover{color:#F3F000;}
		.linkbuttons a.printbtn span{
			border-left:1px solid #FFC079;
			border-right:1px solid #D17D58;
			border-top:1px solid #FFC079;
			display:block;
			line-height:30px;
			height:30px;
			_padding-top:8px;
		}

		.linkbuttons ul{list-style:none outside none;overflow:hidden; margin:5px 15px;padding:0;}
		.linkbuttons ul li{display:inline; float:left;}
		.linkbuttons a.style2tbtn{
			background-image:url("{/literal}{$secure_cdn_base}{literal}/skin1/images/button-style3-bg.png");
			background-position:left top;
			background-repeat:repeat-x;
			border:1px solid #708b04;
			color:#FFFFFF;
			display:block;
			font-size:12px;
			font-weight:bold;
			text-align:center;
			text-decoration:none;
			width:200px;
			margin:5px;


		}

		.linkbuttons a.style2tbtn:hover{color:#F3F000;}
		.linkbuttons a.style2tbtn span{
			border-left:1px solid #a1be2f;
			border-right:1px solid #93af25;
			border-top:1px solid #a1be2f;
			display:block;
			line-height:30px;
			height:30px;
			_padding-top:8px;
		}

		ul.reffmail{list-style:none outside none;margin:0 0 0 35px;}
		ul.reffmail li{margin:10px; font-size:13px;}
		ul.reffmail li input{ font-size:12px; width:165px;}

		.txtbox-style {
				background:url("{/literal}{$secure_cdn_base}{literal}/skin1/site-horizontal/sprite.png") no-repeat scroll -982px top transparent;
				border-color:#D3D3D3;
				border-left:0 solid #D3D3D3;
				border-style:solid;
				border-width:0 1px 1px 0;
				color:#666666;
				font-size:12px;
				height:25px;
				padding:3px 0 0 5px;
			}
		.fw-icons{ display:block; padding-top:10px;}
		.fw-icons img{margin:5px;}
		</style>
		{/literal}
		{*
		<script type="text/javascript">

        function sendCouponToFriends(mode,login){
            var req = null; var errorflag = 0; var emailList = [];
            var strEmailList ="reqType="+mode+"&";
            for(var i=1; i <= 5; i++){
                var txtBoxId = "frdemail"+i;
                var frdNameBoxId  =  "frdname"+i;
                var emails = document.getElementById(txtBoxId).value;
                var frdNames = document.getElementById(frdNameBoxId).value;
                if(emails != '' && frdNames != ""){
                    var emailArray = emails.split(",");
                    var frdNamesArray = frdNames.split(",");
                    for(var j=0; j < 1 ; j++){
                        if(emailArray[j] != ""){
                            if(!echeck(emailArray[j])){
                                document.getElementById(txtBoxId).focus();
                                return false;
                            }
                            else if(login == emailArray[j]){
                                alert("You cannot send coupon to yourself. Please remove your email address from the list"); return false;
                            }
                        }
                    }
                    strEmailList += txtBoxId+"="+emailArray[0]+"&"+frdNameBoxId+"="+frdNamesArray[0]+"&" ;
                }
                else{
                    errorflag = errorflag + 1;
                }
            }

            //added by nishith, sending orderid to track all emailids
            strEmailList +="orderid="+orderid;
            if(errorflag == 5){
                alert("Please enter your friend name(s) and EmailId(s).")
                return false;
            }
            req = (window.XMLHttpRequest) ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");
            var url = "sendAndInviteFriends.php"
            req.onreadystatechange = function(){
                if (req.readyState == 4){
                    var resp = (req.responseText).split("||");
                    if(resp[0] == "SUCCESS"){
                      for(var i=1; i <= 5; i++){
                        document.getElementById("frdname"+i).value = "";
                        document.getElementById("frdemail"+i).disabled = true;
                      }
                      alert(resp[1]);

                    }else{
                    alert(resp[1]);
                   }
                   //document.getElementById('sendCToFrd').style.display ="none";
                }
            };
            req.open('POST', url, true);
            req.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            req.send(encodeURI(strEmailList));

        }
		</script>
	*}
    </head>

    <body id="body">


        <div id="scroll_wrapper">
            <div id="container">

            {include file="site/secure_menu.tpl" }

                <div id="wrapper" >
                {include file="breadcrumb.tpl"}

                    <div class="order-details" style="padding:0px;">

							<div class="clear">&nbsp;</div>
							<div class="order-details-block">
							<div class="order-left-block">
									<div class="ardblock">
									<p class="fs1">
									{if $message}
									{$message}
									{else}
									<strong>CONGRATULATIONS! </strong>YOUR ORDER WAS PLACED SUCCESSFULLY!</p>
									{/if}
									<div>
										<p class="fs2"><strong>YOUR ORDER NUMBER IS : {$orderid}</strong>Please note the number for future reference. <span>&nbsp;</span></p>
										{if $paybyoption == "cod"}
											<p style="padding:5px 10px; text-align:left;">You have chosen to pay cash on delivery. Please have <strong>Rs. {$amountafterdiscount|string_format:"%.2f"}</strong> ready in cash at the shipping address to pay for this order on delivery.</p>
										{/if}

										{if $paybyoption!="chq"}
											<p style="padding:5px 10px; text-align:left;">Your order is being processed now and shall be shipped/dispatched from our warehouse within 24 hours. It would be delivered to you on or before {$delivery_date}.<br> You may check the status of your order in <a class="no-decoration-link" href="{$http_location}/mymyntra.php" title="My Myntra">"My Myntra"</a> page.</p>
                                            <p style={if $cashback_gateway_status eq 'on' && $cashback_tobe_given neq '' && $cashbackearnedcreditsEnabled eq '1'}"display:block;"{else}"display:none;"{/if}>{if $paybyoption == "cod"}You will receive a cashback of Rs. <b>{$cashback_tobe_given}</b> in your MyMyntra account after 30 days.{else}You will receive a cashback of Rs. <b>{$cashback_tobe_given}</b> in your MyMyntra account within the next hour.{/if}</p>

                                            <p><a href="{$http_location}" title="Continue Shopping" class="corners orange-bg" style="padding:8px;font-weight:bold;">Continue Shopping</a></p>
											{if $customerSupportCall && $customerSupportTime}
											<p><span>Have any Queries? <strong>Call: {$customerSupportCall} </strong> ({$customerSupportTime})</span></p>
											{/if}
										{/if}

									<div class="clear">&nbsp;</div>
									</div>
								</div>
								</div>


								<div class="order-right-block">

									 {* order details *}
									 <div class="order-details-new">
									 									<h1>YOUR ORDER DETAILS</h1>
								<div class="clearall" style="font-size:0px;height:8px;">&nbsp;</div>
									<table cellpadding="0" cellspacing="0" style="width:550px;margin-bottom:0;" border="0" class="cart-final-items">
										<tr class="tableheader">
											<th class="leftb">Product Description</th>
											<th>Unit Price</th>
											<th>Option</th>
											<th>Qty</th>
											<th class="rightb">Total</th>
											<th>[VAT Inclusive]</th>
										</tr>

                                          {foreach name=outer  item=product from=$productsInCart}
                                                {foreach key=key item=item from=$product}
                                                                {if $key eq 'productId'}
                                                                {assign var='productId' value=$item}
                                                                {/if}

                                                                {if $key eq 'producTypeId'}
                                                                {assign var='producTypeId' value=$item}
                                                                {/if}

                                                                {if $key eq 'productStyleId'}
                                                                {assign var='productStyleId' value=$item}
                                                                {/if}

                                                                {if $key eq 'productStyleName'}
                                                                {assign var='productStyleName' value=$item}
                                                                {/if}

																{if $key eq 'quantity'}
                                                                {assign var='quantity' value=$item}
                                                                {/if}

                                                                {if $key eq 'productPrice'}
                                                                {assign var='productPrice' value=$item}
                                                                {/if}

                                                                {if $key eq 'productTypeLabel'}
                                                                {assign var='productTypeLabel' value=$item}
                                                                {/if}

                                                                {if $key eq 'totalPrice'}
                                                                {assign var='totalPrice' value=$item}
                                                                {/if}

                                                                {if $key eq 'optionNames'}
                                                                {assign var='optionNames' value=$item}
                                                                {/if}

                                                                {if $key eq 'unitPrice'}
                                                                {assign var='unitPrice' value=$item}
                                                                {/if}

                                                                {if $key eq 'optionCount'}
                                                                {assign var='optionCount' value=$item}
                                                                {/if}

                                                                {if $key eq 'quantity_breakup'}
                                                                {assign var='quantity_breakup' value=$item}
                                                                {/if}

                                                                {if $key eq 'optionValues'}
                                                                {assign var='optionValues' value=$item}
                                                                {/if}


                                                                {if $key eq 'discount'}
                                                                {assign var='discount' value=$item}
                                                                {/if}

                                                                {if $key eq 'custAreaCount'}
                                                                {assign var='custAreaCount' value=$item}
                                                                {/if}

                                                                {if $key eq 'productStyleType'}
                                                                {assign var='productStyleType' value=$item}
                                                                {/if}
                                                                {if $key eq 'vatamount'}
                                                                {assign var='vatamount' value=$item}
                                                                {/if}
																{if $key eq 'flattenSizeOptions'}
                                                    			{assign var='flattenSizeOptions' value=$item}
                                                    			{/if}
                                                    			{if $key eq 'sizenamesunified'}
                                                    				{assign var='sizenamesunified' value=$item}
                                                    			{/if}
																{if $key eq 'cashdiscount'}
																	{assign var='cashdiscount' value=$item}
																{/if}
																 {if $key eq 'cashCouponCode'}
																	{assign var='cashCouponCode' value=$item}
																{/if}
																{if $key eq 'final_size'}
																	{assign var='final_size' value=$item}
																{/if}
																{if $key eq 'final_quantity'}
																	{assign var='final_quantity' value=$item}
																{/if}
                                                            {/foreach}

                                                <tr>
                                                    <td class="prodec" width="185">Type: {$productTypeLabel} <br>Style: {$productStyleName}</td>
                                                    <td class="uprice">{if $totalPrice gt 0}Rs {$unitPrice|string_format:"%.2f"}{else}FREE{/if}</td>
                                                    <td class="cartsize">
                                                    {if $final_size}
												    	<ul style="margin:0">{$final_size}</ul>
                                                        {else}
                                                        <ul>
                                                        	<li>NA</li>
                                                       </ul>
                                                    {/if}
                                                    </td>
                                                    <td class="cartqty">
                                                    	<ul  style="margin:0">
														{if $final_quantity}
															<li>{$final_quantity}</li>
														{else}
                                                       		<li>{$quantity}</li>
                                                       	{/if}
                                                        </ul>
                                                    </td>
                                                    <td class="carttotal">{if $totalPrice gt 0}Rs <span>{$totalPrice|string_format:"%.2f"} </span>{else}FREE{/if}</td>
                                                    <td class="vat">{if $totalPrice gt 0}[Rs {$vatamount}]{else}[NA]{/if}</td>
                                                </tr>

                                        {/foreach}

										<tr>
											<td class="prodec" width="185">&nbsp;</td>
											<td class="pricelist" colspan="3">Amount: <br> Discount: <br>{if $cashCouponCode && $totalcashdiscount gt 0} Mynt Credits: <br>{/if}Shipping cost: {*<br> Gift Packing charges:*}<br>{if $emi_charge && $emi_charge gt 0}EMI Charges: {/if}<br>{if $codCharges && $codCharges gt 0}COD Charges: {/if}</td>

											<td class="pricelisttotal" colspan="2">
											Rs&nbsp;<span>{$grandTotal|string_format:"%.2f"}</span><br>
											Rs&nbsp;<span>{$totalDiscount|string_format:"%.2f"}</span><br>
											{if $cashCouponCode && $totalcashdiscount gt 0} Rs&nbsp;<span>{$totalcashdiscount|string_format:"%.2f"}</span><br>{/if}
											Rs&nbsp;<span>{$shippingRate|string_format:"%.2f"}</span><br>
											{if $emi_charge && $emi_charge gt 0} Rs&nbsp;<span>{$emi_charge|round|string_format:"%.2f"}</span><br>{/if}
											{if $codCharges && $codCharges gt 0} Rs&nbsp;<span>{$codCharges|round|string_format:"%.2f"}</span><br>{/if}
											{*Rs<span id="gift_charges">{$giftamount|string_format:"%.2f"}</span><br>*}

											</td>
										</tr>

									</table>

									<div class="total-block" style="_height:30px;">
										<div class="cart-total-text"><p>&nbsp;</p></div>
										<div class="cart-total-text1"><p>Final Amount {if $paybyoption eq 'cod'}to be{/if} paid</p></div>
										<div class="cart-total-price"><p>Rs <span id="total_charges">{$amountafterdiscount|string_format:"%.2f"}</span></p></div>
									</div>
									</div>

								</div>
															</div>


					</div>

                        <div class="clearall" style="_height:50px">&nbsp;</div>

                    </div>
                {include file="site/secure_footer.tpl" }
                </div>
            </div>
        {* analytics *}
        </body>
       {if $fireSyncGACall}
			<script type="text/javascript" src="https://ssl.google-analytics.com/ga.js"></script>
	   {/if}
 
        <script type="text/javascript">
        try {ldelim}
        	//var pageTracker = _gat._getTracker("{$ga_acc}");
        	_gaq.push(['_setCustomVar',
        	//pageTracker._setCustomVar(
                    1,
                    "Already-Bought",
                    "Yes",
                    1]);

        	{*if $payment_method neq 'chq'*}
				_gaq.push(['_addTrans',
				"{$orderid}",                                     // Order ID
				"{$userinfo.s_firstname} {$userinfo.s_lastname}", //cust name
				"{$amountafterdiscount|string_format:'%.2f'}",	  // Total
				"{$vat}",                                     	  // Tax
				"{$shippingRate}",                                // Shipping
				"{$userinfo.s_city}",                             // City
				"{$userinfo.s_state}",                            // State
				"{$userinfo.s_country}"]                              // Country
				);
			{*/if*}

			{assign var='loopIndex' value=0}
        	{assign var='netTotalPrice' value=0}
        	{assign var='productidDelimString' value=''}
        	{assign var='productTypeDelimString' value=''}
			{assign var='totalQuantity' value=0}

			{***products detail for my things conversion***}
			var myThingsProducts = new Array();

        	{foreach name=outer  item=product from=$productsInCart}
        		{foreach key=key item=item from=$product}
        			{if $key eq 'productId'}
        			{assign var='productId' value=$item}
        			{assign var='productidDelimString' value=$productidDelimString|cat:$productId|cat:"|"}
        			{/if}

        			{if $key eq 'unitPrice'}
            		{assign var='unitPrice' value=$item}
            		{/if}

        			{if $key eq 'productStyleName'}
        			{assign var='productStyleName' value=$item}
        			{/if}

        			{if $key eq 'quantity'}
        			{assign var='quantity' value=$item}
        			{assign var='totalQuantity' value=$quantity+$totalQuantity}
        			{/if}

        			{if $key eq 'discount'}
                    {assign var='discount' value=$item}
                    {/if}

                    {if $key eq 'coupon_discount'}
                    {assign var='coupon_discount' value=$item}
                    {/if}

        			{if $key eq 'productTypeLabel'}
        			{assign var='productTypeLabel' value=$item}
        			{assign var='productTypeDelimString' value=$productTypeDelimString|cat:$productTypeLabel|cat:"|"}
        			{/if}

        		  	{if $key eq 'productCatLabel'}
					{assign var='productCatLabel' value=$item}
					{/if}

        			{if $key eq 'totalPrice'}
        			{assign var='totalPrice' value=$item}
        			{assign var='netTotalPrice' value=$netTotalPrice+$totalPrice}
					{/if}

					{***added for tyroo conversion tracking***}
					{if $key eq 'productPrice'}
						{assign var='productPrice' value=$item}
						{*assign var='itemString' value=":prod:$productPrice:qty:$quantity"*}
						{*assign var='lineItemString' value=$lineItemString|cat:$itemString*}
        			{/if}
        			{***tyroo till here***}

        		{/foreach}
				{assign var='itemString' value=":prod:$productPrice:qty:$quantity"}
				{assign var='lineItemString' value=$lineItemString|cat:$itemString}
        		{assign var='totalDiscountOnItems' value=$coupon_discount}
        		{assign var='discountPerItem' value=$totalDiscountOnItems/$quantity}
        		{assign var='unitPriceAfterDiscount' value=$unitPrice-$discountPerItem}



				{*if $payment_method neq 'chq'*}
					_gaq.push(['_addItem',
					"{$orderid}",			// Order ID
					"{$productId}",         //pid
					"{$productStyleName}",  //style
					"{$productCatLabel}",   //articleType|Brand
					"{$unitPriceAfterDiscount}",        //unit price
					"{$quantity}"           // Quantity
					]);
				{*/if*}

				{***products detail for my things conversion	***}
				myThingsProducts[{$loopIndex}] = {ldelim}id: "{$productId}",price:"{$productPrice|number_format:'2':'.':''}",qty:"{$quantity}"{rdelim};

				{assign var='loopIndex' value=$loopIndex+1}
            {/foreach}

            	{***tyroo conversion tracking***}
			{if $lineItemString|strlen > 40}
				{assign var=lineItemString value=":prod:$netTotalPrice:qty:1"}
			{/if}

            {*if $payment_method neq 'chq'*}
				_gaq.push(['_trackTrans']);
				{if $recordGACall}
					{literal}
                    	_gaq.push(function(){
                        	var newdiv = document.createElement('img');
                            var newGASrc= https_loc+"/baecon/{/literal}{$orderid}{literal}";
                            newdiv.setAttribute("src",newGASrc);
                            newdiv.setAttribute("width","1");
                            newdiv.setAttribute("height","1");
                            newdiv.setAttribute("alt","");
                            document.appendChild(newdiv);
                    	});
                	{/literal}
				{/if}
            // Yahoo Conversion Tracking
            /*window.ysm_customData = new Object();
            window.ysm_customData.conversion = "transId={$orderid},currency=INR,amount={$amountafterdiscount|string_format:'%.2f'}";
            var ysm_accountid  = "1GJC30EFMMKEBN6R4ASQ2SBNQCG";
            document.write("<SCR" + "IPT language='JavaScript' type='text/javascript' "
            + "SRC=https://" + "srv2.wa.marketingsolutions.yahoo.com" + "/script/ScriptServlet" + "?aid=" + ysm_accountid
            + "></SCR" + "IPT>");*/

            {*/if*}
        {rdelim} catch(err) {ldelim}{rdelim}
        {****AVAIL ****}
        	 	avail_ids=[{$avail_ids}];
		        avail_prices=[{$avail_prices}];
		        AVAIL_USER_ID='{$AVAIL_USER_ID}';
		        AVAIL_ORDER_ID='{$orderid}';
		        {literal}
		        $(window).load(function(){
		        	$.getScript("https://service.sg.avail.net/2009-02-13/dynamic/54ed2a6e-d225-11e0-9cab-12313b0349b4/emark.js",function(){
		        		var emark = new Emark();
		        		emark.logPurchase(AVAIL_USER_ID, avail_ids, avail_prices,AVAIL_ORDER_ID);
		        		emark.commit();
			    	});
		        });
		        {/literal}
		{****AVAIL ****}
		</script>

		<!--my things conversion tracking-->
        <script type="text/javascript">
        function _mt_ready(){ldelim}
            if (typeof(MyThings) != "undefined") {ldelim}
                MyThings.Track({ldelim}
                    EventType: MyThings.Event.Conversion,
                    Action: "9902",
                    Products:myThingsProducts,
                    TransactionReference: "{$orderid}",
                    TransactionAmount: "{$netTotalPrice|number_format:'2':'.':','}"
                {rdelim});
            {rdelim}
        {rdelim}
        var mtHost = (("https:" == document.location.protocol) ? "https://rainbowx" : "http://rainbow") + ".mythings.com";
        var mtAdvertiserToken = "1428-100-in";
        document.write(unescape("%3Cscript src='" + mtHost + "/c.aspx?atok="+mtAdvertiserToken+"' type='text/javascript'%3E%3C/script%3E"));
        </script>
		<!--my things conversion tracking-->


		{if $trackingCookie.aff.trackstatus eq 1}
			<!--tyroo conversion tracking-->
			{assign var=categoryForTyroo value=$highestPriceArticleType}
			<script type="text/javascript" src="https://affiliates.tyroodr.com/i_sale_third/82/{$lineItemString}/{$orderid}/,,{$userinfo.s_city},{$paymentoption},{$categoryForTyroo}"></script>
			<noscript><img src="https://affiliates.tyroodr.com/i_track_sale/82/{$lineItemString}/{$orderid}/,,{$userinfo.s_city},{$paymentoption},{$categoryForTyroo}"></noscript>
			<!--tyroo conversion tracking-->
		{/if}
		{assign var='aff_ibibo' value='aff-ibibo'}
		{if $trackingCookie.$aff_ibibo.trackstatus eq 1}
        <div id='m3_tracker_324' style='position: absolute; left: 0px; top: 0px; visibility: hidden;'>
	        <img 
	        	src='http://ads.ibibo.com/ad/www/delivery/ti.php?trackerid=324&amp;TransactionID={$orderid}&amp;sale_amt={$netTotalPrice|number_format:'2':'.':''}&amp;items_no={$totalQuantity}&amp;cb={$random_integer}' 
	        	width='0' height='0' alt='' /></div>
        {/if}
		{assign var='aff_dgm' value='aff-dgm'}	
        {if $trackingCookie.$aff_dgm.trackstatus eq 1}
        <img height="1" width="3" src="http://www.s2d6.com/x/?x=sp&h=18206&o={$orderid}&g=transaction&s={$netTotalPrice|number_format:'2':'.':''}&q={$totalQuantity}&e1=none&e2=none&e3=none&e4=none" />
		{/if}

        {assign var='aff_ozone' value='aff-ozone'}
        {if $trackingCookie.$aff_ozone.trackstatus eq 1}
            <script type="text/javascript" src="https://secure.adnxs.com/px?id=37048&amp;order_id={$orderid}&amp;value={$netTotalPrice|number_format:'2':'.':''}&amp;t=1"></script>
            <noscript><img src="https://secure.adnxs.com/px?id=37048&order_id={$orderid}&value={$netTotalPrice|number_format:'2':'.':''}&t=1" border="0" height="1" width="1"></noscript>
        {/if}

        {assign var='aff_omg' value='aff-omg'}
        {if $trackingCookie.$aff_omg.trackstatus eq 1}
            <img border="0" height="1" width="1" src="https://track.in.omgpm.com/apptag.asp?APPID={$orderid}&MID=349836&PID=9640&status={$netTotalPrice|number_format:'2':'.':''}">
        {/if}

        {assign var='aff_clove' value='aff-clove'}
        {if $trackingCookie.$aff_clove.trackstatus eq 1}
           <iframe src="https://paypixy.com/livetracking_pixel.php?cid=1012&id=&txamount={$netTotalPrice|number_format:'2':'.':''}&txadvid={$orderid}&saleadvid={$orderid}" frameborder="0" height="1" width="1" ></iframe> 
        {/if}

		<script language="javascript" type="text/javascript">
        // Mediamind specific code starts
        var ebRev = '{$netTotalPrice}';
        var ebOrderID = '{$orderid}';
        var ebProductID = '{$productidDelimString}';
        var ebProductInfo = '{$productTypeDelimString}';
        var ebRand = Math.random()+'';
        ebRand = ebRand * 1000000;
        document.write('<scr'+'ipt src="HTTPS://bs.serving-sys.com/BurstingPipe/ActivityServer.bs?cn=as&amp;ActivityID=112558&amp;rnd=' + ebRand + '&amp;Value='+ebRev+'&amp;OrderID='+ebOrderID+'&amp;ProductID='+ebProductID+'&amp;ProductInfo='+ebProductInfo+'"></scr' + 'ipt>');
        // Mediamind specific code ends
        // Admagnet specific code start
        //var p=location.protocol=='https:'?'https://n.admagnet.net/c/js/':'http://n.admagnet.net/c/js/';
        //var r=Math.floor(Math.random()*999999);
        //document.write ("<" + "script language='JavaScript' ");
        //document.write ("type='text/javascript' src='"+p);
        //document.write ("?trackerid=447&amp;r="+r+"'><" + "/script>");
        // Admagnet specific code ends
        </script>
        <!-- Begin Ozone media -->
        <script language="JavaScript" type="text/javascript">
        var zzp=new Image();
        if (location.protocol == "https:") {ldelim}
            zzp.src="https://tt1.zedo.com/ads2/t?o=332743;h=1124786;z="+Math.random();
        {rdelim} else {ldelim}
            zzp.src="http://yads.zedo.com/ads2/t?o=332743;h=1124786;z="+Math.random();
        {rdelim}
        </script>
        <script src="https://ad.yieldmanager.com/pixel?id=1241751&t=1" type="text/javascript"></script>
        <!-- End Ozone media -->
        {* analytics *}
<script type="text/javascript">
var axel = Math.random() + "";
var a = axel * 10000000000000;
document.write('<img src="https://ad.doubleclick.net/activity;src=2994863;type=pagev820;cat=thank605;ord=' + a + '?" width="1" height="1" alt=""/>');
</script>
<noscript>
<img src="https://ad.doubleclick.net/activity;src=2994863;type=pagev820;cat=thank605;ord=1?" width="1" height="1" alt="">
</noscript>

<!--retarget pixel tracking-->
{if $retargetPixel.confirmHttps}
	<script type="text/javascript">
		$(window).load(function () {ldelim}
			$('#retargetSpan').html("{$retargetPixel.confirmHttps}");
		{rdelim});
	</script>
	<span id="retargetSpan" style="display:none;"></span>
{/if}
<!--retarget pixel tracking-->

<!--post click Tracking Pixel Code for tyroo ad network-->
<!--script type="text/javascript">
    $(window).load(function () {*ldelim*}
        imagePixel = $('<img src="https://ads1.tyroo.com/pcsale/3.0/1067/0/0/0/BeaconId=10519;rettype=img;subnid=1;SalesValue=0.00;;" width="1" height"1" style="display:none;">');
        $("body").append(imagePixel);
    {*rdelim*});
</script-->
<!--post click Tracking Pixel Code for tyroo ad network-->

</body>
</html>
