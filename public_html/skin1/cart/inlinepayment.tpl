{include file="site/doctype.tpl"}
<head>
{include file="site/secure_header.tpl" }
</head>
<body class="{$_MAB_css_classes}">
<div class="container clearfix">
    {include file="site/secure_menu.tpl" }
		<div class="divider">&nbsp;</div>
		{if $transaction_status && !$vbvfail}
			<div class="clear">&nbsp;</div>
			<div class="access-block" >
				<div class="access-error"> <img src="{$secure_cdn_base}/skin1/images/no-access.jpg" style="border:0px";> YOUR TRANSACTION HAS FAILED</div>
				<div style="line-height:18px">
					<div>Dear Customer,</div>

				<br/>Your transaction has failed. Please relax and try to place the order again ensuring that: <br>
				<p style="margin-top:5px;">
				1. Card Information/Netbanking credentials provided is accurate.<br>
				2. Your internet connection is not disrupted during the payment process<br>
				</p>
				{if $codErrorCode eq 0}
					<b>You can also pay for this order by Cash on Delivery.</b><br>
					<br>
				{/if}
				<p style="padding:0px;margin:5 0px;text-align: justify;">
				In case your account has been debited, the same amount will be automatically rolled back within 24 hrs. <br><br>
				Have a query? Our Customer Champions are available {if $customerSupportTime} ({$customerSupportTime}) {/if} and will be glad to help you with completing this order or answer any other questions that you may have. You can reach our Customer Champion at {if $customerSupportCall} {$customerSupportCall} or {/if} <a href="mailto:support@myntra.com" target="_blank">support@myntra.com.</a>
				</p>
				</div>
			</div>
		{elseif $vbvfail}
			<div class="clear">&nbsp;</div>
			<div class="access-block" >
				<div class="access-error"> <img src="{$secure_cdn_base}/skin1/images/no-access.jpg" style="border:0px";> SECURE VERIFICATION OF YOUR CARD FAILED</div>
				<div style="line-height:18px">
					<div>Dear Customer,</div>
				<br/>
				Secure verification for your card has failed. Please ensure that: <br>
				<p style="margin-top:5px;">
				1. Card information entered by you is accurate i.e. Card Number, Valid Thru, CVV & Card Holders Name.<br>
				2. PIN/User Name/Password entered on Secure Verification Page are correct.<br>
				</p>
				{if $bankVBVLink}
					In case you are not registered for Secure Verification, you may do so by following the instructions <a href="{$bankVBVLink}" onClick="_gaq.push(['_trackEvent', 'payment_page', '3dregistration', '{$bankName}']);return true;" target="_blank">here</a>.<br>
				{else}
					In case you are not registered for Secure Verification, please contact your Bank.<br>
				{/if}
				{if $codErrorCode eq 0}
					<br>
					<b>You can also pay for this order by Cash on Delivery.</b><br>
				{/if}
				<br>
				<p style="padding:0px;margin:5 0px;text-align: justify;">
				Have a query? Our Customer Champions are available {if $customerSupportTime} ({$customerSupportTime}) {/if} and will be glad to help you with completing this order or answer any other questions that you may have. You can reach our Customer Champion at {if $customerSupportCall} {$customerSupportCall} or {/if} <a href="mailto:support@myntra.com" target="_blank">support@myntra.com.</a>
				</p>
				</div>
			</div>
		{/if}
		{if $citi_discount > 0}
			<div id="citi_discount_div" style="text-align: center;font-weight: bold;padding: 5px 0px 5px 0px; "> Avail {$citi_discount}% discount by paying with your Citibank <a href="#pay_by_dc" onClick="$('#pay_by_dc').click();return false;">Debit Card</a></div>
		{/if}
		{if $icici_discount > 0}
			<div id="icici_discount_div" style="text-align: center;font-weight: bold;padding: 5px 0px 5px 0px; "> Avail {$icici_discount}% discount on payment through <img alt="ICICI" src="{$secure_cdn_base}/skin1/images/ICICI.png"> (credit card, debit card or netbanking)</div>
		{/if}
        <div class="span-24 content-left last left mt10">
             <div class="pay-container">

                <div class="tab3 clearfix">
                {*Redeem*}
                <div class="grand-total left" style="width: 420px;">
                	<span class="my-order-title"><b>Grand Total:</b></span>
                	<span class="my-order-rs" style="margin-left: 4px;">
                		<b>Rs.  <span class="total_charges">{$totalAmount+$giftamount|round|number_format:0:".":","}</span></b>
                	</span>

                </div>
                <div class="right">
                	<!--PORTAL-667 Configurable Order of payment options-->
                	{if $totalAmount eq 0}
                		{assign var="paymentType" value="credit_card"}
                	{else}
                		{assign var="paymentType" value="$defaultPaymentType"}
                	{/if}
                	<input type=hidden id="paymentFormToSubmit" value="{$paymentType}">
                	<a href="#paybtn" {if $totalAmount eq 0} onclick="return submitPaymentForm(false);" {elseif $is_icici_gateway_up} onclick="return submitPaymentForm(true);" {else} onclick="return submitPaymentForm(false);" {/if} class="orange-bg corners cart-btn fbtn submitButton">
                		 {if $totalAmount eq 0}
                		 	<b>Place the Order</b>
                         {elseif $is_icici_gateway_up}
                         	{if $paymentType == "cod" }
                         		<b>Place The Order</b>
                         	{else}
                         		<b>Pay Now</b>
                         	{/if}
                         {else}
                         	<b>Proceed to Payment</b>
                         {/if}
                    </a>
                </div>

                {*Redeem@End*}
                </div>
                    {include file="cart/coupon_widget.tpl"}
                {*PyamentBlock*}
                    <div class="pay-block" id="vtab">
                        {*vertical Tabs*}
                        <div class="vNav">
                            <ul>
                            	{if $totalAmount eq 0}
                            		<li class="selected">Place Your Order</li>
                            		{foreach from=$payment_options item=pmt_option}
                            			{if $pmt_option eq "credit_card"}
                            				<li class="payment-option-disabled">Pay by Credit Card</li>
                            			{elseif $pmt_option eq "emi"}
                            				<li class="payment-option-disabled">Pay by EMI (Credit Card)</li>
                            			{elseif $pmt_option eq "debit_card"}
                            				<li class="payment-option-disabled">Pay by Debit Card</li>
                            			{elseif $pmt_option eq "net_banking"}
                            				<li class="payment-option-disabled">Pay with Net Banking</li>
                            			{elseif $pmt_option eq "cod"}
                            				<li class="payment-option-disabled">Cash on Delivery</li>
                            			{elseif $pmt_option eq "pay_by_phone"}
                            				<li class="payment-option-disabled">Pay by Phone</li>
                            			{/if}
									{/foreach}
                                {else}
                            		{foreach from=$payment_options item=pmt_option}
	                                	{if $pmt_option eq "credit_card"}
	                                		<li id="pay_by_cc" {if $defaultPaymentType eq "credit_card"} class="selected" {/if}>Pay by Credit Card</li>
                            			{elseif $pmt_option eq "emi"}
                            				<li id="pay_by_emi" {if $defaultPaymentType eq "emi"} class="selected" {/if}>Pay by EMI (Credit Card)</li>
	                                	{elseif $pmt_option eq "debit_card"}
	                                		<li id="pay_by_dc" {if $defaultPaymentType eq "debit_card"} class="selected" {/if}>Pay by Debit Card</li>
	                                	{elseif $pmt_option eq "net_banking"}
	                                		<li id="pay_by_nb" {if $defaultPaymentType eq "net_banking"} class="selected" {/if}>Pay with Net Banking</li>
	                                	{elseif $pmt_option eq "cod"}
	                                		<li id="pay_by_cod" {if $defaultPaymentType eq "cod"}  class="selected" {/if}>Cash on Delivery</li>
	                                	{elseif $pmt_option eq "pay_by_phone"}
                            				<li id="pay_by_phone" {if $defaultPaymentType eq "cod"}  class="selected" {/if}>Pay by Phone</li>
                            			{/if}
	                                	<!--li>Pay with Cash Card</li-->
	                                {/foreach}
                                 {/if}
                            </ul>
                        </div>
                        {*vertical Tabs@End*}

                        {*vertical Tabs content*}
                        <div id="payment_form">
                        		{if $totalAmount eq 0}
                        			<form action="{$https_location}/mkorderinsert.php" method="post" id="credit_card" style="display: block;">
										<input type="hidden" name='s_option' value ='{$logisticId}'/>
										<!--delivery preference-->
										<input type="hidden" name="shipment_preferences" value="shipped_together" >
										<!--delivery preference-->
										<input type="hidden" name="is_shipping_cost_set" value="{$s_city}">
										<input type="hidden" name="checksumkey" value="{$checksumkey}">
										<input type="hidden" name="address" value="{$address.id}">
										<input type="hidden" value="creditcards" name="pm" />
										<span>Please click on 'Place the Order' to proceed with your order.<br/></span>
									</form>
                        		{/if}

                        		{foreach from=$payment_options item=pmt_option}
                        			{if $pmt_option eq "credit_card"}
                        				{include file="cart/form_pmt_creditcard.tpl"}
                        			{elseif $pmt_option eq "emi"}
                        				{include file="cart/form_pmt_emi.tpl"}
                        			{elseif $pmt_option eq "debit_card"}
                        				{include file="cart/form_pmt_debitcard.tpl"}
                            		{elseif $pmt_option eq "net_banking"}
                            			{include file="cart/form_pmt_netbanking.tpl"}
                            		{elseif $pmt_option eq "cod"}
                            		    {if $codWindow eq "displayCaptchaScreen"}
                            				{include file="cart/form_pmt_cod.tpl"}
                            			{else}
                            				{include file="cart/form_pmt_mobile_verify.tpl"}
                            			{/if}
                            		{elseif $pmt_option eq "pay_by_phone"}
                            			{include file="cart/form_pmt_pbp.tpl"}
                            		{/if}
								{/foreach}

                        </div>
 		                {*vertical Tabs content@End*}
                    </div>
                    <div class="tab3 mt10 clearfix">
                    	<div class="right">
                			<a href="#paybtn" {if $totalAmount eq 0} onclick="return submitPaymentForm(false);" {elseif $is_icici_gateway_up} onclick="return submitPaymentForm(true);" {else} onclick="return submitPaymentForm(false);" {/if} class="orange-bg corners cart-btn fbtn submitButton">
		                		 {if $totalAmount eq 0}
		                		 	<b>Place the Order</b>
		                         {elseif $is_icici_gateway_up}
		                         	{if $paymentType == "cod" }
		                         		<b>Place The Order</b>
		                         	{else}
		                         		<b>Pay Now</b>
		                         	{/if}
		                         {else}
		                         	<b>Proceed to Payment</b>
		                         {/if}
                    		</a>

		           </div>
                </div>
                {*PyamentBlock@End*}
        </div>
        </div>
        <div class="content-right-block right last">
        	<div class="order-left-block">
				<div class="adds-box">
            		<div><span class="h1-title">SHIPPING ADDRESS</span><span class="header-link"><a onclick="_gaq.push(['_trackEvent', 'payment_page', 'change_shipping_address'])" href="{$https_location}/mkcustomeraddress.php?change=true">(Change Shipping Address)</a></span></div>
                    <div class="addblock">
	                    <p>
							<span id="customer-shipping-name"><strong>{$address.name}</strong></span>
							<span id="customer-shipping-address">{$address.address}</span>
							<span id="customer-shipping-locality">{$address.locality}</span>
							<span id="customer-shipping-city" style="display:inline;">{$address.city}</span>, <span id="customer-shipping-state" style="display:inline;">{$address.statename}</span><br/>
							<span id="customer-shipping-country" style="display:inline;">{$address.countryname}</span>, <span id="customer-shipping-pincode" style="display:inline;">{$address.pincode}</span><br/>
							<span style="padding-top:10px !important;"><b>Mobile:</b> {$address.mobile}</span>
	                    </p>
						{if $address.countryname neq 'India'}
								<div class="int-shipping">
									<strong>Disclosures for International Shipping.</strong>
									<span>All shipments sent to countries outside India are international shipments and may be subject to import duties or fees levied by the country of import. All these duties and fees are the liability of the importer or recipient and Myntra is not liable to pay any such charge. Please contact the local customs office of the importing country for more information.</span>
								</div>
						{/if}
	                </div>
				</div>

                <!-- div class="cart-summary">
                    <span class="h1-title uparrow" id="order-summary">ORDER SUMMARY</span>
                    <div class="verify-loading" id="cart-pre-loading" style="display:none; margin-right: 100px;"></div>
                       <ul class="payto-cart-items hide" id="order-cart-items">

                       </ul>
                </div -->
			</div>
             <div class="section-heading">&nbsp;</div>
             <div class="order-items"></div>
             <div class="order-totals">
                <ul>
                	<li class="last-list"><span class="my-order-title">Order SubTotal</span><span class="my-order-rs">Rs.  {$amount|round|number_format:0:".":","}</span></li>
                	{if $shippingRate neq 0}
						<li class="last-list"><span class="my-order-title">Shipping</span><span class="my-order-rs">Rs.  {$shippingRate|round|number_format:0:".":","}</span></li>
					{/if}
					<li class="last-list hide emi-charges"><span class="my-order-title">EMI Processing Fee</span><span class="my-order-rs value">{if $emi_charge > 0}
		                Rs. {$emi_charge|round|number_format:0:".":","}
		            {else}
		                FREE
		            {/if}</span></li>
					{if $productDiscount neq 0 || $cartLevelDiscount neq 0}
                	    <li class="last-list"><span class="my-order-title">Special Discount</span><span class="my-dis-rs">Rs.  -{math equation="a+b" a=$productDiscount b=$cartLevelDiscount assign="totalDiscount"}{$totalDiscount|round|number_format:0:".":","}</span></li>
                	{/if}
                	{if $coupondiscount neq 0}
                		<li class="last-list"><span class="my-order-title">Coupon Discount</span><span class="my-dis-rs">Rs.  -{$coupondiscount|round|number_format:0:".":","}</span></li>
                	{/if}
                	{if $cashdiscount neq 0}
                		<li class="last-list"><span class="my-order-title">Cashback</span><span class="my-dis-rs">Rs.  -{$cashdiscount|round|number_format:0:".":","}</span></li>
                	{/if}
                    {*<li class="last-list"><span class="my-order-title">Gift:</span><span class="my-order-rs" id="gift_charges">Rs.  {if isset($giftamount)}$giftamount{else}0.00{/if}</span></li>*}
					{if $citi_discount > 0 || $icici_discount > 0}<li class="last-list" style="display:none" id="additional_discount"><span class="my-order-title">Additional Discounts</span><span class="my-dis-rs" id="discounts_section">Rs.  0</span></li>{/if}
					{if $cod_charge > 0}<li class="last-list" style="display:none" id="cod_additional_charge"><span class="my-order-title">COD Charges</span><span class="my-order-rs" id="cod_charge_section">Rs.  0</span></li>{/if}
                    <li class="grand-total"><span class="my-order-title"><b>Grand Total:</b></span><span class="my-order-rs"><b>Rs.  <span class="total_charges" data-original-value="{$totalAmount+$giftamount|round|number_format:0:'.':','}">{$totalAmount+$giftamount|round|number_format:0:".":","}</span></b></span></li>
                </ul>
             </div>
             <div class="cashbackblock" style={if $cashback_gateway_status eq 'on' && $cashback_tobe_given && $cashbackearnedcreditsEnabled eq '1'}"display:block;"{else}"display:none;"{/if}>
             	{*assign var=total_amout_to_be_paid value=$totalAmount+$giftamount+$cashdiscount*}
             		<div class="order-totals" style="margin-top:10px;overflow: hidden;" >You are eligible for a cashback credit of <b style="color:#D20000">Rs. {$cashback_tobe_given|round|number_format:0:".":","}

             			</b> on successful completion of this order <br> <a class="no-decoration-link right" id="lmore">Learn more</a>
             			<p class="lmore hide">
	        			You will get 10% of the post-discount value of the transaction credited to your Mynt credits account after realization of your payment. Online payments will receive their Cashback credit within an hour after the order has been placed. For payments made through Cash-on-Delivery, Cashback will be credited 30 days after the date of the order. <a href="{$http_location}/myntclub" target="_blank">Click here</a>
	     				</p>
             		</div>
			 </div>
             <div class="order-totals">
             	<img src="{$secure_cdn_base}/skin1/images/secure-shopping2.png"/>
             </div>
            </div>

        <hr class="space clear mb10">
		<div id="myOnPageContent" style="display:none;">
			<table cellpadding="0" cellspacing="0" width="100%" class="my-c-table" style="margin-top:10px;border:1px solid #ccc;">
    			<tbody>
        			<tr>
            			<th>Select</th>
            			<th>Coupon code</th>
            			<th>coupon description</th>
            			<th>Expires on</th>
            			<!-- <th>Discount on Cart Contents</th> -->
        			</tr>
        			{section name=prod_num loop=$rest_coupons}
        				<tr class="even">
	            			<td><input name="select" type="radio" onClick="javascript:redeemCoupon2('{$rest_coupons[prod_num].coupon}');"> </td>
	            			<td>{$rest_coupons[prod_num].coupon}</td>
	            			<td>{$rest_coupons[prod_num].description}</td>
	            			<td class="td-cr">{$rest_coupons[prod_num].expire|date_format:$config.Appearance.date_format}</td>
	            			<!-- <td class="td-last td-rs">170.00</td> -->
        				</tr>
        			{/section}

    		</tbody>
		</table>
	</div>
    <div class="divider">&nbsp;</div>
	{include file="site/secure_footer_content.tpl"}
</div>

<!-- commenting it out so that not to use pull method mobile verification
<div id="splash-mobile-verify" class="lightbox">
<div class="mod">

    <div class="hd">
        <h2>VALIDATE MOBILE NUMBER</h2>
    </div>
    <div class="bd clearfix">
        <div class="verify-sms">
            Please send as SMS* within the <strong>10 minutes</strong> from your mobile number (<strong>+91 <span id='userMobile'>{$userMobile}</span></strong>) with the text:<br>
            <strong>VERIFY</strong><br>
            to this number:<br>
            <strong>0-92824-24444</strong>
        </div>
        <i class="mnote">* Standard SMS charges apply, as per your current service provider</i>

        Once you send the SMS, your number will be validated in the next <strong>120 seconds.</strong><br>
        In case you want to validate another number, please edit your profile to update your number before trying again.<br>
        <button class="action-btn right ok-btn" type="button">OK</button>
    </div>
</div>
</div>
-->

{* Do not change id of any div below it is used in orderProcessing.tpl *}
<div id="splash-payments-processing" class="lightbox">
<div class="mod">
    <div class="hd" id="splash-payments-processing-hd">
        <h2>Processing Payments</h2>
    </div>
    <div class="bd clearfix" id="splash-payments-processing-bd">
    	<center><img src="{$secure_cdn_base}/images/image_loading.gif"></center>
        <div class="processing-payments" id="splash-payments-processing-bd-pp">
            Enter your card/bank authentication details in the new window once it appears on your screen. <br/>
			Please do not refresh the page or click the back button of your browser.<br/>
			You will be redirected back to order confirmation page after the authentication. It might take a few seconds.<br/><br/>
        </div>

        <center id="cancel-payment-btn"><button class="action-btn cancel-payment-btn" type="button">Cancel Payment</button></center>
    </div>
</div>
</div>


<script type="text/javascript">
	var total_amount = 	{$totalAmount};
	var iciciMinEMITotalAmount = "{$iciciMinEMITotalAmount}";
	var shipping_countrycode = "{$address.country|escape:'html'}";
	var codErrorCode = {$codErrorCode};
	var codPincode = '{$address.pincode}';
	var codTabClicked = false;
	{literal}
	var toggle_text_elements = {};
	{/literal}
	var loginid = "{$loginid}";
	var citi_discount = {$citi_discount};
	var icici_discount = {$icici_discount};
	var icici_discount_text = "Avail {$icici_discount}% discount on payment through <img alt='ICICI' src='{$secure_cdn_base}/skin1/images/ICICI.png'> (credit card, debit card or netbanking)";
	var oneClickCod = {$oneClickCod};
	{if $is_icici_gateway_up}
	var is_icici_gateway_up = true;
	{else}
	var is_icici_gateway_up = false;
	{/if}
	{if $shippingRate neq 0}
		{assign var=amountToDiscount value=$totalAmount-$shippingRate}
		var amountToDiscount = {$amountToDiscount};
	{else}
		var amountToDiscount = {$totalAmount};
	{/if}

	{if $citi_discount>0}
		var	citiCardArray = new Array();
		{foreach from=$citi_discount_bins item=array_item key=id}
			citiCardArray.push('{$array_item}');
		{/foreach}
	{/if}

	{if $icici_discount>0}
		var iciciCardArray = new Array();
		{foreach from=$icici_discount_bins item=array_item key=id}
			iciciCardArray.push('{$array_item}');
		{/foreach}
	{/if}

	{if $totalAmount>=$iciciMinEMITotalAmount}
		var iciciEMICardArray = new Array();
		{foreach from=$iciciEMIBINS item=array_item key=id}
			iciciEMICardArray.push('{$array_item}');
		{/foreach}
	{/if}

	var mobileVerificationSubmitCount = 0;
	var codCharge = {$cod_charge};
	var orderSummaryDislplayed=false;
	{if $cashcoupons.MRPAmount>0}
		var cashBackAmount = {$cashcoupons.MRPAmount};
	{else}
		var cashBackAmount = 0;
	{/if}

	var userMobileNumber = "{$userMobile}";
	var paymentChildWindow;
	var checkPaymentWindow;
	var paymentProcessingPopup;
</script>
{include file="site/secure_footer_javascript.tpl"}
</body>
</html>
