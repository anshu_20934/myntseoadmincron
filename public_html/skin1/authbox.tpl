{* $Id: authbox.tpl,v 1.25 2005/11/17 15:08:15 max Exp $ *}

{if $usertype eq "C"} {* Crystal_Blue *}

<form action="{$xcart_web_dir}/include/login.php" method="post" name="loginform">

<table cellpadding="0" cellspacing="0">
<tr>
<td><img src="{$cdn_base}/skin1/images/spacer.gif" width="1" height="1" alt="" border="0" /></td>
<td class="VertMenuItems" align="right" height="40">
<table cellspacing="0" cellpadding="0">
<tr>
<td>{$login}&nbsp;&nbsp;{$lng.txt_logged_in}&nbsp;&nbsp;</td>
<td>{include file="buttons/logout_menu.tpl"}</td>
</tr>
</table>
</td>
</tr>
</table>
<input type="hidden" name="mode" value="logout" />
<input type="hidden" name="redirect" value="{$redirect}" />
</form>

{else} {* Crystal_Blue *}

{capture name=menu}
<form action="{$xcart_web_dir}/include/login.php" method="post" name="loginform">

<table cellpadding="0" cellspacing="0">
<tr> 
<td>&nbsp;&nbsp;&nbsp;</td>
<td class="VertMenuItems" valign="top">
{$login}<br />{$lng.txt_logged_in}<br />
<br />
{include file="buttons/logout_menu.tpl"}
<br />
</td>
</tr>
</table>
<input type="hidden" name="mode" value="logout" />
<input type="hidden" name="redirect" value="{$redirect}" />
</form>
{/capture}
{ include file="menu.tpl" dingbats="dingbats_authentification.gif" menu_title=$lng.lbl_authentication menu_content=$smarty.capture.menu }

{/if} {* Crystal_Blue *}
