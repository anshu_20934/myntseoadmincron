<!-- my active coupons starts here -->
    {if $active_coupons}
        <div class="section-heading prepend">My Active Coupon(s)</div>
        <div style="max-height:500px;overflow-y:auto;">
            <table cellpadding="0" cellspacing="0" width="100%" class=" my-c-table">
                <tbody>
                <tr>
                    <th>Coupon name</th>
                    <th>Description</th>
                    <th>Valid till</th>
                </tr>
                {section name=prod_num loop=$active_coupons}
                <tr class="{cycle name="active_cycle" values="odd,even"}">
                    <td class="td" width="150px">{$active_coupons[prod_num].code|upper}</td>
                    <td class="td-cr" width="350px">
                        {if $active_coupons[prod_num].couponType eq "absolute"}
                            <b>Rs. {$active_coupons[prod_num].mrpAmount|round}</b> off
                            {elseif $active_coupons[prod_num].couponType eq "percentage"}
                            <b>{$active_coupons[prod_num].mrpPercentage}%</b> off
                            {elseif $active_coupons[prod_num].couponType eq "dual"}
                            <b>{$active_coupons[prod_num].mrpPercentage}%</b> off upto
                            <b>Rs. {$active_coupons[prod_num].mrpAmount|round}</b>
                        {/if}
                        {if $active_coupons[prod_num].minimum neq '' && $active_coupons[prod_num].minimum gt 0}
                            on a minimum purchase for Rs. {$active_coupons[prod_num].minimum|round}
                        {/if}
                    </td>
                    <td class="td-last ">{$active_coupons[prod_num].expire|date_format:"%e %B, %Y"}
                        <b>({math equation= "ceil((x - y)/86400)" x=$active_coupons[prod_num].expire y=$today assign="valid_days"}{if $valid_days lte 0}{assign var=valid_days value=1}{/if}{$valid_days} {if $valid_days gt 1}
                            days{else}day{/if} to expiry)</b></td>
                </tr>
                {/section}
                </tbody>
            </table>
        </div>
    {/if}
    <!-- active coupons ends here -->
  
    <!-- my used coupons starts here -->
    {if $used_locked_coupons}
        <div class="section-heading prepend">My Used Coupon(s)</div>
        <div style="max-height:500px;overflow-y:auto;">
            <table cellpadding="0" cellspacing="0" width="100%" class=" my-c-table">
                <tbody>
                <tr>
                    <th>Coupon name</th>
                    <th>Description</th>
                    <th>Used On</th>
                </tr>

                {section name=prod_num loop=$used_locked_coupons}
                <tr class="{cycle name="used_cycle" values="odd,even"}">
                    <td class="td" width="150px">{$used_locked_coupons[prod_num].code|upper}</td>
                    <td class="td-cr" width="350px">
                        {if $used_locked_coupons[prod_num].couponType eq "absolute"}
                            <b>Rs. {$used_locked_coupons[prod_num].mrpAmount|round}</b> off
                            {elseif $used_locked_coupons[prod_num].couponType eq "percentage"}
                            <b>{$used_locked_coupons[prod_num].mrpPercentage}%</b> off
                            {elseif $used_locked_coupons[prod_num].couponType eq "dual"}
                            <b>{$used_locked_coupons[prod_num].mrpPercentage}%</b> off upto
                            <b>Rs. {$used_locked_coupons[prod_num].mrpAmount|round}</b>
                        {/if}
                        {if $used_locked_coupons[prod_num].minimum neq '' && $used_locked_coupons[prod_num].minimum gt 0}
                            on a minimum purchase for Rs. {$used_locked_coupons[prod_num].minimum|round}
                        {/if}
                    </td>
                    <td class="td-last ">{$used_locked_coupons[prod_num].used_on|date_format:"%e %B, %Y"} </td>
                </tr>
                {/section}
                </tbody>
            </table>
        </div>
    {/if}
    <!-- used coupons ends here -->
    <!-- my expired coupons starts here -->
    {if $expired_coupons}
        <div class="section-heading prepend">Expired Coupon(s)</div>
        <div style="max-height:500px;overflow-y:auto;">
            <table cellpadding="0" cellspacing="0" width="100%" class=" my-c-table">
                <tbody>
                <tr>
                    <th>Coupon name</th>
                    <th>Description</th>
                    <th>Expired On</th>
                </tr>

                    {section name=prod_num loop=$expired_coupons}
                    <tr class="{cycle name="expired_cycle" values="odd,even"}">
                        <td class="td" width="150px">{$expired_coupons[prod_num].code|upper}</td>
                        <td class="td-cr" width="350px">
                            {if $expired_coupons[prod_num].couponType eq "absolute"}
                                <b>Rs. {$expired_coupons[prod_num].mrpAmount|round}</b> off
                                {elseif $expired_coupons[prod_num].couponType eq "percentage"}
                                <b>{$expired_coupons[prod_num].mrpPercentage}%</b> off
                                {elseif $expired_coupons[prod_num].couponType eq "dual"}
                                <b>{$expired_coupons[prod_num].mrpPercentage}%</b> off upto
                                <b>Rs. {$expired_coupons[prod_num].mrpAmount|round}</b>
                            {/if}
                            {if $expired_coupons[prod_num].minimum neq '' && $expired_coupons[prod_num].minimum gt 0}
                                on a minimum purchase for Rs. {$expired_coupons[prod_num].minimum|round}
                            {/if}
                        </td>
                        <td class="td-last ">{$expired_coupons[prod_num].expire|date_format:"%e %B, %Y"}</b></td>
                    </tr>
                    {/section}
                </tbody>
            </table>
        </div>
    {/if}
    <!-- expired coupons ends here -->


    <!-- FAQs here -->
    {if $displayPRVFaqs eq 'true'}
    <div class="coupons-faq">
        <h4 class="h4">Myntra Privilege Card</h4>
        <ol>
            <li>
                <b>What is the "Myntra Privilege Card"?</b>
                <p>The “Myntra Privilege Card” is a loyalty card offered to certain privileged customers, which entitles them to a discount of 10% across any purchase made on www.myntra.com .</p>
            </li>
            <li>
                <b>How do I use the coupon code?</b>
                <p>
                    <span>a)&nbsp; Go to www.myntra.com</span>
                    <span>b)&nbsp; Select the items you wish to purchase and proceed to the payment page.</span>
                    <span>c)&nbsp; On the payment page, select ‘Coupon’ and enter the coupon code as mentioned on the privilege card and click 'Redeem'. The discount will get added and the final price will be displayed.</span>
                </p>
            </li>
            <li>
                <b>Can I combine the privilege card benefit with any other voucher?</b>
                <p>No. This cannot be combined with any other voucher or coupon.</p>
            </li>
            <li>
                <b>Can I extend the validity of the card beyond the expiry date?</b>
                <p>No, the card is valid only till 31st Dec 2011.</p>
            </li>
            <li>
                <b>I have lost my card/my card is broken. What do I do?</b>
                <p>We will not be able to replace the card. However, we can send you the coupon code to your registered email address, once you notify us. You can write to <a href="mailto:support@myntra.com">support@myntra.com</a> or you can reach out to customer care at +91-80-43541999.</p>
            </li>
            <li>
                <b>Can I gift this card to my family/friends?</b>
                <p>No. The privilege card is only applicable on the registered ID of the card recipient.</p>
            </li>
            <li>
                <b>The coupon code has gotten erased/is not legible on the card. Can the card be replaced?</b>
                <p>We will not be able to replace the card. However, we can send you the coupon code to your registered email address, once you notify us. You can write to <a href="mailto:support@myntra.com">support@myntra.com</a> or you can reach out to customer care at +91-80-43541999.</p>
            </li>
            <li>
                <b>I'm unable to use the voucher code on the site. What do I do?</b>
                <p>Please write to <a href="mailto:support@myntra.com">support@myntra.com</a> or you can reach out to customer care at +91-80-43541999.</p>
            </li>
        </ol>
    </div>
    {/if}
    <!-- FAQs Ends here -->


