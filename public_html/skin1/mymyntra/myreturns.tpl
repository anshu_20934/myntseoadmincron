<div class="content clearfix mk-account-pages">

    <h2>Pending Returns</h2>
    <table border="0" cellspacing="0">
        <col width="125">
        <col width="150">
        <col width="120">
        <col width="160">
        <tr class="mk-table-head">
        	<th>Return No.</th>
        	<th>Date Placed</th>
            <th>Order No.</th>
            <th>Status</th>
        </tr>
        {foreach item=return from=$returns.pending name=return}
        <tr class="{cycle name='p_return_cycle' values='mk-odd,mk-even'}">
            <td>
                <div class="mk-return-number">{$return.returnid}</div>
                <div class="mk-return-tip mk-hide">
                    <h4>Return No. {$return.returnid}</h4>
                    <ul>
                        <li class="mk-last">{$return.productStyleName}</li>
                    </ul>
                    <div class="mk-tip-arrow"><img src="{$cdn_base}/skin1/images/border-tooltip.png"></div>
                </div>
            </td>
            <td>{$return.createddate_display|date_format:"%d %b, %Y"}</td>
            <td>{$return.group_id}</td>
            <td>{$return.mymyntra_status}</td>
        </tr>
        {/foreach}
    </table>

    <h2>Completed Returns</h2>
    <table border="0" cellspacing="0">
        <col width="125">
        <col width="150">
        <col width="120">
        <col width="160">
        <tr class="mk-table-head">
            <th>Return No.</th>
            <th>Date Placed</th>
            <th>Order No.</th>
            <th>Refund</th>
            <th>Status</th>
        </tr>
        {foreach item=return from=$returns.completed name=return}
        <tr class="{cycle name='c_return_cycle' values='mk-odd,mk-even'}">
            <td>
                <div class="mk-return-number">{$return.returnid}</div>
                <div class="mk-return-tip mk-hide">
                    <h4>Return No. {$return.returnid}</h4>
                    <ul>
                        <li class="mk-last">{$return.productStyleName}</li>
                    </ul>
                    {if $return.is_refunded}
                    	<h4>Refund: {$rupeesymbol}{$return.refundamount}</h4>
                    {/if}	
                    <div class="mk-tip-arrow"><img src="{$cdn_base}/skin1/images/border-tooltip.png"></div>
                </div>
            </td>
            <td>{$return.createddate_display|date_format:"%d %b, %Y"}</td>
            <td>{$return.group_id}</td>
            <td>{if $return.is_refunded}{$rupeesymbol}<strong>{$return.refundamount}</strong>{/if}&nbsp;</td>
            <td>{$return.mymyntra_status}</td>
        </tr>
        {/foreach}
    </table>

</div>

{foreach item=return from=$returns.pending name=return}
<div id="return-{$return.returnid}" class="lightbox orderbox myreturnbox">
	<div class="mod">
	    <div class="hd">
	        <h3>Return No: {$return.returnid}</h3>
	        <span class="date">placed on {$return.createddate_display|date_format:"%d %b, %Y"}</span>
	        <span><label>{if $return.return_mode eq 'self'}Self-Shipped{else}Myntra Pickup{/if}</label></span>
	        <span class="total">order no. : <strong>{$return.group_id}</strong></span>
	    </div>
	    <div class="bd">
	    	<div class="return-top-border"></div>
	    	<div class="suborder">
	     	<div class="return-status subhd">
				<h4>{$return.mymyntra_status}</h4>
            	<p id = "return_message_{$return.returnid}">{$return.status_msg}</p>
            	{if $return.status eq 'RRQP' || $return.status eq 'RPI' || $return.status eq 'RRQS' || $return.status eq 'RDU'}
		        	{if $return.return_mode eq 'self'}
		    	    	{if $return.courier_service neq ''}
		        	    	<a href="javascript:void(1);" id ="courier_update_href_{$return.returnid}" onclick = "javascript:showCourierDetailsWindow({$return.returnid}, {$return.itemid});">Edit Shipping Details</a>
		            	{else}
		            		<a href="javascript:void(1);" id ="courier_update_href_{$return.returnid}" onclick = "javascript:showCourierDetailsWindow({$return.returnid}, {$return.itemid});_gaq.push(['_trackEvent', 'myreturns', 'enter_tracking_details', 'click']);">Share the Shipping Details Here</a>
		                {/if}
		            	<div id= "courier_update_div_{$return.returnid}_{$return.itemid}" style="display:none;"></div>
		             {/if}
		             {if $return.status eq 'RRQP' || $return.status eq 'RPI' || $return.status eq 'RRQS' || $return.status eq 'RPI2' || $return.status eq 'RPI3' || $return.status eq 'RPUQ2' || $return.status eq 'RPUQ3'}
						<p id= "links-div-{$return.returnid}" style="margin-top:5px;">
	            			<a href="generatelabel.php?for=returndetails&id={$return.returnid}" target="_blank">Download Returns Form</a>
	        			</p>
					{/if}
	             {/if}
            </div>
            <ul class="items">
                <li>
                    <div class="photo">
                        <img src="{$return.designImagePath}" alt="{$singleitem.productStyleName}">
                    </div>
                    <div class="details">
		                <span><label>product code:</label> {$return.style_id}</span>
		                <span class="title"> {$return.productStyleName}</span>
						<span><label>size:</label> {$return.size}</span>
		                <span><label>quantity:</label> {$return.quantity} nos.</span>
            		</div>
            		<div class="amount">
                        <div class="final-price">
                            {if $return.final_price_before_cashback lt $return.subtotal}
                                <span class="ico ico-expand"></span>
                            {/if}
                            {$rupeesymbol}{$return.final_price_before_cashback|string_format:"%.2f"}
                        </div>
                        {if $return.final_price_paid lt $return.subtotal}
                        <div class="mrp hide">
                            MRP: <span class="rupees">{$rupeesymbol}</span>{$return.subtotal|string_format:"%.2f"} <br>
                        </div>
                        {if $return.emi_charge gt 0}
                        <div class="mrp hide">
                            EMI Charges: <span class="rupees">{$rupeesymbol}</span>{$return.emi_charge|string_format:"%.2f"} <br>
                        </div>
                        {/if}
                        <div class="discounts hide">
                            {if $return.item_discount ne 0.00}
                                Discount: (-) {$rupeesymbol}{$return.item_discount|string_format:"%.2f"} <br>
                            {/if}
                            {if $return.cart_discount ne 0.00}
                                Bag Discount: (-) <span class="rupees">{$rupeesymbol} </span>{$return.cart_discount|string_format:"%.2f"} <br>
                            {/if}
                            {if $return.item_coupon_discount ne 0.00}
                                Coupon: (-) {$rupeesymbol}{$return.item_coupon_discount|string_format:"%.2f"} <br>
                            {/if}
                            {if $return.pg_discount ne 0.00}
                                Gateway: (-) {$rupeesymbol}{$return.pg_discount|string_format:"%.2f"} <br>
                            {/if}
                        </div>
                        {/if}
                    </div>
                </li>
            </ul>
            </div>
            <div class="return-top-border"></div>
	    </div>
	</div>
</div>
{/foreach}

{foreach item=return from=$returns.completed name=return}
<div id="return-{$return.returnid}" class="lightbox orderbox myreturnbox">
	<div class="mod">
	    <div class="hd">
	        <h3>Return No: {$return.returnid}</h3>
	        <span class="date">placed on {$return.createddate_display|date_format:"%d %b, %Y"}</span>
	        <span>{if $return.return_mode eq 'self'}Self-Shipped{else}Myntra Pickup{/if}</span>
	        <span class="total">order no. : <strong>{$return.group_id}</strong></span>
	    </div>
	     <div class="bd">
	     	<div class="return-top-border"></div>
	     	<div class="suborder">
	     	<div class="return-status subhd">
				<h4>{$return.mymyntra_status}</h4>
                <p id = "return_message_{$return.returnid}">{$return.status_msg}</p>
            </div>
            <ul class="items">
                <li>
                    <div class="photo">
                        <img src="{$return.designImagePath}" alt="{$singleitem.productStyleName}">
                    </div>
                    <div class="details">
		                <span><label>product code:</label> {$return.style_id}</span>
		                <span class="title"> {$return.productStyleName}</span>
						<span><label>size:</label> {$return.size}</span>
		                <span><label>quantity:</label> {$return.quantity} nos.</span>
            		</div>
            		<div class="amount">
                        <div class="final-price">
                            {if $return.final_price_before_cashback lt $return.subtotal}
                                <span class="ico ico-expand"></span>
                            {/if}
                            {$rupeesymbol}{$return.final_price_before_cashback|string_format:"%.2f"}
                        </div>
                        {if $return.final_price_paid lt $return.subtotal}
                        <div class="mrp hide">
                            MRP: <span class="rupees">{$rupeesymbol}</span>{$return.subtotal|string_format:"%.2f"} <br>
                        </div>
                        {if $return.emi_charge gt 0}
                        <div class="mrp hide">
                            EMI Charges: <span class="rupees">{$rupeesymbol}</span>{$return.emi_charge|string_format:"%.2f"} <br>
                        </div>
                        {/if}
                        <div class="discounts hide">
                            {if $return.item_discount ne 0.00}
                                Discount: (-) {$rupeesymbol}{$return.item_discount|string_format:"%.2f"} <br>
                            {/if}
			    {if $return.cart_discount ne 0.00}
                                Bag Discount: (-) <span class="rupees">{$rupeesymbol} </span>{$return.cart_discount|string_format:"%.2f"} <br>
                            {/if}
                            {if $return.item_coupon_discount ne 0.00}
                                Coupon: (-) {$rupeesymbol}{$return.item_coupon_discount|string_format:"%.2f"} <br>
                            {/if}
                            {if $return.pg_discount ne 0.00}
                                Gateway: (-) {$rupeesymbol}{$return.pg_discount|string_format:"%.2f"} <br>
                            {/if}
                        </div>
                        {/if}
                    </div>
                </li>
            </ul>
            </div>
            <div class="return-top-border"></div>
	{if $return.is_refunded}
            <div class="suborder">
	            <ul class="items">
	                <li>
	                    <div class="refund-details details" style="width:100%;">
	                    <table style="text-align: center; width: 100%;">
	                    	<tr>
	                    	<td style="text-align:right;width:50%" align=right><label>Refund Amount :</label></td><td style="text-align:left;" align=left>  {$rupeesymbol}{$return.refundamount}</td></tr>
	                    	{if $return.return_mode == 'self' && $return.delivery_credit != 0.00}
			                <tr><td style="text-align:right;width:50%" align=right><label>Includes Self-Shipping Credit of :</label></td><td style="text-align:left;" align=left>  {$rupeesymbol}{$return.delivery_credit}</td></tr>
			                {/if}
				{if $return.difference_refund != 0.00 }
                                        <tr><td style="text-align:right;width:50%" align=right><label>Price Mismatch Difference Refunded Earlier: </label></td><td style="text-align:left;" align=left><span class="rupees">{$rupeesymbol}</span><span>{$return.difference_refund}</span></td></tr>
                                        {/if}
			                {if $return.cashback_issued != 0.00 }
					        	<tr><td style="text-align:right;width:50%" align=right><label>Reversal of Earned Cashback :</label></td><td style="text-align:left;" align=left> (-) {$rupeesymbol}{$return.cashback_issued}</td></tr>
					    	{/if}
					    	{if $return.item_coupon_discount != 0.00}
			            	<tr><td colspan="2" style="text-align:center;"><span>A Replacement Coupon has been generated which has been added to your Mynt Credits</span></td></tr>
							{/if}
			                <tr><td style="text-align:right;width:50%" align=right><label><span class="ico ico-expand"></span>  You Paid :</label></td><td style="text-align:left;" align=left> {$rupeesymbol}{$return.final_price_before_cashback}</td></tr>
			            </table>
			            <table class="refund-details-expand" style="text-align: center; width: 100%;display:none">
					        {if $return.item_cash_redeemed ne 0.00}
                                <tr><td style="text-align:right;width:50%" align=right><label>Cashback Used :</label></td><td style="text-align:left;" align=left>{$rupeesymbol}{$return.item_cash_redeemed}</td></tr>
                            {/if}
					        {if $return.payment_method != ""}
								<tr><td style="text-align:right;width:50%" align=right><label>
									{if $return.payment_method eq "on"}Online Payment :{elseif $return.payment_method eq "cod"}Cash on Delivery :{else}Actual Payment :{/if}</label></td><td style="text-align:left;" align=left>{$rupeesymbol}{$return.final_payment_made}</td></tr>
							{/if}
						</table>

			            <hr style="border:1px dotted #AAAAAA;margin:0px;"></hr>
	            		</div>
	                </li>
	            </ul>
            </div>
	{/if}
	    </div>
	</div>
</div>
{/foreach}
