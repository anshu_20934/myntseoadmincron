<div class="content-left left last span-26">
                <div class="h4">My Referrals</div>
				<div style="padding-top:10px;margin-bottom:10px;"><img alt="referral banner" src="{$cdn_base}/skin1/images/mymyntra_referral_banner.jpg" class="span-26"></div>
                
    <div style="padding-top:10px;margin-bottom:10px;">Refer your friends to join you in the awesome world of shopping
        with Myntra and gain more at every step.
        The moment your friend registers with Myntra, your Mynt Credits gets credited with Rs. {$mrpAmount} and when they make
        their first purchase, your Mynt Club account gets credited with Rs. {$refTotalValue}! <a target="_blank"
                                                                                    class="no-decoration-link bold"
                                                                                    href="/mrp_landing.php">Click here
            to know more</a></div>
    <form id="invite-form" name="invite-form" method="post" action="">
    	<input type="hidden" name="_token" value="{$USER_TOKEN}" />
        <div class="add-book clearfix left" style="width:100%">
            <textarea id="recipient_list" name="recipient_list" rows="3" cols="50" style="height:112px;"
                      class="toggle-text blurred">Please enter email addresses separated by commas</textarea>

        </div>
        <div class="clearfix left" style="width:98%;margin-bottom: 5px;">

            <div id="progressbar" style="display:none;height:22px;width:100%;border:1px solid #ccc">
                <span class="progress_status"><span id="p_status">1</span> % complete </span>

                <div class="ui-progressbar-value ui-widget-header" style="width:0%;height:20px"></div>
            </div>
        </div>
        <div class="left inviteBox">
            <a style="cursor:pointer" class="left"
               onclick="cleanTextArea();_gaq.push(['_trackEvent', 'my_referrals', 'add_contacts', 'click']);showPlaxoABChooser('recipient_list', '/skin1/plaxo.htm'); return false"><img
                    src="{$cdn_base}/skin1/images/invite-icons.gif" alt="Add from my address book"/></a>
        {*<input type="button" id="invite-friends" name="invite-friends" class="invite-friends myntrabtn orange-bg corners p10" value="&laquo; Review invite">*}
            <input type="button" name="invite-friends" class="left invite-friends orange-bg corners p10"
                   value="Send Invites &raquo;">
        </div>
        <div class="left share-icons" style=" width: 280px;">
            Share your unique Mynt Club invitation link on your Facebook wall or Twitter page to start inviting your
            friends and gain at every step.
            <a href="javascript:fb_share_referral('{$login}');"><img alt="facebook"
                                                                     src="{$cdn_base}/skin1/myntra_images/facebook.png"></a>
        {assign var=encodedEmail value=$login|replace:'@':'[AT]'}
            <a href="javascript:twitter_share_referral('{$encodedEmail}','{$num_coupons}','{$value_coupons}');"><img alt="Twitter"
                                                                                 src="{$cdn_base}/skin1/myntra_images/twitter.png"></a>

        </div>
    </form>
    <p>So what are you waiting for? Copy and paste your Invitation Link on Hotmail, Orkut, MySpace, Hi5, iBibo, Msn,
        GTalk, Blogger, Digg, Delicious, or anywhere else and keep collecting the referral credits! Your Invitation
        Link:  <b>{$http_location}/register.php?ref={$login}</b></p>


</div>

<div class="content-left left last span-26">
    <div class="h4">My Invitations</div>
    <div class="reff-block">
        <ul class="right-mytabs">
            <li class="active"><a style="cursor:pointer" class="#tab-c1"
                                  onclick="_gaq.push(['_trackEvent', 'my_referrals', 'open_invites', 'click']);">Open
                Invites ({$num_open_invites})</a></li>
            <li><a class="#tab-c2" style="cursor:pointer"
                   onclick="_gaq.push(['_trackEvent', 'my_referrals', 'registered_on_myntra', 'click']);">Registered on
                Myntra({$num_accepted_invites})</a></li>
            <li class="tab-last"><a class="#tab-c3" style="cursor:pointer"
                                    onclick="_gaq.push(['_trackEvent', 'my_referrals', 'placed_first_order', 'click']);">Placed
                First Order({$num_active_invites})</a></li>
        </ul>


        <ul class="reff-content" id="tab-c1">
        {if $num_open_invites > 0}
            <table cellpadding="0" cellspacing="0" border="1" class="my-c-table" align="center" style="width:98%">
                <tbody>
                <tr>
                    <th>Referral email</th>
                    <th>Last invite sent on</th>
                    <th>No. of invites sent</th>
                </tr>

                    {section name=prod_num loop=$open_invites}
                    <tr>
                        <td>{$open_invites[prod_num].referred|truncate:65:"..."}</td>
                        <td>
                            <table cellpadding="0" cellspacing="0" border="0" width="100%" style="margin-bottom: 0;">
                                <tr>
                                    <td style="border:none;width:100px">
                                        {$open_invites[prod_num].dateReferred|date_format:$config.Appearance.date_format}
                                    </td>
                                    <td style="border:none">
                                        {if ($today - $open_invites[prod_num].dateReferred) > 24*60*60 } <a
                                                class="right resend-invite-link"
                                                onclick=resendFriendInvite('{$open_invites[prod_num].referred}')
                                            style="cursor: pointer;">Resend Invite</a>{/if}

                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td>{$open_invites[prod_num].invitesSent}</td>
                    </tr>
                    {/section}
                </tbody>
            </table>
            {else}
            You do not have any open invites. You can send out invites and earn referral credits by simply following the
            steps listed above.
        {/if}
        </ul>

        <ul class="reff-content" id="tab-c2" style="display:none;">
        {if $num_accepted_invites > 0}
            <table cellpadding="0" cellspacing="0" border="1" class="my-c-table" style="width:98%">
                <tbody>
                <tr>
                    <th>Referral email</th>
                    <th>Registered on Myntra on</th>
                    <th>Coupon awarded</th>
                </tr>
                    {section name=prod_num loop=$accepted_invites}
                    <tr>
                        <td>{$accepted_invites[prod_num].referred|truncate:65:"..."}</td>
                        <td>{$accepted_invites[prod_num].dateJoined|date_format:$config.Appearance.date_format}</td>
                        <td>{$accepted_invites[prod_num].regcompleteCoupon|upper}</a></td>
                    </tr>
                    {/section}
                </tbody>
            </table>
            {else}
            Currently you have no referrals in this category. Urge your friends to accept your invite and sign up on
            Myntra. For each friend who registers, you get a credit of Rs {$mrpAmount}!
        {/if}
        </ul>

        <ul class="reff-content" id="tab-c3" style="display:none;">
        {if $num_active_invites > 0 }
            <table cellpadding="0" cellspacing="0" border="1" class="my-c-table" style="width:98%">

                <tbody>
                <tr>
                    <th>Referral email</th>
                    <th>Placed first order</th>
                    <th>Coupon(s) awarded</th>
                </tr>
                    {section name=prod_num loop=$active_invites}
                    <tr>
                        <td>{$active_invites[prod_num].referred|truncate:65:"..."}</td>
                        <td>{$active_invites[prod_num].dateFirstPurchase|date_format:$config.Appearance.date_format}</td>
                        <td>{$active_invites[prod_num].ordqueuedCoupon|upper}</a></td>
                    </tr>
                    {/section}
                </tbody>
            </table>
            {else}
            You have no referrals who have made their first purchase yet. Enhance your Mynt Credits balance by
            recommending your friends to purchase at Myntra - the one stop destination for lifestyle products. Don't
            forget that on the 1st purchase of each friend you get Rs {$refTotalValue} worth of Mynt credits!
        {/if}
        </ul>


    </div>
</div>

