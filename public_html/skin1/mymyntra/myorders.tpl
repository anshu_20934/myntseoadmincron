<div class="content clearfix mk-account-pages">

    <h2>Being Processed</h2>
    <table border="0" cellspacing="0">
        <col width="125">
        <col width="150">
        <col width="120">
        <col width="160">
        <tr class="mk-table-head">
            <th>Order No.</th>
            <th>Date Placed </th>
            <th>Total</th>
            <th>Delivered / Items</th>
        </tr>
        {foreach key=grouporderid item=ordermap from=$orders}
        {if $ordermap.status eq 'PENDING'}
        <tr class="{cycle name='p_order_cycle' values='mk-odd,mk-even'}">
            <td>
                <div class="mk-order-number">{$grouporderid}</div>
                <div class="mk-order-tip mk-hide">
                    <h4>Order No. {$grouporderid}</h4>
                    <ul>
                        {foreach from=$ordermap.titles item=title name=title}
                        <li{if $smarty.foreach.title.last} class="mk-last"{/if}>{$title}</li>
                        {/foreach}
                    </ul>
                    <h4>Order Total: {$rupeesymbol}{$ordermap.total}</h4>
                    <div class="discount-tool-tip-{$ordermap.orderid}"></div>
                    <div class="mk-tip-arrow"><img src="{$cdn_base}/skin1/images/border-tooltip.png"></div>
                </div>
            </td>
            <td>{$ordermap.orders[0].date|date_format:"%d %b, %Y"}</td>
            <td>{$rupeesymbol}{$ordermap.total}</td>
            <td>{$ordermap.delivered} / {$ordermap.itemsCount}</td>
            {if $ordermap.cancellable eq 'true'}
            	<td>
            		<div class="mk-cancel-order" orderid='{$grouporderid}'>Cancel </div>
            	</td>
            {/if}
        </tr>
        {/if}
        {/foreach}
    </table>

    <h2>Completed</h2>
    <table border="0" cellspacing="0">
        <col width="125">
        <col width="150">
        <col width="120">
        <col width="160">
        <tr class="mk-table-head">
            <th>Order No.</th>
            <th>Date Placed </th>
            <th>Total</th>
            <th>Items</th>
        </tr>
        {foreach key=grouporderid item=ordermap from=$orders}
        {assign var=baseorder value=$ordermap.orders[0]}
        {if $ordermap.status eq 'COMPLETED'}
        <tr class="{cycle name='c_ordercycle' values='mk-odd,mk-even'}">
            <td>
                <div class="mk-order-number">{$grouporderid}</div>
                <div class="mk-order-tip mk-hide">
                    <h4>Order No. {$grouporderid}</h4>
                    <ul>
                        {foreach from=$ordermap.titles item=title name=title}
                        <li{if $smarty.foreach.title.last} class="mk-last"{/if}>{$title}</li>
                        {/foreach}
                    </ul>
                    <h4>Order Total: {$rupeesymbol}{$ordermap.total}</h4>
                    <div class="discount-tool-tip-{$ordermap.orderid}"></div>
                    <div class="mk-tip-arrow"><img src="{$cdn_base}/skin1/images/border-tooltip.png"></div>
                </div>
            </td>
            <td>{$ordermap.orders[0].date|date_format:"%d %b, %Y"}</td>
            <td>{$rupeesymbol}{$ordermap.total}</td>
            <td>{$ordermap.itemsCount}</td>
        </tr>
        {/if}
        {/foreach}
    </table>

</div>

{foreach key=grouporderid item=ordermap from=$orders}
{assign var=baseorder value=$ordermap.orders[0]}
{assign var=subordersCount value=$ordermap.orders|@count}
{assign var=gtotal value=$orderTotals[$grouporderid]}
<div id="order-{$grouporderid}" class="lightbox orderbox">
<div class="mod">
    <div class="hd">
        <h3>Order No: {$grouporderid}</h3>
        <span class="date">placed on {$ordermap.orders[0].date|date_format:"%d %b, %Y"}</span>
        <span>{$ordermap.count} item(s) {if $ordermap.orders|@count gt 1} / multiple shipments{/if}</span>
        <span class="total">
            total: <strong>{$rupeesymbol}{$ordermap.total|string_format:"%.2f"}</strong>
        </span>
    </div>
    <div class="bd">
        <div class="discount-area">
        </div>
        {assign var=shipcount value=0}
        {foreach name=order item=order from=$ordermap.orders}
        {if $order.customerstatusmessage == 'SHIPPED' || $order.customerstatusmessage == 'DELIVERED'}
            {assign var=shipped value=1}
            {assign var=shipcount value=$shipcount+1}
            {if $order.customerstatusmessage == 'SHIPPED'}
                {assign var=statusdate value=$order.shippeddate|date_format:"%d %b, %Y"}
            {else}
                {assign var=statusdate value=$order.delivereddate|date_format:"%d %b, %Y"}
            {/if}
        {else}
            {assign var=shipped value=0}
        {/if}
        <div class="suborder">
            <div class="subhd">
                {if $shipped}
                    <h4>shipment #{$shipcount}</h4>
                    <span>
                        {$order.qtyInOrder} item(s)
                        {$order.customerstatusmessage} via {$order.courier_service} on {$statusdate}
                        {if $order.tracking && $order.showTrackingStatus == "true"}
                        <br> tracking no: <a href="{$order.website}" target="_blank"
                            onclick="javascript:_gaq.push(['_trackEvent', 'my_orders', 'shipment_tracking', '{$order.courier_service}']);
                            showShipmentTracking({$order.orderid}); return false;">{$order.tracking}</a>
                        {elseif $order.tracking}
                        <br> tracking no: {$order.tracking}
                        {/if}
                    </span>
                    {*
                    {if $order.showTrackingStatus == "true" }
                        <div class="link-btn" onclick="javascript:_gaq.push(['_trackEvent', 'my_orders', 'shipment_tracking', '{$order.courier_service}']);showShipmentTracking({$order.orderid})">View tracking details</div>
                    {elseif $order.showTrackingStatus == "empty"}
                        <div>Tracking Details Coming Soon</div>
                    {elseif $order.showTrackingStatus == "purged"}
                        <div title="We retain and display tracking information for upto 30 days after order delivery" class="vtip">Where is my tracking information?</div>
                    {/if}
                    *}
                {else}
                    <h4>to be processed</h4>
                    <span>{$order.qtyInOrder} item(s)</span>
                {/if}
            </div>
            <ul class="items">
                {foreach item=item from=$order.items}
                <li>
                    <div class="photo">
                        <img src="{$item.default_image}">
                    </div>
                    <div class="details">
                        <span><label>product code:</label> {$item.product_style}</span>
                        <span class="title">{$item.stylename}</span>
                        <span><label>size:</label> {if $item.unifiedsize}{$item.unifiedsize}{else}{$item.size}{/if}</span>
                        <span><label>quantity:</label> {$item.quantity} nos.</span>
                        {if $item.returnid}
                            <span class="return-placed">You have placed a return request for this item</span>
                        {elseif $item.return_criteria eq 'allowed'}
                            <span class="return-placed hide">You have placed a return request for this item</span>
                            <button class="link-btn btn-return" data-return-key="{$item.orderid}-{$item.itemid}">return item</button>
                        {elseif $item.return_criteria eq 'not_returnable'}
                            <span class="no-return-msg">THIS ITEM CANNOT BE RETURNED.
                                PLEASE REFER TO <a href="/myntrareturnpolicy.php" target="_blank">OUR RETURNS POLICY</a></span>
                        {elseif $item.return_criteria eq 'time_exceeded'}
                            <span class="no-return-msg">THIS ITEM CANNOT BE RETURNED AS YOUR 30-DAY RETURN PERIOD HAS EXPIRED</span>
                        {/if}
                    </div>
                    <div class="amount">
                        <div class="final-price">
                            {if $item.final_price_paid lt $item.subtotal_price}
                                <span class="ico ico-expand"></span>
                            {/if}
			    {if $item.discount === $item.subtotal_price}
			    FREE	
			    {else}
                            <span class="rupees">{$rupeesymbol} </span>{$item.final_price_paid|string_format:"%.2f"}
                            {/if}
                        </div>
                        {if $item.final_price_paid lt $item.subtotal_price}
                        <div class="mrp hide">
                            MRP: <span class="rupees">{$rupeesymbol}</span>
                                <span>{$item.subtotal_price|string_format:"%.2f"}</span>
                        </div>                         
                        <div class="discounts hide" style="font-size:10px;">
                            {if $item.discount ne 0.00}
                                Item Discount: (-) <span class="rupees">{$rupeesymbol} </span>{$item.discount|string_format:"%.2f"} <br>
                            {/if}
                            {if $item.cart_discount_split_on_ratio ne 0.00}
                                Bag Discount : (-) <span class="rupees">{$rupeesymbol} </span> {$item.cart_discount_split_on_ratio|string_format:"%.2f"}<br />
                            {/if} 
			    {if $item.coupon_discount_product ne 0.00}
                                Coupon: (-) <span class="rupees">{$rupeesymbol} </span>{$item.coupon_discount_product|string_format:"%.2f"} <br>
                            {/if}
                            {if $item.cash_redeemed ne 0.00}
                                Cashback: (-) <span class="rupees">{$rupeesymbol} </span>{$item.cash_redeemed|string_format:"%.2f"} <br>
                            {/if}
                            {if $item.pg_discount ne 0.00}
                                Gateway: (-) <span class="rupees">{$rupeesymbol} </span>{$item.pg_discount|string_format:"%.2f"} <br>
                            {/if}
                        </div>
                        {/if}
                    </div>
                </li>
                {/foreach}
            </ul>
            {if $subordersCount gt 1}
            <div class="total">
                {if $order.shipping_cost > 0 }
                shipping: <strong>{$rupeesymbol}{$order.shipping_cost|string_format:"%.2f"}</strong><br/>
                {/if}
                {if $shipped}shipment #{$shipcount}{/if} total: <strong>{$rupeesymbol}{$order.total|string_format:"%.2f"}</strong>
            </div>
            {/if}
        </div>
        {/foreach}
        <div class="clearfix grand-total">
	        <div class="payment-mode">
	        	{if $baseorder.payment_method eq 'cod'}
	        		Cash On Delivery
		        {elseif $baseorder.payment_method eq 'netbanking'}
		        	Net Banking
		        {elseif $baseorder.payment_method eq 'debitcard'}
		        	Debit Card
		        {elseif $baseorder.payment_method eq 'creditcards'}
		        	Credit Card
                {else}
                    &nbsp;
		        {/if}
	        </div>
            <div class="shipping-address">
                <h4>Shipping Address</h4>
                <address>
                    <div class="name">{$baseorder.s_firstname} {$baseorder.s_lastname}</div>
                    <div>{$baseorder.s_address}</div>
                    <div>{$baseorder.s_locality}</div>
                    <div>{$baseorder.s_city}</div>
                    <div>{$baseorder.s_state}, {$baseorder.s_country}</div>
                    <div>{$baseorder.s_zipcode}</div>
                    <div><label>mobile:</label> {$baseorder.mobile}</div>
                </address>
            </div>
	        <div class="amount">
    	        <div class="final-price">
        	        {if $gtotal.total ne $gtotal.subtotal}
            	        <span class="ico ico-expand"></span>
                	{/if}
        	    	Total : <span class="rupees">{$rupeesymbol} </span>{$gtotal.total|string_format:"%.2f"}
            	</div>
                {if $gtotal.total ne $gtotal.subtotal}
                    <div class="mrp hide">
                        MRP : <span class="rupees">{$rupeesymbol}</span> {$gtotal.subtotal|string_format:"%.2f"}
                    </div>
                    <div class="discounts hide">
                        {if $gtotal.discount}
                            Item Discount : (-) <span class="rupees">{$rupeesymbol} </span> {$gtotal.discount|string_format:"%.2f"}<br>
                        {/if}
                        {if $gtotal.cart_discount ne 0.00}
                            Bag Discount : (-) <span class="rupees">{$rupeesymbol} </span> {$gtotal.cart_discount|string_format:"%.2f"}<br />
                        {/if}
			{if $gtotal.coupon_discount}
                            Coupon : (-) <span class="rupees">{$rupeesymbol} </span> {$gtotal.coupon_discount|string_format:"%.2f"}<br>
                        {/if}
                        {if $gtotal.cash_redeemed ne 0.00}
                            Cashback : (-) <span class="rupees">{$rupeesymbol} </span> {$gtotal.cash_redeemed|string_format:"%.2f"}<br>
                        {/if}
                        {if $gtotal.pg_discount ne 0.00}
                            Gateway : (-) <span class="rupees">{$rupeesymbol} </span> {$gtotal.pg_discount|string_format:"%.2f"}<br>
                        {/if}
                        {if $gtotal.shipping_cost ne 0.00}
                            <span class="shipping-cost">Shipping : {$gtotal.shipping_cost|string_format:"%.2f"}</span>
                        {/if}
                        {if $gtotal.emi_charge gt 0.00}
                        	<br><span class="shipping-cost">EMI Charge : <span class="rupees">{$rupeesymbol} </span> {$gtotal.emi_charge|string_format:"%.2f"}</span>                      
						{/if}
                    </div>
                {/if}
            </div>
        </div>
    </div>
</div>
</div>
{/foreach}

{foreach key=grouporderid item=ordermap from=$orders}
{assign var=baseorder value=$ordermap.orders[0]}
<div id="cancel-order-{$grouporderid}" class="lightbox orderbox">
<div class="mod">
    <div class="hd">
        <h3>Cancel Order No. {$grouporderid}</h3>
        <span class="date">placed on {$ordermap.orders[0].date|date_format:"%d %b, %Y"}</span>
        <span>{$ordermap.count} item(s)</span>
        <span class="total">
            total: <strong>{$rupeesymbol}{$ordermap.total|string_format:"%.2f"}</strong>
        </span>
    </div>
    <div class="bd">
    	<div class="suborder">
    	<div class="canc_msg_box" id="msg-box-{$grouporderid}"></div>
    	<div id="cancel-panel-{$grouporderid}">
            <div class="subhd">
            		<h5 style=" font-size: 14px;font-weight: bold;line-height: 2em;">if you have any questions, have a look at our <a target="_blank" href="/myntracancellationpolicy.php">cancellation policy</a></h5>
                    <span style="font-size:14px;color:#666666;">ANY COUPONS APPLIED ON THE ORDER WILL BE REINSTATED TO YOUR MYNT CREDITS</span>
            </div>
            <div style="padding-top:16px;font-size:14px;font-family:din-med;padding-bottom:16px;border-bottom:1px solid #666666;">
            		<div>you will be refunded Rs. {$order.total} in the for of cashback</div>
            		<input type="hidden" name="groupid" value="{$grouporderid}" />
            		<div style="padding-top:25px;">
    					<label>reason for cancellation *</label>
    				</div>
    				<div id="reason-validation-div-{$grouporderid}" style="display:none;color:red;font-size:12px;" class="cancel-valid">
    					Please select Reason for Cancellation 
    				</div>
    				<div style="padding-top:10px;">
    					<select id="cancel-reason-{$grouporderid}" name="return-reason" style="width: 240px;" class="return-select" onchange='javascript:handleReturnReasonChange(this.value);'>
    						<option value="0">Select Reason</option>
    						<option value="1">Wrong Size</option>
    					</select>
    				</div>
    				<div style="padding-top:25px;">
    					<label>Additional remarks *</label>
    				</div>
    				<div id="remarks-validation-div-{$grouporderid}" style="display:none;color:red;font-size:12px;" class="cancel-valid">
    					Please enter remarks
    				</div>	
    				<div style="padding-top:5px;">	
    					<textarea id="details-{$grouporderid}" name="details" style="height: 60px;width: 240px;"></textarea>
    				</div>
    				<div style="padding-top:25px;">
    					<label>ARE YOU SURE YOU WANT TO CANCEL THIS ORDER?</label>
    				</div>
    				<div style="padding-top:7px;" id = "cancel-buttons-{$grouporderid}">
    					<button orderid = "{$grouporderid}" class="btn normal-btn cancel-submit">Yes</button> &nbsp;&nbsp;
    					<button orderid = "{$grouporderid}" class="btn normal-btn cancel-no">no</button>
    				</div>
       		</div>
       	</div>	
       	
    	
    	
    	</div>
    	
        {foreach name=order item=order from=$ordermap.orders}
        <div class="suborder">
        	
            <ul class="items">
                {foreach item=item from=$order.items}
                <li>
                    <div class="photo">
                        <img src="{$item.default_image}">
                    </div>
                    <div class="details">
                        <span><label>product code:</label> {$item.productid}</span>
                        <span class="title">{$item.stylename}</span>
                        <span><label>size:</label> {if $item.unifiedsize}{$item.unifiedsize}{else}{$item.size}{/if}</span>
                        <span><label>quantity:</label> {$item.quantity} nos.</span>
                        {if $item.returnid}
                            <span class="return-placed">You have placed a return request for this item. {$item.returnid}</span>
                        {/if}
                        {if $item.return_allowed && !$item.returnid}
                            <button class="link-btn btn-return" data-return-key="{$baseorder.orderid}-{$item.itemid}">return item</button>
                        {/if}
                    </div>
                    <div class="amount">
                        <div class="final-price clearfix right">
                            {if $item.final_price_paid lt $item.subtotal_price}
                                <span class="ico ico-expand"></span>
                            {/if}
                            {$rupeesymbol}{$item.final_price_paid|string_format:"%.2f"}
                        </div>
                        {if $item.final_price_paid lt $item.subtotal_price}
                        <div class="mrp hide clearfix right">
                            <label>MRP:</label> {$rupeesymbol}{$item.subtotal_price|string_format:"%.2f"} <br>
                        </div>
                        <div class="discounts hide clearfix right">
                            {if $item.discount ne 0.00}
                                Discount: (-) {$rupeesymbol}{$item.discount|string_format:"%.2f"} <br>
                            {/if} 
                            {if $item.coupon_discount_product ne 0.00}
                                Coupon: (-) {$rupeesymbol}{$item.coupon_discount_product|string_format:"%.2f"} <br>
                            {/if}
                            {if $item.cash_redeemed ne 0.00}
                                Cashback: (-) {$rupeesymbol}{$item.cash_redeemed|string_format:"%.2f"} <br>
                            {/if}
                            {if $item.pg_discount ne 0.00}
                                Gateway: (-) {$rupeesymbol}{$item.pg_discount|string_format:"%.2f"} <br>
                            {/if}
                        </div>
                        {/if}
                    </div>
                </li>
                {/foreach}
            </ul>
            <div class="total">
                {if $order.shipping_cost > 0 }
                shipping: <strong>{$rupeesymbol}{$order.shipping_cost|string_format:"%.2f"}</strong><br/>
                {/if}  
                total: <strong>{$rupeesymbol}{$order.total|string_format:"%.2f"}</strong>
            </div>
        </div>
        {/foreach}
        <div class="shipping-address">
            <h4>Shipping Address</h4>
            <address>
                <div class="name">{$baseorder.s_firstname} {$baseorder.s_lastname}</div>
                <div>{$baseorder.s_address}</div>
                <div>{$baseorder.s_locality}</div>
                <div>{$baseorder.s_city}</div>
                <div>{$baseorder.s_state}, {$baseorder.s_country}</div>
                <div>{$baseorder.s_zipcode}</div>
                <div><label>mobile:</label> {$baseorder.mobile}</div>
            </address>
        </div>
    </div>
</div>
</div>
{/foreach}

<script type="text/json" id="order-items-json">
    {$orderItemsJson}
</script>

