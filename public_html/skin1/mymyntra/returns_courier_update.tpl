<div id="courierdetailspopup" style="">
	<div id="courierdetails-main" style="padding:10px;width:450px;overflow:hidden;margin:0 auto;">
		<div id="courierdetails-content">
			<div>
				<div id="courierdetails-progress" class="mymyntra-ajax-loader" style="display:none;">
					<div class="mymyntra-ajax-loader-icon"></div>
				</div>
				<div id="courierdetails-errorMsg" class="notification hide">We were not able to update courier details. Please try later.</div>
				<input type="hidden" name="current_return_id" id="current_return_id" value="{$returnid}">
				<input type="hidden" name="current_item_id" id="current_item_id" value="{$itemid}">
				<div id="courierdetails" style="overflow:hidden;margin-bottom:10px;">
					<div style="width:45%;float:left;margin-right:10px">
						<div class="label">Shipping Vendor <span>*</span></div>
						<div class="field">
							<select name="courier_service" id="courier_service" class="myselect-box"  onchange="updateCourierServiceSelection(this.value);">
								<option value="">--Select Courier--</option>
								{section name=idx loop=$couriers}
									<option value="{$couriers[idx].code}">{$couriers[idx].display_name}</option>
								{/section}
								<option value="other">Other Service</option>
							</select>
						</div>
					</div>
					<div style="width:44%;float:left;">
						<div class="label">Tracking Number <span>*</span></div>
						<div class="field">
							<input type="text" name="tracking_no" id="tracking_no" class="myfield">
						</div>
						
					</div>
				</div>
				<div style="display:none;text-align:left;clear:both;" id="courier_service_custom_div" class="clearfix">
					<input type="text" name="courier_service_custom" id="courier_service_custom" class="myfield" style="width:190px;">
				</div>
				<div style="clear:both;margin: 10px auto;width: 225px;" class="clearfix">
                          <div class="button" style="text-align: left">
						<input type="button" class="action-btn" value="Save" onclick="updateTrackingDetails();">
						&nbsp;&nbsp;
						<input type="button" class="action-btn" value="Cancel" onclick="closeCourierDetailsWindow({$returnid},{$itemid});">
					</div>
				</div>
			</div>
			
		</div>
	</div>
</div>