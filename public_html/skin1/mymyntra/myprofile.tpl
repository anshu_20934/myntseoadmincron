    				<div class="content-left left last span-15">
                        <div class="profile-details">
                            <div class="h4" style="*position:relative">My Profile<span class="right"  style="*position:absolute;*right:0px;"> <a class="right no-decoration-link edit-profile-link vtip" title="Edit profile information" href="javascript:void(0)" ></a></span></div>

                            <div class="profile-picture clearfix">
                                <div id="preview">
                                	{if $facebook_uid neq ""}
            	        				<fb:profile-pic uid="{$facebook_uid}" facebook-logo="true" size="small" linked="false"></fb:profile-pic>
            	        			{/if}
            <!--                        <img width="100px" height="100px" src="{if $userProfileData.myimage neq ''}$userProfileData.myimage{ else }{/if}" id="thumb">-->
                                </div>
            <!--                    <a class="change-picture no-decoration-link show">Change Picture</a>-->
            <!--                    <div id="upload-area">-->
            <!--                          <form action="/playground/ajax_upload" id="newHotnessForm">-->
            <!--                            <input type="file" size="20" id="imageUpload" class=" ">-->
            <!--                            <button class="button" type="submit">Save</button>-->
            <!--                        </form>-->
            <!--                    </div>-->
                            </div>
                            <form action="{$http_location}/mymyntra.php" method="post" id="change-profile" name="change-profile">
                                <input type="hidden" name="mode" value="update-profile-data" />
                                <input type="hidden" name="_token" value="{$USER_TOKEN}" />
                                <table class="profile align-top">
                                    <tr>
                                        <td class="prepend">Name</td>
                                        <td>
										    <span class="editable name-lbl">{$userProfileData.firstname} {$userProfileData.lastname}</span>
                                            <input type="text" name="name" value="{$userProfileData.firstname} {$userProfileData.lastname}" class="profile-name hide" style="margin-bottom:0px"/>
                                            <div class="profile-name-error error hide"></div>
                                        </td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td class="prepend">Email</td>
                                        <td>{$userProfileData.login}
										<input type="hidden" name="login" id="login" value="{$userProfileData.login}" >
										</td>
										<td></td>
                                    </tr>
                                    <tr>
                                        <td class="prepend" width="30%">My Phone Number </td>

                                        <td {if $verified_for_another eq 1}width="45%"{else}width="30%"{/if}>
											<span>(+91)</span>
                                            <span class="editable mobile-lbl">{$userProfileData.mobile}</span>
                                            <input type="text" id="mobile" name="mobile" value="{$userProfileData.mobile}" class="profile-mobile hide" maxlength="10" style="margin-bottom:0px">
                                            <div class="profile-mobile-error error hide"></div>
                                            {if $verified_for_another eq 1}
                                            <span class="editable status-lbl right">
                                              <strong>
                                                    <a class="right no-decoration-link edit-profile-link vtip" title="Edit profile information" href="javascript:void(0)" ></a>
                                              </strong>
                                            </span>
                                            {else}
                                            <span class="editable status-lbl">&nbsp;&nbsp;&nbsp;&nbsp;
                                            	<strong>
                                            		{if $mobile_status eq "V"}
                                            			<font color="green">Verified</font>
                                            		{elseif $mobile_status neq "E"}
                                            		{if $mrp_skipMobileVerification neq 1}
                                            			{if $userProfileData.mobile neq ""}
                                            				{if $verified_for_another eq 0}
                                            					<font color="red">Unverified</font>
                                            					&nbsp;
                                            					<input type="button" value="Verify" id="verify-mobile" class="verify-mobile-profile-page orange-bg corners"/>
                                            				{/if}
                                            			{/if}
                                            		{/if}
                                            		{/if}

                                            	</strong>
                                            </span>
                                            {/if}
                                            


                                        </td>
                                        
                                    </tr>
									<tr class="hide" id="save_cancel_button_container">
										<td>&nbsp;</td>
										<td width="40%">
                                           
                                            <input type="button" value="Save" id="save-profile" class="save-profile sbtn orange-bg corners hide" style="width:50px">
                                            <input type="button" value="Cancel" id="cancel-save-profile" class="cancel-save-profile sbtn orange-bg corners hide" style="width:50px">
                                        </td>
									</tr>
                                    {if $verified_for_another eq 1}
                                    <tr>
                                        <td colspan="3"><font color="red">Your mobile number is unverified because it has been registered against another user. Please enter a valid mobile number to use your coupons.</font></td>
                                    </tr>
                                    {/if}
                                    {if $userProfileData.mobile eq ''}
                                    <tr class="empty-mobile-lbl">
                                        <td colspan="3"><font color="red">Note: Please provide a contact number on which we can reach you for any issues
                                        related to your transactions. This number is also required to be verified for usage of any user-specific coupons that you may have
                                        been issued through Myntra.</font></td>
                                    </tr>
                                    {/if}
                              
                                    {if $userProfileData.mobile neq "" && $mobile_status neq "V" && $mobile_status neq "E" && $verified_for_another eq 0 && $mrp_skipMobileVerification eq 0}
											<tr class="unverified-mobile-lbl">
                                        	<td colspan="3"><font color="red">Note: Please verify your specified contact number 
                                        	to be able to use any user-specific coupons that you may have been issued through Myntra. 
                                        	We may use this number to contact you for issues related to any of your transactions on Myntra.</font></td>
                                    		</tr>
									{/if}
						
									{if $mobile_status eq 'E'}
										<tr class="excedded-attempts">
										<td colspan="3"><font color="red">
										You have exceeded the maximum no. of attempts to verify this mobile number. If you have still not received the verification code or are facing any other difficulties, please provide an alternate number and retry. Or, try again after 6 hours. </br>
										</font></td>
										</tr>
									{/if}
									{if $mrp_skipMobileVerification neq 1 && $mobile_status neq 'V'}
									<tr>
									    <td colspan="3">
									        In case you do not receive your verification code from Myntra, please call us at our Customer Support number at <b>080-43541999</b> anytime and get your number verified instantly.
									    </td>
									</tr>
									{/if}
                                </table>
                            </form>
                            		<div class="span-12 captcha-block">
                                    	<div class="notification-success"></div>
										<div class="notification-error"></div>
										<form id="captchaform" method="post" action="">
											<div class="clearfix captcha-details">
											<div class="p5 corners black-bg4 span-7 left">
											<img src="{$cdn_base}/skin1/images/spacer.gif" id="captcha" />
											</div>
											<a href="javascript:void(0)" onclick="document.getElementById('captcha').src='{$http_location}/captcha/captcha.php?id=myProfilePage&rand='+Math.random();document.getElementById('captcha-form').focus();"
											id="change-image" class="no-decoration-link left">Change text</a>
											</div>
											
											<div class="clearfix mt10 captcha-entry">								
											<span>Type the text you see in the image above to generate verification code.</span>
											<input type="text" name="userinputcaptcha" id="captcha-form" />
											<input type="hidden" name="login" id="login" value="{$userProfileData.login}" >
												<a href="javascript:void(0)" class="next-button orange-bg corners p5 verify-captcha left" onClick="verifyCaptcha()">Submit</a>
												<div class="captcha-loading left" style="display:none;"></div>
											</div>
											<div class="captcha-message hide"></div>
											</form>
                                  	</div>
                            <form action="{$http_location}/mymyntra.php" method="post" id="verify-mobile-code" name="verify-mobile-code" class="verify-mobile-code hide">
                            	<input type="hidden" name="mode" value="verify-mobile-code" />
                            	<input type="hidden" name="_token" value="{$USER_TOKEN}" />
                                <table>
                                    <tr>
				                            <td colspan="3" class="mobile-code-entry hide">
											<div id="mobile-message" class="hide"></div>
                                            <p><br>Verification Code:</strong> <input id="mobile-code" name="mobile-code" size="4" />
                                    		&nbsp;&nbsp;&nbsp;
                                    		<input type="button" value="Submit" id="submit-code" class="submit-code sbt orange-bg corners" />
										    <div class="common-message">If you have not received your verification code, please wait for at least 2 minutes before pressing submit button.</div>
											
                                    	<!--	<input type="button" value="Cancel" id="cancel-submit-code" class="cancel-submit-code sbt orange-bg corners" />-->
                                    	</td>
                                    </tr>
                                    <tr>
                                    	<td colspan="3">
                                    		<span class="profile-mobile hide"><font color="red">Note: Changing the mobile number will reset
                                    		its verification status to Unverified. You will be required to verify your updated mobile number
                                    		before you are able to:<br>
                                    		<ol>
                                    			<li>Use coupons that are configured for usage exclusively by your ID.</li>
                                    			<li>Place an order through COD.</li>
                                    		</ol></font>
                                    		</span>
                                    	</td>
                                    </tr>
                                    <tr>
                                    	<td colspan="3">
                                    		<span class="verify-mobile-manually {if $mobile_status neq 'E'}hide{/if}"><font color="red">You have exceeded the maximum no. of attempts to verify this mobile number. If you have still not received the verification code or are facing any other difficulties, please provide an alternate number and retry. Or, try again after 6 hours. </br>
                                    		</font>
                                    		</span>
                                    	</td>
                                    </tr>
                                </table>
                            </form>
										
                            <a href="javascript:void(0);" class="change-password-link no-decoration-link prepend">Change Password</a>
                            <div class="change-password hide">
                                <form action="{$http_location}/mymyntra.php" method="post" id="change-password-form" name="change-password-form">
                                	<input type="hidden" name="_token" value="{$USER_TOKEN}" />
                                    <input type="hidden" name="mode" value="change-password" />
                                    <div>
                                        <label class="left">Enter Old Password</label>
                                        <input type="password" name="old-password" class="old-password">
                                        <div class="old-password-error error hide"></div>
                                    </div>
                                    <div>
                                        <label class="left">Enter New Password</label>
                                        <input type="password" name="new-password" class="new-password">
                                        <div class="new-password-error error hide"></div>
                                    </div>
                                    <div>
                                        <label class="left">Confirm New Password</label>
                                        <input type="password" name="confirm-new-password" class="confirm-new-password">
                                        <div class="confirm-new-password-error error hide"></div>
                                    </div>
                                    <div>
                                        <input type="button" value="Save" id="save-change-password" class="save-change-password edit-address-btn sbtn orange-bg corners">
                                        <input type="button" value="Cancel" id="cancel-change-password" class="cancel-change-password edit-address-btn sbtn orange-bg corners">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
            
            <div class="content-right right last span-10">
            <div class="address-details">
               <div class="h4">My Shipping Addresses</div>

               {foreach name=address item=address from=$addresses}
                       {*<td class="first prepend">{if $address.default_address eq 1 }Default{/if}</td>*}
                       <div class="address-listing-mid">
                          {* <input id="id" type="text" class="hide" value="{$address.id}"> *}
                          <div class="edit-details">
                            <ul>
                                <li> <a class="edit-address-link no-decoration-link right vtip" title="Edit Address"  >Edit</a></li>
                                <li><input type="hidden" name="id" id="id" value="{$address.id}">
                                    <a class="delete-address-link no-decoration-link right vtip" title="Delete Address"  >Delete</a></li>
                            </ul>
                          </div>
                          {if $address.default_address eq 1 }
                                    {*<input type="hidden" name="id" id="id" value="{$address.id}"> <img src="{$http_location}/skin1/images/setdefault.gif">*}
                                    <span class="setDefault">(Default Address)</span>
                                {/if}
                           <div class="name {if $address.default_address eq 1}setname{/if}">
                                {$address.name}
                           </div>

                           <div style="display:none">
                           <textarea class="address">{$address.address}</textarea>
                           <div class="locality">{$address.locality}</div>
                           <div class="city">{$address.city}</div>
                           <div class="pincode">{$address.pincode}</div>
                           <div class="state">{$address.state}</div>
                           <div class="country">{$address.country}</div>
                           <div class="mobile-no">{$address.mobile}</div>
						   <div class="email">{$address.email}</div>
                           </div>
                           <div>{$address.address|nl2br}</div>
                           <div>{$address.locality}</div>
                           <div>{$address.city} - {$address.pincode} </div>
                           <div>{$address.state_display}, {$address.country_display}</div>
                           <div><b>Mobile</b>: {$address.mobile}</div>
                           <div><b>Email</b>: {$address.email}</div>

                           <div style="float:left">
                            {if $address.default_address neq 1 }
                                <input type="hidden" name="id" id="id" value="{$address.id}">
                                <a class="set-default-address no-decoration-link">Set as Default</a>
                            {/if}
                           </div>
                       </div>
               {/foreach}
               <a class="no-decoration-link add-address-link prepend" id="add_new_shipping_address">Add Shipping Address</a>
               <div class="edit-address address-form hide">
                   <div class="title">Edit Shipping Address</div>
                    <div class="red">Fields marked with (*) are mandatory</div>
                   <form action="{$http_location}/mymyntra.php" name="edit-address" id="edit-address" method="post">
                   	   <input type="hidden" name="_token" value="{$USER_TOKEN}" />
                       <input type="hidden" value="-1" id="id" name="id">
                       <input type="hidden" value="update-address" name="mode">
                       <div>
                           <label class="left">Pin Code *</label>
                           <input type="text" id="pincode" name="pincode" class="pincode" maxlength="10">
                           <div class="pincode-error error hide"></div>
                           <input type="hidden" class="pinerr" name="pinerr" value="0"/>
                       </div>
					   <div>
                           <label class="left">Name *</label>
                           <input type="text" id="name" name="name" class="name">
                           <div class="name-error error hide"></div>
                       </div>
                       <div>
                           <label class="left">Address *</label>
                           <textarea class="address-textarea" rows="4" cols="30" name="address"></textarea>
                           <div class="address-error error hide"></div>
                       </div>
                       <div>
                           <label class="left">Locality {if $abLocality eq 'yes'}*{/if}</label>
                           <input type="text" id="locality" name="locality" class="locality">
                           <div class="locality-error error hide"></div>
                           <input type="hidden" name="selected-locality" class="selected-locality" value="">
                       </div>
                       <div>
                           <label class="left">City *</label>
                           <input type="text" id="city" name="city" class="city">
                           <div class="city-error error hide"></div>
                       </div>
                       <div>
                           <label class="left">State *</label>
                           <select class="state-select" id="state-filter" name="state" size="1">
                               <option value="-1">--Select State--</option>
                               {section name=state_idx loop=$states}
                                <option value="{$states[state_idx].code}">{$states[state_idx].state}</option>
                               {/section}
                           </select>
                           <input type="text" name="state-text" class="hide state-text">
                           <div class="state-error error hide"></div>
                       </div>
                       <div>
                           <label class="left">Country *</label>
                           <select class="country-select" id="country-filter" name="country" size="1">
                                {foreach item=country key=code from=$countries}
                                <option value="{$code}" {if $code eq "IN"} selected="selected"{/if}>{$country}</option>
                                {/foreach}
                           </select>
                       </div>
                       <div>
                           <label class="left">Mobile *</label>
                           <input type="text" id="mobile" name="mobile" class="mobile" maxlength="10">
                           <div class="mobile-error error hide"></div>
                       </div>
					   <div>
							<label class="left">Email *</label>
							<input type="text" id="email" name="email" class="email">
							<div class="email-error error hide"></div>
						</div>
						<!-- Kundan: Zip validation with state--> 
						<div class="zip-err-warning-my-myntra hide">
							<div class="warning-image"></div>
							<div class="zip-err-text-my-myntra" style="overflow: hidden;"></div>
						</div>
						<div>
							<input type="button" value="Save" id="save-edit-address" class="save-edit-address edit-address-btn sbtn save-btn orange-bg corners" style="width:auto">
                            <input type="button" value="Cancel" id="cancel-edit-address" class="cancel-edit-address edit-address-btn cancel-btn sbtn orange-bg corners">
                       </div>
                    </form>
                </div>
                <div class="add-address address-form hide">
                    <div class="title">Add New Shipping Address</div>
                    <div class="red">Fields marked with (*) are mandatory</div>
                   <form action="{$http_location}/mymyntra.php" name="add-address" id="add-address" method="post">
                   	   <input type="hidden" name="_token" value="{$USER_TOKEN}" />
                       <input type="hidden" value="add-address" name="mode">
                       <div>
                           <label class="left">Pin Code *</label>
                           <input type="text" id="pincode" name="pincode" class="pincode" maxlength="10">
                           <div class="pincode-error error hide"></div>
                           <input type="hidden" class="pinerr" name="pinerr" value="0"/>
                       </div>
                       <div>
                           <label class="left">Recipient's Name *</label>
                           <input type="text" id="name" name="name" class="name" value="{if $userProfileData.firstname neq ''}{$userProfileData.firstname} {$userProfileData.lastname}{/if}" >
                           {if $userProfileData.firstname eq ''}
                           <br>
                           <span><input class="copy-as-acc-name" name="copy-as-acc-name" type="checkbox" checked="true" ></span><span style="margin: 0 5px;">Save as your name</span>
                           {/if}
                           <div class="name-error error hide"></div>
                       </div>
                       {if $userProfileData.firstname eq ''}
                       <div class="your-name-div hide">
                           <label class="left">Your Name *</label>
                           <input type="text" id="your-name" name="your-name" class="your-name" value="{if $userProfileData.firstname neq ''}{$userProfileData.firstname} {$userProfileData.lastname}{/if}" >
                           <div class="your-name-error error hide"></div>
                       </div>
                       {/if}
                       <div>
                           <label class="left">Address *</label>
                           <textarea class="address-textarea" rows="4" cols="30" name="address"></textarea>
                           <div class="address-error error hide"></div>
                       </div>
                       <div>
                           <label class="left">Locality {if $abLocality eq 'yes'}*{/if}</label>
                           <input type="text" id="locality" name="locality" class="locality">
                           <div class="locality-error error hide"></div>
                           <input type="hidden" name="selected-locality" class="selected-locality" value="">
                       </div>
                       <div>
                           <label class="left">City *</label>
                           <input type="text" id="city" name="city" class="city">
                           <div class="city-error error hide"></div>
                       </div>
                       <div>
                           <label class="left">State *</label>
                           <select class="state-select" id="state-filter" name="state">
                                <option value="-1" selected="selected">--Select State--</option>
                                {section name=state_idx loop=$states}
                                <option value="{$states[state_idx].code}">{$states[state_idx].state}</option>
                                {/section}
                           </select>
                           <input type="text" name="state-text" class="hide state-text">
                           <div class="state-error error hide"></div>
                       </div>
                       <div>
                           <label class="left">Country *</label>
                           <select  class="country-select" id="country-filter" name="country">
                                <option value="IN">India</option>
                           </select>
                            <div class="country-error error hide"></div>
                       </div>
                       <div>
                           <label class="left">Mobile *</label>
                           <input type="text" id="mobile" name="mobile" class="mobile" value="{$mobile}" maxlength="10">
                           <div class="mobile-error error hide"></div>
                       </div>
					    <div>
							<label class="left">Email *</label>
							<input type="text" id="email" name="email" class="email">
							<div class="email-error error hide"></div>
						</div>
						<!-- Kundan: Zip validation with state--> 
						<div class="zip-err-warning-my-myntra hide">
							<div class="warning-image"></div>
							<div class="zip-err-text-my-myntra" style="overflow: hidden;"></div>
						</div>
                       <div>
                           <input type="button" value="Save" id="save-add-address" class="save-add-address add-address-btn save-btn sbtn orange-bg corners" style="width:auto">
                            <input type="button" value="Cancel" id="cancel-add-address" class="cancel-add-address add-address-btn cancel-btn sbtn orange-bg corners">
                       </div>
                    </form>
                    <form action="" name="delete-address" id="delete-address" method="post">
                    	<input type="hidden" name="_token" value="{$USER_TOKEN}" />
                        <input type="hidden" name="id" id="id" value="-1" >
                        <input type="hidden" name="mode" value="delete-address" >
                    </form>
                    <form action="" name="set-default-address" id="set-default-address" method="post">
                    	<input type="hidden" name="_token" value="{$USER_TOKEN}" />
                        <input type="hidden" name="id" id="id" value="-1" >
                        <input type="hidden" name="mode" value="set-default-address" >
                    </form>
                </div>
            </div>
            </div>
