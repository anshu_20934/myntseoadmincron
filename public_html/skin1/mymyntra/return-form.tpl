<html>
<head>
	<title>Return Form</title>
	<meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' />
</head>
<body>
<div style="margin:40px;height: 1178px;">
		<table>
			<tr>
				<td>
				<img src='{$cdn_base}/skin1/images/myntra.jpg' height='38px'>
				</td>
			</tr>
			<tr>
				<td style="font-size: 33px;font-family:arial;font-weight: bold;">RETURNS FORM</td>
			</tr>
			<tr>
				<td style="font-size:13px;font-family:arial; padding-top: 6px;">If you have any questions, feel free to call {$phonenumber} or use <a href="http://www.myntra.com/contactus">Contact Us</a> page(www.myntra.com/contactus)</td>
			</tr>
			<tr>
				<td style="font-size:20px;font-family:arial;font-weight: bold;padding-top: 11px;">Product returns policy <span style="color:grey;">(agreed by you while placing the return)</span>:</td>
			</tr>
			<tr>
				<td> 
						<table style="border-collapse: collapse;" >
							<tr>
								<td style="border-bottom: 1px solid;"> <img src="{$cdn_base}/skin1/images/checkbox.jpg"></td>
								<td style="border-bottom: 1px solid;font-size: 16px;font-family:arial;line-height: 14px;background: #fafafa;vertical-align: top;">
									<table>
										<tr> 
											<td style="padding-top: 5px;padding-bottom: 5px;font-family:arial;"> The product is in unused condition (tried on ONLY for fit and sizing)</td>
										</tr>
										<tr> 
											<td style="padding-top: 5px;padding-bottom: 5px;font-family:arial;"> ALL the tags and packaging accompanying the product are intact</td>
										</tr>
										<tr> 
											<td style="padding-top: 5px;padding-bottom: 5px;font-family:arial;"> The product was delivered to you in the last 30 days</td>
										</tr>
										<tr> 
											<td style="padding-top: 5px;padding-bottom: 5px;font-family:arial;"> The product is NOT innerwear, socks, deodorants, perfumes, or part of a promotion that does not allow returns</td>
										</tr>
									</table> 
								</td>
							</tr>						
						</table>
				</td>
			</tr>
			<tr>
				<td style="padding-top:24px;font-family:arial;color:grey;font-size:20px;padding-left:4px;font-weight: bold;">
					<img src="{$cdn_base}/skin1/images/check.jpg" style="vertical-align: top;"> Returns request submitted through My Orders
				</td>
			</tr>
			<tr>
				<td style="padding-top:24px;color:grey;font-size:18px;">
					<table>
						<tr>
							<td colspan="4" style="padding-bottom:10px;color:grey;font-size:20px;font-family:arial;font-weight: bold;">
								<img src="{$cdn_base}/skin1/images/check.jpg" style="vertical-align: top;" > Your Order Number and Returns Number :
							</td>
						</tr>
						<tr style="height: 60px;">
							<td style="padding: 10px;">
								&nbsp;
							</td>
							<td style="background: #efefef;padding: 7px 20px;font-family:arial;font-size: 16px;">
								<b>Order Number </b><br/>
								<input value=" {$orderid}" style="font-family: arial;height: 28px;font-size: 16px;"> 
							</td>
							<td style="padding: 10px;">
								&nbsp;
							</td>
							<td style="background: #efefef;padding: 7px 20px;font-family:arial;font-size: 16px;"> 
								<b>Returns Number </b><br/>
								<input value=" {$returnid}" style="font-family: arial;height: 28px;font-size: 16px;">
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td style="padding-top:24px;">
					&nbsp;
				</td>
			</tr>
			<tr>
				<td style="padding-bottom: 24px;border-top: 1px dashed;">
					&nbsp;
				</td>
			</tr>
			<tr>
				<td style="font-size: 18px;font-family:arial;font-size: 20px;font-weight: bold;">
					Please follow the steps below to complete your returns process :
				</td>
			</tr>
			<tr>
				<td style="padding-top:24px;color:grey;font-family:arial;font-size: 20px;font-weight: bold;">
					<img src="{$cdn_base}/skin1/images/1.jpg" style="vertical-align: top;" > <span style="padding-left:5px;">Prepare your returns package</span>
				</td>
			</tr>
			<tr>
				<td style="padding-left:35px;"> <img src="{$cdn_base}/skin1/images/Returns_PackageFlow.jpg"></td>
			</tr>
			<tr>
				<td style="padding-top:34px;color:grey;font-family:arial;font-size: 20px;font-weight: bold;">
					<img src="{$cdn_base}/skin1/images/2.jpg" style="vertical-align: top;"> 
					{if $returnmode eq 'pickup'}
						<span style="padding-left:5px;">Ship your returns package</span>
					{else}
						<span style="padding-left:5px;">Ship the returns package yourself</span>
					{/if}
					
				</td>
			</tr>
			<tr>
				<td style="padding-left:35px;font-size: 13px;color: grey;font-family:arial;line-height: 23px;">
					{if $returnmode eq 'pickup'}
						<b>We will pickup your returns package</b> (in which case you'll hear from us within <b>48 hours</b> of your placing your returns request). <br/>
					{/if}
					Please ensure that you retain your <b>courier receipt</b> until we notify you that we've received your returns package. <br/>
					You will be notified by <b>email</b> as well as through an update in the <b>My Returns</b> section on your My Myntra page.
				 </td>
			</tr>
			<tr>
				<td style="padding-top:20px;font-family: arial;">
					<table style="border-collapse: collapse;">
						<tr>
							<td style="vertical-align: top;padding-top:10px;padding-left:100px;"> <img src="{$cdn_base}/skin1/images/left_right_border.jpg"> </td>
							<td style="vertical-align: top;width: 300px;"> 
								<table style="border-collapse: collapse;">
									<tr>
										<td>
											<img src="{$cdn_base}/skin1/images/top-border.jpg">
										</td>										
									</tr>
									<tr>
										<td style="padding: 20px 60px;font-family: arial;font-weight: bold;font-size: 16px;line-height: 23px;">
											The Returns Desk, <br/>
											Myntra Designs Pvt Ltd.,<br/>
											7th Mile, Krishna Reddy Industrial Area,<br/>
											Kudlu Gate, Opp Macaulay High School,<br/>
											Bangalore 560 068	
										</td>
									</tr>
								</table> 
							</td>
							<td style="vertical-align: top;padding-top:10px;"> <img src="{$cdn_base}/skin1/images/left_right_border.jpg"> </td>
							<td style="vertical-align: top;padding-top:12px;"> <img src="{$cdn_base}/skin1/images/address_arrow.jpg"> </td>
							<td style="vertical-align: top;width: 135px;float:left;font-family: arial;font-size: 12px;padding-top:10px;"> 
								Please cut and stick <br/>
								this address label on <br/>
								your Returns Bag <br/>
							</td>	
						</tr>
					</table>
				</td>
			</tr>
		</table>
</div>
</body>
</html>