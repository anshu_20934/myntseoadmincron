{if $cashback_gateway_status eq 'off'}
<div class="content-left left last" style="width: 100%;">
    <div class="section-heading prepend">My Coupon Credits</div>
    <div class="blue-divider mb10"></div>
     {include file="mymyntra/mycoupons_common.tpl" }
    </div>
    {else}
<div class="content-left left last" style="width: 100%;">
    <div class="reff-block">
        <ul class="innertabs">
            <li class="active"><a style="cursor:pointer" class="#tab-b1"
                                  onclick="_gaq.push(['_trackEvent', 'my_credits', 'mynt_credits tab', 'click']);">Cashback Account</a></li>
            <li><a style="cursor:pointer" class="#tab-b2" onclick="_gaq.push(['_trackEvent', 'my_credits', 'my_coupons tab', 'click']);">My
                Coupons</a></li>
        </ul>
        <div style="float:left;width:750px;">
            <ul class="inner-reff-content" id="tab-b1" style="display: block;">
               <li>

                {if $cashback_coupon neq ''}
                {*<fieldset class="corners my-c-table">*}

                    <div><b>Summary: Balance = Rs. {if $cashback_balance neq ''}{$cashback_balance}{else}0{/if}</b>
                    </div>
                    <br>
                    <table cellpadding="0" cellspacing="0" width="100%" class=" my-c-table">
                        <tbody>
                        <tr>
                            <th>Date of transaction</th>
                            <th>Description</th>
                            <th>Credit</th>
                            <th>Debit</th>
                            <th>Balance (Rs)</th>
                        </tr>

                            {section name=prod_num loop=$cashback_transactions}
                            <tr class="even">
                                <td>{$cashback_transactions[prod_num].date|date_format:"%e %B, %Y"}</td>
                                <td class="td-cr">{$cashback_transactions[prod_num].description}</td>
                                <td class="td-cr">{if $cashback_transactions[prod_num].txn_type eq 'C'}{$cashback_transactions[prod_num].amount}{else}
                                    - {/if}</td>
                                <td class="td-cr">{if $cashback_transactions[prod_num].txn_type eq 'D'}{$cashback_transactions[prod_num].amount}{else}
                                    - {/if}</td>
                                <td class="td-last td-rs">{$cashback_transactions[prod_num].balance}</td>
                            </tr>
                            {/section}
                        </tbody>
                    </table>
                {*</fieldset>*}
                    {else}
                    <div>You do not have a cashback coupon.</div>
                {/if}
                </li>
            </ul>

            <ul class="inner-reff-content" id="tab-b2" style="display:none;">
                <li>
                {include file="mymyntra/mycoupons_common.tpl" }
                </li>
            </ul>
        </div>
    </div>
</div>
{/if}


