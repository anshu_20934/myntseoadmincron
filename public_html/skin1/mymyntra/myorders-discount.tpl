{if $isActive neq 1 or $image_url eq ''}
<div class="background-voucher">
{/if}
    <div class="discount-hd">Voucher Code</div>
    <div class="discount-md">
        {if $isActive eq 1 && $error eq ''}
            {$dre_code}
        {else}
        <input type="text" id="voucher-inp-order-{$orderid}" value="{if $error neq ''}{$code}{/if}">
        <a href="javascript:void(0);" class="myorder-discount-submit-{$orderid} action-btn" style="margin-bottom: 5px;">SUBMIT</a>
        {/if}
    </div>
    {if $isActive neq 1}
        {if $dre_coupon_code}
        {else}
            <div class="discount-tnc">
                <input type="checkbox" id="discount-tnc-{$orderid}" {if $isActive eq 1} checked="true" disabled="true"{/if} data-orderid="{$orderid}">I AGREE TO THE 
                    <a id="voucher-tnc-abtn" href="paris-calling-terms-conditions" target="_blank">TERMS AND CONDITIONS</a> RELATED TO THE VOUCHER
            </div>
        {/if}
    {/if}
{if $isActive neq 1 or $image_url eq ''}
</div>
{/if}
{if $error}
    <div class="discount-message">
        {eval var=$error}
    </div>
{else} 
    {if $isActive eq 1}
        {if $dre_coupon_code}{*if coupon*}
            <div class="discount-message">
                {eval var=$displayText}
            </div>
        {else}
            <div style="height:200px; width:700px;">
                <img src="{$image_url}"/>
            </div>
            <div class="discount-tnc">
                <input type="checkbox" id="discount-tnc-{$orderid}" {if $isActive eq 1} checked="true" disabled="true"{/if} data-orderid="{$orderid}">I AGREE TO THE
                    <a id="voucher-tnc-abtn" href="paris-calling-terms-conditions" target="_blank">TERMS AND CONDITIONS</a> RELATED TO THE VOUCHER
            </div>
        {/if}
    {/if}
{/if}
