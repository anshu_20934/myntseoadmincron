<div id="scroll_wrapper" class="print" style="height: 90%">
    <div class="display" style="width:500px; font-style:italic; word-wrap:break-word; margin: 0px auto; padding: 2% 0;">
    	<div style="width:49%;padding: 2% 0;"> <b>Dear {$receiver}, </b></div>
        <div style="text-align:justify;width:99%;padding: 5% 0;"> <b>{$giftmessage} </b></div>
        <div style="text-align:right;width:89%;padding: 2% 0;"> <b>{$sender} </b></div>
    </div>
</div>
