<html>
<head>

</head>
<body id="body">
<div id="scroll_wrapper">
<div id="container">
<div id="display">
<div class="center_column">
<div class="print">
<div class="super">
<p>&nbsp;</p>
</div>
<div class="head" style="padding-left: 15px; height: auto;">
<p>{$reportname}</p>
{$reportnotes}
</div>
{foreach from=$reportresult key=name item=group}
<h1>{$name}</h1>
{foreach from=$group key=name item=table}
	{if !is_numeric($name)}
		<h4>{$name}</h4>
	{/if}
	<table cellpadding="3" cellspacing="1" width="100%">
	<tr style="background-color: #4f81bd; color: white;">
		{foreach from=$table.superheaders key=superheader item=span}
		<th style="text-align: center;" colspan={$span}>{$superheader}</th>
		{/foreach}
	</tr>
	<tr style="background-color: #4f81bd; color: white;">
		{foreach from=$table.headers item=header}
		<th style="text-align: center;">{$header}</th>
		{/foreach}
	</tr>
	{foreach from=$table.rows item=row}
	<tr style="background-color: {cycle values="#d0d8e8,#e9edf4"};">
		{foreach from=$row item=cell}
		<td style="{if $cell.style.color}background-color:{$cell.style.color};{/if}text-align: {if $cell.style.align} {$cell.style.align} {else} center {/if}; white-space: nowrap; {if $cell.style.border}{$cell.style.border};{/if}" {if $cell.style.rowspan} rowspan="{$cell.style.rowspan}" {/if}>
		{if $cell.style.hyperlink}<a href="{$cell.style.hyperlink}">{$cell.data}</a>{else}{$cell.data}{/if}
		</td>
		{/foreach}
	</tr>
	{/foreach} {foreach from=$table.footer item=foot}
	<th style="text-align: center;">{$foot}</th>
	{/foreach}
	<tr>
		<td>&nbsp;&nbsp;</td>
	</tr>	
	</table>
{/foreach}
{/foreach}</div>
</div>
</div>
</div>
</div>
{if $mailsent eq "Y"}
		<table>
				<tr>
					<td>{$mailmsg}</td>
					<td>&nbsp;&nbsp;</td>
					<td>&nbsp;&nbsp;</td>
				</tr>
				<tr>
					<td align="center"><input type="button" style="height:20px;width:50px; background-color:black; color:white;" value="Close" onclick="self.close()"></td>
				</tr>
		</table>
{/if}

</body>
</html>

