<html>
<head>
{literal}
<script Language="JavaScript" Type="text/javascript" src="../skin1/main/validation_inv.js"></script>
<script Language="JavaScript" Type="text/javascript">
function chkNegCount(theform)
		{
		
		var quantity=theform.quantity.value;
		//quantity = quantity*1;
		var currentcount=theform.sku_count.value;
		//currentcount = currentcount*1;
		if (quantity==null||quantity== ""||!(isPositiveInteger(quantity)))
		  {
			alert("Please enter  valid quantity >=0.");
			quantity.focus();
		  }
		if ((quantity*1)>(currentcount*1)){
		alert(" Not allowed as Quantity being reduced > Total Count");
		theform.quantity.value=currentcount;
		}
		}
		function FrontPage_Form1_Validator(theform)
		{
         
         var added=theform.reducedby;
         var title=theform.title;
         var quantity=theform.quantity;
         var notes=theform.notes;
         //var added=theform.reducedby;
        
		  if (added.value==null||added.value == "")
		  {
			alert("Please enter your name.");
			added.focus();
			return false;
		  }
		  if (title.value==null||title.value == "")
		  {
			alert("Please enter title.");
			title.focus();
			return false;
		  }
		  if (quantity.value==null||quantity.value == ""||!(isPositiveInteger(quantity.value)))
		  {
			alert("Please enter  valid quantity >=0");
			quantity.focus();
			return false;
		  }
		  if (notes.value==null||notes.value == "")
		  {
			alert("Please enter notes.");
			notes.focus();
			return false;
		  }
		  
		  return true;
		  
		}
		</script>
		{/literal}

<link rel="stylesheet" href="../skin1/mykriti.css"/>

</head>
<body id="body">
	<div id="container"> 
	    <div id="display"> 
	      <div class="center_column"> 
	        <div class="print"> 
		        <div class="super" > 
	           		 <p>&nbsp;</p>
	          	</div>
	          	{if $commentSuccess ne "Y"}
	          	<div class="head" style="padding-left:15px;"> 
		            <p>&nbsp;&nbsp;Reduce Inventory for {$sku_name}</p>
          		</div>
          		<div class="links"><p></p></div>
          		<div class="foot"></div>
          		
          		
<form method="post" enctype="multipart/form-data" onsubmit="return FrontPage_Form1_Validator(this)" language="JavaScript" name="reduceForm" action="reduce_sku_inventory.php">
<input type="hidden" name="reduceinventory" value="true" />
<input type="hidden" name="skuid" value="{$skuid}" />
<input type="hidden" name="sku_count" value="{$sku_count}" />
<input type="hidden" name="thresholdcount" value="{$thresholdcount}" />
		<table>
			<tr>
				<td style="text-align:left;">SKU#</td>
				<td style="text-align:left;"><input type="text" name="sku" value="{$sku_name}" readonly></td>
				<td style="text-align:left;">&nbsp;</td>
				<td style="text-align:left;">&nbsp;</td>
			</tr>
			<tr>
				<td style="text-align:left;">Date</td>
				<td style="text-align:left;"><input type="text" name="date_added" value="{$date}" readonly></td>
				<td style="text-align:left;">&nbsp;</td>
				<td style="text-align:left;">&nbsp;</td>
			</tr>
			<tr>
				<td style="text-align:left;">Subject</td>
				<td style="text-align:left;"><input type="text" name="title"></td>
				<td style="text-align:left;">Reduced By</td>
				<td style="text-align:left;"><input type="text" name="reducedby" ></td>
				<td style="text-align:left;">&nbsp;</td>
				<td style="text-align:left;">&nbsp;</td>
			</tr>
			<tr>
				<td style="text-align:left;">Quantity Reduced</td>
				<td style="text-align:left;"><input type="text" name="quantity" onblur="chkNegCount(document.reduceForm)"></td>
				<td style="text-align:left;">&nbsp;</td>
				<td style="text-align:left;">&nbsp;</td>
			</tr>
			<tr>
				<td style="text-align:left;">Reduction Type</td>
				<td style="text-align:left;">
					<select name="reduction_type">
					{foreach from=$reduction_types key=key item=item}
						<option value="{$key}">{$item}</option>
					{/foreach}
					</select>
				</td>
				<td style="text-align:left;">&nbsp;</td>
				<td style="text-align:left;">&nbsp;</td>
			</tr>
			<tr>
				<td style="text-align:left;">Notes</td>
				<td style="text-align:left;" colspan=3><textarea name="notes" cols='50' rows='5'></textarea></td>
			</tr>
			<tr>
				<td style="text-align:left;">Attachment</td>
				<td style="text-align:left;" colspan=2><input type="file" name="attachment"></td>
				<td style="text-align:left;">&nbsp;</td>
			</tr>
			<tr>
				<td style="text-align:left;" colspan=3>&nbsp;</td>
				<td style="text-align:right;"><input type="submit" value="Reduce" style="background-color:#f2f2f2; color:#000;"></td>
			</tr>
		</table>
	</form>
		{elseif $commentSuccess eq "Y"}
		<table>
				<tr>
					<td>Inventory reduced Succesfully!!!!!</td>
					<td>&nbsp;&nbsp;</td>
					<td>&nbsp;&nbsp;</td>
				</tr>
				<tr>
					<td style="font-size:.8em;color:red;"><b>Please Click the close button below to refresh data on back screen</b></td>
				</tr>
				<tr>
					<td align="center"><input type="button" style="height:20px;width:50px; background-color:black; color:white;" value="Close" onclick="opener.window.location.reload();self.close()"></td>
				</tr>
		</table>
		{/if}
		</div> 
	      </div>   	
	     </div> 
	   </div> 
	</body> 
	</html>
    	