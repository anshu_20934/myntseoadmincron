{* $Id: mykritihome.tpl,v 1.88.2.3 2006/07/19 10:19:35 max Exp $ *}
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
{config_load file="$skin_config"}
<html>
<head>
{include file="mkheadinfo.tpl"}
<script type="text/javascript" src="{$SkinDir}/common_js.js"></script>
<script type="text/javascript" src="{$SkinDir}/PopupWindow.js"></script>
<script type="text/javascript" src="{$SkinDir}/divpop_js.js"></script>
<script type="text/javascript" src="{$SkinDir}/myimage.js"></script>
{if $affiliateId neq ''}
<script type="text/javascript" src="{$SkinDir}/js_script/affiliateRegistrationLogin.js"></script>
{/if}
<script type="text/javascript" src="{$SkinDir}/js_script/redirecturl.js"></script>
<script type="text/javascript" src="{$SkinDir}/js_script/numberformat.js"></script>
<script type="text/javascript" src="{$SkinDir}/js_script/email_validate.js"></script>

<script type="text/javascript" src="{$SkinDir}/js_script/ajaxObject.js"></script>
<link rel="stylesheet" href="{$SkinDir}/modules/organization/css/orgaStyles.css" />
<link rel="stylesheet" href="{$SkinDir}/css/tooltip.css" />
<script type="text/javascript" src="{$SkinDir}/js_script/alttxt.js"></script>
<script type="text/javascript" src="{$SkinDir}/js_script/org_user_js.js"></script>
<link rel="stylesheet" href="{$http_location}/skin1/{$org_css_file}" />

{literal}
<script language="JavaScript" type="text/JavaScript">
<!--
function showPopup(){
document.getElementById('registerpopup').style.visibility = 'visible';
document.getElementById('registerpopup').style.display = '';
}
function hideMessagePopupDelay(){
 setTimeout("hidePopup()", 3000);
}
function hidePopup(){
 document.getElementById('registerpopup').style.visibility = 'hidden';
 document.getElementById('popup').style.display = 'none';
}
//-->
</script>
{/literal}
</head>
<body id="body" >
{if $login_event == 'mycart' && $login}
	{include file="popup_login.tpl" }
{/if}

{if ($register_success == 'created') && $login}
    {include file="popup_register.tpl" }
{/if}
<!-- discount coupon message -->
<div id="registerpopup" >
  <div id="overlay"> </div>
  <div id="{$divid}">
    <div class="wrapper" style="position:absolute; top:300px; left:600px;">
      <div class="head"><a href="javascript:hidePopup();">Click to close</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
      <div class="body">
        <p><strong>{$couponMessage}</strong><br></p>
      </div>
      <div class="foot"></div>
    </div>
  </div>
</div>
{if $couponMessage ne ""}
{literal}
<script type="text/javascript" language="JavaScript 1.2">
	showPopup();
</script>
{/literal}
{/if}
{if $affiliateId ne '' && $affiliateAuthType ne 'AF_MYNTRA_REGISTRATION'}
	{include  file="modules/affiliates/affiliateemailprovider.tpl" }
{else}
	{include file="mkquickregistration.tpl" }
{/if}
<div id="popup"></div>
<div id="scroll_wrapper">
<div id="container">
{include file="mykrititop.tpl" }
<div id="wrapper" >
	<!--breadcrumb-->
	<!--{if $affiliateId neq '' && $themeid eq 'COBRANDED_UI'}
		&nbsp;
	{elseif $loadHanumantpl eq 'Y'}
		&nbsp;  
	{else}
		<div id="breadcrumb">
			<div class="breadcrumb_nav">
				<div class="breadcrumb_text">				
					{section name=bc loop=$breadCrumb}
					{if !$smarty.section.bc.last}
					{if $breadCrumb[bc].title ne ''}
					<div style="float:left;">
					<a href={$breadCrumb[bc].reference}>
					{$breadCrumb[bc].title}
					</a>&nbsp;&gt;&nbsp;
					</div>
					{/if}
					{else}
					{$breadCrumb[bc].title}
					{/if}
					{/section}				
				</div>			
			</div>
		</div>
	{/if}-->
	<div class="clearall"></div>
	<!--breadcrumb-->
	<div id="display">
	
	<!-- start left panel -->
	{if $affiliateleftpanel != ''}
	<div class="menu_column">
		{include file="$affiliateleftpanel" }
	</div>	
	{/if}
	<!-- end -->
    
	    <div class="center_column" style="margin-top:0px;">
		 <div class="form" style="background-color:#f8fee2">
		    {if $productsInCart == "" &&  $giftcerts == "" }
			       <div class="super" style="background-image:url({$cdn_base}/skin1/affiliatetemplates/images/reebok_ipl/shopping_cart.gif);background-repeat:no-repeat;"></div>
				    <div class="links"><p><br/><strong>{$productMessage}</strong></p>
				    {if $affiliateId eq '446'}
				    	<p align="right" style="font-size:1.1em"><a href="{$http_location}/reebok-fan-gear" title='add more products' >add items</a></p>
				    {else}				   
						<p align="right" style="font-size:1.1em"><a href="{$http_location}/reebok-store" title='add more products' >add items</a></p>			       					{/if}
					</div>				 
				  <div class="foot"></div>
		    {else}		        
				
				{include file="reebok_ipl_cartproducts.tpl" } 
				
		    {/if}
		</div>
	</div>

  </div>
<div class="clearall"></div>
</div>
{include file="footer.tpl" }
   </div>
   </div>
   <div id="navtxt" class="navtext" style="position:absolute; top:-100px; left:0px; visibility:hidden"></div>
</body>
</html>
