<html>
<head>
{literal}


<script Language="JavaScript" Type="text/javascript">
		function FrontPage_Form1_Validator(theform)
		{
         
         var thresholdcount=theform.thresholdcount;
         var thresholdemail=theform.thresholdemail;
         var unitprice=theform.unitprice;
         var notes=theform.notes;
         var vendor=theform.vendor;
         
         if (vendor.value==null||vendor.value == "")
		  {
			alert("Please enter vendor");
			vendor.focus();
			return false;
		  }
		  if (thresholdcount.value==null||thresholdcount.value == "")
		  {
			alert("Please enter thresholdcount.");
			thresholdcount.focus();
			return false;
		  }
		  if (thresholdemail.value==null||thresholdemail.value == "")
		  {
			alert("Please enter thresholdemail.");
			thresholdemail.focus();
			return false;
		  }
		 
		 if (unitprice.value==null||unitprice.value == "")
		  {
			alert("Please enter unitprice.");
			unitprice.focus();
			return false;
		  }
		 
		 if (notes.value==null||notes.value == "")
		  {
			alert("Please enter notes.");
			notes.focus();
			return false;
		  }
		 
		 
		  return true;
		  
		}
		</script>
		{/literal}

<link rel="stylesheet" href="../skin1/mykriti.css"/>

</head>
<body id="body">

	<div id="container"> 
	    <div id="display"> 
	      <div class="center_column"> 
	        <div class="print"> 
		        <div class="super" > 
	           		 <p>&nbsp;</p>
	          	</div>
	          	{if $commentSuccess ne "Y"}
	          	<div class="head" style="padding-left:15px;"> 
		            <p>&nbsp;&nbsp;Edit SKU Details</p>
          		</div>
          		<div class="links"><p></p></div>
          		<div class="foot"></div>
   {include file="main/include_js.tpl" src="main/popup_product.js" src="main/overlib.js"}
{popup_init src="main/overlib.js"}       		
          		
<form method="post" onsubmit="return FrontPage_Form1_Validator(this)" language="JavaScript" name="FrontPage_Form1" action="edit_sku_details.php">
<input type="hidden" name="editsku" value="true" />
<input type="hidden" name="sku_id" value="{$sku_id}" />
		<table>
			<tr>
				<td style="text-align:left;">SKU#</td>
				<td style="text-align:left;"><span style="font-size:.9em; color:blue;">{$sku_name}</span></td>
				<td style="text-align:left;">Date Added</td>
				<td style="text-align:left;"><span style="font-size:.9em; color:blue;">{$date}</span></td>
			</tr>
			<tr>
				<td style="text-align:left;">Product Type</td>
				<td style="text-align:left;"><span style="font-size:.9em; color:blue;">{$prtype}</span></td>
				<td style="text-align:left;">Product Style</td>
				<td style="text-align:left;"><span style="font-size:.9em; color:blue;">{$style}</span></td>
			</tr>
			<tr>
				<td style="text-align:left;">Options</td>
				<td style="text-align:left;" colspan=2><span style="font-size:.9em; color:blue;">{$options}</span></td>
				<td style="text-align:left;">&nbsp;</td>
			</tr>
			<tr>
				<td style="text-align:left;">Status</td>
				<td style="text-align:left;">
					<select name="status">
											<option value="Y" {if $status eq "Y"}selected{/if}>Active</option>
                                            <option value="N" {if $status eq "N"}selected{/if}>Inactive</option>
										</select>
				</td>
				<td style="text-align:left;">&nbsp;</td>
				<td style="text-align:left;">&nbsp;</td>

			</tr>
			<tr>
				<td style="text-align:left;">Preferred Vendor</td>
				<td style="text-align:left;"><input type="text" name="vendor" value="{$vendor}"></td>
				<td style="text-align:left;">&nbsp;</td>
				<td style="text-align:left;">&nbsp;</td>
			</tr>
			<tr>
				<td style="text-align:left;">Threshold Count</td>
				<td style="text-align:left;"><input type="text" name="thresholdcount" value="{$thresholdcount}"></td>
				<td style="text-align:left;">&nbsp;</td>
				<td style="text-align:left;">&nbsp;</td>
			</tr>
			<tr>
				<td style="text-align:left;">Threshold Email</td>
				<td style="text-align:left;"><input type="text" name="thresholdemail" value="{$thresholdemail}"></td>
				<td style="text-align:left;">&nbsp;</td>
				<td style="text-align:left;">&nbsp;</td>
			</tr>
			<tr>
				<td style="text-align:left;">Standard Unit Price</td>
				<td style="text-align:left;"><input type="text" name="unitprice" value="{$unitprice}"></td>
				<td style="text-align:left;">Current Value</td>
				<td style="text-align:left;"><span style="font-size:.9em; color:red;">{$currentvalue}</span> Rs</td>
			</tr>
			<tr>
				<td style="text-align:left;">Notes</td>
				<td style="text-align:left;" colspan=3><textarea name="notes" cols='50' rows='5'>{$notes}</textarea></td>
			</tr>
			<tr>
				<td style="text-align:left;" colspan=3>&nbsp;</td>
				<td style="text-align:right;"><input type="submit" value="Update" style="background-color:#f2f2f2; color:#000;"></td>
			</tr>
		</table>
	</form>
	{if $comments}
	<table style="border:solid 1px black; width:100%;">
				<tr style="background-color:#4f81bd;">
					
					<td style="color:white; font-weight:bold; font-size:1.0em;">Comment By</td>
					<td style="color:white; font-weight:bold; font-size:1.0em;">Type</td>
					<td style="color:white; font-weight:bold; font-size:1.0em;">Quantity</td>
					<td style="color:white; font-weight:bold; font-size:1.0em;">Summary</td>
					<td style="color:white; font-weight:bold; font-size:1.0em;">Attachment</td>
					
				</tr>
				{foreach from=$comments item=comment}
				<tr bgcolor="{cycle values="#d0d8e8,#e9edf4"}">					
					<td style="font-size:.8em;">{$comment.commentby}{" ("}{$comment.date|date_format:$config.Appearance.datetime_format}{")"}</td>
					<td style="font-size:.8em;">{$comment.type}</td>
					<td style="font-size:.8em;">{$comment.quantity}</td>
					<td style="font-size:.8em;" {popup caption=$comment.date|date_format:$config.Appearance.datetime_format text=$comment.notes}>{$comment.summary}{"  ...."}</td>
					
					{if $comment.attachment1}
					<td style="font-size:.9em;"><a href="data/{$comment.attachment1}" TARGET = "_blank">{$comment.attachment1}</a></td>
					{else}	
					<td style="font-size:.9em;">No attachment</td>
					{/if}	
				</tr>				
                {/foreach}    
			</table>
			{/if}
		{elseif $commentSuccess eq "Y"}
		<table>
				<tr>
					<td>SKU Edited Succesfully!!!!!</td>
					<td>&nbsp;&nbsp;</td>
					<td>&nbsp;&nbsp;</td>
				</tr>
				<tr>
					<td style="font-size:.8em;color:red;"><b>Please Click the close button below to refresh data on back screen</b></td>
				</tr>
				<tr>
					<td align="center"><input type="button" style="height:20px;width:50px; background-color:black; color:white;" value="Close" onclick="opener.window.location.reload();self.close()"></td>
				</tr>
		</table>
		{/if}
		</div> 
	      </div>   	
	     </div> 
	   </div> 
	</body> 
	</html>
    	