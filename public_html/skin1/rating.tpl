<form id="ratingreviewform" method="post" action="">
	<div id="addReviewRatingWindow" style="display:none">
		<div id="reviewContentWindow" style="display:block;">
				
			<div id="pro-review">
				{if $login eq ''}
					<h4>User not logged in</h4>
					<div>Please login to rate and review the product.</div>
				{else}
					<h4>Please rate the product</h4> 
					<div><strong>User</strong> {$login}</div>
					<div>
						<strong>Product Rating</strong>
					
						<div class="ratingbuttons">
							<input type="radio" name="ratingvalue" value="5" /> Great
							<input type="radio" name="ratingvalue" value="4" /> Good 
							<input type="radio" name="ratingvalue" value="3" checked /> Average 
							<input type="radio" name="ratingvalue" value="2" /> Poor 
							<input type="radio" name="ratingvalue" value="1" /> Bad
						</div>
						
					</div>
					
					<div><strong>Product Review</strong><textarea class="review-textarea" id="review-textarea"></textarea></div>
	
					<div id="submit_wrapper" style="display: block;">
						<span><a id="submit_rating_form" class="orange-bg corners p5" style="cursor:pointer;margin-left:210px;">Submit</a></span>
					</div>
				{/if}
			</div>
		</div>
	</div>
</form>

<script>
	var generictype='{$generictype}';
	var generictypeid='{$generictypeid}';
	var sortmode='none';
</script>

{literal}
	<script type="text/javascript">


	</script>
{/literal}