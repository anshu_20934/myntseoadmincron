/* $Id: check_required_fields_js.js,v 1.3.2.1 2006/06/22 12:01:32 max Exp $ */
function substitute(lbl) {
var x, rg;
	for(x = 1; x < arguments.length; x+=2) {
		if(arguments[x] && arguments[x+1]) {
			rg = new RegExp("\\{\\{"+arguments[x]+"\\}\\}", "gi");
			lbl = lbl.replace(rg,  arguments[x+1]);
			rg = new RegExp('~~'+arguments[x]+'~~', "gi");
			lbl = lbl.replace(rg,  arguments[x+1]);
		}
	}
	return lbl;
}

/*
	Check required fields
*/
function checkRequired(lFields, id) {

	var namefield = /[^a-zA-Z]/;
    //emailRe = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)$/;

	if (!lFields || lFields.length == 0)
		return true;
	if (id) {
		for (var x = 0; x < lFields.length; x++) {
			if (lFields[x][0] == id) {
				lFields = [lFields[x]];
				break;
			}
			
		}
	}
	
	//check for mobile number validation
	var _mobile = document.getElementById('phone').value;
	if(_mobile &&  _mobile.match(/[^0-9]/)){
		alert('enter a valid mobile number');
		return false;
	}
	

  	for (var x = 0; x < lFields.length; x++) {
		if (!lFields[x] || !document.getElementById(lFields[x][0]))
			continue;
		var obj = document.getElementById(lFields[x][0]);
			if (obj.value == '' && (obj.type == 'text' || obj.type == 'password' || obj.type == 'textarea')) {
			if (lbl_required_field_is_empty != '') {
				if(lFields[x][1] == 'Username')
					alert(substitute(lbl_required_field_is_empty, 'field', 'E-mail'));
				else
					alert(substitute(lbl_required_field_is_empty, 'field', lFields[x][1]));
			} else {
				alert(lFields[x][1]);
			}
			
			if (!obj.disabled && obj.type != 'hidden') {
				checkRequiredShow(obj);
				obj.focus();
			}
			return false;
		}
/**************************************************************/
/*Code By: Nikhil Gupta						Date: 30/04/2007  */
/*Purpose: Essential form validation						  */
/**************************************************************/
		if (obj.value != '' && (obj.type == 'text' || obj.type == 'password' || obj.type == 'textarea')){
			
			/*if(lFields[x][0] == 'firstname'){
				var fieldvalue = obj.value;
				if(fieldvalue.match(/[^a-zA-Z]/)){
					alert("Please input valid "+lFields[x][1]);
					obj.focus();
					return false;
				}
			}
			if(lFields[x][0] == 'lastname'){
				var fieldvalue = obj.value;
				if(fieldvalue.match(/[^a-zA-Z ]/)){
					alert("Please input valid "+lFields[x][1]);
					obj.focus();
					return false;
				}
			} */
		if(lFields[x][0] == 'uname'){
			var fieldvalue = obj.value;
			if(!echeck(fieldvalue))
			{
                    //alert("Please input valid E-mail id");
				document.getElementById('uname').focus();
				return false;
			}
			}
			if(lFields[x][0] == 'passwd1'){
				var fieldvalue = obj.value;
				if(fieldvalue.length < 6){
					alert(lFields[x][1]+" should have minimum 6 character");
					obj.focus();
					return false;
				}
			}
			if(lFields[x][0] == 'passwd2'){
				if(document.getElementById('passwd1').value != document.getElementById('passwd2').value)
				{
					alert("Confirm password must be same as password");
					obj.focus();
					return false;
				}	
			}
			if(lFields[x][0] == 's_city'){
				var fieldvalue = obj.value;
				if(fieldvalue.match(/[^a-zA-Z]/)){
					alert("Please enter valid "+lFields[x][1]+" name");
					obj.focus();
					return false;
				}
			}
			if(lFields[x][0] == 'mobile'){
				var fieldvalue = obj.value;
				if(fieldvalue.match(/[^0-9]/)){
					alert("Please enter valid mobile number");
					obj.focus();
					return false;
				}
				if(fieldvalue.length < 10){
					alert("Mobile number must have atleast 10 digit");
					obj.focus();
					return false;
				}
			}
			if(lFields[x][0] == 'phone'){
				var fieldvalue = obj.value;
				if(fieldvalue.match(/[^0-9]/)){
					alert("Please enter valid "+lFields[x][1]);
					obj.focus();
					return false;
				}
			}
			if(lFields[x][0] == 's_zipcode'){
				var fieldvalue = obj.value;
				if(fieldvalue.match(/[^0-9]/)){
					alert("Please enter valid "+lFields[x][1]);
					obj.focus();
					return false;
				}
				if(fieldvalue.length < 6 || fieldvalue.length > 6){
					alert(lFields[x][1]+" must have 6 digit");
					obj.focus();
					return false;
				}
			}
			//return false;
		}
/****************************************************************/
	}
	return true;
}

/*
	Show hidden element and element's parents
*/
function checkRequiredShow(elm) {
	if (elm.style && elm.style.display == 'none') {

		if (elm.id == 'ship_box' && document.getElementById('ship2diff')) {
			/* Exception for Register page */
			document.getElementById('ship2diff').checked = true;
			document.getElementById('ship2diff').onclick();
			
		} else
			elm.style.display = '';
	}

	if (elm.parentNode)
		checkRequiredShow(elm.parentNode);

}

