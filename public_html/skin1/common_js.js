function checkIfLoggedIn(login) {
    if (login == "") {
        if (document.getElementById("errorpopup") != null) {
            document.getElementById("errorpopup").style.visibility = "visible";
            document.getElementById("errorpopup").style.display = "";
        }
        if (document.getElementById("loginpopup") != null) {
            document.getElementById("loginpopup").style.visibility = "visible";
            document.getElementById("loginpopup").style.display = "";
        }
        if (document.getElementById("regpopup") != null) {
            document.getElementById("regpopup").style.visibility = "visible";
            document.getElementById("regpopup").style.display = "";
        }
        return false;
    }
    return true;
}
function notCustomizedForPublish_re(page, login, fromadmin) {
	var cstatus = document.getElementById("cstatus").value;
	if( cstatus == "0")
	{
        alert("Please customize your product.You cannot publish blank product.");
		//document.getElementById("errormessage").innerHTML = "You can not add empty product to the cart";
		return false;
	} 
    else if (fromadmin == 'adm') {
        window.location.href = http_loc + "/mkpublishdesign.php";
    } else {
        return checkIfLogged(login, page);
    }
}
function notCustomizedForPublish(page, cstatus, login, fromadmin) {
    if (cstatus == "notcustomized") {
        alert("Please customize your product.You cannot publish blank product.");
        return false;
    }
    else if (fromadmin == 'adm') {
        window.location.href = http_loc + "/mkpublishdesign.php";
    } else {
        checkIfLogged(login, page);
    }
}
function checkIfLogged(login, page, refid) {
    if (login == "") {
        document.getElementById('errorpopup').style.visibility = 'visible';
        document.getElementById('errorpopup').style.display = "";
        document.getElementById("loginpopup").style.visibility = "visible";
        document.getElementById("loginpopup").style.display = "";
        document.getElementById("regpopup").style.visibility = "visible";
        document.getElementById("regpopup").style.display = "";
        document.getElementById("redirect_page_login").value = page;
        document.getElementById("redirect_page_reg").value = page;
        if (page == 'refer') {
            document.getElementById("refid").value = refid;
            document.getElementById("reg_refid").value = refid;
        }
        return false;
    }
    else if (page == 'mkrequest') {
        var URL = redirectURL(page);
        document.corprequest.action = URL;
        document.corprequest.submit();
    }
    else if (page == 'publish') {
        var URL = redirectURL(page);
       /* (document.getElementById("referredTo")).value = URL;*/
        var x = document.getElementById("hiddenForm");
        (document.getElementById("hiddenForm")).submit();
    }
    else if (page == 'productdetail' || page == 'createproduct') {
        var URL = redirectURL(page);
        document.updatecart.action = URL;
        document.updatecart.submit();
    }
    else {
        var URL = redirectURL(page);
        window.location = (typeof refid != 'undefined') ? URL + "/" + refid : URL;
    }
    return true;
}
function checkLoggedIn(login, page) {
    var authType;
    if (login == "") {
        if(document.getElementById('affiliateAuthType'))
            authType=document.getElementById('affiliateAuthType').value;
        else
            authType = 'AF_MYNTRA_REGISTRATION';
        switch (authType) {
            case 'AF_AUTO_REGISTER':
                autoRegisterAffiliateUser(page);
                break;
            case 'AF_MYNTRA_REGISTRATION':
                checkIfLogged(login, page);
                break;
            case 'AF_EMAIL_PROVIDER':
                document.getElementById('errorpopup').style.visibility = 'visible';
                document.getElementById('errorpopup').style.display = "";
                document.getElementById("loginpopup").style.visibility = "visible";
                document.getElementById("loginpopup").style.display = "";
                //document.getElementById("regpopup").style.visibility="visible";
                //document.getElementById("regpopup").style.display="";
                //document.getElementById("redirect_page_login").value=page;
                //document.getElementById("redirect_page_reg").value=page;
                break;
            case 'AF_USE_AFFILIATE_REG':
                autoRegisterUsingAffiliateAcc(page);
                break;
            default:
            	if(orgId == ""){
            		checkIfLogged(login, page);
            	}else{
            		window.location.href = http_loc + "/modules/organization/login.php";
            	}
                break;
        }
        return false;
    }
    document.updatecart.action = "mkpaymentoptions.php" + "?pagetype=" + page;
    document.updatecart.submit();
    return true;
}
function checkIfAffiliateLoggedIn(login, page) {
    if (login == "") {
        document.getElementById('errorpopup').style.visibility = 'visible';
        document.getElementById('errorpopup').style.display = "";
        document.getElementById("loginpopup").style.visibility = "visible";
        document.getElementById("loginpopup").style.display = "";
        return false;
    }
    return true;
}
function setHiddenFieldOptionsValue(optionsMenu, optionValue, optionName, optionIds, name) {
    var x = document.getElementById(optionsMenu);
    var selectedValue = x.options[x.selectedIndex].text;
    var y = document.getElementById(optionName);
    y.value = name;
    var optionid = document.getElementById(optionValue);
    optionid.value = selectedValue;
    var opid = document.getElementById(optionIds);
    opid.value = x.options[x.selectedIndex].value;
}
function setHiddenFieldOptionsValueForWatch(optionsMenu, optionValue, optionName, optionIds, name, productstyle) {
    var x = document.getElementById(optionsMenu);
    var selectedValue = x.options[x.selectedIndex].text;
    var y = document.getElementById(optionName);
    y.value = name;
    var optionid = document.getElementById(optionValue);
    optionid.value = selectedValue;
    var opid = document.getElementById(optionIds);
    opid.value = x.options[x.selectedIndex].value;
    loadZodiacWatch(productstyle, x.options[x.selectedIndex].text);
}
function setHiddenFieldValue(field, value) {
    var x = document.getElementById(field);
    if(x)
    x.value = value;
}
function setSelectedFontValues(fontvalue, fontimage) {
    setHiddenFieldValue('font_value', fontvalue);
    document.getElementById('font_image').src = 'images/fonts/' + fontimage;
    document.getElementById('show_font_to_user').src = 'images/fonts/' + fontimage;
}

/* to change font sizes based on font type
 * in case of limited text customization(for eg:bags).
 * added by arun
 */
function setSelectedFontValuesForTextLimit(fontvalue, fontimage, fontId, orientationId){
	setHiddenFieldValue('font_value', fontvalue);
    document.getElementById('font_image').src = 'images/fonts/' + fontimage;
    document.getElementById('show_font_to_user').src = 'images/fonts/' + fontimage;
	
	if(typeof fontId != null && typeof orientationId != null){
		setHiddenFieldValue('font_id', fontId);
		loadFontSizes(fontId,orientationId);
	}
}

//to load all sizes of font for orientation(which has limited text customization)
function loadFontSizes(fontId,orientationId) {
    if (window.XMLHttpRequest) {
       var req = new XMLHttpRequest();
    }
    else if (window.ActiveXObject) {
       var req = new ActiveXObject("Microsoft.XMLHTTP");
    }
    var formurl = "customize_limited_fontsizes.php?font_id="+fontId+"&orientation_id="+orientationId;
    req.onreadystatechange = function() {
        if (req.readyState == 4) {
			if(req.status==200){
				var resultArray = req.responseText.split('##');
				document.getElementById("font_size_span").innerHTML = '';
				document.getElementById("font_size_span").innerHTML = resultArray[0];//sizes
				document.getElementById("limit_maxchars").value = resultArray[1];//maxchars
				setHiddenFieldValue('font_size', resultArray[2]);//font size default
			}
        }
    };
    
    req.open('GET', formurl, true);
    req.setRequestHeader("Content-Type", "text/xml");
    req.send(null);
}

function setHiddenFieldOptionValue(getField, setField) {
    var x = document.getElementById(getField);
    var selectedValue = x.options[x.selectedIndex].value;
    var y = document.getElementById(setField);
    y.value = selectedValue;
}
function notCustomized(cstatus, pagetype) {
    if (cstatus == "notcustomized") {
        alert("You can not add a blank product to your shopping cart or wishlist. Please add your design!!");
    } else {
        submitAddToCart(pagetype);
    }
}
function submitAddToCartForZoidacWatch(referredTo) {
    var x = document.getElementById('quantityMenu').value;
    var qtyObj = document.getElementById('quantity');
    qtyObj.value = x;
    var x = document.getElementById("optionsMenu0");
    if (x.options[x.selectedIndex].value == 0) {
        alert("Please select your sunsign");
        return;
    }
    if (!IsNumeric(qtyObj.value) || (qtyObj.value) == "") {
        alert("Please enter valid numeric quantity.")
        document.getElementById('quantityMenu').focus();
        return false
    }
    document.getElementById("referredTo").value = referredTo;
    document.getElementById("option0value").value = x.options[x.selectedIndex].text;
    document.getElementById("option0id").value = x.options[x.selectedIndex].value;
    var x = document.getElementById("hiddenForm");
    document.getElementById("hiddenForm").submit();
}
function IsNumeric(sText) {
    pattern = /^[0-9]*$/;
    if (pattern.test(sText) == false) {
        return false;
    }
    else if (sText == 0) {
        return false;
    }
    else {
        return true;
    }
}
function submitAddToCart(referredTo) {
    var totalOp = document.getElementById('totalOptions').value
    var x = document.getElementById('quantityMenu').value;
    var qtyObj = document.getElementById('quantity');
    qtyObj.value = x;
    if (document.getElementById('yahootpl')) {
        document.getElementById('yahootplselected').value = document.getElementById('yahootpl').value;
    }
    for (i = 0; i < totalOp; i++) {
        var optid = "option" + i + "id";
        var optValue = "option" + i + "value"
        var optname = "option" + i + "name"
        var optidValue = document.getElementById(optid).value
        var value = document.getElementById(optValue).value
        var optionName = document.getElementById(optname).value
        if (optidValue == "" || optidValue == 0) {
            if (value == "" || optidValue == 0) {
                val = optionName
            }
        }
    }
    if (!IsNumeric(qtyObj.value) || (qtyObj.value) == "") {
        alert("Please enter valid numeric quantity.")
        document.getElementById('quantityMenu').focus();
        return false
    }
    (document.getElementById("referredTo")).value = referredTo;
    var x = document.getElementById("hiddenForm");
    (document.getElementById("hiddenForm")).submit();
}
function cancelForm(referredTo) {
    (document.getElementById("referredTo")).value = referredTo;
    (document.getElementById("cancelform")).value = "canceldesign";
    var x = document.getElementById("hiddenForm");
    (document.getElementById("hiddenForm")).submit();
}
function submitForm(referredTo) {
    (document.getElementById("referredTo")).value = referredTo;
    var x = document.getElementById("hiddenForm");
    (document.getElementById("hiddenForm")).submit();
}
function getCustomizationAreaImage(startX, startY, width, height, caId, oId) {
    if (window.XMLHttpRequest) {
        req = new XMLHttpRequest();
    }
    else if (window.ActiveXObject) {
        req = new ActiveXObject("Microsoft.XMLHTTP");
    }
    var formurl = "mkcreatecustomizeimage.php";
    req.onreadystatechange = function() {
        if (req.readyState == 4) {
            document.getElementById("customizedimageloader").style.visibility = "hidden";
            document.getElementById("customizedimageloader").style.display = "none";
            document.getElementById("customizedimage").style.visibility = "visible";
            document.getElementById("customizedimage").style.display = "";
            document.getElementById("customizedimage").innerHTML = req.responseText;
        }
    };
    document.getElementById("customizedimageloader").style.visibility = "visible";
    document.getElementById("customizedimageloader").style.display = "";
    req.open('GET', formurl + "?x=" + startX + "&y=" + startY + "&w=" + width + "&h=" + height + "&caId=" + caId + "&oId=" + oId, true);
    req.setRequestHeader("Content-Type", "text/xml");
    req.send(null);
}
function getCustomizationAreaImageProductDetail(startX, startY, width, height, caId, oId) {
    if (window.XMLHttpRequest) {
        req = new XMLHttpRequest();
    }
    else if (window.ActiveXObject) {
        req = new ActiveXObject("Microsoft.XMLHTTP");
    }
    var formurl = "mkcreatecustomizeimage.php";
    req.onreadystatechange = function() {
        if (req.readyState == 4) {
            if( document.getElementById("customizedimageloader")){
                document.getElementById("customizedimageloader").style.visibility = "hidden";
                document.getElementById("customizedimageloader").style.display = "none";
            }
            document.getElementById("customizedimage").style.visibility = "visible";
            document.getElementById("customizedimage").style.display = "";
            document.getElementById("customizedimage").innerHTML = req.responseText;
        }
    };
    if( document.getElementById("customizedimageloader")){
        document.getElementById("customizedimageloader").style.visibility = "visible";
        document.getElementById("customizedimageloader").style.display = "";
    }
    req.open('GET', formurl + "?x=" + startX + "&y=" + startY + "&w=" + width + "&h=" + height + "&caId=" + caId + "&oId=" + oId + "&isProductDetail=1", true);
    req.setRequestHeader("Content-Type", "text/xml");
    req.send(null);
}
function getCustomizationAreaImageProto(startX, startY, width, height, caId, oId) {
    if (window.XMLHttpRequest) {
        req = new XMLHttpRequest();
    }
    else if (window.ActiveXObject) {
        req = new ActiveXObject("Microsoft.XMLHTTP");
    }
    var formurl = "mkcreatecustomizeimage.php";
    req.onreadystatechange = function() {
        if (req.readyState == 4) {
            document.getElementById("customizedimage").innerHTML = req.responseText;
        }
    };
    req.open('GET', formurl + "?x=" + startX + "&y=" + startY + "&w=" + width + "&h=" + height + "&caId=" + caId + "&oId=" + oId + "&isProto=1", true);
    req.setRequestHeader("Content-Type", "text/xml");
    req.send(null);
}
function setCustomizationSubmit(referredTo, id, areaIndex, cstatusvalue) {
    var x = document.getElementById("caId");
    x.value = id;
    (document.getElementById("custAreaName")).value = areaIndex;
    if (cstatusvalue != '')
    if (document.all)
        document.hiddenForm.cstatus.value = cstatusvalue;
    else
        document.getElementById("cstatus1").value = cstatusvalue;
    submitForm(referredTo);
}
function submitDetailForm(referredTo, cid) {
    (document.getElementById("referredTo")).value = referredTo;
    (document.getElementById("caId")).value = cid;
    var x = document.getElementById("hiddenForm");
    x.submit();
}
function getCustomizationAreaImageForPreview(startX, startY, width, height, caId, oId, cstatus, prev) {
    if (window.XMLHttpRequest) {
        req = new XMLHttpRequest();
    }
    else if (window.ActiveXObject) {
        req = new ActiveXObject("Microsoft.XMLHTTP");
    }
    var formurl = "mkcreatecustomizeimage.php";
    req.onreadystatechange = function() {
        if (req.readyState == 4) {
            document.getElementById("customizedimageloader").style.visibility = "hidden";
            document.getElementById("customizedimageloader").style.display = "none";
            document.getElementById("customizedimage").style.visibility = "visible";
            document.getElementById("customizedimage").style.display = "";
            document.getElementById("customizedimage").innerHTML = req.responseText;
        }
    };
    document.getElementById("customizedimageloader").style.visibility = "visible";
    document.getElementById("customizedimageloader").style.display = "";
    req.open('GET', formurl + "?x=" + startX + "&y=" + startY + "&w=" + width + "&h=" + height + "&caId=" + caId + "&oId=" + oId + "&prev=" + prev + "&isPreview=1&cstatus=" + cstatus, true);
    req.setRequestHeader("Content-Type", "text/xml");
    req.send(null);
}
function getCustomizationAreaImageForMultipleOptionPreview(startX, startY, width, height, caId, oId, cstatus, prev) {
    if (window.XMLHttpRequest) {
        req = new XMLHttpRequest();
    }
    else if (window.ActiveXObject) {
        req = new ActiveXObject("Microsoft.XMLHTTP");
    }
    var formurl = "mkcreatecustomizeimage.php";
    req.onreadystatechange = function() {
        if (req.readyState == 4) {
            document.getElementById("customizedimageloader").style.visibility = "hidden";
            document.getElementById("customizedimageloader").style.display = "none";
            document.getElementById("customizedimage").style.visibility = "visible";
            document.getElementById("customizedimage").style.display = "";
            document.getElementById("customizedimage").innerHTML = req.responseText;
        }
    };
    document.getElementById("customizedimageloader").style.visibility = "visible";
    document.getElementById("customizedimageloader").style.display = "";
    req.open('GET', formurl + "?x=" + startX + "&y=" + startY + "&w=" + width + "&h=" + height + "&caId=" + caId + "&oId=" + oId + "&prev=" + prev + "&isPreview=1&cstatus=" + cstatus + "&multioptions=true", true);
    req.setRequestHeader("Content-Type", "text/xml");
    req.send(null);
}
function checkImageFile(frmObj) {
    if (!/(\.(jpg|jpeg|png))$/i.test(frmObj.uploadedImage.value)) {
        alert("Please upload  (jpg | jpeg | png) image.");
        frmObj.uploadedImage.focus();
        return false;
    }
    else if (frmObj.uploadedImage.value == "") {
        alert("Please browse an image.")
        frmObj.uploadedImage.focus();
        return false
    }
    else {
        return true;
    }
}
function useSelectedImage(imagepath) {
    document.hiddenForm.myimage.value = imagepath;
    document.hiddenForm.referrer.value = "uploadImage";
    document.hiddenForm.referredTo.value = "mkcreateproductpreview.php";
    document.hiddenForm.submit();
}
function useAffiliateSelectedImage(originalimage, imagepath, styleid) {
    document.hiddenForm.originalimage.value = originalimage;
    document.hiddenForm.myimage.value = imagepath;
    document.hiddenForm.referrer.value = "uploadImage";
    document.hiddenForm.referredTo.value = "AFFILIATE";
    document.hiddenForm.cstatus.value = "customized";
    document.hiddenForm.submit();
}
function setOrientationPref(value) {
    document.getElementById("orientationValue").value = value;
}
function moveImage(dx, dy, caId) {
    if (window.XMLHttpRequest) {
        req = new XMLHttpRequest();
    }
    else if (window.ActiveXObject) {
        req = new ActiveXObject("Microsoft.XMLHTTP");
    }
    var formurl = document.getElementById("customizationForm").action;
    req.onreadystatechange = function() {
        if (req.readyState == 4) {
            document.getElementById("customizedimage").innerHTML = req.responseText;
        }
    };
    req.open('GET', formurl + "?dx=" + dx + "&dy=" + dy + "&caId=" + caId, true);
    req.setRequestHeader("Content-Type", "text/xml");
    req.send(null);
}
function centreImage(horizontal, vertical, caId) {
    if (window.XMLHttpRequest) {
        req = new XMLHttpRequest();
    }
    else if (window.ActiveXObject) {
        req = new ActiveXObject("Microsoft.XMLHTTP");
    }
    var formurl = document.getElementById("customizationForm").action;
    req.onreadystatechange = function() {
        if (req.readyState == 4) {
            document.getElementById("customizedimage").innerHTML = req.responseText;
        }
    };
    req.open('GET', formurl + "?h=" + horizontal + "&v=" + vertical + "&caId=" + caId, true);
    req.setRequestHeader("Content-Type", "text/xml");
    req.send(null);
}
function rotateImage(degrees, caId) {
    if (window.XMLHttpRequest) {
        req = new XMLHttpRequest();
    }
    else if (window.ActiveXObject) {
        req = new ActiveXObject("Microsoft.XMLHTTP");
    }
    var formurl = document.getElementById("customizationForm").action;
    req.onreadystatechange = function() {
        if (req.readyState == 4) {
            document.getElementById("customizedimage").innerHTML = req.responseText;
        }
    };
    req.open('GET', formurl + "?r=" + degrees + "&caId=" + caId, true);
    req.setRequestHeader("Content-Type", "text/xml");
    req.send(null);
}
function resizeImage(percent, caId) {
    if (window.XMLHttpRequest) {
        req = new XMLHttpRequest();
    }
    else if (window.ActiveXObject) {
        req = new ActiveXObject("Microsoft.XMLHTTP");
    }
    var formurl = document.getElementById("customizationForm").action;
    req.onreadystatechange = function() {
        if (req.readyState == 4) {
            document.getElementById("customizedimage").innerHTML = req.responseText;
        }
    };
    req.open('GET', formurl + "?p=" + percent + "&caId=" + caId, true);
    req.setRequestHeader("Content-Type", "text/xml");
    req.send(null);
}
function autoSize(orientation, caId) {
    if (window.XMLHttpRequest) {
        req = new XMLHttpRequest();
    }
    else if (window.ActiveXObject) {
        req = new ActiveXObject("Microsoft.XMLHTTP");
    }
    var formurl = document.getElementById("customizationForm").action;
    req.onreadystatechange = function() {
        if (req.readyState == 4) {
            document.getElementById("customizedimage").innerHTML = req.responseText;
        }
    };
    req.open('GET', formurl + "?a=" + orientation + "&caId=" + caId, true);
    req.setRequestHeader("Content-Type", "text/xml");
    req.send(null);
}
function postTextForm(caId) {
    if (window.XMLHttpRequest) {
        req = new XMLHttpRequest();
    }
    else if (window.ActiveXObject) {
        req = new ActiveXObject("Microsoft.XMLHTTP");
    }
    var formurl = document.getElementById("textform").action;
    req.onreadystatechange = function() {
        if (req.readyState == 4) {
            document.getElementById("customizedimage").innerHTML = req.responseText;
        }
    };
    var formparam = getFormParam(document.getElementById("textform"));
    req.open('GET', formurl + "?" + formparam + "&caId=" + caId, true);
    req.setRequestHeader("Content-Type", "text/xml");
    req.send(null);
}
function getFormParam(obj) {
    var getstr = "";
    var agt = navigator.userAgent.toLowerCase();
    var is_ie = (agt.indexOf("msie") != -1);
    var is_ie5 = (agt.indexOf("msie 5.0") != -1);
    for (i = 0; i < obj.childNodes.length; i++) {
        if (obj.childNodes[i].tagName == "INPUT" || obj.childNodes[i].tagName == "input") {
            if (obj.childNodes[i].type == "text" || obj.childNodes[i].type == "hidden") {
                if (is_ie && is_ie5) {
                    getstr += obj.childNodes[i].name + "=" + (obj.childNodes[i].value) + "&";
                }
                else {
                    getstr += obj.childNodes[i].name + "=" + encodeURI(obj.childNodes[i].value) + "&";
                }
            }
            if (obj.childNodes[i].type == "checkbox") {
                if (obj.childNodes[i].checked) {
                    getstr += obj.childNodes[i].name + "=" + obj.childNodes[i].value + "&";
                }
                else {
                    getstr += obj.childNodes[i].name + "=&";
                }
            }
            if (obj.childNodes[i].type == "radio") {
                if (obj.childNodes[i].checked) {
                    getstr += obj.childNodes[i].name + "=" + obj.childNodes[i].value + "&";
                }
            }
        }
        if (obj.childNodes[i].tagName == "SELECT") {
            var sel = obj.childNodes[i];
            getstr += sel.name + "=" + sel.options[sel.selectedIndex].value + "&";
        }
        if (obj.childNodes[i].tagName == "textarea") {
            var text = obj.childNodes[i];
            getstr += text.name + "=" + text.value + "&";
        }
    }
    getstr += "inputtextarea" + "=" + document.getElementById('inputtextarea').value
    return getstr;
}
function moveText(dx, dy, caId) {
    if (window.XMLHttpRequest) {
        req = new XMLHttpRequest();
    }
    else if (window.ActiveXObject) {
        req = new ActiveXObject("Microsoft.XMLHTTP");
    }
    var formurl = document.getElementById("customizationForm").action;
    req.onreadystatechange = function() {
        if (req.readyState == 4) {
            document.getElementById("customizedimage").innerHTML = req.responseText;
        }
    };
    var formparam = getFormParam(document.getElementById("textform"));
    var rand = Math.random();
    req.open('GET', formurl + "?textdx=" + dx + "&textdy=" + dy + "&" + formparam + "&caId=" + caId + "&rand=" + rand, true);
    req.setRequestHeader("Content-Type", "text/xml");
    req.send(null);
}
function setOrientationBGColors(colorString) {
    var colorArray = colorString.split(',');
    var obj = document.getElementById("colorselect");
    for (var i = 0; i < obj.options.length; i++) {
        obj.options[i].value = "";
        obj.options[i].text = "";
    }
    for (var i = 0; i < colorArray.length; i++) {
        obj.options[i].value = colorArray[i];
        obj.options[i].text = colorArray[i];
    }
}
function setTextInitValues(fonttype, fontsize, fcolor) {
    if (fonttype == "")fonttype = "arial";
    document.getElementById("fonttype").value = fonttype;
    if (fontsize == "")fontsize = "12";
    document.getElementById("fontsize").value = fontsize;
    document.getElementById("color").value = fcolor;
}
function eraseText(caId) {
    document.getElementById("fonttype").value = 'arial';
    document.getElementById("fontsize").value = '12';
    document.getElementById("inputtextarea").value = "";
    if (window.XMLHttpRequest) {
        req = new XMLHttpRequest();
    }
    else if (window.ActiveXObject) {
        req = new ActiveXObject("Microsoft.XMLHTTP");
    }
    var formurl = document.getElementById("textform").action;
    req.onreadystatechange = function() {
        if (req.readyState == 4) {
            document.getElementById("customizedimage").innerHTML = req.responseText;
        }
    };
    req.open('GET', formurl + "?erase=text" + "&caId=" + caId, true);
    req.setRequestHeader("Content-Type", "text/xml");
    req.send(null);
}
function eraseBackgroundColor(startX, startY, width, height, caId, oId) {
    if (window.XMLHttpRequest) {
        req = new XMLHttpRequest();
    }
    else if (window.ActiveXObject) {
        req = new ActiveXObject("Microsoft.XMLHTTP");
    }
    var formurl = document.getElementById("customizationForm").action;
    req.onreadystatechange = function() {
        if (req.readyState == 4) {
            document.getElementById("customizedimage").innerHTML = req.responseText;
        }
    };
    req.open('GET', formurl + "?erase=bgcolor" + "&caId=" + caId, true);
    req.setRequestHeader("Content-Type", "text/xml");
    req.send(null);
}
function toggleAdjustImageOrTextDiv() {
    if ("block" == document.getElementById("showing_image_customization").style.display) {
        document.getElementById("showing_image_customization").style.display = "none";
        document.getElementById("showing_text_customization").style.display = "block";
        document.getElementById("design_text").style.display = "block";
        document.getElementById("design_image").style.display = "none";
    }
    else if ("block" == document.getElementById("showing_text_customization").style.display) {
        document.getElementById("showing_image_customization").style.display = "block";
        document.getElementById("showing_text_customization").style.display = "none";
        document.getElementById("design_text").style.display = "none";
        document.getElementById("design_image").style.display = "block";
    }
}
function showImageUploadOptions() {
    document.getElementById("delimageid").value = "DEL";
    document.getElementById('show_image_to_user').style.display = "none";
    document.getElementById('let_user_select_image').style.display = "block";
    document.getElementById("deleteuploadImage").submit();
}
function setFontColorValues(color, custid) {
    document.getElementById('colorselect').style.backgroundColor = color;
    document.getElementById('color').value = color;
    document.getElementById('inputtextarea').style.color = color;
    postTextForm(custid)
}
function setColorValue(color, colorvalue, type) {
    if (type == "Image") {
        document.getElementById('colorvalue').style.color = colorvalue;
        document.getElementById('colorvalue').innerHTML = color;
    } else {
        document.getElementById('colortextvalue').style.color = colorvalue;
        document.getElementById('colortextvalue').innerHTML = color;
    }
}
function unsetColorValue(type) {
    if (type == "Image") {
        document.getElementById('colorvalue').innerHTML = "&nbsp;";
    } else {
        document.getElementById('colortextvalue').innerHTML = "&nbsp;";
    }
}
function setBackgroundColorValues(color, caId) {
    document.getElementById('bgcolorselect').style.backgroundColor = color;
    document.getElementById('bgcolor').value = color;
    if (window.XMLHttpRequest) {
        req = new XMLHttpRequest();
    }
    else if (window.ActiveXObject) {
        req = new ActiveXObject("Microsoft.XMLHTTP");
    }
    var formurl = document.getElementById("customizationForm").action;
    req.onreadystatechange = function() {
        if (req.readyState == 4) {
            document.getElementById("customizedimage").innerHTML = req.responseText;
        }
    };
    var formparam = getFormParam(document.getElementById("customizationForm"));
    req.open('GET', formurl + "?bgcolor=" + color + "&caId=" + caId, true);
    req.setRequestHeader("Content-Type", "text/xml");
    req.send(null);
}
function increaseFontSize(customizationAreaId) {
    var font_size = parseInt(document.getElementById("fontsize").value);
    if (isNaN(font_size))return;
    font_size += 4;
    document.getElementById("fontsize").value = font_size;
    postTextForm(customizationAreaId);
}
function decreaseFontSize(customizationAreaId) {
    var font_size = parseInt(document.getElementById("fontsize").value);
    if (isNaN(font_size))return;
    font_size -= 4;
    document.getElementById("fontsize").value = font_size;
    postTextForm(customizationAreaId);
}
function checkRequired() {
    textObj = document.getElementById("designName");
    //priceObj=document.getElementById("designprice");
    //normalprice=document.getElementById("normalprice");
    var wspace = /^\s+$/ ;
    if (textObj.value == "" || (wspace.test(textObj.value)) == true) {
        alert("Please enter design name.");
        textObj.focus();
        return false;
    }
    //if(priceObj.value==""||(wspace.test(priceObj.value))==true||priceObj.value < normalprice.value){
    //alert("design price should be >=" +normalprice.value);
    //priceObj.focus();
    //return false;}
    document.stepone.submit();
}
function checkKeywords() {
    txtAreaObj = document.getElementById("designKeywords")
    if (txtAreaObj.value == "") {
        alert("Please enter atleast one keyword.")
        return false;
    }
}
function loadAutoComplete() {
    myAutoComp = new YAHOO.widget.AutoComplete("designKeywords", "myContainer", myDataSource);
    myAutoComp.animVert = true;
    myAutoComp.animHoriz = true;
    myAutoComp.animSpeed = 0.5;
    myAutoComp.maxResultsDisplayed = 20;
    myAutoComp.delimChar = ",";
    myAutoComp.autoHighlight = true;
    myAutoComp.highlightClassName = "myCustomHighlightClass";
    myAutoComp.prehighlightClassName = "myCustomPrehighlightClass";
    myAutoComp.useShadow = false;
    myAutoComp.typeAhead = true;
    myAutoComp.allowBrowserAutocomplete = false;
    myAutoComp.alwaysShowContainer = false;
    myAutoComp.queryDelay = 0;
    myDataSource.maxCacheEntries = 0;
}
function showThemeSelectionDiv(div, offsetx, offsety, page) {
    var divPopUp = new PopupWindow(div);
    var anchor = null;
    var req = null;
    if (window.XMLHttpRequest) {
        req = new XMLHttpRequest();
    } else if (window.ActiveXObject) {
        req = new ActiveXObject("Microsoft.XMLHTTP");
    }
    req.onreadystatechange = function() {
        if (req.readyState == 4) {
            if (req.status == 200) {
                var response = req.responseText;
                var elm = document.getElementById(div);
                elm.innerHTML = response;
                divPopUp.offsetX = offsetx;
                divPopUp.offsetY = offsety;
                divPopUp.showPopup(anchor);
            }
        }
    }
    req.open("GET", "mkselecttheme.php?page=" + page, true);
    req.setRequestHeader("Content-Type", "text/xml");
    req.send(null);
}
function showCategorySelectionDiv(url, div, anchor, offsetx, offsety) {
    var divPopUp = new PopupWindow(div);
    var req = null;
    if (window.XMLHttpRequest) {
        req = new XMLHttpRequest();
    } else if (window.ActiveXObject) {
        req = new ActiveXObject("Microsoft.XMLHTTP");
    }
    req.onreadystatechange = function() {
        if (req.readyState == 4) {
            if (req.status == 200) {
                var response = req.responseText;
                var elm = document.getElementById(div);
                elm.innerHTML = response;
                divPopUp.offsetX = offsetx;
                divPopUp.offsetY = offsety;
                divPopUp.showPopup(anchor);
            }
        }
    }
    req.open("GET", url, true);
    req.setRequestHeader("Content-Type", "text/xml");
    req.send(null);
}
function hideThemeSelection(elmid) {
    var x = document.getElementById(elmid);
    x.style.visibility = "hidden";
}
function setCategoryIdAndValue(themeName, themeValue, themeId, themeIdValue) {
    document.getElementById(themeName).value = themeValue;
    document.getElementById(themeId).value = themeIdValue;
}
function checkIfAgreed(login) {
    if (!(document.stepthree.agreementradio.checked)) {
        alert("Please accept to the license agreement");
        return false;
    }
    return true;
}
function checkUploadedImageFile(fileObj) {
    if (!/(\.(jpg|jpeg|png))$/i.test(fileObj.value)) {
        alert("Please upload (jpg/ipeg/png) images.");
        fileObj.focus();
        return false;
    }
    else if (fileObj.value == "") {
        alert("Please browse an image ")
        fileObj.focus();
        return false
    }
    else {
        return true;
    }
}
function removeFromWishList() {
    document.getElementById("wishListToRemove").value = "";
    var checkCount = 0;
    var frmObj = document.frmWishList;
    var cBoxesObj = frmObj.elements["chkWishList[]"];
    var strChkIdsLen = 0;
    if (cBoxesObj != null) {
        if (cBoxesObj.length == null) {
            strChkIdsLen = 1;
        }
        else {
            strChkIdsLen = cBoxesObj.length;
        }
    }
    for (i = 0; i < strChkIdsLen; i++) {
        var thisCheckBox = 'box' + i;
        var elem = document.getElementById(thisCheckBox);
        if (elem.checked) {
            if (strChkIdsLen == 1) {
                document.getElementById("wishListToRemove").value = cBoxesObj.value;
            }
            else {
                if (i == 0) {
                    document.getElementById("wishListToRemove").value = cBoxesObj[i].value;
                }
                else {
                    document.getElementById("wishListToRemove").value += "," + cBoxesObj[i].value;
                }
            }
            checkCount = checkCount + 1;
        }
    }
    if (checkCount == 0) {
        alert("Please select at least one item to remove from wishlist");
        return false;
    }
    else {
        document.frmWishList.submit();
    }
}
function verifyaddToMyCart() {
    document.getElementById("wishListToRemove").value = "";
    var checkCount = 0;
    var frmObj = document.frmWishList;
    var cBoxesObj = frmObj.elements["chkWishList[]"];
    var strChkIdsLen = 0;
    if (cBoxesObj != null) {
        if (cBoxesObj.length == null) {
            strChkIdsLen = 1;
        }
        else {
            strChkIdsLen = cBoxesObj.length;
        }
    }
    for (i = 0; i < strChkIdsLen; i++) {
        var thisCheckBox = 'box' + i;
        var elem = document.getElementById(thisCheckBox);
        if (elem.checked) {
            checkCount = checkCount + 1;
        }
    }
    if (checkCount == 0) {
        alert("Please select at least one item to add into cart");
        return false;
    }
    else {
        addToMyCart();
    }
}
function addToMyCart() {
    document.getElementById("wishListToRemove").value = "";
    var checkCount = 0;
    var frmObj = document.frmWishList;
    var cBoxesObj = frmObj.elements["chkWishList[]"];
    var strChkIdsLen = 0;
    if (cBoxesObj != null) {
        if (cBoxesObj.length == null) {
            strChkIdsLen = 1;
        }
        else {
            strChkIdsLen = cBoxesObj.length;
        }
    }
    for (i = 0; i < strChkIdsLen; i++) {
        var thisCheckBox = 'box' + i;
        var elem = document.getElementById(thisCheckBox);
        if (elem.checked) {
            if (strChkIdsLen == 1) {
                document.getElementById("addToCart").value = cBoxesObj.value;
            }
            else {
                if (i == 0) {
                    document.getElementById("addToCart").value = cBoxesObj[i].value;
                }
                else {
                    document.getElementById("addToCart").value += "," + cBoxesObj[i].value;
                }
            }
        }
    }
    document.frmWishList.action = "mkwltocart.php";
    document.frmWishList.submit();
}
function clearSearch() {
    document.getElementById("txtStartDate").value = "";
    document.getElementById("txtEndDate").value = "";
    document.searchDates.submit();
}
function submitSearch() {
    if ("" == document.getElementById("txtStartDate").value) {
        alert("Please give a valid value for Start Date");
        return false;
    }
    if ("" == document.getElementById("txtEndDate").value) {
        alert("Please give a valid value for End Date");
        return false;
    }
    document.searchDates.submit();
}
function emptySearch() {
    if (document.getElementById("gosearch").value == "") {
        alert("Please input a text to search");
        return false;
    }
    document.basicsearchform.submit();
}
var uploadInputs = 0;
var MAX_UPLOAD_INPUTS = 5;
var progressBarColor;
var progressBarBGColor;
function displayAddress(divname1) {
    document.getElementById("addressoption").value = "M";
    document.getElementById(divname1).style.visibility = "visible";
    document.getElementById(divname1).style.display = "";
    document.getElementById("addressplusimage").style.visibility = "hidden";
    document.getElementById("addressplusimage").style.display = "none";
    {
        document.getElementById("addressminusimage").style.visibility = "visible";
        document.getElementById("addressminusimage").style.display = "";
    }
}
function hideAddress(divname1) {
    document.getElementById("addressoption").value = "P";
    document.getElementById(divname1).style.visibility = "hidden";
    document.getElementById(divname1).style.display = "none";
    document.getElementById("addressplusimage").style.visibility = "visible";
    document.getElementById("addressplusimage").style.display = "";
    document.getElementById("addressminusimage").style.visibility = "hidden";
    document.getElementById("addressminusimage").style.display = "none";
}
function showaddress(option, tc, tc1) {
    if (tc == 1 || tc1 == 1) {
        document.getElementById("tc").style.visibility = "hidden";
        document.getElementById("tc").style.display = "none";
    }
    if (option == 'M') {
        document.getElementById("addressinfo").style.visibility = "visible";
        document.getElementById("addressinfo").style.display = "";
        document.getElementById("addressplusimage").style.visibility = "hidden";
        document.getElementById("addressplusimage").style.display = "none";
        document.getElementById("addressminusimage").style.visibility = "visible";
        document.getElementById("addressminusimage").style.display = "";
    }
    if (option == 'P') {
        document.getElementById("addressinfo").style.visibility = "visible";
        document.getElementById("addressinfo").style.display = "";
        document.getElementById("addressplusimage").style.visibility = "hidden";
        document.getElementById("addressplusimage").style.display = "none";
        document.getElementById("addressminusimage").style.visibility = "visible";
        document.getElementById("addressminusimage").style.display = "";
    }
}
function deleteUploadedImage() {
    document.getElementById("delimageid").value = "DEL";
    document.getElementById("uploadImage").submit();
}
function setBorder(styleid) {
    document.getElementById('imgBox_' + styleid).style.border = "1px solid #000000";
}
function resetBorder(styleid) {
    document.getElementById('imgBox_' + styleid).style.border = "1px solid #E3E3E3";
}
function uploadMonitorStart() {
    var progressBarColor;
    var progressBarBGColor;
    try {
        var isFirstCall = true;
        var reqStr = '/myntra/mkmonitor.php?id=' + $('hidUploadID').value;
        updater = new Ajax.PeriodicalUpdater('uploadStatusDiv', reqStr, {
            asynchronous:true,
            frequency:0.01,
            method:'get',
            evalScripts:true,
            onSuccess:function(request) {
                if (isFirstCall) {
                    isFirstCall = false;
                    progressBarColor = $('progressBar').getStyle('color');
                    progressBarBGColor = $('progressBar').getStyle('background-color');
                    $('progressBar').style.color = progressBarBGColor;
                    $('progressBar').style.backgroundColor = progressBarColor;
                    $('progressBarFrame').style.visibility = 'visible';
                    $('progressBar').style.width = '100%';
                    $('statusText').update('Please wait...');
                } else {
                    if (request.responseText.length > 1) {
                        $('progressBar').style.color = progressBarColor;
                        $('progressBar').style.backgroundColor = progressBarBGColor;
                        $('progressBar').style.width = request.responseText + '%';
                        $('statusText').update(request.responseText + '% uploaded');
                    }
                }
            }});
    } catch(e) {
        alert('submitPostUsingAjax() failed, reason: ' + e);
    } finally {
    }
    return false;
}
function addFormInput() {
    var uploadInputs = 0;
    var MAX_UPLOAD_INPUTS = 5;
    if (uploadInputs < MAX_UPLOAD_INPUTS) {
        var upID = new String(Math.floor(Math.random() * 65535));
        var curDate = new Date();
        upID += new String(curDate.getMilliseconds());
        new Insertion.Before('submitID1', '<input type=\"hidden\" id="hidUploadID" class="upcls" name=\"UPLOAD_IDENTIFIER\" id=\"UPLOAD_IDENTIFIER\" value=\"' + upID + '\" />');
        uploadInputs++;
        return false;
    }
}
function showPostUpload(url) {
    if (window.XMLHttpRequest) {
        req = new XMLHttpRequest();
    } else if (window.ActiveXObject) {
        req = new ActiveXObject("Microsoft.XMLHTTP");
    }
    req.onreadystatechange = function() {
        if (req.readyState == 4) {
            if (req.status == 200) {
                var errorCode = document.getElementById('errorCode');
                errorCode.innerHTML = req.responseText;
                var errorType = document.getElementById('errorValue').innerHTML;
                if (errorType == "error" || errorType == "errorMime" || errorType == "errorSize") {
                    url = url + 'error=' + errorType;
                    window.open(url, '_self');
                } else {
                    url = url + 'error=false';
                }
                window.open(url, '_self');
            }
        }
    }
    req.open("GET", "showUploadError.php", true);
    req.setRequestHeader("Content-Type", "text/xml");
    req.send(null);
}
function loadZodiacWatch(styleid, zodiacsign) {
    window.location = http_loc + "/create-my/watch/" + styleid + "?zodiac=" + zodiacsign;
}
function updateTotalForPhotos(type, index, pagenum) {
    var elm1 = null;
    var elm2 = null;
    var elm3 = null;
    var value = 0;
    var pageimagecount = document.getElementById("totalcount").value;
    var to = (parseInt(pagenum) - 1) * 20 + parseInt(pageimagecount);
    var cost = 0;
    if (type == 'once') {
        for (var i = (pagenum - 1) * 20; i < to; i = i + 4) {
            for (var j = i + 1; j <= i + 4 && j <= to; j++) {
                elm1 = document.getElementById("qty_" + j + "0");
                value = elm1.value;
                if (value == '')value = 0;
                if (value > 0) {
                    document.getElementById("chkPhotos_" + j).checked = true;
                    document.getElementById('photoprices_' + j + "0").innerHTML = "Rs. " + parseInt(value) * 2.99;
                }
                cost = cost + value * 2.99;
                elm2 = document.getElementById("qty_" + j + "1");
                value = elm2.value;
                if (value == '')value = 0;
                if (value > 0) {
                    document.getElementById("chkPhotos_" + j).checked = true;
                    document.getElementById('photoprices_' + j + "1").innerHTML = "Rs. " + parseInt(value) * 6.99;
                }
                cost = cost + value * 6.99;
                elm3 = document.getElementById("qty_" + j + "2");
                value = elm3.value;
                if (value == '')value = 0;
                if (value > 0) {
                    document.getElementById("chkPhotos_" + j).checked = true;
                    document.getElementById('photoprices_' + j + "2").innerHTML = "Rs. " + parseInt(value) * 40;
                }
                cost = cost + value * 40;
            }
        }
        document.getElementById("hiddentotalcost").value = parseFloat(cost);
        cost = cost + parseFloat(document.getElementById("totalphotoprices").value);
        document.getElementById("totalcost").value = parseFloat(cost);
        document.getElementById("totalprintscost").innerHTML = "Rs." + numberformat(parseFloat(cost), 2);
        document.getElementById("totalprintscostlower").innerHTML = "Rs." + numberformat(parseFloat(cost), 2);
    }
    if (type == 'all') {
        var valuetobefilled = document.getElementById("filled_" + index).value;
        var pricetobefilled = document.getElementById('hidd_' + index).value;
        if (valuetobefilled != '' && valuetobefilled > 0 && IsNumeric(valuetobefilled)) {
            if (index == "0") {
                index1 = "1";
                price1 = "6.99";
                index2 = "2";
                price2 = "40"
            }
            if (index == "1") {
                index1 = "0";
                price1 = "2.99";
                index2 = "2";
                price2 = "40"
            }
            if (index == "2") {
                index1 = "0";
                price1 = "2.99";
                index2 = "1";
                price2 = "6.99";
            }
            for (var i = (pagenum - 1) * 20; i < to; i = i + 4) {
                for (var j = i + 1; j <= i + 4 && j <= to; j++) {
                    document.getElementById("qty_" + j + index).value = valuetobefilled;
                    document.getElementById("chkPhotos_" + j).checked = true;
                    document.getElementById('photoprices_' + j + index).innerHTML = "Rs. " + parseInt(valuetobefilled) * parseFloat(pricetobefilled);
                    cost = cost + valuetobefilled * parseFloat(pricetobefilled);
                }
            }
            for (var i = (pagenum - 1) * 20; i < to; i = i + 4) {
                for (var j = i + 1; j <= i + 4 && j <= to; j++) {
                    elm1 = document.getElementById("qty_" + j + index1);
                    value = elm1.value;
                    if (value == '')value = 0;
                    if (value > 0) {
                        document.getElementById('photoprices_' + j + index1).innerHTML = "Rs. " + parseInt(value) * parseFloat(price1);
                    }
                    cost = cost + value * price1;
                    elm2 = document.getElementById("qty_" + j + index2);
                    value = elm2.value;
                    if (value == '')value = 0;
                    if (value > 0) {
                        document.getElementById('photoprices_' + j + index2).innerHTML = "Rs. " + parseInt(value) * parseFloat(price2);
                    }
                    cost = cost + value * price2;
                }
            }
            document.getElementById("hiddentotalcost").value = parseFloat(cost);
            cost = cost + parseFloat(document.getElementById("totalphotoprices").value);
            document.getElementById("totalcost").value = parseFloat(cost);
            document.getElementById("totalprintscost").innerHTML = "Rs." + numberformat(parseFloat(cost), 2);
            document.getElementById("totalprintscostlower").innerHTML = "Rs." + numberformat(parseFloat(cost), 2);
        }
    }
    if (type == 'checkbox') {
        if (document.getElementById("chkPhotos_" + index).checked == false) {
            document.getElementById("qty_" + index + "0").value = "";
            document.getElementById('photoprices_' + index + "0").innerHTML = "";
            document.getElementById("qty_" + index + "1").value = "";
            document.getElementById('photoprices_' + index + "1").innerHTML = "";
            document.getElementById("qty_" + index + "2").value = "";
            document.getElementById('photoprices_' + index + "2").innerHTML = "";
            if (document.getElementById("filled_0").value > 0)
                document.getElementById("filled_0").value = "";
            if (document.getElementById("filled_1").value > 0)
                document.getElementById("filled_1").value = "";
            if (document.getElementById("filled_2").value > 0)
                document.getElementById("filled_2").value = "";
            for (var i = (pagenum - 1) * 20; i < to; i = i + 4) {
                for (var j = i + 1; j <= i + 4 && j <= to; j++) {
                    elm1 = document.getElementById("qty_" + j + "0");
                    value = elm1.value;
                    if (value == '')value = 0;
                    cost = cost + value * 2.99;
                    elm2 = document.getElementById("qty_" + j + "1");
                    value = elm2.value;
                    if (value == '')value = 0;
                    cost = cost + value * 6.99;
                    elm3 = document.getElementById("qty_" + j + "2");
                    value = elm3.value;
                    if (value == '')value = 0;
                    cost = cost + value * 40;
                }
            }
            document.getElementById("hiddentotalcost").value = parseFloat(cost);
            cost = cost + parseFloat(document.getElementById("totalphotoprices").value);
            document.getElementById("totalcost").value = parseFloat(cost);
            document.getElementById("totalprintscost").innerHTML = "Rs." + numberformat(parseFloat(cost), 2);
            document.getElementById("totalprintscostlower").innerHTML = "Rs." + numberformat(parseFloat(cost), 2);
        }
    }
}
var totalValue = new Array();
var totalInputQty = new Array();
var totalCostValue = 0;
function filledSelectedSizes(index) {
    var count = document.getElementById("totalimage").value;
    var valuetobefilled = document.getElementById("filled_" + index).value;
    var priceVal = document.getElementById('hidd_' + index).value;
    var totalValueArray = new Array();
    if (valuetobefilled != '' && valuetobefilled > 0 && IsNumeric(valuetobefilled)) {
        var photocurrentindexprice = 0
        for (var i = 1; i <= count; i++) {
            var quantity = document.getElementById("qty_" + i + index).value;
            if (quantity != '' && quantity > 0) {
                photocurrentindexprice = parseFloat(photocurrentindexprice) + parseFloat(document.getElementById('photoprices_' + i + index));
                var filledtotalcost = document.getElementById("filledtotalcost").value;
                document.getElementById("filledtotalcost").value = parseFloat(filledtotalcost) - parseFloat(photocurrentindexprice);
            }
            document.getElementById("qty_" + i + index).value = valuetobefilled;
            totalCostValue = totalCostValue + parseInt(valuetobefilled) * parseFloat(priceVal);
            if (valuetobefilled > 0) {
                if (document.getElementById("qty_" + i + index).value > 0) {
                    document.getElementById("chkPhotos_" + i).checked = true;
                }
                else {
                    document.getElementById("chkPhotos_" + i).checked = false;
                }
            }
            document.getElementById('photoprices_' + i + index).innerHTML = "Rs. " + parseInt(valuetobefilled) * parseFloat(priceVal);
        }
        document.getElementById("filledtotalcost").value = totalCostValue;
        document.getElementById("totalcost").value = totalCostValue;
        document.getElementById("totalprintscost").innerHTML = "Rs." + numberformat(totalCostValue, 2);
    }
}
function useAffiliateSelectedImageForPhoto(originalimage, imagepath, styleid) {
    document.uploadImage.originalimage.value = originalimage;
    document.uploadImage.myimage.value = imagepath;
    document.uploadImage.referrer.value = "uploadPhotoImage";
    document.uploadImage.referredTo.value = "AFFILIATE";
    document.uploadImage.cstatus.value = "customized";
    if (typeof styleid != 'null') {
        document.uploadImage.productStyleId.value = styleid;
    }
    document.uploadImage.submit();
}
function notCustomizedPhotoPrints(pagetype, total) {
    if (document.getElementById("totalcost").value == "0" || document.getElementById("totalcost").value == "") {
        alert("please add some quantity");
        return;
    }
    else {
        document.getElementById("referrer").value = "addToCart";
        document.getElementById("referredTo").value = pagetype;
        var x = document.getElementById("uploadImage");
        document.getElementById("uploadImage").submit();
    }
}
function paginate(pgNum) {
    document.uploadImage.referrer.value = "paginatePhoto";
    document.uploadImage.referredTo.value = "PHOTOPRINT";
    document.uploadImage.pg.value = pgNum;
    document.uploadImage.submit();
}

//function to obtain captcha image
function get_captcha_image(captcha_img_id, captcha_session_key) {//captcha_session_key => to handle multiple captcha in session
    var http = new getHTTPObject();

    //if session key is not passed take a common key as "common"
    if (typeof captcha_session_key == 'undefined') {
        var captcha_session_key = 'common';
    }
    var SizeSelectionURL = http_loc + "/captchaAjax.php?sess_key=" + captcha_session_key;
    http.open("GET", SizeSelectionURL, true);
    http.onreadystatechange = function() {
        if (http.readyState == 4) {
            if (http.status == 200) {
                var result = http.responseText;
                var imgObject = document.getElementById(captcha_img_id);
                imgObject.src = http_loc + result;
            } else {
                alert(http.status + "====" + http.statusText);
            }
        }
    }
    http.send(null);
}
//function to obtain captcha image ends

/* function to limit number of chars in textarea
 * @param:textareaObj,counttextboxObj,maxlimit
 */
function limit_textarea(textarea, count, maxlimit) {
    if (textarea.value.length > maxlimit) {// if too long...trim it!
        textarea.value = textarea.value.substring(0, maxlimit);
    }
    else
        count.value = maxlimit - textarea.value.length;
}

function LTrim(str)
{
    var whitespace = new String(" \t\n\r");
    var s = new String(str);

    if (whitespace.indexOf(s.charAt(0)) != -1) {
        // We have a string with leading blank(s)...
        var j = 0, i = s.length;
        // Iterate from the far left of string until we
        // don't have any more whitespace...
        while (j < i && whitespace.indexOf(s.charAt(j)) != -1)
            j++;
        // Get the substring from the first non-whitespace
        // character to the end of the string...
        s = s.substring(j, i);
    }
    return s;
}

function RTrim(str)
{
    // We don't want to trip JUST spaces, but also tabs,
    // line feeds, etc.  Add anything else you want to
    // "trim" here in Whitespace
    var whitespace = new String(" \t\n\r");
    var s = new String(str);

    if (whitespace.indexOf(s.charAt(s.length - 1)) != -1) {
        // We have a string with trailing blank(s)...
        var i = s.length - 1;       // Get length of string
        // Iterate from the far right of string until we
        // don't have any more whitespace...
        while (i >= 0 && whitespace.indexOf(s.charAt(i)) != -1)
            i--;
        // Get the substring from the front of the string to
        // where the last non-whitespace character is...
        s = s.substring(0, i + 1);
    }
    return s;
}

function Trim(str)
{
    return RTrim(LTrim(str));
}


function buildAndSubmitPostParam(postUrl, paramArray, resultTagId) {
    var i = 0,element,elementValue,postParam;
    for (i = 0; i < paramArray.length; i++) {
        element = Trim(paramArray[i]);
        elementValue = document.getElementById(paramArray[i]).value;
        //elementValue = elementValue.replace(/'/gi, "\'");
        //elementValue = elementValue.replace(/"/gi, '\"');
        elementValue = Trim(elementValue);
        elementValue = encodeURIComponent(elementValue);
        paramArray[i] = paramArray[i].concat("=", elementValue);
    }
    postParam = paramArray.join('&');
    ajaxDynamicPostRequest(postUrl, postParam,resultTagId);
}

function ajaxDynamicPostRequest(posturl, postparam,resultTagId) {
    var url = posturl;
    var params = postparam;
    var http = getHTTPObject();

    http.onreadystatechange = function() {
        if (http.readyState == 4) {
            if (http.status == 200){
				if(resultTagId != null){
					document.getElementById(resultTagId).innerHTML = '';
					document.getElementById(resultTagId).innerHTML = http.responseText;
				}else{
					document.getElementById("popupmessage").innerHTML = '';
					document.getElementById("popupmessage").innerHTML = http.responseText;
				}
			}else
                alert(http.status + "====" + http.statusText);
        }
    }
    http.open("POST", url, true);
    http.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    http.setRequestHeader("Content-length", params.length);
    http.setRequestHeader("Connection", "close");
    http.send(params);
}
