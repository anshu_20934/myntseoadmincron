{if $redirectStyleFile eq 'hanumanproducts' }
     {assign var="customize"  value = $redirectStyleFile}
{else}
     {assign var="customize"  value = "products"}
{/if}
<div class="head">
    <p>view design on different styles</p>
</div>

 <!--<div class="links">
 {if $popup_image_path ne ""}
      <p>
      <a href="?img={$popup_image_path|substr:21:70}" class="thickbox">
	 <img src="{$cdn_base}/skin1/mkimages/icon_fullsize.gif">&nbsp;<strong>large image</strong></a>
      </p>
 {else}
 {assign var=popup_image_path value="./default_image.gif"}
    <p>
      <a href="{$popup_image_path}" class="thickbox">
       <img src="{$cdn_base}/skin1/mkimages/icon_fullsize.gif">&nbsp;<strong>large image</strong></a>
    </p>
 {/if}
 </div>-->

<!--<div class="links">
<p>
     Style: {$productStyleDetails[0][2]}
    <a href="javascript:showPopupDiv();"><img src="{$skinImages}/help-icon.gif"/></a>
    info
</p>
</div>-->
<div style="text-align:center;margin:0px auto;">
<div id="mygallery" class="stepcarousel" style="width:76%;float:left;height:70px;_height:70px;border:0px solid #333333;margin-left:45px;_margin-left:20px;overflow-x: hidden; overflow-y: hidden;">
	<div class="belt" style="float: left; width: 5000px; left: 0px;">
		<div style="width:auto;float:left;">
			{section name=num loop=$productStyleIconImages}
				<div class="panel" style="height:77px;width:60px;font-size:0.8em;text-align:center;">
					{if $productStyleId eq $productStyleIconImages[num].id}
						<div class="optionselect" style="width:40px;height:40px;margin:5px;float:left;padding:2px;border:2px solid #000000;">
							<img  src="{$productStyleIconImages[num].image_i}"width="40" height="40"  ALT="Choose Style  for {$productStyleIconImages[num].name}"	title="{$productStyleIconImages[num].name}"/>
						</div>
					{else}
						<div class="optionnotselect" style="width:40px;height:40px;margin:5px;float:left;padding:2px;border:2px solid #dddddd;">
						<a rel="nofollow" href="{$currentURL}?productStyleId={$productStyleIconImages[num].id}">
							<img id="carousel_{$productStyleIconImages[num].id}" src="{$cdn_base}/skin1/images/spacer.gif" lazy-src="{$productStyleIconImages[num].image_i}" class="lazy" width="40" height="40" ALT="Choose Style  for {$productStyleIconImages[num].name}" title="{$productStyleIconImages[num].name}">
						</a>
						</div>
					{/if}
				</div>
			{/section}
		</div><!--links-->	
	</div><!--belt-->
</div><!--gallery-->
<div style="width:75%;_width:77%;float:left;margin-left:43px;_margin-left:18px;">
<div id='leftnavig' style="float:left;width:auto;height:20px;margin-top:10px;margin-left:12px;_margin-left:10px;position:relative">
<table >
  <tr>
  <td><a  href="javascript:void(0);" onclick="javascript:stepcarousel.stepBy('mygallery', -1)" style="text-decoration:none;" ><img src="{$cdn_base}/skin1/mkimages/previous_tpl.gif" /></a>
  </td>
   <td>
     <span style='font-size:0.7em;text-align:center;height:20px;'  ><a href="javascript:void(0);" onclick="javascript:stepcarousel.stepBy('mygallery', -1)" style="text-decoration:none;" >previous</a></span>
  </td>
  </tr>
</table>
</div>

<div id='rightnavig' style="width:auto;float:right;height:20px;margin-top:10px;">

  <table >
  <tr>
  <td><span style='font-size:0.7em;height:20px;'  ><a href="javascript:void(0);" onclick="javascript:stepcarousel.stepBy('mygallery', 1)" style="text-decoration:none;" >next</a></span>
  </td>
   <td>
   <a href="javascript:void(0);" onclick="javascript:stepcarousel.stepBy('mygallery', 1)" style="text-decoration:none;" >
    <img src="{$cdn_base}/skin1/mkimages/next_tpl.gif" /></a> 
  </td>
  </tr>
</table>
</div>
</div>
</div>
<div class="foot" style="clear:both;"></div>
<!--<div class="foot">&nbsp;</div>-->
