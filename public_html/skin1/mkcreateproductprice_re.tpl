<div class="super">
	<div>
		<h2 style="font-size:0.9em;font-weight:bold;line-spacing:0.5;">{$designer[6]} {$productTypeLabel} 
		{if $size_chart_image_path neq '' }
		&nbsp; <a href="?img={$size_chart_image_path}" id="sizechart" class="thickbox"><span style="font-size:0.8em;font-weight:normal;">size chart</span></a>
		{/if}
		</h2>
		</div>
	
	<p>
	price:
	<span style='font-size:15px;font-weight:bold;'> Rs.&nbsp;{$productStyleDetails[0][5]}</span>
	{if $accountType != 1}
	&nbsp;
	{if $pagename eq 'proto'}<span class="mandatory">*</span>{/if}
  </span>{if $pagename ne 'proto'}<font style="font-size:0.7em;"><span class="mandatory">
  {if ($productStyleDetails[0][5] + $cart_amount) gt $free_shipping_amount}
                                                       <b>FREE Shipping in India!</b>                                                     
                                  {/if}
  </span></font>{/if}
	{else}
	&nbsp;
	{if $pagename eq 'proto'}<span class="mandatory">*</span>{/if}
  </span>{if $pagename ne 'proto'}<font style="font-size:0.7em;"><span class="mandatory">(<b>Inclusive of shipping and Taxes</b>)</span></font>{/if}
	{/if}
	
  </p>
	
</div>
{if $pagename eq 'proto'}
<div class="links">
 <p><span class="mandatory">*</span>12.5% VAT will be added to sales price<br/>
    <span class="mandatory">*</span>Shipping cost extra , see <a href="javascript:showMessagePopup();" ><b>details</b></a>
  </p>
  <br>
</div>

{/if}
<div class="links">
	<br>
	{if $productOptions}
	<p class="super" style="text-align:left;font-weight:bold;">Quantity:</p>
	{/if}
	<div style="text-align:left;padding-left:5px;">
		<form action="mkretrievedataforcart.php?pagetype=productdetail" name="hiddenForm" id="hiddenForm" method="post">
		{assign var='i' value=1}
		{if $productOptions}
         {section name=options loop=$productOptions}
			{section name=values loop=$productOptions[options][1]}
				{$productOptions[options][1][values][0]}<input type="hidden" name="sizename[]" id="sizename{$i}" value="{$productOptions[options][1][values][0]}">&nbsp;<input type="text" name="sizequantity[]" id="sizequantity{$i}" style="width:30px;" onchange="javascript:updateTotalQuantity();">&nbsp;&nbsp;
				{assign var='i' value=$i+1}
			{/section}
         {/section}
		 {else}
		 {/if}
		 <input type="hidden" id="count" value="{$i}">
		<input type="hidden" name="productStyleId" value="{$productStyleDetails[0][0]}">
		<input type="hidden" name="productId" value="{$productid}">
		<input type="hidden" name="customizationAreaId" id="caId" value="">
		<input type="hidden" name="customizationAreaName" id="custAreaName" value="custArea0">
		<input type="hidden" name="quantity"  id="quantity" value="1">
		<input type="hidden" name="uploadedImagePath" id="imagepath" value="">
		<input type="hidden" name="unitPrice" value="{$productStyleDetails[0][5]}">
		<input type="hidden" name="productId" value="{$productid}">
		<input type="hidden" name="pagetype" value="productdetail">
		<input type="hidden" name="totalOptions" id="totalOptions" value="{$Options}">
		<input type="hidden" name="productName" id="productName" value="{$designer[6]}">
        <input type="hidden" name="productTypeName" id="productTypeName" value="{$productTypeLabel}">
		<input type="hidden" name="windowid" value="{$windowid}">

		{section name=options loop=$productOptions}
		<input type="hidden" name="option{$smarty.section.options.index}name"  id="option{$smarty.section.options.index}name"  value="{$productOptions[options][0]}">
		<input type="hidden" name="option{$smarty.section.options.index}value"  id= "option{$smarty.section.options.index}value" value="">
		<input type="hidden" name="option{$smarty.section.options.index}id"  id= "option{$smarty.section.options.index}id"  value="" >
		{/section}

		<input type="hidden" name="referrer" value="productDetail">
		<input type="hidden" name="referredTo"  id= "referredTo" value="">
        {if $i ne 1}
            <input type="hidden" name="quantityMenu" id="quantityMenu" size="3" maxlength="4" />
        {else}
            Quantity&nbsp;&nbsp;<input type="text" name="quantityMenu" id="quantityMenu" style="width:30px" maxlength="4" />&nbsp;&nbsp;
        {/if}
	<input class="submit" type="button" border="0" value="add to cart" name="submitButton" style="font-size:16px;height:25px;width:100px;" onClick="javascript: if(notCustomized('{$cstatus}', 'mkretrievedataforcart.php?windowid={$windowid}&at=c'));"/>
		 </form>
   </div>
   <br>
   {if $productOptions}
   <!-- div style="text-align:right;"><a href="javascript:showchartDiv();">view size/fitting chart</a></div -->
   {/if}
	<div style="clear:both; padding-left:5px;">

<!--	<input class="submit" type="button" border="0" value="add to wishlist" name="submitButton"
onClick="javascript: if(checkIfLoggedIn('{$login}')) if(notCustomized('{$cstatus}','{$pageType}'));"/>-->
	</div>
 </div>
  <div class="foot" style="border-bottom:1px dashed white"></div>
{literal}
	<script type="text/javascript">
		function hidechartDiv()
		{
			document.getElementById('chartpopup').style.visibility = 'hidden';
		}
		function showchartDiv()
		{
		    document.getElementById('chartpopup').style.visibility = 'visible'; 
		}
		function updateTotalQuantity()
		{
			var quantity = 0;
			quantity = quantity*1;
			var count = document.getElementById('count').value;
			//alert(count);
			for(var i=1; i<count; i++){
				var id = 'sizequantity'+i;
				var value = document.getElementById(id).value;
				value = value*1;
				quantity = quantity + value;
			}
			document.getElementById("quantityMenu").value = quantity;
		}
	</script>
{/literal}


 


