		<div class="content-left left span-33 clearfix pdp-content-div">
				{if $brand_logo}
                <div class="pdp-brand-logo">
                	<a href="{$http_location}/{$brandname|lower|replace:' ':'-'}" title="{$brandname}"><img src="{$brand_logo}" alt="{$brandname}"></a>
                </div>
                {/if}
		        <div class="title-strip">
        		<!--  Page Title -->
                  <h1 class="fn title">{$productDisplayName}</h1>
                  <div class="social-buttons">
                        <div class="fb-like-button left"> <fb:like href="{$pageURL}" send="false" layout="button_count" width="90" show_faces="false" action="like"></fb:like></div>
                        <div class="g-plus-button left"><g:plusone size="medium" count="true" href="{$pageURL}"></g:plusone></div>
                   </div>
        		</div>
		
			<div class="span-15 last">
			    
				<div class="product-block">
					<div class="product-image-preview left" id="preview_image_div">
						<div class="imageframe">
							<img  src="{$defaultImage|replace:'_images':'_images_360_480'}" alt="{$productDisplayName|escape:'htmlall'}" width="360" height="480" id="finalimage" style="position:relative;z-index:1;">
							<div class="preview-loading hide"></div>
							<div class="nav-buttons">
							{if $discountlabeltext}<div class="discount-label val"><span>{$discountlabeltext}</span></div>{/if}
								<div class="preview-prev preview-overlay-control"><a href="javascript:void(0)" class="prev-link no-decoration-link"></a></div>
								<div class="preview-next preview-overlay-control"><a href="javascript:void(0)" class="next-link no-decoration-link"></a></div>
							</div>
						</div>
					</div>

				    <input 	type="hidden" id="styledescription" name="description" value="({$productTypeDetails|escape}) {$descriptionAndDetails|escape}" >
				    <input type="hidden" value="{$cstatus}" id="cstatus">
				    <div class="questionLayer" id="question" style="display:none;"></div>
					<div class="opaqueLayer" id="shadow" style="display:none;"></div>
					
				</div>
				<div class="left clearfix product-thumbnails">
			    	<input type="hidden" value="thumb_0" class="loadedimage" name="loadedimage">
                    <div class="v-tabs thumb-views">
						{assign var=i value=0}
                        {section name="area" loop=$area_icons}
                            <div id="area_{$area_icons[area].name}">
                                <a  {if $area_icons[area].name eq $curr_sel_area} class="selected" {/if} id="thumb_{$i}" href="javascript:void(0)" rel='"prodimage":"{$area_icons[area].image|replace:"_images":"_images_360_480"}","intermediatezoomimage":"{$area_icons[area].image|replace:"_images":"_images_540_720"}","fullimage":"{$area_icons[area].image|replace:"_images":"_images_1080_1440"}"'>
                                    {if $i eq 0}
                                        <img width="48" height="64" src='{$area_icons[area].image|replace:"_images":"_images_48_64"}' alt="{$productDisplayName|escape:'htmlall'} - {$area_icons[area].name} view">
                                    {else}
                                        <img width="48" height="64" src="{$cdn_base}/skin1/images/spacer.gif" class="lazy" lazy-src='{$area_icons[area].image|replace:"_images":"_images_48_64"}' alt="{$productDisplayName|escape:'htmlall'} - {$area_icons[area].name} view">
                                    {/if}
                                </a>
                            </div>
						{assign var=i value=$i+1}
                        {/section}
                    </div>
                </div>
	</div>

			<div class="product-detail-block last right">
				<div class="pro_cart mb10 clearfix">
                 <div style="display:block;float:right;width:188px;margin-top:20px;">
                    <ul id="service-panel">
                        <li><b>FREE SHIPPING</b> IN INDIA</li>
                        <li><b>CASH</b> ON DELIVERY</li>
                        {if $deliveryKnowMoreEnabled}
                            <li><b class="num">24 HOURS</b> <a href="javascript:void(0)" onclick="gaTrackEvent('sports_pdp_{$productTypeLabel}_{$brandname}', 'delivery_know_more', '');" class="deliveryknowmore-link no-decoration-link dotted-line">Dispatch Time</a></li>
                        {/if}
                        <li><b class="num">30 DAY</b> <a href="javascript:void(0)" onclick="gaTrackEvent('sports_pdp_{$productTypeLabel}_{$brandname}', 'returns_know_more', '');" class="ReturnPolicy-popup-link no-decoration-link dotted-line">Returns Policy</a></li>
                    </ul>
                 </div>
                 <div style="width:342px;margin-right:10px;float:left;">                        
                    {if !$disableProductStyle}
                        {if $sizeOptions}
                        <div class="clearfix flat-size-options">
                                <span class="mrp-b">Select Size: </span> <br>
                            <div class="size-options" id="size-options">
                                {assign var='j' value=1}
                                {foreach from=$sizeOptions key=key item=val}
                                    {if $renderTooltip}
                                        <a href="javascript:void(0)" class="vtooltip no-decoration-link" rel="{$sizeOptionsTooltipHtmlArray[$key]}" onclick="sizeSelect({$j},this)">{$val}</a>
                                    {else}
                                        <a href="javascript:void(0)" class="no-decoration-link" rel="" onclick="sizeSelect({$j},this)">{$val}</a>
                                    {/if}
                                    {assign var='j' value=$j+1}
                                {/foreach}
                                <div class="size-options-error-msg clear" style="display:none;">Please select a size.</div>
                            </div>

                            <div>
                                {if $size_chart_image_path neq '' && $renderSizeChart}
                                    <span class="b-link size-chart-link left">
                                        <a href="{$size_chart_image_path}" class="thickbox no-decoration-link" id="sizechart" onclick="gaTrackEvent('sports_pdp_{$productTypeLabel}_{$brandname}', 'size_chart', '');">Size chart</a>
                                    </span>
                                {/if}
                                {if  $size_chart_image_path neq '' && $renderSizeChart && $notifymefeatureon}
                                <span class="hdivers">|</span>
                                {/if}
                                {if $notifymefeatureon}
                                <span class="b-link size-chart-link left">
                                   <a href="javascript:void(0)" class="no-decoration-link" style="white-space: nowrap;" id="notifymelink" onclick="_gaq.push(['_trackEvent', 'sports_pdp_{$productTypeLabel}_{$brandname}', 'didnt_find_size']);opennotifyme()");">Didn't find your size?</a>
                                </span>
                                {/if}
                            </div>
                        </div>

                        {/if}


                                <div class="mt10 last mb10 clear">
                                   {if $discountamount > 0}
                                        <div class="mrp-block">
                                            <span class="mrp-b">MRP</span>
                                            <span class="mrp"> <b>Rs {$originalprice}</b></span>

                                            <span>, Now</span>
                                             <span class="dis-price clearfix"><strong>Rs. {if $discountamount > 0}{math equation="x - d" x=$originalprice d=$discountamount assign="finalamount"}{$finalamount|round|string_format:"%.2f"}{else}{$originalprice|round|string_format:"%.2f"}{/if}</strong></span>
                                        </div>
                                   {else}
                                        <div>
                                            <span class="dis-price pt10 clearfix">
                                                <span class="mrp-b" style="display: block; line-height: 1px;">Now</span>
                                                <strong>Rs. {if $discountamount > 0}{math equation="x - d" x=$originalprice d=$discountamount assign="finalamount"}{$finalamount}{else}{$originalprice}{/if}</strong>
                                            
                                            </span>
                                        </div>
                                   {/if}     
                                </div>

                            <div class="clearfix b1 mb10">
                            <span class="left">
                                {if $buynowdisabled}
                                    <a class="btn-disabled corners span-6 left clearfix">Buy now</a>
                                {else}{literal}
                                    <a class="orange-bg cart-btn corners span-6 left clearfix">Buy now &raquo;</a>
                                    {/literal}
                                {/if}
                                {if $buynowdisabled}
                                <div class="out-of-stock-msg clearfix left">Currently this product is sold out.</div>
                                {/if}
                            </span>
                            {if $showtelesalesNumber}
                                    <span>or <b style="color:#ff9412">call {$telesalesNumber}</b> <br> to place an order</span>
                            {/if}
                        </div>
                        {else}
                        <div class="out-of-stock-msg clearfix left">Currently this product is sold out.</div>
                        {/if}

                        {*Cashback*}
                          {*<div style="display:inline-block;background: #eee; padding:5px 10px;margin:10px 10px 10px 20px;width:135px">& get <b>Rs
                              {if $discounttype eq 'rule'}
                              {math equation="((x - d) * y)/z" x=$originalprice d=$discountamount y=10 z=100  assign="cashback"}{$cashback|round|string_format:"%d"}
                              {else}
                              {math equation="(x * y)/z" x=$discountprice y=10 z=100  assign="cashback"}{$cashback|round|string_format:"%d"}
                              {/if}
                          </b> Cash Back
                          </div>*}
                        {*Cashback End*}
                        {if $discountamount > 0}
                            <div class="pdpcb-block">And get <b>Rs. {$discountamount|round|string_format:"%.2f"}</b>
                                {*{if $discountdisplaytext}<span>{$discountdisplaytext}</span>{/if}*}
                                {if $discountlabeltext}({$discountlabeltext}) <b style="color:#808080">Discount</b>{/if}

                            </div>
                        {/if}
                        <div class="pdpcb-block hide" style={if $cashback_gateway_status eq 'on' && $enableCBOndiscountedProducts eq 'true' && $cashback_displayOnPDPPage eq '1'}"display:block;"{elseif $cashback_gateway_status eq 'on' && $enableCBOndiscountedProducts eq 'false' && ($discountamount eq '' || $discountamount eq 0) && $cashback_displayOnPDPPage eq '1'}"display:block;"{else}"display:none;"{/if}>
                            And get <b>Rs.
                            {if $discounttype eq 'rule'}
                                {if $discountamount eq ''}{assign var="discountamount" value="0"}{/if}
                                  {math equation="(x - d )* y " x=$originalprice d=$discountamount y=0.1  assign="cashback"}{$cashback|round|string_format:"%.2f"}
                            {else}
                                  {math equation="x * y" x=$originalprice y=0.1  assign="cashback"}{$cashback|round|string_format:"%.2f"}
                            {/if}
                            </b> (10%) 
                                <span class="poplink"><b class="popshow">Cashback</b></span>
                        </div>

                        <div class="pdp-cb-pop hide">
                            <img src="http://d6kkp3rk1o2kk.cloudfront.net/skin1/images/cart-removebtn.png" alt="close" class="popclose" style="position:absolute;top:5px;right:10px;cursor:pointer">

                        <div class="p5 black-bg5 mb10" style="border-bottom:1px solid #ccc;">Cashback</div><div class="lb">
                                <div>AFTER YOU PAY<span>Price<i> Rs. </i> <b class="red">{if $discountamount > 0}{math equation="x - d" x=$originalprice d=$discountamount assign="finalamount"}{$finalamount}{else}{$originalprice}{/if}</b></span></div>
                                <div>YOU GET<span>Cashback* <i>Rs. </i><b class="green">{$cashback|round|string_format:"%.2f"}</b></span></div>
                                <div style="border-top:1px solid #333;">VALUE OF PRODUCT <span><i>Rs. </i><b>{if $discountamount > 0}{math equation=x-z x=$finalamount y=$discountamount z=$cashback assign="valueofproduct"}{else}{math equation=x-z x=$originalprice z=$cashback assign="valueofproduct"}{$valueofproduct|round|string_format:"%.2f"}{/if}</b></span></div>
                            </div>

                            <div class="rb">
                            <div style="text-align:center;"> <b>How does Cashback work?</b></div>
                                On purchasing this product, you will receive a cashback credit upto the amount indicated here. Cashback is calculated on item value excluding any coupon discounts. You can redeem this on any of your subsequent purchases at Myntra. Cashback will be credited to your account instantly if you make your purchase using online payment.
                                <div style="text-align:center;"><a href="{$http_location}/myntclub#cashback" target="_blank">I want to know more</a></div>
                            </div>
                        </div>
                        <div style="color:#808080;font-size:11px;text-align:center;padding-top:4px;">All products sold on MYNTRA are brand new and 100% genuine.</div>

            </div>



            </div>

				{if $descriptionAndDetails|trim neq ""}	
                	<div class="bold black-bg5 prod-desc corners-tl-tr">Product Description</div>
					<div class="product-description black-bg5 corners-bl-br">
						{$descriptionAndDetails}
					</div>
				{/if}
			</div>
			<div class="clear">&nbsp;</div>
		</div>
        
