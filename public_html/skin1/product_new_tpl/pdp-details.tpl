<div class="product-detail-block last right">
	<div class="product-detail-block-left left">
		<h1 class="product-title">{$productDisplayName}</h1>
		<div class="pricing-info mt10">
			{if $discountamount > 0}
			<span class="dprice"><span>Rs.</span> {if $discountamount > 0}{math equation="x - d" x=$originalprice d=$discountamount assign="finalamount"}{$finalamount|round|number_format:0:".":","}{else}{$originalprice|round|number_format:0:".":","}{/if}</span>
			<span class="oprice strike"> {$originalprice|round|number_format:0:".":","}</span>
			{else}
			<span class="dprice"><span>Rs.</span> {$originalprice|round|number_format:0:".":","}</span>	
			{/if}
		</div>
		{if !$disableProductStyle}
		
        	
            <div class="add-pricing-info">
        {if $discountlabeltext}
            <div class="clearfix mt5 discount-block" {if $show_combo eq false} style="border:0px none;"{/if}>
                {if $show_combo }
                    <span style="text-transform: uppercase;color: #d20000;">Special Combo Offer!</span><BR>
                    <span style="overflow:hidden;line-height:22px;" class="pdp-sploff pdp-trigger tool-text">
                    {eval var=$discounttooltiptext}
                    </span><BR>
                {else}
                    <div class="pdp-discount-align pdp-discount-icon" ><span style="overflow:hidden;line-height:22px;"><i class="{if $discounttype eq 1}pdp-dis-icon{elseif $discounttype eq 8}pdp-dis-icon{elseif $discounttype eq 4}pdp-gift-icon{elseif $discounttype eq 32}pdp-coupon-icon{else}pdp-dis-icon{/if}">&nbsp;</i></span></div>
                    <div class="pdp-discount-align pdp-sploff pdp-trigger" style="clear:none;">
                    {eval var=$discountlabeltext}</div>
                {/if}

                {*<a href="javascript:void(0);" id="disc_tool_tip" >View Offer Details</a>*}
                {if $show_combo}<a class="combo-overlay-btn action-btn left" data-styleid="{$productStyleId}">&nbsp;Create Combo &raquo;</a>{/if}
            </div>
        {else}
            
            <div class="hide" style={if $cashback_gateway_status eq 'on' && $enableCBOndiscountedProducts eq 'true' && $cashback_displayOnPDPPage eq '1'}"display:block;"{elseif $cashback_gateway_status eq 'on' && $enableCBOndiscountedProducts eq 'false' && ($discountamount eq '' || $discountamount eq 0) && $cashback_displayOnPDPPage eq '1'}"display:block;"{else}"display:none;"{/if}>
			   	And get Rs.
			   	<span class="bold"> {$cashback|round|string_format:"%.2f"}</span> (<span class="bold">10%</span>) <span class="poplink"><b class="popshow">Cashback</b></span>
			</div>
        {/if}
        </div>
            {if $sizeOptionSKUMapping}	
            <div class="clearfix flat-size-options">
            	<span>Select Size: </span><br/>
                <div class="size-options" id="size-options">
		        	{assign var='j' value=1}
		            {foreach from=$sizeOptionSKUMapping key=key item=val}
		            	{if $renderTooltip}
		                <a href="javascript:void(0)" class="action-btn pdp-action-btn vtooltip" rel="{$sizeOptionsTooltipHtmlArray[$key]}" data-tooltipTop="{$tooltipTop}" onclick="sizeSelect({$key},{$j},this)">{$val}</a>
		                {else}
		                <a href="javascript:void(0)" class="action-btn pdp-action-btn" rel="" onclick="sizeSelect({$key},{$j},this)">{$val}</a>
		                {/if}
						{assign var='j' value=$j+1}
					{/foreach}
		            <div class="size-options-error-msg clear" style="display:none;">Please select a size.</div>
				</div>
				<div class="size-chart-section">
			    
						{if $dynamicSizeChart eq 'true' && $sizeRepresentationImageUrl neq ''}
                            <span class="b-link size-chart-link left">
                                <span class="no-decoration-link" id="sizechart-new" >Not sure of your size?</span>
                             </span>
                        {elseif $size_chart_image_path neq ''}
						       <span class="b-link size-chart-link left">
                                <a href="{$size_chart_image_path}" class="thickbox no-decoration-link" id="sizechart" onclick="gaTrackEvent('sports_pdp_{$productTypeLabel}_{$brandname}', 'size_chart', '');">Size chart</a>
                            </span>
							
                        {/if}
                        {if  $sizeRepresentationImageUrl neq ''}
			     			{if $notifymefeatureon}
								<!-- <span class="hdivers">|</span> -->
							{/if}
                        {/if}
						{if $notifymefeatureon}
						<!-- <span class="b-link size-chart-link left ">
							<a href="javascript:void(0)" class="no-decoration-link" style="white-space: nowrap;" id="notifymelink" onclick="_gaq.push(['_trackEvent', 'sports_pdp_{$productTypeLabel}_{$brandname}', 'didnt_find_size']);opennotifyme()");">Didn't find your size?</a>
						</span>
						 -->
						{/if}
                </div>
			</div>
            {/if}
            {if $productDisclaimerTitle|trim neq ""}
			<div class="mk-product-disclaimer-container">
				<div class="mk-product-disclaimer">
					{$productDisclaimerTitle} {if $productDisclaimerText|trim neq ""}<a href="javascript:void(0)" class="articletype-disclaimer-link link">Know More</a>{/if}
				</div>
			</div>
			{/if}
			<div class="clearfix mb10 mt10 buy-now-block">
            	<span class="left">
	                {if $buynowdisabled}
		            	<a class="btn-disabled corners span-6 left clearfix">Buy now</a>
					{else}
						{literal}<a class="orange-bg cart-btn corners span-6 left clearfix">Buy now &raquo;</a>{/literal}
					{/if}
	                {if $buynowdisabled}
	                	<div class="out-of-stock-msg clearfix left">Currently this product is sold out.</div>
	                 {/if}
                </span>
                {if $showtelesalesNumber neq 'control' && !$buynowdisabled}
                	<span>or <b style="color:#ff9412">call {$telesalesNumber}</b> <br> to place an order</span>
				{/if}
			</div>
        {else}
        	{if $productDisclaimerTitle|trim neq ""}
			<div class="mk-product-disclaimer-container">
				<div class="mk-product-disclaimer">
					{$productDisclaimerTitle} {if $productDisclaimerText|trim neq ""}<a href="javascript:void(0)" class="articletype-disclaimer-link link">Know More</a>{/if}
				</div>
			</div>
			{/if}
        	<div class="out-of-stock-msg clearfix left">Currently this product is sold out.</div>
		{/if}
	</div>
	<div class="product-detail-block-right left">
		{if $brand_logo}
		<div class="pdp-brand-logo clearfix">
        	<a href="{$http_location}/{$brandname|lower|replace:' ':'-'}" title="{$brandname}"><img src="{$brand_logo}" alt="{$brandname}"></a>
        </div>
        {/if}
		<ul class="customer-promises cps">
		 	<li><b>100% Original</b> <i>Products</i></li>
            <li><b>Free</b> <i>Shipping</i></li>
            <li><b>Cash</b> <i>on Delivery</i></li>
            {if $deliveryKnowMoreEnabled}
            <li><b>24 Hours</b> <a href="javascript:void(0)" onclick="gaTrackEvent('sports_pdp_{$productTypeLabel}_{$brandname}', 'delivery_know_more', '');" class="deliveryknowmore-link no-decoration-link"><i class="cps-link">Dispatch</i></a></li>
            {/if}
            <li><b>30 Day</b> <a href="javascript:void(0)" onclick="gaTrackEvent('sports_pdp_{$productTypeLabel}_{$brandname}', 'returns_know_more', '');" class="ReturnPolicy-popup-link no-decoration-link"><i class="cps-link">Returns</i></a></li>
		 </ul>
	</div>
	<div class="clear">
		{if $descriptionAndDetails|trim neq "" ||  $style_note|trim neq "" || $materials_care_desc|trim neq "" || $size_fit_desc|trim neq ""}	
    	<div class="product-description-title">Product Description</div>
		<div class="product-description">
		{if $style_note|trim neq ""}
			{$style_note}<br/>
		{/if}
		{if $descriptionAndDetails|trim neq ""}
		 	{$descriptionAndDetails}<br />
		{/if}
		{if $materials_care_desc|trim neq ""}
			{$materials_care_desc}<br/>
		{/if}
		{if $size_fit_desc|trim neq ""}
			{$size_fit_desc}<br />
		{/if}
		</div>
		{/if}
	</div>
</div>
