<div class="zoom-overlay hide"></div>
<div class="zoom-block hide">
	<div class="zoom-container">
	{if $discountlabeltext}
        <div class="discount-label val"><span>{$discountlabeltext}</span></div>
    {/if}
		<div class="close-zoom-overlay"></div>
		<div class="prev zoom-overlay-control"><a href="javascript:void(0)" class="prev-link no-decoration-link"></a></div>
		<div class="next zoom-overlay-control"><a href="javascript:void(0)" class="next-link no-decoration-link"></a></div>
		<img class="zoom-image izoom" height="720" width="520" src="{$cdn_base}/skin1/images/spacer.gif">
		<img class="zoom-image superzoom" height="1306" width="980" src="{$cdn_base}/skin1/images/spacer.gif" style="display:none;">
		<div class="overlay-loading hide"></div>
		{if !$disableProductStyle}
	<div class="zoom-buy-now-block {if $buynowdisabled}hide{/if}">
		<div class="zoom-buy-now-overlay corners-tl-tr"></div>
		<div class="zoom-buy-now-main" style="position:relative;margin:0 auto;padding:7px 10px 10px 10px;">
			<div class="left" style="width:100%">
			<div class="zoom-buy-now-info zoom-buy-now-sections left" style="font-weight:bold;color:#000;width:70%">
				{$productDisplayName}
			</div>
			<div class="zoom-buy-now-pricing zoom-buy-now-sections left" style="width:29%;text-align:center;">
				{if $discountamount > 0}
                    <div style="padding:3px 4px 0px 4px;border: 1px solid #006D9B;width:auto;">
                        <span style="display:block;font-weight:bold;color:#000;line-height:14px;">MRP Rs <span style="text-decoration:line-through;">{$originalprice}</span></span>
                        <span style="display:block;font-weight:bold;color:#000;">Now Rs <span style="font-size:14px;"> {if $discountamount > 0}{math equation="x - d" x=$originalprice d=$discountamount assign="finalamount"}{$finalamount}{else}{$originalprice}{/if}</span></span>
                    </div>
                {else}
                    <div style="padding:3px 4px 4px 4px;border: 1px solid #006D9B;width:auto;">
                        <span style="display:block;font-weight:bold;color:#000;">Now Rs <span style="font-size:14px;"> {if $discountamount > 0}{math equation="x - d" x=$originalprice d=$discountamount assign="finalamount"}{$finalamount}{else}{$originalprice}{/if}</span></span>
                    </div>
                {/if}
			</div>		
			</div>
			<div>
			
				{if $sizeOptions}
				<div class="zoom-buy-now-sizing-flat zoom-buy-now-sections left" style="margin-top:5px;width:70%">
					<div class="clearfix zoom-flat-size-options left" style="width:auto;padding-top:2px;padding-left:2px;">
							<span class="left mrp-b" style="color:#000;">Size: </span> 
							
							{assign var='j' value=1}
							{foreach from=$sizeOptions key=key item=val}
                                {if $renderTooltip}
                                    <a href="javascript:void(0)" class="vtooltip no-decoration-link corners" rel="" onclick="sizeSelect({$j},this)">{$val}</a>
                                {else}
                                    <a href="javascript:void(0)" class="no-decoration-link corners" rel="" onclick="sizeSelect({$j},this)">{$val}</a>
                                {/if}
                                {assign var='j' value=$j+1}
                            {/foreach}
							{*{section name=options loop=$productOptions }
								{section name=values loop=$productOptions[options][1]}
									{assign var='sizename' value=$productOptions[options][1][values][0]}
										<a href="javascript:void(0)" class="no-decoration-link corners" rel="{$sizeOptionsTooltipHtmlArray[$sizename]}" onclick="sizeSelect({$j},this)">{$sizeOptions[$sizename]}</a>
									{assign var='j' value=$j+1}
		                        {/section}
							{/section}*}	
							
						</div>
						<div class="left size-options-error-msg-flat" style="display:none;">Please select a size.</div>
					</div>
				{/if}
			
			<div class="zoom-buy-now-buying zoom-buy-now-sections left" style="margin-top:5px;width:30%">
				{if $buynowdisabled} 
					<a class="btn-disabled corners left clearfix" style="width:100%;line-height:30px;">Buy now</a>
				{else}{literal}
					<a class="orange-bg cart-btn corners left clearfix" style="width:100%;line-height:30px;">Buy now &raquo;</a>
					{/literal}
				{/if}
				{if $buynowdisabled}
					<div class="out-of-stock-msg clearfix left">Currently this product is out of stock.</div>
				{/if}
			</div>
			</div>
		</div>
	</div>
	{/if}
	</div>
	
</div>
