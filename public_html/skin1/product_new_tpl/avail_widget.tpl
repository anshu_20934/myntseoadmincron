			<h2 class="h2g" style="position:relative;">{$avail_title}</h2>
			<div class="product-listing">
			<ul class="clearfix last">
			{foreach name=widgetdataloop item=widgetdata from=$avail_recommendation.data}
				{if $widgetdata.product neq '' && $widgetdata.product neq ' '}
				<li class="item {if $smarty.foreach.widgetdataloop.last}last{/if} itemsizes">
				{if $quicklookenabled}
					<a class="quick-look action-btn" data-widget="avail_widget" data-styleid="{$widgetdata.styleid}" data-href="{$widgetdata.landingpageurl}" href="javascript:void(0)"><span></span>Quick View</a>
				{/if}
				 <div class="new-label"> </div>
				{if $widgetdata.discount_label}<div class="discount-label val"><span>{$widgetdata.discount_label}</span></div>{/if}
					<a href="{$widgetdata.landingpageurl}?t_code={$tracking_code}" class="{if $i lt 5}loaded{else}to-be-loaded{/if}" onclick="_gaq.push(['_trackEvent', 'pdp_click', Myntra.Data.pageName, 'avail_widget']);s_crossSell();">
						<div class="item-box">
                            <span class="item-image item-image-border"> 
                                <img id="im_product_img_{$widget.nameclean}_{$widgetdata.styleid}" src="{$widgetdata.search_image}"  height="320" width="240" alt="{$widgetdata.product}">
                            </span>

                            
                                <span class="name">{$widgetdata.product|truncate:55}</span>
                                                      {if $widgetdata.discounted_price neq $widgetdata.price}
                                                           <div class="dp">{$rupeesymbol}{$widgetdata.discounted_price|number_format:2:".":","}</div>
                                                           <div class="op">MRP <span class="strike">{$rupeesymbol}{$widgetdata.price|number_format:2:".":","}</span></div>
                                                           <div class="dpv">{$rupeesymbol}{math equation="x-y" x=$widgetdata.price y=$widgetdata.discounted_price assign="discount_amount"}{$discount_amount|round|number_format:2:".":","} ({$widgetdata.discount_label}) Discount</div>
                                                      {else}
                                                          <div class="dp">{$rupeesymbol}{$widgetdata.price|number_format:2:".":","}</div>
                                                            {if $cashback_gateway_status eq 'on' && $widgetdata.showcashback eq 'true'}
                                                                <div class="op">Effective Price {$rupeesymbol}{math equation="x-y" x=$widgetdata.price y=$widgetdata.cashback assign="effectiveprice"}{$effectiveprice|round|number_format:2:".":","}</div>
                                                                <div class="dpv">
                                                                	{$rupeesymbol}{$widgetdata.cashback|round|number_format:2:".":","} (10%) <span class="cb">Cashback</span>
                                                                	<!--span class="cbtip hide"><b style="display: block;">Cashback</b> On purchasing this product, you will receive a cashback credit upto the amount indicated here. Cashback is calculated on item value excluding any coupon discounts. You can redeem this on any of your subsequent purchases at Myntra.</span-->
                                                                </div>
                                                            {/if}
                                                      {/if}
                            
                    </div>
                <span class="availability">Size: {if $widgetdata.sizes neq '' && $widgetdata.sizes neq ' '}{$widgetdata.sizes|replace:",":", "}{else}SOLD OUT{/if}</span>
                </a>
				</li>
				{/if}
				{assign var=i value=$i+1}
				{/foreach}
				</ul>
				</div>
