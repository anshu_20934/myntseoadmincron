<div class="related-products-title h2b bold">Other Customers Recommend</div>
<div class="icon-line mb10"></div>
<div class="product-listing">
			<ul class="clearfix last">
			{foreach name=widgetdataloop item=widgetdata from=$related_products}
				{if $widgetdata.product neq '' && $widgetdata.product neq ' '}
				<li class="item">
				{if $widgetdata.discount_label}<div class="discount-label val"><span>{$widgetdata.discount_label}</span></div>{/if}
					<a href="{$http_location}/{$widgetdata.landingpageurl}">
						<span class="item-image item-image-border">
							{if $widgetdata.productid}
							<img id="im_product_img_{$widget.nameclean}_{$widgetdata.styleid}_{$widgetdata.productid}" src="{$cdn_base}/skin1/myntra_images/loading_240_320.gif" lazy-src="{$widgetdata.image_men_s}" class="lazy" height="320" width="240" alt="{$widgetdata.product}">
							{else}
							<img id="im_product_img_{$widget.nameclean}_{$widgetdata.styleid}" src="{$cdn_base}/skin1/myntra_images/loading_240_320.gif" lazy-src="{$widgetdata.search_image|replace:'_images_178_178':'_images_198_198'}" class="lazy" height="320" width="240" alt="{$widgetdata.product}">
							{/if}
						</span>
						<span class="item-divider icon-line"></span>
						<span class="item-description">
							<span class="name">{$widgetdata.product|truncate:40}</span>
								<div class="noncashback" style={if $cashback_gateway_status eq 'on' && $widgetdata.showcashback eq 'true'}"display:none;"{else}"display:block;"{/if}>
								{if $widgetdata.discounted_price neq $widgetdata.price}
									<span class="discount-price highlight" style="display: inline;padding:2px 5px;">{$rupeesymbol}{$widgetdata.discounted_price}</span>
									<span class="original-price strike" style="display: inline;padding:2px 5px;">{$rupeesymbol}{$widgetdata.price}</span>
									{if $widgetdata.discount_label}<span class="price-off black-bg2 corners">{$widgetdata.discount_label} OFF</span>{/if}
								{else}
								<span class="discount-price" style="display: inline;padding:2px 5px;">{$rupeesymbol}{$widgetdata.discounted_price}</span>
								<span class="original-price" style="display: inline;padding:2px 5px;"></span>
								{/if}
							</div>
                            <div class="cashbackblock" style={if $cashback_gateway_status eq 'on' && $widgetdata.showcashback eq 'true'}"display:block;"{else}"display:none;"{/if}>
                                <div class="pblock">
                                    <span class="discount-price{if $widgetdata.discounted_price neq $widgetdata.price} highlight{/if}">{if $widgetdata.discounted_price eq $widgetdata.price}<b class="smalltext">You Pay </b>{/if}{$rupeesymbol}{$widgetdata.discounted_price}</span>
                                    {if $widgetdata.discounted_price neq $widgetdata.price}
                                        <span class="original-price strike">{$rupeesymbol}{$widgetdata.price}</span>
                                    {/if}
                                </div>
                                {if $widgetdata.discounted_price eq $widgetdata.price}
                                <div class="cb-block" style={if $widgetdata.cashback neq 0}"display:block;"{else}"display:none;"{/if}>
                                    <span class="cashtitle">Get 10% Cashback</span><span class="cashrs">{$rupeesymbol}{$widgetdata.cashback|round|string_format:"%d"} <b class="cb">What's This?</b></span>
                                    <span class="cbtip hide"><b style="display: block;">Cashback</b> <img src="{$cdn_base}/skin1/images/cart-removebtn.png" alt="close" class="tipclose">On purchasing this product, you will receive a cashback credit upto the amount indicated here. Cashback is calculated on item value excluding any coupon discounts. You can redeem this on any of your subsequent purchases at Myntra. </span>
                                </div>
                                {/if}
                            </div>

							<span class="availability">AVAILABILITY: {if $widgetdata.sizes neq '' && $widgetdata.sizes neq ' '}{$widgetdata.sizes|replace:",":", "}{else}SOLD OUT{/if}</span>
						</span>
						<span class="item-divider icon-line"></span>
					</a>
				</li>
				{/if}
				{assign var=i value=$i+1}
				{/foreach}
				</ul>
				</div>