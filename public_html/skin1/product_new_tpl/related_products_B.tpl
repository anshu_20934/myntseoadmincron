			<h2 class="h4" style="position:relative;">Other Customers Recommend</h2>
			<div class="product-listing {if $cashback_displayOnPDPPage neq '1'} without_cashback {/if}">
			<ul class="clearfix last">
			{foreach name=widgetdataloop item=widgetdata from=$related_products}
				{if $widgetdata.product neq '' && $widgetdata.product neq ' '}
				<li class="item {if $smarty.foreach.widgetdataloop.last}last{/if} itemsizes">
				{if $quicklookenabled}
					<span class="quick-look action-btn" data-styleid="{$widgetdata.styleid}" data-href="{$widgetdata.landingpageurl}"><span class="quick-look-icon"></span>Quick View</span>
				{/if}
				 <div class="new-label"> </div>
				{if $widgetdata.discount_label}<div class="discount-label val"><span>{eval var=$widgetdata.discount_label}</span></div>{/if}
					<a href="{$http_location}/{$widgetdata.landingpageurl}" class="{if $i lt 5}loaded{else}to-be-loaded{/if}" onclick="_gaq.push(['_trackEvent',' recommedation_widget','myntra', 'pdp']); s_crossSell();">
						<div class="item-box">
                            <img id="im_product_img_{$widget.nameclean}_{$widgetdata.styleid}" src="{$widgetdata.search_image}"  height="320" width="240" alt="{$widgetdata.product}">
                            <div class="item-desc">
                            	<span class="name">{$widgetdata.product|truncate:40}</span>
	                            {if $cashback_displayOnPDPPage eq '1'}
	                                {if $widgetdata.discount > 0}
	                                    <div class="op">MRP <span class="strike">{$rupeesymbol}{$widgetdata.price|number_format:0:".":","}</span> <span style="color:#000"> &nbsp; {$rupeesymbol}{math equation="x-y" x=$widgetdata.price y=$widgetdata.discounted_price assign="discount_amount"}{$discount_amount|round|number_format:0:".":","} Off </span></div>
	                                    <div class="dp"><span style="font-weight:normal">{$rupeesymbol}</span>{$widgetdata.discounted_price|number_format:0:".":","}</div>
	                                    {eval var=$widgetdata.discount_label}
	                                {elseif $widgetdata.discount_label}
											<div class="dp"><span style="font-weight:normal">{$rupeesymbol}</span>{$widgetdata.price|round|number_format:0:".":","}</div>
											{eval var=$widgetdata.discount_label}
	                            	{else}
	                              	    {if $cashback_gateway_status eq 'on' && $widgetdata.showcashback eq 'true'}
	                                        <div class="op">Effective Price {$rupeesymbol}{math equation="x-y" x=$widgetdata.price y=$widgetdata.cashback assign="effectiveprice"}{$effectiveprice|round|number_format:0:".":","}</div>
	                                        <div class="dpv"><div class="dp"><span style="font-weight:normal">{$rupeesymbol}</span>{$widgetdata.price|round|number_format:0:".":","}</div>
	                                            <span class="cbp">with <b>10%</b></span><span class="cb">Cashback</span>
	                                        </div>
	                                    {/if}
	                            	{/if}
		                    	{else}
		                    		{if $widgetdata.discount > 0}
	                                   	<div class="dp left"><span style="font-weight:normal">{$rupeesymbol}</span>{$widgetdata.discounted_price|number_format:0:".":","}</div> <div class="op"><span class="strike" style="float:left;">{$widgetdata.price|number_format:0:".":","}</span></div>
	                                    {eval var=$widgetdata.discount_label}
	                                {else}
	                                    <div class="dp"><span style="font-weight:normal">{$rupeesymbol}</span>{$widgetdata.price|round|number_format:0:".":","}</div>
										{eval var=$widgetdata.discount_label}
	                           		{/if}
		                    	{/if}
	                    	</div>
                    	</div>
                		<span class="availability">Sizes: {if $widgetdata.sizes neq '' && $widgetdata.sizes neq ' '}{$widgetdata.sizes|replace:",":", "}{else}SOLD OUT{/if}</span>
                	</a>
				</li>
				{/if}
				{assign var=i value=$i+1}
				{/foreach}
				</ul>
				</div>
