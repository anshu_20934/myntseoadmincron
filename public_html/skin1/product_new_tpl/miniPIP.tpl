<script type="text/javascript">
  var shownewui='{$shownewui}';
</script>

		<div class="left clearfix pdp-content-div">
			<div class="last left">
				<div class="product-img-thumbnails-block clearfix">
				    <div class="product-block left">
						<div class="product-image-preview left" id="preview_image_div">
							<div class="imageframe">
								<img  src="{$defaultImage|replace:'_images':'_images_360_480'}" alt="{$productDisplayName|escape:'htmlall'}" width="300" height="400" id="finalimage" class="quick-look-preview" style="position:relative;z-index:1;">
								<div class="preview-loading hide"></div>
								<div class="nav-buttons">
								{if $discountlabeltext}<div class="discount-label val"><span>{$discountlabeltext}</span></div>{/if}
									<div class="quick-look-prev preview-overlay-control"><a href="javascript:void(0)" class="prev-link no-decoration-link"></a></div>
									<div class="quick-look-next preview-overlay-control"><a href="javascript:void(0)" class="next-link no-decoration-link"></a></div>
								</div>
							</div>
						</div>
	
					    <input 	type="hidden" id="styledescription" name="description" value="({$productTypeDetails|escape}) {$descriptionAndDetails|escape}" >
					    <input type="hidden" value="{$cstatus}" id="cstatus">
					    <div class="questionLayer" id="question" style="display:none;"></div>
						<div class="opaqueLayer" id="shadow" style="display:none;"></div>
						
					</div>
					<div class="left clearfix product-thumbnails">
				    	<input type="hidden" value="quicklookthumb_0" class="quicklookloadedimage" name="quicklookloadedimage">
	                    <div class="v-tabs quick-look-thumb-views">
							{assign var=i value=0}
	                        {section name="area" loop=$area_icons}
	                            <div id="quick_look_area_{$area_icons[area].name}">
	                                <a  {if $smarty.section.area.first} class="selected" {/if} id="quicklookthumb_{$i}" rel='"prodimage":"{$area_icons[area].image|replace:"_images":"_images_360_480"}","intermediatezoomimage":"{$area_icons[area].image|replace:"_images":"_images_540_720"}","fullimage":"{$area_icons[area].image|replace:"_images":"_images_1080_1440"}"'>
	                                	<img width="39" height="52" src='{$area_icons[area].image|replace:"_images":"_images_48_64"}' alt="{$productDisplayName|escape:'htmlall'} - {$area_icons[area].name} view">
	                                </a>
	                            </div>
							{assign var=i value=$i+1}
	                        {/section}
	                    </div>
	                </div>
                </div>
			</div>
			{*This block is needed here because cashback should be defined outside the pdp-details.tpl file so that the value can be used in the popup*}
			{if $discounttype eq 'rule'}
			    {if $discountamount eq ''}{assign var="discountamount" value="0"}{/if}
				{math equation="(x - d )* y " x=$originalprice d=$discountamount y=0.1  assign="cashback"}
			{else}
			   	{math equation="x * y" x=$originalprice y=0.1  assign="cashback"}
			{/if}
			{include file="product_new_tpl/miniPIP-details.tpl"}
		</div>
        


                    <form action="mkretrievedataforcart.php?pagetype=productdetail" id="hiddenFormMiniPIP" method="post" class="clearfix">
                    	<input type="hidden" name="_token" value="{$USER_TOKEN}" />
                        <input type="hidden" id="count" value="{$i}">
                        <input type="hidden" name="productStyleId" value="{$productStyleId}">
                        <input type="hidden" name="uploadedImagePath" id="imagepath" value="">
                        <input type="hidden" name="unitPrice" value="{if $discountprice}{$discountprice}{else}{$originalprice}{/if}">
                        <input type="hidden" name="pagetype" value="productdetail">
                        <input type="hidden" name="totalOptions" id="totalOptions" value="{$Options}">
                        <input type="hidden" name="productName" id="productName" value="{$designer[6]}">
                        <input type="hidden" name="productTypeName" id="productTypeName" value="{$productTypeLabel}">
                        <input type="hidden" name="productStyleLabel" id="productStyleLabel" value="{$productDisplayName}">
                        <input type="hidden" name="productTypeId" id="productTypeId" value="{$productTypeId}">
                        <input type="hidden" name="gender" value="{$gender}">
                        <input type="hidden" name="brand" value="{$brandname}">


                        <input type="hidden" name="referrer" value="productDetail">
                        <input type="hidden" name="referredTo"  id= "referredTo" value="">
                        <input type="hidden" name="quantityMenu" id="quantityMenu" size="3" maxlength="4" value="1">
                        <input type="hidden" name="productSKUID" id="productSKUIDMiniPIP" value="">
                        <input type="hidden" name="sizeid" id="sizeid" value="">
                        <input type="hidden" name="quantity"  id="quantity" value="1">
						
                        {if $productOptions}
                            {section name=options loop=$productOptions}
                            <input type="hidden" name="option{$smarty.section.options.index}name"  id="option{$smarty.section.options.index}name"  value="{$productOptions[options][0]}">
                            <input type="hidden" name="option{$smarty.section.options.index}value" id="option{$smarty.section.options.index}value" value="">
                            <input type="hidden" name="option{$smarty.section.options.index}id" id="option{$smarty.section.options.index}id"  value="" >
                            {/section}
                        {/if}

                        {assign var='i' value=1}
                        {if $sizeOptions}
                            {foreach from=$sizeOptions key=key item=val}
                                <input type="hidden" name="sizequantity[]" id="sizequantity{$i}" class="sizequantity">
                                <input type="hidden" name="sizename[]" id="sizename{$i}" value="{$key}">
                                {assign var='i' value=$i+1}
                            {/foreach}
                        {else}
                            <input type="hidden" name="sizequantity[]" id="sizequantity1" class="sizequantity">
                            <input type="hidden" name="sizename[]" id="sizename1" value="NA">
                        {/if}
                    </form>
