
{include file="site/doctype.tpl"}
<head>

{* facebook stuff for now only for product & style pages *}
<meta property="og:title" content="{$pageTitle|trim}" />
<meta property="og:type" content="{$objectType}" />
<meta property="og:url" content="{$pageURL}" />
<meta property="og:image" content="{$defaultImage|replace:'_images':'_images_96_128'}" />
<meta property="og:site_name" content="Myntra" />
<meta property="og:description" content="{$metaDescription}" />
<meta property="fb:admins" content="520074227">
<link rel="image_src" href="{$defaultImage|replace:'_images':'_images_96_128'}" />
{*end of facebook stuff*}
{include file="site/header.tpl"}


<script type="text/javascript">
  var imagePath = "images/style/sizechart/";
  var curr_style_id = "{$productStyleId}";
  var windowid 	= "{$windowid}";
  var selectedIndex	= "{$selectedIndex}";
  var custTarget	= "{$custTarget}";
  var current_style_group_id	="{$current_style_group}";
  var templateid	= "{$templateid}";
  var pageName="pdp";
  var t_code='{$t_code}';
  //var shownewui='{$shownewui}';
  var avail_dynamic_param='{$avail_dynamic_param}';
  var allProductOptionDetails={$allProductOptionDetails};
</script>

</head>
<body class="{$_MAB_css_classes}">
<script>
Myntra.Data.pageName="pdp";
</script>
{include file="site/menu.tpl"}
<div class="container clearfix" style="z-index:999;">


	<!--    Body Content    -->
	<div class="span-33 content clearfix">
		<div class="content-left left span-33 clearfix pdp-content-div">
			<div class="span-15 last">
				<div class="product-img-thumbnails-block clearfix">
				    <div class="product-block">
						<div class="product-image-preview left" id="preview_image_div">
							<div class="imageframe">
								<img  src="{$defaultImage|replace:'_images':'_images_360_480'}" alt="{$productDisplayName|escape:'htmlall'}" width="360" height="480" id="finalimage" style="position:relative;z-index:1;">
								<div class="preview-loading hide"></div>
								<div class="nav-buttons">
								{if $discountlabeltext}<div class="discount-label val"><span>{$discountlabeltext}</span></div>{/if}
									<div class="preview-prev preview-overlay-control"><a href="javascript:void(0)" class="prev-link no-decoration-link"></a></div>
									<div class="preview-next preview-overlay-control"><a href="javascript:void(0)" class="next-link no-decoration-link"></a></div>
								</div>
							</div>
						</div>
	
					    <input 	type="hidden" id="styledescription" name="description" value="({$productTypeDetails|escape}) {$descriptionAndDetails|escape}" >
					    <input type="hidden" value="{$cstatus}" id="cstatus">
					    <div class="questionLayer" id="question" style="display:none;"></div>
						<div class="opaqueLayer" id="shadow" style="display:none;"></div>
						
					</div>
					<div class="left clearfix product-thumbnails">
				    	<input type="hidden" value="thumb_0" class="loadedimage" name="loadedimage">
	                    <div class="v-tabs thumb-views">
							{assign var=i value=0}
	                        {section name="area" loop=$area_icons}
	                            <div id="area_{$area_icons[area].name}">
	                                <a  {if $smarty.section.area.first} class="selected" {/if} id="thumb_{$i}" href="javascript:void(0)" rel='"prodimage":"{$area_icons[area].image|replace:"_images":"_images_360_480"}","intermediatezoomimage":"{$area_icons[area].image|replace:"_images":"_images_540_720"}","fullimage":"{$area_icons[area].image|replace:"_images":"_images_1080_1440"}"'>
	                                    {if $i eq 0}
	                                        <img width="48" height="64" src='{$area_icons[area].image|replace:"_images":"_images_48_64"}' alt="{$productDisplayName|escape:'htmlall'} - {$area_icons[area].name} view">
	                                    {else}
	                                        <img width="48" height="64" src="{$cdn_base}/skin1/images/spacer.gif" class="lazy" lazy-src='{$area_icons[area].image|replace:"_images":"_images_48_64"}' alt="{$productDisplayName|escape:'htmlall'} - {$area_icons[area].name} view">
	                                    {/if}
	                                </a>
	                            </div>
							{assign var=i value=$i+1}
	                        {/section}
	                    </div>
	                </div>
                </div>
                <div class="social-buttons mt10">
            		<div class="fb-like-button left"> <fb:like href="{$pageURL}" send="false" layout="button_count" width="90" show_faces="false" action="like"></fb:like></div>
                	<div class="g-plus-button left"><g:plusone size="medium" count="true" href="{$pageURL}"></g:plusone></div>
            	</div>
			</div>
			{*This block is needed here because cashback should be defined outside the pdp-details.tpl file so that the value can be used in the popup*}
			{if $discounttype eq 'rule'}
			    {if $discountamount eq ''}{assign var="discountamount" value="0"}{/if}
				{math equation="(x - d )* y " x=$originalprice d=$discountamount y=0.1  assign="cashback"}
			{else}
			   	{math equation="x * y" x=$originalprice y=0.1  assign="cashback"}
			{/if}
			{include file="product_new_tpl/pdp-details.tpl"}
		</div>
        


                    <form action="mkretrievedataforcart.php?pagetype=productdetail" id="hiddenForm" method="post" class="clearfix">
                    	<input type="hidden" name="_token" value="{$USER_TOKEN}" />
                        <input type="hidden" id="count" value="{$i}">
                        <input type="hidden" name="productStyleId" value="{$productStyleId}">
                        <input type="hidden" name="uploadedImagePath" id="imagepath" value="">
                        <input type="hidden" name="unitPrice" value="{if $discountprice}{$discountprice}{else}{$originalprice}{/if}">
                        <input type="hidden" name="pagetype" value="productdetail">
                        <input type="hidden" name="totalOptions" id="totalOptions" value="{$Options}">
                        <input type="hidden" name="productName" id="productName" value="{$designer[6]}">
                        <input type="hidden" name="productTypeName" id="productTypeName" value="{$productTypeLabel}">
                        <input type="hidden" name="productStyleLabel" id="productStyleLabel" value="{$productDisplayName}">
                        <input type="hidden" name="productTypeId" id="productTypeId" value="{$productTypeId}">
                        <input type="hidden" name="gender" value="{$gender}">
                        <input type="hidden" name="brand" value="{$brandname}">


                        <input type="hidden" name="referrer" value="productDetail">
                        <input type="hidden" name="referredTo"  id= "referredTo" value="">
                        <input type="hidden" name="quantityMenu" id="quantityMenu" size="3" maxlength="4" value="1">
                        <input type="hidden" name="productSKUID" id="productSKUID" value="">
                        <input type="hidden" name="quantity"  id="quantity" value="1">
						{**AVAIL*}
						<input type="hidden" name="uqs"  id="uqs" value="{$uqs}">
						<input type="hidden" name="uq"  id="uq" value="{$uq}">
						<input type="hidden" name="t_code"  id="t_code" value="{$t_code}">
						{**END AVAIL*}
                        {if $productOptions}
                            {section name=options loop=$productOptions}
                            <input type="hidden" name="option{$smarty.section.options.index}name"  id="option{$smarty.section.options.index}name"  value="{$productOptions[options][0]}">
                            <input type="hidden" name="option{$smarty.section.options.index}value" id="option{$smarty.section.options.index}value" value="">
                            <input type="hidden" name="option{$smarty.section.options.index}id" id="option{$smarty.section.options.index}id"  value="" >
                            {/section}
                        {/if}

                        {assign var='i' value=1}
                        {if $sizeOptions}
                            {foreach from=$sizeOptions key=key item=val}
                                <input type="hidden" name="sizequantity[]" id="sizequantity{$i}" class="sizequantity">
                                <input type="hidden" name="sizename[]" id="sizename{$i}" value="{$key}">
                                {assign var='i' value=$i+1}
                            {/foreach}
                        {else}
                            <input type="hidden" name="sizequantity[]" id="sizequantity1" class="sizequantity">
                            <input type="hidden" name="sizename[]" id="sizename1" value="NA">
                        {/if}
                    </form>

		<div class="related-products left clearfix last span-33">
                        {*if $discount_set && $discount_set|@count gt 0}
                            {include file="product_new_tpl/discount_products_set.tpl"}
                        {/if*}
			{if $avail_pdp_abtest neq 'avail_pdp'}
				{if $related_products}
					{if $shownewui eq 'newui'}
						{include file="product_new_tpl/related_products_B.tpl"}
					{else}
						{include file="product_new_tpl/related_products.tpl"}
					{/if}
				{/if}
			{else}
				{**AVAIL*}
					<div id="avail-container"></div>
				{*END AVAIL*}
			{/if}
			{literal}
				{RECENT_WIDGET_PLACEHOLDER}
			{/literal}


		</div>

		 <!--Popular tags-->

     {*<div class="clearfix p10" style="clear:both">
		<h2 class="h4" id="popular-title">Popular</h2>
	    <ul class="tags">
	    	{foreach name=tagtype item=fixedorDynamic from=$mostPopularWidget}
	        	{foreach name=uniquetag item=tag from=$fixedorDynamic}
	            	<li><a href="{$http_location}/{$tag.link_url}">{$tag.link_name}</a></li>
	            {/foreach}
	        {/foreach}
	    </ul>
	</div>*}



</div>
</div>


<div class="zoom-overlay hide"></div>
<div class="zoom-block hide">
	<div class="zoom-container">
	{if $discountlabeltext}
        <div class="discount-label val"><span>{$discountlabeltext}</span></div>
    {/if}
		<div class="close-zoom-overlay"></div>
		<div class="prev zoom-overlay-control"><a href="javascript:void(0)" class="prev-link no-decoration-link"></a></div>
		<div class="next zoom-overlay-control"><a href="javascript:void(0)" class="next-link no-decoration-link"></a></div>
		<img class="zoom-image izoom" height="720" width="520" src="{$cdn_base}/skin1/images/spacer.gif">
		<img class="zoom-image superzoom" height="1306" width="980" src="{$cdn_base}/skin1/images/spacer.gif" style="display:none;">
		<div class="overlay-loading hide"></div>
		{if !$disableProductStyle}
	<div class="zoom-buy-now-block {if $buynowdisabled}hide{/if}">
		<div class="zoom-buy-now-overlay corners-tl-tr"></div>
		<div class="zoom-buy-now-main" style="position:relative;margin:0 auto;padding:7px 10px 10px 10px;">
			<div class="left" style="width:100%">
			<div class="zoom-buy-now-info zoom-buy-now-sections left" style="font-weight:bold;color:#000;width:70%">
				{$productDisplayName}
			</div>
			<div class="zoom-buy-now-pricing zoom-buy-now-sections left" style="width:29%;text-align:center;">
				{if $discountamount > 0}
                    <div style="padding:3px 4px 0px 4px;border: 1px solid #006D9B;width:auto;">
                        <span style="display:block;font-weight:bold;color:#000;line-height:14px;">MRP Rs <span style="text-decoration:line-through;">{$originalprice|round|number_format:0:".":","}</span></span>
                        <span style="display:block;font-weight:bold;color:#000;">Now Rs <span style="font-size:14px;color:#D20000;"> {if $discountamount > 0}{math equation="x - d" x=$originalprice d=$discountamount assign="finalamount"}{$finalamount|round|number_format:0:".":","}{else}{$originalprice|round|number_format:0:".":","}{/if}</span></span>
                    </div>
                {else}
                    <div style="padding:3px 4px 4px 4px;border: 1px solid #006D9B;width:auto;">
                        <span style="display:block;font-weight:bold;color:#000;">Now Rs <span style="font-size:14px;color:#D20000;"> {if $discountamount > 0}{math equation="x - d" x=$originalprice d=$discountamount assign="finalamount"}{$finalamount|round|number_format:0:".":","}{else}{$originalprice|round|number_format:0:".":","}{/if}</span></span>
                    </div>
                {/if}
			</div>		
			</div>
			<div>
			
				{if $sizeOptionSKUMapping}
				<div class="zoom-buy-now-sizing-flat zoom-buy-now-sections left" style="margin-top:5px;width:70%">
					<div class="clearfix zoom-flat-size-options left" style="width:auto;padding-top:2px;padding-left:2px;">
							<span class="left mrp-b" style="color:#000;">Size: </span> 				
							{assign var='j' value=1}
							{foreach from=$sizeOptionSKUMapping key=key item=val}
                                {if $renderTooltip}
                                    <a href="javascript:void(0)" class="action-btn pdp-action-btn vtooltip" rel="" onclick="sizeSelect({$key},{$j},this)">{$val}</a>
                                {else}
                                    <a href="javascript:void(0)" class="action-btn pdp-action-btn" rel="" onclick="sizeSelect({$key},{$j},this)">{$val}</a>
                                {/if}
                                {assign var='j' value=$j+1}
                            {/foreach}							
						</div>
						<div class="left size-options-error-msg-flat" style="display:none;">Please select a size.</div>
					</div>
				{/if}
			
			<div class="zoom-buy-now-buying zoom-buy-now-sections left" style="margin-top:5px;width:30%">
				{if $buynowdisabled} 
					<a class="btn-disabled corners left clearfix" style="width:100%;line-height:30px;">Buy now</a>
				{else}{literal}
					<a class="orange-bg cart-btn corners left clearfix" style="width:100%;line-height:30px;">Buy now &raquo;</a>
					{/literal}
				{/if}
				{if $buynowdisabled}
					<div class="out-of-stock-msg clearfix left">Currently this product is out of stock.</div>
				{/if}
			</div>
			</div>
		</div>
	</div>
	{/if}
	</div>
	
</div>

{if $disableProductStyle || $buynowdisabled}
    {assign var=zoomBuyNowBlockHeight value=0}
{else}
    {assign var=zoomBuyNowBlockHeight value=90}
{/if}

{if $notifymefeatureon}
	<!--<div class="notifyme-overlay"></div>
	<div class="span-14 notifyme-form corners orange-bg last"
		id="notifyme-form-unlogged" style="display: none;">
		<div class="loading-overlay hide">
						<img src="{$cdn_base}/skin1/myntra_images/loading-graphic.gif" alt="loading graphic">
		</div>
		<div class="login-box clearfix popbox">
			<div class="clearfix">
				<div class="login-title">What size are you looking for?</div>
			</div>
			<div class="icon-line ml10 mr10"></div>
			<div class="font-black-1 ml10 pt5">We'll notify you once it is
				available</div>
			<form method="post" id="notifyme-unlogged" name="notifyme"
				action="{$http_location}/subs_notification.php" class="clearfix">
				<input type="hidden" name="_token" value="{$USER_TOKEN}" />
				<fieldset class="field-hide">
					<legend> </legend>
				</fieldset>
				<div class="span-13 left clearfix">
					<div onclick="$('.notify-size-error').css('display','none');">
						<label class="font-black-1 span-2">Size</label>
						{if $flattenSizeOptions}
						<div class = "flat-size-options-n {if $unavailableOptions|@count gt 6}flat-size-options-two-lines{/if}">
						<div class="flat-size-options " style="height: 30px;display:inline-block; clear: right;">
							{foreach from=$unavailableOptions item=option}
									<a href="javascript:void(0)" class="no-decoration-link" style="margin-top: 1px;" onclick="selectedSize='{$option.unified}';sizeNotifyMeSelect({$option.sku_id},this)">{$option.unified}</a>
							{/foreach}
							<input type="text" id="otherSizeU" name="otherSize"  onclick="{literal}sizeNotifyMeSelect(0,null);$('.flat-size-options-n a').removeClass('selected');{/literal}" value="{if $unavailableOptions }Other{else}{/if}" class="text-size1 blurred toggle-text">
						</div>
						</div>
						{else}
							{if $unavailableOptions }
							<select id="nonunifiedsizeU" name="selectedsizeN"
								onchange="{literal}if(this.value=='Others'){$('#otherSizeU').show();}else{$('#otherSizeU').hide();$('#otherSizeU').val('');selectedSize=this.value;{/literal}{foreach	from=$unavailableOptions item=option}if(this.value=='{$option.sku_id}')selectedSize='{$option.value}';{/foreach}{literal}}{/literal}">
								<option value="Select">Select</option> {foreach	from=$unavailableOptions item=option}
								<option value="{$option.sku_id}" >{if $option.value eq
									'XS'}Extra-Small {elseif $option.value eq 'S'}Small {elseif
									$option.value eq 'M'}Medium {elseif $option.value eq 'L'}Large
									{elseif $option.value eq 'XL'}X-Large {elseif $option.value eq
									'XXL'}XX-Large {elseif $option.value eq 'XXXL'}XXX-Large
									{else}{$option.value} {/if}</option> {/foreach}
								<option value="Others" style="padding-right: 10px;" {if $unavailableOptions }{else}selected="selected"{/if}>Others</option>
							</select>
							{/if}
						<input type="text" id="otherSizeU" name="otherSize" value="" class="text-size1" style="display: {if $unavailableOptions }none{else}block{/if};">
						{/if}

					</div>
					<div class="size-options-error-msg notify-size-error size_error1"
						style="display: none;">Please specify size</div>
					<div class="clearfix"></div>
					<div class="last mt10 mb10 clear"
						onclick="$('.notify-email-error').css('display','none');">
						<label class="font-black-1 span-2">Email</label> <input
							type="text" tabindex="9" value="" id="notifymeEmailU"
							name="notifymeEmail" id="username" class="blurred"
							style="color: #333;">
					</div>
					<div class="size-options-error-msg notify-email-error size_error1"
						style="display: none;">Please specify a valid email</div>
					<div>
						<a id="notifyViaSmsLinkU" href="javascript:void(0)"
							onclick="{literal}if($('#notifymeSigninDiv').css('display')=='none'){$('#notifymeSigninDiv').show();$('#notifyViaSmsLinkU').html('[-]Want to get notified via SMS?');_gaq.push(['_trackEvent', 'size_notify_dialog', 'subscribe_sms','logged_out']);}else{$('#notifymeSigninDiv').hide();$('#notifyViaSmsLinkU').html('[+]Want to get notified via SMS?');};{/literal}">[+]Want
							to get notified via SMS?</a>
					</div>
					<div id="notifymeSigninDiv" class="notify-me" style="display: none;padding-left:0px;">
						<div class="font-black-1" style="text-align: center">You need to
							be signed in to get notified by SMS</div>
						<div>
							<div
								class="corners grey-bg1 red-border signinw sign-up-border left mr10"
								style="margin-left: 98px">
								<a href="javascript:void(0);" class="sign"
									onclick="closenotifyme();popupsourceNotifyme=true;_gaq.push(['_trackEvent', 'size_notification_dialog', 'signin', 'click']);">SIGN IN</a>
							</div>
							<div
								class="last corners grey-bg1 red-border newuserw sign-up-border left">
								<a href="javascript:void(0);" class="newuser"
									onclick="closenotifyme();popupsourceNotifyme=true;_gaq.push(['_trackEvent', 'size_notification_dialog', 'signup', 'click']);">SIGN UP</a>
							</div>
						</div>
					</div>
					<div style="padding-top: 12px;">
						<div>
							<a href="javascript:void(0);" class="corners orange-bg p5 bold"
								style="margin-right: 5px;margin-left: 98px;padding: 5px 10px;line-height:25px;" onclick="submitNotifyForm();">Notify
								Me</a> <a href="javascript:void(0);"
								class="corners orange-bg p5 bold" style="padding: 5px 20px;" onclick="_gaq.push(['_trackEvent', 'size_notify_dialog', 'cancel_notify']);closenotifyme();">Cancel</a>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>

	<div class="span-14 notifyme-form corners orange-bg last"
		id="notifyme-form-logged" style="display: none;">
		<div class="loading-overlay hide">
						<img src="{$cdn_base}/skin1/myntra_images/loading-graphic.gif" alt="loading graphic">
		</div>
		<div class="login-box clearfix popbox">
			<div class="clearfix">
				<div class="login-title">What size are you looking for?</div>
			</div>
			<div class="icon-line ml10 mr10"></div>
			<div class="font-black-1 ml10 pt5">We'll notify you once it is
				available</div>
			<form method="post" id="notifyme-logged" name="notifyme"
				action="{$http_location}/subs_notification.php" class="clearfix">
				<input type="hidden" name="_token" value="{$USER_TOKEN}" />
				<div class="span-13 left clearfix">
					<div onclick="$('.notify-size-error').css('display','none');">
						<label class="font-black-1 span-2">Size</label>
						{if $flattenSizeOptions}
						<div class = "flat-size-options-n  {if $unavailableOptions|@count gt 6}flat-size-options-two-lines{/if}" >
						<div class="flat-size-options" style="height: 30px;display:inline-block;clear:right;">
							{foreach from=$unavailableOptions item=option}
									<a href="javascript:void(0)" class="no-decoration-link" style="margin-top: 1px;" onclick="selectedSize='{$option.unified}';sizeNotifyMeSelect({$option.sku_id},this)">{$option.unified}</a>
							{/foreach}
							<input type="text" id="otherSizeL" name="otherSize"  onclick="{literal}sizeNotifyMeSelect(0,null);$('.flat-size-options-n a').removeClass('selected');{/literal}" value="{if $unavailableOptions }Other{else}{/if}" class="text-size1 blurred toggle-text">
						</div>
						</div>
						{else}
							{if $unavailableOptions }
							<select id="nonunifiedsizeL" name="selectedsizeN"
								onchange="{literal}if(this.value=='Others'){$('#otherSizeL').show();}else{$('#otherSizeL').hide();$('#otherSizeL').val('');{/literal}{foreach	from=$unavailableOptions item=option}if(this.value=='{$option.sku_id}')selectedSize='{$option.value}';{/foreach}{literal}}{/literal}">
								<option value="Select">Select</option> {foreach	from=$unavailableOptions item=option}
								<option value="{$option.sku_id}" >{if $option.value eq
									'XS'}Extra-Small {elseif $option.value eq 'S'}Small {elseif
									$option.value eq 'M'}Medium {elseif $option.value eq 'L'}Large
									{elseif $option.value eq 'XL'}X-Large {elseif $option.value eq
									'XXL'}XX-Large {elseif $option.value eq 'XXXL'}XXX-Large
									{else}{$option.value} {/if}</option> {/foreach}
								<option value="Others" style="padding-right: 10px; {if $unavailableOptions }{else}selected="selected"{/if}">Others</option>
							</select>
							{/if}
						<input type="text" id="otherSizeL" name="otherSize" value="" class="text-size1" style="display: {if $unavailableOptions }none{else}block{/if};">
						{/if}

					</div>
					<div class="size-options-error-msg notify-size-error size_error1"
						style="display: none;">Please specify size</div>
					<div class="clearfix"></div>
					<div class="last mt10 mb10 clear">

						<label class="font-black-1 span-2">Email</label> <input
							id="notifymeEmail" type="text" name="notifymeEmail"
							readonly="readonly" value="" style="border: 0 none;padding-left:0px">
					</div>
					<div class="last mt10 mb10 notifymeMobileComp">
						<input type="checkbox" id="notifyonmobileCheck" name="notifyonmobileCheck"
							onclick="{literal}if($('#notifymemobileDiv').css('display')=='none'){$('#notifymemobileDiv').show();_gaq.push(['_trackEvent', 'size_notify_dialog', 'subscribe_sms','logged_in']);}else{$('#notifymemobileDiv').hide();}{/literal}" />&nbsp;<label
							class="font-black-1 " style="font-weight: normal">Get notified
							via SMS?</label>
					</div>
					<div class="last mt10 mb10 notifymeMobileComp" id="notifymemobileDiv" style="display:none">
						<label class="font-black-1 span-2">Mobile</label><input
							id="notifymeMobile" type="text" readonly="readonly" value=""
							style="border: 0 none;padding-left:0px">
					</div>
					<div style="padding-top: 12px;text-align: center;">
						<div>
							<a href="javascript:void(0);" class="corners orange-bg p5 bold"
								style="margin-right: 5px;padding: 5px 10px;line-height:25px;" onclick="submitNotifyForm();">Notify
								Me</a> <a href="javascript:void(0);"
								class="corners orange-bg p5 bold" style="padding: 5px 20px;" onclick="_gaq.push(['_trackEvent', 'size_notify_dialog', 'cancel_notify']);closenotifyme();">Cancel</a>
						</div>

					</div>
				</div>
			</form>
		</div>
	</div>


	<div class="span-14 notifyme-form corners orange-bg last "
		id="notifyme-done" style="display: none;">
		<div class="login-box clearfix popbox" style="padding-bottom: 10px;">
			<span class="notifyme-form-span text-sucess"><img
				src="{$cdn_base}/skin1/images/tick-button.png"> Thanks for letting
				us know your interest in this product. You will be notified once
				this is back in stock!</span>
			<div style="text-align: center; margin: 0 0 16px 0;">
				<a href="javascript:void(0);"
					class="corners orange-bg p5 bold"
					style="padding: 5px 10px; display: inline-block"
					onclick="closenotifyme();">OK</a>
			</div>

		</div>
	</div>-->
{/if}

{if $deliveryKnowMoreEnabled}
<div class="deliveryknowmore-overlay"></div>
<div class="span-18 deliveryknowmore-popup corners orange-bg last" style="display: none;">
	<div class="deliveryknowmore-popup-content">
		<h2 class="h2b"><p style="padding:0;font-weight:bold;float:left;margin:0;">We Dispatch All Our Products Within 24 Hours</p><a href="javascript:void(0);" class="no-decoration-link right" onClick="closeDeliveryKnowMore()">Close</a></h2>
		<div class="icon-line"></div>
		{$deliveryKnowMoreMsg}
	</div>
</div>
{/if}

<div class="ReturnPolicy-overlay"></div>
<div class="span-18 ReturnPolicy-popup corners orange-bg last">
	<div class="ReturnPolicy-popup-content">
		<h2 class="h2b"><p style="padding:0;font-weight:bold;float:left;margin:0;">30 Day Returns Policy</p><a href="javascript:void(0);" class="no-decoration-link right" onClick="closeDeliveryKnowMore()">Close</a></h2>
		<div class="icon-line"></div>
		<span class="textmesg">All products sold on Myntra are covered by our 30 day Returns Policy. You can return your product within 30 days of the date of delivery in case you have any issues with product quality or sizing. On successful processing of returned item(s), you would be refunded the entire item price into your Myntra Cashback account. This can be utilized on any of your subsequent purchases on Myntra. Please refer to our <a href="{$http_location}/myntrareturnpolicy.php" target="_blank" style="color:#006699; border-bottom:1px dotted #333;">Returns Policy</a> for complete details.</span>
	</div>
</div>
{if $productDisclaimerText|trim neq ""}
<div class="articletype-disclaimer-overlay"></div>
<div class="span-18 articletype-disclaimer-popup corners orange-bg last">
	<div class="articletype-disclaimer-popup-content">
		<h2 class="h2b">
			<p style="padding:0;font-weight:bold;float:left;margin:0;">{$articleType}</p>
			<a href="javascript:void(0);" class="no-decoration-link right" onClick="closeDeliveryKnowMore()">Close</a>
		</h2>
		<div class="icon-line"></div>
		<span class="textmesg">{$productDisclaimerText}</span>
	</div>
</div>
{/if}
<div id="cart-combo-overlay" class="lightbox">
    <div class="mod">
        <div class="hd">
        </div>
        <div class="bd clearfix">
        </div>
        <div class="ft">
            <span class="right cps"></span>
        </div>
    </div>
</div>

{if !$disableProductStyle}
<div class="cashback-overlay"></div>
<div class="pdp-cb-pop span-18 cashback-popup corners orange-bg last">
	<div class="cashback-popup-content clearfix">
		<h2 class="h2b"><p style="padding:0;font-weight:bold;float:left;margin:0;">Cashback</p><a href="javascript:void(0);" class="no-decoration-link right" onClick="closeDeliveryKnowMore()">Close</a></h2>
		<div class="icon-line"></div>
		<div class="lb">
			<div>AFTER YOU PAY<span>Price<i> Rs. </i> <b class="red">{if $discountamount > 0}{math equation="x - d" x=$originalprice d=$discountamount assign="finalamount"}{$finalamount|round|number_format:0:".":","}{else}{$originalprice|round|number_format:0:".":","}{/if}</b></span></div>
			<div>YOU GET<span>Cashback* <i>Rs. </i><b class="green">{$cashback|round|number_format:0:".":","}</b></span></div>
			<div style="border-top:1px solid #333;">VALUE OF PRODUCT <span><i>Rs. </i><b>{if $discountamount > 0}{math equation=x-z x=$finalamount y=$discountamount z=$cashback assign="valueofproduct"}{else}{math equation=x-z x=$originalprice z=$cashback assign="valueofproduct"}{$valueofproduct|round|number_format:0:".":","}{/if}</b></span></div>
		</div>
		<div class="rb">
			<div style="text-align:center;"> <b>How does Cashback work?</b></div>
			On purchasing this product, you will receive a cashback credit upto the amount indicated here. Cashback is calculated on item value excluding any coupon discounts. You can redeem this on any of your subsequent purchases at Myntra. Cashback will be credited to your account instantly if you make your purchase using online payment.
			<div style="text-align:center;"><a href="{$http_location}/myntclub#cashback" target="_blank">I want to know more</a></div>
		</div>
	</div>
</div>
{/if}            

{include file="site/footer_content.tpl"}

<script type="text/javascript">
	var currSelectedArea = "{$curr_sel_area}";
	var gaSuffix = "{$productTypeLabel}_{$brandname}";
	var zoomBuyNowBlockHeight="{$zoomBuyNowBlockHeight}";
	var flattenSizeOptions="{$flattenSizeOptions}";
	var tooltipTop=parseInt("{$tooltipTop}");
    var popupsourceNotifyme=false;
    var useremail="{$login}";
    var usermobile="$mobile}";
    var selectedFlatSize=0;
    var selectedSize="UD";
</script>

{include file="site/footer_javascript.tpl"}

<!--retarget pixel tracking-->
{if $retargetPixel.retargetHttp}
	<script type="text/javascript">
		$(window).load(function () {ldelim}
			$('#retargetSpan').html("{$retargetPixel.retargetHttp}");
		{rdelim});
	</script>
	<span id="retargetSpan" style="display:none;"></span>
{/if}
<!--retarget pixel tracking-->

</body>
</html>
