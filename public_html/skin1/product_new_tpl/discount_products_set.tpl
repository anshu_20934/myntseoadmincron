			<h2 class="discount_product_set_h2 h4" style="position:relative; border-bottom: 0px none;">Special Combo Offer</h2><BR>
                        <h4 class="tool-text h4" style="position:relative">{eval var=$discounttooltiptext}</h4>
			<div class="scrollable-box clearfix last">
                        <a class="prevPage browse left"></a>
                        <div class="scrollable product-listing " style="height:420px;">
			<ul class="items items_discount_set_{$dre_placeHolderText}">
			{foreach name=widgetdataloop item=widgetdata from=$discount_set}
				{if $widgetdata.product neq '' && $widgetdata.product neq ' '}
				<li class="item {if $smarty.foreach.widgetdataloop.last}last{/if} re-itemsizes recent-viewed-size-loader">
				{if $quicklookenabled}
                                    <a class="quick-look action-btn" data-styleid="{$widgetdata.styleid}" data-href="{$widgetdata.landingpageurl}" href="javascript:void(0)"><span></span>Quick View</a>
				{/if}
				<div class="new-label"> </div>
				{if $widgetdata.discount_label}<div class="discount-label val"><span>{eval var = $widgetdata.discount_label}</span></div>{/if}
					<a href="{$http_location}/{$widgetdata.landingpageurl}" class="{if $i lt 5}loaded{else}to-be-loaded{/if}" onclick="_gaq.push(['_trackEvent',' recommedation_widget','myntra', 'pdp']); s_crossSell();">
						<div class="item-box" style="height:380px;">
                            <img id="im_product_img_{$widget.nameclean}_{$widgetdata.styleid}" src="{$widgetdata.search_image}"  height="320" width="240" alt="{$widgetdata.product}">
                            <div class="item-desc">
                            	<span class="name">{$widgetdata.product|truncate:40}</span>
	                            {if $cashback_displayOnPDPPage eq '1'}
	                                {if $widgetdata.discount > 0}
	                                    <div class="op">MRP <span class="strike">{$rupeesymbol}{$widgetdata.price|number_format:0:".":","}</span> <span style="color:#000"> &nbsp; {$rupeesymbol}{math equation="x-y" x=$widgetdata.price y=$widgetdata.discounted_price assign="discount_amount"}{$discount_amount|round|number_format:0:".":","} Off </span></div>
	                                    <div class="dp"><span style="font-weight:normal">{$rupeesymbol}</span>{$widgetdata.discounted_price|number_format:0:".":","}</div>
	                                    {eval var = $widgetdata.discount_label}
	                                {elseif $widgetdata.discount_label}
											<div class="dp"><span style="font-weight:normal">{$rupeesymbol}</span>{$widgetdata.price|round|number_format:0:".":","}</div>
											{eval var = $widgetdata.discount_label}
	                            	{else}
	                              	    {if $cashback_gateway_status eq 'on' && $widgetdata.showcashback eq 'true'}
	                                        <div class="op">Effective Price {$rupeesymbol}{math equation="x-y" x=$widgetdata.price y=$widgetdata.cashback assign="effectiveprice"}{$effectiveprice|round|number_format:0:".":","}</div>
	                                        <div class="dpv"><div class="dp"><span style="font-weight:normal">{$rupeesymbol}</span>{$widgetdata.price|round|number_format:0:".":","}</div>
	                                            <span class="cbp">with <b>10%</b></span><span class="cb">Cashback</span>
	                                        </div>
	                                    {/if}
	                            	{/if}
		                    	{else}
		                    		{if $widgetdata.discount > 0}
	                                   	<div class="dp left"><span style="font-weight:normal">{$rupeesymbol}</span>{$widgetdata.discounted_price|number_format:0:".":","}</div> <div class="op"><span class="strike" style="float:left;">{$widgetdata.price|number_format:0:".":","}</span></div>
	                                    {eval var = $widgetdata.discount_label}
	                                {else}
	                                    <div class="dp"><span style="font-weight:normal">{$rupeesymbol}</span>{$widgetdata.price|round|number_format:0:".":","}</div>
										{eval var = $widgetdata.discount_label}
	                           		{/if}
		                    	{/if}
	                    	</div>
                                <form method="post"  action="mkretrievedataforcart.php?pagetype=productdetail&lead=recently_viewed" id="form-{$widgetdata.styleid}" class="re-form-block">
                                	<input type="hidden" name="_token" value="{$USER_TOKEN}" />
                                    <input type="hidden" name="productStyleId" value="{$widgetdata.styleid}">
                                    <input type="hidden" name="quantity" value="1">
                                    <input type="hidden" name="sizename[]" value="" class="sizename">
                                    <input type="hidden" name="sizequantity[]" value="1">
                                    <input type="hidden" name="productSKUID" class="productSKUID" value="">
                                    <select class="widget-size-select size-select" id="style-{$widgetdata.styleid}">
                                        <option value="">select size</option>
                                    </select>

                                     <button class="orange-bg myntrabtn re-btn corners span-4 last add-to-cart-button" style="clear:both;">Buy Now <span style="font-size:15px;">&raquo;</span></button>
                                </form>
                                
                    	</div>
                	</a>
				</li>
				{/if}
				{assign var=i value=$i+1}
				{/foreach}
				</ul>
				</div>
    					<a id='discount_set_{$dre_placeHolderText}' class="nextPage browse right"></span></a>
    			 </div>
    		  	 <div class="clear"><hr /></div>
                                
