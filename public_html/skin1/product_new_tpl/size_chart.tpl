	<script>
			Myntra.Data.articleType='{$style_attributes.article_type_name}';
			Myntra.Data.articleTypeId ={$style_attributes.global_attr_article_type};
			Myntra.Data.ageGroup='{$style_attributes.age_group}';
			Myntra.Data.sizeMeasurementsObj = {$sizeOptionMeasurements};
			Myntra.Data.styleSizeAttributes = {$styleSizeAttributes};
			var sizeImage = '{$sizeRepresentationImageUrl}';
			if(sizeImage.substr(0,4).toLowerCase() != 'http') 
				Myntra.Data.sizeImg='{$cdn_base}/{$sizeRepresentationImageUrl}';
			else 
				Myntra.Data.sizeImg='{$sizeRepresentationImageUrl}';
				
			$(document).ready(function(){
				{if $measurementGuideText}
				$(".tab-btn").bind("click",function(){
					$(".tab-btn").toggleClass("link-tab"); 
					$(".tab-btn").toggleClass("active-tab");
					var tabid = $(this).attr("data-tabid");
					$(".tab").hide();
					$(".tab:eq("+tabid+")").show();
				});
				{/if}
			});
			
		</script>
    	<div class="hd">
    		<h2>{$style_attributes.brand} {$style_attributes.article_type_name}</h2>
    	</div>
    	
   		<div class="bd clearfix">
   		<div>
   		<!-- Portal-2053: Kundan: this should appear in tabs now. -->  
			<div id="tab-list" class="mk-f-left lft-cont">
				<!-- The tab buttons to switch -->
				<div class="tab-btns">
					<ul>
						<li data-tabid="0" class="active-tab tab-btn">Size Options</li>
						{if $measurementGuideText}
						<li data-tabid="1" class="link-tab tab-btn">Measurement Terms</li>
						{/if}
					</ul>
				</div>
				
				<!-- Begin 1st tab -->
				<div class="tab left">
					<div class="size-scrollable-box">
					{**REPEAT START**}
						{assign var='flag' value=$sizearraylength-1}
						{section name=sizearray loop=$sizearraylength-1}
						{if  $smarty.section.sizearray.index > 0}
						{if $totalMappedSizes < 5}<div class="rowCont clear clearfix">{/if}
						{if $smarty.section.sizearray.index == 1}
							<span class="size-label bold left">{$sizeOptionsLabel[$smarty.section.sizearray.index]}</span>
							<a class="prevPage browse left disabled" id="sizes-prev"></a>
						{else}
							<span class="size-label left clear clearfix">{$sizeOptionsLabel[$smarty.section.sizearray.index]}</span>
						{/if}
						<div id="size-scrollable_{$smarty.section.sizearray.index}" class="size-scrollable left {if $smarty.section.sizearray.index == 1}size-scroll-btns bold size-val{else}size-scroll-equi{/if}">
								<ul class=" {if $smarty.section.sizearray.index == 1}size-op {else} equi-sizes{/if} size-items left " id="{if $smarty.section.sizearray.index == 1}size-select{/if}">
								{foreach name=sizesArray key=key item=sizesArrayItem from=$sizeOptionsArray }
									<li data-value="{$sizesArrayItem[0]}" class="item  {if $sizesArrayItem[$flag] eq 'false' && $smarty.section.sizearray.index == 1}size-inactive{else}size-available{/if}">{$sizesArrayItem[$smarty.section.sizearray.index]}{if $sizesArrayItem[$flag] eq 'false' && $smarty.section.sizearray.index == 1}<span class="strike-line"></span>{/if}</li>
								{/foreach}
								</ul>
						</div>
						{if $smarty.section.sizearray.index == 1}
						<a class="nextPage browse right " id="sizes-next"></a>
						{/if}
						{if $totalMappedSizes < 5}</div>{/if}
						{/if}
										
						{/section}
						{**REPEAT END**}
						<span class="out-of-stock">Out Of Stock</span>
					</div>
					
					<div id="sub-btn" class="clearfix">
						<span class="sub-btn btn primary-btn">BUY NOW &raquo;</span>
					</div>
					
					<form class="re-form-block" id="sizeForm-{$styleid}" action="mkretrievedataforcart.php?pagetype=productdetail&amp;lead=size_chart" method="post">
						<input type="hidden" name="_token" value="{$USER_TOKEN}" />
		                <input type="hidden" value="{$styleid}" name="productStyleId">
		                <input type="hidden" value="1" name="quantity">
		                <input type="hidden" class="sizename" value="" name="sizename[]">
		                <input type="hidden" value="1" name="sizequantity[]">
		                <!-- <input type="hidden" value="" class="sizeid" name="sizeid">-->
		                <input type="hidden" name="productSKUID" class="productSKUID" value="">
		            </form>
				
				</div> <!-- End of Tab-1 -->
				<!-- If measurementGuideText is not defined, then don't show any more tab -->
				{if $measurementGuideText}
		 		<!-- Begin 2nd-tab for measurements -->
		 		<div class="tab left hide">
			 		<div class="measure-guide" >
						{$measurementGuideText}					
					</div>
					{if $measurementNote}
					<div class="ft">
						{$measurementNote}	
					</div>
					{/if}
		 		</div> <!-- end of 2nd tab -->
		 		{/if}
			</div><!-- end of tabs -->	
			<div class="right rgt-cont size-measurements" id="measurement-box">
				<span class="size-image main-image" /></span>
				
			</div>
			{if $style_attributes.brand_logo neq ''}
				<div class="lft-cont">
		 			<img class="brand-logo" src="{$style_attributes.brand_logo}" />
		 		</div>
			{/if}
			<div class="clearfix"></div>
			</div>
			{if $measurementToleranceText OR $measurementMasterCategory}
			<div class="measurement-type-text right">				
			{$measurementToleranceText}	
			{if $measurementMasterCategory}All measurements shown are of the {$measurementMasterCategory}. {/if}
			</div>			
			{/if}
		</div>
		 
			
