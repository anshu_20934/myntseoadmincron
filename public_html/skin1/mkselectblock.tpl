{literal}
<script language="javascript">
  function selectBlock(red1, red2)
  {
	if(document.getElementById('selectdesign').checked == true)
	{
		document.getElementById(red1).style.visibility = 'visible';
		document.getElementById(red1).style.display = '';
		document.getElementById(red2).style.visibility = 'hidden';
		document.getElementById(red2).style.display = 'none';
	}
	if(document.getElementById('selectsuggestion').checked == true)
	{
		document.getElementById(red2).style.visibility = 'visible';
		document.getElementById(red2).style.display = '';
		document.getElementById(red1).style.visibility = 'hidden';
		document.getElementById(red1).style.display = 'none';
	}
	
    
  }

  function instructionPopup(type)
  {
	if(type == 'customize')
		var message = "some message here";
	else
		var message = "some message here"

	document.getElementById('instructionPopup').style.visibility = 'visible';
	document.getElementById('instructionPopup').style.display  = '';
	document.getElementById('instructionmessage').innerHTML = message;
  }

  function hideInstrctionDiv()
  {
	document.getElementById('instructionPopup').style.visibility = 'hidden';
	document.getElementById('instructionPopup').style.display  = 'none';
	document.getElementById('instructionmessage').innerHTML = "";
  }
  </script>
{/literal}
<div id="selectblock" >
	<input type='radio' class='radio' name="selectblock" id="selectdesign" value="design" onClick="javascript:selectBlock('designbox', 'suggestionbox')" checked='checked'>&nbsp;i want to upload my own design using the design tool&nbsp;<a href="javascript:instructionPopup('customize');" style="text-decoration:none;font-size:0.7em;"><img src="./skin1/mkimages/help-icon.gif" style="border:0px;"/></a>
</div>
<div id="selectblock" >
	<input type='radio' class='radio' name="selectblock" id="selectsuggestion" value="sugg"  onClick="javascript:selectBlock('designbox', 'suggestionbox')">&nbsp;i want to provide text description for my design theme&nbsp;<a href="javascript:instructionPopup('suggestion');" style="text-decoration:none;font-size:0.7em;"><img src="./skin1/mkimages/help-icon.gif" style="border:0px;"/></a>
</div>
