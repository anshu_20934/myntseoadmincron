{assign var="query" value="$query"|lower|replace:'/':'%2f'|urlencode}

<form name="filter-form" class="filter-form" id="filter-form-block">
	<div class="filter-title filter-by-title pl10 clearfix"><span class="left">FILTERS</span><span class="right div-block-plus" style="display:none;"></span><span class="right div-block-minus"></span></div>
	<div class="div-block-content">
	{if $article_type_filters}
	<div class="filter-type-block">
	{if $query_level != "FULL"}
		<div class="filter-title"><span class="filter-block-minus"></span><span class="filter-block-plus" style="display:none"></span><span class="filter-title-text">What do you want?</span></div>
		<ul class="filter-type-content article-types-filters">
		{foreach key=article_type item=count from=$article_type_filters}
			<li class="filter-box">
				{assign var="url_desc_l" value="$query-$article_type"|lower|replace:' ':'-'}
				<a href="{$http_location}/{myntralink url_desc="$url_desc_l" parent_landingpage_url="$query" global_attr_article_type_facet="$article_type" is_landingpage="y"}" class="filter">{$article_type|lower|capitalize}</a>
				<!--span class="count"> ({$count})</span--></li>
		{/foreach}
		</ul>
		{/if}
	</div>
	{/if}
	{if $rangeMin neq $rangeMax}
	<div class="filter-type-block">
		<div class="filter-title"><span class="filter-block-minus"></span><span class="filter-block-plus" style="display:none"></span><span class="filter-title-text">Price</span></div>
		<ul class="filter-type-content price-filters">
			{if $priceBreakup}
				{foreach item=range from=$priceBreakup}
					<li class="filter-box {$range.rangeMin}-{$range.rangeMax}-price-filter" data-rangeMin="{$range.rangeMin}" data-rangeMax="{$range.rangeMax}">
						Rs {$range.rangeMin} - Rs {$range.rangeMax}
					</li>
				{/foreach}
			{/if}
			<li class="filter-price-slider">
				<div id="leftSlider">
	   				<div id="range">Range <span id="amount"></span></div>
					<div id="slider"></div>
				</div>	
			</li>
		</ul>
	</div>
	{/if}
	{if $fit_filters_arr && $fit_filters_arr_count gt 1}
	<div class="filter-type-block">
		<div class="filter-title">
			<span class="filter-block-minus"></span>
			<span class="filter-block-plus" style="display:none"></span>
			<span class="filter-title-text">Who is it for?</span>
		</div>
		<ul class="filter-type-content cbx-filters gender-filters">
		{foreach key=fit item=count from=$fit_filters_arr}
			<li class="filter-box unchecked {$fit|lower|trim|regex_replace:'/\W+/':''}-gender-filter" data-gender="{$fit|lower}">
				<span class="cbx"></span>
				{if $fit|lower eq "boy" }Boys{elseif $fit|lower eq "girl"}Girls{else}{$fit|lower|capitalize}{/if}
			</li>
		{/foreach}
		</ul>
	</div>
	{/if}
	{if $brands_search_results && $brands_search_results_count gt 1}
	<div class="filter-type-block">
		<div class="filter-title">
			<span class="filter-block-minus"></span>
			<span class="filter-block-plus" style="display:none"></span>
			<span class="filter-title-text">Brands</span>
		</div>
		<ul class="filter-type-content brands-filter-box cbx-filters brand-filters">
			{foreach key=brand item=count from=$brands_search_results}
				<li class="filter-box unchecked {$brand|lower|trim|regex_replace:'/\W+/':''}-brand-filter" data-brand="{$brand}">
					<span class="cbx"></span>
					{$brand}
				</li>
			{/foreach}
		</ul>
	</div>
	{/if}
	{*if $query|lower|replace:" ":"" neq "tshirt" && $query|lower|replace:" ":"" neq "tshirts" && $query|lower|replace:" ":"" neq "shirt" && $query|lower|replace:" ":"" neq "shirts" && $query|lower|replace:" ":"" neq "jersey" && $query|lower|replace:" ":"" neq "jerseys"*}
	{*if $query|lower|strpos:"shoe" !==false*}
		{if $sizes_filter && $sizes_filter_count gt 1}
		<div class="filter-type-block">
			<div class="filter-title">
				<span class="filter-block-minus"></span>
				<span class="filter-block-plus" style="display:none"></span>
				<span class="filter-title-text">Size</span>
			</div>
			<ul class="filter-type-content size-filter-box size-filters clearfix sizes">
				{foreach key=size item=count from=$sizes_filter}
				<li class="size-filter-box" data-size="{$size|lower}"><a class="filter corners unselected {$size|lower|trim|regex_replace:'/\W+/':''}-size-filter">{$size|upper|replace:" ":""}</a></li>
				{/foreach}
			</ul>
		</div>
		{/if}
	{*/if*}
	{if $article_type_attributes}
	{foreach key=attribute item=props from=$article_type_attributes}
	<div class="filter-type-block">
		<div class="filter-title">
			<span class="filter-block-minus"></span>
			<span class="filter-block-plus" style="display:none"></span>
			<span class="filter-title-text">{$props.title}</span>
		</div>
		<ul class="filter-type-content cbx-filters attrib-filters {$props.title|regex_replace:'/\W+/':''}_article_attr-ata-filter">
		<input type="hidden" class="attrib-name" value="{$props.title|replace:' ':'_'}_article_attr">
		{foreach key=prop item=count from=$props.values}
			{if $prop neq 'na'}
			<li class="filter-box unchecked {$prop|lower|replace:' ':'-'}-ata-filter" data-ata="{$prop|lower}">
				<span class="cbx"></span>
				{$prop|lower|capitalize}
			</li>
			{/if}
		{/foreach}
		</ul>
	</div>
	{/foreach}
	{/if}
</div>	
</form>
