<div class="header clearfix">
    <!-- Top Strip -->
    <div class="header-messages black-bg clearfix">
	    <div class="header-messages-content clearfix">
	    	{$topHeaderMessage}
	    	<a class="right">Call {$customerSupportCall} ({$customerSupportTime})</a>
	    </div>
    </div>
    <div class="header-logo-signup-block clearfix">
    	<div class="corners myntra-logo left"><a href="{$http_location}" title="Online Shopping store for Branded Shoes, Clothing, Accessories & Fashion wear in India."></a></div>
    	<div class="span-24 right clearfix topstript last">
    <div class="new-sign {if $login}hide{/if}">
    	<div class="left fb-connect-box">
    		<span>GET Rs.{$mrp_fbCouponValue} WHEN YOU</span>
    		<div class="fb-connect" onClick="_gaq.push(['_trackEvent', 'header', 'fb_connect', 'click']);fbLogin('{$mrp_referer}')"></div>
    	</div>
    	<div class="left new-user-box">
    		<span>NEW USER?</span>
            <div class="last corners grey-bg1 red-border newuserw sign-up-border left"><a href="javascript:void(0);" class="newuser" onclick="_gaq.push(['_trackEvent', 'header', 'signup', 'click']);">SIGN UP</a>
				<div class="signup-box-overlay"></div>
               <div class="span-14 login-form user-form corners orange-bg last">
               		
                	<div class="loading-overlay hide">
						<img src="{$cdn_base}/skin1/myntra_images/loading-graphic.gif" alt="loading graphic">
					</div>
                    <div class="login-box sign-up clearfix">
                <div class="clearfix"><div class="login-title left">Create a new account</div><a class="no-decoration-link signup-close right" href="javascript:void(0)">Close</a></div>
                <div class="icon-line"></div>
                	<form method="post" id="signup" name="signup" action="{$http_location}/myntra/ajax_signup.php" class="clearfix">
                    <fieldset class="field-hide">
                	<legend>
                        <input type="hidden" name="menu_usertype" value="C" >
                        <input type="hidden" name="menu_redirect" value="1" >
                        <input type="hidden" name="menu_filetype" value="{$pagename}">
                        <input type="hidden" name="mode" value="signup" >
                        <input type="hidden" name="mrp_referer" id="mrp_referer" value="{$mrp_referer}" />
                	</legend>
                	</fieldset>
                			<div class="signup-form-left span-13 left clearfix">
                  			<div class="last mt10 mb10">
                   				<label class="font-black-1 span-3">Email</label>
                   				<input type="text" value="Your Email Address" name="menu_username" id="su-username" class="blurred">
                   				<div class="prepend-3 su-username-error error"></div>
                   			</div>
                   			<div class="last mb10">
                   				<label class="font-black-1 span-3">Password</label>
                   				<input type="password" value="" name="menu_password" id="su-password">
                   				<div class="prepend-3 su-password-error error"></div>
                   			</div>
                   			<div class="last mt10 mb10">
                   				<label class="font-black-1 span-3">Mobile (+91)</label>
                   				<input type="text" value="" name="menu_mobile" id="su-mobile" maxlength="10" />
                   				<div class="prepend-3 su-mobile-error error"></div>
                   			</div>
	{if $smarty.get.ref neq ''}
	<div style="padding:4px 0 0 90px;" class="query">Why am I being asked for my mobile no <a style="cursor:pointer" class="vtooltip" rel="<div style='padding:5px'>To maintain the exclusivity of the Mynt Club, all users are required to verify their mobile numbers before they can sign up and enjoy the benefits of membership. Your mobile number will not be shared with any third party or for sending you any unsolicited promotional communication. We may use it to contact you regarding your orders placed with us, but only if absolutely necessary.</div>"><img style="margin-bottom:-6px" src="{$cdn_base}/skin1/images/question.gif"></a></div>

                                                <div class="reg_invalid-mobile hide" align="center"><font color=red>Please enter a valid mobile number and try again</font></div>
                                                <div class="reg_incorrect-mobile-code hide" align="center"><font color="red">The entered code is incorrect. Please recheck and enter again. If you have not received your verification code, please wait for atleast 2 minutes before sending a new verification code to your mobile.</font></div>
                                                <div class="reg_mobile-code-verified hide" style="align: center;">Your mobile has been verified. You will be directed to myntra in 2 seconds.</div>
                                                <div class="reg_excedded-attempts hide" align="center"><font color="red">You have excedded the maximum no. of attempts to verify this mobile number. If you have still not received the verification code or are facing any other difficulties, please provide an alternate number and retry.</font></div> {/if}
 <div class="reg_myntra-agreement" align="center">By signing up, I indicate that I am in agreement with Myntra's Terms of Use and Privacy Policy<br></div>


                   			<div class="prepend-4 mt10 last mb10 clearfix">
					{if $smarty.get.ref eq ''}
                   				<input type="submit"  value="Signup" id="newuser" class="newuser sbtn orange-bg corners">
					{else} 
						<input type="submit"  value="Signup" id="newuser" class="newuser sbtn orange-bg corners hide">
						<input type="button"  value="Submit" id="reg_signupbtn" class="menu_reg-verify-mobile orange-bg corners sbtn" onclick="verifyMobile2()">
							{/if}								
                   			</div>
					<div class="reg_mobile-code-entry hide" align="center">
                            		<div id="reg_mobile-message" class="hide"></div>
                                                <p class="reg_verify-code"><br>Verification Code:<input id="reg_mobile-code" name="mobile-code" size="4" />
                                                &nbsp;&nbsp;&nbsp;
                                                <input type="button" value="Submit" id="reg_submit-code" class="submit-code sbt orange-bg corners" onclick="verifyCode2()" />
                                                <!--<input type="button" value="Cancel" id="cancel-submit-code" class="cancel-submit-code sbt orange-bg corners" />-->
					</div>
					
					<div align="center">

					<div class="reg_captcha-block hide">
						<div class="notification-success"></div>
						<div class="notification-error"></div>
						<form id="reg_captchaform" method="post" action="">
							<div class="clearfix captcha-details" align="center">
								<div class="p5 corners black-bg4">
								<img src="{$cdn_base}/skin1/images/spacer.gif" id="reg_captcha" />
								</div>
								<a href="javascript:void(0)" onclick="document.getElementById('reg_captcha').src='{$http_location}/captcha/captcha.php?id=menuRegistration&rand='+Math.random();document.getElementById('reg_captcha-form').focus();"
								id="change-image" class="no-decoration-link">Change text</a>
							</div>
								
							<div class="clearfix mt10 reg_captcha-entry">								
								<span>Type the text you see in the image above to generate verification code.</span>
								<input type="text" name="userinputcaptcha" id="reg_captcha-form" />
									<a href="javascript:void(0)" class="next-button reg_verify-captcha orange-bg corners p5" onClick="reg_verifyCaptcha()">Submit</a>
									<div class="reg_captcha-loading" style="display:none;"></div>
							</div>
							<div class="reg_captcha-message hide"></div>
						</form>
					</div>
					</div>	

					<div class="reg_resend-verification-entry hide clear" align="center">
                                        	<div class="reg_common-message">If you have not received your verification code, please wait for at least 2 minutes before sending a new verification code to your mobile.</div>
                                                <div class="last mb20 clearfix">
                                                	<div style="clear:both;"> </div>
                                                	<input type="button" value="Resend verification code" class="reg-verify-mobile orange-bg corners sbtn" id="reg_resend-verification-code" onclick="verifyviaMobile2()" style="width: 136px;margin-top: 3px;">
                                                </div>
                                        </div>
                   			
                   		</div>
                	</form>
            	</div>
                </div>
            </div>
            </div>
            
            <div class="cart-box last corners grey-bg1 red-border header-button right"><a href="mkmycart.php?at=c&amp;pagetype=productdetail" class="cart-link">CART ({if $totalQuantity}{$totalQuantity}{else}0{/if})</a></div>
            <div class="corners grey-bg1 red-border header-button signinw sign-up-border right">
            	<a href="javascript:void(0);" class="sign" onclick="_gaq.push(['_trackEvent', 'header', 'signin', 'click']);">LOGIN</a>
            	<div class="signin-box-overlay"></div>
				<div class="span-14 login-form corners orange-bg last" id="login-form">
                	<div class="loading-overlay hide">
						<img src="{$cdn_base}/skin1/myntra_images/loading-graphic.gif" alt="loading graphic">
					</div>
                    <div class="login-box clearfix">
                		<div class="clearfix"><div class="login-title left">Log in to myntra.com</div><a class="no-decoration-link signin-close right" href="javascript:void(0)">Close</a></div>
                		<div class="icon-line"></div>
	                	<form method="post" id="forgot-password" name="forgot-password" action="forgot-password.php" class="no-decoration-link clearfix">
	                		<div class="forgot-password-form-left span-15 left">
	                			<div class="span-13 last mt10 mb10">
	                   				<label class="font-black-1 span-3">Email</label>
	                   				<input type="text"  value="Your Email Address" name="username" id="fp-username" class="blurred">
	                   				<div class="prepend-3 span-11 fp-username-error error"></div>
	                   			</div>
	                   			<div class="prepend-3 span-10 last mb10">
	                   				<input type="submit"  value="Reset Password" id="menu_resetpassword" class="resetpassword sbtn orange-bg corners left" onkeydown="">
	                   				<a href="javascript:coid(0)" class="return-to-login no-decoration-link">Return to Login</a>
	                   			</div>
	                   		</div>
	                	</form>
	                	<form method="post" id="signin" name="signin" action="{$http_location}/myntra/ajax_signup.php" class="clearfix">
		                	<fieldset class="field-hide">
			                	<legend>
			                        <input type="hidden" name="menu_usertype" value="C" >
			                        <input type="hidden" name="menu_redirect" value="1" >
			                        <input type="hidden" name="menu_filetype" value="{$pagename}">
			                        <input type="hidden" name="mode" value="signin" >
			                    </legend>
		                    </fieldset>
	                		<div class="signin-form-left span-13 left clearfix">
	                  			<div class="last mt10 mb10">
	                   				<label class="font-black-1 span-3">Email</label>
	                   				<input type="text"  value="Your Email Address" name="menu_username" id="username" class="blurred">
	                   				<div class="prepend-3 username-error error"></div>
	                   			</div>
	                   			<div class="last mb10">
	                   				<label class="font-black-1 span-3">Password</label>
	                   				<input type="password"  value="" name="menu_password" id="password">
	                   				<div class="prepend-3 password-error error"></div>
	                   			</div>
	                   			<div class="prepend-3 last mb10 clearfix">
	                   				<input type="submit"  value="Login" id="existinguser" class="existinguser sbtn orange-bg corners left">
	                   				<a href="javascript:void(0)" class="forgot-password no-decoration-link" >Forgot Password?</a>
	                   			</div>
	                   		</div>
	                	</form>
            		</div>
                </div>
            </div>
           
        </div>
        
        
        <div class="post-login-exp {if $login}{else}hide{/if}">
        	{if !$facebook_uid || $facebook_uid eq ''}
        	<div class="last left fb-connect-post-login">
        		<div class="fb-connect" onClick="fbLogin()"></div>
        	</div>
        	{/if}
        	<div class="last post-login-details right">
        		<div class="right clearfix">
		        	<div class="last post-login-box clearfix left">
			        	<span id="logged_in_user_profile_picture" class="left">
			        	{if $facebook_uid neq ""}
			        	<img src="https://graph.facebook.com/{$facebook_uid}/picture?type=square"/>
			        	{/if}
			        	</span>
			        	{if $userfirstname neq ""}
			        	<span id="logged_in_user_name" class="left">{$userfirstname|truncate:14},</span>
			        	{else}
			        	<span id="logged_in_user_name">{$login|truncate:14},</span>
			        	{/if}
			        </div>
			        <div class="mrp-content left">
			        	<span class="mrp-prefix">your </span> 
	    				<div id="headerMessage" class="left">{if $login}{$mrp_header_message}{/if}</div>
	    				<a id="logout_from_account" class="no-decoration-link post-login-links logout-link">Logout</a>
	    			</div>
    			</div>
    			<div class="post-login-actions clearfix span-8 right last">
					<div class="cart-box last corners grey-bg1 red-border pl header-button right"><a href="mkmycart.php?at=c&amp;pagetype=productdetail" class="cart-link" id="cartquantityanchor">CART ({if $totalQuantity}{$totalQuantity}{else}0{/if})</a></div>
					<div class="cart-box last corners grey-bg1 red-border pl my-myntra-pl header-button right"><a href="{$http_location}/mymyntra.php" class="no-decoration-link post-login-links my-myntra-link">My Myntra</a></div>
        		</div>
	        </div>
	        <form id="toplogoutform" method="post" action="{$http_location}/include/login.php">
			<fieldset class="field-hide">
               	<legend>
				<input type="hidden" value="logout" name="mode">
			    </legend>
			</fieldset>
			</form>
        </div>
    </div>
   </div> 
    <div class="header-menu-bar theme-bg clearfix">
	    <div class="header-menu-bar-content clearfix">
	    	{$top_nav}
	    </div>
    </div>
</div>
{if $SEMpromo eq 1}
	<div class="header-notice mt10 clearfix corners" style="height:30px;line-height:30px;font-weight:normal;background-image:none; background-color: #EEEEEE; border: 1px solid #E0E0E0; padding: 2px 6px;  white-space: nowrap;"><a href="{$semPromoUrl}" class="no-decoration-link" style="display:block;color: #333333;" onclick="_gaq.push(['_trackEvent', 'header_promo', 'REbanner_click']);">{$semPromoHeaderText}&nbsp;&nbsp;<span class="promo-learn-more" style="text-decoration:underline;color:#006699;font-size:70%;">View Now</span></a></div>
{elseif $promoHeaderEnabled}
    {if $login && $promoHeaderLoggedinText}
        <div class="header-notice mt10 clearfix corners" style="font-weight:normal;background-image:none;background-color:#eeeeee;border-bottom:1px solid #666;overflow:hidden;margin:0;height:25px;">{$promoHeaderLoggedinText}</div>
    {else}
	    <div class="header-notice mt10 clearfix corners" style="font-weight:normal;background-image:none;background-color:#eeeeee;border-bottom:1px solid #666;overflow:hidden;margin:0;height:25px;">{$promoHeaderText}</div>
	{/if}
{else}
	<!--a href="{$http_location}/footwear" class="header-notice mt10" onclick="showLearnMoreDetails()"></a-->
{/if}
<div class="divider">&nbsp;</div>

{if $abLoginPopup eq 'splash'}
<div id="lb-login" class="lightbox lb-login">
    {include file="site/mod_login.tpl"}
</div>
{/if}
{*<span class="cbtip hide" style="display:none"><b style="display: block;">Cashback</b> On purchasing this product, you will receive a cashback credit upto the amount indicated here. Cashback is calculated on item value excluding any coupon discounts. You can redeem this on any of your subsequent purchases at Myntra.</span>*}