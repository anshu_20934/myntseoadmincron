<div class="mod v2">
    <div class="hd">
        <img alt="Myntra Logo" src="{$cdn_base}/skin1/myntra_images/logo.png" width="150" height="34">
        <h2>Login / Sign up</h2>
    </div>
    <div class="bd clearfix">
        <form method="post" action="{$http_location}/mklogin.php?{$xsessionname}={$xsessionid}">
            <div class="row">
                <label>Your Email Address</label>
                <input type="text" name="email" value="">
                <div class="err err-user"></div>
            </div>
            <div class="row">
                <label>Password</label>
                <input type="password" name="password">
                <div class="err err-pass"></div>
            </div>
            <div class="old-user">
                <h3>Existing Customers</h3>
                <div>
                    <button type="button" class="link-btn btn-forgot-pass">Forgot your password?</button>
                </div>
                <button type="submit" class="action-btn btn-signin">Login &raquo;</button>
                <p>Login to <strong>track</strong> your orders, view and use <strong>Mynt Credits</strong> and make purchases <strong>faster</strong>.</p>
            </div>
            <div class="new-user">
                <h3>New to Myntra?</h3>
                <div>
                    <label for="menu_mobile">Mobile (+91)</label>
                    <input type="text" name="mobile" size="8" maxlength="10">
                    <span class="err err-mobile"></span>
                </div>
                <button type="submit" class="action-btn btn-signup">Sign Up &raquo;</button>
                <p>
                    Sign up and get <strong class="price">{$rupeesymbol}{$mrp_nonfbCouponValue}</strong> worth of <strong>Mynt Credits</strong> for your first purchase.
                </p>
            </div>
            <div class="msg-agree">
                By signing up you indicate that you are in agreement with Myntra's 
                <a href="/termandcondition.php">Terms of Use</a> and <a href="/privacy_policy.php">Privacy Policy</a>
            </div>

            <input type="hidden" name="_token" value="{$USER_TOKEN}">
            <input type="hidden" name="menu_usertype" value="C">
            <input type="hidden" name="menu_redirect" value="1">
            <input type="hidden" name="mode" value="signup">
            <input type="hidden" name="menu_filetype" value="{$pagename}">
            <input type="hidden" name="mrp_referer" id="mrp_referer" value="{$mrp_referer}">
        </form>
        <div class="facebook">
            <strong>OR</strong>
            <div class="buttons">
                <button type="button" class="action-btn btn-fb-connect" data-referer="{$mrp_referer}">
                    <span class="ico-fb-small"></span> Connect with Facebook &raquo;</button>
                <div class="err err-fb"></div>
            </div>
            <p>
                Sign up through Facebook and get <strong class="price">{$rupeesymbol}{$mrp_fbCouponValue}</strong> worth of <strong>Mynt Credits</strong> for your first purchase.
            </p>
        </div>
        <div class="forgot-pass-msg">
            <p>An email with a link to reset your password has been sent to:</p>
            <p class="email"></p>
            <p>Hope to see you back soon!</p>
            <button type="button" class="link-btn btn-return-to-login">Try logging in again?</button>
            <button type="button" class="action-btn btn-forgot-pass-ok">OK</button>
        </div>
    </div>
</div>

