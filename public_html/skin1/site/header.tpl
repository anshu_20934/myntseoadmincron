
 <title>
      {if $pageTitle ne ''}{$pageTitle}{elseif $pageHeader ne ''}{$pageHeader}{else}Myntra{/if}
  </title>
  <META http-equiv="Content-Style-Type" content="text/css">
  <META http-equiv="Content-Script-Type" content="type">
  <META http-equiv="Content-Type" content="text/html; charset=utf-8">


  <meta name="description" content="{$metaDescription|escape}">
  <meta name="keywords" content="{$metaKeywords|escape}">
  {if $noindex eq 2}
	<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
  {/if}
  <link rel="shortcut icon" href="{$cdn_base}/skin1/icons/favicon.ico">
	  
	{if $pageTypeSearch}
	<link rel="canonical" href="{$canonicalPageUrl}">
	{/if}
    {foreach from=$cssFiles item=href}
        <link href="{$href}" rel="stylesheet" type="text/css">
    {/foreach}

  
  {if $tooltip }
  	{$tooltip}
  {/if}
  {if $newrelic_header}
    {$newrelic_header}
  {/if}
  <base href="{$baseurl}">
  <script type="text/javascript">
    var http_loc = '{$http_location}';
    var https_loc = '{$https_location}';
    var xsessionid = '{$xsessionid}'
    var cdn_base = '{$cdn_base}';
    var facebook_app_id = '{$facebook_app_id}';
    var lists_image_srcs = new Object() ;
    var searchBoxText='{$searchBoxText}';
    var s_account= '{$analytics_account}';
    var enableMyThings=0;
    {if $enableMyThings == 'true'}enableMyThings=1;{/if}
    var shownewui=0;
    {if $shownewui == 'newui'}shownewui=1;{/if}
    //AVAIL
    var pageName="home";
    var loginTimeout={if $loginTimeout}{$loginTimeout}{else}30{/if};
    //END AVAIL
    
    {literal}
    var Myntra = Myntra || {};
    Myntra.Data = Myntra.Data || {};
    {/literal}
    Myntra.Data.userEmail = "{$login}";
    Myntra.Data.abHeaderUi = "{$abHeaderUi}";
	Myntra.Data.nonFBRegCouponValue = "{$mrp_nonfbCouponValue}";
    Myntra.Data.token = "{$USER_TOKEN}";
    Myntra.Data.showAdminAdditionalStyleDetails = "{$showAdminAdditionalStyleDetails}";
    Myntra.Data.cookiedomain = "{$cookiedomain}";
    Myntra.Data.cookieprefix = "{$cookieprefix}";
    Myntra.Data.showAutoSuggest = "{$enableAutoSuggest}";
    Myntra.Data.pageName="static";
  </script>
