<div class="mod v3">
    <div class="hd">
        <h2>Welcome to India's largest Online Fashion Destination</h2>
    </div>
    <div class="bd clearfix">
        <div class="main-content">
        <form method="post" class="frm-login" action="{$http_location}/mklogin.php?{$xsessionname}={$xsessionid}">
            <h3>Login now</h3>
            
            <label>Email address</label>
            <input type="text" name="email" value="">
            <div class="err err-user"></div>

            <label>Password</label>
            <input type="password" name="password">
            <div class="err err-pass"></div>

            <div class="buttons">
                <button type="submit" class="action-btn btn-signin">Login &raquo;</button>
                <button type="button" class="link-btn btn-forgot-pass">Forgot your password?</button>
            </div>

            <input type="hidden" name="_token" value="{$USER_TOKEN}">
            <input type="hidden" name="mode" value="signin">
            <input type="hidden" name="menu_usertype" value="C">
        </form>
        <form method="post" class="frm-signup" action="{$http_location}/mklogin.php?{$xsessionname}={$xsessionid}">
            <h3>Sign up to be a member</h3>

            <label>Email address</label>
            <input type="text" name="email" value="">
            <div class="err err-user"></div>

            <label>Password</label>
            <input type="password" name="password">
            <div class="err err-pass"></div>

            <label for="menu_mobile">Your mobile number</label>
            <span class="mobile-prefix">+91</span> <input type="text" name="mobile" size="8" maxlength="10">
            <div class="err err-mobile"></div>

            <button type="submit" class="action-btn btn-signup">Sign Up &raquo;</button>
            <p>Sign up and get {$rupeesymbol}{$mrp_nonfbCouponValue} worth of Mynt Credits</p>

            <input type="hidden" name="_token" value="{$USER_TOKEN}">
            <input type="hidden" name="mode" value="signup">
            <input type="hidden" name="menu_usertype" value="C">
            <input type="hidden" name="mrp_referer" id="mrp_referer" value="{$mrp_referer}">
        </form>
        <div class="facebook">
            <strong class="or">or</strong>
            <button type="button" class="action-btn btn-fb-connect" data-referer="{$mrp_referer}">
                <span class="ico-fb-small"></span> Connect with Facebook &raquo;</button>
            <div class="err err-fb"></div>
        </div>
        </div>
        <div class="forgot-pass-msg">
            <p>An email with a link to reset your password has been sent to:</p>
            <p class="email"></p>
            <p>Hope to see you back soon!</p>
            <button type="button" class="link-btn btn-return-to-login">Try logging in again?</button>
            <button type="button" class="action-btn btn-forgot-pass-ok">OK</button>
        </div>
    </div>
</div>

