  {assign var=wi value=0}
		    <div class="category  clearfix mb20">
    		   <h2 class="h4" style="position:relative">{$avail_title}</h2>
           		  <div class="product-listing clearfix last">
           		     		<div class="scrollable {if $cashback_displayOnCartPage neq '1'}without_cb_rescroll{else}rescroll{/if}" style="left:0px;">
       						<ul class="items ">
    						{foreach name=widgetdataloop item=widgetdata from=$avail_recommendation.data}
    						{if $widgetdata.product neq '' && $widgetdata.product neq ' '}
    						<li class="recent-viewed-size-loader item re-itemsizes" {if $i==4}style="margin-right:0;"{/if} data-id="prod_{$widgetdata.id}">
    						{if $quicklookenabled}
								<span class="quick-look action-btn" data-widget="avail_widget" data-styleid="{$widgetdata.styleid}" data-href="{$widgetdata.landingpageurl}" href="javascript:void(0)"><span class="quick-look-icon"></span>Quick View</span>
							{/if}
							<div class="style-details" style="display:none"></div>
                             <div class="new-label"> </div>
                            <div class="item-box {if $cashback_displayOnCartPage neq '1'}without_cb_reitem-box{else}reitem-box{/if}">
                                  <a href="{$widgetdata.landingpageurl}?t_code={$tracking_code}" class="loaded" onclick="_gaq.push(['_trackEvent', 'pdp_click', Myntra.Data.pageName, 'avail_widget']);">
                                  	<img id="im_product_img_{$widget.nameclean}_{$widgetdata.styleid}" src="{$widgetdata.search_image}"  height="320" width="240" alt="{$widgetdata.product}">
                                  </a>
                                <div class="item-desc">
                                	<span class="name">
                                    	<a href="{$http_location}/{$widgetdata.landingpageurl}?t_code={$tracking_code}">{$widgetdata.product|truncate:40}</a>
                                    </span>
                                	{if $cashback_displayOnCartPage eq '1'}
                                		{if $widgetdata.discount > 0}
                                    		<div class="op">MRP <span class="strike">{$rupeesymbol}{$widgetdata.price|number_format:0:".":","}</span> <span style="color:#000"> &nbsp; {$rupeesymbol}{math equation="x-y" x=$widgetdata.price y=$widgetdata.discounted_price assign="discount_amount"}{$discount_amount|round|number_format:0:".":","} Off </span></div>
                                    		<div class="dp"><span style="font-weight:normal">{$rupeesymbol}</span>{$widgetdata.discounted_price|number_format:0:".":","}</div>
                                    			{eval var=$widgetdata.discount_label}
                                		{elseif $widgetdata.discount_label}
                                                <div class="dp"><span style="font-weight:normal">{$rupeesymbol}</span>{$widgetdata.price|round|number_format:0:".":","}</div>
                                                 {eval var=$widgetdata.discount_label}
                            			{else}
                              	    		{if $cashback_gateway_status eq 'on' && $widgetdata.showcashback eq 'true'}
                                        		<div class="op">Effective Price {$rupeesymbol}{math equation="x-y" x=$widgetdata.price y=$widgetdata.cashback assign="effectiveprice"}{$effectiveprice|round|number_format:0:".":","}</div>
                                        		<div class="dpv"><div class="dp"><span style="font-weight:normal">{$rupeesymbol}</span>{$widgetdata.price|round|number_format:0:".":","}</div>
	                                            <span class="cbp">with <b>10%</b></span><span class="cb">Cashback</span>
                                        		</div>
                                    		{/if}
                            			{/if}
									{else}
                                		{if $widgetdata.discount > 0}
                                    		<div class="dp left"><span style="font-weight:normal">{$rupeesymbol}</span>{$widgetdata.discounted_price|number_format:0:".":","}</div> <div class="op"><span class="strike" style="float:left;">{$widgetdata.price|number_format:0:".":","}</span></div>
                                    		{eval var=$widgetdata.discount_label}
                                		{else}
                                                <div class="dp"><span style="font-weight:normal">{$rupeesymbol}</span>{$widgetdata.price|round|number_format:0:".":","}</div>
                                                {eval var=$widgetdata.discount_label}
                                		{/if}
                             		{/if}
                             	</div>

                                <form method="post"  action="mkretrievedataforcart.php?pagetype=productdetail&lead=recently_viewed" id="form-{$widgetdata.styleid}" class="re-form-block"
                                        onsubmit="_gaq.push(['_trackEvent', 'buy_now', Myntra.Data.pageName, 'avail_widget']);">
                                    <input type="hidden" name="_token" value="{$USER_TOKEN}" />
                                    <input type="hidden" name="productStyleId" value="{$widgetdata.styleid}">
                                    <input type="hidden" name="quantity" value="1">
                                    <input type="hidden" name="sizename[]" value="" class="sizename">
                                    <input type="hidden" name="sizequantity[]" value="1">
                                    <input type="hidden" name="productSKUID" class="productSKUID" value="">
                                    <select class="widget-size-select size-select" id="style-{$widgetdata.styleid}">
                                        <option value="">select size</option>
                                    </select>

                                     <button class="orange-bg myntrabtn re-btn corners span-4 last add-to-cart-button" style="clear:both;">Buy Now <span style="font-size:15px;">&raquo;</span></button>
                                </form>
                            </div>
                            </li>


    						{/if}
							{assign var=i value=$i+1}
    						{/foreach}
    						</ul>
    					</div>

    			 </div>
    		  	 <div class="clear"><hr /></div>
    		</div>

