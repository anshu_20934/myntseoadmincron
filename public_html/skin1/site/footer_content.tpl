<div class="mynt-footer">
    <div class="footer-content">
    	{if $megaFooterEnabled}
        <div class="mynt-links-block">
            <div class="mynt-footer-links">
                <ul>
                    <li>
                        <strong>SHOP NOW!</strong>
                        <a href="{$http_location}/men">Men</a>
                        <a href="{$http_location}/women">Women</a>
                        <a href="{$http_location}/kids">Kids</a>
                        <a href="{$http_location}/brands">Brands</a>
                        <a href="{$http_location}/sales">Sale</a>
                        <a href="{$http_location}/new-arrivals?s=RECENCY">New Arrivals</a>
                        <a href="{$http_location}/top-sellers">Top Sellers</a>
                        <a href="{$http_location}/footwear">Footwear</a>
                        <a href="{$http_location}/formal-wear">Formal Wear</a>
                        <a href="{$http_location}/casual-wear">Casual Wear</a>
                        <a href="{$http_location}/ethnic-wear">Ethnic Wear</a>
                        <a href="{$http_location}/sports-wear">Sports Wear</a>
                        <a href="{$http_location}/inner-wear">Inner Wear</a>
                        <a href="{$http_location}/winter-wear">Winter Wear</a>
                        <a href="{$http_location}/accessories">Accessories</a>
                    </li>
                    <li>
                        <strong>ABOUT US</strong>
                        <a href="{$http_location}/aboutus.php" onClick="_gaq.push(['_trackEvent', 'footer', 'aboutus', 'click']);return true;">About Us</a>
                        <a href="{$http_location}/careers.php" onClick="_gaq.push(['_trackEvent', 'footer', 'careers', 'click']);return true;">Work With Us</a>
                        <a href="{$http_location}/contactus" onClick="_gaq.push(['_trackEvent', 'footer', 'contactus', 'click']);return true;">Contact Us</a>

                        <strong class="mt20">CUSTOMER SERVICE</strong>
                        <a href="{$http_location}/mkfaq.php">FAQs</a>
                        <a href="{$http_location}/myntrareturnpolicy.php">Returns Policy</a>
                        <a href="{$http_location}/privacy_policy.php">Privacy Policy</a>
                        <a href="{$http_location}/termandcondition.php">Terms of Use</a>

                        <strong class="mt20">MY MYNTRA</strong>
                        <a href="javascript:void(0);" class="btn-login foot-login {if $login eq ""}hide{/if}">Login/Register</a>                        
                        <a href="{$http_location}/mymyntra.php">My Myntra</a>
                        <a href="{$http_location}/mkmycart.php">My Shopping Bag</a>
                    </li>
                </ul>
            </div>
            <div class="mynt-footer-promos">
                <div class="promo-block">
                    <div class="promo pipe">
                        <strong>{$totalNumStyles}</strong> STYLES<br>
                        <strong>{$totalNumBrands}</strong> BRANDS<br>
                        <span class="promo-small-text">(And counting!)</span>
                    </div>
                    <div class="promo-leaf" style="padding-top: 5px;">
                        We have the <b>largest</b> collection of the <b>latest</b> fashion and lifestyle products in India!
                    </div>
                </div>
                
                <div class="promo-block">
                    <div class="pipe">
                        <div class="certified-icon">&nbsp;</div>
                    </div>
                    <div class="promo-leaf" style="padding-top: 15px;">
                        All our products are <b>brand new</b> and <b>100% original.</b>
                    </div>
                </div>

                <div class="promo-block">
                    <div class="pipe">
                        <strong  style="padding-top: 15px;display:block;">{$customerSupportCall}</strong>
                        <strong><a href="mailto:support@myntra.com" class="maillink">support@myntra.com</a></strong>
                    </div>
                    <div class="promo-leaf" style="padding-top: 15px;">
                        Need help? Got feedback?<br>
                        Do <b>contact</b> us!
                    </div>
                </div>
                
                <div class="promo-block">
                    <div class="pipe">
                        <div  style="padding-top:17px;">
                            <a href="http://www.facebook.com/myntra" target="_blank" class="fb">&nbsp;</a>
                            <a href="http://twitter.com/myntra" target="_blank" class="tw">&nbsp;</a>
                            <a href="https://plus.google.com/109282217040005996404/posts" target="_blank" class="gp">&nbsp;</a>
                            <a href="http://www.youtube.com/user/myntradotcom" target="_blank" class="yt">&nbsp;</a>
                        </div>
                    </div>
                    <div class="promo-leaf" style="padding-top: 5px;">
                        Stay in tune with what we are doing  by visiting our <b>official channels.</b>
                    </div>
                </div>

                <div class="promo-block">
                    <div class="pipe promo-link">

                        <a href="http://stylemynt.com" target="_blank">STYLEMYNT</a>
                    </div>
                    <div class="promo-leaf" style="padding-top: 5px;">
                        Visit our <b>blog</b> for the latest global and local fashion news, views, trends, styles, and tips.
                    </div>
                </div>

            </div>
        </div>
		{/if}
        {*<div class="news-block hide">
            <div class="mynt-news">
                <strong>SIGN UP FOR MYNTRA NEWSLETTER</strong>
                <input type="text" value="Your Email Address" name="yourmail" class="newsletter">
                <button class="action-btn btn-signin" type="submit">SUBMIT</button>
                *}{*<button class="action-btn btn-signin" style="width:85px;margin-left:8px;" type="submit">WOMEN</button>*}{*
            </div>
            <div class="style-mynt">
                <strong>VISIT <a href="#">STYLEMYNT</a>, OUR STYLE BLOG...</strong>
                <span class="style-text">For the latest global and local fashion news, views, trends, styles, and tips.</span>
            </div>
        </div>*}
		{if $megaFooterEnabled}
        <div class="testimonials-block">
            <div class="testimonials">
            <div class="foot-left-arrow" id="prev">&nbsp;</div>
            <div class="testimonials-slide left">

                <strong>A DELIGHTED CUSTOMER SAYS...</strong>
                {include file="site/footer_testimonials.tpl"}
            </div>
            <div class="foot-right-arrow" id="next">&nbsp;</div>
            </div>
            <div class="love-myntra">
                <strong>DO YOU<span class="love-icon">&nbsp;</span>MYNTRA?</strong>
                <div class="footer-social-buttons">
                        <div class="fb-like-button" style="margin-bottom:10px;"> <fb:like href="https://www.facebook.com/myntra" send="false" layout="button_count" width="90" show_faces="false" action="like"></fb:like></div>
                        {*<div class="g-plus-button"><g:plusone size="medium" count="true" href="http://www.myntra.com"></g:plusone></div>*}
                </div>
            </div>
        </div>
		{/if}
        {if $mostPopularWidget}
        <div class="mynt-tags">
        {foreach name=tagtype item=fixedorDynamic from=$mostPopularWidget}
	        	{foreach name=uniquetag item=tag from=$fixedorDynamic}
	        	    {if $smarty.foreach.uniquetag.last}
	        	    <a href="{$http_location}/{$tag.link_url}">{$tag.link_name}</a>
	        	    {else}
	            	<a href="{$http_location}/{$tag.link_url}">{$tag.link_name}</a>,
	            	 {/if}
	            {/foreach}
        {/foreach}
        </div>
        {/if}

        <div class="mynt-text">
            <div class="mynt-foot-title">ABOUT MYNTRA</div>
            Myntra.com is India’s largest online fashion and lifestyle store for men, women, and kids.<br>
            Shop online from the latest collections of apparel, footwear and accessories, featuring the best brands.<br>
            We are committed to delivering the best online shopping experience imaginable.<br>
        </div>

        <div class="payment-block">
           <div class="poweredby">
                <div class="payment-title">POWERED BY</div>
                <div class="powered-icons"></div>
           </div>
           <div class="payment">
                <div class="payment-title">PAY SECURELY</div>
                <div class="payment-icons"></div>
            </div>
        </div>

        <div class="mynt-copy-right">
            &copy; myntra.com | All rights reserved
        </div>
    </div>
</div>
{include file="site/mod_quick_look.tpl"}
<div id="fb-root"></div>
<div id="tbContentElement" style="display:none">
	<div id="tbContentData"></div>
</div>
<!-- @Footer-->
