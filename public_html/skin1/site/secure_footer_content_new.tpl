 {if $orgId==""}
 <!-- Footer -->
    <div class="footer corners-tl-tr blue-bg clearfix span-33 last">
    <div class="footer-links clearfix">
        <ul>
            <li><strong>Myntra</strong></li>
            <li><a href="{$http_location}/aboutus.php">About us</a></li>
            <li><a href="{$http_location}/careers.php">Work for Us</a></li>
            <li><a href="{$http_location}/contactus">Contact Us</a></li>
        </ul>
        <ul>
            <li><strong>Need Help?</strong></li>
            <li><a href="{$http_location}/mkfaq.php">FAQs</a></li>
            <li><a href="{$http_location}/contactus">Get in touch</a></li>
            <li><a href="{$http_location}/privacy_policy.php">Privacy Policy</a></li>
            <li><a href="{$http_location}/termandcondition.php">Terms of Use</a></li>
        </ul>
        <ul>
            <li><strong>My Account</strong></li>
            <li><a href="{$http_location}/register.php">Login/Register</a></li>
            <li><a href="{$http_location}/mymyntra.php">My Account</a></li>
            <li><a href="{$http_location}/mkmycart.php">My Shopping Cart</a></li>
        </ul>
        <ul>
            <li><strong>Browse</strong></li>
            <li><a href="{$http_location}/men-clothing">Clothing for Men</a></li>
            <li><a href="{$http_location}/women-clothing">Clothing for Women</a></li>
        </ul>
        <ul class="paymeny-logos right" style="width: 305px;">
            <li><b>We Accept</b></li>
            <li><img src="{$secure_cdn_base}/skin1/images/payment-gateway.jpg" alt="payment gateway"></li>
        </ul>
    </div>
    <div class="follow-us right"><p class="left snetwork"><div class="append left"><strong>Follow us on:</strong></div><a class="icon-facebook-simple" href="http://www.facebook.com/myntra"></a><a class="icon-twitter-simple" href="http://twitter.com/myntradotcom"></a></p></div>
    <div id="copyright">&copy; myntra.com. All rights reserved.</div>
</div>
	{else}
	<div id="footer">
		{if $orgId=="22"}
		<div class="footercopy" style="width:auto;margin-right:360px;margin-left:10px;float:left">&nbsp;&nbsp;ING Vysya Bank
			Limited. All rights reserved.
		</div>
		{/if}
		<div class="footertext" style="width:auto;">
			&nbsp;&nbsp;For queries mail at : <a href="mailto:{$org_details.support}" style="text-decoration:none;">{$org_details.support}</a> {if $customerSupportCall}or Call: {$customerSupportCall}{/if}
		</div>
	</div>
	<div class="clear"></div>
{/if}
<div id="fb-root"></div>
<div id="fbFooterUserFoundHiddenContent" style="display:none"> 
<h2 style="margin-top : 10px; padding-bottom: 6px;">Welcome <span id="fbFooterUserFoundHiddenContent-Name"></span></h2>
<div class="icon-line"></div>
<br />
<h4 style="text-align:center">We found that you are already registered on Myntra</h4>
<div style="text-align:center">Your account has been linked with your facebook account</div><br />
	<table cellspacing="1" cellpadding="1" border="0" width="100%" class="table-box">
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td align="center">
				<div class="button-img"><span><a id="fbFooterUserFoundHiddenContent-ok" style="cursor:pointer" onclick="tb_remove();" >&nbsp;&nbsp;OK&nbsp;&nbsp;</a></span></div>
			</td>
		</tr>
	</table> 
</div>

<div id="tbContentElement" style="display:none">
	<div id="tbContentData"></div>
</div>
<div id="fbFooterPictureUploadHiddenContent" style="display:none">
	<div>
		<div style="float: left; display: block;">
			<img style="float: left;margin: 4px;" src="{$secure_cdn_base}/skin1/icons/indicator.gif" />
		</div>
		<div style="margin-left: 30px; height: 26px; display: block; margin-top: 12px; float: left;">
			<span style=" font-size: 20px; color: blue;  ">Uploading image to facebook ...</span>
		</div>
	</div>
</div>
<div id="fbFooterPictureUploadCompleteHiddenContent" style="display:none">
	<div style="margin-left: 10px;">
		<div class="clearfix">
			<div style="float: left; display: block;">
				<img style="float: left;margin: 4px;" src="{$secure_cdn_base}/skin1/icons/tick-button.png" />
			</div>
			<div style="margin-left: 30px; height: 26px; display: block; margin-top: 26px; float: left;">
				<span style="font-size: 20px; font-weight: bold;">
					Successfully Uploaded
				</span>
			</div>
		</div>
		<div style="display: block; margin-top: 14px;">
			<span style="font-size: 22px; font-weight: bold;">
				Just one more step!
			</span>
		</div>
		<div class="clearfix" style="margin-top: 10px;">
			<div style="float: left; display: block;border-right: 2px dotted #999999;padding-right: 15px;">
				<img class ="fb-make-my-profile-instruction-pic" src="{$secure_cdn_base}/skin1/icons/fb-image-upload.png" />
			</div>
			<div style="float: left; display: block;padding-left: 10px;padding-top: 100px;">
				<span>
					<input type="button" value="Go to Facebook Now!" class="go-to-facebook-photo" />
				</span>
			</div>
		</div>
		<div style="float: right; font-size: 16px; margin-right: 20px; text-decoration: underline; font-weight: bold;">
			<a href="javascript:void(0);" onClick="_gaq.push(['_trackEvent', 'fB_profile_pic', 'skip_dialog']);tb_remove();">Skip</a></div>
	</div>
</div>
<div id="fbFooterNewUserHiddenContent" style="display:none">
	<div>
	  <div style="text-align: left; padding-left: 0pt; margin-top : 10px; padding-bottom: 6px;"><strong style="font-size: 16px;">One final step! Create a password for Myntra</strong></div>
	  <div class="icon-line"></div>
			<form enctype="multipart/form-data" method="post"  id="fbfooterregister_form" onSubmit="facebookFooterCompleteRegistration('true');" >
					<input type="hidden" name="fbRegistration" value="1" />
					<table cellspacing="1" cellpadding="1" border="0" width="430px" class="table-box" style="margin-left: 10px; margin-top: 14px;">
						<tbody>
							<tr>
								<th align="right"><span>Login Id</span> *</th>
								<td><label><span id="fbFooterNewUserHiddenContent-Name"></span></label></td>
							</tr>
							<tr>
								<th align="right"><span>New Password</span> *</th>
								<td><input type="password" tabindex="11" size="35" class="formtextbox" name="passwd1" id="fbfooterpasswd1"><div style="font-size:85%;color: #BD1A00;"><span id="fbfooterpasswdError"></span></div></td>
							</tr>
						</tbody>
					</table>
					<div class="button-img" style="margin-left:auto; margin-right: auto;">
						<span>
							<a id="fbFooterNewUserHiddenContent-done" tabindex="12" onclick="facebookFooterCompleteRegistration('true','false');" style="cursor: pointer; color: white;">
								Done
							</a>
						</span>
					</div>
				</form>
			<div class="icon-line" style="margin-top:18px;text-align: right;height:1px;">
				<a id="fbFooterNewUserHiddenContent-skip" tabindex="13" onclick="facebookFooterCompleteRegistration('false','false');" style="font-size: 80%;cursor:pointer">
					Skip
				</a>
			</div>
	</div>
</div>
<div id="internationalShippingDisclosures" style="display:none"> 
<h2 class="h2b"><p style="float:left;margin:0;">International Shipping - Disclosures</p><a href="javascript:void(0);" class="no-decoration-link right" onClick="closeDisclosures()">Close</a></h2>
<div class="icon-line"></div>
<p>All shipments sent to countries outside India are international shipments and may be subject to import duties or fees levied by the country of import. All these duties and fees are the liability of the importer or recipient and Myntra is not liable to pay any such charge. Please contact the local customs office of the importing country for more information.</p>
</div>
<div id="fbRegisterPageLoadingIndicator" style="display:none"> 
<h2 style="margin-top : 10px; padding-bottom: 6px;">Welcome <span id="fbRegisterPageLoadingIndicator-Name"></span></h2>
<div class="icon-line"></div>
<br />
<h4 style="text-align:center">Please wait while we log you in for an amazing shopping experience.</h4>
<img src="{$secure_cdn_base}/skin1/myntra_images/loading-graphic.gif" alt="loading...">
</div>
<div id="learnMoreTermsAndConditions" style="display:none"> 
<h2 class="h2b"><p style="float:left;margin:0;font-weight:bold;">Rs 1000 Gift Voucher</p><a href="javascript:void(0);" class="no-decoration-link" style="float:right;" onClick="hideLearnMoreDetails()">Close</a></h2>
<div class="icon-line"></div>
<strong>Eligibility:</strong>
<ul>
	<li>Any purchase on Myntra paid for via an online payment method would receive an assured Rs 1000 Gift Voucher.</li>
	<li>This is not applicable for any purchase paid via Cash on Delivery payment method.</li>
</ul>

<strong>Instructions:</strong>
<ul>
	<li>You need to be logged in on Myntra.com to redeem this coupon.</li>
	<li>Please make sure that you enter the voucher code before making the payment.</li>
</ul>

<strong>Terms & Conditions:</strong>
<ul>
	<li>The voucher can be redeemed against a minimum purchase value of Rs. 2500.</li>
	<li>The voucher is valid on purchases made through any payment method.</li>
	<li>The voucher would be valid up to 3 days from date of issue.</li>
	<li>The voucher is non-transferable and cannot be used by any User ID other than the one it was issued to.</li>
	<li>The voucher can be redeemed only once.</li>
</ul>
<!-- Advertiser 'GROUP M MEDIA INDIA PVT. LTD_MYNTRA DESIGN PVT LTD.',  Include user in segment 'Myntra_site retargeting pixel' - DO NOT MODIFY THIS PIXEL IN ANY WAY -->
<img src="http://ad.yieldmanager.com/pixel?id=1290412&t=2" width="1" height="1" />
<!-- End of segment tag -->
</div>
<!-- @Footer-->