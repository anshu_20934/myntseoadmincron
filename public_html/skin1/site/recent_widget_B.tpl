            {assign var=wi value=0}
		    <div class="category  clearfix mb10" style="clear:both;">
    		   <h4 class="h4_without_border" style="position:relative;">Recently viewed {if $recent_viewed_count < 5}<span class="rec-title">Recommended</span>{/if}</h4>
           		  <div class="scrollable-box clearfix last">
           		     	<a class="prevPage browse left"></a>
              	 		<div class="scrollable {if ($ga_category eq "hp_strip" && $cashback_displayOnHomePage neq '1') || ($ga_category eq "pdp_strip" && $cashback_displayOnPDPPage neq '1') || ( $ga_category eq "lp_strip" && $cashback_displayOnSearchPage neq '1')}without_cb_rescroll{else}rescroll{/if}">
       						<ul class="items items_{$widget.nameclean}">
    						{assign var=i value=1}
    						{foreach name=widgetdataloop item=widgetdata from=$widget.data}
    						{if $widgetdata.product neq '' && $widgetdata.product neq ' '}
    						{if $recent_viewed_count gt 4}
                            <li class="item re-itemsizes recent-viewed-size-loader" data-id="prod_{$widgetdata.id}">
                            {if $quicklookenabled}
								<span class="quick-look action-btn" data-widget="recent_widget" data-styleid="{$widgetdata.styleid}" data-href="{$widgetdata.landingpageurl}" href="javascript:void(0)"><span class="quick-look-icon"></span>Quick View</span>
							{/if}
							<div class="style-details" style="display:none"></div>
                             <div class="new-label"> </div>
                            <div class="item-box {if ($ga_category eq "hp_strip" && $cashback_displayOnHomePage neq '1') || ($ga_category eq "pdp_strip" && $cashback_displayOnPDPPage neq '1') || ( $ga_category eq "lp_strip" && $cashback_displayOnSearchPage neq '1')}without_cb_reitem-box{else}reitem-box{/if}">
                                      <a href="{$http_location}/{$widgetdata.landingpageurl}" class="loaded" onclick="_gaq.push(['_trackEvent', 'pdp_click', Myntra.Data.pageName, 'recent_widget']);">
                                          <img id="im_product_img_{$widget.nameclean}_{$widgetdata.styleid}" src="{$cdn_base}/skin1/images/loader_180x240.gif" lazy-src="{$widgetdata.search_image}" class="lazy" height="240" width="180" alt="{$widgetdata.product}" />
                                      </a>
                                <div class="item-desc">
                                	<span class="name">
                                    	<a href="{$http_location}/{$widgetdata.landingpageurl}" onclick="_gaq.push(['_trackEvent','{$ga_category}','{$ga_action}', 'slot_{$i}_viewed']);">{$widgetdata.product|truncate:40}</a>
                                    </span>
                                    {if ($ga_category eq "hp_strip" && $cashback_displayOnHomePage eq '1') || ($ga_category eq "pdp_strip" && $cashback_displayOnPDPPage eq '1') || ($ga_category eq "lp_strip" && $cashback_displayOnSearchPage eq '1') }
                                    	{if $widgetdata.discount > 0}
                                    		<div class="op">MRP <span class="strike">{$rupeesymbol}{$widgetdata.price|number_format:0:".":","}</span> <span style="color:#000"> &nbsp; {$rupeesymbol}{math equation="x-y" x=$widgetdata.price y=$widgetdata.discounted_price assign="discount_amount"}{$discount_amount|round|number_format:0:".":","}</span></div>
                                    		<div class="dp"><span style="font-weight:normal">{$rupeesymbol}</span>{$widgetdata.discounted_price|number_format:0:".":","}</div>
		                                    {eval var=$widgetdata.discount_label}
		                                {elseif $widgetdata.discount_label}
                                                <div class="dp"><span style="font-weight:normal">{$rupeesymbol}</span>{$widgetdata.price|round|number_format:0:".":","}</div>
                                               {eval var=$widgetdata.discount_label}
		                            	{else}
		                              	    {if $cashback_gateway_status eq 'on' && $widgetdata.showcashback eq 'true'}
		                                        <div class="op">Effective Price {$rupeesymbol}{math equation="x-y" x=$widgetdata.price y=$widgetdata.cashback assign="effectiveprice"}{$effectiveprice|round|number_format:0:".":","}</div>
		                                        <div class="dpv"><div class="dp"><span style="font-weight:normal">{$rupeesymbol}</span>{$widgetdata.price|round|number_format:0:".":","}</div>
		                                        <span class="cbp">with <b>10%</b></span><span class="cb">Cashback</span>
		                                        </div>
                                    		{/if}
                            			{/if}
                                    {else}
                                    	{if $widgetdata.discount > 0}
                                    			<div class="dp left"><span style="font-weight:normal">{$rupeesymbol}</span>{$widgetdata.discounted_price|number_format:0:".":","}</div> <div class="op"><span class="strike" style="float: left;">{$widgetdata.price|number_format:0:".":","}</span></div>
                                    			{eval var=$widgetdata.discount_label}
                                		{else}
                                                <div class="dp"><span style="font-weight:normal">{$rupeesymbol}</span>{$widgetdata.price|round|number_format:0:".":","}</div>
                                                {eval var=$widgetdata.discount_label}
                                		{/if}

                                    {/if}
                                </div>

                                <form method="post"  action="mkretrievedataforcart.php?pagetype=productdetail&lead=recently_viewed" id="form-{$widgetdata.styleid}" class="re-form-block"
                                        onsubmit="_gaq.push(['_trackEvent', 'buy_now', Myntra.Data.pageName, 'recent_widget']);">
                                	<input type="hidden" name="_token" value="{$USER_TOKEN}" />
                                    <input type="hidden" name="productStyleId" value="{$widgetdata.styleid}">
                                    <input type="hidden" name="quantity" value="1">
                                    <input type="hidden" name="sizename[]" value="" class="sizename">
                                    <input type="hidden" name="sizequantity[]" value="1">
                                    <input type="hidden" name="productSKUID" class="productSKUID" value="">
                                    <select class="widget-size-select size-select" id="style-{$widgetdata.styleid}">
                                        <option value="">select size</option>
                                    </select>

                                     <button class="orange-bg myntrabtn re-btn corners span-4 last add-to-cart-button" style="clear:both;">Buy Now <span style="font-size:15px;">&raquo;</span></button>
                                </form>
                            </div>
                            </li>
                            {else}
                                {if $i<=5}
                                    <li class="item re-itemsizes {if $i<= $recent_viewed_count}recent-viewed-size-loader{/if}" data-id="prod_{$widgetdata.id}">
                                    {if $quicklookenabled}
										<span class="quick-look action-btn" data-widget="recent_widget" data-styleid="{$widgetdata.styleid}" data-href="{$widgetdata.landingpageurl}"><span class="quick-look-icon"></span>Quick View</span>
									{/if}
									<div class="style-details" style="display:none"></div>
                            		<div class="item-box {if ($ga_category eq "hp_strip" && $cashback_displayOnHomePage neq '1') || ($ga_category eq "pdp_strip" && $cashback_displayOnPDPPage neq '1') || ( $ga_category eq "lp_strip" && $cashback_displayOnSearchPage neq '1')}without_cb_reitem-box{else}reitem-box{/if}">
                                                  <a href="{$http_location}/{$widgetdata.landingpageurl}" onclick="_gaq.push(['_trackEvent', 'pdp_click', Myntra.Data.pageName, 'recent_widget']);" class="loaded" onclick="_gaq.push(['_trackEvent','{$ga_category}','{$ga_action}', 'slot_{$i}_viewed']);">
                                                      <img id="im_product_img_{$widget.nameclean}_{$widgetdata.styleid}" src="{$cdn_base}/skin1/images/loader_180x240.gif" lazy-src="{$widgetdata.search_image}" class="lazy" height="240" width="180" alt="{$widgetdata.product}" />
                                                  </a>
                                            <div class="item-desc item-details">
                                                  <span class="name">
                                                      <a href="{$http_location}/{$widgetdata.landingpageurl}" onclick="_gaq.push(['_trackEvent','{$ga_category}','{$ga_action}', 'slot_{$i}_viewed']);">{$widgetdata.product|truncate:40}</a>
                                                  </span>
                                    {if ($ga_category eq "hp_strip" && $cashback_displayOnHomePage eq '1') || ($ga_category eq "pdp_strip" && $cashback_displayOnPDPPage eq '1') || ($ga_category eq "lp_strip" && $cashback_displayOnSearchPage eq '1') }
                                    	{if $widgetdata.discount > 0}
                                    			<div class="op">MRP <span class="strike">{$rupeesymbol}{$widgetdata.price|number_format:0:".":","}</span> <span style="color:#000"> &nbsp; {$rupeesymbol}{math equation="x-y" x=$widgetdata.price y=$widgetdata.discounted_price assign="discount_amount"}{$discount_amount|round|number_format:0:".":","} </span></div>
                                    			<div class="dp"><span style="font-weight:normal">{$rupeesymbol}</span>{$widgetdata.discounted_price|number_format:0:".":","}</div>
		                                    	{eval var=$widgetdata.discount_label}
		                                {elseif $widgetdata.discount_label}
                                                <div class="dp"><span style="font-weight:normal">{$rupeesymbol}</span>{$widgetdata.price|round|number_format:0:".":","}</div>
                                                {eval var=$widgetdata.discount_label}
		                            	{else}
		                              	    {if $cashback_gateway_status eq 'on' && $widgetdata.showcashback eq 'true'}
		                                        <div class="op">Effective Price {$rupeesymbol}{math equation="x-y" x=$widgetdata.price y=$widgetdata.cashback assign="effectiveprice"}{$effectiveprice|round|number_format:0:".":","}</div>
		                                        <div class="dpv"><div class="dp"><span style="font-weight:normal">{$rupeesymbol}</span>{$widgetdata.price|round|number_format:0:".":","}</div>
		                                        <span class="cbp">with <b>10%</b></span><span class="cb">Cashback</span>
		                                        </div>
                                    		{/if}
                            			{/if}
                                    {else}
                                    	{if $widgetdata.discount > 0}
                                    			<div class="dp left"><span style="font-weight:normal">{$rupeesymbol}</span>{$widgetdata.discounted_price|number_format:0:".":","}</div> <div class="op"><span class="strike" style="float:left;">{$widgetdata.price|number_format:0:".":","}</span></div>
                                    			{eval var=$widgetdata.discount_label}
                                		{else}
                                                <div class="dp"><span style="font-weight:normal">{$rupeesymbol}</span>{$widgetdata.price|round|number_format:0:".":","}</div>
                                               {eval var=$widgetdata.discount_label}
                                		{/if}

                                    {/if}

                                              </div>
                                            {if $i<= $recent_viewed_count}
                                             {assign var=wi value=$wi+202}
                                            <form method="post"  action="mkretrievedataforcart.php?pagetype=productdetail&lead=recently_viewed" id="form-{$widgetdata.styleid}" class="re-form-block"
                                                    onsubmit="_gaq.push(['_trackEvent', 'buy_now', Myntra.Data.pageName, 'recent_widget']);">
                                                <input type="hidden" name="_token" value="{$USER_TOKEN}" />
                                                <input type="hidden" name="productStyleId" value="{$widgetdata.styleid}">
                                                <input type="hidden" name="quantity" value="1">
                                                <input type="hidden" name="sizename[]" value="" class="sizename">
                                                <input type="hidden" name="sizequantity[]" value="1">
                                                <input type="hidden" name="productSKUID" class="productSKUID" value="">
                                                <select class="widget-size-select size-select" id="style-{$widgetdata.styleid}">
                                                    <option value="">select size</option>
                                                </select>

                                                <button class="orange-bg myntrabtn re-btn corners span-4 last add-to-cart-button">Buy Now <span style="font-size:15px;">&raquo;</span></button>
                                            </form>
                                            {else}

                                            <div class="vdetails">
                                                <span style="visibility:hidden;" class="availability-size">Sizes: {if $widgetdata.sizes neq '' && $widgetdata.sizes neq ' '}{$widgetdata.sizes|replace:",":", "}{else}SOLD OUT{/if}</span>
                                                <a href="{$http_location}/{$widgetdata.landingpageurl}" class="orange-bg myntrabtn re-btn corners span-4 last">View Details</a>
                                            </div>
                                            {/if}
                                        </div>
                                        </li>
                                {/if}
                            {/if}
    					{/if}

    						{assign var=i value=$i+1}
    						{/foreach}
    						</ul>
    					</div>
    					<a id='{$widget.nameclean}' class="nextPage browse right"></span></a>
    			 </div>
    		  	 <div class="clear"><hr /></div>
    		</div>

    		<style>.rec-title{literal}{{/literal}left:{$wi}px;position:absolute;*line-height:15px{literal}}{/literal}</style>
