     <div class="clearfix p10" style="clear:both">
               <h2 class="h4" id="popular-title">Popular</h2>
           <ul class="tags">
               {foreach name=tagtype item=fixedorDynamic from=$mostPopularWidget}
                       {foreach name=uniquetag item=tag from=$fixedorDynamic}
                       <li><a href="{$http_location}/{$tag.link_url}">{$tag.link_name}</a></li>
                   {/foreach}
               {/foreach}
           </ul>
       </div>

