<div class="header clearfix">
    <!-- Top Strip -->
    <div class="span-9 corners myntra-logo-simple left"><a href="{$http_location}" title="Online Shopping store for Branded Shoes, Clothing, Accessories & Fashion wear in India."></a></div>
    <div class="left secure-payment-title">Secure Payment</div>
    <div class="login-info right secure-login-info">
    	<a class="no-decoration-link left" href="{$http_location}/mkmycart.php">My Bag&nbsp;</a> | 
    	{if $userfirstname neq ""}
	    	<span id="logged_in_user_name">{$userfirstname}</span>
	    {else}
	    	<span id="logged_in_user_name">{$login|truncate}</span>
	    {/if}
	    (<a id="logout_from_account" class="no-decoration-link">Logout</a>)
	</div>
	<form id="toplogoutform" method="post" action="{$https_location}/include/login.php">
		<fieldset class="field-hide">
           	<legend>
				<input type="hidden" value="logout" name="mode">
		    </legend>
		</fieldset>
	</form>
</div>
<div class="icon-line-simple"></div>
