{foreach from=$jsFiles item=href}
    <script type="text/javascript" src="{$href}"></script>
{/foreach}
{if $analyticsStatus}
	{include file="site/analytics_data_collection.tpl"}
{/if}
{literal}
<script type="text/javascript">
$("#logout_from_account").click(function(){
   	$("#toplogoutform").trigger("submit");    
});
	
function tracking(){
    // Omniture tracking call
    if(typeof(s_analytics) != "undefined"){
        s_analytics.t();
    }
    // GA tracking page load call
	var ga = document.createElement('script');
	ga.type = 'text/javascript';
	ga.async = true;
	ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	var s = document.getElementsByTagName('script')[0];
	s.parentNode.insertBefore(ga, s);
	// Mediamind page load call
	var mm = document.createElement('script');
	mm.type = 'text/javascript';
	mm.async = true;
	var mmsrc = "";
	if('https:' == document.location.protocol){
		mmsrc = "HTTPS://bs.serving-sys.com/BurstingPipe/ActivityServer.bs?cn=as&ActivityID=112557&rnd=" + ebRand;
	}
	else{
		mmsrc = "HTTP://bs.serving-sys.com/BurstingPipe/ActivityServer.bs?cn=as&ActivityID=112557&rnd=" + ebRand;
	}
	mm.src = mmsrc;
	var s = document.getElementsByTagName('script')[0];
	s.parentNode.insertBefore(mm, s);

	//User Tracking Pixel Code for tyroo
    //trackTyrooAdNetwork();
    if(enableMyThings){
 	   $.browser.chrome = /chrome/.test(navigator.userAgent.toLowerCase());
	   if(($.browser.chrome) || (($.browser.mozilla) && (parseInt($.browser.version,10)>=2)) || (($.browser.msie)&&(parseInt($.browser.version,10)>=6)))
	   {_triggerMyThings();}
     }
	   
}
function _mt_ready(){
	   if (typeof(MyThings) != "undefined") {
	       MyThings.Track({
	           EventType: MyThings.Event.Visit,
	               Action: "300"
	       });
	   }
	}
	var mtHost = (("https:" == document.location.protocol) ? "https" : "http") + "://rainbow-in.mythings.com";
	var mtAdvertiserToken = "1428-100-in";
	var nurl=mtHost+'/c.aspx?atok='+mtAdvertiserToken;
function _triggerMyThings()
{
		$.getScript(nurl);
}


/*function trackTyrooAdNetwork(){
    imagePixel = $('<img src="https://ads1.tyroo.com/utrack/3.0/1067/0/0/0/BeaconId=10520;rettype=img;subnid=1;" width="1" height="1" style="display:none;">');

    if($.browser.msie){
        $(document.body).append(imagePixel);
    } else {
        $('body').append(imagePixel);
    }
}*/

</script>
{/literal}

<script type="text/javascript">
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', '{$ga_acc}']);
    _gaq.push(['_setDomainName', '{$cookiedomain}']);
    
{if $lt_new_session eq "true"}
{/if}
	{if $_MAB_GA_calls}
	{$_MAB_GA_calls}
	{/if}
    _gaq.push(['_trackPageview']);
    _gaq.push(['_trackPageLoadTime']);
    
    //Mediamind specific code starts
    var ebRand = Math.random()+'';
    ebRand = ebRand * 1000000;

    $(window).load(function () {ldelim}
    	tracking();
	{rdelim});
    // Mediamind specific code ends -->
</script>
{if $newrelic_footer}
  {$newrelic_footer}
{/if}
