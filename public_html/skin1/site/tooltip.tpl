<div class="tooltip-container">
	<div class="tooltip-left"><span></span></div>
	<div class="blue-bg corners tooltip">
		<div class="main-left p5 left">
			<div class="image-wrapper corners white-bg panzoom">
			<a id="test"><img src="{$cdn_base}/skin1/images/spacer.gif" alt="No Image"></a></div>
		</div>
		<div class="main-right right">
			<div class="rating mt10 hide">
				<div class="star star-selected"></div>
				<div class="star star-selected"></div>
				<div class="star star-selected"></div>
				<div class="star star-selected"></div>
				<div class="star star-unselected"></div>
			</div>
			<div class="availability">
				<div class="underline">Availability</div>
				<div class="sizes">S,L,XL</div>
			</div>
			<div class="description"></div>
			<div class="features">
				<div class="cod-enabled-text">Cash On delivery</div>
				<div>Free Home Delivery</div>
			</div>
		</div>
		<span class="item-description span-13 pl5">
			<span class="name">Adidas Red shoes</span>
		</span>
	</div>
	<div class="tooltip-right"><span></span></div>
</div>