<div style="border:0px solid gray;width:465px; border: 1px solid gray; margin-top: 8px;">
<div style="clear: both;"></div>

	<div style="width:472px;height:40px; margin-top: 5px;" align="center">
		<div style="padding-top:10px;padding-left:20px;width:160px;height:25px;float:left;font-family:Arial,Helvetica,sans-serif;font-size:18px;border-right:1px solid gray">
			 Myntra.com
		</div>
		<div style="padding-left:20px;width:230px;height:25px;float:left;font-family:Arial,Helvetica,sans-serif;font-size:14px" align="left">
			 The best place to shop for the latest in sports and casual wear!
		</div>
	</div>
	<table background="http://myntramailer.s3.amazonaws.com/november2011/1321512304bg112.jpg" border="0" width="464" height="240" cellpadding="0" cellspacing="0" style="margin-bottom: 0px">
	<tr>
		<td>
			<div style="width:450px; height: 220px;">
				<div style="margin:0px; padding:0px;line-height:0px; width: 230px; float:left; height:200px;">
				
					<img src="http://myntramailer.s3.amazonaws.com/november2011/1321512304invitation-landing-page-banner.jpg" width="228" height="214" alt="Mynt Club at Myntra.com" title="Mynt Club at Myntra.com" style="text-decoration:none;border:none;font-family:Arial,Helvetica,sans-serif;color:#666;font-size:12px;"/>
					
				</div>
				<div style="width: 220px; height:170px; float: right; padding-top: 15px;">
					<p style="font-family:Arial narrow,Helvetica,sans-serif;font-size:16px; color: #917f7d; font-weight: bold; line-height: 0px;">
						
						BECOME A MEMBER NOW!
					</p>
					<p style="font-family:Arial narrow,Helvetica,sans-serif;font-size:26px; color: #a40067; font-weight: bold; margin-top:25px; ">
						<a href="http://www.myntra.com/mrp_landing.php" style="text-decoration:none; color: #a40067;" target="_blank">
						EARN RS. {$mrp_nonfbCouponValue}*<br>
						AS SOON AS YOU<br>
						 SIGN UP!</a>
					</p>
				</div>
			</div>
		</td>
	</tr>
	</table>
	<table background="http://myntramailer.s3.amazonaws.com/november2011/13215123041315500018table-bg.jpg" height="155" width="450" cellspacing="0" cellpadding="0" border="0" align="center">
	<tbody>
	<tr>
		<td valign="top">
			<div style="width:220px; float: left; height: 148px;">
				<div style="padding-left:5px;font-family:Arial narrow,Helvetica,sans-serif;font-size:20px; color: #ff4c1b; font-weight: bold; line-height:35px; ">
					 WHY MYNTRA?
				</div>
				<div style="margin-top:5px;line-height:15px; font-family:Arial,Helvetica,sans-serif;font-size:11px; color: #000; ">
					 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>&#8226; Largest in-season catalog</strong><br>
					 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;of fashion and lifestyle wear.<br>
					<br>
					 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>&#8226; More than 70 brands of</strong><br>
					 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;footwear, apparel & accessories<br>
					<br>
					 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>&#8226;</strong> Avail<strong> 10% Cashback*</strong><br>
				</div>
			</div>
			<div style="width:210px; float: right; height: 148px;">
				<div style="width:170px; height:35px;float:right;padding-left:10px;font-family:Arial,Helvetica,sans-serif;font-size:12px; color: #000; font-weight: bold; line-height:35px; ">
					 SAFE & SECURE PAYMENTS
				</div>
				<div style="padding-top: 15px;float: left;width: 220px;margin-top:5px;line-height:15px; font-family:Arial,Helvetica,sans-serif;font-size:11px; color: #000; ">
					<strong>&#8226; Free shipping & Payment on Delivery*</strong><br>
					<br>
					<strong>&#8226;</strong><strong> 30 Days Return Policy</strong><br>
					<br>
					<strong>&#8226;</strong> 24x7 <strong>Customer Support</strong><br>
					<br>
				</div>
			</div>
		</td>
	</tr>
	</tbody>
	</table>
	<div style="width: 464px; background-color:#ebe7e6; height: 80px; margin-top: 10px;" align="center">
		<div style="padding-top:5px;">
			<span style="font-family:Arial narrow,Helvetica,sans-serif;font-size:18px; color: #a40067; font-weight: bold; ">
			REFER AND EARN MORE.</span>
		</div>
		<div style="padding-top:5px;font-family:Arial narrow,Helvetica,sans-serif;font-size:16px; color: #917f7d; font-weight: bold; line-height: 20px;">
			
			Get <strong style="color: #000;">Rs. {$mrp_refRegCouponValue}*</strong> for every friend who registers<br>
			 Get <strong style="color: #000;">Rs. {$mrp_refPurCouponValue}*</strong> on first purchase by every friend
		</div>
		
	</div>
	<div style="margin-top:0px; width:464px;line-height:0px;font-family:Arial,Helvetica,sans-serif;color:#322725;font-size:12px; padding-bottom: 5px;">
		<img src="http://myntramailer.s3.amazonaws.com/july2011/invitation-mailer-brands-banner.jpg" width="464" height="50" alt="Brands at Myntra.com: Nike, Adidas, Puma, Reebok, Wrangler, Lee and many more.." title="Brands at Myntra.com: Nike, Adidas, Puma, Reebok, Wrangler, Lee and many more.." style="text-decoration:none;border:none;font-family:Arial,Helvetica,sans-serif;color:#666;font-size:12px;"/>
	</div>
</div>