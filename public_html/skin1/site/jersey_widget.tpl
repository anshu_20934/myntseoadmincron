<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
	<title>Jersey Widget</title>
	{if $orgId}
	<link href="{$http_location}/{$brandshop_css_path}" rel="stylesheet" type="text/css">  	
  {else}
	<link href="{$http_location}/{$css_path}" rel="stylesheet" type="text/css">
  {/if}
  
  <base href="{$baseurl}">
  <script type="text/javascript">
    var http_loc = '{$http_location}';
    var cdn_base = '{$cdn_base}';
    var facebook_app_id = '{$facebook_app_id}';
    var lists_image_srcs = new Object() ;
  </script>
  <style type="text/css">
  
  </style>
</head>
<body class="{$_MAB_css_classes}">
	{include file="site/jersey_widget_content.tpl"}
</body>
</html>