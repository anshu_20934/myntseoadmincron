<div class="header clearfix">
    <!-- Top Strip -->
    <div class="header-messages black-bg clearfix">
	    <div class="header-messages-content clearfix">
	    	{*{$topCpsMessage}*}
	    	{if $customerSupportTime && $customerSupportCall}
	    		<span class="left customerCall">{$customerSupportTime} on <strong>{$customerSupportCall}</strong></span>
	    	{/if}
	    	{if $topCpsMessage}
	    		<span class="right cps">{$topCpsMessage}</span>
	    	{/if}	    	
	    </div>
    </div>
    <div class="header-logo-signup-block clearfix">
        <div class="myntra-logo left">
        	<a href="{$http_location}" title="Online Shopping store for Branded Shoes, Clothing, Accessories & Fashion wear in India."></a>
        </div>
        <div class="right search-box">
            <form action="{$http_location}/" method="post" id="menu_search_form" onsubmit="return menuSearchFormSubmit();">
                <input type="text" class="search toggle-text blurred" name="query" value="{$searchBoxText}" id="search_query" tabindex="10">
                <input type="button" id="menu_search" class="search-btn" tabindex="11">
            </form>
        </div>
{*Login/signup*}
{* USER DETAILS*}
        <div class="right userstript last {if $login}hide{else}show{/if} clear">
            <div class="left userblock">
                <div style="text-align:right;">Welcome!</div>
                <button class="link-btn btn-login right" style="padding:0px;margin:0; *border-top:1px solid #FFFFFF;">Login / Sign Up</button></div>
                {*<a href="javascript:void(0);" class="sign" onclick="_gaq.push(['_trackEvent', 'header', 'signin', 'click']);">Login / Signup</a>*}
            <div class="cart-box right" {if !$totalQuantity}style="*width:80px;"{/if}>
                <span class="cart-details left">My bag<br>{if $totalQuantity}<a href="mkmycart.php?at=c&pagetype=productdetail" style="color:#D20000; font-weight:bold;">Rs. {$totalAmountCart|round|number_format:0:".":","}</a> <span style="color:#666">(<a href="mkmycart.php?at=c&amp;pagetype=productdetail">{$totalQuantity}</a>)</span>{else}<b style="color:#666;font-weight:normal;">(Empty)</b>{/if}</span>
                <a href="mkmycart.php?at=c&amp;pagetype=productdetail" style="*width:32px;"><span class="cartbag right"></span></a>
            </div>
        </div>
           
            <div class="post-login-exp {if $login}show{else}hide{/if}">

                <div class="last post-login-details right">
                    <div class="left clearfix" style="margin-right:25px;text-align:right;*width:225px;">
                        <div class="last post-login-box clearfix">

                            {if $userfirstname neq ""}
                            Welcome, <span id="logged_in_user_name">{$userfirstname|truncate:15}</span>! <span> <a id="logout_from_account" class="no-decoration-link post-login-links logout-link">Not you?</a></span>

                            {else}
                            Welcome, <span id="logged_in_user_name">{$login|truncate:15}</span>! <span> <a id="logout_from_account" class="no-decoration-link post-login-links logout-link">Not you?</a></span>
                            {/if}

                        </div>
                        <div style="text-align:right;">
                            <a href="{$http_location}/mymyntra.php">My Myntra</a> <span style="color:#666;padding:0 3px;">|</span> <a href="{$http_location}/mymyntra.php?view=mymyntcredits" onclick="_gaq.push(['_trackEvent', 'header', 'view_mynt_credits','view_mynt_credits']);">My Mynt Credits</a>
                            <span id="headerMessage">{if $login}{$mrp_header_message}{/if}</span>
                        </div>
                    </div>
                    {*Cart block*}
                     <div class="cart-box right">
                     <a href="mkmycart.php?at=c&pagetype=productdetail"><span class="cartbag right"></span></a>
                    {* <b style="color:#D20000" id="totalCartAmount">{$totalAmountCart}</b> <span id="cartquantityanchor" style="color:#006699">{$totalQuantity}</span>*}
                        <span class="cart-details right">My bag<br>
                            <a href="mkmycart.php?at=c&pagetype=productdetail" id="totalCartAmount" style="color:#D20000;font-weight:bold;">
                            {if $totalQuantity}
                                Rs. {$totalAmountCart|round|number_format:0:".":","}
                            {/if}
                            </a>
                            <span id="cartquantityanchor" style="color:#666;">
                                {if $totalQuantity}
                                    (<a href="mkmycart.php?at=c&pagetype=productdetail" style="color:#0084BA">{$totalQuantity}</a>)
                                {else}
                                    (Empty)
                                {/if}
                            </span>
                        </span>
                    </div>
                    {*Cart block End*}
                </div>
                <form id="toplogoutform" method="post" action="{$http_location}/include/login.php">
                <fieldset class="field-hide">
                    <legend>
                    <input type="hidden" value="logout" name="mode">
                    </legend>
                </fieldset>
                </form>
            </div>
        </div>
        {*Login/signup End*}

    </div>


	    <div class="header-menu-bar-content  clearfix">
	    	{$top_nav}
	    	{*{include file="site/megamenu.tpl"}*}
	    </div>

{if $SEMpromo eq 1}
	<div class="header-notice mt10 clearfix" style="height:29px;line-height:29px;font-weight:normal;background-image:none; background-color: #f2f2f2; border-top: 1px solid #b3b3b3; padding: 2px 6px;  white-space: nowrap;"><a href="{$semPromoUrl}" class="no-decoration-link" style="display:block;color: #808080;" onclick="_gaq.push(['_trackEvent', 'header_promo', 'REbanner_click']);">{$semPromoHeaderText}&nbsp;&nbsp;<span class="promo-learn-more" style="text-decoration:underline;color:#808080;font-size:70%;">View Now</span></a></div>
{elseif $promoHeaderEnabled}
    {if $login && $promoHeaderLoggedinText}
        <div class="header-notice mt10 clearfix" style="font-weight:normal;background-image:none;background-color:#F2F2F2;border-top:1px solid #b3b3b3;overflow:hidden;margin:0;height:29px;color:#808080;padding-top:3px;">{$promoHeaderLoggedinText}</div>
    {else}
	    <div class="header-notice mt10 clearfix" style="font-weight:normal;background-image:none;background-color:#F2F2F2;border-top:1px solid #b3b3b3;overflow:hidden;margin:0;height:29px;color:#808080;padding-top:3px;">{$promoHeaderText}</div>
	{/if}
{else}
	<!--a href="{$http_location}/footwear" class="header-notice mt10" onclick="showLearnMoreDetails()"></a-->
{/if}
{if $abLoginPopup eq 'splash'}
<div id="lb-login" class="lightbox lb-login">
    {include file="site/mod_login.tpl"}
</div>
{/if}
<div class="divider">&nbsp;</div>
{*<span class="cbtip hide" style="display:none"><b style="display: block;">Cashback</b> On purchasing this product, you will receive a cashback credit upto the amount indicated here. Cashback is calculated on item value excluding any coupon discounts. You can redeem this on any of your subsequent purchases at Myntra.</span>*}