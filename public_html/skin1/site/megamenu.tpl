<div id="megamenu" class="clearfix">
	<ul id="topnav" class="left">
		{foreach name=topNavigationMenuLoop key=menulink item=menudata from=$topNavigationMenuData}
			{if $menulink eq $megaMenuMensTitle || $menulink eq $megaMenuWomensTitle}
			<li class="f1" class="box-shadow">
				{* Top level links - For him, For her, Kids, Brands *}
				<a href="{if $menulink eq $megaMenuMensTitle}/men{else}/women{/if}">{$menulink}</a>
				{* FOR HIM SUB MENU*}
				<ul class="subnav navbox box-shadow mmbg hide">
					<li class="f2title"><b>SHOP BY CATEGORY</b></li>
					{foreach name=topNavigationSubMenuLoop key=displaycatname item=displaycatdata from=$menudata}
					{if $displaycatname|lower|replace:' ':'-' eq $saleClearanceURL}
						{assign var=linksuffix value="#sortby=DISCOUNT"}
					{else}
						{assign var=linksuffix value=""}
					{/if}
					<li class="f2{if $smarty.foreach.articletypeloop.first} active{/if}">
						{* Display catgories rendered here *}
						<a href="/{if $menulink eq $megaMenuMensTitle}{myntralink global_attr_gender="men" display_category_facet="$displaycatname" is_landingpage="y"}{else}{myntralink global_attr_gender="women" display_category_facet="$displaycatname" is_landingpage="y"}{/if}">{$displaycatname|capitalize}</a>
						{*SUB MENU CATEGORY LIST*}
						<ul class="menu_container hide">
							{* Display category sub menu rendered here - Styles, Brands and What's New Section *}
							{foreach name=topNavigationSubMenuLoop key=submenuname item=submenudata from=$displaycatdata}
								<li class="f3">
									<b>{$submenuname}</b>
									{* limit for only links including one all link *}
									{assign var=limit value=0}
									{foreach name=topNavigationSubLinksLoop key=sublinksname item=sublinksdata from=$submenudata}
										{if $limit lt 9}
											{if is_numeric($sublinksdata)}
												{if $menulink eq $megaMenuMensTitle}
													<a href="/{myntralink global_attr_gender="men" display_category_facet="$displaycatname" global_attr_article_type_facet="$sublinksname" is_landingpage="y"}{$linksuffix}">{$sublinksname|capitalize} <i>({$sublinksdata})</i></a>
												{elseif $menulink eq $megaMenuWomensTitle}
													<a href="/{myntralink global_attr_gender="women" display_category_facet="$displaycatname" global_attr_article_type_facet="$sublinksname" is_landingpage="y"}{$linksuffix}">{$sublinksname|capitalize} <i>({$sublinksdata})</i></a>
												{/if}
											{else}
												{*{if $smarty.foreach.topNavigationSubLinksLoop.last}
													<a class="lastlink" href="{$sublinksdata}{$linksuffix}">{$sublinksname|capitalize}</a>
												{else}*}
													<a href="{$sublinksdata}{$linksuffix}">{$sublinksname|capitalize}</a>
												{*{/if}*}
											{/if}
											{assign var=limit value=$limit+1}
										{/if}
									{/foreach}
									{* Incase of Styles section the last link is a link to All styles under that section *}
									{if $submenuname eq "STYLES"}
										{if $menulink eq $megaMenuMensTitle}
											<a class="lastlink" href="/{myntralink global_attr_gender="men" display_category_facet="$displaycatname" is_landingpage="y"}{$linksuffix}">All Men's {$displaycatname|capitalize}</a>
										{elseif $menulink eq $megaMenuWomensTitle}
											<a class="lastlink" href="/{myntralink global_attr_gender="women" display_category_facet="$displaycatname" is_landingpage="y"}{$linksuffix}">All Women's {$displaycatname|capitalize}</a>
										{/if}
									{elseif $submenuname eq "BRANDS"}
										{if $menulink eq $megaMenuMensTitle}
											<a class="lastlink" href="/{myntralink global_attr_gender="men" display_category_facet="$displaycatname" is_landingpage="y"}{$linksuffix}">More Brands</a>
										{elseif $menulink eq $megaMenuWomensTitle}
											<a class="lastlink" href="/{myntralink global_attr_gender="women" display_category_facet="$displaycatname" is_landingpage="y"}{$linksuffix}">More Brands</a>
										{/if}
									{elseif $submenuname eq "WHAT'S NEW"}
										{if $menulink eq $megaMenuMensTitle}
											<a class="lastlink" href="/{myntralink global_attr_gender="men" display_category_facet="$displaycatname" is_landingpage="y"}{$linksuffix}#!sortby=RECENCY">More New Arrivals</a>
										{elseif $menulink eq $megaMenuWomensTitle}
											<a class="lastlink" href="/{myntralink global_attr_gender="women" display_category_facet="$displaycatname" is_landingpage="y"}{$linksuffix}#!sortby=RECENCY">More New Arrivals</a>
										{/if}
									{/if}
								</li>
							{/foreach}
							{* Featured Product/Bestseller promotional section  *}
							<li class="f3 sublast right">
								{if $featuredProductsData[$menulink][$displaycatname].search_image neq ""}
				                <b class="ct">BESTSELLER</b>
				                <div class="img"><a href="{$http_location}/{$featuredProductsData[$menulink][$displaycatname].landingpageurl}"><img width="84" height="112" src="{$cdn_base}/skin1/images/loader_180x240.gif" lazy-src="{$featuredProductsData[$menulink][$displaycatname].search_image}"></a></div>
				                <div class="imgtxt"><a href="{$http_location}/{$featuredProductsData[$menulink][$displaycatname].landingpageurl}">{$featuredProductsData[$menulink][$displaycatname].stylename}</a></div>
				                {/if}
				            </li>
						</ul>
					</li>
					{/foreach}
				</ul>
			</li>
			{elseif $menulink eq $megaMenuKidsTitle}
			<li class="f1" class="box-shadow">
				<a href="/kids">{$menulink}</a>
				<ul class="subnav navbox box-shadow hide">
					<li>
						<ul class="menu_container_plain">
							{foreach name=kidsTabMenuLoop key=kidstabidx item=kidstabdata from=$menudata}
							<li class="f3 {if $smarty.foreach.kidsTabMenuLoop.last}last{/if}">
								<b>{$kidstabidx}</b>
								{* limit for only 9 links and one all link *}
								{assign var=limit value=0}
								{foreach name=topNavigationSubLinksLoop key=sublinksname item=sublinksdata from=$kidstabdata}
									{if $limit lt 9}
										{assign var=kidsurlprefix value='kids'}
										{if $kidstabidx eq 'FOR BOYS'}
											{assign var=kidsurlprefix value='boys'}
											{assign var=kidsgender value='boy'}
										{else if $kidstabidx eq 'FOR GIRLS'}
											{assign var=kidsurlprefix value='girls'}
											{assign var=kidsgender value='girl'}
										{/if}
										{if is_numeric($sublinksdata)}
											<a href="/{myntralink global_attr_gender="$kidsgender" display_category_facet="$sublinksname" is_landingpage="y"}">{$sublinksname|capitalize} <i>({$sublinksdata})</i></a>
										{else}
											<a href="{$sublinksdata}">{$sublinksname|capitalize}</a>
										{/if}
										{assign var=limit value=$limit+1}
									{/if}
								{/foreach}
								{if $kidstabidx eq "BRANDS"}
									<a class="lastlink" href="/kids">More Brands</a>
								{elseif $kidstabidx eq "WHAT'S NEW"}
									<a class="lastlink" href="/kids#!sortby=RECENCY">More New Arrivals</a>
								{/if}
							</li>
							{/foreach}
							<li class="f3 sublast right">
								{if $featuredProductsData[$menulink].search_image neq ""}
								<b class="ct">BESTSELLER</b>
							   	<div class="img"><a href="{$http_location}/{$featuredProductsData[$menulink].landingpageurl}"><img width="84" height="112" src="{$cdn_base}/skin1/images/loader_180x240.gif" lazy-src="{$featuredProductsData[$menulink].search_image}"></a></div>
				                <div class="imgtxt"><a href="{$http_location}/{$featuredProductsData[$menulink].landingpageurl}">{$featuredProductsData[$menulink].stylename}</a></div>
				                {/if}
							</li>
						</ul>
					</li>
				</ul>
			</li>
			{/if}
		{/foreach}
			<li class="f1" class="box-shadow">
				<a href="/brands">BRANDS</a>
				<ul class="subnav navbox box-shadow mmbg hide">
					<li>
						<ul class="menu_container_plain">
							<li class="f3">
								<b>BRANDS DIRECTORY</b>
								{foreach name=brandsdirloop key=brandsidx item=brandsdata from=$brands_data_directory}
									<a href="/brands#{$brandsdata.range|lower|replace:' ':''}">{$brandsdata.range} <i>({$brandsdata.count})</i></a>
								{/foreach}
							</li>
							<li class="fbrands">
								<b>FEATURED BRANDS</b>
								{* limit for only 12 featuredbrands *}
								{assign var=limit value=0}
								<div class="clearfix">
								{foreach name=fbrandsloop key=fbrandsidx item=fbrandsdata from=$featured_brands_data}
									{if $limit lt 12}
									{assign var=brand_f value=$fbrandsdata.attribute_value}
									<a href="/{myntralink brands_filter_facet="$brand_f" is_landingpage="y"}"><img width="84" height="49" src="{$cdn_base}/skin1/images/loader_24x12.gif" class="lazy" lazy-src="{$fbrandsdata.logo}"></a>
									{assign var=limit value=$limit+1}
									{/if}
								{/foreach}
								</div>
								<a class="lastlink" href="/brands">All Brands</a>
							</li>
							<li class="lbrands f3 sublast right">
								<b>LATEST BRANDS</b>
								{* limit for only 2 featuredbrands *}
								{assign var=limit value=0}
								{foreach name=lbrandsloop key=lbrandsidx item=lbrandsdata from=$latest_brands_data}
									{if $limit lt 2}
									{assign var=brand_l value=$lbrandsdata.attribute_value}
									<a href="/{myntralink brands_filter_facet="$brand_l" is_landingpage="y"}"><img lazy-src="{$lbrandsdata.logo}" class="lazy" width="84" height="49" src="{$cdn_base}/skin1/images/loader_24x12.gif"></a>
									{assign var=limit value=$limit+1}
									{/if}
								{/foreach}
							</li>
						</ul>
					</li>
				</ul>
			</li>
			<li class="f1 sale-highlighter" class="box-shadow">
				<a href="/{$saleClearanceURL}">SALE</a>
			</li>
	</ul>
	{if $abHeaderUi neq 'v3'}
	<div class="right search-box">
			<form action="{$http_location}/" method="post" id="menu_search_form" onsubmit="return menuSearchFormSubmit();">
	    	    <input type="text" class="search toggle-text blurred corners-tl-bl" name="query" value="{$searchBoxText}" id="search_query" tabindex="10">
	       		<input type="button" value="Go" id="menu_search" class="orange-bg corners-tr-br search-btn" tabindex="11">
	    	</form>
   	</div>
   	{/if}
</div>
