<div class="jersey_widget_main_container">
	<form id="nikeform" action="{$http_location}/modules/affiliates/reebok_ipl.php" method="POST" autocomplete="off" class="clearfix" target="_parent">
	<div class="widget-loading hide"></div>
    <input type="hidden" name="styleid" id="style_id" value="{$default_jersey_info.style_id}">
    <input type="hidden" name="styleprice" id="style_price" value="{$default_jersey_info.price}">
    <input type="hidden" name="style_discount_price" id="style_discount_price" value="{$default_jersey_info.discounted_price}">
    <input type="hidden" name="style_discount_value" id="style_discount_value" value="{$default_jersey_info.discount_value}">
    <input type="hidden" name="style_discount_label" id="style_discount_label" value="{$default_jersey_info.discount_label}">
    <input type="hidden" name="personalize" id="personalize" value="on">
    <div class="top_strip clearfix">
		              <div class="product-gallery-head left">
                            <div class="title left prepend">{$default_team_name}</div>
                            <div class="type-filter left">
                                <select id="tshirt-type" class="filter-select filter-tshirt-type" style="width:auto;">
                                    {section name=jersey loop=$jerseys}
                                              <option value="{$jerseys[jersey].style_id}">{$jerseys[jersey].type}</option>
                                      {/section}
                                </select>
                            </div>
                            

                            <span class="price">
                                <span class="currency">Rs</span>
                                <span {if $default_jersey_info.discount_value <= 0}class="per_product_price"{else}class="per_product_price strike" style="font-weight:normal"{/if}>{$default_jersey_info.price}</span>&nbsp;&nbsp;
                                <span class="discount_dynamic"{if $default_jersey_info.discount_value <= 0}style="display:none;"{/if}>
                                    <span class="currency">Rs</span>
                                    <span class="per_product_price">{$default_jersey_info.discounted_price}</span>
                                </span>
                            </span>

              				</div>
              				<div class="title right">Choose a jersey to buy</div>
					<div class="icon-line"></div>
    </div>
	<div class="product-gallery-container clearfix">
			<div class="product-gallery-content clearfix left">
				<div class="product-gallery span-19 left">
					<div class="product-image-tabs blue-bg corners left">
                        {section name=jersey loop=$jerseys}
						    <div>
						        <img class="product-image" src="{$jerseys[jersey].display_image}" alt="{$jerseys[jersey].name} - {$jerseys[jersey].type}" width="240" height="320">
						        <img class="customize-product-image" src="{$jerseys[jersey].image}" alt="{$jerseys[jersey].name} - {$jerseys[jersey].type}" width="240" height="320">
						        <select class="customize-product-image" id="select_list_{$jerseys[jersey].style_id}">{$jerseys[jersey].sizes}</select>
						    </div>
						{/section}
					</div>
					<div class="vertical-scroller left clearfix">
						<span class="browse-btn"><a class="prev up left"></a></span>
						<div class="vertical-scrollable left">
							<div class="vertical-tabs vertical-items left">
                                    {section name=jersey loop=$jerseys}
                                        <div><a id="tab_{$jerseys[jersey].style_id}" class="first" href="#" onclick="jersey_preview('{$jerseys[jersey].image}','{$jerseys[jersey].price}', '{$jerseys[jersey].style_id}', '{$jerseys[jersey].name_font_color}', '{$jerseys[jersey].number_image_path}', '{$jerseys[jersey].is_customizable}', '{$jerseys[jersey].discounted_price}', '{$jerseys[jersey].discount_value}', '{$jerseys[jersey].discount_label}')"><img class="corners" src="{$jerseys[jersey].thumb}" alt="{$jerseys[jersey].name} {$jerseys[jersey].type}" title="{$jerseys[jersey].name} {$jerseys[jersey].type}"></a></div>
                                    {/section}
						    </div>
						</div>
						<span class="browse-btn"><a class="next down left"></a></span>
					</div>
				</div>
				<div class="other-views clearfix span-9">
					<a class="no-decoration-link" onclick="javascript:loadFrontView()" href="javascript:void(0)">Front</a>
					<a class="no-decoration-link" onclick="javascript:loadBackView()" href="javascript:void(0)">Back</a>
				</div>
			</div>
			<div class="product-details left">
				<div class="personalize-details-block">
					<div class="product-details-heading"><span style="float:left;">Personalize your</span><span class="team-name" style="margin-left:5px;">{$default_team_name}</span> Jersey</div>
					<div class="product-details-block corners black-bg5 clearfix" >
						<div class="details">
							<div class="choose text">
								<label>Text</label>
								<input type="text" name="u_name" id="preview-name" value="Enter a name" class="gallery-block-input toggle-text" maxlength="8" tabindex="5">
							</div>
							<div class="choose number" >
								<label>Number</label>
								<input type="text" name="u_number" id="preview-number" value="Enter a number" class="gallery-block-input toggle-text" maxlength="2" tabindex="6">
							</div>
						</div>					
                    	<input type="button" value="Preview" class="product-preview corners">
						<div class="fb-make-my-profile-pic"><img src="{$cdn_base}/skin1/images/fb_app/profile-pic1.png" /></div>
					</div>
				</div>
				<div class="product-details-main clearfix">
					<div class="jersey-product-details-main-left left">
						<div class="choose size">
							<label>Size</label>
							<select name="size" id="size_list" class="filter-select filter-size">
                                   {$jerseys[0].sizes}
    						</select>
						</div>
						<div class="choose quantity">
							<label>Quantity</label>
							<select name="qty" class="filter-select filter-quantity">
	    						<option value="1">1</option>
	  							<option value="2">2</option>
	  							<option value="3">3</option>
	    					</select>
						</div>


                        <span class="price dynamic">
                            <label style="margin-top:10px;">Total Price </label>
                            <span class="currency">Rs</span>
                            <span {if $default_jersey_info.discount_value <= 0}class="product_price"{else}class="product_price strike" style="font-weight:normal"{/if}>{$default_jersey_info.price}</span>&nbsp;&nbsp;
                            <span class="discount_dynamic" {if $default_jersey_info.discount_value <= 0}style="display:none;"{/if}>
                                <span class="currency">Rs</span>
                                <span class="product_price">{$default_jersey_info.discounted_price}</span>
                            </span>
                        </span>

                        <div class="discount_dynamic mb10" {if $default_jersey_info.discount_value <= 0}style="display:none;"{/if}>
                            <span class="pdp-off bold">(<span id="jersey_discount_label">{$default_jersey_info.discount_label}</span> OFF)</span>
                            <span class="offer corners">You save Rs. <span id="jersey_discount_value">{$default_jersey_info.discount_value}</span></span>
                        </div>
                        
                        <span class="notes red">Free Personalization worth Rs. 399/-</span>
						<span class="notes">Free Shipping and Cash on Delivery in India</span>
						<span class="notes">International shipping available</span>
                        <div class="user_name"></div>
                        <div class="user_number"></div>
					</div>
					<div class="product-details-main-right right">
						<div class="personalize-block right mr10">
							<input type="checkbox" name="personalize_option" class="personalize_option" value="" checked="checked">
							<label>Personalize</label>
						</div>
						<a class="sizing-chart no-decoration-link" onclick="$('#size-chart-div').slideDown();"> Size Chart</a>
						<div class="size-chart" id="size-chart-div" style="display:none;">
							<div class="sizechart-title"><b>Size Chart</b> (In Centimeters) - for chest</div>
							<div class="close-btn">
								{literal}
								<a onclick="$('#size-chart-div').slideUp();" style="cursor:pointer">
								{/literal}
								<span class="icon-close-1"></span>
								</a>
							</div>
							<div class="sizes">
								<div class="adults">
								<div>ADULTS</div>
									<ul>
										<li style="width:35px"><span class="size-type">XS</span> <span class="size-number">84</span></li>
										<li style="width:35px;_width:30px"><span class="size-type">S</span> <span class="size-number">92</span></li>
										<li style="width:35px;_width:31px"><span class="size-type">M</span> <span class="size-number">100</span></li>
										<li style="width:35px"><span class="size-type">L</span> <span class="size-number">108</span></li>
										<li style="width:35px"><span class="size-type">XL</span> <span class="size-number">116</span></li>
										<li style="width:35px"><span class="size-type">XXL</span> <span class="size-number">124</span></li>
									</ul>
								</div>
							</div>
							<div class="sizechart-title" style="width:275px"><b>Size Chart</b> (In Years) - for age</div>
							<div class="sizes">
								<div class="adults">
								<div>KIDS</div>
									<ul>
										<li style="width:43px;_width:39px"><span class="size-type">JXS</span> <span class="size-number">4-5</span></li>
										<li style="width:42px;_width:38px"><span class="size-type">JS</span> <span class="size-number">6-7</span></li>
										<li style="width:42px"><span class="size-type">JM</span> <span class="size-number">8-9</span></li>
										<li style="width:42px"><span class="size-type">JL</span> <span class="size-number">10-11</span></li>
										<li style="width:42px"><span class="size-type">JXL</span> <span class="size-number">12-13</span></li>
									</ul>
								</div>
							</div>
						</div>
                        <input type="button" value="Buy Now" class="buy-now corners" onclick="{literal}if(validateJerseyForm('')) {_gaq.push(['_trackEvent', 'jersey_widget', 'buy_now']);$('#nikeform').submit();}{/literal}">						
					</div>
				</div>
			</div>
			<div class="other-teams right">
			{foreach from=$jersey_style_ids item=styleids key=i}
				{if $jerseyLandingPage}
					<a href="{$http_location}/{$team_urls[$i]}" class="{$i} link">{$teamNames[$i]}</a>
				{else}
					<a href="javascript:void(0)" onclick="changeJersey('{$i}')" class="{$i} link">{$teamNames[$i]}</a>
				{/if}	
			{/foreach}		
			</div>
		</div>
</form>
</div>
{if !$exclude_common_js}
	<script type="text/javascript" src="{$cdnJSPath}"></script>
{/if}
{literal}
<script type="text/javascript">
    var _gaq = _gaq || [];
</script>
<script type="text/javascript">
var jersey_info_json;
var windowid;
var styleid;
var curr_sel_team;
var default_jersey_style_id;
var http_loc;

	jersey_info_json = $.parseJSON('{/literal}{$jersey_json}{literal}');
	curr_sel_team = '{/literal}{$default_team}{literal}'
	default_jersey_style_id = $.parseJSON('{/literal}{$default_jersey_style_id_json}{literal}');
	windowid = '{/literal}{$windowid}{literal}';
	http_loc = '{/literal}{$http_location}{literal}';
	styleid = default_jersey_style_id[curr_sel_team];

	$(".personalize_option").click(function(){
		$(".personalize-details-block").slideToggle(250,function(){
			if($(".personalize_option").attr("checked") == true){
				$('#personalize').attr("disabled", false);
				$('#preview-name').attr("disabled", false);
				$('#preview-number').attr("disabled", false);
				$("#tab_" + $("#tshirt-type").val()).trigger("click");
			}
		});
		if($(".personalize_option").attr("checked") == false){
			$('.user_name').hide();
			$('.user_number').hide();
			$('#personalize').attr("disabled", true);
			$('#preview-name').attr("disabled", true);
			$('#preview-number').attr("disabled", true);
			//$('.personalize-block').hide();
		}
	});
	

    $("#tshirt-type").live("change", function() {
    	$("#tab_" + $(this).val()).trigger("click");
    });

    $(".product-preview").live("click", function() {
        if(validateJerseyForm("preview")){
        	$("#tab_" + $("#tshirt-type").val()).trigger("click");
        }
    });
	
	
	$(".fb-make-my-profile-pic").live("click", function() {
        if(validateJerseyForm("preview")){
			$("#tab_" + $("#tshirt-type").val()).trigger("click");
			name = $('#preview-name').val();
    	    number = $('#preview-number').val();
			style_id = $('#style_id').val();
			parent.fbMakeMyProfilePicLogin(style_id,name,number);
        }
    });
	
	
    var toggle_text_elements = {};

    /* Toggle Text */
    toggle_text_all(".toggle-text");

    /*
     * This function finds all HTML elements with "toggle-text" class and binds the "focus"/"blur" events for
     * toggling the default text in those elements.
     */
    function toggle_text_all(selector) {
        $(selector).each(function(e) {
            toggle_text_elements[this.id] = $(this).val();
            addToggleText(this);
        });
    }

    function addToggleText(elem) {
        $(elem).addClass("blurred");
        $(elem).focus(function() {
            if ($(this).val() == toggle_text_elements[this.id]) {
                $(this).val("");
                $(this).removeClass("blurred");
            }
        }).blur(function() {
            if ($(this).val() == "") {
                $(this).addClass("blurred");
                $(this).val(toggle_text_elements[this.id]);
            }
        });
    }
    
    $('.filter-quantity').change(function(){
    	var style_price=parseInt($('#style_price').val());
    	var style_quantity=parseInt($('.filter-quantity').val());
    	if(!isNaN(style_price) && !isNaN(style_quantity) && style_price != 0 && style_quantity != 0){
    		calculateDiscountPriceOnJersey();
    	}
    });

	

    function loadFrontView(){
        var jerseys = jersey_info_json[curr_sel_team];
        var front_image = "";
        var curr_style_id=$("#tshirt-type").val();
        var count=0;
        $.each(jerseys, function(index,jersey){
            count++;
    		if(jersey['style_id'] == curr_style_id){
    			front_image=jersey['display_image'];
    			return false;
    		}
        });
        
        $(".product-gallery .product-image-tabs div:nth-child("+count+")").show();
        if(front_image != ""){
        	$('.product-image',$(".product-gallery .product-image-tabs div:nth-child("+count+")")).attr('src', front_image);
        	$('.user_name').hide();
        	$('.user_number').hide();
        }
    }

    function loadBackView(){
    	if($(".personalize_option").attr("checked") == true){
    		if(validateJerseyForm()){
    			$("#tab_" + $("#tshirt-type").val()).trigger("click");
    		}
    	}
    	else{
    		var jerseys = jersey_info_json[curr_sel_team];
    		var back_image="";
    		var curr_style_id=$("#tshirt-type").val();
    		var count=0;
    		$.each(jerseys, function(index,jersey){
    			count++;
    			if(jersey['style_id'] == curr_style_id){
                    back_image=jersey['image'];
                    return false;
            	}
    		});

    		$(".product-gallery .product-image-tabs div:nth-child("+count+")").show();
    		if(back_image != ""){
    		$('.product-image',$(".product-gallery .product-image-tabs div:nth-child("+count+")")).attr('src', back_image);
    		$('.user_name').hide();
    		$('.user_number').hide();
    	}
    }

    }

    function changeJersey(team){
    	
    	curr_sel_team = team;
    	var jerseys=jersey_info_json[team];
    	var content_htmlstr="";
    	var tab_htmlstr="";
    	var tshirt_type_htmlstr="";
    	var styleid=default_jersey_style_id[team];
    	var team_name="";
    	
    	content_htmlstr+="<div class='product-image-tabs blue-bg corners left'>";
    	tab_htmlstr+="<div class='vertical-scroller left clearfix'><span class='browse-btn'><a class='prev up left'></a></span>";
    	tab_htmlstr+="<div class='vertical-scrollable left'><div class='vertical-tabs vertical-items left'>";
    	tshirt_type_htmlstr+="<select class='filter-select filter-tshirt-type' id='tshirt-type' style='width:auto;'>";
    	
    	$.each(jerseys, function(index,jersey){
    		content_htmlstr+="<div>";
    		content_htmlstr+="<img class='product-image' src='"+jersey["display_image"]+"' alt='"+jersey["name"]+" - "+jersey["type"]+"' width='240' height='320'>";
    		content_htmlstr+="<img class='customize-product-image' src='"+jersey["image"]+"' alt='"+jersey["name"]+" - "+jersey["type"]+"' width='240' height='320'>";
    		content_htmlstr+="<select class='customize-product-image' id='select_list_"+jersey["style_id"]+"'>"+jersey["sizes"]+"</select>";
    		content_htmlstr+="</div>";
			if($.browser.msie){
			    tab_htmlstr+="<div><a id='tab_"+jersey["style_id"]+"' class='first' ><img class='corners' src='"+jersey["thumb"]+"' alt='"+jersey["name"]+" - "+jersey["type"]+"' title='"+jersey["name"]+" - "+jersey["type"]+"'></a></div>";
            }
            else{	    
    		tab_htmlstr+="<div><a id='tab_"+jersey["style_id"]+"' class='first'  onclick='jersey_preview(&apos;"+jersey["image"]+"&apos;,&apos;"+jersey["price"]+"&apos;, &apos;"+jersey["style_id"]+"&apos;,&apos;"+jersey["name_font_color"]+"&apos;,&apos;"+jersey["number_image_path"]+"&apos;,&apos;"+jersey["is_customizable"]+"&apos;,&apos;"+jersey["discounted_price"]+"&apos;,&apos;"+jersey["discount_value"]+"&apos;,&apos;"+jersey["discount_label"]+"&apos;)'><img class='corners' src='"+jersey["thumb"]+"' alt='"+jersey["name"]+" - "+jersey["type"]+"' title='"+jersey["name"]+" - "+jersey["type"]+"'></a></div>";
			}
    		tshirt_type_htmlstr+="<option value='"+jersey["style_id"]+"'>"+jersey["type"]+"</option>";
    	});
    	content_htmlstr+="</div>";
    	tab_htmlstr+="</div></div><span class='browse-btn'><a class='next down left'></a></span></div>";
    	tshirt_type_htmlstr+="</select>";
    	$(".product-gallery").html(content_htmlstr+tab_htmlstr);
    	$(".type-filter").html(tshirt_type_htmlstr);	

    	$("div.vertical-tabs").tabs("div.product-image-tabs > div");

    	 /* Scrollable */
        $(".vertical-scrollable").scrollable({size:4,vertical:true,keyboard:false,items:".vertical-items",clickable:false});

        //Fix for vertical scrollable to disable next button on initial load if the number of items is exactly equal to the size of the window
        $(".vertical-scroller").each(function() {
            if ($(".vertical-scrollable .vertical-items div", this).length <= 4) {
                $(".next", this).addClass("disabled");
                $(this).addClass("scroller-disabled");
                $(".product-image-tabs").removeClass("corners");
                $(".product-image-tabs").addClass("corners-tl-o-bl-br");
            }
        });

        if($.browser.msie){
        	$.each(jerseys, function(index,jersey){
    			$("#tab_"+jersey["style_id"]).click(function(){
    				jersey_preview(jersey["image"],jersey["price"],jersey["style_id"],jersey["name_font_color"],jersey["number_image_path"],jersey["is_customizable"],jersey["discounted_price"],jersey["discount_value"],jersey["discount_label"]);
    			});
        	});
        }
        
        var customizable = parseInt(jerseys[0]["is_customizable"]);
        team_name=jerseys[0]["name"]; 
       
        if(customizable){
        	if($(".personalize_option").attr("checked") != true){
        		$('#personalize').attr("disabled", false);
    			$('#preview-name').attr("disabled", false);
    			$('#preview-number').attr("disabled", false);
        		$(".personalize_option").trigger("click");
        		$('.personalize-block').show();
        	}
        }
        else{
        	if($(".personalize_option").attr("checked") == true){
        		$('#personalize').attr("disabled", true);
    			$('#preview-name').attr("disabled", true);
    			$('#preview-number').attr("disabled", true);
        		$(".personalize_option").trigger("click");
        		$('.personalize-block').hide();
        	}
        }
        $(".team-name").html(team_name);
        $(".product-gallery-head .title").html(team_name);
        $("#tab_" + styleid).trigger("click");
    	
    }

    function setStylesForName(font_color){
    	if(curr_sel_team != "ti"){
    		$('.user_name').css({"top":"-90px", "left":"-315px"});
    		$('.user_number').css("left","-315px");
    	}
    	else{
    		$('.user_name').css({"top":"-165px", "left":"-315px"});
    		$('.user_number').css("left","-315px");
    	}
    	$('.user_name').css("color",font_color);
    }


    function validateJerseyForm(type){
    	if(type == ""){
    		// In case no sizes are shown, it implies that product is out of stock.
    		var outOfStock = $('#tshirt-type option:selected').text() + ' ' + $('#size_list').val() + ' ' + $('.product-gallery-head .title').text() + ' is currently out of Stock.';
    		outOfStock = outOfStock + '\nTry other available options';
    		var selectedOption=$('#size_list option:selected').text();
    		var selectSizeMessage = "Please select a size.";
    		if (selectedOption.indexOf("Select") != -1){ alert(selectSizeMessage); return false;}
    		if (selectedOption.indexOf("Out of Stock") != -1) {  alert(outOfStock); return false; }
    	}

    	if($(".personalize_option").attr("checked") == true){
    		if ($('#preview-name').val() == 'Enter a name' || $('#preview-name').val().length <= 0 || $('#preview-name').val().length > 8) {  alert('Please enter a name up to 8 characters long'); return false; }
    	    var pattern = '^[0-9]{1,2}$';
    	    if ($('#preview-number').val() == 'Enter a number' || $('#preview-number').val().length <= 0 || $('#preview-number').val().length > 2 || !$('#preview-number').val().match(pattern)) { alert('Please enter a valid number between 0 and 99'); return false; }
    	    return true;	
    	}
    	else{
    		return true;
    	}
        
    }

    function jersey_preview(image, price, style_id, font_color, image_path, is_customizable, discount_price, discount_value, discount_label) {

    	$('#style_id').val(style_id);
        $('#style_price').val(price);
        $('#style_discount_price').val(discount_price);
        $('#style_discount_value').val(discount_value);
        $('#style_discount_label').val(discount_label);


    	var customizable=parseInt(is_customizable);
    	var curr_sel_quantity = parseInt($('.filter-quantity').val());
    	var curr_sel_style_price = parseInt(price);
        var total_price = curr_sel_quantity * curr_sel_style_price;

        //calculate price for jersey based on discount
        calculateDiscountPriceOnJersey();

    	var selected_size='';
    	if($("#size_list").val())
    	{
    		selected_size=$("#size_list").val();
    	}

        var html = $('#select_list_'+ style_id).html();
        
        $('#size_list').html(html);
    	if(selected_size !='')
    		$("#size_list").val(selected_size);
        $('#tshirt-type').val(style_id);

        if(customizable){
        	if($(".personalize_option").attr("checked") != true){
        		$('#personalize').attr("disabled", false);
    			$('#preview-name').attr("disabled", false);
    			$('#preview-number').attr("disabled", false);
        		$(".personalize_option").trigger("click");
        		$('.personalize-block').show();
        	}
        }
        else{
        	if($(".personalize_option").attr("checked") == true){
        		$('#personalize').attr("disabled", true);
    			$('#preview-name').attr("disabled", true);
    			$('#preview-number').attr("disabled", true);
        		$(".personalize_option").trigger("click");
        		$('.personalize-block').hide();
        	}
        }

        if ($('#preview-name').val() == 'Enter a name' || $('#preview-number').val() == 'Enter a number') { 
        	$('.user_name').html("");
        	$('.user_number').html("");
            return; 
        }  

        var count=0;
        var jerseys = jersey_info_json[curr_sel_team];
        $.each(jerseys, function(index,jersey){
            count++;
    		if(jersey['style_id'] == style_id){
    			return false;
    		}
        });

        $('.product-image' ,$(".product-gallery .product-image-tabs div:nth-child("+count+")")).attr('src', image);
        
        if(customizable){
        	setStylesForName(font_color);
        	var name = $('#preview-name').val();
        	$('.user_name').show().text(name);
        	var number = $('#preview-number').val();

        	var innerhtml = '';
        	for(var i = 1; i <= number.length; i++) {
            	var digit = number.substring((i - 1), i);
				if($.browser.msie)
                innerhtml += '<span style="position:relative;white-space:pre-line;display:inline-block;background:transparent;width:30px;height:45px;filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src=\''+cdn_base+'/'+image_path+digit+'.png\', sizingMethod=\'scale\');"></span>';
            else
                innerhtml += '<div style="width:30px;display:inline;height:45px;"><img src="'+cdn_base+'/'+image_path+digit+'.png" style="width: 30px;height:45px;border:0px;"/></div>';
        	}	

        	$('.user_number').show().html(innerhtml);
        }
        else{
        	$('.user_name').html("");
        	$('.user_number').html("");
        }

    }

    //calculate price for jersey based on discount(if applicable)
    function calculateDiscountPriceOnJersey(){
        //original price detail
        var price = parseInt($('#style_price').val());
        var quantity = parseInt($('.filter-quantity').val());
        var total_price = quantity * price;

        //pre-selection of price elements to optimize filter() usage
        $per_product_price_ele = $('.price > .per_product_price');
        $product_price_ele = $('.price > .product_price');

        var style_discount_value = parseInt($('#style_discount_value').val());

        //on discount show discounted value for the jersey
        if(style_discount_value > 0){
            var style_discount_price = parseInt($('#style_discount_price').val());
            var style_discount_label = $('#style_discount_label').val();
            var total_discount_price = quantity * style_discount_price;
            var total_discount_value = quantity * style_discount_value;

            $('.per_product_price').text(style_discount_price);
            $('.product_price').text(total_discount_price);
            $('#jersey_discount_value').text(total_discount_value);
            $('#jersey_discount_label').text(style_discount_label);

            //show before discounted price as striked
            $per_product_price_ele.filter(':first').addClass('strike');
            $per_product_price_ele.filter(':first').css('font_weight','normal');
            $product_price_ele.filter(':first').addClass('strike');
            $product_price_ele.filter(':first').css('font_weight','normal');

            $('.price > .strike').text(price);
            $('.dynamic > .strike').text(total_price);
            $('.discount_dynamic').show();

        } else {//otherwise show original value
            $('.per_product_price').text(price);
            $('.product_price').text(total_price);

            //show original price
            $per_product_price_ele.filter(':first').removeClass('strike');
            $per_product_price_ele.filter(':first').css('font_weight','inherit');
            $product_price_ele.filter(':first').removeClass('strike');
            $product_price_ele.filter(':first').css('font_weight','inherit');
            $('.discount_dynamic').hide();
        }
    }

</script>
{/literal}