{foreach from=$jsFiles item=href}
    <script type="text/javascript" src="{$href}"></script>
{/foreach}
{if $analyticsStatus}
	{include file="site/analytics_data_collection.tpl"}
{/if} 
<script type="text/javascript">
    var _gaq = _gaq || [];
    var megamenutestsegment='{$megamenutestsegment}';
    _gaq.push(['_setAccount', '{$ga_acc}']);
    _gaq.push(['_setDomainName', '{$cookiedomain}']);
{if $lt_new_session eq "true"}
{/if}            
	{if $_MAB_GA_calls}
	{$_MAB_GA_calls}
	{/if}
    _gaq.push(['_trackPageview']);
    _gaq.push(['_trackPageLoadTime']);
{* GA page load call moved to afterOnLoad() in custom.js *}
{* Mediamind specific code starts *}
    var ebRand = Math.random()+'';
    ebRand = ebRand * 1000000;
{* Media mind page load call moved to afterOnLoad() in custom.js *}        
{* Mediamind specific code ends *}
{* Call the afterOnLoad function to execute code after the onLoad event *}
    $(window).load(function () {ldelim}
        afterOnLoad();		
    {rdelim});
</script>
 
 {if $enableGoogleCPC == 'true'}
 {if $displayRemarkettingTag == 'true'}
 <script type="text/javascript">			
			var google_conversion_id = 1068346998;
			var google_conversion_language = "en";
			var google_conversion_format = "3";
			var google_conversion_color = "{$googe_conversion_color}";
			var google_conversion_label = "{$googe_conversion_label}"; 
			var google_conversion_value = 0;
</script>
		<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js"></script>
		<noscript>
			<div style="display:inline;">
			<img height="1" width="1" style="border-style:none;" alt="" src="http://www.googleadservices.com/pagead/conversion/1068346998/?label={$googe_conversion_label}&amp;guid=ON&amp;script=0"/> 
			</div>
			</noscript>	
 {/if}
 {/if} 
{if $hideSaleBlock}
	{literal}
	<script type="text/javascript">
		$(document).ready(function(){
			$('.sale-link-block').hide();
		});
	</script>
	{/literal}
{/if}

{if $newrelic_footer}
  {$newrelic_footer}
{/if}
