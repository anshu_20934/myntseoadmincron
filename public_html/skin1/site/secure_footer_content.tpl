<div class="icon-line-simple mb10"></div>
<div class="footer-simple clearfix span-33 last">
	<div id="copyright" class="left">&copy; myntra.com. All rights reserved.</div>
	<div class="footer-links-simple clearfix right">
		<ul>
			<li><a class="no-decoration-link" href="{$http_location}/">Home</a></li>
			<li><a class="no-decoration-link" href="{$http_location}/mkmycart.php">My Shopping Cart</a></li>
			<li><a class="no-decoration-link" href="{$http_location}/contactus">Contact Us</a></li>
			<li><a class="no-decoration-link" href="{$http_location}/mkfaq.php">FAQs</a></li>
			<li><a class="no-decoration-link" href="{$http_location}/privacy_policy.php">Privacy Policy</a></li>
			<li><a class="no-decoration-link" href="{$http_location}/termandcondition.php">Terms of Use</a></li>
		</ul>
	</div>
</div>

<div id="fb-root"></div>