{*if $analyticsStatus}
<script language="JavaScript" type="text/javascript">
var omnistat = {if $analytics_json}{$analytics_json}{else}''{/if};
s.pageName=omnistat.pageName;
s.server="";
s.channel=omnistat.channel;
s.pageType="";
s.prop1=omnistat.prop1;
s.prop2=omnistat.prop2;
s.prop3=omnistat.prop3;
s.prop4=omnistat.prop4;
s.prop5=omnistat.prop5;
/* Conversion Variables */
s.campaign="";
s.state="";
s.zip="";
s.events=omnistat.events;
s.products=omnistat.products;
s.purchaseID=omnistat.purchaseID;
s.eVar1="";
s.eVar2="";
s.eVar3=omnistat.eVar3;
s.eVar4=omnistat.eVar4;
s.eVar5=omnistat.eVar5;
s.eVar6=omnistat.eVar6;
s.eVar12=omnistat.eVar12;
s.eVar15=omnistat.eVar15;
s.eVar16=omnistat.eVar16;
s.eVar18=omnistat.eVar18;
s.eVar20=omnistat.eVar20;
s.eVar21=omnistat.eVar21;
// Setting s to s_analytics global variable as s is getting unset by some peice of code b/w here 
// and afterOnLoad function call in custom.js
var s_analytics = s;
var s_analytics_firesync = {if $fireSyncOmnitureCall}true{else}false{/if};
if(s_analytics_firesync) {
	s_analytics.t();
}
</script>
<!--<noscript><img src="http://myntra.d1.sc.omtrdc.net/b/ss/{$analytics_account}/1/H.24.2--NS/0"
height="1" width="1" border="0" alt="" /></noscript>-->
{/if*}
