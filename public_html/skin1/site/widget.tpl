  {literal}
	{RECENT_WIDGET_PLACEHOLDER}
    {/literal}
{foreach name=widgetloop item=widget from=$scrollableWidgets}
<script type="text/javascript">
	lists_image_srcs['{$widget.nameclean}']= new Object();
</script>
<div class="category  clearfix mb40">
<!--	<a class="widget-title-link corners-tl-tr" href="javascript:void(0)"></a>-->
	<h2 class="h2g prepend">{$widget.name}</h2>
	<div class="icon-line mb10"></div>
	<!-- scrollable-box -->
	<div class="scrollable-box clearfix last">
		<!-- "previous page" action -->
		<a class="prevPage browse left"></a>
		<!-- root element for scrollable -->
		<div class="scrollable">
			<!-- root element for the items -->
			<ul class="items items_{$widget.nameclean}">
				{assign var=i value=1}
				{foreach name=widgetdataloop item=widgetdata from=$widget.data}
				{if $widgetdata.product neq '' && $widgetdata.product neq ' '}
				<li class="item">
				{if $widgetdata.discount_label}<div class="discount-label val"><span>{$widgetdata.discount_label}</span></div>{/if}
					<a href="{$widgetdata.landingpageurl}" class="{if $i lt 5}loaded{else}to-be-loaded{/if}">
						<span class="item-image item-image-border">
							{if $i lt 5}
							<img id="im_product_img_{$widget.nameclean}_{$widgetdata.styleid}" src="{$cdn_base}/skin1/myntra_images/loading_240_320.gif" lazy-src="{$widgetdata.search_image}" class="lazy" height="320" width="240" alt="{$widgetdata.product}">
							{else}
							<img id="im_product_img_{$widget.nameclean}_{$widgetdata.styleid}" src="{$cdn_base}/skin1/myntra_images/loading_240_320.gif" height="320" width="240" alt="{$widgetdata.product}">
							<script type="text/javascript">
								lists_image_srcs["{$widget.nameclean}"]['im_product_img_{$widget.nameclean}_{$widgetdata.styleid}'] = '{$widgetdata.search_image}';
							</script>
							{/if}
<!--							{if $widgetdata.discount_type eq 'percent'}-->
<!--							<span class="discount-details blue-bg">{$widgetdata.discount_value}% Off</span>-->
<!--							{elseif $widgetdata.discount_type eq 'absolute'}-->
<!--							<span class="discount-details blue-bg">Rs {$widgetdata.discount_value}/- Off</span>-->
<!--							{/if}-->
						</span>
						<span class="item-divider icon-line"></span>
						<span class="item-description">
							<span class="name">{$widgetdata.product|truncate:40}</span>
							<div class="noncashback" style={if $cashback_gateway_status eq 'on' && $widgetdata.showcashback eq 'true'}"display:none;"{else}"display:block; float:none;"{/if}>	
								{if $widgetdata.discounted_price neq $widgetdata.price}
									<span class="discount-price highlight" style="display: inline;padding:2px 5px;">Rs {$widgetdata.discounted_price}</span>
									<span class="original-price strike" style="display: inline;padding:2px 5px;">Rs {$widgetdata.price}</span>
									{if $widgetdata.discount_label}<span class="black-bg2 corners price-off">{$widgetdata.discount_label} OFF</span>{/if}
								{else}
								<span class="discount-price" style="display: inline;padding:2px 5px;">Rs {$widgetdata.discounted_price}</span>
								<span class="original-price" style="display: inline;padding:2px 5px;"></span>
								{/if}
							</div>
							<div class="cashbackblock" style={if $cashback_gateway_status eq 'on' && $widgetdata.showcashback eq 'true'}"display:block;"{else}"display:none;"{/if}> 
								<div class="pblock">	
                                        <span class="discount-price{if $widgetdata.discounted_price neq $widgetdata.price} highlight{/if}">{if $widgetdata.discounted_price eq $widgetdata.price}<b class="smalltext">You Pay </b>{/if}Rs {$widgetdata.discounted_price}</span>
                                        {if $widgetdata.discounted_price neq $widgetdata.price}
                                            <span class="original-price strike">Rs {$widgetdata.price}</span>

                                        {/if}
								</div>
								{if $widgetdata.discounted_price eq $widgetdata.price}
								<div class="cb-block" style={if $widgetdata.cashback neq 0}"display:block;"{else}"display:none;"{/if}>
                            		<span class="cashtitle">Get 10% Cashback</span><span class="cashrs">Rs {$widgetdata.cashback|round|string_format:"%d"} <b class="cb">What's This?</b></span>
                            		<span class="cbtip hide"><b style="display: block;">Cashback</b> <img src="{$cdn_base}/skin1/images/cart-removebtn.png" alt="close" class="tipclose">On purchasing this product, you will receive a cashback credit upto the amount indicated here. Cashback is calculated on item value excluding any coupon discounts. You can redeem this on any of your subsequent purchases at Myntra.</span>
                                </div>
                                {/if}
							</div>
							<span class="availability">AVAILABILITY: {if $widgetdata.sizes neq '' && $widgetdata.sizes neq ' '}{$widgetdata.sizes|replace:",":", "}{else}SOLD OUT{/if}</span>
						</span>
						<span class="item-divider icon-line"></span>
					</a>
				</li>
				{/if}
				{assign var=i value=$i+1}
				{/foreach}
			</ul>
		</div>
		<!-- "next page" action -->
		<a id='{$widget.nameclean}' class="nextPage browse right"></a>
    </div>
</div>
{/foreach}

