<div class="brand-logos-scrollable-box clearfix">
<a class="prevPage browse left disabled" id="brand-logos-prev"></a>
<div class="all-brand-logos brand-logos-scrollable mt10 clearfix">
	<ul class="brand-logo-items">
		<li class="item">
			<a href="{$http_location}/nike" class="nike-brand-logo left"></a>
		</li>
		<li class="item">
			<a href="{$http_location}/puma" class="puma-brand-logo left"></a>
		</li>
		<li class="item">
			<a href="{$http_location}/levis" class="levis-brand-logo left"></a>
		</li>
		<li class="item">
			<a href="{$http_location}/lee" class="lee-brand-logo left"></a>
		</li>
		<li class="item">
			<a href="{$http_location}/adidas" class="adidas-brand-logo left"></a>
		</li>
		<li class="item">
			<a href="{$http_location}/arrow" class="arrow-brand-logo left"></a>
		</li>
		<li class="item">
			<a href="{$http_location}/united-colors-of-benetton" class="ucb-brand-logo left"></a>
		</li>
		<li class="item">
			<a href="{$http_location}/US-POLO-ASSN." class="us-polo-brand-logo left"></a>
		</li>
		<li class="item">
			<a href="{$http_location}/reid-&-taylor" class="reid-taylor-brand-logo left"></a>
		</li>
		<li class="item">
			<a href="{$http_location}/newfeel" class="newfeel-brand-logo left"></a>
		</li>
		<li class="item">
			<a href="{$http_location}/numero-uno" class="numero-uno-brand-logo left"></a>
		</li>
		<li class="item">
			<a href="{$http_location}/fila" class="fila-brand-logo left"></a>
		</li>
		<li class="item">
			<a href="{$http_location}/proline" class="proline-brand-logo left"></a>
		</li>
		<li class="item">
			<a href="{$http_location}/wildcraft" class="wildcraft-brand-logo left"></a>
		</li>
	</ul>
</div>
<a class="nextPage browse right" id="brand-logos-next"></a>
</div>
