				<ul class="clearfix last">
                    {section name=product loop=$products }
					<li class="item show_details" data-id="prod_{$products[product].id}">
					{if $show_additional_product_details eq 'y'}
						<div class="style-details" style="display:none"></div>
					{/if}	
						{if $products[product].discount_label}<div class="discount-label val"><span>{$products[product].discount_label}</span></div>{/if}
						<a href="{if $products[product].landingpageurl}{$http_location}/{$products[product].landingpageurl}{/if}">
							<span class="item-image item-image-border">
								<img src="{$cdn_base}/skin1/images/loader_180x240.gif" class="jquery-lazyload" alt="{$products[product].product}" height="320" width="240" original="{$products[product].searchimagepath}">
								<!--img src="{$cdn_base}/skin1/myntra_images/loading_240_320.gif" lazy-src="{$products[product].searchimagepath}" id="im_search_res_{$products[product].id}" class="lazy" alt="{$products[product].product}" height="320" width="240"-->
							</span>
							<span class="item-divider icon-line"></span>
							<div class="item-description">
								<span class="name">{$products[product].product|truncate:40}</span>
									<div class="noncashback" style={if $cashback_gateway_status eq 'on' && $products[product].showcashback eq 'true'}"display:none;"{else}"display:block; float:none;"{/if}>
									{if $products[product].discount_type && $products[product].discount_value neq ''}
										<span class="discount-price highlight" style="display: inline;padding:2px 5px;">{$rupeesymbol}{$products[product].discounted_price}</span>
										<span class="original-price strike" style="display: inline;padding:2px 5px;">{$rupeesymbol}{$products[product].price}</span>
										{if $products[product].discount_label}<span class="black-bg2 corners price-off">{$products[product].discount_label} OFF</span>{/if}
									{else}
										<span class="discount-price" style="display: inline;padding:2px 5px;">{$rupeesymbol}{$products[product].price}</span>
									{/if}
									</div>
									
									<div class="cashbackblock clearfix" style={if $cashback_gateway_status eq 'on' && $products[product].showcashback eq 'true'}"display:block;"{else}"display:none;"{/if}> 
										<div class="pblock">	
											{if $products[product].discount_type && $products[product].discount_value neq ''}
												<span class="discount-price highlight">{$rupeesymbol}{$products[product].discounted_price}</span>
												<span class="original-price strike">{$rupeesymbol}{$products[product].price}</span>
											{else}
												<span class="discount-price"><b class="smalltext">You Pay</b> {$rupeesymbol}{$products[product].price}</span>
											{/if}
										</div>
										<div class="cb-block" style={if $products[product].cashback neq 0}"display:block;"{else}"display:none;"{/if}>
											<span class="cashtitle">Get 10% Cashback</span><span class="cashrs">{$rupeesymbol}{$products[product].cashback|round|string_format:"%d"} <b class="cb">What's This?</b></span>
                                            <span class="cbtip hide"><b style="display: block;">Cashback</b> <img src="{$cdn_base}/skin1/images/cart-removebtn.png" alt="close" class="tipclose">On purchasing this product, you will receive a cashback credit upto the amount indicated here. Cashback is calculated on item value excluding any coupon discounts. You can redeem this on any of your subsequent purchases at Myntra.</span>
										</div>
									</div>
									{if $products[product].generic_type neq 'design'}
										<span class="availability">AVAILABILITY: {if $products[product].sizes neq '' && $products[product].sizes neq ' '}{$products[product].sizes|replace:",":", "}{else}SOLD OUT{/if}</span>
									{/if}
							</div>
							<span class="item-divider icon-line"></span>
						</a>
					</li>
                    {/section}
            	</ul>