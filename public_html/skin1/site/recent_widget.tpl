
   {if $recent_viewed_count gt 2}
   		<script type="text/javascript">
			lists_image_srcs['{$widget.nameclean}']= new Object();
   		</script>
		<div class="category  clearfix mb40">
		   <h2 class="h2g prepend">Recently viewed</h2>
		   <div class="icon-line mb10"></div>
       		  <div class="recent-cont scrollable-box clearfix last">
       		     	<a class="prevPage browse left"></a>
          	 		<div class="new-scrollable">
          	 			<div class="recent-widget-overlay"></div>
   						<ul class="new-recent-widget items items_{$widget.nameclean}">
						{assign var=i value=1}
						{foreach name=widgetdataloop item=widgetdata from=$widget.data}
						{if $widgetdata.product neq '' && $widgetdata.product neq ' '}
							<li class="item ">
								<div class="left img-cont">
									<a href="{$widgetdata.landingpageurl}" class="{if $i lt 5}loaded{else}to-be-loaded{/if}" onclick="_gaq.push(['_trackEvent','{$ga_category}','{$ga_action}', 'slot_{$i}_viewed']);">
									{if $i lt 4}
									<img id="im_product_img_{$widget.nameclean}_{$widgetdata.styleid}" src="{$cdn_base}/skin1/myntra_images/loading_240_320.gif" lazy-src="{$widgetdata.search_image}" class="lazy" height="156" width="116" alt="{$widgetdata.product}" />
									{elseif $i eq 4}
									<img id="im_product_img_{$widget.nameclean}_{$widgetdata.styleid}" src="{$cdn_base}/skin1/myntra_images/loading_240_320.gif" lazy-src="{$widgetdata.search_image}" class="lazy" height="156" width="116" alt="{$widgetdata.product}" />
									{else}
									<img id="im_product_img_{$widget.nameclean}_{$widgetdata.styleid}" src="{$cdn_base}/skin1/myntra_images/loading_240_320.gif" height="156" width="116" alt="{$widgetdata.product}">
									<script type="text/javascript">
									lists_image_srcs["{$widget.nameclean}"]['im_product_img_{$widget.nameclean}_{$widgetdata.styleid}'] = '{$widgetdata.search_image}';
									</script>
						        	{/if}
									</a>
								</div>
								<div class="left prod-details">
									<div class="prod-stat-box">	
										<h3 class="prod-ttle "><a href="{$widgetdata.landingpageurl}" onclick="_gaq.push(['_trackEvent','{$ga_category}','{$ga_action}', 'slot_{$i}_viewed']);">{$widgetdata.product|truncate:40}</a></h3>
										{if $widgetdata.discounted_price neq $widgetdata.price}<span class="strike">{$rupeesymbol}{$widgetdata.price}</span><span class="disc-per"> ({$widgetdata.discount_label} Off)</span>
										<span class="disc-price">Now {$rupeesymbol}{$widgetdata.discounted_price}</span>
										<span class="sav-disp">You save {$rupeesymbol}{math equation="x-y" x=$widgetdata.price y=$widgetdata.discounted_price assign="discount_amount"}{$discount_amount|round|string_format:"%d"}</span>
										{else}
										<span class="orig-price ">{$rupeesymbol}{$widgetdata.price}</span>
										{if $cashback_gateway_status eq 'on' && $widgetdata.showcashback eq 'true'}<span class="widget-cb">Get 10% Cashback</span>{/if}
										{/if}
									</div>
									<form method="post"  action="mkretrievedataforcart.php?pagetype=productdetail&lead=recently_viewed" id="form-{$widgetdata.styleid}">
										<input type="hidden" name="_token" value="{$USER_TOKEN}" />
										<input type="hidden" name="productStyleId" value="{$widgetdata.styleid}">
										<input type="hidden" name="quantity" value="1">
										<input type="hidden" name="sizename[]" value="" class="sizename">
										<input type="hidden" name="sizequantity[]" value="1">
										<input type="hidden" name="productSKUID" class="productSKUID" value="">
										<select class="widget-size-select size-select" id="style-{$widgetdata.styleid}">
											<option value="">select size</option>
										</select>
										<button class="add-to-cart-button new-cart-bttn">Buy Now</button>
									</form>
								</div>
							</li>
						{/if}
						{assign var=i value=$i+1}
						{/foreach}
						</ul>
					</div>
					<a id='{$widget.nameclean}' class="nextPage browse right"><span  class="nxt-new"></span></a>
			 </div>
		  	 <div class="clear"><hr /></div>
		</div>
		
	{else}
		
		<div class="category clearfix mb40" style="width:600px;float:left">
		   	<h2 class="h2g prepend">Recently viewed</h2>
		   	<div class="icon-line mb10"></div>
   			<div class="recent-cont clearfix last">
	   		   <div class="non-scrollable">
	   				<ul class="new-recent-widget items_{$widget.nameclean}">
					{assign var=j value=1}
					{foreach name=widgetdataloop item=widgetdata from=$widget.data}
					{if $widgetdata.product neq '' && $widgetdata.product neq ' '}
						{if $j <= $recent_viewed_count}
						<li class="item ">
							<div class="left img-cont">
								<a href="{$widgetdata.landingpageurl}" class="loaded" onclick="_gaq.push(['_trackEvent','{$ga_category}','{$ga_action}', 'slot_{$i}_viewed']);">
									<img id="im_product_img_{$widget.nameclean}_{$widgetdata.styleid}" src="{$cdn_base}/skin1/myntra_images/loading_240_320.gif" lazy-src="{$widgetdata.search_image}" class="lazy" height="156" width="116" alt="{$widgetdata.product}" />
								</a>
							</div>
							<div class="left prod-details">
								<div class="prod-stat-box">
									<h3 class="prod-ttle "><a href="{$widgetdata.landingpageurl}" onclick="_gaq.push(['_trackEvent','{$ga_category}','{$ga_action}', 'slot_{$i}_viewed']);">{$widgetdata.product|truncate:40}</a></h3>
									{if $widgetdata.discounted_price neq $widgetdata.price}<span class="strike">{$rupeesymbol}{$widgetdata.price}</span><span class="disc-per"> ({$widgetdata.discount_label} Off)</span>
									<span class="disc-price">Now {$rupeesymbol}{$widgetdata.discounted_price}</span>
									<span class="sav-disp">You save {$rupeesymbol}{math equation="x-y" x=$widgetdata.price y=$widgetdata.discounted_price assign="discount_amount"}{$discount_amount|round|string_format:"%d"}</span>
									{else}
									<span class="orig-price ">{$rupeesymbol}{$widgetdata.price}</span>
									{if $cashback_gateway_status eq 'on' && $widgetdata.showcashback eq 'true'}<span class="widget-cb">Get 10% Cashback</span>{/if}
									{/if}
								</div>
								<form method="post"  action="mkretrievedataforcart.php?pagetype=productdetail&lead=recently_viewed" id="form-{$widgetdata.styleid}">
									<input type="hidden" name="_token" value="{$USER_TOKEN}" />
									<input type="hidden" name="productStyleId" value="{$widgetdata.styleid}">
									<input type="hidden" name="quantity" value="1">
									<input type="hidden" name="sizename[]" value="" class="sizename">
									<input type="hidden" name="sizequantity[]" value="1">
									<input type="hidden" name="productSKUID" class="productSKUID" value="">
									<select class="widget-size-select size-select" id="style-{$widgetdata.styleid}">
										<option value="">select size</option>
									</select>
									
									<button class="add-to-cart-button new-cart-bttn" >Buy Now</button>
								</form>
							</div>
						</li>
						{/if}
					{/if}
					{assign var=j value=$j+1}
					{/foreach}
					<div class="clear"><hr /></div>
					</ul>
				</div>
		  </div>
		  <div class="clear"><hr /></div>
		</div>
		
		{****SCROLLABLE RECOMMENDATION****}
		<script type="text/javascript">
			lists_image_srcs['{$widget.nameclean}']= new Object();
 		</script>
		<div class="category clearfix mb40" style="width:350px;float:left;margin-left:30px;position:relative">
			<h2 class="h2g prepend">Recommended for you</h2>
			<div class="icon-line mb10"></div>
			<div class="recomm-cont scrollable-box clearfix last">
       		    <a class="prev browse left "></a>
				<div class="new-single-scrollable">
   					<ul class="new-reco-widget  items items_{$widget.nameclean}">
					{assign var=j value=1}
					{foreach name=widgetdataloop item=widgetdata from=$widget.data}
					{if $widgetdata.product neq '' && $widgetdata.product neq ' '}
						{if $j > $recent_viewed_count}
						<li class="item ">
							<div class="left img-cont">
								<a href="{$widgetdata.landingpageurl}" class="{if $j eq $recent_viewed_count+1}loaded{else}to-be-loaded{/if}" onclick="_gaq.push(['_trackEvent','{$ga_category}','{$ga_action}', 'slot_{$i}_suggested']);">
									{if $j eq $recent_viewed_count+1}
									<img id="im_product_img_{$widget.nameclean}_{$widgetdata.styleid}" src="{$cdn_base}/skin1/myntra_images/loading_240_320.gif" lazy-src="{$widgetdata.search_image}" class="lazy" height="156" width="116" alt="{$widgetdata.product}" />
									{else}
									<img id="im_product_img_{$widget.nameclean}_{$widgetdata.styleid}" src="{$cdn_base}/skin1/myntra_images/loading_240_320.gif"  height="156" width="116" alt="{$widgetdata.product}" />
									<script type="text/javascript">
										lists_image_srcs["{$widget.nameclean}"]['im_product_img_{$widget.nameclean}_{$widgetdata.styleid}'] = '{$widgetdata.search_image}';
									</script>
									{/if}
								</a>
							</div>
							<div class="left prod-details">
								<div class="prod-stat-box">
									<h3 class="prod-ttle "><a href="{$widgetdata.landingpageurl}" onclick="_gaq.push(['_trackEvent','{$ga_category}','{$ga_action}', 'slot_{$i}_suggested']);">{$widgetdata.product|truncate:40}</a></h3>
									{if $widgetdata.discounted_price neq $widgetdata.price}<span class="strike">{$rupeesymbol}{$widgetdata.price}</span><span class="disc-per"> ({$widgetdata.discount_label} Off)</span>
									<span class="disc-price">Now {$rupeesymbol}{$widgetdata.discounted_price}</span>
									<span class="sav-disp">You save {$rupeesymbol}{math equation="x-y" x=$widgetdata.price y=$widgetdata.discounted_price assign="discount_amount"}{$discount_amount|round|string_format:"%d"}</span>
									{else}
									<span class="orig-price ">{$rupeesymbol}{$widgetdata.price}</span>
									{if $cashback_gateway_status eq 'on' && $widgetdata.showcashback eq 'true'}<span class="widget-cb">Get 10% Cashback</span>{/if}
									{/if}
								</div>
								<span class="item-available">In Stock</span><span class="free-ship">Free Shipping in India</span>
								<a href="{$widgetdata.landingpageurl}" class="view-details new-cart-bttn" onclick="_gaq.push(['_trackEvent','{$ga_category}','{$ga_action}_details', 'slot_{$i}_suggested']);">View Details</a>
							</div>
						</li>
						{/if}
					{/if}
					{assign var=j value=$j+1}
					{/foreach}
					</ul>
				</div>
				<a id='{$widget.nameclean}' class="next browse right single-scroll"></a>
		  </div>
		  <div class="clear"><hr /></div>
	</div>
	<div class="clear"><hr /></div>	
	{/if}
		
		
	