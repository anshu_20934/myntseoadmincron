					{assign var=i value=1}
						{assign var=col value=1}
						{foreach key=brand item=count from=$brands}
							{if $col lt $col_limit}
								{if $i gt $limit+1 || $i eq 1}
									<ul class="span-4">
								{/if}
								{if $brand|lower neq '_empty_'}
									<li><a href="{$http_location}/{myntralink search_parameter="global_attr_gender=$prefix&brands_filter_facet=$brand"}" class="no-decoration-link">{$brand}</a></li>
									{if $brand|strlen gt 16}
										{assign var=i value=$i+2}
									{else}		
										{assign var=i value=$i+1}
									{/if}
								{/if}
								{if $i gt $limit+1 || $i eq 1}
									</ul>
									{assign var=col value=$col+1}
									{assign var=i value=1}
								{/if}
							{elseif $col eq $col_limit}
								{if $i gt $limit+1 || $i eq 1}
									<ul class="last">
								{/if}
								{if $brand|lower neq '_empty_'}
									{if $i gt $limit-1}
									<li><a href="{$http_location}/brands" class="no-decoration-link">More Brands...</a></li>
									{assign var=i value=$i+2}
									{else}
									<li><a href="{$http_location}/{$prefix}{$brand|lower|replace:' ':'-'}" class="no-decoration-link">{$brand}</a></li>
									{/if}
									{if $brand|strlen gt 16}
										{assign var=i value=$i+2}
									{else}		
										{assign var=i value=$i+1}
									{/if}
								{/if}
								{if $i gt $limit+1 || $i eq 1}
									</ul>
									{assign var=col value=$col+1}
									{assign var=i value=1}
								{/if}
							{/if}	
    					{/foreach}