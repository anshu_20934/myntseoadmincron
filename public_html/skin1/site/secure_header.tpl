
 <title>
      {if $pageTitle ne ''}{$pageTitle}{elseif $pageHeader ne ''}{$pageHeader}{else}Myntra{/if}
  </title>
  <META http-equiv="Content-Style-Type" content="text/css">
  <META http-equiv="Content-Script-Type" content="type">
  <META http-equiv="Content-Type" content="text/html; charset=utf-8">


  <meta name="description" content="{$metaDescription|escape}">
  <meta name="keywords" content="{$metaKeywords|escape}">
  {if $noindex eq 2}
	<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
  {/if}
  <link rel="shortcut icon" href="{$secure_cdn_base}/skin1/icons/favicon.ico">
  
    {foreach from=$cssFiles item=href}
       <link href="{$href}" rel="stylesheet" type="text/css">
    {/foreach}
  
  {if $affiliateId neq '' && $themeid eq 'COBRANDED_UI'}
	<link rel="stylesheet" href="{$SkinDir}/affiliatetemplates/affiliate.css">
  {/if}
  {if $tooltip }
  	{$tooltip}
  {/if}
  {if $newrelic_header}
    {$newrelic_header}
  {/if}
  <base href="{$https_location}">
  <script type="text/javascript">
    var http_loc = '{$http_location}';
    var https_loc = '{$https_location}';
    var xsessionid = '{$xsessionid}'
    var cdn_base = '{$secure_cdn_base}';
    var facebook_app_id = '{$facebook_app_id}';
    var s_account= '{$analytics_account}';
    var enableMyThings=0;
    {if $enableMyThings == 'true'}enableMyThings=1;{/if}

	{literal}
		var Myntra = Myntra || {};
		Myntra.Data = Myntra.Data || {};
	{/literal}
		Myntra.Data.cookiedomain = "{$cookiedomain}";
	    Myntra.Data.cookieprefix = "{$cookieprefix}";
  </script>
      {literal}
<style type="text/css">
.icon-facebook-simple { 
  background: url('{/literal}{$secure_cdn_base}{literal}/skin1/myntra_images/global-icons.png') no-repeat;
  background-position: -19px top;
  height: 32px;
  width: 32px;
}
.icon-twitter-simple { 
  background:transparent url('{/literal}{$secure_cdn_base}{literal}/skin1/myntra_images/global-icons.png') no-repeat;
  background-position: -51px top;
  height: 32px;
  width: 32px;
}
.myntra-logo-simple{
	top:0px;
	margin-top: 5px;
}
.myntra-logo-simple a {
	background:transparent url('{/literal}{$secure_cdn_base}{literal}/skin1/myntra_images/logo.png') no-repeat;
    color: #FFFFFF;
    font-family: helvetica;
    font-size: 25px;
    font-weight: bold;
    line-height: 37px;
    height: 60px;
    width:252px;
    margin: 0;
    text-align: center;
    text-decoration:none;
    display:block;
}
.icon-line-simple{ 
  background-image: url('{/literal}{$secure_cdn_base}{literal}/skin1/myntra_images/global-icons.png');
  background-position: -264px top;
  height: 4px;
}
</style>
{/literal}
