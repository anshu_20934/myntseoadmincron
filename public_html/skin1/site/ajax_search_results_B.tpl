
                 <ul class="clearfix last {if $cashback_displayOnSearchPage neq '1'}without_cashback_search{/if} ">
                  {foreach name=widgetdataloop key=index item=widgetdata from=$products}
                       {assign var=mod_val value=$index%4}

					<li class="item itemsizes {if $mod_val eq '3'}rowlast{/if}" data-id="prod_{$widgetdata.id}">
					<div class="style-details" style="display:none"></div>
					 	<div class="new-label"> </div>
						<a href="{if $widgetdata.landingpageurl}{$http_location}/{$widgetdata.landingpageurl}{/if}">
							{if $quicklookenabled}
								<span class="quick-look action-btn" data-widget="search_results" data-styleid="{$widgetdata.styleid}" data-href="{$widgetdata.landingpageurl}"><span class="quick-look-icon"></span>Quick View</span>
							{/if}	
                            <div class="item-box">
                                <img src="{$cdn_base}/skin1/images/loader_180x240.gif" class="jquery-lazyload" alt="{$widgetdata.product}" height="240" width="180" original="{$widgetdata.searchimagepath}">
								<div class="item-desc">
	                                <span class="name">{$widgetdata.product|truncate:40}</span>
	                                {if $cashback_displayOnSearchPage eq '1'}
	                                	{if $widgetdata.discount > 0}
	                                    		<div class="op">MRP <span class="strike">{$rupeesymbol}{$widgetdata.price|number_format:0:".":","}</span> <span style="color:#000"> &nbsp; {$rupeesymbol}{math equation="x-y" x=$widgetdata.price y=$widgetdata.discounted_price assign="discount_amount"}{$discount_amount|round|number_format:0:".":","} Off </span></div>
	                                    		<div class="dp"><span style="font-weight:normal">{$rupeesymbol}</span>{$widgetdata.discounted_price|number_format:0:".":","}</div>
	                                    		{eval var=$widgetdata.discount_label} 
	                                	{elseif $widgetdata.discount_label}                                
	                                        	<div class="dp"><span style="font-weight:normal">{$rupeesymbol}</span>{$widgetdata.price|round|number_format:0:".":","}</div>
	                                        	{eval var=$widgetdata.discount_label}                                     
	                            		{else}
	                              	    	{if $cashback_gateway_status eq 'on' && $widgetdata.showcashback eq 'true'}
	                                        	<div class="op">Effective Price {$rupeesymbol}{math equation="x-y" x=$widgetdata.price y=$widgetdata.cashback assign="effectiveprice"}{$effectiveprice|round|number_format:0:".":","}</div>
	                                        	<div class="dpv"><div class="dp"><span style="font-weight:normal">{$rupeesymbol}</span>{$widgetdata.price|round|number_format:0:".":","}</div>
	                                            <span class="cbp">with <b>10%</b></span><span class="cb">Cashback</span>
	                                        	</div>
	                                    	{/if}
	                            		{/if}
	                            	{else} {* condition where displayOnSearchPage is not '1' *}
	                            		{if $widgetdata.discount > 0 }
                                                    <div class="dp left"><span style="font-weight:normal">{$rupeesymbol}</span>{$widgetdata.discounted_price|number_format:0:".":","}</div> <div class="op"><span class="strike" style="float:left;">{$widgetdata.price|number_format:0:".":","}</span></div>
                                                   {eval var=$widgetdata.discount_label} 
	                                	{else}
                                                    <div class="dp"><span style="font-weight:normal">{$rupeesymbol}</span>{$widgetdata.price|round|number_format:0:".":","}</div>
                                                   {eval var=$widgetdata.discount_label}                                     
	                                	{/if}
	                                {/if}
                            	</div>
                            </div>
							{if $widgetdata.generic_type neq 'design'}
                                <span class="availability">Sizes: {if $widgetdata.sizes neq '' && $widgetdata.sizes neq ' '}{$widgetdata.sizes|replace:",":", "}{else}SOLD OUT{/if}</span>
                            {/if}

                        </a>
					</li>
                    {/foreach}
            	</ul>
