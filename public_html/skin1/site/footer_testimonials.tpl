<div id="testimonials-slide">
    <span class="testimonials-text">
    " I really appreciate the website - in fact, I can say that Myntra is the best website for online shopping that I have ever seen. Everything is too good and perfect that nobody can stop themselves from registering and purchasing a product immediately. Well done! "
    <em>- Vivek Kumar, Kolkata</em>
    </span>

    <span class="testimonials-text" style="display:none;">
    " The time taken to deliver the product was excellent and the service was superb. Keep it up! "
    <em>- Praveen Gurjar, Jaipur</em>
    </span>

    <span class="testimonials-text" style="display:none;">
     " I am addicted to Myntra. I enjoy looking at the products and you give amazing Mynt credits which makes me buy more and more. Thank you for all the offers! "
     <em>- Chaitra, Bangalore</em>
    </span>

     <span class="testimonials-text" style="display:none;">
     " It was a great experience shopping with Myntra. I particularly loved the Christmas sale and year clearance sale. Amazing site. Well done."
     <em>- Neeli Ravikumar, Hyderabad</em>
    </span>

     <span class="testimonials-text" style="display:none;">
     " I did my first ever online shopping on Myntra. Had an amazing experience! Have recommended Myntra to my folks. You guys are doing a fab job. Cheers! "
     <em>- Sunny Patil,	Mumbai</em>
    </span>

    <span class="testimonials-text" style="display:none;">
     " Well, what can I say? You promised free door-to-door delivery in seven days and delivered my product in three! Under-promise and Over-deliver. Lovely policy. It's great shopping at Myntra. "
     <em>- Priyank David, Delhi</em>
    </span>

    <span class="testimonials-text" style="display:none;">
     " I am extremely impressed by Myntra . The standard of customer satisfaction is extremely high. I really love your site. Keep up the good job guys! "
     <em>- Arun Babu, Cochin</em>
    </span>

    <span class="testimonials-text" style="display:none;">
     " I ordered a t-shirt from Myntra and the product received by me was genuine, comfortable, wonderfully packed and delivered. I would definitely buy again from Myntra. Thanks! "
     <em>- Kamlesh Joshi, Pune</em>
    </span>

    <span class="testimonials-text" style="display:none;">
     " Your service and your passion for customer satisfaction is excellent. I feel like the customer-Myntra relationship is a long-term one. I hope you continue to excel and make customer satisfaction your business. "
     <em>- Markand Padhya, Unjha</em>
    </span>

    <span class="testimonials-text" style="display:none;">
     " It was a wonderful experience shopping with you. The product was delivered really fast and the product itself is of very fine quality. Thank you! "
     <em>- Harneet Bhangu, Chandigarh</em>
    </span>

</div>
