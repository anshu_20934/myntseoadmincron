<div class="_B.tplmenu-bar clearfix">
	<ul class="left menu-list last clearfix">
		<li class="menu-bar-item no-js">
			<a class="menu-bar-link" href="{$http_location}/men"><span>MEN</span></a>
			<div class="hover-block hide">
				<div class="menu-section-left last left">
					<div class="menu-title">CATEGORIES</div>
					<div class="menu-links">
						{foreach key=display_category item=count from=$mens_display_categories}
						{if $display_category|lower neq '_empty_'}
    					<a href="{$http_location}/{myntralink search_parameter="global_attr_gender=men&display_category_facet=$display_category"}" class="no-decoration-link">{$display_category|capitalize}</a>
    					{/if}
    					{/foreach}
					</div>
				</div>
				<div class="menu-section last left">
					<div class="menu-title">BRANDS</div>
					<div class="menu-links">
						{include file="site/top_nav_menu_columns.tpl" limit=9 col_limit=7 brands=$men_brands prefix="men"}
					</div>
				</div>
			</div>
		</li>
		<li class="menu-bar-item no-js">
			<a class="menu-bar-link" href="{$http_location}/women"><span>WOMEN</span></a>
			<div class="hover-block hide">
				<div class="menu-section-left last left">
					<div class="menu-title">CATEGORIES</div>
					<div class="menu-links">
						{foreach key=display_category item=count from=$womens_display_categories}
						{if $display_category|lower neq '_empty_'}
    					<a href="{$http_location}/{myntralink search_parameter="global_attr_gender=women&display_category_facet=$display_category"}" class="no-decoration-link">{$display_category|capitalize}</a>
    					{/if}
    					{/foreach}
					</div>
				</div>
				<div class="menu-section last left">
					<div class="menu-title">BRANDS</div>
					<div class="menu-links">
						{include file="site/top_nav_menu_columns.tpl" limit=9 col_limit=7 brands=$women_brands prefix="women"}
					</div>
				</div>
			</div>
		</li>
		<li class="menu-bar-item no-js">
			<a class="menu-bar-link" href="{$http_location}/kids"><span>KIDS</span></a>
			<div class="hover-block hide">
				<div class="menu-section-left last left">
					<div class="menu-title">CATEGORIES</div>
					<div class="menu-links">
						{foreach key=display_category item=count from=$kids_display_categories}
						{if $display_category|lower neq '_empty_'}
    					<a href="{$http_location}/kids-{if $display_category|lower eq 't shirt' || $display_category|lower eq 't shirts' || $display_category|lower eq 't-shirts' || $display_category|lower eq 't-shirt'}{$display_category|lower|replace:' ':''|replace:'-':''}{else}{$display_category|lower|replace:' ':'-'}{/if}" class="no-decoration-link">{$display_category|capitalize}</a>
    					{/if}
    					{/foreach}
					</div>
				</div>
				<div class="menu-section last left">
					<div class="menu-title">BRANDS</div>
					<div class="menu-links">
						{include file="site/top_nav_menu_columns.tpl" limit=9 col_limit=7 brands=$kids_brands prefix="boy,girl"}
					</div>
				</div>
			</div>
		</li>
		<li class="menu-bar-item no-js">
			<a class="menu-bar-link" href="{$http_location}/brands"><span>BRANDS</span></a>			
			<div class="hover-block hide">
				<div class="menu-section" style="padding-left:0px;width:100%;">
					<div class="menu-title">BRANDS</div>
					<div class="menu-links" style="padding-left:5px;">
						{include file="site/top_nav_menu_columns.tpl" limit=10 col_limit=8 brands=$global_brands prefix=""}
					</div>
				</div>
			</div>
		</li>
<!--		<li class="menu-bar-item no-js">-->
<!--			<a class="menu-bar-link" href="{$http_location}/sports"><span>SPORTS</span></a>-->
<!--			<div class="hover-block hide">-->
<!--			sports-->
<!--			</div>-->
<!--		</li>-->
	</ul>	
	{if $saleClearanceEnabled}
	<div class="sale-link-block left">
		<a class="sale-link corners" onclick="gaTrackEvent('top_menu','sale_clearance','')" href="{$http_location}/{$saleClearanceURL}">SALE</a>
    </div>
    {/if}
	<div class="right search-box">
			<form action="{$http_location}/" method="post" id="menu_search_form" onsubmit="return menuSearchFormSubmit();">
	    	    <input type="text" class="search toggle-text blurred corners-tl-bl" name="query" value="Search for Brands, Products in the Best Sports/Casual collection" id="search_query" tabindex="10">
	       		<input type="button" value="Go" id="menu_search" class="orange-bg corners-tr-br search-btn" tabindex="11">
	    	</form>
   	</div>
</div>