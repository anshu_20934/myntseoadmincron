<ul id="nav3" class="nav3 clearfix">
    {foreach key=indexA item=itemsA from=$topNavigationWidgetV2 name=topNavigationWidgetV2Loop}
    <li>
        <a href="{$itemsA.link_url}" {if $smarty.foreach.topNavigationWidgetV2Loop.last}target="_blank"{/if}>{$itemsA.link_name}</a>
        <span class="line"></span>
        {if $isMegaMenuEnabled}
        {if count($itemsA.child) gt 0}
        <ul class="nav3-toplevel">
            {foreach key=indexB item=itemsB from=$itemsA.child}
            <li>
                <h3><a href="{$itemsB.link_url}">{$itemsB.link_name}</a></h3>
                {if count($itemsB.child) gt 0}
                <ul>
                    {foreach key=indexC item=itemsC from=$itemsB.child}
                    <li><a href="{$itemsC.link_url}">{$itemsC.link_name}</a></li>
                    {/foreach}
                </ul>
                {/if}
            </li>
            {/foreach}
        </ul>
        {/if}
        {/if}
    </li>
    {/foreach}
</ul>
