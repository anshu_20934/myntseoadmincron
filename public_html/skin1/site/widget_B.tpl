
  {literal}
	{RECENT_WIDGET_PLACEHOLDER}
    {/literal}
{foreach name=widgetloop item=widget from=$scrollableWidgets}
<script type="text/javascript">
	lists_image_srcs['{$widget.nameclean}']= new Object();
</script>
<div class="category  clearfix">
<!--	<a class="widget-title-link corners-tl-tr" href="javascript:void(0)"></a>-->
	<h4 class="h4">{$widget.name}</h4>
	<!-- scrollable-box -->
	<div class="scrollable-box clearfix last">
		<!-- "previous page" action -->
		<a class="prevPage browse left"></a>
		<!-- root element for scrollable -->
		<div class="scrollable {if $cashback_displayOnHomePage neq '1'} without_cashback {/if}">
			<!-- root element for the items -->
			<ul class="items items_{$widget.nameclean}">
				{assign var=i value=1}
				{foreach name=widgetdataloop item=widgetdata from=$widget.data}
				{if $widgetdata.product neq '' && $widgetdata.product neq ' '}
				<li class="item itemsizes" data-id="prod_{$widgetdata.id}">
				<div class="style-details" style="display:none"></div>
					{if $quicklookenabled}
					<span class="quick-look action-btn" data-widget="featured" data-styleid="{$widgetdata.styleid}" data-href="{$widgetdata.landingpageurl}"><span class="quick-look-icon"></span>Quick View</span>
					{/if}
					<div class="new-label"> </div>
					<a href="{$widgetdata.landingpageurl}" class="{if $i lt 6}loaded{else}to-be-loaded{/if}" onclick="_gaq.push(['_trackEvent', 'pdp_click', Myntra.Data.pageName, 'featured']);">
						<div class="item-box">
                                {if $i lt 6}
                                <img id="im_product_img_{$widget.nameclean}_{$widgetdata.styleid}" src="{$cdn_base}/skin1/images/loader_180x240.gif" lazy-src="{$widgetdata.search_image}" class="lazy" height="320" width="240" alt="{$widgetdata.product}">
                                {else}
                                <img id="im_product_img_{$widget.nameclean}_{$widgetdata.styleid}" src="{$cdn_base}/skin1/images/loader_180x240.gif" height="320" width="240" alt="{$widgetdata.product}">
                                <script type="text/javascript">
                                    lists_image_srcs["{$widget.nameclean}"]['im_product_img_{$widget.nameclean}_{$widgetdata.styleid}'] = '{$widgetdata.search_image}';
                                </script>
                                {/if}
                                <div class="item-desc">
                                	<span class="name">{$widgetdata.product|truncate:40}</span>
                                	{if $cashback_displayOnHomePage eq '1'}
                                		{if $widgetdata.discount > 0}
                                    		<div class="op">MRP <span class="strike">{$rupeesymbol}{$widgetdata.price|number_format:0:".":","}</span> <span style="color:#000"> &nbsp; {$rupeesymbol}{math equation="x-y" x=$widgetdata.price y=$widgetdata.discounted_price assign="discount_amount"}{$discount_amount|round|number_format:0:".":","} Off </span></div>
                                    		<div class="dp"><span style="font-weight:normal">{$rupeesymbol}</span>{$widgetdata.discounted_price|number_format:0:".":","}</div>
                                    		{eval var=$widgetdata.discount_label}                                    
                                		{elseif $widgetdata.discount_label}
											<div class="dp"><span style="font-weight:normal">{$rupeesymbol}</span>{$widgetdata.price|round|number_format:0:".":","}</div>
											{eval var=$widgetdata.discount_label}
                            			{else}
                              	    		{if $cashback_gateway_status eq 'on' && $widgetdata.showcashback eq 'true'}
                                        		<div class="op">Effective Price {$rupeesymbol}{math equation="x-y" x=$widgetdata.price y=$widgetdata.cashback assign="effectiveprice"}{$effectiveprice|round|number_format:0:".":","}</div>
                                        		<div class="dpv"><div class="dp"><span style="font-weight:normal">{$rupeesymbol}</span>{$widgetdata.price|round|number_format:0:".":","}</div>
                                            		<span class="cbp">with <b>10%</b></span><span class="cb">Cashback</span>
                                        		</div>                                        
                                    		{/if}
                            			{/if}
									{else}
	                                	{if $widgetdata.discount > 0}
	                                    	<div class="dp left"><span style="font-weight:normal">{$rupeesymbol}</span>{$widgetdata.discounted_price|number_format:0:".":","}</div> <div class="op"><span class="strike" style="float:left;">{$widgetdata.price|number_format:0:".":","}</span></div>           	
											{eval var=$widgetdata.discount_label}	 										
	                                	{else}
											<div class="dp"><span style="font-weight:normal">{$rupeesymbol}</span>{$widgetdata.price|round|number_format:0:".":","}</div>
											{eval var=$widgetdata.discount_label}
	                                	{/if}
                             		{/if}
                                </div>
                    	</div>
                		<span class="availability">Sizes: {if $widgetdata.sizes neq '' && $widgetdata.sizes neq ' '}{$widgetdata.sizes|replace:",":", "}{else}SOLD OUT{/if}</span>
                	</a>
				</li>
				{/if}
				{assign var=i value=$i+1}
				{/foreach}
			</ul>
		</div>
		<!-- "next page" action -->
		<a id='{$widget.nameclean}' class="nextPage browse right"></a>
    </div>
</div>
{/foreach}

