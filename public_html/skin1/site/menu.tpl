{if $abHeaderUi eq 'v3'}
    {include file="site/menu_B.tpl"}
    {else}
    {include file="site/menu_A.tpl"}
{/if}

{if $abSplash eq 'fblogin'}
<div id="lb-splash-min" class="lightbox splash-min">
<div class="mod">
    <div class="hd">
        <img alt="Myntra Logo" src="{$cdn_base}/skin1/myntra_images/logo.png" width="150" height="34">
        <h2>Welcome!</h2>
    </div>
    <div class="bd clearfix">
            <button type="button" class="action-btn btn-fb-connect" data-referer="{$mrp_referer}">
                <span class="ico-fb-small"></span> Connect with Facebook &raquo;</button>
            <div class="err err-fb"></div>
        <p>
            Sign up through Facebook and get <strong class="price">{$rupeesymbol}{$mrp_fbCouponValue}</strong> 
            worth of <strong>Mynt Credits</strong> for your first purchase.
        </p>
    </div>
</div>
</div>
{/if}
