<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
{config_load file="$skin_config"}
<html xmlns:og="http://opengraphprotocol.org/schema/">
	<head>
		<base href="{$baseurl}">
		{include file="site/header.tpl"}
		{literal}
			<style>
		
				.form-screen{ 
					border:0px solid red; 
					margin:0 auto; 
					width:500px !important; 
					margin:0 auto; 
					padding:10px; 
					font-family: Arial, Helvetica, sans-serif;
					font-size:12px;
					text-align:left;
				}
				.form-screen h1{
					color:#333333;
					font-size:18px;
					font-weight:bold;
					margin:30px 0 6px 0;
				}
		
				.reg-users{border:4px solid #CCCCCC;}
		
				.button-img{
					background-image:url("./skin1/images/button-bg.jpg");
					background-position:left top;
					background-repeat:no-repeat;
					height:29px;
					float:left;
				}
				
				.button-img span{
					background-image:url("./skin1/images/button-bg.jpg");
					background-position:right -58px;
					background-repeat:no-repeat;
					margin-left:2px;
					
					height:29px;
					display:block;
				}
				.button-img span a{
					background-image:url("./skin1/images/button-bg.jpg");
					background-position:right -29px;
					background-repeat:repeat-x;
					color:#FFFFFF;
					display:block;
					font-family:Arial,Helvetica,sans-serif;
					font-size:16px;
					font-weight:bold;
					height:29px;
					text-decoration:none;
					text-transform:uppercase;
					padding: 0 10px;
					line-height:29px;
					margin-right:2px;
				}
				.button-img span a:hover{
					text-decoration:none;
					color:#000000;
				}
		
				.table-box td{padding:5px; font-family: Arial, Helvetica, sans-serif; font-size:12px;}
				.table-box td strong{
					font-size:18px;
				}
				.table-box td strong span{
					font-size:18px;
					font-weight:normal;
				}
				.table-box th{
					font-size:11px;
					font-weight:bold;
					text-align:left;
				}
				.message{
					font-size:14px;
					text-align:center;
					font-weight:bold;
					font-family: Arial;
					padding-left: 20px;
					padding-right: 20px;
				}
				label.error { float: left; color:#CC3300;font-size:12px;font-weight:bold;vertical-align: top; display:block}
				.couponGenPage{
					background-color:lightgoldenRodYellow;
					border:1px solid #CCCCCC;					 
					padding:20px;
					width:300px;
					margin : 0 0 0 340px;
					}
				.smsbtn {
					background:url("./skin1/images/button-style1.png") repeat-x scroll left top transparent;
					border-bottom:1px solid #BC8318;
					border-right:1px solid #BC8318;
					color:white;
					cursor:pointer;
					font-weight:bold;
					height:25px;
					line-height:30px;
					margin-left:150px;
					margin-right:10px;
					text-align:center;
					text-decoration:none;
					width:87px;
				}
				.smsbtn a, .smsbtn a:visited{color:#FFFFFF;}
			</style>
		{/literal}
	</head>
	<body id="body">
		<div id="scroll_wrapper">
			<div id="container">
	        {	include file="site/top.tpl"}
				<div id="wrapper" >
					{include file="breadcrumb.tpl" }
					<!--put your page content here-->
						<form action="sendCoupon.php" method="post" id="generateCoupon">
							{if $status eq 'couponNotGenerated' || $status eq 'alreadyGenerated'}
								<div class="couponGenPage">
										Dear {$firstName}, <br>Thanks for registering at Myntra! You will receive your coupon code for free mug in an sms.
										<br>
										<div style="margin-top:10px">Please enter your mobile number here
											<input type="text" id="mobileNoTextBox" name="mobileFromForm" class="formtextbox" width="45px" value="{$mobileNumberInTextBox}" onblur="validateMobileNumber();"/>
											<div class="smsbtn" align="right"><span><a onclick="if(validateMobileNumber() == true)$('#generateCoupon').trigger('submit');">Send SMS</a></span></div>
											<span style="color:red; display:none;" id="mobileNoError">Please enter a valid mobile number.</span>
											<span style="color:red; display:none;" id="mobileNoError2">Mobile number should start with +91.</span>
										</div>
								</div>
								<br> <br>
								<input type="hidden" name="method" value="genereateNewCoupon_{$status}" />
							{elseif $status eq 'invalidURL'}
								<div class="couponGenPage">
									This is an invalid URL, please check.
								</div>
								<br> <br>
							{elseif $status eq 'mobileNumberUsed'}
								<div class="couponGenPage">
									Sorry this mobile number has already been used.
								</div>
								<br> <br>
							{elseif $status eq 'expired'}
								<div class="couponGenPage">
									Coupon code generated had a validity of 15 days and is expired now.
								</div>
								<br> <br>
							{elseif $status eq 'alreadyUsed'}
								<div class="couponGenPage">
									Sorry you have already used your free mug coupon.
								</div>
								<br> <br>
							{elseif $status eq 'smsSent'}
								<div class="couponGenPage">
									Your message has been sent, it may take maximum up to 3 minutes to get it delivered. 
									<div style="margin-top:10px">
										<a href='{$http_location}/create-my/White%20Ceramic%20Mug/14' target='_blank'>
											Click here</a> to order your free mug now! Use the given coupon code during checkout.
									</div>
									<div style="margin-top:10px">
										Did not receive the SMS? <span><a onclick="$('#generateCoupon').trigger('submit');"><u>Resend coupon code</u> </a></span>
									</div>
								</div>
							<!-- <div align="center" style="border:0px solid red; margin:0px;overflow:hidden; text-align:center; padding-left:380px;">
									<div class="button-img" style="_width:185px;"><span><a onclick="$('#generateCoupon').trigger('submit');">Resend coupon code</a></span></div>
								</div> -->
								<br> <br>
								<input type="hidden" name="method" value="resendCoupon" />
							{/if}
						</form>
				</div>

			</div>
		</div>
		<div class="divider">&nbsp;</div>
        {include file="site/footer.tpl"}		
	</body>
	{literal}
	<script type="text/javascript">
    	function validateMobileNumber(){
			if ( !(!isNaN($("#mobileNoTextBox").val()) && ($("#mobileNoTextBox").val()).length > 12)) {
				$("#mobileNoError").css("display","block");
				$("#mobileNoTextBox").focus();
				return false;				
			}
			else {
				$("#mobileNoError").css("display","none");
				$("#mobileNoError2").css("display","none");
				return true;
			}
    	}
    </script>
	{/literal}
</html>

