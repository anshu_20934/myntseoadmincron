<div
	style="padding-top: 10px; padding-bottom: 10px; padding-left: 16px; color: #3A3A3A">
	Dear {$user},<br>
	<br>{$style} (Size: {$size}) that you were looking to buy on
	{$req_date} is available in stock now. <br> Go <a href="{$url}">grab it</a>
	right away before it gets sold out!
	<div style="width: 650px; padding: 20px;">
		<span style="display: inline;"><img src="{$search_image}"
			width="180px">
		</span> <span style="width: 450px; display: inline; float: right;"><div
				style="padding-bottom: 10px;">
				<a href="{$url}">{$style}</a>
			</div>
			<div style="padding-top: 5px; padding-bottom: 5px;">
				{if $discount gt 0}
				<span
					style="padding-right: 10px; text-decoration: line-through; color: gray; font-weight: bold; display: inline;">Rs.{$price}</span>{/if}<span
					style="font-weight: bold; color: rgb(189, 26, 0);">Rs.{$finalprice}</span>
			</div>
			{if $discount gt 0}
			<div style="padding-bottom: 15px; padding-top: 5px; display: block">
				<span style="font-weight: bold; color: rgb(0, 169, 0);">You Save
					&nbsp;</span><span
					style="background: none repeat scroll 0% 0% gray; color: white; border-radius: 3px 3px 3px 3px; padding: 2px;">Rs.{$discount}</span>
			</div>
			{/if}
			<div
				style="display: inline; background: orange; color: white; padding: 5px 10px; vertical-align: middle;">
				<a href="{$url}" style="color:white; text-decoration:none; font-weight: bold">BUY NOW &raquo;
				</a>
			</div> </span>
	</div>
	<br>Warm Regards,<br>Myntra Team
</div>
