{* $Id: lowlimit_warning_notification_admin.tpl,v 1.6 2006/03/31 05:51:43 svowl Exp $ *}
{config_load file="$skin_config"}
{include file="mail/html/mail_header.tpl"}
<p />{$lng.eml_lowlimit_warning_message|substitute:"sender":$config.Company.company_name:"productid":$product.productid}

<p />{$lng.lbl_product}: <b>{$product.product}</b>

<p />{$lng.lbl_items_in_stock|substitute:"items":$product.avail}

{include file="mail/html/signature.tpl"}
