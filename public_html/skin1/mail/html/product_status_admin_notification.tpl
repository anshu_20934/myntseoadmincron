{* $Id: signin_admin_notification.tpl,v 1.10 2006/03/31 05:51:43 svowl Exp $ *}
{include file="mail/mail_header.tpl"}

{$lng.eml_signin_admin_notification}

Product Details :
---------------------
Product Name : {$mailproductinfo.product}

Product Description : {$mailproductinfo.descr}

Product Status : {$mailproductinfo.status}



{include file="mail/signature.tpl"}
