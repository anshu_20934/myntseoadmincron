{* $Id: partner_declined.tpl,v 1.3 2006/03/31 05:51:43 svowl Exp $ *}
{include file="mail/html/mail_header.tpl"}

{$lng.eml_dear|substitute:"customer":"`$userinfo.title` `$userinfo.firstname` `$userinfo.lastname`"},<br />
<br />
{$lng.eml_partner_declined}<br />
<br />
{if $reason ne ""}
<b>{$lng.eml_reason}:</b><br />
{$reason}<br />
<br />
{/if}


{include file="mail/html/signature.tpl"}
