{* $Id: send2friend.tpl,v 1.5 2006/03/31 05:51:43 svowl Exp $ *}
{config_load file="$skin_config"}
{include file="mail/html/mail_header.tpl"}
<br/>
{$lng.eml_hello}<br />
<br />
{$lng.eml_send2friend|substitute:"sender":$name}<br />
<hr />

{$lng.eml_click_to_view_product}:<br />
{$url}
{include file="mail/html/signature.tpl"}
