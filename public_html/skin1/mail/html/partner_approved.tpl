{* $Id: partner_approved.tpl,v 1.5 2006/03/31 05:51:43 svowl Exp $ *}
{include file="mail/html/mail_header.tpl"}

{$lng.eml_dear|substitute:"customer":"`$userinfo.title` `$userinfo.firstname` `$userinfo.lastname`"},<br />
<br />
{$lng.eml_partner_approved}<br />
<br />
{$lng.lbl_profile_details}:<br />
{include file="mail/html/profile_data.tpl" userinfo=$userinfo}
<br />

{include file="mail/html/signature.tpl"}
