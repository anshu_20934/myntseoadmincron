{* $Id: lowlimit_warning_notification_admin.tpl,v 1.8 2006/03/31 05:51:43 svowl Exp $ *}
{config_load file="$skin_config"}
{include file="mail/mail_header.tpl"}
{assign var=max_truncate value=$config.Email.max_truncate}{math assign="max_space" equation="x+5" x=$max_truncate}{assign var="max_space" value="%-"|cat:$max_space|cat:"s"}

{$lng.eml_lowlimit_warning_message|substitute:"sender":$config.Company.company_name:"productid":$product.productid}

{$lng.lbl_product}: {$product.product}

{$lng.lbl_items_in_stock|substitute:"items":$product.avail}


{include file="mail/signature.tpl"}
