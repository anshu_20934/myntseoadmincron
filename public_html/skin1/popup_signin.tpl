{literal}
<script type="text/javascript" language="JavaScript 1.2">
function hideErrorDiv()
{
	document.getElementById('errorpopup').style.visibility = 'hidden';
	document.toploginform.username.focus();
}

</script>
{/literal}

<div id="errorpopup" style="visibility:hidden;display:none">
  <div id="overlay"></div>
  <div id="error" style="position:relative; left:450px; top:60px;">
    <div class="wrapper">
      <div class="head"><a href="javascript:hideErrorDiv()">Click to close</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
      <div class="body">
        <p><strong>You have not logged in as yet.</strong><br>

          Please use the login menu on top to log in or <a href="register.php?redirecturl={$referer}">register</a>
          if you are a new user.<br>
          <br>
          <!--<a href="#">Privacy Policy</a> | <a href="#">Legal</a> | <a href="#">Site
          Map</a>--></p>
      </div>
      <div class="foot"></div>
    </div>
  </div>
</div>

