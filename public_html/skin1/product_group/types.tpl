 {assign var=index value=0}

  <div class="form">
  <div class="subhead">
 		   <h1 style='margin-left:4px;font-size:20px;'>{$tableHeading}</h1>
 		 </div>
 		 <div class="text">

 		 <div class="productGroupTitle"><h1>{$group.title}</h1> </div>
 		 <div class="productGroupDescription">{$group.description}</div>
 		   
 		     <div class="serialbanners" style="margin:10px 0;">
			 {if $group.type eq 3}
				  {section name=product loop=$product_types }
						<div class="listbox">	
							<a rel="nofollow" href="{$http_location}/create-my/{$product_types[product].name}/{$product_types[product].id}" class="BlueLink with-tooltip" title="{$product_types[product].description|regex_replace:'/[\r\t\n]/':' '|escape_quotes}" >
								<img id="im_{$smarty.section.product.index}" src="{$cdn_base}/skin1/mkimages/spacer.gif" lazy-src="{$product_types[product].image_t}" class="lazy" alt="create {$product_types[product].name}" title="create {$product_types[product].name}" >
							</a>
							<div class="textbox" style="font-size:1.4em;">			
								<p><a style="font-size:14px;" href="{$http_location}/create-my/{$product_types[product].name}/{$product_types[product].id}" class="BlueLink with-tooltip"  title="{$product_types[product].description|regex_replace:'/[\r\t\n]/':' '|escape_quotes}" >{$product_types[product].name}</a><br><span style="font-size:14px;">(Rs..{$product_types[product].price}&nbsp;)</span></p>
							</div>
							<div style="font-size: 1.4em;margin-top:20px;" class="textbox">
								<p>
									<a href="{$http_location}/create-my/{$product_types[product].name}/{$product_types[product].id}" style="text-decoration:underline;font-family: arial; font-weight:bold;font-size: 11pt; color: rgb(24, 125, 251);">Create</a>
								</p>
							</div>
						</div>							
				  {/section}
			 {else}
 				    {section name=product loop=$product_types }
 								{if $product_types[product].name|count_characters:true > 20}
 									{assign var=type_name value=$product_types[product].name|truncate:20:'...':true|replace:' ':'&nbsp;'}
 								{else}
 									{assign var=type_name value=$product_types[product].name|replace:' ':'&nbsp;'}
 								{/if}

 								{if ($product_types[product].type =='P' || $product_types[product].type =='POS') && $product_types[product].id neq '39' }

 								<div class="listbox">
 										{if $product_types[product].id eq '218'}
 											 <a rel="nofollow" href="cricket-jerseys"  >
 												  <img src="{$product_types[product].image_t}" alt="create {$product_types[product].name}" class="with-tooltip" title="{$product_types[product].description|regex_replace:'/[\r\t\n]/':' '|escape_quotes}">
 											  </a>
 										{else}

 											{if $qryStringKey  eq ''}

 												  <a rel="nofollow" href="{$http_location}/create-my/{$product_types[product].style_name}/{$product_types[product].default_style_id}" class="with-tooltip" title="{$product_types[product].description|regex_replace:'/[\r\t\n]/':' '|escape_quotes}" >
 													  <img src="{$product_types[product].image_t}" alt="create {$product_types[product].name}" title="create {$product_types[product].name}">
 												  </a>

 											 {else}
 												<a rel="nofollow" href="{$http_location}/create-my/{$product_types[product].style_name}/{$product_types[product].default_style_id}?{$qryStringKey}={$qryStringValue}" class="BlueLink with-tooltip" title="{$product_types[product].description|regex_replace:'/[\r\t\n]/':' '|escape_quotes}" >
 												 <img src="{$product_types[product].image_t}" alt="create {$product_types[product].name}" title="create {$product_types[product].name}">
 												</a>
 											{/if}
 										{/if}
 									<div class="textbox" style="font-size:1.4em;">
 										{if $product_types[product].id eq '218'}
 											<p><a href="{$http_location}/cricket-jerseys" class="BlueLink"  {if $product_types[product].description ne ''} onmouseover="javascript:writetxt('{$product_types[product].description|regex_replace:'/[\r\t\n]/':' '|escape_quotes}');" {/if} onmouseout="javascript:writetxt(0);">{$type_name}</a><br><span style="font-size:0.8em;">(Rs..{$product_types[product].price_start}&nbsp;onwards)</span></p>
 										{else}
 										  {if $qryStringKey  eq ''}
 											<p><a href="{$http_location}/create-my/{$product_types[product].style_name}/{$product_types[product].default_style_id}" class="BlueLink with-tooltip" title="{$product_types[product].description|regex_replace:'/[\r\t\n]/':' '|escape_quotes}">{$type_name}</a><br><span style="font-size:0.8em;">(Rs..{$product_types[product].price_start}&nbsp;onwards)</span></p>
 										  {else}
 											 <p><a href="{$http_location}/create-my/{$product_types[product].style_name}/{$product_types[product].default_style_id}?{$qryStringKey}={$qryStringValue}" class="BlueLink with-tooltip"  title="{$product_types[product].description|regex_replace:'/[\r\t\n]/':' '|escape_quotes}" >{$type_name}</a><br><span style="font-size:0.8em;">(Rs..{$product_types[product].price_start}&nbsp;onwards)</span></p>
 										  {/if}
 										{/if}
 									</div>
 								<div style="font-size: 1.4em;margin-top:20px;" class="textbox">
 								<p>
 									{if $product_types[product].id eq '218'}
 										<a href="{$http_location}/cricket-jerseys" style="text-decoration:underline;font-family: arial; font-weight:bold;font-size: 11pt; color: rgb(24, 125, 251);">Create</a>
 									{else}
 										<a href="{$http_location}/create-my/{$product_types[product].style_name}/{$product_types[product].default_style_id}" style="text-decoration:underline;font-family: arial; font-weight:bold;font-size: 11pt; color: rgb(24, 125, 251);">Create</a>
 									{/if}
 								</p>

 								</div>
 							</div>

 							{/if}
 					{/section}
			{/if}
 	            </div>
 		 </div>
  <div class="foot"></div>
 </div>
