<div id="styleareas">
	<table cellpadding="0" cellspacing="0" border="0" class="style-area">
	<tr>
	{section name="area" loop=$area_icons}
		<td align="center">
			{if $area_icons[area].name eq $curr_sel_area}
				<a href="javascript:void(0)" onclick="javascript:changeAreaEx('{$area_icons[area].name}');trackViewChange();">
			<img width="40" height="40" src="{$area_icons[area].area_icon}" alt="" style="border:2px solid orange;padding:3px" id="area_{$area_icons[area].name}">
				</a>
			{else}
				<a href="javascript:void(0)" onclick="javascript:changeAreaEx('{$area_icons[area].name}');trackViewChange();">
				<img width="40" height="40" src="{$area_icons[area].area_icon}" alt="" style="border:2px solid #676767;padding:3px`" id="area_{$area_icons[area].name}">
				</a>
			{/if}
		</td>
	{/section}

	</tr>
	<tr>
	{section name="area" loop=$area_icons}
		<td align="center">
			{if $area_icons[area].name eq $curr_sel_area}
				<a href="javascript:void(0)" onclick="javascript:changeAreaEx('{$area_icons[area].name}');trackViewChange();">
				{$area_icons[area].name}
				</a>
			{else}
				<a href="javascript:void(0)" onclick="javascript:changeAreaEx('{$area_icons[area].name}');trackViewChange();">
				{$area_icons[area].name}
				</a>
			{/if}
		</td>
	{/section}
	</tr>
	</table>
</div>
<script type="text/javascript">
	var currSelectedArea = "{$curr_sel_area}";
</script>
{literal}
	<script type="text/javascript">
		function changeAreaEx_CB(data)
		{
			//check whether the area is customizable or not(has orientation or not)
			var res = data.trim();
			var result = res.split("##");
			if(result[1] == 1){
				showCustomizePanel();				
			}else{
				showProductsPanel();
			}			
			changeDisplayOrientations(windowid,curr_style_id);
			//ajaxCounter--;trackCustProgress();			
		}
		function changeAreaEx(areaname)
		{
			if(currSelectedArea == areaname)
			{
				return;
			}
			$("#area_"+currSelectedArea).css("border","2px solid #dddddd");
			$("#area_"+areaname).css("border","2px solid black");
			currSelectedArea = areaname;
			ajaxCall.get("change_area","./mkchangeselectedarea.php?windowid="+windowid+"&name="+areaname,changeAreaEx_CB);
		}
		//changed funtion
		function showCustomizePanel(){ 	
			$("#customizeTab").show();
			if($("#customizeTab").children().hasClass("current")){
				$("#productsPanel").hide(); 
				$("#customizePanel").show();
			}
			else
				$("#productsTab a").trigger("click"); 
		} 		 		
		function showProductsPanel(){ 			
			if($("#customizeTab").css("display") != 'none') 			
				$("#productsPanel").show(); 			
			$("#customizeTab").hide(); 			
			$("#customizePanel").hide(); 			
			$("#productsTab a").trigger("click"); 		
		}		
		var viewcount=1;
		function trackViewChange(){
			{/literal}
				_gaq.push(['_trackEvent', 'personalize_pdp_{$producttypelabel}', 'switch_view_'+(viewcount++)]);
			{literal}
		}
		//end		
	</script>
{/literal}
