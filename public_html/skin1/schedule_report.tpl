<html>
<head>
{literal}
<script Language="JavaScript" Type="text/javascript" src="../skin1/main/validation_inv.js"></script>
<script Language="JavaScript" Type="text/javascript">
		function FrontPage_Form1_Validator(theform)
		{
         //alert("Please Generate a valid sku first");
         var mailto=theform.mailto;
         var id = document.scheduleForm.scheduletype.value;
         var time=theform.daltime.value;
         var wektime=theform.wektime.value;
         var montime=theform.montime.value;
          if (mailto.value==null||mailto.value == "")
		  {
			alert("Please enter the e-mail adresses");
			mailto.focus();
			return false;
		  }
   
	if(id=="DAL"){
	if (time==null||time== "")
		  {
			alert("Please enter the time");
			time.focus();
			return false;
	        
	}
	return isValidTime(time);
	}
	if(id=="WEK"){
	if (wektime==null||wektime== "")
		  {
			alert("Please enter the time");
			theform.wektime.focus();
			return false;
	        
	}
	return isValidTime(wektime);
	}
	if(id=="MON"){
	if (montime==null||montime== "")
		  {
			alert("Please enter the time");
			theform.montime.focus();
	       return false;
	}
	 return isValidTime(montime);
	}
	 return true;
}

function showtype(){
   //alert("inside show type");
    var id = document.scheduleForm.scheduletype.value;
	if(id=="DAL"){
	document.getElementById("DAL").style.display='block';
	document.getElementById("WEK").style.display='none';
	document.getElementById("MON").style.display='none';
	}
	if(id=="WEK"){
	document.getElementById("DAL").style.display='none';
	document.getElementById("WEK").style.display='block';
	document.getElementById("MON").style.display='none';
	}
	if(id=="MON"){
	document.getElementById("DAL").style.display='none';
	document.getElementById("WEK").style.display='none';
	document.getElementById("MON").style.display='block';
	}
}		
		</script>
{/literal}

<link rel="stylesheet" href="../skin1/mykriti.css" />

</head>
<body id="body">
<div id="scroll_wrapper">
<div id="container">
<div id="display">
<div class="center_column">
<div class="print">
<div class="super">
<p>&nbsp;</p>
</div>
{if $commentSuccess eq "Y"}
<div class="head" style="padding-left: 15px;">
<p>&nbsp;&nbsp;{$reportname}</p>
</div>
<div class="links">
<p></p>
</div>
<div class="foot"></div>

<form action="edit_report.php" method="post"
	onsubmit="return FrontPage_Form1_Validator(this)" language="JavaScript"
	name="scheduleForm">
	<input type="hidden" name="schedule" value="schedule" />
    <input type="hidden" name="reportid" value="{$reportid}" />
    
<table>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td style="text-align: left;">Mail To</td>
		<td style="text-align: left;"><input type="text" style="width: 350px;"
			name="mailto" value='{$mailto}' /></td>
	</tr>
	{if $scheduledate }
	<tr>
		<td style="text-align: left;">View Report Data for</td>
		<td><select name="scheduledate" >
		{foreach from=$scheduledate key=gid item=gname} 
			{if $gid eq $grp}
			<option value="{$gid}" selected>{$gname}</option>
			{else}
			<option value="{$gid}">{$gname}</option>
			{/if}
			 {/foreach}
		</select></td>
	</tr>
	{/if}
	<tr>
		<td style="text-align: left;">Schedule Type</td>
		<td><select name="scheduletype" onchange="javascript:showtype();">
		<option value="" >select</option>
			{foreach from=$group key=gid item=gname} 
			{if $gid eq $grp}
			<option value="{$gid}" selected>{$gname}</option>
			{else}
			<option value="{$gid}">{$gname}</option>
			{/if}
			 {/foreach}
		</select></td>
	</tr>

</table>
<div id="DAL" style="display:none;">
 <tr>
		<td style="text-align: left;">Enter Time in HH:MM:SS Format (Eg.18:23:34)</td>
		<td style="text-align: left;"><input type="text" style="width: 100px;"
			name="daltime" value='' /></td>
	</tr>
  </div>
  <div id="WEK" style="display:none;">
  <table>
  <tr>
  <td style="text-align: left;font-size:14px;">Select Day Of Week</td>
		<td><select name="dayofweek" ">
		{foreach from=$days key=gid item=gname} 
			<option value="{$gid}" >{$gname}</option>
			{/foreach}
		</select></td>
	</tr>
	<tr>
		<td style="text-align: left;font-size:14px;">Enter Time in HH:MM:SS Format (Eg.18:23:34)</td>
		<td style="text-align: left;"><input type="text" style="width: 100px;"
			name="wektime" value='' /></td>
	</tr>
	</table>
  </div>
  <div id="MON" style="display:none;">
  <table>
  <td style="text-align: left;font-size:14px;">Select Day Of Month</td>
		<td><select name="dayofmonth" ">
		{foreach from=$daysofmonth key=gid item=gname} 
			<option value="{$gid}" >{$gname}</option>
			{/foreach}	
			
		</select></td>
	</tr>
  <tr>
		<td style="text-align: left;font-size:14px;">Enter Time in HH:MM:SS Format (Eg.18:23:34)</td>
		<td style="text-align: left;"><input type="text" style="width:100px;"
			name="montime" value='' /></td>
	</tr>
 </table>
  </div>
  <table>
  <tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
  <tr>
		<td><input type="submit" value="schedule report" /></td>
	</tr>

</table>
</form>

{elseif $commentSuccess eq "schedule"}
<table>
	<tr>
		<td>Report Scheduled Succesfully!!!!!</td>
	</tr>
	<tr>
		<td>Report shall next run on {$nextrun}</td>
	</tr>
	<tr>
		<td style="font-size: .8em; color: red;"><b>Please Click the close
		button below to refresh data on back screen</b></td>
	</tr>
	<tr>
		<td align="center"><input type="button"
			style="height: 20px; width: 50px; background-color: black; color: white;"
			value="Close" onclick="opener.window.open('edit_report.php?action=viewschedules&reportid={$reportid}','null', 'menubar=0,resizable=1,toolbar=0,width=900,height=700');self.close();"></td>
	</tr>
</table>

{/if}</div>
</div>
</div>
</div>
</div>
</body>
</html>
