<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html xml:lang="en" xmlns:fb="http://www.facebook.com/2008/fbml">
<head>
<META http-equiv="Content-Style-Type" content="text/css">
<META http-equiv="Content-Script-Type" content="type/javascript">
<META http-equiv="Content-Type" content="text/html; charset=utf-8">
{include file="site/header.tpl" }
<!--[if lt IE 8]>
    <link rel="stylesheet" href="new-skin-css/ie.css" type="text/css" media="screen, projection"><![endif]-->
{literal}
<style>
.blurred{color:gray}
.survey-email {
font-size: 12px;
margin-top: -6px;
    padding: 0;
    width: 270px;
}
</style>
{/literal}
</head>

<body>
{include file="site/menu.tpl" }
<div class="container clearfix">

    <div style="width:980px;">
    <div style="height:93px; width: 980px;background-image: url({$cdn_base}/skin1/images/welcome-bg.gif); font-family:Arial, Helvetica, sans-serif;font-size:20px;">
    <div style="float:left;padding-top: 30px; margin-left: 140px; width: 600px; height: 40px;">
    Thank you for completing our survey! Here's your gift voucher...
    </div>
    <div style="float:right;padding-top: 10px;width: 206px; margin-right: 10px; font-family:Arial, Helvetica, sans-serif;font-size:11px; color: gray;"  align="center">
    <img src="{$cdn_base}/skin1/images/gift-voucher.gif" width="146" height="63" alt="" title="" /><br>
    *Valid on purchase above Rs. 1500
    </div>
    </div>
    <div style="margin-top: 5px;height: 250px; width: 980px; background-color: #eeeeee; -webkit-border-radius: 5px; -moz-border-radius: 5px;border-radius: 5px; border: 1px; border-style:solid;
    border-color:#c3c3c3; font-family:Arial, Helvetica, sans-serif;font-size:17px; " align="center" >
    {if $show_banner_text eq '0'}
    {else}
        <div style="padding-top: 25px;  width: 980px; height: 40px;">
        {if $banner_text}{$banner_text}
        {else}
        Enter your email ID registered with Myntra.com<br>
        to get your gift voucher of Rs. 500
        {/if}
        </div>
    {/if}



    <div style=" padding-top:15px; height: 30px;" >
	{if $error_msg}
		<div class="error " style="font-size: 12px;    padding: 0;    width: 700px;">{$error_msg}</div>
	{/if}
	{if $success_msg}
		{$success_msg}
	{else}
	
    <form action="{$http_location}/survey_coupon.php" method="post" id="survey_form">
	<div>
    <input id="emailid" name="emailid" type="text" size="50" value="Your Myntra e-mail address"  align="center" style="font-size: 12px;height: 25px; border: 1px solid gray;" class="blurred">
	<div class="survey-email error hide" ></div>
	</div>
    <div >
    <a href="javascript:submitSurveyForm()" style="border:none;"><img src="{$cdn_base}/skin1/images/submit-button-png.png" height="47" width="127" style="border: none;" /></a>
    </div>

    </form>
	{/if}

    </div>
    </div>
    </div>
</div>    
	<div class="divider">&nbsp;</div>
	{include file="site/footer.tpl"}
{literal}
<script>
$(document).ready(function() {
	$("#emailid").focus(function(){
		var email=$.trim($("#emailid").val());
		if(email  == 'Your Myntra e-mail address')
		{
			$("#emailid").val('');
			$("#emailid").removeClass("blurred");
		}
	});
	$("#emailid").blur(function(){
                var email=$.trim($("#emailid").val());
                if(email  == '')
		{
                        $("#emailid").val('Your Myntra e-mail address');
			$("#emailid").addClass("blurred");
		}
		else
		{
			validateEmail()
		}
        });
});
var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
function validateEmail()
{
	var error=false;
	var emailVal=$.trim($("#emailid").val());
	if(emailVal == "" || !emailReg.test(emailVal)){
    		error=true;
    		$(".survey-email").text("Enter a valid email address.").slideDown();
    	}
	else $(".survey-email").slideUp();
	return error;
}
function submitSurveyForm()
{
	if(!validateEmail())
		$("#survey_form").trigger("submit");
}
</script>
{/literal}

</body>
</html>
