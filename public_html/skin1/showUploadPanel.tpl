<html>
	<head>
		{literal}
		
		<style type="text/css">
			#uploadpopup
			{
				width:580px;
			}
			#selectImage
			{
				padding:10px;
				width: 560px;
				height: 210px;
				border: 1px solid;
				font:12px Verdana;
				coor: #333;
			}
			#uploadImage
			{
				padding:10px;
				width: 560px;
				height: 190px;
				border: 1px solid;
				font:12px Verdana;
				coor: #333;
			}
			.selectImageHead
			{
				font-weight:600;
				font-size:14px;
			}
			.selectImageDesc
			{
				font-weight:normal;
				color:#666;
			}
			.selectImageDisp
			{
				padding-top:5px;
			}
			.uploadImagePanel
			{
				width:530px;
				overflow:hidden;
				display:block;
			}
		</style>
		{/literal}
	</head>
	<body>
		<div id="uploadpopup">
			<div class="clearfix"><a class="no-decoration-link right" href="javascript:void(0)" onclick="hideUploadImageDivTemp()">Close</a></div>
			<div id="selectImage">
				<div class="selectImageHead">
					My Myntra Images
				</div>
				<div class="selectImageDesc">
					Images that are uploaded by you in the current session. You can select one of them or upload a new image using the Image Upload panel.
				</div>
				<div class="selectImageDisp">
					<iframe src="./getImagesFromGlobalData.php?windowid={$windowid}" frameborder="0" scrolling="no" width="545" height="165">
					</iframe>
				</div>
			</div>
			<div style="padding-top:2px;"></div>
			<div id="uploadImage">
				<div class="selectImageHead">
					Image upload panel
				</div>
				<div class="selectImageDesc">
					Please select the image to be uploaded.
				</div>
				<div class="uploadImagePanel">
					<iframe src="./getImagesUploadPanel.php?windowid={$windowid}" width="530" height="160" frameborder="0" scrolling="no">
					</iframe>
				</div>
			</div>
		<div>
	</body>
</html>
       
