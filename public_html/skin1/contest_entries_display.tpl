<div style="float:right;font-size:0.8em;padding-right:40px;"><p>{if $totalpages > 1}{$paginator}{/if}</p></div>
<div class="clearall"></div>
<div class="serialbanners" style="margin:15px 15px 40px;_margin:15px 15px 0px;"> 
		<div style="font-size:1.2em;color:red;font-weight:bold;font-style:italic;">Note: Winners will receive a mail from Myntra on the 24th of Feb regarding the prize details.</div>
	<!--showing winner info statically-->
		<div  class="listbox" style="height:215px;_height:180px;">
			<div style="font-size:1em;color:#c2c2c2;font-weight:bolder;margin-bottom:2px;">1<sup>st</sup> prize</div>
			<div  id="imgBox_m1" class="imagebox" style="cursor:pointer;">
				<a href="javascript:openNewWindow('/images/green_design_contest/go green_1.jpg');" onmouseover="javascript:setBorder('m1');" onmouseout="javascript:resetBorder('m1');">
					<div class="imagebox" style="background-image:url('{$http_location}/images/green_design_contest/140/go green_1.jpg');background-repeat:no-repeat;height:150px;width:150px;"></div>
				</a>
			</div>			
			<div class="textbox" style="font-size:1em;text-align:left;">
				<span style="font-weight:bold;color:#c2c2c2;padding-left:4px;">Prachi</span><br/>
				<span style='padding-left:5px;color:#3FA9FF;'>Maratha Mandal Arts and Commerce College, Belgaum</span>		
			</div>
		</div>
		
		<div  class="listbox" style="height:215px;_height:180px;">
			<div style="font-size:1em;color:#c2c2c2;font-weight:bolder;margin-bottom:2px;">2<sup>nd</sup> prize</div>
			<div  id="imgBox_m2" class="imagebox" style="cursor:pointer;">
				<a href="javascript:openNewWindow('/images/green_design_contest/go green_2.jpg');" onmouseover="javascript:setBorder('m2');" onmouseout="javascript:resetBorder('m2');">
					<div class="imagebox" style="background-image:url('{$http_location}/images/green_design_contest/140/go green_2.jpg');background-repeat:no-repeat;height:150px;width:150px;"></div>
				</a>
			</div>			
			<div class="textbox" style="font-size:1em;text-align:left;">
				<span style="font-weight:bold;color:#c2c2c2;padding-left:4px;">Priya John</span><br/>
				<span style='padding-left:5px;color:#3FA9FF;'>Avinashilingam College, Coimbatore</span>		
			</div>
		</div>
</div>
<div class="clearall"></div>
<hr/>
<div class="serialbanners"> 
		<div style="font-size:1em;color:#0A4369;font-weight:bold;font-style:italic;">Designs selected for consolation prizes</div>
	{section name=num loop=$contest_entries}
		<div  class="listbox" style="height:200px;">
			<div  id="imgBox_{$contest_entries[num].map_id}" class="imagebox" style="cursor:pointer;">
				<a href="javascript:openNewWindow('{$contest_entries[num].image}');" onmouseover="javascript:setBorder('{$contest_entries[num].map_id}');" onmouseout="javascript:resetBorder('{$contest_entries[num].map_id}');">
					<div class="imagebox" style="background-image:url('{$http_location}/{$contest_entries[num].image_140}');background-repeat:no-repeat;height:150px;width:150px;"></div>
				</a>
			</div>			
			<div class="textbox" style="font-size:1em;text-align:left;">
				<span style="font-weight:bold;color:#c2c2c2;padding-left:4px;">{$contest_entries[num].firstname}</span><br/>
				<span style='padding-left:5px;color:#3FA9FF;'>{$contest_entries[num].company}</span>		
			</div>
		</div>
	{*sectionelse*}	
	<!--<p style="font-weight:bold;color:#c2c2c2;height:100px;text-align:center;padding-top:100px;">No approved "Green contest designs" to display. </p>	-->
	{/section}
		
</div>
<div class="clearall"></div>
<div style="float:right;font-size:0.8em;padding-right:40px;"><p>{if $totalpages > 1}{$paginator}{/if}</p></div>