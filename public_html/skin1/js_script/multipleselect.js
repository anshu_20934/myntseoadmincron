$(document).ready(function() {
	var selected = new Array();

	$('select').mouseover(function() {
		if (this.multiple == true) {
			for (var i=0,a=0;i<this.options.length;i++) {
				if (this.options[i].selected == true) {
					selected[a] = this.options[i].value;
					a++;
				}
			}
		}
	});

	// safe them when you click the mouse
	$('select').click(function() {
		// make sure it's a multiple select
		if (this.multiple == true) {
			for(var i=0;i<selected.length;i++) {
				for(var a=0;a<this.options.length;a++) {
					if (selected[i] == this.options[a].value && this.options[a].selected == true) {
						this.options[a].selected = false;
						selected.splice(i,1);
					} else if (selected[i] == this.options[a].value) {
						this.options[a].selected = true;
					}
				}
			}

			// load all selected options in array when the mouse pointer hovers the select box
			for (var i=0,a=0;i<this.options.length;i++) {
				if (this.options[i].selected == true) {
					selected[a] = this.options[i].value;
					a++;
				}
			}

			if(window.showFilterNames) {
				window.showFilterNames(selected);
			}
		}
	});
});