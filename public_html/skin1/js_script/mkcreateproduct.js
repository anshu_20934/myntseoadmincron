var updater;
function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}

function initScrollLayer() {
  // arguments: id of layer containing scrolling layers (clipped layer), id of layer to scroll,
  // if horizontal scrolling, id of element containing scrolling content (table?)
  var wndo = new dw_scrollObj('wn', 'lyr1', 't1');

  // pass id('s) of scroll area(s) if inside table(s)
  dw_scrollObj.GeckoTableBugFix('wn');
}

function loadingBar(value1, value2, value3, value4, value5, value6,status,previd)
{
    document.getElementById("baseimage").innerHTML ="<img src='./images/image_loading.gif' >";
	setTimeout("getCustomizationAreaImageForPreview("+value1+", "+value2+", "+value3+", " +value4+", "+ value5+", "+value6+",'"+status+"','"+previd+"')",1000);
	/*var upID = new String(Math.floor(Math.random() * 65535));
	var curDate = new Date(); 	
	upID += new String(curDate.getMilliseconds());		
    
	//added this to insert the uploadText box and UploadId dynamically
	new Insertion.Before('submitID1', '<input type=\"hidden\" id="hidUploadID" class="upcls" name=\"UPLOAD_IDENTIFIER\" id=\"UPLOAD_IDENTIFIER\" value=\"' + upID + '\" />');		
	new Insertion.Before('submitID1', '<input type=\"file\" class="GtextBox" id=\"uploadedImage\" name=\"uploadedImage[]\" onchange=\"addFormInput()\" value=""/><br>');		
	
	//This observes the content of the Iframe, as soon as the Upload progress is finished, it invokes method to stop Upload progress.
	Event.observe('uploadTargetID', 'load', uploadMonitorStop);*/
}
//below method stops the Uploader and hides the progress bar after the upload is complete
/*function uploadMonitorStop() {
	// stop updater manually
    if (typeof updater != 'undefined') {
		updater.stop();
		updater = null;
	}							        
	$('statusText').update('Upload finished!');
    $('progressBar').style.width = '100%';		  
    hideFileUploadProgress();
	var url = "/myntra/mkcreateproductpreview.php?";
	showPostUpload(url);    
    return false;
}*/

function showPopupDiv(){
	document.getElementById("stylepopup").style.visibility = "visible";
	document.getElementById("stylepopup").style.display = "";
}
function hideProductStyleDiv(){
	document.getElementById("stylepopup").style.visibility = "hidden";
	document.getElementById("stylepopup").style.display = "none";
}

function showMessagePopup()
{
	document.getElementById('messagepopup').style.visibility = 'visible';
	document.getElementById('messagepopup').style.display = '';
}
function hideMessageDiv()
{
	document.getElementById('messagepopup').style.visibility = 'hidden';
	document.getElementById('messagepopup').style.display = 'none';
}
/* design guide pop up funcion */
function showDesignPopup()
{
	document.getElementById('designguide').style.visibility = 'visible';
	document.getElementById('designguide').style.display = '';
}
function hideDesignDiv()
{
	document.getElementById('designguide').style.visibility = 'hidden';
	document.getElementById('designguide').style.display = 'none';
}
/* end  */
/*function showFileUploadProgress()
{
	document.getElementById('fileUploadProgress').style.visibility = 'visible';
	document.getElementById('fileUploadProgress').style.display = '';
}
function hideFileUploadProgress()
{
	document.getElementById('fileUploadProgress').style.visibility = 'hidden';
	document.getElementById('fileUploadProgress').style.display = 'none';
}
function hideImageSuccessDiv()
{
	document.getElementById('imageuploadpopup').style.visibility = 'hidden'; 
	document.getElementById('imageuploadpopup').style.display = 'none'; 
}*/

/*
* Function to validate the corporate product name and customization status
*/
function validateCorporateDesign(cstatus, from)
{
	var error = false;
	if(cstatus == 'notcustomized')
	{
		alert("You can not add blank product, Please add your design.");
		var error = true;
		return false;
	}
	if(document.getElementById('designname').value == '')
	{
		alert("Please enter product name");
		document.getElementById('designname').focus();
		var error = true;
		return false;
	}
	if(error == false)
	{
		document.getElementById('productname').value = document.getElementById('designname').value;
		document.getElementById('description').value = document.getElementById('designdescription').value;
		//document.getElementById('referredTo').value = 'mkrequest';
		submitForm(from);
	}

}

/*
* Function to validate the corporate suggestion for their designs
*/
function validateSuggestions(from)
{
	var error = false;
	if(document.getElementById('designsuggestion').value == '')
	{
		alert("Please enter your suggestion for your design");
		document.getElementById('designsuggestion').focus();
		var error = true;
		return false;
	}
	if(error == false)
	{
		document.getElementById('hidesuggestion').value = document.getElementById('designsuggestion').value;
		submitsuggestionForm(from);
	}
}
function submitsuggestionForm(referredTo)
{
	(document.getElementById("suggestTo")).value = referredTo;
    var x = document.getElementById("suggestform");
    (document.getElementById("suggestform")).submit();
}