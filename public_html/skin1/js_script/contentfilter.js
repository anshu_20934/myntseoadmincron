function checkEmail(emField){

//var fieldValue = emField.value  // store field's value in variable, "fieldValue"
var fieldValue = emField;

	if(fieldValue != ""){  //if field is not empty
	var atSymbol = 0;
	
		for(var a = 0; a < fieldValue.length; a++){ //loop through field value string
			if(fieldValue.charAt(a) == "@"){ //look for @ symbol and for each @ found, increment atSymbol variable by 1
			atSymbol++;
			}
		}
		
		if(atSymbol > 1){ // if more than 1 @ symbol exists
		//alert("Please Enter A Valid Email Address") // then cancel
		return false;
		}
		
		if(atSymbol == 1 && fieldValue.charAt(0) != "@"){  // if @ symbol was found, and it is not the 1st character in string
		var period = fieldValue.indexOf(".",fieldValue.indexOf("@")+2); //look for period at 2nd character after @ symbol
		var twoPeriods = (fieldValue.charAt((period+1)) == ".") ? true : false; // "."  immediately following 1st "." ? 
				  
		//if period was not found OR 2 periods together OR field contains less than 5 characters OR period is in last position
			if(period == -1 || twoPeriods || fieldValue.length < period + 2 || fieldValue.charAt(fieldValue.length-1)=="."){
			//alert("Please Enter A Valid Email Address") // then cancel
			return false;
			}
		  		  
		}
		else{  // no @ symbol exists or it is in position 0 (the first character of the field)
		//alert("Please Enter A Valid Email Address")
		return false; // then cancel
		}
	}
	else{  // if field is empty
	//alert("Please Enter A Valid Email Address")
	return false;  // then cancel
	}
	
//alert("VALID EMAIL ADDRESS!")
return true;
}

function isURL(urlStr)
{
	if(urlStr==""||urlStr==null)
	{
		return false;
	}
	urlStr=urlStr.toLowerCase();
	var specialChars="\\(\\)><@,;:\\\\\\\"\\.\\[\\]";
	var validChars="\[^\\s" + specialChars + "\]";
	var atom=validChars + '+';
	var urlPat=/^(\w*)\.([\-\+a-z0-9]*)\.(\w*)/;
	var matchArray=urlStr.match(urlPat);
	if (matchArray==null)
	{
		//alert("The URL seems incorrect \ncheck it begins with http://\n and it has 2 .'s");
		return false;
	}
	return true;
}

// Declaring required variables
var digits = "0123456789";
// non-digit characters which are allowed in phone numbers
var phoneNumberDelimiters = "()- ";
// characters which are allowed in international phone numbers
// (a leading + is OK)
var validWorldPhoneChars = phoneNumberDelimiters + "+";
// Minimum no of digits in an international phone no.
var minDigitsInIPhoneNumber = 9;

function isInteger(s)
{   var i;
    for (i = 0; i < s.length; i++)
    {   
        // Check that current character is number.
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) return false;
    }
    // All characters are numbers.
    return true;
}

function stripCharsInBag(s, bag)
{   var i;
    var returnString = "";
    // Search through string's characters one by one.
    // If character is not in bag, append to returnString.
    for (i = 0; i < s.length; i++)
    {   
        // Check that current character isn't whitespace.
        var c = s.charAt(i);
        if (bag.indexOf(c) == -1) returnString += c;
    }
    return returnString;
}

function checkInternationalPhone(strPhone){
s=stripCharsInBag(strPhone,validWorldPhoneChars);
return (isInteger(s) && s.length >= minDigitsInIPhoneNumber);
}
