/*******Gift message calculation******/

function messageBlock_re(login)
{
	var giftpack_charges = parseFloat(document.getElementById('giftpack_charges').value);
	var amount = parseFloat(document.getElementById('totalAmount').innerHTML);
	
	if(document.getElementById('asgift').checked == true)
	{
		var totAmount = amount + giftpack_charges;
		alert("You have selected the option to send the product as gift. Please make sure that your shipping address written above is correct.");
		document.getElementById('messageblock').style.visibility = 'visible';
		document.getElementById('messageblock').style.display = "";
		document.getElementById('totalAmount').innerHTML = totAmount;
		document.getElementById('giftprice').innerHTML = document.getElementById('giftpack_charges').value;	
		checkOptionEnable(totAmount);
	}
	else
	{
		var totAmount = amount - giftpack_charges;
		document.getElementById('messageblock').style.visibility = 'hidden';
		document.getElementById('messageblock').style.display = "none";
		document.getElementById('totalAmount').innerHTML = totAmount;
		document.getElementById('giftprice').innerHTML = "0.00";	
		checkOptionEnable(totAmount);
	}
	var chkSumKey =  document.getElementById('checksumkey').value;	
	updateCheckSum(parseFloat(document.getElementById("totalAmount").innerHTML),chkSumKey);

}


function messagBlock(login)
{
	var giftpack_charges = document.getElementById('giftpack_charges').value;
	var amount = document.getElementById('Amount').value;
   if(document.getElementById('asgift').checked == true)
   {
	   alert("You have selected the option to send the product as gift. Please make sure that your shipping address written above is correct.");
	document.getElementById('messageblock').style.visibility = 'visible'; 
	document.getElementById('messageblock').style.display = "";

	if(document.getElementById("isexistref").value == 1)
	{
		if(document.getElementById("refcheck").checked == true) 
		{
			if(amount != "0.00")
			{
				var totalamount = parseFloat(amount) + parseFloat(giftpack_charges);
				document.getElementById('Amount').value = numberformat(totalamount,2);
			}
			else
			{
				var totalamount = amount;
			}

			var refamount = document.getElementById('refamt').value;
			var refdiscount = document.getElementById('hideref').value;
			if(refamount != "0.00")
			{
				var refamount = parseFloat(refamount) - parseFloat(giftpack_charges);
				var refdiscount = parseFloat(refdiscount) + parseFloat(giftpack_charges);
			}
			document.getElementById('refamt').value = refamount;
			document.getElementById('hideref').value = refdiscount;
			
			document.getElementById("refamtdiv").innerHTML=" Rs. "+numberformat(refamount,2);
			document.getElementById("refdiscount").innerHTML=" Rs. "+numberformat(refdiscount,2);
		}
		else
		{
			var totalamount = parseFloat(amount) + parseFloat(giftpack_charges);
			document.getElementById('Amount').value = numberformat(totalamount,2);
		}
		document.getElementById('totalamtafterdeduction').innerHTML = "Rs."+numberformat(totalamount,2);
	}
	if(document.getElementById("isexistref").value == 0)
	{
		var totalamount = parseFloat(amount) + parseFloat(giftpack_charges);
		document.getElementById('Amount').value = numberformat(totalamount,2);
		document.getElementById('totalamtaftergiftpack').innerHTML = "Rs."+numberformat(totalamount,2);
	}

	document.getElementById('giftamount').value = giftpack_charges;
	document.getElementById('giftprice').innerHTML = giftpack_charges;
	
	var chkSumKey =  document.getElementById('checksumkey').value;
	updateCheckSum(document.getElementById("Amount").value,chkSumKey);
   }
   else
   {
	document.getElementById('messageblock').style.visibility = 'hidden'; 
	document.getElementById('messageblock').style.display = "none";

	if(document.getElementById("isexistref").value == 1)
	{
		if(document.getElementById("refcheck").checked == true) 
		{
			var refamount = document.getElementById('refamt').value;
			var refdiscount = document.getElementById('hideref').value;
			if(refamount != "0.00")
			{
				var refamount = parseFloat(refamount) + parseFloat(giftpack_charges);
				var refdiscount = parseFloat(refdiscount) - parseFloat(giftpack_charges);
			}
			
			document.getElementById('refamt').value = refamount;
			document.getElementById('hideref').value = refdiscount;
			
			document.getElementById("refamtdiv").innerHTML=" Rs. "+numberformat(refamount,2);
			document.getElementById("refdiscount").innerHTML=" Rs. "+numberformat(refdiscount,2);
		}
	}
	
	if(amount != "0.00"){
		var totalamount =parseFloat(amount) - parseFloat(giftpack_charges);
	}
	else{
		var totalamount = amount;
	}

	
	document.getElementById('Amount').value = numberformat(totalamount,2);
	document.getElementById('giftamount').value = "0.00";
	document.getElementById('giftprice').innerHTML = "Rs.0.00";

	if(document.getElementById("isexistref").value == 1)
	{
		document.getElementById('totalamtafterdeduction').innerHTML = "Rs."+numberformat(totalamount,2);
	}
	if(document.getElementById("isexistref").value == 0)
	{
		document.getElementById('totalamtaftergiftpack').innerHTML = "Rs."+numberformat(totalamount,2);
	}

	var chkSumKey =  document.getElementById('checksumkey').value;
	updateCheckSum(document.getElementById("Amount").value,chkSumKey);
   }
			var shippingRate = parseFloat(document.getElementById("shippingrate").value);
			var giftpack_charges = 0;
			var subtotal = parseFloat(document.getElementById("finalcostwithoutshippingandgift").value);
			if(document.getElementById('asgift').checked == true){
				giftpack_charges = parseFloat(document.getElementById("giftpack_charges").value);
			}
			else{
				giftpack_charges = 0;
			}
			document.getElementById("totalamtaftergiftpackwodiscount").innerHTML = "Rs." + numberformat(shippingRate + giftpack_charges + subtotal,2);
}

/**********End Gift message calculation******/

/**********Referral amount calculation**********/

function deductRefAmount(refamt, totamount,vat,shiprate)
{
	
	if(document.getElementById("refcheck").checked == true) 
	{
		
		if(parseFloat(refamt) > parseFloat(totamount)){
			var refdiscount = parseFloat(refamt) - parseFloat(totamount);
			var totalamount = 0;
			var refamt = parseFloat(refamt) - parseFloat(refdiscount);
			document.getElementById("refamt").value=numberformat(refdiscount,2);
			document.getElementById("refamtdiv").innerHTML=" Rs. "+numberformat(refdiscount,2);
			document.getElementById("refdiscount").innerHTML=" Rs."+numberformat(totamount,2);
			document.getElementById("hideref").value=numberformat(totamount,2);
		}else if(parseFloat(refamt) < parseFloat(totamount)){
			var refdiscount = parseFloat(refamt);
			var totalamount = parseFloat(totamount) - parseFloat(refamt);
			var refamt = "0.00";
			document.getElementById("refamt").value=numberformat(refamt,2);
			document.getElementById("refamtdiv").innerHTML=" Rs. "+numberformat(refamt,2);
			document.getElementById("refdiscount").innerHTML=" Rs. "+numberformat(refdiscount,2);
			document.getElementById("hideref").value=numberformat(refdiscount,2);
		}else{
			var refdiscount = refamt;
			var totalamount = "0.00";
			var refamt = "0.00";
			document.getElementById("refamt").value = numberformat(refamt,2);
			document.getElementById("refamtdiv").innerHTML = " Rs. "+numberformat(refamt,2);
			document.getElementById("refdiscount").innerHTML=" Rs. "+numberformat(refdiscount,2);
			document.getElementById("hideref").value=numberformat(refdiscount,2);
		}
		
	    document.getElementById("totalamtafterdeduction").innerHTML= "";
		document.getElementById("totalamtafterdeduction").innerHTML = " Rs. "+numberformat(totalamount,2);
		document.getElementById("Amount").value = numberformat(totalamount,2);
		
		var chkSumKey =  document.getElementById('checksumkey').value;
		/* update checksum value */
		updateCheckSum(document.getElementById("Amount").value,chkSumKey)
		
	}
	if(document.getElementById("refcheck").checked == false) 
	{
		document.getElementById("refamt").value = refamt;
		var giftamount = document.getElementById('giftamount').value;
		if(document.getElementById('codoptioncount').value == 1)
			var codoptioncount = '100.00';
		else 
			var codoptioncount = '0.00';

		var totalamount  =  parseFloat(document.getElementById("totalamount").value) +  parseFloat(vat) + parseFloat(shiprate) + parseFloat(giftamount) + parseFloat(codoptioncount); 
	    var vatAmount = (numberformat(totalamount,2) * (12.50/100));
		document.getElementById("refamtdiv").innerHTML=" Rs. "+numberformat(refamt,2);
		document.getElementById("refdiscount").innerHTML=" Rs.0.00";
		document.getElementById("hideref").value = "0.00";
	
	    document.getElementById("Amount").value = numberformat(totalamount,2);
		document.getElementById("totalamtafterdeduction").innerHTML= "";
		document.getElementById("totalamtafterdeduction").innerHTML = " Rs. "+numberformat(totalamount,2);
		var chkSumKey =  document.getElementById('checksumkey').value;
		updateCheckSum(document.getElementById("Amount").value,chkSumKey);
		
	}

}


function kotakDiscount(type){
	if(type=='kotak'){
		if(document.getElementById('kotakdiscount').value == 0){
			 /*Load amount to be paid*/
			 var amount = document.getElementById('Amount').value;
			 var totalamount = parseFloat(amount)-(parseFloat(amount)*0.5);
			 document.getElementById('Amount').value = numberformat(totalamount,2);
			 document.getElementById('kotakdiscount').value = 1;
			 
			 if(document.getElementById("isexistref").value == 1){
			    document.getElementById("totalamtafterdeduction").innerHTML= "";
				document.getElementById('totalamtafterdeduction').innerHTML = "Rs."+numberformat(totalamount,2);
			 }
			 
			 if(document.getElementById("isexistref").value == 0){
			    document.getElementById("totalamtaftergiftpack").innerHTML= "";
				document.getElementById('totalamtaftergiftpack').innerHTML = "Rs."+numberformat(totalamount,2);
			 }
			 if(numberformat(totalamount,2)<999.00)
			 	document.getElementById('offer_999').style.display="none";
			 	
			 alert("please see discounted price below");
		}
	}else if(type=='notkotak'){
		if(document.getElementById('kotakdiscount').value == 1){
			 /*Load amount to be paid*/
			 var amount = document.getElementById('Amount').value;
			 var totalamount = parseFloat(amount)*2;
			 document.getElementById('Amount').value = numberformat(totalamount,2);
			 document.getElementById('kotakdiscount').value = 0;
			 
			 if(document.getElementById("isexistref").value == 1){
			    document.getElementById("totalamtafterdeduction").innerHTML= "";
				document.getElementById('totalamtafterdeduction').innerHTML = "Rs."+numberformat(totalamount,2);
			 }
			 
			 if(document.getElementById("isexistref").value == 0){
			    document.getElementById("totalamtaftergiftpack").innerHTML= "";
				document.getElementById('totalamtaftergiftpack').innerHTML = "Rs."+numberformat(totalamount,2);
			 }
			 if(numberformat(totalamount,2)>=999.00)
			 document.getElementById('offer_999').style.display="block";
		}
	}
}
/**********End Referral amount calculation**********/
/** For ammas.com ***/
function sendToAmma(oid,amt)
{
  window.location.href = "https://www.ammas.com/secure/paymentgateway/index.cfm?r=login&bid=12535&orderno="+oid+"&amount="+amt;
}
/**********COD calculation***********/
function cashOnDelivery(cod)
{
	var amount = document.getElementById('Amount').value;
	var paybyoption = document.getElementsByName("payby");
	var hiddenoption = document.getElementById('optioncount').value;
	var optioncount = 0;

	if(document.getElementById("co").checked)
	{
		if(document.getElementById("cash").value == 0)
		{
			document.getElementById('codoptioncount').value = 1;
			if(hiddenoption == 1)
			document.getElementById('optioncount').value = 0;
			document.getElementById('cash').value = 1;
			document.getElementById('cod').style.visibility = 'visible'; 
			document.getElementById('cod').style.display = "";

			if(document.getElementById("isexistref").value == 1)
			{
				if(document.getElementById("refcheck").checked == true) 
				{
					if(amount != "0.00")
					{
						var totalamount = parseFloat(amount) + parseFloat(cod);
						document.getElementById('Amount').value = numberformat(totalamount,2);
					}
					else
					{
						var totalamount = amount;
					}
					var refamount = document.getElementById('refamt').value;
					var refdiscount = document.getElementById('hideref').value;
					if(refamount != "0.00")
					{
						var refamount = parseFloat(refamount) - parseFloat(cod);
						var refdiscount = parseFloat(refdiscount) + parseFloat(cod);
					}
					document.getElementById('refamt').value = refamount;
					document.getElementById('hideref').value = refdiscount;
					
					document.getElementById("refamtdiv").innerHTML=" Rs. "+numberformat(refamount,2);
					document.getElementById("refdiscount").innerHTML=" Rs. "+numberformat(refdiscount,2);
				}
				else
				{
					var totalamount = parseFloat(amount) + parseFloat(cod);
					document.getElementById('Amount').value = numberformat(totalamount,2);
				}
				document.getElementById('totalamtafterdeduction').innerHTML = "Rs."+numberformat(totalamount,2);
			}
			if(document.getElementById("isexistref").value == 0)
			{
				var totalamount = parseFloat(amount) + parseFloat(cod);
				document.getElementById('Amount').value = numberformat(totalamount,2);
				document.getElementById('totalamtaftergiftpack').innerHTML = "Rs."+numberformat(totalamount,2);
			}
		
			var chkSumKey =  document.getElementById('checksumkey').value;
			updateCheckSum(document.getElementById("Amount").value,chkSumKey);
		}
	}
	else if((hiddenoption == 0) && (document.getElementById('codoptioncount').value == 1))
	{
		document.getElementById('optioncount').value = 1; 
		document.getElementById('codoptioncount').value = 0;
		document.getElementById('cash').value = 0;
		document.getElementById('cod').style.visibility = 'hidden'; 
		document.getElementById('cod').style.display = "none";

		if(amount != "0.00")
		{
			var totalamount =parseFloat(amount) - parseFloat(cod);
			document.getElementById('Amount').value = numberformat(totalamount,2);
		}
		else
		{
			var totalamount = amount;
		}

		if(document.getElementById("isexistref").value == 1)
		{
			if(document.getElementById("refcheck").checked == true) 
			{
				var refamount = document.getElementById('refamt').value;
				var refdiscount = document.getElementById('hideref').value;
				if(refamount != "0.00")
				{
					var refamount = parseFloat(refamount) + parseFloat(cod);
					var refdiscount = parseFloat(refdiscount) - parseFloat(cod);
				}
				
				document.getElementById('refamt').value = refamount;
				document.getElementById('hideref').value = refdiscount;
				
				document.getElementById("refamtdiv").innerHTML=" Rs. "+numberformat(refamount,2);
				document.getElementById("refdiscount").innerHTML=" Rs. "+numberformat(refdiscount,2);
			}
			document.getElementById('totalamtafterdeduction').innerHTML = "Rs."+numberformat(totalamount,2);
		}
		if(document.getElementById("isexistref").value == 0)
		{
			document.getElementById('totalamtaftergiftpack').innerHTML = "Rs."+numberformat(totalamount,2);
		}

		var chkSumKey =  document.getElementById('checksumkey').value;
		updateCheckSum(document.getElementById("Amount").value,chkSumKey);
	}
}


/*###########END COD Calculation############*/

var req = null;
function updateCheckSum(amt,chkSumKey)
{
    if(window.XMLHttpRequest){ 
        req = new XMLHttpRequest(); 
    } 
    else if (window.ActiveXObject){ 
        req  = new ActiveXObject("Microsoft.XMLHTTP"); 
    } 

    var formurl="mkupdatechecksum.php";
     
    req.onreadystatechange = function(){
        if (req.readyState == 4)
	    {
	       if(req.responseText == 'S')
	       {
	         //alert(req.responseText)
	        
	       }
	       else if(req.responseText == 'F')
			{
	          //alert(req.responseText)
			}
	
        } 
    };
    req.open('GET',formurl+"?amt="+amt+"&chksumkey="+chkSumKey, true);
    req.setRequestHeader("Content-Type", "text/xml");
    req.send(null);
}

function messagePopup(type)
{
	if(type == "gift")
	{
		document.getElementById('giftpopup').style.visibility = 'visible'; 
		document.getElementById('giftpopup').style.display = "";
	}
	if(type == "payment")
	{
		document.getElementById('paymentoption').style.visibility = 'visible'; 
		document.getElementById('paymentoption').style.display = "";
	}
}
function hidegiftMessageDiv()
{
	document.getElementById('giftpopup').style.visibility = 'hidden';
	document.getElementById('giftpopup').style.display = 'none';
}
function hidepaymentMessageDiv()
{
	document.getElementById('paymentoption').style.visibility = 'hidden';
	document.getElementById('paymentoption').style.display = 'none';
}
