function changeImage(productid,type,image_l,image_s,product,typename)
{

	if($.trim($("#search_"+productid+"_"+type+"_image_a").html()) == '')
	{
		var html='<img title="'+product+' '+typename+'" alt="'+product+' '+typename+'" src="'+image_s+'" style="display: block;" id="search_'+productid+'_'+type+'_image_s" >';
		$("#search_"+productid+"_"+type+"_image_a").html(html);
	}

	$("#search_"+productid+"_"+"men_image").css("display","none");
	$("#search_"+productid+"_"+"women_image").css("display","none");
	$("#search_"+productid+"_"+"kid_image").css("display","none");
	$("#search_"+productid+"_"+type+"_image").css("display","block");
	$("#search_"+productid+"_"+"men_tab").removeClass("selected");
	$("#search_"+productid+"_"+"women_tab").removeClass("selected");
	$("#search_"+productid+"_"+"kid_tab").removeClass("selected");
	$("#search_"+productid+"_"+type+"_tab").addClass("selected");
	$("#search_"+productid+"_result_title_atag").attr("href", $("#search_"+productid+"_"+type+"_image_a").attr("href"));
	//$("#search_"+productid+"_"+type+"_image_l").attr("href",image_l);
	$("#search_"+productid+"_"+type+"_image_s").attr("src",image_s);
	$.panzoom();


}
