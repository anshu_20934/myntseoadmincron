// JavaScript Document
/* Define url for redirecting to server site page*/
var URL = "./modules/affiliates/affiliateRegistrationLogin.php";
var shopBannerPath = 'skin1/mkimages/shopthemes';
var shopMsg =  new Array();
 shopMsg[0] = "Shop with this name already exist.Please try other name.";
 shopMsg[1] = "Congratulation, the shop name is available.";
/*
 * Function Name: getHTTPObject()
 * Purpose: Instantiate object of XMLHttpRequest().
 * Summary: Beauty of this function is, this function instantiate browser independent
 * object of XMLHttpRequest()
 *
*/
var http = null;
function getHTTPObject() {
  try
  {
  	// Firefox, Opera 8.0+, Safari
  	http=new XMLHttpRequest();
  }
  catch (e)
  {
  	// Internet Explorer
  	try{
   			 http=new ActiveXObject("Msxml2.XMLHTTP");
       }
  	catch (e)
    {
	    try
	    {
	      http=new ActiveXObject("Microsoft.XMLHTTP");
	    }
	    catch (e)
	    {
	      alert("Your browser does not support AJAX!");
	      return false;
	    }
    }
  }
}

/* containg all the javascript validation for affiliate section */
function validateForm1(logo)
{
	  
	   var flag  = true;

	   if(document.getElementById('contactname').value == "" )
	   {
		    alert("Please enter contact name.")
			document.getElementById('contactname').focus();
			return false              
	   }
       else if(document.getElementById('company').value == "" )
	   {
		    alert("Please enter company name.")
			document.getElementById('company').focus();
             return false              
	   }
       else if(!echeck(document.getElementById('email').value) ||  document.getElementById('email').value == "" )
	   {
		      document.getElementById('email').focus()
		      return false              
	   }
       else if(document.getElementById('phone1').value == "" )
	   {
		      alert("Please enter phone number.")
			  document.getElementById('phone1').focus();
		      return false              
	   }
      else if(document.getElementById('website').value == ""  ||  !isValidURL(document.getElementById('website').value))
	   {
		      alert("Please enter valid website url.")
			  document.getElementById('website').focus();
		      return false              
	   }
       else if(!validateForm2(logo))
		{
		   return false;
	    }
       else
	   {
		    document.getElementById('step').value = "step3"
			return true;
       }
		
}
/* check if shop is available or not */
function  checkAvailability(obj)
{
	    var shopname  = obj.value
		//	var helpPopupObj;
        if(shopname == "")
	    {
			  alert("Please enter shop name to check the availability. ")
              return false
		}
		else
	    {
			getHTTPObject();
			var url=URL+"?shopname="+encodeURIComponent(shopname)
			http.open("GET",url,true) 
			http.onreadystatechange = function () { 
					if (http.readyState == 4) {
						 document.getElementById("checkStatus").style.visibility = "hidden";
						 document.getElementById("checkStatus").style.display = "none";
						flag = http.responseText
						if(flag == 'S'){
							   	helpPopupObj.showHelpPopupDiv('',shopMsg[0]);
								obj.focus();
								obj.select();
								return false;
						}
						else
						{
							  helpPopupObj.showHelpPopupDiv('',shopMsg[1])
						}
					};
			} 
		  document.getElementById("checkStatus").style.visibility = "visible";
		  document.getElementById("checkStatus").style.display = "";
			http.send(null);
		}
       
}

 function isValidURL(url)
 {
	var RegExp = /^(www\.)?[a-z0-9\-\.]{3,}\.[a-z]{2,3}$/;
    if(RegExp.test(url)){
        return true;
    }else{
        return false;
    }
 }


/* validate form2 of affiliate */
function validateForm2(logo)
{
	   var logoImg = logo
	   if(document.getElementById('title').value == "" )
	   {
		     alert("Please enter the title .")
			 document.getElementById('title').focus();
             return false              
	   }
       else if(logoImg == "")
	  {
		     alert("Please upload your logo .")
			 return false 
	  }
     else
	 {
		      return true;
	 }
}

/* function called while uploading the file */

function uploadFile(fileObj,uploadType)
{
	
	  if (!/(\.(jpg|jpeg|png|gif))$/i.test(fileObj.value))
	  {
		alert("Please upload  (jpg | jpeg | png | gif) image.");
		fileObj.focus();
		//return false;
	  }
      else if(fileObj.value == "")
	  {
	     alert("Please browse an image.")
	     fileObj.focus();
	     //return false
     }
    else
	{
		
		 document.getElementById("action").value = "Upload";
		 if(document.getElementById("step"))
			 document.getElementById("step").value = "default";
		 document.getElementById("uploadType").value = uploadType ;
		 document.affiliatefrm1.submit();
	}
    
}

function confirmAgreementPopup(mode)
{
	  if(!(document.getElementById('agree').checked)  && mode == 'confirm')
	  {
		    alert("Please check the checkbox if you agree with  terms and conditions. ")
            document.getElementById('agree').focus();
			return false;
	  }
	  else if(mode == 'popup')
	  {
		     var httplocation = document.getElementById('http_loc').value
			 window.open(httplocation+'/termconditionpopup.php?case=affpopup','null','width=800,height=700,resizable=1');
	  }
	  else
	  {
		    return true;
	  }
}

function uploadNew(divObj1,divObj2)
{
	  divObj1.style.visibility = "hidden";
	  divObj1.style.display = "none";

	  divObj2.style.visibility = "visible";
      divObj2.style.display = "";
	  
}
function cancelUpload(divObj1,divObj2)
{
	 divObj1.style.visibility = "visible";
	 divObj1.style.display = "";

	  divObj2.style.visibility = "hidden";
      divObj2.style.display = "none";
      if(document.getElementById('chkOwnBanner')!= null)
      {
           document.getElementById('chkOwnBanner').checked = false;
           document.getElementById("showOwnBanner").style.visibility = "hidden";
		   document.getElementById("showOwnBanner").style.display = "none"; 
      }
}
function  newWindow(oid,type)
{
		 window.open(http_loc+'/order_invoice.php?orderid='+oid+'&type='+type,'null', 'menubar=1,resizable=1,toolbar=1,width=800,height=500');
}

var helpPopupObj  =  {
	showHelpPopupDiv : function(obj,msg) {
		document.getElementById("stylepopup").style.visibility = "visible";
		document.getElementById("stylepopup").style.display = "";
		 // If both a message and an object are provided
         if (arguments.length == 2 )
		{
           var str =  "<p>"+msg+"</p>";
		}
		else
		{
			var str =  "<p>"+obj.value+"</p>";
		}
		document.getElementById('helpText').innerHTML = str ;
	},

	   hideHelpPopupDiv :  function(){
		  document.getElementById("stylepopup").style.visibility = "hidden";
		  document.getElementById("stylepopup").style.display = "none";
	   }
  
 };
 var confirmMsgPopUp  =  {
	showMsgPopupDiv : function(obj) {
		document.getElementById("stylepopup").style.visibility = "visible";
		document.getElementById("stylepopup").style.display = "";
		 // If both a message and an object are provided
         if (arguments.length == 2 )
		{
           var str =  "<p>"+msg+"</p>";
		}
		else
		{
			var str =  "<p>"+obj.value+"</p>";
		}
		document.getElementById('helpText').innerHTML = str ;
	},

	   hideHelpPopupDiv :  function(){
		  document.getElementById("stylepopup").style.visibility = "hidden";
		  document.getElementById("stylepopup").style.display = "none";
	   }
  
 };




var validate = {
	      validateContactForm : function (){
			  if(document.getElementById('contactname').value == "" )
	          {
						alert("Please enter contact name.")
						document.getElementById('contactname').focus();
						return false              
			 }
			 else if(document.getElementById('company').value == "" )
			 {
					alert("Please enter company name.")
					document.getElementById('company').focus();
					 return false              
			 }
		     else if(document.getElementById('phone1').value == "" )
		     {
				  alert("Please enter phone number.")
				  document.getElementById('phone1').focus();
				  return false              
		     }
		     else if(document.getElementById('website').value == ""  ||  !isValidURL(document.getElementById('website').value))
		     {
				  alert("Please enter valid website url.")
				  document.getElementById('website').focus();
				  return false              
		     }
             else
				 return true;
			      
	    },

		 validatePersonalForm : function (){
			   if(document.getElementById('title').value == "" )
			   {
					 alert("Please enter the title .")
					 document.getElementById('title').focus();
					 return false              
			   }
               else
				   return true;
			 
		 },
       
         validateShopForm : function(){
			  var shopObj = document.getElementById('shopname');
			  var titleObj   =  document.getElementById('shoptitle')
              var logoObj =  document.getElementById('slogo')
              var errorFlag = false; 
              if(shopObj)
              {
              	  var iChars = "!@#$%^&*()+=-[]\\\';,./{}|\":<>?";
                  if(shopObj.value == "" || (shopObj.value).length <= 3)
	          	  {
 					alert("Please enter shop name with minimum four characters.")
					errorFlag  =  true;	          	  	
	          	  }
	          	  else
	          	  {
	          	      var specialCharList = ''; 	
	          	  	  for (var i = 0; i < shopObj.value.length; i++) {
	  						if (iChars.indexOf(shopObj.value.charAt(i)) != -1) {
	  							specialCharList +=  shopObj.value.charAt(i)+" ";
	  							errorFlag  =  true;	  
	  						}
	  				   }
	  				   if(errorFlag)
	  				       alert ("Your shop name has special characters("+specialCharList+").\n Please remove them and try again.");
	          	  }	
	          	       
			 }	  

             if(errorFlag)
	          {
						shopObj.focus();
						return false              
			 }
			 else if(titleObj.value == "" )
			 {
					alert("Please enter shop title.")
					titleObj.focus();
					 return false              
			 }
	         else if(document.getElementById('chkOwnBanner').checked )
	         {
	         	 var bannerObj = document.getElementById('shopbanner');
	         }
	         else if(document.getElementById('headbanner').value == "")
	         {
	         	 alert("Please select banner or upload your own banner");
			               document.getElementById('headbanner').focus();
			               return false;
	         }
             else
				 return true;
		 },
         validateEditShopForm : function() {
				var titleObj   =  document.getElementById('shoptitle');
				var headBannerObj = document.getElementById('headbanner');
				if(titleObj.value == "" )
				 {
						alert("Please enter shop title.")
						titleObj.focus();
						 return false              
				 }
				 else if((headBannerObj.value == "" && document.getElementById('chkOwnBanner').checked))
				 {
					   alert("Please select banner or upload your own banner");
					   headBannerObj.focus();
						return false;
				 }
				 else
				 {
					 return true;
				 }
		 }
	
};

function removeProducts(mode)
{ 
	   var flag = null
	    if(mode == 'delete')
	   {
			 flag  = confirm('Are you sure ,you want to remove the product from your shop?');
			 if(!flag)
				  return flag;
        }
       document.getElementById('mode').value =  mode;	   
	   document.getElementById('frmadd').submit();
}

function showPreview()
 {
	       var selectedTheme =  document.getElementById("headbanner").value;
	      
			if(selectedTheme == "")
			{ 
				  alert("Please select the theme."); 
				  return false;
			}
			var http_loc = document.getElementById("imgpath").value;
			var bannerImg = "<img src ='"+http_loc+"/"+shopBannerPath+"/"+document.getElementById("bimg_"+selectedTheme).value+"' width='720px' >";
			
			var str =  "<div id='overlay'></div><div id='selectionbig'><div class='wrapperbig'> ";
			 str += "<div class='headbig'> <p><a href=\"javascript:hideDiv('popup');\">";
					
			 str += "<img src='"+http_loc+"/skin1/mkimages/click-to-close.gif' border=\"0\"></a></p></div>";
			 str += "<div class=\"bodybig\"> <div>"+bannerImg+"</div><div class=\"clearall\"></div></div>";
			 str += "<div class=\"footbig\"></div></div></div>";
			
			document.getElementById("popup").innerHTML = str;
			document.getElementById("popup").style.visibility = "visible";
			document.getElementById("popup").style.display = "";
			
   }
   function hideDiv(elmid)
   {
		var x = document.getElementById(elmid);
		x.style.visibility = "hidden";
  }
  function showAndHideBanner()
  {
  	if(document.getElementById('chkOwnBanner').checked )
  	{
	  	document.getElementById("showOwnBanner").style.visibility = "visible";
		document.getElementById("showOwnBanner").style.display = "";
		//document.getElementById("headbanner").options[0].selected = true;
		document.getElementById("headbanner").disabled = true;
  	}
  	else
  	{
  		document.getElementById("showOwnBanner").style.visibility = "hidden";
		document.getElementById("showOwnBanner").style.display = "none";
		document.getElementById("headbanner").disabled = false;
  	}
  }
  
function printDetails()
{
	document.getElementById("printdetails").style.visibility = "visible";
	document.getElementById("printdetails").style.display = "";
}
 
