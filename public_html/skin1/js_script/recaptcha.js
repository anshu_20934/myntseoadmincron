
	function showRecaptcha(element, submitButton, themeName) {
		Recaptcha.create("6Le_oroSAAAAANGCD-foIE8DPo6zwcU1FaW3GiIX", element, {
		theme: themeName,
		tabindex: 0,
		callback: Recaptcha.focus_response_field
		});
		document.getElementById(element).style.visibility = "visible";
	}

	$(window).load(function(){

		$("#feedbackBtn").click(function(){

            $.getScript("http://api.recaptcha.net/js/recaptcha_ajax.js")

			$("#feedback_form").css("display","block");
			$("#feedback-form").css("display","block");
		        $("#part1").css("display","block");
                        $("#part2").css("display","none");
                        $("#part3").css("display","none");
                        $("#part4").css("display","none");
			$("#overlay").css("display","block");
			$("#tell_wrapper").css("display","block");
                        $("#site_feedback_captcha").css("display","none");
			$("#submit_wrapper").css("display","block");
			$("#verify_wrapper").css("display","none");
			$("#back_wrapper").css("display","none");

		});
		$("#tell").click(function(){
			$("#secondPageFlag").attr('value', '1');
			$("#part1").css("display","none");
			$("#tell_wrapper").css("display", "none");
			$("#verify_wrapper").css("display", "block");
			$("#submit_wrapper").css("display", "none");
                        $("#site_feedback_captcha").css("display","block");
			$("#part2").css("display","block");
			$("#back_wrapper").css("display","block");
			showRecaptcha('site_feedback_captcha','verify_wrapper', 'red');
		});
		$("#back_wrapper").click(function() {
			$("#secondPageFlag").attr('value', '0');
			$("#feedback_form").css("display","block");
                        $("#feedback-form").css("display","block");
                        $("#part1").css("display","block");
                        $("#part2").css("display","none");
                        $("#part3").css("display","none");
                        $("#part4").css("display","none");
                        $("#tell_wrapper").css("display","block");
                       $("#site_feedback_captcha").css("display","none");
                        $("#submit_wrapper").css("display","block");
                        $("#verify_wrapper").css("display","none");
			$("#back_wrapper").css("display","none");

		});
		$("#submit_wrapper").click(function(){
			$("#part3").css("display","none");
			$("#part4").css("display","none");
			$("#tell_wrapper").css("display", "none");
			$("#submit_wrapper").css("display", "none");
			$("#verify_wrapper").css("display", "block");
			$("#site_feedback_captcha").css("display","block");
			$("#back_wrapper").css("display","none");
			showRecaptcha('site_feedback_captcha','verify_wrapper', 'red');

		});
		$("#verify-from").click(function(){

			$.ajax({
                                        type: "POST",
                                        url: "site_feedback/site_feedback_post_data.php",
                                        data: $("#feedback_form_submit").serializeArray(),
                                        success: function(msg){
                                        var resp=$.trim(msg);
							$("#feedback_form").css("display","block");
							$("#overlay").css("display","block");
                                                        $("#submit_wrapper").css("display","none");
                                                        $("#verify_wrapper").css("display","none");
							$("#back_wrapper").css("display","none");

                                        if(resp == 'success')
                                                {
                                                        $("#site_feedback_captcha").css("display","none");
				                        $("#part1").css("display","none");
				                        $("#part2").css("display","none");
				                        $("#part3").css("display","block");
				                        $("#part4").css("display","none");
							$("#feedback-form").css("height","100%");
							window.setTimeout(function() {
								$("#feedback_form").hide()
							}, 3000);

                                                } else if (resp == 'fail'){
							Recaptcha.reload();
							$("#recaptcha_instructions_image").html("<div style='color:red;font-weight:bold;font-size:12px'>Please try again<\/div>");
                                                        $("#site_feedback_captcha").css("display","block");
				                        $("#part4").css("display","none");
                                                        $("#verify_wrapper").css("display","block");
                                                //        $("#back_wrapper").css("display","block");
						} else {
						        $("#site_feedback_captcha").css("display","none");
                                                        $("#part4").css("display","block");
                                                        $("#part1").css("display","none");
                                                        $("#part2").css("display","none");
							window.setTimeout(function() {
                                                                $("#feedback_form").hide()
                                                        }, 3000);

						}

                                        }

                                });


		});

		$("#close-feedback-button").click(function(){
			//Recaptcha.reload();
			$("#feedback_form").css("display","none");
			$("#part1").css("display","block");
			$("#part2").css("display","none");
			$("#back_wrapper").css("display","none");

		});

	});