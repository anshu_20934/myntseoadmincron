/* ajaxCall is an object which has a callList to store all latest AJAX calls per type
	eg: callList["preview"] = ajaxObject1;
		callList["change_style"] = ajaxObject2;

	This is to make sure , always one AJAX call per type stays alive by ABORTing the old one of that type
	Imagine a guy clicking "Front" & "Back" continuosuly for 100 times, only the 100th "change_style" should live and all the remaining shud be killed
*/

   $(document).ready(function() {

      $("#ajax_load").ajaxStart(function(){
        $("#preview_image_div").css("opacity",'0.4');
        $("#preview_image_div").css("-moz-opacity",'0.4');
        $(this).show();
     });
    $('#ajax_load').ajaxStop(function() {

        $("#preview_image_div").css("opacity",'');
        $("#preview_image_div").css("-moz-opacity",'');
        $(this).hide();
    });

   });



	
	var ajaxCounter = 0;
	var to = null;
	String.prototype.trim = function() 
	{
		a = this.replace(/^\s+/, '');
		return a.replace(/\s+$/, '');
	}
	var count  = 1;
	var custURL = http_loc + "/previewCustImage.php?";

	/* get customized image & data */
	function getCustomizedImage_CB(data)
	{
		var res = data.trim().split("#");
		$("#cstatus").val(res[0].trim());
		$("#finalimage").attr("src",res[1].trim());
		/*var blurId = document.getElementById("im_message");
		var imageBlur = res[2].trim();
		if( imageBlur == "1")
			blurId.innerHTML = "Image may be blury on the final print.Please upload a larger image or reduce the size of curent image";
		else
			blurId.innerHTML = "";*/
					
		$("#body").css("cursor","auto");
        imageLoaded = true;
		//ajaxCounter--;trackCustProgress();
	}
	function getCustomizedImage(URL)
	{
		var d = new Date();
		URL = URL + "&windowid="+windowid+"&uid="+d.getTime();
		ajaxCall.get("preview",URL,getCustomizedImage_CB);
	}
	
	var limitTextFontsize;
	function getCustomizedImageLimitText(URL,fontsize)
	{
		var d = new Date();
		URL = URL + "&windowid="+windowid+"&uid="+d.getTime();
		limitTextFontsize = fontsize;
		ajaxCall.get("previewLimitTextCust",URL,getCustomizedImageLimitText_CB);
	}
	
	function getCustomizedImageLimitText_CB(data){
		var ajaxURL = custURL + "action=resize&resizeby="+limitTextFontsize;
		getCustomizedImage(ajaxURL);
	}

	function  fitH()
	{
        var ajaxURL = custURL + "action=fith";
		getCustomizedImage(ajaxURL);
	}
	function  fitV()
	{
        var ajaxURL = custURL + "action=fitv"
		getCustomizedImage(ajaxURL);
	}
	function rotate(rotateAngle)
	{	
		var ajaxURL = custURL + "action=rotate&rotateby="+rotateAngle;
		getCustomizedImage(ajaxURL);
	}
	function resize(resizeValue)
	{
		var ajaxURL = custURL + "action=resize&resizeby="+resizeValue;
		getCustomizedImage(ajaxURL);
	}
	function move(direction)
	{
		var ajaxURL = custURL + "action=move&direction="+direction;
		getCustomizedImage(ajaxURL);
	}
	function changeBgColor(color)
	{	
		if(color != ''){
        if(color.indexOf("#") == -1)
       		 var color = rgb2hex(color);
		else
			 var color=  color.replace("#", "");
		}
		var ajaxURL = custURL + "action=changebgcolor&color="+color;
		getCustomizedImage(ajaxURL);
	}
	function changeFontColor(color)
	{
		if(color.indexOf("#") == -1)
			var color = rgb2hex(color);
		else
			var color=	color.replace("#", "")
		var ajaxURL = custURL + "action=changefontcolor&color="+color;
		getCustomizedImage(ajaxURL);
	}
    function changeFontType(font,fontSize)
	{
		if(fontSize != ''){
			var ajaxURL = custURL + "action=changefont&font=./fonts/"+font+".ttf";
			getCustomizedImageLimitText(ajaxURL,fontSize);
			
		}else{
			var ajaxURL = custURL + "action=changefont&font=./fonts/"+font+".ttf";
			getCustomizedImage(ajaxURL);
		}
		
	}
	function rgb2hex(num) 
	{
		var decToHex="";
		var prr = new Array();
		var arr = new Array();
		var numStr = new String();
		numStr = num;
	
		prr = numStr.split("(");
		numStr = prr[1];
		prr = numStr.split(")");
		numStr = prr[0];

		arr = numStr.split(", ");
	
		for(var i=0;i<3;i++)
		{
			var hexArray = new Array( "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F" );
			var code1 = Math.floor(arr[i] / 16);var code2 = arr[i] - code1 * 16;
			decToHex += hexArray[code1];
			decToHex += hexArray[code2];
		}
		return (decToHex);
	}

	function previewCustomizedImage()
	{
		var ajaxURL = custURL + "action=none";
		getCustomizedImage(ajaxURL);
	}
	function changeSelectedObject(type)
	{
		var objectid = "";
		if( type == "image" )
		{
			if( $("#imageTab_a").hasClass("current") )
				return;
			objectid = $("#imageObjectId").val();
		}
		else
		if( type == "text" )
		{
			if( $("#textTab_a").hasClass("current") )
				return;
			objectid = $("#textObjectId").val();
		}
		if( objectid.length != 0)
		{
			var ajaxURL = custURL + "action=changeobject&objectid="+objectid;
			getCustomizedImage(ajaxURL);
		}
		changeCustomizationTemplate(type);
	}

/* change style */
function changeStyle_CB(data)
{
	var res = data.trim();
	var result = res.split("#");
	if( result[0] == "success")
	{
		//previewCustomizedImage();
		if( $("#nocust_label_image").length > 0 )
			$("#nocust_label_image").html(result[1]);
		if( $("#nocust_label_text").length > 0 )
			$("#nocust_label_text").html(result[1]);
		$("#product_desc_right").html(result[1]);
		// commented by vaibhav
		//$("#styleprice").html(result[2]);
		/*if( result[3] != "na")
			document.getElementById("sizechart").href = result[3];*/
		var mergeCheck = result[4];
		if( mergeCheck == 0) // old style custom info didnt merge with new style - reset some fields in the client
		{
			currSelectedArea = "";
		}
		curr_style_id = result[5];
		var disableProductStyle=result[6];
		if(disableProductStyle == 1 ){
			$("#productoptions").hide();
			showDisablePopupDiv();
		}
		else{
			$("#productoptions").show();
			hideDisablePopupDiv();

		}
		changeDisplayAreas();
        if(selectedIndex == 3){
		    changeProductOptions();
            changeOrientationImageDesc();
        }
		if(textLimitation == 1){
			changeOrientationTextLimitations();
			$("#customizeTab").show(); 		
			$("#productsTab a").trigger("click"); 
        }
		changeDisplayStyleTemplates();
		//changeSizeChartInfo();
		changeDescription();
		$("#body").css("cursor","auto");
		//ajaxCounter--;trackCustProgress();
	}
}
//to change font types, sizes and color based on orientation of a style
function changeOrientationTextLimitations()
{
	ajaxCall.get("change_orientation_text_limitations","./mkchangeorientationtextlimitations.php?windowid="+windowid,changeOrientationTextLimitations_CB);
}

function changeOrientationTextLimitations_CB(data){
	var res = data.trim();
	var result = res.split("##");
	
	$("#show_font_to_user").attr('src',result[0]);
	$("#limit_maxchars").val(result[1]);
	$("#font_size_span").html(result[2]);
	$("#limit_font_color_div").html(result[3]);	
}

function changeStyle(styleid)
{
	if( curr_style_id == styleid )
		return;
	ajaxCall.get("change_style","./mkcustomization_changeStyle.php?windowid="+windowid+"&id="+styleid,changeStyle_CB);
}

/* change style templates display */
function changeDisplayStyleTemplates_CB(data)
{
	if( data.trim() == "none" )
	{
		$("#templatesTab").hide();
		$("#templatesPanel").hide().html("");
	}
	else
	{
		$("#templatesTab").show();
		$("#templatesPanel").html(data.trim());
	}
	$("#body").css("cursor","auto");
}
function changeDisplayStyleTemplates()
{
	ajaxCall.get("change_disp_style_templates","./mkchangedisplaystyletemplates.php?windowid="+windowid,changeDisplayStyleTemplates_CB);
}
/* change areas */
function changeDisplayAreas_CB(data)
{
	$("#displayareas").html(data.trim());
	changeDisplayOrientations();
	$("#body").css("cursor","auto");
	//ajaxCounter--;trackCustProgress();
}
function changeDisplayAreas()
{
	ajaxCall.get("display_areas","./mkchangedisplayareaicons.php?windowid="+windowid,changeDisplayAreas_CB);
}
/* change product options */
function changeProductOptions_CB(data)
{
	$("#productoptions").html(data.trim());
	$("#body").css("cursor","auto");
    tb_init('a.thickbox, area.thickbox, input.thickbox');
}
function changeProductOptions()
{
	ajaxCall.get("change_options","./mkchangeproductoptions.php?windowid="+windowid+"&id="+curr_style_id,changeProductOptions_CB);
}

/* change selected orientation */
function changeOrientation_CB(data)
{
	if( data.trim() == "success")
	{
		if( orientationsChanged == true)
			changeCustomizationTemplate("all");
	}
	orientationsChanged = false;
	$("#body").css("cursor","auto");
}
function changeOrientation(orientation)
{
	orientationsChanged = true;
	ajaxCall.get("change_orientation","./mkchangeSelectedOrientation.php?windowid="+windowid+"&name="+orientation,changeOrientation_CB);
}

/* change orientation display */
var orientationsChanged = false;
function changeDisplayOrientations_CB(data)
{
	var res = data.trim();
	$("#orientations").html(res);
	$("#body").css("cursor","auto");
	// change customization template for this orientation
	//changeCustomizationTemplate("image");
	//changeCustomizationTemplate("text");
	previewCustomizedImage();
	orientationsChanged = true;
	if( $("#customizePanel").css("display") == "block" )
		changeCustomizationTemplate("all");
	//ajaxCounter--;trackCustProgress();
}

function changeDisplayOrientations()
{
	ajaxCall.get("display_orientations","./mkchangedisplayorientations.php?windowid="+windowid,changeDisplayOrientations_CB);
}
/*change size chart info*/
function changeSizeChartInfo(){
	ajaxCall.get("change_size_chart_info","./mkchangesizechartdetails.php?windowid="+windowid,changeSizeChartInfo_CB);
}
function changeSizeChartInfo_CB(data){
	var res = data.trim();
	$(".size-chart-tooltip").html(res);

	$(".tabs-size li").mouseover(function(){
		$(".tab_content").hide();
		var id=$(this).children("a").attr("href");
		$(""+id).show();
	});
}
/*change style description in popup*/
function changeDescription(){
	ajaxCall.get("change_description","./mkchangedescription.php?windowid="+windowid,changeDescription_CB);
}
function changeDescription_CB(data){
	var res = data.trim();
	$("#product_style_description").html(res);
	
}
/*change style description in popup*/
function changeOrientationImageDesc(){
	ajaxCall.get("change_orientation_image_description","./mkchangeOrientationImageDesc.php?windowid="+windowid,changeOrientationImageDesc_CB);
}
function changeOrientationImageDesc_CB(data){
	var res = data.trim();
	$("#image_desc_acc_orientation").html(res);

}

/* add or remove image */
function applyImage_CB(data)
{
	var res = data.trim();
	var result = res.split("#");
	if( result[0] == "success")
	{
		previewCustomizedImage();
		window.parent.parent.hideLayer();
		changeCustomizationTemplate("image");
	}	
	$("#body").css("cursor","auto");
}

function applyImage(imgobjid)
{
	ajaxCall.get("add_image","./updateImageInfoInCustArea.php?windowid="+windowid+"&imgobjid="+imgobjid,applyImage_CB);
}

function deleteObject_CB(data)
{
	var res = data.trim();
	if( res == 'success')
	{
		previewCustomizedImage();
		changeCustomizationTemplate("image");
	}
	$("#body").css("cursor","auto");
	
}
function deleteObject()
{
	ajaxCall.get("del_image","./mkDeleteObjectFromCustomization.php?windowid="+windowid,deleteObject_CB);
}

/* add or remove text */
function applyText_CB(data)
{
	var res = data.trim();
	var result = res.split("#");
	if( result[0] == "success")
	{
		previewCustomizedImage();
		changeCustomizationTemplate("text");
	}
	$("#body").css("cursor","auto");
}
function applyText(mode)
{
	if(mode == "rem")
		$("#ipText").val("");
	var text = $("#ipText").val();

    text=text.replace(/\n/g, " ");
    text=text.replace(/  /g, "");
	var params = "text="+escape(text);
	ajaxCall.get("add_text","./updateTextInfoInCustArea.php?windowid="+windowid+"&"+params,applyText_CB);
}
//chage font type
function applyFont(font){

    
}
//change customization template
function changeCustomizationTemplate(obj_type)
{
	if( obj_type == "all" && orientationsChanged == false)
			return;
	var type = "change_template";
	var url = "./mkchangecustomizationtemplate.php?windowid="+windowid+"&obj_type="+obj_type;
	ajaxCall.get(type,url,changeCustomizationTemplate_CB);
	orientationsChanged = false;
}
function changeCustomizationTemplate_CB(data)
{
			var res = data.trim();
			var result = res.split("#");
			//previewCustomizedImage();
			var count = 0;
			var obj_type = result[count++].trim();
			if( obj_type == "all")
			{
				//hide and remove all from panel
				$("#imageTab").hide();
				$("#imageTab_a").removeClass("current");
				$("#imageCustBlock").hide();
				$("#no_img_cust_panel").hide();
				$("#img_cust_panel").hide();
				$("#imagecontrols").hide();

				$("#textTab").hide();
				$("#textTab_a").removeClass("current");
				$("#textCustBlock").hide();
				$("#select_font").hide();
				$("#textcontrols").hide();

				var isImageAllowed = parseInt(result[count++]);
				var isImageAdded = parseInt(result[count++]);
				var imageObjectId = result[count++];
				var baseimagename = result[count++];
				var thumbPath = result[count++];
				var isImageCustomizationAllowed = parseInt(result[count++]);
				var isTextAllowed = parseInt(result[count++]);
				var isTextAdded = parseInt(result[count++]);
				var textObjectId = result[count++];
				var text = result[count++];
				var isTextCustomizationAllowed = parseInt(result[count++]);
                var currentSelectedObjType = result[count++];
                if(currentSelectedObjType == '')currentSelectedObjType='image';
				if( isImageAllowed && isTextAllowed )
				{
					var curr_object = "";
					$("#imageTab").show();
					$("#textTab").show();
                    $("#imageTab_a").removeClass("current");
                    $("#textTab_a").removeClass("current");
                    //show current selected object type
                     $("#"+currentSelectedObjType+"Tab_a").addClass("current");
					 $("#"+currentSelectedObjType+"CustBlock").show();
					if( isImageAdded )
					{
						$("#no_img_cust_panel").hide();	
						$("#img_cust_panel").show();
						$("#baseimagename").html(baseimagename);
						$("#imageObjectId").val(imageObjectId);
						$("#baseimagepath").attr("src",thumbPath);
						if( isImageCustomizationAllowed  )
						{
							$("#imagecontrols").show();
						}
						else
						{
							$("#imagecontrols").hide();
						}
					}
					else
					{
						$("#no_img_cust_panel").show();
						$("#img_cust_panel").hide();
						$("#imagecontrols").hide();
					}
					//Fill text data, but dont show the text block
//					$("#textCustBlock").hide();
					$("#ipText").val(text);
					if( isTextAdded )
					{
						$("#textObjectId").val(textObjectId);
						if( isTextCustomizationAllowed  )
						{
							$("#select_font").show();
							$("#textcontrols").show();
						}
						else
						{
							$("#select_font").hide();
							$("#textcontrols").hide();
						}
					}
					else
					{
						$("#select_font").hide();
						$("#textcontrols").hide();
					}

				}
				else
				if( isImageAllowed )
				{
					$("#imageTab").show();$("#imageTab_a").addClass("current");
					$("#textTab").hide();$("#textTab_a").removeClass("current");
					$("#imageCustBlock").show();
					if( isImageAdded )
					{
						$("#no_img_cust_panel").hide();	
						$("#img_cust_panel").show();
						$("#baseimagename").html(baseimagename);
						$("#imageObjectId").val(imageObjectId);
						$("#baseimagepath").attr("src",thumbPath);
						if( isImageCustomizationAllowed  )
						{
							$("#imagecontrols").show();
						}
						else
						{
							$("#imagecontrols").hide();
						}
					}
					else
					{
						$("#no_img_cust_panel").show();
						$("#img_cust_panel").hide();
						$("#imagecontrols").hide();
					}
				}
				else
				{
					$("#textTab").show();$("#textTab_a").addClass("current");
					$("#imageTab").hide();$("#imageTab_a").removeClass("current");
					$("#textCustBlock").show();
					$("#ipText").val(text);
					if( isTextAdded )
					{
						$("#textObjectId").val(textObjectId);
						if( isTextCustomizationAllowed  )
						{
							$("#select_font").show();
							$("#textcontrols").show();
						}
						else
						{
							$("#select_font").hide();
							$("#textcontrols").hide();
						}
					}
					else
					{
						$("#select_font").hide();
						$("#textcontrols").hide();
					}
				}
			}
			else
			if( obj_type == "text")
			{
				var isTextAllowed = parseInt(result[count++]);
				var isTextAdded = parseInt(result[count++]);
				var textObjectId = result[count++];
				var text = result[count++];
				var isTextCustomizationAllowed = parseInt(result[count++]);
				$("#textTab").show();$("#textTab_a").addClass("current");
				$("#imageTab_a").removeClass("current");
				$("#textCustBlock").show();
				$("#imageCustBlock").hide();
				$("#ipText").val(text);
				if( isTextAdded )
				{
					$("#textObjectId").val(textObjectId);
					if( isTextCustomizationAllowed  )
					{
						$("#select_font").show();
						$("#textcontrols").show();
					}
					else
					{
						$("#select_font").hide();
						$("#textcontrols").hide();
					}
				}
				else
				{
					$("#select_font").hide();
					$("#textcontrols").hide();
				}
			}
			else
			if( obj_type == "image")
			{
				var isImageAllowed = parseInt(result[count++]);
				var isImageAdded = parseInt(result[count++]);
				var imageObjectId = result[count++];
				var baseimagename = result[count++];
				var thumbPath = result[count++];
				var isImageCustomizationAllowed = parseInt(result[count++]);

				$("#imageTab").show();$("#imageTab_a").addClass("current");
				$("#textTab_a").removeClass("current");
				$("#imageCustBlock").show();
				$("#textCustBlock").hide();
				if( isImageAdded )
				{
					$("#no_img_cust_panel").hide();	
					$("#img_cust_panel").show();
					$("#baseimagename").html(baseimagename);
					$("#imageObjectId").val(imageObjectId);
					$("#baseimagepath").attr("src",thumbPath);
					if( isImageCustomizationAllowed  )
					{
						$("#imagecontrols").show();
					}
					else
					{
						$("#imagecontrols").hide();
					}
				}
				else
				{
					$("#no_img_cust_panel").show();
					$("#img_cust_panel").hide();
					$("#imagecontrols").hide();
				}
			}
			$("#body").css("cursor","auto");
}
	
function changeStyleTemplate_CB(data)
{
	var res = data.trim();
	if( res == "success") {
        orientationsChanged = true;
        changeCustomizationTemplate('all');
		previewCustomizedImage();
    }
	$("#body").css("cursor","auto");
}

function changeStyleTemplate(template_master_id)
{
	ajaxCall.get("change_style_template","./mkchangestyletemplate.php?windowid="+windowid+"&tmid="+template_master_id,changeStyleTemplate_CB);
}


var ajaxCall = new function()
{
	this.callList = new Object;
	this.get = function(type,url,callback)
	{
		$("#body").css("cursor","progress");
		if( this.callList[type] != null )
		{
			this.callList[type].abort();
			this.callList[type] = null;
		}
		this.callList[type] = $.get(url,callback);
	}
}

// Show upload panel div
function getBrowserHeight() 
{
	var intH = 0;
	var intW = 0;

	if(typeof window.innerWidth == 'number' ) 
	{
		intH = window.innerHeight;
		intW = window.innerWidth;
	}
	else if(document.documentElement && (document.documentElement.clientWidth || document.documentElement.clientHeight)) 
	{
		intH = document.documentElement.clientHeight;
		intW = document.documentElement.clientWidth;
	}
	else if(document.body && (document.body.clientWidth || document.body.clientHeight)) 
	{
		intH = document.body.clientHeight;
		intW = document.body.clientWidth;
	}
	return { width: parseInt(intW), height: parseInt(intH) };
}
function setLayerPosition() 
{       
	var shadow = document.getElementById('shadow');
	var question = document.getElementById('question');
				        
	var bws = getBrowserHeight();
	shadow.style.width = bws.width + 'px';
	shadow.style.height = (bws.height) + 'px';
	question.style.left = parseInt((bws.width - 620) / 2)+ 'px';
	question.style.top = parseInt((bws.height - 480) / 2)+ 'px';
	shadow = null;
	question = null;
}

function showLayer() 
{
	setLayerPosition();
	var shadow = document.getElementById('shadow');
	var question = document.getElementById('question');
	shadow.style.display = 'block';
	question.style.display = 'block';
	shadow = null;
	question = null;
}
function hideLayer() 
{
	var shadow = document.getElementById('shadow');
	var question = document.getElementById('question');

	shadow.style.display = 'none';
	question.style.display = 'none';

	shadow = null;
	question = null;
}
window.onresize = setLayerPosition;

function showUploadImageDiv()
{
	var ques = document.getElementById("question");
	ques.innerHTML = "";
	//ques.innerHTML = "<div id='close' style='float:right;font-size:12px;font-weight:600;padding-right:20px'><a href='javascript:hideLayer()'>close</a></div><div id='uploadPanelDiv'></div>";
	//ques.innerHTML = "<div id='close' style='float:right;font-size:12px;font-weight:600;padding-right:20px'><iframe src='http://localhost/branch/showUploadPanel.php?windowid="+windowid+"' height='500px' width='500px' frameborder='0' scrolling='no'></iframe><div>";
	
	//close div
	var main_div=document.createElement("div"); 
	main_div.setAttribute('id', 'close');
	main_div.setAttribute('style', 'float:right;font-size:12px;font-weight:600;padding-right:20px');
	var close=document.createElement("a"); 
	close.innerHTML="close";
	close.setAttribute('href', 'javascript:hideLayer()');
	main_div.appendChild(close);
	ques.appendChild(main_div);
	
	//iframe div
	var iframe_div=document.createElement("div"); 
	//append iframe div to parent question div
	ques.appendChild(iframe_div);
	
	//iframe element
	var iframe_el = document.createElement("iframe");
	iframe_el.setAttribute('id', 'ifrm');
	
	//append iframe to parent div
	iframe_div.appendChild(iframe_el);
	iframe_el.setAttribute('src', http_loc+'/showUploadPanel.php?windowid='+windowid);
	iframe_el.setAttribute('height', '600px');
	iframe_el.setAttribute('width', '590px');
	iframe_el.setAttribute('frameborder', '0px');
	iframe_el.setAttribute('border', '0');
	iframe_el.setAttribute('scrolling', 'no');
	showLayer();
	//getUploadPanel();
}

/*function getUploadPanel_CB(data)
{
	$("#uploadPanelDiv").html(data.trim());	
}*/
function getUploadPanel()
{
	//ajaxCall.get("get_upload_panel","./staticShowUploadPanel/"+windowid,getUploadPanel_CB);
	$("#uploadPanelDiv").load("./staticShowUploadPanel/"+windowid);
}

// Below code is used to ensure candidate does not add to cart before image is loaded
var imageLoaded = false;
function submitCartForm() {
    if (!imageLoaded) {
        alert("Please wait for the image to load");
    }
    return imageLoaded;
}


