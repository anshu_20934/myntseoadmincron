function selectAll(flag) 
{
   var count =  document.getElementById('numrecordid').value;

   if(flag == 'parent')
   {
		if(document.getElementById('select').checked == true) 
		{
			if(count > 1)
			{
			   var checkCount = '';
			   for(i = 0; i < count; i++) 
			   {
				   document.manage_req_form.delstyle[i].checked = true;
				   checkCount++;
			   }
			}
			else
			{
				document.manage_req_form.delstyle.checked = true;
			}
		}
		if(document.getElementById('select').checked == false) 
		{
			if(count > 1)
			{
			   var checkCount = count;
			   for(i = 0; i < count; i++) 
			   {
				   document.manage_req_form.delstyle[i].checked = false;
				   checkCount--;
			   }
			}
			else
			{
				document.manage_req_form.delstyle.checked = false;
			}
		}
   }

   if(flag == 'child')
   {
	   var checkCount = (count > 1)?count:1;
		var countcheckedTrue = 0;
    
		if(count > 1)
		{
			for(i = 0; i < count; i++) 
			{
			   if(document.manage_req_form.delstyle[i].checked == true) 
			   {
				 countcheckedTrue = countcheckedTrue + 1;
			   }
			}
		}
		else
	    {
				if(document.manage_req_form.delstyle.checked == true) 
			   {
				 countcheckedTrue = countcheckedTrue + 1;
			   }
		}
    
		if(checkCount != countcheckedTrue) 
		{
			document.getElementById('select').checked = false;
		} 
		if(checkCount == countcheckedTrue) 
		{
			document.getElementById('select').checked = true;
		}
   }
}


function deleteAdminRequest(requestid) 
{
	var strSubmit         = '';
    var strSubmit1         = '';
    var count            = document.getElementById('numrecordid').value;
    var checkCount       = count;
    var countcheckedTrue = 0;
    
	if(count > 1)
	{
		for(i = 0; i < count; i++) 
		{
		   if(document.manage_req_form.delstyle[i].checked == true) 
		   {
				countcheckedTrue = countcheckedTrue + 1;
		   }
		}
	}
	else
	{
		if(document.manage_req_form.delstyle.checked == true) 
	    {
			countcheckedTrue = countcheckedTrue + 1;
	    }
	}
    
    if(countcheckedTrue == 0)
	{
        alert("Select at least one record for deleting!!");
        return false;
    } 
	else
	{
		if(count > 1)
		{
			for(i = 0; i < count; i++) 
			{
				if(document.manage_req_form.delstyle[i].checked == true) 
				{
					strSubmit += ","+document.manage_req_form.delstyle[i].value;
				}
			}
		}
		else
		{
			if(document.manage_req_form.delstyle.checked == true) 
			{
				strSubmit += ","+document.manage_req_form.delstyle.value;
			}
		}


        var delflag = confirm("Do you really want to delete the "+countcheckedTrue+" record(s)!!");
        
        if(delflag == true) 
		{

				if(window.XMLHttpRequest) {
				req = new XMLHttpRequest(); 
				 } else if (window.ActiveXObject) {
				req  = new ActiveXObject("Microsoft.XMLHTTP"); 
				 }
				
				var DELETEURL =  "../admin/deleteproductsfrommanage.php";

				var message = "Please wait while deleting the record(s)...";
				
				//document.getElementById("updaterequest").style.visibility= "visible";
				//document.getElementById("updaterequest").style.display= "";
				//document.getElementById("processmessage").innerHTML= message;
				
				http.open("GET", DELETEURL + "?styleid=" + escape(strSubmit)+"&requestid=" + escape(requestid), true);
		        http.onreadystatechange = callDeleteAll;
		        http.send(null);
	   }
    }
}

function callDeleteAll() 
{
    if(http.readyState==4) 
	{
		//document.getElementById("updaterequest").style.visibility= "hidden";
		//document.getElementById("updaterequest").style.display= "none";
		 if(http.status==200) 
		{
	   var result=http.responseText.split("|");
		alert(result[0]+" rows deleted!!"); 
			window.location = 'admin_ManageCorpRequest.php?requestid='+result[1]+'&flag=recorddeleted';
		}
		else
			alert("status code is::"+http.status+"::contact to administrator!!");

	}
}