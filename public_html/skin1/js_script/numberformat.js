function numberformat(num,dec)
{

	mul=Math.pow(10,dec);
	num=num*mul;
	num=Math.round(num);
	num = num/mul;
	var numstr=String(num);
	if(numstr.indexOf(".") == -1)
	{
		numstr = numstr + ".";
		for(nfi=0;nfi<dec;nfi++) numstr = numstr + "0";
	}
	decpl = numstr.length - numstr.indexOf(".");
	decpl = decpl - 1;
	if (decpl < dec)
	{
		for(nfi=decpl;nfi<dec;nfi++) numstr = numstr + "0";
	}
	return (numstr);

}
