var timerlen = 5;
var slideAniLen = 20;

var timerID = new Array();
var startTime = new Array();
var obj = new Array();
var endHeight = new Array();
var moving = new Array();
var dir = new Array();

var prevColNumber = 1;

function highLightColumnHeading(thisHeading)
{
	thisHeading.className = "v2_tools_hdr_on";
	document.getElementById("heading"+prevColNumber).className = "v2_tools_hdr";
}

function highLightSelectedColumn(thisHeading,currColNumber)
{
	if( currColNumber != prevColNumber)
	{
		document.getElementById("col"+prevColNumber).style.display="none";
		slidedownEx("col"+currColNumber);
		highLightColumnHeading(thisHeading);
		prevColNumber = currColNumber;
	}
}

function slidedownEx(objname)
{
	if(moving[objname])
                return;

        if(document.getElementById(objname).style.display != "none")
                return; // cannot slide down something that is already visible

        moving[objname] = true;
        dir[objname] = "down";
        startslide(objname);
}

function slidedown(thisHeading,currColNumber){
	highLightSelectedColumn(thisHeading,currColNumber);
}

function slideup(objname){
/*        if(moving[objname])
                return;

        if(document.getElementById(objname).style.display == "none")
                return; // cannot slide up something that is already hidden

        moving[objname] = true;
        dir[objname] = "up";
        startslide(objname);*/
}

function startslide(objname){
        obj[objname] = document.getElementById(objname);

        endHeight[objname] = parseInt(obj[objname].style.height);
        startTime[objname] = (new Date()).getTime();

        if(dir[objname] == "down"){
                obj[objname].style.height = "1px";
        }

        obj[objname].style.display = "block";

        timerID[objname] = setInterval('slidetick(\'' + objname + '\');',timerlen);
}

function slidetick(objname){
        var elapsed = (new Date()).getTime() - startTime[objname];

        if (elapsed > slideAniLen)
                endSlide(objname)
        else {
                var d =Math.round(elapsed / slideAniLen * endHeight[objname]);
                if(dir[objname] == "up")
                        d = endHeight[objname] - d;

                obj[objname].style.height = d + "px";
        }

        return;
}

function endSlide(objname){
        clearInterval(timerID[objname]);

        if(dir[objname] == "up")
                obj[objname].style.display = "none";

        obj[objname].style.height = endHeight[objname] + "px";

        delete(moving[objname]);
        delete(timerID[objname]);
        delete(startTime[objname]);
        delete(endHeight[objname]);
        delete(obj[objname]);
        delete(dir[objname]);

        return;
}

