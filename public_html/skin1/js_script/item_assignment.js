// JaavaScript Document
function assignItemAssignment(updatetype, itemid, curitemstatus)
{
    if(window.XMLHttpRequest){
        var http = new XMLHttpRequest();
    }else if(window.ActiveXObject){
        var http = new ActiveXObject("Microsoft.XMLHTTP");
    }
        
        var item_status = document.getElementById("item_st_"+itemid).value;
        var item_quantity = document.getElementById("qty_"+itemid).value;
        var page = document.getElementById("page").value;
        
        if(typeof curitemstatus != undefined)
        {
            var selOBj = document.getElementById("item_st_"+itemid);
            if(curitemstatus == 'A' && item_status == 'UA')
            {
                alert("Status can not be changed from Assigned to Unassigned");
                //selOBj.options[selOBj.selectedIndex+1].selected = true;
                return false;
            }
            if(curitemstatus == 'A' && item_status == 'QD')
            {
                alert("Status can not be changed from Assigned to QADone in this form");
                //selOBj.options[selOBj.selectedIndex+1].selected = true;
                return false;
            }
            if(curitemstatus == 'I' && item_status == 'UA')
            {
                alert("Status can not be changed from In Progress to Unassigned");
                selOBj.options[selOBj.selectedIndex+1].selected = true;
                return false;
            }
            if(curitemstatus == 'S' && item_status == 'A')
            {
                alert("Status can not be changed from Sent Outside to Assigned");
                selOBj.options[selOBj.selectedIndex+4].selected = true;
                return false;
            }
            
            if(curitemstatus == 'S' && item_status == 'UA')
            {
                alert("Status can not be changed from Sent Outside to Unassigned");
                selOBj.options[selOBj.selectedIndex+5].selected = true;
                return false;
            }
        }
            var ITEMASSIGNMENT = http_loc+"/admin/updateitemassignment.php?update=" + updatetype + "&itemid=" + itemid;
            if(updatetype == "assign")
                ITEMASSIGNMENT += "&itemto=" + item_assign_to;
            else if(updatetype == "location")
                ITEMASSIGNMENT += "&loc=" + item_location;
            else if(updatetype == "status")    
                ITEMASSIGNMENT += "&status=" + item_status + "&qunatity=" + item_quantity;
          
            http.open("GET", ITEMASSIGNMENT, true); 
            http.onreadystatechange = function() 
        	{ 
        		if(http.readyState == 4) 
        		{ 
            		  if(http.status == 200) 
            		  { 
      				      
      				      var query_string = document.getElementById("query_string").value; 
                              
      				      if(typeof  page != null)
      				          var URL = "item_assignment.php?" + query_string;
    		              else
    		                  var URL = "item_assignment.php?" + query_string;
    		                  
      				      window.location.href = URL;
            		  }
            		  else
            		  {
            		      alert(http.status + "===" + http.statusText +" OOPS!! I can not work, Please contact the administrator");
                      }
        		}
        	 }
            http.send(null);
}

/*
 *Function for updating the Assignee
 */
function changeAssignee(){

    var page = document.getElementById("page").value;
    var query_string = document.getElementById("query_string").value;
    var item_assignee = document.getElementById("item_ass").value;
    var threshhold = document.getElementById("threshhold_"+item_assignee).value;
    var alreadyassigneditem = document.getElementById("totalassigneditem_"+item_assignee).value;

    var item_location = document.getElementById("item_loc");    
    
    if(item_location.value == "0")
        {
            alert("You have not selected a location");
            var assignee_select = document.getElementById("item_ass")
            assignee_select.options[0].selected = true;
            return false;
        }

    /*Count currently assigning items*/
    var currentlyassigneditems = 0;
    var qty_array_obj = document.item_assignment.elements["qty[]"];
    for(i=0;i<qty_array_obj.length;i++){
        var itemid = qty_array_obj[i].value;
        if(document.getElementById("check_"+itemid).checked == true){
            var quantity = document.getElementById("check_"+itemid).value;
            currentlyassigneditems = parseInt(currentlyassigneditems) + parseInt(quantity);
        }
    }

    /*if(!currentlyassigneditems) {
        document.getElementById("item_ass").options[0].selected = true;
        alert("You have not selected any items to assign!");
        return false;
    }*/
    
    var totalitemsgoingtoassign = parseInt(alreadyassigneditem) + parseInt(currentlyassigneditems);
    var flag = true;
    if(totalitemsgoingtoassign > threshhold){
        flag = confirm("Assignee threshhold value is " + threshhold + ". You are going to assign " + totalitemsgoingtoassign + " item(s).So you are going to assign more than threshhold value. If you are confirm then click Ok otherwise click Cancel");
    }

    if(flag){
        if(page != "")
            var action = http_loc+"/admin/updateitemassignment.php?update=assign" + "&page=" + page + "&ia=" + item_assignee + "&il=" + item_location.value + "&qstring=" + query_string;
        else
            var action = http_loc+"/admin/updateitemassignment.php?update=assign" + "&ia=" + item_assignee + "&il=" + item_location.value + "&qstring=" + query_string;

        document.item_assignment.action = action;
        document.item_assignment.submit();
    }else { return; }
}

function itemDoneAssignee(updatetype, itemid, curitemstatus)
{
    
    if(window.XMLHttpRequest){
        var http = new XMLHttpRequest();
    }else if(window.ActiveXObject){
        var http = new ActiveXObject("Microsoft.XMLHTTP");
    }
        
    var item_status = document.getElementById("item_st_"+itemid).value;
    var item_quantity = document.getElementById("qty_"+itemid).value;
    var item_orderid = document.getElementById("orderid_"+itemid).value;
    var page = document.getElementById("page").value;
    var onhold_comment="";
    

    if(typeof curitemstatus != undefined)
    {
        var selOBj = document.getElementById("item_st_"+itemid);
        if(curitemstatus == 'A' && item_status == 'N')
        {
            alert("Status can not be changed from Assigne to New");
            selOBj.options[selOBj.selectedIndex+1].selected = true;
            return false;
        }

        if(item_status == 'H')
        {            

            //Let addordercomment.php handle putting the item and order to hold.
  
            var onhold_reason = prompt("Enter reason for putting item On Hold","");
            if (onhold_reason == null ){ // Cancel pressed 
               return false;
            } 
            else if (onhold_reason == "" || onhold_reason == " "){ // OK pressed but no text 
                 alert("Valid reason not entered. Please try again."); 
                 return false;
            }
            else {
                onhold_comment=onhold_reason;
            }
            //window.open('addOrder_comment.php?orderid='+item_orderid+'&onhold='+'put','null', 'menubar=1,resizable=1,toolbar=1,width=800,height=500');
            //return false;
        }

        if(item_status == 'QD')
        {
            alert("Status can not be changed to QADone from this form. \n Mark as OpsDone and take item to QA Desk");
            selOBj.options[selOBj.selectedIndex-2].selected = true;
            return false;
        }

        if(item_status == 'D')
        {
            alert("Status can not be changed to Done from this form. \n Mark as OpsDone and take item to QA Desk.");
            selOBj.options[selOBj.selectedIndex-3].selected = true;
            return false;
        }

        if(item_status == 'UA')
        {
            alert("Status can not be changed to Unassigned from this form. \n If there is a problem with your order, inform your Ops Manager");
            selOBj.options[selOBj.selectedIndex+1].selected = true;
            return false;
        }

        if(item_status == 'F')
        {
            alert("Please use \"On Hold \" status instead of Failed \nif you have a problem working with this item " );
            selOBj.options[selOBj.selectedIndex-4].selected = true;
            return false;
        }


        if(curitemstatus == 'S' && item_status == 'A')
        {
            alert("Status can not be changed from Sent Outside to Assigne");
            selOBj.options[selOBj.selectedIndex+4].selected = true;
            return false;
        }
        
        if(curitemstatus == 'S' && item_status == 'N')
        {
            alert("Status can not be changed from Sent Outside to New");
            selOBj.options[selOBj.selectedIndex+5].selected = true;
            return false;
        }
    }
    
    var ITEMASSIGNMENT = http_loc+"/admin/updateitemassignment.php?update=" + updatetype + "&itemid=" + itemid+"&onholdreason="+onhold_comment;
    if(updatetype == "status")    
        ITEMASSIGNMENT += "&status=" + item_status + "&qunatity=" + item_quantity;
      
    http.open("GET", ITEMASSIGNMENT, true); 
    http.onreadystatechange = function() 
	{ 
		if(http.readyState == 4) 
		{ 
    		  if(http.status == 200) 
    		  { 
			      alert("Item upadted successfully");
			      
			      var query_string = document.getElementById("query_string").value; 
                      
			      if(typeof  page != null)
			          var URL = "assignee_worksheet.php?" + query_string;
	              else
	                  var URL = "assignee_worksheet.php?" + query_string;
	                  
			      window.location.href = URL;
    		  }
    		  else
    		  {
    		      alert(http.status + "===" + http.statusText+"OOPS!! I can not work, Please contact to administrator");
              }
		}
	 }
    http.send(null);
}

function itemDoneLocation(updatetype, itemid, curitemstatus)
{
    if(window.XMLHttpRequest){
        var http = new XMLHttpRequest();
    }else if(window.ActiveXObject){
        var http = new ActiveXObject("Microsoft.XMLHTTP");
    }
        
    var item_status = document.getElementById("item_st_"+itemid).value;
    var item_quantity = document.getElementById("qty_"+itemid).value;
    var page = document.getElementById("page").value;
    
    if(typeof curitemstatus != undefined)
    {
        var selOBj = document.getElementById("item_st_"+itemid);
        if(curitemstatus == 'A' && item_status == 'N')
        {
            alert("Status can not be changed from Assigne to New");
            selOBj.options[selOBj.selectedIndex+1].selected = true;
            return false;
        }
        
        if(curitemstatus == 'S' && item_status == 'A')
        {
            alert("Status can not be changed from Sent Outside to Assigne");
            selOBj.options[selOBj.selectedIndex+4].selected = true;
            return false;
        }
        
        if(curitemstatus == 'S' && item_status == 'N')
        {
            alert("Status can not be changed from Sent Outside to New");
            selOBj.options[selOBj.selectedIndex+5].selected = true;
            return false;
        }
    }
    
    var ITEMASSIGNMENT = http_loc+"/admin/updateitemassignment.php?update=" + updatetype + "&itemid=" + itemid;
    if(updatetype == "status")    
        ITEMASSIGNMENT += "&status=" + item_status + "&qunatity=" + item_quantity;
      
    http.open("GET", ITEMASSIGNMENT, true); 
    http.onreadystatechange = function() 
	{ 
		if(http.readyState == 4) 
		{ 
    		  if(http.status == 200) 
    		  { 
			      alert("Item upadted successfully");
			      
			      var query_string = document.getElementById("query_string").value; 
                      
			      if(typeof  page != null)
			          var URL = "location_worksheet.php?" + query_string;
	              else
	                  var URL = "location_worksheet.php?" + query_string;
	                  
			      window.location.href = URL;
    		  }
    		  else
    		  {
    		      alert(http.status + "===" + http.statusText +" OOPS!! I can not work, Please contact to administrator");
              }
		}
	 }
    http.send(null);
}

function is_items_not_assigned(number_of_items){
    var query_string = document.getElementById("query_string").value;
    if(number_of_items>0){
        $flag = confirm("You have not assigned all items to operation executives, " + number_of_items + " item(s) are still pending to assigne. Are you sure want to send the confirmation to Operation Head, if yes click OK otherwise click on cancel");
        if($flag == true){
            window.location = http_loc + "/admin/item_assignment.php?" + query_string + "&action=send_confirmation";
        }else{
            return $flag;
        }
    }
}

