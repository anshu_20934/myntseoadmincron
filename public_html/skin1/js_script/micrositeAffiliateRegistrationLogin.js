// JavaScript Document
/* Define url for redirecting to server site page*/

var URL = "./modules/affiliates/affiliateRegistrationLogin.php";
//var affiliateLogin = "";

/*
 * Function Name: getHTTPObject()
 * Purpose: Instantiate object of XMLHttpRequest().
 * Summary: Beauty of this function is, this function instantiate browser independent
 * object of XMLHttpRequest()
 *
*/
var http = null;
function getHTTPObject() {
      if(window.XMLHttpRequest)
	  { 
			http = new XMLHttpRequest(); 
	  } 
	 else if (window.ActiveXObject)
	  { 
			 http  = new ActiveXObject("Microsoft.XMLHTTP"); 
	  }
       return http;
}


/*******************************************************/
/*********Code for login********************************/
/*******************************************************/
function loginFormValidation(ajaxForm)
{
	  var error = true;
	 if(document.getElementById('username').value == "e-mail")
     {
			alert("please input e-mail id");
			document.getElementById('username').focus();
			error = false;
			return error;
     }
     if(!echeck(document.getElementById('username').value))
     {
		   document.getElementById('username').focus();
		   error = false;
		   return error;
     }
     if(document.getElementById('password').value == "password"){
			alert("please input password");
			document.getElementById('password').focus();
			error = false;
			return error;
     }
	 if(error == true){
         loginData2QueryString(ajaxForm);
     }
   
	  
}

function loginData2QueryString(ajaxForm) {

	//Display login loading bar
   	document.getElementById("loginpopup").style.visibility = "hidden";
    document.getElementById("loginpopup").style.display = "none";
    document.getElementById("loginloader").style.visibility = "visible";
    document.getElementById("loginloader").style.display = "";
   

    var strSubmit       = '';
    var formElem;
    var strLastElemName = '';

    for (i = 0; i < ajaxForm.elements.length; i++) {
    formElem = ajaxForm.elements[i];

    switch (formElem.type) {
    // Text, select, hidden, password, textarea elements
    case 'text':
    case 'hidden':
    case 'password':

        strSubmit += formElem.name +
        '=' + escape(formElem.value) + '&'

    break;
  }
  
 }
  strSubmit=strSubmit+"&isNew=N";
  loginAjax(strSubmit);
}

function loginAjax(strSubmit)
{
  getHTTPObject();	
  http.open('POST', URL, true);
  http.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
  http.onreadystatechange = myLoginFunction;
  http.send(strSubmit);
}

function myLoginFunction()
{
    if(http.readyState==4)
    {
    	//hide login loading bar
    	document.getElementById("loginloader").style.visibility = "hidden";
        document.getElementById("loginloader").style.display = "none";
        document.getElementById("loginpopup").style.visibility = "visible";
        document.getElementById("loginpopup").style.display = "";
        // Hide registration error message
     
        var result=http.responseText;
            flag = result
            
        if(flag == 'S'){

            document.getElementById('errorpopup').style.visibility = 'hidden';
            document.getElementById("loginerror").style.visibility = "hidden";
            document.getElementById("loginerror").style.display = "none";
            document.getElementById("loginpopup").style.visibility = "hidden";
            document.getElementById("loginpopup").style.display = "none";
            document.getElementById("regloader").style.display = "none";
		    document.getElementById("regpopup").style.visibility = "hidden";
		    document.getElementById("regpopup").style.display = "none";
           
            // window.location.reload();
            document.updatecart.action ="microsites/affiliatepaymentoptions.php?pagetype=createproduct";
			document.updatecart.submit();
                        
        }

        else if(flag == 'F'){
        	 document.getElementById("loginerror").style.visibility = "visible";
            document.getElementById("loginerror").style.display = "";
        }
    }
}


function changeColor(divname)
{
    document.getElementById(divname).style.color = "#000000";
    //document.getElementById(divname).style.background = "#000000";
}



function  autoRegisterAffiliateUser(page)
{
	 getHTTPObject();
	 var urlAuth = "./modules/affiliates/affiliateauthentication.php?rand="+Math.random();
	 http.onreadystatechange = function(){
	 if (http.readyState == 4)
		 {
		         var result=http.responseText;
				 if(result == 'S')	
				 {  
						 document.updatecart.action ="microsites/affiliatepaymentoptions.php"+"?pagetype="+page;
						 document.updatecart.submit();
				 }
				 else if(result == 'F')
					 alert("Please enter valid email Id"); 
					   
		} 
	 };
  http.open('GET', urlAuth, true);
  http.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
  http.send(null);

}


function registerAffiliateEmailProvider(useremail)
{
	  getHTTPObject();
	  var urlAuth = "./modules/affiliates/affiliateauthentication.php";
	 
	  //Display login loading bar
	  document.getElementById("loginpopup").style.visibility = "hidden";
	  document.getElementById("loginpopup").style.display = "none";
	  document.getElementById("loginloader").style.visibility = "visible";
	  document.getElementById("loginloader").style.display = "";
	  http.onreadystatechange = function(){
	  if (http.readyState == 4)
	  {
	  	    //hide login loading bar
	    	document.getElementById("loginloader").style.visibility = "hidden";
	        document.getElementById("loginloader").style.display = "none";
	        document.getElementById("loginpopup").style.visibility = "visible";
	        document.getElementById("loginpopup").style.display = "";
	        var result=http.responseText;
			if(result  == 'S')
			{
						 //document.updatecart.action ="mkpaymentoptions.php"?pagetype="+page;
						 document.updatecart.action ="microsites/affiliatepaymentoptions.php?pagetype=createproduct";
						 document.updatecart.submit();
            }


	  } 
	};
		 
  http.open('GET', urlAuth+"?username="+useremail, true);
  http.send(null);
}

function userEmailValidation(ajaxForm)
{
	 var error = true;
	 if(document.getElementById('username').value == "e-mail")
     {
			alert("please input e-mail id");
			document.getElementById('username').focus();
			error = false;
			return error;
     }
     if(!echeck(document.getElementById('username').value))
     {
		   document.getElementById('username').focus();
		   error = false;
		   return error;
     }
   
     if(error == true){
         registerAffiliateEmailProvider(document.getElementById('username').value);
     }
   
	  
}

function autoRegisterUsingAffiliateAcc(page)
{
	 getHTTPObject();
	 var urlAuth = "./modules/affiliates/affiliateauthentication.php?rand="+Math.random();
	 http.onreadystatechange = function(){
		 if (http.readyState == 4)
		 {
			    var result=http.responseText;
			    if(result == 'S')	
				 {  
						 document.updatecart.action ="microsites/affiliatepaymentoptions.php"+"?pagetype="+page;
						 document.updatecart.submit();
				 }
				 else if(result == 'F')
					 alert("Please enter valid email Id"); 
		 } 
	 };
		 
	  http.open('GET', urlAuth, true);
	  http.send(null);
}


function formValidation(ajaxForm){
var error=true;
if(document.getElementById('firstname').value=="firstname"){
alert("please input first name");
document.getElementById('firstname').focus();
var error=false;
return error;}
if(document.getElementById('lastname').value=="lastname"){
alert("please input last name");
document.getElementById('lastname').focus();
var error=false;
return error;}
if(document.getElementById('uname').value=="e-mail"){
alert("please input e-mail id");
document.getElementById('uname').focus();
var error=false;
return error;}
if(!echeck(document.getElementById('uname').value)){
document.getElementById('uname').focus();
var error=false;
return error;}
if(document.getElementById('passwd1').value=="password"){
alert("please input password");
document.getElementById('passwd1').focus();
var error=false;
return error;}
if(document.getElementById('passwd1').value.length<6){
alert("password must have minimum 6 character");
document.getElementById('passwd1').focus();
var error=false;
return error;}
if(document.getElementById('passwd2').value=="password"){
alert("please input confirm password");
document.getElementById('passwd2').focus();
var error=false;
return error;}
if(document.getElementById('passwd1').value!=document.getElementById('passwd2').value){
alert("confirm password does not match with password");
document.getElementById('passwd2').focus();
var error=false;
return error;}
if(error==true){
formData2QueryString(ajaxForm);}}
function formData2QueryString(ajaxForm){
document.getElementById("regpopup").style.visibility="hidden";
document.getElementById("regpopup").style.display="none";
document.getElementById("regloader").style.visibility="visible";
document.getElementById("regloader").style.display="";
var strSubmit='';
var formElem;
var strLastElemName='';
for(i=0;i<ajaxForm.elements.length;i++){
formElem=ajaxForm.elements[i];
switch(formElem.type){
case 'text':
case 'hidden':
case 'password':
strSubmit+=formElem.name+
'='+escape(formElem.value)+'&'
break;}}
saveAjax(strSubmit);}
function saveAjax(strSubmit){
getHTTPObject();
http.open('POST',URL,true);
http.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
http.onreadystatechange=myPostFunction;
http.send(strSubmit);}