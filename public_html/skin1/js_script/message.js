
Ext.onReady(function() {
	Ext.QuickTips.init();
	var myMask = new Ext.LoadMask(Ext.getBody(), {msg: "Loading..."});
	Ext.Ajax.on('beforerequest', myMask.show, myMask);
	Ext.Ajax.on('requestcomplete', myMask.hide, myMask);
	Ext.Ajax.on('requestexception', myMask.hide, myMask);

	var searchPanel = new Ext.form.FormPanel({
		renderTo : 'contentPanel',
		title : 'Add/Edit Message',
		id : 'formPanel',
		autoHeight : true,
		border : true,
		bodyStyle : 'padding: 5px',
		width : 1000,
 		monitorValid:true,
		items : [{
			layout : 'table',
			border : false,
			defaults : {width : 450, border : false, 
				layout : 'form', bodyStyle : 'padding-left : 15px'},
			items : [
					{
						items : [{
		                    xtype : 'textfield',
        	    	        id : 'message',
            	    	    fieldLabel : 'Message',
							allowBlank : false,
                    		width : 300
						},

						{
		                    xtype : 'textfield',
        	    	        id : 'category',
            	    	    fieldLabel : 'Category',
							allowBlank : false,
                    		width : 300
						},
						{
		                    xtype : 'textfield',
        	    	        id : 'sub_category',
            	    	    fieldLabel : 'Sub Category',
							format: 'd/m/Y',
							allowBlank : false,
                    		width : 300
						}]
					}, 
			         {
			        	 items : [{
			        		 xtype : 'datefield',
			        		 name : 'start_date',
			        		 id : 'start_date',
							 format: 'd/m/Y',
							 altFormats:'Y-m-d H:i:s',
			        		 width : 100,
			        		 fieldLabel : "Start Date",
							// minValue: today,
							allowBlank : false
			        	 }
			        ,
			        	 {
			        		 xtype : 'datefield',
							allowBlank : false,
			        		 name : 'end_date',
							 format: 'd/m/Y',
							 altFormats:'Y-m-d H:i:s',
			        		 id : 'end_date',
			        		 width : 100,
			        		 fieldLabel : "To Date"
			        	 },
{
xtype: 'radiogroup',
columns: 'auto',
name: 'state',
fieldLabel: 'State',
			        		 width : 150,
items: [{
name: 'state',
inputValue: 1,
boxLabel: 'Active'
}, {
name: 'state',
inputValue: 0,
boxLabel: 'Inactive'
}]
}] 
			         } 
			]
		}],
		buttons : [ {
			text : 'Save',
        	formBind:true,
			handler : function() {
            var params = getSearchPanelParams();
            if(params != false) {
               	Ext.Ajax.request({
            	url : 'save_message.php',
            	params : params,
            	success : function(response, action) {
	               	reloadGrid();
					Ext.MessageBox.alert('Success', 'Your data is saved!');
            	},
				failure : function(action, response) {
					Ext.MessageBox.alert('Error', 'Unable to save the data');
				}
             });
         }
			}
		}],
	});
	
	function getSearchPanelParams() {
		var form = searchPanel.getForm();
		
		if (form.isValid()) {
			var startDate = form.findField("start_date").getValue();
			var endDate = form.findField("end_date").getValue();

			if(startDate > endDate) {
				Ext.MessageBox.alert('Error', 'Invalid Date range. End date has to be grater than or equal to start date.');
				return false;
			}
			if(!form.findField('state').getValue()) {
				Ext.MessageBox.alert('Error', 'Mark the message as either active or inactive');
				return false;
			}
			var params = {};
			params["startDate"] = form.findField("start_date").getRawValue();
			params["state"] = form.findField('state').getValue().inputValue;
			params["endDate"] = form.findField("end_date").getRawValue();
			params["category"] = form.findField("category").getRawValue();
			params["subCategory"] = form.findField("sub_category").getRawValue();
			params["message"] = form.findField("message").getRawValue();
			if(searchResults.getSelectionModel().getCount() >0) {
				params["id"] = searchResults.getSelectionModel().getSelected().data.id;
			}
			return params;
		} else {
			return false;
		}
	}
	
	function reloadGrid() {
		var params = {};
		params["action"] = "search";
		params["activeOnly"] = onlyActiveMessages;
		searchResultsStore.baseParams = params;
		searchResultsStore.load();
	}
	
	
	var colModel = new Ext.grid.ColumnModel({
		defaults: {
	        sortable: true
	    },
	    columns: [
	        new Ext.grid.RowNumberer(),
	        {id : 'message', header : "Message", sortable: true, dataIndex : 'message'},
	        {id : 'category', header : "Category", sortable: false, dataIndex : 'category'},
	        {id : 'subCategory', header : "Sub Category", sortable: true, dataIndex : 'sub_category', sortable: false},
	        {id : 'startDate', header : "Start Date", sortable: true, dataIndex : 'start_date', width:100},
	        {id : 'endDate', header : "endDate", sortable: true, dataIndex: 'end_date'},
            {header: "State", width: 40, sortable: true, type:'int', renderer: function(v) {
				if(v==1) return 'Active'; else return 'Inactive';
			}, dataIndex: 'state'}
  	  	],
		autoExpandColumn: 'message'
	});
		
	var searchResultsStore = new Ext.data.Store({
		url: 'message.php',
		pruneModifiedRecords : true,
		reader: new Ext.data.JsonReader({
	  		root: 'results',
	  	  	totalProperty: 'count',
			fields : ['id', 'message','category','sub_category','start_date','end_date', {name: 'state', type: 'int'} ]
		}),
	});

	var pagingBar = new Ext.PagingToolbar({
		pageSize : 30,
	    store: searchResultsStore,
	    displayInfo: true
	});
	var onlyActiveMessages = true;
	var searchResults = new Ext.grid.GridPanel({
	    store  : searchResultsStore,
	    cm : colModel,
	    id : 'searchResults-panel',
	    title : 'Messages',
	    frame : true,
		loadMask : false,
		autoHeight : true,
		bbar : pagingBar,
    	tbar : [{
            text: 'Display all messages',
            enableToggle: true,
            pressed: true,
            tooltip: 'When enabled, only active messages will be displayed.',
            toggleHandler: function(btn, pressed) {
                onlyActiveMessages = pressed;
				btn.setText(pressed==false?'Display only active messages':'Display all messages');
				reloadGrid();
            }
		}],
		stripeRows : true,
		autoScroll : true,
		viewConfig : {
			forceFit: true
		},
        listeners: {
            rowclick: function(g, index, ev) {
                var rec = g.store.getAt(index);
                loadRecord(rec);
            },
            destroy : function() {
                onReset();
            }
        }
	});

    var onReset = function(btn, ev) {
		var form = searchPanel.getForm();
        form.reset();
    };
	
    var loadRecord = function(rec) {
        this.record = rec;
        var form = searchPanel.getForm();
		if(searchResults.getSelectionModel().getCount() ==0) {
			form.reset();
		} else {
	        form.loadRecord(rec);
		}
    };
	
	var mainPanel = new Ext.Panel({
		renderTo : 'contentPanel',
		border : false,
		items : [
		     searchPanel, searchResults
		],
		listeners : {
			'afterrender' : function(comp) {
				reloadGrid();
			}
		}
	});
});
