/*
 * Function Name: getHTTPObject()
 * Purpose: Instantiate object of XMLHttpRequest().
 * Summary: Beauty of this function is, this function instantiate browser independent
 * object of XMLHttpRequest()
 *
*/
function getHTTPObject() {
      var xmlhttp;
      /*@cc_on
          @if (@_jscript_version >= 5)
            try {
          xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
            } catch (e) {
          try {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
          } catch (E) {
            xmlhttp = false;
          }
        }
          @else
          xmlhttp = false;
          @end @*/
          if (!xmlhttp && typeof XMLHttpRequest != 'undefined') {
            try {
          xmlhttp = new XMLHttpRequest();
    } catch (e) {
          xmlhttp = false;
    }
  }
          return xmlhttp;
}
var http = new getHTTPObject();


/**
*
*  Javascript trim, ltrim, rtrim
*
**/

function trim(str, chars) {
    return ltrim(rtrim(str, chars), chars);
}

function ltrim(str, chars) {
    chars = chars || "\\s";
    return str.replace(new RegExp("^[" + chars + "]+", "g"), "");
}

function rtrim(str, chars) {
    chars = chars || "\\s";
    return str.replace(new RegExp("[" + chars + "]+$", "g"), "");
}



/*
*Send ajax call to load men and women style
*
*/
function loadGenderStyle(gender,menstyleid,womenstyleid){
	document.getElementById('please_wait_'+menstyleid+womenstyleid).style.display="block";
	var LOADSTYLEURL = "loadCricinfoWidgetDesign.php?gender="+gender+"&menstyleid="+menstyleid+"&womenstyleid="+womenstyleid;
	http.open('GET', LOADSTYLEURL, true);
	http.onreadystatechange = function(){
		if(http.readyState==4){
    		if(http.status==200){
			  /*
			  * Load response from server
			  */
            	var first_level=http.responseText.split(","); 
            	var gender = first_level[0];
            	var menstyleid = first_level[1];
            	var womenstyleid = first_level[2];
            	var price = first_level[3];
            	var image = first_level[5];
            	var sizestring = first_level[4];
            	var style_name = first_level[6];
				var popup_image = first_level[7];

            	var sizearray =sizestring.split("||");
            	
            	gender = trim(gender); 
            	
            	if(gender=='men') var styleid=menstyleid; 
            	else var styleid=womenstyleid;
            	
            	/*
            	* Instantiate elements objects
            	*/
            	var sizeoptions = document.getElementById('size_container_'+menstyleid+womenstyleid);
            	var sizename = document.getElementById('size_name_'+menstyleid+womenstyleid);
            	var sizequantity = document.getElementById('size_quantity_'+menstyleid+womenstyleid);
            	
            	if(navigator.appName=="Microsoft Internet Explorer")
					sizename.innerHTML ="";
				else
			    	sizename.innerHTML =null;
			    	
			    /*
			    * Create an element UL for size name
			    */	
            	var objStyleSizesUL = document.createElement('ul');
            	
            	for(var i=0;i<sizearray.length;i++){
            		sizeTextArray = sizearray[i].split(":");
            		sizeText = sizeTextArray[1];
            	/*
			    * Create an element LI for size name
			    */	
            		var objStyleSizesLI = document.createElement('li');
            	/*
			    * Create a Text node for size name Text
			    */
            		var objStyleSizeText = document.createTextNode(sizeText);
            	/*
			    * Append size name Text node into parent LI
			    */
            		objStyleSizesLI.appendChild(objStyleSizeText);
            	/*
            	* Append LI into UL
            	*/
            		objStyleSizesUL.appendChild(objStyleSizesLI);
            	}
            	/*
            	* Append size name UL into parent DIV
            	*/
            	
            	sizename.appendChild(objStyleSizesUL);
            	
            	/*
			    * Create an element UL for quantity text box
			    */
            	var objStyleSizesUL = document.createElement('ul');
            	
            	for(var i=0;i<sizearray.length;i++){
            		sizeTextArray = sizearray[i].split(":");
            		sizeText = sizeTextArray[1];
            		
            		/*
				    * Create an element LI for quantity text box
				    */
            		var objStyleSizesLI = document.createElement('li');
            		
            		/*
				    * Create an text box for size quantity
				    */
            		var textBox = document.createElement('input');
					var checkBoxIdIndex = i+1;
					textBox.setAttribute("type", "TEXT");
					textBox.setAttribute("name", "size["+sizeText+"]");
					textBox.setAttribute("id", "size_"+i+"_"+menstyleid+womenstyleid);
					if(navigator.appName!="Microsoft Internet Explorer"){
						textBox.setAttribute("class","text");
					}else{
						textBox.setAttribute("className","text");
					}
					//textBox.setAttribute("class", "text");
					/*
	            	* Append Text box into parent Li
	            	*/
					objStyleSizesLI.appendChild(textBox);
					
					/*
	            	* Append LI into parent UL
	            	*/
            		objStyleSizesUL.appendChild(objStyleSizesLI);
            	}
            	if(navigator.appName=="Microsoft Internet Explorer")
					sizequantity.innerHTML ="";
				else
			    	sizequantity.innerHTML =null;
            	
			    /*
            	* Append quantity text box UL into parent DIV
            	*/	
            	sizequantity.appendChild(objStyleSizesUL);
            	
            	var objProductContainerTop = document.getElementById('product_container_top_'+menstyleid+womenstyleid);
            	document.getElementById('product_container_popup_image_'+menstyleid+womenstyleid).value = popup_image;

            	if(navigator.appName!="Microsoft Internet Explorer"){
					objProductContainerTop.setAttribute("style","background:URL("+popup_image+") no-repeat;");
				}else{
					objProductContainerTop.style.setAttribute("cssText","background:URL("+popup_image+") no-repeat;");
				}
            	
				document.getElementById('hidden_size_counter_'+menstyleid+womenstyleid).value = sizearray.length;
				
            	document.getElementById('style_price_'+menstyleid+womenstyleid).innerHTML = price;
            	document.getElementById('hidden_styleid_'+menstyleid+womenstyleid).value = styleid;
            	
            	document.getElementById('style_name_'+menstyleid+womenstyleid).innerHTML = "";
            	document.getElementById('style_name_'+menstyleid+womenstyleid).innerHTML = style_name;
            	
            	document.getElementById('please_wait_'+menstyleid+womenstyleid).style.display="none";
			}else{
				alert(http.status+"===="+http.statusText);
			}
    	}
	}
	http.send(null);
	
}


/*
*Function for validating the size text fields
*/

function cricinfoAddtocart(objFrm,menstyleid,womenstyleid,root_url){
	
	/*
	* Check if total number of quantities are less than 0
	*/
	var size_count = document.getElementById('hidden_size_counter_'+menstyleid+womenstyleid).value;
	
	var totalQuantity = 0;
	for(var i=0;i<size_count;i++){
		totalQuantity=totalQuantity+document.getElementById('size_'+i+'_'+menstyleid+womenstyleid).value;
	}
	if(totalQuantity==0){
		alert("Please enter at least one quantity for selected product!");
		return;
	}else{
		
		var strSubmit='';
		for(i=0;i<objFrm.elements.length;i++){
		formElem=objFrm.elements[i];
			switch(formElem.type){
				case 'text':
				case 'hidden':
				strSubmit+=formElem.name+
				'='+escape(formElem.value)+'&'
				break;
			}
		}
		
		var CRICADDTOCART = root_url + "/modules/affiliates/cricinfoWidgetAddToCart.php";
		http.open('POST', CRICADDTOCART, true);
		http.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
		http.onreadystatechange = function(){
			if(http.readyState==4){
	    		if(http.status==200){
    			/*
				 * Load response from server
				 */
    				var response = http.responseText;
    				
		            if(response.indexOf("TRUE")!=-1){
		            	window.location.href=root_url + "/mkmycart.php?at=cric";
		            }
	    		}else{
				alert(http.status+"===="+http.statusText);
			}
			}
		}
		http.send(strSubmit);
	}
	
}