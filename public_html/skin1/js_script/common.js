/* Put all the js that is in lined here */

function changeBackgroundToGrey(id) {
    var div = document.getElementById(id);
    div.className = "greydiv";
}
function changeBackgroundToWhite(id) {
    var div = document.getElementById(id);
    div.className = "none";
}
function formSubmit() {
    var str = document.getElementById("gosearch").value;
    var str = str.replace(" ", "-");

    if ((str == "") || (str == "search here") || (str == "search other designs here")) {
        alert("Please enter search keyword.");
        return false
    } else {
        document.getElementById("basicsearchform").action = http_loc + '/' + str + '/search/'
        document.getElementById("basicsearchform").submit();
    }
}
function ValidateForm() {
    var str = document.getElementById("txtSearch").value;
    if ((str == "") || (str == "search here") || (str == "search other designs here")) {
        alert("Please enter search keyword.");
        return false
    }

}
jQuery.easing['BounceEaseOut'] = function(p, t, b, c, d) {
    if ((t /= d) < (1 / 2.75)) {
        return c * (7.5625 * t * t) + b;
    } else if (t < (2 / 2.75)) {
        return c * (7.5625 * (t -= (1.5 / 2.75)) * t + .75) + b;
    } else if (t < (2.5 / 2.75)) {
        return c * (7.5625 * (t -= (2.25 / 2.75)) * t + .9375) + b;
    } else {
        return c * (7.5625 * (t -= (2.625 / 2.75)) * t + .984375) + b;
    }
};


$(document).ready(function() {
    // Load our carousel ..
    if($('#mycarousel').length){
        $('#mycarousel').jcarousel({animation:200,scroll:4});    
        // Any tips we wanna show
        $(".with-tooltip").simpletooltip();
    }

});
/**
 * This function gets called when all the elements on the page have loaded ..
 */
$(window).load(
        function() {
            // cross fade .. 
            if ($('.bigbanner').length > 0) {
                $('.bigbanner').crossSlide({
                    sleep: 2,
                    fade: 1
                }, [
                    {
                        src:cdn_base+"/skin1/mkimages/fifa-home-page-banner.jpg" ,
                        href: http_loc + '/soccer-football-jerseys'
                    },
                    {
                        src:cdn_base+"/skin1/mkimages/bag-banner-new.jpg",
                        href: http_loc + '/laptop-bags-backpacks'
                    }

                ]);
            }
            /* if ($('.left-banner').length > 0) {
             $('.left-banner').crossSlide({
             sleep: 2,
             fade: 1,
             callback: changeHeader
             }, [
             {
             src:cdn_base+"/skin1/mkimages/ganguly-banner.jpg",
             href: http_loc + '/sourav-ganguly-tshirts'
             },
             {
             src:cdn_base+"/skin1/mkimages/twestival-banner.jpg" ,
             href: http_loc + '/TwestivalIndia'
             }

             ]);
             }*/

            // load queued images  .. 
            if (typeof(image_queue) == 'undefined') {
                return;
            }

            $.each(image_queue, function(index, value) {
                $('#' + index).attr("src", value);
            });


            // load support suite ..
            /*
             setTimeout(function() {
             if (parseInt($.browser.version) > 6) {
             //                    $(".helpIframe").css('height', '30px');
             //		    $(".helpIframe").css('width', '30px');

             //                    $(".helpIframe").css('margin-top', '10px');
             } else if (parseInt($.browser.version) == 6) {
             //                    $(".helpIframe").css('margin-top', '0px');
             }
             $(".helpIframe").attr('src', http_loc + '/skin1/help/helpserve.html');

             }, 3000);
             */

        }
        );


// legacy functions used globally .. need to revisit and refactor ..
function substitute(lbl) {
    var x, rg;
    for (x = 1; x < arguments.length; x += 2) {
        if (arguments[x] && arguments[x + 1]) {
            rg = new RegExp("\\{\\{" + arguments[x] + "\\}\\}", "gi");
            lbl = lbl.replace(rg, arguments[x + 1]);
            rg = new RegExp('~~' + arguments[x] + '~~', "gi");
            lbl = lbl.replace(rg, arguments[x + 1]);
        }
    }
    return lbl;
}

/*
 Check required fields
 */
function checkRequiredFields(lFields, id) {

    var namefield = /[^a-zA-Z]/;
    //emailRe = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)$/;

    if (!lFields || lFields.length == 0)
        return true;
    if (id) {
        for (var x = 0; x < lFields.length; x++) {
            if (lFields[x][0] == id) {
                lFields = [lFields[x]];
                break;
            }

        }
    }
    for (var x = 0; x < lFields.length; x++) {
        if (!lFields[x] || !document.getElementById(lFields[x][0]))
            continue;
        var obj = document.getElementById(lFields[x][0]);
        if (obj.value == '' && (obj.type == 'text' || obj.type == 'password' || obj.type == 'textarea')) {
            if (lbl_required_field_is_empty != '') {
                if (lFields[x][1] == 'Username')
                    alert(substitute(lbl_required_field_is_empty, 'field', 'E-mail'));
                else
                    alert(substitute(lbl_required_field_is_empty, 'field', lFields[x][1]));
            } else {
                alert(lFields[x][1]);
            }

            if (!obj.disabled && obj.type != 'hidden') {
                checkRequiredShow(obj);
                obj.focus();
            }
            return false;
        }
        /**************************************************************/
        /*Code By: Nikhil Gupta						Date: 30/04/2007  */
        /*Purpose: Essential form validation						  */
        /**************************************************************/
        if (obj.value != '' && (obj.type == 'text' || obj.type == 'password' || obj.type == 'textarea')) {

            /*if(lFields[x][0] == 'firstname'){
             var fieldvalue = obj.value;
             if(fieldvalue.match(/[^a-zA-Z]/)){
             alert("Please input valid "+lFields[x][1]);
             obj.focus();
             return false;
             }
             }
             if(lFields[x][0] == 'lastname'){
             var fieldvalue = obj.value;
             if(fieldvalue.match(/[^a-zA-Z ]/)){
             alert("Please input valid "+lFields[x][1]);
             obj.focus();
             return false;
             }
             } */
            if (lFields[x][0] == 'uname') {
                var fieldvalue = obj.value;
                if (!echeck(fieldvalue)) {
                    //alert("Please input valid E-mail id");
                    document.getElementById('uname').focus();
                    return false;
                }
            }
            if (lFields[x][0] == 'passwd1') {
                var fieldvalue = obj.value;
                if (fieldvalue.length < 6) {
                    alert(lFields[x][1] + " should have minimum 6 character");
                    obj.focus();
                    return false;
                }
            }
            if (lFields[x][0] == 'passwd2') {
                if (document.getElementById('passwd1').value != document.getElementById('passwd2').value) {
                    alert("Confirm password must be same as password");
                    obj.focus();
                    return false;
                }
            }
            if (lFields[x][0] == 's_city') {
                var fieldvalue = obj.value;
                if (fieldvalue.match(/[^a-zA-Z]/)) {
                    alert("Please enter valid " + lFields[x][1] + " name");
                    obj.focus();
                    return false;
                }
            }
            if (lFields[x][0] == 'mobile') {
                var fieldvalue = obj.value;
                if (fieldvalue.match(/[^0-9]/)) {
                    alert("Please enter valid mobile number");
                    obj.focus();
                    return false;
                }
                if (fieldvalue.length < 10) {
                    alert("Mobile number must have atleast 10 digit");
                    obj.focus();
                    return false;
                }
            }
            if (lFields[x][0] == 'phone') {
                var fieldvalue = obj.value;
                if (fieldvalue.match(/[^0-9]/)) {
                    alert("Please enter valid " + lFields[x][1]);
                    obj.focus();
                    return false;
                }
            }
            if (lFields[x][0] == 's_zipcode') {
                var fieldvalue = obj.value;
                if (fieldvalue.match(/[^0-9]/)) {
                    alert("Please enter valid " + lFields[x][1]);
                    obj.focus();
                    return false;
                }
                if (fieldvalue.length < 6 || fieldvalue.length > 6) {
                    alert(lFields[x][1] + " must have 6 digit");
                    obj.focus();
                    return false;
                }
            }
            //return false;
        }
        /****************************************************************/
    }
    return true;
}

/*
 Show hidden element and element's parents
 */
function checkRequiredShow(elm) {
    if (elm.style && elm.style.display == 'none') {

        if (elm.id == 'ship_box' && document.getElementById('ship2diff')) {
            /* Exception for Register page */
            document.getElementById('ship2diff').checked = true;
            document.getElementById('ship2diff').onclick();

        } else
            elm.style.display = '';
    }

    if (elm.parentNode)
        checkRequiredShow(elm.parentNode);

}

function checkEmailAddress(field, empty_err) {
    var err = false;
    var res, x;
    if (!field)
        return true;

    if (field.value.length == 0) {
        if (empty_err != 'Y')
            return true;
        else
            err = true;
    }

    var arrEmail = field.value.split('@');
    if (arrEmail.length != 2 || arrEmail[0].length < 1)
        err = true;
    if (!err) {
        if (arrEmail[0].length > 2)
            res = arrEmail[0].search(/^[-\w][-\.\w]+[-\w]$/gi);
        else
            res = arrEmail[0].search(/^[-\w]+$/gi);
        if (res == -1)
            err = true;
    }
    if (!err) {
        var arr2Email = arrEmail[1].split('.');
        if (arr2Email.length < 2)
            err = true;
    }
    if (!err) {
        var domenTail = arr2Email[arr2Email.length - 1];
        var _arr2Email = new Array();
        for (x = 0; x < arr2Email.length - 1; x++)
            _arr2Email[x] = arr2Email[x];
        arr2Email = _arr2Email;
        var domen = arr2Email.join('.');
        res = domen.search(/^[-!#\$%&*+\\\/=?\.\w^`{|}~]+$/gi);
        if (res == -1)
            err = true;
        res = domenTail.search(/^[a-zA-Z]+$/gi);
        if (res == -1 || domenTail.length < 2 || domenTail.length > 6)
            err = true;
    }
    //	/^([-\w][-\.\w]*)?[-\w]@([-!#\$%&*+\\\/=?\w^`{|}~]+\.)+[a-zA-Z]{2,6}$/gi

    if (err) {
        alert(txt_email_invalid);
        field.focus();
        field.select();
    }
    return !err;
}
var toggle_headtext = 1;
function changeHeader() {

    if (toggle_headtext == 1) {
        $("#left_banner_headtext").html("Sourav Ganguly T-shirts");
        toggle_headtext = 2;
    }
    else {
        $("#left_banner_headtext").html("Twestival India T-shirts");
        toggle_headtext = 1;
    }
}

// Menu code ..
$(window).load(function() {

    //$('ul.ddlist').find('li:first').css('margin-top', '10px');

    $('.dir').mouseover(function () {
        $(this).find('.ddlist').css('visibility', 'visible')
        $(this).find('a').addClass("diractive")
    }
            );
    $('.dir').mouseout(function () {
        $(this).find('.ddlist').css('visibility', 'hidden');
        $(this).find('a').removeClass("diractive");
    });

    $(".dropdown dd a").click(function() {
        $(".dropdown dd ul").toggle();
    });

    $(".dropdown dd ul li a").click(function() {
        var text = $(this).html();
        $(".dropdown dd a span").html(text);
        $(".dropdown dd ul").hide();
    });

    setTimeout(function() {
        if ($('#fbLike').length > 0) {
            $('#fbLike').attr('src', fbLikeURL);
        }
    }, 3000);

});


$(document).ready(function() {
    /* Scrollable */
    $(".vertical-scrollable").scrollable({size:3,vertical:true,keyboard:false,items:".vertical-items"});

    //Fix for vertical scrollable to disable next button on initial load if the number of items is exactly equal to the size of the window
    $(".vertical-scroller").each(function() {
        if ($(".vertical-scrollable .vertical-items div", this).length <= 3) {
            $(".next", this).addClass("disabled");
            $(this).addClass("scroller-disabled");
            $(".product-image-tabs").removeClass("corners");
            $(".product-image-tabs").addClass("corners-tl-o-bl-br");
        }
    });

    $(".vertical-scroller.scroller-disabled .vertical-items div a").click(function() {
        if ($(this).hasClass("first")) {
            $(".product-image-tabs").removeClass("corners");
            $(".product-image-tabs").addClass("corners-tl-o-bl-br");
        }
        else {
            $(".product-image-tabs").removeClass("corners-tl-o-bl-br");
            $(".product-image-tabs").addClass("corners");
        }
    });

    /* Product Description Page Gallery */
    $("div.vertical-tabs").tabs("div.product-image-tabs > div");


    /* Change T shirt Type */
    $("#tshirt-type").change(function() {        
        $("#tab_" + $(this).val()).trigger("click");
    });

    $(".product-preview").click(function() {
        if(validateJerseyForm()){
            $('div.vertical-tabs').data('tabs').getCurrentTab().trigger('click');
        }
    });

    var toggle_text_elements = {};

    /* Toggle Text */
    toggle_text_all(".toggle-text");

    /*
     * This function finds all HTML elements with "toggle-text" class and binds the "focus"/"blur" events for
     * toggling the default text in those elements.
     */
    function toggle_text_all(selector) {
        $(selector).each(function(e) {
            toggle_text_elements[this.id] = $(this).val();
            addToggleText(this);
        });
    }

    function addToggleText(elem) {
        $(elem).addClass("blurred");
        $(elem).focus(function() {
            if ($(this).val() == toggle_text_elements[this.id]) {
                $(this).val("");
                $(this).removeClass("blurred");
            }
        }).blur(function() {
            if ($(this).val() == "") {
                $(this).addClass("blurred");
                $(this).val(toggle_text_elements[this.id]);
            }
        });
    }

});


function validateJerseyForm(){
    if ($('#preview-name').val() == 'Enter a name' || $('#preview-name').val().length <= 0 || $('#preview-name').val().length > 8) {  alert('Please enter a name up to 8 characters long'); return false; }
    var pattern = '^[0-9]{1,2}$';
    if ($('#preview-number').val() == 'Enter a number' || $('#preview-number').val().length <= 0 || $('#preview-number').val().length > 2 || !$('#preview-number').val().match(pattern)) { alert('Please enter a valid number between 0 and 99'); return false; }
    return true;
}

function jersey_preview(image, price, style_id) {
    $('.product_price').text(price);
    $('#style_id').val(style_id);
	var selected_size='';
	if($("#size_list").val())
	{
		selected_size=$("#size_list").val();
	}
    var html = $('#select_list_'+ style_id).html(); 
    $('#size_list').html(html);
	if(selected_size !='')
		$("#size_list").val(selected_size);
    $('#tshirt-type').val(style_id);

    if ($('#preview-name').val() == 'Enter a name') { return; }
    if ($('#preview-number').val() == 'Enter a number') { return; }

    $('.product-image').attr('src', image);
    var name = $('#preview-name').val();
    $('.user_name').show().text(name);
    var number = $('#preview-number').val();

    var innerhtml = '';
    for (var i = 1; i <= number.length; i++) {
        var digit = number.substring((i - 1), i);
        innerhtml += '<div style="width:30px;display:inline;filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src=\'skin1/images/numbers/'+digit+'.png\');"><img src="skin1/images/numbers/'+digit+'.png" style="width: 30px;border:0px;filter:progid:DXImageTransform.Microsoft.Alpha(opacity=0);"/></div>';   
    }

    $('.user_number').show().html(innerhtml);    
}

