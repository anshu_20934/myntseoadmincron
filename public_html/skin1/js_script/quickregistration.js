var LoginURL = http_loc + "/mkajaxlogin.php";
var http = null;
function getHTTPObject() {
    if (window.XMLHttpRequest) {
        http = new XMLHttpRequest();
    }
    else if (window.ActiveXObject) {
        http = new ActiveXObject("Microsoft.XMLHTTP");
    }
    return http;
}
function formValidation(ajaxForm, ignore_attherate) {
    var error = true;
    if (document.getElementById('firstname').value == "firstname") {
        alert("please input first name");
        document.getElementById('firstname').focus();
        var error = false;
        return error;
    }
    if (document.getElementById('lastname').value == "lastname") {
        alert("please input last name");
        document.getElementById('lastname').focus();
        var error = false;
        return error;
    }
    if (document.getElementById('uname').value == "e-mail") {
        alert("please input e-mail id");
        document.getElementById('uname').focus();
        var error = false;
        return error;
    }
    if (!ignore_attherate && document.getElementById('orgid').value != "") {
        var at = "@";
        var user_email = document.getElementById('uname').value;
        var lat = user_email.indexOf(at);
        if (lat != -1) {
            alert("please fill the login id only without using @domain.com");
            document.getElementById('uname').focus();
            var error = false;
            return error;
        }
    }
    else {
        if (!echeck(document.getElementById('uname').value)) {
            document.getElementById('uname').focus();
            var error = false;
            return error;
        }
    }
    if (document.getElementById('passwd1').value == "password") {
        alert("please input password");
        document.getElementById('passwd1').focus();
        var error = false;
        return error;
    }
    if (document.getElementById('passwd1').value.length < 6) {
        alert("password must have minimum 6 character");
        document.getElementById('passwd1').focus();
        var error = false;
        return error;
    }
    if (document.getElementById('passwd2').value == "password") {
        alert("please input confirm password");
        document.getElementById('passwd2').focus();
        var error = false;
        return error;
    }
    if (document.getElementById('passwd1').value != document.getElementById('passwd2').value) {
        alert("confirm password does not match with password");
        document.getElementById('passwd2').focus();
        var error = false;
        return error;
    }
    if (error == true) {
        formData2QueryString(ajaxForm);
    }
}
function formData2QueryString(ajaxForm) {
    document.getElementById("regpopup").style.visibility = "hidden";
    document.getElementById("regpopup").style.display = "none";
    document.getElementById("regloader").style.visibility = "visible";
    document.getElementById("regloader").style.display = "";
    var strSubmit = '';
    var formElem;
    var strLastElemName = '';
    for (i = 0; i < ajaxForm.elements.length; i++) {
        formElem = ajaxForm.elements[i];
        switch (formElem.type) {
            case 'text':
            case 'hidden':
            case 'password':
                strSubmit += formElem.name +
                             '=' + escape(formElem.value) + '&'
                break;
        }
    }
    saveAjax(strSubmit);
}
function saveAjax(strSubmit) {
    getHTTPObject();
    http.open('POST', http_loc + "/mkajaxregistration.php", true);
    http.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    http.onreadystatechange = myPostFunction;
    http.send(strSubmit);
}
function myPostFunction() {
    if (http.readyState == 4) {
        document.getElementById("regloader").style.visibility = "hidden";
        document.getElementById("regloader").style.display = "none";
        document.getElementById("regpopup").style.visibility = "visible";
        document.getElementById("regpopup").style.display = "";
        document.getElementById("loginerror").style.visibility = "hidden";
        document.getElementById("loginerror").style.display = "none";
        var result = http.responseText;
        var returnResult = result.split(",")
        var flag = returnResult[0];
        var script = returnResult[1];
        var length = returnResult.length;
        if (flag.indexOf("S") !== -1) {
            document.getElementById('errorpopup').style.visibility = 'hidden';
            document.getElementById("errormessage").style.visibility = "hidden";
            document.getElementById("errormessage").style.display = "none";
            document.getElementById("displayerror").innerHTML = "";
            document.getElementById("regpopup").style.visibility = "hidden";
            document.getElementById("regpopup").style.display = "none";
            document.getElementById("loginpopup").style.visibility = "hidden";
            document.getElementById("loginpopup").style.display = "none";
            if ((length) > 2) {
                if (returnResult[2] == "productdetail" || returnResult[2] == "createproduct") {
                    var URL = redirectURL(returnResult[2]);
                    document.updatecart.action = URL;
                    document.updatecart.submit();
                }
                else if (returnResult[2] == "couponform") {
                    var couponcode = returnResult[3];
                    document.getElementById('couponcode').value = couponcode;
                    document.couponform.submit();
                }
                else if (returnResult[2] == "mkrequest") {
                    var URL = redirectURL(returnResult[2]);
                    document.corprequest.action = URL;
                    document.corprequest.submit();
                }
                else if (returnResult[2] == 'publish') {
                    var URL = redirectURL(returnResult[2]);
                    if(document.getElementById("referredTo"))
                        document.getElementById("referredTo").value = URL;
                    var x = document.getElementById("hiddenForm");
                    document.getElementById("hiddenForm").submit();
                }
                if (returnResult[2] == "productdetail_doon") {
                    document.doonform.submit();
                }
                else {
                    var refid = returnResult[3];
                    var URL = redirectURL(returnResult[2]);
                    window.location = (returnResult[2] == "refer") ? URL + "/" + refid : URL;
                }
            }
            else
                window.location.reload();
        }
        if (flag.indexOf("F") !== -1) {
            document.getElementById("errormessage").style.visibility = "visible";
            document.getElementById("errormessage").style.display = "";
            document.getElementById("displayerror").innerHTML = script;
        }
        if (flag.indexOf("C") !== -1) {
            document.getElementById("errormessage").style.visibility = "visible";
            document.getElementById("errormessage").style.display = "";
            document.getElementById("displayerror").innerHTML = script;
        }
    }
}
function loginData2QueryString(ajaxForm) {
    document.getElementById("loginpopup").style.visibility = "hidden";
    document.getElementById("loginpopup").style.display = "none";
    document.getElementById("loginloader").style.visibility = "visible";
    document.getElementById("loginloader").style.display = "";
    var strSubmit = '';
    var formElem;
    var strLastElemName = '';
    for (i = 0; i < ajaxForm.elements.length; i++) {
        formElem = ajaxForm.elements[i];
        switch (formElem.type) {
            case 'text':
            case 'hidden':
            case 'password':
                strSubmit += formElem.name +
                             '=' + escape(formElem.value) + '&'
                break;
        }
    }
    loginAjax(strSubmit);
}
function loginAjax(strSubmit) {
    getHTTPObject();
    http.open('POST', LoginURL, true);
    http.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    http.onreadystatechange = myLoginFunction;
    http.send(strSubmit);
}
function myLoginFunction() {
    if (http.readyState == 4) {
        document.getElementById("loginloader").style.visibility = "hidden";
        document.getElementById("loginloader").style.display = "none";
        document.getElementById("loginpopup").style.visibility = "visible";
        document.getElementById("loginpopup").style.display = "";
        document.getElementById("errormessage").style.visibility = "hidden";
        document.getElementById("errormessage").style.display = "none";
        document.getElementById("displayerror").innerHTML = "";
        var result = http.responseText;
        var returnResult = result.split(",")
        var flag = returnResult[0];
        var script = returnResult[1];
        var length = returnResult.length;
        if (flag.indexOf("S") !== -1) {
            document.getElementById('errorpopup').style.visibility = 'hidden';
            document.getElementById("loginerror").style.visibility = "hidden";
            document.getElementById("loginerror").style.display = "none";
            document.getElementById("loginpopup").style.visibility = "hidden";
            document.getElementById("loginpopup").style.display = "none";
            document.getElementById("regpopup").style.visibility = "hidden";
            document.getElementById("regpopup").style.display = "none";
            if ((length) > 2) {
                if (returnResult[2] == "productdetail" || returnResult[2] == "createproduct") {
                    var URL = redirectURL(returnResult[2]);
                    document.updatecart.action = URL;
                    document.updatecart.submit();
                }
                else if (returnResult[2] == "couponform") {
                    var couponcode = returnResult[3];
                    document.getElementById('couponcode').value = couponcode;
                    document.couponform.submit();
                }
                else if (returnResult[2] == "mkrequest") {
                    var URL = redirectURL(returnResult[2]);
                    document.corprequest.action = URL;
                    document.corprequest.submit();
                }
                else if (returnResult[2] == 'publish') {
                    var URL = redirectURL(returnResult[2]);
			
//                    (document.getElementById("referredTo")).value = URL;
                    var x = document.getElementById("hiddenForm");
                    document.getElementById("hiddenForm").submit();
                }
                else if (returnResult[2] == 'designerpage') {
                    var URL = redirectURL(returnResult[2], returnResult[3]);
                    window.location = URL;
                }
                else if (returnResult[2] == 'shoppage') {
                    var URL = redirectURL(returnResult[2], returnResult[3]);
                    window.location = URL;
                }
                else  if (returnResult[2] == "productdetail_doon") {
                  document.doonform.submit();
                }
                else {
                    var refid = returnResult[3];
                    var URL = redirectURL(returnResult[2]);
                    window.location = (returnResult[2] == "refer") ? URL + "/" + refid : URL;
                }
            }
            else
                window.location.reload();
        }
        if (flag.indexOf("F") !== -1) {
            document.getElementById("loginerror").style.visibility = "visible";
            document.getElementById("loginerror").style.display = "";
        }
    }
}
function changeColor(divname) {
    document.getElementById(divname).style.color = "#000000";
}
