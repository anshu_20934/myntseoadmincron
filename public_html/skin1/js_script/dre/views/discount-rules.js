Dre.ViewsCl.DiscountEditRuleList = Backbone.View.extend({
    template :  _.template($('#discount-rule-list-template').html()),
    events : {
        'change .add-new-rule-value' : 'addNewRule'
    },
    initialize: function(options) {
        _.bindAll(this, 'render','addNewRule');
        this.childViews = [];
        this.collection = this.options.collection;
        this.discount = this.options.discount;
        this.render();
    },
    render: function() {
        $(this.el).html(this.template());
        var groups = this.collection.groups();
        this.groupedCollection = [];
        this.lastGroupId = 0;
        var _tv;
        _(groups).each(function(g){
            this.lastGroupId = g > this.lastGroupId ? g : this.lastGroupId ;
            this.groupedCollection.push(new  Dre.Collections.DiscountRules(this.collection.filterByGroup(g)));
        },this);
        _(this.groupedCollection).each(function(gp){
            _tv = new Dre.ViewsCl.DiscountEditRuleListGrouped({collection: gp , discount : this.discount});
            this.childViews.push(_tv);
            $('#discount-rule-group-container', this.el).append(_tv.el);
        }, this);
        return this;
    },
//Change this for "multiple and" rules
    addNewRule: function() {
        var newDiscount = new DiscountRule;
        newDiscount.set({type : $('.add-new-rule-value:checked').val()});
        var dr = new Dre.Collections.DiscountRules(newDiscount.toJSON());
        this.collection = dr;
        this.discount.discountRules = dr;
        $('#discount-rule-group-container', this.el).html(
            new Dre.ViewsCl.DiscountEditRuleListGrouped({
                collection: dr ,
                discount : this.discount,
                group : this.lastGroupId + 1
            }).el
        );

    },
    onClose: function() {
      _(this.childViews).each(function(view) {
        view.close();
      });
    }
});

Dre.ViewsCl.DiscountEditRuleListGrouped = Backbone.View.extend({
	template :  _.template($('#discount-rule-group-template').html()),
	initialize: function(options) {
		_.bindAll(this, 'render');
        this.childViews = [];
		this.collection = this.options.collection;
		this.collection._groupView = this;
		this.discount = this.options.discount;
		this.render();
	},
	render: function() {
		$(this.el).html(this.template());
        var _tv;
		_(this.collection.models).each(function(item){
			item._groupView = this;
			item._parentDiscount = this.discount;
            _tv = new Dre.ViewsCl.DiscountEditRuleItem({model: item});
            this.childViews.push(_tv);
			$('#discount-rule-item-container', this.el).append(_tv.el);
		}, this);
		return this;
	},
    onClose: function() {
      _(this.childViews).each(function(view) {
        view.close();
      });
    }
});

Dre.ViewsCl.DiscountEditRuleItem = Backbone.View.extend({
	template :  _.template($('#discount-rule-item-template').html()),
	events : {
		'click input:checkbox[name=toggleConditions]' : 'toggleConditions',
		'change .benefit' : 'enableBenefit',
        'change .condition': 'enableCondition'
	},
	initialize: function(options) {
		_.bindAll(this, 'render','toggleConditions','enableBenefit','enableCondition');
        this.childViews = [];
		this.model = this.options.model;
		this.render();
	},
	render: function() {
         var _tv,t;
            switch(parseInt(this.model.get('type'))) {
                case 1 :
                case 8 :_tv =  new Dre.ViewsCl.RuleTypeFLAT( {model : this.model});
                                        break;
                case 2 :_tv = new Dre.ViewsCl.RuleTypeBUYXGETY( {model: this.model});
                                        break;
                case 4 :_tv =  new Dre.ViewsCl.RuleTypeFREEITEMS( {model : this.model});
                                        break;
                case 16:_tv = new Dre.ViewsCl.RuleTypeVOUCHER( {model : this.model});
                                        break;
                default :t = '<div style="font-size:20px;color:red;">Please selecte a rule type</div>';
            }
        if(!_.isUndefined(_tv)){
            this.childViews.push(_tv);
            t = _tv.render().el;
        }
        if(this.model._parentDiscount.get('onCart')){
            this.template = _.template($('#oncart-discount-rule-item-template').html());
        } else {
            this.template = _.template($('#discount-rule-item-template').html());
        }
		
		$(this.el).html(this.template(this.model.toJSON()));
		$(this.el).append(t);
		return this;
	},
	toggleConditions: function(){
		Dre.vToggleConditions(this,$('input:checkbox[name=toggleConditions]').is(':checked'));
	},
	enableBenefit: function() {
		Dre.vEnableBenefit(this,$('input:radio[name=benefit]:checked').val());
    },
    enableCondition: function(){
		Dre.vEnableCondition(this,$('input:radio[name=condition]:checked').val());
	},
    onClose: function() {
      _(this.childViews).each(function(view) {
        view.close();
      });
    }
});


Dre.ViewsCl.RuleTypeFLAT = Backbone.View.extend({
    rt_int :'1',
    is_allowed: false,
    events : {
    	'click input:checkbox[name=toggleConditions]'   : 'keepConditionsChecked'
    },
    initialize: function(options) {
	this.is_allowed = Dre.enabledSetRules & this.rt_int;
        _.bindAll(this, 'render','validate','toJSON');
        this.childViews = [];
        this.model = this.options.model;
        this.model._ruleitemview = this;
    },
    render: function() {
	if(this.model._parentDiscount.get('onCart') && this.model._parentDiscount.isNew()){
		this.model.set({type: 8});
	}
		var de_template =  _.template($('#discount-rule-item-condition-template').html());
		$(this.el).html(de_template(this.model.toJSON()));
        de_template =  _.template($('#discount-rt-'+this.rt_int+'-template').html());
        $(this.el).append(de_template(this.model.toJSON()));
        if(this.model._parentDiscount.get('onCart')){
        	$('input:checkbox[name=toggleConditions]' , this.el).attr('checked', 'checked');
		if(this.model.get('type') == 8 ) {
			$('.discount-rule-item-condition-container', this.el).show();
		}
	}
        return this;
    },
    keepConditionsChecked: function(){
    	if(this.model._parentDiscount.get('onCart')){
    		if(!$('input:checkbox[name=toggleConditions]' , this.el).is(':checked')){
    			alert('Conditions can not be unchecked');
    			$('input:checkbox[name=toggleConditions]' , this.el).attr('checked', 'checked');
    		}
    	}
    },
    validate: function(){
	
        if($('input:checkbox[name=toggleConditions]' , this.el).is(':checked')){
            //alert('Flat Conditional not allowed');
            //return false;
        }
		if(!Dre.vValidateRuleCondition(this)) return false;
		if(!Dre.vValidateRuleBenefitFlat(this)) return false;
		return true;
	},
	toJSON: function(){
		var type;
		//Assuming data is validated
		var percent = ($('input:radio[name=benefit]:checked',this.el).val() == 'percentage') ? $('input:text[name=percent]',this.el).val() : '0';
		var amount = ($('input:radio[name=benefit]:checked',this.el).val() == 'amount') ? $('input:text[name=amount]',this.el).val() : '0';
		
		var detail = {
			type: type,
			percent : percent,
			amount  : amount,
			id :  this.model.get('id'),
			discountFreeItems : []
		}
		if($('input:checkbox[name=toggleConditions]' , this.el).is(':checked')) {
			type = 8; //Flat Conditional
			var on_buy_count = ($('input:radio[name=condition]:checked',this.el).val() == 'on_buy_count') ? $('input:text[name=on_buy_count]',this.el).val() : '0';
			var on_buy_amount = ($('input:radio[name=condition]:checked',this.el).val() == 'on_buy_amount') ? $('input:text[name=on_buy_amount]',this.el).val() : '0';
			
			detail.buyCount = on_buy_count;
			detail.buyAmount = on_buy_amount;
			detail.placeHolderText = $('input:text[name=placeHolderText]',this.el).val() ;
			
		} else {
			type = this.rt_int;
		}
		detail.type = type;
		return detail;
	},
    onClose: function() {
      _(this.childViews).each(function(view) {
        view.close();
      });
    }
});


Dre.ViewsCl.RuleTypeVOUCHER = Backbone.View.extend({
    rt_int :'16',
    events: {
      'click input:checkbox[name=toggleConditions]'   : 'keepConditionsChecked'
    },
    freeItemType : '2',//UnlistedStyleLineItem.class
    initialize: function(options) {
        _.bindAll(this, 'render','validate','toJSON');
        this.childViews = [];
        this.model = this.options.model;
        this.model._ruleitemview = this;
    },
    render: function() {
		var de_template =  _.template($('#discount-rule-item-condition-template').html());
		$(this.el).html(de_template(this.model.toJSON()));
        de_template =  _.template($('#discount-rt-'+this.rt_int+'-template').html());
        $(this.el).append(de_template(this.model.toJSON()));
        return this;
    },
    keepConditionsChecked: function(){
        if(!$('input:checkbox[name=toggleConditions]' , this.el).is(':checked')){
            alert('Conditions can not be unchecked');
            $('input:checkbox[name=toggleConditions]' , this.el).attr('checked', 'checked');
        }
    },
    validate: function(){
        if(!$('input:checkbox[name=toggleConditions]' , this.el).is(':checked')) {
            Dre.vValidationHelper.alertFocus(
				'Conditions must be checked',
				$('input:checkbox[name=toggleConditions]',this.el)
			);
            return false;
        }
        /*var voucherCount = $('input:text[name=count]' , this.el).val();
        if(!isUnsignedInteger(voucherCount) || voucherCount < 1){
            Dre.vValidationHelper.alertFocus(
				'Number of Vouchers must be a positive integer and greater than 0',
				$('input:text[name=count]',this.el)
			);
            return false;
        }*/
        var voucherId = $('select[name=voucher_id]',this.el).val();
        if(voucherId == ''){
            Dre.vValidationHelper.alertFocus(
				'Please select a voucher Id',
				$('select[name=voucher_id]',this.el)
			);
            return false;
        }
		if(!Dre.vValidateRuleCondition(this)) return false;
		return true;
	},
	toJSON: function(){
		var type;

        var voucherId = $('select[name=voucher_id]',this.el).val();
        var discountFreeItemsCollection = new Dre.Collections.DiscountFreeItems;

		var discountFreeItemsArray = [];
		var _tempFreeItem = {
			id :  '',
			groupId : 1,
			itemId : voucherId,
			type : this.freeItemType,
            isActive : 1
		};
		discountFreeItemsArray.push(_tempFreeItem);
		discountFreeItemsCollection.add(_tempFreeItem);
		this.model.discountFreeItems = discountFreeItemsCollection;
		var detail = {
			type: type,
			//percent : percent,
			//amount  : amount,
			id :  this.model.get('id'),
            discountFreeItems : discountFreeItemsArray
		}
        type = this.rt_int;
		if($('input:checkbox[name=toggleConditions]' , this.el).is(':checked')) {
			var on_buy_count = ($('input:radio[name=condition]:checked',this.el).val() == 'on_buy_count') ? $('input:text[name=on_buy_count]',this.el).val() : '0';
			var on_buy_amount = ($('input:radio[name=condition]:checked',this.el).val() == 'on_buy_amount') ? $('input:text[name=on_buy_amount]',this.el).val() : '0';
			
			detail.buyCount = on_buy_count;
			detail.buyAmount = on_buy_amount;
			detail.placeHolderText = $('input:text[name=placeHolderText]',this.el).val() ;
			//detail.count = $('input:text[name=count]' , this.el).val();
            detail.count = 1;

		}
        
		detail.type = type;
		return detail;
	},
    onClose: function() {
      _(this.childViews).each(function(view) {
        view.close();
      });
    }
});

Dre.ViewsCl.RuleTypeFREEITEMS = Backbone.View.extend({
    rt_int :'4',
    events: {
      'blur input:text[name=item_id]'   : 'showItemName',
      'click .item-id-list-link'        :  'showItemList'
    },
    freeItemType : '1',//UnlistedStyleLineItem.class
    initialize: function(options) {
		_.bindAll(this, 'render','validate','showItemName');
        this.childViews = [];
		this.model = this.options.model;
		this.model._ruleitemview = this;
    },
    render: function() {
		var de_template =  _.template($('#discount-rule-item-condition-template').html());
		$(this.el).html(de_template(this.model.toJSON()));
        de_template =  _.template($('#discount-rt-'+this.rt_int+'-template').html());
	this.formatted = {};
	this.formatted.itemIdCsv = '';
	var itemIdArr = [];
	var dft = this.model.get('discountFreeItems');
	for(var i=0;i< dft.length;i++){
		itemIdArr[i] = dft[i].itemId;
	}
	this.formatted.itemIdCsv = itemIdArr.valueOf();
	this.totemplate = this.model.toJSON();
	this.totemplate.formatted = this.formatted;
        $(this.el).append(de_template(this.totemplate));
        this.showItemName();
        return this;
    },
    showItemName: function(){
    	if (this.filterFromCsv()){
    		
    	
    		var itemList = ($('input:text[name=item_id]', this.el).val()).split(",");
	        var itemNamesBlock="";
        var itemName;
	        for (var i in itemList ){
	        	var itemId=itemList[i];
        if(itemId != '') {
            _(Dre.Items).each(function(item){
                if(item.style_id == itemId) itemName =  item.product_display_name;
            });
            if(itemName != undefined){
		                itemNamesBlock=itemNamesBlock+'&nbsp;<div style="color:green;">'+itemName+'</div>';
		                itemName=undefined;
            } else {
		            	itemNamesBlock=itemNamesBlock+'&nbsp;<div style="color:red;">No Item with this id : '+itemId+'</div>';
            }
		        }
	        }
	        $('.item_name', this.el).html(itemNamesBlock);
    	}else{
    		
    		
    		$('.item_name', this.el).html('&nbsp;<span style="color:red;">CSV list is not proper</span>');
        }
    },
    showItemList: function(){
        mywindow = window.open("/admin/dre/index.php?a=itemlistpopup", "item-list", "status=0,scrollbars=1,width=400,height=400");
        mywindow.moveTo(0, 0);
    },
    filterFromCsv: function() {
        var style_id_csv_filter = $('input:text[name=item_id]', this.el).val();
        if(style_id_csv_filter != '') {
            var style_id_arr_filter = style_id_csv_filter.split(",");
            var intRegex = /^\d+$/;
            for ( var siaf in style_id_arr_filter ) {
                if(!intRegex.test(style_id_arr_filter[siaf])) {
                        alert("CSV list not proper");
                        return false;
                }
               
            }
        }
        return true;
    },
    validate: function(){
    	this.filterFromCsv();
        var that = this;
		if(!Dre.vValidateRuleCondition(this)) return false;
		if($('input:text[name=item_id]', this.el).val() == '' ) {
			alert('please enter an item id');
			return false;
		} else {
			
	    var itemList = ($('input:text[name=item_id]', this.el).val()).split(",");
			
            var itemMatch = false;
	    var itemInactiveId = 0, itemAlreadyDRId = 0, itemAlreadyId = 0;
            for (var i in itemList)
            	{
            		var itemId=itemList[i];
            _(Dre.Items).each(function(item){
                if(item.style_id == itemId) itemMatch =  true;
            });
            if(!itemMatch) {
                alert('Wrong item id recieved. Please see the list and enter again');
                return false;
            }
			    _(Dre.discountedUnlistedItems).each(function(item){
				if(item.item_id == itemId){
					if(item.is_active == 0){
						itemInactiveId = item.item_id;
					}
					if(item.discount_rule_id != that.model.get('id')){
						itemAlreadyDRId = item.discount_rule_id;
						itemAlreadyId = item.item_id;
					}
				}
			    });
			    if(itemInactiveId != 0 ){
				alert('Item Id ' + itemInactiveId + ' is inactive. Please remove this itemId before saving ');
                                return false;
			    }
			    if(itemAlreadyDRId != 0 ){
				alert('Item Id ' + itemAlreadyId + ' already attached with another discount. Kindly Delete other discount containing this itemId before proceeding');
				return false;
			    }
            	}
        }
		return true;
	},
	toJSON: function(){
		//Assuming data is validated
		var type = this.rt_int;
		var percent = ($('input:radio[name=benefit]:checked',this.el).val() == 'percentage') ? $('input:text[name=percent]',this.el).val() : '0';
		var amount = ($('input:radio[name=benefit]:checked',this.el).val() == 'amount') ? $('input:text[name=amount]',this.el).val() : '0';
		var on_buy_count = ($('input:radio[name=condition]:checked',this.el).val() == 'on_buy_count') ? $('input:text[name=on_buy_count]',this.el).val() : '0';
		var on_buy_amount = ($('input:radio[name=condition]:checked',this.el).val() == 'on_buy_amount') ? $('input:text[name=on_buy_amount]',this.el).val() : '0';
		var itemList = ($('input:text[name=item_id]',this.el).val()).split(",");
		var discountFreeItemsCollection = new Dre.Collections.DiscountFreeItems;
		var discountFreeItemsArray = [];
		var j=0;
		for (var i in itemList){
			var itemId=itemList[i];
		var _tempFreeItem = {
				id : '',
			groupId : 1,
			itemId : itemId,
				type : this.freeItemType,
		                isActive : 1
		};
		discountFreeItemsArray.push(_tempFreeItem);
		discountFreeItemsCollection.add(_tempFreeItem);
		}
		this.model.discountFreeItems = discountFreeItemsCollection;
		this.model.set({discountFreeItems: discountFreeItemsArray});
		var detail = {
			type: type,
			buyCount : on_buy_count,
			buyAmount : on_buy_amount,
			placeHolderText : $('input:text[name=placeHolderText]',this.el).val() ,
			count : 1,
			//discount_id : this.model._parentDiscount.get('id'),
			id :  this.model.get('id'),
			discountFreeItems : discountFreeItemsArray
		};
		return detail;
	},
    onClose: function() {
      _(this.childViews).each(function(view) {
        view.close();
      });
    }
});

Dre.ViewsCl.RuleTypeBUYXGETY = Backbone.View.extend({
    rt_int :'2',
    events: {
      'click input:checkbox[name=toggleConditions]'   : 'keepConditionsChecked'
    },
    initialize: function(options) {
        _.bindAll(this, 'render','validate','toJSON');
        this.childViews = [];
        this.model = this.options.model;
        this.model._ruleitemview = this;
    },
    render: function() {
        var de_template =  _.template($('#discount-rt-'+this.rt_int+'-template').html());
        $(this.el).append(de_template(this.model.toJSON()));
        return this;
    },
    keepConditionsChecked: function(){
        if(!$('input:checkbox[name=toggleConditions]' , this.el).is(':checked')){
            alert('Conditions can not be unchecked');
            $('input:checkbox[name=toggleConditions]' , this.el).attr('checked', 'checked');
        }
    },
    validate: function(){

        if(!$('input:checkbox[name=toggleConditions]' , this.el).is(':checked')) {
            Dre.vValidationHelper.alertFocus(
				'Conditions must be checked',
				$('input:checkbox[name=toggleConditions]',this.el)
			);
            return false;
        }
        
		if(!Dre.vValidateRuleCondition(this)) return false;
		
		var benefitCount = $('input:text[name=count]' , this.el);
		if($(benefitCount).val().length==0) {
			Dre.vValidationHelper.alertFocus(
					'please enter count of free ',
					$(benefitCount)
				);
			return false;
		} else if(!isUnsignedInteger($(benefitCount).val()) || $(benefitCount).val() <= 0) {
			Dre.vValidationHelper.alertFocus(
					'Incorrect value for free Count. Please check if the value entered is an integer and greater than 0',
					$(benefitCount)
				);
			return false;
		}
		
		var buyCount =  $('input:text[name=on_buy_count]',this.el);
		if ($(benefitCount).val() >= $(buyCount).val()){
			Dre.vValidationHelper.alertFocus(
					'buy Count can not be less than or equal to get Count',
					$(benefitCount)
				);
			return false;
		}
		return true;
	},
	toJSON: function(){
		var count = $('input:text[name=count]' , this.el).val();
		var type = this.rt_int;
		var detail = {
			type: type,
			count : count,
			id :  this.model.get('id'),
			placeHolderText : $('input:text[name=placeHolderText]',this.el).val(),
			buyCount :  $('input:text[name=on_buy_count]',this.el).val(),
			discountFreeItems : []
		}
		return detail;
		
	},
    onClose: function() {
      _(this.childViews).each(function(view) {
        view.close();
      });
    }
});

