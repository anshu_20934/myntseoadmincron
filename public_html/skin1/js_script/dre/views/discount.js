Dre.ViewsCl.DiscountEditContainer = Backbone.View.extend({
	template  :  _.template($('#discount-content-template').html()),
	initialize: function(options) {
		_.bindAll(this, 'render','saveDiscount','validateBeforeSave');
        this.childViews = [];
		this.model = this.options.model;
        this.type  = this.options.type;
		this.render();
	},
	events    : {
		'click #saveDiscount'		: 'saveDiscount',
        'change .brand_filter' : 'xStyleFilter',
        //'click .show-style-list'                : 'showStyleList',
		'hover #discountruleenddate'			: 'datetimepicker',
		'hover #discountrulestartdate'			: 'datetimepicker'
	},
	render    : function() {
		this.formatted = {
			expiredOn : this.formatDate(this.model.get('expiredOn')),
			startedOn : this.formatDate(this.model.get('startedOn')),
			isNew : this.model.isNew(),
            onCart: this.model.get('onCart'),
            brand_filter_options : this.brandOptions(),
            
            discountFunding : this.model.get('discountFunding'),
            fundingPercentage : this.model.get('fundingPercentage'),
            fundingTax : this.model.get('fundingTax'),
            fundingBasis : this.model.get('fundingBasis'),
            discountLimit : this.model.get('discountLimit'),

        };

        formatted = this.formatted;
		if(this.model.isNew()){
            var _t_dr = new DiscountRule();
            /*if(this.model.get('onCart')) {
                _t_dr.set({type : 16});
                _t_dr.set({count : 1});
            }*/
			this.model.discountRules.add(_t_dr);
            _t_dr = null;
		}

		$(this.el).html(this.template(this.model.toJSON(), {formatted: this.formatted}));

        var that = this;
        Dre.waitMessage(function(){
            that._ruleTemplate = new Dre.ViewsCl.DiscountEditRuleList({ collection : that.model.discountRules , discount: that.model});
            that.childViews.push(that._ruleTemplate);
            $('#discount-rule-list', that.el).html(that._ruleTemplate.el);
            if(that.model.get('onCart')){
                if (!that.formatted.isNew) {
                   var filters = that.model.get('discountFilters');
                   var excludedBrands = '';
                   for (var i = 0; i < filters.length; i++) {
                        if (filters[i] != undefined && filters[i].brand != null) {
                            excludedBrands += ',' + filters[i].brand;
                        }                       
                   }
                   excludedBrands = excludedBrands.substring(1);
                   $('.stylelist').empty();
                   $('.stylelist',this.el).html(
                            '<br/><br/>Brands to be excluded: '+ excludedBrands);
                }
            } else {
                that.showStyleList();
            }
        },100, 'Fetching data. Please Wait...');
        delete formatted;
		return this;
	},
    showStyleList: function(){
        var that = this;
        that._styleTemplate = new Dre.ViewsCl.DiscountEditStyleList({collection : that.model.discountStyles , discount: that.model});
        $('#discount-style-list', that.el).html(that._styleTemplate.el);
        that.childViews.push(that._styleTemplate);
	},
    brandOptions : function() {
        var brand_filter_options = [];
        if (this.options.model.get('onCart')) {
            var brands = Dre.Styles.brands();
            var filters = this.options.model.get('discountFilters');
            var selectedBrands = [];
            for ( var f in filters ){
                selectedBrands.push(filters[f].brand);
            }
            for (var brand in brands) {
                if(_.indexOf(selectedBrands, brands[brand].toLowerCase()) != -1)
                    selectedStr = ' selected = "selected" '; 
                else
                    selectedStr = '';
                brand_filter_options.push('<option value="'+brands[brand].toLowerCase()+'" ' 
                                           + selectedStr +'>'+brands[brand]+'</option>'); 
             } 
        }
        return brand_filter_options;
    },
	saveDiscount: function(){
		if(this.validateBeforeSave()){
			$('#saveDiscount').attr('disabled',true);
            var that = this;
            Dre.waitMessage(function(){
            	
            	that.model.set({prevDiscountStyles : that.model.previous('discountStyles') });
                // Discount Setters
                var t_name;
                if(that.model.get('isDynamic')) t_name = that.model.get('name');
                else t_name = $('input:text[name=name]').val();

                that.model.set({
                    name : t_name,
                    startedOn : that.toTimestamp($('input:text[name=discountrulestartdate]').val()),
                    expiredOn : that.toTimestamp($('input:text[name=discountruleenddate]').val()),
                    description : $('input:text[name=description]').val(),
                    
                    // Adding discount funding information
                    discountFunding : $('.discount_funding').val() == "select" ? null : $('.discount_funding').val(),
                    fundingPercentage : $('input:text[name=percentage_funding]').val(),
                    fundingTax : $('input:radio[name=funding-tax]:checked').val(),
                    fundingBasis : $('.basis_funding').val() == "select" ? null : $('.basis_funding').val(),
                    discountLimit : $('input:text[name=discount_limit]').val()
                    
                });
                // Discount Setters Ends

                // Style Setters
                var _tempStyleIdMap;
                var _tempStyleIdCollection = new Dre.Collections.DiscountStyles;
                var _tempStyleIdArray = [];
                if(!_.isUndefined(that.selectedStyleCollection)){
                if(that.model.isNew()){
		               	var did = null;
                            } else {
                    var did = that.model.get('id');
                            }
                    _(that.selectedStyleCollection.models).each(function(item){
                        if( Dre.StyleIdMap[item.get('style_id')] == undefined ){
                                _tempStyleIdMap = {
                                    'id' : '',
                                    'styleId': item.get('style_id'),
                                    'discountId': did
                                };
                            } else {
                                _tempStyleIdMap = {
                                    'id' : Dre.StyleIdMap[item.get('style_id')].id,
                                    'styleId': item.get('style_id'),
                                    'discountId': did
                                };
                            }
                            _tempStyleIdCollection.add(_tempStyleIdMap);
                            _tempStyleIdArray.push(_tempStyleIdMap);
                    });
                that.model.discountStyles = _tempStyleIdCollection;
                that.model.set({discountStyles:_tempStyleIdArray});
                }
                // Style Setters Ends

                // Rule Setters
                var _tempDisRule = [];
                _(that.model.discountRules.models).each(function(r) {
                    _tempDisRule.push(r._ruleitemview.toJSON());
                });
		that.model.set({discountRules: {}});
                that.model.set({discountRules: _tempDisRule});
                // Rule Setters Ends

                $('#content_new').html('Saving data...........');
                
                that.model.save(that.model.toJSON(),{
                    success: function(model,response) {
                        $('#content_new').html('Refreshing data...........');
                        Dre.Discounts.fetch({
                            success: function() {
                                refreshStyleMap();
				refreshDiscountedUnlistedItem();
                                $('#content_new').html('Discount saved. <a href="#" >Go back to Home</a>');
                            },
                            error: function() {
                                $('#content_new').html('Discount has been saved. But there is some error in syncing data. Please refresh page');
                            }
                        });
                    },
                    error : function(model, response) {
                        $('#content_new').html('Could not save data.');
                        Dre.Discounts.fetch({
                            success: function() {
                                refreshStyleMap();
				refreshDiscountedUnlistedItem();
                                $('#content_new').append('<br/><br/>Discount <span style="color:red">not saved</span>. <a href="#" >Go back to Home</a>');
                            },
                            error: function(model, response) {
                                $('#content_new').append('<br/><br/>Discount <span style="color:red">not saved</span>. Please refresh page to continue');

                            }
                        });
                        $('#content_new').append('<br/>Error: ');
                        $('#content_new').append(response.responseText);
                        $('#content_new').append('<br/>Refreshing data...........');
                    }
                });
                $('html, body').animate({ scrollTop: 0 }, 0);
            }, 1000, 'Saving Data....');
		}
	},
	validateBeforeSave: function() {
		
		if(!_.isUndefined(this._styleTemplate)){
            if (this.model.get('onCart')) {
                this.selectedStyleCollection = this._styleTemplate;
            } else {
        	    this.selectedStyleCollection = new Dre.Collections.Styles(
        			this._styleTemplate.stylesFiltered.filter(function(data) {
                return (data.get('_isSelectedForDiscount') == true) ;
            }));
            }
		if($('input[name=toggleConditions]').is(':checked') && !this.model.get('onCart')){
	            if(this.selectedStyleCollection.length > Dre.MaxStylePerComboRule){
		        alert('Allowed maximum styles per conditional rule = ' + Dre.MaxStylePerComboRule + ". Please remove some styles to save. ");
			return false;
		    }
		}
        	/*if(this.selectedStyleCollection.length > Dre.MaxStylePerRule && this.model.get('type') == 1){
        		alert('Allowed maximum styles per rule = ' + Dre.MaxStylePerRule + ". Please remove some styles to save. ")
        		return false;
        	}*/
		}
		if(!this.model.get('isDynamic') && $('input:text[name=name]').val()!=undefined ) {
			if($('input:text[name=name]').val().length == 0 ){
			$('input:text[name=name]').focus();
			alert('please enter name');
			return false;
			}
		}
		if($('input:text[name=description]').val() != undefined)
		if($('input:text[name=description]').val().length == 0 ) {
			$('input:text[name=description]').focus();
			alert('please enter description');
			return false;
		}
		if($('input:text[name=discountrulestartdate]') != undefined) {
			$('#discountrulestartdate').datetimepicker();
			if($('input:text[name=discountrulestartdate]').val().length == 0 ) {
			$('input:text[name=discountrulestartdate]').focus();
				alert('please enter Start date');
				return false;
			}
			var start = this.toTimestamp($('input:text[name=discountrulestartdate]').val());
		}
	if($('input:text[name=discountruleenddate]') != undefined) {
		if($('input:text[name=discountruleenddate]').val().length == 0 ) {
			$('#discountruleenddate').datetimepicker();
			$('input:text[name=discountruleenddate]').focus();
			alert('please enter End date');
			return false;
		}
		var end = this.toTimestamp($('input:text[name=discountruleenddate]').val());
	}
    if(end == null || isNaN(end) || start == null || isNaN(start)){
        alert("Please pick dates correctly");
			return false;
    } else {
		if(end<start){
			alert("End date has to be    after the start date");
			return false;
		}
	}
    
    if((!this.model.isNew() && this.model.get('discountFunding') != null) || 
    		(this.model.isNew() && $('.discount_funding').val() != undefined)) {
    	if($('.discount_funding').val().length == 0 || $('.discount_funding').val() == "select") {
    		$('.discount_funding').focus();
    		alert('please enter discount funding department');
    		return false;
    	}
    }

    if((!this.model.isNew() && this.model.get('fundingBasis') != null) || 
    		(this.model.isNew() && $('.basis_funding').val() != undefined)) {
    	if($('.basis_funding').val().length == 0 || $('.basis_funding').val() == "select") {
    		$('.basis_funding').focus();
    		alert('please enter basis of vendor funding');
    		return false;
    	}
    }

    if((!this.model.isNew() && this.model.get('fundingPercentage') != null) || 
    		(this.model.isNew() && $('input:text[name=percentage_funding]').val() != undefined)) {
    	if($('input:text[name=percentage_funding]').val().length == 0 ) {
    		$('input:text[name=percentage_funding]').focus();
    		alert('please enter percentage of vendor funding');
    		return false;
    	}
    }
    if((!this.model.isNew() && this.model.get('discountLimit') != null) || 
    		(this.model.isNew() && $('input:text[name=discount_limit]').val() != undefined)) {
    	if($('input:text[name=discount_limit]').val().length == 0 ) {
    		$('input:text[name=discount_limit]').focus();
    		alert('please enter discount limit');
    		return false;
    	}
    }
    if((!this.model.isNew() && this.model.get('fundingTax') != null) || 
    		(this.model.isNew() && ($('input:radio[name=funding-tax]:checked').val() == undefined ))) {
    	if($('input:radio[name=funding-tax]:checked').val().length == 0) {
    		alert('please choose tax applicability.');
    		return false;
    	}
    } 

	if($('input:text[name=percentage_funding]').val().length > 0 && !this.isValidNumber($('input:text[name=percentage_funding]').val())) {
		$('input:text[name=percentage_funding]').focus();
		alert("Percentage funding should be a number between 1 and 100.");
		return false;
	}
	
	if($('input:text[name=discount_limit]').val().length > 0 && !this.isValidNumber($('input:text[name=discount_limit]').val())) {
		$('input:text[name=discount_limit]').focus();
		alert("Discount Limit should be a number between 1 and 100.");
		return false;
	}
	

	if($('input:radio[name=add-new-rule-value]:checked').val() == undefined || $('input:radio[name=add-new-rule-value]:checked').val().length == 0) {
		alert('please choose a rule type');
		return false;
	} 
	var buyCount = 0;
	var buyAmount = 0;
	var getCount = 0;
	var getAmount = 0;
	var getPercent = 0;
		
        getAmount = parseInt($('input:text[name=amount]').val());;
	if(getAmount != '' && getAmount > 0 ) {
		if($('input:checkbox[name=toggleConditions]' , this.el).is(':checked')) {
			buyCount = $('input:text[name=on_buy_count]').val();
        buyAmount = parseInt($('input:text[name=on_buy_amount]').val());
            if(getAmount>0 && buyAmount>0 && getAmount>buyAmount) {
				Dre.vValidationHelper.alertFocus(
					"You cannot give discount for Rs." + getAmount + "/- while you have the condition for just Rs." + buyAmount + "/-",
					$('input:text[name=amount]')
				);
                    return false;
                }
		} else {
            if(getAmount>0) { buyCount = 1;}
        }
        if(getAmount>0 && buyCount>0) {
			var minPriceInSet;
			var price;
			_(this.selectedStyleCollection.models).each(function(item){
				var styleProperty = Dre.Styles.get(item.get('style_id'));
				if(minPriceInSet == null ) minPriceInSet  = styleProperty.get('price');
                price = styleProperty.get('price');
                if(parseInt(price)<parseInt(minPriceInSet)) minPriceInSet = price;
            });
		}
		if(getAmount>=(minPriceInSet*buyCount)){
			Dre.vValidationHelper.alertFocus(
				"You cannont give discount for more than Rs." + (minPriceInSet*buyCount-1) + "/- as the least priced item in the set is  Rs." + minPriceInSet + "/-",
				$('input:text[name=amount]')
			);
                return false;
            }
	}


	if(1){
		var validated = true;
		_(this.model.discountRules.models).each(function(r) {
			if(!r._ruleitemview.validate()) {
				validated = false;
			}
		});
		if(validated == false )	return validated;

        }
        return true;
    },
    
    
    isValidNumber : function(inputStr) {
    	if(inputStr.indexOf(".") != inputStr.lastIndexOf(".")) {
    		return false;
    	}
    	for (var i = 0; i < inputStr.length; i++) { 
    		var oneChar = inputStr.substring(i, i + 1)
    		if(oneChar == ".") {
    			continue;
    		}
    		if (oneChar < "0" || oneChar > "9") { 
    			return false; 
    		} 
    	}
    	var integerLength = (inputStr.indexOf(".") == -1) ? inputStr.length : inputStr.indexOf(".");  
    	if(integerLength < 1 || integerLength > 3) {
    		return false;
    	}
    	if(integerLength == 1) {
    		if(inputStr == "0") {
    			return false;
    		}
    	}
    	if(integerLength == 3) {
    		if(inputStr.substring(0, 1) > "1") {
    			return false;
    		}
    		if(inputStr.substring(1, 2) > "0" || inputStr.substring(2, 3) > "0") {
    			return false;
    		}
    	}

    	return true; 
    },

	formatDate : function(unix_timestamp) {
	    if(unix_timestamp!='') {
		    var date = new Date(unix_timestamp*1000) ;
		    var dd=date.getDate();
		    if(dd<10)dd='0'+dd;
		    var mm=date.getMonth()+1;
		    if(mm<10)mm='0'+mm;
		    var yyyy=date.getFullYear();
		    var hours = date.getHours();
            if(hours<10)hours='0'+hours;
		    var minutes = date.getMinutes();
            if(minutes<10)minutes='0'+minutes;
		    return String(yyyy+"-"+mm+"-"+dd+" "+hours+":"+minutes);
	    } else {
		    return '';
	    }
	},
	toTimestamp: function(strDate){
		 var datum = Date.parse(this.parseDate(strDate));
		 return datum/1000;
	},
	parseDate: function(input) {
        input = input+':00';
        //input has to be in this format: 2007-06-05 15:26:02
        var regex=/^([0-9]{2,4})-([0-1][0-9])-([0-3][0-9]) (?:([0-2][0-9]):([0-5][0-9]):([0-5][0-9]))?$/;
        var parts=input.replace(regex,"$1 $2 $3 $4 $5 $6").split(' ');
        return new Date(parts[0],parts[1]-1,parts[2],parts[3],parts[4],parts[5]);
	},
	datetimepicker: function() {
		$('#discountruleenddate').datetimepicker();
		$('#discountrulestartdate').datetimepicker();
	},
    onClose: function() {
      _(this.childViews).each(function(view) {
        view.close();
      });
    },

    xStyleFilter: function() {
        $('.stylelist', this.el).html('');
        /* no. of exclusion filters for now: 3 (hardcoded)*/
        delete this.filters;
        this.filters = new Array();
        var that = this;
        var brandsArr = $('.brand_filter').val();
        for (var i = 0; i < brandsArr.length; i++) {
                                    var filter = {
                                        brand : 'all',
                                        articleType : 'all',
                                        fashionType : 'all',
                                        season : 'all',
                                        year : 'all'
                                    };
                                    
                                    filter.brand = brandsArr[i];
                                    if (filter.brand != 'all')
                                        that.filters.push(filter); 
        }

        that.model.set({discountFilters : that.filters});

        Dre.waitMessage(function(){
            $('.stylelist').empty();
            $('.stylelist',this.el).html(
                '<br/><br/>Brands to be excluded: '+ $('.brand_filter').val());
        },1,'Filtering...');
    }
});
