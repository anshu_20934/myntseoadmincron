Dre.ViewsCl.Paginator = Backbone.View.extend({
    template :  _.template($('#paginator-template').html()),
    initialize: function(options) {
        this.numPerPage = this.options.numPerPage;
        this.pageNum = this.options.pageNum;
        this.numTotal = this.options.numTotal;
        /*this.parentView = this.options.parentView;
        this.pagelink = this.options.pagelink;*/
    },
    render: function() {
        var prevPage = 0 ,nextPage = 0;
        var lastPage = parseInt(this.numTotal/this.numPerPage+1);
        $(this.el).html(this.template());
        this.pageNum = parseInt(this.pageNum);
        if(this.pageNum> lastPage || this.pageNum <1 ) this.pageNum = 1;
        if(this.pageNum>1) prevPage = this.pageNum - 1;
        if(this.pageNum < lastPage ) {
            nextPage = this.pageNum+1;
        }

        $(this.el).append('<span >Total:'+this.numTotal+'  &nbsp; &nbsp;');
        $(this.el).append('<span class="backbone-link paginator-link x-tbar-links-first x-tbar-links" pageNum="1">&nbsp; &nbsp; &nbsp;</span>&nbsp;');
        
        if(prevPage)
            $(this.el).append('<span class="backbone-link paginator-link x-tbar-links-prev x-tbar-links" pageNum="'+prevPage+'">&nbsp; &nbsp; &nbsp;</span>&nbsp;');
        else $(this.el).append('<span class="backbone-link-disabled paginator-link x-tbar-links-prev x-tbar-links" pageNum="1">&nbsp; &nbsp; &nbsp;</span>&nbsp;');

        $(this.el).append('<span class="backbone-link" ><input type="text" class="paginator-input-text" value="'+
            this.pageNum+'" pageNum="'+this.pageNum+'" style="width:24px;"/></span>&nbsp;');
        
        if(nextPage) $(this.el).append('<span class="backbone-link paginator-link x-tbar-links-next x-tbar-links" pageNum="'+nextPage+'">&nbsp; &nbsp; &nbsp;</span>&nbsp;');
        else $(this.el).append('<span class="backbone-link-disabled paginator-link x-tbar-links-next x-tbar-links" pageNum="'+lastPage+'">&nbsp; &nbsp; &nbsp;</span>&nbsp;');
        $(this.el).append('<span class="backbone-link paginator-link x-tbar-links-last x-tbar-links" pageNum="'+lastPage+'">&nbsp; &nbsp; &nbsp;</span>&nbsp;');

        $(this.el).append('<button type="button" id="ext-gen1550" class="x-tbar-links-load x-tbar-links">&nbsp;</button></span>');
        //
        return this;
    }
});

Dre.ViewsCl.HomeRuleList = Backbone.View.extend({
	template :  _.template($('#rulelist-template').html()),
    events: {
        'click .start-date-sort'            : 'startDateSort',
        'click .end-date-sort'              : 'endDateSort',
        'click .description-sort'           : 'descriptionSort',
        'click .id-sort'                    : 'idSort',
        'click .styles-count-sort'           : 'stylesCountSort',
        'click .name-sort'                  : 'nameSort',
        'click .filter-rule-list'           : 'filterRuleList',
        'click .paginator-link'             : 'renderPage',
        'click .x-tbar-links-load'          : 'renderPage',
        'blur input:text[name=rulelist-styleid-search]' : 'styleIdSearchUI',
        'click input:text[name=rulelist-styleid-search]'                            : 'styleIdSearchUI2'
	},
	initialize: function(options) {
        this._pag = Object ;
        this.ruleListViewLimit = this.options.ruleListViewLimit;
        this.pageNum = this.options.pageNum;
        this.childViews = [];
		this.filterDynamic = this.options.filterDynamic;
		this.filterStatus = this.options.filterStatus;
		this.render();
        this.pageNum = 1;
	},
	render: function() {  
		this.collection = Dre.Discounts;
		this.rulelist = this.collection;

		$(this.el).html(this.template());
        this.renderPageLinks();
        
        this.renderRuleList();
		return this;
	},
    renderPageLinks: function(){
        if (this._pag.onClose){
            this._pag.onClose();
        }
        this._pag = new Dre.ViewsCl.Paginator({
            numPerPage : this.ruleListViewLimit,
            pageNum : this.pageNum , numTotal : this.rulelist.length 
        });
        $('.dre-pagination-links', this.el).html(this._pag.render().el);
        this.pageNum = this._pag.pageNum;
    },
    renderPage: function(e){
        if(_.isUndefined($(e.target).attr("pageNum"))){
            this.pageNum = $('.paginator-input-text').val();
        } else {
            this.pageNum = $(e.target).attr("pageNum");
        }
        this.renderPageLinks();
        this.renderRuleList();
    },
    renderRuleList: function(){
        var that = this;
         Dre.waitMessage(function(){
             that.onClose();
            var _tv;
            _tv = new Dre.ViewsCl.HomeRuleListDisplay({
                collection: that.rulelist , ruleListViewLimit : that.ruleListViewLimit, pageNum : that.pageNum
            });
            that.childViews.push(_tv);
            $('.rulelist-display-container', that.el).html(_tv.render().el);
         },1,"Loading rule list...");
    },
    filterRuleList: function(){
        if($('.filter-rule-list-dynamic',this.el).val()=='static'){
            this.rulelist = new Dre.Collections.Discounts(this.collection.getStatic());
        } else if($('.filter-rule-list-dynamic',this.el).val()=='dynamic'){
            this.rulelist = new Dre.Collections.Discounts(this.collection.getDynamic());
        }  else if($('.filter-rule-list-dynamic',this.el).val()=='cart'){
            this.rulelist = new Dre.Collections.Discounts(
                this.collection.filter(function(data) {
                    return (data.get('onCart') == true );
                }));
        }  else {
            this.rulelist = this.collection;
        }
        this.filterStatus = $('.filter-rule-list-enabled').val();
        if(this.filterStatus == 'enabled') {
			this.rulelist = new Dre.Collections.Discounts(this.rulelist.getEnabled());
		} else if(this.filterStatus == 'disabled') {
			this.rulelist = new Dre.Collections.Discounts(this.rulelist.getDisabled());
		}
        var styleidsearch = $('input:text[name=rulelist-styleid-search]',this.el).val();
        if(styleidsearch != '' && isNumber(styleidsearch)){
            var _dId = Dre.StyleIdMap[styleidsearch];
            if(_.isUndefined(_dId)){
               this.rulelist = new Dre.Collections.Discounts();
            } else {
                
              this.rulelist = new Dre.Collections.Discounts(
                this.rulelist.filter(function(data) {
                    return (data.get('id') == _dId.dId );
                }));
            }
            /**/
        }
        this.pageNum = 1;
        this.renderPageLinks();
        this.renderRuleList();
    },
    sorter: function(sortedEl,callbckAsc,callbckDsc){
        var sort = 'desc';
        if($(sortedEl).hasClass('sort-asc')){
            sort = 'desc';
        } else if($(sortedEl).hasClass('sort-desc')) {
            sort = 'asc';
        } else {
            sort = 'asc';
        }
        this.onClose();
        if(sort == 'asc'){
            callbckAsc();
        } else if(sort == 'desc'){
            callbckDsc();
        }
        $('.dre-table-head',this.el).children('.backbone-link').removeClass('sort-asc').removeClass('sort-desc');
        $('.dre-table-head',this.el).children('.backbone-link').find('.sort-asc-image').remove();
        $('.dre-table-head',this.el).children('.backbone-link').find('.sort-desc-image').remove();
        if(sort == 'asc'){
            $(sortedEl).addClass('sort-asc');
            $(sortedEl).append('<span class="sort-asc-image"> &nbsp; &nbsp;</span>');
        } else if(sort == 'desc'){
            $(sortedEl).addClass('sort-desc');
            $(sortedEl).append('<span class="sort-desc-image"> &nbsp; &nbsp;</span>');
        }
        this.rulelist.sort();
        this.renderRuleList();
    },

    startDateSort: function(){
        var that = this;
        this.sorter($('.start-date-sort',this.el),
            function(){
                that.rulelist.comparator = function(discount) {
                    return -discount.get("startedOn");
                }
            },
            function(){
                that.rulelist.comparator = function(discount) {
                    return discount.get("startedOn");
                }
            }
        );
    },
    endDateSort: function(){
        var that = this;
        this.sorter($('.end-date-sort',this.el),
            function(){
                that.rulelist.comparator = function(discount) {
                    return -discount.get("expiredOn");
                }
            },
            function(){
                that.rulelist.comparator = function(discount) {
                    return discount.get("expiredOn");
                }
            }
        );
    },
    descriptionSort: function(){
        var that = this;
        this.sorter($('.description-sort',this.el),
            function(){
                that.rulelist.comparator = function(discount) {
                    if (discount.get("description")) {
                        var str = discount.get("description");
                        str = str.toLowerCase();
                        str = str.split("");
                        str = _.map(str, function(letter) { return String.fromCharCode(-(letter.charCodeAt(0))) });
                        return str;
                    };
                }
            },
            function(){
                that.rulelist.comparator = function(discount) {
                    return discount.get("description");
                }
            }
        );
    },
    idSort: function(){
        var that = this;
        this.sorter($('.id-sort',this.el),
            function(){
                that.rulelist.comparator = function(discount) {
                    return -discount.get("id");
                }
            },
            function(){
                that.rulelist.comparator = function(discount) {
                    return discount.get("id");
                }
            }
        );
    },
    nameSort: function(){
        var that = this;
        this.sorter($('.name-sort',this.el),
            function(){
                that.rulelist.comparator = function(discount) {
                    if (discount.get("name")) {
                        var str = discount.get("name");
                        str = str.toLowerCase();
                        str = str.split("");
                        str = _.map(str, function(letter) { return String.fromCharCode(-(letter.charCodeAt(0))) });
                        return str;
                    };
                }
            },
            function(){
                that.rulelist.comparator = function(discount) {
                     return discount.get("name");
                }
            }
        );
    },
    stylesCountSort: function(){
        var that = this;
        this.sorter($('.styles-count-sort',this.el),
            function(){
                that.rulelist.comparator = function(discount) {
                    return -discount.get("stylesCount");
                }
            },
            function(){
                that.rulelist.comparator = function(discount) {
                    return discount.get("stylesCount");
                }
            }
        );
    },
    styleIdSearchUI: function(){
        if(!isNumber($('input:text[name=rulelist-styleid-search]',this.el).val())){
            $('input:text[name=rulelist-styleid-search]',this.el).css('color','grey');
            $('input:text[name=rulelist-styleid-search]',this.el).val('Styleid');
        }
    },
    styleIdSearchUI2: function(){
        if($('input:text[name=rulelist-styleid-search]',this.el).val()=='Styleid'){
            $('input:text[name=rulelist-styleid-search]',this.el).css('color','black');
            $('input:text[name=rulelist-styleid-search]',this.el).val('');
        }
    },
    onClose: function() {
      _(this.childViews).each(function(view) {
        view.close();
      });
    }
});

Dre.ViewsCl.HomeRuleListDisplay = Backbone.View.extend({
	template :  _.template($('#rulelist-display-template').html()),
	initialize: function(options) {
        this.ruleListViewLimit =  this.options.ruleListViewLimit;
        this.collection = this.options.collection;
        this.childViews = [];
        this.pageNum = parseInt(this.options.pageNum);
        if(this.pageNum == 0) this.pageNum = 1;
		this.render();
	},
	render: function() {
        this.onClose();
        var _tv;
		$(this.el).html(this.template());
        var i = -1;
		for( item in this.collection.models ){
            i++;
            if(i<(this.pageNum-1)*this.ruleListViewLimit) continue;
            if(i>=this.pageNum*this.ruleListViewLimit) break;
            _tv = new Dre.ViewsCl.RuleListItem({model: this.collection.models[item]});
            this.childViews.push(_tv);
			$( this.el).append(_tv.render().el);
		};
		return this;
	},
    onClose: function() {
      _(this.childViews).each(function(view) {
        view.close();
      });
    }
});


Dre.ViewsCl.RuleListItem = Backbone.View.extend({
	template: _.template($('#rulelistitem-template').html()),
	events: {
        'change select[name=disableEnableRule]' : 'disableEnableRule',
        'click .get-discount-details'           : 'fetchDetails',
        'click .edit-discount-link'             : 'editDiscountRuleLink',
        'click .delete-rule'                    : 'deleteRule'
	},
	initialize: function(){
        _.bindAll(this, 'render','disableEnableRule','fetchDetails','editDiscountRuleLink');
        this.childViews = [];
	},
	render: function(){
        var _tv;
		formatted = {
			expiredOn : this.formatDate(this.model.get('expiredOn')),
			startedOn : this.formatDate(this.model.get('startedOn')),
			numStyles : _.size(this.model.get('discountStyles')),
			description : this.formatDescription(this.model),
			stylesCount : this.model.get('stylesCount') ? this.model.get('stylesCount') : this.model.get('discountStyles').length 
        };
		$(this.el).html(this.template(this.model.toJSON(), {formatted : formatted}));
        delete formatted;
		return this;
	},
	editDiscountRuleLink: function() {
		window.location.hash="cl/discount/edit/"+this.model.get('id');
	},
    deleteRule: function(){
      $(this.el).children().css('background-color','pink');
      var delConfirm = confirm("Delete '"+this.model.get('name')+"' - rule id "+this.model.get('id'));
      if(delConfirm) {
          this.model.destroy({
            success: function(model, response){
                alert('Deleted');
            },
            error: function(model,response){
                alert('Error in deleting. Error :-'+response.responseText);
                //console.log(response);
            }
          });
          this.remove();
      }
      $(this.el).children().css('background-color','white');
      
	},
	fetchDetails: function() {
		if(this.model.discountRules.length) {
			$('.home-rule-list-info-list',this.el).html(new Dre.ViewsCl.HomeRuleListInfoList({
						collection : this.model.discountRules
			}).el);
		} else {
			$('.home-rule-list-info-list',this.el).html('fetching rules data .. ');
			this.model.discountRules._tview = this;
			this.model.discountRules.fetch({
				success: function(collection, response) {
					$('.home-rule-list-info-list',collection._tview.el).html(new Dre.ViewsCl.HomeRuleListInfoList({collection: collection}).el);
				},
				error: function(collection, response) {
					alert('error fetching details');
				}
			});
		}

		if(this.model.discountStyles.length) {
			$('.home-rule-list-style-list',this.el).html(new Dre.ViewsCl.HomeRuleListStyleList({
						collection : this.model.discountRules
			}).el);
		} else {
			$('.home-rule-list-style-list',this.el).html('fetching styles data .. ');
			this.model.discountStyles._tview = this;
			this.model.discountStyles.fetch({
				success: function(collection, response) {
					$('.home-rule-list-style-list',collection._tview.el).html(new Dre.ViewsCl.HomeRuleListStyleList({collection: collection}).el);
				},
				error: function(collection, response) {
					alert('error fetching details');
				}
			});
		}

	},
	disableEnableRule: function() {
		var val = $( 'select[name=disableEnableRule]', this.el).val();
		var msg ;
		if(val == 'Enable') {
			$( 'select[name=disableEnableRule]', this.el).removeClass("colr").addClass('colg');
			this.model.set({isEnabled : true});
			msg = 'Rule Enabled';
		} else {
			$( 'select[name=disableEnableRule]', this.el).removeClass("colg").addClass('colr');
			this.model.set({isEnabled : false});
			msg = 'Rule Disabled';
		}
        
        if(this.model.get('onCart') == true && this.model.get('isEnabled') == true){
            if(Dre.Discounts.getOnCartEnabled().length > 1){
                this.model.set({isEnabled : false});
                $( 'select[name=disableEnableRule]', this.el).removeClass("colg").addClass('colr');
                $( 'select[name=disableEnableRule]', this.el).val('Disable');
                alert("Already one Cart level rule in enabled state. Please disable all other cart rules");
                return false;
            }
        }

        $('.disable-enable-message-span', this.el).show();
		$('.disable-enable-select-span', this.el).hide();
		this.model.save(this.model.toJSON(),{
				success: function(model,response) {
						alert (msg);
						$('.disable-enable-message-span', this.el).hide();
						$('.disable-enable-select-span', this.el).show();
				},
				error : function(model, response) {
						alert('error in disabling/enabling');
						$('.disable-enable-message-span', this.el).hide();
						$('.disable-enable-select-span', this.el).show();
				}
		});
	},
	formatDate: function(unix_timestamp) {
			var date = new Date(unix_timestamp*1000);
			var dd=date.getDate();
			if(dd<10)dd='0'+dd;
			var mm=date.getMonth()+1;
			if(mm<10)mm='0'+mm;
			var yyyy=date.getFullYear();
			return String(dd+"\/"+mm+"\/"+yyyy);
	},
	formatDescription: function(item) {
			return item.get('description');
	},
    onClose: function() {
      _(this.childViews).each(function(view) {
        view.close();
      });
    }
});
