Dre.ViewsCl.DiscountEditStyleList = Backbone.View.extend({
        template :  _.template($('#discount-style-list-template').html()),
        events : {
            'click .csv-filter-button'  :   'filterFromCsv',
            'click .style-csv-pop'      :   'styleCsvPop'
        },
        initialize: function(options) {
            _.bindAll(this, 'render','filterFromCsv','setInitialData');
            this.childViews = [];
            this.viewlist = new Array();
            this.collection = this.options.collection;
            this.discount = this.options.discount;
	    if(typeof this.discount.get('discountFilters')[0] != "undefined"){
		this.filters = {
		        brand_filter : this.discount.get('discountFilters')[0].brand ? this.discount.get('discountFilters')[0].brand.toLowerCase() : 'all',
		        article_type_filter : this.discount.get('discountFilters')[0].articleType ? this.discount.get('discountFilters')[0].articleType.toLowerCase() : 'all',
		        fashion_type_filter : this.discount.get('discountFilters')[0].fashionType ? this.discount.get('discountFilters')[0].fashionType.toLowerCase() : 'all',
		        season_filter : this.discount.get('discountFilters')[0].season ? this.discount.get('discountFilters')[0].season.toLowerCase() : 'all',
		        year_filter : this.discount.get('discountFilters')[0].year ? this.discount.get('discountFilters')[0].year.toLowerCase() : 'all'
		};
		if(!_.isUndefined(this.discount.get('discountFilters')[0].style_id_arr)){
                	this.filters.style_id_arr = this.discount.get('discountFilters')[0].style_id_arr ;
            	}	
	    }
	    else {
		this.filters = {
		        brand_filter :  'all',
		        article_type_filter : 'all',
		        fashion_type_filter : 'all',
		        season_filter :  'all',
		        year_filter : 'all'
            	};
	    }	

            this.setInitialData();
            $(this.el).html(this.template());//Tricky
            this.render();
        },
        render: function() {
            var _tmpView;
            for(var i in this.viewlist) {
                for(var j in this.viewlist[i]._styleViews){
                    this.viewlist[i]._styleViews[j].close();
                }
                this.viewlist[i].close();
            }
            if(!this.stylesDiscountedAttached.length) {
                this.stylesDiscountedAttached =  new Dre.Collections.Styles();
            }
            if(!this.discount.isNew()) {
                _tmpView = new Dre.ViewsCl.DiscountEditStyleListDiscountAttached({ collection: this.stylesDiscountedAttached });
                this.viewlist.push(_tmpView);
                this.childViews.push(_tmpView);
                $('#discount-style-list-discount-attached-container', this.el).html(
                    _tmpView.render().el
                );
            }
            if(!this.stylesNotDiscounted.length) {
                this.stylesNotDiscounted =  new Dre.Collections.Styles();
            }
            if(!this.stylesDiscounted.length){
                this.stylesDiscounted =  new Dre.Collections.Styles();
            }
            _tmpView = new Dre.ViewsCl.DiscountEditStyleListNotDiscount({ collection: this.stylesNotDiscounted });
            this.viewlist.push(_tmpView);
            this.childViews.push(_tmpView);
            $('#discount-style-list-not-discount-container', this.el).html(
                _tmpView.render().el
            );
            _tmpView = new Dre.ViewsCl.DiscountEditStyleListDiscount({ collection: this.stylesDiscounted });
            this.viewlist.push(_tmpView);
            this.childViews.push(_tmpView);
            $('#discount-style-list-discount-container', this.el).html(
                _tmpView.render().el
            );
            return this;
        },
        setInitialData: function() {
            this.stylesFiltered = Dre.Styles.filtered(this.filters);
            this.setViewDataFromCollection();
        },
        setViewDataFromCollection: function(){
            
            var that = this;
            this.stylesNotDiscounted = new Dre.Collections.Styles(this.stylesFiltered.getNotDiscounted());
            if(!this.discount.isNew()){
                this.stylesDiscountedAll = this.stylesFiltered.getDiscounted();
                this.stylesDiscounted = new Dre.Collections.Styles(this.stylesDiscountedAll.filter(function(data) {
                    return (Dre.StyleIdMap[data.get('style_id')]['dId'] != that.discount.get('id') );
                }));
                this.stylesDiscountedAttached = new Dre.Collections.Styles(this.stylesDiscountedAll.filter(function(data) {
                    return (Dre.StyleIdMap[data.get('style_id')]['dId'] == that.discount.get('id') );
                }));
                this.stylesDiscountedAll = null;
            } else {
                this.stylesDiscounted = new Dre.Collections.Styles(this.stylesFiltered.getDiscounted().filter(function(data) {
                    return (Dre.StyleIdMap[data.get('style_id')] != undefined );
                }));
                this.stylesDiscountedAttached = new Dre.Collections.Styles();
                
            }
        },
        filterFromCsv: function() {
            var style_id_csv_filter = $('#csv-file-list', this.el).val();
            if(style_id_csv_filter != '') {
                var style_id_arr_filter = style_id_csv_filter.split(",");
                var intRegex = /^\d+$/;
                for ( var siaf in style_id_arr_filter ) {
                    if(!intRegex.test(style_id_arr_filter[siaf])) {
                            alert("CSV list not proper");
                            return false;
                    }
                    if( _.intersection(style_id_arr_filter, this.stylesFiltered.styleIds()).length == 0 ) {
                            alert("Style Id not exist in this filter");
                            return false;
                    }
                }
                this.stylesFiltered = new Dre.Collections.Styles(this.stylesFiltered.filterByStyleId(style_id_arr_filter));
                this.setViewDataFromCollection();
            } else {
                this.setInitialData();
            }
            this.render();
        },
        styleCsvPop: function(){
            var csv ;
            _(this.discount.get('discountStyles')).each(function(item){
                if(csv == undefined) csv =  item.styleId;
                else csv = csv + "," + item.styleId;
            });
            $('#csv-file-list').val(csv);
        },
        onClose: function() {
          _(this.childViews).each(function(view) {
            view.close();
          });
        }
});


Dre.ViewsCl.DiscountEditStyleListDiscountAttached = Backbone.View.extend({
    template :  _.template($('#discount-style-list-discount-template').html()),
    listLimit : Dre.StyleListViewLimit,
    events: {
        'click input:button[name=allDiscountStylesId]': 'setAllDiscountStyle',
        'click .show-more' : 'showMoreItems',
        'click .show-less' : 'render'    
    },
    formatted: {
        'title' : 'Styles  with this Discount Rule',
        'checkedAll' : true,
	    'showDiscountName' : false
    },
    initialize: function(options) {
        _.bindAll(this, 'render','setAllDiscountStyle');
        this.childViews = [];
        var that = this;
        var _tv;
        this._styleViews = [];
        this.collection = this.options.collection;
        _(this.collection.models).each(function(item){
            item.setSelectedForDiscount();
            _tv = new Dre.ViewsCl.DiscountEditStyleItem({ model: item, showDiscountName : false});
            that._styleViews.push(_tv);
            that.childViews.push(_tv);
        });
    },
    render: function() {
		var that = this;
        if(!this._styleViews.length){
            $(this.el).html('<h3>No styles for this filter, which are attached to this discount rule</h3>');
            return this;
        }
		$(this.el).html(this.template({formatted: this.formatted}));
		var i = 0 ;
		_(this._styleViews).each(function(sv){
			if(i < that.listLimit ) {
				$('.discount-style-item-discount-attached-container', that.el).append(sv.render().el);
			}
            i++;
		});
		var cnt = that.collection.length;
		if (cnt > that.listLimit ) {
			$('.discount-style-item-discount-attached-container', that.el).append(
		'<div class="backbone-link show-more">'+parseInt(cnt - that.listLimit) +'  more styles</div>'
			);
		}
		return this;
    },
    setAllDiscountStyle: function(){
        Dre.vToggleCheckDiscountStyle(this.el,this.collection);
    },
    showMoreItems: function(){
		var that = this;
		Dre.waitMessage( function() {
				$(that.el).html(that.template({formatted: that.formatted}));
                var i = 0;
				_(that._styleViews).each(function(sv){
                    if(i < Dre.StyleListExtendedViewLimit ) {
                        $('.discount-style-item-discount-attached-container', that.el).append(sv.render().el);
                        i++;
                    }
				});
                if(i < that.collection.length){
                  $('.discount-style-item-discount-attached-container', that.el).append(
                    '<div >Showing '+Dre.StyleListExtendedViewLimit+' of '+that.collection.length+'</div>'
                        );
                }
				$('.discount-style-item-discount-attached-container', that.el).append(
			'<div class="backbone-link show-less">Show less styles</div>'
				);
			}, 
			100, 'Processing...');
		
		return this;
    },
    onClose: function() {
      _(this.childViews).each(function(view) {
        view.close();
      });
    }
});

Dre.ViewsCl.DiscountEditStyleListNotDiscount = Backbone.View.extend({
    template :  _.template($('#discount-style-list-discount-template').html()),
    listLimit : Dre.StyleListViewLimit,
    formatted: {
        'title' : 'Styles not attached with any Discount Rule',
        'checkedAll' : false,
	    'showDiscountName' : false
    },
    events: {
        'click input:button[name=allDiscountStylesId]': 'setAllDiscountStyle',
        'click .show-more' : 'showMoreItems',
        'click .show-less' : 'render'
    },
    initialize: function(options) {
        _.bindAll(this, 'render','setAllDiscountStyle','showMoreItems');
        this.childViews = [];
        var _tv;
        var that = this;
        this._styleViews = [];
        this.collection = this.options.collection;
        var i = 0;
        var item;
        while(i < Dre.StyleListExtendedViewLimit && i < this.collection.length){
            item = this.collection.at(i);
            item.unsetSelectedForDiscount();
            _tv = new Dre.ViewsCl.DiscountEditStyleItem({ model: item ,showDiscountName : false});
            that._styleViews.push(_tv );
            that.childViews.push(_tv);
            i++;
        }

 /*       _(this.collection.models).each(function(item){
            item.unsetSelectedForDiscount();
            _tv = new Dre.ViewsCl.DiscountEditStyleItem({ model: item ,showDiscountName : false});
            that._styleViews.push(_tv );
            that.childViews.push(_tv);
        });*/
    },
    render: function() {
		var that = this;
        if(!this._styleViews.length){
            $(this.el).html('<h3>No styles for this filter, which are not attached to any discount rule</h3>');
            return this;
        }
		$(this.el).html(this.template({formatted: this.formatted}));
		var i = 0 ;
		_(this._styleViews).each(function(sv){
			if(i < that.listLimit ) {
				$('.discount-style-item-discount-attached-container', that.el).append(sv.render().el);
			}
				i++;
		});
		var cnt = that.collection.length;
		if (cnt > that.listLimit ) {
			$('.discount-style-item-discount-attached-container', that.el).append(
		'<div class="backbone-link show-more">'+parseInt(cnt - that.listLimit) +'  more styles</div>'
			);
		}
		return this;
    },
    showMoreItems: function(){
		var that = this;
		Dre.waitMessage( function() {
				$(that.el).html(that.template({formatted: that.formatted}));
                var i = 0;
				_(that._styleViews).each(function(sv){
                    if(i < Dre.StyleListExtendedViewLimit ) {
                        $('.discount-style-item-discount-attached-container', that.el).append(sv.render().el);
                        i++;
                    }
				});
                if(i < that.collection.length){
                  $('.discount-style-item-discount-attached-container', that.el).append(
                    '<div >Showing '+Dre.StyleListExtendedViewLimit+' of '+that.collection.length+'</div>'
                        );
                }
				$('.discount-style-item-discount-attached-container', that.el).append(
			'<div class="backbone-link show-less">Show less styles</div>'
				);



			}, 
			100, 'Processing...');
		
		return this;
    },
    setAllDiscountStyle: function(){
        Dre.vToggleCheckDiscountStyle(this.el,this.collection);
    },
    onClose: function() {
      _(this.childViews).each(function(view) {
        view.close();
      });
    }
});

Dre.ViewsCl.DiscountEditStyleListDiscount = Backbone.View.extend({
    template :  _.template($('#discount-style-list-discount-template').html()),
    listLimit : Dre.StyleListViewLimit,
    formatted: {
        'title' : 'Attached with other Discount Rules',
        'checkedAll' : false,
	    'showDiscountName' : true
    },
    events: {
        'click input:button[name=allDiscountStylesId]': 'setAllDiscountStyle',
        'click .show-more' : 'showMoreItems',
        'click .show-less' : 'render'
    },
    initialize: function(options) {
        _.bindAll(this, 'render','setAllDiscountStyle','showMoreItems');
        this.childViews = [];
        var _tv ;
        var that = this;
        this._styleViews = [];
        this.collection = this.options.collection;
        var i = 0;
        var item;
        while(i < Dre.StyleListExtendedViewLimit && i < this.collection.length){
            item = this.collection.at(i);
            item.unsetSelectedForDiscount();
            item.set({_discountAttachedToName : Dre.Discounts.get(Dre.StyleIdMap[item.get('style_id')].dId).get('name')});
            _tv = new Dre.ViewsCl.DiscountEditStyleItem({ model: item, showDiscountName : true});
            that._styleViews.push(_tv);
            that.childViews.push(_tv);
            i++;
        }

/*        _(this.collection.models).each(function(item){
            item.unsetSelectedForDiscount();
	        if(Dre.StyleIdMap[item.get('style_id')].dId != undefined) {
		        item.set({_discountAttachedToName : Dre.Discounts.get(Dre.StyleIdMap[item.get('style_id')].dId).get('name')});
	        } else {
                item.set({_discountAttachedToName : '' });
            }
            _tv = new Dre.ViewsCl.DiscountEditStyleItem({ model: item , showDiscountName : true });
            that._styleViews.push(_tv);
            that.childViews.push(_tv);
        });*/
    },
    render: function() {
        var that = this;
        if(!this._styleViews.length){
            $(this.el).html('<h3>No styles for this filter, which are attached to other discount rule</h3>');
            return this;
        }
        $(this.el).html(this.template({formatted: this.formatted}));
        var i = 0 ;
        _(this._styleViews).each(function(sv){
            if(i < that.listLimit ) {
                $('.discount-style-item-discount-attached-container', that.el).append(sv.render().el);
            }
                i++;
        });
        var cnt = that.collection.length;
        if (cnt > that.listLimit ) {
            $('.discount-style-item-discount-attached-container', that.el).append(
        '<div class="backbone-link show-more">'+parseInt(cnt - that.listLimit) +'  more styles</div>'
            );
        }
        return this;
    },
    showMoreItems: function(){
		var that = this;
		Dre.waitMessage( function() {
				$(that.el).html(that.template({formatted: that.formatted}));
                var i = 0;
				_(that._styleViews).each(function(sv){
                    if(i < Dre.StyleListExtendedViewLimit ) {
                        $('.discount-style-item-discount-attached-container', that.el).append(sv.render().el);
                        i++;
                    }
				});

                if(i < that.collection.length){
                  $('.discount-style-item-discount-attached-container', that.el).append(
                    '<div >Showing '+Dre.StyleListExtendedViewLimit+' of '+that.collection.length+'</div>'
                        );
                }

				$('.discount-style-item-discount-attached-container', that.el).append(
			'<div class="backbone-link show-less">Show less styles</div>'
				);
			}, 
			100, 'Processing...');
		
		return this;
    },
    setAllDiscountStyle: function(){
        Dre.vToggleCheckDiscountStyle(this.el,this.collection);
    },
    onClose: function() {
      _(this.childViews).each(function(view) {
        view.close();
      });
    }
});

Dre.ViewsCl.DiscountEditStyleItem = Backbone.View.extend({
    template :  _.template($('#discount-rule-style-item-template').html()),
    events: {
        'change input:checkbox[name=discountStylesId]': 'setDiscountStyle'
    },
    initialize: function(options) {
            _.bindAll(this, 'render','setDiscountStyle');
            this.childViews = [];
            this.model = this.options.model;
            this.showDiscountName = this.options.showDiscountName;
    },
    render: function() {
            if(!this.showDiscountName) this.model.set({_discountAttachedToName : '' });
            $(this.el).html(this.template(this.model.toJSON()));
            return this;
    },
    setDiscountStyle: function() {
        var checked = $('input:checkbox[name=discountStylesId]', this.el).is(':checked');
        if(checked) {
            this.model.setSelectedForDiscount();
        } else {
            this.model.unsetSelectedForDiscount();
        }
    },
    onClose: function() {
      _(this.childViews).each(function(view) {
        view.close();
      });
    }
});
