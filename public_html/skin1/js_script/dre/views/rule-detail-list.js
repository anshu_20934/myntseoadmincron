
Dre.ViewsCl.HomeRuleListInfoList = Backbone.View.extend({
    template :  _.template($('#rulelist-infolist-template').html()),
    initialize: function(options) {
        this.childViews = [];
        this.collection = this.options.collection;
        this.render();
    },
    render: function() {
        var _tv;
        $(this.el).html(this.template());
         _(this.collection.models).each(function(item){
                _tv = new Dre.ViewsCl.HomeRuleListInfoItem({ model: item });
                $( this.el).append(_tv.render().el);
                this.childViews.push(_tv);
            }, this);
        return this;
    },
    onClose: function() {
      _(this.childViews).each(function(view) {
        view.close();
      });
    }
});

Dre.ViewsCl.HomeRuleListInfoItem = Backbone.View.extend({
    template :  _.template($('#rulelist-infoitem-template').html()),
    initialize: function(options) {
        this.childViews = [];
        this.model = this.options.model;
        this.render();
    },
    render: function() {
        $(this.el).html(this.template(this.model.toJSON()   ));
        return this;
    },
    onClose: function() {
      _(this.childViews).each(function(view) {
        view.close();
      });
    }
});


Dre.ViewsCl.HomeRuleListStyleList = Backbone.View.extend({
    template :  _.template($('#rulelist-stylelist-template').html()),
    initialize: function(options) {
        this.childViews = [];
        this.collection = this.options.collection;
        this.render();
    },
    render: function() {
        $(this.el).html(this.template());
         _(this.collection.models).each(function(item){
                _tv = new Dre.ViewsCl.HomeRuleListStyleItem({ model: item });
                this.childViews.push(_tv);
                $( this.el).append(_tv.render().el);
            }, this);
        return this;
    },
    onClose: function() {
      _(this.childViews).each(function(view) {
        view.close();
      });
    }
});

Dre.ViewsCl.HomeRuleListStyleItem = Backbone.View.extend({
    template :  _.template($('#rulelist-styleitem-template').html()),
    initialize: function(options) {
        this.childViews = [];
        this.model = this.options.model;
        this.render();
    },
    render: function() {
        $(this.el).html(this.template(this.model.toJSON()   ));
        return this;
    },
    onClose: function() {
      _(this.childViews).each(function(view) {
        view.close();
      });
    }
});