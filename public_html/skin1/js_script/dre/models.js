//Models
Discount = Backbone.Model.extend({
    parentOnlyUpdate : false,
    url : function() {
      var base = '/admin/dre/index.php?a=discount';
      if (this.id ) base =  base + '&id=' + this.id;

      if (this.get('discountRules').length == 0 && this.get('discountStyles').length == 0 ) {
          base = base + '&parent=true';
          this.parentOnlyUpdate = true;
      }
      return base;
    },
    initialize: function() {
        this.discountRules = new Dre.Collections.DiscountRules;
        this.discountRules.url = '/admin/dre/index.php?a=discount&id=' + this.id + '&g=rules';
        this.discountStyles = new Dre.Collections.DiscountStyles;
        this.discountStyles.url = '/admin/dre/index.php?a=discount&id=' + this.id + '&g=styles';
        //this.discountRules.bind("reset", this.updateCounts);
    },
    defaults: {
        "name":  "",
        "description": "",
        "expiredOn" : "",
        "isDynamic" : false,
        "isEnabled" : false,
        "startedOn" : "",
        "updatedOn" : "",
        "updatedBy" : Dre.loggedInUser,
        "discountRules": [],
        "discountStyles" : [],
        "discountFilters" : [Dre.DiscountFiltersSelected],
        "onCart":   false,
        
        //Funding related info
        "discountFunding" : "",
        "fundingPercentage" : "",
        "fundingTax" : "",
        "fundingBasis" : "",
        "discountLimit" : ""

      },
      setDynamicName: function() {
        if(this.isNew() && this.get('isDynamic') == true ) {
            if(this.get('discountFilters') != undefined) {
                var temp = this.get('discountFilters');
                var fils = temp[0];
                var nm = fils.brand ;
                if( fils.articleType != 'all') nm = nm + ', ' +fils.articleType;
                if( fils.fashionType != 'all') nm = nm + ', ' +fils.fashionType;
                if( fils.season != 'all') nm = nm + ', ' +fils.season;
                if( fils.year != 'all') nm = nm + ', ' +fils.year;
                this.set({name : nm })
            }
        } else {
            alert('wrong function call');
        }
      }
      /*save: function(attrs, options){
	if(attrs.discountRules != undefined && attrs.discountRules[0] != undefined){
                if(Dre.enabledSetRules & attrs.discountRules[0].type){
                   this._featureAllowed = true;
		} else {
                   this._featureAllowed = false;
                }
	if(this._featureAllowed) {
                return Backbone.Model.prototype.save.call( this, attrs, options );
        } else {
            alert('Rule Type disabled');
            return false;
        }

	}
	else {
	    this.discountRules.fetch({
		success: function(model){
		    console.log(model.get('discountRules'));
		    console.log(model.discountRules);
	if(this._featureAllowed) {
                return Backbone.Model.prototype.save.call( model, attrs, options );
        } else {
            alert('Rule Type disabled');
            return false;
        }

		},
		error: function(){
			alert('something went wrong , please refresh page');
		}
	    });
	}
      }*/
});

DiscountRule = Backbone.Model.extend({
     defaults: {
        "id" : "",
        "percent" : "",
        "amount":"",
        "type":     "1",
        //"is_on_cart" : 0,
        "placeHolderText" :"",
        "buyCount" : "",
        "buyAmount" : "",
        "discountFreeItems" : [],
        "count"     : "0"
      },
      initialize: function() {
        this.discountFreeItems = new Dre.Collections.DiscountFreeItems(new DiscountFreeItem());
        if(this.get('discountFreeItems').length) this.discountFreeItems = new Dre.Collections.DiscountFreeItems(this.get('discountFreeItems'));
        else this.set({discountFreeItems : this.discountFreeItems.toJSON()});
        //this.discountFreeItems.url = '/admin/dre/index.php?a=discount&id=' + this.id + '&g=freeitems';
    }
});

DiscountFreeItem = Backbone.Model.extend({
    defaults: {
        "itemId" : "",
        "groupId"	: "",
        "type"		: ""
    }
});


Style = Backbone.Model.extend({
      setSelectedForDiscount: function(){
          this.set({_isSelectedForDiscount: true});
      },
      unsetSelectedForDiscount: function(){
          this.set({_isSelectedForDiscount: false});
      }
});
