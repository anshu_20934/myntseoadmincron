Dre.ViewsCl.HomePage = Backbone.View.extend({
    template :  _.template($('#home-content-template').html()),
    initialize: function() {
            _.bindAll(this, 'render');
            this.childViews = [];
            this.filterStatus = this.options.filterStatus;
            this.filterDynamic = this.options.filterDynamic;
    },
    render: function() {
        Dre.cleanLocalStorage();
        $(this.el).html(this.template());
        var that = this;
        Dre.waitMessage(function(){
            var _tv = new Dre.ViewsCl.HomeRuleList({
                ruleListViewLimit: Dre.RuleListViewLimit,
                pageNum: 1,
                filterDynamic: that.filterDynamic,
                filterStatus: that.filterStatus
            });
            that.childViews.push(_tv);
            $('#home-rule-list',this.el).html(_tv.el);
        },100,"Loading rule list...");
    },
    onClose: function() {
      _(this.childViews).each(function(view) {
        view.close();
      });
    }
});
Dre.ViewsCl.DiscountEdit = Backbone.View.extend({
        template :  _.template($('#discount-template').html()),
        initialize: function() {
            _.bindAll(this, 'render','renderHtml');
            this.childViews = [];
            this.id = this.options.id;
            this.filters = this.options.filters;
            this.type = this.options.type;
            this.onCart = this.options.onCart;
        },
        render: function() {
			this.formatted = {
				isNew : true
			};

			$(this.el).html(this.template({formatted:this.formatted}));

			if(_.isUndefined(this.id) && !_.isUndefined(this.onCart) && this.onCart == 1 ){
                this.discount = new Discount();
                this.discount.set({isDynamic : false});
                this.discount.set({onCart : true});
                
                this.renderHtml(this.discount,this.loadedCallbackFn);
            }
			else if(_.isUndefined(this.id) && !_.isUndefined(this.filters)) {
			//Create new rule
				this.discount = new Discount();
				var filters = {
					brand : this.filters.brand_filter,
					articleType : this.filters.article_type_filter,
					fashionType : this.filters.fashion_type_filter,
					season : this.filters.season_filter,
					year : this.filters.year_filter
				};
                if(!_.isUndefined(this.filters.style_id_arr))
                    filters.style_id_arr = this.filters.style_id_arr;
                this.discount.set({discountFilters: [filters]});
				if(this.type == 'dynamic') {
					this.discount.set({isDynamic : true});
					this.discount.setDynamicName();
				}
				else this.discount.set({isDynamic : false});
				
				this.renderHtml(this.discount,this.loadedCallbackFn);

			} else {
				this.discount = Dre.Discounts.get(this.id);
				if(_.isUndefined(this.discount)) {
					alert('Please check the discount id');
					window.location.hash="#";
					return;
				}
                var that = this;
                $('#discount-content').html('<br/><span style="color:blue">Loading discount data...</span>');
				this.discount.fetch({
					success:
						function(model,response) {
							//@TODO: need to make these 2 default
							model.discountRules.reset(model.get('discountRules'));
							model.discountStyles.reset(model.get('discountStyles'));
							that.renderHtml(model,that.loadedCallbackFn);
						},
					error: function() {
							alert('Error in getting discount.Please refresh page.');
						}
				}, this);
			}
        },
        loadedCallbackFn: function() {
            //$('#message').html('');
        },
        renderHtml: function(model,callbackFn){
            var that = this;
            Dre.waitMessage(function(){
                var _tv = new Dre.ViewsCl.DiscountEditContainer({
                    model : model
                });
                that.childViews.push(_tv);
                $('#discount-content',that.el).html(_tv.el);
                callbackFn();
            },100,'Fetching data...');
        },
        onClose: function() {
          _(this.childViews).each(function(view) {
            view.close();
          });
        }
});

Dre.ViewsCl.DiscountCreate = Backbone.View.extend({
	template :  _.template($('#createrule-stylefilter-template').html()),
	events: {
		'click .filter_style'           : 'styleFilter',
		'click .create-rule-from-filters': 'createRuleFromFilters'
	},
	initialize: function() {
		_.bindAll(this, 'render','styleFilter','createRuleFromFilters');
        this.childViews = [];
		this.type = this.options.type;
	},
	render: function() {
		Dre.cleanLocalStorage();
		this.discount = new Discount();
		this.styles = Dre.Styles;
		this.brands = this.styles.brands();
		this.articleTypes = this.styles.articleTypes();
		this.fashionTypes = this.styles.fashionTypes();
		this.seasons 	  = this.styles.seasons();
		this.years 	  	  = this.styles.years();

		this.brand_filter_options = [];
		this.article_type_filter_options = [];
		this.fashion_type_filter_options = [];
		this.season_filter_options = [];
		this.year_filter_options = [];
		var selectedStr = '';

		for ( var brand in this.brands ) {
				if(this.brands[brand].toLowerCase() == Dre.DiscountFiltersSelected.brand) selectedStr = ' selected = "selected" ';
				else selectedStr = '';
				this.brand_filter_options.push('<option value="'+this.brands[brand].toLowerCase()+'" '+ selectedStr +'>'+this.brands[brand]+'</option>');
		}
		for ( var article_type in this.articleTypes ) {
				if(this.articleTypes[article_type].toLowerCase() == Dre.DiscountFiltersSelected.articleType) selectedStr = ' selected = "selected" ';
				else selectedStr = '';
				this.article_type_filter_options.push('<option value="'+this.articleTypes[article_type].toLowerCase()+'" '+ selectedStr +'>'+this.articleTypes[article_type]+'</option>');
		}
		for ( var fashion_type in this.fashionTypes ) {
			if(this.fashionTypes[fashion_type].toLowerCase() == Dre.DiscountFiltersSelected.fashionType) selectedStr = ' selected = "selected" ';
			else selectedStr = '';
			this.fashion_type_filter_options.push('<option value="'+this.fashionTypes[fashion_type].toLowerCase()+'" '+ selectedStr +'>'+this.fashionTypes[fashion_type]+'</option>');
		}
		for ( var key in this.seasons ) {
			if(this.seasons[key].toLowerCase() == Dre.DiscountFiltersSelected.season) selectedStr = ' selected = "selected" ';
			else selectedStr = '';
			this.season_filter_options.push('<option value="'+this.seasons[key].toLowerCase()+'" '+ selectedStr +'>'+this.seasons[key]+'</option>');
		}
		for ( var key in this.years ) {
			if(this.years[key].toLowerCase() == Dre.DiscountFiltersSelected.year) selectedStr = ' selected = "selected" ';
			else selectedStr = '';
			this.year_filter_options.push('<option value="'+this.years[key]+'" '+ selectedStr +'>'+this.years[key]+'</option>');
		}

		$(this.el).html(this.template({
			type: this.type,
			styles:  this.styles,
			discount : this.discount,
			brand_filter_options: this.brand_filter_options,
			article_type_filter_options: this.article_type_filter_options,
			fashion_type_filter_options: this.fashion_type_filter_options,
			season_filter_options:		 this.season_filter_options,
			year_filter_options:		 this.year_filter_options
		}));
	},
	styleFilter: function() {
		$('.stylelist', this.el).html('');
		this.filters = {
			brand_filter : 'all'
		};
		this.filters.brand_filter = $('.brand_filter', this.el).val();
		this.filters.article_type_filter = $('.article_type_filter', this.el).val();
		this.filters.fashion_type_filter = $('.fashion_filter', this.el).val();
		this.filters.season_filter = $('.season_filter', this.el).val();
		this.filters.year_filter = $('.year_filter', this.el).val();
		if(this.type=='dynamic' ) {
		var style_id_csv_filter = '';
		} else {
			var style_id_csv_filter = $('#csv-file-list', this.el).val();
		}
		if(style_id_csv_filter != '') {
			var style_id_arr_filter = style_id_csv_filter.split(",");
			var intRegex = /^\d+$/;
			for ( var siaf in style_id_arr_filter ) {
					if(!intRegex.test(style_id_arr_filter[siaf])) {
							alert("CSV list not proper");
							return false;
					}
					if( _.intersection(style_id_arr_filter, this.styles.styleIds()).length == 0 ) {
							alert("Style Id not exist");
							return false;
					}
			}
			this.filters.style_id_arr = style_id_arr_filter;
		} else {
			if(this.filters.brand_filter == 'all' && this.filters.article_type_filter == 'all' && this.filters.fashion_type_filter == 'all' && this.filters.season_filter == 'all' &&
							this.filters.year_filter == 'all' && this.filters.year_filter == 'all'	) {
				alert('Nope. Cant allow that');
				return false;
			}
		}         
		this.stylesFiltered = Dre.Styles.filtered(this.filters);
		var that = this;

		Dre.waitMessage(function(){
			$('.stylelist').empty();
			//that.stylesNotDiscounted = new Dre.Collections.Styles(that.stylesFiltered.getNotDiscounted());
			//that.stylesDiscounted = new Dre.Collections.Styles(that.stylesFiltered.getDiscounted()).filter(function(data) {
			//	return (Dre.StyleIdMap[data.get('style_id')]['dId'] != that.discount.get('id') );
			//});
			$('.stylelist',this.el).html(
				//'<br/><br/>Number of available Styles : '+ that.stylesNotDiscounted.length
				//+ '<br/>Number of unavailable Styles : '+ that.stylesDiscounted.length
				'<br/><span class="backbone-link create-rule-from-filters">Continue to create rule</span>'
			);
		},1,'Filtering...');
	},
	createRuleFromFilters: function(){
		var that = this;
		Dre.waitMessage(function(){
			var currentView = new Dre.ViewsCl.DiscountEdit({filters: that.filters, type : that.type});
			that.close();
			currentView.render();
			$("#content_new").html(currentView.el);
		},1,'Loading..');
	},
    onClose: function() {
      _(this.childViews).each(function(view) {
        view.close();
      });
    }
});
