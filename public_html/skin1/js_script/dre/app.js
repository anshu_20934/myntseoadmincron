
function handleError(msg) {
	alert(msg);
	window.location.hash="#message";
}
function isUnsignedInteger(s) {
  return (s.toString().search(/^[0-9]+$/) == 0);
}
function refreshStyleMap() {
	$.ajax({
		  url: "/admin/dre/index.php?a=refreshstylemap",
		  success: function(d){
			  Dre.StyleIdMap = eval( "(" + d + ")" );
		  }
	});
}
function refreshDiscountedUnlistedItem(){
	$.ajax({
		url: "/admin/dre/index.php?a=refreshdiscountedunlisteditems",
		success: function(d){
			Dre.discountedUnlistedItems = eval( "(" + d + ")" );
		}
	});
}

//Not used anymore ?
function isNumber(n) {
    return isUnsignedInteger(n);
	  //return !isNaN(parseInt(n)) && isFinite(n);
}

function AppView(){
   this.showView=function(view){
        if (this.currentView){
            this.currentView.close();
        }

        this.currentView = view;
        this.currentView.render();

        $("#content_new").html(this.currentView.el);
    }
}

RuleTypesStr = {};
RuleTypesStr[1] = 'Flat';
RuleTypesStr[4] = 'Free Items';
RuleTypesStr[8] = 'Flat ConditionalMain';
RuleTypesStr[32] = 'Free Coupon';

Backbone.View.prototype.close = function(){
  this.remove();
  this.unbind();
  if (this.onClose){
    this.onClose();
  }
}

Dre = {
	loggedInUser : dreLoggedInUser,
	enabledSetRules : 0,
	MaxStylePerRule: 20000,
    MaxStylePerComboRule: 500,
    StyleListViewLimit: 10,
    StyleListExtendedViewLimit: 1000,
    RuleListViewLimit: 30,
    waitMessageOpen: 0,
	waitMessage: function(fn,time,msg) {
		Dre.waitMessageOpen++;
       		 $('#message').show();
		$('#message .message-content').html(msg);
		/*fn();
		Dre.waitMessageOpen--;
		if(Dre.waitMessageOpen==0){
			$('#message').hide();
	        $('#message .message-content').html('');
		}*/
		setTimeout(
            function(){
                fn();
                $('#message').hide();
                $('#message .message-content').html('');
                }
			,time);
	},
    Views: {},
    ViewsCl: {},
    Router: {},
    Collections: {},
    RuleStyleFiltered: {},
    DiscountFiltersSelected : {
        brand : 'all',
        articleType : 'all',
        fashionType : 'all',
        season : 'all',
        year : 'all',
        updatedBy : '' //Send empty to take care of service integration
    },
    StyleIdMap : new Object(),
    discountedUnlistedItems : new Object(),
    cleanLocalStorage: function() {
    },
    init: function() {
	Dre.enabledSetRules = enabled_set_rules;
        Dre.StyleIdMap = discount_style_map;
        Dre.Styles = new Dre.Collections.Styles(styles_json);
        Dre.Discounts = new Dre.Collections.Discounts();
        Dre.Items = items_json;
        Dre.discountedUnlistedItems = discounted_unlisted_items;
        styles_json = [];
        discount_json = [];
        discount_style_map = [];
        items_json = [];
	
        $('#message').html('Fetching data. Please wait ....');
        Dre.Discounts.fetch({
			success: function() {
					$('#message').html('');
					new Dre.Router.Discount();
					Backbone.history.start();
			},
			error: function(data) {
					$('#message').html('Server is down. Please try again later');
			}
        });
    },
    vValidationHelper: {
		alertFocus: function(msg,el){
			alert(msg);
			$(el).focus();
		}
	},
    vToggleConditions: function(view,checked) {
		var c = $('.discount-rule-item-condition-container ', view.el);
		if(checked == true) {
			$(c).show();
		} else if(checked == false){ 
			$('input:text', c).each(function(){
				$(this).val('');
			});
			$('input:radio', c).each(function(){
				$(this).checked = false;  
			});
			$(c).hide();
		}
	},
	vEnableBenefit: function(view,ben){
		var v = $(view.el);
        if(ben == 'percentage') {
            $(".benefit-percentage", v).parent().show();
            $(".benefit-amount", v).parent().hide();
            $(".benefit-amount", v).val('');
        }
        else if(ben == 'amount') {
            $(".benefit-percentage", v).parent().hide();
            $(".benefit-percentage", v).val('');
            $(".benefit-amount", v).parent().show();
        }
        else {
            $(".benefit-percentage", v).parent().hide();
            $(".benefit-amount", v).val('');
            $(".benefit-percentage", v).val('');
            $(".benefit-amount", v).parent().hide();
        }
	},
	vEnableCondition: function(view,ben){
		var v = $(view.el);
		if(ben == 'on_buy_count') {
			$(".condition-count",v).parent().show();
			$(".condition-amount",v).parent().hide();
			$(".condition-amount",v).val('');
		}
		else if(ben == 'on_buy_amount') {
			$(".condition-count",v).parent().hide();
			$(".condition-count",v).val('');
			$(".condition-amount",v).parent().show();
		}
		else {
			$(".condition-amount",v).parent().hide();
			$(".condition-amount",v).val('');
			$(".condition-count",v).parent().hide();
			$(".condition-count",v).val('');
		}
	},
	vValidateRuleCondition: function(view){
		var v = $(view.el);
		if($('input:checkbox[name=toggleConditions]',v).is(':checked') == false) {
			return true;
        }
		if($('input:text[name=placeHolderText]',v).val() == ''){
			Dre.vValidationHelper.alertFocus(
				'please enter the placeholder text',
				$('input:text[name=placeHolderText]',v)
			);
			return false;
		}
		if($('input:radio[name=condition]:checked',v).val() == undefined) {
			alert('please choose a condition');
			return false;
		}
		else {
			var buyCount = 0;
			var buyAmount = 0;
			if($('input:radio[name=condition]:checked',v).val() == 'on_buy_count') {
				if($('input:text[name=on_buy_count]',v).val().length == 0) {
					Dre.vValidationHelper.alertFocus(
						'please enter buy count',
						$('input:text[name=on_buy_count]',v)
					);
					return false;
				}else if(!isUnsignedInteger($('input:text[name=on_buy_count]',v).val()) || $('input:text[name=on_buy_count]',v).val() <= 1) {
					Dre.vValidationHelper.alertFocus(
						'Incorrect value for Buy Count. Please check if the value entered is an integer and greater than 1',
						$('input:text[name=on_buy_count]',v)
					);
					return false;
				}else {
					buyCount = $('input:text[name=on_buy_count]',v).val();
					if(!isUnsignedInteger(buyCount)) {
						Dre.vValidationHelper.alertFocus(
							'Buy Count should be an integer',
							$('input:text[name=on_buy_count]',v)
						);
						return false;
					}
					buyCount = parseInt(buyCount);
					if(buyCount<0) { 
						Dre.vValidationHelper.alertFocus(
							'You can set count only in positive. Negative count of products doesnt make any sense',
							$('input:text[name=on_buy_count]',v)
						);
						return false;
					}
				}
			} else {
				if($('input:text[name=on_buy_amount]',v).val().length == 0) {
					Dre.vValidationHelper.alertFocus(
						'please enter amount',
						$('input:text[name=on_buy_amount]',v)
					);
					return false;
				} else {
					buyAmount = $('input:text[name=on_buy_amount]',v).val();
					if(!isUnsignedInteger(buyAmount)) { alert('Buy amount should be a positive integer.'); return false;}
					if(buyAmount<=0) { alert("You can set amount only in positive."); return false;}
				}
			}

			if(buyCount==0 && buyAmount==0) {alert('Either of the count or amount should be specified - Condition is mandatory'); return false;}
			return true;
		}
	},
	vValidateRuleBenefitFlat: function(view){
		var v = $(view.el);
		if($('input:radio[name=benefit]:checked',v).val() == undefined) {
			alert('please choose a benefit');
			return false;
		}
		else {
			getPercent = $('input:text[name=percent]',v).val();
			getAmount = $('input:text[name=amount]',v).val();
			if($('input:radio[name=benefit]:checked',v).val() == 'percentage') {
				if($('input:text[name=percent]',v).val().length == 0) {
					Dre.vValidationHelper.alertFocus(
						'please enter percentage',
						$('input:text[name=percent]',v)
					);
					return false;
				}else {
					if(!isUnsignedInteger(getPercent)) { alert('Percentage should be a number. No % is required'); return false;}
					getPercent = parseInt(getPercent);
					if(getPercent<=0 || getPercent>=100) { alert("You can set percentage only between 0% and 100%, exclusive"); return false;}
				}
			} else {
				if($('input:text[name=amount]',v).val().length == 0) {
					alert('please enter amount');
					return false;
				} else {
					if(!isUnsignedInteger(getAmount)) { alert('Amount should be a number. Rs. 22,000 should be written as 22000.'); return false;}
					getAmount = parseInt(getAmount);
					if(getAmount<0) { alert("You can set amount only in positive. Negative amount doesnt have any sense."); return false;}
					var buyAmount = parseInt($('input:text[name=on_buy_amount]',v).val());
                                        if(isUnsignedInteger(buyAmount) && buyAmount > 0 && getAmount >= buyAmount ) {
						alert("Benefit more than Condition");
						return false;
					}
				}
			}
			if(getPercent==0 && getAmount==0) {alert('Either of the percent or amount should be specified - Benefit is mandatory'); return false;}
		}
		return true;
	},
    vToggleCheckDiscountStyle: function(el,c){
        var checkedAll = $('input:button[name=allDiscountStylesId]', el).hasClass('allDiscountStylesIdchecked');
        if(checkedAll) $('input:button[name=allDiscountStylesId]', el).removeClass('allDiscountStylesIdchecked');
        else $('input:button[name=allDiscountStylesId]', el).addClass('allDiscountStylesIdchecked');
        $(".discount-style-item-discount-attached-container", el).find('input:checkbox[name=discountStylesId]').attr('checked',checkedAll);
        Dre.waitMessage( function() {
			_(c.models).each(function(item){
                if(checkedAll) {
                    item.setSelectedForDiscount();
                } else {
                    item.unsetSelectedForDiscount();
                }
			});
		}, 100 , 'Processing..');
    }
};

Backbone.emulateHTTP = true;
Backbone.emulateJSON = true;
