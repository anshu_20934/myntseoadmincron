	//Routers
	Dre.Router.Discount = Backbone.Router.extend({
        initialize: function(){
            this.appView = new AppView();
        },
       
		routes: {
			""                                      :	"indexClean",
            "cl/:filterDynamic/:filterStatus"       :   "indexClean",
            "cl/discount/edit/:id"                  :   "discountEditClean",
			"discount/type/:type"                   :   "discountCreateClean",
            "discount/cart/create"                   :   "discountCartCreateClean",
			"*actions"                              :	"defaultRoute"
		},
		defaultRoute: function( actions ){
                    alert( 'Page does not exist' );
		},
		indexClean: function(filterDynamic,filterStatus) {
			$('html, body').animate({ scrollTop: 0 }, 0);
			var homeView = new Dre.ViewsCl.HomePage({filterDynamic: filterDynamic,filterStatus:filterStatus});
            this.appView.showView(homeView);
		},
		discountEditClean: function(id) {
			$('html, body').animate({ scrollTop: 0 }, 0);
			var disEditView = new Dre.ViewsCl.DiscountEdit({id: id});
            this.appView.showView(disEditView);
		},
        discountCartCreateClean: function(id) {
			$('html, body').animate({ scrollTop: 0 }, 0);
			var disCartView = new Dre.ViewsCl.DiscountEdit({onCart: true});
            this.appView.showView(disCartView);
		},
		discountCreateClean: function(type) {
			$('html, body').animate({ scrollTop: 0 }, 0);
			if(type != 'dynamic') type = 'static';
			var disCreateView = new Dre.ViewsCl.DiscountCreate({type: type});
            this.appView.showView(disCreateView);
		}
	});
