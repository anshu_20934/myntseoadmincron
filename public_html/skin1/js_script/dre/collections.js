//Collections
Dre.Collections.Discounts = Backbone.Collection.extend({
        model: Discount,
        url : '/admin/dre/index.php?a=refreshDiscount',
        initialize: function() {
        },
        /*comparator : function(discount) {
                  return -discount.get("isEnabled") + ", " + -discount.get("isDynamic");
        },*/
        filterByDiscountId: function(id){
                return this.filter(function(data) {
                        return (data.get('id') == id);
                });
        },
        getStatic: function() {
            return this.filter(function(data) {
                    return (data.get('isDynamic') == false );
                });
        },
        getDynamic: function() {
            return this.filter(function(data) {
                    return (data.get('isDynamic') == true );
                });
        },
        getEnabled: function() {
            return this.filter(function(data) {
                    return (data.get('isEnabled') == true );
                });
        },
        getDisabled: function() {
            return this.filter(function(data) {
                    return (data.get('isEnabled') == false );
                });
        },
        getOnCartEnabled: function(){
            return this.filter(function(data){
               return (data.get('isEnabled') == true && data.get('onCart') == true) ;
            });
        }
});
Dre.Collections.DiscountRules = Backbone.Collection.extend({
    model: DiscountRule,
     groups: function() {
                return _.uniq(this.pluck("groupId"));
     },
    filterByGroup: function(groupId){
        return this.filter(function(data) {
            return (data.get('groupId') == groupId);
        });
    }
});
Dre.Collections.DiscountStyles = Backbone.Collection.extend({
});

Dre.Collections.DiscountFreeItems = Backbone.Collection.extend({
    model: DiscountFreeItem
});



Dre.Collections.Styles = Backbone.Collection.extend({
    model: Style,
    url : '/admin/dre/index.php?a=refreshStyle',
    styleIds: function() {
        return this.pluck("style_id");
    },
    append : function(styles) {
        var that = this;
        _.each(styles, function(style) {
                            if (!that.contains(style))
                                that.add(style); 
                       });
    },
    multiFiltered: function(filters) {
        var all = new Dre.Collections.Styles(this.models);
        var fs  = new Dre.Collections.Styles();
        _.each(filters, function(filter) {
                            fs.append(all.filterByBrand(filter.brand));
                        });
        return fs;
    },
    //Returns a collection - and not models
    filtered: function(filters) {
        var fs = new Dre.Collections.Styles(this.models);
        if(filters.brand_filter != 'all') {
            fs.reset(fs.filterByBrand(filters.brand_filter));
        }
        if(filters.article_type_filter != 'all') {
            fs.reset(fs.filterByArticleType(filters.article_type_filter));
        }
        if(filters.fashion_type_filter != 'all') {
            fs.reset(fs.filterByFashion(filters.fashion_type_filter));
        }
        if(filters.season_filter != 'all') {
            fs.reset(fs.filterBySeason(filters.season_filter));
        }
        if(filters.year_filter != 'all') {
            fs.reset(fs.filterByYear(filters.year_filter));
        }
        if(typeof filters.style_id_arr != 'undefined') {
            fs.reset(fs.filterByStyleId(filters.style_id_arr));
        }
        return fs;
    },
    brands : function() {
        return _.sortBy(_.reject(_.uniq(this.pluck("global_attr_brand")), function(b) { return b == null}), function(c){
                return c.toLowerCase();
        });
    },
    articleTypes : function() {
        return _.sortBy(_.reject(_.uniq(this.pluck("global_attr_article_type")), function(b) { return b == null}), function(c){
                return c;
        });
    },
    fashionTypes : function() {
        return _.sortBy(_.reject(_.uniq(this.pluck("global_attr_fashion_type")), function(b) { return b == null}), function(c){
                return c.toLowerCase();
        });
    },
    seasons : function() {
        return _.sortBy(_.reject(_.uniq(this.pluck("global_attr_season")), function(b) { return (b == null || b == '') }), function(c){
                return c.toLowerCase();
        });
    },
    years : function() {
        return _.sortBy(_.reject(_.uniq(this.pluck("global_attr_year")), function(b) { return (b == null || b == '') }), function(c){
                return c.toLowerCase();
        });
    },
    getDiscounted: function(){
        return this.filter(function(data) {
            return (Dre.StyleIdMap[data.get('style_id')] != undefined );
        });
    },
    getNotDiscounted: function(){
        return this.filter(function(data) {
            return (Dre.StyleIdMap[data.get('style_id')] == undefined );
        });
    },
    filterByBrand: function(prop) {
        return this.filter(function(data) {
            return (data.get('global_attr_brand').toLowerCase() == prop);
        });
    },
    filterByArticleType: function(prop) {
        return this.filter(function(data) {
            return (data.get('global_attr_article_type').toLowerCase() == prop);
        });
    },
    filterByFashion: function(prop) {
        return this.filter(function(data) {
            return (data.get('global_attr_fashion_type').toLowerCase() == prop);
        });
    },
    filterBySeason: function(prop) {
        return this.filter(function(data) {
            return (data.get('global_attr_season').toLowerCase() == prop);
        });
    },
    filterByYear: function(prop) {
        return this.filter(function(data) {
            return (data.get('global_attr_year').toLowerCase() == prop);
        });
    },
    filterByStyleId: function(prop) {
        return this.filter(function(data) {
            return (_.indexOf(prop,data.get('style_id')) != -1);
        });
    }
});
