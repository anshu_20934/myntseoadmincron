function sizeAndQuantitySelection(productid, typeid, typename, styleid, stylename, unitprice){
	var http = new getHTTPObject();
	var SizeSelectionURL = http_loc+"/promotions/promotionSelection.php?productid="+productid+"&typeid="+typeid+"&typename="+typename+"&styleid="+styleid+"&stylename="+stylename+"&unitprice="+unitprice;


	http.open("GET", SizeSelectionURL, true);
	http.onreadystatechange = function(){
    	if(http.readyState==4){
    		if(http.status==200){
            	var result=http.responseText;
            	if(result.indexOf("direct_add_to_cart") != -1){
            		window.location = http_loc+"/promotions/promotionAddToCart.php?productqty=1";
            	}else{
	            	document.getElementById("orgquantityselection").style.display="block";
		            document.getElementById("orgquantityselection").innerHTML="";
		            document.getElementById("orgquantityselection").innerHTML=result;            	
            	}
			}else{
				alert(http.status+"===="+http.statusText);
			}
    	}
    }
    http.send(null);
}
