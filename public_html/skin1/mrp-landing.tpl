<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en" xmlns:fb="http://www.facebook.com/2008/fbml">
	<head>
		<META http-equiv="Content-Style-Type" content="text/css">
		<META http-equiv="Content-Script-Type" content="type/javascript">
		<META http-equiv="Content-Type" content="text/html; charset=utf-8">
		{include file="site/header.tpl" }
		{literal}
		    <style type="text/css">
		        .three-box{padding:0 7px}
		        .three-box div{position:relative;width:307px;height: 385px;}
		        .three-box div > div{width:240px;position:absolute;margin: 0 35px;top:190px;font-size:15px;text-align:center;}
		        .three-box div>div+p{position:absolute;bottom:0px;text-align:center;width: 310px;margin: 0 0 6px;}
		        .earn{padding-right:26px;}
		        .faqs b{display:block;font-size:14px;color:#006400;}
		        .faqs i{color: #003399;display: inline;font-style: normal;font-weight: bold;list-style-type: decimal;text-indent: 10px;}
		        .faqs ul{list-style-type:decimal;}
		        .faqs ul li{line-height:25px;}
		        .faqs ul li ul{list-style-type:disc;}
		        .faqs ul li ul li{line-height:20px; margin-bottom: 5px;}
		        .faqs div{margin-bottom:20px;}
		        .bb{border-bottom:1px dashed #666;padding-bottom:15px;}
		        div.bb{padding-bottom:0px;}
		        .faqs p strong{padding:10px 0px;font-weight:normal;display:inline;}
		    </style>
		{/literal}
	</head>
	<body class="{$_MAB_css_classes}">
	{include file="site/menu.tpl"}
	<div class="container clearfix">
	    	
			
			
	<div class="span-33">
	    <div style="border:0px solid gray;width:980px;">
	<div style="margin:0px; padding:0px;line-height:0px;">
	<img src="{$cdn_base}/skin1/images/mynt-club-landingpage-banner.jpg" width="980" height="253" alt="" title="" style="text-decoration:none;border:none;font-family:Arial,Helvetica,sans-serif;color:#666;font-size:12px;"/>
	</div>
	<div style="width: 307px; height: 436px;float: left;margin:0px 0 0 14px; line-height:21px; background-image:url('{$cdn_base}/skin1/images/signup-banner.jpg');padding-left:15px;background-repeat:no-repeat;" align="center">
	<span style="margin:190px 0 0 5px; display:block;color:black;font-family:Arial, Helvetica, sans-serif;font-size:17px;">
	Sign up now and get<br>
	<strong style="color:black;font-weight:bold;font-family:Arial, Helvetica, sans-serif;font-size:17px;"> Rs.{$mrpCouponConfiguration.mrp_firstLogin_nonfbreg_numCoupons*$mrpCouponConfiguration.mrp_firstLogin_nonfbreg_mrpAmount}* </strong>credited to
	your <br>Mynt Club account,
	all for<br> your shopping
	delight.
	</span>
	<div style="margin-top:40px;">
	{if $login}
		<img src="{$cdn_base}/skin1/images/signup-button.jpg" style="border:none;"/>
	{else}
	<a href="{$http_location}/register.php"><img src="{$cdn_base}/skin1/images/signup-button.jpg" style="border:none;"/></a>
	{/if}
	</div>
	</div>
	<div style="width: 307px; height: 436px;float: left;margin:10px 0 0 10; padding:0px;line-height:21px; background-image:url('{$cdn_base}/skin1/images/refferandwin-banner.jpg');padding-left:15px;background-repeat:no-repeat;" align="center">
	<span style="margin:190px 0 0 5px; display:block;color:black;font-family:Arial, Helvetica, sans-serif;font-size:17px;">
	Refer your friends, and<br>
	 earn<strong style="color:black;font-weight:bold;font-family:Arial, Helvetica, sans-serif;font-size:17px;"> Rs.{$mrpCouponConfiguration.mrp_refRegistration_numCoupons*$mrpCouponConfiguration.mrp_refRegistration_mrpAmount}* </strong>for every<br>
	 friend who registers, and<br><strong style="color:black;font-weight:bold;font-family:Arial, Helvetica, sans-serif;font-size:17px;"> Rs.{$mrpCouponConfiguration.mrp_refFirstPurchase_numCoupons*$mrpCouponConfiguration.mrp_refFirstPurchase_mrpAmount}* </strong>
	on the first purchase <br>of each of your friends.
	</span>
	<div style="margin-top:18px;">
	{if $login}
		<a href="{$http_location}/mymyntra.php?view=myreferrals"><img src="{$cdn_base}/skin1/images/refferandearn-button.jpg" style="border:none;"/></a>
	{else}
		<a href="{$http_location}/register.php"><img src="{$cdn_base}/skin1/images/refferandearn-button.jpg" style="border:none;"/></a>
	{/if}
	</div>
	</div>
	{if $cashbackearnedcreditsEnabled eq '1'}
	<div style="float: left; width: 307px; height: 436px;margin:10px 0 0 10; padding:0px;line-height:22px; background-image:url('{$cdn_base}/skin1/images/cashback_mrp.jpg');padding-left:15px;background-repeat:no-repeat;" align="center">
	<span style="margin:208px 0 0 5px; display:block;color:black;font-family:Arial, Helvetica, sans-serif;font-size:17px;">
	Shop till you drop!<br> Get <strong style="color:black;font-weight:bold;font-family:Arial, Helvetica, sans-serif;font-size:17px;">10% cash back</strong> <br>on every purchase.
	</span>
	<div style="margin-top: 40px;">
	<a href="{$http_location}"><img src="{$cdn_base}/skin1/images/start-shopping-button.jpg"></a>
	</div>
	</div>
	</div>
	{else}
	<div style="float: left; width: 307px; height: 436px;margin:10px 0 0 10; padding:0px;line-height:22px; background-image:url('{$cdn_base}/skin1/images/cashback-banner.jpg');padding-left:15px;background-repeat:no-repeat;" align="center">
	<span style="margin:68px 0 0 5px; display:block;color:black;font-family:Arial, Helvetica, sans-serif;font-size:17px;">
	<strong style="color:black;font-weight:bold;font-family:Arial, Helvetica, sans-serif;font-size:17px;"> 24 X 7 </strong>customer care<br> <br>
	<strong style="color:black;font-weight:bold;font-family:Arial, Helvetica, sans-serif;font-size:17px;"> Latest products </strong> from all brands<br><br>
	<strong style="color:black;font-weight:bold;font-family:Arial, Helvetica, sans-serif;font-size:17px;"> Largest catalog </strong> of sports and<br>casual wear<br><br>
	<strong style="color:black;font-weight:bold;font-family:Arial, Helvetica, sans-serif;font-size:17px;"> Free shipping </strong> throughout India<br><br>
	<strong style="color:black;font-weight:bold;font-family:Arial, Helvetica, sans-serif;font-size:17px;"> Cash on delivery</strong>
	</span>
	<div style="margin-top:60px;">
	</div>
	</div>
	</div>
	{/if}
	</div>
	<div style="clear: both;"></div>
	<br>
	<div class="faqs">
	<h2>Mynt Club - Rewards and Loyalty Program - FAQs</h2>
	<p class="bb">
	    <b>How do I become part of Mynt Club?</b>
		To earn the benefits of Mynt Club, all you need to do is register with Myntra.com. If you're registered with Myntra, then you're already a member of the Mynt Club. Get set to enjoy all the benefits of the program.
    </p>
    <p>
        <b>How do I benefit as a member of Mynt Club?</b>
        <i>- Sign up and earn</i><br>
        Register with Myntra to instantly earn Rs {$nonfbregValue}. This will be added to your Mynt Club account as {$mrpCouponConfiguration.mrp_firstLogin_nonfbreg_textNumCoupons} credits of Rs {$mrpCouponConfiguration.mrp_firstLogin_nonfbreg_mrpAmount} each, valid for {$mrpCouponConfiguration.mrp_firstLogin_nonfbreg_validity} days, on purchases of Rs {$mrpCouponConfiguration.mrp_firstLogin_nonfbreg_minCartValue|number_format:0:'':','} and above.<br>
		Alternatively, you could register with Myntra using "Connect with Facebook" option, and instantly earn Rs.{$fbregValue}. This will be added to your Mynt Club account as {$mrpCouponConfiguration.mrp_firstLogin_fbreg_textNumCoupons} credits of Rs {$mrpCouponConfiguration.mrp_firstLogin_fbreg_mrpAmount} each, valid for {$mrpCouponConfiguration.mrp_firstLogin_fbreg_validity} days and on purchases of Rs {$mrpCouponConfiguration.mrp_firstLogin_fbreg_minCartValue|number_format:0:'':','} and above.  <br>
		Please note that you would be getting the benefit based on the method you use to sign up on Myntra for the first time. 
    <p>
        <i>- Refer and Earn</i><br>
                Refer your friends and earn more credits! You earn Rs. {$refregValue} when each of your referral registers with Myntra. This is added to your Mynt Club account as {$mrpCouponConfiguration.mrp_refRegistration_textNumCoupons} credits of Rs.{$mrpCouponConfiguration.mrp_refRegistration_mrpAmount}, valid for {$mrpCouponConfiguration.mrp_refRegistration_validity} days. You can redeem this against any purchase of Rs.{$mrpCouponConfiguration.mrp_refRegistration_minCartValue|number_format:0:'':','} and above.<br>
                You also earn Rs.{$refpurValue} when your referred friends make their first purchase. This is added to your Mynt Club account as {$mrpCouponConfiguration.mrp_refFirstPurchase_textNumCoupons} credits of Rs. {$mrpCouponConfiguration.mrp_refFirstPurchase_mrpAmount} each, valid for {$mrpCouponConfiguration.mrp_refFirstPurchase_validity} days. You can redeem these against purchases of Rs. {$mrpCouponConfiguration.mrp_refFirstPurchase_minCartValue|number_format:0:'':','} and above.
        </p>


    <div  class="bb">
        <b>How can I refer my friends to join the Mynt Club?</b>
		   Sign in, and go to "My Referrals" tab under "My Myntra" to send out invitations in several easy ways:
        <ul>
            <li>Select contacts from your email address book
                <ul>
                    <li>Click on "Add your contacts from" to choose email addresses of friends from your Gmail, Yahoo mail, AOL mail or Outlook accounts.</li>
                    <li>Follow the steps to select the contacts you wish to invite. You can also manually type in email addresses, separating each one with a comma.</li>
                </ul>
            </li>
            <li>Send invites on Facebook - Click on the Facebook icon, follow the steps to invite your friends through message or wall post.</li>
            <li>Send invites on Twitter - Post your invitation as a tweet by clicking the Twitter icon.</li>
            <li>Distribute your invitation link through your blog, social network, instant messengers, website by pasting your referral link along with a personalised message for your friends.</li>
        </ul>
    </div>
    <p class="bb">
        <b>How do I view my Mynt Club credits?</b>
		Your Mynt Club credits comprise of Sign up and referral bonuses. You can view these in the <strong>My Credits</strong> tab of your My Myntra account. Remember to redeem your credits before they expire!    
    </p>
    
    <p class="bb">
        <b>I have not received my Mynt credits. What should I do?</b>
        Check your spam folder and add <a href="mailto:no-reply@myntra.com">no-reply@myntra.com</a> and <a href="mailto:support@myntra.com">support@myntra.com</a> to your address book to ensure delivery of all mails from Mynt Club.<br>
		If the above did not work, write to us at support@myntra.com or call our Customer Care on +91-80-43541999 anytime of the day for assistance.
		</p>
    <div class="bb">
        <b>How can I redeem my Mynt Club credits?</b>
		You will be required to verify your mobile number before you can start redeeming your credits. On initiating the verification process, you will be sent a code via SMS. Please enter this code on the website to complete the verification.<br>
		Please note that we currently support only Indian mobile numbers on our verification process.<br>
		At checkout, you can choose to apply any of the following benefits -
		<ul>
			<li>Sign up or referral credits</li>
			<li>Any other discount voucher from Myntra.com</li>
		</ul>
	</div>
        <a name="cashback"></a>
	<div class="bb cashbackblock">
	<b>Cashback</b>
	{if $cashbackearnedcreditsEnabled eq '1'}
		<p> <i>- 10% Cashback</i><br>
     	   In addition to the registration and referral benefits, you earn a 10% Cashback on every purchase of non-discounted products on Myntra.com. The cashback amount would be 10% of your order total after deduction of any coupon discounts. This Cashback can be used on any of your subsequent purchases. Your cashback account will be valid for a period of six months from your last Cashback transaction.<br><br>
			Please note that the amount of time taken for Cashback to be credited to you depends on the mode of payment :<br> <br>
            <span style="font-weight: bold">For all Online purchases, Cashback will credited to you within one hour after you place your order with Myntra. <br> <br> For all Cash on Delivery purchases, Cashback will be credited to you 30 days after you place your order with Myntra.</span>
		</p>
	{/if}
        <p>
        <i>- Redeeming Cashback</i><br>
        You can use the Cashbank amount anytime you wish at the payment page after processing the order. Please note that coupons and cashback cannot be clubbed together for a single purchase.<br><br>
        If your order value is greater than the Cashback amount, you can make the balance payment through various payment options that we offer. If your order value is lesser than the amount of credits in your Cashback account, Cashback credit equal to the value of your order would be utliized for the purchase.
        </p>


	</div>
    </div>
    </div>
	<div class="divider">&nbsp;</div>
    {include file="site/footer.tpl"}
    </body>
	</html>
