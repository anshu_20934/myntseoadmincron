{include file="site/doctype.tpl"}
<head>
    {include file="site/header.tpl" }
</head>	
</head>	
	<body class="{$_MAB_css_classes}">
		<script>
		Myntra.Data.pageName="mymyntra";
		</script>
		{include file="site/menu.tpl" }
		
	    <div class="container clearfix">
		    <div style="width:100%;clear:both;margin-bottom:10px;">
				<span id="bc0" class="h1">MY MYNTRA</span>
				<span id="bc1" class="h2"></span>
				<span id="bc2" class="h2"></span>
				<span id = "returndisplaydiv" style="float:right;display:none;">Do you want to return an item? Call +91-80-43541999</span>
			</div>
	        <div class="left-nav">
		        {*Left Tabs*}
		        <ul id="accordion">
					<li class="selected">
						<div id="myprofile">
							<a href="#myprofile_tab" onclick="hideMessage();_gaq.push(['_trackEvent', 'my_myntra', 'my_profile_tab', 'click']);return false;">My Profile</a>
						</div>
					</li>
                    <li>
                        <div id="myorders">
							<a href="#myorders_tab" onclick="hideMessage();_gaq.push(['_trackEvent', 'my_myntra', 'my_orders_tab', 'click']); return false;" title="Pending Orders">My Orders</a>
						</div>
					</li>
					<li>
						<div id="mymyntcredits">
							<a href="#mymyntcredits_tab" onclick="hideMessage();_gaq.push(['_trackEvent', 'my_myntra', 'my_credits_tab', 'click']);return false;">My Mynt Credits</a>
						</div>
					</li>
					<li>
						<div id="myreferrals">
							<a href="#myreferrals_tab" onclick="hideMessage();_gaq.push(['_trackEvent', 'my_myntra', 'my_credits_tab', 'click']);return false;">My Referrals</a>
						</div>
					</li>
					{if $returnenabled eq 'true' }
					<li>
						<div id="myreturns">
							<a href="#myreturns_tab" onclick="hideMessage();_gaq.push(['_trackEvent', 'my_myntra', 'my_returns_tab', 'click']);return false;">My Returns</a>
						</div>
						{*<ul>
							<li id="mypendingreturns"><a href="#mypendingreturns_tab" onclick="hideMessage();_gaq.push(['_trackEvent', 'my_myntra', 'my_returns_tab', 'click']);return false;" title="Pending Returns">Pending Returns ({$counts.pending_returns})</a></li>
							<li id="mycompletedreturns"><a href="#mycompletedreturns_tab" onclick="hideMessage();_gaq.push(['_trackEvent', 'my_myntra', 'my_returns_tab', 'click']);return false;" title="Completed Returns">Completed Returns ({$counts.completed_returns})</a></li>
						</ul>*}
					</li>
					{/if}
				</ul>
	        </div>
	
	    	<div class="my-container" style="height: 100%;">
	    		<div class="notification error {if $error eq ''}hide{/if}">{$error}</div>
	    		<div class="notification success {if $message eq ''}hide{/if}">{$message}</div>
			     <div class="tab_container clearfix">
					<div class="mymyntra-ajax-loader left" style="display:none;">
						<div class="mymyntra-ajax-loader-icon"></div>
					</div>
			        <div id="myprofile_tab" class="tab_content" {if $view eq 'myprofile'} style="display:block" {else} style="display:none"{/if} >
						{include file="mymyntra/myprofile.tpl" }
			        </div>
			        <div id="myorders_tab" class="tab_content" {if $view eq 'myorders'} style="display:block" {else} style="display:none"{/if} >
			            {include file="mymyntra/myorders.tpl" }
			        </div>
			        <div id="mymyntcredits_tab" class="tab_content" {if $view eq 'mymyntcredits'} style="display:block" {else} style="display:none"{/if} >
			            {include file="mymyntra/mycoupons.tpl" }
			        </div>
			        <div id="myreferrals_tab" class="tab_content" {if $view eq 'myreferrals'} style="display:block;" {else} style="display:none;"{/if}  >
			            {include file="mymyntra/myreferrals.tpl" } 
			        </div>
			        {if $returnenabled eq 'true' }
			        	<div id="myreturns_tab" class="tab_content" {if $view eq 'myreturns'} style="display:block;" {else} style="display:none;" {/if}>
				            {include file="mymyntra/myreturns.tpl" }
				        </div>
			        {/if}
			        <script type="text/javascript">
						var tab_view='{$view}';
						var get_view = new Array();
						var error = '{$error}';
						var message = '{$message}';
					</script>
			    </div>
			</div>
		</div>	

        <div id="username_missing_div" style="display:none">
        	<div class="overlay" style="display:block;_position:absolute;_width:1300px;_height:1500px;" onClick="Javascript:closeUsernameMissingWindow()">&nbsp;</div>
        	<div style="left:32%; position:fixed; _position:absolute; width:400px; background:white; top:20%; z-index:1006;border:2px solid #999999;">
        		<div id="UsernameMissingContent" style="display:none;">
        		    <div id="TB_title">
        		    <div id="TB_ajaxWindowTitle"><b>Your name is missing from profile information</b></div>
        		    <div id="TB_closeAjaxWindow"><a class="no-decoration-link" href="javascript:closeUsernameMissingWindow()">close</a></div>
        		    </div>


        			<div id="username_missing" style="padding:25px 25px; text-align:left; font-size:12px" >
        				Every email invite you send through this page is personalized with your name as the Referrer.
        				<br>
        				This helps your friend identify you and greatly increases the chances of your friends responding to your invites.
                        <br>
        				<div align="center" style="padding-top:10px"><b> Full Name: </b> <input type="text" name="new_fullname" id="new_fullname" value="{$userProfileData.firstname} {$userProfileData.lastname}" maxlength="255"></div>

        				<div align="center" style="padding-top:10px"><input type="button" class="orange-bg corners" style="cursor:pointer;" id="verify-mobile" value="Save & Send Invites" onclick="updateUsername()"></div>
        			</div>
        		</div>
        	</div>
        </div>
        
        <div class="divider">&nbsp;</div>
        {include file="site/footer_content.tpl"}

        <script language="javascript">
        var stateZipsJson = {$stateZipsJson};
        {if $errorsJson}
        Myntra.Data.my = {$errorsJson};
        {/if}
        </script>
        {include file="site/footer_javascript.tpl"}
        <script type="text/javascript" src="http://www.plaxo.com/css/m/js/util.js"></script>
        <script type="text/javascript" src="http://www.plaxo.com/css/m/js/basic.js"></script>
        <script type="text/javascript" src="http://www.plaxo.com/css/m/js/abc_launcher.js"></script>
	</body>
</html>
