
	{if $productsInCart != 0}
	{*{foreach name=outer item=product from=$productsInCart}*}
	 {section name=idx loop=$productsInCart}
	
          {assign var='productTypeLabel' value=$productsInCart[idx].productTypeLabel}
          {assign var='productStyleName' value=$productsInCart[idx].productStyleName}
          {assign var='productStyleName' value=$productsInCart[idx].productStyleName}
          {assign var='productStyleType' value=$productsInCart[idx].productStyleType}
          {assign var='product_style' value=$productsInCart[idx].product_style}
          {assign var='allCustImagePath' value=$productsInCart[idx].allCustImagePath}
		  {assign var='quantity_breakup' value=$productsInCart[idx].quantity_breakup}
          {assign var='optionValues' value=$productsInCart[idx].optionValues}
          {assign var='optionNames' value=$productsInCart[idx].optionNames}
          {assign var='productPrice' value=$productsInCart[idx].price}
       	  {assign var='totalPrice' value=$productsInCart[idx].totalPrice}
       	  {assign var='promotion_offer' value=$productsInCart[idx].promotion}
       	  {assign var='designImagePath' value=$productsInCart[idx].designImagePath}
       	  {assign var='taxamount' value=$productsInCart[idx].taxamount}
       	  {assign var='order_size' value=$productsInCart[idx].order_size}
       	  {assign var='order_unified_size' value=$productsInCart[idx].order_unified_size}
       	  {assign var='order_qty' value=$productsInCart[idx].order_qty}
       	  
       	  {if $reebok_invoice ne ''}
       	  	{assign var='jersey_text' value=$productsInCart[idx].jersey_text}
       	  	{assign var='jersey_number' value=$productsInCart[idx].jersey_number}
       	  {/if}
       	
	      <div>
          <table class="printtable">
          	<tr>
          		<td class="printtable_name" style="width:25%;padding:5px;">{$productStyleName}</td>
          		{if $order_unified_size neq "" && $order_size neq "" && $order_unified_size neq $order_size}
          			<td class="printtable_size" style="width:10%;padding:5px;">Size: {$order_unified_size}<br/>Shipped as {$order_size}</td>
          		{else}
          			<td class="printtable_size" style="width:10%;padding:5px;">Size: {$order_size}</td>
          		{/if}
          		{if $order_qty gt 1}
          			<td class="printtable_qty" style="width:5%;padding:5px;">{$order_qty} nos</td>
          		{else}
          			<td class="printtable_qty" style="width:5%;padding:5px;">{$order_qty} no</td>
          		{/if}	
          		<td class="printtable_unitprice" style="width:20%;padding:5px;">Rs.&nbsp;{$productPrice|string_format:"%.2f"} per item<br/>VAT(inclusive):&nbsp;Rs.&nbsp;{$taxamount}</td>
          		<td class="printtable_totalprice" style="width:20%;padding:5px;">Rs.&nbsp;{$totalPrice|string_format:"%.2f"} in total</td>
          	</tr>
          </table>
        </div>
           
      {/section}
     {/if}

	{if $giftcerts != 0}
	{assign var=sno value=0}
	{foreach name=outer item=gift from=$giftcerts}
	{foreach key=key item=item from=$gift}
				{if $key eq 'purchaser'}
					{assign var='purchaser' value=$item}
				{/if}
				{if $key eq 'quantity'}
					{assign var='quantity' value=$item}
				{/if}
				{if $key eq 'recipient'}
					{assign var='recipient' value=$item}
				{/if}
				{if $key eq 'amount'}
					{assign var='amount' value=$item}
				{/if}
				{if $key eq 'totalamount'}
					{assign var='totalamount' value=$item}
				{/if}
				{if $key eq 'send_via'}
					{assign var='send_via' value=$item}
				{/if}

				{if $key eq 'recipient_email'}
					{assign var='recipient_email' value=$item}
				{/if}
				{if $key eq 'recipient_firstname'}
					{assign var='recipient_firstname' value=$item}
				{/if}

				{if $key eq 'recipient_lastname'}
					{assign var='recipient_lastname' value=$item}
				{/if}

				{if $key eq 'recipient_address'}
					{assign var='recipient_address' value=$item}
				{/if}

				{if $key eq 'recipient_city'}
					{assign var='recipient_city' value=$item}
				{/if}

				{if $key eq 'recipient_zipcode'}
					{assign var='recipient_zipcode' value=$item}
				{/if}

				{if $key eq 'recipient_county'}
					{assign var='recipient_county' value=$item}
				{/if}

				{if $key eq 'recipient_statename'}
					{assign var='recipient_statename' value=$item}
				{/if}

			{/foreach}

		<div>
              <table cellpadding="2" class="printtable">
                <tr>
                  <td class="align_left" style="width:70%"><strong>From:</strong>&nbsp;{$purchaser}</td>
		  <td class="align_left" rowspan="3" colspan="2" >
			<img src="{$cdn_base}/skin1/images/gift.gif" alt="" />
		  </td>
                </tr>
                <tr>
                  <td class="align_left"><strong>To:</strong>&nbsp;{$recipient}</td>
                </tr>

                <tr>
			{if $send_via eq 'E'}
				<td class="align_left" ><strong>Recipient Email:</strong>&nbsp;{$recipient_email}</td>
			{/if}
			{if $send_via eq 'P'}
				<td class="align_left" ><strong>Recipient Address:</strong>&nbsp;{$recipient_firstname}&nbsp; {$recipient_lastname}<br/>{$recipient_address}<br/>{$recipient_city}<br/>{$recipient_country}</td>
			{/if}

                </tr>

		 <tr>
			<td class="align_left" style="width:90%"><strong>Quantity:</strong>&nbsp;{$quantity}</td>
                </tr>

                <tr>
		  <td class="align_left" ><strong>Unit Price:</strong>&nbsp;Rs.{$amount|string_format:"%.2f"}</td>
		  <td class="align_left" ><strong>Price:</strong>&nbsp;Rs.{$totalamount|string_format:"%.2f"}</td>
                </tr>
              </table>
            </div>
	    {assign var=sno value=$sno+1}
		{/foreach}
		{/if}
	  {if $smarty.get.type eq 'admin' && $source_type_id eq 11}{else}
          <div>
            <table cellpadding="2"  class="printtable">
		     <tr class="font_bold">
			<td class="align_right" colspan="8">Amount</td>
			<td class="align_right">Rs.{$amountwitoutcc|string_format:"%.2f"}</td>
		      </tr>
		 {if ($productDiscount != "") && ($productDiscount gt 0)}
                      <tr class="font_bold">
                        <td class="align_right" colspan="8">Special offer discount</td>
                        <td class="align_right">Rs.{$productDiscount|string_format:"%.2f"}</td>
                      </tr>
                 {/if} 
		 {if ($coupondiscount != "") && ($coupondiscount gt 0)}
		      <tr class="font_bold">
			<td class="align_right" colspan="8">Coupon discount(Coupon code: {$couponCode|upper}({$couponpercent}))</td>
			<td class="align_right">Rs.{$coupondiscount|string_format:"%.2f"}</td>
		      </tr>
		 {/if}
		 {if ($cashdiscount != "") && ($cashdiscount gt 0)}
		      <tr class="font_bold">
			<td class="align_right" colspan="8">Mynt Credits</td>
			<td class="align_right">Rs.{$cashdiscount|string_format:"%.2f"}</td>
		      </tr>
		 {/if}
		 {if ($pg_discount != "") && ($pg_discount gt 0)}
	      <tr class="font_bold">
			<td class="align_right" colspan="8">Payment Discount</td>
			<td class="align_right">Rs.{$pg_discount|string_format:"%.2f"}</td>
	      </tr>
		 {/if}
		<tr class="font_bold">
			<td class="align_right" colspan="8">Shipping Cost  </td>
			<td class="align_right"><div id="shipcost">Rs.{$shippingRate}</div></td>
		</tr>
		{if $paymentoption == 'cod'}
		   <tr class="font_bold">
			<td class="align_right" colspan="8">Cash on delivery charges </td>
			<td class="align_right"><div id="shipcost">Rs.{$cod_charges}</div></td>
		   </tr>
		{/if}
		{if ($emi_charge != "") && ($emi_charge gt 0)}
	      <tr class="font_bold">
			<td class="align_right" colspan="8">EMI Charges</td>
			<td class="align_right">Rs.{$emi_charge|string_format:"%.2f"}</td>
	      </tr>
		 {/if}
		{if $gift_status == 'Y'}
		<tr class="font_bold">
			<td class="align_right" colspan="8">Gift packaging charges  </td>
			<td class="align_right"><div id="shipcost">Rs.20.00</div></td>
		</tr>
		{/if}
		<tr class="font_bold">
			<td class="align_right" colspan="8">Amount to be paid</td>
			<td class="align_right">Rs. {$totalafterdiscount|string_format:"%.2f"}</td>
		</tr>
		{if $ref_discount != 0.00}
			<tr class="font_bold">
				<td class="align_right" colspan="8">Referral discount</td>
				<td class="align_right">&nbsp;Rs.{$ref_discount}</td>
			</tr>
			 {math equation="x - y" y=$ref_discount x=$totalafterdiscount assign ="amtAfterRefDeducton"}
		   <tr class="font_bold">
				<td class="align_right" colspan="8">Amount after referral deduction </td>
				<td class="align_right">&nbsp;Rs.{$amtAfterRefDeducton|string_format:"%.2f"}</td>
		     </tr>	
		 {/if}
		
            </table>
          </div>{/if}
