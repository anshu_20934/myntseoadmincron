{literal}
<script language="javascript">
var updated = true;


function isUpdated(login,pagetype)
{
	if( updated )
		return checkLoggedIn(login,pagetype);
	else
		alert("You have made changes to the cart.Please click update button");
}

function confirmDelete(productname, productid, quantity,producttype)
{
	var flag = confirm("Do you really want to delete "+productname+" "+producttype+" from your cart ?");

	if(flag == true)
	{
		// form submit
	}
	else
	{
		return flag;
	}
}


function updateTotalQuantity(prid,obj)
{
	var quantity = 0;
	quantity = quantity*1;
	
	if(document.getElementById(prid+"count") != null){	
		var cid = prid+"count";
		var count = document.getElementById(cid).value;
		for(var i=1; i<count; i++){
			var id = prid+"sizequantity"+i;
			var value = document.getElementById(id).value;
			if( IsNumeric(value) )
				quantity = quantity + parseInt(value);
			else
			{
				document.getElementById(id).value = "";
			}
		}
	}else
		quantity = document.getElementById(prid+"qty").value;
	
	if(!IsNumeric(quantity) ||  quantity == ''){
		alert("Quantity should atleast be one.");
		obj.value = 1;
		obj.focus();
		document.getElementById(prid+"qty").value = obj.value;
		updated = false;
		return false;
	}else{		
		document.getElementById(prid+"qty").value = quantity;
		alert('please click on the update button to change the total amount as per new quantity.');
		updated = false;		
	}
}

function validateQuantity(prid, quantity){
	var elm = document.getElementById(prid + "qty");
	var qty = elm.value;
	
	if(!IsNumeric(qty) ||  qty == ''){
		alert("Please enter valid quantity.");
		elm.value = quantity;
		return false;
	}else
		return true;
}

function clearCart()
{
	var flag = confirm("Are you sure you want to clear your cart ?");

	if(flag == true)
	{
		window.location.href='./updatecart.php?action=deleteall';

	}
}
function addMore()
{
	window.location.href='./mkstartshopping.php';
}
</script>
{/literal}
	<input type=hidden id="pagetype" name="pagetype" value="{$pagetype}" >
	<input type="hidden" name="ajaxStatus" id= "ajaxStatus" value="0">	
	<div class="super">
		<p>Your shopping cart</p>
	</div>
	<div class="head">
		<p style="_padding-bottom:0px">Products in your shopping cart</p>
	</div>
		{assign var=tabindex value=1}
		{foreach name=outer item=product key=itemid from=$productsInCart}
			{foreach key=key item=item from=$product}
				{if $key eq 'productId'}
					{assign var='productId' value=$item}
				{/if}
				{if $key eq 'producTypeId'}
					{assign var='producTypeId' value=$item}
				{/if}
				{if $key eq 'productStyleId'}
					{assign var='productStyleId' value=$item}
				{/if}
				{if $key eq 'productStyleName'}
					{assign var='productStyleName' value=$item}
				{/if}
				{if $key eq 'productPrice'}
					{assign var='productPrice' value=$item}
				{/if}
				{if $key eq 'unitPrice'}
                    {assign var='unitPrice' value=$item}
                {/if}
				{if $key eq 'quantity'}
					{assign var='quantity' value=$item}
				{/if}
				{if $key eq 'actualPriceOfProduct'}
					{assign var='actualPriceOfProduct' value=$item}
				{/if}
				{if $key eq 'productTypeLabel'}
					{assign var='productTypeLabel' value=$item}
				{/if}
				{if $key eq 'totalPrice'}
					{assign var='totalPrice' value=$item}
				{/if}
				{if $key eq 'sizenames'}
					{assign var='sizenames' value=$item}
				{/if}
				{if $key eq 'sizequantities'}
					{assign var='sizevalues' value=$item}
				{/if}
				{if $key eq 'cartImagePath'}
					<!--to show name and number for jerseys added by arun-->
					{include file="affiliatetemplates/reebok_ipl/cricket_jersey_cart_images.tpl" assign='cartImagePath'}
					<!--to show name and number for jerseys added by arun-->
					{* assign var='cartImagePath' value=$item *}
				{/if}
				{if $key eq 'discount'}
					{assign var='discount' value=$item}
				{/if}
				{if $key eq 'productStyleType'}
					{assign var='productStyleType' value=$item}
				{/if}
				{if $key eq 'description'}
					{assign var='description' value=$item}
				{/if}
				{if $key eq 'totalCustomizedAreas'}
					{assign var='totalCustomizedAreas' value=$item}
				{/if}
			{/foreach}
			<div>
				<form name="cartform_{$itemid}" method="post" action="updatecart.php" style="margin-bottom:0px;margin-top:0px">
				<table cellpadding="2" class="table" style="padding-top:10px" >
					<tr style="border:1px solid red">
						<td class="align_left"><strong>product type:</strong>&nbsp;{$productTypeLabel}</td>
						<td class="align_left" rowspan="5">
						{if $cartImagePath}
							{if $ie eq "true"}
								<span style="width:100px;display:inline-block;filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='{$cartImagePath}', sizingMethod='scale');">
								<img src="{$cartImagePath}"  alt="Design" style="filter:progid:DXImageTransform.Microsoft.Alpha(opacity=0);"  width="100px"  ></span><br/>
							{else}
								<img src="{$cartImagePath}"  width="100px"  ><br/ >
							{/if}
							{if $totalCustomizedAreas neq 1}
								<div align="right"><a href="javascript:custAreaImages('popup', 0,0, '{$productId}');;">view all images</a></div>
							{/if}
						{else}
							<img src="{$cdn_base}/default_image.gif" width="100px" height="100px" />
						{/if}
		  				</td>			
					</tr>
					<tr>
						<td class="align_left" style="width:85%"><strong>product style:</strong>&nbsp;{$productStyleName}</td>
					</tr>
					
					<!--to show name and number for jerseys added by arun-->
					{if $product_custom_display_forcart.$productId.text ne ''}
						<tr>
							<td class="align_left" style="width:85%;"><strong>Name to be printed on the back of Jersey:</strong>&nbsp;<b><u>{$product_custom_display_forcart.$productId.text}</u></b></td>
						</tr>
					{/if}
					{if $product_custom_display_forcart.$productId.num ne ''}
						<tr>
							<td class="align_left" style="width:85%;"><strong>Number to be printed on the back of Jersey:</strong>&nbsp;<b><u>{$product_custom_display_forcart.$productId.num}</u></b></td>
						</tr>
					{/if}
					<!--to show name and number for jerseys added by arun-->

					{if $description }
					<tr>
						<td class="align_left" colspan="2">
							<b>Description</b> : {$description}
						</td>
					</tr>
					{/if}

					{if $sizenames && $sizevalues}
						<tr>
							<td class="align_left">
								<table>
									<tr>
										<td style="text-align:left;width:60px;font-size:1em;">
                                            {if $producTypeId eq 170}
                                                <strong>Color:</strong>
                                            {else}
                                                <strong>Size:</strong>
                                            {/if}
										</td>
										{foreach from=$sizenames item=sizename}
											<td style="text-align:center;width:60px; font-size:1em;">&nbsp;{$sizename}</td>
										{/foreach}
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td class="align_left">
								<table>
									<tr>
										<td style="text-align:left;width:60px;font-size:1em;"><strong>quantity:</strong></td>
										{assign var=i value=1}
										{foreach from=$sizevalues item=sizevalue}
											<td style="text-align:center;width:60px;">
												<input type="text" name="{$itemid}sizequantity[]" class="quantity" id="{$itemid}sizequantity{$i}" value="{$sizevalue}" style="width:30px;" onblur="javascript:updateTotalQuantity({$itemid},this);"/>
											</td>
											{assign var=i value=$i+1}
										{/foreach}
										<input type="hidden" name="{$itemid}count" id="{$itemid}count" value="{$i}">								
									</tr>
								</table>
							</td>
						</tr>
					{else}
						<tr>
							<td class="align_left"></td><td class="align_left"></td>
						</tr>
						<tr>
							<td class="align_left"></td><td class="align_left"></td>
						</tr>
					{/if}

					<tr>
						<td class="align_left">
							{if $sizenames}
								<strong>total quantity:</strong>
								<input id="{$itemid}qty"  name="{$itemid}qty" type="text" class="quantity"  value="{$quantity}" maxlength="4" tabindex="{$tabindex}" style="border:0px;width:30px;" readonly/>
							{else}
								<strong>total quantity:</strong>
								<input id="{$itemid}qty"  name="{$itemid}qty" type="text" class="quantity"  value="{$quantity}" maxlength="4" tabindex="{$tabindex}" style="width:30px;" onblur="javascript:updateTotalQuantity({$itemid},this);" />
							{/if}
							<input type="hidden" name="cartitem" id="cartitem" value="{$itemid}"/>
							<input type="submit" border="0" value="update" name="update" class="submit"  tabindex="{$tabindex}"/>&nbsp;
							<input type="submit" border="0" value="remove" name="remove" class="submit" onClick="return confirmDelete('{$productStyleName|escape:"quotes"}', '{$itemid}', '{$quantity}','{$productTypeLabel}')" tabindex="{$tabindex}"/>
						</td>
						<td class="align_left">
							&nbsp;
						</td>
					</tr>
					<tr>
						<td class="align_left" ><strong>unit price:</strong>&nbsp;Rs.<span >{$unitPrice|string_format:"%.2f"}</span>&nbsp;</td>
						<td class="align_right" ><strong>price:</strong>&nbsp;Rs.{$totalPrice|string_format:"%.2f"}</td>
					</tr>
				</table>
				</form>
			</div>
            <div class="links" > &nbsp; </div>
			{assign var=tabindex value=$tabindex+1}
		{/foreach}

		<table class="table">
			<tr class="font_bold">
				<th class="align_right" width="70%"> amount : </th>
				<th class="align_right"> <span>{$amount|string_format:"%.2f"}</span> </th>
			</tr>


			<tr class="font_bold">
				<th class="align_right" width="70%"> vat/cst : </th>
				<th class="align_right"> <span>{$vat|string_format:"%.2f"} </span></th>
			</tr>

			{if $coupondiscount != 0 }
			
			<tr class="font_bold">
				<th class="align_right">coupon discount(coupon code: {$couponcode}({$couponpercent})):</th>
				<th class="align_right">Rs.{$coupondiscount|string_format:"%.2f"}</th>
			</tr>
			{/if}
			<tr class="font_bold">
				<th class="align_right" width="70%"> amount to be paid : </th>
				<th class="align_right"> Rs.<span>{$totalAmount|string_format:"%.2f"} </span></th>
			</tr>
			<tr>
			<td>&nbsp;</td>
			{if ($cart_amount) gt $free_shipping_amount}
                <td style="font-size:12px; color:red; font-weight:bold; text-align:right;padding:10px 0;">FREE Shipping in India!</td>
	        {/if}
		    </tr>
			<tr>
				<td><a style='cursor:pointer;text-decoration:underline;font-size:1em;' onClick="clearCart()">clear your cart</a></td>
				<td>
					<a href="javascript:addMore();">add more products</a>
					<form name="updatecart" method="post" action="mkpaymentoptions.php" style="float:right">
					<input type="button" border="0" value="buy now >>" name="submitButton" class="submit" style="font-weight:bold;font-size:1.2em;width:100px;background:#CB0029;cursor:pointer;" onClick="return isUpdated('{$login}', '{$pagetype}')" />
					</form>
				</td>
			</tr>
		</table>
	<div class="foot"></div>
