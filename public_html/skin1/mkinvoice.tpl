{* $Id: mykritihome.tpl,v 1.88.2.3 2006/07/19 10:19:35 max Exp $ *}
<link rel="stylesheet" href="{$http_location}/skin1/{$org_css_file}" />
{config_load file="$skin_config"}
<html>
{include file="pagetracker.tpl" }
<script type="text/javascript" src="{$SkinDir}/divpop_js.js"></script>
<script type="text/javascript" src="{$SkinDir}/AnchorPosition.js"></script>
<script type="text/javascript" src="{$SkinDir}/PopupWindow.js"></script>
<script type="text/javascript" src="{$SkinDir}/myimage.js"></script>
<script type="text/javascript" src="{$SkinDir}/js_script/email_validate.js"></script>

<script type="text/javascript" src="{$SkinDir}/js_script/ajaxObject.js"></script>
{if $orgId}
<link rel="stylesheet" href="{$SkinDir}/modules/organization/css/orgaStyles.css" />
{/if}
<link rel="stylesheet" href="{$SkinDir}/css/tooltip.css" />
<script type="text/javascript" src="{$SkinDir}/js_script/alttxt.js"></script>
<script type="text/javascript" src="{$SkinDir}/js_script/org_user_js.js"></script>
<script type="text/javascript">
	var orderid='{$orderid}';
</script>
{literal}
<!-- Google Code for purchase Conversion Page -->
<script language="JavaScript" type="text/javascript">
<!--
var google_conversion_id = 1068346998;
var google_conversion_language = "en_GB";
var google_conversion_format = "1";
var google_conversion_color = "FFFFFF";
if (1) { var google_conversion_value = 1; }
var google_conversion_label = "purchase";
//-->
</script>
<style type="text/css">
  iframe {
   height: 0px;
  }
</style>

<script language="JavaScript" src="http://www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
	<img height=1 width=1 border=0 src="http://www.googleadservices.com/pagead/conversion/1068346998/imp.gif?value=1&label=purchase&script=0">
</noscript>
<script language='javascript'>
function postRequestonAffiliate(psript,params){
	var req = null;
	if(window.XMLHttpRequest){
		req = new XMLHttpRequest();
	}else if (window.ActiveXObject){
		req  = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var url = psript
	req.onreadystatechange = function(){
	if (req.readyState == 4){
		var resp =  req.responseText	;
 	}
 };
req.open('POST', url, true);
req.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
req.send("orderid="+params);
}
function submitOrder(){
	document.order.notes.value =document.getElementById("usercomment").value;
	document.order.submit();
}
function popUpWindow(URL){
	window.open(URL,"Invoice", "menubar=1,resizable=1,toolbar=1,width=800,height=500");
	//window.open(URL);
}
/********** code to send the coupons to friend *************/
function sendCouponToFriends(mode,login){
	var req = null; var errorflag = 0; var emailList = [];
	var strEmailList ="reqType="+mode+"&";
	for(var i=1; i <= 5; i++){
		var txtBoxId = "frdemail"+i;
		var frdNameBoxId  =  "frdname"+i;
		var emails = document.getElementById(txtBoxId).value;
		var frdNames = document.getElementById(frdNameBoxId).value;
		if(emails != '' && frdNames != ""){
			var emailArray = emails.split(",");
			var frdNamesArray = frdNames.split(",");
			for(var j=0; j < 1 ; j++){
				if(emailArray[j] != ""){
					if(!echeck(emailArray[j])){
						document.getElementById(txtBoxId).focus();
						return false;
					}
					else if(login == emailArray[j]){
						alert("You cannot send coupon to yourself. Please remove your email address from the list"); return false;
					}
				}
			}
			strEmailList += txtBoxId+"="+emailArray[0]+"&"+frdNameBoxId+"="+frdNamesArray[0]+"&" ;
		}
		else{
			errorflag = errorflag + 1;
		}
	}
	//added by nishith, sending orderid to track all emailids
	strEmailList +="orderid="+orderid;
	if(errorflag == 5){
		alert("Please enter your friend name(s) and EmailId(s).")
		return false;
	}
	req = (window.XMLHttpRequest) ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");
	var url = "sendAndInviteFriends.php"
	req.onreadystatechange = function(){
		if (req.readyState == 4){
			var resp = (req.responseText).split("||");
			if(resp[0] == "SUCCESS"){
			  for(var i=1; i <= 5; i++){
				document.getElementById("frdname"+i).value = "";
				document.getElementById("frdemail"+i).disabled = true;
			  }
			  alert(resp[1]);
			  
			}else{
			alert(resp[1]);
		   }
		   document.getElementById('sendCToFrd').style.display ="none";
		}
	};
	req.open('POST', url, true);
	req.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	req.send(encodeURI(strEmailList));

}
</script>
{/literal}
{foreach key=key item=item from=$userinfo}
    {if $key eq 'b_firstname'}
     {assign var='firstname' value=$item}
    {/if}
{/foreach}
<script language='JavaScript' type='text/javascript'>
       var total_amount = '{$amountafterdiscount}';
       var currency_code = 'INR';
       var user_name = '{$firstname}';
       var email_address = '{$email}';
    </script>

   <script language='JavaScript' type='text/javascript'>
        var conversion_id= '253';
        if (window.user_name) {$ldim}
        var conversion_username=user_name;
        {$rdim}
        if (window.total_amount) {$ldim}
        var conversion_volume=total_amount;
        {$rdim}
        if (window.currency_code) {$ldim}
        var conversion_currency=currency_code;
        {$rdim}
    </script>
<body  id="body"   >
<div id="popup"></div>
<div id="scroll_wrapper">
<div id="container">
              {include file="mykrititop.tpl" }

<div id="wrapper" >
{if $orgId != ""}
	{include file="modules/organization$brandShopDir/breadcrumb_organization.tpl"}
{else}
	{include file="breadcrumb.tpl" }
{/if}
	  <div id="display">
<!-- start left panel -->
{if $orgId != ""}
	{include file="modules/organization$brandShopDir/orgInviteeLeftLinks.tpl"}
{elseif $affiliateleftpanel != ''}
	<div class="menu_column">{include file="$affiliateleftpanel" }</div>	
{else}
	<div class="menu_column">{include file="leftpaneMyAccount.tpl" }</div>
{/if}
<!-- end -->
	<form name='order' method='POST' action='mkorderBook.php'>
	<input type='hidden' name="option" value="{$option}">
	<input type='hidden' name="optionid" value={$optionid}>
	<input type='hidden' name="notes">
	<input type="hidden" name="ajaxStatus" id= "ajaxStatus" value="0">
	 <div class="center_column">
	    <div class="form">
	    {if $flagcomplete != "underprocess"}
	    	   {if $orderstatus == "Y"}
				    {include file="mkorderconfirmation.tpl" }
					{if $affiliateId neq '95' && $amountafterdiscount gte 500}
						{include file="mkfriendemailfrm.tpl"}
					{/if}
	
				{include file="mkorder.tpl" }
			
				<div class="subhead"></div>
				  <div> 
				    <table cellpadding="2" class="table">
				      <tr> 
					<td class="align_left"> 
						<h3>Billing Address</h3>   
					</td>
					<td class="align_left"> 
						<h3>shipping Address</h3>
					</td>
				      </tr>
				      <tr> 
					<td> 
						<p>{include file="mkbillingaddress.tpl" }</P>
					</td>
					<td>
						<p>{include file="mkshippingaddress.tpl" }</p>
					</td>
				      </tr>
				    </table>
				   </div>
				
				<div class="foot"></div>
		       
				 <br>
				{include file="mkproductsinvoice.tpl" }
				{assign var="headerText" value="Send coupons to your friend's"}
				
			<!--<div class="subhead"> 
		            <p>your message</p>
		          </div>
		           <div class="text"> 
			  <textarea id="usercomment" name="usercomment" cols="" rows="" readonly >{$notes}</textarea>
		          </div>
		          <div class="links"> 
		            <p> 
		              <input class="submit" type="button" name="submitButton" value="print invoice" border="0" onClick="popUpWindow('mk_final_invoice.php?orderid={$orderid}');">
		            </p>
		          </div>
		          <div class="foot"></div>
		          <br/>-->
	          	{else}
	          		 <div class="subhead"></div>
			          <div> 
			            <table cellpadding="2" class="table">
			              <tr> 
			                <td align="center"> 
			                 <h4>{$message}</h4>
			                </td>
			              </tr>
			            </table>
			          </div>
			          <div class="foot"></div>
	         		 <br>
	          	{/if}	
            {else}
          		<div class="subhead"></div>
          		<div> 
	            <table cellpadding="2" class="table">
	              <tr> 
	                <td align="center"> 
	                 <h4>Your order already booked. Your payment has been processed. </h4>
	                </td>
	              </tr>
	            </table>
          		</div>
          		<div class="foot"></div>
          		<br>
         	 {/if}
			</div>				
		 </div> 
		 </form>
		 
	 </div>
 <div class="clearall"></div>

</div>
	{include file="footer.tpl" }
	</div>
	</div>


<div id="navtxt" class="navtext" style="position:absolute; top:-100px; left:0px; visibility:hidden"></div>
<script src="//ah8.facebook.com/js/conversions/tracking.js"></script><script type="text/javascript">
try {ldelim}
var sku='';
var value=0;

  FB.Insights.impression({ldelim}
     'id' : 6002458717317,
     'h' : '12da38347f',
     'value' : "{$amountafterdiscount}",
     'SKU' : "{$orderid}"
 {rdelim});
{rdelim}  catch (e) {ldelim}{rdelim}
</script>
</body>
<!--google analytics-->
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>

<script type="text/javascript">
try {ldelim}
	var pageTracker = _gat._getTracker("UA-1752831-1");
	pageTracker._trackPageview();
	{if $payment_method neq 'chq'}
	pageTracker._addTrans(
	"{$orderid}",                                     // Order ID    
	"{$userinfo.b_firstname} {$userinfo.b_lastname}", //cust name 
	"{$amountafterdiscount|string_format:'%.2f'}",	  // Total
    "{$vat}",                                     	  // Tax    
    "{$shippingRate}",                                // Shipping    
    "{$userinfo.s_city}",                             // City
    "{$userinfo.s_state}",                            // State    
    "{$userinfo.s_country}"                              // Country  
    );
        
	{foreach name=outer k=key item=product from=$productsInCart}
		{foreach key=key item=item from=$product}
			{if $key eq 'productId'}
			{assign var='productId' value=$item}
			{/if}
			
			{if $key eq 'productStyleName'}
			{assign var='productStyleName' value=$item}
			{/if}
			
			{if $key eq 'quantity'}
			{assign var='quantity' value=$item}
			{/if}
			
			{if $key eq 'productTypeLabel'}
			{assign var='productTypeLabel' value=$item}
			{/if}
			
			{if $key eq 'totalPrice'}
			{assign var='totalPrice' value=$item}
			{/if}
		{/foreach}
	
		pageTracker._addItem(    
	    "{$orderid}",			// Order ID
	    "{$productId}",         //pid
	    "{$productStyleName}",  //style     
	    "{$productTypeLabel}",  //type
	    "{$totalPrice}",        //Price(qty*productprice)
	    "{$quantity}"           // Quantity  
	    );
    {/foreach}
    pageTracker._trackTrans();

    // Yahoo Conversion Tracking
    window.ysm_customData = new Object();
    window.ysm_customData.conversion = "transId={$orderid},currency=INR,amount={$amountafterdiscount|string_format:'%.2f'}";
    var ysm_accountid  = "1GJC30EFMMKEBN6R4ASQ2SBNQCG";
    document.write("<SCR" + "IPT language='JavaScript' type='text/javascript' "
    + "SRC=//" + "srv2.wa.marketingsolutions.yahoo.com" + "/script/ScriptServlet" + "?aid=" + ysm_accountid
    + "></SCR" + "IPT>");

    {/if}
{rdelim} catch(err) {ldelim}{rdelim}
</script>
<!--google analytics-->
</html>
