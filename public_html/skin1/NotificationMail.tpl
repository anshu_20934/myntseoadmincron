<html>
<head>
<title>{$title}</title>
</head>
<body>

<table width="100% ">
	<tr>
		<td><span><br>{$message}</span></td>

	</tr>
	<tr>
		<td><br><br>SKU#: {$sku_code}</td>
	</tr>
	<tr>
		<td>Product Name: -<a href="{$product_link}">{$product_name}</a></td>
	</tr>
	<tr>
		<td>Brand: {$brand}</td>
	</tr>
	<tr>
		<td>Current Count: {$current_count}</td>
	</tr>
	<tr>
		<td>Threshold Count: {$threshold_count}</td>
	</tr>
	<tr>
		<td>Changed By: {$user}</td>
	</tr>
	<tr>
		<td><br><br>{$bottom_message}</td>
	</tr>

</table>
</body>
</html>

