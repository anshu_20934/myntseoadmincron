<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html xml:lang="en" xmlns:fb="http://www.facebook.com/2008/fbml">
<head>

<META http-equiv="Content-Style-Type" content="text/css">
<META http-equiv="Content-Script-Type" content="type/javascript">
<META http-equiv="Content-Type" content="text/html; charset=utf-8">
{include file="site/header.tpl" }
<!--[if lt IE 8]>
    <link rel="stylesheet" href="new-skin-css/ie.css" type="text/css" media="screen, projection"><![endif]-->

</head>

<body class="{$_MAB_css_classes}">
{include file="site/menu.tpl" }
<div class="container clearfix">
	<h1 class="page-title">Contact Us</h1>

	<hr class="space clear mb10">
    <div class="content clearfix prepend">
    	<div class="content-left left span-32">
<!--			<div class="main-content-left left span-6 last black-bg5">-->
<!--			</div>-->
<!--			<div class="main-content-right right span-20 last">-->
<!--			</div>-->
				<table class="contact-us-table align-top">
					<tr>
						<th>
						<p>Customer Care</p>
						</th>
					</tr>
					<tr>
						<td class="cont-phone" valign="top">
							<p>Phone Numbers :</p>
						</td>
						<td>
							<div><strong>{if $customerSupportCall}{$customerSupportCall}{/if}</strong></div>
						</td>
						<td>
							<p>Email id :</p>
						</td>
						<td>
							<div><a href="mailto:support@myntra.com" class="no-decoration-link">support@myntra.com</a></div>
						</td>
					</tr>
				</table>
				<p>For order-related enquiries, email us with your order number & contact details in the Subject Line and we will respond within 3 working days</p>
				<p>If you have already done so and are not satisfied with the response from Customer Connect, please email us at <a href="mailto:cclead@myntra.com" class="no-decoration-link">cclead@myntra.com</a> with your Ticket Number</p>
				<p>In case your concern still remains unresolved, please drop an email to : <a href="mailto:ccmanager@myntra.com" class="no-decoration-link">ccmanager@myntra.com</a></p>

				<table class="contact-us-table">
					<tr>
						<th>Other Enquiries</th>
					</tr>
					<tr>
						<td style="width:40%">Corporate Sales</td>
						<td style="text-align:left;"><a href="mailto:sales@myntra.com">sales@myntra.com</a></td>
					</tr>
					<tr>
						<td style="width:40%">Business Development &amp; Partnerships</td>
						<td style="text-align:left;"><a href="mailto:partners@myntra.com">partners@myntra.com</a></td>
					</tr>
                    <tr>
						<td style="width:40%">Press Relations</td>
						<td style="text-align:left;"><a href="mailto:pr@myntra.com">pr@myntra.com</a></td>
					</tr>

				</table>
				<table class="contact-us-table">
				
					<tr class="add-block">
                        <td colspan="3">
                            <h6 class="bold">Returns Processing Facility <b style="font-weight:normal;">(to be used when shipping back returns)</b></h6>
                            <ul class="address">
                                <li>ATTN: The Logistics Manager</li>
                                <li>Vector E-Commerce Pvt Ltd.</li>
                                <li>7th Mile, Krishna Reddy Industrial Area, Kudlu Gate,</li>
                                <li>Opp Macaulay High School, behind Andhra Bank ATM,</li>
                                <li>Bangalore - 560068</li>
                                <li>India</li>
                            </ul>
                        </td>
					</tr>
					<tr><td>&nbsp;</td></tr>
					<tr class="add-block">
						<td>
							<h6 class="bold">Bangalore Office</h6>
							<ul class="address">
								<li>3rd Floor, AKR Tech Park</li>
								<li>7th Mile, Krishna Reddy Industrial Area</li>
								<li>Kudlu Gate, </li>
								<li>Bangalore - 560068</li>
								<li>Phone: 080-43540100</li>
								<li class="route-map"><a href="{$cdn_base}/skin1/pdf/Myntra_Route_Map.pdf" target="_blank"> <img src="http://cdn.myntra.com/skin1/myntra_images/pdf_22_22.png" alt="route map" /><div><span>Download Route Map</span></div></a></li>
							</ul>
						</td>
						<!--td>
							<h6>Mumbai Office</h6>
							<ul class="address">
								<li>202 &amp; 203, Dhamji Shamji Trade Centre,</li>
								<li>Near Vidyavihar Station,Vidyavihar (West)</li>
								<li><b>Mumbai - 400086</b></li>
							</ul>
						</td>
						<td>
							<h6>Delhi Office</h6>
							<ul class="address">
								<li>920, 9th Floor , Devika Towers,</li>
								<li>Nehru Place,</li>
								<li><b>New Delhi - 110019 </b></li>
							</ul>
						</td-->
					</tr>
				</table>
			</div>
			<div class="content-right right prepend-1 last span-6">
			</div>
		</div>

</div>

<div class="divider">&nbsp;</div>
{include file="site/footer.tpl"}
</body>
</html>
