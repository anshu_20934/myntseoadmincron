// JavaScript Document
/* Define url for redirecting to server site page*/

var URL= http_loc+"/mkshippingcalculate.php";

/* 
 * Function Name: getHTTPObject()
 * Purpose: Instantiate object of XMLHttpRequest(). 
 * Summary: Beauty of this function is, this function instantiate browser independent 
 * object of XMLHttpRequest() 
 *     
*/
function getHTTPObject() {
  	if(window.XMLHttpRequest){
         var http = new XMLHttpRequest();
    }else if(window.ActiveXObject){
        var http = new ActiveXObject("Microsoft.XMLHTTP");
    }
    return http;

}
var http = new getHTTPObject();


/* 
 * Function Name: formData2QueryString
 * Parameter: form name(ajaxForm) 
 * Purpose: Concatenate the name/value pair of form elements 
 * Summary: This function retrive the name/value pair of form elements and concatenate 
 * them into a single string. 
 * Note: Redio button and Checkbox cases is yet to implement.  
 *     
*/

function formData2QueryString(ajaxForm) {

    var strSubmit       = '';
    var formElem;
    var strLastElemName = '';
    
    var logisticid = document.getElementById('s_option').value;
    
    for (i = 0; i < ajaxForm.elements.length; i++) {
    	
    formElem = ajaxForm.elements[i];
   
    switch (formElem.type) {
    // Text, select, hidden, password, textarea elements
    case 'text':       
    case 'hidden':
    case 'select-one':
    
    
    if(formElem.name == 'totalquantity' || formElem.name == 's_city' || formElem.name == 'totalamount' || formElem.name == 'checksumkey' || formElem.name == 'hidevat' || formElem.name == 's_country'|| formElem.name == 'b_country'  || formElem.name == 's_state')
		{

        strSubmit += formElem.name + 
        '=' + escape(formElem.value) + '&'
        
    }
    break;
  } 
  
 
}
 strSubmit += "&logisticId=" + logisticid;

 saveAjax(strSubmit);
}

/* 
 * Function Name: saveAjax
 * Parameter: String of name/value pair of form elements (strSubmit).
 * Purpose: 
 * Summary: This function retrive the name/value pair of form elements and concatenate 
 * them into a single string. 
 * Note: Redio button and Checkbox cases is yet to implement.  
 *     
*/


function saveAjax(strSubmit)
{
 
  http.open('POST', URL, true);
  http.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
  http.onreadystatechange =  myPostFunction;
  http.send(strSubmit);   
}


function myPostFunction(){
    if(http.readyState==4){
	    var result=http.responseText;
	    
	    var returnResult = result.split(",")
	    var shippingcost = returnResult[0];
	    var amounttobepaid = returnResult[1];
	    var freeShippingStatus = returnResult[2];

	    if(shippingcost == 0 && freeShippingStatus =='N'){
	    	document.getElementById("is_shipping_cost_set").value = 0;
	    }else{
	    	document.getElementById("is_shipping_cost_set").value = 1;
	    }
	    
    	document.getElementById("shippingcost").innerHTML = "Rs."+numberformat(shippingcost,2);
    	document.getElementById("shippingrate").value = shippingcost;
    	document.getElementById("totalamountajax").innerHTML = "Rs."+numberformat(amounttobepaid,2);

		/*#####Check gift option is checked#####*/
		if(document.getElementById('asgift').checked == true){
			/*#####Add gift price to amount to be paid####*/
			var giftpack_charges = document.getElementById('giftpack_charges').value;
			var amounttobepaid = parseFloat(giftpack_charges) + parseFloat(amounttobepaid); 

			/*#####Check referral amount is exist#####*/
			if(document.getElementById("isexistref").value == 1){
				/*#####Check if referral option is checked#####*/
				if(document.getElementById("refcheck").checked == true) {
					var refamount = document.getElementById('refamt').value;
					var refdiscount = document.getElementById('hideref').value;
					if(refamount != "0.00"){
						var refamount = parseFloat(refamount) - parseFloat(giftpack_charges);
						var refdiscount = parseFloat(refdiscount) + parseFloat(giftpack_charges);
					}
					document.getElementById('refamt').value = refamount;
					document.getElementById('hideref').value = refdiscount;
					
					document.getElementById("refamtdiv").innerHTML=" Rs. "+numberformat(refamount,2);
					document.getElementById("refdiscount").innerHTML=" Rs. "+numberformat(refdiscount,2);

					var amounttobepaid = parseFloat(amounttobepaid) -  parseFloat(refdiscount); 
					document.getElementById('Amount').value = numberformat(amounttobepaid,2);
					document.getElementById('totalamtafterdeduction').innerHTML = "Rs."+numberformat(amounttobepaid,2);
				}
				/*#####Check if referral option is not checked#####*/
				else{
					document.getElementById('Amount').value = numberformat(amounttobepaid,2);
					document.getElementById('totalamtafterdeduction').innerHTML = "Rs."+numberformat(amounttobepaid,2);
				}
			}
			/*#####Check referral amount if not exist#####*/
			else{
				document.getElementById('Amount').value = numberformat(amounttobepaid,2);
				document.getElementById("totalamtaftergiftpack").innerHTML = "Rs."+numberformat(amounttobepaid,2);
			}

			var chkSumKey =  document.getElementById('checksumkey').value;
			updateCheckSum(document.getElementById("Amount").value,chkSumKey);
		}
		/*#####If gift option is not checked#####*/
		else{
			/*#####Add gift price to amount to be paid####*/
			//var giftpack_charges = "0.00";
			//var amounttobepaid = giftpack_charges + parseFloat(amounttobepaid); 
			
			/*#####Check referral amount is exist#####*/
			if(document.getElementById("isexistref").value == 1)
			{
				/*#####Check if referral option is checked#####*/
				if(document.getElementById("refcheck").checked == true) 
				{
					var refamount = document.getElementById('refamt').value;
					var refdiscount = document.getElementById('hideref').value;
					if(refamount != "0.00")
					{
						var refamount = parseFloat(refamount) + parseFloat(giftpack_charges);
						var refdiscount = parseFloat(refdiscount) - parseFloat(giftpack_charges);
					}
					
					document.getElementById('refamt').value = refamount;
					document.getElementById('hideref').value = refdiscount;
					
					document.getElementById("refamtdiv").innerHTML=" Rs. "+numberformat(refamount,2);
					document.getElementById("refdiscount").innerHTML=" Rs. "+numberformat(refdiscount,2);

					var amounttobepaid = parseFloat(amounttobepaid) -  parseFloat(refdiscount); 
					document.getElementById('Amount').value = numberformat(amounttobepaid,2);
					document.getElementById('totalamtafterdeduction').innerHTML = "Rs."+numberformat(amounttobepaid,2);
				}
				/*#####Check if referral option is not checked#####*/
				else
				{
					document.getElementById('Amount').value = numberformat(amounttobepaid,2);
					document.getElementById('totalamtafterdeduction').innerHTML = "Rs."+numberformat(amounttobepaid,2);
				}
			}
			/*#####Check referral amount does not exist#####*/
			else
			{
				document.getElementById('Amount').value = numberformat(amounttobepaid,2);
				document.getElementById("totalamtaftergiftpack").innerHTML = "Rs."+numberformat(amounttobepaid,2);
			}

			var chkSumKey =  document.getElementById('checksumkey').value;
			updateCheckSum(document.getElementById("Amount").value,chkSumKey);
		}
    	document.getElementById("setshippingcost").innerHTML = "";
    	document.getElementById("settotalamountajax").innerHTML = "";
	}
}

var req = null;
function updateCheckSum(amt,chkSumKey)
{
    if(window.XMLHttpRequest){ 
        req = new XMLHttpRequest(); 
    } 
    else if (window.ActiveXObject){ 
        req  = new ActiveXObject("Microsoft.XMLHTTP"); 
    } 

    var formurl="mkupdatechecksum.php";
     
    req.onreadystatechange = function(){
        if (req.readyState == 4)
	    {
	       if(req.responseText == 'S')
	       {
	      
	        
	       }
	       else if(req.responseText == 'F')
			{
	          
			}
	
        } 
    };
    req.open('GET',formurl+"?amt="+amt+"&chksumkey="+chkSumKey, true);
    req.setRequestHeader("Content-Type", "text/xml");
    req.send(null);
}


function onchange_country(country_type)
{
	if(window.XMLHttpRequest){
        var http = new XMLHttpRequest();
    }else if(window.ActiveXObject){
        var http = new ActiveXObject("Microsoft.XMLHTTP");
    }

   document.getElementById("s_city").value = "";
       
    if (country_type == 's_country'){
    var country = document.getElementById("s_country").value;
    }
    else {
    var country = document.getElementById("b_country").value;
    }
    
    
    URL_country = URL +"?country=" + country+"&action="+country_type;
    
    http.open("GET",URL_country, true); 
            
            http.onreadystatechange = function() 
        	{ 
        		if(http.readyState == 4) 
        		{ 
            		  if(http.status == 200) 
            		  { 
      				      
      				     var state_list = http.responseText;
      				     var return_state_list = state_list.split(",")
      				    
      				     
      				     if (return_state_list[1] == 's_country'){
      				     	
      				     	document.getElementById("s_state_list").innerHTML="";
	   					 	document.getElementById("s_state_list").innerHTML = return_state_list[0];
	   					 }
            		     else {
            		     	
      				     	document.getElementById("b_state_list").innerHTML="";
	   					 	document.getElementById("b_state_list").innerHTML = return_state_list[0];
	   					 }
	   				
        	       	}
        	    }
          
            }
              http.send(null);
              
              
            
}

/*
 *this function load all country states of shipping country and replace all values of billing elements with shipping elements
 */

function flushCall(){
   
    /*Load country code of shipping country*/
    var s_country = document.getElementById('s_country').value;
    var b_country = document.getElementById('b_country').value;
    var indexof_s_state = document.getElementById('s_state').selectedIndex;
    var obj_S_State = document.getElementById('s_state');
    
    if(document.getElementById('s_state').type=='select-one'){
    	
    	var obj_B_State = document.getElementById('b_state');
    	if(obj_B_State.options){
    		for (var i=0; i < obj_B_State.options.length; i++) {
    			obj_B_State.options[i].value = null;
    			obj_B_State.options[i].text = null;
    		}
    	}
    	else {
    		obj_B_State.value = null;
    		obj_B_State.text = null;
    	}
	    	document.getElementById("b_state_list").innerHTML="";
	    	var b_text_box = '<select name="b_state" id="b_state" class="select"></select>';
	    	document.getElementById("b_state_list").innerHTML= b_text_box;
	    	document.getElementById("b_state_list").disabled= true;
	    	var obj_B_State = document.getElementById('b_state');
	    	if(obj_B_State.options){
		    	for (var i=0; i < obj_B_State.options.length; i++) {
		       		 obj_B_State.options[i].value = null;
		       	 	 obj_B_State.options[i].text = null;
		        }
	    	}
	        
	        for (var i=0; i < obj_S_State.options.length; i++) {
	      	  var sopt = obj_S_State.options[i];
	      	  var b_options = new Option(sopt.text, sopt.value, indexof_s_state, sopt.selected);
	      	  obj_B_State.options[i] = b_options;
	         }
    }else{
    	var b_text_box = '<input type="text" name="b_state" id="b_state" value="">';
    	
    	document.getElementById("b_state_list").innerHTML="";
	   	document.getElementById("b_state_list").innerHTML = b_text_box;
    }
    
	var add = "b_";
	if(document.getElementById('billtoship').checked == true){
		document.getElementById(add+'firstname').value=document.getElementById('s_firstname').value;
		document.getElementById(add+'firstname').disabled= true;
		document.getElementById(add+'lastname').value=document.getElementById('s_lastname').value;
		document.getElementById(add+'lastname').disabled= true;
		document.getElementById(add+'address').value=document.getElementById('s_address').value;
		document.getElementById(add+'address').disabled= true;
		document.getElementById(add+'city').value=document.getElementById('s_city').value;
		document.getElementById(add+'city').disabled= true;
		document.getElementById(add+'country').selectedIndex=document.getElementById('s_country').selectedIndex;
		document.getElementById(add+'country').disabled= true;
 
		if(document.getElementById('s_state').type=='select-one'){
	        document.getElementById(add+'state').selectedIndex=document.getElementById('s_state').selectedIndex;
			document.getElementById(add+'state').disabled= true;
		}else{
			document.getElementById(add+'state').value=document.getElementById('s_state').value;
			document.getElementById(add+'state').disabled= true;
		}
        
	    	document.getElementById("b_state_list").disabled= true;
		document.getElementById(add+'zipcode').value=document.getElementById('s_zipcode').value;
		document.getElementById(add+'zipcode').disabled= true;
		document.getElementById(add+'phone').value=document.getElementById('s_phone').value;
		document.getElementById(add+'phone').disabled= true;
		document.getElementById(add+'mobile').value=document.getElementById('s_mobile').value;
		document.getElementById(add+'mobile').disabled= true;
		document.getElementById(add+'email').value=document.getElementById('s_email').value;
		document.getElementById(add+'email').disabled= true;
		document.getElementById('updatebill').disabled= true;
	}
	if(document.getElementById('billtoship').checked == false){
		document.getElementById(add+'firstname').disabled= false;
		document.getElementById(add+'lastname').disabled= false;
		document.getElementById(add+'address').disabled= false;
		document.getElementById(add+'city').disabled= false;
		document.getElementById(add+'state').disabled= false;
	    	document.getElementById("b_state_list").disabled= false;
		document.getElementById(add+'country').disabled= false;
		document.getElementById(add+'zipcode').disabled= false;
		document.getElementById(add+'phone').disabled= false;
		document.getElementById(add+'mobile').disabled= false;
		document.getElementById(add+'email').disabled= false;
		document.getElementById('updatebill').disabled= false;
	}
}

function updateShipping(){
	
	
	if(window.XMLHttpRequest){
        var http = new XMLHttpRequest();
    }else if(window.ActiveXObject){
        var http = new ActiveXObject("Microsoft.XMLHTTP");
    }
    
	var logisticId = document.getElementById('s_option').value;
    var totalquantity = document.getElementById('totalquantity').value;
    var s_city = document.getElementById('s_city').value;
    var totalamount = document.getElementById('totalamount').value;
    var checksumkey = document.getElementById('checksumkey').value;
    var hidevat = document.getElementById('hidevat').value;
    var s_country = document.getElementById('s_country').value;
    var b_country = document.getElementById('b_country').value;
    var s_state = document.getElementById('s_state').value;
    var s_zipcode = document.getElementById('s_zipcode').value;
    var cod_charges = document.getElementById('cash').value;
    var payby = "other";

    if(document.getElementById('cod')!=null &&  document.getElementById('cod').checked==true)
        payby="cod";
    else if (document.getElementById('ch')!=null && document.getElementById('ch').checked==true)
        payby="ch";
    else if (document.getElementById('net')!=null && document.getElementById('net').checked==true)
        payby="net";
    

    URL_Logistic = URL + "?totalquantity="+ totalquantity + "&s_city="+ s_city +"&totalamount="+ totalamount +"&checksumkey="+ checksumkey +"&hidevat="+ hidevat +"&s_country="+ s_country +"&b_country=" + b_country + "&logisticId=" + logisticId + "&action=logistic"+"&s_state="+ s_state+"&payby="+payby+"&s_zipcode="+s_zipcode;
    
    http.open("GET",URL_Logistic, true); 
            
            http.onreadystatechange = function() 
        	{ 
        		if(http.readyState == 4) 
        		{ 
            		  if(http.status == 200) 
            		  { 
            		  		var result=http.responseText;
						    var returnResult = result.split(",")
						    var shippingcost = returnResult[0];
						    var amounttobepaid = returnResult[1];
						    var freeShippingStatus = returnResult[2];

						    if(shippingcost == 0 && freeShippingStatus =='N'){
						    	document.getElementById("is_shipping_cost_set").value = 0;                                
						    }else{
						    	document.getElementById("is_shipping_cost_set").value = 1;
						    }                            

					    	document.getElementById("shippingcost").innerHTML = "Rs."+numberformat(shippingcost,2);
					    	document.getElementById("shippingrate").value = shippingcost;
					    	document.getElementById("totalamountajax").innerHTML = "Rs."+numberformat(amounttobepaid,2);
							document.getElementById("setshippingcost").innerHTML = "";
					    	document.getElementById("settotalamountajax").innerHTML = "";
							
                            if(payby=="cod")
                                amounttobepaid=parseInt(cod_charges)+parseInt(amounttobepaid);

							//#####Check gift option is checked#####
							if(document.getElementById('asgift').checked == true)
							{
								//#####Add gift price to amount to be paid####
								var giftpack_charges = document.getElementById('giftpack_charges').value;
								var amounttobepaid = parseFloat(giftpack_charges) + parseFloat(amounttobepaid); 
					
								//#####Check referral amount is exist#####
								if(document.getElementById("isexistref").value == 1)
								{
									//#####Check if referral option is checked#####
									if(document.getElementById("refcheck").checked == true) 
									{
										var refamount = document.getElementById('refamt').value;
										var refdiscount = document.getElementById('hideref').value;
										if(refamount != "0.00")
										{
											var refamount = parseFloat(refamount) - parseFloat(giftpack_charges);
											var refdiscount = parseFloat(refdiscount) + parseFloat(giftpack_charges);
										}
										document.getElementById('refamt').value = refamount;
										document.getElementById('hideref').value = refdiscount;
										
										document.getElementById("refamtdiv").innerHTML=" Rs. "+numberformat(refamount,2);
										document.getElementById("refdiscount").innerHTML=" Rs. "+numberformat(refdiscount,2);
					
										var amounttobepaid = parseFloat(amounttobepaid) -  parseFloat(refdiscount); 
										document.getElementById('Amount').value = numberformat(amounttobepaid,2);
										document.getElementById('totalamtafterdeduction').innerHTML = "Rs."+numberformat(amounttobepaid,2);
									}
									//#####Check if referral option is not checked#####
									else
									{
										document.getElementById('Amount').value = numberformat(amounttobepaid,2);
										document.getElementById('totalamtafterdeduction').innerHTML = "Rs."+numberformat(amounttobepaid,2);
									}
								}
								//#####Check referral amount if not exist#####
								else
								{
									document.getElementById('Amount').value = numberformat(amounttobepaid,2);
									document.getElementById("totalamtaftergiftpack").innerHTML = "Rs."+numberformat(amounttobepaid,2);
								}
					
								var chkSumKey =  document.getElementById('checksumkey').value;
								updateCheckSum(document.getElementById("Amount").value,chkSumKey);
								
							}
							/*#####If gift option is not checked#####*/
							else
							{
								/*#####Add gift price to amount to be paid####*/
								//var giftpack_charges = "0.00";
								//var amounttobepaid = giftpack_charges + parseFloat(amounttobepaid); 
								
								/*#####Check referral amount is exist#####*/
								if(document.getElementById("isexistref").value == 1)
								{
									/*#####Check if referral option is checked#####*/
									if(document.getElementById("refcheck").checked == true) 
									{
										var refamount = document.getElementById('refamt').value;
										var refdiscount = document.getElementById('hideref').value;
										if(refamount != "0.00")
										{
											var refamount = parseFloat(refamount) + parseFloat(giftpack_charges);
											var refdiscount = parseFloat(refdiscount) - parseFloat(giftpack_charges);
										}
										
										document.getElementById('refamt').value = refamount;
										document.getElementById('hideref').value = refdiscount;
										
										document.getElementById("refamtdiv").innerHTML=" Rs. "+numberformat(refamount,2);
										document.getElementById("refdiscount").innerHTML=" Rs. "+numberformat(refdiscount,2);
					
										var amounttobepaid = parseFloat(amounttobepaid) -  parseFloat(refdiscount); 
										document.getElementById('Amount').value = numberformat(amounttobepaid,2);
										document.getElementById('totalamtafterdeduction').innerHTML = "Rs."+numberformat(amounttobepaid,2);
									}
									/*#####Check if referral option is not checked#####*/
									else
									{
										document.getElementById('Amount').value = numberformat(amounttobepaid,2);
										document.getElementById('totalamtafterdeduction').innerHTML = "Rs."+numberformat(amounttobepaid,2);
									}
								}
								/*#####Check referral amount does not exist#####*/
								else
								{
									document.getElementById('Amount').value = numberformat(amounttobepaid,2);
									document.getElementById("totalamtaftergiftpack").innerHTML = "Rs."+numberformat(amounttobepaid,2);
								}
						
								var chkSumKey =  document.getElementById('checksumkey').value;
								updateCheckSum(document.getElementById("Amount").value,chkSumKey);
							}					    	
							
							//added for c&w to check for negative account balance after shipping cost is added
							/*if(document.getElementById("remainingpoints") != null){
								checkAccountBalanceAfterShip(document.getElementById("availablepoints_hidden").value,amounttobepaid,shippingcost);								
							}*/
      				      
							var shippingRate = parseFloat(document.getElementById("shippingrate").value);
							var giftpack_charges = 0;
							var subtotal = parseFloat(document.getElementById("finalcostwithoutshippingandgift").value);
							if(document.getElementById('asgift').checked == true){
								giftpack_charges = parseFloat(document.getElementById("giftpack_charges").value);
							}
							else{
								giftpack_charges = 0;
							}
							document.getElementById("totalamtaftergiftpackwodiscount").innerHTML = "Rs." + numberformat(shippingRate + giftpack_charges + subtotal,2);

            		  }
            		  else
            		  {
            		      alert(http.status + "===" + http.statusText +" Could not retrieve shipping cost from server. Please try again/ contact our customer care - Sorry for the inconvenience.");
                      }
        		}
        	 }
            http.send(null);

}


function view_shipping_detail(){
	
	if(window.XMLHttpRequest){
        var http = new XMLHttpRequest();
    }else if(window.ActiveXObject){
        var http = new ActiveXObject("Microsoft.XMLHTTP");
    }
    
	var logisticId = document.getElementById('s_option').value;
   
    var s_city = document.getElementById('s_city').value;
    
    var s_country = document.getElementById('s_country').value;
    
    var s_state = document.getElementById('s_state').value;
    
   
    
    URL_view_shipping = http_loc + "/view_shipping_detail.php"  +"?logisticId=" + logisticId +"&s_city="+ s_city +"&s_country="+ s_country + "&s_state="+ s_state; 
   
    
     http.open("GET",URL_view_shipping, true); 
            
            http.onreadystatechange = function() 
        	{ 
        		if(http.readyState == 4) 
        		{ 
            		  if(http.status == 200) 
            		  { 
            		  		var result=http.responseText;
            		  		document.getElementById("view_shipping_details").style.display = "block";
            		  		document.getElementById("view_shipping_details").innerHTML = "";
						    document.getElementById("view_shipping_details").innerHTML = result;
						    
            		  }
        		}
        	}
	
     http.send(null);	
}


/*
* Function to close the popup div
*/

function closePopupDiv(divId){
	document.getElementById(divId).style.display = "none";
}
function updateShipping_re()
{
	if(window.XMLHttpRequest)
	{
		var http = new XMLHttpRequest();
	}
	else if(window.ActiveXObject)
	{
		var http = new ActiveXObject("Microsoft.XMLHTTP");
	}
    var logisticId = document.getElementById('s_option').value;
	var s_city = document.getElementById('s_city').value;
	var s_country = document.getElementById('s_country').value;
	var s_state = document.getElementById('s_state').value;
	var s_zipcode = document.getElementById('s_zipcode').value;
	var cod_charges = document.getElementById('cash').value;

	if( logisticId == 'AR' && s_country != 'IN')
	{
		alert("Shipping option ARAMEX(cash on delivery) is not available for the selected country. Please select either DTDC or BlueDart");
		//defaulting to BlueDart
		document.getElementById('s_option').selectedIndex = 0;
		document.getElementById('net').checked = true;
		document.getElementById("codtext").style.display = "none";
		document.getElementById("codcharge").style.display = "none";
		logisticId = 'BD';
	}


	URL_Logistic = "./mkshippingcalculate_re.php?s_city="+ s_city +"&s_country="+ s_country +"&logisticId=" + logisticId + "&action=logistic"+"&s_state="+ s_state+"&s_zipcode="+s_zipcode;


	http.onreadystatechange = function()
	{
		if( http.readyState == 4 && http.status == 200)
		{
			var result=http.responseText;
			var shippingcost = result;

			document.getElementById("shippingcost").innerHTML = shippingcost;
			
			//added by arun, to proceed on shipping cost calculated correctly
			if(shippingcost == 0){
				document.getElementById("is_shipping_cost_set").value = 0;                                
			}else{
			   	document.getElementById("is_shipping_cost_set").value = 1;
			}			
			updateTotalAmount();
		}
	}
	http.open("GET",URL_Logistic,true);
	http.send(null);
}
