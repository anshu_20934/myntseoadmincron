{* $Id: mykritmeta.tpl,v 1.26 2006/04/10 07:36:17 max Exp $ *}
<head>
<title>
	{if $pageTitle ne ''}{$pageTitle}{elseif $pageHeader ne ''}{$pageHeader}{else}Myntra{/if}
</title>
<meta name="description" content="{$metaDescription|escape}" />
<meta name="keywords" content="{$metaKeywords|escape}" />
<link rel="shortcut icon" href="{$SkinDir}/favicon.ico">

<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>

<link rel="stylesheet" href="{$SkinDir}/{#mkCSSFile#}" />

{if $affiliateId neq '' && $themeid eq 'COBRANDED_UI'}
	<link rel="stylesheet" href="{$SkinDir}/affiliatetemplates/affiliate.css" />
{/if}  
<script type="text/javascript">
    var http_loc = '{$http_location}'
 </script>
<script type="text/javascript" src="{$SkinDir}/js_script/email_validate.js"></script>
<script type="text/javascript" src="{$SkinDir}/js_script/redirecturl.js"></script>
<base href="{$baseurl}">

 </head>
