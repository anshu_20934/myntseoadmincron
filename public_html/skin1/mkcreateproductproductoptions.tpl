<div class="foot" style="clear:both;"></div> 
<div class="head">
	<p>select different {$productTypeLabel|lower}</p>
</div>
{if $redirectStyleFile eq 'createhanuman' }
	{assign var="customize" value=$redirectStyleFile}          
{else}
   	{assign var="customize" value="create-my"}  
{/if}
<a name="morestyles" ></a>
<div style="text-align:center;margin:0px auto;">
<div id="mygallery" class="stepcarousel" style="width:76%;float:left;height:70px;_height:70px;border:0px solid #333333;margin-left:40px;_margin-left:20px;">
	<div class="belt" style="float:left;width:auto;left:0px;">
		<div style="width:auto;float:left;">
			{section name=num loop=$productStyleIconImages}
				<div class="panel" style="height:77px;width:60px;font-size:0.8em;text-align:center;">
					{if $productStyleId eq $productStyleIconImages[num].id}
						<div class="optionselect" style="width:40px;height:40px;margin:5px;float:left;padding:2px;border:2px solid #000000;">
							<img  src="{$productStyleIconImages[num].image_i}"width="40" height="40"  onclick="setHiddenFieldValue('productStyleId',{$productStyleIconImages[num].id});"	ALT="Choose Style  for {$productStyleIconImages[num].name}"	title="{$productStyleIconImages[num].name}"/>
						</div>
					{else}
						<div class="optionnotselect" style="width:40px;height:40px;margin:5px;float:left;padding:2px;border:2px solid #dddddd;">
							<a href="{$customize}/{$productStyleIconImages[num].name|replace:' ':'-'}/{$productStyleIconImages[num].id}?cstatus={$cstatus}" ><img src="{$productStyleIconImages[num].image_i}" width="40" height="40"   onclick="setHiddenFieldValue('productStyleId',{$productStyleIconImages[num].id});" ALT="Choose Style  for {$productStyleIconImages[num].name}" title="{$productStyleIconImages[num].name}"></a>
						</div>
					{/if}
				</div>
			{/section}
		</div><!--links-->	
	</div><!--belt-->
</div><!--gallery-->
<div style="width:75%;_width:77%;float:left;margin-left:40px;_margin-left:18px;">
<div id='leftnavig' style="float:left;width:auto;height:20px;margin-top:10px;margin-left:12px;_margin-left:10px;position:relative">
<table >
  <tr>
  <td><a  href="javascript:void(0);" onclick="javascript:stepcarousel.stepBy('mygallery', -1)" style="text-decoration:none;" ><img src="{$cdn_base}/skin1/mkimages/previous_tpl.gif" /></a>
  </td>
   <td>
     <span style='font-size:0.7em;text-align:center;height:20px;'  ><a href="javascript:void(0);" onclick="javascript:stepcarousel.stepBy('mygallery', -1)" style="text-decoration:none;" >previous</a></span>
  </td>
  </tr>
</table>
</div>

<div id='rightnavig' style="width:auto;float:right;height:20px;margin-top:10px;">

  <table >
  <tr>
  <td><span style='font-size:0.7em;height:20px;'  ><a href="javascript:void(0);" onclick="javascript:stepcarousel.stepBy('mygallery', 1)" style="text-decoration:none;" >next</a></span>
  </td>
   <td>
   <a href="javascript:void(0);" onclick="javascript:stepcarousel.stepBy('mygallery', 1)" style="text-decoration:none;" >
    <img src="{$cdn_base}/skin1/mkimages/next_tpl.gif" /></a> 
  </td>
  </tr>
</table>
</div>
</div>
</div>

<div class="foot" style="clear:both;"></div>