<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html xml:lang="en" xmlns:fb="http://www.facebook.com/2008/fbml">
<head>
    <META http-equiv="Content-Style-Type" content="text/css">
    <META http-equiv="Content-Script-Type" content="type/javascript">
    <META http-equiv="Content-Type" content="text/html; charset=utf-8">
{include file="site/header.tpl" }
    <!--[if lt IE 8]>
    <link rel="stylesheet" href="new-skin-css/ie.css" type="text/css" media="screen, projection"><![endif]-->

</head>

<body class="{$_MAB_css_classes}">
{include file="site/menu.tpl"}
<div class="container clearfix">
<h1 class="head1">Terms of Use &amp; Service</h1>
<div class="content clearfix prepend">
<!--    	<div class="content-left left span-26">-->
<!--		</div>-->
<!--		<div class="content-right right prepend-1 last span-6">-->
<!--		</div>-->

<p>
    <strong>Please read the following terms and conditions very carefully as your use of service is subject to your
        acceptance of and compliance with the following terms and conditions (&quot;Terms&quot;).
    </strong>
</p>

<p>By subscribing to or using any of our services you agree that you have read, understood and are bound by the Terms,
    regardless of how you subscribe to or use the services. If you do not want to be bound by the Terms, you must not
    subscribe to or use our services. These Terms and various other policies are binding as per the provisions of
    the Information Technology (Intermediaries guidelines) Rules, 2011 formulated under the Information Technology Act
    of 2000.</p>

<p>In these Terms, references to &quot;you&quot;, &quot;User&quot; shall mean the end user accessing the Website, its 
    contents and using the Services offered through the Website, &quot;Service Providers&quot; mean independent third 
    party service providers, and &quot;Myntra.com&quot;, &quot;we&quot;, &quot;us&quot; and &quot;our&quot; shall 
    mean Vector E-commerce Private Limited and its affiliates.</p>


<p>
    <strong><u>1. Introduction:</u></strong>
</p>


<p>
    a) <a href="http://www.myntra.com">www.myntra.com</a>  (&quot;Website&quot;) is an Internet based content and 
    e-commerce portal operated by Vector E-commerce Private Limited, a company incorporated under the laws of India.
</p>


<p>
    b) Use of the Website is offered to you conditioned on acceptance without modification of all the terms, conditions 
    and notices contained in these Terms, as may be posted on the Website from time to time. Myntra.com at its sole 
    discretion reserves the right not to accept a User from registering on the Website without assigning any reason 
    thereof.

</p>

<p>
    <strong><u>2. User Account, Password, and Security:</u></strong>
</p>


<p>
    You will receive a password and account designation upon completing the Website's registration process. You are 
    responsible for maintaining the confidentiality of the password and account, and are fully responsible for all 
    activities that occur under your password or account. You agree to (a) immediately notify Myntra.com of any 
    unauthorized use of your password or account or any other breach of security, and (b) ensure that you exit from your 
    account at the end of each session. Myntra.com cannot and will not be liable for any loss or damage arising from 
    your failure to comply with this Section 2. </p>


<p>
    <strong><u>3. Services Offered:</u></strong>
</p>


<p>
    Myntra.com provides a number of Internet-based services through the Web Site (all such services, collectively, the 
    &quot;Service&quot;). One such service enables users to purchase original merchandise such as clothing, footwear and 
    accessories from various fashion and lifestyle brands (collectively, &quot;Products&quot;). The Products can be 
    purchased through the Website through various methods of payments offered. The sale/purchase of Products shall be 
    additionally governed by specific policies of sale, like cancellation policy, return policy, etc. (which are found 
    on the following URL http://www.myntra.com/faqs), and all of which are incorporated here by reference. In addition, these terms and policies may be further supplemented by Product specific conditions, which may be displayed on the 
    webpage of that Product.

</p>


<p>
    <strong><u>4. Privacy Policy:</u></strong>
</p>


<p>
    The User hereby consents, expresses and agrees that he has read and fully understands the <a 
    href="privacy_policy.php">Privacy Policy</a> of Myntra.com. The user further consents that the terms and contents of such Privacy Policy are acceptable to him.
</p>


<p>
    <strong><u>5. Limited User:</u></strong>
</p>


<p>
    The User agrees and undertakes not to reverse engineer, modify, copy, distribute, transmit, display, perform, 
    reproduce, publish, license, create derivative works from, transfer, or sell any information or software obtained 
    from the Website. Limited reproduction and copying of the content of the Website is permitted provided that Myntra's 
    name is stated as the source and prior written permission of Myntra.com is sought. For the removal of doubt, it is 
    clarified that unlimited or wholesale reproduction, copying of the content for commercial or non-commercial purposes 
    and unwarranted modification of data and information within the content of the Website is not permitted.
</p>


<p>
    <strong><u>6. User Conduct and Rules:</u></strong>
</p>


<p>You agree and undertake to use the Website and the Service only to post and upload messages and material that are 
    proper. By way of example, and not as a limitation, you agree and undertake that when using a Service, you will not:
</p>


<p>
    (a)&nbsp;&nbsp;defame, abuse, harass, stalk, threaten or otherwise violate the legal rights of others;
</p>


<p>
    (b)&nbsp;&nbsp;publish, post, upload, distribute or disseminate any inappropriate, profane, defamatory, infringing, 
    obscene, indecent or unlawful topic, name, material or information;
</p>


<p>
    (c)&nbsp;&nbsp;upload files that contain software or other material protected by intellectual property laws unless 
    you own or control the rights thereto or have received all necessary consents; you own or control the rights thereto 
    or have received all necessary consents;
</p>


<p>
    (d)&nbsp;&nbsp;upload or distribute files that contain viruses, corrupted files, or any other similar software or 
    programs that may damage the operation of the Website or another's computer;
</p>


<p>
    (e)&nbsp;&nbsp;conduct or forward surveys, contests, pyramid schemes or chain letters;
</p>


<p>
    (f)&nbsp;&nbsp;download any file posted by another user of a Service that you know, or reasonably should know, 
    cannot be legally distributed in such manner;
</p>


<p>
    (g)&nbsp;&nbsp;falsify or delete any author attributions, legal or other proper notices or proprietary designations or labels of the origin or source of software or other material contained in a file that is uploaded;

</p>


<p>
    (h)&nbsp;&nbsp;violate any code of conduct or other guidelines, which may be applicable for or to any particular 
    Service;
</p>


<p>
    (i)&nbsp;&nbsp;violate any applicable laws or regulations for the time being in force in or outside India; and

</p>


<p>
    (j)&nbsp;&nbsp;violate, abuse, unethically manipulate or exploit, any of the terms and conditions of this Agreement 
    or any other terms and conditions for the use of the Website contained elsewhere.
</p>


<p>
    <strong><u>7. User Warranty and Representation:</u></strong>
</p>


<p>
    The user guarantees, warrants, and certifies that you are the owner of the content which you submit or otherwise 
    authorised to use the content and that the content does not infringe upon the property rights, intellectual property 
    rights or other rights of others. You further warrant that to your knowledge, no action, suit, proceeding, or 
    investigation has been instituted or threatened relating to any content, including trademark, trade name service 
    mark, and copyright formerly or currently used by you in connection with the Services rendered by Myntra.com.
</p>


<p>
    <strong><u>8. Exactness Not Guaranteed:</u></strong>
</p>


<p>
    Myntra.com hereby disclaims any guarantees of exactness as to the finish and appearance of the final Product as 
    ordered by the user. The quality of any products, Services, information, or other material purchased or obtained by 
    you through the Website may not meet your expectations. Alterations to certain aspects of your order such as the 
    merchandise brand, size, color etc. may be required due to limitations caused by availability of product difference 
    in size charts of respective brands etc. In this instance you agree that Myntra.com will send an approval request 
    via the email address, which you submitted when placing your order. If you do not agree with the requested change 
    you retain the right to reject the requested production change by replying to it within 10 days of it being sent to 
    you. Myntra.com may re-request that you accept a production alteration one additional time if an alternative method to send your merchandise is available. If you reject this 2nd request your order will be cancelled and you will be 
    fully refunded via your initial method of payment.
</p>


<p>
    <strong><u>9. Intellectual Property Rights:</u></strong>
</p>


<p>
    a) Unless otherwise indicated or anything contained to the contrary or any proprietary material owned by a third
    party and so expressly mentioned, Myntra.com owns all Intellectual Property Rights to and into the Website,
    including, without limitation, any and all rights, title and interest in and to copyright, related rights, patents,
    utility models, trademarks, trade names, service marks, designs, know-how, trade secrets and inventions (whether
    patentable or not), goodwill, source code, meta tags, databases, text, content, graphics, icons, and hyperlinks. You
    acknowledge and agree that you shall not use, reproduce or distribute any content from the Website belonging to
    Myntra.com without obtaining authorization from Myntra.com.
</p>


<p>
    b) Notwithstanding the foregoing, it is expressly clarified that you will retain ownership and shall solely be 
    responsible for any content that you provide or upload when using any Service, including any text, data, 
    information, images, photographs, music, sound, video or any other material which you may upload, transmit or store 
    when making use of our various Service. However, with regard to the product customization Service (as against other 
    Services like blogs and forums) you expressly agree that by uploading and posting content on to the Website for 
    public viewing and reproduction/use of your content by third party users, you accept the User whereby you grant a 
    non-exclusive license for the use of the same.
</p>


<p>
    <strong><u>10. Links To Third Party Sites:</u></strong>
</p>


<p>
    The Website may contain links to other websites (&quot;Linked Sites&quot;). The Linked Sites are not under the 
    control of Myntra.com or the Website and Myntra.com is not responsible for the contents of any Linked Site, 
    including without limitation any link contained in a Linked Site, or any changes or updates to a Linked Site. 
    Myntra.com is not responsible for any form of transmission, whatsoever, received by you from any Linked Site. 
    Myntra.com is providing these links to you only as a convenience, and the inclusion of any link does not imply 
    endorsement by Myntra.com or the Website of the Linked Sites or any association with its operators or owners 
    including the legal heirs or assigns thereof. The users are requested to verify the accuracy of all information on 
    their own before undertaking any reliance on such information.

</p>


<p>
    <strong><u>11. Disclaimer Of Warranties/Limitation Of Liability:</u></strong>
</p>


<p>
    Myntra.com has endeavoured to ensure that all the information on the Website is correct, but Myntra.com neitsher 
    warrants nor makes any representations regarding the quality, accuracy or completeness of any data, information, 
    product or Service. In no event shall Myntra.com be liable for any direct, indirect, punitive, incidental, special, 
    consequential damages or any other damages resulting from: (a) the use or the inability to use the Services or 
    Products; (b) unauthorized access to or alteration of the user's transmissions or data; (c) any other matter 
    relating to the services; including, without limitation, damages for loss of use, data or profits, arising out of or 
    in any way connected with the use or performance of the Website or Service. Neither shall Myntra.com be responsible 
    for the delay or inability to use the Website or related services, the provision of or failure to provide Services, 
    or for any information, software, products, services and related graphics obtained through the Website, or otherwise 
    arising out of the use of the website, whether based on contract, tort, negligence, strict liability or otherwise. 
    Further, Myntra.com shall not be held responsible for non-availability of the Website during periodic maintenance 
    operations or any unplanned suspension of access to the website that may occur due to technical reasons or for any 
    reason beyond Myntra.com's control. The user understands and agrees that any material and/or data downloaded or 
    otherwise obtained through the Website is done entirely at their own discretion and risk and they will be solely 
    responsible for any damage to their computer systems or loss of data that results from the download of such material 
    and/or data.
</p>


<p>
    <strong><u>12. Indemnification:</u></strong>
</p>


<p>
    You agree to indemnify, defend and hold harmless Myntra.com from and against any and all losses, liabilities, 
    claims, damages, costs and expenses (including legal fees and disbursements in connection therewith and interest 
    chargeable thereon) asserted against or incurred by Myntra.com that arise out of, result from, or may be payable by 
    virtue of, any breach or non-performance of any representation, warranty, covenant or agreement made or obligation 
    to be performed by you pursuant to these Terms.
</p>


<p>
    <strong><u>13. Pricing:</u></strong>
</p>


<p>
    Prices for products are described on our Website and are incorporated into these Terms by reference. All prices are in Indian rupees. Prices, products and Services may change at Myntra.com's discretion.


</p>


<p>
    <strong><u>14. Shipping:</u></strong>
</p>


<p>
    Title and risk of loss for all products ordered by you shall pass on to you upon Myntra.com shipment to the shipping 
    carrier.
</p>

<p>
    <strong><u>15. Termination:</u></strong>
</p>


<p>
    a)&nbsp;&nbsp;Myntra.com may suspend or terminate your use of the Website or any Service if it believes, in its sole 
    and absolute discretion that you have breached, violated, abused, or unethically manipulated or exploited any term 
    of these Terms or anyway otherwise acted unethically.

</p>


<p>
    b)&nbsp;&nbsp;Notwithstanding Section 15.a above, these Terms will survive indefinitely unless and until Myntra.com 
    chooses to terminate them.
</p>


<p>
    c)&nbsp;&nbsp; If you or Myntra.com terminates your use of the Website or any Service, Myntra.com may delete any 
    content or other materials relating to your use of the Service and Myntra.com will have no liability to you or any 
    third party for doing so.

</p>


<p>
    d)&nbsp;&nbsp;You shall be liable to pay for any Service or product that you have already ordered till the time of 
    Termination by either party whatsoever.
</p>


<p>
    <strong><u>16. Governing Law:</u></strong>
</p>


<p>
    These terms shall be governed by and constructed in accordance with the laws of India without reference to conflict 
    of laws principles and disputes arising in relation hereto shall be subject to the exclusive jurisdiction of the 
    courts at Bangalore.

</p>


<p>
    <strong><u>17. Severability:</u></strong>
</p>


<p>
    If any provision of the Terms is determined to be invalid or unenforceable in whole or in part, such invalidity or 
    unenforceability shall attach only to such provision or part of such provision and the remaining part of such 
    provision and all other provisions of these Terms shall continue to be in full force and effect.
</p>


<p>
    <strong><u>18. Report Abuse:</u></strong>
</p>

<p>
    As per these Terms, users are solely responsible for every material or content uploaded on to the Website. Users can be held legally liable for their contents and may be held legally accountable if their contents or material include, for example, defamatory comments or material protected by copyright, trademark, etc. If you come across any abuse or violation of these Terms, please report to <a href="mailto:reportabuse@myntra.com">reportabuse@myntra.com</a>.

</p>


</div>

</div>
<div class="divider">&nbsp;</div>
{include file="site/footer.tpl"}
</body>
</html>
