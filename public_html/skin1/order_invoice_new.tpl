<html>
	<head>
		<script Language="JavaScript" Type="text/javascript">
		var printIt = '{$print}';
		function printPage() {
			if(printIt == 'true'){
				window.print();
				window.close();
			}
		}
		</script>
	</head>

	<body onload="javascript:printPage()">
	<p align="center"><font color="red" size="30">Use PRISM for generating invoice</font></p>
	{*
		<div id="scroll_wrapper" class="print">
		    <div class="display" style="width:666px; margin: 15px auto;"> 
		        <div style="text-align: right;width:99%;margin-bottom:3px;">
		            {$pageno} / {$totalpages} 
		        </div>
		        <div style="width:99%;">
		            <div class="HeadLogo" style="float:left; width: 35%;">
		                <img style="border: none;" src="{$cdn_base}/skin1/myntra_images/myntra_logo_bw.jpg" height=38><br>
		                <span style="color:#666666; font-size: 11px; line-height: 1.3em;">
		                    {$invoice.companyName}<br>
		                    {$invoice.warehouse_addressline1}<br>
		                    {if $invoiceType == 'finance'}
		                    	{$invoice.warehouse_addressline2}<br>{$invoice.warehouse_city} {$invoice.warehouse_zipcode}<br>
		                    {else}
		                    	{$invoice.warehouse_addressline2}, {$invoice.warehouse_city} {$invoice.warehouse_zipcode}<br>
		                    	TEL: +91-80-43541999<br>
		                    {/if}
		            	<b>TIN No.: {$invoice.warehouse_tin}</b>
		                </span>
		            </div>
		            <div style="float:left; width: 20%; text-align:center;">
		                <strong style="font-size: 15px; font-weight:bold; font-family: Arial;">TAX INVOICE <br/>
	                       {if $invoice.ordertype eq 'ex'} (EXCHANGE) {else} {if $invoice.gift_status eq 'Y'} (GIFT-SHIPMENT) {/if}</strong>{/if}</strong>
		            </div>
		            <div style="float:right; margin-right: 5px;text-align:center;">
		                <div style="font-weight: bold; font-size: 15px; float:left; margin: 60px 10px 0 0;"></div>
		                <div style="float:right; width: 260px; margin-right: 10px">
		                    <span style="color: #555555;">{$invoice.tracking_no}</span><br>
       		                    <img src="{$invoice.tracking_code_url}" width='260' height='60'/><br><br>
					{if $invoiceType != 'finance'}
		                    	<span style="font-weight:bold; font-size:28px; text-transform: uppercase;">{$invoice.courier_code}{if $invoice.payment_method eq 'cod' && $invoice.payable_amount ne 0 }-COD{/if}
		                    	{if $invoice.shipping_method eq 'XPRESS'}
		                    		(X)
		                    	{/if}
		                    	{if $invoice.shipping_method eq 'PRIORITY'}
		                    		(P)
		                    	{/if}
		                    	</span>
		                    	{if $invoice.shipping_method eq 'XPRESS'}
			                    	<br><hr style="border: 10px solid black; padding:0; margin: 20px 0 0 0; width: 99%;"/>
					{/if}
		                    {/if}					
		               </div>
		               <div style="clear:both;"></div>
		            </div>
		            <div style="clear:both;"></div>
		        </div>
		        
		        <hr style="border: 1px solid black; padding:0; margin: 20px 0 0 0; width: 99%;"/>
		        
		        <div style="margin-top: 20px; width:99%;">
		            {if $invoiceType != 'finance'}
			            <div style="float:left; width: 15%; margin-top: 20px;">
			              <div id="verticalImageDiv"><img src="{$invoice.shipmentbarcodeUrl}" style="display:block; position:relative;" width='150' height='60'/></div>
			            </div>
		            {/if}
		            <div style="float:left; width: 40%; font-size:12px; border-right: 1px dotted #666666; margin-left: 10px;">
		                {if $invoiceType != 'finance'}
			                {if $invoice.payment_method eq 'cod' }
			                	{if $invoice.payable_amount eq 0 }
			                    	<strong style="font-size: 15px; margin:0; padding:0;">FREE GIFT ITEM</strong><br><br>
			                    {else}
			                    	<strong style="font-size: 15px; margin:0; padding:0;">CASH ON DELIVERY</strong><br><br>
			                    {/if}
			                    <strong style="font-size: 15px; margin:0; padding:0;">Amount to be Paid: {$invoice.payable_amount}</strong><br>
			                    <span class="caption">(Price is inclusive of all taxes)</span><br><br>
			                {else}
			                    <strong style="font-size: 15px; margin:0; padding:0;">PREPAID (No Amount Due)</strong><br><br>
			                    <strong style="font-size: 15px; margin:0; padding:0;">Amount Paid: {$invoice.payable_amount}
	                               {if $invoice.ordertype eq 'ex' }(Exchange Order){/if}</strong><br>
			                    <span class="caption">(Price is inclusive of all taxes)</span><br><br>
			                {/if}
		               	{/if}
		                <strong style="font-size: 15px; margin:0; padding:0;">Order No. : {$invoice.group_id}</strong><BR>
		                Order Date: {$invoice.order_date}<br><br>
		                <strong style="font-size: 15px; margin:0; padding:0;">Invoice No. : {$invoice.invoiceid}</strong><BR>
		                Invoice Date: {$invoice.invoice_date} <br/>
		                <strong style="font-size: 15px; margin:0; padding:0;">Shipment No. : {$invoice.orderid}</strong>
		                {if $invoice.weight != ""}<br><strong style="font-size: 15px; margin:0; padding:0;">Weight: {$invoice.weight} kgs </strong>{/if}
                                {if $invoice.courier_code eq 'FD'}
                                <br/><strong style="font-size: 15px; margin:5px; padding:0;">BILL SHIPPER</strong>
                                {/if}
			        </div>
		            <div style="float:left; width: 40%; margin-left: 20px;">
			            {if $invoiceType != 'finance'}
			                Ship To <strong style="font-size: 15px; font-weight:bold;">{$invoice.city_area_code} ({$invoice.zipcode})</strong> {if $invoice.promiseDate} ({$invoice.promiseDate}) {/if}  
			                <br><br>
		                {/if}
		                <strong style="font-size: 15px; font-weight:bold;">{$invoice.firstname} {$invoice.lastname}</strong><br>
		                {$invoice.address}, {$invoice.locality}<br>
		                {$invoice.city} {$invoice.zipcode}<br>
		                {$invoice.state}, {$invoice.country}<br>
		                {if $invoiceType != 'finance'}
		                	<strong style="font-size: 15px; font-weight:bold;">Phone: {$invoice.phone} {if $invoice.phone ne $invoice.customer_phone}, {$invoice.customer_phone}{/if}</strong>
		                {else}
		                	<strong style="font-size: 15px; font-weight:bold;">Tin No: {$invoice.tinNumber}</strong>
		                {/if}
		                {if $invoiceType != 'finance'}
				            {if $invoice.courier_code eq 'BD'}
				                <br><br>
				                <span class="caption">
				                    <strong style="font-size: 12px;">
		                                        IF UNDELIVERED PLEASE RETURN TO {if $invoice.warehouse eq 1}566668\MYN{elseif $invoice.warehouse eq 2}DEL/ITG-111117{/if}
				                    </strong>
				                </span>
				            {/if}
		                {/if}
		            </div>
		            <div style="clear:both;"></div>
		        </div>
		        
		        <hr style="border: 1px dotted black; padding:0; margin: 20px 0 0 0; width: 99%;"/>
		        
		         {if $invoice.ordertype eq 'ex'}
		         	<div style="margin-top: 10px; text-align:center;">
                    <b>Replacement item, not for sale</b><br>
                 {else}
                 	<div style="margin-top: 50px; text-align:center;">
                 {/if}
		            <strong style="font-size: 15px; font-weight:bold; font-family: Arial;">Invoice Details</strong><br>
		            <b>Note:</b> This shipment contains following items<br><br>
		        </div>
		        {$invoice.items_html}
		        <div style="clear:both;"></div>
		        
		        {if $invoiceType != 'finance'}
			        <div style="position: relative; bottom: 2px;">
                                    {if $amount_details.tax ne 0 }
			            <div style="text-align:left; margin-top: 30px; padding: 0;">
                                        *Differential VAT, if applicable, is borne by Vector E-Commerce Pvt Ltd.
				    </div>
                                    <div style="text-align:left; margin-top: 10px; padding: 0;">
                                    {else}
			            <div style="text-align:left; margin-top: 30px; padding: 0;">
                                    {/if}
				            If you have any questions, feel free to call customer care at <b>+91-80-43541999</b> or email <b>support@myntra.com</b>                
				    </div>
                                    {if $invoice.courier_code eq 'FD'}
                                        <br/>*Destination Octroi charges to be billed to shipper account
                                        <br/>
                                        *Terms and Conditions : Subject to the "Conditions of Carriage", which limits the liability of FedEx for loss, 
                                        delay or damage to the consignment. Visit www.fedex.com/in to view the "Conditions of Carriage"
                                    {/if}
				        <div><hr style="border: 1px solid black;"/></div>
				        <div style="text-align:center; margin: 5px 0px;">
				            100% Original Products | Free Shipping | 24 Hours Dispatch | Cash on Delivery | 30 Day Return
				        </div>
			        </div>
			    {/if}
		    </div>
		</div>
		*}
	</body>
</html>

