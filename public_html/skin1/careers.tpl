<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html xml:lang="en" xmlns:fb="http://www.facebook.com/2008/fbml">
<head>
<META http-equiv="Content-Style-Type" content="text/css">
<META http-equiv="Content-Script-Type" content="type/javascript">
<META http-equiv="Content-Type" content="text/html; charset=utf-8">
{include file="site/header.tpl" }
<!--[if lt IE 8]>
    <link rel="stylesheet" href="new-skin-css/ie.css" type="text/css" media="screen, projection"><![endif]-->

</head>

<body class="{$_MAB_css_classes}">
{include file="site/menu.tpl"}
<div class="container clearfix">

	<h1 class="head1">Careers</h1>	
    <div class="content clearfix">
		<div class="content-left left span-23 prepend last">			
				<p><strong>About Myntra</strong></p>
				<p>Myntra.com is ranked among the top 10 e-commerce companies in India and is scaling rapidly. Myntra was started by a group of IIT/IIM graduates in early 2007 and is headquartered in Bangalore with regional offices in New Delhi, Mumbai and Chennai. The company is funded by top tier Venture Capital Funds and is now among the best funded e-commerce companies in the country.</p>
				<p>In addition to accelerated growth opportunities, we foster openness, trust and respect in the workplace. We�re a warm and fun-filled place to work; we�re passionate about sport and music,debate and design and about getting things right. And while we are small, we are growing rapidly and our growth philosophy is to hire and retain the best. To this end, we offer all employees a culture of fun and learning, and a very competitive package of pay and benefits. Some highlights of our offer are as follows:</p>
				
				<p>
					<ul>
						<li>One of the best paying start-ups in the country</li>
						<li>All Myntra employees are eligible to stock options to ensure they receive a meaningful stake in the company</li>
						<li>11 paid holidays in a year for employees to celebrate national events and festivals with their families, along with 24 days of paid annual leave to help maintain work-life balance. Additional leave types that include paid paternity leave of up to 10 business days per annum and an option to work-from-home for women for a defined period at the birth of a child, in addition to standard maternity leave and benefits</li>
						<li>Every employee is assigned two days of professional development opportunities per year in the form of trainings, certifications, seminar and conference participation opportunities etc.</li>
						<li>Myntra provides medical insurance to all its employees. 100% of the premium for this is paid by Myntra. This provides reimbursement per annum of up to Rs. 2 Lakhs per family including parents/in-laws.</li>
						<li>All employees are eligible for Group Personal Accident insurance. Due to bodily injury or death as a result of an accident, GPA insurance provides benefit of upto Rs. 10 Lakhs per defined payment schedule. Myntra pays 100% of the premium.</li>
						<li>We foster a culture of fun and transparency with events such as our Monthly All Hands Meets, Sports @ Myntra initiative, Founding Day Annual Celebration for employees and their families and lots more.</li>
						<li>We encourage the generation of smart ideas by our employees from across teams and levels - the best idea of the month receives an I pad!</li>
					</ul>
				</p>
				
				<p><strong>What Myntra is looking for?</strong></p>
				<p>We are looking for dynamic professionals who want to ride the steep learning curve to accelerated responsibility and success, while enjoying the journey and making it a fun experience for those they work with. If you are excited by what you've read above about us, and if you are an achiever with passion, energy, integrity and a mission to excel, this is the place for you. JOIN US</p>
				<p class="go-to-top"><a href="careers.php#" class="go-to-top-link no-decoration-link">Go to Top</a></p>
				
				<p class="placeholder"><a name="chief-architect"></a></p>
				<h2 class="head3">Designation: Chief Architect</h2>
				<p><div>Responsibilities:</div>
					<ul>
						<li>Architect, design and develop highly scalable web based products/services.</li>
						<li>Ownership and responsibility for over all architecture of the system.</li>
						<li>Mentor software engineers on design, architecture, coding practices and engineering processes.</li>
						<li>Work with cross-functional team, collaborating with multiple sub-teams during development cycle.</li>
						<li>Evangelize best practices across multiple sub-teams.</li>
					</ul>
					<div>Requirements:</div>
					<ul>
						<li>B Tech in Computer Science or equivalent required.</li>
						<li>Minimum 8 years experience. At least 3 years in a highly-scalable web environment</li>
						<li>Hands on Knowledge in LAMP Technologies and Java.</li> 
						<li>Strong knowledge of design patterns and object-oriented models.</li>
						<li>Experience in effectively mentoring junior engineers</li>
						<li>Experience with full life cycle development in PHP, and/or Java on a Linux platform</li>
						<li>Understanding of database-driven Web application design, relational databases, and SQL</li>
						<li>Excellent communication skills.</li>
					</ul>
				</p>
				<p class="go-to-top"><a href="careers.php#" class="go-to-top-link no-decoration-link">Go to Top</a></p>
				<p class="placeholder"><a name="engineering-manager"></a></p>
				<h2 class="head3">Designation: Engineering Manager</h2>
				<p><div>Responsibilities:</div>
					<ul>
						<li>Architect, design and develop highly scalable web based products/services.</li>
						<li>Ownership and responsibility for over all architecture of the system.</li>
						<li>Overall ownership of the quality of the software releases</li>
						<li>Work closely with product management team to ensure we're building the right software system</li>
						<li>Manage a team of 5-10 software engineers, including task planning and code reviews</li>
						<li>Mentor junior engineers on design, architecture, coding practices and engineering processes</li>
					</ul>
					<div>Requirements:</div>
					<ul>
						<li>B Tech/BE in Computer Science or equivalent required</li>
						<li>Minimum 6 years experience. At least 2 years in a highly-scalable web environment</li>
						<li>A solid foundation in computer science, with strong competencies in data structures, algorithms, and problem solving</li> 
						<li>Good knowledge of design patterns and object-oriented models</li>
						<li>Experience with full life cycle development in PHP, and/or Java on a Linux platform</li>
						<li>Experience in Web Technologies (HTML, JavaScript, AJAX etc)</li>
						<li>Understanding of database-driven Web application design, relational databases, and SQL</li>
						<li>Excellent communication skills.</li>
						<li>Proven leadership ability, capable of mentoring and motivating some of the best software engineers</li>
					</ul>
				</p>
				<p class="go-to-top"><a href="careers.php#" class="go-to-top-link no-decoration-link">Go to Top</a></p>
				<p class="placeholder"><a name="deployment-lead"></a></p>
				<h2 class="head3">Designation: NOC/Deployment Lead</h2>
				<p><div>Responsibilities:</div>
					<ul>
						<li>Production hardware, software, network management</li>
						<li>Owning production LAMP libraries and version compatibility between various open source components.</li>
						<li>Handle data centers in US and India, ensuring high site availability.</li>
						<li>Owning the entire website deployment.</li>
						<li>Deployment aligned with release cycles and release management.</li>
						<li>Menotring junior Linux sys admins and NOC persons.</li>
						<li>Long term strategy/plan for production hardware requirements for the company</li>
						<li>Owning CDN(content delivery network)</li>
						<li>Owning all data backup, restore and general data management stuff.</li>
					</ul>
					<div>Qualifications:</div>
					<ul>
						<li>Minimum 5 years experience, ideally in a relevant area. Experience in very large scale web-portals will be preferred</li>
						<li>Strong problem solving skills, and analytical skills</li>
						<li>Very strong understanding of Linux operating system.</li>
						<li>Prior experience of managing production, CDN, Nagios, SVN and network monitoring tools.</li>
						<li>Good understanding of LAMP stack ( Linux, Apache, MySQL and PhP)</li>
						<li>B Tech/BE in Computer Science or equivalent from a reputed college.</li>
					</ul>        
 				</p>
 				<p class="go-to-top"><a href="careers.php#" class="go-to-top-link no-decoration-link">Go to Top</a></p>
 				<p class="placeholder"><a name="technical-lead"></a></p>
				<h2 class="head3">Designation: Technical Lead</h2>
				<p><div>Responsibilities:</div>
					<ul>
						<li>Write maintainable/scalable/efficient code in PHP and Java.</li>
						<li>Responsible for designing and building client and server side for solving business problems of a very large e-commerce portal.</li>
						<li>Perform detailed design for modules that you are responsible for.</li>
						<li>Perform testing for the developed modules to ensure high quality of code.</li>
						<li>Manage a team of 2-3 software engineers, including task planning and code reviews</li>
					</ul>
					<div>Requirements:</div>
					<ul>
						<li>B Tech/BE in Computer Science or equivalent from a reputed college.</li>
						<li>3+ years of experience in building web applications using the LAMP stack/Java.</li>
						<li>A solid foundation in computer science, with strong competencies in data structures, algorithms, and problem solving.</li>
						<li>Excellent programming skills in Java and PHP.</li>
						<li>Experience in Web Technologies (HTML, JavaScript, AJAX, Apache Web Server, Apache Tomcat etc.)</li>
						<li>Strong database concepts and experience in MySQL.</li>
						<li>Excellent communication abilities.</li>
					</ul>        
 				</p>
 				<p class="go-to-top"><a href="careers.php#" class="go-to-top-link no-decoration-link">Go to Top</a></p>
 				<p class="placeholder"><a name="front-end"></a></p>
				<h2 class="head3">Designation: Software Engineer - Front End</h2>
				<p><div>Responsibilities:</div>
					<ul>
						<li>Write maintainable/scalable/efficient and modular code for a very  front end for the portal and alternative channels (mobile etc.)</li>
						<li>Design and develop user interface for business sensitive consumer facing e-commerce portal.</li>
						<li>Work in a cross-functional team, collaborating with peers during entire SDLC.</li>
						<li>Follow coding standards, unit-testing, code reviews etc.</li>
						<li>Follow release cycles and commitment to deadlines.</li>
					</ul>
					<div>Requirements:</div>
					<ul>
						<li>B Tech/BE in Computer Science or equivalent from a reputed college.</li>
						<li>Minimum 2 year experience in developing front-end for large scale web based consumer facing products.</li>
						<li>Excellent programming skills in HTML, CSS, JavaScript, XML, AJAX, Apache, Flash, JQuery and PHP.</li>
						<li>Ability to write code compatible across browsers and other clients.</li>
						<li>Exposure to mobile based interfaces, social media ( facebook, orkut ).</li>
						<li>Good understanding of backend systems i.e. web services, APIs and RDBMS from a consumer perspective.</li>
					</ul>        
 				</p>
 				<p class="go-to-top"><a href="careers.php#" class="go-to-top-link no-decoration-link">Go to Top</a></p>
 				<p class="placeholder"><a name="sse"></a></p>
				<h2 class="head3">Designation: Senior Software Engineer</h2>
				<p><div>Responsibilities:</div>
 					<ul>
						<li>Write maintainable/scalable/efficient code in PHP and Java.</li>
						<li>Design and architect technical solutions for the business problems of a very large e-commerce portal.</li>
						<li>Work in cross-functional team, collaborating with peers during entire SDLC.</li>
						<li>Follow coding standards, unit-testing, code reviews etc.</li>
						<li>Follow release cycles and commitment to deadlines.</li>
					</ul>
					<div>Requirements:</div>
					<ul>
						<li>Minimum 1 year experience, ideally in a relevant area. Ideal candidate would be 2+ years if experience in very large scale web-portal</li>
						<li>Strong problem solving skills, data structures and algorithms.</li>
						<li>Excellent coding skills in Java and PHP</li>
						<li>Very good understanding of Web Technologies(Apache, SEO, Ajax, HTML, Java Script)</li>
						<li>Very good understanding of any RDBMS</li>
						<li>B Tech/BE in Computer Science or equivalent from a reputed college.</li> 
 					</ul>
 				</p>
 				<p class="go-to-top"><a href="careers.php#" class="go-to-top-link no-decoration-link">Go to Top</a></p>
 				<p class="placeholder"><a name="se-testing"></a></p>
				<h2 class="head3">Designation: Software Engineer - Testing</h2>
				<p><div>Job Description:</div>
					<p>As a Software Engineer in Testing, your charter is would be to improve the quality of Myntra software.You will be responsible for designing and implementing testing tools, creating and enforcing good testing practices, and evaluating product quality. You will be part of a dynamic Engineering team working on solving complex problems in the E-commerce and Web 2.0 domains.</p>				
					<div>Responsibilities:</div>
					<ul>
	 					<li>Plan and execute regression and functional test cases of the web-site and all back-end services for Myntra.com.</li>
						<li>Drive test automation and framework development.</li>
						<li>Work with the development and test engineering teams to automate testing.</li>
						<li>Conduct research on emerging technologies.</li>
						<li>Training and mentoring other team members.</li>
					</ul>
					<div>Requirements:</div>	
					<ul>
						<li>Bachelors degree in Computer Science or equivalent.</li>
						<li>3-4 years of test automation experience with a strong focus on testing of web applications.</li>
						<li>Strong manual testing fundamentals.</li>
						<li>Knowledge of Web Concepts, HTML and Javascript.</li>
						<li>Knowledge of database, writing and executing simple SQL queries.</li>
						<li>Good scripting skills in at least one common language (Perl, Python, Ruby, Shell).</li>
						<li>Knowledge of at least one language: PHP, Java</li>
						<li>Strong hands on experience on automation tools like Selenium, Watir, TESTNG or any other open source tools.</li>
					</ul>
				</p>
				<p class="go-to-top"><a href="careers.php#" class="go-to-top-link no-decoration-link">Go to Top</a></p>
				<p class="placeholder"><a name="senior-product-manager-erp"></a></p>
				<h2 class="head3">Designation: Senior Product Manager, Back End/ERP - Bangalore</h2>
				<p>
					<p>Sr. Product Manager for Backend/ERP will have end to end ownership of the roadmap for all features of the Myntra's ERP system which governs all backend fulfilment, warehouse management, logistics, vendor management and purchase processes. The responsibilities will encompass customer analysis, requirement definition, working with the engineering team to plan the release cycles and measuring the customer impact.</p>
					<div>Key Responsibilities:</div>
					<ul>
						<li>Collaborate with the Operations and Finance teams to understand the capacity plan and translate the business goals into key requirements for the Backend system</li>
						<li>Evaluate and implement an ERP system that can be used the base platform which is continually enhanced to suit the needs of Myntra's business</li>
						<li>Monitor and understand the key back-end processes and translate the process understanding into feature enhancements to support the process in most efficient fashion.</li>
						<li>Work with all the business stakeholders to get a sign-off on new features and enhancements</li> 
						<li>Translate the market requirements into detailed wireframes and functional specifications that can be given to engineering team for development</li>
						<li>Work with UI Designers and Usability experts to define the exact UI details for all customer facing feature and be responsible for overall usability of the product</li>
						<li>Work with engineering team to plan out the release schedule, track key milestones and provide required details throughout the product development lifecycle.</li>
						<li>Work with QA team to ensure detailed test plans and test cases have been prepared in accordance with the Product requirements.</li>
						<li>Work with content team to ensure that high quality and easily readable content is developed for all relevant sections of the product</li>
						<li>Coordinate the public launch with the Marketing team to ensure the product is released with high impact on the target audience</li>
						<li>Define and track all key metrics related to the product performance and define product enhancements based on the trends and gap analysis.</li>
					</ul>
					<div>Qualification:</div>
					<ul>
						<li>Total experience of 8-10 years with a mix of engineering background and product management background for a back-end/ERP products</li>
						<li>Deep knowledge of ERP system and understanding of various modules that a trypical ERP supports</li>
						<li>Understanding of the process definition, process lifecycle and adoption of process in an organization. Six Sigma or any other process certification is a plus.</li>
						<li>At least 3-4 years of experience as a Product Manager, having gone through multiple product lifecycle end-to-end.</li>
						<li>Engineering degree from a good college; Having an MBA is a plus</li>
						<li>Deep understanding of the product development lifecycle and track record of working with various departments to manage the product release cycles</li>
						<li>Knowledge of the best practices for Usability, UI standards and the benchmark products as a reference for highly usable consumer products</li>
						<li>Excellent verbal and written communication skills</li>
					</ul>
 				</p>
 				<p class="go-to-top"><a href="careers.php#" class="go-to-top-link no-decoration-link">Go to Top</a></p>
 				<p class="placeholder"><a name="senior-product-manager-bi"></a></p>
				<h2 class="head3">Designation: Senior Product Manager, BI/Analytics - Bangalore</h2>
				<p>
					<p>Sr. Product Manager will have end to end ownership of the roadmap for all features pertaining to the Business Intelligence and Analytical tools in the company. The responsibilities will encompass customer analysis, requirement definition, working with the engineering team to plan the release cycles and measuring the customer impact.</p>
					<div>Key Responsibilities:</div>
					<ul>
						<li>Work with all the departments to understand various Reporting and Analytical requirements for different levels of the organization</li>
						<li>Evaluate and implement a Data warehousing solution with appropriate Data mart, canned Reports, ad-hoc query tool and analytical cubes</li>
						<li>Define the requirements for all the data that needs to be captured for different processes and customer touch points.</li>
						<li>Work with all the business stakeholders to get a sign-off on new features and enhancements</li> 
						<li>Translate the market requirements into detailed wireframes and functional specifications that can be given to engineering team for development</li>
						<li>Work with UI Designers and Usability experts to define the exact UI details for all customer facing feature and be responsible for overall usability of the product</li>
						<li>Work with engineering team to plan out the release schedule, track key milestones and provide required details throughout the product development lifecycle.</li>
						<li>Work with QA team to ensure detailed test plans and test cases have been prepared in accordance with the Product requirements.</li>
						<li>Work with content team to ensure that high quality and easily readable content is developed for all relevant sections of the product</li>
						<li>Coordinate the public launch with the Marketing team to ensure the product is released with high impact on the target audience</li>
						<li>Define and track all key metrics related to the product performance and define product enhancements based on the trends and gap analysis.</li>
					</ul>
					<div>Qualification:</div>
					<ul>
						<li>Total experience of 8-10 years with a mix of engineering background and product management experience for a Business Intelligence/Reporting product</li>
						<li>Deep knowledge of various BI Suites such a Business Objects, Cognos, Jasper, Pentaho etc.</li>
						<li>Experience in delivering a rich Reporting solution comprising of business relevant Reports and configurability to add new Reports.</li>
						<li>At least 3-4 years of experience as a Product Manager, having gone through multiple product lifecycle end-to-end.</li>
						<li>Engineering degree from a good college; Having an MBA is a plus</li>
						<li>Deep understanding of the product development lifecycle and track record of working with various departments to manage the product release cycles</li>
						<li>Knowledge of the best practices for Usability, UI standards and the benchmark products as a reference for highly usable consumer products</li>
						<li>Excellent verbal and written communication skills</li>
					</ul>
 				</p>
 				<p class="go-to-top"><a href="careers.php#" class="go-to-top-link no-decoration-link">Go to Top</a></p>
 				<p class="placeholder"><a name="senior-ux-designer"></a></p>
				<h2 class="head3">Designation: Senior UX Designer - Bangalore</h2>
 				<p><div>Responsibilities:</div>
	 				<ul> 
						<li>Analyze user interface problems and create on-brand design solutions that meet measurable business goals and requirements.</li>
						<li>Create user-centered designs by considering market analysis, customer feedback, and usability findings.</li>
						<li>Use business requirements and market research to assist in developing use cases and high-level requirements.</li>
						<li>Design the UI, architecture, and interaction flow for the entire site.</li>
						<li>Quickly yet thoroughly create process flows, wireframes, and visual design mockups as needed to effectively conceptualize and communicate detailed interaction behaviors.</li>
						<li>Develop and maintain detailed user-interface specifications.</li>
						<li>Present design work to the product/engineering team and Myntra.com executives for review and feedback.</li>
						<li>Provide leadership in the area of ecommerce trends and user interface innovations.</li>
						<li>Have an excellent eye for detail and provide visual direction for the Myntra.com site</li> 
					</ul>
					<div>The ideal candidate will have:</div>
					<ul>
						<li>A Design/Computer Science degree from a top-tier college and more than 5 years work experience.</li>
						<li>An excellent online portfolio of their work which displays their sense of aesthetics and experience in creating user-centered design solutions.</li>
						<li>Experience working in a collaborative team and working directly with Product managers/developers for implementation of designs.</li>
						<li>Excellent communication, presentation, and interpersonal skills.</li>
						<li>Experience designing for the web and web based devices.</li>
						<li>Experience with a variety of design tools such as Photoshop, Illustrator, Fireworks, Visio, and/or Dreamweaver.</li> 
						<li>Ability to prototype in HTML, JavaScript, and CSS, or Flash.</li>
					</ul>
					<div>What we offer:</div>
					<ul>
						<li>You will be surrounded by seriously smart people who are focused on solving the most complex ecommerce problems you will probably ever encounter. Highly technical managers who encourage innovation.</li>
						<li>Unless you get bored creating great experiences, you will have plenty to keep you challenged.</li>
						<li>Uncommon stability and high customer visibility.</li>
						<li>We provide compensation and benefits that are commensurate with the exceptional quality of candidates sought.</li>
	 				</ul>
	 			</p>
	 			<p class="go-to-top"><a href="careers.php#" class="go-to-top-link no-decoration-link">Go to Top</a></p>
	 			<p class="placeholder"><a name="business-analyst"></a></p>
				<h2 class="head3">Designation: Business Analyst - Online</h2>
	 			<p>
	 				<p>Currently we require a sharp business analyst in online team who can take ownership of improving complex business areas, highlight problem areas and provide suggestions to business head. This will entail deep diving into data to analyze trends and providing insights to support decision making. This person will own certain areas of online business and make them a profitable/viable for Myntra.</p>
					<div>Roles & Responsibilities:</div>
					<ul>
						<li>Analyze/monitor large and complex online business areas and propose solutions for improving and making those areas more efficient to get better ROI from that.</li>  
						<li>Provide recommendations in form of clear and detailed requirements to Product Management</li>
						<li>Take ownership of the business areas/problems and make it work in favor of Myntra</li>
					</ul>
					<div>Requirements:</div> 
					<ul>
						<li>Experience level: 3-5 Years</li>
						<li>Must have very strong analytical skills and should be process oriented</li>
						<li>Should have MBA or B.Tech. degree from tier 1 institute</li>
						<li>Prior experience in e-commerce business is added advantage</li>
						<li>Must have very good handle on spreadsheet and presentation software</li>  
						<li>Must have excellent communication and presentation skills</li> 
						<li>Should be aggressive and willing to work in a startup</li> 
					</ul>
				</p>
				<p class="go-to-top"><a href="careers.php#" class="go-to-top-link no-decoration-link">Go to Top</a></p>
				<p class="placeholder"><a name="channel-manager"></a></p>
				<h2 class="head3">Designation: Channel Manager Email Marketing</h2>
				<p>	
					<div>Roles & Responsibilities:</div>
					<ul>
						<li>Responsible for revenue ,no of transactions from different product categories from Email marketing campaigns</li>
						<li>Plan ,execute and measure effectiveness of all the email marketing campaigns</li>
						<li>Ensure high deliverability and implement best practices of email marketing</li> 
						<li>Coordinate/negotiate/build relationships with third parties to get new databases</li> 
						<li>Do profiling of the databases and run targeted campaigns to drive maximum ROI</li> 
					</ul>
					<div>Requirements:</div>
 					<ul>
						<li>Must have 2+ yrs of experience in the field of email-marketing in reputed online company</li>
						<li>Should have MBA or BTech degree from a reputed institute.</li>
						<li>Good understanding of online consumer behavior and online business</li>
						<li>Aggressive and willing to work in a startup</li> 
						<li>Should be able handle target based goals</li> 
						<li>Should have strong analytical skills</li>
	 				</ul>
 				</p>
 				<p class="go-to-top"><a href="careers.php#" class="go-to-top-link no-decoration-link">Go to Top</a></p>
 				<p class="placeholder"><a name="portal-manager"></a></p>
				<h2 class="head3">Designation: Portal Manager</h2>
 				<p><div>Roles & Responsibilities:</div>
					<ul>				
						<li>Responsible for conversion and revenue from direct traffic.</li>
						<li>Monitor/Analyze revenue from all real estate properties on myntra.com on daily basis.</li> 
						<li>Devise and implement strategies for maximizing revenue from each property based on past consumer data (sales pattern, click pattern) and trends.</li>
						<li>Implement upsell,cross sell strategies and monitor/optimize on continuous basis.</li>
					</ul>
					<div>Requirements:</div>
 					<ul>
						<li>Must have atleast 2-3 yrs of experience in sales marketing or product management role in an online internet business.</li> 
						<li>Must have strong analytical skills.</li>
						<li>Must be expert in tools like google analytics/omniture etc.</li>
						<li>Must have MBA or BTech degree from a reputed institute (IIT/IIM/REC graduates are preferred).</li>
						<li>Knowledge of UI/Usability is added advantage.</li>
						<li>Aggressive and willing to work in a startup</li> 
						<li>Good communication skills</li>
 					</ul>
 				</p>
 				<p class="go-to-top"><a href="careers.php#" class="go-to-top-link no-decoration-link">Go to Top</a></p>
 				<p class="placeholder"><a name="category-manager"></a></p>
				<h2 class="head3">Designation: Category Manager - Bangalore</h2>
 				<p><div>Roles & Responsibilities:</div>
 					<ul>
						<li>Sourcing, negotiating and buying branded apparel for sale at the portal</li>
						<li>Managing relationships with multiple t-shirt brands</li>
						<li>Managing roll-out of new products on the website</li>
						<li>Tracking sales targets, inventory, margins etc on a regular basis</li>
						<li>Analysing performance of t-shirt styles/designs</li> 
						<li>Market research and store visits to identify popular brands/ design themes</li>
						<li>Creating product catalogue for distribution across retail and POS partners</li>
						<li>Planning and executing category promotions</li>
					</ul>
					<div>Requirements:</div>
					<ul>
						<li>Graduate/PG with prior experience in managing some apparel category</li>
						<li>Experience: 3-6 Years</li>
						<li>Excellent relationship management skills</li>
						<li>Aggressive and willing to work in a startup</li> 
						<li>Should be able handle target based goals</li> 
						<li>Strong analytical skills</li>
						<li>Strong communication skills</li>
					</ul>
				</p>
				<p class="go-to-top"><a href="careers.php#" class="go-to-top-link no-decoration-link">Go to Top</a></p>
				<p class="placeholder"><a name="content-writer"></a></p>
				<h2 class="head3">Designation: Content Writer/Manager</h2>
				<p><div>Roles & Responsibilities:</div>
					<ul>
						<li>Develop/proof read/edit Myntra website content keeping SEO as key focus and clear understanding of target audience with right tone of voice.</li> 
						<li>Develop highly effective, original and targeted marketing copy for the promotion and sale of Myntra products online.</li> 
						<li>Work with various departments of Myntra to generate collaterals for promotions, direct marketing (email, newsletter), advertisements as well as website content updates.</li> 
						<li>Experience & Skills Required</li> 
						<li>over the English language. Expert writing skills, editing and proof reading abilities.</li> 
						<li>Ability to write terse, attention grabbing and professional copy that puts the message across.</li> 
						<li>Creative bend of mind with strong visualization and idea generation skills.</li>   
						<li>Exposure to writing print promotional materials, online web content, press releases, copy to assist advertising and marketing efforts, etc.</li> 
						<li>Ability deliver within tight deadlines</li>
					</ul> 
					<div>Requirements:</div>
					<ul>
						<li>2-4 years of copywriting experience within Internet Marketing and SEO realm for a major brand.</li>
						<li>Experience: 3-6 Years</li>
						<li>Excellent relationship management skills</li>
						<li>Aggressive and willing to work in a startup</li> 
					</ul> 				
 				</p>
 				<p class="go-to-top"><a href="careers.php#" class="go-to-top-link no-decoration-link">Go to Top</a></p>
 				<p class="placeholder"><a name="avp-operations"></a></p>
				<h2 class="head3">Designation: AVP - Operations - Bangalore</h2>
 				 <p><div>Key Responsibilities:</div>
					<ul>
						<li>Inventory planning and guaranteeing orders are efficiently processed is the focus of a fulfillment manager's job.</li> 
						<li>Constant interaction with vendors guarantees a fluid, up-to-date inventory of goods resulting in a high level of customer satisfaction.</li> 
						<li>The manager is also required to constantly monitor warehouse processes and recommend improvements to increase productivity and profits.</li> 
						<li>Regular communications with marketing managers are necessary to forecast market trends, along with contract with enough vendors so that customer demands will be met without interruption.</li> 
						<li>A fulfillment manager is also required to regularly prepare budgets for review and approval by upper management.</li>
					</ul>
					<div>Requirement:</div>
					<ul>	
						<li>Exp - 8 to 10 yrs</li>
						<li>Education - MBA / IIT (Ops Mgt background)</li>
						<li>Industry - Retail / Mfg etc</li>
					</ul>
				</p>
				<p class="go-to-top"><a href="careers.php#" class="go-to-top-link no-decoration-link">Go to Top</a></p>			
		</div>
		<div class="content-right right black-bg5 corners last">
			<ul class="links mt10 mb10">
                    <li><strong>Send your resume to</strong></li>
                    <li><a class="no-decoration-link" href="mailto:jobs@myntra.com">jobs@myntra.com</a></li>
			        <li><strong>Open Positions</strong></li>
					<li><a class="no-decoration-link" href="careers.php#chief-architect">Chief Architect</a></li>
					<li><a class="no-decoration-link" href="careers.php#engineering-manager">Engineering Manager</a></li>
					<li><a class="no-decoration-link" href="careers.php#deployment-lead">NOC/Deployment Lead</a></li>
					<li><a class="no-decoration-link" href="careers.php#technical-lead">Technical Lead</a></li>
					<li><a class="no-decoration-link" href="careers.php#front-end">Software Engineer - Front End</a></li>
					<li><a class="no-decoration-link" href="careers.php#sse">Senior Software Engineer</a></li>
					<li><a class="no-decoration-link" href="careers.php#se-testing">Software Engineer - Testing</a></li>
					<li><a class="no-decoration-link" href="careers.php#senior-product-manager-erp">Senior Product Manager, Back End/ERP</a></li>
					<li><a class="no-decoration-link" href="careers.php#senior-product-manager-bi">Senior Product Manager, BI/Analytics</a></li>
					<li><a class="no-decoration-link" href="careers.php#senior-ux-designer">Senior UX Designer</a></li>
					<li><a class="no-decoration-link" href="careers.php#business-analyst">Business Analyst - Online</a></li>
					<li><a class="no-decoration-link" href="careers.php#channel-manager">Channel Manager Email Marketing</a></li>
					<li><a class="no-decoration-link" href="careers.php#portal-manager">Portal Manager</a></li>
					<li><a class="no-decoration-link" href="careers.php#category-manager">Category Manager</a></li>
					<li><a class="no-decoration-link" href="careers.php#content-writer">Content Writer/Manager</a></li>
					<li><a class="no-decoration-link" href="careers.php#avp-operations">AVP - Operations</a></li>
	        </ul>
		</div>
    </div>    

</div>
	<div class="divider">&nbsp;</div>
	{include file="site/footer.tpl"}
</body>
</html>