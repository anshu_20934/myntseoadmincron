{literal}
<script language="javascript">
  	function sortProducts(id,url,pg,httplocation)
	{
		
		    var urlLoc = httplocation +"/"+url +"/"+id+"/"+pg;
			window.location = urlLoc;
	}
</script>
{/literal}


<div class="form">
          <div class="subhead">
            <div style="height:25px; margin: 0 0 0 15px;"> {$heading}</div>
{if $accountType ne 1}            <div style='float:right;padding-right:30px;margin-top:-23px;'><font style="font-size:0.7em;"><span class="mandatory" ><b>FREE Shipping in India</b></span></font></div>{/if}

          </div>

	  {if $allproducts != 'empty'}

	   <div class="links">
            <div class="navigator">
              <div class="left">

			{assign var=sortparam1 value=USERRATING}
			{assign var=sortparam2 value=BESTSELLING}
			{assign var=sortparam3 value=MOSTRECENT}
			{assign var=sortparam4 value=EXPERTRATING}
		<p>
		  Sort By :
				{if $sortMethod eq MOSTRECENT}
				<a rel="nofollow" href="{$http_location}/{$sorturl}/{$sortparam4}/1">Expert Rating</a>&nbsp;|&nbsp;<a rel="nofollow" href="{$http_location}/{$sorturl}/{$sortparam1}/1">User Rating</a>&nbsp;|&nbsp;<a rel="nofollow" href="{$http_location}/{$sorturl}/{$sortparam2}/1">Best Selling</a>&nbsp;|&nbsp;Just Arrived
				{/if}
				{if $sortMethod eq BESTSELLING}
				<a rel="nofollow" href="{$http_location}/{$sorturl}/{$sortparam4}/1">Expert Rating</a>&nbsp;|&nbsp;<a rel="nofollow" href="{$http_location}/{$sorturl}/{$sortparam2}/1">User Rating</a>&nbsp;|&nbsp;Best Selling&nbsp;|&nbsp;<a rel="nofollow" href="{$http_location}/{$sorturl}/{$sortparam3}/1">Just Arrived</a>
				{/if}
				{if $sortMethod eq USERRATING}
				<a rel="nofollow" href="{$http_location}/{$sorturl}/{$sortparam4}/1">Expert Rating</a>&nbsp;|&nbsp;User Rating&nbsp;|&nbsp;<a rel="nofollow" href="{$http_location}/{$sorturl}/{$sortparam2}/1">Best Selling</a>&nbsp;|&nbsp;<a rel="nofollow" href="{$http_location}/{$sorturl}/{$sortparam3}/1">Just Arrived</a>
				{/if}
				{if $sortMethod eq EXPERTRATING}
				Expert Rating&nbsp;|&nbsp;<a rel="nofollow" href="{$http_location}/{$sorturl}/{$sortparam1}/1">User Rating</a>&nbsp;|&nbsp;<a rel="nofollow" href="{$http_location}/{$sorturl}/{$sortparam2}/1">Best Selling</a>&nbsp;|&nbsp;<a rel="nofollow" href="{$http_location}/{$sorturl}/{$sortparam3}/1">Just Arrived</a>
				{/if}
		 </p>

              </div>
              <div class="right" >

               <p>

                {if $totalpages > 1}{$paginator}{/if}
			</p>
                <!--this is a number bar that will be used universally-->

              </div>
              <div class="clearall"></div>
              <div style="width:auto;height:auto;font-size:1.2em;font-weight:bold;padding-right:5px;float:right;text-decoration:underline;text-transform:lowercase;"><a href="{$http_location}/create/{$selectedproductypename}/{$selectedproductypid}">Create your own {$selectedproductypename}</a></div>
            </div>

          </div>
     {else}
			<div class="links">
				<div class="navigator">
					<div class="left"></div>
					<div class="right" ></div>
					<div class="clearall"></div>
					<div style="width:auto;height:auto;font-size:1.2em;font-weight:bold;padding-right:5px;float:right;text-decoration:underline;text-transform:lowercase;"><a href="{$http_location}/create/{$productlabel}/{$selectedproductypid}">Create your own {$selectedproductypename}</a></div>
				</div>
			</div>
     {/if}


