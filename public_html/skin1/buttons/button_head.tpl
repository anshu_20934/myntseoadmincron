{if $type eq "input"}{assign var="img_type" value='input type="image"'}{else}{assign var="img_type" value="img"}{/if}
{assign var="js_link" value=$href|regex_replace:"/^\s*javascript\s*:/Si":""}
{if $js_link eq $href}{assign var="js_link" value="javascript: self.location='`$href`'"}
{else}{assign var="js_link" value=$href}{if $js_to_href ne 'Y'}{assign var="onclick" value=$href}{assign var="href" value="javascript: void(0);"}{/if}{/if}
{if $style eq "button" and $webmaster_mode ne "editor"}
<table cellspacing="0" cellpadding="0" onclick="{$js_link}" class="ButtonTable"{if $title ne ''} title="{$title}"{/if}>
<tr>
	<td><{$img_type} src="{$ImagesDir}/custom/but_head1.gif" class="ButtonHeadSide1" alt="{if $title ne ''}{$title}{/if}" /></td>
	<td class="ButtonHead" valign="middle" nowrap="nowrap">&nbsp;{$button_title}&nbsp;</td>
	<td><img src="{$cdn_base}/skin1/images/custom/but_head2.gif" class="ButtonHeadSide2" alt="{if $title ne ''}{$title}{/if}" /></td></tr>
</table>
{else}
<a class="Button2" href="{$href}"{if $onclick ne ''} onclick="{$onclick}"{/if}{if $title ne ''} title="{$title}"{/if}{if $target ne ''} target="{$target}"{/if}>{$button_title} <{$img_type} src="{if $full_url}{$http_host}{$ImagesDir|replace:"..":""}{else}{$ImagesDir}{/if}/custom/auth_go.gif" width="18" height="16" align="middle" /></a>
{/if}
