<div class="subhead"></div>
          <div> 
            <table cellpadding="2" class="table">
              <tr> 
                <td class="align_left">Date:</td>
                <td class="align_left">{$today}</td>
                <td  class="align_left"  colspan="2" >Myntra</td>

              </tr>
              <tr> 
                <td  class="align_left">Invoice number:</td>
                <td class="align_left">{$invoiceid}</td>
		 <td  class="align_left">Order number:</td>
                <td class="align_left">{$orderid}</td>
               
              </tr>
	 
              <tr> 
                <td   class="align_left">Order Status:</td>
		{if $paybyoption=="PD"}
		<td class="align_left">Pending check collection</td>
		{elseif $paybyoption=="PP"}
		<td class="align_left">Pending payment collection</td>
		{elseif $paybyoption=="CD"}
		<td class="align_left">COD Verification</td>
		{else}
                <td class="align_left">Queued</td>
		{/if}

		 <td colspan="2"  class="align_left">{$address},<br>{$cityname}, {$statename}<br>
					
                    {$countryname} PINCODE-{$pincode}</td>
              </tr>

              <tr> 
                 <td  class="align_left">Mobile No:</td>
                <td class="align_left">{$mobile}</td>
		<td  class="align_left">Phone No:</td>
                <td class="align_left">{$phone}</td>
              </tr>

              <tr> 
                <td  class="align_left">Delivery Method:</td>
                <td class="align_left">{$deliverymethod}</td>
                <td class="align_left">Email:</td>
                <td class="align_left">{$email}</td>
              </tr>
            </table>

          </div>
          <div class="foot"></div>
