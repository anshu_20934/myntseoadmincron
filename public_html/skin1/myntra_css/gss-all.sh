#!/bin/bash

for i in *.gss
do
    fname=${i%.gss}
    [ "$fname" == "include" ] && continue
    echo -n "Processing $i ... "
    java -jar ../../utils/closure-stylesheets.jar --pretty-print include.gss $fname.gss > $fname.css
    [ $? -ne 0 ] && exit 1
    echo "OK"
done
