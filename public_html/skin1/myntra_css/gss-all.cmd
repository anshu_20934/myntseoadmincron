@echo off

for %%G in (*.gss) do (
    if "%%~nG" NEQ "include" (
        echo Processing %%G ... 
        java -jar ../../utils/closure-stylesheets.jar --pretty-print include.gss %%~nG.gss > %%~nG.css
        if not errorlevel 0 goto end
    )
)

:end
