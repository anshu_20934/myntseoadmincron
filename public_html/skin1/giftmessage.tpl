{literal}
<script language="javascript">

function messagBlock(login)
{
	var giftpack_charges = document.getElementById('giftpack_charges').value;
	var amount = document.getElementById('Amount').value;
   if(document.getElementById('asgift').checked == true)
   {
	document.getElementById('messageblock').style.visibility = 'visible'; 
	document.getElementById('messageblock').style.display = "";

	if(amount != "0.00")
	{
		var totalamount = parseFloat(amount) + parseFloat(giftpack_charges);
		document.getElementById('Amount').value = totalamount;
	}
	else
	{
		var totalamount = amount;
	}
	document.getElementById('giftamount').value = giftpack_charges;
	document.getElementById('giftprice').innerHTML = giftpack_charges;

	if(document.getElementById("isexistref").value == 1)
	{
		if(document.getElementById("refcheck").checked == true) 
		{
			var refamount = document.getElementById('refamt').value;
			var refdiscount = document.getElementById('hideref').value;
			if(refamount != "0.00")
			{
				var refamount = parseFloat(refamount) - parseFloat(giftpack_charges);
				var refdiscount = parseFloat(refdiscount) + parseFloat(giftpack_charges);
			}
			document.getElementById('refamt').value = refamount;
			document.getElementById('hideref').value = refdiscount;
			
			document.getElementById("refamtdiv").innerHTML=" Rs. "+numberformat(refamount,2);
			document.getElementById("refdiscount").innerHTML=" Rs. "+numberformat(refdiscount,2);
		}
		document.getElementById('totalamtafterdeduction').innerHTML = "Rs."+numberformat(totalamount,2);
	}
	if(document.getElementById("isexistref").value == 0)
	{
		document.getElementById('totalamtaftergiftpack').innerHTML = "Rs."+numberformat(totalamount,2);
	}
	
	var chkSumKey =  document.getElementById('checksumkey').value;
	updateCheckSum(document.getElementById("Amount").value,chkSumKey);
   }
   else
   {
	document.getElementById('messageblock').style.visibility = 'hidden'; 
	document.getElementById('messageblock').style.display = "none";

	if(document.getElementById("isexistref").value == 1)
	{
		if(document.getElementById("refcheck").checked == true) 
		{
			var refamount = document.getElementById('refamt').value;
			var refdiscount = document.getElementById('hideref').value;
			if(refamount != "0.00")
			{
				var refamount = parseFloat(refamount) + parseFloat(giftpack_charges);
				var refdiscount = parseFloat(refdiscount) - parseFloat(giftpack_charges);
			}
			
			document.getElementById('refamt').value = refamount;
			document.getElementById('hideref').value = refdiscount;
			
			document.getElementById("refamtdiv").innerHTML=" Rs. "+numberformat(refamount,2);
			document.getElementById("refdiscount").innerHTML=" Rs. "+numberformat(refdiscount,2);
		}
	}
	
	if(amount != "0.00"){
		var totalamount =parseFloat(amount) - parseFloat(giftpack_charges);
	}
	else{
		var totalamount = amount;
	}

	
	document.getElementById('Amount').value = numberformat(totalamount,2);
	document.getElementById('giftamount').value = "0.00";
	document.getElementById('giftprice').innerHTML = "Rs.0.00";

	if(document.getElementById("isexistref").value == 1)
	{
		document.getElementById('totalamtafterdeduction').innerHTML = "Rs."+numberformat(totalamount,2);
	}
	if(document.getElementById("isexistref").value == 0)
	{
		document.getElementById('totalamtaftergiftpack').innerHTML = "Rs."+numberformat(totalamount,2);
	}

	var chkSumKey =  document.getElementById('checksumkey').value;
	updateCheckSum(document.getElementById("Amount").value,chkSumKey);
   }
}

/* COD functionality*/

function cashOnDelivery(cod)
{
	var amount = document.getElementById('Amount').value;
	var paybyoption = document.getElementsByName("payby");
	var hiddenoption = document.getElementById('optioncount').value;
	var optioncount = 0;

	for(var i =0;  i<paybyoption.length; i++)
	{
		if((paybyoption[i].checked == true) && (paybyoption[i].value == 'cod'))
		{
			optioncount = optioncount+1;
			
		}
	}

	if(optioncount == 1)
	{
		document.getElementById('codoptioncount').value = 1;
		if(hiddenoption == 1)
		document.getElementById('optioncount').value = 0;
		document.getElementById('cod').style.visibility = 'visible'; 
		document.getElementById('cod').style.display = "";

		if(amount != "0.00")
		{
			var totalamount = parseFloat(amount) + parseFloat(cod);
			document.getElementById('Amount').value = totalamount;
		}
		else
		{
			var totalamount = amount;
		}

		if(document.getElementById("isexistref").value == 1)
		{
			if(document.getElementById("refcheck").checked == true) 
			{
				var refamount = document.getElementById('refamt').value;
				var refdiscount = document.getElementById('hideref').value;
				if(refamount != "0.00")
				{
					var refamount = parseFloat(refamount) - parseFloat(cod);
					var refdiscount = parseFloat(refdiscount) + parseFloat(cod);
				}
				document.getElementById('refamt').value = refamount;
				document.getElementById('hideref').value = refdiscount;
				
				document.getElementById("refamtdiv").innerHTML=" Rs. "+numberformat(refamount,2);
				document.getElementById("refdiscount").innerHTML=" Rs. "+numberformat(refdiscount,2);
			}
			document.getElementById('totalamtafterdeduction').innerHTML = "Rs."+numberformat(totalamount,2);
		}
		if(document.getElementById("isexistref").value == 0)
		{
			document.getElementById('totalamtaftergiftpack').innerHTML = "Rs."+numberformat(totalamount,2);
		}
		

		var chkSumKey =  document.getElementById('checksumkey').value;
		updateCheckSum(document.getElementById("Amount").value,chkSumKey);
	}
	else if((hiddenoption == 0) && (document.getElementById('codoptioncount').value == 1))
	{
		document.getElementById('optioncount').value = 1; 
		document.getElementById('codoptioncount').value = 0; 
		document.getElementById('cod').style.visibility = 'hidden'; 
		document.getElementById('cod').style.display = "none";

		if(amount != "0.00")
		{
			var totalamount =parseFloat(amount) - parseFloat(cod);
			document.getElementById('Amount').value = totalamount;
		}
		else
		{
			var totalamount = amount;
		}

		if(document.getElementById("isexistref").value == 1)
		{
			if(document.getElementById("refcheck").checked == true) 
			{
				var refamount = document.getElementById('refamt').value;
				var refdiscount = document.getElementById('hideref').value;
				if(refamount != "0.00")
				{
					var refamount = parseFloat(refamount) + parseFloat(cod);
					var refdiscount = parseFloat(refdiscount) - parseFloat(cod);
				}
				
				document.getElementById('refamt').value = refamount;
				document.getElementById('hideref').value = refdiscount;
				
				document.getElementById("refamtdiv").innerHTML=" Rs. "+numberformat(refamount,2);
				document.getElementById("refdiscount").innerHTML=" Rs. "+numberformat(refdiscount,2);
			}
			document.getElementById('totalamtafterdeduction').innerHTML = "Rs."+numberformat(totalamount,2);
		}
		if(document.getElementById("isexistref").value == 0)
		{
			document.getElementById('totalamtaftergiftpack').innerHTML = "Rs."+numberformat(totalamount,2);
		}

		var chkSumKey =  document.getElementById('checksumkey').value;
		updateCheckSum(document.getElementById("Amount").value,chkSumKey);
	}
}


/*###########END COD############*/

var req = null;
function updateCheckSum(amt,chkSumKey)
{
    if(window.XMLHttpRequest){ 
        req = new XMLHttpRequest(); 
    } 
    else if (window.ActiveXObject){ 
        req  = new ActiveXObject("Microsoft.XMLHTTP"); 
    } 

    var formurl="mkupdatechecksum.php";
     
    req.onreadystatechange = function(){
        if (req.readyState == 4)
	    {
	       if(req.responseText == 'S')
	       {
	         //alert(req.responseText)
	        
	       }
	       else if(req.responseText == 'F')
			{
	          //alert(req.responseText)
			}
	
        } 
    };
    req.open('GET',formurl+"?amt="+amt+"&chksumkey="+chkSumKey, true);
    req.setRequestHeader("Content-Type", "text/xml");
    req.send(null);
}

function messagePopup(type)
{
	if(type == "gift")
	{
		document.getElementById('giftpopup').style.visibility = 'visible'; 
		document.getElementById('giftpopup').style.display = "";
	}
	if(type == "payment")
	{
		document.getElementById('paymentoption').style.visibility = 'visible'; 
		document.getElementById('paymentoption').style.display = "";
	}
}
function hidegiftMessageDiv()
{
	document.getElementById('giftpopup').style.visibility = 'hidden';
	document.getElementById('giftpopup').style.display = 'none';
}
function hidepaymentMessageDiv()
{
	document.getElementById('paymentoption').style.visibility = 'hidden';
	document.getElementById('paymentoption').style.display = 'none';
}
</script>
{/literal}