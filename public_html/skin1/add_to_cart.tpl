{include file="codlearnmore.tpl" }

 <div class="product-info">
 <form action="mkretrievedataforcart.php?pagetype=createproduct" method="post" id="cartform" >
<div class="quantity-block">
                        <div class="w-box">
                            <div class="quantity">
                                <div>


                                <ul>
                                        <li><strong>Quantity</strong></li>
                    {assign var='i' value=1}
                    {if $productOptions}
                    {section name=options loop=$productOptions}
                        {section name=values loop=$productOptions[options][1]}
                            <li>
                                {assign var='j' value=$i-1}
                                {$productOptions[options][1][values][0]} 
                                <select class="tab{$j}" name="sizequantity[]" id="sizequantity{$i}" onclick="_gaq.push(['_trackEvent', 'personalize_pdp_{$producttypelabel}', 'select_size','size_{$productOptions[options][1][values][0]}']);">
                                    <option value="0">0</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                </select>
                                <input type="hidden" name="sizename[]" id="sizename{$i}" value="{$productOptions[options][1][values][0]}">
                            </li>
                            {assign var='i' value=$i+1}
                        {/section}
                    {/section}
                        <li style="display:none; list-style:none"><input type="hidden" id="count" value="{$i}"></li>
                    {else}
                        <input type="hidden" id="count" value="2">
                        <li>
                                <select id="sizequantity1">
                                    <option value="0">0</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                </select>
                        </li>
                    {/if}
                </ul>
                                {if $thisTypeGroup eq 1}
<p><span><a href="{$http_location}/images/style/sizechart/{$size_chart_image_path}" id="sizechart" class="thickbox" onClick="_gaq.push(['_trackEvent', 'personalize_pdp_{$producttypelabel}', 'size_chart']);return true;">view size chart</a></span></p>
{/if}
                                            </div>    
                            </div>
                            <div class="clear">&nbsp;</div>
                              <div class="size-chart-tooltip"  style="position:absolute">
                                {include file='product_size_chart.tpl'}                  
                              </div>
                        </div>
                    </div>
    
            <input type="hidden" name="quantity" id="quantity" value="0">
            <input type="hidden" name="windowid" value="{$windowid}">

            <!--  The codsetting hidden parameter is passed, for handling directly reaching to
                                      checkout page, when Pay Cash on Delivery Button is clicked. -->
                                <!--  javascript function setcodsetting() handles changing this value -->
            <input type="hidden"  id="codsetting" name="codoption" value="no" >
            <div class="buy-block">
                <div class="pro-price"><span style="display: block; margin-bottom: 5px;">Rs.{$stylePrice}</span><p>
                {if ($stylePrice + $cart_amount) gt $free_shipping_amount}
                        FREE Shipping in India!<br/>
                        Cash on Delivery Available <a style="font-size:10" href="JavaScript:showLearnMore();_gaq.push(['_trackEvent', 'sports_pdp_{$producttypelabel}_{$brandname}', 'cod_more_info']);" class="learnmore">Learn More</a>
                {/if}
                </p></div>
                <div class="clear">&nbsp;</div>
            </div>
            <div id ="codaddtocartbtn" class="cartbtn">
                <div>
                    <span>
                        <a onClick="setcodsetting('no');return preCheckAddtoCart();" style="padding:0 35px;cursor:pointer">Buy Now <span style="font-size:30px;">&raquo;</span></a>
                    </span>
                </div>
        </div>
</form>
</div>

{literal}
    <script type="text/javascript">
        function setcodsetting(codvalue) {
            $('#codsetting').val(codvalue);
        }
        
        function preCheckAddtoCart()
        {
            var cstatus = document.getElementById("cstatus").value;
            if( cstatus == "0")
            {
                //document.getElementById("errormessage").innerHTML = 'Click on "Customize" tab to add text/photo of your choice to the product.';
                alert('Click on "Customize" tab to add text/photo of your choice to the product.');
                return false;
            }
            var quantity = document.getElementById("quantity").value;
            if( quantity == 0 )
            {
                //document.getElementById("errormessage").innerHTML = "Please enter valid quantity";
                alert("Please enter valid quantity");
                return false;
            }
            {/literal}
            _gaq.push(['_trackEvent', 'personalize_pdp_{$producttypelabel}', 'buy_now', '{$isreturninguser}']);
            {literal}
            document.getElementById("cartform").submit();
        }
        function updateTotalQuantity()
        {
        }
    </script>
{/literal}
{literal}
    <script type="text/javascript">
		
        $(document).ready(function(){
            $(".size-chart-content").hide();
            $('select').focus(function(){
                $(this).addClass("focusField");
                 //alert('hi');
                if (this.value == this.defaultValue){
                        this.value = '';
                }
                /*if(this.value != this.defaultValue){
                    this.select();
                }*/
            });

            $('.quantity select').blur(function() {
                $(".size-chart-content-block").hide();
                    $(this).removeClass("focusField").addClass("idleField");
                    if ($.trim(this.value) == ''){
                        $(this).removeClass("focusFieldcolor").addClass("idleField");
                            this.value = (this.defaultValue ? this.defaultValue : '');
                    }
                    if(this.value != this.defaultValue){
                        $(this).removeClass("idleField").addClass("focusFieldcolor");
                    }
//                updateTotalQuantity(); updating quantity field here itself because there was conflict with jquery onblur event and js onchange event
                var quantity = 0;
                var count = $('#count').val();
                for(var i=1; i<count; i++){
                    var id = 'sizequantity'+i;
                    var value = parseInt($("#"+id).val());
                    if( isNaN(value) )
                    {
                        $("#"+id).value = "";
                        value = 0;
                    }
                    quantity = quantity + value;
                }
                $("#quantity").val(quantity);
                });

/*
            $('.quantity select[type="text"]').keypress(function (e){
              //if the letter is not digit then don't type anything
              if( e.which!=8 && e.which!=0 && (e.which<48 || e.which>57)){
                return false;
              }
            });

*/

            $(".quantity ul li select").focus(function(){
                var offset = $(this).offset();
                var optleft=offset.left;
                // alert(optleft);
                $(".size-chart-tooltip").css("left",optleft);
                $(".size-chart-content-block").hide();
                $(".size-chart-content").hide();
                var id=$(this).attr("class");
                var pos=id.indexOf(" ");
                if(pos)
                {
                    id=id.substr(0,pos);
                }
                $("#"+id).fadeIn().show();
                $(".size-chart-content-block").fadeIn("fast").show();
            });
        });
    </script>
    {/literal}
