<?php
include_once "$xcart_dir/modules/loyaltyPoints/LoyaltyPointsPortalClient.php";
include_once "$xcart_dir/modules/loyaltyPoints/LoyaltyPointsPortalHelper.php";


function getTierLabel($tierNum){
	$tierLabel = 'Silver';
	if($tierNum == 1){
		$tierLabel = 'Silver';
	}elseif($tierNum == 2){
		$tierLabel = 'Gold';
	}elseif($tierNum == 3){
		$tierLabel = 'Platinum';
	}
	return $tierLabel;
}
//Configurable parameters starts
$tierInfoForTnc = LoyaltyPointsPortalHelper::getTierInfo();
$tierOnePurchaseValue = 3000;
$tierTwoPurchaseValue = 6000;
$tierThreePurchaseValue = 9000;
if($tierInfoForTnc){
	$tierOnePurchaseValue = $tierInfoForTnc[0]['minPurchaseValue'];
	$tierTwoPurchaseValue = $tierInfoForTnc[1]['minPurchaseValue'];
	$tierThreePurchaseValue = $tierInfoForTnc[2]['minPurchaseValue'];
}
$customerSupportCall = WidgetKeyValuePairs::getWidgetValueForKey('customerSupportCall');
$sLoyaltyTermsCondition = WidgetKeyValuePairs::getWidgetValueForKey('loyalty.mymyntra.terms');
$sLoyaltyTermsCondition = str_replace('{#tierOnePurchaseValue}',$tierOnePurchaseValue,$sLoyaltyTermsCondition);
$sLoyaltyTermsCondition = str_replace('{#tierTwoPurchaseValue}',$tierTwoPurchaseValue,$sLoyaltyTermsCondition);
$sLoyaltyTermsCondition = str_replace('{#tierThreePurchaseValue}',$tierThreePurchaseValue,$sLoyaltyTermsCondition);
$sLoyaltyTermsCondition = str_replace('{#customerSupportCall}',$customerSupportCall,$sLoyaltyTermsCondition);

$smarty->assign('loyaltyDisclamer',WidgetKeyValuePairs::getWidgetValueForKey("loyalty.mymyntra.activitytable.disclaimer"));
$loyaltyTermsCondition = json_decode($sLoyaltyTermsCondition,true);
$smarty->assign('loyaltyTermsCondition',$loyaltyTermsCondition);

$loyalty_description = WidgetKeyValuePairs::getWidgetValueForKey('loyalty.mymyntra.loyalty_description');
$smarty->assign('loyalty_description',$loyalty_description);

$banner_img_url = WidgetKeyValuePairs::getWidgetValueForKey('loyalty.mymyntra.banner_img_url');
$smarty->assign('banner_img_url',$banner_img_url);

$sHow_it_works_block1 = WidgetKeyValuePairs::getWidgetValueForKey('loyalty.mymyntra.how_it_works.block1');
$how_it_works_block1 = json_decode($sHow_it_works_block1,true);
$smarty->assign('how_it_works_block1',$how_it_works_block1);

$sHow_it_works_block2 = WidgetKeyValuePairs::getWidgetValueForKey('loyalty.mymyntra.how_it_works.block2');
$how_it_works_block2 = json_decode($sHow_it_works_block2,true);
$smarty->assign('how_it_works_block2',$how_it_works_block2);

$sHow_it_works_block3 = WidgetKeyValuePairs::getWidgetValueForKey('loyalty.mymyntra.how_it_works.block3');
$how_it_works_block3 = json_decode($sHow_it_works_block3,true);
$smarty->assign('how_it_works_block3',$how_it_works_block3);
//Configurable parameters ends


$loyaltyPointsDetails = LoyaltyPointsPortalClient::getAccountInfoForMyMyntra($login);

if(empty($loyaltyPointsDetails) || (!empty($loyaltyPointsDetails['status']) && $loyaltyPointsDetails['status']['statusMessage'] != 'SUCCESS')){
	//Error condition send loyalty error page
	$errorlog->error("Loyalty Points Service Error! loyaltyPointsDetails : ". print_r($loyaltyPointsDetails,true));
    $smarty->display('my/error_page.tpl');
    exit;
}

$showLoyaltyWelcomeMessage = false; // Flag to show loyalty welcome message on first activation only

if(!empty($XCART_SESSION_VARS['showLoyaltyWelcomeMessage']) && $XCART_SESSION_VARS['showLoyaltyWelcomeMessage']){
	$showLoyaltyWelcomeMessage = true;
	$XCART_SESSION_VARS['showLoyaltyWelcomeMessage'] = false;
}

$smarty->assign('showLoyaltyWelcomeMessage',$showLoyaltyWelcomeMessage);
$isActivatedUser = $loyaltyPointsDetails['userAccountInfo']['tNcAccepted']; //When the user accept terms and conditions
$smarty->assign('isActivatedUser',$isActivatedUser);

$tierSlabsInfo = $loyaltyPointsDetails['tierInfo']['tierSlabsInfo'];

$currentTierInfo = array();
$currentTierInfo['tierNumber'] = $loyaltyPointsDetails['tierInfo']['presentTierNumber']; 
$currentTierInfo['awardForHundred'] = $loyaltyPointsDetails['tierInfo']['loyaltyPointsUserAwardFactor'];
$currentTierInfo['tierLabel'] = getTierLabel($currentTierInfo['tierNumber']);

$currentTierNumber = $currentTierInfo['tierNumber'];
$nextTierNumber = $currentTierInfo['tierNumber'] + 1;
$nextTierInfo = array();
$nextTierInfo['show'] = false;

foreach ($tierSlabsInfo as $key => $value) {
	// Putting in tier message for current tier
	if ($value['tierNumber'] == $currentTierNumber) {
		if(!empty($value['tierMessage'])){
			$currentTierInfo['tierMessages'] = explode("|", $value['tierMessage']);
			$currentTierInfo['tierMessagesCount'] = count($currentTierInfo['tierMessages']);
		}
	}elseif($value['tierNumber'] == $nextTierNumber){
		$nextTierInfo['show'] = true;
		$nextTierInfo['tierNumber'] = $value['tierNumber'];
		$nextTierInfo['tierLabel'] = getTierLabel($value['tierNumber']);
		$nextTierInfo['awardForHundred'] = $value['loyaltyPointsUserAwardFactor'];
		$nextTierInfo['minPurchaseValue'] = $value['minPurchaseValue'];
		$nextTierInfo['tierMessage'] = $value['tierMessage'];
		if(!empty($value['tierMessage'])){
			$nextTierInfo['tierMessages'] = explode("|", $value['tierMessage']);
			$nextTierInfo['tierMessagesCount'] = count($nextTierInfo['tierMessages']);
		}
		break;
	}
}

$smarty->assign('currentTierInfo',$currentTierInfo);
$smarty->assign('nextTierInfo',$nextTierInfo);

$activePointsBalance = $loyaltyPointsDetails['userAccountInfo']['activePointsBalance'];
$inActivePointsBalance = $loyaltyPointsDetails['userAccountInfo']['inActivePointsBalance'];
$activePointsBalanceRupees = $loyaltyPointsDetails['userAccountInfo']['cashEqualantAmount'];
$smarty->assign('activePointsBalance',$activePointsBalance);
$smarty->assign('inActivePointsBalance',$inActivePointsBalance);
$smarty->assign('activePointsBalanceRupees',$activePointsBalanceRupees);

$transactionHistoryStart = 0 ;
$transactionHistorySize = 10 ;
$transactionHistory = LoyaltyPointsPortalClient::getTransactionHistory($login,LoyaltyPointsPortalClient::CONSOLIDATED,$transactionHistoryStart,$transactionHistorySize + 1); //+1 to check if more data is present or not
$recentActivity = $transactionHistory['consolidatedTransactionHistory'];
$showMoreRecentActivity = false;
if(count($recentActivity) > $transactionHistorySize){
	//We need to show show more link and remove the last element from array
	$showMoreRecentActivity = true;
	unset($recentActivity[count($recentActivity)-1]);	
}

for ($i=0; $i < count($recentActivity); $i++) { 
	$recentActivity[$i]['showRow'] = (strtolower($recentActivity[$i]['accountType']) === 'inactivepoints' && $recentActivity[$i]['businesProcess'] === 'POINTS_ACTIVATED' && $recentActivity[$i]['creditOutflow'] > 0) ? false : true;
	// $recentActivity[$i]['description'] = (strtolower($recentActivity[$i]['accountType']) === 'activepoints' && $recentActivity[$i]['businesProcess'] === 'POINTS_ACTIVATED' && $recentActivity[$i]['creditInflow'] > 0) ? 'Activating Points for item no. $recentActivity[$i]["itemId"] ' : $recentActivity[$i]['description'];
}
$smarty->assign('recentActivity',$recentActivity);
$smarty->assign('transactionHistoryStart',$transactionHistoryStart + $transactionHistorySize);
$smarty->assign('transactionHistorySize',$transactionHistorySize);
$smarty->assign('showMoreRecentActivity',$showMoreRecentActivity);

$fiveDaysAgo = time() - (5 * 24 * 60 * 60);
$smarty->assign('fiveDaysAgo',$fiveDaysAgo);

$tierSlabsInfo = $loyaltyPointsDetails['tierInfo']['tierSlabsInfo'];

for ($i=0; $i < count($tierSlabsInfo); $i++) { 
	$tierSlabsInfo[$i]['isCurrentTier'] = $tierSlabsInfo[$i]['tierNumber'] == $currentTierInfo['tierNumber'] ? true : false;
	$tierSlabsInfo[$i]['awardForHundred'] = $tierSlabsInfo[$i]['loyaltyPointsUserAwardFactor'];
	$tierSlabsInfo[$i]['tierLabel'] = getTierLabel($tierSlabsInfo[$i]['tierNumber']);
	if(!empty($tierSlabsInfo[$i]['tierMessage'])){
		$tierSlabsInfo[$i]['tierMessages'] = explode("|", $tierSlabsInfo[$i]['tierMessage']);
		$tierSlabsInfo[$i]['tierMessagesCount'] = count($tierSlabsInfo[$i]['tierMessages']);
	}
}
$smarty->assign('tierSlabsInfo',$tierSlabsInfo);
$smarty->assign('loyaltyDisableMessage',WidgetKeyValuePairs::getWidgetValueForKey('loyalty.disableMessage'));

$smarty->display("my/myntloyalty.tpl");
