<?php

$newrelicFilteredAgents = array("KTXN");
$filter = false;
foreach($newrelicFilteredAgents as $agent){
    if(strpos($_SERVER["HTTP_USER_AGENT"], $agent) !== FALSE){
        $filter = true;
        break;
    }
}
/*
//Code for removing monitoring for affiliates
if(strpos($_GET['utm_source'], 'aff-') !== false){
    $filter = true;
}
*/
if( extension_loaded('newrelic') && $filter != true){
	$newrelic_header = newrelic_get_browser_timing_header();
	$newrelic_footer = newrelic_get_browser_timing_footer();
	$smarty->assign("newrelic_header", $newrelic_header);
	$smarty->assign("newrelic_footer", $newrelic_footer);
}
if( extension_loaded('oboe') && $filter != true){
    $tracelytics_header = oboe_get_rum_header();
    $tracelytics_footer = oboe_get_rum_footer();
    $smarty->assign("tracelytics_header", $tracelytics_header);
    $smarty->assign("tracelytics_footer", $tracelytics_footer);
}

?>
