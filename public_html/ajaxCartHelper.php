<?php

include_once("./auth.php");
include_once("$xcart_dir/include/class/instrumentation/BaseTracker.php");
$tracker = new BaseTracker(BaseTracker::PAYMENT_PAGE);

include_once "$xcart_dir/include/func/func.mkcore.php";
include_once "$xcart_dir/include/func/func.randnum.php";
include_once "$xcart_dir/include/func/func.mk_orderbook.php";
include_once "$xcart_dir/include/class/class.mymyntra.php";
include_once "$xcart_dir/include/class/class.orders.php";
include_once "$xcart_dir/modules/coupon/database/CouponAdapter.php";
include_once "$xcart_dir/modules/coupon/CouponValidator.php";
include_once "$xcart_dir/modules/coupon/exception/CouponNotFoundException.php";
include_once "$xcart_dir/include/class/class.feature_gate.keyvaluepair.php";
include_once "$xcart_dir/include/class/mcart/class.MCart.php";
include_once "$xcart_dir/modules/discount/DiscountEngine.php";
include_once "$xcart_dir/modules/coupon/CouponDiscountCalculator.php";
include_once "$xcart_dir/modules/coupon/CashCouponCalculator.php";
include_once "$xcart_dir/include/class/class.feature_gate.keyvaluepair.php";
//$data = filter_input(INPUT_POST,'data',FILTER_SANITIZE_STRING);

    $mcartFactory= new MCartFactory();
    $myCart= $mcartFactory->getCurrentUserCart();
    $calcCouponDiscount = false;
    $showCouponMessage = false;
    $userCouponCode = filter_input(INPUT_POST, 'couponcode', FILTER_SANITIZE_STRING);
    $removecouponcode = filter_input(INPUT_POST, 'removecouponcode', FILTER_SANITIZE_STRING);
    //coupone code logic
    if (!empty($userCouponCode)) {
        $myCart->addCouponCode($userCouponCode, MCart::$__COUPON_TYPE_DEFAULT);
    } else if(!empty($removecouponcode) && $removecouponcode=='remove') {
        $couponCodeArrayRem=array_keys($myCart->getAllCouponCodes(MCart::$__COUPON_TYPE_DEFAULT));
        $myCart->removeCoupon($couponCodeArrayRem[0], MCart::$__COUPON_TYPE_DEFAULT);
        $smarty->assign("appliedCouponCode", "");
    }
    $myCart->refresh(true,true,false,true);
    MCartUtils::persistCart($myCart);
    x_session_unregister("LAST_CART_UPDATE_TIME");
    unset($XCART_SESSION_VARS["LAST_CART_UPDATE_TIME"]);
    $lastUpdatedTime=$myCart->getLastContentUpdatedTime();
    x_session_register('LAST_CART_UPDATE_TIME',$lastUpdatedTime);
    $couponAppliedItemsCount=$myCart->getCouponAppliedItemCount();
    $discountCouponArray=array_keys($myCart->getAllCouponCodes(MCart::$__COUPON_TYPE_DEFAULT));
    //Currently We have just one coupon for each type
    $discountCouponCode=$discountCouponArray[0];
    if(!empty($discountCouponCode)){
        $applicationCode = $myCart->getCouponApplicationCode($discountCouponCode, MCart::$__COUPON_TYPE_DEFAULT);
        if($applicationCode===MCart::$__COUPON_APP_CODE_SUCCESS){
            $discount = number_format($myCart->getDiscount(), 2, ".", ',');
                $couponMessage = "<span class=\"ajax-success\"></span><span class=\"black\">You have "
                    . "successfully saved <span class=\"red\">Rs. $discount</span> by using coupon "
                    .strtoupper($discountCouponCode)." on ".$couponAppliedItemsCount." items in your cart</span>" ;
            $smarty->assign("divid", "success");
        }else{
                $couponMessage = "<font color=\"red\">".$myCart->getCouponApplicationErrorMessage($discountCouponCode,  MCart::$__COUPON_TYPE_DEFAULT)."</font>";
                $myCart->removeCoupon($discountCouponCode, MCart::$__COUPON_TYPE_DEFAULT);
                $smarty->assign("divid", "error");
        }
    
        $showCouponMessage=true;
    }
    
    $additionalCharges = number_format($myCart->getAdditionalCharges(), 2, ".", '');
    $totalMRP = number_format($myCart->getMrp(), 2, ".", '');
    $vat = number_format($myCart->getVat(), 2, ".", '');
    $totalQuantity = number_format($myCart->getItemQuantity(), 0);
    $couponDiscount = number_format($myCart->getDiscount(), 2, ".", '');
    $cashDiscountge = number_format($myCart->getShippingCharge(), 2, ".",'');
    $cartDiscount = number_format($myCart->getCartLevelDiscount(), 2, ".", '');
    $giftCharges = number_format($myCart->getGiftCharge(), 2, ".",'');
    $cashDiscount = $myCart->getCashDiscount();
    $mrp = $totalMRP;
    $amount = $totalMRP + $additionalCharges;
    $cashback_tobe_given=$myCart->getCashbackToGivenOnCart();
    $nonDiscountedItemAmount=$myCart->getNonDiscountedItemsSubtotal();
    $nonDiscountedItemCount=$myCart->getNonDiscountedItemCount();
    $couponAppliedItemsCount=$myCart->getCouponAppliedItemCount();
    $couponAppliedSub = number_format($myCart->getCashDiscount(), 2, ".", '');
    $productDiscount = number_format($myCart->getDiscountAmount(), 2, ".", '');
    $shippingChartotal=$myCart->getCouponAppliedSubtotal();
    $shippingCharge = $myCart->getShippingCharge();
    $totalAmount = $totalMRP + $additionalCharges - $productDiscount - $couponDiscount - $cashDiscount + $shippingCharge - $cartDiscount + $giftCharges;
    $shippingText = "SHIPPING ";
    if($shippingCharge > 0) {
        $shippingText .=  "FEE (+)Rs."; 
        $shippingText  = $shippingText . '<span class="red">'. number_format(round($shippingCharge)).'</span>';
    }else{
        $shippingText .= '<span class="green">FREE</span>';
    }

    $arr = array(
        'totalMRP' => round($totalMRP),
        'couponDiscount' => round($couponDiscount),
        'totalAmount' => round($totalAmount),
        'couponMessage' => $couponMessage,
        'productDiscount' => round($productDiscount),
        'vat' => round($additionalCharges),
        'shippingCharge' => $shippingCharge,
        'shippingText' => $shippingText
        );
    if($couponDiscount != null && $couponDiscount != 0 && $couponMessage != null){
        $response = json_encode(array('status' => 'SUCCESS', 'data' => $arr));
        echo $response;
    }
    else {
        $response = json_encode(array('status' => 'ERROR', 'data' => $arr));
        echo $response;
    }

?>
