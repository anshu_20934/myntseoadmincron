<?php

require_once "auth.php";
include_once("$xcart_dir/include/class/widget/class.widget.keyvaluepair.php");
include_once \HostConfig::$documentRoot."/include/class/class.mymyntra.php";

//use shopfest\ShopFest;
include_once("$xcart_dir/include/class/shopfest/ShopFest.php");

$isXHR = isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtoupper($_SERVER['HTTP_X_REQUESTED_WITH']) == 'XMLHTTPREQUEST';
$smarty->assign('isXHR', $isXHR);

if (empty($login)) {
	if ($isXHR) {
        $response = array('status' => 'LOGIN');
        header('Content-Type:application/json');
        echo json_encode($response);
    }
   /* else {
        func_header_location($http_location . '/register.php');
    }*/
   
}

$showDailyPrizes = FeatureGateKeyValuePairs::getFeatureGateValueForKey('shopfest.showDailyPrizes');
$showAssuredPrizes = FeatureGateKeyValuePairs::getFeatureGateValueForKey('shopfest.showAssuredPrizes');
$showBumperPrizes = FeatureGateKeyValuePairs::getFeatureGateValueForKey('shopfest.showBumperPrizes');

$smarty->assign('showDailyPrizes', $showDailyPrizes);
$smarty->assign('showAssuredPrizes', $showAssuredPrizes);
$smarty->assign('showBumperPrizes', $showBumperPrizes);

$smarty->assign('login', $login);

$api = new ShopFest();
$lbid = ShopFest::getLeaderBoardId();
$daysToGoForThisWeek = $api->DaysToGoForThisWeek();
$daysToGoForMonth = $api->DaysToGoForMonth();
$lastWeekWinners = $api->getLastWeekWinners($lbid);
$numberOfWinners = $api->getNumberOfWinnersMsg($lbid);
$numberOfWeeklyWinners = $api->getNumberOfWeeklyWinnersMsg($lbid);

$smarty->assign('lastWeekWinners', $lastWeekWinners);
$smarty->assign('numberOfWinners', $numberOfWinners);
$smarty->assign('numberOfWeeklyWinners', $numberOfWeeklyWinners);

$myBag = $api->getTotalAmountByUser($lbid,false,$login);
$weeklyTickets = $api->getTotalWeekTickets($lbid,$login);

if(!$login){
	$BumperPrizeUserMsg = "";
}else if($weeklyTickets['tickets'] == 0){
	$BumperPrizeUserMsg = "You are <strong> Rs." . $weeklyTickets['amount'] . " </strong> away from qualifying for the lucky draw!" ;
}else if($weeklyTickets['tickets'] != 0){
	$BumperPrizeUserMsg = "Congratulations!! You qualify for the lucky draw this week. You currently have <strong>" . intval($weeklyTickets['tickets']) . ((intval($weeklyTickets['tickets']) == 1 ) ? " entry" : " entries") . " </strong> in the lucky draw.Increase your chance of winning by shopping in increments of Rs 1,000";
}
	

$winners50K = $api->getAllBuyersInARange($lbid,true,50000,100000);
$winners1Lakh = $api->getAllBuyersInARange($lbid,false,100000,0);

$winnersAbove50K = $api->getAllBuyersInARange($lbid,false,50000,0);
if($winnersAbove50K){
	//echo "inside true</br>";
	$winnersAbove50K = $winnersAbove50K . ' winners so far.';
}else{
	//echo "inside false</br>";
	$winnersAbove50K = '';
}

$smarty->assign('winnersAbove50K', $winnersAbove50K);
$smarty->assign('winners50K', $winners50K);
$smarty->assign('winners1Lakh', $winners1Lakh);
$smarty->assign('BumperPrizeUserMsg', $BumperPrizeUserMsg);

$leftBag1 = 270 * $myBag/50000 . 'px';
$leftBag2 = 270 * $myBag/100000 . 'px';

if( $myBag > 50000){
  $leftBag1 = '270px';
}

if( $myBag > 100000){
   $leftBag2 = '270px';
}

if($myBag == null || $myBag == '' || $myBag == 0){
	$myBag = 0;
}
$myBagDisplay = 'Rs.' . $myBag;
$moreForFifty = 'Rs. ' . (50000 - $myBag);
$moreForLakh  = 'Rs. ' . (100000 - $myBag);
$filler = '&nbsp;';

if($myBag < 50000){

	$moreToWinFor50 = "<p> Shop for <strong style='font-color:black;'> " . $moreForFifty ." more</strong> in December to get a <strong style='font-color:black;'>Samsung Galaxy Tab 2</strong> ";
	$filler = "OR";

	$moreToWinFor100 = "<p>Shop for <strong style='font-color:black;'> " . $moreForLakh . " more </strong> in December to get a <strong style='font-color:black;'>Samsung Galaxy Note 2</strong></p>";
}else if ($myBag >= 50000 && $myBag < 100000){
	$moreToWinFor50 = "<p> Congratulations!! You have won a <strong>Samsung Galaxy Tab 2</strong> .";
	$moreToWinFor100 = "<p>Shop for Rs. " . $moreForLakh . " more to get a <strong>Samsung Galaxy Note 2 </strong> instead. </p>";
}else if ($myBag >= 100000){
	$moreToWinFor50 = "<p style='height:28px;width:220px;'> &nbsp; <br/> &nbsp; </p>";
        $moreToWinFor100 = "<p> Congratulations!! You have won a <strong>Samsung Galaxy Note 2 </strong> </p>";
}

$smarty->assign('moreForFifty', $moreForFifty);
$smarty->assign('moreForLakh', $moreForLakh);
$smarty->assign('moreToWin', $moreToWin);
$smarty->assign('moreToWinFor50', $moreToWinFor50);
$smarty->assign('moreToWinFor100', $moreToWinFor100);
$smarty->assign('leftBag2', $leftBag2);
$smarty->assign('leftBag2', $leftBag2);
$smarty->assign('leftBag1', $leftBag1);
$smarty->assign('leftBag2', $leftBag2);
$smarty->assign('filler', $filler);
$smarty->assign('current-spending', $myBag);
$smarty->assign('myBag', $myBagDisplay);
$smarty->assign('daysToGoForThisWeek', $daysToGoForThisWeek);
$smarty->assign('daysToGoForMonth', $daysToGoForMonth);
$smarty->assign('topNBuyers', WidgetKeyValuePairs::getWidgetValueForKey("fest_topNbuyers"));
$smarty->assign('topNCatBuyers', WidgetKeyValuePairs::getWidgetValueForKey("fest_topNCategoryBuyers"));
$smarty->assign('festLuckydrawMinAmount',WidgetKeyValuePairs::getFloat("fest_lucky_draw_min_amount", 5000));

// terms and conditions markup
$smarty->assign('termsCondDaily', WidgetKeyValuePairs::getWidgetValueForKey("fest_terms_cond_daily"));
$smarty->assign('termsCondAssured', WidgetKeyValuePairs::getWidgetValueForKey("fest_terms_cond_assured"));
$smarty->assign('termsCondBumper', WidgetKeyValuePairs::getWidgetValueForKey("fest_terms_cond_bumper"));




$prizes_images = explode(",",$smarty->get_template_vars("shoppingfestDailyPrizes"));
$prizesConfigured =  WidgetKeyValuePairs::getWidgetValueForKey("fest_prizes_semicolon_separated");
$top_prizes = explode(";", $prizesConfigured);
$prizesConfigured =  WidgetKeyValuePairs::getWidgetValueForKey("fest_prizes_semicolon_separated");
$top_prizes = explode(";", $prizesConfigured);
$prizes = array(
    array('image' => $prizes_images[0], 'label'=> 'Fiery Five<br/>#1' , 'name' => $top_prizes[0]),
    array('image' => $prizes_images[1], 'label'=> 'Fiery Five<br/>#2', 'name' => $top_prizes[1]),
    array('image' => $prizes_images[2], 'label'=> 'Fiery Five<br/>#3', 'name'=> $top_prizes[2]),
    array('image' => $prizes_images[3], 'label'=> 'Fiery Five<br/>#4', 'name'=> $top_prizes[3]),
    array('image' => $prizes_images[4], 'label'=> 'Fiery Five<br/>#5', 'name'=>$top_prizes[4]),
    array('image' => $prizes_images[5], 'label'=> 'Hot Shopper - Womenswear', 'name'=>WidgetKeyValuePairs::getWidgetValueForKey("fest_prizes_women_semicolon_separated")),
    array('image' => $prizes_images[6], 'label'=> 'Hot Shopper - Menswear', 'name'=>WidgetKeyValuePairs::getWidgetValueForKey("fest_prizes_men_semicolon_separated"))
);

$dw_ui_msg = WidgetKeyValuePairs::getWidgetValueForKey("fest_daily_winners_ui_msg");
$smarty->assign('dw_ui_msg', $dw_ui_msg);

function getToppers() {
	global $login;
    global $aLeaderBoardData, $smarty, $api, $lbid;
	
    $aLeaderBoardData = $api->getLeaderBoardData($lbid, date('Y-m-d'), false, $login, true); 
  
    $top = $aLeaderBoardData ;

    $smarty->assign('topAll', $top['topAll']);
    $smarty->assign('topMen', $top['topMen']);
    $smarty->assign('topWomen', $top['topWomen']);
    $smarty->assign('userTopAmountAll', $top['myBagAll']);
    $smarty->assign('userTopAmountMen', $top['myMen']);
    $smarty->assign('userTopAmountWomen', $top['myWomen']);
    $smarty->assign('userTopMsgForPrize', $top['msg']);
}

function getWinners($date) {
	global $login;
    global $winners, $prizes, $smarty, $api, $lbid;
    if($date == ''){
       $yesterday = date("Y-m-d", strtotime( '-1 days' ) );;
    }
    $winners = $api->getLeaderBoardData($lbid, $yesterday, false, $login, false);
    $winner_names = array();
    
    foreach ($winners as $category=>$aWinners){
	    foreach ($aWinners as $winner){
			if(!$winner['name']){
				$winner['name'] = 'No Takers';
			}
			if($category == 'topAll'){
				$winner_names[$winner['pos']]["name"] = $winner['name'];
			}	
			if($category == 'topMen'){
				$winner_names["Menswear"]["name"] = $winner['name'];
			}	
			if($category == 'topWomen'){
				$winner_names["Womenswear"]["name"] = $winner['name'];
			}	
    	}
    }
	
	if(strtotime(WidgetKeyValuePairs::getWidgetValueForKey('fest_month_start_date')) == strtotime(date("Y-m-d"))) $winner_names = null;
    $smarty->assign('winner_names', $winner_names);
    $smarty->assign('prizes', $prizes);
}

if (!$isXHR) {
    getToppers();
    getWinners();
    $smarty->display('shopfest.tpl');
}
else {
    $response = array();
    $response['status'] = 'SUCCESS';
    $_action = filter_var($_GET['_action'], FILTER_SANITIZE_STRING);

    if ($_action == 'toppers') {
        getToppers();
        $response['markup'] = $smarty->fetch('shopfest/toppers.tpl');
    }

    header('Content-Type:application/json');
    echo json_encode($response);
}


