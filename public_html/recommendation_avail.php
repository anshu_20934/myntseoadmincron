<?php
/*Useful for checking the recos coming from avail*/
define("QUICK_START", 1);
include_once("./auth.php");
include_once "$xcart_dir/include/class/widget/class.widget.skulist.php";

$styles_to_show = filter_input(INPUT_GET, 'avail_ids', FILTER_SANITIZE_STRING);
$tracking_code = filter_input(INPUT_GET, 'avail_track', FILTER_SANITIZE_STRING);
$page          = filter_input(INPUT_GET, 'page', FILTER_SANITIZE_STRING);
if(empty($styles_to_show)){
	echo 0;
	exit;
}

switch($page)
{
	case "home":
		$avail_title="";
		break;
	case "pdp" :
		$avail_title="Other Customers Recommend";
		break;
	case "search":
		$avail_title="Other Customers Recommend";
	    break;
	case "cart":
		$avail_title="You Might Also Be Interested In";
		break;
	default:
		$avail_title="You May Also Like";
		break;

}
$styles_to_show = explode(",",$styles_to_show);
if($skin=='skin1' && $page=='pdp'){
	$styles_to_show=array_slice($styles_to_show,0,5);
}
global $shownewui;
if(!empty($styles_to_show)){
	$styleWidget=new WidgetSKUList('avail recommendation', $styles_to_show);
	$widgetData = $styleWidget->getData();
	$widgetArray = array();
	$smarty->assign('page',$page);
	$smarty->assign('tracking_code',$tracking_code);
	$smarty->assign("avail_recommendation",$widgetData);
	$smarty->assign("avail_title",$avail_title);
	if($skin=='skin2'){
		if($page=='addedtocart'){
			$div_content=$smarty->fetch("inc/matchmaker-horizontal.tpl", $smarty);
		}
		elseif($page=='cart'){
			$smarty->assign('cartpage','true');
			$div_content=$smarty->fetch("inc/matchmaker-horizontal.tpl", $smarty);
		}
		else{
			$div_content=$smarty->fetch("inc/matchmaker-vertical.tpl", $smarty);
		}
	}
	else{
		if($page=='cart'){
			$div_content=$smarty->fetch("site/avail_widget_cart.tpl", $smarty);
		}
		else{
   			$div_content=$smarty->fetch("site/avail_widget.tpl", $smarty);
		}
	}
	echo $div_content;
}
else
{
	echo "0";
}
?>
