<?php
require_once "./auth.php";
include_once("$xcart_dir/modules/coupon/database/CouponAdapter.php");
if(!empty($_POST) && !empty($_POST["emailid"]))
{
	$error_msg='';
	$success_msg='';
	$check=func_query("select * from mk_survey_coupon where login='".$_POST["emailid"]."' and survey_type='CUSTSURVEY1011'");
	if(empty($check[0]))
	{
		$error_msg="Sorry! This email ID does not exist in our database. This could be because you may not be original recipient of the survey. Please enter correct email ID on which you have received the survey questionnaire";
		$smarty->assign("error_msg",$error_msg);	
	}
	else{
		if(!empty($check[0]['coupon_code']))
		{
			$error_msg="A coupon has already been issued against this email address on ".date('d F Y',$check[0]['issued_on']).". Please check \"My Credits\" under \"My Myntra\" once you're signed in. In case the credit is not added, please write to support@myntra.com.";
			$smarty->assign("error_msg",$error_msg);
		}
		else
		{
			global $mrpCouponConfiguration;
		
			$validity = $mrpCouponConfiguration['cust_survey_validity']; 
			$amount = $mrpCouponConfiguration['cust_survey_mrpAmount'];
			$minimum = $mrpCouponConfiguration['cust_survey_minCartValue'];
							
			$startDate = strtotime(date('d F Y', time()));		
			$endDate = $startDate + $validity * 24 * 60 * 60;
			$groupName = 'CUSTSURVEY1011';
			$type = 'absolute';
			$percent = '';
			
			$couponAdapter = CouponAdapter::getInstance();
			//need group name 
			if (!$couponAdapter->existsCouponGroup($groupName)) {
				$couponAdapter->createCouponGroup(
				$groupName, 'online', time(), 'myntraprovider');
			}
		
			$login=$_POST["emailid"];			
			$desc="Survey Coupon - Rs. $amount coupon";
		
		
			$couponCode = $couponAdapter->generateSingleCouponForUser("SRVY", "6", $groupName, $startDate, $endDate, $login, $type, $amount, '', $minimum, $desc);
        	func_query("update mk_survey_coupon set coupon_code='".$couponCode."',issued_on=".time()." where login='".$login."'");
		
        
        // Mail the customer.
        	$template = "custsurvey0611";
            $login_tw = str_replace("@", "%40", $login);
            $args = array (
                "USER"			=> $login,
                "USER_TW"		=> $login_tw,
                "COUPONCODE"	=> strtoupper($couponCode),
                "VALIDITY"		=> $validity,
            	"MRP_AMOUNT"	=> intval($amount),
            	"MIN_AMOUNT"	=> intval($minimum)
            );
		sendMessageDynamicSubject($template, $args, $login);
		//$success_msg="Your gift voucher has been sent to ".$login.".  In case you have not received it, please write to support@myntra.com ";
            $success_msg="<span style=\"float: left; font-size: 13px; padding: 0pt 15px; text-align: left;\">Thanks for taking time to complete our survey. As a token of appreciation, please accept a gift voucher of Rs.".intval($amount)." from us.<br>
            Your voucher code is <b>".strtoupper($couponCode)."</b>. This voucher is valid for $validity days from the date of issue, and on purchases of Rs.".intval($minimum)." or above. You can view this voucher in \"My Credits\" under \"My Myntra\" once you're signed in.<br>
            <br>
                        <b>How to use your voucher -</b>

            <ul>
            <li>Select products, add to cart</li>
            <li>
                        At check out, enter the voucher code and redeem<br></li>
            <li>            Pay the balance using your credit/debit cards or internet banking<br></li>

            </ul>
            In case of any queries, please feel free to call us any time of the day at +91 80 43541999 .</span>";
		$smarty->assign("success_msg",$success_msg);
        $smarty->assign("show_banner_text","0");            
		$smarty->assign("banner_text","Your coupon has been generated.");
		$smarty->assign("couponCode",$couponCode);
		}
	}
}
func_display("survey_coupon.tpl",$smarty);

?>
