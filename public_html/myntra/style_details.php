<?php
require_once "../auth.php";
require_once($xcart_dir."/include/class/search/SearchProducts.php");
require_once($xcart_dir."/modules/apiclient/SkuApiClient.php");
include_once($xcart_dir."/include/class/widget/class.widget.keyvaluepair.php");
/* Get the additional product details required
 *	# of units sold sicne Sept 1-2011
 *	# of Units in stock
 *	# of Units sold in last 3 days
 *	Season
 *	Popularity Matrix
 *		availability
 *		revanue_sort_field
 *		potential_revenue_sort_field
 */


//check if the logged in user is allowed to see the style_details
$usersToShowAdditionalDetails = WidgetKeyValuePairs::getWidgetValueForKey('showStyleDetailsToUser');
if(empty($usersToShowAdditionalDetails)){
	exit;
}else{
	$user_list=explode(",", $usersToShowAdditionalDetails);
	if(!in_array($login, $user_list)){
		exit;
	}
}

$details = array();
// Units sold since (get value from feature gate)
$startDate = FeatureGateKeyValuePairs::getFeatureGateValueForKey('styleDeatilsUnitsSoldSince');
if(empty($startDate) || !strtotime($startDate))
{
	$startDate = "01-02-2012";
}
$start_date = strtotime($startDate);
$startDate = date('M j, Y', $start_date);

$query1 = "select min(orderid) as minorderid from xcart_orders where date>$start_date";
$result1=func_query_first($query1, true);
$style_id = mysql_escape_string($style_id);
if(!empty($result1['minorderid'])){
	$query2="select count(*) as count from xcart_order_details where product_style='$style_id' and orderid>".$result1['minorderid'];
	$result2=func_query_first($query2, true);
	$details["Units sold since ".$startDate]= $result2['count'];
}else{
	$details["Units sold since ".$startDate]= '0';
}

// Toal units in warehouse retrieved using WMS API
$skuids = array();
//this returns sku ids which has > 0 count
$skusSql = "SELECT sku_id from mk_styles_options_skus_mapping where style_id = $style_id";
$skuResults = func_query($skusSql, true);
foreach ($skuResults as $row) {
	$skuids[] = $row['sku_id'];
}
//this returns count of each above obtained sku ids
//$t_count = SkuApiClient::getAvailableInvCountMap($skuids);
$skuId2InventoryMap = AtpApiClient::getAvailableInventoryForSkus($skuids);
$details["Total units in warehouse"] = 0;
foreach($skuId2InventoryMap as $skuId => $row) {
	$details["Total units in warehouse"] = $row['availableCount'];
}

/**
 * Updates from Solr
 * */
$ajaxSearchProducts = SearchProducts::getInstance();
$style_out = $ajaxSearchProducts->getSingleStyle($style_id);
$details["Season"]= $style_out['global_attr_season'];

$curTime = date('Y-m-d\TH:i:s\Z', time());
$diff = strtotime($curTime) - strtotime($style_out['global_attr_catalog_add_date']);
$days = floor($diff/86400);
$details["Go live days"] = $days;

$details["Units sold (Last 7 days)"] = $style_out['count_purchased'];
$details["Revenue (Last 7 days)"] = "Rs.".$style_out['revenue_sort_field'];
$details["Pot. Rev. (Last 7 days)"] = "Rs.".$style_out['potential_revenue_sort_field'];
$details["Category"] = $style_out['categories'];
if($format == 'json') {
	header('Content-type: application/json');
	$output="<ul>";
	foreach ($details as $key=>$value) {
		$output.="<li>".$key."</li><li>".$value."</li>"	;
	}
	$output.="</ul>";
	echo json_encode(array("innerhtml"=>$output));
}
?>