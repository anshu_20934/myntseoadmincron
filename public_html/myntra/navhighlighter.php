<?php
if(empty($nav_id)){
    $nav_id = filter_input(INPUT_GET, 'nav_id', FILTER_SANITIZE_NUMBER_INT);
    $nav_id = mysql_real_escape_string(htmlentities($nav_id));
}else{
        $nav_id = mysql_real_escape_string(htmlentities($nav_id));
	$nav_id = (int) $nav_id;
}
/*Store nav ids to xcache*/
global $xcache;
$key = "NavigationIdList";
$navIdWithUrlKey = "NavigationIdListWithUrl";
/*----------------------------*/

$smarty->assign ("nav_id",$nav_id);
$nav_id_temp = $nav_id;
$nav_arr = $xcache->fetchObject($key."::".$nav_id);
$nav_arr_url = $xcache->fetchObject($navIdWithUrlKey."::".$nav_id);

if(empty($nav_arr) || empty($nav_arr_url)){
    $nav_arr = array();
    $nav_arr_url = array();
    while(!empty($nav_id_temp) && $nav_id_temp != -1){
        $navQuery="select * from mk_widget_top_nav_v3 where id='$nav_id_temp'";
        $out = func_query_first($navQuery,true);
        $nav_id_temp=$out['parent_id'];
        $nav_arr[$out['id']]=$out[link_name];
        $nav_arr_url[$out[link_name]]=$out[link_url];
    }
    $xcache->storeObject($key."::".$nav_id,$nav_arr);
    $xcache->storeObject($navIdWithUrlKey."::".$nav_id,$nav_arr_url);
}
$nav_arr_reverse=array_reverse($nav_arr,true);
$nav_arr_url_reverse=array_reverse($nav_arr_url,true);
$smarty->assign("nav_selection_arr", $nav_arr_reverse);
$smarty->assign("nav_selection_arr_url", $nav_arr_url_reverse);

$nav_arr_reverse_breadcrumb = array(); 
foreach($nav_arr_url_reverse as $key=>$value) {
	if (!stristr($key, '<img')) {
		$nav_arr_reverse_breadcrumb[$key] = "<a href='" . $nav_arr_url_reverse[$key] . "'>" . $key . "</a>";
	} else if (strstr($key, 'alt="')) {
		$start = strpos($key, 'alt="') + 5;
		$end = strpos($key, '"', $start);
		$str = substr($key, $start, $end-$start);
		$nav_arr_reverse_breadcrumb[$key] = "<a href='" . $nav_arr_url_reverse[$key] . "'>" . $str . "</a>";
	}
}

if(count($nav_arr) > 1){
	$smarty->assign("navflag", true);
	$smarty->assign("navString", implode(" <span class='sep'>/</span> ", $nav_arr_reverse_breadcrumb));
}
