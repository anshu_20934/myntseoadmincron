<?php
require_once( "../auth.php");
include_once( "../include/solr/solrSuggest.php" );
if(!empty($term)){
    $query_org = str_replace( array( '\'', '"', ',' , ';', '*', '~' , '&'), '', $term);
    $list_size=10;
}
$query = strtolower($query_org);
$query = "keyword_edgy:(\"". trim($query)."\") ";
$query .= " AND (count_of_products:[1 TO *])";
$solr = new solrSuggest();
$solr_out = $solr->searchAndSort($query,0,$list_size,'',array("sort"=>"relevance_order desc, score desc, count_of_products desc"));
$documents = $solr_out->docs;
$id_list=array(0);
if(!empty($term)){
    $out_arr = array();
    foreach($documents as $landing_page){
    	$label = $landing_page->keyword;
        if ($layoutVariant == 'test' && strlen($label) > 45) {
        	$label = substr($label, 0, 42) . '...';
		}
    	$out_arr[] = array( "id"=>"$landing_page->keyword",
                            "label"=>"$label",
                            "value"=>"$landing_page->keyword",
                            "count"=>"$landing_page->count_of_products",
                            "category"=>"$landing_page->category"
                            );
        $id_list[]=$landing_page->id;
    }
// Check if the numebr is less then 10 then get like results
    if(empty($documents) || count($documents) != 10){
        $query = "keyword:(". trim($query_org)."*) ";
        $query .= " AND (count_of_products:[1 TO *])";
        $query .= " AND -id:(".implode(" OR ",$id_list).")";
        if(!empty($documents)){
            $list_size = $list_size - count($documents);
        }
        $solr_out = $solr->searchAndSort($query,0,$list_size,'',array("sort"=>"score desc, count_of_products desc"));
        $documents = $solr_out->docs;
        if(!empty($term)){
            foreach($documents as $landing_page) {
            	$label = $landing_page->keyword;
            	if ($layoutVariant == 'test' && strlen($label) > 45) {
            		$label = substr($label, 0, 42) . '...';
            	}
                $out_arr[] = array( "id"=>"$landing_page->keyword",
                                    "label"=>"$label",
                                    "value"=>"$landing_page->keyword",
                                    "count"=>"$landing_page->count_of_products",
                                    "category"=>"All Others"
                                    );
            }
        }
    }
    echo json_encode($out_arr);
}
