<?php
define("QUICK_START", 1);
use abtest\MABTest;
include_once("../auth.php");
require_once $xcart_dir.'/modules/RestAPI/RestRequest.php';

$styleId = filter_input(INPUT_GET, 'styleId', FILTER_SANITIZE_STRING);

$url = HostConfig::$styleServieUrl.'/recommendation/'.$styleId;
$method = 'GET';
$headers = array();
$headers[] = 'Accept: application/json';
$headers[] = 'Content-type: application/json';
$headers[] = 'Accept-Language: en-US';
$request = new RestRequest($url, $method);
$request->setHttpHeaders($headers);
$request->execute();
$info = $request->getResponseInfo();
$body = $request->getResponseBody();
if($info['http_code'] == 200){
    $response = json_decode($body,true);
    if($response['status']['statusType'] == 'SUCCESS'){
        $out =array();
        foreach($response['recommendedStyleList'] as $k=>$v) {
           $out[$k] = array($v);
        }
        echo json_encode($out);             
    }
}
?>