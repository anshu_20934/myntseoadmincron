<?php

define("QUICK_START", 1);
define("XCART_SESSION_START", 1);

include_once("../auth.php");

$pincode = filter_input(INPUT_GET, 'pincode', FILTER_SANITIZE_NUMBER_INT);
if (empty($pincode)) {
    $data = array(
        'status' => 'ERROR',
        'message' => 'Invalid input'
    );
}
else {
    $locality = array();
    $state = '';
    $city = '';
    $cities = array();
    $states = array();

    $query = "SELECT * FROM mk_pincode_city_state WHERE pincode = '$pincode'";
    $rows = func_query($query, true);
    foreach ($rows as $row) {
        $cities[] = $row['city'];
        $states[] = $row['state'];
    }

    if (count($cities)) {
        $cities = array_unique($cities);
        $states = array_unique($states);
        $city = $cities[0];
        $state = $states[0];
    }

    $query = "SELECT * FROM mk_pincode_locality WHERE pincode = '$pincode'";
    $rows = func_query($query, true);
    foreach ($rows as $row) {
        $locality[] = $row['locality'];
    }

    $data = array(
        'status'   => 'OK',
        'cities'   => $cities,
        'states'   => $states,
        'city'     => $city,
        'state'    => $state,
        'locality' => $locality
    );
}

$expires = 60 * 60;
header("Pragma: public");
header("Cache-Control: max-age=".$expires);
header('Expires: ' . gmdate('D, d M Y H:i:s', time() + $expires) . ' GMT');
header('Content-Type: application/json');
echo json_encode($data);
exit;

