<?php

define("QUICK_START", 1);
//define("XCART_SESSION_START", 1);

require_once("../auth.php");
require_once $xcart_dir."/include/check_useraccount.php";
require_once $xcart_dir."/include/func/func.courier.php";
require_once $xcart_dir."/include/func/func.returns.php";
require_once $xcart_dir."/include/func/func.utilities.php";
require_once $xcart_dir."/include/class/class.mymyntra.php";

//pickup changes
$ReturnPickupExcludedlist = WidgetKeyValuePairs::getWidgetValueForKey('pickup.disabled.articletypes');
$ReturnPickupExcludedArray = explode(",", $ReturnPickupExcludedlist);


function getReasonView() {
    global $smarty, $login;

	$selfDeliveryCredit = WidgetKeyValuePairs::getWidgetValueForKey('returnSelfDeliveryCredit');
    $customerSupportCall = WidgetKeyValuePairs::getWidgetValueForKey('customerSupportCall');
    $customerSupportTime = WidgetKeyValuePairs::getWidgetValueForKey('customerSupportTime');

    $markup = <<<HTML
<div class="step-reason">
    <h2> Select A Reason </h2>
    <label class="lbl-qty">choose the quantity that you want to return *</label>
    <div class="mk-custom-drop-down sel-qty">
        <select name="qty">
            <option value="" selected="selected">Select a quantity</option>
            <option value="1">1</option>
        </select>
    </div>

    <label>tell us why you want to return the product? *</label>
    <div class="mk-custom-drop-down sel-reason">
        <select name="reason">
            <option value="" selected="selected">SELECT A REASON</option>
HTML;

        $reasons = get_all_return_reasons(true);
        foreach ($reasons as $reason) {
            $markup .= <<<HTML
            <option value="{$reason['code']}">{$reason['displayname']}</option>
HTML;
    }

        $markup .= <<<HTML
        </select>
    </div>

    <label>Please provide any additional details</label>
    <textarea name="details"></textarea>
</div>
HTML;

    return array(
        'status' => 'SUCCESS',
        'markup' => $markup,
        'amtCredit' => (float)$selfDeliveryCredit,
        'customerSupportTime' => $customerSupportTime,
        'customerSupportCall' => $customerSupportCall
    );
}

function getRefundAmount() {
    global $smarty, $login;

    $shipOpt = $_GET['shipopt'] === 'self' ? 'self' : 'pickup';
    $itemId = filter_input(INPUT_GET, 'itemid', FILTER_SANITIZE_NUMBER_INT);
    $qty = filter_input(INPUT_GET, 'qty', FILTER_SANITIZE_NUMBER_INT);
    $amtRefundArray = func_calculate_refund_amount($itemId, $qty, $shipOpt);

    $resp = array(
        'itemId'    => $itemId,
        'qty'       => $qty,
        'shipOpt'   => $shipOpt,
        'amtRefund' => $amtRefundArray['refundAmount'],
    	'cashbackRefund'=> $amtRefundArray['cashbackRedeemed'],
        'status'    => 'SUCCESS',
    );
    return $resp;
}

function getConfirmView() {
    global $smarty, $login,$userToken;

    $markup = '';
    $isSelfReturn = $_REQUEST['ship-opt'] === 'self';
    $token = filter_input(INPUT_POST, '_token', FILTER_SANITIZE_STRING);
    
    //pickup changes
    $pickup_excluded = filter_input(INPUT_POST, 'pickup_excluded', FILTER_SANITIZE_STRING);
    $articletype = filter_input(INPUT_GET, 'articletype', FILTER_SANITIZE_NUMBER_INT);

    $orderid = filter_input(INPUT_POST, 'orderId', FILTER_SANITIZE_NUMBER_INT);
    $itemid = filter_input(INPUT_POST, 'itemId', FILTER_SANITIZE_NUMBER_INT);
    $option = filter_input(INPUT_POST, 'option', FILTER_SANITIZE_STRING);
    $quantity = filter_input(INPUT_POST, 'qty', FILTER_SANITIZE_NUMBER_INT);
    $customer_login = $login;
    $userid = $login;
    $reasoncode = filter_input(INPUT_POST, 'reason', FILTER_SANITIZE_STRING);
    $details = filter_input(INPUT_POST, 'details', FILTER_SANITIZE_STRING);
    $return_mode = $isSelfReturn ? 'self' : 'pickup';
    $selected_address_id = filter_input(INPUT_POST, 'addressId', FILTER_SANITIZE_NUMBER_INT);
    $pickupCourier = filter_input(INPUT_POST, 'pickupCourier', FILTER_SANITIZE_STRING);   
    
    //pickup changes
    
    // refund mode
    $refundMode = filter_input(INPUT_POST, 'refundmode-opt', FILTER_SANITIZE_STRING);
    $neftAccountId = $_REQUEST['neft-account']['neftAccountId'];
    $refundNeftAccountId = filter_var($neftAccountId, FILTER_SANITIZE_NUMBER_INT);
    
    if(in_array($articletype, $ReturnPickupExcludedArray) && $return_mode == 'pickup' ){
   /* if (($pickup_excluded == 'true' || $pickup_excluded == true )  && $return_mode == 'pickup') {*/
        $markup .= <<<HTML
        <li>This item can only be returned using self ship option. We regret the inconvenience caused.</li>
HTML;
        return array(
            'status' => 'ERROR' ,
            'markup' => $markup,
            'message' => 'This item is allowed to return using self ship only',
            'data'   => ''
        );
    }
    
    $response = array();
    $response['status'] = false;
    if (checkCSRF($token, "ajax_return")){
 		$response = func_add_new_return_request($orderid, $itemid, $option, $quantity,
            	$customer_login, $userid, $reasoncode, $details, $return_mode,
 				$selected_address_id,false,$pickupCourier, false, false,
 				$refundMode,$refundNeftAccountId);
    }
    if(!$response['status']){
    	$markup .= <<<HTML
        <li>There has been a technical glitch, because of which we are unable to process this request right now. We regret the inconvenience caused.</li>
        <li>Please visit "<a href="/mymyntra.php?view=myorders">My Myntra > My Orders</a>" and try again later.</li>
HTML;
    	return array(
	        'status' => 'ERROR',
	        'markup' => $markup,
	        'data'   => ''
	    );  
    }
    
    $data = array();
    $data['returnId'] = $response['returnid'];
    $data['orderId'] = $orderid;
    $data['itemId'] = $itemid;

    /* NOTE: uncomment the block to debug the POST, request and response params
    $request = compact('orderid', 'itemid', 'option', 'quantity',
            'customer_login', 'userid', 'reasoncode', 'details', 'return_mode', 'selected_address_id');
    $data['_post'] = $_POST;
    $data['request'] = $request;
    $data['response'] = $response;
    */

    $urlReturnForm = "/generatelabel.php?for=returndetails&id=" . $data['returnId'];
    $markup .= <<<HTML
<div class="step-confirm">
    <h3 class="gray">Your Return No.: <strong>{$data['returnId']}</strong>.</h3>
    <div class="success-msg">
        <div class="msg">
        	Please follow the steps below to prepare your returns package    
        </div>
    </div>
    <ol>
        <li>Download the &ldquo;<a href="{$urlReturnForm}" target="_blank">Returns Form</a>&rdquo;.
            Print it out or write down the particulars mentioned on it on a sheet of paper.</li>
        <li>Re-pack the item along with its original product packaging and accompanying tags in a tamper-proof packaging.</li>
        <li>Cut out or copy the return address provided at the bottom of the
            "<a href="{$urlReturnForm}" target="_blank">Returns Form</a>" and paste it on the Return bag so it is clearly visible.
            Print it out or write down the particulars mentioned on it on a sheet of paper.</li>
        <li>Insert the Returns Form into the Returns package, and seal it.</li>
HTML;
    if ($isSelfReturn) {
    $markup .= <<<HTML
        <li>Ship the package and update the shipping details on the
            "<a href="/mymyntra.php?view=myreturns" target="_blank">My Myntra > My Returns</a>"
            section so that the package can be tracked until it reaches us.</li>
        <li>Retain the receipt from your shipping vendor until you receive an email from us confirming that we’ve received your returns.</li>
HTML;
    }
    else {
    $markup .= <<<HTML
        <li>Expect a call from our shipping vendor within the next 24-48 hours for scheduling of the pickup of your return.</li>
HTML;
    }

    $markup .= <<<HTML
    </ol>
</div>
HTML;

    return array(
        'status' => 'SUCCESS',
        'markup' => $markup,
        'data'   => $data
    );
}
global $login;
if (empty($login)) {
    $resp = array(
        'status'  => 'ERROR',
        'message' => 'Please login'
    );
}
else {
    $view = $_REQUEST['_view'];
    if ($view == 'reason') {
        $resp = getReasonView();
    }
    else if ($view == 'confirm') {
        $resp = getConfirmView();
    }
    else if ($view == 'amt-refund') {
        $resp = getRefundAmount();
    }
    else {
        $resp = array(
            'status'  => 'ERROR',
            'message' => 'Invalid View'
        );
    }
}

header('Content-Type: application/json');
echo json_encode($resp);
exit;

