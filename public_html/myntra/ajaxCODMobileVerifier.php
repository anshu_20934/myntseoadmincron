<?php
include_once("../auth.php");
include_once \HostConfig::$documentRoot."/include/class/class.mymyntra.php";
include_once \HostConfig::$documentRoot."/include/func/func.sms_alerts.php";
include_once \HostConfig::$documentRoot."/include/func/func.utilities.php";
include_once \HostConfig::$documentRoot."/modules/coupon/mrp/SmsVerifier.php";

// max number of attempts to try verification code
define('MAX_ATTEMPT',2);

$login = $XCART_SESSION_VARS['login'];
$mymyntra = new MyMyntra(mysql_real_escape_string($login));
$userProfileData = $mymyntra->getUserProfileData();

$smsVerifier = SmsVerifier::getInstance();

// user account data
$mobile = $userProfileData['mobile'];
$login = $userProfileData['login'];

// input params
$mode = filter_input(INPUT_POST,'mode',FILTER_SANITIZE_STRING);
$code = filter_input(INPUT_POST,'v_code',FILTER_SANITIZE_NUMBER_INT);

if(checkCSRF($_POST["_token"], "ajaxCODMobileVerifier") && !empty($login) && !empty($mobile)){

    switch($mode){

        case "verify-mobile-code" :
            
            if (!empty($code)){

                $ret = $smsVerifier->verifyMobileCode($mobile, $code, $login);

                // if the 4 digit code is authentic
                if ($ret) {
                    $verStatus = $smsVerifier->verifyMapping($mobile, $login, $COD=true);

                    if ($verStatus) {
                        // on success, show success message, show button 'close'
                        $data['mode'] = $mode;
                        $data['status'] = "S";                        

                    } else {
                        // on error, show wrong account for mobile message
                        $data['mode'] = $mode;
                        $data['status'] = "F";
                    }

                } else {                   

                    // on error, show wrong code message
                    $data['mode'] = $mode;
                    $data['status'] = "wrong-code";
                }

            } else {
                // on error, show wrong code message
                $data['mode'] = $mode;
                $data['status'] = "empty-code";             
            }

            break;

        case "generate-code" :

            $code = rand(1000, 9999);
            $retval = $smsVerifier->addMobileCodeMapping($mobile, $code, $login);

            // to show resend option based on num_attempts
            $num_attempts = $smsVerifier->getAttempts($mobile, $login);
            $data['num_attempt'] = $num_attempts;

            // lock the mobile number when number of attempts reaches max threshold
            if($num_attempts == 2) {
                $smsVerifier->lockMobileLogin($mobile, $login);
            }

            if($retval == "showverifybtn")	{                
                $sms = "Your mobile verification code is $code. If not initiated by you, please ignore this message.";
                func_send_sms_cellnext($mobile, $sms);

                $smsVerifier->updateCODMobileVerifyToOngoing($mobile, $login);                

                // on success, show verify code box, show wait message(for 2 mins), change button 'verify code', show button 'cancel'
                $data['mode'] = $mode;
                $data['status'] = "S";


            } else if ($retval == "verifyFailed"){
                // on error, show max attemt exceed info, change button to 'close'
                $data['mode'] = $mode;
                $data['status'] = "F";

            } else {
                // Not giving an option of captcha in mobile verification for COD.
                if($retval == "showCaptcha"){
                    $data['mode'] = $mode;
                    $data['status'] = "F";
                }
            }
            
            break;
        
    }
    
} else {    
    $data['status'] = "F";
}

// MAX ATTEMPT
$data['max_attempt'] = MAX_ATTEMPT;

echo json_encode($data);
exit;
?>