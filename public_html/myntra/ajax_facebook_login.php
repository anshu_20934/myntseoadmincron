<?php

require_once "../auth.php";
include_once("../include/func/func.mkcore.php");
include_once("../include/func/func.db.php");
include_once("../include/func/func.mail.php");
include_once("../include/func/func.randnum.php");
include_once("../include/func/func.user.php");
include_once ("$xcart_dir/include/class/instrumentation/BaseTracker.php");
require_once("$xcart_dir/modules/Registration/fb_registration.php");
require_once($xcart_dir.'/include/sessionstore/session_store_wrapper.php');
include_once "$xcart_dir/include/func/func.loginhelper.php";

x_load('cart','crypt','user');
global $menuDisplayRefAmount;

$login = $XCART_SESSION_VARS['login'];
if(!empty($login)){
	//user is logged into myntra and clicking on FB Connect
	require_once($xcart_dir."/include/fb/facebook_login.php");
	$facebook_login = new Facebook_Login();
	$facebook = $facebook_login->facebook;
	x_session_register('fb_uid');
	$fb_uid = $facebook->getUser();
	if ($fb_uid) {
	  try {
	  	$userAlreadyPresent = func_query_first_cell("SELECT COUNT(*) FROM mk_facebook_user WHERE fb_uid='$fb_uid'");
    	if($userAlreadyPresent==0) {
	    		//make a entry into fb db
    		$entryQuery = "insert into mk_facebook_user (fb_uid) values ('$fb_uid')";
    		func_query($entryQuery);
    	}
    	$fb_me = $facebook->api('/me');
	   /* $multiQuery = "{ 
	  		'query1':'SELECT uid,first_name,last_name,name,birthday_date,sex,hometown_location,relationship_status,current_location,activities,interests,music,tv,movies,books,quotes,about_me,email,family FROM user WHERE uid =$fb_uid',
     		'query2':'SELECT uid,first_name,last_name,name,birthday_date,sex,hometown_location,current_location,family,relationship_status,activities,interests,music,tv,movies,books,quotes,about_me from user WHERE uid in (SELECT uid2 FROM friend WHERE uid1 = $fb_uid)'
     	}";
  
	  	$param = array(       
	     'method' => 'fql.multiquery',       
	     'queries' => $multiQuery,       
	     'callback' => '');       
		$queryresults = $facebook->api($param);
		
	  //$fb_data = $facebook->api(array("method"=>"fql.query","query"=>"SELECT uid,first_name,last_name,name,birthday_date,sex,hometown_location,relationship_status,current_location,activities,interests,music,tv,movies,books,quotes,about_me,email,family FROM user WHERE uid =$fb_uid"));
	    $fb_me = $queryresults[0]['fql_result_set'][0];
	    $fb_friends = $queryresults[1]['fql_result_set'];
	    $fb_likes = null;//$queryresults[2]['fql_result_set'];*/
	  } catch (FacebookApiException $e) {
	    error_log($e);
	  }
	}
	
	//$weblog->debug($fb_me);
		
	if($fb_me) {
		if(func_query_first_cell("SELECT COUNT(*) FROM $sql_tbl[customers] WHERE login='$login' and (firstname = '' or firstname is null)") > 0) {
			db_query("UPDATE $sql_tbl[customers] SET firstname='".mysql_real_escape_string($fb_me['first_name'])."' WHERE login = '$login'");
		}
		
		$birth_date = date_create_from_format('m/d/Y',$fb_me["birthday"]);
		$b_day = date_format($birth_date,'Y-m-d')?date_format($birth_date,'Y-m-d'):$birth_date;		
		db_query("UPDATE $sql_tbl[customers] SET 
			gender ='".mysql_real_escape_string($fb_me['gender'])."',
			DOB='".$b_day."'
			WHERE login = '$login'");
		if(func_query_first_cell("SELECT COUNT(*) FROM customer_personna WHERE login='$login'") > 0) {
			db_query("UPDATE customer_personna SET relationship_status ='".mysql_real_escape_string($fb_me['relationship_status'])."' WHERE login = '$login'");
		}
		else {
			db_query("insert into customer_personna(login,relationship_status) values('".$login."','".mysql_real_escape_string($fb_me['relationship_status'])."')");
		}			
		
		
		x_session_register('userfirstname');
		$userfirstname = $fb_me['first_name'];
		//***Login Expiry ticket
		//destroy the sxid    
    	if(x_session_is_registered("sxid"))
       		x_session_unregister("sxid");
     	setSecureSessionId();


		$fb_access_token = $facebook->getAccessToken();
    	$topass=array('fb_access_token'=>$fb_access_token);
		$topass['myntra_login'] = $login;
		FacebookRegistration::post_async(HostConfig::$selfHostURL."/include/fb/fb_database_update.php", $topass);
	 	//$facebook_login->insertOrUpdateFaceBookDB($fb_me,$fb_friends,$fb_likes,$login);
		$msg=array("status"=>'success',"username"=>$fb_me['first_name'],"fb_uid"=>$fb_uid,"refAmount"=>$menuDisplayRefAmount);
			
	} else {
		$msg=array("status"=>'error',"redirect"=>'',"str"=>'',"username"=>'',"toDisplay"=>'',"fb_uid"=>'');
	}
	
	echo json_encode($msg);
	exit;
}

$toDisplay=0;
if($_POST['fbRegistration']=="1"){
	//$weblog->debug(print_r($_POST,true));
	require_once("$xcart_dir/modules/Registration/fb_registration.php");
	try {
			$regObj = new FacebookRegistration(trim($_POST['passwd1']));
            if(!empty($_POST['mrp_referrer']))
            {
                $referrer=str_replace("%40","@",$_POST['mrp_referrer']);
                $regObj->setReferer($referrer);
            }
			$regObj->executeActions();
			if (x_session_is_registered("fb_uid")) {
        		$fb_uid = $XCART_SESSION_VARS['fb_uid'];
			}
			
			if (x_session_is_registered("userfirstname")) {
        		$userfirstname = $XCART_SESSION_VARS['userfirstname'];
			}
			$login = $XCART_SESSION_VARS['login'];
			$checkCODPaid = func_query_first_cell("select login from xcart_orders where login='$login' and payment_method='cod' and (cod_pay_status='paidtobluedart' or cod_pay_status='paid') limit 1");
    		x_session_unregister("oneClickCODElegible");
    		if(!empty($checkCODPaid)){
    			x_session_register("oneClickCODElegible","true");
    			$msg["oneClickCODElegible"] = "true";
    		} else {
    			x_session_register("oneClickCODElegible","false");
    			$msg["oneClickCODElegible"] = "false";
    		}
    				//***Login Expiry ticket
			//destroy the sxid    
    		if(x_session_is_registered("sxid"))
       			x_session_unregister("sxid");
    		setSecureSessionId();

			$sub_str=substr($userfirstname, 0, 12);
    		$logged_in_user=(strlen($userfirstname)>15)?$sub_str."...":$sub_str;
			$msg=array("status"=>'success',"redirect"=>'',"str"=>'',"username"=>$logged_in_user,"toDisplay"=>$toDisplay,"fb_uid"=>$fb_uid,"refAmount"=>$menuDisplayRefAmount);
	} catch (Exception $e){
			$msg=array("status"=>'error',"redirect"=>'',"str"=>'',"username"=>$logged_in_user,"toDisplay"=>$toDisplay,"fb_uid"=>$fb_uid);
	}
	echo json_encode($msg);
	exit;
}


global $sql_tbl;

$coupon = $_POST['rcoupon'];
$redirect_page = $_POST['menu_redirect_page'];
$refid = $_POST['refid'];
require_once($xcart_dir."/include/fb/facebook_login.php");
$facebook_login = new Facebook_Login();
$facebook = $facebook_login->facebook;
x_session_register('fb_uid');
$fb_uid = $facebook->getUser();
if ($fb_uid) {
	  $weblog->debug("FACEBOOK SESSION FB UID: $fb_uid");
	  try {
	  		
	    	$userAlreadyPresent = func_query_first_cell("SELECT COUNT(*) FROM mk_facebook_user WHERE fb_uid='$fb_uid'");
	    	if($userAlreadyPresent==0) {
	    		//make a entry into fb db
	    		$entryQuery = "insert into mk_facebook_user (fb_uid) values ('$fb_uid')";
	    		func_query($entryQuery);
	    	}
	  		/*$multiQuery = "{ 
		  		'query1':'SELECT uid,first_name,last_name,name,birthday_date,sex,hometown_location,relationship_status,current_location,activities,interests,music,tv,movies,books,quotes,about_me,email,family FROM user WHERE uid =$fb_uid',
	     		'query2':'SELECT uid,first_name,last_name,name,birthday_date,sex,hometown_location,current_location,family,relationship_status,activities,interests,music,tv,movies,books,quotes,about_me from user WHERE uid in (SELECT uid2 FROM friend WHERE uid1 = $fb_uid)'
	     	}";*/
	  
		  	//$param = array(       
		    // 'method' => 'fql.multiquery',       
		    //'queries' => $multiQuery,       
		    // 'callback' => '');       
			//$queryresults = $facebook->api($param);
			$fb_me = $facebook->api('/me');
			
			//$weblog->info(print_r($fb_me,true));
			//$fb_family = $facebook->api('/me/family');			
			
			
		  	//$fb_data = $facebook->api(array("method"=>"fql.query","query"=>"SELECT uid,first_name,last_name,name,birthday_date,sex,hometown_location,relationship_status,current_location,activities,interests,music,tv,movies,books,quotes,about_me,email,family FROM user WHERE uid =$fb_uid"));
		    //$fb_me = $queryresults[0]['fql_result_set'][0];
		    //$fb_friends = $queryresults[1]['fql_result_set'];
		    //$fb_likes = null;//$queryresults[2]['fql_result_set'];
		   // $weblog->debug("FACEBOOK REPLY: " . print_r($queryresults,true));
	  } catch (FacebookApiException $e) {
	  	$weblog->debug("FACEBOOK EXCEPTION");
	  	$weblog->debug(print_r($e,true));
	    error_log($e);
	    $msg=array("status"=>'error',"redirect"=>'',"str"=>'',"username"=>$logged_in_user,"toDisplay"=>$toDisplay,"fb_uid"=>$fb_uid);
		echo json_encode($msg); exit;
	  }
} else {
	$weblog->debug("NO FACEBOOK SESSION");
}

//$weblog->debug("FB_ME" .print_r($fb_me,true));

if($fb_me) {
	$fb_email = $fb_me["email"];
	x_session_register("userfirstname");
	$userfirstname = $fb_me["first_name"];
	
	if(func_query_first_cell("SELECT COUNT(*) FROM $sql_tbl[customers] WHERE login='$fb_email'") > 0) {
        x_session_register("account_type");
		$_curtime = time();
        #db_query("update mk_session set updated='Y' where sessionid= '$XCARTSESSID'");
		updateSessionUpdatedFlagBySessionId($XCARTSESSID, 'Y');
        db_query("UPDATE $sql_tbl[customers] SET last_login='$_curtime' WHERE login = '$fb_email'");
		//$weblog->debug("All IS WELL TILL HERE 1");
		if($userAlreadyPresent > 0) {
			//user already present and connected with fb we will update the user info now
			$toDisplay = '2';
		} else {
			//user is not present in the fb database show him pop dialog and make a entry in the db
			$toDisplay = '1';
		}
		
		$auto_login = true;
        $login = $fb_email;
        $login_type = "C";
        
        x_session_register("login",$login);
        x_session_register("login_type",$login_type);
        $logged = "";
        x_session_register("identifiers",array());
        $identifiers[$login_type] = array (
            'login' => $login,
            'firstname' => $userfirstname,
            'login_type' => $login_type
        );
        //***Login Expiry ticket
		//destroy the sxid    
    	if(x_session_is_registered("sxid"))
       		x_session_unregister("sxid");
     	setSecureSessionId();

        $flag = "S";
     	$referer = base64_decode($_POST['addreferer']);
     	$referer = urldecode($referer);
     	if(strpos($referer, "?")) $script = $referer."&success=I";
     	else $script = $referer."?success=I";

     	if(!empty($coupon))
     		$script .= "&c=1";

	     $str = $flag.",".$script;
	
	     if(!empty($redirect_page))
	        $str .= ",".$redirect_page;
	
	     if(!empty($coupon))
	          $str .= ",".$coupon;
	
	     if(!empty($refid))
	        $str .= ",".$refid;
    	$sub_str=substr($userfirstname, 0, 12);
    	$logged_in_user=(strlen($userfirstname)>15)?$sub_str."...":$sub_str;
    	
    	// check if the user has bought something before.
		$checkallreadybought = db_query("SELECT orderid, login from xcart_orders WHERE login = '$login'");
        $ab_numrows = db_num_rows($checkallreadybought);		
        $msg=array("status"=>'success',"redirect"=>'',"str"=>$str,"username"=>$logged_in_user,"toDisplay"=>$toDisplay,"fb_uid"=>$fb_uid,"refAmount"=>$menuDisplayRefAmount);
		if(empty($ab_numrows))	{
    		$msg["fbuyer"]='Yes';
    	} else	{ 
    		$msg["fbuyer"]='No';
    	}
				
        /**** Gopi: After login/signup the page is refreshed. So no need to send these data
		include_once "$xcart_dir/modules/coupon/database/CouponAdapter.php";
		$couponAdapter = CouponAdapter::getInstance();
		$mrp_header_message = $couponAdapter->getHeaderMessage($login);
		
    	$msg["hMesg"] = $mrp_header_message;
		$cashback_gateway_status = FeatureGateKeyValuePairs::getFeatureGateValueForKey('cashbackenabled');
		if($cashback_gateway_status == "internal") {
			$pos = strpos($login, "@myntra.com");
			if($pos === false)
			$cashback_gateway_status = "off";
			else
			$cashback_gateway_status = "on";
		}
		$msg["cashback"] = $cashback_gateway_status;
		
		$checkCODPaid = func_query_first_cell("select login from xcart_orders where login='$login' and payment_method='cod' and (cod_pay_status='paidtobluedart' or cod_pay_status='paid') limit 1");
	    x_session_unregister("oneClickCODElegible");
	    if(!empty($checkCODPaid)){
	    	x_session_register("oneClickCODElegible","true");
	    	$msg["oneClickCODElegible"] = "true";
	    } else {
	    	x_session_register("oneClickCODElegible","false");
	    	$msg["oneClickCODElegible"] = "false";
        }
        ****/
		
        //$weblog->debug("All IS WELL TILL HERE");
    	//$facebook_login->insertOrUpdateFaceBookDB($fb_me,$fb_friends,$fb_likes);
    	$fb_access_token = $facebook->getAccessToken();
    	$topass=array('fb_access_token'=>$fb_access_token);
    	FacebookRegistration::post_async(HostConfig::$selfHostURL."/include/fb/fb_database_update.php", $topass);
    	include_once \HostConfig::$documentRoot."/include/class/mcart/class.MCartUtils.php";
    	MCartUtils::fetchAndUpdateSessionCartVariables();
		//post_async("$http_location/include/fb/fb_database_update.php", $fb_session);
    	$weblog->debug("All IS WELL $fb_access_token");
    	echo json_encode($msg);
		
	} else {
		//user not registered make a entry in to the facebook database and show him the password entering form so will check what is his myntra account
		//$weblog->debug("All IS WELL TILL HERE 2");
		$checkMyntraUserid = db_query("SELECT myntra_login FROM mk_facebook_user WHERE fb_uid='$fb_uid'");
		$numMyntrarows = db_num_rows($checkMyntraUserid);
		$Myntrarow  = db_fetch_array($checkMyntraUserid);
		$login = $Myntrarow['myntra_login'];		
		if(!empty($login) && func_query_first_cell("SELECT COUNT(*) FROM $sql_tbl[customers] WHERE login='$login'") > 0) {
			//found a myntra user
			//$weblog->debug("All IS WELL TILL HERE 3");
			$_curtime = time();
			
	        #db_query("update mk_session set updated='Y' where sessionid= '$XCARTSESSID'");
			updateSessionUpdatedFlagBySessionId($XCARTSESSID, 'Y');
	        db_query("UPDATE $sql_tbl[customers] SET last_login='$_curtime' WHERE login = '$login'");
			
			$auto_login = true;
	        
	        $login_type = "C";
	        
	        x_session_register("login",$login);
	        x_session_register("login_type",$login_type);
	        $logged = "";
	        x_session_register("identifiers",array());
	        $identifiers[$menu_usertype] = array (
	            'login' => $login,
	            'firstname' => $userfirstname,
	            'login_type' => $login_type
	        );
					//***Login Expiry ticket
			//destroy the sxid    
    		if(x_session_is_registered("sxid"))
       			x_session_unregister("sxid");
     		setSecureSessionId();

	        $flag = "S";
	     	$referer = base64_decode($_POST['addreferer']);
	     	$referer = urldecode($referer);
	     	if(strpos($referer, "?")) $script = $referer."&success=I";
	     	else $script = $referer."?success=I";
	
	     	if(!empty($coupon))
	     		$script .= "&c=1";
	
		     $str = $flag.",".$script;
		
		     if(!empty($redirect_page))
		        $str .= ",".$redirect_page;
		
		     if(!empty($coupon))
		          $str .= ",".$coupon;
		
		     if(!empty($refid))
		        $str .= ",".$refid;
	    	$sub_str=substr($userfirstname, 0, 12);
	    	$logged_in_user=(strlen($userfirstname)>15)?$sub_str."...":$sub_str;

	    	$msg=array("status"=>'success',"redirect"=>'',"str"=>$str,"username"=>$logged_in_user,"toDisplay"=>$toDisplay,"fb_uid"=>$fb_uid,"refAmount"=>$menuDisplayRefAmount);
	    	// check if the user has bought something before.
			$checkallreadybought = db_query("SELECT orderid, login from xcart_orders WHERE login = '$login'");
       		$ab_numrows = db_num_rows($checkallreadybought);		
    		if(empty($ab_numrows))	{
    			$msg["fbuyer"]='Yes';
    		} else	{ 
    			$msg["fbuyer"]='No';
    		}	

            /**** Gopi: After login/signup the page is refreshed. So no need to send these data
			include_once "$xcart_dir/modules/coupon/database/CouponAdapter.php";
			$couponAdapter = CouponAdapter::getInstance();
			$mrp_header_message = $couponAdapter->getHeaderMessage($login);
		
    		$msg["hMesg"] = $mrp_header_message;
			$cashback_gateway_status = FeatureGateKeyValuePairs::getFeatureGateValueForKey('cashbackenabled');
			if($cashback_gateway_status == "internal") {
				$pos = strpos($login, "@myntra.com");
				if($pos === false)
				$cashback_gateway_status = "off";
				else
				$cashback_gateway_status = "on";
			}
			$msg["cashback"] = $cashback_gateway_status;

			$checkCODPaid = func_query_first_cell("select login from xcart_orders where login='$login' and payment_method='cod' and (cod_pay_status='paidtobluedart' or cod_pay_status='paid') limit 1");
		    x_session_unregister("oneClickCODElegible");
		    if(!empty($checkCODPaid)){
		    	x_session_register("oneClickCODElegible","true");
		    	$msg["oneClickCODElegible"] = "true";
		    } else {
		    	x_session_register("oneClickCODElegible","false");
		    	$msg["oneClickCODElegible"] = "false";
		    }
            ****/

			//$weblog->debug("All IS WELL 4");
	    	//$facebook_login->insertOrUpdateFaceBookDB($fb_me,$fb_friends,$fb_likes,$login);
	    	$fb_access_token = $facebook->getAccessToken();
    		$topass=array('fb_access_token'=>$fb_access_token);
	    	$topass['myntra_login'] = $login;
			FacebookRegistration::post_async(HostConfig::$selfHostURL."/include/fb/fb_database_update.php", $topass);
			include_once \HostConfig::$documentRoot."/include/class/mcart/class.MCartUtils.php";
			MCartUtils::fetchAndUpdateSessionCartVariables();
 	    	//$weblog->debug("All IS WELL 5");
	    	echo json_encode($msg);			
		} else {
			//user not present anywhere
			//$smarty->assign("fb_new_user", '1');
			$weblog->debug("Facebook Registration: User registering using facebook for " . $fb_email . "session ID = " .$XCARTSESSID);
			//if($_POST['click_from'] == "fbUploadImage"){
				//$weblog->debug("Registering a user when he clicks on profile pic upload");
				
				try {
				        $fb_me["mrp_referer"] = $_GET['ref'];
						$regObj = new FacebookRegistration(null,$facebook_login,$fb_me,$fb_friends,$fb_likes);
						$regObj->saveToDB();
						$regObj->sendEmail();
						//$regObj->sendPasswordResetEmail();
						$regObj->autoLogin();
						
						x_session_register("userfirstname");
						x_session_register("fb_uid",$fb_uid);
						
						$auto_login = true;
	        			$fb_firstName = $fb_me["first_name"];
			        	$_curtime = time();
				        #db_query("update mk_session set updated='Y' where sessionid= '$XCARTSESSID'");
			        	updateSessionUpdatedFlagBySessionId($XCARTSESSID, 'Y');
				        db_query("UPDATE xcart_customers SET last_login='$_curtime', first_login = '$_curtime' WHERE login = '$fb_email'");
				        $auto_login = true;
				        $login = $fb_email;
				        $login_type = "C";
				        $userfirstname = $fb_firstName;
				        
				        x_session_register("login",$login);
				        x_session_register("login_type",$login_type);
				        $logged = "";
				        x_session_register("identifiers",array());
				        $identifiers[$login_type] = array (
				            'login' => $login,
				            'firstname' => $userfirstname,
				            'login_type' => $login_type,
				        );
						//***Login Expiry ticket
						//destroy the sxid    
    					if(x_session_is_registered("sxid"))
       						x_session_unregister("sxid");
     					setSecureSessionId();

						$sub_str=substr($userfirstname, 0, 12);
			    		$logged_in_user=(strlen($userfirstname)>15)?$sub_str."...":$sub_str;

			    		
			    		
                        /**** Gopi: After login/signup the page is refreshed. So no need to send these data
						$cashback_gateway_status = FeatureGateKeyValuePairs::getFeatureGateValueForKey('cashbackenabled');
						if($cashback_gateway_status == "internal") {
							$pos = strpos($login, "@myntra.com");
							if($pos === false)
							$cashback_gateway_status = "off";
							else
							$cashback_gateway_status = "on";
						}
                        ****/

						$logged_in_user=$fb_email;
						$toDisplay =2;
						$msg=array("status"=>'success',"redirect"=>'',"str"=>'',"username"=>$logged_in_user,"toDisplay"=>$toDisplay,"fb_uid"=>$fb_uid,"fbuyer"=>'No',"refAmount"=>$menuDisplayRefAmount);
                        $msg['fb_action'] = 'signup'; // FB connect is used for signup

                        /**** Gopi: After login/signup the page is refreshed. So no need to send these data
						$msg["cashback"] = $cashback_gateway_status;
						include_once "$xcart_dir/modules/coupon/database/CouponAdapter.php";
						$couponAdapter = CouponAdapter::getInstance();
						$mrp_header_message = $couponAdapter->getHeaderMessage($login);
		
    					$msg["hMesg"] = $mrp_header_message;
						$checkCODPaid = func_query_first_cell("select login from xcart_orders where login='$login' and payment_method='cod' and (cod_pay_status='paidtobluedart' or cod_pay_status='paid') limit 1");
					    x_session_unregister("oneClickCODElegible");
					    if(!empty($checkCODPaid)){
					    	x_session_register("oneClickCODElegible","true");
					    	$msg["oneClickCODElegible"] = "true";
					    } else {
					    	x_session_register("oneClickCODElegible","false");
					    	$msg["oneClickCODElegible"] = "false";
					    }
                        ****/

						echo json_encode($msg); exit;
				} catch (Exception $e){
						$msg=array("status"=>'error',"redirect"=>'',"str"=>'',"username"=>$logged_in_user,"toDisplay"=>$toDisplay,"fb_uid"=>$fb_uid);
						echo json_encode($msg); exit;
				}
			//	echo json_encode($msg);
			//	exit;
			//}
	    	//$logged_in_user=$fb_email;
			//$toDisplay=3;
			//$msg=array("status"=>'success',"redirect"=>'',"str"=>'',"username"=>$logged_in_user,"toDisplay"=>$toDisplay,"fb_uid"=>$fb_uid);
			//echo json_encode($msg);
		}
	}
	
} else {
	$weblog->debug("FB_ME IS EMPTY");
	$msg=array("status"=>'error',"redirect"=>'',"str"=>'',"username"=>$logged_in_user,"toDisplay"=>$toDisplay,"fb_uid"=>$fb_uid);
	echo json_encode($msg); exit;
}



?>
