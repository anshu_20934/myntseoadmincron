<?php
define("QUICK_START", 1);
$show_additional_product_details=$_GET["showadditionaldetail"];
if($show_additional_product_details != 'y') {
	define("XCART_SESSION_START", 1);
}

include_once("../auth.php");

include_once("$xcart_dir/include/class/instrumentation/BaseTracker.php");
$tracker = new BaseTracker(BaseTracker::AJAX_SEARCH);

require_once($xcart_dir."/include/class/search/SearchProducts.php");
include_once($xcart_dir."/include/class/widget/class.widget.keyvaluepair.php");
use abtest\MABTest;

$ajaxSearchCacheTime=WidgetKeyValuePairs::getWidgetValueForKey("ajaxSearchCacheTime");
if($ajaxSearchCacheTime != NULL){
    $ajaxSearchCacheTime=trim(intval($ajaxSearchCacheTime));
}
else{
	$ajaxSearchCacheTime=0;
}


/*---------------block for registering the previous search by user-----------*/

function saveLastSearchPage($prev_page){
	$prev_page = urldecode($prev_page);
	
	global $XCART_SESSION_VARS;

	$uriComponents = parse_url($prev_page);
	
	$prev_page = $uriComponents["path"] ."?". $uriComponents["query"] ."#". $uriComponents["fragment"];

	if(!x_session_is_registered("last_search_page_1")){
		x_session_register("last_search_page_1", $prev_page);
	}
	else{
	
		$tmp_search_page = $XCART_SESSION_VARS["last_search_page_1"];
		$tmpUriComponents = parse_url($tmp_search_page);

		$tmp_search_page = $tmpUriComponents["path"] ."?". $tmpUriComponents["query"] ."#". $tmpUriComponents["fragement"];

		if($prev_page != $tmp_search_page){

			x_session_unregister("last_search_page_1");

			if($tmpUriComponents["path"] == $uriComponents["path"]){
				x_session_register("last_search_page_1", $prev_page);
			}
			else{
				x_session_unregister("last_search_page_2");
				x_session_register("last_search_page_1", $prev_page);
				x_session_register("last_search_page_2", $tmp_search_page);
			}
		}
	}
}

/*-------------------------------------------------------------------------*/


/*$json = '{
	"url" : "adidas-puma-nike",
	"gender" : ["men"],
	"sizes" : [],
	"brands" : ["adidas", "nike"],
	"pricerange" : {"minValue": 100, "maxValue": 5000},
	"sort-by" : "PRICEA",
	"attr_sleevelength" : [],
	"attr_collarlength" : []
}';*/

$json = $_GET["query"];
$getJson = $_GET["getjson"]==="true";

//saving last search page in session
saveLastSearchPage($_GET["prev_page"]);

$timeSearhStart = microtime(true);
$searchProducts=getFilteredResults($json);
$timeSearhFinish = microtime(true);
	
// just a one-off thing for data collection
$searchMetaDataForGAStats = array(  'searchType'   => 'UNDEF',
                                    'resultsCount' => 0,
                            );

if($searchProducts->is_parameteric_search == 'y') {
    $searchMetaDataForGAStats['searchType'] = 'PARAM';
} else {
	if ($searchProducts->query_level == 'FULL') {
        $searchMetaDataForGAStats['searchType'] = 'OR';
    } else {
        $searchMetaDataForGAStats['searchType'] = 'AND';
    }
}
$searchMetaDataForGAStats['resultsCount'] = $searchProducts->products_count;
$smarty->assign("searchMetaDataForGAStats", $searchMetaDataForGAStats);

$products=$searchProducts->products;
$smarty->assign("products",$products);

$filterParams = json_decode($json,true);
{
	//$tracker->searchData->setQueriedByUser($filterParams['filteredByUser'] ? true : false);
	$tracker->searchData->setFilters($json);
	$tracker->searchData->setNumOfResults($searchProducts->products_count);
	$tracker->searchData->setTimeTakenToSearch($timeSearhFinish - $timeSearhStart);
	$tracker->searchData->setSolrCallLatency($searchProducts->getSolrCallLatencies());
	$tracker->searchData->setSearchInitLatency($searchProducts->getInitLatency());


	if($searchProducts->products_count > 0)
	{
		$resultSet = array();
		//$tracker->searchData->
		$pos = $filterParams['page'] * $filterParams['noofitems'];
		foreach ($searchProducts->products as $searhResult)
		{
			array_push($resultSet, array("styleid" => $searhResult['styleid'], "pos" => ++$pos));
		}
		$tracker->searchData->setResultSet($resultSet);
	}
	$tracker->fireRequest();
}
// show addition product details
$usersToShowAdditionalDetails = WidgetKeyValuePairs::getWidgetValueForKey('showStyleDetailsToUser');
if(empty($usersToShowAdditionalDetails)){
	$smarty->assign("show_additional_product_details",'n');
}else{
	$user_list=explode(",", $usersToShowAdditionalDetails);
	if(in_array($login, $user_list)){
		$smarty->assign("show_additional_product_details",'y');
	}
}
$html="";

$smarty->assign("nav_id",(int)$nav_id);
//AB Test for Colour Options
$colourOptionsVariant = MABTest::getInstance()->getUserSegment('ListPageColourOptions');
$smarty->assign("colourOptionsVariant", $colourOptionsVariant);

/**
 * Start of style grouping currently data is based on colour group
 */
$styleIdGroup = array(); //style ids to be searched for style groupping
$styleIdGroupKey = array(); //style ids already present in $products

foreach ($products as $key => &$val) {
    $pos = stripos($val["product"], $val["brands_filter_facet"]);
    $val["product"] = trim(substr($val["product"], $pos+strlen($val["brands_filter_facet"]), strlen($val["product"])));
    if ($colourOptionsVariant == 'test') {
        $styleIdsArray = explode(",", $val['style_group']);
        $styleIdGroup = array_merge($styleIdGroup, $styleIdsArray);
        $styleIdGroupKey[] = $val['styleid'];
    }
}

unset($val);

if ($colourOptionsVariant == 'test') {
    //remove duplicates and already solr searched styles from $styleIdGroup
    $styleIdGroup = array_filter(array_unique($styleIdGroup));//list of style ids which belong to a group
    $styleIdGroupDiff = array_diff($styleIdGroup, $styleIdGroupKey);//list of style ids for which solr data need to be fetched

    $solrSearchProducts = SearchProducts::getInstance();
    $stylesArray = @$solrSearchProducts->getMultipleStyleFormattedSolrData($styleIdGroupDiff);

    //creat a separat array for group styles
    $solrStyles = array();
    foreach ($stylesArray as $style) {
        $solrStyles[$style['styleid']] = $style;
    }

    $styleIdGroupDiff = array_diff($styleIdGroup, $styleIdGroupDiff);
    foreach($products as $style) {
        if(in_array($style['styleid'], $styleIdGroupDiff)) {
            $solrStyles[$style['styleid']] = $style;
        }
    }

    foreach($products as $key => &$val) {
        $styleIdsArray = explode(",", $val['style_group']);
        $val['colourVariantsData'] = array();
        foreach ($styleIdsArray as $styleId) {
            if ($styleId != $val['styleid']) {
                $style = $solrStyles[$styleId];
                if(!empty($style)) {
                    $articletype = $style['global_attr_article_type'];
                    $brandname = $style['global_attr_brand'];
                    $stylename = $style['stylename'];
                    $styleid = $style['styleid'];
                    $landingpageurl = construct_style_url($articletype,$brandname,$stylename,$styleid);
                    $val['colourVariantsData'][$styleid] = array(
                        "global_attr_base_colour" => $style['global_attr_base_colour'],
                        "global_attr_colour1" => $style['global_attr_colour1'],
                        "landing_page_url" => $landingpageurl,
                        "search_image" => $style['search_image']
                    );
                }
            }
        }
    }
}

unset($val);
/**
 * End of style grouping
 */

$smarty->assign("products",$products);
$smarty->assign("current_page",$searchProducts->offset);
$html= "";
if($getJson){
		header('Content-type: application/json');
		$json = array($products);
}else if($skin === "skinmobile"){
	$html= $smarty->fetch("search/search-list.tpl");
}
else{
	$html= $smarty->fetch("inc/product_unit.tpl");
}

$result=array();
$result["html"]=$html;
$result["data"]=$json;
$result["skin"]=$skin;
$result["page_count"]=$searchProducts->getTotalPages();
$result["product_count"]=$searchProducts->products_count;
$result["current_page"]=($searchProducts->offset);
$result["productuiABparam"]=$productuiABparam;
$result["filters"]=$searchProducts->faceted_search_results_new;
$result["price_rage_details"]=$searchProducts->price_rage_details;
$result["query_level"] = $searchProducts->query_level;
global $_MABTestObject;
$showStickySize = $_MABTestObject->getUserSegment("stickySize");
if ($showStickySize == "test") {
	$result["fpcSizeGroup"] = $searchProducts->fpcSizeGroup;
}

if($ajaxSearchCacheTime>0){
	$expires = 60*$ajaxSearchCacheTime;
	header("Pragma: public");
	header("Cache-Control: max-age=".$expires);
	header('Expires: ' . gmdate('D, d M Y H:i:s', time()+$expires) . ' GMT');
}
else {
	header("Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
	header("Pragma: no-cache");
}
echo json_encode($result);


function getFilteredResults($json) {
	$qparams = translateJsonToQueryParams($json);
	$getStatsInstance=false;
	$json_new = json_decode($json);
	$ajaxSearchProducts = new SearchProducts($qparams['original_query'],null,$getStatsInstance);
	$offsetCoefficient = getMyntraCookie('productPosition');
	$ajaxSearchProducts->setOffsetCoefficient($offsetCoefficient);
	$ajaxSearchProducts->getAjaxQueryResult($json_new->parameter_query,$qparams['ajaxFilterString'], $qparams['sort_field'], $qparams['page'], $qparams['noofitems'],$qparams['boosted_query'],$json_new->query_level);
	$ajaxSearchProducts->fixImagePaths($ajaxSearchProducts->products, $qparams['original_query']);
	
	/**
	 * sticky size updation, update if only 1 size group is selected
	 */
	global $_MABTestObject;
	$showStickySize = $_MABTestObject->getUserSegment("stickySize");
	if ($showStickySize == "test") {
		$paramJsonDecode = json_decode($json, 1);
		$sizeGroupArray = $paramJsonDecode["size_group"];
		global $XCART_SESSION_VARS;
		$userEmailId = $XCART_SESSION_VARS['login'];
		$fpcSizeGroup = $XCART_SESSION_VARS['fpcSizeGroup'];
		//update/insert the sticky size info in db and cookie
		if (count($sizeGroupArray) == 1) {
			$sizeArray = $paramJsonDecode["sizes"];
			
			$sizeGroupKey = $sizeGroupArray[0];
			$sizeCSV = mysql_real_escape_string(implode(":", $sizeArray));
			$savedSizes = array();
			$savedSizes[$sizeGroupKey] = $sizeCSV;
			
			//set the session for saved sizes here for both logged in and logged out user
			$fpcSizeGroup = updateFpcCookieVal($fpcSizeGroup, $savedSizes);
			global $XCART_SESSION_VARS;
			x_session_unregister('fpcSizeGroup',true);
			x_session_register('fpcSizeGroup', "$fpcSizeGroup");
			//if the user is logged in then save it in the db also
			if($userEmailId != '') {
				stickySizeSave($userEmailId, $sizeGroupKey, $sizeCSV);
			}
		}
		
		$ajaxSearchProducts->fpcSizeGroup = $fpcSizeGroup;
	}
	
	return $ajaxSearchProducts;
}

/**
 * function to update the fpc cookie for a given size group and selected sizes
 * @param array $sizes : selected/saved sizes of the size group colon separated e.g. array("shoe_size" => "1:2:3")
 */
function updateFpcCookieVal($fpcSizeGroup, $sizesSaved) {
	//handle the boudary cases
	if(empty($sizesSaved)) {
		if($fpcSizeGroup == '') {
			$fpcSizeGroup = "$fpcSizeGroup|";
			$fpcSizeGroup = preg_replace('/(\|){2,}/', '|', $fpcSizeGroup);
		}
		return $fpcSizeGroup;
	}
	
	foreach ($sizesSaved as $sizeGroup => $sizes) {
		$pattern = '/'.$sizeGroup.'[^\|]*(\|){0,1}/';
		$replacement = $sizeGroup.'='.$sizes;
		
		//we remove the old value and add update one at the end
		$fpcSizeGroup = preg_replace($pattern, '', $fpcSizeGroup);
		if($fpcSizeGroup != "") {
			$fpcSizeGroup = $fpcSizeGroup."|".$replacement;
			$fpcSizeGroup = preg_replace('/(\|){2,}/', '|', $fpcSizeGroup);
		} else {
			$fpcSizeGroup = $replacement;
		}
	}	
		
	return $fpcSizeGroup;
}

/**
 * function to update the sticky size for a given size group for a given user
 * @param string $userEmailId : email id of logged in user
 * @param string $sizeGroup : size group name e.g. shoe_size
 * @param string $sizeCSV : different sizes selected by user colon separated
 */
function stickySizeSave($userEmailId, $sizeGroup, $sizeCSV) {
	$savedStickySizes = func_query("SELECT * FROM sticky_size_search WHERE login = '$userEmailId' AND  size_group = '$sizeGroup'", TRUE);
	global $weblog; $weblog->debug("sticky only : size_group = $sizeGroup $sizeCSV");
	if(!empty($savedStickySizes)) {
		//update the data in db
		$updateData = array("sizes_csv" => $sizeCSV);
		$whereData = array("login" => $userEmailId, "size_group" => $sizeGroup);
		func_array2updateWithTypeCheck("sticky_size_search", $updateData, $whereData);
	} else {
		//insert the data in db
		$insertData = array("login" => $userEmailId, "size_group" => $sizeGroup, "sizes_csv" => $sizeCSV);
		func_array2insertWithTypeCheck("sticky_size_search", $insertData);
	}
}

function translateJsonToQueryParams($json)
{
	$uiToSolrSchema = Array (
						"url" => Array("type"=> "original_query", "fieldname" => "original_query"),
						"boosted_query" => Array("type"=> "boosted_query", "fieldname" => "boosted_query"),
						"gender" => Array("type"=> "csv", "fieldname" => "global_attr_gender"),
						//"gender" => Array("type"=> "csv", "fieldname" => "fit_filter"),
						"sizes" => Array("type"=> "csv", "fieldname" => "sizes"),
						"brands" => Array("type"=> "csv", "fieldname" => "brands_filter_facet"),
						"articletypes" => Array("type"=> "csv", "fieldname" => "global_attr_article_type_facet"),
						"colour" => Array("type"=> "csv", "fieldname" => "colour_family_list"),
						"pricerange" => Array("type"=> "range", "fieldname" => "discounted_price"),
						"discountpercentage" => Array("type"=> "lowerrange", "fieldname" => "discount_percentage"),
						"sortby" => Array("type"=> "sort_field", "fieldname" => "sort_field"),
						"page" => Array("type"=> "page", "fieldname" => "page"),
						"noofitems" => Array("type"=> "noofitems", "fieldname" => "noofitems"),
						"offer" => Array("type"=> "csv", "fieldname" => "dre_offerType"),
						);
	global $weblog;
	$uiObj = json_decode($json);

	$queryString = "";
	$original_query = "";
	$sort_field = "";
	$page_num=0;
	$noofitems=24;
	
	foreach ($uiObj as $key=>$value)
	{
		if(isset($value) && !empty($value))
		{
			if(key_exists($key, $uiToSolrSchema))
			{
				if($uiToSolrSchema[$key]['type'] == "csv")
				{
					$queryString .= " +" . $uiToSolrSchema[$key]['fieldname'] . ":(";
					$cnt = 0;
					foreach ($value as $subValue)
					{
                    $subValue=str_replace("~","",$subValue);
						if($cnt == 0)
							$queryString .= "\"" . $subValue . "\"";
						else
							$queryString .= " OR \"" . $subValue . "\"";
						$cnt++;
					}
					$queryString .= ")";
				}
				else if($uiToSolrSchema[$key]['type'] == "range")
				{
					$queryString .= " +discounted_price:[" . $value->rangeMin . " TO " . $value->rangeMax ."]";
				}
				else if($uiToSolrSchema[$key]['type'] == "lowerrange")
				{
					$queryString .= " +" . $uiToSolrSchema[$key]['fieldname'] . ":[" . $value . " TO * ]";
				}
				else if($uiToSolrSchema[$key]['type'] == "original_query")
                {
                    // @paranoids
                    // the initial sanitized search query is carried to the javascript land as is (refer the search.tpl)
                    // it is decoded here and used *only* by the solr
					$original_query = html_entity_decode($value, ENT_QUOTES);
				}
				else if($uiToSolrSchema[$key]['type'] == "boosted_query")
				{
					$boosted_query = $value;
				}
				else if($uiToSolrSchema[$key]['type'] == "sort_field")
				{
					$sort_field = $value;
				}else if($uiToSolrSchema[$key]['type'] == "page")
				{
					$page_num = $value;
				}
				else if($uiToSolrSchema[$key]['type'] == "noofitems"){
					$noofitems = $value;
				}
			}
			else if(strpos($key, "_article_attr"))
			{
					$queryString .= " +" . $key . ":(";
				$cnt = 0;
				foreach ($value as $subValue)
				{
                    $subValue=str_replace("~","",$subValue);
					if($cnt == 0)
						$queryString .= "\"" . $subValue . "\"";
					else
						$queryString .= " OR \"" . $subValue . "\"";
					$cnt++;
				}
				$queryString .= ")";
			}
		}

	}

	$retval = Array('ajaxFilterString' => $queryString, 'sort_field' => $sort_field, 'original_query' => $original_query, 'page' => $page_num, 'noofitems' => $noofitems,'boosted_query' => $boosted_query,);
	//$weblog->info("afiltered query = $queryString");
	return $retval;
}
