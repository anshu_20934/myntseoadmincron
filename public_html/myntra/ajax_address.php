<?php

//define("QUICK_START", 1);
//define("XCART_SESSION_START", 1);

require_once(dirname(__FILE__)."/../auth.php");
require_once $xcart_dir."/include/func/func.courier.php";
require_once $xcart_dir."/include/class/class.mymyntra.php";
include_once "$xcart_dir/include/class/mcart/class.MCart.php";
include_once "$xcart_dir/include/func.func.utilities.php";

use style\StyleGroup;
use style\builder\CachedStyleBuilder;
use style\builder\StyleBuilder;

function getAddressList() {
    global $smarty, $login, $XCART_SESSION_VARS;

    $selAddrId = $XCART_SESSION_VARS['selected_address'];
    $customerSupportCall = WidgetKeyValuePairs::getWidgetValueForKey('customerSupportCall');
    $customerSupportTime = WidgetKeyValuePairs::getWidgetValueForKey('customerSupportTime');

    $itemid = filter_var($_REQUEST['itemid'], FILTER_SANITIZE_STRING);
    if($itemid && !empty($itemid)) {
    	$itemResult = func_query_first("Select style_id, sku_id from mk_order_item_option_quantity oq, mk_styles_options_skus_mapping som where oq.optionid = som.option_id and oq.itemid = $itemid", true);
    	$cachedStyleBuilderObj = CachedStyleBuilder::getInstance();
    	$styleObj = $cachedStyleBuilderObj->getStyleObject($itemResult['style_id']);
    	$productOptionDetails = $styleObj->getActiveProductOptionsData();
    	$availableInWarehouses = false;
    	foreach ($productOptionDetails as $options) {
    		if(!empty($options['availableInWarehouses'])) {
    			if(!$availableInWarehouses)
    				$availableInWarehouses = explode(",", $options['availableInWarehouses']);
    			else
    				$availableInWarehouses = array_merge($availableInWarehouses, explode(",", $options['availableInWarehouses']));
    		}
    	}
    	$availableInWarehouses = array_values(array_unique($availableInWarehouses));
    }
    
    $type = filter_var($_REQUEST['_type'], FILTER_SANITIZE_STRING);
    $addresses_all = func_select_query('mk_customer_address', array('login' => $login), 0, 20,
            array('datecreated' => 'desc'));
    $addresses = array();
    $state_codes = array();
    foreach($addresses_all as $key => &$address) {
        if ($address['country'] != 'IN') {
            continue;
        }
        $address['country'] = 'India';
        if ($type == 'return') {
            $pickupCouriers = func_get_pickup_couriers($address['pincode'], $itemResult['style_id'], $itemResult['sku_id']);
            if($pickupCouriers && !empty($pickupCouriers)) {
        		$address['is_servicable'] = true;
        		$address['pickupCourier'] = $pickupCouriers[0];
            } else {
            	$address['is_servicable'] = false;
            }
        }
        else if ($type == 'shipping') {
        	$mcartFactory= new MCartFactory();
        	$myCart= $mcartFactory->getCurrentUserCart();
        	if($myCart != null) {
        		$productsInCart = MCartUtils::convertCartToArray($myCart);
        		$skuDetails = array();
        		foreach($productsInCart as $index=>$product) {
        			$productsInCart[$index]['style_id'] = $product['productStyleId'];
        			$productsInCart[$index]['sku_id'] = $product['skuid'];
        			$skuDetails[$product['skuid']] = array('id'=>$product['skuid'], 'availableItems'=> $product['availableItems']);
        			if(!empty($product['availableInWarehouses'])) {
        				foreach(explode(",", $product['availableInWarehouses']) as $whId)
        					$skuDetails[$product['skuid']]['warehouse2AvailableItems'][$whId] = 1;
        			}
        		}
        		$paymentModes = getServiceablePaymentModesForProducts($address['pincode'], $productsInCart, false, false, $skuDetails);
        		$address['is_servicable'] = (!empty($paymentModes) && count($paymentModes)>0)?true:false;
        	} else {
            	$address['is_servicable'] = is_zipcode_servicable_delivery($address['pincode']);
        	}
        }
        else if($type == 'exchange'){
        	$sku_id = func_query_first_cell("Select sku_id from mk_product_options po, mk_styles_options_skus_mapping som where po.id = som.option_id and po.is_active = 1 and po.style = $itemResult[style_id]", true);
            $itemResult['sku_id'] = $sku_id;
        	$address['is_servicable'] = func_check_exchange_serviceability_products($address['pincode'], array($itemResult), $availableInWarehouses);
        }
        else if($type == 'billing') {
            $address['is_servicable'] = true;
        }
        else {
            $address['is_servicable'] = false;
        }
        $addresses[(string)$address['id']] = $address;
        $state_codes[] = $address['state'];
    }
    unset($address);

    $states = array();
    $sql = "SELECT state, code FROM xcart_states WHERE code IN ('" . implode("','", $state_codes) . "')";
    $rows = func_query($sql, true);
    foreach ($rows as $row) {
        $states[$row['code']] = $row['state'];
    }

    $idxDefault = -1;
    foreach ($addresses as &$address) {
        $address['state'] = $states[$address['state']];
    }
    unset($address);



    $sorted = array();
    // move the selected address (from session) to the top
    if (!empty($selAddrId)) {
        foreach($addresses as $key => $val) {
            if ($selAddrId == $key) {
                $sorted[$key] = $val;
                unset($addresses[$key]);
                break;
            }
        }
    }

    $decodedAddress = array();

    foreach($addresses as $key => $val) {
        $decodedAddress[$key] = $val;
        foreach($val as $key1 => $val1) {
            $decodedAddress[$key][$key1] = html_entity_decode($val1);
        }
    }

    $addresses = $decodedAddress;

    // move the default address to top
    foreach($addresses as $key => $val) {
        if ($val['default_address'] == '1') {
            $sorted[$key] = $val;
            unset($addresses[$key]);
            break;
        }
    }
    // copy the rest of the items
    foreach($addresses as $key => $val) {
        $sorted[$key] = $val;
    }
    $addresses = $sorted;

    $smarty->assign('type', $type);
    $smarty->assign('addresses', $addresses);
    return array(
        'status'    => 'SUCCESS',
        'addresses' => $addresses,
        'ids'       => array_keys($addresses),
        'count'     => count($addresses),
        'selAddrId' => $XCART_SESSION_VARS['selected_address'],
        'customerSupportTime' => $customerSupportTime,
        'customerSupportCall' => $customerSupportCall
    );
}

function getAddressForm() {
    global $smarty, $skin, $login, $abLocality;

    $customerSupportCall = WidgetKeyValuePairs::getWidgetValueForKey('customerSupportCall');
    $customerSupportTime = WidgetKeyValuePairs::getWidgetValueForKey('customerSupportTime');

    $type = filter_var($_REQUEST['_type'], FILTER_SANITIZE_STRING);
    $action = filter_var($_REQUEST['_action'], FILTER_SANITIZE_STRING);

    $userData = func_select_first('xcart_customers', array('login' => $login));
    $firstname = $userData['firstname'];
    $lastname = $userData['lastname'];
    $promptForName = empty($firstname);

    $states = func_get_states($country);
    $username = $lastname != "" ? $firstname . " " . $lastname : $firstname;
    $smarty->assign("username", $username);
    $smarty->assign("promptForName", $promptForName);
    $smarty->assign("states", $states);

    if ($action == 'edit') {
        $id = filter_var($_REQUEST['id'], FILTER_SANITIZE_NUMBER_INT);
        $addr = func_select_first('mk_customer_address', array('login' => $login, 'id' => $id));
    }
    else {
        $addr = array(
            'id' => 0,
            'name' => $username,
            'yourname' => '',
            'address' => '',
            'locality' => '',
            'city' => '',
            'state' => '',
            'pincode' => '',
            'mobile' => $userData['mobile']
        );
    }
    $smarty->assign('addr', $addr);

    $markup = '';
    if ($_REQUEST['markup'] == 'yes') {
        $markup = $skin == 'skin1'
            ? func_display('site/address_form.tpl', $smarty, false)
            : $smarty->fetch('inc/address-form.tpl');
    }
    $resp = array(
        'status'    => 'SUCCESS',
        'markup'    => $markup,
        'data'      => $addr,
        'promptForName' => $promptForName,
        'customerSupportTime' => $customerSupportTime,
        'customerSupportCall' => $customerSupportCall
    );

    return $resp;
}

function postAddressForm() {
    global $smarty, $login, $abLocality ,$userToken;
    if (!checkCSRF($_POST["_token"], "ajax_address")) {
        return array(
            'status'  => 'ERROR',
            'message' => 'Invalid access'
        );
    }

    $errors = array();
    $userData = func_select_first('xcart_customers', array('login' => $login));
    $firstname = $userData['firstname'];
    $promptForName = empty($firstname);

    $type = filter_var($_REQUEST['_type'], FILTER_SANITIZE_STRING);
    $action = filter_var($_REQUEST['_action'], FILTER_SANITIZE_STRING);

    $id = filter_input(INPUT_POST,'id', FILTER_SANITIZE_NUMBER_INT);
    $name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);
    $yourname = filter_input(INPUT_POST, 'yourname', FILTER_SANITIZE_STRING);
    $locality = filter_input(INPUT_POST, 'locality', FILTER_SANITIZE_STRING);
    $selectedLocality = filter_input(INPUT_POST, 'selected-locality', FILTER_SANITIZE_STRING);
    $city = filter_input(INPUT_POST, 'city', FILTER_SANITIZE_STRING);
    $country = filter_input(INPUT_POST, 'country', FILTER_SANITIZE_STRING);
    $state = filter_input(INPUT_POST, 'state', FILTER_SANITIZE_STRING);
    $pincode = filter_input(INPUT_POST, 'pincode', FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => '/^\d{6}$/')));
    $mobile = filter_input(INPUT_POST, 'mobile', FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => '/^\d{10}$/')));
    $copyRecipientName = filter_input(INPUT_POST,'copy-name', FILTER_SANITIZE_STRING);
    $itemid = filter_var($_REQUEST['itemid'], FILTER_SANITIZE_STRING);
    
    
    $address = $_POST['address'];
    $address = preg_replace(array('/&(\w+|#\d+);/', '/[ \t]+/', '/(\n|\r|\r\n)+/'), array('', ' ', "\n"), $address);
    $address = filter_var($address, FILTER_SANITIZE_STRING);

    // remove all the html entities and special chars except dot, comma and slash
    // regex to match html entities (like &#39; &amp; &#x2019;)
    $rexEntities = '/\&(#x?\d+|[a-z]+);/U';
    // regex to match all special chars except dot, comma and slash
    //$rexChars = '/[#`~\^&\*\(\)\-\+\{\}\[\]\\\|;:\'"\><@!%\$]+/';
    //whitelist characters #^*()_-+{[}]\|;:/.>,< @ ~$\ [a-z][A-Z][0-9] as per LIVE-249
    $rexChars = '/[^\w\d\s#\^~\*\(\)\-\+\{\}\[\]\\\|;:\/\.>,<@\$]+/';
    foreach (array('name', 'yourname', 'address', 'locality', 'selectedLocality', 'city', 'state') as $var) {
        $$var = iconv("UTF-8", "ASCII//TRANSLIT", $$var);
        $$var = preg_replace($rexEntities, '', $$var);
        $$var = preg_replace($rexChars, '', $$var);
    }
    
    $query = "SELECT * FROM mk_pincode_city_state WHERE pincode = '$pincode'";
    $rows = func_query($query, true);
    foreach ($rows as $row) {
        $cities[] = $row['city'];
        $states[] = $row['state'];
    }
    $cities = array_map('strtolower', $cities);
    $states = array_map('strtolower', $states);

    foreach (array('name', 'address', 'city', 'state', 'pincode') as $var) {
        if (empty($_POST[$var])) {
            $errors[$var] = 'Please enter ' . $var;
        } else{
            $showError = false;
            if($var == 'city' || $var == 'state') {
                if(!in_array(strtolower($$var),$cities) && !in_array(strtolower($$var),$states)) {
                    $showError = true;
                    $errors['locality'] = 'Please enter valid locality';
                }
            }
            if(empty($$var)) {
                $errors[$var] = 'Please enter valid input';
            }
        }
    }

    if (empty($_POST['mobile']) || empty($mobile)) {
    	$errors['mobile'] = 'Please enter a valid 10 digit mobile number';
    }
    if (strlen($address) > 500) {
        $errors['address'] = 'Address can not exceed more than 500 characters';
    }

    if ($abLocality == 'yes') {
        if (empty($_POST['locality']))
            $errors['locality'] = 'Please enter locality';
        else if (empty($locality))
            $errors['locality'] = 'Please enter valid locality';
    }

    if (!empty($promtForName)) {
        if (empty($_POST['yourname']))
            $errors['yourname'] = 'Please enter your name';
        else if (empty($yourname))
            $errors['yourname'] = 'Please enter valid name';
    }

    if ($country != 'IN') {
        $errors['country'] = 'Please choose India';
    }

    if($itemid && !empty($itemid)) {
    	$itemResult = func_query_first("Select style_id, sku_id from mk_order_item_option_quantity oq, mk_styles_options_skus_mapping som where oq.optionid = som.option_id and oq.itemid = $itemid", true);
    	$cachedStyleBuilderObj = CachedStyleBuilder::getInstance();
    	$styleObj = $cachedStyleBuilderObj->getStyleObject($itemResult['style_id']);
    	$productOptionDetails = $styleObj->getActiveProductOptionsData();
    	$availableInWarehouses = false;
    	foreach ($productOptionDetails as $options) {
    		if(!empty($options['availableInWarehouses'])) {
    			if(!$availableInWarehouses)
    				$availableInWarehouses = explode(",", $options['availableInWarehouses']);
    			else
    				$availableInWarehouses = array_merge($availableInWarehouses, explode(",", $options['availableInWarehouses']));
    		}
    	}
    	$availableInWarehouses = array_values(array_unique($availableInWarehouses));
    }
    if (!empty($pincode)) {
        if ($type == 'return') {
        	$couriers = func_get_pickup_couriers($pincode, $itemResult['style_id'], $itemResult['sku_id']);
            if(count($couriers)==0 || empty($couriers)){
            	$is_servicable = false;
            } else {
            	$is_servicable = true;
            	$pickupCourier = $couriers[0];
            }
        }
        else if ($type == 'shipping') {
            $is_servicable = is_zipcode_servicable_delivery($pincode);
        }
        else if ($type == 'exchange') {
        	$sku_id = func_query_first_cell("Select sku_id from mk_product_options po, mk_styles_options_skus_mapping som where po.id = som.option_id and po.is_active = 1 and po.style = $itemResult[style_id]", true);
        	$itemResult['sku_id'] = $sku_id;
        	$is_servicable = func_check_exchange_serviceability_products($pincode, array($itemResult),$availableInWarehouses);
        }
        if (!$is_servicable) {
		    if($type == 'return'){
		            $errors['pincode'] = 'We\'re sorry. We currently do not pickup returns from this location.Please Ship the return to us';
		    }else if($type == 'exchange'){
		            $errors['pincode'] = 'We\'re sorry. We currently do not service exchange at this location';
		    }else{
			    $errors['pincode'] = 'Currently shipment is not possible in this pincode area. ';
			    $errors['pincode'] .= 'We are in the process of expanding our network and apologize for the inconvenience.';
		    }
        }
    }

    if (!empty($errors)) {
        return array(
            'status'  => 'ERROR',
            'message' => 'Data validation failed',
            'errors'  => $errors
        );
    }

    $pinerr = (int)filter_input(INPUT_POST, 'pinerr', FILTER_SANITIZE_NUMBER_INT);
    $bitPinErrState = empty($pinerr) ? 0 : 1;
    $bitPinErrLocality = (!empty($locality) && ($locality == $selectedLocality)) ? 0 : 1;
    $errmask = "b'" . $bitPinErrLocality . $bitPinErrState . "'";

    $mymyntra = new MyMyntra(mysql_real_escape_string($login));
    $data = array(
        'name' => $name,
        'address' => $address,
        'locality' => $locality,
        'city' => $city,
        'state' => $state,
        'country' => $country,
        'pincode' => $pincode,
        'email' => $email,
        'mobile' => $mobile,
        'phone' => '',
        'datecreated' => date("Y-m-d H:i:s"),
        'errmask' => array("value"=>$errmask, "type"=>"binary")
    );

    if (!empty($id)) {
        $data['id'] = $id;
        $actionResult = $mymyntra->updateAddress($data);
    }
    else {
        $actionResult = $mymyntra->addAddress($data);
        if (empty($actionResult[MyMyntra::$ERROR])) {
            $data['id'] = $actionResult['id'];
            if ($copyRecipientName) {
                $yourname = $name;
            }
            $namearr = MyMyntra::splitFirstLastName($yourname);
            $firstname = $namearr[0];
            $lastname = $namearr[1];
            if(!empty($firstname)) {
                $dataProfile = array('firstname' => $firstname, 'lastname' => $lastname);
                $actionResult = array_merge($actionResult, $mymyntra->updateProfile($dataProfile));
            }
        }
    }

    if (empty($actionResult[MyMyntra::$ERROR])) {
        unset($data['datecreated']);
        unset($data['errmask']);
        $sql = "SELECT state, code FROM xcart_states WHERE code = '$state'";
        $row = func_query_first($sql, true);
        $data['state_code'] = $row['code'];
        $data['state'] = $row['state'];
        $data['country'] = 'India';
        $data['is_servicable'] = true;
        if($pickupCourier && !empty($pickupCourier))
        	$data['pickupCourier'] = $pickupCourier;
        return array(
            'status'  => 'SUCCESS',
            'data'    => $data,
            '_action'  => empty($id) ? 'create' : 'edit'
        );
    }
    else {
        return array(
            'status'  => 'SUCCESS',
            'message' => $actionResult[MyMyntra::$ERROR]
        );
    }
}

function deleteAddress() {
    global $smarty, $login;
    if (!checkCSRF($_POST["_token"], "ajax_address")) {
        return array(
            'status'  => 'ERROR',
            'message' => 'Invalid access'
        );
    }
    $mymyntra = new MyMyntra(mysql_real_escape_string($login));
    $id = mysql_real_escape_string(filter_input(INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT));
    if (empty($id)) {
        return array(
            'status'  => 'ERROR',
            'message' => 'Insufficient data to complete the action'
        );
    }
    $actionResult = $mymyntra->deleteAddress($id);
    if (isset($actionResult[MyMyntra::$ERROR])) {
        return array(
            'status'  => 'ERROR',
            'message' => $actionResult[MyMyntra::$ERROR]
        );
    }
    else {
        return array(
            'status'  => 'SUCCESS',
            'id'      => $id
        );
    }
}

function setDefaultAddress() {
    global $smarty, $login;
    if (!checkCSRF($_POST["_token"], "ajax_address")) {
        return array(
            'status'  => 'ERROR',
            'message' => 'Invalid access'
        );
    }
    $mymyntra = new MyMyntra(mysql_real_escape_string($login));
    $id = mysql_real_escape_string(filter_input(INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT));

    if (empty($id)) {
        return array(
            'status'  => 'ERROR',
            'message' => 'Insufficient data to complete the action'
        );
    }
    $actionResult = $mymyntra->setDefaultAddress($id);
    if (isset($actionResult[MyMyntra::$ERROR])) {
        return array(
            'status'  => 'ERROR',
            'message' => $actionResult[MyMyntra::$ERROR]
        );
    }
    else {
        return array(
            'status'  => 'SUCCESS',
            'id'      => $id
        );
    }
}


if (empty($login)) {
    $resp = array(
        'status'  => 'ERROR',
        'message' => 'Please login'
    );
}
else {
    $view = $_REQUEST['_view'];
    if ($view == 'address-list') {
        $resp = getAddressList();
    }
    else if ($view == 'address-form') {
        $resp = ($_SERVER['REQUEST_METHOD'] == 'GET') ? getAddressForm() : postAddressForm();
    }
    else if ($view == 'address-set-default') {
        $resp = setDefaultAddress();
    }
    else if ($view == 'address-delete') {
        $resp = deleteAddress();
    }
    else {
        $resp = array(
            'status'  => 'ERROR',
            'message' => 'Invalid View'
        );
    }
}

header('Content-Type: application/json');
echo json_encode($resp);
exit;

