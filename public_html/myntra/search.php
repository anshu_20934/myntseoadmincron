<?php
@include_once "./auth.php";
@include_once "../auth.php";
require_once($xcart_dir.'/include/solr/solrProducts.php');
require_once($xcart_dir.'/include/solr/solrCategorySearch.php');
require_once($xcart_dir.'/include/solr/solrStatsSearch.php');
require_once($xcart_dir.'/include/solr/solrQueryBuilder.php');
require_once($xcart_dir.'/include/solr/solrUtils.php');
require_once($xcart_dir."/include/func/func.core.php");
require_once($xcart_dir."/include/func/func_seo.php");
require_once($xcart_dir."/include/func/func.utilities.php");
require_once($xcart_dir."/include/navigation.php");
require_once($xcart_dir."/include/class/search/SearchProducts.php");
require_once($xcart_dir."/tracking/trackingUtil.php");
require_once 'myntra/navhighlighter.php';
include_once($xcart_dir.'/env/Host.config.php');
include_once($xcart_dir."/include/class/instrumentation/BaseTracker.php");
include_once($xcart_dir."/myntra/setShowStyleDetails.php");
include_once($xcart_dir."/include/class/seo/SeoAdminCache.php");
include_once("$xcart_dir/include/class/widget/class.widget.keyvaluepair.php");

use enums\pageconfig\PageConfigSectionConstants;
use enums\pageconfig\PageConfigLocationConstants;
use enums\pageconfig\PagePageConfigSectionConstants;
use pageconfig\manager\PageConfigManager;
use pageconfig\manager\CachedBannerManager;
use abtest\MABTest;
use seo\SeoStrategy;
use seo\SeoPageType;
use seo\SeoHashUtil;
use web\utils\DeviceDetector;


use macros\PageMacros;
use imageUtils\dao\JpegminiDBAdapter;

$pageName = 'search';
include_once $xcart_dir."/webengage.php";
include_once $xcart_dir."/gtm.php";

/*---------------block for registering the previous search by user-----------*/

function saveLastSearchPage($prev_page){
    $prev_page = urldecode($prev_page);

    global $XCART_SESSION_VARS;

    $uriComponents = parse_url($prev_page);
    
    $prev_page = $uriComponents["path"] ."?". $uriComponents["query"] ."#". $uriComponents["fragement"];

    if(!x_session_is_registered("last_search_page_1")){
        x_session_register("last_search_page_1", $prev_page);
    }
    else{
    
        $tmp_search_page = $XCART_SESSION_VARS["last_search_page_1"];
        $tmpUriComponents = parse_url($tmp_search_page);
        
        if($prev_page != $tmp_search_page){

            x_session_unregister("last_search_page_1");

            if($tmpUriComponents["path"] == $uriComponents["path"]){
                x_session_register("last_search_page_1", $prev_page);
            }
            else{
                x_session_unregister("last_search_page_2");
                x_session_register("last_search_page_1", $prev_page);
                x_session_register("last_search_page_2", $tmp_search_page);
            }
        }
    }
}

/*-------------------------------------------------------------------------*/

/*
require_once("$xcart_dir/include/phpab.php");
$sortOrderABTesting = new phpab("sortorder", FALSE); // set the name of the a/b test
$sortOrderABTesting->set_ga_slot('5');

$testVar=ABTestingVariables::getTestVariables('sortorder');
foreach ($testVar['variations'] as $variation){
	$sortOrderABTesting->add_variation_with_probability($variation['name'],'',$variation['p_prob']);
}
//echo "User Segment: ".$sortOrderABTesting->get_user_segment();
*/
$smarty->assign("searchRecentWidgetEnabled", FeatureGateKeyValuePairs::getBoolean('search.recentWidgetEnabled', true));

$jsonSearchData = array();
// Atlas data for SEO purpose
$isORSearch   = false;
$isNoIndexed  = false;
$isSynonym    = false;
$isParametrised = false;
$parametrisedDesc = '';
$cannonicalPageUrl = '';

if(!(count($nav_arr) > 1)){
    $smarty->assign("showSearchInHeader", true);//Show serch box in header
}

$offsetCoefficient = getMyntraCookie('productPosition');
if(!isset($offsetCoefficient)){
	$offsetCoefficient = rand(0,100);
	setMyntraCookie('productPosition', $offsetCoefficient, time() + 900, "/",$cookiedomain); // Add timout cookie
}else{ // Refresh the timout cookie's expires time	
	setMyntraCookie('productPosition', $offsetCoefficient, time() + 900, "/",$cookiedomain); // Add timout cookie
}

$shownewui= $_MABTestObject->getUserSegment("productui");
$smarty->assign("shownewui", $shownewui);
$abinfinitescroll= $_MABTestObject->getUserSegment('abinfinitescroll');
$smarty->assign("searchpagescrolltype",$abinfinitescroll);

//AB Test for Search Page Scroll
$smarty->assign("scrollLockVariant", MABTest::getInstance()->getUserSegment('ListScrollLock'));

//AB Test for Colour Options
$colourOptionsVariant = MABTest::getInstance()->getUserSegment('ListPageColourOptions');
$smarty->assign("colourOptionsVariant", $colourOptionsVariant);
$jsonSearchData["colourOptionsVariant"] = $colourOptionsVariant;

//AB Test for Search Optimization
$optimVariant = MABTest::getInstance()->getUserSegment('SearchOptim');
$smarty->assign("optimEnabled", $optimVariant == 'test' ? 'true' : 'false');

include_once("$xcart_dir/include/class/class.recent_viewed_content.php");
$__RECENT_HISTORY_DCONTENT->setGACategory("lp_strip");
$__RECENT_HISTORY_DCONTENT->genContent();
$__DCONTENT_MANAGER->startCapture();
$getStatsInstance=false;

$timeSearchStart = microtime(true);
$searchMetaDataForGAStats = array(  'searchType'   => 'UNDEF',
                                    'resultsCount' => 0
                                 );

$searchProducts = new SearchProducts(null,null,$getStatsInstance);
$searchProducts->setOffsetCoefficient($offsetCoefficient);

$weblog->info("original searcch phrase :: $searchProducts->original_phrase in file ".__FILE__);
$weblog->info("searcch phrase :: $searchProducts->search_phrase in file ".__FILE__);
if (RequestUtils::getRequestType() == RequestType::$PARAMETRIZED || RequestUtils::getRequestType() == RequestType::$LANDING_PAGE){
    $isParametrised = true;
    $parametrisedDesc = RequestUtils::getLandingPageURL(); 
	$searchProducts->execute_parametrized();
	$smarty->assign("parameter_query",$searchProducts->getFinalQuery());
	$smarty->assign("boosted_query",$searchProducts->getBoostedQuery());
    if($searchProducts->is_parameteric_search == 'y'){
	    $smarty->assign("generatelink","y");
    }
    $searchMetaDataForGAStats['searchType'] = 'PARAM';
}else{
    $searchProducts->execute_new();
    if ($searchProducts->query_level == 'FULL') {
        $searchMetaDataForGAStats['searchType'] = 'OR';
        $smarty->assign("nofollowForSearchFilterLinks",true);
        $jsonSearchData["nofollowForSearchFilterLinks"] = true;
        
    } else {
        $searchMetaDataForGAStats['searchType'] = 'AND';
    }
}
$searchMetaDataForGAStats['resultsCount'] = $searchProducts->products_count;

/**
 * set article type and size group mapping
 * required for sticky size
 * store the data in xcache
 */
global $xcache;
if($xcache==null){
	$xcache = new XCache();
}
$articleTypeSizeGroupMapKey = 'article_type_size_group_map';
$cachedATSGMap = $xcache->fetchObject($articleTypeSizeGroupMapKey);
if($cachedATSGMap == NULL) {
	$articleTypeSizeGroupMapResult = func_query('SELECT DISTINCT(a.typename), b.groupName FROM mk_catalogue_classification a, mk_articletype_size_group_mapping b WHERE a.id = b.article_type', TRUE);
	$cachedATSGMap = array();
	foreach ($articleTypeSizeGroupMapResult as $articleTypeSizeGroupMap) {
		$cachedATSGMap[$articleTypeSizeGroupMap['typename']] = $articleTypeSizeGroupMap['groupName'];
	}
	$xcache->storeObject($articleTypeSizeGroupMapKey, $cachedATSGMap, 0);
}
$smarty->assign('articleTypeSizeGroupMap', $cachedATSGMap);
$jsonSearchData['articleTypeSizeGroupMap'] = $cachedATSGMap;

// identify the main, sub and sub-sub categories for re-marketing
$solrKeyValues = array();
$solrQuery = $searchProducts->getFinalQuery();

preg_match('/\(.*\)/', $solrQuery, $matches);   $solrQuery = $matches[0];
$solrQuery = preg_replace('/\s+AND\s+\(count_options_availbale:\[.*\]\)/i','',$solrQuery);
preg_match('/\((.*)\)/', $solrQuery, $matches); $sQuery = $matches[1];

if (!empty($sQuery)) {
    if (($subClauses = explode(' AND ', $sQuery)) !== false) {
        foreach ($subClauses as $clause) {
            $parts = explode(':', $clause);
            preg_match('/\((.*)\)/',$parts[1],$matches); $value = $matches[1];

            if (isset($solrKeyValues[$parts[0]])) {
                // ignore multiple instances of same clause: ambiguous
                //TBDWL: To-Be-Dealt-With-Later 
                $solrKeyValues[$parts[0]] = "123456";
            } else {
                $solrKeyValues[$parts[0]] = $value;
            }
        } 
    }

   //gender
     if (($v = $solrKeyValues['global_attr_age_group']) && isset($v) && $v != "123456") {
        $parts = explode(' OR ', $v);
        if ($parts[0] == $v) {
            preg_match('/\"(.*)\"/', $parts[0], $matches); $v = $matches[1];
            $parts = explode('-', $v); $gender = $parts[1];
        } else {
            $genderChoices = array();
            foreach ($parts as $ageGroup) {
                preg_match('/\"(.*)\"/', $ageGroup, $matches); $v = $matches[1];
                $parts = explode('-', $v); $gender = $parts[1];
                if ($gender != 'Unisex') {
                    array_push($genderChoices,$gender);
                }
            }
        }
        if (count($genderChoices) == 1) {
            $gender = $genderChoices[0];
        } else {// don't send this one. Ambiguous and therefore not useful
            $gender = '';
        }
     }

    $isBrandSet = false;
    $isArticleTypeSet = false;
    $isCategorySet = false;
    if (($v = $solrKeyValues['brands_filter_facet']) && isset($v) && $v != "123456") {
        $isBrandSet = true;

        $parts = explode(' OR ', $v);
        if ($parts[0] == $v) {
            preg_match('/\"(.*)\"/', $parts[0], $matches); $v = $matches[1];
            $brandName = $v;
        } else {    //try the article type then
            $isBrandSet = false;
        }

		if ($isBrandSet && !empty($brandName)) {
			$smarty->assign("isBrandSet", $isBrandSet);
			$smarty->assign("brandName", $brandName);
		}
    }
    if (!$isBrandSet && ($v = $solrKeyValues['global_attr_article_type_facet']) && isset($v) && $v != "123456") {
        $isArticleTypeSet = true;

        $parts = explode(' OR ', $v);
        // if set to just one, forward it ..
        if ($parts[0] == $v) {
            preg_match('/\"(.*)\"/', $parts[0], $matches); $v = $matches[1];
            $articleType = $v;
        } else { // else dig and pass the sub-category parent
            $pos = rand(0, count($parts)-1);
            preg_match('/\"(.*)\"/', $parts[$pos], $matches); $articleType = $matches[1];
        }

        //dig out the master category
        $masterCategory = $xcache->fetchAndStore(function($articleType){
            $sql = "select b.typename from mk_catalogue_classification a, mk_catalogue_classification b
                    where a.typename='".$articleType."' and b.id=a.parent2";
	        if (($v = func_query_first_cell($sql)) !== false) {
                return $v;
            }
            return null;
            
        }, array($articleType), "$articleType-masterCategory", 86400);//1 day cache as this mapping changes very rarely

        if ($isArticleTypeSet && !empty($articleType) && !empty($masterCategory) && !empty($gender)) {
		    $smarty->assign("isArticleTypeSet", $isArticleTypeSet);
		    $smarty->assign("articleType", $gender.'-'.$articleType);
		    $smarty->assign("masterCategory", $gender.'-'.$masterCategory);
        }
    } else if (( $v = $solrKeyValues['global_attr_master_category']) && isset($v) && $v != "123456") {
        $isCategorySet = true;

        $parts = explode(' OR ', $v);
        if ($parts[0] == $v) {
            preg_match('/\"(.*)\"/', $parts[0], $matches); $v = $matches[1];
            $masterCategory = $v;
        } else {    //alphabetical bias broken by randomness
            $pos = rand(0, count($parts)-1);
            preg_match('/\"(.*)\"/', $parts[$pos], $matches);
            $masterCategory = $matches[1];
        }
        if ($isCategorySet && !empty($masterCategory) && !empty($gender)) {
		    $smarty->assign("isCategorySet", $isCategorySet);
		    $smarty->assign("masterCategory", $gender.'-'.$masterCategory);
        }
    }
}

$timeSearchFinish = microtime(true);
/* fix for google index and bounce rate:
 * when there is no search results show http status as 404 rather than 200.
 */
$noOfProducts =  count($searchProducts->products);
if($noOfProducts<1){
	header("HTTP/1.0 404 Not Found");
}

$searchProducts->fixImagePaths($searchProducts->products, $_GET['query']);


/**
 * specifics on this list of products
 */
$filtersAndCounts = (array)$searchProducts->faceted_search_results_new;
$productListData = array();
$filterKeyDataLayerMap = array (
    'global_attr_master_category_facet'       => array (  'list' => 'searchCategoryList',
                                                    'top' => 'searchTopCategory'),
    'global_attr_sub_category_facet'    => array (  'list' => 'searchSubCategoryList',
                                                    'top' => 'searchTopSubCategory'),
    'brands_filter_facet'               => array (  'list' => 'searchBrandList',
                                                    'top' => 'searchTopBrand'),
    'global_attr_gender'                => array (  'list' => 'searchGenderList',
                                                    'top' => 'searchTopGender'),
    'global_attr_article_type_facet'    => array (  'list' => 'searchArticleTypeList',
                                                    'top' => 'searchTopArticleType'),
    'colour_family_list'                => array (  'list' => 'searchColorList',
                                                    'top' => 'searchTopColor'),
    'sizes_facet'                       => array (  'list' => 'searchSizeList',
                                                    'top' => 'searchTopSize'),
);


/***** PORTAL-3192 - Remove Recursive link from category filters *****/

// Author: Gopi
// Date: 08.08.2013

// feature gate to control the noindex of article type filter url based on the below values.
// true : continue the wrong way of concating to generate the url, but add the noindex
// false: generate the url correctly, and remove the noindex
$seoNoindexEnabled = FeatureGateKeyValuePairs::getBoolean("seo.noindex.recurse.category");
// $seoNoindexEnabled = false;

// stop sending the whole solr data to the templates.
// instead, create the proper data models.

// starting with the article types filter
$filters = new stdclass;

// strip out the quotes
$squery = str_replace("'", "", html_entity_decode($_GET['query'], ENT_QUOTES));
$squery = filter_var($squery, FILTER_SANITIZE_STRING, array('flags' => FILTER_FLAG_STRIP_LOW|FILTER_FLAG_STRIP_HIGH));

// search query: transform spaces to hypens
$squery = strtolower(str_replace(' ', '-', $squery));
// error_log('eee squery: ' . $squery);

// split the search query into words
$qwords = explode('-', $squery);
// error_log('eee qwords: ' . print_r($qwords, true));

// filter: article types
$articleTypes = array();
$kvPairs = (array)$filtersAndCounts['global_attr_article_type_facet'] ;

// flag to denote if the query string contains any of article type
$squeryHasName = false;

// create the data map for article types
foreach ($kvPairs as $key => $val) {
    $name = trim($key);
    $count = $val;

    // normalize: convert spaces into hypens, and lowercase
    $nname = strtolower(str_replace(' ', '-', $name));

    // split the name into words
    $nwords = explode('-', $nname);
    // error_log('eee nwords: ' . print_r($nwords, true));

    array_push($articleTypes, array(
        'name'  => $name,
        'count' => $count,
        'nname' => $nname,
        'css'   => preg_replace('/\W/', '', $name) . '-at-filter',
        'sizeGroup' => $cachedATSGMap[$name],
        'noindex' => false
    ));

    // if search words intersects with article type words, there will be repetitions, if we concat the same
    // so, set the flag accordingly, to avoid that situation.
    if (!$squeryHasName) {
        $iwords = array_intersect($qwords, $nwords);
        // error_log('eee iwords: ' . print_r($iwords, true));
        $squeryHasName = (count($iwords) > 0);
    }
}

if ($squeryHasName) {
    $dwords = array_diff($qwords, $iwords);
    // error_log('eee dwords: ' . print_r($dwords, true));
    if (count($dwords) == 1) {
        // if there is only one non-intersected word from the search query (may be it is a brand/gender), use it as prefix
        $prefix = $dwords[0] . '-';
    }
    else {
        // otherwise, avoid any prefixes
        $prefix = '';
    }
}
else {
    // now it is ok to join the search query and article type. bcoz, the two will be different type (for example, brand+atype)
    $prefix = $squery . '-';
}

// create the proper url for the article types
foreach ($articleTypes as &$atype) {
    if ($seoNoindexEnabled) {
        // continue the *wrong* way of making url by joining the search query and article type
        // and stop the bots from indexing
        $atype['url'] = $squery . '-' . $atype['nname'];

        // deny the bot from indexing
        $atype['noindex'] = true;
    }
    else {
        // use the correct prefix
        $atype['url'] = $prefix . $atype['nname'];

        // allow the bot to index
        $atype['noindex'] = false;
    }
}

// bcoz, it is used as reference in the above foreach
unset($atype);

$filters->articleTypes = $articleTypes;
$smarty->assign('filters', $filters);

// determine whether we need to set the noindex meta tag for *wrong* page url
// so, what is wrong page? if the page url contains any repetitons.
// examples: adidas-adidas, casual-shoes-flats-casual-shoes

// regex check for single word repetitons or multiple words repetitions
// $etaTagNoindex = (preg_match("/([a-z-]+)-\g1/", $squery) || preg_match("/([a-z-]+)-.*-\g1/", $squery));
if (preg_match("/([a-z-]{3,})-\g1/", $squery, $matches)) {
    $metaTagNoindex = 1;
    // error_log('eee metaTagNoindex = 1, single word match ' . print_r($matches, true));
}
else if (preg_match("/([a-z-]{3,})-.*-\g1/", $squery, $matches)) {
    $metaTagNoindex = 1;
    // error_log('eee metaTagNoindex = 1, multi words match ' . print_r($matches, true));
}
else {
    $metaTagNoindex = 0;
    // error_log('eee metaTagNoindex = 0');
}
$smarty->assign('metaTagNoindex', $metaTagNoindex);

/***** /PORTAL-3192 *****/


$dominantGroup = array();
foreach ($filterKeyDataLayerMap as $filterKey => $map) {
    if (array_key_exists($filterKey, $filtersAndCounts)) {
        $countsMap = (array)$filtersAndCounts[$filterKey];

        //to extract the key with maxCount out
        reset(arsort($countsMap));
        $productListData[$filterKeyDataLayerMap[$filterKey]['top']] = preg_replace('/(\s+)/','-', strtolower(key($countsMap)));

        //specificity
        if (count($countsMap) == 1) {
            $dominantGroup[$filterKey] = true;
        }

        //populate keys
        $groupList = array();
        foreach ($countsMap as $name => $count) {
            array_push($groupList, preg_replace('/(\s+)/','-', strtolower($name)));
        }
        $productListData[$filterKeyDataLayerMap[$filterKey]['list']] = implode(',',$groupList);
    }
}
$smarty->assign('listGroup', 'NA');
if (!empty($dominantGroup)) {
    if (isset($dominantGroup['brands_filter_facet'])) {
        $smarty->assign('listGroup', 'brand');
    } else if (isset($dominantGroup['global_attr_article_type_facet'])) {
        $smarty->assign('listGroup', 'articleType');
    } else if (isset($dominantGroup['global_attr_sub_category_facet'])) {
        $smarty->assign('listGroup', 'subCategory');
    } else if (isset($dominantGroup['global_attr_master_category_facet'])) {
            $smarty->assign('listGroup', 'category'); 
    }
}
$smarty->assign("productListData", $productListData);

// collecting page specifics ends here


$products=$searchProducts->products;

/**
 * Start of style grouping currently data is based on colour group
 */
$styleIdGroup = array(); //style ids to be searched for style groupping
$styleIdGroupKey = array(); //style ids already present in $products

foreach ($products as $key => &$val) {
    $pos = stripos($val["product"], $val["brands_filter_facet"]);
    $val["product"] = trim(substr($val["product"], $pos+strlen($val["brands_filter_facet"]), strlen($val["product"])));
    if ($colourOptionsVariant == 'test') {
        $styleIdsArray = explode(",", $val['style_group']);
        $styleIdGroup = array_merge($styleIdGroup, $styleIdsArray);
        $styleIdGroupKey[] = $val['styleid'];
    }
}

unset($val);

if ($colourOptionsVariant == 'test') {
    //remove duplicates and already solr searched styles from $styleIdGroup
    $styleIdGroup = array_filter(array_unique($styleIdGroup));//list of style ids which belong to a group
    $styleIdGroupDiff = array_diff($styleIdGroup, $styleIdGroupKey);//list of style ids for which solr data need to be fetched

    $solrSearchProducts = SearchProducts::getInstance();
    $stylesArray = @$solrSearchProducts->getMultipleStyleFormattedSolrData($styleIdGroupDiff);

    //creat a separat array for group styles
    $solrStyles = array();
    foreach ($stylesArray as $style) {
        $solrStyles[$style['styleid']] = $style;
    }

    $styleIdGroupDiff = array_diff($styleIdGroup, $styleIdGroupDiff);
    foreach($products as $style) {
        if(in_array($style['styleid'], $styleIdGroupDiff)) {
            $solrStyles[$style['styleid']] = $style;
        }
    }

    foreach($products as $key => &$val) {
        if ($optimVariant == 'test') {
            $val['search_image'] = $val['searchimagepath'];
        }
        $styleIdsArray = explode(",", $val['style_group']);
        $val['colourVariantsData'] = array();
        foreach ($styleIdsArray as $styleId) {
            if ($styleId != $val['styleid']) {
                $style = $solrStyles[$styleId];
                if(!empty($style)) {
                    $articletype = $style['global_attr_article_type'];
                    $brandname = $style['global_attr_brand'];
                    $stylename = $style['stylename'];
                    $styleid = $style['styleid'];
                    $landingpageurl = construct_style_url($articletype,$brandname,$stylename,$styleid);
                    $val['colourVariantsData'][$styleid] = array(
                        "global_attr_base_colour" => $style['global_attr_base_colour'],
                        "global_attr_colour1" => $style['global_attr_colour1'],
                        "landing_page_url" => $landingpageurl,
                        "dre_landing_page_url" => $landingpageurl,
                        "search_image" => $style['search_image']
                    );
                }
            }
        }
    }
}

unset($val);
/**
 * End of style grouping
 */

$macros = new PageMacros('SEARCH');
$macros->page_type = SeoPageType::SEARCH_PAGE;
$macros->page_name = 'Search';
$macros->url = currentPageURLWithStripedQueryStringsAndPort();
$macros->url = preg_replace("/-{2,}/","-",$macros->url);
$macros->url = preg_replace("/^-{1,}/","",$macros->url);
$macros->search_string = implode(' ', array_unique(explode('-', trim(strtolower($_GET['query'])))));
$temp_macro_search_string = $macros->search_string;
$macros->category = (count(explode(',', $productListData['searchCategoryList'])) == 1) ? $productListData['searchTopCategory'] : '';
str_replace($macros->category, '', $temp_macro_search_string);
$macros->sub_category = count(explode(',', $productListData['searchSubCategoryList'])) == 1 ? $productListData['searchTopSubCategory'] : '';
str_replace($macros->sub_category, '', $temp_macro_search_string);
$macros->article_type = count(explode(',', $productListData['searchArticleTypeList'])) == 1 ? $productListData['searchTopArticleType'] : '';
str_replace($macros->article_type, '', $temp_macro_search_string);
$macros->brand = count(explode(',', $productListData['searchBrandList'])) == 1 ? $productListData['searchTopBrand'] : '';
str_replace($macros->brand, '', $temp_macro_search_string);
$macros->gender = count(explode(',', $productListData['searchGenderList'])) == 1 ? $productListData['searchTopGender'] : '';
str_replace($macros->gender, '', $temp_macro_search_string);
//Figure out the colour user is searching for
$macros->colour = count(explode(',', $productListData['searchColorList'])) == 1 ? $productListData['searchTopColor'] : '';
if($macros->colour == '' && count(explode(',', $productListData['searchColorList'])) > 1){
    if(in_array($productListData['searchTopColor'], explode(" ",$temp_macro_search_string))){
        $macros->colour = $productListData['searchTopColor'];
    }
}
str_replace($macros->colour, '', $temp_macro_search_string);

//Figure out the size user is searching for
$macros->size = count(explode(',', $productListData['searchSizeList'])) == 1 ? $productListData['searchTopSize'] : '';
if($macros->size == '' && count(explode(',', $productListData['searchSizeList'])) > 1){
    if(in_array($productListData['searchTopSize'], explode(" ",$temp_macro_search_string))){
        $macros->size = $productListData['searchTopSize'];
    }
}
//str_replace($macros->size, '', $temp_macro_search_string);

$seoEnabled=FeatureGateKeyValuePairs::getBoolean('SeoV3Enabled');
if($seoEnabled){
    try {
        $handle = Profiler::startTiming("seov3_load");
        $seoStrategy = new SeoStrategy($macros);
        $seoh1 = $seoStrategy->getH1Title();
        $rule  = $seoStrategy->getRule();
        preg_match("/.*?(com|in)\/(.*)/",$rule,$matches);  
        $ruleKeyword  = $matches[2];
        Profiler::endTiming($handle);
        $smarty->assign("seoStrategy",$seoStrategy); 
    } catch (Exception $e) {
        $seolog->error("search :: ".$e->getMessage());
    }
}

$price_range_details=$searchProducts->price_range_details;
$discount_percentage_details=$searchProducts->discount_percentage_details;
$smarty->assign("discountPercentageBreakup",$discount_percentage_details->counts);
$smarty->assign("discountPercentageBreakupCount", count((array)$discount_percentage_details->counts));
$smarty->assign("rangeMin",$price_range_details["rangeMin"]);
$smarty->assign("rangeMax",$price_range_details["rangeMax"]);
$smarty->assign("priceBreakup",$price_range_details["breakUps"]);
$smarty->assign("products", $products);
$smarty->assign("sortMethod",$searchProducts->sort_param);
$smarty->assign("query_level",$searchProducts->query_level);
$smarty->assign("paginator",$searchProducts->pagination);
$smarty->assign("pg",$searchProducts->offset);
$smarty->assign("page",filter_input(INPUT_GET, 'page', FILTER_SANITIZE_STRING));
$smarty->assign("navigation_url",$http_location."/");
$smarty->assign("search_phrase_in_url",$searchProducts->search_phrase);
$smarty->assign("noofitems",$searchProducts->limit);
$smarty->assign("first_item", $first_page+1);
$smarty->assign("last_item", min($first_page + $searchProducts->limit, $searchProducts->products_count));

$total_items_in_search = $searchProducts->products_count;

//saving the last searched page only if the search resulted in some products
if($total_items_in_search > 0){
    saveLastSearchPage($requestUri);
}

$smarty->assign("total_items", $total_items_in_search);
$smarty->assign("limit",4);
$pageLimitCrossed = false;
$pg=$searchProducts->offset + 1;
$pages=$searchProducts->getTotalPages();
if($pg > $pages){
	$pg=$pages;
	$pageLimitCrossed = true;
}
$begin = $pg - 4;
while($begin < 1){
	$begin++;
}
$end = $pg + 4;
while($end > $pages){
	$end--;
}
$smarty->assign("current_page",$searchProducts->offset + 1);
$smarty->assign("start_page",$begin);
$smarty->assign("end_page",$end);
$smarty->assign("page_count",$pages);
$smarty->assign("faceted_search_results",$searchProducts->faceted_search_results);
$faceted_search_results_new=$searchProducts->faceted_search_results_new;
$smarty->assign("faceted_search_results_new",$faceted_search_results_new);
$brands_search_results=$faceted_search_results_new->brands_filter_facet;
$offer_type=$faceted_search_results_new->dre_offerType;

use seo\SearchBreadCrumb;
use seo\SeoBreadCrumbKeyEnum;

if(count(get_object_vars($faceted_search_results_new->brands_filter_facet))==1){
    foreach ($faceted_search_results_new->brands_filter_facet as $brand => $id) {
        $current_list_page_brand = $brand;
    }
}

if(count(get_object_vars($faceted_search_results_new->global_attr_master_category_facet))==1){
    foreach ($faceted_search_results_new->global_attr_master_category_facet as $mc => $id) {
        $current_list_page_master_category = $mc;
    }
}

if(count(get_object_vars($faceted_search_results_new->global_attr_sub_category_facet))==1){
    foreach ($faceted_search_results_new->global_attr_sub_category_facet as $sc => $id) {
        $current_list_page_sub_category = $sc;
    }
}

if(count(get_object_vars($faceted_search_results_new->global_attr_article_type_facet))==1){
    foreach ($faceted_search_results_new->global_attr_article_type_facet as $at => $id) {
        $current_list_page_article_type = $at;
    }
}

if(count(get_object_vars($faceted_search_results_new->global_attr_gender))==1){
    foreach ($faceted_search_results_new->global_attr_gender as $gender => $id) {
        $current_list_page_gender = $gender;
    }
}


$productDataForBreadCrumb = array( 
    SeoBreadCrumbKeyEnum::Brand => $current_list_page_brand,
    SeoBreadCrumbKeyEnum::ArticleType => $current_list_page_article_type ,
    SeoBreadCrumbKeyEnum::SubCategory => $current_list_page_sub_category ,
    SeoBreadCrumbKeyEnum::MasterCategory => $current_list_page_master_category ,
    SeoBreadCrumbKeyEnum::Gender => $current_list_page_gender
);


if(!empty($brands_search_results)){
	$brands_search_results_count=count((array)$brands_search_results);
	$smarty->assign("brands_search_results",$brands_search_results);
	$smarty->assign("brands_search_results_count",$brands_search_results_count);
}

if(!empty($offer_type)){
    $offer_type_count=count((array)$offer_type);
    $smarty->assign("offer_type",$offer_type);
    $smarty->assign("offer_type_count",$offer_type_count);
}

//$fit_search_results=$faceted_search_results_new->fit_filter_facet;
$fit_search_results=$faceted_search_results_new->global_attr_gender;
$smarty->assign("fit_filters_arr",$fit_search_results);

if(!empty($fit_search_results)){
	$fit_search_results_count=count((array)$fit_search_results);
	$smarty->assign("fit_filters_arr_count",$fit_search_results_count);
}

$article_types=$faceted_search_results_new->global_attr_article_type_facet;

$article_type_attributes=null;

$article_type_filters=null;

if(!empty($article_types)){
	if(count((array)$article_types) > 1){
		$article_type_filters=$article_types;
	}
}
if(!empty($article_types)){
	if(count((array)$article_types) == 1){
		$article_type_attributes=$faceted_search_results_new->article_type_attributes;
	}
}

//echo "<pre>";
//print_r($article_type_attributes);

$smarty->assign("article_type_filters",$article_type_filters);
$smarty->assign("article_type_attributes",$article_type_attributes);

$display_category_filters=$faceted_search_results_new->display_category_facet;
$smarty->assign("display_category_filters",$display_category_filters);

$colour_falmily_filters=$faceted_search_results_new->colour_family_list;
$colorSwatchMapping=$xcache->fetchAndStore(function(){
        $sql = "select family_name, swatch_position from attribute_type_family";
        $colorSwatchMappingTemp=func_query($sql);
		$mapping = array();
		foreach($colorSwatchMappingTemp as $key => $value){
        	$mapping[strtolower($value["family_name"])]=$value["swatch_position"];
        }
		return $mapping;
}, array(), "color_attribute_type_family");

$smarty->assign("colorSwatchMapping",$colorSwatchMapping);
$jsonSearchData["colorSwatchMapping"] = $colorSwatchMapping;
$smarty->assign("colour_arr",$colour_falmily_filters);
$smarty->assign("colour_arr_count",count((array)$colour_falmily_filters));
$sizes_filter=$faceted_search_results_new->sizes_facet;
if(!empty($sizes_filter)){
	$sizes_filter_count=count((array)$faceted_search_results_new->sizes_facet);
}

$smarty->assign("sizes_filter",$sizes_filter);
$smarty->assign("sizes_filter_count",$sizes_filter_count);
$query=str_replace("-"," ",$_GET['query']);
$all_filter_details=$xcache->fetchAndStore(function(){
        $filter_sql="select * from mk_filters";
        $filters = func_query($filter_sql);
        $filter_map = array();
        foreach ($filters as $f) {
            $filter_map[strtolower($f["filter_name"])]=$f;
        }
        return $filter_map;
}, array($query), "mk_filters");
$filter_details = array($all_filter_details[strtolower($query)]);
$weblog->debug("Filter Details:".print_r($filter_details, true));
if(!empty($filter_details[0]['top_banner']))
{
	$smarty->assign("top_banner",$filter_details[0]['top_banner']);
}
else if(!empty($searchProducts->top_banner))
{
	$smarty->assign("top_banner",$searchProducts->top_banner);
}

$h1 = null;
if(!empty($filter_details[0]['h1_title']))
{
	$smarty->assign("h1",$filter_details[0]['h1_title']);
    $h1 = $filter_details[0]['h1_title'];
}
else if(!empty($searchProducts->page_h1))
{
	$smarty->assign("h1",$searchProducts->page_h1);
    $h1 = $searchProducts->page_h1;
}

if(!empty($filter_details[0]['about_filter']))
{
	$smarty->assign ("description",$filter_details[0]['about_filter']);
}
else
{
	$smarty->assign ("description",$searchProducts->description);
}

$metaDescription = (!empty($filter_details[0]['meta_description']))? $filter_details[0]['meta_description'] : $searchProducts->meta_description ;
$pageTitle = (!empty($filter_details[0]['page_title']))? $filter_details[0]['page_title'] : $searchProducts->page_title ;
// Incase of OR search, prepend string to the pageTitle
if($searchProducts->query_level == "FULL") {
    $pageTitle = "Search - ".$pageTitle;
    $isORSearch = true;
}

$smarty->assign ("metaDescriptionNoPage",$metaDescription);
if(!empty($_GET['synonymQuery']))
{
	$pageTitle = $_GET['synonymQuery'];
}
$smarty->assign ("pageTitleNoPage",$pageTitle);
if($pg != 1){	 
     $metaDescription = $metaDescription." Page $pg of $pages";	 
     $pageTitle = $pageTitle." | Page $pg/$pages";	 
}
$smarty->assign ("metaDescription",$metaDescription);
$smarty->assign ("pageTitle",$pageTitle);
$smarty->assign("sort_param_in_url",$searchProducts->sort_param);

$showCanonicalTag = false;
$tmp_pageLocation = trim($_GET['query']);
$tmp_tokensArray = explode("-",$tmp_pageLocation);
$searchProducts->canonical_search_phrase = strtolower(implode("-", array_unique($tmp_tokensArray)));
$searchProducts->canonical_search_phrase = preg_replace("/-{2,}/","-",$searchProducts->canonical_search_phrase);
$searchProducts->canonical_search_phrase = preg_replace("/^-{1,}/","",$searchProducts->canonical_search_phrase);

$request_uri = explode("?",$_SERVER['REQUEST_URI'],2);
$queryStrIndex = strpos($_SERVER['REQUEST_URI'], '?');

if ($searchProducts->canonical_search_phrase != trim($_GET['query']) || '/'.$searchProducts->canonical_search_phrase != $request_uri[0] || $queryStrIndex !== false) {
    $showCanonicalTag = true;
    $cannonicalPageUrl  = HostConfig::$baseUrl;
    $cannonicalPageUrl .= $searchProducts->canonical_search_phrase;
    $smarty->assign("canonicalPageUrl", $cannonicalPageUrl);
}

if(str_replace("/","",$request_uri[0]) !== $ruleKeyword && $ruleKeyword) {
    $showCanonicalTag = true;
    $cannonicalPageUrl  = HostConfig::$baseUrl;
    $cannonicalPageUrl .= $ruleKeyword;
    $smarty->assign("canonicalPageUrl", $cannonicalPageUrl);
}

$sizeCanonicalPageUrl = stristr($squery, '-size', true);
if($sizeCanonicalPageUrl) {
    $showCanonicalTag = true;
    $smarty->assign("canonicalPageUrl", HostConfig::$baseUrl.$sizeCanonicalPageUrl);
}

$smarty->assign("pageTypeSearch", true);
$smarty->assign("showCanonicalTag", $showCanonicalTag);

if(!empty($filter_details[0]['meta_keywords']))
	$smarty->assign ("metaKeywords",$filter_details[0]['meta_keywords']);
else
	$smarty->assign ("metaKeywords",$searchProducts->meta_keywords);

if(!empty($searchProducts->page_promotion)){
	$smarty->assign ("promotion_content",$searchProducts->page_promotion);
}

//retargeting tracking code
$retargetPixel = retargetingPixelOnType($products,'PL');
if(!empty($retargetPixel)){
	$smarty->assign("retargetPixel",$retargetPixel);
}
if(!empty($filter_details[0]['h1_title'])){
	$smarty->assign("h1",$filter_details[0]['h1_title']);
    $h1 = $filter_details[0]['h1_title'];
}
else if(!empty($searchProducts->page_h1)){
	$smarty->assign("h1",$searchProducts->page_h1);
    $h1 = $searchProducts->page_h1;
}
if($seoEnabled){        
    $smarty->assign("h1",$seoh1);
}

// PORTAL-2486 Left navigation links are obtained by appending
// article types with smarty variable "query". We plan to use 
// only the unique words from the search query.
$pageLocation = trim($_GET['query']);
$tokensArray = explode("-",$pageLocation);
$pageLocation = implode("-", array_unique($tokensArray));
$smarty->assign ("query",$pageLocation);
$newslideShowBanners = CachedBannerManager::getCachedBanners(PageConfigLocationConstants::SearchPage, PageConfigSectionConstants::StaticImage, $pageLocation);
$searchLeftPaneBanner = CachedBannerManager::getCachedBanners(PageConfigLocationConstants::SearchPage, PageConfigSectionConstants::SlideshowImage, $pageLocation);
$multiBannerData = CachedBannerManager::getMultiBannerData($newslideShowBanners,PageConfigLocationConstants::SearchPage, PageConfigSectionConstants::SlideshowImage, $pageLocation);
$smarty->assign("bannerImageProperties", $newslideShowBanners[0]);
$smarty->assign("searchLeftPaneBanner", $searchLeftPaneBanner[0]);
$smarty->assign('multiBannerData',$multiBannerData);
$smarty->assign ("synonymQuery",$_GET['synonymQuery']);
$smarty->assign("fluid", true);

/* BreadCrumb Creation*/
$title = null;
$h1 = trim($h1);
$synonymQuery = $_GET['synonymQuery'];
$synonymQuery = trim($synonymQuery);
if(!empty($synonymQuery)) {
    $isSynonym = true;
}
$query = trim($query);
if(!empty($h1)){
    $title = $h1;
} else if(!empty($synonymQuery)) {
    $title = $synonymQuery;
} else {
    $title = implode(" ", explode("-", $query));
    $title = substr($title, 0, 40);
}


if(isset($_GET["bc"]) ) {
        $clickedFromBC=filter_input(INPUT_GET,"bc", FILTER_SANITIZE_NUMBER_INT);
}

$productDataForBreadCrumb[SeoBreadCrumbKeyEnum::ProductName] = $title;

$breadCrumb = new SearchBreadCrumb(); 
$smarty->assign("clickedFromBC", $clickedFromBC);
$smarty->assign("locationBasedBreadCrumb", $breadCrumb->getData($productDataForBreadCrumb));
$smarty->assign("LocationBreadcrumbABTest", MABTest::getInstance()->getUserSegment('LocationBreadcrumbABTest'));

//robot Indexing 
$noRobotIndexing = false;
if($searchProducts->query_level == 'FULL') {
    $noRobotIndexing = true;   
    // checking for OR page search enabled for robot indexing from the seo admin
    $seoAdminDBenabled=\FeatureGateKeyValuePairs::getBoolean('seoAdminDBenabled');
    if($seoEnabled && !$seoAdminDBenabled) {
        $urlHash = SeoHashUtil::toHash($macros->url, $macros->page_type);
        if(SeoAdminCache::getDataForHash($urlHash)) {
            $noRobotIndexing = false;
        }
    }
}
if($metaTagNoindex or $noRobotIndexing) {
    $isNoIndexed = true;
}
//AVAIL
$smarty->assign ("userQuery",filter_input(INPUT_GET, 'userQuery', FILTER_SANITIZE_STRING));
$rcv_str='';
$rcv_avail_content = getMyntraCookie('RCV_AVAIL',true);
if(!empty($rcv_avail_content)) {
	$rcv_str=$rcv_avail_content;
}

$smarty->assign ("avail_cookie_ids",$rcv_str);
if(!is_null($tracker->searchData))
{
	$tracker->searchData->setNumOfResults($searchProducts->products_count);
	$tracker->searchData->setTimeTakenToSearch($timeSearchFinish - $timeSearchStart);
	$tracker->searchData->setSolrCallLatency($searchProducts->getSolrCallLatencies());
	$tracker->searchData->setSearchInitLatency($searchProducts->getInitLatency());
        if($searchProducts->products_count > 0)
        {
                $resultSet = array();
                //$tracker->searchData->
                $pos = $searchProducts->limit * $searchProducts->offset;
                foreach ($searchProducts->products as $searhResult)
                {
                        array_push($resultSet, array("styleid" => $searhResult['styleid'], "pos" => ++$pos));
                }
                $tracker->searchData->setResultSet($resultSet);
        }
}
if(!is_null($tracker->seoData)) {
    $tracker->seoData->isCanonical = $showCanonicalTag;
    $tracker->seoData->isORSearch = $isORSearch;
    $tracker->seoData->isNoIndexed = $isNoIndexed;
    $tracker->seoData->isSynonym = $isSynonym;
    $tracker->seoData->canonicalUrl = $cannonicalPageUrl;
    $tracker->seoData->isParametrised = $isParametrised;
    $tracker->seoData->parametrisedDesc = $parametrisedDesc;
}
$tracker->fireRequest();

$analyticsObj = AnalyticsBase::getInstance();
if ($_GET['userQuery'] === "true") {
	//Internal search tracking
	$analyticsObj->setContextualPageData(AnalyticsBase::SEARCH_PAGE, $pageLocation);
	$analyticsObj->setSearchEventVars($pageLocation, $searchProducts->products_count);
} else {
	$analyticsObj->setContextualPageData(AnalyticsBase::LANDING_PAGE, $pageLocation);
}

$analyticsObj->setInternalCampaignTrackingCode($_GET['hpbnr']);	// Set Internal Tracking Campaign Code.

$analyticsObj->setMerchandisingBrowseCategoryData($pageLocation);
$analyticsObj->setTemplateVars($smarty);

// store the serach keyword only for non-robot calls and if userQuery = true
if(($_GET['userQuery'] === "true") && ($XCART_SESSION_VARS['is_robot'] != 'Y')) {
	$url = HostConfig::$searchSystemUrl;
	$searchData= array('keyword'=>trim($searchProducts->original_phrase), 'count_of_products'=>$searchProducts->products_count);
	$searchJSONdata = json_encode($searchData);
	$base64Str = base64_encode($searchJSONdata);
	$throwAsyncData = FeatureGateKeyValuePairs::getBoolean('search.sitemaps.sendData', false);
	if($throwAsyncData) {
		fireAsync($url,"/data?"  . $base64Str . "#" .$configMode);
	} 
}


$p_url			=	$http_location."/$searchProducts->canonical_search_phrase".(($searchProducts->sort_param)? "/".$searchProducts->sort_param : "");
/*
if(!$showCanonicalTag) {
    if($pg > 2 && $pg < $pages) {
        $nextUrl = $p_url."/".($pg +1);
        $smarty->assign("nextLinkUrl",$nextUrl);
        $prevUrl = $p_url."/".($pg-1);	
        $smarty->assign("previousLinkUrl",$prevUrl);
    } else if ($pg == 2) {
	    $nextUrl = $p_url."/".($pg +1);
        $smarty->assign("nextLinkUrl",$nextUrl);
        $prevUrl = $p_url.(($searchProducts->sort_param)?"/".($pg-1):"");
        $smarty->assign("previousLinkUrl",$prevUrl);
    } else if ($pg == 1 ) {
        if($pg != $pages) {
            $nextUrl = $p_url."/".($pg +1);
            $smarty->assign("nextLinkUrl",$nextUrl);
        }
    } else if ( !$pageLimitCrossed && ($pg == $pages)) {
        $prevUrl = $p_url."/".($pg-1);
        $smarty->assign("previousLinkUrl",$prevUrl);
    }
}	
 */

$adapter = new JpegminiDBAdapter();
$jpegminiconf = json_encode($adapter->getConfig());
$smarty->assign("jpegminiconf", $jpegminiconf);

if ($optimVariant == 'test') {
    $products = $searchProducts->getSeachOptimProducts();
    /*
     * Queries for search service
     */
    //normal query
    $jsonSearchData['search']['query'] = $searchProducts->getFinalQuery();
    $jsonSearchData['search']['start'] = 0;
    $jsonSearchData['search']['rows'] = FeatureGateKeyValuePairs::getInteger('searchServiceQueryRows');
    $jsonSearchData['search']['sort'] = $searchProducts->getSortFieldForSolr();
    $jsonSearchData['search']['return_docs'] = true;
    $jsonSearchData['search']['facetField'] = array();
    if ($colourOptionsVariant == 'test') {
        $jsonSearchData['search']['colour_grouping'] = true;
    }

    //filter query
    $jsonSearchData['search']['fq'] = array();

    //facet flag
    $jsonSearchData['search']['facet'] = false;

    //curated query
    $jsonSearchData['search']['curated_query'] = $searchProducts->getCuratedQuery();
    if(!$jsonSearchData['search']['curated_query']['enable']) {
        unset($jsonSearchData['search']['curated_query']);
    } else {
        $jsonSearchData['search']['curated_query']['start'] = 0;
        $jsonSearchData['search']['curated_query']['rows'] = $jsonSearchData['search']['curated_query']['no_slots'];
        $jsonSearchData["curated_products"] = array_splice($products, 0, $jsonSearchData['search']['curated_query']['no_slots']);
    }

    //solr sort fields mapping
    $jsonSearchData["solr_sort_field"] = $searchProducts->getSolrSortField();

    //article type and specific attribute types mapping for left filter in search
    $articleAttributeMapKey = 'all_article_attribute';
    $cachedATSAMap = $xcache->fetchObject($articleAttributeMapKey);
    if($cachedATSAMap == NULL) {
        $sqlQuery = 'SELECT at.attribute_type AS attribute_type, cc.typename AS typename FROM mk_catalogue_classification cc LEFT JOIN mk_attribute_type at ON at.catalogue_classification_id = cc.id WHERE is_filter=1';
        $results = func_query($sqlQuery, TRUE);
        $cachedATSAMap = array();
        foreach ($results as $result) {
            $cachedATSAMap[$result['typename']][] = str_replace(" ","_",$result['attribute_type']).'_article_attr';
        }
        $xcache->storeObject($articleAttributeMapKey, $cachedATSAMap, 0);
    }

    $jsonSearchData['all_article_attribute'] = $cachedATSAMap;
    $jsonSearchData['quicklookenabled'] = $quicklookenabled;
    $jsonSearchData['rupeesymbol'] = $rupeesymbol ? $rupeesymbol : 'Rs.';
    $jsonSearchData['http_location'] = $http_location;
    $jsonSearchData['cdn_base'] = $cdn_base;
    $jsonSearchData['nav_id'] = $nav_id;
    $jsonSearchData['pageSize'] = WidgetKeyValuePairs::getInteger('searchPageDefaultNoOfItems');
    $jsonSearchData['totalProductsCount'] = $total_items_in_search;
    $jsonSearchData['displayedProductsCount'] = 0;
    $jsonSearchData['query'] = $pageLocation;
    $jsonSearchData["products"] = $products;
    $jsonSearchData["filters"] = $faceted_search_results_new;
    $jsonSearchData["filters"]->discount_percentage = $jsonSearchData["filters"]->discount_percentage_details;
    $jsonSearchData["filters"]->discounted_price = $jsonSearchData["filters"]->price_range_details;
    unset($jsonSearchData["filters"]->discount_percentage_details);
    unset($jsonSearchData["filters"]->price_range_details);

    //atlas data for ajax search
    //sending: trafficdata, referer, device data ad mabtest
    $jsonSearchData['search']['mongoData']['trafficData'] = $tracker->objData['trafficData'];
    $jsonSearchData['search']['mongoData']['referer'] = $tracker->objData['referer'];
    $jsonSearchData['search']['mongoData']['deviceData'] = $tracker->objData['deviceData'];
    global $_MABTestObject;
    $jsonSearchData['search']['mongoData']['mabTestData'] = $_MABTestObject->getTrackingData();

    $smarty->assign("jsonSearchData", json_encode($jsonSearchData));
}
global $googleBot;
if ($googleBot)
    $smarty->assign("minNonLazyFilters", 2000);
else
    $smarty->assign("minNonLazyFilters", 2000);
//  $smarty->assign("minNonLazyFilters", 7);

$synonymQuery_get = $_GET['synonymQuery'];


$showBreadCrumb = true;
if($searchProducts->query_level == 'FULL') {
    $showBreadCrumb = false;    
}
if($synonymQuery_get and trim($synonymQuery_get) != "") {
    $showBreadCrumb = false;
    $searchMetaDataForGAStats['searchType'] = 'SYNONYM';
}

preg_match("/\/[^\w\d]/i",$request_uri[0],$matches);
if(!empty($matches)) {
    $noRobotIndexing = true;
}

//mobile ads data section
if( DeviceDetector::isMobile() ) {
    $mobileDiscountAdString= WidgetKeyValuePairs::getWidgetValueForKey("mobile.header.promotion.data");
    $mobileDiscountAds=json_decode($mobileDiscountAdString,true,512);
    $uri_parts = explode('?', $_SERVER['REQUEST_URI']);
    $urlPath=substr($uri_parts[0],1);
    //var_dump($mobileDiscountAd['defaults']);
    $mobileListPageAds = $mobileDiscountAds['list']['url-specific'];
    $mobileListPageDefaultAds = $mobileDiscountAds['list']['default'];
    $urlPathSpecificAds=$mobileListPageAds[$urlPath];
    if($urlPathSpecificAds!==null) {
        $smarty->assign("mobile_promotions",$urlPathSpecificAds);
    }
    else {
        $smarty->assign("mobile_promotions",array($mobileListPageDefaultAds));
    }
}
//mobile ads data section ends.

$smarty->assign("showBreadCrumb", $showBreadCrumb);
$smarty->assign("noRobotIndexing", $noRobotIndexing);
$smarty->assign("searchMetaDataForGAStats", $searchMetaDataForGAStats);
$smarty->display("search.tpl");
?>
