<?php

define("QUICK_START", 1);
define("XCART_SESSION_START", 1);

require_once $_SERVER['DOCUMENT_ROOT']."/authAPI.php";
include_once $_SERVER['DOCUMENT_ROOT']."/databaseinit.php";
require_once "$xcart_dir/Cache/Cache.php";
require_once "$xcart_dir/include/class/search/SearchProducts.php";
use style\StyleGroup;
use style\builder\CachedStyleBuilder;
use style\builder\StyleBuilder;

if(isset($_GET["ids"]) ) {
	$productStyleIds = explode(',', $_GET["ids"]);
}
$output = filter_input(INPUT_GET, 'output', FILTER_SANITIZE_STRING);

$data = array();
foreach ($productStyleIds as $productStyleId) {
    $productStyleId = filter_var($productStyleId, FILTER_SANITIZE_NUMBER_INT);
    if (empty($productStyleId)) {
        continue;
    }

    $cachedStyleBuilderObj = CachedStyleBuilder::getInstance();
    $styleObj = $cachedStyleBuilderObj->getStyleObject($productStyleId);
    //header('Content-Type:text/plain'); print_r($styleObj); exit;

    // This should NOT happen on prod environment. May happen in local environment
    if (empty($styleObj)) {
        continue;
    }

    $productOptionDetails = $styleObj->getActiveProductOptionsData();
    $allProductOptionDetails = $styleObj->getAllProductOptions();
    //$inactiveProductOptionDetails = $styleObj->getInactiveProductOptionsData();
    $sizeOptions = $styleObj->getSizeUnificationDataForAvailableOptions();
    $allSizeOptions = $styleObj->getSizeUnificationDataForAllOptions();
    /*
    $flattenSizeOptions = Size_unification::ifSizesUnified($sizeOptions);
    $flattenSizeOptionsAll = Size_unification::ifSizesUnified($allSizeOptions);
    $allSizeKeys = array_keys($allSizeOptions);

    $unavaliableOptions = array();
    $inactiveProductOptionDetails = $styleObj->getInactiveProductOptionsData();
    foreach ($inactiveProductOptionDetails as $row) {
        $unavailabeOptions[$row['value']] = $row;
        if ($flattenSizeOptionsAll){
            $unavailabeOptions[$row['value']]['unified'] = $allSizeOptions[$row['value']];
        }
    }
    */

    $sizeOptionSKUMapping = array();
    $allSizeOptionSKUMapping = array();
    foreach ($sizeOptions as $key => $value) {
        foreach ($productOptionDetails as $options) {
            if ($options['value'] == $key) {
                $sizeOptionSKUMapping[$options['sku_id']] = $value;
            }
        }
    }
    foreach ($allSizeOptions as $key => $value) {
        foreach ($allProductOptionDetails as $options) {
            if ($options['value'] == $key) {
                if ($options['sku_count'] > 0) {
                    $allSizeOptionSKUMapping[$options['sku_id']] = array($value, true, $options['sku_count']);
                }
                else {
                    $allSizeOptionSKUMapping[$options['sku_id']] = array($value, false, $options['sku_count']);
                }
            }
        }
    }

    // data needed by the BuyLook widget
    if ($output == 'all') {
        $styleDetails = $styleObj->getProductStyleDetails();
        $styleProps = $styleObj->getStyleProperties();
        $discountData = $styleObj->getDiscountData();
        $catalogData = $styleObj->getCatalogueClassificationData();

        // to maintain the sort order, make skus as an indexd array
        $skus = array();
        foreach ($allSizeOptionSKUMapping as $key => $val) {
            array_unshift($val, $key);
            $skus[] = $val; // format: [sku-id:int, unified-size-name:string, in-stock:boolean, count-available:int]
        }
       // echo "<PRE>--";print_r($discountData);
        $img = $styleProps['default_image'];
        $img = str_replace('_images.jpg', '_images_96_128.jpg', $img);
        $mrp = $styleDetails["price"];
        $price = empty($discountData->discountAmount) ? $mrp : ($mrp-$discountData->discountAmount);
        $brandName = $styleObj->getBrandName();
        $title = preg_replace("/^{$brandName}\s*/i", "", $styleProps['product_display_name']);
		$data[$productStyleId] = array(
            'styleId'     => $productStyleId,
            'brand'       => $brandName,
            'articleType' => $catalogData['articletype'],
            'title'       => $title,
            'image'       => $img,
            'price'       => round($price),
            'skus'        => $skus,
			'url'		  =>construct_style_url($catalogData["articletype"],$styleProps["global_attr_brand"],$styleProps["product_display_name"],$productStyleId)
        );
        if ($price !== $mrp) {
            $data[$productStyleId]['mrp'] = round($mrp);
           }
        $response = array('status' => 'SUCCESS', 'data' => $data);
    }
    // default output is just sizes data only
    else {
    $data[$productStyleId] = $allSizeOptionSKUMapping;
        $response = $data;
    }
}

header('Content-Type: application/json');
echo json_encode($response);
exit;

