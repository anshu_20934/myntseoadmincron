<?php
define("QUICK_START", 1);
//define("XCART_SESSION_START", 1);

require_once("../auth.php");
require_once $xcart_dir."/include/check_useraccount.php";
require_once $xcart_dir."/include/func/func.courier.php";
require_once $xcart_dir."/include/func/func.returns.php";
require_once $xcart_dir."/include/class/class.mymyntra.php";
require_once $xcart_dir."/include/class/oms/OrderExchangeManager.php";
require_once($xcart_dir.'/include/class/class.ordertime.php');
include_once("$xcart_dir/include/class/widget/class.widget.keyvaluepair.php");
include_once "$xcart_dir/include/func/func.order.php";
include_once "$xcart_dir/include/func/func.mk_orderbook.php";
include_once "$xcart_dir/include/func/func.utilities.php";

use style\StyleGroup;
use style\builder\CachedStyleBuilder;
use style\builder\StyleBuilder;

//pickup changes
$ReturnPickupExcludedlist = WidgetKeyValuePairs::getWidgetValueForKey('pickup.disabled.articletypes');
$ReturnPickupExcludedArray = explode(",", $ReturnPickupExcludedlist);

function getSizesView() {
    global $smarty, $login;


    $itemId = filter_input(INPUT_GET, 'itemId', FILTER_SANITIZE_NUMBER_INT);
    $styleId = filter_input(INPUT_GET, 'styleId', FILTER_SANITIZE_NUMBER_INT);
    $zipcode = filter_input(INPUT_GET, 'zipcode', FILTER_SANITIZE_NUMBER_INT);
    //**COMMENTING AS THIS CAN BE TAKEN FROM STYLE OBJECT which is CACHED and also sorted 
    //$aSizes = OrderExchangeManager::getItemSkuDetails($itemId);

    $cachedStyleBuilderObj = CachedStyleBuilder::getInstance();
    $styleObj = $cachedStyleBuilderObj->getStyleObject($styleId);
    // This should NOT happen on prod environment. May happen in local environment
    if (empty($styleObj)) {
        continue;
    }

    $catalogData = $styleObj->getCatalogueClassificationData();
    $style_properties =$styleObj->getStyleProperties();
    $sizeRepresentationImageUrl=$style_properties["size_representation_image_url"];


    $productOptionDetails = $styleObj->getActiveProductOptionsData();
    $allProductOptionDetails = $styleObj->getAllProductOptions();
    $sizeOptions = $styleObj->getSizeUnificationDataForAvailableOptions();
    $allSizeOptions = $styleObj->getSizeUnificationDataForAllOptions();
    
    $sizeOptionSKUMapping = array();
    $allSizeOptionSKUMapping = array();
    
    $availableInWarehouses = false;
    foreach ($productOptionDetails as $options) {
    	if(!empty($options['availableInWarehouses'])) {
    		if(!$availableInWarehouses)
    			$availableInWarehouses = explode(",", $options['availableInWarehouses']);
    		else
    			$availableInWarehouses = array_merge($availableInWarehouses, explode(",", $options['availableInWarehouses']));
    	}
    }
    $availableInWarehouses = array_values(array_unique($availableInWarehouses));
    
    foreach ($sizeOptions as $key => $value) {
        foreach ($productOptionDetails as $options) {
            if ($options['value'] == $key) {
                $sizeOptionSKUMapping[$options['sku_id']] = $value;
            }
        }
    }
    foreach ($allSizeOptions as $key => $value) {
        foreach ($allProductOptionDetails as $options) {
            if ($options['value'] == $key) {
                if ($options['sku_count'] > 0) {
                    $allSizeOptionSKUMapping[] = array("size" => $value, "optionid" =>$options['id'], "sku_id" =>$options['sku_id'], "is_available" => true,  "sku_count" =>$options['sku_count'],);
                }
                else {
                   $allSizeOptionSKUMapping[] = array("size" => $value, "optionid" =>$options['id'], "sku_id" =>$options['sku_id'], "is_available" => false,  "sku_count" =>$options['sku_count'],);
                }
            }
        }
    }
    $aSizes = $allSizeOptionSKUMapping;

    $query = 'select size_chart_image from mk_product_style where id=' . $styleId ;
    $size_chart = func_query($query);


	// we can check the exchange serviceability here and return it as part of the array?
    $singleitem = func_query_first("Select od.*, som.style_id, som.sku_id from xcart_order_details od, mk_order_item_option_quantity oq, mk_styles_options_skus_mapping som where od.itemid = oq.itemid and oq.optionid = som.option_id and od.itemid = $itemId", true);
    $singleitem['totalProductPrice'] = $singleitem['price']*$singleitem['amount'];
    $singleitem['couponDiscount'] = $singleitem['coupon_disount_product'];
    $singleitem['cartDiscount'] = $singleitem['cart_discount_split_on_ratio'];
    $singleitem['totalMyntCashUsage'] = $singleitem['cash_redeemed'];
    
    $sku_id = func_query_first_cell("Select sku_id from mk_product_options po, mk_styles_options_skus_mapping som where po.id = som.option_id and po.is_active = 1 and po.style = $singleitem[product_style]", true);
    $product = array('style_id' => $singleitem['product_style'], 'sku_id'=> $sku_id);
    
    if(!empty($availableInWarehouses))
    	$isZipCodeServiceable = func_check_exchange_serviceability_products($zipcode, array($product), $availableInWarehouses);
    else
    	$isZipCodeServiceable = 0;

    return array(
        'status' => 'SUCCESS',
        'serviceable' => $isZipCodeServiceable ,
        'sizes' => $aSizes,
        'size_chart' => $size_chart[0]['size_chart_image'],
		'sizeRepresentationImageUrl' => $sizeRepresentationImageUrl,
        'allProductOptionDetails' => $allProductOptionDetails
    );
}

function getReasonView() {
    global $smarty, $login;

    $selfDeliveryCredit = WidgetKeyValuePairs::getWidgetValueForKey('returnSelfDeliveryCredit');
    $customerSupportCall = WidgetKeyValuePairs::getWidgetValueForKey('customerSupportCall');
    $customerSupportTime = WidgetKeyValuePairs::getWidgetValueForKey('customerSupportTime');

    $markup = <<<HTML
<div class="step-reason">
    <div class="label"> Select A Reason </div>
    <label class="lbl-qty">choose the quantity that you want to exchange *</label>
    <div class="mk-custom-drop-down sel-qty">
        <select name="qty">
            <option value="" selected="selected">Select a quantity</option>
            <option value="1">1</option>
        </select>
    </div>

    <label>tell us why you want to exchange the product? *</label>
    <div class="mk-custom-drop-down sel-reason">
        <select name="reason">
            <option value="" selected="selected">SELECT A REASON</option>
HTML;

        $reasons = get_all_return_reasons(true);
        foreach ($reasons as $reason) {
            $markup .= <<<HTML
            <option value="{$reason['code']}">{$reason['displayname']}</option>
HTML;
    }

        $markup .= <<<HTML
        </select>
    </div>

    <label>Please provide any additional details</label>
    <textarea name="details"></textarea>
</div>
HTML;

    return array(
        'status' => 'SUCCESS',
        'markup' => $markup,
        'amtCredit' => (float)$selfDeliveryCredit,
        'customerSupportTime' => $customerSupportTime,
        'customerSupportCall' => $customerSupportCall
    );
}

function getRefundAmount() {
    global $smarty, $login;

    $shipOpt = $_GET['shipopt'] === 'self' ? 'self' : 'pickup';
    $itemId = filter_input(INPUT_GET, 'itemid', FILTER_SANITIZE_NUMBER_INT);
    $qty = filter_input(INPUT_GET, 'qty', FILTER_SANITIZE_NUMBER_INT);

    $resp = array(
        'itemId'    => $itemId,
        'qty'       => $qty,
        'shipOpt'   => $shipOpt,
        'amtRefund' => $amtRefund
    );

    $amtRefund = func_calculate_refund_amount($itemId, $qty, $shipOpt);
    $amtRefund = $amtRefund['refundAmount'];
    
    if (!empty($amtRefund)) {
        $resp['status'] = 'SUCCESS';
        $resp['amtRefund'] = $amtRefund;
    }
    else {
        $resp['status'] = 'ERROR';
        $resp['message'] = 'Internal server error';
    }

    return $resp;
}
function replyFailure($markup, $warnCode = ''){
    $markup = '';
    	$markup .= <<<HTML
        <li>There has been a technical glitch, because of which we are unable to process this request right now. We regret the inconvenience caused.</li>
        <li>Please visit "<a href="/mymyntra.php?view=myorders">My Myntra > My Orders</a>" and try again later.</li>
HTML;
    	return array(
	        'status' => 'ERROR',
	        'markup' => $markup,
	        'data'   => '',
            'warnCode'=> $warnCode
	    );  
}
function getConfirmView() {
    global $smarty, $login,$userToken;

    $markup = '';
    $token = filter_input(INPUT_POST, '_token', FILTER_SANITIZE_STRING);
    if (!checkCSRF($token, "ajax_exchange"))
        replyFailure ($markup);
    $orderid = filter_input(INPUT_POST, 'orderId', FILTER_SANITIZE_NUMBER_INT);
    $itemid = filter_input(INPUT_POST, 'itemId', FILTER_SANITIZE_NUMBER_INT);
    $option = filter_input(INPUT_POST, 'option', FILTER_SANITIZE_STRING);
    $foption = filter_input(INPUT_POST, 'f_option', FILTER_SANITIZE_STRING);
    $quantity = filter_input(INPUT_POST, 'qty', FILTER_SANITIZE_NUMBER_INT);
    $customer_login = $login;
    $userid = $login;
    $reasoncode = filter_input(INPUT_POST, 'reason', FILTER_SANITIZE_STRING);
    $details = filter_input(INPUT_POST, 'details', FILTER_SANITIZE_STRING);
    $address = filter_input(INPUT_POST, 'addressId', FILTER_SANITIZE_NUMBER_INT);
    $styleId = filter_input(INPUT_POST, 'styleId', FILTER_SANITIZE_NUMBER_INT);

    //pickup changes
    $articletype = filter_input(INPUT_GET, 'articletype', FILTER_SANITIZE_NUMBER_INT);

    if (in_array($articletype,$pickup_excluded_array) && $return_mode == 'pickup') {
        return array(
            'status' => 'ERROR' ,
            'markup' => $markup,
            'message' => 'Item not applicable for exchange.',
            'data'   => ''
        );
    }

    if(empty($address)) {
        $address = $_POST['address'];
    }
    
    $cachedStyleBuilderObj = CachedStyleBuilder::getInstance();
    $styleObj = $cachedStyleBuilderObj->getStyleObject($styleId);
    // This should NOT happen on prod environment. May happen in local environment
    if (empty($styleObj)) {
        continue;
    }

    $productOptionDetails = $styleObj->getActiveProductOptionsData();
    $allProductOptionDetails = $styleObj->getAllProductOptions();
    $sizeOptions = $styleObj->getSizeUnificationDataForAvailableOptions();
    $allSizeOptions = $styleObj->getSizeUnificationDataForAllOptions();
    
    $sizeOptionSKUMapping = array();
    $allSizeOptionSKUMapping = array();

    foreach ($sizeOptions as $key => $value) {
        foreach ($productOptionDetails as $options) {
            if ($options['value'] == $key) {
                $sizeOptionSKUMapping[$options['sku_id']] = $value;
            }
        }
    }
    foreach ($allSizeOptions as $key => $value) {
        foreach ($allProductOptionDetails as $options) {
            if ($options['value'] == $key) {
                if ($options['sku_count'] > 0) {
                    $allSizeOptionSKUMapping[] = array("size" => $value, "optionid" =>$options['id'], "sku_id" =>$options['sku_id'], "is_available" => true,  "sku_count" =>$options['sku_count'],);
                }
                else {
                   $allSizeOptionSKUMapping[] = array("size" => $value, "optionid" =>$options['id'], "sku_id" =>$options['sku_id'], "is_available" => false,  "sku_count" =>$options['sku_count'],);
                }
            }
        }
    }
    $aSizes = $allSizeOptionSKUMapping;

    //$aSizes = OrderExchangeManager::getItemSkuDetails($itemid);
    /*

	Given option , get optionid and skuid

    */      


	foreach($aSizes as $key=>$value)
	{
		if($value['size'] == $option)
		{
			$optionid = $value['optionid'];
			$sku_id = $value['sku_id'];
			break;
		}
	}

    if(empty($sku_id)){
        foreach($aSizes as $key=>$value)
        {
            if($value['size'] == $foption)
            {
                $optionid = $value['optionid'];
                $sku_id = $value['sku_id'];
                break;
            }
        }
    }

    $response = OrderExchangeManager::createExchangeforOrder($orderid, $itemid, $optionid , $quantity, $sku_id , $userid, $reasoncode, $details, $address,false);

	$order_amount_detail = func_get_order_amount_details($response['exchange_orderid']);
    $productsIncart = func_create_array_products_of_order($response['exchange_orderid']);

	$query = 'select s_zipcode,payment_method,courier_service,warehouseid from xcart_orders where orderid=' . $response['exchange_orderid'];
	$exchange_order = func_query_first($query);

    $delivery_date = date("jS M Y", getDeliveryDateForOrder($order_amount_detail['date'], $productsIncart, $exchange_order['s_zipcode'], $exchange_order['payment_method'], false, false));

    if(!$response['status']){
        return replyFailure($markup, $response['warn_code']);
    }
    
    $data = array();
    $data['exchangeId'] = $response['exchange_orderid'];
    $data['orderId'] = $orderid;
    $data['itemId'] = $itemid;

    /* NOTE: uncomment the block to debug the POST, request and response params
    $request = compact('orderid', 'itemid', 'option', 'quantity',
            'customer_login', 'userid', 'reasoncode', 'details', 'return_mode', 'selected_address_id');
    $data['_post'] = $_POST;
    $data['request'] = $request;
    $data['response'] = $response;
    */
    
    $exchangeSuccessImage =  WidgetKeyValuePairs::getWidgetValueForKey("exchangeSuccessImage");

	error_log("return froms uccess image");
    $urlReturnForm = "/generatelabel.php?for=returndetails&id=" . $data['returnId'];
    $markup .= <<<HTML
<div class="step-confirm">
    <h3 class="gray">Your Exchange Order Id is <strong>{$data['exchangeId']}</strong></h3>
    <div class="success-msg">
        <div class="msg" style="text-transform:none;font-weight:normal;font-size:14px;">
		Your expected delivery date: {$delivery_date} and You can track your order from <a href="/mymyntra.php?view=myorders">MY ORDERS</a> section
        </div>
	<img src="{$exchangeSuccessImage}" width='826px' height='227px' style="margin-left:-50px;" />
    </div>
HTML;
    $markup .= <<<HTML
</div>
HTML;

	error_log("return from getdeliverydatefor order***************** final " . $data . "sdadasf" . $markup);
    return array(
        'status' => 'SUCCESS',
        'markup' => $markup,
        'data'   => $data
    );
	error_log("return from getdeliverydatefor order***************** final ");
}


global $login;
if (empty($login)) {
    $resp = array(
        'status'  => 'ERROR',
        'message' => 'Please login'
    );
}
else {
    $view = $_REQUEST['_view'];
    if ($view == 'reason') {
        $resp = getReasonView();
    }
    else if ($view == 'confirm') {
        $resp = getConfirmView();
    }
    else if ($view == 'amt-refund') {
        $resp = getRefundAmount();
    }
    else if ($view == 'size') {
        $resp = getSizesView();
    }
    else {
        $resp = array(
            'status'  => 'ERROR',
            'message' => 'Invalid View'
        );
    }
}

header('Content-Type: application/json');
echo json_encode($resp);
exit;

