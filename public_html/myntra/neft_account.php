<?php

require_once(dirname(__FILE__)."/../auth.php");
require_once $xcart_dir."/include/func/func.courier.php";
require_once $xcart_dir."/include/class/class.mymyntra.php";
include_once "$xcart_dir/include/class/mcart/class.MCart.php";
include_once "$xcart_dir/include/func.func.utilities.php";
include_once $xcart_dir."/modules/apiclient/RefundCrmApiClient.php";

use style\StyleGroup;
use style\builder\CachedStyleBuilder;
use style\builder\StyleBuilder;


function getNeftAccountList() {
	
    global $smarty, $login, $XCART_SESSION_VARS;    
    
    $customerSupportCall = WidgetKeyValuePairs::getWidgetValueForKey('customerSupportCall');
    $customerSupportTime = WidgetKeyValuePairs::getWidgetValueForKey('customerSupportTime');    
    
    $bankRespo = RefundCrmApiClient::getNEFTBankAccount($login);
    $neftAccountList = array();
    
    if($bankRespo->isSuccess()){
    	$neftAccountList = $bankRespo->getData();
    	$neftAccountList = $neftAccountList['neftAccountEntry'];   	
    	$accounts = array();
    	
    	if(!empty($neftAccountList) && is_array($neftAccountList)){
    		foreach($neftAccountList as $index => $account) {
    			$accounts[(string)$account['neftAccountId']] = $account;
    		}    		
    		 
    		$decodedAccounts = array();
    		foreach($accounts as $key => $val) {
    			$decodedAccounts[$key] = $val;
    			foreach($val as $key1 => $val1) {
    				$decodedAccounts[$key][$key1] = html_entity_decode($val1);   
    				// mask account number by asterisks 				
    				if($key1 == "accountNumber"){    					
    					$decodedAccounts[$key][$key1] = maskString($val1);
    				}
    			}
    		}
    		
    		$accounts = $decodedAccounts;
    		 
    		return array(
    				'status'    => 'SUCCESS',
    				'neft_accounts' => $accounts,
    				'ids'       => array_keys($accounts),
    				'count'     => count($accounts),
    				'customerSupportTime' => $customerSupportTime,
    				'customerSupportCall' => $customerSupportCall
    		);
    	} 
    // no data   	    	
    } else {    	
    	return array(
    			'status'    => 'SUCCESS',// needed to handle at js side    			
    			'count'     => 0,// needed to handle at js side
    			'customerSupportTime' => $customerSupportTime,
    			'customerSupportCall' => $customerSupportCall
    	);
    }    
}


function getAccountForm() {
    global $smarty, $skin, $login, $abLocality;

    $customerSupportCall = WidgetKeyValuePairs::getWidgetValueForKey('customerSupportCall');
    $customerSupportTime = WidgetKeyValuePairs::getWidgetValueForKey('customerSupportTime');
    
    $action = filter_var($_REQUEST['_action'], FILTER_SANITIZE_STRING);
    $account = array();
    
    if ($action == 'edit') {
    	// TODO need to handle editing later
        $neftAccountId = filter_var($_REQUEST['id'], FILTER_SANITIZE_NUMBER_INT);
        //$account = func_select_first('mk_customer_address', array('login' => $login, 'id' => $id));        
    }
    
    $smarty->assign('account', $account);

    $markup = '';
    if ($_REQUEST['markup'] == 'yes') {
        $markup = $smarty->fetch('inc/neft-account-form.tpl');
    }
    
    $resp = array(
        'status'    => 'SUCCESS',
        'markup'    => $markup,
        'data'      => $account,        
        'customerSupportTime' => $customerSupportTime,
        'customerSupportCall' => $customerSupportCall
    );

    return $resp;
}


function postAccountForm() {
    global $smarty, $login, $abLocality ,$userToken;
    
    if (!checkCSRF($_POST["_token"], "neft_account")) {
        return array(
            'status'  => 'ERROR',
            'message' => 'Invalid access'
        );
    }    
    // in case of edit is set
    $neftAccountId = filter_input(INPUT_POST,'id', FILTER_SANITIZE_NUMBER_INT);
    
    $accountName = filter_input(INPUT_POST, 'account-name', FILTER_SANITIZE_STRING);
    $bankName = filter_input(INPUT_POST, 'bank-name', FILTER_SANITIZE_STRING);
    $accountNumber = filter_input(INPUT_POST, 'account-number', FILTER_SANITIZE_STRING);
    $reAccountNumber = filter_input(INPUT_POST, 're-account-number', FILTER_SANITIZE_STRING);
    $accountType = filter_input(INPUT_POST, 'account-type', FILTER_SANITIZE_STRING);
    $ifsc = filter_input(INPUT_POST, 'ifsc', FILTER_SANITIZE_STRING);
    $branch = filter_input(INPUT_POST, 'branch', FILTER_SANITIZE_STRING);

    $accountName = strtoupper(trim($accountName));
    $bankName = strtoupper(trim($bankName));
    $accountNumber = strtoupper(trim($accountNumber));
    $reAccountNumber = strtoupper(trim($reAccountNumber));
    $accountType = strtoupper(trim($accountType));
    $ifsc = strtoupper(trim($ifsc));
    $branch = trim($branch);   
    
    // array to hold all error messages for form elements
    $errors = array();
    
    // validate account number and confirm a/c number
    if($accountNumber != $reAccountNumber){    	
    	$errors['re-account-number'] = 'Re-entered bank account number does not match';
    }
    
    // validate all i/ps against regex    
    
    // customer name alphabets with spaces allowed(not at the start)
    if(!preg_match('/^[A-Z]{1}[A-Z\s]*$/', $accountName)){
    	$errors['account-name'] = 'Account holder name should be alphabetic only';
    }
    
    // bank name alphabets with spaces allowed(not at the start)
    if(!preg_match('/^[A-Z]{1}[A-Z\s]*$/', $bankName)){
    	$errors['bank-name'] = 'Bank name should be alphabetic only';
    }    
    
    if(strlen($ifsc) != 11) {
    	$errors['ifsc'] = 'IFSC code is incorrect. It should be 11 (eleven) characters only';
    	 
    } else {
	    // ifsc - 11 digits, first 4 alphabets, 5th digit a number and rest alphanumerics
	    $ifscFirstFourCharacters = substr($ifsc,0,4);
	    $ifscFifthDigit = substr($ifsc,4,1);
	    $ifscLastSixCharacters = substr($ifsc,5);
	    
	    if(!preg_match('/^[A-Z]{4}$/', $ifscFirstFourCharacters)){
	    	$errors['ifsc'] = 'IFSC code is incorrect. First four characters should be alphabetic only';
	    	
	    } else if($ifscFifthDigit != "0"){
	    	$errors['ifsc'] = 'IFSC code is incorrect. Fifth character should be 0 (zero) only';
	    	
	    } else if(!preg_match('/^[A-Z0-9]{6}$/', $ifscLastSixCharacters)){
	    	$errors['ifsc'] = 'IFSC code is incorrect. Last six characters should be alpha-numeric only';
	    }
    }
        
    // bank account number 3-20 digits alphanumerics
    if(!preg_match('/^[A-Z0-9]{3,20}$/', $accountNumber)){
    	$errors['account-number'] = 'Bank account number is incorrect. It should be between 3 to 20 alpha-numeric characters';
    }
    
    // bank account number 3-20 digits alphanumerics
    if(!preg_match('/^[A-Z0-9]{3,20}$/', $reAccountNumber)){
    	$errors['re-account-number'] = 'Bank account number is incorrect. It should be between 3 to 20 alpha-numeric characters';
    }
    
    // bank account type savings/current case insensitive
    if(!preg_match('/^(savings|current){1}$/i', $accountType)){
    	$errors['account-type'] = 'Bank account type should be "SAVINGS" or "CURRENT"';
    }
    
    // check for empty values in the form    
    foreach (array('account-name', 'bank-name', 'branch',  'ifsc', 'account-number', 're-account-number', 'account-type' ) as $var) {
    	if (empty($_POST[$var])) {
    		$errors[$var] = 'Please enter ' . $var;    		 
    	}
    }
    
    // show errors in the form (if any)
    if (!empty($errors)) {
    	return array(
    			'status'  => 'ERROR',
    			'message' => 'Data validation failed',
    			'errors'  => $errors
    	);
    }
    
	// create i/p array for bank neft save api client
    $refundEntryObj = array(
    		"login"=>$login,
    		"bankName"=>$bankName,
    		"branch"=>$branch,
    		"accountType"=>$accountType,
    		"accountNumber"=>$accountNumber,
    		"accountName"=>$accountName,
    		"ifscCode"=>$ifsc
    );
    
    // call bank neft account save apiclient
    $bankSaveRespo = RefundCrmApiClient::saveNEFTBankAccount($refundEntryObj);
    $data = $bankSaveRespo->getData();
    $neftAccountId = $data["neftAccountEntry"][0]["neftAccountId"];    

    if(empty($neftAccountId)){
    	return array(
    			'status'  => 'ERROR',
    			'message' => $bankSaveRespo->getErrorMsg(),    			
    	);
    }
    
    // mask account number to show only last few digits
    $refundEntryObj['accountNumber'] = maskString($refundEntryObj['accountNumber']);

    // set the response data
    $data = $refundEntryObj + array('neftAccountId' => $neftAccountId);
                    
    return array(
    	'status'  => 'SUCCESS',
   		'data'    => $data,
   		'_action'  => 'create'
    );
}



if(empty($login)) {
	$resp = array(
			'status'  => 'ERROR',
			'message' => 'Please login'
	);

} else {
	$view = $_REQUEST['_view'];
	
	if ($view == 'neft-account-list') {
		$resp = getNeftAccountList();

	} else if ($view == 'neft-account-form') {
		$resp = ($_SERVER['REQUEST_METHOD'] == 'GET') ? getAccountForm() : postAccountForm();
		
	} else {
		$resp = array(
				'status'  => 'ERROR',
        	    'message' => 'Invalid View'
        );
    }
}

header('Content-Type: application/json');
echo json_encode($resp);
exit;