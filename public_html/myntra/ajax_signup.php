<?php
/**
 * Created by PhpStorm.
 * User: myntra
 * Date: Jan 16, 2011
 * Time: 4:07:00 AM
 * To change this template use File | Settings | File Templates.
 */

require_once "../auth.php";
include_once("../include/func/func.mkcore.php");
include_once("../include/func/func.db.php");
include_once("../include/func/func.mail.php");
include_once("../include/func/func.randnum.php");
include_once("../include/func/func.user.php");
include_once ("../modules/coupon/mrp/RuleExecutor.php");
include_once "$xcart_dir/include/class/class.feature_gate.keyvaluepair.php";
include_once ("$xcart_dir/include/class/mcart/class.MCart.php");
require_once($xcart_dir.'/include/sessionstore/session_store_wrapper.php');

x_load('cart','crypt','user');
global $sql_tbl, $weblog;
$mode=filter_input(INPUT_POST,'mode',FILTER_SANITIZE_STRING);
$tb_customer = $sql_tbl['customers'];
$_curtime = time();
$profile_values = array();
$profile_values['email'] = mysql_real_escape_string(filter_input(INPUT_POST,'menu_username',FILTER_VALIDATE_EMAIL));
$profile_values['login'] = mysql_real_escape_string(filter_input(INPUT_POST,'menu_username',FILTER_VALIDATE_EMAIL));
$profile_values['mobile'] = mysql_real_escape_string(filter_input(INPUT_POST,'menu_mobile',FILTER_SANITIZE_NUMBER_INT));
$profile_values['usertype'] = "C";
$crypted = "";//TODO to remove once all passwords are ported to MD5
$profile_values['password'] = $crypted;
$profile_values['password_md5']= addslashes(user\UserAuthenticationManager::encryptToMD5($_POST['menu_password']));
$profile_values['cart']="test";
//$profile_values['firstname'] = $_POST['menu_username'];
$profile_values['termcondition'] = '1';
$profile_values['addressoption'] = 'M';
$profile_values['first_login'] = $_curtime;
$profile_values['last_login'] = $_curtime;
$profile_values['referer'] = mysql_real_escape_string(filter_input(INPUT_POST,'mrp_referer',FILTER_SANITIZE_STRING));
$coupon = mysql_real_escape_string(filter_input(INPUT_POST,'rcoupon',FILTER_SANITIZE_STRING));
$redirect_page = mysql_real_escape_string(filter_input(INPUT_POST,'menu_redirect_page',FILTER_SANITIZE_STRING));
$refid = mysql_real_escape_string(filter_input(INPUT_POST,'refid',FILTER_SANITIZE_STRING));
$tb_customer = $sql_tbl['customers'];

if(empty($profile_values['login'])) {
	$msg=array("status"=>'error',"redirect"=>'',"str"=>$str,"errorcode"=>"1");
	echo json_encode($msg);
	exit;
}

$checkuserid = db_query("SELECT login, password, firstname, account_type,mobile, first_login FROM $tb_customer WHERE login = '".($profile_values['login'])."' AND password='".($profile_values['password'])."'");
$numrows = db_num_rows($checkuserid);
$row  = db_fetch_array($checkuserid);
//for existing user login:- if login and password doesn't match then return error
if($mode == 'signin' && empty($numrows))
{
	$msg=array("status"=>'error',"redirect"=>'',"str"=>$str);
	$checkUserExists = func_query("SELECT login from $tb_customer WHERE login='".($profile_values['login'])."';");
	if(!empty($checkUserExists))	{
		$msg["errorcode"] = "1";
	} else {
		$msg["errorcode"] = "2";
	}
    echo json_encode($msg);
    exit;
}
else if($mode == 'signup')
{
    $check_email_exists = db_query("SELECT login, password, firstname, account_type, first_login FROM $tb_customer WHERE login = '".($profile_values['login'])."' ");
    $c_numrows = db_num_rows($check_email_exists);
    if(!empty($c_numrows))
    {
        $msg=array("status"=>'error',"redirect"=>'',"str"=>$str,"errorcode"=>"1");
        echo json_encode($msg);
        exit;
    }
   
	include_once "$xcart_dir/modules/coupon/mrp/SmsVerifier.php";
	$smsVerifier = SmsVerifier::getInstance();
	$smsVerifier->addNewMapping($profile_values[mobile],$profile_values[login]);
	
}

		if(!empty($profile_values['login'])) {
			$checkFBuserid = db_query("SELECT fb_uid FROM mk_facebook_user WHERE myntra_login='".($profile_values['login'])."'");
			$numFBrows = db_num_rows($checkFBuserid);
			$FBrow  = db_fetch_array($checkFBuserid);
			$fb_uid = '';
			if(!empty($numFBrows)){
				x_session_register('fb_uid');
				$fb_uid = $FBrow['fb_uid'];
			}
		}

        x_session_register("userfirstname");
        x_session_register("account_type");
   

        #db_query("update mk_session set updated='Y' where sessionid= '$XCARTSESSID'");
        updateSessionUpdatedFlagBySessionId($XCARTSESSID, 'Y');

        if(!empty($numrows))
        {
            db_query("UPDATE $sql_tbl[customers] SET last_login='$_curtime' WHERE login = '".($profile_values['login'])."'");
            $account_created_date = $row['first_login'];
        }
        else
        {
            func_array2insert('customers', $profile_values);
            
            // Update the referral log.
            include_once "$xcart_dir/modules/coupon/database/CouponAdapter.php";
            $couponAdapter = CouponAdapter::getInstance();
            $couponAdapter->customerJoined($profile_values['login'], $profile_values['referer']);
	 
            // Synchronous call to the MRP New Login rule.
            $logging_str= "Generating First login coupons for ".$profile_values['login'];
            $weblog->warn($logging_str);
            $ruleExecutor = RuleExecutor::getInstance();
            $ruleExecutor->runSynchronous("customer", $profile_values['login'], "1");    // "1" is the ruleId for "NewLogin" rule.
            $weblog->warn("Calling the ruleid 1");
            $account_created_date=$profile_values['first_login'];
        }
        $auto_login = true;
        $auto_login = true;
        $login = $profile_values['login'];
        $login_type = $profile_values['usertype'];
        $userfirstname = $row['firstname'];
        $mobile = $row['mobile'];
        
		
        if(empty($mobile)){
        	$mobile=$profile_values['mobile'];
        }
        x_session_register("login",$login);
        x_session_register("mobile",$mobile);
        x_session_register("login_type",$login_type);
        $logged = "";
        x_session_register("identifiers",array());
        $identifiers[$menu_usertype] = array (
            'login' => $login,
            'firstname' => $userfirstname,
            'login_type' => $login_type,
       		'account_type' => $account_type,
         	'first_login_date' => $account_created_date,
        );
     $flag = "S";
     $referer = base64_decode($_POST['addreferer']);
     $referer = urldecode($referer);
     if(strpos($referer, "?")) $script = $referer."&success=I";
     else $script = $referer."?success=I";

     if(!empty($coupon))
     $script .= "&c=1";

     $str = $flag.",".$script;

     if(!empty($redirect_page))
        $str .= ",".$redirect_page;

     if(!empty($coupon))
          $str .= ",".$coupon;

     if(!empty($refid))
        $str .= ",".$refid;
     //   echo $str;
     if(!empty($userfirstname)){
    	$sub_str=substr($userfirstname, 0, 12);
    	$logged_in_user=(strlen($userfirstname)>15)?$sub_str."...":$sub_str;
     } else {
     	$sub_str=substr($login, 0, 12);
    	$logged_in_user=(strlen($login)>15)?$sub_str."...":$sub_str;
     }

     $msg=array("status"=>'success',"redirect"=>'',"str"=>$str,"username"=>$logged_in_user,"fb_uid"=>$fb_uid);
	 
	include_once "$xcart_dir/modules/coupon/database/CouponAdapter.php";
	$couponAdapter = CouponAdapter::getInstance();
	$mrp_header_message = $couponAdapter->getHeaderMessage($profile_values['login']);
		
    $msg["hMesg"] = $mrp_header_message;
    $msg["promoHeaderLoggedinText"] = $promoHeaderLoggedinText;

	
	$cashback_gateway_status = FeatureGateKeyValuePairs::getFeatureGateValueForKey('cashbackenabled');
	if($cashback_gateway_status == "internal") {
		$pos = strpos($profile_values['login'], "@myntra.com");
		if($pos === false)
		$cashback_gateway_status = "off";
		else
		$cashback_gateway_status = "on";
	}
	$msg["cashback"] = $cashback_gateway_status ;			
	  
     $checkallreadybought = db_query("SELECT orderid, login from xcart_orders WHERE login = '".($profile_values['login'])."'");
     $ab_numrows = db_num_rows($checkallreadybought);
     if(empty($ab_numrows))
	$msg["fbuyer"]="No";
     else 
	$msg["fbuyer"]="Yes";
    $msg['email']=$login;
    $msg['mobile']=$mobile;
    $checkCODPaid = func_query_first_cell("select login from xcart_orders where login='".($profile_values['login'])."' and payment_method='cod' and (cod_pay_status='paidtobluedart' or cod_pay_status='paid') limit 1");
    x_session_unregister("oneClickCODElegible");
    if(!empty($checkCODPaid)){
    	x_session_register("oneClickCODElegible","true");
    	$msg["oneClickCODElegible"] = "true";
    } else {
    	x_session_register("oneClickCODElegible","false");
    	$msg["oneClickCODElegible"] = "false";
    }
    $mcartFactory= new MCartFactory();
    $mcart=$mcartFactory->getCurrentUserCart();
    $msg['itemsincart']=0;
    x_session_register("totalAmountCart","0");
    $amountCart=0;
    if($mcart!=null){
    	$msg['itemsincart']=$mcart->getItemQuantity();
        x_session_register("oneClickCODElegible","false");
        $XCART_SESSION_VARS["totalQuantity"]=$mcart->getItemQuantity();
        $amountCart=$mcart->getMrp()+$mcart->getAdditionalCharges()-$mcart->getDiscount()-$mcart->getCashDiscount()-$mcart->getDiscountAmount()+$mcart->getShippingCharge();
        if(x_session_is_registered('totalAmountCart'))
            $XCART_SESSION_VARS["totalAmountCart"]=$amountCart;
        else
            x_session_register('totalAmountCart',$amountCart);
    }
     $msg['totalAmountCart'] = $amountCart;
    // display the amount earnable from refer and earn
    global $menuDisplayRefAmount;
    $msg['refAmount'] = $menuDisplayRefAmount;	  
    echo json_encode($msg);

?>