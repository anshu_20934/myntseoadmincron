<?php
//************************************************************ /
// INCLUDE ALL FILES REQUIRED                                  /
//************************************************************ /
use abtest\MABTest;
use style\StyleGroup;
include_once("./auth.php");
include_once($xcart_dir."/Profiler/Profiler.php");
Profiler::setContext("PDP-V2");
include_once("$xcart_dir/include/class/instrumentation/BaseTracker.php");
include_once($xcart_dir."/include/func/func.mkcore.php");
require_once($xcart_dir."/include/func/func.utilities.php");
require_once($xcart_dir."/include/class/widget/class.static.functions.widget.php");
include_once($xcart_dir."/include/class/search/UserInterestCalculator.php");
include_once($xcart_dir."/include/class/class.feature_gate.keyvaluepair.php");
include_once($xcart_dir."/include/class/notify/class.notify.php");
$publickey  = "6Le_oroSAAAAANGCD-foIE8DPo6zwcU1FaW3GiIX";
$privatekey = "6Le_oroSAAAAAKuArW6iPxhOlk0QVJMwWOTGVQnt";
include_once($xcart_dir."/include/func/func.utilities.php");
require_once($xcart_dir."/tracking/trackingUtil.php");
require_once($xcart_dir."/modules/discount/DiscountEngine.php");
include_once($xcart_dir."/Cache/Cache.php");
require_once("$xcart_dir/include/class/search/SearchProducts.php");
include_once "$xcart_dir/modules/vat/VatCalculator.php";

use seo\SeoStrategy;
use seo\SeoPageType;
use seo\PDPBreadCrumb;
use seo\SeoBreadCrumbKeyEnum;
use macros\PageMacros;
use style\builder\CachedStyleBuilder;
use style\builder\StyleBuilder;
use revenue\payments\service\PaymentServiceInterface;
#require_once "$xcart_dir/storyboard.php";

$pageName = 'pdp';
include_once $xcart_dir."/webengage.php";
include_once $xcart_dir."/gtm.php";

if(isset($HTTP_GET_VARS["id"]) ) {
	$productStyleId=mysql_real_escape_string(filter_input(INPUT_GET,"id", FILTER_SANITIZE_NUMBER_INT));
}

if(isset($HTTP_GET_VARS["previous_style_id"]) ) {
	$previousProductStyleId=mysql_real_escape_string(filter_input(INPUT_GET,"previous_style_id", FILTER_SANITIZE_NUMBER_INT));
}

if(isset($HTTP_GET_VARS["previous_recommendation_type"]) ) {
	$previousRecommendationType=mysql_real_escape_string(filter_input(INPUT_GET,"previous_recommendation_type"));
}

if(isset($HTTP_GET_VARS["previous_recommendation_styleids"]) ) {
	$previousRecommendationStyleIds=mysql_real_escape_string(filter_input(INPUT_GET,"previous_recommendation_styleids"));
}

$expressBuyEnabled = false;
if(!empty($login)){

	$expressBuyEnabled = PaymentServiceInterface::showExpressBuyForUser($login);
}
$smarty->assign('expressBuyEnabled',$expressBuyEnabled);	

/* Check if nav id is passed else just generate it and assign*/
$nav_id = filter_input(INPUT_GET, 'nav_id', FILTER_SANITIZE_NUMBER_INT);
 $nav_id = mysql_real_escape_string(htmlentities($nav_id));
if(empty($nav_id)){
    /* Check if nav id is already assigned to style*/
    $navQuery="select nav_id from mk_style_properties where style_id='$productStyleId'";
    $out = func_query_first($navQuery,true);
    $nav_id=$out['nav_id'];
    if(empty($nav_id)){
    $nav_id = WidgetUtility::generateNavId($productStyleId);
    }
}
require_once "$xcart_dir/myntra/navhighlighter.php";
require_once("$xcart_dir/socialInit.php");

$pdpMiniNav = FeatureGateKeyValuePairs::getBoolean('pdp.mininav.enabled');
$smarty->assign("pdpMiniNav", $pdpMiniNav);

$pdpColorGrouping = FeatureGateKeyValuePairs::getBoolean('pdp.colorgrouping.enabled');
$smarty->assign("pdpColorGrouping", $pdpColorGrouping);

$pdpSocial = FeatureGateKeyValuePairs::getBoolean('pdp.social.enabled');
$smarty->assign("pdpSocial", $pdpSocial);

$icici3MONTHMinEMITotalAmount = FeatureGateKeyValuePairs::getInteger('payments.icici3MONTHMinEMITotalAmount', 0);
$smarty->assign('icici3MONTHMinEMITotalAmount', $icici3MONTHMinEMITotalAmount);

$icici6MONTHMinEMITotalAmount = FeatureGateKeyValuePairs::getInteger('payments.icici6MONTHMinEMITotalAmount', 0);
$smarty->assign('icici6MONTHMinEMITotalAmount', $icici6MONTHMinEMITotalAmount);

$icici3MONTHEMICharge = FeatureGateKeyValuePairs::getFeatureGateValueForKey('payments.icici3MONTHEMICharge');
$smarty->assign('icici3MONTHEMICharge',$icici3MONTHEMICharge);

$icici6MONTHEMICharge = FeatureGateKeyValuePairs::getFeatureGateValueForKey('payments.icici6MONTHEMICharge');
$smarty->assign('icici6MONTHEMICharge',$icici6MONTHEMICharge);

$tracker = new BaseTracker(BaseTracker::PDP);

$shownewui= $_MABTestObject->getUserSegment("productui");
$productuiABparam=$shownewui;

$smarty->assign("shownewui", $shownewui);
$smarty->assign("productuiABparam", $productuiABparam);


$productStyleId=sanitize_int($productStyleId);

$cachedStyleBuilderObj=CachedStyleBuilder::getInstance();
$styleObj=$cachedStyleBuilderObj->getStyleObject($productStyleId);
// validate productStyleid

if($styleObj == NULL){
	$XCART_SESSION_VARS['errormessage']="Requested page was not found on this server";
    func_header_location("mksystemerror.php");
    exit;
}
$style_properties= $styleObj->getStyleProperties();
$smarty->assign("style_properties",$style_properties);
//visual tag
$producttags = explode('|',$style_properties["product_tags"]);
$producttags[1] = trim($producttags[1]);
if (!empty($producttags[1])) {
	$producttags = explode(':', $producttags[1]);
	$producttags[0] = strtolower(trim($producttags[0]));
	$producttags[1] = strtolower(trim($producttags[1]));
	if(!empty($producttags[0]) && !empty($producttags[1])) {
		$smarty->assign('visualtagText', $producttags[0]);
		$smarty->assign('visualtagCssClass', $producttags[1]);
	}	
}

//Loyalty 
$loyalty = $styleObj->getLoyaltyFactor();
$smarty->assign('loyaltyFactor',$loyalty);




// If the URL through which the page is accessed does not match the default constructed URL, add a meta tag for canonical redirect to default URL
$queryStrIndex = strpos($_SERVER['REQUEST_URI'], '?');
$uri = $queryStrIndex ? substr($_SERVER['REQUEST_URI'], 0, $queryStrIndex) : $_SERVER['REQUEST_URI'];
if (($style_properties['pdpUrl'] && ($uri != '/'.$style_properties['pdpUrl'])) || $queryStrIndex !== false) {
    $showCanonicalTag = true;
    $canonicalPageUrl = $http_location.'/'.$style_properties['pdpUrl'];
    $smarty->assign('canonicalPageUrl', $canonicalPageUrl);
}
else {
    $showCanonicalTag = false;
}

$isStyle = true;

include_once($xcart_dir."/include/class/class.recent_viewed_content.php");
$__RECENT_HISTORY_DCONTENT->setGACategory("pdp_strip");
$__RECENT_HISTORY_DCONTENT->genContent();
$__DCONTENT_MANAGER->startCapture();

$recentlyViewedStyles = $__RECENT_HISTORY_DCONTENT->addStyleToRecentViews($productStyleId);
//Kundan: fixed the Header RECENT COUNT
$recentViewsCount=sizeof($recentlyViewedStyles);
$smarty->assign('recentViewedCount',$recentViewsCount);

/*** Cod limit values from configuration ***/
$codAmountRange = trim(FeatureGateKeyValuePairs::getFeatureGateValueForKey('cod.limit.range'));
$codAmountRangeArray = explode("-", $codAmountRange);
$smarty->assign('codMinVal',$codAmountRangeArray[0]);
$smarty->assign('codMaxVal',$codAmountRangeArray[1]);

// ***************************************************************  /
// Start Filling Smarty data to load view		                    /
// ***************************************************************  /

$productStyleDetails=$styleObj->getProductStyleDetails();

$styleiddetail=$styleObj->getStyleIdDetails();

//color group styles selection
$relatedStylesDetailsByGroupId=$styleObj->getStyleIdDetailsForColorGroup();
$relatedColorStyleDetailsArray=array();
if (!empty($relatedStylesDetailsByGroupId)){
	foreach ($relatedStylesDetailsByGroupId as $value)
	{	if ($value["styleid"] != $productStyleId){

			$articletype = $value['global_attr_article_type'];
			$brandname = $value['global_attr_brand'];
			$stylename = $value['stylename'];
			$styleid = $value['styleid'];
			$landingpageurl = construct_style_url($articletype,$brandname,$stylename,$styleid);
			$relatedColorStyleDetailsArray[$styleid] = array(
				"base_color" => $value['global_attr_base_colour'],
				"color1" => $value['global_attr_colour1'],
				"url" => $landingpageurl,
				"image" => $value['search_image']
			);
		}
		else
		{
			$smarty->assign('styleBaseColor',$value['global_attr_base_colour']);
			$smarty->assign('styleColor1',$value['global_attr_colour1']);
			$smarty->assign('styleArticleType',$value['global_attr_article_type']);
		}
	}
}
if(!empty($relatedColorStyleDetailsArray)){
	$smarty->assign('relatedColorStyleDetailsArray',$relatedColorStyleDetailsArray);
}


$productTypeId=strip_tags($styleiddetail["product_type"]);
$productTypeLabel=strip_tags($styleiddetail["name"]);
$currentPageURL=currentPageURL();
$prettyURL=strrpos($currentPageURL,'?');
if(!empty($prettyURL)){
	$pageCleanURL=substr($currentPageURL,0,strrpos($currentPageURL,'?'));
}
else {
	$pageCleanURL=$currentPageURL;
}

//Clean URL for fb like and Google Plus
$smarty->assign("pageTypePdp",'pdp');
$smarty->assign("pageCleanURL",$pageCleanURL);
$smarty->assign("pageURL",currentPageURL());

//NEW SIZE CHART
$dynamicSizeChart=FeatureGateKeyValuePairs::getFeatureGateValueForKey('sizechart.dynamicSizeChart');
$smarty->assign("dynamicSizeChart",$dynamicSizeChart);

// load size chart image
//encode single quotes to %27 if any
$size_chart_image_path = str_replace("'", "%27", $productStyleDetails["size_chart_image"]);
$isCustomizable = $productStyleDetails["is_customizable"];
$smarty->assign("size_chart_image_path",$size_chart_image_path);

$enableAllStyles=FeatureGateKeyValuePairs::getBoolean('enableAllStyles');
if(!$enableAllStyles){
	$row_disableStyle = $styleObj->getProductStyleCount();
}
$disableProductStyle = false;

$myntraUser = false;
$pos = strpos($login, "@myntra.com");
if($pos !== false)
	$myntraUser = true;

$mplstyles = explode(",", trim(FeatureGateKeyValuePairs::getFeatureGateValueForKey('mpl.styles')));

if(empty($row_disableStyle) &&(!$enableAllStyles) || ( in_array($productStyleId, $mplstyles) && !$myntraUser ) ) {
	$disableProductStyle = true;
	$disableMessage =  	"This product is currently out-of-stock. ";
	$smarty->assign("disableProductStyle",$disableProductStyle);
	$smarty->assign("disableMessage",$disableMessage);
}

//AB Test for Pincode Widget
$smarty->assign("pincodeWidgetVariant", MABTest::getInstance()->getUserSegment('PincodeWidget'));

//AB Test for Cross selling recommendations
$smarty->assign("crossSell", MABTest::getInstance()->getUserSegment('crossSell'));

//AB test for new checkout flow /Used for new Combo page as well
$smarty->assign("checkout", MABTest::getInstance()->getUserSegment('checkout'));

$productStyleLabel = $productStyleDetails["label"];
$descriptionAndDetails = $productStyleDetails["description"];
// Check if there is any discount applied.
$originalprice = $productStyleDetails["price"];

$smarty->assign("originalprice", $originalprice);
$smarty->assign("descriptionAndDetails",$descriptionAndDetails);
$smarty->assign("productTypeType",$productStyleDetails['type']);
$smarty->assign("productTypeLabel",$productTypeLabel);
$smarty->assign("productStyleDetails",$productStyleDetails);
$smarty->assign("productStyleLabel",$productStyleLabel);
$smarty->assign("productStyleId",$productStyleId);
$smarty->assign("productTypeId",$productTypeId);

// Load all product options
$allProductOptionDetails=$styleObj->getAllProductOptions();
$smarty->assign("allProductOptionDetails",json_encode($allProductOptionDetails));
$productOptionDetails=$styleObj->getActiveProductOptionsData();
$inStock = 1;
if (empty($productOptionDetails)) {
    $inStock = 0;
}
$smarty->assign("inStock",$inStock);

$inactiveProductOptionDetails=$styleObj->getInactiveProductOptionsData();
$productOptionNameValues=$styleObj->getProductOptionNameValues();
$discountedprice = $originalprice;

$discountData= $styleObj->getDiscountData();
//discountData->displayText->id is being used as a macro.
$discountType = $discountData->discountType;
$discountAmount = $discountData->discountAmount;
$discountPercent = round($discountData->discountPercent,2);

foreach($discountData->discountToolTipText->params as $k=>$v) {
    $smarty->assign("dre_".$k, $v);
}
foreach($discountData->displayText->params as $k=>$v) {
    $smarty->assign("dre_".$k, $v);
}

$discountSetStyles = $discountData->discountToolTipText->relatedProducts;

if($discountSetStyles!=null && count($discountSetStyles)>0 ) {
    $smarty->assign("show_combo", true);
}else{
    $smarty->assign("show_combo", false);
}

$smarty->assign("discountamount", round($discountAmount));
$smarty->assign("newPrice", round($originalprice-$discountAmount));
$smarty->assign("discountId", $discountData->discountId);
$smarty->assign("discounttype", $discountType);
$smarty->assign("discountlabeltext", $_rst[$discountData->displayText->id]);
$smarty->assign("discounttooltiptext", $_rst[$discountData->discountToolTipText->id]);

$finalprice  = ($discountedprice>0) ? $discountedprice : $originalprice;

$defaultImage = $style_properties['default_image'];
$areanames = array('default', 'top', 'front', 'back', 'right', 'left', 'bottom');

$area_icons =  array();

foreach($areanames as $singlearea) {
	if(!empty($style_properties["$singlearea" . "_image"]) && $style_properties["$singlearea" . "_image"]!="$cdn_base/") {
		$singleareaproperties = array();
		$imagepath = $style_properties["$singlearea" . "_image"];
			
		$singleareaproperties["image"] = $imagepath;
		$singleareaproperties["name"] = $singlearea;
		
		// With new rectangular images in places, area icons are 48x64 pixels.
		$singleareaproperties["area_icon"] = str_replace('_images.', '_images_48_64.', $imagepath);
			
		$area_icons[] = $singleareaproperties;
	}
}

$smarty->assign('style_note',$style_properties["style_note"]);
$smarty->assign('materials_care_desc',$style_properties["materials_care_desc"]);
$smarty->assign('size_fit_desc',$style_properties["size_fit_desc"]);
if(!empty($style_properties["style_note"])){
	$smarty->assign('fb_meta_decription',strip_tags($style_properties["style_note"]));
}
else {
	$smarty->assign('fb_meta_decription','Buy '.$style_properties['product_display_name'].' at Myntra');
}

$smarty->assign("defaultImage",$defaultImage);
$smarty->assign("area_icons", $area_icons);
$smarty->assign("pageTitle", $style_properties['product_display_name']." | Myntra");
$smarty->assign("productDisplayName", $style_properties['product_display_name']);
$smarty->assign("objectType","product");

// For Facebook metaDescription.
$meta = 'Buy ' . $style_properties['product_display_name'] . ' online at Rs. ' . $finalprice . ' . ';
$meta.= ' In stock and ready to ship. All India Free Shipping and Cash On Delivery available';

// Fetching related products to show on right column		
if(!empty($style_properties['product_tags'])) {
	$producttags= explode(",",$style_properties['product_tags']);
}

if(!empty($producttags)) {

	$keywords = array();
	foreach($producttags as $t) {
		$keywords[] = trim($t);
	}
		
	$keywords [] = 'sportswear';
	$keywords = array_merge($keywords, explode(" ",$style_properties['product_display_name']));
			
	$smarty->assign("metaKeywords", implode(",", $keywords));
		
	$meta = 'Buy ' . $style_properties['product_display_name'].' online at Rs. ';
			
	if($discountedprice>0) {
		$meta.= "$discountedprice";
	} else {
		$meta.= "$originalprice";
	}
}

$buynowdisabled=false;
if(empty($productOptionDetails)) {
	$buynowdisabled=true;
	$smarty->assign("buynowdisabled", true);
	$meta.= '. In stock and ready to ship. All India Free Shipping and Cash On Delivery available';
}else{
	$buynowdisabled=false;
	$smarty->assign("buynowdisabled", false);
}

$smarty->assign("metaDescription", $meta);


if($isStyle && !empty($productStyleId)) {
	//UserInterestCalculator::trackAction($productStyleId, UserInterestCalculator::PRODUCT_VIEW);
}
	
$smarty->assign("isreturninguser", ($expiry_time-time())/(30*24*60*60)<=14?"returning":"new");

$sizeSpecUsed="";

$flattenSizeOptions=false;
$gender=$style_properties["global_attr_gender"];
$brand_name=$style_properties["global_attr_brand"];
$agegroup=$style_properties["global_attr_age_group"];

$brand_filters=$styleObj->getBrandData();
if(!empty($brand_filters['logo'])) {
$smarty->assign("brand_logo", $brand_filters['logo']);
}
$smarty->assign("brandname", $brand_name);
if(empty($gender) || $gender == "NA"){
	$gender="Men";
}

//Retrieving data from mk_catalogue_classification for the product
$catalogData = $styleObj->getCatalogueClassificationData();

$smarty->assign("catalogData",$catalogData);
$articleType = $catalogData["articletype"];
$masterCategory = $catalogData["master_category"];
$masterCategoryID = $catalogData['master_category_id'];
$productDisclaimerTitle = $catalogData["disclaimer_title"];
$productDisclaimerText = $catalogData["disclaimer_pdp_display_text"];
$sizeRepresentationImageUrl=$style_properties["size_representation_image_url"];
$smarty->assign("articleTypeId",$catalogData["article_id"]);

$smarty->assign("masterCategory",$masterCategory);
$smarty->assign("masterCategoryId",$masterCategoryID);

//macros
$macros = new PageMacros('PDP');
$macros->url = currentPageURLWithStripedQueryStringsAndPort();
$macros->page_type = SeoPageType::PDP_PAGE;
$macros->page_name = 'PDP';
$macros->style_id = $productStyleDetails["id"];
$macros->vendor_id = $style_properties['article_number'];
$macros->name = $style_properties['product_display_name'];
$macros->image = $style_properties['default_image'];
$macros->search_image = $style_properties['search_image'];
$macros->brand = $style_properties["global_attr_brand"];
$macros->article_type = $catalogData["articletype"];
$macros->sub_category = $catalogData['sub_category'];
$macros->category = $catalogData['master_category'];
$macros->gender = $style_properties["global_attr_gender"];
$macros->price = $productStyleDetails["price"];
$macros->discount = $discountData->discountAmount;
$macros->discounted_price = $macros->price - $macros->discount;
$macros->in_stock = $inStock;
$macros->colour = $style_properties["global_attr_base_colour"];
$macros->discount_string = $_rst[$discountData->displayText->id]?$_rst[$discountData->displayText->id]:'';
$smarty->assign('macrosObj',$macros);

$seoEnabled=FeatureGateKeyValuePairs::getBoolean('SeoV3Enabled');
if($seoEnabled){
    try {
		$handle = Profiler::startTiming("seov3_load");
        $seoStrategy = new SeoStrategy($macros);
        Profiler::endTiming($handle);
        $smarty->assign("seoStrategy",$seoStrategy);
    } catch (Exception $e) {
        $seolog->error("pdp :: ".$e->getMessage());
    }
}


/* paroksh */
$showSpecificAttributeFitInfo = $styleObj->getSpecificAttributeFitInfo();
$smarty->assign("specificAttributeFitInfo",$showSpecificAttributeFitInfo);
/* paroksh ends */

$smarty->assign("productDisclaimerTitle",$productDisclaimerTitle);
$smarty->assign("productDisclaimerText",$productDisclaimerText);

$sizeOptions=$styleObj->getSizeUnificationDataForAvailableOptions();
$allSizeOptions=$styleObj->getSizeUnificationDataForAllOptions();
// not a db call
$flattenSizeOptions = Size_unification::ifSizesUnified($sizeOptions);
$flattenSizeOptionsAll = Size_unification::ifSizesUnified($allSizeOptions);
$allSizeKeys=array_keys($allSizeOptions);

foreach ($inactiveProductOptionDetails as $row){
	$unavailabeOptions[$row['value']]=$row;
	if($flattenSizeOptionsAll){
		$unavailabeOptions[$row['value']]['unified']=$allSizeOptions[$row['value']];
	}
}

$sizeOptionsTooltip=$styleObj->getSizeOptionsTooltipData();

$sizeOptionSKUMapping = array();
$allSizeOptionSKUMapping =array();
$sizeOptionsTooltipSKUMapping = array();
$allSizeOptionsTooltipSKUMapping = array();
$ShowSuppliedByPartner = false;
foreach ($sizeOptions as $key=>$value) {
	foreach ($productOptionDetails as $options) {
		if($options['value'] == $key) {
			$sizeOptionSKUMapping[$options['sku_id']] = $value;
			$sizeOptionsTooltipSKUMapping[$options['sku_id']] = $sizeOptionsTooltip[$key];
		}
	}
}
foreach ($allSizeOptions as $key=>$value) {
	foreach ($allProductOptionDetails as $options) {
		if($options['value'] == $key) {
			$isAvailable = ($options['sku_count'] > 0 && !empty($options['is_active']));
			$allSizeOptionSKUMapping[$options['sku_id']] = array($value,$isAvailable,$options['sku_count'],array(
					'availableInWarehouses'  => $options['availableInWarehouses'],
				    'leadTime' => $options['leadTime'],
				    'supplyType' => $options['supplyType']
				));
			$allSizeOptionsTooltipSKUMapping[$options['sku_id']] = $sizeOptionsTooltip[$key];
			$ShowSuppliedByPartner = ($options['supplyType'] === 'JUST_IN_TIME' ? true : $ShowSuppliedByPartner);
		}
	}
}

$sizeOptionsTooltipHtmlArray=array();
if($flattenSizeOptions){
	//iterate over all the Size options: each id (key) is size option id
	foreach ($allSizeOptionSKUMapping as $idx => $sizeopt){
		//corresponding to the size option id, fetch the tooltip
		//PORTAL-2712: don't add blank HTML Markup if there is no tooltip data to show
		$str="";
		if(!empty($allSizeOptionsTooltipSKUMapping[$idx])){
			$sizeopt_breakdown=explode(",", $allSizeOptionsTooltipSKUMapping[$idx]);
			$str="<table>";
			foreach($sizeopt_breakdown as $sizeopt_breakdown_idx=>$sizeopt_breakdown_val){
				$str .="<tr>";
				if((strpos($sizeopt_breakdown_val, "/") === false) && (strpos($sizeopt_breakdown_val, "-") === false)){
					$alpha_val=ereg_replace("[^A-Z]", "", $sizeopt_breakdown_val);
					$numeric_val=ereg_replace("[^0-9.]", "", $sizeopt_breakdown_val);
					if(!empty($alpha_val)){
						$str .= "<td>" . ($alpha_val == 'UK'?'UK/IN':$alpha_val) . "</td>";
					}
					if(!empty($numeric_val)){
						$str .= "<td>" . $numeric_val . "</td>";	
					}
				}
				else{
					$str .= "<td>" . $sizeopt_breakdown_val . "</td>";
				}
				$str .="</tr>";
			}
			$str .= "</table>";
		}

		$sizeOptionsTooltipHtmlArray[$idx]=$str;
		//defines the coordinates in the Div created on hover
		$tooltipTop=count($sizeopt_breakdown) * 25 + 25;		
		$smarty->assign("tooltipTop",$tooltipTop);
	}
}

$sizeScale;
if($flattenSizeOptions)
	$sizeScale=Size_unification::getSizeScaleFromOption($allSizeKeys[0]);
$smarty->assign("sizeScale",$sizeScale);
$smarty->assign("unavailableOptions",$unavailabeOptions);

//Featuregates for sizechart and tooltip
$tooltipFlag=FeatureGateKeyValuePairs::getBoolean('PDPShowTooltip');
$PDPShowTooltip=$flattenSizeOptions && $tooltipFlag;
$renderTooltip=$PDPShowTooltip;	
$renderSizeChart=!$PDPShowTooltip;
//retargeting tracking code
$retargetPixel = retargetingPixelOnType($productTypeId);
if(!empty($retargetPixel)){
    $smarty->assign("retargetPixel",$retargetPixel);
}    
$mobile = $XCART_SESSION_VARS['mobile'];
//echo "<pre>sizeOptionSKUMapping";print_r($allSizeOptionSKUMapping); echo "</pre>";
$smarty->assign("sizeOptionSKUMapping",$sizeOptionSKUMapping);
$smarty->assign("ShowSuppliedByPartner",$ShowSuppliedByPartner);
$smarty->assign("allSizeOptionSKUMapping",$allSizeOptionSKUMapping);
$smarty->assign("renderTooltip",$renderTooltip);
$smarty->assign("renderSizeChart",$renderSizeChart);
$smarty->assign("sizeSpecUsed",$sizeSpecUsed);
$smarty->assign("gender",$gender);
$smarty->assign("sizeOptionsMapping",$sizeOptionsMapping);
$smarty->assign("sizeOptions",$sizeOptions);
$smarty->assign("sizeOptionsTooltipHtmlArray",$sizeOptionsTooltipHtmlArray);
$smarty->assign("productOptions",$productOptionNameValues);
$smarty->assign("flattenSizeOptions",$flattenSizeOptions);
$smarty->assign("mobile",$mobile);
$smarty->assign("notifymefeatureon",FeatureGateKeyValuePairs::getBoolean(Notifications::$_CUSTOMER_INSTOCK_NOTIFYME.'.enable')==true);

$smarty->assign("sizeRepresentationImageUrl",$sizeRepresentationImageUrl);

$deliveryKnowMoreEnabled = FeatureGateKeyValuePairs::getBoolean('deliveryKnowMoreEnabled');
$deliveryKnowMoreEnabled = $deliveryKnowMoreEnabled && !$buynowdisabled && !$disableProductStyle; 
$smarty->assign("deliveryKnowMoreEnabled", $deliveryKnowMoreEnabled);

//zoom enable/disale
$zoomenabled = FeatureGateKeyValuePairs::getBoolean('zoom.enabled', true) && MABTest::getInstance()->getUserSegment('HoverZoom') == 'test';
$smarty->assign("zoomenabled",$zoomenabled);

$showMyntraRecommendations = $_MABTestObject->getUserSegment("showMyntraRecommendations");
$smarty->assign("showMyntraRecommendations",$showMyntraRecommendations);

$deliveryKnowMoreMsg=WidgetKeyValuePairs::getWidgetValueForKey("deliveryKnowMoreMsg");
if($deliveryKnowMoreMsg != NULL){
	$smarty->assign("deliveryKnowMoreMsg", $deliveryKnowMoreMsg);
}

$smarty->assign("shippingRate", $system_shipping_rate);
/*FOR AVAIL RECOMMENDATION*/
$avail_pdp_abtest= $_MABTestObject->getUserSegment("avail_pdp_abtest");
$smarty->assign("avail_pdp_abtest", $avail_pdp_abtest);
/* Master Category Wrapper - Anjana*/
//replacing spaces with hyphen
$masterCategory = str_replace(" ", "-", $masterCategory);
/* Master Category Wrapper ends */
/*Avail recommendations for similar products tab*/
$avail_dynamic_param = "$gender,$masterCategory";
/*Avail recommendations for matchwith products tab*/
$cross_sell_avail_dynamic_param1 = "$gender";
$cross_sell_avail_dynamic_param2 = $styleObj->getCrossSellProductInfo();
$cross_sell_avail_dynamic_param2 = explode(",",$cross_sell_avail_dynamic_param2);
for($i = 0; $i < count($cross_sell_avail_dynamic_param2); ++$i){
	$cross_sell_avail_dynamic_param2[$i] = str_replace(" ", "-", $cross_sell_avail_dynamic_param2[$i]);
}
$cross_sell_avail_dynamic_param2 = implode(",",$cross_sell_avail_dynamic_param2);
$smarty->assign('articleType',$articleType);
$smarty->assign('agegroup',$agegroup);
$smarty->assign("avail_dynamic_param",$avail_dynamic_param);
$smarty->assign("cross_sell_avail_dynamic_param1",$cross_sell_avail_dynamic_param1);
$smarty->assign("cross_sell_avail_dynamic_param2",$cross_sell_avail_dynamic_param2);
if(!empty($_GET['t_code'])){
$smarty->assign("t_code",filter_input(INPUT_GET, 't_code', FILTER_SANITIZE_STRING));
}

if(!empty($_GET['uq'])){
$smarty->assign("uq",filter_input(INPUT_GET, 'uq', FILTER_SANITIZE_STRING));
}
if(!empty($_GET['searchQuery'])){
$smarty->assign("uqs",filter_input(INPUT_GET, 'searchQuery', FILTER_SANITIZE_STRING));
}
if(!empty($_GET['sku'])) {
	$smarty->assign("preselectSKU", filter_input(INPUT_GET,"sku", FILTER_SANITIZE_NUMBER_INT));
}

$rcv_avail=array();
if(!empty($_COOKIE['RCV_AVAIL'])){
	$rcv_avail=explode(',',filter_input(INPUT_COOKIE, 'RCV_AVAIL', FILTER_SANITIZE_STRING));
}
array_unshift($rcv_avail,$productStyleId);
$rcv_avail=array_unique($rcv_avail);
$rcv_avail=array_slice($rcv_avail,0,5);
$rcv_avail_str=implode(',',$rcv_avail);
setMyntraCookie('RCV_AVAIL',$rcv_avail_str,time()+3600*24*365,'/',$cookiedomain);
$smarty->assign("rcv_avail_str",$rcv_avail_str);

if(!is_null($tracker->pdpData)){
	$tracker->pdpData->setStyleId($productStyleId);
	$tracker->pdpData->setBrand($brand_name);
	$tracker->pdpData->setArticleType($articleType);
	$tracker->pdpData->setAvailableSizes(json_encode(array_values($sizeOptionSKUMapping)));
	$tracker->pdpData->setPreviousPDPStyleId($previousProductStyleId);
	$tracker->pdpData->setPreviousPDPRecommendationType($previousRecommendationType); 
	$tracker->pdpData->setPreviousPDPRecommendationStyleIds($previousRecommendationStyleIds);
}
if(!is_null($tracker->seoData)){
    if ($showCanonicalTag) {
        $tracker->seoData->isCanonical = true;
        $tracker->seoData->canonicalUrl = $canonicalPageUrl;
    }
}

$socialExcludeArray=addSocialExcludeToCache();
$socialExclude=checkSocialExclude($style_properties["global_attr_article_type"],$socialExcludeArray);
$smarty->assign('socialExclude',$socialExclude);

$tracker->fireRequest();

$analyticsObj = AnalyticsBase::getInstance();
$analyticsObj->setContextualPageData(AnalyticsBase::PDP);
$analyticsObj->setPDPEventVars($productStyleId, $style_properties["global_attr_season"], $style_properties["global_attr_year"]);
$analyticsObj->setTemplateVars($smarty);

//FOR EXPRESS BUY BUTTON TEXT
$smarty->assign('expressBuyLabel', WidgetKeyValuePairs::getWidgetValueForKey("expressbuy.label"));

// shop fest related texts, popups
$smarty->assign('festPdpText', WidgetKeyValuePairs::getWidgetValueForKey("fest_pdp_text"));
$smarty->assign('festPdpPopupTitle', WidgetKeyValuePairs::getWidgetValueForKey("fest_pdp_popup_title"));
$smarty->assign('festPdpPopupContent', WidgetKeyValuePairs::getWidgetValueForKey("fest_pdp_popup_content"));

$vatEnabled = FeatureGateKeyValuePairs::getBoolean('vat.enabled');
if($vatEnabled) {
    $vatThreshold = VatCalculator::getVatOnStyle($brand_name, $productStyleDetails["product_type"], $catalogData["article_id"]);
    if($discountPercent >= $vatThreshold) {
        $vatMessage = FeatureGateKeyValuePairs::getFeatureGateValueForKey('vat.pdp.message');
        $smarty->assign("showVatMessage", true);
        $smarty->assign("vatMessage", $vatMessage);
    }
    $vatCouponMessage = FeatureGateKeyValuePairs::getFeatureGateValueForKey('vat.pdpcoupon.message');
    $smarty->assign("vatThreshold",$vatThreshold);
    $smarty->assign("vatCouponMessage", $vatCouponMessage);
    $smarty->assign("vatEnabled",$vatEnabled);
}

/** Check for PDP Coupon Widget  START**/
$pdpCouponWidgetTitleText = WidgetKeyValuePairs::getWidgetValueForKey("PDPCouponWidgetTitleText");
$pdpCouponWidgetABTest=MABTest::getInstance()->getUserSegment("PDPCouponWidget");
$pdpCouponWidgetEnable = FeatureGateKeyValuePairs::getBoolean('PDPCouponWidget.enable');
$PDPCouponWidgetMobileEnable = FeatureGateKeyValuePairs::getBoolean('PDPCouponWidgetMobileEnable');
$PDPCouponWidgetTitleTextMobile = WidgetKeyValuePairs::getWidgetValueForKey("PDPCouponWidgetTitleTextMobile");
if(substr($pdpCouponWidgetABTest,0,4) == 'test' && $pdpCouponWidgetEnable){
    $pdpdCouponWidgetEnabled = true; 
}
else{
    $pdpdCouponWidgetEnabled = false; 
}

$smarty->assign('pdpdCouponWidgetEnabled',$pdpdCouponWidgetEnabled);
$smarty->assign('pdpCouponWidgetABTest',$pdpCouponWidgetABTest);
$smarty->assign('pdpCouponWidgetTitleText',$pdpCouponWidgetTitleText);
$smarty->assign('PDPCouponWidgetTitleTextMobile',$PDPCouponWidgetTitleTextMobile);
$smarty->assign('PDPCouponWidgetMobileEnable',$PDPCouponWidgetMobileEnable);

/** Check for PDP Coupon Widget  END**/


$product = array(
    SeoBreadCrumbKeyEnum::ProductName => $productStyleLabel , 
    SeoBreadCrumbKeyEnum::Brand => $style_properties['global_attr_brand'] ,
    SeoBreadCrumbKeyEnum::ArticleType => $catalogData["articletype"] ,
    SeoBreadCrumbKeyEnum::SubCategory => $catalogData["sub_category"] ,
    SeoBreadCrumbKeyEnum::MasterCategory => $catalogData["master_category"] ,
    SeoBreadCrumbKeyEnum::Gender => $gender 
);


$breadCrumb = new PDPBreadCrumb(); 
$smarty->assign("locationBasedBreadCrumb", $breadCrumb->getData($product));

$smarty->assign("LocationBreadcrumbABTest", MABTest::getInstance()->getUserSegment('LocationBreadcrumbABTest'));

$smarty->assign("pdpv1", MABTest::getInstance()->getUserSegment('pdpv1'));
$smarty->assign("pdpv1_phase2", MABTest::getInstance()->getUserSegment('pdpv1_phase2'));
$smarty->assign("contentServiceHost",\HostConfig::$contentServiceHost);
$smarty->assign("SoldByVendorText",WidgetKeyValuePairs::getWidgetValueForKey("SoldBy_Vendor_Text"));
$smarty->assign("SuppliedByPartnerTooltip",WidgetKeyValuePairs::getWidgetValueForKey("SuppliedBy_Partner_Tooltip"));
$smarty->display("pdp.tpl");
?>
