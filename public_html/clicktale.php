<?php
// ClickTale added here
require_once($xcart_dir. "/ClickTale/ClickTaleInit.php");
require_once(ClickTale_Root."/ClickTale.inc.php");
require_once(ClickTale_Root."/ClickTale.Logger.php");
require_once(ClickTale_Root."/ClickTale.Settings.php");

function clicktale_output_filter($tpl_source, &$templater) {
	return ClickTale_ProcessOutput($tpl_source);
}

$smarty->register_outputfilter("clicktale_output_filter");

?>