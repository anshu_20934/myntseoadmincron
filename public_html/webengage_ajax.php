<?php
require_once 'auth.php';
include_once $xcart_dir.'/include/class/class.mymyntra.php';
include_once $xcart_dir.'/modules/coupon/database/CouponAdapter.php';
include_once($xcart_dir."/Profiler/Profiler.php");
global $telesales;
if ($telesales){
    echo "{}";exit;
}

$returnArr = array();

if ($login) {
	/* fetch balance from MyntCashService */
	/* fetching it from raw service to avoid load on remote service */
	$handle = Profiler::startTiming("webengage_getCashback");
	$myntCashDetails = MyntCashRawService::getBalance($login);
	Profiler::endTiming($handle);

	$handle = Profiler::startTiming("webengage_instantiateMyMyntra");
	$myMyntra = new MyMyntra(mysql_real_escape_string($login));
	Profiler::endTiming($handle);

	$handle = Profiler::startTiming("webengage_getOrderCount");
	$counts = $myMyntra->getOrderAndReturnsCountsForDashboard($login);
	Profiler::endTiming($handle);

	$handle = Profiler::startTiming("webengage_instantiateCouponAdapter");
	$couponAdapter = CouponAdapter::getInstance();
	Profiler::endTiming($handle);
	
	$handle = Profiler::startTiming("webengage_getDaysToCouponExpiry");
	$expCoupons = $couponAdapter->getExpiringSoonCoupons($login);
	Profiler::endTiming($handle);

	$returnArr['cashback'] = $myntCashDetails['balance'];
	$returnArr['orderCount'] = $counts['completed_orders'] + $counts['pending_orders'];

	$handle = Profiler::startTiming("webengage_getCouponCount");
	$returnArr['couponCount'] = $couponAdapter->getActiveCouponsCount($login);
	Profiler::endTiming($handle);

	$returnArr['daysToCouponExpiry'] = $expCoupons['expdays'];
} else {
	$returnArr['cashback'] = '';
	$returnArr['orderCount'] = '';
	$returnArr['couponCount'] = '';
	$returnArr['daysToCouponExpiry'] = '';
}

$isCheckout = filter_input(INPUT_GET, 'checkout', FILTER_SANITIZE_NUMBER_INT);
if (!$isCheckout) {
	// $getStyleIds function is used in array_map below
	$getStyleIds = function($product) {
		return $product['productStyleId'];
	};
	// Cart data
	$handle = Profiler::startTiming("webengage_getCartData");
	include_once $xcart_dir.'/cart_details.php';
	Profiler::endTiming($handle);

	$returnArr['itemIds'] = implode(array_map($getStyleIds, $productsInCart),',');
	$returnArr['itemCount'] = count($productsInCart);
	$returnArr['totalQuantity'] = $totalQuantity;
	$returnArr['mrp'] = $totalMRP;
	$returnArr['isGiftOrder'] = $isGiftOrder;
	$returnArr['giftCharge'] = $giftCharges;
	$returnArr['shippingCharge'] = $shippingCharge;
	$returnArr['amount'] = $amount;
	$returnArr['itemDiscount'] = $productDiscount;
	$returnArr['cartLevelDiscount'] = $cartLevelDiscount;
	$returnArr['couponDiscount'] = $couponDiscount;
	$returnArr['cashDiscount'] = $cashDiscount;
	$returnArr['savings'] = $savings;
	$returnArr['totalAmount'] = $totalAmount;
}

if (!empty($returnArr)){
	$resp = array(
    	'status'  => 'SUCCESS',
        'content' => $returnArr
	);
}
else {
	$resp = array(
    	'status'  => 'ERROR',
        'content' => ''
	);
}

echo json_encode($resp);
//echo "<pre>";print_r($returnArr);